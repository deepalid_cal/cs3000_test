/*  file = foal_flow.c                10.27.00 2011.08.01 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for fabsf
#include	<math.h>

// 6/18/2014 ajv : Required for abs
#include	<stdlib.h>

// 6/18/2014 ajv : Required for memcpy and memset
#include	<string.h>

#include	"foal_flow.h"

#include	"foal_comm.h"

#include	"irri_comm.h"

#include	"foal_irri.h"

#include	"app_startup.h"

#include	"configuration_controller.h"

#include	"flow_recorder.h"

#include	"alerts.h"

#include	"cal_string.h"

#include	"comm_mngr.h"

#include	"cal_math.h"

#include	"change.h"

#include	"station_groups.h"

#include	"flowsense.h"

#include	"cs_mem.h"

#include	"alert_parsing.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*
    HOW FLOW READINGS PROPAGATE THROUGHOUT THE SYSTEM AND THE SUPPORTING DATA STRUCTURES:

    Each flow meter belongs to a POC. A network has a limit of 8 POC's. This seems ample
    enough to cover the systems we will encounter. Note POC's will be consumed for
    non-irrigation things. Such as a MLB monitor, a scheduled pond fill device, etc.

    At first I thought the POC accumulators would be kept locally at each box. But I think
    I'll keep them at the master. Each box will keep a 60 reading queue of the raw second by
    second count for each POC it owns. We will keep a record of which readings have been
    transferred to the master. The choice of 60 seconds worth of readings is supposed to
    represent the longest time between tokens the network would ever have.
	
    So there will be a local (IRRI side) by POC structure kept in SDRAM. (Keeping in battery
    backed along with report data by poc for ease. If battery backed SRAM space becomes an
    issue can move to SDRAM.) It holds the array of 60 most recent readings. For each
    reading we know if it has previously been transferred to the master (transferred via a
    token response that is).
	
    The POC accumulators will be kept at the network master (within the LPC3250 board). As
    an experiment I want to keep two differently calculated accumulators. One will use a
    strict second-by-second reading method and the other a TBD seconds reading average type
    method. The purpose is to see which method better matches actual water meter billings.

    At the master (FOAL side), in (battery backed for convenience) SRAM we will keep a by
    POC structure. For each possible POC it will hold the array of the 60 lastest readings.
    And it will contain the POC accumulator record in process (rip). The accumulators kept
    in battery backed SRAM are a 24 hour based tally. The time of day for the accumulation
    roll-over is a by POC (maybe should be a by system setting) setting.
	
	ONCE PER SECOND AT EACH BOX (IRRI SIDE):
    - The only thing that happens once per second is down on the TP Micro. And that is just
      to keep the resident POC flow meter reading array current. The 2-wire POC decoder flow
      meter arrays are kept up to date via the 2-wire communication process. The 60 raw
      count array on the irri side is kept up to date via communications with the TP Micro.
	  
	ONCE PER RCVD TOKEN AT EACH BOX (IRRI SIDE):
    - From the token pick out the current flow rate for each valve ON in the network. Find
      the valve in the Irri side ILC list and adjust it's share of flow. This number is for
      display purposes. Also pick out the 5 second average for each POC flow meter in the
      network. Again this is for display purposes.
    - In the token response back to the master, for each POC at this box, return the raw
      meter readings that have not previously been sent to the master.
		
    ONCE PER SECOND AT THE MASTER (FOAL SIDE):
    - For each POC one second flow rate	value that has not been used perform the following:
      * Add the reading to the appropriate POC accumulator. Using the two different methods.
      * Add the reading to any SYSTEM level accumulators (if any) for the system the POC
        belongs to.
    - Once per second compute the latest 5 second average gpm value for each flow meter in
      each POC. These are the values that get distributed with each token for display
      purposes. But we are careful to ONLY send the values for the non-zero numbers in order
      to minimize token size. Also once per second we also add these up to create the system
      5 second average flow rate for each system. We keep the most recent 20 of them. And
      use them to create the stability average each second.
    - Also using the 5 second average system flow rate value, figure out the share of flow
      for each valve ON and post into the ilc list.

    ONCE PER RCVD TOKEN RESP AT THE MASTER (FOAL SIDE):
    - Pick out the raw readings for the POC owned by that box and place in the POC battery
      backed flow meter reading arrays.
	  
    ONCE PER TOKEN GENERATED AT THE MASTER (FOAL SIDE):
    - Distribute the 5 second reading average for each POC in the network. Also distribute
      the share of flow for each station ON.
*/ 
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Note from ET2000e : Here is the chart which controls when to do what. The chart should
// not care if there is more than 1 controller - what it cares about is if the
// communications is ok.
//
//      	     (2)	   HAVE        PROG    	            
//   (1) 	 IRRIGATING     A          IN       CONTROLLER    CONTROLLER
// TESTING       BY        FLOW       DAILY	      USING	       TRACKING			   REPORTS 
//  FLOW      CAPACITY     METER       ET	     BUDGETS	   EXPECTED 	        TALLY
// ----------------------------------------------------------------------------------------
//	 YES	     X       (implied)      X           X             X				   ACTUAL
//	  NO        YES         YES         X	        X	          X				   ACTUAL
//	  NO        YES         NO          X	        X	          X				   EXPECTED
//
//	  NO         NO         YES         X	        X	          X				   ACTUAL
//
//	  NO	     NO         X          YES		    X		      X				   EXPECTED
//	  NO	     NO         X           NO 		   YES		      X			       EXPECTED
//	  NO	     NO         X           NO		    NO           YES               EXPECTED
//
//	  NO	     NO         X           NO 		    NO	          NO                 ZEROS
//
// Notes:
// (1) TESTING FLOW can be true only when we have a flow meter and the communications is OK.
// (2) IRRIGATING BY CAPACITY can be true only when communications is OK.
//
// ALSO NOTE: For clarity I have left in the TESTING FLOW column. It actually can be removed. The logic
//			  boils down to if we have a flow meter we will tally ACTUALS! Additionally if we have a 
//			  flow meter we	 expect the expecteds to be accurate! (used in divi up the actual amongst the
//			  stations on)	
//
// END OF 2000e NOTE
//
// 2012.01.19 rmd : For the CS3000 this all simplifies quite a bit in my mind. If there is
// at least one flow meter in the system all tallys are driven by the real flow rate as
// reported from the flow meters. Otherwise, without a flow meter, what is possible to tally
// is done so with the expecteds. And those possible things are system accumulators, budget
// accumulators, station report data accumulators. And they are all driven via the latest 5
// second average or the ilc share of expected numbers.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Function called by TD_CHECK at approximately a 1 Hz rate. That may have
    jitter in due to task priorities - higher priority tasks of course delaying this
    processing. But such jitter is okay as the raw flow numbers that go into the averages
    change asynchronously to real time with each incoming token anyway. What we can be
    assured of is that each of the raw flow readings does indeed represent the count over a
    1 second period. Once per second compute the latest 5 second average gpm value for each
    flow meter in each POC. These are the values that get distributed with each token for
    display purposes. But we are careful to ONLY send the values for the non-zero numbers in
    order to minimize token size. Also once per second we also add these up to create the
    system 5 second average flow rate for each system. We keep the most recent 20 of them.
    And use them to create the stability average each second.
	
    @CALLER MUTEX REQUIREMENTS (none) This function will take the poc_presrves MUTEX 
    internally as we are working with the raw flow readings array.

    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (none)

	@AUTHOR 2012.01.13 rmd
*/
extern void FOAL_FLOW__A__build_poc_flow_rates( void )
{
	// 8/27/2014 rmd : See the same constant declarations in the bypass_operation.c file for
	// more explanation. We don't have the optimization problem here that is described there.
	// However as a cautionary move I will implement them here too. The VOLATILE constants seem
	// to allow a work around to a compiler bug. As I said better described in
	// bypass_operation.c.
	const volatile float ONE_HUNDRED_THOUSAND_POINT_ZERO = 100000.0;

	const volatile float ONE_THOUSAND_POINT_ZERO = 1000.0;

	const volatile float SIXTY_POINT_ZERO = 60.0;

	const volatile float FIVE_POINT_ZERO = 5.0;

	// ----------

	POC_FM_CHOICE_AND_RATE_DETAILS	fm_choice_and_rate_details[ POC_BYPASS_LEVELS_MAX ];
	
	UNS_32	p, f, levels;
	
	float	k_times_offset_term, k_times_freq_term;
	
	BOOL_32	saw_an_error;
	
	void 	*lpoc;
	
	BOOL_32	zero_em_all_out;

	UNS_32	time_limit;
	
	UNS_32	pulse_multiplier;

	float 	gpm_multiplier;
	
	// ----------
	
	saw_an_error = (false);

	// 2012.01.13 rmd : Yes we are about to peruse through the poc_preserves. And they can also
	// be changed by the COMM_MNGR task. So protect we will. To make sure the other task is done
	// with their change.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
	
	xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	for( p=0; p<MAX_POCS_IN_NETWORK; p++ )
	{
		if( poc_preserves.poc[ p ].poc_gid != 0 )
		{
			// 2012.01.19 rmd : Load up the flow meter K and O for each meter. And ONLY if the function
			// was successful in finding the group do we try to compute values.
			if( POC_get_fm_choice_and_rate_details( poc_preserves.poc[ p ].poc_gid, fm_choice_and_rate_details ) == (false) )
			{
				saw_an_error = (true);
			}
			
			// ----------
			
			// 7/10/2014 rmd : Though a function that got the number of bypass levels using the gid
			// would eliminate getting a pointer to the poc in the file, this approach actually provides
			// a level of verification that the preserves and the file are indeed in sync. And that our
			// data structures are intact.
			lpoc = nm_POC_get_pointer_to_poc_with_this_gid_and_box_index( poc_preserves.poc[ p ].poc_gid, poc_preserves.poc[ p ].box_index );
			
			if( lpoc == NULL )
			{
				// 7/9/2014 rmd : This is an error. Alert and set error flag.
				ALERT_MESSAGE_WITH_FILE_NAME( "file POC not found" );

				saw_an_error = (true);
			}
			else
			{
				// 7/9/2014 rmd : Query the appropriate levels based upon the type of POC we are working
				// with.
				levels = 1;
				
				if( POC_get_type_of_poc( poc_preserves.poc[ p ].poc_gid ) == POC__FILE_TYPE__DECODER_BYPASS )
				{
					// 7/8/2014 rmd : Use the formal GET function so value is range checked.
					levels = POC_get_bypass_number_of_levels( lpoc );
				}
			}
			
			// ----------
			
			for( f=0; f<POC_BYPASS_LEVELS_MAX; f++ )
			{
				zero_em_all_out = (false);

				if( saw_an_error )
				{
					zero_em_all_out = (true);
				}
				else
				if( f >= levels )
				{
					zero_em_all_out = (true);
				}
				else
				{
					// 2012.01.19 rmd : If the flow meter is turned OFF. Meaning set to NONE_IN_USE do not
					// include the values coming from the flow meter. Even if the hardware reports such. This
					// may be the first place in the code this block (to turn OFF a flow meter) is.
					if( fm_choice_and_rate_details[ f ].flow_meter_choice != POC_FM_NONE )
					{
						if( (fm_choice_and_rate_details[ f ].flow_meter_choice >= POC_FM_BERMAD_0150) && (fm_choice_and_rate_details[ f ].flow_meter_choice <= POC_FM_BERMAD_1000) )
						{
							// 1/16/2015 rmd : These flow meters use a 1 pulse per gallon or 10 pulse per gallon scheme.
							// Goal here is to calculate two values. The latest average flow rate and the latest gallons
							// for the accumulators.
							
							// 1/30/2015 rmd : To come up with a flow rate we look at the delta_4khz between the last 2
							// pulses and the time since the last pulse.
							
							// 1/20/2016 rmd : Comments on the values used below. On the CS3000 platform using the 4khz
							// counter to measure the time between pulses there is effectively no MAXIMUM flow rate.
							// There is a max after which the error measure the delta between the pulses becomes too
							// large but that number is a physical impossiblity for our irrigation systems (more than
							// 20,000gpm).
							//
							// On the minimum side the name of the game is how long does the user wait before the
							// displayed flow rate goes to 0. When the pulses slow to stop we have a time limit we wait
							// for the next pulse before declaring the flow rate 0. We don't want to keep the user
							// waiting too long. In the 2000e this number was 40 seconds. Well here in the CS3000 we
							// have made that 1 minute for the 10x meter (represents 10gpm) and 20 seconds for the 1x
							// meter (represents 3gpm). Just keep in mind the lower you go the longer the user has to
							// wait to see the flow go to 0.
							if( (fm_choice_and_rate_details[ f ].reed_switch == POC_FM_REED_SWITCH_1_TO_1) || (fm_choice_and_rate_details[ f ].reed_switch == POC_FM_REED_SWITCH_1_TO_10) )
							{
								if( fm_choice_and_rate_details[ f ].reed_switch == POC_FM_REED_SWITCH_1_TO_1 )
								{
									time_limit = 20;
									
									pulse_multiplier = 1;
									
									gpm_multiplier = 60.0;
								}
								else
								{
									time_limit = 60;

									pulse_multiplier = 10;
									
									gpm_multiplier = 600.0;
								}
								
								// 1/30/2015 rmd : ONLY use the delta between pulses if it isn't zero.
								if( (poc_preserves.poc[ p ].ws[ f ].fm_delta_between_last_two_fm_pulses_4khz_foal > 0) && (poc_preserves.poc[ p ].ws[ f ].fm_seconds_since_last_pulse_foal < time_limit) )
								{
									// 1/30/2015 rmd : The equation is 1 divided by seconds between last two pulses times the
									// gpm_multiplier. Number of seconds is 4khz count divided by 4000.
									poc_preserves.poc[ p ].ws[ f ].latest_5_second_average_gpm_foal = ( (4000.0 / poc_preserves.poc[ p ].ws[ f ].fm_delta_between_last_two_fm_pulses_4khz_foal) * gpm_multiplier );
								}
								else
								{
									poc_preserves.poc[ p ].ws[ f ].latest_5_second_average_gpm_foal = 0.0;
								}

								// ----------
								
								// 2/13/2015 rmd : Debug line. Commented out now.
								//Alert_Message_va( "FOAL 1hz - count is %u", poc_preserves.poc[ p ].ws[ f ].fm_accumulated_pulses_foal );

								// 1/30/2015 rmd : ALWAYS add in the accumulated pulses. Regardless of how long since the
								// last pulse. I think that is the right thing to do. A pulse is a pulse and always valid
								// when it comes to accumulation.
								poc_preserves.poc[ p ].ws[ f ].accumulated_gallons_for_accumulators_foal += ( poc_preserves.poc[ p ].ws[ f ].fm_accumulated_pulses_foal * pulse_multiplier );

								// 2/11/2015 rmd : And since we've accounted for these pulses zero them out.
								poc_preserves.poc[ p ].ws[ f ].fm_accumulated_pulses_foal = 0;
							}
							else
							{
								zero_em_all_out = (true);
								
								Alert_Message( "POC : Unexpected Bermad Reed choice" );
							}
							
						}
						else
						{
							// 1/16/2015 rmd : These flow meters use the traditional K and OFFSET equeation to calulate
							// the flow information.

							// 5/29/2014 rmd : We are about to calculate the essentially real time flow rate using the
							// last reported accumulated 5 second count. How old this count actually is depends on
							// several factors but primarily the number of controllers in the chain, the communications
							// medium, and if they are decoder based or not.
							
							// 2012.01.19 rmd : ONLY if the reading is non-zero do we go ahead and compute the 5 second
							// gpm value. Because the equation produces a gpm even if the value is 0. And that would
							// ALWAYS show a flow rate. Incorrectly.
							if(  poc_preserves.poc[ p ].ws[ f ].fm_latest_5_second_pulse_count_foal > 0 )
							{
								// 6/3/2014 rmd : At a one hertz rate the equation is : gpm = (K * count) + (K * offset)
								//
								// So if we take the count for the full 5 seconds and plug it into this equation we get the
								// gpm as if all that flow occurred in one second. So divide by 5 to get the average for the
								// 5 second window. BUT the K*O term is meant to be added in each second. So add that in 5
								// times before dividing the total by 5.
								k_times_freq_term = ( (fm_choice_and_rate_details[ f ].kvalue_100000u / ONE_HUNDRED_THOUSAND_POINT_ZERO) * poc_preserves.poc[ p ].ws[ f ].fm_latest_5_second_pulse_count_foal );
								
								k_times_offset_term = ( FIVE_POINT_ZERO * (fm_choice_and_rate_details[ f ].kvalue_100000u / ONE_HUNDRED_THOUSAND_POINT_ZERO) * (fm_choice_and_rate_details[ f ].offset_100000u / ONE_HUNDRED_THOUSAND_POINT_ZERO) );
								
								poc_preserves.poc[ p ].ws[ f ].latest_5_second_average_gpm_foal = ( (k_times_freq_term + k_times_offset_term) / FIVE_POINT_ZERO );
								
								// 9/12/2012 rmd : With a NEGATIVE offset (see the 1.5 PVC meter) if the pulse count is low
								// enough we can actually develop a negative flow rate here. That needs to be suppressed. So
								// the user doesn't see a negative flow rate and raise questions.
								if( poc_preserves.poc[ p ].ws[ f ].latest_5_second_average_gpm_foal < 0.0 )
								{
									poc_preserves.poc[ p ].ws[ f ].latest_5_second_average_gpm_foal = 0.0;
								}
							}
							else
							{
								poc_preserves.poc[ p ].ws[ f ].latest_5_second_average_gpm_foal = 0.0;
							}
							
							// ----------
							
							// 6/3/2014 rmd : SANITY check. This should always be zero meaning the data has been added
							// to the accumulators between the time it is set herein to before the next time this
							// function executes. That is the program flow we are expecting.
							if( poc_preserves.poc[ p ].ws[ f ].accumulated_gallons_for_accumulators_foal != 0.0 )
							{
								ALERT_MESSAGE_WITH_FILE_NAME( "Unexp flow accum gal value" );
							}
							
							// 6/3/2014 rmd : Now we should convert any accumulated pulses into an accumulated gallons.
							// For the companion system level function to add these values to accumulated gallons at the
							// system level. Both of which after acting on them to add their values to the various
							// accumulators are ZEROED out.
							if( poc_preserves.poc[ p ].ws[ f ].fm_accumulated_pulses_foal > 0 )
							{
								k_times_freq_term = ( (fm_choice_and_rate_details[ f ].kvalue_100000u / ONE_HUNDRED_THOUSAND_POINT_ZERO) * poc_preserves.poc[ p ].ws[ f ].fm_accumulated_pulses_foal );
								
								k_times_offset_term = ( (poc_preserves.poc[ p ].ws[ f ].fm_accumulated_ms_foal/ONE_THOUSAND_POINT_ZERO) * (fm_choice_and_rate_details[ f ].kvalue_100000u / ONE_HUNDRED_THOUSAND_POINT_ZERO) * (fm_choice_and_rate_details[ f ].offset_100000u / ONE_HUNDRED_THOUSAND_POINT_ZERO) );
								
								// 6/3/2014 rmd : To get the gallons directly add the two terms and divide by 60. Adding the
								// two terms gives gpm. So dividing by 60 give gallons. (Note - add to the accumulated
								// gallons here. Not required to add to the variable because of the code structure it is
								// supposed to be zero (tdcheck does all the work to this variable) but no harm.)
								poc_preserves.poc[ p ].ws[ f ].accumulated_gallons_for_accumulators_foal += ( (k_times_freq_term + k_times_offset_term) / SIXTY_POINT_ZERO );
								
								// 9/12/2012 rmd : With a NEGATIVE offset (see the 1.5 PVC meter) if the pulse count is low
								// enough we can actually develop a negative flow rate here. That needs to be suppressed. So
								// the user doesn't see a negative flow rate and raise questions.
								if( poc_preserves.poc[ p ].ws[ f ].accumulated_gallons_for_accumulators_foal < 0.0 )
								{
									poc_preserves.poc[ p ].ws[ f ].accumulated_gallons_for_accumulators_foal = 0.0;
								}
								
								// ----------
								
								// 6/3/2014 rmd : And since we've accounted for these pulses zero them out.
								poc_preserves.poc[ p ].ws[ f ].fm_accumulated_pulses_foal = 0;

								poc_preserves.poc[ p ].ws[ f ].fm_accumulated_ms_foal = 0;
							}
						}
						
					}
					else
					{
						// 6/5/2014 rmd : If no flow meter ALL should be 0 right.
						zero_em_all_out = (true);
					}

				}  // of if this level is active
				
				// ----------
				
				// 1/30/2015 rmd : If error or otherwise zero all concerned.
				if( zero_em_all_out )
				{
					// 6/3/2014 rmd : Force the unused levels to a 0.0 flow rate for housekeeping cleanliness.
					poc_preserves.poc[ p ].ws[ f ].fm_accumulated_pulses_foal = 0;

					poc_preserves.poc[ p ].ws[ f ].fm_accumulated_ms_foal = 0;

					poc_preserves.poc[ p ].ws[ f ].fm_delta_between_last_two_fm_pulses_4khz_foal = 0;
					
					// 1/16/2015 rmd : Don't particularly need to set the seconds since because if the delta is
					// 0 a flow rate calcualtion will not be attempted.
					poc_preserves.poc[ p ].ws[ f ].fm_seconds_since_last_pulse_foal = 0;


					poc_preserves.poc[ p ].ws[ f ].latest_5_second_average_gpm_foal = 0.0;

					poc_preserves.poc[ p ].ws[ f ].accumulated_gallons_for_accumulators_foal = 0.0;
				}


			}  // of each level
		}
	}

	// ----------

	xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION 2011.01.17 rmd : New CS3000 summary: Within a SYSTEM there doesn't have to
    be a flow meter. There doesn't even HAVE to be a POC. But we could have a POC just for
    the purposes of operating a MV. Without a flow meter. With regards to communication
    being up and running: if we are in FORCED I don't think we are doing much in the way of
    tallying flows or checking flows. Actually I think none at all. Why bother and why
    complicate things. With regards to when expected flows are to be considered valid. We
    need expected flows to be accurate for a number of reasons. First if we are testing flow
    we use the expecteds to figure out the normal flow rate which is compared against the
    actual flow meter rate. If we are irrigating by capacity the expecteds are used to
    figure out how many valves are allowed ON. If we are using budgets the expected rates
    are used to predict the consumption over the budget period. If we are using Daily ET
    then the expecteds are used to figure out how long to irrigate for. So if any of those
    options are enabled the expecteds are considered to be valid.

    If there is one or more flow meters in a system we will calculate the system 5 second
    average. And the stability average. Wether we are checking flow or not. And we will use
    the expected flow rates for theose ON to divi up the system 5 second average for each
    ilc ON.

    If there is NO flow meter the system average flow rate will be 0 unless valves are ON.
    In which case the average system flow rate is the sum of the expecteds for those ON. And
    each ilc share will be its own expected.
	
	We will also fill in some system statistics. Such as is there a flow meter or not.

	@CALLER MUTEX REQUIREMENTS

    @EXECUTED Executed within the context of the TD_CHECK task. At a 1 hz rate.
	
	@RETURN (none)

	@AUTHOR 2012.01.00 rmd
*/
extern void FOAL_FLOW__B__build_system_flow_rates( void )
{
	// 5/29/2014 rmd : This function executes within the context of the TDCHECK task at a 1
	// hertz rate.

	// ----------
	
	// 9/10/2014 rmd : We have seen elsewhere in the code that optimized code with literal
	// floating point values in equations produces garbage outcomes. I believe this to be a
	// known GCC compiler bug. As test show the problem corrected in GCC 4.8 something. A
	// workaround AJ came up with is to use these volatile constants.
	const volatile float ZERO_POINT_SEVEN = 0.7;

	const volatile float ZERO_POINT_THREE = 0.3;

	const volatile float ONE_HUNDRED_POINT_O = 100.0;

	const volatile float ONE_POINT_O = 1.0;

	// ----------
	
	UNS_32		s, p, f, levels;
	
	POC_FM_CHOICE_AND_RATE_DETAILS	fm_choice_and_rate_details[ POC_BYPASS_LEVELS_MAX ];
	
	UNS_32		mv_no_or_nc[ POC_BYPASS_LEVELS_MAX ];
	
	// Work with a pointer to the next index so we can increment it at the end. And to keep
	// referencing it easier to read.
	UNS_32		*next_index;
	
	BOOL_32		saw_an_error;
	
	void		*lpoc;
	
	// ----------
	
	saw_an_error = (false);

	// ----------

	// 2012.01.17 rmd : Yes we are about to peruse through the system_preserves. And they can
	// also be changed by the COMM_MNGR task. So protect against potential clash.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// At this point we are supposed to have a system_preserves array that reflects the system
	// file (all systems in the network). So for each non-zero gid we can go through and find
	// all the POC's that belong to it and compute the latest 5 second average.
	
	for( s=0; s<MAX_POSSIBLE_SYSTEMS; s++ )
	{
		if( system_preserves.system[ s ].system_gid != 0 )
		{
			// 6/3/2014 rmd : Start zeroed out. Once tallied here in this function, the report data
			// accumulator function WILL be called before this function can execute again. BY DESIGN. So
			// the gallons have already been added. As a matter of fact the accumulated gallons should
			// always be 0 here at this point as proof the code execution order is as expected.
			if( system_preserves.system[ s ].accumulated_gallons_for_accumulators_foal != 0.0 )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "Unexp flow accum gal value" );
			}
			
			// 6/3/2014 rmd : Set to zero for clarity.
			system_preserves.system[ s ].accumulated_gallons_for_accumulators_foal = 0.0;
			
			// ----------

			// 3/11/2016 rmd : Initialize the following variables in preparation for the coming loop
			// through the poc_preserves. Following the loop they are set correctly and available for
			// other processes to use.
			system_preserves.system[ s ].sbf.number_of_flow_meters_in_this_sys = 0;

			system_preserves.system[ s ].sbf.number_of_pocs_in_this_system = 0;

			system_preserves.system[ s ].sbf.there_are_pocs_without_flow_meters = (false);

			system_preserves.system[ s ].sbf.master_valve_has_at_least_one_normally_closed = (false);

			// ----------
			
			next_index = &(system_preserves.system[ s ].system_master_5_sec_avgs_next_index);

			system_preserves.system[ s ].system_master_5_second_averages_ring[ *next_index ] = 0.0;

			// Because we are working with the values embedded within the poc preserves array protect
			// against another task changing such.
			xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

			xSemaphoreTakeRecursive( list_poc_recursive_MUTEX, portMAX_DELAY );

			for( p=0; p<MAX_POCS_IN_NETWORK; p++ )
			{
				// If the record is in use.
				if( poc_preserves.poc[ p ].poc_gid != 0 )
				{
					// If the system gid matches.
					if( poc_preserves.poc[ p ].this_pocs_system_preserves_ptr == &(system_preserves.system[ s ]) )
					{
						// 2012.01.19 rmd : We've got a hit. This poc is part of this system. So add in the
						// contribution. Don't forget to add in for ALL three flow meters. If not in use we make
						// sure they are 0 elsewhere. We need to get the details of the POC flow meters so we can
						// see how many are in use. It is not so much the total number, though that may be
						// interesting to see, but more the fact that the system has at least ONE flow meter that we
						// are after. . Load up the flow meter K and O for each meter. And ONLY if the function was
						// successful in finding the group do we try to compute values.

						// ----------
						
						// 2012.01.19 rmd : Load up the flow meter K and O for each meter. And ONLY if the function
						// was successful in finding the group do we try to compute values.
						if( POC_get_fm_choice_and_rate_details( poc_preserves.poc[ p ].poc_gid, fm_choice_and_rate_details ) == (false) )
						{
							saw_an_error = (true);
						}
						
						// ----------
						
						// 2012.01.19 rmd : Load up the master valve NO NC information each level. And ONLY if the
						// function was successful in finding the group do we use the data.
						if( POC_get_master_valve_NO_or_NC( poc_preserves.poc[ p ].poc_gid, mv_no_or_nc ) == (false) )
						{
							saw_an_error = (true);
						}
						
						// ----------
						
						// 7/10/2014 rmd : Though a function that got the number of bypass levels using the gid
						// would eliminate getting a pointer to the poc in the file, this approach actually provides
						// a level of verification that the preserves and the file are indeed in sync. And that our
						// data structures are intact.
						lpoc = nm_POC_get_pointer_to_poc_with_this_gid_and_box_index( poc_preserves.poc[ p ].poc_gid, poc_preserves.poc[ p ].box_index );
						
						if( lpoc == NULL )
						{
							// 7/9/2014 rmd : This is an error. Alert and set error flag.
							ALERT_MESSAGE_WITH_FILE_NAME( "file POC not found" );
			
							saw_an_error = (true);
						}
						else
						{
							// 7/9/2014 rmd : Query the appropriate levels based upon the type of POC we are working
							// with.
							levels = 1;
							
							if( POC_get_type_of_poc( poc_preserves.poc[ p ].poc_gid ) == POC__FILE_TYPE__DECODER_BYPASS )
							{
								// 7/8/2014 rmd : Use the formal GET function so value is range checked.
								levels = POC_get_bypass_number_of_levels( lpoc );
							}
						}
						
						// ----------
						
						if( !saw_an_error )
						{
							// 3/11/2016 rmd : The world is intact so count this POC as part of this system.
							system_preserves.system[ s ].sbf.number_of_pocs_in_this_system += 1;

							// ----------

							for( f=0; f<levels; f++ )
							{
								// 5/29/2014 rmd : It is a valid configuration to have a non-bypass POC without a flow
								// meter. Meaning only a MV. In that case be sure to skip over counting the number of flow
								// meters or adding the the 5 second avg flow rate to the flow ring buffer.
								if( fm_choice_and_rate_details[ f ].flow_meter_choice != POC_FM_NONE )
								{
									// 3/11/2016 rmd : Add one on. Cannot overflow being 6 bits can hold up to 12 bypass poc's.
									system_preserves.system[ s ].sbf.number_of_flow_meters_in_this_sys += 1;
									
									// ----------
									
									system_preserves.system[ s ].system_master_5_second_averages_ring[ *next_index ] += poc_preserves.poc[ p ].ws[ f ].latest_5_second_average_gpm_foal;

									// ----------
									
									// 6/3/2014 rmd : Take the poc accumulated gallons and add to the system version. BUT do not
									// yet zero this poc value. It is still needed for the report data poc accumulators. It is
									// zeroed there.
									system_preserves.system[ s ].accumulated_gallons_for_accumulators_foal += poc_preserves.poc[ p ].ws[ f ].accumulated_gallons_for_accumulators_foal;
								}
								else
								{
									// 7/10/2014 rmd : Turns out when a bypass is first created there is plenty of time between
									// then and when the user sets the flow meter specifics. During this time we indicate
									// missing flow meters in the system. Which causes us to not perform any flow checking, to
									// accumulate using the expecteds, and to do our budgets work using expecteds. And this is
									// the right thing to do.
									system_preserves.system[ s ].sbf.there_are_pocs_without_flow_meters = (true);
								}
								
								// ----------
								
								if( mv_no_or_nc[ f ] == POC_MV_TYPE_NORMALLY_CLOSED )
								{
									system_preserves.system[ s ].sbf.master_valve_has_at_least_one_normally_closed = (true);
								}
								
							}  // for each fm in the poc
	
						}  // of if we can get the fm details
						else
						{
							// 7/10/2014 rmd : Abandon trying to find poc's for the system. This should never happen of
							// course.
							break; 
						}
					}  // of if this poc belongs to this system
				}

			}  // of each possible poc
			
			xSemaphoreGiveRecursive( list_poc_recursive_MUTEX );

			xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

			// ----------------------------

			// 8/1/2016 rmd : NO flow checking allowed there are no flow meters on the mainline.
			if( system_preserves.system[ s ].sbf.number_of_flow_meters_in_this_sys == 0 )
			{
				system_preserves.system[ s ].sbf.flow_checking_enabled_and_allowed = (false);
			}
			else
			{
				// 5/31/2012 rmd : We have the correct number of flow meters in the system. At this point it
				// is up to the user setting.
				system_preserves.system[ s ].sbf.flow_checking_enabled_and_allowed = system_preserves.system[ s ].sbf.flow_checking_enabled_by_user_setting;
			}
			
			// -----------------------------
			
			// 7/19/2012 rmd : If there are NO flow meters in the system the 5 second average value in
			// the ring is 0 at this point. The system_preserves bit_field, the expected of those ON,
			// and the actual most recent 5 second avg are distributed to all IRRI machines on the
			// chain. The display functions are to use the tallys_driven_with_expecteds bit to decide
			// which flow rate number to display as the present flow rate. And indicate if that is from
			// expecteds or a real flow meter. The point being DO NOT load the
			// system_master_most_recent_5_second_average with the expecteds of those ON. The display
			// routines have both values (the real and the sum of the expecteds) to figure which to
			// display. Anyway, assign the convenience variable so it is clear what the latest 5 second
			// system average is without having to go through an index decrement.
			system_preserves.system[ s ].system_master_most_recent_5_second_average = system_preserves.system[ s ].system_master_5_second_averages_ring[ *next_index ];
			
			// -----------------------------
			
			// NOW - update index to point at the NEXT 5 second average we would build up when we
			// execute this function one second from now.
			INC_RING_BUFFER_INDEX( (*next_index), SYSTEM_5_SECOND_AVERAGES_KEPT );
			
			// -----------------------------
			
		}  // Of if the system is in use.

	}  // For all the possible systems.
	

	// NOW make the stability average and assign it to where the stability next pointer points.
	// The stability average is a weighted 30 70 computation made up of the latest 20 5 second
	// averages.
	
	// For each system.
	for( s=0; s<MAX_POSSIBLE_SYSTEMS; s++ )
	{
		// If it is in use.
		if( system_preserves.system[ s ].system_gid != 0 )
		{
			float	new_20_second_avg, new_stability_avg;
			
			INT_32	r;
			
			new_20_second_avg = 0.0;
			
			// 2012.01.19 rmd : Because the stability is made up of ALL the retained 5 second averages
			// we just add 'em all up.
			for( r=0; r<SYSTEM_5_SECOND_AVERAGES_KEPT; r++ )
			{
				new_20_second_avg += system_preserves.system[ s ].system_master_5_second_averages_ring[ r ];
			}
			
			new_20_second_avg = (new_20_second_avg / SYSTEM_5_SECOND_AVERAGES_KEPT);
			
			// I only introduced this variable to facitlitate the code construction. Maybe to make it
			// more readable. Technically not needed.
			UNS_32	*stability_ring_index;
			
			stability_ring_index = &(system_preserves.system[ s ].stability_avgs_index_of_last_computed);
			
			// 2012.01.27 rmd : NOTE - I have watched this stability average thing and the stable_flow
			// algorithm in action. If you input a fixed 60 Hz clock it takes right around 1 minute to
			// go 'stable'. The stability averages move up as expected. The issue becomes the stablity
			// test looking at the NOW, 15 seconds ago, and 30 seconds ago numbers and comparing them.
			// That means the stability average array must be full of values AT THOSE POINTS IN TIME
			// that are within the 1% test of one another. Well it is what it is I guess. We have
			// deployed this method for quite sometime. Successfully I might add. So I leave it as is.
			//
			// A change MIGHT be to look at the stability average array as a whole. Averaging perhaps
			// groups of 10 together. And then compare thos averages in turn to be within 1% of each
			// other. Something like that. Just a thought.

			// Take 70% of the last stability average computed.
			new_stability_avg = ( ZERO_POINT_SEVEN * system_preserves.system[ s ].system_stability_averages_ring[ *stability_ring_index ] );
			
			// Add to it 30% of the new 5 second average computed.
			new_stability_avg += ( ZERO_POINT_THREE * new_20_second_avg );
			
			// Now bump the index. And REMEMBER when looking for the most recent stability average this
			// index value is pointed right at it. Ready to use.
			INC_RING_BUFFER_INDEX( (*stability_ring_index), SYSTEM_STABILITY_AVERAGES_KEPT );
			
			// 9/12/2012 rmd : With all this floating point math and this diminishing average we
			// eventually calculate numbers below the range of the smallest floating point number. Which
			// may be returned as <negative infinity>. There is an EXCEPTION generated about 4 minutes
			// after the flow stops. The stability averages are very small and the math performed runs
			// into some kind of limit and generates a data abort exception. This simple statement
			// seemingly clears up the issue. We circumvent the averaging to 0 when the numbers get
			// small.
			if( new_stability_avg < 0.1 )
			{
				new_stability_avg = 0.0;
			}

			// Assign it.
			system_preserves.system[ s ].system_stability_averages_ring[ *stability_ring_index ] = new_stability_avg;
			

			// 2012.01.19 rmd : STABILITY TEST WRITE-UP
			// NOW determine if the flow is stable. This is an interesting game. And we have learned
			// over the years. Our first approach went like this: compare the average flow against the
			// instantaneous flow. If they were within (like) 2% of one another declare that second to
			// be stable. We would then keep the history of this comparison and look for like 20 seconds
			// worth of stables to declare the flow stable. So if on one second out of past 20 the
			// comparison failed we would mark flow as unstable. We learned that some flow, for reasons
			// not fully understood, would oscillate up and down. Especially on the larger FMBX
			// installs. I'm sure it's due to some kind of a funky installation. But several sites
			// exhibited this. And because of it they could not use flow checking. And there was nothing
			// apparently wrong with the install and the systems were being used and declared
			// operational.
			//
			// Our approach to this was to introduce an even heavier averaging scheme. And this seemed
			// to solve the situation. And has been used broadly now for several years. It is possible
			// this approach also eliminates the need for the line-fill time as the flow average moves
			// quite slowly (the avg takes about 30 to 40 seconds to catch up an actual and stable
			// flow). But that is an experiment that remains to be seen.
			//
			// The new method uses a 20 second WEIGHTED average. The 20 second average is composed of
			// the latest twenty 5-second averages. And then that is incorpated in a 30-70 fashion into
			// the existing running stability average.
			//
			// To declare stability the difference between the current weighted average and the weighted
			// average 15 and 30 seconds ago must be less than 1% of the flow rate. See it
			// implementation below.
			
			float	avg_0, avg_14, avg_29, allowable_diff, diff_14, diff_29;

			// Grab the current weighted 20 second average.
			avg_0 = new_stability_avg;
			
			// Now course through the avgs stopping at 14 and 29. Remember this is a ring buffer. So we
			// can increment the index by one and that will be index 29. The increment it 14 more times
			// to get the index that represents about 15 seconds ago.
			UNS_32		ring_index_copy;  // work with a copy so as not to corrupt the actual index
			
			// This index presently points at the LAST average made. So if we bump by one we'll be
			// looking at the OLDEST.
			ring_index_copy = *stability_ring_index;
			
			INC_RING_BUFFER_INDEX( ring_index_copy, SYSTEM_STABILITY_AVERAGES_KEPT );
			
			avg_29 = system_preserves.system[ s ].system_stability_averages_ring[ ring_index_copy ];
			
			INT_32	i;
			for( i=0; i<14; i++ )
			{
				INC_RING_BUFFER_INDEX( ring_index_copy, SYSTEM_STABILITY_AVERAGES_KEPT );
			}
			
			avg_14 = system_preserves.system[ s ].system_stability_averages_ring[ ring_index_copy ];
			
			diff_14 = fabsf( (avg_0 - avg_14) );

			diff_29 = fabsf( (avg_0 - avg_29) );
			
			allowable_diff = ( avg_0 / ONE_HUNDRED_POINT_O ) + ONE_POINT_O;
			
			if( (diff_14 <= allowable_diff) && (diff_29 <= allowable_diff) )
			{
				system_preserves.system[ s ].sbf.stable_flow = (true);
			}
			else
			{
				system_preserves.system[ s ].sbf.stable_flow = (false);
			}
			
		}  // Of if the system is in use.
		
	}  // Of each system.
	
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern BOOL_32 FOAL_FLOW_extract_poc_update_from_token_response( UNS_8 **pucp_ptr, UNS_32 const pcontroller_index )
{
	UNS_8	number_of_pocs;

	POC_DISTRIBUTION_FROM_SLAVE_TO_MASTER	pdfs;

	UNS_32	p, this_decoders_level;
	
	POC_DECODER_OR_TERMINAL_WORKING_STRUCT	*ws;
			
	BY_POC_RECORD	*bpr_ptr;

	BOOL_32	rv;

	BOOL_32	network_available;
	
	// ----------
	
	rv = (true);

	// ----------

	network_available = COMM_MNGR_network_is_available_for_normal_use();

	if( !network_available )
	{
		// 8/20/2015 ajv : Mike found this test. At this point, we're not sure why. This alert line
		// is to try to see whether this condition ever exists. If it does, we're still going to
		// extract the message content, we're just not going to try and use it.
		Alert_Message( "POC content rcvd network not ready" );
	}
	
	// ----------

	// Get the number of pocs.
	number_of_pocs = **pucp_ptr;

	*pucp_ptr += sizeof(UNS_8);
	
	// ----------

	if( number_of_pocs > MAX_POCS_IN_NETWORK )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "FOAL: error in token_resp poc data" );

		rv = (false);  // this causes us to abandon the rest of the message
	}
	else
	{
		// 2012.01.27 rmd : We are about to unload the token response flow data into the
		// poc_preserves. So take this MUTEX. Remember TD_CHECK also looks at the preserves when
		// computing the flow rates and stability averages.
		xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );
	
		for( p=0; p<number_of_pocs; p++ )
		{
			memcpy( &pdfs, *pucp_ptr, sizeof(POC_DISTRIBUTION_FROM_SLAVE_TO_MASTER) );

			*pucp_ptr += sizeof(POC_DISTRIBUTION_FROM_SLAVE_TO_MASTER);
	
			// ----------
			
			if( network_available )
			{
				
				// 4/12/2012 rmd : The range check will in effect be performed when we try to find the poc
				// matching this data we just pulled out of the message. If we can't find one that is an
				// error. Obtain a pointer to the poc_preserves record. If none found...error. How could the
				// slave send poc data to the master about a poc the master doesn't yet know about?
				ws = POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters( pcontroller_index, pdfs.decoder_serial_number, &bpr_ptr, &this_decoders_level );
				
				if( ws == NULL )
				{
					Alert_poc_not_found_extracting_token_resp( pdfs.decoder_serial_number, pcontroller_index );
			
					rv = (false);  // this causes us to abandon the rest of the message
				
					break;  // from the for loop .. we're DONE!
				}

				// 10/23/2014 ajv : If we decide NOT to break out of the loop if the POC is not found, make
				// sure that the following lines are put inside an else-statement to ensure we don't try to
				// access a NULL pointer.

				// ----------
				
				// 6/5/2014 rmd : By this point, cause we check when the data arrives from the tpmicro over
				// on the irri side, if the accumulated pulses is zero the accumulated ms is zero. And this
				// is needed due to the 'accumulated' nature of time adding even though no pulses. And then
				// flow could start for a portion of the 'delivery slot' and then the accumulated ms would
				// be in error.
				ws->fm_accumulated_pulses_foal += pdfs.fm_accumulated_pulses;

				ws->fm_accumulated_ms_foal += pdfs.fm_accumulated_ms;
				
				// 6/5/2014 rmd : Always overwrite the latest 5 second count. It is always accurate.
				ws->fm_latest_5_second_pulse_count_foal = pdfs.fm_latest_5_second_count;

				// 1/16/2015 rmd : And also update with the latest pulse rate for Bermad.
				ws->fm_delta_between_last_two_fm_pulses_4khz_foal = pdfs.fm_delta_between_last_two_fm_pulses_4khz;

				ws->fm_seconds_since_last_pulse_foal = pdfs.fm_seconds_since_last_pulse;
				
				// -------

				// 2/12/2015 rmd : Debug alert message only for the B controller. Used to see how poc flow
				// on a chain was working. Was working fine. Line commented out.
				/*
				if( pcontroller_index == 1 )
				{
					Alert_Message_va( "TOKEN RESP from %c - count is %u", 'A' + pcontroller_index, ws->fm_accumulated_pulses_foal );
				}
				*/

				// ----------
				
				ws->mv_current_for_distribution_in_the_token_ma = pdfs.last_measured_mv_ma;

				ws->pump_current_for_distribution_in_the_token_ma = pdfs.last_measured_pump_ma;

			}	// of network is available
			
		}  // for the number of poc's to do	
			
	}  // of number_of_pocs is within range

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This is the distribution of system information. For each system we will
    send some information. Mostly flow related but will add to this as necessary. This
    function will allocate and fill the needed memory. Setting the pointer included as a
    argument to the start of the block. And will return the length of the block. The length
    reflects the size of the block to be placed within the token. The length should be
    returned as 0 if the block is not to be sent. The memory block will be dynamically
    allocated and must be freed after the caller takes a copy of the data and implants it
    into the message.
	
	@CALLER MUTEX REQUIREMENTS (none) 

    @EXECUTED Executed within the context of the COMM_MNGR task.
	
    @RETURN Set the pdata_ptr to the start of the block if there is data to ship, NULL other
    wise. The return value is the length of the block to ship. And should return 0 if there
    is no data to include within the token.

	@AUTHOR 2012.03.06 rmd
*/
extern UNS_32 FOAL_FLOW_load_system_info_for_distribution_to_slaves( UNS_8 **pdata_ptr )
{
/*
	Depiction of structure we are building.
 
	// 6/26/2015 mpd : FogBugz #3113. Added "number_of_valves_ON" to the structure.
    
	-------------------------
	| # of Systems's (uns_8)
	|
	| |----------------------
    | |
	| |System GID (uns_16)
    | |
	| |system_bit_field_struct (sizeof(SYSTEM_BIT_FIELD_STRUCT)
	| |
	| |number_of_valves_ON (uns_16)
	| |
	| |system_expected_flow_rate_for_those_ON (uns_16)
	| |
	| |system_master_most_recent_5_second_average (float)
    | |
	| |----------------------
    |
	-------------------------
*/

	UNS_32	rv;
	
	rv = 0;
	
	*pdata_ptr = NULL;
	
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	UNS_32	i, number_of_systems;
	
	number_of_systems = 0;

	// FIRST figure out how many bytes we are dealing with.
	for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
	{
		if( system_preserves.system[ i ].system_gid != 0 )
		{
			number_of_systems += 1;
		}
	}

	if( number_of_systems != 0 )
	{
		// So there is at least one system to include. So build up the transfer.
		
		UNS_32	llength;
		UNS_8	*ucp;
		
		// 6/26/2015 mpd : FogBugz #3113. Added "number_of_valves_ON" to the structure. This added a UNS_16. 
		#define		BYTES_SENT_PER_SYSTEM	(sizeof(UNS_16) + sizeof(SYSTEM_BIT_FIELD_STRUCT) + sizeof(UNS_16) + sizeof(UNS_16) + sizeof(float) + sizeof(BOOL_32) + sizeof(BOOL_32) + sizeof(UNS_32))

		// Include the byte that says how many systems are included.
		llength = ( 1 + ( number_of_systems * ( BYTES_SENT_PER_SYSTEM ) ) );
		
		ucp = mem_malloc( llength );
		
		*pdata_ptr = ucp;  // set the pointer for the caller
		
		rv = llength;  // and set the return value now cause we know the length
		
		// Put in the number of systems.
		memcpy( ucp, &number_of_systems, 1 );
		ucp += 1;
		
		// Use llength as a sanity test counter.
		llength -= 1;
		
		for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
		{
			if( system_preserves.system[ i ].system_gid != 0 )
			{
				// System GID as an UNS_16.
				memcpy( ucp, &(system_preserves.system[ i ].system_gid), sizeof(UNS_16) );
				ucp += sizeof(UNS_16);
				
				// The bit field
				memcpy( ucp, &(system_preserves.system[ i ].sbf), sizeof(SYSTEM_BIT_FIELD_STRUCT) );
				ucp += sizeof(SYSTEM_BIT_FIELD_STRUCT);
				
				// 6/26/2015 mpd : FogBugz #3113. Added "number_of_valves_ON" to the structure. This FOAL side value
				// needs to be visable on the IRRI side. Like the next UNS_16, two bytes will cover the full range of
				// valves ON.
				memcpy( ucp, &(system_preserves.system[ i ].system_master_number_of_valves_ON), sizeof(UNS_16) );
				ucp += sizeof(UNS_16);

				// The expected flow rate as an UNS_16. Yes even though this is an UNS_32 we can ship only
				// the first two bytes of this variable and obtain the full range (only works for little
				// endian!).
				memcpy( ucp, &(system_preserves.system[ i ].ufim_expected_flow_rate_for_those_ON), sizeof(UNS_16) );
				ucp += sizeof(UNS_16);

				// And the lastest system average as a float.
				memcpy( ucp, &(system_preserves.system[ i ].system_master_most_recent_5_second_average), sizeof(float) );
				ucp += sizeof(float);

				// 9/24/2015 ajv : And don't forget the MVOR in effect flags. However, since the flags are
				// in a bitfield, copy them to a local variable first.
				BOOL_32 lmvor_in_effect;
				lmvor_in_effect = system_preserves.system[ i ].sbf.MVOR_in_effect_opened;
				memcpy( ucp, &lmvor_in_effect, sizeof(BOOL_32) );
				ucp += sizeof(BOOL_32);

				lmvor_in_effect = system_preserves.system[ i ].sbf.MVOR_in_effect_closed;
				memcpy( ucp, &lmvor_in_effect, sizeof(BOOL_32) );
				ucp += sizeof(BOOL_32);

				memcpy( ucp, &system_preserves.system[ i ].MVOR_remaining_seconds, sizeof(UNS_32) );
				ucp += sizeof(UNS_32);

				// And update our sanity test counters.
				number_of_systems -= 1;
				llength -= BYTES_SENT_PER_SYSTEM;
			}
		}

		if( number_of_systems || llength )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "SYSTEM: token info error" );
		}

	}  // of if the number_of_systems to send is != 0

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern UNS_32 FOAL_FLOW_load_poc_info_for_distribution_to_slaves( UNS_8 **pdata_ptr )
{
	// 6/5/2014 rmd : In EACH token for EACH poc in the network we send the flow rate and last
	// known pump and mv current.
	
	// ----------
	
	UNS_32	rv;
	
	UNS_8	*ucp;
	
	UNS_8	num_of_pocs;

	UNS_8	*num_of_pocs_in_msg_ptr;

	UNS_32	i, l;
	
	POC_DISTRIBUTION_FROM_MASTER	pdfm;
	
	// ----------
	
	rv = 0;
	
	// 6/4/2014 rmd : Get a block that for sure can hold the entire poc content. Added 16 to the
	// front to cover the num_of_pocs byte plus a bunch of margin.
	ucp = mem_malloc( (16 + (MAX_POCS_IN_NETWORK * sizeof(POC_DISTRIBUTION_FROM_MASTER)) ) );

	*pdata_ptr = ucp;  // set the pointer for the caller

	// ----------
	
	// 6/4/2014 rmd : Initialize and make pointer to.
	num_of_pocs = 0;

	num_of_pocs_in_msg_ptr = ucp;
	
	ucp += 1;
	
	rv += 1;
	
	// ----------
	
	// 2012.02.02 rmd : Since we are perusing through the poc_preserves best take this MUTEX.
	xSemaphoreTakeRecursive( poc_preserves_recursive_MUTEX, portMAX_DELAY );

	for( i=0; i<MAX_POCS_IN_NETWORK; i++ )
	{
		// 4/12/2012 rmd : If the record is in use.
		if( poc_preserves.poc[ i ].poc_gid != 0 )
		{
			// 8/10/2012 rmd : We'll be sending this one.
			num_of_pocs += 1;
			
			// ----------
			
			memset( &pdfm, 0x00, sizeof(POC_DISTRIBUTION_FROM_MASTER) );

			pdfm.box_index = poc_preserves.poc[ i ].box_index;

			pdfm.poc_type__file_type = poc_preserves.poc[ i ].poc_type__file_type;
			
			// 8/8/2016 rmd : And only need to include the level 0 decoder serial number as part of the
			// search criteria.
			pdfm.decoder_serial_number = poc_preserves.poc[ i ].ws[ 0 ].decoder_serial_number;

			// ----------
			
			for( l=0; l<POC_BYPASS_LEVELS_MAX; l++ )
			{
				pdfm.latest_5_second_average_gpm_foal[ l ] = poc_preserves.poc[ i ].ws[ l ].latest_5_second_average_gpm_foal;

				pdfm.mv_current_for_distribution_ma[ l ] = poc_preserves.poc[ i ].ws[ l ].mv_current_for_distribution_in_the_token_ma;

				pdfm.pump_current_for_distribution_ma[ l ] = poc_preserves.poc[ i ].ws[ l ].pump_current_for_distribution_in_the_token_ma;
			}
			
			// ----------
			
			memcpy( ucp, &pdfm, sizeof(POC_DISTRIBUTION_FROM_MASTER) );

			ucp += sizeof(POC_DISTRIBUTION_FROM_MASTER);

			rv += sizeof(POC_DISTRIBUTION_FROM_MASTER);
		}
		
	}

	xSemaphoreGiveRecursive( poc_preserves_recursive_MUTEX );

	// ----------
	
	if( num_of_pocs > 0 )
	{
		// 8/10/2012 rmd : Place the poc count into the msg.
		*num_of_pocs_in_msg_ptr = num_of_pocs;
	}
	else
	{
		// 6/4/2014 rmd : Indicate to the caller nothing to send.
		rv = 0;
		
		mem_free( *pdata_ptr );

		*pdata_ptr = NULL;
	}

	// ---------------

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Makes the decision to test the flow rate against the MLB trip point or not.
    Notice we do not test for flow stability. And there is no user ability to not check for
    MLB. If you have a flow meter ... we are checking.
	
    @CALLER_MUTEX_REQUIREMENTS Caller is to be holding the system_preserves MUTEX. We are
    using values within the system_preserves.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (true) if we are to proceed with the test, (false) otherwise.

	@ORIGINAL 2012.05.04 rmd

	@REVISIONS (none)
*/
static BOOL_32 _nm_okay_to_test_for_MLB_with_valves_ON( BY_SYSTEM_RECORD *pbsr_ptr )
{
	BOOL_32	rv;
	
	rv = (true);
	
	if( pbsr_ptr->system_master_number_of_valves_ON == 1 )
	{
		IRRIGATION_LIST_COMPONENT *lilc_ptr;
		
		// 5/4/2012 rmd : Only one in the list (supposedly) ... just grab the head.
		lilc_ptr = foal_irri.list_of_foal_stations_ON.phead;

		if( lilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_TEST )
		{
			// 5/4/2012 rmd : If there is one ON and for test we ignore the MVJO delay to allow MLB and
			// flow checking to commence after the line fill time has elapsed. Without having to wait
			// the 150 seconds for the MVJO timer to elapse. As an aid for the user when testing.
		}
		else
		{
			if( pbsr_ptr->timer_MVJO_flow_checking_blockout_seconds_remaining > 0 )
			{
				rv = (false);	
			}
		}
	}
	else
	{
		if( pbsr_ptr->timer_MVJO_flow_checking_blockout_seconds_remaining > 0 )
		{
			rv = (false);	
		}
	}

	// 8/1/2016 rmd : If there are NO flow meters on the mainline do not check for MLB.
	if( pbsr_ptr->sbf.number_of_flow_meters_in_this_sys == 0 )
	{
		rv = (false);
	}
	

	// 2/20/2013 rmd : The flow checking block out reflects both the line fill and slow closing
	// valve delays.
	if( pbsr_ptr->flow_checking_block_out_remaining_seconds > 0 )
	{
		rv = (false);
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Makes the decision to test the flow rate against the MLB trip point or not.
    Notice we do not test for flow stability. And there is no user ability to not check for
    MLB. If you have a flow meter ... we are checking.
	
    @CALLER_MUTEX_REQUIREMENTS Caller is to be holding the system_preserves MUTEX. We are
    using values within the system_preserves.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (true) if we are to proceed with the test, (false) otherwise.

	@ORIGINAL 2012.05.04 rmd

	@REVISIONS (none)
*/
static BOOL_32 _nm_okay_to_test_for_MLB_with_all_valves_OFF( BY_SYSTEM_RECORD *pbsr_ptr )
{
	BOOL_32	rv;
	
	rv = (true);
	
	// 8/1/2016 rmd : If there are NO flow meters on the mainline do not check for MLB.
	if( pbsr_ptr->sbf.number_of_flow_meters_in_this_sys == 0 )
	{
		rv = (false);
	}

	// 2/20/2013 rmd : This just stopped irrigating timer is continously re-loaded during
	// irrigation. Therefore as soon as there are 0 valves ON it is valid. Even during slow
	// closing valve MV and PMP hang-over it continues to re-load.
	if( pbsr_ptr->timer_MLB_just_stopped_irrigating_blockout_seconds_remaining > 0 )
	{
		rv = (false);	
	}

	// 7/2/2014 rmd : We are including the MVJO timer here first off as the means for blocking
	// MLB checking upon program startup. It is loaded with the 150 second count on startup.
	// Additionally it is somewhat logical that if the MVJO timer is still counting down from a
	// legit MV opening not to check for MLB.
	if( pbsr_ptr->timer_MVJO_flow_checking_blockout_seconds_remaining > 0 )
	{
		rv = (false);
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION A MLB has been detected during irrigation for the system_gid argument. We
    set the flag in the system_preserve so that when the outgoing TOKENS will contain an
    indication. We go about the process of turn OFF and removing all stations in the list
    for this system. And we make an alert line.
	
    @CALLER MUTEX REQUIREMENTS Caller to be holding the system_preserves MUTEX as we are
    going to edit the record.

    @EXECUTED Executed within the context of the TD_CHECK task as part of the MLB check.

	@RETURN (none)

	@AUTHOR 2012.05.07 rmd
*/
static void _nm_take_action_for_a_detected_MLB( BY_SYSTEM_RECORD* psystem_ptr, const UNS_32 pmeasured, const UNS_32 plimit, UNS_32 pmlb_during_what )
{
	// 5/8/2012 rmd : Make the alert line. Notice we test with the average but report the
	// actualflowrate for the alert line and status screen...this is because the avg moves
	// slowly and when it trips it is usually just over the trip point when in fact the actual
	// flow could be way over the trip point ... testing with the avg demands that the actual
	// flow is big for a long enough period of time to be legit...but we dont want to see the
	// avg as what caused the trip in the alert line! At least I don't.
	if( pmlb_during_what == MLB_TYPE_DURING_IRRIGATION )
	{
		// 5/8/2012 rmd : Flag the event in the system_preserves record.
		psystem_ptr->latest_mlb_record.there_was_a_MLB_during_irrigation = (true);

		psystem_ptr->latest_mlb_record.mlb_measured_during_irrigation_gpm = pmeasured;

		psystem_ptr->latest_mlb_record.mlb_limit_during_irrigation_gpm = plimit;
	}
	else if( pmlb_during_what == MLB_TYPE_DURING_MVOR )
	{
		// 5/8/2012 rmd : Flag the event in the system_preserves record.
		psystem_ptr->latest_mlb_record.there_was_a_MLB_during_mvor_closed = (true);

		psystem_ptr->latest_mlb_record.mlb_measured_during_mvor_closed_gpm = pmeasured;

		psystem_ptr->latest_mlb_record.mlb_limit_during_mvor_closed_gpm = plimit;
	}
	else if( pmlb_during_what == MLB_TYPE_DURING_ALL_OTHER_TIMES )
	{
		// 5/8/2012 rmd : Flag the event in the system_preserves record.
		psystem_ptr->latest_mlb_record.there_was_a_MLB_during_all_other_times = (true);

		psystem_ptr->latest_mlb_record.mlb_measured_during_all_other_times_gpm = pmeasured;

		psystem_ptr->latest_mlb_record.mlb_limit_during_all_other_times_gpm = plimit;
	}
	else
	{
		Alert_Message( "MLB : during what UNK." );
	}
	
	// ----------
	
	// 4/29/2015 rmd : Make the alert. Taking the system MUTEX since we call the
	// nm_GROUP_get_name function.
	xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

	Alert_mainline_break( "", nm_GROUP_get_name( SYSTEM_get_group_with_this_GID(psystem_ptr->system_gid) ), pmlb_during_what, pmeasured, plimit );

	xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

	// ----------
	
	// 5/8/2012 rmd : Now remove ALL stations belonging to this system GID from the main list.
	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists( psystem_ptr, 0, REMOVE_FOR_ALL_REASONS, ACTION_REASON_TURN_OFF_AND_REMOVE_mlb );

	// 9/30/2015 rmd : And cancel any MVOR that may be going on. The reasoning for this is first
	// of all MLB should indeed have full control of the MV's when there is a MLB (it does even
	// if there is a MLB but ...). Secondly let's say the MVOR opened is in effect and we didn't
	// cancel it. When the user clears the MLB if the MVOR opened is still in effect the MV's
	// will open. And that might be unexpected. Overall it seems safer to cancel the MVOR in the
	// face of a MLB.
	FOAL_IRRI_initiate_or_cancel_a_master_valve_override( psystem_ptr->system_gid, MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE, 0, INITIATED_VIA_MLB );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called once per second via TD_CHECK. If allowed, checks the system flow
    rate against the criteria for a MLB. Repeats the process for each registered system.
	
	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (none)

	@ORIGINAL 2012.05.04 rmd

	@REVISIONS (none)
*/
extern void FOAL_FLOW_check_for_MLB( void )
{
	INT_32	s;
	
	UNS_32	lmlb_limit, lpresent_long_avg_flow_rate, lpresent_short_avg_flow_rate;

	// 5/3/2012 rmd : We reference and set several things within the system preserves. Make sure
	// they are stable.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// 5/3/2012 rmd : Check for MLB one system at a time.
	for( s=0; s<MAX_POSSIBLE_SYSTEMS; s++ )
	{
		// 5/3/2012 rmd : Only if this preserves record is in use (registered).
		if( system_preserves.system[ s ].system_gid != 0 )
		{
			// 5/9/2012 rmd : We always test against the heavily averaged stability average. Only need a
			// single test against the latest average.
			lpresent_long_avg_flow_rate = system_preserves.system[ s ].system_stability_averages_ring[ system_preserves.system[ s ].stability_avgs_index_of_last_computed ];

			lpresent_short_avg_flow_rate = system_preserves.system[ s ].system_master_most_recent_5_second_average;
			
			// 5/8/2012 rmd : It used to be in the past we saw occasion when a simple short termed
			// average flow would trip the MLB. And if the alert we made then used the actual flow rate
			// we had a discrepancy in the the actual flow rate could be less than the MLB number. This
			// indicated that we had perhaps a good sized spike in the flow that tripped the MLB. And
			// may not have been a MLB at all. Subsequently we began requiring that both the averaged
			// and the immediate flow rates surpassed the MLB trip point. And that seemed to work well.
			// Fast forward a bit. Still in the 2000e we experienced flow meters, especially saddles,
			// that generated wildly varying flows. That people tried to fix. But could not. To work
			// with them we introduced a very heavily averaged flow rate. To use during rate tests. And
			// that worked. That avg is in place in the CS3000 today. And we will use it as our single
			// test against MLB.
			//
			// 7/2/2014 rmd : UPDATE - I've decided to mimic the 2000e approach. Both the short average
			// and the long average must exceed the limit to trip a MLB. See the notes that follow from
			// the 2000e:
			// ...from the 2000e
			// now this is interesting...apparently we have seen the case where the avg is greater
			// then the MLB number however the instantaneous is less than the MLB number...we trip a MLB
			// and then report the actual(instantaneous) flow rate in the alert line which is less
			// and we wonder why did it trip...what this signals to me is that perhaps our average for
			// MLB isn't long enough...but maybe another to go at this is to require both the AVG and
			// the actual to be above the trip number...that way a sustained high flow rate is required
			// not just a large spike which is what we seem to be tripping on
			//
			// notice we test with the average but report the actualflowrate for the alert
			// line and status screen...this is because the avg moves slowly and when it trips
			// it is usually just over the trip point when in fact the actual flow could be way
			// over the trip point...testing with the avg demands that the actual flow is big
			// for a long enough period of time to be legit...but we dont want to see the avg
			// as the actual flow
			// ...end from the 2000e
			// ----------

			if( system_preserves.system[ s ].system_master_number_of_valves_ON > 0 )
			{
				if( _nm_okay_to_test_for_MLB_with_valves_ON( &(system_preserves.system[ s ]) ) == (true) )
				{
					// 5/9/2012 rmd : Get the during irrigation limit.
					lmlb_limit = SYSTEM_get_mlb_during_irri_gpm( system_preserves.system[ s ].system_gid );
					
					if( (lpresent_long_avg_flow_rate > lmlb_limit) && (lpresent_short_avg_flow_rate > lmlb_limit) )
					{
						// 5/9/2012 rmd : If the bit is not already set.
						if( system_preserves.system[ s ].latest_mlb_record.there_was_a_MLB_during_irrigation == (false) )
						{
							// 5/8/2012 rmd : Take the resultant actions needed for the detected MLB. Use the most
							// recent short average as the reported trip rate. I think this shows better what flow may
							// have ramped up to. As the heavily averaged value lags significantly.
							_nm_take_action_for_a_detected_MLB( &(system_preserves.system[ s ]), lpresent_short_avg_flow_rate, lmlb_limit, MLB_TYPE_DURING_IRRIGATION );
						}
					}

				}  // valves are ON and it is okay to test for MLB

			}  // end of if valves are ON
			else
			if( _nm_okay_to_test_for_MLB_with_all_valves_OFF( &(system_preserves.system[ s ]) ) == (true) )
			{
				// 10/18/2016 rmd : To test if a MVOR is in effect, ONLY look at the MVOR_in_effect_opened
				// variable. DO NOT reference the MVOR_remaining_seconds variable. It is not needed and
				// actually can go to 0 BEFORE a MVOR has officially been terminated. Officially terminating
				// the MVOR loads up the MVJO MLB checking block out counter for 150 seconds. Without that
				// we could (and HAVE) tripped a false MLB when the MVOR_remaining_seconds went to 0 yet the
				// MVOR had not yet been offically terminated. This happens because the tesat to terminate
				// is buried inside the foal_irrigation_maintain function. It probably should not be - but
				// I'm not moving it now. Only testing the MVOR_in_effect_opened variable fixes our problem.
				if( system_preserves.system[ s ].sbf.MVOR_in_effect_opened == (true) )
				{
					// 5/9/2012 rmd : This is the MLB test against the MVOR opened number.
					lmlb_limit = SYSTEM_get_mlb_during_mvor_opened_gpm( system_preserves.system[ s ].system_gid );
					
					if( (lpresent_long_avg_flow_rate > lmlb_limit) && (lpresent_short_avg_flow_rate > lmlb_limit) )
					{
						// 5/9/2012 rmd : If the bit is not already set.
						if( system_preserves.system[ s ].latest_mlb_record.there_was_a_MLB_during_mvor_closed == (false) )
						{
							// 5/8/2012 rmd : Take the resultant actions needed for the detected MLB. Use the most
							// recent short average as the reported trip rate. I think this shows better what flow may
							// have ramped up to. As the heavily averaged value lags significantly.
							_nm_take_action_for_a_detected_MLB( &(system_preserves.system[ s ]), lpresent_short_avg_flow_rate, lmlb_limit, MLB_TYPE_DURING_MVOR );
						}
					}

				}
				else
				{
					// 5/9/2012 rmd : This is the MLB test against the ALL OTHER TIMES number.
					lmlb_limit = SYSTEM_get_mlb_during_all_other_times_gpm( system_preserves.system[ s ].system_gid );
					
					if( (lpresent_long_avg_flow_rate > lmlb_limit) && (lpresent_short_avg_flow_rate > lmlb_limit) )
					{
						// 5/9/2012 rmd : If the bit is not already set.
						if( system_preserves.system[ s ].latest_mlb_record.there_was_a_MLB_during_all_other_times == (false) )
						{
							// 5/8/2012 rmd : Take the resultant actions needed for the detected MLB. Use the most
							// recent short average as the reported trip rate. I think this shows better what flow may
							// have ramped up to. As the heavily averaged value lags significantly.
							_nm_take_action_for_a_detected_MLB( &(system_preserves.system[ s ]), lpresent_short_avg_flow_rate, lmlb_limit, MLB_TYPE_DURING_ALL_OTHER_TIMES );
						}
					}

				}
				
			}  // of all valves are OFF

		}  // of if this system is in use

	}  // of each system

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* --------------------------------------- */
/*
unsigned short FOAL_WhatsTheFlowRateForThisBadStation( IRRIGATION_LIST_COMPONENT *pilc, unsigned short pactual_system, float pfactor ) {

IRRIGATION_LIST_COMPONENT *ilc;

// Make this an int to deal with low flow rates that cause us to drive this negative while doing the subtractions. If we have
// a low flow rate we could subtract off much more than the actual flow rate.
//
int bad_stations_share;

	

	bad_stations_share = pactual_system;
	

	// Go through the list of those ON skipping pilc. For each one on except for pilc subtract
	// from the actaul system the expected. Whats left is the amount allocated to pilc.
	//
	// Factor represents the resulting percentage of the expected flow to use after it is derated.
	//
	ilc = ListGetFirst( &foal_irri.ONList );
	
	while ( ilc != NULL ) {
	
		if ( ilc != pilc ) {
		
			bad_stations_share -= pfactor * ilc->expected_flow;
			
		}

	
		// pull next guy on
		//
		ilc = ListGetNext( &foal_irri.ONList, ilc );
		
	}
	
	
	// For the case of no flows or LOW flow rates we probably drove this value to < 0
	//
	if ( bad_stations_share < 0 ) {
	
		bad_stations_share = 0;
	
	}


	// return what's left
	//
	return( bad_stations_share );
		
}
*/	

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The hi or lo flow flow check has picked up an error. The flags bit_field
    argument contains decriptive flags. Sift through the ON list turning OFF the valves ON
    in this system, flagging them as being involved in a flow problem, and if one of them
    was already involved it it either removed from the list or marked as no more flow
    checking. Based upon the flow group it is in.

    @CALLER_MUTEX_REQUIREMENTS The caller is expected to be holding the system_preserves
    MUTEX and the foal_irri MUTEX.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. At a 1 Hz rate as part of
    flow checking.
	
	@RETURN (none)

	@ORIGINAL 2012.06.11 rmd

	@REVISIONS (none)
*/
static void _nm_nm_for_a_flow_error_make_flow_recording_lines_and_turn_OFF_all_ON( FLOW_RECORDER_FLAG_BIT_FIELD pfrbf, BY_SYSTEM_RECORD *pbsr_ptr, UNS_32 psystem_actual_flow )
{
	// -----------------------
	
	// 6/20/2012 rmd : So as we 'unwind' the list of valves the system values for the number of
	// valves ON and the expected flow rate for those ON will track. That is as we turn OFF the
	// valves these values will decline. And when we make the flow recording lines and make
	// decisions about first or second high flow based on number ON we want to use the intial
	// values upon entry to this function.

	UNS_32	captured_expected_flow_rate_for_those_ON;
	
	UNS_32	captured_number_of_valves_ON;
	
	captured_expected_flow_rate_for_those_ON = (pbsr_ptr->ufim_expected_flow_rate_for_those_ON);
	
	captured_number_of_valves_ON = (pbsr_ptr->system_master_number_of_valves_ON);
	
	// -----------------------
	
	// 6/27/2012 rmd : We're going to loop through turning OFF. Only make a flow recording line
	// for the first group (if not already made). This is technically not needed here, as we
	// manually set the flag indicating lines have been made before each call to the turn OFF
	// function.
	foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (false);

	IRRIGATION_LIST_COMPONENT *lilc;
	
	lilc = nm_ListGetFirst( &(foal_irri.list_of_foal_stations_ON) );

	while( lilc != NULL )
	{
		// 6/11/2012 rmd : Needed when we turn OFF valves.
		void*	next_ilc;

		if( lilc->bsr == pbsr_ptr )
		{
			// ---------------------------
			
			// 6/11/2012 rmd : Flag we've been checking flow.
			lilc->bbf.at_some_point_flow_was_checked = (true);

			// ---------------------------
			
			// 6/11/2012 rmd : If this station was the ONLY one ON or this station was already involved
			// in a flow error then this flow check error behaves the same as if this station was
			// already on the problem list.
			BOOL_32	remove_from_list;
			
			// 6/12/2012 rmd : Even if we have not yet stamped the report data (for programmed
			// irrigation only) we don't want to do so unless this is the SECOND and FINAL error.
			// Otherwise the report data would be stamped for all the station ON. And the ones that
			// actually have NORMAL flow would get distorted data for their report flow. Cause each
			// station would contain a share of the bad one. This could only happen on the first cycle
			// when the report data has not yet been stamped. But it could and will happen of course.
			BOOL_32	stamp_the_report_data;
			
			// 6/12/2012 rmd : Only make an alert line on the final alert. Similar to stamping the
			// report data.
			BOOL_32	make_an_alert_line;

			UNS_32	laid, system_flow_check_limit_for_the_alert_line;

			// 6/12/2012 rmd : These will be over-written if the ilc falls into the group of already
			// involved in a problem.
			if( pfrbf.flow_check_status == FRF_CHECK_STATUS_hi_flow )
			{
				// 6/12/2012 rmd : So it was the high flow test that failed.
				system_flow_check_limit_for_the_alert_line = pbsr_ptr->flow_check_hi_limit;
				
				laid = AID_FIRST_HIGHFLOW;
			}
			else
			{
				// 6/12/2012 rmd : So it was the low flow test that failed.
				system_flow_check_limit_for_the_alert_line = pbsr_ptr->flow_check_lo_limit;
				
				laid = AID_FIRST_LOWFLOW;
			}

			remove_from_list = (false);
			
			// 6/26/2012 rmd : We shall only stamp the report data if a station has a confirmed flow
			// error (second time). And only for that single station. Otherwise the report data is not
			// stamped. We don't want errant flow data in the report record.
			stamp_the_report_data = (false);
			
			make_an_alert_line = (false);
			
			// 6/11/2012 rmd : So if we failed the high/low flow test and we are set to shut-off then we
			// should be removed from the list.
			if( (captured_number_of_valves_ON == 1) || (lilc->bbf.w_involved_in_a_flow_problem == (true)) )
			{
				// ---------------------------
		
				// 6/12/2012 rmd : Its a final error one way or another.
				stamp_the_report_data = (true);

				make_an_alert_line = (true);
					
				// ---------------------------
		
				if( pfrbf.flow_check_status == FRF_CHECK_STATUS_hi_flow )
				{
					laid = AID_FINAL_HIGHFLOW;

					if( lilc->bbf.flow_check_hi_action == ALERT_ACTIONS_ALERT_SHUTOFF )
					{
						remove_from_list = (true);
					}
					else
					if( lilc->bbf.flow_check_hi_action == ALERT_ACTIONS_ALERT_NO_ACTION )
					{
						// 6/12/2012 rmd : This flag causes us to change his flow check group to 0 at the end of
						// this function. Which is the easiest way to transition the station to a no flow check
						// station for the balance of his irrigation.
						lilc->bbf.flow_check_to_be_excluded_from_future_checking = (true);
					}
				}
				else
				{
					laid = AID_FINAL_LOWFLOW;

					if( lilc->bbf.flow_check_lo_action == ALERT_ACTIONS_ALERT_SHUTOFF )
					{
						remove_from_list = (true);
					}
					else
					if( lilc->bbf.flow_check_lo_action == ALERT_ACTIONS_ALERT_NO_ACTION )
					{
						lilc->bbf.flow_check_to_be_excluded_from_future_checking = (true);
					}
				}
			}

			// ---------------------------
		
			float	the_stations_share;
			
			UNS_32	station_share_of_derated_expected, station_share_of_actual_flow, station_share_of_hi_limit, station_share_of_lo_limit;
		
			// 6/12/2012 rmd : Protect against a divide by 0.
			if( captured_expected_flow_rate_for_those_ON == 0 )
			{
				the_stations_share = 0.0;
			}
			else
			{
				the_stations_share = ( ((float)lilc->expected_flow_rate_gpm_u16) / ((float)captured_expected_flow_rate_for_those_ON) );
			}
			
			station_share_of_derated_expected = __round_UNS16( (((float)pbsr_ptr->flow_check_derated_expected) * the_stations_share) );  // this value is now a derated expected
			
			station_share_of_actual_flow  = __round_UNS16( (((float)psystem_actual_flow) * the_stations_share) );  // this is a proportional piece of the actual based on the ratio of the expecteds
			
			station_share_of_hi_limit = __round_UNS16( (((float)pbsr_ptr->flow_check_hi_limit) * the_stations_share) );  // proportional piece of the limits
	
			station_share_of_lo_limit = __round_UNS16( (((float)pbsr_ptr->flow_check_lo_limit) * the_stations_share) );

			// ---------------------------
		
			if( lilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
			{
				// 6/12/2012 rmd : Whether or not if the report data has been stamped we will stamp it if
				// this is a confirmed flow error (second error). We want this data in the report rip.
				if( stamp_the_report_data == (true) )
				{
					if( pfrbf.flow_check_status == FRF_CHECK_STATUS_hi_flow )
					{
						station_preserves.sps[ lilc->station_preserves_index ].station_history_rip.pi_flag.flow_high = (true);
					}
					else
					{
						station_preserves.sps[ lilc->station_preserves_index ].station_history_rip.pi_flag.flow_low = (true);
					}

					// 6/5/2012 rmd : Stamp the share of actual, and the share of hi and lo limits.
					station_preserves.sps[ lilc->station_preserves_index ].station_history_rip.pi_flow_check_share_of_actual_gpm = station_share_of_actual_flow;
	
					station_preserves.sps[ lilc->station_preserves_index ].station_history_rip.pi_flow_check_share_of_hi_limit_gpm = station_share_of_hi_limit;
	
					station_preserves.sps[ lilc->station_preserves_index ].station_history_rip.pi_flow_check_share_of_lo_limit_gpm = station_share_of_lo_limit;
	
					// 6/12/2012 rmd : Do indicate we have stamped.
					station_preserves.sps[ lilc->station_preserves_index ].station_history_rip.pi_flag.pi_flow_data_has_been_stamped = (true);
				}
			}

			// ---------------------------
		
			nm_flow_recorder_add(	lilc, 

									station_share_of_derated_expected,		// station derated expected

									station_share_of_hi_limit,				// station limit
									station_share_of_lo_limit,				// station limit

									station_share_of_actual_flow,			// proportional amount of actual flow rate

									pfrbf );

			// ---------------------------
		
			// 6/12/2012 rmd : TODO for now make alert lines for both the first and second flow errors.
			// We'll want to DELETE this line soon.
			make_an_alert_line = (true);  // TODO ... delete this line all-together
			
			if( make_an_alert_line == (true) )
			{
				Alert_flow_error_idx( laid,												// alert ID		
									  lilc->bbf.w_reason_in_list,						// reason in list
									  lilc->box_index_0,								// box_index
									  lilc->station_number_0_u8,						// station number
									  
									  captured_expected_flow_rate_for_those_ON,			// system expected
									  pbsr_ptr->flow_check_derated_expected,   			// system derated expected
									  system_flow_check_limit_for_the_alert_line,		// flow check limit
									  psystem_actual_flow,								// measured flow rate
									  
									  station_share_of_derated_expected,				// station derated expected
									  station_share_of_actual_flow,						// station share of actual flow
									  captured_number_of_valves_ON						// number of stations ON (?)
								);
			}

			// ---------------------------
		
			// 6/11/2012 rmd : We've already made ours so prevent the turn OFF process from generating
			// another flow recording line.
			pbsr_ptr->sbf.checked_or_updated_and_made_flow_recording_lines = (true);
			
			// ------------------------

			UNS_32	action_reason;

			if( remove_from_list == (true) )
			{
				// 6/27/2012 rmd : Being here means was a second and final flow error for this
				// station. So we want to cause the flow error indication in the station preserves to be
				// set.
				if( pfrbf.flow_check_status == FRF_CHECK_STATUS_hi_flow )
				{
					action_reason = ACTION_REASON_TURN_OFF_AND_REMOVE_hi_flow_fault;
				}
				else
				{
					action_reason = ACTION_REASON_TURN_OFF_AND_REMOVE_lo_flow_fault;
				}

				// 11/5/2014 rmd : Turn OFF and add to the action_needed list. And remove from the foal irri
				// list.
				next_ilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_stations_ON), lilc, action_reason );
			}
			else
			{
				// 6/27/2012 rmd : We want to change the flow group to 0 if excluded from future flow
				// checking. However if we do this before we turn OFF the valve we will trigger an error.
				// There is a count of the valves ON by group. And the counts will become skewed. So use
				// this flag to know to change the group after we turn OFF the valve.
				BOOL_32	change_flow_group;
				
				change_flow_group = (false);
				
				// 6/12/2012 rmd : If we have marked the station to be excluded from future tests then this
				// ilc was already involved in a flow problem when it failed again. TWO cycles in a row.
				if( lilc->bbf.flow_check_to_be_excluded_from_future_checking == (true) )
				{
					// 6/12/2012 rmd : If the station is marked then change the flow group to group 0.
					change_flow_group = (true);

					// 6/12/2012 rmd : This station is done with flow check. Has failed twice in a row. And
					// alert action set to alert only. So now longer belongs on the problem list.
					lilc->bbf.w_involved_in_a_flow_problem = (false);

					// 6/27/2012 rmd : Get the flow error indication in the station preserves set.
					if( pfrbf.flow_check_status == FRF_CHECK_STATUS_hi_flow )
					{
						action_reason = ACTION_REASON_TURN_OFF_hi_flow_fault;
					}
					else
					{
						action_reason = ACTION_REASON_TURN_OFF_lo_flow_fault;
					}

				}
				else
				{
					// 6/20/2012 rmd : The stations that fall down through to here were NOT on the problem list.
					// So if in this group of valves that was ON no-one was on the problem list then all valves
					// should end up on the problem list. If there was a valve on the problem list (AND THERE
					// SHOULD ONLY BE ONE) that valve is not this station that we are deciding what to do with.
					// And if a group with one on the problem list failed (as is the case here) the bad valve is
					// caught and all others should end up off the problem list. We don't want them on the list
					// just cause they were part of the hunt. If we left them on the problem list the then when
					// it turned back ON they would be caught as a second error if they happened to be part ofa
					// group with another bad valve.
					if( pbsr_ptr->ufim_one_ON_from_the_problem_list_b == (true) )
					{
						lilc->bbf.w_involved_in_a_flow_problem = (false);
					}
					else
					{
						lilc->bbf.w_involved_in_a_flow_problem = (true);
					}

					// 6/27/2012 rmd : In any case the action reason is considered a first fault or not a fault
					// for this valve. So we do not want to set the flow error indication in the station
					// preserves.
					action_reason = ACTION_REASON_TURN_OFF_first_flow_fault;
				}

				// 6/12/2012 rmd : Turn OFF the station. AFTER we are done setting if the station is
				// involved in a problem or not. Because of the weighting effect of that setting. And part
				// of the turn OFF is to re-sequence the list.
				next_ilc = nm_nm_FOAL_if_station_is_ON_turn_it_OFF( &(foal_irri.list_of_foal_stations_ON), lilc, action_reason );
				
				// 6/27/2012 rmd : Change the group now that the station is OFF.
				if( change_flow_group == (true) )
				{
					lilc->bbf.flow_check_group = FLOW_CHECK_GROUP_no_flow_checking;
				}

			}

		}
		else
		{
			next_ilc = nm_ListGetNext( &(foal_irri.list_of_foal_stations_ON), lilc );
		}
		
		lilc = next_ilc;
		
	}
	
}
	
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used during routine and turn offs and at the moment flow checking begins
    during a cycle and at the moment an expected is acquired. Not used during the process
    that takes place when there is a flow anomaly.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task as part of flow checking and
    the list maintenance function. Also called from the COMM_MNGR as part of various
    turn-off processes such as STOP key processing.

	@RETURN (none)

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS (none)
*/
extern void FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines(	BY_SYSTEM_RECORD				*pbsr_ptr,

																				BOOL_32							pmark_as_flow_checked,
																				BOOL_32							pstamp_report_rip,
																				BOOL_32							pmake_flow_recorder_line,

																				FLOW_RECORDER_FLAG_BIT_FIELD	pflow_recorder_flag,
																				UNS_32							psystem_actual_flow )
{
	IRRIGATION_LIST_COMPONENT	*ilc;
	
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	ilc = nm_ListGetFirst( &foal_irri.list_of_foal_stations_ON );
	
	while( ilc != NULL )
	{
		// 6/5/2012 rmd : Only for the system of interest.
		if( ilc->bsr == pbsr_ptr )
		{
			// ---------------------------
		
			if( pmark_as_flow_checked == (true) )
			{
				// 6/5/2012 rmd : The mark as flow checked argument is set true when we have performed and
				// passed flow checking. Indicate so in the ilc. (If there is a flow error we also set this
				// flag (true) via a different function.) Do we have to qualify this with the reason in the
				// list. Meaning only set this if in the list for programmed irrigation? I don't think so
				// because at the end we only test this if in the list for programmmed irrigation. This also
				// should be set (true) when this station is involved in a flow problem.
				ilc->bbf.at_some_point_flow_was_checked = (true);
			}
			
			// ---------------------------
		
			float	the_stations_share;
			
			UNS_32	station_derated_expected, station_share_of_actual_flow, station_share_of_hi_limit, station_share_of_lo_limit;
		
			// 6/12/2012 rmd : Protect against a divide by 0.
			if( pbsr_ptr->ufim_expected_flow_rate_for_those_ON == 0 )
			{
				the_stations_share = 0.0;
			}
			else
			{
				the_stations_share = ( ((float)ilc->expected_flow_rate_gpm_u16) / ((float)pbsr_ptr->ufim_expected_flow_rate_for_those_ON) );
			}
			
			station_derated_expected = __round_UNS16( (((float)pbsr_ptr->flow_check_derated_expected) * the_stations_share) );  // this value is now a derated expected
			
			station_share_of_actual_flow  = __round_UNS16( (((float)psystem_actual_flow) * the_stations_share) );  // this is a proportional piece of the actual based on the ratio of the expecteds
			
			station_share_of_hi_limit = __round_UNS16( (((float)pbsr_ptr->flow_check_hi_limit) * the_stations_share) );  // proportional piece of the limits
	
			station_share_of_lo_limit = __round_UNS16( (((float)pbsr_ptr->flow_check_lo_limit) * the_stations_share) );

			// ---------------------------
			
			if( pstamp_report_rip == (true) )
			{
				if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
				{
					// 6/4/2012 rmd : For the case of programmed irrigation, if the report data flow numbers
					// have not yet been stamped then stamp 'em. This is how we stamp them ONLY at the first
					// measurement point.
					if( station_preserves.sps[ ilc->station_preserves_index ].station_history_rip.pi_flag.pi_flow_data_has_been_stamped == (false) )
					{
						// 6/5/2012 rmd : Stamp the share of actual, and the share of hi and lo limits.
						station_preserves.sps[ ilc->station_preserves_index ].station_history_rip.pi_flow_check_share_of_actual_gpm = station_share_of_actual_flow;
		
						station_preserves.sps[ ilc->station_preserves_index ].station_history_rip.pi_flow_check_share_of_hi_limit_gpm = station_share_of_hi_limit;
		
						station_preserves.sps[ ilc->station_preserves_index ].station_history_rip.pi_flow_check_share_of_lo_limit_gpm = station_share_of_lo_limit;
		
						station_preserves.sps[ ilc->station_preserves_index ].station_history_rip.pi_flag.pi_flow_data_has_been_stamped = (true);
					}
				}
			}
	
			if( pmake_flow_recorder_line == (true) )
			{
				nm_flow_recorder_add(	ilc, 
	
										station_derated_expected,		// station derated expected
	
										station_share_of_hi_limit,		// station limit
										station_share_of_lo_limit,		// station limit
	
										station_share_of_actual_flow,	// proportional amount of actual flow rate
	
										pflow_recorder_flag );
			}

		}  // of if this ilc belongs to the system of interest
		
		ilc = nm_ListGetNext( &foal_irri.list_of_foal_stations_ON, ilc );
	}
	
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
}

/* ---------------------------------- */
static UNS_32 _derate_table_index( BY_SYSTEM_RECORD *pbsr_ptr, UNS_32 pgpm_index )
{
	// 6/22/2012 rmd : First of all we should only be here with TWO or more stations ON. We do
	// not use the derate table for the case of only ONE valve ON. Secondly keep in mind the
	// indexing should not waste the case of 1 valve ON in the table. So to calculate the index
	// not only do we subtract one from the number of valves ON we subtract another 1 to get the
	// number of valves ON a 0 based number. Imagining a multi-dimensional array, two valves ON
	// would be index 0 for the valve count axis. The gpm_index passed must already be correct.
	UNS_32	rv;

	// 6/22/2012 rmd : With the default numbers we have 9 station columns and 500 gpm slots.
	rv = ( (pgpm_index * (pbsr_ptr->flow_check_derate_table_max_stations_ON-1)) + (pbsr_ptr->system_master_number_of_valves_ON-2) );

	return( rv );
}

/* ---------------------------------- */
static char *_nm_nm_make_an_alert_line_about_the_valves_ON( BY_SYSTEM_RECORD *pbsr_ptr, char *pstr_64 )
{
	IRRIGATION_LIST_COMPONENT	*ilc;

	UNS_32	loop_count;

	char	str_8[ 8 ];

	// Initialize the string.
	memset( pstr_64, 0x00, 64 );
			
	ilc = nm_ListGetFirst( &foal_irri.list_of_foal_stations_ON );
	
	loop_count = 0;

	while( ilc != NULL )
	{
		if( ilc->bsr == pbsr_ptr )
		{
			// the line only allows so much room
			if( ++loop_count == 6 )
			{  
				sp_strlcat( pstr_64, 64, " (MORE)" );
		
				break;
			}

			ALERTS_get_station_number_with_or_without_two_wire_indicator( ilc->box_index_0, ilc->station_number_0_u8, str_8, sizeof(str_8) );

			sp_strlcat( pstr_64, 64, " %s", str_8 );
		}
		
		ilc = nm_ListGetNext( &foal_irri.list_of_foal_stations_ON, ilc );

		if( ilc == NULL )
		{
			// 6/13/2012 rmd : So we know the whole list is being displayed.
			sp_strlcat( pstr_64, 64, " (finished)" );
		}

	}  // of looping through all those ON
	
	return( pstr_64 );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Update the derate table cell pointed out by the ptable_index parameter.
    Additionally for those valves ON if needed update their cycle count.

    @CALLER_MUTEX_REQUIREMENTS The caller is expected to be holding both the
    system_preserves MUTEX and the foal_irri MUTEX.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. During the 1 hz rate flow
    checking. However this update will only be called ONCE per valve transition.
	
	@RETURN (none)

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS (none)
*/
static void _nm_nm_update_the_derate_table_and_valve_cycle_count_as_needed(	BY_SYSTEM_RECORD	*pbsr_ptr,
																			UNS_32				pworking_stability_avg,
																			UNS_32				pderate_table_flow_slot_index,
																			BOOL_32				pperform_hi_flow_test,
																			BOOL_32				pperform_lo_flow_test,
																			FLOW_RECORDER_FLAG_BIT_FIELD	*pfrbf_ptr )
{
	char		str_64[ 64 ];

	INT_32		diff_int;
	
	BOOL_32		proceed;
	
	IRRIGATION_LIST_COMPONENT *ilc;
	
	UNS_32	table_index;
	
	BOOL_32	stations_ON_have_enough_cycles, update_the_cell;

	UNS_32	old_cell_iteration_count, new_cell_iteration_count;

	INT_16	old_cell_value, new_cell_value;

	float	diff_fl, existing_value_fl, new_value_fl;
	
	// ----------
	
	// 6/18/2015 rmd : Use volatile constants to fix GCC version 4.6 floating point compiler
	// bug. YES A COMPILER BUG. Fixed in version 4.8 and up.

	const volatile float ZERO_POINT_THREE = 0.3;

	const volatile float ZERO_POINT_FIVE = 0.5;
	
	const volatile float TEN_POINT_O = 10.0;
	
	// ----------
	
	if( pderate_table_flow_slot_index >= pbsr_ptr->flow_check_derate_table_number_of_gpm_slots )
	{
		// 6/13/2012 rmd : This function should not be called if this index is out of bounds.
		ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_FLOW: unexp out of bounds index" );
	}
	else
	if( pbsr_ptr->system_master_number_of_valves_ON > pbsr_ptr->flow_check_derate_table_max_stations_ON )
	{
		// 6/13/2012 rmd : This function should not be called if this index is out of bounds.
		ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_FLOW: unexp out of bounds index" );
	}
	else
	if( pbsr_ptr->system_master_number_of_valves_ON == 1 )
	{
		// 6/13/2012 rmd : This function should not be called when only one valve is ON.
		ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_FLOW: unexp here with one ON" );
	}
	else
	{
		proceed = (true);
		
		// --------------------------
		
		// If we are testing for low flow and we have a zero flow rate do not allow the table to
		// update wether or not we are still learning - the point is if we are learning and we
		// intend to test for low flow do not learn a 0 flow rate into the table - if we are testing
		// and we passed the low flow test do not learn a 0 flow rate into the table either
		//
		// Note: we are however to learn a 0 flow rate into the table if we are not testing for low flow
		// this way the derated expected becomes 0 and if there is any flow (beyond the window) that would
		// trip the high flow
		if( (pperform_lo_flow_test == (true)) && (pworking_stability_avg == 0) )
		{
			proceed = (false);
			
			pfrbf_ptr->FRF_NO_UPDATE_REASON_zero_flow_rate = (true);

			strlcpy( str_64, _nm_nm_make_an_alert_line_about_the_valves_ON( pbsr_ptr, (char*)&str_64 ), sizeof(str_64) );

			// The truth is if we were actually testing as opposed to learning and the flow rate was 0
			// the low flow test would have failed - so we know we are here for learning.

			Alert_derate_table_update_failed( AID_DERATE_TABLE_UPDATE_FAILED_NO_FLOW, 0, pbsr_ptr->ufim_expected_flow_rate_for_those_ON, str_64 );
		}
		
		
		// okay so we have checked for the 0 flow rate now let's see that the expected and measured
		// aren't too far apart but only for MEASURED rates above 20 gpm cause at the low end 30% could
		// mean 1 gpm - and if the case of 0 measured makes it through to here the difference will
		// always be more than 30% of the expected
		//
		// this is really a flag for the user that stuff ain't flowing as he expected
		//
		// We have encountered a strange phenomenon - if you have the flow checking set to not make
		// the low flow check and the valve passes the high flow test we will update the derate table.
		// This would normally be okay except imagine if the user has twisted down the valve stem - now
		// we are taking a zero flow as an acceptable flow rate and incorporating it into the derate
		// table - after enough iterations this will create values that approach the expected itself.
		// When the valve stem is finally opened again the derated expected flow rate will be 0 and
		// the normal valve flow will trip an overflow.  
		//
		// We are going to control this by limiting the size of the value in the derate table
		// to be less than 30% of the expected on the positive side and less than 15% of the expected
		// on the negative side (remember the positive numbers are used to reduce the expected). 
		//
		// if we look at (pexpected_us >= 20 ) then when the expected is say 1 and the measured 25 we
		// won't throw the flag and we'll go about learning the derate table against expecteds of 1
		// again we use 20 cause at the lower flow rate 30% of a small number is a smaller number and
		// its hard to say where to draw the line on acceptable differences
		//
		// 10/30/2008 rmd - Rodger's discussion regarding a 12 gpm valve that falsely reported a high
		// flow because it had a 9 in the derate table. The 9 snuck in there because of the 20 and
		// came about because we think it learned based upon a bad valve in the system.
		// 
		// There is some thought that the 20 may be too big and in some systems you'd like to do this
		// 30% check for even lower flow rates. Perhaps the bottom limit ought to be tied to the flow
		// meter size. But be careful because of multiple flow meters and bypass manifolds.
		// 
		// Additionally, there is a thought that perhaps, for single valves on, we shouldn't used the
		// derate table at all. In other words, the flow test should use the straight expected numbers,
		// not derated expecteds.
		//
		// 608.f 01/28/2009 ajv - After some discussion, we have decided to lower the pmeasured down
		// to >= 10 rather than 20. Additionally, for flow rates between 5 and 10, we've added a 50%
		// check.
		if( proceed == (true) )
		{
			if( pworking_stability_avg >= 10 )
			{
				diff_int = (pbsr_ptr->ufim_expected_flow_rate_for_those_ON - pworking_stability_avg);
			
				if( abs(diff_int) > (ZERO_POINT_THREE * pbsr_ptr->ufim_expected_flow_rate_for_those_ON) )
				{
					// We are here to update the derate table which means we are learning to check flow or the
					// flow tests have passed - in either case if we are checking low flow (or potentially so)
					// and the flow rate is more than 30% less than the expected OR we are checking high flow
					// (or potentially so) and the flow rate is more than 30% higher than the expected do not
					// allow the update.
					if( ((pperform_lo_flow_test == (true)) && (pworking_stability_avg < pbsr_ptr->ufim_expected_flow_rate_for_those_ON)) ||
																	   ((pperform_hi_flow_test == TRUE) && (pworking_stability_avg > pbsr_ptr->ufim_expected_flow_rate_for_those_ON)) 
					   )
					{
						proceed = (false);
						
						pfrbf_ptr->FRF_NO_UPDATE_REASON_thirty_percent = (true);
						
						strlcpy( str_64, _nm_nm_make_an_alert_line_about_the_valves_ON( pbsr_ptr, (char*)&str_64 ), sizeof(str_64) );

						Alert_derate_table_update_failed( AID_DERATE_TABLE_UPDATE_FAILED_30_PCT_DIFF, pworking_stability_avg, pbsr_ptr->ufim_expected_flow_rate_for_those_ON, str_64 );
					}
					
				}
			
			} else  // if we are headed for an update and the expected is greater than or equal to 10 gpm
			if( pworking_stability_avg >= 5 )
			{
				diff_int = (pbsr_ptr->ufim_expected_flow_rate_for_those_ON - pworking_stability_avg);
				
				if( abs(diff_int) > (ZERO_POINT_FIVE * pbsr_ptr->ufim_expected_flow_rate_for_those_ON) )
				{
					// We are here to update the derate table which means we are learning to check flow or the
					// flow tests have passed - in either case if we are checking low flow (or potentially so)
					// and the flow rate is more than 50% less than the expected OR we are checking high flow
					// (or potentially so) and the flow rate is more than 50% higher than the expected do not
					// allow the update.
					if( ((pperform_lo_flow_test == (true)) && (pworking_stability_avg < pbsr_ptr->ufim_expected_flow_rate_for_those_ON)) ||
																	   ((pperform_hi_flow_test == TRUE) && (pworking_stability_avg > pbsr_ptr->ufim_expected_flow_rate_for_those_ON)) 
					   )
					{
						proceed = (false);
						
						// 6/21/2012 rmd : We don't have the 50% bit to set. And all 16 bits within the flag are
						// inuse. So just use the 30% error indication. If we really want to be smart look at the
						// flow recording measured flow. If it's less than 10 write 50%.
						pfrbf_ptr->FRF_NO_UPDATE_REASON_thirty_percent = (true);
						
						strlcpy( str_64, _nm_nm_make_an_alert_line_about_the_valves_ON( pbsr_ptr, (char*)&str_64 ), sizeof(str_64) );

						Alert_derate_table_update_failed( AID_DERATE_TABLE_UPDATE_FAILED_50_PCT_DIFF, pworking_stability_avg, pbsr_ptr->ufim_expected_flow_rate_for_those_ON, str_64 );
					}

				}
	
			}  // if we are headed for an update and the expected is greater than or equal to 5 gpm (and < 10)
			
		}
		
		if( proceed == (true) )
		{
			stations_ON_have_enough_cycles = (true);

			update_the_cell = (false);
			
			// ---------------------------
			
			// to prevent calling the function to calculate the index over and over again
			table_index = _derate_table_index( pbsr_ptr, pderate_table_flow_slot_index );
			
			// ---------------------------
			
			// FIRST learn if all the valves on have less than the 6 station cycles and also learn if
			// the cell has had 6 updates to it. If either the station(s) themselves haven't run enough
			// cycles or the derate cell hasn't had enough updates we WILL update the cell otherwise we
			// will NOT. This is the cell locking behavior and is akin to switching from learned to
			// limits automatically.
			
			// The scheme goes like this...when stations are on as a GROUP if one of them doesn't have
			// enough cycles to allow flow checking that causes the derate cell count to be reset to 0 -
			// this is to allow a station time to bounce around through different combinations resetting
			// the derate cell counts as it joins its new pairings - once all the cells in a group have
			// reached enough station cycle counts then the derate cell count will begin to increase -
			// this gives more time for the value in the derate cells to mature before using them - if a
			// station always combines with the same pairing the draw back to this approach is that it
			// will take 12 cycles to begin flow checking - that's probably ok. Remember however that
			// during a night the station count increments by one for each repeat. Same with the cell
			// value when it is updating.

			// 6/13/2012 rmd : Well we are referencing and changing the station preserves.
			xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

			ilc = nm_ListGetFirst( &foal_irri.list_of_foal_stations_ON );
			
			while( ilc != NULL )
			{
				if( ilc->bsr == pbsr_ptr )
				{
					// Check if this station has had enough cycles...if less than the required it has not.
					if( station_preserves.sps[ ilc->station_preserves_index ].spbf.flow_check_station_cycles_count < ilc->bsr->flow_check_required_station_cycles )
					{
						stations_ON_have_enough_cycles = (false);
						
						if( station_preserves.sps[ ilc->station_preserves_index ].spbf.flow_check_station_cycles_count < STATION_PRESERVES_MAXIMUM_FLOW_CHECK_COUNT )
						{
							station_preserves.sps[ ilc->station_preserves_index ].spbf.flow_check_station_cycles_count += 1;
						}

						// make an alert line about the specifics
						if( config_c.debug.show_flow_table_interaction == (true) )
						{
							Alert_derate_table_update_station_count_idx( ilc->box_index_0, (UNS_32)ilc->station_number_0_u8, station_preserves.sps[ ilc->station_preserves_index ].spbf.flow_check_station_cycles_count );
						}
						
					}  // of if the flow check iteration count is low
					
				}
				
				ilc = nm_ListGetNext( &foal_irri.list_of_foal_stations_ON, ilc );
				
			}  // of looping through all those ON
			
			xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
			
			// ---------------------------
			
			if( stations_ON_have_enough_cycles == (false) )
			{
				update_the_cell = (true);
			}
			else
			if( (pbsr_ptr->flow_check_allow_table_to_lock == (false)) || (pbsr_ptr->derate_cell_iterations[ table_index ] < pbsr_ptr->flow_check_required_cell_iteration) )
			{
				// 6/25/2012 rmd : The cell is still learning.
				update_the_cell = (true);
			}
			
			// ---------------------------
			
			// 6/25/2012 rmd : Now we are ready to update the cell. Either the station count for one of
			// those ON is too low. (Remember we hold the cell iteration count to zero until the station
			// count for those ON is achieved.) Or the cell iterations themselves are too low. Or the
			// setting is to never lock. As of this date all tables lock.
			if( update_the_cell )
			{
				// 6/25/2012 rmd : for the alert line
				old_cell_value = pbsr_ptr->derate_table_10u[ table_index ];
				
				old_cell_iteration_count = pbsr_ptr->derate_cell_iterations[ table_index ];
			
				// ----------
				
				existing_value_fl = ( ((float)pbsr_ptr->derate_table_10u[ table_index ]) / TEN_POINT_O );
				
				diff_fl = ((float)pbsr_ptr->ufim_expected_flow_rate_for_those_ON - (float)pworking_stability_avg);
				
				// 2000e note: I think the only reason to perform the 70% 30% game is when cells are shared
				// - this would prevent the derate value from widely bouncing around.
				//
				// 6/22/2012 rmd : With the NEW much larger derate table the likely hood of shared cells is
				// way down. I am leaning towards a faster convergence. Therefore I am moving the weighting
				// to 50-50 for the CS3000. The table learns and then locks. I am still leaving the
				// iterations requirement at 6 however. Believing for cases with a big difference (like 60
				// gpm, for example 360 expected and flows at 300) we need that many iterations.
				new_value_fl = ( ZERO_POINT_FIVE * existing_value_fl ) + ( ZERO_POINT_FIVE * diff_fl );
				
				pbsr_ptr->derate_table_10u[ table_index ] = ( new_value_fl * TEN_POINT_O );
				
				// ----------
				
				// 6/18/2015 rmd : Grab the alert line value as an integer.
				new_cell_value = pbsr_ptr->derate_table_10u[ table_index ];

				// ----------
				
				// 6/25/2012 rmd : Only when the stations ON have met their cycle count do we allw the cell
				// iteration count to begin its climb.
				if( stations_ON_have_enough_cycles == (true) )
				{
					if( pbsr_ptr->derate_cell_iterations[ table_index ] < pbsr_ptr->flow_check_required_cell_iteration )
					{
						pbsr_ptr->derate_cell_iterations[ table_index ] += 1;
					}
				}
				else
				{
					pbsr_ptr->derate_cell_iterations[ table_index ] = 0;
				}
				
				new_cell_iteration_count = pbsr_ptr->derate_cell_iterations[ table_index ];
				
				// ----------
				
				if( config_c.debug.show_flow_table_interaction == (true) )
				{
					Alert_derate_table_update_successful( pbsr_ptr->system_master_number_of_valves_ON,
						(pderate_table_flow_slot_index * pbsr_ptr->flow_check_derate_table_gpm_slot_size),
						table_index,
						old_cell_iteration_count, new_cell_iteration_count,
						old_cell_value, new_cell_value );
				}
			}
			else
			{
				// 6/25/2012 rmd : The implementation of the logic surraounding the call to this function is
				// such that we'll never call this function unless we have the need to update the cell.
				// Therefore we never see this error message.
				Alert_Message( "FOAL_FLOW: not logically expected." );
				/*
				if( config_c.debug.show_flow_table_interaction == (true) )
				{
					Alert_Message_va(	"Table is locked: [%u valve(s) on, %u gpm] index: %u, count: %u, value: %u",
										pbsr_ptr->number_of_valves_ON,
										(pderate_table_flow_slot_index * pbsr_ptr->flow_check_derate_table_gpm_slot_size),
										table_index,
										pbsr_ptr->derate_cell_iterations[ table_index ], pbsr_ptr->derate_table_10u[ table_index ] );
				}
				*/
			}
			
		}  // of we are going to update the derate table
		
	}  // of we passed the initial tests about index range and number ON > 1
	
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION For the system fill out the hi and lo limits based upon the derated
    expected flow rate at the moment. We are counting on the derated expected be up to date
    when calling this function.

	@CALLER_MUTEX_REQUIREMENTS Caller to be holding the system_preserves MUTEX.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task as part of flow checking.

	@RETURN (none)

	@ORIGINAL 2012.06.04 rmd

	@REVISIONS (none)
*/
static void _nm_set_the_hi_lo_limits( BY_SYSTEM_RECORD *pbsr_ptr )
{
	UNS_32	range_index;
	
	// 6/11/2012 rmd : Choose the range based on the derated expected.
	if( pbsr_ptr->flow_check_derated_expected < pbsr_ptr->flow_check_ranges_gpm[ 0 ] )
	{
		range_index = 0;
	}
	else
	if( (pbsr_ptr->flow_check_derated_expected >= pbsr_ptr->flow_check_ranges_gpm[ 0 ]) && (pbsr_ptr->flow_check_derated_expected <= pbsr_ptr->flow_check_ranges_gpm[ 1 ]) )
	{
		range_index = 1;
	}
	else
	if( (pbsr_ptr->flow_check_derated_expected >= pbsr_ptr->flow_check_ranges_gpm[ 1 ]) && (pbsr_ptr->flow_check_derated_expected <= pbsr_ptr->flow_check_ranges_gpm[ 2 ]) )
	{
		range_index = 2;
	}
	else
	{
		range_index = 3;
	}
	
	// Now that we have the range, set the hi and low limits - checking the low limit for
	// underflow
	pbsr_ptr->flow_check_hi_limit = (pbsr_ptr->flow_check_derated_expected + pbsr_ptr->flow_check_tolerance_plus_gpm[ range_index ]);
	
	if( pbsr_ptr->flow_check_tolerance_minus_gpm[ range_index ] >= pbsr_ptr->flow_check_derated_expected )
	{
		pbsr_ptr->flow_check_lo_limit = 0;
	}
	else
	{
		pbsr_ptr->flow_check_lo_limit = (pbsr_ptr->flow_check_derated_expected - pbsr_ptr->flow_check_tolerance_minus_gpm[ range_index ]);
	}
	
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Signal when we are allowed to test flow for valves that are ON.

    @CALLER_MUTEX_REQUIREMENTS Caller is to be holding the system_preserves MUTEX. We are
    using values within the system_preserves.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. Once per second as part of
    flow checking.
	
    @RETURN (true) if we are to proceed with the flow test, (false) otherwise.

	@ORIGINAL 2012.05.04 rmd

	@REVISIONS (none)
*/
static BOOL_32 _allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line( BY_SYSTEM_RECORD *pbsr_ptr )
{ 
	// ------------------------
	
	BOOL_32	rv;
	
	rv = (true);
	
	// ------------------------
	
	// 5/29/2012 rmd : Logically one would say you cannot check flow rates without a flow meter.
	// So we could check the number of flow meters in the system. HOWEVER AJ believes we should
	// at least make flow recording lines if we don't have a flow meter. That sounds logical. So
	// we don't look to see if there is a flow meter in the system here.
	
	// ------------------------
	
	// 5/29/2012 rmd : Make an exception to the MVJO timer for the case of 1 valve ON for TEST.
	// So the user doesn't have to wait so long to test flow.
	if( pbsr_ptr->system_master_number_of_valves_ON == 1 )
	{
		IRRIGATION_LIST_COMPONENT *lilc_ptr;
		
		// 5/4/2012 rmd : Only one in the list (supposedly) ... just grab the head.
		lilc_ptr = foal_irri.list_of_foal_stations_ON.phead;

		if( lilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_TEST )
		{
			// 5/4/2012 rmd : If there is one ON and for test we ignore the MVJO delay to allow MLB and
			// flow checking to commence after the line fill time has elapsed. Without having to wait
			// the 150 seconds for the MVJO timer to elapse. An aid for the user.
		}
		else
		{
			if( pbsr_ptr->timer_MVJO_flow_checking_blockout_seconds_remaining > 0 )
			{
				rv = (false);	
			}
		}
	}
	else
	{
		if( pbsr_ptr->timer_MVJO_flow_checking_blockout_seconds_remaining > 0 )
		{
			rv = (false);	
		}
	}
	
	// ------------------------
	
	// 2/20/2013 rmd : Test the flow checking block out timer. It must be 0 to allow flow
	// checking.
	if( pbsr_ptr->flow_checking_block_out_remaining_seconds > 0 )
	{
		rv = (false);
	}
	
	// ------------------------
	
	// 5/29/2012 rmd : And finally our all important stable flow indication. Which I actually am
	// beginning to wonder how 'all important' it really is. I mean we average the flow numbers
	// so heavily for the stability avergaes. And we are protected by all sorts of event
	// boundary timers. So is this even needed anymore? For now I'll leave it.
	if( pbsr_ptr->sbf.stable_flow == (false) )
	{
		rv = (false);
	}
	
	// ------------------------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION So the conditions have been met to check the flow or as in this case
    acquire the expected. flow has been Check flow for the valves that are ON. And make flow
    recording lines if not yet made. Also responsible to acquire expected flow rate if that
    is what we're up to ie the flag is set for the valve ON.

    @CALLER_MUTEX_REQUIREMENTS Caller to be holding both the system_preserves and the
    foal_irri MUTEXES.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. Once per second.

    @RETURN (none)

	@ORIGINAL 2012.05.30 rmd

	@REVISIONS (none)
*/
static void _nm_assign_new_expected_and_turn_all_stations_OFF( BY_SYSTEM_RECORD *psystem_ptr, float pnew_expected_fl )
{
	IRRIGATION_LIST_COMPONENT	*lilc;

	STATION_STRUCT				*lss_ptr;

	UNS_32						new_expected_uns32;
	
	UNS_32						captured_number_ON;

	char    					str_16[ 16 ];
	
	// ----------
	
	// 6/4/2012 rmd : Test that there is ONLY one ON.
	if( psystem_ptr->system_master_number_of_valves_ON != 1 )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_IRRI: unable to acquire - more than 1 ON." );
	}

	// 6/19/2012 rmd : Because if there is more than one actually ON, as we turn off the valves
	// eventually this number will become 1. And we would then in error acquire the expected. So
	// capture the value and use the captured number ON to control if we will acquire.
	captured_number_ON = psystem_ptr->system_master_number_of_valves_ON;
	
	// 6/4/2012 rmd : Now turn OFF all those ON within the system of interest. This covers the
	// case where more than 1 is ON. Which is an ERROR. We alerted about this error at the start
	// of this function.

	// 6/27/2012 rmd : We're going to loop through turning OFF. If there is more than 1 ON, (an
	// error), only make a flow recording line for the first group (if not already made).
	foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (false);


	lilc = nm_ListGetFirst( &(foal_irri.list_of_foal_stations_ON) );

	while( lilc != NULL )
	{
		if( lilc->bsr == psystem_ptr )
		{
			// 6/5/2012 rmd : So if indeed this was the ONLY one ON acquire the expected.
			if( captured_number_ON == 1 )
			{
				// 7/17/2015 rmd : Convert to an integer format to prepare for the test against 0. We would
				// have to test the floating point value against 1.0 (maybe). More sure fired to test as an
				// integer. We are protecting against learning and saving a value of 0. This would cause
				// math problems later when calculating gallons for report data accumulators.
				new_expected_uns32 = (UNS_32)pnew_expected_fl;
				
				if( new_expected_uns32 == 0 )
				{
					Alert_Message_va( "Unable to acquire expected for Sta %s - measured 0 gpm", STATION_get_station_number_string( lilc->box_index_0, (lilc->station_number_0_u8+1), (char*)&str_16, sizeof(str_16) ) );
					
					// 7/17/2015 rmd : And we are not going to bring this value into range - say set the new
					// expected to a 1 - and then assign that to the expected. That just doesn't make any sense.
					// Abandon the attempt to acquire.
				}
				else
				{
					// 6/25/2012 rmd : To call the STATION GET and SET functions we are to hold this mutex.
					xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
					
					lss_ptr = nm_STATION_get_pointer_to_station( lilc->box_index_0, lilc->station_number_0_u8 );

					if( nm_OnList( &station_info_list_hdr, lss_ptr ) == (true) )
					{
						nm_STATION_set_expected_flow_rate( lss_ptr, new_expected_uns32, CHANGE_generate_change_line, CHANGE_REASON_ACQUIRED_EXPECTED_GPM, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( lss_ptr, CHANGE_REASON_ACQUIRED_EXPECTED_GPM ) );
					}
					else
					{
						ALERT_MESSAGE_WITH_FILE_NAME( "FLOW: not on list!" );
					}
					
					xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

					// 6/5/2012 rmd : And update the ilc for use in sucessive irrigations that may follow (by
					// definition if this is programmed irrigation the normal programmed irrigation will
					// follow).
					lilc->expected_flow_rate_gpm_u16 = (UNS_16)new_expected_uns32;

					// 6/21/2012 rmd : And since we update the ilc expected we MUST update the system expected
					// for when this valve is going to turn OFF. Which is coming right up. If not the system
					// expected will be out of whack as compared to the ilc expected and throw an alert
					// line. Since there is ONLY ONE station ON it is a direct equate.
					psystem_ptr->ufim_expected_flow_rate_for_those_ON = new_expected_uns32;
				}
				
			}

			// ----------
			
			if( lilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
			{
				// 5/9/2012 rmd : Turn OFF the station. But do not remove it. We subsequently want to
				// irrigate its scheduled irrigation.
				lilc = nm_nm_FOAL_if_station_is_ON_turn_it_OFF( &(foal_irri.list_of_foal_stations_ON), lilc, ACTION_REASON_TURN_OFF_have_acquired_expected );
			}
			else
			{
				// 6/7/2012 rmd : For cases other than programmed irrigation we not only turn OFF the valve
				// we remove it from the main irrigation list. Add to action_needed first to ensure ilc is
				// not eligible for re-use. Though MUTEX protection covers that.
				lilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_stations_ON), lilc, ACTION_REASON_TURN_OFF_AND_REMOVE_have_acquired_expected );
			}

		}
		else
		{
			lilc = nm_ListGetNext( &(foal_irri.list_of_foal_stations_ON), lilc );
		}
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Check flow for the valves that are ON. And make flow recording lines if not
    yet made. Also responsible to acquire expected flow rate if that is what we're up to ie
    the flag is set for the valve ON.

    @CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task. Once per second.

    @RETURN (none)

	@ORIGINAL 2012.05.30 rmd

	@REVISIONS (none)
*/
extern void FOAL_FLOW_check_valve_flow( void )
{
	UNS_32		s;
	
	BOOL_32		derate_indicies_out_of_bounds, enough_updates_to_derate_cell, valves_on_have_enough_cycles;
				
	UNS_32		derate_table_flow_slot_index, table_index;
	
	BOOL_32		waiting_to_check_flow, waiting_to_acquire_expected;
	
	FLOW_RECORDER_FLAG_BIT_FIELD	frbf;

	BOOL_32		perform_hi_flow_test, perform_lo_flow_test;
	
	float		working_stability_average;

	// 6/23/2015 rmd : Be sure to use signed integers. Remember the derate table holds negative
	// values and the math gets all messed up if we mix up signed and unsigned variables.
	INT_32		working_system_expected_int, working_derated_system_expected_int, derate_amount_int;
							
	// ----------
	
	// 5/31/2012 rmd : We want the irrigation lists (main list of all and the ON list, etc..) to
	// remain stable during this functions execution. For example I think it is a somewhat out
	// of control situation if the communications delivers a message about the STOP key which
	// empties the list after we have already decided to check flow. This may or may not make
	// trouble.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );
			
	// 5/3/2012 rmd : We reference and set several things within the system preserves. Make sure
	// they are stable.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// 5/30/2012 rmd : For each system go through the flow checking process.
	for( s=0; s<MAX_POSSIBLE_SYSTEMS; s++ )
	{
		if( system_preserves.system[ s ].system_gid != 0 )
		{
			// 5/31/2012 rmd : Start out set up so that we are checking flow. The first reason we bump
			// into - through the series of tests we make - that sets this (false) is the reason we'll
			// reveal to the rest of the world. And the reason that'll show in the flow recording line.
			waiting_to_check_flow = (true);
			
			waiting_to_acquire_expected = ( (system_preserves.system[ s ].ufim_one_ON_to_set_expected_b == (true)) || (system_preserves.system[ s ].ufim_one_RRE_ON_to_set_expected_b == (true)) );

			// 5/31/2012 rmd : Start the flow recording flag and reasons all out to all zeros (false).
			frbf.overall_size = 0;

			// 6/5/2012 rmd : NOTE - the system level flags about the flow checking activity are all
			// cleared to (false) each time a valve turns ON. Within this function all we are allowed to
			// do is to set as appropriate flags to (true). Evaluation of such flags is performed when
			// valves are turned OFF. And that can happen in a few different places.

			// ----------------------------

			// 5/31/2012 rmd : If there are none ON we are not waiting to do anything.
			if( system_preserves.system[ s ].system_master_number_of_valves_ON == 0 )
			{
				waiting_to_check_flow = (false);

				waiting_to_acquire_expected = (false);
				
				// 5/31/2012 rmd : Let all others on the chain know we are not even considering testing
				// flow. The system bit field structure for the active systems is sent with each token.
				system_preserves.system[ s ].sbf.system_level_no_valves_ON_therefore_no_flow_checking = (true);
			}
			else
			if( system_preserves.system[ s ].sbf.number_of_flow_meters_in_this_sys == 0 )
			{
				// 5/31/2012 rmd : Hey no flow meters so we can't be waiting to check flow or acquire an
				// expected.
				waiting_to_check_flow = (false);
				
				waiting_to_acquire_expected = (false);
				
				frbf.flow_check_status = FRF_CHECK_STATUS_not_going_to_perform;

				frbf.FRF_NO_CHECK_REASON_no_flow_meter = (true);

				system_preserves.system[ s ].sbf.system_level_valves_are_ON_but_will_not_be_checking_flow = (true);
			}
			
			// ----------------------------

			// 5/31/2012 rmd : If there is a valves ON, waiting to learn expected will still be (true).
			// And waiting to learn expected takes priority over waiting to check flow. It's one or the
			// other man.
			if( waiting_to_acquire_expected == (true) )
			{
				waiting_to_check_flow = (false);

				// 6/1/2012 rmd : The turn ON rules are supposed to prevent more than ONE valve on at a time
				// when acquiring the flow rate.
				if( system_preserves.system[ s ].system_master_number_of_valves_ON == 1 )
				{
					frbf.flow_check_status = FRF_CHECK_STATUS_not_going_to_perform;
	
					frbf.FRF_NO_CHECK_REASON_acquiring_expected = (true);
	
					system_preserves.system[ s ].sbf.system_level_valves_are_ON_and_waiting_to_acquire_expected = (true);
				}
				else
				{
					// 6/1/2012 rmd : Don't acquire when more than ONE on.
					waiting_to_acquire_expected = (false);
					
					Alert_Message( "FOAL_FLOW: expected only 1 valve ON during acquire" );
				}
			}
	   
			// 5/31/2012 rmd : So after this point we KNOW if we are still waiting to test flow we are
			// NOT waiting to acquire an expected. And vice-a-versa!

			// ----------------------------

			if( waiting_to_check_flow == (true) )
			{
				if( system_preserves.system[ s ].sbf.flow_checking_enabled_by_user_setting == (false) )
				{
					waiting_to_check_flow = (false);
					
					frbf.flow_check_status = FRF_CHECK_STATUS_not_going_to_perform;
	
					frbf.FRF_NO_CHECK_REASON_not_enabled_by_user_setting = (true);
	
					system_preserves.system[ s ].sbf.system_level_valves_are_ON_but_will_not_be_checking_flow = (true);
				}
			}
			
			// ----------------------------

			if( waiting_to_check_flow == (true) )
			{
				// 5/31/2012 rmd : Are we running any from group of valves that doesn't test flow at all
				// - if so we are not waiting to test flow.
				if( system_preserves.system[ s ].ufim_flow_check_group_count_of_ON[ FLOW_CHECK_GROUP_no_flow_checking ] > 0 )
				{
					waiting_to_check_flow = (false);
					
					frbf.flow_check_status = FRF_CHECK_STATUS_not_going_to_perform;

					// 8/4/2015 rmd : Have considered not setting any flag here. Is this a somewhat normal
					// occurance? Will the on at a time report be cluttered. Let's test and see.
					frbf.FRF_NO_CHECK_REASON_not_supposed_to_check = (true);
	
					system_preserves.system[ s ].sbf.system_level_valves_are_ON_but_will_not_be_checking_flow = (true);
				}
			}
			
			// ----------------------------

			if( waiting_to_check_flow == (true) )
			{
				// If there are multiple station ON and some want the pump and others not - not allowed to
				// check flow  (This really can't happen except during TEST and DTMF and under those
				// conditions we won't be checking flow anyway - so this term is probably useless but
				// it don't cost much code so leave it.)
				if( system_preserves.system[ s ].ufim_there_is_a_PUMP_mix_condition_b == (true) )
				{
					waiting_to_check_flow = (false);
					
					frbf.flow_check_status = FRF_CHECK_STATUS_not_going_to_perform;
	
					frbf.FRF_NO_CHECK_REASON_combo_not_allowed_to_check = (true);
	
					system_preserves.system[ s ].sbf.system_level_valves_are_ON_but_will_not_be_checking_flow = (true);
				}
			}
			
			// ----------------------------

			// 6/19/2012 rmd : I'm not sure why we do this. That is not test flow if there is more than
			// 1 valve ON. Doesn't make any sense to me. We allow pump and non-pump to ONE 

			/*
			if( waiting_to_check_flow == (true) )
			{
				// If the number on during test is more than one we do not test flow.
				if( system_preserves.system[ s ].ufim_number_ON_during_test > 1 )
				{
					waiting_to_check_flow = (false);
					
					frbf.flow_check_status = FRF_CHECK_STATUS_not_going_to_perform;
	
					frbf.FRF_NO_CHECK_REASON_combo_not_allowed_to_check = (true);
	
					system_preserves.system[ s ].sbf.system_level_valves_are_ON_but_will_not_be_checking_flow = (true);
				}
			}
			*/
			
			// -----------------------------
			
			// 6/4/2012 rmd : A convenience variable so we don't have to keep looking it up from the
			// ring.
			working_stability_average = system_preserves.system[ s ].system_stability_averages_ring[ system_preserves.system[ s ].stability_avgs_index_of_last_computed ];
			
			working_system_expected_int = system_preserves.system[ s ].ufim_expected_flow_rate_for_those_ON;
			
			// -----------------------------

			if( waiting_to_acquire_expected == (true) )
			{
				if( _allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line( &(system_preserves.system[ s ]) ) == (true) )
				{
					if( system_preserves.system[ s ].sbf.checked_or_updated_and_made_flow_recording_lines == (false) )
					{
						// 6/1/2012 rmd : The flag was set up ealier. Make the line. If we didn't do it here the
						// turn OFF process would generate one. Without the appropriate flag.
						FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines(	&(system_preserves.system[ s ]),
																							FLOW_RECORDING_OPTION_NO_DONT_mark_as_flow_checked,			// zero flow problem count
																							FLOW_RECORDING_OPTION_NO_DONT_stamp_the_report_rip,			// stamp the report rip
																							FLOW_RECORDING_OPTION_YES_make_flow_recording_lines, 		// make flow recorder line
																							frbf,   													// flow_recorder_flag_uc
																							working_stability_average									// system_actual_flow_us
																						);
																							 
						// 6/4/2012 rmd : Prevent the turn OFF function from generating another flow recording line.
						system_preserves.system[ s ].sbf.checked_or_updated_and_made_flow_recording_lines = (true);
						
						// 6/1/2012 rmd : Assign the latest system stability average to the expected for the station
						// that is ON. And reset the flag to acquire expected. And turn the station OFF.
						_nm_assign_new_expected_and_turn_all_stations_OFF( &(system_preserves.system[ s ]), working_stability_average );
					}
					
				}
				
			}
			else
			if( waiting_to_check_flow == (true) )
			{
				// 6/22/2012 rmd : Setup the defaults to cause flow checking.
				derate_indicies_out_of_bounds = (false);

				enough_updates_to_derate_cell = (true);

				valves_on_have_enough_cycles = (true);
				
				table_index = 0;
				
				// ----------------------------
				
				// 7/18/2012 rmd : To prevent compiler warning about potential use of an uninitialized
				// variable.
				derate_table_flow_slot_index = 0;
				
				// ----------------------------
				
				// 6/22/2012 rmd : We only use the table for 2 or more valves ON.
				if( system_preserves.system[ s ].system_master_number_of_valves_ON >= 2 )
				{
					// 6/22/2012 rmd : The derate table is used for up to but NOT including the max flow rate.
					// So our default handles up to but not including 500 gpm.
					derate_table_flow_slot_index = (working_system_expected_int / system_preserves.system[ s ].flow_check_derate_table_gpm_slot_size);
					
					if( derate_table_flow_slot_index >= system_preserves.system[ s ].flow_check_derate_table_number_of_gpm_slots )
					{
						derate_indicies_out_of_bounds = (true);
					}
					
					// 6/22/2012 rmd : Do not have to include the fact that we do not use the table for the case
					// of ONE valve because the max stations ON doesn't factor that in. It would say 10 for
					// example and that means we can have 10 valves ON. But the derate table only has 9 columns
					// for valves ON. 2 through 10.
					if( system_preserves.system[ s ].system_master_number_of_valves_ON > system_preserves.system[ s ].flow_check_derate_table_max_stations_ON )
					{
						derate_indicies_out_of_bounds = (true);
					}
					
					// Don't look at the derate_update_count when we know the indicies are out of bounds.
					if( derate_indicies_out_of_bounds == (false) )
					{
						table_index = _derate_table_index( &(system_preserves.system[ s ]), derate_table_flow_slot_index );
						
						if( system_preserves.system[ s ].derate_cell_iterations[ table_index ] < system_preserves.system[ s ].flow_check_required_cell_iteration )
						{
							enough_updates_to_derate_cell = (false);
						}
					}
					
					// 6/22/2012 rmd : Take this at face value.
					valves_on_have_enough_cycles = system_preserves.system[ s ].ufim_the_valves_ON_meet_the_flow_checking_cycles_requirement;
				}

				// ----------------------------

				if( derate_indicies_out_of_bounds == (true) )
				{
					// 6/5/2012 rmd : If the derate indicies are out of bounds we are not going to check flow or
					// learn the derate table.
				}
				else
				if( system_preserves.system[ s ].ufim_one_ON_from_the_problem_list_b || (valves_on_have_enough_cycles && enough_updates_to_derate_cell) )
				{
					// 609.a ajv POTENTIAL BUG: If you look carefully at the above if-statement, we are looking
					// to see whether a station is on the problem list, and if it is, continuing into this code.
					// This means that when a station on the problem list is ON, we will attempt to check flow,
					// regardless of whether the other valves on have enough iterations or the cell has had
					// enough updates.
					//
					// 6/5/2012 rmd : As an alternative we could reset the station flag 'saying it is on the
					// problem list' if the conditions to test weren't met. But this would in effect ignore the
					// flow error condition detected. Well this has been this way in the 2000e and is out there
					// working so must not be much of an issue. If it has any effect I would expect such only
					// early on as the system was learning the derate table and getting cycles on the valves. So
					// only for a short period.
					
					// Let the world know we'd like to check flow. Just not yet allowed to.
					system_preserves.system[ s ].sbf.system_level_valves_are_ON_and_waiting_to_check_flow = (true);
				}
				else
				{
					// Let the world know we'd like to update the derate table. Just not yet allowed to.
					system_preserves.system[ s ].sbf.system_level_valves_are_ON_and_waiting_to_update_derate_table = (true);
				}
				
			
				if( _allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line( &(system_preserves.system[ s ]) ) == (true) )
				{
					// 6/12/2012 rmd : Set what we need to do. Either we are going to check flow OR we are going
					// to update the derate table and/or station cycle counts.

					// If the derate indicies are out of bounds we are not going to test flow or learn the
					// derate table.
					if( derate_indicies_out_of_bounds == (true) )
					{
						// do nothing to the flow check status used by the display over on the irri side
						// it is already set to say 'no checking'
					}
					else
					if( (system_preserves.system[ s ].ufim_one_ON_from_the_problem_list_b == (true)) || 
																	   ((valves_on_have_enough_cycles == (true)) && (enough_updates_to_derate_cell == (true)) ) )
					{
						// 6/11/2012 rmd : Once this is set (true) during a cycle it remains (true). And indicates
						// just what it says. We are checking flow.
						system_preserves.system[ s ].sbf.system_level_valves_are_ON_and_actively_checking = (true);
					}
					else
					{
						// let the world know we have updated the derate table
						system_preserves.system[ s ].sbf.system_level_valves_are_ON_and_has_updated_the_derate_table = (true);
					}
					
					// ------------------------

					// Based on the flow_group_count_us array set the vars controlling which test to perform.
					if( system_preserves.system[ s ].ufim_flow_check_group_count_of_ON[ FLOW_CHECK_GROUP_check_for_high_and_low_flow ] > 0 )
					{
						perform_hi_flow_test = (true);

						perform_lo_flow_test = (true);
					}
					else
					if( system_preserves.system[ s ].ufim_flow_check_group_count_of_ON[ FLOW_CHECK_GROUP_check_for_high_flow_only ] > 0 )
					{
						perform_hi_flow_test = (true);

						perform_lo_flow_test = (false);
					}
					else
					if( system_preserves.system[ s ].ufim_flow_check_group_count_of_ON[ FLOW_CHECK_GROUP_check_for_low_flow_only ] > 0 )
					{
						perform_hi_flow_test = (false);

						perform_lo_flow_test = (true);
					}
					else
					{
						perform_hi_flow_test = (false);

						perform_lo_flow_test = (false);
					}
					
					// ------------------------

					// Calculate the derated expected.
					if( derate_indicies_out_of_bounds == (false) )
					{
						// 609.a ajv - We no longer want to use the derated expected when running just one station at
						// a time. Therefore, if only one station is on, set the derated system_derated_expected_us
						// to system_expected_us.
						if( system_preserves.system[ s ].system_master_number_of_valves_ON == 1 )
						{
							working_derated_system_expected_int = working_system_expected_int;
						}
						else
						{
							derate_amount_int = (system_preserves.system[ s ].derate_table_10u[ table_index ] / 10);
							
							if( derate_amount_int >= working_system_expected_int )
							{
								working_derated_system_expected_int = 0;
							}
							else
							{
								working_derated_system_expected_int = (working_system_expected_int - derate_amount_int);
							}
						}
					}
					else
					{
						// 6/8/2012 rmd : When the indicies are out of bounds we are not going to make any tests but
						// set the derated expected equal to the system expected as it may get stamped in a line
						// somewhere.
						working_derated_system_expected_int = working_system_expected_int;
					}
					
					// 6/8/2012 rmd : The function that sets the hi and lo limits is expecting the system
					// derated expected to be up to date.
					system_preserves.system[ s ].flow_check_derated_expected = working_derated_system_expected_int;
					
					_nm_set_the_hi_lo_limits( &(system_preserves.system[ s ]) );

					// THE ACTUAL TESTS
					if( (system_preserves.system[ s ].sbf.system_level_valves_are_ON_and_actively_checking == (true)) &&
						(perform_hi_flow_test == (true)) &&
						(working_stability_average > system_preserves.system[ s ].flow_check_hi_limit)
					  )
					{
						// 6/11/2012 rmd : Then we have an overflow. Find each on valve and mark it with an
						// overflow. Set the flow recorder flag here since we know what it should be.
						frbf.flow_check_status = FRF_CHECK_STATUS_hi_flow;
						
						_nm_nm_for_a_flow_error_make_flow_recording_lines_and_turn_OFF_all_ON( frbf, &(system_preserves.system[ s ]), working_stability_average );

					}
					else 							           
					if( (system_preserves.system[ s ].sbf.system_level_valves_are_ON_and_actively_checking == (true)) &&
						(perform_lo_flow_test == (true)) &&
						(working_stability_average < system_preserves.system[ s ].flow_check_lo_limit)
					  )
					{
						// 6/11/2012 rmd : Then we have an overflow. Find each on valve and mark it with an
						// overflow. Set the flow recorder flag here since we know what it should be.
						frbf.flow_check_status = FRF_CHECK_STATUS_lo_flow;
						
						_nm_nm_for_a_flow_error_make_flow_recording_lines_and_turn_OFF_all_ON( frbf, &(system_preserves.system[ s ]), working_stability_average );

					}
					else
					if( system_preserves.system[ s ].sbf.checked_or_updated_and_made_flow_recording_lines == (false) )
					{
						// 6/13/2012 rmd : If we tested flow then set the flow recording flag appropriately.
						if( (system_preserves.system[ s ].sbf.system_level_valves_are_ON_and_actively_checking == (true)) && (derate_indicies_out_of_bounds == (false))
						  )
						{
							// then we tested flow
							frbf.flow_check_status = FRF_CHECK_STATUS_passed;
						}
						else
						{
							frbf.flow_check_status = FRF_CHECK_STATUS_not_going_to_perform;
							
							if( derate_indicies_out_of_bounds == (true) )
							{
								frbf.FRF_NO_CHECK_REASON_indicies_out_of_range = (true);

								if( derate_table_flow_slot_index >= system_preserves.system[ s ].flow_check_derate_table_number_of_gpm_slots )
								{
									Alert_derate_table_flow_too_high();
								}
								
								if( system_preserves.system[ s ].system_master_number_of_valves_ON > system_preserves.system[ s ].flow_check_derate_table_max_stations_ON )
								{
									Alert_derate_table_too_many_stations();
								}
							}
							else
							{
								if( valves_on_have_enough_cycles == (false) )
								{
									frbf.FRF_NO_CHECK_REASON_station_cycles_too_low = (true);
								}
								
								if( enough_updates_to_derate_cell == (false) )
								{
									frbf.FRF_NO_CHECK_REASON_cell_iterations_too_low = (true);
								}
								
								// 6/12/2012 rmd : The logic is not very straight-forward. But we end up here if either
								// "valves on have enough cycles" is (false) or "enough updates to derate cell" is (false).
								// Or both.
								_nm_nm_update_the_derate_table_and_valve_cycle_count_as_needed( &(system_preserves.system[ s ]),
																								working_stability_average,
																								derate_table_flow_slot_index,
																								perform_hi_flow_test,
																								perform_lo_flow_test,
																								&frbf );
							}
							
						}
						
						FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines(	&(system_preserves.system[ s ]),
																							FLOW_RECORDING_OPTION_YES_mark_as_flow_checked,				// zero flow problem count
																							FLOW_RECORDING_OPTION_YES_stamp_the_report_rip,				// stamp the report rip
																							FLOW_RECORDING_OPTION_YES_make_flow_recording_lines, 		// make flow recorder line
																							frbf,   													// flow_recorder_flag_uc
																							working_stability_average									// system_actual_flow_us
																						);

						system_preserves.system[ s ].sbf.checked_or_updated_and_made_flow_recording_lines = (true);
						
					}  // of if flow has already been check during this cycle
					
				}  // of finally allowed to make the flow test
				
			}  // end of if flow_checking
			else
			{
				// 6/5/2012 rmd : Section when not flow_checking and not acquiring expected. Which can be
				// for a few reasons - like say flow groups says we are not checking flow (ON for DTMF or
				// Alert Actions say no check) or we have two valves on during TEST - well we would still
				// like to generate a flow recording line if we have a flow meter and there are valves ON.
				if( system_preserves.system[ s ].system_master_number_of_valves_ON > 0 )
				{
					if( system_preserves.system[ s ].sbf.number_of_flow_meters_in_this_sys > 0 )
					{
						if( _allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line( &(system_preserves.system[ s ]) ) == (true) )
						{
							if( system_preserves.system[ s ].sbf.checked_or_updated_and_made_flow_recording_lines == (false) )
							{
								// We're not testing flow but if we have a flow meter we should stuff the log lines with the
								// flow rates...remember with a flow meter we ASSUME the expecteds are good (cause that's
								// how the flow rate is split up for the report data)
								//
								// Stamp the report rip. We have a flow meter. We should.
								//
								// Make a flow recording line for the valves ON.
								FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines(	&(system_preserves.system[ s ]),
																									FLOW_RECORDING_OPTION_NO_DONT_mark_as_flow_checked,		// zero flow problem count
																									FLOW_RECORDING_OPTION_YES_stamp_the_report_rip,			// stamp the report rip
																									FLOW_RECORDING_OPTION_YES_make_flow_recording_lines,	// make flow recorder line
																									frbf,   												// flow_recorder_flag_uc
																									working_stability_average								// system_actual_flow_us
																								);
																							  
								system_preserves.system[ s ].sbf.checked_or_updated_and_made_flow_recording_lines = (true);

							}
							
						}
						
					}
					else
					{
						// 6/7/2012 rmd : Case of NO flow meter.

						if( system_preserves.system[ s ].sbf.checked_or_updated_and_made_flow_recording_lines == (false) )
						{
							// 6/1/2012 rmd : The flag was set up ealier. Make the line. If we didn't do it here the
							// turn OFF process would generate one. Without the appropriate flag. Use the system
							// expected as both the expected and the measured since there is no flow meter.
							FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines(	&(system_preserves.system[ s ]),
																								FLOW_RECORDING_OPTION_NO_DONT_mark_as_flow_checked,		// zero flow problem count
																								FLOW_RECORDING_OPTION_NO_DONT_stamp_the_report_rip,		// stamp the report rip
																								FLOW_RECORDING_OPTION_YES_make_flow_recording_lines,	// make flow recorder line
																								frbf,   												// flow_recorder_flag_uc
																								0														// system_actual_flow_us
																							);
																						  
							system_preserves.system[ s ].sbf.checked_or_updated_and_made_flow_recording_lines = (true);

						}  // of if we already made a flow recording line
						
					}  // of if there are any flow meters in the system
					
				}  // of if any stations are ON
				
			}  // of not waiting to acquire expected or check flow

		}  // of if this system is in use
		
	}  // of for each possible system
		
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );

}  // of function

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

