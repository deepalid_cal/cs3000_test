/*  file = foal_irri.c                10.27.00 2011.08.01 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"foal_irri.h"

#include	"app_startup.h"

#include	"background_calculations.h"

#include	"budgets.h"

#include	"configuration_controller.h"

#include	"configuration_network.h"

#include	"td_check.h"

#include	"foal_flow.h"

#include	"flow_recorder.h"

#include	"epson_rx_8025sa.h"

#include	"alerts.h"

#include	"comm_mngr.h"

#include	"battery_backed_vars.h"

#include	"r_alerts.h"

#include	"irri_time.h"

#include	"irri_comm.h"

#include	"irri_irri.h"

#include	"irrigation_system.h"

#include	"freeRTOSconfig.h"

#include	"change.h"

#include	"manual_programs.h"

#include	"station_groups.h"

#include	"weather_control.h"

#include	"range_check.h"

#include	"flowsense.h"

#include	"cs_mem.h"

#include	"alert_parsing.h"

#include	"watersense.h"

#include	"controller_initiated.h"

#include	"moisture_sensors.h"

#include	"ftimes_funcs.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void rain_switch_timer_callback( xTimerHandle pxTimer )
{
	// Remember this is executed within the context of the high priority timer task.
	weather_preserves.rain_switch_active = (false);

	Alert_rain_switch_inactive();
}

/* ---------------------------------------------------------- */
static void freeze_switch_timer_callback( xTimerHandle pxTimer )
{
	// Remember this is executed within the context of the high priority timer task.

	weather_preserves.freeze_switch_active = (false);

	Alert_freeze_switch_inactive();
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function is called on a program start. Any on-going irrigation is
    preserved but all the stations are turned OFF. Flow recording lines are made marked with
    a specific reason (restart, forced in, forced out).

    If the irrigation is not destroyed we set a flag so that the first TOKEN tells the IRRI
    machines to also restart and accept a new irrigation list. Remember the remote irri
    machines may not have experienced the power failure.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the APP_STARTUP task as part of progam startup
    initialization. Also executed when the chain transitions to DOWN.

	@RETURN (none)

	@ORIGINAL 2012.05.11 rmd

	@REVISIONS (none)
*/
extern void FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down( BOOL_32 pcompletely_wipe )
{
	// Then one thing we do is send the irrigation list over to this controllers irri machine.

	// 5/11/2012 rmd : Stations may or maynot be ON. For example a power fail during irrigation
	// presents us with active lists that we need to clean up. If we are in forced and
	// performing a periodic rescan, if the chain comes up new we are going to completely wipe
	// the irrigation. If we are performing our scan as part of a normal restart if the chain is
	// the same we just need to make sure all stations are OFF so we can start again.
	
	// ----------
	
	UNS_32	i;

	// ----------
	
	// 5/11/2012 rmd : Protect the lists as we traverse and change them.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// ----------
	
	// 11/13/2014 rmd : When the chain goes down we completely wipe the list.
	if( pcompletely_wipe )
	{
		// 5/11/2012 rmd : No dynamic memory to free in these lists. Permantely allocated battery
		// backed array of ilcs. Therefore re-initializing the lists WILL NOT LEAD TO MEMORY LEAKS.
		
		nm_ListInit( &foal_irri.list_of_foal_all_irrigation, offsetof( IRRIGATION_LIST_COMPONENT, list_support_foal_all_irrigation ) );
		
		nm_ListInit( &foal_irri.list_of_foal_stations_ON, offsetof( IRRIGATION_LIST_COMPONENT, list_support_foal_stations_ON ) );
	
		// 4/24/2012 rmd : Initialize the action needed list.
		nm_ListInit( &foal_irri.list_of_foal_stations_with_action_needed, offsetof( IRRIGATION_LIST_COMPONENT, list_support_foal_action_needed ) );
	
		// To make sure ALL the list links are 0. That is part of our test when finding an available
		// ilc to use.
		memset( &foal_irri.ilcs, 0, sizeof(foal_irri.ilcs) );

		// 9/24/2015 ajv : Clear the MVOR state as well - both FOAL- and IRRI-side since this
		// routine is called from both places when the chain goes down.
		for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
		{
			system_preserves.system[ i ].sbf.MVOR_in_effect_opened = (false);
			system_preserves.system[ i ].sbf.MVOR_in_effect_closed = (false);
			system_preserves.system[ i ].MVOR_remaining_seconds = 0;

			system_preserves.system[ i ].sbf.delivered_MVOR_in_effect_opened = (false);
			system_preserves.system[ i ].sbf.delivered_MVOR_in_effect_closed = (false);
			system_preserves.system[ i ].delivered_MVOR_remaining_seconds = 0;
		}
	}
	else
	{
		#if defined(LIST_DEBUG) && LIST_DEBUG
			// 8/10/2015 rmd : Now here is a very subtle issue that only can happen with battery backed
			// list headers! If there is a code restart (power failure or whatever) at just the right
			// time when one of the list INSERT or DELETE functions are executing for a list with a
			// battery backed list header, the IN_USE flag could be set true. Well upon a startup being
			// battery backed that flag is still set (true) and of course the list is not in use. And
			// with this flag SET (true) the list functions (insert or delete) will report a FATAL
			// error. So picture the flag is set. And the controller keeps trying to perform a list
			// insert or delete and that triggers a restart. The right thing to do is to set these
			// flags, rightfully so, (false) upon code startup.
			//
			// 8/10/2015 rmd : Note - I have verified, as of this point in time, these 3 list are the
			// only battery backed lists in the system. Ahh ... there is actually a fourth one. The
			// LIGHTS list. Its init function should have code that looks like this.
			foal_irri.list_of_foal_all_irrigation.InUse = (false);

			foal_irri.list_of_foal_stations_ON.InUse = (false);

			foal_irri.list_of_foal_stations_with_action_needed.InUse = (false);

		#endif
		
		// ----------
		
		// 11/5/2014 rmd : The goal here is to loop through the foal irrigation list. Turning OFF
		// any stations that are ON. Leaving the irrigation list intact meaning not removing the
		// ilcs from the list. Remember we want to preserve the irrigation through the power
		// failure. We do need to mark each ilc to send to the irri machines though. As their list
		// is wiped on a restart. If they even restarted.
		
		// ----------
		
		// 6/27/2012 rmd : For any loop that is turning off valves (1 or more valves) we enter the
		// loop with this variable (false). Then if lines need to be made we'll one batch of flow
		// recording lines for the group of valves ON. So set this false. If lines have already been
		// made (say at the flow checking mark) another variable manages suppressing the generation
		// of more lines.
		foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (false);
		
		IRRIGATION_LIST_COMPONENT	*lilc;
		
		lilc = nm_ListGetFirst( &(foal_irri.list_of_foal_all_irrigation) );
		
		while( lilc != NULL )
		{
			// 5/10/2012 rmd : Because of the potential list re-ordering when turning OFF.
			IRRIGATION_LIST_COMPONENT	*next_ilc;
			
			// 4/17/2012 rmd : If there was an irrigation list and irrigation was on-going station
			// could've been ON when the power failure occurred. So turn them OFF. Properly. If they
			// were ON the balance of their cycle is recovered within this function.
			next_ilc = nm_nm_FOAL_if_station_is_ON_turn_it_OFF( &(foal_irri.list_of_foal_all_irrigation), lilc, ACTION_REASON_INTERNAL_TURN_OFF_reboot_or_chain_went_down );
			
			// ----------
			
			// 5/11/2012 rmd : Leave the state of the orphan indication alone. When PROGRAMMED
			// irrigation starts all stations are made to look like orphans. And as irrigation occurs
			// that setting is cleared. If there was a power failure that is not a reason to undo the
			// orphan setting.
			
			// 4/24/2012 rmd : If this station was ON it is now on the action_needed list ... remove it!
			// This is an irrigation restart and past action requests are wiped. The irri side will be
			// receiving a copy of any ilcs in the list. And if the irri side didn't restart will
			// overwrite any existing copy. If the irri side indicates the valve is ON that gets wiped
			// in the process.
			if( nm_OnList( &foal_irri.list_of_foal_stations_with_action_needed, (void*)lilc ) )
			{
				if( nm_ListRemove( &foal_irri.list_of_foal_stations_with_action_needed, (void*)lilc ) != MIST_SUCCESS )
				{
					ALERT_MESSAGE_WITH_FILE_NAME( "Error removing from list" );
				}
			}
			
			// ----------
			
			// 5/11/2012 rmd : And under the conditions that this is called the irrigation list must be
			// redistributed to all the IRRI machines.
			lilc->bbf.xfer_to_irri_machines = (true);
			
			// ----------
			
			// 5/10/2012 rmd : And capture the next list item.
			lilc = next_ilc;
		}

	}
	
	// 5/9/2012 rmd : And indicate nothing is ON by controller.
	memset( &(foal_irri.stations_ON_by_controller), 0, sizeof(foal_irri.stations_ON_by_controller) );
	
	// ----------
	
	for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
	{
		// 8/21/2015 rmd : Cleanup the system preserves block with regards to irrigation. We used to
		// go overboard here and actually wipe system_preserves records. But we have no business
		// here in this function playing with what I consider system_preserves housekeeping and
		// further more such a system preserves wiping can be quite DANGEROUS. It leaves a
		// system_preserves structure unsynced to the system file. And that is an unexpected state
		// for the rest of the application! [Note: we saw the fall out of this. The POC preserves
		// tried to sync with the system_preserves in this state. And that led to a NULL system
		// preserves pointer in the POC preserves records. So stay within our bounds and wipe the
		// irrigation associated things in the system preserves records.
		nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds( &(system_preserves.system[ i ]) );
	}

	// -----------------------------

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
	
	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );

	// -----------------------------

	// And clear the wind_paused variable that controls if stations are going to be turned on or
	// not. Because of this on a power up we may actually ask an irri machine to turn on a
	// station...as soon as the wind machine reports in however we will distribute that and the
	// irri machine will ask that station be paused
	foal_irri.wind_paused = (false);
}

/* ---------------------------------------------------------- */
extern void FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation( void )
{
	// 11/26/2014 rmd : A function called from the COMM_MNGR context and KEY_PROCESS task
	// context. Really could be called from anytask. But is rarely and very specially used.
	// Generally during chain building when NEW controllers are found or when the user switches
	// a controller from being in a chain to a standalone.
	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down( (true) );
	
	IRRI_restart_all_of_irrigation();

	// ----------
	
	// 4/30/2014 rmd : TODO one very important thing to consider is how do we shut off the
	// two-wire decoders? Powering down the cable may be the easiest sure fire way to do this.
	// That's actually not needed. In the absence of an irri side irrigation list (we just
	// erased it), the message to the TPMicro will not contain any stations ON. And when sent to
	// the TPMicro is the way we tell the TPMicro to turn a station OFF. So they will all go
	// OFF. Terminal and 2Wire both.
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 8/6/2015 rmd : Do not exceed (BB_PRE_STRING_LENGTH - 1) visible characters. If you do you
// can end up changing the version number with no recognition of the change. Follow the code
// through to understand this.

// 4/27/2016 rmd : Moved from V03 to V04 when I added the mositure sensor decoder serial
// number variable to the end of the ILC definition.
static const char	FI_PRE_TEST_STRING[] = "foal_irri v04";

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The initialization function called on EVERY program start. It may be called
    a second time if the user requests a full restart.
	
	@RETURN (none)

	@EXECUTED Executed within the context of the STARTUP task.

	@AUTHOR 2011.10.21 rmd
*/ 
void init_battery_backed_foal_irri( const BOOL_32 pforce_the_initialization )
{
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );
	
	// ----------

	// 8/7/2015 rmd : If the string isn't intact signal we've failed.
	if( pforce_the_initialization || (strncmp( foal_irri.verify_string_pre, FI_PRE_TEST_STRING, sizeof(FI_PRE_TEST_STRING) ) != 0) )
	{
		Alert_battery_backed_var_initialized( "Foal Irri", pforce_the_initialization );
		
		// ----------

		// 11/11/2014 rmd : Zero the whole foal_irri structure.
		memset( &foal_irri, 0x00, sizeof(FOAL_IRRI_BB_STRUCT) );
		
		// ----------
		
		FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down( (true) );
		
		// ----------

		strlcpy( foal_irri.verify_string_pre, FI_PRE_TEST_STRING, BB_PRE_STRING_LENGTH );
	}
	else
	{
		#if ALERT_SHOW_ROUTINE_INITIALIZATIONS_DURING_STARTUP
			Alert_battery_backed_var_valid( "Foal Irri structure" );
		#endif

		// ----------
		
		// 2012.02.13 rmd : Coming from a power fail this function is executed. And you might expect
		// to reset aspects of the irrigation list.
		FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down( (false) );
	}

	/* ---------------------------------------------------------- */
	// AND THE STUFF WE ALWAYS INITIALIZE
	/* ---------------------------------------------------------- */
	
	// On program startup ONLY the rain and freeze timers should be created -
	// the other settings are to be preserved throughout a power failure else we would
	// get extra alert lines should a switch be active (first showing inactive
	// then active again).
	
	// 11/11/2014 rmd : TECHNICALLY making these two timers a second time as a result of when
	// the user requests a full reset will result in a memory leak. BUT a restart is triggered
	// as part of that effort and that cleans that small pile of poop up.
	foal_irri.timer_rain_switch = xTimerCreate( (const signed char*)"rain switch timer", MS_to_TICKS(WEATHER_RAIN_SWITCH_TIMEOUT_MILLISECONDS), (false), 0, rain_switch_timer_callback );

	if( foal_irri.timer_rain_switch == NULL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Timer NOT CREATED" );
	}

	// ----------
	
	foal_irri.timer_freeze_switch = xTimerCreate( (const signed char*)"freeze switch timer", MS_to_TICKS(WEATHER_FREEZE_SWITCH_TIMEOUT_MILLISECONDS), (false), 0, freeze_switch_timer_callback );

	if( foal_irri.timer_freeze_switch == NULL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Timer NOT CREATED" );
	}

	// ----------
	
	// 10/28/2014 rmd : And keep the unused expansion portion of the structure set to zeros. So
	// that in the future as one goes to use it [meaning they made some variables and reduced
	// the 'expansion' size accordingly] the new variables will start life as zero.
	memset( &foal_irri.expansion, 0, sizeof(foal_irri.expansion) );
	
	// ----------
	
	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION When stations are added to the foal irrigation list and also following a
    power failure during irrigation the stations in the ilc are marked to send to each of
    the irri machines in the network. When the outgoing token is made this function is
    called. This function will allocate and fill the needed memory. Setting the pointer
    included as a argument to the start of the block. And will return the length of the
    block. The length reflects the size of the block to be placed within the token. The
    length should be returned as 0 if the block is not to be sent. The memory block will be
    dynamically allocated and must be freed after the caller takes a copy of the data and
    implants it into the message.
	
	@CALLER MUTEX REQUIREMENTS (none) 

    @EXECUTED Executed within the context of the COMM_MNGR task.
	
    @RETURN Set the pdata_ptr to the start of the block if there is data to ship, NULL other
    wise. The return value is the length of the block to ship. And should return 0 if there
    is no data to include within the token.

	@AUTHOR 2012.03.06 rmd
*/
extern UNS_32 FOAL_IRRI_load_sfml_for_distribution_to_slaves( UNS_32 pcontact_index, UNS_8 **pdata_ptr )
{
	UNS_32	rv;
	
	// Indicate nothing to send by setting botht the returned length and pointer argument to
	// 0.
	rv = 0;
	
	*pdata_ptr = NULL;
	
	// 2012.03.06 rmd : We used to take this MUTEX and hold it the entire time the token was
	// being built. However now that we've re-structured the way we go through the foal irri
	// list we need only take it here and hold it for the duration of this function. We take it
	// because we are looping through the list and we do not want the list to change during our
	// activity.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );
	
	// 2012.03.05 rmd : We use an UNS_16 to save two bytes during the transmission.
	UNS_16	lnumber_of_stations;
	
	IRRIGATION_LIST_COMPONENT	*lilc_ptr;
	
	lnumber_of_stations = 0;

	lilc_ptr = nm_ListGetFirst( &foal_irri.list_of_foal_all_irrigation );
	
	while( lilc_ptr != NULL )
	{
		// 2012.03.06 rmd : Is there a request to send this station?
		if( lilc_ptr->bbf.xfer_to_irri_machines == (true) )
		{
			lnumber_of_stations += 1;
			
			// 2011.12.01 rmd : If we test the limit, that is all 768 station hit a start time and are
			// added to the list all at once and therefore all need xfer tot he irri machines we end up
			// with a roughly 16K token. That is VERY DARN large. We should manage that by limiting the
			// number of stations xferred at once. To say 48. That would roughly produce a 1K byte
			// token. And it would take 16 tokens to fully distribute the list. We should ALSO prohibit
			// attempted turn ON's when there are any ilc's in the list that needs xfer. If we didn't
			// take this step we could send a message to an IRRI machine to turn ON a station that
			// wasn't yet in his list. And that would be flagged as an error.
		}
		
		lilc_ptr = nm_ListGetNext( &foal_irri.list_of_foal_all_irrigation, lilc_ptr );
	}
	
	if( lnumber_of_stations > 0 )
	{
		// 2012.03.06 rmd : llength will also be used in a sanity check.
		UNS_32	llength;
		
		UNS_8	*ucp;
		
		// 2012.03.05 rmd : We send the station count short plus a special structure which includes
		// only the necessary pieces from the ilc to send.
		llength = sizeof(UNS_16) + (lnumber_of_stations * sizeof(STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT) );
		
		ucp = mem_malloc( llength );

		*pdata_ptr = ucp;  // set the pointer for the caller
		
		rv = llength;  // and set the return value now cause we know the length
		

		memcpy( ucp, &lnumber_of_stations, sizeof(UNS_16) );
		
		ucp += sizeof(UNS_16);
		
		llength -= sizeof(UNS_16);
		

		lilc_ptr = nm_ListGetFirst( &foal_irri.list_of_foal_all_irrigation );
		
		while( lilc_ptr != NULL )
		{
			if( lilc_ptr->bbf.xfer_to_irri_machines == (true) )
			{
				if( lnumber_of_stations == 0)
				{
					// This is not a fatal error but we'll note for now.
					ALERT_MESSAGE_WITH_FILE_NAME( "FOAL main list must have changed" );
					
					break;  // We are done! So we don't overrun the memory space allocated.
				}
				
				lnumber_of_stations -= 1;
				
				STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT	sds;
				
				sds.system_gid = lilc_ptr->bsr->system_gid;
				sds.box_index_0 = lilc_ptr->box_index_0;
				sds.station_number_0_u8 = lilc_ptr->station_number_0_u8;
				sds.reason_in_list_u8 = lilc_ptr->bbf.w_reason_in_list;
				sds.requested_irrigation_seconds_u32 = lilc_ptr->requested_irrigation_seconds_ul;
				sds.cycle_seconds = lilc_ptr->cycle_seconds_ul;
				sds.soak_seconds_remaining = lilc_ptr->soak_seconds_remaining_ul;
				
				memcpy( ucp, &sds, sizeof(STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT) );
				
				ucp += sizeof( STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT );
				
				// 5/2/2012 rmd : Reset the request. One included in one token. Remember the token is
				// broadcast.
				lilc_ptr->bbf.xfer_to_irri_machines = (false);
				
				// adjust the remaining length for post sanity check
				llength -= sizeof( STATION_FROM_MAIN_LIST_DISTRIBUTION_STRUCT );
			}
			
			lilc_ptr = nm_ListGetNext( &foal_irri.list_of_foal_all_irrigation, lilc_ptr );

		}  // of looping through the entire list
		
		if( lnumber_of_stations || llength )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_IRRI: token info error" );
		}

	}  // of if there are stations to send

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );

	return( rv );
}

/*--------------------------------------------------------------------------
	Name: 			unsigned long FOAL_CycleAndSoakWeighting( IRRIGATION_LIST_COMPONENT *pilc )
	
	Description:    This function is called to determine the weighting as calculated by the cycle and soak
					equations. It shoudl only be used for a station in the list for PROGRAMMED IRRIGATION.
					
	Parameters:     pilc		pointer to the station (who CANNOT BE IN THE IRRIGATIONLIST)
	
	Return Values:  an unsigned long representative of the weighting
	  
	Date:           rmd 02/02/2002
	
 
 -------------------------------------------------------------------------*/
static UNS_32 foal_irri_cycle_and_soak_weighting( BOOL_32 const pfor_ftimes, void *const p_ptr )
{
	FT_STATION_STRUCT	*pft_station_ptr;

	IRRIGATION_LIST_COMPONENT	*pilc_station_ptr;

	UNS_32	t, c, s, rv;
	
	// ----------
	
	rv = 0;
	
	if( pfor_ftimes )
	{
		pft_station_ptr = p_ptr;
		
		t = pft_station_ptr->requested_irrigation_seconds_balance_ul;

		// 7/30/2018 rmd : The cycle seconds used during irrigation has been set prior to adding to
		// the irrigating stations list.
		c = pft_station_ptr->cycle_seconds_used_during_irrigation;

		s = pft_station_ptr->soak_seconds_used_during_irrigation;
	}
	else
	{
		pilc_station_ptr = p_ptr;
		
		t = pilc_station_ptr->requested_irrigation_seconds_ul;

		c = pilc_station_ptr->cycle_seconds_ul;

		s = pilc_station_ptr->soak_seconds_ul;
	}
	
	// ----------
	
	// Protect against divide by zero - which should never happen.
	if( c == 0 )
	{
		Alert_Message( "Cycle=0 (out of range)" );

		rv = 0;
		
	}
	else
	if( t == 0 )
	{
		// This is an error condition. Well not exactly. When a station in the FOAL list turns off
		// its a 2 step process. First the station does a normal turn off - which causes it to be
		// sorted. If it was the last cycle we would be sorting a station with no time on it. This
		// is not an error. It just happen in the order of things. After the normal turn-off the
		// Irri machine will signal the FOAL machine to remove the station from the list (KILL).
		//
		//Alert_Error( "FOAL: weighting with no irrigation time", FALSE );
		
		rv = 0;
	}
	else
	{
		// The weighting equation. If the time remaining is less than or equal to the cycle time the
		// weighting should be 0 because we have no soaking to do only irrigation. I dont know of a
		// reason why a station with 2 minutes left (1 cycle) is more or less important than a
		// station with 3 minutes left (1 cycle) - so I will give them equal weighting. Notice we
		// are working with integers and therefore only using the integer portion of the division -
		// its only the total soak time remaining that we are interested in.
		if( t <= c )
		{
			rv = 0;
		}
		else 
		if( ( t % c ) == 0 )
		{
			// then the amount of soak is the integer number of cycles - 1
			rv = ( ( t / c ) - 1 ) * s;
		}
		else
		{
			// then the amount of soak is the integer number of cycles
			rv = ( t / c ) * s;
		}
		
	}
	
	return( rv );
}

/* ---------------------------------- */
static UNS_32 _nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting( BIG_BIT_FIELD_FOR_ILC_STRUCT const *const pbbf_ptr )
{
	UNS_32	rv;

	rv = 0x00000000;
	
	// ----------
	
	if( pbbf_ptr->w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION ) {

		// 7/16/2015 rmd : The didn't irrigate last time term is for Programmed_Irrigation only. And
		// as it turns out if didn't irrigate last time is the most important issue - put it first
		// in the list.
		if( pbbf_ptr->w_did_not_irrigate_last_time )
		{
			rv += 0x00010000;
		}

		// ----------
		
		// 7/16/2015 rmd : (605.a ajv) Program priority is next - since there are different priority
		// levels, we must add varying values to the return value depending up the priority. NOTE:
		// program priorities are only available for Programmed Irrigation as of this moment.
		// Therefore, we only look at this value if it is in the list for this reason
		if( pbbf_ptr->station_priority == PRIORITY_HIGH )
		{
			rv += 0x00007000;
		}
		else
		if( pbbf_ptr->station_priority == PRIORITY_MEDIUM )
		{
			rv += 0x00006000;
		}
		else
		if( pbbf_ptr->station_priority == PRIORITY_LOW )
		{
			rv += 0x00005000;
		}
	}

	// ----------
	
	// Pump use next - this tends to make pump stations go first but what about the
	// WhatAreWeTurningOn_uc term that overrides pump going first.
	if( pbbf_ptr->w_uses_the_pump )
	{
		rv += 0x00000100;
	}
	
	// This weighting is unecessary as FOAL_maintain irri list function makes sure set expected
	// station irrigate first and by themselves - this does make the FOAL ilc debug list more
	// readable though.
	if( pbbf_ptr->w_to_set_expected )
	{
		rv += 0x00000010;
	}
	
	// 6/1/2012 rmd : By qualifying the addition of the flow check group value we are removing
	// the flow check group weighting from the list ordering when the valve is ON for a reason
	// (WALK-THRU say) we do not perform flow checking.
	if( pbbf_ptr->flow_check_when_possible_based_on_reason_in_list )
	{
		// So now based on the flow group he belongs to we are going to add from between 0 to 3.
		// This is not a critical ordering cause the turn ON rules will not allow mixed flow
		// checking groups ON at the same time - what this does do is to let you have an idea of
		// which groups will go first (hi flow testing valves go first - low next - and finally
		// those with no testing at all at the end).
		rv += (pbbf_ptr->flow_check_group);  // adds 0, 1, 2, or 3
	}
	
	// ----------
	
	return( rv );
}

/*--------------------------------------------------------------------------
	Name:           BOOLEAN TheListItemsWeighsLESSThanTheOneToAdd( IRRIGATION_LIST_COMPONENT *list_item, IRRIGATION_LIST_COMPONENT *toadd_item )
	
	Description:    This function is called as we add to the irrigation list. It is
					only called from the foal machine when adding to its list. The irrigation
					list NEVER decides when a station is to turn on. The FOAL machine ALWAYS
					makes that decision - no matter what mode we are in (standalone, POAFS, or whatever).
					
	Parameters:     list_item		pointer to the list station
					add_item		pointer to the station to add
					
	Return Values:  TRUE if the list station weighs less than the station to add
	  
	Date:           rmd 02/15/01
	
 
 -------------------------------------------------------------------------*/
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used when figuring where in the list an ilc fits. The list is ordered by
    our invented weighting. And this is the core function that calulates the weight
    value.

    @CALLER MUTEX REQUIREMENTS None. This function takes and releases the
    foal_irri_list_mutex.
	
    @EXECUTED Executed within the context of the TD_CHECK task. And the COMM_MNGR task as
    stations are added to the list via incoming messages.
	
    @RETURN (true) if the add station weighs more than the list station. (false) if not.

	@AUTHOR 2011.10.28 rmd
*/
static BOOL_32 nm_this_station_weighs_MORE_than_the_list_station( BOOL_32 const pfor_ftimes, void *const pstation_to_add, void *const pstation_in_list )
{
	UNS_8	add_in_list_for, list_in_list_for;
	
	UNS_32	add_weighting, list_weighting;

	BOOL_32	add_involved_in_a_flow_problem, list_involved_in_a_flow_problem;
	
	FT_STATION_STRUCT	*ft_station_to_add, *ft_station_in_list;
	
	IRRIGATION_LIST_COMPONENT	*station_to_add, *station_in_list;
	
	BIG_BIT_FIELD_FOR_ILC_STRUCT	*add_bbf_ptr, *list_bbf_ptr;
	
	// ----------
	
	// ONLY Programmed Irrigation concerns itself with weighting?? WRONG! Everybody who goes in the list
	// has some sort of ordering rules to control their position in the list. First order is if they are in the
	// PROBLEM LIST. That takes priority and bumps stations to the head of the irrigation list. Second is the
	// reason they are in the list. Third there may be some weighting associated with each reason in the list 
	// (cycle&soak and hold-over weighting). 
	//
	// Expected flow rate weighting is associated only with the problem list stations. This ordering
	// helps us to get the stations with highest flows in the problem list to come on first. We know we want that 
	// when we are working the problem list we want to minimize the number of new valves (two issues - more new valves
	// tend to pollute the problem list and more importantly if one of the new valves has a problem we will falsely
	// identify a valve as having an overflow). Well see how all this plays out.


	// FIRST FIRST FIRST 
	// The problem list weighting
	// If the one to add is on the problem list he carrys a lot of weight. Remember they both could be on the
	// problem list. In that case their expected flows will determine weighting. While on the problem list
	// cycle and soak weighting is not used. This was a decision made to minimize the number of other valves
	// that come on when irrigating a problem list valve but I don't know if it really helps - it may simply
	// delay the inevitable - but perhaps gives us a chance to find the problem valve while introducing the
	// minimum number of new valves into the mix.
	//
	// The rules here are actually complicated but we are going to code them so they are
	// easy to understand. The groupings are as follows:
	// 1) Pump is the high order sort
	// 2) Set Expected next (by flow rate in this group)
	// 3) Regular Irrigation left sort by cycle and soak weighting
	//
	// Within the Pump valves we order on the following:
	// 1) SetExpected valves come first (sorted by flow rate)
	// 2) Didn't irrigate last time valves next (by Cycle&SoakWeighting - which handles HO time)
	// 3) Finally whats left by Cycle&SoakWeighting - which handles HO time
	//
	// Within the Non-Pump valves we must order on the following:
	// 1) SetExpected valves come first (sorted by flow rate)
	// 2) Didn't irrigate last time valves next (by Cycle&SoakWeighting - which handles HO time)
	// 3) Finally whats left by Cycle&SoakWeighting - which handles HO time

	
	// We have the weight of the list we are comparing against as well as the weight of the
	// item we are trying to add. Normally we sort them by PUMP / NON-PUMP however for the
	// case of the WALK-THRU we don't want to disturb the walk-thru sequence and order in
	// the PUMP / NON-pump fashion.
	
	// ----------

	// 3/15/2018 rmd : Transition from generic pointers to context specific.
	ft_station_to_add = pstation_to_add;

	ft_station_in_list = pstation_in_list;
	
	station_to_add = pstation_to_add;

	station_in_list = pstation_in_list;
	
	// ----------
	
	// start by comparing the reason in the list ... that's our highest ordering criteria

	if( pfor_ftimes )
	{
		add_in_list_for = ft_station_to_add->bbf.w_reason_in_list;

		list_in_list_for = ft_station_in_list->bbf.w_reason_in_list;

		add_involved_in_a_flow_problem = (false);

		list_involved_in_a_flow_problem = (false);

		add_bbf_ptr = &ft_station_to_add->bbf;

		list_bbf_ptr = &ft_station_in_list->bbf;
	}
	else
	{
		add_in_list_for = station_to_add->bbf.w_reason_in_list;

		list_in_list_for = station_in_list->bbf.w_reason_in_list;

		add_involved_in_a_flow_problem = station_to_add->bbf.w_involved_in_a_flow_problem;

		list_involved_in_a_flow_problem = station_in_list->bbf.w_involved_in_a_flow_problem;

		add_bbf_ptr = &station_to_add->bbf;

		list_bbf_ptr = &station_in_list->bbf;
	}

	// ----------
	
	if( add_in_list_for > list_in_list_for )
	{
		return( (true) );
	}
	else
	if ( add_in_list_for < list_in_list_for )
	{
		// return FALSE so the caller will continue to move through the irrigation list until either the
		// end of the list or the one to add is greater than the list station
		return( (false) );
	}
	
	// ----------
	
	if( !pfor_ftimes )
	{
		// SMS_SEQUENCE valves are special...they should be added to the end of their group regardless of
		// pump, blah, blah, and blah (all other considerations are forgotten)...they just wait till the
		// end of their group and since we would be in the middle of the group return FALSE at this point
		// for the sms valves to cause the caller to proceed through the list
		if( station_to_add->bbf.w_reason_in_list == IN_LIST_FOR_WALK_THRU )
		{
			return( (false) );
		}
	}
	
	// ----------
	
	// so we now know we are within a group in the list all in the list for the same reason
	//
	// the first item to consider is if they are on the flow problem list as we want to put them up front in the group
	// what about ordering with the problem list group...they should be ordered so that the last ones added are added
	// to the end of the problem list group...that means if the one to add is on the problem list we should hunt until
	// we see a station not in the problem list and insert BEFORE that one (which is the way the functions work)
	if( add_involved_in_a_flow_problem )
	{
		if ( list_involved_in_a_flow_problem )
		{
			// this list item is also on the problem list and the new one should be inserted at the end of the
			// one on the problem list
			return( (false) );
		}
		else
		{
			// okay the list item is here for the same in the list reason and it is not on the problem list so we
			// we can stop else we must evaluate the next one in the list
			return( (true) );
		}
	}
	
	
	// so we are within a group and the one to add is NOT on the problem list...first make sure the list item is not on
	// the problem list...if it is we need to continue on down through the list
	if( list_involved_in_a_flow_problem )
	{
		return( (false) );
	}
	
	// ----------
	
	// now compare about whether or not it uses the pump, about if its in the list to set expected, AND about
	// did it irrigate last time...the function returns a number that we can compare
	add_weighting = _nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting( add_bbf_ptr );

	list_weighting = _nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting( list_bbf_ptr );
	
	if( add_weighting > list_weighting )
	{
		return( (true) );
	}
	else
	if( add_weighting < list_weighting )
	{
		return( (false) );
	}
	else
	{
		// 10/5/2012 rmd : Here because the weighting is equal. And ONLY for the case of programmed
		// irrigation do we jigger the list to reflect the cycle & soak weighting. ALL other reasons
		// to irrigate order the list according to the order they brought up to be added to the
		// list. David Byma and I decided yesterday that manual programs should not soak between
		// cycles. If the guy stacks starts the stations should irrigate sequentially and the
		// immediately start over. No soak time effect.
		//
		// 7/16/2015 rmd : And Richard agrees. No soak during manual programs. And as much as
		// possible irrigate in numberical order.
		if( add_bbf_ptr->w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
		{
			if( foal_irri_cycle_and_soak_weighting( pfor_ftimes, pstation_to_add ) > foal_irri_cycle_and_soak_weighting( pfor_ftimes, pstation_in_list ) )
			{
				return( (true) );
			}
			else
			{
				return( (false) );
			}
		}
		else
		{
			// If the weighting is equal we want the order to be the order they were added to the list.
			// This FALSE will keep newcomers at the end of their group.
			return( (false) );
		}
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Loop through the list till either the list ends or the calculated priority
    level of the next item is lower than the than the one we are comparing to. PILC MUST NOT
    BE IN THE IRRIGATION LIST. After all we are trying to find where he belongs in the list
    and if we bump into ourselves we may not return the right answer. He should've been
    removed because his order in the list is suspected to no longer be valid.

    @CALLER MUTEX REQUIREMENTS None. This function takes and releases the
    foal_irri_list_mutex.
	
    @EXECUTED Executed within the context of the TD_CHECK task. And the COMM_MNGR task as
    stations are added to the list via incoming messages.
	
    @RETURN The ilc before which to place the ilc of interest (pilc). Or NULL if it is to go
    at the end of the list.

	@AUTHOR 2011.10.28 rmd
*/
static void *nm_return_station_this_station_belongs_before_in_the_irrigation_list( BOOL_32 pfor_ftimes, MIST_LIST_HDR_TYPE *pptr_irrigation_list_hdr, void *plist_item )
{
	// 3/15/2018 rmd : As needed, the caller should be holding the appropriate list MUTEX. This
	// function has been modified to treat the list items as a generic structure in oder to
	// support ftimes.
	
	// 7/16/2015 rmd : Looking at the irrigation look move down through it till we encounter
	// someone with less priority than pilc has. At that point exit. Goal is to leave ilc either
	// NULL or pointing to the first element with a reason greater than the passed ilc reason.
	
	void *llist_item;
	
	llist_item = nm_ListGetFirst( pptr_irrigation_list_hdr );

	while( llist_item != NULL )
	{
		// The irrigation list is ordered by a "reason in list" priority. The lower the reason
		// number the lower the priority and the later in the list the item should occur.
		// The goal is to leave ilc either NULL or pointing to the first element with a reason on 
		// less than than the passed ReasonOn.
		//
		// Everybody can have weighting. Let's see how the two compare. If the weighting of the
		// station to add is more than the station in the list we're done.
		if( nm_this_station_weighs_MORE_than_the_list_station( pfor_ftimes, plist_item, llist_item ) )
		{
			break;  // out of the while loop
		}

		llist_item = nm_ListGetNext( pptr_irrigation_list_hdr, llist_item );
	}
	
	// A NULL causes the new one to be inserted at the end of list (or at the head if the list
	// is empty)
	return( llist_item );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function is used to re-sequence the irrigation list after we freshly
    add an ILC to the list. Also used to update ordering after an irrigation cycle
    completes. This is safe to do even if the item is ON. Or on other lists. Such as the
    problem list. Because all we are doing is didling the links of the list items. Not
    altering how many are in the list or the memory associated with each item. All other
    lists the item is a member of are unaffected by this operation.

    @CALLER MUTEX REQUIREMENTS None. This function takes and releases the
    foal_irri_list_mutex.
	
    @EXECUTED Executed within the context of the TD_CHECK task. May also get executed within
    the context of the COMM_MNGR as an irri machine makes a request to turn off a station.
	
	@RETURN (none)

	@AUTHOR 2011.10.28 rmd
*/
extern void FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list( BOOL_32 pfor_ftimes, void *pstation_list_element )
{
	// 10/22/2014 rmd : End result is ilc is still in the list. So do not use formal remove from
	// list function call during this function.

	void	*list_element;

	if( pfor_ftimes == FOR_FTIMES )
	{
		nm_ListRemove( &ft_irrigating_stations_list_hdr, pstation_list_element );
		
		// Now that we know its NOT in the FOAL Irrigation list we can call this.
		list_element = nm_return_station_this_station_belongs_before_in_the_irrigation_list( FOR_FTIMES, &ft_irrigating_stations_list_hdr, pstation_list_element );

		// Now insert it BEFORE the ilc already on the list - if ilc is NULL it goes at the end of
		// the list.
		nm_ListInsert( &ft_irrigating_stations_list_hdr, pstation_list_element, list_element );
	}
	else
	{
		// Protect the list from changes while we work with it.
		xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );
	
		// 10/22/2014 rmd : Remove it from the Irrigation list so we can find out where it belongs.
		// The act of removing happens to verify for us that the ilc is on the list too. Which is a
		// good thing.
		if( nm_ListRemove( &foal_irri.list_of_foal_all_irrigation, pstation_list_element ) != MIST_SUCCESS )
		{
			Alert_Message( "Couldn't remove from irri list to reinsert" );
		}
		else
		{
			// Now that we know its NOT in the FOAL Irrigation list we can call this.
			list_element = nm_return_station_this_station_belongs_before_in_the_irrigation_list( NOT_FOR_FTIMES, &foal_irri.list_of_foal_all_irrigation, pstation_list_element );
	
			// Now insert it BEFORE the ilc already on the list - if ilc is NULL it goes at the end of
			// the list.
			nm_ListInsert( &foal_irri.list_of_foal_all_irrigation, pstation_list_element, list_element );
		}
	
		// Release.
		xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
	/*
		* Resolved issue with flow recorder lines being created where,
		  when multiple stations closed within the same second, the
		  lines would be repeated after each valve was turned off.  For
		  instance, if station A1, A2, A3 and A4 were all scheduled to
		  turn off at the same time, flow recorder lines would be
		  created listing A1, A2, A3, A4, A2, A3, A4, A3, A4, A4 all
		  within the same second.  The problem stemmed from the fact
		  that each time a valve was turned off, a new set of flow
		  recorder lines were stamped.
	*/
	// 608.rmd
	// See that comment above. I extracted it from main.c comment listing. It was added in 602.a
	// 11.08.2005 - likely by AJ. We figure that as the approach to use the time & date to solve
	// this problem isn't what I would refer to as a seasoned or elegant approach. For example
	// if communciations reaches the point speed wise where more than one token response per
	// second arrives this won't work. How close are we to that now? Does the coder know? But it
	// doesn't really matter who added it - it is what it is for now (and it seems to work - for
	// now). To note, I think rather the production of lines ought to be governed by a "one
	// group of lines per token response" algorithm.  But we'll leave it alone for now.
	//
	// 2012.01.09 rmd : Maybe the variable below about flow recording line made since last turn
	// ON is the kind of tracking we need to control this phenomenon.
	DATE_TIME	last_time_a_valve_was_turned_off;

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called on-demand as events occur that dictate we need to turn stations OFF.
    This can be a regular time has expired issue during regular list maintenance. Or a STOP
    key command has been recevied by some means (key or comm). Or a flow checking error
    requires this station to turn OFF. The list to get the next item in is included to allow
    the caller to use this function when traversing either the ON list or the main foal_irri
    irrigation list.

    BEWARE if the caller is traversing the main irrigation list when calling this function.
    One of the last steps is to remove the station from the main list and re-insert in
    according to the weighting. We do this because as irrigation occurs the weighting
    changes (due to number of cycles remaining) and the list should be in order of
    weighting. Which controls the turn ON order. To resolve this issue this function returns
    the next ilc in the list prior to the re-ordering. And the caller may use this as the
    next in the list if the call to this function is inside a loop traversing the main
    irrigation list. There are some exceptions to re-ordering that can be viewed at that
    section in the function.

    @CALLER MUTEX REQUIREMENTS The caller should be holding TWO MUTEXES. The foal_irri_MUTEX
    to protect the pilc list item and the list itself from changes. And the system_preserves
    MUTEX as we are going to set some timers within the system preserves.

    @EXECUTED Executed within the context of the COMM_MNGR task. When a token response is
    parsed contain a request to terminate stations. Also within the context of the TD_CHECK
    task as part of routine list maintenance.

    @RETURN A pointer to the next ilc in the plist_hdr list. Which should be either the ON
    list or the main foal_irri irrigation list. Or NULL if the pilc_ptr is NOT on the
    foal_irri irrigation list.

	@AUTHOR 2012.05.10 rmd
*/
extern IRRIGATION_LIST_COMPONENT* nm_nm_FOAL_if_station_is_ON_turn_it_OFF( MIST_LIST_HDR_TYPE_PTR plist_hdr, IRRIGATION_LIST_COMPONENT *const pilc_ptr, UNS_32 const paction_reason )
{
	IRRIGATION_LIST_COMPONENT	*rv;
	
	BOOL_32		inhibit_next_turn_on;

	// ----------
	
	rv = NULL;
	
	// ----------
	
	// 11/5/2014 rmd : Regardless of the nm_nm_ designation on this function call take the
	// appropriate MUTEXES in case the caller did not. They are recursive therefore this is okay
	// to do and results in a small hit to code size and performance to do so.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// ----------
	
	// 5/10/2012 rmd : Must be on the irrigation list.
	if( nm_OnList( &(foal_irri.list_of_foal_all_irrigation), pilc_ptr ) == (false) )
	{
		//ALERT_MESSAGE_WITH_FILE_NAME( "FOAL: pilc not on main list!" ); 
		Alert_item_not_on_list( pilc_ptr, foal_irri.list_of_foal_all_irrigation );  
	}
	else
	{
		// 5/10/2012 rmd : To support those callers who are actually traversing the main ilc list.
		// The re-ordering dictates we take this action. That is get the next and return it to the
		// caller. The result of the re-ordering may be the caller actually bumps into this list
		// item AGAIN further down the list. But the station will already be OFF and this is okay.
		// The caller will get through the whole list without missing any stations. Even in the
		// presences of the re-ordering! We pass the list argument because sometimes the caller is
		// in the process of traversing the ON list as opposed to the main irri list.
		rv = nm_ListGetNext( plist_hdr, pilc_ptr );
		
		// 5/10/2012 rmd : In some cases we do not actually know if the station is ON. And this
		// function is called as a cleanup measure before the list item is to be abruptly deleted
		// from the list. In other cases the station is expected to be ON such as due to a flow
		// check violation or even simply just a normal turn OFF cause out of time.
		if( pilc_ptr->bbf.station_is_ON == (true) )
		{
			if( foal_irri.flow_recording_group_made_during_this_turn_OFF_loop == (false) )
			{
				// 6/5/2012 rmd : Performing a station turn off in effect starts a new group irrigating and
				// if that group doesn't make a flow recording line we should make one ... the dilema comes
				// with what to do with FoalFlow.flow recording lines have been made since the last valve
				// transition ... if we do nothing for each valve that turns off we may make a new flow
				// recording line ... if we set it TRUE here then we'll only get one line at the first valve
				// that turns off in the group of those ON ... and not another line until another valve turn
				// ON. We have decided not to set it (true).
				if( pilc_ptr->bsr->sbf.checked_or_updated_and_made_flow_recording_lines == (false) )
				{
					FLOW_RECORDER_FLAG_BIT_FIELD	flow_recorder_flag;
					
					// 5/1/2012 rmd : Clear all bits and values.
					flow_recorder_flag.overall_size = 0;
			
					// 6/15/2012 rmd : On a firmware restart (powerfail) any stations that were ON are turned
					// OFF. And we can mark their flow recording lines about this. It is sort of an additional
					// reason which may explain what is happening. This would also happen if the chain goes
					// down.
					if( paction_reason == ACTION_REASON_INTERNAL_TURN_OFF_reboot_or_chain_went_down )
					{
						flow_recorder_flag.FRF_NO_CHECK_REASON_reboot_or_chain_went_down = (true);
					}

					// 6/5/2012 rmd : Use the flag that states this ilc should be checking flow. Now this flag
					// is intended primarily for use during programmed irrigation. But it is actually valid for
					// any reason in the list. And is set when the ilc is added to the list. It effectively
					// means this valve should be checking flow.
					if( pilc_ptr->bbf.at_some_point_should_check_flow && !pilc_ptr->bbf.flow_check_to_be_excluded_from_future_checking )
					{
						// Okay we've been waiting to check flow but never did - because of that we never made a
						// flow recording line about this (or these) valves ever being on - so make one.
						flow_recorder_flag.flow_check_status = FRF_CHECK_STATUS_not_going_to_perform;
					
						// 6/14/2012 rmd : While the following logic may notbe complete it is our best shot at
						// summarizing why flow checking did not happen. The cycle time too short is the catch all
						// reason. Assuming some timer had not yet expired. The unstable flow is the first one we
						// check though. And that reason would perhaps hide any tiemrs that hadn't expired. It's all
						// a matter of how elaborate we want to be here.
						if( pilc_ptr->bsr->sbf.stable_flow == (false) )
						{
							flow_recorder_flag.FRF_NO_CHECK_REASON_unstable_flow = (true);

							// 10/24/2014 ajv : Jim has requested that this
							// alert be added to the user pile. However, we're
							// not sure how prevalent this alert is so perhaps
							// we'll start with only including it in the
							// Engineering alerts.
							Alert_flow_not_checked_with_reason_idx( AID_FLOW_NOT_CHECKED__UNSTABLE_FLOW, pilc_ptr->box_index_0, pilc_ptr->station_number_0_u8 );
						}
						else
						{
							flow_recorder_flag.FRF_NO_CHECK_REASON_cycle_time_too_short = (true);

							// 10/24/2014 ajv : Jim has requested that this
							// alert be added to the user pile. However, we're
							// not sure how prevalent this alert is so perhaps
							// we'll start with only including it in the
							// Engineering alerts.
							Alert_flow_not_checked_with_reason_idx( AID_FLOW_NOT_CHECKED__CYCLE_TOO_SHORT, pilc_ptr->box_index_0, pilc_ptr->station_number_0_u8 );
						}
					}
					else
					{
						// 6/5/2012 rmd : Well at this point there can be quite a few reasons why we didn't check
						// flow and make a flow recording line. But these reasons are not reason like stability
						// wasn't met. Or the cycle time wasn't long enough. They are more along the lines of there
						// are no flow meters in the system. Flow checking is disabled. The reason in the list
						// doesn't want flow checking. Things like that.
						flow_recorder_flag.flow_check_status = FRF_CHECK_STATUS_not_going_to_perform;
					
						if( pilc_ptr->bsr->sbf.flow_checking_enabled_by_user_setting == (false) )
						{
							flow_recorder_flag.FRF_NO_CHECK_REASON_not_enabled_by_user_setting = (true);
						}
						else
						if( pilc_ptr->bsr->sbf.number_of_flow_meters_in_this_sys == 0 )
						{
							flow_recorder_flag.FRF_NO_CHECK_REASON_no_flow_meter = (true);
						}
						else
						{
							// 6/5/2012 rmd : A general catch all. If we're interested in the future we can develop more
							// specific reasons.
							flow_recorder_flag.FRF_NO_CHECK_REASON_not_supposed_to_check = (true);
						}
					
					}
					
					FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines(	pilc_ptr->bsr,
																						FLOW_RECORDING_OPTION_NO_DONT_mark_as_flow_checked,			// zero flow problem count
																						FLOW_RECORDING_OPTION_NO_DONT_stamp_the_report_rip,			// stamp the report rip
																						FLOW_RECORDING_OPTION_YES_make_flow_recording_lines, 		// make flow recorder line
																						flow_recorder_flag,   										// flow_recorder_flag_uc
																						pilc_ptr->bsr->system_master_most_recent_5_second_average	// system_actual_flow_us
																					);
																					
				}
		
				// ------------------------------			
				
				// 6/21/2012 rmd : And even if we did not actually make any lines set the flag true that we
				// did. Otherwise, while in a loop turning OFF, the first valve OFF actually made a new
				// group. So the flag saying we made lines for this group is cleared (just below here). So
				// the next valve OFF would then make the lines again. And that is the whole point of this
				// flag. To prevent that!
				foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (true);

			}  // of flow records not made this pass	
			
			// ------------------------------			
			
			// 6/5/2012 rmd : SYSTEM level flow checking type flags that are cleared. NOW this seems
			// really strange to do but ... in effect by turning off a station we have made a new group
			// and if we don't turn another new valve ON we will not stamp a flow recording line showing
			// this group and and other groups until we turn a new valve ON again. So ...
			pilc_ptr->bsr->sbf.checked_or_updated_and_made_flow_recording_lines = (false);
			
			pilc_ptr->bsr->sbf.system_level_no_valves_ON_therefore_no_flow_checking = (false);
			pilc_ptr->bsr->sbf.system_level_valves_are_ON_but_will_not_be_checking_flow = (false);
			pilc_ptr->bsr->sbf.system_level_valves_are_ON_and_waiting_to_check_flow = (false);
			pilc_ptr->bsr->sbf.system_level_valves_are_ON_and_actively_checking = (false);
			pilc_ptr->bsr->sbf.system_level_valves_are_ON_and_waiting_to_update_derate_table = (false);
			pilc_ptr->bsr->sbf.system_level_valves_are_ON_and_has_updated_the_derate_table = (false);
			pilc_ptr->bsr->sbf.system_level_valves_are_ON_and_waiting_to_acquire_expected = (false);
			
			// ------------------------------			
			
			// 5/11/2012 rmd : Make sure the index is within range as we are going to use it 'blindly'
			// to index an array. If out of range a bug like that could cause a crash and be VERY hard
			// to find.
			if( RangeCheck_uns8( &(pilc_ptr->box_index_0), BOX_INDEX_MINIMUM, BOX_INDEX_MAXIMUM, BOX_INDEX_DEFAULT, NULL, "Box Index" ) )
			{
				if( foal_irri.stations_ON_by_controller[ pilc_ptr->box_index_0 ] == 0 )
				{
					ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_IRRI: expected count is 0." );
				}
				else
				{
					foal_irri.stations_ON_by_controller[ pilc_ptr->box_index_0 ] -= 1;
				}
			}
	
			// ------------------------------			
			
			// List remove will remove the item if it is on the list. If not on the list will generate
			// an error message. So test first. AND we can remove from this list even if the caller is
			// traversing this ON list cause we already have obtained the next in the list!
			if( nm_OnList( &foal_irri.list_of_foal_stations_ON, (void*)pilc_ptr ) )
			{
				if( nm_ListRemove( &foal_irri.list_of_foal_stations_ON, (void*)pilc_ptr ) != MIST_SUCCESS )
				{
					ALERT_MESSAGE_WITH_FILE_NAME( "FOAL: error removing from ON list" );
				}
		
			}
			else
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "FOAL: should've been on the ON list." );
			}
	
			// ----------
			
			// 11/4/2014 rmd : Always add to the action reason list. If the caller doesn't desire this
			// he would have to discretely remove himself. Which is not allowed!
			nm_FOAL_add_to_action_needed_list( pilc_ptr, paction_reason );
			
			// ----------
			
			// 9/9/2014 rmd : Do NOT move this relative to the SLOW-CLOSING valve timer logic below. We
			// count on this value being updated first before that logic is executed.
			if( pilc_ptr->bsr->system_master_number_of_valves_ON > 0 )
			{
				pilc_ptr->bsr->system_master_number_of_valves_ON -= 1;
			}
			else
			{
				// 5/30/2012 rmd : Apparently the number ON is 0.
				Alert_Message( "FOAL_IRRI: number_of_valves_ON unexpectedly zero." );
			}

			// ------------------------------			

			if( pilc_ptr->bsr->ufim_expected_flow_rate_for_those_ON >= pilc_ptr->expected_flow_rate_gpm_u16 )
			{
				pilc_ptr->bsr->ufim_expected_flow_rate_for_those_ON -= pilc_ptr->expected_flow_rate_gpm_u16;
			}
			else
			{
				Alert_Message( "FOAL_IRRI: system expected flow rate out of range." );
				
				// 5/30/2012 rmd : Zero it. Couldn't think of anything else to do.
				pilc_ptr->bsr->ufim_expected_flow_rate_for_those_ON = 0;
			}
	
			// ------------------------------			

			if( pilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_TEST )
			{
				if( pilc_ptr->bsr->ufim_number_ON_during_test > 0 )
				{
					pilc_ptr->bsr->ufim_number_ON_during_test -= 1;
				}
				else
				{
					// 5/30/2012 rmd : Apparently the number ON for test is 0.
					Alert_Message( "FOAL_IRRI: number_ON_for_test unexpectedly zero." );
				}
			}
			
			// ----------
			
			// 5/30/2018 rmd : This is CRITICAL when the station is in the list for programmed
			// irrigation. There are 3 reasons we can be in the list and trying to set the expected flow
			// rate, MOBILE, TEST, and for PROGRAMMED IRRIGATION. In the case of test and mobile when we
			// run out of time there is no more pending time to run and we'll be removed from the list,
			// so clearing this flag is not needed for that case. BUT for programmed irrigation, if the
			// flow was unstable (not 0, i said UNSTABLE, there is a difference!) the expected will not
			// be set within the allotted 6 minutes. And there is time remaining (the original
			// programmed time that is) so the station is not removed from the list. If the flow was
			// stable, the flag used to be cleared in the function that 'learns' the flow. BUT I've
			// moved that here now, so that if the flow is unstable, and the 6 minutes runs down, the
			// flag is cleared so we DO NOT try again to acquire the expected. Actually what we saw, in
			// a severe case of unstable flow, was that over and over again ALL NIGHT and DAY long, the
			// station tried it's six minutes, couldn't learn, soaked, then tried again. Clearing the
			// flag here will break that viscious cycle. AND because, for the other reasons we are in
			// the list, it is also okay to also clear this flag, we'll clear it without testing
			// specifically for the programmed irrigation reason. So for all reasons in the list we
			// clear it.
			pilc_ptr->bbf.w_to_set_expected = (false);

			// ----------
			
			if( pilc_ptr->bsr->ufim_flow_check_group_count_of_ON[ pilc_ptr->bbf.flow_check_group ] > 0 )
			{
				pilc_ptr->bsr->ufim_flow_check_group_count_of_ON[ pilc_ptr->bbf.flow_check_group ] -= 1;
			}
			else
			{
				// 5/30/2012 rmd : Well apparently the flow group count is already 0.
				Alert_Message( "FOAL_IRRI: flow group count unexpectedly zero." );
			}

			// ----------

			// 2/20/2013 rmd : When a valve turns OFF there are a couple of processes are taking place.
			// And it is even more complicated when it is the last valve to turn OFF. 1) FIRST we want
			// to block regular flow checking from taking place because a valve has turned OFF. Usually
			// what happens is another valve is going to turn ON soon. But till then we need to block
			// flow checking. And since we do not have a traditional valve close delay anymore and given
			// if a valve is going to turn ON soon it is reasonable to use the line fill delay at this
			// point. So we have something. If another valve is going to turn ON this value will just
			// get reloaded where the valve turns ON. And if there are other valves still ON, and we are
			// not going to turn ON another valve, and the slow closing block out period is 0, this line
			// fill time will just have to suffice. If we are not going to turn ON another valve and
			// there are no others ON then the MLB just stopped irrigating timer will take over.
			if( pilc_ptr->bsr->flow_checking_block_out_remaining_seconds < pilc_ptr->line_fill_seconds )
			{
				pilc_ptr->bsr->flow_checking_block_out_remaining_seconds = pilc_ptr->line_fill_seconds;
			}
			
			// ----------

			// 9/10/2015 rmd : START of slow closing valve section.
			
			// 2/20/2013 rmd : Set the SLOW CLOSING VALVE INHIBIT timer. Test the time to set will take
			// us out past the time already ticking down. If the slow_closing_valve is non-zero that
			// means the user wants to delay between valves.
			inhibit_next_turn_on = (true);
			
			// ----------
			
			// 9/9/2014 rmd : When irrigating TEST or RRE we do not activate the slow closing valve
			// timer.
			if( (pilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE) || (pilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_TEST) )
			{
				inhibit_next_turn_on = (false);
			}
			
			// ----------
			
			// 9/9/2014 rmd : The logic we want to implement is that if we were running the pump
			// (irrigating pump valves) and this was the only valve ON then skip the slow closing valve
			// mechanism. This was decided by David Byma, Richard Wilkinson, and myself. This is being
			// done to avoid the resultant condition where we would be left with the pump running and no
			// valves ON.
			if( pilc_ptr->bbf.w_uses_the_pump)
			{
				// 9/9/2014 rmd : Check the number_of_valves_ON in this system. It has already been
				// decremented reflecting how many would be ON after this valve we are shutting OFF is OFF.
				if( pilc_ptr->bsr->system_master_number_of_valves_ON == 0 )
				{
					inhibit_next_turn_on = (false);

					// 9/9/2014 rmd : Turns out this alert will show on the last valve of any scheduled
					// irrigation that uses the pump. But it is an engineering alert and I think it's better to
					// show as a reminder to us engineers for the cases when it does ignore the slow closing in 
					// the midst of irrigation. We'll see. At only show it if the setting is non-zero. [Remember
					// this is a by system affair and the irrigation list potnetially contains valves for
					// multiple systems. So the idea of check if the list count > 0 to help decide to write the
					// alert or not is faulty!]
					if( pilc_ptr->slow_closing_valve_seconds > 0 )
					{
						Alert_Message( "Slow closing delay ignored: pump running with no valves ON." );
					}
				}
			}
			
			// ----------
			
			if( inhibit_next_turn_on )
			{
				if( pilc_ptr->bsr->inhibit_next_turn_ON_remaining_seconds < pilc_ptr->slow_closing_valve_seconds )
				{
					pilc_ptr->bsr->inhibit_next_turn_ON_remaining_seconds = pilc_ptr->slow_closing_valve_seconds;
					
					//Alert_Message_va( "Turn ON inhibit set %u", pilc_ptr->bsr->inhibit_next_turn_ON_remaining_seconds );
				}
			}
			else
			{
				// 9/9/2014 rmd : A couple of scenarios here to cover. Let's take the case during regular
				// irrigation where 2 valves expire at the same time. And they were the only 2 ON at the
				// time. And they used the pump. The first one OFF would pass through here and set the
				// inhibit time because as far as we could see there was still another valve ON. The second
				// one and last one ON would then pass through here and not set the time as he was the only
				// one ON. HOWEVER the inhibit time is still set and ticking down. So the issue we are
				// trying to prevent was created. That is the pump ON and no valves ON. The solution is to 0
				// the time if we are not setting the time. This works for this scenario and I think is safe
				// for all other scenarios.
				//
				// 9/9/2014 rmd : The other case this covers is if a TEST interrupts a scheduled irrigation
				// when the pump was running and multiple valves were ON. If we didn't 0 the time here the
				// TEST valve would be delayed.
				pilc_ptr->bsr->inhibit_next_turn_ON_remaining_seconds = 0;
				
				//Alert_Message_va( "Turn ON inhibit set to %u", pilc_ptr->bsr->inhibit_next_turn_ON_remaining_seconds );
			}
				
			// 9/10/2015 rmd : END of slow closing valve section.

			// ----------
			
			// 6/4/2012 rmd : Generally we recover the balance of the cycle presently running. That is
			// not the case when acquiring an expected. So if the ACTION_REASON indicates turning OFF as
			// we have acquired an expected let the balance be dropped.
			if( paction_reason != ACTION_REASON_TURN_OFF_have_acquired_expected )
			{
				if( pilc_ptr->remaining_seconds_ON > 0 )
				{
					pilc_ptr->requested_irrigation_seconds_ul += pilc_ptr->remaining_seconds_ON;
				}
			}
			
			// ------------------------------			
			
			pilc_ptr->bbf.station_is_ON = (false);
		

			// 5/10/2012 rmd : Cleanup remaining seconds to zero. Don't have to but might look funny
			// during the soak to see a negative (if one developed). Or during a flow check hunting
			// process to see time remaining in the cycle but not ON.
			pilc_ptr->remaining_seconds_ON = 0;
	
			// ------------------------------			
			
			// This is a logical place to capture the last_off address and station number for use in 
			// diverting the non-controller flow.
			pilc_ptr->bsr->last_off__station_number_0 = pilc_ptr->station_number_0_u8;
	
			pilc_ptr->bsr->last_off__reason_in_list = pilc_ptr->bbf.w_reason_in_list;
	
			// ----------
			
			// For the case of an rre pause must set the status pause bit in the ilc record to prevent
			// the foal machine from turning back ON the station.
			if( paction_reason == ACTION_REASON_TURN_OFF_pause_rre_commanded )
			{
				pilc_ptr->bbf.rre_station_is_paused = (true);
			}	
				
			// ----------
			
			if( (paction_reason == ACTION_REASON_TURN_OFF_cause_cycle_has_completed) ||
				(paction_reason == ACTION_REASON_INTERNAL_TURN_OFF_reboot_or_chain_went_down)
			  )
			{
				// 6/26/2012 rmd : START the soak cycle for a NORMAL cycle end (for problem turn offs we set
				// soak to 0! UPDATE: AJ convinced me that on the other side of a power fail the stations
				// that were ON should now be soaking. Technically I think he is right. As we could start
				// them right back up with another full cycle. And likely cause run-off. My idea was to have
				// the ones ON come back ON. To avoid confusion. But hey how often is somebody watching.
				pilc_ptr->soak_seconds_remaining_ul = pilc_ptr->soak_seconds_ul;
			}
			else
			{
				// 11/13/2014 rmd : For ALL other reasons that we are turning OFF a valve set the soak time
				// to 0. For example during flow checking errors we don't want a soak time. Think about it
				// this way the only case to soak is that the cycle has completed normally.
				pilc_ptr->soak_seconds_remaining_ul = 0;
			}

			// ----------
			
			// 6/30/2015 rmd : If a mobile station turns off by running out of time or for any other
			// reason for that matter we want to make the OFF alert line and send a status to update the
			// remote screen the user MAY be looking at. Forcing this status will provide an update if
			// he is still looking at the screen. This is sent sort 'outside of' the mobile session. As
			// the user has closed his screen. But no matter send the status anyway.
			if( pilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE )
			{
				// 7/6/2015 rmd : We are going to turn it OFF for sure so alert about this. This is how the
				// alert is generated when the time expires.
				Alert_mobile_station_off( pilc_ptr->box_index_0, pilc_ptr->station_number_0_u8 );
		
				// 7/2/2015 rmd : Make sure to post to send the status after station is for sure OFF. Even
				// though TDCHECK is a higher priority than the CI task we don't want the code structure to
				// contain the possibility the status message content is built up before the station is
				// actually OFF.
				CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_mobile_status, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, queueSEND_TO_BACK );
			}
			
			// ----------
			
			// 2012.02.17 rmd : Typically we want to re-order the list to juggle the effective
			// priorities of which station will come ON next. Except for the case of WALK_THRU. Which
			// should retain it's list order once they are added to the list. Even if say during
			// WALK-THRU a radio remote station comes on for a bit. Then off. We want the WALK-THRU to
			// pick up where it left off. They should go in order.
			if( pilc_ptr->bbf.w_reason_in_list != IN_LIST_FOR_WALK_THRU )
			{
				FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list( NOT_FOR_FTIMES, pilc_ptr );
			}

		}  // of if the station is ON
		else
		{
			// 5/3/2012 rmd : SANITY CHECK. This ilc should NOT be on the ON list.
			if( nm_OnList( &foal_irri.list_of_foal_stations_ON, (void*)pilc_ptr ) )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "FOAL: unexpectedly on ON list." );
			
				if( nm_ListRemove( &foal_irri.list_of_foal_stations_ON, (void*)pilc_ptr ) != MIST_SUCCESS )
				{
					ALERT_MESSAGE_WITH_FILE_NAME( "Error removing from list" );
				}
			}
		}

	}  // of if the pilc is not on the irrigation list
	
	// ------------

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Circumvents the anti-chatter mechanism for both the MV and PMP outputs for
    a given system. Also ignores the slow closing valve setting. [Which may prolong any PMP
    shutdown for quite some time. Usually if slow closing is in use it is set to 30+
    seconds.] The effect of this function is to turning off the MV and PMP outputs
    immediately. Given there are no stations coming ON. Useful with the STOP key and during
    short processing.

	@CALLER_MUTEX_REQUIREMENTS Caller needs to be holding the system_preserves MUTEX.
	
	@MEMORY_RESPONSIBILITES (none)
	
    @EXECUTED Executed within the context of the COMM_MNGR task during short and stop key
    processing.
	
	@RETURN (none)
	
	@ORIGINAL 2013.02.21 rmd
	
	@REVISIONS (none)
*/
extern void nm_shut_down_mvs_and_pmps_throughout_this_system( UNS_32 pfor_which_system_gid )
{
	// 10/23/2015 rmd : Should ONLY be called after we are sure there are no more valves ON is
	// this system. If there were valves irrigating the MV and/or PUMP would just pop back on!
	
	// ----------

	UNS_32	sss;
	
	for( sss=0; sss<MAX_POSSIBLE_SYSTEMS; sss++ )
	{
		if( system_preserves.system[ sss ].system_gid == pfor_which_system_gid )
		{
			// 2/21/2013 rmd : Force the MVs to be closed in this system. Overriding the anti-chatter
			// mechanism and any slow closing valve time the user may have set for any of the valves
			// that just turned OFF.
			system_preserves.system[ sss ].sbf.mv_open_for_irrigation = (false);

			system_preserves.system[ sss ].sbf.pump_activate_for_irrigation = (false);

			// 2/21/2013 rmd : And perform some house keeping to cleanup left over timers. Probably not
			// necessary but won't hurt.
			system_preserves.system[ sss ].transition_timer_all_stations_are_OFF = 0;
			system_preserves.system[ sss ].transition_timer_all_pump_valves_are_OFF = 0;

			// ----------

			// 11/6/2013 rmd : Regarding the MLB just stopped irrigating timer - if station were ON that
			// timer is loaded. If none were ON then we are already either counting down the timer or in
			// a MLB checking time frame. So no maintenance needed for the just stopped irrigating
			// timer.

			// ----------

			// 2/21/2013 rmd : If the user is using TEST or RRe this turn ON restriction for slow
			// closing valves is overlooked. However the STOP key has closed the MVs and shutdown the
			// PMP so this likely serves no purpose being ineffect. So cancel it.
			system_preserves.system[ sss ].inhibit_next_turn_ON_remaining_seconds = 0;
		}
	}
}

/* ---------------------------------------------------------- */
static void set_station_history_flags_if_in_the_list_for_programmed_irrigation( IRRIGATION_LIST_COMPONENT const *const pilc, UNS_32 const paction_reason )
{
	// 11/7/2014 rmd : Called mostly within the context of the comm_mngr task. Though also
	// called within the context of the TD_CHECK task whent he foal_irri list maintenance
	// function runs.

	// 10/22/2014 rmd : If it is in the list for  PROGRAMMED_IRRIGATION then consider the
	// station history flags.
	if( pilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
	{
		// 8/23/2012 rmd : Take the MUTEX as the report data bit field operations are not atomic.
		xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );
	
		// 10/29/2014 rmd : For some of the reasons we want to set flag bits in the station history
		// lines. For the case of the POC short and low voltage however we do not.
		if( (paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_station_short) || (paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_solenoid_short) )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.current_short = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_voltage_too_low )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.two_wire_cable_problem = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_inoperative )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.two_wire_station_decoder_inoperative = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_two_wire_cable_fault )
		{
			// 8/23/2012 rmd : Set the station history flag about why the irrigation did not complete.
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.two_wire_cable_problem = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_controller_turned_off )
		{
			// 4/5/2013 ajv : Set the station history flag about why the
			// irrigation did not complete.
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.controller_turned_off = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_mlb )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.mlb_prevented_or_curtailed = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_mv_short )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.poc_short_cancelled_irrigation = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_pump_short )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.poc_short_cancelled_irrigation = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_solenoid_short )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.poc_short_cancelled_irrigation = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_voltage_too_low )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.two_wire_cable_problem = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_inoperative )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.two_wire_poc_decoder_inoperative = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_stop_key )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.stop_key_pressed = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_hit_stop_time )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.hit_stop_time = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_rain_switch_active )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.switch_rain_prevented_or_curtailed = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_freeze_switch_active )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.switch_freeze_prevented_or_curtailed = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_rain_crossed_minimum_or_poll )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.rain_table_R_M_or_Poll_prevented_or_curtailed = (true);
		}
		else
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_moisture_reading_hit_set_point )
		{
			station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.mois_cause_cycle_skip = (true);
		}
		
		
		// ----------
		
		// 11/11/2014 rmd : The only time we let the ORPHAN indication persist is when irrigation
		// ends normally or when the stop time is hit. Otherwise for all other reasons we do not
		// allow orphans to form.
		if( (paction_reason != ACTION_REASON_TURN_OFF_AND_REMOVE_irrigation_has_completed) && (paction_reason != ACTION_REASON_TURN_OFF_AND_REMOVE_hit_stop_time) )
		{
			station_preserves.sps[ pilc->station_preserves_index ].spbf.did_not_irrigate_last_time = (false);
		}
		
		// ----------

		xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called during ANY turn off process or list removal process. The station
    does not have to be ON when calling this function. But it surely will be OFF
    afterwards. Note the return value is the next ilc in the list. To accomodate the list
    re-ordering that takes place as a station is turned OFF. Removes the station from all
    lists including the action_needed list.

    @CALLER MUTEX REQUIREMENTS The caller should be holding TWO MUTEXES. The foal_irri_MUTEX
    to protect the pilc list item and the list itself from changes. And the system_preserves
    MUTEX as we are going to set some timers within the system preserves.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task. When a token response is
    parsed contain a request to terminate stations. Also within the context of the TD_CHECK
    task as part of routine list maintenance.

    @RETURN A pointer to the next ilc in the plist_hdr list. Which should be either the ON
    list or the main foal_irri irrigation list. Or NULL if the pilc_ptr is NOT on the
    foal_irri irrigation list.

	@ORIGINAL 2012.08.23 rmd

	@REVISIONS (none)
*/
extern IRRIGATION_LIST_COMPONENT* nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( MIST_LIST_HDR_TYPE_PTR plist_hdr, IRRIGATION_LIST_COMPONENT *const pilc_ptr, UNS_32 const paction_reason )
{
	// 12/11/2015 rmd : To be extra sure with the floating point math. There is a known compiler
	// bug. If this function were to ever be made a 'static' without using these const
	// definitions there is a very good chance the MATH WOULDN'T WORK! So protect against that
	// possibility.

//	const volatile float SIXTY_POINT_ZERO = 60.0;

	const volatile float ONE_HUNDRED_POINT_ZERO = 100.0;

//	const volatile float ONE_HUNDRED_THOUSAND_POINT_ZERO = 100000.0;

	// ----------
	
	IRRIGATION_LIST_COMPONENT	*rv;

	// ----------
	
	// 11/5/2014 rmd : Regardless of the nm_nm_ designation on this function call take the
	// appropriate MUTEXES in case the caller did not. They are recursive therefore this is okay
	// to do and results in a small hit to code size and performance to do so.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	
	// ----------
	
	// 11/11/2014 rmd : If the ilc is in the list for programmed irrigation, whether it is ON or
	// not, depending on the paction_reason we may want to set a station history flag.
	set_station_history_flags_if_in_the_list_for_programmed_irrigation( pilc_ptr, paction_reason );
	
	// ----------
			
	// 5/9/2012 rmd : Turn OFF the station if it is ON.
	rv = nm_nm_FOAL_if_station_is_ON_turn_it_OFF( plist_hdr, pilc_ptr, paction_reason );
	
	// ----------
	
	// 11/5/2014 rmd : MUST tell the IRRI side about what is happening on the FOAL side. It will
	// already be on the list if the station was ON when the prior function call was made. And
	// that's okay.
	nm_FOAL_add_to_action_needed_list( pilc_ptr, paction_reason );
	
	// ----------
	
	// 11/19/2014 rmd : CLOSE the station history line. This is the place. To be removed from
	// the foal irri list the station MUST pass through this function. By application design.
	//
	// 12/11/2015 rmd : This is ALSO where the MOISTURE BALANCE is adjusted for each station.
	if( pilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
	{
		// 9/3/2014 rmd : This function to close also wipes and partially initializes the rip. And
		// as such we are to be holding the STATION_PRESERVES mutex. Which we are.
		xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );


		STATION_STRUCT	*lstation;

		//float	runtime_to_adjust_moisture_balance_with;

		//float	RZWWS, MB;;

		// ----------

		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		lstation = nm_STATION_get_pointer_to_station( pilc_ptr->box_index_0, pilc_ptr->station_number_0_u8 );

		if( lstation != NULL )
		{
			if( WEATHER_get_station_uses_daily_et( lstation ) )
			{
				// 4/18/2016 ajv : If this flag is set, force the moisture balance back to 50% at the
				// conclusion of irrigation. This is used to correct errant moisture balance values due to
				// bugs in the run time calculation.
				if( nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag(lstation) )
				{
					// 3/15/2017 ajv : Reset the moisture balance back to 50%. This is the same code as in the
					// else below, but we also do two other things here, so let's keep it explicit.
					nm_STATION_set_moisture_balance_percent( lstation, STATION_MOISTURE_BALANCE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, CHANGE_REASON_DONT_SHOW_A_REASON ) );

					// 4/26/2016 ajv : Also make sure we zero out any left over irrigation (that is time less
					// than 3.0 minutes that was banked at the last irrigation).
					station_preserves.sps[ pilc_ptr->station_preserves_index ].left_over_irrigation_seconds = 0;

					// 4/18/2016 ajv : Clear the flag now that we've reset the moisture balance.
					nm_STATION_set_ignore_moisture_balance_at_next_irrigation( lstation, STATION_MOISTURE_BALANCE_CLEAR_IGNORE_FLAG, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, CHANGE_REASON_DONT_SHOW_A_REASON ) );
				}
				else
				{
					/*
					// 12/19/2016 rmd : LEAVING THIS OLD CODE FOR REFERENCE. TO USE WHEN THINKING ABOUT ANY
					// FURTHER ADJUSTEMENTS TO HOW WE HANDLE THE MOISUTRE BALANCE. SEE NEW CODE BELOW.

					// 6/30/2015 ajv : Moisture Balance is calculated independently of the station adjust or
					// percent adjust factors. Therefore, we calculated the runtime using the WaterSense
					// equations when we crossed the start time and then adjusted it using those factors. If we
					// ran longer than the WaterSense equations specified, only increase the moisture balance
					// by the pre-adjusted run time.
					//
					// 12/11/2015 rmd : I BELIEVE the following 'if' also has the effect of including behaviors
					// such as FLOW CHECKING in the moisture balance adjustment. In other words if there was a
					// 'shutoff high flow' the MB will drop. The next time we go to irrigate, the lost time will
					// not be made up, and the MB will remain at a 'lower' level unit a rain event. That would
					// be the ONLY way to get the MB back up towards or beyond the 50% mark. I'm commenting on
					// the behavior here to provide recognition of this behavior.
					if( station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_watersense_requested_seconds < station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_seconds_irrigated_ul )
					{
						runtime_to_adjust_moisture_balance_with = (station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_watersense_requested_seconds / SIXTY_POINT_ZERO);
					}
					else
					{
						runtime_to_adjust_moisture_balance_with = (station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_seconds_irrigated_ul / SIXTY_POINT_ZERO);
					}

					RZWWS = (STATION_GROUP_get_soil_storage_capacity_inches_100u( lstation ) / ONE_HUNDRED_POINT_ZERO);

					MB = WATERSENSE_increase_moisture_balance_by_irrigation( runtime_to_adjust_moisture_balance_with, (nm_STATION_get_distribution_uniformity_100u(lstation) / ONE_HUNDRED_POINT_ZERO), (STATION_GROUP_get_station_precip_rate_in_per_hr_100000u(lstation) / ONE_HUNDRED_THOUSAND_POINT_ZERO), RZWWS, STATION_get_moisture_balance(lstation) );

					// 6/30/2015 ajv : Convert the Moisture Balance back into a percentage before saving it to
					// the station file.
					nm_STATION_set_moisture_balance_percent( lstation, (MB / RZWWS), CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, CHANGE_REASON_DONT_SHOW_A_REASON ) );
					*/
					

					// 12/19/2016 rmd : Okay. So now here is how we are going to DEFINE moisture balance to work
					// in the Calsense controller
					// 
					//  1) After irrigation, by definition, the MB will be set to 50%. We still left in place
					//  the means to CALCULATE the MB value following irrigation (see the commented out code
					//  above) BUT there is a scenario where there is a problem. If one changes the irrigation
					//  days schedule, the amount to irrigate may drive the MB above 50%. Imagine switching from
					//  irrigating every day to every third day. And the first irrigation is tonight. We'll
					//  irrigate for 3 days of ET and the calculated MB will be above 50%. On the next
					//  irrigation we'll probably be below 50%, depending on the ET. But not guaranteed. So we
					//  MIGHT skip an irrigation. Another unexpectedly skipped irrigation could occur during
					//  accelerated irrigation for test if we moved the start time ahead to cause two
					//  irrigations today. OR if the power were to be out for a day or to. When it came back on
					//  and it hit an irrigation day it may not irrigate if the MB is still above 50% (REMEMBER
					//  the MB is decremented every day at the start time).
					//
					//  2) When it rains we snap the MB to 50% prior to adding the rain equivalent to the MB.
					//
					// So, only if it irrigated, which I think it must have to be here in the code but make the
					// test anyway, force the MB to 50%. That way we ALWAYS irrigate at the next start time if
					// it is a water day.
					if( station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_watersense_requested_seconds > 0 )
					{
						nm_STATION_set_moisture_balance_percent( lstation, STATION_MOISTURE_BALANCE_DEFAULT, CHANGE_do_not_generate_change_line, CHANGE_REASON_DONT_SHOW_A_REASON, FLOWSENSE_get_controller_index(), CHANGE_set_change_bits, STATION_get_change_bits_ptr( lstation, CHANGE_REASON_DONT_SHOW_A_REASON ) );
					}  // of if irrigated
					
				}  // of if not reseting MB to 50%
				
			}  // of if uses ET to irrigate

			// 7/17/2015 ajv : Also store the moisture balance percentage into the station history line
			// at the conclusion of irrigation. We'll set this regardless of whether the user is using
			// ET or not.
			station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_moisture_balance_percentage_after_schedule_completes_100u = (INT_32)(STATION_get_moisture_balance_percent(lstation) * ONE_HUNDRED_POINT_ZERO * ONE_HUNDRED_POINT_ZERO);

			// 10/28/2016 ajv : Make sure to included the leftover (banked) time in the total requested
			// minutes so that the report record shows, for example, that it was scheduled to run for
			// 4.5 minutes, ran 3.0 minutes, with a flag that indicates cycle time too short.
			station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_total_requested_minutes_us_10u = ((station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_total_requested_minutes_us_10u * 6) + (station_preserves.sps[ pilc_ptr->station_preserves_index ].left_over_irrigation_seconds)) / 6;
		}
		else
		{
			Alert_station_not_found();

			// 7/17/2015 ajv : Well, we didn't find a valid station with this ilc (which should never
			// happen, by the way), so initialize the moisture balance to 0.
			station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_moisture_balance_percentage_after_schedule_completes_100u = 0;
		}

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

		// ----------

		nm_STATION_HISTORY_close_and_start_a_new_record( pilc_ptr->station_preserves_index );
	
		xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );

	}  // of if in the list for programmed irrigation
	
	// ----------
	
	// 12/11/2015 rmd : NOW - Remove from the irrigation list. That is what this function is all
	// about after all. This does not affect the fact that the ilc is on the ACTION NEEDED list.
	nm_ListRemove( &(foal_irri.list_of_foal_all_irrigation), pilc_ptr );

	// ------------

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The TOKEN_RESP indicates a station short. We find and turn OFF the station.
    The controller index parameter is of the range 0..(MAX_CHAIN_LENGTH-1). The station
    number paramter is of course 0 based.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task. When rcving the TOKEN_RESP
    and it indicates a station short has been detected.
	
	@RETURN (none)

	@ORIGINAL 2012.08.23 rmd

	@REVISIONS (none)
*/
extern void FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 paction_reason )
{
	// 4/18/2014 rmd : The parameter pbox_index_0 is 0 based.
	
	// 10/22/2014 rmd : This function is called for a conventional output short, a two-wire
	// decoder output short, a two-wire decoder low voltage condition, and for an inoperative
	// two-wire decoder. ALL instances of the station will be removed. Meaning if in the
	// irrigation list for more than one reason, say for manual programs and programmed
	// irrigation, both instances of the station will be removed.
	
	// ----------
	
	// 8/21/2012 rmd : Take both these MUTEXES to support direct irrigation and system
	// manipulation while we find a station, turn it OFF, and remove it from the list(s).
	// System_preserves is involved when turn OFF any station.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 12/4/2012 rmd : If a flow recording line has not yet been made for the stations ON
	// (whether it is 1 or more) setting this (false) allows us to go ahead and make a
	// line.
	foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (false);

	// ----------
	
	BY_SYSTEM_RECORD	*captured_bsr;
	
	captured_bsr = NULL;
	
	// ----------
	
	IRRIGATION_LIST_COMPONENT	*lilc;

	// 10/22/2014 rmd : Look through the irrigation list.
	lilc = nm_ListGetFirst( &foal_irri.list_of_foal_all_irrigation );

	while( lilc != NULL )
	{
		// 8/23/2012 rmd : We are looking for the particular station at a particular box. That takes
		// a controller index and station number to find.
		if( (lilc->box_index_0 == pbox_index_0) && (lilc->station_number_0_u8 == pstation_number_0 ) )
		{
			// 2/21/2013 rmd : Well we found the station of interest. But only capture the bsr for the
			// first station found. That would be the one ON due to the list ordering by irrigation
			// priority. The station may be in the list for other reasons (like TEST). WHOA! wait a
			// minute the bsr would still be valid no matter the reason. So can capture for each
			// occurance of the station no matter the reason in the list or how many times found.
			captured_bsr = lilc->bsr;

			// ----------

			lilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), lilc, paction_reason );

			// 10/24/2014 rmd : NOTE - we keep going even though we found one case of the station. For
			// the kinds of problems handled within this function we remove ALL instances of the
			// station. Could be in the list for multiple reasons.
		}
		else
		{
			lilc = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), lilc );
		}
	}

	// ----------
	
	if( captured_bsr == NULL )
	{
		// 10/22/2014 rmd : For all reasons except the INOP station or poc decoder we expect to find
		// a station in the irrigation list.
		if( (paction_reason != ACTION_REASON_TURN_OFF_AND_REMOVE_station_decoder_inoperative) && (paction_reason != ACTION_REASON_TURN_OFF_AND_REMOVE_poc_decoder_inoperative) )
		{
			Alert_Message( "FOAL_IRRI: couldn't find failed station?" );
		}
	}
	else
	{
		// 10/31/2014 rmd : So the captured bsr is not NULL. We can use if needed.
		
		// 10/22/2014 rmd : For the case of a CONVENTIONAL short I found a behavior anomaly that I
		// was able to fix here. Read below for more info.
		if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_station_short )
		{
			// 2/21/2013 rmd : So the station was found and removed from the list. And what is the state
			// of the MV and PMP at the box where the short was? They are OFF right now. And if the list
			// is empty for this system we would like to keep them off and turn OFF all the other MV's
			// and PMPs right away. This is more of an issue when someone is standing in front of the
			// box testing. After the shorted station hunt is over and we acknowledge the short to the
			// TP Micro the MV and PMP pop back ON till the anti-chatter timers run down. And that just
			// looks mis-behaved.
	
			// 2/21/2013 rmd : See if any stations belonging to the system that experienced the short
			// remain in the list.
			BOOL_32	no_more_for_system;
			
			no_more_for_system = (true); 
	
			lilc = nm_ListGetFirst( &(foal_irri.list_of_foal_all_irrigation) );
		
			while( lilc != NULL )
			{
				if( lilc->bsr == captured_bsr )
				{
					no_more_for_system = (false);
					
					break;  // all done. 
				}
	
				lilc = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), lilc );
			}	
	
			// 2/21/2013 rmd : If there are no more valves for this system we want to turn OFF the MVs
			// and PMPs. NOW. Without waiting for the anti-chatter delay to count down. And without
			// waiting for the slow closing valve delay to count down.
			if( no_more_for_system )
			{
				nm_shut_down_mvs_and_pmps_throughout_this_system( captured_bsr->system_gid );
			}
		}
	}

	// ----------
	
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists( const UNS_32 pcontroller_index, const UNS_32 paction_reason )
{
	// 9/25/2013 rmd : This is called in response to a repeated unsuccessful short hunt at the
	// TP Micro coupled with seeing that there are indeed station outputs ON at the reporting
	// box. Called within the context of the COMM_MNGR task.
	
	BOOL_32		found_one;
	
	found_one = (false);
	
	// ----------

	// 8/21/2012 rmd : Take both these MUTEXES to support direct irrigation and system
	// manipulation while we find a station, turn it OFF, and remove it from the list(s).
	// System_preserves is involved when turn OFF any station.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 12/4/2012 rmd : If a flow recording line has not yet been made for the stations ON
	// (whether it is 1 or more) setting this (false) allows us to go ahead and make a
	// line.
	foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (false);

	// ----------
	
	IRRIGATION_LIST_COMPONENT	*lilc;

	// 11/19/2014 rmd : Look through the list of those ON. Per the intent of the function.
	lilc = nm_ListGetFirst( &(foal_irri.list_of_foal_stations_ON) );

	while( lilc != NULL )
	{
		// 9/25/2013 rmd : In this case we are looking to turn off all ON at this box.
		if( lilc->box_index_0 == pcontroller_index )
		{
			if( paction_reason == ACTION_REASON_TURN_OFF_AND_REMOVE_conventional_station_short )
			{
				// 9/26/2013 rmd : Make the alert now before the lilc changes.
				Alert_conventional_station_short_idx( pcontroller_index, lilc->station_number_0_u8 );
			}

			// ----------
			
			// 2/21/2013 rmd : We found one of the stations we're after.
			found_one = (true);

			// ----------
			
			// 9/26/2013 rmd : Returns the next station in the list. So after this call lilc points to
			// the next station. Or possibly NULL.
			lilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_stations_ON), lilc, paction_reason );
		}
		else
		{
			lilc = nm_ListGetNext( &(foal_irri.list_of_foal_stations_ON), lilc );
		}
	}

	// ----------
	
	if( !found_one )
	{
		// 9/30/2015 rmd : Well we didn't find a station ON. I used to alert about this. But now
		// with the addition of LIGHTS it could be one of those outputs. So this isn't necessarily
		// an error. This brings to mind ... what about MVOR activating the MV and we have a
		// short? Anyway COMMENT OUT THIS ALERT.
		//Alert_Message( "FOAL_IRRI: hunt failed and then couldn't find any ON?" );
	}
	
	// ----------
	
	// 10/23/2014 rmd : If you look at the turn OFF function for a short there is a long note
	// about the MV and PUMP behavior when all the valves ON have been turned OFF and there are
	// no more to come ON. The MV and PUMP go through a chatter of sorts by turning back ON till
	// their delay time runs down - about 5 seconds. Well to do that correctly here you must
	// look at each system that we turned valves OFF for. And system by system decide if there
	// are more valves in the list to irrigate. I suppose we could just in the blind look at
	// each system and if there are no vavles in the list for that system shutdown all the MV's
	// and PMP's. That would work. But you know what this unknown short isn't supposed to happen
	// anyway. So let's just ignore this funnny. No one will ever notice.
	
	// ----------
	
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists( const UNS_32 pbox_index_0, const UNS_32 pstation_number_0, const UNS_32 preason_in_list, const UNS_32 paction_reason )
{
	// 6/18/2015 rmd : So far this is called in response to receiving mobile station OFF
	//command. So therefore this function is exectued in that case within the context of the 
	//COMM_MNGR task.
	
	// ----------
	
	BOOL_32		found_one;
	
	found_one = (false);
	
	// ----------

	// 8/21/2012 rmd : Take both these MUTEXES to support direct irrigation and system
	// manipulation while we find a station, turn it OFF, and remove it from the list(s).
	// System_preserves is involved when we turn OFF any station.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 12/4/2012 rmd : If a flow recording line has not yet been made for the stations ON
	// (whether it is 1 or more) setting this (false) allows us to go ahead and make a
	// line.
	foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (false);

	// ----------
	
	IRRIGATION_LIST_COMPONENT	*lilc;

	// 6/18/2015 rmd : Look through the entire irrigation list. Not just the list of those ON.
	// In the case of mobile maybe it isn't ON yet? Anyway no harm in looking through the main
	// list to be thourough.
	lilc = nm_ListGetFirst( &(foal_irri.list_of_foal_all_irrigation) );

	while( lilc != NULL )
	{
		if( (lilc->box_index_0 == pbox_index_0) && (lilc->station_number_0_u8 == pstation_number_0) && (lilc->bbf.w_reason_in_list == preason_in_list) )
		{
			// 2/21/2013 rmd : We found one of the stations we're after.
			found_one = (true);

			// ----------
			
			// 9/26/2013 rmd : Returns the next station in the list. So after this call lilc points to
			// the next station. Or possibly NULL.
			lilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), lilc, paction_reason );

		}
		else
		{
			lilc = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), lilc );
		}
	}

	// ----------
	
	if( !found_one )
	{
		Alert_Message( "FOAL_IRRI: couldn't find one in the list" );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists( const UNS_32 pcontroller_index, const UNS_32 paction_reason )
{
	IRRIGATION_LIST_COMPONENT	*lilc;

	// ----------
	
	// 8/21/2012 rmd : Take both these MUTEXES to support direct irrigation and system
	// manipulation while we find a station, turn it OFF, and remove it from the list(s).
	// System_preserves is involved when turn OFF any station.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 12/4/2012 rmd : If a flow recording line has not yet been made for the stations ON
	// (whether it is 1 or more) setting this (false) allows us to go ahead and make a
	// line.
	foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (false);

	// ----------
	
	// 11/19/2014 rmd : Look through the list of all irrigation stations. Per the intent of the
	// function.
	lilc = nm_ListGetFirst( &(foal_irri.list_of_foal_all_irrigation) );

	while( lilc != NULL )
	{
		// 9/25/2013 rmd : In this case we are looking to turn off all ON at this box.
		if( lilc->box_index_0 == pcontroller_index )
		{
			// 9/26/2013 rmd : Returns the next station in the list. So after this call lilc points to
			// the next station. Or possibly NULL.
			lilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), lilc, paction_reason );
		}
		else
		{
			lilc = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), lilc );
		}
	}

	// ----------
	
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FOAL_IRRI_remove_all_stations_from_the_irrigation_lists( const UNS_32 paction_reason )
{
	UNS_32	iii;
	
	xSemaphoreTakeRecursive( chain_members_recursive_MUTEX, portMAX_DELAY );

	for( iii=0; iii<MAX_CHAIN_LENGTH; iii++ )
	{
		if( chain.members[ iii ].saw_during_the_scan )
		{
			// 10/30/2014 rmd : This will generate a flow recording line for each boxes stations. But oh
			// well. A minor anomoly.
			FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists( iii, paction_reason );
		}
	}

	xSemaphoreGiveRecursive( chain_members_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION The TOKEN_RESP indicates a short on either a MV or a PUMP. We have located
    the poc_preserves for that mv/pump. And in turn will now turn OFF and remove from
    irrigaiton ALL stations in the system that owns that mv/pump.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task. When rcving the TOKEN_RESP
    and it indicates a mv/pump short has occurred.
	
	@RETURN (none)

	@ORIGINAL 2012.08.23 rmd

	@REVISIONS (none)
*/
extern BOOL_32 FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists( BY_SYSTEM_RECORD const *const pbsr, UNS_32 const pfor_this_reason, BOOL_32 pfor_all_reasons, UNS_32 const paction_reason )
{
	// 11/7/2014 rmd : Returns (true) if we removed one from the list. Useful for the STOP key
	// processing.

	BOOL_32		rv;
	
	IRRIGATION_LIST_COMPONENT	*lilc;

	BOOL_32		no_more_for_system;

	rv = (false);

	// 8/21/2012 rmd : Take both these MUTEXES to support direct irrigation and system
	// manipulation while we find a station, turn it OFF, and remove it from the list(s).
	// System_preserves is involved when turn OFF any station.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 12/4/2012 rmd : This variable is always set (false) before any station turn OFF sequence.
	// To allow a batch of flow recording lines to be made - if none yet made for the group ON.
	// This variable manages the generation of only one batch of flow recording lines as we loop
	// through turning off each successive valve.
	foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (false);

	// ----------

	// 8/23/2012 rmd : For a poc short look through the ENTIRE irrigation list finding all
	// stations owned by the system that owns the POC.
	lilc = nm_ListGetFirst( &(foal_irri.list_of_foal_all_irrigation) );

	while( lilc != NULL )
	{
		if( lilc->bsr == pbsr && (pfor_all_reasons || (lilc->bbf.w_reason_in_list == pfor_this_reason)) )
		{
			rv = (true);

			lilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), lilc, paction_reason );
		}
		else
		{
			lilc = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), lilc );
		}
	}

	// ----------

	// 11/7/2014 rmd : For cases like the stop key I figure the user wants stuff to stop.
	// Possibly in an urgent fashion. So using a brute force method, ignoring slow closing valve
	// requirements shut down the MVs and PMPs. Since the STOP key only stops one layer of
	// irrigation it is possible this system still has valves in the list. So we need to roll
	// through the list looking for stations belonging to the system in question.
	
	no_more_for_system = (true); 

	lilc = nm_ListGetFirst( &(foal_irri.list_of_foal_all_irrigation) );

	while( lilc != NULL )
	{
		if( lilc->bsr == pbsr )
		{
			no_more_for_system = (false);
				
			break;  // all done.
		}

		lilc = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), lilc );
	}	

	// 8/27/2012 rmd : And we need to cancel the MV and PUMP anti-chatter delay. Otherwise the
	// MV and PUMP will actually turn back ON while that anti-chatter timer winds down. And if
	// there is a short on the mv or pump (one of the reasons we may be here) we will generate
	// another short sequence.
	if( no_more_for_system )
	{
		nm_shut_down_mvs_and_pmps_throughout_this_system( pbsr->system_gid );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
	
	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
extern void FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate( UNS_32 pcontroller_index, UNS_32 pstation_number )
{
	// 4/18/2014 rmd : The parameter pcontroller_index is 0 based.
	
	// 10/2/2013 rmd : As we are traversing the foal irrigation list take the MUTEX.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	IRRIGATION_LIST_COMPONENT	*lilc;

	// 8/23/2012 rmd : For a short looking through the list of those ON.
	lilc = nm_ListGetFirst( &(foal_irri.list_of_foal_stations_ON) );

	while( lilc != NULL )
	{
		// 8/23/2012 rmd : For the no current we are looking for the particular station at a
		// particular box. That takes a controller index and station number to find.
		if( (lilc->box_index_0 == pcontroller_index) && (lilc->station_number_0_u8 == pstation_number ) )
		{
			if( lilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
			{
				// 8/23/2012 rmd : Take the MUTEX as the report data bit field operations are not atomic.
				xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );
			
				// 8/23/2012 rmd : Set the station history flag per the intent of the function.
				station_preserves.sps[ lilc->station_preserves_index ].station_history_rip.pi_flag.current_none = (true);
			
				xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
			}
			
			// 8/23/2012 rmd : Only need to find the station ONCE in the ON list. It can only be ON for
			// ONE reason at a time. Once found we are done!
			break;
		}
		else
		{
			lilc = nm_ListGetNext( &(foal_irri.list_of_foal_stations_ON), lilc );
		}
	}

	// ----------
						
	if( lilc == NULL )
	{
		Alert_Message( "FOAL_IRRI: couldn't find no current station?" );
	}
	
	// ----------
	
	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION When the TOKEN_RESP is pulled apart we see it contains system gids to clear
    mlbs for and we call this function. First byte in the message is a count of how many
    UNS-16 gids are there. The pucp pointer into the message is kept up.
	
	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task.
	
    @RETURN (true) if all the range checks pass, (false) otherwise

	@ORIGINAL 2012.05.14 rmd

	@REVISIONS
*/
extern BOOL_32 FOAL_extract_clear_mlb_gids( UNS_8 **pucp_ptr )
{
	BOOL_32	rv;
	
	rv = (true);

	// 5/3/2012 rmd : We are chainging the system preserves. Make sure they are stable.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	
	UNS_8	record_count;
	
	// 5/16/2012 rmd : Can read directly as only a single byte. No alignment issues.
	record_count = **pucp_ptr;
	
	*pucp_ptr += sizeof(UNS_8);
			
	if( (record_count>0) && (record_count <= MAX_POSSIBLE_SYSTEMS) )
	{
		INT_32	i;
		
		for( i=0; i<record_count; i++ )
		{
			BY_SYSTEM_RECORD	*bsr;
			
			UNS_16	lgid;
			
			memcpy( &lgid, *pucp_ptr, sizeof(UNS_16) );
			
			*pucp_ptr += sizeof(UNS_16);
			
			bsr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( lgid );
			
			if( bsr != NULL )
			{
				if(( bsr->latest_mlb_record.there_was_a_MLB_during_irrigation == (true))  ||
				   ( bsr->latest_mlb_record.there_was_a_MLB_during_mvor_closed == (true)) ||
				   ( bsr->latest_mlb_record.there_was_a_MLB_during_all_other_times == (true)))
				{
					// 2/24/2015 mpd : Clearing a MLB on a NO MV results in immediate flow as the lines fill.
					// The flow rate is likely to exceed the MLB threshold for non-irrigation, which will
					// trigger another MLB response. Use the MVJO blockout time to allow the lines to fill
					// without further user intervention. This action is taken on NO and NC MVs since the MV
					// type(s) are not easily opbtained at the FOAL level. Since no corrective action needs to
					// be taken on a NC MV after a MLB reset, the MVJO blockout does not create a problem for NC
					// type valves even though it is unnecessary.
					//
					// 2/26/2015 rmd : A re-wording by Bob - If there is a MLB there is no irrigation taking
					// place and all the MV's are closed. When the user clears the MLB we 'NORMAL" the valves
					// which means to take away power. In the case of a NO MV the valve will open. And
					// potentially a mainline will fill. Therefore by definition of the MVJO flow checking
					// blockout we set the MVJO blockout timer.
					//
					// 2/26/2015 rmd : Additionally NOTE - we are not specifically checking if we are ACTUALLY
					// opening a NO MV. We set the timer for both cases NC and NO. And that is okay. The
					// blockout timers job is to allow the mainline to fill. In the case of a NC MV nothing
					// physically changes when the MLB is cleared as the valve is already de-energized. If we
					// DID'T set this timer for the case of a NC MV, and saw another MLB right after they
					// cleared the MLB, there is actually NOTHING we physically do to stop the flow. So not
					// seeing the MLB for the 2.5 minute blockout is no harm no foul.
					bsr->timer_MVJO_flow_checking_blockout_seconds_remaining = MLB_AND_FLOW_CHECKING_MVJO_BLOCKOUT_SECONDS;
				}

				// ----------
				
				// 5/16/2012 rmd : The command will clear ALL the mlb conditions that exist.
				if( bsr->latest_mlb_record.there_was_a_MLB_during_irrigation == (true) )
				{
					Alert_mainline_break_cleared( lgid );
					
					bsr->latest_mlb_record.there_was_a_MLB_during_irrigation = (false);
				}

				if( bsr->latest_mlb_record.there_was_a_MLB_during_mvor_closed == (true) )
				{
					Alert_mainline_break_cleared( lgid );
					
					bsr->latest_mlb_record.there_was_a_MLB_during_mvor_closed = (false);
				}

				if( bsr->latest_mlb_record.there_was_a_MLB_during_all_other_times == (true) )
				{
					Alert_mainline_break_cleared( lgid );
					
					bsr->latest_mlb_record.there_was_a_MLB_during_all_other_times = (false);
				}
			}
			else
			{
				// 5/16/2012 rmd : Error. Couldn't find a system preserves record for the gid.
				rv = (false);
				
				break;  // from the for loop
			}
			
		}  // of for each record
		
	}
	else
	{
		// 5/16/2012 rmd : range check failure on record count.
		rv = (false);
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION When the TOKEN is being built this function is called to test if we have
    any mlb info to include in the token. This information is sent in each and every token.
    As this is such an important event I prefer to keep sending the information. The IRRI
    machine interprets the lack of any mlb info to mean there is no mlb.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task.
	
    @RETURN The length of this piece of the message. Zero if not to include within the
    message.

	@ORIGINAL 2012.05.14 rmd

	@REVISIONS
*/
extern UNS_32 FOAL_IRRI_load_mlb_info_into_outgoing_token( UNS_8 **pmlb_info_data_ptr )
{
	UNS_32	rv;
	
	rv = 0;
	
	UNS_8	*ucp, record_count;
	
	// 5/14/2012 rmd : Get the worst case size. The 1 is for the byte that says how many records
	// will be in the message. Even though we get the worst case size, only the needed amount is
	// actually placed within the message.
	*pmlb_info_data_ptr = mem_malloc( 1 + (MAX_POSSIBLE_SYSTEMS * sizeof(UNS_32) * sizeof(SYSTEM_MAINLINE_BREAK_RECORD)) );
	
	// 5/15/2012 rmd : This is where the record_count goes. Also use this variable to release
	// the memory if needed.
	ucp = *pmlb_info_data_ptr;

	record_count = 0;
	
	ucp += sizeof( UNS_8 );
	
	// 5/3/2012 rmd : We reference the system preserves. Make sure they are stable.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
	
	INT_32	i;
	
	for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
	{
		// 5/14/2012 rmd : If this record is in use.
		if( system_preserves.system[ i ].system_gid != 0 )
		{
			// 7/17/2014 rmd : Only include the system record if a mainline break exists. Look at the
			// LATEST mlb record here on the foal side. This is our mechanism for clearing the mlb
			// condition. If the TOKEN doesn't explicitly contain the mlb data there isn't a mlb!
			if( SYSTEM_PRESERVES_there_is_a_mlb( &(system_preserves.system[ i ].latest_mlb_record) ) )
			{
				// Since the system GID is not part of the structure, include it
				// first.
				memcpy( ucp, &system_preserves.system[ i ].system_gid, sizeof(UNS_32) );
				ucp += sizeof(UNS_32);
				rv += sizeof(UNS_32);

				memcpy( ucp, &system_preserves.system[ i ].latest_mlb_record, sizeof(SYSTEM_MAINLINE_BREAK_RECORD) );
				ucp += sizeof(SYSTEM_MAINLINE_BREAK_RECORD);
				rv += sizeof(SYSTEM_MAINLINE_BREAK_RECORD);

				record_count++;
			}
		}
	}
	
	if( rv == 0 )
	{
		// 5/14/2012 rmd : Free the memory and set the ptr to NULL to signify nothing to send in the
		// message.
		mem_free( *pmlb_info_data_ptr );
		
		*pmlb_info_data_ptr = NULL;
	}
	else
	{
		// 5/14/2012 rmd : Up the size to include the record_count byte which has not yet been
		// included into the length.
		rv += sizeof( UNS_8 );
		
		// 5/14/2012 rmd : Place the count of records into the message. It's okay to directly write
		// to this pointer location cause it's just a byte. No alignment concerns.
		**pmlb_info_data_ptr = record_count;
	}
	
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function is called when adding an ilc to the main irrigation
    list. There is no MUTEX activity within this function. The caller should own the program
    data mutex prior to this call.
	
    @EXECUTED Executed within the context of the TD_CHECK task via a start time detection.
*/ 
extern UNS_32 FOAL_IRRI_translate_alert_actions_to_flow_checking_group( BIG_BIT_FIELD_FOR_ILC_STRUCT *pbbf_ptr )
{
	UNS_32	rv;
	
	rv = 0;

	// Alert Shutoff / Alert Only variables. If you are going to alert you have to test.
	BOOL_32	ASO_AO_HI, ASO_AO_LO;

	// Set the FLOW GROUP this station operates under.
	if( pbbf_ptr->flow_check_hi_action == ALERT_ACTIONS_NONE )
	{
		ASO_AO_HI = (false);
	}
	else
	{
		ASO_AO_HI = (true);  // its set to either alert/shutoff or alert only
	}

	// 4/1/2018 rmd : WOW - years after original release I discovered this LOW FLOW action check
	// below was using the 'flow_check_hi_action', INSTEAD of using 'flow_check_lo_action'. I
	// have fixed this now!!!
	if( pbbf_ptr->flow_check_lo_action == ALERT_ACTIONS_NONE )
	{
		ASO_AO_LO = (false);
	}
	else
	{
		ASO_AO_LO = (true);  // its set to either alert/shutoff or alert only
	}

	// Now set the group number based on the settings of the 2 ASO_AO vars.
	if( (ASO_AO_HI == (true)) &&  (ASO_AO_LO == (true)) )
	{
		rv = FLOW_CHECK_GROUP_check_for_high_and_low_flow;
	}
	else
	if ( (ASO_AO_HI == (true)) &&  (ASO_AO_LO == (false)) )
	{
		rv = FLOW_CHECK_GROUP_check_for_high_flow_only;
	}
	else
	if ( (ASO_AO_HI == (false)) &&  (ASO_AO_LO == (true)) )
	{
		rv = FLOW_CHECK_GROUP_check_for_low_flow_only;
	}
	else
	if ( (ASO_AO_HI == (false)) &&  (ASO_AO_LO == (false)) )
	{
		rv = FLOW_CHECK_GROUP_no_flow_checking;
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 FOAL_IRRI_we_test_flow_when_ON_for_this_reason( UNS_32 const preason_in_list )
{
	BOOL_32   rv;
	
	rv = (true);
	
	// 5/29/2012 rmd : So we always test flow except for MOBILE, WALK_THRU, and true MANUAL.
	
	// 7/16/2015 rmd : Richard and I had a nice discussion. And we agreed Manual Programs should
	// probably go ahead and test flow. We finally reached the point of "ahhh why not ... we're
	// directing people to use it for irrigation and if their is a break we supposed it should
	// report that".

	if( preason_in_list == IN_LIST_FOR_MOBILE )
	{
		// 8/4/2015 rmd : AJ and I think it is better to test flow when turning ON a station with
		// the mobile. Test when ONLY ONE valve on to keep the hunt process sane looking for the
		// user with the mobile when standing there (remember we go through a process). HOWEVER do
		// not enable this until the MOBILE status updates contain HIGH or LOW flow info for
		// stations.
		rv = (false);
	}					
	else
	if( preason_in_list == IN_LIST_FOR_WALK_THRU )
	{
		rv = (false);
	}					
	else
	if( preason_in_list == IN_LIST_FOR_MANUAL )
	{
		rv = (false);
	}					

	return( rv );	
}				

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Used when adding a new ilc to the list of ilc's. We are looking through the
    battery backed ilc list held by the foal side within foal_irri.

    @CALLER MUTEX REQUIREMENTS The caller of this function must hold the
    list_foal_irri_recursive_MUTEX. As we are counting on a stable list subsequent to the
    determination made within this function.
	
    @EXECUTED Executed within the context of the TD_CHECK task. And may also be executed
    from within the COMM_MNGR task when the communications contains for example a request to
    test a valve.
	
    @RETURN A pointer to the next ilc to use for the new station. Returns NULL if none
    available. Which would mean more than 768 stations queued up to irrigate! Which could
    happen if you hit a start time for all stations then asked to test a valve (for
    example).
	
	@AUTHOR 2011.10.28 rmd
*/
static IRRIGATION_LIST_COMPONENT *nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc( void )
{
	IRRIGATION_LIST_COMPONENT	*rv;

	INT_32						i;

	rv = NULL;
	
	for( i=0; i<N_ARRAY_ELEMENTS( foal_irri.ilcs ); i++ )
	{
		if( foal_irri.ilcs[ i ].list_support_foal_all_irrigation.pListHdr == NULL )
		{
			// 10/25/2012 rmd : Is it still on the ACTION_NEEDED list? It is possible to be out of the
			// main irrigation list but still on the action needed list. (Whenever a station has
			// completed programmed irrigation. It is removed from the main list while still being on
			// the ACTION_NEEDED list. Until the next token has been made. Then it is removed from the
			// action needed list.) So if not on the action needed list should be a cadidate.
			if( foal_irri.ilcs[ i ].list_support_foal_action_needed.pListHdr == NULL )
			{
				// 10/25/2012 rmd : Sanity check. Should not be on the ON list at this point! If it is,
				// still use the item so they don't build up. We are going to clean it.
				if( (foal_irri.ilcs[ i ].list_support_foal_stations_ON.pListHdr != NULL) )
				{
					// Alert about this. Should never see.
					ALERT_MESSAGE_WITH_FILE_NAME( "FOAL: dirty list item." );
				}
				
				// ----------
				
				rv = &foal_irri.ilcs[ i ];
	
				// 10/12/2012 rmd : Fully clean it. There may be residual bits set in the ilc big bit field
				// for example. Always return a clean ilc.
				memset( &foal_irri.ilcs[ i ], 0, sizeof(IRRIGATION_LIST_COMPONENT) );
				
				break;
			}
		}
	}
	
	if( rv == NULL )
	{
		Alert_Message( "FOAL: Station Skipped (list full)" );
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */

typedef struct
{
	// 10/4/2012 rmd : As we add and add stations to the main foal irrigation list it may become
	// full. It is limited to 768 stations simulataneously trying to irrigate. Perhaps for
	// different reasons. When full new stations to add are dropped.
	BOOL_32		results_irrigation_list_full;
	
	// ----------
	// 10/4/2012 rmd : Typically the function to add to the list is called repeatedly. And this
	// structure tracks the net result of having attempted to irrigate several stations. This
	// flag reveals if during the attempts session any single valve did end up irrigating.
	BOOL_32		results_at_least_one_valve_irrigated;
	
	// ----------

	// 10/4/2012 rmd : All irrigation reasons check if there is a MLB of any sort. And prohibit
	// adding to the list if so. So there is no variable here providing direction. There is
	// however a variable indicating if a MLB prevented the station from being added.
	BOOL_32		results_skipped_as_there_is_a_MLB;

	// ----------

	// 10/4/2012 rmd : All irrigation reasons check if the MV is closed by virtue of a MVOR. And
	// prohibit adding to the list if so. So there is no variable here providing direction.
	// There is however a variable indicating if the MVOR condition prevented the station from
	// being added.
	BOOL_32		results_skipped_as_there_is_a_MVOR_CLOSED;

	// ----------

	// 10/4/2012 rmd : Not all reasons to irrigate pay attention to if the controller is set to
	// ignore scheduled starts (turned OFF). For example RRE and TEST both still work. I think
	// MANUAL will work too. But certainly PROGRAMMED IRRIGATION and MANUAL PROGRAMS will not
	// irrigate.
	BOOL_32		directions_honor_controller_set_to_OFF;
	BOOL_32		results_skipped_as_the_controller_is_set_to_OFF;

	// ----------

	// 10/4/2012 rmd : Not all reasons to irrigate honor the manual now DAYS. For example RRE
	// and TEST both still work. I think MANUAL will work too. But certainly PROGRAMMED
	// IRRIGATION and MANUAL PROGRAMS will not irrigate.
	BOOL_32		directions_honor_MANUAL_NOW;
	BOOL_32		results_skipped_due_to_MANUAL_NOW;

	// ----------

	// 10/4/2012 rmd : Not all reasons to irrigate honor the calendar now DAYS. For example RRE
	// and TEST both still work. I think MANUAL will work too. But certainly PROGRAMMED
	// IRRIGATION and MANUAL PROGRAMS will not irrigate.
	BOOL_32		directions_honor_CALENDAR_NOW;
	BOOL_32		results_skipped_due_to_CALENDAR_NOW;

	// ----------

	// 10/4/2012 rmd : Not all reasons to irrigate honor the rain switch, freeze switch, and
	// wind conditions. For example RRE and TEST both still work. I think MANUAL will work too.
	// But certainly PROGRAMMED IRRIGATION and MANUAL PROGRAMS will not irrigate.
	BOOL_32		directions_honor_RAIN_SWITCH;
	BOOL_32		results_skipped_due_to_RAIN_SWITCH;
	

	BOOL_32		directions_honor_FREEZE_SWITCH;
	BOOL_32		results_skipped_due_to_FREEZE_SWITCH;
	

	BOOL_32		directions_honor_WIND_PAUSE;
	BOOL_32		results_skipped_due_to_WIND_PAUSE;
	
	// ----------

	// 10/5/2012 rmd : Only MANUAL_PROGRAMS and PROGRAMMED_IRRIGATION consume the rain time.
	BOOL_32		directions_consume_RAIN_TIME;
	BOOL_32		results_skipped_due_to_RAIN_TIME;
	BOOL_32		results_reduced_due_to_RAIN_TIME;
	
	// ----------

	// 10/5/2012 rmd : The flag to look at if the minimum has been crossed, we've been polled
	// to, or the rain table has an R in it. If set (true) and there is rain in the table the
	// LEFT-OVER is set to 0.
	BOOL_32		directions_honor_RAIN_TABLE;
	BOOL_32		results_skipped_due_to_RAIN_TABLE;
	
	// ----------

	// 11/13/2013 ajv : All irrigation reasons check if a Two-Wire cable is
	// shorted. And prohibit adding to the list if so. So there is no variable
	// here providing direction.
	BOOL_32		results_skipped_due_to_2_WIRE_CABLE_OVERHEATED;

	// ----------
	
	BOOL_32		results_skipped_due_to_WATERSENSE_MIN_CYCLE;

	// ----------

	// 11/23/2015 ajv : This flag is used ONLY during PROGRAMMED_IRRIGATION and skips irrigation
	// if the MB is greater than 50%.
	BOOL_32		directions_honor_MOISTURE_BALANCE;
	BOOL_32		results_skipped_due_to_MOISTURE_BALANCE;

	// ----------

} ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT;

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION All stations attempting to irrigate in ANY way MUST pass through this
    function. The caller must already possess the foal list MUTEX (though we take it again)
    because the caller has taken the next available ilc and it isn't all the way filled out.
    Seems best to keep the list frozen.

    This function is invoked via two methods. First is local start time detection. For that
    matter any scheduled start detection. The second would be a UI initiated request to run
    a station or group of stations. This request would arrive with the token.
	
    Following sucessful addition to the main foal irri list such entries (in an abreviated
    form) must be sent to each controller in the chain. This allows for reports of what is
    running to be shown at any controller but also to provide the saftey of making sure
    stations ON are turned OFF in a timely fashion when something goes wrong. A code bug of
    communication fault.

    @param pilc_ptr is the next available ilc acquired using the get next function.
    @param pss_ptr is a pointer to the STATION_STRUCT for the station we are adding
    @param psgs_ptr is a pointer to the STATION_GROUP_GROUP_STRUCT for the station we are adding.
    It is used if the tail-ends caluclation is required. And is therefore set to NULL except
    when adding a station for PROGRAMMED IRRIGATION or MANUAL.
	 
    @CALLER MUTEX REQUIREMENTS The caller is expected to be holding THREE mutexs!!! The
    program_data MUTEX. The station_preserves MUTEX. And the foal_irri MUTEX.

    @EXECUTED Executed within the context of the TD_CHECK task. And the COMM_MNGR task as
    requests come in to turn stations ON. Such as for TEST. And from the TD_CHECK task via
    our check for start times.

    @RETURN Returns (true) if added to the foal irri list. Returns (false) if not. When
    returns (false) there will not be irrigation for that station. There are many reasons
    why it may not be added tot he list. Such as there is a MLB. Or NOW days. Also for the
    case of being in the list for MANUAL_PROGRAM_1 or 2 also returns (true) if already found
    in the list and we adjusted the remaining time.
	
	@AUTHOR 2012.10.10 rmd
*/
static BOOL_32 _nm_nm_nm_add_to_the_irrigation_list(	IRRIGATION_LIST_COMPONENT *const pilc_ptr,
														STATION_STRUCT *const pss_ptr,
														STATION_GROUP_STRUCT *const psgs_ptr, 
														ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT *const pd_and_r_ptr )
{	// 03/01/2016 skc : Prevent FP bug
	const volatile float SIXTY_POINT_ZERO = 60.0;

	BOOL_32	rv;
	
	UNS_32	lstation_preserves_index;

	BOOL_32	irrigation_denied;

	rv = (false);
	
	// The following items MUST already be filled out in the ILC.

	//  STATION NUMBER

	//  BOX_INDEX_0

	//  REASON IN LIST (weighting)

	//  REQUESTED TIME

	//  CYCLE TIME

	//  SET EXPECTED (weighting)

	//  DID NOT IRRIGATE LAST TIME (weighting)

	//  STOP TIME & DATE (only used for PROGRAMMED IRRIGATION)
	
	
	// 2011.12.12 rmd : We need the bsr pointer to see the state of a few things in the system.
	// In addition we keep it in the ilc for convenience. That way we don't have to keep looking
	// it up each time we want to know the system this station is attached to.
	pilc_ptr->bsr = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( STATION_GROUP_get_GID_irrigation_system_for_this_station( pss_ptr ) );

	if( (pilc_ptr->bsr != NULL) && STATION_PRESERVES_get_index_using_ptr_to_station_struct( pss_ptr, &lstation_preserves_index ) )
	{
		irrigation_denied = (false);

		// ----------
		
		if( pd_and_r_ptr->directions_consume_RAIN_TIME )
		{
			// 6/28/2012 rmd : Bounce the irrigation time against any remaining rain time. If we are
			// supposed to consume rain we always want to consume rain. Even though this may be a NOW
			// day. Play the rain time against the irrigation time and irrigate the balance if any.
			if( station_preserves.sps[ lstation_preserves_index ].rain_minutes_10u > 0 )
			{
				// If there is rain we will not irrigate at all (cause there's enough rain time) or we will
				// irrigate whats left of the irrigation_seconds.
				if( ((station_preserves.sps[ lstation_preserves_index ].rain_minutes_10u) * 6) >= pilc_ptr->requested_irrigation_seconds_ul )
				{
					station_preserves.sps[ lstation_preserves_index ].rain_minutes_10u -= (pilc_ptr->requested_irrigation_seconds_ul / 6);
	
					pd_and_r_ptr->results_skipped_due_to_RAIN_TIME = (true);
					
					irrigation_denied = (true);
				}
				else
				{
					pilc_ptr->requested_irrigation_seconds_ul -= ((station_preserves.sps[ lstation_preserves_index ].rain_minutes_10u) * 6);
					
					station_preserves.sps[ lstation_preserves_index ].rain_minutes_10u = 0;

					pd_and_r_ptr->results_reduced_due_to_RAIN_TIME = (true);
				}
	
			}  // of if we have some rain time
		}

		// ----------
		
		// 10/3/2012 rmd : Now check for the real-time rain possiblities.
		if( pd_and_r_ptr->directions_honor_RAIN_TABLE )
		{
			// 10/12/2012 rmd : Set the flag in the ilc so if the RAIN_TABLE crosses the minimum or we
			// are polled to AFTER we have started irrigating we will STOP! The foal irrigation list
			// maintenance function looks at this setting.
			pilc_ptr->bbf.directions_honor_RAIN_TABLE = (true);
			
			// ----------
			
			RAIN_TABLE_ENTRY	rain_entry;
			
			// 9/24/2012 rmd : Flag used to prevent code duplication for each case below.
			BOOL_32	rain_causes_irrigation_to_be_skipped;

			rain_causes_irrigation_to_be_skipped = (false);
			
			if( WEATHER_TABLES_get_rain_table_entry_for_index( 1, &rain_entry ) )
			{
				// 6/17/2015 rmd : David Byma and I have decided that if there is an 'R' in slot 1 don't
				// irrigate at all as that means recent rain and more could be happening and we want to
				// error on the side of not irrigating in the rain. This was actually a decision made
				// regarding how the 2000e works and carried through here to the CS3000.
				//
				// 7/22/2015 rmd : Additionally if rain we delivered via communications the implication is
				// that such rain is past the minimum and counted at face value. BEWARE: The CS3000
				// commserver shares WEATHERSENSE rain even though it is a zero. So we must look to see if
				// the value is non-zero when deciding to skip irrigation. Should this be just any amount of
				// rain to skip irrigation? What about a minimum threshold? Well I'm going to code this up
				// so ANY rain from weathersense causes us to skip irrigation.
				//
				// 7/27/2015 rmd : The SHARED_FROM_CENTRAL status is only delivered if the controller with
				// the bucket saw an R in the table. Otherwise when the rain data is provided to the
				// commserver by the -RB controller the status left set at BELOW_MINIMUM.
				if( (rain_entry.status == RAIN_STATUS_FROM_RAIN_BUCKET_R) ||
				    (rain_entry.status == RAIN_STATUS_SHARED_FROM_THE_CENTRAL) ||
					((rain_entry.status == RAIN_STATUS_FROM_WEATHERSENSE) && (rain_entry.rain_inches_u16_100u > 0))
				  )
				{
					rain_causes_irrigation_to_be_skipped = (true);
				}
			}
			
			// ----------
			
			// 6/16/2015 rmd : Has either the accumulated rain so far crossed the minimum or has the
			// commserver sent to us a rain shutdown command? If so skip this irrigation. For both reads
			// of the weather_preserves data no MUTEX is needed. They are atomic operations.
			if( weather_preserves.rain.inhibit_irrigation_have_crossed_the_minimum || (weather_preserves.rain.rain_shutdown_rcvd_from_commserver_uns32 != 0) )
			{
				rain_causes_irrigation_to_be_skipped = (true);
			}
			
			// ----------

			if( rain_causes_irrigation_to_be_skipped )
			{
				pd_and_r_ptr->results_skipped_due_to_RAIN_TABLE = (true);
				
				irrigation_denied = (true);
			}
		}

		// ----------

		if( pd_and_r_ptr->directions_honor_RAIN_SWITCH )
		{
			// 11/13/2014 rmd : For foal irri list maintenance function.
			pilc_ptr->bbf.directions_honor_RAIN_SWITCH = (true);

			if( weather_preserves.rain_switch_active )
			{
				pd_and_r_ptr->results_skipped_due_to_RAIN_SWITCH = (true);

				irrigation_denied = (true);
			}
		}

		// ----------

		if( pd_and_r_ptr->directions_honor_FREEZE_SWITCH )
		{
			// 11/13/2014 rmd : For foal irri list maintenance function.
			pilc_ptr->bbf.directions_honor_FREEZE_SWITCH = (true);

			if( weather_preserves.freeze_switch_active )
			{
				pd_and_r_ptr->results_skipped_due_to_FREEZE_SWITCH = (true);

				irrigation_denied = (true);
			}
		}

		// ----------
		
		if( pd_and_r_ptr->directions_honor_WIND_PAUSE )
		{
			// 11/13/2014 rmd : For foal irri list maintenance function.
			pilc_ptr->bbf.directions_honor_WIND_PAUSE = (true);
			
			// 11/12/2014 rmd : If PAUSED should we skip the irrigation? Or should we load up and pause
			// it? In discussion with Richard and AJ we decided to load it up and let it pause. This way
			// a momentary high wind condition will not lead to a whole lost irrigation. WE ALSO decided
			// that if the user is using a wind gage that he will be forced to use a STOP time for each
			// of the station groups that are set to respond to wind.
		}

		// ----------

		// 11/11/2014 rmd : The irri_comm version of the cable overheated variable is valid at each
		// controller and therefore is okay to use here. It is a bit different in that it feels as
		// if we are breaking the foal / irri separation. Though in fact, IN THIS CASE, it is
		// legitiment to use the irri variable anywhere. Which is probably a new concept to be
		// carefully variable by variable considered.
		if( irri_comm.two_wire_cable_overheated[ pilc_ptr->box_index_0 ] )
		{
			pd_and_r_ptr->results_skipped_due_to_2_WIRE_CABLE_OVERHEATED = (true);

			irrigation_denied = (true);
		}

		// ----------

		// 10/4/2012 rmd : No direction decision needed. ALL stations are denied if there is a
		// MLB.
		if( SYSTEM_PRESERVES_there_is_a_mlb( &(pilc_ptr->bsr->latest_mlb_record) ) )
		{
			// 8/28/2014 rmd : Nuisance alert. Commentted out!
			//Alert_Message_va( "Ser No: %u, Station %u, : Skipped due to a MAINLINE BREAK", pilc_ptr->serial_number_ul, (pilc_ptr->station_number_0_u8 + 1) );
			
			pd_and_r_ptr->results_skipped_as_there_is_a_MLB = (true);
			
			irrigation_denied = (true);
		}

		// ----------

		// 10/4/2012 rmd : No direction decision needed. ALL stations are denied if the MV is closed
		// by virtue of a MVOR.
		if ( pilc_ptr->bsr->sbf.MVOR_in_effect_closed == (true) )
		{
			// 8/28/2014 rmd : Nuisance alert. Commentted out!
			//Alert_Message_va( "Ser No: %u, Station %u, : Skipped because MASTER VALVE IS CLOSED", pilc_ptr->serial_number_ul, (pilc_ptr->station_number_0_u8 + 1) );

			pd_and_r_ptr->results_skipped_as_there_is_a_MVOR_CLOSED = (true);
			
			irrigation_denied = (true);
		}

		// ----------
		
		// 10/4/2012 rmd : Not all reasons to turn valves ON honor if the controller is set to skip
		// scheduled starts. For example TEST doesn't.
		if( pd_and_r_ptr->directions_honor_controller_set_to_OFF == (true) )
		{
			// ----------
	
			// 10/12/2012 rmd : Set the flag to honor after irrigation started.
			pilc_ptr->bbf.directions_honor_controller_set_to_OFF = (true);
			
			// ----------
			
			if( NETWORK_CONFIG_get_controller_off() == (true) )
			{
				pd_and_r_ptr->results_skipped_as_the_controller_is_set_to_OFF = (true);
				
				irrigation_denied = (true);
			}
		}

		// ----------
		
		// 10/4/2012 rmd : Not all reasons to turn valves ON honor manual NOW days. For example TEST
		// doesn't.
		if( pd_and_r_ptr->directions_honor_MANUAL_NOW == (true) )
		{
			// ----------
	
			// 10/12/2012 rmd : Set the flag to honor after irrigation started.
			pilc_ptr->bbf.directions_honor_MANUAL_NOW = (true);
			
			// ----------
			
			// 10/3/2012 rmd : Is the flag set indicating a manual NOW period is in effect?
			if( station_preserves.sps[ lstation_preserves_index ].spbf.skip_irrigation_due_to_manual_NOW == (true) )
			{
				pd_and_r_ptr->results_skipped_due_to_MANUAL_NOW = (true);
				
				irrigation_denied = (true);
			}
		}

		// ----------
		
		// 10/4/2012 rmd : Not all reasons to turn valves ON honor calendar NOW days. For example
		// TEST doesn't.
		if( pd_and_r_ptr->directions_honor_CALENDAR_NOW == (true) )
		{
			// ----------
	
			// 10/12/2012 rmd : Set the flag to honor after irrigation started.
			pilc_ptr->bbf.directions_honor_CALENDAR_NOW = (true);
			
			// ----------
			
			// 10/3/2012 rmd : Is the flag set indicating a manual NOW period is in effect?
			if( station_preserves.sps[ lstation_preserves_index ].spbf.skip_irrigation_due_to_calendar_NOW == (true) )
			{
				pd_and_r_ptr->results_skipped_due_to_CALENDAR_NOW = (true);
				
				irrigation_denied = (true);
			}
		}

		// ----------

		// 7/9/2015 ajv : Now let's check whether our moisture balance level is going to prevent us
		// from irrigating.
		if( pd_and_r_ptr->directions_honor_MOISTURE_BALANCE )
		{
			// 4/18/2016 ajv : If this flag is set, we're going to force the moisture balance back to
			// 50% at the conclusion of irrigation. Because this is being used to correct errant
			// moisture balance values due to bugs in the run time calculation, we can't rely on the
			// current value, therefore we're going to skip the moisture balance check if it's set.
			if( nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag(pss_ptr) == (false) )
			{
				if( STATION_get_moisture_balance_percent(pss_ptr) > WATERSENSE_MAXIMUM_MOISTURE_BALANCE_ALLOWED_FOR_IRRI )
				{
					pd_and_r_ptr->results_skipped_due_to_MOISTURE_BALANCE = (true);

					irrigation_denied = (true);
				}
			}
		}

		// ----------

		if( !irrigation_denied )
		{
			// Preparation to add it to the list after it is filled out.
			xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );
			
			// 6/27/2012 rmd : We're going to loop through turning OFF. Only make a flow recording line
			// for the first group (if not already made).
			foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (false);
		
			// FIRST look for it to be in the list already for the same reason. And if so REMOVE it. We
			// just remove it. And then proceed. The IRRI side will perform the same activity when
			// adding to it's list. Which happens after us here and as the next token makes it's way to
			// each irri machine. We are moving down the path that all stations in the list show at all
			// controllers in the NETWORK.
			IRRIGATION_LIST_COMPONENT	*lilc_ptr, *ilc_ptr_copy;
			
			BOOL_32	add_to_the_list, delete_this_ilc;
			
			add_to_the_list = (true);

			lilc_ptr = nm_ListGetFirst( &(foal_irri.list_of_foal_all_irrigation) );
	
			while( lilc_ptr != NULL )
			{
				delete_this_ilc = (false);

				// What constitutes a match? Same reason in the list, same box_index, and same station
				// number. If however we have the reason for SMS_1 or SMS_2 we will add to the time as
				// opposed to removing the item.
				if( (lilc_ptr->box_index_0 == pilc_ptr->box_index_0) && (lilc_ptr->station_number_0_u8 == pilc_ptr->station_number_0_u8) && (lilc_ptr->bbf.w_reason_in_list == pilc_ptr->bbf.w_reason_in_list) )
				{
					if( lilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_MANUAL_PROGRAM )
					{
						// Then we want to ADD to the TIME in the existing ilc in the list. DO NOT add a new item to
						// the list. But should remove and re-insert into the list to compensate for the new time
						// changing the weighting.
						add_to_the_list = (false);
						
						// Add to the time left to go. This is the case where we've encountered a new SMS start time
						// while SMS irrigation was on-going. This can happen for example if multiple starts are set
						// to the same time. We WILL remove and re-insert this item at the end of the function to
						// incorporate the change in weighting that may have occurred.
						lilc_ptr->requested_irrigation_seconds_ul += pilc_ptr->requested_irrigation_seconds_ul;
						
						// 10/5/2012 rmd : We are not adding this to the list as new. However we should
						// re-distribute this ilc to all the irri machine to cause an update in their total
						// remaining time. The irri side will search for the station in its list and if found then
						// for this case of Manual Programs will only add to the requested time. If the station is
						// presently ON that is okay. Its ON/OFF status is unaffected.
						lilc_ptr->bbf.xfer_to_irri_machines = (true);
						
						// ----------
						
						// 7/16/2015 rmd : And what about making sure the pilc_ptr item, which has been pulled from
						// our ilc pool, can be re-used at a later date. Well that should be fine as the ilc has yet
						// to be added to any list. But clean it just to be sure. That is NULL the fields that
						// define it as available.
						pilc_ptr->list_support_foal_all_irrigation.pListHdr = NULL;

						pilc_ptr->list_support_foal_action_needed.pListHdr = NULL;

						pilc_ptr->list_support_foal_stations_ON.pListHdr = NULL;
						
						// ----------
						
						// And indicate this was handled normally.
						rv = (true);
					}
					else
					{
						// 10/11/2012 rmd : For all others delete the existing record. And then re-add it as NEW.
						delete_this_ilc = (true);
					}
				}
				
				if( delete_this_ilc == (true) )
				{
					// 6/7/2012 rmd : The turn_OFF function requires this MUTEX be held by the caller.
					xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

					// 11/19/2014 rmd : Get a copy because the lilc_ptr isn't valid on the other side of the
					// removal function call.
					ilc_ptr_copy = lilc_ptr;
					
					// 11/13/2014 rmd : And delete the item from the list. Hmmmm ... how do we tell the irri
					// side this item is gone? We do not have to tell the irri side. It goes through the same
					// process of looking for a match when adding it's abbreviated station records to its own
					// list.
					lilc_ptr = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), lilc_ptr, ACTION_REASON_INTERNAL_TURN_OFF_AND_REMOVE_list_dup_removal );
					
					// 11/13/2014 rmd : Note the station WILL be on the action_needed list by definition. And we
					// cannot allow it to be because the token will add the new station to the list and then
					// immediately within the same token remove it from the list. That is the order of the token
					// content. And we need it that way so that we can add and then turn ON within the same
					// token. Whcih I think can easily occur on bigger slower toekn moving chains.
					nm_ListRemove( &foal_irri.list_of_foal_stations_with_action_needed, ilc_ptr_copy );

					xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
				}
				else
				{
					lilc_ptr = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), lilc_ptr );
				}
	
			}  // End of going through the existing list looking for dups or SMS entries.
			
			if( add_to_the_list )
			{
				// Make sure the NEW list item is filled out ENTIRELY!!
					
				// 2011.12.12 rmd : And again for convenience we set this.
				pilc_ptr->station_preserves_index = lstation_preserves_index;
				
				// -------------------------
				
				pilc_ptr->bbf.station_is_ON = (false);
				
				// -------------------------
				
				// 10/5/2012 rmd : Having a soak time obviously affects the delay between cycles. And for
				// the cases of PROGRAMMED IRRIGATION and MANUAL we desire that behaviour. Not for MANUAL
				// PROGRAMS. The soak time also affects the behaviour on the other side of a power fail if
				// the station was irrigating. For some reaons in the list we would like to see the valve
				// come right back ON. If there is a soak time that won't happen. Setting to 0 here creates
				// that behaviour.
				if( (pilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION) || (pilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_MANUAL) )
				{
					pilc_ptr->soak_seconds_ul = (nm_STATION_get_soak_minutes( pss_ptr ) * 60);
				}
				else
				{
					pilc_ptr->soak_seconds_ul = 0;
				}
				
				// I suppose to delay turn on we could play with this.
				pilc_ptr->soak_seconds_remaining_ul = 0;
	
				pilc_ptr->remaining_seconds_ON = 0;

				// 02/11/2016 skc - Not all methods of running use budgets
				// Automatic, Manual Program, Manual, Test, Mobile, Walk Thru
				// 4/25/2016 skc : We decided Manual Programs do not get any time reductions.
				// They are still part of predicted volume.
				if( (IN_LIST_FOR_PROGRAMMED_IRRIGATION == pilc_ptr->bbf.w_reason_in_list) ) 
				{
					DATE_TIME dt;
					EPSON_obtain_latest_time_and_date( &dt );
					pilc_ptr->requested_irrigation_seconds_ul = 60 * nm_BUDGET_calc_time_adjustment( pilc_ptr->requested_irrigation_seconds_ul / SIXTY_POINT_ZERO, pss_ptr, dt );
				}
				
				// -------------------------
				
				pilc_ptr->bbf.w_uses_the_pump = PUMP_get_station_uses_pump( pss_ptr );
				
				// We carry the on at a time GID. And that is used to track the on at a time counts within
				// the actual on at a time (file) list.
				pilc_ptr->GID_on_at_a_time = STATION_get_GID_station_group( pss_ptr );
				
				// -------------------------
				
				// 6/11/2012 rmd : Obtain the alert action settings before the call to set the flow check
				// group. Because the function that accomplishes the flow check group translation counts on
				// the action settings being in place.
				pilc_ptr->bbf.flow_check_hi_action = ALERT_ACTIONS_get_station_high_flow_action( pss_ptr );

				pilc_ptr->bbf.flow_check_lo_action = ALERT_ACTIONS_get_station_low_flow_action( pss_ptr );
				
				pilc_ptr->bbf.flow_check_to_be_excluded_from_future_checking = (false);

				// ----------
				
				// 4/6/2018 rmd : The key here is to set the FLOW_CHECK_GROUP appropriately to allow flow
				// checking or not. The flow check group IS the mechanism that blocks flow checking in the
				// function that is actually trying to perform the flow check - NOT as one might think the
				// at_some_point_should_check_flow term, or the
				// flow_check_when_possible_based_on_reason_in_list term.
				if( FOAL_IRRI_we_test_flow_when_ON_for_this_reason( pilc_ptr->bbf.w_reason_in_list ) )
				{
					pilc_ptr->bbf.flow_check_group = FOAL_IRRI_translate_alert_actions_to_flow_checking_group( &pilc_ptr->bbf );

					pilc_ptr->bbf.flow_check_when_possible_based_on_reason_in_list = (true);
				}
				else
				{
					pilc_ptr->bbf.flow_check_group = FLOW_CHECK_GROUP_no_flow_checking;

					pilc_ptr->bbf.flow_check_when_possible_based_on_reason_in_list = (false);
				}
				
				// ----------
				
				pilc_ptr->bbf.w_involved_in_a_flow_problem = (false);
				
				pilc_ptr->bbf.at_some_point_flow_was_checked = (false);
				
				if( (pilc_ptr->bbf.flow_check_group != FLOW_CHECK_GROUP_no_flow_checking) && 
										(pilc_ptr->bsr->sbf.flow_checking_enabled_and_allowed) && 
																		 (pilc_ptr->bbf.flow_check_when_possible_based_on_reason_in_list) )
				{
					pilc_ptr->bbf.at_some_point_should_check_flow = (true);
				}
				else
				{
					pilc_ptr->bbf.at_some_point_should_check_flow = (false);
				}
				
				// -------------------------
				
				pilc_ptr->expected_flow_rate_gpm_u16 = nm_STATION_get_expected_flow_rate_gpm( pss_ptr );
	
				// ----------
				
				pilc_ptr->bbf.responds_to_wind = WEATHER_get_station_uses_wind( pss_ptr );
	
				pilc_ptr->bbf.responds_to_rain = WEATHER_get_station_uses_rain( pss_ptr );

				// 3/30/2016 rmd : Obtain the moisture sensor decoder serial number. The function will
				// return 0 if this station is not assigned to a station group, and 0 if assigned to a
				// station group not using moisture sensing.
				pilc_ptr->moisture_sensor_decoder_serial_number = STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group( psgs_ptr );
				
				// ----------
				
				pilc_ptr->bbf.station_priority = PRIORITY_get_station_priority_level( pss_ptr );
				
				// -------------------------
				
				pilc_ptr->line_fill_seconds = LINE_FILL_TIME_get_station_line_fill_seconds( pss_ptr );
				
				pilc_ptr->slow_closing_valve_seconds = DELAY_BETWEEN_VALVES_get_station_slow_closing_valve_seconds( pss_ptr );
										
				// -------------------------
				
				// 606.a rmd - For clarity set the new RRe activity bits low to signify not in those states.
				pilc_ptr->bbf.rre_station_is_paused = (false);
	
				pilc_ptr->bbf.rre_on_sxr_to_pause = (false);
	
				pilc_ptr->bbf.rre_on_sxr_to_turn_OFF = (false);
				
				pilc_ptr->bbf.rre_in_process_to_turn_ON = (false);
	
				if ( pilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE )
				{
					// The reason we are adding a radio remote station to the list is to turn it ON so ...
					pilc_ptr->bbf.rre_in_process_to_turn_ON = (true);
				}
				
				// Now add it to the end of the list.
				if( nm_ListInsertTail( &foal_irri.list_of_foal_all_irrigation, pilc_ptr ) == MIST_SUCCESS )
				{
					// And indicate this was handled normally.
					rv = (true);
					
					// ----------
					
					// 5/2/2012 rmd : Mark for inclusion in the next outgoing token. Remember the token (from
					// the master) is broadcast to all controllers in the chain.
					pilc_ptr->bbf.xfer_to_irri_machines = (true);
					
					// ----------
					
					// And order it properly based on the weighting criteria we define.
					FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list( NOT_FOR_FTIMES, pilc_ptr );
				}
			}
	
			xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );

		}  // of if irrigation has not been denied
	}
	else
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "FOAL: problem...can't add to list." );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function is to be called after it is determined that irrigation will
    occur...this function will set the actual stop DAY (i.e. date) which is to be carried
    into irrigation. And is what is used to actually see when the stop time has passed. Used
    for Programmed Irrigation and Manual Programs.
	
    @CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task at scheduled starts that use
    a STOP time (as mentioned in the description).

	@RETURN The STOP date as an UNS_32. Normally then used as an UNS_16 by the caller.

	@ORIGINAL 2012.05.09 rmd

	@REVISIONS (none)
*/
extern UNS_32 FOAL_IRRI_return_stop_date_according_to_stop_time( DATE_TIME const *const pcurrent_datetime, UNS_32 const pstop_time )
{
	UNS_32	rv;
	
	// This function is to be called after it is determined that irrigation will occur...this
	// function will set the actual stop DAY (i.e. date) which is to be carried into irrigation.
	// And is what is used to actually see when the stop time has passed.
	if( pstop_time == 86400 )  // dont have to explicitly check for 86400 but for clarity sakes
	{
		// If its OFF (not used) make a date that can't be reached. Not needed as the time already
		// can't be reached.
		rv = 0xFFFF;
	}
	else
	{
		if( pstop_time < pcurrent_datetime->T )
		{
			// The intention is the stop time is some time tomorrow.
			rv = ( (pcurrent_datetime->D) + 1 );
		}
		else
		{
			// The intention is the stop time is later today.
			rv = ( pcurrent_datetime->D );
		}
		
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Each day at the start time perform the maintenance on the manual NOW
    controls.

    @CALLER_MUTEX_REQUIREMENTS The caller must be holding the station_preserves MUTEX. We
    are reading and writing to the station_preserves.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (none)

	@ORIGINAL 2012.05.09 rmd

	@REVISIONS (none)
*/
static void nm_at_starttime_decrement_NOW_days_count( STATION_STRUCT *const pstation, UNS_32 const pstation_preserves_index, const DATE_TIME *const pcurrent_dt )
{
	if( pstation_preserves_index >= STATION_PRESERVE_ARRAY_SIZE )
	{
		Alert_index_out_of_range();  // Should never happen.
	}
	else
	{
		if( STATION_get_no_water_days( pstation ) > 0 )
		{
			// Remember the At_1Hz_Rate_Set_SkipIrrigationForNOW function will already have set
			// "skip_irrigation_due_to_manual_NOW" (true).
			
			if( STATION_decrement_no_water_days_and_return_value_after_decrement( pstation ) == 0 )
			{
				// 2011.09.09 rmd : So NOW is the time to set the don't irrigate till date and time!
				
				// 2011.09.09 rmd : If the time is less than or equal to 5AM the cutoff time is today. If
				// not, it's tomorrow.
				//
				// 6/28/2017 rmd : This works well for traditionally valued start times, values between 10PM
				// and 3AM. A 6AM start time end up giving an extra day of no water. Perhpas, algorithm
				// should be re-considered.
				if( pcurrent_dt->T <= (5 * 60 *60) )
				{
					station_preserves.sps[ pstation_preserves_index ].skip_irrigation_till_due_to_manual_NOW_date = pcurrent_dt->D;
				}
				else
				{
					station_preserves.sps[ pstation_preserves_index ].skip_irrigation_till_due_to_manual_NOW_date = ((pcurrent_dt->D) + 1);
				}
		
				// 2011.09.09 rmd : Set to 5AM. This used to be 4AM but D Byma wanted it to 5AM. So I did
				// that. I agreed.
				station_preserves.sps[ pstation_preserves_index ].skip_irrigation_till_due_to_manual_NOW_time = (5 * 60 *60);
			}
		}
		else
		{
			// SkipIrrigationDueToNOW is set FALSE in FIVE( 5 ) places:
			//
			// FIRST -	when the battery backed SRAM variables are reset (due to a failure or first time
			// 			startup) 
			//
			// SECOND -	at 5AM if NoWaterDays is a 0 (to allow the Manual Programs to run the day
			//          after a No Water Day)
			//
			// THIRD -	at a stations start time when the No Water Day count is zero
			//
			// FOURTH - when the user edits (either central or keyboard) NoWaterDays to a 0
			//
			// FIFTH - when the user edits the Manual Programs and the NoWaterDays count is 0
			
			// 6/27/2017 rmd : This particular clearing of the flag is probably not necessary, as the
			// flag is cleared at the established 'skip_irrigation_till' date and time. But since I
			// can't prove this particular clearing of the flag is not needed, I'm going to leave it in
			// place. It serves as a backup mechanism, if for some reason the flag remains set at the SX
			// even though the NOW day count is 0.
			station_preserves.sps[ pstation_preserves_index ].spbf.skip_irrigation_due_to_manual_NOW = (false);
		}
	}
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Each and every second perform maintenance on the manual NOW controls.

    @CALLER_MUTEX_REQUIREMENTS The caller must be holding the station_preserves MUTEX. We
    are reading and writing to the station_preserves.

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (none)

	@ORIGINAL 2012.05.09 rmd

	@REVISIONS (none)
*/
static void nm_at_1Hz_rate_set_skip_irrigation_due_to_NOW_days_and_check_for_isolated_stations(	STATION_STRUCT *const pstation_ptr,
																								UNS_32 const pstation_preserves_index,
																								const DATE_TIME *const pcurrent_dt,
																								STATION_GROUP_STRUCT **pstation_group_ptr_ptr )
{
	DATE_TIME				ldt;
				
	UNS_32					station_group_GID;

	BOOL_32					is_an_isolated_station;

	UNS_32					station_start_time;

	// ----------
	
	// 6/28/2017 rmd : Guarantee a return value for the station group pointer is set.
	*pstation_group_ptr_ptr = NULL;
	
	// ----------
	
	if( pstation_preserves_index >= STATION_PRESERVE_ARRAY_SIZE )
	{
		Alert_index_out_of_range();  // Should never happen.
	}
	else
	{
		if( STATION_get_no_water_days( pstation_ptr ) > 0 )
		{
			// 10/3/2012 rmd : This is important to set our NOW flag (true) here. So that from the
			// moment the guy sets any NOW days things like MANUAL_PROGRAMS will also be inhibited. We
			// count on this running every second to achieve this.
			station_preserves.sps[ pstation_preserves_index ].spbf.skip_irrigation_due_to_manual_NOW = (true);
		}
		else
		{
			// 2011.09.08 rmd : So the count is 0. And we are still set (true) to skip irrigation. If we
			// are past the cutoff time end the skip irrigation period.
			//
			// This mechanism allows the use of skip irrigation for other than just Programmed
			// Irrigation. We also wanted it to control Manual Program irrigations. But we didn't want
			// to block out the whole next day from irrigating (as the skip irrigation setting is
			// normally manipulated at the station start time. So we introduced this 5AM thing. So that
			// at 5AM the skip irrigation period would end. Allow Manual Programs to once again irrigate
			// the day after the Programmed Irrigation no water period ended. Without having to wait
			// till the NEXT start time.
			//
			// To avoid making the evaluation each and every second for all stations test if we need to.
			if( station_preserves.sps[ pstation_preserves_index ].spbf.skip_irrigation_due_to_manual_NOW )
			{
				ldt.D = (station_preserves.sps[ pstation_preserves_index ].skip_irrigation_till_due_to_manual_NOW_date); 
				ldt.T = (station_preserves.sps[ pstation_preserves_index ].skip_irrigation_till_due_to_manual_NOW_time); 
	
				if( DT1_IsBiggerThanOrEqualTo_DT2( pcurrent_dt, &ldt ))
				{
					station_preserves.sps[ pstation_preserves_index ].spbf.skip_irrigation_due_to_manual_NOW = (false);
				}
			}
		}
		
		// ----------
		
		// 6/27/2017 rmd : Isolated station check, what are isolated stations? These are stations
		// that are not part of a station group, OR they are part of a station group but do not have
		// a start time. In these cases we must 'pretend' the start time is at midnight, and
		// decrement the NOW days count then. If we didn't do this, being there is no start time the
		// NOW day count would never decrement for those stations.
		is_an_isolated_station = (false);
		
		station_group_GID = STATION_get_GID_station_group( pstation_ptr );

		if( station_group_GID > 0 )
		{
			*pstation_group_ptr_ptr = STATION_GROUP_get_group_with_this_GID( station_group_GID );

			if( *pstation_group_ptr_ptr == NULL )
			{
				// 6/27/2017 rmd : Should have been able to find the group. Something wrong, so cover this
				// case making sure any NOW days will count down.
				is_an_isolated_station = (true);
			}
			else
			{
				station_start_time = SCHEDULE_get_start_time( *pstation_group_ptr_ptr );
				
				if( station_start_time == SCHEDULE_OFF_TIME )
				{
					// 6/27/2017 rmd : This means the station has NO start time, and therefore the NOW days
					// won't count down at a start time.
					is_an_isolated_station = (true);
				}
			}
		}
		else
		{
			// 6/28/2017 rmd : The station doesn't belong to a station group, and therefore the NOW days
			// won't count down like normal at a station group start time.
			is_an_isolated_station = (true);
		}

		if( is_an_isolated_station )
		{
			// 6/28/2017 rmd : Act as if the start time is midnight, and call the function to decrement
			// the NOW days count.
			if( pcurrent_dt->T == 0 )
			{
				nm_at_starttime_decrement_NOW_days_count( pstation_ptr, pstation_preserves_index, pcurrent_dt );
			}
		}
	}  // of station preserves index is valid
}

/* ---------------------------------------------------------- */

// 2011.12.01 rmd : For debug purposes to measure how long it takes to do the processing for
// a start time when all 768 stations are starting. It takes quite some time. Like 2300 ms.
// Don't have a problem with that right now. The token stops for that period as the TD_CHECK
// task is at a higher priority than the COMM_MNGR task.
UNS_32	__largest_start_time_delta;

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called at a 1Hz rate. For each station in the system looks at it schedule
    to see if the start time is now. If so and after numberous other manipulation we are
    going to irrigate this function secures the next available foal_ilcs slot. Partially
    fills it out and then call the function to finish off adding it to the list of all
    stations waiting for irrigation. This function should be thought about as being on the
    FOAL side. It is called from within the TD_CHECK task. And only if we are making tokens
    and the iccc structure indicates system in sync.
	
	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (none)

	@AUTHOR 2011.10.25 rmd
*/
extern void TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start( DATE_TIME_COMPLETE_STRUCT const *const pdtcs_ptr )
{
	// 2/15/2018 rmd : Executed within the context of the TDCHECK task. Note that the ftimes
	// calculation does not use this function, it has it own start time detection function.
	
	// ----------
	
	// 10/10/2012 rmd : Three copies of the directions and results vtructure. The pi (programmed
	// irrigation)and mp (manual programs) copies are used to LATCH the RESULTS condition as the
	// entire list of stations is passed through. And then at the end after looping through all
	// stations single alert lines are produced to reflect conditions that occurred. The
	// 'station' copy of the d_and_r is used to support the programmed irrigation effort to set
	// the station history flags on individual stations.
	ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT	station_d_and_r;

	ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT   pi_overall_d_and_r;

	ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT	mp_overall_d_and_r;

	// ----------

	const volatile float SIXTY_POINT_ZERO = 60.0;

	// ----------
	
	BOOL_32			pi_alert_there_was_a_start;

	BOOL_32			pi_alert_some_skipped_due_to_stop_time;
	
	BOOL_32			pi_alert_some_skipped_due_to_mow_day;

	BOOL_32			pi_alert_some_skipped_due_to_not_being_in_use;

	BOOL_32			mp_alert_there_was_a_start;

	UNS_32			mp;

	UNS_32	number_of_mp_starts, manual_program_GID, mp_seconds;
	
	// ----------

	UNS_32			requested_irrigation_seconds_ul;

	DATE_TIME		lstop_datetime;
	
	// ----------
	
	STATION_STRUCT	*lss_ptr;

	STATION_GROUP_STRUCT	*lsgs_ptr;

	// ----------
	
	// 2/21/2013 ajv : In order to stamp substitution and limiting lines once, rather than for
	// every station, we need to store a station which is using daily ET to pass into the
	// routine which performs the substitution and limiting. This is a pointer to that station.
	STATION_STRUCT const			*lss_ptr_to_station_using_et;

	STATION_GROUP_STRUCT const		*lss_ptr_to_station_group_using_et;
	
	// ----------
	
	UNS_32							station_start_time;
	
	UNS_32							lstation_preserves_index;
	
	IRRIGATION_LIST_COMPONENT		*lilc_ptr, *ilc_on_action_list;

	BOOL_32							new_station_made_it_onto_the_foal_irrigation_list;

	// ----------

	UNS_32	i;

	// ----------

	// 6/30/2015 ajv : Rain related variable used for WaterSense Moisture Balance adjustment.
	RAIN_TABLE_ENTRY	rain_data;

	UNS_32	usable_rain_100u;

	float	RZWWS;

	float	MB;

	float	MBo;

	// 02/11/2016 skc - Flag to prevent multiple calls to calculate predicted volume usage
	BOOL_32 Vp_flag;

	// ----------

	Vp_flag = false;

	// 2011.09.07 rmd : Obtain the MUTEX that allows us to work with the lists of station data
	// and controller settings.
	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
	
	// 2011.09.23 rmd : And the MUTEX that allows work with the station_preserves array.
	xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 10/4/2012 rmd : Set the two LATCHING copies of the directions and results. We actually
	// are only using the 'results' fields from these two; the directions fields are ignored for
	// these two. (The directions and results fields are fully used for the 'station' copy of
	// this sturcutre.) Setting to (false) here and as we progress through the station some
	// 'results' may be set to (true). After done with the whole stations list we generate alert
	// lines for the flags set.
	memset( &pi_overall_d_and_r, 0x00, sizeof(pi_overall_d_and_r) );

	memset( &mp_overall_d_and_r, 0x00, sizeof(mp_overall_d_and_r) );

	// ----------

	pi_alert_there_was_a_start = (false);

	pi_alert_some_skipped_due_to_stop_time = (false);

	pi_alert_some_skipped_due_to_mow_day = (false);
	
	pi_alert_some_skipped_due_to_not_being_in_use = (false);
	
	mp_alert_there_was_a_start = (false);

	lss_ptr_to_station_using_et = NULL;

	lss_ptr_to_station_group_using_et = NULL;
	
	// ----------

	// 2011.12.01 rmd : For debug purposes to measure how long it takes to do the processing for
	// a start time when all 768 stations are starting. It takes quite some time. Like 2300 ms.
	// Don't have a problem with that right now. The token stops for that period as the TD_CHECK
	// task is at a higher priority than the COMM_MNGR task. On that note you may want to
	// suspend the scheduler to improve the accuracy of the measured time. But I've found it to
	// make little difference.
	UNS_32	__this_time_delta;

	__this_time_delta = my_tick_count;

	// ----------

	// 8/18/2016 skc : Clear the flag for all station groups so we can send budget alerts per
	// group at start time.
	STATION_GROUP_clear_budget_start_time_flags();

	// ----------

	lss_ptr	= nm_ListGetFirst( &station_info_list_hdr );
	
	while( lss_ptr != NULL )
	{
		// Capture this so we don't have to keep calulating it. And test that we can get a valid
		// index to use.
		if( STATION_PRESERVES_get_index_using_ptr_to_station_struct( lss_ptr, &lstation_preserves_index ) )
		{
			// 8/31/2015 rmd : At this loop indentation level we perform some station housekeeping
			// maintenance. Then check for a scheduled programmed irrigation start. And also check for
			// manual program starts.
			
			// ----------
			
			// 6/27/2012 rmd : Obtain these so they are available to work with.
			UNS_32	lstation_number_0, lbox_index_0;

			lbox_index_0 = nm_STATION_get_box_index_0( lss_ptr );

			lstation_number_0 = nm_STATION_get_station_number_0( lss_ptr );

			// ----------
			
			// 11/19/2014 rmd : To support the logic used when stations are still irrigating at their
			// next start time.
			new_station_made_it_onto_the_foal_irrigation_list = (false);
			
			ilc_on_action_list = NULL;
			
			// ----------
			
			// 7/20/2012 rmd : If the record has not yet been registered ... do so. At mid-night, when
			// these records are closed, there is no initialization of the rip performed. This is it
			// here!
			if( station_preserves.sps[ lstation_preserves_index ].spbf.station_report_data_record_is_in_use == (false) )
			{
				// 7/20/2012 rmd : Mark the fact that this station exists and its report data record is
				// considered to be in use.
				station_preserves.sps[ lstation_preserves_index ].spbf.station_report_data_record_is_in_use = (true);
	
				// 7/20/2012 rmd : Fill out required fields in the report record.
				station_preserves.sps[ lstation_preserves_index ].station_report_data_rip.box_index_0 = lbox_index_0;
	
				station_preserves.sps[ lstation_preserves_index ].station_report_data_rip.station_number = lstation_number_0;
				
				station_preserves.sps[ lstation_preserves_index ].station_report_data_rip.GID_station_group = STATION_get_GID_station_group(  lss_ptr );
			}

			// ----------

			// 6/28/2017 rmd : Every second we check the NOW days station for each station. We take
			// advantage of this call to set the STATION GROUP pointer for us. It can come back NULL
			// indicating the station doesn't belong to a station group.
			nm_at_1Hz_rate_set_skip_irrigation_due_to_NOW_days_and_check_for_isolated_stations( lss_ptr, lstation_preserves_index, &(pdtcs_ptr->date_time), &lsgs_ptr );

			// ----------
			// PROGRAMMED_IRRIGATION STARTS CHECK
			// ----------

			// 6/28/2017 rmd : lsgs_ptr was set with the preceeding call to the NOW days maintenance
			// function. It is possible the STATION GROUP pointer is a NULL, meaning the station does
			// not belong to a station group. This is not an error, it is legitiment for a station to
			// not belong to a station group, it could for example be part of a manual program and
			// irrigate that way.
			if( lsgs_ptr != NULL )
			{
				station_start_time = SCHEDULE_get_start_time( lsgs_ptr );
				
				// Does the present time match the start time. Regardless of if it is a water day or not we need to
				// determine this. As there are actions to take at each start time - watering or not. Fairly straight
				// forward except in 172_r 11/11/98 we added the DLS_IgnoreSXs term for a bug with DayLightSavings
				// where if a start time was between 1am and 2am and DayLight savings Fall back occurred the start
				// time would be hit twice. This added term makes it so once you hit the 2AM mark and go back to do
				// the 1AM-2AM hour again we won't recognize start times till we hit 2AM again.
				if( (station_start_time == pdtcs_ptr->date_time.T) && !pdtcs_ptr->dls_after_fall_back_ignore_start_times )
				{
					if( STATION_station_is_available_for_use(lss_ptr) )
					{
						// 6/30/2015 ajv : Update each station's moisture balance based on today's ET and rain
						// values. We do this every day - not just on water days - so we can see the moisture
						// balance reduce daily and replenish when it actually irrigates.
						if( WEATHER_get_station_uses_daily_et(lss_ptr) )
						{
							MBo = STATION_get_moisture_balance( lss_ptr );

							RZWWS = (STATION_GROUP_get_soil_storage_capacity_inches_100u( lss_ptr ) / 100.0F);

							// 11/23/2015 ajv : If we have rain, reset the moisture balance back to 50% before
							// deducting today's ET and adding the rain. This has a similar effect to when we cleared
							// positive hold-over in the ET2000e when it rained - it cleans up any issues caused by stop
							// times or other causes of the moisture balance falling below 50%.
							if( WEATHER_get_station_uses_rain( lss_ptr ) )
							{
								WEATHER_TABLES_get_rain_table_entry_for_index( 1, &rain_data );

								// 11/23/2015 ajv : Since rain is 100-up, compare against 100. Alternately, we could divide
								// the rain by 100.0 and compare it against 0.0.
								if( rain_data.rain_inches_u16_100u > 100 )
								{
									MBo = (RZWWS * WATERSENSE_MAXIMUM_MOISTURE_BALANCE_ALLOWED_FOR_IRRI);
								}
							}

							MB = WATERSENSE_reduce_moisture_balance_by_ETc( (STATION_GROUP_get_station_crop_coefficient_100u(lss_ptr, pdtcs_ptr->__month) / 100.0F), WATERSENSE_get_et_value_after_substituting_or_limiting( NOT_FOR_FTIMES, NULL, pdtcs_ptr, 1, IRRITIME_DONT_LOG_EVENTS ), MBo );

							if( WEATHER_get_station_uses_rain( lss_ptr ) )
							{
								// 6/30/2015 ajv : If sharing rain via WeatherSense, allow for an arbitrary loss of 20% of
								// the rainfall for non-uniformity and runoff. - WaterSense Specification for Weather-Based
								// Irrigation Controllers, Version 1.0 (November 3, 2011), Section 5.2.
								//
								// When sharing rain via a rain bucket, we've allowed the user to set their max hourly and
								// max 24-hour at the -RB controller to handle runoff, so set the usable rain to 100%.
								if( rain_data.status == RAIN_STATUS_FROM_WEATHERSENSE )
								{
									usable_rain_100u = STATION_GROUP_get_station_usable_rain_percentage_100u( lss_ptr );
								}
								else
								{
									usable_rain_100u = 100;
								}

								MB = WATERSENSE_increase_moisture_balance_by_rain( (rain_data.rain_inches_u16_100u / 100.0F), (STATION_GROUP_get_soil_storage_capacity_inches_100u(lss_ptr) / 100.0F), (usable_rain_100u / 100.0F), MB );
							}

							nm_STATION_set_moisture_balance_percent( lss_ptr, (MB / RZWWS), CHANGE_do_not_generate_change_line, CHANGE_REASON_HIT_A_STARTTIME, lbox_index_0, CHANGE_set_change_bits, STATION_get_change_bits_ptr( lss_ptr, CHANGE_REASON_HIT_A_STARTTIME ) );

							// 9/1/2015 ajv : Also set the moisture balance percentage after schedule completes because,
							// in the event the station doesn't run, we need this to have the adjusted value. If the
							// station does run, this number will automatically be updated when the station history
							// record closes. This ONLY affects the Station History line - it does not affect what's in
							// the Stations file - which is what's used throughout.
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_moisture_balance_percentage_after_schedule_completes_100u = ((MB / RZWWS) * 100 * 100);

							// 7/10/2015 ajv : Additionally, since Moisture Balance effectively replaces rain, let's
							// ensure the rain minutes are set to 0. By doing this here, early on, there's no further
							// logic that needs to be updated to prevent the use of rain when calculating run times
							// based on WaterSense.
							station_preserves.sps[ lstation_preserves_index ].rain_minutes_10u = 0;
						}
						else
						{
							// 7/20/2012 rmd : Calculate the -'ve time for any 'R' in the rain table. Does NOT make rain
							// for stations NOT IN USE. Which is a good thing. We don't want rain building up if not in
							// use.
							nm_RAIN_for_this_station_generate_rain_time_for_raintable_slot1( lss_ptr, lstation_preserves_index );
						}
					}
					 
					// -------------------------------

					// 5/6/2014 rmd : Whether station is in use or not - at the start time decrement the
					// NoWaterDays left and perhaps set SkipIrrigationDueToNOW to (false).
					nm_at_starttime_decrement_NOW_days_count( lss_ptr, lstation_preserves_index, &(pdtcs_ptr->date_time) );

					// -------------------------------

					// If it is an actual watering day we've really got some work to do!
					// 
					// 7/9/2015 ajv : Note that we DO NOT adjust the begin date (as indicated by the (false)
					// parameter. This is EXTREMELY IMPORTANT! If the begin date is adjusted here, the next
					// station to run will fall into this function and return FALSE because we've moved the
					// begin date into the future. We correct for this at the very end of this function.
					if( SCHEDULE_does_it_irrigate_on_this_date( lsgs_ptr, pdtcs_ptr, SCHEDULE_BEGIN_DATE_ACTION_update_it_but_dont_save_it_to_the_file ) == (true) )
					{
						pi_alert_there_was_a_start = (true);
						
						// ----------
						
						// 11/19/2014 rmd : CHECK FOR STATIONS STILL ON THE ILC LIST. Whether station is in use or
						// not. If not in use we could just let it run out its time but this seems better. Stop it
						// at the next scheduled water day and start time. Note - if truly still is on the foal irri
						// list this is the last place to close and restart the station history rip record. And that
						// happens in the remove from all irrigation lists function. So check if this station is
						// presently still in the foal irrigation list. If so it it needs to be removed here and
						// NOW. This is the last chance to close the STATION_HISTORY line. If a station irrigates
						// all the way into its next start time the station history record is still OPEN. This
						// removal will close it.
						//
						// 11/19/2014 rmd : NOTE - we CANNOT do this in the function that adds to the foal irri
						// list. Because that is too late to close and re-start the station history line. Must be
						// done here. And if the station is added to the list (passes all the hurdles) then we must
						// remove from the action needed list).
						xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );
						
						// 6/27/2012 rmd : We're going to loop through turning OFF. Only make a flow recording line
						// for the first group (if not already made).
						foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (false);
					
						lilc_ptr = nm_ListGetFirst( &(foal_irri.list_of_foal_all_irrigation) );
				
						while( lilc_ptr != NULL )
						{
							// 11/18/2014 rmd : If this station is still in the list for PROGRAMMED IRRIGATION remove
							// it!
							if( (lilc_ptr->box_index_0 == lbox_index_0) && (lilc_ptr->station_number_0_u8 == lstation_number_0) && (lilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION) )
							{
								Alert_programmed_irrigation_still_running_idx( lbox_index_0, lstation_number_0 );
								
								// ----------
								
								// 11/18/2014 rmd : I was going to remove the ilc from the ACTION_NEEDED list here. Because
								// over on the irri side when this station is added we check for dups there too just like
								// here. And if found would over-write the existing item in the list and turn it OFF if ON.
								// Well what would happen if the station here we are working with was subsequent to this
								// point determined not able to irrigate for some reason (say a mow day). In that case we
								// would end up with a station stuck in the irri list. Until it irrigated again for this
								// reason. So I decided to let the station remain on the ACTION_NEEDED. And if we
								// sucessfully add a new station to the foal irri list then we will remove from the action
								// needed list.
								//
								// 11/19/2014 rmd : The reason you have to remove the station from the action needed list is
								// over on the irri side the token message structure will using the xfer record ADD the
								// station first to the irri list and then using the action needed record we would
								// immediately remove it. End result being the station would be missing from the list.
								//
								// 11/19/2014 rmd : And we need to capture it here BEFORE we call the removal function cause
								// the removal function returns the NEXT ilc in the list!
								ilc_on_action_list = lilc_ptr;
								
								// 6/7/2012 rmd : The turn_OFF function requires this MUTEX be held by the caller.
								xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );
								// 11/13/2014 rmd : And delete the item from the list. Hmmmm ... how do we tell the irri
								// side this item is gone? We do not have to tell the irri side. It goes through the same
								// process of looking for a match when adding it's abbreviated station records to its own
								// list.
								lilc_ptr = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), lilc_ptr, ACTION_REASON_INTERNAL_TURN_OFF_AND_REMOVE_list_dup_removal );
								
								xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );

								// 11/19/2014 rmd : A station can only be on the list for this reason once.
								break;
							}
							else
							{
								lilc_ptr = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), lilc_ptr );
							}
						}
						
						// ----------
						
						// 6/28/2012 rmd : Not sure if this is the right depth for this if. If the station is not in
						// use should we be making report lines? Gee right off I'd say not. 
						if( STATION_station_is_available_for_use( lss_ptr ) )
						{
							memset( &station_d_and_r, 0x00, sizeof(ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT) );

							// 8/11/2015 rmd : Update the station history rip to reflect this start. If the station
							// history file wrote to flash since the station was removed from irrigation and the record
							// closed (time elapsed, user pushed stop, high flow, etc ...) then this rip record would
							// already be zeroed out. But if the file did not yet write for some extreme reason and we
							// were here at another start time the rip contains a copy of the previous record. Which is
							// now in the SDRAM completed records structure. BUT following this if there is a power
							// failure BEFORE the file writes to flash then we will have lost a record. But that sure
							// takes some doing!
							// 5/10/2018 rmd : I'M not sure that prior paragraph is true any longer, since we
							// implemented a record saving scheme utilizing battery backed SRAM. That would need to be
							// looked at - so this is just a note at this point.
							nm_init_station_history_record( &(station_preserves.sps[ lstation_preserves_index ].station_history_rip) );
							
							// ----------
							
							// 11/19/2014 rmd : Set the flag that it is valid to display.
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.rip_valid_to_show = (true);

							// 9/4/2014 rmd : And time stamp the record origin. The start time is by definition the
							// record origin time & date. So make it so.
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.record_start_time = pdtcs_ptr->date_time.T;
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.record_start_date = pdtcs_ptr->date_time.D;
						
							// 1/18/2017 rmd : And put in sane date/time for the first cycle start and last cycle end.
							// This is NEEDED to support the commserver database logic deciding to do use an INSERT or
							// MERGE statment. The MERGE statements are very time consuming to process for the database
							// so we want to avoid. We use these fields to support that logic.
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_first_cycle_start_time = pdtcs_ptr->date_time.T;
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_first_cycle_start_date = pdtcs_ptr->date_time.D;
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_last_cycle_end_time = pdtcs_ptr->date_time.T;
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_last_cycle_end_date = pdtcs_ptr->date_time.D;

							// 6/27/2012 rmd : Set up who this record belongs to. So when it is transferred to the
							// completed records array we know who it belongs to.
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.box_index_0 = lbox_index_0;
		
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.station_number = lstation_number_0;
							
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.GID_irrigation_schedule = STATION_get_GID_station_group(  lss_ptr );
	
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.GID_irrigation_system = STATION_GROUP_get_GID_irrigation_system_for_this_station(  lss_ptr );
	
							// ----------
							
							// Well we are at the start time of a water day - for the purposes of which alerts to show
							// in the "your most recent alerts" view mark this date/time
							nm_SCHEDULE_set_last_ran( lsgs_ptr, pdtcs_ptr->date_time, CHANGE_do_not_generate_change_line, CHANGE_REASON_HIT_A_STARTTIME, 0, CHANGE_set_change_bits, STATION_GROUP_get_change_bits_ptr( lsgs_ptr, CHANGE_REASON_HIT_A_STARTTIME ) );
							
							// -------------------------------
	
							// 2/21/2013 ajv : To stamp an alert for any substitution or limiting actions that occured
							// when this start time was crossed, we need a pointer to a station that is using daily ET.
							// Capture that pointer here. Since we only have to capture a single pointer, just grab the
							// first one and skip this test for any additional stations.
							if( (lss_ptr_to_station_using_et == NULL) && (WEATHER_get_station_uses_daily_et( lss_ptr )) )
							{
								lss_ptr_to_station_using_et = lss_ptr;

								lss_ptr_to_station_group_using_et = lsgs_ptr;
							}
							
							// ----------
							
							// 6/30/2015 ajv : Calculate how long we should run based on the WaterSense calculation. If
							// the station is not using ET, this function will return the stored Total Minutes value.
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_watersense_requested_seconds = ( SIXTY_POINT_ZERO * nm_WATERSENSE_calculate_run_minutes_for_this_station(	NOT_FOR_FTIMES,
																																																				lss_ptr,
																																																				lsgs_ptr,
																																																				NULL,
																																																				pdtcs_ptr,
																																																				nm_STATION_get_distribution_uniformity_100u(lss_ptr) ) );

							// ----------

							// 8/4/2015 ajv : Next, perform the minimum cycle calculation. Remember that any new
							// left-over irrigation needs to be deducted from the watersense requested seconds.
							if( WEATHER_get_station_uses_daily_et( lss_ptr ) )
							{
								// 8/4/2015 ajv : Now that we've calculated what WaterSense indicates what we should run,
								// add the current left-over time and 0 it out.
								station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_watersense_requested_seconds += station_preserves.sps[ lstation_preserves_index ].left_over_irrigation_seconds;

								station_preserves.sps[ lstation_preserves_index ].left_over_irrigation_seconds = 0;

								// ----------

								if( station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_watersense_requested_seconds > 0 )
								{
									// 7/17/2015 ajv : WATERSENSE MINIMUM CYCLE adjustment for normal programmed time.
									// 
									// Also store whatever time was thrown away so that we can reapply it during the next
									// irrigation. This ensures our moisture balance doesn't consistently drop simply because a
									// there's a cycle of 3-minutes or less.
									station_preserves.sps[ lstation_preserves_index ].left_over_irrigation_seconds = WATERSENSE_make_min_cycle_adjustment(	NOT_FOR_FTIMES,
																																							&(station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_watersense_requested_seconds),
																																							((nm_STATION_get_cycle_minutes_10u( lss_ptr ) / 10.0) * 60.0),
																																							&(station_preserves.sps[ lstation_preserves_index ].station_history_rip) );

									if( station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_watersense_requested_seconds == 0 )
									{
										station_d_and_r.results_skipped_due_to_WATERSENSE_MIN_CYCLE = (true);
									}
								}
							}

							// ----------

							// 6/30/2015 ajv : Now calculate how long we are actually scheduled to run for based on the
							// station adjust and percent adjust factors.
							requested_irrigation_seconds_ul = ( (nm_WATERSENSE_calculate_adjusted_run_time(	NOT_FOR_FTIMES,
																											lss_ptr,
																											NULL,
																											(station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_watersense_requested_seconds / 60.0F),
																											STATION_get_ET_factor_100u(lss_ptr),
																											PERCENT_ADJUST_get_station_percentage_100u(lss_ptr, pdtcs_ptr->date_time.D)) * 60.0F) );

							// ----------

							// 8/4/2015 ajv : Finally, reperform the minimum cycle adjustment but DO NOT deduct any
							// left-over from the WaterSense calculated number. This second adjustment is to ensure we
							// don't have any run times of 3-minutes or less after performing our station or percent
							// adjust, which don't impact WaterSense.
							if( WEATHER_get_station_uses_daily_et( lss_ptr ) )
							{
								if( requested_irrigation_seconds_ul > 0 )
								{
									// 7/17/2015 ajv : WATERSENSE MINIMUM CYCLE adjustment for normal programmed time.
									WATERSENSE_make_min_cycle_adjustment(	NOT_FOR_FTIMES,
																			&(requested_irrigation_seconds_ul),
																			((nm_STATION_get_cycle_minutes_10u( lss_ptr ) / 10.0) * 60.0),
																			&(station_preserves.sps[ lstation_preserves_index ].station_history_rip) );

									if( requested_irrigation_seconds_ul == 0 )
									{
										station_d_and_r.results_skipped_due_to_WATERSENSE_MIN_CYCLE = (true);
									}
								}
							}

							// ----------
							
							// 6/28/2012 rmd : Stamp the log line with the time that may or may not be irrigated. Moved
							// this stamping activity from the cycle start function because of the bug Jim Jordan found
							// about if the first cycle occurs after midnight the amount to irrigate put into the log
							// line then will most likely be different than the time calculated now as the ET table has
							// rolled. 161.b rmd. Additionally get this stamped BEFORE the rain and WaterSense minimum
							// cycle adjustments are made.
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_total_requested_minutes_us_10u = ( requested_irrigation_seconds_ul / 6 );
							
							// ----------
	
							// 6/28/2012 rmd : And capture the rain before we work it off against the run time.
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_rain_at_start_time_before_working_down__minutes_10u = station_preserves.sps[ lstation_preserves_index ].rain_minutes_10u;

							// ----------

							// 9/16/2013 ajv : Also stamp the last measured
							// current at the start of irrigation. This can be
							// used as a troubleshooting tool to show stability.
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_last_measured_current_ma = station_preserves.sps[ lstation_preserves_index ].last_measured_current_ma;

							// ----------
	
							lstop_datetime.T = SCHEDULE_get_stop_time( lsgs_ptr );

							// 5/2/2018 rmd : Set to something to keep compiler happy. Only used if we actually are
							// going to irrigate this valve.
							lstop_datetime.D = pdtcs_ptr->date_time.D;

							// If the stop time is equal to the start time then we do not irrigate.
							if( lstop_datetime.T == pdtcs_ptr->date_time.T )
							{
								// We will not irrigate if the stop time is equal to the start time.
								requested_irrigation_seconds_ul = 0;
								
								station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.hit_stop_time = (true);
								
								pi_alert_some_skipped_due_to_stop_time = (true);
							}
							else
							{
								// 5/24/2012 rmd : Adjust the stop day based on the stop time. Is it tomorrow or today?
								lstop_datetime.D = ( (UNS_16)FOAL_IRRI_return_stop_date_according_to_stop_time( &(pdtcs_ptr->date_time), lstop_datetime.T ) );
							}
							
							// ----------

							// 12/4/2014 rmd : UPDATED THOUGHT - since a mow day could be viewed as a none-scheduled day
							// perhaps this mow day decision should be moved outside of this section. We should test if
							// station is in use. And then test if a mow day or not. If it is do not make a station
							// history line? Just a thought.

							if( STATION_GROUPS_skip_irrigation_due_to_a_mow_day( NOT_FOR_FTIMES, lsgs_ptr, NULL, pdtcs_ptr ) )
							{
								pi_alert_some_skipped_due_to_mow_day = (true);
								
								requested_irrigation_seconds_ul = 0;

								station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.mow_day = (true);
							}
							
							// ----------

							if( requested_irrigation_seconds_ul > 0 )
							{
								// 10/25/2012 rmd : If the list is not full see if there is a slot available. Otherwise the
								// ilc_ptr must be NULL.
								lilc_ptr = NULL;

								// 10/25/2012 rmd : Test so we don't keep making alerts about the list being full.
								if( pi_overall_d_and_r.results_irrigation_list_full == (false) )
								{
									// 8/30/2012 rmd : See if there is a slot available to use.
									lilc_ptr = nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc();

									if( lilc_ptr == NULL )
									{
										// 10/25/2012 rmd : Set the flag. But for the case of PROGRAMMED IRRIGATION keep rolling
										// through the ilc list so we make all the new station history lines.
										pi_overall_d_and_r.results_irrigation_list_full = (true);
									}
								}
								
								// ----------

								if( lilc_ptr != NULL )
								{
									lilc_ptr->box_index_0 = lbox_index_0;

									lilc_ptr->station_number_0_u8 = lstation_number_0;

									lilc_ptr->bbf.w_reason_in_list = IN_LIST_FOR_PROGRAMMED_IRRIGATION;

									lilc_ptr->bbf.w_to_set_expected = ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow( lss_ptr );
									
									lilc_ptr->bbf.w_did_not_irrigate_last_time = station_preserves.sps[ lstation_preserves_index ].spbf.did_not_irrigate_last_time;
									
									lilc_ptr->requested_irrigation_seconds_ul = requested_irrigation_seconds_ul;
									
									lilc_ptr->cycle_seconds_ul = ( (nm_STATION_get_cycle_minutes_10u( lss_ptr ) / 10.0) * 60.0 );

									lilc_ptr->stop_datetime_d = lstop_datetime.D;

									lilc_ptr->stop_datetime_t = lstop_datetime.T;
									
									// ----------
		
									station_d_and_r.directions_honor_controller_set_to_OFF = (true);
									
									station_d_and_r.directions_honor_MANUAL_NOW = (true);

									station_d_and_r.directions_honor_CALENDAR_NOW = (true);

									// ----------
									
									// 11/12/2014 rmd : Apparently if you have a rain or freeze switch in the network there can
									// be no programmed or manual programmed irrigation.
									station_d_and_r.directions_honor_RAIN_SWITCH = (true);

									station_d_and_r.directions_honor_FREEZE_SWITCH = (true);

									// ----------
									
									station_d_and_r.directions_honor_WIND_PAUSE = WEATHER_get_station_uses_wind( lss_ptr );

									// 11/12/2014 rmd : For the case of PROGRAMMED_IRRIGATION if the station group this station
									// belongs to does not use rain then make sure the consume_RAIN_TIME and honor_RAIN_TABLE
									// are set (false). Also make sure if not using rain the accumulate rain time is zeroed to
									// cover the case rain was used and then the user said no more. Cleanliness.
									if( !WEATHER_get_station_uses_rain( lss_ptr ) )
									{
										station_preserves.sps[ lstation_preserves_index ].rain_minutes_10u = 0;

										station_d_and_r.directions_consume_RAIN_TIME = (false);
	
										station_d_and_r.directions_honor_RAIN_TABLE = (false);
									}
									else
									{
										station_d_and_r.directions_consume_RAIN_TIME = (true);
	
										station_d_and_r.directions_honor_RAIN_TABLE = (true);
									}

									// ----------
									
									// 11/23/2015 ajv : Only affect PROGRAMMED_IRRIGATION with the moisture balance. If it's
									// greater than 50%, then we'll skip irrigation.
									station_d_and_r.directions_honor_MOISTURE_BALANCE = WEATHER_get_station_uses_daily_et( lss_ptr );
									
									// ----------
		
									// 02/11/2016 skc : If we have yet to precalculate budget stuff, do it now
									if( ((false) == Vp_flag) )
									{
										BUDGET_calculate_ratios( pdtcs_ptr, (true) ); // Precalculate values used in budget corrections

										Vp_flag = (true); // we don't want to call more than once per start time
									}

									// 5/23/2016 skc : We only want to send an alert once per station group about budget stuff.
									if( 0 == nm_STATION_GROUP_get_budget_start_time_flag( lsgs_ptr ) )
									{
										// 4/1/2016 skc : Check for over budget
										UNS_32 gid_sys = STATION_GROUP_get_GID_irrigation_system( lsgs_ptr ); 

										BUDGET_handle_alerts_at_start_time( gid_sys, lsgs_ptr );

										nm_STATION_GROUP_set_budget_start_time_flag( lsgs_ptr, 1 );
									}

									// ----------

									if( _nm_nm_nm_add_to_the_irrigation_list( lilc_ptr, lss_ptr, lsgs_ptr, &station_d_and_r ) == (true) )
									{
										new_station_made_it_onto_the_foal_irrigation_list = (true);
										
										// ----------
	
										// 5/10/2018 rmd : Initialize the moisture sensor control variables held by the station
										// group.
										MOISTURE_SENSORS_initialize__saved__irrigation_control_variables_for_this_moisture_sensor( lilc_ptr->moisture_sensor_decoder_serial_number );
										
										// ----------
										
										// 6/5/2012 rmd : Remember this is for programmed irrigation. Set this marker (true). It
										// will be set (false) when the station is turned on.
										station_preserves.sps[ lstation_preserves_index ].spbf.did_not_irrigate_last_time = (true);

										// 10/10/2012 rmd : Indicate at least one valve queued up for porgrammed irrigation.
										pi_overall_d_and_r.results_at_least_one_valve_irrigated = (true);
							
										// 10/5/2012 rmd : If the time was reduced set the log line flag.
										if( station_d_and_r.results_reduced_due_to_RAIN_TIME == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.rain_as_negative_time_reduced_irrigation = (true);
											
											// 10/10/2012 rmd : Set the flag so we make an alert after going through all the stations.
											pi_overall_d_and_r.results_reduced_due_to_RAIN_TIME = (true);
										}
										
										// ----------
										
										// 11/19/2014 rmd : Now that we know the station made it onto the list check if the station
										// was one of those cases where it was still irrigating from its previous start time. See
										// where the ilc_on_action_list variable is set for a complete explanation.
										if( ilc_on_action_list )
										{
											if( nm_ListRemove( &foal_irri.list_of_foal_stations_with_action_needed, (void*)ilc_on_action_list ) != MIST_SUCCESS )
											{
												Alert_Message( "Foal Irri SX: error removing" );
											}
										}
										
									}
									else
									{
										if( station_d_and_r.results_skipped_due_to_2_WIRE_CABLE_OVERHEATED == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.two_wire_cable_problem = (true);

											// 10/10/2012 rmd : Set the flag so we make an alert after going through all the stations.
											pi_overall_d_and_r.results_skipped_due_to_2_WIRE_CABLE_OVERHEATED = (true);
										}

										// ------------
										
										// 10/4/2012 rmd : If the station was skipped set the log line flag.
										if( station_d_and_r.results_skipped_as_there_is_a_MLB == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.mlb_prevented_or_curtailed = (true);
											
											// 10/10/2012 rmd : Set the flag so we make an alert after going through all the stations.
											pi_overall_d_and_r.results_skipped_as_there_is_a_MLB = (true);
										}

										// ------------

										// 10/4/2012 rmd : If the station was skipped set the log line flag.
										if( station_d_and_r.results_skipped_as_there_is_a_MVOR_CLOSED == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.mvor_closed_prevented_or_curtailed = (true);
											
											pi_overall_d_and_r.results_skipped_as_there_is_a_MVOR_CLOSED = (true);
										}

										// ------------

										// 10/4/2012 rmd : If the controller was OFF set the log line flag.
										if( station_d_and_r.results_skipped_as_the_controller_is_set_to_OFF == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.controller_turned_off = (true);
											
											pi_overall_d_and_r.results_skipped_as_the_controller_is_set_to_OFF = (true);
										}

										// ------------

										// 10/4/2012 rmd : If MANUAL NOW in force.
										if( station_d_and_r.results_skipped_due_to_MANUAL_NOW == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.no_water_by_manual_prevented = (true);
											
											pi_overall_d_and_r.results_skipped_due_to_MANUAL_NOW = (true);
										}

										// ------------

										// 10/4/2012 rmd : If CALENDAR NOW in force.
										if( station_d_and_r.results_skipped_due_to_CALENDAR_NOW == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.no_water_by_calendar_prevented = (true);
											
											pi_overall_d_and_r.results_skipped_due_to_CALENDAR_NOW = (true);
										}

										// ------------

										// 10/4/2012 rmd : If RAIN SWITCH in force.
										if( station_d_and_r.results_skipped_due_to_RAIN_SWITCH == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.switch_rain_prevented_or_curtailed = (true);
											
											pi_overall_d_and_r.results_skipped_due_to_RAIN_SWITCH = (true);
										}

										// ------------

										// 10/4/2012 rmd : If FREEZE SWITCH in force.
										if( station_d_and_r.results_skipped_due_to_FREEZE_SWITCH == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.switch_freeze_prevented_or_curtailed = (true);
											
											pi_overall_d_and_r.results_skipped_due_to_FREEZE_SWITCH = (true);
										}

										// ------------

										// 10/4/2012 rmd : If WIND CONDITIONS in force.
										if( station_d_and_r.results_skipped_due_to_WIND_PAUSE == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.wind_conditions_prevented_or_curtailed = (true);
											
											pi_overall_d_and_r.results_skipped_due_to_WIND_PAUSE = (true);
										}

										// ------------

										// 10/4/2012 rmd : If RAIN TIME affected the irrigation.
										if( station_d_and_r.results_skipped_due_to_RAIN_TIME == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.rain_as_negative_time_prevented_irrigation = (true);
											
											pi_overall_d_and_r.results_skipped_due_to_RAIN_TIME = (true);
										}

										// 10/4/2012 rmd : If RAIN TABLE affected the irrigation.
										if( station_d_and_r.results_skipped_due_to_RAIN_TABLE == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.rain_table_R_M_or_Poll_prevented_or_curtailed = (true);
											
											pi_overall_d_and_r.results_skipped_due_to_RAIN_TABLE = (true);
										}

										// ----------

										// 11/23/2015 ajv : If MOISTURE BALANCE caused us to skip irrigation.
										if( station_d_and_r.results_skipped_due_to_MOISTURE_BALANCE == (true) )
										{
											station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.moisture_balance_prevented_irrigation = (true);

											pi_overall_d_and_r.results_skipped_due_to_MOISTURE_BALANCE = (true);
										}

										// ------------

									}
									
								}  // of we found a valid ilc to use - and we ALWAYS should

							}  // of the requested irrigation seconds are more than 0

							// ----------

							// 7/7/2015 ajv : Regarding WaterSense minimum cycle time. If skipped all together we
							// set the station history flag for that here. And the flag to make an alert when done. Note
							// that this is OUTSIDE of the check of requested irrigation seconds are more than 0
							// because, in this case, the time is actually zeroed...
							if( station_d_and_r.results_skipped_due_to_WATERSENSE_MIN_CYCLE == (true) )
							{
								station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_flag.watersense_min_cycle_zeroed_the_irrigation_time = (true);

								pi_overall_d_and_r.results_skipped_due_to_WATERSENSE_MIN_CYCLE = (true);
							}

							// ------------

							// 12/4/2014 rmd : And now record what's left of the rain whether it has changed or not
							// (changes when runs through the add to list function). Stamp the station history line with
							// the resultant amount. Of course before we potentially close the line.
							station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_rain_at_start_time_after_working_down__minutes_10u = station_preserves.sps[ lstation_preserves_index ].rain_minutes_10u;
							
							// ----------
							
							// 12/4/2014 rmd : We are at the indentation level where the station history rip was
							// initialized and made 'real'. Therefore if the station DID NOT make it onto the foal
							// irrigation list we MUST close the station history record. The normal mechanism for
							// closing these records is tied to when the station is removed from the list. So for the
							// case the station doesn't even get onto the list we have to take care of that here!
							if( !new_station_made_it_onto_the_foal_irrigation_list )
							{
								// 10/6/2015 ajv : Set the moisture balance percentage after schedule completes because we
								// still want to track this even if the station didn't run. This way, when there's rain, we
								// can track how much the moisture balance increases after the rain event and how much it
								// decreases each subsequent day based on ET.
								if( WEATHER_get_station_uses_daily_et(lss_ptr) )
								{
									station_preserves.sps[ lstation_preserves_index ].station_history_rip.pi_moisture_balance_percentage_after_schedule_completes_100u = (STATION_get_moisture_balance(lss_ptr) * 100 * 100);
								}

								// 12/4/2014 rmd : Close the record. This also causes transmission to the comm server.
								nm_STATION_HISTORY_close_and_start_a_new_record( lstation_preserves_index );
							}
							
						}  // if station in use
						else
						{
							// 7/23/2015 rmd : Set the flag to show some not in use.
							pi_alert_some_skipped_due_to_not_being_in_use = (true);

							// 9/24/2012 rmd : So for stations NOT in use should we zero out the left-over irrigation?
							// And the rain irrigation? Gee I think so. So this means if a station is not in use when it
							// hits its start time and it is a watering day the left over and rain will both be zeroed.
							station_preserves.sps[ lstation_preserves_index ].left_over_irrigation_seconds = 0;

							station_preserves.sps[ lstation_preserves_index ].rain_minutes_10u = 0;
						}

						// ----------
						
						xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
						
					}  // Of if today is a watering day.
		
				}  // Of if the time is a start time.

			}  // of if schedule group is not a NULL
			
			// ----------
			// PROGRAMMED_IRRIGATION END
			// ----------

			// -------------------------------

			// ----------
			// MANUAL_PROGRAMS STARTS CHECK
			// ----------
	
			// 8/31/2015 rmd : Only if the station is physically available and user has kept station
			// in use should we check to run this stations manual program.
			if( STATION_station_is_available_for_use( lss_ptr ) )
			{
				// 10/25/2012 rmd : Settings for first pass through.
				manual_program_GID = STATION_get_GID_manual_program_A( lss_ptr );
	
				// ----------
				
				// 10/25/2012 rmd : There are TWO potential manual programs tied to each station.
				for( mp=0; mp<2; mp++ )
				{
					// 10/25/2012 rmd : In the default, and perhaps most commonly, at least one of the programs
					// doesn't even exist and possibly both. In that case their GID would come up as zero and if
					// we try to find the group with a GID of ZERO that would be reported as an error. So test
					// for that case.
					number_of_mp_starts = 0;
					if( manual_program_GID != 0 )
					{
						// 10/3/2012 rmd : Check for any MANUAL_PROGRAM start associated with this station.
						number_of_mp_starts = MANUAL_PROGRAMS_return_the_number_of_starts_for_this_GID( manual_program_GID, pdtcs_ptr );
					}
	
					// ----------
	
					if( number_of_mp_starts > 0 )
					{
						// 7/20/2015 rmd : Now get the run time for this manual program GID.
						mp_seconds = MANUAL_PROGRAMS_get_run_time( MANUAL_PROGRAMS_get_group_with_this_GID( manual_program_GID ) );
						
						// ------------
		
						mp_alert_there_was_a_start = (true);
						
						// ------------
		
						// 2011.10.17 rmd : And now we are going to start messing with the foal irri irrigation list
						// for the first time within this function.
						xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );
						
						// ----------
						// 10/25/2012 rmd : If the list is not full see if there is a slot available. Otherwise the
						// ilc_ptr must be NULL.
						lilc_ptr = NULL;
						// 10/25/2012 rmd : Check that there is a run time (should be). And test so we don't keep
						// making alerts about the list being full.
						if( mp_seconds && !mp_overall_d_and_r.results_irrigation_list_full )
						{
							// 8/30/2012 rmd : See if there is a slot available to use.
							lilc_ptr = nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc();
						 
							if( lilc_ptr == NULL )
							{
								// 10/25/2012 rmd : Set the flag. But for the case of PROGRAMMED IRRIGATION keep rolling
								// through the ilc list so we make all the new station history lines.
								mp_overall_d_and_r.results_irrigation_list_full = (true);
							}
						}
						
						// ----------
	
						if( lilc_ptr != NULL )
						{
							// 10/5/2012 rmd : The station number and serial number are valid inside the station by
							// station loop.
							lilc_ptr->box_index_0 = lbox_index_0;
			
							lilc_ptr->station_number_0_u8 = lstation_number_0;
			
							lilc_ptr->bbf.w_reason_in_list = IN_LIST_FOR_MANUAL_PROGRAM;
			
							lilc_ptr->requested_irrigation_seconds_ul = (number_of_mp_starts * mp_seconds);
			
							lilc_ptr->cycle_seconds_ul = mp_seconds;
			
							// 10/5/2012 rmd : Everything else in the ilc is set to 0 (false). And that is appropriate
							// for MANUAL_PROGRAMS.
							
							// ------------
		
							// 10/11/2012 rmd : These directions flag could be cleared and set outside the two manual
							// program GID loop (they are for the same station afterall). But for coding clarity fully
							// manipulated here.
							memset( &station_d_and_r, 0x00, sizeof(ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT) );
							
							station_d_and_r.directions_honor_controller_set_to_OFF = (true);
							
							station_d_and_r.directions_honor_MANUAL_NOW = (true);
		
							station_d_and_r.directions_honor_CALENDAR_NOW = (true);
		
							station_d_and_r.directions_honor_RAIN_SWITCH = (true);
		
							station_d_and_r.directions_honor_FREEZE_SWITCH = (true);
		
							// ----------
							
							station_d_and_r.directions_honor_WIND_PAUSE = WEATHER_get_station_uses_wind( lss_ptr );
	
							// 11/12/2014 rmd : For the case of MANUAL PROGRAMS if the station group this station
							// belongs to does not use rain then make sure the consume_RAIN_TIME and honor_RAIN_TABLE
							// are set (false). Also make sure if not using rain the accumulate rain time is zeroed to
							// cover the case rain was used and then the user said no more. Cleanliness.
							if( !WEATHER_get_station_uses_rain( lss_ptr ) )
							{
								station_preserves.sps[ lstation_preserves_index ].rain_minutes_10u = 0;
	
								station_d_and_r.directions_consume_RAIN_TIME = (false);
	
								station_d_and_r.directions_honor_RAIN_TABLE = (false);
							}
							else
							{
								station_d_and_r.directions_consume_RAIN_TIME = (true);
	
								station_d_and_r.directions_honor_RAIN_TABLE = (true);
							}
							
							// ----------

							// 02/11/2016 skc : If we have yet to precalculate budget stuff, do it now		
							if( (false == Vp_flag) )
							{
								BUDGET_calculate_ratios( pdtcs_ptr, true ); // Precalculate values used in budget corrections
								Vp_flag = true; // we don't want to call more than once
							}

							// 5/23/2016 skc : We only want to send an alert once per system about budget stuff.
							if( 0 == nm_STATION_GROUP_get_budget_start_time_flag( lsgs_ptr ) )
							{
								// 4/1/2016 skc : Check for over budget
								UNS_32 gid_sys = STATION_GROUP_get_GID_irrigation_system( lsgs_ptr ); 
								BUDGET_handle_alerts_at_start_time( gid_sys, lsgs_ptr );
								nm_STATION_GROUP_set_budget_start_time_flag( lsgs_ptr, 1 );
							}

							// ----------
		
							if( _nm_nm_nm_add_to_the_irrigation_list( lilc_ptr, lss_ptr, NULL, &station_d_and_r ) == (true) )
							{
								// 10/10/2012 rmd : Indicate at least one valve queued up for manual programs.
								mp_overall_d_and_r.results_at_least_one_valve_irrigated = (true);
	
								// ----------
								
								if( station_d_and_r.results_reduced_due_to_RAIN_TIME == (true) )
								{
									// 10/10/2012 rmd : Set the flag so we make an alert after going through all the stations.
									mp_overall_d_and_r.results_reduced_due_to_RAIN_TIME = (true);
								}
	
							}
							else
							{
								// 7/16/2015 rmd : Capture the flags into the overall results.
	
								if( station_d_and_r.results_skipped_due_to_2_WIRE_CABLE_OVERHEATED == (true) )
								{
									// 10/10/2012 rmd : Set the flag so we make an alert after going through all the stations.
									mp_overall_d_and_r.results_skipped_due_to_2_WIRE_CABLE_OVERHEATED = (true);
								}
	
								// ------------
								
								if( station_d_and_r.results_skipped_as_there_is_a_MLB == (true) )
								{
									// 10/10/2012 rmd : Set the flag so we make an alert after going through all the stations.
									mp_overall_d_and_r.results_skipped_as_there_is_a_MLB = (true);
								}
	
								// ------------
	
								if( station_d_and_r.results_skipped_as_there_is_a_MVOR_CLOSED == (true) )
								{
									mp_overall_d_and_r.results_skipped_as_there_is_a_MVOR_CLOSED = (true);
								}
	
								// ------------
	
								if( station_d_and_r.results_skipped_as_the_controller_is_set_to_OFF == (true) )
								{
									mp_overall_d_and_r.results_skipped_as_the_controller_is_set_to_OFF = (true);
								}
	
								// ------------
	
								if( station_d_and_r.results_skipped_due_to_MANUAL_NOW == (true) )
								{
									mp_overall_d_and_r.results_skipped_due_to_MANUAL_NOW = (true);
								}
	
								// ------------
	
								// 10/4/2012 rmd : If CALENDAR NOW in force.
								if( station_d_and_r.results_skipped_due_to_CALENDAR_NOW == (true) )
								{
									mp_overall_d_and_r.results_skipped_due_to_CALENDAR_NOW = (true);
								}
	
								// ------------
	
								// 10/4/2012 rmd : If RAIN SWITCH in force.
								if( station_d_and_r.results_skipped_due_to_RAIN_SWITCH == (true) )
								{
									mp_overall_d_and_r.results_skipped_due_to_RAIN_SWITCH = (true);
								}
	
								// ------------
	
								// 10/4/2012 rmd : If FREEZE SWITCH in force.
								if( station_d_and_r.results_skipped_due_to_FREEZE_SWITCH == (true) )
								{
									mp_overall_d_and_r.results_skipped_due_to_FREEZE_SWITCH = (true);
								}
	
								// ------------
	
								// 10/4/2012 rmd : If WIND CONDITIONS in force.
								if( station_d_and_r.results_skipped_due_to_WIND_PAUSE == (true) )
								{
									mp_overall_d_and_r.results_skipped_due_to_WIND_PAUSE = (true);
								}
	
								// ------------
	
								// 10/4/2012 rmd : If RAIN TIME affected the irrigation.
								if( station_d_and_r.results_skipped_due_to_RAIN_TIME == (true) )
								{
									mp_overall_d_and_r.results_skipped_due_to_RAIN_TIME = (true);
								}
	
								// 10/4/2012 rmd : If RAIN TABLE affected the irrigation.
								if( station_d_and_r.results_skipped_due_to_RAIN_TABLE == (true) )
								{
									mp_overall_d_and_r.results_skipped_due_to_RAIN_TABLE = (true);
								}
	
								// ------------

								// 7/22/2015 ajv : Why should Manual Programs be affected by the 3-minute minimum cycle
								// requirement for WaterSense? It doesn't really make sense that it should, does it? TODO
								mp_overall_d_and_r.results_skipped_due_to_WATERSENSE_MIN_CYCLE = (false);
							}
		
						}  // of we found a valid ilc to use
			
						xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
		
					}  // of if there is a manual program start detected
		
					// ----------
					
					// 8/31/2015 rmd : Obtain potential second manual program GID this station belongs to.
					manual_program_GID = STATION_get_GID_manual_program_B( lss_ptr );
					
				}  // of for each manual schedule to check

			}  // of if station is in use

			// ----------
			// MANUAL_PROGRAMS END
			// ----------
			


		}  // we couldn't get a valid station preserves or report index to work with
		
		lss_ptr = nm_ListGetNext( &station_info_list_hdr, (void *)lss_ptr );

	}  // of station by station loop

	// ----------
	// ----------
	// ----------
	// PROGRAMMED IRRIGATION ALERTS
	// ----------
	// ----------
	// ----------

	if( pi_alert_there_was_a_start )
	{
		// 10/11/2012 rmd : AJ and I decided not to include the GID in the alert. We reasoned we are
		// likely providing to the user more than they want or need. They are quite likely satified
		// with simply "START : Programmed Irrigation" or "START : Manual Programs" and therefore we
		// will for-go the group name. If we provided the group name (by including a GID) the alerts
		// generation would become more complicated, possibly requiring a post check located here
		// where we would loop through all the schedules looking for a matching time & waterday with
		// stations_in_use.
		Alert_scheduled_irrigation_started( IN_LIST_FOR_PROGRAMMED_IRRIGATION );

		// 2/21/2013 ajv : If no stations are using Daily ET no substitution or
		// limiting will have occurred and the the station pointer will be NULL.
		// Therefore, only generate an alert if the station is valid.
		if( (lss_ptr_to_station_using_et != NULL) && (lss_ptr_to_station_group_using_et != NULL) )
		{
			IRRITIME_make_ETTable_substitute_and_limit_alerts_after_we_know_program_ran( lss_ptr_to_station_group_using_et, lss_ptr_to_station_using_et, pdtcs_ptr );
		}
	}
	
	if( pi_overall_d_and_r.results_irrigation_list_full )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_TOO_MANY_STATIONS );
	}
	
	if( pi_alert_some_skipped_due_to_not_being_in_use )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_STATION_NOT_IN_USE );
	}

	if( pi_overall_d_and_r.results_skipped_due_to_MOISTURE_BALANCE )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_MOISTURE_BALANCE );
	}

	if( pi_alert_some_skipped_due_to_stop_time )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_STOP_TIME );
	}

	if( pi_alert_some_skipped_due_to_mow_day )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_MOW_DAY );
	}
	
	if( pi_overall_d_and_r.results_skipped_as_the_controller_is_set_to_OFF )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_CONTROLLER_OFF );
	}

	if( pi_overall_d_and_r.results_skipped_as_there_is_a_MLB )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_MLB );
	}

	if( pi_overall_d_and_r.results_skipped_due_to_2_WIRE_CABLE_OVERHEATED )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_TWO_WIRE_CABLE_OVERHEATED );
	}

	if( pi_overall_d_and_r.results_skipped_due_to_MANUAL_NOW )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_MANUAL_NOW );
	}

	if( pi_overall_d_and_r.results_skipped_due_to_CALENDAR_NOW )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_CALENDAR_NOW );
	}

	if( pi_overall_d_and_r.results_skipped_as_there_is_a_MVOR_CLOSED )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_MVOR_CLOSED );
	}

	if( pi_overall_d_and_r.results_skipped_due_to_RAIN_TABLE )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_RAIN_TABLE );
	}

	if( pi_overall_d_and_r.results_skipped_due_to_RAIN_TIME )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_RAIN_NEGATIVE_TIME );
	}

	if( pi_overall_d_and_r.results_skipped_due_to_RAIN_SWITCH )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_RAIN_SWITCH );
	}

	if( pi_overall_d_and_r.results_skipped_due_to_FREEZE_SWITCH )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_FREEZE_SWITCH );
	}

	if( pi_overall_d_and_r.results_skipped_due_to_WIND_PAUSE )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_WIND );
	}

	if( pi_overall_d_and_r.results_skipped_due_to_WATERSENSE_MIN_CYCLE )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_PROGRAMMED_IRRIGATION, SKIPPED_DUE_TO_WATERSENSE_MIN_CYCLE );
	}

	if( pi_alert_there_was_a_start && (!pi_overall_d_and_r.results_at_least_one_valve_irrigated) )
	{
		Alert_Message( "Programmed Irrigation: NO irrigation due to specified reason(s)" );
	}
	

	// ----------
	// ----------
	// MANUAL_PROGRAM ALERTS
	// ----------
	// ----------

	if( mp_alert_there_was_a_start )
	{
		// 10/11/2012 rmd : Wether we pass MANUAL_PROGRAM_A or _B doesn't matter. The resultant
		// string is the same.
		Alert_scheduled_irrigation_started( IN_LIST_FOR_MANUAL_PROGRAM );
	}
	
	if( mp_overall_d_and_r.results_irrigation_list_full )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_TOO_MANY_STATIONS );
	}
	
	if( mp_overall_d_and_r.results_skipped_as_the_controller_is_set_to_OFF )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_CONTROLLER_OFF );
	}

	if( mp_overall_d_and_r.results_skipped_as_there_is_a_MLB )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_MLB );
	}

	if( mp_overall_d_and_r.results_skipped_due_to_2_WIRE_CABLE_OVERHEATED )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_TWO_WIRE_CABLE_OVERHEATED );
	}

	if( mp_overall_d_and_r.results_skipped_due_to_MANUAL_NOW )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_MANUAL_NOW );
	}

	if( mp_overall_d_and_r.results_skipped_due_to_CALENDAR_NOW )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_CALENDAR_NOW );
	}

	if( mp_overall_d_and_r.results_skipped_as_there_is_a_MVOR_CLOSED )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_MVOR_CLOSED );
	}

	if( mp_overall_d_and_r.results_skipped_due_to_RAIN_TABLE )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_RAIN_TABLE );
	}

	if( mp_overall_d_and_r.results_skipped_due_to_RAIN_TIME )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_RAIN_NEGATIVE_TIME );
	}

	if( mp_overall_d_and_r.results_skipped_due_to_RAIN_SWITCH )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_RAIN_SWITCH );
	}

	if( mp_overall_d_and_r.results_skipped_due_to_FREEZE_SWITCH )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_FREEZE_SWITCH );
	}

	if( mp_overall_d_and_r.results_skipped_due_to_WIND_PAUSE )
	{
		Alert_some_or_all_skipping_their_start( IN_LIST_FOR_MANUAL_PROGRAM, SKIPPED_DUE_TO_WIND );
	}

	if( mp_alert_there_was_a_start && (!mp_overall_d_and_r.results_at_least_one_valve_irrigated) )
	{
		Alert_Message( "Manual Program: no irrigation due to specified reason(s)" );
	}
	
	// ----------

	// 7/9/2015 ajv : Now that we've gone through all of the stations, adjust the begin date of
	// any schedules that are scheduled to start right now.
	for( i = 0; i < STATION_GROUP_get_num_groups_in_use(); ++i )
	{
		lsgs_ptr = STATION_GROUP_get_group_at_this_index( i );

		if( (SCHEDULE_get_start_time(lsgs_ptr) == pdtcs_ptr->date_time.T) && !pdtcs_ptr->dls_after_fall_back_ignore_start_times )
		{
			// 7/9/2015 ajv : Since now's a start time, call the following function to actually move the
			// begin date forward.
			SCHEDULE_does_it_irrigate_on_this_date( lsgs_ptr, pdtcs_ptr, SCHEDULE_BEGIN_DATE_ACTION_advance_it_and_save_it_to_the_file );
		}
	}

	// ----------

	// 02/11/2016 skc : Update the main status screen.
	// These were seperate calls to postBackground_Calculation_Event(), no need to call twice in a row.
	if( pi_alert_there_was_a_start || mp_alert_there_was_a_start )
	{
		// 4/17/2015 ajv : The next scheduled irrigation is normally kept up-to-date by
		// recalculating every time a change is made. However, after a start time is crossed, it's
		// important to update the screen to indicate the NEXT scheduled start. Therefore, post a
		// message to update the GuiVars used to display the next scheduled irrigation.
		postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );

		// 02/23/2016 skc : We only care about the timing when we have a start time crossing.
		// We also want to track this EVERY time, at least until we are confident of budgets CPU load.
		__this_time_delta = (my_tick_count - __this_time_delta);

#if 0
		if( __this_time_delta > __largest_start_time_delta )
		{
			__largest_start_time_delta = __this_time_delta;
			Alert_Message_va("DELTA %d", __largest_start_time_delta);
		}
#else
		// 3/21/2018 ajv : Suppressed as it's unnecessary
		// Alert_Message_va("DELTA %d", __this_time_delta);
#endif
	}

	// -----------------------------------
	
	// Return the MUTEXES.
	xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Setup for and starts a test. The intended sequence is that a user pushes a
    key (irri machine) and that information is sent to the master. The master (foal machine)
    comm_mngr upon receipt of token_resp will call this function to actually kick off the
    test.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the COMM_MNGR task. Upon receipt of the
    token_resp.
	
	@RETURN (none)

	@ORIGINAL 2012.08.30 rmd

	@REVISIONS (none)
*/
extern void FOAL_IRRI_initiate_a_station_test( TEST_AND_MOBILE_KICK_OFF_STRUCT *tkos, const UNS_32 preason_in_list )
{
	STATION_STRUCT	*lss_ptr;
	
	IRRIGATION_LIST_COMPONENT	*lilc_ptr;
	
	ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT	test_d_and_r;

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	// ----------

	// 8/30/2012 rmd : Finding the station will test the integrity of the serial number and
	// station number values.
	lss_ptr = nm_STATION_get_pointer_to_station( tkos->box_index_0, tkos->station_number );
	
	if( lss_ptr == NULL )
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_IRRI: sta not found" );
	}
	else
	{
		// 8/30/2012 rmd : See if there is a slot available to use.
		lilc_ptr = nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc();
		
		if( lilc_ptr == NULL )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_IRRI: ilc not available" );
		}
		else
		{
			lilc_ptr->box_index_0 = tkos->box_index_0;
			
			lilc_ptr->station_number_0_u8 = tkos->station_number;
			
			lilc_ptr->bbf.w_reason_in_list = preason_in_list;
			
			lilc_ptr->bbf.w_to_set_expected = tkos->set_expected;
			
			lilc_ptr->requested_irrigation_seconds_ul = tkos->time_seconds;

			// 8/30/2012 rmd : During TEST the cycle time is the requested time.
			lilc_ptr->cycle_seconds_ul = tkos->time_seconds;

			// 10/5/2012 rmd : Everything else in the ilc is set to 0 (false). And that is appropriate
			// for TEST.
			
			// ----------

			memset( &test_d_and_r, 0, sizeof(ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT) );

			// 10/11/2012 rmd : For the case of TEST we there are no directions set to (true). We ignore
			// all of them and add the valve to the list.
			
			// ----------

			if( _nm_nm_nm_add_to_the_irrigation_list( lilc_ptr, lss_ptr, NULL, &test_d_and_r ) == (true) )
			{
				if( preason_in_list == IN_LIST_FOR_TEST )
				{
					// 12/11/2012 ajv : Generate an alert that the Test was invoked
					Alert_test_station_started_idx( tkos->box_index_0, tkos->station_number, INITIATED_VIA_KEYPAD );
				}
				else
				{
					// 7/6/2015 rmd : For the case of MOBILE, alert when the station actually turns ON in the
					// maintain function.
				}
			}
			else
			{
				// 10/11/2012 rmd : Nothing to do here for test. There are TWO reasons why we won't add to
				// the list though. If there is a MLB or if the MVOR CLOSED is in effect.
			}
			
		}  // of we found a valid ilc to use

	}

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );

	xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
extern void FOAL_IRRI_start_a_walk_thru( UNS_32 pwalk_thru_gid )
{
	// 3/22/2018 rmd : This thing is pretty big. Do not put this on the stack! Use a pointer to
	// it.
	WALK_THRU_KICK_OFF_STRUCT	*lwalk_thru;
	
	STATION_STRUCT	*lss_ptr;
	
	UNS_32	iii;
	
	IRRIGATION_LIST_COMPONENT	*lilc_ptr;
	
	ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT	test_d_and_r;
	
	UNS_32		number_added;

	// ----------
	
	if( mem_obtain_a_block_if_available( sizeof(WALK_THRU_KICK_OFF_STRUCT), (void**)&lwalk_thru ) )
	{
		// 3/22/2018 rmd : Okay the first thing is to, using the GID, get the walk through details
		// from the actual walk thru list item.
		if( WALK_THRU_load_kick_off_struct( pwalk_thru_gid, lwalk_thru ) )
		{
			// 3/22/2018 rmd : So far so good. Now to load the irrigation list.
			
			// ----------
			
			// 10/23/2018 rmd : Init tracker to make alert or not.
			number_added = 0;
			
			// ----------
			
			// 3/22/2018 rmd : Need to take all THREE mutexes for the add to list function.
			xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );
		
			xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );
		
			xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );
		
			// ----------
			
			for( iii=0; iii<MAX_NUMBER_OF_WALK_THRU_STATIONS; iii++ )
			{
				if( lwalk_thru->stations[ iii ] != WALK_THRU_SEQUENCE_SLOT_no_station_set )
				{
					lss_ptr = nm_STATION_get_pointer_to_station( lwalk_thru->box_index_0, lwalk_thru->stations[ iii ] );

					if( lss_ptr == NULL )
					{
						ALERT_MESSAGE_WITH_FILE_NAME( "WALK THRU: station not found" );
					}
					else
					{
						// 3/22/2018 rmd : See if there is a slot available to use.
						lilc_ptr = nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc();
						
						if( lilc_ptr == NULL )
						{
							ALERT_MESSAGE_WITH_FILE_NAME( "WALK THRU: ilc not available" );
						}
						else
						{
							lilc_ptr->box_index_0 = lwalk_thru->box_index_0;
							
							lilc_ptr->station_number_0_u8 = lwalk_thru->stations[ iii ];
							
							lilc_ptr->bbf.w_reason_in_list = IN_LIST_FOR_WALK_THRU;
							
							lilc_ptr->requested_irrigation_seconds_ul = lwalk_thru->run_time_seconds;
				
							// 3/22/2018 rmd : During WALK_THRU the cycle time is the run time.
							lilc_ptr->cycle_seconds_ul = lwalk_thru->run_time_seconds;
				
							// 10/5/2012 rmd : Everything else in the ilc is set to 0 (false). And that is appropriate
							// for WALK_THRU.
							
							// ----------
				
							memset( &test_d_and_r, 0, sizeof(ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT) );
				
							// 10/11/2012 rmd : For the case of WALK_THRU there are no directions set to (true). We
							// ignore all of them and insert the ilc into the list.
							
							// ----------
				
							if( _nm_nm_nm_add_to_the_irrigation_list( lilc_ptr, lss_ptr, NULL, &test_d_and_r ) )
							{
								number_added += 1;
							}
							else
							{
								// 3/22/2018 rmd : Well there are reason such as MLB, or MV closed. So no alert?
							}
							
						}  // of we found a valid ilc to use
	
					}  // of if we found the station
					
				}  // of if the slot is occupied
				
			}  // of each sequence slot
			
			xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
		
			xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );
		
			xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
			
			// ----------
			
			// 10/15/2018 Ryan : If walk thru has been added to the list, then make an alert for the
			// user about it.
			if( number_added )
			{		
				Alert_walk_thru_started( lwalk_thru->group_name );
			}
			
		}  // of if we loaded the kick off struct
		
	}  // of if the memory block was available
	
}

/* ---------------------------------------------------------- */
extern void FOAL_IRRI_initiate_manual_watering( MANUAL_WATER_KICK_OFF_STRUCT *mwkos )
{
	const volatile float SIXTY_POINT_ZERO = 60.0;

	ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT	manual_d_and_r;

	IRRIGATION_LIST_COMPONENT	*lilc_ptr;

	BOOL_32	lwater_this_station;

	// ----------

	STATION_GROUP_STRUCT	*lschedule;

	UNS_32	GID_irrigation_schedule;

	lschedule = NULL;

	// ----------

	BOOL_32	lat_least_one_station_added;

	lat_least_one_station_added = (false);

	// ----------

	DATE_TIME_COMPLETE_STRUCT	ldt;

	EPSON_obtain_latest_complete_time_and_date( &ldt );

	// ----------

	xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( station_preserves_recursive_MUTEX, portMAX_DELAY );

	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	// ----------

	STATION_STRUCT	*lss_ptr;

	lss_ptr = nm_ListGetFirst( &station_info_list_hdr );

	while( lss_ptr != NULL )
	{
		lwater_this_station = (false);

		GID_irrigation_schedule = STATION_get_GID_station_group( lss_ptr );

		if( mwkos->manual_how == MANUAL_WATER_A_STATION )
		{
			// 4/18/2014 rmd : Must be fixed so the kick off strucutre use index not serial number.
			if( (nm_STATION_get_box_index_0(lss_ptr) == mwkos->box_index_0) && (nm_STATION_get_station_number_0(lss_ptr) == mwkos->station_number) )
			{
				lwater_this_station = (true);
			}
		}
		else if( mwkos->manual_how == MANUAL_WATER_A_PROGRAM )
		{
			if( GID_irrigation_schedule == mwkos->program_GID )
			{
				lwater_this_station = (true);
			}
		}
		else
		{
			lwater_this_station = (true);
		}

		// 4/4/2013 ajv : We've determined that this station is to be watered.
		// However, we need make sure the station is in use and it's card and
		// terminal are connected to the TP Micro.
		if( lwater_this_station )
		{
			lwater_this_station = STATION_station_is_available_for_use( lss_ptr );
		}

		if( lwater_this_station )
		{
			// 8/30/2012 rmd : See if there is a slot available to use.
			lilc_ptr = nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc();
		
			if( lilc_ptr == NULL )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_IRRI: ilc not available" );
			}
			else
			{
				// Set the pointer to the SCHEDULE GROUP.
				if( GID_irrigation_schedule > 0 )
				{
					lschedule = STATION_GROUP_get_group_with_this_GID( GID_irrigation_schedule );
				}
				else
				{
					lschedule = NULL;
				}

				// 4/18/2014 rmd : For the case of manual watering a program must use the 'get' function as
				// mwkos doesn't contain what we need to stuff the ilc.
				lilc_ptr->box_index_0 = nm_STATION_get_box_index_0(lss_ptr);
				
				lilc_ptr->station_number_0_u8 = nm_STATION_get_station_number_0(lss_ptr);
				
				lilc_ptr->bbf.w_reason_in_list = IN_LIST_FOR_MANUAL;
				
				if( mwkos->manual_how == MANUAL_WATER_A_STATION )
				{
					lilc_ptr->requested_irrigation_seconds_ul = mwkos->manual_seconds;
				}
				else
				{
					lilc_ptr->requested_irrigation_seconds_ul = ( SIXTY_POINT_ZERO * nm_WATERSENSE_calculate_run_minutes_for_this_station(	NOT_FOR_FTIMES,
																																			lss_ptr,
																																			lschedule,
																																			NULL,
																																			&ldt,
																																			nm_STATION_get_distribution_uniformity_100u(lss_ptr) ) );

					lilc_ptr->requested_irrigation_seconds_ul = ( SIXTY_POINT_ZERO * nm_WATERSENSE_calculate_adjusted_run_time(	NOT_FOR_FTIMES,
																																lss_ptr,
																																NULL,
																																(lilc_ptr->requested_irrigation_seconds_ul / SIXTY_POINT_ZERO),
																																STATION_get_ET_factor_100u(lss_ptr),
																																PERCENT_ADJUST_get_station_percentage_100u(lss_ptr, ldt.date_time.D) ) );
				}

				lilc_ptr->cycle_seconds_ul = (nm_STATION_get_cycle_minutes_10u( lss_ptr ) * 6);

				// 4/4/2014 ajv : Everything else in the ilc is set to 0
				// (false) which is appropriate for MANUAL.
				
				// ----------

				memset( &manual_d_and_r, 0, sizeof(ADD_TO_LIST_DIRECTIONS_AND_RESULTS_STRUCT) );

				// 4/4/2013 ajv : For the case of MANUAL, there are no
				// directions to set True. We ignore all of them and add the
				// valve to the list.
				
				// ----------

				if( _nm_nm_nm_add_to_the_irrigation_list( lilc_ptr, lss_ptr, NULL, &manual_d_and_r ) == (true) )
				{
					lat_least_one_station_added = (true);
				}

			}  // of we found a valid ilc to use

			// Since we're only manually watering a single station, break out of
			// the loop after it's been added to the list.
			if( mwkos->manual_how == MANUAL_WATER_A_STATION )
			{
				break;
			}

		} // of this station is to be watered

		// Get the next station
		lss_ptr = nm_ListGetNext( &station_info_list_hdr, lss_ptr );
	}

	// ----------

	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );

	xSemaphoreGiveRecursive( station_preserves_recursive_MUTEX );

	xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

	// ----------

	// 4/4/2013 ajv : Generate an alert if at least one station was added to the
	// list.
	if( lat_least_one_station_added == (true) )
	{
		switch( mwkos->manual_how )
		{
			case MANUAL_WATER_A_STATION:
				Alert_manual_water_station_started_idx( mwkos->box_index_0, mwkos->station_number, INITIATED_VIA_KEYPAD );
				break;

			case MANUAL_WATER_A_PROGRAM:
				if( lschedule != NULL )
				{
					Alert_manual_water_program_started( nm_GROUP_get_name(lschedule), INITIATED_VIA_KEYPAD );
				}
				break;

			case MANUAL_WATER_ALL:
				Alert_manual_water_all_started( INITIATED_VIA_KEYPAD );
				break;
		}
	}
}

/* ---------------------------------------------------------- */
extern void FOAL_IRRI_initiate_or_cancel_a_master_valve_override( const UNS_32 psystem_gid, const UNS_32 pmvor_action_to_take, const UNS_32 mvor_seconds, const INITIATED_VIA_ENUM pinitiated_by )
{
	DATE_TIME_COMPLETE_STRUCT	ldtcs;

	BY_SYSTEM_RECORD	*lpreserve;

	char	lgroup_name[ NUMBER_OF_CHARS_IN_A_NAME ];

	// ----------

	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// ----------

	EPSON_obtain_latest_complete_time_and_date( &ldtcs );

	if( psystem_gid > 0 )
	{
		lpreserve = SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid( psystem_gid );

		// ----------

		if( lpreserve != NULL )
		{
			// ----------

			xSemaphoreTakeRecursive( list_system_recursive_MUTEX, portMAX_DELAY );

			strlcpy( lgroup_name, nm_GROUP_get_name( SYSTEM_get_group_with_this_GID( psystem_gid ) ), NUMBER_OF_CHARS_IN_A_NAME );

			xSemaphoreGiveRecursive( list_system_recursive_MUTEX );

			// ----------

			// 9/30/2015 rmd : If there is a MLB do not allow MVOR to go into effect. But DO ALLOW it to
			// be cancelled. As it is cancelled when a MLB occurs.
			if( SYSTEM_PRESERVES_there_is_a_mlb( &(lpreserve->delivered_mlb_record) ) && (pmvor_action_to_take != MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE) )
			{
				Alert_MVOR_skipped( lgroup_name, SKIPPED_DUE_TO_MLB );
			}
			else
			if( (lpreserve->sbf.MVOR_in_effect_closed) && (pmvor_action_to_take == MVOR_ACTION_OPEN_MASTER_VALVE) )
			{
				// 8/28/2015 ajv : The Master Valve is already forced closed and now a request is received
				// to open it. Since there's likely a good reason why it's been forced closed, we're going
				// to allow that to take precedent and generate an alert to that effect. This matches how
				// the ET2000e operated.
				Alert_MVOR_skipped( lgroup_name, SKIPPED_DUE_TO_MVOR_CLOSED );
			}
			else
			{
				// 8/1/2016 rmd : If we are actually going to OPEN or CLOSE a MV setup a MLB checking
				// blockout period. This is especially needed if we are opening the MV to allow for possible
				// mainline inrush flow. However I've adopted the philosophy that when either opening or
				// closing the MV (changing state) we should establish a MLB blockout period. AND let's just
				// take the approach that if there is going to be a MVOR state change regardless of the type
				// of MV we'll set the timer. Why regardless of the type of MV? Because it is hard to know
				// in a complicated system how many NO and NC valves we actually have. So the safest route
				// is to start the blocking timer when the MVOR state changes.
				if( (pmvor_action_to_take == MVOR_ACTION_OPEN_MASTER_VALVE) && (lpreserve->sbf.MVOR_in_effect_opened == (false)) )
				{
					// 8/1/2016 rmd : So the MV is closed and it is going to be opened.
					lpreserve->timer_MVJO_flow_checking_blockout_seconds_remaining = MLB_AND_FLOW_CHECKING_MVJO_BLOCKOUT_SECONDS;
				}

				if( (pmvor_action_to_take == MVOR_ACTION_CLOSE_MASTER_VALVE) && (lpreserve->sbf.MVOR_in_effect_closed == (false)) )
				{
					// 8/1/2016 rmd : MVOR change of state going to happen.
					lpreserve->timer_MVJO_flow_checking_blockout_seconds_remaining = MLB_AND_FLOW_CHECKING_MVJO_BLOCKOUT_SECONDS;
				}

				if( (pmvor_action_to_take == MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE) && ((lpreserve->sbf.MVOR_in_effect_opened == (true)) || (lpreserve->sbf.MVOR_in_effect_closed == (true))) )
				{
					// 8/1/2016 rmd : MVOR change of state going to happen.
					lpreserve->timer_MVJO_flow_checking_blockout_seconds_remaining = MLB_AND_FLOW_CHECKING_MVJO_BLOCKOUT_SECONDS;
				}
				
				// ----------
				
				switch( pmvor_action_to_take )
				{
					case MVOR_ACTION_OPEN_MASTER_VALVE:
					case MVOR_ACTION_CLOSE_MASTER_VALVE:

						Alert_MVOR_started( lgroup_name, pmvor_action_to_take, mvor_seconds, pinitiated_by );

						lpreserve->sbf.MVOR_in_effect_opened = (pmvor_action_to_take == MVOR_ACTION_OPEN_MASTER_VALVE);
						lpreserve->sbf.MVOR_in_effect_closed = (pmvor_action_to_take == MVOR_ACTION_CLOSE_MASTER_VALVE);

						TDUTILS_add_seconds_to_passed_DT_ptr( &ldtcs.date_time, mvor_seconds );

						lpreserve->mvor_stop_date = ldtcs.date_time.D;
						lpreserve->mvor_stop_time = ldtcs.date_time.T;

						lpreserve->MVOR_remaining_seconds = mvor_seconds;

						break;

					case MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE:
					default:

						// 10/1/2015 rmd : Only make the alert line if we are actually cancelling something.
						// Otherwise it is a nuisance.
						if( lpreserve->sbf.MVOR_in_effect_opened || lpreserve->sbf.MVOR_in_effect_closed )
						{
							Alert_MVOR_started( lgroup_name, MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE, mvor_seconds, pinitiated_by );
						}
						
						lpreserve->sbf.MVOR_in_effect_opened = (false);
						lpreserve->sbf.MVOR_in_effect_closed = (false);

						lpreserve->mvor_stop_date = DATE_MINIMUM;
						lpreserve->mvor_stop_time = TIME_MINIMUM;

						// 8/26/2015 ajv : Since we're cancelling the Master Valve Override, zero out the seconds
						// too.
						lpreserve->MVOR_remaining_seconds = 0;

						break;

				}  // of switch
			}
		}
		else
		{
			Alert_func_call_with_null_ptr();
		}
	}
	else
	{
		Alert_Message_va( "MVOR: System GID should not be 0 (%d)", pinitiated_by );
	}

	// ----------

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called when the token is being assembled.

    @CALLER_MUTEX_REQUIREMENTS The caller should be holding the foal_irri MUTEX to protect
    the pilc_ptr list item data. AND the system_preserves MUTEX as we are going to load the
    line fill time and adjust other items in the system_preserve record.
	
	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Called with the context of the TD_CHECK task. As part of the foal_irri list
    maintenance function. When it is decided to turn ON a station.

	@RETURN (none)

	@ORIGINAL 2012.04.26 rmd

	@REVISIONS
*/
static void _nm_nm_foal_irri_complete_the_action_needed_turn_ON( IRRIGATION_LIST_COMPONENT *pilc_ptr )
{
	if( nm_OnList( &foal_irri.list_of_foal_all_irrigation, pilc_ptr) == (false) )
	{
		//ALERT_MESSAGE_WITH_FILE_NAME( "FOAL: pointer not on list!" );
		Alert_item_not_on_list( pilc_ptr, foal_irri.list_of_foal_all_irrigation );
	}
	else
	{
		// --------------------------------
		
		// 4/26/2012 rmd : We are sending the action_needed record to turn ON the station. So do it
		// now.
		pilc_ptr->bbf.station_is_ON = (true);
		
			
		// --------------------------------
		
		// 5/11/2012 rmd : Make sure the index is within range as we are going to use it 'blindly'
		// to index an array. If out of range a bug like that could cause a crash and be VERY hard
		// to find.
		RangeCheck_uns8( &(pilc_ptr->box_index_0), BOX_INDEX_MINIMUM, BOX_INDEX_MAXIMUM, BOX_INDEX_DEFAULT, NULL, "Box Index" );

		// 5/9/2012 rmd : Update the count ON by controller.
		foal_irri.stations_ON_by_controller[ pilc_ptr->box_index_0 ] += 1;
		
		// --------------------------------
		
		// Add to the ON list here - so far this is the only place. Always add the last one ON to
		// the end of the list.
		if( nm_ListInsertTail( &foal_irri.list_of_foal_stations_ON, pilc_ptr ) != MIST_SUCCESS )
		{
			Alert_Message_va( "Controller: %u, sta %u : not added to ON list", pilc_ptr->box_index_0+'A', (pilc_ptr->station_number_0_u8 + 1) );
		}
		
		// --------------------------------

		// 2/20/2013 rmd : Set the flow checking block out timer. If the proposed line fill is more
		// than the remaining block out time.
		if( pilc_ptr->bsr->flow_checking_block_out_remaining_seconds < pilc_ptr->line_fill_seconds )
		{
			pilc_ptr->bsr->flow_checking_block_out_remaining_seconds = pilc_ptr->line_fill_seconds;
		}

		// 2/21/2013 rmd : Do not need to load this all important non-irrigation MLB block out timer
		// here. Because the fact that this function is running guarantees the routine MV
		// anti-chatter timers maintenance function will run which always loads this. So there is no
		// way the station will be ON without this non-irrigation block out timer being set.
		//pilc_ptr->bsr->timer_MLB_just_stopped_irrigating_blockout_seconds_remaining = MLB_JUST_STOPPED_IRRIGATING_CHECKING_BLOCKOUT_SECONDS;

		// --------------------------------

		// 6/5/2012 rmd : SYSTEM level flow checking type flags that are cleared.
		
		pilc_ptr->bsr->sbf.checked_or_updated_and_made_flow_recording_lines = (false);
		
		pilc_ptr->bsr->sbf.system_level_no_valves_ON_therefore_no_flow_checking = (false);
		pilc_ptr->bsr->sbf.system_level_valves_are_ON_but_will_not_be_checking_flow = (false);
		pilc_ptr->bsr->sbf.system_level_valves_are_ON_and_waiting_to_check_flow = (false);
		pilc_ptr->bsr->sbf.system_level_valves_are_ON_and_actively_checking = (false);
		pilc_ptr->bsr->sbf.system_level_valves_are_ON_and_waiting_to_update_derate_table = (false);
		pilc_ptr->bsr->sbf.system_level_valves_are_ON_and_has_updated_the_derate_table = (false);
		pilc_ptr->bsr->sbf.system_level_valves_are_ON_and_waiting_to_acquire_expected = (false);
		
		// --------------------------------

		if( pilc_ptr->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
		{
			// 6/22/2012 rmd : Catch the first cycle start date and time.
			if( station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_number_of_repeats == 0 )
			{
				DATE_TIME	ldt;
				
				EPSON_obtain_latest_time_and_date( &ldt );
				
				station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_first_cycle_start_date = ldt.D;

				station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_first_cycle_start_time = ldt.T;
			}

			// 2000e note: In order to avoid questions, only increment repeat count when station is not
			// on the problem list (for a flow error).
			//
			// 04.20.2006 ajv FIND A WAY TO ALSO NOT INCREMENT IF THE STATION WAS PAUSED DUE TO
			// PRIORITIES See foalirri.c:3347 for current (temporary) solution.
			if( pilc_ptr->bbf.w_involved_in_a_flow_problem == (false) )
			{
				station_preserves.sps[ pilc_ptr->station_preserves_index ].station_history_rip.pi_number_of_repeats += 1;
			}

			// 2011.12.03 rmd : For the reason of Programmed Irrigation mark this station as having
			// irrigated. This flag is set (true) when the station is added to the irrigation list.
			// However, do not flag as having irrigated if we are acquiring the expected.
			if( pilc_ptr->bbf.w_to_set_expected == (false) )
			{
				station_preserves.sps[ pilc_ptr->station_preserves_index ].spbf.did_not_irrigate_last_time = (false);
			}

		}
		
		// ----------
		
		// 5/5/2014 rmd : Note - there should be NO attempt made here in this foal side function to
		// clear the station preserves flow error flag. That is done on the irri side when the sxr
		// record to accomplish this turn ON is rcvd.

		// ----------

	}  // of if the pilc_ptr is on the list
}
				
/* ---------------------------------------------------------- */
/**
    @DESCRIPTION When the token is made we extract any SXRU records that need to go to the
    pcontact_index argument. This function will allocate and fill the needed memory. Setting
    the pointer included as a argument to the start of the block. And will return the length
    of the block. The length reflects the size of the block to be placed within the token.
    The length should be returned as 0 if the block is not to be sent. The memory block will
    be dynamically allocated and must be freed after the caller takes a copy of the data and
    implants it into the message.
 
    In a sense the action_needed reason reflects the final reason after what may have been a
    series of updates to the reson once the station was added to the action_needed list. The
    design is to support this. And this activity may occur. For example if the FOAL_IRRI
    maintenance function adds the station to the list to turn ON. Then before we generate
    the next outgoing token we receive a token RESP that requests we remove the station from
    the list. We would over-write the ON reason with the REMOVE reason and turn the station
    OFF. As we should. And that is what would finally get transmitted.

    There are only three basic actions that take place to a station in the list. Turn it ON.
    Turn it OFF. And removal from the list. Only the REMOVAL from the list occurs as part of
    the token construction. If it were removed we would be able to signal the irri machines
    about it's removal as it would no longer be on the action_needed list.

	@CALLER_MUTEX_REQUIREMENTS (none)

    @MEMORY_RESPONSIBILITES This function will allocate a memory block if there is data to
    send.

    @EXECUTED Executed within the context of the COMM_MNGR task.
	
    @RETURN Set the pdata_ptr to the start of the block if there is data to ship, NULL other
    wise. The return value is the length of the block to ship. And should return 0 if there
    is no data to include within the token.

	@ORIGINAL 2012.03.06 rmd

	@REVISIONS
*/
extern UNS_32 FOAL_IRRI_buildup_action_needed_records_for_token( UNS_8 **pdata_ptr )
{
	UNS_32	rv;
	
	// 2012.05.06 rmd : The return value is the size of the xfer.
	rv = 0;
	
	*pdata_ptr = NULL;
	
	// 11/12/2014 rmd : Take the list MUTEX as we are traversing the action_needed list.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	// 2012.05.06 rmd : This variable is purposely an UNS_16. We include the count in the
	// message and to save space we only have this as a 2-byte count.
	UNS_16	action_records_to_xfer;
	
	// 4/17/2012 rmd : Since we are using a broadcast TOKEN all the action_needed records on the
	// list go.
	action_records_to_xfer = foal_irri.list_of_foal_stations_with_action_needed.count;
	
	if( action_records_to_xfer > 0 )
	{
		// So there is at least one xfer to include. So build up the transfer.
		UNS_32	llength;
		
		UNS_8	*ucp;

		// Include the 2-bytes that says how many xfers are included. And for each record the size
		// of the sxru struct.
		llength = ( 2 + ( action_records_to_xfer * sizeof( ACTION_NEEDED_XFER_RECORD ) ) );
		
		ucp = mem_malloc( llength );
		
		*pdata_ptr = ucp;  // set the pointer for the caller
		
		rv = llength;  // and set the return value now cause we know the length
		
			
		memcpy( ucp, &action_records_to_xfer, sizeof(UNS_16) );
		ucp += sizeof( UNS_16);
		llength -= sizeof( UNS_16);
		

		IRRIGATION_LIST_COMPONENT	*lilc;
	
		lilc = nm_ListRemoveHead( &foal_irri.list_of_foal_stations_with_action_needed );

		while( lilc != NULL )
		{
			ACTION_NEEDED_XFER_RECORD	lanxr;
			
			// 11/13/2014 rmd : Zero the record to be clean.
			memset( &lanxr, 0x00, sizeof(ACTION_NEEDED_XFER_RECORD) );

			lanxr.sxru_xfer_reason_u8 = lilc->action_reason;
			
			lanxr.box_index_0 = lilc->box_index_0;
			
			lanxr.station_number_0_u8 = lilc->station_number_0_u8;

			lanxr.w_irrigation_reason_in_list_u8 = lilc->bbf.w_reason_in_list;
			
			// 11/13/2014 rmd : Always include a copy of the remaining requested irrigation amount. To
			// easily keep the IRRI side in sync. Only used when turning ON and when turning OFF. List
			// removal reasons don't care.
			lanxr.updated_requested_irrigation_seconds_ul = lilc->requested_irrigation_seconds_ul;
			
			if( ( lilc->action_reason == ACTION_REASON_TURN_ON_and_clear_flow_status ) ||
				( lilc->action_reason == ACTION_REASON_TURN_ON_and_leave_flow_status )
			  )
			{
				// 4/26/2012 rmd : Prior to the decision to add this station to the action_needed list for
				// the reason to turn ON the remaining seconds ON were updated. So they're ready to use.
				lanxr.remaining_ON_or_soak_seconds = lilc->remaining_seconds_ON;
			}
			else
			{
				// 11/13/2014 rmd : For ALL other reasons just grab the soak seconds. The list removal
				// reasons will not use. Other particular reasons will.
				lanxr.remaining_ON_or_soak_seconds = lilc->soak_seconds_remaining_ul;
			}
			
			// ----------
			
			// 4/26/2012 rmd : Now copy the completed action_needed record into the message.
			memcpy( ucp, &lanxr, sizeof( ACTION_NEEDED_XFER_RECORD ) );
			ucp += sizeof( ACTION_NEEDED_XFER_RECORD );
			llength -= sizeof( ACTION_NEEDED_XFER_RECORD );
			action_records_to_xfer -= 1;
			
			// ----------
			
			// 11/12/2014 rmd : Now get and remove the next item on the list.
			lilc = nm_ListRemoveHead( &foal_irri.list_of_foal_stations_with_action_needed );
		}

		// ----------
		
		if( llength || action_records_to_xfer )
		{
			ALERT_MESSAGE_WITH_FILE_NAME( "FOAL: action_needed token error." );
		}

	}  // of if the number of action_needed station in the list is not 0
	
	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Takes the ilc and figures out how many seconds to set in preparation to
    running a cycle. The various reasons in the list change the result.

	@CALLER MUTEX REQUIREMENTS (none)

    @EXECUTED Executed within the context of the TD_CHECK task as part of the FOAL
    irrigation maintenance function.
	
	@RETURN (none)

	@AUTHOR 2011.11.10 rmd
*/
static void set_remaining_seconds_on( IRRIGATION_LIST_COMPONENT *ilc )
{
	// We are here to adjust the remaining times in preparation of a turn on. if we return with
	// no time to run that is recognized as an error so set that error condition now.
	ilc->remaining_seconds_ON = 0;
	
	// Currently anybody in the list could have his set expected bit set. This causes us to
	// first learn the expected flow and then perhaps based on the reason stay in the list or
	// maybe get removed (programmed irrigation would stay - test would be done after they
	// acquired the expected).
	if ( ilc->bbf.w_to_set_expected == (true) ) {
	
		if ( ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE ) {

			ilc->remaining_seconds_ON = ilc->requested_irrigation_seconds_ul;  // using this value gives the remote (handheld) the ability to set for how long

			ilc->requested_irrigation_seconds_ul = 0;

		} else
		if ( ilc->bbf.w_reason_in_list == IN_LIST_FOR_TEST ) {

			ilc->remaining_seconds_ON = 360;  // see note that follows

			// If we are in the list for test after the set expected functionality is complete there is
			// no more left to do (unlike prorammed irrigation) so drive the remaining time to 0.
			ilc->requested_irrigation_seconds_ul = 0;

		}
		else
		{
			// 6/20/2012 rmd : 2000e comment - Set expected using a fixed amount of time that does not
			// affect the ProgrammedSecondsLeft. This was set to 3 minutes and that proved to not be
			// long enough for some customers ... lengthend to 6 minutes ... a better way may be to
			// embed in the ilc the line fill time and use that plus say 60 seconds as the length to run
			// for.
			
			// 6/20/2012 rmd : In the case of programmed irrigation the MVJO timer gets in the way of
			// acquiring the expected. With the 6 minutes here we're okay as the MVJO timer is set to a
			// fixed 150 seconds at the first opening.
			ilc->remaining_seconds_ON = 360;
		}

	}
	else
	{
		// Set the time to run for based on why its on.
		switch( ilc->bbf.w_reason_in_list )
		{
			case IN_LIST_FOR_MOBILE:
				// Simple for the case of radio remote - by definition we always turn on for the full
				// RemainingTotalSeconds.
				ilc->remaining_seconds_ON = ilc->requested_irrigation_seconds_ul;

				ilc->requested_irrigation_seconds_ul = 0;

				break;
			
			
			case IN_LIST_FOR_TEST:
				// Simple for the case of test - by definition we always turn on for the full
				// RemainingTotalSeconds
				ilc->remaining_seconds_ON = ilc->requested_irrigation_seconds_ul;

				ilc->requested_irrigation_seconds_ul = 0;

				break;
				
			case IN_LIST_FOR_WALK_THRU:
				// When these stations are added to the list if they are in the list already for this reason
				// they are over written (we do not add to their existing time) So we always irrigate their
				// total time.
				ilc->remaining_seconds_ON = ilc->requested_irrigation_seconds_ul;

				ilc->requested_irrigation_seconds_ul = 0;

				break;

			case IN_LIST_FOR_MANUAL:
				// Set the time to irrigate for a normally scheduled cycle for manual irrigation which
				// follows the cycle and soak rules if cycle and soak during manual is set TRUE ... this
				// evaluation is made when the station is added to the irri list on the irri side.
				if ( ilc->requested_irrigation_seconds_ul > ilc->cycle_seconds_ul )
				{
					ilc->remaining_seconds_ON = ilc->cycle_seconds_ul;

					ilc->requested_irrigation_seconds_ul -= ilc->cycle_seconds_ul;
				}
				else
				{
					ilc->remaining_seconds_ON = ilc->requested_irrigation_seconds_ul;

					ilc->requested_irrigation_seconds_ul = 0;
				}
				break;
			
			case IN_LIST_FOR_MANUAL_PROGRAM:
				// When a station is added to the irrigation list for the SMS reason, if the station doesn't
				// exsist in the list for that reason set the cycle time equal to the total time and set the
				// total time to the amount to irrigate. If it already exists add to the amount to irrigate
				// and leave the cycle time alone. Therefore here we look at the total as compared to the
				// cycle and figure the remaining seconds on just like for regular programmed irrigation.
				if( ilc->requested_irrigation_seconds_ul > ilc->cycle_seconds_ul )
				{
					ilc->remaining_seconds_ON = ilc->cycle_seconds_ul;

					ilc->requested_irrigation_seconds_ul -= ilc->cycle_seconds_ul;
				}
				else
				{
					ilc->remaining_seconds_ON = ilc->requested_irrigation_seconds_ul;

					ilc->requested_irrigation_seconds_ul = 0;
				}
				break;
			
			case IN_LIST_FOR_PROGRAMMED_IRRIGATION:
				// set the time to irrigate for a normally scheduled cycle
				if( ilc->requested_irrigation_seconds_ul > ilc->cycle_seconds_ul )
				{
					ilc->remaining_seconds_ON = ilc->cycle_seconds_ul;

					ilc->requested_irrigation_seconds_ul -= ilc->cycle_seconds_ul;
				}
				else
				{
					ilc->remaining_seconds_ON = ilc->requested_irrigation_seconds_ul;

					ilc->requested_irrigation_seconds_ul = 0;
				}
				break;
			
		}  // of switch of reason on
		
	}  // of normal turn on set time by reason on

}
	
/*--------------------------------------------------------------------------
	Name:           BOOLEAN WillExceedSystemFlowLimit( IRRIGATION_LIST_COMPONENT *pilc,	unsigned short pSumOfEstimateds, unsigned short pnumber_currently_on )
	
	FileName:		C:\develop\GHS\ET2000\ETSource\foalirri.c

	Description:    Called as part of the to turn on decision - of course by the foal machine
					only.
					
					If we dont have estimateds to work with return FALSE which says
					the flow is ok.
					
					The passed in Flow should be the sum of the estimateds of those ON and set 
					to go ON. Be very careful that we include those that are SET TO GO ON i.e.
					marked for xfer to turn on. Remember a station could be marked for xfer to turn
					on for several seconds in a longer chain before we actually xfer it and
					add it to the foal ONList. During that wait time if we scan again for ones
					to turn on we MUST include those marked for turn on as already on. That is
					accomplished using the OnByController lists.
					
	Parameters:     pFlow and the list item we are consdering turning on
	
	Return Values:  TRUE	if turning on station will exceed system limit
					FALSE	if OK to turn on this station as far as flow is concerned
					
	History:		date		approval	author	description
					02/15/01	--/--/--	rmd		created
					05/01/01	--/--/--	rmd		updated to coding standard
													
 -------------------------------------------------------------------------*/
static BOOL_32 will_exceed_system_capacity( IRRIGATION_LIST_COMPONENT const *pilc )
{
	// I don't think the all_remaining_greater_than_limit variable is necessary. If there are
	// none ON and one ready to turn on has an Estimated greater than the SystemMax - it's
	// greater than the system Max - why do we wait until the end to adjust it? why not adjust
	// it now??

	BOOL_32	rv, with_pump;

	char	str_8[ 8 ];
	
	rv = (false);

	if( SYSTEM_get_capacity_in_use_bool( pilc->bsr->system_gid ) == (true) )
	{
		// If the pump is on - which in the case of say radio remote or test the pump could be on
		// for one station and we are going to turn on a station that doesn't use the pump - we still
		// need to compare to the pump number.
		//
		// If the one we are to turn on is going to use the pump - compare to pump capacity	number.
		UNS_32	capacity;
	
		if( (pilc->bsr->ufim_stations_ON_with_the_pump_b == (true)) || (pilc->bbf.w_uses_the_pump == (true)) )
		{
			capacity = SYSTEM_get_capacity_with_pump_gpm( pilc->bsr->system_gid );
			
			with_pump = (true);
		}
		else
		{
			capacity = SYSTEM_get_capacity_without_pump_gpm( pilc->bsr->system_gid );

			with_pump = (false);
		}
		
		if( pilc->bsr->system_master_number_of_valves_ON == 0 )
		{
			// If nothing is on - can check expected against capacity numbers. Include pSum so the logic
			// works even if pSum is not 0.
			if( pilc->expected_flow_rate_gpm_u16 > capacity )
			{
				ALERTS_get_station_number_with_or_without_two_wire_indicator( pilc->box_index_0, pilc->station_number_0_u8, str_8, sizeof(str_8) );

				// Tell user what we are overriding the capacity limit.
				if( with_pump == (true) )
				{
					Alert_Message_va( "Pump Capacity Override: Station %s expected is higher.", str_8 );
				}
				else
				{
					Alert_Message_va( "Non-Pump Capacity Override: Station %s expected is higher.", str_8 );
				}

				// Adjust it! So it will come on. Don't let the user step in a pile of shit i.e. blocking
				// out a station.
				capacity = pilc->expected_flow_rate_gpm_u16;
			}
		}

		if( (pilc->bsr->ufim_expected_flow_rate_for_those_ON + pilc->expected_flow_rate_gpm_u16) > capacity )
		{
			rv = (true);
		}
	}

	return( rv );
}

/* ---------------------------------- */
BOOL_32 FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list( UNS_32 preason, UNS_32 phighest_reason )
{
	BOOL_32	rv;

	// 7/17/2015 rmd : FOR THE TURN ON RULES - the reasons MANUAL_PROGRAM and PROGRAMMED
	// irrigation are considered equal for the TURN ON RULES.
	
	if( (preason == IN_LIST_FOR_MANUAL_PROGRAM) || (preason == IN_LIST_FOR_PROGRAMMED_IRRIGATION) )
	{
		rv = ( phighest_reason > IN_LIST_FOR_MANUAL_PROGRAM );
	}
	else
	{
		rv = ( phighest_reason > preason );
	}
	
	return( rv );
}

/* ---------------------------------- */
BOOL_32 FOAL_IRRI_there_are_other_flow_check_groups_already_ON( BIG_BIT_FIELD_FOR_ILC_STRUCT *pbbf_ptr, UNS_32 *parray_ptr )
{
	BOOL_32	rv;
	
	rv = (false);
	
	UNS_32	i;
	
	for( i=FLOW_CHECK_GROUP_no_flow_checking; i<=FLOW_CHECK_GROUP_check_for_high_and_low_flow; i++ )
	{
		if( i != pbbf_ptr->flow_check_group )
		{
			if( *parray_ptr > 0 )
			{
				rv = (true);
				
				// 4/30/2018 rmd : Only need to see one.
				break;
			}
		}
		
		parray_ptr++;
	}
	
	return( rv );
}	

/* ---------------------------------- */
BOOL_32 FOAL_IRRI_there_is_more_than_one_flow_group_ON( UNS_32 *parray_ptr )
{
	BOOL_32	rv;
		  
	rv = (false);
	
	UNS_32	i, groups_on;	
	
	groups_on = 0;
	
	for( i=FLOW_CHECK_GROUP_no_flow_checking; i<=FLOW_CHECK_GROUP_check_for_high_and_low_flow; i++ )
	{
		if( *parray_ptr > 0 )
		{
			groups_on += 1;
		}
		
		parray_ptr++;
	}
	
	if( groups_on > 1 )
	{
		rv = (true);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Will add the pilc irrigation list item to the action needed list. The
    caller MUST be holding the foal_irri MUTEX so we can modify the list.

	The MLB and STOP processes do NOT use the action_needed list.

    @CALLER_MUTEX_REQUIREMENTS The caller MUST be holding the foal_irri_recursive_MUTEX. We
    are messing with the action_needed list. Or potetially the reason on the list.
	
	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task while performing the
    foal_irri list maintenance.

	@RETURN (none)

	@ORIGINAL 2012.03.12 rmd

	@REVISIONS
*/
extern void nm_FOAL_add_to_action_needed_list( IRRIGATION_LIST_COMPONENT *pilc, UNS_32 paction_reason )
{
	UNS_32	how_many_on_list;
				
	if( !nm_OnList( &foal_irri.list_of_foal_stations_with_action_needed, (void*)pilc ) )
	{
		// 4/24/2012 rmd : It is not on the action_needed list. So add it.
		nm_ListInsertTail( &foal_irri.list_of_foal_stations_with_action_needed, pilc );
	}
	else
	{
		// 6/4/2012 rmd : Not sure under which conditions this will happen. So at least lets let
		// engineering know about it.
		
		// 6/7/2012 rmd : Here's one scenario that it happens for. In the foal_irri list maintenance
		// function when it is time to turn OFF a station we do so. Which causes the ilc to be
		// re-positioned in the list according to it's new weighting. Which likely means lower in
		// the list since it just ran a cycle. We are stepping through the list when this happens.
		// Which means we then bump into this list item again. This time it is OFF and it may be
		// ready for list removal. If it also happens to be ready for removal we are going to put it
		// on the list for that reason. And we end up here. Changing the reason it is on the list.
		// There is no harm yet identified by changing the list reason. As there is no action
		// performed when the token is assembled (meaning we would skip the action for the first
		// reason). So I let this happen. And we'll see if all performs correctly. Which I think it
		// will.
		//
		// Alert however about overwrite reasons we have not yet understood. So we are aware of and
		// 'approve' of them.
		if( pilc->action_reason != ACTION_REASON_TURN_OFF_cause_cycle_has_completed )
		{
			// 11/5/2014 rmd : If it is on the list for a different reason let's hear about it.
			if( pilc->action_reason != paction_reason )
			{
				how_many_on_list = foal_irri.list_of_foal_stations_with_action_needed.count;
				
				Alert_Message_va( "Already on action needed list: for %u, changing to %u, count=%u", pilc->action_reason, paction_reason, how_many_on_list );
			}
		}
	}
	
	// ----------
	
	// 11/5/2014 rmd : The final outcome is ALWAYS the ilc is on the action needed list for the
	// reason requested. The scheme we have adopted is the last reason wins.
	pilc->action_reason = paction_reason;
}

/* ---------------------------------------------------------- */
static void _if_supposed_to_but_did_not_ever_check_flow_make_an_alert( IRRIGATION_LIST_COMPONENT *pilc )
{
	// 6/5/2012 rmd : Only for the case of programmed irrigation do we make this alert.
	if( pilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
	{
		if( pilc->bbf.at_some_point_should_check_flow )
		{
			if( pilc->bbf.at_some_point_flow_was_checked == (false) )
			{
				station_preserves.sps[ pilc->station_preserves_index ].station_history_rip.pi_flag.flow_never_checked = (true);
				
				Alert_flow_not_checked_with_no_reasons_idx( pilc->box_index_0, pilc->station_number_0_u8 );
			}
		}
	}
}

/* ---------------------------------------------------------- */
static BOOL_32 foal_irri_maintenance_may_run( void )
{
	BOOL_32	rv;
	
	// ----------
	
	rv = (true);
	
	// 11/7/2014 rmd : Let the lists get in sync. Over there on the IRRI side there may be turn
	// OFFs that need to take place or list removals that need to happen. It seems better to let
	// those activities take palce before tryingto yet turn on other valves. Have not shown this
	// to be a problem but I think is a conservative method of operation.
	if( foal_irri.list_of_foal_stations_with_action_needed.count != 0 )
	{
		rv = (false);	
	}

	// ----------
	
	return( rv );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function runs at a 1 hertz rate. And is to be called regardless if the
    foal_irri function is blocked from running because of (typically) stations are on the
    action_needed list. We simply look at the timers and decrement if they have a count.

    @CALLER_MUTEX_REQUIREMENTS (none) This function will take the system_preserves MUTEX as
    needed.
	
	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task as part of the foal irri
    irrigation list maintenance.

	@RETURN (none)

	@ORIGINAL 2012.06.19 rmd

	@REVISIONS
*/
static void decrement_system_preserves_timers( void )
{
	UNS_32	s;
	
	BY_SYSTEM_RECORD	*pbsr_ptr;
	
	pbsr_ptr = &(system_preserves.system[ 0 ]);
	
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	for( s=0; s<MAX_POSSIBLE_SYSTEMS; s++ )
	{
		// 6/19/2012 rmd : Only if the system is in use.
		if( pbsr_ptr->system_gid != 0 )
		{
			if( pbsr_ptr->timer_MVJO_flow_checking_blockout_seconds_remaining > 0 )
			{
				pbsr_ptr->timer_MVJO_flow_checking_blockout_seconds_remaining -= 1;
			}
		
			if( pbsr_ptr->timer_MLB_just_stopped_irrigating_blockout_seconds_remaining > 0 )
			{
				pbsr_ptr->timer_MLB_just_stopped_irrigating_blockout_seconds_remaining -= 1;
			}
		
			if( pbsr_ptr->flow_checking_block_out_remaining_seconds > 0 )
			{
				pbsr_ptr->flow_checking_block_out_remaining_seconds -= 1;
			}
		
			if( pbsr_ptr->inhibit_next_turn_ON_remaining_seconds > 0 )
			{
				pbsr_ptr->inhibit_next_turn_ON_remaining_seconds -= 1;
			}
		
			// 8/31/2015 ajv : Note that this value is really only here for display purposes - it has no
			// impact on MVOR_in_effect_opened or MVOR_in_effect_closed since these are set by the
			// FOAL_IRRI_initiate_or_cancel_a_master_valve_override function.
			if( pbsr_ptr->MVOR_remaining_seconds > 0 )
			{
				pbsr_ptr->MVOR_remaining_seconds -= 1;
			}

		}  // of if the system is in use
	
		// -----------------------------
		
		pbsr_ptr += 1;		

		// -----------------------------
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION This function runs at a variable rate but no more than once per second. It
    should be thought of like this. When the foal irri maintenance function is able to run
    it runs at a 1 hz rate. When that function cannot run (action_needed list) we run at the
    token rate. In the case of a long winded SR chain type configuration the token response
    may take several seconds. So in the moment that the foal irri maintenance function
    cannot run we may have quite a gap. But this is by design. Because we don't want to
    count down during moments that stations aren't ON and allow the MV to close. That is the
    whole purpose of this mechanism.

    @CALLER_MUTEX_REQUIREMENTS (none) This function will take the system_preserves MUTEX as
    needed.
	
	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed within the context of the TD_CHECK task as part of the foal irri
    irrigation list maintenance.

	@RETURN (none)

	@ORIGINAL 2012.06.19 rmd

	@REVISIONS
*/

static void _nm_anti_chatter_maintenance_sub_function( BY_SYSTEM_RECORD *pbsr_ptr )
{
	// 10/23/2015 rmd : The anit chatter function provides the smoothed out MV and PUMP
	// activation. To be used ONLY during irrigation. This function is NOT how the MV is
	// controlled for other reasons such as MVOR or MLB. Those reasons are handled on the IRRI
	// side when building the message for the TPMicro.
	
	// ----------
	
	// 6/19/2012 rmd : Only if the system is in use.
	if( pbsr_ptr->system_gid != 0 )
	{
		if( pbsr_ptr->system_master_number_of_valves_ON > 0 )
		{
			// 5/3/2012 rmd : ALWAYS re-load the MV delay timer. This is the timer that keeps the MV
			// open during valve transitions. During those brief 'system moments' when there are
			// actually no valves ON. We load this with a 3. This is not seconds. But rather can be
			// thought of as the longer of the two...time between tokens or seconds.
			pbsr_ptr->transition_timer_all_stations_are_OFF = 3;
			
			// ----------
	
			// 2/20/2013 rmd : By always loading this MLB checking block out mechanism it is ready to go
			// when the number of valves ON drops to 0. We are making sure we bridge from the irrigating
			// to the non-irrigation MLB checking in a manner that doesn't leave any holes (like 1
			// second holes) where we test for non-irrigation MLB after the last valve is turned off but
			// before this is loaded. So keep this loaded while valves are ON!
			pbsr_ptr->timer_MLB_just_stopped_irrigating_blockout_seconds_remaining = MLB_JUST_STOPPED_IRRIGATING_CHECKING_BLOCKOUT_SECONDS;
			
			// ----------
	
			if( pbsr_ptr->sbf.mv_open_for_irrigation == (false) )
			{
				if( pbsr_ptr->sbf.master_valve_has_at_least_one_normally_closed == (true) )
				{
					// 5/3/2012 rmd : So there is at least one NC MV. And it is going to be opened (shortly).
					// Note how this may not interact well with the bypass manifold. As its valves continue to
					// open and close autonomously.
					pbsr_ptr->timer_MVJO_flow_checking_blockout_seconds_remaining = MLB_AND_FLOW_CHECKING_MVJO_BLOCKOUT_SECONDS;
				}
				
				pbsr_ptr->sbf.mv_open_for_irrigation = (true);
			}
			
			// ----------
	
			if( pbsr_ptr->ufim_stations_ON_with_the_pump_b == (true) )
			{
				// 5/3/2012 rmd : Then the pump output should be energized.
				pbsr_ptr->sbf.pump_activate_for_irrigation = (true);
				
				// 6/19/2012 rmd : And reload it's transition timer.
				pbsr_ptr->transition_timer_all_pump_valves_are_OFF = 3;
			}
			else
			{
				if( pbsr_ptr->transition_timer_all_pump_valves_are_OFF > 0 )
				{
					pbsr_ptr->transition_timer_all_pump_valves_are_OFF -= 1;
				}
				
				if( pbsr_ptr->transition_timer_all_pump_valves_are_OFF == 0 )
				{
					// 6/19/2012 rmd : All the pumps should be OFF.
					pbsr_ptr->sbf.pump_activate_for_irrigation = (false);
				}
			}
				
			// ----------
		}
		else
		{
			// 2/20/2013 rmd : For the case of the last valve is a slow closing valve we want to keep
			// the MV open and the PUMP running till the slow closer period has elapsed. Once it has
			// elapsed we begin the MV and PUMP countdown. This way for the case of 1 on-at-a-time and a
			// slow closing valve the MV and PUMP will behave as expected. That is not cycle between
			// valves.
			if( pbsr_ptr->inhibit_next_turn_ON_remaining_seconds == 0 )
			{
				if( pbsr_ptr->transition_timer_all_stations_are_OFF > 0 )
				{
					// 2/20/2013 rmd : Keep reloading the blockout period cause it doesn't start until after the
					// MV and PUMP have shut down. And while waiting it will be chipped away at.
					pbsr_ptr->timer_MLB_just_stopped_irrigating_blockout_seconds_remaining = MLB_JUST_STOPPED_IRRIGATING_CHECKING_BLOCKOUT_SECONDS;

					pbsr_ptr->transition_timer_all_stations_are_OFF -= 1;
		
					if( pbsr_ptr->transition_timer_all_stations_are_OFF == 0 )
					{
						// 6/19/2012 rmd : All the master valves should be CLOSED.
						pbsr_ptr->sbf.mv_open_for_irrigation = (false);
					}
				}
				
				if( pbsr_ptr->transition_timer_all_pump_valves_are_OFF > 0 )
				{
					pbsr_ptr->transition_timer_all_pump_valves_are_OFF -= 1;
		
					if( pbsr_ptr->transition_timer_all_pump_valves_are_OFF == 0 )
					{
						// 6/19/2012 rmd : All the pumps should be OFF.
						pbsr_ptr->sbf.pump_activate_for_irrigation = (false);
					}
				}
			}
			else
			{
				// 2/20/2013 rmd : Keep reloading the blockout period cause it doesn't start until after the
				// MV and PUMP have shut down. And while waiting it will be chipped away at.
				pbsr_ptr->timer_MLB_just_stopped_irrigating_blockout_seconds_remaining = MLB_JUST_STOPPED_IRRIGATING_CHECKING_BLOCKOUT_SECONDS;
			}
			
			// 6/19/2012 rmd : A sanity check and a FAIL-SAFE check.
			if( (pbsr_ptr->transition_timer_all_stations_are_OFF == 0) && (pbsr_ptr->sbf.mv_open_for_irrigation == (true)) )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_IRRI: unexp condition" );
	
				// 6/19/2012 rmd : correct the condition
				pbsr_ptr->sbf.mv_open_for_irrigation = (false);
			}
	
			// 6/19/2012 rmd : A sanity check and a FAIL-SAFE check.
			if( (pbsr_ptr->transition_timer_all_pump_valves_are_OFF == 0) && (pbsr_ptr->sbf.pump_activate_for_irrigation == (true)) )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_IRRI: unexp condition" );
	
				// 6/19/2012 rmd : correct the condition
				pbsr_ptr->sbf.pump_activate_for_irrigation = (false);
			}
			
		}  // of if there are valves ON or not
		
		// 8/14/2012 rmd : FAIL-SAFE - whatever the outcome of all that above if the pump output is
		// activated the master valves had better be open!
		if( pbsr_ptr->sbf.pump_activate_for_irrigation == (true) )
		{
			if( pbsr_ptr->sbf.mv_open_for_irrigation == (false) )
			{
				ALERT_MESSAGE_WITH_FILE_NAME( "FOAL_IRRI: unexp pump setting!" );
				
				pbsr_ptr->sbf.pump_activate_for_irrigation = (false);
			}
		}
		
	}  // of if the system is in use
}

static void _load_and_maintain_system_anti_chatter_timers( void )
{
	UNS_32	s;
	
	BY_SYSTEM_RECORD	*bsr_ptr;
	
	bsr_ptr = &(system_preserves.system[ 0 ]);
	
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	for( s=0; s<MAX_POSSIBLE_SYSTEMS; s++ )
	{
		_nm_anti_chatter_maintenance_sub_function( bsr_ptr );
		
		// -----------------------------
		
		bsr_ptr += 1;		

		// -----------------------------
	}

	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
}

/* ---------------------------------------------------------- */

// 5/30/2018 rmd : Introduced to limit walk_thru valves to one ON at a time within the
// Network. Must be a global to have access across several of the maintain functions. May
// incorporate into the foal_irri structure, but whats the diff ehh?
BOOL_32		one_ON_for_walk_thru;
	
/* ---------------------------------------------------------- */
void irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations( const DATE_TIME *const pdate_time )
{
	// 3/31/2018 rmd : This is executed within the context of the TDCHECK task, and only for the
	// REAL irrigation maintenance. NOT USED FOR FTIMES.
	
	// ----------
	
	IRRIGATION_LIST_COMPONENT *ilc;
	
	DATE_TIME	stop_dt;

	BOOL_32		may_remove;
	
	UNS_32		removal_reason;

	// ----------
	
	// 9/27/2012 rmd : In the following pass through the irrigation list we may remove one or
	// more stations for the reasons of hitting the stop time. Or rain. We only want to see one
	// alert line about such.
	BOOL_32	alert_already_made_for_stop_time;
	BOOL_32	alert_already_made_for_rain;
	BOOL_32	alert_already_made_for_rain_switch;
	BOOL_32	alert_already_made_for_freeze_switch;

	// ----------
	
	alert_already_made_for_stop_time = (false);
	alert_already_made_for_rain = (false);
	alert_already_made_for_rain_switch = (false);
	alert_already_made_for_freeze_switch = (false);
	
	// --------------------------
	
	ilc = nm_ListGetFirst( &(foal_irri.list_of_foal_all_irrigation) );
	
	// Actually the only reason we run the following loop when there are stations on the xfer list
	// is to keep the decrement of soak seconds moving down so that we don't lengthen the soak times
	// by some unexpected and indeterminent amount.
	while( ilc != NULL )
	{
		// THIS MUST LOOP THROUGH ALL STATIONS IN THE LIST. Certain settings - such as Pump Outputs
		// Should Be On and There Is A Mix Of Pump And Non Pump Valves On depend upon a complete
		// look at all station in the list.
		
		// ----------------------

		// 9/25/2012 rmd : As we encounter removal situations, such as hitting the stop time, this
		// flag is set (false) so that we do not include the station in the list state details
		// gathered.
		BOOL_32	ilc_still_in_the_list_and_need_to_get_the_next_ilc;
		
		ilc_still_in_the_list_and_need_to_get_the_next_ilc = (true);
		
		// ----------------

		// 4/18/2014 rmd : Sanity check. Just to make sure. We are going to use it as an array index
		// and if out of range would be a difficult problem to determine why we crashed!
		RangeCheck_uns8( &(ilc->box_index_0), BOX_INDEX_MINIMUM, BOX_INDEX_MAXIMUM, BOX_INDEX_DEFAULT, NULL, "Box Index" );

		// ----------------

		// 10/5/2012 rmd : Only honor the stop time and date for the case of programmed irrigation.
		if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
		{
			stop_dt.D = ilc->stop_datetime_d;
			stop_dt.T = ilc->stop_datetime_t;
			
			if( DT1_IsBiggerThanOrEqualTo_DT2( pdate_time, &stop_dt ) == (true) )
			{
				// ----------------
				
				if( alert_already_made_for_stop_time == (false) )
				{
					alert_already_made_for_stop_time = (true);
					
					Alert_hit_stop_time();
				}

				// ----------------

				// 9/26/2012 rmd : Execute a turn OFF. This can change the list order (and likely does) and
				// therefore returns the next list item so we can proceed properly through the list.
				ilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), ilc, ACTION_REASON_TURN_OFF_AND_REMOVE_hit_stop_time );
				
				// 6/7/2012 rmd : NOTE - Now this ilc is on the ACTION_NEEDED list but not on the main
				// irrigation list. Until the token is made. The test for if an ilc is available for re-use
				// includes if we are on the action_needed list. So this is a safe condition.

				ilc_still_in_the_list_and_need_to_get_the_next_ilc = (false);
			}
		}
		
		// ----------------

		if( ilc->bbf.directions_honor_RAIN_TABLE )
		{
			// 6/16/2015 rmd : Has either the accumulated rain so far crossed the minimum or has the
			// commserver sent to us a rain shutdown command? If so terminate this irrigation. For both
			// reads of the weather_preserves data no MUTEX is needed. They are atomic operations.
			if( weather_preserves.rain.inhibit_irrigation_have_crossed_the_minimum || (weather_preserves.rain.rain_shutdown_rcvd_from_commserver_uns32 != 0) )
			{
				if( !alert_already_made_for_rain )
				{
					alert_already_made_for_rain = (true);
					
					Alert_rain_affecting_irrigation();
				}
				
				// ----------
				
				// 9/26/2012 rmd : Execute a turn OFF. This can change the list order (and likely does) and
				// therefore returns the next list item so we can proceed properly through the list.
				ilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), ilc, ACTION_REASON_TURN_OFF_AND_REMOVE_rain_crossed_minimum_or_poll );
				
				ilc_still_in_the_list_and_need_to_get_the_next_ilc = (false);
			}
		}

		// ----------------

		if( ilc->bbf.directions_honor_RAIN_SWITCH )
		{
			if( weather_preserves.rain_switch_active == (true) )
			{
				if( !alert_already_made_for_rain_switch )
				{
					alert_already_made_for_rain_switch = (true);

					Alert_rain_switch_affecting_irrigation();
				}

				// ----------------

				// 9/26/2012 rmd : Execute a turn OFF. This can change the list order (and likely does) and
				// therefore returns the next list item so we can proceed properly through the list.
				ilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), ilc, ACTION_REASON_TURN_OFF_AND_REMOVE_rain_switch_active );

				ilc_still_in_the_list_and_need_to_get_the_next_ilc = (false);
			}
		}
		
		// ----------------

		if( ilc->bbf.directions_honor_FREEZE_SWITCH )
		{
			if( weather_preserves.freeze_switch_active == (true) )
			{
				if( !alert_already_made_for_freeze_switch )
				{
					alert_already_made_for_freeze_switch = (true);

					Alert_freeze_switch_affecting_irrigation();
				}

				// ----------------

				// 9/26/2012 rmd : Execute a turn OFF. This can change the list order (and likely does) and
				// therefore returns the next list item so we can proceed properly through the list.
				ilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), ilc, ACTION_REASON_TURN_OFF_AND_REMOVE_freeze_switch_active );

				ilc_still_in_the_list_and_need_to_get_the_next_ilc = (false);
			}
		}

		// ----------------

		// 9/26/2012 rmd : If the station has been removed from the irrigation list not only is
		// there no sense in building up the stats on that list entry but it may be down right
		// dangerous. As technically the item is no longer in the list. Now in reality this list is
		// made up of element of a permantely allocated array so there isn't the danger one would
		// normally expect with dynamically allocated memory.
		if( ilc_still_in_the_list_and_need_to_get_the_next_ilc == (true) )
		{
			// 2011.12.12 rmd : It is assumed that the bsr pointer is valid. It is set when the ilc is
			// added to the irrigation list. If we experience ANY difficulty we could check here that at
			// least it points to a system record.
			
			// ----------
			
			// 5/10/2018 rmd : The preceeding activity in this ilc loop could have removed a station.
			// Well at this point we know the station is still intact, so update the transient control
			// variables using this station. Internal to the update function we test if this station is
			// using a moisture sensor, but ONLY need to do this for programmed irrigation.
			if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
			{
				MOISTURE_SENSORS_update__transient__control_variables_for_this_station( ilc );
			}
			
			// ----------
			
			// 2011.11.09 rmd : Perhaps should only be counted if they are not being removed from the
			// list cause they are done.
			ilc->bsr->ufim_valves_in_the_list_for_this_system += 1;
			
			// 4/16/2012 rmd : Find the highest reason in the list. This is currently a variable of
			// convience for the STOP key processing. So we know what we should be stopping.
			if( (ilc->bbf.w_reason_in_list) > (ilc->bsr->highest_reason_in_list) )
			{
				ilc->bsr->highest_reason_in_list = (ilc->bbf.w_reason_in_list);
			}
			
			// ----------------

			// 5/21/2012 rmd : Capture who is in the list. To detect when irrigation types wrap up.
			if( (ilc->bbf.w_reason_in_list) == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
			{
				ilc->bsr->sbf.ufim_one_or_more_in_list_for_programmed_irrigation = (true);
			}
			else
			if( (ilc->bbf.w_reason_in_list) == IN_LIST_FOR_MANUAL_PROGRAM )
			{
				ilc->bsr->sbf.ufim_one_or_more_in_list_for_manual_program = (true);
			}
			else
			if( (ilc->bbf.w_reason_in_list) == IN_LIST_FOR_MANUAL )
			{
				ilc->bsr->sbf.ufim_one_or_more_in_list_for_manual = (true);
			}
			else
			if( (ilc->bbf.w_reason_in_list) == IN_LIST_FOR_WALK_THRU )
			{
				ilc->bsr->sbf.ufim_one_or_more_in_list_for_walk_thru = (true);
			}
			else
			if( (ilc->bbf.w_reason_in_list) == IN_LIST_FOR_TEST )
			{
				ilc->bsr->sbf.ufim_one_or_more_in_list_for_test = (true);
			}
			else
			if( (ilc->bbf.w_reason_in_list) == IN_LIST_FOR_MOBILE )
			{
				ilc->bsr->sbf.ufim_one_or_more_in_list_for_mobile = (true);
			}
			
			// ----------------

			// Find the highest one in the list - ABLE TO IRRIGATE - whether its ON or not!
			//
			// Because of the ordering of the list (the reason in the list is the highest order sort
			// criteria) we can be sure that the first station we look at is the highest priority reason
			// in the list. But that may not be true since we introduced the "RemainingSoakSeconds == 0"
			// term. We can be sure that once the highest_reason_in_list is set it won't get set again.
			// We can be assured that the soak time is ONLY set when a soaking is intended. If it is
			// unintentionally set not only will this logic fail and we'll have problems with stations
			// not coming on when expected cause of the soak term in the turn on criteria loop.
			if ( ilc->soak_seconds_remaining_ul == 0 )
			{
				// 608 rmd : A comment. Don't be fooled by these statements and the variable names such as
				// "highest_reason_in_list_uc" - what they REALLY mean is "highest reason in list available
				// for turn ON". This distinction is because of the "RemainingSoakSeconds == 0" term.
				if ( ilc->bbf.w_reason_in_list > (ilc->bsr->ufim_highest_reason_in_list_available_to_turn_ON) )
				{
					// Set the "highest_reason_in_list".
					(ilc->bsr->ufim_highest_reason_in_list_available_to_turn_ON) = ilc->bbf.w_reason_in_list;
				}
	
				// 607 ajv : Find the highest reasons in the list for both pump valves and non-pump valves.
				if( (ilc->bbf.w_uses_the_pump == (true)) && (ilc->bbf.w_reason_in_list > (ilc->bsr->ufim_highest_pump_reason_in_list_available_to_turn_ON)) )
				{
					(ilc->bsr->ufim_highest_pump_reason_in_list_available_to_turn_ON) = ilc->bbf.w_reason_in_list;
				}
	
				if( (ilc->bbf.w_uses_the_pump == (false)) && (ilc->bbf.w_reason_in_list > (ilc->bsr->ufim_highest_non_pump_reason_in_list_available_to_turn_ON)) )
				{
					(ilc->bsr->ufim_highest_non_pump_reason_in_list_available_to_turn_ON) = ilc->bbf.w_reason_in_list;
				}
			}
			
			// ----------------

			if( ilc->bbf.station_is_ON )
			{
				// 06.07.2006 ajv - Verify that, since the station status is ON that the station is, in fact,
				// on the ONList.  If not, we need to pull the chain down into forced to destroy and rebuild
				// the lists.
				//
				// 2011.11.02 rmd : This used to be the part about forcing you into forced. A rather
				// dramatic but I suppose effective method to killing all the irrigation and cleaning up
				// this mucho bad error. For now we'll just flag about it. And decide later how to take
				// action if we ever see this error. Which of course we should not.
				if( nm_OnList( &foal_irri.list_of_foal_stations_ON, ilc ) == (false) )
				{
					//ALERT_MESSAGE_WITH_FILE_NAME( "Not on ON list!" ); 
					Alert_item_not_on_list( ilc, foal_irri.list_of_foal_stations_ON );   
				}

				// If its ON we adjust remaining seconds.
				ilc->remaining_seconds_ON -= 1;

				// 2011.11.09 rmd : This is where we actually turn OFF the valves. As opposed how we used to
				// let the irri machines do the thinking for that. In this new CS3000 fully centralized
				// environment the irri machines are mostly just slaves!
				if( ilc->remaining_seconds_ON <= 0 )
				{
					// 11/11/2014 rmd : The CYCLE has ended. Turn OFF the station. But do not remove from the
					// irrigation lists. There may well be remaining run time. The turn OFF function does place
					// the ilc on the action needed list too.
					ilc = nm_nm_FOAL_if_station_is_ON_turn_it_OFF( &(foal_irri.list_of_foal_all_irrigation), ilc, ACTION_REASON_TURN_OFF_cause_cycle_has_completed );
					
					ilc_still_in_the_list_and_need_to_get_the_next_ilc = (false);
				}
				else
				{
					// 6/1/2012 rmd : Maintain if any valves are ON that want to check flow. This is support for
					// a sanity check about is there a mix of flow groups among the valves ON.
					if( ilc->bbf.flow_check_when_possible_based_on_reason_in_list )
					{
						ilc->bsr->ufim_based_on_reason_in_list_at_least_one_valve_is_ON_that_wants_to_check_flow = (true);
					}

					if( station_preserves.sps[ ilc->station_preserves_index ].spbf.flow_check_station_cycles_count < ilc->bsr->flow_check_required_station_cycles )
					{
						ilc->bsr->ufim_the_valves_ON_meet_the_flow_checking_cycles_requirement = (false);
					}

					// ----------
					
					// 8/30/2012 rmd : For TEST and RRE use the number ON limit is the electrical limit for the
					// box. TEST and RRE completely ignore the on_at_a_time limits.
					if( (ilc->bbf.w_reason_in_list != IN_LIST_FOR_TEST) && (ilc->bbf.w_reason_in_list != IN_LIST_FOR_MOBILE) )
					{
						// 5/3/2012 rmd : Update number ON in the SYSTEM. And update the limit of how many ARE
						// allowed ON.
						SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit( ilc->bsr, ilc->GID_on_at_a_time );
						
						// Keep the GROUP on_at_a_time limit up to date.
						ON_AT_A_TIME_bump_this_groups_ON_count( ilc->GID_on_at_a_time );
					}
				
					// ----------

					if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_WALK_THRU )
					{
						// 5/30/2018 rmd : Update our marker. This is a NETWORK level flag, meaning above a system
						// level flag (also called a mainline level - unfortunately we use system and mainline
						// interchangeably).
						one_ON_for_walk_thru = (true);
					}
					
					// ----------
					
					// For ftimes we make sure the weighting_involved_in_flow_problem bit is NOT set.
					//
					// For time reasons (even though it makes the code a bit harder to follow) use this loop
					// for a second reason. Track if one is on from the problem list.
					if( ilc->bbf.w_involved_in_a_flow_problem == (true) )
					{
						// Set this true so that no more from the problem list can come on.
						ilc->bsr->ufim_one_ON_from_the_problem_list_b = (true);
					}
		
					// 5/3/2012 rmd : Keep our pump needed flags up to date.
					if( ilc->bbf.w_uses_the_pump == (true) )
					{
						ilc->bsr->ufim_stations_ON_with_the_pump_b = (true);
					}
					else
					{
						ilc->bsr->ufim_stations_ON_without_the_pump_b = (true);
					}
					
					
					if( ilc->bbf.w_to_set_expected == (true) )
					{
						// 608.rmd for the new RRE mode we get specific otherwise we execute just as it was
						if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE )
						{
							ilc->bsr->ufim_one_RRE_ON_to_set_expected_b = (true);
						}
						else
						{
							// If this guy is set TRUE no others will be allowed to turn on.
							ilc->bsr->ufim_one_ON_to_set_expected_b = (true);

							// 7/4/2015 ajv : If this station is irrigating as part of a group and it's set to acquire
							// an expected, it means that this group was set to acquire an expected. Therefore, let's
							// clear the group's flag now.
							// 
							// This is different from the ET2000e which clears the flag when a start time is crossed.
							// Because that process loops through stations rather than station groups, there's a lot of
							// complexity to determine whether to clear the flag for a particular group or not.
							// Additionally, clearing the flag here sets the stage for clearing the flag by-station
							// rather than by group if we opt to move that direction in the future.
							if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
							{
								ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_for_GID( ilc->GID_on_at_a_time );
							}
						}
					}

				}
	
			}  // of if it is ON
			else
			{
				// STATION IS OFF

				// 2011.12.07 rmd : L06.07.2006 ajv - Verify that, since the station status is OFF that the
				// station is, in fact, no longer on the ONList.  If not, we need to pull the chain down
				// into forced to destroy and rebuild the lists. 2011.11.03 rmd : Just alert about it. No
				// more of this pulling the chain down.
				if( nm_OnList( &foal_irri.list_of_foal_stations_ON, ilc ) == (true) )
				{
					ALERT_MESSAGE_WITH_FILE_NAME( "Is OFF but on the ON list!" );
				}
				
				// DECREMENT THE SOAK TIME here and look at it in the test for turn on loop
				if ( ilc->soak_seconds_remaining_ul > 0 )
				{
					ilc->soak_seconds_remaining_ul -= 1;
				}

				// ----------
				
				// 6/5/2012 rmd : LIST REMOVAL HERE - If the station is OFF and the requested seconds has
				// been driven to zero it is done, OR, if there still is remaining time but according to the
				// moisture sensor criteria we can remove it early, then remove it.
				may_remove = (false);
				
				if( ilc->requested_irrigation_seconds_ul == 0 )
				{
					may_remove = (true);
					
					removal_reason = ACTION_REASON_TURN_OFF_AND_REMOVE_irrigation_has_completed;
						
					//Alert_Message_va( "TIME: Removing station %d", (ilc->station_number_0_u8 + 1) );
				}
				else
				if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
				{
					if( MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_the_list( ilc ) )
					{
						may_remove = (true);
						
						removal_reason = ACTION_REASON_TURN_OFF_AND_REMOVE_moisture_reading_hit_set_point;

						//Alert_Message_va( "MOISTURE: Removing station %d", (ilc->station_number_0_u8 + 1) );
					}
				}
				
				if( may_remove )
				{
					// 6/5/2012 rmd : See if we were supposed to flow check but never did during all of its
					// cycles. This only makes an alert when in the list for the reason of PROGRAMMED
					// IRRIGATION. And we should ONLY make this check when the irrigation ends by running out of
					// time. As is the case here. So this here should be the ONLY place this is called!
					_if_supposed_to_but_did_not_ever_check_flow_make_an_alert( ilc );
					
					// 11/5/2014 rmd : We know it is not ON but this routine function call performs the steps we
					// need to do. That is to put the ilc on the action_needed list, to get to the next ilc in
					// the list, and to remove the ilc from the list.
					ilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), ilc, removal_reason );
					
					ilc_still_in_the_list_and_need_to_get_the_next_ilc = (false);
				}
				else
				{
					// 6/7/2012 rmd : Do not keep up list statistics on a station no longer in the list. That
					// just doesn't seem right now does it!
						
					// Need to track the on status of those that want to set expected for the turn on loop
					// that follows.
					if( ilc->bbf.w_to_set_expected == (true) )
					{
						// 608.rmd for the new RRE mode we get specific otherwise we execute just as it was
						if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE )
						{
							ilc->bsr->ufim_list_contains_some_RRE_to_setex_that_are_not_ON_b = (true);
						}
						else
						{
							ilc->bsr->ufim_list_contains_some_to_setex_that_are_not_ON_b = (true);
							
							if( ilc->bbf.w_uses_the_pump == (true) )
							{
								ilc->bsr->ufim_list_contains_some_pump_to_setex_that_are_not_ON_b = (true);
							}
							else
							{
								ilc->bsr->ufim_list_contains_some_non_pump_to_setex_that_are_not_ON_b = (true);
							}
						}
		
						// Now capture the highest reason for all the valves in the list to set expecteds - we need
						// this when a valve of a higher reason joins the list.
						if( ilc->bbf.w_reason_in_list > ilc->bsr->ufim_highest_reason_of_OFF_valve_to_set_expected )
						{
							ilc->bsr->ufim_highest_reason_of_OFF_valve_to_set_expected = ilc->bbf.w_reason_in_list;
						}
					}
					
									
					if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
					{
						// Looking at stations that are OFF. If they are not soaking and have regular time left to
						// irrigate they are waiting.
						if( (ilc->soak_seconds_remaining_ul == 0) && (ilc->requested_irrigation_seconds_ul > 0) )
						{
							// 5/19/2015 ajv : Find the highest priorities for both pump and non-pump valves. Note the
							// '>' symbol... This is because PRIORITY_LOW is 1 while PRIORITY_HIGH is 3.
							if( (ilc->bbf.station_priority > ilc->bsr->ufim_highest_priority_pump_waiting) && (ilc->bbf.w_uses_the_pump == (true)) )
							{
								ilc->bsr->ufim_highest_priority_pump_waiting = ilc->bbf.station_priority;
							}
		
							if ( (ilc->bbf.station_priority > ilc->bsr->ufim_highest_priority_non_pump_waiting) && (ilc->bbf.w_uses_the_pump == (false)) )
							{
								ilc->bsr->ufim_highest_priority_non_pump_waiting = ilc->bbf.station_priority;
							}
						}
					}
					
					// ----------
					
					/*
					// 4/1/2016 rmd : NOTE - this ufim_list_contains_waiting_programmed_irrigation_b FLAG is no
					// longer used in the turn ON logic. But I've left the setting of the flag code here,
					// commented out, for reference.
					//
					// The purpose of list_contains_waiting_programmed_irrigation is to stop stations from
					// coming on to use their HO (when thats all that is left for that station) when there are
					// stations in the list that have and are ready to use MANUAL_PROGRAM or PROGRAMMED
					// IRRIGATION.
					if( (ilc->bbf.w_reason_in_list == IN_LIST_FOR_MANUAL_PROGRAM) || (ilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION) )
					{
						// Looking at stations that are OFF. If they are not soaking and have regular time left to
						// irrigate they are waiting.
						if( (ilc->soak_seconds_remaining_ul == 0) && (ilc->requested_irrigation_seconds_ul > 0) )
						{
							ilc->bsr->ufim_list_contains_waiting_programmed_irrigation_b = (true);
						}
					}
					*/
					
					// ----------
					
					// 607 ajv : Examine each ilc to see whether there are any pump valves and non-pump valves
					// waiting. This will be used to determine whether to continue turning on what we have been,
					// or whether we need to switch from pump to non-pump or vice versa. Looking at stations
					// that are OFF. If they are not soaking and have regular time left to irrigate they are
					// waiting.
					if( (ilc->soak_seconds_remaining_ul == 0) && (ilc->requested_irrigation_seconds_ul > 0) )
					{
						if( ilc->bbf.w_uses_the_pump == (true) )
						{
							ilc->bsr->ufim_list_contains_waiting_pump_valves_b = (true);
						}
						else
						{
							ilc->bsr->ufim_list_contains_waiting_non_pump_valves_b = (true);
						}
					}
		
				}  // of if we didn't remove the station from the list ie it wasn't done
				
			}  // if entry is OFF
			
		}  // of if station is still in the list
		
		// ----------

		// 5/10/2012 rmd : If we turned OFF a valve or removed the ilc from the list the next list
		// item has already been obtained.
		if( ilc_still_in_the_list_and_need_to_get_the_next_ilc == (true) )
		{
			ilc = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), ilc );
		}

	}  // of all stations
}

/* ---------------------------------------------------------- */
void irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars( void )
{
	UNS_32	i;
	
	// ----------
	
	// Now that we have established some statistics for each system, loop through ALL systems in
	// use and jigger certain items based on some of those settings.
	for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
	{
		if( system_preserves.system[ i ].system_gid != 0 )
		{
			// --------------------
	
			// SANITY checks here.
			if( system_preserves.system[ i ].system_master_number_of_valves_ON == 0 )
			{
				if( system_preserves.system[ i ].ufim_expected_flow_rate_for_those_ON != 0 )
				{
					Alert_Message( "FOAL_IRRI: expected flow rate should be zero." );
	
					system_preserves.system[ i ].ufim_expected_flow_rate_for_those_ON = 0;  // fix it up --- why not
				}
	
				if( system_preserves.system[ i ].ufim_number_ON_during_test != 0 )
				{
					Alert_Message( "FOAL_IRRI: number ON during TEST should be zero." );
	
					system_preserves.system[ i ].ufim_number_ON_during_test = 0;  // fix it up --- why not
				}
			}
	
			// --------------------
	
			// 609.a ajv : When we have stations waiting to acquire expected flow rates, we should
			// ignore priorities.
			if( system_preserves.system[ i ].ufim_list_contains_some_to_setex_that_are_not_ON_b == (true) )
			{
				system_preserves.system[ i ].ufim_highest_priority_non_pump_waiting = PRIORITY_LOW;
		
				system_preserves.system[ i ].ufim_highest_priority_pump_waiting = PRIORITY_LOW;
			}
			
			// --------------------
	
			// 607 ajv : Now that we have a number of statistics related to pump and non-pump activity
			// and items in the list, we need to determine whether we should continue turning on what we
			// were turning on last pass or whether we need to switch from pump to non-pump or vice
			// versa.
			if( system_preserves.system[ i ].ufim_valves_in_the_list_for_this_system == 0 )
			{
				system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
			}
			else
			{	// the list is not empty
		
				// 5/19/2015 ajv : Check the priority levels for pump and non-pump valves first. Note the
				// '>' symbol... This is because PRIORITY_LOW is 1 while PRIORITY_HIGH is 3.
				if( (system_preserves.system[ i ].ufim_highest_priority_pump_waiting > system_preserves.system[ i ].ufim_highest_priority_non_pump_waiting) &&
					(system_preserves.system[ i ].ufim_highest_reason_in_list_available_to_turn_ON == IN_LIST_FOR_PROGRAMMED_IRRIGATION) )
				{
					system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
				}
				else if( (system_preserves.system[ i ].ufim_highest_priority_non_pump_waiting > system_preserves.system[ i ].ufim_highest_priority_pump_waiting) &&
						 (system_preserves.system[ i ].ufim_highest_reason_in_list_available_to_turn_ON == IN_LIST_FOR_PROGRAMMED_IRRIGATION) )
				{
					system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
				}
				else	// priorities are all equal OR there is a higher reason in the list than PROGRAMMED IRRIGATION
				{	
					if ( system_preserves.system[ i ].ufim_stations_ON_with_the_pump_b == (true) )
					{
						system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
					}
					else
					if ( system_preserves.system[ i ].ufim_stations_ON_without_the_pump_b == (true) )
					{
						system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
					}
					else
					{	// nothing is ON
		
						// We only really care about the highest reason in the list if
						// it is higher than Manual Programs and Programmed Irrigation
						if ( system_preserves.system[ i ].ufim_highest_reason_in_list_available_to_turn_ON > IN_LIST_FOR_MANUAL_PROGRAM )
						{
							if ( system_preserves.system[ i ].ufim_highest_pump_reason_in_list_available_to_turn_ON > system_preserves.system[ i ].ufim_highest_non_pump_reason_in_list_available_to_turn_ON ) {
							
								system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
		
							} else if ( system_preserves.system[ i ].ufim_highest_pump_reason_in_list_available_to_turn_ON < system_preserves.system[ i ].ufim_highest_non_pump_reason_in_list_available_to_turn_ON ) {
		
								system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
		
							} else {	// the highest reasons are equal
		
								// Make no change - keep WhatAreWeTurning on to whatever
								// we are currently turning on - it will switch whenever
								// the highest priority of this type has completed
		
							}
						
						}
						else
						{	// there is nothing higher than Manual Programs & Programmed Irrigation
		
							// 609.a ajv
							// What if we are acquiring expecteds? A potential hole exists where a pump station
							// can acquire an expected and then will never switch to non-pump to allow the non-
							// pump station to acquire its expected.
							//
							if ( system_preserves.system[ i ].ufim_list_contains_some_to_setex_that_are_not_ON_b == (true) ) {
								
								if ( ( system_preserves.system[ i ].ufim_what_are_we_turning_on_b == TURNING_ON_PUMP_VALVES ) &&
									 ( system_preserves.system[ i ].ufim_list_contains_some_pump_to_setex_that_are_not_ON_b == (true) ) ) {
									
									system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
									
								} else if ( ( system_preserves.system[ i ].ufim_what_are_we_turning_on_b == TURNING_ON_NON_PUMP_VALVES ) &&
									 ( system_preserves.system[ i ].ufim_list_contains_some_non_pump_to_setex_that_are_not_ON_b == (true) ) ) {
									
									system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
									
								} else {	// we need to switch from pump to non-pump or vice versa
		
									if ( system_preserves.system[ i ].ufim_what_are_we_turning_on_b == TURNING_ON_PUMP_VALVES ) {
		
										system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
		
									} else {
		
										system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
		
									}
		
								}
								
							} else {
								
								if ( ( system_preserves.system[ i ].ufim_what_are_we_turning_on_b == TURNING_ON_PUMP_VALVES ) &&
									 ( system_preserves.system[ i ].ufim_list_contains_waiting_pump_valves_b == (true) ) ) {
			
									system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
			
								} else if ( ( system_preserves.system[ i ].ufim_what_are_we_turning_on_b == TURNING_ON_NON_PUMP_VALVES ) &&
											( system_preserves.system[ i ].ufim_list_contains_waiting_non_pump_valves_b == (true) ) ) {
			
									system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
			
								} else {	// we need to switch from pump to non-pump or vice versa
			
									if ( system_preserves.system[ i ].ufim_what_are_we_turning_on_b == TURNING_ON_PUMP_VALVES ) {
			
										system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_NON_PUMP_VALVES;
			
									} else {
			
										system_preserves.system[ i ].ufim_what_are_we_turning_on_b = TURNING_ON_PUMP_VALVES;
			
									}
			
								}
								
							}
		
						}
		
					}
		
				}
		
			}
			
			// --------------------
	
			// SANITY test on one aspect of the statistics gathered. Cannot have valves ON from multiple
			// flow groups at once except during test - during which we allow it.
			if( (system_preserves.system[ i ].ufim_based_on_reason_in_list_at_least_one_valve_is_ON_that_wants_to_check_flow) && (system_preserves.system[ i ].ufim_number_ON_during_test <= 1) )
			{
				if( FOAL_IRRI_there_is_more_than_one_flow_group_ON( &system_preserves.system[ i ].ufim_flow_check_group_count_of_ON[ 0 ] ) )
				{
					Alert_Message( "FOAL_IRRI: mix of flow groups ON." );
				}
			}
			
			// --------------------
	
			// 2000e : Set this global in support of flow checking. We shouldn't check flow when there
			// are pumper valves on with non-pumper valves also on. The way this is coded today this can
			// ONLY happen with TEST and DTMF.
			//
			// 2000e : And now we have modified the code so that we never test flow for DTMF and we only
			// test flow when doing TEST if one valve is on - when more than one we do not test flow -
			// so this term is esentially useless but keep it around now that it is here.
			if( (system_preserves.system[ i ].ufim_stations_ON_without_the_pump_b == (true)) && (system_preserves.system[ i ].ufim_stations_ON_with_the_pump_b == (true)) )
			{
				system_preserves.system[ i ].ufim_there_is_a_PUMP_mix_condition_b = (true);
			}
			else
			{
				system_preserves.system[ i ].ufim_there_is_a_PUMP_mix_condition_b = (false);
			}
			
			// --------------------
	
			if( system_preserves.system[ i ].sbf.one_or_more_in_list_for_programmed_irrigation && (!system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_programmed_irrigation) )
			{
				Alert_irrigation_ended( IN_LIST_FOR_PROGRAMMED_IRRIGATION );
			}
	
			if( system_preserves.system[ i ].sbf.one_or_more_in_list_for_manual_program && (!system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_manual_program) )
			{
				Alert_irrigation_ended( IN_LIST_FOR_MANUAL_PROGRAM );
			}
	
			if( system_preserves.system[ i ].sbf.one_or_more_in_list_for_manual && (!system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_manual) )
			{
				Alert_irrigation_ended( IN_LIST_FOR_MANUAL );
			}
	
			if( system_preserves.system[ i ].sbf.one_or_more_in_list_for_walk_thru && (!system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_walk_thru) )
			{
				Alert_irrigation_ended( IN_LIST_FOR_WALK_THRU );
			}
	
			if( system_preserves.system[ i ].sbf.one_or_more_in_list_for_test && (!system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_test) )
			{
				Alert_irrigation_ended( IN_LIST_FOR_TEST );
			}
	
			if( system_preserves.system[ i ].sbf.one_or_more_in_list_for_rre && (!system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_mobile) )
			{
				// Suppress this alert for DTMF as we have a DTMF activated and DTMF finished alert and this
				// one would show up as the user switched stations...you know turned one station off and
				// then another one ON.
				//Alert_IrrigationEnded( IN_LIST_FOR_MOBILE );
			}
			
			// 5/21/2012 rmd : In any case set all the trackers equal to the latest indication of what's
			// in the list.
			system_preserves.system[ i ].sbf.one_or_more_in_list_for_programmed_irrigation = system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_programmed_irrigation;
			system_preserves.system[ i ].sbf.one_or_more_in_list_for_manual_program = system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_manual_program;
			system_preserves.system[ i ].sbf.one_or_more_in_list_for_manual = system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_manual;
			system_preserves.system[ i ].sbf.one_or_more_in_list_for_walk_thru = system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_walk_thru;
			system_preserves.system[ i ].sbf.one_or_more_in_list_for_test = system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_test;
			system_preserves.system[ i ].sbf.one_or_more_in_list_for_rre = system_preserves.system[ i ].sbf.ufim_one_or_more_in_list_for_mobile;
			
			// --------------------
	
		}  // of if the system is in use
	
	}  // of each system loop
}

/* ---------------------------------------------------------- */
void irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON( void )
{
	IRRIGATION_LIST_COMPONENT *ilc;
	
	// ----------
	
	// With the information about what is in the list trying to turn on and what is actually on we may
	// need to turn some stations off. We do that with our turn off rules which are fairly straight
	// forward at this point. 
	//
	// Our turn off to make room for higher priority kinds of irrigation are fairly straight forward -
	// if a higher priority item is in the list all stations below him in priority (reason in the list)
	// must turn off and remain (be) off. We do however want to allow lower priority stuff to come on
	// if a station isn't ready to come on (soaking). So when a station is soaking it isn't "SEEN" in
	// the list for the turn off to make room purpose.
	ilc = nm_ListGetFirst( &(foal_irri.list_of_foal_all_irrigation) );
	
	while( ilc != NULL )
	{
		BOOL_32	need_to_get_the_next_ilc;
		
		need_to_get_the_next_ilc = (true);
	
		// ----------
	
		// 4/1/2016 rmd : If the station is ON, run some tests to see if he should be paused.
		if( ilc->bbf.station_is_ON )
		{
			BOOL_32	pause_it;
			
			UNS_32	pause_reason;

			pause_it = (false);
			
			// 7/17/2015 rmd : Use the turn ON function reason in list priority scheme. If stations are
			// ON for Programmed Irrigation already and a Manual Program starts it's probably okay NOT
			// to immediately turn off the Programmed Irrigation stations and start the Manual Program
			// stations. As any Programmed Irrigation stations end the Manual Program stations will
			// start turning ON because they are in the list first and the maintain function (this
			// function) will encounter them first when looking for stations to turn ON.
			if( FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list( ilc->bbf.w_reason_in_list, ilc->bsr->ufim_highest_reason_in_list_available_to_turn_ON ) == (true) )
			{
				// 608 rmd : AJ & I quickly decided it makes sense to leave a station ON when it is learning
				// its expected and not to pause it to let another of a higher priority come on. It's really
				// a decision to protect the learning process at the expense of a higher priority valve
				// having to wait. I believe the conditions for this would be rare. For example a programmed
				// irrigation to set expected is running (ONE valve at a time right) and a valve is ON when
				// a guy pushes TEST. We won't pause the programmed irrigation valve till the set expected
				// has learned. Now you could argue this is really not what the user would expect so this is
				// wrong. And I could agree with that. Another case would be if the user is using TEST to
				// set the expected and a RRE valve is added to the list - the RRE valve will not come ON
				// till the test valve is done with its set expected activity - BUT in this case the
				// RRE-TRAN will indicate to the user that there is a TEST valve running (or will it?). I'm
				// mixed as to the correct behaviour - so don't do it.
				pause_reason = ACTION_REASON_TURN_OFF_pause_for_a_higher_reason_in_the_list;
				
				pause_it = (true);
			}
			else
			if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE )
			{
				// 608.rmd : Well it didn't get caught above as being ON with a reason less than the highest
				// so check for this condition - it is in the list for RRE and is on and there is at least
				// one other RRE valve in the list to turn on to help set expected and this one ain't it ...
				// so pause him.
				// 
				// The RRE has a different behaviour than the other stations with their set expected bit set
				// ... for the RRE we want to pause all other valves ON then turn ON the first RRE set
				// expected valve we encounter. The other set expected reasons in the list pretty much wait
				// until nothing is ON before they come on - but they do block other new ones from coming
				// on.
				if ( ilc->bsr->ufim_list_contains_some_RRE_to_setex_that_are_not_ON_b == (true) )
				{
					if( ilc->bbf.w_to_set_expected == (false) )
					{
						// 608.rmd : Then this is an RRE valve in the list who is ON while there are other RRE
						// valves in the list waiting to come on to help set expected so pause this guy. I'll
						// consider this for a higher reason in the list. No harm there cause there are no flags set
						// for this reason.
						pause_reason = ACTION_REASON_TURN_OFF_pause_for_a_higher_reason_in_the_list;
						
						pause_it = (true);
					}
				}
			}
			else
			if( ilc->bbf.directions_honor_WIND_PAUSE && foal_irri.wind_paused )
			{
				// 11/13/2014 rmd : If the station is set to HONOR WIND PAUSE then it is in the list for a
				// reason that is supposed to respond to wind. This was decided when the ilc was added to
				// the list. So pause it!
				pause_reason = ACTION_REASON_TURN_OFF_pause_wind;
				
				pause_it = (true);
			}
			
			// ----------
			
			if( pause_it )
			{
				// 5/10/2012 rmd : Execute a turn OFF.
				ilc = nm_nm_FOAL_if_station_is_ON_turn_it_OFF( &(foal_irri.list_of_foal_all_irrigation), ilc, pause_reason );
				
				need_to_get_the_next_ilc = (false);
			}
			
		}  // of if the station is ON
		
		// ------------------------
	
		// 6/7/2012 rmd : If we turned OFF a valve the next list item has already been obtained.
		if( need_to_get_the_next_ilc )
		{
			ilc = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), ilc );
		}

	}  // of all stations in the irrigation list
}

/* ---------------------------------------------------------- */
void irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON( void )
{
	IRRIGATION_LIST_COMPONENT	*ilc;

	IRRIGATION_LIST_COMPONENT	*tmp_ilc;
	
	BOOL_32	need_to_get_the_next_ilc;

	char	str_8[ 8 ];
	
	// ----------

	// 5/17/2018 rmd : Okay, we've developed the "transient" variables for each moisture sensor,
	// actually they were developed in the first ilc loop. Now that all the list removal
	// activity has stablized, for each moisture sensor see if it is appropriate to take a
	// reading, and check if the set point has been crossed, and if not bump the required
	// cycles.
	MOISTURE_SENSORS_check_each_sensor_to_possibly_update__saved__control_variables();

	// ----------

	// 10/9/2018 ajv : Also, take this opportunity to generate an alert if/when a soil moisture, 
	// temperature, or conductivity threshold has been crossed. 
	MOISTURE_SENSORS_check_each_sensor_for_a_crossed_threshold(); 
	
	// ----------
	
	// ACTUAL HUNT FOR STATIONS TO TURN ON
	//	
	// If any stations on the xfer list let that empty first so system is settled. This covers stations that have
	// been marked to turn on as well as those marked to turn off. Remember it isn't actually considered on till
	// the token to the controller has been mailed. Turn off go a step further in that they require the response
	// to the token to turn off. If we remove the test for the xferlist.count to be empty then we must perform a
	// loop on the each of the stations in the foal_irri.StationsONByController[ i ] to learn the following:
	// est_total_flow, number_currently_on, one_on_from_the_problem_list, pump_should_be_on, 
	// StationsON_Pump, StationsON_NoPump, and one_on_to_set_expected.
	// We also have to add a statement such as "if ( FOAL_This_ilc_IsOnTheXferList( ilc ) == TRUE ) continue;"
	// to the tests in the decision to turn on. This is the way it is in 507_f.
	//
	// Now that we've decided to implement the "TURN OFF TO MAKE ROOM FOR" (turn off all lower priority stations)
	// rules we can say that we want the turn offs complete before we look to turn anything on. Turn offs decisions
	// made at the foal machine will show on the foalirri.xferlist so we check its count to know that things have
	// settled.

	// -------------------
	
	need_to_get_the_next_ilc = (false);

	// -------------------
	
	ilc = nm_ListGetFirst( &(foal_irri.list_of_foal_all_irrigation) );

	// -------------------
	
	while( (true) )
	{
		// 6/7/2012 rmd : If we remove any stations from the list we utilize this declaration.
		IRRIGATION_LIST_COMPONENT	*ilc_to_remove;

		// ----------------------

		// 6/7/2012 rmd : If we turned OFF a valve or removed the ilc from the list the next list
		// item has already been obtained.
		if( need_to_get_the_next_ilc == (true) )
		{
			ilc = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), ilc );
		}

		need_to_get_the_next_ilc = (true);
		
		// 6/7/2012 rmd : This is how the logic works when we finally reach the end of the list.
		if( ilc == NULL ) break;

		// ---------------------------

		// 2011.11.08 rmd : Obviously if it's already ON skip this station.
		if( ilc->bbf.station_is_ON == (true) ) continue;
		
		// 2011.11.08 rmd : If it's soaking skip this station.
		if( ilc->soak_seconds_remaining_ul > 0 ) continue;
		
		// ----------
		
		// 2/20/2013 rmd : For the cases of TEST and RADIO REMOTE override the slow closing valve
		// turn ON delay. It will generate confusion if the user does not see his TEST or RRE valve
		// come ON right away. The slow-closing valve timer could be set if the TEST say interrupted
		// a scheduled irrigation.
		if( !((ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE) || (ilc->bbf.w_reason_in_list == IN_LIST_FOR_TEST)) )
		{
			// 2011.12.12 rmd : If the inhibit timer for the system this valve belongs to is still
			// counting down then discontinue the attempt to turn another ON.
			if( ilc->bsr->inhibit_next_turn_ON_remaining_seconds > 0 ) continue;
		}

		// If the station is in the list for RADIO REMOTE and it is paused PAUSED ... we are going
		// to skip over it and look for another to turn ON.
		if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE )
		{
			if( ilc->bbf.rre_station_is_paused == (true) ) continue;
		}
		
		// The next check primarily segregates the reason stations are ON for and prevents stations
		// in the list for a lessor reason from coming on till all stations for the higher reason are
		// complete. A fall out of this is that a station cannot be ON for 2 reasons at once (you could
		// put a station in the list for TEST and also for MANUAL and it could turn on that station for
		// both reasons - but this test forces the TEST to finish before the MANUAL starts). 
		//
		// 7/16/2015 rmd : In the 2000e we consider Manual Program and Programmed Irrigation to be
		// the same priority so that their irrigation could meld if they were both in the list at
		// the same time. However I think that only makes sense if the Manual program stations soak.
		// But they do not soak. SO that means they will all irrigate until no more Manual Program
		// stations left in the list. It might be that pump and non-pump would change that if we
		// allowed Manual Program and Program Irrigation to be considered equal reasons in the list.
		// But I think the user would expect ALL the Manual Program stations to complete before the
		// Programmed Irrigation began working itself off. Besides this is a strange situation where
		// manual programs overlaps programmed irrigation. AHHHH that has got me thinking about
		// things like capacity and on-at-a-time rules. If indeed the Manual Program stations
		// generally went first as we got down to the end if there were pending Programmed
		// Irrigation stations in the list they could join in if there were capacity room. So why
		// not leave this the way it is. That is Manual Programs and Programmed Irrigation are
		// considered to be of the same IN_THE_LIST reason when it comes to turning ON the next
		// valve. (Not when it comes to inserting the next valve into the list however.)
		if( FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list( ilc->bbf.w_reason_in_list, ilc->bsr->ufim_highest_reason_in_list_available_to_turn_ON ) == (true) )
		{
			// If in the list for DTMF or TEST remove it with an advisory for the user so he
			// understands. Of course there isn't anything higher than RRE right now but add the
			// statement for completeness sakes.
			//
			// 2011.11.08 rmd : It appears if someone in the system is using the radio remote we will
			// reject a request to perform a test. Even more so I think it goes to say if a user is
			// running a test and an RRE request to turn ON a station is made, the TEST will stop, AND
			// the valve will be REMOVED from the list!
			if( (ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE) || (ilc->bbf.w_reason_in_list == IN_LIST_FOR_TEST) )
			{
				ilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), ilc, ACTION_REASON_TURN_OFF_AND_REMOVE_higher_reasons_in_the_list );
				
				need_to_get_the_next_ilc = (false);
			}
			
			// For all reasons - test or not - skip over it - if its for test it won't be here next
			// time.
			continue;
		}
		
		
		if( (ilc->bbf.w_involved_in_a_flow_problem == (true)) && (ilc->bsr->ufim_one_ON_from_the_problem_list_b == (true)) )
		{
			continue;
		}
		
		// 6/1/2012 rmd : When we are in the list for a reason that allows flow checking we must
		// take into account the mixing of flow check groups. They cannot mix else the flow check
		// could report false errors. Also, when in the list for test we allow flow groups to mix to
		// avoid confusion - when the user asks to TEST we turn it ON for them.
		if( ilc->bbf.flow_check_when_possible_based_on_reason_in_list && (ilc->bbf.w_reason_in_list != IN_LIST_FOR_TEST) )
		{
			if( FOAL_IRRI_there_are_other_flow_check_groups_already_ON( &ilc->bbf, &ilc->bsr->ufim_flow_check_group_count_of_ON[ 0 ] ) )
			{
				// skip over it
				continue;
			}
		}			
		
		// 2011.11.09 rmd : Now test against capacity. If we encounter a single station with an
		// expected greater than the capacity limit we ALERT about such and bypass the limit. In the
		// 2000e we used to actually change the limit. 
		//
		// But not for TEST or DTMF.
		if( (ilc->bbf.w_reason_in_list != IN_LIST_FOR_MOBILE) && (ilc->bbf.w_reason_in_list != IN_LIST_FOR_TEST) )
		{
			if( will_exceed_system_capacity( ilc ) == (true) ) continue;
		}
		
		
		// Pump versus non-pump turn-on rules - for the reasons of MOBILE and TEST we ignore the
		// pump considerations i.e. we allow a mix of valves to comes ON!
		//
		// It also turns out that this happened in a sms sequence in a mixed environment.  Therefore, to eliminate this problem,
		// a condition was added to only execute the following statements if the controller is not performing a walk-thru.
		if( (ilc->bbf.w_reason_in_list != IN_LIST_FOR_MOBILE) && (ilc->bbf.w_reason_in_list != IN_LIST_FOR_TEST) && (ilc->bbf.w_reason_in_list != IN_LIST_FOR_WALK_THRU) )
		{
			// When there is a mix of pump and non-pump valves ON don't add more to the mess - I think
			// this should never happen.
			if( ilc->bsr->ufim_there_is_a_PUMP_mix_condition_b == (true) ) continue;
			
			// If this is a pump station
			if( ilc->bbf.w_uses_the_pump == (true) )
			{
				// When working pumpers is TRUE it implies several things. It implies a station
				// is on that requires the pump. Or it implies that we were in non-pump mode and made
				// one pass through this turn-on algorithm and didn't turn any non-pumpers on so we switched
				// to look at pumpers.
				if ( ilc->bsr->ufim_what_are_we_turning_on_b == TURNING_ON_NON_PUMP_VALVES ) continue;
				
				// This condition could exist if you had say 4 pumpers on and we added a non-pump test to
				// the list. The test would come on when the pumpers went off. Lets say they turned off
				// together. If there were more pumpers ready to turn on we would turn on the first one
				// before declaring a mix condition (the mix condition would prevent more from turning on).
				if ( (ilc->bsr->system_master_number_of_valves_ON > 0) && (ilc->bsr->ufim_stations_ON_with_the_pump_b == (false)) ) continue;
			}
			else
			{
				if ( ilc->bsr->ufim_what_are_we_turning_on_b == TURNING_ON_PUMP_VALVES ) continue;
				
				// Same condition but in reverse for the pumpers above. A pump valve could be on (for TEST)
				// even though we are hunting for NON-PUMPERS.
				if ( ilc->bsr->ufim_stations_ON_with_the_pump_b == (true) ) continue;
			}
			
		}


		// ELECTRICAL LIMIT - all reasons in the list follow this.
		if( foal_irri.stations_ON_by_controller[ ilc->box_index_0 ] >= NETWORK_CONFIG_get_electrical_limit( ilc->box_index_0 ) )
		{
			// If in the list for DTMF or TEST remove it with an advisory for the user so he
			// understands.
			if( (ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE) || (ilc->bbf.w_reason_in_list == IN_LIST_FOR_TEST) )
			{
				ilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), ilc, ACTION_REASON_TURN_OFF_AND_REMOVE_hit_electrical_limit );
				
				need_to_get_the_next_ilc = (false);
			}
			
			// for all reasons - test or not - skip over it - if its for test it won't be here next time
			continue;
		}
		
		
		// WIND - is the system in the pause state 
		if( foal_irri.wind_paused )
		{
			// 11/13/2014 rmd : If it was set to respond to wind when the station joined the list then
			// that is the intention. No reason to check the reason in the list.
			if( ilc->bbf.responds_to_wind )
			{
				// 11/17/2014 rmd : However do need to check reason in list if we are going to set the
				// station history flag.
				if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
				{
					station_preserves.sps[ ilc->station_preserves_index ].station_history_rip.pi_flag.wind_conditions_prevented_or_curtailed = (true);
				}

				continue;
			}
		}
	

		// ON-AT-A-TIME rules
		//
		// When in FORCED we control the number of valves ON by have previously setting 
		// "maximum_number_of_valves_we_can_have_on_now" to a 1 - the result is for reasons <= MANUAL
		// we limit the number on to 1, for other reasons the usual	rules apply.
		//
		// With regards to the standard turn-on rules - who should follow them - I would say everybody except 
		// DTMF, TEST, and WALK_THRU (WALK_THRU has its own turn on rules of 1 per valve in the
		// whole system at a time).
		if( (ilc->bbf.w_reason_in_list != IN_LIST_FOR_MOBILE) && (ilc->bbf.w_reason_in_list != IN_LIST_FOR_TEST) && (ilc->bbf.w_reason_in_list != IN_LIST_FOR_WALK_THRU) )
		{
			// Are we at the limit already? (only for MANUAL, SMS1, SMS2, PROGRAMMED IRRIGATION, and
			// SMS_HO do we observe the by program limits) Are we at the system limit yet? And can this
			// valve come ON and will the toal ON then be below this valves limit?
			if( SYSTEM_PRESERVES_is_this_valve_allowed_ON( ilc->bsr, ilc->GID_on_at_a_time ) == (false) )
			{
				continue;	
			}

			// 12/19/2012 ajv : If the station isn't assigned an on-at-a-time group yet, we can't check
			// the group's presently_on_group_count. However, since the defaults are 1 in system and 1
			// in group, the station's in system value will kick back any stations that try to turn on
			// beyond 1.
			if( ilc->GID_on_at_a_time > 0 )
			{
				// - NOW check at the GROUP level!
				if( ON_AT_A_TIME_can_another_valve_come_ON_within_this_group( ilc->GID_on_at_a_time ) == (false) )
				{
					continue;	
				}
			}
		}
		else
		if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_WALK_THRU )
		{
			// 5/30/2018 rmd : The SHORT explanation is, we have been asked to allow only ONE walk_thru
			// valve on at a time, within the NETWORK (which might include multiple mainlines). So the
			// if statement test here is just that, one walk_thru valve ON, regardless of mainline. We
			// used to test if one was one in the system [if(ilc->bsr->system_master_number_of_valves_ON
			// >= 1 )], but that allows multiple walk_thru valves on at a time if the valves in the
			// sequence are from more than one mainline.
			//
			// The LONGER explanation is, I usually envision separate mainlines as having valves
			// physically apart from one another, and therefore when it comes to walk-thru, the user
			// could, and would normally, build a sequence of valves from one mainline or another, and
			// if two maintenance guys were at a site, they may want to run two walk-thrus at the same
			// time, one on one mainline and one on the other mainline. WELL APPARENTLY that assumption
			// is not valid at some sites. To make it even more strange, we already restrict the valves
			// in a WALK_THRU sequence to be from the same BOX, but when building a sequence, even
			// within a box, there are sites where the valves within a box are from multiple mainlines,
			// and the user TRULY wants to perform the walk_thru, following his sequence, one valve ON
			// at a time, and the walk_thru sequence of valves indeed comes from one mainline or
			// another. [Mark Huntzinger ran into this case within 4 weeks of deployment of the WalkThru
			// feature.]
			//
			// So, in the case that there is only one MAINLINE, this change has no effect on the
			// existing behavior. For the case of multiple mainlines, the change is you cannot
			// simultaneously run two walk_thru sequences. The first sequence has to complete, before
			// the second one will start, you can queue them up, but the second one won't run. In some
			// rare cases this will probably be an annoyance.
			if( one_ON_for_walk_thru )
			{
				continue;
			}
		}
		
		// If we have one on to set the expected don't allow another to come on - (for now) NO
		// matter what the priority of the one that wants to come on. Remember we can turn a valve
		// on using TEST to learn the expected (actually to set the expected) - so if a station is
		// added to the list for TEST while another is already on to set expected we will KILL it
		// and tell the user about that.
		//
		// If you ask me this test should be made in the loop where we cause xfer's to be made ...
		// that's the loop just before this BIG 'continue' loop that we're in. You see we test if
		// there are any on the xfer list before we start this loop we're in so it feels funny to be
		// adding to the xfer list here. I'll leave for now but maybe Dan S. will restructure when
		// he is mostly confident about what is happenning here.
		if( (ilc->bsr->ufim_one_ON_to_set_expected_b == (true)) || (ilc->bsr->ufim_one_RRE_ON_to_set_expected_b == (true)) )
		{
			// Actually if one_RRE_ON_to_help_set_expected_uc is TRUE that means the highest reason in the list is radio remote
			// which says any TEST valve would be already removed (by being in the list for a lessor reason) - we however must
			// do this if statement as the mechanism for skipping over other RRE valves in the list ... that is what the 'continue' does
			// I think this would kick in if one valve was ON for TEST to set expected and another valve for TEST was added to the list
			if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_TEST )
			{
				ilc = nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists( &(foal_irri.list_of_foal_all_irrigation), ilc, ACTION_REASON_TURN_OFF_AND_REMOVE_already_one_ON_to_acquire_expected );
				
				need_to_get_the_next_ilc = (false);
			}
			
			// for ALL reasons - test or not - skip over it - if its for test it won't be here next time
			continue;
		}
		
		
		// If the list contains stations that have the set expected marked and this station doesn't
		// have his set expected marked we are not interested in him. This controls getting all the
		// set expecteds done first.
		if( (ilc->bsr->ufim_list_contains_some_to_setex_that_are_not_ON_b == (true)) || (ilc->bsr->ufim_list_contains_some_RRE_to_setex_that_are_not_ON_b == (true)) )
		{
			if( FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list( ilc->bbf.w_reason_in_list, ilc->bsr->ufim_highest_reason_in_list_available_to_turn_ON ) == (true) )
			{
				// actually this statement WILL NEVER execute because there is an identical "if" prior to this
				// that skips over valves in the list for a reason less than the highest BUT I have it here for
				// CLARITY while reading this so it's easier to understand the "else" part which is the meat
				//
				continue;
			}
			else
			{
				// soooooo ... this valve is in the list for the a reason equal to the highest
				// now we should ONLY test against it's set expected bits if the highest_reason_of_OFF_valve_to_set_expected_uc
				// is equal to this valves reason in the list
				if ( ilc->bsr->ufim_highest_reason_of_OFF_valve_to_set_expected == ilc->bbf.w_reason_in_list )
				{
					if ( ilc->bbf.w_to_set_expected == (false) )
					{
						// If this is not one to set expected for we are not interested in him.
						continue;
					}

				}
				else
				{
					// else this valve is in the list for the highest reason - there are valves in the list to set expected
					// BUT apparently they are in the list for a lessor reason so this valve WINS and can come ON
				}
			}

			// If there are others on at the time we also don't want to turn on the one to acquire the
			// expected flow rate cause it will fail (more than one on when making acquisition). Remember
			// if there are valves in the list for a higher reason than the set expected one they will be
			// PAUSED. If all the valves are for the same reason we wait until all on turn OFF normally.
			if ( ilc->bsr->system_master_number_of_valves_ON > 0 )
			{
				// Don't turn on the one to set expected if there are others on at this time.
				continue;
			}
		}
		
		// ----------
		
		// CAN'T HAVE A STATION ON FOR 2 REASONS. Check that the station we are about to turn on
		// isn't already on - for some other reason!! Don't want to run the station for two
		// different reasons at the same time - could happen if reasons are equal (OR TREATED AS
		// EQUALS!). Such as can happen with Manual program and Programmed Irrigation?
		BOOL_32 lthis_station_is_already_on_b;

		lthis_station_is_already_on_b = (false);
		
		// 5/9/2012 rmd : We are presently going through the main irrigation ilc list. Here we can
		// safely pass through the list of those ON. Looking to see if this station is already in
		// the list. If so we won't be attempting to turn him ON again.
		tmp_ilc = nm_ListGetFirst( &(foal_irri.list_of_foal_stations_ON) );
		
		while( tmp_ilc != NULL )
		{
			if( (tmp_ilc->box_index_0 == ilc->box_index_0) && (tmp_ilc->station_number_0_u8 == ilc->station_number_0_u8) )
			{
				lthis_station_is_already_on_b = (true);
			}
		
			tmp_ilc = nm_ListGetNext( &(foal_irri.list_of_foal_stations_ON), tmp_ilc );
		}
		
		if( lthis_station_is_already_on_b == (true) ) continue;
		
		// ----------
		
		// 5/17/2018 rmd : MOISTURE SENSING test if station is allowed to irrigate another cycle.
		if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
		{
			if( MOISTURE_SENSORS_this_ilc_has_irrigated_the_required_cycles( ilc ) )
			{
				// 5/17/2018 rmd : All caught up, must wait.
				continue;
			}
		}
		
		// ----------
		
		set_remaining_seconds_on( ilc );
		
		if( ilc->remaining_seconds_ON > 0 )
		{
			// 5/30/2012 rmd : The INTENTION IS this is the ONLY, I repeat ONLY, place in the code where
			// we turn stations ON. This is important for the accuracy of the dynamic properties kept
			// about each system. Specifically the expected_flow_rate_for_those_ON and the
			// flow_group_count are two which pop to mind. There are others.

			// ------------------------------			

			// BEFORE we update the local_pump_is_on state and the number_currently_on we MUST check if
			// we are entering a mix condition. We need to do it before the update of those two as the
			// logic depends on it. If there is a already a mix, there still is a mix. If there are
			// stations on, then if the pump is off and we are adding a station that uses the pump we
			// have a mix. If the pump is on and we adding a station that doesn't use the pump we have a
			// mix.
			if( ilc->bsr->system_master_number_of_valves_ON > 0 )
			{
				if ( ilc->bsr->ufim_stations_ON_with_the_pump_b == (false) )
				{
					if ( ilc->bbf.w_uses_the_pump == (true) )
					{
						// Here we are...the pump is off, there are stations on and we are adding a station that
						// uses the pump.
						ilc->bsr->ufim_there_is_a_PUMP_mix_condition_b = (true);
					}
				}
				else
				{
					if ( ilc->bbf.w_uses_the_pump == (false) )
					{
						// Here we are...the pump is on, there are stations on and we are adding a station that
						// doesn't use the pump.
						ilc->bsr->ufim_there_is_a_PUMP_mix_condition_b = (true);
					}
				}
			}

			// ------------------------------			

			// 6/27/2012 rmd : BEFORE we call the function that completes the turn ON decide what the
			// action needed reason will be so we will properly handle the flow and current flag
			// settings. Need to do before the turn ON function as it increments the repeat count for
			// the case of programmed irrigation.
			UNS_32	action_needed_reason;
			
			if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_PROGRAMMED_IRRIGATION )
			{
				// 6/22/2012 rmd : If it is the first repeat.
				if( station_preserves.sps[ ilc->station_preserves_index ].station_history_rip.pi_number_of_repeats == 0 )
				{
					action_needed_reason = ACTION_REASON_TURN_ON_and_clear_flow_status;
				}
				else
				{
					action_needed_reason = ACTION_REASON_TURN_ON_and_leave_flow_status;
				}
			}
			else
			if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_MANUAL )
			{
				// 6/22/2012 rmd : MANUAL irrigation does not check for flow. It does check the current
				// though. But the action reason actually does not manage clearing the i_status. That is
				// always cleared upon each turn ON.
				action_needed_reason = ACTION_REASON_TURN_ON_and_leave_flow_status;
			}
			else
			{
				action_needed_reason = ACTION_REASON_TURN_ON_and_clear_flow_status;
			}
	
			// 5/9/2012 rmd : COMPLETE the turn ON. Because we also add this ilc to the action_needed
			// list this function won't run again until this turn ON has been transmitted in a TOKEN.
			// And that means the time will not count down until then.
			//
			// This introduces another behavior which may prove undesireable. When ever a station is a
			// member of the action_needed list, the countdown of run times is temporarily suspended as
			// the foal_irri maintenance function, THIS maintenance function, doesn't run. We may need
			// to address this. So that we at least run through the list of those ON and count their
			// time down. Left to be seen if this is a concern.
			_nm_nm_foal_irri_complete_the_action_needed_turn_ON( ilc );

			// ------------------------------			

			// 5/9/2012 rmd : Add to the action_needed list so we tell all the irri machines. And serves
			// to freeze the time until we do send the next TOKEN with the notification. This tends
			// to keep the countdown of time remaining in sync between the irri and foal machines.
			nm_FOAL_add_to_action_needed_list( ilc, action_needed_reason );

			// ----------------------------
			
			// And KEEP THE EXPECTED FLOW RATE up to date of course.
			ilc->bsr->ufim_expected_flow_rate_for_those_ON += ilc->expected_flow_rate_gpm_u16;

			// update our FLOW TEST group count
			ilc->bsr->ufim_flow_check_group_count_of_ON[ ilc->bbf.flow_check_group ] += 1;

			// 6/18/2012 rmd : And of course the number ON in the system. Set this before we merge in
			// the allowed ON into the limit. So the test made in that function is accurate.
			ilc->bsr->system_master_number_of_valves_ON += 1;
			
			// ----------------------------
			
			// 8/30/2012 rmd : For TEST and RRE use the number ON limit is the electrical limit for the
			// box. TEST and RRE completely ignore the on_at_a_time limits.
			if( (ilc->bbf.w_reason_in_list != IN_LIST_FOR_TEST) && (ilc->bbf.w_reason_in_list != IN_LIST_FOR_MOBILE) )
			{
				// Update the system limit of how many are allowed ON. But this is not done for TEST and
				// RRE. Their limits are the electrical limit. And that is handled differently.
				SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit( ilc->bsr, ilc->GID_on_at_a_time );
				
				// Keep the GROUP on_at_a_time limit up to date. Actually bumps a number ON count. And is
				// okay to skip for the case of TEST or RRE as they do not use the ON_AT_A_TIME limits. 
				ON_AT_A_TIME_bump_this_groups_ON_count( ilc->GID_on_at_a_time );
			}
			
			// ----------
			
			if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_WALK_THRU )
			{
				// 5/30/2018 rmd : If we just turned one ON for WALK_THRU, we must update our flag to
				// reflect that, preventing more from turning ON!
				one_ON_for_walk_thru = (true);
			}
			
			// ----------------------------
			
			if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_TEST )
			{
				ilc->bsr->ufim_number_ON_during_test += 1;
			}
			
			// ----------------------------
			
			// And keep the problem list tracker up to date too.
			if( ilc->bbf.w_involved_in_a_flow_problem == (true) )
			{
				ilc->bsr->ufim_one_ON_from_the_problem_list_b = (true);
			}

			// ----------------------------
			
			// And keep the pump_on indicator up to date too.
			if( ilc->bbf.w_uses_the_pump == (true) )
			{
				ilc->bsr->ufim_stations_ON_with_the_pump_b = (true);
			}
			else
			{
				ilc->bsr->ufim_stations_ON_without_the_pump_b = (true);
			}

			// ----------------------------
			
			// And keep the set expected indicator up to date too.
			if( ilc->bbf.w_to_set_expected == (true) )
			{
				if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE )
				{
					// Set this true to prevent any other stations from turning on.
					ilc->bsr->ufim_one_RRE_ON_to_set_expected_b = (true);

				}
				else
				{
					// Set this true to prevent any other stations from turning on.
					ilc->bsr->ufim_one_ON_to_set_expected_b = (true);
				}
			}
							
			// ----------
			
			// 7/2/2015 rmd : Send the status to the mobile NOW after the station has been added to the
			// ON list. We did not send a status when the command to turn ON was rcvd.
			if( ilc->bbf.w_reason_in_list == IN_LIST_FOR_MOBILE )
			{
				// 6/18/2015 rmd : Alert.
				Alert_mobile_station_on( ilc->box_index_0, ilc->station_number_0_u8 );

				CONTROLLER_INITIATED_post_to_messages_queue( CI_MESSAGE_send_mobile_status, NULL, CI_IF_PRESENT_LEAVE_IN_POSITION_AND_DONT_ADD, queueSEND_TO_BACK );
			}

		}
		else
		{
			// So the time to run came back 0!

			// This can happen when the chain goes down. For instance, let's assume "A" is watering
			// station 1. Communications starts to struggle. Station 1 finishes just before the chain
			// goes down. "A" will try to resume any stations that were watering when the chain started
			// to enter forced. Since station 1 has already completed, this message will appear because
			// it has no time remaining.
			// 
			// 608.f ajv - Since this alert only seems to appear when the chain goes up and down during
			// irrigation, we have decided to suppress this. If we need to re-enable it at any time,
			// we can do so via CMOS.
			// 
			// 2011.11.10 rmd : I have undone the ability to supress this alert with the debug bit. I
			// believe that now becasue the way the chain works (all decisions made in one place)
			// this may not happen anymore. But we shall see about that. Literally...in the alerts.

			ALERTS_get_station_number_with_or_without_two_wire_indicator( ilc->box_index_0, ilc->station_number_0_u8, str_8, sizeof(str_8) );

			Alert_Message_va( "Station %s Skipped: (0 time)", str_8 );

			// 6/7/2012 rmd : Remove this station from the list. Tell the irri machines and do it now
			// for us the foal machine.
			nm_FOAL_add_to_action_needed_list( ilc, ACTION_REASON_TURN_OFF_AND_REMOVE_irrigation_time_unexpectedly_zero );
			
			ilc_to_remove = ilc;
			
			ilc = nm_ListGetNext( &(foal_irri.list_of_foal_all_irrigation), ilc );
			
			need_to_get_the_next_ilc = (false);

			nm_ListRemove( &(foal_irri.list_of_foal_all_irrigation), ilc_to_remove );

			continue;

		}  // of the time came back as 0!
		
	}  // of for loop looking through the list for one to turn on
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Called on at a 1 hz rate if we are in sync and generating tokens..  

    @CALLER MUTEX REQUIREMENTS (none) This function will internally need to take and give
    certain needed list mutexes. One obvious one is the foal irri list mutex. It should be
    noted that when the TD_CHECK task is calling this function it is holding the iccc MUTEX.
	
    @EXECUTED Executed within the context of the TD_CHECK task.
	
	@RETURN (none)

	@AUTHOR 2011.10.31 rmd
*/
extern void FOAL_IRRI_maintain_irrigation_list( const DATE_TIME *const pdate_time )
{
	DATE_TIME	stop_dt;

	INT_32	i;
	
	// ----------
	
	// 6/19/2012 rmd : As the foal_irri maintenance function is called at a 1 hz rate and I
	// consider the timers part of the irrigation maintenance we perform the timer housekeeping
	// here. Keeping this call outside of the consideration to run the station maintenance or
	// not.
	decrement_system_preserves_timers();
	
	// ----------
	
	// Protect the foal_irri lists (irrigation, sxru, etc.) from changes while we work with it.
	xSemaphoreTakeRecursive( list_foal_irri_recursive_MUTEX, portMAX_DELAY );

	// 4/16/2012 rmd : And because we are using looking at the system_preserves
	// highest_reason_in_list variable outside of this task we need to take the system preserves
	// mutex to assure the highest reason is correct when it is used.
	xSemaphoreTakeRecursive( system_preserves_recursive_MUTEX, portMAX_DELAY );

	// ----------
	
	// 6/21/2012 rmd : Even if we don't run this function, allow a new group of records to be
	// made. But don't set this (false) till we have the MUTEXES. As that could mess up other
	// functions that are in the process of turning all the station OFF. Like the STOP key
	// processing.
	// 12/4/2012 rmd : I suspect this line is not necessary. This variable is set (false) on a
	// case by case basis as we enter into a turn OFF loop (for 1 or more stations). Ahhhh. But
	// we are entering into a potential turn OFF loop. We are going to go through all the
	// stations in the list and if we are say at the STOP time would indeed turn OFF a group of
	// valves. Or simply running out of time. We want a batch of flow recording lines made for
	// the valves that are ON, if that group hasn't yet had a batch made. This particular
	// variable manages making ONE batch for the valves ON.
	foal_irri.flow_recording_group_made_during_this_turn_OFF_loop = (false);
	
	// ----------
	
	// 2011.12.03 rmd : Here's the scenario. We came through this function and found some valves
	// to turn OFF. And we did so putting them on the sxru list. Now picture we are off making
	// TOKENS and we haven't yet made the critical token to turn OFF the valve so actually it is
	// still ON. If we came through here again we would put the valves to turn OFF on the sxru
	// list AGAIN. Checking the sxru list for that situation prevents this from occuring. And we
	// leave this function if so.
	//
	// 2012.02.17 rmd : At first all we did was check for some ASAP items to exisit on the SXRU
	// list. But I think the safest approach is to insist on an EMPTY list before proceeding.
	// This may affect performance in certain ways. Resulting in delays at times as the list is
	// worked down. But that might be acceptable. We shall see.
	if( foal_irri_maintenance_may_run() )
	{
		// 6/19/2012 rmd : Initialize those variables that we are about to learn as we travers
		// through the entire irrigation list.
		SYSTEM_PRESERVES_init_system_preserves_ufim_variables_for_all_systems();  // ufim = Used For Irrigation Management
		
		ON_AT_A_TIME_init_all_groups_ON_count();
		
		// 3/31/2016 rmd : Initialize the tracking variables used for the part of the moisture
		// sensing algorithm that decides if a station can run a cycle or not.
		MOISTURE_SENSORS_initialize__transient__control_variables_for_all_sensors();
		
		// 6/6/2018 rmd : Initialize for Walk_thru control. Only one ON at a time within the
		// NETWORK.
		one_ON_for_walk_thru = (false);
		
		// ----------
		
		// 4/30/2014 rmd : First of all the in_forced term has been eliminated. Now called
		// chain_down. Second of all we now have defined that when the chain is down we will NOT
		// irrigate at all. If we were to irrigate though and wanted to limit the on-at-a-time to
		// one the following is the code to do so.
		/*
		// 10/1/2012 rmd : When it comes to checking if we are in FORCED or not go straight to the
		// comm_mngr. The chain_members structure may not be accurate when in forced. I have yet
		// verify that.
		xSemaphoreTakeRecursive( comm_mngr_recursive_MUTEX, portMAX_DELAY );

		// SET the limit of those ON to 1 for manual, sms1, sms2, programmed irrigation, and sms_ho
		// when we are in FORCED
		if( comm_mngr.in_forced )
		{
			// Loop through ALL systems and jigger their limits to reflect ONE on-at-a-time as we are in
			// forced. This becomes effectively a NETWORK setting.
			for( i=0; i<MAX_POSSIBLE_SYSTEMS; i++ )
			{
				system_preserves.system[ i ].ufim_maximum_valves_in_system_we_can_have_ON_now = 1;
			}
		}
		
		xSemaphoreGiveRecursive( comm_mngr_recursive_MUTEX );
		*/

		// ----------
		
		irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations( pdate_time );

		// ----------

		// ONLY proceed when the system is stable - ie all turn ons/offs that need to take place are completed.
		if( foal_irri_maintenance_may_run() )
		{
			irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars();

			irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON();
	
		}  // of if the action_needed list is empty and the function may proceed

		// ----------
		
		// ONLY proceed when the system is stable - ie all turn ons/offs that need to take place are completed.
		// This is critical to proper functioning...one example would be turn on of masters during moisture sensing
		// based irrigation. If we put a master on the xfer list and ran this hunt to turn on we could put another
		// master on the turn on list cause the delay between master turn ons doesn't get set until the station is
		// actually transferred.
		if( foal_irri_maintenance_may_run() )
		{
			irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON();
			
			// ----------
			
			// 8/27/2015 ajv : Now, let's do some Master Valve Override maintenance. Specifically, let's
			// check to see whether it's a stop time (or we've crossed a stop time due to a powerfail)
			// and stop any ongoing MVORs.
			for( i = 0; i < MAX_POSSIBLE_SYSTEMS; ++i )
			{
				if( (system_preserves.system[ i ].sbf.MVOR_in_effect_opened) || (system_preserves.system[ i ].sbf.MVOR_in_effect_closed) )
				{
					stop_dt.D = system_preserves.system[ i ].mvor_stop_date;
					stop_dt.T = system_preserves.system[ i ].mvor_stop_time;

					if( DT1_IsBiggerThanOrEqualTo_DT2( pdate_time, &stop_dt ) )
					{
						FOAL_IRRI_initiate_or_cancel_a_master_valve_override( system_preserves.system[ i ].system_gid, MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE, 0, INITIATED_VIA_MVOR_SCHEDULE );
					}
				}
			}
			
			// ----------
			
			// 6/19/2012 rmd : Okay. So we have passed through the all the stations looking to turn some
			// ON. There may or maynot be valves on the action_needed list. And that's okay. All the
			// valves that were supposed to turn OFF happened ealier in this function and we wouldn't be
			// here if there were some. To be here means we did run the turn ON loop, and the tokens
			// have caught up. Our big concern was catching the state in between valves and catching
			// that as a 'no valves ON condition'. Which would then cause us to reload the MVJO timer
			// the next time a valve turned ON.
			// 2/21/2013 rmd : It is critical this runs following any valve turn ONs. The non-irrigation
			// MLB block out timer must be loaded so that when the valve turns off we do not fall into
			// non-irrigation MLB checking right away. MV's and PMPs need to shut down. And slow closing
			// valve delays must expire. Not to mention the valve has to close and flow rates settle
			// down.
			_load_and_maintain_system_anti_chatter_timers();
			
		}  // of if the function can continue to run
		
	}  // of this maintenance function was okayed to run

	// ----------
	
	xSemaphoreGiveRecursive( system_preserves_recursive_MUTEX );
	
	xSemaphoreGiveRecursive( list_foal_irri_recursive_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

