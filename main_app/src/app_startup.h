/* file = app_startup.h                      10.09.2009  rmd  */
/* ---------------------------------------------------------- */

#ifndef INC_APP_STARTUP_H_
#define INC_APP_STARTUP_H_

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cs_common.h"

#include	"foal_defs.h"

#include	"lpc_types.h"

#include	"serial.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Structure for a task creation table entry
typedef struct 
{
	BOOL_32					bCreateTask;			// TRUE if this entry is to be processed

	BOOL_32					include_in_wdt;			// if true will monitor for activity in the watchdog task
	
	UNS_32					execution_limit_ms;		// how many ms since last execute will we tolerate	

	pdTASK_CODE				pTaskFunc;				// The task function

	const char * const		TaskName;				// The task name, maximum 16 chars. incl. NULL

	UNS_32					stack_depth;			// The depth of the stack in 32-bit words

	void 					*parameter;				// Soemthing unique the task can extract about itself (if needed).

	UNS_32					priority;				// Task priority

} TASK_ENTRY_STRUCT;
	

#define	WATCHDOG_MONITORS		(true)
#define	WATCHDOG_EXCLUDED		(false)

#define NUMBER_OF_TABLE_TASKS	(24)

extern const TASK_ENTRY_STRUCT Task_Table[ NUMBER_OF_TABLE_TASKS ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32	startup_task_FLOP_registers[ portNO_FLOP_REGISTERS_TO_SAVE ] __attribute__( APPLICATION_IRAM_VARS );  

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// TASK HANDLES. Why are these important? The power_fail task uses them to suspend all tasks . Except for the power fail task itself
// and the FLASH_STORAGE task.

// Don't forget about these THREE which are created outside of the task table.
// We want the RTC task to start and run for a bit before we allow the system_startup_task to proceed. One way to do that
// is by knowing the startup tasks handle. And we'll resume it from this RTC task.
extern xTaskHandle		startup_task_handle, epson_rtc_task_handle, powerfail_task_handle, wdt_task_handle;

// And provide a task handle for the internally generated timer task (internal to the OS that is).
extern xTaskHandle		timer_task_handle;

// 3/10/2017 rmd : And let both flash tasks make a graceful attempt at an activity exit in
// the impending face of a power failure. Need to capture their handles to do this. The shut
// down task suspends almost all tasks except the two flash tasks.
extern xTaskHandle		flash_storage_0_task_handle;

extern xTaskHandle		flash_storage_1_task_handle;

// And this array of handle for the task table entries.
extern xTaskHandle		created_task_handles[ NUMBER_OF_TABLE_TASKS ];


extern UNS_32			rtc_task_last_x_stamp, pwrfail_task_last_x_stamp;

extern UNS_32			task_last_execution_stamp[ NUMBER_OF_TABLE_TASKS ] __attribute__( APPLICATION_IRAM_VARS );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
////////////////////////////////
// System Semaphore Handles
////////////////////////////////

// 7/16/2012 rmd : Used to synchronize the startup task progress till after a particular
// point in the RTC task has completed.
extern xSemaphoreHandle		startup_rtc_task_sync_semaphore;


// Handle to the memory_protection MUTEX
extern xSemaphoreHandle		memory_protection_MUTEX;


extern xSemaphoreHandle		alerts_pile_recursive_MUTEX;


// Handle to the DF Storage Manager MUTEX
extern xSemaphoreHandle		Flash_0_MUTEX;

extern xSemaphoreHandle		Flash_1_MUTEX;


// These semaphores signal the ring buffer task to go ahead and take a look as some data has come in.
extern xSemaphoreHandle		rcvd_data_binary_semaphore[ UPORT_TOTAL_PHYSICAL_PORTS ];


extern xSemaphoreHandle		list_program_data_recursive_MUTEX;


extern xSemaphoreHandle		list_tpl_in_messages_MUTEX;

extern xSemaphoreHandle		list_tpl_out_messages_MUTEX;


extern xSemaphoreHandle		list_incoming_messages_recursive_MUTEX;


extern xSemaphoreHandle		list_packets_waiting_for_token_MUTEX;


extern xSemaphoreHandle		xmit_list_MUTEX[ UPORT_TOTAL_PHYSICAL_PORTS ];


extern xSemaphoreHandle		ci_and_comm_mngr_activity_recursive_MUTEX;


// 2012.02.07 rmd : This MUTEX protects the irri_irri structure. Which contains the irri
// side irrigation list as well as the irri SXRU list. And several other additional
// variables. All protected with this one MUTEX.
extern xSemaphoreHandle		irri_irri_recursive_MUTEX;


// 2011.08.20 rmd : Again a single MUTEX to protect multiple lists. This time we are
// protecting 5 lists. They are the IrrigationList, the ONList, the StationsONByController
// lists (all 12 of them), the XferList, and the Flow_Check_List.
//
// And the use of the recursive mutex is to facilitate the Taking and Giving when the call
// structure of several functions requiring list protection becomes complicated and deep.
extern xSemaphoreHandle		list_foal_irri_recursive_MUTEX;



// This is to protect against any outside reference to the comm_mngr management structure.
// Which is so far only to display it. And that's all it should ever be. All other
// references should be within the context of the comm mngr task itself!
extern xSemaphoreHandle		comm_mngr_recursive_MUTEX;

// 2011.11.01 rmd : This mutex is used to protect the battery backed record of what the
// chain looks like. It is referenced from several task contexts. The comm_mngr generally
// changes it while TD_CHECK and other look at it. And the TD_CHECK task expects the
// structure to be stable until it is done with certain activites.
extern xSemaphoreHandle		chain_members_recursive_MUTEX;

// 5/1/2014 rmd : Introduced to protect the send_box_configuration_to_master variable. Which
// can be changed via the keypad (controller name in config_c) or via the comm_mngr when a
// tpmicro message is recvd with the what's installed.
extern xSemaphoreHandle		irri_comm_recursive_MUTEX;


// 9/4/2012 rmd : To protect the system_report_data_completed_records.
extern xSemaphoreHandle		system_report_completed_records_recursive_MUTEX;

// 2011.09.23 rmd : To protect the poc_report_data_completed_records.
extern xSemaphoreHandle		poc_report_completed_records_recursive_MUTEX;

// 2011.09.23 rmd : To protect the station_report_data_completed_records.
extern xSemaphoreHandle		station_report_data_completed_records_recursive_MUTEX;

// 2011.09.23 rmd : To protect the station_history_completed_records.
extern xSemaphoreHandle		station_history_completed_records_recursive_MUTEX;

// 2011.09.23 rmd : To protect the weather preserves data.
extern xSemaphoreHandle		weather_preserves_recursive_MUTEX;

// 2011.09.23 rmd : To protect the station preserves array data.
extern xSemaphoreHandle		station_preserves_recursive_MUTEX;

// 2011.11.08 rmd : To protect the system preserves array data.
extern xSemaphoreHandle		system_preserves_recursive_MUTEX;

// 2012.01.05 rmd : To protect the poc preserves array data.
extern xSemaphoreHandle		poc_preserves_recursive_MUTEX;

//2012.02.06 ajv : To protect the irrigation system list.
extern xSemaphoreHandle		list_system_recursive_MUTEX;

//2012.02.06 ajv : To protect the POC list.
extern xSemaphoreHandle		list_poc_recursive_MUTEX;

// 9/13/2012 rmd : To specifically protect the weather control variables.
extern xSemaphoreHandle		weather_control_recursive_MUTEX;

// 9/13/2012 rmd : To protect the et and rain tables during access of value and rolling.
extern xSemaphoreHandle		weather_tables_recursive_MUTEX;

// 8/2/2012 rmd : See comment at actual declaration.
extern xSemaphoreHandle		speaker_hardware_MUTEX;

// 8/2/2012 rmd : See comment at actual declaration.
extern xSemaphoreHandle		tpmicro_data_recursive_MUTEX;


extern xSemaphoreHandle		pending_changes_recursive_MUTEX;

// 9/9/2015 mpd : Lights has it's own set of mutexes. Here are all the Mutex calls used by 
// lights along with the structures and variables they protect. Viewing or editing any of
// these items requires a mutex to ensure the data is stable.
// 
//	xSemaphoreTakeRecursive( list_lights_recursive_MUTEX, portMAX_DELAY );
// 	LIGHT_STRUCT
// 	light_list_hdr
//	xSemaphoreGiveRecursive( list_lights_recursive_MUTEX, );
// 
//	xSemaphoreTakeRecursive( list_foal_lights_recursive_MUTEX, portMAX_DELAY );
//	foal_lights
//	FOAL_LIGHTS_BATTERY_BACKED_STRUCT
//	header_for_foal_lights_ON_list
//	header_for_foal_lights_with_action_needed_list
//	xSemaphoreGiveRecursive( list_foal_lights_recursive_MUTEX );
// 
//	xSemaphoreTakeRecursive( irri_lights_recursive_MUTEX, portMAX_DELAY );
//	irri_lights
//	IRRI_LIGHTS
//	header_for_irri_lights_ON_list
//	xSemaphoreGiveRecursive( irri_lights_recursive_MUTEX );
//
//	xSemaphoreTakeRecursive( lights_preserves_recursive_MUTEX, portMAX_DELAY );
//	lights_preserves
//	LIGHTS_BB_STRUCT
// 	xSemaphoreGiveRecursive( lights_preserves_recursive_MUTEX );
// 
//	xSemaphoreTakeRecursive( lights_report_completed_records_recursive_MUTEX, portMAX_DELAY );
//	lights_report_data_completed
//	COMPLETED_LIGHTS_REPORT_DATA_STRUCT
//	LIGHTS_REPORT_RECORD
//	xSemaphoreGiveRecursive( lights_report_completed_records_recursive_MUTEX );
// 
 
// 9/9/2015 mpd :  	This mutex protects all instances of the LIGHT_STRUCT. This lights-specific mutex 
// is used in place of "list_program_data_recursive_MUTEX".
extern xSemaphoreHandle		list_lights_recursive_MUTEX;

// 9/9/2015 mpd :  	This mutex protects the "foal_lights" FOAL_LIGHTS_BATTERY_BACKED_STRUCT.
extern xSemaphoreHandle		list_foal_lights_recursive_MUTEX;

// 9/9/2015 mpd :  	This mutex protects the "irri_lights" IRRI_LIGHTS.
extern xSemaphoreHandle		irri_lights_recursive_MUTEX;

// 9/9/2015 mpd :  	This mutex protects the "lights_preserves" LIGHTS_BB_STRUCT.
extern xSemaphoreHandle		lights_preserves_recursive_MUTEX;

// 9/9/2015 mpd :  	This mutex protects the "lights_report_data_completed" COMPLETED_LIGHTS_REPORT_DATA_STRUCT as well 
// as all instances of LIGHTS_REPORT_RECORD.
extern xSemaphoreHandle		lights_report_completed_records_recursive_MUTEX;

// ----------

// 2/4/2016 rmd : I'm going to introduce a sigle MUTEX that hopefully I can use for all
// MOISTURE_SENSOR list and preserves operations. Protecting both of those entities.
extern xSemaphoreHandle	moisture_sensor_items_recursive_MUTEX;

// 2/17/2016 rmd : This MUTEX is focused at the moisture_sensor_recorder records.
extern xSemaphoreHandle	moisture_sensor_recorder_recursive_MUTEX;

// 2/5/2018 Ryan : This mutex is used to protect WALK_THRU
extern xSemaphoreHandle walk_thru_recursive_MUTEX;

// ----------
// 02/22/2016 skc : This mutex protects the COMPLETED_BUDGET_REPORT_RECORDS_STRUCT.
extern xSemaphoreHandle		budget_report_completed_records_recursive_MUTEX;

extern xSemaphoreHandle		ci_message_list_MUTEX;

extern xSemaphoreHandle		code_distribution_control_structure_recursive_MUTEX;


extern xSemaphoreHandle		chain_sync_control_structure_recursive_MUTEX;


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern xSemaphoreHandle	router_hub_list_MUTEX;

extern xQueueHandle		router_hub_list_queue;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
////////////////////////////////
// System Queue Handles
////////////////////////////////

extern xQueueHandle		Contrast_and_Volume_Queue;

extern xQueueHandle		Epson_RTC_Queue;


extern xQueueHandle		Key_Scanner_Queue;

extern xQueueHandle		Key_To_Process_Queue;


extern xQueueHandle		FLASH_STORAGE_task_queue_0;

extern xQueueHandle		FLASH_STORAGE_task_queue_1;


extern xQueueHandle		Display_Command_Queue;


extern xQueueHandle		TD_CHECK_queue;


extern xQueueHandle		TPL_IN_event_queue;

extern xQueueHandle		TPL_OUT_event_queue;


extern xQueueHandle		COMM_MNGR_task_queue;

extern xQueueHandle		CODE_DISTRIBUTION_task_queue;

extern xQueueHandle		CONTROLLER_INITIATED_task_queue;


extern xQueueHandle		BYPASS_event_queue;

extern xQueueHandle     RADIO_TEST_task_queue;

// 8/28/2013 ajv : This queue is used for background calculations such as Finish
// Times and next scheduled irrigation.
extern xQueueHandle		Background_Calculation_Queue;

extern xQueueHandle		FTIMES_task_queue;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void elevate_priority_of_ISP_related_code_update_tasks( void );

extern void restore_priority_of_ISP_related_code_update_tasks( void );

extern void system_startup_task( void *pvParameters );


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /*STARTUP_H_*/

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

