/*  file = lcd_init.h                        2009.06.23  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_LCD_INIT_H
#define _INC_LCD_INIT_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"GuiLib.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// We do not actually allocate anything of this structure type. It is only a
// definition so that the GuiLib_DisplayBuf #define works correctly when
// computing the offset into the buffer area.
typedef struct
{
	UNS_8	db[ GuiConst_BYTE_LINES ][ GuiConst_BYTES_PR_LINE ];

} __DBUF_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// We do not actually allocate anything using this structure. It is only a
// definition so that the GuiLib_DisplayBuf #define works correctly when
// computing the offset into the buffer area.
#define		GuiLib_DisplayBuf	((__DBUF_STRUCT*)(guilib_display_ptr))->db

// TODO Fine-tune this value. 1024 bytes covers 6 controllers of 128 stations
// each, but doesn't cover 12 controllers of 64 stations each. This requires
// 1160 pixel rows. This may be the worst case, but I'm not sure. Therefore,
// some buffer's been added for now - specifically 1024 + 512.
#define		UTILITY_BUFFER_PIXEL_ROWS		1536

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void *guilib_display_ptr;

#ifdef	GUILIB_USE_DOUBLE_BUFFERING
 
	// 11/27/2012 rmd : For the library to double buffer into. The guilib library writes to this
	// buffer. Then on the call to the display refresh we copy from this buffer to the
	// primrary_display_buf.
	extern UNS_8 gui_lib_display_buf[ GuiConst_BYTE_LINES ][ GuiConst_BYTES_PR_LINE ]__attribute__((aligned(8)));

#endif

extern UNS_8 primary_display_buf[ GuiConst_BYTE_LINES ][ GuiConst_BYTES_PR_LINE ]__attribute__((aligned(8)));

extern UNS_8 utility_display_buf[ UTILITY_BUFFER_PIXEL_ROWS ][ GuiConst_BYTES_PR_LINE ]__attribute__((aligned(8)));

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_lcd_2( void );


extern void led_backlight_set_brightness( void );


extern void FDTO_set_guilib_to_write_to_the_primary_display_buffer( void );

extern BOOL_32 CS_DrawStr( const UNS_32 dot_row_origin, const INT_32 X, const INT_32 Y, const UNS_32 FontNo, const INT_32 CharSetSelector, char *String, const UNS_32 Alignment, const UNS_32 PsWriting, const UNS_32 Transparent, const UNS_32 Underlining, const INT_32 BackBoxSizeX, const INT_32 BackBoxSizeY1, const INT_32 BackBoxSizeY2, const UNS_32 BackBorderPixels, const UNS_32 ForeColor, const UNS_32 BackColor );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

