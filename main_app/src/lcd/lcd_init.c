/* file = lcd_init.c                           2011.04.29 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for memset
#include	<string.h>

#include	"lcd_init.h"

#include	"lpc3250_chip.h"

#include	"lpc32xx_clcdc_driver.h"

#include	"lpc32xx_clkpwr_driver.h"

#include	"lpc3xxx_spwm.h"

#include	"contrast_and_speaker_vol.h"

#include	"group_base_file.h"

#include	"screen_utils.h"

#include	"r_alerts.h"

#include	"configuration_controller.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// This is the pointer that we actually direct GuiLib to use when writing to the
// display buffer. With the use of a define and by changing where this pointer
// points to we can cause the gui library to write to any buffer we desire. We
// use this technique coupled with brute force copies from one buffer to another
// to achieve various display effects. Such as pixel by pixel scrolling and
// pop-up windows.
void *guilib_display_ptr;


#ifdef	GUILIB_USE_DOUBLE_BUFFERING

	UNS_8 gui_lib_display_buf[ GuiConst_BYTE_LINES ][ GuiConst_BYTES_PR_LINE ]__attribute__((aligned(8)));

#endif


// The normal place the gui library writes to.
// The frame buffer MUST be doubleword aligned for the LPC3250! Else it won't
// work properly.
UNS_8 primary_display_buf[ GuiConst_BYTE_LINES ][ GuiConst_BYTES_PR_LINE ]__attribute__((aligned(8)));

// The utility display buffer. Its size will have to be carefully choosen on an
// as need basis. We may actually decide to dynamically allocate this buffer.
// But for now it is permanently allocated.
UNS_8 utility_display_buf[ UTILITY_BUFFER_PIXEL_ROWS ][ GuiConst_BYTES_PR_LINE ]__attribute__((aligned(8)));

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/10/2013 rmd : A 686.3 hertz pwm frequency seems to produce flicker free backlight
// operation. Which is about a load value of 37 (when the clock is divided by 2).
#define LED_BACKLIGHT_PWM_BASE_FREQ		(37)

extern void led_backlight_set_brightness( void )
{
	// 6/10/2013 rmd : Support function for the variable brightness LED backlight supply.
	// Brightness driven by a PWM duty cycle. This function may be called from any task at any
	// time. There are no particular restrictions. During the PWM programming the scheduler is
	// suspended to prevent a context switch.
	
	// ----------

	SPWM_REGS_T 		*lpwm;

	lpwm = ((SPWM_REGS_T*)(PWM2_BASE));

	// ----------

	// 6/10/2013 rmd : To protect the hardware suspend the scheduler.
	vTaskSuspendAll();
	
	// ----------

	UNS_32 led_backlight_duty;

	led_backlight_duty = CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty();

	lpwm->pwm_ctrl = (SPWM_EN | (SPWM_RELOAD( LED_BACKLIGHT_PWM_BASE_FREQ ) | SPWM_DUTY((UNS_32)((((100 - led_backlight_duty) << 8) - led_backlight_duty) / 100))) );

	// ----------

	xTaskResumeAll();
	
	// ----------
}

/* ---------------------------------------------------------- */
static void init_backlight_pwm( void )
{
	// 6/10/2013 rmd : Support function for the variable brightness LED backlight supply.
	// Brightness driven by a PWM duty cycle.

	// ----------

	// 6/10/2013 rmd : To protect the hardware suspend the scheduler.
	vTaskSuspendAll();
	
	// ----------

	// Enable the PWM clock. Typical first step with any peripheral.
	clkpwr_clk_en_dis( CLKPWR_PWM2_CLK, 1 );
	
	// Select PERIPH_CLK for PWM2 and set divider to 2.
	clkpwr_setup_pwm( 2, 1, 2 );

	// ----------

	xTaskResumeAll();

	// ----------

	// 6/10/2013 rmd : Set brightness to the value stored in the config_c file. Which has been
	// read into memroy prior to this function call.
	led_backlight_set_brightness();
}

/* ---------------------------------------------------------- */
extern void init_lcd_1( void )
{
    INT_32 lcddev;

	// 12/21/2016 rmd : Shut 'er down if enabled (maybe needed for BrownOut)
	CLCDC->lcdctrl &= ~(CLCDC_LCDCTRL_ENABLE | CLCDC_LCDCTRL_PWR);

	// Set the output pin group
	clkpwr_setup_lcd( CLKPWR_LCDMUX_STN4M, 4 );

	// Enable the clock to LCD peripheral (HCLK_EN)
	clkpwr_clk_en_dis( CLKPWR_LCD_CLK, 1 );

	// start with blank display
	memset( primary_display_buf, 0xff, sizeof(primary_display_buf) );
	memset( utility_display_buf, 0xff, sizeof(utility_display_buf) );

	// ----------
	
	// Setup LCD parameters in the LCD controller
	lcddev = calsense_lcd_open( CLCDC, (INT_32)&calsense_4bit_grey16 );

	// Upper Panel Frame Base Address register
	lcd_ioctl( lcddev, LCD_SET_UP_FB, (INT_32)primary_display_buf );
	//lcd_ioctl( lcddev, LCD_SET_LW_FB, (INT_32)our_display_ptr );

	if( display_model_is == DISPLAY_MODEL_KYOCERA_KG057 )
	{
		// Load the grey scale palette.
		CLCDC->lcdpalette[ 0x00 ] = 0x00020000;
		CLCDC->lcdpalette[ 0x01 ] = 0x00060004;
		CLCDC->lcdpalette[ 0x02 ] = 0x000A0008;
		CLCDC->lcdpalette[ 0x03 ] = 0x000E000C;
		CLCDC->lcdpalette[ 0x04 ] = 0x00120010;
		CLCDC->lcdpalette[ 0x05 ] = 0x00160014;
		CLCDC->lcdpalette[ 0x06 ] = 0x001A0018;
		CLCDC->lcdpalette[ 0x07 ] = 0x001E001C;
	}
	else
	{
		// 8/2/2016 rmd : Works well for the new AZ Displays TFT black and white unit.
		CLCDC->lcdpalette[ 0x00 ] = 0x001E001E;
		CLCDC->lcdpalette[ 0x01 ] = 0x001E001E;
		CLCDC->lcdpalette[ 0x02 ] = 0x001E001E;
		CLCDC->lcdpalette[ 0x03 ] = 0x001E001E;
		CLCDC->lcdpalette[ 0x04 ] = 0x00000000;
		CLCDC->lcdpalette[ 0x05 ] = 0x00000000;
		CLCDC->lcdpalette[ 0x06 ] = 0x00000000;
		CLCDC->lcdpalette[ 0x07 ] = 0x00000000;
	}
	
	// ----------
	
	// 12/23/2016 rmd : Raise DISP. But the DISP output isn't driven until the ENABLE bit is
	// set.
	CLCDC->lcdctrl |= CLCDC_LCDCTRL_PWR;
	
	// 12/23/2016 rmd : Enable the outputs and begin clocking activity.
	CLCDC->lcdctrl |= CLCDC_LCDCTRL_ENABLE;

	// ----------
	
	/*
	// The following code fills the entire screen with equal sized bands of each of the
	// 16 shades of gray we have to work with.
	unsigned char	tmp_uc, *ucp;
	unsigned long	i_ul, j_ul;
	
	for( tmp_uc = 0,j_ul = 0; j_ul<16; tmp_uc++,j_ul++ )
	{
		ucp = ((unsigned char*)&secondary_display_buf) + (j_ul*( 15 * 320 / 2 ));
		
		for ( i_ul = 0; i_ul<(15*320/2); i_ul++ )
		{
			*ucp++ = (tmp_uc<<4) + tmp_uc;
		}
	} 
	*/
}

/* ---------------------------------------------------------- */
extern void init_lcd_2( void )
{
	init_lcd_1();
	
	// Synchronize the application of the bias voltage to the display till after the display controller is up and running.
	// So post the first two queue items to the contrast and speaker volume DAC.
	postContrast_or_Volume_Event( CVQC_POWERUP_DAC, 0 );  // Complete the DAC initialization.

	init_backlight_pwm();
	
	BACKLIGHT_ON;
}

/* ---------------------------------------------------------- */
/**
 * Set the GuiLib display pointer to point to the utility display buffer so
 * anything written by the easyGUI library is written to the utility buffer.
 * 
 * @executed Executed within the context of the display processing task when the
 * CS_DrawStr function is called.
 * 
 * @param dot_row_origin The dot row which indicates the top row of the utility
 *                       buffer as it relates to the display buffer.
 * 
 * @author 6/27/2011 BobD
 *
 * @revisions
 *    6/27/2011 Initial release
 *    2/16/2012 Changed to static function
 */
static void FDTO_set_guilib_to_write_to_the_utility_display_buffer( UNS_32 dot_row_origin )
{
	guilib_display_ptr = &utility_display_buf[ dot_row_origin ];
}

/* ---------------------------------------------------------- */
/**
 * Set the GuiLib display pointer to point to the primary display buffer so
 * anything written by the easyGUI library is written directly to the display.
 * 
 * @executed Executed within the context of the display processing task when the
 * task first starts, and when the CS_DrawStr function is called.
 *
 * @author 6/27/2011 BobD
 *
 * @revisions
 *    6/27/2011 Initial release
 */
extern void FDTO_set_guilib_to_write_to_the_primary_display_buffer( void )
{
	#ifdef	GUILIB_USE_DOUBLE_BUFFERING

		guilib_display_ptr = &gui_lib_display_buf;

	#else

		guilib_display_ptr = &primary_display_buf;

	#endif
}

/* ---------------------------------------------------------- */
/**
 * A wrapper for GuiLib_DrawStr which allows us to write information to the
 * utility buffer. This is currently only used when building the station
 * selection grid because anything beyond 48 stations exceeds the size of the
 * display buffer.
 * 
 * @executed Executed within the context of the display processing task when
 * drawing the station selection grid.
 * 
 * @param dot_row_origin The dot row which indicates the top row of the utility
 *                       buffer as it relates to the display buffer.
 * 
 * @param X The x-coordinate to write the string.
 * @param Y The y-coordinate to write the string.
 * @param FontNo The font index, such as GuiFont_ANSI7.
 * @param CharSetSelector The character set selection. This should always be -1
 *  					  to select the default character set.
 * @param String The string to write to the buffer.
 * @param Alignment The text alignment: GuiLib_ALIGN_LEFT, _CENTER, or _RIGHT.
 * @param PsWriting Whether to use proportional writing or not. Valid values
 *  				include: GuiLib_PS_OFF, _ON, or _NUM.
 * @param Transparent Whether to turn transparent writing off or not. Valid
 *                    values can be GuiLib_TRANSPARENT_OFF or _ON.
 * @param Underlining Whether to underline the text or not. Can be either
 *                    GuiLib_UNDERLINE_OFF or GuiLib_UNDERLINE_ON.
 * @param BackBoxSizeX Determines the width of the background box. 0 indicates
 *                     no background box.
 * @param BackBoxSizeY1 The height of the background box, measured from the font
 *                      baseline and up.
 * @param BackBoxSizeY2 Determines the height of the background box, measured
 *                      from the font baseline and down.
 * @param BackBorderPixels Border pixels for the background box. One extra pixel
 *  					   can be added to the background box on each of its
 *  				       edges. GuiLib_BBP_NONE adds no pixels. _LEFT, _RIGHT,
 *  					   _TOP, or _BOTTOM add an extra pixel edge to the
 *  					   specified edge. The last four settings can be
 *  					   combined by adding them together.
 * @param ForeColor The text color.
 * @param BackColor The color of the background box, if used.
 * 
 * @return BOOL_32 True if the text was written to the utility buffer; otherwise,
 *  	         false, indicating the caller tried to write past the end of the
 *  	         buffer.
 *
 * @author 8/3/2011 Adrianusv
 *
 * @revisions
 *    8/3/2011 Initial release
 */
extern BOOL_32 CS_DrawStr( const UNS_32 dot_row_origin,
						 const INT_32 X,
						 const INT_32 Y,
						 const UNS_32 FontNo,
						 const INT_32 CharSetSelector,
						 char *String,
						 const UNS_32 Alignment,
						 const UNS_32 PsWriting,
						 const UNS_32 Transparent,
						 const UNS_32 Underlining,
						 const INT_32 BackBoxSizeX,
						 const INT_32 BackBoxSizeY1,
						 const INT_32 BackBoxSizeY2,
						 const UNS_32 BackBorderPixels,
						 const UNS_32 ForeColor,
						 const UNS_32 BackColor)
{
	UNS_32	lFontHeight;

	BOOL_32	rv;

	rv = (false);

	switch (FontNo)
	{
		case GuiFont_ANSI7:
			lFontHeight = ANSI7_HEIGHT;
			break;

		case GuiFont_ANSI7Bold:
			lFontHeight = ANSI7_BOLD_HEIGHT;
			break;

		case GuiFont_Unicode9_15:
			lFontHeight = UNICODE9_15_HEIGHT;
			break;

		case GuiFont_Icon16x16:
			lFontHeight = 16;
			break;

		default:
			lFontHeight = 0;
	}

	if( lFontHeight > 0 )
	{
		if( (dot_row_origin + (lFontHeight + 1)) < UTILITY_BUFFER_PIXEL_ROWS )
		{
			FDTO_set_guilib_to_write_to_the_utility_display_buffer( dot_row_origin );

			GuiLib_DrawStr( (INT_16)X,
							(INT_16)Y,
							(UNS_16)FontNo,
							(INT_8)CharSetSelector,
							(char *)String,
							(UNS_8)Alignment,
							(UNS_8)PsWriting,
							(UNS_8)Transparent,
							(UNS_8)Underlining,
							(INT_16)BackBoxSizeX,
							(INT_16)BackBoxSizeY1,
							(INT_16)BackBoxSizeY2,
							(UNS_8)BackBorderPixels,
							(GuiConst_INTCOLOR)ForeColor,
							(GuiConst_INTCOLOR)BackColor );

			rv = (true);

			FDTO_set_guilib_to_write_to_the_primary_display_buffer();
		}
		else
		{
			Alert_Message_va( "Utility buffer too small: needs %lu; has %lu", (dot_row_origin + (lFontHeight + 1)), UTILITY_BUFFER_PIXEL_ROWS );
		}
	}
	else
	{
		ALERT_MESSAGE_WITH_FILE_NAME( "Function call with invalid font" );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif	// _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

