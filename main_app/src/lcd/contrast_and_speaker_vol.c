/*  file = contrast_and_speaker_vol.C        2011.04.20  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"contrast_and_speaker_vol.h"

#include	"lpc32xx_clkpwr_driver.h"

#include	"lpc32xx_i2c_driver.h"

#include	"lpc32xx_intc_driver.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"lcd_init.h"

#include	"flash_storage.h"

#include	"range_check.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct
{
	UNS_8				our_tx_fifo[ 8 ];

	UNS_32				remaining_to_xmit;

	volatile UNS_32		busy;

} CONTRAST_SPEAKER_I2C_CONTROL_STRUCT;

static volatile CONTRAST_SPEAKER_I2C_CONTROL_STRUCT		i2c2cs;  // LOCAL Control structure for the SECOND I2C channel.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/7/2013 rmd : The MAX5820LEUA part has an address of 0x70. The MEUA part address is
// 0xB0. As of Rev G pcb we are using the MEUA part.
#define	MAX5820_DAC_I2C_ADDR	0xB0

#define	I2C_WRITE_INDICATION	0x00
#define	I2C_READ_INDICATION		0x01

// DAC commands
#define DAC_UPDATE_OUTPUTS		0xE0
#define DAC_LOAD_VOLUME_DAC		0x40
#define DAC_LOAD_CONTRAST_DAC	0x50

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void calsense_i2c2_isr( void )
{
	I2C_REGS_T					*reg_ptr = (I2C_REGS_T*)I2C2_BASE;

	C_AND_V_QUEUE_EVENT_STRUCT	lcvqe;

	portBASE_TYPE				higher_priority_task_woken;



	lcvqe.value = reg_ptr->i2c_stat;

	if( (lcvqe.value & (I2C_NAI | I2C_AFI)) != 0 )
	{
		// could set up for re-transmissions here...but as we are the only device on the bus not necessary
		lcvqe.cmd = CVQC_FLAG_AN_ERROR;
        // Post to our queue. We don't really care if there was an error ie queue full because at least some of these messages
		// would make it to the alerts pile.
		xQueueSendToBackFromISR( Contrast_and_Volume_Queue, &lcvqe, &higher_priority_task_woken );

		// Restart the i2c machine. Disabling interrupts, clearing the status register and resetting the i2c state to idle.
		reg_ptr->i2c_ctrl = I2C_RESET;

		i2c2cs.busy = FALSE;  // free to use again
	}
	else
	if( (lcvqe.value & I2C_TDI) != 0)
	{
		// Clear the TDI bit. And reset the full i2c machine for that matter.
		reg_ptr->i2c_ctrl = I2C_RESET;

		i2c2cs.busy = FALSE;  // free to use again
	}
	else
	{
		// Feed the TX FIFO if needed.
		while( (i2c2cs.remaining_to_xmit > 0) && ((reg_ptr->i2c_stat & I2C_TFF) == 0) )
		{
			i2c2cs.remaining_to_xmit -= 1;

			if( i2c2cs.remaining_to_xmit == 0 )
			{
				reg_ptr->i2c_ctrl = ( I2C_NAIE | I2C_AFIE | I2C_TDIE );  // No longer need the FIFO available interrupts enabled.
				reg_ptr->i2c_txrx = I2C_STOP | i2c2cs.our_tx_fifo[ 0 ];
			}
			else
			{
				reg_ptr->i2c_txrx = i2c2cs.our_tx_fifo[ i2c2cs.remaining_to_xmit ];
			}
		}
	}
}

/* ---------------------------------------------------------- */
static void init_contrast_and_volume_i2c2_channel( void )
{
	I2C_REGS_T	*reg_ptr = (I2C_REGS_T*)I2C2_BASE;

	volatile UNS_32 tmp, high, low;

	clkpwr_clk_en_dis( CLKPWR_I2C2_CLK, 1 );

	// Setup a 200Khz 50% duty cycle channel.
	low = high = 50;
	tmp = clkpwr_get_base_clock_rate( CLKPWR_HCLK );
	reg_ptr->i2c_clk_lo = (low * (tmp / (high + low))) / (200000);
	reg_ptr->i2c_clk_hi = (high * (tmp / (high + low))) / (200000);

	clkpwr_set_i2c_driver( 2, I2C_PINS_HIGH_DRIVE );

	reg_ptr->i2c_ctrl = I2C_RESET;  // perform soft reset...self clearing

	i2c2cs.busy = FALSE;

	xDisable_ISR( I2C_2_INT );
	xSetISR_Vector( I2C_2_INT, INTERRUPT_PRIORITY_dac_i2c_channel, ISR_TRIGGER_LOW_LEVEL, calsense_i2c2_isr, NULL );
	// Do not need to enable the INT yet. That happens when the transmission is setup.
}

/* ---------------------------------------------------------- */
static void powerup_both_dac_channels( void )
{
	I2C_REGS_T	*reg_ptr = (I2C_REGS_T*)I2C2_BASE;
	
	// For some reason that I do not fully understand it seems to be very important (won't work properly otherwise) to
	// have the I2C channel interrupt DISABLED during the setup of the transmission. I don't know why they can't be enabled
	// in the ctrl register and then the write to the txrx reg cause an immediate interrupt. Intead we have to keep the MIC
	// disabled until the end. And that makes everything work A-OK.
	xDisable_ISR( I2C_2_INT );

	reg_ptr->i2c_ctrl = I2C_RESET;  // perform soft reset...self clearing
	
	// NOTE: they actually have to be loaded into our tx fifo in reverse! (Shown here in the order they arrive at the DAC.)
	i2c2cs.our_tx_fifo[ 1 ] = 0xF0;  // cmd
	i2c2cs.our_tx_fifo[ 0 ] = 0x0C;  // switch to operational mode
	
	i2c2cs.remaining_to_xmit = 2;
	
    i2c2cs.busy = TRUE;
	
	reg_ptr->i2c_ctrl = ( I2C_TFFIE | I2C_DRMIE | I2C_NAIE | I2C_AFIE | I2C_TDIE );  // Enable the interrupts...which are needed to complete the transmission.

	reg_ptr->i2c_txrx = (I2C_START | MAX5820_DAC_I2C_ADDR | I2C_WRITE_INDICATION);  // Send the FIRST byte. Interrupt driven transmission will commence.

	xEnable_ISR( I2C_2_INT );  // Enable interrupt in the mic for this channel.
}

/* ---------------------------------------------------------- */
static void write_command_and_value_to_dac( UNS_8 pcommand, UNS_32 pvalue )
{
	I2C_REGS_T	*reg_ptr = (I2C_REGS_T*)I2C2_BASE;
	
	UNS_8		byte_value = (UNS_8)pvalue;
	
	// For some reason that I do not fully understand it seems to be very important (won't work properly otherwise) to
	// have the I2C channel interrupt DISABLED during the setup of the transmission. I don't know why they can't be enabled
	// in the ctrl register and then the write to the txrx reg cause an immediate interrupt. Intead we have to keep the MIC
	// disabled until the end. And that makes everything work A-OK.
	xDisable_ISR( I2C_2_INT );

	reg_ptr->i2c_ctrl = I2C_RESET;  // perform soft reset...self clearing
	
	// NOTE: they actually have to be loaded into our tx fifo in reverse! (Shown here in the order they arrive at the DAC.)
	i2c2cs.our_tx_fifo[ 2 ] = (pcommand | (byte_value >> 4));
	i2c2cs.our_tx_fifo[ 1 ] = (UNS_8)(byte_value << 4);
	i2c2cs.our_tx_fifo[ 0 ] = (DAC_UPDATE_OUTPUTS);
	
	i2c2cs.remaining_to_xmit = 3;
	
	i2c2cs.busy = TRUE;
	
	reg_ptr->i2c_ctrl = ( I2C_TFFIE | I2C_DRMIE | I2C_NAIE | I2C_AFIE | I2C_TDIE );  // Enable the interrupts...which are needed to complete the transmission.
	
	reg_ptr->i2c_txrx = (I2C_START | MAX5820_DAC_I2C_ADDR | I2C_WRITE_INDICATION);  // Send the FIRST byte. Interrupt driven transmission will commence.

	xEnable_ISR( I2C_2_INT );  // Enable interrupt in the mic for this channel.
}

/* ---------------------------------------------------------- */
void contrast_and_speaker_volume_control_task( void *pvParameters )
{
	C_AND_V_QUEUE_EVENT_STRUCT	lcvqe;

	UNS_32	waited;

	// ----------
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32	task_index;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	// Kick off the hardware. Set the clock rate, drive the lines high, and idle the device.
	init_contrast_and_volume_i2c2_channel();

	// Do not issue the POWERUP command sequence to the DAC here. Either directly or by posting to the queue. We want to synchronize the application
	// of the bias voltage to the display till after the display controller is up and running. So post the first two queue items in the init_lcd function.

	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( Contrast_and_Volume_Queue, &lcvqe, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			// Sit tight for a bit if the hardware is busy. If we wait for too long go ahead anyway but flag the error.
			// For each sequence we send to the DAC we perform a soft RESET on the LPC3250 I2C state machine. So if there
			// is some sort of an issue at least the reset would allow us to start over.
			// 
			// Be AWARE. If we accidentally OVERFLOW the TX FIFO there will be a DATA ABORT exception generated so we really do
			// not want to allow this to happen. It ain't pretty!
			waited = 0;
			while( i2c2cs.busy == TRUE )
			{
				// 2012.01.26 rmd : Set to 1 TICK! So regardless of the tick rate we calc out to 1 tick.
				// Using the MS_to_TICKS macro can lead to a delay of 0 if the tick rate is slower than the
				// requested delay.
				vTaskDelay( 1 );  // These dac transactions are usually on the order of 300usec each. 1 TICK! NOT 1 MS.

				if( ++waited == 10 )
				{
					// So we waited 10 ms for the process to end gracefully. Flag an alert and move ahead.
					// Counting on the soft RESET to clean up the hardware.
					Alert_Message( "Unexpected C&V I2C delay!" );
					break;  // Git out of here! and try the i2c device again.
				}
			}

			switch( lcvqe.cmd )
			{
				case CVQC_FLAG_AN_ERROR:
					Alert_Message( "C & V I2C error detected by the isr" );
					break;
					
				case CVQC_POWERUP_DAC:
					powerup_both_dac_channels();
					break;

				case CVQC_SET_CONTRAST_DAC:
					write_command_and_value_to_dac( DAC_LOAD_CONTRAST_DAC, lcvqe.value );
					break;

				case CVQC_SET_VOLUME_DAC:
					write_command_and_value_to_dac( DAC_LOAD_VOLUME_DAC, lcvqe.value );
					break;

				default:
					break;
			}

		} 

		// ----------
		
		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------
		
	}  // of the forever task loop
	
}

/* ---------------------------------------------------------- */
void postContrast_or_Volume_Event( UNS_32 pcmd, UNS_32 pvalue )
{
	//	WARNING: THIS FUNCTION MUST NOT BE CALLED FROM AN INTERRUPT!

	C_AND_V_QUEUE_EVENT_STRUCT	lcvqe;

	lcvqe.cmd = pcmd;
	lcvqe.value = pvalue;

    // post the event using the queue handle for this serial port driver
	if ( xQueueSendToBack( Contrast_and_Volume_Queue,  &lcvqe, 0 ) != pdPASS )  // do not wait for queue space ... flag an error
	{
		Alert_Message( "Contrast and Volume QUEUE OVERFLOW!" );
    }
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static const char CONTRAST_AND_SPEAKER_VOL_FILENAME[] =	"CONTRAST_AND_SPEARK_VOL";

// The structure revision number. This is a number!
#define	CONTRAST_AND_SPEAKER_VOL_LATEST_FILE_REVISION	(0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/10/2013 rmd : Reasonable limits based upon the observed brightness.
#define LED_BACKLIGHT_MINIMUM_DUTY_CYCLE	(15)
#define LED_BACKLIGHT_DEFAULT_DUTY_CYCLE	(50)
#define LED_BACKLIGHT_MAXIMUM_DUTY_CYCLE	(90)

// ---------------------

#define CONTRAST_LIGHTEST	(130)
#define	CONTRAST_DEFAULT	(145)
#define CONTRAST_DARKEST	(160)

// ---------------------

// A DAC setting of 104 produces 1.3VDC to the amplifier volume control voltage input.
// The volume seems to stop increasing after around 1.2VDC.
#define VOLUME_MIN			(65)
#define	VOLUME_DEFAULT		(75)

// 11/11/2016 rmd : This used to be set to 95. BUT I believe we are over driving the speaker
// and blowing the coil. Result -> speaker blown. I'm limiting to 82. I can hear that just
// begins to back it down off of maximum volume. Turns out the last 10 some increments (85
// to 95) did almost nothing for speaker volume. But may have been pumping more energy into
// the coil for nothing. (I didn't use a scope ... just my guess based upon what I hear).
#define VOLUME_MAX			(82)

// ---------------------

#define	LANGUAGE_MIN		(GuiConst_LANGUAGE_ENGLISH)
#define	LANGUAGE_DEFAULT	(GuiConst_LANGUAGE_ENGLISH)
#define	LANGUAGE_MAX		(GuiConst_LANGUAGE_SPANISH)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

struct CONTRAST_AND_SPEAKER_VOL
{
	// 6/10/2013 rmd : The LED backlight brightness as a PWM duty cycle. Typically runs from 20%
	// to 80%.
	UNS_32					led_backlight_duty;

	// The lcd contrast the way the user left it last.
	UNS_32					lcd_contrast_value;

	// The speaker volume the way the user left it last.
	UNS_32					speaker_volume;

	// ---------------------

	// 3/24/2014 ajv : I'm not entirely sure why we store this. What's wrong
	// with defaulting it to English on power up?
	UNS_32					screen_language;
};

static CONTRAST_AND_SPEAKER_VOL c_and_sv;

/* ---------------------------------------------------------- */

static void CONTRAST_AND_SPEAKER_VOL_set_default_values( void );

/* ---------------------------------------------------------- */
static void contrast_and_speaker_volume_updater( UNS_32 pfrom_revision )
{
	// 1/26/2015 rmd : Called during startup within the context of the startup task. Only when
	// the file revision is changed. FYI there is no MUTEX on the controller configuration
	// structure.

	// ----------
	
	if( pfrom_revision == CONTRAST_AND_SPEAKER_VOL_LATEST_FILE_REVISION )
	{
		Alert_Message_va( "C & V file unexpd update %u", pfrom_revision );	
	}
	else
	{
		Alert_Message_va( "C & V file update : to revision %u from %u", CONTRAST_AND_SPEAKER_VOL_LATEST_FILE_REVISION, pfrom_revision );

		// ----------
		
		if( pfrom_revision == 0 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 0 to REVISION 1.

			
			// 1/30/2015 rmd : Done. Now bump the from revision so we process the from 1 to 2 changes if
			// needed.
			pfrom_revision += 1;
		}
		
		// ----------
		
		/*
		if( pfrom_revision == 1 )
		{
			// 1/23/2015 rmd : This is the work to do when moving from REVISION 1 to REVISION 2.
				


			pfrom_revision += 1;
		}
		*/
	}

	// ----------

	// 1/29/2015 rmd : Well after the updater has run the pfrom_revision is supposed to equal
	// the latest revision. By design of this update process.
	if( pfrom_revision != CONTRAST_AND_SPEAKER_VOL_LATEST_FILE_REVISION )
	{
		Alert_Message( "C & V updater error" );
	}
}

/* ---------------------------------------------------------- */
extern void init_file_contrast_and_speaker_vol( void )
{
	FLASH_FILE_find_or_create_variable_file(	FLASH_INDEX_1_GENERAL_STORAGE,
												CONTRAST_AND_SPEAKER_VOL_FILENAME,
												CONTRAST_AND_SPEAKER_VOL_LATEST_FILE_REVISION,
												&c_and_sv,
												sizeof( c_and_sv ),
												NULL,  // no MUTEX on this structure
												&contrast_and_speaker_volume_updater,
												&CONTRAST_AND_SPEAKER_VOL_set_default_values,
												FF_CONTRAST_AND_VOLUME );

	// 3/24/2014 ajv : Once we've read the file into memory, set the contrast
	// and volume levels accordingly.
	postContrast_or_Volume_Event( CVQC_SET_CONTRAST_DAC, CONTRAST_AND_SPEAKER_VOL_get_lcd_contrast_value() );
	
	postContrast_or_Volume_Event( CVQC_SET_VOLUME_DAC, CONTRAST_AND_SPEAKER_VOL_get_speaker_volume() );
}

/* ---------------------------------------------------------- */
extern void save_file_contrast_and_speaker_vol( void )
{
	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file( FLASH_INDEX_1_GENERAL_STORAGE,
															CONTRAST_AND_SPEAKER_VOL_FILENAME,
															CONTRAST_AND_SPEAKER_VOL_LATEST_FILE_REVISION,
															&c_and_sv,
															sizeof( c_and_sv ),
															NULL,
															FF_CONTRAST_AND_VOLUME );
}

/* ---------------------------------------------------------- */
static void CONTRAST_AND_SPEAKER_VOL_set_default_values( void )
{
	c_and_sv.led_backlight_duty = LED_BACKLIGHT_DEFAULT_DUTY_CYCLE;

	c_and_sv.lcd_contrast_value = CONTRAST_DEFAULT;

	c_and_sv.speaker_volume = VOLUME_DEFAULT;

	c_and_sv.screen_language = LANGUAGE_DEFAULT;
}

/* ---------------------------------------------------------- */
extern void CONTRAST_AND_SPEAKER_VOL__copy_settings_into_guivars( void )
{
	GuiVar_DisplayType = display_model_is;

	GuiVar_BacklightSliderPos = (((c_and_sv.led_backlight_duty - LED_BACKLIGHT_MINIMUM_DUTY_CYCLE) * 12) / 5);
	GuiVar_Backlight = c_and_sv.led_backlight_duty;

	GuiVar_ContrastSliderPos = ((c_and_sv.lcd_contrast_value - CONTRAST_LIGHTEST) * 6);
	GuiVar_Contrast = c_and_sv.lcd_contrast_value;

	GuiVar_VolumeSliderPos = ((c_and_sv.speaker_volume - VOLUME_MIN) * 11);
	GuiVar_Volume = c_and_sv.speaker_volume;
}

/* ---------------------------------------------------------- */
extern BOOL_32 LED_backlight_brighter( void )
{
	BOOL_32	rv;

	rv = (false);

	if( c_and_sv.led_backlight_duty < LED_BACKLIGHT_MAXIMUM_DUTY_CYCLE )
	{
		c_and_sv.led_backlight_duty += 5;

		led_backlight_set_brightness();

		rv = (true);
	}
		
	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 LED_backlight_darker( void )
{
	BOOL_32	rv;

	rv = (false);

	if( c_and_sv.led_backlight_duty > LED_BACKLIGHT_MINIMUM_DUTY_CYCLE )
	{
		c_and_sv.led_backlight_duty -= 5;

		led_backlight_set_brightness();
		
		rv = (true);
	}

	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 LCD_contrast_DARKEN( void )
{
	BOOL_32	rv;

	rv = (false);

	if( c_and_sv.lcd_contrast_value < CONTRAST_DARKEST )
	{
		++c_and_sv.lcd_contrast_value;

		postContrast_or_Volume_Event( CVQC_SET_CONTRAST_DAC, c_and_sv.lcd_contrast_value );
		
		rv = (true);
	}
		
	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 LCD_contrast_LIGHTEN( void )
{
	BOOL_32	rv;

	rv = (false);

	if( c_and_sv.lcd_contrast_value > CONTRAST_LIGHTEST )
	{
		--c_and_sv.lcd_contrast_value;

		postContrast_or_Volume_Event( CVQC_SET_CONTRAST_DAC, c_and_sv.lcd_contrast_value );
		
		rv = (true);
	}
		
	return( rv );
}

/* ---------------------------------------------------------- */
extern BOOL_32 SPEAKER_vol_increase( void )
{
	BOOL_32	rv;

	rv = (false);

	if( c_and_sv.speaker_volume < VOLUME_MAX )
	{
		++c_and_sv.speaker_volume;
		
		postContrast_or_Volume_Event( CVQC_SET_VOLUME_DAC, c_and_sv.speaker_volume );

		rv = (true);
	}

	return( rv );

}

/* ---------------------------------------------------------- */
extern BOOL_32 SPEAKER_vol_decrease( void )
{
	BOOL_32	rv;

	rv = (false);

	if( c_and_sv.speaker_volume > VOLUME_MIN )
	{
		--c_and_sv.speaker_volume;

		postContrast_or_Volume_Event( CVQC_SET_VOLUME_DAC, c_and_sv.speaker_volume );

		rv = (true);
	}

	return( rv );

}

/* ---------------------------------------------------------- */
extern UNS_32 CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty( void )
{
	if( !RangeCheck_uint32( &c_and_sv.led_backlight_duty, LED_BACKLIGHT_MINIMUM_DUTY_CYCLE, LED_BACKLIGHT_MAXIMUM_DUTY_CYCLE, LED_BACKLIGHT_DEFAULT_DUTY_CYCLE, NULL, "LED Backlight Duty" ) )
	{
		// 11/11/2016 rmd : Attempts to queue a file write. If no memory generates a postponed file
		// save. Trying again 5 seconds later.
		save_file_contrast_and_speaker_vol();
	}

	return( c_and_sv.led_backlight_duty );
}

/* ---------------------------------------------------------- */
extern UNS_32 CONTRAST_AND_SPEAKER_VOL_get_lcd_contrast_value( void )
{
	if( !RangeCheck_uint32( &c_and_sv.lcd_contrast_value, CONTRAST_LIGHTEST, CONTRAST_DARKEST, CONTRAST_DEFAULT, NULL, "LCD Contrast" ) )
	{
		// 11/11/2016 rmd : Attempts to queue a file write. If no memory generates a postponed file
		// save. Trying again 5 seconds later.
		save_file_contrast_and_speaker_vol();
	}

	return( c_and_sv.lcd_contrast_value );
}

/* ---------------------------------------------------------- */
extern UNS_32 CONTRAST_AND_SPEAKER_VOL_get_speaker_volume( void )
{
	// 11/11/2016 rmd : If the range check fails we should save the file. Else it will fail over
	// and over again on each reboot. Unless of course someone does something to trigger a file
	// save.
	if( !RangeCheck_uint32( &c_and_sv.speaker_volume, VOLUME_MIN, VOLUME_MAX, VOLUME_DEFAULT, NULL, "Speaker Volume" ) )
	{
		// 11/11/2016 rmd : Attempts to queue a file write. If no memory generates a postponed file
		// save. Trying again 5 seconds later.
		save_file_contrast_and_speaker_vol();
	}
	
	return( c_and_sv.speaker_volume );
}

/* ---------------------------------------------------------- */
extern UNS_32 CONTRAST_AND_SPEAKER_VOL_get_screen_language( void )
{
	if( !RangeCheck_uint32( &c_and_sv.screen_language, LANGUAGE_MIN, LANGUAGE_MAX, LANGUAGE_DEFAULT, NULL, "Screen Language" ) )
	{
		// 11/11/2016 rmd : Attempts to queue a file write. If no memory generates a postponed file
		// save. Trying again 5 seconds later.
		save_file_contrast_and_speaker_vol();
	}

	return( c_and_sv.screen_language );
}

/* ---------------------------------------------------------- */
extern void CONTRAST_AND_SPEAKER_VOL_set_screen_language( UNS_32 pnew_screen_language )
{
	if( ( pnew_screen_language >= LANGUAGE_MIN ) && ( pnew_screen_language <= LANGUAGE_MAX ) )
	{
		c_and_sv.screen_language = pnew_screen_language;
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

