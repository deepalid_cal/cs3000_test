/*  file = contrast_and_speaker_vol.h        2011.04.15  rmd  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_CONTRAST_AND_SPEAKER_VOL_H
#define _INC_CONTRAST_AND_SPEAKER_VOL_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define C_AND_V_NUMBER_OF_QUEUE_ITEMS	10

typedef struct
{
	UNS_32	cmd;

	UNS_32	value;

} C_AND_V_QUEUE_EVENT_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Contrast and Volume Queue Commands
#define CVQC_POWERUP_DAC			0x1111
#define CVQC_SET_CONTRAST_DAC		0x2222
#define CVQC_SET_VOLUME_DAC			0x3333
#define CVQC_FLAG_AN_ERROR			0x4444

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef struct CONTRAST_AND_SPEAKER_VOL CONTRAST_AND_SPEAKER_VOL;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

void contrast_and_speaker_volume_control_task( void *pvParameters );

// ---------------------

void postContrast_or_Volume_Event( UNS_32 pcmd, UNS_32 pvalue );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void init_file_contrast_and_speaker_vol( void );

extern void save_file_contrast_and_speaker_vol( void );

// ---------------------

extern void CONTRAST_AND_SPEAKER_VOL__copy_settings_into_guivars( void );

// ---------------------

extern BOOL_32 LED_backlight_brighter( void );

extern BOOL_32 LED_backlight_darker( void );


extern BOOL_32 LCD_contrast_DARKEN( void );

extern BOOL_32 LCD_contrast_LIGHTEN( void );


extern BOOL_32 SPEAKER_vol_increase( void );

extern BOOL_32 SPEAKER_vol_decrease( void );

// ---------------------

extern UNS_32 CONTRAST_AND_SPEAKER_VOL_get_led_backlight_duty( void );

extern UNS_32 CONTRAST_AND_SPEAKER_VOL_get_lcd_contrast_value( void );

extern UNS_32 CONTRAST_AND_SPEAKER_VOL_get_speaker_volume( void );

extern UNS_32 CONTRAST_AND_SPEAKER_VOL_get_screen_language( void );

extern void CONTRAST_AND_SPEAKER_VOL_set_screen_language( UNS_32 pnew_screen_language );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

