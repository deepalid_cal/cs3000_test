/***********************************************************************
 * $Id:: lpc_lcd_params.c 745 2008-05-13 19:59:29Z pdurgesh            $
 *
 * Project: Sharp LCD parameters
 *
 * Description:
 *     This file contains common LCD parameters used on all Sharp
 *     evaluation boards.
 *
 */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_lcd_params.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

const LCD_PARAM_T	calsense_4bit_grey16 =
{
	1,          /* Horizontal back porch */
	1,          /* Horizontal front porch */
	1,          /* HSYNC pulse width */
	320,        /* Pixels per line */
	0,          /* Vertical back porch */
	0,          /* Vertical front porch */
	1,          /* VSYNC pulse width */
	240,        /* Lines per panel */
	0,          /* Do not invert output enable (NA) */
	0,          /* Do not invert panel clock */
	0,          /* Do not invert HSYNC */
	0,          /* Do not invert VSYNC */
	1,          /* AC bias frequency */
	4,          /* Bits per pixel */
	//1250000,    /* Optimal clock rate (Hz) */ // This gives us a frame rate of 61.7 Hz ... too slow!
	1450000,    /* Optimal clock rate (Hz) */  // This gives us a frame rate of 72.3 Hz ... right on the money! 
	MONO_4BIT,  /* LCD panel type */
	0,          /* Dual panel display */
	0,          /* HRTFT CLS enable flag (NA) */
	0,          /* HRTFT SPS enable flag (NA) */
	0,          /* HRTFT LP to PS delay (NA) */
	0,          /* HRTFT polarity delay (NA) */
	0,          /* HRTFT LP delay (NA) */
	0,          /* HRTFT SPL delay (NA) */
	0           /* HRTFT SPL to CLKS delay (NA) */
};

#if 0
const LCD_PARAM_T	calsense_4bit_grey16 =
{
  1,          /* Horizontal back porch */
  1,          /* Horizontal front porch */
  1,          /* HSYNC pulse width */
  320,        /* Pixels per line */
  0,          /* Vertical back porch */
  0,          /* Vertical front porch */
  1,          /* VSYNC pulse width */
  240,        /* Lines per panel */
  0,          /* Do not invert output enable (NA) */
  0,          /* Do not invert panel clock */
  0,          /* Do not invert HSYNC */
  0,          /* Do not invert VSYNC */
  1,          /* AC bias frequency */
  4,          /* Bits per pixel */
  1250000,    /* Optimal clock rate (Hz) */
  MONO_4BIT,  /* LCD panel type */
  0,          /* Dual panel display */
  0,          /* HRTFT CLS enable flag (NA) */
  0,          /* HRTFT SPS enable flag (NA) */
  0,          /* HRTFT LP to PS delay (NA) */
  0,          /* HRTFT polarity delay (NA) */
  0,          /* HRTFT LP delay (NA) */
  0,          /* HRTFT SPL delay (NA) */
  0           /* HRTFT SPL to CLKS delay (NA) */
};
#endif
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifdef NOT_COMPILING_THESE_LCD_TYPES

//I've left them in the code base for reference.


const LCD_PARAM_T ea_QVGA_v2 =
{
  28,       /* Horizontal back porch */
  10,       /* Horizontal front porch */
  2,        /* HSYNC pulse width */
  240,      /* Pixels per line */
  3,        /* Vertical back porch */
  2,        /* Vertical front porch */
  2,        /* VSYNC pulse width */
  320,      /* Lines per panel */
  0,        /* Do not invert output enable */
  1,        /* Invert panel clock */
  1,        /* Invert HSYNC */
  1,        /* Invert VSYNC */
  1,        /* AC bias frequency (not used) */
  16,       /* Bits per pixel */
  /*5213000*/5000000/*176366*/,  /* Optimal clock rate (Hz) */
  TFT,   	  /* LCD panel type */
  0,        /* Single panel display */
  0,        /* HRTFT CLS enable flag */
  0,        /* HRTFT SPS enable flag */
  0,        /* HRTFT LP to PS delay */
  0,        /* HRTFT polarity delay */
  0,        /* HRTFT LP delay */
  0,        /* HRTFT SPL delay */
  0         /* HRTFT SPL to CLKS delay */
};

/* Hitachi TX09D71VM1CCA portrait mode display parameters */
const LCD_PARAM_T hitachi_tx09d71 =
{
  22,       /* Horizontal back porch */
  11,       /* Horizontal front porch */
  5,        /* HSYNC pulse width */
  240,      /* Pixels per line */
  2,        /* Vertical back porch */
  1,        /* Vertical front porch */
  2,        /* VSYNC pulse width */
  320,      /* Lines per panel */
  0,        /* Do not invert output enable */
  0,        /* Invert panel clock */
  1,        /* Invert HSYNC */
  1,        /* Do not invert VSYNC */
  1,        /* AC bias frequency (not used) */
  18,       /* Bits per pixel */
  5213000,  /* Optimal clock rate (Hz) */
  TFT,   	  /* LCD panel type */
  0,        /* Single panel display */
  0,        /* HRTFT CLS enable flag */
  0,        /* HRTFT SPS enable flag */
  0,        /* HRTFT LP to PS delay */
  0,        /* HRTFT polarity delay */
  0,        /* HRTFT LP delay */
  0,        /* HRTFT SPL delay */
  0         /* HRTFT SPL to CLKS delay */
};

/* Sharp LQ035 portrait mode ADTFT display parameters */
const LCD_PARAM_T sharp_lq035 =
{
  21,       /* Horizontal back porch */
  11,       /* Horizontal front porch */
  13,       /* HSYNC pulse width */
  240,      /* Pixels per line */
  3,        /* Vertical back porch */
  4,        /* Vertical front porch */
  2,        /* VSYNC pulse width */
  320,      /* Lines per panel */
  0,        /* Do not invert output enable */
  1,        /* Invert panel clock */
  1,        /* Invert HSYNC */
  0,        /* Do not invert VSYNC */
  1,        /* AC bias frequency (not used) */
  18,       /* Bits per pixel */
  5213000,  /* Optimal clock rate (Hz) */
  ADTFT,    /* LCD panel type */
  0,        /* Single panel display */
  1,        /* HRTFT CLS enable flag */
  1,        /* HRTFT SPS enable flag */
  9,        /* HRTFT LP to PS delay */
  3,        /* HRTFT polarity delay */
  14,       /* HRTFT LP delay */
  34,       /* HRTFT SPL delay */
  209       /* HRTFT SPL to CLKS delay */
};

/* Sharp LQ039 HRTFT display parameters */
const LCD_PARAM_T sharp_lq039 =
{
  21,       /* Horizontal back porch */
  11,       /* Horizontal front porch */
  13,       /* HSYNC pulse width */
  320,      /* Pixels per line */
  3,        /* Vertical back porch */
  4,        /* Vertical front porch */
  2,        /* VSYNC pulse width */
  240,      /* Lines per panel */
  0,        /* Do not invert output enable */
  1,        /* Invert panel clock */
  1,        /* Invert HSYNC */
  0,        /* Do not invert VSYNC */
  1,        /* AC bias frequency (not used) */
  18,       /* Bits per pixel */
  5213000,  /* Optimal clock rate (Hz) */
  HRTFT,    /* LCD panel type */
  0,        /* Single panel display */
  1,        /* HRTFT CLS enable flag */
  1,        /* HRTFT SPS enable flag */
  9,        /* HRTFT LP to PS delay */
  3,        /* HRTFT polarity delay */
  14,       /* HRTFT LP delay */
  34,       /* HRTFT SPL delay */
  209       /* HRTFT SPL to CLKS delay */
};

/* Sharp LQ050 TFT display parameters */
const LCD_PARAM_T sharp_lq050 =
{
  21,       /* Horizontal back porch */
  11,       /* Horizontal front porch */
  13,       /* HSYNC pulse width */
  320,      /* Pixels per line */
  8,        /* Vertical back porch */
  5,        /* Vertical front porch */
  2,        /* VSYNC pulse width */
  240,      /* Lines per panel */
  0,        /* Do not invert output enable */
  0,        /* Do not invert panel clock */
  1,        /* Invert HSYNC */
  0,        /* Do not invert VSYNC */
  1,        /* AC bias frequency (not used) */
  18,       /* Bits per pixel */
  5213000,  /* Optimal clock rate (Hz) */
  TFT,      /* LCD panel type (NA in TFT mode) */
  0,        /* Single panel display */
  0,        /* HRTFT CLS enable flag (NA in TFT mode) */
  0,        /* HRTFT SPS enable flag (NA in TFT mode) */
  0,        /* HRTFT LP to PS delay (NA in TFT mode) */
  0,        /* HRTFT polarity delay (NA in TFT mode) */
  0,        /* HRTFT LP delay (NA in TFT mode) */
  0,        /* HRTFT SPL delay (NA in TFT mode) */
  0         /* HRTFT SPL to CLKS delay (NA in TFT mode) */
};

/* Sharp LQ057 TFT display parameters */
const LCD_PARAM_T sharp_lq057 =
{
  21,       /* Horizontal back porch */
  11,       /* Horizontal front porch */
  13,       /* HSYNC pulse width */
  320,      /* Pixels per line */
  8,        /* Vertical back porch */
  5,        /* Vertical front porch */
  2,        /* VSYNC pulse width */
  240,      /* Lines per panel */
  0,        /* Do not invert output enable */
  0,        /* Do not invert panel clock */
  1,        /* Invert HSYNC */
  1,        /* Invert VSYNC */
  1,        /* AC bias frequency (not used) */
  18,       /* Bits per pixel */
  5213000,  /* Optimal clock rate (Hz) */
  TFT,      /* LCD panel type (NA in TFT mode) */
  0,        /* Single panel display */
  0,        /* HRTFT CLS enable flag (NA in TFT mode) */
  0,        /* HRTFT SPS enable flag (NA in TFT mode) */
  0,        /* HRTFT LP to PS delay (NA in TFT mode) */
  0,        /* HRTFT polarity delay (NA in TFT mode) */
  0,        /* HRTFT LP delay (NA in TFT mode) */
  0,        /* HRTFT SPL delay (NA in TFT mode) */
  0         /* HRTFT SPL to CLKS delay (NA in TFT mode) */
};

/* Sharp LQ064 TFT display parameters */
const LCD_PARAM_T sharp_lq064 =
{
  89,       /* Horizontal back porch */
  50,       /* Horizontal front porch */
  14,       /* HSYNC pulse width */
  640,      /* Pixels per line */
  18,       /* Vertical back porch */
  94,       /* Vertical front porch */
  17,       /* VSYNC pulse width */
  480,      /* Lines per panel */
  0,        /* Do not invert output enable */
  0,        /* Do not invert panel clock */
  1,        /* Invert HSYNC */
  1,        /* Invert VSYNC */
  1,        /* AC bias frequency (not used) */
  18,       /* Bits per pixel */
  25180000, /* Optimal clock rate (Hz) */
  TFT,      /* LCD panel type (NA in TFT mode) */
  0,        /* Single panel display */
  0,        /* HRTFT CLS enable flag (NA in TFT mode) */
  0,        /* HRTFT SPS enable flag (NA in TFT mode) */
  0,        /* HRTFT LP to PS delay (NA in TFT mode) */
  0,        /* HRTFT polarity delay (NA in TFT mode) */
  0,        /* HRTFT LP delay (NA in TFT mode) */
  0,        /* HRTFT SPL delay (NA in TFT mode) */
  0         /* HRTFT SPL to CLKS delay (NA in TFT mode) */
};
#if 0
//original values
/* Sharp LQ104 TFT display parameters */
const LCD_PARAM_T sharp_lq104 =
{
  89,       /* Horizontal back porch */
  169,      /* Horizontal front porch */
  129,      /* HSYNC pulse width */
  640,      /* Pixels per line */
  32,       /* Vertical back porch */
  242,      /* Vertical front porch */
  3,        /* VSYNC pulse width */
  480,      /* Lines per panel */
  0,        /* Do not invert output enable */
  0,        /* Do not invert panel clock */
  1,        /* Do not invert HSYNC */
  1,        /* Invert VSYNC */
  1,        /* AC bias frequency (not used) */
  18,       /* Bits per pixel */
  25180000, /* Optimal clock rate (Hz) */
  TFT,      /* LCD panel type (NA in TFT mode) */
  0,        /* Single panel display */
  0,        /* HRTFT CLS enable flag (NA in TFT mode) */
  0,        /* HRTFT SPS enable flag (NA in TFT mode) */
  0,        /* HRTFT LP to PS delay (NA in TFT mode) */
  0,        /* HRTFT polarity delay (NA in TFT mode) */
  0,        /* HRTFT LP delay (NA in TFT mode) */
  0,        /* HRTFT SPL delay (NA in TFT mode) */
  0         /* HRTFT SPL to CLKS delay (NA in TFT mode) */
};
#endif
/* Sharp LQ104 TFT display parameters */
const LCD_PARAM_T sharp_lq104 =
{
  79,       /* Horizontal back porch */
  150,      /* Horizontal front porch */
  129,      /* HSYNC pulse width */
  640,      /* Pixels per line */
  32,       /* Vertical back porch */
  242,      /* Vertical front porch */
  3,        /* VSYNC pulse width */
  480,      /* Lines per panel */
  0,        /* Do not invert output enable */
  0,        /* Do not invert panel clock */
  1,        /* Do not invert HSYNC */
  1,        /* Invert VSYNC */
  1,        /* AC bias frequency (not used) */
  18,       /* Bits per pixel */
  25180000, /* Optimal clock rate (Hz) */
  TFT,      /* LCD panel type (NA in TFT mode) */
  0,        /* Single panel display */
  0,        /* HRTFT CLS enable flag (NA in TFT mode) */
  0,        /* HRTFT SPS enable flag (NA in TFT mode) */
  0,        /* HRTFT LP to PS delay (NA in TFT mode) */
  0,        /* HRTFT polarity delay (NA in TFT mode) */
  0,        /* HRTFT LP delay (NA in TFT mode) */
  0,        /* HRTFT SPL delay (NA in TFT mode) */
  0         /* HRTFT SPL to CLKS delay (NA in TFT mode) */
};

/* Sharp LQ121 TFT display parameters */
const LCD_PARAM_T sharp_lq121 =
{
  89,       /* Horizontal back porch */
  169,      /* Horizontal front porch */
  129,      /* HSYNC pulse width */
  800,      /* Pixels per line */
  24,       /* Vertical back porch */
  44,       /* Vertical front porch */
  5,        /* VSYNC pulse width */
  600,      /* Lines per panel */
  0,        /* Do not invert output enable */
  0,        /* Do not invert panel clock */
  1,        /* Invert HSYNC */
  1,        /* Invert VSYNC */
  1,        /* AC bias frequency (not used) */
  18,       /* Bits per pixel */
  31680000, /* Optimal clock rate (Hz) */
  TFT,      /* LCD panel type (NA in TFT mode) */
  0,        /* Single panel display */
  0,        /* HRTFT CLS enable flag (NA in TFT mode) */
  0,        /* HRTFT SPS enable flag (NA in TFT mode) */
  0,        /* HRTFT LP to PS delay (NA in TFT mode) */
  0,        /* HRTFT polarity delay (NA in TFT mode) */
  0,        /* HRTFT LP delay (NA in TFT mode) */
  0,        /* HRTFT SPL delay (NA in TFT mode) */
  0         /* HRTFT SPL to CLKS delay (NA in TFT mode) */
};

/* Sharp LM10V DSTN display */
const LCD_PARAM_T sharp_lm10v =
{
  1,        /* Horizontal back porch */
  1,        /* Horizontal front porch */
  6,        /* HSYNC pulse width */
  640,      /* Pixels per line */
  1,        /* Vertical back porch */
  1,        /* Vertical front porch */
  2,        /* VSYNC pulse width */
  480,      /* Lines per panel */
  0,        /* Do not invert output enable (NA) */
  0,        /* Do not invert panel clock */
  0,        /* Do not invert HSYNC */
  0,        /* Do not invert VSYNC */
  21,       /* AC bias frequency */
  16,       /* Bits per pixel */
  12500000, /* Optimal clock rate (Hz) */
  CSTN,     /* LCD panel type */
  1,        /* Dual panel display */
  0,        /* HRTFT CLS enable flag (NA) */
  0,        /* HRTFT SPS enable flag (NA) */
  0,        /* HRTFT LP to PS delay (NA) */
  0,        /* HRTFT polarity delay (NA) */
  0,        /* HRTFT LP delay (NA) */
  0,        /* HRTFT SPL delay (NA) */
  0         /* HRTFT SPL to CLKS delay (NA) */
};

/* Sharp LM057QB STN display */
const LCD_PARAM_T sharp_lm057qb =
{
  5,          /* Horizontal back porch */
  5,          /* Horizontal front porch */
  3,          /* HSYNC pulse width */
  320,        /* Pixels per line */
  1,          /* Vertical back porch */
  1,          /* Vertical front porch */
  1,          /* VSYNC pulse width */
  240,        /* Lines per panel */
  0,          /* Do not invert output enable (NA) */
  0,          /* Do not invert panel clock */
  0,          /* Do not invert HSYNC */
  0,          /* Do not invert VSYNC */
  1,          /* AC bias frequency */
  4,          /* Bits per pixel */
  4500000,    /* Optimal clock rate (Hz) */
  MONO_4BIT,  /* LCD panel type */
  0,          /* Dual panel display */
  0,          /* HRTFT CLS enable flag (NA) */
  0,          /* HRTFT SPS enable flag (NA) */
  0,          /* HRTFT LP to PS delay (NA) */
  0,          /* HRTFT polarity delay (NA) */
  0,          /* HRTFT LP delay (NA) */
  0,          /* HRTFT SPL delay (NA) */
  0           /* HRTFT SPL to CLKS delay (NA) */
};

/* Sharp LM057QC STN display */
const LCD_PARAM_T sharp_lm057qc =
{
  5,          /* Horizontal back porch */
  5,          /* Horizontal front porch */
  3,          /* HSYNC pulse width */
  320,        /* Pixels per line */
  0,          /* Vertical back porch */
  0,          /* Vertical front porch */
  0,          /* VSYNC pulse width */
  240,        /* Lines per panel */
  0,          /* Do not invert output enable (NA) */
  0,          /* Do not invert panel clock */
  0,          /* Do not invert HSYNC */
  0,          /* Do not invert VSYNC */
  0,//1,          /* AC bias frequency */
  8,          /* Bits per pixel */
  2500000,    /* Optimal clock rate (Hz) */
  CSTN,       /* LCD panel type */
  0,          /* Dual panel display */
  0,          /* HRTFT CLS enable flag (NA) */
  0,          /* HRTFT SPS enable flag (NA) */
  0,          /* HRTFT LP to PS delay (NA) */
  0,          /* HRTFT polarity delay (NA) */
  0,          /* HRTFT LP delay (NA) */
  0,          /* HRTFT SPL delay (NA) */
  0           /* HRTFT SPL to CLKS delay (NA) */
};

/* Sharp LM64K11 STN display */
const LCD_PARAM_T sharp_lm64k11 =
{
  5,          /* Horizontal back porch */
  5,          /* Horizontal front porch */
  3,          /* HSYNC pulse width */
  640,        /* Pixels per line */
  1,          /* Vertical back porch */
  1,          /* Vertical front porch */
  1,          /* VSYNC pulse width */
  240,        /* Lines per panel */
  0,          /* Do not invert output enable (NA) */
  0,          /* Do not invert panel clock */
  0,          /* Do not invert HSYNC */
  0,          /* Do not invert VSYNC */
  1,          /* AC bias frequency */
  4,          /* Bits per pixel */
  4500000,    /* Optimal clock rate (Hz) */
  MONO_4BIT,  /* LCD panel type */
  1,          /* Dual panel display */
  0,          /* HRTFT CLS enable flag (NA) */
  0,          /* HRTFT SPS enable flag (NA) */
  0,          /* HRTFT LP to PS delay (NA) */
  0,          /* HRTFT polarity delay (NA) */
  0,          /* HRTFT LP delay (NA) */
  0,          /* HRTFT SPL delay (NA) */
  0           /* HRTFT SPL to CLKS delay (NA) */
};
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

