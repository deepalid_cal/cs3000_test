/* file = speaker.c                            2011.04.29 rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"speaker.h"

#include	"lpc3xxx_spwm_driver.h"

#include	"lpc3xxx_spwm.h"

#include	"lpc32xx_clkpwr_driver.h"

#include	"lpc32xx_timer.h"

#include	"lpc32xx_timer_driver.h"

#include	"lpc32xx_intc_driver.h"

#include	"app_startup.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/18/2015 ajv : Typically, the speaker is always enabled, allowing the controller to play
// good and bad key beeps when processing keys. However, when the keypad times out, we roll
// back to the Status screen by processing fake BACK keys. Since the processing of the BACK
// key plays a good key beep, this results in a series of beeps when the keypad times out.
// Therefore, we've introduced this static which allows the speaker to be disable and
// enabled when necessary.
static BOOL_32 speaker_is_enabled;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
static void speaker_isr( void )
{
	TIMER_CNTR_REGS_T	*ltimer;
	SPWM_REGS_T 	*lpwm;

	// Turn off the pwm.
	lpwm = ((SPWM_REGS_T*)(PWM1_BASE));
	lpwm->pwm_ctrl = 0x00;

	// Clear the timer ISR
	ltimer = ((TIMER_CNTR_REGS_T *)(TIMER4_BASE));
	ltimer->ir = 0xFF;  // Reset ALL sources of an interrupt in the timer. 
}

/* ---------------------------------------------------------- */
static void nm_start_speaker( const UNS_32 pwm_reload, const UNS_32 timer_match )
{
	TIMER_CNTR_REGS_T	*ltimer;

	SPWM_REGS_T 		*lpwm;
	

	// -------------- PWM ---------------
	lpwm = ((SPWM_REGS_T*)(PWM1_BASE));

	UNS_32	duty = 50;

	lpwm->pwm_ctrl = SPWM_RELOAD( pwm_reload ) | SPWM_DUTY((UNS_32)((((100 - duty) << 8) - duty) / 100));


	// -------------- TIMER_4 ---------------
	ltimer = SPEAKER_TIMER;

	ltimer->mr[ 0 ] = timer_match;  // Set the duration

	ltimer->ir = 0xFF;  // Clear ALL pending interrupts.

	ltimer->pc = 0;  // reset the counts
	ltimer->tc = 0;


	ltimer->tcr = TIMER_CNTR_TCR_EN;  // Enable the timer/counter.

	lpwm->pwm_ctrl |= SPWM_EN;  // Enable the PWM.
}

/* ---------------------------------------------------------- */
extern void init_speaker( void )
{
	// 8/2/2012 rmd : To protect the clocks and counters programmed within this function from
	// re-entrancy.
	xSemaphoreTake( speaker_hardware_MUTEX, portMAX_DELAY );

	// --------------
	
	// Initialize the Simple PWM hardware. And initialize Timer_4 hardware. We are using Timer_4 to manage the speaker sounds.
	// Just as a reminder OpenRTOS uses Timer_5 for the OS tick.

	// -------------- PWM ---------------
	// Enable the PWM clock. Typical first step with any peripheral.
	clkpwr_clk_en_dis( CLKPWR_PWM1_CLK, 1 );
	
	/* Select PERIPH_CLK for PWM1 and set divider to 1 */
	clkpwr_setup_pwm( 1, 1, 1 );

	SPWM_REGS_T 		*lpwm;

	lpwm = ((SPWM_REGS_T*)(PWM1_BASE));

	lpwm->pwm_ctrl = 0;  // Stop the PWM.


	// -------------- TIMER_4 ---------------
	TIMER_CNTR_REGS_T	*ltimer;

	//ltimer = (TIMER_CNTR_REGS_T *)(TIMER4_BASE);
	ltimer = SPEAKER_TIMER;


	clkpwr_clk_en_dis( SPEAKER_TIMER_CLK_EN, 1 );

	// Setup default timer state as standard timer mode, timer disabled and all match and counters disabled.
	ltimer->tcr = 0;  // timer control register - disabled
	ltimer->ctcr = TIMER_CNTR_SET_MODE(TIMER_CNTR_CTCR_TIMER_MODE);  // timer mode
	ltimer->ccr = 0;  // capture control register fully disabled
	ltimer->emr = (TIMER_CNTR_EMR_EMC_SET(0, TIMER_CNTR_EMR_NOTHING) | TIMER_CNTR_EMR_EMC_SET(1, TIMER_CNTR_EMR_NOTHING) | TIMER_CNTR_EMR_EMC_SET(2, TIMER_CNTR_EMR_NOTHING) | TIMER_CNTR_EMR_EMC_SET(3, TIMER_CNTR_EMR_NOTHING));

	// Clear pending interrupts and reset counts.
	ltimer->tc = 0;  // the count register itself
	ltimer->pc = 0;  // prescale counter register

	ltimer->mr[ 0 ] = 0;  // Zero all the match registers.
	ltimer->mr[ 1 ] = 0;
	ltimer->mr[ 2 ] = 0;
	ltimer->mr[ 3 ] = 0;

	ltimer->pr = 0;  // prescale register to 0
	ltimer->mcr = 0x05;  // Interrupt and Stop on match of MR_0

	ltimer->ir = 0xFF;  // Clear ALL pending interrupts.

	xDisable_ISR( SPEAKER_TIMER_INT );
	xSetISR_Vector( SPEAKER_TIMER_INT, INTERRUPT_PRIORITY_speaker_timer, ISR_TRIGGER_LOW_LEVEL, speaker_isr, NULL );
	xEnable_ISR( SPEAKER_TIMER_INT );

	// ----------

	// 5/18/2015 ajv : By default, ensure the speaker is always enabled.
	speaker_is_enabled = (true);

	// ----------
	
	xSemaphoreGive( speaker_hardware_MUTEX );
}

/* ---------------------------------------------------------- */
extern void speaker_enable( void )
{
	xSemaphoreTake( speaker_hardware_MUTEX, portMAX_DELAY );

	speaker_is_enabled = (true);
	
	xSemaphoreGive( speaker_hardware_MUTEX );
}

/* ---------------------------------------------------------- */
extern void speaker_disable( void )
{
	xSemaphoreTake( speaker_hardware_MUTEX, portMAX_DELAY );

	speaker_is_enabled = (false);

	xSemaphoreGive( speaker_hardware_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Produces our GOOD key beep sound.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed primarily within the context of the KEY_PROCESS task. However with
    the use of the speaker hardware MUTEX may actually be called from ANY task.
	
	@RETURN (none)

	@ORIGINAL 2012.08.03 rmd

	@REVISIONS (none)
*/
extern void good_key_beep( void )
{
	// 8/2/2012 rmd : To protect the clocks and counters programmed within this function from
	// re-entrancy.
	xSemaphoreTake( speaker_hardware_MUTEX, portMAX_DELAY );

	// 5/18/2015 ajv : Only play the good key beep if the speaker is enabled.
	if( speaker_is_enabled )
	{
		/*
			freq = 13000000/reload/256;
			
			therefore reload = 13000000/freq/256;
		*/
		/*
		if( speaker_freq > 250 )
		{
			// This gets us down to an absolute minimum frequency of 199 hz.
			clkpwr_setup_pwm( 1, 1, 1 );  // Select PERIPH_CLK for PWM1 and set divider to 1.
			pwm_reload = (13000000/speaker_freq)/256;
		}
		else
		if( speaker_freq > 100 )
		{
			// This gets us down to an absolute minimum frequency of 100 hz.
			clkpwr_setup_pwm( 1, 1, 2 );  // Select PERIPH_CLK for PWM1 and set divider to 1.
			pwm_reload = (6500000/speaker_freq)/256;
		}
		else
		{
			// This gets us down to an absolute minimum frequency of 50 hz.
			clkpwr_setup_pwm( 1, 1, 4 );  // Select PERIPH_CLK for PWM1 and set divider to 1.
			pwm_reload = (3250000/speaker_freq)/256;
		}
		*/

		// ----------
		
		clkpwr_setup_pwm( 1, 1, 1 );  // Select PERIPH_CLK for PWM1 and set divider to 1.

		UNS_32	pwm_reload, timer_match;

		// Have determined these two aren't bad for the good key beep.
		pwm_reload = 40;

		// 8/2/2012 rmd : Sound for 12 cycles at the frequency. Freq = (13MHz/(40*256)) = 1269Hz
		// (Set so that we have an integer number of pwm cycles)
		timer_match = pwm_reload * 256 * 12;

		// ----------
		
		// The PWM waveform first goes high then low. If we back the counter down by 25% of the waveform that gives us plenty of time to service the interrupt and prevent
		// a small edge from possibly showing if we were busy with another interrupt. Say from communications.
		//timer_match -= ((pwm_reload * 256) / 4);  // shave off 25% of the waveform (which is 50% of the low duration
		// And for some reason that didn't work. We got a piece of a high. But I don't have time to figure out why. So comment out the prior line. This seems to work just fine.

		nm_start_speaker( pwm_reload, timer_match );
	}

	xSemaphoreGive( speaker_hardware_MUTEX );
}

/* ---------------------------------------------------------- */
/**
    @DESCRIPTION Produces our BAD key beep sound.

	@CALLER_MUTEX_REQUIREMENTS (none)

	@MEMORY_RESPONSIBILITES (none)

    @EXECUTED Executed primarily within the context of the KEY_PROCESS task. However with
    the use of the speaker hardware MUTEX may actually be called from ANY task.
	
	@RETURN (none)

	@ORIGINAL 2012.08.03 rmd

	@REVISIONS (none)
*/
extern void bad_key_beep( void )
{
	// 8/2/2012 rmd : To protect the clocks and counters programmed within this function from
	// re-entrancy.
	xSemaphoreTake( speaker_hardware_MUTEX, portMAX_DELAY );

	// 5/18/2015 ajv : Only play the bad key beep if the speaker is enabled.
	if( speaker_is_enabled )
	{
		// 8/2/2012 rmd : Select PERIPH_CLK for PWM1 and set divider to 2. Making the PWM clock set
		// to 6.5Mhz. Necessary to get below 200 hz. This gets us down to an absolute minimum
		// frequency of 100 hz.
		clkpwr_setup_pwm( 1, 1, 2 );

		UNS_32	pwm_reload, timer_match;

		pwm_reload = 115;   // 220Hz is around 115   (6500000/220)/256;

		timer_match = (70 * 13000);		// Set the duration

		// ----------
		
		nm_start_speaker( pwm_reload, timer_match );
	}
	
	xSemaphoreGive( speaker_hardware_MUTEX );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

