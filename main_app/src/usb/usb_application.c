/* file = usb_application                    01.24.2011  rmd  */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"usb_application.h"

#include	"os.h"

#include	"usbd_cdcser_cfg.h"

#include	"os_common.h"

#include	"usbd.h"

#include	"usbd_cdcser.h"

#include	"serport_drvr.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"wdt_and_powerfail.h"

#include	"cs_mem.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

struct line_info_tag 
{
    /* State for the echo task. */
    enum
    {
		ST_INIT,
		ST_RUN,
		ST_NO_CONNECTION
    } state;
    
    /* Data buffer for the echo task. */
    struct 
    {
		char buffer[ 64 ];
        unsigned int bytes;
        enum
        {
			BST_FREE,
			BST_FULL,
			BST_INTX
        } state;
    } rx_buffer;
} lines[ N_LINES ];

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
int init_usb( void )
{

int	rv_int;

	rv_int = OS_SUCCESS;


	/* initialise USB device */
    if( rv_int == OS_SUCCESS ) 
	{
		rv_int = usbd_init( );         
	}
    
	/* initialise USB device mass-storage class */
	if( rv_int == OS_SUCCESS ) 
	{
		rv_int = cdcserd_init( );      
	}
    
	/* Start cdc class driver */
	if( rv_int == OS_SUCCESS ) 
	{
		rv_int = cdcserd_start( );     
	}
    
	/* Start USB device driver */
	if( rv_int == OS_SUCCESS ) 
	{
		rv_int = usbd_start( ( usbd_config_t* )&device_cfg_cdc_ser );  
	}

	if( rv_int == OS_SUCCESS ) 
    {
        /* Set usbd VBUS power enable. */
        usbd_vbus( 1 );
		
    }

    return( rv_int );
}

/*-----------------------------------------------------------*/
/*
 * Tell USB device driver if the board is currently running USB powered
 * or not.
 *
 */
int usbd_is_self_powered( void )
{
  /* always self powered */
  return( 1 );
}

/*-----------------------------------------------------------*/

/* This is a template for the usbd_conn_state callback function. 
 * This will be called from USB connection state callback function
 * each time connection transition is detected in the USB control task.
 */
void usbd_conn_state( usbd_conn_state_t new_state )
{
    switch(new_state)
    {
    case usbdcst_offline:
		/* cable not connected, stack stopped */
		break;
      /* cable not connected, stack started */
    case usbdcst_ready:
		break;
    case usbdcst_stopped:
		/* cable connected, stack stopped; bus powered device may draw 500uA from USB */
		break;
    case usbdcst_not_cfg:
		/* online, not configured; bus powered device may draw 10mA from USB */
		break;
    case usbdcst_cfg:
		/* configured; bus powered device may the maximum amount of current
		specified in the configuration descriptor */
		break;
    case usbdcst_suspend:
		/* suspended; bus powered device may draw 500uA from USB */
		break;
    case usbdcst_suspend_hic:
		/* suspended; bus powered device may draw 2.5 mA from USB */
		break;
	case usbdcst_invalid:
		/* suspended; bus powered device may draw 2.5 mA from USB */
		break;
    }
}
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
void cdc_echo_init( void )
{
    memset( &lines,0, sizeof( lines ) );
}

/*-----------------------------------------------------------*/
/* Application call-back. This shall be implemented by the application. 
   The function will be called by the CDC class-driver. */
void cdcserd_send_brk( unsigned short  interval )
{
	(void) interval;
}

/*-----------------------------------------------------------*/
void USB_rcvr_task( void *pvParameters )
{
	static int				line_ndx = 0;

	struct line_info_tag	*line;
	
	unsigned int			bytes_rcvd;

	UART_RING_BUFFER_s		*ptr_ring_buffer_s;

	char					*ucp;

	// ----------
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32	task_index;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------
	
	// This USB driver task is responsible for reception only. So initialize both the associated buffers.
	memset( (void*)&SerDrvrVars_s[ UPORT_USB ].UartRingBuffer_s, 0, sizeof(UART_RING_BUFFER_s) );  // reset ring buffer variables (zero everything)

	// initialize the accumulators
	memset( &SerDrvrVars_s[ UPORT_USB ].stats, 0, sizeof(UART_STATS_STRUCT) );


	// For our application there is only 1 lines. Therefore...
    line_ndx = 0;

	line = &lines[ 0 ];

	line->state = ST_INIT;


	for(;;)
	{
		switch( line->state )
		{
			case ST_INIT:
				line->rx_buffer.bytes = 0;
				line->rx_buffer.state = BST_FREE;
				line->state = ST_NO_CONNECTION;  // change of state
				break;
	
			case ST_NO_CONNECTION:
				if( cdcserd_present( line_ndx ) == TRUE )
				{
					line->state = ST_RUN;  // change of state
				}
				else
				{
					os_delay( MS_to_TICKS( 500 ) );  // while nobody is plugged in
				}
				break;
	
			case ST_RUN:
				while( line->state == ST_RUN )
				{
					ptr_ring_buffer_s = &SerDrvrVars_s[ UPORT_USB ].UartRingBuffer_s;

					// 2010.12.14 rmd
					// The cdcserd_receive function blocks till data is available. When data is streaming in another 64 byte chunk may become available about once
					// every 60 usec. This is pretty fast. About 8.5 Mbaud. But is probably dependent on many factors. It was this way on the system (target & PC &
					// installed win drivers) I was using.
					bytes_rcvd = cdcserd_receive( line_ndx, &line->rx_buffer.buffer, sizeof(line->rx_buffer.buffer) );
	
					if( bytes_rcvd > 0 )
					{
						ucp = (char*)&line->rx_buffer.buffer;
	
						// Update the stats to reflect what we are going to read.
						SerDrvrVars_s[ UPORT_USB ].stats.rcvd_bytes += bytes_rcvd;
						
						while( bytes_rcvd > 0 )
						{
							bytes_rcvd--;

                            ptr_ring_buffer_s->ring[ ptr_ring_buffer_s->next ] = *ucp++;

							ptr_ring_buffer_s->next++;
							if( ptr_ring_buffer_s->next == RING_BUFFER_SIZE )
							{
								ptr_ring_buffer_s->next = 0x00;
							}
	
							// if the tail ever catches up to index we've over run the buffer
							// so either make it bigger or check it more often
							// ONLY test for the mode we are active in cause for the inactive modes the tail will always eventually catch the index
							ptr_ring_buffer_s->ph_tail_caught_index = ( ptr_ring_buffer_s->hunt_for_packets && (ptr_ring_buffer_s->next == ptr_ring_buffer_s->ph.packet_index) );
							ptr_ring_buffer_s->dh_tail_caught_index = ( ptr_ring_buffer_s->hunt_for_data && (ptr_ring_buffer_s->next == ptr_ring_buffer_s->dh.data_index) );
							ptr_ring_buffer_s->sh_tail_caught_index = ( ptr_ring_buffer_s->hunt_for_specified_string && (ptr_ring_buffer_s->next == ptr_ring_buffer_s->sh.string_index) );
							ptr_ring_buffer_s->th_tail_caught_index = ( ptr_ring_buffer_s->hunt_for_specified_termination && (ptr_ring_buffer_s->next == ptr_ring_buffer_s->th.string_index) );
						}
	
						// signal that data has arrived
						xSemaphoreGive( rcvd_data_binary_semaphore[ UPORT_USB ] );
					}
	
                    if( cdcserd_present(line_ndx) == FALSE )
					{
						cdcserd_set_lsflags( line_ndx, 0 );  // don't know what this does
						line->state = ST_INIT;
					}
	
				}
				break;
	
		}  // of switch
	
	}  // of task
}

/*-----------------------------------------------------------*/
void USB_xmit_task( void *pvParameters )
{
	xQueueHandle			EvtQHandle;

	SERPORT_EVENT_DEFS		event;

	XMIT_CONTROL_LIST_ITEM	*llist_item;

	// ----------
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32	task_index;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------
	
	// This USB serial port driver task is responsible for the xmission. And as such we should
	// initialize the xmit list.
	xSemaphoreTake( xmit_list_MUTEX[ UPORT_USB ], portMAX_DELAY );

	nm_ListInit( &(xmit_cntrl[ UPORT_USB ].xlist), offsetof( XMIT_CONTROL_LIST_ITEM, dl ) );

	xSemaphoreGive( xmit_list_MUTEX[ UPORT_USB ] );
	
	// ----------

	// The state (though not directly used here) is important. It prevent another task from
	// trying to add to the xmit list until the list has been initialized (above).
	SerDrvrVars_s[ UPORT_USB ].SerportTaskState = UPORT_STATE_IDLE;  // not DISABLED

	// ----------

	// Get handle to our port's Event queue
	EvtQHandle = SerDrvrVars_s[ UPORT_USB ].SerportDrvrEventQHandle;

	if( EvtQHandle == NULL )
	{
		// Should never get here
		Alert_Message( "Event queue handle not valid!" );
		
		FREEZE_and_RESTART_APP;
	}
	else
	{
		while( (true) )
		{
			// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
			if( xQueueReceive( EvtQHandle, &event, MS_to_TICKS( 500 ) ) == pdPASS )
			{
				if( event == UPEV_NEW_TX_BLK_AVAIL )
				{
					// There should be a ONE for ONE correspondence. One NEW_BLK_AVAIL event for each new block. And we pick them up
					// and process them each. Completing each block before getting the next.

					if( cdcserd_present( 0 ) == (true) )  // Testing something about the port. But not sure what.
					{
						xSemaphoreTake( xmit_list_MUTEX[ UPORT_USB ], portMAX_DELAY );

						llist_item = nm_ListRemoveHead( &xmit_cntrl[ UPORT_USB ].xlist );
						
						xSemaphoreGive( xmit_list_MUTEX[ UPORT_USB ] );

						// Make sure there's something to send
						if( llist_item != NULL )
						{
							// ------------
							
							/*
							// 9/28/2012 rmd : OLD WAY. Notice no retry capability if the USB driver is busy. Not sure 
							retry is needed but we have alerts about it if a retry is ever performed.

							cdcserd_send( 0, llist_item->OriginalPtr, llist_item->Length );
							
							// Update the stats to reflect what we just sent.
							SerDrvrVars_s[ UPORT_USB ].stats.total_xmitted_bytes += llist_item->Length;
							
							*/

							// ------------
							
							BOOL_32	sucessful;
							
							UNS_32	attempts;
							
							sucessful = (false);

							for( attempts=0; attempts<10; attempts++ )
							{
								// 9/28/2012 rmd : The following usually returns when the data has been sent. However with
								// many packets back to back the driver becomes busy so this returns without success. Hence
								// we repeat.
								if( cdcserd_send( 0, llist_item->OriginalPtr, llist_item->Length ) == CDC_SUCCESS )
								{
									// Update the stats to reflect what we just sent.
									SerDrvrVars_s[ UPORT_USB ].stats.xmit_bytes += llist_item->Length;
									
									sucessful = (true);
									
									break;  // from the for loop
								}
								
								Alert_Message( "USB: needed to try again" );

								// 9/28/2012 rmd : Wait a bit to give the driver time to clear.
								vTaskDelay( MS_to_TICKS(10) );
							}
							
							// ------------
							
							if( !sucessful )
							{
								Alert_Message( "USB: xmit failed" );
							}
							
							// ------------
							
							// 9/28/2012 rmd : Always free the memory even if we didn't sucessfully send the block.
							mem_free( llist_item->OriginalPtr );
							mem_free( llist_item );
							
							// ------------
						}
						else
						{
							Alert_Message( "USB: list unexpectedly empty?" );
						}
					}
					else
					{
						Alert_Message( "USB: xmit rqst and port not available" );

						// Throw away the block of data.
						xSemaphoreTake( xmit_list_MUTEX[ UPORT_USB ], portMAX_DELAY );

						llist_item = nm_ListRemoveHead( &xmit_cntrl[ UPORT_USB ].xlist );

						xSemaphoreGive( xmit_list_MUTEX[ UPORT_USB ] );
						
						if( llist_item != NULL )
						{
							mem_free( llist_item->OriginalPtr );
							mem_free( llist_item );
						}
					}
				}
				else
				{
					Alert_Message( "USB: unexpected queue posting" );
				}

			}  // of queue returned a message

			// ----------
			
			// 10/23/2012 rmd : Keep the activity indication going.
			task_last_execution_stamp[ task_index ] = xTaskGetTickCount();
	
			// ----------
			
		}  // of for ever task loop

	}

}

/*-----------------------------------------------------------*/
BOOL_32 USB_device_is_active( void )
{
	// 2011.07.21 rmd
	// Returns a 1 if the device endpoints are valid. Does that mean it is plugged in? Don't
	// know. TODO need to test. We may also have more hardware capability (indicators) at the
	// MAXIM transceiver chip.
	return( cdcserd_present(0) );
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
/*
OS_TASK_FN( cdc_echo_task )
{
//Line index.
static int				line_ndx = 0;
struct line_info_tag	*line;
unsigned long			number_pending_ul;

	line_ndx++;
	if( line_ndx <= N_LINES )
	{
		line_ndx = 0;
	}

	line = lines + line_ndx;


	next_rcvd_index_ul = 0;
	copy_of_nri = 0;

	line->state = ST_INIT;

	for(;;)
	{
        switch( line->state )
		{
			case ST_INIT:
				line->rx_buffer.bytes = 0;
				line->rx_buffer.state = BST_FREE;
				line->state = ST_NO_CONNECTION;
                // and on to next case

			case ST_NO_CONNECTION:
				if( !cdcserd_present(line_ndx) )
				{
					os_delay( 100 );  // while nobody is home
					break;
				}
	
				line->state = ST_RUN;
                // and on to next case

			case ST_RUN:
				while( line->state == ST_RUN )
				{
					number_pending_ul = cdcserd_rx_chars( line_ndx );
	
					if( number_pending_ul )
					{
						// TODO : !!! could run out the top of the buffer if the pc side were to send a block bigger than our 1024 byte buffer (even though USB delivers 64 bytes at a time)
						// QUESTION: does USB driver deliver at this point 64 bytes at a time????
						next_rcvd_index_ul += cdcserd_receive( line_ndx, &(line->rx_buffer.buffer[ next_rcvd_index_ul ]), number_pending_ul );

                        os_delay( 3 );  // while a device is plugged in and we are actively receiving data
                    }
					else
					{
						// okay 5ms has gone by and nothing came in so send it

						if( next_rcvd_index_ul )
						{
							// okay some data was received so send it, then reset the buffer and leave this state
							// QUESTION: what happens to the potentially large block I hand off here?
                            cdcserd_send( line_ndx, line->rx_buffer.buffer, next_rcvd_index_ul );

							// TODO: what about error conditions when sending??? I don't know enough.
						}

						line->state = ST_INIT;
						cdcserd_set_lsflags( line_ndx, 0 );  // don't know what this does
						next_rcvd_index_ul = 0;
						os_delay( 25 );  // while a device is plugged in but we didn't see any new data
					}
                }
				break;

		}  // of switch

	}  // of task

}
*/



/*

ORIGINAL CODE

#if !OS_TASK_POLL_MODE
	for(;;)
	{
        os_delay( 10 );
		//os_delay( 1 );
#endif
		switch( line->state )
		{
			case st_init:
			line->rx_buffer.bytes = 0;
			line->rx_buffer.state = bst_free;
			line->state = st_no_connection;
			// to next state
			case st_no_connection:

			if( !cdcserd_present(line_ndx) )
			{
				os_delay( 10 );
				break;
			}

			line->state = st_run;

			// to next case
			case st_run:
			if ( !cdcserd_present( line_ndx ) )
			{
				line->state=st_init;
				cdcserd_set_lsflags( line_ndx, 0 );
				break;
			}

			pending=( int )cdcserd_rx_chars( line_ndx );
			if (bst_free == (line->rx_buffer.state && pending) )
			{
				line->rx_buffer.bytes = cdcserd_receive( line_ndx, line->rx_buffer.buffer, sizeof(line->rx_buffer.buffer) );
                line->rx_buffer.state = bst_full;
			}

			if( bst_full == line->rx_buffer.state )
			{
				if( cdcserd_send( line_ndx, line->rx_buffer.buffer, line->rx_buffer.bytes ) )
				{
					line->state = st_init;
					break;
				}
				line->rx_buffer.state = bst_intx;      	  
			}

			if( bst_intx == line->rx_buffer.state )
			{
				int r = cdcserd_get_tx_status( line_ndx );
				switch( r )
				{
					case USBDERR_NONE:
						line->rx_buffer.state = bst_free;
						break;
					case USBDERR_BUSY:
						break;
					default:
						line->rx_buffer.state = bst_full;
						line->state=st_init;
				}
			}
			break;
		}
#if !OS_TASK_POLL_MODE
    }
#endif

*/
