/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"dialog.h"

#include	"code_distribution_task.h"

#include	"cursor_utils.h"

#include	"d_comm_options.h"

#include	"d_copy_dialog.h"

#include	"d_device_exchange.h"

#include	"d_live_screens.h"

#include	"d_process.h"

#include	"d_tech_support.h"

#include	"d_two_wire_assignment.h"

#include	"d_two_wire_debug.h"

#include	"d_two_wire_discovery.h"

// 6/22/2017 ajv : The Hub List dialog is generated directly on the hub screen, not in a 
// separate file. 
#include	"e_hub.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32	g_DIALOG_cursor_position_when_dialog_displayed;

UNS_32			g_DIALOG_modal_result;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

UNS_32			g_DIALOG_ok_dialog_visible;

static UNS_32	g_DIALOG_yes_no_cancel_dialog_visible;

static UNS_32	g_DIALOG_yes_no_dialog_visible;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
* Determines whether one of the core dialogs (OK, Yes/No/Cancel, or Yes/No) is 
* currently displayed by examining the global _dialog_visible variables. 
* 
* @return BOOL_32 TRUE if one of the dialogs is visible; otherwise, FALSE.
*
* @author 11/29/2017 adrianusv
*
* @revisions (none)
*/
extern BOOL_32 DIALOG_a_dialog_is_visible( void )
{
	BOOL_32	rv;

	rv = (false);

	// ----------

	if( (g_DIALOG_ok_dialog_visible) ||
		(g_DIALOG_yes_no_cancel_dialog_visible) ||
		(g_DIALOG_yes_no_dialog_visible) )
	{
		rv = (true);
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_DIALOG_OK_DIALOG_OK		(99)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void FDTO_DIALOG_redraw_ok_dialog( void )
{
	GuiLib_ShowScreen( GuiLib_CurStructureNdx, CP_DIALOG_OK_DIALOG_OK, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Draws a dialog box with a single OK button. This is typically used for
 * warning messages.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task.
 * 
 * @param pstructure_to_draw The index of the structure to draw.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_DIALOG_draw_ok_dialog( const UNS_32 pstructure_to_draw )
{
	DIALOG_close_all_dialogs();

	g_DIALOG_ok_dialog_visible = (true);

	g_DIALOG_cursor_position_when_dialog_displayed = (UNS_32)GuiLib_ActiveCursorFieldNo;

	g_DIALOG_modal_result = MODAL_RESULT_NONE;

	GuiLib_ShowScreen( (UNS_16)pstructure_to_draw, CP_DIALOG_OK_DIALOG_OK, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void DIALOG_draw_ok_dialog( const UNS_32 pstructure_to_draw )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_DIALOG_draw_ok_dialog;
	lde._06_u32_argument1 = pstructure_to_draw;

	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
extern void DIALOG_close_ok_dialog( void )
{
	g_DIALOG_ok_dialog_visible = (false);

	// Return to the last cursor position the user was on when they
	// pressed BACK and redraw the screen to remove the dialog box.
	GuiLib_ActiveCursorFieldNo = (INT_16)g_DIALOG_cursor_position_when_dialog_displayed;

	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while an OK dialog box is active.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses a key while an OK dialog box is active.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void DIALOG_process_ok_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case KEY_ENG_SPA:
			KEY_process_ENG_SPA();

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_DIALOG_redraw_ok_dialog;
			Display_Post_Command( &lde );
			break;

		case KEY_SELECT:
		case KEY_BACK:
			// 10/18/2013 ajv : Don't allow the user to close the Downloading
			// Firmware dialog.
			if( GuiVar_CodeDownloadState != CODE_DOWNLOAD_STATE_IDLE )
			{
				bad_key_beep();
			}
			// 10/18/2013 ajv : Don't allow the user to leave the Stop dialog
			// while the pending message is displayed.
			else if( (GuiLib_CurStructureNdx == GuiStruct_dlgStop_0) && (GuiVar_StopKeyPending == (true)) )
			{
				bad_key_beep();
			}
			else if( GuiLib_CurStructureNdx == GuiStruct_dlgDiscoveringDecoders_0 )
			{
				FDTO_TWO_WIRE_close_discovery_dialog( pkey_event, &g_DIALOG_ok_dialog_visible );
			}
			else if( (GuiLib_CurStructureNdx == GuiStruct_dlgDeviceExchange_0) )
			{
				FDTO_DEVICE_EXCHANGE_close_dialog( pkey_event.keycode );
			}
			// 7/10/2018 Ryan : For finish times dialog box, select should not close the box, and the
			// back button should leave the finish times screen entirely.
			else if( GuiLib_CurStructureNdx == GuiStruct_dlgFinishTimesCalc_0 )
			{
				if( pkey_event.keycode == KEY_SELECT )
				{
					bad_key_beep();
				}
				// 7/19/2018 Ryan : Can't close dialog like other cases, because we are returning to a
				// different screen then it was called on, so set the g_DIALOG_ok_dialog_visible and
				// GuiLib_ActiveCursorFieldNo variables here instead.
				else
				{
					g_DIALOG_modal_result = MODAL_RESULT_OK;
	
					g_DIALOG_ok_dialog_visible = (false);

					GuiLib_ActiveCursorFieldNo = CP_SCHEDULE_MENU_FINISH_TIMES;
					
					GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;
					
					KEY_process_global_keys( pkey_event );				
				}
			}
			else
			{
				good_key_beep();

				g_DIALOG_modal_result = MODAL_RESULT_OK;

				DIALOG_close_ok_dialog();
			}
			break;

		case DEVICE_EXCHANGE_KEY_busy_try_again_later:
		case DEVICE_EXCHANGE_KEY_read_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_read_settings_error:
		case DEVICE_EXCHANGE_KEY_read_settings_in_progress:
		case DEVICE_EXCHANGE_KEY_write_settings_completed_ok:
		case DEVICE_EXCHANGE_KEY_write_settings_error:
		case DEVICE_EXCHANGE_KEY_write_settings_in_progress:
			// 5/18/2015 ajv : Allow the programming or diagnostic screen that started the device
			// exchange to process the device exchange result keys when a dialog is displayed.
			(ScreenHistory[ screen_history_index ].key_process_func_ptr)( pkey_event );
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_DIALOG_YES_NO_CANCEL_DIALOG_YES		(97)
#define	CP_DIALOG_YES_NO_CANCEL_DIALOG_NO		(98)
#define	CP_DIALOG_YES_NO_CANCEL_DIALOG_CANCEL	(99)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_DIALOG_redraw_yes_no_cancel_dialog( void )
{
	GuiLib_ShowScreen( GuiLib_CurStructureNdx, CP_DIALOG_YES_NO_CANCEL_DIALOG_YES, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void FDTO_DIALOG_draw_yes_no_cancel_dialog( const UNS_32 pstructure_to_draw )
{
	DIALOG_close_all_dialogs();

	g_DIALOG_yes_no_cancel_dialog_visible = (true);

	g_DIALOG_cursor_position_when_dialog_displayed = (UNS_32)GuiLib_ActiveCursorFieldNo;

	g_DIALOG_modal_result = MODAL_RESULT_NONE;

	GuiLib_ShowScreen( (UNS_16)pstructure_to_draw, CP_DIALOG_YES_NO_CANCEL_DIALOG_YES, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void DIALOG_draw_yes_no_cancel_dialog( const UNS_32 pstructure_to_draw )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_DIALOG_draw_yes_no_cancel_dialog;
	lde._06_u32_argument1 = pstructure_to_draw;

	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
static void DIALOG_close_yes_no_cancel_dialog( void )
{
	g_DIALOG_yes_no_cancel_dialog_visible = (false);

	// Return to the last cursor position the user was on when they
	// pressed BACK and redraw the screen to remove the dialog box.
	GuiLib_ActiveCursorFieldNo = (INT_16)g_DIALOG_cursor_position_when_dialog_displayed;

	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while an OK dialog box is active.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses a key while an OK dialog box is active.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void DIALOG_process_yes_no_cancel_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, void (*callback_func_ptr)( void ) )
{
	DISPLAY_EVENT_STRUCT	lde;

	// ----------

	switch( pkey_event.keycode )
	{
		case KEY_ENG_SPA:
			KEY_process_ENG_SPA();

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_DIALOG_redraw_yes_no_cancel_dialog;
			Display_Post_Command( &lde );
			break;

		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_DIALOG_YES_NO_CANCEL_DIALOG_YES:
					g_DIALOG_modal_result = MODAL_RESULT_YES;
					break;

				case CP_DIALOG_YES_NO_CANCEL_DIALOG_NO:
					g_DIALOG_modal_result = MODAL_RESULT_NO;
					break;

				case CP_DIALOG_YES_NO_CANCEL_DIALOG_CANCEL:
					g_DIALOG_modal_result = MODAL_RESULT_CANCEL;
					break;
			}

			if( g_DIALOG_modal_result != MODAL_RESULT_NONE )
			{
				good_key_beep();

				if( callback_func_ptr != NULL )
				{
					(callback_func_ptr)();
				}

				DIALOG_close_yes_no_cancel_dialog();
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_BACK:
			good_key_beep();

			g_DIALOG_modal_result = MODAL_RESULT_CANCEL;

			if( callback_func_ptr != NULL )
			{
				(callback_func_ptr)();
			}

			DIALOG_close_yes_no_cancel_dialog();
			break;

		case KEY_C_LEFT:
			CURSOR_Up( (true) );
			break;

		case KEY_C_RIGHT:
			CURSOR_Down( (true) );
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	CP_DIALOG_YES_NO_DIALOG_YES		(97)
#define	CP_DIALOG_YES_NO_DIALOG_NO		(98)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_DIALOG_redraw_yes_no_dialog( void )
{
	GuiLib_ShowScreen( GuiLib_CurStructureNdx, CP_DIALOG_YES_NO_DIALOG_YES, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
static void FDTO_DIALOG_draw_yes_no_dialog( const UNS_32 pstructure_to_draw )
{
	DIALOG_close_all_dialogs();

	g_DIALOG_yes_no_dialog_visible = (true);

	g_DIALOG_cursor_position_when_dialog_displayed = (UNS_32)GuiLib_ActiveCursorFieldNo;

	g_DIALOG_modal_result = MODAL_RESULT_NONE;

	GuiLib_ShowScreen( (UNS_16)pstructure_to_draw, CP_DIALOG_YES_NO_DIALOG_YES, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void DIALOG_draw_yes_no_dialog( const UNS_32 pstructure_to_draw )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_DIALOG_draw_yes_no_dialog;
	lde._06_u32_argument1 = pstructure_to_draw;

	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
static void DIALOG_close_yes_no_dialog( void )
{
	g_DIALOG_yes_no_dialog_visible = (false);

	// Return to the last cursor position the user was on when they
	// pressed BACK and redraw the screen to remove the dialog box.
	GuiLib_ActiveCursorFieldNo = (INT_16)g_DIALOG_cursor_position_when_dialog_displayed;

	Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
/**
 * Processes any keys pressed while an OK dialog box is active.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user presses a key while an OK dialog box is active.
 * 
 * @param pkey_event The key event to be processed.
 *
 * @author 10/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void DIALOG_process_yes_no_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, void (*callback_func_ptr)( void ) )
{
	DISPLAY_EVENT_STRUCT	lde;

	// ----------

	switch( pkey_event.keycode )
	{
		case KEY_ENG_SPA:
			KEY_process_ENG_SPA();

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = (void*)&FDTO_DIALOG_redraw_yes_no_dialog;
			Display_Post_Command( &lde );
			break;

		case KEY_SELECT:
			switch( GuiLib_ActiveCursorFieldNo )
			{
				case CP_DIALOG_YES_NO_DIALOG_YES:
					g_DIALOG_modal_result = MODAL_RESULT_YES;
					break;

				case CP_DIALOG_YES_NO_DIALOG_NO:
					g_DIALOG_modal_result = MODAL_RESULT_NO;
					break;
			}

			if( g_DIALOG_modal_result != MODAL_RESULT_NONE )
			{
				good_key_beep();

				if( callback_func_ptr != NULL )
				{
					(callback_func_ptr)();
				}

				DIALOG_close_yes_no_dialog();
			}
			else
			{
				bad_key_beep();
			}
			break;

		case KEY_BACK:
			good_key_beep();

			g_DIALOG_modal_result = MODAL_RESULT_NO;

			if( callback_func_ptr != NULL )
			{
				(callback_func_ptr)();
			}

			DIALOG_close_yes_no_dialog();
			break;

		case KEY_C_LEFT:
			CURSOR_Up( (true) );
			break;

		case KEY_C_RIGHT:
			CURSOR_Down( (true) );
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void DIALOG_close_all_dialogs( void )
{
	if( g_DIALOG_ok_dialog_visible )
	{
		DIALOG_close_ok_dialog();
	}
	else if( g_DIALOG_yes_no_cancel_dialog_visible )
	{
		DIALOG_close_yes_no_cancel_dialog();
	}
	else if( g_DIALOG_yes_no_dialog_visible )
	{
		DIALOG_close_yes_no_dialog();
	}
	else if( g_COMM_OPTIONS_dialog_visible )
	{
		g_COMM_OPTIONS_dialog_visible = (false);

		// 3/13/2014 ajv : Return to the last cursor position the user was
		// on when they pressed BACK and redraw the screen to remove the
		// dialog box.
		GuiLib_ActiveCursorFieldNo = (INT_16)g_COMM_OPTIONS_cursor_position_when_dialog_displayed;

		Redraw_Screen( (false) );
	}
	else if( g_COPY_DIALOG_visible )
	{
		g_COPY_DIALOG_visible = (false);

		Redraw_Screen( (false) );
	}
	else if( g_LIVE_SCREENS_dialog_visible )
	{
		g_LIVE_SCREENS_dialog_visible = (false);

		// 3/16/2016 ajv : Return to the last cursor position the user was on when they pressed BACK
		// and redraw the screen to remove the dialog box.
		GuiLib_ActiveCursorFieldNo = (INT_16)g_LIVE_SCREENS_cursor_position_when_dialog_displayed;

		Redraw_Screen( (false) );
	}
	else if( g_TWO_WIRE_dialog_visible )
	{
		g_TWO_WIRE_dialog_visible = (false);

		// 3/13/2014 ajv : Return to the last cursor position the user was
		// on when they pressed BACK and redraw the screen to remove the
		// dialog box.
		GuiLib_ActiveCursorFieldNo = (INT_16)g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_displayed;

		Redraw_Screen( (false) );
	}
	else if( g_TWO_WIRE_DEBUG_dialog_visible )
	{
		g_TWO_WIRE_DEBUG_dialog_visible = (false);

		// 3/13/2014 ajv : Return to the last cursor position the user was on when they pressed BACK
		// and redraw the screen to remove the dialog box.
		GuiLib_ActiveCursorFieldNo = (INT_16)g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed;

		Redraw_Screen( (false) );
	}
	else if( g_TECH_SUPPORT_dialog_visible )
	{
		g_TECH_SUPPORT_dialog_visible = (false);

		// 3/13/2014 ajv : Return to the last cursor position the user was on when they pressed BACK
		// and redraw the screen to remove the dialog box.
		GuiLib_ActiveCursorFieldNo = (INT_16)g_TECH_SUPPORT_cursor_position_when_dialog_displayed;

		Redraw_Screen( (false) );
	}
	else if( g_HUB_dialog_visible )
	{
		g_HUB_dialog_visible = (false);

		// 6/22/2017 ajv : Return to the last cursor position the user was on when they pressed BACK
		// and redraw the screen to remove the dialog box.
		GuiLib_ActiveCursorFieldNo = (INT_16)g_HUB_cursor_position_when_dialog_displayed;

		Redraw_Screen( (false) );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

