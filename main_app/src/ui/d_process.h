/*  file = d_process.h    2009.05.21  rmd                     */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_D_PROCESS_H
#define _INC_D_PROCESS_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"screen_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// C startup code will initialize to FALSE. Not set TRUE until after
// Display_Processing_Task is created and runs. Initializing the GuiLib Library
// itself and finally when truly ready for use sets this variable TRUE.
// Primarily introduced for use during detected, early startup, error
// conditions. We don't want to write to the display and makes things worse.
// Side tracking use from the real error at hand.
extern BOOL_32	GuiLib_okay_to_use;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void Display_Post_Command( DISPLAY_EVENT_STRUCT *de_ptr );

extern void Display_Processing_Task( void *pvParameters );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif /* D_PROCESS_H */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

