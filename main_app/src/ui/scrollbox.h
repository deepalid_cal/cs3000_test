/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_INC_SCROLLBOX_H
#define	_INC_SCROLLBOX_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED	(false)
#define SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED		(true)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_SCROLL_BOX_to_line( const UNS_32 pscrollbox_index, const UNS_32 pnew_scroll_line );

extern void FDTO_SCROLL_BOX_redraw_retaining_topline( const UNS_32 pscrollbox_index, const UNS_32 pnumber_of_lines, const BOOL_32 predraw_entire_scrollbox );



extern void SCROLL_BOX_redraw( const UNS_32 pscrollbox_index );

extern void SCROLL_BOX_redraw_line( const UNS_32 pscrollbox_index, const UNS_32 pline_index );

extern void SCROLL_BOX_up_or_down( const UNS_32 pscrollbox_index, const UNS_32 pkeycode );

extern void SCROLL_BOX_to_line( const UNS_32 pscrollbox_index, const UNS_32 pline_index );



extern void SCROLL_BOX_close_all_scroll_boxes( void );



extern void SCROLL_BOX_toggle_scroll_box_automatic_redraw( const BOOL_32 pallow_redraw );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

