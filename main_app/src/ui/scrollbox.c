/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"scrollbox.h"

#include	"cs_common.h"

#include	"d_process.h"

#include	"dialog.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	SCROLL_BOX_DELAY_REDRAW		vTaskDelay( MS_to_TICKS(5) )

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/**
 * 10/24/2013 ajv : A global flag which is used to deterine whether the call the
 * GuiLib_ScrollBox_Redraw and GuiLib_Refresh routines after moving the active
 * scroll line. This is used when you want to move the active line without
 * actually highlighting it.
 *  
 * <b>Note:</b> This is variable must ONLY be changed while in the context of 
 * the DISPLAY PROCESSING task. Attempts to change it outside of this task will 
 * result in unexpected behavior. To ensure this, the variable is declared as 
 * static and only the SCROLL_BOX_toggle_scroll_box_automatic_redraw routine has 
 * the ability to change it. 
 *  
 * @example 
 *   To prevent the scroll box from being redrawn, effectively hiding the scroll
 *   bar marker:<br><br>
 *   <code>
 *     SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );
 *   </code><br>
 *   <code>
 *     <...>
 *   </code><br>
 *   <code>
 *     SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
 *   </code>
 */
static BOOL_32	g_SCROLL_BOX_prevent_redraw;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Calls the GuiLib_ScrollBox_Down routine and performs routine clean-up after
 * the call. If the move is successful, a good key beep is played. The bad key
 * beep will play if there are no scroll items in the list, there's only one
 * item in the list, or we're already at the last item in the list.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses the down key to scroll down a scroll box.
 * 
 * @param pscrollbox_index The scroll box index to redraw. This should always be
 * 0 unless there is more than one scroll box on the screen.
 *
 * @author 8/6/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_SCROLL_BOX_down( const UNS_32 pscrollbox_index )
{
	// Move the scroll line down. 
	if( GuiLib_ScrollBox_Down( (UNS_8)pscrollbox_index ) == (true) )
	{
		good_key_beep();

		if( g_SCROLL_BOX_prevent_redraw == (false) )
		{
			// 10/30/2013 ajv : Refresh the scroll box manually to ensure it's
			// updated real-time for the user.
			GuiLib_ScrollBox_Redraw( (UNS_8)pscrollbox_index );
			GuiLib_Refresh();
		}

		SCROLL_BOX_DELAY_REDRAW;
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Calls the GuiLib_ScrollBox_Up routine and performs routine clean-up after
 * the call. If the move is successful, a good key beep is played. The bad key
 * beep will play if there are no scroll items in the list, there's only one
 * item in the list, or we're already at the first item in the list.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses the down key to scroll down a scroll box.
 * 
 * @param pscrollbox_index The scroll box index to redraw. This should always be
 * 0 unless there is more than one scroll box on the screen.
 *
 * @author 8/6/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_SCROLL_BOX_up( const UNS_32 pscrollbox_index )
{
	// Move the scroll line down.
	if( GuiLib_ScrollBox_Up( (UNS_8)pscrollbox_index ) == (true) )
	{
		good_key_beep();

		if( g_SCROLL_BOX_prevent_redraw == (false) )
		{
			// 10/30/2013 ajv : Refresh the scroll box manually to ensure it's
			// updated real-time for the user.
			GuiLib_ScrollBox_Redraw( (UNS_8)pscrollbox_index );
			GuiLib_Refresh();
		}

		SCROLL_BOX_DELAY_REDRAW;
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Calls the GuiLib_ScrollBox_To_Line routine to move to the specified line and
 * redraws the cursor positions. If the move is successful, that is the line the
 * user is moving to exists, a good key beep is played; otherwise, a bad key
 * beep is played.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task.
 * 
 * @param pscrollbox_index The scroll box index to redraw. This should always be
 * 0 unless there is more than one scroll box on the screen.
 *
 * @author 8/6/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_SCROLL_BOX_to_line( const UNS_32 pscrollbox_index, const UNS_32 pnew_scroll_line )
{
	if( GuiLib_ScrollBox_To_Line( (UNS_8)pscrollbox_index, (UNS_16)pnew_scroll_line ) == (true) )
	{
		if( g_SCROLL_BOX_prevent_redraw == (false) )
		{
			// Reset the horizontal scroll position in case the user has scrolled
			// right.
			GuiVar_ScrollBoxHorizScrollPos = 0;

			// 10/30/2013 ajv : Refresh the scroll box manually to ensure it's
			// updated real-time for the user.
			GuiLib_ScrollBox_Redraw( (UNS_8)pscrollbox_index );
			GuiLib_Refresh();
		}

		SCROLL_BOX_DELAY_REDRAW;
	}
	else
	{
		//bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Calls the GuiLib_ScrollBox_Redraw routine and redraws the cursor positions.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task.
 * 
 * @param pscrollbox_index The scroll box index to redraw. This should always be
 * 0 unless there is more than one scroll box on the screen.
 *
 * @author 8/6/2012 Adrianusv
 *
 * @revisions (none)
 */
static void FDTO_SCROLL_BOX_redraw( const UNS_32 pscrollbox_index )
{
	GuiLib_ScrollBox_Redraw( (UNS_8)pscrollbox_index );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Redraws the scrollbox, retaining the currently highlighted item and the item 
 * at the top of the visible scroll box window. This can be viewed as a wrapper
 * for the GuiLib_ScrollBox_Redraw routine with added functionality to retain or
 * adjust the visible top row.
 * 
 * @mutex_requirements Calling function must have the
 * list_system_recursive_MUTEX mutex if redrawing the irrigation system list,
 * the list_program_data_recursive_MUTEX if redrawing any other group list, or,
 * for the case of the irrigation details screen, either the
 * list_foal_irri_recursive_MUTEX or the list_irri_irri_recursive_MUTEX, to
 * ensure the appropriate list isn't changed during this process.
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task.
 * 
 * @param pscrollbox_index The scroll box index to redraw. This should always be
 * 0 unless there is more than one scroll box on the screen.
 * 
 * @param pnumber_of_lines The number of items in the scroll box. When using a
 * list to populate the scroll box, this should be the list count.
 * 
 * @param predraw_scrollbox_func_ptr A pointer to the screen's redraw_scrollbox
 * function.
 * 
 * @param predraw_entire_scrollbox True if the entire scroll box should be
 * redrawn; otherwise, false to only redraw the scroll lines that are currently
 * visible on the screen.
 * 
 * @param pretain_active_line True if the selected line should be retained if a
 * new line is added.
 *
 * @author 7/29/2011 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_SCROLL_BOX_redraw_retaining_topline( const UNS_32 pscrollbox_index,
													  const UNS_32 pnumber_of_lines,
													  const BOOL_32 predraw_entire_scrollbox )
{
	if( pnumber_of_lines == 0 )
	{
		if( (predraw_entire_scrollbox) && (DIALOG_a_dialog_is_visible() == (false)) )
		{
			FDTO_Redraw_Screen( (false) );
		}
		else
		{
			GuiLib_Refresh();
		}
	}
	else
	{
		if( predraw_entire_scrollbox )
		{
			if( DIALOG_a_dialog_is_visible() == (false) )
			{
				FDTO_Redraw_Screen( (false) );
			}
		}
		else
		{
			GuiLib_ScrollBox_Redraw( (UNS_8)pscrollbox_index );

			// Refresh the variables and update the display
			GuiLib_Refresh();
		}
	}
}

/* ---------------------------------------------------------- */
static void FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw( const BOOL_32 pallow_redraw )
{
	g_SCROLL_BOX_prevent_redraw = !(pallow_redraw);
}

/* ---------------------------------------------------------- */
/**
 * Safely calls the GuiLib_ScrollBox_Redraw routine and associated processing
 * routines such as GuiLib_Refresh when not in the context of the display
 * processing task.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user tries to jump to a specific scroll line of a scroll box.
 * 
 * @param pscrollbox_index The scroll box index to redraw. This should always be
 * 0 unless there is more than one scroll box on the screen.
 *
 * @author 5/18/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void SCROLL_BOX_redraw( const UNS_32 pscrollbox_index )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_SCROLL_BOX_redraw;
	lde._06_u32_argument1 = pscrollbox_index;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
extern void SCROLL_BOX_redraw_line( const UNS_32 pscrollbox_index, const UNS_32 pline_index )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
	lde._04_func_ptr = (void*)&GuiLib_ScrollBox_RedrawLine;
	lde._06_u32_argument1 = pscrollbox_index;
	lde._07_u32_argument2 = pline_index;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/**
 * Safely calls the GuiLib_ScrollBox_Up and GuiLib_ScrollBox_Down routines and
 * associated processing routines such as GuiLib_Refresh when not in the context
 * of the display processing task.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user tries to jump to a specific scroll line of a scroll box.
 * 
 * @param pscrollbox_index The scroll box index to redraw. This should always be
 * 0 unless there is more than one scroll box on the screen.
 * 
 * @param pkeycode The key that was pressed - either KEY_C_UP or KEY_C_DOWN.
 * This is used to determine whether to move the scroll line up or down.
 *
 * @author 8/7/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void SCROLL_BOX_up_or_down( const UNS_32 pscrollbox_index, const UNS_32 pkeycode )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;

	if( (pkeycode == KEY_C_UP) || (pkeycode == KEY_PLUS) )
	{
		lde._04_func_ptr = (void*)&FDTO_SCROLL_BOX_up;
	}
	else
	{
		lde._04_func_ptr = (void*)&FDTO_SCROLL_BOX_down;
	}

	lde._06_u32_argument1 = pscrollbox_index;

	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
extern void SCROLL_BOX_to_line( const UNS_32 pscrollbox_index, const UNS_32 pline_index )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
	lde._04_func_ptr = (void*)&FDTO_SCROLL_BOX_to_line;
	lde._06_u32_argument1 = pscrollbox_index;
	lde._07_u32_argument2 = pline_index;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
extern void SCROLL_BOX_close_all_scroll_boxes( void )
{
	UNS_32	i;

	// Close any open scroll boxes.
	for( i = 0; i < GuiConst_SCROLLITEM_BOXES_MAX; ++i )
	{
		if( GuiLib_ScrollBox_GetActiveLine( (UNS_8)i, 0 ) > GuiLib_NO_CURSOR )
		{
			GuiLib_ScrollBox_Close( (UNS_8)i );
		}
	}

	// 5/29/2015 ajv : Reset the horizontal scroll markers as well.
	GuiVar_ScrollBoxHorizScrollPos = 0;
	GuiVar_ScrollBoxHorizScrollMarker = 0;
}

/* ---------------------------------------------------------- */
/**
 * Toggles a static flag which determines whether the call the
 * GuiLib_ScrollBox_Redraw and GuiLib_Refresh routines after moving the active
 * scroll line. This is used when you want to move the active line without
 * actually highlighting it.
 * 
 * @mutex_requirements (none) 
 *
 * @memory_responsibilities (none) 
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * using NEXT and PREV to navigate to the next or previous group or station
 * while not on the scroll box.
 * 
 * @param pallow_redraw False to suppress the calls to redraw the screen. True
 * to re-enable them. This routine MUST be called twice - once with (false) and
 * afterwards with (true) for scroll boxes to behave properly.
 *
 * @example 
 *   To prevent the scroll box from being redrawn, effectively hiding the scroll
 *   bar marker:<br><br>
 *   <code>
 *     SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_DISABLED );
 *   </code><br>
 *   <code>
 *     <...>
 *   </code><br>
 *   <code>
 *     SCROLL_BOX_toggle_scroll_box_automatic_redraw( SCROLL_BOX_AUTOMATIC_REDRAW_ENABLED );
 *   </code>
 * 
 * @author 10/24/2013 AdrianusV
 *
 * @revisions (none)
 */
extern void SCROLL_BOX_toggle_scroll_box_automatic_redraw( const BOOL_32 pallow_redraw )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw;
	lde._06_u32_argument1 = pallow_redraw;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

