/*  file = background_calculations.C          8/24/2013  ajv  */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for strlcat
#include	<string.h>

#include	"background_calculations.h"

#include	"app_startup.h"

#include	"alerts.h"

#include	"configuration_network.h"

#include	"e_status.h"

#include	"epson_rx_8025sa.h"

#include	"manual_programs.h"

#include	"station_groups.h"

#include	"stations.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 08/27/2013 ajv : When a new request to calculate the next scheduled
// irrigation is posted to the queue, we want to cancel any ongoing calculation.
// This variable is therefore set TRUE whenever a new event is posted and set
// false once it's removed from the queue.
// 
// This is also used to control how many requests to recalculate can be posted
// the queue. If this variable is set TRUE, meaning there's already an event
// queued, we won't post another request to the queue until that request has
// been removed.
static BOOL_32 STATUS_next_scheduled_calc_queued;

// 07/08/2013 ajv : For debug purposes to measure how long it takes to do the
// processing for next scheduled irrigation
static UNS_32 __largest_next_scheduled_delta;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void STATUS_load_next_scheduled_into_guivars( void )
{
	MANUAL_PROGRAMS_GROUP_STRUCT	*lman_prog;

	STATION_GROUP_STRUCT	*lschedule;

	STATION_STRUCT	*lstation;

	DATE_TIME_COMPLETE_STRUCT	ldtcs;

	UNS_32	residual;

	UNS_32	lstart_time_idx;

	UNS_32	lday_of_month, lmonth_1_12, lyear, ldow, ltodays_date;

	UNS_32	lseconds;

	UNS_32	lno_water_days;

	BOOL_32	lthere_is_at_least_one_start_time_and_water_day;

	UNS_32 d, i;

	char	lgroup_name_str_48[ 48 ];

	char	lirrigation_date_str_64[ 64 ];


	// 2011.12.01 rmd : For debug purposes to measure how long it takes to
	// calculate the next scheduled start time. Factors that can affect the
	// duration include multiple schedules, no water days, a large number of
	// stations.
	// 
	// The worst case number of loop iterations is as follows:
	//  	38 days * 144 starts/day *
	//  	((768 MPs/start * 6 start times/MP * 768 stations/start time) +
	//  	(768 programs/start * 768 stations/program))
	UNS_32	__this_time_delta;

	__this_time_delta = my_tick_count;

	// Start with the present
	EPSON_obtain_latest_complete_time_and_date( &ldtcs );

	// Capture today's date for comparison later
	ltodays_date = ldtcs.date_time.D;

	// Round to the nearest 10 minute boundary
	residual = ldtcs.date_time.T % 600;

	if( residual != 0 )
	{
		ldtcs.date_time.T += 600 - residual;
	}
	
	// Test that we didn't push it up to midnight or beyond
	if( ldtcs.date_time.T >= 86400 )
	{
		ldtcs.date_time.T = 0;
		ldtcs.date_time.D += 1;
	}

	GuiVar_StatusTypeOfSchedule = STATUS_SCHEDULE_TYPE_NONE;
	GuiVar_StatusNextScheduledIrriGID = 0;

	lthere_is_at_least_one_start_time_and_water_day = (false);

	if( NETWORK_CONFIG_get_controller_off() == (false) )
	{
		// 7/20/2015 rmd : Apparently we look ahead 38 days. Someone somehow decided this but was
		// not documented when I bumped into this. It does seem reasonable however.
		for( d = 0; d < (STATION_NO_WATER_DAYS_MAX + DAYS_IN_A_WEEK); ++d )
		{
			DateToDMY( ldtcs.date_time.D, &lday_of_month, &lmonth_1_12, &lyear, &ldow );

			// 8/26/2013 ajv : Since we use the ldtcs to determine whether today
			// is a water day, we need to keep the month, day, year, and day of
			// week properties up-to-date based upon whatever day the loop is
			// on.
			ldtcs.__month = lmonth_1_12;
			ldtcs.__day = lday_of_month;
			ldtcs.__year = lyear;
			ldtcs.__dayofweek = ldow;

			while( (ldtcs.date_time.T < 86400) && (GuiVar_StatusTypeOfSchedule == STATUS_SCHEDULE_TYPE_NONE) )
			{
				// 2/11/2016 ajv : FIRST check to see if any MANUAL PROGRAMS are scheduled to run. They are
				// tested for before Programmed Irrigation because they are a higher priority and will tend
				// to irrigate first.
				// 
				// Note that we're only looping through groups that are in use - we're ignoring any that are
				// deleted.
				for( i = 0; i < MANUAL_PROGRAMS_get_num_groups_in_use(); ++i )
				{
					lman_prog = MANUAL_PROGRAMS_get_group_at_this_index( i );

					if( lman_prog != NULL )
					{
						// 7/20/2015 rmd : Test that today is within the start and stop range. And that today is a
						// water day.
						if( (ldtcs.date_time.D >= MANUAL_PROGRAMS_get_start_date(lman_prog)) && (ldtcs.date_time.D <= MANUAL_PROGRAMS_get_end_date(lman_prog)) && (MANUAL_PROGRAMS_today_is_a_water_day(lman_prog, ldow) == (true)) )
						{
							for( lstart_time_idx = 0; lstart_time_idx < MANUAL_PROGRAMS_NUMBER_OF_START_TIMES; lstart_time_idx++ )
							{
								if( MANUAL_PROGRAMS_get_start_time( lman_prog, lstart_time_idx ) == ldtcs.date_time.T )
								{
									// 7/20/2015 rmd : AJ did some testing early on (2012 ish) and determined then with full
									// station load that holding the station list MUTEX for the duration of the station list
									// caused problems for other tasks that wanted this MUTEX. I think this code needs to be
									// re-thought. And this issue retested. I believe there may be a better way to come up with
									// more efficient code here.
									xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

									lstation = nm_ListGetFirst( &station_info_list_hdr );

									xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

									while( lstation != NULL )
									{
										if( STATION_does_this_station_belong_to_this_manual_program( lstation, nm_GROUP_get_group_ID(lman_prog) ) && STATION_station_is_available_for_use(lstation) )
										{
											lseconds = 0;

											lseconds = MANUAL_PROGRAMS_get_run_time( lman_prog );
											
											if( lseconds > 0 )
											{
												lthere_is_at_least_one_start_time_and_water_day = (true);

												lno_water_days = STATION_get_no_water_days(lstation);

												if ( ((INT_32)(lno_water_days - d) < 0) || ((((INT_32)(lno_water_days - d) == 0)) && (ldtcs.date_time.T > 14400)) )
												{
													GuiVar_StatusTypeOfSchedule = STATUS_SCHEDULE_TYPE_MANUAL_PROGRAM;

													GuiVar_StatusNextScheduledIrriGID = nm_GROUP_get_group_ID( lman_prog );
													
													snprintf( lgroup_name_str_48, sizeof(lgroup_name_str_48), "%s %s", nm_GROUP_get_name(lman_prog), GuiLib_GetTextPtr( GuiStruct_txtWillRun_0, 0 ) );

													TDUTILS_time_to_time_string_with_ampm( lirrigation_date_str_64, sizeof(lirrigation_date_str_64), ldtcs.date_time.T, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING );

													if( ldtcs.date_time.D == ltodays_date )
													{
														strlcat( lirrigation_date_str_64, GuiLib_GetTextPtr( GuiStruct_txtToday_0, 0 ), sizeof(lirrigation_date_str_64) );
													}
													else
													{
														sp_strlcat( lirrigation_date_str_64, sizeof(lirrigation_date_str_64), " on %s, %s %d, %d", GetDayLongStr(ldow), GetMonthShortStr((lmonth_1_12 - 1)), lday_of_month, lyear );
													}

													break;  // out of the WHILE loop of each possible station
												}
											}
										}

										xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

										lstation = nm_ListGetNext( &station_info_list_hdr, lstation );

										xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

										// 8/27/2013 ajv : Since another request to calculate next scheduled was posted,
										// cancel the ongoing calculation. This ensures the calculation isn't performed
										// using potentially different data.
										if( STATUS_next_scheduled_calc_queued == (true) )
										{
											break;
										}
									}
								}

								if( GuiVar_StatusTypeOfSchedule != STATUS_SCHEDULE_TYPE_NONE )
								{
									break;	// out of the FOR loop for each of the 6 start times
								}
							}
						}
					}
					else
					{
						Alert_group_not_found();
					}

					if( GuiVar_StatusTypeOfSchedule != STATUS_SCHEDULE_TYPE_NONE )
					{
						break;	// out of the FOR loop for each of the manual programs
					}
				}

				// SECOND check to see if any IRRIGATION SCHEDULES are scheduled
				if( (STATUS_next_scheduled_calc_queued == (false)) && (GuiVar_StatusTypeOfSchedule == STATUS_SCHEDULE_TYPE_NONE) )
				{
					for( i = 0; i < STATION_GROUP_get_num_groups_in_use(); ++i )
					{
						lschedule = STATION_GROUP_get_group_at_this_index( i );

						if( lschedule != NULL )
						{
							// 5/8/2015 ajv : Added check to ensure the start and stop times are not equal since this
							// will prevent the schedule from running.
							if( (ldtcs.date_time.T == SCHEDULE_get_start_time(lschedule)) && (SCHEDULE_get_start_time(lschedule) != SCHEDULE_get_stop_time(lschedule)) )
							{
								if( SCHEDULE_does_it_irrigate_on_this_date( lschedule, &ldtcs, SCHEDULE_BEGIN_DATE_ACTION_do_nothing ) )
								{
									xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

									lstation = nm_ListGetFirst( &station_info_list_hdr );

									xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

									while( lstation != NULL )
									{
										if( (STATION_station_is_available_for_use(lstation) == (true)) && (STATION_get_GID_station_group(lstation) == nm_GROUP_get_group_ID(lschedule)) )
										{
											lno_water_days = STATION_get_no_water_days(lstation);

											if( WEATHER_get_station_uses_daily_et(lstation) == (true) )
											{
												if( STATION_get_ET_factor_100u(lstation) > 0 )
												{
													lthere_is_at_least_one_start_time_and_water_day = (true);

													if ( (INT_32)(lno_water_days - d) <= 0 )
													{
														GuiVar_StatusNextScheduledIrriGID = nm_GROUP_get_group_ID(lschedule);

														GuiVar_StatusTypeOfSchedule = STATUS_SCHEDULE_TYPE_STATION_GROUP;

														break;  // out of the WHILE loop of each possible station
													}
												}
											}
											else if( STATION_get_total_run_minutes_10u(lstation) > 0 )
											{
												lthere_is_at_least_one_start_time_and_water_day = (true);

												if ( (INT_32)(lno_water_days - d) <= 0 )
												{
													GuiVar_StatusTypeOfSchedule = STATUS_SCHEDULE_TYPE_STATION_GROUP;
												
													break;  // out of the WHILE loop of each possible station
												}
											}
										}

										xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

										lstation = nm_ListGetNext( &station_info_list_hdr, lstation );

										xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );

										// 8/27/2013 ajv : Since another request to calculate next scheduled was posted,
										// cancel the ongoing calculation. This ensures the calculation isn't performed
										// using potentially different data.
										if( STATUS_next_scheduled_calc_queued == (true) )
										{
											break;
										}
									}

									if( GuiVar_StatusTypeOfSchedule != STATUS_SCHEDULE_TYPE_NONE )
									{
										snprintf( lgroup_name_str_48, sizeof(lgroup_name_str_48), "%s %s", nm_GROUP_get_name(lschedule), GuiLib_GetTextPtr( GuiStruct_txtWillRun_0, 0 ) );

										TDUTILS_time_to_time_string_with_ampm( lirrigation_date_str_64, sizeof(lirrigation_date_str_64), ldtcs.date_time.T, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_DONT_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING );

										if( ldtcs.date_time.D == ltodays_date )
										{
											strlcat( lirrigation_date_str_64, GuiLib_GetTextPtr( GuiStruct_txtToday_0, 0 ), sizeof(lirrigation_date_str_64) );
										}
										else
										{
											sp_strlcat( lirrigation_date_str_64, sizeof(lirrigation_date_str_64), " on %s, %s %d, %d", GetDayLongStr(ldow), GetMonthShortStr((lmonth_1_12-1)), lday_of_month, lyear );
										}

										break;  // out of the FOR loop of each irrigation schedule
									}
								}
							}
						}
						else
						{
							Alert_group_not_found();
						}
					}
				}

				// increment the time ... assume ALL start times are on 10 minute boundaries!
				//
				ldtcs.date_time.T += 600;
			}

			// 9/9/2014 ajv : If we've found a schedule, break out of the loop
			// now to skip any further processing.
			if( GuiVar_StatusTypeOfSchedule != STATUS_SCHEDULE_TYPE_NONE )
			{
				break;
			}
			else
			{
				// It's a new day, so restart at midnight
				ldtcs.date_time.T = 0;

				// ... and increment the day
				ldtcs.date_time.D += 1;
			}
		}
	}

	// Reasons why the schedule may not run include the controller is off, there
	// are no start times or water days, no stations are assigned to a group, or
	// no water days are preventing irrigation.
	if( (GuiVar_StatusTypeOfSchedule == STATUS_SCHEDULE_TYPE_MANUAL_PROGRAM) ||
		(GuiVar_StatusTypeOfSchedule == STATUS_SCHEDULE_TYPE_STATION_GROUP) )
	{
		strlcpy( GuiVar_StatusNextScheduledIrriType, lgroup_name_str_48, sizeof(GuiVar_StatusNextScheduledIrriType) );
		strlcpy( GuiVar_StatusNextScheduledIrriDate, lirrigation_date_str_64, sizeof(GuiVar_StatusNextScheduledIrriDate) );
	}
	else if( NETWORK_CONFIG_get_controller_off() )
	{
		strlcpy( GuiVar_StatusNextScheduledIrriType, GuiLib_GetTextPtr( GuiStruct_txtControllerIsOff_0, 0 ), sizeof(GuiVar_StatusNextScheduledIrriType) );
		strlcpy( GuiVar_StatusNextScheduledIrriDate, GuiLib_GetTextPtr( GuiStruct_txtThereWillBeNoSchIrri_0, 0 ), sizeof(GuiVar_StatusNextScheduledIrriDate) );
	}
	else if( STATION_get_num_stations_assigned_to_groups() == 0 )
	{
		GuiVar_StatusTypeOfSchedule = STATUS_SCHEDULE_TYPE_GROUPS_ARE_EMPTY;

		strlcpy( GuiVar_StatusNextScheduledIrriType, GuiLib_GetTextPtr( GuiStruct_txtThereWillBeNoSchIrri_0, 0 ), sizeof(GuiVar_StatusNextScheduledIrriType) );
		strlcpy( GuiVar_StatusNextScheduledIrriDate, GuiLib_GetTextPtr( GuiStruct_txtBecauseAllStationGroupsAreEmpty_0, 0 ), sizeof(GuiVar_StatusNextScheduledIrriDate) );
	}
	else if( lthere_is_at_least_one_start_time_and_water_day == (false) )
	{
		strlcpy( GuiVar_StatusNextScheduledIrriType, GuiLib_GetTextPtr( GuiStruct_txtThereIsNoIrrigationScheduled_0, 0 ), sizeof(GuiVar_StatusNextScheduledIrriType) );
		strlcpy( GuiVar_StatusNextScheduledIrriDate, "", sizeof(GuiVar_StatusNextScheduledIrriDate) );
	}
	else
	{
		strlcpy( GuiVar_StatusNextScheduledIrriType, "", sizeof(GuiVar_StatusNextScheduledIrriType) );
		strlcpy( GuiVar_StatusNextScheduledIrriDate, "", sizeof(GuiVar_StatusNextScheduledIrriDate) );
	}

	__this_time_delta = (my_tick_count - __this_time_delta);

	if( __this_time_delta > __largest_next_scheduled_delta )
	{
		__largest_next_scheduled_delta = __this_time_delta;
	}
}

/* ---------------------------------------------------------- */
extern void background_calculation_task( void *pvParameters )
{
	BKGRND_CALC_EVENT_DEFS	lcmd;

	// ----------
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32	task_index;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	STATUS_next_scheduled_calc_queued = (false);

	while( (true) )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( Background_Calculation_Queue, &lcmd, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			// 9/9/2014 ajv : When we post an event to this task from the Date
			// and Time screen after making a change to the date or time,
			// there's some time required for the RTC to store the information
			// and read it back out into the variables used by our calculations.
			// Worst case, once the information is written to the i2c, there
			// will be an entire second where the values haven't been updated
			// yet. Therefore, we're introducing a delay here, before we start
			// any calculation, to allow this procses to complete.
			// 
			// The only visible drawback to this is the user may navigate back
			// to the Status screen before the calculation has time to finish,
			// resulting in the next scheduled irrigation changing while they're
			// viewing it. We've deemed this acceptable.
			vTaskDelay( MS_to_TICKS(1200) );

			switch( lcmd )
			{
				case BKGRND_CALC_NEXT_SCHEDULED:
					STATUS_next_scheduled_calc_queued = (false);

					STATUS_load_next_scheduled_into_guivars();
					break;
					
				case BKGRND_CALC_FINISH_TIMES:
					break;
			}
		} 

		// ----------
		
		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------
		
	}  // of the forever task loop
	
}

/* ---------------------------------------------------------- */
extern void postBackground_Calculation_Event( const UNS_32 pcmd )
{
	//	WARNING: THIS FUNCTION MUST NOT BE CALLED FROM AN INTERRUPT!

	switch( pcmd )
	{
		case BKGRND_CALC_NEXT_SCHEDULED:
			// 8/28/2013 ajv : Only post the event to the queue if we don't
			// already have an event of this type in the queue. This prevents
			// a queue overflow if multiple changes are made before this task
			// has time to process them.
			if( STATUS_next_scheduled_calc_queued == (false) )
			{
				// post the event using the queue handle for this task.
				if ( xQueueSendToBack( Background_Calculation_Queue, &pcmd, 0 ) == errQUEUE_FULL )  // do not wait for queue space ... flag an error
				{
					Alert_Message( "Background Calculation QUEUE OVERFLOW!" );
				}
				else
				{
					STATUS_next_scheduled_calc_queued = (true);
				}
			}
			break;

		case BKGRND_CALC_FINISH_TIMES:
			break;
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

