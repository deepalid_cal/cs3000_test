/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"textbox.h"

#include	"d_process.h"

#include	"GuiLib.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static void FDTO_TEXT_BOX_up( const UNS_32 ptextbox_index )
{
	if( (GuiLib_TextBox_Scroll_Get_Pos( (UNS_8)ptextbox_index ) != GuiLib_TEXTBOX_SCROLL_AT_HOME) &&
		(GuiLib_TextBox_Scroll_Up( (UNS_8)ptextbox_index ) == (true)) )
	{
		good_key_beep();

		GuiLib_Refresh();
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
static void FDTO_TEXT_BOX_down( const UNS_32 ptextbox_index )
{
	if( (GuiLib_TextBox_Scroll_Get_Pos( (UNS_8)ptextbox_index ) != GuiLib_TEXTBOX_SCROLL_AT_END) &&
		(GuiLib_TextBox_Scroll_Down( (UNS_8)ptextbox_index ) == (true)) )
	{
		good_key_beep();

		GuiLib_Refresh();
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
extern void TEXT_BOX_up_or_down( const UNS_32 ptextbox_index, const UNS_32 pkeycode )
{
	DISPLAY_EVENT_STRUCT	lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;

	if( (pkeycode == KEY_C_UP) || (pkeycode == KEY_PLUS) )
	{
		lde._04_func_ptr = (void*)&FDTO_TEXT_BOX_up;
	}
	else
	{
		lde._04_func_ptr = (void*)&FDTO_TEXT_BOX_down;
	}

	lde._06_u32_argument1 = ptextbox_index;

	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

