/*  file = screen_utils.h  2009.05.29  rmd                    */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_SCREEN_UTILS_H
#define _INC_SCREEN_UTILS_H



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cal_td_utils.h"

#include	"k_process.h"

#include	"m_main.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// LCD COLORS
// The only lcd shades of grey that we should be using are below:
//
// This is due to the timing effects the EPSON LCD controller has on the other
// 'colors'. They twinkle and move with strange patterns in them.

#define		COLOR_BLACK				0x00		// RGB=0,0,0
#define		COLOR_BLACKISH_GREY		0x04		// RGB=68,68,68
#define		COLOR_GREY				0x07		// RGB=119,119,119
#define		COLOR_WHITISH_GRAY		0x0A		// RGB=170,170,170
#define		COLOR_MENU_GRAY			0x0C		// RGB=204,204,204
#define		COLOR_WHITE				0x0F		// RGB=255,255,255

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// With standard ANSI7 default character set, characters are 6 pixels wide and
// 11 pixels tall. So a 320 x 240 display gives us 53 characters x 21 rows.

#define ANSI7_WIDTH				(6)
#define ANSI7_HEIGHT			(11)

#define ANSI7_BOLD_WIDTH		(7)
#define ANSI7_BOLD_HEIGHT		(11)

#define ANSI9_WIDTH				(7)
#define ANSI9_HEIGHT			(12)

#define ANSI11_LIGHT_WIDTH		(6)
#define ANSI11_LIGHT_HEIGHT		(17)

#define ANSI11_CONDENSED_WIDTH	(8)
#define ANSI11_CONDENSED_HEIGHT	(17)

#define ANSI11_WIDTH			(9)
#define ANSI11_HEIGHT			(17)

#define ANSI13_WIDTH			(11)
#define ANSI13_HEIGHT			(21)

#define ANSI19_WIDTH			(17)
#define ANSI19_HEIGHT			(31)

#define ANSI24_WIDTH			(19)
#define ANSI24_HEIGHT			(39)

#define ANSI30_WIDTH			(21)
#define ANSI30_HEIGHT			(47)

#define UNICODE7_14_BOLD_WIDTH	(15)
#define UNICODE7_14_BOLD_HEIGHT	(16)

#define UNICODE9_15_WIDTH		(16)
#define UNICODE9_15_HEIGHT		(17)

#define UNICODE11_16_WIDTH		(17)
#define UNICODE11_16_HEIGHT		(18)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define ANSI7_boxwidth( length )	(INT_16)(ANSI7_WIDTH * length)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define SCREEN_MENU_MAIN						(CP_MAIN_MENU_STATUS)
#define SCREEN_MENU_SCHEDULE					(CP_MAIN_MENU_SCHEDULED_IRRIGATION)
#define	SCREEN_MENU_POCS						(CP_MAIN_MENU_POCS)
#define	SCREEN_MENU_MAINLINES					(CP_MAIN_MENU_MAIN_LINES)
#define SCREEN_MENU_WEATHER						(CP_MAIN_MENU_WEATHER)
#define	SCREEN_MENU_BUDGETS						(CP_MAIN_MENU_BUDGETS)
#define SCREEN_MENU_LIGHTS						(CP_MAIN_MENU_LIGHTS)
#define	SCREEN_MENU_MANUAL						(CP_MAIN_MENU_MANUAL)
#define	SCREEN_MENU_NO_WATER_DAYS				(CP_MAIN_MENU_NO_WATER_DAYS)
#define SCREEN_MENU_REPORTS						(CP_MAIN_MENU_REPORTS)
#define SCREEN_MENU_DIAGNOSTICS					(CP_MAIN_MENU_DIAGNOSTICS)
#define	SCREEN_MENU_SETUP						(CP_MAIN_MENU_SETUP)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		DE_COMMAND_execute_func_with_no_arguments			0x0001
#define		DE_COMMAND_execute_func_with_single_u32_argument	0x0002
#define		DE_COMMAND_execute_func_with_two_u32_arguments		0x0003

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/**
 * Structure for the FreeRTOS queue...the idea for the queue is to hold the 
 * commands and data needed to paint all screens...we use a queue so that only 
 * one task is actually using the EasyGui library at a time. 
 */
typedef struct
{
	// Tells the task that receives the queue messages what it needs to do
	UNS_32	_01_command;

	// when sending a DISPLAY_EVENT_STRUCTURE to the Change_Screen function this screen is set to the screen we are changing to...
	// we also keep the history of the DISPLAY_EVENT_STRUCTURE that went through the Change_Screen functon because they contain all the
	// information we need to process the BACK key and redraw a screen later on (when the user presses the BACK key)
	// 
	// Only used during a subsequent call to ChangeScreen (included in this
	// struct as a convience)
	UNS_32	_02_menu;				

	// The GuiLib structure number
	UNS_32	_03_structure_to_draw;

	void	(*populate_scroll_box_func_ptr)( const INT_16 pindex_0_i16 );

	// This is the key_process function pointer. It is either this or a big
	// giant case statement for each display state that in turn calls the
	// function. I decided to give this way a try.
	void 	(*key_process_func_ptr)( KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

	// This is a little loose as we are using a single finction pointer to
	// reference a ptr to several different types of functions (i.e., ones with
	// arguments or not, and different arguments)
	void	(*_04_func_ptr)();

	// this is the classic 'pcomplete_redraw' parameter most top level screen
	// drawing functions use
	UNS_32	_06_u32_argument1;						

	// So far, only used when scrolling to indicate the scroll line to draw and
	// for drawing a station description.
	UNS_32	_07_u32_argument2;

	UNS_32	_08_screen_to_draw;

} DISPLAY_EVENT_STRUCT;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define SCREEN_HISTORY_QUEUE_LENGTH 50

// we keep a history of the change screen DISPLAY_EVENTS posted to the task that
// drawing task...note only the structs posted to the queue as a result of the
// ChangeScreen function call...so if a screen is to be in the KEY_BACK history
// it must be originally navigated to via ChangeScreen
//
// this structure contains either the details (structure name or function call)
// to entirely redraw the screen for the new display
extern DISPLAY_EVENT_STRUCT ScreenHistory[ SCREEN_HISTORY_QUEUE_LENGTH ];

extern UNS_32 screen_history_index;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_DrawStr( const INT_32 x, const INT_32 y, const UNS_32 proportional_spacing, char* fmt, ... );

extern void FDTO_show_time_date( DATE_TIME *pdt_ptr );

extern void FDTO_show_screen_name_in_title_bar( const UNS_32 px_coord, const UNS_32 ptxt_str_to_draw );



extern void FDTO_scroll_left( const UNS_32 pmax_pixels_to_move, const UNS_32 pmove_by );

extern void FDTO_scroll_right( const UNS_32 pmax_pixels_to_move, const UNS_32 pmove_by );



extern void Change_Screen( DISPLAY_EVENT_STRUCT *pevent_ptr );

extern void KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen( void );

extern void FDTO_Redraw_Screen( const BOOL_32 pcomplete_redraw );

extern void Redraw_Screen( const BOOL_32 pcomplete_redraw );


extern void Refresh_Screen( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

