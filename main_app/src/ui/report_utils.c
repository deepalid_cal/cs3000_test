/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"report_utils.h"

#include	"d_process.h"

#include	"GuiLib.h"

#include	"m_main.h"

#include	"screen_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

static UNS_32 g_REPORT_active_line;

static UNS_32 g_REPORT_top_line;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern void FDTO_REPORTS_draw_report( const BOOL_32 pcomplete_redraw, const UNS_32 pline_count, void (*DataFuncPtr)(INT_16 pindex_0) )
{
	GuiLib_ScrollBox_Close( 0 );

	if( pcomplete_redraw == (true) )
	{
		GuiVar_ScrollBoxHorizScrollPos = 0;

		g_REPORT_active_line = 0;

		g_REPORT_top_line = 0;

		if( pline_count == 0 )
		{
			GuiLib_ScrollBox_Init( 0, DataFuncPtr, (INT_16)pline_count, GuiLib_NO_CURSOR );
		}
		else
		{
			GuiLib_ScrollBox_Init( 0, DataFuncPtr, (INT_16)pline_count, (INT_16)g_REPORT_top_line );
		}
	}
	else
	{
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, DataFuncPtr, (INT_16)pline_count, (INT_16)g_REPORT_active_line, (INT_16)g_REPORT_top_line );
	}

	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
extern void REPORTS_process_report( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, const UNS_32 pamount_to_scroll, const UNS_32 pmax_pixels_to_scroll )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkey_event.keycode )
	{
		case KEY_C_DOWN:
		case KEY_C_UP:
			SCROLL_BOX_up_or_down( 0, pkey_event.keycode );
			break;

		case KEY_C_LEFT:
		case KEY_C_RIGHT:
			g_REPORT_active_line = (UNS_32)GuiLib_ScrollBox_GetActiveLine( 0, 0 );

			g_REPORT_top_line = (UNS_32)GuiLib_ScrollBox_GetTopLine( 0 );

			lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;

			if( pkey_event.keycode == KEY_C_RIGHT )
			{
				lde._04_func_ptr = (void*)&FDTO_scroll_right;
			}
			else
			{
				lde._04_func_ptr = (void*)&FDTO_scroll_left;
			}

			lde._06_u32_argument1 = pmax_pixels_to_scroll;
			lde._07_u32_argument2 = pamount_to_scroll;
			Display_Post_Command( &lde );
			break;

		case KEY_BACK:
			GuiVar_MenuScreenToShow = ScreenHistory[ screen_history_index ]._02_menu;
			// 5/29/2015 ajv : No need to break here. This allows us to fall directly into the default
			// case.

		default:
			KEY_process_global_keys( pkey_event );
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

