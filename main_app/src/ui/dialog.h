/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_INC_DIALOG_H
#define	_INC_DIALOG_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"

#include	"k_process.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	DIALOG_TYPE_OK				(0)
#define	DIALOG_TYPE_YES_NO_CANCEL	(1)
#define	DIALOG_TYPE_YES_NO			(2)

// ----------

#define MODAL_RESULT_NONE			(0)
#define	MODAL_RESULT_OK				(1)
#define	MODAL_RESULT_CANCEL			(2)
#define	MODAL_RESULT_ABORT			(3)
#define	MODAL_RESULT_RETRY			(4)
#define MODAL_RESULT_IGNORE			(5)
#define	MODAL_RESULT_YES			(6)
#define MODAL_RESULT_NO				(7)
#define	MODAL_RESULT_CLOSE			(8)
#define MODAL_RESULT_ALL			(9)
#define MODAL_RESULT_NO_TO_ALL		(10)
#define	MODAL_RESULT_YES_TO_ALL		(11)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern UNS_32	g_DIALOG_modal_result;

// ----------

extern UNS_32	g_DIALOG_ok_dialog_visible;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern BOOL_32 DIALOG_a_dialog_is_visible( void );

// ----------

extern void FDTO_DIALOG_redraw_ok_dialog( void );

extern void DIALOG_draw_ok_dialog( const UNS_32 pstructure_to_draw );

extern void DIALOG_close_ok_dialog( void );

extern void DIALOG_process_ok_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event );

// ----------

extern void DIALOG_draw_yes_no_cancel_dialog( const UNS_32 pstructure_to_draw );

extern void DIALOG_process_yes_no_cancel_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, void (*callback_func_ptr)( void ) );

// ----------

extern void DIALOG_draw_yes_no_dialog( const UNS_32 pstructure_to_draw );

extern void DIALOG_process_yes_no_dialog( const KEY_TO_PROCESS_QUEUE_STRUCT pkey_event, void (*callback_func_ptr)( void ) );

// ----------

extern void DIALOG_close_all_dialogs( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

