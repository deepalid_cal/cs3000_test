/* file = screen_utils.c   2009.05.18  rmd                    */

// A collection of useful lcd support to generate our screens

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/18/2014 ajv : Required for va_list variable type and va_ routines
#include	<stdarg.h>

// 6/18/2014 ajv : Required for abs
#include	<stdlib.h>

#include	"cs_common.h"

#include	"startup.h"

#include	"flash_storage.h"

#include	"epson_rx_8025sa.h"

#include	"td_check.h"

#include	"irri_flow.h"

#include	"irri_irri.h"

#include	"speaker.h"

#include	"stations.h"

#include	"e_time_and_date.h"

#include	"irrigation_system.h"

#include	"poc.h"

#include	"alerts.h"

#include	"r_alerts.h"

#include	"app_startup.h"

#include	"e_flowsense.h"

#include	"change.h"

#include	"station_groups.h"

#include	"poc_report_data.h"

#include	"system_report_data.h"

#include	"weather_control.h"

#include	"dialog.h"

#include	"d_process.h"

#include	"cursor_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define DFP_MAX_STRING_I_CAN_HANDLE 255

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// we keep a history of the change screen DISPLAY_EVENTS posted to the task that
// drawing task...note only the structs posted to the queue as a result of the
// ChangeScreen function call...so if a screen is to be in the KEY_BACK history
// it must be originally navigated to via ChangeScreen
//
// this structure contains either the details (structure name or function call)
// to entirely redraw the screen for the new display
DISPLAY_EVENT_STRUCT ScreenHistory[ SCREEN_HISTORY_QUEUE_LENGTH ];

UNS_32 screen_history_index;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Draws the passed in string to the screen at the specified coordinates.
 * 
 * @executed Executed within the context of the dipslay processing task.
 * 
 * @param x The x-coordinate to draw the string.
 * @param y The y-coordinate to draw the string.
 * @param proportional_spacing True if the string should be drawing using
 *  						   proportional spacing; otherwise (for fixed
 *  						   spacing), false.
 * @param fmt The format string for sprintf. 
 * @param ... Variables to convert to strings for vsprintf.
 *
 * @author BobD
 *
 * @revisions
 *    2/14/2012 Replaced call to WriteFromPtrToDisplay to GuiLib_DrawStr. There
 *  			was no need to keep that function as a wrapper for
 *  			GuiLib_DrawStr since they both have to be called from the
 *  			display processing task anyway.
 */
extern void FDTO_DrawStr( const INT_32 x, const INT_32 y, const UNS_32 proportional_spacing, char* fmt, ... )
{
	char		sbuf[ DFP_MAX_STRING_I_CAN_HANDLE ];
	va_list		vargs;
	INT_32		len;

	// First argument is where to put the string. Second argument is the format
	// string for sprintf. All other possible ones that follow would be vars to
	// convert to strings.
	va_start( vargs, fmt );
	len = vsprintf( sbuf, fmt, vargs );

	// 6/1/2015 ajv : Don't forget the va_end.
	va_end( vargs );
	
	GuiLib_DrawStr( (INT_16)x, (INT_16)y, GuiFont_ANSI7, -1, sbuf, GuiLib_ALIGN_LEFT, (UNS_8)proportional_spacing, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, ANSI7_boxwidth(len), 0, 0, 0, GuiConst_PIXEL_ON, GuiConst_PIXEL_OFF );
}

/* ---------------------------------------------------------- */
/**
 * Copies the function argument date and time into GuiVar_DateTimeFullStr for display. And
 * displays them.
 * 
 * @executed Executed within the context of the display processing task once
 *           a second (display event is queued by the TD Check task).
 *
 * @author BobD
 *
 * @revisions
 *    
 */
extern void FDTO_show_time_date( DATE_TIME *pdt_ptr )
{
	char	t_str_32[ 32 ], d_str_32[ 32 ];

	snprintf(GuiVar_DateTimeFullStr, sizeof(GuiVar_DateTimeFullStr), "%s %s",
			 GetDateStr(d_str_32, sizeof(d_str_32), pdt_ptr->D, DATESTR_show_long_year, DATESTR_show_long_dow),
			 ShaveLeftPad(t_str_32, TDUTILS_time_to_time_string_with_ampm(t_str_32, sizeof(t_str_32), pdt_ptr->T, TDUTILS_DONT_TREAT_AS_A_SX, TDUTILS_INCLUDE_SECONDS, TDUTILS_DONT_CONCAT_TO_STRING)));

	GuiLib_Refresh();
}

extern void FDTO_show_screen_name_in_title_bar( const UNS_32 px_coord, const UNS_32 ptxt_str_to_draw )
{
	GuiLib_DrawStr( (INT_16)(px_coord + 5), 8, GuiFont_ANSI7Bold, -1, GuiLib_GetTextPtr( GuiStruct_symGT_0, 0), GuiLib_ALIGN_LEFT, GuiLib_PS_ON, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, 0, 0, 0, 0, GuiConst_PIXEL_OFF, GuiConst_PIXEL_ON );
	GuiLib_DrawStr( (INT_16)(px_coord + 15), 8, GuiFont_ANSI7, -1, GuiLib_GetTextPtr( (UNS_16)ptxt_str_to_draw, 0), GuiLib_ALIGN_LEFT, GuiLib_PS_ON, GuiLib_TRANSPARENT_OFF, GuiLib_UNDERLINE_OFF, 0, 0, 0, 0, GuiConst_PIXEL_OFF, GuiConst_PIXEL_ON );
}

#define	SCROLL_PIXELS_PER_SCREEN	(201)

extern void FDTO_scroll_left( const UNS_32 pmax_pixels_to_move, const UNS_32 pmove_by )
{
	if( GuiVar_ScrollBoxHorizScrollPos < 0 )
	{
		good_key_beep();

		// Move the table.
		GuiVar_ScrollBoxHorizScrollPos += (INT_32)pmove_by;

		if( GuiVar_ScrollBoxHorizScrollPos > 0 )
		{
			GuiVar_ScrollBoxHorizScrollPos = 0;
		}

		// Move the scroll line marker as well. Since the table position is
		// negative, and the scroll marker has to move by a positive amount,
		// simply use the absolute value of the horizontal scroll marker.
		GuiVar_ScrollBoxHorizScrollMarker = abs(GuiVar_ScrollBoxHorizScrollPos);

		if( pmax_pixels_to_move > SCROLL_PIXELS_PER_SCREEN )
		{
			GuiVar_ScrollBoxHorizScrollMarker = (GuiVar_ScrollBoxHorizScrollMarker / 2);
		}

		FDTO_Redraw_Screen( (false) );
	}
	else
	{
		bad_key_beep();
	}
}

extern void FDTO_scroll_right( const UNS_32 pmax_pixels_to_move, const UNS_32 pmove_by )
{
	INT_32	lpixels_to_move;

	lpixels_to_move = ((INT_32)pmax_pixels_to_move * -1);

	if( GuiVar_ScrollBoxHorizScrollPos > lpixels_to_move )
	{
		good_key_beep();

		// Move the table.
		GuiVar_ScrollBoxHorizScrollPos -= (INT_32)pmove_by;

		// Make sure we haven't exceeded the maximum amount we are allowed to
		// scroll.
		if( GuiVar_ScrollBoxHorizScrollPos < lpixels_to_move )
		{
			GuiVar_ScrollBoxHorizScrollPos = lpixels_to_move;
		}

		// Move the scroll line marker as well. Since the table position is
		// negative, and the scroll marker has to move by a positive amount,
		// simply use the absolute value of the horizontal scroll marker.
		GuiVar_ScrollBoxHorizScrollMarker = abs(GuiVar_ScrollBoxHorizScrollPos);

		if( pmax_pixels_to_move > SCROLL_PIXELS_PER_SCREEN )
		{
			GuiVar_ScrollBoxHorizScrollMarker = (GuiVar_ScrollBoxHorizScrollMarker / 2);
		}

		FDTO_Redraw_Screen( (false) );
	}
	else
	{
		bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/**
 * Changes to the specified screen.
 * 
 * Should not be used to cause a screen refresh as needs to return if already at
 * the requested screen. We need this return if already at screen so as not to
 * queue up multiple listings of the same screen as would happen, for example,
 * during communications (each command starts the timeout timer which times out
 * after communications and would stack multiple I_ControllerStatus - one for
 * each communication during the night - then in the morning the user could hit
 * the BACK key several times without actually seeing any screen changes)
 * 
 * @executed Executed within the context of the KEY_PROCESS task.
 * 
 * @param pevent_ptr A pointer to the display event structure to process.
 *
 * @author BobD
 *
 * @revisions
 *    
 */
extern void Change_Screen( DISPLAY_EVENT_STRUCT *pevent_ptr )
{
	UNS_32 i;

	// 6/2/2015 ajv : If already at the screen do nothing ... this function should not be used
	// for a screen refresh as for one it queues up the new screen into the BACK key queue
	if( (UNS_32)GuiLib_CurStructureNdx != pevent_ptr->_03_structure_to_draw )
	{
		good_key_beep();

		KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen();

		// Increment the screen index
		if ( screen_history_index < SCREEN_HISTORY_QUEUE_LENGTH - 1 )
		{
			++screen_history_index;
		}
		else
		{
			// Roll but preserve item 0 (the Controller Status)
			for ( i = 1; i < SCREEN_HISTORY_QUEUE_LENGTH - 1; ++i )
			{
				ScreenHistory[ i ] = ScreenHistory[ i + 1 ];
			}
		}

		ScreenHistory[ screen_history_index ] = *pevent_ptr;

		Display_Post_Command( pevent_ptr );
	}
	else
	{
		bad_key_beep();
	}
}

/**
 * Calls a specific screen's routines to generate change lines and store 
 * changes. This should be called from the Change_Screen routine so the changes 
 * are saved as soon as the screen changes, regardless of the reason. 
 * 
 * @author Adrianusv (6/20/2011)
 * 
 * @param pscreen_index 
 */
extern void KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen( void )
{
	// 7/27/2012 rmd : Release the array of ptrs used to draw the reports. We've left the
	// screen.
	SYSTEM_REPORT_free_report_support();

	// 8/1/2012 rmd : And same for the POC report support.
	POC_REPORT_free_report_support();

	// 8/1/2012 rmd : And same for the station report support.
	STATION_REPORT_DATA_free_report_support();

	// 8/1/2012 rmd : And same for the station history report support.
	STATION_HISTORY_free_report_support();

	// 9/9/2015 mpd : Release the pointer array for lights report support.
	LIGHTS_REPORT_DATA_free_report_support();

}

/* ---------------------------------------------------------- */
/**
 * This routine redraws the screen, retaining the current cursor position. 
 *  
 * This routine is intended to be called when using an indexed structure which 
 * appears or disappears based upon a variable's value. According to Page 169 of 
 * the easyGUI manual, the variable must be set <b>before</b> the screen is
 * shown. Therefore, if the value of the controlling variable is changed while
 * the screen is visible, the screen must be redrawn.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * after the user makes a change to a setting on the screen which requires the
 * screen to be redrawn. For example, on certain screens, when changing a Yes/No
 * question, other fields appear or disappear.
 *
 * @author 5/25/2011 Adrianusv
 *
 * @revisions (none)
 */
extern void FDTO_Redraw_Screen( const BOOL_32 pcomplete_redraw )
{
	if( DIALOG_a_dialog_is_visible() == (false) )
	{
		ScreenHistory[ screen_history_index ]._04_func_ptr( pcomplete_redraw );
	}
}

/* ---------------------------------------------------------- */
/**
 * This routine redraws the screen, retaining the current cursor position. 
 *  
 * This routine is intended to be called when using an indexed structure which 
 * appears or disappears based upon a variable's value. According to Page 169 of 
 * the easyGUI manual, the variable must be set <b>before</b> the screen is
 * shown. Therefore, if the value of the controlling variable is changed while
 * the screen is visible, the screen must be redrawn.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the KEY PROCESSING task when
 * the user makes a change to a setting on the screen which requires the screen
 * to be redrawn. For example, on certain screens, when changing a Yes/No
 * question, other fields appear or disappear.
 *
 * @author 5/25/2011 Adrianusv
 *
 * @revisions (none)
 */
extern void Redraw_Screen( const BOOL_32 pcomplete_redraw )
{
	DISPLAY_EVENT_STRUCT lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void *)&FDTO_Redraw_Screen;
	lde._06_u32_argument1 = pcomplete_redraw;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
#if 0
NO LONGER USED, BUT KEPT FOR REFERENCE IN CASE THIS IS NEEDED IN THE FUTURE
static void FDTO_Redraw_Structure( const BOOL_32 predraw_scrollbox )
{
	UNS_32	ltop_line;

	ltop_line = (UNS_32)GuiLib_ScrollBox_GetTopLine( 0 );

	GuiLib_ShowScreen( (UNS_16)(ScreenHistory[ screen_history_index ]._03_structure_to_draw), GuiLib_ActiveCursorFieldNo, GuiLib_RESET_AUTO_REDRAW );

	if( predraw_scrollbox == (true) )
	{
		xSemaphoreTakeRecursive( list_program_data_recursive_MUTEX, portMAX_DELAY );

		// 10/28/2013 ajv : Draw the scroll box without highlighting the active
		// line and draw the indicator to show which group the user is looking
		// at.
		//
		// 11/14/2013 ajv : Note the (2 * 1) in the NoOfLines parameters. We
		// need to display the <Add New Group> item at the bottom of the list of
		// groups. Since we've now added an additional divider line between the
		// groups and the Add New Group item, we need to make sure we add two
		// lines to the count.
		GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, ScreenHistory[ screen_history_index ].populate_scroll_box_func_ptr, (INT_16)(g_GROUP_number_of_groups + (2 * 1)), GuiLib_NO_CURSOR, (INT_16)ltop_line );
		GuiLib_ScrollBox_SetIndicator( 0, (INT_16)g_GROUP_list_item_index );
		GuiLib_ScrollBox_Redraw( 0 );
		GuiLib_Refresh();

		xSemaphoreGiveRecursive( list_program_data_recursive_MUTEX );
	}
}
#endif

/* ---------------------------------------------------------- */
/**
 * Safely calls the GuiLib_Refresh routine to refresh the screen and it's cursor
 * positions when not in the context of the display processing task.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed within the context of the key processing task when
 * the user changes a setting which requires the screen to be refreshed.
 *
 * @author 5/18/2011 Adrianusv
 *
 * @revisions (none)
 */
extern void Refresh_Screen( void )
{
	DISPLAY_EVENT_STRUCT lde;

	lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
	lde._04_func_ptr = &GuiLib_Refresh;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

