/*  file = d_process.c  2009.05.21  rmd                       */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"d_process.h"

#include	"cs_common.h"

#include	"r_memory_use.h"

#include	"app_startup.h"

#include	"wdt_and_powerfail.h"

#include	"alerts.h"

#include	"lcd_init.h"

#include	"battery_backed_vars.h"

#include	"flow_recorder.h"

#include	"foal_comm.h"

#include	"e_status.h"

#include	"irri_comm.h"

#include	"m_main.h"

#include	"background_calculations.h"

#include	"contrast_and_speaker_vol.h"

#include	"screen_utils.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// C startup code will initialize to FALSE. Not set TRUE untial after Display_Processing_Task is created and runs.
// Initializing the GuiLib Library itself and finally when truly ready for use sets this variable TRUE. Primarily introduced
// for use during detected, early startup, error conditions. We don't want to write to the display and makes things worse.
// Side tracking use from the real error at hand.
BOOL_32	GuiLib_okay_to_use;

/* ---------------------------------------------------------- */
extern void Display_Post_Command( DISPLAY_EVENT_STRUCT *de_ptr )
{
	// Post to the queue but wait no time if the queue is full...it should never be full...if so
	// that means the queue is too small or we have some other more serious problem.
	if( xQueueSendToBack( Display_Command_Queue, de_ptr, (portTickType)0 ) != pdPASS )
	{
		// If you are looking at alerts when this happens (a likely scenario would be you add an
		// alert and the queue is full) here we are trying to add another alert ... so I think we
		// best not alert here but what to do ??? an endless loop. That's scary. Suppose it happens
		// to a customer. Hmmmm. Write something to the display and cause the WDT to expire? Maybe.
		// TODO: something here
		//
		// There is now protection in the keypad ISR to prevent queueing another key to process if
		// there is an item on the display queue. So this queue should NEVER become full.
		//
		// This alert message CAN BE DISASTEROUS! If we are looking at alerts and scrolling though
		// them and somehow the display queue becomes full (there IS protection against this in the
		// keypad ISR) when we go to write this alert to the pile that will cause us to again post
		// to the display queue which at that point we have an endless loop. The stack will be
		// overrun and OS/sytem crashes! So let's test for the display mode first.
		if( GuiLib_CurStructureNdx != GuiStruct_rptAlerts_0 )
		{
			Alert_Message( "D_PROCESS: queue FULL" );
		}

	}

}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if 0

typedef struct
{
	UNS_32  T;

	UNS_16  D;

} __attribute__((packed)) NEW_DATE_TIME_STRUCT;
//} NEW_DATE_TIME_STRUCT;


typedef struct
{
	NEW_DATE_TIME_STRUCT	dt_1;

	NEW_DATE_TIME_STRUCT	dt_2;

	UNS_32					ul;

	float					fl;

} MY_TEST_STRUCT;

MY_TEST_STRUCT		mts, mts_array[ 4 ];

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/**
    The task that reads from the display command queue. The queue through which all display
    commands pass. THIS SHOULD BE THE ONLY TASK THAT ACTUALLY MAKES ANY CALLS TO THE GUI
    LIBRARY. SOLVES THE RE-ENTRANCY CONCERN.
*/
extern void Display_Processing_Task( void *pvParameters )
{
	DISPLAY_EVENT_STRUCT	de;

	// ----------
	
	// 10/23/2012 rmd : To support the WDT activity monitor.
	UNS_32	task_index;
	
	// 10/23/2012 rmd : Figure out the index within the task table array.
	task_index = ( (UNS_32)(pvParameters - (void*)Task_Table) / sizeof(TASK_ENTRY_STRUCT) );

	// ----------

	// Initialize to which buffer GuiLib is going to write screens to.
	FDTO_set_guilib_to_write_to_the_primary_display_buffer();

	// ----------

	// Initializes the graphical user interface system and clears the display.
	GuiLib_Init();

	// 8/28/2012 rmd : Retrieve the last language saved. The config_c file data is available
	// prior to this task - the display task - being created.
	GuiLib_SetLanguage( (INT_16)CONTRAST_AND_SPEAKER_VOL_get_screen_language() );

	GuiLib_okay_to_use = (true);  // It IS ready.

	// ----------

	// 07/08/2013 ajv : The next scheduled irrigation is normally kept
	// up-to-date by recalculating every time a change is made. However, on an
	// initial power-up, there is no information. Therefore, post a message to
	// update the GuiVars used to display the next scheduled irrigation.
	postBackground_Calculation_Event( BKGRND_CALC_NEXT_SCHEDULED );

	// ----------

	GuiLib_ShowBitmap( GuiStruct_Bitmap_X_OEMLOGO, 47, 57, -1 );
	GuiLib_Refresh();

	// ----------

	// 10/24/2012 rmd : We can have TWO tasks blocking on the key queue. The higher priority
	// task will 'win' the key. So by blocking on the key queue here if the user presses a key
	// we can perform certain behaviours. Such as RESET the unit and/or move past the spalsh
	// screen. Depending on the key. All the while keeping the watchdog happy for this task!
	KEY_TO_PROCESS_QUEUE_STRUCT		lktpqs;

	UNS_32	delay_total_ms;
	
	delay_total_ms = 5000;
	
	while( delay_total_ms > 0 )
	{
		// 3/12/2013 rmd : The 500ms timeout is to allow the task to check into the watchdog scheme.
		if( xQueueReceive( Key_To_Process_Queue, &lktpqs, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			// 10/24/2012 rmd : Means a key was pressed. Can look at it here to decide what to do. Or
			// ignore it if we want.
			


			// 10/24/2012 rmd : Break out of the while loop.
			break;
		}

		// ----------
		
		// 10/23/2012 rmd : Keep the watchdog activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------

		if( delay_total_ms >= 500 )
		{
			delay_total_ms -= 500;	

			if( delay_total_ms < 500 )
			{
				// 10/24/2012 rmd : Done. Move along.
				break;	
			}
		}

		// ----------
	}

	// ----------

	// and initalize the Status screen and draw it
	FDTO_STATUS_draw_screen();

	FDTO_MAIN_MENU_draw_menu( (true) );

	#ifdef USE_ENGINEERING_VALUES

		// --------------------------

		// 2012.03.07 rmd : At location 0x80000040 (in the SDRAM) is a string we have the compiler
		// embed. It shows the date and time the startup assembly file was assembled.
		FDTO_DrawStr( 2, 24, GuiLib_PS_ON, MAIN_APP_RUNNING_BUILD_DT );

		// --------------------------


		//FDTO_DrawStr( 2, 34, GuiLib_PS_ON, "size: %u", sizeof( RESTART_INFORMATION_STRUCT ) );

		// ----------
	
		/*
		// 10/22/2012 rmd : Produces a data abort error.
		UNS_8	*ucp;
		UNS_32	*ulp, dummy;
		
		ucp = (UNS_8*)0x1000;
	
		ucp += 1;
		
		ulp = (UNS_32*)ucp;
		
		dummy = *ulp;
		*/
		
		// ----------
	
		//FDTO_DrawStr( 2, 34, GuiLib_PS_ON, "size: %u, %u, %u, %u, %u  ",	sizeof( FOAL_COMM_MLB_STRUCT ),
		//															sizeof( FOAL_COMMANDS ),
		//															sizeof( BIG_BIT_FIELD_FOR_ILC_STRUCT ),
		//															sizeof( STATION_REPORT_DATA_RECORD ),
		//															sizeof( STATION_HISTORY_RECORD ) );
		//FDTO_DrawStr( 2, 34, GuiLib_PS_ON, "size: %u  ",	 sizeof(CHAIN_MEMBERS_STRUCT) );

	#endif



	//FDTO_DrawStr( 2, 24, GuiLib_PS_ON, "sizes: %u %u ", sizeof( BOOL_32 ), sizeof( BITFIELD_BOOL ) );

	//DFP_PS( 1, 24, GuiLib_PS_ON, "sizes: %u %u, %u ", ALERTS_MIN_STORED_BYTE_COUNT, sizeof( POC_MASTER_COUNT_STRUCT ), sizeof( poc_preserves.poc[ 0 ].raw_counts_at_master_ring ) );
	/*
	#include	"sxr_util.h"
	CS3000_SXR_UNION_STRUCT		sxru;
	sxru.turn_on.serial_number_u32 = 25;
	sxru.turn_off.serial_number_u32 = 27;
	DFP_PS( 1, 24, GuiLib_PS_ON, "values: %u, %u, %u ", sizeof(CS3000_SXR_UNION_STRUCT), sxru.turn_on.serial_number_u32, sxru.turn_off.serial_number_u32 );
	*/

	/*
	mts.dt_1.T = 0x12345678;
	mts.dt_1.D = 0x9ABC;
	mts.dt_2.T = 0x87654321;
	mts.dt_2.D = 0xCBA9;
	DFP_PS( 1, 24, GuiLib_PS_ON, "addresses: %08X, %08X, %08X, %08X", &mts.dt_1, &mts.dt_2, &mts.ul, &mts.fl );
	*/

	//DFP_PS( 1, 24, GuiLib_PS_ON, "sizes: %u", sizeof( STATION_STRUCT ) );  // to do this I placed a copy of the struct definition within this file!

	//DFP_PS( 1, 24, GuiLib_PS_ON, "sizes: %u, %u, %u", sizeof( STATION_REPORT_RECORD ), offsetof(STATION_REPORT_RECORD, rre_seconds_us), offsetof(STATION_REPORT_RECORD, pi_number_of_repeats) );

	//DFP_PS( 10, 20, GuiLib_PS_ON, "tracking bit field size : %u", sizeof( CENTRAL_COMPUTER_TRACKING_BITFIELD ) );

	//DFP_PS( 10, 20, GuiLib_PS_ON, "location of static flop registers" );

	//DFP_PS( 10, 20, GuiLib_PS_ON, "size_t size=%d ", sizeof( size_t ) );
	//DFP_PS( 10, 40, GuiLib_PS_ON, "long   size=%d ", sizeof( long ) );
	//DFP_PS( 10, 60, GuiLib_PS_ON, "ticktype size=%d ", sizeof( portTickType ) );
	//DFP_PS( 10, 60, GuiLib_PS_ON, "int size=%d ", sizeof( int ) );


	//DFP_PS( 10, 20, GuiLib_PS_ON, "ILB size=%d ", sizeof( IRRIGATION_LIST_BASICS ) );

	//DFP_PS( 10, 20, GuiLib_PS_ON, "TCB size=%d  reent=%d", sizeof(tskTCB), sizeof(struct _reent) );


	//DFP_PS( 10,  20, GuiLib_PS_ON, "line_fill_group: %d,   common_group: %d    ", sizeof(LINE_FILL_VALVE_CLOSE_STRUCT), sizeof(GROUP_BASE_DEFINITION_STRUCT) );
	//DFP_PS( 10,  32, GuiLib_PS_ON, "size of DF_DIR_ENTRY_s: %d   ", sizeof(DF_DIR_ENTRY_s) );
	//DFP_PS( 10,  64, GuiLib_PS_ON, "size of array of pointers %d   ", sizeof( GROUP_ss_from_tracking ) );
	//DFP_PS( 10,  110, GuiLib_PS_ON, "size of PAGE UNION: %d   ", sizeof(DF_PAGE_BUFFER_UNION) );

	//DFP_PS( 200, 28, GuiLib_PS_ON, "size=%d ", sizeof( Gl_DateTime ) );
	//DFP_PS( 200, 28, GuiLib_PS_ON, "size=%d ", sizeof( ConfigTable ) );
	//DFP_PS( 200, 38, GuiLib_PS_ON, "size=%d ", sizeof( UART_DEVICE_LEVELS ) );
	//DFP_PS( 200, 48, GuiLib_PS_ON, "size=%d ", sizeof( DEBUG_LINES_DEF ) );
	//DFP_PS( 200, 58, GuiLib_PS_ON, "size=%d ", sizeof( CHECKSUM ) );
	//DFP_PS( 200, 68, GuiLib_PS_ON, "size=%d ", sizeof( HUB_BITFIELD_STRUCT ) );
	//DFP_PS( 200, 78, GuiLib_PS_ON, "size=%d ", sizeof( MY_TABLE ) );
	//DFP_PS( 200, 28, GuiLib_PS_ON, "AlertID size=%d ", sizeof( ALERT_ID ) );
	//DFP_PS( 200, 28, GuiLib_PS_ON, "struct size=%d ", sizeof( ea_offsets ) );

	//DFP_PS( 200, 28, GuiLib_PS_ON, "size=%d, %d", sizeof( int ), sizeof( long ) );

	/*
	for( line_count_ul=0; line_count_ul<150; line_count_ul++ )
	{

		snprintf( str_128, sizeof(str_128), "LLLL %ld: here is a new line my friend...it needs to be long!!! OKAY?", line_count_ul );
		Alert_Message( str_128 );

	}
	*/

	//gpio_clr_gpio_pin( PIN_RRE_RESET_TO_RRE );  // scope probe

	// endless task loop to process all display commands for the application
	while( (true) )
	{
		if( xQueueReceive( Display_Command_Queue, &de, MS_to_TICKS( 500 ) ) == pdTRUE )
		{
			switch( de._01_command )
			{
				case DE_COMMAND_execute_func_with_no_arguments:
					(*de._04_func_ptr)();
					break;

				case DE_COMMAND_execute_func_with_two_u32_arguments:
					(*de._04_func_ptr)( de._06_u32_argument1, de._07_u32_argument2 );
					break;

				case DE_COMMAND_execute_func_with_single_u32_argument:
					(*de._04_func_ptr)( de._06_u32_argument1 );
					break;
			}
		}

		// ----------
		
		// 10/23/2012 rmd : Keep the activity indication going.
		task_last_execution_stamp[ task_index ] = xTaskGetTickCount();

		// ----------
		
	}  // of while forever

}  // of task

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

