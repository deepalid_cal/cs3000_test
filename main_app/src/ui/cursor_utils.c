/* file = curutil.c     052200  rmd   */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"cursor_utils.h"

#include	"guilib.h"

#include	"d_process.h"

#include	"limits.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Calls the GuiLib_Cursor_Up routine to move a easyGUI-managed cursor followed
 * by GuiLib_Refresh to redraw the cursor. Also handles all key beeps so no
 * additional calls to good_key_beep or bad_key_beep are necessary.
 * 
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user tries to move the cursor up.
 * 
 * @param psound_beep TRUE if a key beep should be played after the cursor is
 * moved; otherwise, FALSE. If set true, a good key beep will sound after a
 * successful move; otherwise, a bad key beep will sound.
 * 
 * @return BOOL_32 True or false to indicate whether the corsor movement was
 * successful or not.
 *
 * @author 8/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 FDTO_Cursor_Up( const UNS_32 psound_beep )
{
	BOOL_32	rv;

	rv = (false);

	if( GuiLib_Cursor_Up() == (true) )
	{
		if( psound_beep == (true) )
		{
			good_key_beep();
		}

		GuiLib_Refresh();

		rv = (true);
	}
	else
	{
		if( psound_beep == (true) )
		{
			bad_key_beep();
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Calls the GuiLib_Cursor_Down routine to move a easyGUI-managed cursor
 * followed by GuiLib_Refresh to redraw the cursor. Also handles all key beeps
 * so no additional calls to good_key_beep or bad_key_beep are necessary.
 * 
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user tries to move the cursor down.
 * 
 * @param psound_beep TRUE if a key beep should be played after the cursor is
 * moved; otherwise, FALSE. If set true, a good key beep will sound after a
 * successful move; otherwise, a bad key beep will sound.
 * 
 * @return BOOL_32 True or false to indicate whether the corsor movement was
 * successful or not.
 *
 * @author 8/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 FDTO_Cursor_Down( const UNS_32 psound_beep )
{
	BOOL_32	rv;

	rv = (false);

	if( GuiLib_Cursor_Down() == (true) )
	{
		if( psound_beep == (true) )
		{
			good_key_beep();
		}

		GuiLib_Refresh();

		rv = (true);
	}
	else
	{
		if( psound_beep == (true) )
		{
			bad_key_beep();
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Calls the GuiLib_Cursor_Select routine to move a easyGUI-managed cursor to
 * the specified cursor positions followed by GuiLib_Refresh to redraw the
 * cursor. Also handles all key beeps so no additional calls to good_key_beep or
 * bad_key_beep are necessary.
 * 
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user tries to move the cursor to a specific location.
 * 
 * @param psound_beep TRUE if a key beep should be played after the cursor is
 * moved; otherwise, FALSE. If set true, a good key beep will sound after a
 * successful move; otherwise, a bad key beep will sound.
 * 
 * @return BOOL_32 True or false to indicate whether the corsor movement was
 * successful or not.
 *
 * @author 8/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern BOOL_32 FDTO_Cursor_Select( const UNS_32 pnew_cursor_no, const UNS_32 psound_beep )
{
	BOOL_32	rv;

	rv = (false);

	if( GuiLib_Cursor_Select( (INT_16)pnew_cursor_no ) == (true) )
	{
		if( psound_beep == (true) )
		{
			good_key_beep();
		}

		GuiLib_Refresh();

		rv = (true);
	}
	else
	{
		if( psound_beep == (true) )
		{
			bad_key_beep();
		}
	}

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Safely calls the GuiLib_Cursor_Up routine to move a easyGUI-managed
 * cursor when not in the display processing task. Also handles all key beeps so
 * no additional calls to good_key_beep or bad_key_beep are necessary.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed from the KEY PROCESSING task when the user tries to
 * move the cursor up.
 * 
 * @param psound_beep True if a key beep should be played after the cursor is
 * moved; otherwise, false. If set true, a good key beep will sound after a
 * successful move; otherwise, a bad key beep will sound.
 *
 * @author 5/24/2011 Adrianusv
 *
 * @revisions (none)
 */
extern void CURSOR_Up( const BOOL_32 psound_beep )
{
	DISPLAY_EVENT_STRUCT lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_Cursor_Up;
	lde._06_u32_argument1 = psound_beep;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/**
 * Safely calls the GuiLib_Cursor_Down routine to move a easyGUI-managed
 * cursor when not in the display processing task. Also handles all key beeps so
 * no additional calls to good_key_beep or bad_key_beep are necessary.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed from the KEY PROCESSING task when the user tries
 * to move the cursor down.
 * 
 * @param psound_beep True if a key beep should be played after the cursor is
 * moved; otherwise, false. If set true, a good key beep will sound after a
 * successful move; otherwise, a bad key beep will sound.
 *
 * @author 5/24/2011 Adrianusv
 *
 * @revisions (none)
 */
extern void CURSOR_Down( const BOOL_32 psound_beep )
{
	DISPLAY_EVENT_STRUCT lde;

	lde._01_command = DE_COMMAND_execute_func_with_single_u32_argument;
	lde._04_func_ptr = (void*)&FDTO_Cursor_Down;
	lde._06_u32_argument1 = psound_beep;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/**
 * Safely calls the GuiLib_Cursor_Select routine to move a easyGUI-managed
 * cursor to the specified cursor position when not in the display processing
 * task. Also handles all key beeps so no additional calls to good_key_beep or
 * bad_key_beep are necessary.
 * 
 * @mutex_requirements (none)
 *
 * @memory_responsibilities (none)
 *
 * @task_execution Executed from the KEY PROCESSING task when the user tries to
 * move the cursor to a specific field.
 * 
 * @param pnew_cursor_no The number of the new cursor position to move to.
 * 
 * @param psound_beep True if a key beep should be played after the cursor is
 * moved; otherwise, false. If set true, a good key beep will sound after a
 * successful move; otherwise, a bad key beep will sound.
 *
 * @author 8/9/2012 Adrianusv
 *
 * @revisions (none)
 */
extern void CURSOR_Select( const UNS_32 pnew_cursor_no, const BOOL_32 psound_beep )
{
	DISPLAY_EVENT_STRUCT lde;

	lde._01_command = DE_COMMAND_execute_func_with_two_u32_arguments;
	lde._04_func_ptr = (void*)&FDTO_Cursor_Select;
	lde._06_u32_argument1 = pnew_cursor_no;
	lde._07_u32_argument2 = psound_beep;
	Display_Post_Command( &lde );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Sets the passed in Boolean value to true if it was false and vice versa.
 * 
 * Since easyGUI defines it's Bool values as GuiConst_INT8U (8-bit unsigned),
 * the parameter type is just that. Hopefully, this will be updated in a future
 * version.
 * 
 * @executed Executed within the context of the key processing task when the
 *           user presses a key on the keypad.
 * 
 * @param pvalue A pointer to the Boolean value.
 *
 * @author 8/12/2011 Adrianusv
 *
 * @revisions
 *    8/12/2011 Initial release
 */
extern void process_bool( BOOL_32 *pvalue )
{
	good_key_beep();
	
	if( *pvalue == (true) )
	{
		*pvalue = (false);
	}
	else
	{
		*pvalue = (true);
	}
}

/* ---------------------------------------------------------- */
/**
 * Increments or decrements the passed in character value based upon whether the user
 * pressed the Plus or Minus key. This is effectively a wrapper for the process_uns32
 * function that converts the character to its ASCII value equivalent callsed process_uns32,
 * and converts it back.
 * 
 * @executed Executed within the context of the key processing task when the user presses a
 * key on the keypad.
 * 
 * @param pkey_code The key that was pressed.
 * @param pvalue The character to increment or decrement. Note: We can't use UNS_8 for this
 * because easyGUI's characters are signed, not unsigned.
 * @param pmin The minimum allowed value.
 * @param pmax The maximum allowed value.
 * @param pinc_by The amount to increment or decrement the value by.
 * @param prollover True if the value should rollover when it reaches the minimum or
 *                  maximum; otherwise, false.
 *
 * @author 1/22/2018 Adrianusv
 *
 * @revisions
 *    1/22/2018 Initial release
 */
extern void process_char( const UNS_32 pkey_code, char *pvalue, const UNS_8 pmin, const UNS_8 pmax, const UNS_32 pinc_by, const BOOL_32 prollover )
{
	UNS_32	ascii_value_of_char;

	ascii_value_of_char = *pvalue;

	process_uns32( pkey_code, &ascii_value_of_char, (UNS_32)pmin, (UNS_32)pmax, pinc_by, prollover ); 

	*pvalue = (UNS_8)ascii_value_of_char;
}

/* ---------------------------------------------------------- */
/**
 * Increments or decrements the passed in value based upon whether the user
 * pressed the Plus or Minus key.
 * 
 * @executed Executed within the context of the key processing task when the
 *           user presses a key on the keypad.
 * 
 * @param pkey_code The key that was pressed.
 * @param pvalue The 32-bit unsigned integer to increment or decrement.
 * @param pmin The minimum allowed value.
 * @param pmax The maximum allowed value.
 * @param pinc_by The amount to increment or decrement the value by.
 * @param prollover True if the value should rollover when it reaches the
 *                  minimum or maximum; otherwise, false.
 *
 * @author 8/12/2011 Adrianusv
 *
 * @revisions
 *    8/12/2011 Initial release
 */
extern void process_uns32( const UNS_32 pkey_code, UNS_32 *pvalue, const UNS_32 pmin, const UNS_32 pmax, const UNS_32 pinc_by, const BOOL_32 prollover )
{
	if( pmin == pmax )
	{
		bad_key_beep();
	}
	else if( pkey_code == KEY_PLUS )
	{
		if( *pvalue == pmax )
		{
			if( prollover == (true) )
			{
				good_key_beep();
				*pvalue = pmin;
			}
			else
			{
				bad_key_beep();
			}
		}
		else if( *pvalue < pmin )
		{
			good_key_beep();
			*pvalue = pmin;
		}
		else
		{
			// Prevent overflow by comparing against unsigned long
			// limit
			if( (*pvalue < pmax - pinc_by) && ((UNS_64)(*pvalue + pinc_by) < UNS_32_MAX) )
			{
				*pvalue = *pvalue + pinc_by;
			}
			else
			{
				*pvalue = pmax;
			}
			good_key_beep();
		}
	}
	else if( pkey_code == KEY_MINUS )
	{
		if( *pvalue == pmin )
		{
			if( prollover == (true) )
			{
				good_key_beep();
				*pvalue = pmax;
			}
			else
			{
				bad_key_beep();
			}
		}
		else if( *pvalue > pmax )
		{
			good_key_beep();
			*pvalue = pmax;
		}
		else
		{
			if( *pvalue > pmin + pinc_by )
			{
				*pvalue = *pvalue - pinc_by;
			}
			else
			{
				*pvalue = pmin;
			}
			good_key_beep();
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Increments or decrements the passed in value based upon whether the user
 * pressed the Plus or Minus key.
 * 
 * @executed Executed within the context of the key processing task when the
 *           user presses a key on the keypad.
 * 
 * @param pkey_code The key that was pressed.
 * @param pvalue The 32-bit signed integer to increment or decrement.
 * @param pmin The minimum allowed value.
 * @param pmax The maximum allowed value.
 * @param pinc_by The amount to increment or decrement the value by.
 * @param prollover True if the value should rollover when it reaches the
 *                  minimum or maximum; otherwise, false.
 *
 * @author 8/12/2011 Adrianusv
 *
 * @revisions
 *    8/12/2011 Initial release
 */
extern void process_int32( const UNS_32 pkey_code, INT_32 *pvalue, const INT_32 pmin, const INT_32 pmax, const UNS_32 pinc_by, const BOOL_32 prollover )
{
	if( pkey_code == KEY_PLUS )
	{
		if ( *pvalue == pmax )
		{
			if( prollover == (true) )
			{
				good_key_beep();
				*pvalue = pmin;
			}
			else
			{
				bad_key_beep();
			}
		}
		else if( *pvalue < pmin )
		{
			good_key_beep();
			*pvalue = pmin;
		}
		else
		{
			good_key_beep();

			if( (INT_32)*pvalue < (INT_32)(pmax - pinc_by) )
			{
				*pvalue = *pvalue + pinc_by;
			}
			else
			{
				*pvalue = pmax;
			}
		}
	}
	else if( pkey_code == KEY_MINUS )
	{
		if( *pvalue == pmin )
		{
			if( prollover == (true) )
			{
				good_key_beep();
				*pvalue = pmax;
			}
			else
			{
				bad_key_beep();
			}
		}
		else if( *pvalue > pmax )
		{
			good_key_beep();
			*pvalue = pmax;
		}
		else
		{
			good_key_beep();

			// Prevent overflow by comparing against integer limit
			if( ((INT_32)*pvalue > (INT_32)(pmin + pinc_by)) && ((INT_64)(*pvalue - pinc_by) > INT_MIN) )
			{
				*pvalue = *pvalue - pinc_by;
			}
			else
			{
				*pvalue = pmin;
			}
		}
	}
}

/* ---------------------------------------------------------- */
/**
 * Increments or decrements the passed in value based upon whether the user
 * pressed the Plus or Minus key.
 * 
 * @executed Executed within the context of the key processing task when the
 *           user presses a key on the keypad.
 * 
 * @param pkey_code The key that was pressed.
 * @param pvalue The floating-point number to increment or decrement.
 * @param pmin The minimum allowed value.
 * @param pmax The maximum allowed value.
 * @param pinc_by The amount to increment or decrement the value by.
 * @param prollover True if the value should rollover when it reaches the
 *                  minimum or maximum; otherwise, false.
 *
 * @author 8/12/2011 Adrianusv
 *
 * @revisions
 *    8/12/2011 Initial release
 */
extern void process_fl( const UNS_32 pkey_code, float *pvalue, const float pmin, const float pmax, const float pinc_by, const BOOL_32 prollover )
{
	if( pkey_code == KEY_PLUS )
	{
		if( *pvalue == pmax )
		{
			if( prollover == (true) )
			{
				good_key_beep();
				*pvalue = pmin;
			}
			else
			{
				bad_key_beep();
			}
		}
		else if( *pvalue < pmin )
		{
			good_key_beep();
			*pvalue = pmin;
		}
		else
		{
			if( *pvalue < pmax - pinc_by )
			{
				*pvalue = *pvalue + pinc_by;
			}
			else
			{
				*pvalue = pmax;
			}
			good_key_beep();
		}
	}
	else if( pkey_code == KEY_MINUS )
	{
		if( *pvalue == pmin )
		{
			if( prollover == (true) )
			{
				good_key_beep();
				*pvalue = pmax;
			}
			else
			{
				bad_key_beep();
			}
		}
		else if( *pvalue > pmax )
		{
			good_key_beep();
			*pvalue = pmax;
		}
		else
		{
			if( *pvalue > pmin + pinc_by )
			{
				*pvalue = *pvalue - pinc_by;
			}
			else
			{
				*pvalue = pmin;
			}
			good_key_beep();
		}
	}
}

/* ---------------------------------------------------------- */
// 12/17/2015 skc : Perform truncation on pvalue, i.e. if pinc_by is 1000, truncate pvalue to ___000.
extern void process_uns32_r( const UNS_32 pkey_code, UNS_32 *pvalue, const UNS_32 pmin, const UNS_32 pmax, const UNS_32 pinc_by, const BOOL_32 prollover)
{
	process_uns32( pkey_code, pvalue, pmin, pmax, pinc_by, prollover ); 

	// subtract any remainder to make nice, neat numbers
	*pvalue -= (*pvalue % pinc_by);
}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

