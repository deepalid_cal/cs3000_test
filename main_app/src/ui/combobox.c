/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"combobox.h"

#include	"d_process.h"

#include	"GuiLib.h"

#include	"screen_utils.h"

#include	"scrollbox.h"

#include	"speaker.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/**
 * Draws a generic no/yes ComboBox atop the main window at the specified coordinates.
 * 
 * @task_execution Executed within the context of the DISPLAY PROCESSING task when the user
 * presses SELECT on a field of a no/yes question.
 * 
 * @param px_coord The x-coordinate of the top-left corner of the window. This should be 2
 * pixels less than the top-left corner of the background box around the text in easyGUI.
 * 
 * @param py_coord The y-coordinate of the top-left corner of the window. This should be 2
 * pixels less than the top-left corner of the background box around the text in easyGUI.
 * 
 * @param pselected_item The value to default to combo box to.
 */
extern void FDTO_COMBO_BOX_show_no_yes_dropdown( const UNS_32 px_coord, const UNS_32 py_coord, const UNS_32 pselected_item )
{
	GuiVar_ComboBox_X1 = px_coord;
	GuiVar_ComboBox_Y1 = py_coord;

	FDTO_COMBOBOX_show( GuiStruct_cbxNoYes_0, &FDTO_COMBOBOX_add_items, 2, pselected_item );
}

/* ---------------------------------------------------------- */
/**
 * Adds items to a ComboBox. This is really just used as a callback function in the
 * call to build the listbox. This cannot be used to do any custom processing 
 * 
 * @task_execution Executed within the context of the DISPLAY PROCESSING task when a CombBox
 * is displayed.
 * 
 * @param pindex_0_i16 The index of the line to populate - 0-based.
 */
extern void FDTO_COMBOBOX_add_items( const INT_16 pindex_0_i16 )
{
	GuiVar_ComboBoxItemIndex = (UNS_32)pindex_0_i16;
}

/* ---------------------------------------------------------- */
/**
 * Returns the index of the selected item.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task when the user
 * closes the combobox using BACK or SELECT.
 * 
 * @return UNS_32 The index of the selected item.
 */
static UNS_32 COMBOBOX_get_item_index( void )
{
	UNS_32	rv;

	rv = GuiLib_ScrollBox_GetActiveLine( 0, 0 );

	return( rv );
}

/* ---------------------------------------------------------- */
/**
 * Refreshes the item contained at the specified location.
 * 
 * @param pline_index The index of the specified item to redraw.
 */
extern void COMBOBOX_refresh_item( const UNS_32 pline_index )
{
	SCROLL_BOX_redraw_line( 0, pline_index );
}

/* ---------------------------------------------------------- */
/**
 * Refreshes all ComboBox items.
 */
extern void COMBOBOX_refresh_items( void )
{
	SCROLL_BOX_redraw( 0 );
}

/* ---------------------------------------------------------- */
/**
 * Displays the specified ComboBox atop the current active window.
 * 
 * @task_execution Executed within the context of the DISPLAY PROCESSING task.
 * 
 * @param pcombobox_struct_index The index of the easyGUI combobox structure to draw.
 * 
 * @param pcombobox_data_func_ptr A pointer to the callback function to populate the combo
 * box items.
 * 
 * @param pmax_dropdown_items The maximum number of items to be shown in the drop-down
 * portion of the ComboBox.
 * 
 * @param pselected_item The index of the currently selected item in the ComboBox.
 */
extern void FDTO_COMBOBOX_show( const UNS_32 pcombobox_struct_index, void (*pcombobox_data_func_ptr)(INT_16 pline_index_0), const UNS_32 pmax_dropdown_items, const UNS_32 pselected_item )
{
	// Retain the active cursor position, even though the combo box doesn't use
	// it, to ensure that appropriate cursor field is highlighted when the combo
	// box is closed.
	GuiLib_ShowScreen( (UNS_16)pcombobox_struct_index, GuiLib_ActiveCursorFieldNo, GuiLib_RESET_AUTO_REDRAW );
	GuiLib_ScrollBox_Init_Custom_SetTopLine( 0, pcombobox_data_func_ptr, (INT_16)pmax_dropdown_items, (INT_16)pselected_item, (INT_16)pselected_item );
	GuiLib_Refresh();
}

/* ---------------------------------------------------------- */
/**
 * Hides the currently displayed combo box and redraws the screen if specified.
 * 
 * @task_execution Executed within the context of the DISPLAY PROCESSING task
 * when the user presses a key to close the combo box - typically either SELECT
 * or BACK.
 */
extern void FDTO_COMBOBOX_hide( void )
{
	GuiLib_ScrollBox_Close( 0 );

	FDTO_Redraw_Screen( (false) );
}

/* ---------------------------------------------------------- */
/**
 * Occurs when a keypad button is pressed while the control has focus.
 * 
 * @task_execution Executed within the context of the KEY PROCESSING task when the user
 * presses a key while a combo box is active.
 * 
 * @param pkeycode The key the user pressed.
 * 
 * @param pitem_to_set_ptr A pointer to the GuiVar variable to set if/when the combobox is 
 * closed. 
 */
extern void COMBO_BOX_key_press( const UNS_32 pkeycode, UNS_32 *pitem_to_set_ptr )
{
	DISPLAY_EVENT_STRUCT	lde;

	switch( pkeycode )
	{
		case KEY_C_UP:
		case KEY_C_DOWN:
		case KEY_PLUS:
		case KEY_MINUS:
			SCROLL_BOX_up_or_down( 0, pkeycode );
			break;

		case KEY_SELECT:
		case KEY_BACK:
			good_key_beep();

			if( pitem_to_set_ptr != NULL )
			{
				*pitem_to_set_ptr = COMBOBOX_get_item_index();
			}

			lde._01_command = DE_COMMAND_execute_func_with_no_arguments;
			lde._04_func_ptr = FDTO_COMBOBOX_hide;
			Display_Post_Command( &lde );
			break;

		default:
			bad_key_beep();
	}
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

