/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef	_INC_COMBO_BOX_H
#define	_INC_COMBO_BOX_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

extern void FDTO_COMBO_BOX_show_no_yes_dropdown( const UNS_32 px_coord, const UNS_32 py_coord, const UNS_32 pselected_item );

// ----------

extern void FDTO_COMBOBOX_add_items( const INT_16 pindex_0_i16 );

extern void COMBOBOX_refresh_item( const UNS_32 pline_index );

extern void COMBOBOX_refresh_items( void );

// ----------

extern void FDTO_COMBOBOX_show( const UNS_32 pcombobox_struct_index, void (*pcombobox_data_func_ptr)(INT_16 pline_index_0), const UNS_32 pmax_dropdown_items, const UNS_32 pselected_item );

extern void FDTO_COMBOBOX_hide( void );

// ----------

extern void COMBO_BOX_key_press( const UNS_32 pkeycode, UNS_32 *pitem_to_set_ptr );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

