/*  file = curutil.h    052200  rmd   */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_CURUTIL_H
#define _INC_CURUTIL_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"lpc_types.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


extern BOOL_32 FDTO_Cursor_Select( const UNS_32 pnew_cursor_no, const UNS_32 psound_beep );

extern BOOL_32 FDTO_Cursor_Down( const UNS_32 psound_beep );

extern BOOL_32 FDTO_Cursor_Up( const UNS_32 psound_beep );

extern void CURSOR_Up( const BOOL_32 psound_beep );

extern void CURSOR_Down( const BOOL_32 psound_beep );

extern void CURSOR_Select( const UNS_32 pnew_cursor_field_no, const BOOL_32 psound_beep );

// ----------

extern void process_bool( BOOL_32 *pvalue );

extern void process_char( const UNS_32 pkey_code, char *pvalue, const UNS_8 pmin, const UNS_8 pmax, const UNS_32 pinc_by, const BOOL_32 prollover );

extern void process_uns32( const UNS_32 pkey_code, UNS_32 *pvalue, const UNS_32 pmin, const UNS_32 pmax, const UNS_32 pinc_by, const BOOL_32 prollover);

extern void process_int32( const UNS_32 pkey_code, INT_32 *pvalue, const INT_32 pmin, const INT_32 pmax, const UNS_32 pinc_by, const BOOL_32 prollover );

extern void process_fl( const UNS_32 pkey_code, float *pvalue, const float pmin, const float pmax, const float pinc_by, const BOOL_32 prollover );

/* ---------------------------------------------------------- */
// 12/17/2015 skc : Perform rounding on pvalue, i.e. if pinc_by is 1000, round pvalue to ___000
extern void process_uns32_r( const UNS_32 pkey_code, UNS_32 *pvalue, const UNS_32 pmin, const UNS_32 pmax, const UNS_32 pinc_by, const BOOL_32 prollover);

/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

