/******************************************************************************
  Target Script for a Phytec LPC3250 board.

  Copyright (c) 2009 Rowley Associates Limited.

  This file may be distributed under the terms of the License Agreement
  provided with this software.

  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE
  WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 ******************************************************************************/


var HCLKDIV_CTRL = 0x40004040
var PWR_CTRL = 0x40004044
var HCLKPLL_CTRL = 0x40004058
var SDRAMCLK_CTRL = 0x40004068
//
var EMCControl = 0x31080000
var EMCConfig = 0x31080008
var EMCDynamicControl = 0x31080020
var EMCDynamicRefresh = 0x31080024
var EMCDynamicReadConfig = 0x31080028
var EMCDynamictRP = 0x31080030
var EMCDynamictRAS = 0x31080034
var EMCDynamictSREX = 0x31080038
var EMCDynamictWR = 0x31080044
var EMCDynamictRC = 0x31080048
var EMCDynamictRFC = 0x3108004C
var EMCDynamictXSR = 0x31080050
var EMCDynamictRRD = 0x31080054
var EMCDynamictMRD = 0x31080058
var EMCDynamictCDLR = 0x3108005C
//
var EMCDynamicConfig0 = 0x31080100
var EMCDynamicRasCas0 = 0x31080104
//
var EMCStaticConfig0 = 0x31080200
//
var EMCAHBControl0 = 0x31080400
var EMCCAHBTimeOut0 = 0x31080408
//
var EMCAHBControl2 = 0x31080440
var EMCCAHBTimeOut2 = 0x31080448
//
var EMCAHBControl3 = 0x31080460
var EMCCAHBTimeOut3 = 0x31080468
//
var EMCAHBControl4 = 0x31080480
var EMCCAHBTimeOut4 = 0x31080488

function InitSDRAMCLOCKS()
{   
    // START CALSENSE 2M x 32 (8MByte) SDR 3.3VDC SDRAM

    TargetInterface.pokeWord(HCLKDIV_CTRL, (15<<2)|(1<<0)); // PERIPH_CLK is ARM PLL Clock/16; HCLK is ARM PLL Clock/2
    TargetInterface.pokeWord(HCLKPLL_CTRL, (1<<16)|(1<<14)|(15<<1)); // Enable|ByPass Post Divider|M==16 /*(1<<16)|(1<<14)|(1<<13)|(15<<1)); // Enable|ByPass Post Divider|Feedback Divider clock by PLL_CLKOUT|M==16 */
    TargetInterface.delay(10);//while(!(HCLKPLL_CTRL&1)); // Wait for PLL to Lock
    TargetInterface.pokeWord(PWR_CTRL, 0x00000016)/*(1<<2)); //  Normal Run mode*/

    TargetInterface.pokeWord(EMCControl, 0x00000001); // SDRAM enabled
    TargetInterface.pokeWord(EMCConfig, 0x00000000); // Little endian mode

    TargetInterface.pokeWord(EMCDynamicRefresh, 0x000007ff);  // a VERY long refresh time so the controller isn't always busy refreshing 

    TargetInterface.pokeWord(SDRAMCLK_CTRL, 0x0001C000);// HCLKDELAY_DELAY = 7

    TargetInterface.pokeWord(EMCDynamicConfig0, 0x00004302); // 32-bit 3.3vdc sdr sdram SDRAM 64MB(2M*32)
    TargetInterface.pokeWord(EMCDynamicRasCas0, 0x00000302);
    TargetInterface.pokeWord(EMCDynamicReadConfig, 0x00000011);

    TargetInterface.pokeWord(EMCDynamictRP, 0x00000001);
    TargetInterface.pokeWord(EMCDynamictRAS, 0x00000004);
    TargetInterface.pokeWord(EMCDynamictSREX, 0x00000006);
    TargetInterface.pokeWord(EMCDynamictWR, 0x00000001);
    TargetInterface.pokeWord(EMCDynamictRC, 0x00000007);
    TargetInterface.pokeWord(EMCDynamictRFC, 0x00000006);
    TargetInterface.pokeWord(EMCDynamictXSR, 0x00000006);
    TargetInterface.pokeWord(EMCDynamictRRD, 0x00000001);
    TargetInterface.pokeWord(EMCDynamictMRD, 0x00000000);
    TargetInterface.pokeWord(EMCDynamictCDLR, 0x00000000);

    TargetInterface.pokeWord(EMCDynamicControl, 0x00000193); // SDRAM NOP|CS|CE /*0x00000193); // SDRAM NOP|IMMC|CS|CE*/
    TargetInterface.delay(100);
    TargetInterface.pokeWord(EMCDynamicControl, 0x00000113); // SDRAM PALL|CS|CE /* 0x00000113); // SDRAM PALL|IMMC|CS|CE*/
    TargetInterface.pokeWord(EMCDynamicRefresh, 0x00000000); // CS
    TargetInterface.delay(100);
    TargetInterface.pokeWord(EMCDynamicRefresh, 0x00000032/*0x00000065*/);

    TargetInterface.pokeWord(EMCDynamicControl, 0x00000093); // SDRAM NORMAL|CS|CE /*0x00000093); // SDRAM NORMAL|IMMC|CS|CE*/
    TargetInterface.peekWord(0x80030000);

    TargetInterface.pokeWord(EMCDynamicControl, 0x00000018); // SDRAM NORMAL|CS|CE /*0x00000010); // SDRAM NORMAL|IMMC*/

    TargetInterface.pokeWord(EMCAHBControl0, 0x00000001); // Enable buffers in AHB ports
    TargetInterface.pokeWord(EMCAHBControl2, 0x00000001); // Enable
    TargetInterface.pokeWord(EMCAHBControl3, 0x00000001); // Enable
    TargetInterface.pokeWord(EMCAHBControl4, 0x00000001); // Enable

    TargetInterface.pokeWord(EMCAHBTimeOut0, 0x00000020);
    TargetInterface.pokeWord(EMCAHBTimeOut2, 0x00000020);
    TargetInterface.pokeWord(EMCAHBTimeOut3, 0x00000020);
    TargetInterface.pokeWord(EMCAHBTimeOut4, 0x00000020);

    // END CALSENSE 2M x 32 (8MByte) SDR 3.3VDC SDRAM




    // PHYTEC BOARD SDRAM INIT
  //TargetInterface.pokeWord(SDRAMCLK_CTRL, 0x0001C000);// HCLKDELAY_DELAY = 7
  //TargetInterface.pokeWord(EMCControl, 0x00000001); // SDRAM enabled
  //TargetInterface.pokeWord(EMCConfig, 0x00000000); // Little endian mode
  //TargetInterface.pokeWord(EMCAHBControl0, 0x00000001); // Enable
  //TargetInterface.pokeWord(EMCAHBControl2, 0x00000001); // Enable
  //TargetInterface.pokeWord(EMCAHBControl3, 0x00000001); // Enable
  //TargetInterface.pokeWord(EMCAHBControl4, 0x00000001); // Enable
  //TargetInterface.pokeWord(EMCDynamicConfig0, 0x00005682/*0x00005482*/); // 32-bit low-power SDRAM 128MB(4M*32) | low power SDR SDRAM
  //TargetInterface.pokeWord(EMCDynamicRasCas0, 0x00000302/*0x00000303*/);
  //TargetInterface.pokeWord(EMCDynamicReadConfig, 0x00000011);
  //TargetInterface.pokeWord(EMCDynamictRP, 0x00000001/*0x00000002*/);
  //TargetInterface.pokeWord(EMCDynamictRAS, 0x00000004/*0x00000005*/);
  //TargetInterface.pokeWord(EMCDynamictSREX, 0x00000008);
  //TargetInterface.pokeWord(EMCDynamictWR, 0x00000001);
  //TargetInterface.pokeWord(EMCDynamictRC, 0x00000007/*0x00000008*/);
  //TargetInterface.pokeWord(EMCDynamictRFC, 0x00000008);
  //TargetInterface.pokeWord(EMCDynamictXSR, 0x00000008);
  //TargetInterface.pokeWord(EMCDynamictRRD, 0x00000002/*0x00000001*/);
  //TargetInterface.pokeWord(EMCDynamictMRD, 0x00000002/*0x00000001*/);
  //TargetInterface.pokeWord(EMCDynamictCDLR, 0x00000001/*0x00000000*/);
  //TargetInterface.pokeWord(EMCDynamicControl, 0x00000183); // SDRAM NOP|CS|CE /*0x00000193); // SDRAM NOP|IMMC|CS|CE*/
  //TargetInterface.delay(100);
  //TargetInterface.pokeWord(EMCDynamicControl, 0x00000112); // SDRAM PALL|CS|CE /* 0x00000113); // SDRAM PALL|IMMC|CS|CE*/
  //TargetInterface.pokeWord(EMCDynamicRefresh, 0x00000002); // CS
  //TargetInterface.delay(100);
  //TargetInterface.pokeWord(EMCDynamicRefresh, 0x00000032/*0x00000065*/);
  //TargetInterface.pokeWord(MPMCDynamicRasCas0, 0x00000303);
  //TargetInterface.pokeWord(MPMCDynamicConfig0, 0x00005482);
  //TargetInterface.pokeWord(EMCDynamicControl, 0x00000083); // SDRAM NORMAL|CS|CE /*0x00000093); // SDRAM NORMAL|IMMC|CS|CE*/
  //TargetInterface.peekWord(0x80018000);
  //TargetInterface.pokeWord(EMCDynamicControl, 0x00000083); // SDRAM NORMAL|CS|CE /*0x00000093); // SDRAM NORMAL|IMMC|CS|CE*/
  //TargetInterface.peekWord(0x8102C000);
  //TargetInterface.pokeWord(EMCDynamicControl, 0x00000003); // SDRAM NORMAL|CS|CE /*0x00000010); // SDRAM NORMAL|IMMC*/
  //TargetInterface.pokeWord(EMCAHBTimeOut0, 0x00000064);
  //TargetInterface.pokeWord(EMCAHBTimeOut2, 0x00000190);
  //TargetInterface.pokeWord(EMCAHBTimeOut3, 0x00000190);
  //TargetInterface.pokeWord(EMCAHBTimeOut4, 0x00000190);

  //TargetInterface.pokeWord(HCLKDIV_CTRL, (15<<2)|(1<<0)); // PERIPH_CLK is ARM PLL Clock/16; HCLK is ARM PLL Clock/2
  //TargetInterface.pokeWord(HCLKPLL_CTRL, (1<<16)|(1<<14)|(15<<1)); // Enable|ByPass Post Divider|M==16 /*(1<<16)|(1<<14)|(1<<13)|(15<<1)); // Enable|ByPass Post Divider|Feedback Divider clock by PLL_CLKOUT|M==16 */
  //TargetInterface.delay(10);//while(!(HCLKPLL_CTRL&1)); // Wait for PLL to Lock
  //TargetInterface.pokeWord(PWR_CTRL, 0x00000016)/*(1<<2)); //  Normal Run mode*/
}

function Reset()
{
  TargetInterface.resetAndStop(1)
  // if a normal (not service) boot has occured then the user program may have run so
  // turn off the caches
  i = TargetInterface.executeMRC(0xEE010F00);
  TargetInterface.executeMCR(0xEE010F00, i & ~(1<<0|1<<2|1<<12));
  // clear out debug comms port
  TargetInterface.getICEBreakerRegister(5);
  InitSDRAMCLOCKS();  
  TargetInterface.pokeWord(0x40028004, 1<<19); // enable NAND flash write
}

function Reset2()
{
  TargetInterface.resetAndStop(1)
  // if a normal (not service) boot has occured then the user program may have run so
  // turn off the caches
  i = TargetInterface.executeMRC(0xEE010F00);
  TargetInterface.executeMCR(0xEE010F00, i & ~(1<<0|1<<2|1<<12));
  // clear out debug comms port
  TargetInterface.getICEBreakerRegister(5);
  // if a service boot has occured then
  TargetInterface.pokeWord(EMCStaticConfig0, 0x00000082) //  32-bit NOR flash
}
