/****************************************************************************
 *
 *            Copyright (c) 2007 by HCC Embedded
 *
 * This software is copyrighted by and is the sole property of
 * HCC.  All rights, title, ownership, or other interests
 * in the software remain the property of HCC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of HCC.
 *
 * HCC reserves the right to modify this software without notice.
 *
 * HCC Embedded
 * Budapest 1133
 * Vaci ut 76
 * Hungary
 *
 * Tel:  +36 (1) 450 1302
 * Fax:  +36 (1) 450 1303
 * http: www.hcc-embedded.com
 * email: info@hcc-embedded.com
 *
 ***************************************************************************/
#ifndef _LPC3250_REGS_H_
#define _LPC3250_REGS_H_

#include "hcc_types.h"

#define hcc_reg32	volatile hcc_u32
#define hcc_reg16	volatile hcc_u16
#define hcc_reg8	volatile hcc_u8

#define SYS_BASE          0x40004000u /* System base register address */
#define USB_BASE          0x31020000u /* USB device interface */

#define BIT0    (1u<<0)
#define BIT1    (1u<<1)
#define BIT2    (1u<<2)
#define BIT3    (1u<<3)
#define BIT4    (1u<<4)
#define BIT5    (1u<<5)
#define BIT6    (1u<<6)
#define BIT7    (1u<<7)
#define BIT8    (1u<<8)
#define BIT9    (1u<<9)
#define BIT10   (1u<<10)
#define BIT11   (1u<<11)
#define BIT12   (1u<<12)
#define BIT13   (1u<<13)
#define BIT14   (1u<<14)
#define BIT15   (1u<<15)
#define BIT16   (1u<<16)
#define BIT17   (1u<<17)
#define BIT18   (1u<<18)
#define BIT19   (1u<<19)
#define BIT20   (1u<<20)
#define BIT21   (1u<<21)
#define BIT22   (1u<<22)
#define BIT23   (1u<<23)
#define BIT24   (1u<<24)
#define BIT25   (1u<<25)
#define BIT26   (1u<<26)
#define BIT27   (1u<<27)
#define BIT28   (1u<<28)
#define BIT29   (1u<<29)
#define BIT30   (1u<<30)
#define BIT31   (1u<<31)

#define BIT_FIELD(width)  ((1u<<(width))-1)

/*****************************************************************************/
/***************************** CLOCK CONTROL *********************************/
/*****************************************************************************/

#define HCLKDIVCTRL  (*(hcc_reg32 *) (SYS_BASE + 0x40))
#define PWRCTRL      (*(hcc_reg32 *) (SYS_BASE + 0x44))
#define PLL397CTRL   (*(hcc_reg32 *) (SYS_BASE + 0x48))
#define OSCCTRL      (*(hcc_reg32 *) (SYS_BASE + 0x4C))
#define SYSCLKCTRL   (*(hcc_reg32 *) (SYS_BASE + 0x50))
#define HCLKPLLCTRL  (*(hcc_reg32 *) (SYS_BASE + 0x58))

/* Clock control registers */
/*USB Clock Control */
#define USBCLKCTRL (*(hcc_reg32 *)(0x40004064u))
#define USBCLKDIV  (*(hcc_reg32 *)(0x4000401Cu))

/*****************************************************************************/
/********************************** USB **************************************/
/*****************************************************************************/
/*USB Device Interrupt Status */
#define USBDEVINTST (*(hcc_reg32 *)(USB_BASE+0x200))
#define USBDEVINTST_FRAME BIT0
#define USBDEVINTST_EP_FAST BIT1
#define USBDEVINTST_EP_SLOW BIT2
#define USBDEVINTST_DEV_STAT BIT3
#define USBDEVINTST_CCEMPTY BIT4
#define USBDEVINTST_CDFULL BIT5
#define USBDEVINTST_RXENDPKT BIT6
#define USBDEVINTST_TXENDPKT BIT7
#define USBDEVINTST_EP_RLZED BIT8
#define USBDEVINTST_ERR_INT BIT9

/*USB Device Interrupt Enable */
#define USBDEVINTEN (*(hcc_reg32 *)(USB_BASE+0x204))
#define USBDEVINTEN_FRAME BIT0
#define USBDEVINTEN_EP_FAST BIT1
#define USBDEVINTEN_EP_SLOW BIT2
#define USBDEVINTEN_DEV_STAT BIT3
#define USBDEVINTEN_CCEMPTY BIT4
#define USBDEVINTEN_CDFULL BIT5
#define USBDEVINTEN_RXENDPKT BIT6
#define USBDEVINTEN_TXENDPKT BIT7
#define USBDEVINTEN_EP_RLZED BIT8
#define USBDEVINTEN_EPR_INT BIT9

/*USB Device Interrupt Clear */
#define USBDEVINTCLR (*(hcc_reg32 *)(USB_BASE+0x208))
#define USBDEVINTCLR_FRAME BIT0
#define USBDEVINTCLR_EP_FAST BIT1
#define USBDEVINTCLR_EP_SLOW BIT2
#define USBDEVINTCLR_DEV_STAT BIT3
#define USBDEVINTCLR_CCEMPTY BIT4
#define USBDEVINTCLR_CDFULL BIT5
#define USBDEVINTCLR_RXENDPKT BIT6
#define USBDEVINTCLR_TXENDPKT BIT7
#define USBDEVINTCLR_EP_RLZED BIT8
#define USBDEVINTCLR_ERR_INT BIT9

/*USB Device Interrupt Set */
#define USBDEVINTSET (*(hcc_reg32 *)(USB_BASE+0x20C))
#define USBDEVINTSET_FRAME BIT0
#define USBDEVINTSET_EP_FAST BIT1
#define USBDEVINTSET_EP_SLOW BIT2
#define USBDEVINTSET_DEV_STAT BIT3
#define USBDEVINTSET_CCEMPTY BIT4
#define USBDEVINTSET_CDFULL BIT5
#define USBDEVINTSET_RXENDPKT BIT6
#define USBDEVINTSET_TXENDPKT BIT7
#define USBDEVINTSET_EP_RLZED BIT8
#define USBDEVINTSET_ERR_INT BIT9

/*USB Device Interrupt Priority */
#define USBDEVINTPRI (*(hcc_reg8 *)(USB_BASE+0x22C))
#define USBDEVINTPRI_FRAME BIT0
#define USBDEVINTPRI_EP_FAST BIT1

/* Endpoint interrupt registers */
/*USB Endpoint Interrupt Status */
#define USBEPINTST (*(hcc_reg32 *)(USB_BASE+0x230))
#define USBEPINTST_EP0RX BIT0
#define USBEPINTST_EP0TX BIT1
#define USBEPINTST_EP1RX BIT2
#define USBEPINTST_EP1TX BIT3
#define USBEPINTST_EP2RX BIT4
#define USBEPINTST_EP2TX BIT5
#define USBEPINTST_EP3RX BIT6
#define USBEPINTST_EP3TX BIT7
#define USBEPINTST_EP4RX BIT8
#define USBEPINTST_EP4TX BIT9
#define USBEPINTST_EP5RX BIT10
#define USBEPINTST_EP5TX BIT11
#define USBEPINTST_EP6RX BIT12
#define USBEPINTST_EP6TX BIT13
#define USBEPINTST_EP7RX BIT14
#define USBEPINTST_EP7TX BIT15
#define USBEPINTST_EP8RX BIT16
#define USBEPINTST_EP8TX BIT17
#define USBEPINTST_EP9RX BIT18
#define USBEPINTST_EP9TX BIT19
#define USBEPINTST_EP10RX BIT20
#define USBEPINTST_EP10TX BIT21
#define USBEPINTST_EP11RX BIT22
#define USBEPINTST_EP11TX BIT23
#define USBEPINTST_EP12RX BIT24
#define USBEPINTST_EP12TX BIT25
#define USBEPINTST_EP13RX BIT26
#define USBEPINTST_EP13TX BIT27
#define USBEPINTST_EP14RX BIT28
#define USBEPINTST_EP14TX BIT29
#define USBEPINTST_EP15RX BIT30
#define USBEPINTST_EP15TX BIT31

/*USB Endpoint Interrupt Enable */
#define USBEPINTEN (*(hcc_reg32 *)(USB_BASE+0x234))
#define USBEPINTEN_EP0RX BIT0
#define USBEPINTEN_EP0TX BIT1
#define USBEPINTEN_EP1RX BIT2
#define USBEPINTEN_EP1TX BIT3
#define USBEPINTEN_EP2RX BIT4
#define USBEPINTEN_EP2TX BIT5
#define USBEPINTEN_EP3RX BIT6
#define USBEPINTEN_EP3TX BIT7
#define USBEPINTEN_EP4RX BIT8
#define USBEPINTEN_EP4TX BIT9
#define USBEPINTEN_EP5RX BIT10
#define USBEPINTEN_EP5TX BIT11
#define USBEPINTEN_EP6RX BIT12
#define USBEPINTEN_EP6TX BIT13
#define USBEPINTEN_EP7RX BIT14
#define USBEPINTEN_EP7TX BIT15
#define USBEPINTEN_EP8RX BIT16
#define USBEPINTEN_EP8TX BIT17
#define USBEPINTEN_EP9RX BIT18
#define USBEPINTEN_EP9TX BIT19
#define USBEPINTEN_EP10RX BIT20
#define USBEPINTEN_EP10TX BIT21
#define USBEPINTEN_EP11RX BIT22
#define USBEPINTEN_EP11TX BIT23
#define USBEPINTEN_EP12RX BIT24
#define USBEPINTEN_EP12TX BIT25
#define USBEPINTEN_EP13RX BIT26
#define USBEPINTEN_EP13TX BIT27
#define USBEPINTEN_EP14RX BIT28
#define USBEPINTEN_EP14TX BIT29
#define USBEPINTEN_EP15RX BIT30
#define USBEPINTEN_EP15TX BIT31

/*USB Endpoint Interrupt Clear */
#define USBEPINTCLR (*(hcc_reg32 *)(USB_BASE+0x238))
#define USBEPINTCLR_EP0RX BIT0
#define USBEPINTCLR_EP0TX BIT1
#define USBEPINTCLR_EP1RX BIT2
#define USBEPINTCLR_EP1TX BIT3
#define USBEPINTCLR_EP2RX BIT4
#define USBEPINTCLR_EP2TX BIT5
#define USBEPINTCLR_EP3RX BIT6
#define USBEPINTCLR_EP3TX BIT7
#define USBEPINTCLR_EP4RX BIT8
#define USBEPINTCLR_EP4TX BIT9
#define USBEPINTCLR_EP5RX BIT10
#define USBEPINTCLR_EP5TX BIT11
#define USBEPINTCLR_EP6RX BIT12
#define USBEPINTCLR_EP6TX BIT13
#define USBEPINTCLR_EP7RX BIT14
#define USBEPINTCLR_EP7TX BIT15
#define USBEPINTCLR_EP8RX BIT16
#define USBEPINTCLR_EP8TX BIT17
#define USBEPINTCLR_EP9RX BIT18
#define USBEPINTCLR_EP9TX BIT19
#define USBEPINTCLR_EP10RX BIT20
#define USBEPINTCLR_EP10TX BIT21
#define USBEPINTCLR_EP11RX BIT22
#define USBEPINTCLR_EP11TX BIT23
#define USBEPINTCLR_EP12RX BIT24
#define USBEPINTCLR_EP12TX BIT25
#define USBEPINTCLR_EP13RX BIT26
#define USBEPINTCLR_EP13TX BIT27
#define USBEPINTCLR_EP14RX BIT28
#define USBEPINTCLR_EP14TX BIT29
#define USBEPINTCLR_EP15RX BIT30
#define USBEPINTCLR_EP15TX BIT31

/*USB Endpoint Interrupt Set */
#define USBEPINTSET (*(hcc_reg32 *)(USB_BASE+0x23C))
#define USBEPINTSET_EP0RX BIT0
#define USBEPINTSET_EP0TX BIT1
#define USBEPINTSET_EP1RX BIT2
#define USBEPINTSET_EP1TX BIT3
#define USBEPINTSET_EP2RX BIT4
#define USBEPINTSET_EP2TX BIT5
#define USBEPINTSET_EP3RX BIT6
#define USBEPINTSET_EP3TX BIT7
#define USBEPINTSET_EP4RX BIT8
#define USBEPINTSET_EP4TX BIT9
#define USBEPINTSET_EP5RX BIT10
#define USBEPINTSET_EP5TX BIT11
#define USBEPINTSET_EP6RX BIT12
#define USBEPINTSET_EP6TX BIT13
#define USBEPINTSET_EP7RX BIT14
#define USBEPINTSET_EP7TX BIT15
#define USBEPINTSET_EP8RX BIT16
#define USBEPINTSET_EP8TX BIT17
#define USBEPINTSET_EP9RX BIT18
#define USBEPINTSET_EP9TX BIT19
#define USBEPINTSET_EP10RX BIT20
#define USBEPINTSET_EP10TX BIT21
#define USBEPINTSET_EP11RX BIT22
#define USBEPINTSET_EP11TX BIT23
#define USBEPINTSET_EP12RX BIT24
#define USBEPINTSET_EP12TX BIT25
#define USBEPINTSET_EP13RX BIT26
#define USBEPINTSET_EP13TX BIT27
#define USBEPINTSET_EP14RX BIT28
#define USBEPINTSET_EP14TX BIT29
#define USBEPINTSET_EP15RX BIT30
#define USBEPINTSET_EP15TX BIT31

/*USB Endpoint Priority */
#define USBEPINTPRI (*(hcc_reg32 *)(USB_BASE+0x240))
#define USBEPINTPRI_EP0RX BIT0
#define USBEPINTPRI_EP0TX BIT1
#define USBEPINTPRI_EP1RX BIT2
#define USBEPINTPRI_EP1TX BIT3
#define USBEPINTPRI_EP2RX BIT4
#define USBEPINTPRI_EP2TX BIT5
#define USBEPINTPRI_EP3RX BIT6
#define USBEPINTPRI_EP3TX BIT7
#define USBEPINTPRI_EP4RX BIT8
#define USBEPINTPRI_EP4TX BIT9
#define USBEPINTPRI_EP5RX BIT10
#define USBEPINTPRI_EP5TX BIT11
#define USBEPINTPRI_EP6RX BIT12
#define USBEPINTPRI_EP6TX BIT13
#define USBEPINTPRI_EP7RX BIT14
#define USBEPINTPRI_EP7TX BIT15
#define USBEPINTPRI_EP8RX BIT16
#define USBEPINTPRI_EP8TX BIT17
#define USBEPINTPRI_EP9RX BIT18
#define USBEPINTPRI_EP9TX BIT19
#define USBEPINTPRI_EP10RX BIT20
#define USBEPINTPRI_EP10TX BIT21
#define USBEPINTPRI_EP11RX BIT22
#define USBEPINTPRI_EP11TX BIT23
#define USBEPINTPRI_EP12RX BIT24
#define USBEPINTPRI_EP12TX BIT25
#define USBEPINTPRI_EP13RX BIT26
#define USBEPINTPRI_EP13TX BIT27
#define USBEPINTPRI_EP14RX BIT28
#define USBEPINTPRI_EP14TX BIT29
#define USBEPINTPRI_EP15RX BIT30
#define USBEPINTPRI_EP15TX BIT31

/* Endpoint realization registers */
/*USB Realize Endpoint */
#define USBREEP (*(hcc_reg32 *)(USB_BASE+0x244))

/*USB Endpoint Index */
#define USBEPIND (*(hcc_reg32 *)(USB_BASE+0x248))
/*USB MaxPacketSize */
#define USBMAXPSIZE (*(hcc_reg32 *)(USB_BASE+0x24C))
/* USB transfer registers */
/*USB Receive Data */
#define USBRXDATA (*(hcc_reg32 *)(USB_BASE+0x218))
/*USB Receive Packet Length */
#define USBRXPLEN (*(hcc_reg32 *)(USB_BASE+0x220))
#define USBRXPLEN_DV        BIT10
#define USBRXPLEN_PKT_RDY   BIT11
#define USBRXPLEN_LEN_MASK  (0x1fful)

/*USB Transmit Data */
#define USBTXDATA (*(hcc_reg32 *)(USB_BASE+0x21C))
/*USB Transmit Packet Length */
#define USBTXPLEN (*(hcc_reg32 *)(USB_BASE+0x224))
/*USB Control */
#define USBCTRL (*(hcc_reg32 *)(USB_BASE+0x228))
#define USBCTRL_RD_EN       BIT0
#define USBCTRL_WR_EN       BIT1

/* SIE Command registers */
/*USB Command Code */
#define USBCMDCODE (*(hcc_reg32 *)(USB_BASE+0x210))
/*USB Command Data */
#define USBCMDDATA (*(hcc_reg32 *)(USB_BASE+0x214))
/* DMA registers */
/*USB DMA Request Status */
#define USBDMARST (*(hcc_reg32 *)(USB_BASE+0x250))
/*USB DMA Request Clear */
#define USBDMARCLR (*(hcc_reg32 *)(USB_BASE+0x254))
/*USB DMA Request Set */
#define USBDMARSET (*(hcc_reg32*)(USB_BASE+0x258))
/*USB UDCA Head */
#define USBUDCAHEAD (*(hcc_reg32 *)(USB_BASE+0x280))
/*USB Endpoint DMA Status */
#define USBEPDMAST (*(hcc_reg32 *)(USB_BASE+0x284))
/*USB Endpoint DMA Enable */
#define USBEPDMAEN (*(hcc_reg32 *)(USB_BASE+0x288))
/*USB Endpoint DMA Disable */
#define USBEPDMADIS (*(hcc_reg32 *)(USB_BASE+0x28C))
/*USB DMA Interrupt Status */
#define USBDMAINTST (*(hcc_reg32 *)(USB_BASE+0x290))
/*USB DMA Interrupt Enable */
#define USBDMAINTEN (*(hcc_reg32 *)(USB_BASE+0x294))
/*USB End of Transfer Interrupt Status */
#define USBEOTINTST (*(hcc_reg32 *)(USB_BASE+0x2A0))
/*USB End of Transfer Interrupt Clear */
#define USBEOTINTCLR (*(hcc_reg32 *)(USB_BASE+0x2A4))
/*USB End of Transfer Interrupt Set */
#define USBEOTINTSET (*(hcc_reg32 *)(USB_BASE+0x2A8))
/*USB New DD Request Interrupt Status */
#define USBNDDRINTST (*(hcc_reg32 *)(USB_BASE+0x2AC))
/*USB New DD Request Interrupt Clear */
#define USBNDDRINTCLR (*(hcc_reg32 *)(USB_BASE+0x2B0))
/*USB New DD Request Interrupt Set */
#define USBNDDRINTSET (*(hcc_reg32 *)(USB_BASE+0x2B4))
/*USB System Error Interrupt Status */
#define USBSYSERRINTST (*(hcc_reg32 *)(USB_BASE+0x2B8))
/*USB System Error Interrupt Clear */
#define USBSYSERRINTCLR (*(hcc_reg32 *)(USB_BASE+0x2BC))
/*USB System Error Interrupt Set */
#define USBSYSERRINTSET (*(hcc_reg32 *)(USB_BASE+0x2C0))


#define USBOTG_BASE_ADDR	0x31020100
#define OTG_INT_STAT        (*(hcc_reg32 *)(USBOTG_BASE_ADDR + 0x00))
#define OTG_INT_EN          (*(hcc_reg32 *)(USBOTG_BASE_ADDR + 0x04))
#define OTG_INT_SET         (*(hcc_reg32 *)(USBOTG_BASE_ADDR + 0x08))
#define OTG_INT_CLR         (*(hcc_reg32 *)(USBOTG_BASE_ADDR + 0x0C))

#define OTG_STAT_CTRL       (*(hcc_reg32 *)(USBOTG_BASE_ADDR + 0x10))
#define OTG_TIMER           (*(hcc_reg32 *)(USBOTG_BASE_ADDR + 0x14))

#define USBOTG_I2C_BASE_ADDR	0x31020300
#define OTG_I2C_RX          (*(hcc_reg32 *)(USBOTG_I2C_BASE_ADDR + 0x00))
#define OTG_I2C_TX          (*(hcc_reg32 *)(USBOTG_I2C_BASE_ADDR + 0x00))
#define OTG_I2C_STS         (*(hcc_reg32 *)(USBOTG_I2C_BASE_ADDR + 0x04))
#define OTG_I2C_CTL         (*(hcc_reg32 *)(USBOTG_I2C_BASE_ADDR + 0x08))
#define OTG_I2C_CLKHI       (*(hcc_reg32 *)(USBOTG_I2C_BASE_ADDR + 0x0C))
#define OTG_I2C_CLKLO       (*(hcc_reg32 *)(USBOTG_I2C_BASE_ADDR + 0x10))

#define USBOTG_CLK_BASE_ADDR	0x31020FF0
#define OTG_CLK_CTRL        (*(hcc_reg32 *)(USBOTG_CLK_BASE_ADDR + 0x04))
#define OTG_CLK_STAT        (*(hcc_reg32 *)(USBOTG_CLK_BASE_ADDR + 0x08))

#endif
/****************************** END OF FILE **********************************/
