#include "lpc3250_otg_i2c.h"
#include "lpc3250_regs.h"
#include "os.h"

int otg_i2c_init(void)
{
  OTG_CLK_CTRL |= 0x1e;
  while ( (OTG_CLK_STAT & 0x1e) != 0x1e )
    ;
  /* Reset I2C */
  OTG_I2C_CTL |= (1 << 8);
  
  OTG_I2C_CLKLO = 0x41;
  OTG_I2C_CLKHI = 0x41;

  OTG_I2C_CTL=0x100;
  return(OS_SUCCESS);
}

int otg_i2c_start(void)
{
  /* empty */
  return(OS_SUCCESS);
}

int otg_i2c_stop(void)
{
  OTG_CLK_CTRL &= ~0x0c;
  while ( (OTG_CLK_STAT & 0x1c) != 0x0c )
    ;
  return(OS_SUCCESS);
}

int otg_i2c_delete(void)
{
  /* empty */
  return(OS_SUCCESS);
}

hcc_u8 otg_i2c_read (hcc_u8 addr)
{
  hcc_u8 rc;

  OTG_I2C_TX=0x158|(ISP1301_A0<<1);
  while ((OTG_I2C_STS&TFE)==0)
    ;
  OTG_I2C_TX=addr;
  while ((OTG_I2C_STS&TFE)==0)
    ;
  OTG_I2C_TX=0x159|(ISP1301_A0<<1);
  while ((OTG_I2C_STS&TFE)==0)
    ;
  OTG_I2C_TX=0;
  while ((OTG_I2C_STS&TFE)==0)
    ;

  OTG_I2C_TX = 0x200;
  while ((OTG_I2C_STS&TFE)==0)
    ;

  while (OTG_I2C_STS&RFE)
    ;
  (void)OTG_I2C_RX;
  rc=(hcc_u8)OTG_I2C_RX;
  return rc;
}

void otg_i2c_write (hcc_u8 addr, hcc_u8 val)
{
  OTG_I2C_TX=0x158|(ISP1301_A0<<1);
  while ((OTG_I2C_STS&TFE)==0)
    ;
  OTG_I2C_TX=addr;
  while ((OTG_I2C_STS&TFE)==0)
    ;
  OTG_I2C_TX = 0x200 | val;
  while ((OTG_I2C_STS&TFE)==0)
    ;
}
