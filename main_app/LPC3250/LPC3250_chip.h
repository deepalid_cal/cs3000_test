/*
	OpenRTOS V6.1.0 Copyright (C) Wittenstein High Integrity Systems.

	OpenRTOS is distributed exclusively by Wittenstein High Integrity Systems,
	and is subject to the terms of the License granted to your organization,
	including its warranties and limitations on distribution.  It cannot be
	copied or reproduced in any way except as permitted by the License.

	Licenses are issued for each concurrent user working on a specified product
	line.

	WITTENSTEIN high integrity systems is a trading name of WITTENSTEIN
	aerospace & simulation ltd, Registered Office: Brown's Court,, Long Ashton
	Business Park, Yanley Lane, Long Ashton, Bristol, BS41 9LB, UK.
	Tel: +44 (0) 1275 395 600, fax: +44 (0) 1275 393 630.
	E-mail: info@HighIntegritySystems.com
	Registered in England No. 3711047; VAT No. GB 729 1583 15

	http://www.HighIntegritySystems.com
*/

#ifndef LPC3250_CHIP_H
#define LPC3250_CHIP_H

#define SLC_DATA (*(volatile unsigned short *)0x20020000)
#define SLC_DATA_Data_MASK 0xFF
#define SLC_DATA_Data_BIT 0

#define SLC_ADDR (*(volatile unsigned char *)0x20020004)

#define SLC_CMD (*(volatile unsigned char *)0x20020008)

#define SLC_STOP (*(volatile unsigned char *)0x2002000C)

#define SLC_CTRL (*(volatile unsigned *)0x20020010)
#define SLC_CTRL_SW_RESET_MASK 0x4
#define SLC_CTRL_SW_RESET 0x4
#define SLC_CTRL_SW_RESET_BIT 2
#define SLC_CTRL_ECC_CLEAR_MASK 0x2
#define SLC_CTRL_ECC_CLEAR 0x2
#define SLC_CTRL_ECC_CLEAR_BIT 1
#define SLC_CTRL_DMA_START_MASK 0x1
#define SLC_CTRL_DMA_START 0x1
#define SLC_CTRL_DMA_START_BIT 0

#define SLC_CFG (*(volatile unsigned *)0x20020014)
#define SLC_CFG_CE_LOW_MASK 0x20
#define SLC_CFG_CE_LOW 0x20
#define SLC_CFG_CE_LOW_BIT 5
#define SLC_CFG_DMA_ECC_MASK 0x10
#define SLC_CFG_DMA_ECC 0x10
#define SLC_CFG_DMA_ECC_BIT 4
#define SLC_CFG_ECC_EN_MASK 0x8
#define SLC_CFG_ECC_EN 0x8
#define SLC_CFG_ECC_EN_BIT 3
#define SLC_CFG_DMA_BURST_MASK 0x4
#define SLC_CFG_DMA_BURST 0x4
#define SLC_CFG_DMA_BURST_BIT 2
#define SLC_CFG_DMA_DIR_MASK 0x2
#define SLC_CFG_DMA_DIR 0x2
#define SLC_CFG_DMA_DIR_BIT 1
#define SLC_CFG_WIDTH_MASK 0x1
#define SLC_CFG_WIDTH 0x1
#define SLC_CFG_WIDTH_BIT 0

#define SLC_STAT (*(volatile unsigned *)0x20020018)
#define SLC_STAT_DMA_ACTIVE_MASK 0x4
#define SLC_STAT_DMA_ACTIVE 0x4
#define SLC_STAT_DMA_ACTIVE_BIT 2
#define SLC_STAT_SLC_ACTIVE_MASK 0x2
#define SLC_STAT_SLC_ACTIVE 0x2
#define SLC_STAT_SLC_ACTIVE_BIT 1
#define SLC_STAT_READY_MASK 0x1
#define SLC_STAT_READY 0x1
#define SLC_STAT_READY_BIT 0

#define SLC_INT_STAT (*(volatile unsigned *)0x2002001C)
#define SLC_INT_STAT_INT_TC_STAT_MASK 0x2
#define SLC_INT_STAT_INT_TC_STAT 0x2
#define SLC_INT_STAT_INT_TC_STAT_BIT 1
#define SLC_INT_STAT_INT_RDY_STAT_MASK 0x1
#define SLC_INT_STAT_INT_RDY_STAT 0x1
#define SLC_INT_STAT_INT_RDY_STAT_BIT 0

#define SLC_IEN (*(volatile unsigned *)0x20020020)
#define SLC_IEN_INT_TC_EN_MASK 0x2
#define SLC_IEN_INT_TC_EN 0x2
#define SLC_IEN_INT_TC_EN_BIT 1
#define SLC_IEN_INT_RDY_EN_MASK 0x1
#define SLC_IEN_INT_RDY_EN 0x1
#define SLC_IEN_INT_RDY_EN_BIT 0

#define SLC_ISR (*(volatile unsigned *)0x20020024)
#define SLC_ISR_INT_TC_CLR_MASK 0x2
#define SLC_ISR_INT_TC_CLR 0x2
#define SLC_ISR_INT_TC_CLR_BIT 1
#define SLC_ISR_INT_RDY_CLR_MASK 0x1
#define SLC_ISR_INT_RDY_CLR 0x1
#define SLC_ISR_INT_RDY_CLR_BIT 0

#define SLC_ICR (*(volatile unsigned *)0x20020028)
#define SLC_ICR_W_RDY_MASK 0xF0000000
#define SLC_ICR_W_RDY_BIT 28
#define SLC_ICR_W_WIDTH_MASK 0xF000000
#define SLC_ICR_W_WIDTH_BIT 24
#define SLC_ICR_W_HOLD_MASK 0xF00000
#define SLC_ICR_W_HOLD_BIT 20
#define SLC_ICR_W_SETUP_MASK 0xF0000
#define SLC_ICR_W_SETUP_BIT 16
#define SLC_ICR_R_RDY_MASK 0xF000
#define SLC_ICR_R_RDY_BIT 12
#define SLC_ICR_R_WIDTH_MASK 0xF00
#define SLC_ICR_R_WIDTH_BIT 8
#define SLC_ICR_R_HOLD_MASK 0xF0
#define SLC_ICR_R_HOLD_BIT 4
#define SLC_ICR_R_SETUP_MASK 0xF
#define SLC_ICR_R_SETUP_BIT 0

#define SLC_TAC (*(volatile unsigned *)0x2002002C)
#define SLC_TAC_T_C_MASK 0xFFFF
#define SLC_TAC_T_C_BIT 0

#define SLC_TC (*(volatile unsigned *)0x20020030)
#define SLC_TC_T_C_MASK 0xFFFF
#define SLC_TC_T_C_BIT 0

#define SLC_ECC (*(volatile unsigned *)0x20020034)
#define SLC_ECC_LP_MASK 0x3FFFC0
#define SLC_ECC_LP_BIT 6
#define SLC_ECC_CP_MASK 0x3F
#define SLC_ECC_CP_BIT 0

#define SLC_DMA_DATA (*(volatile unsigned *)0x20020038)
#define SLC_DMA_DATA_First_data_byte_transfered_MASK 0xFF000000
#define SLC_DMA_DATA_First_data_byte_transfered_BIT 24
#define SLC_DMA_DATA_Second_data_byte_transfered_MASK 0xFF0000
#define SLC_DMA_DATA_Second_data_byte_transfered_BIT 16
#define SLC_DMA_DATA_Third_data_byte_transfered_MASK 0xFF00
#define SLC_DMA_DATA_Third_data_byte_transfered_BIT 8
#define SLC_DMA_DATA_Fourth_data_byte_transfered_MASK 0xFF
#define SLC_DMA_DATA_Fourth_data_byte_transfered_BIT 0

#define SSP0CR0 (*(volatile unsigned long *)0x20084000)
#define SSP0CR0_SCR_MASK 0xFF00
#define SSP0CR0_SCR_BIT 8
#define SSP0CR0_CPHA_MASK 0x80
#define SSP0CR0_CPHA 0x80
#define SSP0CR0_CPHA_BIT 7
#define SSP0CR0_CPOL_MASK 0x40
#define SSP0CR0_CPOL 0x40
#define SSP0CR0_CPOL_BIT 6
#define SSP0CR0_FRF_MASK 0x30
#define SSP0CR0_FRF_BIT 4
#define SSP0CR0_DSS_MASK 0xF
#define SSP0CR0_DSS_BIT 0

#define SSP0CR1 (*(volatile unsigned long *)0x20084004)
#define SSP0CR1_SOD_MASK 0x8
#define SSP0CR1_SOD 0x8
#define SSP0CR1_SOD_BIT 3
#define SSP0CR1_MS_MASK 0x4
#define SSP0CR1_MS 0x4
#define SSP0CR1_MS_BIT 2
#define SSP0CR1_SSE_MASK 0x2
#define SSP0CR1_SSE 0x2
#define SSP0CR1_SSE_BIT 1
#define SSP0CR1_LBM_MASK 0x1
#define SSP0CR1_LBM 0x1
#define SSP0CR1_LBM_BIT 0

#define SSP0DR (*(volatile unsigned long *)0x20084008)

#define SSP0SR (*(volatile unsigned long *)0x2008400C)
#define SSP0SR_BSY_MASK 0x10
#define SSP0SR_BSY 0x10
#define SSP0SR_BSY_BIT 4
#define SSP0SR_RFF_MASK 0x8
#define SSP0SR_RFF 0x8
#define SSP0SR_RFF_BIT 3
#define SSP0SR_RNE_MASK 0x4
#define SSP0SR_RNE 0x4
#define SSP0SR_RNE_BIT 2
#define SSP0SR_TNF_MASK 0x2
#define SSP0SR_TNF 0x2
#define SSP0SR_TNF_BIT 1
#define SSP0SR_TFE_MASK 0x1
#define SSP0SR_TFE 0x1
#define SSP0SR_TFE_BIT 0

#define SSP0CPSR (*(volatile unsigned long *)0x20084010)
#define SSP0CPSR_CPSDVSR_MASK 0xFF
#define SSP0CPSR_CPSDVSR_BIT 0

#define SSP0IMSC (*(volatile unsigned long *)0x20084014)
#define SSP0IMSC_TXIM_MASK 0x8
#define SSP0IMSC_TXIM 0x8
#define SSP0IMSC_TXIM_BIT 3
#define SSP0IMSC_RXIM_MASK 0x4
#define SSP0IMSC_RXIM 0x4
#define SSP0IMSC_RXIM_BIT 2
#define SSP0IMSC_RTIM_MASK 0x2
#define SSP0IMSC_RTIM 0x2
#define SSP0IMSC_RTIM_BIT 1
#define SSP0IMSC_RORIM_MASK 0x1
#define SSP0IMSC_RORIM 0x1
#define SSP0IMSC_RORIM_BIT 0

#define SSP0RIS (*(volatile unsigned long *)0x20084018)
#define SSP0RIS_TXRIS_MASK 0x8
#define SSP0RIS_TXRIS 0x8
#define SSP0RIS_TXRIS_BIT 3
#define SSP0RIS_RXRIS_MASK 0x4
#define SSP0RIS_RXRIS 0x4
#define SSP0RIS_RXRIS_BIT 2
#define SSP0RIS_RTRIS_MASK 0x2
#define SSP0RIS_RTRIS 0x2
#define SSP0RIS_RTRIS_BIT 1
#define SSP0RIS_RORRIS_MASK 0x1
#define SSP0RIS_RORRIS 0x1
#define SSP0RIS_RORRIS_BIT 0

#define SSP0MIS (*(volatile unsigned long *)0x2008401C)
#define SSP0MIS_TXMIS_MASK 0x8
#define SSP0MIS_TXMIS 0x8
#define SSP0MIS_TXMIS_BIT 3
#define SSP0MIS_RXMIS_MASK 0x4
#define SSP0MIS_RXMIS 0x4
#define SSP0MIS_RXMIS_BIT 2
#define SSP0MIS_RTMIS_MASK 0x2
#define SSP0MIS_RTMIS 0x2
#define SSP0MIS_RTMIS_BIT 1
#define SSP0MIS_RORMIS_MASK 0x1
#define SSP0MIS_RORMIS 0x1
#define SSP0MIS_RORMIS_BIT 0

#define SSP0ICR (*(volatile unsigned long *)0x20084020)
#define SSP0ICR_RTIC_MASK 0x2
#define SSP0ICR_RTIC 0x2
#define SSP0ICR_RTIC_BIT 1
#define SSP0ICR_RORIC_MASK 0x1
#define SSP0ICR_RORIC 0x1
#define SSP0ICR_RORIC_BIT 0

#define SSP0DMACR (*(volatile unsigned long *)0x20084024)
#define SSP0DMACR_RXDMAE_MASK 0x1
#define SSP0DMACR_RXDMAE 0x1
#define SSP0DMACR_RXDMAE_BIT 0
#define SSP0DMACR_TXDMAE_MASK 0x2
#define SSP0DMACR_TXDMAE 0x2
#define SSP0DMACR_TXDMAE_BIT 1

#define SPI1_GLOBAL (*(volatile unsigned *)0x20088000)
#define SPI1_GLOBAL_rst_MASK 0x2
#define SPI1_GLOBAL_rst 0x2
#define SPI1_GLOBAL_rst_BIT 1
#define SPI1_GLOBAL_enable_MASK 0x1
#define SPI1_GLOBAL_enable 0x1
#define SPI1_GLOBAL_enable_BIT 0

#define SPI1_CON (*(volatile unsigned *)0x20088004)
#define SPI1_CON_unidir_MASK 0x800000
#define SPI1_CON_unidir 0x800000
#define SPI1_CON_unidir_BIT 23
#define SPI1_CON_bhalt_MASK 0x400000
#define SPI1_CON_bhalt 0x400000
#define SPI1_CON_bhalt_BIT 22
#define SPI1_CON_bpol_MASK 0x200000
#define SPI1_CON_bpol 0x200000
#define SPI1_CON_bpol_BIT 21
#define SPI1_CON_msb_MASK 0x80000
#define SPI1_CON_msb 0x80000
#define SPI1_CON_msb_BIT 19
#define SPI1_CON_mode_MASK 0x30000
#define SPI1_CON_mode_BIT 16
#define SPI1_CON_rxtx_MASK 0x8000
#define SPI1_CON_rxtx 0x8000
#define SPI1_CON_rxtx_BIT 15
#define SPI1_CON_thr_MASK 0x4000
#define SPI1_CON_thr 0x4000
#define SPI1_CON_thr_BIT 14
#define SPI1_CON_shift_off_MASK 0x2000
#define SPI1_CON_shift_off 0x2000
#define SPI1_CON_shift_off_BIT 13
#define SPI1_CON_bitnum_MASK 0x1E00
#define SPI1_CON_bitnum_BIT 9
#define SPI1_CON_ms_MASK 0x80
#define SPI1_CON_ms 0x80
#define SPI1_CON_ms_BIT 7
#define SPI1_CON_rate_MASK 0x7F
#define SPI1_CON_rate_BIT 0

#define SPI1_FRM (*(volatile unsigned *)0x20088008)
#define SPI1_FRM_spif_MASK 0xFFFF
#define SPI1_FRM_spif_BIT 0

#define SPI1_IER (*(volatile unsigned *)0x2008800C)
#define SPI1_IER_inteot_MASK 0x2
#define SPI1_IER_inteot 0x2
#define SPI1_IER_inteot_BIT 1
#define SPI1_IER_intthr_MASK 0x1
#define SPI1_IER_intthr 0x1
#define SPI1_IER_intthr_BIT 0

#define SPI1_STAT (*(volatile unsigned *)0x20088010)
#define SPI1_STAT_intclr_MASK 0x100
#define SPI1_STAT_intclr 0x100
#define SPI1_STAT_intclr_BIT 8
#define SPI1_STAT_eot_MASK 0x80
#define SPI1_STAT_eot 0x80
#define SPI1_STAT_eot_BIT 7
#define SPI1_STAT_busylev_MASK 0x40
#define SPI1_STAT_busylev 0x40
#define SPI1_STAT_busylev_BIT 6
#define SPI1_STAT_shiftact_MASK 0x8
#define SPI1_STAT_shiftact 0x8
#define SPI1_STAT_shiftact_BIT 3
#define SPI1_STAT_bf_MASK 0x4
#define SPI1_STAT_bf 0x4
#define SPI1_STAT_bf_BIT 2
#define SPI1_STAT_thr_MASK 0x2
#define SPI1_STAT_thr 0x2
#define SPI1_STAT_thr_BIT 1
#define SPI1_STAT_be_MASK 0x1
#define SPI1_STAT_be 0x1
#define SPI1_STAT_be_BIT 0

#define SPI1_DAT (*(volatile unsigned *)0x20088014)
#define SPI1_DAT_spid_MASK 0xFFFF
#define SPI1_DAT_spid_BIT 0

#define SPI1_TIM_CTRL (*(volatile unsigned *)0x20088400)
#define SPI1_TIM_CTRL_tireq_MASK 0x4
#define SPI1_TIM_CTRL_tireq 0x4
#define SPI1_TIM_CTRL_tireq_BIT 2
#define SPI1_TIM_CTRL_pirqe_MASK 0x2
#define SPI1_TIM_CTRL_pirqe 0x2
#define SPI1_TIM_CTRL_pirqe_BIT 1
#define SPI1_TIM_CTRL_mode_MASK 0x1
#define SPI1_TIM_CTRL_mode 0x1
#define SPI1_TIM_CTRL_mode_BIT 0

#define SPI1_TIM_COUNT (*(volatile unsigned *)0x20088404)
#define SPI1_TIM_COUNT_count_MASK 0xFFFF
#define SPI1_TIM_COUNT_count_BIT 0

#define SPI1_TIM_STAT (*(volatile unsigned *)0x20088408)
#define SPI1_TIM_STAT_tirqstat_MASK 0x8000
#define SPI1_TIM_STAT_tirqstat 0x8000
#define SPI1_TIM_STAT_tirqstat_BIT 15

#define SSP1CR0 (*(volatile unsigned long *)0x2008C000)
#define SSP1CR0_SCR_MASK 0xFF00
#define SSP1CR0_SCR_BIT 8
#define SSP1CR0_CPHA_MASK 0x80
#define SSP1CR0_CPHA 0x80
#define SSP1CR0_CPHA_BIT 7
#define SSP1CR0_CPOL_MASK 0x40
#define SSP1CR0_CPOL 0x40
#define SSP1CR0_CPOL_BIT 6
#define SSP1CR0_FRF_MASK 0x30
#define SSP1CR0_FRF_BIT 4
#define SSP1CR0_DSS_MASK 0xF
#define SSP1CR0_DSS_BIT 0

#define SSP1CR1 (*(volatile unsigned long *)0x2008C004)
#define SSP1CR1_SOD_MASK 0x8
#define SSP1CR1_SOD 0x8
#define SSP1CR1_SOD_BIT 3
#define SSP1CR1_MS_MASK 0x4
#define SSP1CR1_MS 0x4
#define SSP1CR1_MS_BIT 2
#define SSP1CR1_SSE_MASK 0x2
#define SSP1CR1_SSE 0x2
#define SSP1CR1_SSE_BIT 1
#define SSP1CR1_LBM_MASK 0x1
#define SSP1CR1_LBM 0x1
#define SSP1CR1_LBM_BIT 0

#define SSP1DR (*(volatile unsigned long *)0x2008C008)

#define SSP1SR (*(volatile unsigned long *)0x2008C00C)
#define SSP1SR_BSY_MASK 0x10
#define SSP1SR_BSY 0x10
#define SSP1SR_BSY_BIT 4
#define SSP1SR_RFF_MASK 0x8
#define SSP1SR_RFF 0x8
#define SSP1SR_RFF_BIT 3
#define SSP1SR_RNE_MASK 0x4
#define SSP1SR_RNE 0x4
#define SSP1SR_RNE_BIT 2
#define SSP1SR_TNF_MASK 0x2
#define SSP1SR_TNF 0x2
#define SSP1SR_TNF_BIT 1
#define SSP1SR_TFE_MASK 0x1
#define SSP1SR_TFE 0x1
#define SSP1SR_TFE_BIT 0

#define SSP1CPSR (*(volatile unsigned long *)0x2008C010)
#define SSP1CPSR_CPSDVSR_MASK 0xFF
#define SSP1CPSR_CPSDVSR_BIT 0

#define SSP1IMSC (*(volatile unsigned long *)0x2008C014)
#define SSP1IMSC_TXIM_MASK 0x8
#define SSP1IMSC_TXIM 0x8
#define SSP1IMSC_TXIM_BIT 3
#define SSP1IMSC_RXIM_MASK 0x4
#define SSP1IMSC_RXIM 0x4
#define SSP1IMSC_RXIM_BIT 2
#define SSP1IMSC_RTIM_MASK 0x2
#define SSP1IMSC_RTIM 0x2
#define SSP1IMSC_RTIM_BIT 1
#define SSP1IMSC_RORIM_MASK 0x1
#define SSP1IMSC_RORIM 0x1
#define SSP1IMSC_RORIM_BIT 0

#define SSP1RIS (*(volatile unsigned long *)0x2008C018)
#define SSP1RIS_TXRIS_MASK 0x8
#define SSP1RIS_TXRIS 0x8
#define SSP1RIS_TXRIS_BIT 3
#define SSP1RIS_RXRIS_MASK 0x4
#define SSP1RIS_RXRIS 0x4
#define SSP1RIS_RXRIS_BIT 2
#define SSP1RIS_RTRIS_MASK 0x2
#define SSP1RIS_RTRIS 0x2
#define SSP1RIS_RTRIS_BIT 1
#define SSP1RIS_RORRIS_MASK 0x1
#define SSP1RIS_RORRIS 0x1
#define SSP1RIS_RORRIS_BIT 0

#define SSP1MIS (*(volatile unsigned long *)0x2008C01C)
#define SSP1MIS_TXMIS_MASK 0x8
#define SSP1MIS_TXMIS 0x8
#define SSP1MIS_TXMIS_BIT 3
#define SSP1MIS_RXMIS_MASK 0x4
#define SSP1MIS_RXMIS 0x4
#define SSP1MIS_RXMIS_BIT 2
#define SSP1MIS_RTMIS_MASK 0x2
#define SSP1MIS_RTMIS 0x2
#define SSP1MIS_RTMIS_BIT 1
#define SSP1MIS_RORMIS_MASK 0x1
#define SSP1MIS_RORMIS 0x1
#define SSP1MIS_RORMIS_BIT 0

#define SSP1ICR (*(volatile unsigned long *)0x2008C020)
#define SSP1ICR_RTIC_MASK 0x2
#define SSP1ICR_RTIC 0x2
#define SSP1ICR_RTIC_BIT 1
#define SSP1ICR_RORIC_MASK 0x1
#define SSP1ICR_RORIC 0x1
#define SSP1ICR_RORIC_BIT 0

#define SSP1DMACR (*(volatile unsigned long *)0x2008C024)
#define SSP1DMACR_RXDMAE_MASK 0x1
#define SSP1DMACR_RXDMAE 0x1
#define SSP1DMACR_RXDMAE_BIT 0
#define SSP1DMACR_TXDMAE_MASK 0x2
#define SSP1DMACR_TXDMAE 0x2
#define SSP1DMACR_TXDMAE_BIT 1

#define SPI2_GLOBAL (*(volatile unsigned *)0x20090000)
#define SPI2_GLOBAL_rst_MASK 0x2
#define SPI2_GLOBAL_rst 0x2
#define SPI2_GLOBAL_rst_BIT 1
#define SPI2_GLOBAL_enable_MASK 0x1
#define SPI2_GLOBAL_enable 0x1
#define SPI2_GLOBAL_enable_BIT 0

#define SPI2_CON (*(volatile unsigned *)0x20090004)
#define SPI2_CON_unidir_MASK 0x800000
#define SPI2_CON_unidir 0x800000
#define SPI2_CON_unidir_BIT 23
#define SPI2_CON_bhalt_MASK 0x400000
#define SPI2_CON_bhalt 0x400000
#define SPI2_CON_bhalt_BIT 22
#define SPI2_CON_bpol_MASK 0x200000
#define SPI2_CON_bpol 0x200000
#define SPI2_CON_bpol_BIT 21
#define SPI2_CON_msb_MASK 0x80000
#define SPI2_CON_msb 0x80000
#define SPI2_CON_msb_BIT 19
#define SPI2_CON_mode_MASK 0x30000
#define SPI2_CON_mode_BIT 16
#define SPI2_CON_rxtx_MASK 0x8000
#define SPI2_CON_rxtx 0x8000
#define SPI2_CON_rxtx_BIT 15
#define SPI2_CON_thr_MASK 0x4000
#define SPI2_CON_thr 0x4000
#define SPI2_CON_thr_BIT 14
#define SPI2_CON_shift_off_MASK 0x2000
#define SPI2_CON_shift_off 0x2000
#define SPI2_CON_shift_off_BIT 13
#define SPI2_CON_bitnum_MASK 0x1E00
#define SPI2_CON_bitnum_BIT 9
#define SPI2_CON_ms_MASK 0x80
#define SPI2_CON_ms 0x80
#define SPI2_CON_ms_BIT 7
#define SPI2_CON_rate_MASK 0x7F
#define SPI2_CON_rate_BIT 0

#define SPI2_FRM (*(volatile unsigned *)0x20090008)
#define SPI2_FRM_spif_MASK 0xFFFF
#define SPI2_FRM_spif_BIT 0

#define SPI2_IER (*(volatile unsigned *)0x2009000C)
#define SPI2_IER_inteot_MASK 0x2
#define SPI2_IER_inteot 0x2
#define SPI2_IER_inteot_BIT 1
#define SPI2_IER_intthr_MASK 0x1
#define SPI2_IER_intthr 0x1
#define SPI2_IER_intthr_BIT 0

#define SPI2_STAT (*(volatile unsigned *)0x20090010)
#define SPI2_STAT_intclr_MASK 0x100
#define SPI2_STAT_intclr 0x100
#define SPI2_STAT_intclr_BIT 8
#define SPI2_STAT_eot_MASK 0x80
#define SPI2_STAT_eot 0x80
#define SPI2_STAT_eot_BIT 7
#define SPI2_STAT_busylev_MASK 0x40
#define SPI2_STAT_busylev 0x40
#define SPI2_STAT_busylev_BIT 6
#define SPI2_STAT_shiftact_MASK 0x8
#define SPI2_STAT_shiftact 0x8
#define SPI2_STAT_shiftact_BIT 3
#define SPI2_STAT_bf_MASK 0x4
#define SPI2_STAT_bf 0x4
#define SPI2_STAT_bf_BIT 2
#define SPI2_STAT_thr_MASK 0x2
#define SPI2_STAT_thr 0x2
#define SPI2_STAT_thr_BIT 1
#define SPI2_STAT_be_MASK 0x1
#define SPI2_STAT_be 0x1
#define SPI2_STAT_be_BIT 0

#define SPI2_DAT (*(volatile unsigned *)0x20090014)
#define SPI2_DAT_spid_MASK 0xFFFF
#define SPI2_DAT_spid_BIT 0

#define SPI2_TIM_CTRL (*(volatile unsigned *)0x20090400)
#define SPI2_TIM_CTRL_tireq_MASK 0x4
#define SPI2_TIM_CTRL_tireq 0x4
#define SPI2_TIM_CTRL_tireq_BIT 2
#define SPI2_TIM_CTRL_pirqe_MASK 0x2
#define SPI2_TIM_CTRL_pirqe 0x2
#define SPI2_TIM_CTRL_pirqe_BIT 1
#define SPI2_TIM_CTRL_mode_MASK 0x1
#define SPI2_TIM_CTRL_mode 0x1
#define SPI2_TIM_CTRL_mode_BIT 0

#define SPI2_TIM_COUNT (*(volatile unsigned *)0x20090404)
#define SPI2_TIM_COUNT_count_MASK 0xFFFF
#define SPI2_TIM_COUNT_count_BIT 0

#define SPI2_TIM_STAT (*(volatile unsigned *)0x20090408)
#define SPI2_TIM_STAT_tirqstat_MASK 0x8000
#define SPI2_TIM_STAT_tirqstat 0x8000
#define SPI2_TIM_STAT_tirqstat_BIT 15

#define I2S0DAO (*(volatile unsigned long *)0x20094000)
#define I2S0DAO_wordwidth_MASK 0x3
#define I2S0DAO_wordwidth_BIT 0
#define I2S0DAO_mono_MASK 0x4
#define I2S0DAO_mono 0x4
#define I2S0DAO_mono_BIT 2
#define I2S0DAO_stop_MASK 0x8
#define I2S0DAO_stop 0x8
#define I2S0DAO_stop_BIT 3
#define I2S0DAO_reset_MASK 0x10
#define I2S0DAO_reset 0x10
#define I2S0DAO_reset_BIT 4
#define I2S0DAO_ws_sel_MASK 0x20
#define I2S0DAO_ws_sel 0x20
#define I2S0DAO_ws_sel_BIT 5
#define I2S0DAO_ws_halfperiod_MASK 0x3FC0
#define I2S0DAO_ws_halfperiod_BIT 6
#define I2S0DAO_mute_MASK 0x8000
#define I2S0DAO_mute 0x8000
#define I2S0DAO_mute_BIT 15

#define I2S0DAI (*(volatile unsigned long *)0x20094004)
#define I2S0DAI_wordwidth_MASK 0x3
#define I2S0DAI_wordwidth_BIT 0
#define I2S0DAI_mono_MASK 0x4
#define I2S0DAI_mono 0x4
#define I2S0DAI_mono_BIT 2
#define I2S0DAI_stop_MASK 0x8
#define I2S0DAI_stop 0x8
#define I2S0DAI_stop_BIT 3
#define I2S0DAI_reset_MASK 0x10
#define I2S0DAI_reset 0x10
#define I2S0DAI_reset_BIT 4
#define I2S0DAI_ws_sel_MASK 0x20
#define I2S0DAI_ws_sel 0x20
#define I2S0DAI_ws_sel_BIT 5
#define I2S0DAI_ws_halfperiod_MASK 0x3FC0
#define I2S0DAI_ws_halfperiod_BIT 6

#define I2S0TXFIFO (*(volatile unsigned long *)0x20094008)

#define I2S0RXFIFO (*(volatile unsigned long *)0x2009400C)

#define I2S0STATE (*(volatile unsigned long *)0x20094010)
#define I2S0STATE_irq_MASK 0x1
#define I2S0STATE_irq 0x1
#define I2S0STATE_irq_BIT 0
#define I2S0STATE_dma0_MASK 0x2
#define I2S0STATE_dma0 0x2
#define I2S0STATE_dma0_BIT 1
#define I2S0STATE_dma1_MASK 0x4
#define I2S0STATE_dma1 0x4
#define I2S0STATE_dma1_BIT 2
#define I2S0STATE_rx_level_MASK 0xF00
#define I2S0STATE_rx_level_BIT 8
#define I2S0STATE_tx_level_MASK 0xF0000
#define I2S0STATE_tx_level_BIT 16

#define I2S0DMA0 (*(volatile unsigned long *)0x20094014)
#define I2S0DMA0_rx_dma0_enable_MASK 0x1
#define I2S0DMA0_rx_dma0_enable 0x1
#define I2S0DMA0_rx_dma0_enable_BIT 0
#define I2S0DMA0_tx_dma0_enable_MASK 0x2
#define I2S0DMA0_tx_dma0_enable 0x2
#define I2S0DMA0_tx_dma0_enable_BIT 1
#define I2S0DMA0_rx_depth_dma0_MASK 0x700
#define I2S0DMA0_rx_depth_dma0_BIT 8
#define I2S0DMA0_tx_depth_dma0_MASK 0x70000
#define I2S0DMA0_tx_depth_dma0_BIT 16

#define I2S0DMA1 (*(volatile unsigned long *)0x20094018)
#define I2S0DMA1_rx_dma1_enable_MASK 0x1
#define I2S0DMA1_rx_dma1_enable 0x1
#define I2S0DMA1_rx_dma1_enable_BIT 0
#define I2S0DMA1_tx_dma1_enable_MASK 0x2
#define I2S0DMA1_tx_dma1_enable 0x2
#define I2S0DMA1_tx_dma1_enable_BIT 1
#define I2S0DMA1_rx_depth_dma1_MASK 0x700
#define I2S0DMA1_rx_depth_dma1_BIT 8
#define I2S0DMA1_tx_depth_dma1_MASK 0x70000
#define I2S0DMA1_tx_depth_dma1_BIT 16

#define I2S0IRQ (*(volatile unsigned long *)0x2009401C)
#define I2S0IRQ_rx_irq_enable_MASK 0x1
#define I2S0IRQ_rx_irq_enable 0x1
#define I2S0IRQ_rx_irq_enable_BIT 0
#define I2S0IRQ_tx_irq_enable_MASK 0x2
#define I2S0IRQ_tx_irq_enable 0x2
#define I2S0IRQ_tx_irq_enable_BIT 1
#define I2S0IRQ_rx_depth_irq_MASK 0x700
#define I2S0IRQ_rx_depth_irq_BIT 8
#define I2S0IRQ_tx_depth_irq_MASK 0x70000
#define I2S0IRQ_tx_depth_irq_BIT 16

#define I2S0TXRATE (*(volatile unsigned long *)0x20094020)
#define I2S0TXRATE_Y_divider_MASK 0xFF
#define I2S0TXRATE_Y_divider_BIT 0
#define I2S0TXRATE_X_divider_MASK 0xFF00
#define I2S0TXRATE_X_divider_BIT 8

#define I2S0RXRATE (*(volatile unsigned long *)0x20094024)
#define I2S0RXRATE_Y_divider_MASK 0xFF
#define I2S0RXRATE_Y_divider_BIT 0
#define I2S0RXRATE_X_divider_MASK 0xFF00
#define I2S0RXRATE_X_divider_BIT 8

#define SD_Power (*(volatile unsigned *)0x20098000)
#define SD_Power_OpenDrain_MASK 0x40
#define SD_Power_OpenDrain 0x40
#define SD_Power_OpenDrain_BIT 6
#define SD_Power_Ctrl_MASK 0x3
#define SD_Power_Ctrl_BIT 0

#define SD_Clock (*(volatile unsigned *)0x20098004)
#define SD_Clock_WideBus_MASK 0x800
#define SD_Clock_WideBus 0x800
#define SD_Clock_WideBus_BIT 11
#define SD_Clock_Bypass_MASK 0x400
#define SD_Clock_Bypass 0x400
#define SD_Clock_Bypass_BIT 10
#define SD_Clock_PwrSave_MASK 0x200
#define SD_Clock_PwrSave 0x200
#define SD_Clock_PwrSave_BIT 9
#define SD_Clock_Enable_MASK 0x100
#define SD_Clock_Enable 0x100
#define SD_Clock_Enable_BIT 8
#define SD_Clock_ClkDiv_MASK 0xFF
#define SD_Clock_ClkDiv_BIT 0

#define SD_Argument (*(volatile unsigned *)0x20098008)

#define SD_Command (*(volatile unsigned *)0x2009800C)
#define SD_Command_Enable_MASK 0x400
#define SD_Command_Enable 0x400
#define SD_Command_Enable_BIT 10
#define SD_Command_Pending_MASK 0x200
#define SD_Command_Pending 0x200
#define SD_Command_Pending_BIT 9
#define SD_Command_Interrupt_MASK 0x100
#define SD_Command_Interrupt 0x100
#define SD_Command_Interrupt_BIT 8
#define SD_Command_LongRsp_MASK 0x80
#define SD_Command_LongRsp 0x80
#define SD_Command_LongRsp_BIT 7
#define SD_Command_Response_MASK 0x40
#define SD_Command_Response 0x40
#define SD_Command_Response_BIT 6
#define SD_Command_CmdIndex_MASK 0x3F
#define SD_Command_CmdIndex_BIT 0

#define SD_Respcmd (*(volatile unsigned *)0x20098010)
#define SD_Respcmd_RespCmd_MASK 0x3F
#define SD_Respcmd_RespCmd_BIT 0

#define SD_Response0 (*(volatile unsigned *)0x20098014)

#define SD_Response1 (*(volatile unsigned *)0x20098018)

#define SD_Response2 (*(volatile unsigned *)0x2009801C)

#define SD_Response3 (*(volatile unsigned *)0x20098020)

#define SD_DataTimer (*(volatile unsigned *)0x20098024)

#define SD_DataLength (*(volatile unsigned *)0x20098028)
#define SD_DataLength_DataLength_MASK 0xFFFF
#define SD_DataLength_DataLength_BIT 0

#define SD_DataCtrl (*(volatile unsigned *)0x2009802C)
#define SD_DataCtrl_BlockSize_MASK 0xF0
#define SD_DataCtrl_BlockSize_BIT 4
#define SD_DataCtrl_DMAEnable_MASK 0x8
#define SD_DataCtrl_DMAEnable 0x8
#define SD_DataCtrl_DMAEnable_BIT 3
#define SD_DataCtrl_Mode_MASK 0x4
#define SD_DataCtrl_Mode 0x4
#define SD_DataCtrl_Mode_BIT 2
#define SD_DataCtrl_Direction_MASK 0x2
#define SD_DataCtrl_Direction 0x2
#define SD_DataCtrl_Direction_BIT 1
#define SD_DataCtrl_Enable_MASK 0x1
#define SD_DataCtrl_Enable 0x1
#define SD_DataCtrl_Enable_BIT 0

#define SD_DataCnt (*(volatile unsigned *)0x20098030)
#define SD_DataCnt_DataCount_MASK 0xFFFF
#define SD_DataCnt_DataCount_BIT 0

#define SD_Status (*(volatile unsigned *)0x20098034)
#define SD_Status_RxDataAvlbl_MASK 0x200000
#define SD_Status_RxDataAvlbl 0x200000
#define SD_Status_RxDataAvlbl_BIT 21
#define SD_Status_TxDataAvlbl_MASK 0x100000
#define SD_Status_TxDataAvlbl 0x100000
#define SD_Status_TxDataAvlbl_BIT 20
#define SD_Status_RxFifoAvlbl_MASK 0x80000
#define SD_Status_RxFifoAvlbl 0x80000
#define SD_Status_RxFifoAvlbl_BIT 19
#define SD_Status_TxFifoAvlbl_MASK 0x40000
#define SD_Status_TxFifoAvlbl 0x40000
#define SD_Status_TxFifoAvlbl_BIT 18
#define SD_Status_RxFifoFull_MASK 0x20000
#define SD_Status_RxFifoFull 0x20000
#define SD_Status_RxFifoFull_BIT 17
#define SD_Status_TxFifoFull_MASK 0x10000
#define SD_Status_TxFifoFull 0x10000
#define SD_Status_TxFifoFull_BIT 16
#define SD_Status_RxFifoHalfFull_MASK 0x8000
#define SD_Status_RxFifoHalfFull 0x8000
#define SD_Status_RxFifoHalfFull_BIT 15
#define SD_Status_TxFifoHalfEmpty_MASK 0x4000
#define SD_Status_TxFifoHalfEmpty 0x4000
#define SD_Status_TxFifoHalfEmpty_BIT 14
#define SD_Status_RxActive_MASK 0x2000
#define SD_Status_RxActive 0x2000
#define SD_Status_RxActive_BIT 13
#define SD_Status_TxActive_MASK 0x1000
#define SD_Status_TxActive 0x1000
#define SD_Status_TxActive_BIT 12
#define SD_Status_CmdActive_MASK 0x800
#define SD_Status_CmdActive 0x800
#define SD_Status_CmdActive_BIT 11
#define SD_Status_DataBlockEnd_MASK 0x400
#define SD_Status_DataBlockEnd 0x400
#define SD_Status_DataBlockEnd_BIT 10
#define SD_Status_StartBitErr_MASK 0x200
#define SD_Status_StartBitErr 0x200
#define SD_Status_StartBitErr_BIT 9
#define SD_Status_DataEnd_MASK 0x100
#define SD_Status_DataEnd 0x100
#define SD_Status_DataEnd_BIT 8
#define SD_Status_CmdSent_MASK 0x80
#define SD_Status_CmdSent 0x80
#define SD_Status_CmdSent_BIT 7
#define SD_Status_CmdRespEnd_MASK 0x40
#define SD_Status_CmdRespEnd 0x40
#define SD_Status_CmdRespEnd_BIT 6
#define SD_Status_RxOverrun_MASK 0x20
#define SD_Status_RxOverrun 0x20
#define SD_Status_RxOverrun_BIT 5
#define SD_Status_TxUnderrun_MASK 0x10
#define SD_Status_TxUnderrun 0x10
#define SD_Status_TxUnderrun_BIT 4
#define SD_Status_DataTimeOut_MASK 0x8
#define SD_Status_DataTimeOut 0x8
#define SD_Status_DataTimeOut_BIT 3
#define SD_Status_CmdTimeOut_MASK 0x4
#define SD_Status_CmdTimeOut 0x4
#define SD_Status_CmdTimeOut_BIT 2
#define SD_Status_DataCrcFail_MASK 0x2
#define SD_Status_DataCrcFail 0x2
#define SD_Status_DataCrcFail_BIT 1
#define SD_Status_CmdCrcFail_MASK 0x1
#define SD_Status_CmdCrcFail 0x1
#define SD_Status_CmdCrcFail_BIT 0

#define SD_Clear (*(volatile unsigned *)0x20098038)
#define SD_Clear_DataBlockEndClr_MASK 0x400
#define SD_Clear_DataBlockEndClr 0x400
#define SD_Clear_DataBlockEndClr_BIT 10
#define SD_Clear_StartBitErrClr_MASK 0x200
#define SD_Clear_StartBitErrClr 0x200
#define SD_Clear_StartBitErrClr_BIT 9
#define SD_Clear_DataEndClr_MASK 0x100
#define SD_Clear_DataEndClr 0x100
#define SD_Clear_DataEndClr_BIT 8
#define SD_Clear_CmdSentClr_MASK 0x80
#define SD_Clear_CmdSentClr 0x80
#define SD_Clear_CmdSentClr_BIT 7
#define SD_Clear_CmdRespEndClr_MASK 0x40
#define SD_Clear_CmdRespEndClr 0x40
#define SD_Clear_CmdRespEndClr_BIT 6
#define SD_Clear_RxOverrunClr_MASK 0x20
#define SD_Clear_RxOverrunClr 0x20
#define SD_Clear_RxOverrunClr_BIT 5
#define SD_Clear_TxUnderrunClr_MASK 0x10
#define SD_Clear_TxUnderrunClr 0x10
#define SD_Clear_TxUnderrunClr_BIT 4
#define SD_Clear_DataTimeOutClr_MASK 0x8
#define SD_Clear_DataTimeOutClr 0x8
#define SD_Clear_DataTimeOutClr_BIT 3
#define SD_Clear_CmdTimeOutClr_MASK 0x4
#define SD_Clear_CmdTimeOutClr 0x4
#define SD_Clear_CmdTimeOutClr_BIT 2
#define SD_Clear_DataCrcFailClr_MASK 0x2
#define SD_Clear_DataCrcFailClr 0x2
#define SD_Clear_DataCrcFailClr_BIT 1
#define SD_Clear_CmdCrcFailClr_MASK 0x1
#define SD_Clear_CmdCrcFailClr 0x1
#define SD_Clear_CmdCrcFailClr_BIT 0

#define SD_Mask0 (*(volatile unsigned *)0x2009803C)

#define SD_Mask1 (*(volatile unsigned *)0x20098040)

#define SD_FIFOCnt (*(volatile unsigned *)0x20098048)
#define SD_FIFOCnt_DataCount_MASK 0x7FFF
#define SD_FIFOCnt_DataCount_BIT 0

#define SD_FIFO (*(volatile unsigned *)0x20098080)

#define I2S1DAO (*(volatile unsigned long *)0x2009C000)
#define I2S1DAO_wordwidth_MASK 0x3
#define I2S1DAO_wordwidth_BIT 0
#define I2S1DAO_mono_MASK 0x4
#define I2S1DAO_mono 0x4
#define I2S1DAO_mono_BIT 2
#define I2S1DAO_stop_MASK 0x8
#define I2S1DAO_stop 0x8
#define I2S1DAO_stop_BIT 3
#define I2S1DAO_reset_MASK 0x10
#define I2S1DAO_reset 0x10
#define I2S1DAO_reset_BIT 4
#define I2S1DAO_ws_sel_MASK 0x20
#define I2S1DAO_ws_sel 0x20
#define I2S1DAO_ws_sel_BIT 5
#define I2S1DAO_ws_halfperiod_MASK 0x3FC0
#define I2S1DAO_ws_halfperiod_BIT 6
#define I2S1DAO_mute_MASK 0x8000
#define I2S1DAO_mute 0x8000
#define I2S1DAO_mute_BIT 15

#define I2S1DAI (*(volatile unsigned long *)0x2009C004)
#define I2S1DAI_wordwidth_MASK 0x3
#define I2S1DAI_wordwidth_BIT 0
#define I2S1DAI_mono_MASK 0x4
#define I2S1DAI_mono 0x4
#define I2S1DAI_mono_BIT 2
#define I2S1DAI_stop_MASK 0x8
#define I2S1DAI_stop 0x8
#define I2S1DAI_stop_BIT 3
#define I2S1DAI_reset_MASK 0x10
#define I2S1DAI_reset 0x10
#define I2S1DAI_reset_BIT 4
#define I2S1DAI_ws_sel_MASK 0x20
#define I2S1DAI_ws_sel 0x20
#define I2S1DAI_ws_sel_BIT 5
#define I2S1DAI_ws_halfperiod_MASK 0x3FC0
#define I2S1DAI_ws_halfperiod_BIT 6

#define I2S1TXFIFO (*(volatile unsigned long *)0x2009C008)

#define I2S1RXFIFO (*(volatile unsigned long *)0x2009C00C)

#define I2S1STATE (*(volatile unsigned long *)0x2009C010)
#define I2S1STATE_irq_MASK 0x1
#define I2S1STATE_irq 0x1
#define I2S1STATE_irq_BIT 0
#define I2S1STATE_dma0_MASK 0x2
#define I2S1STATE_dma0 0x2
#define I2S1STATE_dma0_BIT 1
#define I2S1STATE_dma1_MASK 0x4
#define I2S1STATE_dma1 0x4
#define I2S1STATE_dma1_BIT 2
#define I2S1STATE_rx_level_MASK 0xF00
#define I2S1STATE_rx_level_BIT 8
#define I2S1STATE_tx_level_MASK 0xF0000
#define I2S1STATE_tx_level_BIT 16

#define I2S1DMA0 (*(volatile unsigned long *)0x2009C014)
#define I2S1DMA0_rx_dma0_enable_MASK 0x1
#define I2S1DMA0_rx_dma0_enable 0x1
#define I2S1DMA0_rx_dma0_enable_BIT 0
#define I2S1DMA0_tx_dma0_enable_MASK 0x2
#define I2S1DMA0_tx_dma0_enable 0x2
#define I2S1DMA0_tx_dma0_enable_BIT 1
#define I2S1DMA0_rx_depth_dma0_MASK 0x700
#define I2S1DMA0_rx_depth_dma0_BIT 8
#define I2S1DMA0_tx_depth_dma0_MASK 0x70000
#define I2S1DMA0_tx_depth_dma0_BIT 16

#define I2S1DMA1 (*(volatile unsigned long *)0x2009C018)
#define I2S1DMA1_rx_dma1_enable_MASK 0x1
#define I2S1DMA1_rx_dma1_enable 0x1
#define I2S1DMA1_rx_dma1_enable_BIT 0
#define I2S1DMA1_tx_dma1_enable_MASK 0x2
#define I2S1DMA1_tx_dma1_enable 0x2
#define I2S1DMA1_tx_dma1_enable_BIT 1
#define I2S1DMA1_rx_depth_dma1_MASK 0x700
#define I2S1DMA1_rx_depth_dma1_BIT 8
#define I2S1DMA1_tx_depth_dma1_MASK 0x70000
#define I2S1DMA1_tx_depth_dma1_BIT 16

#define I2S1IRQ (*(volatile unsigned long *)0x2009C01C)
#define I2S1IRQ_rx_irq_enable_MASK 0x1
#define I2S1IRQ_rx_irq_enable 0x1
#define I2S1IRQ_rx_irq_enable_BIT 0
#define I2S1IRQ_tx_irq_enable_MASK 0x2
#define I2S1IRQ_tx_irq_enable 0x2
#define I2S1IRQ_tx_irq_enable_BIT 1
#define I2S1IRQ_rx_depth_irq_MASK 0x700
#define I2S1IRQ_rx_depth_irq_BIT 8
#define I2S1IRQ_tx_depth_irq_MASK 0x70000
#define I2S1IRQ_tx_depth_irq_BIT 16

#define I2S1TXRATE (*(volatile unsigned long *)0x2009C020)
#define I2S1TXRATE_Y_divider_MASK 0xFF
#define I2S1TXRATE_Y_divider_BIT 0
#define I2S1TXRATE_X_divider_MASK 0xFF00
#define I2S1TXRATE_X_divider_BIT 8

#define I2S1RXRATE (*(volatile unsigned long *)0x2009C024)
#define I2S1RXRATE_Y_divider_MASK 0xFF
#define I2S1RXRATE_Y_divider_BIT 0
#define I2S1RXRATE_X_divider_MASK 0xFF00
#define I2S1RXRATE_X_divider_BIT 8

#define MLC_CMD (*(volatile unsigned *)0x200B8000)
#define MLC_CMD_Command_Code_MASK 0xFF
#define MLC_CMD_Command_Code_BIT 0

#define MLC_ADDR (*(volatile unsigned *)0x200B8004)
#define MLC_ADDR_Address_MASK 0xFF
#define MLC_ADDR_Address_BIT 0

#define MLC_ECC_ENC_REG (*(volatile unsigned *)0x200B8008)
#define MLC_ECC_ENC_REG_Data_MASK 0xFF
#define MLC_ECC_ENC_REG_Data_BIT 0

#define MLC_ECC_DEC_REG (*(volatile unsigned *)0x200B800C)
#define MLC_ECC_DEC_REG_Data_MASK 0xFF
#define MLC_ECC_DEC_REG_Data_BIT 0

#define MLC_ECC_AUTO_ENC_REG (*(volatile unsigned *)0x200B8010)
#define MLC_ECC_AUTO_ENC_REG_Auto_Program_command_enabled_MASK 0x100
#define MLC_ECC_AUTO_ENC_REG_Auto_Program_command_enabled 0x100
#define MLC_ECC_AUTO_ENC_REG_Auto_Program_command_enabled_BIT 8
#define MLC_ECC_AUTO_ENC_REG_Auto_Program_command_MASK 0xFF
#define MLC_ECC_AUTO_ENC_REG_Auto_Program_command_BIT 0

#define MLC_ECC_AUTO_DEC_REG (*(volatile unsigned *)0x200B8014)
#define MLC_ECC_AUTO_DEC_REG_Data_MASK 0xFF
#define MLC_ECC_AUTO_DEC_REG_Data_BIT 0

#define MLC_RPR (*(volatile unsigned *)0x200B8018)
#define MLC_RPR_Data_MASK 0xFF
#define MLC_RPR_Data_BIT 0

#define MLC_WPR (*(volatile unsigned *)0x200B801C)
#define MLC_WPR_Data_MASK 0xFF
#define MLC_WPR_Data_BIT 0

#define MLC_RUBP (*(volatile unsigned *)0x200B8020)
#define MLC_RUBP_Data_MASK 0xFF
#define MLC_RUBP_Data_BIT 0

#define MLC_ROBP (*(volatile unsigned *)0x200B8024)
#define MLC_ROBP_Data_MASK 0xFF
#define MLC_ROBP_Data_BIT 0

#define MLC_SW_WP_ADD_LOW (*(volatile unsigned *)0x200B8028)
#define MLC_SW_WP_ADD_LOW_Lower_bound_MASK 0xFFFFFF
#define MLC_SW_WP_ADD_LOW_Lower_bound_BIT 0

#define MLC_SW_WP_ADD_HIG (*(volatile unsigned *)0x200B802C)
#define MLC_SW_WP_ADD_HIG_Upper_bound_MASK 0xFFFFFF
#define MLC_SW_WP_ADD_HIG_Upper_bound_BIT 0

#define MLC_ICR (*(volatile unsigned *)0x200B8030)
#define MLC_ICR_Software_Write_Protection_Enabled_MASK 0x8
#define MLC_ICR_Software_Write_Protection_Enabled 0x8
#define MLC_ICR_Software_Write_Protection_Enabled_BIT 3
#define MLC_ICR_Large_Block_Flash_Device_MASK 0x4
#define MLC_ICR_Large_Block_Flash_Device 0x4
#define MLC_ICR_Large_Block_Flash_Device_BIT 2
#define MLC_ICR_NAND_Flash_Word_Count_4_MASK 0x2
#define MLC_ICR_NAND_Flash_Word_Count_4 0x2
#define MLC_ICR_NAND_Flash_Word_Count_4_BIT 1
#define MLC_ICR_NAND_Flash_IO_Bus_With_16_Bit_MASK 0x1
#define MLC_ICR_NAND_Flash_IO_Bus_With_16_Bit 0x1
#define MLC_ICR_NAND_Flash_IO_Bus_With_16_Bit_BIT 0

#define MLC_TIME_REG (*(volatile unsigned *)0x200B8034)
#define MLC_TIME_REG_TCEA_DELAY_MASK 0x3000000
#define MLC_TIME_REG_TCEA_DELAY_BIT 24
#define MLC_TIME_REG_BUSY_DELAY_MASK 0xF80000
#define MLC_TIME_REG_BUSY_DELAY_BIT 19
#define MLC_TIME_REG_NAND_TA_MASK 0xF0000
#define MLC_TIME_REG_NAND_TA_BIT 16
#define MLC_TIME_REG_RD_HIGH_MASK 0xF000
#define MLC_TIME_REG_RD_HIGH_BIT 12
#define MLC_TIME_REG_RD_LOW_MASK 0xF00
#define MLC_TIME_REG_RD_LOW_BIT 8
#define MLC_TIME_REG_WR_HIGH_MASK 0xF0
#define MLC_TIME_REG_WR_HIGH_BIT 4
#define MLC_TIME_REG_WR_LOW_MASK 0xF
#define MLC_TIME_REG_WR_LOW_BIT 0

#define MLC_IRQ_MR (*(volatile unsigned *)0x200B8038)
#define MLC_IRQ_MR_NAND_Ready_MASK 0x20
#define MLC_IRQ_MR_NAND_Ready 0x20
#define MLC_IRQ_MR_NAND_Ready_BIT 5
#define MLC_IRQ_MR_Controller_Ready_MASK 0x10
#define MLC_IRQ_MR_Controller_Ready 0x10
#define MLC_IRQ_MR_Controller_Ready_BIT 4
#define MLC_IRQ_MR_Decode_Failure_MASK 0x8
#define MLC_IRQ_MR_Decode_Failure 0x8
#define MLC_IRQ_MR_Decode_Failure_BIT 3
#define MLC_IRQ_MR_Decode_Error_Detected_MASK 0x4
#define MLC_IRQ_MR_Decode_Error_Detected 0x4
#define MLC_IRQ_MR_Decode_Error_Detected_BIT 2
#define MLC_IRQ_MR_ECC_Encode_Decode_Ready_MASK 0x2
#define MLC_IRQ_MR_ECC_Encode_Decode_Ready 0x2
#define MLC_IRQ_MR_ECC_Encode_Decode_Ready_BIT 1
#define MLC_IRQ_MR_Software_Write_Protection_Fault_MASK 0x1
#define MLC_IRQ_MR_Software_Write_Protection_Fault 0x1
#define MLC_IRQ_MR_Software_Write_Protection_Fault_BIT 0

#define MLC_IRQ_SR (*(volatile unsigned *)0x200B803C)
#define MLC_IRQ_SR_NAND_Ready_MASK 0x20
#define MLC_IRQ_SR_NAND_Ready 0x20
#define MLC_IRQ_SR_NAND_Ready_BIT 5
#define MLC_IRQ_SR_Controller_Ready_MASK 0x10
#define MLC_IRQ_SR_Controller_Ready 0x10
#define MLC_IRQ_SR_Controller_Ready_BIT 4
#define MLC_IRQ_SR_Decode_Failure_MASK 0x8
#define MLC_IRQ_SR_Decode_Failure 0x8
#define MLC_IRQ_SR_Decode_Failure_BIT 3
#define MLC_IRQ_SR_Decode_Error_Detected_MASK 0x4
#define MLC_IRQ_SR_Decode_Error_Detected 0x4
#define MLC_IRQ_SR_Decode_Error_Detected_BIT 2
#define MLC_IRQ_SR_ECC_Encode_Decode_Ready_MASK 0x2
#define MLC_IRQ_SR_ECC_Encode_Decode_Ready 0x2
#define MLC_IRQ_SR_ECC_Encode_Decode_Ready_BIT 1
#define MLC_IRQ_SR_Software_Write_Protection_Fault_MASK 0x1
#define MLC_IRQ_SR_Software_Write_Protection_Fault 0x1
#define MLC_IRQ_SR_Software_Write_Protection_Fault_BIT 0

#define MLC_LOCK_PR (*(volatile unsigned *)0x200B8044)
#define MLC_LOCK_PR_Data_MASK 0xFFFF
#define MLC_LOCK_PR_Data_BIT 0

#define MLC_ISR (*(volatile unsigned *)0x200B8048)
#define MLC_ISR_Decoder_Failure_MASK 0x40
#define MLC_ISR_Decoder_Failure 0x40
#define MLC_ISR_Decoder_Failure_BIT 6
#define MLC_ISR_Number_Of_RS_Symbols_Errors_MASK 0x30
#define MLC_ISR_Number_Of_RS_Symbols_Errors_BIT 4
#define MLC_ISR_ERRORS_DETECTED_MASK 0x8
#define MLC_ISR_ERRORS_DETECTED 0x8
#define MLC_ISR_ERRORS_DETECTED_BIT 3
#define MLC_ISR_ECC_READY_MASK 0x4
#define MLC_ISR_ECC_READY 0x4
#define MLC_ISR_ECC_READY_BIT 2
#define MLC_ISR_Controller_READY_MASK 0x2
#define MLC_ISR_Controller_READY 0x2
#define MLC_ISR_Controller_READY_BIT 1
#define MLC_ISR_NAND_READY_MASK 0x1
#define MLC_ISR_NAND_READY 0x1
#define MLC_ISR_NAND_READY_BIT 0

#define MLC_CEH (*(volatile unsigned *)0x200B804C)
#define MLC_CEH_Force_nCE_assert_MASK 0x1
#define MLC_CEH_Force_nCE_assert 0x1
#define MLC_CEH_Force_nCE_assert_BIT 0

#define DMACIntStat (*(volatile unsigned *)0x31000000)
#define DMACIntStat_C0_MASK 0x1
#define DMACIntStat_C0 0x1
#define DMACIntStat_C0_BIT 0
#define DMACIntStat_C1_MASK 0x2
#define DMACIntStat_C1 0x2
#define DMACIntStat_C1_BIT 1
#define DMACIntStat_C2_MASK 0x4
#define DMACIntStat_C2 0x4
#define DMACIntStat_C2_BIT 2
#define DMACIntStat_C3_MASK 0x8
#define DMACIntStat_C3 0x8
#define DMACIntStat_C3_BIT 3
#define DMACIntStat_C4_MASK 0x10
#define DMACIntStat_C4 0x10
#define DMACIntStat_C4_BIT 4
#define DMACIntStat_C5_MASK 0x20
#define DMACIntStat_C5 0x20
#define DMACIntStat_C5_BIT 5
#define DMACIntStat_C6_MASK 0x40
#define DMACIntStat_C6 0x40
#define DMACIntStat_C6_BIT 6
#define DMACIntStat_C7_MASK 0x80
#define DMACIntStat_C7 0x80
#define DMACIntStat_C7_BIT 7

#define DMACIntTCStat (*(volatile unsigned *)0x31000004)
#define DMACIntTCStat_C0_MASK 0x1
#define DMACIntTCStat_C0 0x1
#define DMACIntTCStat_C0_BIT 0
#define DMACIntTCStat_C1_MASK 0x2
#define DMACIntTCStat_C1 0x2
#define DMACIntTCStat_C1_BIT 1
#define DMACIntTCStat_C2_MASK 0x4
#define DMACIntTCStat_C2 0x4
#define DMACIntTCStat_C2_BIT 2
#define DMACIntTCStat_C3_MASK 0x8
#define DMACIntTCStat_C3 0x8
#define DMACIntTCStat_C3_BIT 3
#define DMACIntTCStat_C4_MASK 0x10
#define DMACIntTCStat_C4 0x10
#define DMACIntTCStat_C4_BIT 4
#define DMACIntTCStat_C5_MASK 0x20
#define DMACIntTCStat_C5 0x20
#define DMACIntTCStat_C5_BIT 5
#define DMACIntTCStat_C6_MASK 0x40
#define DMACIntTCStat_C6 0x40
#define DMACIntTCStat_C6_BIT 6
#define DMACIntTCStat_C7_MASK 0x80
#define DMACIntTCStat_C7 0x80
#define DMACIntTCStat_C7_BIT 7

#define DMACIntTCClear (*(volatile unsigned *)0x31000008)
#define DMACIntTCClear_C0_MASK 0x1
#define DMACIntTCClear_C0 0x1
#define DMACIntTCClear_C0_BIT 0
#define DMACIntTCClear_C1_MASK 0x2
#define DMACIntTCClear_C1 0x2
#define DMACIntTCClear_C1_BIT 1
#define DMACIntTCClear_C2_MASK 0x4
#define DMACIntTCClear_C2 0x4
#define DMACIntTCClear_C2_BIT 2
#define DMACIntTCClear_C3_MASK 0x8
#define DMACIntTCClear_C3 0x8
#define DMACIntTCClear_C3_BIT 3
#define DMACIntTCClear_C4_MASK 0x10
#define DMACIntTCClear_C4 0x10
#define DMACIntTCClear_C4_BIT 4
#define DMACIntTCClear_C5_MASK 0x20
#define DMACIntTCClear_C5 0x20
#define DMACIntTCClear_C5_BIT 5
#define DMACIntTCClear_C6_MASK 0x40
#define DMACIntTCClear_C6 0x40
#define DMACIntTCClear_C6_BIT 6
#define DMACIntTCClear_C7_MASK 0x80
#define DMACIntTCClear_C7 0x80
#define DMACIntTCClear_C7_BIT 7

#define DMACIntErrStat (*(volatile unsigned *)0x3100000C)
#define DMACIntErrStat_C0_MASK 0x1
#define DMACIntErrStat_C0 0x1
#define DMACIntErrStat_C0_BIT 0
#define DMACIntErrStat_C1_MASK 0x2
#define DMACIntErrStat_C1 0x2
#define DMACIntErrStat_C1_BIT 1
#define DMACIntErrStat_C2_MASK 0x4
#define DMACIntErrStat_C2 0x4
#define DMACIntErrStat_C2_BIT 2
#define DMACIntErrStat_C3_MASK 0x8
#define DMACIntErrStat_C3 0x8
#define DMACIntErrStat_C3_BIT 3
#define DMACIntErrStat_C4_MASK 0x10
#define DMACIntErrStat_C4 0x10
#define DMACIntErrStat_C4_BIT 4
#define DMACIntErrStat_C5_MASK 0x20
#define DMACIntErrStat_C5 0x20
#define DMACIntErrStat_C5_BIT 5
#define DMACIntErrStat_C6_MASK 0x40
#define DMACIntErrStat_C6 0x40
#define DMACIntErrStat_C6_BIT 6
#define DMACIntErrStat_C7_MASK 0x80
#define DMACIntErrStat_C7 0x80
#define DMACIntErrStat_C7_BIT 7

#define DMACIntErrClr (*(volatile unsigned *)0x31000010)
#define DMACIntErrClr_C0_MASK 0x1
#define DMACIntErrClr_C0 0x1
#define DMACIntErrClr_C0_BIT 0
#define DMACIntErrClr_C1_MASK 0x2
#define DMACIntErrClr_C1 0x2
#define DMACIntErrClr_C1_BIT 1
#define DMACIntErrClr_C2_MASK 0x4
#define DMACIntErrClr_C2 0x4
#define DMACIntErrClr_C2_BIT 2
#define DMACIntErrClr_C3_MASK 0x8
#define DMACIntErrClr_C3 0x8
#define DMACIntErrClr_C3_BIT 3
#define DMACIntErrClr_C4_MASK 0x10
#define DMACIntErrClr_C4 0x10
#define DMACIntErrClr_C4_BIT 4
#define DMACIntErrClr_C5_MASK 0x20
#define DMACIntErrClr_C5 0x20
#define DMACIntErrClr_C5_BIT 5
#define DMACIntErrClr_C6_MASK 0x40
#define DMACIntErrClr_C6 0x40
#define DMACIntErrClr_C6_BIT 6
#define DMACIntErrClr_C7_MASK 0x80
#define DMACIntErrClr_C7 0x80
#define DMACIntErrClr_C7_BIT 7

#define DMACRawIntTCStat (*(volatile unsigned *)0x31000014)
#define DMACRawIntTCStat_C0_MASK 0x1
#define DMACRawIntTCStat_C0 0x1
#define DMACRawIntTCStat_C0_BIT 0
#define DMACRawIntTCStat_C1_MASK 0x2
#define DMACRawIntTCStat_C1 0x2
#define DMACRawIntTCStat_C1_BIT 1
#define DMACRawIntTCStat_C2_MASK 0x4
#define DMACRawIntTCStat_C2 0x4
#define DMACRawIntTCStat_C2_BIT 2
#define DMACRawIntTCStat_C3_MASK 0x8
#define DMACRawIntTCStat_C3 0x8
#define DMACRawIntTCStat_C3_BIT 3
#define DMACRawIntTCStat_C4_MASK 0x10
#define DMACRawIntTCStat_C4 0x10
#define DMACRawIntTCStat_C4_BIT 4
#define DMACRawIntTCStat_C5_MASK 0x20
#define DMACRawIntTCStat_C5 0x20
#define DMACRawIntTCStat_C5_BIT 5
#define DMACRawIntTCStat_C6_MASK 0x40
#define DMACRawIntTCStat_C6 0x40
#define DMACRawIntTCStat_C6_BIT 6
#define DMACRawIntTCStat_C7_MASK 0x80
#define DMACRawIntTCStat_C7 0x80
#define DMACRawIntTCStat_C7_BIT 7

#define DMACRawIntErrStat (*(volatile unsigned *)0x31000018)
#define DMACRawIntErrStat_C0_MASK 0x1
#define DMACRawIntErrStat_C0 0x1
#define DMACRawIntErrStat_C0_BIT 0
#define DMACRawIntErrStat_C1_MASK 0x2
#define DMACRawIntErrStat_C1 0x2
#define DMACRawIntErrStat_C1_BIT 1
#define DMACRawIntErrStat_C2_MASK 0x4
#define DMACRawIntErrStat_C2 0x4
#define DMACRawIntErrStat_C2_BIT 2
#define DMACRawIntErrStat_C3_MASK 0x8
#define DMACRawIntErrStat_C3 0x8
#define DMACRawIntErrStat_C3_BIT 3
#define DMACRawIntErrStat_C4_MASK 0x10
#define DMACRawIntErrStat_C4 0x10
#define DMACRawIntErrStat_C4_BIT 4
#define DMACRawIntErrStat_C5_MASK 0x20
#define DMACRawIntErrStat_C5 0x20
#define DMACRawIntErrStat_C5_BIT 5
#define DMACRawIntErrStat_C6_MASK 0x40
#define DMACRawIntErrStat_C6 0x40
#define DMACRawIntErrStat_C6_BIT 6
#define DMACRawIntErrStat_C7_MASK 0x80
#define DMACRawIntErrStat_C7 0x80
#define DMACRawIntErrStat_C7_BIT 7

#define DMACEnbldChns (*(volatile unsigned *)0x3100001C)
#define DMACEnbldChns_C0_MASK 0x1
#define DMACEnbldChns_C0 0x1
#define DMACEnbldChns_C0_BIT 0
#define DMACEnbldChns_C1_MASK 0x2
#define DMACEnbldChns_C1 0x2
#define DMACEnbldChns_C1_BIT 1
#define DMACEnbldChns_C2_MASK 0x4
#define DMACEnbldChns_C2 0x4
#define DMACEnbldChns_C2_BIT 2
#define DMACEnbldChns_C3_MASK 0x8
#define DMACEnbldChns_C3 0x8
#define DMACEnbldChns_C3_BIT 3
#define DMACEnbldChns_C4_MASK 0x10
#define DMACEnbldChns_C4 0x10
#define DMACEnbldChns_C4_BIT 4
#define DMACEnbldChns_C5_MASK 0x20
#define DMACEnbldChns_C5 0x20
#define DMACEnbldChns_C5_BIT 5
#define DMACEnbldChns_C6_MASK 0x40
#define DMACEnbldChns_C6 0x40
#define DMACEnbldChns_C6_BIT 6
#define DMACEnbldChns_C7_MASK 0x80
#define DMACEnbldChns_C7 0x80
#define DMACEnbldChns_C7_BIT 7

#define DMACSoftBReq (*(volatile unsigned *)0x31000020)
#define DMACSoftBReq_SoftBReq_MASK 0xFFFF
#define DMACSoftBReq_SoftBReq_BIT 0

#define DMACSoftSReq (*(volatile unsigned *)0x31000024)
#define DMACSoftSReq_SoftSReq_MASK 0xFFFF
#define DMACSoftSReq_SoftSReq_BIT 0

#define DMACSoftLBReq (*(volatile unsigned *)0x31000028)
#define DMACSoftLBReq_SoftLBReq_MASK 0xFFFF
#define DMACSoftLBReq_SoftLBReq_BIT 0

#define DMACSoftLSReq (*(volatile unsigned *)0x3100002C)
#define DMACSoftLSReq_SoftLSReq_MASK 0xFFFF
#define DMACSoftLSReq_SoftLSReq_BIT 0

#define DMACConfig (*(volatile unsigned *)0x31000030)
#define DMACConfig_M1_MASK 0x4
#define DMACConfig_M1 0x4
#define DMACConfig_M1_BIT 2
#define DMACConfig_M0_MASK 0x2
#define DMACConfig_M0 0x2
#define DMACConfig_M0_BIT 1
#define DMACConfig_E_MASK 0x1
#define DMACConfig_E 0x1
#define DMACConfig_E_BIT 0

#define DMACC0SrcAddr (*(volatile unsigned *)0x31000100)

#define DMACC0DestAddr (*(volatile unsigned *)0x31000104)

#define DMACC0LLI (*(volatile unsigned *)0x31000108)
#define DMACC0LLI_LLI_MASK 0xFFFFFFFC
#define DMACC0LLI_LLI_BIT 2
#define DMACC0LLI_LM_MASK 0x1
#define DMACC0LLI_LM 0x1
#define DMACC0LLI_LM_BIT 0

#define DMACC0Control (*(volatile unsigned *)0x3100010C)
#define DMACC0Control_I_MASK 0x80000000
#define DMACC0Control_I 0x80000000
#define DMACC0Control_I_BIT 31
#define DMACC0Control_Prot3_MASK 0x40000000
#define DMACC0Control_Prot3 0x40000000
#define DMACC0Control_Prot3_BIT 30
#define DMACC0Control_Prot2_MASK 0x20000000
#define DMACC0Control_Prot2 0x20000000
#define DMACC0Control_Prot2_BIT 29
#define DMACC0Control_Prot1_MASK 0x10000000
#define DMACC0Control_Prot1 0x10000000
#define DMACC0Control_Prot1_BIT 28
#define DMACC0Control_DI_MASK 0x8000000
#define DMACC0Control_DI 0x8000000
#define DMACC0Control_DI_BIT 27
#define DMACC0Control_SI_MASK 0x4000000
#define DMACC0Control_SI 0x4000000
#define DMACC0Control_SI_BIT 26
#define DMACC0Control_D_MASK 0x2000000
#define DMACC0Control_D 0x2000000
#define DMACC0Control_D_BIT 25
#define DMACC0Control_S_MASK 0x1000000
#define DMACC0Control_S 0x1000000
#define DMACC0Control_S_BIT 24
#define DMACC0Control_DWidth_MASK 0xE00000
#define DMACC0Control_DWidth_BIT 21
#define DMACC0Control_SWidth_MASK 0x1C0000
#define DMACC0Control_SWidth_BIT 18
#define DMACC0Control_DBSize_MASK 0x38000
#define DMACC0Control_DBSize_BIT 15
#define DMACC0Control_SBSize_MASK 0x7000
#define DMACC0Control_SBSize_BIT 12
#define DMACC0Control_TransferSize_MASK 0xFFF
#define DMACC0Control_TransferSize_BIT 0

#define DMACC0Config (*(volatile unsigned *)0x31000110)
#define DMACC0Config_H_MASK 0x40000
#define DMACC0Config_H 0x40000
#define DMACC0Config_H_BIT 18
#define DMACC0Config_A_MASK 0x20000
#define DMACC0Config_A 0x20000
#define DMACC0Config_A_BIT 17
#define DMACC0Config_L_MASK 0x10000
#define DMACC0Config_L 0x10000
#define DMACC0Config_L_BIT 16
#define DMACC0Config_ITC_MASK 0x8000
#define DMACC0Config_ITC 0x8000
#define DMACC0Config_ITC_BIT 15
#define DMACC0Config_IE_MASK 0x4000
#define DMACC0Config_IE 0x4000
#define DMACC0Config_IE_BIT 14
#define DMACC0Config_FlowCntrl_MASK 0x3800
#define DMACC0Config_FlowCntrl_BIT 11
#define DMACC0Config_DestPeripheral_MASK 0x7C0
#define DMACC0Config_DestPeripheral_BIT 6
#define DMACC0Config_SrcPeripheral_MASK 0x3E
#define DMACC0Config_SrcPeripheral_BIT 1
#define DMACC0Config_E_MASK 0x1
#define DMACC0Config_E 0x1
#define DMACC0Config_E_BIT 0

#define DMACC1SrcAddr (*(volatile unsigned *)0x31000120)

#define DMACC1DestAddr (*(volatile unsigned *)0x31000124)

#define DMACC1LLI (*(volatile unsigned *)0x31000128)
#define DMACC1LLI_LLI_MASK 0xFFFFFFFC
#define DMACC1LLI_LLI_BIT 2
#define DMACC1LLI_LM_MASK 0x1
#define DMACC1LLI_LM 0x1
#define DMACC1LLI_LM_BIT 0

#define DMACC1Control (*(volatile unsigned *)0x3100012C)
#define DMACC1Control_I_MASK 0x80000000
#define DMACC1Control_I 0x80000000
#define DMACC1Control_I_BIT 31
#define DMACC1Control_Prot3_MASK 0x40000000
#define DMACC1Control_Prot3 0x40000000
#define DMACC1Control_Prot3_BIT 30
#define DMACC1Control_Prot2_MASK 0x20000000
#define DMACC1Control_Prot2 0x20000000
#define DMACC1Control_Prot2_BIT 29
#define DMACC1Control_Prot1_MASK 0x10000000
#define DMACC1Control_Prot1 0x10000000
#define DMACC1Control_Prot1_BIT 28
#define DMACC1Control_DI_MASK 0x8000000
#define DMACC1Control_DI 0x8000000
#define DMACC1Control_DI_BIT 27
#define DMACC1Control_SI_MASK 0x4000000
#define DMACC1Control_SI 0x4000000
#define DMACC1Control_SI_BIT 26
#define DMACC1Control_D_MASK 0x2000000
#define DMACC1Control_D 0x2000000
#define DMACC1Control_D_BIT 25
#define DMACC1Control_S_MASK 0x1000000
#define DMACC1Control_S 0x1000000
#define DMACC1Control_S_BIT 24
#define DMACC1Control_DWidth_MASK 0xE00000
#define DMACC1Control_DWidth_BIT 21
#define DMACC1Control_SWidth_MASK 0x1C0000
#define DMACC1Control_SWidth_BIT 18
#define DMACC1Control_DBSize_MASK 0x38000
#define DMACC1Control_DBSize_BIT 15
#define DMACC1Control_SBSize_MASK 0x7000
#define DMACC1Control_SBSize_BIT 12
#define DMACC1Control_TransferSize_MASK 0xFFF
#define DMACC1Control_TransferSize_BIT 0

#define DMACC1Config (*(volatile unsigned *)0x31000130)
#define DMACC1Config_H_MASK 0x40000
#define DMACC1Config_H 0x40000
#define DMACC1Config_H_BIT 18
#define DMACC1Config_A_MASK 0x20000
#define DMACC1Config_A 0x20000
#define DMACC1Config_A_BIT 17
#define DMACC1Config_L_MASK 0x10000
#define DMACC1Config_L 0x10000
#define DMACC1Config_L_BIT 16
#define DMACC1Config_ITC_MASK 0x8000
#define DMACC1Config_ITC 0x8000
#define DMACC1Config_ITC_BIT 15
#define DMACC1Config_IE_MASK 0x4000
#define DMACC1Config_IE 0x4000
#define DMACC1Config_IE_BIT 14
#define DMACC1Config_FlowCntrl_MASK 0x3800
#define DMACC1Config_FlowCntrl_BIT 11
#define DMACC1Config_DestPeripheral_MASK 0x7C0
#define DMACC1Config_DestPeripheral_BIT 6
#define DMACC1Config_SrcPeripheral_MASK 0x3E
#define DMACC1Config_SrcPeripheral_BIT 1
#define DMACC1Config_E_MASK 0x1
#define DMACC1Config_E 0x1
#define DMACC1Config_E_BIT 0

#define DMACC2SrcAddr (*(volatile unsigned *)0x31000140)

#define DMACC2DestAddr (*(volatile unsigned *)0x31000144)

#define DMACC2LLI (*(volatile unsigned *)0x31000148)
#define DMACC2LLI_LLI_MASK 0xFFFFFFFC
#define DMACC2LLI_LLI_BIT 2
#define DMACC2LLI_LM_MASK 0x1
#define DMACC2LLI_LM 0x1
#define DMACC2LLI_LM_BIT 0

#define DMACC2Control (*(volatile unsigned *)0x3100014C)
#define DMACC2Control_I_MASK 0x80000000
#define DMACC2Control_I 0x80000000
#define DMACC2Control_I_BIT 31
#define DMACC2Control_Prot3_MASK 0x40000000
#define DMACC2Control_Prot3 0x40000000
#define DMACC2Control_Prot3_BIT 30
#define DMACC2Control_Prot2_MASK 0x20000000
#define DMACC2Control_Prot2 0x20000000
#define DMACC2Control_Prot2_BIT 29
#define DMACC2Control_Prot1_MASK 0x10000000
#define DMACC2Control_Prot1 0x10000000
#define DMACC2Control_Prot1_BIT 28
#define DMACC2Control_DI_MASK 0x8000000
#define DMACC2Control_DI 0x8000000
#define DMACC2Control_DI_BIT 27
#define DMACC2Control_SI_MASK 0x4000000
#define DMACC2Control_SI 0x4000000
#define DMACC2Control_SI_BIT 26
#define DMACC2Control_D_MASK 0x2000000
#define DMACC2Control_D 0x2000000
#define DMACC2Control_D_BIT 25
#define DMACC2Control_S_MASK 0x1000000
#define DMACC2Control_S 0x1000000
#define DMACC2Control_S_BIT 24
#define DMACC2Control_DWidth_MASK 0xE00000
#define DMACC2Control_DWidth_BIT 21
#define DMACC2Control_SWidth_MASK 0x1C0000
#define DMACC2Control_SWidth_BIT 18
#define DMACC2Control_DBSize_MASK 0x38000
#define DMACC2Control_DBSize_BIT 15
#define DMACC2Control_SBSize_MASK 0x7000
#define DMACC2Control_SBSize_BIT 12
#define DMACC2Control_TransferSize_MASK 0xFFF
#define DMACC2Control_TransferSize_BIT 0

#define DMACC2Config (*(volatile unsigned *)0x31000150)
#define DMACC2Config_H_MASK 0x40000
#define DMACC2Config_H 0x40000
#define DMACC2Config_H_BIT 18
#define DMACC2Config_A_MASK 0x20000
#define DMACC2Config_A 0x20000
#define DMACC2Config_A_BIT 17
#define DMACC2Config_L_MASK 0x10000
#define DMACC2Config_L 0x10000
#define DMACC2Config_L_BIT 16
#define DMACC2Config_ITC_MASK 0x8000
#define DMACC2Config_ITC 0x8000
#define DMACC2Config_ITC_BIT 15
#define DMACC2Config_IE_MASK 0x4000
#define DMACC2Config_IE 0x4000
#define DMACC2Config_IE_BIT 14
#define DMACC2Config_FlowCntrl_MASK 0x3800
#define DMACC2Config_FlowCntrl_BIT 11
#define DMACC2Config_DestPeripheral_MASK 0x7C0
#define DMACC2Config_DestPeripheral_BIT 6
#define DMACC2Config_SrcPeripheral_MASK 0x3E
#define DMACC2Config_SrcPeripheral_BIT 1
#define DMACC2Config_E_MASK 0x1
#define DMACC2Config_E 0x1
#define DMACC2Config_E_BIT 0

#define DMACC3SrcAddr (*(volatile unsigned *)0x31000160)

#define DMACC3DestAddr (*(volatile unsigned *)0x31000164)

#define DMACC3LLI (*(volatile unsigned *)0x31000168)
#define DMACC3LLI_LLI_MASK 0xFFFFFFFC
#define DMACC3LLI_LLI_BIT 2
#define DMACC3LLI_LM_MASK 0x1
#define DMACC3LLI_LM 0x1
#define DMACC3LLI_LM_BIT 0

#define DMACC3Control (*(volatile unsigned *)0x3100016C)
#define DMACC3Control_I_MASK 0x80000000
#define DMACC3Control_I 0x80000000
#define DMACC3Control_I_BIT 31
#define DMACC3Control_Prot3_MASK 0x40000000
#define DMACC3Control_Prot3 0x40000000
#define DMACC3Control_Prot3_BIT 30
#define DMACC3Control_Prot2_MASK 0x20000000
#define DMACC3Control_Prot2 0x20000000
#define DMACC3Control_Prot2_BIT 29
#define DMACC3Control_Prot1_MASK 0x10000000
#define DMACC3Control_Prot1 0x10000000
#define DMACC3Control_Prot1_BIT 28
#define DMACC3Control_DI_MASK 0x8000000
#define DMACC3Control_DI 0x8000000
#define DMACC3Control_DI_BIT 27
#define DMACC3Control_SI_MASK 0x4000000
#define DMACC3Control_SI 0x4000000
#define DMACC3Control_SI_BIT 26
#define DMACC3Control_D_MASK 0x2000000
#define DMACC3Control_D 0x2000000
#define DMACC3Control_D_BIT 25
#define DMACC3Control_S_MASK 0x1000000
#define DMACC3Control_S 0x1000000
#define DMACC3Control_S_BIT 24
#define DMACC3Control_DWidth_MASK 0xE00000
#define DMACC3Control_DWidth_BIT 21
#define DMACC3Control_SWidth_MASK 0x1C0000
#define DMACC3Control_SWidth_BIT 18
#define DMACC3Control_DBSize_MASK 0x38000
#define DMACC3Control_DBSize_BIT 15
#define DMACC3Control_SBSize_MASK 0x7000
#define DMACC3Control_SBSize_BIT 12
#define DMACC3Control_TransferSize_MASK 0xFFF
#define DMACC3Control_TransferSize_BIT 0

#define DMACC3Config (*(volatile unsigned *)0x31000170)
#define DMACC3Config_H_MASK 0x40000
#define DMACC3Config_H 0x40000
#define DMACC3Config_H_BIT 18
#define DMACC3Config_A_MASK 0x20000
#define DMACC3Config_A 0x20000
#define DMACC3Config_A_BIT 17
#define DMACC3Config_L_MASK 0x10000
#define DMACC3Config_L 0x10000
#define DMACC3Config_L_BIT 16
#define DMACC3Config_ITC_MASK 0x8000
#define DMACC3Config_ITC 0x8000
#define DMACC3Config_ITC_BIT 15
#define DMACC3Config_IE_MASK 0x4000
#define DMACC3Config_IE 0x4000
#define DMACC3Config_IE_BIT 14
#define DMACC3Config_FlowCntrl_MASK 0x3800
#define DMACC3Config_FlowCntrl_BIT 11
#define DMACC3Config_DestPeripheral_MASK 0x7C0
#define DMACC3Config_DestPeripheral_BIT 6
#define DMACC3Config_SrcPeripheral_MASK 0x3E
#define DMACC3Config_SrcPeripheral_BIT 1
#define DMACC3Config_E_MASK 0x1
#define DMACC3Config_E 0x1
#define DMACC3Config_E_BIT 0

#define DMACC4SrcAddr (*(volatile unsigned *)0x31000180)

#define DMACC4DestAddr (*(volatile unsigned *)0x31000184)

#define DMACC4LLI (*(volatile unsigned *)0x31000188)
#define DMACC4LLI_LLI_MASK 0xFFFFFFFC
#define DMACC4LLI_LLI_BIT 2
#define DMACC4LLI_LM_MASK 0x1
#define DMACC4LLI_LM 0x1
#define DMACC4LLI_LM_BIT 0

#define DMACC4Control (*(volatile unsigned *)0x3100018C)
#define DMACC4Control_I_MASK 0x80000000
#define DMACC4Control_I 0x80000000
#define DMACC4Control_I_BIT 31
#define DMACC4Control_Prot3_MASK 0x40000000
#define DMACC4Control_Prot3 0x40000000
#define DMACC4Control_Prot3_BIT 30
#define DMACC4Control_Prot2_MASK 0x20000000
#define DMACC4Control_Prot2 0x20000000
#define DMACC4Control_Prot2_BIT 29
#define DMACC4Control_Prot1_MASK 0x10000000
#define DMACC4Control_Prot1 0x10000000
#define DMACC4Control_Prot1_BIT 28
#define DMACC4Control_DI_MASK 0x8000000
#define DMACC4Control_DI 0x8000000
#define DMACC4Control_DI_BIT 27
#define DMACC4Control_SI_MASK 0x4000000
#define DMACC4Control_SI 0x4000000
#define DMACC4Control_SI_BIT 26
#define DMACC4Control_D_MASK 0x2000000
#define DMACC4Control_D 0x2000000
#define DMACC4Control_D_BIT 25
#define DMACC4Control_S_MASK 0x1000000
#define DMACC4Control_S 0x1000000
#define DMACC4Control_S_BIT 24
#define DMACC4Control_DWidth_MASK 0xE00000
#define DMACC4Control_DWidth_BIT 21
#define DMACC4Control_SWidth_MASK 0x1C0000
#define DMACC4Control_SWidth_BIT 18
#define DMACC4Control_DBSize_MASK 0x38000
#define DMACC4Control_DBSize_BIT 15
#define DMACC4Control_SBSize_MASK 0x7000
#define DMACC4Control_SBSize_BIT 12
#define DMACC4Control_TransferSize_MASK 0xFFF
#define DMACC4Control_TransferSize_BIT 0

#define DMACC4Config (*(volatile unsigned *)0x31000190)
#define DMACC4Config_H_MASK 0x40000
#define DMACC4Config_H 0x40000
#define DMACC4Config_H_BIT 18
#define DMACC4Config_A_MASK 0x20000
#define DMACC4Config_A 0x20000
#define DMACC4Config_A_BIT 17
#define DMACC4Config_L_MASK 0x10000
#define DMACC4Config_L 0x10000
#define DMACC4Config_L_BIT 16
#define DMACC4Config_ITC_MASK 0x8000
#define DMACC4Config_ITC 0x8000
#define DMACC4Config_ITC_BIT 15
#define DMACC4Config_IE_MASK 0x4000
#define DMACC4Config_IE 0x4000
#define DMACC4Config_IE_BIT 14
#define DMACC4Config_FlowCntrl_MASK 0x3800
#define DMACC4Config_FlowCntrl_BIT 11
#define DMACC4Config_DestPeripheral_MASK 0x7C0
#define DMACC4Config_DestPeripheral_BIT 6
#define DMACC4Config_SrcPeripheral_MASK 0x3E
#define DMACC4Config_SrcPeripheral_BIT 1
#define DMACC4Config_E_MASK 0x1
#define DMACC4Config_E 0x1
#define DMACC4Config_E_BIT 0

#define DMACC5SrcAddr (*(volatile unsigned *)0x310001A0)

#define DMACC5DestAddr (*(volatile unsigned *)0x310001A4)

#define DMACC5LLI (*(volatile unsigned *)0x310001A8)
#define DMACC5LLI_LLI_MASK 0xFFFFFFFC
#define DMACC5LLI_LLI_BIT 2
#define DMACC5LLI_LM_MASK 0x1
#define DMACC5LLI_LM 0x1
#define DMACC5LLI_LM_BIT 0

#define DMACC5Control (*(volatile unsigned *)0x310001AC)
#define DMACC5Control_I_MASK 0x80000000
#define DMACC5Control_I 0x80000000
#define DMACC5Control_I_BIT 31
#define DMACC5Control_Prot3_MASK 0x40000000
#define DMACC5Control_Prot3 0x40000000
#define DMACC5Control_Prot3_BIT 30
#define DMACC5Control_Prot2_MASK 0x20000000
#define DMACC5Control_Prot2 0x20000000
#define DMACC5Control_Prot2_BIT 29
#define DMACC5Control_Prot1_MASK 0x10000000
#define DMACC5Control_Prot1 0x10000000
#define DMACC5Control_Prot1_BIT 28
#define DMACC5Control_DI_MASK 0x8000000
#define DMACC5Control_DI 0x8000000
#define DMACC5Control_DI_BIT 27
#define DMACC5Control_SI_MASK 0x4000000
#define DMACC5Control_SI 0x4000000
#define DMACC5Control_SI_BIT 26
#define DMACC5Control_D_MASK 0x2000000
#define DMACC5Control_D 0x2000000
#define DMACC5Control_D_BIT 25
#define DMACC5Control_S_MASK 0x1000000
#define DMACC5Control_S 0x1000000
#define DMACC5Control_S_BIT 24
#define DMACC5Control_DWidth_MASK 0xE00000
#define DMACC5Control_DWidth_BIT 21
#define DMACC5Control_SWidth_MASK 0x1C0000
#define DMACC5Control_SWidth_BIT 18
#define DMACC5Control_DBSize_MASK 0x38000
#define DMACC5Control_DBSize_BIT 15
#define DMACC5Control_SBSize_MASK 0x7000
#define DMACC5Control_SBSize_BIT 12
#define DMACC5Control_TransferSize_MASK 0xFFF
#define DMACC5Control_TransferSize_BIT 0

#define DMACC5Config (*(volatile unsigned *)0x310001B0)
#define DMACC5Config_H_MASK 0x40000
#define DMACC5Config_H 0x40000
#define DMACC5Config_H_BIT 18
#define DMACC5Config_A_MASK 0x20000
#define DMACC5Config_A 0x20000
#define DMACC5Config_A_BIT 17
#define DMACC5Config_L_MASK 0x10000
#define DMACC5Config_L 0x10000
#define DMACC5Config_L_BIT 16
#define DMACC5Config_ITC_MASK 0x8000
#define DMACC5Config_ITC 0x8000
#define DMACC5Config_ITC_BIT 15
#define DMACC5Config_IE_MASK 0x4000
#define DMACC5Config_IE 0x4000
#define DMACC5Config_IE_BIT 14
#define DMACC5Config_FlowCntrl_MASK 0x3800
#define DMACC5Config_FlowCntrl_BIT 11
#define DMACC5Config_DestPeripheral_MASK 0x7C0
#define DMACC5Config_DestPeripheral_BIT 6
#define DMACC5Config_SrcPeripheral_MASK 0x3E
#define DMACC5Config_SrcPeripheral_BIT 1
#define DMACC5Config_E_MASK 0x1
#define DMACC5Config_E 0x1
#define DMACC5Config_E_BIT 0

#define DMACC6SrcAddr (*(volatile unsigned *)0x310001C0)

#define DMACC6DestAddr (*(volatile unsigned *)0x310001C4)

#define DMACC6LLI (*(volatile unsigned *)0x310001C8)
#define DMACC6LLI_LLI_MASK 0xFFFFFFFC
#define DMACC6LLI_LLI_BIT 2
#define DMACC6LLI_LM_MASK 0x1
#define DMACC6LLI_LM 0x1
#define DMACC6LLI_LM_BIT 0

#define DMACC6Control (*(volatile unsigned *)0x310001CC)
#define DMACC6Control_I_MASK 0x80000000
#define DMACC6Control_I 0x80000000
#define DMACC6Control_I_BIT 31
#define DMACC6Control_Prot3_MASK 0x40000000
#define DMACC6Control_Prot3 0x40000000
#define DMACC6Control_Prot3_BIT 30
#define DMACC6Control_Prot2_MASK 0x20000000
#define DMACC6Control_Prot2 0x20000000
#define DMACC6Control_Prot2_BIT 29
#define DMACC6Control_Prot1_MASK 0x10000000
#define DMACC6Control_Prot1 0x10000000
#define DMACC6Control_Prot1_BIT 28
#define DMACC6Control_DI_MASK 0x8000000
#define DMACC6Control_DI 0x8000000
#define DMACC6Control_DI_BIT 27
#define DMACC6Control_SI_MASK 0x4000000
#define DMACC6Control_SI 0x4000000
#define DMACC6Control_SI_BIT 26
#define DMACC6Control_D_MASK 0x2000000
#define DMACC6Control_D 0x2000000
#define DMACC6Control_D_BIT 25
#define DMACC6Control_S_MASK 0x1000000
#define DMACC6Control_S 0x1000000
#define DMACC6Control_S_BIT 24
#define DMACC6Control_DWidth_MASK 0xE00000
#define DMACC6Control_DWidth_BIT 21
#define DMACC6Control_SWidth_MASK 0x1C0000
#define DMACC6Control_SWidth_BIT 18
#define DMACC6Control_DBSize_MASK 0x38000
#define DMACC6Control_DBSize_BIT 15
#define DMACC6Control_SBSize_MASK 0x7000
#define DMACC6Control_SBSize_BIT 12
#define DMACC6Control_TransferSize_MASK 0xFFF
#define DMACC6Control_TransferSize_BIT 0

#define DMACC6Config (*(volatile unsigned *)0x310001D0)
#define DMACC6Config_H_MASK 0x40000
#define DMACC6Config_H 0x40000
#define DMACC6Config_H_BIT 18
#define DMACC6Config_A_MASK 0x20000
#define DMACC6Config_A 0x20000
#define DMACC6Config_A_BIT 17
#define DMACC6Config_L_MASK 0x10000
#define DMACC6Config_L 0x10000
#define DMACC6Config_L_BIT 16
#define DMACC6Config_ITC_MASK 0x8000
#define DMACC6Config_ITC 0x8000
#define DMACC6Config_ITC_BIT 15
#define DMACC6Config_IE_MASK 0x4000
#define DMACC6Config_IE 0x4000
#define DMACC6Config_IE_BIT 14
#define DMACC6Config_FlowCntrl_MASK 0x3800
#define DMACC6Config_FlowCntrl_BIT 11
#define DMACC6Config_DestPeripheral_MASK 0x7C0
#define DMACC6Config_DestPeripheral_BIT 6
#define DMACC6Config_SrcPeripheral_MASK 0x3E
#define DMACC6Config_SrcPeripheral_BIT 1
#define DMACC6Config_E_MASK 0x1
#define DMACC6Config_E 0x1
#define DMACC6Config_E_BIT 0

#define DMACC7SrcAddr (*(volatile unsigned *)0x310001E0)

#define DMACC7DestAddr (*(volatile unsigned *)0x310001E4)

#define DMACC7LLI (*(volatile unsigned *)0x310001E8)
#define DMACC7LLI_LLI_MASK 0xFFFFFFFC
#define DMACC7LLI_LLI_BIT 2
#define DMACC7LLI_LM_MASK 0x1
#define DMACC7LLI_LM 0x1
#define DMACC7LLI_LM_BIT 0

#define DMACC7Control (*(volatile unsigned *)0x310001EC)
#define DMACC7Control_I_MASK 0x80000000
#define DMACC7Control_I 0x80000000
#define DMACC7Control_I_BIT 31
#define DMACC7Control_Prot3_MASK 0x40000000
#define DMACC7Control_Prot3 0x40000000
#define DMACC7Control_Prot3_BIT 30
#define DMACC7Control_Prot2_MASK 0x20000000
#define DMACC7Control_Prot2 0x20000000
#define DMACC7Control_Prot2_BIT 29
#define DMACC7Control_Prot1_MASK 0x10000000
#define DMACC7Control_Prot1 0x10000000
#define DMACC7Control_Prot1_BIT 28
#define DMACC7Control_DI_MASK 0x8000000
#define DMACC7Control_DI 0x8000000
#define DMACC7Control_DI_BIT 27
#define DMACC7Control_SI_MASK 0x4000000
#define DMACC7Control_SI 0x4000000
#define DMACC7Control_SI_BIT 26
#define DMACC7Control_D_MASK 0x2000000
#define DMACC7Control_D 0x2000000
#define DMACC7Control_D_BIT 25
#define DMACC7Control_S_MASK 0x1000000
#define DMACC7Control_S 0x1000000
#define DMACC7Control_S_BIT 24
#define DMACC7Control_DWidth_MASK 0xE00000
#define DMACC7Control_DWidth_BIT 21
#define DMACC7Control_SWidth_MASK 0x1C0000
#define DMACC7Control_SWidth_BIT 18
#define DMACC7Control_DBSize_MASK 0x38000
#define DMACC7Control_DBSize_BIT 15
#define DMACC7Control_SBSize_MASK 0x7000
#define DMACC7Control_SBSize_BIT 12
#define DMACC7Control_TransferSize_MASK 0xFFF
#define DMACC7Control_TransferSize_BIT 0

#define DMACC7Config (*(volatile unsigned *)0x310001F0)
#define DMACC7Config_H_MASK 0x40000
#define DMACC7Config_H 0x40000
#define DMACC7Config_H_BIT 18
#define DMACC7Config_A_MASK 0x20000
#define DMACC7Config_A 0x20000
#define DMACC7Config_A_BIT 17
#define DMACC7Config_L_MASK 0x10000
#define DMACC7Config_L 0x10000
#define DMACC7Config_L_BIT 16
#define DMACC7Config_ITC_MASK 0x8000
#define DMACC7Config_ITC 0x8000
#define DMACC7Config_ITC_BIT 15
#define DMACC7Config_IE_MASK 0x4000
#define DMACC7Config_IE 0x4000
#define DMACC7Config_IE_BIT 14
#define DMACC7Config_FlowCntrl_MASK 0x3800
#define DMACC7Config_FlowCntrl_BIT 11
#define DMACC7Config_DestPeripheral_MASK 0x7C0
#define DMACC7Config_DestPeripheral_BIT 6
#define DMACC7Config_SrcPeripheral_MASK 0x3E
#define DMACC7Config_SrcPeripheral_BIT 1
#define DMACC7Config_E_MASK 0x1
#define DMACC7Config_E 0x1
#define DMACC7Config_E_BIT 0

#define HcRevision (*(volatile unsigned *)0x31020000)
#define HcRevision_REV_MASK 0xFF
#define HcRevision_REV_BIT 0

#define HcControl (*(volatile unsigned *)0x31020004)
#define HcControl_CBSR_MASK 0x3
#define HcControl_CBSR_BIT 0
#define HcControl_PLE_MASK 0x4
#define HcControl_PLE 0x4
#define HcControl_PLE_BIT 2
#define HcControl_IE_MASK 0x8
#define HcControl_IE 0x8
#define HcControl_IE_BIT 3
#define HcControl_CLE_MASK 0x10
#define HcControl_CLE 0x10
#define HcControl_CLE_BIT 4
#define HcControl_BLE_MASK 0x20
#define HcControl_BLE 0x20
#define HcControl_BLE_BIT 5
#define HcControl_HCFS_MASK 0xC0
#define HcControl_HCFS_BIT 6
#define HcControl_IR_MASK 0x100
#define HcControl_IR 0x100
#define HcControl_IR_BIT 8
#define HcControl_RWC_MASK 0x200
#define HcControl_RWC 0x200
#define HcControl_RWC_BIT 9
#define HcControl_RWE_MASK 0x400
#define HcControl_RWE 0x400
#define HcControl_RWE_BIT 10

#define HcCommandStatus (*(volatile unsigned *)0x31020008)
#define HcCommandStatus_HCR_MASK 0x1
#define HcCommandStatus_HCR 0x1
#define HcCommandStatus_HCR_BIT 0
#define HcCommandStatus_CLF_MASK 0x2
#define HcCommandStatus_CLF 0x2
#define HcCommandStatus_CLF_BIT 1
#define HcCommandStatus_BLF_MASK 0x4
#define HcCommandStatus_BLF 0x4
#define HcCommandStatus_BLF_BIT 2
#define HcCommandStatus_OCR_MASK 0x8
#define HcCommandStatus_OCR 0x8
#define HcCommandStatus_OCR_BIT 3
#define HcCommandStatus_SOC_MASK 0xC0
#define HcCommandStatus_SOC_BIT 6

#define HcInterruptStatus (*(volatile unsigned *)0x3102000C)
#define HcInterruptStatus_SO_MASK 0x1
#define HcInterruptStatus_SO 0x1
#define HcInterruptStatus_SO_BIT 0
#define HcInterruptStatus_WDH_MASK 0x2
#define HcInterruptStatus_WDH 0x2
#define HcInterruptStatus_WDH_BIT 1
#define HcInterruptStatus_SF_MASK 0x4
#define HcInterruptStatus_SF 0x4
#define HcInterruptStatus_SF_BIT 2
#define HcInterruptStatus_RD_MASK 0x8
#define HcInterruptStatus_RD 0x8
#define HcInterruptStatus_RD_BIT 3
#define HcInterruptStatus_UE_MASK 0x10
#define HcInterruptStatus_UE 0x10
#define HcInterruptStatus_UE_BIT 4
#define HcInterruptStatus_FNO_MASK 0x20
#define HcInterruptStatus_FNO 0x20
#define HcInterruptStatus_FNO_BIT 5
#define HcInterruptStatus_RHSC_MASK 0x40
#define HcInterruptStatus_RHSC 0x40
#define HcInterruptStatus_RHSC_BIT 6
#define HcInterruptStatus_OC_MASK 0x40000000
#define HcInterruptStatus_OC 0x40000000
#define HcInterruptStatus_OC_BIT 30

#define HcInterruptEnable (*(volatile unsigned *)0x31020010)
#define HcInterruptEnable_SO_MASK 0x1
#define HcInterruptEnable_SO 0x1
#define HcInterruptEnable_SO_BIT 0
#define HcInterruptEnable_WDH_MASK 0x2
#define HcInterruptEnable_WDH 0x2
#define HcInterruptEnable_WDH_BIT 1
#define HcInterruptEnable_SF_MASK 0x4
#define HcInterruptEnable_SF 0x4
#define HcInterruptEnable_SF_BIT 2
#define HcInterruptEnable_RD_MASK 0x8
#define HcInterruptEnable_RD 0x8
#define HcInterruptEnable_RD_BIT 3
#define HcInterruptEnable_UE_MASK 0x10
#define HcInterruptEnable_UE 0x10
#define HcInterruptEnable_UE_BIT 4
#define HcInterruptEnable_FNO_MASK 0x20
#define HcInterruptEnable_FNO 0x20
#define HcInterruptEnable_FNO_BIT 5
#define HcInterruptEnable_RHSC_MASK 0x40
#define HcInterruptEnable_RHSC 0x40
#define HcInterruptEnable_RHSC_BIT 6
#define HcInterruptEnable_OC_MASK 0x40000000
#define HcInterruptEnable_OC 0x40000000
#define HcInterruptEnable_OC_BIT 30
#define HcInterruptEnable_MIE_MASK 0x80000000
#define HcInterruptEnable_MIE 0x80000000
#define HcInterruptEnable_MIE_BIT 31

#define HcInterruptDisable (*(volatile unsigned *)0x31020014)
#define HcInterruptDisable_SO_MASK 0x1
#define HcInterruptDisable_SO 0x1
#define HcInterruptDisable_SO_BIT 0
#define HcInterruptDisable_WDH_MASK 0x2
#define HcInterruptDisable_WDH 0x2
#define HcInterruptDisable_WDH_BIT 1
#define HcInterruptDisable_SF_MASK 0x4
#define HcInterruptDisable_SF 0x4
#define HcInterruptDisable_SF_BIT 2
#define HcInterruptDisable_RD_MASK 0x8
#define HcInterruptDisable_RD 0x8
#define HcInterruptDisable_RD_BIT 3
#define HcInterruptDisable_UE_MASK 0x10
#define HcInterruptDisable_UE 0x10
#define HcInterruptDisable_UE_BIT 4
#define HcInterruptDisable_FNO_MASK 0x20
#define HcInterruptDisable_FNO 0x20
#define HcInterruptDisable_FNO_BIT 5
#define HcInterruptDisable_RHSC_MASK 0x40
#define HcInterruptDisable_RHSC 0x40
#define HcInterruptDisable_RHSC_BIT 6
#define HcInterruptDisable_OC_MASK 0x40000000
#define HcInterruptDisable_OC 0x40000000
#define HcInterruptDisable_OC_BIT 30
#define HcInterruptDisable_MIE_MASK 0x80000000
#define HcInterruptDisable_MIE 0x80000000
#define HcInterruptDisable_MIE_BIT 31

#define HcHCCA (*(volatile unsigned *)0x31020018)
#define HcHCCA_HCCA_MASK 0xFFFFFF00
#define HcHCCA_HCCA_BIT 8

#define HcPeriodCurrentED (*(volatile unsigned *)0x3102001C)
#define HcPeriodCurrentED_PCED_MASK 0xFFFFFFF0
#define HcPeriodCurrentED_PCED_BIT 4

#define HcControlHeadED (*(volatile unsigned *)0x31020020)
#define HcControlHeadED_CHED_MASK 0xFFFFFFF0
#define HcControlHeadED_CHED_BIT 4

#define HcControlCurrentED (*(volatile unsigned *)0x31020024)
#define HcControlCurrentED_CCED_MASK 0xFFFFFFF0
#define HcControlCurrentED_CCED_BIT 4

#define HcBulkHeadED (*(volatile unsigned *)0x31020028)
#define HcBulkHeadED_BHED_MASK 0xFFFFFFF0
#define HcBulkHeadED_BHED_BIT 4

#define HcBulkCurrentED (*(volatile unsigned *)0x3102002C)
#define HcBulkCurrentED_BCED_MASK 0xFFFFFFF0
#define HcBulkCurrentED_BCED_BIT 4

#define HcDoneHead (*(volatile unsigned *)0x31020030)
#define HcDoneHead_DH_MASK 0xFFFFFFF0
#define HcDoneHead_DH_BIT 4

#define HcFmInterval (*(volatile unsigned *)0x31020034)
#define HcFmInterval_FI_MASK 0x3FFF
#define HcFmInterval_FI_BIT 0
#define HcFmInterval_FSMPS_MASK 0x7FFF0000
#define HcFmInterval_FSMPS_BIT 16
#define HcFmInterval_FIT_MASK 0x80000000
#define HcFmInterval_FIT 0x80000000
#define HcFmInterval_FIT_BIT 31

#define HcFmRemaining (*(volatile unsigned *)0x31020038)
#define HcFmRemaining_FR_MASK 0x3FFF
#define HcFmRemaining_FR_BIT 0
#define HcFmRemaining_FRT_MASK 0x80000000
#define HcFmRemaining_FRT 0x80000000
#define HcFmRemaining_FRT_BIT 31

#define HcFmNumber (*(volatile unsigned *)0x3102003C)
#define HcFmNumber_FN_MASK 0xFFFF
#define HcFmNumber_FN_BIT 0

#define HcPeriodicStart (*(volatile unsigned *)0x31020040)
#define HcPeriodicStart_PS_MASK 0x3FFF
#define HcPeriodicStart_PS_BIT 0

#define HcLSThreshold (*(volatile unsigned *)0x31020044)
#define HcLSThreshold_LST_MASK 0xFFF
#define HcLSThreshold_LST_BIT 0

#define HcRhDescriptorA (*(volatile unsigned *)0x31020048)
#define HcRhDescriptorA_NDP_MASK 0xFF
#define HcRhDescriptorA_NDP_BIT 0
#define HcRhDescriptorA_PSM_MASK 0x100
#define HcRhDescriptorA_PSM 0x100
#define HcRhDescriptorA_PSM_BIT 8
#define HcRhDescriptorA_NPS_MASK 0x200
#define HcRhDescriptorA_NPS 0x200
#define HcRhDescriptorA_NPS_BIT 9
#define HcRhDescriptorA_DT_MASK 0x400
#define HcRhDescriptorA_DT 0x400
#define HcRhDescriptorA_DT_BIT 10
#define HcRhDescriptorA_OCPM_MASK 0x800
#define HcRhDescriptorA_OCPM 0x800
#define HcRhDescriptorA_OCPM_BIT 11
#define HcRhDescriptorA_NOCP_MASK 0x1000
#define HcRhDescriptorA_NOCP 0x1000
#define HcRhDescriptorA_NOCP_BIT 12
#define HcRhDescriptorA_POTPGT_MASK 0xFF000000
#define HcRhDescriptorA_POTPGT_BIT 24

#define HcRhDescriptorB (*(volatile unsigned *)0x3102004C)
#define HcRhDescriptorB_DR_MASK 0xFFFF
#define HcRhDescriptorB_DR_BIT 0
#define HcRhDescriptorB_PPCM_MASK 0xFFFF0000
#define HcRhDescriptorB_PPCM_BIT 16

#define HcRhStatus (*(volatile unsigned *)0x31020050)
#define HcRhStatus_LPS_MASK 0x1
#define HcRhStatus_LPS 0x1
#define HcRhStatus_LPS_BIT 0
#define HcRhStatus_OCI_MASK 0x2
#define HcRhStatus_OCI 0x2
#define HcRhStatus_OCI_BIT 1
#define HcRhStatus_DRWE_MASK 0x8000
#define HcRhStatus_DRWE 0x8000
#define HcRhStatus_DRWE_BIT 15
#define HcRhStatus_LPSC_MASK 0x10000
#define HcRhStatus_LPSC 0x10000
#define HcRhStatus_LPSC_BIT 16
#define HcRhStatus_OCIC_MASK 0x20000
#define HcRhStatus_OCIC 0x20000
#define HcRhStatus_OCIC_BIT 17
#define HcRhStatus_CRWE_MASK 0x80000000
#define HcRhStatus_CRWE 0x80000000
#define HcRhStatus_CRWE_BIT 31

#define HcRhPortStatus1 (*(volatile unsigned *)0x31020054)
#define HcRhPortStatus1_CCS_MASK 0x1
#define HcRhPortStatus1_CCS 0x1
#define HcRhPortStatus1_CCS_BIT 0
#define HcRhPortStatus1_PES_MASK 0x2
#define HcRhPortStatus1_PES 0x2
#define HcRhPortStatus1_PES_BIT 1
#define HcRhPortStatus1_PSS_MASK 0x4
#define HcRhPortStatus1_PSS 0x4
#define HcRhPortStatus1_PSS_BIT 2
#define HcRhPortStatus1_POCI_MASK 0x8
#define HcRhPortStatus1_POCI 0x8
#define HcRhPortStatus1_POCI_BIT 3
#define HcRhPortStatus1_PRS_MASK 0x10
#define HcRhPortStatus1_PRS 0x10
#define HcRhPortStatus1_PRS_BIT 4
#define HcRhPortStatus1_PPS_MASK 0x100
#define HcRhPortStatus1_PPS 0x100
#define HcRhPortStatus1_PPS_BIT 8
#define HcRhPortStatus1_LSDA_MASK 0x200
#define HcRhPortStatus1_LSDA 0x200
#define HcRhPortStatus1_LSDA_BIT 9
#define HcRhPortStatus1_CSC_MASK 0x10000
#define HcRhPortStatus1_CSC 0x10000
#define HcRhPortStatus1_CSC_BIT 16
#define HcRhPortStatus1_PESC_MASK 0x20000
#define HcRhPortStatus1_PESC 0x20000
#define HcRhPortStatus1_PESC_BIT 17
#define HcRhPortStatus1_PSSC_MASK 0x40000
#define HcRhPortStatus1_PSSC 0x40000
#define HcRhPortStatus1_PSSC_BIT 18
#define HcRhPortStatus1_OCIC_MASK 0x80000
#define HcRhPortStatus1_OCIC 0x80000
#define HcRhPortStatus1_OCIC_BIT 19
#define HcRhPortStatus1_PRSC_MASK 0x100000
#define HcRhPortStatus1_PRSC 0x100000
#define HcRhPortStatus1_PRSC_BIT 20

#define HcRhPortStatus2 (*(volatile unsigned *)0x31020058)
#define HcRhPortStatus2_CCS_MASK 0x1
#define HcRhPortStatus2_CCS 0x1
#define HcRhPortStatus2_CCS_BIT 0
#define HcRhPortStatus2_PES_MASK 0x2
#define HcRhPortStatus2_PES 0x2
#define HcRhPortStatus2_PES_BIT 1
#define HcRhPortStatus2_PSS_MASK 0x4
#define HcRhPortStatus2_PSS 0x4
#define HcRhPortStatus2_PSS_BIT 2
#define HcRhPortStatus2_POCI_MASK 0x8
#define HcRhPortStatus2_POCI 0x8
#define HcRhPortStatus2_POCI_BIT 3
#define HcRhPortStatus2_PRS_MASK 0x10
#define HcRhPortStatus2_PRS 0x10
#define HcRhPortStatus2_PRS_BIT 4
#define HcRhPortStatus2_PPS_MASK 0x100
#define HcRhPortStatus2_PPS 0x100
#define HcRhPortStatus2_PPS_BIT 8
#define HcRhPortStatus2_LSDA_MASK 0x200
#define HcRhPortStatus2_LSDA 0x200
#define HcRhPortStatus2_LSDA_BIT 9
#define HcRhPortStatus2_CSC_MASK 0x10000
#define HcRhPortStatus2_CSC 0x10000
#define HcRhPortStatus2_CSC_BIT 16
#define HcRhPortStatus2_PESC_MASK 0x20000
#define HcRhPortStatus2_PESC 0x20000
#define HcRhPortStatus2_PESC_BIT 17
#define HcRhPortStatus2_PSSC_MASK 0x40000
#define HcRhPortStatus2_PSSC 0x40000
#define HcRhPortStatus2_PSSC_BIT 18
#define HcRhPortStatus2_OCIC_MASK 0x80000
#define HcRhPortStatus2_OCIC 0x80000
#define HcRhPortStatus2_OCIC_BIT 19
#define HcRhPortStatus2_PRSC_MASK 0x100000
#define HcRhPortStatus2_PRSC 0x100000
#define HcRhPortStatus2_PRSC_BIT 20

#define OTG_int_status (*(volatile unsigned *)0x31020100)
#define OTG_int_status_hnp_success_MASK 0x8
#define OTG_int_status_hnp_success 0x8
#define OTG_int_status_hnp_success_BIT 3
#define OTG_int_status_hnp_failure_MASK 0x4
#define OTG_int_status_hnp_failure 0x4
#define OTG_int_status_hnp_failure_BIT 2
#define OTG_int_status_remove_pullup_MASK 0x2
#define OTG_int_status_remove_pullup 0x2
#define OTG_int_status_remove_pullup_BIT 1
#define OTG_int_status_timer_interrupt_status_MASK 0x1
#define OTG_int_status_timer_interrupt_status 0x1
#define OTG_int_status_timer_interrupt_status_BIT 0

#define OTG_int_enable (*(volatile unsigned *)0x31020104)
#define OTG_int_enable_hnp_success_en_MASK 0x8
#define OTG_int_enable_hnp_success_en 0x8
#define OTG_int_enable_hnp_success_en_BIT 3
#define OTG_int_enable_hnp_failure_en_MASK 0x4
#define OTG_int_enable_hnp_failure_en 0x4
#define OTG_int_enable_hnp_failure_en_BIT 2
#define OTG_int_enable_remove_pullup_en_MASK 0x2
#define OTG_int_enable_remove_pullup_en 0x2
#define OTG_int_enable_remove_pullup_en_BIT 1
#define OTG_int_enable_timer_interrupt_en_MASK 0x1
#define OTG_int_enable_timer_interrupt_en 0x1
#define OTG_int_enable_timer_interrupt_en_BIT 0

#define OTG_int_set (*(volatile unsigned *)0x31020108)
#define OTG_int_set_hnp_success_set_MASK 0x8
#define OTG_int_set_hnp_success_set 0x8
#define OTG_int_set_hnp_success_set_BIT 3
#define OTG_int_set_hnp_failure_set_MASK 0x4
#define OTG_int_set_hnp_failure_set 0x4
#define OTG_int_set_hnp_failure_set_BIT 2
#define OTG_int_set_remove_pullup_set_MASK 0x2
#define OTG_int_set_remove_pullup_set 0x2
#define OTG_int_set_remove_pullup_set_BIT 1
#define OTG_int_set_timer_interrupt_set_MASK 0x1
#define OTG_int_set_timer_interrupt_set 0x1
#define OTG_int_set_timer_interrupt_set_BIT 0

#define OTG_int_clear (*(volatile unsigned *)0x3102010C)
#define OTG_int_clear_hnp_success_clear_MASK 0x8
#define OTG_int_clear_hnp_success_clear 0x8
#define OTG_int_clear_hnp_success_clear_BIT 3
#define OTG_int_clear_hnp_failure_clear_MASK 0x4
#define OTG_int_clear_hnp_failure_clear 0x4
#define OTG_int_clear_hnp_failure_clear_BIT 2
#define OTG_int_clear_remove_pullup_clear_MASK 0x2
#define OTG_int_clear_remove_pullup_clear 0x2
#define OTG_int_clear_remove_pullup_clear_BIT 1
#define OTG_int_clear_timer_interrupt_clear_MASK 0x1
#define OTG_int_clear_timer_interrupt_clear 0x1
#define OTG_int_clear_timer_interrupt_clear_BIT 0

#define OTG_status (*(volatile unsigned *)0x31020110)
#define OTG_status_Timer_count_status_MASK 0xFFFF0000
#define OTG_status_Timer_count_status_BIT 16
#define OTG_status_Pullup_removed_MASK 0x400
#define OTG_status_Pullup_removed 0x400
#define OTG_status_Pullup_removed_BIT 10
#define OTG_status_a_to_b_hnp_track_MASK 0x200
#define OTG_status_a_to_b_hnp_track 0x200
#define OTG_status_a_to_b_hnp_track_BIT 9
#define OTG_status_b_to_a_hnp_track_MASK 0x100
#define OTG_status_b_to_a_hnp_track 0x100
#define OTG_status_b_to_a_hnp_track_BIT 8
#define OTG_status_Transparent_I2C_en_MASK 0x80
#define OTG_status_Transparent_I2C_en 0x80
#define OTG_status_Transparent_I2C_en_BIT 7
#define OTG_status_Timer_reset_MASK 0x40
#define OTG_status_Timer_reset 0x40
#define OTG_status_Timer_reset_BIT 6
#define OTG_status_Timer_enable_MASK 0x20
#define OTG_status_Timer_enable 0x20
#define OTG_status_Timer_enable_BIT 5
#define OTG_status_Timer_mode_MASK 0x10
#define OTG_status_Timer_mode 0x10
#define OTG_status_Timer_mode_BIT 4
#define OTG_status_Timer_scale_MASK 0xC
#define OTG_status_Timer_scale_BIT 2
#define OTG_status_Host_En_MASK 0x1
#define OTG_status_Host_En 0x1
#define OTG_status_Host_En_BIT 0

#define OTG_timer (*(volatile unsigned *)0x31020114)
#define OTG_timer_Timer_value_MASK 0xFFFF
#define OTG_timer_Timer_value_BIT 0

#define OTG_clock_control (*(volatile unsigned *)0x31020FF4)
#define OTG_clock_control_AHB_CLK_ON_MASK 0x10
#define OTG_clock_control_AHB_CLK_ON 0x10
#define OTG_clock_control_AHB_CLK_ON_BIT 4
#define OTG_clock_control_OTG_CLK_ON_MASK 0x8
#define OTG_clock_control_OTG_CLK_ON 0x8
#define OTG_clock_control_OTG_CLK_ON_BIT 3
#define OTG_clock_control_I2C_CLK_ON_MASK 0x4
#define OTG_clock_control_I2C_CLK_ON 0x4
#define OTG_clock_control_I2C_CLK_ON_BIT 2
#define OTG_clock_control_DEV_CLK_ON_MASK 0x2
#define OTG_clock_control_DEV_CLK_ON 0x2
#define OTG_clock_control_DEV_CLK_ON_BIT 1
#define OTG_clock_control_HOST_CLK_ON_MASK 0x1
#define OTG_clock_control_HOST_CLK_ON 0x1
#define OTG_clock_control_HOST_CLK_ON_BIT 0

#define OTG_clock_status (*(volatile unsigned *)0x31020FF8)
#define OTG_clock_status_AHB_CLK_ON_MASK 0x10
#define OTG_clock_status_AHB_CLK_ON 0x10
#define OTG_clock_status_AHB_CLK_ON_BIT 4
#define OTG_clock_status_OTG_CLK_ON_MASK 0x8
#define OTG_clock_status_OTG_CLK_ON 0x8
#define OTG_clock_status_OTG_CLK_ON_BIT 3
#define OTG_clock_status_I2C_CLK_ON_MASK 0x4
#define OTG_clock_status_I2C_CLK_ON 0x4
#define OTG_clock_status_I2C_CLK_ON_BIT 2
#define OTG_clock_status_DEV_CLK_ON_MASK 0x2
#define OTG_clock_status_DEV_CLK_ON 0x2
#define OTG_clock_status_DEV_CLK_ON_BIT 1
#define OTG_clock_status_HOST_CLK_ON_MASK 0x1
#define OTG_clock_status_HOST_CLK_ON 0x1
#define OTG_clock_status_HOST_CLK_ON_BIT 0

#define USBDevIntSt (*(volatile unsigned *)0x31020200)
#define USBDevIntSt_ERR_INT_MASK 0x200
#define USBDevIntSt_ERR_INT 0x200
#define USBDevIntSt_ERR_INT_BIT 9
#define USBDevIntSt_EP_RLZED_MASK 0x100
#define USBDevIntSt_EP_RLZED 0x100
#define USBDevIntSt_EP_RLZED_BIT 8
#define USBDevIntSt_TxENDPKT_MASK 0x80
#define USBDevIntSt_TxENDPKT 0x80
#define USBDevIntSt_TxENDPKT_BIT 7
#define USBDevIntSt_RxENDPKT_MASK 0x40
#define USBDevIntSt_RxENDPKT 0x40
#define USBDevIntSt_RxENDPKT_BIT 6
#define USBDevIntSt_CDFULL_MASK 0x20
#define USBDevIntSt_CDFULL 0x20
#define USBDevIntSt_CDFULL_BIT 5
#define USBDevIntSt_CCEMPTY_MASK 0x10
#define USBDevIntSt_CCEMPTY 0x10
#define USBDevIntSt_CCEMPTY_BIT 4
#define USBDevIntSt_DEV_STAT_MASK 0xF8
#define USBDevIntSt_DEV_STAT_BIT 3
#define USBDevIntSt_EP_SLOW_MASK 0x4
#define USBDevIntSt_EP_SLOW 0x4
#define USBDevIntSt_EP_SLOW_BIT 2
#define USBDevIntSt_EP_FAST_MASK 0x2
#define USBDevIntSt_EP_FAST 0x2
#define USBDevIntSt_EP_FAST_BIT 1
#define USBDevIntSt_FRAME_MASK 0x1
#define USBDevIntSt_FRAME 0x1
#define USBDevIntSt_FRAME_BIT 0

#define USBDevIntEn (*(volatile unsigned *)0x31020204)
#define USBDevIntEn_ERR_INT_MASK 0x200
#define USBDevIntEn_ERR_INT 0x200
#define USBDevIntEn_ERR_INT_BIT 9
#define USBDevIntEn_EP_RLZED_MASK 0x100
#define USBDevIntEn_EP_RLZED 0x100
#define USBDevIntEn_EP_RLZED_BIT 8
#define USBDevIntEn_TxENDPKT_MASK 0x80
#define USBDevIntEn_TxENDPKT 0x80
#define USBDevIntEn_TxENDPKT_BIT 7
#define USBDevIntEn_RxENDPKT_MASK 0x40
#define USBDevIntEn_RxENDPKT 0x40
#define USBDevIntEn_RxENDPKT_BIT 6
#define USBDevIntEn_CDFULL_MASK 0x20
#define USBDevIntEn_CDFULL 0x20
#define USBDevIntEn_CDFULL_BIT 5
#define USBDevIntEn_CCEMPTY_MASK 0x10
#define USBDevIntEn_CCEMPTY 0x10
#define USBDevIntEn_CCEMPTY_BIT 4
#define USBDevIntEn_DEV_STAT_MASK 0xF8
#define USBDevIntEn_DEV_STAT_BIT 3
#define USBDevIntEn_EP_SLOW_MASK 0x4
#define USBDevIntEn_EP_SLOW 0x4
#define USBDevIntEn_EP_SLOW_BIT 2
#define USBDevIntEn_EP_FAST_MASK 0x2
#define USBDevIntEn_EP_FAST 0x2
#define USBDevIntEn_EP_FAST_BIT 1
#define USBDevIntEn_FRAME_MASK 0x1
#define USBDevIntEn_FRAME 0x1
#define USBDevIntEn_FRAME_BIT 0

#define USBDevIntClr (*(volatile unsigned *)0x31020208)
#define USBDevIntClr_ERR_INT_MASK 0x200
#define USBDevIntClr_ERR_INT 0x200
#define USBDevIntClr_ERR_INT_BIT 9
#define USBDevIntClr_EP_RLZED_MASK 0x100
#define USBDevIntClr_EP_RLZED 0x100
#define USBDevIntClr_EP_RLZED_BIT 8
#define USBDevIntClr_TxENDPKT_MASK 0x80
#define USBDevIntClr_TxENDPKT 0x80
#define USBDevIntClr_TxENDPKT_BIT 7
#define USBDevIntClr_RxENDPKT_MASK 0x40
#define USBDevIntClr_RxENDPKT 0x40
#define USBDevIntClr_RxENDPKT_BIT 6
#define USBDevIntClr_CDFULL_MASK 0x20
#define USBDevIntClr_CDFULL 0x20
#define USBDevIntClr_CDFULL_BIT 5
#define USBDevIntClr_CCEMPTY_MASK 0x10
#define USBDevIntClr_CCEMPTY 0x10
#define USBDevIntClr_CCEMPTY_BIT 4
#define USBDevIntClr_DEV_STAT_MASK 0xF8
#define USBDevIntClr_DEV_STAT_BIT 3
#define USBDevIntClr_EP_SLOW_MASK 0x4
#define USBDevIntClr_EP_SLOW 0x4
#define USBDevIntClr_EP_SLOW_BIT 2
#define USBDevIntClr_EP_FAST_MASK 0x2
#define USBDevIntClr_EP_FAST 0x2
#define USBDevIntClr_EP_FAST_BIT 1
#define USBDevIntClr_FRAME_MASK 0x1
#define USBDevIntClr_FRAME 0x1
#define USBDevIntClr_FRAME_BIT 0

#define USBDevIntSet (*(volatile unsigned *)0x3102020C)
#define USBDevIntSet_ERR_INT_MASK 0x200
#define USBDevIntSet_ERR_INT 0x200
#define USBDevIntSet_ERR_INT_BIT 9
#define USBDevIntSet_EP_RLZED_MASK 0x100
#define USBDevIntSet_EP_RLZED 0x100
#define USBDevIntSet_EP_RLZED_BIT 8
#define USBDevIntSet_TxENDPKT_MASK 0x80
#define USBDevIntSet_TxENDPKT 0x80
#define USBDevIntSet_TxENDPKT_BIT 7
#define USBDevIntSet_RxENDPKT_MASK 0x40
#define USBDevIntSet_RxENDPKT 0x40
#define USBDevIntSet_RxENDPKT_BIT 6
#define USBDevIntSet_CDFULL_MASK 0x20
#define USBDevIntSet_CDFULL 0x20
#define USBDevIntSet_CDFULL_BIT 5
#define USBDevIntSet_CCEMPTY_MASK 0x10
#define USBDevIntSet_CCEMPTY 0x10
#define USBDevIntSet_CCEMPTY_BIT 4
#define USBDevIntSet_DEV_STAT_MASK 0xF8
#define USBDevIntSet_DEV_STAT_BIT 3
#define USBDevIntSet_EP_SLOW_MASK 0x4
#define USBDevIntSet_EP_SLOW 0x4
#define USBDevIntSet_EP_SLOW_BIT 2
#define USBDevIntSet_EP_FAST_MASK 0x2
#define USBDevIntSet_EP_FAST 0x2
#define USBDevIntSet_EP_FAST_BIT 1
#define USBDevIntSet_FRAME_MASK 0x1
#define USBDevIntSet_FRAME 0x1
#define USBDevIntSet_FRAME_BIT 0

#define USBDevIntPri (*(volatile unsigned *)0x3102022C)
#define USBDevIntPri_EP_FAST_MASK 0x2
#define USBDevIntPri_EP_FAST 0x2
#define USBDevIntPri_EP_FAST_BIT 1
#define USBDevIntPri_FRAME_MASK 0x1
#define USBDevIntPri_FRAME 0x1
#define USBDevIntPri_FRAME_BIT 0

#define USBEpIntSt (*(volatile unsigned *)0x3102023C)
#define USBEpIntSt_EP_15TX_MASK 0x80000000
#define USBEpIntSt_EP_15TX 0x80000000
#define USBEpIntSt_EP_15TX_BIT 31
#define USBEpIntSt_EP_15RX_MASK 0x40000000
#define USBEpIntSt_EP_15RX 0x40000000
#define USBEpIntSt_EP_15RX_BIT 30
#define USBEpIntSt_EP_14TX_MASK 0x20000000
#define USBEpIntSt_EP_14TX 0x20000000
#define USBEpIntSt_EP_14TX_BIT 29
#define USBEpIntSt_EP_14RX_MASK 0x10000000
#define USBEpIntSt_EP_14RX 0x10000000
#define USBEpIntSt_EP_14RX_BIT 28
#define USBEpIntSt_EP_13TX_MASK 0x8000000
#define USBEpIntSt_EP_13TX 0x8000000
#define USBEpIntSt_EP_13TX_BIT 27
#define USBEpIntSt_EP_13RX_MASK 0x4000000
#define USBEpIntSt_EP_13RX 0x4000000
#define USBEpIntSt_EP_13RX_BIT 26
#define USBEpIntSt_EP_12TX_MASK 0x2000000
#define USBEpIntSt_EP_12TX 0x2000000
#define USBEpIntSt_EP_12TX_BIT 25
#define USBEpIntSt_EP_12RX_MASK 0x1000000
#define USBEpIntSt_EP_12RX 0x1000000
#define USBEpIntSt_EP_12RX_BIT 24
#define USBEpIntSt_EP_11TX_MASK 0x800000
#define USBEpIntSt_EP_11TX 0x800000
#define USBEpIntSt_EP_11TX_BIT 23
#define USBEpIntSt_EP_11RX_MASK 0x400000
#define USBEpIntSt_EP_11RX 0x400000
#define USBEpIntSt_EP_11RX_BIT 22
#define USBEpIntSt_EP_10TX_MASK 0x200000
#define USBEpIntSt_EP_10TX 0x200000
#define USBEpIntSt_EP_10TX_BIT 21
#define USBEpIntSt_EP_10RX_MASK 0x100000
#define USBEpIntSt_EP_10RX 0x100000
#define USBEpIntSt_EP_10RX_BIT 20
#define USBEpIntSt_EP_9TX_MASK 0x80000
#define USBEpIntSt_EP_9TX 0x80000
#define USBEpIntSt_EP_9TX_BIT 19
#define USBEpIntSt_EP_9RX_MASK 0x40000
#define USBEpIntSt_EP_9RX 0x40000
#define USBEpIntSt_EP_9RX_BIT 18
#define USBEpIntSt_EP_8TX_MASK 0x20000
#define USBEpIntSt_EP_8TX 0x20000
#define USBEpIntSt_EP_8TX_BIT 17
#define USBEpIntSt_EP_8RX_MASK 0x10000
#define USBEpIntSt_EP_8RX 0x10000
#define USBEpIntSt_EP_8RX_BIT 16
#define USBEpIntSt_EP_7TX_MASK 0x8000
#define USBEpIntSt_EP_7TX 0x8000
#define USBEpIntSt_EP_7TX_BIT 15
#define USBEpIntSt_EP_7RX_MASK 0x4000
#define USBEpIntSt_EP_7RX 0x4000
#define USBEpIntSt_EP_7RX_BIT 14
#define USBEpIntSt_EP_6TX_MASK 0x2000
#define USBEpIntSt_EP_6TX 0x2000
#define USBEpIntSt_EP_6TX_BIT 13
#define USBEpIntSt_EP_6RX_MASK 0x1000
#define USBEpIntSt_EP_6RX 0x1000
#define USBEpIntSt_EP_6RX_BIT 12
#define USBEpIntSt_EP_5TX_MASK 0x800
#define USBEpIntSt_EP_5TX 0x800
#define USBEpIntSt_EP_5TX_BIT 11
#define USBEpIntSt_EP_5RX_MASK 0x400
#define USBEpIntSt_EP_5RX 0x400
#define USBEpIntSt_EP_5RX_BIT 10
#define USBEpIntSt_EP_4TX_MASK 0x200
#define USBEpIntSt_EP_4TX 0x200
#define USBEpIntSt_EP_4TX_BIT 9
#define USBEpIntSt_EP_4RX_MASK 0x100
#define USBEpIntSt_EP_4RX 0x100
#define USBEpIntSt_EP_4RX_BIT 8
#define USBEpIntSt_EP_3TX_MASK 0x80
#define USBEpIntSt_EP_3TX 0x80
#define USBEpIntSt_EP_3TX_BIT 7
#define USBEpIntSt_EP_3RX_MASK 0x40
#define USBEpIntSt_EP_3RX 0x40
#define USBEpIntSt_EP_3RX_BIT 6
#define USBEpIntSt_EP_2TX_MASK 0x20
#define USBEpIntSt_EP_2TX 0x20
#define USBEpIntSt_EP_2TX_BIT 5
#define USBEpIntSt_EP_2RX_MASK 0x10
#define USBEpIntSt_EP_2RX 0x10
#define USBEpIntSt_EP_2RX_BIT 4
#define USBEpIntSt_EP_1TX_MASK 0x8
#define USBEpIntSt_EP_1TX 0x8
#define USBEpIntSt_EP_1TX_BIT 3
#define USBEpIntSt_EP_1RX_MASK 0x4
#define USBEpIntSt_EP_1RX 0x4
#define USBEpIntSt_EP_1RX_BIT 2
#define USBEpIntSt_EP_0TX_MASK 0x2
#define USBEpIntSt_EP_0TX 0x2
#define USBEpIntSt_EP_0TX_BIT 1
#define USBEpIntSt_EP_0RX_MASK 0x1
#define USBEpIntSt_EP_0RX 0x1
#define USBEpIntSt_EP_0RX_BIT 0

#define USBEpIntEn (*(volatile unsigned *)0x31020234)
#define USBEpIntEn_EP_15TX_MASK 0x80000000
#define USBEpIntEn_EP_15TX 0x80000000
#define USBEpIntEn_EP_15TX_BIT 31
#define USBEpIntEn_EP_15RX_MASK 0x40000000
#define USBEpIntEn_EP_15RX 0x40000000
#define USBEpIntEn_EP_15RX_BIT 30
#define USBEpIntEn_EP_14TX_MASK 0x20000000
#define USBEpIntEn_EP_14TX 0x20000000
#define USBEpIntEn_EP_14TX_BIT 29
#define USBEpIntEn_EP_14RX_MASK 0x10000000
#define USBEpIntEn_EP_14RX 0x10000000
#define USBEpIntEn_EP_14RX_BIT 28
#define USBEpIntEn_EP_13TX_MASK 0x8000000
#define USBEpIntEn_EP_13TX 0x8000000
#define USBEpIntEn_EP_13TX_BIT 27
#define USBEpIntEn_EP_13RX_MASK 0x4000000
#define USBEpIntEn_EP_13RX 0x4000000
#define USBEpIntEn_EP_13RX_BIT 26
#define USBEpIntEn_EP_12TX_MASK 0x2000000
#define USBEpIntEn_EP_12TX 0x2000000
#define USBEpIntEn_EP_12TX_BIT 25
#define USBEpIntEn_EP_12RX_MASK 0x1000000
#define USBEpIntEn_EP_12RX 0x1000000
#define USBEpIntEn_EP_12RX_BIT 24
#define USBEpIntEn_EP_11TX_MASK 0x800000
#define USBEpIntEn_EP_11TX 0x800000
#define USBEpIntEn_EP_11TX_BIT 23
#define USBEpIntEn_EP_11RX_MASK 0x400000
#define USBEpIntEn_EP_11RX 0x400000
#define USBEpIntEn_EP_11RX_BIT 22
#define USBEpIntEn_EP_10TX_MASK 0x200000
#define USBEpIntEn_EP_10TX 0x200000
#define USBEpIntEn_EP_10TX_BIT 21
#define USBEpIntEn_EP_10RX_MASK 0x100000
#define USBEpIntEn_EP_10RX 0x100000
#define USBEpIntEn_EP_10RX_BIT 20
#define USBEpIntEn_EP_9TX_MASK 0x80000
#define USBEpIntEn_EP_9TX 0x80000
#define USBEpIntEn_EP_9TX_BIT 19
#define USBEpIntEn_EP_9RX_MASK 0x40000
#define USBEpIntEn_EP_9RX 0x40000
#define USBEpIntEn_EP_9RX_BIT 18
#define USBEpIntEn_EP_8TX_MASK 0x20000
#define USBEpIntEn_EP_8TX 0x20000
#define USBEpIntEn_EP_8TX_BIT 17
#define USBEpIntEn_EP_8RX_MASK 0x10000
#define USBEpIntEn_EP_8RX 0x10000
#define USBEpIntEn_EP_8RX_BIT 16
#define USBEpIntEn_EP_7TX_MASK 0x8000
#define USBEpIntEn_EP_7TX 0x8000
#define USBEpIntEn_EP_7TX_BIT 15
#define USBEpIntEn_EP_7RX_MASK 0x4000
#define USBEpIntEn_EP_7RX 0x4000
#define USBEpIntEn_EP_7RX_BIT 14
#define USBEpIntEn_EP_6TX_MASK 0x2000
#define USBEpIntEn_EP_6TX 0x2000
#define USBEpIntEn_EP_6TX_BIT 13
#define USBEpIntEn_EP_6RX_MASK 0x1000
#define USBEpIntEn_EP_6RX 0x1000
#define USBEpIntEn_EP_6RX_BIT 12
#define USBEpIntEn_EP_5TX_MASK 0x800
#define USBEpIntEn_EP_5TX 0x800
#define USBEpIntEn_EP_5TX_BIT 11
#define USBEpIntEn_EP_5RX_MASK 0x400
#define USBEpIntEn_EP_5RX 0x400
#define USBEpIntEn_EP_5RX_BIT 10
#define USBEpIntEn_EP_4TX_MASK 0x200
#define USBEpIntEn_EP_4TX 0x200
#define USBEpIntEn_EP_4TX_BIT 9
#define USBEpIntEn_EP_4RX_MASK 0x100
#define USBEpIntEn_EP_4RX 0x100
#define USBEpIntEn_EP_4RX_BIT 8
#define USBEpIntEn_EP_3TX_MASK 0x80
#define USBEpIntEn_EP_3TX 0x80
#define USBEpIntEn_EP_3TX_BIT 7
#define USBEpIntEn_EP_3RX_MASK 0x40
#define USBEpIntEn_EP_3RX 0x40
#define USBEpIntEn_EP_3RX_BIT 6
#define USBEpIntEn_EP_2TX_MASK 0x20
#define USBEpIntEn_EP_2TX 0x20
#define USBEpIntEn_EP_2TX_BIT 5
#define USBEpIntEn_EP_2RX_MASK 0x10
#define USBEpIntEn_EP_2RX 0x10
#define USBEpIntEn_EP_2RX_BIT 4
#define USBEpIntEn_EP_1TX_MASK 0x8
#define USBEpIntEn_EP_1TX 0x8
#define USBEpIntEn_EP_1TX_BIT 3
#define USBEpIntEn_EP_1RX_MASK 0x4
#define USBEpIntEn_EP_1RX 0x4
#define USBEpIntEn_EP_1RX_BIT 2
#define USBEpIntEn_EP_0TX_MASK 0x2
#define USBEpIntEn_EP_0TX 0x2
#define USBEpIntEn_EP_0TX_BIT 1
#define USBEpIntEn_EP_0RX_MASK 0x1
#define USBEpIntEn_EP_0RX 0x1
#define USBEpIntEn_EP_0RX_BIT 0

#define USBEpIntClr (*(volatile unsigned *)0x31020238)
#define USBEpIntClr_EP_15TX_MASK 0x80000000
#define USBEpIntClr_EP_15TX 0x80000000
#define USBEpIntClr_EP_15TX_BIT 31
#define USBEpIntClr_EP_15RX_MASK 0x40000000
#define USBEpIntClr_EP_15RX 0x40000000
#define USBEpIntClr_EP_15RX_BIT 30
#define USBEpIntClr_EP_14TX_MASK 0x20000000
#define USBEpIntClr_EP_14TX 0x20000000
#define USBEpIntClr_EP_14TX_BIT 29
#define USBEpIntClr_EP_14RX_MASK 0x10000000
#define USBEpIntClr_EP_14RX 0x10000000
#define USBEpIntClr_EP_14RX_BIT 28
#define USBEpIntClr_EP_13TX_MASK 0x8000000
#define USBEpIntClr_EP_13TX 0x8000000
#define USBEpIntClr_EP_13TX_BIT 27
#define USBEpIntClr_EP_13RX_MASK 0x4000000
#define USBEpIntClr_EP_13RX 0x4000000
#define USBEpIntClr_EP_13RX_BIT 26
#define USBEpIntClr_EP_12TX_MASK 0x2000000
#define USBEpIntClr_EP_12TX 0x2000000
#define USBEpIntClr_EP_12TX_BIT 25
#define USBEpIntClr_EP_12RX_MASK 0x1000000
#define USBEpIntClr_EP_12RX 0x1000000
#define USBEpIntClr_EP_12RX_BIT 24
#define USBEpIntClr_EP_11TX_MASK 0x800000
#define USBEpIntClr_EP_11TX 0x800000
#define USBEpIntClr_EP_11TX_BIT 23
#define USBEpIntClr_EP_11RX_MASK 0x400000
#define USBEpIntClr_EP_11RX 0x400000
#define USBEpIntClr_EP_11RX_BIT 22
#define USBEpIntClr_EP_10TX_MASK 0x200000
#define USBEpIntClr_EP_10TX 0x200000
#define USBEpIntClr_EP_10TX_BIT 21
#define USBEpIntClr_EP_10RX_MASK 0x100000
#define USBEpIntClr_EP_10RX 0x100000
#define USBEpIntClr_EP_10RX_BIT 20
#define USBEpIntClr_EP_9TX_MASK 0x80000
#define USBEpIntClr_EP_9TX 0x80000
#define USBEpIntClr_EP_9TX_BIT 19
#define USBEpIntClr_EP_9RX_MASK 0x40000
#define USBEpIntClr_EP_9RX 0x40000
#define USBEpIntClr_EP_9RX_BIT 18
#define USBEpIntClr_EP_8TX_MASK 0x20000
#define USBEpIntClr_EP_8TX 0x20000
#define USBEpIntClr_EP_8TX_BIT 17
#define USBEpIntClr_EP_8RX_MASK 0x10000
#define USBEpIntClr_EP_8RX 0x10000
#define USBEpIntClr_EP_8RX_BIT 16
#define USBEpIntClr_EP_7TX_MASK 0x8000
#define USBEpIntClr_EP_7TX 0x8000
#define USBEpIntClr_EP_7TX_BIT 15
#define USBEpIntClr_EP_7RX_MASK 0x4000
#define USBEpIntClr_EP_7RX 0x4000
#define USBEpIntClr_EP_7RX_BIT 14
#define USBEpIntClr_EP_6TX_MASK 0x2000
#define USBEpIntClr_EP_6TX 0x2000
#define USBEpIntClr_EP_6TX_BIT 13
#define USBEpIntClr_EP_6RX_MASK 0x1000
#define USBEpIntClr_EP_6RX 0x1000
#define USBEpIntClr_EP_6RX_BIT 12
#define USBEpIntClr_EP_5TX_MASK 0x800
#define USBEpIntClr_EP_5TX 0x800
#define USBEpIntClr_EP_5TX_BIT 11
#define USBEpIntClr_EP_5RX_MASK 0x400
#define USBEpIntClr_EP_5RX 0x400
#define USBEpIntClr_EP_5RX_BIT 10
#define USBEpIntClr_EP_4TX_MASK 0x200
#define USBEpIntClr_EP_4TX 0x200
#define USBEpIntClr_EP_4TX_BIT 9
#define USBEpIntClr_EP_4RX_MASK 0x100
#define USBEpIntClr_EP_4RX 0x100
#define USBEpIntClr_EP_4RX_BIT 8
#define USBEpIntClr_EP_3TX_MASK 0x80
#define USBEpIntClr_EP_3TX 0x80
#define USBEpIntClr_EP_3TX_BIT 7
#define USBEpIntClr_EP_3RX_MASK 0x40
#define USBEpIntClr_EP_3RX 0x40
#define USBEpIntClr_EP_3RX_BIT 6
#define USBEpIntClr_EP_2TX_MASK 0x20
#define USBEpIntClr_EP_2TX 0x20
#define USBEpIntClr_EP_2TX_BIT 5
#define USBEpIntClr_EP_2RX_MASK 0x10
#define USBEpIntClr_EP_2RX 0x10
#define USBEpIntClr_EP_2RX_BIT 4
#define USBEpIntClr_EP_1TX_MASK 0x8
#define USBEpIntClr_EP_1TX 0x8
#define USBEpIntClr_EP_1TX_BIT 3
#define USBEpIntClr_EP_1RX_MASK 0x4
#define USBEpIntClr_EP_1RX 0x4
#define USBEpIntClr_EP_1RX_BIT 2
#define USBEpIntClr_EP_0TX_MASK 0x2
#define USBEpIntClr_EP_0TX 0x2
#define USBEpIntClr_EP_0TX_BIT 1
#define USBEpIntClr_EP_0RX_MASK 0x1
#define USBEpIntClr_EP_0RX 0x1
#define USBEpIntClr_EP_0RX_BIT 0

#define USBEpIntSet (*(volatile unsigned *)0x3102023C)
#define USBEpIntSet_EP_15TX_MASK 0x80000000
#define USBEpIntSet_EP_15TX 0x80000000
#define USBEpIntSet_EP_15TX_BIT 31
#define USBEpIntSet_EP_15RX_MASK 0x40000000
#define USBEpIntSet_EP_15RX 0x40000000
#define USBEpIntSet_EP_15RX_BIT 30
#define USBEpIntSet_EP_14TX_MASK 0x20000000
#define USBEpIntSet_EP_14TX 0x20000000
#define USBEpIntSet_EP_14TX_BIT 29
#define USBEpIntSet_EP_14RX_MASK 0x10000000
#define USBEpIntSet_EP_14RX 0x10000000
#define USBEpIntSet_EP_14RX_BIT 28
#define USBEpIntSet_EP_13TX_MASK 0x8000000
#define USBEpIntSet_EP_13TX 0x8000000
#define USBEpIntSet_EP_13TX_BIT 27
#define USBEpIntSet_EP_13RX_MASK 0x4000000
#define USBEpIntSet_EP_13RX 0x4000000
#define USBEpIntSet_EP_13RX_BIT 26
#define USBEpIntSet_EP_12TX_MASK 0x2000000
#define USBEpIntSet_EP_12TX 0x2000000
#define USBEpIntSet_EP_12TX_BIT 25
#define USBEpIntSet_EP_12RX_MASK 0x1000000
#define USBEpIntSet_EP_12RX 0x1000000
#define USBEpIntSet_EP_12RX_BIT 24
#define USBEpIntSet_EP_11TX_MASK 0x800000
#define USBEpIntSet_EP_11TX 0x800000
#define USBEpIntSet_EP_11TX_BIT 23
#define USBEpIntSet_EP_11RX_MASK 0x400000
#define USBEpIntSet_EP_11RX 0x400000
#define USBEpIntSet_EP_11RX_BIT 22
#define USBEpIntSet_EP_10TX_MASK 0x200000
#define USBEpIntSet_EP_10TX 0x200000
#define USBEpIntSet_EP_10TX_BIT 21
#define USBEpIntSet_EP_10RX_MASK 0x100000
#define USBEpIntSet_EP_10RX 0x100000
#define USBEpIntSet_EP_10RX_BIT 20
#define USBEpIntSet_EP_9TX_MASK 0x80000
#define USBEpIntSet_EP_9TX 0x80000
#define USBEpIntSet_EP_9TX_BIT 19
#define USBEpIntSet_EP_9RX_MASK 0x40000
#define USBEpIntSet_EP_9RX 0x40000
#define USBEpIntSet_EP_9RX_BIT 18
#define USBEpIntSet_EP_8TX_MASK 0x20000
#define USBEpIntSet_EP_8TX 0x20000
#define USBEpIntSet_EP_8TX_BIT 17
#define USBEpIntSet_EP_8RX_MASK 0x10000
#define USBEpIntSet_EP_8RX 0x10000
#define USBEpIntSet_EP_8RX_BIT 16
#define USBEpIntSet_EP_7TX_MASK 0x8000
#define USBEpIntSet_EP_7TX 0x8000
#define USBEpIntSet_EP_7TX_BIT 15
#define USBEpIntSet_EP_7RX_MASK 0x4000
#define USBEpIntSet_EP_7RX 0x4000
#define USBEpIntSet_EP_7RX_BIT 14
#define USBEpIntSet_EP_6TX_MASK 0x2000
#define USBEpIntSet_EP_6TX 0x2000
#define USBEpIntSet_EP_6TX_BIT 13
#define USBEpIntSet_EP_6RX_MASK 0x1000
#define USBEpIntSet_EP_6RX 0x1000
#define USBEpIntSet_EP_6RX_BIT 12
#define USBEpIntSet_EP_5TX_MASK 0x800
#define USBEpIntSet_EP_5TX 0x800
#define USBEpIntSet_EP_5TX_BIT 11
#define USBEpIntSet_EP_5RX_MASK 0x400
#define USBEpIntSet_EP_5RX 0x400
#define USBEpIntSet_EP_5RX_BIT 10
#define USBEpIntSet_EP_4TX_MASK 0x200
#define USBEpIntSet_EP_4TX 0x200
#define USBEpIntSet_EP_4TX_BIT 9
#define USBEpIntSet_EP_4RX_MASK 0x100
#define USBEpIntSet_EP_4RX 0x100
#define USBEpIntSet_EP_4RX_BIT 8
#define USBEpIntSet_EP_3TX_MASK 0x80
#define USBEpIntSet_EP_3TX 0x80
#define USBEpIntSet_EP_3TX_BIT 7
#define USBEpIntSet_EP_3RX_MASK 0x40
#define USBEpIntSet_EP_3RX 0x40
#define USBEpIntSet_EP_3RX_BIT 6
#define USBEpIntSet_EP_2TX_MASK 0x20
#define USBEpIntSet_EP_2TX 0x20
#define USBEpIntSet_EP_2TX_BIT 5
#define USBEpIntSet_EP_2RX_MASK 0x10
#define USBEpIntSet_EP_2RX 0x10
#define USBEpIntSet_EP_2RX_BIT 4
#define USBEpIntSet_EP_1TX_MASK 0x8
#define USBEpIntSet_EP_1TX 0x8
#define USBEpIntSet_EP_1TX_BIT 3
#define USBEpIntSet_EP_1RX_MASK 0x4
#define USBEpIntSet_EP_1RX 0x4
#define USBEpIntSet_EP_1RX_BIT 2
#define USBEpIntSet_EP_0TX_MASK 0x2
#define USBEpIntSet_EP_0TX 0x2
#define USBEpIntSet_EP_0TX_BIT 1
#define USBEpIntSet_EP_0RX_MASK 0x1
#define USBEpIntSet_EP_0RX 0x1
#define USBEpIntSet_EP_0RX_BIT 0

#define USBEpIntPri (*(volatile unsigned *)0x31020240)
#define USBEpIntPri_EP_15TX_MASK 0x80000000
#define USBEpIntPri_EP_15TX 0x80000000
#define USBEpIntPri_EP_15TX_BIT 31
#define USBEpIntPri_EP_15RX_MASK 0x40000000
#define USBEpIntPri_EP_15RX 0x40000000
#define USBEpIntPri_EP_15RX_BIT 30
#define USBEpIntPri_EP_14TX_MASK 0x20000000
#define USBEpIntPri_EP_14TX 0x20000000
#define USBEpIntPri_EP_14TX_BIT 29
#define USBEpIntPri_EP_14RX_MASK 0x10000000
#define USBEpIntPri_EP_14RX 0x10000000
#define USBEpIntPri_EP_14RX_BIT 28
#define USBEpIntPri_EP_13TX_MASK 0x8000000
#define USBEpIntPri_EP_13TX 0x8000000
#define USBEpIntPri_EP_13TX_BIT 27
#define USBEpIntPri_EP_13RX_MASK 0x4000000
#define USBEpIntPri_EP_13RX 0x4000000
#define USBEpIntPri_EP_13RX_BIT 26
#define USBEpIntPri_EP_12TX_MASK 0x2000000
#define USBEpIntPri_EP_12TX 0x2000000
#define USBEpIntPri_EP_12TX_BIT 25
#define USBEpIntPri_EP_12RX_MASK 0x1000000
#define USBEpIntPri_EP_12RX 0x1000000
#define USBEpIntPri_EP_12RX_BIT 24
#define USBEpIntPri_EP_11TX_MASK 0x800000
#define USBEpIntPri_EP_11TX 0x800000
#define USBEpIntPri_EP_11TX_BIT 23
#define USBEpIntPri_EP_11RX_MASK 0x400000
#define USBEpIntPri_EP_11RX 0x400000
#define USBEpIntPri_EP_11RX_BIT 22
#define USBEpIntPri_EP_10TX_MASK 0x200000
#define USBEpIntPri_EP_10TX 0x200000
#define USBEpIntPri_EP_10TX_BIT 21
#define USBEpIntPri_EP_10RX_MASK 0x100000
#define USBEpIntPri_EP_10RX 0x100000
#define USBEpIntPri_EP_10RX_BIT 20
#define USBEpIntPri_EP_9TX_MASK 0x80000
#define USBEpIntPri_EP_9TX 0x80000
#define USBEpIntPri_EP_9TX_BIT 19
#define USBEpIntPri_EP_9RX_MASK 0x40000
#define USBEpIntPri_EP_9RX 0x40000
#define USBEpIntPri_EP_9RX_BIT 18
#define USBEpIntPri_EP_8TX_MASK 0x20000
#define USBEpIntPri_EP_8TX 0x20000
#define USBEpIntPri_EP_8TX_BIT 17
#define USBEpIntPri_EP_8RX_MASK 0x10000
#define USBEpIntPri_EP_8RX 0x10000
#define USBEpIntPri_EP_8RX_BIT 16
#define USBEpIntPri_EP_7TX_MASK 0x8000
#define USBEpIntPri_EP_7TX 0x8000
#define USBEpIntPri_EP_7TX_BIT 15
#define USBEpIntPri_EP_7RX_MASK 0x4000
#define USBEpIntPri_EP_7RX 0x4000
#define USBEpIntPri_EP_7RX_BIT 14
#define USBEpIntPri_EP_6TX_MASK 0x2000
#define USBEpIntPri_EP_6TX 0x2000
#define USBEpIntPri_EP_6TX_BIT 13
#define USBEpIntPri_EP_6RX_MASK 0x1000
#define USBEpIntPri_EP_6RX 0x1000
#define USBEpIntPri_EP_6RX_BIT 12
#define USBEpIntPri_EP_5TX_MASK 0x800
#define USBEpIntPri_EP_5TX 0x800
#define USBEpIntPri_EP_5TX_BIT 11
#define USBEpIntPri_EP_5RX_MASK 0x400
#define USBEpIntPri_EP_5RX 0x400
#define USBEpIntPri_EP_5RX_BIT 10
#define USBEpIntPri_EP_4TX_MASK 0x200
#define USBEpIntPri_EP_4TX 0x200
#define USBEpIntPri_EP_4TX_BIT 9
#define USBEpIntPri_EP_4RX_MASK 0x100
#define USBEpIntPri_EP_4RX 0x100
#define USBEpIntPri_EP_4RX_BIT 8
#define USBEpIntPri_EP_3TX_MASK 0x80
#define USBEpIntPri_EP_3TX 0x80
#define USBEpIntPri_EP_3TX_BIT 7
#define USBEpIntPri_EP_3RX_MASK 0x40
#define USBEpIntPri_EP_3RX 0x40
#define USBEpIntPri_EP_3RX_BIT 6
#define USBEpIntPri_EP_2TX_MASK 0x20
#define USBEpIntPri_EP_2TX 0x20
#define USBEpIntPri_EP_2TX_BIT 5
#define USBEpIntPri_EP_2RX_MASK 0x10
#define USBEpIntPri_EP_2RX 0x10
#define USBEpIntPri_EP_2RX_BIT 4
#define USBEpIntPri_EP_1TX_MASK 0x8
#define USBEpIntPri_EP_1TX 0x8
#define USBEpIntPri_EP_1TX_BIT 3
#define USBEpIntPri_EP_1RX_MASK 0x4
#define USBEpIntPri_EP_1RX 0x4
#define USBEpIntPri_EP_1RX_BIT 2
#define USBEpIntPri_EP_0TX_MASK 0x2
#define USBEpIntPri_EP_0TX 0x2
#define USBEpIntPri_EP_0TX_BIT 1
#define USBEpIntPri_EP_0RX_MASK 0x1
#define USBEpIntPri_EP_0RX 0x1
#define USBEpIntPri_EP_0RX_BIT 0

#define USBReEp (*(volatile unsigned *)0x31020244)
#define USBReEp_EP0_MASK 0x1
#define USBReEp_EP0 0x1
#define USBReEp_EP0_BIT 0
#define USBReEp_EP1_MASK 0x2
#define USBReEp_EP1 0x2
#define USBReEp_EP1_BIT 1
#define USBReEp_EP2_MASK 0x4
#define USBReEp_EP2 0x4
#define USBReEp_EP2_BIT 2
#define USBReEp_EP3_MASK 0x8
#define USBReEp_EP3 0x8
#define USBReEp_EP3_BIT 3
#define USBReEp_EP4_MASK 0x10
#define USBReEp_EP4 0x10
#define USBReEp_EP4_BIT 4
#define USBReEp_EP5_MASK 0x20
#define USBReEp_EP5 0x20
#define USBReEp_EP5_BIT 5
#define USBReEp_EP6_MASK 0x40
#define USBReEp_EP6 0x40
#define USBReEp_EP6_BIT 6
#define USBReEp_EP7_MASK 0x80
#define USBReEp_EP7 0x80
#define USBReEp_EP7_BIT 7
#define USBReEp_EP8_MASK 0x100
#define USBReEp_EP8 0x100
#define USBReEp_EP8_BIT 8
#define USBReEp_EP9_MASK 0x200
#define USBReEp_EP9 0x200
#define USBReEp_EP9_BIT 9
#define USBReEp_EP10_MASK 0x400
#define USBReEp_EP10 0x400
#define USBReEp_EP10_BIT 10
#define USBReEp_EP11_MASK 0x800
#define USBReEp_EP11 0x800
#define USBReEp_EP11_BIT 11
#define USBReEp_EP12_MASK 0x1000
#define USBReEp_EP12 0x1000
#define USBReEp_EP12_BIT 12
#define USBReEp_EP13_MASK 0x2000
#define USBReEp_EP13 0x2000
#define USBReEp_EP13_BIT 13
#define USBReEp_EP14_MASK 0x4000
#define USBReEp_EP14 0x4000
#define USBReEp_EP14_BIT 14
#define USBReEp_EP15_MASK 0x8000
#define USBReEp_EP15 0x8000
#define USBReEp_EP15_BIT 15
#define USBReEp_EP16_MASK 0x10000
#define USBReEp_EP16 0x10000
#define USBReEp_EP16_BIT 16
#define USBReEp_EP17_MASK 0x20000
#define USBReEp_EP17 0x20000
#define USBReEp_EP17_BIT 17
#define USBReEp_EP18_MASK 0x40000
#define USBReEp_EP18 0x40000
#define USBReEp_EP18_BIT 18
#define USBReEp_EP19_MASK 0x80000
#define USBReEp_EP19 0x80000
#define USBReEp_EP19_BIT 19
#define USBReEp_EP20_MASK 0x100000
#define USBReEp_EP20 0x100000
#define USBReEp_EP20_BIT 20
#define USBReEp_EP21_MASK 0x200000
#define USBReEp_EP21 0x200000
#define USBReEp_EP21_BIT 21
#define USBReEp_EP22_MASK 0x400000
#define USBReEp_EP22 0x400000
#define USBReEp_EP22_BIT 22
#define USBReEp_EP23_MASK 0x800000
#define USBReEp_EP23 0x800000
#define USBReEp_EP23_BIT 23
#define USBReEp_EP24_MASK 0x1000000
#define USBReEp_EP24 0x1000000
#define USBReEp_EP24_BIT 24
#define USBReEp_EP25_MASK 0x2000000
#define USBReEp_EP25 0x2000000
#define USBReEp_EP25_BIT 25
#define USBReEp_EP26_MASK 0x4000000
#define USBReEp_EP26 0x4000000
#define USBReEp_EP26_BIT 26
#define USBReEp_EP27_MASK 0x8000000
#define USBReEp_EP27 0x8000000
#define USBReEp_EP27_BIT 27
#define USBReEp_EP28_MASK 0x10000000
#define USBReEp_EP28 0x10000000
#define USBReEp_EP28_BIT 28
#define USBReEp_EP29_MASK 0x20000000
#define USBReEp_EP29 0x20000000
#define USBReEp_EP29_BIT 29
#define USBReEp_EP30_MASK 0x40000000
#define USBReEp_EP30 0x40000000
#define USBReEp_EP30_BIT 30
#define USBReEp_EP31_MASK 0x80000000
#define USBReEp_EP31 0x80000000
#define USBReEp_EP31_BIT 31

#define USBEpInd (*(volatile unsigned *)0x31020248)
#define USBEpInd_Phy_endpoint_MASK 0x1F
#define USBEpInd_Phy_endpoint_BIT 0

#define USBEpMaxPSize (*(volatile unsigned *)0x3102024C)
#define USBEpMaxPSize_MaxPacketSize_MASK 0x3FF
#define USBEpMaxPSize_MaxPacketSize_BIT 0

#define USBRxData (*(volatile unsigned *)0x31020218)

#define USBRxPLen (*(volatile unsigned *)0x31020220)
#define USBRxPLen_PKT_RDY_MASK 0x800
#define USBRxPLen_PKT_RDY 0x800
#define USBRxPLen_PKT_RDY_BIT 11
#define USBRxPLen_DV_MASK 0x400
#define USBRxPLen_DV 0x400
#define USBRxPLen_DV_BIT 10
#define USBRxPLen_PKT_LNGTH_MASK 0x3FF
#define USBRxPLen_PKT_LNGTH_BIT 0

#define USBTxData (*(volatile unsigned *)0x3102021C)

#define USBTxPLen (*(volatile unsigned *)0x31020224)
#define USBTxPLen_PKT_LNGTH_MASK 0x3FF
#define USBTxPLen_PKT_LNGTH_BIT 0

#define USBCtrl (*(volatile unsigned *)0x31020228)
#define USBCtrl_LOG_ENDPOINT_MASK 0x3C
#define USBCtrl_LOG_ENDPOINT_BIT 2
#define USBCtrl_WR_EN_MASK 0x2
#define USBCtrl_WR_EN 0x2
#define USBCtrl_WR_EN_BIT 1
#define USBCtrl_RD_EN_MASK 0x1
#define USBCtrl_RD_EN 0x1
#define USBCtrl_RD_EN_BIT 0

#define USBCmdCode (*(volatile unsigned *)0x31020210)
#define USBCmdCode_CMD_CODE_MASK 0xFF0000
#define USBCmdCode_CMD_CODE_BIT 16
#define USBCmdCode_CMD_PHASE_MASK 0xFF00
#define USBCmdCode_CMD_PHASE_BIT 8

#define USBCmdData (*(volatile unsigned *)0x31020214)
#define USBCmdData_Command_Data_MASK 0xFF
#define USBCmdData_Command_Data_BIT 0

#define USBDMARSt (*(volatile unsigned *)0x31020250)
#define USBDMARSt_EP0_MASK 0x1
#define USBDMARSt_EP0 0x1
#define USBDMARSt_EP0_BIT 0
#define USBDMARSt_EP1_MASK 0x2
#define USBDMARSt_EP1 0x2
#define USBDMARSt_EP1_BIT 1
#define USBDMARSt_EP2_MASK 0x4
#define USBDMARSt_EP2 0x4
#define USBDMARSt_EP2_BIT 2
#define USBDMARSt_EP3_MASK 0x8
#define USBDMARSt_EP3 0x8
#define USBDMARSt_EP3_BIT 3
#define USBDMARSt_EP4_MASK 0x10
#define USBDMARSt_EP4 0x10
#define USBDMARSt_EP4_BIT 4
#define USBDMARSt_EP5_MASK 0x20
#define USBDMARSt_EP5 0x20
#define USBDMARSt_EP5_BIT 5
#define USBDMARSt_EP6_MASK 0x40
#define USBDMARSt_EP6 0x40
#define USBDMARSt_EP6_BIT 6
#define USBDMARSt_EP7_MASK 0x80
#define USBDMARSt_EP7 0x80
#define USBDMARSt_EP7_BIT 7
#define USBDMARSt_EP8_MASK 0x100
#define USBDMARSt_EP8 0x100
#define USBDMARSt_EP8_BIT 8
#define USBDMARSt_EP9_MASK 0x200
#define USBDMARSt_EP9 0x200
#define USBDMARSt_EP9_BIT 9
#define USBDMARSt_EP10_MASK 0x400
#define USBDMARSt_EP10 0x400
#define USBDMARSt_EP10_BIT 10
#define USBDMARSt_EP11_MASK 0x800
#define USBDMARSt_EP11 0x800
#define USBDMARSt_EP11_BIT 11
#define USBDMARSt_EP12_MASK 0x1000
#define USBDMARSt_EP12 0x1000
#define USBDMARSt_EP12_BIT 12
#define USBDMARSt_EP13_MASK 0x2000
#define USBDMARSt_EP13 0x2000
#define USBDMARSt_EP13_BIT 13
#define USBDMARSt_EP14_MASK 0x4000
#define USBDMARSt_EP14 0x4000
#define USBDMARSt_EP14_BIT 14
#define USBDMARSt_EP15_MASK 0x8000
#define USBDMARSt_EP15 0x8000
#define USBDMARSt_EP15_BIT 15
#define USBDMARSt_EP16_MASK 0x10000
#define USBDMARSt_EP16 0x10000
#define USBDMARSt_EP16_BIT 16
#define USBDMARSt_EP17_MASK 0x20000
#define USBDMARSt_EP17 0x20000
#define USBDMARSt_EP17_BIT 17
#define USBDMARSt_EP18_MASK 0x40000
#define USBDMARSt_EP18 0x40000
#define USBDMARSt_EP18_BIT 18
#define USBDMARSt_EP19_MASK 0x80000
#define USBDMARSt_EP19 0x80000
#define USBDMARSt_EP19_BIT 19
#define USBDMARSt_EP20_MASK 0x100000
#define USBDMARSt_EP20 0x100000
#define USBDMARSt_EP20_BIT 20
#define USBDMARSt_EP21_MASK 0x200000
#define USBDMARSt_EP21 0x200000
#define USBDMARSt_EP21_BIT 21
#define USBDMARSt_EP22_MASK 0x400000
#define USBDMARSt_EP22 0x400000
#define USBDMARSt_EP22_BIT 22
#define USBDMARSt_EP23_MASK 0x800000
#define USBDMARSt_EP23 0x800000
#define USBDMARSt_EP23_BIT 23
#define USBDMARSt_EP24_MASK 0x1000000
#define USBDMARSt_EP24 0x1000000
#define USBDMARSt_EP24_BIT 24
#define USBDMARSt_EP25_MASK 0x2000000
#define USBDMARSt_EP25 0x2000000
#define USBDMARSt_EP25_BIT 25
#define USBDMARSt_EP26_MASK 0x4000000
#define USBDMARSt_EP26 0x4000000
#define USBDMARSt_EP26_BIT 26
#define USBDMARSt_EP27_MASK 0x8000000
#define USBDMARSt_EP27 0x8000000
#define USBDMARSt_EP27_BIT 27
#define USBDMARSt_EP28_MASK 0x10000000
#define USBDMARSt_EP28 0x10000000
#define USBDMARSt_EP28_BIT 28
#define USBDMARSt_EP29_MASK 0x20000000
#define USBDMARSt_EP29 0x20000000
#define USBDMARSt_EP29_BIT 29
#define USBDMARSt_EP30_MASK 0x40000000
#define USBDMARSt_EP30 0x40000000
#define USBDMARSt_EP30_BIT 30
#define USBDMARSt_EP31_MASK 0x80000000
#define USBDMARSt_EP31 0x80000000
#define USBDMARSt_EP31_BIT 31

#define USBDMARClr (*(volatile unsigned *)0x31020254)
#define USBDMARClr_EP0_MASK 0x1
#define USBDMARClr_EP0 0x1
#define USBDMARClr_EP0_BIT 0
#define USBDMARClr_EP1_MASK 0x2
#define USBDMARClr_EP1 0x2
#define USBDMARClr_EP1_BIT 1
#define USBDMARClr_EP2_MASK 0x4
#define USBDMARClr_EP2 0x4
#define USBDMARClr_EP2_BIT 2
#define USBDMARClr_EP3_MASK 0x8
#define USBDMARClr_EP3 0x8
#define USBDMARClr_EP3_BIT 3
#define USBDMARClr_EP4_MASK 0x10
#define USBDMARClr_EP4 0x10
#define USBDMARClr_EP4_BIT 4
#define USBDMARClr_EP5_MASK 0x20
#define USBDMARClr_EP5 0x20
#define USBDMARClr_EP5_BIT 5
#define USBDMARClr_EP6_MASK 0x40
#define USBDMARClr_EP6 0x40
#define USBDMARClr_EP6_BIT 6
#define USBDMARClr_EP7_MASK 0x80
#define USBDMARClr_EP7 0x80
#define USBDMARClr_EP7_BIT 7
#define USBDMARClr_EP8_MASK 0x100
#define USBDMARClr_EP8 0x100
#define USBDMARClr_EP8_BIT 8
#define USBDMARClr_EP9_MASK 0x200
#define USBDMARClr_EP9 0x200
#define USBDMARClr_EP9_BIT 9
#define USBDMARClr_EP10_MASK 0x400
#define USBDMARClr_EP10 0x400
#define USBDMARClr_EP10_BIT 10
#define USBDMARClr_EP11_MASK 0x800
#define USBDMARClr_EP11 0x800
#define USBDMARClr_EP11_BIT 11
#define USBDMARClr_EP12_MASK 0x1000
#define USBDMARClr_EP12 0x1000
#define USBDMARClr_EP12_BIT 12
#define USBDMARClr_EP13_MASK 0x2000
#define USBDMARClr_EP13 0x2000
#define USBDMARClr_EP13_BIT 13
#define USBDMARClr_EP14_MASK 0x4000
#define USBDMARClr_EP14 0x4000
#define USBDMARClr_EP14_BIT 14
#define USBDMARClr_EP15_MASK 0x8000
#define USBDMARClr_EP15 0x8000
#define USBDMARClr_EP15_BIT 15
#define USBDMARClr_EP16_MASK 0x10000
#define USBDMARClr_EP16 0x10000
#define USBDMARClr_EP16_BIT 16
#define USBDMARClr_EP17_MASK 0x20000
#define USBDMARClr_EP17 0x20000
#define USBDMARClr_EP17_BIT 17
#define USBDMARClr_EP18_MASK 0x40000
#define USBDMARClr_EP18 0x40000
#define USBDMARClr_EP18_BIT 18
#define USBDMARClr_EP19_MASK 0x80000
#define USBDMARClr_EP19 0x80000
#define USBDMARClr_EP19_BIT 19
#define USBDMARClr_EP20_MASK 0x100000
#define USBDMARClr_EP20 0x100000
#define USBDMARClr_EP20_BIT 20
#define USBDMARClr_EP21_MASK 0x200000
#define USBDMARClr_EP21 0x200000
#define USBDMARClr_EP21_BIT 21
#define USBDMARClr_EP22_MASK 0x400000
#define USBDMARClr_EP22 0x400000
#define USBDMARClr_EP22_BIT 22
#define USBDMARClr_EP23_MASK 0x800000
#define USBDMARClr_EP23 0x800000
#define USBDMARClr_EP23_BIT 23
#define USBDMARClr_EP24_MASK 0x1000000
#define USBDMARClr_EP24 0x1000000
#define USBDMARClr_EP24_BIT 24
#define USBDMARClr_EP25_MASK 0x2000000
#define USBDMARClr_EP25 0x2000000
#define USBDMARClr_EP25_BIT 25
#define USBDMARClr_EP26_MASK 0x4000000
#define USBDMARClr_EP26 0x4000000
#define USBDMARClr_EP26_BIT 26
#define USBDMARClr_EP27_MASK 0x8000000
#define USBDMARClr_EP27 0x8000000
#define USBDMARClr_EP27_BIT 27
#define USBDMARClr_EP28_MASK 0x10000000
#define USBDMARClr_EP28 0x10000000
#define USBDMARClr_EP28_BIT 28
#define USBDMARClr_EP29_MASK 0x20000000
#define USBDMARClr_EP29 0x20000000
#define USBDMARClr_EP29_BIT 29
#define USBDMARClr_EP30_MASK 0x40000000
#define USBDMARClr_EP30 0x40000000
#define USBDMARClr_EP30_BIT 30
#define USBDMARClr_EP31_MASK 0x80000000
#define USBDMARClr_EP31 0x80000000
#define USBDMARClr_EP31_BIT 31

#define USBDMARSet (*(volatile unsigned *)0x31020258)
#define USBDMARSet_EP0_MASK 0x1
#define USBDMARSet_EP0 0x1
#define USBDMARSet_EP0_BIT 0
#define USBDMARSet_EP1_MASK 0x2
#define USBDMARSet_EP1 0x2
#define USBDMARSet_EP1_BIT 1
#define USBDMARSet_EP2_MASK 0x4
#define USBDMARSet_EP2 0x4
#define USBDMARSet_EP2_BIT 2
#define USBDMARSet_EP3_MASK 0x8
#define USBDMARSet_EP3 0x8
#define USBDMARSet_EP3_BIT 3
#define USBDMARSet_EP4_MASK 0x10
#define USBDMARSet_EP4 0x10
#define USBDMARSet_EP4_BIT 4
#define USBDMARSet_EP5_MASK 0x20
#define USBDMARSet_EP5 0x20
#define USBDMARSet_EP5_BIT 5
#define USBDMARSet_EP6_MASK 0x40
#define USBDMARSet_EP6 0x40
#define USBDMARSet_EP6_BIT 6
#define USBDMARSet_EP7_MASK 0x80
#define USBDMARSet_EP7 0x80
#define USBDMARSet_EP7_BIT 7
#define USBDMARSet_EP8_MASK 0x100
#define USBDMARSet_EP8 0x100
#define USBDMARSet_EP8_BIT 8
#define USBDMARSet_EP9_MASK 0x200
#define USBDMARSet_EP9 0x200
#define USBDMARSet_EP9_BIT 9
#define USBDMARSet_EP10_MASK 0x400
#define USBDMARSet_EP10 0x400
#define USBDMARSet_EP10_BIT 10
#define USBDMARSet_EP11_MASK 0x800
#define USBDMARSet_EP11 0x800
#define USBDMARSet_EP11_BIT 11
#define USBDMARSet_EP12_MASK 0x1000
#define USBDMARSet_EP12 0x1000
#define USBDMARSet_EP12_BIT 12
#define USBDMARSet_EP13_MASK 0x2000
#define USBDMARSet_EP13 0x2000
#define USBDMARSet_EP13_BIT 13
#define USBDMARSet_EP14_MASK 0x4000
#define USBDMARSet_EP14 0x4000
#define USBDMARSet_EP14_BIT 14
#define USBDMARSet_EP15_MASK 0x8000
#define USBDMARSet_EP15 0x8000
#define USBDMARSet_EP15_BIT 15
#define USBDMARSet_EP16_MASK 0x10000
#define USBDMARSet_EP16 0x10000
#define USBDMARSet_EP16_BIT 16
#define USBDMARSet_EP17_MASK 0x20000
#define USBDMARSet_EP17 0x20000
#define USBDMARSet_EP17_BIT 17
#define USBDMARSet_EP18_MASK 0x40000
#define USBDMARSet_EP18 0x40000
#define USBDMARSet_EP18_BIT 18
#define USBDMARSet_EP19_MASK 0x80000
#define USBDMARSet_EP19 0x80000
#define USBDMARSet_EP19_BIT 19
#define USBDMARSet_EP20_MASK 0x100000
#define USBDMARSet_EP20 0x100000
#define USBDMARSet_EP20_BIT 20
#define USBDMARSet_EP21_MASK 0x200000
#define USBDMARSet_EP21 0x200000
#define USBDMARSet_EP21_BIT 21
#define USBDMARSet_EP22_MASK 0x400000
#define USBDMARSet_EP22 0x400000
#define USBDMARSet_EP22_BIT 22
#define USBDMARSet_EP23_MASK 0x800000
#define USBDMARSet_EP23 0x800000
#define USBDMARSet_EP23_BIT 23
#define USBDMARSet_EP24_MASK 0x1000000
#define USBDMARSet_EP24 0x1000000
#define USBDMARSet_EP24_BIT 24
#define USBDMARSet_EP25_MASK 0x2000000
#define USBDMARSet_EP25 0x2000000
#define USBDMARSet_EP25_BIT 25
#define USBDMARSet_EP26_MASK 0x4000000
#define USBDMARSet_EP26 0x4000000
#define USBDMARSet_EP26_BIT 26
#define USBDMARSet_EP27_MASK 0x8000000
#define USBDMARSet_EP27 0x8000000
#define USBDMARSet_EP27_BIT 27
#define USBDMARSet_EP28_MASK 0x10000000
#define USBDMARSet_EP28 0x10000000
#define USBDMARSet_EP28_BIT 28
#define USBDMARSet_EP29_MASK 0x20000000
#define USBDMARSet_EP29 0x20000000
#define USBDMARSet_EP29_BIT 29
#define USBDMARSet_EP30_MASK 0x40000000
#define USBDMARSet_EP30 0x40000000
#define USBDMARSet_EP30_BIT 30
#define USBDMARSet_EP31_MASK 0x80000000
#define USBDMARSet_EP31 0x80000000
#define USBDMARSet_EP31_BIT 31

#define USBUDCAH (*(volatile unsigned *)0x31020280)
#define USBUDCAH_UDCA_Header_MASK 0xFFFFFF80
#define USBUDCAH_UDCA_Header_BIT 7

#define USBEpDMASt (*(volatile unsigned *)0x31020284)
#define USBEpDMASt_EP0_MASK 0x1
#define USBEpDMASt_EP0 0x1
#define USBEpDMASt_EP0_BIT 0
#define USBEpDMASt_EP1_MASK 0x2
#define USBEpDMASt_EP1 0x2
#define USBEpDMASt_EP1_BIT 1
#define USBEpDMASt_EP2_MASK 0x4
#define USBEpDMASt_EP2 0x4
#define USBEpDMASt_EP2_BIT 2
#define USBEpDMASt_EP3_MASK 0x8
#define USBEpDMASt_EP3 0x8
#define USBEpDMASt_EP3_BIT 3
#define USBEpDMASt_EP4_MASK 0x10
#define USBEpDMASt_EP4 0x10
#define USBEpDMASt_EP4_BIT 4
#define USBEpDMASt_EP5_MASK 0x20
#define USBEpDMASt_EP5 0x20
#define USBEpDMASt_EP5_BIT 5
#define USBEpDMASt_EP6_MASK 0x40
#define USBEpDMASt_EP6 0x40
#define USBEpDMASt_EP6_BIT 6
#define USBEpDMASt_EP7_MASK 0x80
#define USBEpDMASt_EP7 0x80
#define USBEpDMASt_EP7_BIT 7
#define USBEpDMASt_EP8_MASK 0x100
#define USBEpDMASt_EP8 0x100
#define USBEpDMASt_EP8_BIT 8
#define USBEpDMASt_EP9_MASK 0x200
#define USBEpDMASt_EP9 0x200
#define USBEpDMASt_EP9_BIT 9
#define USBEpDMASt_EP10_MASK 0x400
#define USBEpDMASt_EP10 0x400
#define USBEpDMASt_EP10_BIT 10
#define USBEpDMASt_EP11_MASK 0x800
#define USBEpDMASt_EP11 0x800
#define USBEpDMASt_EP11_BIT 11
#define USBEpDMASt_EP12_MASK 0x1000
#define USBEpDMASt_EP12 0x1000
#define USBEpDMASt_EP12_BIT 12
#define USBEpDMASt_EP13_MASK 0x2000
#define USBEpDMASt_EP13 0x2000
#define USBEpDMASt_EP13_BIT 13
#define USBEpDMASt_EP14_MASK 0x4000
#define USBEpDMASt_EP14 0x4000
#define USBEpDMASt_EP14_BIT 14
#define USBEpDMASt_EP15_MASK 0x8000
#define USBEpDMASt_EP15 0x8000
#define USBEpDMASt_EP15_BIT 15
#define USBEpDMASt_EP16_MASK 0x10000
#define USBEpDMASt_EP16 0x10000
#define USBEpDMASt_EP16_BIT 16
#define USBEpDMASt_EP17_MASK 0x20000
#define USBEpDMASt_EP17 0x20000
#define USBEpDMASt_EP17_BIT 17
#define USBEpDMASt_EP18_MASK 0x40000
#define USBEpDMASt_EP18 0x40000
#define USBEpDMASt_EP18_BIT 18
#define USBEpDMASt_EP19_MASK 0x80000
#define USBEpDMASt_EP19 0x80000
#define USBEpDMASt_EP19_BIT 19
#define USBEpDMASt_EP20_MASK 0x100000
#define USBEpDMASt_EP20 0x100000
#define USBEpDMASt_EP20_BIT 20
#define USBEpDMASt_EP21_MASK 0x200000
#define USBEpDMASt_EP21 0x200000
#define USBEpDMASt_EP21_BIT 21
#define USBEpDMASt_EP22_MASK 0x400000
#define USBEpDMASt_EP22 0x400000
#define USBEpDMASt_EP22_BIT 22
#define USBEpDMASt_EP23_MASK 0x800000
#define USBEpDMASt_EP23 0x800000
#define USBEpDMASt_EP23_BIT 23
#define USBEpDMASt_EP24_MASK 0x1000000
#define USBEpDMASt_EP24 0x1000000
#define USBEpDMASt_EP24_BIT 24
#define USBEpDMASt_EP25_MASK 0x2000000
#define USBEpDMASt_EP25 0x2000000
#define USBEpDMASt_EP25_BIT 25
#define USBEpDMASt_EP26_MASK 0x4000000
#define USBEpDMASt_EP26 0x4000000
#define USBEpDMASt_EP26_BIT 26
#define USBEpDMASt_EP27_MASK 0x8000000
#define USBEpDMASt_EP27 0x8000000
#define USBEpDMASt_EP27_BIT 27
#define USBEpDMASt_EP28_MASK 0x10000000
#define USBEpDMASt_EP28 0x10000000
#define USBEpDMASt_EP28_BIT 28
#define USBEpDMASt_EP29_MASK 0x20000000
#define USBEpDMASt_EP29 0x20000000
#define USBEpDMASt_EP29_BIT 29
#define USBEpDMASt_EP30_MASK 0x40000000
#define USBEpDMASt_EP30 0x40000000
#define USBEpDMASt_EP30_BIT 30
#define USBEpDMASt_EP31_MASK 0x80000000
#define USBEpDMASt_EP31 0x80000000
#define USBEpDMASt_EP31_BIT 31

#define USBEpDMAEn (*(volatile unsigned *)0x31020288)
#define USBEpDMAEn_EP0_MASK 0x1
#define USBEpDMAEn_EP0 0x1
#define USBEpDMAEn_EP0_BIT 0
#define USBEpDMAEn_EP1_MASK 0x2
#define USBEpDMAEn_EP1 0x2
#define USBEpDMAEn_EP1_BIT 1
#define USBEpDMAEn_EP2_MASK 0x4
#define USBEpDMAEn_EP2 0x4
#define USBEpDMAEn_EP2_BIT 2
#define USBEpDMAEn_EP3_MASK 0x8
#define USBEpDMAEn_EP3 0x8
#define USBEpDMAEn_EP3_BIT 3
#define USBEpDMAEn_EP4_MASK 0x10
#define USBEpDMAEn_EP4 0x10
#define USBEpDMAEn_EP4_BIT 4
#define USBEpDMAEn_EP5_MASK 0x20
#define USBEpDMAEn_EP5 0x20
#define USBEpDMAEn_EP5_BIT 5
#define USBEpDMAEn_EP6_MASK 0x40
#define USBEpDMAEn_EP6 0x40
#define USBEpDMAEn_EP6_BIT 6
#define USBEpDMAEn_EP7_MASK 0x80
#define USBEpDMAEn_EP7 0x80
#define USBEpDMAEn_EP7_BIT 7
#define USBEpDMAEn_EP8_MASK 0x100
#define USBEpDMAEn_EP8 0x100
#define USBEpDMAEn_EP8_BIT 8
#define USBEpDMAEn_EP9_MASK 0x200
#define USBEpDMAEn_EP9 0x200
#define USBEpDMAEn_EP9_BIT 9
#define USBEpDMAEn_EP10_MASK 0x400
#define USBEpDMAEn_EP10 0x400
#define USBEpDMAEn_EP10_BIT 10
#define USBEpDMAEn_EP11_MASK 0x800
#define USBEpDMAEn_EP11 0x800
#define USBEpDMAEn_EP11_BIT 11
#define USBEpDMAEn_EP12_MASK 0x1000
#define USBEpDMAEn_EP12 0x1000
#define USBEpDMAEn_EP12_BIT 12
#define USBEpDMAEn_EP13_MASK 0x2000
#define USBEpDMAEn_EP13 0x2000
#define USBEpDMAEn_EP13_BIT 13
#define USBEpDMAEn_EP14_MASK 0x4000
#define USBEpDMAEn_EP14 0x4000
#define USBEpDMAEn_EP14_BIT 14
#define USBEpDMAEn_EP15_MASK 0x8000
#define USBEpDMAEn_EP15 0x8000
#define USBEpDMAEn_EP15_BIT 15
#define USBEpDMAEn_EP16_MASK 0x10000
#define USBEpDMAEn_EP16 0x10000
#define USBEpDMAEn_EP16_BIT 16
#define USBEpDMAEn_EP17_MASK 0x20000
#define USBEpDMAEn_EP17 0x20000
#define USBEpDMAEn_EP17_BIT 17
#define USBEpDMAEn_EP18_MASK 0x40000
#define USBEpDMAEn_EP18 0x40000
#define USBEpDMAEn_EP18_BIT 18
#define USBEpDMAEn_EP19_MASK 0x80000
#define USBEpDMAEn_EP19 0x80000
#define USBEpDMAEn_EP19_BIT 19
#define USBEpDMAEn_EP20_MASK 0x100000
#define USBEpDMAEn_EP20 0x100000
#define USBEpDMAEn_EP20_BIT 20
#define USBEpDMAEn_EP21_MASK 0x200000
#define USBEpDMAEn_EP21 0x200000
#define USBEpDMAEn_EP21_BIT 21
#define USBEpDMAEn_EP22_MASK 0x400000
#define USBEpDMAEn_EP22 0x400000
#define USBEpDMAEn_EP22_BIT 22
#define USBEpDMAEn_EP23_MASK 0x800000
#define USBEpDMAEn_EP23 0x800000
#define USBEpDMAEn_EP23_BIT 23
#define USBEpDMAEn_EP24_MASK 0x1000000
#define USBEpDMAEn_EP24 0x1000000
#define USBEpDMAEn_EP24_BIT 24
#define USBEpDMAEn_EP25_MASK 0x2000000
#define USBEpDMAEn_EP25 0x2000000
#define USBEpDMAEn_EP25_BIT 25
#define USBEpDMAEn_EP26_MASK 0x4000000
#define USBEpDMAEn_EP26 0x4000000
#define USBEpDMAEn_EP26_BIT 26
#define USBEpDMAEn_EP27_MASK 0x8000000
#define USBEpDMAEn_EP27 0x8000000
#define USBEpDMAEn_EP27_BIT 27
#define USBEpDMAEn_EP28_MASK 0x10000000
#define USBEpDMAEn_EP28 0x10000000
#define USBEpDMAEn_EP28_BIT 28
#define USBEpDMAEn_EP29_MASK 0x20000000
#define USBEpDMAEn_EP29 0x20000000
#define USBEpDMAEn_EP29_BIT 29
#define USBEpDMAEn_EP30_MASK 0x40000000
#define USBEpDMAEn_EP30 0x40000000
#define USBEpDMAEn_EP30_BIT 30
#define USBEpDMAEn_EP31_MASK 0x80000000
#define USBEpDMAEn_EP31 0x80000000
#define USBEpDMAEn_EP31_BIT 31

#define USBEpDMADis (*(volatile unsigned *)0x3102028C)
#define USBEpDMADis_EP0_MASK 0x1
#define USBEpDMADis_EP0 0x1
#define USBEpDMADis_EP0_BIT 0
#define USBEpDMADis_EP1_MASK 0x2
#define USBEpDMADis_EP1 0x2
#define USBEpDMADis_EP1_BIT 1
#define USBEpDMADis_EP2_MASK 0x4
#define USBEpDMADis_EP2 0x4
#define USBEpDMADis_EP2_BIT 2
#define USBEpDMADis_EP3_MASK 0x8
#define USBEpDMADis_EP3 0x8
#define USBEpDMADis_EP3_BIT 3
#define USBEpDMADis_EP4_MASK 0x10
#define USBEpDMADis_EP4 0x10
#define USBEpDMADis_EP4_BIT 4
#define USBEpDMADis_EP5_MASK 0x20
#define USBEpDMADis_EP5 0x20
#define USBEpDMADis_EP5_BIT 5
#define USBEpDMADis_EP6_MASK 0x40
#define USBEpDMADis_EP6 0x40
#define USBEpDMADis_EP6_BIT 6
#define USBEpDMADis_EP7_MASK 0x80
#define USBEpDMADis_EP7 0x80
#define USBEpDMADis_EP7_BIT 7
#define USBEpDMADis_EP8_MASK 0x100
#define USBEpDMADis_EP8 0x100
#define USBEpDMADis_EP8_BIT 8
#define USBEpDMADis_EP9_MASK 0x200
#define USBEpDMADis_EP9 0x200
#define USBEpDMADis_EP9_BIT 9
#define USBEpDMADis_EP10_MASK 0x400
#define USBEpDMADis_EP10 0x400
#define USBEpDMADis_EP10_BIT 10
#define USBEpDMADis_EP11_MASK 0x800
#define USBEpDMADis_EP11 0x800
#define USBEpDMADis_EP11_BIT 11
#define USBEpDMADis_EP12_MASK 0x1000
#define USBEpDMADis_EP12 0x1000
#define USBEpDMADis_EP12_BIT 12
#define USBEpDMADis_EP13_MASK 0x2000
#define USBEpDMADis_EP13 0x2000
#define USBEpDMADis_EP13_BIT 13
#define USBEpDMADis_EP14_MASK 0x4000
#define USBEpDMADis_EP14 0x4000
#define USBEpDMADis_EP14_BIT 14
#define USBEpDMADis_EP15_MASK 0x8000
#define USBEpDMADis_EP15 0x8000
#define USBEpDMADis_EP15_BIT 15
#define USBEpDMADis_EP16_MASK 0x10000
#define USBEpDMADis_EP16 0x10000
#define USBEpDMADis_EP16_BIT 16
#define USBEpDMADis_EP17_MASK 0x20000
#define USBEpDMADis_EP17 0x20000
#define USBEpDMADis_EP17_BIT 17
#define USBEpDMADis_EP18_MASK 0x40000
#define USBEpDMADis_EP18 0x40000
#define USBEpDMADis_EP18_BIT 18
#define USBEpDMADis_EP19_MASK 0x80000
#define USBEpDMADis_EP19 0x80000
#define USBEpDMADis_EP19_BIT 19
#define USBEpDMADis_EP20_MASK 0x100000
#define USBEpDMADis_EP20 0x100000
#define USBEpDMADis_EP20_BIT 20
#define USBEpDMADis_EP21_MASK 0x200000
#define USBEpDMADis_EP21 0x200000
#define USBEpDMADis_EP21_BIT 21
#define USBEpDMADis_EP22_MASK 0x400000
#define USBEpDMADis_EP22 0x400000
#define USBEpDMADis_EP22_BIT 22
#define USBEpDMADis_EP23_MASK 0x800000
#define USBEpDMADis_EP23 0x800000
#define USBEpDMADis_EP23_BIT 23
#define USBEpDMADis_EP24_MASK 0x1000000
#define USBEpDMADis_EP24 0x1000000
#define USBEpDMADis_EP24_BIT 24
#define USBEpDMADis_EP25_MASK 0x2000000
#define USBEpDMADis_EP25 0x2000000
#define USBEpDMADis_EP25_BIT 25
#define USBEpDMADis_EP26_MASK 0x4000000
#define USBEpDMADis_EP26 0x4000000
#define USBEpDMADis_EP26_BIT 26
#define USBEpDMADis_EP27_MASK 0x8000000
#define USBEpDMADis_EP27 0x8000000
#define USBEpDMADis_EP27_BIT 27
#define USBEpDMADis_EP28_MASK 0x10000000
#define USBEpDMADis_EP28 0x10000000
#define USBEpDMADis_EP28_BIT 28
#define USBEpDMADis_EP29_MASK 0x20000000
#define USBEpDMADis_EP29 0x20000000
#define USBEpDMADis_EP29_BIT 29
#define USBEpDMADis_EP30_MASK 0x40000000
#define USBEpDMADis_EP30 0x40000000
#define USBEpDMADis_EP30_BIT 30
#define USBEpDMADis_EP31_MASK 0x80000000
#define USBEpDMADis_EP31 0x80000000
#define USBEpDMADis_EP31_BIT 31

#define USBDMAIntSt (*(volatile unsigned *)0x31020290)
#define USBDMAIntSt_System_Error_Interrupt_MASK 0x4
#define USBDMAIntSt_System_Error_Interrupt 0x4
#define USBDMAIntSt_System_Error_Interrupt_BIT 2
#define USBDMAIntSt_New_DD_Request_Interrupt_MASK 0x2
#define USBDMAIntSt_New_DD_Request_Interrupt 0x2
#define USBDMAIntSt_New_DD_Request_Interrupt_BIT 1
#define USBDMAIntSt_End_of_Transfer_Interrupt_MASK 0x1
#define USBDMAIntSt_End_of_Transfer_Interrupt 0x1
#define USBDMAIntSt_End_of_Transfer_Interrupt_BIT 0

#define USBDMAIntEn (*(volatile unsigned *)0x31020294)
#define USBDMAIntEn_System_Error_Interrupt_MASK 0x4
#define USBDMAIntEn_System_Error_Interrupt 0x4
#define USBDMAIntEn_System_Error_Interrupt_BIT 2
#define USBDMAIntEn_New_DD_Request_Interrupt_MASK 0x2
#define USBDMAIntEn_New_DD_Request_Interrupt 0x2
#define USBDMAIntEn_New_DD_Request_Interrupt_BIT 1
#define USBDMAIntEn_End_of_Transfer_Interrupt_MASK 0x1
#define USBDMAIntEn_End_of_Transfer_Interrupt 0x1
#define USBDMAIntEn_End_of_Transfer_Interrupt_BIT 0

#define USBEoTIntSt (*(volatile unsigned *)0x310202A0)
#define USBEoTIntSt_EP0_MASK 0x1
#define USBEoTIntSt_EP0 0x1
#define USBEoTIntSt_EP0_BIT 0
#define USBEoTIntSt_EP1_MASK 0x2
#define USBEoTIntSt_EP1 0x2
#define USBEoTIntSt_EP1_BIT 1
#define USBEoTIntSt_EP2_MASK 0x4
#define USBEoTIntSt_EP2 0x4
#define USBEoTIntSt_EP2_BIT 2
#define USBEoTIntSt_EP3_MASK 0x8
#define USBEoTIntSt_EP3 0x8
#define USBEoTIntSt_EP3_BIT 3
#define USBEoTIntSt_EP4_MASK 0x10
#define USBEoTIntSt_EP4 0x10
#define USBEoTIntSt_EP4_BIT 4
#define USBEoTIntSt_EP5_MASK 0x20
#define USBEoTIntSt_EP5 0x20
#define USBEoTIntSt_EP5_BIT 5
#define USBEoTIntSt_EP6_MASK 0x40
#define USBEoTIntSt_EP6 0x40
#define USBEoTIntSt_EP6_BIT 6
#define USBEoTIntSt_EP7_MASK 0x80
#define USBEoTIntSt_EP7 0x80
#define USBEoTIntSt_EP7_BIT 7
#define USBEoTIntSt_EP8_MASK 0x100
#define USBEoTIntSt_EP8 0x100
#define USBEoTIntSt_EP8_BIT 8
#define USBEoTIntSt_EP9_MASK 0x200
#define USBEoTIntSt_EP9 0x200
#define USBEoTIntSt_EP9_BIT 9
#define USBEoTIntSt_EP10_MASK 0x400
#define USBEoTIntSt_EP10 0x400
#define USBEoTIntSt_EP10_BIT 10
#define USBEoTIntSt_EP11_MASK 0x800
#define USBEoTIntSt_EP11 0x800
#define USBEoTIntSt_EP11_BIT 11
#define USBEoTIntSt_EP12_MASK 0x1000
#define USBEoTIntSt_EP12 0x1000
#define USBEoTIntSt_EP12_BIT 12
#define USBEoTIntSt_EP13_MASK 0x2000
#define USBEoTIntSt_EP13 0x2000
#define USBEoTIntSt_EP13_BIT 13
#define USBEoTIntSt_EP14_MASK 0x4000
#define USBEoTIntSt_EP14 0x4000
#define USBEoTIntSt_EP14_BIT 14
#define USBEoTIntSt_EP15_MASK 0x8000
#define USBEoTIntSt_EP15 0x8000
#define USBEoTIntSt_EP15_BIT 15
#define USBEoTIntSt_EP16_MASK 0x10000
#define USBEoTIntSt_EP16 0x10000
#define USBEoTIntSt_EP16_BIT 16
#define USBEoTIntSt_EP17_MASK 0x20000
#define USBEoTIntSt_EP17 0x20000
#define USBEoTIntSt_EP17_BIT 17
#define USBEoTIntSt_EP18_MASK 0x40000
#define USBEoTIntSt_EP18 0x40000
#define USBEoTIntSt_EP18_BIT 18
#define USBEoTIntSt_EP19_MASK 0x80000
#define USBEoTIntSt_EP19 0x80000
#define USBEoTIntSt_EP19_BIT 19
#define USBEoTIntSt_EP20_MASK 0x100000
#define USBEoTIntSt_EP20 0x100000
#define USBEoTIntSt_EP20_BIT 20
#define USBEoTIntSt_EP21_MASK 0x200000
#define USBEoTIntSt_EP21 0x200000
#define USBEoTIntSt_EP21_BIT 21
#define USBEoTIntSt_EP22_MASK 0x400000
#define USBEoTIntSt_EP22 0x400000
#define USBEoTIntSt_EP22_BIT 22
#define USBEoTIntSt_EP23_MASK 0x800000
#define USBEoTIntSt_EP23 0x800000
#define USBEoTIntSt_EP23_BIT 23
#define USBEoTIntSt_EP24_MASK 0x1000000
#define USBEoTIntSt_EP24 0x1000000
#define USBEoTIntSt_EP24_BIT 24
#define USBEoTIntSt_EP25_MASK 0x2000000
#define USBEoTIntSt_EP25 0x2000000
#define USBEoTIntSt_EP25_BIT 25
#define USBEoTIntSt_EP26_MASK 0x4000000
#define USBEoTIntSt_EP26 0x4000000
#define USBEoTIntSt_EP26_BIT 26
#define USBEoTIntSt_EP27_MASK 0x8000000
#define USBEoTIntSt_EP27 0x8000000
#define USBEoTIntSt_EP27_BIT 27
#define USBEoTIntSt_EP28_MASK 0x10000000
#define USBEoTIntSt_EP28 0x10000000
#define USBEoTIntSt_EP28_BIT 28
#define USBEoTIntSt_EP29_MASK 0x20000000
#define USBEoTIntSt_EP29 0x20000000
#define USBEoTIntSt_EP29_BIT 29
#define USBEoTIntSt_EP30_MASK 0x40000000
#define USBEoTIntSt_EP30 0x40000000
#define USBEoTIntSt_EP30_BIT 30
#define USBEoTIntSt_EP31_MASK 0x80000000
#define USBEoTIntSt_EP31 0x80000000
#define USBEoTIntSt_EP31_BIT 31

#define USBEoTIntClr (*(volatile unsigned *)0x310202A4)
#define USBEoTIntClr_EP0_MASK 0x1
#define USBEoTIntClr_EP0 0x1
#define USBEoTIntClr_EP0_BIT 0
#define USBEoTIntClr_EP1_MASK 0x2
#define USBEoTIntClr_EP1 0x2
#define USBEoTIntClr_EP1_BIT 1
#define USBEoTIntClr_EP2_MASK 0x4
#define USBEoTIntClr_EP2 0x4
#define USBEoTIntClr_EP2_BIT 2
#define USBEoTIntClr_EP3_MASK 0x8
#define USBEoTIntClr_EP3 0x8
#define USBEoTIntClr_EP3_BIT 3
#define USBEoTIntClr_EP4_MASK 0x10
#define USBEoTIntClr_EP4 0x10
#define USBEoTIntClr_EP4_BIT 4
#define USBEoTIntClr_EP5_MASK 0x20
#define USBEoTIntClr_EP5 0x20
#define USBEoTIntClr_EP5_BIT 5
#define USBEoTIntClr_EP6_MASK 0x40
#define USBEoTIntClr_EP6 0x40
#define USBEoTIntClr_EP6_BIT 6
#define USBEoTIntClr_EP7_MASK 0x80
#define USBEoTIntClr_EP7 0x80
#define USBEoTIntClr_EP7_BIT 7
#define USBEoTIntClr_EP8_MASK 0x100
#define USBEoTIntClr_EP8 0x100
#define USBEoTIntClr_EP8_BIT 8
#define USBEoTIntClr_EP9_MASK 0x200
#define USBEoTIntClr_EP9 0x200
#define USBEoTIntClr_EP9_BIT 9
#define USBEoTIntClr_EP10_MASK 0x400
#define USBEoTIntClr_EP10 0x400
#define USBEoTIntClr_EP10_BIT 10
#define USBEoTIntClr_EP11_MASK 0x800
#define USBEoTIntClr_EP11 0x800
#define USBEoTIntClr_EP11_BIT 11
#define USBEoTIntClr_EP12_MASK 0x1000
#define USBEoTIntClr_EP12 0x1000
#define USBEoTIntClr_EP12_BIT 12
#define USBEoTIntClr_EP13_MASK 0x2000
#define USBEoTIntClr_EP13 0x2000
#define USBEoTIntClr_EP13_BIT 13
#define USBEoTIntClr_EP14_MASK 0x4000
#define USBEoTIntClr_EP14 0x4000
#define USBEoTIntClr_EP14_BIT 14
#define USBEoTIntClr_EP15_MASK 0x8000
#define USBEoTIntClr_EP15 0x8000
#define USBEoTIntClr_EP15_BIT 15
#define USBEoTIntClr_EP16_MASK 0x10000
#define USBEoTIntClr_EP16 0x10000
#define USBEoTIntClr_EP16_BIT 16
#define USBEoTIntClr_EP17_MASK 0x20000
#define USBEoTIntClr_EP17 0x20000
#define USBEoTIntClr_EP17_BIT 17
#define USBEoTIntClr_EP18_MASK 0x40000
#define USBEoTIntClr_EP18 0x40000
#define USBEoTIntClr_EP18_BIT 18
#define USBEoTIntClr_EP19_MASK 0x80000
#define USBEoTIntClr_EP19 0x80000
#define USBEoTIntClr_EP19_BIT 19
#define USBEoTIntClr_EP20_MASK 0x100000
#define USBEoTIntClr_EP20 0x100000
#define USBEoTIntClr_EP20_BIT 20
#define USBEoTIntClr_EP21_MASK 0x200000
#define USBEoTIntClr_EP21 0x200000
#define USBEoTIntClr_EP21_BIT 21
#define USBEoTIntClr_EP22_MASK 0x400000
#define USBEoTIntClr_EP22 0x400000
#define USBEoTIntClr_EP22_BIT 22
#define USBEoTIntClr_EP23_MASK 0x800000
#define USBEoTIntClr_EP23 0x800000
#define USBEoTIntClr_EP23_BIT 23
#define USBEoTIntClr_EP24_MASK 0x1000000
#define USBEoTIntClr_EP24 0x1000000
#define USBEoTIntClr_EP24_BIT 24
#define USBEoTIntClr_EP25_MASK 0x2000000
#define USBEoTIntClr_EP25 0x2000000
#define USBEoTIntClr_EP25_BIT 25
#define USBEoTIntClr_EP26_MASK 0x4000000
#define USBEoTIntClr_EP26 0x4000000
#define USBEoTIntClr_EP26_BIT 26
#define USBEoTIntClr_EP27_MASK 0x8000000
#define USBEoTIntClr_EP27 0x8000000
#define USBEoTIntClr_EP27_BIT 27
#define USBEoTIntClr_EP28_MASK 0x10000000
#define USBEoTIntClr_EP28 0x10000000
#define USBEoTIntClr_EP28_BIT 28
#define USBEoTIntClr_EP29_MASK 0x20000000
#define USBEoTIntClr_EP29 0x20000000
#define USBEoTIntClr_EP29_BIT 29
#define USBEoTIntClr_EP30_MASK 0x40000000
#define USBEoTIntClr_EP30 0x40000000
#define USBEoTIntClr_EP30_BIT 30
#define USBEoTIntClr_EP31_MASK 0x80000000
#define USBEoTIntClr_EP31 0x80000000
#define USBEoTIntClr_EP31_BIT 31

#define USBEoTIntSet (*(volatile unsigned *)0x310202A8)
#define USBEoTIntSet_EP0_MASK 0x1
#define USBEoTIntSet_EP0 0x1
#define USBEoTIntSet_EP0_BIT 0
#define USBEoTIntSet_EP1_MASK 0x2
#define USBEoTIntSet_EP1 0x2
#define USBEoTIntSet_EP1_BIT 1
#define USBEoTIntSet_EP2_MASK 0x4
#define USBEoTIntSet_EP2 0x4
#define USBEoTIntSet_EP2_BIT 2
#define USBEoTIntSet_EP3_MASK 0x8
#define USBEoTIntSet_EP3 0x8
#define USBEoTIntSet_EP3_BIT 3
#define USBEoTIntSet_EP4_MASK 0x10
#define USBEoTIntSet_EP4 0x10
#define USBEoTIntSet_EP4_BIT 4
#define USBEoTIntSet_EP5_MASK 0x20
#define USBEoTIntSet_EP5 0x20
#define USBEoTIntSet_EP5_BIT 5
#define USBEoTIntSet_EP6_MASK 0x40
#define USBEoTIntSet_EP6 0x40
#define USBEoTIntSet_EP6_BIT 6
#define USBEoTIntSet_EP7_MASK 0x80
#define USBEoTIntSet_EP7 0x80
#define USBEoTIntSet_EP7_BIT 7
#define USBEoTIntSet_EP8_MASK 0x100
#define USBEoTIntSet_EP8 0x100
#define USBEoTIntSet_EP8_BIT 8
#define USBEoTIntSet_EP9_MASK 0x200
#define USBEoTIntSet_EP9 0x200
#define USBEoTIntSet_EP9_BIT 9
#define USBEoTIntSet_EP10_MASK 0x400
#define USBEoTIntSet_EP10 0x400
#define USBEoTIntSet_EP10_BIT 10
#define USBEoTIntSet_EP11_MASK 0x800
#define USBEoTIntSet_EP11 0x800
#define USBEoTIntSet_EP11_BIT 11
#define USBEoTIntSet_EP12_MASK 0x1000
#define USBEoTIntSet_EP12 0x1000
#define USBEoTIntSet_EP12_BIT 12
#define USBEoTIntSet_EP13_MASK 0x2000
#define USBEoTIntSet_EP13 0x2000
#define USBEoTIntSet_EP13_BIT 13
#define USBEoTIntSet_EP14_MASK 0x4000
#define USBEoTIntSet_EP14 0x4000
#define USBEoTIntSet_EP14_BIT 14
#define USBEoTIntSet_EP15_MASK 0x8000
#define USBEoTIntSet_EP15 0x8000
#define USBEoTIntSet_EP15_BIT 15
#define USBEoTIntSet_EP16_MASK 0x10000
#define USBEoTIntSet_EP16 0x10000
#define USBEoTIntSet_EP16_BIT 16
#define USBEoTIntSet_EP17_MASK 0x20000
#define USBEoTIntSet_EP17 0x20000
#define USBEoTIntSet_EP17_BIT 17
#define USBEoTIntSet_EP18_MASK 0x40000
#define USBEoTIntSet_EP18 0x40000
#define USBEoTIntSet_EP18_BIT 18
#define USBEoTIntSet_EP19_MASK 0x80000
#define USBEoTIntSet_EP19 0x80000
#define USBEoTIntSet_EP19_BIT 19
#define USBEoTIntSet_EP20_MASK 0x100000
#define USBEoTIntSet_EP20 0x100000
#define USBEoTIntSet_EP20_BIT 20
#define USBEoTIntSet_EP21_MASK 0x200000
#define USBEoTIntSet_EP21 0x200000
#define USBEoTIntSet_EP21_BIT 21
#define USBEoTIntSet_EP22_MASK 0x400000
#define USBEoTIntSet_EP22 0x400000
#define USBEoTIntSet_EP22_BIT 22
#define USBEoTIntSet_EP23_MASK 0x800000
#define USBEoTIntSet_EP23 0x800000
#define USBEoTIntSet_EP23_BIT 23
#define USBEoTIntSet_EP24_MASK 0x1000000
#define USBEoTIntSet_EP24 0x1000000
#define USBEoTIntSet_EP24_BIT 24
#define USBEoTIntSet_EP25_MASK 0x2000000
#define USBEoTIntSet_EP25 0x2000000
#define USBEoTIntSet_EP25_BIT 25
#define USBEoTIntSet_EP26_MASK 0x4000000
#define USBEoTIntSet_EP26 0x4000000
#define USBEoTIntSet_EP26_BIT 26
#define USBEoTIntSet_EP27_MASK 0x8000000
#define USBEoTIntSet_EP27 0x8000000
#define USBEoTIntSet_EP27_BIT 27
#define USBEoTIntSet_EP28_MASK 0x10000000
#define USBEoTIntSet_EP28 0x10000000
#define USBEoTIntSet_EP28_BIT 28
#define USBEoTIntSet_EP29_MASK 0x20000000
#define USBEoTIntSet_EP29 0x20000000
#define USBEoTIntSet_EP29_BIT 29
#define USBEoTIntSet_EP30_MASK 0x40000000
#define USBEoTIntSet_EP30 0x40000000
#define USBEoTIntSet_EP30_BIT 30
#define USBEoTIntSet_EP31_MASK 0x80000000
#define USBEoTIntSet_EP31 0x80000000
#define USBEoTIntSet_EP31_BIT 31

#define USBNDDRIntSt (*(volatile unsigned *)0x310202AC)
#define USBNDDRIntSt_EP0_MASK 0x1
#define USBNDDRIntSt_EP0 0x1
#define USBNDDRIntSt_EP0_BIT 0
#define USBNDDRIntSt_EP1_MASK 0x2
#define USBNDDRIntSt_EP1 0x2
#define USBNDDRIntSt_EP1_BIT 1
#define USBNDDRIntSt_EP2_MASK 0x4
#define USBNDDRIntSt_EP2 0x4
#define USBNDDRIntSt_EP2_BIT 2
#define USBNDDRIntSt_EP3_MASK 0x8
#define USBNDDRIntSt_EP3 0x8
#define USBNDDRIntSt_EP3_BIT 3
#define USBNDDRIntSt_EP4_MASK 0x10
#define USBNDDRIntSt_EP4 0x10
#define USBNDDRIntSt_EP4_BIT 4
#define USBNDDRIntSt_EP5_MASK 0x20
#define USBNDDRIntSt_EP5 0x20
#define USBNDDRIntSt_EP5_BIT 5
#define USBNDDRIntSt_EP6_MASK 0x40
#define USBNDDRIntSt_EP6 0x40
#define USBNDDRIntSt_EP6_BIT 6
#define USBNDDRIntSt_EP7_MASK 0x80
#define USBNDDRIntSt_EP7 0x80
#define USBNDDRIntSt_EP7_BIT 7
#define USBNDDRIntSt_EP8_MASK 0x100
#define USBNDDRIntSt_EP8 0x100
#define USBNDDRIntSt_EP8_BIT 8
#define USBNDDRIntSt_EP9_MASK 0x200
#define USBNDDRIntSt_EP9 0x200
#define USBNDDRIntSt_EP9_BIT 9
#define USBNDDRIntSt_EP10_MASK 0x400
#define USBNDDRIntSt_EP10 0x400
#define USBNDDRIntSt_EP10_BIT 10
#define USBNDDRIntSt_EP11_MASK 0x800
#define USBNDDRIntSt_EP11 0x800
#define USBNDDRIntSt_EP11_BIT 11
#define USBNDDRIntSt_EP12_MASK 0x1000
#define USBNDDRIntSt_EP12 0x1000
#define USBNDDRIntSt_EP12_BIT 12
#define USBNDDRIntSt_EP13_MASK 0x2000
#define USBNDDRIntSt_EP13 0x2000
#define USBNDDRIntSt_EP13_BIT 13
#define USBNDDRIntSt_EP14_MASK 0x4000
#define USBNDDRIntSt_EP14 0x4000
#define USBNDDRIntSt_EP14_BIT 14
#define USBNDDRIntSt_EP15_MASK 0x8000
#define USBNDDRIntSt_EP15 0x8000
#define USBNDDRIntSt_EP15_BIT 15
#define USBNDDRIntSt_EP16_MASK 0x10000
#define USBNDDRIntSt_EP16 0x10000
#define USBNDDRIntSt_EP16_BIT 16
#define USBNDDRIntSt_EP17_MASK 0x20000
#define USBNDDRIntSt_EP17 0x20000
#define USBNDDRIntSt_EP17_BIT 17
#define USBNDDRIntSt_EP18_MASK 0x40000
#define USBNDDRIntSt_EP18 0x40000
#define USBNDDRIntSt_EP18_BIT 18
#define USBNDDRIntSt_EP19_MASK 0x80000
#define USBNDDRIntSt_EP19 0x80000
#define USBNDDRIntSt_EP19_BIT 19
#define USBNDDRIntSt_EP20_MASK 0x100000
#define USBNDDRIntSt_EP20 0x100000
#define USBNDDRIntSt_EP20_BIT 20
#define USBNDDRIntSt_EP21_MASK 0x200000
#define USBNDDRIntSt_EP21 0x200000
#define USBNDDRIntSt_EP21_BIT 21
#define USBNDDRIntSt_EP22_MASK 0x400000
#define USBNDDRIntSt_EP22 0x400000
#define USBNDDRIntSt_EP22_BIT 22
#define USBNDDRIntSt_EP23_MASK 0x800000
#define USBNDDRIntSt_EP23 0x800000
#define USBNDDRIntSt_EP23_BIT 23
#define USBNDDRIntSt_EP24_MASK 0x1000000
#define USBNDDRIntSt_EP24 0x1000000
#define USBNDDRIntSt_EP24_BIT 24
#define USBNDDRIntSt_EP25_MASK 0x2000000
#define USBNDDRIntSt_EP25 0x2000000
#define USBNDDRIntSt_EP25_BIT 25
#define USBNDDRIntSt_EP26_MASK 0x4000000
#define USBNDDRIntSt_EP26 0x4000000
#define USBNDDRIntSt_EP26_BIT 26
#define USBNDDRIntSt_EP27_MASK 0x8000000
#define USBNDDRIntSt_EP27 0x8000000
#define USBNDDRIntSt_EP27_BIT 27
#define USBNDDRIntSt_EP28_MASK 0x10000000
#define USBNDDRIntSt_EP28 0x10000000
#define USBNDDRIntSt_EP28_BIT 28
#define USBNDDRIntSt_EP29_MASK 0x20000000
#define USBNDDRIntSt_EP29 0x20000000
#define USBNDDRIntSt_EP29_BIT 29
#define USBNDDRIntSt_EP30_MASK 0x40000000
#define USBNDDRIntSt_EP30 0x40000000
#define USBNDDRIntSt_EP30_BIT 30
#define USBNDDRIntSt_EP31_MASK 0x80000000
#define USBNDDRIntSt_EP31 0x80000000
#define USBNDDRIntSt_EP31_BIT 31

#define USBNDDRIntClr (*(volatile unsigned *)0x310202B0)
#define USBNDDRIntClr_EP0_MASK 0x1
#define USBNDDRIntClr_EP0 0x1
#define USBNDDRIntClr_EP0_BIT 0
#define USBNDDRIntClr_EP1_MASK 0x2
#define USBNDDRIntClr_EP1 0x2
#define USBNDDRIntClr_EP1_BIT 1
#define USBNDDRIntClr_EP2_MASK 0x4
#define USBNDDRIntClr_EP2 0x4
#define USBNDDRIntClr_EP2_BIT 2
#define USBNDDRIntClr_EP3_MASK 0x8
#define USBNDDRIntClr_EP3 0x8
#define USBNDDRIntClr_EP3_BIT 3
#define USBNDDRIntClr_EP4_MASK 0x10
#define USBNDDRIntClr_EP4 0x10
#define USBNDDRIntClr_EP4_BIT 4
#define USBNDDRIntClr_EP5_MASK 0x20
#define USBNDDRIntClr_EP5 0x20
#define USBNDDRIntClr_EP5_BIT 5
#define USBNDDRIntClr_EP6_MASK 0x40
#define USBNDDRIntClr_EP6 0x40
#define USBNDDRIntClr_EP6_BIT 6
#define USBNDDRIntClr_EP7_MASK 0x80
#define USBNDDRIntClr_EP7 0x80
#define USBNDDRIntClr_EP7_BIT 7
#define USBNDDRIntClr_EP8_MASK 0x100
#define USBNDDRIntClr_EP8 0x100
#define USBNDDRIntClr_EP8_BIT 8
#define USBNDDRIntClr_EP9_MASK 0x200
#define USBNDDRIntClr_EP9 0x200
#define USBNDDRIntClr_EP9_BIT 9
#define USBNDDRIntClr_EP10_MASK 0x400
#define USBNDDRIntClr_EP10 0x400
#define USBNDDRIntClr_EP10_BIT 10
#define USBNDDRIntClr_EP11_MASK 0x800
#define USBNDDRIntClr_EP11 0x800
#define USBNDDRIntClr_EP11_BIT 11
#define USBNDDRIntClr_EP12_MASK 0x1000
#define USBNDDRIntClr_EP12 0x1000
#define USBNDDRIntClr_EP12_BIT 12
#define USBNDDRIntClr_EP13_MASK 0x2000
#define USBNDDRIntClr_EP13 0x2000
#define USBNDDRIntClr_EP13_BIT 13
#define USBNDDRIntClr_EP14_MASK 0x4000
#define USBNDDRIntClr_EP14 0x4000
#define USBNDDRIntClr_EP14_BIT 14
#define USBNDDRIntClr_EP15_MASK 0x8000
#define USBNDDRIntClr_EP15 0x8000
#define USBNDDRIntClr_EP15_BIT 15
#define USBNDDRIntClr_EP16_MASK 0x10000
#define USBNDDRIntClr_EP16 0x10000
#define USBNDDRIntClr_EP16_BIT 16
#define USBNDDRIntClr_EP17_MASK 0x20000
#define USBNDDRIntClr_EP17 0x20000
#define USBNDDRIntClr_EP17_BIT 17
#define USBNDDRIntClr_EP18_MASK 0x40000
#define USBNDDRIntClr_EP18 0x40000
#define USBNDDRIntClr_EP18_BIT 18
#define USBNDDRIntClr_EP19_MASK 0x80000
#define USBNDDRIntClr_EP19 0x80000
#define USBNDDRIntClr_EP19_BIT 19
#define USBNDDRIntClr_EP20_MASK 0x100000
#define USBNDDRIntClr_EP20 0x100000
#define USBNDDRIntClr_EP20_BIT 20
#define USBNDDRIntClr_EP21_MASK 0x200000
#define USBNDDRIntClr_EP21 0x200000
#define USBNDDRIntClr_EP21_BIT 21
#define USBNDDRIntClr_EP22_MASK 0x400000
#define USBNDDRIntClr_EP22 0x400000
#define USBNDDRIntClr_EP22_BIT 22
#define USBNDDRIntClr_EP23_MASK 0x800000
#define USBNDDRIntClr_EP23 0x800000
#define USBNDDRIntClr_EP23_BIT 23
#define USBNDDRIntClr_EP24_MASK 0x1000000
#define USBNDDRIntClr_EP24 0x1000000
#define USBNDDRIntClr_EP24_BIT 24
#define USBNDDRIntClr_EP25_MASK 0x2000000
#define USBNDDRIntClr_EP25 0x2000000
#define USBNDDRIntClr_EP25_BIT 25
#define USBNDDRIntClr_EP26_MASK 0x4000000
#define USBNDDRIntClr_EP26 0x4000000
#define USBNDDRIntClr_EP26_BIT 26
#define USBNDDRIntClr_EP27_MASK 0x8000000
#define USBNDDRIntClr_EP27 0x8000000
#define USBNDDRIntClr_EP27_BIT 27
#define USBNDDRIntClr_EP28_MASK 0x10000000
#define USBNDDRIntClr_EP28 0x10000000
#define USBNDDRIntClr_EP28_BIT 28
#define USBNDDRIntClr_EP29_MASK 0x20000000
#define USBNDDRIntClr_EP29 0x20000000
#define USBNDDRIntClr_EP29_BIT 29
#define USBNDDRIntClr_EP30_MASK 0x40000000
#define USBNDDRIntClr_EP30 0x40000000
#define USBNDDRIntClr_EP30_BIT 30
#define USBNDDRIntClr_EP31_MASK 0x80000000
#define USBNDDRIntClr_EP31 0x80000000
#define USBNDDRIntClr_EP31_BIT 31

#define USBNDDRIntSet (*(volatile unsigned *)0x310202B4)
#define USBNDDRIntSet_EP0_MASK 0x1
#define USBNDDRIntSet_EP0 0x1
#define USBNDDRIntSet_EP0_BIT 0
#define USBNDDRIntSet_EP1_MASK 0x2
#define USBNDDRIntSet_EP1 0x2
#define USBNDDRIntSet_EP1_BIT 1
#define USBNDDRIntSet_EP2_MASK 0x4
#define USBNDDRIntSet_EP2 0x4
#define USBNDDRIntSet_EP2_BIT 2
#define USBNDDRIntSet_EP3_MASK 0x8
#define USBNDDRIntSet_EP3 0x8
#define USBNDDRIntSet_EP3_BIT 3
#define USBNDDRIntSet_EP4_MASK 0x10
#define USBNDDRIntSet_EP4 0x10
#define USBNDDRIntSet_EP4_BIT 4
#define USBNDDRIntSet_EP5_MASK 0x20
#define USBNDDRIntSet_EP5 0x20
#define USBNDDRIntSet_EP5_BIT 5
#define USBNDDRIntSet_EP6_MASK 0x40
#define USBNDDRIntSet_EP6 0x40
#define USBNDDRIntSet_EP6_BIT 6
#define USBNDDRIntSet_EP7_MASK 0x80
#define USBNDDRIntSet_EP7 0x80
#define USBNDDRIntSet_EP7_BIT 7
#define USBNDDRIntSet_EP8_MASK 0x100
#define USBNDDRIntSet_EP8 0x100
#define USBNDDRIntSet_EP8_BIT 8
#define USBNDDRIntSet_EP9_MASK 0x200
#define USBNDDRIntSet_EP9 0x200
#define USBNDDRIntSet_EP9_BIT 9
#define USBNDDRIntSet_EP10_MASK 0x400
#define USBNDDRIntSet_EP10 0x400
#define USBNDDRIntSet_EP10_BIT 10
#define USBNDDRIntSet_EP11_MASK 0x800
#define USBNDDRIntSet_EP11 0x800
#define USBNDDRIntSet_EP11_BIT 11
#define USBNDDRIntSet_EP12_MASK 0x1000
#define USBNDDRIntSet_EP12 0x1000
#define USBNDDRIntSet_EP12_BIT 12
#define USBNDDRIntSet_EP13_MASK 0x2000
#define USBNDDRIntSet_EP13 0x2000
#define USBNDDRIntSet_EP13_BIT 13
#define USBNDDRIntSet_EP14_MASK 0x4000
#define USBNDDRIntSet_EP14 0x4000
#define USBNDDRIntSet_EP14_BIT 14
#define USBNDDRIntSet_EP15_MASK 0x8000
#define USBNDDRIntSet_EP15 0x8000
#define USBNDDRIntSet_EP15_BIT 15
#define USBNDDRIntSet_EP16_MASK 0x10000
#define USBNDDRIntSet_EP16 0x10000
#define USBNDDRIntSet_EP16_BIT 16
#define USBNDDRIntSet_EP17_MASK 0x20000
#define USBNDDRIntSet_EP17 0x20000
#define USBNDDRIntSet_EP17_BIT 17
#define USBNDDRIntSet_EP18_MASK 0x40000
#define USBNDDRIntSet_EP18 0x40000
#define USBNDDRIntSet_EP18_BIT 18
#define USBNDDRIntSet_EP19_MASK 0x80000
#define USBNDDRIntSet_EP19 0x80000
#define USBNDDRIntSet_EP19_BIT 19
#define USBNDDRIntSet_EP20_MASK 0x100000
#define USBNDDRIntSet_EP20 0x100000
#define USBNDDRIntSet_EP20_BIT 20
#define USBNDDRIntSet_EP21_MASK 0x200000
#define USBNDDRIntSet_EP21 0x200000
#define USBNDDRIntSet_EP21_BIT 21
#define USBNDDRIntSet_EP22_MASK 0x400000
#define USBNDDRIntSet_EP22 0x400000
#define USBNDDRIntSet_EP22_BIT 22
#define USBNDDRIntSet_EP23_MASK 0x800000
#define USBNDDRIntSet_EP23 0x800000
#define USBNDDRIntSet_EP23_BIT 23
#define USBNDDRIntSet_EP24_MASK 0x1000000
#define USBNDDRIntSet_EP24 0x1000000
#define USBNDDRIntSet_EP24_BIT 24
#define USBNDDRIntSet_EP25_MASK 0x2000000
#define USBNDDRIntSet_EP25 0x2000000
#define USBNDDRIntSet_EP25_BIT 25
#define USBNDDRIntSet_EP26_MASK 0x4000000
#define USBNDDRIntSet_EP26 0x4000000
#define USBNDDRIntSet_EP26_BIT 26
#define USBNDDRIntSet_EP27_MASK 0x8000000
#define USBNDDRIntSet_EP27 0x8000000
#define USBNDDRIntSet_EP27_BIT 27
#define USBNDDRIntSet_EP28_MASK 0x10000000
#define USBNDDRIntSet_EP28 0x10000000
#define USBNDDRIntSet_EP28_BIT 28
#define USBNDDRIntSet_EP29_MASK 0x20000000
#define USBNDDRIntSet_EP29 0x20000000
#define USBNDDRIntSet_EP29_BIT 29
#define USBNDDRIntSet_EP30_MASK 0x40000000
#define USBNDDRIntSet_EP30 0x40000000
#define USBNDDRIntSet_EP30_BIT 30
#define USBNDDRIntSet_EP31_MASK 0x80000000
#define USBNDDRIntSet_EP31 0x80000000
#define USBNDDRIntSet_EP31_BIT 31

#define USBSysErrIntSt (*(volatile unsigned *)0x310202B8)
#define USBSysErrIntSt_EP0_MASK 0x1
#define USBSysErrIntSt_EP0 0x1
#define USBSysErrIntSt_EP0_BIT 0
#define USBSysErrIntSt_EP1_MASK 0x2
#define USBSysErrIntSt_EP1 0x2
#define USBSysErrIntSt_EP1_BIT 1
#define USBSysErrIntSt_EP2_MASK 0x4
#define USBSysErrIntSt_EP2 0x4
#define USBSysErrIntSt_EP2_BIT 2
#define USBSysErrIntSt_EP3_MASK 0x8
#define USBSysErrIntSt_EP3 0x8
#define USBSysErrIntSt_EP3_BIT 3
#define USBSysErrIntSt_EP4_MASK 0x10
#define USBSysErrIntSt_EP4 0x10
#define USBSysErrIntSt_EP4_BIT 4
#define USBSysErrIntSt_EP5_MASK 0x20
#define USBSysErrIntSt_EP5 0x20
#define USBSysErrIntSt_EP5_BIT 5
#define USBSysErrIntSt_EP6_MASK 0x40
#define USBSysErrIntSt_EP6 0x40
#define USBSysErrIntSt_EP6_BIT 6
#define USBSysErrIntSt_EP7_MASK 0x80
#define USBSysErrIntSt_EP7 0x80
#define USBSysErrIntSt_EP7_BIT 7
#define USBSysErrIntSt_EP8_MASK 0x100
#define USBSysErrIntSt_EP8 0x100
#define USBSysErrIntSt_EP8_BIT 8
#define USBSysErrIntSt_EP9_MASK 0x200
#define USBSysErrIntSt_EP9 0x200
#define USBSysErrIntSt_EP9_BIT 9
#define USBSysErrIntSt_EP10_MASK 0x400
#define USBSysErrIntSt_EP10 0x400
#define USBSysErrIntSt_EP10_BIT 10
#define USBSysErrIntSt_EP11_MASK 0x800
#define USBSysErrIntSt_EP11 0x800
#define USBSysErrIntSt_EP11_BIT 11
#define USBSysErrIntSt_EP12_MASK 0x1000
#define USBSysErrIntSt_EP12 0x1000
#define USBSysErrIntSt_EP12_BIT 12
#define USBSysErrIntSt_EP13_MASK 0x2000
#define USBSysErrIntSt_EP13 0x2000
#define USBSysErrIntSt_EP13_BIT 13
#define USBSysErrIntSt_EP14_MASK 0x4000
#define USBSysErrIntSt_EP14 0x4000
#define USBSysErrIntSt_EP14_BIT 14
#define USBSysErrIntSt_EP15_MASK 0x8000
#define USBSysErrIntSt_EP15 0x8000
#define USBSysErrIntSt_EP15_BIT 15
#define USBSysErrIntSt_EP16_MASK 0x10000
#define USBSysErrIntSt_EP16 0x10000
#define USBSysErrIntSt_EP16_BIT 16
#define USBSysErrIntSt_EP17_MASK 0x20000
#define USBSysErrIntSt_EP17 0x20000
#define USBSysErrIntSt_EP17_BIT 17
#define USBSysErrIntSt_EP18_MASK 0x40000
#define USBSysErrIntSt_EP18 0x40000
#define USBSysErrIntSt_EP18_BIT 18
#define USBSysErrIntSt_EP19_MASK 0x80000
#define USBSysErrIntSt_EP19 0x80000
#define USBSysErrIntSt_EP19_BIT 19
#define USBSysErrIntSt_EP20_MASK 0x100000
#define USBSysErrIntSt_EP20 0x100000
#define USBSysErrIntSt_EP20_BIT 20
#define USBSysErrIntSt_EP21_MASK 0x200000
#define USBSysErrIntSt_EP21 0x200000
#define USBSysErrIntSt_EP21_BIT 21
#define USBSysErrIntSt_EP22_MASK 0x400000
#define USBSysErrIntSt_EP22 0x400000
#define USBSysErrIntSt_EP22_BIT 22
#define USBSysErrIntSt_EP23_MASK 0x800000
#define USBSysErrIntSt_EP23 0x800000
#define USBSysErrIntSt_EP23_BIT 23
#define USBSysErrIntSt_EP24_MASK 0x1000000
#define USBSysErrIntSt_EP24 0x1000000
#define USBSysErrIntSt_EP24_BIT 24
#define USBSysErrIntSt_EP25_MASK 0x2000000
#define USBSysErrIntSt_EP25 0x2000000
#define USBSysErrIntSt_EP25_BIT 25
#define USBSysErrIntSt_EP26_MASK 0x4000000
#define USBSysErrIntSt_EP26 0x4000000
#define USBSysErrIntSt_EP26_BIT 26
#define USBSysErrIntSt_EP27_MASK 0x8000000
#define USBSysErrIntSt_EP27 0x8000000
#define USBSysErrIntSt_EP27_BIT 27
#define USBSysErrIntSt_EP28_MASK 0x10000000
#define USBSysErrIntSt_EP28 0x10000000
#define USBSysErrIntSt_EP28_BIT 28
#define USBSysErrIntSt_EP29_MASK 0x20000000
#define USBSysErrIntSt_EP29 0x20000000
#define USBSysErrIntSt_EP29_BIT 29
#define USBSysErrIntSt_EP30_MASK 0x40000000
#define USBSysErrIntSt_EP30 0x40000000
#define USBSysErrIntSt_EP30_BIT 30
#define USBSysErrIntSt_EP31_MASK 0x80000000
#define USBSysErrIntSt_EP31 0x80000000
#define USBSysErrIntSt_EP31_BIT 31

#define USBSysErrIntClr (*(volatile unsigned *)0x310202BC)
#define USBSysErrIntClr_EP0_MASK 0x1
#define USBSysErrIntClr_EP0 0x1
#define USBSysErrIntClr_EP0_BIT 0
#define USBSysErrIntClr_EP1_MASK 0x2
#define USBSysErrIntClr_EP1 0x2
#define USBSysErrIntClr_EP1_BIT 1
#define USBSysErrIntClr_EP2_MASK 0x4
#define USBSysErrIntClr_EP2 0x4
#define USBSysErrIntClr_EP2_BIT 2
#define USBSysErrIntClr_EP3_MASK 0x8
#define USBSysErrIntClr_EP3 0x8
#define USBSysErrIntClr_EP3_BIT 3
#define USBSysErrIntClr_EP4_MASK 0x10
#define USBSysErrIntClr_EP4 0x10
#define USBSysErrIntClr_EP4_BIT 4
#define USBSysErrIntClr_EP5_MASK 0x20
#define USBSysErrIntClr_EP5 0x20
#define USBSysErrIntClr_EP5_BIT 5
#define USBSysErrIntClr_EP6_MASK 0x40
#define USBSysErrIntClr_EP6 0x40
#define USBSysErrIntClr_EP6_BIT 6
#define USBSysErrIntClr_EP7_MASK 0x80
#define USBSysErrIntClr_EP7 0x80
#define USBSysErrIntClr_EP7_BIT 7
#define USBSysErrIntClr_EP8_MASK 0x100
#define USBSysErrIntClr_EP8 0x100
#define USBSysErrIntClr_EP8_BIT 8
#define USBSysErrIntClr_EP9_MASK 0x200
#define USBSysErrIntClr_EP9 0x200
#define USBSysErrIntClr_EP9_BIT 9
#define USBSysErrIntClr_EP10_MASK 0x400
#define USBSysErrIntClr_EP10 0x400
#define USBSysErrIntClr_EP10_BIT 10
#define USBSysErrIntClr_EP11_MASK 0x800
#define USBSysErrIntClr_EP11 0x800
#define USBSysErrIntClr_EP11_BIT 11
#define USBSysErrIntClr_EP12_MASK 0x1000
#define USBSysErrIntClr_EP12 0x1000
#define USBSysErrIntClr_EP12_BIT 12
#define USBSysErrIntClr_EP13_MASK 0x2000
#define USBSysErrIntClr_EP13 0x2000
#define USBSysErrIntClr_EP13_BIT 13
#define USBSysErrIntClr_EP14_MASK 0x4000
#define USBSysErrIntClr_EP14 0x4000
#define USBSysErrIntClr_EP14_BIT 14
#define USBSysErrIntClr_EP15_MASK 0x8000
#define USBSysErrIntClr_EP15 0x8000
#define USBSysErrIntClr_EP15_BIT 15
#define USBSysErrIntClr_EP16_MASK 0x10000
#define USBSysErrIntClr_EP16 0x10000
#define USBSysErrIntClr_EP16_BIT 16
#define USBSysErrIntClr_EP17_MASK 0x20000
#define USBSysErrIntClr_EP17 0x20000
#define USBSysErrIntClr_EP17_BIT 17
#define USBSysErrIntClr_EP18_MASK 0x40000
#define USBSysErrIntClr_EP18 0x40000
#define USBSysErrIntClr_EP18_BIT 18
#define USBSysErrIntClr_EP19_MASK 0x80000
#define USBSysErrIntClr_EP19 0x80000
#define USBSysErrIntClr_EP19_BIT 19
#define USBSysErrIntClr_EP20_MASK 0x100000
#define USBSysErrIntClr_EP20 0x100000
#define USBSysErrIntClr_EP20_BIT 20
#define USBSysErrIntClr_EP21_MASK 0x200000
#define USBSysErrIntClr_EP21 0x200000
#define USBSysErrIntClr_EP21_BIT 21
#define USBSysErrIntClr_EP22_MASK 0x400000
#define USBSysErrIntClr_EP22 0x400000
#define USBSysErrIntClr_EP22_BIT 22
#define USBSysErrIntClr_EP23_MASK 0x800000
#define USBSysErrIntClr_EP23 0x800000
#define USBSysErrIntClr_EP23_BIT 23
#define USBSysErrIntClr_EP24_MASK 0x1000000
#define USBSysErrIntClr_EP24 0x1000000
#define USBSysErrIntClr_EP24_BIT 24
#define USBSysErrIntClr_EP25_MASK 0x2000000
#define USBSysErrIntClr_EP25 0x2000000
#define USBSysErrIntClr_EP25_BIT 25
#define USBSysErrIntClr_EP26_MASK 0x4000000
#define USBSysErrIntClr_EP26 0x4000000
#define USBSysErrIntClr_EP26_BIT 26
#define USBSysErrIntClr_EP27_MASK 0x8000000
#define USBSysErrIntClr_EP27 0x8000000
#define USBSysErrIntClr_EP27_BIT 27
#define USBSysErrIntClr_EP28_MASK 0x10000000
#define USBSysErrIntClr_EP28 0x10000000
#define USBSysErrIntClr_EP28_BIT 28
#define USBSysErrIntClr_EP29_MASK 0x20000000
#define USBSysErrIntClr_EP29 0x20000000
#define USBSysErrIntClr_EP29_BIT 29
#define USBSysErrIntClr_EP30_MASK 0x40000000
#define USBSysErrIntClr_EP30 0x40000000
#define USBSysErrIntClr_EP30_BIT 30
#define USBSysErrIntClr_EP31_MASK 0x80000000
#define USBSysErrIntClr_EP31 0x80000000
#define USBSysErrIntClr_EP31_BIT 31

#define USBSysErrIntSet (*(volatile unsigned *)0x310202C0)
#define USBSysErrIntSet_EP0_MASK 0x1
#define USBSysErrIntSet_EP0 0x1
#define USBSysErrIntSet_EP0_BIT 0
#define USBSysErrIntSet_EP1_MASK 0x2
#define USBSysErrIntSet_EP1 0x2
#define USBSysErrIntSet_EP1_BIT 1
#define USBSysErrIntSet_EP2_MASK 0x4
#define USBSysErrIntSet_EP2 0x4
#define USBSysErrIntSet_EP2_BIT 2
#define USBSysErrIntSet_EP3_MASK 0x8
#define USBSysErrIntSet_EP3 0x8
#define USBSysErrIntSet_EP3_BIT 3
#define USBSysErrIntSet_EP4_MASK 0x10
#define USBSysErrIntSet_EP4 0x10
#define USBSysErrIntSet_EP4_BIT 4
#define USBSysErrIntSet_EP5_MASK 0x20
#define USBSysErrIntSet_EP5 0x20
#define USBSysErrIntSet_EP5_BIT 5
#define USBSysErrIntSet_EP6_MASK 0x40
#define USBSysErrIntSet_EP6 0x40
#define USBSysErrIntSet_EP6_BIT 6
#define USBSysErrIntSet_EP7_MASK 0x80
#define USBSysErrIntSet_EP7 0x80
#define USBSysErrIntSet_EP7_BIT 7
#define USBSysErrIntSet_EP8_MASK 0x100
#define USBSysErrIntSet_EP8 0x100
#define USBSysErrIntSet_EP8_BIT 8
#define USBSysErrIntSet_EP9_MASK 0x200
#define USBSysErrIntSet_EP9 0x200
#define USBSysErrIntSet_EP9_BIT 9
#define USBSysErrIntSet_EP10_MASK 0x400
#define USBSysErrIntSet_EP10 0x400
#define USBSysErrIntSet_EP10_BIT 10
#define USBSysErrIntSet_EP11_MASK 0x800
#define USBSysErrIntSet_EP11 0x800
#define USBSysErrIntSet_EP11_BIT 11
#define USBSysErrIntSet_EP12_MASK 0x1000
#define USBSysErrIntSet_EP12 0x1000
#define USBSysErrIntSet_EP12_BIT 12
#define USBSysErrIntSet_EP13_MASK 0x2000
#define USBSysErrIntSet_EP13 0x2000
#define USBSysErrIntSet_EP13_BIT 13
#define USBSysErrIntSet_EP14_MASK 0x4000
#define USBSysErrIntSet_EP14 0x4000
#define USBSysErrIntSet_EP14_BIT 14
#define USBSysErrIntSet_EP15_MASK 0x8000
#define USBSysErrIntSet_EP15 0x8000
#define USBSysErrIntSet_EP15_BIT 15
#define USBSysErrIntSet_EP16_MASK 0x10000
#define USBSysErrIntSet_EP16 0x10000
#define USBSysErrIntSet_EP16_BIT 16
#define USBSysErrIntSet_EP17_MASK 0x20000
#define USBSysErrIntSet_EP17 0x20000
#define USBSysErrIntSet_EP17_BIT 17
#define USBSysErrIntSet_EP18_MASK 0x40000
#define USBSysErrIntSet_EP18 0x40000
#define USBSysErrIntSet_EP18_BIT 18
#define USBSysErrIntSet_EP19_MASK 0x80000
#define USBSysErrIntSet_EP19 0x80000
#define USBSysErrIntSet_EP19_BIT 19
#define USBSysErrIntSet_EP20_MASK 0x100000
#define USBSysErrIntSet_EP20 0x100000
#define USBSysErrIntSet_EP20_BIT 20
#define USBSysErrIntSet_EP21_MASK 0x200000
#define USBSysErrIntSet_EP21 0x200000
#define USBSysErrIntSet_EP21_BIT 21
#define USBSysErrIntSet_EP22_MASK 0x400000
#define USBSysErrIntSet_EP22 0x400000
#define USBSysErrIntSet_EP22_BIT 22
#define USBSysErrIntSet_EP23_MASK 0x800000
#define USBSysErrIntSet_EP23 0x800000
#define USBSysErrIntSet_EP23_BIT 23
#define USBSysErrIntSet_EP24_MASK 0x1000000
#define USBSysErrIntSet_EP24 0x1000000
#define USBSysErrIntSet_EP24_BIT 24
#define USBSysErrIntSet_EP25_MASK 0x2000000
#define USBSysErrIntSet_EP25 0x2000000
#define USBSysErrIntSet_EP25_BIT 25
#define USBSysErrIntSet_EP26_MASK 0x4000000
#define USBSysErrIntSet_EP26 0x4000000
#define USBSysErrIntSet_EP26_BIT 26
#define USBSysErrIntSet_EP27_MASK 0x8000000
#define USBSysErrIntSet_EP27 0x8000000
#define USBSysErrIntSet_EP27_BIT 27
#define USBSysErrIntSet_EP28_MASK 0x10000000
#define USBSysErrIntSet_EP28 0x10000000
#define USBSysErrIntSet_EP28_BIT 28
#define USBSysErrIntSet_EP29_MASK 0x20000000
#define USBSysErrIntSet_EP29 0x20000000
#define USBSysErrIntSet_EP29_BIT 29
#define USBSysErrIntSet_EP30_MASK 0x40000000
#define USBSysErrIntSet_EP30 0x40000000
#define USBSysErrIntSet_EP30_BIT 30
#define USBSysErrIntSet_EP31_MASK 0x80000000
#define USBSysErrIntSet_EP31 0x80000000
#define USBSysErrIntSet_EP31_BIT 31

#define I2C_RX (*(volatile unsigned *)0x31020300)
#define I2C_RX_RxData_MASK 0xFF
#define I2C_RX_RxData_BIT 0

#define I2C_TX (*(volatile unsigned *)0x31020300)
#define I2C_TX_STOP_MASK 0x200
#define I2C_TX_STOP 0x200
#define I2C_TX_STOP_BIT 9
#define I2C_TX_START_MASK 0x100
#define I2C_TX_START 0x100
#define I2C_TX_START_BIT 8
#define I2C_TX_TxData_MASK 0xFF
#define I2C_TX_TxData_BIT 0

#define I2C_STAT (*(volatile unsigned *)0x31020304)
#define I2C_STAT_TFES_MASK 0x2000
#define I2C_STAT_TFES 0x2000
#define I2C_STAT_TFES_BIT 13
#define I2C_STAT_TFFS_MASK 0x1000
#define I2C_STAT_TFFS 0x1000
#define I2C_STAT_TFFS_BIT 12
#define I2C_STAT_TFE_MASK 0x800
#define I2C_STAT_TFE 0x800
#define I2C_STAT_TFE_BIT 11
#define I2C_STAT_TFF_MASK 0x400
#define I2C_STAT_TFF 0x400
#define I2C_STAT_TFF_BIT 10
#define I2C_STAT_RFE_MASK 0x200
#define I2C_STAT_RFE 0x200
#define I2C_STAT_RFE_BIT 9
#define I2C_STAT_RFF_MASK 0x100
#define I2C_STAT_RFF 0x100
#define I2C_STAT_RFF_BIT 8
#define I2C_STAT_SDA_MASK 0x80
#define I2C_STAT_SDA 0x80
#define I2C_STAT_SDA_BIT 7
#define I2C_STAT_SCL_MASK 0x40
#define I2C_STAT_SCL 0x40
#define I2C_STAT_SCL_BIT 6
#define I2C_STAT_ACTIVE_MASK 0x20
#define I2C_STAT_ACTIVE 0x20
#define I2C_STAT_ACTIVE_BIT 5
#define I2C_STAT_DRMI_MASK 0x8
#define I2C_STAT_DRMI 0x8
#define I2C_STAT_DRMI_BIT 3
#define I2C_STAT_NAI_MASK 0x4
#define I2C_STAT_NAI 0x4
#define I2C_STAT_NAI_BIT 2
#define I2C_STAT_TDI_MASK 0x1
#define I2C_STAT_TDI 0x1
#define I2C_STAT_TDI_BIT 0

#define I2C_CTRL (*(volatile unsigned *)0x31020308)
#define I2C_CTRL_TFFSIE_MASK 0x400
#define I2C_CTRL_TFFSIE 0x400
#define I2C_CTRL_TFFSIE_BIT 10
#define I2C_CTRL_SEVEN_MASK 0x200
#define I2C_CTRL_SEVEN 0x200
#define I2C_CTRL_SEVEN_BIT 9
#define I2C_CTRL_RESET_MASK 0x100
#define I2C_CTRL_RESET 0x100
#define I2C_CTRL_RESET_BIT 8
#define I2C_CTRL_TFFIE_MASK 0x80
#define I2C_CTRL_TFFIE 0x80
#define I2C_CTRL_TFFIE_BIT 7
#define I2C_CTRL_RFDAIE_MASK 0x40
#define I2C_CTRL_RFDAIE 0x40
#define I2C_CTRL_RFDAIE_BIT 6
#define I2C_CTRL_RFFIE_MASK 0x20
#define I2C_CTRL_RFFIE 0x20
#define I2C_CTRL_RFFIE_BIT 5
#define I2C_CTRL_DRSIE_MASK 0x10
#define I2C_CTRL_DRSIE 0x10
#define I2C_CTRL_DRSIE_BIT 4
#define I2C_CTRL_DRMIE_MASK 0x8
#define I2C_CTRL_DRMIE 0x8
#define I2C_CTRL_DRMIE_BIT 3
#define I2C_CTRL_NAIE_MASK 0x4
#define I2C_CTRL_NAIE 0x4
#define I2C_CTRL_NAIE_BIT 2
#define I2C_CTRL_AFIE_MASK 0x2
#define I2C_CTRL_AFIE 0x2
#define I2C_CTRL_AFIE_BIT 1
#define I2C_CTRL_TDIE_MASK 0x1
#define I2C_CTRL_TDIE 0x1
#define I2C_CTRL_TDIE_BIT 0

#define I2C_CLK_HI (*(volatile unsigned *)0x3102030C)
#define I2C_CLK_HI_CLK_DIV_HI_MASK 0x3FF
#define I2C_CLK_HI_CLK_DIV_HI_BIT 0

#define I2C_CLK_LO (*(volatile unsigned *)0x31020310)
#define I2C_CLK_LO_CLK_DIV_LO_MASK 0x3FF
#define I2C_CLK_LO_CLK_DIV_LO_BIT 0

#define I2C_ADR (*(volatile unsigned *)0x31020314)
#define I2C_ADR_ADR_MASK 0x3FF
#define I2C_ADR_ADR_BIT 0

#define I2C_RXFL (*(volatile unsigned *)0x31020318)
#define I2C_RXFL_RxFL_MASK 0x3
#define I2C_RXFL_RxFL_BIT 0

#define I2C_TXFL (*(volatile unsigned *)0x3102031C)
#define I2C_TXFL_TxFL_MASK 0x3
#define I2C_TXFL_TxFL_BIT 0

#define I2C_RXB (*(volatile unsigned *)0x31020320)
#define I2C_RXB_RxB_MASK 0xFFFF
#define I2C_RXB_RxB_BIT 0

#define I2C_TXB (*(volatile unsigned *)0x31020324)
#define I2C_TXB_TxB_MASK 0xFFFF
#define I2C_TXB_TxB_BIT 0

#define I2C_S_TX (*(volatile unsigned *)0x31020328)
#define I2C_S_TX_TXS_MASK 0xFF
#define I2C_S_TX_TXS_BIT 0

#define I2C_S_TXFL (*(volatile unsigned *)0x3102032C)
#define I2C_S_TXFL_TxFL_MASK 0x3
#define I2C_S_TXFL_TxFL_BIT 0

#define LCD_TIMH (*(volatile unsigned *)0x31040000)
#define LCD_TIMH_PPL_MASK 0xFC
#define LCD_TIMH_PPL_BIT 2
#define LCD_TIMH_HSW_MASK 0xFF00
#define LCD_TIMH_HSW_BIT 8
#define LCD_TIMH_HFP_MASK 0xFF0000
#define LCD_TIMH_HFP_BIT 16
#define LCD_TIMH_HBP_MASK 0xFF000000
#define LCD_TIMH_HBP_BIT 24

#define LCD_TIMV (*(volatile unsigned *)0x31040004)
#define LCD_TIMV_LPP_MASK 0x3FF
#define LCD_TIMV_LPP_BIT 0
#define LCD_TIMV_VSW_MASK 0xFC00
#define LCD_TIMV_VSW_BIT 10
#define LCD_TIMV_VFP_MASK 0xFF0000
#define LCD_TIMV_VFP_BIT 16
#define LCD_TIMV_VBP_MASK 0xFF000000
#define LCD_TIMV_VBP_BIT 24

#define LCD_POL (*(volatile unsigned *)0x31040008)
#define LCD_POL_PCD_LO_MASK 0x1F
#define LCD_POL_PCD_LO_BIT 0
#define LCD_POL_CLKSEL_MASK 0x20
#define LCD_POL_CLKSEL 0x20
#define LCD_POL_CLKSEL_BIT 5
#define LCD_POL_ACB_MASK 0x7C0
#define LCD_POL_ACB_BIT 6
#define LCD_POL_IVS_MASK 0x800
#define LCD_POL_IVS 0x800
#define LCD_POL_IVS_BIT 11
#define LCD_POL_IHS_MASK 0x1000
#define LCD_POL_IHS 0x1000
#define LCD_POL_IHS_BIT 12
#define LCD_POL_IPC_MASK 0x2000
#define LCD_POL_IPC 0x2000
#define LCD_POL_IPC_BIT 13
#define LCD_POL_IOE_MASK 0x4000
#define LCD_POL_IOE 0x4000
#define LCD_POL_IOE_BIT 14
#define LCD_POL_CPL_MASK 0x3FF0000
#define LCD_POL_CPL_BIT 16
#define LCD_POL_BCD_MASK 0x4000000
#define LCD_POL_BCD 0x4000000
#define LCD_POL_BCD_BIT 26
#define LCD_POL_PCD_HI_MASK 0xF8000000
#define LCD_POL_PCD_HI_BIT 27

#define LCD_LE (*(volatile unsigned *)0x3104000C)
#define LCD_LE_LED_MASK 0x7F
#define LCD_LE_LED_BIT 0
#define LCD_LE_LEE_MASK 0x10000
#define LCD_LE_LEE 0x10000
#define LCD_LE_LEE_BIT 16

#define LCD_UPBASE (*(volatile unsigned *)0x31040010)
#define LCD_UPBASE_LCDUPBASE_MASK 0xFFFFFFF8
#define LCD_UPBASE_LCDUPBASE_BIT 3

#define LCD_LPBASE (*(volatile unsigned *)0x31040014)
#define LCD_LPBASE_LCDLPBASE_MASK 0xFFFFFFF8
#define LCD_LPBASE_LCDLPBASE_BIT 3

#define LCD_CTRL (*(volatile unsigned *)0x31040018)
#define LCD_CTRL_LcdEn_MASK 0x1
#define LCD_CTRL_LcdEn 0x1
#define LCD_CTRL_LcdEn_BIT 0
#define LCD_CTRL_LcdBpp_MASK 0xE
#define LCD_CTRL_LcdBpp_BIT 1
#define LCD_CTRL_LcdBW_MASK 0x10
#define LCD_CTRL_LcdBW 0x10
#define LCD_CTRL_LcdBW_BIT 4
#define LCD_CTRL_LcdTFT_MASK 0x20
#define LCD_CTRL_LcdTFT 0x20
#define LCD_CTRL_LcdTFT_BIT 5
#define LCD_CTRL_LcdMon8_MASK 0x40
#define LCD_CTRL_LcdMon8 0x40
#define LCD_CTRL_LcdMon8_BIT 6
#define LCD_CTRL_LcdDual_MASK 0x80
#define LCD_CTRL_LcdDual 0x80
#define LCD_CTRL_LcdDual_BIT 7
#define LCD_CTRL_BGR_MASK 0x100
#define LCD_CTRL_BGR 0x100
#define LCD_CTRL_BGR_BIT 8
#define LCD_CTRL_BEBO_MASK 0x200
#define LCD_CTRL_BEBO 0x200
#define LCD_CTRL_BEBO_BIT 9
#define LCD_CTRL_BEPO_MASK 0x400
#define LCD_CTRL_BEPO 0x400
#define LCD_CTRL_BEPO_BIT 10
#define LCD_CTRL_LcdPwr_MASK 0x800
#define LCD_CTRL_LcdPwr 0x800
#define LCD_CTRL_LcdPwr_BIT 11
#define LCD_CTRL_LcdVComp_MASK 0x3000
#define LCD_CTRL_LcdVComp_BIT 12
#define LCD_CTRL_WATERMARK_MASK 0x10000
#define LCD_CTRL_WATERMARK 0x10000
#define LCD_CTRL_WATERMARK_BIT 16

#define LCD_INTMSK (*(volatile unsigned *)0x3104001C)
#define LCD_INTMSK_FUFIM_MASK 0x2
#define LCD_INTMSK_FUFIM 0x2
#define LCD_INTMSK_FUFIM_BIT 1
#define LCD_INTMSK_LNBUIM_MASK 0x4
#define LCD_INTMSK_LNBUIM 0x4
#define LCD_INTMSK_LNBUIM_BIT 2
#define LCD_INTMSK_VCompIM_MASK 0x8
#define LCD_INTMSK_VCompIM 0x8
#define LCD_INTMSK_VCompIM_BIT 3
#define LCD_INTMSK_BERIM_MASK 0x10
#define LCD_INTMSK_BERIM 0x10
#define LCD_INTMSK_BERIM_BIT 4

#define LCD_INTRAW (*(volatile unsigned *)0x31040020)
#define LCD_INTRAW_FUFRIS_MASK 0x2
#define LCD_INTRAW_FUFRIS 0x2
#define LCD_INTRAW_FUFRIS_BIT 1
#define LCD_INTRAW_LNBURIS_MASK 0x4
#define LCD_INTRAW_LNBURIS 0x4
#define LCD_INTRAW_LNBURIS_BIT 2
#define LCD_INTRAW_VCompRIS_MASK 0x8
#define LCD_INTRAW_VCompRIS 0x8
#define LCD_INTRAW_VCompRIS_BIT 3
#define LCD_INTRAW_BERRAW_MASK 0x10
#define LCD_INTRAW_BERRAW 0x10
#define LCD_INTRAW_BERRAW_BIT 4

#define LCD_INTSTAT (*(volatile unsigned *)0x31040024)
#define LCD_INTSTAT_FUFMIS_MASK 0x2
#define LCD_INTSTAT_FUFMIS 0x2
#define LCD_INTSTAT_FUFMIS_BIT 1
#define LCD_INTSTAT_LNBUMIS_MASK 0x4
#define LCD_INTSTAT_LNBUMIS 0x4
#define LCD_INTSTAT_LNBUMIS_BIT 2
#define LCD_INTSTAT_VCompMIS_MASK 0x8
#define LCD_INTSTAT_VCompMIS 0x8
#define LCD_INTSTAT_VCompMIS_BIT 3
#define LCD_INTSTAT_BERMIS_MASK 0x10
#define LCD_INTSTAT_BERMIS 0x10
#define LCD_INTSTAT_BERMIS_BIT 4

#define LCD_INTCLR (*(volatile unsigned *)0x31040028)
#define LCD_INTCLR_FUFIC_MASK 0x2
#define LCD_INTCLR_FUFIC 0x2
#define LCD_INTCLR_FUFIC_BIT 1
#define LCD_INTCLR_LNBUIC_MASK 0x4
#define LCD_INTCLR_LNBUIC 0x4
#define LCD_INTCLR_LNBUIC_BIT 2
#define LCD_INTCLR_VCompIC_MASK 0x8
#define LCD_INTCLR_VCompIC 0x8
#define LCD_INTCLR_VCompIC_BIT 3
#define LCD_INTCLR_BERIC_MASK 0x10
#define LCD_INTCLR_BERIC 0x10
#define LCD_INTCLR_BERIC_BIT 4

#define LCD_UPCURR (*(volatile unsigned *)0x3104002C)

#define LCD_LPCURR (*(volatile unsigned *)0x31040030)

#define LCD_PAL0 (*(volatile unsigned long *)0x31040200)

#define LCD_PAL1 (*(volatile unsigned long *)0x31040204)

#define LCD_PAL2 (*(volatile unsigned long *)0x31040208)

#define LCD_PAL3 (*(volatile unsigned long *)0x3104020C)

#define LCD_PAL4 (*(volatile unsigned long *)0x31040210)

#define LCD_PAL5 (*(volatile unsigned long *)0x31040214)

#define LCD_PAL6 (*(volatile unsigned long *)0x31040218)

#define LCD_PAL7 (*(volatile unsigned long *)0x3104021C)

#define LCD_PAL8 (*(volatile unsigned long *)0x31040220)

#define LCD_PAL9 (*(volatile unsigned long *)0x31040224)

#define LCD_PAL10 (*(volatile unsigned long *)0x31040228)

#define LCD_PAL11 (*(volatile unsigned long *)0x3104022C)

#define LCD_PAL12 (*(volatile unsigned long *)0x31040230)

#define LCD_PAL13 (*(volatile unsigned long *)0x31040234)

#define LCD_PAL14 (*(volatile unsigned long *)0x31040238)

#define LCD_PAL15 (*(volatile unsigned long *)0x3104023C)

#define LCD_PAL16 (*(volatile unsigned long *)0x31040240)

#define LCD_PAL17 (*(volatile unsigned long *)0x31040244)

#define LCD_PAL18 (*(volatile unsigned long *)0x31040248)

#define LCD_PAL19 (*(volatile unsigned long *)0x3104024C)

#define LCD_PAL20 (*(volatile unsigned long *)0x31040250)

#define LCD_PAL21 (*(volatile unsigned long *)0x31040254)

#define LCD_PAL22 (*(volatile unsigned long *)0x31040258)

#define LCD_PAL23 (*(volatile unsigned long *)0x3104025C)

#define LCD_PAL24 (*(volatile unsigned long *)0x31040260)

#define LCD_PAL25 (*(volatile unsigned long *)0x31040264)

#define LCD_PAL26 (*(volatile unsigned long *)0x31040268)

#define LCD_PAL27 (*(volatile unsigned long *)0x3104026C)

#define LCD_PAL28 (*(volatile unsigned long *)0x31040270)

#define LCD_PAL29 (*(volatile unsigned long *)0x31040274)

#define LCD_PAL30 (*(volatile unsigned long *)0x31040278)

#define LCD_PAL31 (*(volatile unsigned long *)0x3104027C)

#define LCD_PAL32 (*(volatile unsigned long *)0x31040280)

#define LCD_PAL33 (*(volatile unsigned long *)0x31040284)

#define LCD_PAL34 (*(volatile unsigned long *)0x31040288)

#define LCD_PAL35 (*(volatile unsigned long *)0x3104028C)

#define LCD_PAL36 (*(volatile unsigned long *)0x31040290)

#define LCD_PAL37 (*(volatile unsigned long *)0x31040294)

#define LCD_PAL38 (*(volatile unsigned long *)0x31040298)

#define LCD_PAL39 (*(volatile unsigned long *)0x3104029C)

#define LCD_PAL40 (*(volatile unsigned long *)0x310402A0)

#define LCD_PAL41 (*(volatile unsigned long *)0x310402A4)

#define LCD_PAL42 (*(volatile unsigned long *)0x310402A8)

#define LCD_PAL43 (*(volatile unsigned long *)0x310402AC)

#define LCD_PAL44 (*(volatile unsigned long *)0x310402B0)

#define LCD_PAL45 (*(volatile unsigned long *)0x310402B4)

#define LCD_PAL46 (*(volatile unsigned long *)0x310402B8)

#define LCD_PAL47 (*(volatile unsigned long *)0x310402BC)

#define LCD_PAL48 (*(volatile unsigned long *)0x310402C0)

#define LCD_PAL49 (*(volatile unsigned long *)0x310402C4)

#define LCD_PAL50 (*(volatile unsigned long *)0x310402C8)

#define LCD_PAL51 (*(volatile unsigned long *)0x310402CC)

#define LCD_PAL52 (*(volatile unsigned long *)0x310402D0)

#define LCD_PAL53 (*(volatile unsigned long *)0x310402D4)

#define LCD_PAL54 (*(volatile unsigned long *)0x310402D8)

#define LCD_PAL55 (*(volatile unsigned long *)0x310402DC)

#define LCD_PAL56 (*(volatile unsigned long *)0x310402E0)

#define LCD_PAL57 (*(volatile unsigned long *)0x310402E4)

#define LCD_PAL58 (*(volatile unsigned long *)0x310402E8)

#define LCD_PAL59 (*(volatile unsigned long *)0x310402EC)

#define LCD_PAL60 (*(volatile unsigned long *)0x310402F0)

#define LCD_PAL61 (*(volatile unsigned long *)0x310402F4)

#define LCD_PAL62 (*(volatile unsigned long *)0x310402F8)

#define LCD_PAL63 (*(volatile unsigned long *)0x310402FC)

#define LCD_PAL64 (*(volatile unsigned long *)0x31040300)

#define LCD_PAL65 (*(volatile unsigned long *)0x31040304)

#define LCD_PAL66 (*(volatile unsigned long *)0x31040308)

#define LCD_PAL67 (*(volatile unsigned long *)0x3104030C)

#define LCD_PAL68 (*(volatile unsigned long *)0x31040310)

#define LCD_PAL69 (*(volatile unsigned long *)0x31040314)

#define LCD_PAL70 (*(volatile unsigned long *)0x31040318)

#define LCD_PAL71 (*(volatile unsigned long *)0x3104031C)

#define LCD_PAL72 (*(volatile unsigned long *)0x31040320)

#define LCD_PAL73 (*(volatile unsigned long *)0x31040324)

#define LCD_PAL74 (*(volatile unsigned long *)0x31040328)

#define LCD_PAL75 (*(volatile unsigned long *)0x3104032C)

#define LCD_PAL76 (*(volatile unsigned long *)0x31040330)

#define LCD_PAL77 (*(volatile unsigned long *)0x31040334)

#define LCD_PAL78 (*(volatile unsigned long *)0x31040338)

#define LCD_PAL79 (*(volatile unsigned long *)0x3104033C)

#define LCD_PAL80 (*(volatile unsigned long *)0x31040340)

#define LCD_PAL81 (*(volatile unsigned long *)0x31040344)

#define LCD_PAL82 (*(volatile unsigned long *)0x31040348)

#define LCD_PAL83 (*(volatile unsigned long *)0x3104034C)

#define LCD_PAL84 (*(volatile unsigned long *)0x31040350)

#define LCD_PAL85 (*(volatile unsigned long *)0x31040354)

#define LCD_PAL86 (*(volatile unsigned long *)0x31040358)

#define LCD_PAL87 (*(volatile unsigned long *)0x3104035C)

#define LCD_PAL88 (*(volatile unsigned long *)0x31040360)

#define LCD_PAL89 (*(volatile unsigned long *)0x31040364)

#define LCD_PAL90 (*(volatile unsigned long *)0x31040368)

#define LCD_PAL91 (*(volatile unsigned long *)0x3104036C)

#define LCD_PAL92 (*(volatile unsigned long *)0x31040370)

#define LCD_PAL93 (*(volatile unsigned long *)0x31040374)

#define LCD_PAL94 (*(volatile unsigned long *)0x31040378)

#define LCD_PAL95 (*(volatile unsigned long *)0x3104037C)

#define LCD_PAL96 (*(volatile unsigned long *)0x31040380)

#define LCD_PAL97 (*(volatile unsigned long *)0x31040384)

#define LCD_PAL98 (*(volatile unsigned long *)0x31040388)

#define LCD_PAL99 (*(volatile unsigned long *)0x3104038C)

#define LCD_PAL100 (*(volatile unsigned long *)0x31040390)

#define LCD_PAL101 (*(volatile unsigned long *)0x31040394)

#define LCD_PAL102 (*(volatile unsigned long *)0x31040398)

#define LCD_PAL103 (*(volatile unsigned long *)0x3104039C)

#define LCD_PAL104 (*(volatile unsigned long *)0x310403A0)

#define LCD_PAL105 (*(volatile unsigned long *)0x310403A4)

#define LCD_PAL106 (*(volatile unsigned long *)0x310403A8)

#define LCD_PAL107 (*(volatile unsigned long *)0x310403AC)

#define LCD_PAL108 (*(volatile unsigned long *)0x310403B0)

#define LCD_PAL109 (*(volatile unsigned long *)0x310403B4)

#define LCD_PAL110 (*(volatile unsigned long *)0x310403B8)

#define LCD_PAL111 (*(volatile unsigned long *)0x310403BC)

#define LCD_PAL112 (*(volatile unsigned long *)0x310403C0)

#define LCD_PAL113 (*(volatile unsigned long *)0x310403C4)

#define LCD_PAL114 (*(volatile unsigned long *)0x310403C8)

#define LCD_PAL115 (*(volatile unsigned long *)0x310403CC)

#define LCD_PAL116 (*(volatile unsigned long *)0x310403D0)

#define LCD_PAL117 (*(volatile unsigned long *)0x310403D4)

#define LCD_PAL118 (*(volatile unsigned long *)0x310403D8)

#define LCD_PAL119 (*(volatile unsigned long *)0x310403DC)

#define LCD_PAL120 (*(volatile unsigned long *)0x310403E0)

#define LCD_PAL121 (*(volatile unsigned long *)0x310403E4)

#define LCD_PAL122 (*(volatile unsigned long *)0x310403E8)

#define LCD_PAL123 (*(volatile unsigned long *)0x310403EC)

#define LCD_PAL124 (*(volatile unsigned long *)0x310403F0)

#define LCD_PAL125 (*(volatile unsigned long *)0x310403F4)

#define LCD_PAL126 (*(volatile unsigned long *)0x310403F8)

#define LCD_PAL127 (*(volatile unsigned long *)0x310403FC)

#define CRSR_IMG0 (*(volatile unsigned long *)0x31040800)

#define CRSR_IMG1 (*(volatile unsigned long *)0x31040804)

#define CRSR_IMG2 (*(volatile unsigned long *)0x31040808)

#define CRSR_IMG3 (*(volatile unsigned long *)0x3104080C)

#define CRSR_IMG4 (*(volatile unsigned long *)0x31040810)

#define CRSR_IMG5 (*(volatile unsigned long *)0x31040814)

#define CRSR_IMG6 (*(volatile unsigned long *)0x31040818)

#define CRSR_IMG7 (*(volatile unsigned long *)0x3104081C)

#define CRSR_IMG8 (*(volatile unsigned long *)0x31040820)

#define CRSR_IMG9 (*(volatile unsigned long *)0x31040824)

#define CRSR_IMG10 (*(volatile unsigned long *)0x31040828)

#define CRSR_IMG11 (*(volatile unsigned long *)0x3104082C)

#define CRSR_IMG12 (*(volatile unsigned long *)0x31040830)

#define CRSR_IMG13 (*(volatile unsigned long *)0x31040834)

#define CRSR_IMG14 (*(volatile unsigned long *)0x31040838)

#define CRSR_IMG15 (*(volatile unsigned long *)0x3104083C)

#define CRSR_IMG16 (*(volatile unsigned long *)0x31040840)

#define CRSR_IMG17 (*(volatile unsigned long *)0x31040844)

#define CRSR_IMG18 (*(volatile unsigned long *)0x31040848)

#define CRSR_IMG19 (*(volatile unsigned long *)0x3104084C)

#define CRSR_IMG20 (*(volatile unsigned long *)0x31040850)

#define CRSR_IMG21 (*(volatile unsigned long *)0x31040854)

#define CRSR_IMG22 (*(volatile unsigned long *)0x31040858)

#define CRSR_IMG23 (*(volatile unsigned long *)0x3104085C)

#define CRSR_IMG24 (*(volatile unsigned long *)0x31040860)

#define CRSR_IMG25 (*(volatile unsigned long *)0x31040864)

#define CRSR_IMG26 (*(volatile unsigned long *)0x31040868)

#define CRSR_IMG27 (*(volatile unsigned long *)0x3104086C)

#define CRSR_IMG28 (*(volatile unsigned long *)0x31040870)

#define CRSR_IMG29 (*(volatile unsigned long *)0x31040874)

#define CRSR_IMG30 (*(volatile unsigned long *)0x31040878)

#define CRSR_IMG31 (*(volatile unsigned long *)0x3104087C)

#define CRSR_IMG32 (*(volatile unsigned long *)0x31040880)

#define CRSR_IMG33 (*(volatile unsigned long *)0x31040884)

#define CRSR_IMG34 (*(volatile unsigned long *)0x31040888)

#define CRSR_IMG35 (*(volatile unsigned long *)0x3104088C)

#define CRSR_IMG36 (*(volatile unsigned long *)0x31040890)

#define CRSR_IMG37 (*(volatile unsigned long *)0x31040894)

#define CRSR_IMG38 (*(volatile unsigned long *)0x31040898)

#define CRSR_IMG39 (*(volatile unsigned long *)0x3104089C)

#define CRSR_IMG40 (*(volatile unsigned long *)0x310408A0)

#define CRSR_IMG41 (*(volatile unsigned long *)0x310408A4)

#define CRSR_IMG42 (*(volatile unsigned long *)0x310408A8)

#define CRSR_IMG43 (*(volatile unsigned long *)0x310408AC)

#define CRSR_IMG44 (*(volatile unsigned long *)0x310408B0)

#define CRSR_IMG45 (*(volatile unsigned long *)0x310408B4)

#define CRSR_IMG46 (*(volatile unsigned long *)0x310408B8)

#define CRSR_IMG47 (*(volatile unsigned long *)0x310408BC)

#define CRSR_IMG48 (*(volatile unsigned long *)0x310408C0)

#define CRSR_IMG49 (*(volatile unsigned long *)0x310408C4)

#define CRSR_IMG50 (*(volatile unsigned long *)0x310408C8)

#define CRSR_IMG51 (*(volatile unsigned long *)0x310408CC)

#define CRSR_IMG52 (*(volatile unsigned long *)0x310408D0)

#define CRSR_IMG53 (*(volatile unsigned long *)0x310408D4)

#define CRSR_IMG54 (*(volatile unsigned long *)0x310408D8)

#define CRSR_IMG55 (*(volatile unsigned long *)0x310408DC)

#define CRSR_IMG56 (*(volatile unsigned long *)0x310408E0)

#define CRSR_IMG57 (*(volatile unsigned long *)0x310408E4)

#define CRSR_IMG58 (*(volatile unsigned long *)0x310408E8)

#define CRSR_IMG59 (*(volatile unsigned long *)0x310408EC)

#define CRSR_IMG60 (*(volatile unsigned long *)0x310408F0)

#define CRSR_IMG61 (*(volatile unsigned long *)0x310408F4)

#define CRSR_IMG62 (*(volatile unsigned long *)0x310408F8)

#define CRSR_IMG63 (*(volatile unsigned long *)0x310408FC)

#define CRSR_IMG64 (*(volatile unsigned long *)0x31040900)

#define CRSR_IMG65 (*(volatile unsigned long *)0x31040904)

#define CRSR_IMG66 (*(volatile unsigned long *)0x31040908)

#define CRSR_IMG67 (*(volatile unsigned long *)0x3104090C)

#define CRSR_IMG68 (*(volatile unsigned long *)0x31040910)

#define CRSR_IMG69 (*(volatile unsigned long *)0x31040914)

#define CRSR_IMG70 (*(volatile unsigned long *)0x31040918)

#define CRSR_IMG71 (*(volatile unsigned long *)0x3104091C)

#define CRSR_IMG72 (*(volatile unsigned long *)0x31040920)

#define CRSR_IMG73 (*(volatile unsigned long *)0x31040924)

#define CRSR_IMG74 (*(volatile unsigned long *)0x31040928)

#define CRSR_IMG75 (*(volatile unsigned long *)0x3104092C)

#define CRSR_IMG76 (*(volatile unsigned long *)0x31040930)

#define CRSR_IMG77 (*(volatile unsigned long *)0x31040934)

#define CRSR_IMG78 (*(volatile unsigned long *)0x31040938)

#define CRSR_IMG79 (*(volatile unsigned long *)0x3104093C)

#define CRSR_IMG80 (*(volatile unsigned long *)0x31040940)

#define CRSR_IMG81 (*(volatile unsigned long *)0x31040944)

#define CRSR_IMG82 (*(volatile unsigned long *)0x31040948)

#define CRSR_IMG83 (*(volatile unsigned long *)0x3104094C)

#define CRSR_IMG84 (*(volatile unsigned long *)0x31040950)

#define CRSR_IMG85 (*(volatile unsigned long *)0x31040954)

#define CRSR_IMG86 (*(volatile unsigned long *)0x31040958)

#define CRSR_IMG87 (*(volatile unsigned long *)0x3104095C)

#define CRSR_IMG88 (*(volatile unsigned long *)0x31040960)

#define CRSR_IMG89 (*(volatile unsigned long *)0x31040964)

#define CRSR_IMG90 (*(volatile unsigned long *)0x31040968)

#define CRSR_IMG91 (*(volatile unsigned long *)0x3104096C)

#define CRSR_IMG92 (*(volatile unsigned long *)0x31040970)

#define CRSR_IMG93 (*(volatile unsigned long *)0x31040974)

#define CRSR_IMG94 (*(volatile unsigned long *)0x31040978)

#define CRSR_IMG95 (*(volatile unsigned long *)0x3104097C)

#define CRSR_IMG96 (*(volatile unsigned long *)0x31040980)

#define CRSR_IMG97 (*(volatile unsigned long *)0x31040984)

#define CRSR_IMG98 (*(volatile unsigned long *)0x31040988)

#define CRSR_IMG99 (*(volatile unsigned long *)0x3104098C)

#define CRSR_IMG100 (*(volatile unsigned long *)0x31040990)

#define CRSR_IMG101 (*(volatile unsigned long *)0x31040994)

#define CRSR_IMG102 (*(volatile unsigned long *)0x31040998)

#define CRSR_IMG103 (*(volatile unsigned long *)0x3104099C)

#define CRSR_IMG104 (*(volatile unsigned long *)0x310409A0)

#define CRSR_IMG105 (*(volatile unsigned long *)0x310409A4)

#define CRSR_IMG106 (*(volatile unsigned long *)0x310409A8)

#define CRSR_IMG107 (*(volatile unsigned long *)0x310409AC)

#define CRSR_IMG108 (*(volatile unsigned long *)0x310409B0)

#define CRSR_IMG109 (*(volatile unsigned long *)0x310409B4)

#define CRSR_IMG110 (*(volatile unsigned long *)0x310409B8)

#define CRSR_IMG111 (*(volatile unsigned long *)0x310409BC)

#define CRSR_IMG112 (*(volatile unsigned long *)0x310409C0)

#define CRSR_IMG113 (*(volatile unsigned long *)0x310409C4)

#define CRSR_IMG114 (*(volatile unsigned long *)0x310409C8)

#define CRSR_IMG115 (*(volatile unsigned long *)0x310409CC)

#define CRSR_IMG116 (*(volatile unsigned long *)0x310409D0)

#define CRSR_IMG117 (*(volatile unsigned long *)0x310409D4)

#define CRSR_IMG118 (*(volatile unsigned long *)0x310409D8)

#define CRSR_IMG119 (*(volatile unsigned long *)0x310409DC)

#define CRSR_IMG120 (*(volatile unsigned long *)0x310409E0)

#define CRSR_IMG121 (*(volatile unsigned long *)0x310409E4)

#define CRSR_IMG122 (*(volatile unsigned long *)0x310409E8)

#define CRSR_IMG123 (*(volatile unsigned long *)0x310409EC)

#define CRSR_IMG124 (*(volatile unsigned long *)0x310409F0)

#define CRSR_IMG125 (*(volatile unsigned long *)0x310409F4)

#define CRSR_IMG126 (*(volatile unsigned long *)0x310409F8)

#define CRSR_IMG127 (*(volatile unsigned long *)0x310409FC)

#define CRSR_IMG128 (*(volatile unsigned long *)0x31040A00)

#define CRSR_IMG129 (*(volatile unsigned long *)0x31040A04)

#define CRSR_IMG130 (*(volatile unsigned long *)0x31040A08)

#define CRSR_IMG131 (*(volatile unsigned long *)0x31040A0C)

#define CRSR_IMG132 (*(volatile unsigned long *)0x31040A10)

#define CRSR_IMG133 (*(volatile unsigned long *)0x31040A14)

#define CRSR_IMG134 (*(volatile unsigned long *)0x31040A18)

#define CRSR_IMG135 (*(volatile unsigned long *)0x31040A1C)

#define CRSR_IMG136 (*(volatile unsigned long *)0x31040A20)

#define CRSR_IMG137 (*(volatile unsigned long *)0x31040A24)

#define CRSR_IMG138 (*(volatile unsigned long *)0x31040A28)

#define CRSR_IMG139 (*(volatile unsigned long *)0x31040A2C)

#define CRSR_IMG140 (*(volatile unsigned long *)0x31040A30)

#define CRSR_IMG141 (*(volatile unsigned long *)0x31040A34)

#define CRSR_IMG142 (*(volatile unsigned long *)0x31040A38)

#define CRSR_IMG143 (*(volatile unsigned long *)0x31040A3C)

#define CRSR_IMG144 (*(volatile unsigned long *)0x31040A40)

#define CRSR_IMG145 (*(volatile unsigned long *)0x31040A44)

#define CRSR_IMG146 (*(volatile unsigned long *)0x31040A48)

#define CRSR_IMG147 (*(volatile unsigned long *)0x31040A4C)

#define CRSR_IMG148 (*(volatile unsigned long *)0x31040A50)

#define CRSR_IMG149 (*(volatile unsigned long *)0x31040A54)

#define CRSR_IMG150 (*(volatile unsigned long *)0x31040A58)

#define CRSR_IMG151 (*(volatile unsigned long *)0x31040A5C)

#define CRSR_IMG152 (*(volatile unsigned long *)0x31040A60)

#define CRSR_IMG153 (*(volatile unsigned long *)0x31040A64)

#define CRSR_IMG154 (*(volatile unsigned long *)0x31040A68)

#define CRSR_IMG155 (*(volatile unsigned long *)0x31040A6C)

#define CRSR_IMG156 (*(volatile unsigned long *)0x31040A70)

#define CRSR_IMG157 (*(volatile unsigned long *)0x31040A74)

#define CRSR_IMG158 (*(volatile unsigned long *)0x31040A78)

#define CRSR_IMG159 (*(volatile unsigned long *)0x31040A7C)

#define CRSR_IMG160 (*(volatile unsigned long *)0x31040A80)

#define CRSR_IMG161 (*(volatile unsigned long *)0x31040A84)

#define CRSR_IMG162 (*(volatile unsigned long *)0x31040A88)

#define CRSR_IMG163 (*(volatile unsigned long *)0x31040A8C)

#define CRSR_IMG164 (*(volatile unsigned long *)0x31040A90)

#define CRSR_IMG165 (*(volatile unsigned long *)0x31040A94)

#define CRSR_IMG166 (*(volatile unsigned long *)0x31040A98)

#define CRSR_IMG167 (*(volatile unsigned long *)0x31040A9C)

#define CRSR_IMG168 (*(volatile unsigned long *)0x31040AA0)

#define CRSR_IMG169 (*(volatile unsigned long *)0x31040AA4)

#define CRSR_IMG170 (*(volatile unsigned long *)0x31040AA8)

#define CRSR_IMG171 (*(volatile unsigned long *)0x31040AAC)

#define CRSR_IMG172 (*(volatile unsigned long *)0x31040AB0)

#define CRSR_IMG173 (*(volatile unsigned long *)0x31040AB4)

#define CRSR_IMG174 (*(volatile unsigned long *)0x31040AB8)

#define CRSR_IMG175 (*(volatile unsigned long *)0x31040ABC)

#define CRSR_IMG176 (*(volatile unsigned long *)0x31040AC0)

#define CRSR_IMG177 (*(volatile unsigned long *)0x31040AC4)

#define CRSR_IMG178 (*(volatile unsigned long *)0x31040AC8)

#define CRSR_IMG179 (*(volatile unsigned long *)0x31040ACC)

#define CRSR_IMG180 (*(volatile unsigned long *)0x31040AD0)

#define CRSR_IMG181 (*(volatile unsigned long *)0x31040AD4)

#define CRSR_IMG182 (*(volatile unsigned long *)0x31040AD8)

#define CRSR_IMG183 (*(volatile unsigned long *)0x31040ADC)

#define CRSR_IMG184 (*(volatile unsigned long *)0x31040AE0)

#define CRSR_IMG185 (*(volatile unsigned long *)0x31040AE4)

#define CRSR_IMG186 (*(volatile unsigned long *)0x31040AE8)

#define CRSR_IMG187 (*(volatile unsigned long *)0x31040AEC)

#define CRSR_IMG188 (*(volatile unsigned long *)0x31040AF0)

#define CRSR_IMG189 (*(volatile unsigned long *)0x31040AF4)

#define CRSR_IMG190 (*(volatile unsigned long *)0x31040AF8)

#define CRSR_IMG191 (*(volatile unsigned long *)0x31040AFC)

#define CRSR_IMG192 (*(volatile unsigned long *)0x31040B00)

#define CRSR_IMG193 (*(volatile unsigned long *)0x31040B04)

#define CRSR_IMG194 (*(volatile unsigned long *)0x31040B08)

#define CRSR_IMG195 (*(volatile unsigned long *)0x31040B0C)

#define CRSR_IMG196 (*(volatile unsigned long *)0x31040B10)

#define CRSR_IMG197 (*(volatile unsigned long *)0x31040B14)

#define CRSR_IMG198 (*(volatile unsigned long *)0x31040B18)

#define CRSR_IMG199 (*(volatile unsigned long *)0x31040B1C)

#define CRSR_IMG200 (*(volatile unsigned long *)0x31040B20)

#define CRSR_IMG201 (*(volatile unsigned long *)0x31040B24)

#define CRSR_IMG202 (*(volatile unsigned long *)0x31040B28)

#define CRSR_IMG203 (*(volatile unsigned long *)0x31040B2C)

#define CRSR_IMG204 (*(volatile unsigned long *)0x31040B30)

#define CRSR_IMG205 (*(volatile unsigned long *)0x31040B34)

#define CRSR_IMG206 (*(volatile unsigned long *)0x31040B38)

#define CRSR_IMG207 (*(volatile unsigned long *)0x31040B3C)

#define CRSR_IMG208 (*(volatile unsigned long *)0x31040B40)

#define CRSR_IMG209 (*(volatile unsigned long *)0x31040B44)

#define CRSR_IMG210 (*(volatile unsigned long *)0x31040B48)

#define CRSR_IMG211 (*(volatile unsigned long *)0x31040B4C)

#define CRSR_IMG212 (*(volatile unsigned long *)0x31040B50)

#define CRSR_IMG213 (*(volatile unsigned long *)0x31040B54)

#define CRSR_IMG214 (*(volatile unsigned long *)0x31040B58)

#define CRSR_IMG215 (*(volatile unsigned long *)0x31040B5C)

#define CRSR_IMG216 (*(volatile unsigned long *)0x31040B60)

#define CRSR_IMG217 (*(volatile unsigned long *)0x31040B64)

#define CRSR_IMG218 (*(volatile unsigned long *)0x31040B68)

#define CRSR_IMG219 (*(volatile unsigned long *)0x31040B6C)

#define CRSR_IMG220 (*(volatile unsigned long *)0x31040B70)

#define CRSR_IMG221 (*(volatile unsigned long *)0x31040B74)

#define CRSR_IMG222 (*(volatile unsigned long *)0x31040B78)

#define CRSR_IMG223 (*(volatile unsigned long *)0x31040B7C)

#define CRSR_IMG224 (*(volatile unsigned long *)0x31040B80)

#define CRSR_IMG225 (*(volatile unsigned long *)0x31040B84)

#define CRSR_IMG226 (*(volatile unsigned long *)0x31040B88)

#define CRSR_IMG227 (*(volatile unsigned long *)0x31040B8C)

#define CRSR_IMG228 (*(volatile unsigned long *)0x31040B90)

#define CRSR_IMG229 (*(volatile unsigned long *)0x31040B94)

#define CRSR_IMG230 (*(volatile unsigned long *)0x31040B98)

#define CRSR_IMG231 (*(volatile unsigned long *)0x31040B9C)

#define CRSR_IMG232 (*(volatile unsigned long *)0x31040BA0)

#define CRSR_IMG233 (*(volatile unsigned long *)0x31040BA4)

#define CRSR_IMG234 (*(volatile unsigned long *)0x31040BA8)

#define CRSR_IMG235 (*(volatile unsigned long *)0x31040BAC)

#define CRSR_IMG236 (*(volatile unsigned long *)0x31040BB0)

#define CRSR_IMG237 (*(volatile unsigned long *)0x31040BB4)

#define CRSR_IMG238 (*(volatile unsigned long *)0x31040BB8)

#define CRSR_IMG239 (*(volatile unsigned long *)0x31040BBC)

#define CRSR_IMG240 (*(volatile unsigned long *)0x31040BC0)

#define CRSR_IMG241 (*(volatile unsigned long *)0x31040BC4)

#define CRSR_IMG242 (*(volatile unsigned long *)0x31040BC8)

#define CRSR_IMG243 (*(volatile unsigned long *)0x31040BCC)

#define CRSR_IMG244 (*(volatile unsigned long *)0x31040BD0)

#define CRSR_IMG245 (*(volatile unsigned long *)0x31040BD4)

#define CRSR_IMG246 (*(volatile unsigned long *)0x31040BD8)

#define CRSR_IMG247 (*(volatile unsigned long *)0x31040BDC)

#define CRSR_IMG248 (*(volatile unsigned long *)0x31040BE0)

#define CRSR_IMG249 (*(volatile unsigned long *)0x31040BE4)

#define CRSR_IMG250 (*(volatile unsigned long *)0x31040BE8)

#define CRSR_IMG251 (*(volatile unsigned long *)0x31040BEC)

#define CRSR_IMG252 (*(volatile unsigned long *)0x31040BF0)

#define CRSR_IMG253 (*(volatile unsigned long *)0x31040BF4)

#define CRSR_IMG254 (*(volatile unsigned long *)0x31040BF8)

#define CRSR_IMG255 (*(volatile unsigned long *)0x31040BFC)

#define CRSR_CTRL (*(volatile unsigned *)0x31040C00)
#define CRSR_CTRL_CrsrOn_MASK 0x1
#define CRSR_CTRL_CrsrOn 0x1
#define CRSR_CTRL_CrsrOn_BIT 0
#define CRSR_CTRL_CrsrNum_MASK 0x30
#define CRSR_CTRL_CrsrNum_BIT 4

#define CRSR_CFG (*(volatile unsigned *)0x31040C04)
#define CRSR_CFG_CrsrSize_MASK 0x1
#define CRSR_CFG_CrsrSize 0x1
#define CRSR_CFG_CrsrSize_BIT 0
#define CRSR_CFG_FrameSync_MASK 0x2
#define CRSR_CFG_FrameSync 0x2
#define CRSR_CFG_FrameSync_BIT 1

#define CRSR_PAL0 (*(volatile unsigned *)0x31040C08)
#define CRSR_PAL0_Red_MASK 0xFF
#define CRSR_PAL0_Red_BIT 0
#define CRSR_PAL0_Green_MASK 0xFF00
#define CRSR_PAL0_Green_BIT 8
#define CRSR_PAL0_Blue_MASK 0xFF0000
#define CRSR_PAL0_Blue_BIT 16

#define CRSR_PAL1 (*(volatile unsigned *)0x31040C0C)
#define CRSR_PAL1_Red_MASK 0xFF
#define CRSR_PAL1_Red_BIT 0
#define CRSR_PAL1_Green_MASK 0xFF00
#define CRSR_PAL1_Green_BIT 8
#define CRSR_PAL1_Blue_MASK 0xFF0000
#define CRSR_PAL1_Blue_BIT 16

#define CRSR_XY (*(volatile unsigned *)0x31040C10)
#define CRSR_XY_CrsrX_MASK 0x3FF
#define CRSR_XY_CrsrX_BIT 0
#define CRSR_XY_CrsrY_MASK 0x3FF0000
#define CRSR_XY_CrsrY_BIT 16

#define CRSR_CLIP (*(volatile unsigned *)0x31040C14)
#define CRSR_CLIP_CrsrClipX_MASK 0x3F
#define CRSR_CLIP_CrsrClipX_BIT 0
#define CRSR_CLIP_CrsrClipY_MASK 0x3F00
#define CRSR_CLIP_CrsrClipY_BIT 8

#define CRSR_INTMSK (*(volatile unsigned *)0x31040C20)
#define CRSR_INTMSK_CrsrIM_MASK 0x1
#define CRSR_INTMSK_CrsrIM 0x1
#define CRSR_INTMSK_CrsrIM_BIT 0

#define CRSR_INTCLR (*(volatile unsigned *)0x31040C24)
#define CRSR_INTCLR_CrsrIC_MASK 0x1
#define CRSR_INTCLR_CrsrIC 0x1
#define CRSR_INTCLR_CrsrIC_BIT 0

#define CRSR_INTRAW (*(volatile unsigned *)0x31040C28)
#define CRSR_INTRAW_CrsrRIS_MASK 0x1
#define CRSR_INTRAW_CrsrRIS 0x1
#define CRSR_INTRAW_CrsrRIS_BIT 0

#define CRSR_INTSTAT (*(volatile unsigned *)0x31040C2C)
#define CRSR_INTSTAT_CrsrMIS_MASK 0x1
#define CRSR_INTSTAT_CrsrMIS 0x1
#define CRSR_INTSTAT_CrsrMIS_BIT 0

#define MAC1 (*(volatile unsigned *)0x31060000)
#define MAC1_RECEIVE_ENABLE_MASK 0x1
#define MAC1_RECEIVE_ENABLE 0x1
#define MAC1_RECEIVE_ENABLE_BIT 0
#define MAC1_PASS_ALL_RECEIVE_FRAMES_MASK 0x2
#define MAC1_PASS_ALL_RECEIVE_FRAMES 0x2
#define MAC1_PASS_ALL_RECEIVE_FRAMES_BIT 1
#define MAC1_RX_FLOW_CONTROL_MASK 0x4
#define MAC1_RX_FLOW_CONTROL 0x4
#define MAC1_RX_FLOW_CONTROL_BIT 2
#define MAC1_TX_FLOW_CONTROL_MASK 0x8
#define MAC1_TX_FLOW_CONTROL 0x8
#define MAC1_TX_FLOW_CONTROL_BIT 3
#define MAC1_LOOPBACK_MASK 0x10
#define MAC1_LOOPBACK 0x10
#define MAC1_LOOPBACK_BIT 4
#define MAC1_RESET_TX_MASK 0x100
#define MAC1_RESET_TX 0x100
#define MAC1_RESET_TX_BIT 8
#define MAC1_RESET_MCS_TX_MASK 0x200
#define MAC1_RESET_MCS_TX 0x200
#define MAC1_RESET_MCS_TX_BIT 9
#define MAC1_RESET_RX_MASK 0x400
#define MAC1_RESET_RX 0x400
#define MAC1_RESET_RX_BIT 10
#define MAC1_RESET_MCS_RX_MASK 0x800
#define MAC1_RESET_MCS_RX 0x800
#define MAC1_RESET_MCS_RX_BIT 11
#define MAC1_SIMULATION_RESET_MASK 0x4000
#define MAC1_SIMULATION_RESET 0x4000
#define MAC1_SIMULATION_RESET_BIT 14
#define MAC1_SOFT_RESET_MASK 0x8000
#define MAC1_SOFT_RESET 0x8000
#define MAC1_SOFT_RESET_BIT 15

#define MAC2 (*(volatile unsigned *)0x31060004)
#define MAC2_FULL_DUPLEX_MASK 0x1
#define MAC2_FULL_DUPLEX 0x1
#define MAC2_FULL_DUPLEX_BIT 0
#define MAC2_FRAME_LENGTH_CHECKING_MASK 0x2
#define MAC2_FRAME_LENGTH_CHECKING 0x2
#define MAC2_FRAME_LENGTH_CHECKING_BIT 1
#define MAC2_HUGE_FRAME_ENABLE_MASK 0x4
#define MAC2_HUGE_FRAME_ENABLE 0x4
#define MAC2_HUGE_FRAME_ENABLE_BIT 2
#define MAC2_DELAYED_CRC_MASK 0x8
#define MAC2_DELAYED_CRC 0x8
#define MAC2_DELAYED_CRC_BIT 3
#define MAC2_CRC_ENABLE_MASK 0x10
#define MAC2_CRC_ENABLE 0x10
#define MAC2_CRC_ENABLE_BIT 4
#define MAC2_PAD_CRC_ENABLE_MASK 0x20
#define MAC2_PAD_CRC_ENABLE 0x20
#define MAC2_PAD_CRC_ENABLE_BIT 5
#define MAC2_VLAN_PAD_ENABLE_MASK 0x40
#define MAC2_VLAN_PAD_ENABLE 0x40
#define MAC2_VLAN_PAD_ENABLE_BIT 6
#define MAC2_AUTO_DETECT_PAD_ENABLE_MASK 0x80
#define MAC2_AUTO_DETECT_PAD_ENABLE 0x80
#define MAC2_AUTO_DETECT_PAD_ENABLE_BIT 7
#define MAC2_PURE_PREAMBLE_ENFORCEMENT_MASK 0x100
#define MAC2_PURE_PREAMBLE_ENFORCEMENT 0x100
#define MAC2_PURE_PREAMBLE_ENFORCEMENT_BIT 8
#define MAC2_LONG_PREAMBLE_ENFORCEMENT_MASK 0x200
#define MAC2_LONG_PREAMBLE_ENFORCEMENT 0x200
#define MAC2_LONG_PREAMBLE_ENFORCEMENT_BIT 9
#define MAC2_NO_BACKOFF_MASK 0x1000
#define MAC2_NO_BACKOFF 0x1000
#define MAC2_NO_BACKOFF_BIT 12
#define MAC2_BACK_PRESSURE_NO_BACKOFF_MASK 0x2000
#define MAC2_BACK_PRESSURE_NO_BACKOFF 0x2000
#define MAC2_BACK_PRESSURE_NO_BACKOFF_BIT 13
#define MAC2_EXCESS_DEFER_MASK 0x4000
#define MAC2_EXCESS_DEFER 0x4000
#define MAC2_EXCESS_DEFER_BIT 14

#define IPGT (*(volatile unsigned *)0x31060008)
#define IPGT_BACK_TO_BACK_INTER_PACKET_GAP_MASK 0x7F
#define IPGT_BACK_TO_BACK_INTER_PACKET_GAP_BIT 0

#define IPGR (*(volatile unsigned *)0x3106000C)
#define IPGR_NON_BACK_TO_BACK_INTER_PACKET_GAP_PART2_MASK 0x7F
#define IPGR_NON_BACK_TO_BACK_INTER_PACKET_GAP_PART2_BIT 0
#define IPGR_NON_BACK_TO_BACK_INTER_PACKET_GAP_PART1_MASK 0x7F00
#define IPGR_NON_BACK_TO_BACK_INTER_PACKET_GAP_PART1_BIT 8

#define CLRT (*(volatile unsigned *)0x31060010)
#define CLRT_RETRANSMISSION_MAXIMUM_MASK 0xF
#define CLRT_RETRANSMISSION_MAXIMUM_BIT 0
#define CLRT_COLLISION_WINDOW_MASK 0x3F00
#define CLRT_COLLISION_WINDOW_BIT 8

#define MAXF (*(volatile unsigned *)0x31060014)
#define MAXF_MAXIMUM_FRAME_LENGTH_MASK 0xFFFF
#define MAXF_MAXIMUM_FRAME_LENGTH_BIT 0

#define SUPP (*(volatile unsigned *)0x31060018)
#define SUPP_SPEED_MASK 0x100
#define SUPP_SPEED 0x100
#define SUPP_SPEED_BIT 8

#define TEST (*(volatile unsigned *)0x3106001C)
#define TEST_SHORTCUT_PAUSE_QUANTA_MASK 0x1
#define TEST_SHORTCUT_PAUSE_QUANTA 0x1
#define TEST_SHORTCUT_PAUSE_QUANTA_BIT 0
#define TEST_TEST_PAUSE_MASK 0x2
#define TEST_TEST_PAUSE 0x2
#define TEST_TEST_PAUSE_BIT 1
#define TEST_TEST_BACKPRESSURE_MASK 0x4
#define TEST_TEST_BACKPRESSURE 0x4
#define TEST_TEST_BACKPRESSURE_BIT 2

#define MCFG (*(volatile unsigned *)0x31060020)
#define MCFG_SCAN_INCREMENT_MASK 0x1
#define MCFG_SCAN_INCREMENT 0x1
#define MCFG_SCAN_INCREMENT_BIT 0
#define MCFG_SUPPRESS_PREAMBLE_MASK 0x2
#define MCFG_SUPPRESS_PREAMBLE 0x2
#define MCFG_SUPPRESS_PREAMBLE_BIT 1
#define MCFG_CLOCK_SELECT_MASK 0x1C
#define MCFG_CLOCK_SELECT_BIT 2
#define MCFG_RESET_MII_MGMT_MASK 0x8000
#define MCFG_RESET_MII_MGMT 0x8000
#define MCFG_RESET_MII_MGMT_BIT 15

#define MCMD (*(volatile unsigned *)0x31060024)
#define MCMD_READ_MASK 0x1
#define MCMD_READ 0x1
#define MCMD_READ_BIT 0
#define MCMD_SCAN_MASK 0x2
#define MCMD_SCAN 0x2
#define MCMD_SCAN_BIT 1

#define MADR (*(volatile unsigned *)0x31060028)
#define MADR_REGISTER_ADDRESS_MASK 0x1F
#define MADR_REGISTER_ADDRESS_BIT 0
#define MADR_PHY_ADDRESS_MASK 0x1F00
#define MADR_PHY_ADDRESS_BIT 8

#define MWTD (*(volatile unsigned *)0x3106002C)
#define MWTD_WRITE_DATA_MASK 0xFFFF
#define MWTD_WRITE_DATA_BIT 0

#define MRDD (*(volatile unsigned *)0x31060030)
#define MRDD_WRITE_DATA_MASK 0xFFFF
#define MRDD_WRITE_DATA_BIT 0

#define MIND (*(volatile unsigned *)0x31060034)
#define MIND_BUSY_MASK 0x1
#define MIND_BUSY 0x1
#define MIND_BUSY_BIT 0
#define MIND_SCANNING_MASK 0x2
#define MIND_SCANNING 0x2
#define MIND_SCANNING_BIT 1
#define MIND_NOT_VALID_MASK 0x4
#define MIND_NOT_VALID 0x4
#define MIND_NOT_VALID_BIT 2
#define MIND_MII_Link_Fail_MASK 0x8
#define MIND_MII_Link_Fail 0x8
#define MIND_MII_Link_Fail_BIT 3

#define SA0 (*(volatile unsigned *)0x31060040)
#define SA0_STATION_ADDRESS_2nd_octet_MASK 0xFF
#define SA0_STATION_ADDRESS_2nd_octet_BIT 0
#define SA0_STATION_ADDRESS_1st_octet_MASK 0xFF00
#define SA0_STATION_ADDRESS_1st_octet_BIT 8

#define SA1 (*(volatile unsigned *)0x31060044)
#define SA1_STATION_ADDRESS_4th_octet_MASK 0xFF
#define SA1_STATION_ADDRESS_4th_octet_BIT 0
#define SA1_STATION_ADDRESS_3rd_octet_MASK 0xFF00
#define SA1_STATION_ADDRESS_3rd_octet_BIT 8

#define SA2 (*(volatile unsigned *)0x31060048)
#define SA2_STATION_ADDRESS_6th_octet_MASK 0xFF
#define SA2_STATION_ADDRESS_6th_octet_BIT 0
#define SA2_STATION_ADDRESS_5th_octet_MASK 0xFF00
#define SA2_STATION_ADDRESS_5th_octet_BIT 8

#define Command (*(volatile unsigned *)0x31060100)
#define Command_RxEnable_MASK 0x1
#define Command_RxEnable 0x1
#define Command_RxEnable_BIT 0
#define Command_TxEnable_MASK 0x2
#define Command_TxEnable 0x2
#define Command_TxEnable_BIT 1
#define Command_RegReset_MASK 0x8
#define Command_RegReset 0x8
#define Command_RegReset_BIT 3
#define Command_TxReset_MASK 0x10
#define Command_TxReset 0x10
#define Command_TxReset_BIT 4
#define Command_RxReset_MASK 0x20
#define Command_RxReset 0x20
#define Command_RxReset_BIT 5
#define Command_PassRuntFrame_MASK 0x40
#define Command_PassRuntFrame 0x40
#define Command_PassRuntFrame_BIT 6
#define Command_PassRxFilter_MASK 0x80
#define Command_PassRxFilter 0x80
#define Command_PassRxFilter_BIT 7
#define Command_TxFlowControl_MASK 0x100
#define Command_TxFlowControl 0x100
#define Command_TxFlowControl_BIT 8
#define Command_RMII_MASK 0x200
#define Command_RMII 0x200
#define Command_RMII_BIT 9
#define Command_FullDuplex_MASK 0x400
#define Command_FullDuplex 0x400
#define Command_FullDuplex_BIT 10

#define Status (*(volatile unsigned *)0x31060104)
#define Status_RxStatus_MASK 0x1
#define Status_RxStatus 0x1
#define Status_RxStatus_BIT 0
#define Status_TxStatus_MASK 0x2
#define Status_TxStatus 0x2
#define Status_TxStatus_BIT 1

#define RxDescriptor (*(volatile unsigned *)0x31060108)

#define RxStatus (*(volatile unsigned *)0x3106010C)

#define RxDescriptorNumber (*(volatile unsigned *)0x31060110)

#define RxProduceIndex (*(volatile unsigned *)0x31060114)

#define RxConsumeIndex (*(volatile unsigned *)0x31060118)

#define TxDescriptor (*(volatile unsigned *)0x3106011C)

#define TxStatus (*(volatile unsigned *)0x31060120)

#define TxDescriptorNumber (*(volatile unsigned *)0x31060124)

#define TxProduceIndex (*(volatile unsigned *)0x31060128)

#define TxConsumeIndex (*(volatile unsigned *)0x3106012C)

#define TSV0 (*(volatile unsigned *)0x31060158)
#define TSV0_CRC_error_MASK 0x1
#define TSV0_CRC_error 0x1
#define TSV0_CRC_error_BIT 0
#define TSV0_Length_check_error_MASK 0x2
#define TSV0_Length_check_error 0x2
#define TSV0_Length_check_error_BIT 1
#define TSV0_Length_out_of_range_MASK 0x4
#define TSV0_Length_out_of_range 0x4
#define TSV0_Length_out_of_range_BIT 2
#define TSV0_Done_MASK 0x8
#define TSV0_Done 0x8
#define TSV0_Done_BIT 3
#define TSV0_Multicast_MASK 0x10
#define TSV0_Multicast 0x10
#define TSV0_Multicast_BIT 4
#define TSV0_Broadcast_MASK 0x20
#define TSV0_Broadcast 0x20
#define TSV0_Broadcast_BIT 5
#define TSV0_Packet_Defer_MASK 0x40
#define TSV0_Packet_Defer 0x40
#define TSV0_Packet_Defer_BIT 6
#define TSV0_Excessive_Defer_MASK 0x80
#define TSV0_Excessive_Defer 0x80
#define TSV0_Excessive_Defer_BIT 7
#define TSV0_Excessive_Collison_MASK 0x100
#define TSV0_Excessive_Collison 0x100
#define TSV0_Excessive_Collison_BIT 8
#define TSV0_Late_Collison_MASK 0x200
#define TSV0_Late_Collison 0x200
#define TSV0_Late_Collison_BIT 9
#define TSV0_Giant_MASK 0x400
#define TSV0_Giant 0x400
#define TSV0_Giant_BIT 10
#define TSV0_Underrun_MASK 0x800
#define TSV0_Underrun 0x800
#define TSV0_Underrun_BIT 11
#define TSV0_Total_bytes_MASK 0xFFFF000
#define TSV0_Total_bytes_BIT 12
#define TSV0_Control_frame_MASK 0x10000000
#define TSV0_Control_frame 0x10000000
#define TSV0_Control_frame_BIT 28
#define TSV0_Pause_MASK 0x20000000
#define TSV0_Pause 0x20000000
#define TSV0_Pause_BIT 29
#define TSV0_Backpressure_MASK 0x40000000
#define TSV0_Backpressure 0x40000000
#define TSV0_Backpressure_BIT 30
#define TSV0_VLAN_MASK 0x80000000
#define TSV0_VLAN 0x80000000
#define TSV0_VLAN_BIT 31

#define TSV1 (*(volatile unsigned *)0x3106015C)
#define TSV1_Transmit_byte_count_MASK 0xFFFF
#define TSV1_Transmit_byte_count_BIT 0
#define TSV1_Transmit_collision_count_MASK 0xF0000
#define TSV1_Transmit_collision_count_BIT 16

#define RSV (*(volatile unsigned *)0x31060160)
#define RSV_Receive_byte_count_MASK 0xFFFF
#define RSV_Receive_byte_count_BIT 0
#define RSV_Packet_previously_ignored_MASK 0x10000
#define RSV_Packet_previously_ignored 0x10000
#define RSV_Packet_previously_ignored_BIT 16
#define RSV_RXDV_event_previously_seen_MASK 0x20000
#define RSV_RXDV_event_previously_seen 0x20000
#define RSV_RXDV_event_previously_seen_BIT 17
#define RSV_Carrier_event_previously_seen_MASK 0x40000
#define RSV_Carrier_event_previously_seen 0x40000
#define RSV_Carrier_event_previously_seen_BIT 18
#define RSV_Receive_code_violation_MASK 0x80000
#define RSV_Receive_code_violation 0x80000
#define RSV_Receive_code_violation_BIT 19
#define RSV_CRC_error_MASK 0x100000
#define RSV_CRC_error 0x100000
#define RSV_CRC_error_BIT 20
#define RSV_Length_check_error_MASK 0x200000
#define RSV_Length_check_error 0x200000
#define RSV_Length_check_error_BIT 21
#define RSV_Length_out_of_range_MASK 0x400000
#define RSV_Length_out_of_range 0x400000
#define RSV_Length_out_of_range_BIT 22
#define RSV_Receive_OK_MASK 0x800000
#define RSV_Receive_OK 0x800000
#define RSV_Receive_OK_BIT 23
#define RSV_Multicast_MASK 0x1000000
#define RSV_Multicast 0x1000000
#define RSV_Multicast_BIT 24
#define RSV_Broadcast_MASK 0x2000000
#define RSV_Broadcast 0x2000000
#define RSV_Broadcast_BIT 25
#define RSV_Dribble_Nibble_MASK 0x4000000
#define RSV_Dribble_Nibble 0x4000000
#define RSV_Dribble_Nibble_BIT 26
#define RSV_Control_frame_MASK 0x8000000
#define RSV_Control_frame 0x8000000
#define RSV_Control_frame_BIT 27
#define RSV_PAUSE_MASK 0x10000000
#define RSV_PAUSE 0x10000000
#define RSV_PAUSE_BIT 28
#define RSV_Unsupported_Opcode_MASK 0x20000000
#define RSV_Unsupported_Opcode 0x20000000
#define RSV_Unsupported_Opcode_BIT 29
#define RSV_VLAN_MASK 0x40000000
#define RSV_VLAN 0x40000000
#define RSV_VLAN_BIT 30

#define FlowControlCounter (*(volatile unsigned *)0x31060170)
#define FlowControlCounter_MirrorCounter_MASK 0xFFFF
#define FlowControlCounter_MirrorCounter_BIT 0
#define FlowControlCounter_Pause_Timer_MASK 0xFFFF0000
#define FlowControlCounter_Pause_Timer_BIT 16

#define FlowControlStatus (*(volatile unsigned *)0x31060174)

#define RxFilterCtrl (*(volatile unsigned *)0x31060200)
#define RxFilterCtrl_AcceptUnicastEn_MASK 0x1
#define RxFilterCtrl_AcceptUnicastEn 0x1
#define RxFilterCtrl_AcceptUnicastEn_BIT 0
#define RxFilterCtrl_AcceptBroadcastEn_MASK 0x2
#define RxFilterCtrl_AcceptBroadcastEn 0x2
#define RxFilterCtrl_AcceptBroadcastEn_BIT 1
#define RxFilterCtrl_AcceptMulticastEn_MASK 0x4
#define RxFilterCtrl_AcceptMulticastEn 0x4
#define RxFilterCtrl_AcceptMulticastEn_BIT 2
#define RxFilterCtrl_AcceptUnicastHashEn_MASK 0x8
#define RxFilterCtrl_AcceptUnicastHashEn 0x8
#define RxFilterCtrl_AcceptUnicastHashEn_BIT 3
#define RxFilterCtrl_AcceptMulticastHashEn_MASK 0x10
#define RxFilterCtrl_AcceptMulticastHashEn 0x10
#define RxFilterCtrl_AcceptMulticastHashEn_BIT 4
#define RxFilterCtrl_AcceptPerfectEn_MASK 0x20
#define RxFilterCtrl_AcceptPerfectEn 0x20
#define RxFilterCtrl_AcceptPerfectEn_BIT 5
#define RxFilterCtrl_MagicPacketEnWoL_MASK 0x1000
#define RxFilterCtrl_MagicPacketEnWoL 0x1000
#define RxFilterCtrl_MagicPacketEnWoL_BIT 12
#define RxFilterCtrl_RxFilterEnWoL_MASK 0x2000
#define RxFilterCtrl_RxFilterEnWoL 0x2000
#define RxFilterCtrl_RxFilterEnWoL_BIT 13

#define RxFilterWoLStatus (*(volatile unsigned *)0x31060204)
#define RxFilterWoLStatus_AcceptUnicastWoL_MASK 0x1
#define RxFilterWoLStatus_AcceptUnicastWoL 0x1
#define RxFilterWoLStatus_AcceptUnicastWoL_BIT 0
#define RxFilterWoLStatus_AcceptBroadcastWoL_MASK 0x2
#define RxFilterWoLStatus_AcceptBroadcastWoL 0x2
#define RxFilterWoLStatus_AcceptBroadcastWoL_BIT 1
#define RxFilterWoLStatus_AcceptMulticastWoL_MASK 0x4
#define RxFilterWoLStatus_AcceptMulticastWoL 0x4
#define RxFilterWoLStatus_AcceptMulticastWoL_BIT 2
#define RxFilterWoLStatus_AcceptUnicastHashWoL_MASK 0x8
#define RxFilterWoLStatus_AcceptUnicastHashWoL 0x8
#define RxFilterWoLStatus_AcceptUnicastHashWoL_BIT 3
#define RxFilterWoLStatus_AcceptMulticastHashWoL_MASK 0x10
#define RxFilterWoLStatus_AcceptMulticastHashWoL 0x10
#define RxFilterWoLStatus_AcceptMulticastHashWoL_BIT 4
#define RxFilterWoLStatus_AcceptPerfectWoL_MASK 0x20
#define RxFilterWoLStatus_AcceptPerfectWoL 0x20
#define RxFilterWoLStatus_AcceptPerfectWoL_BIT 5
#define RxFilterWoLStatus_RxFilterWoL_MASK 0x80
#define RxFilterWoLStatus_RxFilterWoL 0x80
#define RxFilterWoLStatus_RxFilterWoL_BIT 7
#define RxFilterWoLStatus_MagicPacketWoL_MASK 0x100
#define RxFilterWoLStatus_MagicPacketWoL 0x100
#define RxFilterWoLStatus_MagicPacketWoL_BIT 8

#define RxFilterWoLClear (*(volatile unsigned *)0x31060208)
#define RxFilterWoLClear_AcceptUnicastWoLClr_MASK 0x1
#define RxFilterWoLClear_AcceptUnicastWoLClr 0x1
#define RxFilterWoLClear_AcceptUnicastWoLClr_BIT 0
#define RxFilterWoLClear_AcceptBroadcastWoLClr_MASK 0x2
#define RxFilterWoLClear_AcceptBroadcastWoLClr 0x2
#define RxFilterWoLClear_AcceptBroadcastWoLClr_BIT 1
#define RxFilterWoLClear_AcceptMulticastWoLClr_MASK 0x4
#define RxFilterWoLClear_AcceptMulticastWoLClr 0x4
#define RxFilterWoLClear_AcceptMulticastWoLClr_BIT 2
#define RxFilterWoLClear_AcceptUnicastHashWoLClr_MASK 0x8
#define RxFilterWoLClear_AcceptUnicastHashWoLClr 0x8
#define RxFilterWoLClear_AcceptUnicastHashWoLClr_BIT 3
#define RxFilterWoLClear_AcceptMulticastHashWoLClr_MASK 0x10
#define RxFilterWoLClear_AcceptMulticastHashWoLClr 0x10
#define RxFilterWoLClear_AcceptMulticastHashWoLClr_BIT 4
#define RxFilterWoLClear_AcceptPerfectWoLClr_MASK 0x20
#define RxFilterWoLClear_AcceptPerfectWoLClr 0x20
#define RxFilterWoLClear_AcceptPerfectWoLClr_BIT 5
#define RxFilterWoLClear_RxFilterWoLClr_MASK 0x80
#define RxFilterWoLClear_RxFilterWoLClr 0x80
#define RxFilterWoLClear_RxFilterWoLClr_BIT 7
#define RxFilterWoLClear_MagicPacketWoLClr_MASK 0x100
#define RxFilterWoLClear_MagicPacketWoLClr 0x100
#define RxFilterWoLClear_MagicPacketWoLClr_BIT 8

#define HashFilterL (*(volatile unsigned *)0x31060210)

#define HashFilterH (*(volatile unsigned *)0x31060214)

#define IntStatus (*(volatile unsigned *)0x31060FE0)
#define IntStatus_RxOverrunInt_MASK 0x1
#define IntStatus_RxOverrunInt 0x1
#define IntStatus_RxOverrunInt_BIT 0
#define IntStatus_RxErrorInt_MASK 0x2
#define IntStatus_RxErrorInt 0x2
#define IntStatus_RxErrorInt_BIT 1
#define IntStatus_RxFinishedInt_MASK 0x4
#define IntStatus_RxFinishedInt 0x4
#define IntStatus_RxFinishedInt_BIT 2
#define IntStatus_RxDoneInt_MASK 0x8
#define IntStatus_RxDoneInt 0x8
#define IntStatus_RxDoneInt_BIT 3
#define IntStatus_TxUnderrunInt_MASK 0x10
#define IntStatus_TxUnderrunInt 0x10
#define IntStatus_TxUnderrunInt_BIT 4
#define IntStatus_TxErrorInt_MASK 0x20
#define IntStatus_TxErrorInt 0x20
#define IntStatus_TxErrorInt_BIT 5
#define IntStatus_TxFinishedInt_MASK 0x40
#define IntStatus_TxFinishedInt 0x40
#define IntStatus_TxFinishedInt_BIT 6
#define IntStatus_TxDoneInt_MASK 0x80
#define IntStatus_TxDoneInt 0x80
#define IntStatus_TxDoneInt_BIT 7
#define IntStatus_SoftInt_MASK 0x1000
#define IntStatus_SoftInt 0x1000
#define IntStatus_SoftInt_BIT 12
#define IntStatus_WakeupInt_MASK 0x2000
#define IntStatus_WakeupInt 0x2000
#define IntStatus_WakeupInt_BIT 13

#define IntEnable (*(volatile unsigned *)0x31060FE4)
#define IntEnable_RxOverrunIntEn_MASK 0x1
#define IntEnable_RxOverrunIntEn 0x1
#define IntEnable_RxOverrunIntEn_BIT 0
#define IntEnable_RxErrorIntEn_MASK 0x2
#define IntEnable_RxErrorIntEn 0x2
#define IntEnable_RxErrorIntEn_BIT 1
#define IntEnable_RxFinishedIntEn_MASK 0x4
#define IntEnable_RxFinishedIntEn 0x4
#define IntEnable_RxFinishedIntEn_BIT 2
#define IntEnable_RxDoneIntEn_MASK 0x8
#define IntEnable_RxDoneIntEn 0x8
#define IntEnable_RxDoneIntEn_BIT 3
#define IntEnable_TxUnderrunIntEn_MASK 0x10
#define IntEnable_TxUnderrunIntEn 0x10
#define IntEnable_TxUnderrunIntEn_BIT 4
#define IntEnable_TxErrorIntEn_MASK 0x20
#define IntEnable_TxErrorIntEn 0x20
#define IntEnable_TxErrorIntEn_BIT 5
#define IntEnable_TxFinishedIntEn_MASK 0x40
#define IntEnable_TxFinishedIntEn 0x40
#define IntEnable_TxFinishedIntEn_BIT 6
#define IntEnable_TxDoneIntEn_MASK 0x80
#define IntEnable_TxDoneIntEn 0x80
#define IntEnable_TxDoneIntEn_BIT 7
#define IntEnable_SoftIntEn_MASK 0x1000
#define IntEnable_SoftIntEn 0x1000
#define IntEnable_SoftIntEn_BIT 12
#define IntEnable_WakeupIntEn_MASK 0x2000
#define IntEnable_WakeupIntEn 0x2000
#define IntEnable_WakeupIntEn_BIT 13

#define IntClear (*(volatile unsigned *)0x31060FE8)
#define IntClear_RxOverrunIntClr_MASK 0x1
#define IntClear_RxOverrunIntClr 0x1
#define IntClear_RxOverrunIntClr_BIT 0
#define IntClear_RxErrorIntClr_MASK 0x2
#define IntClear_RxErrorIntClr 0x2
#define IntClear_RxErrorIntClr_BIT 1
#define IntClear_RxFinishedIntClr_MASK 0x4
#define IntClear_RxFinishedIntClr 0x4
#define IntClear_RxFinishedIntClr_BIT 2
#define IntClear_RxDoneIntClr_MASK 0x8
#define IntClear_RxDoneIntClr 0x8
#define IntClear_RxDoneIntClr_BIT 3
#define IntClear_TxUnderrunIntClr_MASK 0x10
#define IntClear_TxUnderrunIntClr 0x10
#define IntClear_TxUnderrunIntClr_BIT 4
#define IntClear_TxErrorIntClr_MASK 0x20
#define IntClear_TxErrorIntClr 0x20
#define IntClear_TxErrorIntClr_BIT 5
#define IntClear_TxFinishedIntClr_MASK 0x40
#define IntClear_TxFinishedIntClr 0x40
#define IntClear_TxFinishedIntClr_BIT 6
#define IntClear_TxDoneIntClr_MASK 0x80
#define IntClear_TxDoneIntClr 0x80
#define IntClear_TxDoneIntClr_BIT 7
#define IntClear_SoftIntClr_MASK 0x1000
#define IntClear_SoftIntClr 0x1000
#define IntClear_SoftIntClr_BIT 12
#define IntClear_WakeupIntClr_MASK 0x2000
#define IntClear_WakeupIntClr 0x2000
#define IntClear_WakeupIntClr_BIT 13

#define IntSet (*(volatile unsigned *)0x31060FEC)
#define IntSet_RxOverrunIntSet_MASK 0x1
#define IntSet_RxOverrunIntSet 0x1
#define IntSet_RxOverrunIntSet_BIT 0
#define IntSet_RxErrorIntSet_MASK 0x2
#define IntSet_RxErrorIntSet 0x2
#define IntSet_RxErrorIntSet_BIT 1
#define IntSet_RxFinishedIntSet_MASK 0x4
#define IntSet_RxFinishedIntSet 0x4
#define IntSet_RxFinishedIntSet_BIT 2
#define IntSet_RxDoneIntSet_MASK 0x8
#define IntSet_RxDoneIntSet 0x8
#define IntSet_RxDoneIntSet_BIT 3
#define IntSet_TxUnderrunIntSet_MASK 0x10
#define IntSet_TxUnderrunIntSet 0x10
#define IntSet_TxUnderrunIntSet_BIT 4
#define IntSet_TxErrorIntSet_MASK 0x20
#define IntSet_TxErrorIntSet 0x20
#define IntSet_TxErrorIntSet_BIT 5
#define IntSet_TxFinishedIntSet_MASK 0x40
#define IntSet_TxFinishedIntSet 0x40
#define IntSet_TxFinishedIntSet_BIT 6
#define IntSet_TxDoneIntSet_MASK 0x80
#define IntSet_TxDoneIntSet 0x80
#define IntSet_TxDoneIntSet_BIT 7
#define IntSet_SoftIntSet_MASK 0x1000
#define IntSet_SoftIntSet 0x1000
#define IntSet_SoftIntSet_BIT 12
#define IntSet_WakeupIntSet_MASK 0x2000
#define IntSet_WakeupIntSet 0x2000
#define IntSet_WakeupIntSet_BIT 13

#define PowerDown (*(volatile unsigned *)0x31060FF4)
#define PowerDown_PowerDownMACAHB_MASK 0x80000000
#define PowerDown_PowerDownMACAHB 0x80000000
#define PowerDown_PowerDownMACAHB_BIT 31

#define EMCControl (*(volatile unsigned long *)0x31080000)
#define EMCControl_E_MASK 0x1
#define EMCControl_E 0x1
#define EMCControl_E_BIT 0
#define EMCControl_M_MASK 0x2
#define EMCControl_M 0x2
#define EMCControl_M_BIT 1
#define EMCControl_L_MASK 0x4
#define EMCControl_L 0x4
#define EMCControl_L_BIT 2

#define EMCStatus (*(volatile unsigned long *)0x31080004)
#define EMCStatus_B_MASK 0x1
#define EMCStatus_B 0x1
#define EMCStatus_B_BIT 0
#define EMCStatus_S_MASK 0x2
#define EMCStatus_S 0x2
#define EMCStatus_S_BIT 1
#define EMCStatus_SA_MASK 0x4
#define EMCStatus_SA 0x4
#define EMCStatus_SA_BIT 2

#define EMCConfig (*(volatile unsigned long *)0x31080008)
#define EMCConfig_Endian_mode_MASK 0x1
#define EMCConfig_Endian_mode 0x1
#define EMCConfig_Endian_mode_BIT 0

#define EMCDynamicControl (*(volatile unsigned long *)0x31080020)
#define EMCDynamicControl_CE_MASK 0x1
#define EMCDynamicControl_CE 0x1
#define EMCDynamicControl_CE_BIT 0
#define EMCDynamicControl_CS_MASK 0x2
#define EMCDynamicControl_CS 0x2
#define EMCDynamicControl_CS_BIT 1
#define EMCDynamicControl_SR_MASK 0x4
#define EMCDynamicControl_SR 0x4
#define EMCDynamicControl_SR_BIT 2
#define EMCDynamicControl_MMC_MASK 0x20
#define EMCDynamicControl_MMC 0x20
#define EMCDynamicControl_MMC_BIT 5
#define EMCDynamicControl_I_MASK 0x180
#define EMCDynamicControl_I_BIT 7
#define EMCDynamicControl_DP_MASK 0x2000
#define EMCDynamicControl_DP 0x2000
#define EMCDynamicControl_DP_BIT 13

#define EMCDynamicRefresh (*(volatile unsigned long *)0x31080024)
#define EMCDynamicRefresh_REFRESH_MASK 0x7FF
#define EMCDynamicRefresh_REFRESH_BIT 0

#define EMCDynamicReadConfig (*(volatile unsigned long *)0x31080028)
#define EMCDynamicReadConfig_RD_MASK 0x3
#define EMCDynamicReadConfig_RD_BIT 0

#define EMCDynamictRP (*(volatile unsigned long *)0x31080030)
#define EMCDynamictRP_tRP_MASK 0xF
#define EMCDynamictRP_tRP_BIT 0

#define EMCDynamictRAS (*(volatile unsigned long *)0x31080034)
#define EMCDynamictRAS_tRAS_MASK 0xF
#define EMCDynamictRAS_tRAS_BIT 0

#define EMCDynamictSREX (*(volatile unsigned long *)0x31080038)
#define EMCDynamictSREX_tSREX_MASK 0xF
#define EMCDynamictSREX_tSREX_BIT 0

#define EMCDynamictWR (*(volatile unsigned long *)0x31080044)
#define EMCDynamictWR_tWR_MASK 0xF
#define EMCDynamictWR_tWR_BIT 0

#define EMCDynamictRC (*(volatile unsigned long *)0x31080048)
#define EMCDynamictRC_tRC_MASK 0xF
#define EMCDynamictRC_tRC_BIT 0

#define EMCDynamictRFC (*(volatile unsigned long *)0x3108004C)
#define EMCDynamictRFC_tRFC_MASK 0xF
#define EMCDynamictRFC_tRFC_BIT 0

#define EMCDynamictXSR (*(volatile unsigned long *)0x31080050)
#define EMCDynamictXSR_tXSR_MASK 0xF
#define EMCDynamictXSR_tXSR_BIT 0

#define EMCDynamictRRD (*(volatile unsigned long *)0x31080054)
#define EMCDynamictRRD_tRRD_MASK 0xF
#define EMCDynamictRRD_tRRD_BIT 0

#define EMCDynamictMRD (*(volatile unsigned long *)0x31080058)
#define EMCDynamictMRD_tMRD_MASK 0xF
#define EMCDynamictMRD_tMRD_BIT 0

#define EMCDynamictCDLR (*(volatile unsigned long *)0x3108005C)
#define EMCDynamictCDLR_tCDLR_MASK 0xF
#define EMCDynamictCDLR_tCDLR_BIT 0

#define EMCStaticExtendedWait (*(volatile unsigned long *)0x31080080)
#define EMCStaticExtendedWait_EXTENDEDWAIT_MASK 0x3FF
#define EMCStaticExtendedWait_EXTENDEDWAIT_BIT 0

#define EMCDynamicConfig0 (*(volatile unsigned *)0x31080100)
#define EMCDynamicConfig0_MD_MASK 0x7
#define EMCDynamicConfig0_MD_BIT 0
#define EMCDynamicConfig0_AM_MASK 0x7F80
#define EMCDynamicConfig0_AM_BIT 7
#define EMCDynamicConfig0_P_MASK 0x100000
#define EMCDynamicConfig0_P 0x100000
#define EMCDynamicConfig0_P_BIT 20

#define EMCDynamicRasCas0 (*(volatile unsigned *)0x31080104)
#define EMCDynamicRasCas0_RAS_MASK 0x3
#define EMCDynamicRasCas0_RAS_BIT 0
#define EMCDynamicRasCas0_CAS_MASK 0x300
#define EMCDynamicRasCas0_CAS_BIT 8

#define EMCDynamicConfig1 (*(volatile unsigned *)0x31080120)
#define EMCDynamicConfig1_MD_MASK 0x7
#define EMCDynamicConfig1_MD_BIT 0
#define EMCDynamicConfig1_AM_MASK 0x7F80
#define EMCDynamicConfig1_AM_BIT 7
#define EMCDynamicConfig1_P_MASK 0x100000
#define EMCDynamicConfig1_P 0x100000
#define EMCDynamicConfig1_P_BIT 20

#define EMCDynamicRasCas1 (*(volatile unsigned *)0x31080124)
#define EMCDynamicRasCas1_RAS_MASK 0x3
#define EMCDynamicRasCas1_RAS_BIT 0
#define EMCDynamicRasCas1_CAS_MASK 0x300
#define EMCDynamicRasCas1_CAS_BIT 8

#define EMCStaticConfig0 (*(volatile unsigned *)0x31080200)
#define EMCStaticConfig0_MW_MASK 0x3
#define EMCStaticConfig0_MW_BIT 0
#define EMCStaticConfig0_PM_MASK 0x8
#define EMCStaticConfig0_PM 0x8
#define EMCStaticConfig0_PM_BIT 3
#define EMCStaticConfig0_PC_MASK 0x40
#define EMCStaticConfig0_PC 0x40
#define EMCStaticConfig0_PC_BIT 6
#define EMCStaticConfig0_PB_MASK 0x80
#define EMCStaticConfig0_PB 0x80
#define EMCStaticConfig0_PB_BIT 7
#define EMCStaticConfig0_EW_MASK 0x100
#define EMCStaticConfig0_EW 0x100
#define EMCStaticConfig0_EW_BIT 8
#define EMCStaticConfig0_B_MASK 0x80000
#define EMCStaticConfig0_B 0x80000
#define EMCStaticConfig0_B_BIT 19
#define EMCStaticConfig0_P_MASK 0x100000
#define EMCStaticConfig0_P 0x100000
#define EMCStaticConfig0_P_BIT 20

#define EMCStaticWaitWen0 (*(volatile unsigned *)0x31080204)
#define EMCStaticWaitWen0_WAITWEN_MASK 0xF
#define EMCStaticWaitWen0_WAITWEN_BIT 0

#define EMCStaticWaitOen0 (*(volatile unsigned *)0x31080208)
#define EMCStaticWaitOen0_WAITOEN_MASK 0xF
#define EMCStaticWaitOen0_WAITOEN_BIT 0

#define EMCStaticWaitRd0 (*(volatile unsigned *)0x3108020C)
#define EMCStaticWaitRd0_WAITRD_MASK 0x1F
#define EMCStaticWaitRd0_WAITRD_BIT 0

#define EMCStaticWaitPage0 (*(volatile unsigned *)0x31080210)
#define EMCStaticWaitPage0_WAITPAGE_MASK 0x1F
#define EMCStaticWaitPage0_WAITPAGE_BIT 0

#define EMCStaticWaitWr0 (*(volatile unsigned *)0x31080214)
#define EMCStaticWaitWr0_WAITWR_MASK 0x1F
#define EMCStaticWaitWr0_WAITWR_BIT 0

#define EMCStaticWaitTurn0 (*(volatile unsigned *)0x31080218)
#define EMCStaticWaitTurn0_WAITTURN_MASK 0xF
#define EMCStaticWaitTurn0_WAITTURN_BIT 0

#define EMCStaticConfig1 (*(volatile unsigned *)0x31080220)
#define EMCStaticConfig1_MW_MASK 0x3
#define EMCStaticConfig1_MW_BIT 0
#define EMCStaticConfig1_PM_MASK 0x8
#define EMCStaticConfig1_PM 0x8
#define EMCStaticConfig1_PM_BIT 3
#define EMCStaticConfig1_PC_MASK 0x40
#define EMCStaticConfig1_PC 0x40
#define EMCStaticConfig1_PC_BIT 6
#define EMCStaticConfig1_PB_MASK 0x80
#define EMCStaticConfig1_PB 0x80
#define EMCStaticConfig1_PB_BIT 7
#define EMCStaticConfig1_EW_MASK 0x100
#define EMCStaticConfig1_EW 0x100
#define EMCStaticConfig1_EW_BIT 8
#define EMCStaticConfig1_B_MASK 0x80000
#define EMCStaticConfig1_B 0x80000
#define EMCStaticConfig1_B_BIT 19
#define EMCStaticConfig1_P_MASK 0x100000
#define EMCStaticConfig1_P 0x100000
#define EMCStaticConfig1_P_BIT 20

#define EMCStaticWaitWen1 (*(volatile unsigned *)0x31080224)
#define EMCStaticWaitWen1_WAITWEN_MASK 0xF
#define EMCStaticWaitWen1_WAITWEN_BIT 0

#define EMCStaticWaitOen1 (*(volatile unsigned *)0x31080228)
#define EMCStaticWaitOen1_WAITOEN_MASK 0xF
#define EMCStaticWaitOen1_WAITOEN_BIT 0

#define EMCStaticWaitRd1 (*(volatile unsigned *)0x3108022C)
#define EMCStaticWaitRd1_WAITRD_MASK 0x1F
#define EMCStaticWaitRd1_WAITRD_BIT 0

#define EMCStaticWaitPage1 (*(volatile unsigned *)0x31080230)
#define EMCStaticWaitPage1_WAITPAGE_MASK 0x1F
#define EMCStaticWaitPage1_WAITPAGE_BIT 0

#define EMCStaticWaitWr1 (*(volatile unsigned *)0x31080234)
#define EMCStaticWaitWr1_WAITWR_MASK 0x1F
#define EMCStaticWaitWr1_WAITWR_BIT 0

#define EMCStaticWaitTurn1 (*(volatile unsigned *)0x31080238)
#define EMCStaticWaitTurn1_WAITTURN_MASK 0xF
#define EMCStaticWaitTurn1_WAITTURN_BIT 0

#define EMCStaticConfig2 (*(volatile unsigned *)0x31080240)
#define EMCStaticConfig2_MW_MASK 0x3
#define EMCStaticConfig2_MW_BIT 0
#define EMCStaticConfig2_PM_MASK 0x8
#define EMCStaticConfig2_PM 0x8
#define EMCStaticConfig2_PM_BIT 3
#define EMCStaticConfig2_PC_MASK 0x40
#define EMCStaticConfig2_PC 0x40
#define EMCStaticConfig2_PC_BIT 6
#define EMCStaticConfig2_PB_MASK 0x80
#define EMCStaticConfig2_PB 0x80
#define EMCStaticConfig2_PB_BIT 7
#define EMCStaticConfig2_EW_MASK 0x100
#define EMCStaticConfig2_EW 0x100
#define EMCStaticConfig2_EW_BIT 8
#define EMCStaticConfig2_B_MASK 0x80000
#define EMCStaticConfig2_B 0x80000
#define EMCStaticConfig2_B_BIT 19
#define EMCStaticConfig2_P_MASK 0x100000
#define EMCStaticConfig2_P 0x100000
#define EMCStaticConfig2_P_BIT 20

#define EMCStaticWaitWen2 (*(volatile unsigned *)0x31080244)
#define EMCStaticWaitWen2_WAITWEN_MASK 0xF
#define EMCStaticWaitWen2_WAITWEN_BIT 0

#define EMCStaticWaitOen2 (*(volatile unsigned *)0x31080248)
#define EMCStaticWaitOen2_WAITOEN_MASK 0xF
#define EMCStaticWaitOen2_WAITOEN_BIT 0

#define EMCStaticWaitRd2 (*(volatile unsigned *)0x3108024C)
#define EMCStaticWaitRd2_WAITRD_MASK 0x1F
#define EMCStaticWaitRd2_WAITRD_BIT 0

#define EMCStaticWaitPage2 (*(volatile unsigned *)0x31080250)
#define EMCStaticWaitPage2_WAITPAGE_MASK 0x1F
#define EMCStaticWaitPage2_WAITPAGE_BIT 0

#define EMCStaticWaitWr2 (*(volatile unsigned *)0x31080254)
#define EMCStaticWaitWr2_WAITWR_MASK 0x1F
#define EMCStaticWaitWr2_WAITWR_BIT 0

#define EMCStaticWaitTurn2 (*(volatile unsigned *)0x31080258)
#define EMCStaticWaitTurn2_WAITTURN_MASK 0xF
#define EMCStaticWaitTurn2_WAITTURN_BIT 0

#define EMCStaticConfig3 (*(volatile unsigned *)0x31080260)
#define EMCStaticConfig3_MW_MASK 0x3
#define EMCStaticConfig3_MW_BIT 0
#define EMCStaticConfig3_PM_MASK 0x8
#define EMCStaticConfig3_PM 0x8
#define EMCStaticConfig3_PM_BIT 3
#define EMCStaticConfig3_PC_MASK 0x40
#define EMCStaticConfig3_PC 0x40
#define EMCStaticConfig3_PC_BIT 6
#define EMCStaticConfig3_PB_MASK 0x80
#define EMCStaticConfig3_PB 0x80
#define EMCStaticConfig3_PB_BIT 7
#define EMCStaticConfig3_EW_MASK 0x100
#define EMCStaticConfig3_EW 0x100
#define EMCStaticConfig3_EW_BIT 8
#define EMCStaticConfig3_B_MASK 0x80000
#define EMCStaticConfig3_B 0x80000
#define EMCStaticConfig3_B_BIT 19
#define EMCStaticConfig3_P_MASK 0x100000
#define EMCStaticConfig3_P 0x100000
#define EMCStaticConfig3_P_BIT 20

#define EMCStaticWaitWen3 (*(volatile unsigned *)0x31080264)
#define EMCStaticWaitWen3_WAITWEN_MASK 0xF
#define EMCStaticWaitWen3_WAITWEN_BIT 0

#define EMCStaticWaitOen3 (*(volatile unsigned *)0x31080268)
#define EMCStaticWaitOen3_WAITOEN_MASK 0xF
#define EMCStaticWaitOen3_WAITOEN_BIT 0

#define EMCStaticWaitRd3 (*(volatile unsigned *)0x3108026C)
#define EMCStaticWaitRd3_WAITRD_MASK 0x1F
#define EMCStaticWaitRd3_WAITRD_BIT 0

#define EMCStaticWaitPage3 (*(volatile unsigned *)0x31080270)
#define EMCStaticWaitPage3_WAITPAGE_MASK 0x1F
#define EMCStaticWaitPage3_WAITPAGE_BIT 0

#define EMCStaticWaitWr3 (*(volatile unsigned *)0x31080274)
#define EMCStaticWaitWr3_WAITWR_MASK 0x1F
#define EMCStaticWaitWr3_WAITWR_BIT 0

#define EMCStaticWaitTurn3 (*(volatile unsigned *)0x31080278)
#define EMCStaticWaitTurn3_WAITTURN_MASK 0xF
#define EMCStaticWaitTurn3_WAITTURN_BIT 0

#define EMCAHBControl0 (*(volatile unsigned *)0x31080400)
#define EMCAHBControl0_E_MASK 0x1
#define EMCAHBControl0_E 0x1
#define EMCAHBControl0_E_BIT 0

#define EMCAHBStatus0 (*(volatile unsigned *)0x31080404)
#define EMCAHBStatus0_S_MASK 0x2
#define EMCAHBStatus0_S 0x2
#define EMCAHBStatus0_S_BIT 1

#define EMCAHBTimeOut0 (*(volatile unsigned *)0x31080408)
#define EMCAHBTimeOut0_AHBTIMEOUT_MASK 0x3FF
#define EMCAHBTimeOut0_AHBTIMEOUT_BIT 0

#define EMCAHBControl3 (*(volatile unsigned *)0x31080460)
#define EMCAHBControl3_E_MASK 0x1
#define EMCAHBControl3_E 0x1
#define EMCAHBControl3_E_BIT 0

#define EMCAHBStatus3 (*(volatile unsigned *)0x31080464)
#define EMCAHBStatus3_S_MASK 0x2
#define EMCAHBStatus3_S 0x2
#define EMCAHBStatus3_S_BIT 1

#define EMCAHBTimeOut3 (*(volatile unsigned *)0x31080468)
#define EMCAHBTimeOut3_AHBTIMEOUT_MASK 0x3FF
#define EMCAHBTimeOut3_AHBTIMEOUT_BIT 0

#define EMCAHBControl4 (*(volatile unsigned *)0x31080480)
#define EMCAHBControl4_E_MASK 0x1
#define EMCAHBControl4_E 0x1
#define EMCAHBControl4_E_BIT 0

#define EMCAHBStatus4 (*(volatile unsigned *)0x31080484)
#define EMCAHBStatus4_S_MASK 0x2
#define EMCAHBStatus4_S 0x2
#define EMCAHBStatus4_S_BIT 1

#define EMCAHBTimeOut4 (*(volatile unsigned *)0x31080488)
#define EMCAHBTimeOut4_AHBTIMEOUT_MASK 0x3FF
#define EMCAHBTimeOut4_AHBTIMEOUT_BIT 0

#define BOOT_MAP (*(volatile unsigned *)0x40004014)
#define BOOT_MAP_IRAM_not_IROM_MASK 0x1
#define BOOT_MAP_IRAM_not_IROM 0x1
#define BOOT_MAP_IRAM_not_IROM_BIT 0

#define PWR_CTRL (*(volatile unsigned *)0x40004044)
#define PWR_CTRL_Force_HCLK_and_ARMCLK_to_run_from_PERIPH_CLK_MASK 0x400
#define PWR_CTRL_Force_HCLK_and_ARMCLK_to_run_from_PERIPH_CLK 0x400
#define PWR_CTRL_Force_HCLK_and_ARMCLK_to_run_from_PERIPH_CLK_BIT 10
#define PWR_CTRL_EMCSREFREQ_MASK 0x200
#define PWR_CTRL_EMCSREFREQ 0x200
#define PWR_CTRL_EMCSREFREQ_BIT 9
#define PWR_CTRL_Update_EMCSREFREQ_MASK 0x100
#define PWR_CTRL_Update_EMCSREFREQ 0x100
#define PWR_CTRL_Update_EMCSREFREQ_BIT 8
#define PWR_CTRL_SDRAM_auto_exit_self_refresh_enabled_MASK 0x80
#define PWR_CTRL_SDRAM_auto_exit_self_refresh_enabled 0x80
#define PWR_CTRL_SDRAM_auto_exit_self_refresh_enabled_BIT 7
#define PWR_CTRL_HIGHCORE_pin_level_MASK 0x20
#define PWR_CTRL_HIGHCORE_pin_level 0x20
#define PWR_CTRL_HIGHCORE_pin_level_BIT 5
#define PWR_CTRL_SYSCLKEN_pin_level_MASK 0x10
#define PWR_CTRL_SYSCLKEN_pin_level 0x10
#define PWR_CTRL_SYSCLKEN_pin_level_BIT 4
#define PWR_CTRL_SYSCLKEN_pin_drives_high_when_external_input_clock_on_SYSXIN_is_requested_MASK 0x8
#define PWR_CTRL_SYSCLKEN_pin_drives_high_when_external_input_clock_on_SYSXIN_is_requested 0x8
#define PWR_CTRL_SYSCLKEN_pin_drives_high_when_external_input_clock_on_SYSXIN_is_requested_BIT 3
#define PWR_CTRL_RUN_mode_control_MASK 0x4
#define PWR_CTRL_RUN_mode_control 0x4
#define PWR_CTRL_RUN_mode_control_BIT 2
#define PWR_CTRL_Core_voltage_supply_level_signalling_control_MASK 0x2
#define PWR_CTRL_Core_voltage_supply_level_signalling_control 0x2
#define PWR_CTRL_Core_voltage_supply_level_signalling_control_BIT 1
#define PWR_CTRL_STOP_mode_control_register_MASK 0x1
#define PWR_CTRL_STOP_mode_control_register 0x1
#define PWR_CTRL_STOP_mode_control_register_BIT 0

#define OSC_CTRL (*(volatile unsigned *)0x4000404C)
#define OSC_CTRL_Add_load_capaciance_to_SYSX_IN_and_SYSX_OUT_MASK 0x1FC
#define OSC_CTRL_Add_load_capaciance_to_SYSX_IN_and_SYSX_OUT_BIT 2
#define OSC_CTRL_Main_oscillator_test_mode_MASK 0x2
#define OSC_CTRL_Main_oscillator_test_mode 0x2
#define OSC_CTRL_Main_oscillator_test_mode_BIT 1
#define OSC_CTRL_Main_oscillator_enable_MASK 0x1
#define OSC_CTRL_Main_oscillator_enable 0x1
#define OSC_CTRL_Main_oscillator_enable_BIT 0

#define SYSCLK_CTRL (*(volatile unsigned *)0x40004050)
#define SYSCLK_CTRL_Clock_switch_number_MASK 0xFFC
#define SYSCLK_CTRL_Clock_switch_number_BIT 2
#define SYSCLK_CTRL_Switch_to_13Mhz_clock_source_MASK 0x2
#define SYSCLK_CTRL_Switch_to_13Mhz_clock_source 0x2
#define SYSCLK_CTRL_Switch_to_13Mhz_clock_source_BIT 1
#define SYSCLK_CTRL_SYSCLK_MUX_Status_MASK 0x1
#define SYSCLK_CTRL_SYSCLK_MUX_Status 0x1
#define SYSCLK_CTRL_SYSCLK_MUX_Status_BIT 0

#define PLL397_CTRL (*(volatile unsigned *)0x40004048)
#define PLL397_CTRL_PLL_MSLOCK_status_MASK 0x400
#define PLL397_CTRL_PLL_MSLOCK_status 0x400
#define PLL397_CTRL_PLL_MSLOCK_status_BIT 10
#define PLL397_CTRL_PLL397_bypass_control_MASK 0x200
#define PLL397_CTRL_PLL397_bypass_control 0x200
#define PLL397_CTRL_PLL397_bypass_control_BIT 9
#define PLL397_CTRL_PLL397_charge_pump_bias_control_MASK 0x1C0
#define PLL397_CTRL_PLL397_charge_pump_bias_control_BIT 6
#define PLL397_CTRL_PLL397_operational_control_MASK 0x2
#define PLL397_CTRL_PLL397_operational_control 0x2
#define PLL397_CTRL_PLL397_operational_control_BIT 1
#define PLL397_CTRL_PLL_LOCK_status_MASK 0x1
#define PLL397_CTRL_PLL_LOCK_status 0x1
#define PLL397_CTRL_PLL_LOCK_status_BIT 0

#define HCLKPLL_CTRL (*(volatile unsigned *)0x40004058)
#define HCLKPLL_CTRL_PLL_Power_down_MASK 0x30000
#define HCLKPLL_CTRL_PLL_Power_down_BIT 16
#define HCLKPLL_CTRL_Bypass_control_MASK 0x18000
#define HCLKPLL_CTRL_Bypass_control_BIT 15
#define HCLKPLL_CTRL_Direct_output_control_MASK 0xC000
#define HCLKPLL_CTRL_Direct_output_control_BIT 14
#define HCLKPLL_CTRL_Feedback_divider_path_control_MASK 0x2000
#define HCLKPLL_CTRL_Feedback_divider_path_control 0x2000
#define HCLKPLL_CTRL_Feedback_divider_path_control_BIT 13
#define HCLKPLL_CTRL_PLL_post_divider_MASK 0x1800
#define HCLKPLL_CTRL_PLL_post_divider_BIT 11
#define HCLKPLL_CTRL_PLL_pre_divider_MASK 0x600
#define HCLKPLL_CTRL_PLL_pre_divider_BIT 9
#define HCLKPLL_CTRL_PLL_feedback_divider_MASK 0x1FE
#define HCLKPLL_CTRL_PLL_feedback_divider_BIT 1
#define HCLKPLL_CTRL_PLL_LOCK_status_MASK 0x1
#define HCLKPLL_CTRL_PLL_LOCK_status 0x1
#define HCLKPLL_CTRL_PLL_LOCK_status_BIT 0

#define HCLKDIV_CTRL (*(volatile unsigned *)0x40004040)
#define HCLKDIV_CTRL_DDRAM_CLK_control_MASK 0x180
#define HCLKDIV_CTRL_DDRAM_CLK_control_BIT 7
#define HCLKDIV_CTRL_PERIPH_CLK_divider_control_MASK 0x7C
#define HCLKDIV_CTRL_PERIPH_CLK_divider_control_BIT 2
#define HCLKDIV_CTRL_HCLK_divider_control_MASK 0x3
#define HCLKDIV_CTRL_HCLK_divider_control_BIT 0

#define TEST_CLK (*(volatile unsigned *)0x400040A4)
#define TEST_CLK_TST_CLK1_output_MASK 0x60
#define TEST_CLK_TST_CLK1_output_BIT 5
#define TEST_CLK_TST_CLK1_output_selected_MASK 0x10
#define TEST_CLK_TST_CLK1_output_selected 0x10
#define TEST_CLK_TST_CLK1_output_selected_BIT 4
#define TEST_CLK_TST_CLK2_output_MASK 0xE
#define TEST_CLK_TST_CLK2_output_BIT 1
#define TEST_CLK_TST_CLK2_output_selected_MASK 0x1
#define TEST_CLK_TST_CLK2_output_selected 0x1
#define TEST_CLK_TST_CLK2_output_selected_BIT 0

#define AUTOCLK_CTRL (*(volatile unsigned *)0x400040EC)
#define AUTOCLK_CTRL_Autoclock_enabled_on_USB_Slave_HCLK_MASK 0x40
#define AUTOCLK_CTRL_Autoclock_enabled_on_USB_Slave_HCLK 0x40
#define AUTOCLK_CTRL_Autoclock_enabled_on_USB_Slave_HCLK_BIT 6
#define AUTOCLK_CTRL_Autoclock_enabled_on_IRAM_MASK 0x2
#define AUTOCLK_CTRL_Autoclock_enabled_on_IRAM 0x2
#define AUTOCLK_CTRL_Autoclock_enabled_on_IRAM_BIT 1
#define AUTOCLK_CTRL_Autoclock_enabled_on_IROM_MASK 0x1
#define AUTOCLK_CTRL_Autoclock_enabled_on_IROM 0x1
#define AUTOCLK_CTRL_Autoclock_enabled_on_IROM_BIT 0

#define START_ER_INT (*(volatile unsigned *)0x40004020)
#define START_ER_INT_AD_IRQ_MASK 0x80000000
#define START_ER_INT_AD_IRQ 0x80000000
#define START_ER_INT_AD_IRQ_BIT 31
#define START_ER_INT_TS_P_MASK 0x40000000
#define START_ER_INT_TS_P 0x40000000
#define START_ER_INT_TS_P_BIT 30
#define START_ER_INT_TS_AUX_MASK 0x20000000
#define START_ER_INT_TS_AUX 0x20000000
#define START_ER_INT_TS_AUX_BIT 29
#define START_ER_INT_USB_AHB_NEED_CLK_MASK 0x4000000
#define START_ER_INT_USB_AHB_NEED_CLK 0x4000000
#define START_ER_INT_USB_AHB_NEED_CLK_BIT 26
#define START_ER_INT_MSTIMER_INT_MASK 0x2000000
#define START_ER_INT_MSTIMER_INT 0x2000000
#define START_ER_INT_MSTIMER_INT_BIT 25
#define START_ER_INT_RTC_INT_MASK 0x1000000
#define START_ER_INT_RTC_INT 0x1000000
#define START_ER_INT_RTC_INT_BIT 24
#define START_ER_INT_USB_NEED_CLK_MASK 0x800000
#define START_ER_INT_USB_NEED_CLK 0x800000
#define START_ER_INT_USB_NEED_CLK_BIT 23
#define START_ER_INT_USB_INT_MASK 0x400000
#define START_ER_INT_USB_INT 0x400000
#define START_ER_INT_USB_INT_BIT 22
#define START_ER_INT_USB_I2C_INT_MASK 0x200000
#define START_ER_INT_USB_I2C_INT 0x200000
#define START_ER_INT_USB_I2C_INT_BIT 21
#define START_ER_INT_USB_OTG_TIMER_INT_MASK 0x100000
#define START_ER_INT_USB_OTG_TIMER_INT 0x100000
#define START_ER_INT_USB_OTG_TIMER_INT_BIT 20
#define START_ER_INT_USB_OTG_ATX_INT_N_MASK 0x80000
#define START_ER_INT_USB_OTG_ATX_INT_N 0x80000
#define START_ER_INT_USB_OTG_ATX_INT_N_BIT 19
#define START_ER_INT_KEY_IRQ_MASK 0x10000
#define START_ER_INT_KEY_IRQ 0x10000
#define START_ER_INT_KEY_IRQ_BIT 16
#define START_ER_INT_Ethernet_MAC_MASK 0x80
#define START_ER_INT_Ethernet_MAC 0x80
#define START_ER_INT_Ethernet_MAC_BIT 7
#define START_ER_INT_PORT0_PORT1_MASK 0x40
#define START_ER_INT_PORT0_PORT1 0x40
#define START_ER_INT_PORT0_PORT1_BIT 6
#define START_ER_INT_GPIO_05_MASK 0x20
#define START_ER_INT_GPIO_05 0x20
#define START_ER_INT_GPIO_05_BIT 5
#define START_ER_INT_GPIO_04_MASK 0x10
#define START_ER_INT_GPIO_04 0x10
#define START_ER_INT_GPIO_04_BIT 4
#define START_ER_INT_GPIO_03_MASK 0x8
#define START_ER_INT_GPIO_03 0x8
#define START_ER_INT_GPIO_03_BIT 3
#define START_ER_INT_GPIO_02_MASK 0x4
#define START_ER_INT_GPIO_02 0x4
#define START_ER_INT_GPIO_02_BIT 2
#define START_ER_INT_GPIO_01_MASK 0x2
#define START_ER_INT_GPIO_01 0x2
#define START_ER_INT_GPIO_01_BIT 1
#define START_ER_INT_GPIO_00_MASK 0x1
#define START_ER_INT_GPIO_00 0x1
#define START_ER_INT_GPIO_00_BIT 0

#define START_ER_PIN (*(volatile unsigned *)0x40004030)
#define START_ER_PIN_U7_RX_MASK 0x80000000
#define START_ER_PIN_U7_RX 0x80000000
#define START_ER_PIN_U7_RX_BIT 31
#define START_ER_PIN_U7_HCTS_MASK 0x40000000
#define START_ER_PIN_U7_HCTS 0x40000000
#define START_ER_PIN_U7_HCTS_BIT 30
#define START_ER_PIN_U6_IRRX_MASK 0x10000000
#define START_ER_PIN_U6_IRRX 0x10000000
#define START_ER_PIN_U6_IRRX_BIT 28
#define START_ER_PIN_U5_RX_MASK 0x4000000
#define START_ER_PIN_U5_RX 0x4000000
#define START_ER_PIN_U5_RX_BIT 26
#define START_ER_PIN_GPI_28_MASK 0x2000000
#define START_ER_PIN_GPI_28 0x2000000
#define START_ER_PIN_GPI_28_BIT 25
#define START_ER_PIN_U3_RX_MASK 0x1000000
#define START_ER_PIN_U3_RX 0x1000000
#define START_ER_PIN_U3_RX_BIT 24
#define START_ER_PIN_U2_HCTS_MASK 0x800000
#define START_ER_PIN_U2_HCTS 0x800000
#define START_ER_PIN_U2_HCTS_BIT 23
#define START_ER_PIN_U2_RX_MASK 0x400000
#define START_ER_PIN_U2_RX 0x400000
#define START_ER_PIN_U2_RX_BIT 22
#define START_ER_PIN_U1_RX_MASK 0x200000
#define START_ER_PIN_U1_RX 0x200000
#define START_ER_PIN_U1_RX_BIT 21
#define START_ER_PIN_SDIO_INT_N_MASK 0x40000
#define START_ER_PIN_SDIO_INT_N 0x40000
#define START_ER_PIN_SDIO_INT_N_BIT 18
#define START_ER_PIN_GPIO_06_MASK 0x10000
#define START_ER_PIN_GPIO_06 0x10000
#define START_ER_PIN_GPIO_06_BIT 16
#define START_ER_PIN_GPIO_05_MASK 0x8000
#define START_ER_PIN_GPIO_05 0x8000
#define START_ER_PIN_GPIO_05_BIT 15
#define START_ER_PIN_GPIO_04_MASK 0x4000
#define START_ER_PIN_GPIO_04 0x4000
#define START_ER_PIN_GPIO_04_BIT 14
#define START_ER_PIN_GPIO_03_MASK 0x2000
#define START_ER_PIN_GPIO_03 0x2000
#define START_ER_PIN_GPIO_03_BIT 13
#define START_ER_PIN_GPIO_02_MASK 0x1000
#define START_ER_PIN_GPIO_02 0x1000
#define START_ER_PIN_GPIO_02_BIT 12
#define START_ER_PIN_GPIO_01_MASK 0x800
#define START_ER_PIN_GPIO_01 0x800
#define START_ER_PIN_GPIO_01_BIT 11
#define START_ER_PIN_GPIO_00_MASK 0x400
#define START_ER_PIN_GPIO_00 0x400
#define START_ER_PIN_GPIO_00_BIT 10
#define START_ER_PIN_SYSCLKEN_MASK 0x200
#define START_ER_PIN_SYSCLKEN 0x200
#define START_ER_PIN_SYSCLKEN_BIT 9
#define START_ER_PIN_SPI1_DATIN_MASK 0x100
#define START_ER_PIN_SPI1_DATIN 0x100
#define START_ER_PIN_SPI1_DATIN_BIT 8
#define START_ER_PIN_GPIO_07_MASK 0x80
#define START_ER_PIN_GPIO_07 0x80
#define START_ER_PIN_GPIO_07_BIT 7
#define START_ER_PIN_SPI2_DATIN_MASK 0x40
#define START_ER_PIN_SPI2_DATIN 0x40
#define START_ER_PIN_SPI2_DATIN_BIT 6
#define START_ER_PIN_GPIO_19_MASK 0x20
#define START_ER_PIN_GPIO_19 0x20
#define START_ER_PIN_GPIO_19_BIT 5
#define START_ER_PIN_GPIO_09_MASK 0x10
#define START_ER_PIN_GPIO_09 0x10
#define START_ER_PIN_GPIO_09_BIT 4
#define START_ER_PIN_GPIO_08_MASK 0x8
#define START_ER_PIN_GPIO_08 0x8
#define START_ER_PIN_GPIO_08_BIT 3

#define START_RSR_INT (*(volatile unsigned *)0x40004024)
#define START_RSR_INT_AD_IRQ_MASK 0x80000000
#define START_RSR_INT_AD_IRQ 0x80000000
#define START_RSR_INT_AD_IRQ_BIT 31
#define START_RSR_INT_TS_P_MASK 0x40000000
#define START_RSR_INT_TS_P 0x40000000
#define START_RSR_INT_TS_P_BIT 30
#define START_RSR_INT_TS_AUX_MASK 0x20000000
#define START_RSR_INT_TS_AUX 0x20000000
#define START_RSR_INT_TS_AUX_BIT 29
#define START_RSR_INT_USB_AHB_NEED_CLK_MASK 0x4000000
#define START_RSR_INT_USB_AHB_NEED_CLK 0x4000000
#define START_RSR_INT_USB_AHB_NEED_CLK_BIT 26
#define START_RSR_INT_MSTIMER_INT_MASK 0x2000000
#define START_RSR_INT_MSTIMER_INT 0x2000000
#define START_RSR_INT_MSTIMER_INT_BIT 25
#define START_RSR_INT_RTC_INT_MASK 0x1000000
#define START_RSR_INT_RTC_INT 0x1000000
#define START_RSR_INT_RTC_INT_BIT 24
#define START_RSR_INT_USB_NEED_CLK_MASK 0x800000
#define START_RSR_INT_USB_NEED_CLK 0x800000
#define START_RSR_INT_USB_NEED_CLK_BIT 23
#define START_RSR_INT_USB_INT_MASK 0x400000
#define START_RSR_INT_USB_INT 0x400000
#define START_RSR_INT_USB_INT_BIT 22
#define START_RSR_INT_USB_I2C_INT_MASK 0x200000
#define START_RSR_INT_USB_I2C_INT 0x200000
#define START_RSR_INT_USB_I2C_INT_BIT 21
#define START_RSR_INT_USB_OTG_TIMER_INT_MASK 0x100000
#define START_RSR_INT_USB_OTG_TIMER_INT 0x100000
#define START_RSR_INT_USB_OTG_TIMER_INT_BIT 20
#define START_RSR_INT_USB_OTG_ATX_INT_N_MASK 0x80000
#define START_RSR_INT_USB_OTG_ATX_INT_N 0x80000
#define START_RSR_INT_USB_OTG_ATX_INT_N_BIT 19
#define START_RSR_INT_KEY_IRQ_MASK 0x10000
#define START_RSR_INT_KEY_IRQ 0x10000
#define START_RSR_INT_KEY_IRQ_BIT 16
#define START_RSR_INT_Ethernet_MAC_MASK 0x80
#define START_RSR_INT_Ethernet_MAC 0x80
#define START_RSR_INT_Ethernet_MAC_BIT 7
#define START_RSR_INT_PORT0_PORT1_MASK 0x40
#define START_RSR_INT_PORT0_PORT1 0x40
#define START_RSR_INT_PORT0_PORT1_BIT 6
#define START_RSR_INT_GPIO_05_MASK 0x20
#define START_RSR_INT_GPIO_05 0x20
#define START_RSR_INT_GPIO_05_BIT 5
#define START_RSR_INT_GPIO_04_MASK 0x10
#define START_RSR_INT_GPIO_04 0x10
#define START_RSR_INT_GPIO_04_BIT 4
#define START_RSR_INT_GPIO_03_MASK 0x8
#define START_RSR_INT_GPIO_03 0x8
#define START_RSR_INT_GPIO_03_BIT 3
#define START_RSR_INT_GPIO_02_MASK 0x4
#define START_RSR_INT_GPIO_02 0x4
#define START_RSR_INT_GPIO_02_BIT 2
#define START_RSR_INT_GPIO_01_MASK 0x2
#define START_RSR_INT_GPIO_01 0x2
#define START_RSR_INT_GPIO_01_BIT 1
#define START_RSR_INT_GPIO_00_MASK 0x1
#define START_RSR_INT_GPIO_00 0x1
#define START_RSR_INT_GPIO_00_BIT 0

#define START_RSR_PIN (*(volatile unsigned *)0x40004034)
#define START_RSR_PIN_U7_RX_MASK 0x80000000
#define START_RSR_PIN_U7_RX 0x80000000
#define START_RSR_PIN_U7_RX_BIT 31
#define START_RSR_PIN_U7_HCTS_MASK 0x40000000
#define START_RSR_PIN_U7_HCTS 0x40000000
#define START_RSR_PIN_U7_HCTS_BIT 30
#define START_RSR_PIN_U6_IRRX_MASK 0x10000000
#define START_RSR_PIN_U6_IRRX 0x10000000
#define START_RSR_PIN_U6_IRRX_BIT 28
#define START_RSR_PIN_U5_RX_MASK 0x4000000
#define START_RSR_PIN_U5_RX 0x4000000
#define START_RSR_PIN_U5_RX_BIT 26
#define START_RSR_PIN_GPI_28_MASK 0x2000000
#define START_RSR_PIN_GPI_28 0x2000000
#define START_RSR_PIN_GPI_28_BIT 25
#define START_RSR_PIN_U3_RX_MASK 0x1000000
#define START_RSR_PIN_U3_RX 0x1000000
#define START_RSR_PIN_U3_RX_BIT 24
#define START_RSR_PIN_U2_HCTS_MASK 0x800000
#define START_RSR_PIN_U2_HCTS 0x800000
#define START_RSR_PIN_U2_HCTS_BIT 23
#define START_RSR_PIN_U2_RX_MASK 0x400000
#define START_RSR_PIN_U2_RX 0x400000
#define START_RSR_PIN_U2_RX_BIT 22
#define START_RSR_PIN_U1_RX_MASK 0x200000
#define START_RSR_PIN_U1_RX 0x200000
#define START_RSR_PIN_U1_RX_BIT 21
#define START_RSR_PIN_SDIO_INT_N_MASK 0x40000
#define START_RSR_PIN_SDIO_INT_N 0x40000
#define START_RSR_PIN_SDIO_INT_N_BIT 18
#define START_RSR_PIN_GPIO_06_MASK 0x10000
#define START_RSR_PIN_GPIO_06 0x10000
#define START_RSR_PIN_GPIO_06_BIT 16
#define START_RSR_PIN_GPIO_05_MASK 0x8000
#define START_RSR_PIN_GPIO_05 0x8000
#define START_RSR_PIN_GPIO_05_BIT 15
#define START_RSR_PIN_GPIO_04_MASK 0x4000
#define START_RSR_PIN_GPIO_04 0x4000
#define START_RSR_PIN_GPIO_04_BIT 14
#define START_RSR_PIN_GPIO_03_MASK 0x2000
#define START_RSR_PIN_GPIO_03 0x2000
#define START_RSR_PIN_GPIO_03_BIT 13
#define START_RSR_PIN_GPIO_02_MASK 0x1000
#define START_RSR_PIN_GPIO_02 0x1000
#define START_RSR_PIN_GPIO_02_BIT 12
#define START_RSR_PIN_GPIO_01_MASK 0x800
#define START_RSR_PIN_GPIO_01 0x800
#define START_RSR_PIN_GPIO_01_BIT 11
#define START_RSR_PIN_GPIO_00_MASK 0x400
#define START_RSR_PIN_GPIO_00 0x400
#define START_RSR_PIN_GPIO_00_BIT 10
#define START_RSR_PIN_SYSCLKEN_MASK 0x200
#define START_RSR_PIN_SYSCLKEN 0x200
#define START_RSR_PIN_SYSCLKEN_BIT 9
#define START_RSR_PIN_SPI1_DATIN_MASK 0x100
#define START_RSR_PIN_SPI1_DATIN 0x100
#define START_RSR_PIN_SPI1_DATIN_BIT 8
#define START_RSR_PIN_GPIO_07_MASK 0x80
#define START_RSR_PIN_GPIO_07 0x80
#define START_RSR_PIN_GPIO_07_BIT 7
#define START_RSR_PIN_SPI2_DATIN_MASK 0x40
#define START_RSR_PIN_SPI2_DATIN 0x40
#define START_RSR_PIN_SPI2_DATIN_BIT 6
#define START_RSR_PIN_GPIO_19_MASK 0x20
#define START_RSR_PIN_GPIO_19 0x20
#define START_RSR_PIN_GPIO_19_BIT 5
#define START_RSR_PIN_GPIO_09_MASK 0x10
#define START_RSR_PIN_GPIO_09 0x10
#define START_RSR_PIN_GPIO_09_BIT 4
#define START_RSR_PIN_GPIO_08_MASK 0x8
#define START_RSR_PIN_GPIO_08 0x8
#define START_RSR_PIN_GPIO_08_BIT 3

#define START_SR_INT (*(volatile unsigned *)0x40004028)
#define START_SR_INT_AD_IRQ_MASK 0x80000000
#define START_SR_INT_AD_IRQ 0x80000000
#define START_SR_INT_AD_IRQ_BIT 31
#define START_SR_INT_TS_P_MASK 0x40000000
#define START_SR_INT_TS_P 0x40000000
#define START_SR_INT_TS_P_BIT 30
#define START_SR_INT_TS_AUX_MASK 0x20000000
#define START_SR_INT_TS_AUX 0x20000000
#define START_SR_INT_TS_AUX_BIT 29
#define START_SR_INT_USB_AHB_NEED_CLK_MASK 0x4000000
#define START_SR_INT_USB_AHB_NEED_CLK 0x4000000
#define START_SR_INT_USB_AHB_NEED_CLK_BIT 26
#define START_SR_INT_MSTIMER_INT_MASK 0x2000000
#define START_SR_INT_MSTIMER_INT 0x2000000
#define START_SR_INT_MSTIMER_INT_BIT 25
#define START_SR_INT_RTC_INT_MASK 0x1000000
#define START_SR_INT_RTC_INT 0x1000000
#define START_SR_INT_RTC_INT_BIT 24
#define START_SR_INT_USB_NEED_CLK_MASK 0x800000
#define START_SR_INT_USB_NEED_CLK 0x800000
#define START_SR_INT_USB_NEED_CLK_BIT 23
#define START_SR_INT_USB_INT_MASK 0x400000
#define START_SR_INT_USB_INT 0x400000
#define START_SR_INT_USB_INT_BIT 22
#define START_SR_INT_USB_I2C_INT_MASK 0x200000
#define START_SR_INT_USB_I2C_INT 0x200000
#define START_SR_INT_USB_I2C_INT_BIT 21
#define START_SR_INT_USB_OTG_TIMER_INT_MASK 0x100000
#define START_SR_INT_USB_OTG_TIMER_INT 0x100000
#define START_SR_INT_USB_OTG_TIMER_INT_BIT 20
#define START_SR_INT_USB_OTG_ATX_INT_N_MASK 0x80000
#define START_SR_INT_USB_OTG_ATX_INT_N 0x80000
#define START_SR_INT_USB_OTG_ATX_INT_N_BIT 19
#define START_SR_INT_KEY_IRQ_MASK 0x10000
#define START_SR_INT_KEY_IRQ 0x10000
#define START_SR_INT_KEY_IRQ_BIT 16
#define START_SR_INT_Ethernet_MAC_MASK 0x80
#define START_SR_INT_Ethernet_MAC 0x80
#define START_SR_INT_Ethernet_MAC_BIT 7
#define START_SR_INT_PORT0_PORT1_MASK 0x40
#define START_SR_INT_PORT0_PORT1 0x40
#define START_SR_INT_PORT0_PORT1_BIT 6
#define START_SR_INT_GPIO_05_MASK 0x20
#define START_SR_INT_GPIO_05 0x20
#define START_SR_INT_GPIO_05_BIT 5
#define START_SR_INT_GPIO_04_MASK 0x10
#define START_SR_INT_GPIO_04 0x10
#define START_SR_INT_GPIO_04_BIT 4
#define START_SR_INT_GPIO_03_MASK 0x8
#define START_SR_INT_GPIO_03 0x8
#define START_SR_INT_GPIO_03_BIT 3
#define START_SR_INT_GPIO_02_MASK 0x4
#define START_SR_INT_GPIO_02 0x4
#define START_SR_INT_GPIO_02_BIT 2
#define START_SR_INT_GPIO_01_MASK 0x2
#define START_SR_INT_GPIO_01 0x2
#define START_SR_INT_GPIO_01_BIT 1
#define START_SR_INT_GPIO_00_MASK 0x1
#define START_SR_INT_GPIO_00 0x1
#define START_SR_INT_GPIO_00_BIT 0

#define START_SR_PIN (*(volatile unsigned *)0x40004038)
#define START_SR_PIN_U7_RX_MASK 0x80000000
#define START_SR_PIN_U7_RX 0x80000000
#define START_SR_PIN_U7_RX_BIT 31
#define START_SR_PIN_U7_HCTS_MASK 0x40000000
#define START_SR_PIN_U7_HCTS 0x40000000
#define START_SR_PIN_U7_HCTS_BIT 30
#define START_SR_PIN_U6_IRRX_MASK 0x10000000
#define START_SR_PIN_U6_IRRX 0x10000000
#define START_SR_PIN_U6_IRRX_BIT 28
#define START_SR_PIN_U5_RX_MASK 0x4000000
#define START_SR_PIN_U5_RX 0x4000000
#define START_SR_PIN_U5_RX_BIT 26
#define START_SR_PIN_GPI_28_MASK 0x2000000
#define START_SR_PIN_GPI_28 0x2000000
#define START_SR_PIN_GPI_28_BIT 25
#define START_SR_PIN_U3_RX_MASK 0x1000000
#define START_SR_PIN_U3_RX 0x1000000
#define START_SR_PIN_U3_RX_BIT 24
#define START_SR_PIN_U2_HCTS_MASK 0x800000
#define START_SR_PIN_U2_HCTS 0x800000
#define START_SR_PIN_U2_HCTS_BIT 23
#define START_SR_PIN_U2_RX_MASK 0x400000
#define START_SR_PIN_U2_RX 0x400000
#define START_SR_PIN_U2_RX_BIT 22
#define START_SR_PIN_U1_RX_MASK 0x200000
#define START_SR_PIN_U1_RX 0x200000
#define START_SR_PIN_U1_RX_BIT 21
#define START_SR_PIN_SDIO_INT_N_MASK 0x40000
#define START_SR_PIN_SDIO_INT_N 0x40000
#define START_SR_PIN_SDIO_INT_N_BIT 18
#define START_SR_PIN_GPIO_06_MASK 0x10000
#define START_SR_PIN_GPIO_06 0x10000
#define START_SR_PIN_GPIO_06_BIT 16
#define START_SR_PIN_GPIO_05_MASK 0x8000
#define START_SR_PIN_GPIO_05 0x8000
#define START_SR_PIN_GPIO_05_BIT 15
#define START_SR_PIN_GPIO_04_MASK 0x4000
#define START_SR_PIN_GPIO_04 0x4000
#define START_SR_PIN_GPIO_04_BIT 14
#define START_SR_PIN_GPIO_03_MASK 0x2000
#define START_SR_PIN_GPIO_03 0x2000
#define START_SR_PIN_GPIO_03_BIT 13
#define START_SR_PIN_GPIO_02_MASK 0x1000
#define START_SR_PIN_GPIO_02 0x1000
#define START_SR_PIN_GPIO_02_BIT 12
#define START_SR_PIN_GPIO_01_MASK 0x800
#define START_SR_PIN_GPIO_01 0x800
#define START_SR_PIN_GPIO_01_BIT 11
#define START_SR_PIN_GPIO_00_MASK 0x400
#define START_SR_PIN_GPIO_00 0x400
#define START_SR_PIN_GPIO_00_BIT 10
#define START_SR_PIN_SYSCLKEN_MASK 0x200
#define START_SR_PIN_SYSCLKEN 0x200
#define START_SR_PIN_SYSCLKEN_BIT 9
#define START_SR_PIN_SPI1_DATIN_MASK 0x100
#define START_SR_PIN_SPI1_DATIN 0x100
#define START_SR_PIN_SPI1_DATIN_BIT 8
#define START_SR_PIN_GPIO_07_MASK 0x80
#define START_SR_PIN_GPIO_07 0x80
#define START_SR_PIN_GPIO_07_BIT 7
#define START_SR_PIN_SPI2_DATIN_MASK 0x40
#define START_SR_PIN_SPI2_DATIN 0x40
#define START_SR_PIN_SPI2_DATIN_BIT 6
#define START_SR_PIN_GPIO_19_MASK 0x20
#define START_SR_PIN_GPIO_19 0x20
#define START_SR_PIN_GPIO_19_BIT 5
#define START_SR_PIN_GPIO_09_MASK 0x10
#define START_SR_PIN_GPIO_09 0x10
#define START_SR_PIN_GPIO_09_BIT 4
#define START_SR_PIN_GPIO_08_MASK 0x8
#define START_SR_PIN_GPIO_08 0x8
#define START_SR_PIN_GPIO_08_BIT 3

#define START_APR_INT (*(volatile unsigned *)0x4000402C)
#define START_APR_INT_AD_IRQ_MASK 0x80000000
#define START_APR_INT_AD_IRQ 0x80000000
#define START_APR_INT_AD_IRQ_BIT 31
#define START_APR_INT_TS_P_MASK 0x40000000
#define START_APR_INT_TS_P 0x40000000
#define START_APR_INT_TS_P_BIT 30
#define START_APR_INT_TS_AUX_MASK 0x20000000
#define START_APR_INT_TS_AUX 0x20000000
#define START_APR_INT_TS_AUX_BIT 29
#define START_APR_INT_USB_AHB_NEED_CLK_MASK 0x4000000
#define START_APR_INT_USB_AHB_NEED_CLK 0x4000000
#define START_APR_INT_USB_AHB_NEED_CLK_BIT 26
#define START_APR_INT_MSTIMER_INT_MASK 0x2000000
#define START_APR_INT_MSTIMER_INT 0x2000000
#define START_APR_INT_MSTIMER_INT_BIT 25
#define START_APR_INT_RTC_INT_MASK 0x1000000
#define START_APR_INT_RTC_INT 0x1000000
#define START_APR_INT_RTC_INT_BIT 24
#define START_APR_INT_USB_NEED_CLK_MASK 0x800000
#define START_APR_INT_USB_NEED_CLK 0x800000
#define START_APR_INT_USB_NEED_CLK_BIT 23
#define START_APR_INT_USB_INT_MASK 0x400000
#define START_APR_INT_USB_INT 0x400000
#define START_APR_INT_USB_INT_BIT 22
#define START_APR_INT_USB_I2C_INT_MASK 0x200000
#define START_APR_INT_USB_I2C_INT 0x200000
#define START_APR_INT_USB_I2C_INT_BIT 21
#define START_APR_INT_USB_OTG_TIMER_INT_MASK 0x100000
#define START_APR_INT_USB_OTG_TIMER_INT 0x100000
#define START_APR_INT_USB_OTG_TIMER_INT_BIT 20
#define START_APR_INT_USB_OTG_ATX_INT_N_MASK 0x80000
#define START_APR_INT_USB_OTG_ATX_INT_N 0x80000
#define START_APR_INT_USB_OTG_ATX_INT_N_BIT 19
#define START_APR_INT_KEY_IRQ_MASK 0x10000
#define START_APR_INT_KEY_IRQ 0x10000
#define START_APR_INT_KEY_IRQ_BIT 16
#define START_APR_INT_Ethernet_MAC_MASK 0x80
#define START_APR_INT_Ethernet_MAC 0x80
#define START_APR_INT_Ethernet_MAC_BIT 7
#define START_APR_INT_PORT0_PORT1_MASK 0x40
#define START_APR_INT_PORT0_PORT1 0x40
#define START_APR_INT_PORT0_PORT1_BIT 6
#define START_APR_INT_GPIO_05_MASK 0x20
#define START_APR_INT_GPIO_05 0x20
#define START_APR_INT_GPIO_05_BIT 5
#define START_APR_INT_GPIO_04_MASK 0x10
#define START_APR_INT_GPIO_04 0x10
#define START_APR_INT_GPIO_04_BIT 4
#define START_APR_INT_GPIO_03_MASK 0x8
#define START_APR_INT_GPIO_03 0x8
#define START_APR_INT_GPIO_03_BIT 3
#define START_APR_INT_GPIO_02_MASK 0x4
#define START_APR_INT_GPIO_02 0x4
#define START_APR_INT_GPIO_02_BIT 2
#define START_APR_INT_GPIO_01_MASK 0x2
#define START_APR_INT_GPIO_01 0x2
#define START_APR_INT_GPIO_01_BIT 1
#define START_APR_INT_GPIO_00_MASK 0x1
#define START_APR_INT_GPIO_00 0x1
#define START_APR_INT_GPIO_00_BIT 0

#define START_APR_PIN (*(volatile unsigned *)0x4000403C)
#define START_APR_PIN_U7_RX_MASK 0x80000000
#define START_APR_PIN_U7_RX 0x80000000
#define START_APR_PIN_U7_RX_BIT 31
#define START_APR_PIN_U7_HCTS_MASK 0x40000000
#define START_APR_PIN_U7_HCTS 0x40000000
#define START_APR_PIN_U7_HCTS_BIT 30
#define START_APR_PIN_U6_IRRX_MASK 0x10000000
#define START_APR_PIN_U6_IRRX 0x10000000
#define START_APR_PIN_U6_IRRX_BIT 28
#define START_APR_PIN_U5_RX_MASK 0x4000000
#define START_APR_PIN_U5_RX 0x4000000
#define START_APR_PIN_U5_RX_BIT 26
#define START_APR_PIN_GPI_28_MASK 0x2000000
#define START_APR_PIN_GPI_28 0x2000000
#define START_APR_PIN_GPI_28_BIT 25
#define START_APR_PIN_U3_RX_MASK 0x1000000
#define START_APR_PIN_U3_RX 0x1000000
#define START_APR_PIN_U3_RX_BIT 24
#define START_APR_PIN_U2_HCTS_MASK 0x800000
#define START_APR_PIN_U2_HCTS 0x800000
#define START_APR_PIN_U2_HCTS_BIT 23
#define START_APR_PIN_U2_RX_MASK 0x400000
#define START_APR_PIN_U2_RX 0x400000
#define START_APR_PIN_U2_RX_BIT 22
#define START_APR_PIN_U1_RX_MASK 0x200000
#define START_APR_PIN_U1_RX 0x200000
#define START_APR_PIN_U1_RX_BIT 21
#define START_APR_PIN_SDIO_INT_N_MASK 0x40000
#define START_APR_PIN_SDIO_INT_N 0x40000
#define START_APR_PIN_SDIO_INT_N_BIT 18
#define START_APR_PIN_GPIO_06_MASK 0x10000
#define START_APR_PIN_GPIO_06 0x10000
#define START_APR_PIN_GPIO_06_BIT 16
#define START_APR_PIN_GPIO_05_MASK 0x8000
#define START_APR_PIN_GPIO_05 0x8000
#define START_APR_PIN_GPIO_05_BIT 15
#define START_APR_PIN_GPIO_04_MASK 0x4000
#define START_APR_PIN_GPIO_04 0x4000
#define START_APR_PIN_GPIO_04_BIT 14
#define START_APR_PIN_GPIO_03_MASK 0x2000
#define START_APR_PIN_GPIO_03 0x2000
#define START_APR_PIN_GPIO_03_BIT 13
#define START_APR_PIN_GPIO_02_MASK 0x1000
#define START_APR_PIN_GPIO_02 0x1000
#define START_APR_PIN_GPIO_02_BIT 12
#define START_APR_PIN_GPIO_01_MASK 0x800
#define START_APR_PIN_GPIO_01 0x800
#define START_APR_PIN_GPIO_01_BIT 11
#define START_APR_PIN_GPIO_00_MASK 0x400
#define START_APR_PIN_GPIO_00 0x400
#define START_APR_PIN_GPIO_00_BIT 10
#define START_APR_PIN_SYSCLKEN_MASK 0x200
#define START_APR_PIN_SYSCLKEN 0x200
#define START_APR_PIN_SYSCLKEN_BIT 9
#define START_APR_PIN_SPI1_DATIN_MASK 0x100
#define START_APR_PIN_SPI1_DATIN 0x100
#define START_APR_PIN_SPI1_DATIN_BIT 8
#define START_APR_PIN_GPIO_07_MASK 0x80
#define START_APR_PIN_GPIO_07 0x80
#define START_APR_PIN_GPIO_07_BIT 7
#define START_APR_PIN_SPI2_DATIN_MASK 0x40
#define START_APR_PIN_SPI2_DATIN 0x40
#define START_APR_PIN_SPI2_DATIN_BIT 6
#define START_APR_PIN_GPIO_19_MASK 0x20
#define START_APR_PIN_GPIO_19 0x20
#define START_APR_PIN_GPIO_19_BIT 5
#define START_APR_PIN_GPIO_09_MASK 0x10
#define START_APR_PIN_GPIO_09 0x10
#define START_APR_PIN_GPIO_09_BIT 4
#define START_APR_PIN_GPIO_08_MASK 0x8
#define START_APR_PIN_GPIO_08 0x8
#define START_APR_PIN_GPIO_08_BIT 3

#define USB_CTRL (*(volatile unsigned *)0x40004064)
#define USB_CTRL_USB_Slave_HCKL_control_MASK 0x1000000
#define USB_CTRL_USB_Slave_HCKL_control 0x1000000
#define USB_CTRL_USB_Slave_HCKL_control_BIT 24
#define USB_CTRL_usb_i2c_enable_MASK 0x800000
#define USB_CTRL_usb_i2c_enable 0x800000
#define USB_CTRL_usb_i2c_enable_BIT 23
#define USB_CTRL_usb_dev_need_clk_en_MASK 0x400000
#define USB_CTRL_usb_dev_need_clk_en 0x400000
#define USB_CTRL_usb_dev_need_clk_en_BIT 22
#define USB_CTRL_usb_host_need_clk_en_MASK 0x200000
#define USB_CTRL_usb_host_need_clk_en 0x200000
#define USB_CTRL_usb_host_need_clk_en_BIT 21
#define USB_CTRL_Pad_control_for_USB_DAT_VP_and_USB_SE0_VM_pads_MASK 0x180000
#define USB_CTRL_Pad_control_for_USB_DAT_VP_and_USB_SE0_VM_pads_BIT 19
#define USB_CTRL_USB_Clken2_clock_control_MASK 0x40000
#define USB_CTRL_USB_Clken2_clock_control 0x40000
#define USB_CTRL_USB_Clken2_clock_control_BIT 18
#define USB_CTRL_USB_Clken1_clock_control_MASK 0x20000
#define USB_CTRL_USB_Clken1_clock_control 0x20000
#define USB_CTRL_USB_Clken1_clock_control_BIT 17
#define USB_CTRL_PLL_Power_down_MASK 0x10000
#define USB_CTRL_PLL_Power_down 0x10000
#define USB_CTRL_PLL_Power_down_BIT 16
#define USB_CTRL_Bypass_control_MASK 0x8000
#define USB_CTRL_Bypass_control 0x8000
#define USB_CTRL_Bypass_control_BIT 15
#define USB_CTRL_Direct_output_control_MASK 0x4000
#define USB_CTRL_Direct_output_control 0x4000
#define USB_CTRL_Direct_output_control_BIT 14
#define USB_CTRL_Feedback_divider_path_control_MASK 0x2000
#define USB_CTRL_Feedback_divider_path_control 0x2000
#define USB_CTRL_Feedback_divider_path_control_BIT 13
#define USB_CTRL_P_MASK 0x1800
#define USB_CTRL_P_BIT 11
#define USB_CTRL_N_MASK 0x600
#define USB_CTRL_N_BIT 9
#define USB_CTRL_M_MASK 0x1FE
#define USB_CTRL_M_BIT 1
#define USB_CTRL_PLL_LOCK_status_MASK 0x1
#define USB_CTRL_PLL_LOCK_status 0x1
#define USB_CTRL_PLL_LOCK_status_BIT 0

#define SDRAMCLK_CTRL (*(volatile unsigned *)0x40004068)
#define SDRAMCLK_CTRL_SDRAM_PIN_SPEED3_MASK 0x400000
#define SDRAMCLK_CTRL_SDRAM_PIN_SPEED3 0x400000
#define SDRAMCLK_CTRL_SDRAM_PIN_SPEED3_BIT 22
#define SDRAMCLK_CTRL_SDRAM_PIN_SPEED2_MASK 0x200000
#define SDRAMCLK_CTRL_SDRAM_PIN_SPEED2 0x200000
#define SDRAMCLK_CTRL_SDRAM_PIN_SPEED2_BIT 21
#define SDRAMCLK_CTRL_SDRAM_PIN_SPEED1_MASK 0x300000
#define SDRAMCLK_CTRL_SDRAM_PIN_SPEED1_BIT 20
#define SDRAMCLK_CTRL_SW_DDR_RESET_MASK 0x180000
#define SDRAMCLK_CTRL_SW_DDR_RESET_BIT 19
#define SDRAMCLK_CTRL_HCLKDELAY_DELAY_MASK 0x7C000
#define SDRAMCLK_CTRL_HCLKDELAY_DELAY_BIT 14
#define SDRAMCLK_CTRL_Delay_circuitry_Adder_status_MASK 0x2000
#define SDRAMCLK_CTRL_Delay_circuitry_Adder_status 0x2000
#define SDRAMCLK_CTRL_Delay_circuitry_Adder_status_BIT 13
#define SDRAMCLK_CTRL_Sensitivity_Factor_for_DDR_SDRAM_calibration_MASK 0x1C00
#define SDRAMCLK_CTRL_Sensitivity_Factor_for_DDR_SDRAM_calibration_BIT 10
#define SDRAMCLK_CTRL_CAL_DELAY_MASK 0x200
#define SDRAMCLK_CTRL_CAL_DELAY 0x200
#define SDRAMCLK_CTRL_CAL_DELAY_BIT 9
#define SDRAMCLK_CTRL_SW_DDR_CAL_MASK 0x100
#define SDRAMCLK_CTRL_SW_DDR_CAL 0x100
#define SDRAMCLK_CTRL_SW_DDR_CAL_BIT 8
#define SDRAMCLK_CTRL_RTC_TICK_EN_MASK 0x80
#define SDRAMCLK_CTRL_RTC_TICK_EN 0x80
#define SDRAMCLK_CTRL_RTC_TICK_EN_BIT 7
#define SDRAMCLK_CTRL_DDR_DQSIN_DELAY_MASK 0x7C
#define SDRAMCLK_CTRL_DDR_DQSIN_DELAY_BIT 2
#define SDRAMCLK_CTRL_DDR_SEL_MASK 0x2
#define SDRAMCLK_CTRL_DDR_SEL 0x2
#define SDRAMCLK_CTRL_DDR_SEL_BIT 1
#define SDRAMCLK_CTRL_SDRAM_HCLK_and_Inverted_HCLK_enabled_MASK 0x1
#define SDRAMCLK_CTRL_SDRAM_HCLK_and_Inverted_HCLK_enabled 0x1
#define SDRAMCLK_CTRL_SDRAM_HCLK_and_Inverted_HCLK_enabled_BIT 0

#define DDR_LAP_NOM (*(volatile unsigned *)0x4000406C)

#define DDR_LAP_COUNT (*(volatile unsigned *)0x40004070)

#define DDR_CAL_DELAY (*(volatile unsigned *)0x40004074)
#define DDR_CAL_DELAY_Value_MASK 0x1F
#define DDR_CAL_DELAY_Value_BIT 0

#define MS_CTRL (*(volatile unsigned *)0x40004080)
#define MS_CTRL_Disable_SD_card_pins_MASK 0x400
#define MS_CTRL_Disable_SD_card_pins 0x400
#define MS_CTRL_Disable_SD_card_pins_BIT 10
#define MS_CTRL_MSSDIO_pull_up_enabled_MASK 0x200
#define MS_CTRL_MSSDIO_pull_up_enabled 0x200
#define MS_CTRL_MSSDIO_pull_up_enabled_BIT 9
#define MS_CTRL_MSSDIO2_and_MSSDIO3_pad_control_MASK 0x100
#define MS_CTRL_MSSDIO2_and_MSSDIO3_pad_control 0x100
#define MS_CTRL_MSSDIO2_and_MSSDIO3_pad_control_BIT 8
#define MS_CTRL_MSSDIO1_pad_control_MASK 0x80
#define MS_CTRL_MSSDIO1_pad_control 0x80
#define MS_CTRL_MSSDIO1_pad_control_BIT 7
#define MS_CTRL_MSSDIO0_pad_control_MASK 0x40
#define MS_CTRL_MSSDIO0_pad_control 0x40
#define MS_CTRL_MSSDIO0_pad_control_BIT 6
#define MS_CTRL_SD_Card_clock_control_MASK 0x20
#define MS_CTRL_SD_Card_clock_control 0x20
#define MS_CTRL_SD_Card_clock_control_BIT 5
#define MS_CTRL_MSSDCLK_MASK 0xF
#define MS_CTRL_MSSDCLK_BIT 0

#define RINGOSC_CTRL (*(volatile unsigned *)0x40004088)
#define RINGOSC_CTRL_Ring_Oscillator_Start_Measure_Control_Bit_MASK 0x400
#define RINGOSC_CTRL_Ring_Oscillator_Start_Measure_Control_Bit 0x400
#define RINGOSC_CTRL_Ring_Oscillator_Start_Measure_Control_Bit_BIT 10
#define RINGOSC_CTRL_Ring_Oscillator_Clock_Counter_Value_MASK 0x3FF
#define RINGOSC_CTRL_Ring_Oscillator_Clock_Counter_Value_BIT 0

#define SW_INT (*(volatile unsigned *)0x400040A8)
#define SW_INT_Parameter_MASK 0x2
#define SW_INT_Parameter 0x2
#define SW_INT_Parameter_BIT 1
#define SW_INT_SW_INT_source_active_MASK 0x1
#define SW_INT_SW_INT_source_active 0x1
#define SW_INT_SW_INT_source_active_BIT 0

#define I2CCLK_CTRL (*(volatile unsigned *)0x400040AC)
#define I2CCLK_CTRL_USB_I2C_SCL_and_USB_I2C_SDA_driver_strength_control_MASK 0x10
#define I2CCLK_CTRL_USB_I2C_SCL_and_USB_I2C_SDA_driver_strength_control 0x10
#define I2CCLK_CTRL_USB_I2C_SCL_and_USB_I2C_SDA_driver_strength_control_BIT 4
#define I2CCLK_CTRL_I2C2_SCL_and_I2C2_SDA_driver_strength_control_MASK 0x8
#define I2CCLK_CTRL_I2C2_SCL_and_I2C2_SDA_driver_strength_control 0x8
#define I2CCLK_CTRL_I2C2_SCL_and_I2C2_SDA_driver_strength_control_BIT 3
#define I2CCLK_CTRL_I2C1_SCL_and_I2C1_SDA_driver_strength_control_MASK 0x4
#define I2CCLK_CTRL_I2C1_SCL_and_I2C1_SDA_driver_strength_control 0x4
#define I2CCLK_CTRL_I2C1_SCL_and_I2C1_SDA_driver_strength_control_BIT 2
#define I2CCLK_CTRL_I2C2_HCLK_enabled_MASK 0x2
#define I2CCLK_CTRL_I2C2_HCLK_enabled 0x2
#define I2CCLK_CTRL_I2C2_HCLK_enabled_BIT 1
#define I2CCLK_CTRL_I2C1_HCLK_enabled_MASK 0x1
#define I2CCLK_CTRL_I2C1_HCLK_enabled 0x1
#define I2CCLK_CTRL_I2C1_HCLK_enabled_BIT 0

#define KEYCLK_CTRL (*(volatile unsigned *)0x400040B0)
#define KEYCLK_CTRL_Enable_clock_MASK 0x1
#define KEYCLK_CTRL_Enable_clock 0x1
#define KEYCLK_CTRL_Enable_clock_BIT 0

#define ADCCLK_CTRL (*(volatile unsigned *)0x400040B4)
#define ADCCLK_CTRL_Enable_clock_MASK 0x1
#define ADCCLK_CTRL_Enable_clock 0x1
#define ADCCLK_CTRL_Enable_clock_BIT 0

#define PWMCLK_CTRL (*(volatile unsigned *)0x400040B8)
#define PWMCLK_CTRL_PWM2_FREQ_MASK 0xF00
#define PWMCLK_CTRL_PWM2_FREQ_BIT 8
#define PWMCLK_CTRL_PWM1_FREQ_MASK 0xF0
#define PWMCLK_CTRL_PWM1_FREQ_BIT 4
#define PWMCLK_CTRL_PWM2_clock_source_selection_MASK 0x8
#define PWMCLK_CTRL_PWM2_clock_source_selection 0x8
#define PWMCLK_CTRL_PWM2_clock_source_selection_BIT 3
#define PWMCLK_CTRL_Enable_clock_to_PWM2_block_MASK 0x4
#define PWMCLK_CTRL_Enable_clock_to_PWM2_block 0x4
#define PWMCLK_CTRL_Enable_clock_to_PWM2_block_BIT 2
#define PWMCLK_CTRL_PWM1_clock_source_selection_MASK 0x2
#define PWMCLK_CTRL_PWM1_clock_source_selection 0x2
#define PWMCLK_CTRL_PWM1_clock_source_selection_BIT 1
#define PWMCLK_CTRL_Enable_clock_to_PWM1_block_MASK 0x1
#define PWMCLK_CTRL_Enable_clock_to_PWM1_block 0x1
#define PWMCLK_CTRL_Enable_clock_to_PWM1_block_BIT 0

#define TIMCLK_CTRL (*(volatile unsigned *)0x400040BC)
#define TIMCLK_CTRL_HSTimer_clock_enable_control_MASK 0x2
#define TIMCLK_CTRL_HSTimer_clock_enable_control 0x2
#define TIMCLK_CTRL_HSTimer_clock_enable_control_BIT 1
#define TIMCLK_CTRL_Watchdog_clock_enable_control_MASK 0x1
#define TIMCLK_CTRL_Watchdog_clock_enable_control 0x1
#define TIMCLK_CTRL_Watchdog_clock_enable_control_BIT 0

#define SPICLK_CTRL (*(volatile unsigned *)0x400040C4)
#define SPICLK_CTRL_SPI2_DATIO_output_level_MASK 0x80
#define SPICLK_CTRL_SPI2_DATIO_output_level 0x80
#define SPICLK_CTRL_SPI2_DATIO_output_level_BIT 7
#define SPICLK_CTRL_SPI2_CLK_output_level_MASK 0x40
#define SPICLK_CTRL_SPI2_CLK_output_level 0x40
#define SPICLK_CTRL_SPI2_CLK_output_level_BIT 6
#define SPICLK_CTRL_SPI2_Output_pin_control_MASK 0x20
#define SPICLK_CTRL_SPI2_Output_pin_control 0x20
#define SPICLK_CTRL_SPI2_Output_pin_control_BIT 5
#define SPICLK_CTRL_SPI2_clock_enable_control_MASK 0x10
#define SPICLK_CTRL_SPI2_clock_enable_control 0x10
#define SPICLK_CTRL_SPI2_clock_enable_control_BIT 4
#define SPICLK_CTRL_SPI1_DATIO_output_level_MASK 0x8
#define SPICLK_CTRL_SPI1_DATIO_output_level 0x8
#define SPICLK_CTRL_SPI1_DATIO_output_level_BIT 3
#define SPICLK_CTRL_SPI1_CLK_output_level_MASK 0x4
#define SPICLK_CTRL_SPI1_CLK_output_level 0x4
#define SPICLK_CTRL_SPI1_CLK_output_level_BIT 2
#define SPICLK_CTRL_SPI1_Output_pin_control_MASK 0x2
#define SPICLK_CTRL_SPI1_Output_pin_control 0x2
#define SPICLK_CTRL_SPI1_Output_pin_control_BIT 1
#define SPICLK_CTRL_SPI1_clock_enable_control_MASK 0x1
#define SPICLK_CTRL_SPI1_clock_enable_control 0x1
#define SPICLK_CTRL_SPI1_clock_enable_control_BIT 0

#define FLASHCLK_CTRL (*(volatile unsigned *)0x400040C8)
#define FLASHCLK_CTRL_Enable_MLC_NAND_Flash_Controller_interrupt_MASK 0x20
#define FLASHCLK_CTRL_Enable_MLC_NAND_Flash_Controller_interrupt 0x20
#define FLASHCLK_CTRL_Enable_MLC_NAND_Flash_Controller_interrupt_BIT 5
#define FLASHCLK_CTRL_Enable_NAND_DMA_REQ_on_NAND_RnB_MASK 0x10
#define FLASHCLK_CTRL_Enable_NAND_DMA_REQ_on_NAND_RnB 0x10
#define FLASHCLK_CTRL_Enable_NAND_DMA_REQ_on_NAND_RnB_BIT 4
#define FLASHCLK_CTRL_Enable_NAND_DMA_REQ_on_NAND_INT_MASK 0x8
#define FLASHCLK_CTRL_Enable_NAND_DMA_REQ_on_NAND_INT 0x8
#define FLASHCLK_CTRL_Enable_NAND_DMA_REQ_on_NAND_INT_BIT 3
#define FLASHCLK_CTRL_SLC_MLC_Select_MASK 0x4
#define FLASHCLK_CTRL_SLC_MLC_Select 0x4
#define FLASHCLK_CTRL_SLC_MLC_Select_BIT 2
#define FLASHCLK_CTRL_MLC_NAND_Flash_clock_enable_MASK 0x2
#define FLASHCLK_CTRL_MLC_NAND_Flash_clock_enable 0x2
#define FLASHCLK_CTRL_MLC_NAND_Flash_clock_enable_BIT 1
#define FLASHCLK_CTRL_SLC_NAND_Flash_clock_enable_MASK 0x1
#define FLASHCLK_CTRL_SLC_NAND_Flash_clock_enable 0x1
#define FLASHCLK_CTRL_SLC_NAND_Flash_clock_enable_BIT 0

#define U3CLK (*(volatile unsigned *)0x400040D0)
#define U3CLK_Clock_Source_Select_MASK 0x10000
#define U3CLK_Clock_Source_Select 0x10000
#define U3CLK_Clock_Source_Select_BIT 16
#define U3CLK_X_divider_value_MASK 0xFF00
#define U3CLK_X_divider_value_BIT 8
#define U3CLK_Y_divider_value_MASK 0xFF
#define U3CLK_Y_divider_value_BIT 0

#define U4CLK (*(volatile unsigned *)0x400040D4)
#define U4CLK_Clock_Source_Select_MASK 0x10000
#define U4CLK_Clock_Source_Select 0x10000
#define U4CLK_Clock_Source_Select_BIT 16
#define U4CLK_X_divider_value_MASK 0xFF00
#define U4CLK_X_divider_value_BIT 8
#define U4CLK_Y_divider_value_MASK 0xFF
#define U4CLK_Y_divider_value_BIT 0

#define U5CLK (*(volatile unsigned *)0x400040D8)
#define U5CLK_Clock_Source_Select_MASK 0x10000
#define U5CLK_Clock_Source_Select 0x10000
#define U5CLK_Clock_Source_Select_BIT 16
#define U5CLK_X_divider_value_MASK 0xFF00
#define U5CLK_X_divider_value_BIT 8
#define U5CLK_Y_divider_value_MASK 0xFF
#define U5CLK_Y_divider_value_BIT 0

#define U6CLK (*(volatile unsigned *)0x400040DC)
#define U6CLK_Clock_Source_Select_MASK 0x10000
#define U6CLK_Clock_Source_Select 0x10000
#define U6CLK_Clock_Source_Select_BIT 16
#define U6CLK_X_divider_value_MASK 0xFF00
#define U6CLK_X_divider_value_BIT 8
#define U6CLK_Y_divider_value_MASK 0xFF
#define U6CLK_Y_divider_value_BIT 0

#define IRDACLK (*(volatile unsigned *)0x400040E0)
#define IRDACLK_X_divider_value_MASK 0xFF00
#define IRDACLK_X_divider_value_BIT 8
#define IRDACLK_Y_divider_value_MASK 0xFF
#define IRDACLK_Y_divider_value_BIT 0

#define UARTCLK_CTRL (*(volatile unsigned *)0x400040E4)
#define UARTCLK_CTRL_Uart6_HCKL_enabled_MASK 0x8
#define UARTCLK_CTRL_Uart6_HCKL_enabled 0x8
#define UARTCLK_CTRL_Uart6_HCKL_enabled_BIT 3
#define UARTCLK_CTRL_Uart5_HCKL_enabled_MASK 0x4
#define UARTCLK_CTRL_Uart5_HCKL_enabled 0x4
#define UARTCLK_CTRL_Uart5_HCKL_enabled_BIT 2
#define UARTCLK_CTRL_Uart4_HCKL_enabled_MASK 0x2
#define UARTCLK_CTRL_Uart4_HCKL_enabled 0x2
#define UARTCLK_CTRL_Uart4_HCKL_enabled_BIT 1
#define UARTCLK_CTRL_Uart3_HCKL_enabled_MASK 0x1
#define UARTCLK_CTRL_Uart3_HCKL_enabled 0x1
#define UARTCLK_CTRL_Uart3_HCKL_enabled_BIT 0

#define DMACLK_CTRL (*(volatile unsigned *)0x400040E8)
#define DMACLK_CTRL_All_clocks_to_DMA_enabled_MASK 0x1
#define DMACLK_CTRL_All_clocks_to_DMA_enabled 0x1
#define DMACLK_CTRL_All_clocks_to_DMA_enabled_BIT 0

#define USBDIV_CTRL (*(volatile unsigned *)0x4000401C)

#define P0_INTR_ER (*(volatile unsigned *)0x40004020)
#define P0_INTR_ER_P0_0_MASK 0x1
#define P0_INTR_ER_P0_0 0x1
#define P0_INTR_ER_P0_0_BIT 0
#define P0_INTR_ER_P0_1_MASK 0x2
#define P0_INTR_ER_P0_1 0x2
#define P0_INTR_ER_P0_1_BIT 1
#define P0_INTR_ER_P0_2_MASK 0x4
#define P0_INTR_ER_P0_2 0x4
#define P0_INTR_ER_P0_2_BIT 2
#define P0_INTR_ER_P0_3_MASK 0x8
#define P0_INTR_ER_P0_3 0x8
#define P0_INTR_ER_P0_3_BIT 3
#define P0_INTR_ER_P0_4_MASK 0x10
#define P0_INTR_ER_P0_4 0x10
#define P0_INTR_ER_P0_4_BIT 4
#define P0_INTR_ER_P0_5_MASK 0x20
#define P0_INTR_ER_P0_5 0x20
#define P0_INTR_ER_P0_5_BIT 5
#define P0_INTR_ER_P0_6_MASK 0x40
#define P0_INTR_ER_P0_6 0x40
#define P0_INTR_ER_P0_6_BIT 6
#define P0_INTR_ER_P0_7_MASK 0x80
#define P0_INTR_ER_P0_7 0x80
#define P0_INTR_ER_P0_7_BIT 7
#define P0_INTR_ER_P1_0_MASK 0x100
#define P0_INTR_ER_P1_0 0x100
#define P0_INTR_ER_P1_0_BIT 8
#define P0_INTR_ER_P1_1_MASK 0x200
#define P0_INTR_ER_P1_1 0x200
#define P0_INTR_ER_P1_1_BIT 9
#define P0_INTR_ER_P1_2_MASK 0x400
#define P0_INTR_ER_P1_2 0x400
#define P0_INTR_ER_P1_2_BIT 10
#define P0_INTR_ER_P1_3_MASK 0x800
#define P0_INTR_ER_P1_3 0x800
#define P0_INTR_ER_P1_3_BIT 11
#define P0_INTR_ER_P1_4_MASK 0x1000
#define P0_INTR_ER_P1_4 0x1000
#define P0_INTR_ER_P1_4_BIT 12
#define P0_INTR_ER_P1_5_MASK 0x2000
#define P0_INTR_ER_P1_5 0x2000
#define P0_INTR_ER_P1_5_BIT 13
#define P0_INTR_ER_P1_6_MASK 0x4000
#define P0_INTR_ER_P1_6 0x4000
#define P0_INTR_ER_P1_6_BIT 14
#define P0_INTR_ER_P1_7_MASK 0x8000
#define P0_INTR_ER_P1_7 0x8000
#define P0_INTR_ER_P1_7_BIT 15
#define P0_INTR_ER_P1_8_MASK 0x10000
#define P0_INTR_ER_P1_8 0x10000
#define P0_INTR_ER_P1_8_BIT 16
#define P0_INTR_ER_P1_9_MASK 0x20000
#define P0_INTR_ER_P1_9 0x20000
#define P0_INTR_ER_P1_9_BIT 17
#define P0_INTR_ER_P1_10_MASK 0x40000
#define P0_INTR_ER_P1_10 0x40000
#define P0_INTR_ER_P1_10_BIT 18
#define P0_INTR_ER_P1_11_MASK 0x80000
#define P0_INTR_ER_P1_11 0x80000
#define P0_INTR_ER_P1_11_BIT 19
#define P0_INTR_ER_P1_12_MASK 0x100000
#define P0_INTR_ER_P1_12 0x100000
#define P0_INTR_ER_P1_12_BIT 20
#define P0_INTR_ER_P1_13_MASK 0x200000
#define P0_INTR_ER_P1_13 0x200000
#define P0_INTR_ER_P1_13_BIT 21
#define P0_INTR_ER_P1_14_MASK 0x400000
#define P0_INTR_ER_P1_14 0x400000
#define P0_INTR_ER_P1_14_BIT 22
#define P0_INTR_ER_P1_15_MASK 0x800000
#define P0_INTR_ER_P1_15 0x800000
#define P0_INTR_ER_P1_15_BIT 23
#define P0_INTR_ER_P1_16_MASK 0x1000000
#define P0_INTR_ER_P1_16 0x1000000
#define P0_INTR_ER_P1_16_BIT 24
#define P0_INTR_ER_P1_17_MASK 0x2000000
#define P0_INTR_ER_P1_17 0x2000000
#define P0_INTR_ER_P1_17_BIT 25
#define P0_INTR_ER_P1_18_MASK 0x4000000
#define P0_INTR_ER_P1_18 0x4000000
#define P0_INTR_ER_P1_18_BIT 26
#define P0_INTR_ER_P1_19_MASK 0x8000000
#define P0_INTR_ER_P1_19 0x8000000
#define P0_INTR_ER_P1_19_BIT 27
#define P0_INTR_ER_P1_20_MASK 0x10000000
#define P0_INTR_ER_P1_20 0x10000000
#define P0_INTR_ER_P1_20_BIT 28
#define P0_INTR_ER_P1_21_MASK 0x20000000
#define P0_INTR_ER_P1_21 0x20000000
#define P0_INTR_ER_P1_21_BIT 29
#define P0_INTR_ER_P1_22_MASK 0x40000000
#define P0_INTR_ER_P1_22 0x40000000
#define P0_INTR_ER_P1_22_BIT 30
#define P0_INTR_ER_P1_23_MASK 0x80000000
#define P0_INTR_ER_P1_23 0x80000000
#define P0_INTR_ER_P1_23_BIT 31

#define LCDCLK_CTRL (*(volatile unsigned *)0x40004054)
#define LCDCLK_CTRL_DISPLAY_TYPE_MASK 0x300
#define LCDCLK_CTRL_DISPLAY_TYPE_BIT 8
#define LCDCLK_CTRL_MODE_SELECT_MASK 0xC0
#define LCDCLK_CTRL_MODE_SELECT_BIT 6
#define LCDCLK_CTRL_HCLK_ENABLE_MASK 0x20
#define LCDCLK_CTRL_HCLK_ENABLE 0x20
#define LCDCLK_CTRL_HCLK_ENABLE_BIT 5
#define LCDCLK_CTRL_CLKDIV_MASK 0x1F
#define LCDCLK_CTRL_CLKDIV_BIT 0

#define SSP_CTRL (*(volatile unsigned *)0x40004078)
#define SSP_CTRL_SSP1_RX_DMA_MASK 0x20
#define SSP_CTRL_SSP1_RX_DMA 0x20
#define SSP_CTRL_SSP1_RX_DMA_BIT 5
#define SSP_CTRL_SSP1_TX_DMA_MASK 0x10
#define SSP_CTRL_SSP1_TX_DMA 0x10
#define SSP_CTRL_SSP1_TX_DMA_BIT 4
#define SSP_CTRL_SSP0_RX_DMA_MASK 0x8
#define SSP_CTRL_SSP0_RX_DMA 0x8
#define SSP_CTRL_SSP0_RX_DMA_BIT 3
#define SSP_CTRL_SSP0_TX_DMA_MASK 0x4
#define SSP_CTRL_SSP0_TX_DMA 0x4
#define SSP_CTRL_SSP0_TX_DMA_BIT 2
#define SSP_CTRL_SSP1_CLK_enable_MASK 0x2
#define SSP_CTRL_SSP1_CLK_enable 0x2
#define SSP_CTRL_SSP1_CLK_enable_BIT 1
#define SSP_CTRL_SSP0_CLK_enable_MASK 0x1
#define SSP_CTRL_SSP0_CLK_enable 0x1
#define SSP_CTRL_SSP0_CLK_enable_BIT 0

#define I2S_CTRL (*(volatile unsigned *)0x4000407C)
#define I2S_CTRL_I2S1_CLK_TX_MODE_MASK 0x40
#define I2S_CTRL_I2S1_CLK_TX_MODE 0x40
#define I2S_CTRL_I2S1_CLK_TX_MODE_BIT 6
#define I2S_CTRL_I2S1_CLK_RX_MODE_MASK 0x20
#define I2S_CTRL_I2S1_CLK_RX_MODE 0x20
#define I2S_CTRL_I2S1_CLK_RX_MODE_BIT 5
#define I2S_CTRL_I2S1_DMA1_MASK 0x10
#define I2S_CTRL_I2S1_DMA1 0x10
#define I2S_CTRL_I2S1_DMA1_BIT 4
#define I2S_CTRL_I2S0_CLK_TX_MODE_MASK 0x8
#define I2S_CTRL_I2S0_CLK_TX_MODE 0x8
#define I2S_CTRL_I2S0_CLK_TX_MODE_BIT 3
#define I2S_CTRL_I2S0_CLK_RX_MODE_MASK 0x4
#define I2S_CTRL_I2S0_CLK_RX_MODE 0x4
#define I2S_CTRL_I2S0_CLK_RX_MODE_BIT 2
#define I2S_CTRL_I2S1_CLK_enable_MASK 0x2
#define I2S_CTRL_I2S1_CLK_enable 0x2
#define I2S_CTRL_I2S1_CLK_enable_BIT 1
#define I2S_CTRL_I2S0_CLK_enable_MASK 0x1
#define I2S_CTRL_I2S0_CLK_enable 0x1
#define I2S_CTRL_I2S0_CLK_enable_BIT 0

#define MACCLK_CTRL (*(volatile unsigned *)0x40004090)
#define MACCLK_CTRL_HWD_INF_CTRL_MASK 0x18
#define MACCLK_CTRL_HWD_INF_CTRL_BIT 3
#define MACCLK_CTRL_MASTER_CLK_MASK 0x4
#define MACCLK_CTRL_MASTER_CLK 0x4
#define MACCLK_CTRL_MASTER_CLK_BIT 2
#define MACCLK_CTRL_SLAVE_CLK_MASK 0x2
#define MACCLK_CTRL_SLAVE_CLK 0x2
#define MACCLK_CTRL_SLAVE_CLK_BIT 1
#define MACCLK_CTRL_REG_CLK_MASK 0x1
#define MACCLK_CTRL_REG_CLK 0x1
#define MACCLK_CTRL_REG_CLK_BIT 0

#define TIMCLK_CTRL1 (*(volatile unsigned *)0x400040C0)
#define TIMCLK_CTRL1_Motor_Control_clock_enable_MASK 0x40
#define TIMCLK_CTRL1_Motor_Control_clock_enable 0x40
#define TIMCLK_CTRL1_Motor_Control_clock_enable_BIT 6
#define TIMCLK_CTRL1_Timer3_clock_enable_MASK 0x20
#define TIMCLK_CTRL1_Timer3_clock_enable 0x20
#define TIMCLK_CTRL1_Timer3_clock_enable_BIT 5
#define TIMCLK_CTRL1_Timer2_clock_enable_MASK 0x10
#define TIMCLK_CTRL1_Timer2_clock_enable 0x10
#define TIMCLK_CTRL1_Timer2_clock_enable_BIT 4
#define TIMCLK_CTRL1_Timer1_clock_enable_MASK 0x8
#define TIMCLK_CTRL1_Timer1_clock_enable 0x8
#define TIMCLK_CTRL1_Timer1_clock_enable_BIT 3
#define TIMCLK_CTRL1_Timer0_clock_enable_MASK 0x4
#define TIMCLK_CTRL1_Timer0_clock_enable 0x4
#define TIMCLK_CTRL1_Timer0_clock_enable_BIT 2
#define TIMCLK_CTRL1_Timer5_clock_enable_MASK 0x2
#define TIMCLK_CTRL1_Timer5_clock_enable 0x2
#define TIMCLK_CTRL1_Timer5_clock_enable_BIT 1
#define TIMCLK_CTRL1_Timer4_clock_enable_MASK 0x1
#define TIMCLK_CTRL1_Timer4_clock_enable 0x1
#define TIMCLK_CTRL1_Timer4_clock_enable_BIT 0

#define ADCCLK_CTRL1 (*(volatile unsigned *)0x40004060)
#define ADCCLK_CTRL1_ADCCLK_SEL_MASK 0x100
#define ADCCLK_CTRL1_ADCCLK_SEL 0x100
#define ADCCLK_CTRL1_ADCCLK_SEL_BIT 8
#define ADCCLK_CTRL1_ADC_FREQ_MASK 0xFF
#define ADCCLK_CTRL1_ADC_FREQ_BIT 0

#define MIC_ER (*(volatile unsigned *)0x40008000)
#define MIC_ER_Sub2FIQn_MASK 0x80000000
#define MIC_ER_Sub2FIQn 0x80000000
#define MIC_ER_Sub2FIQn_BIT 31
#define MIC_ER_Sub1FIQn_MASK 0x40000000
#define MIC_ER_Sub1FIQn 0x40000000
#define MIC_ER_Sub1FIQn_BIT 30
#define MIC_ER_Ethernet_MASK 0x20000000
#define MIC_ER_Ethernet 0x20000000
#define MIC_ER_Ethernet_BIT 29
#define MIC_ER_DMAINT_MASK 0x10000000
#define MIC_ER_DMAINT 0x10000000
#define MIC_ER_DMAINT_BIT 28
#define MIC_ER_MSTIMER_INT_MASK 0x8000000
#define MIC_ER_MSTIMER_INT 0x8000000
#define MIC_ER_MSTIMER_INT_BIT 27
#define MIC_ER_IIR1_MASK 0x4000000
#define MIC_ER_IIR1 0x4000000
#define MIC_ER_IIR1_BIT 26
#define MIC_ER_IIR2_MASK 0x2000000
#define MIC_ER_IIR2 0x2000000
#define MIC_ER_IIR2_BIT 25
#define MIC_ER_IIR7_MASK 0x1000000
#define MIC_ER_IIR7 0x1000000
#define MIC_ER_IIR7_BIT 24
#define MIC_ER_I2S1_MASK 0x800000
#define MIC_ER_I2S1 0x800000
#define MIC_ER_I2S1_BIT 23
#define MIC_ER_I2S0_MASK 0x400000
#define MIC_ER_I2S0 0x400000
#define MIC_ER_I2S0_BIT 22
#define MIC_ER_SSP1_MASK 0x200000
#define MIC_ER_SSP1 0x200000
#define MIC_ER_SSP1_BIT 21
#define MIC_ER_SSP0_MASK 0x100000
#define MIC_ER_SSP0 0x100000
#define MIC_ER_SSP0_BIT 20
#define MIC_ER_Timer3_MASK 0x80000
#define MIC_ER_Timer3 0x80000
#define MIC_ER_Timer3_BIT 19
#define MIC_ER_Timer2_MASK 0x40000
#define MIC_ER_Timer2 0x40000
#define MIC_ER_Timer2_BIT 18
#define MIC_ER_Timer1_MASK 0x20000
#define MIC_ER_Timer1 0x20000
#define MIC_ER_Timer1_BIT 17
#define MIC_ER_Timer0_MASK 0x10000
#define MIC_ER_Timer0 0x10000
#define MIC_ER_Timer0_BIT 16
#define MIC_ER_SD0_INT_MASK 0x8000
#define MIC_ER_SD0_INT 0x8000
#define MIC_ER_SD0_INT_BIT 15
#define MIC_ER_LCD_INT_MASK 0x4000
#define MIC_ER_LCD_INT 0x4000
#define MIC_ER_LCD_INT_BIT 14
#define MIC_ER_SD1_INT_MASK 0x2000
#define MIC_ER_SD1_INT 0x2000
#define MIC_ER_SD1_INT_BIT 13
#define MIC_ER_FLASH_INT_MASK 0x800
#define MIC_ER_FLASH_INT 0x800
#define MIC_ER_FLASH_INT_BIT 11
#define MIC_ER_IIR6_MASK 0x400
#define MIC_ER_IIR6 0x400
#define MIC_ER_IIR6_BIT 10
#define MIC_ER_IIR5_MASK 0x200
#define MIC_ER_IIR5 0x200
#define MIC_ER_IIR5_BIT 9
#define MIC_ER_IIR4_MASK 0x100
#define MIC_ER_IIR4 0x100
#define MIC_ER_IIR4_BIT 8
#define MIC_ER_IIR3_MASK 0x80
#define MIC_ER_IIR3 0x80
#define MIC_ER_IIR3_BIT 7
#define MIC_ER_WATCH_INT_MASK 0x40
#define MIC_ER_WATCH_INT 0x40
#define MIC_ER_WATCH_INT_BIT 6
#define MIC_ER_HSTIMER_INT_MASK 0x20
#define MIC_ER_HSTIMER_INT 0x20
#define MIC_ER_HSTIMER_INT_BIT 5
#define MIC_ER_Timer5_MASK 0x10
#define MIC_ER_Timer5 0x10
#define MIC_ER_Timer5_BIT 4
#define MIC_ER_Timer4_MASK 0x8
#define MIC_ER_Timer4 0x8
#define MIC_ER_Timer4_BIT 3
#define MIC_ER_Sub2IRQn_MASK 0x2
#define MIC_ER_Sub2IRQn 0x2
#define MIC_ER_Sub2IRQn_BIT 1
#define MIC_ER_Sub1IRQn_MASK 0x1
#define MIC_ER_Sub1IRQn 0x1
#define MIC_ER_Sub1IRQn_BIT 0

#define MIC_RSR (*(volatile unsigned *)0x40008004)
#define MIC_RSR_Sub2FIQn_MASK 0x80000000
#define MIC_RSR_Sub2FIQn 0x80000000
#define MIC_RSR_Sub2FIQn_BIT 31
#define MIC_RSR_Sub1FIQn_MASK 0x40000000
#define MIC_RSR_Sub1FIQn 0x40000000
#define MIC_RSR_Sub1FIQn_BIT 30
#define MIC_RSR_Ethernet_MASK 0x20000000
#define MIC_RSR_Ethernet 0x20000000
#define MIC_RSR_Ethernet_BIT 29
#define MIC_RSR_DMAINT_MASK 0x10000000
#define MIC_RSR_DMAINT 0x10000000
#define MIC_RSR_DMAINT_BIT 28
#define MIC_RSR_MSTIMER_INT_MASK 0x8000000
#define MIC_RSR_MSTIMER_INT 0x8000000
#define MIC_RSR_MSTIMER_INT_BIT 27
#define MIC_RSR_IIR1_MASK 0x4000000
#define MIC_RSR_IIR1 0x4000000
#define MIC_RSR_IIR1_BIT 26
#define MIC_RSR_IIR2_MASK 0x2000000
#define MIC_RSR_IIR2 0x2000000
#define MIC_RSR_IIR2_BIT 25
#define MIC_RSR_IIR7_MASK 0x1000000
#define MIC_RSR_IIR7 0x1000000
#define MIC_RSR_IIR7_BIT 24
#define MIC_RSR_I2S1_MASK 0x800000
#define MIC_RSR_I2S1 0x800000
#define MIC_RSR_I2S1_BIT 23
#define MIC_RSR_I2S0_MASK 0x400000
#define MIC_RSR_I2S0 0x400000
#define MIC_RSR_I2S0_BIT 22
#define MIC_RSR_SSP1_MASK 0x200000
#define MIC_RSR_SSP1 0x200000
#define MIC_RSR_SSP1_BIT 21
#define MIC_RSR_SSP0_MASK 0x100000
#define MIC_RSR_SSP0 0x100000
#define MIC_RSR_SSP0_BIT 20
#define MIC_RSR_Timer3_MASK 0x80000
#define MIC_RSR_Timer3 0x80000
#define MIC_RSR_Timer3_BIT 19
#define MIC_RSR_Timer2_MASK 0x40000
#define MIC_RSR_Timer2 0x40000
#define MIC_RSR_Timer2_BIT 18
#define MIC_RSR_Timer1_MASK 0x20000
#define MIC_RSR_Timer1 0x20000
#define MIC_RSR_Timer1_BIT 17
#define MIC_RSR_Timer0_MASK 0x10000
#define MIC_RSR_Timer0 0x10000
#define MIC_RSR_Timer0_BIT 16
#define MIC_RSR_SD0_INT_MASK 0x8000
#define MIC_RSR_SD0_INT 0x8000
#define MIC_RSR_SD0_INT_BIT 15
#define MIC_RSR_LCD_INT_MASK 0x4000
#define MIC_RSR_LCD_INT 0x4000
#define MIC_RSR_LCD_INT_BIT 14
#define MIC_RSR_SD1_INT_MASK 0x2000
#define MIC_RSR_SD1_INT 0x2000
#define MIC_RSR_SD1_INT_BIT 13
#define MIC_RSR_FLASH_INT_MASK 0x800
#define MIC_RSR_FLASH_INT 0x800
#define MIC_RSR_FLASH_INT_BIT 11
#define MIC_RSR_IIR6_MASK 0x400
#define MIC_RSR_IIR6 0x400
#define MIC_RSR_IIR6_BIT 10
#define MIC_RSR_IIR5_MASK 0x200
#define MIC_RSR_IIR5 0x200
#define MIC_RSR_IIR5_BIT 9
#define MIC_RSR_IIR4_MASK 0x100
#define MIC_RSR_IIR4 0x100
#define MIC_RSR_IIR4_BIT 8
#define MIC_RSR_IIR3_MASK 0x80
#define MIC_RSR_IIR3 0x80
#define MIC_RSR_IIR3_BIT 7
#define MIC_RSR_WATCH_INT_MASK 0x40
#define MIC_RSR_WATCH_INT 0x40
#define MIC_RSR_WATCH_INT_BIT 6
#define MIC_RSR_HSTIMER_INT_MASK 0x20
#define MIC_RSR_HSTIMER_INT 0x20
#define MIC_RSR_HSTIMER_INT_BIT 5
#define MIC_RSR_Timer5_MASK 0x10
#define MIC_RSR_Timer5 0x10
#define MIC_RSR_Timer5_BIT 4
#define MIC_RSR_Timer4_MASK 0x8
#define MIC_RSR_Timer4 0x8
#define MIC_RSR_Timer4_BIT 3
#define MIC_RSR_Sub2IRQn_MASK 0x2
#define MIC_RSR_Sub2IRQn 0x2
#define MIC_RSR_Sub2IRQn_BIT 1
#define MIC_RSR_Sub1IRQn_MASK 0x1
#define MIC_RSR_Sub1IRQn 0x1
#define MIC_RSR_Sub1IRQn_BIT 0

#define MIC_SR (*(volatile unsigned *)0x40008008)
#define MIC_SR_Sub2FIQn_MASK 0x80000000
#define MIC_SR_Sub2FIQn 0x80000000
#define MIC_SR_Sub2FIQn_BIT 31
#define MIC_SR_Sub1FIQn_MASK 0x40000000
#define MIC_SR_Sub1FIQn 0x40000000
#define MIC_SR_Sub1FIQn_BIT 30
#define MIC_SR_Ethernet_MASK 0x20000000
#define MIC_SR_Ethernet 0x20000000
#define MIC_SR_Ethernet_BIT 29
#define MIC_SR_DMAINT_MASK 0x10000000
#define MIC_SR_DMAINT 0x10000000
#define MIC_SR_DMAINT_BIT 28
#define MIC_SR_MSTIMER_INT_MASK 0x8000000
#define MIC_SR_MSTIMER_INT 0x8000000
#define MIC_SR_MSTIMER_INT_BIT 27
#define MIC_SR_IIR1_MASK 0x4000000
#define MIC_SR_IIR1 0x4000000
#define MIC_SR_IIR1_BIT 26
#define MIC_SR_IIR2_MASK 0x2000000
#define MIC_SR_IIR2 0x2000000
#define MIC_SR_IIR2_BIT 25
#define MIC_SR_IIR7_MASK 0x1000000
#define MIC_SR_IIR7 0x1000000
#define MIC_SR_IIR7_BIT 24
#define MIC_SR_I2S1_MASK 0x800000
#define MIC_SR_I2S1 0x800000
#define MIC_SR_I2S1_BIT 23
#define MIC_SR_I2S0_MASK 0x400000
#define MIC_SR_I2S0 0x400000
#define MIC_SR_I2S0_BIT 22
#define MIC_SR_SSP1_MASK 0x200000
#define MIC_SR_SSP1 0x200000
#define MIC_SR_SSP1_BIT 21
#define MIC_SR_SSP0_MASK 0x100000
#define MIC_SR_SSP0 0x100000
#define MIC_SR_SSP0_BIT 20
#define MIC_SR_Timer3_MASK 0x80000
#define MIC_SR_Timer3 0x80000
#define MIC_SR_Timer3_BIT 19
#define MIC_SR_Timer2_MASK 0x40000
#define MIC_SR_Timer2 0x40000
#define MIC_SR_Timer2_BIT 18
#define MIC_SR_Timer1_MASK 0x20000
#define MIC_SR_Timer1 0x20000
#define MIC_SR_Timer1_BIT 17
#define MIC_SR_Timer0_MASK 0x10000
#define MIC_SR_Timer0 0x10000
#define MIC_SR_Timer0_BIT 16
#define MIC_SR_SD0_INT_MASK 0x8000
#define MIC_SR_SD0_INT 0x8000
#define MIC_SR_SD0_INT_BIT 15
#define MIC_SR_LCD_INT_MASK 0x4000
#define MIC_SR_LCD_INT 0x4000
#define MIC_SR_LCD_INT_BIT 14
#define MIC_SR_SD1_INT_MASK 0x2000
#define MIC_SR_SD1_INT 0x2000
#define MIC_SR_SD1_INT_BIT 13
#define MIC_SR_FLASH_INT_MASK 0x800
#define MIC_SR_FLASH_INT 0x800
#define MIC_SR_FLASH_INT_BIT 11
#define MIC_SR_IIR6_MASK 0x400
#define MIC_SR_IIR6 0x400
#define MIC_SR_IIR6_BIT 10
#define MIC_SR_IIR5_MASK 0x200
#define MIC_SR_IIR5 0x200
#define MIC_SR_IIR5_BIT 9
#define MIC_SR_IIR4_MASK 0x100
#define MIC_SR_IIR4 0x100
#define MIC_SR_IIR4_BIT 8
#define MIC_SR_IIR3_MASK 0x80
#define MIC_SR_IIR3 0x80
#define MIC_SR_IIR3_BIT 7
#define MIC_SR_WATCH_INT_MASK 0x40
#define MIC_SR_WATCH_INT 0x40
#define MIC_SR_WATCH_INT_BIT 6
#define MIC_SR_HSTIMER_INT_MASK 0x20
#define MIC_SR_HSTIMER_INT 0x20
#define MIC_SR_HSTIMER_INT_BIT 5
#define MIC_SR_Timer5_MASK 0x10
#define MIC_SR_Timer5 0x10
#define MIC_SR_Timer5_BIT 4
#define MIC_SR_Timer4_MASK 0x8
#define MIC_SR_Timer4 0x8
#define MIC_SR_Timer4_BIT 3
#define MIC_SR_Sub2IRQn_MASK 0x2
#define MIC_SR_Sub2IRQn 0x2
#define MIC_SR_Sub2IRQn_BIT 1
#define MIC_SR_Sub1IRQn_MASK 0x1
#define MIC_SR_Sub1IRQn 0x1
#define MIC_SR_Sub1IRQn_BIT 0

#define MIC_APR (*(volatile unsigned *)0x4000800C)
#define MIC_APR_Sub2FIQn_MASK 0x80000000
#define MIC_APR_Sub2FIQn 0x80000000
#define MIC_APR_Sub2FIQn_BIT 31
#define MIC_APR_Sub1FIQn_MASK 0x40000000
#define MIC_APR_Sub1FIQn 0x40000000
#define MIC_APR_Sub1FIQn_BIT 30
#define MIC_APR_Ethernet_MASK 0x20000000
#define MIC_APR_Ethernet 0x20000000
#define MIC_APR_Ethernet_BIT 29
#define MIC_APR_DMAINT_MASK 0x10000000
#define MIC_APR_DMAINT 0x10000000
#define MIC_APR_DMAINT_BIT 28
#define MIC_APR_MSTIMER_INT_MASK 0x8000000
#define MIC_APR_MSTIMER_INT 0x8000000
#define MIC_APR_MSTIMER_INT_BIT 27
#define MIC_APR_IIR1_MASK 0x4000000
#define MIC_APR_IIR1 0x4000000
#define MIC_APR_IIR1_BIT 26
#define MIC_APR_IIR2_MASK 0x2000000
#define MIC_APR_IIR2 0x2000000
#define MIC_APR_IIR2_BIT 25
#define MIC_APR_IIR7_MASK 0x1000000
#define MIC_APR_IIR7 0x1000000
#define MIC_APR_IIR7_BIT 24
#define MIC_APR_I2S1_MASK 0x800000
#define MIC_APR_I2S1 0x800000
#define MIC_APR_I2S1_BIT 23
#define MIC_APR_I2S0_MASK 0x400000
#define MIC_APR_I2S0 0x400000
#define MIC_APR_I2S0_BIT 22
#define MIC_APR_SSP1_MASK 0x200000
#define MIC_APR_SSP1 0x200000
#define MIC_APR_SSP1_BIT 21
#define MIC_APR_SSP0_MASK 0x100000
#define MIC_APR_SSP0 0x100000
#define MIC_APR_SSP0_BIT 20
#define MIC_APR_Timer3_MASK 0x80000
#define MIC_APR_Timer3 0x80000
#define MIC_APR_Timer3_BIT 19
#define MIC_APR_Timer2_MASK 0x40000
#define MIC_APR_Timer2 0x40000
#define MIC_APR_Timer2_BIT 18
#define MIC_APR_Timer1_MASK 0x20000
#define MIC_APR_Timer1 0x20000
#define MIC_APR_Timer1_BIT 17
#define MIC_APR_Timer0_MASK 0x10000
#define MIC_APR_Timer0 0x10000
#define MIC_APR_Timer0_BIT 16
#define MIC_APR_SD0_INT_MASK 0x8000
#define MIC_APR_SD0_INT 0x8000
#define MIC_APR_SD0_INT_BIT 15
#define MIC_APR_LCD_INT_MASK 0x4000
#define MIC_APR_LCD_INT 0x4000
#define MIC_APR_LCD_INT_BIT 14
#define MIC_APR_SD1_INT_MASK 0x2000
#define MIC_APR_SD1_INT 0x2000
#define MIC_APR_SD1_INT_BIT 13
#define MIC_APR_FLASH_INT_MASK 0x800
#define MIC_APR_FLASH_INT 0x800
#define MIC_APR_FLASH_INT_BIT 11
#define MIC_APR_IIR6_MASK 0x400
#define MIC_APR_IIR6 0x400
#define MIC_APR_IIR6_BIT 10
#define MIC_APR_IIR5_MASK 0x200
#define MIC_APR_IIR5 0x200
#define MIC_APR_IIR5_BIT 9
#define MIC_APR_IIR4_MASK 0x100
#define MIC_APR_IIR4 0x100
#define MIC_APR_IIR4_BIT 8
#define MIC_APR_IIR3_MASK 0x80
#define MIC_APR_IIR3 0x80
#define MIC_APR_IIR3_BIT 7
#define MIC_APR_WATCH_INT_MASK 0x40
#define MIC_APR_WATCH_INT 0x40
#define MIC_APR_WATCH_INT_BIT 6
#define MIC_APR_HSTIMER_INT_MASK 0x20
#define MIC_APR_HSTIMER_INT 0x20
#define MIC_APR_HSTIMER_INT_BIT 5
#define MIC_APR_Timer5_MASK 0x10
#define MIC_APR_Timer5 0x10
#define MIC_APR_Timer5_BIT 4
#define MIC_APR_Timer4_MASK 0x8
#define MIC_APR_Timer4 0x8
#define MIC_APR_Timer4_BIT 3
#define MIC_APR_Sub2IRQn_MASK 0x2
#define MIC_APR_Sub2IRQn 0x2
#define MIC_APR_Sub2IRQn_BIT 1
#define MIC_APR_Sub1IRQn_MASK 0x1
#define MIC_APR_Sub1IRQn 0x1
#define MIC_APR_Sub1IRQn_BIT 0

#define MIC_ATR (*(volatile unsigned *)0x40008010)
#define MIC_ATR_Sub2FIQn_MASK 0x80000000
#define MIC_ATR_Sub2FIQn 0x80000000
#define MIC_ATR_Sub2FIQn_BIT 31
#define MIC_ATR_Sub1FIQn_MASK 0x40000000
#define MIC_ATR_Sub1FIQn 0x40000000
#define MIC_ATR_Sub1FIQn_BIT 30
#define MIC_ATR_Ethernet_MASK 0x20000000
#define MIC_ATR_Ethernet 0x20000000
#define MIC_ATR_Ethernet_BIT 29
#define MIC_ATR_DMAINT_MASK 0x10000000
#define MIC_ATR_DMAINT 0x10000000
#define MIC_ATR_DMAINT_BIT 28
#define MIC_ATR_MSTIMER_INT_MASK 0x8000000
#define MIC_ATR_MSTIMER_INT 0x8000000
#define MIC_ATR_MSTIMER_INT_BIT 27
#define MIC_ATR_IIR1_MASK 0x4000000
#define MIC_ATR_IIR1 0x4000000
#define MIC_ATR_IIR1_BIT 26
#define MIC_ATR_IIR2_MASK 0x2000000
#define MIC_ATR_IIR2 0x2000000
#define MIC_ATR_IIR2_BIT 25
#define MIC_ATR_IIR7_MASK 0x1000000
#define MIC_ATR_IIR7 0x1000000
#define MIC_ATR_IIR7_BIT 24
#define MIC_ATR_I2S1_MASK 0x800000
#define MIC_ATR_I2S1 0x800000
#define MIC_ATR_I2S1_BIT 23
#define MIC_ATR_I2S0_MASK 0x400000
#define MIC_ATR_I2S0 0x400000
#define MIC_ATR_I2S0_BIT 22
#define MIC_ATR_SSP1_MASK 0x200000
#define MIC_ATR_SSP1 0x200000
#define MIC_ATR_SSP1_BIT 21
#define MIC_ATR_SSP0_MASK 0x100000
#define MIC_ATR_SSP0 0x100000
#define MIC_ATR_SSP0_BIT 20
#define MIC_ATR_Timer3_MASK 0x80000
#define MIC_ATR_Timer3 0x80000
#define MIC_ATR_Timer3_BIT 19
#define MIC_ATR_Timer2_MASK 0x40000
#define MIC_ATR_Timer2 0x40000
#define MIC_ATR_Timer2_BIT 18
#define MIC_ATR_Timer1_MASK 0x20000
#define MIC_ATR_Timer1 0x20000
#define MIC_ATR_Timer1_BIT 17
#define MIC_ATR_Timer0_MASK 0x10000
#define MIC_ATR_Timer0 0x10000
#define MIC_ATR_Timer0_BIT 16
#define MIC_ATR_SD0_INT_MASK 0x8000
#define MIC_ATR_SD0_INT 0x8000
#define MIC_ATR_SD0_INT_BIT 15
#define MIC_ATR_LCD_INT_MASK 0x4000
#define MIC_ATR_LCD_INT 0x4000
#define MIC_ATR_LCD_INT_BIT 14
#define MIC_ATR_SD1_INT_MASK 0x2000
#define MIC_ATR_SD1_INT 0x2000
#define MIC_ATR_SD1_INT_BIT 13
#define MIC_ATR_FLASH_INT_MASK 0x800
#define MIC_ATR_FLASH_INT 0x800
#define MIC_ATR_FLASH_INT_BIT 11
#define MIC_ATR_IIR6_MASK 0x400
#define MIC_ATR_IIR6 0x400
#define MIC_ATR_IIR6_BIT 10
#define MIC_ATR_IIR5_MASK 0x200
#define MIC_ATR_IIR5 0x200
#define MIC_ATR_IIR5_BIT 9
#define MIC_ATR_IIR4_MASK 0x100
#define MIC_ATR_IIR4 0x100
#define MIC_ATR_IIR4_BIT 8
#define MIC_ATR_IIR3_MASK 0x80
#define MIC_ATR_IIR3 0x80
#define MIC_ATR_IIR3_BIT 7
#define MIC_ATR_WATCH_INT_MASK 0x40
#define MIC_ATR_WATCH_INT 0x40
#define MIC_ATR_WATCH_INT_BIT 6
#define MIC_ATR_HSTIMER_INT_MASK 0x20
#define MIC_ATR_HSTIMER_INT 0x20
#define MIC_ATR_HSTIMER_INT_BIT 5
#define MIC_ATR_Timer5_MASK 0x10
#define MIC_ATR_Timer5 0x10
#define MIC_ATR_Timer5_BIT 4
#define MIC_ATR_Timer4_MASK 0x8
#define MIC_ATR_Timer4 0x8
#define MIC_ATR_Timer4_BIT 3
#define MIC_ATR_Sub2IRQn_MASK 0x2
#define MIC_ATR_Sub2IRQn 0x2
#define MIC_ATR_Sub2IRQn_BIT 1
#define MIC_ATR_Sub1IRQn_MASK 0x1
#define MIC_ATR_Sub1IRQn 0x1
#define MIC_ATR_Sub1IRQn_BIT 0

#define MIC_ITR (*(volatile unsigned *)0x40008014)
#define MIC_ITR_Sub2FIQn_MASK 0x80000000
#define MIC_ITR_Sub2FIQn 0x80000000
#define MIC_ITR_Sub2FIQn_BIT 31
#define MIC_ITR_Sub1FIQn_MASK 0x40000000
#define MIC_ITR_Sub1FIQn 0x40000000
#define MIC_ITR_Sub1FIQn_BIT 30
#define MIC_ITR_Ethernet_MASK 0x20000000
#define MIC_ITR_Ethernet 0x20000000
#define MIC_ITR_Ethernet_BIT 29
#define MIC_ITR_DMAINT_MASK 0x10000000
#define MIC_ITR_DMAINT 0x10000000
#define MIC_ITR_DMAINT_BIT 28
#define MIC_ITR_MSTIMER_INT_MASK 0x8000000
#define MIC_ITR_MSTIMER_INT 0x8000000
#define MIC_ITR_MSTIMER_INT_BIT 27
#define MIC_ITR_IIR1_MASK 0x4000000
#define MIC_ITR_IIR1 0x4000000
#define MIC_ITR_IIR1_BIT 26
#define MIC_ITR_IIR2_MASK 0x2000000
#define MIC_ITR_IIR2 0x2000000
#define MIC_ITR_IIR2_BIT 25
#define MIC_ITR_IIR7_MASK 0x1000000
#define MIC_ITR_IIR7 0x1000000
#define MIC_ITR_IIR7_BIT 24
#define MIC_ITR_I2S1_MASK 0x800000
#define MIC_ITR_I2S1 0x800000
#define MIC_ITR_I2S1_BIT 23
#define MIC_ITR_I2S0_MASK 0x400000
#define MIC_ITR_I2S0 0x400000
#define MIC_ITR_I2S0_BIT 22
#define MIC_ITR_SSP1_MASK 0x200000
#define MIC_ITR_SSP1 0x200000
#define MIC_ITR_SSP1_BIT 21
#define MIC_ITR_SSP0_MASK 0x100000
#define MIC_ITR_SSP0 0x100000
#define MIC_ITR_SSP0_BIT 20
#define MIC_ITR_Timer3_MASK 0x80000
#define MIC_ITR_Timer3 0x80000
#define MIC_ITR_Timer3_BIT 19
#define MIC_ITR_Timer2_MASK 0x40000
#define MIC_ITR_Timer2 0x40000
#define MIC_ITR_Timer2_BIT 18
#define MIC_ITR_Timer1_MASK 0x20000
#define MIC_ITR_Timer1 0x20000
#define MIC_ITR_Timer1_BIT 17
#define MIC_ITR_Timer0_MASK 0x10000
#define MIC_ITR_Timer0 0x10000
#define MIC_ITR_Timer0_BIT 16
#define MIC_ITR_SD0_INT_MASK 0x8000
#define MIC_ITR_SD0_INT 0x8000
#define MIC_ITR_SD0_INT_BIT 15
#define MIC_ITR_LCD_INT_MASK 0x4000
#define MIC_ITR_LCD_INT 0x4000
#define MIC_ITR_LCD_INT_BIT 14
#define MIC_ITR_SD1_INT_MASK 0x2000
#define MIC_ITR_SD1_INT 0x2000
#define MIC_ITR_SD1_INT_BIT 13
#define MIC_ITR_FLASH_INT_MASK 0x800
#define MIC_ITR_FLASH_INT 0x800
#define MIC_ITR_FLASH_INT_BIT 11
#define MIC_ITR_IIR6_MASK 0x400
#define MIC_ITR_IIR6 0x400
#define MIC_ITR_IIR6_BIT 10
#define MIC_ITR_IIR5_MASK 0x200
#define MIC_ITR_IIR5 0x200
#define MIC_ITR_IIR5_BIT 9
#define MIC_ITR_IIR4_MASK 0x100
#define MIC_ITR_IIR4 0x100
#define MIC_ITR_IIR4_BIT 8
#define MIC_ITR_IIR3_MASK 0x80
#define MIC_ITR_IIR3 0x80
#define MIC_ITR_IIR3_BIT 7
#define MIC_ITR_WATCH_INT_MASK 0x40
#define MIC_ITR_WATCH_INT 0x40
#define MIC_ITR_WATCH_INT_BIT 6
#define MIC_ITR_HSTIMER_INT_MASK 0x20
#define MIC_ITR_HSTIMER_INT 0x20
#define MIC_ITR_HSTIMER_INT_BIT 5
#define MIC_ITR_Timer5_MASK 0x10
#define MIC_ITR_Timer5 0x10
#define MIC_ITR_Timer5_BIT 4
#define MIC_ITR_Timer4_MASK 0x8
#define MIC_ITR_Timer4 0x8
#define MIC_ITR_Timer4_BIT 3
#define MIC_ITR_Sub2IRQn_MASK 0x2
#define MIC_ITR_Sub2IRQn 0x2
#define MIC_ITR_Sub2IRQn_BIT 1
#define MIC_ITR_Sub1IRQn_MASK 0x1
#define MIC_ITR_Sub1IRQn 0x1
#define MIC_ITR_Sub1IRQn_BIT 0

#define SIC1_ER (*(volatile unsigned *)0x4000C000)
#define SIC1_ER_USB_i2c_int_MASK 0x80000000
#define SIC1_ER_USB_i2c_int 0x80000000
#define SIC1_ER_USB_i2c_int_BIT 31
#define SIC1_ER_USB_dev_hp_int_MASK 0x40000000
#define SIC1_ER_USB_dev_hp_int 0x40000000
#define SIC1_ER_USB_dev_hp_int_BIT 30
#define SIC1_ER_USB_dev_lp_int_MASK 0x20000000
#define SIC1_ER_USB_dev_lp_int 0x20000000
#define SIC1_ER_USB_dev_lp_int_BIT 29
#define SIC1_ER_USB_dev_dma_int_MASK 0x10000000
#define SIC1_ER_USB_dev_dma_int 0x10000000
#define SIC1_ER_USB_dev_dma_int_BIT 28
#define SIC1_ER_USB_host_int_MASK 0x8000000
#define SIC1_ER_USB_host_int 0x8000000
#define SIC1_ER_USB_host_int_BIT 27
#define SIC1_ER_USB_otg_atx_int_n_MASK 0x4000000
#define SIC1_ER_USB_otg_atx_int_n 0x4000000
#define SIC1_ER_USB_otg_atx_int_n_BIT 26
#define SIC1_ER_USB_otg_timer_int_MASK 0x2000000
#define SIC1_ER_USB_otg_timer_int 0x2000000
#define SIC1_ER_USB_otg_timer_int_BIT 25
#define SIC1_ER_SW_INT_MASK 0x1000000
#define SIC1_ER_SW_INT 0x1000000
#define SIC1_ER_SW_INT_BIT 24
#define SIC1_ER_SPI1_INT_MASK 0x800000
#define SIC1_ER_SPI1_INT 0x800000
#define SIC1_ER_SPI1_INT_BIT 23
#define SIC1_ER_KEY_IRQ_MASK 0x400000
#define SIC1_ER_KEY_IRQ 0x400000
#define SIC1_ER_KEY_IRQ_BIT 22
#define SIC1_ER_RTC_INT_MASK 0x100000
#define SIC1_ER_RTC_INT 0x100000
#define SIC1_ER_RTC_INT_BIT 20
#define SIC1_ER_I2C_1_INT_MASK 0x80000
#define SIC1_ER_I2C_1_INT 0x80000
#define SIC1_ER_I2C_1_INT_BIT 19
#define SIC1_ER_I2C_2_INT_MASK 0x40000
#define SIC1_ER_I2C_2_INT 0x40000
#define SIC1_ER_I2C_2_INT_BIT 18
#define SIC1_ER_PLL397_INT_MASK 0x20000
#define SIC1_ER_PLL397_INT 0x20000
#define SIC1_ER_PLL397_INT_BIT 17
#define SIC1_ER_PLLHCLK_INT_MASK 0x4000
#define SIC1_ER_PLLHCLK_INT 0x4000
#define SIC1_ER_PLLHCLK_INT_BIT 14
#define SIC1_ER_PLLUSB_INT_MASK 0x2000
#define SIC1_ER_PLLUSB_INT 0x2000
#define SIC1_ER_PLLUSB_INT_BIT 13
#define SIC1_ER_SPI2_INT_MASK 0x1000
#define SIC1_ER_SPI2_INT 0x1000
#define SIC1_ER_SPI2_INT_BIT 12
#define SIC1_ER_TS_AUX_MASK 0x100
#define SIC1_ER_TS_AUX 0x100
#define SIC1_ER_TS_AUX_BIT 8
#define SIC1_ER_ADC_INT_MASK 0x80
#define SIC1_ER_ADC_INT 0x80
#define SIC1_ER_ADC_INT_BIT 7
#define SIC1_ER_TS_P_MASK 0x40
#define SIC1_ER_TS_P 0x40
#define SIC1_ER_TS_P_BIT 6
#define SIC1_ER_GPI_28_MASK 0x20
#define SIC1_ER_GPI_28 0x20
#define SIC1_ER_GPI_28_BIT 5
#define SIC1_ER_JTAG_COMM_RX_MASK 0x4
#define SIC1_ER_JTAG_COMM_RX 0x4
#define SIC1_ER_JTAG_COMM_RX_BIT 2
#define SIC1_ER_JTAG_COMM_TX_MASK 0x2
#define SIC1_ER_JTAG_COMM_TX 0x2
#define SIC1_ER_JTAG_COMM_TX_BIT 1

#define SIC1_RSR (*(volatile unsigned *)0x4000C004)
#define SIC1_RSR_USB_i2c_int_MASK 0x80000000
#define SIC1_RSR_USB_i2c_int 0x80000000
#define SIC1_RSR_USB_i2c_int_BIT 31
#define SIC1_RSR_USB_dev_hp_int_MASK 0x40000000
#define SIC1_RSR_USB_dev_hp_int 0x40000000
#define SIC1_RSR_USB_dev_hp_int_BIT 30
#define SIC1_RSR_USB_dev_lp_int_MASK 0x20000000
#define SIC1_RSR_USB_dev_lp_int 0x20000000
#define SIC1_RSR_USB_dev_lp_int_BIT 29
#define SIC1_RSR_USB_dev_dma_int_MASK 0x10000000
#define SIC1_RSR_USB_dev_dma_int 0x10000000
#define SIC1_RSR_USB_dev_dma_int_BIT 28
#define SIC1_RSR_USB_host_int_MASK 0x8000000
#define SIC1_RSR_USB_host_int 0x8000000
#define SIC1_RSR_USB_host_int_BIT 27
#define SIC1_RSR_USB_otg_atx_int_n_MASK 0x4000000
#define SIC1_RSR_USB_otg_atx_int_n 0x4000000
#define SIC1_RSR_USB_otg_atx_int_n_BIT 26
#define SIC1_RSR_USB_otg_timer_int_MASK 0x2000000
#define SIC1_RSR_USB_otg_timer_int 0x2000000
#define SIC1_RSR_USB_otg_timer_int_BIT 25
#define SIC1_RSR_SW_INT_MASK 0x1000000
#define SIC1_RSR_SW_INT 0x1000000
#define SIC1_RSR_SW_INT_BIT 24
#define SIC1_RSR_SPI1_INT_MASK 0x800000
#define SIC1_RSR_SPI1_INT 0x800000
#define SIC1_RSR_SPI1_INT_BIT 23
#define SIC1_RSR_KEY_IRQ_MASK 0x400000
#define SIC1_RSR_KEY_IRQ 0x400000
#define SIC1_RSR_KEY_IRQ_BIT 22
#define SIC1_RSR_RTC_INT_MASK 0x100000
#define SIC1_RSR_RTC_INT 0x100000
#define SIC1_RSR_RTC_INT_BIT 20
#define SIC1_RSR_I2C_1_INT_MASK 0x80000
#define SIC1_RSR_I2C_1_INT 0x80000
#define SIC1_RSR_I2C_1_INT_BIT 19
#define SIC1_RSR_I2C_2_INT_MASK 0x40000
#define SIC1_RSR_I2C_2_INT 0x40000
#define SIC1_RSR_I2C_2_INT_BIT 18
#define SIC1_RSR_PLL397_INT_MASK 0x20000
#define SIC1_RSR_PLL397_INT 0x20000
#define SIC1_RSR_PLL397_INT_BIT 17
#define SIC1_RSR_PLLHCLK_INT_MASK 0x4000
#define SIC1_RSR_PLLHCLK_INT 0x4000
#define SIC1_RSR_PLLHCLK_INT_BIT 14
#define SIC1_RSR_PLLUSB_INT_MASK 0x2000
#define SIC1_RSR_PLLUSB_INT 0x2000
#define SIC1_RSR_PLLUSB_INT_BIT 13
#define SIC1_RSR_SPI2_INT_MASK 0x1000
#define SIC1_RSR_SPI2_INT 0x1000
#define SIC1_RSR_SPI2_INT_BIT 12
#define SIC1_RSR_TS_AUX_MASK 0x100
#define SIC1_RSR_TS_AUX 0x100
#define SIC1_RSR_TS_AUX_BIT 8
#define SIC1_RSR_ADC_INT_MASK 0x80
#define SIC1_RSR_ADC_INT 0x80
#define SIC1_RSR_ADC_INT_BIT 7
#define SIC1_RSR_TS_P_MASK 0x40
#define SIC1_RSR_TS_P 0x40
#define SIC1_RSR_TS_P_BIT 6
#define SIC1_RSR_GPI_28_MASK 0x20
#define SIC1_RSR_GPI_28 0x20
#define SIC1_RSR_GPI_28_BIT 5
#define SIC1_RSR_JTAG_COMM_RX_MASK 0x4
#define SIC1_RSR_JTAG_COMM_RX 0x4
#define SIC1_RSR_JTAG_COMM_RX_BIT 2
#define SIC1_RSR_JTAG_COMM_TX_MASK 0x2
#define SIC1_RSR_JTAG_COMM_TX 0x2
#define SIC1_RSR_JTAG_COMM_TX_BIT 1

#define SIC1_SR (*(volatile unsigned *)0x4000C008)
#define SIC1_SR_USB_i2c_int_MASK 0x80000000
#define SIC1_SR_USB_i2c_int 0x80000000
#define SIC1_SR_USB_i2c_int_BIT 31
#define SIC1_SR_USB_dev_hp_int_MASK 0x40000000
#define SIC1_SR_USB_dev_hp_int 0x40000000
#define SIC1_SR_USB_dev_hp_int_BIT 30
#define SIC1_SR_USB_dev_lp_int_MASK 0x20000000
#define SIC1_SR_USB_dev_lp_int 0x20000000
#define SIC1_SR_USB_dev_lp_int_BIT 29
#define SIC1_SR_USB_dev_dma_int_MASK 0x10000000
#define SIC1_SR_USB_dev_dma_int 0x10000000
#define SIC1_SR_USB_dev_dma_int_BIT 28
#define SIC1_SR_USB_host_int_MASK 0x8000000
#define SIC1_SR_USB_host_int 0x8000000
#define SIC1_SR_USB_host_int_BIT 27
#define SIC1_SR_USB_otg_atx_int_n_MASK 0x4000000
#define SIC1_SR_USB_otg_atx_int_n 0x4000000
#define SIC1_SR_USB_otg_atx_int_n_BIT 26
#define SIC1_SR_USB_otg_timer_int_MASK 0x2000000
#define SIC1_SR_USB_otg_timer_int 0x2000000
#define SIC1_SR_USB_otg_timer_int_BIT 25
#define SIC1_SR_SW_INT_MASK 0x1000000
#define SIC1_SR_SW_INT 0x1000000
#define SIC1_SR_SW_INT_BIT 24
#define SIC1_SR_SPI1_INT_MASK 0x800000
#define SIC1_SR_SPI1_INT 0x800000
#define SIC1_SR_SPI1_INT_BIT 23
#define SIC1_SR_KEY_IRQ_MASK 0x400000
#define SIC1_SR_KEY_IRQ 0x400000
#define SIC1_SR_KEY_IRQ_BIT 22
#define SIC1_SR_RTC_INT_MASK 0x100000
#define SIC1_SR_RTC_INT 0x100000
#define SIC1_SR_RTC_INT_BIT 20
#define SIC1_SR_I2C_1_INT_MASK 0x80000
#define SIC1_SR_I2C_1_INT 0x80000
#define SIC1_SR_I2C_1_INT_BIT 19
#define SIC1_SR_I2C_2_INT_MASK 0x40000
#define SIC1_SR_I2C_2_INT 0x40000
#define SIC1_SR_I2C_2_INT_BIT 18
#define SIC1_SR_PLL397_INT_MASK 0x20000
#define SIC1_SR_PLL397_INT 0x20000
#define SIC1_SR_PLL397_INT_BIT 17
#define SIC1_SR_PLLHCLK_INT_MASK 0x4000
#define SIC1_SR_PLLHCLK_INT 0x4000
#define SIC1_SR_PLLHCLK_INT_BIT 14
#define SIC1_SR_PLLUSB_INT_MASK 0x2000
#define SIC1_SR_PLLUSB_INT 0x2000
#define SIC1_SR_PLLUSB_INT_BIT 13
#define SIC1_SR_SPI2_INT_MASK 0x1000
#define SIC1_SR_SPI2_INT 0x1000
#define SIC1_SR_SPI2_INT_BIT 12
#define SIC1_SR_TS_AUX_MASK 0x100
#define SIC1_SR_TS_AUX 0x100
#define SIC1_SR_TS_AUX_BIT 8
#define SIC1_SR_ADC_INT_MASK 0x80
#define SIC1_SR_ADC_INT 0x80
#define SIC1_SR_ADC_INT_BIT 7
#define SIC1_SR_TS_P_MASK 0x40
#define SIC1_SR_TS_P 0x40
#define SIC1_SR_TS_P_BIT 6
#define SIC1_SR_GPI_28_MASK 0x20
#define SIC1_SR_GPI_28 0x20
#define SIC1_SR_GPI_28_BIT 5
#define SIC1_SR_JTAG_COMM_RX_MASK 0x4
#define SIC1_SR_JTAG_COMM_RX 0x4
#define SIC1_SR_JTAG_COMM_RX_BIT 2
#define SIC1_SR_JTAG_COMM_TX_MASK 0x2
#define SIC1_SR_JTAG_COMM_TX 0x2
#define SIC1_SR_JTAG_COMM_TX_BIT 1

#define SIC1_APR (*(volatile unsigned *)0x4000C00C)
#define SIC1_APR_USB_i2c_int_MASK 0x80000000
#define SIC1_APR_USB_i2c_int 0x80000000
#define SIC1_APR_USB_i2c_int_BIT 31
#define SIC1_APR_USB_dev_hp_int_MASK 0x40000000
#define SIC1_APR_USB_dev_hp_int 0x40000000
#define SIC1_APR_USB_dev_hp_int_BIT 30
#define SIC1_APR_USB_dev_lp_int_MASK 0x20000000
#define SIC1_APR_USB_dev_lp_int 0x20000000
#define SIC1_APR_USB_dev_lp_int_BIT 29
#define SIC1_APR_USB_dev_dma_int_MASK 0x10000000
#define SIC1_APR_USB_dev_dma_int 0x10000000
#define SIC1_APR_USB_dev_dma_int_BIT 28
#define SIC1_APR_USB_host_int_MASK 0x8000000
#define SIC1_APR_USB_host_int 0x8000000
#define SIC1_APR_USB_host_int_BIT 27
#define SIC1_APR_USB_otg_atx_int_n_MASK 0x4000000
#define SIC1_APR_USB_otg_atx_int_n 0x4000000
#define SIC1_APR_USB_otg_atx_int_n_BIT 26
#define SIC1_APR_USB_otg_timer_int_MASK 0x2000000
#define SIC1_APR_USB_otg_timer_int 0x2000000
#define SIC1_APR_USB_otg_timer_int_BIT 25
#define SIC1_APR_SW_INT_MASK 0x1000000
#define SIC1_APR_SW_INT 0x1000000
#define SIC1_APR_SW_INT_BIT 24
#define SIC1_APR_SPI1_INT_MASK 0x800000
#define SIC1_APR_SPI1_INT 0x800000
#define SIC1_APR_SPI1_INT_BIT 23
#define SIC1_APR_KEY_IRQ_MASK 0x400000
#define SIC1_APR_KEY_IRQ 0x400000
#define SIC1_APR_KEY_IRQ_BIT 22
#define SIC1_APR_RTC_INT_MASK 0x100000
#define SIC1_APR_RTC_INT 0x100000
#define SIC1_APR_RTC_INT_BIT 20
#define SIC1_APR_I2C_1_INT_MASK 0x80000
#define SIC1_APR_I2C_1_INT 0x80000
#define SIC1_APR_I2C_1_INT_BIT 19
#define SIC1_APR_I2C_2_INT_MASK 0x40000
#define SIC1_APR_I2C_2_INT 0x40000
#define SIC1_APR_I2C_2_INT_BIT 18
#define SIC1_APR_PLL397_INT_MASK 0x20000
#define SIC1_APR_PLL397_INT 0x20000
#define SIC1_APR_PLL397_INT_BIT 17
#define SIC1_APR_PLLHCLK_INT_MASK 0x4000
#define SIC1_APR_PLLHCLK_INT 0x4000
#define SIC1_APR_PLLHCLK_INT_BIT 14
#define SIC1_APR_PLLUSB_INT_MASK 0x2000
#define SIC1_APR_PLLUSB_INT 0x2000
#define SIC1_APR_PLLUSB_INT_BIT 13
#define SIC1_APR_SPI2_INT_MASK 0x1000
#define SIC1_APR_SPI2_INT 0x1000
#define SIC1_APR_SPI2_INT_BIT 12
#define SIC1_APR_TS_AUX_MASK 0x100
#define SIC1_APR_TS_AUX 0x100
#define SIC1_APR_TS_AUX_BIT 8
#define SIC1_APR_ADC_INT_MASK 0x80
#define SIC1_APR_ADC_INT 0x80
#define SIC1_APR_ADC_INT_BIT 7
#define SIC1_APR_TS_P_MASK 0x40
#define SIC1_APR_TS_P 0x40
#define SIC1_APR_TS_P_BIT 6
#define SIC1_APR_GPI_28_MASK 0x20
#define SIC1_APR_GPI_28 0x20
#define SIC1_APR_GPI_28_BIT 5
#define SIC1_APR_JTAG_COMM_RX_MASK 0x4
#define SIC1_APR_JTAG_COMM_RX 0x4
#define SIC1_APR_JTAG_COMM_RX_BIT 2
#define SIC1_APR_JTAG_COMM_TX_MASK 0x2
#define SIC1_APR_JTAG_COMM_TX 0x2
#define SIC1_APR_JTAG_COMM_TX_BIT 1

#define SIC1_ATR (*(volatile unsigned *)0x4000C010)
#define SIC1_ATR_USB_i2c_int_MASK 0x80000000
#define SIC1_ATR_USB_i2c_int 0x80000000
#define SIC1_ATR_USB_i2c_int_BIT 31
#define SIC1_ATR_USB_dev_hp_int_MASK 0x40000000
#define SIC1_ATR_USB_dev_hp_int 0x40000000
#define SIC1_ATR_USB_dev_hp_int_BIT 30
#define SIC1_ATR_USB_dev_lp_int_MASK 0x20000000
#define SIC1_ATR_USB_dev_lp_int 0x20000000
#define SIC1_ATR_USB_dev_lp_int_BIT 29
#define SIC1_ATR_USB_dev_dma_int_MASK 0x10000000
#define SIC1_ATR_USB_dev_dma_int 0x10000000
#define SIC1_ATR_USB_dev_dma_int_BIT 28
#define SIC1_ATR_USB_host_int_MASK 0x8000000
#define SIC1_ATR_USB_host_int 0x8000000
#define SIC1_ATR_USB_host_int_BIT 27
#define SIC1_ATR_USB_otg_atx_int_n_MASK 0x4000000
#define SIC1_ATR_USB_otg_atx_int_n 0x4000000
#define SIC1_ATR_USB_otg_atx_int_n_BIT 26
#define SIC1_ATR_USB_otg_timer_int_MASK 0x2000000
#define SIC1_ATR_USB_otg_timer_int 0x2000000
#define SIC1_ATR_USB_otg_timer_int_BIT 25
#define SIC1_ATR_SW_INT_MASK 0x1000000
#define SIC1_ATR_SW_INT 0x1000000
#define SIC1_ATR_SW_INT_BIT 24
#define SIC1_ATR_SPI1_INT_MASK 0x800000
#define SIC1_ATR_SPI1_INT 0x800000
#define SIC1_ATR_SPI1_INT_BIT 23
#define SIC1_ATR_KEY_IRQ_MASK 0x400000
#define SIC1_ATR_KEY_IRQ 0x400000
#define SIC1_ATR_KEY_IRQ_BIT 22
#define SIC1_ATR_RTC_INT_MASK 0x100000
#define SIC1_ATR_RTC_INT 0x100000
#define SIC1_ATR_RTC_INT_BIT 20
#define SIC1_ATR_I2C_1_INT_MASK 0x80000
#define SIC1_ATR_I2C_1_INT 0x80000
#define SIC1_ATR_I2C_1_INT_BIT 19
#define SIC1_ATR_I2C_2_INT_MASK 0x40000
#define SIC1_ATR_I2C_2_INT 0x40000
#define SIC1_ATR_I2C_2_INT_BIT 18
#define SIC1_ATR_PLL397_INT_MASK 0x20000
#define SIC1_ATR_PLL397_INT 0x20000
#define SIC1_ATR_PLL397_INT_BIT 17
#define SIC1_ATR_PLLHCLK_INT_MASK 0x4000
#define SIC1_ATR_PLLHCLK_INT 0x4000
#define SIC1_ATR_PLLHCLK_INT_BIT 14
#define SIC1_ATR_PLLUSB_INT_MASK 0x2000
#define SIC1_ATR_PLLUSB_INT 0x2000
#define SIC1_ATR_PLLUSB_INT_BIT 13
#define SIC1_ATR_SPI2_INT_MASK 0x1000
#define SIC1_ATR_SPI2_INT 0x1000
#define SIC1_ATR_SPI2_INT_BIT 12
#define SIC1_ATR_TS_AUX_MASK 0x100
#define SIC1_ATR_TS_AUX 0x100
#define SIC1_ATR_TS_AUX_BIT 8
#define SIC1_ATR_ADC_INT_MASK 0x80
#define SIC1_ATR_ADC_INT 0x80
#define SIC1_ATR_ADC_INT_BIT 7
#define SIC1_ATR_TS_P_MASK 0x40
#define SIC1_ATR_TS_P 0x40
#define SIC1_ATR_TS_P_BIT 6
#define SIC1_ATR_GPI_28_MASK 0x20
#define SIC1_ATR_GPI_28 0x20
#define SIC1_ATR_GPI_28_BIT 5
#define SIC1_ATR_JTAG_COMM_RX_MASK 0x4
#define SIC1_ATR_JTAG_COMM_RX 0x4
#define SIC1_ATR_JTAG_COMM_RX_BIT 2
#define SIC1_ATR_JTAG_COMM_TX_MASK 0x2
#define SIC1_ATR_JTAG_COMM_TX 0x2
#define SIC1_ATR_JTAG_COMM_TX_BIT 1

#define SIC1_ITR (*(volatile unsigned *)0x4000C014)
#define SIC1_ITR_USB_i2c_int_MASK 0x80000000
#define SIC1_ITR_USB_i2c_int 0x80000000
#define SIC1_ITR_USB_i2c_int_BIT 31
#define SIC1_ITR_USB_dev_hp_int_MASK 0x40000000
#define SIC1_ITR_USB_dev_hp_int 0x40000000
#define SIC1_ITR_USB_dev_hp_int_BIT 30
#define SIC1_ITR_USB_dev_lp_int_MASK 0x20000000
#define SIC1_ITR_USB_dev_lp_int 0x20000000
#define SIC1_ITR_USB_dev_lp_int_BIT 29
#define SIC1_ITR_USB_dev_dma_int_MASK 0x10000000
#define SIC1_ITR_USB_dev_dma_int 0x10000000
#define SIC1_ITR_USB_dev_dma_int_BIT 28
#define SIC1_ITR_USB_host_int_MASK 0x8000000
#define SIC1_ITR_USB_host_int 0x8000000
#define SIC1_ITR_USB_host_int_BIT 27
#define SIC1_ITR_USB_otg_atx_int_n_MASK 0x4000000
#define SIC1_ITR_USB_otg_atx_int_n 0x4000000
#define SIC1_ITR_USB_otg_atx_int_n_BIT 26
#define SIC1_ITR_USB_otg_timer_int_MASK 0x2000000
#define SIC1_ITR_USB_otg_timer_int 0x2000000
#define SIC1_ITR_USB_otg_timer_int_BIT 25
#define SIC1_ITR_SW_INT_MASK 0x1000000
#define SIC1_ITR_SW_INT 0x1000000
#define SIC1_ITR_SW_INT_BIT 24
#define SIC1_ITR_SPI1_INT_MASK 0x800000
#define SIC1_ITR_SPI1_INT 0x800000
#define SIC1_ITR_SPI1_INT_BIT 23
#define SIC1_ITR_KEY_IRQ_MASK 0x400000
#define SIC1_ITR_KEY_IRQ 0x400000
#define SIC1_ITR_KEY_IRQ_BIT 22
#define SIC1_ITR_RTC_INT_MASK 0x100000
#define SIC1_ITR_RTC_INT 0x100000
#define SIC1_ITR_RTC_INT_BIT 20
#define SIC1_ITR_I2C_1_INT_MASK 0x80000
#define SIC1_ITR_I2C_1_INT 0x80000
#define SIC1_ITR_I2C_1_INT_BIT 19
#define SIC1_ITR_I2C_2_INT_MASK 0x40000
#define SIC1_ITR_I2C_2_INT 0x40000
#define SIC1_ITR_I2C_2_INT_BIT 18
#define SIC1_ITR_PLL397_INT_MASK 0x20000
#define SIC1_ITR_PLL397_INT 0x20000
#define SIC1_ITR_PLL397_INT_BIT 17
#define SIC1_ITR_PLLHCLK_INT_MASK 0x4000
#define SIC1_ITR_PLLHCLK_INT 0x4000
#define SIC1_ITR_PLLHCLK_INT_BIT 14
#define SIC1_ITR_PLLUSB_INT_MASK 0x2000
#define SIC1_ITR_PLLUSB_INT 0x2000
#define SIC1_ITR_PLLUSB_INT_BIT 13
#define SIC1_ITR_SPI2_INT_MASK 0x1000
#define SIC1_ITR_SPI2_INT 0x1000
#define SIC1_ITR_SPI2_INT_BIT 12
#define SIC1_ITR_TS_AUX_MASK 0x100
#define SIC1_ITR_TS_AUX 0x100
#define SIC1_ITR_TS_AUX_BIT 8
#define SIC1_ITR_ADC_INT_MASK 0x80
#define SIC1_ITR_ADC_INT 0x80
#define SIC1_ITR_ADC_INT_BIT 7
#define SIC1_ITR_TS_P_MASK 0x40
#define SIC1_ITR_TS_P 0x40
#define SIC1_ITR_TS_P_BIT 6
#define SIC1_ITR_GPI_28_MASK 0x20
#define SIC1_ITR_GPI_28 0x20
#define SIC1_ITR_GPI_28_BIT 5
#define SIC1_ITR_JTAG_COMM_RX_MASK 0x4
#define SIC1_ITR_JTAG_COMM_RX 0x4
#define SIC1_ITR_JTAG_COMM_RX_BIT 2
#define SIC1_ITR_JTAG_COMM_TX_MASK 0x2
#define SIC1_ITR_JTAG_COMM_TX 0x2
#define SIC1_ITR_JTAG_COMM_TX_BIT 1

#define SIC2_ER (*(volatile unsigned *)0x40010000)
#define SIC2_ER_SYSCLK_mux_MASK 0x80000000
#define SIC2_ER_SYSCLK_mux 0x80000000
#define SIC2_ER_SYSCLK_mux_BIT 31
#define SIC2_ER_GPI_06_MASK 0x10000000
#define SIC2_ER_GPI_06 0x10000000
#define SIC2_ER_GPI_06_BIT 28
#define SIC2_ER_GPI_05_MASK 0x8000000
#define SIC2_ER_GPI_05 0x8000000
#define SIC2_ER_GPI_05_BIT 27
#define SIC2_ER_GPI_04_MASK 0x4000000
#define SIC2_ER_GPI_04 0x4000000
#define SIC2_ER_GPI_04_BIT 26
#define SIC2_ER_GPI_03_MASK 0x2000000
#define SIC2_ER_GPI_03 0x2000000
#define SIC2_ER_GPI_03_BIT 25
#define SIC2_ER_GPI_02_MASK 0x1000000
#define SIC2_ER_GPI_02 0x1000000
#define SIC2_ER_GPI_02_BIT 24
#define SIC2_ER_GPI_01_MASK 0x800000
#define SIC2_ER_GPI_01 0x800000
#define SIC2_ER_GPI_01_BIT 23
#define SIC2_ER_GPI_00_MASK 0x400000
#define SIC2_ER_GPI_00 0x400000
#define SIC2_ER_GPI_00_BIT 22
#define SIC2_ER_SPI1_DATIN_MASK 0x100000
#define SIC2_ER_SPI1_DATIN 0x100000
#define SIC2_ER_SPI1_DATIN_BIT 20
#define SIC2_ER_U5_RX_MASK 0x80000
#define SIC2_ER_U5_RX 0x80000
#define SIC2_ER_U5_RX_BIT 19
#define SIC2_ER_SDIO_INT_N_MASK 0x40000
#define SIC2_ER_SDIO_INT_N 0x40000
#define SIC2_ER_SDIO_INT_N_BIT 18
#define SIC2_ER_GPI_07_MASK 0x8000
#define SIC2_ER_GPI_07 0x8000
#define SIC2_ER_GPI_07_BIT 15
#define SIC2_ER_U7_HCTS_MASK 0x1000
#define SIC2_ER_U7_HCTS 0x1000
#define SIC2_ER_U7_HCTS_BIT 12
#define SIC2_ER_GPI_19_MASK 0x800
#define SIC2_ER_GPI_19 0x800
#define SIC2_ER_GPI_19_BIT 11
#define SIC2_ER_GPI_09_MASK 0x400
#define SIC2_ER_GPI_09 0x400
#define SIC2_ER_GPI_09_BIT 10
#define SIC2_ER_GPI_08_MASK 0x200
#define SIC2_ER_GPI_08 0x200
#define SIC2_ER_GPI_08_BIT 9
#define SIC2_ER_Pn_GPIO_MASK 0x100
#define SIC2_ER_Pn_GPIO 0x100
#define SIC2_ER_Pn_GPIO_BIT 8
#define SIC2_ER_U2_HCTS_MASK 0x80
#define SIC2_ER_U2_HCTS 0x80
#define SIC2_ER_U2_HCTS_BIT 7
#define SIC2_ER_SPI2_DATIN_MASK 0x40
#define SIC2_ER_SPI2_DATIN 0x40
#define SIC2_ER_SPI2_DATIN_BIT 6
#define SIC2_ER_GPIO_05_MASK 0x20
#define SIC2_ER_GPIO_05 0x20
#define SIC2_ER_GPIO_05_BIT 5
#define SIC2_ER_GPIO_04_MASK 0x10
#define SIC2_ER_GPIO_04 0x10
#define SIC2_ER_GPIO_04_BIT 4
#define SIC2_ER_GPIO_03_MASK 0x8
#define SIC2_ER_GPIO_03 0x8
#define SIC2_ER_GPIO_03_BIT 3
#define SIC2_ER_GPIO_02_MASK 0x4
#define SIC2_ER_GPIO_02 0x4
#define SIC2_ER_GPIO_02_BIT 2
#define SIC2_ER_GPIO_01_MASK 0x2
#define SIC2_ER_GPIO_01 0x2
#define SIC2_ER_GPIO_01_BIT 1
#define SIC2_ER_GPIO_00_MASK 0x1
#define SIC2_ER_GPIO_00 0x1
#define SIC2_ER_GPIO_00_BIT 0

#define SIC2_RSR (*(volatile unsigned *)0x40010004)
#define SIC2_RSR_SYSCLK_mux_MASK 0x80000000
#define SIC2_RSR_SYSCLK_mux 0x80000000
#define SIC2_RSR_SYSCLK_mux_BIT 31
#define SIC2_RSR_GPI_06_MASK 0x10000000
#define SIC2_RSR_GPI_06 0x10000000
#define SIC2_RSR_GPI_06_BIT 28
#define SIC2_RSR_GPI_05_MASK 0x8000000
#define SIC2_RSR_GPI_05 0x8000000
#define SIC2_RSR_GPI_05_BIT 27
#define SIC2_RSR_GPI_04_MASK 0x4000000
#define SIC2_RSR_GPI_04 0x4000000
#define SIC2_RSR_GPI_04_BIT 26
#define SIC2_RSR_GPI_03_MASK 0x2000000
#define SIC2_RSR_GPI_03 0x2000000
#define SIC2_RSR_GPI_03_BIT 25
#define SIC2_RSR_GPI_02_MASK 0x1000000
#define SIC2_RSR_GPI_02 0x1000000
#define SIC2_RSR_GPI_02_BIT 24
#define SIC2_RSR_GPI_01_MASK 0x800000
#define SIC2_RSR_GPI_01 0x800000
#define SIC2_RSR_GPI_01_BIT 23
#define SIC2_RSR_GPI_00_MASK 0x400000
#define SIC2_RSR_GPI_00 0x400000
#define SIC2_RSR_GPI_00_BIT 22
#define SIC2_RSR_SPI1_DATIN_MASK 0x100000
#define SIC2_RSR_SPI1_DATIN 0x100000
#define SIC2_RSR_SPI1_DATIN_BIT 20
#define SIC2_RSR_U5_RX_MASK 0x80000
#define SIC2_RSR_U5_RX 0x80000
#define SIC2_RSR_U5_RX_BIT 19
#define SIC2_RSR_SDIO_INT_N_MASK 0x40000
#define SIC2_RSR_SDIO_INT_N 0x40000
#define SIC2_RSR_SDIO_INT_N_BIT 18
#define SIC2_RSR_GPI_07_MASK 0x8000
#define SIC2_RSR_GPI_07 0x8000
#define SIC2_RSR_GPI_07_BIT 15
#define SIC2_RSR_U7_HCTS_MASK 0x1000
#define SIC2_RSR_U7_HCTS 0x1000
#define SIC2_RSR_U7_HCTS_BIT 12
#define SIC2_RSR_GPI_19_MASK 0x800
#define SIC2_RSR_GPI_19 0x800
#define SIC2_RSR_GPI_19_BIT 11
#define SIC2_RSR_GPI_09_MASK 0x400
#define SIC2_RSR_GPI_09 0x400
#define SIC2_RSR_GPI_09_BIT 10
#define SIC2_RSR_GPI_08_MASK 0x200
#define SIC2_RSR_GPI_08 0x200
#define SIC2_RSR_GPI_08_BIT 9
#define SIC2_RSR_Pn_GPIO_MASK 0x100
#define SIC2_RSR_Pn_GPIO 0x100
#define SIC2_RSR_Pn_GPIO_BIT 8
#define SIC2_RSR_U2_HCTS_MASK 0x80
#define SIC2_RSR_U2_HCTS 0x80
#define SIC2_RSR_U2_HCTS_BIT 7
#define SIC2_RSR_SPI2_DATIN_MASK 0x40
#define SIC2_RSR_SPI2_DATIN 0x40
#define SIC2_RSR_SPI2_DATIN_BIT 6
#define SIC2_RSR_GPIO_05_MASK 0x20
#define SIC2_RSR_GPIO_05 0x20
#define SIC2_RSR_GPIO_05_BIT 5
#define SIC2_RSR_GPIO_04_MASK 0x10
#define SIC2_RSR_GPIO_04 0x10
#define SIC2_RSR_GPIO_04_BIT 4
#define SIC2_RSR_GPIO_03_MASK 0x8
#define SIC2_RSR_GPIO_03 0x8
#define SIC2_RSR_GPIO_03_BIT 3
#define SIC2_RSR_GPIO_02_MASK 0x4
#define SIC2_RSR_GPIO_02 0x4
#define SIC2_RSR_GPIO_02_BIT 2
#define SIC2_RSR_GPIO_01_MASK 0x2
#define SIC2_RSR_GPIO_01 0x2
#define SIC2_RSR_GPIO_01_BIT 1
#define SIC2_RSR_GPIO_00_MASK 0x1
#define SIC2_RSR_GPIO_00 0x1
#define SIC2_RSR_GPIO_00_BIT 0

#define SIC2_SR (*(volatile unsigned *)0x40010008)
#define SIC2_SR_SYSCLK_mux_MASK 0x80000000
#define SIC2_SR_SYSCLK_mux 0x80000000
#define SIC2_SR_SYSCLK_mux_BIT 31
#define SIC2_SR_GPI_06_MASK 0x10000000
#define SIC2_SR_GPI_06 0x10000000
#define SIC2_SR_GPI_06_BIT 28
#define SIC2_SR_GPI_05_MASK 0x8000000
#define SIC2_SR_GPI_05 0x8000000
#define SIC2_SR_GPI_05_BIT 27
#define SIC2_SR_GPI_04_MASK 0x4000000
#define SIC2_SR_GPI_04 0x4000000
#define SIC2_SR_GPI_04_BIT 26
#define SIC2_SR_GPI_03_MASK 0x2000000
#define SIC2_SR_GPI_03 0x2000000
#define SIC2_SR_GPI_03_BIT 25
#define SIC2_SR_GPI_02_MASK 0x1000000
#define SIC2_SR_GPI_02 0x1000000
#define SIC2_SR_GPI_02_BIT 24
#define SIC2_SR_GPI_01_MASK 0x800000
#define SIC2_SR_GPI_01 0x800000
#define SIC2_SR_GPI_01_BIT 23
#define SIC2_SR_GPI_00_MASK 0x400000
#define SIC2_SR_GPI_00 0x400000
#define SIC2_SR_GPI_00_BIT 22
#define SIC2_SR_SPI1_DATIN_MASK 0x100000
#define SIC2_SR_SPI1_DATIN 0x100000
#define SIC2_SR_SPI1_DATIN_BIT 20
#define SIC2_SR_U5_RX_MASK 0x80000
#define SIC2_SR_U5_RX 0x80000
#define SIC2_SR_U5_RX_BIT 19
#define SIC2_SR_SDIO_INT_N_MASK 0x40000
#define SIC2_SR_SDIO_INT_N 0x40000
#define SIC2_SR_SDIO_INT_N_BIT 18
#define SIC2_SR_GPI_07_MASK 0x8000
#define SIC2_SR_GPI_07 0x8000
#define SIC2_SR_GPI_07_BIT 15
#define SIC2_SR_U7_HCTS_MASK 0x1000
#define SIC2_SR_U7_HCTS 0x1000
#define SIC2_SR_U7_HCTS_BIT 12
#define SIC2_SR_GPI_19_MASK 0x800
#define SIC2_SR_GPI_19 0x800
#define SIC2_SR_GPI_19_BIT 11
#define SIC2_SR_GPI_09_MASK 0x400
#define SIC2_SR_GPI_09 0x400
#define SIC2_SR_GPI_09_BIT 10
#define SIC2_SR_GPI_08_MASK 0x200
#define SIC2_SR_GPI_08 0x200
#define SIC2_SR_GPI_08_BIT 9
#define SIC2_SR_Pn_GPIO_MASK 0x100
#define SIC2_SR_Pn_GPIO 0x100
#define SIC2_SR_Pn_GPIO_BIT 8
#define SIC2_SR_U2_HCTS_MASK 0x80
#define SIC2_SR_U2_HCTS 0x80
#define SIC2_SR_U2_HCTS_BIT 7
#define SIC2_SR_SPI2_DATIN_MASK 0x40
#define SIC2_SR_SPI2_DATIN 0x40
#define SIC2_SR_SPI2_DATIN_BIT 6
#define SIC2_SR_GPIO_05_MASK 0x20
#define SIC2_SR_GPIO_05 0x20
#define SIC2_SR_GPIO_05_BIT 5
#define SIC2_SR_GPIO_04_MASK 0x10
#define SIC2_SR_GPIO_04 0x10
#define SIC2_SR_GPIO_04_BIT 4
#define SIC2_SR_GPIO_03_MASK 0x8
#define SIC2_SR_GPIO_03 0x8
#define SIC2_SR_GPIO_03_BIT 3
#define SIC2_SR_GPIO_02_MASK 0x4
#define SIC2_SR_GPIO_02 0x4
#define SIC2_SR_GPIO_02_BIT 2
#define SIC2_SR_GPIO_01_MASK 0x2
#define SIC2_SR_GPIO_01 0x2
#define SIC2_SR_GPIO_01_BIT 1
#define SIC2_SR_GPIO_00_MASK 0x1
#define SIC2_SR_GPIO_00 0x1
#define SIC2_SR_GPIO_00_BIT 0

#define SIC2_APR (*(volatile unsigned *)0x4001000C)
#define SIC2_APR_SYSCLK_mux_MASK 0x80000000
#define SIC2_APR_SYSCLK_mux 0x80000000
#define SIC2_APR_SYSCLK_mux_BIT 31
#define SIC2_APR_GPI_06_MASK 0x10000000
#define SIC2_APR_GPI_06 0x10000000
#define SIC2_APR_GPI_06_BIT 28
#define SIC2_APR_GPI_05_MASK 0x8000000
#define SIC2_APR_GPI_05 0x8000000
#define SIC2_APR_GPI_05_BIT 27
#define SIC2_APR_GPI_04_MASK 0x4000000
#define SIC2_APR_GPI_04 0x4000000
#define SIC2_APR_GPI_04_BIT 26
#define SIC2_APR_GPI_03_MASK 0x2000000
#define SIC2_APR_GPI_03 0x2000000
#define SIC2_APR_GPI_03_BIT 25
#define SIC2_APR_GPI_02_MASK 0x1000000
#define SIC2_APR_GPI_02 0x1000000
#define SIC2_APR_GPI_02_BIT 24
#define SIC2_APR_GPI_01_MASK 0x800000
#define SIC2_APR_GPI_01 0x800000
#define SIC2_APR_GPI_01_BIT 23
#define SIC2_APR_GPI_00_MASK 0x400000
#define SIC2_APR_GPI_00 0x400000
#define SIC2_APR_GPI_00_BIT 22
#define SIC2_APR_SPI1_DATIN_MASK 0x100000
#define SIC2_APR_SPI1_DATIN 0x100000
#define SIC2_APR_SPI1_DATIN_BIT 20
#define SIC2_APR_U5_RX_MASK 0x80000
#define SIC2_APR_U5_RX 0x80000
#define SIC2_APR_U5_RX_BIT 19
#define SIC2_APR_SDIO_INT_N_MASK 0x40000
#define SIC2_APR_SDIO_INT_N 0x40000
#define SIC2_APR_SDIO_INT_N_BIT 18
#define SIC2_APR_GPI_07_MASK 0x8000
#define SIC2_APR_GPI_07 0x8000
#define SIC2_APR_GPI_07_BIT 15
#define SIC2_APR_U7_HCTS_MASK 0x1000
#define SIC2_APR_U7_HCTS 0x1000
#define SIC2_APR_U7_HCTS_BIT 12
#define SIC2_APR_GPI_19_MASK 0x800
#define SIC2_APR_GPI_19 0x800
#define SIC2_APR_GPI_19_BIT 11
#define SIC2_APR_GPI_09_MASK 0x400
#define SIC2_APR_GPI_09 0x400
#define SIC2_APR_GPI_09_BIT 10
#define SIC2_APR_GPI_08_MASK 0x200
#define SIC2_APR_GPI_08 0x200
#define SIC2_APR_GPI_08_BIT 9
#define SIC2_APR_Pn_GPIO_MASK 0x100
#define SIC2_APR_Pn_GPIO 0x100
#define SIC2_APR_Pn_GPIO_BIT 8
#define SIC2_APR_U2_HCTS_MASK 0x80
#define SIC2_APR_U2_HCTS 0x80
#define SIC2_APR_U2_HCTS_BIT 7
#define SIC2_APR_SPI2_DATIN_MASK 0x40
#define SIC2_APR_SPI2_DATIN 0x40
#define SIC2_APR_SPI2_DATIN_BIT 6
#define SIC2_APR_GPIO_05_MASK 0x20
#define SIC2_APR_GPIO_05 0x20
#define SIC2_APR_GPIO_05_BIT 5
#define SIC2_APR_GPIO_04_MASK 0x10
#define SIC2_APR_GPIO_04 0x10
#define SIC2_APR_GPIO_04_BIT 4
#define SIC2_APR_GPIO_03_MASK 0x8
#define SIC2_APR_GPIO_03 0x8
#define SIC2_APR_GPIO_03_BIT 3
#define SIC2_APR_GPIO_02_MASK 0x4
#define SIC2_APR_GPIO_02 0x4
#define SIC2_APR_GPIO_02_BIT 2
#define SIC2_APR_GPIO_01_MASK 0x2
#define SIC2_APR_GPIO_01 0x2
#define SIC2_APR_GPIO_01_BIT 1
#define SIC2_APR_GPIO_00_MASK 0x1
#define SIC2_APR_GPIO_00 0x1
#define SIC2_APR_GPIO_00_BIT 0

#define SIC2_ATR (*(volatile unsigned *)0x40010010)
#define SIC2_ATR_SYSCLK_mux_MASK 0x80000000
#define SIC2_ATR_SYSCLK_mux 0x80000000
#define SIC2_ATR_SYSCLK_mux_BIT 31
#define SIC2_ATR_GPI_06_MASK 0x10000000
#define SIC2_ATR_GPI_06 0x10000000
#define SIC2_ATR_GPI_06_BIT 28
#define SIC2_ATR_GPI_05_MASK 0x8000000
#define SIC2_ATR_GPI_05 0x8000000
#define SIC2_ATR_GPI_05_BIT 27
#define SIC2_ATR_GPI_04_MASK 0x4000000
#define SIC2_ATR_GPI_04 0x4000000
#define SIC2_ATR_GPI_04_BIT 26
#define SIC2_ATR_GPI_03_MASK 0x2000000
#define SIC2_ATR_GPI_03 0x2000000
#define SIC2_ATR_GPI_03_BIT 25
#define SIC2_ATR_GPI_02_MASK 0x1000000
#define SIC2_ATR_GPI_02 0x1000000
#define SIC2_ATR_GPI_02_BIT 24
#define SIC2_ATR_GPI_01_MASK 0x800000
#define SIC2_ATR_GPI_01 0x800000
#define SIC2_ATR_GPI_01_BIT 23
#define SIC2_ATR_GPI_00_MASK 0x400000
#define SIC2_ATR_GPI_00 0x400000
#define SIC2_ATR_GPI_00_BIT 22
#define SIC2_ATR_SPI1_DATIN_MASK 0x100000
#define SIC2_ATR_SPI1_DATIN 0x100000
#define SIC2_ATR_SPI1_DATIN_BIT 20
#define SIC2_ATR_U5_RX_MASK 0x80000
#define SIC2_ATR_U5_RX 0x80000
#define SIC2_ATR_U5_RX_BIT 19
#define SIC2_ATR_SDIO_INT_N_MASK 0x40000
#define SIC2_ATR_SDIO_INT_N 0x40000
#define SIC2_ATR_SDIO_INT_N_BIT 18
#define SIC2_ATR_GPI_07_MASK 0x8000
#define SIC2_ATR_GPI_07 0x8000
#define SIC2_ATR_GPI_07_BIT 15
#define SIC2_ATR_U7_HCTS_MASK 0x1000
#define SIC2_ATR_U7_HCTS 0x1000
#define SIC2_ATR_U7_HCTS_BIT 12
#define SIC2_ATR_GPI_19_MASK 0x800
#define SIC2_ATR_GPI_19 0x800
#define SIC2_ATR_GPI_19_BIT 11
#define SIC2_ATR_GPI_09_MASK 0x400
#define SIC2_ATR_GPI_09 0x400
#define SIC2_ATR_GPI_09_BIT 10
#define SIC2_ATR_GPI_08_MASK 0x200
#define SIC2_ATR_GPI_08 0x200
#define SIC2_ATR_GPI_08_BIT 9
#define SIC2_ATR_Pn_GPIO_MASK 0x100
#define SIC2_ATR_Pn_GPIO 0x100
#define SIC2_ATR_Pn_GPIO_BIT 8
#define SIC2_ATR_U2_HCTS_MASK 0x80
#define SIC2_ATR_U2_HCTS 0x80
#define SIC2_ATR_U2_HCTS_BIT 7
#define SIC2_ATR_SPI2_DATIN_MASK 0x40
#define SIC2_ATR_SPI2_DATIN 0x40
#define SIC2_ATR_SPI2_DATIN_BIT 6
#define SIC2_ATR_GPIO_05_MASK 0x20
#define SIC2_ATR_GPIO_05 0x20
#define SIC2_ATR_GPIO_05_BIT 5
#define SIC2_ATR_GPIO_04_MASK 0x10
#define SIC2_ATR_GPIO_04 0x10
#define SIC2_ATR_GPIO_04_BIT 4
#define SIC2_ATR_GPIO_03_MASK 0x8
#define SIC2_ATR_GPIO_03 0x8
#define SIC2_ATR_GPIO_03_BIT 3
#define SIC2_ATR_GPIO_02_MASK 0x4
#define SIC2_ATR_GPIO_02 0x4
#define SIC2_ATR_GPIO_02_BIT 2
#define SIC2_ATR_GPIO_01_MASK 0x2
#define SIC2_ATR_GPIO_01 0x2
#define SIC2_ATR_GPIO_01_BIT 1
#define SIC2_ATR_GPIO_00_MASK 0x1
#define SIC2_ATR_GPIO_00 0x1
#define SIC2_ATR_GPIO_00_BIT 0

#define SIC2_ITR (*(volatile unsigned *)0x40010014)
#define SIC2_ITR_SYSCLK_mux_MASK 0x80000000
#define SIC2_ITR_SYSCLK_mux 0x80000000
#define SIC2_ITR_SYSCLK_mux_BIT 31
#define SIC2_ITR_GPI_06_MASK 0x10000000
#define SIC2_ITR_GPI_06 0x10000000
#define SIC2_ITR_GPI_06_BIT 28
#define SIC2_ITR_GPI_05_MASK 0x8000000
#define SIC2_ITR_GPI_05 0x8000000
#define SIC2_ITR_GPI_05_BIT 27
#define SIC2_ITR_GPI_04_MASK 0x4000000
#define SIC2_ITR_GPI_04 0x4000000
#define SIC2_ITR_GPI_04_BIT 26
#define SIC2_ITR_GPI_03_MASK 0x2000000
#define SIC2_ITR_GPI_03 0x2000000
#define SIC2_ITR_GPI_03_BIT 25
#define SIC2_ITR_GPI_02_MASK 0x1000000
#define SIC2_ITR_GPI_02 0x1000000
#define SIC2_ITR_GPI_02_BIT 24
#define SIC2_ITR_GPI_01_MASK 0x800000
#define SIC2_ITR_GPI_01 0x800000
#define SIC2_ITR_GPI_01_BIT 23
#define SIC2_ITR_GPI_00_MASK 0x400000
#define SIC2_ITR_GPI_00 0x400000
#define SIC2_ITR_GPI_00_BIT 22
#define SIC2_ITR_SPI1_DATIN_MASK 0x100000
#define SIC2_ITR_SPI1_DATIN 0x100000
#define SIC2_ITR_SPI1_DATIN_BIT 20
#define SIC2_ITR_U5_RX_MASK 0x80000
#define SIC2_ITR_U5_RX 0x80000
#define SIC2_ITR_U5_RX_BIT 19
#define SIC2_ITR_SDIO_INT_N_MASK 0x40000
#define SIC2_ITR_SDIO_INT_N 0x40000
#define SIC2_ITR_SDIO_INT_N_BIT 18
#define SIC2_ITR_GPI_07_MASK 0x8000
#define SIC2_ITR_GPI_07 0x8000
#define SIC2_ITR_GPI_07_BIT 15
#define SIC2_ITR_U7_HCTS_MASK 0x1000
#define SIC2_ITR_U7_HCTS 0x1000
#define SIC2_ITR_U7_HCTS_BIT 12
#define SIC2_ITR_GPI_19_MASK 0x800
#define SIC2_ITR_GPI_19 0x800
#define SIC2_ITR_GPI_19_BIT 11
#define SIC2_ITR_GPI_09_MASK 0x400
#define SIC2_ITR_GPI_09 0x400
#define SIC2_ITR_GPI_09_BIT 10
#define SIC2_ITR_GPI_08_MASK 0x200
#define SIC2_ITR_GPI_08 0x200
#define SIC2_ITR_GPI_08_BIT 9
#define SIC2_ITR_Pn_GPIO_MASK 0x100
#define SIC2_ITR_Pn_GPIO 0x100
#define SIC2_ITR_Pn_GPIO_BIT 8
#define SIC2_ITR_U2_HCTS_MASK 0x80
#define SIC2_ITR_U2_HCTS 0x80
#define SIC2_ITR_U2_HCTS_BIT 7
#define SIC2_ITR_SPI2_DATIN_MASK 0x40
#define SIC2_ITR_SPI2_DATIN 0x40
#define SIC2_ITR_SPI2_DATIN_BIT 6
#define SIC2_ITR_GPIO_05_MASK 0x20
#define SIC2_ITR_GPIO_05 0x20
#define SIC2_ITR_GPIO_05_BIT 5
#define SIC2_ITR_GPIO_04_MASK 0x10
#define SIC2_ITR_GPIO_04 0x10
#define SIC2_ITR_GPIO_04_BIT 4
#define SIC2_ITR_GPIO_03_MASK 0x8
#define SIC2_ITR_GPIO_03 0x8
#define SIC2_ITR_GPIO_03_BIT 3
#define SIC2_ITR_GPIO_02_MASK 0x4
#define SIC2_ITR_GPIO_02 0x4
#define SIC2_ITR_GPIO_02_BIT 2
#define SIC2_ITR_GPIO_01_MASK 0x2
#define SIC2_ITR_GPIO_01 0x2
#define SIC2_ITR_GPIO_01_BIT 1
#define SIC2_ITR_GPIO_00_MASK 0x1
#define SIC2_ITR_GPIO_00 0x1
#define SIC2_ITR_GPIO_00_BIT 0

#define HSU1_RX (*(volatile unsigned *)0x40014000)
#define HSU1_RX_HSU_BREAK_MASK 0x400
#define HSU1_RX_HSU_BREAK 0x400
#define HSU1_RX_HSU_BREAK_BIT 10
#define HSU1_RX_HSU_ERROR_MASK 0x200
#define HSU1_RX_HSU_ERROR 0x200
#define HSU1_RX_HSU_ERROR_BIT 9
#define HSU1_RX_HSU_RX_EMPTY_MASK 0x100
#define HSU1_RX_HSU_RX_EMPTY 0x100
#define HSU1_RX_HSU_RX_EMPTY_BIT 8
#define HSU1_RX_HSU_RX_DATA_MASK 0xFF
#define HSU1_RX_HSU_RX_DATA_BIT 0

#define HSU1_TX (*(volatile unsigned *)0x40014000)
#define HSU1_TX_HSU_TX_DATA_MASK 0xFF
#define HSU1_TX_HSU_TX_DATA_BIT 0

#define HSU1_LEVEL (*(volatile unsigned *)0x40014004)
#define HSU1_LEVEL_HSU_TX_LEV_MASK 0xFF00
#define HSU1_LEVEL_HSU_TX_LEV_BIT 8
#define HSU1_LEVEL_HSU_RX_LEV_MASK 0xFF
#define HSU1_LEVEL_HSU_RX_LEV_BIT 0

#define HSU1_IIR (*(volatile unsigned *)0x40014008)
#define HSU1_IIR_HSU_TX_INT_SET_MASK 0x40
#define HSU1_IIR_HSU_TX_INT_SET 0x40
#define HSU1_IIR_HSU_TX_INT_SET_BIT 6
#define HSU1_IIR_HSU_RX_OE_MASK 0x20
#define HSU1_IIR_HSU_RX_OE 0x20
#define HSU1_IIR_HSU_RX_OE_BIT 5
#define HSU1_IIR_HSU_BRK_MASK 0x10
#define HSU1_IIR_HSU_BRK 0x10
#define HSU1_IIR_HSU_BRK_BIT 4
#define HSU1_IIR_HSU_FE_MASK 0x8
#define HSU1_IIR_HSU_FE 0x8
#define HSU1_IIR_HSU_FE_BIT 3
#define HSU1_IIR_HSU_RX_TIMEOUT_MASK 0x4
#define HSU1_IIR_HSU_RX_TIMEOUT 0x4
#define HSU1_IIR_HSU_RX_TIMEOUT_BIT 2
#define HSU1_IIR_HSU_RX_TRIG_MASK 0x2
#define HSU1_IIR_HSU_RX_TRIG 0x2
#define HSU1_IIR_HSU_RX_TRIG_BIT 1
#define HSU1_IIR_HSU_TX_MASK 0x1
#define HSU1_IIR_HSU_TX 0x1
#define HSU1_IIR_HSU_TX_BIT 0

#define HSU1_CTRL (*(volatile unsigned *)0x4001400C)
#define HSU1_CTRL_HRTS_INV_MASK 0x200000
#define HSU1_CTRL_HRTS_INV 0x200000
#define HSU1_CTRL_HRTS_INV_BIT 21
#define HSU1_CTRL_HRTS_TRIG_MASK 0x180000
#define HSU1_CTRL_HRTS_TRIG_BIT 19
#define HSU1_CTRL_HRTS_EN_MASK 0x40000
#define HSU1_CTRL_HRTS_EN 0x40000
#define HSU1_CTRL_HRTS_EN_BIT 18
#define HSU1_CTRL_TMO_CONFIG_MASK 0x30000
#define HSU1_CTRL_TMO_CONFIG_BIT 16
#define HSU1_CTRL_HCTS_INV_MASK 0x8000
#define HSU1_CTRL_HCTS_INV 0x8000
#define HSU1_CTRL_HCTS_INV_BIT 15
#define HSU1_CTRL_HCTS_EN_MASK 0x4000
#define HSU1_CTRL_HCTS_EN 0x4000
#define HSU1_CTRL_HCTS_EN_BIT 14
#define HSU1_CTRL_HSU_OFFSET_MASK 0x3E00
#define HSU1_CTRL_HSU_OFFSET_BIT 9
#define HSU1_CTRL_HSU_BREAK_MASK 0x100
#define HSU1_CTRL_HSU_BREAK 0x100
#define HSU1_CTRL_HSU_BREAK_BIT 8
#define HSU1_CTRL_HSU_ERR_INT_EN_MASK 0x80
#define HSU1_CTRL_HSU_ERR_INT_EN 0x80
#define HSU1_CTRL_HSU_ERR_INT_EN_BIT 7
#define HSU1_CTRL_HSU_RX_INT_EN_MASK 0x40
#define HSU1_CTRL_HSU_RX_INT_EN 0x40
#define HSU1_CTRL_HSU_RX_INT_EN_BIT 6
#define HSU1_CTRL_HSU_TX_INT_EN_MASK 0x20
#define HSU1_CTRL_HSU_TX_INT_EN 0x20
#define HSU1_CTRL_HSU_TX_INT_EN_BIT 5
#define HSU1_CTRL_HSU_RX_TRIG_MASK 0x1C
#define HSU1_CTRL_HSU_RX_TRIG_BIT 2
#define HSU1_CTRL_HSU_TX_TRIG_MASK 0x3
#define HSU1_CTRL_HSU_TX_TRIG_BIT 0

#define HSU1_RATE (*(volatile unsigned *)0x40014010)
#define HSU1_RATE_HSU_RATE_MASK 0xFF
#define HSU1_RATE_HSU_RATE_BIT 0

#define HSU2_RX (*(volatile unsigned *)0x40018000)
#define HSU2_RX_HSU_BREAK_MASK 0x400
#define HSU2_RX_HSU_BREAK 0x400
#define HSU2_RX_HSU_BREAK_BIT 10
#define HSU2_RX_HSU_ERROR_MASK 0x200
#define HSU2_RX_HSU_ERROR 0x200
#define HSU2_RX_HSU_ERROR_BIT 9
#define HSU2_RX_HSU_RX_EMPTY_MASK 0x100
#define HSU2_RX_HSU_RX_EMPTY 0x100
#define HSU2_RX_HSU_RX_EMPTY_BIT 8
#define HSU2_RX_HSU_RX_DATA_MASK 0xFF
#define HSU2_RX_HSU_RX_DATA_BIT 0

#define HSU2_TX (*(volatile unsigned *)0x40018000)
#define HSU2_TX_HSU_TX_DATA_MASK 0xFF
#define HSU2_TX_HSU_TX_DATA_BIT 0

#define HSU2_LEVEL (*(volatile unsigned *)0x40018004)
#define HSU2_LEVEL_HSU_TX_LEV_MASK 0xFF00
#define HSU2_LEVEL_HSU_TX_LEV_BIT 8
#define HSU2_LEVEL_HSU_RX_LEV_MASK 0xFF
#define HSU2_LEVEL_HSU_RX_LEV_BIT 0

#define HSU2_IIR (*(volatile unsigned *)0x40018008)
#define HSU2_IIR_HSU_TX_INT_SET_MASK 0x40
#define HSU2_IIR_HSU_TX_INT_SET 0x40
#define HSU2_IIR_HSU_TX_INT_SET_BIT 6
#define HSU2_IIR_HSU_RX_OE_MASK 0x20
#define HSU2_IIR_HSU_RX_OE 0x20
#define HSU2_IIR_HSU_RX_OE_BIT 5
#define HSU2_IIR_HSU_BRK_MASK 0x10
#define HSU2_IIR_HSU_BRK 0x10
#define HSU2_IIR_HSU_BRK_BIT 4
#define HSU2_IIR_HSU_FE_MASK 0x8
#define HSU2_IIR_HSU_FE 0x8
#define HSU2_IIR_HSU_FE_BIT 3
#define HSU2_IIR_HSU_RX_TIMEOUT_MASK 0x4
#define HSU2_IIR_HSU_RX_TIMEOUT 0x4
#define HSU2_IIR_HSU_RX_TIMEOUT_BIT 2
#define HSU2_IIR_HSU_RX_TRIG_MASK 0x2
#define HSU2_IIR_HSU_RX_TRIG 0x2
#define HSU2_IIR_HSU_RX_TRIG_BIT 1
#define HSU2_IIR_HSU_TX_MASK 0x1
#define HSU2_IIR_HSU_TX 0x1
#define HSU2_IIR_HSU_TX_BIT 0

#define HSU2_CTRL (*(volatile unsigned *)0x4001800C)
#define HSU2_CTRL_HRTS_INV_MASK 0x200000
#define HSU2_CTRL_HRTS_INV 0x200000
#define HSU2_CTRL_HRTS_INV_BIT 21
#define HSU2_CTRL_HRTS_TRIG_MASK 0x180000
#define HSU2_CTRL_HRTS_TRIG_BIT 19
#define HSU2_CTRL_HRTS_EN_MASK 0x40000
#define HSU2_CTRL_HRTS_EN 0x40000
#define HSU2_CTRL_HRTS_EN_BIT 18
#define HSU2_CTRL_TMO_CONFIG_MASK 0x30000
#define HSU2_CTRL_TMO_CONFIG_BIT 16
#define HSU2_CTRL_HCTS_INV_MASK 0x8000
#define HSU2_CTRL_HCTS_INV 0x8000
#define HSU2_CTRL_HCTS_INV_BIT 15
#define HSU2_CTRL_HCTS_EN_MASK 0x4000
#define HSU2_CTRL_HCTS_EN 0x4000
#define HSU2_CTRL_HCTS_EN_BIT 14
#define HSU2_CTRL_HSU_OFFSET_MASK 0x3E00
#define HSU2_CTRL_HSU_OFFSET_BIT 9
#define HSU2_CTRL_HSU_BREAK_MASK 0x100
#define HSU2_CTRL_HSU_BREAK 0x100
#define HSU2_CTRL_HSU_BREAK_BIT 8
#define HSU2_CTRL_HSU_ERR_INT_EN_MASK 0x80
#define HSU2_CTRL_HSU_ERR_INT_EN 0x80
#define HSU2_CTRL_HSU_ERR_INT_EN_BIT 7
#define HSU2_CTRL_HSU_RX_INT_EN_MASK 0x40
#define HSU2_CTRL_HSU_RX_INT_EN 0x40
#define HSU2_CTRL_HSU_RX_INT_EN_BIT 6
#define HSU2_CTRL_HSU_TX_INT_EN_MASK 0x20
#define HSU2_CTRL_HSU_TX_INT_EN 0x20
#define HSU2_CTRL_HSU_TX_INT_EN_BIT 5
#define HSU2_CTRL_HSU_RX_TRIG_MASK 0x1C
#define HSU2_CTRL_HSU_RX_TRIG_BIT 2
#define HSU2_CTRL_HSU_TX_TRIG_MASK 0x3
#define HSU2_CTRL_HSU_TX_TRIG_BIT 0

#define HSU2_RATE (*(volatile unsigned *)0x40018010)
#define HSU2_RATE_HSU_RATE_MASK 0xFF
#define HSU2_RATE_HSU_RATE_BIT 0

#define HSU7_RX (*(volatile unsigned *)0x4001C000)
#define HSU7_RX_HSU_BREAK_MASK 0x400
#define HSU7_RX_HSU_BREAK 0x400
#define HSU7_RX_HSU_BREAK_BIT 10
#define HSU7_RX_HSU_ERROR_MASK 0x200
#define HSU7_RX_HSU_ERROR 0x200
#define HSU7_RX_HSU_ERROR_BIT 9
#define HSU7_RX_HSU_RX_EMPTY_MASK 0x100
#define HSU7_RX_HSU_RX_EMPTY 0x100
#define HSU7_RX_HSU_RX_EMPTY_BIT 8
#define HSU7_RX_HSU_RX_DATA_MASK 0xFF
#define HSU7_RX_HSU_RX_DATA_BIT 0

#define HSU7_TX (*(volatile unsigned *)0x4001C000)
#define HSU7_TX_HSU_TX_DATA_MASK 0xFF
#define HSU7_TX_HSU_TX_DATA_BIT 0

#define HSU7_LEVEL (*(volatile unsigned *)0x4001C004)
#define HSU7_LEVEL_HSU_TX_LEV_MASK 0xFF00
#define HSU7_LEVEL_HSU_TX_LEV_BIT 8
#define HSU7_LEVEL_HSU_RX_LEV_MASK 0xFF
#define HSU7_LEVEL_HSU_RX_LEV_BIT 0

#define HSU7_IIR (*(volatile unsigned *)0x4001C008)
#define HSU7_IIR_HSU_TX_INT_SET_MASK 0x40
#define HSU7_IIR_HSU_TX_INT_SET 0x40
#define HSU7_IIR_HSU_TX_INT_SET_BIT 6
#define HSU7_IIR_HSU_RX_OE_MASK 0x20
#define HSU7_IIR_HSU_RX_OE 0x20
#define HSU7_IIR_HSU_RX_OE_BIT 5
#define HSU7_IIR_HSU_BRK_MASK 0x10
#define HSU7_IIR_HSU_BRK 0x10
#define HSU7_IIR_HSU_BRK_BIT 4
#define HSU7_IIR_HSU_FE_MASK 0x8
#define HSU7_IIR_HSU_FE 0x8
#define HSU7_IIR_HSU_FE_BIT 3
#define HSU7_IIR_HSU_RX_TIMEOUT_MASK 0x4
#define HSU7_IIR_HSU_RX_TIMEOUT 0x4
#define HSU7_IIR_HSU_RX_TIMEOUT_BIT 2
#define HSU7_IIR_HSU_RX_TRIG_MASK 0x2
#define HSU7_IIR_HSU_RX_TRIG 0x2
#define HSU7_IIR_HSU_RX_TRIG_BIT 1
#define HSU7_IIR_HSU_TX_MASK 0x1
#define HSU7_IIR_HSU_TX 0x1
#define HSU7_IIR_HSU_TX_BIT 0

#define HSU7_CTRL (*(volatile unsigned *)0x4001C00C)
#define HSU7_CTRL_HRTS_INV_MASK 0x200000
#define HSU7_CTRL_HRTS_INV 0x200000
#define HSU7_CTRL_HRTS_INV_BIT 21
#define HSU7_CTRL_HRTS_TRIG_MASK 0x180000
#define HSU7_CTRL_HRTS_TRIG_BIT 19
#define HSU7_CTRL_HRTS_EN_MASK 0x40000
#define HSU7_CTRL_HRTS_EN 0x40000
#define HSU7_CTRL_HRTS_EN_BIT 18
#define HSU7_CTRL_TMO_CONFIG_MASK 0x30000
#define HSU7_CTRL_TMO_CONFIG_BIT 16
#define HSU7_CTRL_HCTS_INV_MASK 0x8000
#define HSU7_CTRL_HCTS_INV 0x8000
#define HSU7_CTRL_HCTS_INV_BIT 15
#define HSU7_CTRL_HCTS_EN_MASK 0x4000
#define HSU7_CTRL_HCTS_EN 0x4000
#define HSU7_CTRL_HCTS_EN_BIT 14
#define HSU7_CTRL_HSU_OFFSET_MASK 0x3E00
#define HSU7_CTRL_HSU_OFFSET_BIT 9
#define HSU7_CTRL_HSU_BREAK_MASK 0x100
#define HSU7_CTRL_HSU_BREAK 0x100
#define HSU7_CTRL_HSU_BREAK_BIT 8
#define HSU7_CTRL_HSU_ERR_INT_EN_MASK 0x80
#define HSU7_CTRL_HSU_ERR_INT_EN 0x80
#define HSU7_CTRL_HSU_ERR_INT_EN_BIT 7
#define HSU7_CTRL_HSU_RX_INT_EN_MASK 0x40
#define HSU7_CTRL_HSU_RX_INT_EN 0x40
#define HSU7_CTRL_HSU_RX_INT_EN_BIT 6
#define HSU7_CTRL_HSU_TX_INT_EN_MASK 0x20
#define HSU7_CTRL_HSU_TX_INT_EN 0x20
#define HSU7_CTRL_HSU_TX_INT_EN_BIT 5
#define HSU7_CTRL_HSU_RX_TRIG_MASK 0x1C
#define HSU7_CTRL_HSU_RX_TRIG_BIT 2
#define HSU7_CTRL_HSU_TX_TRIG_MASK 0x3
#define HSU7_CTRL_HSU_TX_TRIG_BIT 0

#define HSU7_RATE (*(volatile unsigned *)0x4001C010)
#define HSU7_RATE_HSU_RATE_MASK 0xFF
#define HSU7_RATE_HSU_RATE_BIT 0

#define RTC_UCOUNT (*(volatile unsigned *)0x40024000)

#define RTC_DCOUNT (*(volatile unsigned *)0x40024004)

#define RTC_MATCH0 (*(volatile unsigned *)0x40024008)

#define RTC_MATCH1 (*(volatile unsigned *)0x4002400C)

#define RTC_CTRL (*(volatile unsigned *)0x40024010)
#define RTC_CTRL_RTC_CLK_level_MASK 0x400
#define RTC_CTRL_RTC_CLK_level 0x400
#define RTC_CTRL_RTC_CLK_level_BIT 10
#define RTC_CTRL_RTC_force_ONSW_MASK 0x80
#define RTC_CTRL_RTC_force_ONSW 0x80
#define RTC_CTRL_RTC_force_ONSW_BIT 7
#define RTC_CTRL_RTC_counter_clock_disable_MASK 0x40
#define RTC_CTRL_RTC_counter_clock_disable 0x40
#define RTC_CTRL_RTC_counter_clock_disable_BIT 6
#define RTC_CTRL_Software_controlled_RTC_reset_MASK 0x10
#define RTC_CTRL_Software_controlled_RTC_reset 0x10
#define RTC_CTRL_Software_controlled_RTC_reset_BIT 4
#define RTC_CTRL_Match_1_ONSW_control_MASK 0x8
#define RTC_CTRL_Match_1_ONSW_control 0x8
#define RTC_CTRL_Match_1_ONSW_control_BIT 3
#define RTC_CTRL_Match_0_ONSW_control_MASK 0x4
#define RTC_CTRL_Match_0_ONSW_control 0x4
#define RTC_CTRL_Match_0_ONSW_control_BIT 2
#define RTC_CTRL_Match_1_interrupt_enable_bit_MASK 0x2
#define RTC_CTRL_Match_1_interrupt_enable_bit 0x2
#define RTC_CTRL_Match_1_interrupt_enable_bit_BIT 1
#define RTC_CTRL_Match_0_interrupt_enable_bit_MASK 0x1
#define RTC_CTRL_Match_0_interrupt_enable_bit 0x1
#define RTC_CTRL_Match_0_interrupt_enable_bit_BIT 0

#define RTC_INTSTAT (*(volatile unsigned *)0x40024014)
#define RTC_INTSTAT_ONSW_Match_status_MASK 0x4
#define RTC_INTSTAT_ONSW_Match_status 0x4
#define RTC_INTSTAT_ONSW_Match_status_BIT 2
#define RTC_INTSTAT_Match_1_interrupt_status_MASK 0x2
#define RTC_INTSTAT_Match_1_interrupt_status 0x2
#define RTC_INTSTAT_Match_1_interrupt_status_BIT 1
#define RTC_INTSTAT_Match_0_interrupt_status_MASK 0x1
#define RTC_INTSTAT_Match_0_interrupt_status 0x1
#define RTC_INTSTAT_Match_0_interrupt_status_BIT 0

#define RTC_KEY (*(volatile unsigned *)0x40024018)

#define RTC_SRAM (*(volatile unsigned *)0x40024080)

#define P3_INP_STATE (*(volatile unsigned *)0x40028000)
#define P3_INP_STATE_GPI_28_MASK 0x10000000
#define P3_INP_STATE_GPI_28 0x10000000
#define P3_INP_STATE_GPI_28_BIT 28
#define P3_INP_STATE_GPI_27_MASK 0x8000000
#define P3_INP_STATE_GPI_27 0x8000000
#define P3_INP_STATE_GPI_27_BIT 27
#define P3_INP_STATE_GPI_25_MASK 0x2000000
#define P3_INP_STATE_GPI_25 0x2000000
#define P3_INP_STATE_GPI_25_BIT 25
#define P3_INP_STATE_GPIO_05_MASK 0x1000000
#define P3_INP_STATE_GPIO_05 0x1000000
#define P3_INP_STATE_GPIO_05_BIT 24
#define P3_INP_STATE_GPI_23_MASK 0x800000
#define P3_INP_STATE_GPI_23 0x800000
#define P3_INP_STATE_GPI_23_BIT 23
#define P3_INP_STATE_GPI_22_MASK 0x400000
#define P3_INP_STATE_GPI_22 0x400000
#define P3_INP_STATE_GPI_22_BIT 22
#define P3_INP_STATE_GPI_21_MASK 0x200000
#define P3_INP_STATE_GPI_21 0x200000
#define P3_INP_STATE_GPI_21_BIT 21
#define P3_INP_STATE_GPI_20_MASK 0x100000
#define P3_INP_STATE_GPI_20 0x100000
#define P3_INP_STATE_GPI_20_BIT 20
#define P3_INP_STATE_GPI_19_MASK 0x80000
#define P3_INP_STATE_GPI_19 0x80000
#define P3_INP_STATE_GPI_19_BIT 19
#define P3_INP_STATE_GPI_18_MASK 0x40000
#define P3_INP_STATE_GPI_18 0x40000
#define P3_INP_STATE_GPI_18_BIT 18
#define P3_INP_STATE_GPI_17_MASK 0x20000
#define P3_INP_STATE_GPI_17 0x20000
#define P3_INP_STATE_GPI_17_BIT 17
#define P3_INP_STATE_GPI_16_MASK 0x10000
#define P3_INP_STATE_GPI_16 0x10000
#define P3_INP_STATE_GPI_16_BIT 16
#define P3_INP_STATE_GPI_15_MASK 0x8000
#define P3_INP_STATE_GPI_15 0x8000
#define P3_INP_STATE_GPI_15_BIT 15
#define P3_INP_STATE_GPIO_04_MASK 0x4000
#define P3_INP_STATE_GPIO_04 0x4000
#define P3_INP_STATE_GPIO_04_BIT 14
#define P3_INP_STATE_GPIO_03_MASK 0x2000
#define P3_INP_STATE_GPIO_03 0x2000
#define P3_INP_STATE_GPIO_03_BIT 13
#define P3_INP_STATE_GPIO_02_MASK 0x1000
#define P3_INP_STATE_GPIO_02 0x1000
#define P3_INP_STATE_GPIO_02_BIT 12
#define P3_INP_STATE_GPIO_01_MASK 0x800
#define P3_INP_STATE_GPIO_01 0x800
#define P3_INP_STATE_GPIO_01_BIT 11
#define P3_INP_STATE_GPIO_00_MASK 0x400
#define P3_INP_STATE_GPIO_00 0x400
#define P3_INP_STATE_GPIO_00_BIT 10
#define P3_INP_STATE_GPI_09_MASK 0x200
#define P3_INP_STATE_GPI_09 0x200
#define P3_INP_STATE_GPI_09_BIT 9
#define P3_INP_STATE_GPI_08_MASK 0x100
#define P3_INP_STATE_GPI_08 0x100
#define P3_INP_STATE_GPI_08_BIT 8
#define P3_INP_STATE_GPI_07_MASK 0x80
#define P3_INP_STATE_GPI_07 0x80
#define P3_INP_STATE_GPI_07_BIT 7
#define P3_INP_STATE_GPI_06_MASK 0x40
#define P3_INP_STATE_GPI_06 0x40
#define P3_INP_STATE_GPI_06_BIT 6
#define P3_INP_STATE_GPI_05_MASK 0x20
#define P3_INP_STATE_GPI_05 0x20
#define P3_INP_STATE_GPI_05_BIT 5
#define P3_INP_STATE_GPI_04_MASK 0x10
#define P3_INP_STATE_GPI_04 0x10
#define P3_INP_STATE_GPI_04_BIT 4
#define P3_INP_STATE_GPI_03_MASK 0x8
#define P3_INP_STATE_GPI_03 0x8
#define P3_INP_STATE_GPI_03_BIT 3
#define P3_INP_STATE_GPI_02_MASK 0x4
#define P3_INP_STATE_GPI_02 0x4
#define P3_INP_STATE_GPI_02_BIT 2
#define P3_INP_STATE_GPI_01_MASK 0x2
#define P3_INP_STATE_GPI_01 0x2
#define P3_INP_STATE_GPI_01_BIT 1
#define P3_INP_STATE_GPI_00_MASK 0x1
#define P3_INP_STATE_GPI_00 0x1
#define P3_INP_STATE_GPI_00_BIT 0

#define P3_OUTP_SET (*(volatile unsigned *)0x40028004)
#define P3_OUTP_SET_GPIO_05_MASK 0x40000000
#define P3_OUTP_SET_GPIO_05 0x40000000
#define P3_OUTP_SET_GPIO_05_BIT 30
#define P3_OUTP_SET_GPIO_04_MASK 0x20000000
#define P3_OUTP_SET_GPIO_04 0x20000000
#define P3_OUTP_SET_GPIO_04_BIT 29
#define P3_OUTP_SET_GPIO_03_MASK 0x10000000
#define P3_OUTP_SET_GPIO_03 0x10000000
#define P3_OUTP_SET_GPIO_03_BIT 28
#define P3_OUTP_SET_GPIO_02_MASK 0x8000000
#define P3_OUTP_SET_GPIO_02 0x8000000
#define P3_OUTP_SET_GPIO_02_BIT 27
#define P3_OUTP_SET_GPIO_01_MASK 0x4000000
#define P3_OUTP_SET_GPIO_01 0x4000000
#define P3_OUTP_SET_GPIO_01_BIT 26
#define P3_OUTP_SET_GPIO_00_MASK 0x2000000
#define P3_OUTP_SET_GPIO_00 0x2000000
#define P3_OUTP_SET_GPIO_00_BIT 25
#define P3_OUTP_SET_GPO_23_MASK 0x800000
#define P3_OUTP_SET_GPO_23 0x800000
#define P3_OUTP_SET_GPO_23_BIT 23
#define P3_OUTP_SET_GPO_22_MASK 0x400000
#define P3_OUTP_SET_GPO_22 0x400000
#define P3_OUTP_SET_GPO_22_BIT 22
#define P3_OUTP_SET_GPO_21_MASK 0x200000
#define P3_OUTP_SET_GPO_21 0x200000
#define P3_OUTP_SET_GPO_21_BIT 21
#define P3_OUTP_SET_GPO_20_MASK 0x100000
#define P3_OUTP_SET_GPO_20 0x100000
#define P3_OUTP_SET_GPO_20_BIT 20
#define P3_OUTP_SET_GPO_19_MASK 0x80000
#define P3_OUTP_SET_GPO_19 0x80000
#define P3_OUTP_SET_GPO_19_BIT 19
#define P3_OUTP_SET_GPO_18_MASK 0x40000
#define P3_OUTP_SET_GPO_18 0x40000
#define P3_OUTP_SET_GPO_18_BIT 18
#define P3_OUTP_SET_GPO_17_MASK 0x20000
#define P3_OUTP_SET_GPO_17 0x20000
#define P3_OUTP_SET_GPO_17_BIT 17
#define P3_OUTP_SET_GPO_16_MASK 0x10000
#define P3_OUTP_SET_GPO_16 0x10000
#define P3_OUTP_SET_GPO_16_BIT 16
#define P3_OUTP_SET_GPO_15_MASK 0x8000
#define P3_OUTP_SET_GPO_15 0x8000
#define P3_OUTP_SET_GPO_15_BIT 15
#define P3_OUTP_SET_GPO_14_MASK 0x4000
#define P3_OUTP_SET_GPO_14 0x4000
#define P3_OUTP_SET_GPO_14_BIT 14
#define P3_OUTP_SET_GPO_13_MASK 0x2000
#define P3_OUTP_SET_GPO_13 0x2000
#define P3_OUTP_SET_GPO_13_BIT 13
#define P3_OUTP_SET_GPO_12_MASK 0x1000
#define P3_OUTP_SET_GPO_12 0x1000
#define P3_OUTP_SET_GPO_12_BIT 12
#define P3_OUTP_SET_GPO_11_MASK 0x800
#define P3_OUTP_SET_GPO_11 0x800
#define P3_OUTP_SET_GPO_11_BIT 11
#define P3_OUTP_SET_GPO_10_MASK 0x400
#define P3_OUTP_SET_GPO_10 0x400
#define P3_OUTP_SET_GPO_10_BIT 10
#define P3_OUTP_SET_GPO_09_MASK 0x200
#define P3_OUTP_SET_GPO_09 0x200
#define P3_OUTP_SET_GPO_09_BIT 9
#define P3_OUTP_SET_GPO_08_MASK 0x100
#define P3_OUTP_SET_GPO_08 0x100
#define P3_OUTP_SET_GPO_08_BIT 8
#define P3_OUTP_SET_GPO_07_MASK 0x80
#define P3_OUTP_SET_GPO_07 0x80
#define P3_OUTP_SET_GPO_07_BIT 7
#define P3_OUTP_SET_GPO_06_MASK 0x40
#define P3_OUTP_SET_GPO_06 0x40
#define P3_OUTP_SET_GPO_06_BIT 6
#define P3_OUTP_SET_GPO_05_MASK 0x20
#define P3_OUTP_SET_GPO_05 0x20
#define P3_OUTP_SET_GPO_05_BIT 5
#define P3_OUTP_SET_GPO_04_MASK 0x10
#define P3_OUTP_SET_GPO_04 0x10
#define P3_OUTP_SET_GPO_04_BIT 4
#define P3_OUTP_SET_GPO_03_MASK 0x8
#define P3_OUTP_SET_GPO_03 0x8
#define P3_OUTP_SET_GPO_03_BIT 3
#define P3_OUTP_SET_GPO_02_MASK 0x4
#define P3_OUTP_SET_GPO_02 0x4
#define P3_OUTP_SET_GPO_02_BIT 2
#define P3_OUTP_SET_GPO_01_MASK 0x2
#define P3_OUTP_SET_GPO_01 0x2
#define P3_OUTP_SET_GPO_01_BIT 1
#define P3_OUTP_SET_GPO_00_MASK 0x1
#define P3_OUTP_SET_GPO_00 0x1
#define P3_OUTP_SET_GPO_00_BIT 0

#define P3_OUTP_CLR (*(volatile unsigned *)0x40028008)
#define P3_OUTP_CLR_GPIO_05_MASK 0x40000000
#define P3_OUTP_CLR_GPIO_05 0x40000000
#define P3_OUTP_CLR_GPIO_05_BIT 30
#define P3_OUTP_CLR_GPIO_04_MASK 0x20000000
#define P3_OUTP_CLR_GPIO_04 0x20000000
#define P3_OUTP_CLR_GPIO_04_BIT 29
#define P3_OUTP_CLR_GPIO_03_MASK 0x10000000
#define P3_OUTP_CLR_GPIO_03 0x10000000
#define P3_OUTP_CLR_GPIO_03_BIT 28
#define P3_OUTP_CLR_GPIO_02_MASK 0x8000000
#define P3_OUTP_CLR_GPIO_02 0x8000000
#define P3_OUTP_CLR_GPIO_02_BIT 27
#define P3_OUTP_CLR_GPIO_01_MASK 0x4000000
#define P3_OUTP_CLR_GPIO_01 0x4000000
#define P3_OUTP_CLR_GPIO_01_BIT 26
#define P3_OUTP_CLR_GPIO_00_MASK 0x2000000
#define P3_OUTP_CLR_GPIO_00 0x2000000
#define P3_OUTP_CLR_GPIO_00_BIT 25
#define P3_OUTP_CLR_GPO_23_MASK 0x800000
#define P3_OUTP_CLR_GPO_23 0x800000
#define P3_OUTP_CLR_GPO_23_BIT 23
#define P3_OUTP_CLR_GPO_22_MASK 0x400000
#define P3_OUTP_CLR_GPO_22 0x400000
#define P3_OUTP_CLR_GPO_22_BIT 22
#define P3_OUTP_CLR_GPO_21_MASK 0x200000
#define P3_OUTP_CLR_GPO_21 0x200000
#define P3_OUTP_CLR_GPO_21_BIT 21
#define P3_OUTP_CLR_GPO_20_MASK 0x100000
#define P3_OUTP_CLR_GPO_20 0x100000
#define P3_OUTP_CLR_GPO_20_BIT 20
#define P3_OUTP_CLR_GPO_19_MASK 0x80000
#define P3_OUTP_CLR_GPO_19 0x80000
#define P3_OUTP_CLR_GPO_19_BIT 19
#define P3_OUTP_CLR_GPO_18_MASK 0x40000
#define P3_OUTP_CLR_GPO_18 0x40000
#define P3_OUTP_CLR_GPO_18_BIT 18
#define P3_OUTP_CLR_GPO_17_MASK 0x20000
#define P3_OUTP_CLR_GPO_17 0x20000
#define P3_OUTP_CLR_GPO_17_BIT 17
#define P3_OUTP_CLR_GPO_16_MASK 0x10000
#define P3_OUTP_CLR_GPO_16 0x10000
#define P3_OUTP_CLR_GPO_16_BIT 16
#define P3_OUTP_CLR_GPO_15_MASK 0x8000
#define P3_OUTP_CLR_GPO_15 0x8000
#define P3_OUTP_CLR_GPO_15_BIT 15
#define P3_OUTP_CLR_GPO_14_MASK 0x4000
#define P3_OUTP_CLR_GPO_14 0x4000
#define P3_OUTP_CLR_GPO_14_BIT 14
#define P3_OUTP_CLR_GPO_13_MASK 0x2000
#define P3_OUTP_CLR_GPO_13 0x2000
#define P3_OUTP_CLR_GPO_13_BIT 13
#define P3_OUTP_CLR_GPO_12_MASK 0x1000
#define P3_OUTP_CLR_GPO_12 0x1000
#define P3_OUTP_CLR_GPO_12_BIT 12
#define P3_OUTP_CLR_GPO_11_MASK 0x800
#define P3_OUTP_CLR_GPO_11 0x800
#define P3_OUTP_CLR_GPO_11_BIT 11
#define P3_OUTP_CLR_GPO_10_MASK 0x400
#define P3_OUTP_CLR_GPO_10 0x400
#define P3_OUTP_CLR_GPO_10_BIT 10
#define P3_OUTP_CLR_GPO_09_MASK 0x200
#define P3_OUTP_CLR_GPO_09 0x200
#define P3_OUTP_CLR_GPO_09_BIT 9
#define P3_OUTP_CLR_GPO_08_MASK 0x100
#define P3_OUTP_CLR_GPO_08 0x100
#define P3_OUTP_CLR_GPO_08_BIT 8
#define P3_OUTP_CLR_GPO_07_MASK 0x80
#define P3_OUTP_CLR_GPO_07 0x80
#define P3_OUTP_CLR_GPO_07_BIT 7
#define P3_OUTP_CLR_GPO_06_MASK 0x40
#define P3_OUTP_CLR_GPO_06 0x40
#define P3_OUTP_CLR_GPO_06_BIT 6
#define P3_OUTP_CLR_GPO_05_MASK 0x20
#define P3_OUTP_CLR_GPO_05 0x20
#define P3_OUTP_CLR_GPO_05_BIT 5
#define P3_OUTP_CLR_GPO_04_MASK 0x10
#define P3_OUTP_CLR_GPO_04 0x10
#define P3_OUTP_CLR_GPO_04_BIT 4
#define P3_OUTP_CLR_GPO_03_MASK 0x8
#define P3_OUTP_CLR_GPO_03 0x8
#define P3_OUTP_CLR_GPO_03_BIT 3
#define P3_OUTP_CLR_GPO_02_MASK 0x4
#define P3_OUTP_CLR_GPO_02 0x4
#define P3_OUTP_CLR_GPO_02_BIT 2
#define P3_OUTP_CLR_GPO_01_MASK 0x2
#define P3_OUTP_CLR_GPO_01 0x2
#define P3_OUTP_CLR_GPO_01_BIT 1
#define P3_OUTP_CLR_GPO_00_MASK 0x1
#define P3_OUTP_CLR_GPO_00 0x1
#define P3_OUTP_CLR_GPO_00_BIT 0

#define P3_OUTP_STATE (*(volatile unsigned *)0x4002800C)
#define P3_OUTP_STATE_GPIO_05_MASK 0x40000000
#define P3_OUTP_STATE_GPIO_05 0x40000000
#define P3_OUTP_STATE_GPIO_05_BIT 30
#define P3_OUTP_STATE_GPIO_04_MASK 0x20000000
#define P3_OUTP_STATE_GPIO_04 0x20000000
#define P3_OUTP_STATE_GPIO_04_BIT 29
#define P3_OUTP_STATE_GPIO_03_MASK 0x10000000
#define P3_OUTP_STATE_GPIO_03 0x10000000
#define P3_OUTP_STATE_GPIO_03_BIT 28
#define P3_OUTP_STATE_GPIO_02_MASK 0x8000000
#define P3_OUTP_STATE_GPIO_02 0x8000000
#define P3_OUTP_STATE_GPIO_02_BIT 27
#define P3_OUTP_STATE_GPIO_01_MASK 0x4000000
#define P3_OUTP_STATE_GPIO_01 0x4000000
#define P3_OUTP_STATE_GPIO_01_BIT 26
#define P3_OUTP_STATE_GPIO_00_MASK 0x2000000
#define P3_OUTP_STATE_GPIO_00 0x2000000
#define P3_OUTP_STATE_GPIO_00_BIT 25
#define P3_OUTP_STATE_GPO_23_MASK 0x800000
#define P3_OUTP_STATE_GPO_23 0x800000
#define P3_OUTP_STATE_GPO_23_BIT 23
#define P3_OUTP_STATE_GPO_22_MASK 0x400000
#define P3_OUTP_STATE_GPO_22 0x400000
#define P3_OUTP_STATE_GPO_22_BIT 22
#define P3_OUTP_STATE_GPO_21_MASK 0x200000
#define P3_OUTP_STATE_GPO_21 0x200000
#define P3_OUTP_STATE_GPO_21_BIT 21
#define P3_OUTP_STATE_GPO_20_MASK 0x100000
#define P3_OUTP_STATE_GPO_20 0x100000
#define P3_OUTP_STATE_GPO_20_BIT 20
#define P3_OUTP_STATE_GPO_19_MASK 0x80000
#define P3_OUTP_STATE_GPO_19 0x80000
#define P3_OUTP_STATE_GPO_19_BIT 19
#define P3_OUTP_STATE_GPO_18_MASK 0x40000
#define P3_OUTP_STATE_GPO_18 0x40000
#define P3_OUTP_STATE_GPO_18_BIT 18
#define P3_OUTP_STATE_GPO_17_MASK 0x20000
#define P3_OUTP_STATE_GPO_17 0x20000
#define P3_OUTP_STATE_GPO_17_BIT 17
#define P3_OUTP_STATE_GPO_16_MASK 0x10000
#define P3_OUTP_STATE_GPO_16 0x10000
#define P3_OUTP_STATE_GPO_16_BIT 16
#define P3_OUTP_STATE_GPO_15_MASK 0x8000
#define P3_OUTP_STATE_GPO_15 0x8000
#define P3_OUTP_STATE_GPO_15_BIT 15
#define P3_OUTP_STATE_GPO_14_MASK 0x4000
#define P3_OUTP_STATE_GPO_14 0x4000
#define P3_OUTP_STATE_GPO_14_BIT 14
#define P3_OUTP_STATE_GPO_13_MASK 0x2000
#define P3_OUTP_STATE_GPO_13 0x2000
#define P3_OUTP_STATE_GPO_13_BIT 13
#define P3_OUTP_STATE_GPO_12_MASK 0x1000
#define P3_OUTP_STATE_GPO_12 0x1000
#define P3_OUTP_STATE_GPO_12_BIT 12
#define P3_OUTP_STATE_GPO_11_MASK 0x800
#define P3_OUTP_STATE_GPO_11 0x800
#define P3_OUTP_STATE_GPO_11_BIT 11
#define P3_OUTP_STATE_GPO_10_MASK 0x400
#define P3_OUTP_STATE_GPO_10 0x400
#define P3_OUTP_STATE_GPO_10_BIT 10
#define P3_OUTP_STATE_GPO_09_MASK 0x200
#define P3_OUTP_STATE_GPO_09 0x200
#define P3_OUTP_STATE_GPO_09_BIT 9
#define P3_OUTP_STATE_GPO_08_MASK 0x100
#define P3_OUTP_STATE_GPO_08 0x100
#define P3_OUTP_STATE_GPO_08_BIT 8
#define P3_OUTP_STATE_GPO_07_MASK 0x80
#define P3_OUTP_STATE_GPO_07 0x80
#define P3_OUTP_STATE_GPO_07_BIT 7
#define P3_OUTP_STATE_GPO_06_MASK 0x40
#define P3_OUTP_STATE_GPO_06 0x40
#define P3_OUTP_STATE_GPO_06_BIT 6
#define P3_OUTP_STATE_GPO_05_MASK 0x20
#define P3_OUTP_STATE_GPO_05 0x20
#define P3_OUTP_STATE_GPO_05_BIT 5
#define P3_OUTP_STATE_GPO_04_MASK 0x10
#define P3_OUTP_STATE_GPO_04 0x10
#define P3_OUTP_STATE_GPO_04_BIT 4
#define P3_OUTP_STATE_GPO_03_MASK 0x8
#define P3_OUTP_STATE_GPO_03 0x8
#define P3_OUTP_STATE_GPO_03_BIT 3
#define P3_OUTP_STATE_GPO_02_MASK 0x4
#define P3_OUTP_STATE_GPO_02 0x4
#define P3_OUTP_STATE_GPO_02_BIT 2
#define P3_OUTP_STATE_GPO_01_MASK 0x2
#define P3_OUTP_STATE_GPO_01 0x2
#define P3_OUTP_STATE_GPO_01_BIT 1
#define P3_OUTP_STATE_GPO_00_MASK 0x1
#define P3_OUTP_STATE_GPO_00 0x1
#define P3_OUTP_STATE_GPO_00_BIT 0

#define P2_DIR_SET (*(volatile unsigned *)0x40028010)
#define P2_DIR_SET_GPIO_05_MASK 0x40000000
#define P2_DIR_SET_GPIO_05 0x40000000
#define P2_DIR_SET_GPIO_05_BIT 30
#define P2_DIR_SET_GPIO_04_MASK 0x20000000
#define P2_DIR_SET_GPIO_04 0x20000000
#define P2_DIR_SET_GPIO_04_BIT 29
#define P2_DIR_SET_GPIO_03_MASK 0x10000000
#define P2_DIR_SET_GPIO_03 0x10000000
#define P2_DIR_SET_GPIO_03_BIT 28
#define P2_DIR_SET_GPIO_02_MASK 0x8000000
#define P2_DIR_SET_GPIO_02 0x8000000
#define P2_DIR_SET_GPIO_02_BIT 27
#define P2_DIR_SET_GPIO_01_MASK 0x4000000
#define P2_DIR_SET_GPIO_01 0x4000000
#define P2_DIR_SET_GPIO_01_BIT 26
#define P2_DIR_SET_GPIO_00_MASK 0x2000000
#define P2_DIR_SET_GPIO_00 0x2000000
#define P2_DIR_SET_GPIO_00_BIT 25
#define P2_DIR_SET_EMC_D31_MASK 0x1000
#define P2_DIR_SET_EMC_D31 0x1000
#define P2_DIR_SET_EMC_D31_BIT 12
#define P2_DIR_SET_EMC_D30_MASK 0x800
#define P2_DIR_SET_EMC_D30 0x800
#define P2_DIR_SET_EMC_D30_BIT 11
#define P2_DIR_SET_EMC_D29_MASK 0x400
#define P2_DIR_SET_EMC_D29 0x400
#define P2_DIR_SET_EMC_D29_BIT 10
#define P2_DIR_SET_EMC_D28_MASK 0x200
#define P2_DIR_SET_EMC_D28 0x200
#define P2_DIR_SET_EMC_D28_BIT 9
#define P2_DIR_SET_EMC_D27_MASK 0x100
#define P2_DIR_SET_EMC_D27 0x100
#define P2_DIR_SET_EMC_D27_BIT 8
#define P2_DIR_SET_EMC_D26_MASK 0x80
#define P2_DIR_SET_EMC_D26 0x80
#define P2_DIR_SET_EMC_D26_BIT 7
#define P2_DIR_SET_EMC_D25_MASK 0x40
#define P2_DIR_SET_EMC_D25 0x40
#define P2_DIR_SET_EMC_D25_BIT 6
#define P2_DIR_SET_EMC_D24_MASK 0x20
#define P2_DIR_SET_EMC_D24 0x20
#define P2_DIR_SET_EMC_D24_BIT 5
#define P2_DIR_SET_EMC_D23_MASK 0x10
#define P2_DIR_SET_EMC_D23 0x10
#define P2_DIR_SET_EMC_D23_BIT 4
#define P2_DIR_SET_EMC_D22_MASK 0x8
#define P2_DIR_SET_EMC_D22 0x8
#define P2_DIR_SET_EMC_D22_BIT 3
#define P2_DIR_SET_EMC_D21_MASK 0x4
#define P2_DIR_SET_EMC_D21 0x4
#define P2_DIR_SET_EMC_D21_BIT 2
#define P2_DIR_SET_EMC_D20_MASK 0x2
#define P2_DIR_SET_EMC_D20 0x2
#define P2_DIR_SET_EMC_D20_BIT 1
#define P2_DIR_SET_EMC_D19_MASK 0x1
#define P2_DIR_SET_EMC_D19 0x1
#define P2_DIR_SET_EMC_D19_BIT 0

#define P2_DIR_CLR (*(volatile unsigned *)0x40028014)
#define P2_DIR_CLR_GPIO_05_MASK 0x40000000
#define P2_DIR_CLR_GPIO_05 0x40000000
#define P2_DIR_CLR_GPIO_05_BIT 30
#define P2_DIR_CLR_GPIO_04_MASK 0x20000000
#define P2_DIR_CLR_GPIO_04 0x20000000
#define P2_DIR_CLR_GPIO_04_BIT 29
#define P2_DIR_CLR_GPIO_03_MASK 0x10000000
#define P2_DIR_CLR_GPIO_03 0x10000000
#define P2_DIR_CLR_GPIO_03_BIT 28
#define P2_DIR_CLR_GPIO_02_MASK 0x8000000
#define P2_DIR_CLR_GPIO_02 0x8000000
#define P2_DIR_CLR_GPIO_02_BIT 27
#define P2_DIR_CLR_GPIO_01_MASK 0x4000000
#define P2_DIR_CLR_GPIO_01 0x4000000
#define P2_DIR_CLR_GPIO_01_BIT 26
#define P2_DIR_CLR_GPIO_00_MASK 0x2000000
#define P2_DIR_CLR_GPIO_00 0x2000000
#define P2_DIR_CLR_GPIO_00_BIT 25
#define P2_DIR_CLR_EMC_D31_MASK 0x1000
#define P2_DIR_CLR_EMC_D31 0x1000
#define P2_DIR_CLR_EMC_D31_BIT 12
#define P2_DIR_CLR_EMC_D30_MASK 0x800
#define P2_DIR_CLR_EMC_D30 0x800
#define P2_DIR_CLR_EMC_D30_BIT 11
#define P2_DIR_CLR_EMC_D29_MASK 0x400
#define P2_DIR_CLR_EMC_D29 0x400
#define P2_DIR_CLR_EMC_D29_BIT 10
#define P2_DIR_CLR_EMC_D28_MASK 0x200
#define P2_DIR_CLR_EMC_D28 0x200
#define P2_DIR_CLR_EMC_D28_BIT 9
#define P2_DIR_CLR_EMC_D27_MASK 0x100
#define P2_DIR_CLR_EMC_D27 0x100
#define P2_DIR_CLR_EMC_D27_BIT 8
#define P2_DIR_CLR_EMC_D26_MASK 0x80
#define P2_DIR_CLR_EMC_D26 0x80
#define P2_DIR_CLR_EMC_D26_BIT 7
#define P2_DIR_CLR_EMC_D25_MASK 0x40
#define P2_DIR_CLR_EMC_D25 0x40
#define P2_DIR_CLR_EMC_D25_BIT 6
#define P2_DIR_CLR_EMC_D24_MASK 0x20
#define P2_DIR_CLR_EMC_D24 0x20
#define P2_DIR_CLR_EMC_D24_BIT 5
#define P2_DIR_CLR_EMC_D23_MASK 0x10
#define P2_DIR_CLR_EMC_D23 0x10
#define P2_DIR_CLR_EMC_D23_BIT 4
#define P2_DIR_CLR_EMC_D22_MASK 0x8
#define P2_DIR_CLR_EMC_D22 0x8
#define P2_DIR_CLR_EMC_D22_BIT 3
#define P2_DIR_CLR_EMC_D21_MASK 0x4
#define P2_DIR_CLR_EMC_D21 0x4
#define P2_DIR_CLR_EMC_D21_BIT 2
#define P2_DIR_CLR_EMC_D20_MASK 0x2
#define P2_DIR_CLR_EMC_D20 0x2
#define P2_DIR_CLR_EMC_D20_BIT 1
#define P2_DIR_CLR_EMC_D19_MASK 0x1
#define P2_DIR_CLR_EMC_D19 0x1
#define P2_DIR_CLR_EMC_D19_BIT 0

#define P2_DIR_STATE (*(volatile unsigned *)0x40028018)
#define P2_DIR_STATE_GPIO_05_MASK 0x40000000
#define P2_DIR_STATE_GPIO_05 0x40000000
#define P2_DIR_STATE_GPIO_05_BIT 30
#define P2_DIR_STATE_GPIO_04_MASK 0x20000000
#define P2_DIR_STATE_GPIO_04 0x20000000
#define P2_DIR_STATE_GPIO_04_BIT 29
#define P2_DIR_STATE_GPIO_03_MASK 0x10000000
#define P2_DIR_STATE_GPIO_03 0x10000000
#define P2_DIR_STATE_GPIO_03_BIT 28
#define P2_DIR_STATE_GPIO_02_MASK 0x8000000
#define P2_DIR_STATE_GPIO_02 0x8000000
#define P2_DIR_STATE_GPIO_02_BIT 27
#define P2_DIR_STATE_GPIO_01_MASK 0x4000000
#define P2_DIR_STATE_GPIO_01 0x4000000
#define P2_DIR_STATE_GPIO_01_BIT 26
#define P2_DIR_STATE_GPIO_00_MASK 0x2000000
#define P2_DIR_STATE_GPIO_00 0x2000000
#define P2_DIR_STATE_GPIO_00_BIT 25
#define P2_DIR_STATE_RAM_D31_MASK 0x1000
#define P2_DIR_STATE_RAM_D31 0x1000
#define P2_DIR_STATE_RAM_D31_BIT 12
#define P2_DIR_STATE_RAM_D30_MASK 0x800
#define P2_DIR_STATE_RAM_D30 0x800
#define P2_DIR_STATE_RAM_D30_BIT 11
#define P2_DIR_STATE_RAM_D29_MASK 0x400
#define P2_DIR_STATE_RAM_D29 0x400
#define P2_DIR_STATE_RAM_D29_BIT 10
#define P2_DIR_STATE_RAM_D28_MASK 0x200
#define P2_DIR_STATE_RAM_D28 0x200
#define P2_DIR_STATE_RAM_D28_BIT 9
#define P2_DIR_STATE_RAM_D27_MASK 0x100
#define P2_DIR_STATE_RAM_D27 0x100
#define P2_DIR_STATE_RAM_D27_BIT 8
#define P2_DIR_STATE_RAM_D26_MASK 0x80
#define P2_DIR_STATE_RAM_D26 0x80
#define P2_DIR_STATE_RAM_D26_BIT 7
#define P2_DIR_STATE_RAM_D25_MASK 0x40
#define P2_DIR_STATE_RAM_D25 0x40
#define P2_DIR_STATE_RAM_D25_BIT 6
#define P2_DIR_STATE_RAM_D24_MASK 0x20
#define P2_DIR_STATE_RAM_D24 0x20
#define P2_DIR_STATE_RAM_D24_BIT 5
#define P2_DIR_STATE_RAM_D23_MASK 0x10
#define P2_DIR_STATE_RAM_D23 0x10
#define P2_DIR_STATE_RAM_D23_BIT 4
#define P2_DIR_STATE_RAM_D22_MASK 0x8
#define P2_DIR_STATE_RAM_D22 0x8
#define P2_DIR_STATE_RAM_D22_BIT 3
#define P2_DIR_STATE_RAM_D21_MASK 0x4
#define P2_DIR_STATE_RAM_D21 0x4
#define P2_DIR_STATE_RAM_D21_BIT 2
#define P2_DIR_STATE_RAM_D20_MASK 0x2
#define P2_DIR_STATE_RAM_D20 0x2
#define P2_DIR_STATE_RAM_D20_BIT 1
#define P2_DIR_STATE_RAM_D19_MASK 0x1
#define P2_DIR_STATE_RAM_D19 0x1
#define P2_DIR_STATE_RAM_D19_BIT 0

#define P2_INP_STATE (*(volatile unsigned *)0x4002801C)
#define P2_INP_STATE_EMC_D31_MASK 0x1000
#define P2_INP_STATE_EMC_D31 0x1000
#define P2_INP_STATE_EMC_D31_BIT 12
#define P2_INP_STATE_EMC_D30_MASK 0x800
#define P2_INP_STATE_EMC_D30 0x800
#define P2_INP_STATE_EMC_D30_BIT 11
#define P2_INP_STATE_EMC_D29_MASK 0x400
#define P2_INP_STATE_EMC_D29 0x400
#define P2_INP_STATE_EMC_D29_BIT 10
#define P2_INP_STATE_EMC_D28_MASK 0x200
#define P2_INP_STATE_EMC_D28 0x200
#define P2_INP_STATE_EMC_D28_BIT 9
#define P2_INP_STATE_EMC_D27_MASK 0x100
#define P2_INP_STATE_EMC_D27 0x100
#define P2_INP_STATE_EMC_D27_BIT 8
#define P2_INP_STATE_EMC_D26_MASK 0x80
#define P2_INP_STATE_EMC_D26 0x80
#define P2_INP_STATE_EMC_D26_BIT 7
#define P2_INP_STATE_EMC_D25_MASK 0x40
#define P2_INP_STATE_EMC_D25 0x40
#define P2_INP_STATE_EMC_D25_BIT 6
#define P2_INP_STATE_EMC_D24_MASK 0x20
#define P2_INP_STATE_EMC_D24 0x20
#define P2_INP_STATE_EMC_D24_BIT 5
#define P2_INP_STATE_EMC_D23_MASK 0x10
#define P2_INP_STATE_EMC_D23 0x10
#define P2_INP_STATE_EMC_D23_BIT 4
#define P2_INP_STATE_EMC_D22_MASK 0x8
#define P2_INP_STATE_EMC_D22 0x8
#define P2_INP_STATE_EMC_D22_BIT 3
#define P2_INP_STATE_EMC_D21_MASK 0x4
#define P2_INP_STATE_EMC_D21 0x4
#define P2_INP_STATE_EMC_D21_BIT 2
#define P2_INP_STATE_EMC_D20_MASK 0x2
#define P2_INP_STATE_EMC_D20 0x2
#define P2_INP_STATE_EMC_D20_BIT 1
#define P2_INP_STATE_EMC_D19_MASK 0x1
#define P2_INP_STATE_EMC_D19 0x1
#define P2_INP_STATE_EMC_D19_BIT 0

#define P2_OUTP_SET (*(volatile unsigned *)0x40028020)
#define P2_OUTP_SET_EMC_D31_MASK 0x1000
#define P2_OUTP_SET_EMC_D31 0x1000
#define P2_OUTP_SET_EMC_D31_BIT 12
#define P2_OUTP_SET_EMC_D30_MASK 0x800
#define P2_OUTP_SET_EMC_D30 0x800
#define P2_OUTP_SET_EMC_D30_BIT 11
#define P2_OUTP_SET_EMC_D29_MASK 0x400
#define P2_OUTP_SET_EMC_D29 0x400
#define P2_OUTP_SET_EMC_D29_BIT 10
#define P2_OUTP_SET_EMC_D28_MASK 0x200
#define P2_OUTP_SET_EMC_D28 0x200
#define P2_OUTP_SET_EMC_D28_BIT 9
#define P2_OUTP_SET_EMC_D27_MASK 0x100
#define P2_OUTP_SET_EMC_D27 0x100
#define P2_OUTP_SET_EMC_D27_BIT 8
#define P2_OUTP_SET_EMC_D26_MASK 0x80
#define P2_OUTP_SET_EMC_D26 0x80
#define P2_OUTP_SET_EMC_D26_BIT 7
#define P2_OUTP_SET_EMC_D25_MASK 0x40
#define P2_OUTP_SET_EMC_D25 0x40
#define P2_OUTP_SET_EMC_D25_BIT 6
#define P2_OUTP_SET_EMC_D24_MASK 0x20
#define P2_OUTP_SET_EMC_D24 0x20
#define P2_OUTP_SET_EMC_D24_BIT 5
#define P2_OUTP_SET_EMC_D23_MASK 0x10
#define P2_OUTP_SET_EMC_D23 0x10
#define P2_OUTP_SET_EMC_D23_BIT 4
#define P2_OUTP_SET_EMC_D22_MASK 0x8
#define P2_OUTP_SET_EMC_D22 0x8
#define P2_OUTP_SET_EMC_D22_BIT 3
#define P2_OUTP_SET_EMC_D21_MASK 0x4
#define P2_OUTP_SET_EMC_D21 0x4
#define P2_OUTP_SET_EMC_D21_BIT 2
#define P2_OUTP_SET_EMC_D20_MASK 0x2
#define P2_OUTP_SET_EMC_D20 0x2
#define P2_OUTP_SET_EMC_D20_BIT 1
#define P2_OUTP_SET_EMC_D19_MASK 0x1
#define P2_OUTP_SET_EMC_D19 0x1
#define P2_OUTP_SET_EMC_D19_BIT 0

#define P2_OUTP_CLR (*(volatile unsigned *)0x40028024)
#define P2_OUTP_CLR_EMC_D31_MASK 0x1000
#define P2_OUTP_CLR_EMC_D31 0x1000
#define P2_OUTP_CLR_EMC_D31_BIT 12
#define P2_OUTP_CLR_EMC_D30_MASK 0x800
#define P2_OUTP_CLR_EMC_D30 0x800
#define P2_OUTP_CLR_EMC_D30_BIT 11
#define P2_OUTP_CLR_EMC_D29_MASK 0x400
#define P2_OUTP_CLR_EMC_D29 0x400
#define P2_OUTP_CLR_EMC_D29_BIT 10
#define P2_OUTP_CLR_EMC_D28_MASK 0x200
#define P2_OUTP_CLR_EMC_D28 0x200
#define P2_OUTP_CLR_EMC_D28_BIT 9
#define P2_OUTP_CLR_EMC_D27_MASK 0x100
#define P2_OUTP_CLR_EMC_D27 0x100
#define P2_OUTP_CLR_EMC_D27_BIT 8
#define P2_OUTP_CLR_EMC_D26_MASK 0x80
#define P2_OUTP_CLR_EMC_D26 0x80
#define P2_OUTP_CLR_EMC_D26_BIT 7
#define P2_OUTP_CLR_EMC_D25_MASK 0x40
#define P2_OUTP_CLR_EMC_D25 0x40
#define P2_OUTP_CLR_EMC_D25_BIT 6
#define P2_OUTP_CLR_EMC_D24_MASK 0x20
#define P2_OUTP_CLR_EMC_D24 0x20
#define P2_OUTP_CLR_EMC_D24_BIT 5
#define P2_OUTP_CLR_EMC_D23_MASK 0x10
#define P2_OUTP_CLR_EMC_D23 0x10
#define P2_OUTP_CLR_EMC_D23_BIT 4
#define P2_OUTP_CLR_EMC_D22_MASK 0x8
#define P2_OUTP_CLR_EMC_D22 0x8
#define P2_OUTP_CLR_EMC_D22_BIT 3
#define P2_OUTP_CLR_EMC_D21_MASK 0x4
#define P2_OUTP_CLR_EMC_D21 0x4
#define P2_OUTP_CLR_EMC_D21_BIT 2
#define P2_OUTP_CLR_EMC_D20_MASK 0x2
#define P2_OUTP_CLR_EMC_D20 0x2
#define P2_OUTP_CLR_EMC_D20_BIT 1
#define P2_OUTP_CLR_EMC_D19_MASK 0x1
#define P2_OUTP_CLR_EMC_D19 0x1
#define P2_OUTP_CLR_EMC_D19_BIT 0

#define P2_MUX_SET (*(volatile unsigned *)0x40028028)
#define P2_MUX_SET_KEY_ROW6_MASK 0x1
#define P2_MUX_SET_KEY_ROW6 0x1
#define P2_MUX_SET_KEY_ROW6_BIT 0
#define P2_MUX_SET_KEY_ROW7_MASK 0x2
#define P2_MUX_SET_KEY_ROW7 0x2
#define P2_MUX_SET_KEY_ROW7_BIT 1
#define P2_MUX_SET_U4_TX_MASK 0x4
#define P2_MUX_SET_U4_TX 0x4
#define P2_MUX_SET_U4_TX_BIT 2
#define P2_MUX_SET_EMC_D_SEL_MASK 0x8
#define P2_MUX_SET_EMC_D_SEL 0x8
#define P2_MUX_SET_EMC_D_SEL_BIT 3
#define P2_MUX_SET_SSEL1_MASK 0x10
#define P2_MUX_SET_SSEL1 0x10
#define P2_MUX_SET_SSEL1_BIT 4
#define P2_MUX_SET_SSEL0_MASK 0x20
#define P2_MUX_SET_SSEL0 0x20
#define P2_MUX_SET_SSEL0_BIT 5

#define P2_MUX_CLR (*(volatile unsigned *)0x4002802C)
#define P2_MUX_CLR_GPIO_02_MASK 0x1
#define P2_MUX_CLR_GPIO_02 0x1
#define P2_MUX_CLR_GPIO_02_BIT 0
#define P2_MUX_CLR_GPIO_03_MASK 0x2
#define P2_MUX_CLR_GPIO_03 0x2
#define P2_MUX_CLR_GPIO_03_BIT 1
#define P2_MUX_CLR_GPO_21_MASK 0x4
#define P2_MUX_CLR_GPO_21 0x4
#define P2_MUX_CLR_GPO_21_BIT 2
#define P2_MUX_CLR_GPIO_MASK 0x8
#define P2_MUX_CLR_GPIO 0x8
#define P2_MUX_CLR_GPIO_BIT 3
#define P2_MUX_CLR_GPIO_4_MASK 0x10
#define P2_MUX_CLR_GPIO_4 0x10
#define P2_MUX_CLR_GPIO_4_BIT 4
#define P2_MUX_CLR_GPIO_5_MASK 0x20
#define P2_MUX_CLR_GPIO_5 0x20
#define P2_MUX_CLR_GPIO_5_BIT 5

#define P2_MUX_STATE (*(volatile unsigned *)0x40028030)
#define P2_MUX_STATE__0_MASK 0x1
#define P2_MUX_STATE__0 0x1
#define P2_MUX_STATE__0_BIT 0
#define P2_MUX_STATE__1_MASK 0x2
#define P2_MUX_STATE__1 0x2
#define P2_MUX_STATE__1_BIT 1
#define P2_MUX_STATE__2_MASK 0x4
#define P2_MUX_STATE__2 0x4
#define P2_MUX_STATE__2_BIT 2
#define P2_MUX_STATE__3_MASK 0x8
#define P2_MUX_STATE__3 0x8
#define P2_MUX_STATE__3_BIT 3
#define P2_MUX_STATE__4_MASK 0x10
#define P2_MUX_STATE__4 0x10
#define P2_MUX_STATE__4_BIT 4
#define P2_MUX_STATE__5_MASK 0x20
#define P2_MUX_STATE__5 0x20
#define P2_MUX_STATE__5_BIT 5

#define P0_INP_STATE (*(volatile unsigned *)0x40028040)
#define P0_INP_STATE_P0_0_MASK 0x1
#define P0_INP_STATE_P0_0 0x1
#define P0_INP_STATE_P0_0_BIT 0
#define P0_INP_STATE_P0_1_MASK 0x2
#define P0_INP_STATE_P0_1 0x2
#define P0_INP_STATE_P0_1_BIT 1
#define P0_INP_STATE_P0_2_MASK 0x4
#define P0_INP_STATE_P0_2 0x4
#define P0_INP_STATE_P0_2_BIT 2
#define P0_INP_STATE_P0_3_MASK 0x8
#define P0_INP_STATE_P0_3 0x8
#define P0_INP_STATE_P0_3_BIT 3
#define P0_INP_STATE_P0_4_MASK 0x10
#define P0_INP_STATE_P0_4 0x10
#define P0_INP_STATE_P0_4_BIT 4
#define P0_INP_STATE_P0_5_MASK 0x20
#define P0_INP_STATE_P0_5 0x20
#define P0_INP_STATE_P0_5_BIT 5
#define P0_INP_STATE_P0_6_MASK 0x40
#define P0_INP_STATE_P0_6 0x40
#define P0_INP_STATE_P0_6_BIT 6
#define P0_INP_STATE_P0_7_MASK 0x80
#define P0_INP_STATE_P0_7 0x80
#define P0_INP_STATE_P0_7_BIT 7

#define P0_OUTP_SET (*(volatile unsigned *)0x40028044)
#define P0_OUTP_SET_P0_0_MASK 0x1
#define P0_OUTP_SET_P0_0 0x1
#define P0_OUTP_SET_P0_0_BIT 0
#define P0_OUTP_SET_P0_1_MASK 0x2
#define P0_OUTP_SET_P0_1 0x2
#define P0_OUTP_SET_P0_1_BIT 1
#define P0_OUTP_SET_P0_2_MASK 0x4
#define P0_OUTP_SET_P0_2 0x4
#define P0_OUTP_SET_P0_2_BIT 2
#define P0_OUTP_SET_P0_3_MASK 0x8
#define P0_OUTP_SET_P0_3 0x8
#define P0_OUTP_SET_P0_3_BIT 3
#define P0_OUTP_SET_P0_4_MASK 0x10
#define P0_OUTP_SET_P0_4 0x10
#define P0_OUTP_SET_P0_4_BIT 4
#define P0_OUTP_SET_P0_5_MASK 0x20
#define P0_OUTP_SET_P0_5 0x20
#define P0_OUTP_SET_P0_5_BIT 5
#define P0_OUTP_SET_P0_6_MASK 0x40
#define P0_OUTP_SET_P0_6 0x40
#define P0_OUTP_SET_P0_6_BIT 6
#define P0_OUTP_SET_P0_7_MASK 0x80
#define P0_OUTP_SET_P0_7 0x80
#define P0_OUTP_SET_P0_7_BIT 7

#define P0_OUTP_CLR (*(volatile unsigned *)0x40028048)
#define P0_OUTP_CLR_P0_0_MASK 0x1
#define P0_OUTP_CLR_P0_0 0x1
#define P0_OUTP_CLR_P0_0_BIT 0
#define P0_OUTP_CLR_P0_1_MASK 0x2
#define P0_OUTP_CLR_P0_1 0x2
#define P0_OUTP_CLR_P0_1_BIT 1
#define P0_OUTP_CLR_P0_2_MASK 0x4
#define P0_OUTP_CLR_P0_2 0x4
#define P0_OUTP_CLR_P0_2_BIT 2
#define P0_OUTP_CLR_P0_3_MASK 0x8
#define P0_OUTP_CLR_P0_3 0x8
#define P0_OUTP_CLR_P0_3_BIT 3
#define P0_OUTP_CLR_P0_4_MASK 0x10
#define P0_OUTP_CLR_P0_4 0x10
#define P0_OUTP_CLR_P0_4_BIT 4
#define P0_OUTP_CLR_P0_5_MASK 0x20
#define P0_OUTP_CLR_P0_5 0x20
#define P0_OUTP_CLR_P0_5_BIT 5
#define P0_OUTP_CLR_P0_6_MASK 0x40
#define P0_OUTP_CLR_P0_6 0x40
#define P0_OUTP_CLR_P0_6_BIT 6
#define P0_OUTP_CLR_P0_7_MASK 0x80
#define P0_OUTP_CLR_P0_7 0x80
#define P0_OUTP_CLR_P0_7_BIT 7

#define P0_OUTP_STATE (*(volatile unsigned *)0x4002804C)
#define P0_OUTP_STATE_P0_0_MASK 0x1
#define P0_OUTP_STATE_P0_0 0x1
#define P0_OUTP_STATE_P0_0_BIT 0
#define P0_OUTP_STATE_P0_1_MASK 0x2
#define P0_OUTP_STATE_P0_1 0x2
#define P0_OUTP_STATE_P0_1_BIT 1
#define P0_OUTP_STATE_P0_2_MASK 0x4
#define P0_OUTP_STATE_P0_2 0x4
#define P0_OUTP_STATE_P0_2_BIT 2
#define P0_OUTP_STATE_P0_3_MASK 0x8
#define P0_OUTP_STATE_P0_3 0x8
#define P0_OUTP_STATE_P0_3_BIT 3
#define P0_OUTP_STATE_P0_4_MASK 0x10
#define P0_OUTP_STATE_P0_4 0x10
#define P0_OUTP_STATE_P0_4_BIT 4
#define P0_OUTP_STATE_P0_5_MASK 0x20
#define P0_OUTP_STATE_P0_5 0x20
#define P0_OUTP_STATE_P0_5_BIT 5
#define P0_OUTP_STATE_P0_6_MASK 0x40
#define P0_OUTP_STATE_P0_6 0x40
#define P0_OUTP_STATE_P0_6_BIT 6
#define P0_OUTP_STATE_P0_7_MASK 0x80
#define P0_OUTP_STATE_P0_7 0x80
#define P0_OUTP_STATE_P0_7_BIT 7

#define P0_DIR_SET (*(volatile unsigned *)0x40028050)
#define P0_DIR_SET_P0_0_MASK 0x1
#define P0_DIR_SET_P0_0 0x1
#define P0_DIR_SET_P0_0_BIT 0
#define P0_DIR_SET_P0_1_MASK 0x2
#define P0_DIR_SET_P0_1 0x2
#define P0_DIR_SET_P0_1_BIT 1
#define P0_DIR_SET_P0_2_MASK 0x4
#define P0_DIR_SET_P0_2 0x4
#define P0_DIR_SET_P0_2_BIT 2
#define P0_DIR_SET_P0_3_MASK 0x8
#define P0_DIR_SET_P0_3 0x8
#define P0_DIR_SET_P0_3_BIT 3
#define P0_DIR_SET_P0_4_MASK 0x10
#define P0_DIR_SET_P0_4 0x10
#define P0_DIR_SET_P0_4_BIT 4
#define P0_DIR_SET_P0_5_MASK 0x20
#define P0_DIR_SET_P0_5 0x20
#define P0_DIR_SET_P0_5_BIT 5
#define P0_DIR_SET_P0_6_MASK 0x40
#define P0_DIR_SET_P0_6 0x40
#define P0_DIR_SET_P0_6_BIT 6
#define P0_DIR_SET_P0_7_MASK 0x80
#define P0_DIR_SET_P0_7 0x80
#define P0_DIR_SET_P0_7_BIT 7

#define P0_DIR_CLR (*(volatile unsigned *)0x40028054)
#define P0_DIR_CLR_P0_0_MASK 0x1
#define P0_DIR_CLR_P0_0 0x1
#define P0_DIR_CLR_P0_0_BIT 0
#define P0_DIR_CLR_P0_1_MASK 0x2
#define P0_DIR_CLR_P0_1 0x2
#define P0_DIR_CLR_P0_1_BIT 1
#define P0_DIR_CLR_P0_2_MASK 0x4
#define P0_DIR_CLR_P0_2 0x4
#define P0_DIR_CLR_P0_2_BIT 2
#define P0_DIR_CLR_P0_3_MASK 0x8
#define P0_DIR_CLR_P0_3 0x8
#define P0_DIR_CLR_P0_3_BIT 3
#define P0_DIR_CLR_P0_4_MASK 0x10
#define P0_DIR_CLR_P0_4 0x10
#define P0_DIR_CLR_P0_4_BIT 4
#define P0_DIR_CLR_P0_5_MASK 0x20
#define P0_DIR_CLR_P0_5 0x20
#define P0_DIR_CLR_P0_5_BIT 5
#define P0_DIR_CLR_P0_6_MASK 0x40
#define P0_DIR_CLR_P0_6 0x40
#define P0_DIR_CLR_P0_6_BIT 6
#define P0_DIR_CLR_P0_7_MASK 0x80
#define P0_DIR_CLR_P0_7 0x80
#define P0_DIR_CLR_P0_7_BIT 7

#define P0_DIR_STATE (*(volatile unsigned *)0x40028058)
#define P0_DIR_STATE_P0_0_MASK 0x1
#define P0_DIR_STATE_P0_0 0x1
#define P0_DIR_STATE_P0_0_BIT 0
#define P0_DIR_STATE_P0_1_MASK 0x2
#define P0_DIR_STATE_P0_1 0x2
#define P0_DIR_STATE_P0_1_BIT 1
#define P0_DIR_STATE_P0_2_MASK 0x4
#define P0_DIR_STATE_P0_2 0x4
#define P0_DIR_STATE_P0_2_BIT 2
#define P0_DIR_STATE_P0_3_MASK 0x8
#define P0_DIR_STATE_P0_3 0x8
#define P0_DIR_STATE_P0_3_BIT 3
#define P0_DIR_STATE_P0_4_MASK 0x10
#define P0_DIR_STATE_P0_4 0x10
#define P0_DIR_STATE_P0_4_BIT 4
#define P0_DIR_STATE_P0_5_MASK 0x20
#define P0_DIR_STATE_P0_5 0x20
#define P0_DIR_STATE_P0_5_BIT 5
#define P0_DIR_STATE_P0_6_MASK 0x40
#define P0_DIR_STATE_P0_6 0x40
#define P0_DIR_STATE_P0_6_BIT 6
#define P0_DIR_STATE_P0_7_MASK 0x80
#define P0_DIR_STATE_P0_7 0x80
#define P0_DIR_STATE_P0_7_BIT 7

#define P1_INP_STATE (*(volatile unsigned *)0x40028060)
#define P1_INP_STATE_P0_0_MASK 0x1
#define P1_INP_STATE_P0_0 0x1
#define P1_INP_STATE_P0_0_BIT 0
#define P1_INP_STATE_P0_1_MASK 0x2
#define P1_INP_STATE_P0_1 0x2
#define P1_INP_STATE_P0_1_BIT 1
#define P1_INP_STATE_P0_2_MASK 0x4
#define P1_INP_STATE_P0_2 0x4
#define P1_INP_STATE_P0_2_BIT 2
#define P1_INP_STATE_P0_3_MASK 0x8
#define P1_INP_STATE_P0_3 0x8
#define P1_INP_STATE_P0_3_BIT 3
#define P1_INP_STATE_P0_4_MASK 0x10
#define P1_INP_STATE_P0_4 0x10
#define P1_INP_STATE_P0_4_BIT 4
#define P1_INP_STATE_P0_5_MASK 0x20
#define P1_INP_STATE_P0_5 0x20
#define P1_INP_STATE_P0_5_BIT 5
#define P1_INP_STATE_P0_6_MASK 0x40
#define P1_INP_STATE_P0_6 0x40
#define P1_INP_STATE_P0_6_BIT 6
#define P1_INP_STATE_P0_7_MASK 0x80
#define P1_INP_STATE_P0_7 0x80
#define P1_INP_STATE_P0_7_BIT 7
#define P1_INP_STATE_P0_8_MASK 0x100
#define P1_INP_STATE_P0_8 0x100
#define P1_INP_STATE_P0_8_BIT 8
#define P1_INP_STATE_P0_9_MASK 0x200
#define P1_INP_STATE_P0_9 0x200
#define P1_INP_STATE_P0_9_BIT 9
#define P1_INP_STATE_P0_10_MASK 0x400
#define P1_INP_STATE_P0_10 0x400
#define P1_INP_STATE_P0_10_BIT 10
#define P1_INP_STATE_P0_11_MASK 0x800
#define P1_INP_STATE_P0_11 0x800
#define P1_INP_STATE_P0_11_BIT 11
#define P1_INP_STATE_P0_12_MASK 0x1000
#define P1_INP_STATE_P0_12 0x1000
#define P1_INP_STATE_P0_12_BIT 12
#define P1_INP_STATE_P0_13_MASK 0x2000
#define P1_INP_STATE_P0_13 0x2000
#define P1_INP_STATE_P0_13_BIT 13
#define P1_INP_STATE_P0_14_MASK 0x4000
#define P1_INP_STATE_P0_14 0x4000
#define P1_INP_STATE_P0_14_BIT 14
#define P1_INP_STATE_P0_15_MASK 0x8000
#define P1_INP_STATE_P0_15 0x8000
#define P1_INP_STATE_P0_15_BIT 15
#define P1_INP_STATE_P0_16_MASK 0x10000
#define P1_INP_STATE_P0_16 0x10000
#define P1_INP_STATE_P0_16_BIT 16
#define P1_INP_STATE_P0_17_MASK 0x20000
#define P1_INP_STATE_P0_17 0x20000
#define P1_INP_STATE_P0_17_BIT 17
#define P1_INP_STATE_P0_18_MASK 0x40000
#define P1_INP_STATE_P0_18 0x40000
#define P1_INP_STATE_P0_18_BIT 18
#define P1_INP_STATE_P0_19_MASK 0x80000
#define P1_INP_STATE_P0_19 0x80000
#define P1_INP_STATE_P0_19_BIT 19
#define P1_INP_STATE_P0_20_MASK 0x100000
#define P1_INP_STATE_P0_20 0x100000
#define P1_INP_STATE_P0_20_BIT 20
#define P1_INP_STATE_P0_21_MASK 0x200000
#define P1_INP_STATE_P0_21 0x200000
#define P1_INP_STATE_P0_21_BIT 21
#define P1_INP_STATE_P0_22_MASK 0x400000
#define P1_INP_STATE_P0_22 0x400000
#define P1_INP_STATE_P0_22_BIT 22
#define P1_INP_STATE_P0_23_MASK 0x800000
#define P1_INP_STATE_P0_23 0x800000
#define P1_INP_STATE_P0_23_BIT 23

#define P1_OUTP_SET (*(volatile unsigned *)0x40028064)
#define P1_OUTP_SET_P0_0_MASK 0x1
#define P1_OUTP_SET_P0_0 0x1
#define P1_OUTP_SET_P0_0_BIT 0
#define P1_OUTP_SET_P0_1_MASK 0x2
#define P1_OUTP_SET_P0_1 0x2
#define P1_OUTP_SET_P0_1_BIT 1
#define P1_OUTP_SET_P0_2_MASK 0x4
#define P1_OUTP_SET_P0_2 0x4
#define P1_OUTP_SET_P0_2_BIT 2
#define P1_OUTP_SET_P0_3_MASK 0x8
#define P1_OUTP_SET_P0_3 0x8
#define P1_OUTP_SET_P0_3_BIT 3
#define P1_OUTP_SET_P0_4_MASK 0x10
#define P1_OUTP_SET_P0_4 0x10
#define P1_OUTP_SET_P0_4_BIT 4
#define P1_OUTP_SET_P0_5_MASK 0x20
#define P1_OUTP_SET_P0_5 0x20
#define P1_OUTP_SET_P0_5_BIT 5
#define P1_OUTP_SET_P0_6_MASK 0x40
#define P1_OUTP_SET_P0_6 0x40
#define P1_OUTP_SET_P0_6_BIT 6
#define P1_OUTP_SET_P0_7_MASK 0x80
#define P1_OUTP_SET_P0_7 0x80
#define P1_OUTP_SET_P0_7_BIT 7
#define P1_OUTP_SET_P0_8_MASK 0x100
#define P1_OUTP_SET_P0_8 0x100
#define P1_OUTP_SET_P0_8_BIT 8
#define P1_OUTP_SET_P0_9_MASK 0x200
#define P1_OUTP_SET_P0_9 0x200
#define P1_OUTP_SET_P0_9_BIT 9
#define P1_OUTP_SET_P0_10_MASK 0x400
#define P1_OUTP_SET_P0_10 0x400
#define P1_OUTP_SET_P0_10_BIT 10
#define P1_OUTP_SET_P0_11_MASK 0x800
#define P1_OUTP_SET_P0_11 0x800
#define P1_OUTP_SET_P0_11_BIT 11
#define P1_OUTP_SET_P0_12_MASK 0x1000
#define P1_OUTP_SET_P0_12 0x1000
#define P1_OUTP_SET_P0_12_BIT 12
#define P1_OUTP_SET_P0_13_MASK 0x2000
#define P1_OUTP_SET_P0_13 0x2000
#define P1_OUTP_SET_P0_13_BIT 13
#define P1_OUTP_SET_P0_14_MASK 0x4000
#define P1_OUTP_SET_P0_14 0x4000
#define P1_OUTP_SET_P0_14_BIT 14
#define P1_OUTP_SET_P0_15_MASK 0x8000
#define P1_OUTP_SET_P0_15 0x8000
#define P1_OUTP_SET_P0_15_BIT 15
#define P1_OUTP_SET_P0_16_MASK 0x10000
#define P1_OUTP_SET_P0_16 0x10000
#define P1_OUTP_SET_P0_16_BIT 16
#define P1_OUTP_SET_P0_17_MASK 0x20000
#define P1_OUTP_SET_P0_17 0x20000
#define P1_OUTP_SET_P0_17_BIT 17
#define P1_OUTP_SET_P0_18_MASK 0x40000
#define P1_OUTP_SET_P0_18 0x40000
#define P1_OUTP_SET_P0_18_BIT 18
#define P1_OUTP_SET_P0_19_MASK 0x80000
#define P1_OUTP_SET_P0_19 0x80000
#define P1_OUTP_SET_P0_19_BIT 19
#define P1_OUTP_SET_P0_20_MASK 0x100000
#define P1_OUTP_SET_P0_20 0x100000
#define P1_OUTP_SET_P0_20_BIT 20
#define P1_OUTP_SET_P0_21_MASK 0x200000
#define P1_OUTP_SET_P0_21 0x200000
#define P1_OUTP_SET_P0_21_BIT 21
#define P1_OUTP_SET_P0_22_MASK 0x400000
#define P1_OUTP_SET_P0_22 0x400000
#define P1_OUTP_SET_P0_22_BIT 22
#define P1_OUTP_SET_P0_23_MASK 0x800000
#define P1_OUTP_SET_P0_23 0x800000
#define P1_OUTP_SET_P0_23_BIT 23

#define P1_OUTP_CLR (*(volatile unsigned *)0x40028068)
#define P1_OUTP_CLR_P0_0_MASK 0x1
#define P1_OUTP_CLR_P0_0 0x1
#define P1_OUTP_CLR_P0_0_BIT 0
#define P1_OUTP_CLR_P0_1_MASK 0x2
#define P1_OUTP_CLR_P0_1 0x2
#define P1_OUTP_CLR_P0_1_BIT 1
#define P1_OUTP_CLR_P0_2_MASK 0x4
#define P1_OUTP_CLR_P0_2 0x4
#define P1_OUTP_CLR_P0_2_BIT 2
#define P1_OUTP_CLR_P0_3_MASK 0x8
#define P1_OUTP_CLR_P0_3 0x8
#define P1_OUTP_CLR_P0_3_BIT 3
#define P1_OUTP_CLR_P0_4_MASK 0x10
#define P1_OUTP_CLR_P0_4 0x10
#define P1_OUTP_CLR_P0_4_BIT 4
#define P1_OUTP_CLR_P0_5_MASK 0x20
#define P1_OUTP_CLR_P0_5 0x20
#define P1_OUTP_CLR_P0_5_BIT 5
#define P1_OUTP_CLR_P0_6_MASK 0x40
#define P1_OUTP_CLR_P0_6 0x40
#define P1_OUTP_CLR_P0_6_BIT 6
#define P1_OUTP_CLR_P0_7_MASK 0x80
#define P1_OUTP_CLR_P0_7 0x80
#define P1_OUTP_CLR_P0_7_BIT 7
#define P1_OUTP_CLR_P0_8_MASK 0x100
#define P1_OUTP_CLR_P0_8 0x100
#define P1_OUTP_CLR_P0_8_BIT 8
#define P1_OUTP_CLR_P0_9_MASK 0x200
#define P1_OUTP_CLR_P0_9 0x200
#define P1_OUTP_CLR_P0_9_BIT 9
#define P1_OUTP_CLR_P0_10_MASK 0x400
#define P1_OUTP_CLR_P0_10 0x400
#define P1_OUTP_CLR_P0_10_BIT 10
#define P1_OUTP_CLR_P0_11_MASK 0x800
#define P1_OUTP_CLR_P0_11 0x800
#define P1_OUTP_CLR_P0_11_BIT 11
#define P1_OUTP_CLR_P0_12_MASK 0x1000
#define P1_OUTP_CLR_P0_12 0x1000
#define P1_OUTP_CLR_P0_12_BIT 12
#define P1_OUTP_CLR_P0_13_MASK 0x2000
#define P1_OUTP_CLR_P0_13 0x2000
#define P1_OUTP_CLR_P0_13_BIT 13
#define P1_OUTP_CLR_P0_14_MASK 0x4000
#define P1_OUTP_CLR_P0_14 0x4000
#define P1_OUTP_CLR_P0_14_BIT 14
#define P1_OUTP_CLR_P0_15_MASK 0x8000
#define P1_OUTP_CLR_P0_15 0x8000
#define P1_OUTP_CLR_P0_15_BIT 15
#define P1_OUTP_CLR_P0_16_MASK 0x10000
#define P1_OUTP_CLR_P0_16 0x10000
#define P1_OUTP_CLR_P0_16_BIT 16
#define P1_OUTP_CLR_P0_17_MASK 0x20000
#define P1_OUTP_CLR_P0_17 0x20000
#define P1_OUTP_CLR_P0_17_BIT 17
#define P1_OUTP_CLR_P0_18_MASK 0x40000
#define P1_OUTP_CLR_P0_18 0x40000
#define P1_OUTP_CLR_P0_18_BIT 18
#define P1_OUTP_CLR_P0_19_MASK 0x80000
#define P1_OUTP_CLR_P0_19 0x80000
#define P1_OUTP_CLR_P0_19_BIT 19
#define P1_OUTP_CLR_P0_20_MASK 0x100000
#define P1_OUTP_CLR_P0_20 0x100000
#define P1_OUTP_CLR_P0_20_BIT 20
#define P1_OUTP_CLR_P0_21_MASK 0x200000
#define P1_OUTP_CLR_P0_21 0x200000
#define P1_OUTP_CLR_P0_21_BIT 21
#define P1_OUTP_CLR_P0_22_MASK 0x400000
#define P1_OUTP_CLR_P0_22 0x400000
#define P1_OUTP_CLR_P0_22_BIT 22
#define P1_OUTP_CLR_P0_23_MASK 0x800000
#define P1_OUTP_CLR_P0_23 0x800000
#define P1_OUTP_CLR_P0_23_BIT 23

#define P1_OUTP_STATE (*(volatile unsigned *)0x4002806C)
#define P1_OUTP_STATE_P0_0_MASK 0x1
#define P1_OUTP_STATE_P0_0 0x1
#define P1_OUTP_STATE_P0_0_BIT 0
#define P1_OUTP_STATE_P0_1_MASK 0x2
#define P1_OUTP_STATE_P0_1 0x2
#define P1_OUTP_STATE_P0_1_BIT 1
#define P1_OUTP_STATE_P0_2_MASK 0x4
#define P1_OUTP_STATE_P0_2 0x4
#define P1_OUTP_STATE_P0_2_BIT 2
#define P1_OUTP_STATE_P0_3_MASK 0x8
#define P1_OUTP_STATE_P0_3 0x8
#define P1_OUTP_STATE_P0_3_BIT 3
#define P1_OUTP_STATE_P0_4_MASK 0x10
#define P1_OUTP_STATE_P0_4 0x10
#define P1_OUTP_STATE_P0_4_BIT 4
#define P1_OUTP_STATE_P0_5_MASK 0x20
#define P1_OUTP_STATE_P0_5 0x20
#define P1_OUTP_STATE_P0_5_BIT 5
#define P1_OUTP_STATE_P0_6_MASK 0x40
#define P1_OUTP_STATE_P0_6 0x40
#define P1_OUTP_STATE_P0_6_BIT 6
#define P1_OUTP_STATE_P0_7_MASK 0x80
#define P1_OUTP_STATE_P0_7 0x80
#define P1_OUTP_STATE_P0_7_BIT 7
#define P1_OUTP_STATE_P0_8_MASK 0x100
#define P1_OUTP_STATE_P0_8 0x100
#define P1_OUTP_STATE_P0_8_BIT 8
#define P1_OUTP_STATE_P0_9_MASK 0x200
#define P1_OUTP_STATE_P0_9 0x200
#define P1_OUTP_STATE_P0_9_BIT 9
#define P1_OUTP_STATE_P0_10_MASK 0x400
#define P1_OUTP_STATE_P0_10 0x400
#define P1_OUTP_STATE_P0_10_BIT 10
#define P1_OUTP_STATE_P0_11_MASK 0x800
#define P1_OUTP_STATE_P0_11 0x800
#define P1_OUTP_STATE_P0_11_BIT 11
#define P1_OUTP_STATE_P0_12_MASK 0x1000
#define P1_OUTP_STATE_P0_12 0x1000
#define P1_OUTP_STATE_P0_12_BIT 12
#define P1_OUTP_STATE_P0_13_MASK 0x2000
#define P1_OUTP_STATE_P0_13 0x2000
#define P1_OUTP_STATE_P0_13_BIT 13
#define P1_OUTP_STATE_P0_14_MASK 0x4000
#define P1_OUTP_STATE_P0_14 0x4000
#define P1_OUTP_STATE_P0_14_BIT 14
#define P1_OUTP_STATE_P0_15_MASK 0x8000
#define P1_OUTP_STATE_P0_15 0x8000
#define P1_OUTP_STATE_P0_15_BIT 15
#define P1_OUTP_STATE_P0_16_MASK 0x10000
#define P1_OUTP_STATE_P0_16 0x10000
#define P1_OUTP_STATE_P0_16_BIT 16
#define P1_OUTP_STATE_P0_17_MASK 0x20000
#define P1_OUTP_STATE_P0_17 0x20000
#define P1_OUTP_STATE_P0_17_BIT 17
#define P1_OUTP_STATE_P0_18_MASK 0x40000
#define P1_OUTP_STATE_P0_18 0x40000
#define P1_OUTP_STATE_P0_18_BIT 18
#define P1_OUTP_STATE_P0_19_MASK 0x80000
#define P1_OUTP_STATE_P0_19 0x80000
#define P1_OUTP_STATE_P0_19_BIT 19
#define P1_OUTP_STATE_P0_20_MASK 0x100000
#define P1_OUTP_STATE_P0_20 0x100000
#define P1_OUTP_STATE_P0_20_BIT 20
#define P1_OUTP_STATE_P0_21_MASK 0x200000
#define P1_OUTP_STATE_P0_21 0x200000
#define P1_OUTP_STATE_P0_21_BIT 21
#define P1_OUTP_STATE_P0_22_MASK 0x400000
#define P1_OUTP_STATE_P0_22 0x400000
#define P1_OUTP_STATE_P0_22_BIT 22
#define P1_OUTP_STATE_P0_23_MASK 0x800000
#define P1_OUTP_STATE_P0_23 0x800000
#define P1_OUTP_STATE_P0_23_BIT 23

#define P1_DIR_SET (*(volatile unsigned *)0x40028070)
#define P1_DIR_SET_P0_0_MASK 0x1
#define P1_DIR_SET_P0_0 0x1
#define P1_DIR_SET_P0_0_BIT 0
#define P1_DIR_SET_P0_1_MASK 0x2
#define P1_DIR_SET_P0_1 0x2
#define P1_DIR_SET_P0_1_BIT 1
#define P1_DIR_SET_P0_2_MASK 0x4
#define P1_DIR_SET_P0_2 0x4
#define P1_DIR_SET_P0_2_BIT 2
#define P1_DIR_SET_P0_3_MASK 0x8
#define P1_DIR_SET_P0_3 0x8
#define P1_DIR_SET_P0_3_BIT 3
#define P1_DIR_SET_P0_4_MASK 0x10
#define P1_DIR_SET_P0_4 0x10
#define P1_DIR_SET_P0_4_BIT 4
#define P1_DIR_SET_P0_5_MASK 0x20
#define P1_DIR_SET_P0_5 0x20
#define P1_DIR_SET_P0_5_BIT 5
#define P1_DIR_SET_P0_6_MASK 0x40
#define P1_DIR_SET_P0_6 0x40
#define P1_DIR_SET_P0_6_BIT 6
#define P1_DIR_SET_P0_7_MASK 0x80
#define P1_DIR_SET_P0_7 0x80
#define P1_DIR_SET_P0_7_BIT 7
#define P1_DIR_SET_P0_8_MASK 0x100
#define P1_DIR_SET_P0_8 0x100
#define P1_DIR_SET_P0_8_BIT 8
#define P1_DIR_SET_P0_9_MASK 0x200
#define P1_DIR_SET_P0_9 0x200
#define P1_DIR_SET_P0_9_BIT 9
#define P1_DIR_SET_P0_10_MASK 0x400
#define P1_DIR_SET_P0_10 0x400
#define P1_DIR_SET_P0_10_BIT 10
#define P1_DIR_SET_P0_11_MASK 0x800
#define P1_DIR_SET_P0_11 0x800
#define P1_DIR_SET_P0_11_BIT 11
#define P1_DIR_SET_P0_12_MASK 0x1000
#define P1_DIR_SET_P0_12 0x1000
#define P1_DIR_SET_P0_12_BIT 12
#define P1_DIR_SET_P0_13_MASK 0x2000
#define P1_DIR_SET_P0_13 0x2000
#define P1_DIR_SET_P0_13_BIT 13
#define P1_DIR_SET_P0_14_MASK 0x4000
#define P1_DIR_SET_P0_14 0x4000
#define P1_DIR_SET_P0_14_BIT 14
#define P1_DIR_SET_P0_15_MASK 0x8000
#define P1_DIR_SET_P0_15 0x8000
#define P1_DIR_SET_P0_15_BIT 15
#define P1_DIR_SET_P0_16_MASK 0x10000
#define P1_DIR_SET_P0_16 0x10000
#define P1_DIR_SET_P0_16_BIT 16
#define P1_DIR_SET_P0_17_MASK 0x20000
#define P1_DIR_SET_P0_17 0x20000
#define P1_DIR_SET_P0_17_BIT 17
#define P1_DIR_SET_P0_18_MASK 0x40000
#define P1_DIR_SET_P0_18 0x40000
#define P1_DIR_SET_P0_18_BIT 18
#define P1_DIR_SET_P0_19_MASK 0x80000
#define P1_DIR_SET_P0_19 0x80000
#define P1_DIR_SET_P0_19_BIT 19
#define P1_DIR_SET_P0_20_MASK 0x100000
#define P1_DIR_SET_P0_20 0x100000
#define P1_DIR_SET_P0_20_BIT 20
#define P1_DIR_SET_P0_21_MASK 0x200000
#define P1_DIR_SET_P0_21 0x200000
#define P1_DIR_SET_P0_21_BIT 21
#define P1_DIR_SET_P0_22_MASK 0x400000
#define P1_DIR_SET_P0_22 0x400000
#define P1_DIR_SET_P0_22_BIT 22
#define P1_DIR_SET_P0_23_MASK 0x800000
#define P1_DIR_SET_P0_23 0x800000
#define P1_DIR_SET_P0_23_BIT 23

#define P1_DIR_CLR (*(volatile unsigned *)0x40028074)
#define P1_DIR_CLR_P0_0_MASK 0x1
#define P1_DIR_CLR_P0_0 0x1
#define P1_DIR_CLR_P0_0_BIT 0
#define P1_DIR_CLR_P0_1_MASK 0x2
#define P1_DIR_CLR_P0_1 0x2
#define P1_DIR_CLR_P0_1_BIT 1
#define P1_DIR_CLR_P0_2_MASK 0x4
#define P1_DIR_CLR_P0_2 0x4
#define P1_DIR_CLR_P0_2_BIT 2
#define P1_DIR_CLR_P0_3_MASK 0x8
#define P1_DIR_CLR_P0_3 0x8
#define P1_DIR_CLR_P0_3_BIT 3
#define P1_DIR_CLR_P0_4_MASK 0x10
#define P1_DIR_CLR_P0_4 0x10
#define P1_DIR_CLR_P0_4_BIT 4
#define P1_DIR_CLR_P0_5_MASK 0x20
#define P1_DIR_CLR_P0_5 0x20
#define P1_DIR_CLR_P0_5_BIT 5
#define P1_DIR_CLR_P0_6_MASK 0x40
#define P1_DIR_CLR_P0_6 0x40
#define P1_DIR_CLR_P0_6_BIT 6
#define P1_DIR_CLR_P0_7_MASK 0x80
#define P1_DIR_CLR_P0_7 0x80
#define P1_DIR_CLR_P0_7_BIT 7
#define P1_DIR_CLR_P0_8_MASK 0x100
#define P1_DIR_CLR_P0_8 0x100
#define P1_DIR_CLR_P0_8_BIT 8
#define P1_DIR_CLR_P0_9_MASK 0x200
#define P1_DIR_CLR_P0_9 0x200
#define P1_DIR_CLR_P0_9_BIT 9
#define P1_DIR_CLR_P0_10_MASK 0x400
#define P1_DIR_CLR_P0_10 0x400
#define P1_DIR_CLR_P0_10_BIT 10
#define P1_DIR_CLR_P0_11_MASK 0x800
#define P1_DIR_CLR_P0_11 0x800
#define P1_DIR_CLR_P0_11_BIT 11
#define P1_DIR_CLR_P0_12_MASK 0x1000
#define P1_DIR_CLR_P0_12 0x1000
#define P1_DIR_CLR_P0_12_BIT 12
#define P1_DIR_CLR_P0_13_MASK 0x2000
#define P1_DIR_CLR_P0_13 0x2000
#define P1_DIR_CLR_P0_13_BIT 13
#define P1_DIR_CLR_P0_14_MASK 0x4000
#define P1_DIR_CLR_P0_14 0x4000
#define P1_DIR_CLR_P0_14_BIT 14
#define P1_DIR_CLR_P0_15_MASK 0x8000
#define P1_DIR_CLR_P0_15 0x8000
#define P1_DIR_CLR_P0_15_BIT 15
#define P1_DIR_CLR_P0_16_MASK 0x10000
#define P1_DIR_CLR_P0_16 0x10000
#define P1_DIR_CLR_P0_16_BIT 16
#define P1_DIR_CLR_P0_17_MASK 0x20000
#define P1_DIR_CLR_P0_17 0x20000
#define P1_DIR_CLR_P0_17_BIT 17
#define P1_DIR_CLR_P0_18_MASK 0x40000
#define P1_DIR_CLR_P0_18 0x40000
#define P1_DIR_CLR_P0_18_BIT 18
#define P1_DIR_CLR_P0_19_MASK 0x80000
#define P1_DIR_CLR_P0_19 0x80000
#define P1_DIR_CLR_P0_19_BIT 19
#define P1_DIR_CLR_P0_20_MASK 0x100000
#define P1_DIR_CLR_P0_20 0x100000
#define P1_DIR_CLR_P0_20_BIT 20
#define P1_DIR_CLR_P0_21_MASK 0x200000
#define P1_DIR_CLR_P0_21 0x200000
#define P1_DIR_CLR_P0_21_BIT 21
#define P1_DIR_CLR_P0_22_MASK 0x400000
#define P1_DIR_CLR_P0_22 0x400000
#define P1_DIR_CLR_P0_22_BIT 22
#define P1_DIR_CLR_P0_23_MASK 0x800000
#define P1_DIR_CLR_P0_23 0x800000
#define P1_DIR_CLR_P0_23_BIT 23

#define P1_DIR_STATE (*(volatile unsigned *)0x40028078)
#define P1_DIR_STATE_P0_0_MASK 0x1
#define P1_DIR_STATE_P0_0 0x1
#define P1_DIR_STATE_P0_0_BIT 0
#define P1_DIR_STATE_P0_1_MASK 0x2
#define P1_DIR_STATE_P0_1 0x2
#define P1_DIR_STATE_P0_1_BIT 1
#define P1_DIR_STATE_P0_2_MASK 0x4
#define P1_DIR_STATE_P0_2 0x4
#define P1_DIR_STATE_P0_2_BIT 2
#define P1_DIR_STATE_P0_3_MASK 0x8
#define P1_DIR_STATE_P0_3 0x8
#define P1_DIR_STATE_P0_3_BIT 3
#define P1_DIR_STATE_P0_4_MASK 0x10
#define P1_DIR_STATE_P0_4 0x10
#define P1_DIR_STATE_P0_4_BIT 4
#define P1_DIR_STATE_P0_5_MASK 0x20
#define P1_DIR_STATE_P0_5 0x20
#define P1_DIR_STATE_P0_5_BIT 5
#define P1_DIR_STATE_P0_6_MASK 0x40
#define P1_DIR_STATE_P0_6 0x40
#define P1_DIR_STATE_P0_6_BIT 6
#define P1_DIR_STATE_P0_7_MASK 0x80
#define P1_DIR_STATE_P0_7 0x80
#define P1_DIR_STATE_P0_7_BIT 7
#define P1_DIR_STATE_P0_8_MASK 0x100
#define P1_DIR_STATE_P0_8 0x100
#define P1_DIR_STATE_P0_8_BIT 8
#define P1_DIR_STATE_P0_9_MASK 0x200
#define P1_DIR_STATE_P0_9 0x200
#define P1_DIR_STATE_P0_9_BIT 9
#define P1_DIR_STATE_P0_10_MASK 0x400
#define P1_DIR_STATE_P0_10 0x400
#define P1_DIR_STATE_P0_10_BIT 10
#define P1_DIR_STATE_P0_11_MASK 0x800
#define P1_DIR_STATE_P0_11 0x800
#define P1_DIR_STATE_P0_11_BIT 11
#define P1_DIR_STATE_P0_12_MASK 0x1000
#define P1_DIR_STATE_P0_12 0x1000
#define P1_DIR_STATE_P0_12_BIT 12
#define P1_DIR_STATE_P0_13_MASK 0x2000
#define P1_DIR_STATE_P0_13 0x2000
#define P1_DIR_STATE_P0_13_BIT 13
#define P1_DIR_STATE_P0_14_MASK 0x4000
#define P1_DIR_STATE_P0_14 0x4000
#define P1_DIR_STATE_P0_14_BIT 14
#define P1_DIR_STATE_P0_15_MASK 0x8000
#define P1_DIR_STATE_P0_15 0x8000
#define P1_DIR_STATE_P0_15_BIT 15
#define P1_DIR_STATE_P0_16_MASK 0x10000
#define P1_DIR_STATE_P0_16 0x10000
#define P1_DIR_STATE_P0_16_BIT 16
#define P1_DIR_STATE_P0_17_MASK 0x20000
#define P1_DIR_STATE_P0_17 0x20000
#define P1_DIR_STATE_P0_17_BIT 17
#define P1_DIR_STATE_P0_18_MASK 0x40000
#define P1_DIR_STATE_P0_18 0x40000
#define P1_DIR_STATE_P0_18_BIT 18
#define P1_DIR_STATE_P0_19_MASK 0x80000
#define P1_DIR_STATE_P0_19 0x80000
#define P1_DIR_STATE_P0_19_BIT 19
#define P1_DIR_STATE_P0_20_MASK 0x100000
#define P1_DIR_STATE_P0_20 0x100000
#define P1_DIR_STATE_P0_20_BIT 20
#define P1_DIR_STATE_P0_21_MASK 0x200000
#define P1_DIR_STATE_P0_21 0x200000
#define P1_DIR_STATE_P0_21_BIT 21
#define P1_DIR_STATE_P0_22_MASK 0x400000
#define P1_DIR_STATE_P0_22 0x400000
#define P1_DIR_STATE_P0_22_BIT 22
#define P1_DIR_STATE_P0_23_MASK 0x800000
#define P1_DIR_STATE_P0_23 0x800000
#define P1_DIR_STATE_P0_23_BIT 23

#define P_MUX_SET (*(volatile unsigned *)0x40028100)
#define P_MUX_SET_MAT3_1_MASK 0x4
#define P_MUX_SET_MAT3_1 0x4
#define P_MUX_SET_MAT3_1_BIT 2
#define P_MUX_SET_MAT3_0_MASK 0x8
#define P_MUX_SET_MAT3_0 0x8
#define P_MUX_SET_MAT3_0_BIT 3
#define P_MUX_SET_CAP3_0_MASK 0x10
#define P_MUX_SET_CAP3_0 0x10
#define P_MUX_SET_CAP3_0_BIT 4
#define P_MUX_SET_MOSI1_MASK 0x20
#define P_MUX_SET_MOSI1 0x20
#define P_MUX_SET_MOSI1_BIT 5
#define P_MUX_SET_MISO1_MASK 0x40
#define P_MUX_SET_MISO1 0x40
#define P_MUX_SET_MISO1_BIT 6
#define P_MUX_SET_SCK1_MASK 0x100
#define P_MUX_SET_SCK1 0x100
#define P_MUX_SET_SCK1_BIT 8
#define P_MUX_SET_MOSI0_MASK 0x200
#define P_MUX_SET_MOSI0 0x200
#define P_MUX_SET_MOSI0_BIT 9
#define P_MUX_SET_MISO0_MASK 0x400
#define P_MUX_SET_MISO0 0x400
#define P_MUX_SET_MISO0_BIT 10
#define P_MUX_SET_SCK0_MASK 0x1000
#define P_MUX_SET_SCK0 0x1000
#define P_MUX_SET_SCK0_BIT 12
#define P_MUX_SET_MAT1_1_MASK 0x8000
#define P_MUX_SET_MAT1_1 0x8000
#define P_MUX_SET_MAT1_1_BIT 15

#define P_MUX_CLR (*(volatile unsigned *)0x40028104)
#define P_MUX_CLR_I2S1TX_SDA_MASK 0x4
#define P_MUX_CLR_I2S1TX_SDA 0x4
#define P_MUX_CLR_I2S1TX_SDA_BIT 2
#define P_MUX_CLR_I2S1TX_CLK_MASK 0x8
#define P_MUX_CLR_I2S1TX_CLK 0x8
#define P_MUX_CLR_I2S1TX_CLK_BIT 3
#define P_MUX_CLR_I2S1TX_WS_MASK 0x10
#define P_MUX_CLR_I2S1TX_WS 0x10
#define P_MUX_CLR_I2S1TX_WS_BIT 4
#define P_MUX_CLR_SPI2_DATIO_MASK 0x20
#define P_MUX_CLR_SPI2_DATIO 0x20
#define P_MUX_CLR_SPI2_DATIO_BIT 5
#define P_MUX_CLR_SPI2_DATIN_MASK 0x40
#define P_MUX_CLR_SPI2_DATIN 0x40
#define P_MUX_CLR_SPI2_DATIN_BIT 6
#define P_MUX_CLR_SPI2_CLK_MASK 0x100
#define P_MUX_CLR_SPI2_CLK 0x100
#define P_MUX_CLR_SPI2_CLK_BIT 8
#define P_MUX_CLR_SPI1_DATIO_MASK 0x200
#define P_MUX_CLR_SPI1_DATIO 0x200
#define P_MUX_CLR_SPI1_DATIO_BIT 9
#define P_MUX_CLR_SPI1_DATIN_MASK 0x400
#define P_MUX_CLR_SPI1_DATIN 0x400
#define P_MUX_CLR_SPI1_DATIN_BIT 10
#define P_MUX_CLR_SPI1_CLK_MASK 0x1000
#define P_MUX_CLR_SPI1_CLK 0x1000
#define P_MUX_CLR_SPI1_CLK_BIT 12
#define P_MUX_CLR_MAT2_0_MASK 0x4000
#define P_MUX_CLR_MAT2_0 0x4000
#define P_MUX_CLR_MAT2_0_BIT 14
#define P_MUX_CLR_U7_TX_MASK 0x8000
#define P_MUX_CLR_U7_TX 0x8000
#define P_MUX_CLR_U7_TX_BIT 15
#define P_MUX_CLR_MAT0_3_MASK 0x20000
#define P_MUX_CLR_MAT0_3 0x20000
#define P_MUX_CLR_MAT0_3_BIT 17
#define P_MUX_CLR_MAT0_2_MASK 0x40000
#define P_MUX_CLR_MAT0_2 0x40000
#define P_MUX_CLR_MAT0_2_BIT 18
#define P_MUX_CLR_MAT0_1_MASK 0x80000
#define P_MUX_CLR_MAT0_1 0x80000
#define P_MUX_CLR_MAT0_1_BIT 19
#define P_MUX_CLR_MAT0_0_MASK 0x100000
#define P_MUX_CLR_MAT0_0 0x100000
#define P_MUX_CLR_MAT0_0_BIT 20

#define P_MUX_STATE (*(volatile unsigned *)0x40028108)
#define P_MUX_STATE__2_MASK 0x4
#define P_MUX_STATE__2 0x4
#define P_MUX_STATE__2_BIT 2
#define P_MUX_STATE__3_MASK 0x8
#define P_MUX_STATE__3 0x8
#define P_MUX_STATE__3_BIT 3
#define P_MUX_STATE__4_MASK 0x10
#define P_MUX_STATE__4 0x10
#define P_MUX_STATE__4_BIT 4
#define P_MUX_STATE__5_MASK 0x20
#define P_MUX_STATE__5 0x20
#define P_MUX_STATE__5_BIT 5
#define P_MUX_STATE__6_MASK 0x40
#define P_MUX_STATE__6 0x40
#define P_MUX_STATE__6_BIT 6
#define P_MUX_STATE__8_MASK 0x100
#define P_MUX_STATE__8 0x100
#define P_MUX_STATE__8_BIT 8
#define P_MUX_STATE__9_MASK 0x200
#define P_MUX_STATE__9 0x200
#define P_MUX_STATE__9_BIT 9
#define P_MUX_STATE__10_MASK 0x400
#define P_MUX_STATE__10 0x400
#define P_MUX_STATE__10_BIT 10
#define P_MUX_STATE__12_MASK 0x1000
#define P_MUX_STATE__12 0x1000
#define P_MUX_STATE__12_BIT 12
#define P_MUX_STATE__15_MASK 0x8000
#define P_MUX_STATE__15 0x8000
#define P_MUX_STATE__15_BIT 15

#define P3_MUX_SET (*(volatile unsigned *)0x40028110)
#define P3_MUX_SET_MAT1_0_MASK 0x4
#define P3_MUX_SET_MAT1_0 0x4
#define P3_MUX_SET_MAT1_0_BIT 2
#define P3_MUX_SET_MC2B_MASK 0x400
#define P3_MUX_SET_MC2B 0x400
#define P3_MUX_SET_MC2B_BIT 10
#define P3_MUX_SET_MC2A_MASK 0x1000
#define P3_MUX_SET_MC2A 0x1000
#define P3_MUX_SET_MC2A_BIT 12
#define P3_MUX_SET_MC1B_MASK 0x2000
#define P3_MUX_SET_MC1B 0x2000
#define P3_MUX_SET_MC1B_BIT 13
#define P3_MUX_SET_MC1A_MASK 0x8000
#define P3_MUX_SET_MC1A 0x8000
#define P3_MUX_SET_MC1A_BIT 15
#define P3_MUX_SET_MC0B_MASK 0x10000
#define P3_MUX_SET_MC0B 0x10000
#define P3_MUX_SET_MC0B_BIT 16
#define P3_MUX_SET_MC0A_MASK 0x40000
#define P3_MUX_SET_MC0A 0x40000
#define P3_MUX_SET_MC0A_BIT 18

#define P3_MUX_CLR (*(volatile unsigned *)0x40028114)
#define P3_MUX_CLR_GPO_02_MASK 0x4
#define P3_MUX_CLR_GPO_02 0x4
#define P3_MUX_CLR_GPO_02_BIT 2
#define P3_MUX_CLR_GPO_06_MASK 0x40
#define P3_MUX_CLR_GPO_06 0x40
#define P3_MUX_CLR_GPO_06_BIT 6
#define P3_MUX_CLR_GPO_08_MASK 0x100
#define P3_MUX_CLR_GPO_08 0x100
#define P3_MUX_CLR_GPO_08_BIT 8
#define P3_MUX_CLR_GPO_09_MASK 0x200
#define P3_MUX_CLR_GPO_09 0x200
#define P3_MUX_CLR_GPO_09_BIT 9
#define P3_MUX_CLR_GPO_10_MASK 0x400
#define P3_MUX_CLR_GPO_10 0x400
#define P3_MUX_CLR_GPO_10_BIT 10
#define P3_MUX_CLR_GPO_12_MASK 0x1000
#define P3_MUX_CLR_GPO_12 0x1000
#define P3_MUX_CLR_GPO_12_BIT 12
#define P3_MUX_CLR_GPO_13_MASK 0x2000
#define P3_MUX_CLR_GPO_13 0x2000
#define P3_MUX_CLR_GPO_13_BIT 13
#define P3_MUX_CLR_GPO_15_MASK 0x8000
#define P3_MUX_CLR_GPO_15 0x8000
#define P3_MUX_CLR_GPO_15_BIT 15
#define P3_MUX_CLR_GPO_16_MASK 0x10000
#define P3_MUX_CLR_GPO_16 0x10000
#define P3_MUX_CLR_GPO_16_BIT 16
#define P3_MUX_CLR_GPO_18_MASK 0x40000
#define P3_MUX_CLR_GPO_18 0x40000
#define P3_MUX_CLR_GPO_18_BIT 18

#define P3_MUX_STATE (*(volatile unsigned *)0x40028118)
#define P3_MUX_STATE__2_MASK 0x4
#define P3_MUX_STATE__2 0x4
#define P3_MUX_STATE__2_BIT 2
#define P3_MUX_STATE__10_MASK 0x400
#define P3_MUX_STATE__10 0x400
#define P3_MUX_STATE__10_BIT 10
#define P3_MUX_STATE__12_MASK 0x1000
#define P3_MUX_STATE__12 0x1000
#define P3_MUX_STATE__12_BIT 12
#define P3_MUX_STATE__13_MASK 0x2000
#define P3_MUX_STATE__13 0x2000
#define P3_MUX_STATE__13_BIT 13
#define P3_MUX_STATE__15_MASK 0x8000
#define P3_MUX_STATE__15 0x8000
#define P3_MUX_STATE__15_BIT 15
#define P3_MUX_STATE__16_MASK 0x10000
#define P3_MUX_STATE__16 0x10000
#define P3_MUX_STATE__16_BIT 16
#define P3_MUX_STATE__18_MASK 0x40000
#define P3_MUX_STATE__18 0x40000
#define P3_MUX_STATE__18_BIT 18

#define P0_MUX_SET (*(volatile unsigned *)0x40028120)
#define P0_MUX_SET_I2S1RX_CLK_MASK 0x1
#define P0_MUX_SET_I2S1RX_CLK 0x1
#define P0_MUX_SET_I2S1RX_CLK_BIT 0
#define P0_MUX_SET_I2S1RX_WS_MASK 0x2
#define P0_MUX_SET_I2S1RX_WS 0x2
#define P0_MUX_SET_I2S1RX_WS_BIT 1
#define P0_MUX_SET_I2S0RX_SDA_MASK 0x4
#define P0_MUX_SET_I2S0RX_SDA 0x4
#define P0_MUX_SET_I2S0RX_SDA_BIT 2
#define P0_MUX_SET_I2S0RX_CLK_MASK 0x8
#define P0_MUX_SET_I2S0RX_CLK 0x8
#define P0_MUX_SET_I2S0RX_CLK_BIT 3
#define P0_MUX_SET_I2S0RX_WS_MASK 0x10
#define P0_MUX_SET_I2S0RX_WS 0x10
#define P0_MUX_SET_I2S0RX_WS_BIT 4
#define P0_MUX_SET_I2S0TX_SDA_MASK 0x20
#define P0_MUX_SET_I2S0TX_SDA 0x20
#define P0_MUX_SET_I2S0TX_SDA_BIT 5
#define P0_MUX_SET_I2S0TX_CLK_MASK 0x40
#define P0_MUX_SET_I2S0TX_CLK 0x40
#define P0_MUX_SET_I2S0TX_CLK_BIT 6
#define P0_MUX_SET_I2S0TX_WS_MASK 0x80
#define P0_MUX_SET_I2S0TX_WS 0x80
#define P0_MUX_SET_I2S0TX_WS_BIT 7

#define P0_MUX_CLR (*(volatile unsigned *)0x40028124)
#define P0_MUX_CLR_P0_0_MASK 0x1
#define P0_MUX_CLR_P0_0 0x1
#define P0_MUX_CLR_P0_0_BIT 0
#define P0_MUX_CLR_P0_1_MASK 0x2
#define P0_MUX_CLR_P0_1 0x2
#define P0_MUX_CLR_P0_1_BIT 1
#define P0_MUX_CLR_P0_2_MASK 0x4
#define P0_MUX_CLR_P0_2 0x4
#define P0_MUX_CLR_P0_2_BIT 2
#define P0_MUX_CLR_P0_3_MASK 0x8
#define P0_MUX_CLR_P0_3 0x8
#define P0_MUX_CLR_P0_3_BIT 3
#define P0_MUX_CLR_P0_4_MASK 0x10
#define P0_MUX_CLR_P0_4 0x10
#define P0_MUX_CLR_P0_4_BIT 4
#define P0_MUX_CLR_P0_5_MASK 0x20
#define P0_MUX_CLR_P0_5 0x20
#define P0_MUX_CLR_P0_5_BIT 5
#define P0_MUX_CLR_P0_6_MASK 0x40
#define P0_MUX_CLR_P0_6 0x40
#define P0_MUX_CLR_P0_6_BIT 6
#define P0_MUX_CLR_P0_7_MASK 0x80
#define P0_MUX_CLR_P0_7 0x80
#define P0_MUX_CLR_P0_7_BIT 7

#define P0_MUX_STATE (*(volatile unsigned *)0x40028128)
#define P0_MUX_STATE__0_MASK 0x1
#define P0_MUX_STATE__0 0x1
#define P0_MUX_STATE__0_BIT 0
#define P0_MUX_STATE__1_MASK 0x2
#define P0_MUX_STATE__1 0x2
#define P0_MUX_STATE__1_BIT 1
#define P0_MUX_STATE__2_MASK 0x4
#define P0_MUX_STATE__2 0x4
#define P0_MUX_STATE__2_BIT 2
#define P0_MUX_STATE__3_MASK 0x8
#define P0_MUX_STATE__3 0x8
#define P0_MUX_STATE__3_BIT 3
#define P0_MUX_STATE__4_MASK 0x10
#define P0_MUX_STATE__4 0x10
#define P0_MUX_STATE__4_BIT 4
#define P0_MUX_STATE__5_MASK 0x20
#define P0_MUX_STATE__5 0x20
#define P0_MUX_STATE__5_BIT 5
#define P0_MUX_STATE__6_MASK 0x40
#define P0_MUX_STATE__6 0x40
#define P0_MUX_STATE__6_BIT 6
#define P0_MUX_STATE__7_MASK 0x80
#define P0_MUX_STATE__7 0x80
#define P0_MUX_STATE__7_BIT 7


#define P1_MUX_SET (*(volatile unsigned *)0x40028130)

#define P1_MUX_SET_P1_0_MASK 0x1
#define P1_MUX_SET_P1_0 0x1
#define P1_MUX_SET_P1_0_BIT 0

#define P1_MUX_SET_P1_1_MASK 0x2
#define P1_MUX_SET_P1_1 0x2
#define P1_MUX_SET_P1_1_BIT 1

#define P1_MUX_SET_P1_2_MASK 0x4
#define P1_MUX_SET_P1_2 0x4
#define P1_MUX_SET_P1_2_BIT 2

#define P1_MUX_SET_P1_3_MASK 0x8
#define P1_MUX_SET_P1_3 0x8
#define P1_MUX_SET_P1_3_BIT 3
#define P1_MUX_SET_P1_4_MASK 0x10
#define P1_MUX_SET_P1_4 0x10
#define P1_MUX_SET_P1_4_BIT 4
#define P1_MUX_SET_P1_5_MASK 0x20
#define P1_MUX_SET_P1_5 0x20
#define P1_MUX_SET_P1_5_BIT 5
#define P1_MUX_SET_P1_6_MASK 0x40
#define P1_MUX_SET_P1_6 0x40
#define P1_MUX_SET_P1_6_BIT 6
#define P1_MUX_SET_P1_7_MASK 0x80
#define P1_MUX_SET_P1_7 0x80
#define P1_MUX_SET_P1_7_BIT 7
#define P1_MUX_SET_P1_8_MASK 0x100
#define P1_MUX_SET_P1_8 0x100
#define P1_MUX_SET_P1_8_BIT 8
#define P1_MUX_SET_P1_9_MASK 0x200
#define P1_MUX_SET_P1_9 0x200
#define P1_MUX_SET_P1_9_BIT 9
#define P1_MUX_SET_P1_10_MASK 0x400
#define P1_MUX_SET_P1_10 0x400
#define P1_MUX_SET_P1_10_BIT 10
#define P1_MUX_SET_P1_11_MASK 0x800
#define P1_MUX_SET_P1_11 0x800
#define P1_MUX_SET_P1_11_BIT 11
#define P1_MUX_SET_P1_12_MASK 0x1000
#define P1_MUX_SET_P1_12 0x1000
#define P1_MUX_SET_P1_12_BIT 12
#define P1_MUX_SET_P1_13_MASK 0x2000
#define P1_MUX_SET_P1_13 0x2000
#define P1_MUX_SET_P1_13_BIT 13
#define P1_MUX_SET_P1_14_MASK 0x4000
#define P1_MUX_SET_P1_14 0x4000
#define P1_MUX_SET_P1_14_BIT 14
#define P1_MUX_SET_P1_15_MASK 0x8000
#define P1_MUX_SET_P1_15 0x8000
#define P1_MUX_SET_P1_15_BIT 15
#define P1_MUX_SET_P1_16_MASK 0x10000
#define P1_MUX_SET_P1_16 0x10000
#define P1_MUX_SET_P1_16_BIT 16
#define P1_MUX_SET_P1_17_MASK 0x20000
#define P1_MUX_SET_P1_17 0x20000
#define P1_MUX_SET_P1_17_BIT 17
#define P1_MUX_SET_P1_18_MASK 0x40000
#define P1_MUX_SET_P1_18 0x40000
#define P1_MUX_SET_P1_18_BIT 18
#define P1_MUX_SET_P1_19_MASK 0x80000
#define P1_MUX_SET_P1_19 0x80000
#define P1_MUX_SET_P1_19_BIT 19
#define P1_MUX_SET_P1_20_MASK 0x100000
#define P1_MUX_SET_P1_20 0x100000
#define P1_MUX_SET_P1_20_BIT 20
#define P1_MUX_SET_P1_21_MASK 0x200000
#define P1_MUX_SET_P1_21 0x200000
#define P1_MUX_SET_P1_21_BIT 21
#define P1_MUX_SET_P1_22_MASK 0x400000
#define P1_MUX_SET_P1_22 0x400000
#define P1_MUX_SET_P1_22_BIT 22
#define P1_MUX_SET_P1_23_MASK 0x800000
#define P1_MUX_SET_P1_23 0x800000
#define P1_MUX_SET_P1_23_BIT 23

#define P1_MUX_CLR (*(volatile unsigned *)0x40028134)
#define P1_MUX_CLR_EMC_A_0_MASK 0x1
#define P1_MUX_CLR_EMC_A_0 0x1
#define P1_MUX_CLR_EMC_A_0_BIT 0
#define P1_MUX_CLR_EMC_A_1_MASK 0x2
#define P1_MUX_CLR_EMC_A_1 0x2
#define P1_MUX_CLR_EMC_A_1_BIT 1
#define P1_MUX_CLR_EMC_A_2_MASK 0x4
#define P1_MUX_CLR_EMC_A_2 0x4
#define P1_MUX_CLR_EMC_A_2_BIT 2
#define P1_MUX_CLR_EMC_A_3_MASK 0x8
#define P1_MUX_CLR_EMC_A_3 0x8
#define P1_MUX_CLR_EMC_A_3_BIT 3
#define P1_MUX_CLR_EMC_A_4_MASK 0x10
#define P1_MUX_CLR_EMC_A_4 0x10
#define P1_MUX_CLR_EMC_A_4_BIT 4
#define P1_MUX_CLR_EMC_A_5_MASK 0x20
#define P1_MUX_CLR_EMC_A_5 0x20
#define P1_MUX_CLR_EMC_A_5_BIT 5
#define P1_MUX_CLR_EMC_A_6_MASK 0x40
#define P1_MUX_CLR_EMC_A_6 0x40
#define P1_MUX_CLR_EMC_A_6_BIT 6
#define P1_MUX_CLR_EMC_A_7_MASK 0x80
#define P1_MUX_CLR_EMC_A_7 0x80
#define P1_MUX_CLR_EMC_A_7_BIT 7
#define P1_MUX_CLR_EMC_A_8_MASK 0x100
#define P1_MUX_CLR_EMC_A_8 0x100
#define P1_MUX_CLR_EMC_A_8_BIT 8
#define P1_MUX_CLR_EMC_A_9_MASK 0x200
#define P1_MUX_CLR_EMC_A_9 0x200
#define P1_MUX_CLR_EMC_A_9_BIT 9
#define P1_MUX_CLR_EMC_A_10_MASK 0x400
#define P1_MUX_CLR_EMC_A_10 0x400
#define P1_MUX_CLR_EMC_A_10_BIT 10
#define P1_MUX_CLR_EMC_A_11_MASK 0x800
#define P1_MUX_CLR_EMC_A_11 0x800
#define P1_MUX_CLR_EMC_A_11_BIT 11
#define P1_MUX_CLR_EMC_A_12_MASK 0x1000
#define P1_MUX_CLR_EMC_A_12 0x1000
#define P1_MUX_CLR_EMC_A_12_BIT 12
#define P1_MUX_CLR_EMC_A_13_MASK 0x2000
#define P1_MUX_CLR_EMC_A_13 0x2000
#define P1_MUX_CLR_EMC_A_13_BIT 13
#define P1_MUX_CLR_EMC_A_14_MASK 0x4000
#define P1_MUX_CLR_EMC_A_14 0x4000
#define P1_MUX_CLR_EMC_A_14_BIT 14
#define P1_MUX_CLR_EMC_A_15_MASK 0x8000
#define P1_MUX_CLR_EMC_A_15 0x8000
#define P1_MUX_CLR_EMC_A_15_BIT 15
#define P1_MUX_CLR_EMC_A_16_MASK 0x10000
#define P1_MUX_CLR_EMC_A_16 0x10000
#define P1_MUX_CLR_EMC_A_16_BIT 16
#define P1_MUX_CLR_EMC_A_17_MASK 0x20000
#define P1_MUX_CLR_EMC_A_17 0x20000
#define P1_MUX_CLR_EMC_A_17_BIT 17
#define P1_MUX_CLR_EMC_A_18_MASK 0x40000
#define P1_MUX_CLR_EMC_A_18 0x40000
#define P1_MUX_CLR_EMC_A_18_BIT 18
#define P1_MUX_CLR_EMC_A_19_MASK 0x80000
#define P1_MUX_CLR_EMC_A_19 0x80000
#define P1_MUX_CLR_EMC_A_19_BIT 19
#define P1_MUX_CLR_EMC_A_20_MASK 0x100000
#define P1_MUX_CLR_EMC_A_20 0x100000
#define P1_MUX_CLR_EMC_A_20_BIT 20
#define P1_MUX_CLR_EMC_A_21_MASK 0x200000
#define P1_MUX_CLR_EMC_A_21 0x200000
#define P1_MUX_CLR_EMC_A_21_BIT 21
#define P1_MUX_CLR_EMC_A_22_MASK 0x400000
#define P1_MUX_CLR_EMC_A_22 0x400000
#define P1_MUX_CLR_EMC_A_22_BIT 22
#define P1_MUX_CLR_EMC_A_23_MASK 0x800000
#define P1_MUX_CLR_EMC_A_23 0x800000
#define P1_MUX_CLR_EMC_A_23_BIT 23

#define P1_MUX_STATE (*(volatile unsigned *)0x40028138)
#define P1_MUX_STATE__0_MASK 0x1
#define P1_MUX_STATE__0 0x1
#define P1_MUX_STATE__0_BIT 0
#define P1_MUX_STATE__1_MASK 0x2
#define P1_MUX_STATE__1 0x2
#define P1_MUX_STATE__1_BIT 1
#define P1_MUX_STATE__2_MASK 0x4
#define P1_MUX_STATE__2 0x4
#define P1_MUX_STATE__2_BIT 2
#define P1_MUX_STATE__3_MASK 0x8
#define P1_MUX_STATE__3 0x8
#define P1_MUX_STATE__3_BIT 3
#define P1_MUX_STATE__4_MASK 0x10
#define P1_MUX_STATE__4 0x10
#define P1_MUX_STATE__4_BIT 4
#define P1_MUX_STATE__5_MASK 0x20
#define P1_MUX_STATE__5 0x20
#define P1_MUX_STATE__5_BIT 5
#define P1_MUX_STATE__6_MASK 0x40
#define P1_MUX_STATE__6 0x40
#define P1_MUX_STATE__6_BIT 6
#define P1_MUX_STATE__7_MASK 0x80
#define P1_MUX_STATE__7 0x80
#define P1_MUX_STATE__7_BIT 7
#define P1_MUX_STATE__8_MASK 0x100
#define P1_MUX_STATE__8 0x100
#define P1_MUX_STATE__8_BIT 8
#define P1_MUX_STATE__9_MASK 0x200
#define P1_MUX_STATE__9 0x200
#define P1_MUX_STATE__9_BIT 9
#define P1_MUX_STATE__10_MASK 0x400
#define P1_MUX_STATE__10 0x400
#define P1_MUX_STATE__10_BIT 10
#define P1_MUX_STATE__11_MASK 0x800
#define P1_MUX_STATE__11 0x800
#define P1_MUX_STATE__11_BIT 11
#define P1_MUX_STATE__12_MASK 0x1000
#define P1_MUX_STATE__12 0x1000
#define P1_MUX_STATE__12_BIT 12
#define P1_MUX_STATE__13_MASK 0x2000
#define P1_MUX_STATE__13 0x2000
#define P1_MUX_STATE__13_BIT 13
#define P1_MUX_STATE__14_MASK 0x4000
#define P1_MUX_STATE__14 0x4000
#define P1_MUX_STATE__14_BIT 14
#define P1_MUX_STATE__15_MASK 0x8000
#define P1_MUX_STATE__15 0x8000
#define P1_MUX_STATE__15_BIT 15
#define P1_MUX_STATE__16_MASK 0x10000
#define P1_MUX_STATE__16 0x10000
#define P1_MUX_STATE__16_BIT 16
#define P1_MUX_STATE__17_MASK 0x20000
#define P1_MUX_STATE__17 0x20000
#define P1_MUX_STATE__17_BIT 17
#define P1_MUX_STATE__18_MASK 0x40000
#define P1_MUX_STATE__18 0x40000
#define P1_MUX_STATE__18_BIT 18
#define P1_MUX_STATE__19_MASK 0x80000
#define P1_MUX_STATE__19 0x80000
#define P1_MUX_STATE__19_BIT 19
#define P1_MUX_STATE__20_MASK 0x100000
#define P1_MUX_STATE__20 0x100000
#define P1_MUX_STATE__20_BIT 20
#define P1_MUX_STATE__21_MASK 0x200000
#define P1_MUX_STATE__21 0x200000
#define P1_MUX_STATE__21_BIT 21
#define P1_MUX_STATE__22_MASK 0x400000
#define P1_MUX_STATE__22 0x400000
#define P1_MUX_STATE__22_BIT 22
#define P1_MUX_STATE__23_MASK 0x800000
#define P1_MUX_STATE__23 0x800000
#define P1_MUX_STATE__23_BIT 23

#define T4IR (*(volatile unsigned char *)0x4002C000)
#define T4IR_MR0_MASK 0x1
#define T4IR_MR0 0x1
#define T4IR_MR0_BIT 0
#define T4IR_MR1_MASK 0x2
#define T4IR_MR1 0x2
#define T4IR_MR1_BIT 1
#define T4IR_MR2_MASK 0x4
#define T4IR_MR2 0x4
#define T4IR_MR2_BIT 2
#define T4IR_MR3_MASK 0x8
#define T4IR_MR3 0x8
#define T4IR_MR3_BIT 3
#define T4IR_CR0_MASK 0x10
#define T4IR_CR0 0x10
#define T4IR_CR0_BIT 4
#define T4IR_CR1_MASK 0x20
#define T4IR_CR1 0x20
#define T4IR_CR1_BIT 5
#define T4IR_CR2_MASK 0x40
#define T4IR_CR2 0x40
#define T4IR_CR2_BIT 6
#define T4IR_CR3_MASK 0x80
#define T4IR_CR3 0x80
#define T4IR_CR3_BIT 7

#define T4TCR (*(volatile unsigned char *)0x4002C004)
#define T4TCR_Counter_Enable_MASK 0x1
#define T4TCR_Counter_Enable 0x1
#define T4TCR_Counter_Enable_BIT 0
#define T4TCR_Counter_Reset_MASK 0x2
#define T4TCR_Counter_Reset 0x2
#define T4TCR_Counter_Reset_BIT 1

#define T4TC (*(volatile unsigned long *)0x4002C008)

#define T4PR (*(volatile unsigned long *)0x4002C00C)

#define T4PC (*(volatile unsigned long *)0x4002C010)

#define T4MCR (*(volatile unsigned short *)0x4002C014)
#define T4MCR_MR0I_MASK 0x1
#define T4MCR_MR0I 0x1
#define T4MCR_MR0I_BIT 0
#define T4MCR_MR0R_MASK 0x2
#define T4MCR_MR0R 0x2
#define T4MCR_MR0R_BIT 1
#define T4MCR_MR0S_MASK 0x4
#define T4MCR_MR0S 0x4
#define T4MCR_MR0S_BIT 2
#define T4MCR_MR1I_MASK 0x8
#define T4MCR_MR1I 0x8
#define T4MCR_MR1I_BIT 3
#define T4MCR_MR1R_MASK 0x10
#define T4MCR_MR1R 0x10
#define T4MCR_MR1R_BIT 4
#define T4MCR_MR1S_MASK 0x20
#define T4MCR_MR1S 0x20
#define T4MCR_MR1S_BIT 5
#define T4MCR_MR2I_MASK 0x40
#define T4MCR_MR2I 0x40
#define T4MCR_MR2I_BIT 6
#define T4MCR_MR2R_MASK 0x80
#define T4MCR_MR2R 0x80
#define T4MCR_MR2R_BIT 7
#define T4MCR_MR2S_MASK 0x100
#define T4MCR_MR2S 0x100
#define T4MCR_MR2S_BIT 8
#define T4MCR_MR3I_MASK 0x200
#define T4MCR_MR3I 0x200
#define T4MCR_MR3I_BIT 9
#define T4MCR_MR3R_MASK 0x400
#define T4MCR_MR3R 0x400
#define T4MCR_MR3R_BIT 10
#define T4MCR_MR3S_MASK 0x800
#define T4MCR_MR3S 0x800
#define T4MCR_MR3S_BIT 11

#define T4MR0 (*(volatile unsigned long *)0x4002C018)

#define T4MR1 (*(volatile unsigned long *)0x4002C01C)

#define T4MR2 (*(volatile unsigned long *)0x4002C020)

#define T4MR3 (*(volatile unsigned long *)0x4002C024)

#define T4CCR (*(volatile unsigned short *)0x4002C028)
#define T4CCR_CAP0RE_MASK 0x1
#define T4CCR_CAP0RE 0x1
#define T4CCR_CAP0RE_BIT 0
#define T4CCR_CAP0FE_MASK 0x2
#define T4CCR_CAP0FE 0x2
#define T4CCR_CAP0FE_BIT 1
#define T4CCR_CAP0I_MASK 0x4
#define T4CCR_CAP0I 0x4
#define T4CCR_CAP0I_BIT 2
#define T4CCR_CAP1RE_MASK 0x8
#define T4CCR_CAP1RE 0x8
#define T4CCR_CAP1RE_BIT 3
#define T4CCR_CAP1FE_MASK 0x10
#define T4CCR_CAP1FE 0x10
#define T4CCR_CAP1FE_BIT 4
#define T4CCR_CAP1I_MASK 0x20
#define T4CCR_CAP1I 0x20
#define T4CCR_CAP1I_BIT 5
#define T4CCR_CAP2RE_MASK 0x40
#define T4CCR_CAP2RE 0x40
#define T4CCR_CAP2RE_BIT 6
#define T4CCR_CAP2FE_MASK 0x80
#define T4CCR_CAP2FE 0x80
#define T4CCR_CAP2FE_BIT 7
#define T4CCR_CAP2I_MASK 0x100
#define T4CCR_CAP2I 0x100
#define T4CCR_CAP2I_BIT 8
#define T4CCR_CAP3RE_MASK 0x200
#define T4CCR_CAP3RE 0x200
#define T4CCR_CAP3RE_BIT 9
#define T4CCR_CAP3FE_MASK 0x400
#define T4CCR_CAP3FE 0x400
#define T4CCR_CAP3FE_BIT 10
#define T4CCR_CAP3I_MASK 0x800
#define T4CCR_CAP3I 0x800
#define T4CCR_CAP3I_BIT 11

#define T4CR0 (*(volatile unsigned long *)0x4002C02C)

#define T4CR1 (*(volatile unsigned long *)0x4002C030)

#define T4CR2 (*(volatile unsigned long *)0x4002C034)

#define T4CR3 (*(volatile unsigned long *)0x4002C038)

#define T4EMR (*(volatile unsigned short *)0x4002C03C)
#define T4EMR_EM0_MASK 0x1
#define T4EMR_EM0 0x1
#define T4EMR_EM0_BIT 0
#define T4EMR_EM1_MASK 0x2
#define T4EMR_EM1 0x2
#define T4EMR_EM1_BIT 1
#define T4EMR_EM2_MASK 0x4
#define T4EMR_EM2 0x4
#define T4EMR_EM2_BIT 2
#define T4EMR_EM3_MASK 0x8
#define T4EMR_EM3 0x8
#define T4EMR_EM3_BIT 3
#define T4EMR_EMC0_MASK 0x30
#define T4EMR_EMC0_BIT 4
#define T4EMR_EMC1_MASK 0xC0
#define T4EMR_EMC1_BIT 6
#define T4EMR_EMC2_MASK 0x300
#define T4EMR_EMC2_BIT 8
#define T4EMR_EMC3_MASK 0xC00
#define T4EMR_EMC3_BIT 10

#define T4CTCR (*(volatile unsigned long *)0x4002C070)
#define T4CTCR_Counter_Timer_Mode_MASK 0x3
#define T4CTCR_Counter_Timer_Mode_BIT 0
#define T4CTCR_Count_Input_Select_MASK 0xC
#define T4CTCR_Count_Input_Select_BIT 2

#define T5IR (*(volatile unsigned char *)0x40030000)
#define T5IR_MR0_MASK 0x1
#define T5IR_MR0 0x1
#define T5IR_MR0_BIT 0
#define T5IR_MR1_MASK 0x2
#define T5IR_MR1 0x2
#define T5IR_MR1_BIT 1
#define T5IR_MR2_MASK 0x4
#define T5IR_MR2 0x4
#define T5IR_MR2_BIT 2
#define T5IR_MR3_MASK 0x8
#define T5IR_MR3 0x8
#define T5IR_MR3_BIT 3
#define T5IR_CR0_MASK 0x10
#define T5IR_CR0 0x10
#define T5IR_CR0_BIT 4
#define T5IR_CR1_MASK 0x20
#define T5IR_CR1 0x20
#define T5IR_CR1_BIT 5
#define T5IR_CR2_MASK 0x40
#define T5IR_CR2 0x40
#define T5IR_CR2_BIT 6
#define T5IR_CR3_MASK 0x80
#define T5IR_CR3 0x80
#define T5IR_CR3_BIT 7

#define T5TCR (*(volatile unsigned char *)0x40030004)
#define T5TCR_Counter_Enable_MASK 0x1
#define T5TCR_Counter_Enable 0x1
#define T5TCR_Counter_Enable_BIT 0
#define T5TCR_Counter_Reset_MASK 0x2
#define T5TCR_Counter_Reset 0x2
#define T5TCR_Counter_Reset_BIT 1

#define T5TC (*(volatile unsigned long *)0x40030008)

#define T5PR (*(volatile unsigned long *)0x4003000C)

#define T5PC (*(volatile unsigned long *)0x40030010)

#define T5MCR (*(volatile unsigned short *)0x40030014)
#define T5MCR_MR0I_MASK 0x1
#define T5MCR_MR0I 0x1
#define T5MCR_MR0I_BIT 0
#define T5MCR_MR0R_MASK 0x2
#define T5MCR_MR0R 0x2
#define T5MCR_MR0R_BIT 1
#define T5MCR_MR0S_MASK 0x4
#define T5MCR_MR0S 0x4
#define T5MCR_MR0S_BIT 2
#define T5MCR_MR1I_MASK 0x8
#define T5MCR_MR1I 0x8
#define T5MCR_MR1I_BIT 3
#define T5MCR_MR1R_MASK 0x10
#define T5MCR_MR1R 0x10
#define T5MCR_MR1R_BIT 4
#define T5MCR_MR1S_MASK 0x20
#define T5MCR_MR1S 0x20
#define T5MCR_MR1S_BIT 5
#define T5MCR_MR2I_MASK 0x40
#define T5MCR_MR2I 0x40
#define T5MCR_MR2I_BIT 6
#define T5MCR_MR2R_MASK 0x80
#define T5MCR_MR2R 0x80
#define T5MCR_MR2R_BIT 7
#define T5MCR_MR2S_MASK 0x100
#define T5MCR_MR2S 0x100
#define T5MCR_MR2S_BIT 8
#define T5MCR_MR3I_MASK 0x200
#define T5MCR_MR3I 0x200
#define T5MCR_MR3I_BIT 9
#define T5MCR_MR3R_MASK 0x400
#define T5MCR_MR3R 0x400
#define T5MCR_MR3R_BIT 10
#define T5MCR_MR3S_MASK 0x800
#define T5MCR_MR3S 0x800
#define T5MCR_MR3S_BIT 11

#define T5MR0 (*(volatile unsigned long *)0x40030018)

#define T5MR1 (*(volatile unsigned long *)0x4003001C)

#define T5MR2 (*(volatile unsigned long *)0x40030020)

#define T5MR3 (*(volatile unsigned long *)0x40030024)

#define T5CCR (*(volatile unsigned short *)0x40030028)
#define T5CCR_CAP0RE_MASK 0x1
#define T5CCR_CAP0RE 0x1
#define T5CCR_CAP0RE_BIT 0
#define T5CCR_CAP0FE_MASK 0x2
#define T5CCR_CAP0FE 0x2
#define T5CCR_CAP0FE_BIT 1
#define T5CCR_CAP0I_MASK 0x4
#define T5CCR_CAP0I 0x4
#define T5CCR_CAP0I_BIT 2
#define T5CCR_CAP1RE_MASK 0x8
#define T5CCR_CAP1RE 0x8
#define T5CCR_CAP1RE_BIT 3
#define T5CCR_CAP1FE_MASK 0x10
#define T5CCR_CAP1FE 0x10
#define T5CCR_CAP1FE_BIT 4
#define T5CCR_CAP1I_MASK 0x20
#define T5CCR_CAP1I 0x20
#define T5CCR_CAP1I_BIT 5
#define T5CCR_CAP2RE_MASK 0x40
#define T5CCR_CAP2RE 0x40
#define T5CCR_CAP2RE_BIT 6
#define T5CCR_CAP2FE_MASK 0x80
#define T5CCR_CAP2FE 0x80
#define T5CCR_CAP2FE_BIT 7
#define T5CCR_CAP2I_MASK 0x100
#define T5CCR_CAP2I 0x100
#define T5CCR_CAP2I_BIT 8
#define T5CCR_CAP3RE_MASK 0x200
#define T5CCR_CAP3RE 0x200
#define T5CCR_CAP3RE_BIT 9
#define T5CCR_CAP3FE_MASK 0x400
#define T5CCR_CAP3FE 0x400
#define T5CCR_CAP3FE_BIT 10
#define T5CCR_CAP3I_MASK 0x800
#define T5CCR_CAP3I 0x800
#define T5CCR_CAP3I_BIT 11

#define T5CR0 (*(volatile unsigned long *)0x4003002C)

#define T5CR1 (*(volatile unsigned long *)0x40030030)

#define T5CR2 (*(volatile unsigned long *)0x40030034)

#define T5CR3 (*(volatile unsigned long *)0x40030038)

#define T5EMR (*(volatile unsigned short *)0x4003003C)
#define T5EMR_EM0_MASK 0x1
#define T5EMR_EM0 0x1
#define T5EMR_EM0_BIT 0
#define T5EMR_EM1_MASK 0x2
#define T5EMR_EM1 0x2
#define T5EMR_EM1_BIT 1
#define T5EMR_EM2_MASK 0x4
#define T5EMR_EM2 0x4
#define T5EMR_EM2_BIT 2
#define T5EMR_EM3_MASK 0x8
#define T5EMR_EM3 0x8
#define T5EMR_EM3_BIT 3
#define T5EMR_EMC0_MASK 0x30
#define T5EMR_EMC0_BIT 4
#define T5EMR_EMC1_MASK 0xC0
#define T5EMR_EMC1_BIT 6
#define T5EMR_EMC2_MASK 0x300
#define T5EMR_EMC2_BIT 8
#define T5EMR_EMC3_MASK 0xC00
#define T5EMR_EMC3_BIT 10

#define T5CTCR (*(volatile unsigned long *)0x40030070)
#define T5CTCR_Counter_Timer_Mode_MASK 0x3
#define T5CTCR_Counter_Timer_Mode_BIT 0
#define T5CTCR_Count_Input_Select_MASK 0xC
#define T5CTCR_Count_Input_Select_BIT 2

#define MSTIM_INT (*(volatile unsigned *)0x40034000)
#define MSTIM_INT_MATCH1_INT_MASK 0x2
#define MSTIM_INT_MATCH1_INT 0x2
#define MSTIM_INT_MATCH1_INT_BIT 1
#define MSTIM_INT_MATCH0_INT_MASK 0x1
#define MSTIM_INT_MATCH0_INT 0x1
#define MSTIM_INT_MATCH0_INT_BIT 0

#define MSTIM_CTRL (*(volatile unsigned *)0x40034004)
#define MSTIM_CTRL_PAUSE_EN_MASK 0x4
#define MSTIM_CTRL_PAUSE_EN 0x4
#define MSTIM_CTRL_PAUSE_EN_BIT 2
#define MSTIM_CTRL_RESET_COUNT_MASK 0x2
#define MSTIM_CTRL_RESET_COUNT 0x2
#define MSTIM_CTRL_RESET_COUNT_BIT 1
#define MSTIM_CTRL_COUNT_ENAB_MASK 0x1
#define MSTIM_CTRL_COUNT_ENAB 0x1
#define MSTIM_CTRL_COUNT_ENAB_BIT 0

#define MSTIM_COUNTER (*(volatile unsigned *)0x40034008)

#define MSTIM_MCTRL (*(volatile unsigned *)0x40034014)
#define MSTIM_MCTRL_STOP_COUNT1_MASK 0x20
#define MSTIM_MCTRL_STOP_COUNT1 0x20
#define MSTIM_MCTRL_STOP_COUNT1_BIT 5
#define MSTIM_MCTRL_RESET_COUNT1_MASK 0x10
#define MSTIM_MCTRL_RESET_COUNT1 0x10
#define MSTIM_MCTRL_RESET_COUNT1_BIT 4
#define MSTIM_MCTRL_MR1_INT_MASK 0x8
#define MSTIM_MCTRL_MR1_INT 0x8
#define MSTIM_MCTRL_MR1_INT_BIT 3
#define MSTIM_MCTRL_STOP_COUNT0_MASK 0x4
#define MSTIM_MCTRL_STOP_COUNT0 0x4
#define MSTIM_MCTRL_STOP_COUNT0_BIT 2
#define MSTIM_MCTRL_RESET_COUNT0_MASK 0x2
#define MSTIM_MCTRL_RESET_COUNT0 0x2
#define MSTIM_MCTRL_RESET_COUNT0_BIT 1
#define MSTIM_MCTRL_MR0_INT_MASK 0x1
#define MSTIM_MCTRL_MR0_INT 0x1
#define MSTIM_MCTRL_MR0_INT_BIT 0

#define MSTIM_MATCH0 (*(volatile unsigned *)0x40034018)

#define MSTIM_MATCH1 (*(volatile unsigned *)0x4003401C)

#define HSTIM_INT (*(volatile unsigned *)0x40038000)
#define HSTIM_INT_RTC_TICK_MASK 0x20
#define HSTIM_INT_RTC_TICK 0x20
#define HSTIM_INT_RTC_TICK_BIT 5
#define HSTIM_INT_HSIM_CAP_MASK 0x10
#define HSTIM_INT_HSIM_CAP 0x10
#define HSTIM_INT_HSIM_CAP_BIT 4
#define HSTIM_INT_MATCH2_INT_MASK 0x4
#define HSTIM_INT_MATCH2_INT 0x4
#define HSTIM_INT_MATCH2_INT_BIT 2
#define HSTIM_INT_MATCH1_INT_MASK 0x2
#define HSTIM_INT_MATCH1_INT 0x2
#define HSTIM_INT_MATCH1_INT_BIT 1
#define HSTIM_INT_MATCH0_INT_MASK 0x1
#define HSTIM_INT_MATCH0_INT 0x1
#define HSTIM_INT_MATCH0_INT_BIT 0

#define HSTIM_CTRL (*(volatile unsigned *)0x40038004)
#define HSTIM_CTRL_PAUSE_EN_MASK 0x4
#define HSTIM_CTRL_PAUSE_EN 0x4
#define HSTIM_CTRL_PAUSE_EN_BIT 2
#define HSTIM_CTRL_RESET_COUNT_MASK 0x2
#define HSTIM_CTRL_RESET_COUNT 0x2
#define HSTIM_CTRL_RESET_COUNT_BIT 1
#define HSTIM_CTRL_COUNT_ENAB_MASK 0x1
#define HSTIM_CTRL_COUNT_ENAB 0x1
#define HSTIM_CTRL_COUNT_ENAB_BIT 0

#define HSTIM_COUNTER (*(volatile unsigned *)0x40038008)

#define HSTIM_PMATCH (*(volatile unsigned *)0x4003800C)
#define HSTIM_PMATCH_PMATCH_MASK 0xFFFF
#define HSTIM_PMATCH_PMATCH_BIT 0

#define HSTIM_PCOUNT (*(volatile unsigned *)0x40038010)
#define HSTIM_PCOUNT_PCOUNT_MASK 0xFFFF
#define HSTIM_PCOUNT_PCOUNT_BIT 0

#define HSTIM_MCTRL (*(volatile unsigned *)0x40038014)
#define HSTIM_MCTRL_STOP_COUNT2_MASK 0x100
#define HSTIM_MCTRL_STOP_COUNT2 0x100
#define HSTIM_MCTRL_STOP_COUNT2_BIT 8
#define HSTIM_MCTRL_RESET_COUNT2_MASK 0x80
#define HSTIM_MCTRL_RESET_COUNT2 0x80
#define HSTIM_MCTRL_RESET_COUNT2_BIT 7
#define HSTIM_MCTRL_MR2_INT_MASK 0x40
#define HSTIM_MCTRL_MR2_INT 0x40
#define HSTIM_MCTRL_MR2_INT_BIT 6
#define HSTIM_MCTRL_STOP_COUNT1_MASK 0x20
#define HSTIM_MCTRL_STOP_COUNT1 0x20
#define HSTIM_MCTRL_STOP_COUNT1_BIT 5
#define HSTIM_MCTRL_RESET_COUNT1_MASK 0x10
#define HSTIM_MCTRL_RESET_COUNT1 0x10
#define HSTIM_MCTRL_RESET_COUNT1_BIT 4
#define HSTIM_MCTRL_MR1_INT_MASK 0x8
#define HSTIM_MCTRL_MR1_INT 0x8
#define HSTIM_MCTRL_MR1_INT_BIT 3
#define HSTIM_MCTRL_STOP_COUNT0_MASK 0x4
#define HSTIM_MCTRL_STOP_COUNT0 0x4
#define HSTIM_MCTRL_STOP_COUNT0_BIT 2
#define HSTIM_MCTRL_RESET_COUNT0_MASK 0x2
#define HSTIM_MCTRL_RESET_COUNT0 0x2
#define HSTIM_MCTRL_RESET_COUNT0_BIT 1
#define HSTIM_MCTRL_MR0_INT_MASK 0x1
#define HSTIM_MCTRL_MR0_INT 0x1
#define HSTIM_MCTRL_MR0_INT_BIT 0

#define HSTIM_MATCH0 (*(volatile unsigned *)0x40038018)

#define HSTIM_MATCH1 (*(volatile unsigned *)0x4003801C)

#define HSTIM_MATCH2 (*(volatile unsigned *)0x40038020)

#define HSTIM_CCR (*(volatile unsigned *)0x40038028)
#define HSTIM_CCR_RTC_TICK_EVENT_MASK 0x20
#define HSTIM_CCR_RTC_TICK_EVENT 0x20
#define HSTIM_CCR_RTC_TICK_EVENT_BIT 5
#define HSTIM_CCR_RTC_TICK_FALL_MASK 0x10
#define HSTIM_CCR_RTC_TICK_FALL 0x10
#define HSTIM_CCR_RTC_TICK_FALL_BIT 4
#define HSTIM_CCR_RTC_TICK_RISE_MASK 0x8
#define HSTIM_CCR_RTC_TICK_RISE 0x8
#define HSTIM_CCR_RTC_TICK_RISE_BIT 3
#define HSTIM_CCR_HSTIM_CAP_EVENT_MASK 0x4
#define HSTIM_CCR_HSTIM_CAP_EVENT 0x4
#define HSTIM_CCR_HSTIM_CAP_EVENT_BIT 2
#define HSTIM_CCR_HSTIM_CAP_FALL_MASK 0x2
#define HSTIM_CCR_HSTIM_CAP_FALL 0x2
#define HSTIM_CCR_HSTIM_CAP_FALL_BIT 1
#define HSTIM_CCR_HSTIM_CAP_RISE_MASK 0x1
#define HSTIM_CCR_HSTIM_CAP_RISE 0x1
#define HSTIM_CCR_HSTIM_CAP_RISE_BIT 0

#define HSTIM_CR0 (*(volatile unsigned *)0x4003802C)

#define HSTIM_CR1 (*(volatile unsigned *)0x40038030)

#define WDTIM_INT (*(volatile unsigned *)0x4003C000)
#define WDTIM_INT_MATCH_INT_MASK 0x1
#define WDTIM_INT_MATCH_INT 0x1
#define WDTIM_INT_MATCH_INT_BIT 0

#define WDTIM_CTRL (*(volatile unsigned *)0x4003C004)
#define WDTIM_CTRL_PAUSE_EN_MASK 0x4
#define WDTIM_CTRL_PAUSE_EN 0x4
#define WDTIM_CTRL_PAUSE_EN_BIT 2
#define WDTIM_CTRL_RESET_COUNT_MASK 0x2
#define WDTIM_CTRL_RESET_COUNT 0x2
#define WDTIM_CTRL_RESET_COUNT_BIT 1
#define WDTIM_CTRL_COUNT_ENAB_MASK 0x1
#define WDTIM_CTRL_COUNT_ENAB 0x1
#define WDTIM_CTRL_COUNT_ENAB_BIT 0

#define WDTIM_COUNTER (*(volatile unsigned *)0x4003C008)

#define WDTIM_MCTRL (*(volatile unsigned *)0x4003C00C)
#define WDTIM_MCTRL_RESFRC2_MASK 0x40
#define WDTIM_MCTRL_RESFRC2 0x40
#define WDTIM_MCTRL_RESFRC2_BIT 6
#define WDTIM_MCTRL_RESFRC1_MASK 0x20
#define WDTIM_MCTRL_RESFRC1 0x20
#define WDTIM_MCTRL_RESFRC1_BIT 5
#define WDTIM_MCTRL_M_RES2_MASK 0x10
#define WDTIM_MCTRL_M_RES2 0x10
#define WDTIM_MCTRL_M_RES2_BIT 4
#define WDTIM_MCTRL_M_RES1_MASK 0x8
#define WDTIM_MCTRL_M_RES1 0x8
#define WDTIM_MCTRL_M_RES1_BIT 3
#define WDTIM_MCTRL_STOP_COUNT0_MASK 0x4
#define WDTIM_MCTRL_STOP_COUNT0 0x4
#define WDTIM_MCTRL_STOP_COUNT0_BIT 2
#define WDTIM_MCTRL_RESET_COUNT0_MASK 0x2
#define WDTIM_MCTRL_RESET_COUNT0 0x2
#define WDTIM_MCTRL_RESET_COUNT0_BIT 1
#define WDTIM_MCTRL_MR0_INT_MASK 0x1
#define WDTIM_MCTRL_MR0_INT 0x1
#define WDTIM_MCTRL_MR0_INT_BIT 0

#define WDTIM_MATCH0 (*(volatile unsigned *)0x4003C010)

#define WDTIM_EMR (*(volatile unsigned *)0x4003C014)
#define WDTIM_EMR_MATCH_CTRL_MASK 0x30
#define WDTIM_EMR_MATCH_CTRL_BIT 4
#define WDTIM_EMR_EXT_MATCH0_MASK 0x1
#define WDTIM_EMR_EXT_MATCH0 0x1
#define WDTIM_EMR_EXT_MATCH0_BIT 0

#define WDTIM_PULSE (*(volatile unsigned *)0x4003C018)
#define WDTIM_PULSE_PULSE_MASK 0xFFFF
#define WDTIM_PULSE_PULSE_BIT 0

#define WDTIM_RES (*(volatile unsigned *)0x4003C01C)
#define WDTIM_RES_Watchdog_reset_MASK 0x1
#define WDTIM_RES_Watchdog_reset 0x1
#define WDTIM_RES_Watchdog_reset_BIT 0

#define DEBUG_CTRL (*(volatile unsigned *)0x40040000)
#define DEBUG_CTRL_VFP9_CLKEN_MASK 0x10
#define DEBUG_CTRL_VFP9_CLKEN 0x10
#define DEBUG_CTRL_VFP9_CLKEN_BIT 4
#define DEBUG_CTRL_VFP_BIGEND_MASK 0x8
#define DEBUG_CTRL_VFP_BIGEND 0x8
#define DEBUG_CTRL_VFP_BIGEND_BIT 3
#define DEBUG_CTRL_ARMDBG_DIS_MASK 0x4
#define DEBUG_CTRL_ARMDBG_DIS 0x4
#define DEBUG_CTRL_ARMDBG_DIS_BIT 2

#define DEBUG_GRANT (*(volatile unsigned *)0x40040004)
#define DEBUG_GRANT_USB_Master_MASK 0x80
#define DEBUG_GRANT_USB_Master 0x80
#define DEBUG_GRANT_USB_Master_BIT 7
#define DEBUG_GRANT_DMA_M1_Master_MASK 0x2
#define DEBUG_GRANT_DMA_M1_Master 0x2
#define DEBUG_GRANT_DMA_M1_Master_BIT 1
#define DEBUG_GRANT_DMA_M0_Master_MASK 0x1
#define DEBUG_GRANT_DMA_M0_Master 0x1
#define DEBUG_GRANT_DMA_M0_Master_BIT 0

#define T0IR (*(volatile unsigned char *)0x40044000)
#define T0IR_MR0_MASK 0x1
#define T0IR_MR0 0x1
#define T0IR_MR0_BIT 0
#define T0IR_MR1_MASK 0x2
#define T0IR_MR1 0x2
#define T0IR_MR1_BIT 1
#define T0IR_MR2_MASK 0x4
#define T0IR_MR2 0x4
#define T0IR_MR2_BIT 2
#define T0IR_MR3_MASK 0x8
#define T0IR_MR3 0x8
#define T0IR_MR3_BIT 3
#define T0IR_CR0_MASK 0x10
#define T0IR_CR0 0x10
#define T0IR_CR0_BIT 4
#define T0IR_CR1_MASK 0x20
#define T0IR_CR1 0x20
#define T0IR_CR1_BIT 5
#define T0IR_CR2_MASK 0x40
#define T0IR_CR2 0x40
#define T0IR_CR2_BIT 6
#define T0IR_CR3_MASK 0x80
#define T0IR_CR3 0x80
#define T0IR_CR3_BIT 7

#define T0TCR (*(volatile unsigned char *)0x40044004)
#define T0TCR_Counter_Enable_MASK 0x1
#define T0TCR_Counter_Enable 0x1
#define T0TCR_Counter_Enable_BIT 0
#define T0TCR_Counter_Reset_MASK 0x2
#define T0TCR_Counter_Reset 0x2
#define T0TCR_Counter_Reset_BIT 1

#define T0TC (*(volatile unsigned long *)0x40044008)

#define T0PR (*(volatile unsigned long *)0x4004400C)

#define T0PC (*(volatile unsigned long *)0x40044010)

#define T0MCR (*(volatile unsigned short *)0x40044014)
#define T0MCR_MR0I_MASK 0x1
#define T0MCR_MR0I 0x1
#define T0MCR_MR0I_BIT 0
#define T0MCR_MR0R_MASK 0x2
#define T0MCR_MR0R 0x2
#define T0MCR_MR0R_BIT 1
#define T0MCR_MR0S_MASK 0x4
#define T0MCR_MR0S 0x4
#define T0MCR_MR0S_BIT 2
#define T0MCR_MR1I_MASK 0x8
#define T0MCR_MR1I 0x8
#define T0MCR_MR1I_BIT 3
#define T0MCR_MR1R_MASK 0x10
#define T0MCR_MR1R 0x10
#define T0MCR_MR1R_BIT 4
#define T0MCR_MR1S_MASK 0x20
#define T0MCR_MR1S 0x20
#define T0MCR_MR1S_BIT 5
#define T0MCR_MR2I_MASK 0x40
#define T0MCR_MR2I 0x40
#define T0MCR_MR2I_BIT 6
#define T0MCR_MR2R_MASK 0x80
#define T0MCR_MR2R 0x80
#define T0MCR_MR2R_BIT 7
#define T0MCR_MR2S_MASK 0x100
#define T0MCR_MR2S 0x100
#define T0MCR_MR2S_BIT 8
#define T0MCR_MR3I_MASK 0x200
#define T0MCR_MR3I 0x200
#define T0MCR_MR3I_BIT 9
#define T0MCR_MR3R_MASK 0x400
#define T0MCR_MR3R 0x400
#define T0MCR_MR3R_BIT 10
#define T0MCR_MR3S_MASK 0x800
#define T0MCR_MR3S 0x800
#define T0MCR_MR3S_BIT 11

#define T0MR0 (*(volatile unsigned long *)0x40044018)

#define T0MR1 (*(volatile unsigned long *)0x4004401C)

#define T0MR2 (*(volatile unsigned long *)0x40044020)

#define T0MR3 (*(volatile unsigned long *)0x40044024)

#define T0CCR (*(volatile unsigned short *)0x40044028)
#define T0CCR_CAP0RE_MASK 0x1
#define T0CCR_CAP0RE 0x1
#define T0CCR_CAP0RE_BIT 0
#define T0CCR_CAP0FE_MASK 0x2
#define T0CCR_CAP0FE 0x2
#define T0CCR_CAP0FE_BIT 1
#define T0CCR_CAP0I_MASK 0x4
#define T0CCR_CAP0I 0x4
#define T0CCR_CAP0I_BIT 2
#define T0CCR_CAP1RE_MASK 0x8
#define T0CCR_CAP1RE 0x8
#define T0CCR_CAP1RE_BIT 3
#define T0CCR_CAP1FE_MASK 0x10
#define T0CCR_CAP1FE 0x10
#define T0CCR_CAP1FE_BIT 4
#define T0CCR_CAP1I_MASK 0x20
#define T0CCR_CAP1I 0x20
#define T0CCR_CAP1I_BIT 5
#define T0CCR_CAP2RE_MASK 0x40
#define T0CCR_CAP2RE 0x40
#define T0CCR_CAP2RE_BIT 6
#define T0CCR_CAP2FE_MASK 0x80
#define T0CCR_CAP2FE 0x80
#define T0CCR_CAP2FE_BIT 7
#define T0CCR_CAP2I_MASK 0x100
#define T0CCR_CAP2I 0x100
#define T0CCR_CAP2I_BIT 8
#define T0CCR_CAP3RE_MASK 0x200
#define T0CCR_CAP3RE 0x200
#define T0CCR_CAP3RE_BIT 9
#define T0CCR_CAP3FE_MASK 0x400
#define T0CCR_CAP3FE 0x400
#define T0CCR_CAP3FE_BIT 10
#define T0CCR_CAP3I_MASK 0x800
#define T0CCR_CAP3I 0x800
#define T0CCR_CAP3I_BIT 11

#define T0CR0 (*(volatile unsigned long *)0x4004402C)

#define T0CR1 (*(volatile unsigned long *)0x40044030)

#define T0CR2 (*(volatile unsigned long *)0x40044034)

#define T0CR3 (*(volatile unsigned long *)0x40044038)

#define T0EMR (*(volatile unsigned short *)0x4004403C)
#define T0EMR_EM0_MASK 0x1
#define T0EMR_EM0 0x1
#define T0EMR_EM0_BIT 0
#define T0EMR_EM1_MASK 0x2
#define T0EMR_EM1 0x2
#define T0EMR_EM1_BIT 1
#define T0EMR_EM2_MASK 0x4
#define T0EMR_EM2 0x4
#define T0EMR_EM2_BIT 2
#define T0EMR_EM3_MASK 0x8
#define T0EMR_EM3 0x8
#define T0EMR_EM3_BIT 3
#define T0EMR_EMC0_MASK 0x30
#define T0EMR_EMC0_BIT 4
#define T0EMR_EMC1_MASK 0xC0
#define T0EMR_EMC1_BIT 6
#define T0EMR_EMC2_MASK 0x300
#define T0EMR_EMC2_BIT 8
#define T0EMR_EMC3_MASK 0xC00
#define T0EMR_EMC3_BIT 10

#define T0CTCR (*(volatile unsigned long *)0x40044070)
#define T0CTCR_Counter_Timer_Mode_MASK 0x3
#define T0CTCR_Counter_Timer_Mode_BIT 0
#define T0CTCR_Count_Input_Select_MASK 0xC
#define T0CTCR_Count_Input_Select_BIT 2

#define ADC_STAT (*(volatile unsigned *)0x40048000)
#define ADC_STAT_TS_FIFO_OVERRUN_MASK 0x100
#define ADC_STAT_TS_FIFO_OVERRUN 0x100
#define ADC_STAT_TS_FIFO_OVERRUN_BIT 8
#define ADC_STAT_TS_FIFO_EMPTY_MASK 0x80
#define ADC_STAT_TS_FIFO_EMPTY 0x80
#define ADC_STAT_TS_FIFO_EMPTY_BIT 7

#define ADC_SELECT (*(volatile unsigned *)0x40048004)
#define ADC_SELECT_TS_Ref_Neg_MASK 0x300
#define ADC_SELECT_TS_Ref_Neg_BIT 8
#define ADC_SELECT_TS_Ref_Pos_MASK 0xC0
#define ADC_SELECT_TS_Ref_Pos_BIT 6
#define ADC_SELECT_TS_IN_MASK 0x30
#define ADC_SELECT_TS_IN_BIT 4
#define ADC_SELECT_TS_YMC_MASK 0x8
#define ADC_SELECT_TS_YMC 0x8
#define ADC_SELECT_TS_YMC_BIT 3
#define ADC_SELECT_TS_YPC_MASK 0x4
#define ADC_SELECT_TS_YPC 0x4
#define ADC_SELECT_TS_YPC_BIT 2
#define ADC_SELECT_TS_XMC_MASK 0x2
#define ADC_SELECT_TS_XMC 0x2
#define ADC_SELECT_TS_XMC_BIT 1
#define ADC_SELECT_TS_XPC_MASK 0x1
#define ADC_SELECT_TS_XPC 0x1
#define ADC_SELECT_TS_XPC_BIT 0

#define ADC_CTRL (*(volatile unsigned *)0x40048008)
#define ADC_CTRL_TS_FIFO_CTRL_MASK 0x1800
#define ADC_CTRL_TS_FIFO_CTRL_BIT 11
#define ADC_CTRL_TS_AUX_EN_MASK 0x400
#define ADC_CTRL_TS_AUX_EN 0x400
#define ADC_CTRL_TS_AUX_EN_BIT 10
#define ADC_CTRL_TS_X_ACC_MASK 0x380
#define ADC_CTRL_TS_X_ACC_BIT 7
#define ADC_CTRL_TS_Y_ACC_MASK 0x70
#define ADC_CTRL_TS_Y_ACC_BIT 4
#define ADC_CTRL_TS_POS_DET_MASK 0x8
#define ADC_CTRL_TS_POS_DET 0x8
#define ADC_CTRL_TS_POS_DET_BIT 3
#define ADC_CTRL_TS_ADC_PDN_CTRL_MASK 0x4
#define ADC_CTRL_TS_ADC_PDN_CTRL 0x4
#define ADC_CTRL_TS_ADC_PDN_CTRL_BIT 2
#define ADC_CTRL_TS_ADC_STROBE_MASK 0x2
#define ADC_CTRL_TS_ADC_STROBE 0x2
#define ADC_CTRL_TS_ADC_STROBE_BIT 1
#define ADC_CTRL_TS_AUTO_EN_MASK 0x1
#define ADC_CTRL_TS_AUTO_EN 0x1
#define ADC_CTRL_TS_AUTO_EN_BIT 0

#define TSC_SAMPLE_FIFO (*(volatile unsigned *)0x4004800C)
#define TSC_SAMPLE_FIFO_TSC_P_LEVEL_MASK 0x80000000
#define TSC_SAMPLE_FIFO_TSC_P_LEVEL 0x80000000
#define TSC_SAMPLE_FIFO_TSC_P_LEVEL_BIT 31
#define TSC_SAMPLE_FIFO_FIFO_EMPTY_MASK 0x40000000
#define TSC_SAMPLE_FIFO_FIFO_EMPTY 0x40000000
#define TSC_SAMPLE_FIFO_FIFO_EMPTY_BIT 30
#define TSC_SAMPLE_FIFO_FIFO_OVERRUN_MASK 0x20000000
#define TSC_SAMPLE_FIFO_FIFO_OVERRUN 0x20000000
#define TSC_SAMPLE_FIFO_FIFO_OVERRUN_BIT 29
#define TSC_SAMPLE_FIFO_TS_X_VALUE_MASK 0x3FF0000
#define TSC_SAMPLE_FIFO_TS_X_VALUE_BIT 16
#define TSC_SAMPLE_FIFO_ADC_VALUE_MASK 0x3FF
#define TSC_SAMPLE_FIFO_ADC_VALUE_BIT 0

#define TSC_DTR (*(volatile unsigned *)0x40048010)

#define TSC_RTR (*(volatile unsigned *)0x40048014)

#define TSC_UTR (*(volatile unsigned *)0x40048018)

#define TSC_TTR (*(volatile unsigned *)0x4004801C)

#define TSC_DXP (*(volatile unsigned *)0x40048020)

#define TSC_MIN_X (*(volatile unsigned *)0x40048024)

#define TSC_MAX_X (*(volatile unsigned *)0x40048028)

#define TSC_MIN_Y (*(volatile unsigned *)0x4004802C)

#define TSC_MAX_Y (*(volatile unsigned *)0x40048030)

#define TSC_AUX_UTR (*(volatile unsigned *)0x40048034)

#define TSC_AUX_MIN (*(volatile unsigned *)0x40048038)

#define TSC_AUX_MAX (*(volatile unsigned *)0x4004803C)

#define TSC_AUX_VALUE (*(volatile unsigned *)0x40048044)

#define ADC_VALUE (*(volatile unsigned *)0x40048048)
#define ADC_VALUE_TSC_P_LEVEL_MASK 0x400
#define ADC_VALUE_TSC_P_LEVEL 0x400
#define ADC_VALUE_TSC_P_LEVEL_BIT 10
#define ADC_VALUE_ADC_VALUE_MASK 0x3FF
#define ADC_VALUE_ADC_VALUE_BIT 0

#define ADSEL (*(volatile unsigned *)0x40048004)
#define ADSEL_AD_Ref_Neg_MASK 0x300
#define ADSEL_AD_Ref_Neg_BIT 8
#define ADSEL_AD_Ref_Pos_MASK 0xC0
#define ADSEL_AD_Ref_Pos_BIT 6
#define ADSEL_AD_IN_MASK 0x30
#define ADSEL_AD_IN_BIT 4

#define ADCON (*(volatile unsigned *)0x40048008)
#define ADCON_AD_ACC_MASK 0x380
#define ADCON_AD_ACC_BIT 7
#define ADCON_AD_PDN_CTRL_MASK 0x4
#define ADCON_AD_PDN_CTRL 0x4
#define ADCON_AD_PDN_CTRL_BIT 2
#define ADCON_AD_STROBE_MASK 0x2
#define ADCON_AD_STROBE 0x2
#define ADCON_AD_STROBE_BIT 1

#define ADDAT (*(volatile unsigned *)0x40048048)
#define ADDAT_ADC_VALUE_MASK 0x3FF
#define ADDAT_ADC_VALUE_BIT 0

#define T1IR (*(volatile unsigned char *)0x4004C000)
#define T1IR_MR0_MASK 0x1
#define T1IR_MR0 0x1
#define T1IR_MR0_BIT 0
#define T1IR_MR1_MASK 0x2
#define T1IR_MR1 0x2
#define T1IR_MR1_BIT 1
#define T1IR_MR2_MASK 0x4
#define T1IR_MR2 0x4
#define T1IR_MR2_BIT 2
#define T1IR_MR3_MASK 0x8
#define T1IR_MR3 0x8
#define T1IR_MR3_BIT 3
#define T1IR_CR0_MASK 0x10
#define T1IR_CR0 0x10
#define T1IR_CR0_BIT 4
#define T1IR_CR1_MASK 0x20
#define T1IR_CR1 0x20
#define T1IR_CR1_BIT 5
#define T1IR_CR2_MASK 0x40
#define T1IR_CR2 0x40
#define T1IR_CR2_BIT 6
#define T1IR_CR3_MASK 0x80
#define T1IR_CR3 0x80
#define T1IR_CR3_BIT 7

#define T1TCR (*(volatile unsigned char *)0x4004C004)
#define T1TCR_Counter_Enable_MASK 0x1
#define T1TCR_Counter_Enable 0x1
#define T1TCR_Counter_Enable_BIT 0
#define T1TCR_Counter_Reset_MASK 0x2
#define T1TCR_Counter_Reset 0x2
#define T1TCR_Counter_Reset_BIT 1

#define T1TC (*(volatile unsigned long *)0x4004C008)

#define T1PR (*(volatile unsigned long *)0x4004C00C)

#define T1PC (*(volatile unsigned long *)0x4004C010)

#define T1MCR (*(volatile unsigned short *)0x4004C014)
#define T1MCR_MR0I_MASK 0x1
#define T1MCR_MR0I 0x1
#define T1MCR_MR0I_BIT 0
#define T1MCR_MR0R_MASK 0x2
#define T1MCR_MR0R 0x2
#define T1MCR_MR0R_BIT 1
#define T1MCR_MR0S_MASK 0x4
#define T1MCR_MR0S 0x4
#define T1MCR_MR0S_BIT 2
#define T1MCR_MR1I_MASK 0x8
#define T1MCR_MR1I 0x8
#define T1MCR_MR1I_BIT 3
#define T1MCR_MR1R_MASK 0x10
#define T1MCR_MR1R 0x10
#define T1MCR_MR1R_BIT 4
#define T1MCR_MR1S_MASK 0x20
#define T1MCR_MR1S 0x20
#define T1MCR_MR1S_BIT 5
#define T1MCR_MR2I_MASK 0x40
#define T1MCR_MR2I 0x40
#define T1MCR_MR2I_BIT 6
#define T1MCR_MR2R_MASK 0x80
#define T1MCR_MR2R 0x80
#define T1MCR_MR2R_BIT 7
#define T1MCR_MR2S_MASK 0x100
#define T1MCR_MR2S 0x100
#define T1MCR_MR2S_BIT 8
#define T1MCR_MR3I_MASK 0x200
#define T1MCR_MR3I 0x200
#define T1MCR_MR3I_BIT 9
#define T1MCR_MR3R_MASK 0x400
#define T1MCR_MR3R 0x400
#define T1MCR_MR3R_BIT 10
#define T1MCR_MR3S_MASK 0x800
#define T1MCR_MR3S 0x800
#define T1MCR_MR3S_BIT 11

#define T1MR0 (*(volatile unsigned long *)0x4004C018)

#define T1MR1 (*(volatile unsigned long *)0x4004C01C)

#define T1MR2 (*(volatile unsigned long *)0x4004C020)

#define T1MR3 (*(volatile unsigned long *)0x4004C024)

#define T1CCR (*(volatile unsigned short *)0x4004C028)
#define T1CCR_CAP0RE_MASK 0x1
#define T1CCR_CAP0RE 0x1
#define T1CCR_CAP0RE_BIT 0
#define T1CCR_CAP0FE_MASK 0x2
#define T1CCR_CAP0FE 0x2
#define T1CCR_CAP0FE_BIT 1
#define T1CCR_CAP0I_MASK 0x4
#define T1CCR_CAP0I 0x4
#define T1CCR_CAP0I_BIT 2
#define T1CCR_CAP1RE_MASK 0x8
#define T1CCR_CAP1RE 0x8
#define T1CCR_CAP1RE_BIT 3
#define T1CCR_CAP1FE_MASK 0x10
#define T1CCR_CAP1FE 0x10
#define T1CCR_CAP1FE_BIT 4
#define T1CCR_CAP1I_MASK 0x20
#define T1CCR_CAP1I 0x20
#define T1CCR_CAP1I_BIT 5
#define T1CCR_CAP2RE_MASK 0x40
#define T1CCR_CAP2RE 0x40
#define T1CCR_CAP2RE_BIT 6
#define T1CCR_CAP2FE_MASK 0x80
#define T1CCR_CAP2FE 0x80
#define T1CCR_CAP2FE_BIT 7
#define T1CCR_CAP2I_MASK 0x100
#define T1CCR_CAP2I 0x100
#define T1CCR_CAP2I_BIT 8
#define T1CCR_CAP3RE_MASK 0x200
#define T1CCR_CAP3RE 0x200
#define T1CCR_CAP3RE_BIT 9
#define T1CCR_CAP3FE_MASK 0x400
#define T1CCR_CAP3FE 0x400
#define T1CCR_CAP3FE_BIT 10
#define T1CCR_CAP3I_MASK 0x800
#define T1CCR_CAP3I 0x800
#define T1CCR_CAP3I_BIT 11

#define T1CR0 (*(volatile unsigned long *)0x4004C02C)

#define T1CR1 (*(volatile unsigned long *)0x4004C030)

#define T1CR2 (*(volatile unsigned long *)0x4004C034)

#define T1CR3 (*(volatile unsigned long *)0x4004C038)

#define T1EMR (*(volatile unsigned short *)0x4004C03C)
#define T1EMR_EM0_MASK 0x1
#define T1EMR_EM0 0x1
#define T1EMR_EM0_BIT 0
#define T1EMR_EM1_MASK 0x2
#define T1EMR_EM1 0x2
#define T1EMR_EM1_BIT 1
#define T1EMR_EM2_MASK 0x4
#define T1EMR_EM2 0x4
#define T1EMR_EM2_BIT 2
#define T1EMR_EM3_MASK 0x8
#define T1EMR_EM3 0x8
#define T1EMR_EM3_BIT 3
#define T1EMR_EMC0_MASK 0x30
#define T1EMR_EMC0_BIT 4
#define T1EMR_EMC1_MASK 0xC0
#define T1EMR_EMC1_BIT 6
#define T1EMR_EMC2_MASK 0x300
#define T1EMR_EMC2_BIT 8
#define T1EMR_EMC3_MASK 0xC00
#define T1EMR_EMC3_BIT 10

#define T1CTCR (*(volatile unsigned long *)0x4004C070)
#define T1CTCR_Counter_Timer_Mode_MASK 0x3
#define T1CTCR_Counter_Timer_Mode_BIT 0
#define T1CTCR_Count_Input_Select_MASK 0xC
#define T1CTCR_Count_Input_Select_BIT 2

#define KS_DEB (*(volatile unsigned *)0x40050000)
#define KS_DEB_Keypad_debouncing_duration_MASK 0xFF
#define KS_DEB_Keypad_debouncing_duration_BIT 0

#define KS_STATE_COND (*(volatile unsigned *)0x40050004)
#define KS_STATE_COND_STATE_MASK 0x3
#define KS_STATE_COND_STATE_BIT 0

#define KS_IRQ (*(volatile unsigned *)0x40050008)
#define KS_IRQ_KIRQN_MASK 0x1
#define KS_IRQ_KIRQN 0x1
#define KS_IRQ_KIRQN_BIT 0

#define KS_SCAN_CTL (*(volatile unsigned *)0x4005000C)
#define KS_SCAN_CTL_SCN_CTL_MASK 0xFF
#define KS_SCAN_CTL_SCN_CTL_BIT 0

#define KS_FAST_TST (*(volatile unsigned *)0x40050010)
#define KS_FAST_TST_BIT0_MASK 0x1
#define KS_FAST_TST_BIT0 0x1
#define KS_FAST_TST_BIT0_BIT 0
#define KS_FAST_TST_BIT1_MASK 0x2
#define KS_FAST_TST_BIT1 0x2
#define KS_FAST_TST_BIT1_BIT 1

#define KS_MATRIX_DIM (*(volatile unsigned *)0x40050014)
#define KS_MATRIX_DIM_MX_DIM_MASK 0xF
#define KS_MATRIX_DIM_MX_DIM_BIT 0

#define KS_DATA0 (*(volatile unsigned *)0x40050040)
#define KS_DATA0_KEY_R0_C_MASK 0xFF
#define KS_DATA0_KEY_R0_C_BIT 0

#define KS_DATA1 (*(volatile unsigned *)0x40050044)
#define KS_DATA1_KEY_R1_C_MASK 0xFF
#define KS_DATA1_KEY_R1_C_BIT 0

#define KS_DATA2 (*(volatile unsigned *)0x40050048)
#define KS_DATA2_KEY_R2_C_MASK 0xFF
#define KS_DATA2_KEY_R2_C_BIT 0

#define KS_DATA3 (*(volatile unsigned *)0x4005004C)
#define KS_DATA3_KEY_R3_C_MASK 0xFF
#define KS_DATA3_KEY_R3_C_BIT 0

#define KS_DATA4 (*(volatile unsigned *)0x40050050)
#define KS_DATA4_KEY_R4_C_MASK 0xFF
#define KS_DATA4_KEY_R4_C_BIT 0

#define KS_DATA5 (*(volatile unsigned *)0x40050054)
#define KS_DATA5_KEY_R5_C_MASK 0xFF
#define KS_DATA5_KEY_R5_C_BIT 0

#define KS_DATA6 (*(volatile unsigned *)0x40050058)
#define KS_DATA6_KEY_R6_C_MASK 0xFF
#define KS_DATA6_KEY_R6_C_BIT 0

#define KS_DATA7 (*(volatile unsigned *)0x4005005C)
#define KS_DATA7_KEY_R7_C_MASK 0xFF
#define KS_DATA7_KEY_R7_C_BIT 0

#define UART_CTRL (*(volatile unsigned *)0x40054000)
#define UART_CTRL_UART3_MD_CTRL_MASK 0x800
#define UART_CTRL_UART3_MD_CTRL 0x800
#define UART_CTRL_UART3_MD_CTRL_BIT 11
#define UART_CTRL_HDPX_INV_MASK 0x400
#define UART_CTRL_HDPX_INV 0x400
#define UART_CTRL_HDPX_INV_BIT 10
#define UART_CTRL_HDPX_EN_MASK 0x200
#define UART_CTRL_HDPX_EN 0x200
#define UART_CTRL_HDPX_EN_BIT 9
#define UART_CTRL_UART6_IRDA_MASK 0x20
#define UART_CTRL_UART6_IRDA 0x20
#define UART_CTRL_UART6_IRDA_BIT 5
#define UART_CTRL_IRTX6_INV_MASK 0x10
#define UART_CTRL_IRTX6_INV 0x10
#define UART_CTRL_IRTX6_INV_BIT 4
#define UART_CTRL_IRRX6_INV_MASK 0x8
#define UART_CTRL_IRRX6_INV 0x8
#define UART_CTRL_IRRX6_INV_BIT 3
#define UART_CTRL_IR_RxLength_MASK 0x4
#define UART_CTRL_IR_RxLength 0x4
#define UART_CTRL_IR_RxLength_BIT 2
#define UART_CTRL_IR_TxLength_MASK 0x2
#define UART_CTRL_IR_TxLength 0x2
#define UART_CTRL_IR_TxLength_BIT 1
#define UART_CTRL_UART5_MODE_MASK 0x1
#define UART_CTRL_UART5_MODE 0x1
#define UART_CTRL_UART5_MODE_BIT 0

#define UART_CLKMODE (*(volatile unsigned *)0x40054004)
#define UART_CLKMODE_CLK_STATX_MASK 0x7F0000
#define UART_CLKMODE_CLK_STATX_BIT 16
#define UART_CLKMODE_CLK_STAT_MASK 0x4000
#define UART_CLKMODE_CLK_STAT 0x4000
#define UART_CLKMODE_CLK_STAT_BIT 14
#define UART_CLKMODE_UART6_CLK_MASK 0xC00
#define UART_CLKMODE_UART6_CLK_BIT 10
#define UART_CLKMODE_UART5_CLK_MASK 0x300
#define UART_CLKMODE_UART5_CLK_BIT 8
#define UART_CLKMODE_UART4_CLK_MASK 0x7C0
#define UART_CLKMODE_UART4_CLK_BIT 6
#define UART_CLKMODE_UART3_CLK_MASK 0x10
#define UART_CLKMODE_UART3_CLK 0x10
#define UART_CLKMODE_UART3_CLK_BIT 4

#define UART_LOOP (*(volatile unsigned *)0x40054008)
#define UART_LOOP_LOOPBACK7_MASK 0x40
#define UART_LOOP_LOOPBACK7 0x40
#define UART_LOOP_LOOPBACK7_BIT 6
#define UART_LOOP_LOOPBACK6_MASK 0x20
#define UART_LOOP_LOOPBACK6 0x20
#define UART_LOOP_LOOPBACK6_BIT 5
#define UART_LOOP_LOOPBACK5_MASK 0x10
#define UART_LOOP_LOOPBACK5 0x10
#define UART_LOOP_LOOPBACK5_BIT 4
#define UART_LOOP_LOOPBACK4_MASK 0x8
#define UART_LOOP_LOOPBACK4 0x8
#define UART_LOOP_LOOPBACK4_BIT 3
#define UART_LOOP_LOOPBACK3_MASK 0x4
#define UART_LOOP_LOOPBACK3 0x4
#define UART_LOOP_LOOPBACK3_BIT 2
#define UART_LOOP_LOOPBACK2_MASK 0x2
#define UART_LOOP_LOOPBACK2 0x2
#define UART_LOOP_LOOPBACK2_BIT 1
#define UART_LOOP_LOOPBACK1_MASK 0x1
#define UART_LOOP_LOOPBACK1 0x1
#define UART_LOOP_LOOPBACK1_BIT 0

#define T2IR (*(volatile unsigned char *)0x40058000)
#define T2IR_MR0_MASK 0x1
#define T2IR_MR0 0x1
#define T2IR_MR0_BIT 0
#define T2IR_MR1_MASK 0x2
#define T2IR_MR1 0x2
#define T2IR_MR1_BIT 1
#define T2IR_MR2_MASK 0x4
#define T2IR_MR2 0x4
#define T2IR_MR2_BIT 2
#define T2IR_MR3_MASK 0x8
#define T2IR_MR3 0x8
#define T2IR_MR3_BIT 3
#define T2IR_CR0_MASK 0x10
#define T2IR_CR0 0x10
#define T2IR_CR0_BIT 4
#define T2IR_CR1_MASK 0x20
#define T2IR_CR1 0x20
#define T2IR_CR1_BIT 5
#define T2IR_CR2_MASK 0x40
#define T2IR_CR2 0x40
#define T2IR_CR2_BIT 6
#define T2IR_CR3_MASK 0x80
#define T2IR_CR3 0x80
#define T2IR_CR3_BIT 7

#define T2TCR (*(volatile unsigned char *)0x40058004)
#define T2TCR_Counter_Enable_MASK 0x1
#define T2TCR_Counter_Enable 0x1
#define T2TCR_Counter_Enable_BIT 0
#define T2TCR_Counter_Reset_MASK 0x2
#define T2TCR_Counter_Reset 0x2
#define T2TCR_Counter_Reset_BIT 1

#define T2TC (*(volatile unsigned long *)0x40058008)

#define T2PR (*(volatile unsigned long *)0x4005800C)

#define T2PC (*(volatile unsigned long *)0x40058010)

#define T2MCR (*(volatile unsigned short *)0x40058014)
#define T2MCR_MR0I_MASK 0x1
#define T2MCR_MR0I 0x1
#define T2MCR_MR0I_BIT 0
#define T2MCR_MR0R_MASK 0x2
#define T2MCR_MR0R 0x2
#define T2MCR_MR0R_BIT 1
#define T2MCR_MR0S_MASK 0x4
#define T2MCR_MR0S 0x4
#define T2MCR_MR0S_BIT 2
#define T2MCR_MR1I_MASK 0x8
#define T2MCR_MR1I 0x8
#define T2MCR_MR1I_BIT 3
#define T2MCR_MR1R_MASK 0x10
#define T2MCR_MR1R 0x10
#define T2MCR_MR1R_BIT 4
#define T2MCR_MR1S_MASK 0x20
#define T2MCR_MR1S 0x20
#define T2MCR_MR1S_BIT 5
#define T2MCR_MR2I_MASK 0x40
#define T2MCR_MR2I 0x40
#define T2MCR_MR2I_BIT 6
#define T2MCR_MR2R_MASK 0x80
#define T2MCR_MR2R 0x80
#define T2MCR_MR2R_BIT 7
#define T2MCR_MR2S_MASK 0x100
#define T2MCR_MR2S 0x100
#define T2MCR_MR2S_BIT 8
#define T2MCR_MR3I_MASK 0x200
#define T2MCR_MR3I 0x200
#define T2MCR_MR3I_BIT 9
#define T2MCR_MR3R_MASK 0x400
#define T2MCR_MR3R 0x400
#define T2MCR_MR3R_BIT 10
#define T2MCR_MR3S_MASK 0x800
#define T2MCR_MR3S 0x800
#define T2MCR_MR3S_BIT 11

#define T2MR0 (*(volatile unsigned long *)0x40058018)

#define T2MR1 (*(volatile unsigned long *)0x4005801C)

#define T2MR2 (*(volatile unsigned long *)0x40058020)

#define T2MR3 (*(volatile unsigned long *)0x40058024)

#define T2CCR (*(volatile unsigned short *)0x40058028)
#define T2CCR_CAP0RE_MASK 0x1
#define T2CCR_CAP0RE 0x1
#define T2CCR_CAP0RE_BIT 0
#define T2CCR_CAP0FE_MASK 0x2
#define T2CCR_CAP0FE 0x2
#define T2CCR_CAP0FE_BIT 1
#define T2CCR_CAP0I_MASK 0x4
#define T2CCR_CAP0I 0x4
#define T2CCR_CAP0I_BIT 2
#define T2CCR_CAP1RE_MASK 0x8
#define T2CCR_CAP1RE 0x8
#define T2CCR_CAP1RE_BIT 3
#define T2CCR_CAP1FE_MASK 0x10
#define T2CCR_CAP1FE 0x10
#define T2CCR_CAP1FE_BIT 4
#define T2CCR_CAP1I_MASK 0x20
#define T2CCR_CAP1I 0x20
#define T2CCR_CAP1I_BIT 5
#define T2CCR_CAP2RE_MASK 0x40
#define T2CCR_CAP2RE 0x40
#define T2CCR_CAP2RE_BIT 6
#define T2CCR_CAP2FE_MASK 0x80
#define T2CCR_CAP2FE 0x80
#define T2CCR_CAP2FE_BIT 7
#define T2CCR_CAP2I_MASK 0x100
#define T2CCR_CAP2I 0x100
#define T2CCR_CAP2I_BIT 8
#define T2CCR_CAP3RE_MASK 0x200
#define T2CCR_CAP3RE 0x200
#define T2CCR_CAP3RE_BIT 9
#define T2CCR_CAP3FE_MASK 0x400
#define T2CCR_CAP3FE 0x400
#define T2CCR_CAP3FE_BIT 10
#define T2CCR_CAP3I_MASK 0x800
#define T2CCR_CAP3I 0x800
#define T2CCR_CAP3I_BIT 11

#define T2CR0 (*(volatile unsigned long *)0x4005802C)

#define T2CR1 (*(volatile unsigned long *)0x40058030)

#define T2CR2 (*(volatile unsigned long *)0x40058034)

#define T2CR3 (*(volatile unsigned long *)0x40058038)

#define T2EMR (*(volatile unsigned short *)0x4005803C)
#define T2EMR_EM0_MASK 0x1
#define T2EMR_EM0 0x1
#define T2EMR_EM0_BIT 0
#define T2EMR_EM1_MASK 0x2
#define T2EMR_EM1 0x2
#define T2EMR_EM1_BIT 1
#define T2EMR_EM2_MASK 0x4
#define T2EMR_EM2 0x4
#define T2EMR_EM2_BIT 2
#define T2EMR_EM3_MASK 0x8
#define T2EMR_EM3 0x8
#define T2EMR_EM3_BIT 3
#define T2EMR_EMC0_MASK 0x30
#define T2EMR_EMC0_BIT 4
#define T2EMR_EMC1_MASK 0xC0
#define T2EMR_EMC1_BIT 6
#define T2EMR_EMC2_MASK 0x300
#define T2EMR_EMC2_BIT 8
#define T2EMR_EMC3_MASK 0xC00
#define T2EMR_EMC3_BIT 10

#define T2CTCR (*(volatile unsigned long *)0x40058070)
#define T2CTCR_Counter_Timer_Mode_MASK 0x3
#define T2CTCR_Counter_Timer_Mode_BIT 0
#define T2CTCR_Count_Input_Select_MASK 0xC
#define T2CTCR_Count_Input_Select_BIT 2

#define SPWM1 (*(volatile unsigned *)0x4005C000)
#define SPWM1_PWM1_EN_MASK 0x80000000
#define SPWM1_PWM1_EN 0x80000000
#define SPWM1_PWM1_EN_BIT 31
#define SPWM1_PWM1_PIN_LEVEL_MASK 0x40000000
#define SPWM1_PWM1_PIN_LEVEL 0x40000000
#define SPWM1_PWM1_PIN_LEVEL_BIT 30
#define SPWM1_PWM1_RELOADV_MASK 0xFF00
#define SPWM1_PWM1_RELOADV_BIT 8
#define SPWM1_PWM1_DUTY_MASK 0xFF
#define SPWM1_PWM1_DUTY_BIT 0

#define SPWM2 (*(volatile unsigned *)0x4005C004)
#define SPWM2_PWM2_EN_MASK 0x80000000
#define SPWM2_PWM2_EN 0x80000000
#define SPWM2_PWM2_EN_BIT 31
#define SPWM2_PWM2_PIN_LEVEL_MASK 0x40000000
#define SPWM2_PWM2_PIN_LEVEL 0x40000000
#define SPWM2_PWM2_PIN_LEVEL_BIT 30
#define SPWM2_PWM2_INT_MASK 0x20000000
#define SPWM2_PWM2_INT 0x20000000
#define SPWM2_PWM2_INT_BIT 29
#define SPWM2_PWM2_RELOADV_MASK 0xFF00
#define SPWM2_PWM2_RELOADV_BIT 8
#define SPWM2_PWM2_DUTY_MASK 0xFF
#define SPWM2_PWM2_DUTY_BIT 0

#define T3IR (*(volatile unsigned char *)0x40060000)
#define T3IR_MR0_MASK 0x1
#define T3IR_MR0 0x1
#define T3IR_MR0_BIT 0
#define T3IR_MR1_MASK 0x2
#define T3IR_MR1 0x2
#define T3IR_MR1_BIT 1
#define T3IR_MR2_MASK 0x4
#define T3IR_MR2 0x4
#define T3IR_MR2_BIT 2
#define T3IR_MR3_MASK 0x8
#define T3IR_MR3 0x8
#define T3IR_MR3_BIT 3
#define T3IR_CR0_MASK 0x10
#define T3IR_CR0 0x10
#define T3IR_CR0_BIT 4
#define T3IR_CR1_MASK 0x20
#define T3IR_CR1 0x20
#define T3IR_CR1_BIT 5
#define T3IR_CR2_MASK 0x40
#define T3IR_CR2 0x40
#define T3IR_CR2_BIT 6
#define T3IR_CR3_MASK 0x80
#define T3IR_CR3 0x80
#define T3IR_CR3_BIT 7

#define T3TCR (*(volatile unsigned char *)0x40060004)
#define T3TCR_Counter_Enable_MASK 0x1
#define T3TCR_Counter_Enable 0x1
#define T3TCR_Counter_Enable_BIT 0
#define T3TCR_Counter_Reset_MASK 0x2
#define T3TCR_Counter_Reset 0x2
#define T3TCR_Counter_Reset_BIT 1

#define T3TC (*(volatile unsigned long *)0x40060008)

#define T3PR (*(volatile unsigned long *)0x4006000C)

#define T3PC (*(volatile unsigned long *)0x40060010)

#define T3MCR (*(volatile unsigned short *)0x40060014)
#define T3MCR_MR0I_MASK 0x1
#define T3MCR_MR0I 0x1
#define T3MCR_MR0I_BIT 0
#define T3MCR_MR0R_MASK 0x2
#define T3MCR_MR0R 0x2
#define T3MCR_MR0R_BIT 1
#define T3MCR_MR0S_MASK 0x4
#define T3MCR_MR0S 0x4
#define T3MCR_MR0S_BIT 2
#define T3MCR_MR1I_MASK 0x8
#define T3MCR_MR1I 0x8
#define T3MCR_MR1I_BIT 3
#define T3MCR_MR1R_MASK 0x10
#define T3MCR_MR1R 0x10
#define T3MCR_MR1R_BIT 4
#define T3MCR_MR1S_MASK 0x20
#define T3MCR_MR1S 0x20
#define T3MCR_MR1S_BIT 5
#define T3MCR_MR2I_MASK 0x40
#define T3MCR_MR2I 0x40
#define T3MCR_MR2I_BIT 6
#define T3MCR_MR2R_MASK 0x80
#define T3MCR_MR2R 0x80
#define T3MCR_MR2R_BIT 7
#define T3MCR_MR2S_MASK 0x100
#define T3MCR_MR2S 0x100
#define T3MCR_MR2S_BIT 8
#define T3MCR_MR3I_MASK 0x200
#define T3MCR_MR3I 0x200
#define T3MCR_MR3I_BIT 9
#define T3MCR_MR3R_MASK 0x400
#define T3MCR_MR3R 0x400
#define T3MCR_MR3R_BIT 10
#define T3MCR_MR3S_MASK 0x800
#define T3MCR_MR3S 0x800
#define T3MCR_MR3S_BIT 11

#define T3MR0 (*(volatile unsigned long *)0x40060018)

#define T3MR1 (*(volatile unsigned long *)0x4006001C)

#define T3MR2 (*(volatile unsigned long *)0x40060020)

#define T3MR3 (*(volatile unsigned long *)0x40060024)

#define T3CCR (*(volatile unsigned short *)0x40060028)
#define T3CCR_CAP0RE_MASK 0x1
#define T3CCR_CAP0RE 0x1
#define T3CCR_CAP0RE_BIT 0
#define T3CCR_CAP0FE_MASK 0x2
#define T3CCR_CAP0FE 0x2
#define T3CCR_CAP0FE_BIT 1
#define T3CCR_CAP0I_MASK 0x4
#define T3CCR_CAP0I 0x4
#define T3CCR_CAP0I_BIT 2
#define T3CCR_CAP1RE_MASK 0x8
#define T3CCR_CAP1RE 0x8
#define T3CCR_CAP1RE_BIT 3
#define T3CCR_CAP1FE_MASK 0x10
#define T3CCR_CAP1FE 0x10
#define T3CCR_CAP1FE_BIT 4
#define T3CCR_CAP1I_MASK 0x20
#define T3CCR_CAP1I 0x20
#define T3CCR_CAP1I_BIT 5
#define T3CCR_CAP2RE_MASK 0x40
#define T3CCR_CAP2RE 0x40
#define T3CCR_CAP2RE_BIT 6
#define T3CCR_CAP2FE_MASK 0x80
#define T3CCR_CAP2FE 0x80
#define T3CCR_CAP2FE_BIT 7
#define T3CCR_CAP2I_MASK 0x100
#define T3CCR_CAP2I 0x100
#define T3CCR_CAP2I_BIT 8
#define T3CCR_CAP3RE_MASK 0x200
#define T3CCR_CAP3RE 0x200
#define T3CCR_CAP3RE_BIT 9
#define T3CCR_CAP3FE_MASK 0x400
#define T3CCR_CAP3FE 0x400
#define T3CCR_CAP3FE_BIT 10
#define T3CCR_CAP3I_MASK 0x800
#define T3CCR_CAP3I 0x800
#define T3CCR_CAP3I_BIT 11

#define T3CR0 (*(volatile unsigned long *)0x4006002C)

#define T3CR1 (*(volatile unsigned long *)0x40060030)

#define T3CR2 (*(volatile unsigned long *)0x40060034)

#define T3CR3 (*(volatile unsigned long *)0x40060038)

#define T3EMR (*(volatile unsigned short *)0x4006003C)
#define T3EMR_EM0_MASK 0x1
#define T3EMR_EM0 0x1
#define T3EMR_EM0_BIT 0
#define T3EMR_EM1_MASK 0x2
#define T3EMR_EM1 0x2
#define T3EMR_EM1_BIT 1
#define T3EMR_EM2_MASK 0x4
#define T3EMR_EM2 0x4
#define T3EMR_EM2_BIT 2
#define T3EMR_EM3_MASK 0x8
#define T3EMR_EM3 0x8
#define T3EMR_EM3_BIT 3
#define T3EMR_EMC0_MASK 0x30
#define T3EMR_EMC0_BIT 4
#define T3EMR_EMC1_MASK 0xC0
#define T3EMR_EMC1_BIT 6
#define T3EMR_EMC2_MASK 0x300
#define T3EMR_EMC2_BIT 8
#define T3EMR_EMC3_MASK 0xC00
#define T3EMR_EMC3_BIT 10

#define T3CTCR (*(volatile unsigned long *)0x40060070)
#define T3CTCR_Counter_Timer_Mode_MASK 0x3
#define T3CTCR_Counter_Timer_Mode_BIT 0
#define T3CTCR_Count_Input_Select_MASK 0xC
#define T3CTCR_Count_Input_Select_BIT 2

#define U3RBR (*(volatile unsigned char *)0x40080000)

#define U3THR (*(volatile unsigned char *)0x40080000)

#define U3DLL (*(volatile unsigned char *)0x40080000)

#define U3DLM (*(volatile unsigned char *)0x40080004)

#define U3IER (*(volatile unsigned char *)0x40080004)
#define U3IER_Rx_Line_Status_Interrupt_Enable_MASK 0x4
#define U3IER_Rx_Line_Status_Interrupt_Enable 0x4
#define U3IER_Rx_Line_Status_Interrupt_Enable_BIT 2
#define U3IER_THRE_Interrupt_Enable_MASK 0x2
#define U3IER_THRE_Interrupt_Enable 0x2
#define U3IER_THRE_Interrupt_Enable_BIT 1
#define U3IER_RDA_Interrupt_Enable_MASK 0x1
#define U3IER_RDA_Interrupt_Enable 0x1
#define U3IER_RDA_Interrupt_Enable_BIT 0

#define U3IIR (*(volatile unsigned char *)0x40080008)
#define U3IIR_FIFO_Enable_MASK 0xC0
#define U3IIR_FIFO_Enable_BIT 6
#define U3IIR_Interrupt_Identification_MASK 0xE
#define U3IIR_Interrupt_Identification_BIT 1
#define U3IIR_Interrupt_Pending_MASK 0x1
#define U3IIR_Interrupt_Pending 0x1
#define U3IIR_Interrupt_Pending_BIT 0

#define U3FCR (*(volatile unsigned char *)0x40080008)
#define U3FCR_Receiver_Trigger_Level_Select_MASK 0xC0
#define U3FCR_Receiver_Trigger_Level_Select_BIT 6
#define U3FCR_Transmitter_Trigger_Level_Select_MASK 0x30
#define U3FCR_Transmitter_Trigger_Level_Select_BIT 4
#define U3FCR_FIFO_Control_MASK 0x8
#define U3FCR_FIFO_Control 0x8
#define U3FCR_FIFO_Control_BIT 3
#define U3FCR_Transmitter_FIFO_Reset_MASK 0x4
#define U3FCR_Transmitter_FIFO_Reset 0x4
#define U3FCR_Transmitter_FIFO_Reset_BIT 2
#define U3FCR_Receiver_FIFO_Reset_MASK 0x2
#define U3FCR_Receiver_FIFO_Reset 0x2
#define U3FCR_Receiver_FIFO_Reset_BIT 1
#define U3FCR_FIFO_Enable_MASK 0x1
#define U3FCR_FIFO_Enable 0x1
#define U3FCR_FIFO_Enable_BIT 0

#define U3LCR (*(volatile unsigned char *)0x4008000C)
#define U3LCR_Divisor_Latch_Access_Bit_MASK 0x80
#define U3LCR_Divisor_Latch_Access_Bit 0x80
#define U3LCR_Divisor_Latch_Access_Bit_BIT 7
#define U3LCR_Break_Control_MASK 0x40
#define U3LCR_Break_Control 0x40
#define U3LCR_Break_Control_BIT 6
#define U3LCR_Parity_Select_MASK 0x30
#define U3LCR_Parity_Select_BIT 4
#define U3LCR_Parity_Enable_MASK 0x8
#define U3LCR_Parity_Enable 0x8
#define U3LCR_Parity_Enable_BIT 3
#define U3LCR_Stop_Bit_Select_MASK 0x4
#define U3LCR_Stop_Bit_Select 0x4
#define U3LCR_Stop_Bit_Select_BIT 2
#define U3LCR_Word_Length_Select_MASK 0x3
#define U3LCR_Word_Length_Select_BIT 0

#define U3MCR (*(volatile unsigned char *)0x40080010)
#define U3MCR_Loopback_Mode_Select_MASK 0x10
#define U3MCR_Loopback_Mode_Select 0x10
#define U3MCR_Loopback_Mode_Select_BIT 4
#define U3MCR_RTS_Control_MASK 0x2
#define U3MCR_RTS_Control 0x2
#define U3MCR_RTS_Control_BIT 1
#define U3MCR_DTR_Control_MASK 0x1
#define U3MCR_DTR_Control 0x1
#define U3MCR_DTR_Control_BIT 0

#define U3LSR (*(volatile unsigned char *)0x40080014)
#define U3LSR_FIFO_Rx_Error_MASK 0x80
#define U3LSR_FIFO_Rx_Error 0x80
#define U3LSR_FIFO_Rx_Error_BIT 7
#define U3LSR_TEMT_MASK 0x40
#define U3LSR_TEMT 0x40
#define U3LSR_TEMT_BIT 6
#define U3LSR_THRE_MASK 0x20
#define U3LSR_THRE 0x20
#define U3LSR_THRE_BIT 5
#define U3LSR_BI_MASK 0x10
#define U3LSR_BI 0x10
#define U3LSR_BI_BIT 4
#define U3LSR_FE_MASK 0x8
#define U3LSR_FE 0x8
#define U3LSR_FE_BIT 3
#define U3LSR_PE_MASK 0x4
#define U3LSR_PE 0x4
#define U3LSR_PE_BIT 2
#define U3LSR_OE_MASK 0x2
#define U3LSR_OE 0x2
#define U3LSR_OE_BIT 1
#define U3LSR_RDR_MASK 0x1
#define U3LSR_RDR 0x1
#define U3LSR_RDR_BIT 0

#define U3MSR (*(volatile unsigned char *)0x40080018)
#define U3MSR_dCTS_MASK 0x1
#define U3MSR_dCTS 0x1
#define U3MSR_dCTS_BIT 0
#define U3MSR_dDSR_MASK 0x2
#define U3MSR_dDSR 0x2
#define U3MSR_dDSR_BIT 1
#define U3MSR_TERI_MASK 0x4
#define U3MSR_TERI 0x4
#define U3MSR_TERI_BIT 2
#define U3MSR_dDCD_MASK 0x8
#define U3MSR_dDCD 0x8
#define U3MSR_dDCD_BIT 3
#define U3MSR_CTS_MASK 0x10
#define U3MSR_CTS 0x10
#define U3MSR_CTS_BIT 4
#define U3MSR_DSR_MASK 0x20
#define U3MSR_DSR 0x20
#define U3MSR_DSR_BIT 5
#define U3MSR_RI_MASK 0x40
#define U3MSR_RI 0x40
#define U3MSR_RI_BIT 6
#define U3MSR_DCD_MASK 0x80
#define U3MSR_DCD 0x80
#define U3MSR_DCD_BIT 7

#define U3RXLEV (*(volatile unsigned char *)0x4008001C)

#define U4RBR (*(volatile unsigned char *)0x40088000)

#define U4THR (*(volatile unsigned char *)0x40088000)

#define U4DLL (*(volatile unsigned char *)0x40088000)

#define U4DLM (*(volatile unsigned char *)0x40088004)

#define U4IER (*(volatile unsigned char *)0x40088004)
#define U4IER_Rx_Line_Status_Interrupt_Enable_MASK 0x4
#define U4IER_Rx_Line_Status_Interrupt_Enable 0x4
#define U4IER_Rx_Line_Status_Interrupt_Enable_BIT 2
#define U4IER_THRE_Interrupt_Enable_MASK 0x2
#define U4IER_THRE_Interrupt_Enable 0x2
#define U4IER_THRE_Interrupt_Enable_BIT 1
#define U4IER_RDA_Interrupt_Enable_MASK 0x1
#define U4IER_RDA_Interrupt_Enable 0x1
#define U4IER_RDA_Interrupt_Enable_BIT 0

#define U4IIR (*(volatile unsigned char *)0x40088008)
#define U4IIR_FIFO_Enable_MASK 0xC0
#define U4IIR_FIFO_Enable_BIT 6
#define U4IIR_Interrupt_Identification_MASK 0xE
#define U4IIR_Interrupt_Identification_BIT 1
#define U4IIR_Interrupt_Pending_MASK 0x1
#define U4IIR_Interrupt_Pending 0x1
#define U4IIR_Interrupt_Pending_BIT 0

#define U4FCR (*(volatile unsigned char *)0x40088008)
#define U4FCR_Receiver_Trigger_Level_Select_MASK 0xC0
#define U4FCR_Receiver_Trigger_Level_Select_BIT 6
#define U4FCR_Transmitter_Trigger_Level_Select_MASK 0x30
#define U4FCR_Transmitter_Trigger_Level_Select_BIT 4
#define U4FCR_FIFO_Control_MASK 0x8
#define U4FCR_FIFO_Control 0x8
#define U4FCR_FIFO_Control_BIT 3
#define U4FCR_Transmitter_FIFO_Reset_MASK 0x4
#define U4FCR_Transmitter_FIFO_Reset 0x4
#define U4FCR_Transmitter_FIFO_Reset_BIT 2
#define U4FCR_Receiver_FIFO_Reset_MASK 0x2
#define U4FCR_Receiver_FIFO_Reset 0x2
#define U4FCR_Receiver_FIFO_Reset_BIT 1
#define U4FCR_FIFO_Enable_MASK 0x1
#define U4FCR_FIFO_Enable 0x1
#define U4FCR_FIFO_Enable_BIT 0

#define U4LCR (*(volatile unsigned char *)0x4008800C)
#define U4LCR_Divisor_Latch_Access_Bit_MASK 0x80
#define U4LCR_Divisor_Latch_Access_Bit 0x80
#define U4LCR_Divisor_Latch_Access_Bit_BIT 7
#define U4LCR_Break_Control_MASK 0x40
#define U4LCR_Break_Control 0x40
#define U4LCR_Break_Control_BIT 6
#define U4LCR_Parity_Select_MASK 0x30
#define U4LCR_Parity_Select_BIT 4
#define U4LCR_Parity_Enable_MASK 0x8
#define U4LCR_Parity_Enable 0x8
#define U4LCR_Parity_Enable_BIT 3
#define U4LCR_Stop_Bit_Select_MASK 0x4
#define U4LCR_Stop_Bit_Select 0x4
#define U4LCR_Stop_Bit_Select_BIT 2
#define U4LCR_Word_Length_Select_MASK 0x3
#define U4LCR_Word_Length_Select_BIT 0

#define U4LSR (*(volatile unsigned char *)0x40088014)
#define U4LSR_FIFO_Rx_Error_MASK 0x80
#define U4LSR_FIFO_Rx_Error 0x80
#define U4LSR_FIFO_Rx_Error_BIT 7
#define U4LSR_TEMT_MASK 0x40
#define U4LSR_TEMT 0x40
#define U4LSR_TEMT_BIT 6
#define U4LSR_THRE_MASK 0x20
#define U4LSR_THRE 0x20
#define U4LSR_THRE_BIT 5
#define U4LSR_BI_MASK 0x10
#define U4LSR_BI 0x10
#define U4LSR_BI_BIT 4
#define U4LSR_FE_MASK 0x8
#define U4LSR_FE 0x8
#define U4LSR_FE_BIT 3
#define U4LSR_PE_MASK 0x4
#define U4LSR_PE 0x4
#define U4LSR_PE_BIT 2
#define U4LSR_OE_MASK 0x2
#define U4LSR_OE 0x2
#define U4LSR_OE_BIT 1
#define U4LSR_RDR_MASK 0x1
#define U4LSR_RDR 0x1
#define U4LSR_RDR_BIT 0

#define U4RXLEV (*(volatile unsigned char *)0x4008801C)

#define U5RBR (*(volatile unsigned char *)0x40090000)

#define U5THR (*(volatile unsigned char *)0x40090000)

#define U5DLL (*(volatile unsigned char *)0x40090000)

#define U5DLM (*(volatile unsigned char *)0x40090004)

#define U5IER (*(volatile unsigned char *)0x40090004)
#define U5IER_Rx_Line_Status_Interrupt_Enable_MASK 0x4
#define U5IER_Rx_Line_Status_Interrupt_Enable 0x4
#define U5IER_Rx_Line_Status_Interrupt_Enable_BIT 2
#define U5IER_THRE_Interrupt_Enable_MASK 0x2
#define U5IER_THRE_Interrupt_Enable 0x2
#define U5IER_THRE_Interrupt_Enable_BIT 1
#define U5IER_RDA_Interrupt_Enable_MASK 0x1
#define U5IER_RDA_Interrupt_Enable 0x1
#define U5IER_RDA_Interrupt_Enable_BIT 0

#define U5IIR (*(volatile unsigned char *)0x40090008)
#define U5IIR_FIFO_Enable_MASK 0xC0
#define U5IIR_FIFO_Enable_BIT 6
#define U5IIR_Interrupt_Identification_MASK 0xE
#define U5IIR_Interrupt_Identification_BIT 1
#define U5IIR_Interrupt_Pending_MASK 0x1
#define U5IIR_Interrupt_Pending 0x1
#define U5IIR_Interrupt_Pending_BIT 0

#define U5FCR (*(volatile unsigned char *)0x40090008)
#define U5FCR_Receiver_Trigger_Level_Select_MASK 0xC0
#define U5FCR_Receiver_Trigger_Level_Select_BIT 6
#define U5FCR_Transmitter_Trigger_Level_Select_MASK 0x30
#define U5FCR_Transmitter_Trigger_Level_Select_BIT 4
#define U5FCR_FIFO_Control_MASK 0x8
#define U5FCR_FIFO_Control 0x8
#define U5FCR_FIFO_Control_BIT 3
#define U5FCR_Transmitter_FIFO_Reset_MASK 0x4
#define U5FCR_Transmitter_FIFO_Reset 0x4
#define U5FCR_Transmitter_FIFO_Reset_BIT 2
#define U5FCR_Receiver_FIFO_Reset_MASK 0x2
#define U5FCR_Receiver_FIFO_Reset 0x2
#define U5FCR_Receiver_FIFO_Reset_BIT 1
#define U5FCR_FIFO_Enable_MASK 0x1
#define U5FCR_FIFO_Enable 0x1
#define U5FCR_FIFO_Enable_BIT 0

#define U5LCR (*(volatile unsigned char *)0x4009000C)
#define U5LCR_Divisor_Latch_Access_Bit_MASK 0x80
#define U5LCR_Divisor_Latch_Access_Bit 0x80
#define U5LCR_Divisor_Latch_Access_Bit_BIT 7
#define U5LCR_Break_Control_MASK 0x40
#define U5LCR_Break_Control 0x40
#define U5LCR_Break_Control_BIT 6
#define U5LCR_Parity_Select_MASK 0x30
#define U5LCR_Parity_Select_BIT 4
#define U5LCR_Parity_Enable_MASK 0x8
#define U5LCR_Parity_Enable 0x8
#define U5LCR_Parity_Enable_BIT 3
#define U5LCR_Stop_Bit_Select_MASK 0x4
#define U5LCR_Stop_Bit_Select 0x4
#define U5LCR_Stop_Bit_Select_BIT 2
#define U5LCR_Word_Length_Select_MASK 0x3
#define U5LCR_Word_Length_Select_BIT 0

#define U5LSR (*(volatile unsigned char *)0x40090014)
#define U5LSR_FIFO_Rx_Error_MASK 0x80
#define U5LSR_FIFO_Rx_Error 0x80
#define U5LSR_FIFO_Rx_Error_BIT 7
#define U5LSR_TEMT_MASK 0x40
#define U5LSR_TEMT 0x40
#define U5LSR_TEMT_BIT 6
#define U5LSR_THRE_MASK 0x20
#define U5LSR_THRE 0x20
#define U5LSR_THRE_BIT 5
#define U5LSR_BI_MASK 0x10
#define U5LSR_BI 0x10
#define U5LSR_BI_BIT 4
#define U5LSR_FE_MASK 0x8
#define U5LSR_FE 0x8
#define U5LSR_FE_BIT 3
#define U5LSR_PE_MASK 0x4
#define U5LSR_PE 0x4
#define U5LSR_PE_BIT 2
#define U5LSR_OE_MASK 0x2
#define U5LSR_OE 0x2
#define U5LSR_OE_BIT 1
#define U5LSR_RDR_MASK 0x1
#define U5LSR_RDR 0x1
#define U5LSR_RDR_BIT 0

#define U5RXLEV (*(volatile unsigned char *)0x4009001C)

#define U6RBR (*(volatile unsigned char *)0x40098000)

#define U6THR (*(volatile unsigned char *)0x40098000)

#define U6DLL (*(volatile unsigned char *)0x40098000)

#define U6DLM (*(volatile unsigned char *)0x40098004)

#define U6IER (*(volatile unsigned char *)0x40098004)
#define U6IER_Rx_Line_Status_Interrupt_Enable_MASK 0x4
#define U6IER_Rx_Line_Status_Interrupt_Enable 0x4
#define U6IER_Rx_Line_Status_Interrupt_Enable_BIT 2
#define U6IER_THRE_Interrupt_Enable_MASK 0x2
#define U6IER_THRE_Interrupt_Enable 0x2
#define U6IER_THRE_Interrupt_Enable_BIT 1
#define U6IER_RDA_Interrupt_Enable_MASK 0x1
#define U6IER_RDA_Interrupt_Enable 0x1
#define U6IER_RDA_Interrupt_Enable_BIT 0

#define U6IIR (*(volatile unsigned char *)0x40098008)
#define U6IIR_FIFO_Enable_MASK 0xC0
#define U6IIR_FIFO_Enable_BIT 6
#define U6IIR_Interrupt_Identification_MASK 0xE
#define U6IIR_Interrupt_Identification_BIT 1
#define U6IIR_Interrupt_Pending_MASK 0x1
#define U6IIR_Interrupt_Pending 0x1
#define U6IIR_Interrupt_Pending_BIT 0

#define U6FCR (*(volatile unsigned char *)0x40098008)
#define U6FCR_Receiver_Trigger_Level_Select_MASK 0xC0
#define U6FCR_Receiver_Trigger_Level_Select_BIT 6
#define U6FCR_Transmitter_Trigger_Level_Select_MASK 0x30
#define U6FCR_Transmitter_Trigger_Level_Select_BIT 4
#define U6FCR_FIFO_Control_MASK 0x8
#define U6FCR_FIFO_Control 0x8
#define U6FCR_FIFO_Control_BIT 3
#define U6FCR_Transmitter_FIFO_Reset_MASK 0x4
#define U6FCR_Transmitter_FIFO_Reset 0x4
#define U6FCR_Transmitter_FIFO_Reset_BIT 2
#define U6FCR_Receiver_FIFO_Reset_MASK 0x2
#define U6FCR_Receiver_FIFO_Reset 0x2
#define U6FCR_Receiver_FIFO_Reset_BIT 1
#define U6FCR_FIFO_Enable_MASK 0x1
#define U6FCR_FIFO_Enable 0x1
#define U6FCR_FIFO_Enable_BIT 0

#define U6LCR (*(volatile unsigned char *)0x4009800C)
#define U6LCR_Divisor_Latch_Access_Bit_MASK 0x80
#define U6LCR_Divisor_Latch_Access_Bit 0x80
#define U6LCR_Divisor_Latch_Access_Bit_BIT 7
#define U6LCR_Break_Control_MASK 0x40
#define U6LCR_Break_Control 0x40
#define U6LCR_Break_Control_BIT 6
#define U6LCR_Parity_Select_MASK 0x30
#define U6LCR_Parity_Select_BIT 4
#define U6LCR_Parity_Enable_MASK 0x8
#define U6LCR_Parity_Enable 0x8
#define U6LCR_Parity_Enable_BIT 3
#define U6LCR_Stop_Bit_Select_MASK 0x4
#define U6LCR_Stop_Bit_Select 0x4
#define U6LCR_Stop_Bit_Select_BIT 2
#define U6LCR_Word_Length_Select_MASK 0x3
#define U6LCR_Word_Length_Select_BIT 0

#define U6LSR (*(volatile unsigned char *)0x40098014)
#define U6LSR_FIFO_Rx_Error_MASK 0x80
#define U6LSR_FIFO_Rx_Error 0x80
#define U6LSR_FIFO_Rx_Error_BIT 7
#define U6LSR_TEMT_MASK 0x40
#define U6LSR_TEMT 0x40
#define U6LSR_TEMT_BIT 6
#define U6LSR_THRE_MASK 0x20
#define U6LSR_THRE 0x20
#define U6LSR_THRE_BIT 5
#define U6LSR_BI_MASK 0x10
#define U6LSR_BI 0x10
#define U6LSR_BI_BIT 4
#define U6LSR_FE_MASK 0x8
#define U6LSR_FE 0x8
#define U6LSR_FE_BIT 3
#define U6LSR_PE_MASK 0x4
#define U6LSR_PE 0x4
#define U6LSR_PE_BIT 2
#define U6LSR_OE_MASK 0x2
#define U6LSR_OE 0x2
#define U6LSR_OE_BIT 1
#define U6LSR_RDR_MASK 0x1
#define U6LSR_RDR 0x1
#define U6LSR_RDR_BIT 0

#define U6RXLEV (*(volatile unsigned char *)0x4009801C)

#define I2C1_RX (*(volatile unsigned *)0x400A0000)
#define I2C1_RX_RxData_MASK 0xFF
#define I2C1_RX_RxData_BIT 0

#define I2C1_TX (*(volatile unsigned *)0x400A0000)
#define I2C1_TX_STOP_MASK 0x200
#define I2C1_TX_STOP 0x200
#define I2C1_TX_STOP_BIT 9
#define I2C1_TX_START_MASK 0x100
#define I2C1_TX_START 0x100
#define I2C1_TX_START_BIT 8
#define I2C1_TX_TxData_MASK 0xFF
#define I2C1_TX_TxData_BIT 0

#define I2C1_STAT (*(volatile unsigned *)0x400A0004)
#define I2C1_STAT_TFES_MASK 0x2000
#define I2C1_STAT_TFES 0x2000
#define I2C1_STAT_TFES_BIT 13
#define I2C1_STAT_TFFS_MASK 0x1000
#define I2C1_STAT_TFFS 0x1000
#define I2C1_STAT_TFFS_BIT 12
#define I2C1_STAT_TFE_MASK 0x800
#define I2C1_STAT_TFE 0x800
#define I2C1_STAT_TFE_BIT 11
#define I2C1_STAT_TFF_MASK 0x400
#define I2C1_STAT_TFF 0x400
#define I2C1_STAT_TFF_BIT 10
#define I2C1_STAT_RFE_MASK 0x200
#define I2C1_STAT_RFE 0x200
#define I2C1_STAT_RFE_BIT 9
#define I2C1_STAT_RFF_MASK 0x100
#define I2C1_STAT_RFF 0x100
#define I2C1_STAT_RFF_BIT 8
#define I2C1_STAT_SDA_MASK 0x80
#define I2C1_STAT_SDA 0x80
#define I2C1_STAT_SDA_BIT 7
#define I2C1_STAT_SCL_MASK 0x40
#define I2C1_STAT_SCL 0x40
#define I2C1_STAT_SCL_BIT 6
#define I2C1_STAT_ACTIVE_MASK 0x20
#define I2C1_STAT_ACTIVE 0x20
#define I2C1_STAT_ACTIVE_BIT 5
#define I2C1_STAT_DRMI_MASK 0x8
#define I2C1_STAT_DRMI 0x8
#define I2C1_STAT_DRMI_BIT 3
#define I2C1_STAT_NAI_MASK 0x4
#define I2C1_STAT_NAI 0x4
#define I2C1_STAT_NAI_BIT 2
#define I2C1_STAT_TDI_MASK 0x1
#define I2C1_STAT_TDI 0x1
#define I2C1_STAT_TDI_BIT 0

#define I2C1_CTRL (*(volatile unsigned *)0x400A0008)
#define I2C1_CTRL_TFFSIE_MASK 0x400
#define I2C1_CTRL_TFFSIE 0x400
#define I2C1_CTRL_TFFSIE_BIT 10
#define I2C1_CTRL_SEVEN_MASK 0x200
#define I2C1_CTRL_SEVEN 0x200
#define I2C1_CTRL_SEVEN_BIT 9
#define I2C1_CTRL_RESET_MASK 0x100
#define I2C1_CTRL_RESET 0x100
#define I2C1_CTRL_RESET_BIT 8
#define I2C1_CTRL_TFFIE_MASK 0x80
#define I2C1_CTRL_TFFIE 0x80
#define I2C1_CTRL_TFFIE_BIT 7
#define I2C1_CTRL_RFDAIE_MASK 0x40
#define I2C1_CTRL_RFDAIE 0x40
#define I2C1_CTRL_RFDAIE_BIT 6
#define I2C1_CTRL_RFFIE_MASK 0x20
#define I2C1_CTRL_RFFIE 0x20
#define I2C1_CTRL_RFFIE_BIT 5
#define I2C1_CTRL_DRSIE_MASK 0x10
#define I2C1_CTRL_DRSIE 0x10
#define I2C1_CTRL_DRSIE_BIT 4
#define I2C1_CTRL_DRMIE_MASK 0x8
#define I2C1_CTRL_DRMIE 0x8
#define I2C1_CTRL_DRMIE_BIT 3
#define I2C1_CTRL_NAIE_MASK 0x4
#define I2C1_CTRL_NAIE 0x4
#define I2C1_CTRL_NAIE_BIT 2
#define I2C1_CTRL_AFIE_MASK 0x2
#define I2C1_CTRL_AFIE 0x2
#define I2C1_CTRL_AFIE_BIT 1
#define I2C1_CTRL_TDIE_MASK 0x1
#define I2C1_CTRL_TDIE 0x1
#define I2C1_CTRL_TDIE_BIT 0

#define I2C1_CLK_HI (*(volatile unsigned *)0x400A000C)
#define I2C1_CLK_HI_CLK_DIV_HI_MASK 0x3FF
#define I2C1_CLK_HI_CLK_DIV_HI_BIT 0

#define I2C1_CLK_LO (*(volatile unsigned *)0x400A0010)
#define I2C1_CLK_LO_CLK_DIV_LO_MASK 0x3FF
#define I2C1_CLK_LO_CLK_DIV_LO_BIT 0

#define I2C1_ADR (*(volatile unsigned *)0x400A0014)
#define I2C1_ADR_ADR_MASK 0x3FF
#define I2C1_ADR_ADR_BIT 0

#define I2C1_RXFL (*(volatile unsigned *)0x400A0018)
#define I2C1_RXFL_RxFL_MASK 0x3
#define I2C1_RXFL_RxFL_BIT 0

#define I2C1_TXFL (*(volatile unsigned *)0x400A001C)
#define I2C1_TXFL_TxFL_MASK 0x3
#define I2C1_TXFL_TxFL_BIT 0

#define I2C1_RXB (*(volatile unsigned *)0x400A0020)
#define I2C1_RXB_RxB_MASK 0xFFFF
#define I2C1_RXB_RxB_BIT 0

#define I2C1_TXB (*(volatile unsigned *)0x400A0024)
#define I2C1_TXB_TxB_MASK 0xFFFF
#define I2C1_TXB_TxB_BIT 0

#define I2C1_S_TX (*(volatile unsigned *)0x400A0028)
#define I2C1_S_TX_TXS_MASK 0xFF
#define I2C1_S_TX_TXS_BIT 0

#define I2C1_S_TXFL (*(volatile unsigned *)0x400A002C)
#define I2C1_S_TXFL_TxFL_MASK 0x3
#define I2C1_S_TXFL_TxFL_BIT 0

#define I2C2_RX (*(volatile unsigned *)0x400A8000)
#define I2C2_RX_RxData_MASK 0xFF
#define I2C2_RX_RxData_BIT 0

#define I2C2_TX (*(volatile unsigned *)0x400A8000)
#define I2C2_TX_STOP_MASK 0x200
#define I2C2_TX_STOP 0x200
#define I2C2_TX_STOP_BIT 9
#define I2C2_TX_START_MASK 0x100
#define I2C2_TX_START 0x100
#define I2C2_TX_START_BIT 8
#define I2C2_TX_TxData_MASK 0xFF
#define I2C2_TX_TxData_BIT 0

#define I2C2_STAT (*(volatile unsigned *)0x400A8004)
#define I2C2_STAT_TFES_MASK 0x2000
#define I2C2_STAT_TFES 0x2000
#define I2C2_STAT_TFES_BIT 13
#define I2C2_STAT_TFFS_MASK 0x1000
#define I2C2_STAT_TFFS 0x1000
#define I2C2_STAT_TFFS_BIT 12
#define I2C2_STAT_TFE_MASK 0x800
#define I2C2_STAT_TFE 0x800
#define I2C2_STAT_TFE_BIT 11
#define I2C2_STAT_TFF_MASK 0x400
#define I2C2_STAT_TFF 0x400
#define I2C2_STAT_TFF_BIT 10
#define I2C2_STAT_RFE_MASK 0x200
#define I2C2_STAT_RFE 0x200
#define I2C2_STAT_RFE_BIT 9
#define I2C2_STAT_RFF_MASK 0x100
#define I2C2_STAT_RFF 0x100
#define I2C2_STAT_RFF_BIT 8
#define I2C2_STAT_SDA_MASK 0x80
#define I2C2_STAT_SDA 0x80
#define I2C2_STAT_SDA_BIT 7
#define I2C2_STAT_SCL_MASK 0x40
#define I2C2_STAT_SCL 0x40
#define I2C2_STAT_SCL_BIT 6
#define I2C2_STAT_ACTIVE_MASK 0x20
#define I2C2_STAT_ACTIVE 0x20
#define I2C2_STAT_ACTIVE_BIT 5
#define I2C2_STAT_DRMI_MASK 0x8
#define I2C2_STAT_DRMI 0x8
#define I2C2_STAT_DRMI_BIT 3
#define I2C2_STAT_NAI_MASK 0x4
#define I2C2_STAT_NAI 0x4
#define I2C2_STAT_NAI_BIT 2
#define I2C2_STAT_TDI_MASK 0x1
#define I2C2_STAT_TDI 0x1
#define I2C2_STAT_TDI_BIT 0

#define I2C2_CTRL (*(volatile unsigned *)0x400A8008)
#define I2C2_CTRL_TFFSIE_MASK 0x400
#define I2C2_CTRL_TFFSIE 0x400
#define I2C2_CTRL_TFFSIE_BIT 10
#define I2C2_CTRL_SEVEN_MASK 0x200
#define I2C2_CTRL_SEVEN 0x200
#define I2C2_CTRL_SEVEN_BIT 9
#define I2C2_CTRL_RESET_MASK 0x100
#define I2C2_CTRL_RESET 0x100
#define I2C2_CTRL_RESET_BIT 8
#define I2C2_CTRL_TFFIE_MASK 0x80
#define I2C2_CTRL_TFFIE 0x80
#define I2C2_CTRL_TFFIE_BIT 7
#define I2C2_CTRL_RFDAIE_MASK 0x40
#define I2C2_CTRL_RFDAIE 0x40
#define I2C2_CTRL_RFDAIE_BIT 6
#define I2C2_CTRL_RFFIE_MASK 0x20
#define I2C2_CTRL_RFFIE 0x20
#define I2C2_CTRL_RFFIE_BIT 5
#define I2C2_CTRL_DRSIE_MASK 0x10
#define I2C2_CTRL_DRSIE 0x10
#define I2C2_CTRL_DRSIE_BIT 4
#define I2C2_CTRL_DRMIE_MASK 0x8
#define I2C2_CTRL_DRMIE 0x8
#define I2C2_CTRL_DRMIE_BIT 3
#define I2C2_CTRL_NAIE_MASK 0x4
#define I2C2_CTRL_NAIE 0x4
#define I2C2_CTRL_NAIE_BIT 2
#define I2C2_CTRL_AFIE_MASK 0x2
#define I2C2_CTRL_AFIE 0x2
#define I2C2_CTRL_AFIE_BIT 1
#define I2C2_CTRL_TDIE_MASK 0x1
#define I2C2_CTRL_TDIE 0x1
#define I2C2_CTRL_TDIE_BIT 0

#define I2C2_CLK_HI (*(volatile unsigned *)0x400A800C)
#define I2C2_CLK_HI_CLK_DIV_HI_MASK 0x3FF
#define I2C2_CLK_HI_CLK_DIV_HI_BIT 0

#define I2C2_CLK_LO (*(volatile unsigned *)0x400A8010)
#define I2C2_CLK_LO_CLK_DIV_LO_MASK 0x3FF
#define I2C2_CLK_LO_CLK_DIV_LO_BIT 0

#define I2C2_ADR (*(volatile unsigned *)0x400A8014)
#define I2C2_ADR_ADR_MASK 0x3FF
#define I2C2_ADR_ADR_BIT 0

#define I2C2_RXFL (*(volatile unsigned *)0x400A8018)
#define I2C2_RXFL_RxFL_MASK 0x3
#define I2C2_RXFL_RxFL_BIT 0

#define I2C2_TXFL (*(volatile unsigned *)0x400A801C)
#define I2C2_TXFL_TxFL_MASK 0x3
#define I2C2_TXFL_TxFL_BIT 0

#define I2C2_RXB (*(volatile unsigned *)0x400A8020)
#define I2C2_RXB_RxB_MASK 0xFFFF
#define I2C2_RXB_RxB_BIT 0

#define I2C2_TXB (*(volatile unsigned *)0x400A8024)
#define I2C2_TXB_TxB_MASK 0xFFFF
#define I2C2_TXB_TxB_BIT 0

#define I2C2_S_TX (*(volatile unsigned *)0x400A8028)
#define I2C2_S_TX_TXS_MASK 0xFF
#define I2C2_S_TX_TXS_BIT 0

#define I2C2_S_TXFL (*(volatile unsigned *)0x400A802C)
#define I2C2_S_TXFL_TxFL_MASK 0x3
#define I2C2_S_TXFL_TxFL_BIT 0

#define MCCON (*(volatile unsigned *)0x400E8000)
#define MCCON_DCMODE_MASK 0x80000000
#define MCCON_DCMODE 0x80000000
#define MCCON_DCMODE_BIT 31
#define MCCON_ACMODE_MASK 0x40000000
#define MCCON_ACMODE 0x40000000
#define MCCON_ACMODE_BIT 30
#define MCCON_INVBDC_MASK 0x20000000
#define MCCON_INVBDC 0x20000000
#define MCCON_INVBDC_BIT 29
#define MCCON_DISUP0_MASK 0x10
#define MCCON_DISUP0 0x10
#define MCCON_DISUP0_BIT 4
#define MCCON_DTE0_MASK 0x8
#define MCCON_DTE0 0x8
#define MCCON_DTE0_BIT 3
#define MCCON_POLA0_MASK 0x4
#define MCCON_POLA0 0x4
#define MCCON_POLA0_BIT 2
#define MCCON_CENTER0_MASK 0x2
#define MCCON_CENTER0 0x2
#define MCCON_CENTER0_BIT 1
#define MCCON_RUN0_MASK 0x1
#define MCCON_RUN0 0x1
#define MCCON_RUN0_BIT 0
#define MCCON_DISUP1_MASK 0x1000
#define MCCON_DISUP1 0x1000
#define MCCON_DISUP1_BIT 12
#define MCCON_DTE1_MASK 0x800
#define MCCON_DTE1 0x800
#define MCCON_DTE1_BIT 11
#define MCCON_POLA1_MASK 0x400
#define MCCON_POLA1 0x400
#define MCCON_POLA1_BIT 10
#define MCCON_CENTER1_MASK 0x200
#define MCCON_CENTER1 0x200
#define MCCON_CENTER1_BIT 9
#define MCCON_RUN1_MASK 0x100
#define MCCON_RUN1 0x100
#define MCCON_RUN1_BIT 8
#define MCCON_DISUP2_MASK 0x100000
#define MCCON_DISUP2 0x100000
#define MCCON_DISUP2_BIT 20
#define MCCON_DTE2_MASK 0x80000
#define MCCON_DTE2 0x80000
#define MCCON_DTE2_BIT 19
#define MCCON_POLA2_MASK 0x40000
#define MCCON_POLA2 0x40000
#define MCCON_POLA2_BIT 18
#define MCCON_CENTER2_MASK 0x20000
#define MCCON_CENTER2 0x20000
#define MCCON_CENTER2_BIT 17
#define MCCON_RUN2_MASK 0x10000
#define MCCON_RUN2 0x10000
#define MCCON_RUN2_BIT 16

#define MCCON_SET (*(volatile unsigned *)0x400E8004)
#define MCCON_SET_DCMODE_MASK 0x80000000
#define MCCON_SET_DCMODE 0x80000000
#define MCCON_SET_DCMODE_BIT 31
#define MCCON_SET_ACMODE_MASK 0x40000000
#define MCCON_SET_ACMODE 0x40000000
#define MCCON_SET_ACMODE_BIT 30
#define MCCON_SET_INVBDC_MASK 0x20000000
#define MCCON_SET_INVBDC 0x20000000
#define MCCON_SET_INVBDC_BIT 29
#define MCCON_SET_DISUP0_MASK 0x10
#define MCCON_SET_DISUP0 0x10
#define MCCON_SET_DISUP0_BIT 4
#define MCCON_SET_DTE0_MASK 0x8
#define MCCON_SET_DTE0 0x8
#define MCCON_SET_DTE0_BIT 3
#define MCCON_SET_POLA0_MASK 0x4
#define MCCON_SET_POLA0 0x4
#define MCCON_SET_POLA0_BIT 2
#define MCCON_SET_CENTER0_MASK 0x2
#define MCCON_SET_CENTER0 0x2
#define MCCON_SET_CENTER0_BIT 1
#define MCCON_SET_RUN0_MASK 0x1
#define MCCON_SET_RUN0 0x1
#define MCCON_SET_RUN0_BIT 0
#define MCCON_SET_DISUP1_MASK 0x1000
#define MCCON_SET_DISUP1 0x1000
#define MCCON_SET_DISUP1_BIT 12
#define MCCON_SET_DTE1_MASK 0x800
#define MCCON_SET_DTE1 0x800
#define MCCON_SET_DTE1_BIT 11
#define MCCON_SET_POLA1_MASK 0x400
#define MCCON_SET_POLA1 0x400
#define MCCON_SET_POLA1_BIT 10
#define MCCON_SET_CENTER1_MASK 0x200
#define MCCON_SET_CENTER1 0x200
#define MCCON_SET_CENTER1_BIT 9
#define MCCON_SET_RUN1_MASK 0x100
#define MCCON_SET_RUN1 0x100
#define MCCON_SET_RUN1_BIT 8
#define MCCON_SET_DISUP2_MASK 0x100000
#define MCCON_SET_DISUP2 0x100000
#define MCCON_SET_DISUP2_BIT 20
#define MCCON_SET_DTE2_MASK 0x80000
#define MCCON_SET_DTE2 0x80000
#define MCCON_SET_DTE2_BIT 19
#define MCCON_SET_POLA2_MASK 0x40000
#define MCCON_SET_POLA2 0x40000
#define MCCON_SET_POLA2_BIT 18
#define MCCON_SET_CENTER2_MASK 0x20000
#define MCCON_SET_CENTER2 0x20000
#define MCCON_SET_CENTER2_BIT 17
#define MCCON_SET_RUN2_MASK 0x10000
#define MCCON_SET_RUN2 0x10000
#define MCCON_SET_RUN2_BIT 16

#define MCCON_CLR (*(volatile unsigned *)0x400E8008)
#define MCCON_CLR_DCMODE_MASK 0x80000000
#define MCCON_CLR_DCMODE 0x80000000
#define MCCON_CLR_DCMODE_BIT 31
#define MCCON_CLR_ACMODE_MASK 0x40000000
#define MCCON_CLR_ACMODE 0x40000000
#define MCCON_CLR_ACMODE_BIT 30
#define MCCON_CLR_INVBDC_MASK 0x20000000
#define MCCON_CLR_INVBDC 0x20000000
#define MCCON_CLR_INVBDC_BIT 29
#define MCCON_CLR_DISUP0_MASK 0x10
#define MCCON_CLR_DISUP0 0x10
#define MCCON_CLR_DISUP0_BIT 4
#define MCCON_CLR_DTE0_MASK 0x8
#define MCCON_CLR_DTE0 0x8
#define MCCON_CLR_DTE0_BIT 3
#define MCCON_CLR_POLA0_MASK 0x4
#define MCCON_CLR_POLA0 0x4
#define MCCON_CLR_POLA0_BIT 2
#define MCCON_CLR_CENTER0_MASK 0x2
#define MCCON_CLR_CENTER0 0x2
#define MCCON_CLR_CENTER0_BIT 1
#define MCCON_CLR_RUN0_MASK 0x1
#define MCCON_CLR_RUN0 0x1
#define MCCON_CLR_RUN0_BIT 0
#define MCCON_CLR_DISUP1_MASK 0x1000
#define MCCON_CLR_DISUP1 0x1000
#define MCCON_CLR_DISUP1_BIT 12
#define MCCON_CLR_DTE1_MASK 0x800
#define MCCON_CLR_DTE1 0x800
#define MCCON_CLR_DTE1_BIT 11
#define MCCON_CLR_POLA1_MASK 0x400
#define MCCON_CLR_POLA1 0x400
#define MCCON_CLR_POLA1_BIT 10
#define MCCON_CLR_CENTER1_MASK 0x200
#define MCCON_CLR_CENTER1 0x200
#define MCCON_CLR_CENTER1_BIT 9
#define MCCON_CLR_RUN1_MASK 0x100
#define MCCON_CLR_RUN1 0x100
#define MCCON_CLR_RUN1_BIT 8
#define MCCON_CLR_DISUP2_MASK 0x100000
#define MCCON_CLR_DISUP2 0x100000
#define MCCON_CLR_DISUP2_BIT 20
#define MCCON_CLR_DTE2_MASK 0x80000
#define MCCON_CLR_DTE2 0x80000
#define MCCON_CLR_DTE2_BIT 19
#define MCCON_CLR_POLA2_MASK 0x40000
#define MCCON_CLR_POLA2 0x40000
#define MCCON_CLR_POLA2_BIT 18
#define MCCON_CLR_CENTER2_MASK 0x20000
#define MCCON_CLR_CENTER2 0x20000
#define MCCON_CLR_CENTER2_BIT 17
#define MCCON_CLR_RUN2_MASK 0x10000
#define MCCON_CLR_RUN2 0x10000
#define MCCON_CLR_RUN2_BIT 16

#define MCCAPCON (*(volatile unsigned *)0x400E800C)
#define MCCAPCON_HNFCAP0_MASK 0x200000
#define MCCAPCON_HNFCAP0 0x200000
#define MCCAPCON_HNFCAP0_BIT 21
#define MCCAPCON_RT0_MASK 0x40000
#define MCCAPCON_RT0 0x40000
#define MCCAPCON_RT0_BIT 18
#define MCCAPCON_HNFCAP1_MASK 0x400000
#define MCCAPCON_HNFCAP1 0x400000
#define MCCAPCON_HNFCAP1_BIT 22
#define MCCAPCON_RT1_MASK 0x80000
#define MCCAPCON_RT1 0x80000
#define MCCAPCON_RT1_BIT 19
#define MCCAPCON_HNFCAP2_MASK 0x800000
#define MCCAPCON_HNFCAP2 0x800000
#define MCCAPCON_HNFCAP2_BIT 23
#define MCCAPCON_RT2_MASK 0x100000
#define MCCAPCON_RT2 0x100000
#define MCCAPCON_RT2_BIT 20
#define MCCAPCON_CAP0MCI2_FE_MASK 0x20
#define MCCAPCON_CAP0MCI2_FE 0x20
#define MCCAPCON_CAP0MCI2_FE_BIT 5
#define MCCAPCON_CAP0MCI2_RE_MASK 0x10
#define MCCAPCON_CAP0MCI2_RE 0x10
#define MCCAPCON_CAP0MCI2_RE_BIT 4
#define MCCAPCON_CAP0MCI1_FE_MASK 0x8
#define MCCAPCON_CAP0MCI1_FE 0x8
#define MCCAPCON_CAP0MCI1_FE_BIT 3
#define MCCAPCON_CAP0MCI1_RE_MASK 0x4
#define MCCAPCON_CAP0MCI1_RE 0x4
#define MCCAPCON_CAP0MCI1_RE_BIT 2
#define MCCAPCON_CAP0MCI0_FE_MASK 0x2
#define MCCAPCON_CAP0MCI0_FE 0x2
#define MCCAPCON_CAP0MCI0_FE_BIT 1
#define MCCAPCON_CAP0MCI0_RE_MASK 0x1
#define MCCAPCON_CAP0MCI0_RE 0x1
#define MCCAPCON_CAP0MCI0_RE_BIT 0
#define MCCAPCON_CAP1MCI2_FE_MASK 0x800
#define MCCAPCON_CAP1MCI2_FE 0x800
#define MCCAPCON_CAP1MCI2_FE_BIT 11
#define MCCAPCON_CAP1MCI2_RE_MASK 0x400
#define MCCAPCON_CAP1MCI2_RE 0x400
#define MCCAPCON_CAP1MCI2_RE_BIT 10
#define MCCAPCON_CAP1MCI1_FE_MASK 0x200
#define MCCAPCON_CAP1MCI1_FE 0x200
#define MCCAPCON_CAP1MCI1_FE_BIT 9
#define MCCAPCON_CAP1MCI1_RE_MASK 0x100
#define MCCAPCON_CAP1MCI1_RE 0x100
#define MCCAPCON_CAP1MCI1_RE_BIT 8
#define MCCAPCON_CAP1MCI0_FE_MASK 0x80
#define MCCAPCON_CAP1MCI0_FE 0x80
#define MCCAPCON_CAP1MCI0_FE_BIT 7
#define MCCAPCON_CAP1MCI0_RE_MASK 0x40
#define MCCAPCON_CAP1MCI0_RE 0x40
#define MCCAPCON_CAP1MCI0_RE_BIT 6
#define MCCAPCON_CAP2MCI2_FE_MASK 0x20000
#define MCCAPCON_CAP2MCI2_FE 0x20000
#define MCCAPCON_CAP2MCI2_FE_BIT 17
#define MCCAPCON_CAP2MCI2_RE_MASK 0x10000
#define MCCAPCON_CAP2MCI2_RE 0x10000
#define MCCAPCON_CAP2MCI2_RE_BIT 16
#define MCCAPCON_CAP2MCI1_FE_MASK 0x8000
#define MCCAPCON_CAP2MCI1_FE 0x8000
#define MCCAPCON_CAP2MCI1_FE_BIT 15
#define MCCAPCON_CAP2MCI1_RE_MASK 0x4000
#define MCCAPCON_CAP2MCI1_RE 0x4000
#define MCCAPCON_CAP2MCI1_RE_BIT 14
#define MCCAPCON_CAP2MCI0_FE_MASK 0x2000
#define MCCAPCON_CAP2MCI0_FE 0x2000
#define MCCAPCON_CAP2MCI0_FE_BIT 13
#define MCCAPCON_CAP2MCI0_RE_MASK 0x1000
#define MCCAPCON_CAP2MCI0_RE 0x1000
#define MCCAPCON_CAP2MCI0_RE_BIT 12

#define MCCAPCON_SET (*(volatile unsigned *)0x400E8010)
#define MCCAPCON_SET_HNFCAP0_MASK 0x200000
#define MCCAPCON_SET_HNFCAP0 0x200000
#define MCCAPCON_SET_HNFCAP0_BIT 21
#define MCCAPCON_SET_RT0_MASK 0x40000
#define MCCAPCON_SET_RT0 0x40000
#define MCCAPCON_SET_RT0_BIT 18
#define MCCAPCON_SET_HNFCAP1_MASK 0x400000
#define MCCAPCON_SET_HNFCAP1 0x400000
#define MCCAPCON_SET_HNFCAP1_BIT 22
#define MCCAPCON_SET_RT1_MASK 0x80000
#define MCCAPCON_SET_RT1 0x80000
#define MCCAPCON_SET_RT1_BIT 19
#define MCCAPCON_SET_HNFCAP2_MASK 0x800000
#define MCCAPCON_SET_HNFCAP2 0x800000
#define MCCAPCON_SET_HNFCAP2_BIT 23
#define MCCAPCON_SET_RT2_MASK 0x100000
#define MCCAPCON_SET_RT2 0x100000
#define MCCAPCON_SET_RT2_BIT 20
#define MCCAPCON_SET_CAP0MCI2_FE_MASK 0x20
#define MCCAPCON_SET_CAP0MCI2_FE 0x20
#define MCCAPCON_SET_CAP0MCI2_FE_BIT 5
#define MCCAPCON_SET_CAP0MCI2_RE_MASK 0x10
#define MCCAPCON_SET_CAP0MCI2_RE 0x10
#define MCCAPCON_SET_CAP0MCI2_RE_BIT 4
#define MCCAPCON_SET_CAP0MCI1_FE_MASK 0x8
#define MCCAPCON_SET_CAP0MCI1_FE 0x8
#define MCCAPCON_SET_CAP0MCI1_FE_BIT 3
#define MCCAPCON_SET_CAP0MCI1_RE_MASK 0x4
#define MCCAPCON_SET_CAP0MCI1_RE 0x4
#define MCCAPCON_SET_CAP0MCI1_RE_BIT 2
#define MCCAPCON_SET_CAP0MCI0_FE_MASK 0x2
#define MCCAPCON_SET_CAP0MCI0_FE 0x2
#define MCCAPCON_SET_CAP0MCI0_FE_BIT 1
#define MCCAPCON_SET_CAP0MCI0_RE_MASK 0x1
#define MCCAPCON_SET_CAP0MCI0_RE 0x1
#define MCCAPCON_SET_CAP0MCI0_RE_BIT 0
#define MCCAPCON_SET_CAP1MCI2_FE_MASK 0x800
#define MCCAPCON_SET_CAP1MCI2_FE 0x800
#define MCCAPCON_SET_CAP1MCI2_FE_BIT 11
#define MCCAPCON_SET_CAP1MCI2_RE_MASK 0x400
#define MCCAPCON_SET_CAP1MCI2_RE 0x400
#define MCCAPCON_SET_CAP1MCI2_RE_BIT 10
#define MCCAPCON_SET_CAP1MCI1_FE_MASK 0x200
#define MCCAPCON_SET_CAP1MCI1_FE 0x200
#define MCCAPCON_SET_CAP1MCI1_FE_BIT 9
#define MCCAPCON_SET_CAP1MCI1_RE_MASK 0x100
#define MCCAPCON_SET_CAP1MCI1_RE 0x100
#define MCCAPCON_SET_CAP1MCI1_RE_BIT 8
#define MCCAPCON_SET_CAP1MCI0_FE_MASK 0x80
#define MCCAPCON_SET_CAP1MCI0_FE 0x80
#define MCCAPCON_SET_CAP1MCI0_FE_BIT 7
#define MCCAPCON_SET_CAP1MCI0_RE_MASK 0x40
#define MCCAPCON_SET_CAP1MCI0_RE 0x40
#define MCCAPCON_SET_CAP1MCI0_RE_BIT 6
#define MCCAPCON_SET_CAP2MCI2_FE_MASK 0x20000
#define MCCAPCON_SET_CAP2MCI2_FE 0x20000
#define MCCAPCON_SET_CAP2MCI2_FE_BIT 17
#define MCCAPCON_SET_CAP2MCI2_RE_MASK 0x10000
#define MCCAPCON_SET_CAP2MCI2_RE 0x10000
#define MCCAPCON_SET_CAP2MCI2_RE_BIT 16
#define MCCAPCON_SET_CAP2MCI1_FE_MASK 0x8000
#define MCCAPCON_SET_CAP2MCI1_FE 0x8000
#define MCCAPCON_SET_CAP2MCI1_FE_BIT 15
#define MCCAPCON_SET_CAP2MCI1_RE_MASK 0x4000
#define MCCAPCON_SET_CAP2MCI1_RE 0x4000
#define MCCAPCON_SET_CAP2MCI1_RE_BIT 14
#define MCCAPCON_SET_CAP2MCI0_FE_MASK 0x2000
#define MCCAPCON_SET_CAP2MCI0_FE 0x2000
#define MCCAPCON_SET_CAP2MCI0_FE_BIT 13
#define MCCAPCON_SET_CAP2MCI0_RE_MASK 0x1000
#define MCCAPCON_SET_CAP2MCI0_RE 0x1000
#define MCCAPCON_SET_CAP2MCI0_RE_BIT 12

#define MCCAPCON_CLR (*(volatile unsigned *)0x400E8014)
#define MCCAPCON_CLR_HNFCAP0_MASK 0x200000
#define MCCAPCON_CLR_HNFCAP0 0x200000
#define MCCAPCON_CLR_HNFCAP0_BIT 21
#define MCCAPCON_CLR_RT0_MASK 0x40000
#define MCCAPCON_CLR_RT0 0x40000
#define MCCAPCON_CLR_RT0_BIT 18
#define MCCAPCON_CLR_HNFCAP1_MASK 0x400000
#define MCCAPCON_CLR_HNFCAP1 0x400000
#define MCCAPCON_CLR_HNFCAP1_BIT 22
#define MCCAPCON_CLR_RT1_MASK 0x80000
#define MCCAPCON_CLR_RT1 0x80000
#define MCCAPCON_CLR_RT1_BIT 19
#define MCCAPCON_CLR_HNFCAP2_MASK 0x800000
#define MCCAPCON_CLR_HNFCAP2 0x800000
#define MCCAPCON_CLR_HNFCAP2_BIT 23
#define MCCAPCON_CLR_RT2_MASK 0x100000
#define MCCAPCON_CLR_RT2 0x100000
#define MCCAPCON_CLR_RT2_BIT 20
#define MCCAPCON_CLR_CAP0MCI2_FE_MASK 0x20
#define MCCAPCON_CLR_CAP0MCI2_FE 0x20
#define MCCAPCON_CLR_CAP0MCI2_FE_BIT 5
#define MCCAPCON_CLR_CAP0MCI2_RE_MASK 0x10
#define MCCAPCON_CLR_CAP0MCI2_RE 0x10
#define MCCAPCON_CLR_CAP0MCI2_RE_BIT 4
#define MCCAPCON_CLR_CAP0MCI1_FE_MASK 0x8
#define MCCAPCON_CLR_CAP0MCI1_FE 0x8
#define MCCAPCON_CLR_CAP0MCI1_FE_BIT 3
#define MCCAPCON_CLR_CAP0MCI1_RE_MASK 0x4
#define MCCAPCON_CLR_CAP0MCI1_RE 0x4
#define MCCAPCON_CLR_CAP0MCI1_RE_BIT 2
#define MCCAPCON_CLR_CAP0MCI0_FE_MASK 0x2
#define MCCAPCON_CLR_CAP0MCI0_FE 0x2
#define MCCAPCON_CLR_CAP0MCI0_FE_BIT 1
#define MCCAPCON_CLR_CAP0MCI0_RE_MASK 0x1
#define MCCAPCON_CLR_CAP0MCI0_RE 0x1
#define MCCAPCON_CLR_CAP0MCI0_RE_BIT 0
#define MCCAPCON_CLR_CAP1MCI2_FE_MASK 0x800
#define MCCAPCON_CLR_CAP1MCI2_FE 0x800
#define MCCAPCON_CLR_CAP1MCI2_FE_BIT 11
#define MCCAPCON_CLR_CAP1MCI2_RE_MASK 0x400
#define MCCAPCON_CLR_CAP1MCI2_RE 0x400
#define MCCAPCON_CLR_CAP1MCI2_RE_BIT 10
#define MCCAPCON_CLR_CAP1MCI1_FE_MASK 0x200
#define MCCAPCON_CLR_CAP1MCI1_FE 0x200
#define MCCAPCON_CLR_CAP1MCI1_FE_BIT 9
#define MCCAPCON_CLR_CAP1MCI1_RE_MASK 0x100
#define MCCAPCON_CLR_CAP1MCI1_RE 0x100
#define MCCAPCON_CLR_CAP1MCI1_RE_BIT 8
#define MCCAPCON_CLR_CAP1MCI0_FE_MASK 0x80
#define MCCAPCON_CLR_CAP1MCI0_FE 0x80
#define MCCAPCON_CLR_CAP1MCI0_FE_BIT 7
#define MCCAPCON_CLR_CAP1MCI0_RE_MASK 0x40
#define MCCAPCON_CLR_CAP1MCI0_RE 0x40
#define MCCAPCON_CLR_CAP1MCI0_RE_BIT 6
#define MCCAPCON_CLR_CAP2MCI2_FE_MASK 0x20000
#define MCCAPCON_CLR_CAP2MCI2_FE 0x20000
#define MCCAPCON_CLR_CAP2MCI2_FE_BIT 17
#define MCCAPCON_CLR_CAP2MCI2_RE_MASK 0x10000
#define MCCAPCON_CLR_CAP2MCI2_RE 0x10000
#define MCCAPCON_CLR_CAP2MCI2_RE_BIT 16
#define MCCAPCON_CLR_CAP2MCI1_FE_MASK 0x8000
#define MCCAPCON_CLR_CAP2MCI1_FE 0x8000
#define MCCAPCON_CLR_CAP2MCI1_FE_BIT 15
#define MCCAPCON_CLR_CAP2MCI1_RE_MASK 0x4000
#define MCCAPCON_CLR_CAP2MCI1_RE 0x4000
#define MCCAPCON_CLR_CAP2MCI1_RE_BIT 14
#define MCCAPCON_CLR_CAP2MCI0_FE_MASK 0x2000
#define MCCAPCON_CLR_CAP2MCI0_FE 0x2000
#define MCCAPCON_CLR_CAP2MCI0_FE_BIT 13
#define MCCAPCON_CLR_CAP2MCI0_RE_MASK 0x1000
#define MCCAPCON_CLR_CAP2MCI0_RE 0x1000
#define MCCAPCON_CLR_CAP2MCI0_RE_BIT 12

#define MCTC0 (*(volatile unsigned *)0x400E8018)

#define MCTC1 (*(volatile unsigned *)0x400E801C)

#define MCTC2 (*(volatile unsigned *)0x400E8020)

#define MCLIM0 (*(volatile unsigned *)0x400E8024)

#define MCLIM1 (*(volatile unsigned *)0x400E8028)

#define MCLIM2 (*(volatile unsigned *)0x400E802C)

#define MCMAT0 (*(volatile unsigned *)0x400E8030)

#define MCMAT1 (*(volatile unsigned *)0x400E8034)

#define MCMAT2 (*(volatile unsigned *)0x400E8038)

#define MCDT (*(volatile unsigned *)0x400E803C)
#define MCDT_DT2_MASK 0x3FF00000
#define MCDT_DT2_BIT 20
#define MCDT_DT1_MASK 0xFFC00
#define MCDT_DT1_BIT 10
#define MCDT_DT0_MASK 0x3FF
#define MCDT_DT0_BIT 0

#define MCCP (*(volatile unsigned *)0x400E8040)
#define MCCP_CCPB2_MASK 0x20
#define MCCP_CCPB2 0x20
#define MCCP_CCPB2_BIT 5
#define MCCP_CCPA2_MASK 0x10
#define MCCP_CCPA2 0x10
#define MCCP_CCPA2_BIT 4
#define MCCP_CCPB1_MASK 0x8
#define MCCP_CCPB1 0x8
#define MCCP_CCPB1_BIT 3
#define MCCP_CCPA1_MASK 0x4
#define MCCP_CCPA1 0x4
#define MCCP_CCPA1_BIT 2
#define MCCP_CCPB0_MASK 0x2
#define MCCP_CCPB0 0x2
#define MCCP_CCPB0_BIT 1
#define MCCP_CCPA0_MASK 0x1
#define MCCP_CCPA0 0x1
#define MCCP_CCPA0_BIT 0

#define MCCAP0 (*(volatile unsigned *)0x400E8044)

#define MCCAP1 (*(volatile unsigned *)0x400E8048)

#define MCCAP2 (*(volatile unsigned *)0x400E804C)

#define MCINTEN (*(volatile unsigned *)0x400E8050)
#define MCINTEN_ILIM0_MASK 0x1
#define MCINTEN_ILIM0 0x1
#define MCINTEN_ILIM0_BIT 0
#define MCINTEN_IMAT0_MASK 0x2
#define MCINTEN_IMAT0 0x2
#define MCINTEN_IMAT0_BIT 1
#define MCINTEN_ICAP0_MASK 0x4
#define MCINTEN_ICAP0 0x4
#define MCINTEN_ICAP0_BIT 2
#define MCINTEN_ILIM1_MASK 0x10
#define MCINTEN_ILIM1 0x10
#define MCINTEN_ILIM1_BIT 4
#define MCINTEN_IMAT1_MASK 0x20
#define MCINTEN_IMAT1 0x20
#define MCINTEN_IMAT1_BIT 5
#define MCINTEN_ICAP1_MASK 0x40
#define MCINTEN_ICAP1 0x40
#define MCINTEN_ICAP1_BIT 6
#define MCINTEN_ILIM2_MASK 0x100
#define MCINTEN_ILIM2 0x100
#define MCINTEN_ILIM2_BIT 8
#define MCINTEN_IMAT2_MASK 0x200
#define MCINTEN_IMAT2 0x200
#define MCINTEN_IMAT2_BIT 9
#define MCINTEN_ICAP2_MASK 0x400
#define MCINTEN_ICAP2 0x400
#define MCINTEN_ICAP2_BIT 10
#define MCINTEN_ABORT_MASK 0x8000
#define MCINTEN_ABORT 0x8000
#define MCINTEN_ABORT_BIT 15

#define MCINTEN_SET (*(volatile unsigned *)0x400E8054)
#define MCINTEN_SET_ILIM0_MASK 0x1
#define MCINTEN_SET_ILIM0 0x1
#define MCINTEN_SET_ILIM0_BIT 0
#define MCINTEN_SET_IMAT0_MASK 0x2
#define MCINTEN_SET_IMAT0 0x2
#define MCINTEN_SET_IMAT0_BIT 1
#define MCINTEN_SET_ICAP0_MASK 0x4
#define MCINTEN_SET_ICAP0 0x4
#define MCINTEN_SET_ICAP0_BIT 2
#define MCINTEN_SET_ILIM1_MASK 0x10
#define MCINTEN_SET_ILIM1 0x10
#define MCINTEN_SET_ILIM1_BIT 4
#define MCINTEN_SET_IMAT1_MASK 0x20
#define MCINTEN_SET_IMAT1 0x20
#define MCINTEN_SET_IMAT1_BIT 5
#define MCINTEN_SET_ICAP1_MASK 0x40
#define MCINTEN_SET_ICAP1 0x40
#define MCINTEN_SET_ICAP1_BIT 6
#define MCINTEN_SET_ILIM2_MASK 0x100
#define MCINTEN_SET_ILIM2 0x100
#define MCINTEN_SET_ILIM2_BIT 8
#define MCINTEN_SET_IMAT2_MASK 0x200
#define MCINTEN_SET_IMAT2 0x200
#define MCINTEN_SET_IMAT2_BIT 9
#define MCINTEN_SET_ICAP2_MASK 0x400
#define MCINTEN_SET_ICAP2 0x400
#define MCINTEN_SET_ICAP2_BIT 10
#define MCINTEN_SET_ABORT_MASK 0x8000
#define MCINTEN_SET_ABORT 0x8000
#define MCINTEN_SET_ABORT_BIT 15

#define MCINTEN_CLR (*(volatile unsigned *)0x400E8058)
#define MCINTEN_CLR_ILIM0_MASK 0x1
#define MCINTEN_CLR_ILIM0 0x1
#define MCINTEN_CLR_ILIM0_BIT 0
#define MCINTEN_CLR_IMAT0_MASK 0x2
#define MCINTEN_CLR_IMAT0 0x2
#define MCINTEN_CLR_IMAT0_BIT 1
#define MCINTEN_CLR_ICAP0_MASK 0x4
#define MCINTEN_CLR_ICAP0 0x4
#define MCINTEN_CLR_ICAP0_BIT 2
#define MCINTEN_CLR_ILIM1_MASK 0x10
#define MCINTEN_CLR_ILIM1 0x10
#define MCINTEN_CLR_ILIM1_BIT 4
#define MCINTEN_CLR_IMAT1_MASK 0x20
#define MCINTEN_CLR_IMAT1 0x20
#define MCINTEN_CLR_IMAT1_BIT 5
#define MCINTEN_CLR_ICAP1_MASK 0x40
#define MCINTEN_CLR_ICAP1 0x40
#define MCINTEN_CLR_ICAP1_BIT 6
#define MCINTEN_CLR_ILIM2_MASK 0x100
#define MCINTEN_CLR_ILIM2 0x100
#define MCINTEN_CLR_ILIM2_BIT 8
#define MCINTEN_CLR_IMAT2_MASK 0x200
#define MCINTEN_CLR_IMAT2 0x200
#define MCINTEN_CLR_IMAT2_BIT 9
#define MCINTEN_CLR_ICAP2_MASK 0x400
#define MCINTEN_CLR_ICAP2 0x400
#define MCINTEN_CLR_ICAP2_BIT 10
#define MCINTEN_CLR_ABORT_MASK 0x8000
#define MCINTEN_CLR_ABORT 0x8000
#define MCINTEN_CLR_ABORT_BIT 15

#define MCINTFLAG (*(volatile unsigned *)0x400E8068)
#define MCINTFLAG_ILIM0_MASK 0x1
#define MCINTFLAG_ILIM0 0x1
#define MCINTFLAG_ILIM0_BIT 0
#define MCINTFLAG_IMAT0_MASK 0x2
#define MCINTFLAG_IMAT0 0x2
#define MCINTFLAG_IMAT0_BIT 1
#define MCINTFLAG_ICAP0_MASK 0x4
#define MCINTFLAG_ICAP0 0x4
#define MCINTFLAG_ICAP0_BIT 2
#define MCINTFLAG_ILIM1_MASK 0x10
#define MCINTFLAG_ILIM1 0x10
#define MCINTFLAG_ILIM1_BIT 4
#define MCINTFLAG_IMAT1_MASK 0x20
#define MCINTFLAG_IMAT1 0x20
#define MCINTFLAG_IMAT1_BIT 5
#define MCINTFLAG_ICAP1_MASK 0x40
#define MCINTFLAG_ICAP1 0x40
#define MCINTFLAG_ICAP1_BIT 6
#define MCINTFLAG_ILIM2_MASK 0x100
#define MCINTFLAG_ILIM2 0x100
#define MCINTFLAG_ILIM2_BIT 8
#define MCINTFLAG_IMAT2_MASK 0x200
#define MCINTFLAG_IMAT2 0x200
#define MCINTFLAG_IMAT2_BIT 9
#define MCINTFLAG_ICAP2_MASK 0x400
#define MCINTFLAG_ICAP2 0x400
#define MCINTFLAG_ICAP2_BIT 10
#define MCINTFLAG_ABORT_MASK 0x8000
#define MCINTFLAG_ABORT 0x8000
#define MCINTFLAG_ABORT_BIT 15

#define MCINTFLAG_SET (*(volatile unsigned *)0x400E806C)
#define MCINTFLAG_SET_ILIM0_MASK 0x1
#define MCINTFLAG_SET_ILIM0 0x1
#define MCINTFLAG_SET_ILIM0_BIT 0
#define MCINTFLAG_SET_IMAT0_MASK 0x2
#define MCINTFLAG_SET_IMAT0 0x2
#define MCINTFLAG_SET_IMAT0_BIT 1
#define MCINTFLAG_SET_ICAP0_MASK 0x4
#define MCINTFLAG_SET_ICAP0 0x4
#define MCINTFLAG_SET_ICAP0_BIT 2
#define MCINTFLAG_SET_ILIM1_MASK 0x10
#define MCINTFLAG_SET_ILIM1 0x10
#define MCINTFLAG_SET_ILIM1_BIT 4
#define MCINTFLAG_SET_IMAT1_MASK 0x20
#define MCINTFLAG_SET_IMAT1 0x20
#define MCINTFLAG_SET_IMAT1_BIT 5
#define MCINTFLAG_SET_ICAP1_MASK 0x40
#define MCINTFLAG_SET_ICAP1 0x40
#define MCINTFLAG_SET_ICAP1_BIT 6
#define MCINTFLAG_SET_ILIM2_MASK 0x100
#define MCINTFLAG_SET_ILIM2 0x100
#define MCINTFLAG_SET_ILIM2_BIT 8
#define MCINTFLAG_SET_IMAT2_MASK 0x200
#define MCINTFLAG_SET_IMAT2 0x200
#define MCINTFLAG_SET_IMAT2_BIT 9
#define MCINTFLAG_SET_ICAP2_MASK 0x400
#define MCINTFLAG_SET_ICAP2 0x400
#define MCINTFLAG_SET_ICAP2_BIT 10
#define MCINTFLAG_SET_ABORT_MASK 0x8000
#define MCINTFLAG_SET_ABORT 0x8000
#define MCINTFLAG_SET_ABORT_BIT 15

#define MCINTFLAG_CLR (*(volatile unsigned *)0x400E8070)
#define MCINTFLAG_CLR_ILIM0_MASK 0x1
#define MCINTFLAG_CLR_ILIM0 0x1
#define MCINTFLAG_CLR_ILIM0_BIT 0
#define MCINTFLAG_CLR_IMAT0_MASK 0x2
#define MCINTFLAG_CLR_IMAT0 0x2
#define MCINTFLAG_CLR_IMAT0_BIT 1
#define MCINTFLAG_CLR_ICAP0_MASK 0x4
#define MCINTFLAG_CLR_ICAP0 0x4
#define MCINTFLAG_CLR_ICAP0_BIT 2
#define MCINTFLAG_CLR_ILIM1_MASK 0x10
#define MCINTFLAG_CLR_ILIM1 0x10
#define MCINTFLAG_CLR_ILIM1_BIT 4
#define MCINTFLAG_CLR_IMAT1_MASK 0x20
#define MCINTFLAG_CLR_IMAT1 0x20
#define MCINTFLAG_CLR_IMAT1_BIT 5
#define MCINTFLAG_CLR_ICAP1_MASK 0x40
#define MCINTFLAG_CLR_ICAP1 0x40
#define MCINTFLAG_CLR_ICAP1_BIT 6
#define MCINTFLAG_CLR_ILIM2_MASK 0x100
#define MCINTFLAG_CLR_ILIM2 0x100
#define MCINTFLAG_CLR_ILIM2_BIT 8
#define MCINTFLAG_CLR_IMAT2_MASK 0x200
#define MCINTFLAG_CLR_IMAT2 0x200
#define MCINTFLAG_CLR_IMAT2_BIT 9
#define MCINTFLAG_CLR_ICAP2_MASK 0x400
#define MCINTFLAG_CLR_ICAP2 0x400
#define MCINTFLAG_CLR_ICAP2_BIT 10
#define MCINTFLAG_CLR_ABORT_MASK 0x8000
#define MCINTFLAG_CLR_ABORT 0x8000
#define MCINTFLAG_CLR_ABORT_BIT 15

#define MCCAP_CLR (*(volatile unsigned *)0x400E8074)
#define MCCAP_CLR_CAP_CLR2_MASK 0x4
#define MCCAP_CLR_CAP_CLR2 0x4
#define MCCAP_CLR_CAP_CLR2_BIT 2
#define MCCAP_CLR_CAP_CLR1_MASK 0x2
#define MCCAP_CLR_CAP_CLR1 0x2
#define MCCAP_CLR_CAP_CLR1_BIT 1
#define MCCAP_CLR_CAP_CLR0_MASK 0x1
#define MCCAP_CLR_CAP_CLR0 0x1
#define MCCAP_CLR_CAP_CLR0_BIT 0


#endif
