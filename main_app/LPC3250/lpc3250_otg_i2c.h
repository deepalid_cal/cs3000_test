#ifndef _LPC3250_OTG_I2C_H_
#define _LPC3250_OTG_I2C_H_

#include "hcc_types.h"

#define ISP1301_A0		0

#define RFE				(1<<9)
#define TFE				(1<<11)

int otg_i2c_init(void);
int otg_i2c_start(void);
int otg_i2c_stop(void);
int otg_i2c_delete(void);

hcc_u8 otg_i2c_read (hcc_u8 addr);
void otg_i2c_write (hcc_u8 addr, hcc_u8 val);

#endif