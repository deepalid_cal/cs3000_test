/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2008 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*                               v5.4.3.015                                 */
/*                                                                          */
/* ************************************************************************ */

//------------------------------------------------------------------------------

#include "GuiConst.h"
#include "GuiLib.h"
#include <string.h>
#include <stdlib.h>
#ifdef GuiConst_CODEVISION_COMPILER
#include <math.h>
#endif

#define GuiLib_CHR_PSLEFT_OFS              0
#define GuiLib_CHR_PSRIGHT_OFS             5
#define GuiLib_CHR_XLEFT_OFS               10
#define GuiLib_CHR_XWIDTH_OFS              11
#define GuiLib_CHR_YTOP_OFS                12
#define GuiLib_CHR_YHEIGHT_OFS             13
#define GuiLib_CHR_LINECTRL_OFS            14
#define GuiLib_CHR_PS_TOP_OFS              0
#define GuiLib_CHR_PS_MID_OFS              1
#define GuiLib_CHR_PS_MIDBASE_OFS          2
#define GuiLib_CHR_PS_BASE_OFS             3
#define GuiLib_CHR_PS_BOTTOM_OFS           4

#define GuiLib_ITEM_TEXT                   0
#define GuiLib_ITEM_DOT                    1
#define GuiLib_ITEM_LINE                   2
#define GuiLib_ITEM_FRAME                  3
#define GuiLib_ITEM_BLOCK                  4
#define GuiLib_ITEM_STRUCTURE              5
#define GuiLib_ITEM_STRUCTARRAY            6
#define GuiLib_ITEM_CLIPRECT               7
#define GuiLib_ITEM_VAR                    8
#define GuiLib_ITEM_FORMATTER              9
#define GuiLib_ITEM_BITMAP                 10
#define GuiLib_ITEM_TEXTBLOCK              11
#define GuiLib_ITEM_TOUCHAREA              12
#define GuiLib_ITEM_VARBLOCK               13
#define GuiLib_ITEM_ACTIVEAREA             14
#define GuiLib_ITEM_SCROLLBOX              15
#define GuiLib_ITEM_CIRCLE                 16
#define GuiLib_ITEM_ELLIPSE                17
#define GuiLib_ITEM_BACKGROUND             18
#define GuiLib_ITEM_CLEARAREA              19
#define GuiLib_ITEM_ADVGRAPH_COORDSYST     20
#define GuiLib_ITEM_ADVGRAPH_PIXEL         21
#define GuiLib_ITEM_ADVGRAPH_LINE          22
#define GuiLib_ITEM_ADVGRAPH_ARC           23
#define GuiLib_ITEM_ADVGRAPH_RECT          24
#define GuiLib_ITEM_ADVGRAPH_ELLIPSE       25
#define GuiLib_ITEM_ADVGRAPH_SEGMENT       26
#define GuiLib_ITEM_ADVGRAPH_TRIANGLE      27
#define GuiLib_ITEM_ADVGRAPH_POLYGON       28
#define GuiLib_ITEM_GRAPH                  29

#define GuiLib_ITEMBIT_TEXT                0x00000001
#define GuiLib_ITEMBIT_DOT                 0x00000002
#define GuiLib_ITEMBIT_LINE                0x00000004
#define GuiLib_ITEMBIT_FRAME               0x00000008
#define GuiLib_ITEMBIT_BLOCK               0x00000010
#define GuiLib_ITEMBIT_STRUCTURE           0x00000020
#define GuiLib_ITEMBIT_STRUCTARRAY         0x00000040
#define GuiLib_ITEMBIT_CLIPRECT            0x00000080
#define GuiLib_ITEMBIT_VAR                 0x00000100
#define GuiLib_ITEMBIT_FORMATTER           0x00000200
#define GuiLib_ITEMBIT_BITMAP              0x00000400
#define GuiLib_ITEMBIT_TEXTBLOCK           0x00000800
#define GuiLib_ITEMBIT_TOUCHAREA           0x00001000
#define GuiLib_ITEMBIT_VARBLOCK            0x00002000
#define GuiLib_ITEMBIT_ACTIVEAREA          0x00004000
#define GuiLib_ITEMBIT_SCROLLBOX           0x00008000
#define GuiLib_ITEMBIT_CIRCLE              0x00010000
#define GuiLib_ITEMBIT_ELLIPSE             0x00020000
#define GuiLib_ITEMBIT_BACKGROUND          0x00040000
#define GuiLib_ITEMBIT_CLEARAREA           0x00080000
#define GuiLib_ITEMBIT_ADVGRAPH_COORDSYST  0x00100000
#define GuiLib_ITEMBIT_ADVGRAPH_PIXEL      0x00200000
#define GuiLib_ITEMBIT_ADVGRAPH_LINE       0x00400000
#define GuiLib_ITEMBIT_ADVGRAPH_ARC        0x00800000
#define GuiLib_ITEMBIT_ADVGRAPH_RECT       0x01000000
#define GuiLib_ITEMBIT_ADVGRAPH_ELLIPSE    0x02000000
#define GuiLib_ITEMBIT_ADVGRAPH_SEGMENT    0x04000000
#define GuiLib_ITEMBIT_ADVGRAPH_TRIANGLE   0x08000000
#define GuiLib_ITEMBIT_ADVGRAPH_POLYGON    0x10000000
#define GuiLib_ITEMBIT_GRAPH               0x20000000

#define GuiLib_COOR_ABS                    0
#define GuiLib_COOR_REL                    1
#define GuiLib_COOR_REL_1                  2
#define GuiLib_COOR_REL_2                  3

#define GuiLib_COLOR_NOCHANGE              0
#define GuiLib_COLOR_FORE                  1
#define GuiLib_COLOR_BACK                  2
#define GuiLib_COLOR_OTHER                 3
#define GuiLib_COLOR_INVERT                4
#define GuiLib_COLOR_TRANSP                5

#define GuiLib_MARKER_NONE                 0
#define GuiLib_MARKER_ICON                 1
#define GuiLib_MARKER_BITMAP               2
#define GuiLib_MARKER_FIXED_BLOCK          3
#define GuiLib_MARKER_VARIABLE_BLOCK       4
#define GuiLib_MARKER_SIZE                 8

#define GuiLib_MEMORY_MIN                  1
#define GuiLib_MEMORY_MAX                  3
#define GuiLib_MEMORY_CNT                  (GuiLib_MEMORY_MAX - GuiLib_MEMORY_MIN + 1)

#define GuiLib_MARK_AS_NONE                0
#define GuiLib_MARK_AS_SCROLL_BOX          1
#define GuiLib_MARK_AS_SCROLL_BAR          2
#define GuiLib_MARK_AS_SCROLL_LINE         3

#define GuiLib_COL_INVERT_OFF              0
#define GuiLib_COL_INVERT_ON               1
#define GuiLib_COL_INVERT_IF_CURSOR        2

#define GuiLib_BITFLAG_INUSE               0x00000001
#define GuiLib_BITFLAG_TRANSPARENT         0x00000002
#define GuiLib_BITFLAG_UNDERLINE           0x00000004
#define GuiLib_BITFLAG_PATTERNEDLINE       0x00000004
#define GuiLib_BITFLAG_FORMATSHOWSIGN      0x00000008
#define GuiLib_BITFLAG_FORMATZEROPADDING   0x00000010
#define GuiLib_BITFLAG_AUTOREDRAWFIELD     0x00000020
#define GuiLib_BITFLAG_FIELDSCROLLS        0x00000040
#define GuiLib_BITFLAG_TRANSLATION         0x00000080
#define GuiLib_BITFLAG_BLINKTEXTFIELD      0x00000100
#define GuiLib_BITFLAG_CLIPPING            0x00000200
#define GuiLib_BITFLAG_ACTIVEAREARELCOORD  0x00000400
#define GuiLib_BITFLAG_REVERSEWRITING      0x00000800
#define GuiLib_BITFLAG_FORMATTRAILINGZEROS 0x00001000
#define GuiLib_BITFLAG_FORMATTHOUSANDSSEP  0x00002000
#define GuiLib_BITFLAG_FIELDSCROLLBOX      0x00004000
#define GuiLib_BITFLAG_BARTRANSPARENT      0x00008000

#define GuiLib_SCROLL_STRUCTURE_UNDEF      0
#define GuiLib_SCROLL_STRUCTURE_READ       1
#define GuiLib_SCROLL_STRUCTURE_USED       2

#define GuiLib_GRAPH_STRUCTURE_UNDEF       0
#define GuiLib_GRAPH_STRUCTURE_USED        1

#define GuiLib_GRAPH_DATATYPE_DOT          0
#define GuiLib_GRAPH_DATATYPE_LINE         1
#define GuiLib_GRAPH_DATATYPE_BAR          2
#define GuiLib_GRAPH_DATATYPE_CROSS        3
#define GuiLib_GRAPH_DATATYPE_X            4

#define GuiLib_INDICATOR_NONE              0

#define GuiLib_FULL_BITMAP                 0
#define GuiLib_AREA_BITMAP                 1

#define GuiLib_LANGUAGE_INACTIVE           9999

#define GuiLib_LINEFEED                    0x0A

#ifdef GuiConst_ARAB_CHARS_INUSE
#define GuiLib_ARAB_LIGATURES_CNT          4
const GuiConst_INT16U GuiLib_ARAB_LIGATURES[GuiLib_ARAB_LIGATURES_CNT][3] =
   {{0x0644, 0x0622, 0xFEF5},
    {0x0644, 0x0623, 0xFEF7},
    {0x0644, 0x0625, 0xFEF9},
    {0x0644, 0x0627, 0xFEFB}};

#define GuiLib_ARAB_CHAR_PRI_MIN           0x0622
#define GuiLib_ARAB_CHAR_PRI_MAX           0x06D6
#define GuiLib_ARAB_CHAR_SEC_MIN           0xFB50
#define GuiLib_ARAB_CHAR_SEC_MAX           0xFEF4
#define GuiLib_ARAB_CHAR_TYPE_ISO          0
#define GuiLib_ARAB_CHAR_TYPE_FIN          1
#define GuiLib_ARAB_CHAR_TYPE_INI          2
#define GuiLib_ARAB_CHAR_TYPE_MED          3
#define GuiLib_ARAB_CHAR_ISOFIN            0x02
#define GuiLib_ARAB_CHAR_ISOFININIMED      0x04
#define GuiLib_ARAB_CHAR_DIACRITIC         0x0A
#define GuiLib_ARAB_CHAR_CONVERT_CNT       83

const GuiConst_INT16U GuiLib_ARAB_CHAR_CONVERT[GuiLib_ARAB_CHAR_CONVERT_CNT][3] =
   {{0x0622, 0xFE81, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0623, 0xFE83, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0624, 0xFE85, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0625, 0xFE87, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0626, 0xFE89, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0627, 0xFE8D, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0628, 0xFE8F, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0629, 0xFE93, GuiLib_ARAB_CHAR_ISOFIN},
    {0x062A, 0xFE95, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x062B, 0xFE99, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x062C, 0xFE9D, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x062D, 0xFEA1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x062E, 0xFEA5, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x062F, 0xFEA9, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0630, 0xFEAB, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0631, 0xFEAD, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0632, 0xFEAF, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0633, 0xFEB1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0634, 0xFEB5, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0635, 0xFEB9, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0636, 0xFEBD, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0637, 0xFEC1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0638, 0xFEC5, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0639, 0xFEC9, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x063A, 0xFECD, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0641, 0xFED1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0642, 0xFED5, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0643, 0xFED9, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0644, 0xFEDD, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0645, 0xFEE1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0646, 0xFEE5, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0647, 0xFEE9, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0648, 0xFEED, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0649, 0xFEEF, GuiLib_ARAB_CHAR_ISOFIN},
    {0x064A, 0xFEF1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x064E, 0xFE76, GuiLib_ARAB_CHAR_DIACRITIC},
    {0x064F, 0xFE78, GuiLib_ARAB_CHAR_DIACRITIC},
    {0x0650, 0xFE7A, GuiLib_ARAB_CHAR_DIACRITIC},
    {0x0651, 0xFE7C, GuiLib_ARAB_CHAR_DIACRITIC},
    {0x0652, 0xFE7E, GuiLib_ARAB_CHAR_DIACRITIC},
    {0x0671, 0xFB50, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0679, 0xFB66, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x067A, 0xFB5E, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x067B, 0xFB52, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x067E, 0xFB56, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x067F, 0xFB62, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0680, 0xFB5A, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0683, 0xFB76, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0684, 0xFB72, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0686, 0xFB7A, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0687, 0xFB7E, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0688, 0xFB88, GuiLib_ARAB_CHAR_ISOFIN},
    {0x068C, 0xFB84, GuiLib_ARAB_CHAR_ISOFIN},
    {0x068D, 0xFB82, GuiLib_ARAB_CHAR_ISOFIN},
    {0x068E, 0xFB86, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0691, 0xFB8C, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0698, 0xFB8A, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06A4, 0xFB6A, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06A6, 0xFB6E, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06A9, 0xFB8E, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06AD, 0xFBD3, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06AF, 0xFB92, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06B1, 0xFB9A, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06B3, 0xFB96, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06BA, 0xFB9E, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06BB, 0xFBA0, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06BE, 0xFBAA, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06C0, 0xFBA4, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06C1, 0xFBA6, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06C5, 0xFBE0, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06C6, 0xFBD9, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06C7, 0xFBD7, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06C8, 0xFBDB, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06C9, 0xFBE2, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06CB, 0xFBDE, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06CC, 0xFBFC, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06D0, 0xFBE4, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06D2, 0xFBAE, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06D3, 0xFBB0, GuiLib_ARAB_CHAR_ISOFIN},
    {     0, 0xFEF5, GuiLib_ARAB_CHAR_ISOFIN},
    {     0, 0xFEF7, GuiLib_ARAB_CHAR_ISOFIN},
    {     0, 0xFEF9, GuiLib_ARAB_CHAR_ISOFIN},
    {     0, 0xFEFB, GuiLib_ARAB_CHAR_ISOFIN}};
#endif

//------------------------------------------------------------------------------
#ifdef GuiConst_REMOTE_STRUCT_DATA
typedef GuiConst_PTR *GuiLib_StructPtr;
#else
#ifdef GuiConst_AVRGCC_COMPILER
unsigned char displayVarNow;
typedef GuiConst_PTR *GuiLib_StructPtr;
#else
#ifdef GuiConst_ICC_COMPILER
typedef void PrefixRom *GuiLib_StructPtr;
unsigned char displayVarNow;
#else
#ifdef GuiConst_CODEVISION_COMPILER
typedef char PrefixRom *GuiLib_StructPtr;
unsigned char displayVarNow;
#else
typedef GuiConst_PTR *GuiLib_StructPtr;
#endif
#endif
#endif
#endif
typedef struct
{
  void *VarPtr;
  GuiConst_TEXT *TextPtr;
  GuiConst_INT16U StructToCallIndex;
  GuiConst_INT16S X1, Y1, X2, Y2;
  GuiConst_INT16S BackBoxSizeX;
  GuiConst_INT8U BackBoxSizeY1, BackBoxSizeY2;
  GuiConst_INT16S RX, RY;
  GuiConst_INT16S RX1, RY1, RX2, RY2;
  GuiConst_INT8U Drawn;
  GuiConst_INT16S DrawnX1, DrawnY1, DrawnX2, DrawnY2;
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
  GuiConst_INT16S CoordOrigoX, CoordOrigoY;
#endif
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiConst_INT16S ClipRectX1, ClipRectY1, ClipRectX2, ClipRectY2;
#endif
  GuiConst_INT8U FontIndex;
  GuiConst_INT16U TextLength;
  GuiConst_INT8U ItemType;
  GuiConst_INT32U BitFlags;
  GuiConst_INT8U Alignment;
  GuiConst_INTCOLOR ForeColor, BackColor;
  GuiConst_INTCOLOR BarForeColor, BarBackColor;
  GuiConst_INT8U BackBorderPixels;
  GuiConst_INT8U Ps;
  GuiConst_INT8U FrameThickness;
  GuiConst_INT8U FormatFieldWidth;
  GuiConst_INT8U FormatDecimals;
  GuiConst_INT8U FormatAlignment;
  GuiConst_INT8U FormatFormat;
#ifdef GuiConst_CURSOR_SUPPORT_ON
  GuiConst_INT8S CursorFieldNo;
  GuiConst_INT8U CursorFieldLevel;
  GuiConst_INT8S AutoRedrawCursorFieldNo;
#endif
#ifdef GuiConst_BLINK_SUPPORT_ON
  GuiConst_INT8S BlinkFieldNo;
#endif
#ifdef GuiConst_CHARMODE_ANSI
  GuiConst_INT8S CharSetSelector;
#endif
  GuiConst_INT8U VarType;
  GuiConst_INT16U IndexCount;
#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
  GuiConst_INT8U TextBoxHorzAlignment;
  GuiConst_INT8U TextBoxVertAlignment;
  GuiConst_INT8S TextBoxLineDist;
  GuiConst_INT8U TextBoxLineDistRelToFont;
#endif
#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
  GuiConst_INT8U TouchAreaNo;
#endif
#ifdef GuiConst_TEXTBOX_FIELDS_ON
  GuiConst_INT8U TextBoxScrollIndex;
  GuiConst_INT16S TextBoxScrollPos;
  GuiConst_INT16U TextBoxLines;
#endif
#ifdef GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INTCOLOR BitmapTranspColor;
#endif
  GuiConst_INT8U LinePattern;
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  GuiConst_INT8U CursorScrollBoxIndex;
#endif
} GuiLib_ItemRec;
typedef GuiLib_ItemRec* GuiLib_ItemRecPtr;
#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
typedef struct
{
  GuiConst_INT8U InUse;
  GuiConst_INT16S X1, Y1, X2, Y2;
} GuiLib_TouchAreaRec;
#endif
typedef GuiConst_INT16S TextXOfsAry[GuiConst_MAX_TEXT_LEN + 2];
typedef TextXOfsAry* TextXOfsAryPtr;
#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
typedef struct
{
  GuiConst_TEXT PrefixGeneric *TextPtr;
  GuiConst_INT8U InUse;
  GuiConst_INT8U Active;
  GuiConst_INT8U XSize;
  GuiConst_INT8U Ps;
  GuiConst_INT8U ItemType;
  GuiConst_INT8U VarType;
#ifdef GuiConst_CHARMODE_ANSI
  GuiConst_INT8S CharSetSelector;
#endif
  GuiConst_INT8U Alignment;
  GuiConst_INT8U FormatFieldWidth;
  GuiConst_INT8U FormatDecimals;
  GuiConst_INT8U FormatAlignment;
  GuiConst_INT8U FormatFormat;
  GuiConst_INT8U FontIndex;
  GuiConst_INT8U PsNumWidth;
  GuiConst_INT8U PsSpace;
  GuiConst_INT8U BlinkBoxRate;
  GuiConst_INT8U BlinkBoxState;
  GuiConst_INT8U BlinkBoxInverted;
  GuiConst_INT16U CharNo;
  GuiConst_INT32U BitFlags;
  GuiConst_INT16U CharCnt;
  GuiConst_INT16S X1, X2;
  GuiConst_INT16S Y1, Y2;
  GuiConst_INT16S BlinkBoxX1, BlinkBoxX2;
  GuiConst_INT16S BlinkBoxY1, BlinkBoxY2;
#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
  GuiConst_INT8U YSize;
  GuiConst_INT16S LineCnt;
  GuiConst_INT16S BlindLinesAtTop;
  GuiConst_INT8S TextBoxLineDist;
  GuiConst_INT8U TextBoxHorzAlignment;
  GuiConst_INT8U TextBoxVertAlignment;
#endif
#ifdef GuiConst_TEXTBOX_FIELDS_ON
  GuiConst_INT16S TextBoxScrollPos;
#endif
} GuiLib_BlinkTextItemRec;
#endif
#endif
GuiConst_INT8U Drawn;
GuiConst_INT16S DrawnX1, DrawnY1, DrawnX2, DrawnY2;
GuiConst_INT8U Dummy1_8U;
GuiConst_INT8U Dummy2_8U;
GuiConst_INT8U Dummy3_8U;
GuiConst_INT16S Dummy1_16S;
GuiLib_DisplayLineRec GuiLib_DisplayRepaint[GuiConst_BYTE_LINES];
#ifdef GuiConst_ARAB_CHARS_INUSE
GuiConst_INT8U ArabicCharJoiningMode[GuiConst_MAX_TEXT_LEN + 2];
GuiConst_INT8U ArabicCharJoiningModeBefore;
GuiConst_INT8U ArabicCharJoiningModeAfter;
GuiConst_INT16S ArabicCharJoiningModeIndex[GuiConst_MAX_TEXT_LEN + 2];
#endif
#ifdef GuiConst_CURSOR_SUPPORT_ON
GuiConst_INT16S GuiLib_ActiveCursorFieldNo;
#endif
GuiConst_INT16S GuiLib_AutoRedrawFieldNo;
#ifdef GuiConst_SCROLL_SUPPORT_ON
GuiConst_INT16S GuiLib_ScrollActiveLine;
GuiConst_INT16S GuiLib_ScrollTopLine;
GuiConst_INT16S GuiLib_ScrollVisibleLines;
#endif
GuiConst_INT16S GuiLib_LanguageIndex;
#ifdef GuiConst_CHARMODE_ANSI
GuiConst_INT8U GuiLib_LanguageCharSet;
GuiConst_INT8U GuiLib_CurCharSet;
#endif
GuiConst_INT16S GuiLib_CurStructureNdx;
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
GuiConst_INT8U GuiLib_DisplayUpsideDown;
#endif
static GuiConst_INT32U RefreshClock;
#ifdef GuiConst_REMOTE_DATA
void (*GuiLib_RemoteDataReadBlock) (
   GuiConst_INT32U SourceOffset,
   GuiConst_INT32U SourceSize,
   GuiConst_INT8U * TargetAddr);
#ifdef GuiConst_REMOTE_TEXT_DATA
void (*GuiLib_RemoteTextReadBlock) (
   GuiConst_INT32U SourceOffset,
   GuiConst_INT32U SourceSize,
   void * TargetAddr);
#endif
#ifdef GuiConst_REMOTE_FONT_DATA
static GuiConst_INT8U GuiLib_RemoteFontBuffer[GuiConst_REMOTE_FONT_BUF_SIZE];
static GuiConst_INT32S CurRemoteFont;
#endif
#ifdef GuiConst_REMOTE_STRUCT_DATA
static GuiConst_INT8U GuiLib_RemoteStructBuffer[GuiConst_REMOTE_STRUCT_BUF_SIZE];
static GuiConst_INT32S CurRemoteStruct;
#endif
#ifdef GuiConst_REMOTE_TEXT_DATA
static GuiConst_TEXT GuiLib_RemoteTextBuffer[GuiConst_REMOTE_TEXT_BUF_SIZE];
static GuiConst_INT16U RemoteTextLen;
static GuiConst_INT32S CurRemoteText;
static GuiConst_INT32S RemoteTextTableOfs;
#endif
#ifdef GuiConst_REMOTE_BITMAP_DATA
static GuiConst_INT8U GuiLib_RemoteBitmapBuffer[GuiConst_REMOTE_BITMAP_BUF_SIZE];
static GuiConst_INT32S CurRemoteBitmap;
#endif
#endif
static GuiConst_INT16S DisplayLevel;
static GuiConst_INT8U DisplayWriting;
static GuiLib_ItemRec CurItem;
#ifdef GuiConst_CURSOR_SUPPORT_ON
#ifndef GuiConst_CURSOR_FIELDS_OFF
static GuiLib_ItemRec CursorItems[GuiConst_CURSOR_FIELDS_MAX];
#endif
#endif
static GuiLib_ItemRec AutoRedrawItems[GuiConst_AUTOREDRAW_FIELDS_MAX];
static GuiConst_INT8U AutoRedrawLevel[GuiConst_AUTOREDRAW_FIELDS_MAX];
static GuiConst_INT16S AutoRedrawSaveIndex;
#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
static GuiConst_INT8U AutoRedrawOldVal[GuiConst_AUTOREDRAW_FIELDS_MAX]
#ifdef GuiConst_CHARMODE_ANSI
                                      [GuiConst_AUTOREDRAW_MAX_VAR_SIZE];
#else
                                      [2 * GuiConst_AUTOREDRAW_MAX_VAR_SIZE];
#endif
static GuiConst_INT16U AutoRedrawSize[GuiConst_AUTOREDRAW_FIELDS_MAX];
#endif
#ifdef GuiConst_TEXTBOX_FIELDS_ON
static GuiLib_ItemRec TextboxScrollItems[GuiConst_TEXTBOX_FIELDS_MAX];
#endif
#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
static GuiLib_TouchAreaRec TouchAreas[GuiConst_TOUCHAREA_MAX];
static GuiConst_INT8U TouchAdjustActive;
static GuiConst_INT8U TouchAdjustInUse[4];
static GuiConst_INT16S TouchAdjustXTrue[4];
static GuiConst_INT16S TouchAdjustYTrue[4];
static GuiConst_INT32S TouchAdjustXMeasured[4];
static GuiConst_INT32S TouchAdjustYMeasured[4];
static GuiConst_INT32S TouchAdjustXTL, TouchAdjustYTL;
static GuiConst_INT32S TouchAdjustXTR, TouchAdjustYTR;
static GuiConst_INT32S TouchAdjustXBL, TouchAdjustYBL;
static GuiConst_INT32S TouchAdjustXBR, TouchAdjustYBR;
#endif
static GuiConst_INT16S XMemory[GuiLib_MEMORY_CNT];
static GuiConst_INT16S YMemory[GuiLib_MEMORY_CNT];
#ifdef GuiConst_REMOTE_FONT_DATA
static GuiConst_INT32U TextCharNdx[GuiConst_MAX_TEXT_LEN + 1];
#else
static GuiConst_INT8U PrefixRom *TextCharPtrAry[GuiConst_MAX_TEXT_LEN + 1];
#endif
static GuiConst_INT8U TextPsMode[GuiConst_MAX_TEXT_LEN + 1];
static GuiLib_FontRecPtr CurFont;
static GuiConst_INT16S FontWriteX1, FontWriteY1, FontWriteX2, FontWriteY2;
static GuiConst_CHAR VarNumTextStr[GuiConst_MAX_VARNUM_TEXT_LEN + 1];
#ifdef GuiConst_FLOAT_SUPPORT_ON
static GuiConst_INT16S VarExponent;
#endif
#ifdef GuiConst_CHARMODE_UNICODE
static GuiConst_TEXT VarNumUnicodeTextStr[GuiConst_MAX_VARNUM_TEXT_LEN + 1];
static GuiConst_TEXT UnicodeTextBuf[GuiConst_MAX_TEXT_LEN + 1];
#else
static GuiConst_TEXT AnsiTextBuf[GuiConst_MAX_TEXT_LEN + 1];
#endif
static GuiConst_INT8U InitialDrawing;
static GuiConst_INT8U DrawingLevel;
static GuiLib_StructPtr TopLevelStructure;
#ifdef GuiConst_CURSOR_SUPPORT_ON
static GuiConst_INT8U CursorInUse;
static GuiConst_INT16S CursorFieldFound;
static GuiConst_INT8U CursorActiveFieldFound;
#endif
static GuiConst_INT8U SwapColors;
#ifdef GuiConst_SCROLL_SUPPORT_ON
static GuiLib_ItemRec ScrollLineItem;
static void (*ScrollLineDataFunc) (GuiConst_INT16S LineIndex);
static GuiConst_INT16S ScrollBoxX1, ScrollBoxY1, ScrollBoxX2, ScrollBoxY2;
static GuiConst_INT16S ScrollBarX1, ScrollBarY1, ScrollBarX2, ScrollBarY2;
static GuiConst_INT16S ScrollBarSize;
static GuiConst_INT16S ScrollBarArrowHeight;
static GuiConst_INT16S ScrollBarIndicatorRange;
static GuiConst_INT16S ScrollLineDY;
static GuiConst_INT8U ScrollBoxDetected;
static GuiConst_INT8U ScrollBarDetected;
static GuiConst_INT8U ScrollLineDetected;
static GuiConst_INTCOLOR ScrollBarMarkerForeColor;
static GuiConst_INTCOLOR ScrollBarMarkerBackColor;
static GuiConst_INT16S ScrollNoOfLines;
static GuiConst_INT8U InsideScrollLine;
#ifdef GuiConst_CURSOR_SUPPORT_ON
static GuiConst_INT8U CursorFieldsInScrollLine;
#endif
#endif
#ifdef GuiConst_BLINK_SUPPORT_ON
static GuiConst_INT16S BlinkBoxX1, BlinkBoxY1, BlinkBoxX2, BlinkBoxY2;
static GuiConst_INT16S BlinkBoxRate;
static GuiConst_INT16S BlinkBoxState;
static GuiConst_INT8U BlinkBoxInverted;
#ifndef GuiConst_BLINK_FIELDS_OFF
static GuiLib_BlinkTextItemRec BlinkTextItems[GuiConst_BLINK_FIELDS_MAX];
#endif
#endif
static GuiConst_INT16S InvertBoxX1, InvertBoxY1, InvertBoxX2, InvertBoxY2;
static GuiConst_INT8U InvertBoxOn;
static GuiConst_INT8U CommonByte0;
static GuiConst_INT8U CommonByte1;
static GuiConst_INT8U CommonByte2;
static GuiConst_INT8U CommonByte3;
static GuiConst_INT8U CommonByte4;
static GuiConst_INT8U CommonByte5;
static GuiConst_INT8U CommonByte6;
static GuiConst_INT32U ItemTypeBit;
static GuiConst_INT16S ItemX1, ItemY1;
static GuiConst_INT16S ItemX2, ItemY2;
static GuiConst_INT8U X1Mode, Y1Mode;
static GuiConst_INT8U X2Mode, Y2Mode;
static GuiConst_INT8U X1MemoryRead, Y1MemoryRead;
static GuiConst_INT8U X1MemoryWrite, Y1MemoryWrite;
static GuiConst_INT8U X2MemoryRead, Y2MemoryRead;
static GuiConst_INT8U X2MemoryWrite, Y2MemoryWrite;
static GuiConst_INT8U X1VarType, Y1VarType, X2VarType, Y2VarType;
static GuiConst_INT16S BbX1, BbX2;
static GuiConst_INT16S BbY1, BbY2;
#ifdef GuiConst_SCROLL_SUPPORT_ON
static GuiConst_INT8S MarkedAs;
static GuiConst_INT8U ScrLineDy;
#endif
static GuiConst_INT8U *ItemDataPtr;
static GuiConst_INT16U ItemDataBufCnt;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
static GuiConst_INT16S DisplayActiveAreaX1, DisplayActiveAreaY1;
static GuiConst_INT16S DisplayActiveAreaX2, DisplayActiveAreaY2;
#endif
static GuiConst_INT16S DisplayOrigoX, DisplayOrigoY;
static GuiConst_INT16S CoordOrigoX, CoordOrigoY;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
static GuiConst_INT16S ClippingX1, ClippingY1, ClippingX2, ClippingY2;
static GuiConst_INT8U ClippingTotal;
static GuiConst_INT16S ActiveAreaX1, ActiveAreaY1, ActiveAreaX2, ActiveAreaY2;
#endif
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
static GuiConst_INT8U NextScrollLineReading;
static GuiConst_INT8U GlobalScrollBoxIndex;
typedef struct
{
  GuiConst_INT16S X1;
  GuiConst_INT16S Y1;
  GuiConst_INT8U InUse;
  void (*ScrollLineDataFunc) (GuiConst_INT16S LineIndex);
  GuiConst_INT16S ScrollTopLine;
  GuiConst_INT16U LastScrollTopLine;
  GuiConst_INT16S LastMarkerLine;
  GuiConst_INT16U ScrollActiveLine;
  GuiConst_INT16U NumberOfLines;
  GuiLib_ItemRec ScrollBoxItem;
  GuiConst_INT8U ScrollBoxType;
  GuiConst_INT16U MakeUpStructIndex;
  GuiConst_INT16U ScrollVisibleLines;
  GuiConst_INT16S LineVerticalOffset;
  GuiConst_INT16S LineOffsetX;
  GuiConst_INT16S LineOffsetY;
  GuiConst_INT16S LineSizeX;
  GuiConst_INT16S LineSizeY;
  GuiConst_INT16S LineSizeY2;
  GuiConst_INT16U LineStructIndex;
  GuiConst_INT16S LineStructOffsetX;
  GuiConst_INT16S LineStructOffsetY;
  GuiConst_INT8U LineColorMode;
  GuiConst_INTCOLOR LineColor;
  GuiConst_INTCOLOR BackColor;
  GuiConst_INT8U LineColorTransparent;
  GuiConst_INT8U WrapMode;
  GuiConst_INT8U ScrollStartOfs;
  GuiConst_INT8U ScrollMode;
  GuiConst_INT8U LineMarkerCount;
  GuiConst_INT8U MarkerColorMode[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INTCOLOR MarkerColor[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT8U MarkerColorTransparent[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT16U MarkerStructIndex[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT8U MarkerDrawingOrder[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT16S MarkerStartLine[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT16S MarkerSize[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT8U BarType;
#ifndef GuiConst_SCROLLITEM_BAR_NONE
  GuiConst_INT8U BarMode;
  GuiConst_INT16S BarPositionX;
  GuiConst_INT16S BarPositionY;
  GuiConst_INT16S BarSizeX;
  GuiConst_INT16S BarSizeY;
  GuiConst_INT16U BarStructIndex;
  GuiConst_INT8U BarColorMode;
  GuiConst_INT8U BarTransparent;
  GuiConst_INTCOLOR BarForeColor;
  GuiConst_INTCOLOR BarBackColor;
  GuiConst_INT8U BarThickness;
  GuiConst_INT16S BarMarkerLeftOffset;
  GuiConst_INT16S BarMarkerRightOffset;
  GuiConst_INT16S BarMarkerTopOffset;
  GuiConst_INT16S BarMarkerBottomOffset;
  GuiConst_INT16U BarMarkerIconCharacter;
  GuiConst_INT8U BarMarkerIconFont;
  GuiConst_INT16U BarMarkerIconOffsetX;
  GuiConst_INT16U BarMarkerIconOffsetY;
  GuiConst_INT8U BarMarkerColorMode;
  GuiConst_INT8U BarMarkerTransparent;
  GuiConst_INTCOLOR BarMarkerForeColor;
  GuiConst_INTCOLOR BarMarkerBackColor;
  GuiConst_INT16U BarMarkerBitmapIndex;
  GuiConst_INT16U BarMarkerBitmapHeight;
#ifdef GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INT8U BarMarkerBitmapIsTransparent;
  GuiConst_INTCOLOR BarMarkerBitmapTranspColor;
#endif
  GuiConst_TEXT *BarIconPtr;
#endif
  GuiConst_INT8U IndicatorType;
#ifndef GuiConst_SCROLLITEM_INDICATOR_NONE
  GuiConst_INT8U IndicatorMode;
  GuiConst_INT16S IndicatorPositionX;
  GuiConst_INT16S IndicatorPositionY;
  GuiConst_INT16S IndicatorSizeX;
  GuiConst_INT16S IndicatorSizeY;
  GuiConst_INT16U IndicatorStructIndex;
  GuiConst_INT8U IndicatorColorMode;
  GuiConst_INT8U IndicatorTransparent;
  GuiConst_INTCOLOR IndicatorForeColor;
  GuiConst_INTCOLOR IndicatorBackColor;
  GuiConst_INT8U IndicatorThickness;
  GuiConst_INT16S IndicatorMarkerLeftOffset;
  GuiConst_INT16S IndicatorMarkerRightOffset;
  GuiConst_INT16S IndicatorMarkerTopOffset;
  GuiConst_INT16S IndicatorMarkerBottomOffset;
  GuiConst_INT16U IndicatorMarkerIconCharacter;
  GuiConst_INT16U IndicatorMarkerIconFont;
  GuiConst_INT16U IndicatorMarkerIconOffsetX;
  GuiConst_INT16U IndicatorMarkerIconOffsetY;
  GuiConst_INT8U IndicatorMarkerColorMode;
  GuiConst_INT8U IndicatorMarkerTransparent;
  GuiConst_INTCOLOR IndicatorMarkerForeColor;
  GuiConst_INTCOLOR IndicatorMarkerBackColor;
  GuiConst_INT16U IndicatorMarkerBitmapIndex;
#ifdef GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INT8U IndicatorMarkerBitmapIsTransparent;
  GuiConst_INTCOLOR IndicatorMarkerBitmapTranspColor;
#endif
  GuiConst_INT16S IndicatorLine;
  GuiConst_TEXT *IndicatorIconPtr;
#endif
#ifdef GuiConst_CURSOR_SUPPORT_ON
  GuiConst_INT8U ContainsCursorFields;
#endif
} ScrollBoxRec;
static ScrollBoxRec ScrollBoxesAry[GuiConst_SCROLLITEM_BOXES_MAX];
#endif
#ifdef GuiConst_ITEM_GRAPH_INUSE
typedef GuiLib_GraphDataPoint GraphDataAry[1000];
typedef GraphDataAry *GraphDataPtr;
typedef struct
{
  GuiConst_INT8U Visible;
  GuiConst_INT16S Offset;
  GuiConst_INT8U Line;
  GuiConst_INT16S LineNegative;
  GuiConst_INT8U Arrow;
  GuiConst_INT16S ArrowLength;
  GuiConst_INT16S ArrowWidth;
  GuiConst_INT8U TicksMajor;
  GuiConst_INT16S TicksMajorLength;
  GuiConst_INT16S TicksMajorWidth;
  GuiConst_INT8U TicksMinor;
  GuiConst_INT16S TicksMinorLength;
  GuiConst_INT16S TicksMinorWidth;
  GuiConst_INT8U Numbers;
  GuiConst_INT32S NumbersMinValue;
  GuiConst_INT32S NumbersMaxValue;
  GuiConst_INT32S NumbersStepMajor;
  GuiConst_INT32S NumbersStepMinor;
  GuiConst_INT8U NumbersAtOrigo;
  GuiConst_INT16S NumbersAtEnd;
  GuiConst_INT16S NumbersOffset;
  GuiConst_INT32S Scale;
} GraphAxisRec;
typedef struct
{
  GuiConst_INT8U Visible;
  GuiConst_INT8U Representation;
  GuiConst_INT16S Width;
  GuiConst_INT16S Height;
  GuiConst_INT16S Thickness;
  GuiConst_INT8U ColorMode;
  GuiConst_INTCOLOR ForeColor, BackColor;
  GuiConst_INT8U BackColorTransparent;
  GraphDataPtr DataPtr;
  GuiConst_INT16U DataSize;
  GuiConst_INT16U DataFirst;
  GuiConst_INT16U DataCount;
  GuiConst_INT8U AxisIndexX, AxisIndexY;
} GraphDataSetRec;
typedef struct
{
  GuiLib_ItemRec GraphItem;
  GuiConst_INT16S OrigoX, OrigoY;
  GuiConst_INT8U InUse;
  GuiConst_INT16S OriginOffsetX, OriginOffsetY;
  GuiConst_INTCOLOR ForeColor, BackColor;
  GuiConst_INT8U GraphAxesCnt[2];
  GraphAxisRec GraphAxes[GuiConst_GRAPH_AXES_MAX][2];
  GuiConst_INT8U GraphDataSetCnt;
  GraphDataSetRec GraphDataSets[GuiConst_GRAPH_DATASETS_MAX];
} GraphItemRec;
static GuiConst_INT16U GlobalGraphIndex;
static GraphItemRec GraphAry[GuiConst_GRAPH_MAX];
#endif
#ifdef GuiConst_BITMAP_SUPPORT_ON
typedef struct
{
  GuiConst_INT8U InUse;
  GuiConst_INT16S Index;
  GuiConst_INT16S X;
  GuiConst_INT16S Y;
} BackgrBitmapRec;
static BackgrBitmapRec BackgrBitmapAry[GuiConst_MAX_BACKGROUND_BITMAPS];
static GuiConst_INT16U GlobalBackgrBitmapIndex;
static GuiConst_INT16S BitmapWriteX2, BitmapWriteY2;
#endif

//==============================================================================
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
  #define GuiLib_COORD_ADJUST(X, Y)                                   \
  {                                                                   \
    if (GuiLib_DisplayUpsideDown)                                     \
    {                                                                 \
      X = GuiConst_DISPLAY_WIDTH_HW - 1 - CoordOrigoX - X;            \
      Y = GuiConst_DISPLAY_HEIGHT_HW - 1 - CoordOrigoY - Y;           \
    }                                                                 \
    else                                                              \
    {                                                                 \
      X = CoordOrigoX + X;                                            \
      Y = CoordOrigoY + Y;                                            \
    }                                                                 \
  }

  #define GuiLib_MIRROR_BITS(B)                                       \
  {                                                                   \
    B = (((B & 0x80) >> 7) | ((B & 0x40) >> 5) |                      \
         ((B & 0x20) >> 3) | ((B & 0x10) >> 1) |                      \
         ((B & 0x08) << 1) | ((B & 0x04) << 3) |                      \
         ((B & 0x02) << 5) | ((B & 0x01) << 7));                      \
  }
#else
  #ifdef GuiConst_ROTATED_OFF
    #ifdef GuiConst_MIRRORED_HORIZONTALLY
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = GuiConst_DISPLAY_WIDTH_HW - 1 - CoordOrigoX - X;          \
          Y = GuiConst_DISPLAY_HEIGHT_HW - 1 - CoordOrigoY - Y;         \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = GuiConst_DISPLAY_WIDTH_HW - 1 - CoordOrigoX - X;          \
          Y = CoordOrigoY + Y;                                          \
        }
      #endif
    #else
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = CoordOrigoX + X;                                          \
          Y = GuiConst_DISPLAY_HEIGHT_HW - 1 - CoordOrigoY - Y;         \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = CoordOrigoX + X;                                          \
          Y = CoordOrigoY + Y;                                          \
        }
      #endif
    #endif
  #endif
  #ifdef GuiConst_ROTATED_90DEGREE_RIGHT
    #ifdef GuiConst_MIRRORED_HORIZONTALLY
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = CoordOrigoX + X;                                          \
          Y = GuiConst_DISPLAY_WIDTH_HW - 1 - CoordOrigoY - Y;          \
          SwapCoord(&X, &Y);                                            \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = CoordOrigoX + X;                                          \
          Y = CoordOrigoY + Y;                                          \
          SwapCoord(&X, &Y);                                            \
        }
      #endif
    #else
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = GuiConst_DISPLAY_HEIGHT_HW - 1 - CoordOrigoX - X;         \
          Y = GuiConst_DISPLAY_WIDTH_HW - 1 - CoordOrigoY - Y;          \
          SwapCoord(&X, &Y);                                            \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = GuiConst_DISPLAY_HEIGHT_HW - 1 - CoordOrigoX - X;         \
          Y = CoordOrigoY + Y;                                          \
          SwapCoord(&X, &Y);                                            \
        }
      #endif
    #endif
  #endif
  #ifdef GuiConst_ROTATED_UPSIDEDOWN
    #ifdef GuiConst_MIRRORED_HORIZONTALLY
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = CoordOrigoX + X;                                          \
          Y = CoordOrigoY + Y;                                          \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = CoordOrigoX + X;                                          \
          Y = GuiConst_DISPLAY_HEIGHT_HW - 1 - CoordOrigoY - Y;         \
        }
      #endif
    #else
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = GuiConst_DISPLAY_WIDTH_HW - 1 - CoordOrigoX - X;          \
          Y = CoordOrigoY + Y;                                          \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = GuiConst_DISPLAY_WIDTH_HW - 1 - CoordOrigoX - X;          \
          Y = GuiConst_DISPLAY_HEIGHT_HW - 1 - CoordOrigoY - Y;         \
        }
      #endif
    #endif
  #endif
  #ifdef GuiConst_ROTATED_90DEGREE_LEFT
    #ifdef GuiConst_MIRRORED_HORIZONTALLY
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = GuiConst_DISPLAY_HEIGHT_HW - 1 - CoordOrigoX - X;         \
          Y = CoordOrigoY + Y;                                          \
          SwapCoord(&X, &Y);                                            \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = GuiConst_DISPLAY_HEIGHT_HW - 1 - CoordOrigoX - X;         \
          Y = GuiConst_DISPLAY_WIDTH_HW - 1 - CoordOrigoY - Y;          \
          SwapCoord(&X, &Y);                                            \
        }
      #endif
    #else
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = CoordOrigoX + X;                                          \
          Y = CoordOrigoY + Y;                                          \
          SwapCoord(&X, &Y);                                            \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                               \
        {                                                               \
          X = CoordOrigoX + X;                                          \
          Y = GuiConst_DISPLAY_WIDTH_HW - 1 - CoordOrigoY - Y;          \
          SwapCoord(&X, &Y);                                            \
        }
      #endif
    #endif
  #endif
#endif

#ifdef GuiConst_COLOR_DEPTH_1
#define GuiLib_COLOR_ADJUST(C) \
{                              \
  C &= 0x01;                   \
}
#endif
#ifdef GuiConst_COLOR_DEPTH_2
#define GuiLib_COLOR_ADJUST(C) \
{                              \
  C &= 0x03;                   \
}
#endif
#ifdef GuiConst_COLOR_DEPTH_4
#define GuiLib_COLOR_ADJUST(C) \
{                              \
  C &= 0x0F;                   \
}
#endif
#ifdef GuiConst_COLOR_DEPTH_5
#define GuiLib_COLOR_ADJUST(C) \
{                              \
  C = (C & 0x1F) << 3;         \
}
#endif
#ifdef GuiConst_COLOR_DEPTH_8
#define GuiLib_COLOR_ADJUST(C) \
{                              \
}
#endif
#ifdef GuiConst_COLOR_DEPTH_12
#define GuiLib_COLOR_ADJUST(C) \
{                              \
}
#endif
#ifdef GuiConst_COLOR_DEPTH_15
#define GuiLib_COLOR_ADJUST(C) \
{                              \
}
#endif
#ifdef GuiConst_COLOR_DEPTH_16
#define GuiLib_COLOR_ADJUST(C) \
{                              \
}
#endif
#ifdef GuiConst_COLOR_DEPTH_18
#define GuiLib_COLOR_ADJUST(C) \
{                              \
}
#endif
#ifdef GuiConst_COLOR_DEPTH_24
#define GuiLib_COLOR_ADJUST(C) \
{                              \
}
#endif

#define GuiLib_GET_MIN(A, B) ((A) > (B) ? (B) : (A))

#define GuiLib_GET_MAX(A, B) ((A) > (B) ? (A) : (B))

#define GuiLib_GET_MINMAX(X, A, B) ((X) > (A) ? (GuiLib_GET_MIN(X,B)) : (A))

#define GuiLib_LIMIT_MIN(X, A) \
{                              \
  if (X < A)                   \
    X = A;                     \
}

#define GuiLib_LIMIT_MAX(X, B) \
{                              \
  if (X > B)                   \
    X = B;                     \
}

#define GuiLib_LIMIT_MINMAX(X, A, B) \
{                                    \
  if (X < A)                         \
    X = A;                           \
  else if ((B < A) && (X > A))       \
    X = A;                           \
  else if ((B >= A) && (X > B))      \
    X = B;                           \
}

//==============================================================================

//------------------------------------------------------------------------------
static void SwapCoord(
   GuiConst_INT16S * X1,
   GuiConst_INT16S * X2)
{
  GuiConst_INT16S Tmp;

  Tmp = *X1;
  *X1 = *X2;
  *X2 = Tmp;
}

//------------------------------------------------------------------------------
static GuiConst_INT8U OrderCoord(
   GuiConst_INT16S * X1,
   GuiConst_INT16S * X2)
{
  if (*X1 > *X2)
  {
    SwapCoord (X1, X2);
    return (1);
  }
  else
    return (0);
}

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
static GuiConst_INT8U CheckRect(
   GuiConst_INT16S * X1,
   GuiConst_INT16S * Y1,
   GuiConst_INT16S * X2,
   GuiConst_INT16S * Y2)
{
  if (ClippingTotal ||
     (*X1 > ClippingX2) || (*X2 < ClippingX1) ||
     (*Y1 > ClippingY2) || (*Y2 < ClippingY1))
    return (0);
  else
  {
    if (*X1 < ClippingX1)
      *X1 = ClippingX1;
    if (*X2 > ClippingX2)
      *X2 = ClippingX2;
    if (*Y1 < ClippingY1)
      *Y1 = ClippingY1;
    if (*Y2 > ClippingY2)
      *Y2 = ClippingY2;
    return (1);
  }
}
#endif

//==============================================================================

#ifdef GuiConst_COLOR_DEPTH_1
  #ifdef GuiConst_BYTE_HORIZONTAL
    #include "GuiGraph1H.c"
  #else
    #include "GuiGraph1V.c"
  #endif
#endif
#ifdef GuiConst_COLOR_DEPTH_2
  #ifdef GuiConst_BYTE_HORIZONTAL
    #ifdef GuiConst_COLOR_PLANES_2
      #include "GuiGraph2H2P.c"
    #else
      #include "GuiGraph2H.c"
    #endif
  #else
    #ifdef GuiConst_COLOR_PLANES_2
      #include "GuiGraph2V2P.c"
    #else
      #include "GuiGraph2V.c"
    #endif
  #endif
#endif
#ifdef GuiConst_COLOR_DEPTH_4
  #ifdef GuiConst_BYTE_HORIZONTAL
    #include "GuiGraph4H.c"
  #else
    #include "GuiGraph4V.c"
  #endif
#endif
#ifdef GuiConst_COLOR_DEPTH_5
  #include "GuiGraph5.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_8
  #include "GuiGraph8.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_12
  #include "GuiGraph16.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_15
  #include "GuiGraph16.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_16
  #include "GuiGraph16.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_18
  #include "GuiGraph24.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_24
  #include "GuiGraph24.c"
#endif

#include "GuiGraph.c"
#ifdef GuiConst_ADV_GRAPHICS_ON
//#include "GuiGraphAdv.c"
#endif

//==============================================================================
static void DrawStructure(
   GuiLib_StructPtr Structure,
   GuiConst_INT8U ColorInvert) PrefixReentrant;
#ifdef GuiConst_CURSOR_SUPPORT_ON
static void DrawCursorItem(
   GuiConst_INT8U CursorVisible);
#endif
#ifdef GuiConst_CHARMODE_ANSI
static void SetLanguageCharSet(
   GuiConst_INT8U LangCharSet);
#endif
#ifdef GuiConst_SCROLL_SUPPORT_ON
static void DrawScrollIndicator(void);
static void DrawScrollList(void);
#endif
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
static void ScrollBox_DrawScrollLine(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16S LineNdx);
#endif

//==============================================================================

//------------------------------------------------------------------------------
void GuiLib_Init(void)
{
  RefreshClock = 0;

#ifdef GuiConst_DISPLAY_ACTIVE_AREA
#ifdef GuiConst_DISPLAY_ACTIVE_AREA_COO_REL
  DisplayOrigoX = GuiConst_DISPLAY_ACTIVE_AREA_X1;
  DisplayOrigoY = GuiConst_DISPLAY_ACTIVE_AREA_Y1;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  DisplayActiveAreaX1 = 0;
  DisplayActiveAreaY1 = 0;
#endif
#ifdef GuiConst_DISPLAY_ACTIVE_AREA_CLIPPING
  DisplayActiveAreaX2 =
     GuiConst_DISPLAY_ACTIVE_AREA_X2 - GuiConst_DISPLAY_ACTIVE_AREA_X1;
  DisplayActiveAreaY2 =
     GuiConst_DISPLAY_ACTIVE_AREA_Y2 - GuiConst_DISPLAY_ACTIVE_AREA_Y1;
#else
  DisplayActiveAreaX2 =
     GuiConst_DISPLAY_WIDTH - GuiConst_DISPLAY_ACTIVE_AREA_X1 - 1;
  DisplayActiveAreaY2 =
     GuiConst_DISPLAY_HEIGHT - GuiConst_DISPLAY_ACTIVE_AREA_Y1 - 1;
#endif
#else
  DisplayOrigoX = 0;
  DisplayOrigoY = 0;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
#ifdef GuiConst_DISPLAY_ACTIVE_AREA_CLIPPING
  DisplayActiveAreaX1 = GuiConst_DISPLAY_ACTIVE_AREA_X1;
  DisplayActiveAreaY1 = GuiConst_DISPLAY_ACTIVE_AREA_Y1;
  DisplayActiveAreaX2 = GuiConst_DISPLAY_ACTIVE_AREA_X2;
  DisplayActiveAreaY2 = GuiConst_DISPLAY_ACTIVE_AREA_Y2;
#else
  DisplayActiveAreaX1 = 0;
  DisplayActiveAreaY1 = 0;
  DisplayActiveAreaX2 = GuiConst_DISPLAY_WIDTH - 1;
  DisplayActiveAreaY2 = GuiConst_DISPLAY_HEIGHT - 1;
#endif
#endif
#endif
#else
  DisplayOrigoX = 0;
  DisplayOrigoY = 0;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  DisplayActiveAreaX1 = 0;
  DisplayActiveAreaY1 = 0;
  DisplayActiveAreaX2 = GuiConst_DISPLAY_WIDTH - 1;
  DisplayActiveAreaY2 = GuiConst_DISPLAY_HEIGHT - 1;
#endif
#endif
  CoordOrigoX = DisplayOrigoX;
  CoordOrigoY = DisplayOrigoY;

#ifdef GuiConst_ADV_GRAPHICS_ON
//  GuiLib_AG_Init();
#endif

  GuiDisplay_Init();
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiLib_ResetClipping();
#endif
  GuiLib_ResetDisplayRepaint();
  GuiLib_Clear();

  DisplayWriting = 1;
  InitialDrawing = 0;
  TopLevelStructure = 0;
  SwapColors = 0;
  GuiLib_SetLanguage(0);
#ifdef GuiConst_SCROLL_SUPPORT_ON
  ScrollLineDataFunc = 0;
#endif
#ifdef GuiConst_BLINK_SUPPORT_ON
  BlinkBoxRate = 0;
#endif
  InvertBoxOn = 0;
  DrawingLevel = 0;
  GuiLib_CurStructureNdx = -1;
#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
  GuiLib_TouchAdjustReset();
#endif
#ifdef GuiConst_REMOTE_DATA
  GuiLib_RemoteDataReadBlock = 0;
  #ifdef GuiConst_REMOTE_TEXT_DATA
  GuiLib_RemoteTextReadBlock = 0;
  CurRemoteText = -1;
  RemoteTextTableOfs = -1;
  #endif
  #ifdef GuiConst_REMOTE_FONT_DATA
  CurRemoteFont = -1;
  #endif
  #ifdef GuiConst_REMOTE_STRUCT_DATA
  CurRemoteStruct = -1;
  #endif
  #ifdef GuiConst_REMOTE_BITMAP_DATA
  CurRemoteBitmap = -1;
  #endif
#endif
}

//------------------------------------------------------------------------------
void GuiLib_Clear(void)
{
  GuiConst_INT16S N;

  GuiLib_ClearDisplay();

  GuiLib_CurStructureNdx = -1;

  for (N = 0; N < GuiConst_AUTOREDRAW_FIELDS_MAX; N++)
    AutoRedrawItems[N].BitFlags = 0;

#ifdef GuiConst_CURSOR_SUPPORT_ON
  CursorInUse = 0;
  GuiLib_ActiveCursorFieldNo = 0;
#ifndef GuiConst_CURSOR_FIELDS_OFF
  for (N = 0; N < GuiConst_CURSOR_FIELDS_MAX; N++)
    CursorItems[N].BitFlags = 0;
#endif
#endif
#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
  for (N = 0; N < GuiConst_BLINK_FIELDS_MAX; N++)
  {
    BlinkTextItems[N].InUse = 0;
    BlinkTextItems[N].Active = 0;
  }
#endif
#endif
#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
  for (N = 0; N < GuiConst_TOUCHAREA_MAX; N++)
    TouchAreas[N].InUse = 0;
#endif

#ifdef GuiConst_TEXTBOX_FIELDS_ON
  for (N = 0; N < GuiConst_TEXTBOX_FIELDS_MAX; N++)
    TextboxScrollItems[N].TextBoxScrollIndex = 0xFF;
#endif

  CoordOrigoX = DisplayOrigoX;
  CoordOrigoY = DisplayOrigoY;

#ifdef GuiConst_SCROLL_SUPPORT_ON
  ScrollBoxDetected = 0;
  ScrollBarDetected = 0;
  ScrollLineDetected = 0;
#endif

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  NextScrollLineReading = 0;
  GlobalScrollBoxIndex = 0;
  for (N = 0; N < GuiConst_SCROLLITEM_BOXES_MAX; N++)
  {
    ScrollBoxesAry[N].X1 = 0;
    ScrollBoxesAry[N].Y1 = 0;
    ScrollBoxesAry[N].InUse = GuiLib_SCROLL_STRUCTURE_UNDEF;
    ScrollBoxesAry[N].ScrollTopLine = 0;
    ScrollBoxesAry[N].LastScrollTopLine = 0;
    ScrollBoxesAry[N].LastMarkerLine = 0;
    ScrollBoxesAry[N].ScrollActiveLine = 0;
    ScrollBoxesAry[N].NumberOfLines = 0;
  }
#endif
#ifdef GuiConst_ITEM_GRAPH_INUSE
  GlobalGraphIndex = 0;
  for (N = 0; N < GuiConst_GRAPH_MAX; N++)
  {
    GraphAry[N].InUse = GuiLib_GRAPH_STRUCTURE_UNDEF;
    GraphAry[N].GraphAxesCnt[GuiLib_GRAPHAXIS_X] = 0;
    GraphAry[N].GraphAxesCnt[GuiLib_GRAPHAXIS_Y] = 0;
    GraphAry[N].GraphDataSetCnt = 0;
  }
#endif
#ifdef GuiConst_BITMAP_SUPPORT_ON
  GlobalBackgrBitmapIndex = 0;
  for (N = 0; N < GuiConst_MAX_BACKGROUND_BITMAPS; N++)
  {
    BackgrBitmapAry[N].InUse = 0;
    BackgrBitmapAry[N].Index = 0;
    BackgrBitmapAry[N].X = 0;
    BackgrBitmapAry[N].Y = 0;
  }
#endif
}

#ifdef GuiConst_CHARMODE_UNICODE
//------------------------------------------------------------------------------
GuiConst_INT16U GuiLib_UnicodeStrLen(
   GuiConst_TEXT *S)
{
  GuiConst_INT16U StrLen;

  StrLen = 0;
  while (*S != 0)
  {
    StrLen++;
    S++;
  }
  return (StrLen);
}

//------------------------------------------------------------------------------
void GuiLib_StrAnsiToUnicode(
   GuiConst_TEXT *S2,
   GuiConst_CHAR *S1)
{
  do
  {
    *S2 = (GuiConst_TEXT)(*S1);
    *S2 &= 0x00ff;
    if (*S1 == 0)
      return;
    S1++;
    S2++;
  }
  while (1);
}

//------------------------------------------------------------------------------
GuiConst_INT16S GuiLib_UnicodeStrCmp(
   GuiConst_TEXT *S1,
   GuiConst_TEXT *S2)
{
  do
  {
    if ((*S1 == 0) && (*S2 == 0))
      return (0);

    else if (*S1 == 0)
      return (-1);
    else if (*S2 == 0)
      return (1);
    else if (*S1 < *S2)
      return (-1);
    else if (*S1 > *S2)
      return (1);
    S1++;
    S2++;
  }
  while (1);
}

//------------------------------------------------------------------------------
GuiConst_INT16S GuiLib_UnicodeStrNCmp(
   GuiConst_TEXT *S1,
   GuiConst_TEXT *S2,
   GuiConst_INT16U StrLen)
{
  while (StrLen > 0)
  {
    if ((*S1 == 0) && (*S2 == 0))
      return (0);
    else if (*S1 == 0)
      return (-1);
    else if (*S2 == 0)
      return (1);
    else if (*S1 < *S2)
      return (-1);
    else if (*S1 > *S2)
      return (1);
    S1++;
    S2++;
    StrLen--;
  }
  return (0);
}

//------------------------------------------------------------------------------
void GuiLib_UnicodeStrCpy(
   GuiConst_TEXT *S2,
   GuiConst_TEXT *S1)
{
  do
  {
    *S2 = *S1;
    if (*S1 == 0)
      return;
    S1++;
    S2++;
  }
  while (1);
}

//------------------------------------------------------------------------------
void GuiLib_UnicodeStrNCpy(
   GuiConst_TEXT *S2,
   GuiConst_TEXT *S1,
   GuiConst_INT16U StrLen)
{
  while (StrLen > 0)
  {
    *S2 = *S1;
    if (*S1 != 0)
      S1++;
    S2++;
    StrLen--;
  }
}
#endif

//------------------------------------------------------------------------------
static void ConvertIntToStr(
   GuiConst_INT32U num,
   GuiConst_CHAR *string,
   GuiConst_INT32U base)
{
  GuiConst_INT8U digits[16] =
     { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70 };
  GuiConst_CHAR buffer[11];
  GuiConst_INT8U bufferLen;
  GuiConst_INT8U i;

  bufferLen = 0;
  do
  {
    i = num % base;
    buffer[bufferLen++] = digits[i];
    num /= base;
  }
  while (num != 0);

  if (bufferLen <= GuiConst_MAX_VARNUM_TEXT_LEN)
    while (bufferLen-- > 0)
      *string++ = buffer[bufferLen];

  *string = '\0';
}

//------------------------------------------------------------------------------
static GuiConst_INT16S CharDist(
   GuiConst_INT16U ChPos1,
   GuiConst_INT16U ChPos2,
   GuiConst_INT8U Ps)
{
  GuiConst_INT16S Result, D;
  GuiConst_INT8U PrefixRom *Ps1;
  GuiConst_INT8U PrefixRom *Ps2;
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U CharHeader1[GuiLib_CHR_LINECTRL_OFS];
  GuiConst_INT8U CharHeader2[GuiLib_CHR_LINECTRL_OFS];
#endif

  if (Ps == GuiLib_PS_OFF)
#ifdef GuiConst_AVRGCC_COMPILER
    return (pgm_read_byte(&CurFont->XSize));
#else
    return (CurFont->XSize);
#endif
  else if ((Ps == GuiLib_PS_ON) || (Ps == GuiLib_PS_NUM))
  {
#ifdef GuiConst_REMOTE_FONT_DATA
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[TextCharNdx[ChPos1]],
       GuiLib_CHR_LINECTRL_OFS, CharHeader1);
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[TextCharNdx[ChPos2]],
       GuiLib_CHR_LINECTRL_OFS, CharHeader2);
#endif
    if ((Ps == GuiLib_PS_ON) || (TextPsMode[ChPos1] && TextPsMode[ChPos2]))
    {
#ifdef GuiConst_REMOTE_FONT_DATA
      Result = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_TOP_OFS] -
          CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_TOP_OFS];
#else
      Ps1 = TextCharPtrAry[ChPos1] + GuiLib_CHR_PSRIGHT_OFS;
      Ps2 = TextCharPtrAry[ChPos2] + GuiLib_CHR_PSLEFT_OFS;
#ifdef GuiConst_AVRGCC_COMPILER
      Result = (GuiConst_INT16S)(pgm_read_byte(Ps1 + GuiLib_CHR_PS_TOP_OFS)) -
         (GuiConst_INT16S)(pgm_read_byte(Ps2 + GuiLib_CHR_PS_TOP_OFS));
#else
      Result = (GuiConst_INT16S)(*(Ps1 + GuiLib_CHR_PS_TOP_OFS)) -
         (GuiConst_INT16S)(*(Ps2 + GuiLib_CHR_PS_TOP_OFS));
#endif
#endif
#ifdef GuiConst_REMOTE_FONT_DATA
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_MID_OFS] -
          CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_MID_OFS];
#else
#ifdef GuiConst_AVRGCC_COMPILER
      D = (GuiConst_INT16S)(pgm_read_byte(Ps1 + GuiLib_CHR_PS_MID_OFS)) -
          (GuiConst_INT16S)(pgm_read_byte(Ps2 + GuiLib_CHR_PS_MID_OFS));
#else
      D = (GuiConst_INT16S)(*(Ps1 + GuiLib_CHR_PS_MID_OFS)) -
          (GuiConst_INT16S)(*(Ps2 + GuiLib_CHR_PS_MID_OFS));
#endif
#endif
      if (D > Result)
        Result = D;
#ifdef GuiConst_REMOTE_FONT_DATA
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_MIDBASE_OFS] -
          CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_MIDBASE_OFS];
#else
#ifdef GuiConst_AVRGCC_COMPILER
      D = (GuiConst_INT16S)(pgm_read_byte(Ps1 + GuiLib_CHR_PS_MIDBASE_OFS)) -
          (GuiConst_INT16S)(pgm_read_byte(Ps2 + GuiLib_CHR_PS_MIDBASE_OFS));
#else
      D = (GuiConst_INT16S)(*(Ps1 + GuiLib_CHR_PS_MIDBASE_OFS)) -
          (GuiConst_INT16S)(*(Ps2 + GuiLib_CHR_PS_MIDBASE_OFS));
#endif
#endif
      if (D > Result)
        Result = D;
#ifdef GuiConst_REMOTE_FONT_DATA
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_BASE_OFS] -
          CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_BASE_OFS];
#else
#ifdef GuiConst_AVRGCC_COMPILER
      D = (GuiConst_INT16S)(pgm_read_byte(Ps1 + GuiLib_CHR_PS_BASE_OFS)) -
          (GuiConst_INT16S)(pgm_read_byte(Ps2 + GuiLib_CHR_PS_BASE_OFS));
#else
      D = (GuiConst_INT16S)(*(Ps1 + GuiLib_CHR_PS_BASE_OFS)) -
          (GuiConst_INT16S)(*(Ps2 + GuiLib_CHR_PS_BASE_OFS));
#endif
#endif
      if (D > Result)
        Result = D;
#ifdef GuiConst_REMOTE_FONT_DATA
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_BOTTOM_OFS] -
          CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_BOTTOM_OFS];
#else
#ifdef GuiConst_AVRGCC_COMPILER
      D = (GuiConst_INT16S)(pgm_read_byte(Ps1 + GuiLib_CHR_PS_BOTTOM_OFS)) -
          (GuiConst_INT16S)(pgm_read_byte(Ps2 + GuiLib_CHR_PS_BOTTOM_OFS));
#else
      D = (GuiConst_INT16S)(*(Ps1 + GuiLib_CHR_PS_BOTTOM_OFS)) -
          (GuiConst_INT16S)(*(Ps2 + GuiLib_CHR_PS_BOTTOM_OFS));
#endif
#endif
      if (D > Result)
        Result = D;
#ifdef GuiConst_AVRGCC_COMPILER
      Result += pgm_read_byte(&CurFont->PsSpace) + 1;
#else
      Result += (GuiConst_INT16S)CurFont->PsSpace + 1;
#endif
      return (Result);
    }
    else if (TextPsMode[ChPos1])
#ifdef GuiConst_REMOTE_FONT_DATA
      return (CharHeader1[GuiLib_CHR_XLEFT_OFS] +
          CharHeader1[GuiLib_CHR_XWIDTH_OFS] + (GuiConst_INT16S)CurFont->PsSpace);
#else
#ifdef GuiConst_AVRGCC_COMPILER
      return (pgm_read_byte(TextCharPtrAry[ChPos1] + GuiLib_CHR_XLEFT_OFS) +
         pgm_read_byte(TextCharPtrAry[ChPos1] + GuiLib_CHR_XWIDTH_OFS) +
         pgm_read_byte(&CurFont->PsSpace));
#else
      return (*(TextCharPtrAry[ChPos1] + GuiLib_CHR_XLEFT_OFS) +
         *(TextCharPtrAry[ChPos1] + GuiLib_CHR_XWIDTH_OFS) + (GuiConst_INT16S)CurFont->PsSpace);
#endif
#endif
    else if (TextPsMode[ChPos2])
#ifdef GuiConst_REMOTE_FONT_DATA
      return (CurFont->PsNumWidth -
         CharHeader2[GuiLib_CHR_XLEFT_OFS] + (GuiConst_INT16S)CurFont->PsSpace);
#else
#ifdef GuiConst_AVRGCC_COMPILER
      return (pgm_read_byte(&CurFont->PsNumWidth) -
         pgm_read_byte(TextCharPtrAry[ChPos2] +
         GuiLib_CHR_XLEFT_OFS) + pgm_read_byte(&CurFont->PsSpace));
#else
      return (CurFont->PsNumWidth - *(TextCharPtrAry[ChPos2] +
         GuiLib_CHR_XLEFT_OFS) + (GuiConst_INT16S)CurFont->PsSpace);
#endif
#endif
    else
#ifdef GuiConst_AVRGCC_COMPILER
      return (pgm_read_byte(&CurFont->PsNumWidth) +
         pgm_read_byte(&CurFont->PsSpace));
#else
      return (CurFont->PsNumWidth + (GuiConst_INT16S)CurFont->PsSpace);
#endif
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
static GuiConst_INT16S TextPixelLength(
   GuiConst_INT8U Ps,
   GuiConst_INT16U CharCnt,
   TextXOfsAryPtr TextXOfsPtr)
{
  GuiConst_INT16U P;
  GuiConst_INT16S L;
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U CharHeader1[GuiLib_CHR_LINECTRL_OFS + 1];
  GuiConst_INT8U CharHeader2[GuiLib_CHR_LINECTRL_OFS + 1];
#endif

  if (CharCnt == 0)
    return (0);
  else
  {
#ifdef GuiConst_REMOTE_FONT_DATA
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[TextCharNdx[0]],
       GuiLib_CHR_LINECTRL_OFS + 1, CharHeader1);
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[TextCharNdx[CharCnt - 1]],
       GuiLib_CHR_LINECTRL_OFS + 1, CharHeader2);
#endif

    if (TextPsMode[0])
    {
#ifdef GuiConst_REMOTE_FONT_DATA
      if (CharHeader1[GuiLib_CHR_LINECTRL_OFS] & 0x01)
        L = -(GuiConst_INT16S)CharHeader1[GuiLib_CHR_PSLEFT_OFS];
      else
        L = -(GuiConst_INT16S)CharHeader1[GuiLib_CHR_XLEFT_OFS];
#else
#ifdef GuiConst_AVRGCC_COMPILER
      if (pgm_read_byte(TextCharPtrAry[0] + GuiLib_CHR_LINECTRL_OFS) & 0x01)
        L = -(GuiConst_INT16S)(pgm_read_byte(TextCharPtrAry[0] +
           GuiLib_CHR_PSLEFT_OFS));
      else
        L = -(GuiConst_INT16S)(pgm_read_byte(TextCharPtrAry[0] +
         GuiLib_CHR_XLEFT_OFS));
#else
      if (*(TextCharPtrAry[0] + GuiLib_CHR_LINECTRL_OFS) & 0x01)
        L = -(GuiConst_INT16S)(*(TextCharPtrAry[0] + GuiLib_CHR_PSLEFT_OFS));
      else
        L = -(GuiConst_INT16S)(*(TextCharPtrAry[0] + GuiLib_CHR_XLEFT_OFS));
#endif
#endif
    }
    else
      L = 0;

    if (TextXOfsPtr != 0)
      (*TextXOfsPtr)[0] = L;

    for (P = 0; P < CharCnt - 1; P++)
    {
      L += CharDist (P, P + 1, Ps);
      if (TextXOfsPtr != 0)
        (*TextXOfsPtr)[P + 1] = L;
    }

#ifdef GuiConst_REMOTE_FONT_DATA
    if (TextPsMode[CharCnt - 1])
    {
      if (CharHeader2[GuiLib_CHR_LINECTRL_OFS] & 0x01)
        L += (GuiConst_INT16S)CharHeader2[GuiLib_CHR_PSRIGHT_OFS] + 1;
      else
        L += (GuiConst_INT16S)CharHeader2[GuiLib_CHR_XLEFT_OFS] +
             (GuiConst_INT16S)CharHeader2[GuiLib_CHR_XWIDTH_OFS] + 1;
    }
    else if (Ps == GuiLib_PS_NUM)
    {
      if (CharHeader2[GuiLib_CHR_LINECTRL_OFS] & 0x01)
        L += (GuiConst_INT16S)CharHeader2[GuiLib_CHR_PSRIGHT_OFS] + 1;
      else
        L += CurFont->PsNumWidth + CurFont->PsSpace;
    }
    else
      L += CurFont->XSize;
#else
#ifdef GuiConst_AVRGCC_COMPILER
    if (TextPsMode[CharCnt - 1])
    {
      if (pgm_read_byte(TextCharPtrAry[CharCnt - 1] +
          GuiLib_CHR_LINECTRL_OFS) & 0x01)
        L += (GuiConst_INT16S)(pgm_read_byte(TextCharPtrAry[CharCnt - 1] +
              GuiLib_CHR_PSRIGHT_OFS)) + 1;
      else
        L += pgm_read_byte(TextCharPtrAry[CharCnt - 1] + GuiLib_CHR_XLEFT_OFS) +
          pgm_read_byte(TextCharPtrAry[CharCnt - 1] + GuiLib_CHR_XWIDTH_OFS);
    }
    else if (Ps == GuiLib_PS_NUM)
    {
      if (pgm_read_byte(TextCharPtrAry[CharCnt - 1] +
          GuiLib_CHR_LINECTRL_OFS) & 0x01)
        L += (GuiConst_INT16S)(pgm_read_byte(TextCharPtrAry[CharCnt - 1] +
              GuiLib_CHR_PSRIGHT_OFS)) + 1;
      else
        L += pgm_read_byte(&CurFont->PsNumWidth) +
           pgm_read_byte(&CurFont->PsSpace);
    }
    else
      L += pgm_read_byte(&CurFont->XSize);
#else
    if (TextPsMode[CharCnt - 1])
    {
      if (*(TextCharPtrAry[CharCnt - 1] + GuiLib_CHR_LINECTRL_OFS) & 0x01)
        L += (GuiConst_INT16S)(*(TextCharPtrAry[CharCnt - 1] +
              GuiLib_CHR_PSRIGHT_OFS)) + 1;
      else
        L += (GuiConst_INT16S)(*(TextCharPtrAry[CharCnt - 1] + GuiLib_CHR_XLEFT_OFS) +
          *(TextCharPtrAry[CharCnt - 1] + GuiLib_CHR_XWIDTH_OFS));
    }
    else if (Ps == GuiLib_PS_NUM)
    {
      if (*(TextCharPtrAry[CharCnt - 1] + GuiLib_CHR_LINECTRL_OFS) & 0x01)
        L += (GuiConst_INT16S)(*(TextCharPtrAry[CharCnt - 1] +
              GuiLib_CHR_PSRIGHT_OFS)) + 1;
      else
        L += CurFont->PsNumWidth + (GuiConst_INT16S)CurFont->PsSpace;
    }
    else
      L += CurFont->XSize;
#endif
#endif
    if (TextXOfsPtr != 0)
      (*TextXOfsPtr)[CharCnt] = L;
    return (L);
  }
}

//------------------------------------------------------------------------------
static GuiConst_INT16U CalcCharsWidth(
   GuiConst_INT16U CharPos1,
   GuiConst_INT16U CharPos2,
   TextXOfsAryPtr TextXOfsPtr,
   GuiConst_INT16S *pXStart,
   GuiConst_INT16S *pXEnd)
{
  GuiConst_INT16S X1;
  GuiConst_INT16S X2;

#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U CharHeader1[GuiLib_CHR_LINECTRL_OFS];
  GuiConst_INT8U CharHeader2[GuiLib_CHR_LINECTRL_OFS];

  GuiLib_RemoteDataReadBlock(
     (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[TextCharNdx[CharPos1]],
      GuiLib_CHR_LINECTRL_OFS, CharHeader1);
  GuiLib_RemoteDataReadBlock(
     (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[TextCharNdx[CharPos2]],
      GuiLib_CHR_LINECTRL_OFS, CharHeader2);

  if (TextPsMode[CharPos1])
  {
    if (CharHeader1[GuiLib_CHR_LINECTRL_OFS] & 0x01)
      X1 = (*TextXOfsPtr)[CharPos1] +
           (GuiConst_INT16S)CharHeader1[GuiLib_CHR_PSLEFT_OFS];
    else
      X1 = (*TextXOfsPtr)[CharPos1] +
           (GuiConst_INT16S)CharHeader1[GuiLib_CHR_XLEFT_OFS];
  }
  else
    X1 = (*TextXOfsPtr)[CharPos1];
  if (TextPsMode[CharPos2])
  {
    if (CharHeader2[GuiLib_CHR_LINECTRL_OFS] & 0x01)
      X2 = (*TextXOfsPtr)[CharPos2] +
           (GuiConst_INT16S)CharHeader2[GuiLib_CHR_PSRIGHT_OFS];
    else
      X2 = (*TextXOfsPtr)[CharPos2] +
           (GuiConst_INT16S)CharHeader1[GuiLib_CHR_XLEFT_OFS] +
           (GuiConst_INT16S)CharHeader2[GuiLib_CHR_XWIDTH_OFS] - 1;
  }
  else
    X2 = (*TextXOfsPtr)[CharPos2] + CurFont->XSize - 1;
#else
#ifdef GuiConst_AVRGCC_COMPILER
  if (TextPsMode[CharPos1])
  {
    if (pgm_read_byte(TextCharPtrAry[CharPos1] +
        GuiLib_CHR_LINECTRL_OFS) & 0x01)
      X1 = (*TextXOfsPtr)[CharPos1] +
           (GuiConst_INT16S)(pgm_read_byte(TextCharPtrAry[CharPos1] +
            GuiLib_CHR_PSLEFT_OFS));
    else
      X1 = (*TextXOfsPtr)[CharPos1] +
           (GuiConst_INT16S)(pgm_read_byte(TextCharPtrAry[CharPos1] +
            GuiLib_CHR_XLEFT_OFS));
  }
  else
    X1 = (*TextXOfsPtr)[CharPos1];
  if (TextPsMode[CharPos2])
  {
    if (pgm_read_byte(TextCharPtrAry[CharPos2] +
        GuiLib_CHR_LINECTRL_OFS) & 0x01)
      X2 = (*TextXOfsPtr)[CharPos2] +
           (GuiConst_INT16S)(pgm_read_byte(TextCharPtrAry[CharPos2] +
            GuiLib_CHR_PSRIGHT_OFS));
    else
      X2 = (*TextXOfsPtr)[CharPos2] +
           (GuiConst_INT16S)(pgm_read_byte(TextCharPtrAry[CharPos2] +
            GuiLib_CHR_XLEFT_OFS)) +
           (GuiConst_INT16S)(pgm_read_byte(TextCharPtrAry[CharPos2] +
            GuiLib_CHR_XWIDTH_OFS)) - 1;
  }
  else
    X2 = (*TextXOfsPtr)[CharPos2] + CurFont->XSize - 1;
#else
  if (TextPsMode[CharPos1])
  {
    if (*(TextCharPtrAry[CharPos1] + GuiLib_CHR_LINECTRL_OFS) & 0x01)
      X1 = (*TextXOfsPtr)[CharPos1] +
           (GuiConst_INT16S)(*(TextCharPtrAry[CharPos1] +
            GuiLib_CHR_PSLEFT_OFS));
    else
      X1 = (*TextXOfsPtr)[CharPos1] +
           (GuiConst_INT16S)(*(TextCharPtrAry[CharPos1] +
            GuiLib_CHR_XLEFT_OFS));
  }
  else
    X1 = (*TextXOfsPtr)[CharPos1];
  if (TextPsMode[CharPos2])
  {
    if (*(TextCharPtrAry[CharPos2] + GuiLib_CHR_LINECTRL_OFS) & 0x01)
      X2 = (*TextXOfsPtr)[CharPos2] +
           (GuiConst_INT16S)(*(TextCharPtrAry[CharPos2] +
            GuiLib_CHR_PSRIGHT_OFS));
    else
      X2 = (*TextXOfsPtr)[CharPos2] +
           (GuiConst_INT16S)(*(TextCharPtrAry[CharPos2] +
            GuiLib_CHR_XLEFT_OFS)) +
           (GuiConst_INT16S)(*(TextCharPtrAry[CharPos2] +
            GuiLib_CHR_XWIDTH_OFS)) - 1;
  }
  else
    X2 = (*TextXOfsPtr)[CharPos2] + CurFont->XSize - 1;
#endif
#endif

  *pXStart = X1;
  *pXEnd = X2;
  return(X2 - X1 + 1);
}

#ifdef GuiConst_CHARMODE_UNICODE
//------------------------------------------------------------------------------
static GuiConst_INT32U GetCharNdx(
   GuiLib_FontRecPtr Font,
   GuiConst_INT16U CharCode)
{
  GuiConst_INT32U CharNdx,CharNdx1,CharNdx2;

#ifdef GuiConst_AVRGCC_COMPILER
  CharNdx1 = pgm_read_word(&Font->FirstCharNdx) + 1;
  CharNdx2 = CharNdx1 + pgm_read_word(&Font->CharCount) - 1;
#else
  CharNdx1 = Font->FirstCharNdx + 1;
  CharNdx2 = CharNdx1 + Font->CharCount - 1;
#endif
  do
  {
    CharNdx = CharNdx1 + ((CharNdx2 - CharNdx1) >> 1);
#ifdef GuiConst_AVRGCC_COMPILER
    if (pgm_read_word(&GuiFont_ChUnicodeList[CharNdx]) == CharCode)
#else
    if (GuiFont_ChUnicodeList[CharNdx] == CharCode)
#endif
      return (CharNdx);
    if (CharNdx1 == CharNdx2)
#ifdef GuiConst_AVRGCC_COMPILER
      return (pgm_read_word(&Font->FirstCharNdx));
#else
      return (Font->FirstCharNdx);
#endif
#ifdef GuiConst_AVRGCC_COMPILER
    if (pgm_read_word(&GuiFont_ChUnicodeList[CharNdx]) > CharCode)
#else
    if (GuiFont_ChUnicodeList[CharNdx] > CharCode)
#endif
      CharNdx2 = CharNdx - 1;
    else
      CharNdx1 = CharNdx + 1;
    if (CharNdx1 > CharNdx2)
#ifdef GuiConst_AVRGCC_COMPILER
      return (pgm_read_word(&Font->FirstCharNdx));
#else
      return (Font->FirstCharNdx);
#endif
  }
  while (1);
}
#endif

//------------------------------------------------------------------------------
static void PrepareText(
   GuiConst_TEXT PrefixGeneric *CharPtr,
   GuiConst_INT16U CharCnt)
{
  GuiConst_INT16S P;
#ifdef GuiConst_CHARMODE_ANSI
  GuiConst_INT8U CharCode;
  GuiConst_INT8U CharSet;
#else
  GuiConst_INT16U CharCode;
  GuiConst_INT16U CharNdx;
#endif

  if (CharCnt > GuiConst_MAX_TEXT_LEN)
    CharCnt = GuiConst_MAX_TEXT_LEN;

  for (P = 0; P < CharCnt; P++)
  {
#ifdef GuiConst_CHARMODE_ANSI

#ifdef GuiConst_AVRGCC_COMPILER
    if (displayVarNow)
      CharCode = (unsigned GuiConst_CHAR) *CharPtr;
    else
      CharCode = (unsigned GuiConst_CHAR) pgm_read_byte(CharPtr);
#else
#ifdef GuiConst_ICC_COMPILER
    if (displayVarNow)
      CharCode = (unsigned GuiConst_CHAR) *CharPtr;
    else
      CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
    if (displayVarNow)
      CharCode = (unsigned GuiConst_CHAR) *CharPtr;
    else
      CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
#else
      CharCode = (unsigned GuiConst_TEXT) *CharPtr;
#endif
#endif
#endif

    CharSet = GuiLib_CurCharSet;

#ifdef GuiConst_REMOTE_FONT_DATA
    if ((CharSet > 0) &&
       ((CharCode < CurFont->FirstChar[CharSet]) ||
        (CharCode > CurFont->LastChar[CharSet])))
      CharSet = 0;
    if ((CharCode < CurFont->FirstChar[CharSet]) ||
        (CharCode > CurFont->LastChar[CharSet]))
      TextCharNdx[P] = CurFont->IllegalCharNdx;
    else
    {
      TextCharNdx[P] =
         CurFont->FirstCharNdx[CharSet] + (GuiConst_INT16U)CharCode -
         (GuiConst_INT16U)CurFont->FirstChar[CharSet];

      if ((GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[TextCharNdx[P] + 1] -
          (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[TextCharNdx[P]] == 0)
        TextCharNdx[P] = CurFont->IllegalCharNdx;
    }
#else
#ifdef GuiConst_AVRGCC_COMPILER
    if ((CharSet > 0) &&
       ((CharCode < pgm_read_byte(&CurFont->FirstChar[CharSet])) ||
        (CharCode > pgm_read_byte(&CurFont->LastChar[CharSet]))))
      CharSet = 0;
    if ((CharCode < pgm_read_byte(&CurFont->FirstChar[CharSet])) ||
        (CharCode > pgm_read_byte(&CurFont->LastChar[CharSet])))
      TextCharPtrAry[P] = (GuiConst_INT8U*)pgm_read_word(
         &GuiFont_ChPtrList[pgm_read_word(&CurFont->IllegalCharNdx)]);
    else
      TextCharPtrAry[P] = (GuiConst_INT8U*)pgm_read_word(
         &GuiFont_ChPtrList[pgm_read_word(&CurFont->FirstCharNdx[CharSet]) +
         (GuiConst_INT16U)CharCode -
         (GuiConst_INT16U)pgm_read_byte(&CurFont->FirstChar[CharSet])]);
#else
    if ((CharSet > 0) &&
       ((CharCode < CurFont->FirstChar[CharSet]) ||
        (CharCode > CurFont->LastChar[CharSet])))
      CharSet = 0;
    if ((CharCode < CurFont->FirstChar[CharSet]) ||
        (CharCode > CurFont->LastChar[CharSet]))
      TextCharPtrAry[P] =
        (GuiConst_INT8U PrefixRom *)GuiFont_ChPtrList[CurFont->IllegalCharNdx];
    else
      TextCharPtrAry[P] = (GuiConst_INT8U PrefixRom *)GuiFont_ChPtrList[
         CurFont->FirstCharNdx[CharSet] + (GuiConst_INT16U)CharCode -
         (GuiConst_INT16U)CurFont->FirstChar[CharSet]];
#endif
#endif

    if (CurItem.Ps == GuiLib_PS_ON)
      TextPsMode[P] = 1;
    else if (CurItem.Ps == GuiLib_PS_NUM)
    {
      if (CharSet > 0)
        TextPsMode[P] = 1;
      else if (((CharCode >= '0') && (CharCode <= '9')) ||
         (CharCode == ' ') || (CharCode == '+') || (CharCode == '-') ||
         (CharCode == '*') || (CharCode == '/') || (CharCode == '='))
        TextPsMode[P] = 0;
      else
        TextPsMode[P] = 1;
    }
    else
      TextPsMode[P] = 0;

#else
    CharCode = (GuiConst_INT16U) *CharPtr;
    CharNdx = GetCharNdx(CurFont, CharCode);

#ifdef GuiConst_REMOTE_FONT_DATA
    TextCharNdx[P] = CharNdx;
#else
#ifdef GuiConst_AVRGCC_COMPILER
    TextCharPtrAry[P] =
       (GuiConst_INT8U PrefixRom *)pgm_read_word(&GuiFont_ChPtrList[CharNdx]);
#else
    TextCharPtrAry[P] = (GuiConst_INT8U PrefixRom *)GuiFont_ChPtrList[CharNdx];
#endif
#endif

    if (CurItem.Ps == GuiLib_PS_ON)
      TextPsMode[P] = 1;
    else if ((CurItem.Ps == GuiLib_PS_NUM) && (CharNdx > 0))
    {
      if (((CharCode >= '0') && (CharCode <= '9')) ||
         (CharCode == ' ') || (CharCode == '+') || (CharCode == '-') ||
         (CharCode == '*') || (CharCode == '/') || (CharCode == '='))
        TextPsMode[P] = 0;
      else
        TextPsMode[P] = 1;
    }
    else
      TextPsMode[P] = 0;
#endif
    CharPtr++;
  }
}

#ifdef GuiConst_ARAB_CHARS_INUSE
//------------------------------------------------------------------------------
static GuiConst_INT16U ArabicCorrection(
   GuiConst_TEXT PrefixGeneric *CharPtr,
   GuiConst_INT16U CharCnt,
   GuiConst_INT8U RightToLeftWriting)
{
  GuiConst_INT16S I, J, P;
  GuiConst_INT16U CharCode;
  GuiConst_INT16U CharCode2;
  GuiConst_TEXT *CharPtr2;
  GuiConst_TEXT *CharPtrA;
  GuiConst_TEXT *CharPtrB;

  if (!RightToLeftWriting)
    return CharCnt;

  CharPtr2 = CharPtr;

  for (P = 0; P < CharCnt; P++)
  {
    CharCode = *((GuiConst_INT16U*)CharPtr);
    switch (CharCode)
    {
      case 40:
        *((GuiConst_INT16U*)CharPtr) = 41;
        break;
      case 41:
        *((GuiConst_INT16U*)CharPtr) = 40;
        break;

      case 91:
        *((GuiConst_INT16U*)CharPtr) = 93;
        break;
      case 93:
        *((GuiConst_INT16U*)CharPtr) = 91;
        break;

      case 123:
        *((GuiConst_INT16U*)CharPtr) = 125;
        break;
      case 125:
        *((GuiConst_INT16U*)CharPtr) = 123;
        break;
    }

    CharPtr++;
  }

  P = 0;
  CharPtr = CharPtr2;
  do
  {
    CharCode = *((GuiConst_INT16U*)CharPtr);
    CharPtrA = CharPtr;
    CharPtr++;
    CharCode2 = *((GuiConst_INT16U*)CharPtr);

    for (I = 0; I < GuiLib_ARAB_LIGATURES_CNT; I++)
      if ((CharCode == GuiLib_ARAB_LIGATURES[I][0]) &&
          (CharCode2 == GuiLib_ARAB_LIGATURES[I][1]))
      {
        *((GuiConst_INT16U*)CharPtrA) = GuiLib_ARAB_LIGATURES[I][2];
        CharPtrA = CharPtr;
        CharPtrB = CharPtrA;
        CharPtrB++;
        CharCnt--;
        for (J = P + 1; J < CharCnt; J++)
        {
          *((GuiConst_INT16U*)CharPtrA) = *((GuiConst_INT16U*)CharPtrB);
          CharPtrA++;
          CharPtrB++;
        }
        *((GuiConst_INT16U*)CharPtrA) = 0;
        break;
      }

    P++;
  }
  while (P < CharCnt - 1);

  CharPtr = CharPtr2;
  for (P = 0; P < CharCnt; P++)
  {
    CharCode = *((GuiConst_INT16U*)CharPtr);

    for (I = 0; I < GuiLib_ARAB_CHAR_CONVERT_CNT; I++)
      if ((CharCode == GuiLib_ARAB_CHAR_CONVERT[I][0]) && (CharCode > 0))
      {
        *((GuiConst_INT16U*)CharPtr) = GuiLib_ARAB_CHAR_CONVERT[I][1];
        break;
      }

    CharPtr++;
  }

  ArabicCharJoiningModeIndex[0] = -1;
  CharPtr = CharPtr2;
  for (P = 0; P < CharCnt; P++)
  {
    CharCode = *((GuiConst_INT16U*)CharPtr);

    ArabicCharJoiningModeIndex[P + 1] = -1;
    for (I = 0; I < GuiLib_ARAB_CHAR_CONVERT_CNT; I++)
      if ((CharCode >= GuiLib_ARAB_CHAR_CONVERT[I][1]) &&
          (CharCode <= GuiLib_ARAB_CHAR_CONVERT[I][1] +
          (GuiLib_ARAB_CHAR_CONVERT[I][2] & 7) - 1))
      {
        ArabicCharJoiningModeIndex[P + 1] = I;
        break;
      }

    CharPtr++;
  }
  ArabicCharJoiningModeIndex[CharCnt + 1] = -1;

  for (P = 0; P < CharCnt + 2; P++)
  {
    if (ArabicCharJoiningModeIndex[P] == -1)
      ArabicCharJoiningMode[P] = GuiLib_ARAB_CHAR_TYPE_ISO;
    else
      ArabicCharJoiningMode[P] =
         GuiLib_ARAB_CHAR_CONVERT[ArabicCharJoiningModeIndex[P]][2];
  }

  CharPtr = CharPtr2;
  for (P = 0; P < CharCnt; P++)
  {
    CharCode = *((GuiConst_INT16U*)CharPtr);

    I = P;
    while (ArabicCharJoiningMode[I] == GuiLib_ARAB_CHAR_DIACRITIC)
      I--;
    ArabicCharJoiningModeBefore = ArabicCharJoiningMode[I];
    I = P + 2;
    while (ArabicCharJoiningMode[I] == GuiLib_ARAB_CHAR_DIACRITIC)
      I++;
    ArabicCharJoiningModeAfter = ArabicCharJoiningMode[I];

    switch (ArabicCharJoiningMode[P + 1])
    {
      case GuiLib_ARAB_CHAR_ISOFIN:
        if (ArabicCharJoiningModeBefore == GuiLib_ARAB_CHAR_ISOFININIMED)
          *((GuiConst_INT16U*)CharPtr) =
             GuiLib_ARAB_CHAR_CONVERT[ArabicCharJoiningModeIndex[P + 1]][1] +
             GuiLib_ARAB_CHAR_TYPE_FIN;
        break;

      case GuiLib_ARAB_CHAR_ISOFININIMED:
        if ((ArabicCharJoiningModeAfter == GuiLib_ARAB_CHAR_ISOFIN) ||
            (ArabicCharJoiningModeAfter == GuiLib_ARAB_CHAR_ISOFININIMED))
        {
          if (ArabicCharJoiningModeBefore == GuiLib_ARAB_CHAR_ISOFININIMED)
            *((GuiConst_INT16U*)CharPtr) =
               GuiLib_ARAB_CHAR_CONVERT[ArabicCharJoiningModeIndex[P + 1]][1] +
               GuiLib_ARAB_CHAR_TYPE_MED;
          else
            *((GuiConst_INT16U*)CharPtr) =
               GuiLib_ARAB_CHAR_CONVERT[ArabicCharJoiningModeIndex[P + 1]][1] +
               GuiLib_ARAB_CHAR_TYPE_INI;
        }
        else if (ArabicCharJoiningModeBefore == GuiLib_ARAB_CHAR_ISOFININIMED)
          *((GuiConst_INT16U*)CharPtr) =
             GuiLib_ARAB_CHAR_CONVERT[ArabicCharJoiningModeIndex[P + 1]][1] +
             GuiLib_ARAB_CHAR_TYPE_FIN;
        break;

      case GuiLib_ARAB_CHAR_DIACRITIC:
        if (((ArabicCharJoiningMode[P + 2] == GuiLib_ARAB_CHAR_ISOFIN) ||
             (ArabicCharJoiningMode[P + 2] == GuiLib_ARAB_CHAR_ISOFININIMED)) &&
            (ArabicCharJoiningMode[P] == GuiLib_ARAB_CHAR_ISOFININIMED))
          *((GuiConst_INT16U*)CharPtr) =
             GuiLib_ARAB_CHAR_CONVERT[ArabicCharJoiningModeIndex[P + 1]][1] + 1;
        break;
    }

    CharPtr++;
  }

  return (CharCnt);
}
#endif

//------------------------------------------------------------------------------
static void ResetDrawLimits(void)
{
  Drawn = 0;
  DrawnX1 = 0x7FFF;
  DrawnY1 = 0x7FFF;
  DrawnX2 = 0x8000;
  DrawnY2 = 0x8000;
}

//------------------------------------------------------------------------------
static void UpdateDrawLimits(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  CurItem.Drawn = 1;
  CurItem.DrawnX1 = X1;
  CurItem.DrawnY1 = Y1;
  CurItem.DrawnX2 = X2;
  CurItem.DrawnY2 = Y2;

  Drawn = 1;
  DrawnX1 = GuiLib_GET_MIN(DrawnX1, X1);
  DrawnY1 = GuiLib_GET_MIN(DrawnY1, Y1);
  DrawnX2 = GuiLib_GET_MAX(DrawnX2, X2);
  DrawnY2 = GuiLib_GET_MAX(DrawnY2, Y2);
}

//------------------------------------------------------------------------------
static void DrawBorderBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR BorderColor,
   GuiConst_INTCOLOR FillColor,
   GuiConst_INT8U FillTransparent,
   GuiConst_INT16U BorderThickness)
{
  if (BorderThickness == 1)
    GuiLib_Box(X1, Y1, X2, Y2, BorderColor);
  else
  {
    GuiLib_FillBox(X1, Y1, X1 + BorderThickness - 1, Y2, BorderColor);
    GuiLib_FillBox(X2 - BorderThickness + 1, Y1, X2, Y2, BorderColor);
    GuiLib_FillBox(X1, Y1, X2, Y1 + BorderThickness - 1, BorderColor);
    GuiLib_FillBox(X1, Y2 - BorderThickness + 1, X2, Y2, BorderColor);
  }
  if ((!FillTransparent) &&
     (X2 - X1 >= 2 * BorderThickness) &&
     (Y2 - Y1 >= 2 * BorderThickness))
    GuiLib_FillBox(X1 + BorderThickness,
                   Y1 + BorderThickness,
                   X2 - BorderThickness,
                   Y2 - BorderThickness,
                   FillColor);
}

//------------------------------------------------------------------------------
static void SetBackColorBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  BbX1 = X1;
  BbY1 = Y1;
  BbX2 = X2;
  BbY2 = Y2;

  if ((CurItem.BackBorderPixels & GuiLib_BBP_LEFT) != 0)
    BbX1--;
  if ((CurItem.BackBorderPixels & GuiLib_BBP_RIGHT) != 0)
    BbX2++;
  if ((CurItem.BackBorderPixels & GuiLib_BBP_TOP) != 0)
    BbY1--;
  if ((CurItem.BackBorderPixels & GuiLib_BBP_BOTTOM) != 0)
    BbY2++;
}

//------------------------------------------------------------------------------
static void SetBackBox(void)
{
  GuiConst_INT16S L;

  L = CurItem.BackBoxSizeX;
  BbX1 = CurItem.X1;
  BbX2 = CurItem.X1 + L - 1;

  if (CurItem.Alignment == GuiLib_ALIGN_CENTER)
  {
    BbX1 -= L / 2;
    BbX2 -= L / 2;
  }
  else if (CurItem.Alignment == GuiLib_ALIGN_RIGHT)
  {
    BbX1 -= L - 1;
    BbX2 -= L - 1;
  }

  if (CurItem.BackBoxSizeY1 > 0)
    BbY1 = CurItem.Y1 - CurItem.BackBoxSizeY1;
  else
#ifdef GuiConst_AVRGCC_COMPILER
    BbY1 = CurItem.Y1 - pgm_read_byte(&CurFont->BaseLine);
#else
    BbY1 = CurItem.Y1 - CurFont->BaseLine;
#endif
  if (CurItem.BackBoxSizeY2 > 0)
    BbY2 = CurItem.Y1 + CurItem.BackBoxSizeY2;
  else
#ifdef GuiConst_AVRGCC_COMPILER
    BbY2 = CurItem.Y1 - pgm_read_byte(&CurFont->BaseLine) +
       pgm_read_byte(&CurFont->YSize) - 1;
#else
    BbY2 = CurItem.Y1 - CurFont->BaseLine + CurFont->YSize - 1;
#endif

  if ((CurItem.BackBorderPixels & GuiLib_BBP_LEFT) != 0)
    BbX1--;
  if ((CurItem.BackBorderPixels & GuiLib_BBP_RIGHT) != 0)
    BbX2++;
  if ((CurItem.BackBorderPixels & GuiLib_BBP_TOP) != 0)
    BbY1--;
  if ((CurItem.BackBorderPixels & GuiLib_BBP_BOTTOM) != 0)
    BbY2++;
}

//------------------------------------------------------------------------------
static void DrawBackBox(
   GuiConst_INTCOLOR BoxColor,
   GuiConst_INT8U Transparent)
{
  SetBackBox();
  if (!Transparent)
    GuiLib_FillBox(BbX1, BbY1, BbX2, BbY2, BoxColor);
}

//------------------------------------------------------------------------------
static void DrawText(
   GuiConst_TEXT PrefixGeneric *CharPtr,
   GuiConst_INT16U CharCnt,
   GuiConst_INTCOLOR ForeColor,
   GuiConst_INTCOLOR BackColor,
   GuiConst_INT8U Transparent)
{
  GuiConst_INT16S P, N;
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT32U PrefixRom TempOfs;
  GuiConst_INT8U CharHeader[GuiLib_CHR_LINECTRL_OFS];
#else
  GuiConst_INT8U PrefixRom * TempPtr;
#endif
  GuiConst_INT8U TempPsMode;
  GuiConst_INT16S TextPixelLen;
  GuiConst_INT16S X1, Y1, X2, Y2;
  TextXOfsAry TextXOfs;

#ifdef GuiConst_ARAB_CHARS_INUSE
  CharCnt = ArabicCorrection(CharPtr,
                             CharCnt,
                             (CurItem.BitFlags &
                              GuiLib_BITFLAG_REVERSEWRITING) > 0);
#endif
  PrepareText(CharPtr, CharCnt);

  if (CurItem.BitFlags & GuiLib_BITFLAG_REVERSEWRITING)
  {
    for (N = 0; N < CharCnt / 2; N++)
    {
#ifdef GuiConst_REMOTE_FONT_DATA
      TempOfs = TextCharNdx[N];
      TextCharNdx[N] = TextCharNdx[CharCnt - 1 - N];
      TextCharNdx[CharCnt - 1 - N] = TempOfs;
#else
      TempPtr = TextCharPtrAry[N];
      TextCharPtrAry[N] =
         (GuiConst_INT8U PrefixRom *)TextCharPtrAry[CharCnt - 1 - N];
      TextCharPtrAry[CharCnt - 1 - N] = (GuiConst_INT8U PrefixRom *)TempPtr;
#endif
      TempPsMode = TextPsMode[N];
      TextPsMode[N] = TextPsMode[CharCnt - 1 - N];
      TextPsMode[CharCnt - 1 - N] = TempPsMode;
    }
  }

  TextPixelLen = TextPixelLength(CurItem.Ps, CharCnt, (TextXOfsAryPtr)TextXOfs);

  X1 = CurItem.X1;
  switch (CurItem.Alignment)
  {
    case GuiLib_ALIGN_CENTER:
      if (CharCnt > 0)
        X1 -= TextPixelLen / 2;
      break;

    case GuiLib_ALIGN_RIGHT:
      X1 -= TextPixelLen - 1;
      break;
  }
#ifdef GuiConst_AVRGCC_COMPILER
  Y1 = CurItem.Y1 - pgm_read_byte(&CurFont->BaseLine);
  X2 = X1 + TextPixelLen - 1;
  Y2 = Y1 + pgm_read_byte(&CurFont->YSize) - 1;
#else
  Y1 = CurItem.Y1 - CurFont->BaseLine;
  X2 = X1 + TextPixelLen - 1;
  Y2 = Y1 + CurFont->YSize - 1;
#endif
  FontWriteX1 = X1;
  FontWriteY1 = Y1;
  FontWriteX2 = X2;
  FontWriteY2 = Y2;

  if (DisplayWriting)
  {
    if (CharCnt > 0)
    {
      if (!Transparent && (CurItem.BackBoxSizeX == 0))
      {
        SetBackColorBox(X1, Y1, X2, Y2);
        GuiLib_FillBox(BbX1, BbY1, BbX2, BbY2, BackColor);
      }

      for (P = 0; P < CharCnt; P++)
#ifdef GuiConst_REMOTE_FONT_DATA
        DrawChar(X1 + TextXOfs[P], Y1, CurFont, TextCharNdx[P], ForeColor);
#else
        DrawChar(X1 + TextXOfs[P], Y1, CurFont, TextCharPtrAry[P], ForeColor);
#endif
    }

    if ((CurItem.BitFlags & GuiLib_BITFLAG_UNDERLINE) && (CharCnt > 0))
#ifdef GuiConst_AVRGCC_COMPILER
      GuiLib_FillBox(X1, Y1 + pgm_read_byte(&CurFont->Underline1), X2,
         Y1 + pgm_read_byte(&CurFont->Underline2), ForeColor);
#else
      GuiLib_FillBox(X1, Y1 + CurFont->Underline1, X2,
         Y1 + CurFont->Underline2, ForeColor);
#endif

    GuiLib_MarkDisplayBoxRepaint(X1, Y1, X2, Y2);

#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
    if ((CurItem.BitFlags & GuiLib_BITFLAG_BLINKTEXTFIELD) &&
        (CurItem.BlinkFieldNo < GuiConst_BLINK_FIELDS_MAX))
    {
      BlinkTextItems[CurItem.BlinkFieldNo].InUse = 1;
      BlinkTextItems[CurItem.BlinkFieldNo].ItemType = CurItem.ItemType;
      BlinkTextItems[CurItem.BlinkFieldNo].CharCnt = CharCnt;
      BlinkTextItems[CurItem.BlinkFieldNo].Ps = CurItem.Ps;
      BlinkTextItems[CurItem.BlinkFieldNo].Alignment = CurItem.Alignment;
      BlinkTextItems[CurItem.BlinkFieldNo].FormatFieldWidth =
         CurItem.FormatFieldWidth;
      BlinkTextItems[CurItem.BlinkFieldNo].FormatDecimals =
         CurItem.FormatDecimals;
      BlinkTextItems[CurItem.BlinkFieldNo].FormatAlignment =
         CurItem.FormatAlignment;
      BlinkTextItems[CurItem.BlinkFieldNo].FormatFormat = CurItem.FormatFormat;
      BlinkTextItems[CurItem.BlinkFieldNo].VarType = CurItem.VarType;
#ifdef GuiConst_CHARMODE_ANSI
      BlinkTextItems[CurItem.BlinkFieldNo].CharSetSelector =
         CurItem.CharSetSelector;
#endif
      BlinkTextItems[CurItem.BlinkFieldNo].FontIndex = CurItem.FontIndex;
      BlinkTextItems[CurItem.BlinkFieldNo].XSize =
#ifdef GuiConst_AVRGCC_COMPILER
      pgm_read_byte(&CurFont->XSize);
#else
      CurFont->XSize;
#endif
      BlinkTextItems[CurItem.BlinkFieldNo].PsNumWidth =
#ifdef GuiConst_AVRGCC_COMPILER
      pgm_read_byte(&CurFont->PsNumWidth);
#else
      CurFont->PsNumWidth;
#endif
      BlinkTextItems[CurItem.BlinkFieldNo].PsSpace =
#ifdef GuiConst_AVRGCC_COMPILER
      pgm_read_byte(&CurFont->PsSpace);
#else
      CurFont->PsSpace;
#endif
      if (CurItem.ItemType == GuiLib_ITEM_TEXT)
        BlinkTextItems[CurItem.BlinkFieldNo].TextPtr = CurItem.TextPtr;
      else
        BlinkTextItems[CurItem.BlinkFieldNo].TextPtr =
           (GuiConst_TEXT*)CurItem.VarPtr;
      BlinkTextItems[CurItem.BlinkFieldNo].BitFlags = CurItem.BitFlags;
      BlinkTextItems[CurItem.BlinkFieldNo].X1 = CurItem.X1;
      BlinkTextItems[CurItem.BlinkFieldNo].X2 = CurItem.X2;
      if (CurItem.BackBoxSizeX > 0)
      {
        if (CurItem.BackBoxSizeY1 > 0)
          BlinkTextItems[CurItem.BlinkFieldNo].Y1 =
             CurItem.Y1 - CurItem.BackBoxSizeY1;
        else
          BlinkTextItems[CurItem.BlinkFieldNo].Y1 =
#ifdef GuiConst_AVRGCC_COMPILER
             CurItem.Y1 - pgm_read_byte(&CurFont->BaseLine);
#else
             CurItem.Y1 - CurFont->BaseLine;
#endif
        if (CurItem.BackBoxSizeY1 > 0)
          BlinkTextItems[CurItem.BlinkFieldNo].Y2 =
             CurItem.Y1 + CurItem.BackBoxSizeY2;
        else
          BlinkTextItems[CurItem.BlinkFieldNo].Y2 =
#ifdef GuiConst_AVRGCC_COMPILER
             CurItem.Y1 - pgm_read_byte(&CurFont->BaseLine) +
                pgm_read_byte(&CurFont->YSize) - 1;
#else
             CurItem.Y1 - CurFont->BaseLine + CurFont->YSize - 1;
#endif
      }
      else
      {
        BlinkTextItems[CurItem.BlinkFieldNo].Y1 = Y1;
        BlinkTextItems[CurItem.BlinkFieldNo].Y2 = Y2;
      }
    }
#endif
#endif
  }
}

#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
//------------------------------------------------------------------------------
static void DrawTextBlock(
   GuiConst_TEXT PrefixGeneric *CharPtr,
   GuiConst_INT16U CharCnt,
   GuiConst_INTCOLOR ForeColor,
   GuiConst_INTCOLOR BackColor,
   GuiConst_INT8U Transparent)
{
  GuiConst_INT16S X1, Y1, X2, Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiConst_INT16S CX1, CX2;
#endif
  GuiConst_INT16S CY1, CY2;
  TextXOfsAry TextXOfs;
  GuiConst_INT16S TextCharLineStart[GuiConst_MAX_PARAGRAPH_LINE_CNT];
  GuiConst_INT16S TextCharLineEnd[GuiConst_MAX_PARAGRAPH_LINE_CNT];
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiConst_INT16S RemClipX1, RemClipY1, RemClipX2, RemClipY2;
#endif
#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
  GuiConst_INT16S BlinkNo;
#endif
#endif
  GuiConst_INT16S PixWidth;
  GuiConst_INT16S N, P, M;
  GuiConst_INT16S LineLen;
  GuiConst_INT16S LineCnt;
  GuiConst_INT16S LineCnt2;
  GuiConst_INT16S BlindLinesAtTop;
  GuiConst_INT16S XPos, YPos;
  GuiConst_INT16S XStart, XEnd;
  GuiConst_INT8U  TempPsMode;
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT32U PrefixRom TempOfs;
  GuiConst_INT8U CharHeader1[GuiLib_CHR_LINECTRL_OFS];
  GuiConst_INT8U CharHeader2[GuiLib_CHR_LINECTRL_OFS];
#else
  GuiConst_INT8U PrefixRom * TempPtr;
#endif

  X1 = CurItem.X1;
  Y1 = CurItem.Y1;
  X2 = CurItem.X2;
  Y2 = CurItem.Y2;
  FontWriteX1 = X1;
  FontWriteY1 = Y1;
  FontWriteX2 = X2;
  FontWriteY2 = Y2;

  PixWidth = 0;

  if (DisplayWriting)
  {
    if (!Transparent && (CurItem.BackBoxSizeX == 0))
    {
      SetBackColorBox(X1, Y1, X2, Y2);
      GuiLib_FillBox(BbX1, BbY1, BbX2, BbY2, BackColor);
    }

    if (CurItem.TextBoxLineDistRelToFont)
    {
      CurItem.TextBoxLineDist +=CurFont->YSize;
      CurItem.TextBoxLineDistRelToFont = 0;
    }

    LineCnt = 0;
#ifdef GuiConst_TEXTBOX_FIELDS_ON
    CurItem.TextBoxLines = 1;
#endif
    if (CharCnt > 0)
    {
      PixWidth = X2 - X1 + 1;

#ifdef GuiConst_ARAB_CHARS_INUSE
      CharCnt =
         ArabicCorrection(CharPtr,
                          CharCnt,
                          (CurItem.BitFlags &
                           GuiLib_BITFLAG_REVERSEWRITING) > 0);
#endif
      PrepareText(CharPtr, CharCnt);
      TextPixelLength(CurItem.Ps, CharCnt, (TextXOfsAryPtr)TextXOfs);

      TextCharLineStart[0] = 0;
      TextCharLineEnd[0] = -1;
#ifdef GuiConst_TEXTBOX_FIELDS_ON
      if (CurItem.TextBoxScrollIndex == 0xFF)
        BlindLinesAtTop = 0;
      else
        BlindLinesAtTop = CurItem.TextBoxScrollPos / CurItem.TextBoxLineDist;
#else
      BlindLinesAtTop = 0;
#endif
      LineCnt = 1 - BlindLinesAtTop;
      if (LineCnt >= 1)
        LineCnt = 1;
      LineCnt2 = 1;
      P = 0;
#ifdef GuiConst_REMOTE_FONT_DATA
      GuiLib_RemoteDataReadBlock(
         (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[
         TextCharNdx[TextCharLineStart[LineCnt2 - 1]]], GuiLib_CHR_LINECTRL_OFS,
         CharHeader2);
#endif
      while (P < CharCnt)
      {
#ifdef GuiConst_AVRGCC_COMPILER
        while ((P < CharCnt - 1) &&
           !((pgm_read_byte(CharPtr + P) == GuiLib_LINEFEED) ||
  #ifdef GuiConst_BLINK_LF_COUNTS
           ((pgm_read_byte(CharPtr + P - 1) != ' ') &&
           (pgm_read_byte(CharPtr + P) == ' ')) ||
           ((pgm_read_byte(CharPtr + P - 1) == '-') &&
           (pgm_read_byte(CharPtr + P) != ' '))))
  #else
           ((pgm_read_byte(CharPtr + P) != ' ') &&
           (pgm_read_byte(CharPtr + P + 1) == ' ')) ||
           ((pgm_read_byte(CharPtr + P) == '-') &&
           (pgm_read_byte(CharPtr + P + 1) != ' '))))
  #endif
          P++;
#else
        while ((P < CharCnt - 1) &&
            !((*(CharPtr + P) == GuiLib_LINEFEED) ||
  #ifdef GuiConst_BLINK_LF_COUNTS
              ((*(CharPtr + P - 1) != ' ') && (*(CharPtr + P) == ' ')) ||
              ((*(CharPtr + P - 1) == '-') && (*(CharPtr + P) != ' '))))

  #else
              ((*(CharPtr + P) != ' ') && (*(CharPtr + P + 1) == ' ')) ||
              ((*(CharPtr + P) == '-') && (*(CharPtr + P + 1) != ' '))))
  #endif
          P++;
#endif

        if (CalcCharsWidth(TextCharLineStart[LineCnt2 - 1], P,
            &TextXOfs, &XStart, &XEnd) > PixWidth)
        {
          if (TextCharLineEnd[LineCnt2 - 1] == -1)
          {
            TextCharLineEnd[LineCnt2 - 1] = P;
            TextCharLineStart[LineCnt2] = P + 1;
            TextCharLineEnd[LineCnt2] = -1;
          }
          else
          {
            TextCharLineStart[LineCnt2] = TextCharLineEnd[LineCnt2 - 1] + 1;
#ifdef GuiConst_AVRGCC_COMPILER
            while ((TextCharLineStart[LineCnt2] < P) &&
               (pgm_read_byte(CharPtr + TextCharLineStart[LineCnt2]) == ' '))
              TextCharLineStart[LineCnt2]++;
#else
            while ((TextCharLineStart[LineCnt2] < P) &&
               (*(CharPtr + TextCharLineStart[LineCnt2]) == ' '))
              TextCharLineStart[LineCnt2]++;
#endif
            TextCharLineEnd[LineCnt2] = P;
          }
          if (LineCnt >= GuiConst_MAX_PARAGRAPH_LINE_CNT)
          {
            P = CharCnt;
            break;
          }
          LineCnt++;
          if (LineCnt > 1)
            LineCnt2 = LineCnt;
          else
          {
            TextCharLineStart[LineCnt2 - 1] = TextCharLineStart[LineCnt2];
            TextCharLineEnd[LineCnt2 - 1] = TextCharLineEnd[LineCnt2];
          }
#ifdef GuiConst_TEXTBOX_FIELDS_ON
          CurItem.TextBoxLines++;
#endif
        }
        else
          TextCharLineEnd[LineCnt2 - 1] = P;

#ifdef GuiConst_AVRGCC_COMPILER
        if (pgm_read_byte(CharPtr + P) == GuiLib_LINEFEED)
#else
        if (*(CharPtr + P) == GuiLib_LINEFEED)
#endif
        {
          TextCharLineEnd[LineCnt2 - 1] = P - 1;
          TextCharLineStart[LineCnt2] = P + 1;
          TextCharLineEnd[LineCnt2] = -1;

          if (LineCnt >= GuiConst_MAX_PARAGRAPH_LINE_CNT)
          {
            P = CharCnt;
            break;
          }
          LineCnt++;
          if (LineCnt > 1)
            LineCnt2 = LineCnt;
          else
          {
            TextCharLineStart[LineCnt2 - 1] = TextCharLineStart[LineCnt2];
            TextCharLineEnd[LineCnt2 - 1] = TextCharLineEnd[LineCnt2];
          }
#ifdef GuiConst_TEXTBOX_FIELDS_ON
          CurItem.TextBoxLines++;
#endif
        }

        P++;
      }
    }

    if (LineCnt >= 1)
    {
      if (CurItem.BitFlags & GuiLib_BITFLAG_REVERSEWRITING)
      {
        for (M = 0; M < LineCnt ; M++)
        {
          for (P = TextCharLineStart[M]; P <= (TextCharLineStart[M] +
             ((TextCharLineEnd[M] - TextCharLineStart[M] + 1) / 2) - 1); P++)
          {
#ifdef GuiConst_REMOTE_FONT_DATA
            TempOfs = TextCharNdx[P];
            TextCharNdx[P] =
               TextCharNdx[TextCharLineEnd[M] - (P - TextCharLineStart[M])];
            TextCharNdx[TextCharLineEnd[M] - (P - TextCharLineStart[M])] =
               TempOfs;
#else
#ifdef GuiConst_CODEVISION_COMPILER
            TempPtr = &TextCharPtrAry[P];
#else
            TempPtr = TextCharPtrAry[P];
#endif
            TextCharPtrAry[P] = (GuiConst_INT8U PrefixRom *)
               TextCharPtrAry[TextCharLineEnd[M] - (P - TextCharLineStart[M])];
            TextCharPtrAry[TextCharLineEnd[M] - (P - TextCharLineStart[M])] =
               (GuiConst_INT8U PrefixRom *)TempPtr;
#endif
            TempPsMode = TextPsMode[P];
            TextPsMode[P] =
               TextPsMode[TextCharLineEnd[M] - (P - TextCharLineStart[M])];
            TextPsMode[TextCharLineEnd[M] - (P - TextCharLineStart[M])] =
               TempPsMode;
          }
        }
        TextPixelLength(CurItem.Ps, CharCnt, (TextXOfsAryPtr)TextXOfs);
      }

      CY1 = Y1;
      CY2 = Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
      CX1 = X1;
      CX2 = X2;
      RemClipX1 = CurItem.ClipRectX1;
      RemClipY1 = CurItem.ClipRectY1;
      RemClipX2 = CurItem.ClipRectX2;
      RemClipY2 = CurItem.ClipRectY2;
      if (RemClipX1 > CX1)
        CX1 = RemClipX1;
      if (RemClipY1 > CY1)
        CY1 = RemClipY1;
      if (RemClipX2 < CX2)
        CX2 = RemClipX2;
      if (RemClipY2 < CY2)
        CY2 = RemClipY2;
      GuiLib_SetClipping(CX1, CY1, CX2, CY2);
#endif

#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
      if ((CurItem.BitFlags & GuiLib_BITFLAG_BLINKTEXTFIELD) &&
          (CurItem.BlinkFieldNo < GuiConst_BLINK_FIELDS_MAX))
      {
        BlinkNo = CurItem.BlinkFieldNo;
        BlinkTextItems[BlinkNo].InUse = 1;
        BlinkTextItems[BlinkNo].ItemType = CurItem.ItemType;
        BlinkTextItems[BlinkNo].VarType = CurItem.VarType;
        BlinkTextItems[BlinkNo].CharCnt = CharCnt;
#ifdef GuiConst_CHARMODE_ANSI
        BlinkTextItems[BlinkNo].CharSetSelector = CurItem.CharSetSelector;
#endif
        BlinkTextItems[BlinkNo].FontIndex = CurItem.FontIndex;
        BlinkTextItems[BlinkNo].XSize = CurFont->XSize;
        BlinkTextItems[BlinkNo].YSize = CurFont->YSize;
        BlinkTextItems[BlinkNo].PsNumWidth = CurFont->PsNumWidth;
        BlinkTextItems[BlinkNo].PsSpace = CurFont->PsSpace;
        if (CurItem.ItemType == GuiLib_ITEM_TEXTBLOCK)
          BlinkTextItems[BlinkNo].TextPtr = CurItem.TextPtr;
        else
          BlinkTextItems[BlinkNo].TextPtr = (GuiConst_TEXT*)CurItem.VarPtr;
        BlinkTextItems[BlinkNo].X1 = X1;
        BlinkTextItems[BlinkNo].X2 = X2;
        BlinkTextItems[BlinkNo].Y1 = Y1;
        BlinkTextItems[BlinkNo].Y2 = Y2;
        BlinkTextItems[BlinkNo].LineCnt = LineCnt;
        BlinkTextItems[BlinkNo].TextBoxLineDist = CurItem.TextBoxLineDist;
        BlinkTextItems[BlinkNo].BlindLinesAtTop = BlindLinesAtTop;
        BlinkTextItems[BlinkNo].Ps = CurItem.Ps;
        BlinkTextItems[BlinkNo].Alignment = CurItem.Alignment;
        BlinkTextItems[BlinkNo].FormatFieldWidth = CurItem.FormatFieldWidth;
        BlinkTextItems[BlinkNo].FormatDecimals = CurItem.FormatDecimals;
        BlinkTextItems[BlinkNo].FormatAlignment = CurItem.FormatAlignment;
        BlinkTextItems[BlinkNo].FormatFormat = CurItem.FormatFormat;
        BlinkTextItems[BlinkNo].BitFlags = CurItem.BitFlags;
        BlinkTextItems[BlinkNo].TextBoxHorzAlignment =
           CurItem.TextBoxHorzAlignment;
        BlinkTextItems[BlinkNo].TextBoxVertAlignment =
           CurItem.TextBoxVertAlignment;
#ifdef GuiConst_TEXTBOX_FIELDS_ON
        BlinkTextItems[BlinkNo].TextBoxScrollPos = CurItem.TextBoxScrollPos;
#endif
      }
      else
        BlinkNo = -1;
#endif
#endif

      YPos = Y1;
#ifdef GuiConst_TEXTBOX_FIELDS_ON
      if (BlindLinesAtTop < 0)
        BlindLinesAtTop = 0;
      YPos -=
         CurItem.TextBoxScrollPos - (CurItem.TextBoxLineDist * BlindLinesAtTop);
#endif

#ifdef GuiConst_AVRGCC_COMPILER
      N = pgm_read_byte(&CurFont->YSize) +
          (LineCnt - 1) * CurItem.TextBoxLineDist;
#else
      N = CurFont->YSize + (LineCnt - 1) * CurItem.TextBoxLineDist;
#endif
      switch (CurItem.TextBoxVertAlignment)
      {
        case GuiLib_ALIGN_CENTER:
          YPos += (Y2 - Y1 + 1 - N) / 2;
          break;

        case GuiLib_ALIGN_RIGHT:
          YPos += Y2 - Y1 + 1 - N;
          break;
      }

      for (N = 0; N < LineCnt; N++)
      {
        if (TextCharLineEnd[N] >= TextCharLineStart[N])
        {
          XPos = X1;

          LineLen = CalcCharsWidth(TextCharLineStart[N],
                                   TextCharLineEnd[N],
                                   &TextXOfs,
                                   &XStart,
                                   &XEnd);
          switch (CurItem.TextBoxHorzAlignment)
          {
            case GuiLib_ALIGN_CENTER:
              XPos += (PixWidth - LineLen) / 2;
              break;

            case GuiLib_ALIGN_RIGHT:
              XPos += PixWidth - LineLen;
              break;
          }

          if (CurItem.BitFlags & GuiLib_BITFLAG_UNDERLINE)
#ifdef GuiConst_AVRGCC_COMPILER
            GuiLib_FillBox(XPos, YPos + pgm_read_byte(&CurFont->Underline1),
               XPos + LineLen - 1, YPos + pgm_read_byte(&CurFont->Underline2),
               ForeColor);
#else
            GuiLib_FillBox(XPos, YPos + CurFont->Underline1,
               XPos + LineLen - 1, YPos + CurFont->Underline2, ForeColor);
#endif
          XPos -= XStart;
          if ((YPos + CurFont->YSize >= CY1) && (YPos <= CY2))
            for (P = TextCharLineStart[N]; P <= TextCharLineEnd[N]; P++)
#ifdef GuiConst_REMOTE_FONT_DATA
              DrawChar(
                 XPos + TextXOfs[P], YPos, CurFont, TextCharNdx[P], ForeColor);
#else
                   DrawChar(XPos + TextXOfs[P], YPos, CurFont, TextCharPtrAry[P],
                 ForeColor);
#endif
        }
        YPos += CurItem.TextBoxLineDist;
      }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
      GuiLib_SetClipping(RemClipX1, RemClipY1, RemClipX2, RemClipY2);
#endif
    }
    GuiLib_MarkDisplayBoxRepaint(X1, Y1, X2, Y2);
  }
}
#endif

//------------------------------------------------------------------------------
static GuiConst_INT32S ReadVar(
   void *VarPtr,
   GuiConst_INT8U VarType)
{
#ifdef GuiConst_FLOAT_SUPPORT_ON
  GuiConst_INT32S tmp;
  GuiConst_INT8U i;
  double dfactor;
  double dx;
  float ffactor;
  float fx;
#endif

  if (VarPtr == 0)
    return (0);
  else
  {
    if (CurItem.FormatFormat == GuiLib_FORMAT_EXP)
    {
#ifdef GuiConst_FLOAT_SUPPORT_ON
      if ((VarType == GuiLib_VAR_FLOAT) || (VarType == GuiLib_VAR_DOUBLE))
        VarExponent = 0;
      else
#endif
        CurItem.FormatFormat = GuiLib_FORMAT_DEC;
    }
    switch (VarType)
    {
      case GuiLib_VAR_BOOL:
        if (*(GuiConst_INT8U *) VarPtr == 0)
          return (0);
        else
          return (1);

      case GuiLib_VAR_UNSIGNED_CHAR:
        return (*(GuiConst_INT8U *) VarPtr);

      case GuiLib_VAR_SIGNED_CHAR:
        return (*(GuiConst_INT8S *) VarPtr);

      case GuiLib_VAR_UNSIGNED_INT:
        return (*(GuiConst_INT16U *) VarPtr);

      case GuiLib_VAR_SIGNED_INT:
        return (*(GuiConst_INT16S *) VarPtr);

      case GuiLib_VAR_UNSIGNED_LONG:
        return (*(GuiConst_INT32U *) VarPtr);

      case GuiLib_VAR_SIGNED_LONG:
        return (*(GuiConst_INT32S *) VarPtr);

      case GuiLib_VAR_FLOAT:
#ifdef GuiConst_FLOAT_SUPPORT_ON
        fx = *(float*) VarPtr;
        if (fx < 0)
          fx = -fx;
        if ((CurItem.FormatFormat == GuiLib_FORMAT_EXP) && (fx != 0))
        {
          while (fx > 10)
          {
            fx /= 10;
            VarExponent++;
          }
          while (fx < 1)
          {
            fx *= 10;
            VarExponent--;
          }
        }
        if ((CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
            (CurItem.FormatFormat == GuiLib_FORMAT_EXP))
        {
          ffactor=1.0;
          for (i = 0; i < CurItem.FormatDecimals ; i++)
            ffactor *= 10.0;
          tmp = (GuiConst_INT32S) (fx * ffactor);
          ffactor *= 10.0;
          if (((GuiConst_INT32S) (fx * ffactor) - (tmp * 10)) >= 5)
            tmp++;
          if (*(float*) VarPtr < 0)
            tmp = -tmp;
        }
        else
          tmp = (GuiConst_INT32S) *(float*) VarPtr;
        return (tmp);
#else
        return (0);
#endif

      case GuiLib_VAR_DOUBLE:
#ifdef GuiConst_FLOAT_SUPPORT_ON
        dx = *(double*) VarPtr;
        if (dx < 0)
          dx = -dx;
        if ((CurItem.FormatFormat == GuiLib_FORMAT_EXP) && (dx != 0))
        {
          while (dx > 10)
          {
            dx /= 10;
            VarExponent++;
          }
          while (dx < 1)
          {
            dx *= 10;
            VarExponent--;
          }
        }
        if ((CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
            (CurItem.FormatFormat == GuiLib_FORMAT_EXP))
        {
          dfactor=1.0;
          for (i = 0; i < CurItem.FormatDecimals ; i++)
            dfactor *= 10.0;
          tmp = (GuiConst_INT32S) (dx * dfactor);
          dfactor *= 10.0;
          if (((GuiConst_INT32S) (dx * dfactor) - (tmp * 10)) >= 5)
            tmp++;
          if (*(double*) VarPtr < 0)
            tmp = -tmp;
        }
        else
          tmp = (GuiConst_INT32S) dx;
        return (tmp);
#else
        return (0);
#endif

      default:
        return (0);
    }
  }
}

//------------------------------------------------------------------------------
static GuiConst_INT8U DataNumStr(
   GuiConst_INT32S DataValue,
   GuiConst_INT8U VarType)
{
  GuiConst_CHAR *S1;
  GuiConst_INT8U StrLen, L;
  GuiConst_INT16S I, N, P;
  GuiConst_INT8U Offset;
  GuiConst_INT8U Sign;
  GuiConst_INT8U ShowSign;
  GuiConst_INT8U ZeroPadding;
  GuiConst_INT8U TrailingZeros;
  GuiConst_INT8U ThousandsSeparator;
  GuiConst_INT8U Time;
  GuiConst_INT16S TT1, TT2, TT3;
  GuiConst_INT8U am;
  GuiConst_INT8U DecimalsPos;
#ifdef GuiConst_FLOAT_SUPPORT_ON
  GuiConst_CHAR ExponentStr[5];
#endif

  VarNumTextStr[0] = 0;

  Sign = 0;
  switch (VarType)
  {
    case GuiLib_VAR_BOOL:
    case GuiLib_VAR_UNSIGNED_CHAR:
      if (DataValue > 255)
        DataValue = 255;
      else if (DataValue < 0)
        DataValue = 0;
      break;

    case GuiLib_VAR_SIGNED_CHAR:
      if (DataValue > 127)
        DataValue = 127;
      else if (DataValue < -128)
        DataValue = -128;
      if (DataValue < 0)
        Sign = 1;
      break;

    case GuiLib_VAR_UNSIGNED_INT:
      if (DataValue > 65535)
        DataValue = 65535;
      else if (DataValue < 0)
        DataValue = 0;
      break;

    case GuiLib_VAR_SIGNED_INT:
      if (DataValue > 32767)
        DataValue = 32767;
      else if (DataValue < -32768)
        DataValue = -32768;
      if (DataValue < 0)
        Sign = 1;
      break;

    case GuiLib_VAR_SIGNED_LONG:
    case GuiLib_VAR_FLOAT:
    case GuiLib_VAR_DOUBLE:
      if (DataValue < 0)
        Sign = 1;
      break;
  }

  if ((CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
      (CurItem.FormatFormat == GuiLib_FORMAT_EXP))
    DecimalsPos = CurItem.FormatDecimals;
  else
    DecimalsPos = 0;
  Time = ((CurItem.FormatFormat == GuiLib_FORMAT_TIME_MMSS) ||
          (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_24) ||
          (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_24) ||
          (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_ampm) ||
          (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_ampm) ||
          (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_AMPM) ||
          (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_AMPM));
  if ((CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
      (CurItem.FormatFormat == GuiLib_FORMAT_EXP))
    ShowSign = ((CurItem.BitFlags & GuiLib_BITFLAG_FORMATSHOWSIGN) > 0);
  else
  {
    Sign = 0;
    ShowSign = 0;
  }
  if (CurItem.FormatAlignment == GuiLib_FORMAT_ALIGNMENT_RIGHT)
    ZeroPadding = ((CurItem.BitFlags & GuiLib_BITFLAG_FORMATZEROPADDING) > 0);
  else
    ZeroPadding = 0;
  if ((CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
      (CurItem.FormatFormat == GuiLib_FORMAT_EXP))
    TrailingZeros =
       ((CurItem.BitFlags & GuiLib_BITFLAG_FORMATTRAILINGZEROS) > 0);
  else
    TrailingZeros = 0;
  if ((CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
      (CurItem.FormatFormat == GuiLib_FORMAT_HEX))
    ThousandsSeparator =
       ((CurItem.BitFlags & GuiLib_BITFLAG_FORMATTHOUSANDSSEP) > 0);
  else
    ThousandsSeparator = 0;

  if ((CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
      (CurItem.FormatFormat == GuiLib_FORMAT_EXP))
  {
    if (Sign)
      ConvertIntToStr(-DataValue, VarNumTextStr, 10);
    else
      ConvertIntToStr(DataValue, VarNumTextStr, 10);
  }

  else if (CurItem.FormatFormat == GuiLib_FORMAT_HEX)
  {
    ConvertIntToStr(DataValue, VarNumTextStr, 16);
    S1 = VarNumTextStr;
    StrLen = 0;
    while (*S1 != 0)
    {
      if ((*S1 >= 'a') && (*S1 <= 'f'))
        *S1 -= 32;
      S1++;
      StrLen++;
    }
    if (DataValue < 0)
    {
      while ((StrLen > 1) && (StrLen >= CurItem.FormatFieldWidth))
      {
        if (VarNumTextStr[0] == 'F')
        {
          for (N = 0; N < StrLen; N++)
            VarNumTextStr[N] = VarNumTextStr[N + 1];
          VarNumTextStr[StrLen] = 0;
          StrLen--;
        }
        else
          break;
      }

    }
    if (StrLen >= GuiConst_MAX_VARNUM_TEXT_LEN)
      return (0);

#ifdef GuiConst_CODEVISION_COMPILER
    strcatf(VarNumTextStr, "h");
#else
    strcat(VarNumTextStr, "h");
#endif
  }

  else if (Time)
  {
    if ((CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_24) ||
        (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_ampm) ||
        (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_AMPM))
    {
      DataValue %= 360000;
      TT1 = DataValue / 3600;
      TT2 = (DataValue % 3600) / 60;
      TT3 = DataValue % 60;
    }
    else
    {
      DataValue %= 6000;
      TT1 = DataValue / 60;
      TT2 = DataValue % 60;
      TT3 = -1;
    }

    if ((CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_ampm) ||
        (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_ampm) ||
        (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_AMPM) ||
        (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_AMPM))
    {
      am = (TT1 < 12);
      TT1 %= 12;
      if (TT1 == 0)
        TT1 = 12;
    }
    else
      am = 0;

    if (ZeroPadding && (TT1 < 10))
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(VarNumTextStr, "0");
#else
      strcat(VarNumTextStr, "0");
#endif
    ConvertIntToStr(TT1, VarNumTextStr + strlen(VarNumTextStr), 10);

#ifdef GuiConst_CODEVISION_COMPILER
    strcatf(VarNumTextStr, ":");
#else
    strcat(VarNumTextStr, ":");
#endif

    if (TT2 < 10)
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(VarNumTextStr, "0");
#else
      strcat(VarNumTextStr, "0");
#endif
    ConvertIntToStr(TT2, VarNumTextStr + strlen(VarNumTextStr), 10);

    if (TT3 >= 0)
    {
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(VarNumTextStr, ":");
#else
      strcat(VarNumTextStr, ":");
#endif

      if (TT3 < 10)
#ifdef GuiConst_CODEVISION_COMPILER
        strcatf(VarNumTextStr, "0");
#else
        strcat(VarNumTextStr, "0");
#endif
      ConvertIntToStr(TT3, VarNumTextStr + strlen(VarNumTextStr), 10);
    }

    if ((CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_ampm) ||
        (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_ampm))
    {
      if (am)
#ifdef GuiConst_CODEVISION_COMPILER
        strcatf(VarNumTextStr, "am");
#else
        strcat(VarNumTextStr, "am");
#endif
      else
#ifdef GuiConst_CODEVISION_COMPILER
        strcatf(VarNumTextStr, "pm");
#else
        strcat(VarNumTextStr, "pm");
#endif
    }
    else if ((CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_AMPM) ||
             (CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_AMPM))
    {
      if (am)
#ifdef GuiConst_CODEVISION_COMPILER
        strcatf(VarNumTextStr, "AM");
#else
        strcat(VarNumTextStr, "AM");
#endif
      else
#ifdef GuiConst_CODEVISION_COMPILER
        strcatf(VarNumTextStr, "PM");
#else
        strcat(VarNumTextStr, "PM");
#endif
    }
  }
  else
    return (0);

  StrLen = 0;
  S1 = VarNumTextStr;
  while (*S1 != 0)
  {
    StrLen++;
    S1++;
  }

  if (DecimalsPos > 0)
  {
    L = DecimalsPos + 1;
    if (StrLen < L)
    {
      if (L > GuiConst_MAX_VARNUM_TEXT_LEN)
        return (0);
      for (N = 1; N <= StrLen; N++)
        VarNumTextStr[L - N] = VarNumTextStr[StrLen - N];
      for (N = 0; N < L - StrLen; N++)
        VarNumTextStr[N] = '0';

      StrLen = L;
    }

    if ((!TrailingZeros) && (!Time) && (VarType != GuiLib_VAR_BOOL))
    {
      L = StrLen;
      for (N = L - 1; N > L - CurItem.FormatDecimals; N--)
        if (VarNumTextStr[N] == '0')
        {
          DecimalsPos--;
          StrLen--;
        }
        else
          break;
    }

    if (StrLen >= GuiConst_MAX_VARNUM_TEXT_LEN)
      return (0);
    P = StrLen - DecimalsPos;
    for (N = StrLen; N > P; N--)
      VarNumTextStr[N] = VarNumTextStr[N - 1];
#ifdef GuiConst_AVRGCC_COMPILER
    if (pgm_read_byte(&GuiFont_DecimalChar[GuiLib_LanguageIndex]) == GuiLib_DECIMAL_PERIOD)
#else
    if (GuiFont_DecimalChar[GuiLib_LanguageIndex] == GuiLib_DECIMAL_PERIOD)
#endif
      VarNumTextStr[P] = '.';
    else
      VarNumTextStr[P] = ',';
    StrLen++;
    VarNumTextStr[StrLen] = 0;
  }
  else
    P = StrLen;

  if (ThousandsSeparator)
  {
    I = 0;
    while (P > 0)
    {
      if ((I > 0) && (I % 3 ==0 ))
      {
        for (N = StrLen; N > P; N--)
          VarNumTextStr[N] = VarNumTextStr[N - 1];
#ifdef GuiConst_AVRGCC_COMPILER
        if (pgm_read_byte(&GuiFont_DecimalChar[GuiLib_LanguageIndex]) == GuiLib_DECIMAL_PERIOD)
#else
        if (GuiFont_DecimalChar[GuiLib_LanguageIndex] == GuiLib_DECIMAL_PERIOD)
#endif
          VarNumTextStr[P] = ',';
        else
          VarNumTextStr[P] = '.';
        StrLen++;
        if (StrLen >= GuiConst_MAX_VARNUM_TEXT_LEN)
          return (0);
        VarNumTextStr[StrLen] = 0;
      }
      I++;
      P--;
    }
  }

  if (Sign || ShowSign)
  {
    if (StrLen > GuiConst_MAX_VARNUM_TEXT_LEN)
      return (0);
    for (N = StrLen; N >= 1; N--)
      VarNumTextStr[N] = VarNumTextStr[N - 1];
    if (Sign)
      VarNumTextStr[0] = '-';
    else
      VarNumTextStr[0] = '+';
    StrLen++;
    VarNumTextStr[StrLen] = 0;
  }

#ifdef GuiConst_FLOAT_SUPPORT_ON
  if (CurItem.FormatFormat == GuiLib_FORMAT_EXP)
  {
    N = VarExponent;
    if (N < 0)
      N = -N;
    ConvertIntToStr(N, ExponentStr, 10);
    S1 = ExponentStr;
    N = 0;
    while (*S1 != 0)
    {
      S1++;
      N++;
    }
    if (N == 1)
      I = 2;
    else
      I = N;
    if (StrLen + 2 + I >= GuiConst_MAX_VARNUM_TEXT_LEN)
      return (0);
#ifdef GuiConst_CODEVISION_COMPILER
    strcatf(VarNumTextStr, "E");
#else
    strcat(VarNumTextStr, "E");
#endif
    StrLen++;
    if (VarExponent >= 0)
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(VarNumTextStr, "+");
#else
      strcat(VarNumTextStr, "+");
#endif
    else
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(VarNumTextStr, "-");
#else
      strcat(VarNumTextStr, "-");
#endif
    StrLen++;
    if (N == 1)
    {
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(VarNumTextStr, "0");
#else
      strcat(VarNumTextStr, "0");
#endif
      StrLen++;
    }
    strcat(VarNumTextStr, ExponentStr);
    StrLen += N;
  }
#endif

  if (CurItem.FormatFieldWidth > 0)
  {
    if (StrLen > CurItem.FormatFieldWidth)
    {
      for (N = 0; N < CurItem.FormatFieldWidth; N++)
        VarNumTextStr[N] = '-';
    }
    else
    {
      if (CurItem.FormatFieldWidth > GuiConst_MAX_VARNUM_TEXT_LEN)
        return (0);
      if (ZeroPadding && (!Time))
      {
        if ((VarNumTextStr[0] == '-') || (VarNumTextStr[0] == '+'))
          Offset = 1;
        else
          Offset = 0;
        for (N = 1; N <= StrLen - Offset; N++)
          VarNumTextStr[CurItem.FormatFieldWidth - N] =
            VarNumTextStr[StrLen - N];
        for (N = 0; N < CurItem.FormatFieldWidth - StrLen; N++)
          VarNumTextStr[N + Offset] = '0';
      }
      else if (CurItem.FormatAlignment == GuiLib_FORMAT_ALIGNMENT_LEFT)
      {
        for (N = StrLen; N < CurItem.FormatFieldWidth; N++)
          VarNumTextStr[N] = ' ';
      }
      else if (CurItem.FormatAlignment == GuiLib_FORMAT_ALIGNMENT_CENTER)
      {
        Offset = (CurItem.FormatFieldWidth - StrLen) / 2;
        if (Offset > 0)
        {
          for (N = StrLen - 1; N >= 0; N--)
            VarNumTextStr[N + Offset] = VarNumTextStr[N];
          for (N = 0; N < Offset; N++)
            VarNumTextStr[N] = ' ';
        }
        Offset = CurItem.FormatFieldWidth - StrLen - Offset;
        if (Offset > 0)
          for (N = CurItem.FormatFieldWidth - Offset;
               N < CurItem.FormatFieldWidth; N++)
            VarNumTextStr[N] = ' ';
      }
      else if (CurItem.FormatAlignment == GuiLib_FORMAT_ALIGNMENT_RIGHT)
      {
        for (N = 1; N <= StrLen; N++)
          VarNumTextStr[CurItem.FormatFieldWidth - N] =
            VarNumTextStr[StrLen - N];
        for (N = 0; N < CurItem.FormatFieldWidth - StrLen; N++)
          VarNumTextStr[N] = ' ';
      }
    }
    VarNumTextStr[CurItem.FormatFieldWidth] = 0;
    return (CurItem.FormatFieldWidth);
  }
  else
  {
    VarNumTextStr[StrLen] = 0;
    return (StrLen);
  }
}

//------------------------------------------------------------------------------
static GuiConst_INT8U GetItemByte(
   GuiConst_INT8U**ItemDataPtrPtr)
{
  GuiConst_INT8U D;
#ifdef GuiConst_AVRGCC_COMPILER
  D = pgm_read_byte(*ItemDataPtrPtr);
#else
#ifdef GuiConst_AVR_COMPILER_FLASH_RAM
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
#else
#ifdef GuiConst_ICC_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
#else
#ifdef GuiConst_CODEVISION_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr;
  ItemFlashPtr = (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
#else
  D = **ItemDataPtrPtr;
#endif
#endif
#endif
#endif
  (*ItemDataPtrPtr)++;
  ItemDataBufCnt++;
  return (D);
}

//------------------------------------------------------------------------------
static GuiConst_INT16S GetItemWord(
   GuiConst_INT8U**ItemDataPtrPtr)
{
  GuiConst_INT16U D;

#ifdef GuiConst_AVRGCC_COMPILER
  D = pgm_read_byte(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x0100 * (GuiConst_INT16U)pgm_read_byte(*ItemDataPtrPtr);
#else
#ifdef GuiConst_AVR_COMPILER_FLASH_RAM
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x0100 * (*ItemFlashPtr);
#else
#ifdef GuiConst_ICC_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x0100 * (*ItemFlashPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr;
  ItemFlashPtr = (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x0100 * (*ItemFlashPtr);
#else
  D = **ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x0100 * (GuiConst_INT16U)**ItemDataPtrPtr;
#endif
#endif
#endif
#endif
  (*ItemDataPtrPtr)++;
  ItemDataBufCnt += 2;
  return ((GuiConst_INT16S) D);
}

//------------------------------------------------------------------------------
#ifdef GuiLib_COLOR_BYTESIZE_3
static GuiConst_INT32S GetItemTriple(
   GuiConst_INT8U**ItemDataPtrPtr)
{
  GuiConst_INT32U D;

#ifdef GuiConst_AVRGCC_COMPILER
  D = pgm_read_byte(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x00000100 * (GuiConst_INT32U)pgm_read_byte(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x00010000 * (GuiConst_INT32U)pgm_read_byte(*ItemDataPtrPtr);
#else
#ifdef GuiConst_AVR_COMPILER_FLASH_RAM
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
#else
#ifdef GuiConst_ICC_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr;
  ItemFlashPtr = (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
#else
  D = **ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x00000100 * (GuiConst_INT32U)**ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x00010000 * (GuiConst_INT32U)**ItemDataPtrPtr;
#endif
#endif
#endif
#endif
  (*ItemDataPtrPtr)++;
  ItemDataBufCnt += 3;
  return ((GuiConst_INT32S) D);
}
#endif

//------------------------------------------------------------------------------
#ifdef GuiConst_ITEM_GRAPH_INUSE
static GuiConst_INT32S GetItemLong(
   GuiConst_INT8U**ItemDataPtrPtr)
{
  GuiConst_INT32U D;

#ifdef GuiConst_AVRGCC_COMPILER
  D = pgm_read_byte(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x00000100 * (GuiConst_INT32U)pgm_read_byte(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x00010000 * (GuiConst_INT32U)pgm_read_byte(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x01000000 * (GuiConst_INT32U)pgm_read_byte(*ItemDataPtrPtr);
#else
#ifdef GuiConst_AVR_COMPILER_FLASH_RAM
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x01000000 * (*ItemFlashPtr);
#else
#ifdef GuiConst_ICC_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x01000000 * (*ItemFlashPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr;
  ItemFlashPtr = (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x01000000 * (*ItemFlashPtr);
#else
  D = **ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x00000100 * (GuiConst_INT32U)**ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x00010000 * (GuiConst_INT32U)**ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x01000000 * (GuiConst_INT32U)**ItemDataPtrPtr;
#endif
#endif
#endif
#endif
  (*ItemDataPtrPtr)++;
  ItemDataBufCnt += 4;
  return ((GuiConst_INT32S) D);
}
#endif

#ifdef GuiConst_REMOTE_STRUCT_DATA
//------------------------------------------------------------------------------
static GuiLib_StructPtr GetRemoteStructData(
   GuiConst_INT32S StructIndex)
{
  if (StructIndex != CurRemoteStruct)
  {
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiStruct_StructPtrList[StructIndex],
       (GuiConst_INT32U PrefixRom)GuiStruct_StructPtrList[StructIndex + 1] -
       (GuiConst_INT32U PrefixRom)GuiStruct_StructPtrList[StructIndex],
       GuiLib_RemoteStructBuffer);
    CurRemoteStruct = StructIndex;
  }
  return((GuiLib_StructPtr)&GuiLib_RemoteStructBuffer[0]);
}

#ifdef GuiConst_REMOTE_TEXT_DATA
//------------------------------------------------------------------------------
static GuiConst_INT16U GetRemoteText(
   GuiConst_INT16U TextIndex)
{
  GuiConst_INT32S TextOfs;

  if (TextIndex != CurRemoteText)
  {
    if (RemoteTextTableOfs == -1)
      GuiLib_RemoteTextReadBlock(0, 4, &RemoteTextTableOfs);

    GuiLib_RemoteTextReadBlock(RemoteTextTableOfs + 6 * TextIndex, 4, &TextOfs);
    GuiLib_RemoteTextReadBlock(
       RemoteTextTableOfs + 6 * TextIndex + 4, 2, &RemoteTextLen);

#ifdef GuiConst_CHARMODE_ANSI
    GuiLib_RemoteTextReadBlock(TextOfs, RemoteTextLen, GuiLib_RemoteTextBuffer);
#else
    GuiLib_RemoteTextReadBlock(TextOfs, 2 * RemoteTextLen, GuiLib_RemoteTextBuffer);
#endif
    CurRemoteText = TextIndex;
  }
  return(RemoteTextLen);
}
#endif

#endif

#ifdef GuiConst_CHARMODE_UNICODE
//------------------------------------------------------------------------------
static void ExtractUnicodeString(
   GuiConst_INT8U *ItemTextPtr)
{
  GuiConst_INT16U P;

  for (P = 0; P <= CurItem.TextLength; P++)
    UnicodeTextBuf[P] = GetItemWord(&ItemTextPtr);
}
#endif

//------------------------------------------------------------------------------
static void SetCurFont(
   GuiConst_INT8U FontIndex)
{
  if (FontIndex >= GuiFont_FontCnt)
    FontIndex = 0;

#ifdef GuiConst_AVRGCC_COMPILER
  CurFont = (GuiLib_FontRecPtr)pgm_read_word(&GuiFont_FontList[FontIndex]);
#else
  CurFont = GuiFont_FontList[FontIndex];
#endif
}

#ifdef GuiConst_BITMAP_SUPPORT_ON
//------------------------------------------------------------------------------
static void UpdateBackgroundBitmap(void)
{
  GuiConst_INT8U BackgroundOn;
  GuiConst_INT16U BackgroundX1, BackgroundY1, BackgroundX2, BackgroundY2;
  GuiConst_INT16S N;

  for (N = 0; N < GuiConst_MAX_BACKGROUND_BITMAPS; N++)
    if (BackgrBitmapAry[N].InUse)
    {
      BackgroundOn = 0;
      switch (CurItem.ItemType)
      {
        case GuiLib_ITEM_TEXT:
        case GuiLib_ITEM_VAR:
          BackgroundOn = 1;
          if (CurItem.BackBoxSizeX == 0)
          {
            BackgroundX1 = CurItem.DrawnX1;
            BackgroundY1 = CurItem.DrawnY1;
            BackgroundX2 = CurItem.DrawnX2;
            BackgroundY2 = CurItem.DrawnY2;
          }
          else
          {
            SetCurFont(CurItem.FontIndex);
            SetBackBox();
            BackgroundX1 = BbX1;
            BackgroundY1 = BbY1;
            BackgroundX2 = BbX2;
            BackgroundY2 = BbY2;
          }
          break;

        case GuiLib_ITEM_STRUCTURE:
        case GuiLib_ITEM_STRUCTARRAY:
          if (CurItem.Drawn)
          {
            BackgroundOn = 1;
            BackgroundX1 = CurItem.DrawnX1;
            BackgroundY1 = CurItem.DrawnY1;
            BackgroundX2 = CurItem.DrawnX2;
            BackgroundY2 = CurItem.DrawnY2;
          }
          break;

        case GuiLib_ITEM_TEXTBLOCK:
        case GuiLib_ITEM_VARBLOCK:
          if (CurItem.BitFlags & GuiLib_BITFLAG_TRANSPARENT)
          {
            BackgroundOn = 1;
            SetBackColorBox(CurItem.X1, CurItem.Y1, CurItem.X2, CurItem.Y2);
            BackgroundX1 = BbX1;
            BackgroundY1 = BbY1;
            BackgroundX2 = BbX2;
            BackgroundY2 = BbY2;
          }
          break;
      }
      if (BackgroundOn)
        GuiLib_ShowBitmapArea(BackgrBitmapAry[N].Index,
           BackgrBitmapAry[N].X, BackgrBitmapAry[N].Y,
           BackgroundX1, BackgroundY1, BackgroundX2, BackgroundY2, -1);
    }
}
#endif

//------------------------------------------------------------------------------
static void DrawItem(
   GuiConst_INT8U ColorInvert)
{
  GuiConst_INT16U StructToCallIndex;
  GuiLib_StructPtr StructToCall;
  GuiConst_INT8U RemDrawn;
  GuiConst_INT16S RemDrawnX1, RemDrawnY1, RemDrawnX2, RemDrawnY2;
  GuiConst_INT8U RemBackBox;
  GuiConst_INT16S RemBbX1, RemBbX2;
  GuiConst_INT16S RemBbY1, RemBbY2;
  GuiConst_INT32S VarValue;
  GuiConst_INT16U I;
  GuiConst_INT16U StrLen;
#ifdef GuiConst_CURSOR_SUPPORT_ON
  GuiConst_INT8U IsCursorField;
  GuiConst_INT8U FoundActiveCursorField;
#endif
  GuiConst_INTCOLOR ForeColor;
  GuiConst_INTCOLOR BackColor;
  GuiConst_INT8U BackColorTransp;
  GuiConst_INT32S BackColor2;
#ifdef GuiConst_CHARMODE_ANSI
  GuiConst_INT16S LangIndex;
#else
  GuiConst_INT16U P;
#endif
  GuiConst_TEXT *CharPtr;
#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
  GuiConst_INT16S AutoRedrawFieldNo;
  GuiConst_INT16U AutoRedrawVarSize;
  void *AutoRedrawVarPtr;
#endif
  GuiConst_INT16S X1, X2, Y1, Y2;
#ifdef GuiConst_REMOTE_STRUCT_DATA
  GuiConst_INT32S RemRemoteStruct;
#endif
  GuiConst_INT16S RemAutoRedrawSaveIndex;

#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
  AutoRedrawFieldNo = 0;
  AutoRedrawVarSize = 0;
#endif

#ifdef GuiConst_REL_COORD_ORIGO_INUSE
  if (!InitialDrawing)
  {
    CoordOrigoX = CurItem.CoordOrigoX;
    CoordOrigoY = CurItem.CoordOrigoY;
  }
#endif
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (!InitialDrawing && DisplayWriting)
    GuiLib_SetClipping(CurItem.ClipRectX1, CurItem.ClipRectY1,
                       CurItem.ClipRectX2, CurItem.ClipRectY2);
#endif

  SetCurFont(CurItem.FontIndex);

#ifdef GuiConst_CURSOR_SUPPORT_ON
  IsCursorField = (CursorInUse && (CurItem.CursorFieldNo >= 0) &&
                  (CurItem.CursorFieldNo < GuiConst_CURSOR_FIELDS_MAX));
  FoundActiveCursorField =
     (IsCursorField && (GuiLib_ActiveCursorFieldNo == CurItem.CursorFieldNo));
  if (FoundActiveCursorField)
    CursorActiveFieldFound = 1;
#endif

  if (InitialDrawing)
  {
#ifdef GuiConst_CURSOR_SUPPORT_ON
    if ((CurItem.CursorFieldNo >= 0) && (CursorFieldFound == -1))
      CursorFieldFound = CurItem.CursorFieldNo;

    if (IsCursorField)
    {
      CurItem.CursorFieldLevel++;
      CurItem.AutoRedrawCursorFieldNo = CurItem.CursorFieldNo;

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
      if (NextScrollLineReading)
      {
        CurItem.BitFlags |= GuiLib_BITFLAG_FIELDSCROLLBOX;
        CurItem.CursorScrollBoxIndex = GlobalScrollBoxIndex;
        CursorFieldsInScrollLine = 1;
      }
      else
#endif
        CurItem.BitFlags &= ~GuiLib_BITFLAG_FIELDSCROLLBOX;
#ifdef GuiConst_SCROLL_SUPPORT_ON
      if (InsideScrollLine)
      {
        CurItem.BitFlags |= GuiLib_BITFLAG_FIELDSCROLLS;
        CursorFieldsInScrollLine = 1;
      }
      else
#endif
        CurItem.BitFlags &= ~GuiLib_BITFLAG_FIELDSCROLLS;

#ifndef GuiConst_CURSOR_FIELDS_OFF
      memcpy(&CursorItems[CurItem.CursorFieldNo], &CurItem,
         sizeof(GuiLib_ItemRec));
#endif
    }
#endif

    if ((CurItem.BitFlags & GuiLib_BITFLAG_AUTOREDRAWFIELD) &&
        (GuiLib_AutoRedrawFieldNo < GuiConst_AUTOREDRAW_FIELDS_MAX))
    {
#ifdef GuiConst_CURSOR_SUPPORT_ON
      if (CurItem.CursorFieldLevel == 0)
        CurItem.AutoRedrawCursorFieldNo = -1;
#endif

#ifdef GuiConst_SCROLL_SUPPORT_ON
      if (InsideScrollLine)
        CurItem.BitFlags |= GuiLib_BITFLAG_FIELDSCROLLS;
      else
        CurItem.BitFlags &= ~GuiLib_BITFLAG_FIELDSCROLLS;
#endif
      memcpy(&AutoRedrawItems[GuiLib_AutoRedrawFieldNo], &CurItem,
         sizeof(GuiLib_ItemRec));
      AutoRedrawSaveIndex = GuiLib_AutoRedrawFieldNo;
      AutoRedrawLevel[GuiLib_AutoRedrawFieldNo] = DrawingLevel;
#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
      AutoRedrawFieldNo = GuiLib_AutoRedrawFieldNo;
      if (CurItem.VarPtr != 0)
      {
        switch (CurItem.VarType)
        {
          case GuiLib_VAR_BOOL:
          case GuiLib_VAR_UNSIGNED_CHAR:
          case GuiLib_VAR_SIGNED_CHAR:
            AutoRedrawSize[AutoRedrawFieldNo] = 1;
            break;

          case GuiLib_VAR_UNSIGNED_INT:
          case GuiLib_VAR_SIGNED_INT:
            AutoRedrawSize[AutoRedrawFieldNo] = 2;
            break;

          case GuiLib_VAR_UNSIGNED_LONG:
          case GuiLib_VAR_SIGNED_LONG:
            AutoRedrawSize[AutoRedrawFieldNo] = 4;
            break;

          case GuiLib_VAR_FLOAT:
            AutoRedrawSize[AutoRedrawFieldNo] = 4;
            break;

          case GuiLib_VAR_DOUBLE:
            AutoRedrawSize[AutoRedrawFieldNo] = 8;
            break;

          case GuiLib_VAR_STRING:
            AutoRedrawSize[AutoRedrawFieldNo] =
#ifdef GuiConst_CHARMODE_ANSI
               GuiConst_AUTOREDRAW_MAX_VAR_SIZE;
#else
               2 * GuiConst_AUTOREDRAW_MAX_VAR_SIZE;
#endif
            break;

          default:
            AutoRedrawSize[AutoRedrawFieldNo] = 0;
        }
      }
      else
        AutoRedrawSize[AutoRedrawFieldNo] = 0;
#endif
      GuiLib_AutoRedrawFieldNo++;
    }
#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
    else
      AutoRedrawFieldNo = -1;
#endif
  }

  switch (ColorInvert)
  {
    case GuiLib_COL_INVERT_OFF:
      SwapColors = 0;
      break;

    case GuiLib_COL_INVERT_ON:
      SwapColors = 1;
      break;

    case GuiLib_COL_INVERT_IF_CURSOR:
#ifdef GuiConst_CURSOR_SUPPORT_ON
      if (FoundActiveCursorField)
        SwapColors = 1;
#else
      SwapColors = 0;
#endif
      break;
  }
  if (SwapColors)
  {
    ForeColor = CurItem.BarForeColor;
    BackColor = CurItem.BarBackColor;
    BackColorTransp = ((CurItem.BitFlags & GuiLib_BITFLAG_BARTRANSPARENT) > 0);
  }
  else
  {
    ForeColor = CurItem.ForeColor;
    BackColor = CurItem.BackColor;
    BackColorTransp = ((CurItem.BitFlags & GuiLib_BITFLAG_TRANSPARENT) > 0);
  }

#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
  if (InitialDrawing)
  {
    AutoRedrawVarSize = AutoRedrawSize[AutoRedrawFieldNo];
    AutoRedrawVarPtr = CurItem.VarPtr;
  }
#endif

  CurItem.Drawn = 0;
  switch (CurItem.ItemType)
  {
    case GuiLib_ITEM_CLEARAREA:
      if (DisplayWriting)
      {
        X1 = CurItem.X1;
        X2 = CurItem.X2;
        Y1 = CurItem.Y1;
        Y2 = CurItem.Y2;
        OrderCoord(&X1, &X2);
        OrderCoord(&Y1, &Y2);
        GuiLib_FillBox(X1, Y1, X2, Y2, BackColor);
        UpdateDrawLimits(X1, Y1, X2, Y2);
      }

      break;

    case GuiLib_ITEM_TEXT:
      if (DisplayWriting)
      {
        if (CurItem.BackBoxSizeX > 0)
          DrawBackBox(BackColor, BackColorTransp);

#ifdef GuiConst_CHARMODE_ANSI
        if (CurItem.BitFlags & GuiLib_BITFLAG_TRANSLATION)
          LangIndex = GuiLib_LanguageIndex;
        else
          LangIndex = 0;
        if (CurItem.CharSetSelector >= 0)
          SetLanguageCharSet(CurItem.CharSetSelector);
        else
  #ifdef GuiConst_AVRGCC_COMPILER
          SetLanguageCharSet(
             pgm_read_byte(&GuiFont_LanguageCharSets[LangIndex]));
  #else
          SetLanguageCharSet(GuiFont_LanguageCharSets[LangIndex]);
  #endif
#endif
#ifdef GuiConst_CHARMODE_ANSI
  #ifdef GuiConst_ICC_COMPILER
        DrawText((GuiConst_TEXT *)CurItem.TextPtr,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
        DrawText((GuiConst_TEXT *)CurItem.TextPtr,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #else
        DrawText((GuiConst_TEXT PrefixRom *)CurItem.TextPtr,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #endif
  #endif
#else
  #ifdef GuiConst_ICC_COMPILER
        ExtractUnicodeString((GuiConst_INT8U *)CurItem.TextPtr);
        DrawText((GuiConst_TEXT *)&UnicodeTextBuf,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
        ExtractUnicodeString((GuiConst_INT8U *)CurItem.TextPtr);
        DrawText((GuiConst_TEXT *)UnicodeTextBuf,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #else
        ExtractUnicodeString((GuiConst_INT8U PrefixRom *)CurItem.TextPtr);
        DrawText((GuiConst_TEXT *)&UnicodeTextBuf,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #endif
  #endif
#endif

        UpdateDrawLimits(FontWriteX1, FontWriteY1, FontWriteX2, FontWriteY2);
      }

      break;

    case GuiLib_ITEM_TEXTBLOCK:
      if (DisplayWriting)
      {
#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
#ifdef GuiConst_CHARMODE_ANSI
        if (CurItem.BitFlags & GuiLib_BITFLAG_TRANSLATION)
          LangIndex = GuiLib_LanguageIndex;
        else
          LangIndex = 0;
        if (CurItem.CharSetSelector >= 0)
          SetLanguageCharSet(CurItem.CharSetSelector);
        else
  #ifdef GuiConst_AVRGCC_COMPILER
          SetLanguageCharSet(pgm_read_byte(&GuiFont_LanguageCharSets[LangIndex]));
  #else
          SetLanguageCharSet(GuiFont_LanguageCharSets[LangIndex]);
  #endif

  #ifdef GuiConst_ICC_COMPILER
        DrawTextBlock((GuiConst_TEXT *)CurItem.TextPtr,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
        DrawTextBlock((GuiConst_TEXT *)CurItem.TextPtr,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #else
        DrawTextBlock((GuiConst_TEXT PrefixRom *)CurItem.TextPtr,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #endif
  #endif
#else
  #ifdef GuiConst_ICC_COMPILER
        ExtractUnicodeString((GuiConst_INT8U *)CurItem.TextPtr);
        DrawTextBlock((GuiConst_TEXT *)&UnicodeTextBuf,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
        ExtractUnicodeString((GuiConst_INT8U *)CurItem.TextPtr);
        DrawTextBlock((GuiConst_TEXT *)UnicodeTextBuf,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #else
        ExtractUnicodeString((GuiConst_INT8U PrefixRom *)CurItem.TextPtr);
        DrawTextBlock((GuiConst_TEXT *)&UnicodeTextBuf,
           CurItem.TextLength, ForeColor, BackColor, BackColorTransp);
  #endif
  #endif
#endif
#endif

        UpdateDrawLimits(CurItem.X1, CurItem.Y1, CurItem.X2, CurItem.Y2);
      }

      break;

    case GuiLib_ITEM_VAR:
      if (DisplayWriting)
      {
#ifdef GuiConst_AVRGCC_COMPILER
        displayVarNow = 1;
#endif
#ifdef GuiConst_ICC_COMPILER
        displayVarNow = 1;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
        displayVarNow = 1;
#endif

        if (CurItem.BackBoxSizeX > 0)
          DrawBackBox(BackColor, BackColorTransp);

        if (CurItem.VarPtr != 0)
        {
          if (CurItem.VarType == GuiLib_VAR_STRING)
          {
            CharPtr = (GuiConst_TEXT*)CurItem.VarPtr;
#ifdef GuiConst_CHARMODE_ANSI
            StrLen = strlen(CharPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
            StrLen = GuiLib_UnicodeStrLen((GuiConst_TEXT*)CharPtr);
#else
            StrLen = GuiLib_UnicodeStrLen(CharPtr);
#endif
#endif
#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
#ifdef GuiConst_CHARMODE_ANSI
            AutoRedrawVarSize = StrLen + 1;
#else
            AutoRedrawVarSize = 2 * (StrLen + 1);
#endif
#endif
          }
          else
          {
            VarValue = ReadVar(CurItem.VarPtr, CurItem.VarType);
            StrLen = DataNumStr(VarValue, CurItem.VarType);
#ifdef GuiConst_CHARMODE_ANSI
            CharPtr = (GuiConst_TEXT *) VarNumTextStr;
#else
            for (P = 0; P <= StrLen; P++)
              VarNumUnicodeTextStr[P] = VarNumTextStr[P];
            CharPtr = (GuiConst_TEXT *) VarNumUnicodeTextStr;
#endif
          }

#ifdef GuiConst_CHARMODE_ANSI
          if (CurItem.CharSetSelector >= 0)
            SetLanguageCharSet(CurItem.CharSetSelector);
          else
  #ifdef GuiConst_AVRGCC_COMPILER
            SetLanguageCharSet(
               pgm_read_byte(&GuiFont_LanguageCharSets[GuiLib_LanguageIndex]));
  #else
            SetLanguageCharSet(GuiFont_LanguageCharSets[GuiLib_LanguageIndex]);
  #endif
#endif

#ifdef GuiConst_CHARMODE_ANSI
          strcpy(AnsiTextBuf, CharPtr);
          DrawText(AnsiTextBuf, StrLen, ForeColor, BackColor, BackColorTransp);
#else
          GuiLib_UnicodeStrCpy(UnicodeTextBuf, CharPtr);
          DrawText(
             UnicodeTextBuf, StrLen, ForeColor, BackColor, BackColorTransp);
#endif
        }

#ifdef GuiConst_AVRGCC_COMPILER
        displayVarNow = 0;
#endif
#ifdef GuiConst_ICC_COMPILER
        displayVarNow = 0;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
        displayVarNow = 0;
#endif

        UpdateDrawLimits(FontWriteX1, FontWriteY1, FontWriteX2, FontWriteY2);
      }

      break;

    case GuiLib_ITEM_VARBLOCK:
      if (DisplayWriting)
      {
#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
#ifdef GuiConst_AVRGCC_COMPILER
        displayVarNow = 1;
#endif
#ifdef GuiConst_ICC_COMPILER
        displayVarNow = 1;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
        displayVarNow = 1;
#endif

        if (CurItem.VarPtr != 0)
        {
          if (CurItem.VarType == GuiLib_VAR_STRING)
          {
            CharPtr = (GuiConst_TEXT*)CurItem.VarPtr;
#ifdef GuiConst_CHARMODE_ANSI
            StrLen = strlen(CharPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
            StrLen = GuiLib_UnicodeStrLen((GuiConst_TEXT*)CharPtr);
#else
            StrLen = GuiLib_UnicodeStrLen(CharPtr);
#endif
#endif
#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
#ifdef GuiConst_CHARMODE_ANSI
            AutoRedrawVarSize = StrLen + 1;
#else
            AutoRedrawVarSize = 2 * (StrLen + 1);
#endif
#endif
          }
          else
          {
            VarValue = ReadVar(CurItem.VarPtr, CurItem.VarType);
            StrLen = DataNumStr(VarValue, CurItem.VarType);
#ifdef GuiConst_CHARMODE_ANSI
            CharPtr = (GuiConst_TEXT *) VarNumTextStr;
#else
            for (P = 0; P <= StrLen; P++)
              VarNumUnicodeTextStr[P] = VarNumTextStr[P];
            CharPtr = (GuiConst_TEXT *) VarNumUnicodeTextStr;
#endif
          }

#ifdef GuiConst_CHARMODE_ANSI
          if (CurItem.CharSetSelector >= 0)
            SetLanguageCharSet(CurItem.CharSetSelector);
          else
  #ifdef GuiConst_AVRGCC_COMPILER
            SetLanguageCharSet(
               pgm_read_byte(&GuiFont_LanguageCharSets[GuiLib_LanguageIndex]));
  #else
            SetLanguageCharSet(GuiFont_LanguageCharSets[GuiLib_LanguageIndex]);
  #endif
#endif

#ifdef GuiConst_CHARMODE_ANSI
          strcpy(AnsiTextBuf, CharPtr);
          DrawTextBlock(
             AnsiTextBuf, StrLen, ForeColor, BackColor, BackColorTransp);
#else
          GuiLib_UnicodeStrCpy(UnicodeTextBuf, CharPtr);
          DrawTextBlock(
             UnicodeTextBuf, StrLen, ForeColor, BackColor, BackColorTransp);
#endif
        }

#ifdef GuiConst_AVRGCC_COMPILER
        displayVarNow = 0;
#endif
#ifdef GuiConst_ICC_COMPILER
        displayVarNow = 0;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
        displayVarNow = 0;
#endif
#endif

        UpdateDrawLimits(CurItem.X1, CurItem.Y1, CurItem.X2, CurItem.Y2);
      }

      break;

    case GuiLib_ITEM_DOT:
      if (DisplayWriting)
      {
        GuiLib_Dot(CurItem.X1, CurItem.Y1, ForeColor);
        UpdateDrawLimits(CurItem.X1, CurItem.Y1, CurItem.X1, CurItem.Y1);
      }

      break;

    case GuiLib_ITEM_LINE:
      if (DisplayWriting)
      {
        X1 = CurItem.X1;
        X2 = CurItem.X2;
        Y1 = CurItem.Y1;
        Y2 = CurItem.Y2;
        if (CurItem.BitFlags & GuiLib_BITFLAG_PATTERNEDLINE)
          GuiLib_LinePattern(X1, Y1, X2, Y2, CurItem.LinePattern, ForeColor);
        else
          GuiLib_Line(X1, Y1, X2, Y2, ForeColor);
        UpdateDrawLimits(X1, Y1, X2, Y2);
      }
      break;

    case GuiLib_ITEM_FRAME:
      if (DisplayWriting)
      {
        X1 = CurItem.X1;
        X2 = CurItem.X2;
        Y1 = CurItem.Y1;
        Y2 = CurItem.Y2;
        OrderCoord(&X1, &X2);
        OrderCoord(&Y1, &Y2);
        DrawBorderBox(X1, Y1, X2, Y2, ForeColor, BackColor, BackColorTransp,
                      CurItem.FrameThickness);
        UpdateDrawLimits(X1, Y1, X2, Y2);
      }

      break;

    case GuiLib_ITEM_BLOCK:
      if (DisplayWriting)
      {
        X1 = CurItem.X1;
        X2 = CurItem.X2;
        Y1 = CurItem.Y1;
        Y2 = CurItem.Y2;
        OrderCoord(&X1, &X2);
        OrderCoord(&Y1, &Y2);
        GuiLib_FillBox(X1, Y1, X2, Y2, ForeColor);
        UpdateDrawLimits(X1, Y1, X2, Y2);
      }

      break;

    case GuiLib_ITEM_CIRCLE:
      if (DisplayWriting)
      {
        if (CurItem.X2 >= 0)
        {
          if (BackColorTransp)
            BackColor2 = GuiLib_NO_COLOR;
          else
            BackColor2 = BackColor;
          GuiLib_Circle(CurItem.X1, CurItem.Y1, CurItem.X2,
                        ForeColor, BackColor2);
        }
        UpdateDrawLimits(CurItem.X1 - CurItem.X2,
                         CurItem.Y1 - CurItem.X2,
                         CurItem.X1 + CurItem.X2,
                         CurItem.Y1 + CurItem.X2);
      }
      break;

    case GuiLib_ITEM_ELLIPSE:
      if (DisplayWriting)
      {
        if ((CurItem.X2 >= 0) && (CurItem.Y2 >= 0))
        {
          if (BackColorTransp)
            BackColor2 = GuiLib_NO_COLOR;
          else
            BackColor2 = BackColor;
          GuiLib_Ellipse(CurItem.X1, CurItem.Y1, CurItem.X2, CurItem.Y2,
                         ForeColor, BackColor2);
        }
        UpdateDrawLimits(CurItem.X1 - CurItem.X2,
                         CurItem.Y1 - CurItem.Y2,
                         CurItem.X1 + CurItem.X2,
                         CurItem.Y1 + CurItem.Y2);
      }
      break;

    case GuiLib_ITEM_BITMAP:
    case GuiLib_ITEM_BACKGROUND:
#ifdef GuiConst_BITMAP_SUPPORT_ON
      if (DisplayWriting)
      {
        if (CommonByte6 & 0x02)
          BackColor2 = CurItem.BitmapTranspColor;
        else
          BackColor2 = -1;
        GuiLib_ShowBitmap(CurItem.StructToCallIndex, CurItem.X1, CurItem.Y1,
           BackColor2);
        UpdateDrawLimits(CurItem.X1, CurItem.Y1, BitmapWriteX2, BitmapWriteY2);
      }

      if (CurItem.ItemType == GuiLib_ITEM_BACKGROUND)
      {
        BackgrBitmapAry[GlobalBackgrBitmapIndex].InUse = 1;
        BackgrBitmapAry[GlobalBackgrBitmapIndex].Index =
           CurItem.StructToCallIndex;
        BackgrBitmapAry[GlobalBackgrBitmapIndex].X = CurItem.X1;
        BackgrBitmapAry[GlobalBackgrBitmapIndex].Y = CurItem.Y1;
        if (GlobalBackgrBitmapIndex < GuiConst_MAX_BACKGROUND_BITMAPS - 1)
          GlobalBackgrBitmapIndex++;
      }
#endif

      break;

    case GuiLib_ITEM_ACTIVEAREA:
      if (DisplayWriting)
      {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
        if (CurItem.BitFlags & GuiLib_BITFLAG_CLIPPING)
        {
          ActiveAreaX1 = CurItem.X1;
          ActiveAreaY1 = CurItem.Y1;
          ActiveAreaX2 = CurItem.X2;
          ActiveAreaY2 = CurItem.Y2;
          OrderCoord(&ActiveAreaX1, &ActiveAreaX2);
          OrderCoord(&ActiveAreaY1, &ActiveAreaY2);
#ifdef GuiConst_DISPLAY_ACTIVE_AREA_CLIPPING
          if (ActiveAreaX1 < DisplayActiveAreaX1)
            ActiveAreaX1 = DisplayActiveAreaX1;
          if (ActiveAreaY1 < DisplayActiveAreaY1)
            ActiveAreaY1 = DisplayActiveAreaY1;
          if (ActiveAreaX2 > DisplayActiveAreaX2)
            ActiveAreaX2 = DisplayActiveAreaX2;
          if (ActiveAreaY2 > DisplayActiveAreaY2)
            ActiveAreaY2 = DisplayActiveAreaY2;
#endif
          GuiLib_SetClipping(ActiveAreaX1, ActiveAreaY1,
                             ActiveAreaX2, ActiveAreaY2);
        }
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
        if (CurItem.BitFlags & GuiLib_BITFLAG_ACTIVEAREARELCOORD)
        {
          CurItem.ClipRectX1 += CoordOrigoX;
          CurItem.ClipRectY1 += CoordOrigoY;
          CurItem.ClipRectX2 += CoordOrigoX;
          CurItem.ClipRectY2 += CoordOrigoY;
        }
#endif
#endif

        CoordOrigoX = DisplayOrigoX;
        CoordOrigoY = DisplayOrigoY;
        if (CurItem.BitFlags & GuiLib_BITFLAG_ACTIVEAREARELCOORD)
        {
          CoordOrigoX += CurItem.X1;
          CoordOrigoY += CurItem.Y1;
        }
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
        CurItem.CoordOrigoX = CoordOrigoX;
        CurItem.CoordOrigoY = CoordOrigoY;
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
        CurItem.ClipRectX1 -= CoordOrigoX;
        CurItem.ClipRectY1 -= CoordOrigoY;
        CurItem.ClipRectX2 -= CoordOrigoX;
        CurItem.ClipRectY2 -= CoordOrigoY;
#endif
#endif
      }
      break;

    case GuiLib_ITEM_CLIPRECT:
#ifdef GuiConst_CLIPPING_SUPPORT_ON
      if (DisplayWriting)
      {
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
        CurItem.ClipRectX1 += CoordOrigoX;
        CurItem.ClipRectY1 += CoordOrigoY;
        CurItem.ClipRectX2 += CoordOrigoX;
        CurItem.ClipRectY2 += CoordOrigoY;
#endif
        if (CurItem.BitFlags & GuiLib_BITFLAG_CLIPPING)
        {
          OrderCoord(&CurItem.ClipRectX1, &CurItem.ClipRectX2);
          OrderCoord(&CurItem.ClipRectY1, &CurItem.ClipRectY2);
          if (CurItem.ClipRectX1 < ActiveAreaX1)
            CurItem.ClipRectX1 = ActiveAreaX1;
          if (CurItem.ClipRectY1 < ActiveAreaY1)
            CurItem.ClipRectY1 = ActiveAreaY1;
          if (CurItem.ClipRectX2 > ActiveAreaX2)
            CurItem.ClipRectX2 = ActiveAreaX2;
          if (CurItem.ClipRectY2 > ActiveAreaY2)
            CurItem.ClipRectY2 = ActiveAreaY2;
        }
        else
        {
          CurItem.ClipRectX1 = ActiveAreaX1;
          CurItem.ClipRectY1 = ActiveAreaY1;
          CurItem.ClipRectX2 = ActiveAreaX2;
          CurItem.ClipRectY2 = ActiveAreaY2;
        }
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
        CurItem.ClipRectX1 -= CoordOrigoX;
        CurItem.ClipRectY1 -= CoordOrigoY;
        CurItem.ClipRectX2 -= CoordOrigoX;
        CurItem.ClipRectY2 -= CoordOrigoY;
#endif
        GuiLib_SetClipping(CurItem.ClipRectX1, CurItem.ClipRectY1,
                           CurItem.ClipRectX2, CurItem.ClipRectY2);
      }
#endif
      break;

    case GuiLib_ITEM_STRUCTURE:
    case GuiLib_ITEM_STRUCTARRAY:
      if (DisplayWriting && (CurItem.BackBoxSizeX > 0))
        DrawBackBox(BackColor, BackColorTransp);

      StructToCallIndex = CurItem.StructToCallIndex;
      if (CurItem.ItemType == GuiLib_ITEM_STRUCTARRAY)
      {
        if (StructToCallIndex != 0xFFFF)
        {
          VarValue = ReadVar(CurItem.VarPtr, CurItem.VarType);

          I = CurItem.IndexCount;
          while (I > 0)
          {
#ifdef GuiConst_AVRGCC_COMPILER
            if (pgm_read_word(&GuiStruct_StructNdxList[StructToCallIndex]) ==
                VarValue)
#else
            if (GuiStruct_StructNdxList[StructToCallIndex] == VarValue)
#endif
              break;

            StructToCallIndex++;
            I--;
          }
          if (I == 0)
            StructToCallIndex = 0xFFFF;
        }
      }
      if (StructToCallIndex != 0xFFFF)
      {
#ifdef GuiConst_REMOTE_STRUCT_DATA
        RemRemoteStruct = CurRemoteStruct;
#endif
        RemBackBox = (CurItem.BackBoxSizeX);
        RemBbX1 = BbX1;
        RemBbX2 = BbX2;
        RemBbY1 = BbY1;
        RemBbY2 = BbY2;
        RemDrawn = Drawn;
        RemDrawnX1 = DrawnX1;
        RemDrawnY1 = DrawnY1;
        RemDrawnX2 = DrawnX2;
        RemDrawnY2 = DrawnY2;
        ResetDrawLimits();
        RemAutoRedrawSaveIndex = AutoRedrawSaveIndex;

        DisplayLevel++;
        StructToCall =
#ifdef GuiConst_REMOTE_STRUCT_DATA
           GetRemoteStructData(StructToCallIndex);
#else
#ifdef GuiConst_AVRGCC_COMPILER
           (GuiLib_StructPtr)pgm_read_word(
           &GuiStruct_StructPtrList[StructToCallIndex]);
#else
           (GuiLib_StructPtr)GuiStruct_StructPtrList[StructToCallIndex];
#endif
#endif
        DrawStructure(StructToCall, ColorInvert);
        DisplayLevel--;

        if (RemBackBox)
        {
          CurItem.Drawn = 1;
          CurItem.DrawnX1 = RemBbX1;
          CurItem.DrawnY1 = RemBbY1;
          CurItem.DrawnX2 = RemBbX2;
          CurItem.DrawnY2 = RemBbY2;

          DrawnX1 = GuiLib_GET_MIN(BbX1, RemDrawnX1);
          DrawnY1 = GuiLib_GET_MIN(BbY1, RemDrawnY1);
          DrawnX2 = GuiLib_GET_MAX(BbX2, RemDrawnX2);
          DrawnY2 = GuiLib_GET_MAX(BbY2, RemDrawnY2);
        }
        else if (Drawn)
        {
          CurItem.Drawn = 1;
          CurItem.DrawnX1 = DrawnX1;
          CurItem.DrawnY1 = DrawnY1;
          CurItem.DrawnX2 = DrawnX2;
          CurItem.DrawnY2 = DrawnY2;

          DrawnX1 = GuiLib_GET_MIN(DrawnX1, RemDrawnX1);
          DrawnY1 = GuiLib_GET_MIN(DrawnY1, RemDrawnY1);
          DrawnX2 = GuiLib_GET_MAX(DrawnX2, RemDrawnX2);
          DrawnY2 = GuiLib_GET_MAX(DrawnY2, RemDrawnY2);
        }
        else
        {
          Drawn = RemDrawn;
          DrawnX1 = RemDrawnX1;
          DrawnY1 = RemDrawnY1;
          DrawnX2 = RemDrawnX2;
          DrawnY2 = RemDrawnY2;
        }
        AutoRedrawSaveIndex = RemAutoRedrawSaveIndex;

#ifdef GuiConst_REMOTE_STRUCT_DATA
        GetRemoteStructData(RemRemoteStruct);
#endif

      }
      break;

#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
    case GuiLib_ITEM_TOUCHAREA:
      TouchAreas[CurItem.TouchAreaNo].InUse = 1;
      TouchAreas[CurItem.TouchAreaNo].X1 = CurItem.X1;
      TouchAreas[CurItem.TouchAreaNo].Y1 = CurItem.Y1;
      TouchAreas[CurItem.TouchAreaNo].X2 = CurItem.X2;
      TouchAreas[CurItem.TouchAreaNo].Y2 = CurItem.Y2;
      OrderCoord(&TouchAreas[CurItem.TouchAreaNo].X1,
                 &TouchAreas[CurItem.TouchAreaNo].X2);
      OrderCoord(&TouchAreas[CurItem.TouchAreaNo].Y1,
                 &TouchAreas[CurItem.TouchAreaNo].Y2);

      break;
#endif

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
    case GuiLib_ITEM_SCROLLBOX:
      break;
#endif

#ifdef GuiConst_ITEM_GRAPH_INUSE
    case GuiLib_ITEM_GRAPH:
      break;
#endif
  }

#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
  if (InitialDrawing)
  {
    if ((AutoRedrawFieldNo >= 0) && (AutoRedrawSize[AutoRedrawFieldNo] > 0) &&
       (AutoRedrawVarPtr != 0))
      memcpy(&AutoRedrawOldVal[AutoRedrawFieldNo][0],AutoRedrawVarPtr,
         AutoRedrawVarSize);
  }
#endif

#ifdef GuiConst_TEXTBOX_FIELDS_ON
  if (InitialDrawing)
  {
    if (((CurItem.ItemType == GuiLib_ITEM_TEXTBLOCK) ||
         (CurItem.ItemType == GuiLib_ITEM_VARBLOCK)) &&
         (CurItem.TextBoxScrollIndex != 0xFF))
      memcpy(&TextboxScrollItems[CurItem.TextBoxScrollIndex], &CurItem,
         sizeof(GuiLib_ItemRec));
  }
#endif

#ifdef GuiConst_CURSOR_SUPPORT_ON
  if (InitialDrawing && IsCursorField)
    CurItem.CursorFieldLevel--;

  if ((ColorInvert == GuiLib_COL_INVERT_IF_CURSOR) && FoundActiveCursorField)
    SwapColors = 0;
#endif

#ifdef GuiConst_REL_COORD_ORIGO_INUSE
  if (!InitialDrawing)
  {
    CoordOrigoX = DisplayOrigoX;
    CoordOrigoY = DisplayOrigoY;
  }
#endif
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (!InitialDrawing && DisplayWriting)
    GuiLib_ResetClipping();
#endif
}

//------------------------------------------------------------------------------
static void ReadItem(
   GuiConst_INT16S LanguageIndex)
{
  GuiConst_INT16S X;
  GuiConst_INT16S N;
  GuiConst_INTCOLOR TmpForeColor, TmpBackColor;
  GuiConst_INT8U B1;
  GuiConst_INT16U W1;
  GuiConst_INT16U W2;
#ifdef GuiConst_CHARMODE_UNICODE
  GuiConst_INT8U CB1;
  GuiConst_INT8U CB2;
#endif
#ifdef GuiConst_CURSOR_SUPPORT_ON
  GuiConst_INT8S BS1;
#endif
#ifdef GuiConst_BLINK_SUPPORT_ON
  GuiConst_INT8S BS2;
#endif
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  GuiConst_INT8U ScrollBoxIndex;
  GuiConst_INT8U L;
#ifndef GuiConst_SCROLLITEM_BAR_NONE
#ifdef GuiConst_REMOTE_BITMAP_DATA
  GuiConst_INT8U * PixelDataPtr;
  GuiConst_INT8U BitmapHeader[4];
#else
  GuiConst_INT8U PrefixRom * PixelDataPtr;
#endif
#endif
#endif
#ifdef GuiConst_ITEM_GRAPH_INUSE
  GuiConst_INT8U GraphIndex;
  GuiConst_INT8U Axis;
  GuiConst_INT8U B;
#endif

  CurItem.BitFlags |= GuiLib_BITFLAG_INUSE;

#ifdef GuiConst_REL_COORD_ORIGO_INUSE
  if (DisplayLevel == 0)
  {
    CurItem.CoordOrigoX = CoordOrigoX;
    CurItem.CoordOrigoY = CoordOrigoY;
  }
#endif

  CommonByte0 = GetItemByte(&ItemDataPtr);
  CommonByte1 = GetItemByte(&ItemDataPtr);
  CommonByte2 = GetItemByte(&ItemDataPtr);
  CommonByte3 = GetItemByte(&ItemDataPtr);
  CommonByte4 = GetItemByte(&ItemDataPtr);
  CommonByte5 = GetItemByte(&ItemDataPtr);
  CommonByte6 = GetItemByte(&ItemDataPtr);

  CurItem.ItemType = CommonByte0 & 0x1F;
  ItemTypeBit = (GuiConst_INT32U)0x00000001 << CurItem.ItemType;

  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK))
  {
    if (CommonByte4 & 0x80)
      CurItem.BitFlags |= GuiLib_BITFLAG_TRANSLATION;
    else
      CurItem.BitFlags &= ~GuiLib_BITFLAG_TRANSLATION;

    if (CurItem.BitFlags & GuiLib_BITFLAG_TRANSLATION)
    {
#ifdef GuiConst_LANGUAGE_ALL_ACTIVE
      N = GuiConst_LANGUAGE_CNT;
      W1 = LanguageIndex;
#else
      N = GuiConst_LANGUAGE_ACTIVE_CNT;
#ifdef GuiConst_AVRGCC_COMPILER
      W1 = pgm_read_word(&GuiFont_LanguageIndex[LanguageIndex]);
#else
      W1 = GuiFont_LanguageIndex[LanguageIndex];
#endif
#endif
    }
    else
    {
      N = 1;
      W1 = 0;
    }

    for (X = 0; X < N; X++)
    {
#ifdef GuiConst_REMOTE_TEXT_DATA
      W2 = GetItemWord(&ItemDataPtr);
#endif
      if (X == W1)
#ifdef GuiConst_REMOTE_TEXT_DATA
      {
        CurItem.TextLength = GetRemoteText(W2);
        CurItem.TextPtr = &GuiLib_RemoteTextBuffer[0];
      }
#else
        CurItem.TextPtr = (GuiConst_TEXT *)ItemDataPtr;

      W2 = 0;
#ifdef GuiConst_CHARMODE_ANSI
  #ifdef GuiConst_AVRGCC_COMPILER
      while (pgm_read_byte(ItemDataPtr) != 0)
  #else
      while (*((GuiConst_TEXT PrefixRom *)ItemDataPtr) != 0)
  #endif
      {
        ItemDataPtr++;
        ItemDataBufCnt++;
        W2++;
      }

      ItemDataPtr++;
      ItemDataBufCnt++;
#else
      do
      {
  #ifdef GuiConst_AVRGCC_COMPILER
        CB1 = pgm_read_byte(ItemDataPtr);
  #else
        CB1 = *((GuiConst_INT8U PrefixRom *)ItemDataPtr);
  #endif
        ItemDataPtr++;
        ItemDataBufCnt++;
  #ifdef GuiConst_AVRGCC_COMPILER
        CB2 = pgm_read_byte(ItemDataPtr);
  #else
        CB2 = *((GuiConst_INT8U PrefixRom *)ItemDataPtr);
  #endif
        ItemDataPtr++;
        ItemDataBufCnt++;

        if ((CB1 == 0) && (CB2 == 0))
          break;
        W2++;
      }
      while (1);
#endif

      if (X == W1)
        CurItem.TextLength = W2;
#endif
    }
  }

#ifdef GuiConst_BLINK_SUPPORT_ON
  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK))
  {
    if (CommonByte5 & 0x01)
      CurItem.BitFlags |= GuiLib_BITFLAG_BLINKTEXTFIELD;
    else
      CurItem.BitFlags &= ~GuiLib_BITFLAG_BLINKTEXTFIELD;
  }
#endif

  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK +
                     GuiLib_ITEMBIT_SCROLLBOX +
                     GuiLib_ITEMBIT_GRAPH))
  {
    B1 = GetItemByte(&ItemDataPtr);
    if (B1 != 0xFF)
      CurItem.FontIndex = B1 + 1;
  }

#ifdef GuiConst_CHARMODE_ANSI
  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK))
    CurItem.CharSetSelector = GetItemByte(&ItemDataPtr);
#endif

  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_VARBLOCK))
  {
#ifdef GuiConst_TEXTBOX_FIELDS_ON
    CurItem.TextBoxScrollIndex = GetItemByte(&ItemDataPtr);
#else
    GetItemByte(&ItemDataPtr);
#endif
  }

  if (ItemTypeBit & (GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK))
    CurItem.VarType = GetItemByte(&ItemDataPtr);

  if (ItemTypeBit & GuiLib_ITEMBIT_FRAME)
  {
    CurItem.FrameThickness = GetItemByte(&ItemDataPtr);
    if (CurItem.FrameThickness == 0)
      CurItem.FrameThickness = 1;
  }

  if (ItemTypeBit & GuiLib_ITEMBIT_STRUCTARRAY)
    CurItem.IndexCount = GetItemWord(&ItemDataPtr);

  if (ItemTypeBit & (GuiLib_ITEMBIT_ACTIVEAREA +
                     GuiLib_ITEMBIT_CLIPRECT))
  {
    B1 = GetItemByte(&ItemDataPtr);
    if (B1 & 0x01)
      CurItem.BitFlags |= GuiLib_BITFLAG_CLIPPING;
    else
      CurItem.BitFlags &= ~GuiLib_BITFLAG_CLIPPING;
    if (B1 & 0x02)
      CurItem.BitFlags |= GuiLib_BITFLAG_ACTIVEAREARELCOORD;
    else
      CurItem.BitFlags &= ~GuiLib_BITFLAG_ACTIVEAREARELCOORD;
  }

  if (ItemTypeBit & GuiLib_ITEMBIT_FORMATTER)
  {
    CurItem.FormatFormat = GetItemByte(&ItemDataPtr);
    B1 = GetItemByte(&ItemDataPtr);
    if (B1 & 0x01)
      CurItem.BitFlags |= GuiLib_BITFLAG_FORMATSHOWSIGN;
    else
      CurItem.BitFlags &= ~GuiLib_BITFLAG_FORMATSHOWSIGN;
    if (B1 & 0x02)
      CurItem.BitFlags |= GuiLib_BITFLAG_FORMATZEROPADDING;
    else
      CurItem.BitFlags &= ~GuiLib_BITFLAG_FORMATZEROPADDING;
    CurItem.FormatAlignment = (B1 >> 2) & 0x03;
    if (B1 & 0x10)
      CurItem.BitFlags |= GuiLib_BITFLAG_FORMATTRAILINGZEROS;
    else
      CurItem.BitFlags &= ~GuiLib_BITFLAG_FORMATTRAILINGZEROS;
    if (B1 & 0x20)
      CurItem.BitFlags |= GuiLib_BITFLAG_FORMATTHOUSANDSSEP;
    else
      CurItem.BitFlags &= ~GuiLib_BITFLAG_FORMATTHOUSANDSSEP;
    CurItem.FormatFieldWidth = GetItemByte(&ItemDataPtr);
    CurItem.FormatDecimals = GetItemByte(&ItemDataPtr);
  }

  if (ItemTypeBit & (GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_BITMAP +
                     GuiLib_ITEMBIT_BACKGROUND))
    CurItem.StructToCallIndex = GetItemWord(&ItemDataPtr);

#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_VARBLOCK))
  {
    CurItem.TextBoxHorzAlignment = GetItemByte(&ItemDataPtr);
    CurItem.TextBoxVertAlignment = GetItemByte(&ItemDataPtr);
    CurItem.TextBoxLineDist = GetItemByte(&ItemDataPtr);
    CurItem.TextBoxLineDistRelToFont = GetItemByte(&ItemDataPtr);
  }
#endif

#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
  if (ItemTypeBit & GuiLib_ITEMBIT_TOUCHAREA)
    CurItem.TouchAreaNo = GetItemByte(&ItemDataPtr);
#endif

#ifdef GuiConst_COLOR_DEPTH_1
#ifdef GuiConst_BITMAP_SUPPORT_ON
  if ((ItemTypeBit & (GuiLib_ITEMBIT_BITMAP +
                      GuiLib_ITEMBIT_BACKGROUND)) && (CommonByte6 & 0x02))
    CurItem.BitmapTranspColor = (CommonByte6 >> 2) & 0x01;
#endif
#else
  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_DOT +
                     GuiLib_ITEMBIT_LINE +
                     GuiLib_ITEMBIT_FRAME +
                     GuiLib_ITEMBIT_BLOCK +
                     GuiLib_ITEMBIT_CIRCLE +
                     GuiLib_ITEMBIT_ELLIPSE +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK +
                     GuiLib_ITEMBIT_SCROLLBOX +
                     GuiLib_ITEMBIT_GRAPH))
  {
    if ((CommonByte3 & 0x07) == GuiLib_COLOR_OTHER)
#ifdef GuiLib_COLOR_BYTESIZE_1
      CurItem.ForeColor = (GuiConst_INTCOLOR)GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
      CurItem.ForeColor = (GuiConst_INTCOLOR)GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
      CurItem.ForeColor = (GuiConst_INTCOLOR)GetItemTriple(&ItemDataPtr);
#endif
    if (((CommonByte5 >> 1) & 0x07) == GuiLib_COLOR_OTHER)
#ifdef GuiLib_COLOR_BYTESIZE_1
      CurItem.BarForeColor = (GuiConst_INTCOLOR)GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
      CurItem.BarForeColor = (GuiConst_INTCOLOR)GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
      CurItem.BarForeColor = (GuiConst_INTCOLOR)GetItemTriple(&ItemDataPtr);
#endif
  }
  if (ItemTypeBit & (GuiLib_ITEMBIT_CLEARAREA +
                     GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_FRAME +
                     GuiLib_ITEMBIT_CIRCLE +
                     GuiLib_ITEMBIT_ELLIPSE +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK +
                     GuiLib_ITEMBIT_SCROLLBOX +
                     GuiLib_ITEMBIT_GRAPH))
  {
    if (((CommonByte3 >> 3) & 0x07) == GuiLib_COLOR_OTHER)
#ifdef GuiLib_COLOR_BYTESIZE_1
      CurItem.BackColor = GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
      CurItem.BackColor = GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
      CurItem.BackColor = GetItemTriple(&ItemDataPtr);
#endif
    if (((CommonByte5 >> 4) & 0x07) == GuiLib_COLOR_OTHER)
#ifdef GuiLib_COLOR_BYTESIZE_1
      CurItem.BarBackColor = GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
      CurItem.BarBackColor = GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
      CurItem.BarBackColor = GetItemTriple(&ItemDataPtr);
#endif
  }
#ifdef GuiConst_BITMAP_SUPPORT_ON
  if ((ItemTypeBit & (GuiLib_ITEMBIT_BITMAP +
                      GuiLib_ITEMBIT_BACKGROUND)) &&
      (CommonByte6 & 0x02))
  {
#ifdef GuiLib_COLOR_BYTESIZE_1
    CurItem.BitmapTranspColor = GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
    CurItem.BitmapTranspColor = GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
    CurItem.BitmapTranspColor = GetItemTriple(&ItemDataPtr);
#endif
  }
#endif
#endif

  if (CommonByte0 & 0x20)
    B1 = GetItemByte(&ItemDataPtr);
  else
    B1 = 0;
  X1MemoryRead = B1 & 0x03;
  X1MemoryWrite = (B1 >> 2) & 0x03;
  Y1MemoryRead = (B1 >> 4) & 0x03;
  Y1MemoryWrite = (B1 >> 6) & 0x03;
  if (CommonByte0 & 0x40)
    B1 = GetItemByte(&ItemDataPtr);
  else
    B1 = 0;
  X2MemoryRead = B1 & 0x03;
  X2MemoryWrite = (B1 >> 2) & 0x03;
  Y2MemoryRead = (B1 >> 4) & 0x03;
  Y2MemoryWrite = (B1 >> 6) & 0x03;

  if (CommonByte6 & 0x01)
    B1 = GetItemByte(&ItemDataPtr);
  else
    B1 = 0;
  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK))
    CurItem.BackBorderPixels = B1;
  else
    CurItem.BackBorderPixels = 0;

  if (CommonByte0 & 0x80)
  {
    N = GetItemWord(&ItemDataPtr);
    CurItem.BackBoxSizeY1 = GetItemByte(&ItemDataPtr);
    CurItem.BackBoxSizeY2 = GetItemByte(&ItemDataPtr);
  }
  else
    N = 0;
  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR))
    CurItem.BackBoxSizeX = N;
  else
    CurItem.BackBoxSizeX = 0;

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  if (ItemTypeBit & GuiLib_ITEMBIT_SCROLLBOX)
  {
    if (NextScrollLineReading)
      if (CommonByte0 & 0x80)
      {
        ScrollBoxesAry[GlobalScrollBoxIndex].LineSizeX = CurItem.BackBoxSizeX;
        ScrollBoxesAry[GlobalScrollBoxIndex].LineSizeY = CurItem.BackBoxSizeY1;
        ScrollBoxesAry[GlobalScrollBoxIndex].LineSizeY2 = CurItem.BackBoxSizeY2;
      }
  }
#endif

  X1Mode = CommonByte1 & 0x03;
  Y1Mode = (CommonByte1 >> 2) & 0x03;
  X2Mode = (CommonByte1 >> 4) & 0x03;
  Y2Mode = (CommonByte1 >> 6) & 0x03;

  if (CommonByte2 & 0x01)
    ItemX1 = GetItemWord(&ItemDataPtr);
  else
    ItemX1 = 0;
  if (CommonByte2 & 0x02)
    ItemY1 = GetItemWord(&ItemDataPtr);
  else
    ItemY1 = 0;
  if (CommonByte2 & 0x04)
    ItemX2 = GetItemWord(&ItemDataPtr);
  else
    ItemX2 = 0;
  if (CommonByte2 & 0x08)
    ItemY2 = GetItemWord(&ItemDataPtr);
  else
    ItemY2 = 0;

  if (CommonByte2 & 0x10)
    X1VarType = GetItemByte(&ItemDataPtr);
  if (CommonByte2 & 0x20)
    Y1VarType = GetItemByte(&ItemDataPtr);
  if (CommonByte2 & 0x40)
    X2VarType = GetItemByte(&ItemDataPtr);
  if (CommonByte2 & 0x80)
    Y2VarType = GetItemByte(&ItemDataPtr);

  TmpForeColor = CurItem.ForeColor;
  TmpBackColor = CurItem.BackColor;
  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_DOT +
                     GuiLib_ITEMBIT_LINE +
                     GuiLib_ITEMBIT_FRAME +
                     GuiLib_ITEMBIT_BLOCK +
                     GuiLib_ITEMBIT_CIRCLE +
                     GuiLib_ITEMBIT_ELLIPSE +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK +
                     GuiLib_ITEMBIT_SCROLLBOX +
                     GuiLib_ITEMBIT_GRAPH))
  {
    switch (CommonByte3 & 0x07)
    {
      case GuiLib_COLOR_FORE:
        CurItem.ForeColor = GuiConst_PIXEL_ON;
        break;

      case GuiLib_COLOR_BACK:
        CurItem.ForeColor = GuiConst_PIXEL_OFF;
        break;

      case GuiLib_COLOR_INVERT:
        CurItem.ForeColor = TmpBackColor;
        break;
    }

    switch ((CommonByte5 >> 1) & 0x07)
    {
      case GuiLib_COLOR_FORE:
        CurItem.BarForeColor = GuiConst_PIXEL_ON;
        break;

      case GuiLib_COLOR_BACK:
        CurItem.BarForeColor = GuiConst_PIXEL_OFF;
        break;

      case GuiLib_COLOR_INVERT:
        CurItem.BarForeColor = CurItem.BackColor;
        break;
    }
  }

  if (ItemTypeBit & (GuiLib_ITEMBIT_CLEARAREA +
                     GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_FRAME +
                     GuiLib_ITEMBIT_CIRCLE +
                     GuiLib_ITEMBIT_ELLIPSE +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK +
                     GuiLib_ITEMBIT_SCROLLBOX +
                     GuiLib_ITEMBIT_GRAPH))
  {
    switch ((CommonByte3 >> 3) & 0x07)
    {
      case GuiLib_COLOR_FORE:
        CurItem.BackColor = GuiConst_PIXEL_ON;
        CurItem.BitFlags &= ~GuiLib_BITFLAG_TRANSPARENT;
        break;

      case GuiLib_COLOR_BACK:
        CurItem.BackColor = GuiConst_PIXEL_OFF;
        CurItem.BitFlags &= ~GuiLib_BITFLAG_TRANSPARENT;
        break;

      case GuiLib_COLOR_OTHER:
        CurItem.BitFlags &= ~GuiLib_BITFLAG_TRANSPARENT;
        break;

      case GuiLib_COLOR_INVERT:
        CurItem.BackColor = TmpForeColor;
        CurItem.BitFlags &= ~GuiLib_BITFLAG_TRANSPARENT;
        break;

      case GuiLib_COLOR_TRANSP:
        CurItem.BitFlags |= GuiLib_BITFLAG_TRANSPARENT;
        break;
    }

    switch ((CommonByte5 >> 4) & 0x07)
    {
      case GuiLib_COLOR_FORE:
        CurItem.BarBackColor = GuiConst_PIXEL_ON;
        CurItem.BitFlags &= ~GuiLib_BITFLAG_BARTRANSPARENT;
        break;

      case GuiLib_COLOR_BACK:
        CurItem.BarBackColor = GuiConst_PIXEL_OFF;
        CurItem.BitFlags &= ~GuiLib_BITFLAG_BARTRANSPARENT;
        break;

      case GuiLib_COLOR_OTHER:
        CurItem.BitFlags &= ~GuiLib_BITFLAG_BARTRANSPARENT;
        break;

      case GuiLib_COLOR_INVERT:
        CurItem.BitFlags &= ~GuiLib_BITFLAG_BARTRANSPARENT;
        CurItem.BarBackColor = CurItem.ForeColor;
        break;

      case GuiLib_COLOR_TRANSP:
        CurItem.BitFlags |= GuiLib_BITFLAG_BARTRANSPARENT;
        break;
    }
  }

  if (ItemTypeBit & (GuiLib_ITEMBIT_CLEARAREA +
                     GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_LINE +
                     GuiLib_ITEMBIT_FRAME +
                     GuiLib_ITEMBIT_BLOCK +
                     GuiLib_ITEMBIT_CIRCLE +
                     GuiLib_ITEMBIT_ELLIPSE +
                     GuiLib_ITEMBIT_BITMAP +
                     GuiLib_ITEMBIT_BACKGROUND +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_ACTIVEAREA +
                     GuiLib_ITEMBIT_CLIPRECT +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK +
                     GuiLib_ITEMBIT_TOUCHAREA +
                     GuiLib_ITEMBIT_SCROLLBOX +
                     GuiLib_ITEMBIT_GRAPH))
  {
    B1 = (CommonByte3 >> 6) & 0x03;
    if (B1 != GuiLib_ALIGN_NOCHANGE)
      CurItem.Alignment = B1;
  }

  B1 = CommonByte4 & 0x03;
  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK))
  {
    if (B1 != GuiLib_PS_NOCHANGE)
      CurItem.Ps = B1;
  }

#ifdef GuiConst_SCROLL_SUPPORT_ON
  MarkedAs = (CommonByte4 >> 2) & 0x03;
#endif

  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK))
  {
    if (CommonByte4 & 0x10)
      CurItem.BitFlags |= GuiLib_BITFLAG_UNDERLINE;
    else
      CurItem.BitFlags &= ~GuiLib_BITFLAG_UNDERLINE;
  }
  else if (ItemTypeBit & (GuiLib_ITEMBIT_LINE))
  {
    if (CommonByte4 & 0x10)
      CurItem.BitFlags |= GuiLib_BITFLAG_PATTERNEDLINE;
    else
      CurItem.BitFlags &= ~GuiLib_BITFLAG_PATTERNEDLINE;
  }

  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_DOT +
                     GuiLib_ITEMBIT_LINE +
                     GuiLib_ITEMBIT_FRAME +
                     GuiLib_ITEMBIT_BLOCK +
                     GuiLib_ITEMBIT_CIRCLE +
                     GuiLib_ITEMBIT_ELLIPSE +
                     GuiLib_ITEMBIT_BITMAP +
                     GuiLib_ITEMBIT_BACKGROUND +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK))
  {
    if (CommonByte4 & 0x20)
      CurItem.BitFlags |= GuiLib_BITFLAG_AUTOREDRAWFIELD;
    else
      CurItem.BitFlags &= ~GuiLib_BITFLAG_AUTOREDRAWFIELD;
  }
  else
    CurItem.BitFlags &= ~GuiLib_BITFLAG_AUTOREDRAWFIELD;

#ifdef GuiConst_CURSOR_SUPPORT_ON
  if (CommonByte4 & 0x40)
    BS1 = GetItemByte(&ItemDataPtr);
  else
    BS1 = -1;
  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_DOT +
                     GuiLib_ITEMBIT_LINE +
                     GuiLib_ITEMBIT_FRAME +
                     GuiLib_ITEMBIT_BLOCK +
                     GuiLib_ITEMBIT_CIRCLE +
                     GuiLib_ITEMBIT_ELLIPSE +
                     GuiLib_ITEMBIT_STRUCTURE +
                     GuiLib_ITEMBIT_STRUCTARRAY +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK))
    CurItem.CursorFieldNo = BS1;
  else
    CurItem.CursorFieldNo = -1;
#endif

#ifdef GuiConst_BLINK_SUPPORT_ON
  if (CommonByte5 & 0x01)
    BS2 = GetItemByte(&ItemDataPtr);
  else
    BS2 = -1;
  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK))
    CurItem.BlinkFieldNo = BS2;
  else
    CurItem.BlinkFieldNo = -1;
#endif

#ifdef GuiConst_SCROLL_SUPPORT_ON
  if (MarkedAs == GuiLib_MARK_AS_SCROLL_LINE)
    ScrLineDy = GetItemByte(&ItemDataPtr);
#endif

#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
  if ((CommonByte4 & 0x20) &&
     (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_DOT +
                     GuiLib_ITEMBIT_LINE +
                     GuiLib_ITEMBIT_FRAME +
                     GuiLib_ITEMBIT_BLOCK +
                     GuiLib_ITEMBIT_CIRCLE +
                     GuiLib_ITEMBIT_ELLIPSE +
                     GuiLib_ITEMBIT_BITMAP +
                     GuiLib_ITEMBIT_BACKGROUND +
                     GuiLib_ITEMBIT_STRUCTURE)))
    CurItem.VarType = GetItemByte(&ItemDataPtr);
#endif

  if (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                     GuiLib_ITEMBIT_TEXTBLOCK +
                     GuiLib_ITEMBIT_VAR +
                     GuiLib_ITEMBIT_VARBLOCK))
  {
    if ((ItemTypeBit & (GuiLib_ITEMBIT_VAR +
                        GuiLib_ITEMBIT_VARBLOCK)) ||
        (CurItem.BitFlags & GuiLib_BITFLAG_TRANSLATION))
    {
      if (CommonByte5 & 0x80)
      {
        if (GetItemByte(&ItemDataPtr))
          CurItem.BitFlags |= GuiLib_BITFLAG_REVERSEWRITING;
        else
          CurItem.BitFlags &= ~GuiLib_BITFLAG_REVERSEWRITING;
      }
      else
      {
#ifdef GuiConst_AVRGCC_COMPILER
        if (pgm_read_byte(&GuiFont_LanguageTextDir[LanguageIndex]))
#else
        if (GuiFont_LanguageTextDir[LanguageIndex])
#endif
          CurItem.BitFlags |= GuiLib_BITFLAG_REVERSEWRITING;
        else
          CurItem.BitFlags &= ~GuiLib_BITFLAG_REVERSEWRITING;
      }
    }
    else
    {
      if (CommonByte5 & 0x80)
      {
        if (GetItemByte(&ItemDataPtr))
          CurItem.BitFlags |= GuiLib_BITFLAG_REVERSEWRITING;
        else
          CurItem.BitFlags &= ~GuiLib_BITFLAG_REVERSEWRITING;
      }
      else
      {
#ifdef GuiConst_AVRGCC_COMPILER
        if (pgm_read_byte(&GuiFont_LanguageTextDir[0]))
#else
        if (GuiFont_LanguageTextDir[0])
#endif
          CurItem.BitFlags |= GuiLib_BITFLAG_REVERSEWRITING;
        else
          CurItem.BitFlags &= ~GuiLib_BITFLAG_REVERSEWRITING;
      }
    }
  }
  else
    CurItem.BitFlags &= ~GuiLib_BITFLAG_REVERSEWRITING;

  if ((ItemTypeBit & (GuiLib_ITEMBIT_LINE)) &&
      (CurItem.BitFlags & GuiLib_BITFLAG_PATTERNEDLINE))
    CurItem.LinePattern = GetItemByte(&ItemDataPtr);

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  if (ItemTypeBit & GuiLib_ITEMBIT_SCROLLBOX)
  {
    ScrollBoxIndex = GetItemByte(&ItemDataPtr);
    GlobalScrollBoxIndex = ScrollBoxIndex;
    ScrollBoxesAry[ScrollBoxIndex].InUse = GuiLib_SCROLL_STRUCTURE_READ;
    ScrollBoxesAry[ScrollBoxIndex].ScrollBoxType = GetItemByte(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].MakeUpStructIndex =
       GetItemWord(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines =
       GetItemWord(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].LineVerticalOffset =
       GetItemWord(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].LineOffsetX = GetItemWord(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].LineOffsetY = GetItemWord(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].LineSizeX = GetItemWord(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].LineSizeY = GetItemWord(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].LineStructIndex = GetItemWord(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].LineStructOffsetX =
       GetItemWord(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].LineStructOffsetY =
       GetItemWord(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].LineColorMode = GetItemByte(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].LineColorTransparent = 0;
    switch (ScrollBoxesAry[ScrollBoxIndex].LineColorMode)
    {
      case GuiLib_COLOR_NOCHANGE:
        ScrollBoxesAry[ScrollBoxIndex].LineColor = CurItem.ForeColor;
        break;

      case GuiLib_COLOR_FORE:
        ScrollBoxesAry[ScrollBoxIndex].LineColor = GuiConst_PIXEL_ON;
        break;

      case GuiLib_COLOR_BACK:
        ScrollBoxesAry[ScrollBoxIndex].LineColor = GuiConst_PIXEL_OFF;
        break;

      case GuiLib_COLOR_OTHER:
        ScrollBoxesAry[ScrollBoxIndex].LineColor = (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
           GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
           GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
           GetItemTriple(&ItemDataPtr);
#endif
        break;

      case GuiLib_COLOR_INVERT:
        ScrollBoxesAry[ScrollBoxIndex].LineColor = TmpForeColor;
        break;

      case GuiLib_COLOR_TRANSP:
        ScrollBoxesAry[ScrollBoxIndex].LineColorTransparent = 1;
        break;
    }
    ScrollBoxesAry[ScrollBoxIndex].WrapMode = GetItemByte(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs = GetItemByte(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].ScrollMode = GetItemByte(&ItemDataPtr);
    ScrollBoxesAry[ScrollBoxIndex].LineMarkerCount = GetItemByte(&ItemDataPtr);
    for (L = 0; L < ScrollBoxesAry[ScrollBoxIndex].LineMarkerCount; L++)
    {
      ScrollBoxesAry[ScrollBoxIndex].MarkerColorMode[L] =
         GetItemByte(&ItemDataPtr);
      switch (ScrollBoxesAry[ScrollBoxIndex].MarkerColorMode[L])
      {
        case GuiLib_COLOR_NOCHANGE:
          ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = CurItem.BackColor;
          break;

        case GuiLib_COLOR_FORE:
          ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = GuiConst_PIXEL_ON;
          ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 0;
          break;

        case GuiLib_COLOR_BACK:
          ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = GuiConst_PIXEL_OFF;
          ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 0;
          break;

        case GuiLib_COLOR_OTHER:
          ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
             GetItemTriple(&ItemDataPtr);
#endif
          ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 0;
          break;

        case GuiLib_COLOR_INVERT:
          ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = TmpForeColor;
          ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 0;
          break;

        case GuiLib_COLOR_TRANSP:
          ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 1;
          break;
      }
      ScrollBoxesAry[ScrollBoxIndex].MarkerStructIndex[L] =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].MarkerDrawingOrder[L] =
         GetItemByte(&ItemDataPtr);
      if (L || (ScrollBoxesAry[ScrollBoxIndex].ScrollBoxType))
        ScrollBoxesAry[ScrollBoxIndex].MarkerSize[L] = 0;
      else
        ScrollBoxesAry[ScrollBoxIndex].MarkerSize[L] = 1;
      ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[L] = 0;
    }

    ScrollBoxesAry[ScrollBoxIndex].BarType = GetItemByte(&ItemDataPtr);
#ifndef GuiConst_SCROLLITEM_BAR_NONE
    if (ScrollBoxesAry[ScrollBoxIndex].BarType != GuiLib_MARKER_NONE)
    {
      ScrollBoxesAry[ScrollBoxIndex].BarMode = GetItemByte(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarPositionX = GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarPositionY = GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarSizeX = GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarSizeY = GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarStructIndex = GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarColorMode = GetItemByte(&ItemDataPtr);
      switch (ScrollBoxesAry[ScrollBoxIndex].BarColorMode & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          ScrollBoxesAry[ScrollBoxIndex].BarForeColor = CurItem.ForeColor;
          break;

        case GuiLib_COLOR_FORE:
          ScrollBoxesAry[ScrollBoxIndex].BarForeColor = GuiConst_PIXEL_ON;
          break;

        case GuiLib_COLOR_BACK:
          ScrollBoxesAry[ScrollBoxIndex].BarForeColor = GuiConst_PIXEL_OFF;
          break;

        case GuiLib_COLOR_OTHER:
          ScrollBoxesAry[ScrollBoxIndex].BarForeColor = (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
             GetItemTriple(&ItemDataPtr);
#endif
          break;

        case GuiLib_COLOR_INVERT:
          ScrollBoxesAry[ScrollBoxIndex].BarForeColor = TmpBackColor;
          break;

        case GuiLib_COLOR_TRANSP:
          break;
      }
      ScrollBoxesAry[ScrollBoxIndex].BarTransparent = 0;
      switch ((ScrollBoxesAry[ScrollBoxIndex].BarColorMode & 0x38) >> 3)
      {
        case GuiLib_COLOR_NOCHANGE:
          ScrollBoxesAry[ScrollBoxIndex].BarBackColor = CurItem.ForeColor;
          break;

        case GuiLib_COLOR_FORE:
          ScrollBoxesAry[ScrollBoxIndex].BarBackColor = GuiConst_PIXEL_ON;
          break;

        case GuiLib_COLOR_BACK:
          ScrollBoxesAry[ScrollBoxIndex].BarBackColor = GuiConst_PIXEL_OFF;
          break;

        case GuiLib_COLOR_OTHER:
          ScrollBoxesAry[ScrollBoxIndex].BarBackColor = (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
             GetItemTriple(&ItemDataPtr);
#endif
          break;

        case GuiLib_COLOR_INVERT:
          ScrollBoxesAry[ScrollBoxIndex].BarBackColor = TmpForeColor;
          break;

        case GuiLib_COLOR_TRANSP:
          ScrollBoxesAry[ScrollBoxIndex].BarTransparent = 1;
          break;
      }
      ScrollBoxesAry[ScrollBoxIndex].BarThickness = GetItemByte(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarMarkerLeftOffset =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarMarkerRightOffset =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarMarkerTopOffset =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarMarkerBottomOffset =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarIconPtr =
         (GuiConst_TEXT PrefixRom *)ItemDataPtr;
      ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconCharacter =
#ifdef GuiConst_CHARMODE_ANSI
         GetItemByte(&ItemDataPtr);
#else
         GetItemWord(&ItemDataPtr);
#endif
      ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconFont =
         GetItemByte(&ItemDataPtr) + 1;
      ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconOffsetX =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconOffsetY =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].BarMarkerColorMode =
         GetItemByte(&ItemDataPtr);
      switch (ScrollBoxesAry[ScrollBoxIndex].BarMarkerColorMode & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor = CurItem.ForeColor;
          break;

        case GuiLib_COLOR_FORE:
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor = GuiConst_PIXEL_ON;
          break;

        case GuiLib_COLOR_BACK:
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor =
             GuiConst_PIXEL_OFF;
          break;

        case GuiLib_COLOR_OTHER:
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor =
             (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
             GetItemTriple(&ItemDataPtr);
#endif
          break;

        case GuiLib_COLOR_INVERT:
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor = TmpBackColor;
          break;

        case GuiLib_COLOR_TRANSP:
          break;
      }
      ScrollBoxesAry[ScrollBoxIndex].BarMarkerTransparent = 0;
      switch ((ScrollBoxesAry[ScrollBoxIndex].BarMarkerColorMode & 0x38) >> 3)
      {
        case GuiLib_COLOR_NOCHANGE:
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor = CurItem.BackColor;
          break;

        case GuiLib_COLOR_FORE:
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor = GuiConst_PIXEL_ON;
          break;

        case GuiLib_COLOR_BACK:
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor =
             GuiConst_PIXEL_OFF;
          break;

        case GuiLib_COLOR_OTHER:
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor =
             (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
             GetItemTriple(&ItemDataPtr);
#endif
          break;

        case GuiLib_COLOR_INVERT:
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor = TmpForeColor;
          break;

        case GuiLib_COLOR_TRANSP:
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerTransparent = 1;
          break;
      }
      if (ScrollBoxesAry[ScrollBoxIndex].BarType == GuiLib_MARKER_BITMAP)
      {
        ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIndex =
           GetItemWord(&ItemDataPtr);
#ifdef GuiConst_REMOTE_BITMAP_DATA
        GuiLib_RemoteDataReadBlock(
           (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[
           ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIndex], 4,
           BitmapHeader);
        PixelDataPtr = &BitmapHeader[0] + 2;
#else
#ifdef GuiConst_AVRGCC_COMPILER
        PixelDataPtr = (GuiConst_INT8U PrefixRom *)pgm_read_word(&GuiStruct_BitmapPtrList[
           ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIndex]) + 2;
#else
        PixelDataPtr = (GuiConst_INT8U PrefixRom *)GuiStruct_BitmapPtrList[
           ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIndex] + 2;
#endif
#endif
        ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapHeight =
           (GuiConst_INT16S)*PixelDataPtr;
        PixelDataPtr++;
        ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapHeight +=
           256*(GuiConst_INT16S)*PixelDataPtr;
        B1 = GetItemByte(&ItemDataPtr);
        ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIsTransparent =
           (B1 & 0x01);
#ifdef GuiConst_BITMAP_SUPPORT_ON
        if (ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIsTransparent)
#ifdef GuiConst_COLOR_DEPTH_1
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapTranspColor =
             (B1 >> 1) & 0x01;
#else
#ifdef GuiLib_COLOR_BYTESIZE_1
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapTranspColor =
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapTranspColor =
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
          ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapTranspColor =
             GetItemTriple(&ItemDataPtr);
#endif
#endif
#endif
      }
    }
#endif

    ScrollBoxesAry[ScrollBoxIndex].IndicatorType = GetItemByte(&ItemDataPtr);
#ifndef GuiConst_SCROLLITEM_INDICATOR_NONE
    if (ScrollBoxesAry[ScrollBoxIndex].IndicatorType != GuiLib_INDICATOR_NONE)
    {
      ScrollBoxesAry[ScrollBoxIndex].IndicatorMode = GetItemByte(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorPositionX =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorPositionY =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorSizeX =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorSizeY =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorStructIndex =
          GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorColorMode =
         GetItemByte(&ItemDataPtr);
      switch (ScrollBoxesAry[ScrollBoxIndex].IndicatorColorMode & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor = CurItem.ForeColor;
          break;

        case GuiLib_COLOR_FORE:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor = GuiConst_PIXEL_ON;
          break;

        case GuiLib_COLOR_BACK:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor =
             GuiConst_PIXEL_OFF;
          break;

        case GuiLib_COLOR_OTHER:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor =
             (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
             GetItemTriple(&ItemDataPtr);
#endif
          break;

        case GuiLib_COLOR_INVERT:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor = TmpBackColor;
          break;

        case GuiLib_COLOR_TRANSP:
          break;
      }
      ScrollBoxesAry[ScrollBoxIndex].IndicatorTransparent = 0;
      switch ((ScrollBoxesAry[ScrollBoxIndex].IndicatorColorMode & 0x38) >> 3)
      {
        case GuiLib_COLOR_NOCHANGE:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor = CurItem.BackColor;
          break;

        case GuiLib_COLOR_FORE:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor = GuiConst_PIXEL_ON;
          break;

        case GuiLib_COLOR_BACK:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor =
              GuiConst_PIXEL_OFF;
          break;

        case GuiLib_COLOR_OTHER:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor =
             (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
             GetItemTriple(&ItemDataPtr);
#endif
          break;

        case GuiLib_COLOR_INVERT:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor = TmpForeColor;
          break;

        case GuiLib_COLOR_TRANSP:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorTransparent = 1;
          break;
      }
      ScrollBoxesAry[ScrollBoxIndex].IndicatorThickness =
         GetItemByte(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerLeftOffset =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerRightOffset =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerTopOffset =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBottomOffset =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorIconPtr =
         (GuiConst_TEXT PrefixRom *)ItemDataPtr;
      ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconCharacter =
#ifdef GuiConst_CHARMODE_ANSI
         GetItemByte(&ItemDataPtr);
#else
         GetItemWord(&ItemDataPtr);
#endif
      ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconFont =
         GetItemByte(&ItemDataPtr) + 1;
      ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconOffsetX =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconOffsetY =
         GetItemWord(&ItemDataPtr);
      ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerColorMode =
         GetItemByte(&ItemDataPtr);
      switch (ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerColorMode & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor =
             CurItem.ForeColor;
          break;

        case GuiLib_COLOR_FORE:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor =
             GuiConst_PIXEL_ON;
          break;

        case GuiLib_COLOR_BACK:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor =
             GuiConst_PIXEL_OFF;
          break;

        case GuiLib_COLOR_OTHER:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor =
             (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
             GetItemTriple(&ItemDataPtr);
#endif
          break;

        case GuiLib_COLOR_INVERT:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor =
             TmpBackColor;
          break;

        case GuiLib_COLOR_TRANSP:
          break;
      }
      ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerTransparent = 0;
      switch ((ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerColorMode &
         0x38) >> 3)
      {
        case GuiLib_COLOR_NOCHANGE:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor =
             CurItem.BackColor;
          break;

        case GuiLib_COLOR_FORE:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor =
             GuiConst_PIXEL_ON;
          break;

        case GuiLib_COLOR_BACK:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor =
             GuiConst_PIXEL_OFF;
          break;

        case GuiLib_COLOR_OTHER:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor =
             (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
             GetItemTriple(&ItemDataPtr);
#endif
          break;

        case GuiLib_COLOR_INVERT:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor =
             TmpForeColor;
          break;

        case GuiLib_COLOR_TRANSP:
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerTransparent = 1;
          break;
      }
      if (ScrollBoxesAry[ScrollBoxIndex].IndicatorType == GuiLib_MARKER_BITMAP)
      {
        ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapIndex =
           GetItemWord(&ItemDataPtr);
        B1 = GetItemByte(&ItemDataPtr);
        ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapIsTransparent =
           (B1 & 0x01);
#ifdef GuiConst_BITMAP_SUPPORT_ON
        if (ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapIsTransparent)
#ifdef GuiConst_COLOR_DEPTH_1
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapTranspColor =
             (B1 >> 1) & 0x01;
#else
#ifdef GuiLib_COLOR_BYTESIZE_1
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapTranspColor =
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapTranspColor =
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
          ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapTranspColor =
             GetItemTriple(&ItemDataPtr);
#endif
#endif
#endif
      }
      ScrollBoxesAry[ScrollBoxIndex].IndicatorLine = -1;
    }
#endif
  }
#endif

#ifdef GuiConst_ITEM_GRAPH_INUSE
  if (ItemTypeBit & GuiLib_ITEMBIT_GRAPH)
  {
    GraphIndex = GetItemByte(&ItemDataPtr);
    GlobalGraphIndex = GraphIndex;

    GraphAry[GraphIndex].InUse = GuiLib_SCROLL_STRUCTURE_USED;
    GraphAry[GraphIndex].OriginOffsetX = GetItemWord(&ItemDataPtr);
    GraphAry[GraphIndex].OriginOffsetY = GetItemWord(&ItemDataPtr);

    for (Axis = GuiLib_GRAPHAXIS_X; Axis <= GuiLib_GRAPHAXIS_Y; Axis++)
    {
      GraphAry[GraphIndex].GraphAxesCnt[Axis] = GetItemByte(&ItemDataPtr);
      for (L = 0; L < GraphAry[GraphIndex].GraphAxesCnt[Axis]; L++)
      {
        B = GetItemByte(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].Visible = 1;
        GraphAry[GraphIndex].GraphAxes[L][Axis].Line = B & 0x01;
        GraphAry[GraphIndex].GraphAxes[L][Axis].LineNegative = (B >> 1) & 0x01;
        GraphAry[GraphIndex].GraphAxes[L][Axis].Arrow = (B >> 2) & 0x01;
        GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMajor = (B >> 3) & 0x01;
        GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMinor = (B >> 4) & 0x01;
        GraphAry[GraphIndex].GraphAxes[L][Axis].Numbers = (B >> 5) & 0x01;
        GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersAtOrigo = (B >> 6) & 0x01;
        GraphAry[GraphIndex].GraphAxes[L][Axis].Offset =
           GetItemWord(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].ArrowLength =
           GetItemWord(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].ArrowWidth =
           GetItemWord(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMajorLength =
           GetItemWord(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMajorWidth =
           GetItemWord(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMinorLength =
           GetItemWord(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMinorWidth =
           GetItemWord(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersMinValue =
           GetItemLong(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersMaxValue =
           GetItemLong(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersStepMajor =
           GetItemLong(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersStepMinor =
           GetItemLong(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersOffset =
           GetItemWord(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersAtEnd =
           GetItemWord(&ItemDataPtr);
        GraphAry[GraphIndex].GraphAxes[L][Axis].Scale = 10000;
      }
    }

    GraphAry[GraphIndex].GraphDataSetCnt = GetItemByte(&ItemDataPtr);
    for (L = 0; L < GraphAry[GraphIndex].GraphDataSetCnt; L++)
    {
      GraphAry[GraphIndex].GraphDataSets[L].DataSize = 0;
      GraphAry[GraphIndex].GraphDataSets[L].DataFirst = 0;
      GraphAry[GraphIndex].GraphDataSets[L].DataCount = 0;
GraphAry[GraphIndex].GraphDataSets[L].AxisIndexX = 0;
GraphAry[GraphIndex].GraphDataSets[L].AxisIndexY = 0;
      GraphAry[GraphIndex].GraphDataSets[L].Visible = 1;
      GraphAry[GraphIndex].GraphDataSets[L].Representation =
         GetItemByte(&ItemDataPtr);
      GraphAry[GraphIndex].GraphDataSets[L].Width =
         GetItemWord(&ItemDataPtr);
      GraphAry[GraphIndex].GraphDataSets[L].Height =
         GetItemWord(&ItemDataPtr);
      GraphAry[GraphIndex].GraphDataSets[L].Thickness =
         GetItemWord(&ItemDataPtr);
      GraphAry[GraphIndex].GraphDataSets[L].ColorMode =
         GetItemByte(&ItemDataPtr);
      switch (GraphAry[GraphIndex].GraphDataSets[L].ColorMode & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          GraphAry[GraphIndex].GraphDataSets[L].ForeColor = CurItem.ForeColor;
          break;

        case GuiLib_COLOR_FORE:
          GraphAry[GraphIndex].GraphDataSets[L].ForeColor = GuiConst_PIXEL_ON;
          break;

        case GuiLib_COLOR_BACK:
          GraphAry[GraphIndex].GraphDataSets[L].ForeColor = GuiConst_PIXEL_OFF;
          break;

        case GuiLib_COLOR_OTHER:
          GraphAry[GraphIndex].GraphDataSets[L].ForeColor = (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
             GetItemTriple(&ItemDataPtr);
#endif
          break;

        case GuiLib_COLOR_INVERT:
          GraphAry[GraphIndex].GraphDataSets[L].ForeColor = TmpBackColor;
          break;

        case GuiLib_COLOR_TRANSP:
          break;
      }
      GraphAry[GraphIndex].GraphDataSets[L].BackColorTransparent = 0;
      switch ((GraphAry[GraphIndex].GraphDataSets[L].ColorMode & 0x38) >> 3)
      {
        case GuiLib_COLOR_NOCHANGE:
          GraphAry[GraphIndex].GraphDataSets[L].BackColor = CurItem.BackColor;
          break;

        case GuiLib_COLOR_FORE:
          GraphAry[GraphIndex].GraphDataSets[L].BackColor = GuiConst_PIXEL_ON;
          break;

        case GuiLib_COLOR_BACK:
          GraphAry[GraphIndex].GraphDataSets[L].BackColor = GuiConst_PIXEL_OFF;
          break;

        case GuiLib_COLOR_OTHER:
          GraphAry[GraphIndex].GraphDataSets[L].BackColor = (GuiConst_INTCOLOR)
#ifdef GuiLib_COLOR_BYTESIZE_1
             GetItemByte(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
             GetItemWord(&ItemDataPtr);
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
             GetItemTriple(&ItemDataPtr);
#endif
          break;

        case GuiLib_COLOR_INVERT:
          GraphAry[GraphIndex].GraphDataSets[L].BackColor = TmpForeColor;
          break;

        case GuiLib_COLOR_TRANSP:
          GraphAry[GraphIndex].GraphDataSets[L].BackColorTransparent = 1;
          break;
      }
    }
  }
#endif
}

//------------------------------------------------------------------------------
static void DrawStructure(
   GuiLib_StructPtr Structure,
   GuiConst_INT8U ColorInvert) PrefixReentrant
{
  GuiConst_INT16S ItemNdx;
  GuiConst_INT16S X;
#ifdef GuiConst_SCROLL_SUPPORT_ON
  GuiConst_INT16S Y;
  GuiConst_INT8U Thickness;
#endif
  void *XVarPtr;
  void *YVarPtr;
#ifdef GuiConst_SCROLL_SUPPORT_ON
  GuiConst_INT8U ThisIsScrollBar;
  GuiConst_INT8U ThisIsScrollLine;
#endif
  GuiConst_INT8U ItemCnt;
  GuiConst_INT8U *LocalItemDataPtr;
  GuiConst_INT16U ItemPtrNdx;

  LocalItemDataPtr = (GuiConst_INT8U*)Structure;
  ItemDataBufCnt = 0;
  ItemPtrNdx = GetItemWord(&LocalItemDataPtr);
  ItemCnt = GetItemByte(&LocalItemDataPtr);

  for (ItemNdx = 0; ItemNdx < ItemCnt; ItemNdx++)
  {
    ItemDataPtr = LocalItemDataPtr;
    ReadItem(GuiLib_LanguageIndex);
    LocalItemDataPtr = ItemDataPtr;

    AutoRedrawSaveIndex = -1;

    if (ItemTypeBit & (GuiLib_ITEMBIT_CLEARAREA +
                       GuiLib_ITEMBIT_TEXT +
                       GuiLib_ITEMBIT_TEXTBLOCK +
                       GuiLib_ITEMBIT_DOT +
                       GuiLib_ITEMBIT_LINE +
                       GuiLib_ITEMBIT_FRAME +
                       GuiLib_ITEMBIT_BLOCK +
                       GuiLib_ITEMBIT_CIRCLE +
                       GuiLib_ITEMBIT_ELLIPSE +
                       GuiLib_ITEMBIT_BITMAP +
                       GuiLib_ITEMBIT_BACKGROUND +
                       GuiLib_ITEMBIT_STRUCTURE +
                       GuiLib_ITEMBIT_STRUCTARRAY +
                       GuiLib_ITEMBIT_ACTIVEAREA +
                       GuiLib_ITEMBIT_CLIPRECT +
                       GuiLib_ITEMBIT_VAR +
                       GuiLib_ITEMBIT_VARBLOCK +
                       GuiLib_ITEMBIT_TOUCHAREA +
                       GuiLib_ITEMBIT_SCROLLBOX +
                       GuiLib_ITEMBIT_GRAPH))
    {
      if (CommonByte2 & 0x10)
      {
#ifdef GuiConst_AVRGCC_COMPILER
        XVarPtr = (void*)pgm_read_word(&GuiStruct_ItemPtrList[ItemPtrNdx]);
#else
        XVarPtr = (void*)GuiStruct_ItemPtrList[ItemPtrNdx];
#endif
        ItemPtrNdx++;
      }
      else
        XVarPtr = 0;
      if (XVarPtr != 0)
        ItemX1 += ReadVar(XVarPtr, X1VarType);
      if (X1MemoryRead > 0)
        ItemX1 += XMemory[X1MemoryRead - 1];
      if (X1Mode == GuiLib_COOR_REL)
        ItemX1 += CurItem.RX;
      else if (X1Mode == GuiLib_COOR_REL_1)
        ItemX1 += CurItem.RX1;
      else if (X1Mode == GuiLib_COOR_REL_2)
        ItemX1 += CurItem.RX2;
      if (X1MemoryWrite > 0)
        XMemory[X1MemoryWrite - 1] = ItemX1;
      CurItem.X1 = ItemX1;

      if (CommonByte2 & 0x20)
      {
#ifdef GuiConst_AVRGCC_COMPILER
        YVarPtr = (void*)pgm_read_word(&GuiStruct_ItemPtrList[ItemPtrNdx]);
#else
        YVarPtr = (void*)GuiStruct_ItemPtrList[ItemPtrNdx];
#endif
        ItemPtrNdx++;
      }
      else
        YVarPtr = 0;
      if (YVarPtr != 0)
        ItemY1 += ReadVar(YVarPtr, Y1VarType);
      if (Y1MemoryRead > 0)
        ItemY1 += YMemory[Y1MemoryRead - 1];
      if (Y1Mode == GuiLib_COOR_REL)
        ItemY1 += CurItem.RY;
      else if (Y1Mode == GuiLib_COOR_REL_1)
        ItemY1 += CurItem.RY1;
      else if (Y1Mode == GuiLib_COOR_REL_2)
        ItemY1 += CurItem.RY2;
      if (Y1MemoryWrite > 0)
        YMemory[Y1MemoryWrite - 1] = ItemY1;
      CurItem.Y1 = ItemY1;

      CurItem.RX = CurItem.X1;
      CurItem.RY = CurItem.Y1;
    }

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
    if (ItemTypeBit & GuiLib_ITEMBIT_SCROLLBOX)
    {
      ScrollBoxesAry[GlobalScrollBoxIndex].X1 = CurItem.X1;
      ScrollBoxesAry[GlobalScrollBoxIndex].Y1 = CurItem.Y1;
      ScrollBoxesAry[GlobalScrollBoxIndex].BackColor = CurItem.BackColor;

#ifndef GuiConst_SCROLLITEM_BAR_NONE
      if ((ScrollBoxesAry[GlobalScrollBoxIndex].BarMode & 0x03) ==
         GuiLib_COOR_REL)
        ScrollBoxesAry[GlobalScrollBoxIndex].BarPositionX += CurItem.X1;
      if (((ScrollBoxesAry[GlobalScrollBoxIndex].BarMode >> 2) & 0x03) ==
         GuiLib_COOR_REL)
        ScrollBoxesAry[GlobalScrollBoxIndex].BarPositionY += CurItem.Y1;
#endif
#ifndef GuiConst_SCROLLITEM_INDICATOR_NONE
      if ((ScrollBoxesAry[GlobalScrollBoxIndex].IndicatorMode & 0x03) ==
         GuiLib_COOR_REL)
        ScrollBoxesAry[GlobalScrollBoxIndex].IndicatorPositionX += CurItem.X1;
      if (((ScrollBoxesAry[GlobalScrollBoxIndex].IndicatorMode >> 2) & 0x03) ==
         GuiLib_COOR_REL)
        ScrollBoxesAry[GlobalScrollBoxIndex].IndicatorPositionY += CurItem.Y1;
#endif
    }
#endif

    if (ItemTypeBit & (GuiLib_ITEMBIT_CLEARAREA +
                       GuiLib_ITEMBIT_TEXTBLOCK +
                       GuiLib_ITEMBIT_VARBLOCK +
                       GuiLib_ITEMBIT_LINE +
                       GuiLib_ITEMBIT_FRAME +
                       GuiLib_ITEMBIT_BLOCK +
                       GuiLib_ITEMBIT_CIRCLE +
                       GuiLib_ITEMBIT_ELLIPSE +
                       GuiLib_ITEMBIT_BITMAP +
                       GuiLib_ITEMBIT_BACKGROUND +
                       GuiLib_ITEMBIT_ACTIVEAREA +
                       GuiLib_ITEMBIT_CLIPRECT +
                       GuiLib_ITEMBIT_TOUCHAREA +
                       GuiLib_ITEMBIT_GRAPH))
    {
      if (CommonByte2 & 0x40)
      {
#ifdef GuiConst_AVRGCC_COMPILER
        XVarPtr = (void*)pgm_read_word(&GuiStruct_ItemPtrList[ItemPtrNdx]);
#else
        XVarPtr = (void*)GuiStruct_ItemPtrList[ItemPtrNdx];
#endif
        ItemPtrNdx++;
      }
      else
        XVarPtr = 0;
      if (XVarPtr != 0)
        ItemX2 += ReadVar(XVarPtr, X2VarType);
      if (X2MemoryRead > 0)
        ItemX2 += XMemory[X2MemoryRead - 1];
      if (X2Mode == GuiLib_COOR_REL)
        ItemX2 += CurItem.X1;
      else if (X2Mode == GuiLib_COOR_REL_1)
        ItemX2 += CurItem.RX1;
      else if (X2Mode == GuiLib_COOR_REL_2)
        ItemX2 += CurItem.RX2;
      if (X2MemoryWrite > 0)
        XMemory[X2MemoryWrite - 1] = ItemX2;
      CurItem.X2 = ItemX2;

      if (!(ItemTypeBit & GuiLib_ITEMBIT_CIRCLE))
      {
        if (CommonByte2 & 0x80)
        {
#ifdef GuiConst_AVRGCC_COMPILER
          YVarPtr = (void*)pgm_read_word(&GuiStruct_ItemPtrList[ItemPtrNdx]);
#else
          YVarPtr = (void*)GuiStruct_ItemPtrList[ItemPtrNdx];
#endif
          ItemPtrNdx++;
        }
        else
          YVarPtr = 0;
        if (YVarPtr != 0)
          ItemY2 += ReadVar(YVarPtr, Y2VarType);
        if (Y2MemoryRead > 0)
          ItemY2 += YMemory[Y2MemoryRead - 1];
        if (Y2Mode == GuiLib_COOR_REL)
          ItemY2 += CurItem.Y1;
        else if (Y2Mode == GuiLib_COOR_REL_1)
          ItemY2 += CurItem.RY1;
        else if (Y2Mode == GuiLib_COOR_REL_2)
          ItemY2 += CurItem.RY2;
        if (Y2MemoryWrite > 0)
          YMemory[Y2MemoryWrite - 1] = ItemY2;
        CurItem.Y2 = ItemY2;
      }
    }

    if (ItemTypeBit & (GuiLib_ITEMBIT_CLEARAREA +
                       GuiLib_ITEMBIT_TEXTBLOCK +
                       GuiLib_ITEMBIT_VARBLOCK +
                       GuiLib_ITEMBIT_LINE +
                       GuiLib_ITEMBIT_FRAME +
                       GuiLib_ITEMBIT_BLOCK +
                       GuiLib_ITEMBIT_BITMAP +
                       GuiLib_ITEMBIT_BACKGROUND +
                       GuiLib_ITEMBIT_ACTIVEAREA +
                       GuiLib_ITEMBIT_CLIPRECT +
                       GuiLib_ITEMBIT_TOUCHAREA +
                       GuiLib_ITEMBIT_GRAPH))
    {
      X = CurItem.X2 - CurItem.X1 + 1;
      if (CurItem.Alignment == GuiLib_ALIGN_CENTER)
      {
        CurItem.X1 -= (X - 1) / 2;
        CurItem.X2 -= (X - 1) / 2;
      }
      else if (CurItem.Alignment == GuiLib_ALIGN_RIGHT)
      {
        CurItem.X1 -= X - 1;
        CurItem.X2 -= X - 1;
      }
    }
    else if (ItemTypeBit & (GuiLib_ITEMBIT_CIRCLE +
                            GuiLib_ITEMBIT_ELLIPSE))
    {
      if (CurItem.Alignment == GuiLib_ALIGN_LEFT)
        CurItem.X1 += CurItem.X2;
      else if (CurItem.Alignment == GuiLib_ALIGN_RIGHT)
        CurItem.X1 -= CurItem.X2;
    }

    if (ItemTypeBit & (GuiLib_ITEMBIT_STRUCTARRAY +
                       GuiLib_ITEMBIT_VAR +
                       GuiLib_ITEMBIT_VARBLOCK))
    {
#ifdef GuiConst_AVRGCC_COMPILER
      CurItem.VarPtr = (void*)pgm_read_word(&GuiStruct_ItemPtrList[ItemPtrNdx]);
#else
      CurItem.VarPtr = (void*)GuiStruct_ItemPtrList[ItemPtrNdx];
#endif
      ItemPtrNdx++;
    }

#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
    if ((CurItem.BitFlags & GuiLib_BITFLAG_AUTOREDRAWFIELD) &&
       (ItemTypeBit & (GuiLib_ITEMBIT_TEXT +
                       GuiLib_ITEMBIT_TEXTBLOCK +
                       GuiLib_ITEMBIT_DOT +
                       GuiLib_ITEMBIT_LINE +
                       GuiLib_ITEMBIT_FRAME +
                       GuiLib_ITEMBIT_BLOCK +
                       GuiLib_ITEMBIT_CIRCLE +
                       GuiLib_ITEMBIT_ELLIPSE +
                       GuiLib_ITEMBIT_BITMAP +
                       GuiLib_ITEMBIT_BACKGROUND +
                       GuiLib_ITEMBIT_STRUCTURE)))
    {
  #ifdef GuiConst_AVRGCC_COMPILER
      CurItem.VarPtr = (void*)pgm_read_word(&GuiStruct_ItemPtrList[ItemPtrNdx]);
  #else
      CurItem.VarPtr = (void*)GuiStruct_ItemPtrList[ItemPtrNdx];
  #endif
      ItemPtrNdx++;
    }
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if ((ItemTypeBit & (GuiLib_ITEMBIT_ACTIVEAREA +
                        GuiLib_ITEMBIT_CLIPRECT)) &&
       (CurItem.BitFlags & GuiLib_BITFLAG_CLIPPING))
    {
      CurItem.ClipRectX1 = CurItem.X1;
      CurItem.ClipRectY1 = CurItem.Y1;
      CurItem.ClipRectX2 = CurItem.X2;
      CurItem.ClipRectY2 = CurItem.Y2;
    }
#endif

#ifdef GuiConst_SCROLL_SUPPORT_ON
    ThisIsScrollBar = 0;
    ThisIsScrollLine = 0;
    Thickness = CurItem.FrameThickness;
    if (Thickness < 1)
      Thickness = 1;
    if ((!ScrollBoxDetected) &&
        (MarkedAs == GuiLib_MARK_AS_SCROLL_BOX) &&
        (ItemTypeBit & (GuiLib_ITEMBIT_FRAME +
                        GuiLib_ITEMBIT_BLOCK +
                        GuiLib_ITEMBIT_CIRCLE +
                        GuiLib_ITEMBIT_ELLIPSE +
                        GuiLib_ITEMBIT_BITMAP +
                        GuiLib_ITEMBIT_BACKGROUND +
                        GuiLib_ITEMBIT_STRUCTURE +
                        GuiLib_ITEMBIT_STRUCTARRAY +
                        GuiLib_ITEMBIT_CLIPRECT)))
    {
      ScrollBoxDetected = 1;
      ScrollBoxX1 = CurItem.X1;
      ScrollBoxY1 = CurItem.Y1;
      ScrollBoxX2 = CurItem.X2;
      ScrollBoxY2 = CurItem.Y2;
      if (ItemTypeBit & GuiLib_ITEMBIT_FRAME)
      {
        ScrollBoxX1 += Thickness;
        ScrollBoxY1 += Thickness;
        ScrollBoxX2 -= Thickness;
        ScrollBoxY2 -= Thickness;
      }
    }
    else if ((!ScrollBarDetected) &&
             (MarkedAs == GuiLib_MARK_AS_SCROLL_BAR) &&
             (ItemTypeBit & (GuiLib_ITEMBIT_FRAME +
                             GuiLib_ITEMBIT_BLOCK +
                             GuiLib_ITEMBIT_CIRCLE +
                             GuiLib_ITEMBIT_ELLIPSE +
                             GuiLib_ITEMBIT_BITMAP +
                             GuiLib_ITEMBIT_BACKGROUND +
                             GuiLib_ITEMBIT_STRUCTURE +
                             GuiLib_ITEMBIT_STRUCTARRAY +
                             GuiLib_ITEMBIT_CLIPRECT)))
    {
      ThisIsScrollBar = 1;
      ScrollBarDetected = 1;
      ScrollBarX1 = CurItem.X1;
      ScrollBarY1 = CurItem.Y1;
      ScrollBarX2 = CurItem.X2;
      ScrollBarY2 = CurItem.Y2;
      if (ItemTypeBit & GuiLib_ITEMBIT_FRAME)
      {
        ScrollBarX1 += Thickness;
        ScrollBarY1 += Thickness;
        ScrollBarX2 -= Thickness;
        ScrollBarY2 -= Thickness;
      }
      ScrollBarSize = ScrollBarX2 - ScrollBarX1;
      ScrollBarArrowHeight = (ScrollBarSize / 2) + 2;
      ScrollBarIndicatorRange = ScrollBarY2 - ScrollBarY1 -
         2 * (ScrollBarSize / 2) - 4 - ScrollBarSize;
      ScrollBarMarkerForeColor = CurItem.ForeColor;
      ScrollBarMarkerBackColor = CurItem.BackColor;
    }
    else if ((!ScrollLineDetected) && (MarkedAs == GuiLib_MARK_AS_SCROLL_LINE))
    {
      ThisIsScrollLine = 1;
      InsideScrollLine = 1;
      ScrollLineDetected = 1;
      ScrollLineDY = ScrLineDy;
      GuiLib_ScrollVisibleLines =
         ((ScrollBoxY2 - ScrollBoxY1 + 1) / ScrollLineDY);

      memcpy(&ScrollLineItem, &CurItem, sizeof(GuiLib_ItemRec));
    }
#endif

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  if (ItemTypeBit & GuiLib_ITEMBIT_SCROLLBOX)
    memcpy(&ScrollBoxesAry[GlobalScrollBoxIndex].ScrollBoxItem, &CurItem,
       sizeof(GuiLib_ItemRec));
#endif
#ifdef GuiConst_ITEM_GRAPH_INUSE
  if (ItemTypeBit & GuiLib_ITEMBIT_GRAPH)
  {
    memcpy(&GraphAry[GlobalGraphIndex].GraphItem, &CurItem,
       sizeof(GuiLib_ItemRec));
    OrderCoord(&GraphAry[GlobalGraphIndex].GraphItem.X1,
               &GraphAry[GlobalGraphIndex].GraphItem.X2);
    OrderCoord(&GraphAry[GlobalGraphIndex].GraphItem.Y1,
               &GraphAry[GlobalGraphIndex].GraphItem.Y2);
    GraphAry[GlobalGraphIndex].OrigoX =
       GraphAry[GlobalGraphIndex].GraphItem.X1 +
       GraphAry[GlobalGraphIndex].OriginOffsetX;
    GraphAry[GlobalGraphIndex].OrigoY =
       GraphAry[GlobalGraphIndex].GraphItem.Y2 -
       GraphAry[GlobalGraphIndex].OriginOffsetY;
  }
#endif

#ifdef GuiConst_SCROLL_SUPPORT_ON
    DisplayWriting = !InsideScrollLine;
#else
    DisplayWriting = 1;
#endif
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
    if (NextScrollLineReading)
      DisplayWriting = 0;
#endif
    DrawItem(ColorInvert);

    DisplayWriting = 1;

#ifdef GuiConst_SCROLL_SUPPORT_ON
    if (ThisIsScrollLine)
      InsideScrollLine = 0;
    if (ThisIsScrollBar)
      if (DisplayWriting)
      {
        for (Y = 0; Y < ScrollBarSize / 2; Y++)
          GuiLib_HLine(ScrollBarX1 + 1 + Y,
                       ScrollBarX1 - 1 + ScrollBarSize - Y,
                       ScrollBarY1 + (ScrollBarSize / 2) - Y,
                       CurItem.ForeColor);

        GuiLib_HLine(ScrollBarX1, ScrollBarX2,
                     ScrollBarY1 + 2 + (ScrollBarSize / 2),
                     CurItem.ForeColor);

        GuiLib_HLine(ScrollBarX1, ScrollBarX2,
                     ScrollBarY2 - 2 - (ScrollBarSize / 2),
                     CurItem.ForeColor);

        for (Y = 0; Y < ScrollBarSize / 2; Y++)
          GuiLib_HLine(ScrollBarX1 + 1 + Y,
                       ScrollBarX1 - 1 + ScrollBarSize - Y,
                       ScrollBarY2 - (ScrollBarSize / 2) + Y,
                       CurItem.ForeColor);
      }
#endif

    switch (CurItem.ItemType)
    {
      case GuiLib_ITEM_TEXT:
      case GuiLib_ITEM_VAR:
        CurItem.RX1 = FontWriteX1;
        CurItem.RY1 = FontWriteY1;
        CurItem.RX2 = FontWriteX2 + 1;
        CurItem.RY2 = FontWriteY2;
        break;

      case GuiLib_ITEM_DOT:
        CurItem.RX1 = CurItem.X1;
        CurItem.RY1 = CurItem.Y1;
        CurItem.RX2 = CurItem.X1 + 1;
        CurItem.RY2 = CurItem.Y1;
        break;

      case GuiLib_ITEM_CIRCLE:
        CurItem.RX1 = CurItem.X1 - CurItem.X2;
        CurItem.RY1 = CurItem.Y1 - CurItem.X2;
        CurItem.RX2 = CurItem.X1 + CurItem.X2 + 1;
        CurItem.RY2 = CurItem.Y1 + CurItem.X2;
        break;

      case GuiLib_ITEM_ELLIPSE:
        CurItem.RX1 = CurItem.X1 - CurItem.X2;
        CurItem.RY1 = CurItem.Y1 - CurItem.Y2;
        CurItem.RX2 = CurItem.X1 + CurItem.X2 + 1;
        CurItem.RY2 = CurItem.Y1 + CurItem.Y2;
        break;

      case GuiLib_ITEM_CLEARAREA:
      case GuiLib_ITEM_TEXTBLOCK:
      case GuiLib_ITEM_VARBLOCK:
      case GuiLib_ITEM_LINE:
      case GuiLib_ITEM_FRAME:
      case GuiLib_ITEM_BLOCK:
      case GuiLib_ITEM_BITMAP:
      case GuiLib_ITEM_BACKGROUND:
      case GuiLib_ITEM_ACTIVEAREA:
      case GuiLib_ITEM_CLIPRECT:
      case GuiLib_ITEM_TOUCHAREA:
        CurItem.RX1 = CurItem.X1;
        CurItem.RY1 = CurItem.Y1;
        CurItem.RX2 = CurItem.X2 + 1;
        CurItem.RY2 = CurItem.Y2;
        break;
    }

    if (AutoRedrawSaveIndex >= 0)
    {
      AutoRedrawItems[AutoRedrawSaveIndex].Drawn = CurItem.Drawn;
      AutoRedrawItems[AutoRedrawSaveIndex].DrawnX1 = CurItem.DrawnX1;
      AutoRedrawItems[AutoRedrawSaveIndex].DrawnY1 = CurItem.DrawnY1;
      AutoRedrawItems[AutoRedrawSaveIndex].DrawnX2 = CurItem.DrawnX2;
      AutoRedrawItems[AutoRedrawSaveIndex].DrawnY2 = CurItem.DrawnY2;
    }
  }
}

//------------------------------------------------------------------------------
void GuiLib_ShowScreen(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16S CursorFieldToShow,
   GuiConst_INT8U ResetAutoRedraw)
{
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  GuiConst_INT16S M;
#endif
  GuiConst_INT16S N;
  GuiLib_StructPtr StructureToCall;

  GuiDisplay_Lock();

  GuiLib_CurStructureNdx = StructureNdx;
  StructureToCall =
#ifdef GuiConst_REMOTE_STRUCT_DATA
     GetRemoteStructData(StructureNdx);
#else
#ifdef GuiConst_AVRGCC_COMPILER
     (GuiLib_StructPtr)pgm_read_word(&GuiStruct_StructPtrList[StructureNdx]);
#else
     (GuiLib_StructPtr)GuiStruct_StructPtrList[StructureNdx];
#endif
#endif

#ifdef GuiConst_CURSOR_SUPPORT_ON
  CursorFieldFound = -1;
  CursorActiveFieldFound = 0;
  GuiLib_ActiveCursorFieldNo = CursorFieldToShow;
  CursorInUse = (GuiLib_ActiveCursorFieldNo >= 0);
#else
  Dummy1_16S = CursorFieldToShow;   // To avoid compiler warning
#endif

  if ((GuiLib_StructPtr)StructureToCall != 0)
  {
    CurItem.X1 = 0;
    CurItem.Y1 = 0;
    CurItem.X2 = 0;
    CurItem.Y2 = 0;

    CurItem.RX = 0;
    CurItem.RY = 0;
    CurItem.RX1 = 0;
    CurItem.RY1 = 0;
    CurItem.RX2 = 0;
    CurItem.RY2 = 0;

    for (N = 0; N < GuiLib_MEMORY_CNT; N++)
    {
      XMemory[N] = 0;
      YMemory[N] = 0;
    }

    CurItem.BitFlags = 0;

    CurItem.ForeColor = GuiConst_PIXEL_ON;
    CurItem.BackColor = GuiConst_PIXEL_OFF;
    CurItem.BarForeColor = GuiConst_PIXEL_OFF;
    CurItem.BarBackColor = GuiConst_PIXEL_ON;

    CurItem.BitFlags |= GuiLib_BITFLAG_TRANSPARENT;
    CurItem.Alignment = GuiLib_ALIGN_LEFT;
    CurItem.BackBoxSizeX = 0;
    CurItem.FontIndex = 0;
    CurItem.Ps = GuiLib_PS_ON;
    CurItem.FormatFieldWidth = 10;
    CurItem.FormatDecimals = 0;
    CurItem.FormatAlignment = GuiLib_FORMAT_ALIGNMENT_RIGHT;
    CurItem.FormatFormat = GuiLib_FORMAT_DEC;

#ifdef GuiConst_CURSOR_SUPPORT_ON
#ifndef GuiConst_CURSOR_FIELDS_OFF
    for (N = 0; N < GuiConst_CURSOR_FIELDS_MAX; N++)
      CursorItems[N].BitFlags = 0;
#endif
#endif

#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
    for (N = 0; N < GuiConst_BLINK_FIELDS_MAX; N++)
    {
      BlinkTextItems[N].InUse = 0;
      BlinkTextItems[N].Active = 0;
    }
#endif
#endif

#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
    for (N = 0; N < GuiConst_TOUCHAREA_MAX; N++)
      TouchAreas[N].InUse = 0;
#endif

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
    for (M = 0; M < GuiConst_SCROLLITEM_BOXES_MAX; M++)
      for (N = 0; N < GuiConst_SCROLLITEM_MARKERS_MAX; N++)
      {
        ScrollBoxesAry[M].MarkerColor[N] = GuiConst_PIXEL_ON;
        ScrollBoxesAry[M].MarkerColorTransparent[N] = 1;
      }
#endif

#ifdef GuiConst_ITEM_GRAPH_INUSE
    for (N = 0; N < GuiConst_GRAPH_MAX; N++)
      GraphAry[N].InUse = GuiLib_GRAPH_STRUCTURE_UNDEF;
#endif

#ifdef GuiConst_BITMAP_SUPPORT_ON
    GlobalBackgrBitmapIndex = 0;
    for (N = 0; N < GuiConst_MAX_BACKGROUND_BITMAPS; N++)
      BackgrBitmapAry[N].InUse = 0;
#endif

    if (ResetAutoRedraw)
    {
      for (N = 0; N < GuiConst_AUTOREDRAW_FIELDS_MAX; N++)
        AutoRedrawItems[N].BitFlags = 0;
      GuiLib_AutoRedrawFieldNo = 0;
    }
    else
    {
#ifdef GuiConst_CURSOR_SUPPORT_ON
      for (N = 0; N < GuiConst_AUTOREDRAW_FIELDS_MAX; N++)
        if (AutoRedrawItems[N].BitFlags & GuiLib_BITFLAG_INUSE)
          AutoRedrawItems[N].CursorFieldNo = GuiLib_NO_CURSOR;
#endif
    }
    DrawingLevel = 0;
    TopLevelStructure = 0;

#ifdef GuiConst_TEXTBOX_FIELDS_ON
    CurItem.TextBoxScrollIndex = 0;
    CurItem.TextBoxScrollPos = 0;
#endif

    CoordOrigoX = DisplayOrigoX;
    CoordOrigoY = DisplayOrigoY;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    CurItem.ClipRectX1 = DisplayActiveAreaX1;
    CurItem.ClipRectY1 = DisplayActiveAreaY1;
    CurItem.ClipRectX2 = DisplayActiveAreaX2;
    CurItem.ClipRectY2 = DisplayActiveAreaY2;
    ActiveAreaX1 = DisplayActiveAreaX1;
    ActiveAreaY1 = DisplayActiveAreaY1;
    ActiveAreaX2 = DisplayActiveAreaX2;
    ActiveAreaY2 = DisplayActiveAreaY2;
    if (DisplayWriting)
    {
      if ((DisplayActiveAreaX1 != 0) || (DisplayActiveAreaY1 != 0) ||
          (DisplayActiveAreaX2 != GuiConst_DISPLAY_WIDTH - 1) ||
          (DisplayActiveAreaY2 != GuiConst_DISPLAY_HEIGHT - 1))
        GuiLib_SetClipping(DisplayActiveAreaX1, DisplayActiveAreaY1,
                           DisplayActiveAreaX2, DisplayActiveAreaY2);
      else
        GuiLib_ResetClipping();
    }
#endif
#ifdef GuiConst_CURSOR_SUPPORT_ON
    CurItem.CursorFieldLevel = 0;
#ifdef GuiConst_SCROLL_SUPPORT_ON
    CursorFieldsInScrollLine = 0;
#endif
#endif
#ifdef GuiConst_SCROLL_SUPPORT_ON
    ScrollBoxDetected = 0;
    ScrollBarDetected = 0;
    ScrollLineDetected = 0;
    InsideScrollLine = 0;
#endif
    DisplayLevel = 0;
    InitialDrawing = 1;
    SwapColors = 0;
    DrawStructure((GuiLib_StructPtr) StructureToCall,
                   GuiLib_COL_INVERT_IF_CURSOR);
    InitialDrawing = 0;
    CoordOrigoX = DisplayOrigoX;
    CoordOrigoY = DisplayOrigoY;

#ifdef GuiConst_SCROLL_SUPPORT_ON
    if (ScrollBoxDetected && ScrollLineDetected && (ScrollNoOfLines > 0))
    {
      if (ScrollNoOfLines > GuiLib_ScrollVisibleLines)
      {
        GuiLib_ScrollTopLine =
          GuiLib_ScrollActiveLine - (GuiLib_ScrollVisibleLines / 2);
        if (GuiLib_ScrollTopLine < 0)
          GuiLib_ScrollTopLine = 0;
        else if (GuiLib_ScrollTopLine >
                 ScrollNoOfLines - GuiLib_ScrollVisibleLines)
          GuiLib_ScrollTopLine = ScrollNoOfLines - GuiLib_ScrollVisibleLines;
      }
      else
        GuiLib_ScrollTopLine = 0;

      DrawScrollList();
      DrawScrollIndicator();
    }
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if (DisplayWriting)
      GuiLib_ResetClipping();
#endif
  }

#ifdef GuiConst_CURSOR_SUPPORT_ON
  if (CursorFieldFound == -1)
    CursorInUse = 0;
  else if (CursorInUse)
  {
    if (CursorActiveFieldFound == 0)
    {
      GuiLib_ActiveCursorFieldNo = CursorFieldFound;

      DrawCursorItem(1);
    }
  }
#endif

#ifdef GuiConst_BLINK_SUPPORT_ON
  BlinkBoxInverted = 0;
#endif

  GuiDisplay_Unlock();
}

//------------------------------------------------------------------------------
GuiConst_INT16S CheckLanguageIndex(
   GuiConst_INT16S LanguageIndex)
{
  if ((LanguageIndex < 0) || (LanguageIndex > GuiConst_LANGUAGE_CNT - 1))
    LanguageIndex = 0;

#ifdef GuiConst_LANGUAGE_SOME_ACTIVE
#ifdef GuiConst_AVRGCC_COMPILER
  if (!pgm_read_byte(&GuiFont_LanguageActive[LanguageIndex]))
#else
  if (!GuiFont_LanguageActive[LanguageIndex])
#endif
    LanguageIndex = GuiConst_LANGUAGE_FIRST;
#endif

  return(LanguageIndex);
}

//------------------------------------------------------------------------------
GuiConst_TEXT *GuiLib_GetTextLanguagePtr(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16U TextNo,
   GuiConst_INT16S LanguageIndex)
{
  GuiConst_INT16U I;
  GuiConst_INT16S ItemNdx;
  GuiConst_INT8U ItemCnt;
  GuiLib_StructPtr StructureToCall;

  StructureToCall =
#ifdef GuiConst_REMOTE_STRUCT_DATA
     GetRemoteStructData(StructureNdx);
#else
#ifdef GuiConst_AVRGCC_COMPILER
     (GuiLib_StructPtr)pgm_read_word(&GuiStruct_StructPtrList[StructureNdx]);
#else
     (GuiLib_StructPtr)GuiStruct_StructPtrList[StructureNdx];
#endif
#endif

  if (StructureToCall != 0)
  {
    LanguageIndex = CheckLanguageIndex(LanguageIndex);

    ItemDataPtr = (GuiConst_INT8U*)StructureToCall + 2;
    ItemDataBufCnt = 0;
    ItemCnt = GetItemByte(&ItemDataPtr);
    I = 0;

    for (ItemNdx = 0; ItemNdx < ItemCnt; ItemNdx++)
    {
      ReadItem(LanguageIndex);

      if ((CurItem.ItemType == GuiLib_ITEM_TEXT) ||
         (CurItem.ItemType == GuiLib_ITEM_TEXTBLOCK))
      {
        if (I == TextNo)
        {
#ifdef GuiConst_CHARMODE_ANSI
          return (GuiConst_TEXT *) CurItem.TextPtr;
#else
          #ifdef GuiConst_ICC_COMPILER
          ExtractUnicodeString((GuiConst_INT8U *)CurItem.TextPtr);
          #else
          #ifdef GuiConst_CODEVISION_COMPILER
          ExtractUnicodeString((GuiConst_INT8U *)CurItem.TextPtr);
          #else
          ExtractUnicodeString((GuiConst_INT8U PrefixRom *)CurItem.TextPtr);
          #endif
          #endif

          return (UnicodeTextBuf);
#endif
        }
        I++;
      }
    }
  }

  return (0);
}

//------------------------------------------------------------------------------
GuiConst_TEXT *GuiLib_GetTextPtr(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16U TextNo)
{
  return (GuiLib_GetTextLanguagePtr(StructureNdx, TextNo, GuiLib_LanguageIndex));
}

//------------------------------------------------------------------------------
GuiConst_INT16U GuiLib_GetTextWidth(
   GuiConst_TEXT *String,
   GuiLib_FontRecConstPtr Font,
   GuiConst_INT8U PsWriting)
{
  GuiLib_ItemRec TextData;
  GuiConst_INT16U CharCnt;

  if ((String[0] != 0) && (Font != 0))
  {
#ifdef GuiConst_CHARMODE_ANSI
    SetLanguageCharSet(GuiLib_LanguageCharSet);
#endif
    TextData.Ps = PsWriting;
    CurItem.Ps = TextData.Ps;
#ifdef GuiConst_CHARMODE_ANSI
    CharCnt = strlen(String);
#else
    CharCnt = GuiLib_UnicodeStrLen(String);
#endif
    CurFont = (GuiLib_FontRecPtr)Font;
#ifdef GuiConst_AVRGCC_COMPILER
    displayVarNow = 1;
#endif
#ifdef GuiConst_ICC_COMPILER
    displayVarNow = 1;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
    displayVarNow = 1;
#endif

#ifdef GuiConst_CODEVISION_COMPILER
    PrepareText((GuiConst_TEXT *)String, CharCnt);
#else
    PrepareText(String, CharCnt);
#endif
#ifdef GuiConst_AVRGCC_COMPILER
    displayVarNow = 0;
#endif
#ifdef GuiConst_ICC_COMPILER
    displayVarNow = 0;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
    displayVarNow = 0;
#endif

    return (TextPixelLength(TextData.Ps, CharCnt, 0));
  }

  return (0);
}

//---------------------------------------------------------------
static GuiConst_TEXT GetCharCode(
   GuiConst_TEXT PrefixRom * CharPtr,
   GuiConst_INT16U CharCnt,
   GuiConst_INT16U CharNo,
   GuiConst_INT16U OmitCtrlCode)
{
  GuiConst_INT16U P;
  GuiConst_TEXT CharCode, PreviousCharCode;

  // 2012.10.17 ajv - Added CharCode and PreviousCharCode = NULL to resolve
  // compilation warnings when compiling using ARM Release.
  CharCode = NULL;
  PreviousCharCode = NULL;

  if ((CharNo > CharCnt) || (CharNo > GuiConst_MAX_TEXT_LEN))
    return 0;

  if (OmitCtrlCode)
  {
    for (P = 0; P < CharNo + 1; P++)
    {
#ifdef GuiConst_CHARMODE_ANSI
  #ifdef GuiConst_AVRGCC_COMPILER
      CharCode = (unsigned GuiConst_CHAR) pgm_read_byte(CharPtr);
  #else
  #ifdef GuiConst_ICC_COMPILER
      CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
      CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
  #else
      CharCode = (unsigned GuiConst_TEXT) *CharPtr;
  #endif
  #endif
  #endif
#else
      CharCode = (GuiConst_INT16U) *CharPtr;
#endif

      if (P)
      {
        if ((CharCode == GuiLib_LINEFEED) ||
           ((CharCode == ' ') && (PreviousCharCode == ' ')) ||
           ((CharCode == ' ') && (PreviousCharCode == '-')))
          CharNo++;
      }
      PreviousCharCode = CharCode;

      CharPtr++;
    }
  }
  else
  {
    CharPtr += CharNo;

#ifdef GuiConst_CHARMODE_ANSI
  #ifdef GuiConst_AVRGCC_COMPILER
    CharCode = (unsigned GuiConst_CHAR) pgm_read_byte(CharPtr);
  #else
  #ifdef GuiConst_ICC_COMPILER
    CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
    CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
  #else
    CharCode = (unsigned GuiConst_TEXT) *CharPtr;
  #endif
  #endif
  #endif
#else
    CharCode = (GuiConst_INT16U) *CharPtr;
#endif
  }

  return (GuiConst_TEXT)CharCode;
}

//------------------------------------------------------------------------------
GuiConst_TEXT GuiLib_GetCharCode(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16U TextNo,
   GuiConst_INT16U CharNo,
   GuiConst_INT16U OmitCtrlCode)
{
  GuiConst_TEXT PrefixRom * CharPtr;

  CharPtr = GuiLib_GetTextPtr(StructureNdx, TextNo);

#ifdef GuiConst_CHARMODE_ANSI
  return GetCharCode(CharPtr,
                     strlen(CharPtr),
                     CharNo,
                     OmitCtrlCode);
#else
  return GetCharCode(CharPtr,
                     GuiLib_UnicodeStrLen(CharPtr),
                     CharNo,
                     OmitCtrlCode);
#endif
}

//------------------------------------------------------------------------------
#ifdef GuiConst_BLINK_SUPPORT_ON
GuiConst_TEXT GuiLib_GetBlinkingCharCode(
   GuiConst_INT16U BlinkFieldNo,
   GuiConst_INT16U CharNo,
   GuiConst_INT16U OmitCtrlCode)
{
#ifndef GuiConst_BLINK_FIELDS_OFF
  GuiConst_INT16U StrLen;

  if (BlinkTextItems[BlinkFieldNo].InUse)
  {
    #ifdef GuiConst_CHARMODE_ANSI
    StrLen = strlen(BlinkTextItems[BlinkFieldNo].TextPtr);
    #else
    #ifdef GuiConst_CODEVISION_COMPILER
    StrLen = GuiLib_UnicodeStrLen(
       (GuiConst_TEXT*)BlinkTextItems[BlinkFieldNo].TextPtr);
    #else
    StrLen = GuiLib_UnicodeStrLen(BlinkTextItems[BlinkFieldNo].TextPtr);
    #endif
    #endif

    return GetCharCode(BlinkTextItems[BlinkFieldNo].TextPtr,
                       StrLen,
                       CharNo,
                       OmitCtrlCode);
  }
  else
#endif
    return 0;
}
#endif

//------------------------------------------------------------------------------
void GuiLib_SetLanguage(
   GuiConst_INT16S NewLanguage)
{
  GuiLib_LanguageIndex = CheckLanguageIndex(NewLanguage);

#ifdef GuiConst_CHARMODE_ANSI
  #ifdef GuiConst_AVRGCC_COMPILER
  GuiLib_LanguageCharSet =
     pgm_read_byte(&GuiFont_LanguageCharSets[GuiLib_LanguageIndex]);
  #else
  GuiLib_LanguageCharSet = GuiFont_LanguageCharSets[GuiLib_LanguageIndex];
  #endif
  SetLanguageCharSet(GuiLib_LanguageCharSet);
#endif
}

#ifdef GuiConst_CHARMODE_ANSI
//------------------------------------------------------------------------------
static void SetLanguageCharSet(
   GuiConst_INT8U LangCharSet)
{
  GuiLib_CurCharSet = LangCharSet;
}
#endif

#ifdef GuiConst_SCROLL_SUPPORT_ON
//------------------------------------------------------------------------------
void GuiLib_SetScrollPars(
   void (*DataFuncPtr) (GuiConst_INT16S LineIndex),
   GuiConst_INT16S NoOfLines,
   GuiConst_INT16S ActiveLine)
{
  ScrollLineDataFunc = DataFuncPtr;
  if (NoOfLines < 0)
    ScrollNoOfLines = 0;
  else
    ScrollNoOfLines = NoOfLines;

  if (ActiveLine < -1)
    ActiveLine = -1;
  else if (ActiveLine > ScrollNoOfLines - 1)
    ActiveLine = ScrollNoOfLines - 1;
  GuiLib_ScrollActiveLine = ActiveLine;
}

//------------------------------------------------------------------------------
static void DrawScrollIndicator(void)
{
  GuiConst_INT16S X, Y, YTop;

  if (ScrollBoxDetected && ScrollBarDetected &&
      (ScrollNoOfLines > GuiLib_ScrollVisibleLines))

    if (DisplayWriting)
    {
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
      CoordOrigoX = ScrollLineItem.CoordOrigoX;
      CoordOrigoY = ScrollLineItem.CoordOrigoY;
#endif

      GuiLib_FillBox(ScrollBarX1, ScrollBarY1 + ScrollBarArrowHeight + 1,
                     ScrollBarX2, ScrollBarY2 - ScrollBarArrowHeight - 1,
                     ScrollBarMarkerBackColor);

      YTop = ScrollBarY1 + ScrollBarArrowHeight +
         (GuiLib_ScrollTopLine * ScrollBarIndicatorRange) /
         (ScrollNoOfLines - GuiLib_ScrollVisibleLines);

      GuiLib_Box(ScrollBarX1, YTop, ScrollBarX2, YTop + ScrollBarSize,
                 ScrollBarMarkerForeColor);

      for (X = 1; X < ScrollBarSize; X++)
        for (Y = 1; Y < ScrollBarSize; Y++)
          if ((X + Y) % 2 == 1)
            GuiLib_Dot(ScrollBarX1 + X, YTop + Y, ScrollBarMarkerForeColor);

      CoordOrigoX = DisplayOrigoX;
      CoordOrigoY = DisplayOrigoY;
    }
}

//------------------------------------------------------------------------------
GuiConst_INT16S GuiLib_ScrollLineOffsetY(void)
{
  if (GuiLib_ScrollActiveLine == -1)
    return (0);
  else
    return ((GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine) * ScrollLineDY);
}

//------------------------------------------------------------------------------
static void ScrollLineAdjustY(
   GuiLib_ItemRecPtr Item,
   GuiConst_INT16S LineIndex)
{
  Item->Y1 += LineIndex * ScrollLineDY;
  Item->Y2 += LineIndex * ScrollLineDY;
  Item->RY += LineIndex * ScrollLineDY;
}

//------------------------------------------------------------------------------
static void DrawScrollLine(
   GuiConst_INT16S LineIndex)
{
  if ((ScrollLineDataFunc != 0) && ScrollBoxDetected && ScrollLineDetected &&
     (ScrollNoOfLines > 0))
  {
    (*ScrollLineDataFunc)(GuiLib_ScrollTopLine + LineIndex);

    memcpy(&CurItem, &ScrollLineItem, sizeof(GuiLib_ItemRec));

    ScrollLineAdjustY(&CurItem, LineIndex);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if (DisplayWriting)
    {
      CurItem.ClipRectX1 = ScrollBoxX1;
      CurItem.ClipRectY1 = ScrollBoxY1;
      CurItem.ClipRectX2 = ScrollBoxX2;
      CurItem.ClipRectY2 = ScrollBoxY2;
    }
#endif

    SwapColors = 0;
    if (GuiLib_ScrollTopLine + LineIndex == GuiLib_ScrollActiveLine)
    {
#ifdef GuiConst_CURSOR_SUPPORT_ON
      if (CursorFieldsInScrollLine)
        DrawItem(GuiLib_COL_INVERT_IF_CURSOR);
      else
#endif
        DrawItem(GuiLib_COL_INVERT_ON);
    }
    else
      DrawItem(GuiLib_COL_INVERT_OFF);
  }
}

//------------------------------------------------------------------------------
static void DrawScrollList(void)
{
  GuiConst_INT16S Y, YMax;

  if (ScrollBoxDetected && ScrollLineDetected && (ScrollNoOfLines > 0))
  {
    YMax = ScrollNoOfLines;
    if (YMax > GuiLib_ScrollVisibleLines)
      YMax = GuiLib_ScrollVisibleLines;
    for (Y = 0; Y < YMax; Y++)
      DrawScrollLine(Y);
  }
}

//------------------------------------------------------------------------------
void GuiLib_RedrawScrollList(void)
{
  DrawScrollList();
  DrawScrollIndicator();
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Scroll_Down(void)
{
  if (ScrollBoxDetected && ScrollLineDetected && (ScrollNoOfLines > 0))
  {
    if (GuiLib_ScrollActiveLine == -1)
    {
      if ((ScrollNoOfLines > GuiLib_ScrollVisibleLines) &&
          (GuiLib_ScrollTopLine < ScrollNoOfLines - GuiLib_ScrollVisibleLines))
      {
        GuiLib_ScrollTopLine++;
        DrawScrollList();
        DrawScrollIndicator();

        return (1);
      }
      else
        return (0);
    }
    else if (GuiLib_ScrollActiveLine < ScrollNoOfLines - 1)
    {
      GuiLib_ScrollActiveLine++;

      if ((ScrollNoOfLines > GuiLib_ScrollVisibleLines) &&
         ((GuiLib_ScrollActiveLine < ScrollNoOfLines - 1) ||
         ((GuiLib_ScrollVisibleLines <= 2) &&
          (GuiLib_ScrollActiveLine == ScrollNoOfLines - 1))) &&
          (GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine >=
           GuiLib_ScrollVisibleLines - 1) &&
          (GuiLib_ScrollActiveLine > GuiLib_ScrollTopLine + 1))
      {
        GuiLib_ScrollTopLine++;
        DrawScrollList();
      }
      else
      {
        DrawScrollLine(GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine - 1);
        DrawScrollLine(GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine);
      }
      DrawScrollIndicator();

      return (1);
    }
    else
#ifdef GuiConst_SCROLL_MODE_WRAP_AROUND
      return (GuiLib_Scroll_Home());
#else
      return (0);
#endif
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Scroll_Up(void)
{
  if (ScrollBoxDetected && ScrollLineDetected && (ScrollNoOfLines > 0))
  {
    if (GuiLib_ScrollActiveLine == -1)
    {
      if ((ScrollNoOfLines > GuiLib_ScrollVisibleLines) &&
          (GuiLib_ScrollTopLine > 0))
      {
        GuiLib_ScrollTopLine--;
        DrawScrollList();
        DrawScrollIndicator();

        return (1);
      }
      else
        return (0);
    }
    else if (GuiLib_ScrollActiveLine > 0)
    {
      GuiLib_ScrollActiveLine--;

      if ((ScrollNoOfLines > GuiLib_ScrollVisibleLines) &&
         ((GuiLib_ScrollActiveLine > 0) ||
         ((GuiLib_ScrollVisibleLines <= 2) &&
          (GuiLib_ScrollActiveLine == 0))) &&
          (GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine <= 0) &&
          (GuiLib_ScrollActiveLine <
           GuiLib_ScrollTopLine + GuiLib_ScrollVisibleLines - 2))
      {
        GuiLib_ScrollTopLine--;
        DrawScrollList();
      }
      else
      {
        DrawScrollLine(GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine + 1);
        DrawScrollLine(GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine);
      }
      DrawScrollIndicator();

      return (1);
    }
    else
#ifdef GuiConst_SCROLL_MODE_WRAP_AROUND
      return (GuiLib_Scroll_End());
#else
      return (0);
#endif
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Scroll_Home(void)
{
  GuiConst_INT16S RemLine;

  if (ScrollBoxDetected && ScrollLineDetected && (ScrollNoOfLines > 0))
  {
    if (GuiLib_ScrollActiveLine == -1)
    {
      if ((ScrollNoOfLines > GuiLib_ScrollVisibleLines) &&
          (GuiLib_ScrollTopLine > 0))
      {
        GuiLib_ScrollTopLine = 0;
        DrawScrollList();
        DrawScrollIndicator();

        return (1);
      }
      else
        return (0);
    }
    else if (GuiLib_ScrollActiveLine > 0)
    {
      RemLine = GuiLib_ScrollActiveLine;
      GuiLib_ScrollActiveLine = 0;
      if ((ScrollNoOfLines <= GuiLib_ScrollVisibleLines) ||
         (GuiLib_ScrollTopLine == 0))
      {
        DrawScrollLine(RemLine - GuiLib_ScrollTopLine);
        DrawScrollLine(GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine);
      }
      else
      {
        GuiLib_ScrollTopLine = 0;
        DrawScrollList();
      }
      DrawScrollIndicator();

      return (1);
    }
    else
      return (0);
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Scroll_End(void)
{
  GuiConst_INT16S RemLine;

  if (ScrollBoxDetected && ScrollLineDetected && (ScrollNoOfLines > 0))
  {
    if (GuiLib_ScrollActiveLine == -1)
    {
      if ((ScrollNoOfLines > GuiLib_ScrollVisibleLines) &&
          (GuiLib_ScrollTopLine < ScrollNoOfLines - GuiLib_ScrollVisibleLines))
      {
        GuiLib_ScrollTopLine = ScrollNoOfLines - GuiLib_ScrollVisibleLines;
        DrawScrollList();
        DrawScrollIndicator();

        return (1);
      }
      else
        return (0);
    }
    else if (GuiLib_ScrollActiveLine < ScrollNoOfLines - 1)
    {
      RemLine = GuiLib_ScrollActiveLine;
      GuiLib_ScrollActiveLine = ScrollNoOfLines - 1;
      if ((ScrollNoOfLines <= GuiLib_ScrollVisibleLines) ||
         (GuiLib_ScrollTopLine == ScrollNoOfLines - GuiLib_ScrollVisibleLines))
      {
        DrawScrollLine(RemLine - GuiLib_ScrollTopLine);
        DrawScrollLine(GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine);
      }
      else
      {
        GuiLib_ScrollTopLine = ScrollNoOfLines - GuiLib_ScrollVisibleLines;
        DrawScrollList();
      }
      DrawScrollIndicator();

      return (1);
    }
    else
      return (0);
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Scroll_To_Line(
   GuiConst_INT16S NewLine)
{
  GuiConst_INT16S RemLine;
  GuiConst_INT16S DY;

  if (NewLine > ScrollNoOfLines - 1)
    NewLine = ScrollNoOfLines - 1;
  else if (NewLine < 0)
    NewLine = 0;

  if (ScrollBoxDetected && ScrollLineDetected && (ScrollNoOfLines > 0))
  {
    if (GuiLib_ScrollActiveLine == -1)
    {
      if (ScrollNoOfLines > GuiLib_ScrollVisibleLines)
      {
        if (NewLine > ScrollNoOfLines - GuiLib_ScrollVisibleLines)
          NewLine = ScrollNoOfLines - GuiLib_ScrollVisibleLines;
        if (GuiLib_ScrollTopLine != NewLine)
        {
          GuiLib_ScrollTopLine = NewLine;
          DrawScrollList();
          DrawScrollIndicator();

          return (1);
        }
        else
          return (0);
      }
      else
        return (0);
    }
    else if (GuiLib_ScrollActiveLine != NewLine)
    {
      RemLine = GuiLib_ScrollActiveLine;
      GuiLib_ScrollActiveLine = NewLine;
      if (GuiLib_ScrollVisibleLines <= 2)
        DY = 0;
      else
        DY = 1;

      if (GuiLib_ScrollActiveLine < GuiLib_ScrollTopLine + DY)
      {
        GuiLib_ScrollTopLine = GuiLib_ScrollActiveLine - DY;
        if (GuiLib_ScrollTopLine < 0)
          GuiLib_ScrollTopLine = 0;
        DrawScrollList();
      }
      else if (GuiLib_ScrollActiveLine >
         GuiLib_ScrollTopLine + GuiLib_ScrollVisibleLines - DY - 1)
      {
        GuiLib_ScrollTopLine =
           GuiLib_ScrollActiveLine - GuiLib_ScrollVisibleLines + DY + 1;
        if (GuiLib_ScrollTopLine > ScrollNoOfLines - GuiLib_ScrollVisibleLines)
          GuiLib_ScrollTopLine = ScrollNoOfLines - GuiLib_ScrollVisibleLines;
        DrawScrollList();
      }
      else
      {
        DrawScrollLine(RemLine - GuiLib_ScrollTopLine );
        DrawScrollLine(GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine);
      }
      DrawScrollIndicator();

      return (1);
    }
    else
      return (0);
  }
  else
    return (0);
}
#endif

#ifdef GuiConst_CURSOR_SUPPORT_ON
//------------------------------------------------------------------------------
static void DrawCursorItem(
   GuiConst_INT8U CursorVisible)
{
#ifndef GuiConst_CURSOR_FIELDS_OFF
  GuiConst_INT16S RemCursorFieldNo;

  if (CursorInUse && (GuiLib_ActiveCursorFieldNo >= 0) &&
     (GuiLib_ActiveCursorFieldNo < GuiConst_CURSOR_FIELDS_MAX) &&
     (CursorItems[GuiLib_ActiveCursorFieldNo].BitFlags &
      GuiLib_BITFLAG_INUSE))
  {
    memcpy(&CurItem,
           &CursorItems[GuiLib_ActiveCursorFieldNo],
           sizeof(GuiLib_ItemRec));

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
    if (CurItem.BitFlags & GuiLib_BITFLAG_FIELDSCROLLBOX)
      ScrollBoxesAry[CurItem.CursorScrollBoxIndex].ScrollLineDataFunc(
         ScrollBoxesAry[CurItem.CursorScrollBoxIndex].MarkerStartLine[0]);
#endif
#ifdef GuiConst_SCROLL_SUPPORT_ON
    if (CurItem.BitFlags & GuiLib_BITFLAG_FIELDSCROLLS)
    {
      if (ScrollLineDataFunc != 0)
        (*ScrollLineDataFunc)(GuiLib_ScrollActiveLine);
      ScrollLineAdjustY(&CurItem,
                        GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine);
    }
#endif

    if (!CursorVisible)
    {
      RemCursorFieldNo = GuiLib_ActiveCursorFieldNo;
      GuiLib_ActiveCursorFieldNo = -1;
    }
    else
      RemCursorFieldNo = 0;

    SwapColors = 0;
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
    if (CurItem.BitFlags & GuiLib_BITFLAG_FIELDSCROLLBOX)
      ScrollBox_DrawScrollLine(CurItem.CursorScrollBoxIndex,
         ScrollBoxesAry[CurItem.CursorScrollBoxIndex].MarkerStartLine[0]);
    else
#endif
    {
#ifdef GuiConst_BITMAP_SUPPORT_ON
      UpdateBackgroundBitmap();
#endif

      DrawItem(GuiLib_COL_INVERT_IF_CURSOR);
    }

    if (!CursorVisible)
      GuiLib_ActiveCursorFieldNo = RemCursorFieldNo;
  }
#else
  CursorVisible = 0;
#endif
}

//------------------------------------------------------------------------------
void GuiLib_Cursor_Hide(void)
{
#ifndef GuiConst_CURSOR_FIELDS_OFF
  GuiDisplay_Lock();
  DrawCursorItem(0);
  GuiDisplay_Unlock();
  GuiLib_ActiveCursorFieldNo = -1;
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_IsCursorFieldInUse(
   GuiConst_INT16S AskCursorFieldNo)
{
#ifndef GuiConst_CURSOR_FIELDS_OFF
  if ((AskCursorFieldNo >= 0) &&
      (AskCursorFieldNo < GuiConst_CURSOR_FIELDS_MAX) &&
      (CursorItems[AskCursorFieldNo].BitFlags & GuiLib_BITFLAG_INUSE))
    return(1);
  else
    return(0);
#else
  return (0);
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Cursor_Select(
   GuiConst_INT16S NewCursorFieldNo)
{
#ifndef GuiConst_CURSOR_FIELDS_OFF
  if (NewCursorFieldNo == -1)
  {
    GuiLib_Cursor_Hide();
    return (0);
  }
  else if ((NewCursorFieldNo >= 0) &&
           (NewCursorFieldNo < GuiConst_CURSOR_FIELDS_MAX) &&
           (CursorItems[NewCursorFieldNo].BitFlags & GuiLib_BITFLAG_INUSE))
  {
    GuiDisplay_Lock();

    DrawCursorItem(0);
    GuiLib_ActiveCursorFieldNo = NewCursorFieldNo;
    DrawCursorItem(1);

    GuiDisplay_Unlock();

    return (1);
  }
  else
    return (0);
#else
  NewCursorFieldNo = 0;
  return (0);
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Cursor_Down(void)
{
#ifndef GuiConst_CURSOR_FIELDS_OFF
  GuiConst_INT16S NewCursorFieldNo;

  NewCursorFieldNo = GuiLib_ActiveCursorFieldNo;
  do
    if (NewCursorFieldNo >= GuiConst_CURSOR_FIELDS_MAX - 1)
  #ifdef GuiConst_CURSOR_MODE_WRAP_AROUND
      return (GuiLib_Cursor_Home());
  #else
      return (0);
  #endif
    else
      NewCursorFieldNo++;
  while (!(CursorItems[NewCursorFieldNo].BitFlags & GuiLib_BITFLAG_INUSE) &&
        (NewCursorFieldNo != GuiLib_ActiveCursorFieldNo));

  GuiLib_Cursor_Select(NewCursorFieldNo);

  return (1);
#else
  return (0);
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Cursor_Up(void)
{
#ifndef GuiConst_CURSOR_FIELDS_OFF
  GuiConst_INT16S NewCursorFieldNo;

  NewCursorFieldNo = GuiLib_ActiveCursorFieldNo;
  do
    if (NewCursorFieldNo <= 0)
  #ifdef GuiConst_CURSOR_MODE_WRAP_AROUND
      return (GuiLib_Cursor_End());
  #else
      return (0);
  #endif
    else
      NewCursorFieldNo--;
  while (!(CursorItems[NewCursorFieldNo].BitFlags & GuiLib_BITFLAG_INUSE) &&
        (NewCursorFieldNo != GuiLib_ActiveCursorFieldNo));

  GuiLib_Cursor_Select(NewCursorFieldNo);

  return (1);
#else
  return (0);
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Cursor_Home(void)
{
#ifndef GuiConst_CURSOR_FIELDS_OFF
  GuiConst_INT16S NewCursorFieldNo;

  NewCursorFieldNo = 0;
  while ((NewCursorFieldNo < GuiConst_CURSOR_FIELDS_MAX - 1) &&
     (!(CursorItems[NewCursorFieldNo].BitFlags & GuiLib_BITFLAG_INUSE)))
    NewCursorFieldNo++;
  if ((NewCursorFieldNo >= GuiConst_CURSOR_FIELDS_MAX) ||
      (NewCursorFieldNo == GuiLib_ActiveCursorFieldNo))
    return (0);
  else
  {
    GuiLib_Cursor_Select(NewCursorFieldNo);
    return (1);
  }
#else
  return (0);
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Cursor_End(void)
{
#ifndef GuiConst_CURSOR_FIELDS_OFF
  GuiConst_INT16S NewCursorFieldNo;

  NewCursorFieldNo = GuiConst_CURSOR_FIELDS_MAX - 1;
  while ((NewCursorFieldNo >= 0) &&
     (!(CursorItems[NewCursorFieldNo].BitFlags & GuiLib_BITFLAG_INUSE)))
    NewCursorFieldNo--;
  if ((NewCursorFieldNo < 0) ||
      (NewCursorFieldNo == GuiLib_ActiveCursorFieldNo))
    return (0);
  else
  {
    GuiLib_Cursor_Select(NewCursorFieldNo);
    return (1);
  }
#else
  return (0);
#endif
}
#endif

#ifdef GuiConst_BLINK_SUPPORT_ON
//------------------------------------------------------------------------------
static void BlinkBox(void)
{
  if ((BlinkBoxRate) && (DisplayWriting))
    GuiLib_InvertBox(BlinkBoxX1, BlinkBoxY1, BlinkBoxX2, BlinkBoxY2);
  BlinkBoxInverted = !BlinkBoxInverted;
}

//------------------------------------------------------------------------------
void GuiLib_BlinkBoxStart(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT16S Rate)
{
  GuiLib_BlinkBoxStop();
  BlinkBoxX1 = X1;
  BlinkBoxY1 = Y1;
  BlinkBoxX2 = X2;
  BlinkBoxY2 = Y2;
  if (Rate < 1)
    BlinkBoxRate = 1;
  else
    BlinkBoxRate = Rate;
  BlinkBoxState = BlinkBoxRate;
  BlinkBoxInverted = 0;
  BlinkBox();
}

//------------------------------------------------------------------------------
void GuiLib_BlinkBoxStop(void)
{
#ifndef GuiConst_BLINK_FIELDS_OFF
  GuiConst_INT16U I;
#endif

  if (BlinkBoxRate && BlinkBoxInverted)
    BlinkBox();
  BlinkBoxRate = 0;

#ifndef GuiConst_BLINK_FIELDS_OFF
  for (I = 0; I < GuiConst_BLINK_FIELDS_MAX; I++)
    if (BlinkTextItems[I].InUse && BlinkTextItems[I].Active)
      GuiLib_BlinkBoxMarkedItemStop(I);
#endif
}

//------------------------------------------------------------------------------
void GuiLib_BlinkBoxMarkedItem(
   GuiConst_INT16U BlinkFieldNo,
   GuiConst_INT16U CharNo,
   GuiConst_INT16S Rate)
{
#ifndef GuiConst_BLINK_FIELDS_OFF
  TextXOfsAry TextXOfs;
  GuiConst_INT8U TempPs;
  GuiConst_INT16U TempTextLength;
  GuiConst_INT8U TempFormatFieldWidth;
  GuiConst_INT8U TempFormatDecimals;
  GuiConst_INT8U TempFormatAlignment;
  GuiConst_INT8U TempFormatFormat;
  GuiConst_INT32U TempBitFlags;
  #ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT32U PrefixRom TempOfs;
  GuiConst_INT8U CharHeader[GuiLib_CHR_LINECTRL_OFS];
    #ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
  GuiConst_INT8U CharHeader1[GuiLib_CHR_LINECTRL_OFS];
  GuiConst_INT8U CharHeader2[GuiLib_CHR_LINECTRL_OFS];
    #endif
  #else
  GuiConst_INT8U PrefixRom * TempPtr;
  #endif
  #ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
  GuiConst_INT16S TextCharLineStart[GuiConst_MAX_PARAGRAPH_LINE_CNT];
  GuiConst_INT16S TextCharLineEnd[GuiConst_MAX_PARAGRAPH_LINE_CNT];
  GuiConst_INT16S M;
  GuiConst_INT16S LineCnt;
  GuiConst_INT16S LineCnt2;
  GuiConst_INT16S LineLen;
  GuiConst_INT16S XStart, XEnd;
  #endif
  GuiConst_INT16S N, P;
  GuiConst_INT16S LangIndex;
  GuiConst_TEXT *CharPtr;
  GuiConst_INT32S VarValue;
  GuiConst_INT16S TextPixelLen;

  if ((BlinkFieldNo >= GuiConst_BLINK_FIELDS_MAX) ||
      !BlinkTextItems[BlinkFieldNo].InUse)
    return;

  if (BlinkTextItems[BlinkFieldNo].Active &&
      BlinkTextItems[BlinkFieldNo].BlinkBoxRate &&
      BlinkTextItems[BlinkFieldNo].BlinkBoxInverted)
  {
    if (DisplayWriting)
    {
      GuiLib_InvertBox(BlinkTextItems[BlinkFieldNo].BlinkBoxX1,
                       BlinkTextItems[BlinkFieldNo].BlinkBoxY1,
                       BlinkTextItems[BlinkFieldNo].BlinkBoxX2,
                       BlinkTextItems[BlinkFieldNo].BlinkBoxY2);
      BlinkTextItems[BlinkFieldNo].BlinkBoxInverted =
         !BlinkTextItems[BlinkFieldNo].BlinkBoxInverted;
    }
  }

  BlinkTextItems[BlinkFieldNo].Active = 0;
  BlinkTextItems[BlinkFieldNo].BlinkBoxRate = 0;

  if ((BlinkFieldNo < GuiConst_BLINK_FIELDS_MAX) &&
       BlinkTextItems[BlinkFieldNo].InUse)
  {
    BlinkTextItems[BlinkFieldNo].BlinkBoxX1 = BlinkTextItems[BlinkFieldNo].X1;
    BlinkTextItems[BlinkFieldNo].BlinkBoxX2 = BlinkTextItems[BlinkFieldNo].X2;
    BlinkTextItems[BlinkFieldNo].BlinkBoxY1 = BlinkTextItems[BlinkFieldNo].Y1;
    BlinkTextItems[BlinkFieldNo].BlinkBoxY2 = BlinkTextItems[BlinkFieldNo].Y2;

    if (BlinkTextItems[BlinkFieldNo].InUse)
    {
      TempPs = CurItem.Ps;
      TempTextLength = CurItem.TextLength;
      TempFormatFieldWidth = CurItem.FormatFieldWidth;
      TempFormatDecimals = CurItem.FormatDecimals;
      TempFormatAlignment = CurItem.FormatAlignment;
      TempFormatFormat = CurItem.FormatFormat;
      TempBitFlags = CurItem.BitFlags;

      CurItem.FormatFieldWidth = BlinkTextItems[BlinkFieldNo].FormatFieldWidth;
      CurItem.FormatDecimals = BlinkTextItems[BlinkFieldNo].FormatDecimals;
      CurItem.FormatAlignment = BlinkTextItems[BlinkFieldNo].FormatAlignment;
      CurItem.FormatFormat = BlinkTextItems[BlinkFieldNo].FormatFormat;
      CurItem.BitFlags = BlinkTextItems[BlinkFieldNo].BitFlags;
      CurItem.TextLength = BlinkTextItems[BlinkFieldNo].CharCnt;
      CurItem.Ps = BlinkTextItems[BlinkFieldNo].Ps;
      SetCurFont(BlinkTextItems[BlinkFieldNo].FontIndex);

      if ((BlinkTextItems[BlinkFieldNo].ItemType == GuiLib_ITEM_TEXT) ||
          (BlinkTextItems[BlinkFieldNo].ItemType == GuiLib_ITEM_TEXTBLOCK))
      {
#ifdef GuiConst_CHARMODE_ANSI
        if (BlinkTextItems[BlinkFieldNo].BitFlags & GuiLib_BITFLAG_TRANSLATION)
          LangIndex = GuiLib_LanguageIndex;
        else
          LangIndex = 0;
        if (BlinkTextItems[BlinkFieldNo].CharSetSelector >= 0)
          SetLanguageCharSet(BlinkTextItems[BlinkFieldNo].CharSetSelector);
        else
  #ifdef GuiConst_AVRGCC_COMPILER
          SetLanguageCharSet(pgm_read_byte(&GuiFont_LanguageCharSets[LangIndex]));
  #else
          SetLanguageCharSet(GuiFont_LanguageCharSets[LangIndex]);
  #endif
#endif
#ifdef GuiConst_CHARMODE_UNICODE
        ExtractUnicodeString(
           (GuiConst_INT8U PrefixRom *)BlinkTextItems[BlinkFieldNo].TextPtr);
  #ifdef GuiConst_ARAB_CHARS_INUSE
        if (BlinkTextItems[BlinkFieldNo].BitFlags & GuiLib_BITFLAG_REVERSEWRITING)
        {
          BlinkTextItems[BlinkFieldNo].CharCnt = ArabicCorrection(
             UnicodeTextBuf,
             BlinkTextItems[BlinkFieldNo].CharCnt,
             (BlinkTextItems[BlinkFieldNo].BitFlags &
              GuiLib_BITFLAG_REVERSEWRITING) > 0);
        }
  #endif
#endif

#ifdef GuiConst_CHARMODE_UNICODE
        PrepareText(UnicodeTextBuf,BlinkTextItems[BlinkFieldNo].CharCnt);
        CharPtr = (GuiConst_TEXT *)UnicodeTextBuf;
#else
        PrepareText(BlinkTextItems[BlinkFieldNo].TextPtr,
           BlinkTextItems[BlinkFieldNo].CharCnt);
        CharPtr = (GuiConst_TEXT PrefixRom *)BlinkTextItems[BlinkFieldNo].TextPtr;
#endif
      }
      else
      {
#ifdef GuiConst_AVRGCC_COMPILER
        displayVarNow = 1;
#endif
#ifdef GuiConst_ICC_COMPILER
        displayVarNow = 1;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
        displayVarNow = 1;
#endif
        if (BlinkTextItems[BlinkFieldNo].VarType == GuiLib_VAR_STRING)
        {
          CharPtr = (GuiConst_TEXT*)BlinkTextItems[BlinkFieldNo].TextPtr;
#ifdef GuiConst_CHARMODE_ANSI
          BlinkTextItems[BlinkFieldNo].CharCnt = strlen(CharPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
          BlinkTextItems[BlinkFieldNo].CharCnt =
             GuiLib_UnicodeStrLen((GuiConst_TEXT*)CharPtr);
#else
          BlinkTextItems[BlinkFieldNo].CharCnt = GuiLib_UnicodeStrLen(CharPtr);
#endif
#endif
        }
        else
        {
          VarValue = ReadVar(BlinkTextItems[BlinkFieldNo].TextPtr,
                             BlinkTextItems[BlinkFieldNo].VarType);

          BlinkTextItems[BlinkFieldNo].CharCnt =
             DataNumStr(VarValue, BlinkTextItems[BlinkFieldNo].VarType);

#ifdef GuiConst_CHARMODE_ANSI
          CharPtr = (GuiConst_TEXT *) VarNumTextStr;
#else
          for (P=0; P<=BlinkTextItems[BlinkFieldNo].CharCnt; P++)
            VarNumUnicodeTextStr[P] = VarNumTextStr[P];
          CharPtr = (GuiConst_TEXT *) VarNumUnicodeTextStr;
#endif
        }
#ifdef GuiConst_CHARMODE_ANSI
        if (BlinkTextItems[BlinkFieldNo].BitFlags & GuiLib_BITFLAG_TRANSLATION)
            LangIndex = GuiLib_LanguageIndex;
        else
          LangIndex = 0;
        if (BlinkTextItems[BlinkFieldNo].CharSetSelector >= 0)
          SetLanguageCharSet(BlinkTextItems[BlinkFieldNo].CharSetSelector);
        else
  #ifdef GuiConst_AVRGCC_COMPILER
          SetLanguageCharSet(pgm_read_byte(&GuiFont_LanguageCharSets[LangIndex]));
  #else
          SetLanguageCharSet(GuiFont_LanguageCharSets[LangIndex]);
  #endif
#endif
#ifdef GuiConst_CHARMODE_ANSI
         strcpy(AnsiTextBuf, CharPtr);
#else
       GuiLib_UnicodeStrCpy(UnicodeTextBuf, CharPtr);
#endif
#ifdef   GuiConst_ARAB_CHARS_INUSE
         if (BlinkTextItems[BlinkFieldNo].BitFlags & GuiLib_BITFLAG_REVERSEWRITING)
         {
           BlinkTextItems[BlinkFieldNo].CharCnt = ArabicCorrection(
              UnicodeTextBuf, BlinkTextItems[BlinkFieldNo].CharCnt,
              (BlinkTextItems[BlinkFieldNo].BitFlags &
               GuiLib_BITFLAG_REVERSEWRITING) > 0);
         }
#endif
#ifdef GuiConst_CHARMODE_ANSI
         PrepareText(AnsiTextBuf, BlinkTextItems[BlinkFieldNo].CharCnt);
         CharPtr = AnsiTextBuf;
#else
         PrepareText(UnicodeTextBuf, BlinkTextItems[BlinkFieldNo].CharCnt);
         CharPtr = UnicodeTextBuf;
#endif

#ifdef GuiConst_AVRGCC_COMPILER
         displayVarNow = 0;
#endif
#ifdef GuiConst_ICC_COMPILER
         displayVarNow = 0;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
         displayVarNow = 0;
#endif
      }

      if (CharNo > BlinkTextItems[BlinkFieldNo].CharCnt)
      {
        SetCurFont(CurItem.FontIndex);
                CurItem.Ps = TempPs;
                CurItem.TextLength = TempTextLength;
        CurItem.FormatFieldWidth = TempFormatFieldWidth;
        CurItem.FormatDecimals = TempFormatDecimals;
        CurItem.FormatAlignment = TempFormatAlignment;
        CurItem.FormatFormat = TempFormatFormat;
        CurItem.BitFlags = TempBitFlags;
        return;
      }

      if(CharNo > 0)
      {
#ifdef GuiConst_BLINK_LF_COUNTS
        if (*(CharPtr + (CharNo - 1)) != GuiLib_LINEFEED)
#endif
        {
          BlinkTextItems[BlinkFieldNo].Active = 1;
          BlinkTextItems[BlinkFieldNo].CharNo = CharNo;
        }
      }

#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
      if ((BlinkTextItems[BlinkFieldNo].ItemType == GuiLib_ITEM_TEXTBLOCK)||
          (BlinkTextItems[BlinkFieldNo].ItemType == GuiLib_ITEM_VARBLOCK))
      {
        if(CharNo > 0)
        {
          TextPixelLength(BlinkTextItems[BlinkFieldNo].Ps,
                          BlinkTextItems[BlinkFieldNo].CharCnt,
                          (TextXOfsAryPtr)TextXOfs);

          TextCharLineStart[0] = 0;
          TextCharLineEnd[0] = -1;

          LineCnt = 1 - BlinkTextItems[BlinkFieldNo].BlindLinesAtTop;
          if (LineCnt >= 1)
            LineCnt = 1;
          LineCnt2 = 1;
          P = 0;
  #ifdef GuiConst_REMOTE_FONT_DATA
          GuiLib_RemoteDataReadBlock(
             (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[
              TextCharNdx[TextCharLineStart[LineCnt2 - 1]]],
              GuiLib_CHR_LINECTRL_OFS,
              CharHeader2);
  #endif
          while (P < BlinkTextItems[BlinkFieldNo].CharCnt)
          {
  #ifdef GuiConst_AVRGCC_COMPILER
            while ((P < BlinkTextItems[BlinkFieldNo].CharCnt - 1) &&
                !((pgm_read_byte(CharPtr + P) ==
                   GuiLib_LINEFEED) ||
                ((pgm_read_byte(CharPtr + P) !=
                  ' ') &&
                (pgm_read_byte(CharPtr + P + 1) ==
                 ' ')) ||
                ((pgm_read_byte(CharPtr + P) ==
                 '-') &&
                (pgm_read_byte(CharPtr + P + 1) !=
                 ' '))))
            P++;
  #else
            while ((P < BlinkTextItems[BlinkFieldNo].CharCnt - 1) &&
                 !((*(CharPtr + P) == GuiLib_LINEFEED) ||
    #ifdef GuiConst_BLINK_LF_COUNTS
                 ((*(CharPtr + P - 1) != ' ') && (*(CharPtr + P) == ' ')) ||
                 ((*(CharPtr + P - 1) == '-') && (*(CharPtr + P) != ' '))))
    #else
                 ((*(CharPtr + P) != ' ') && (*(CharPtr + P + 1) == ' ')) ||
                 ((*(CharPtr + P) == '-') && (*(CharPtr + P + 1) != ' '))))
    #endif
            P++;
  #endif

            if (CalcCharsWidth(TextCharLineStart[LineCnt2 - 1], P,
                  &TextXOfs, &XStart, &XEnd) >
                 (BlinkTextItems[BlinkFieldNo].X2 -
                  BlinkTextItems[BlinkFieldNo].X1 + 1))
            {
              if (TextCharLineEnd[LineCnt2 - 1] == -1)
              {
                TextCharLineEnd[LineCnt2 - 1] = P;
                TextCharLineStart[LineCnt2] = P + 1;
                TextCharLineEnd[LineCnt2] = -1;
              }
              else
              {
                TextCharLineStart[LineCnt2] = TextCharLineEnd[LineCnt2 - 1] + 1;
  #ifdef GuiConst_AVRGCC_COMPILER
                while ((TextCharLineStart[LineCnt2] < P) &&
                       (pgm_read_byte(CharPtr + TextCharLineStart[LineCnt2]) ==
                        ' '))
                  TextCharLineStart[LineCnt2]++;
  #else
                while ((TextCharLineStart[LineCnt2] < P) &&
                       (*(CharPtr +
                        TextCharLineStart[LineCnt2]) == ' '))
                  TextCharLineStart[LineCnt2]++;
  #endif
                  TextCharLineEnd[LineCnt2] = P;
              }
              if (LineCnt >= GuiConst_MAX_PARAGRAPH_LINE_CNT)
              {
                P = BlinkTextItems[BlinkFieldNo].CharCnt;
                  break;
              }
              LineCnt++;
              if (LineCnt > 1)
                LineCnt2 = LineCnt;
              else
                TextCharLineStart[LineCnt2 - 1] = TextCharLineStart[LineCnt2];
            }
            else
              TextCharLineEnd[LineCnt2 - 1] = P;
  #ifdef GuiConst_AVRGCC_COMPILER
            if (pgm_read_byte(CharPtr + P) == GuiLib_LINEFEED)
  #else
            if (*(CharPtr + P) == GuiLib_LINEFEED)
  #endif
            {
              TextCharLineEnd[LineCnt2 - 1] = P - 1;
              TextCharLineStart[LineCnt2] = P + 1;
              TextCharLineEnd[LineCnt2] = -1;

              if (LineCnt >= GuiConst_MAX_PARAGRAPH_LINE_CNT)
              {
                P = BlinkTextItems[BlinkFieldNo].CharCnt;
                break;
              }
              LineCnt++;
              if (LineCnt > 1)
                LineCnt2 = LineCnt;
              else
                TextCharLineStart[LineCnt2 - 1] = TextCharLineStart[LineCnt2];
            }
            P++;
          }

            if (BlinkTextItems[BlinkFieldNo].BitFlags &
                GuiLib_BITFLAG_REVERSEWRITING)
            {
              for (M = 0; M < BlinkTextItems[BlinkFieldNo].LineCnt ; M++)
              {
                for (P = TextCharLineStart[M]; P <= (TextCharLineStart[M] +
                   ((TextCharLineEnd[M] - TextCharLineStart[M] + 1) / 2) - 1);
                     P++)
                {
 #ifdef GuiConst_REMOTE_FONT_DATA
                  TempOfs = TextCharNdx[P];
                  TextCharNdx[P] =
                     TextCharNdx[TextCharLineEnd[M] - (P - TextCharLineStart[M])];
                  TextCharNdx[TextCharLineEnd[M] - (P - TextCharLineStart[M])] =
                     TempOfs;
 #else
                  TempPtr = TextCharPtrAry[P];
                  TextCharPtrAry[P] =
                     (GuiConst_INT8U PrefixRom *)TextCharPtrAry[
                     TextCharLineEnd[M] - (P - TextCharLineStart[M])];
                  TextCharPtrAry[TextCharLineEnd[M] - (P - TextCharLineStart[M])] =
                     (GuiConst_INT8U PrefixRom *)TempPtr;
 #endif
                }
              }
              TextPixelLength(BlinkTextItems[BlinkFieldNo].Ps,
                              BlinkTextItems[BlinkFieldNo].CharCnt,
                              (TextXOfsAryPtr)TextXOfs);
            }

 #ifdef GuiConst_TEXTBOX_FIELDS_ON
            if (BlinkTextItems[BlinkFieldNo].BlindLinesAtTop < 0)
              BlinkTextItems[BlinkFieldNo].BlindLinesAtTop = 0;
            BlinkTextItems[BlinkFieldNo].BlinkBoxY1 -=
               BlinkTextItems[BlinkFieldNo].TextBoxScrollPos -
              (BlinkTextItems[BlinkFieldNo].TextBoxLineDist *
               BlinkTextItems[BlinkFieldNo].BlindLinesAtTop);
 #endif

            switch (BlinkTextItems[BlinkFieldNo].TextBoxVertAlignment)
            {
             case GuiLib_ALIGN_CENTER:
               BlinkTextItems[BlinkFieldNo].BlinkBoxY1 +=
                  (BlinkTextItems[BlinkFieldNo].Y2 -
                   BlinkTextItems[BlinkFieldNo].Y1 + 1 -
                  (BlinkTextItems[BlinkFieldNo].YSize +
                  (BlinkTextItems[BlinkFieldNo].LineCnt - 1) *
                   BlinkTextItems[BlinkFieldNo].TextBoxLineDist)) / 2;
               break;

             case GuiLib_ALIGN_RIGHT:
               BlinkTextItems[BlinkFieldNo].BlinkBoxY1 +=
                  BlinkTextItems[BlinkFieldNo].Y2 -
                  BlinkTextItems[BlinkFieldNo].Y1 + 1 -
                 (BlinkTextItems[BlinkFieldNo].YSize +
                 (BlinkTextItems[BlinkFieldNo].LineCnt - 1) *
                  BlinkTextItems[BlinkFieldNo].TextBoxLineDist);
               break;
            }

            for (N = 0; N < BlinkTextItems[BlinkFieldNo].LineCnt; N++)
            {
              if ((CharNo - 1) <= TextCharLineEnd[N])
              {
                if (BlinkTextItems[BlinkFieldNo].BitFlags &
                      GuiLib_BITFLAG_REVERSEWRITING)
                  CharNo = TextCharLineStart[N] + TextCharLineEnd[N] + 2 - CharNo;

                BlinkTextItems[BlinkFieldNo].BlinkBoxX1 =
                   BlinkTextItems[BlinkFieldNo].X1;

                LineLen = CalcCharsWidth(TextCharLineStart[N],
                                         TextCharLineEnd[N],
                                         &TextXOfs,
                                         &XStart,
                                         &XEnd);
                switch (BlinkTextItems[BlinkFieldNo].TextBoxHorzAlignment)
                {
                  case GuiLib_ALIGN_CENTER:
                   BlinkTextItems[BlinkFieldNo].BlinkBoxX1 +=
                      (BlinkTextItems[BlinkFieldNo].X2 -
                       BlinkTextItems[BlinkFieldNo].X1 + LineLen - 1) / 2;
                   break;

                  case GuiLib_ALIGN_RIGHT:
                   BlinkTextItems[BlinkFieldNo].BlinkBoxX1 +=
                      BlinkTextItems[BlinkFieldNo].X2 -
                      BlinkTextItems[BlinkFieldNo].X1 + LineLen - 1;
                   break;
                }

                BlinkTextItems[BlinkFieldNo].BlinkBoxX1 -= XStart;
                LineLen = CalcCharsWidth(CharNo - 1,
                                         CharNo - 1,
                                         &TextXOfs,
                                         &XStart,
                                         &XEnd);
                BlinkTextItems[BlinkFieldNo].BlinkBoxX2 =
                   BlinkTextItems[BlinkFieldNo].BlinkBoxX1 + XEnd;
                BlinkTextItems[BlinkFieldNo].BlinkBoxX1 += XStart;
                BlinkTextItems[BlinkFieldNo].BlinkBoxY2 =
                   BlinkTextItems[BlinkFieldNo].BlinkBoxY1 +
                   BlinkTextItems[BlinkFieldNo].YSize - 1;
                break;
              }
              else
 #ifndef GuiConst_BLINK_LF_COUNTS
                CharNo++;
 #endif

              BlinkTextItems[BlinkFieldNo].BlinkBoxY1 +=
                 BlinkTextItems[BlinkFieldNo].TextBoxLineDist;
            }
        }
        else
        {
          BlinkTextItems[BlinkFieldNo].Active = 1;
          BlinkTextItems[BlinkFieldNo].CharNo = CharNo;
        }
      }
      else
#endif
      {
        if (BlinkTextItems[BlinkFieldNo].BitFlags & GuiLib_BITFLAG_REVERSEWRITING)
        {
          for (N = 0; N < BlinkTextItems[BlinkFieldNo].CharCnt / 2; N++)
          {
#ifdef GuiConst_REMOTE_FONT_DATA
            TempOfs = TextCharNdx[N];
            TextCharNdx[N] =
               TextCharNdx[BlinkTextItems[BlinkFieldNo].CharCnt - 1 - N];
            TextCharNdx[BlinkTextItems[BlinkFieldNo].CharCnt - 1 - N] =
               TempOfs;
#else
            TempPtr = TextCharPtrAry[N];
            TextCharPtrAry[N] = (GuiConst_INT8U PrefixRom *)
               TextCharPtrAry[BlinkTextItems[BlinkFieldNo].CharCnt - 1 - N];
            TextCharPtrAry[BlinkTextItems[BlinkFieldNo].CharCnt - 1 - N] =
              (GuiConst_INT8U PrefixRom *)TempPtr;
#endif
          }
          CharNo =  BlinkTextItems[BlinkFieldNo].CharCnt + 1 - CharNo;
        }

        TextPixelLen = TextPixelLength(BlinkTextItems[BlinkFieldNo].Ps,
                          BlinkTextItems[BlinkFieldNo].CharCnt,
                          (TextXOfsAryPtr)TextXOfs);

        switch (BlinkTextItems[BlinkFieldNo].Alignment)
        {
          case GuiLib_ALIGN_CENTER:
            if (BlinkTextItems[BlinkFieldNo].CharCnt > 0)
                BlinkTextItems[BlinkFieldNo].BlinkBoxX1 -= TextPixelLen / 2;
              break;
          case GuiLib_ALIGN_RIGHT:
            BlinkTextItems[BlinkFieldNo].BlinkBoxX1 -= TextPixelLen - 1;
              break;
        }

#ifdef GuiConst_AVRGCC_COMPILER
        BlinkTextItems[BlinkFieldNo].BlinkBoxX2 = BlinkTextItems[BlinkFieldNo].BlinkBoxX1 +
           TextPixelLen - 1;
#else
        BlinkTextItems[BlinkFieldNo].BlinkBoxX2 = BlinkTextItems[BlinkFieldNo].BlinkBoxX1 +
           TextPixelLen - 1;
#endif

        if (CharNo)
        {
          if (BlinkTextItems[BlinkFieldNo].Ps == GuiLib_PS_OFF)
          {
            BlinkTextItems[BlinkFieldNo].BlinkBoxX1 =
               BlinkTextItems[BlinkFieldNo].BlinkBoxX1 + TextXOfs[CharNo-1] - 1;
            BlinkTextItems[BlinkFieldNo].BlinkBoxX2 =
               BlinkTextItems[BlinkFieldNo].BlinkBoxX1 +
               BlinkTextItems[BlinkFieldNo].XSize;
          }
          else if ((BlinkTextItems[BlinkFieldNo].Ps == GuiLib_PS_NUM) &&
                   (TextPsMode[CharNo - 1] == 0))
          {
               BlinkTextItems[BlinkFieldNo].BlinkBoxX1 =
                  BlinkTextItems[BlinkFieldNo].BlinkBoxX1 + TextXOfs[CharNo - 1] - 1;
               BlinkTextItems[BlinkFieldNo].BlinkBoxX2 =
                  BlinkTextItems[BlinkFieldNo].BlinkBoxX1 +
                  BlinkTextItems[BlinkFieldNo].PsNumWidth +
                  BlinkTextItems[BlinkFieldNo].PsSpace;
          }
          else
          {
#ifdef GuiConst_REMOTE_FONT_DATA
            GuiLib_RemoteDataReadBlock((GuiConst_INT32U PrefixRom)
               GuiFont_ChPtrList[TextCharNdx[CharNo - 1]],
               GuiLib_CHR_LINECTRL_OFS, CharHeader);
            BlinkTextItems[BlinkFieldNo].BlinkBoxX1 =
               BlinkTextItems[BlinkFieldNo].BlinkBoxX1 +
               TextXOfs[CharNo-1] + CharHeader[GuiLib_CHR_XLEFT_OFS] - 1;
#else
            BlinkTextItems[BlinkFieldNo].BlinkBoxX1 =
               BlinkTextItems[BlinkFieldNo].BlinkBoxX1 + TextXOfs[CharNo - 1] +
  #ifdef GuiConst_AVRGCC_COMPILER
               pgm_read_byte(TextCharPtrAry[CharNo - 1] +
               GuiLib_CHR_XLEFT_OFS) - 1;
  #else
               *(TextCharPtrAry[CharNo-1] + GuiLib_CHR_XLEFT_OFS) - 1;
  #endif
#endif
#ifdef GuiConst_REMOTE_FONT_DATA
          BlinkTextItems[BlinkFieldNo].BlinkBoxX2 =
             BlinkTextItems[BlinkFieldNo].BlinkBoxX1 +
             CharHeader[GuiLib_CHR_XWIDTH_OFS] + 1;
#else
          BlinkTextItems[BlinkFieldNo].BlinkBoxX2 =
             BlinkTextItems[BlinkFieldNo].BlinkBoxX1 +
  #ifdef GuiConst_AVRGCC_COMPILER
             pgm_read_byte(TextCharPtrAry[CharNo - 1] +
             GuiLib_CHR_XWIDTH_OFS) + 1;
  #else
             *(TextCharPtrAry[CharNo - 1] + GuiLib_CHR_XWIDTH_OFS) + 1;
  #endif
#endif
          }
        }
        else
        {
          BlinkTextItems[BlinkFieldNo].Active = 1;
          BlinkTextItems[BlinkFieldNo].CharNo = CharNo;
        }
      }
      SetCurFont(CurItem.FontIndex);
      CurItem.Ps = TempPs;
      CurItem.TextLength = TempTextLength;
      CurItem.FormatFieldWidth = TempFormatFieldWidth;
      CurItem.FormatDecimals = TempFormatDecimals;
      CurItem.FormatAlignment = TempFormatAlignment;
      CurItem.FormatFormat = TempFormatFormat;
      CurItem.BitFlags = TempBitFlags;

    }

    if (BlinkTextItems[BlinkFieldNo].Active)
    {
      if (Rate < 1)
        BlinkTextItems[BlinkFieldNo].BlinkBoxRate = 1;
      else
        BlinkTextItems[BlinkFieldNo].BlinkBoxRate = Rate;
      if (BlinkTextItems[BlinkFieldNo].BlinkBoxRate < 255)
      {
        BlinkTextItems[BlinkFieldNo].BlinkBoxState =
           BlinkTextItems[BlinkFieldNo].BlinkBoxRate -
          (RefreshClock % BlinkTextItems[BlinkFieldNo].BlinkBoxRate);
        BlinkTextItems[BlinkFieldNo].BlinkBoxInverted =
           ((RefreshClock / BlinkTextItems[BlinkFieldNo].BlinkBoxRate) % 2) ==
             0;
        if (BlinkTextItems[BlinkFieldNo].BlinkBoxInverted && DisplayWriting)
          GuiLib_InvertBox(BlinkTextItems[BlinkFieldNo].BlinkBoxX1,
                           BlinkTextItems[BlinkFieldNo].BlinkBoxY1,
                           BlinkTextItems[BlinkFieldNo].BlinkBoxX2,
                           BlinkTextItems[BlinkFieldNo].BlinkBoxY2);
      }
      else
      {
        BlinkTextItems[BlinkFieldNo].BlinkBoxState =
             BlinkTextItems[BlinkFieldNo].BlinkBoxRate;
        BlinkTextItems[BlinkFieldNo].BlinkBoxInverted = 0;
      }
    }
  }
#endif
}

//------------------------------------------------------------------------------
void GuiLib_BlinkBoxMarkedItemStop(
   GuiConst_INT16U BlinkFieldNo)
{
#ifndef GuiConst_BLINK_FIELDS_OFF
  if ((BlinkFieldNo >= GuiConst_BLINK_FIELDS_MAX) ||
      !BlinkTextItems[BlinkFieldNo].InUse)
    return;

  if (BlinkTextItems[BlinkFieldNo].Active &&
      BlinkTextItems[BlinkFieldNo].BlinkBoxRate &&
      BlinkTextItems[BlinkFieldNo].BlinkBoxInverted)
  {
    if (DisplayWriting)
    {
      GuiLib_InvertBox(BlinkTextItems[BlinkFieldNo].BlinkBoxX1,
                       BlinkTextItems[BlinkFieldNo].BlinkBoxY1,
                       BlinkTextItems[BlinkFieldNo].BlinkBoxX2,
                       BlinkTextItems[BlinkFieldNo].BlinkBoxY2);
      BlinkTextItems[BlinkFieldNo].BlinkBoxInverted =
         !BlinkTextItems[BlinkFieldNo].BlinkBoxInverted;
    }
  }
  BlinkTextItems[BlinkFieldNo].BlinkBoxRate = 0;
  BlinkTextItems[BlinkFieldNo].Active = 0;
#endif
}

//------------------------------------------------------------------------------
void GuiLib_BlinkBoxMarkedItemUpdate(
   GuiConst_INT16U BlinkFieldNo)
{
#ifndef GuiConst_BLINK_FIELDS_OFF
  if ((BlinkFieldNo >= GuiConst_BLINK_FIELDS_MAX) ||
      !BlinkTextItems[BlinkFieldNo].InUse)
    return;

  if (BlinkTextItems[BlinkFieldNo].Active &&
    ((BlinkTextItems[BlinkFieldNo].ItemType == GuiLib_ITEM_VAR) ||
     (BlinkTextItems[BlinkFieldNo].ItemType == GuiLib_ITEM_VARBLOCK)))
    GuiLib_BlinkBoxMarkedItem(
       BlinkFieldNo,
       BlinkTextItems[BlinkFieldNo].CharNo,
       BlinkTextItems[BlinkFieldNo].BlinkBoxRate);
#endif
}
#endif

//------------------------------------------------------------------------------
static void InvertBox(void)
{
  if (DisplayWriting)
    GuiLib_InvertBox(InvertBoxX1, InvertBoxY1, InvertBoxX2, InvertBoxY2);
  InvertBoxOn = !InvertBoxOn;
}

//------------------------------------------------------------------------------
void GuiLib_InvertBoxStart(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiLib_InvertBoxStop();
  InvertBoxX1 = X1;
  InvertBoxY1 = Y1;
  InvertBoxX2 = X2;
  InvertBoxY2 = Y2;
  InvertBox();
}

//------------------------------------------------------------------------------
void GuiLib_InvertBoxStop(void)
{
  if (InvertBoxOn)
    InvertBox();
}

#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
//------------------------------------------------------------------------------
GuiConst_INT16S GuiLib_TouchCheck(
   GuiConst_INT16S X,
   GuiConst_INT16S Y)
{
  GuiConst_INT8U N;
  GuiConst_INT32S XX, YY;
  GuiConst_INT32S XL, XR, YT, YB;
  GuiConst_INT16S X1, X2, Y1, Y2;

  if (TouchAdjustActive)
  {
    XX = 10 * (GuiConst_INT32S)X;
    YY = 10 * (GuiConst_INT32S)Y;
    XL = TouchAdjustXTL +
       (YY - TouchAdjustYTL) * (TouchAdjustXBL - TouchAdjustXTL) /
       (TouchAdjustYBL - TouchAdjustYTL);
    XR = TouchAdjustXTR +
       (YY - TouchAdjustYTR) * (TouchAdjustXBR - TouchAdjustXTR) /
       (TouchAdjustYBR - TouchAdjustYTR);
    YT = TouchAdjustYTL +
       (XX - TouchAdjustXTL) * (TouchAdjustYTR - TouchAdjustYTL) /
       (TouchAdjustXTR - TouchAdjustXTL);
    YB = TouchAdjustYBL +
       (XX - TouchAdjustXBL) * (TouchAdjustYBR - TouchAdjustYBL) /
       (TouchAdjustXBR - TouchAdjustXBL);
    XX = GuiConst_DISPLAY_WIDTH * (XX - XL) / (XR - XL);
    YY = GuiConst_DISPLAY_HEIGHT * (YY - YT) / (YB - YT);
  }
  else
  {
    XX = X;
    YY = Y;
  }

  if ((XX >= 0) && (XX < GuiConst_DISPLAY_WIDTH) &&
      (YY >= 0) && (YY < GuiConst_DISPLAY_HEIGHT))
    for (N = 0; N < GuiConst_TOUCHAREA_MAX; N++)
      if (TouchAreas[N].InUse)
      {
        X1 = TouchAreas[N].X1;
        X2 = TouchAreas[N].X2;
        Y1 = TouchAreas[N].Y1;
        Y2 = TouchAreas[N].Y2;
        if ((XX >= X1) && (XX <= X2) && (YY >= Y1) && (YY <= Y2))
          return (N);
      }

  return (-1);
}

//------------------------------------------------------------------------------
void GuiLib_TouchAdjustReset(void)
{
  TouchAdjustActive = 0;
  TouchAdjustInUse[0] = 0;
  TouchAdjustInUse[1] = 0;
  TouchAdjustInUse[2] = 0;
  TouchAdjustInUse[3] = 0;
}

//------------------------------------------------------------------------------
void GuiLib_TouchAdjustSet(
   GuiConst_INT16S XTrue,
   GuiConst_INT16S YTrue,
   GuiConst_INT16S XMeasured,
   GuiConst_INT16S YMeasured)
{
  GuiConst_INT8U I;
  GuiConst_INT32S XTL, YTL, XTR, YTR, XBL, YBL, XBR, YBR, XL, XR, YT, YB;

  if (YTrue < GuiConst_DISPLAY_HEIGHT / 2)
  {
    if (XTrue < GuiConst_DISPLAY_WIDTH / 2)
      I = 0;
    else
      I = 1;
  }
  else
  {
    if (XTrue < GuiConst_DISPLAY_WIDTH / 2)
      I = 2;
    else
      I = 3;
  }

  TouchAdjustInUse[I] = 1;

  TouchAdjustXTrue[I] = XTrue;
  TouchAdjustYTrue[I] = YTrue;
  TouchAdjustXMeasured[I] = XMeasured;
  TouchAdjustYMeasured[I] = YMeasured;

  if (TouchAdjustInUse[0] && TouchAdjustInUse[3])
  {
    TouchAdjustActive = 1;
    if (!TouchAdjustInUse[1])
    {
      TouchAdjustInUse[1] = 1;
      TouchAdjustXTrue[1] = TouchAdjustXTrue[3];
      TouchAdjustYTrue[1] = TouchAdjustYTrue[0];
      TouchAdjustXMeasured[1] = TouchAdjustXMeasured[3];
      TouchAdjustYMeasured[1] = TouchAdjustYMeasured[0];
    }
    if (!TouchAdjustInUse[2])
    {
      TouchAdjustInUse[2] = 1;
      TouchAdjustXTrue[2] = TouchAdjustXTrue[0];
      TouchAdjustYTrue[2] = TouchAdjustYTrue[3];
      TouchAdjustXMeasured[2] = TouchAdjustXMeasured[0];
      TouchAdjustYMeasured[2] = TouchAdjustYMeasured[3];
    }
  }
  else if (TouchAdjustInUse[1] && TouchAdjustInUse[2])
  {
    TouchAdjustActive = 1;
    if (!TouchAdjustInUse[0])
    {
      TouchAdjustInUse[0] = 1;
      TouchAdjustXTrue[0] = TouchAdjustXTrue[2];
      TouchAdjustYTrue[0] = TouchAdjustYTrue[1];
      TouchAdjustXMeasured[0] = TouchAdjustXMeasured[2];
      TouchAdjustYMeasured[0] = TouchAdjustYMeasured[1];
    }
    if (!TouchAdjustInUse[3])
    {
      TouchAdjustInUse[3] = 1;
      TouchAdjustXTrue[3] = TouchAdjustXTrue[1];
      TouchAdjustYTrue[3] = TouchAdjustYTrue[2];
      TouchAdjustXMeasured[3] = TouchAdjustXMeasured[1];
      TouchAdjustYMeasured[3] = TouchAdjustYMeasured[2];
    }
  }

  if (TouchAdjustActive)
  {
    XTL = (10 * TouchAdjustXMeasured[1]) - ((10 * TouchAdjustXTrue[1] *
       (TouchAdjustXMeasured[1] - TouchAdjustXMeasured[0])) /
       (TouchAdjustXTrue[1] - TouchAdjustXTrue[0]));
    XTR = (10 * TouchAdjustXMeasured[0]) +
       ((10 * (GuiConst_DISPLAY_WIDTH - TouchAdjustXTrue[0]) *
       (TouchAdjustXMeasured[1] - TouchAdjustXMeasured[0])) /
       (TouchAdjustXTrue[1] - TouchAdjustXTrue[0]));
    XBL = (10 * TouchAdjustXMeasured[3]) - ((10 * TouchAdjustXTrue[3] *
       (TouchAdjustXMeasured[3] - TouchAdjustXMeasured[2])) /
       (TouchAdjustXTrue[3] - TouchAdjustXTrue[2]));
    XBR = (10 * TouchAdjustXMeasured[2]) +
       ((10 * (GuiConst_DISPLAY_WIDTH - TouchAdjustXTrue[2]) *
       (TouchAdjustXMeasured[3] - TouchAdjustXMeasured[2])) /
       (TouchAdjustXTrue[3] - TouchAdjustXTrue[2]));

    YT = 5 * (TouchAdjustYTrue[0] + TouchAdjustYTrue[1]);
    YB = 5 * (TouchAdjustYTrue[2] + TouchAdjustYTrue[3]);

    TouchAdjustXTL = XBL - (YB * (XBL - XTL) / (YB - YT));
    TouchAdjustXBL =
       XTL + (((10 * GuiConst_DISPLAY_HEIGHT) - YT) * (XBL - XTL) / (YB - YT));
    TouchAdjustXTR = XBR - (YB * (XBR - XTR) / (YB - YT));
    TouchAdjustXBR =
       XTR + (((10 * GuiConst_DISPLAY_HEIGHT) - YT) * (XBR - XTR) / (YB - YT));

    YTL = (10 * TouchAdjustYMeasured[2]) - ((10 * TouchAdjustYTrue[2] *
       (TouchAdjustYMeasured[2] - TouchAdjustYMeasured[0])) /
       (TouchAdjustYTrue[2] - TouchAdjustYTrue[0]));
    YBL = (10 * TouchAdjustYMeasured[0]) +
       ((10 * (GuiConst_DISPLAY_HEIGHT - TouchAdjustYTrue[0]) *
       (TouchAdjustYMeasured[2] - TouchAdjustYMeasured[0])) /
       (TouchAdjustYTrue[2] - TouchAdjustYTrue[0]));
    YTR = (10 * TouchAdjustYMeasured[3]) - ((10 * TouchAdjustYTrue[3] *
       (TouchAdjustYMeasured[3] - TouchAdjustYMeasured[1])) /
       (TouchAdjustYTrue[3] - TouchAdjustYTrue[1]));
    YBR = (10 * TouchAdjustYMeasured[1]) +
       ((10 * (GuiConst_DISPLAY_HEIGHT - TouchAdjustYTrue[1]) *
       (TouchAdjustYMeasured[3] - TouchAdjustYMeasured[1])) /
       (TouchAdjustYTrue[3] - TouchAdjustYTrue[1]));

    XL = 5 * (TouchAdjustXTrue[0] + TouchAdjustXTrue[2]);
    XR = 5 * (TouchAdjustXTrue[1] + TouchAdjustXTrue[3]);

    TouchAdjustYTL = YTR - (XR * (YTR - YTL) / (XR - XL));
    TouchAdjustYTR =
       YTL + (((10 * GuiConst_DISPLAY_WIDTH) - XL) * (YTR - YTL) / (XR - XL));
    TouchAdjustYBL = YBR - (XR * (YBR - YBL) / (XR - XL));
    TouchAdjustYBR =
       YBL + (((10 * GuiConst_DISPLAY_WIDTH) - XL) * (YBR - YBL) / (XR - XL));
  }
}
#endif

#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
//------------------------------------------------------------------------------
static GuiConst_INT16S VarStrCmp(
   GuiConst_INT8U *S1,
   GuiConst_INT8U *S2)
{
  GuiConst_INT16S len = 0;

#ifdef GuiConst_CHARMODE_ANSI
  do
  {
    if ((*S1 == 0) && (*S2 == 0))
      return (0);

    else if (*S1 == 0)
      return (-1);
    else if (*S2 == 0)
      return (1);
    else if (*S1 < *S2)
      return (-1);
    else if (*S1 > *S2)
      return (1);
    S1++;
    S2++;
  }
  while ((len++) < GuiConst_AUTOREDRAW_MAX_VAR_SIZE);
#else
  GuiConst_INT16U T1, T2, T3;

  do
  {
    T1 = *S1++;
    T3 = *S1++;
    T1 |= T3 << 8;

    T2 = *S2++;
    T3 = *S2++;
    T2 |= T3 << 8;

    if (T1 < T2)
      return (-1);
    else if (T1 > T2)
      return (1);
    else if (T1 == 0)
      return (0);
  }
  while ((len++) < GuiConst_AUTOREDRAW_MAX_VAR_SIZE);
#endif
  return (1);
}
#endif

//------------------------------------------------------------------------------
void GuiLib_Refresh(void)
{
  GuiConst_INT16S N;
  GuiConst_INT8U RedrawBottomLevel;
#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
  GuiConst_INT16U VarLen;
  GuiConst_INT16S VarChange;
#endif

  GuiDisplay_Lock();

  RefreshClock++;

  RedrawBottomLevel = 0;
  for (N = 0; N < GuiConst_AUTOREDRAW_FIELDS_MAX; N++)
    if ((AutoRedrawItems[N].BitFlags & GuiLib_BITFLAG_INUSE) &&
        (AutoRedrawItems[N].VarPtr != 0))
    {
#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
      VarLen = AutoRedrawSize[N];
      if (VarLen > 0)
      {
        if (AutoRedrawItems[N].VarType == GuiLib_VAR_STRING)
          VarChange = VarStrCmp(&AutoRedrawOldVal[N][0],
             (GuiConst_INT8U*)AutoRedrawItems[N].VarPtr);
        else
          VarChange =
             memcmp(&AutoRedrawOldVal[N][0],AutoRedrawItems[N].VarPtr,VarLen);
      }
      if ((VarLen == 0) || (VarChange != 0))
#endif
      {
        memcpy(&CurItem, &AutoRedrawItems[N], sizeof(GuiLib_ItemRec));
#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
        if (VarLen > 0)
        {
          if (AutoRedrawItems[N].VarType == GuiLib_VAR_STRING)
  #ifdef GuiConst_CHARMODE_ANSI
            strcpy((GuiConst_TEXT*)&AutoRedrawOldVal[N][0],
               (GuiConst_TEXT*)AutoRedrawItems[N].VarPtr);
  #else
            GuiLib_UnicodeStrCpy((GuiConst_TEXT*)&AutoRedrawOldVal[N][0],
               (GuiConst_TEXT*)AutoRedrawItems[N].VarPtr);
  #endif
          else
            memcpy(&AutoRedrawOldVal[N][0],AutoRedrawItems[N].VarPtr,VarLen);
        }
#endif

#ifdef GuiConst_SCROLL_SUPPORT_ON
        if (CurItem.BitFlags & GuiLib_BITFLAG_FIELDSCROLLS)
          ScrollLineAdjustY(&CurItem,
                            GuiLib_ScrollActiveLine - GuiLib_ScrollTopLine);
#endif

#ifdef GuiConst_BITMAP_SUPPORT_ON
        UpdateBackgroundBitmap();
#endif

        DisplayLevel = 0;
        SwapColors = 0;

#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
        if ((CurItem.BitFlags & GuiLib_BITFLAG_BLINKTEXTFIELD) &&
            (CurItem.BlinkFieldNo < GuiConst_BLINK_FIELDS_MAX) &&
             BlinkTextItems[CurItem.BlinkFieldNo].InUse &&
             BlinkTextItems[CurItem.BlinkFieldNo].Active &&
             BlinkTextItems[CurItem.BlinkFieldNo].BlinkBoxInverted)
           GuiLib_InvertBox(BlinkTextItems[CurItem.BlinkFieldNo].BlinkBoxX1,
                            BlinkTextItems[CurItem.BlinkFieldNo].BlinkBoxY1,
                            BlinkTextItems[CurItem.BlinkFieldNo].BlinkBoxX2,
                            BlinkTextItems[CurItem.BlinkFieldNo].BlinkBoxY2);
#endif
#endif

#ifdef GuiConst_CURSOR_SUPPORT_ON
        if ((CurItem.BitFlags & GuiLib_BITFLAG_FIELDSCROLLS) ||
           (CursorInUse && (GuiLib_ActiveCursorFieldNo != -1) &&
           (CurItem.AutoRedrawCursorFieldNo == GuiLib_ActiveCursorFieldNo)))
#else
        if (CurItem.BitFlags & GuiLib_BITFLAG_FIELDSCROLLS)
#endif
          DrawItem(GuiLib_COL_INVERT_ON);
        else
          DrawItem(GuiLib_COL_INVERT_OFF);

#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
        if ((CurItem.BitFlags & GuiLib_BITFLAG_BLINKTEXTFIELD) &&
            (CurItem.BlinkFieldNo < GuiConst_BLINK_FIELDS_MAX) &&
             BlinkTextItems[CurItem.BlinkFieldNo].InUse &&
             BlinkTextItems[CurItem.BlinkFieldNo].Active &&
             BlinkTextItems[CurItem.BlinkFieldNo].BlinkBoxInverted)
           GuiLib_InvertBox(BlinkTextItems[CurItem.BlinkFieldNo].BlinkBoxX1,
                            BlinkTextItems[CurItem.BlinkFieldNo].BlinkBoxY1,
                            BlinkTextItems[CurItem.BlinkFieldNo].BlinkBoxX2,
                            BlinkTextItems[CurItem.BlinkFieldNo].BlinkBoxY2);
#endif
#endif

        if (AutoRedrawLevel[N] == 0)
          RedrawBottomLevel = 1;
      }
    }

  if ((DrawingLevel > 0) && RedrawBottomLevel && (TopLevelStructure != 0))
  {
    DisplayLevel = 0;
    SwapColors = 0;
    DrawStructure(TopLevelStructure, GuiLib_COL_INVERT_IF_CURSOR);
  }

#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
  for (N = 0; N < GuiConst_BLINK_FIELDS_MAX; N++)
    if (BlinkTextItems[N].InUse &&
        BlinkTextItems[N].Active &&
       (BlinkTextItems[N].BlinkBoxState < 255))
    {
      BlinkTextItems[N].BlinkBoxState--;
      if (BlinkTextItems[N].BlinkBoxState == 0)
      {
        BlinkTextItems[N].BlinkBoxState = BlinkTextItems[N].BlinkBoxRate;
        if (DisplayWriting)
        {
          GuiLib_InvertBox(BlinkTextItems[N].BlinkBoxX1,
                           BlinkTextItems[N].BlinkBoxY1,
                           BlinkTextItems[N].BlinkBoxX2,
                           BlinkTextItems[N].BlinkBoxY2);
          BlinkTextItems[N].BlinkBoxInverted =
             !BlinkTextItems[N].BlinkBoxInverted;
        }
      }
    }
  if (BlinkBoxRate)
  {
    if (BlinkBoxState < 255)
      BlinkBoxState--;
    if (BlinkBoxState == 0)
    {
      BlinkBoxState = BlinkBoxRate;
      BlinkBox();
    }
  }
#endif
#endif

  GuiDisplay_Unlock();

  GuiDisplay_Refresh();
}

//------------------------------------------------------------------------------
void GuiLib_DrawChar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U FontNo,
   GuiConst_TEXT Character,
   GuiConst_INTCOLOR Color)
{
  GuiLib_FontRecPtr Font;
  GuiConst_INT8U PrefixRom *CharPtr;
  GuiConst_INT16S X1,Y1,X2,Y2;
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT32U CharNdx;
#else
#ifdef GuiConst_CHARMODE_UNICODE
  GuiConst_INT32U CharNdx;
#endif
#endif

#ifdef GuiConst_AVRGCC_COMPILER
  Font = (GuiLib_FontRecPtr) pgm_read_word(&GuiFont_FontList[FontNo]);
#else
  Font = GuiFont_FontList[FontNo];
#endif

#ifdef GuiConst_AVRGCC_COMPILER
  Y -= pgm_read_byte(&Font->BaseLine);
#else
  Y -= Font->BaseLine;
#endif

#ifdef GuiConst_CHARMODE_ANSI
  #ifdef GuiConst_REMOTE_FONT_DATA
  if ((Character < Font->FirstChar[0]) || (Character > Font->LastChar[0]))
    CharNdx =
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[Font->IllegalCharNdx];
  else
    CharNdx =
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[Font->FirstCharNdx[0] +
       (GuiConst_INT16U)Character - (GuiConst_INT16U)Font->FirstChar[0]];
  DrawChar(X, Y, Font, CharNdx, Color);
  #else
  #ifdef GuiConst_AVRGCC_COMPILER
  if ((Character < pgm_read_byte(&Font->FirstChar[0])) ||
     (Character > pgm_read_byte(&Font->LastChar[0])))
    CharPtr = (GuiConst_INT8U*)pgm_read_word(
       &GuiFont_ChPtrList[pgm_read_word(&Font->IllegalCharNdx)]);
  else
    CharPtr = (GuiConst_INT8U*)pgm_read_word(
       &GuiFont_ChPtrList[pgm_read_word(&Font->FirstCharNdx[0]) +
       (GuiConst_INT16U)Character -
       (GuiConst_INT16U)pgm_read_byte(&Font->FirstChar[0])]);
  #else
  if ((Character < Font->FirstChar[0]) || (Character > Font->LastChar[0]))
    CharPtr =
       (GuiConst_INT8U PrefixRom *)GuiFont_ChPtrList[Font->IllegalCharNdx];
  else
    CharPtr =
       (GuiConst_INT8U PrefixRom *)GuiFont_ChPtrList[Font->FirstCharNdx[0] +
       (GuiConst_INT16U)Character - (GuiConst_INT16U)Font->FirstChar[0]];
  #endif
  DrawChar(X, Y, Font, CharPtr, Color);
  #endif
#else
  CharNdx = GetCharNdx(Font, Character);
  #ifdef GuiConst_REMOTE_FONT_DATA
  DrawChar(X, Y, Font, CharNdx, Color);
  #else
  #ifdef GuiConst_AVRGCC_COMPILER
  CharPtr = (GuiConst_INT8U*)pgm_read_word(&GuiFont_ChPtrList[CharNdx]);
  #else
  CharPtr = (GuiConst_INT8U PrefixRom *)GuiFont_ChPtrList[CharNdx];
  #endif
  DrawChar(X, Y, Font, CharPtr, Color);
  #endif
#endif

#ifdef GuiConst_REMOTE_FONT_DATA
  X1 = X + GuiLib_RemoteFontBuffer[GuiLib_CHR_XLEFT_OFS];
  X2 = X1 + GuiLib_RemoteFontBuffer[GuiLib_CHR_XWIDTH_OFS] - 1;
  Y1 = Y + GuiLib_RemoteFontBuffer[GuiLib_CHR_YTOP_OFS];
  Y2 = Y1 + GuiLib_RemoteFontBuffer[GuiLib_CHR_YHEIGHT_OFS] - 1;
#else
#ifdef GuiConst_AVRGCC_COMPILER
  X1 = X + pgm_read_byte(CharPtr + GuiLib_CHR_XLEFT_OFS);
  X2 = X1 + pgm_read_byte(CharPtr + GuiLib_CHR_XWIDTH_OFS) - 1;
  Y1 = Y + pgm_read_byte(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y2 = Y1 + pgm_read_byte(CharPtr + GuiLib_CHR_YHEIGHT_OFS) - 1;
#else
  X1 = X + *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  X2 = X1 + *(CharPtr + GuiLib_CHR_XWIDTH_OFS) - 1;
  Y1 = Y + *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y2 = Y1 + *(CharPtr + GuiLib_CHR_YHEIGHT_OFS) - 1;
#endif
#endif

  GuiLib_MarkDisplayBoxRepaint(X1,Y1,X2,Y2);
}

//------------------------------------------------------------------------------
void GuiLib_DrawStr(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U FontNo,
#ifdef GuiConst_CHARMODE_ANSI
   GuiConst_INT8S CharSetSelector,
#endif
   GuiConst_TEXT *String,
   GuiConst_INT8U Alignment,
   GuiConst_INT8U PsWriting,
   GuiConst_INT8U Transparent,
   GuiConst_INT8U Underlining,
   GuiConst_INT16S BackBoxSizeX,
   GuiConst_INT16S BackBoxSizeY1,
   GuiConst_INT16S BackBoxSizeY2,
   GuiConst_INT8U BackBorderPixels,
   GuiConst_INTCOLOR ForeColor,
   GuiConst_INTCOLOR BackColor)
{
  if (FontNo >= GuiFont_FontCnt)
    return;
  if (*String == 0)
    return;

  SetCurFont(FontNo);

#ifdef GuiConst_AVRGCC_COMPILER
  #ifdef GuiConst_CHARMODE_ANSI
  CurItem.TextPtr = (GuiConst_INT8U *)String;
  #else
  CurItem.TextPtr = (GuiConst_INT16U *)String;
  #endif
#else
#ifdef GuiConst_ICC_COMPILER
  #ifdef GuiConst_CHARMODE_ANSI
  CurItem.TextPtr = (GuiConst_INT8U *)String;
  #else
  CurItem.TextPtr = (GuiConst_INT16U *)String;
  #endif
#else
#ifdef GuiConst_CODEVISION_COMPILER
  #ifdef GuiConst_CHARMODE_ANSI
  CurItem.TextPtr = (GuiConst_INT8U *)String;
  #else
  CurItem.TextPtr = (GuiConst_INT16U *)String;
  #endif
#else
  CurItem.TextPtr = String;
#endif
#endif
#endif

  CurItem.TextLength = 0;
  while (*String != 0)
  {
    CurItem.TextLength++;
    String++;
  }
  CurItem.X1 = X;
  CurItem.Y1 = Y;
  CurItem.FontIndex = FontNo;
#ifdef GuiConst_CHARMODE_ANSI
  CurItem.CharSetSelector = CharSetSelector;
  if (CurItem.CharSetSelector >= 0)
    SetLanguageCharSet(CurItem.CharSetSelector);
  else
  #ifdef GuiConst_AVRGCC_COMPILER
    SetLanguageCharSet(
       pgm_read_byte(&GuiFont_LanguageCharSets[GuiLib_LanguageIndex]));
  #else
    SetLanguageCharSet(GuiFont_LanguageCharSets[GuiLib_LanguageIndex]);
  #endif
#endif
  CurItem.Alignment = Alignment;
  CurItem.Ps = PsWriting;
  CurItem.BitFlags = 0;
  if (Underlining)
    CurItem.BitFlags |= GuiLib_BITFLAG_UNDERLINE;
  CurItem.BackBoxSizeX = BackBoxSizeX;
  CurItem.BackBoxSizeY1 = BackBoxSizeY1;
  CurItem.BackBoxSizeY2 = BackBoxSizeY2;
  CurItem.BackBorderPixels = BackBorderPixels;
  if (CurItem.BackBoxSizeX > 0)
    DrawBackBox(BackColor, Transparent);

#ifdef GuiConst_AVRGCC_COMPILER
  displayVarNow = 1;
#endif
#ifdef GuiConst_ICC_COMPILER
  displayVarNow = 1;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
  displayVarNow = 1;
#endif

  DrawText(CurItem.TextPtr,
           CurItem.TextLength,
           ForeColor,
           BackColor,
           Transparent);

#ifdef GuiConst_AVRGCC_COMPILER
  displayVarNow = 0;
#endif
#ifdef GuiConst_ICC_COMPILER
  displayVarNow = 0;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
  displayVarNow = 0;
#endif
}

//------------------------------------------------------------------------------
void GuiLib_DrawVar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U FontNo,
#ifdef GuiConst_CHARMODE_ANSI
   GuiConst_INT8S CharSetSelector,
#endif
   void *VarPtr,
   GuiConst_INT8U VarType,
   GuiConst_INT8U FormatterFormat,
   GuiConst_INT8U FormatterFieldWidth,
   GuiConst_INT8U FormatterAlignment,
   GuiConst_INT8U FormatterDecimals,
   GuiConst_INT8U FormatterShowSign,
   GuiConst_INT8U FormatterZeroPadding,
   GuiConst_INT8U FormatterTrailingZeros,
   GuiConst_INT8U FormatterThousandsSeparator,
   GuiConst_INT8U Alignment,
   GuiConst_INT8U PsWriting,
   GuiConst_INT8U Transparent,
   GuiConst_INT8U Underlining,
   GuiConst_INT16S BackBoxSizeX,
   GuiConst_INT16S BackBoxSizeY1,
   GuiConst_INT16S BackBoxSizeY2,
   GuiConst_INT8U BackBorderPixels,
   GuiConst_INTCOLOR ForeColor,
   GuiConst_INTCOLOR BackColor)
{
  GuiConst_INT32S VarValue;
  GuiConst_INT16U StrLen;
#ifndef GuiConst_CHARMODE_ANSI
  GuiConst_INT16U P;
#endif
  GuiConst_TEXT *CharPtr;

  if (FontNo >= GuiFont_FontCnt)
    return;

  SetCurFont(FontNo);

  CurItem.VarPtr = VarPtr;
  CurItem.VarType = VarType;
  CurItem.FormatFormat = FormatterFormat;
  CurItem.FormatFieldWidth = FormatterFieldWidth;
  CurItem.FormatAlignment = FormatterAlignment;
  CurItem.FormatDecimals = FormatterDecimals;
  CurItem.BitFlags = 0;
  if (FormatterShowSign)
    CurItem.BitFlags |= GuiLib_BITFLAG_FORMATSHOWSIGN;
  if (FormatterZeroPadding)
    CurItem.BitFlags |= GuiLib_BITFLAG_FORMATZEROPADDING;
  if (FormatterTrailingZeros)
    CurItem.BitFlags |= GuiLib_BITFLAG_FORMATTRAILINGZEROS;
  if (FormatterThousandsSeparator)
    CurItem.BitFlags |= GuiLib_BITFLAG_FORMATTHOUSANDSSEP;
  CurItem.X1 = X;
  CurItem.Y1 = Y;
  CurItem.FontIndex = FontNo;
#ifdef GuiConst_CHARMODE_ANSI
  CurItem.CharSetSelector = CharSetSelector;
  if (CurItem.CharSetSelector >= 0)
    SetLanguageCharSet(CurItem.CharSetSelector);
  else
  #ifdef GuiConst_AVRGCC_COMPILER
    SetLanguageCharSet(
       pgm_read_byte(&GuiFont_LanguageCharSets[GuiLib_LanguageIndex]));
  #else
    SetLanguageCharSet(GuiFont_LanguageCharSets[GuiLib_LanguageIndex]);
  #endif
#endif
  CurItem.Alignment = Alignment;
  CurItem.Ps = PsWriting;
  if (Underlining)
    CurItem.BitFlags |= GuiLib_BITFLAG_UNDERLINE;
  CurItem.BackBoxSizeX = BackBoxSizeX;
  CurItem.BackBoxSizeY1 = BackBoxSizeY1;
  CurItem.BackBoxSizeY2 = BackBoxSizeY2;
  CurItem.BackBorderPixels = BackBorderPixels;
  if (CurItem.BackBoxSizeX > 0)
    DrawBackBox(BackColor, Transparent);

#ifdef GuiConst_AVRGCC_COMPILER
  displayVarNow = 1;
#endif
#ifdef GuiConst_ICC_COMPILER
  displayVarNow = 1;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
  displayVarNow = 1;
#endif

  if (CurItem.VarType == GuiLib_VAR_STRING)
  {
    CharPtr = (GuiConst_TEXT*)CurItem.VarPtr;
#ifdef GuiConst_CHARMODE_ANSI
    StrLen = strlen(CharPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
    StrLen = GuiLib_UnicodeStrLen((GuiConst_TEXT*)CharPtr);
#else
    StrLen = GuiLib_UnicodeStrLen(CharPtr);
#endif
#endif
  }
  else
  {
    VarValue = ReadVar(CurItem.VarPtr, CurItem.VarType);
    StrLen = DataNumStr(VarValue, CurItem.VarType);
#ifdef GuiConst_CHARMODE_ANSI
    CharPtr = (GuiConst_TEXT *) VarNumTextStr;
#else
    for (P = 0; P <= StrLen; P++)
      VarNumUnicodeTextStr[P] = VarNumTextStr[P];
    CharPtr = (GuiConst_TEXT *) VarNumUnicodeTextStr;
#endif
  }

#ifdef GuiConst_CHARMODE_ANSI
  if (CurItem.CharSetSelector >= 0)
    SetLanguageCharSet(CurItem.CharSetSelector);
  else
#ifdef GuiConst_AVRGCC_COMPILER
    SetLanguageCharSet(
       pgm_read_byte(&GuiFont_LanguageCharSets[GuiLib_LanguageIndex]));
#else
    SetLanguageCharSet(GuiFont_LanguageCharSets[GuiLib_LanguageIndex]);
#endif
#endif
  DrawText(CharPtr, StrLen, ForeColor, BackColor, Transparent);

#ifdef GuiConst_AVRGCC_COMPILER
  displayVarNow = 0;
#endif
#ifdef GuiConst_ICC_COMPILER
  displayVarNow = 0;
#endif
#ifdef GuiConst_CODEVISION_COMPILER
  displayVarNow = 0;
#endif
}

//------------------------------------------------------------------------------
void GuiLib_TestPattern(void)
{
  GuiLib_HLine(0, 31, 0, GuiConst_PIXEL_ON);
  GuiLib_VLine(0, 1, 31, GuiConst_PIXEL_ON);
  GuiLib_HLine(2, 8, 2, GuiConst_PIXEL_ON);
  GuiLib_HLine(11, 16, 2, GuiConst_PIXEL_ON);
  GuiLib_VLine(4, 4, 10, GuiConst_PIXEL_ON);
  GuiLib_VLine(4, 13, 18, GuiConst_PIXEL_ON);
}

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
//------------------------------------------------------------------------------
static void ScrollBox_ShowLineStruct(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16U StructToCallIndex,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT8U ColorInvert)
{
  GuiLib_StructPtr StructToCall;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiConst_INT16S RemClippingX1, RemClippingY1, RemClippingX2, RemClippingY2;
#endif

  if (StructToCallIndex != 0xFFFF)
  {
    DisplayLevel++;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    RemClippingX1 = CurItem.ClipRectX1;
    RemClippingY1 = CurItem.ClipRectY1;
    RemClippingX2 = CurItem.ClipRectX2;
    RemClippingY2 = CurItem.ClipRectY2;
#endif
    memcpy(&CurItem, &ScrollBoxesAry[ScrollBoxIndex].ScrollBoxItem,
       sizeof(GuiLib_ItemRec));
    CurItem.RX = X;
    CurItem.RY = Y;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    CurItem.ClipRectX1 = RemClippingX1;
    CurItem.ClipRectY1 = RemClippingY1;
    CurItem.ClipRectX2 = RemClippingX2;
    CurItem.ClipRectY2 = RemClippingY2;
#endif
    StructToCall = (GuiLib_StructPtr)
#ifdef GuiConst_REMOTE_STRUCT_DATA
       GetRemoteStructData(StructToCallIndex);
#else
#ifdef GuiConst_AVRGCC_COMPILER
       pgm_read_word(&GuiStruct_StructPtrList[StructToCallIndex]);
#else
       GuiStruct_StructPtrList[StructToCallIndex];
#endif
#endif

#ifdef GuiConst_CURSOR_SUPPORT_ON
    if (ColorInvert == GuiLib_COL_INVERT_ON)
    {
      if (ScrollBoxesAry[ScrollBoxIndex].ContainsCursorFields)
        ColorInvert = GuiLib_COL_INVERT_IF_CURSOR;
    }
#endif

    DrawStructure(StructToCall, ColorInvert);
    DisplayLevel--;
  }
}

//------------------------------------------------------------------------------
static void ScrollBox_ShowLineBlock(
   GuiConst_INTCOLOR Color,
   GuiConst_INT8U Transparent,
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  if (!Transparent)
    GuiLib_FillBox(X1, Y1, X2, Y2, Color);
}

//------------------------------------------------------------------------------
// 2011.09.26 ajv - Added #ifndef GuiConst_SCROLLITEM_BAR_NONE to resolve
// compilation warnings when -Wunused-variable flag is set.
#ifndef GuiConst_SCROLLITEM_BAR_NONE
static void ScrollBox_ShowBarBlock(
   GuiConst_INTCOLOR ForeColor,
   GuiConst_INTCOLOR BackColor,
   GuiConst_INT8U Thickness,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S SizeX,
   GuiConst_INT16S SizeY)
{
  GuiConst_INT8U T;
  GuiConst_INT16S BX1, BY1, BX2, BY2;

  if (Thickness > 0)
    for (T = 0; T < Thickness; T++)
    {
      BX1 = X + T;
      BY1 = Y + T;
      BX2 = X + SizeX - 1 - T;
      BY2 = Y + SizeY - 1 - T;
      GuiLib_Box(BX1, BY1, BX2, BY2, ForeColor);
    }
  BX1 = X + Thickness;
  BY1 = Y + Thickness;
  BX2 = X + SizeX - 1 - Thickness;
  BY2 = Y + SizeY - 1 - Thickness;
  GuiLib_FillBox(BX1, BY1, BX2, BY2, BackColor);
}
#endif

//------------------------------------------------------------------------------
static void ScrollBox_SetTopLine(
   GuiConst_INT8U ScrollBoxIndex)
{
  if (ScrollBoxesAry[ScrollBoxIndex].ScrollMode == 0)
  {
    ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
       ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] -
       ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs;
    GuiLib_LIMIT_MINMAX(ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine,
       0, ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
       ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines);
  }
  else
    ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
       ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] -
       ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs;
}

//------------------------------------------------------------------------------
static void ScrollBox_DrawScrollLine(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16S LineNdx)
{
  GuiConst_INT16U N;
  GuiConst_INT16S SX1, SY1;
  GuiConst_INT16S X1, Y1, X2, Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiConst_INT16S RemClippingX1, RemClippingY1, RemClippingX2, RemClippingY2;
#endif

  if ((LineNdx < ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine) ||
      (LineNdx >= ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine +
                  ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines))
    return;

  X1 = ScrollBoxesAry[ScrollBoxIndex].X1 +
       ScrollBoxesAry[ScrollBoxIndex].LineOffsetX;
  Y1 = ScrollBoxesAry[ScrollBoxIndex].Y1 +
       ScrollBoxesAry[ScrollBoxIndex].LineOffsetY +
       (LineNdx - ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine) *
       ScrollBoxesAry[ScrollBoxIndex].LineVerticalOffset;
  X2 = X1 + ScrollBoxesAry[ScrollBoxIndex].LineSizeX - 1;
  Y2 = Y1 + ScrollBoxesAry[ScrollBoxIndex].LineSizeY - 1;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  RemClippingX1 = CurItem.ClipRectX1;
  RemClippingY1 = CurItem.ClipRectY1;
  RemClippingX2 = CurItem.ClipRectX2;
  RemClippingY2 = CurItem.ClipRectY2;
  GuiLib_LIMIT_MINMAX(CurItem.ClipRectX1, X1, X2);
  GuiLib_LIMIT_MINMAX(CurItem.ClipRectY1, Y1, Y2);
  GuiLib_LIMIT_MINMAX(CurItem.ClipRectX2, X1, X2);
  GuiLib_LIMIT_MINMAX(CurItem.ClipRectY2, Y1, Y2);
  GuiLib_SetClipping(CurItem.ClipRectX1, CurItem.ClipRectY1,
                     CurItem.ClipRectX2, CurItem.ClipRectY2);
#endif

  if ((LineNdx >= 0) &&
      (LineNdx < ScrollBoxesAry[ScrollBoxIndex].NumberOfLines))
  {
    ScrollBoxesAry[ScrollBoxIndex].ScrollLineDataFunc(LineNdx);

    SX1 = X1 + ScrollBoxesAry[ScrollBoxIndex].LineStructOffsetX;
    SY1 = Y1 + ScrollBoxesAry[ScrollBoxIndex].LineStructOffsetY;
    for (N = 0; N <= GuiConst_SCROLLITEM_MARKERS_MAX; N++)
    {
      if (N < GuiConst_SCROLLITEM_MARKERS_MAX)
      {
        if ((ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[N] >= 0) &&
            (ScrollBoxesAry[ScrollBoxIndex].MarkerSize[N] >= 1))
        {
          if ((LineNdx >= ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[N]) &&
              (LineNdx <= (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[N] +
               ScrollBoxesAry[ScrollBoxIndex].MarkerSize[N] - 1)))
          {
            switch (ScrollBoxesAry[ScrollBoxIndex].MarkerDrawingOrder[N])
            {
              case 0:
                ScrollBox_ShowLineStruct(ScrollBoxIndex,
                   ScrollBoxesAry[ScrollBoxIndex].LineStructIndex, SX1, SY1,
                   GuiLib_COL_INVERT_ON);
                if (ScrollBoxesAry[ScrollBoxIndex].MarkerStructIndex[N] ==
                    0xFFFF)
                  ScrollBox_ShowLineBlock(
                     ScrollBoxesAry[ScrollBoxIndex].MarkerColor[N],
                     ScrollBoxesAry[ScrollBoxIndex].
                     MarkerColorTransparent[N], X1, Y1, X2, Y2);
                else
                  ScrollBox_ShowLineStruct(ScrollBoxIndex,
                     ScrollBoxesAry[ScrollBoxIndex].MarkerStructIndex[N],
                     SX1, SY1, GuiLib_COL_INVERT_ON);
                break;

              case 1:
                if (ScrollBoxesAry[ScrollBoxIndex].MarkerStructIndex[N] ==
                    0xFFFF)
                  ScrollBox_ShowLineBlock(
                     ScrollBoxesAry[ScrollBoxIndex].MarkerColor[N],
                     ScrollBoxesAry[ScrollBoxIndex].
                     MarkerColorTransparent[N], X1, Y1, X2, Y2);
                else
                  ScrollBox_ShowLineStruct(ScrollBoxIndex,
                     ScrollBoxesAry[ScrollBoxIndex].MarkerStructIndex[N],
                     SX1, SY1, GuiLib_COL_INVERT_ON);
                ScrollBox_ShowLineStruct(ScrollBoxIndex,
                   ScrollBoxesAry[ScrollBoxIndex].LineStructIndex, SX1, SY1,
                   GuiLib_COL_INVERT_ON);
                break;

              case 2:
                ScrollBox_ShowLineStruct(ScrollBoxIndex,
                   ScrollBoxesAry[ScrollBoxIndex].LineStructIndex, SX1, SY1,
                   GuiLib_COL_INVERT_ON);
                break;

              case 3:
                if (ScrollBoxesAry[ScrollBoxIndex].MarkerStructIndex[N] ==
                    0xFFFF)
                  ScrollBox_ShowLineBlock(
                     ScrollBoxesAry[ScrollBoxIndex].MarkerColor[N],
                     ScrollBoxesAry[ScrollBoxIndex].
                     MarkerColorTransparent[N], X1, Y1, X2, Y2);
                else
                  ScrollBox_ShowLineStruct(ScrollBoxIndex,
                     ScrollBoxesAry[ScrollBoxIndex].MarkerStructIndex[N],
                     SX1, SY1, GuiLib_COL_INVERT_ON);
                break;
            }

            break;
          }
        }
      }
      else
      {
        ScrollBox_ShowLineBlock(
           ScrollBoxesAry[ScrollBoxIndex].LineColor,
           ScrollBoxesAry[ScrollBoxIndex].LineColorTransparent,
           X1, Y1, X2, Y2);
        ScrollBox_ShowLineStruct(ScrollBoxIndex,
           ScrollBoxesAry[ScrollBoxIndex].LineStructIndex, SX1, SY1,
           GuiLib_COL_INVERT_OFF);
      }
    }
  }
  else
    ScrollBox_ShowLineBlock(ScrollBoxesAry[ScrollBoxIndex].LineColor,
       ScrollBoxesAry[ScrollBoxIndex].LineColorTransparent,
       X1, Y1, X2, Y2);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  CurItem.ClipRectX1 = RemClippingX1;
  CurItem.ClipRectY1 = RemClippingY1;
  CurItem.ClipRectX2 = RemClippingX2;
  CurItem.ClipRectY2 = RemClippingY2;
  GuiLib_SetClipping(CurItem.ClipRectX1, CurItem.ClipRectY1,
                     CurItem.ClipRectX2, CurItem.ClipRectY2);
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_Init(
   GuiConst_INT8U ScrollBoxIndex,
   void (*DataFuncPtr) (GuiConst_INT16S LineIndex),
   GuiConst_INT16S NoOfLines,
   GuiConst_INT16S ActiveLine)
{
  GuiConst_INT16U  StructToCallIndex;
  GuiLib_StructPtr StructToCall;
  GuiLib_ItemRec   RemCurItem;
#ifdef GuiConst_CURSOR_SUPPORT_ON
  GuiConst_INT8U RemCursorInUse;
#endif

  GuiLib_LIMIT_MINMAX(ActiveLine, -1, NoOfLines - 1);

  if (ScrollBoxesAry[ScrollBoxIndex].InUse == GuiLib_SCROLL_STRUCTURE_READ)
  {
    memcpy(&RemCurItem, &CurItem, sizeof(GuiLib_ItemRec));

    GlobalScrollBoxIndex = ScrollBoxIndex;

    memcpy(&CurItem, &ScrollBoxesAry[ScrollBoxIndex].ScrollBoxItem,
       sizeof(GuiLib_ItemRec));
    StructToCallIndex = ScrollBoxesAry[ScrollBoxIndex].LineStructIndex;
    if (StructToCallIndex != 0xFFFF)
    {
      DisplayLevel++;
      StructToCall = (GuiLib_StructPtr)
#ifdef GuiConst_REMOTE_STRUCT_DATA
         GetRemoteStructData(StructToCallIndex);
#else
#ifdef GuiConst_AVRGCC_COMPILER
         pgm_read_word(&GuiStruct_StructPtrList[StructToCallIndex]);
#else
         GuiStruct_StructPtrList[StructToCallIndex];
#endif
#endif
      NextScrollLineReading = 1;
      InitialDrawing = 1;
#ifdef GuiConst_CURSOR_SUPPORT_ON
      RemCursorInUse = CursorInUse;
      ScrollBoxesAry[ScrollBoxIndex].ContainsCursorFields = 0;
      CursorInUse = (GuiLib_ActiveCursorFieldNo >= 0);
      CursorFieldFound = -1;
      CursorActiveFieldFound = 0;
#endif
      DrawStructure(StructToCall, GuiLib_COL_INVERT_OFF);
#ifdef GuiConst_CURSOR_SUPPORT_ON
      InitialDrawing = 0;
      if (CursorFieldFound == -1)
        CursorInUse = 0;
      else
      {
        ScrollBoxesAry[ScrollBoxIndex].ContainsCursorFields = 1;
        if (CursorActiveFieldFound == 0)
        {
          GuiLib_ActiveCursorFieldNo = CursorFieldFound;

          DrawCursorItem(1);
        }
      }
      CursorInUse = CursorInUse | RemCursorInUse;
#endif
      NextScrollLineReading = 0;
      DisplayLevel--;
    }

    SetCurFont(CurItem.FontIndex);
    if ((ScrollBoxesAry[ScrollBoxIndex].LineSizeY == 0) &&
        (ScrollBoxesAry[ScrollBoxIndex].LineSizeY2 == 0))
    {
      ScrollBoxesAry[ScrollBoxIndex].LineSizeY = CurFont->BaseLine;
      ScrollBoxesAry[ScrollBoxIndex].LineSizeY2 =
         CurFont->YSize - CurFont->BaseLine - 1;
    }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
    GuiLib_SetClipping(CurItem.ClipRectX1,CurItem.ClipRectY1,
                       CurItem.ClipRectX2,CurItem.ClipRectY2);
#endif

    StructToCallIndex = ScrollBoxesAry[ScrollBoxIndex].MakeUpStructIndex;
    if (StructToCallIndex != 0xFFFF)
    {
      DisplayLevel++;
      memcpy(&CurItem, &ScrollBoxesAry[ScrollBoxIndex].ScrollBoxItem,
         sizeof(GuiLib_ItemRec));
      CurItem.RX = ScrollBoxesAry[GlobalScrollBoxIndex].X1;
      CurItem.RY = ScrollBoxesAry[GlobalScrollBoxIndex].Y1;
      StructToCall = (GuiLib_StructPtr)
#ifdef GuiConst_REMOTE_STRUCT_DATA
         GetRemoteStructData(StructToCallIndex);
#else
#ifdef GuiConst_AVRGCC_COMPILER
         pgm_read_word(&GuiStruct_StructPtrList[StructToCallIndex]);
#else
         GuiStruct_StructPtrList[StructToCallIndex];
#endif
#endif
      DrawStructure(StructToCall,GuiLib_COL_INVERT_OFF);
      DisplayLevel--;
    }

    ScrollBoxesAry[ScrollBoxIndex].ScrollLineDataFunc = DataFuncPtr;
    ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] = ActiveLine;
    ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine = ActiveLine;
    ScrollBoxesAry[ScrollBoxIndex].NumberOfLines = NoOfLines;
    if ((ScrollBoxesAry[ScrollBoxIndex].ScrollMode == 0) &&
        (NoOfLines < ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines))
      ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines = NoOfLines;
    ScrollBoxesAry[ScrollBoxIndex].InUse = GuiLib_SCROLL_STRUCTURE_USED;
    ScrollBox_SetTopLine(ScrollBoxIndex);
    GuiLib_ScrollBox_Redraw(ScrollBoxIndex);
    ScrollBoxesAry[ScrollBoxIndex].LastScrollTopLine = 0xFFFF;

    memcpy(&CurItem, &RemCurItem, sizeof(GuiLib_ItemRec));

    return (1);
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_Redraw(
   GuiConst_INT8U ScrollBoxIndex)
{
  GuiConst_INT16S N, ScrollStartLine, ScrollStopLine;
// 2011.09.26 ajv - Added #ifndef GuiConst_SCROLLITEM_BAR_NONE to resolve
// compilation warnings when -Wunused-variable flag is set.
#ifndef GuiConst_SCROLLITEM_BAR_NONE
  GuiConst_INT16U StructToCallIndex;
  GuiLib_StructPtr StructToCall;
#endif
  GuiLib_ItemRec   RemCurItem;
#ifndef GuiConst_SCROLLITEM_BAR_NONE
  GuiConst_INT16S BarMarkerY;
  GuiConst_INT16S BarMarkerHeight;
  GuiConst_INT16S BarMarkerMovementDY;
  GuiConst_INT32S N1, N2;
  GuiConst_INT16S SX1, SY1;
  GuiConst_INT16S X1, Y1;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiConst_INT16S SX2, SY2;
  GuiConst_INT16S X2, Y2;
  GuiConst_INT16S RemClippingX1, RemClippingY1, RemClippingX2, RemClippingY2;
#endif
  GuiConst_INT32S BackColor;
#endif

  if (ScrollBoxesAry[ScrollBoxIndex].InUse != GuiLib_SCROLL_STRUCTURE_USED)
    return (0);

  memcpy(&RemCurItem, &CurItem, sizeof(GuiLib_ItemRec));

  GlobalScrollBoxIndex = ScrollBoxIndex;

// 2012.02.13 ajv - Added #ifndef GuiConst_SCROLLITEM_BAR_NONE to resolve
// compilation warning when -Wunused-variable flag is set.
#ifndef GuiConst_SCROLLITEM_BAR_NONE
  BarMarkerHeight = 0;
#endif

  ScrollStartLine = ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine;
  ScrollStopLine = ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine +
     ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines - 1;
  if (ScrollBoxesAry[ScrollBoxIndex].LastScrollTopLine ==
      ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine)
  {
    if ((ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine == -1) ||
        (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] == -1))
    {
      if (ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine !=
          ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0])
      {
        if (ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine == -1)
        {
          ScrollStartLine = ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0];
          ScrollStopLine = ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0];
        }
        else
        {
          ScrollStartLine = ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine;
          ScrollStopLine = ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine;
        }
      }
    }
    else if (ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine <
             ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0])
    {
      ScrollStartLine = ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine;
      if (ScrollStartLine < ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine)
        ScrollStartLine = ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine;
      ScrollStopLine = ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0];
    }
    else if (ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine >
             ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0])
    {
      ScrollStartLine = ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0];
      ScrollStopLine = ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine;
      GuiLib_LIMIT_MAX(ScrollStartLine,
         ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine +
         ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines - 1);
    }
  }

  if (ScrollBoxesAry[ScrollBoxIndex].ScrollLineDataFunc != 0)
    for (N = ScrollStartLine; N <= ScrollStopLine; N++)
      ScrollBox_DrawScrollLine(ScrollBoxIndex, N);

  ScrollBoxesAry[ScrollBoxIndex].LastScrollTopLine =
     ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine;
  ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine =
     ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0];

#ifndef GuiConst_SCROLLITEM_BAR_NONE
  if (ScrollBoxesAry[ScrollBoxIndex].BarType != GuiLib_MARKER_NONE)
  {
    SX1 = ScrollBoxesAry[ScrollBoxIndex].BarPositionX;
    SY1 = ScrollBoxesAry[ScrollBoxIndex].BarPositionY;
    X1 = SX1 + ScrollBoxesAry[ScrollBoxIndex].BarMarkerLeftOffset;
    Y1 = SY1 + ScrollBoxesAry[ScrollBoxIndex].BarMarkerTopOffset;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    SX2 = SX1 + ScrollBoxesAry[ScrollBoxIndex].BarSizeX - 1;
    SY2 = SY1 + ScrollBoxesAry[ScrollBoxIndex].BarSizeY - 1;
    X2 = SX2 - ScrollBoxesAry[ScrollBoxIndex].BarMarkerRightOffset;
    Y2 = SY2 - ScrollBoxesAry[ScrollBoxIndex].BarMarkerBottomOffset;
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
    RemClippingX1 = CurItem.ClipRectX1;
    RemClippingY1 = CurItem.ClipRectY1;
    RemClippingX2 = CurItem.ClipRectX2;
    RemClippingY2 = CurItem.ClipRectY2;
    GuiLib_LIMIT_MINMAX(CurItem.ClipRectX1, SX1, SX2);
    GuiLib_LIMIT_MINMAX(CurItem.ClipRectY1, SY1, SY2);
    GuiLib_LIMIT_MINMAX(CurItem.ClipRectX2, SX1, SX2);
    GuiLib_LIMIT_MINMAX(CurItem.ClipRectY2, SY1, SY2);
    GuiLib_SetClipping(CurItem.ClipRectX1, CurItem.ClipRectY1,
                       CurItem.ClipRectX2, CurItem.ClipRectY2);
#endif

    StructToCallIndex = ScrollBoxesAry[ScrollBoxIndex].BarStructIndex;
    if (StructToCallIndex != 0xFFFF)
    {
      ScrollBox_ShowBarBlock(
         ScrollBoxesAry[ScrollBoxIndex].BarForeColor,
         ScrollBoxesAry[ScrollBoxIndex].BarBackColor,
         0,
         SX1,
         SY1,
         ScrollBoxesAry[ScrollBoxIndex].BarSizeX,
         ScrollBoxesAry[ScrollBoxIndex].BarSizeY);

      DisplayLevel++;
      memcpy(&CurItem,
             &ScrollBoxesAry[ScrollBoxIndex].ScrollBoxItem,
             sizeof(GuiLib_ItemRec));
      CurItem.RX = SX1;
      CurItem.RY = SY1;
      StructToCall = (GuiLib_StructPtr)
#ifdef GuiConst_REMOTE_STRUCT_DATA
         GetRemoteStructData(StructToCallIndex);
#else
#ifdef GuiConst_AVRGCC_COMPILER
         pgm_read_word(&GuiStruct_StructPtrList[StructToCallIndex]);
#else
         GuiStruct_StructPtrList[StructToCallIndex];
#endif
#endif
      DrawStructure(StructToCall, GuiLib_COL_INVERT_OFF);
      DisplayLevel--;
    }
    else
      ScrollBox_ShowBarBlock(
         ScrollBoxesAry[ScrollBoxIndex].BarForeColor,
         ScrollBoxesAry[ScrollBoxIndex].BarBackColor,
         ScrollBoxesAry[ScrollBoxIndex].BarThickness,
         SX1,
         SY1,
         ScrollBoxesAry[ScrollBoxIndex].BarSizeX,
         ScrollBoxesAry[ScrollBoxIndex].BarSizeY);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
    CurItem.ClipRectX1 = RemClippingX1;
    CurItem.ClipRectY1 = RemClippingY1;
    CurItem.ClipRectX2 = RemClippingX2;
    CurItem.ClipRectY2 = RemClippingY2;
    GuiLib_SetClipping(CurItem.ClipRectX1, CurItem.ClipRectY1,
                       CurItem.ClipRectX2, CurItem.ClipRectY2);
#endif

    if (ScrollBoxesAry[ScrollBoxIndex].NumberOfLines >
        ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines)
    {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
      RemClippingX1 = CurItem.ClipRectX1;
      RemClippingY1 = CurItem.ClipRectY1;
      RemClippingX2 = CurItem.ClipRectX2;
      RemClippingY2 = CurItem.ClipRectY2;
      GuiLib_LIMIT_MINMAX(CurItem.ClipRectX1, X1, X2);
      GuiLib_LIMIT_MINMAX(CurItem.ClipRectY1, Y1, Y2);
      GuiLib_LIMIT_MINMAX(CurItem.ClipRectX2, X1, X2);
      GuiLib_LIMIT_MINMAX(CurItem.ClipRectY2, Y1, Y2);
      GuiLib_SetClipping(CurItem.ClipRectX1, CurItem.ClipRectY1,
                         CurItem.ClipRectX2, CurItem.ClipRectY2);
#endif

      BarMarkerMovementDY = ScrollBoxesAry[ScrollBoxIndex].BarSizeY -
         ScrollBoxesAry[ScrollBoxIndex].BarMarkerBottomOffset -
         ScrollBoxesAry[ScrollBoxIndex].BarMarkerTopOffset;

      switch (ScrollBoxesAry[ScrollBoxIndex].BarType)
      {
        case GuiLib_MARKER_ICON:
#ifdef GuiConst_AVRGCC_COMPILER
          CurFont = (GuiLib_FontRecPtr)pgm_read_word(&GuiFont_FontList[
             ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconFont]);
#else
          CurFont =
             GuiFont_FontList[ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconFont];
#endif
          BarMarkerHeight = CurFont->YSize;
          break;

        case GuiLib_MARKER_BITMAP:
          BarMarkerHeight =
             ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapHeight;
          break;

        case GuiLib_MARKER_FIXED_BLOCK:
          BarMarkerHeight =
             ScrollBoxesAry[ScrollBoxIndex].BarSizeX -
             ScrollBoxesAry[ScrollBoxIndex].BarMarkerLeftOffset -
             ScrollBoxesAry[ScrollBoxIndex].BarMarkerRightOffset;
          break;

        case GuiLib_MARKER_VARIABLE_BLOCK:
          BarMarkerHeight =
             (((10 * BarMarkerMovementDY *
             ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines) + 5) /
             ScrollBoxesAry[ScrollBoxIndex].NumberOfLines) / 10;
          GuiLib_LIMIT_MIN(BarMarkerHeight, 4);
          break;
      }

      N1 = ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
         ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines;
      N2 = ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine;
      GuiLib_LIMIT_MINMAX(N2, 0, N1);
      BarMarkerY = (((10 * (BarMarkerMovementDY - BarMarkerHeight) *
         N2) + 5) / N1) / 10;

      switch (ScrollBoxesAry[ScrollBoxIndex].BarType)
      {
        case GuiLib_MARKER_ICON:
          CurItem.X1 = X1 + ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconOffsetX;
          CurItem.Y1 = Y1 + BarMarkerY +
             ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconOffsetY +
             CurFont->BaseLine;
          CurItem.FontIndex = ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconFont;
          CurItem.Alignment = GuiLib_ALIGN_LEFT;
          CurItem.Ps = GuiLib_PS_OFF;
          CurItem.BitFlags = 0;
          CurItem.BackBoxSizeX = 0;
          CurItem.BackBorderPixels = 0;
          DrawText(ScrollBoxesAry[ScrollBoxIndex].BarIconPtr, 1,
             ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor,
             ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor,
             ScrollBoxesAry[ScrollBoxIndex].BarMarkerTransparent);
          break;

        case GuiLib_MARKER_BITMAP:
          if (ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIsTransparent)
            BackColor =
               ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapTranspColor;
          else
            BackColor = -1;
          GuiLib_ShowBitmap(ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIndex,
             X1, Y1 + BarMarkerY, BackColor);
          break;

        case GuiLib_MARKER_FIXED_BLOCK:
        case GuiLib_MARKER_VARIABLE_BLOCK:
          if (ScrollBoxesAry[ScrollBoxIndex].BarMarkerTransparent)
            GuiLib_Box(X1, Y1 + BarMarkerY,
               X1 + ScrollBoxesAry[ScrollBoxIndex].BarSizeX -
               ScrollBoxesAry[ScrollBoxIndex].BarMarkerLeftOffset -
               ScrollBoxesAry[ScrollBoxIndex].BarMarkerRightOffset - 1,
               Y1 + BarMarkerY + BarMarkerHeight - 1,
               ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor);
          else
            GuiLib_BorderBox(X1, Y1 + BarMarkerY,
               X1 + ScrollBoxesAry[ScrollBoxIndex].BarSizeX -
               ScrollBoxesAry[ScrollBoxIndex].BarMarkerLeftOffset -
               ScrollBoxesAry[ScrollBoxIndex].BarMarkerRightOffset - 1,
               Y1 + BarMarkerY + BarMarkerHeight - 1,
               ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor,
               ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor);
          break;
      }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
      CurItem.ClipRectX1 = RemClippingX1;
      CurItem.ClipRectY1 = RemClippingY1;
      CurItem.ClipRectX2 = RemClippingX2;
      CurItem.ClipRectY2 = RemClippingY2;
      GuiLib_SetClipping(CurItem.ClipRectX1, CurItem.ClipRectY1,
                         CurItem.ClipRectX2, CurItem.ClipRectY2);
#endif
    }
  }
#endif

#ifndef GuiConst_SCROLLITEM_INDICATOR_NONE
  if (ScrollBoxesAry[ScrollBoxIndex].IndicatorType != GuiLib_INDICATOR_NONE)
  {
    SX1 = ScrollBoxesAry[ScrollBoxIndex].IndicatorPositionX;
    SY1 = ScrollBoxesAry[ScrollBoxIndex].IndicatorPositionY;
    X1 = SX1 + ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerLeftOffset;
    Y1 = SY1 + ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerTopOffset;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    SX2 = SX1 + ScrollBoxesAry[ScrollBoxIndex].IndicatorSizeX - 1;
    SY2 = SY1 + ScrollBoxesAry[ScrollBoxIndex].IndicatorSizeY - 1;
    X2 = SX2 - ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerRightOffset;
    Y2 = SY2 - ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBottomOffset;
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
    RemClippingX1 = CurItem.ClipRectX1;
    RemClippingY1 = CurItem.ClipRectY1;
    RemClippingX2 = CurItem.ClipRectX2;
    RemClippingY2 = CurItem.ClipRectY2;
    GuiLib_LIMIT_MINMAX(CurItem.ClipRectX1, SX1, SX2);
    GuiLib_LIMIT_MINMAX(CurItem.ClipRectY1, SY1, SY2);
    GuiLib_LIMIT_MINMAX(CurItem.ClipRectX2, SX1, SX2);
    GuiLib_LIMIT_MINMAX(CurItem.ClipRectY2, SY1, SY2);
    GuiLib_SetClipping(CurItem.ClipRectX1, CurItem.ClipRectY1,
                       CurItem.ClipRectX2, CurItem.ClipRectY2);
#endif

    StructToCallIndex = ScrollBoxesAry[ScrollBoxIndex].IndicatorStructIndex;
    if (StructToCallIndex != 0xFFFF)
    {
      ScrollBox_ShowBarBlock(
         ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor,
         ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor,
         0,
         SX1,
         SY1,
         ScrollBoxesAry[ScrollBoxIndex].IndicatorSizeX,
         ScrollBoxesAry[ScrollBoxIndex].IndicatorSizeY);

      DisplayLevel++;
      memcpy(&CurItem,
             &ScrollBoxesAry[ScrollBoxIndex].ScrollBoxItem,
             sizeof(GuiLib_ItemRec));
      CurItem.RX = SX1;
      CurItem.RY = SY1;
      StructToCall = (GuiLib_StructPtr)
#ifdef GuiConst_REMOTE_STRUCT_DATA
         GetRemoteStructData(StructToCallIndex);
#else
#ifdef GuiConst_AVRGCC_COMPILER
         pgm_read_word(&GuiStruct_StructPtrList[StructToCallIndex]);
#else
         GuiStruct_StructPtrList[StructToCallIndex];
#endif
#endif
      DrawStructure(StructToCall, GuiLib_COL_INVERT_OFF);
      DisplayLevel--;
    }
    else
      ScrollBox_ShowBarBlock(
         ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor,
         ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor,
         ScrollBoxesAry[ScrollBoxIndex].IndicatorThickness,
         SX1,
         SY1,
         ScrollBoxesAry[ScrollBoxIndex].IndicatorSizeX,
         ScrollBoxesAry[ScrollBoxIndex].IndicatorSizeY);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
    CurItem.ClipRectX1 = RemClippingX1;
    CurItem.ClipRectY1 = RemClippingY1;
    CurItem.ClipRectX2 = RemClippingX2;
    CurItem.ClipRectY2 = RemClippingY2;
    GuiLib_SetClipping(CurItem.ClipRectX1, CurItem.ClipRectY1,
                       CurItem.ClipRectX2, CurItem.ClipRectY2);
#endif

    if ((ScrollBoxesAry[ScrollBoxIndex].IndicatorLine >= 0) &&
        (ScrollBoxesAry[ScrollBoxIndex].IndicatorLine >=
         ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine) &&
        (ScrollBoxesAry[ScrollBoxIndex].IndicatorLine <
        (ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine +
         ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines)))
    {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
      GuiLib_LIMIT_MINMAX(CurItem.ClipRectX1, X1, X2);
      GuiLib_LIMIT_MINMAX(CurItem.ClipRectY1, Y1, Y2);
      GuiLib_LIMIT_MINMAX(CurItem.ClipRectX2, X1, X2);
      GuiLib_LIMIT_MINMAX(CurItem.ClipRectY2, Y1, Y2);
      GuiLib_SetClipping(CurItem.ClipRectX1, CurItem.ClipRectY1,
                         CurItem.ClipRectX2, CurItem.ClipRectY2);
#endif

      switch (ScrollBoxesAry[ScrollBoxIndex].IndicatorType)
      {
        case GuiLib_MARKER_ICON:
#ifdef GuiConst_AVRGCC_COMPILER
          CurFont = (GuiLib_FontRecPtr)pgm_read_word(&GuiFont_FontList[
             ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconFont]);
#else
          CurFont = GuiFont_FontList[
             ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconFont];
#endif
          CurItem.X1 =
             X1 + ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconOffsetX;
          CurItem.Y1 =
             Y1 + ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconOffsetY +
             CurFont->BaseLine +
             ScrollBoxesAry[ScrollBoxIndex].LineVerticalOffset *
             (ScrollBoxesAry[ScrollBoxIndex].IndicatorLine -
             ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine);
          CurItem.FontIndex =
             ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconFont;
          CurItem.Alignment = GuiLib_ALIGN_LEFT;
          CurItem.Ps = GuiLib_PS_OFF;
          CurItem.BitFlags = 0;
          CurItem.BackBoxSizeX = 0;
          CurItem.BackBorderPixels = 0;
          DrawText(ScrollBoxesAry[ScrollBoxIndex].IndicatorIconPtr, 1,
                   ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor,
                   ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor,
                   ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerTransparent);
          break;

        case GuiLib_MARKER_BITMAP:
          if (ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapIsTransparent)
            BackColor =
               ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapTranspColor;
          else
            BackColor = -1;
          GuiLib_ShowBitmap(
             ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapIndex,
             X1, Y1 + ScrollBoxesAry[ScrollBoxIndex].LineVerticalOffset *
             (ScrollBoxesAry[ScrollBoxIndex].IndicatorLine -
             ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine), BackColor);
          break;
      }
    }
  }
#endif

  memcpy(&CurItem, &RemCurItem, sizeof(GuiLib_ItemRec));

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_RedrawLine(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16U ScrollLine)
{
  GuiLib_ItemRec RemCurItem;

  if (ScrollBoxesAry[ScrollBoxIndex].InUse != GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if (ScrollBoxesAry[ScrollBoxIndex].ScrollLineDataFunc == 0)
    return (0);

  memcpy(&RemCurItem, &CurItem, sizeof(GuiLib_ItemRec));
  GlobalScrollBoxIndex = ScrollBoxIndex;

  ScrollBox_DrawScrollLine(ScrollBoxIndex, ScrollLine);

  memcpy(&CurItem, &RemCurItem, sizeof(GuiLib_ItemRec));

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_Close(
   GuiConst_INT8U ScrollBoxIndex)
{
  if (ScrollBoxesAry[ScrollBoxIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
  {
    ScrollBoxesAry[ScrollBoxIndex].InUse = GuiLib_SCROLL_STRUCTURE_READ;
    return (1);
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_Down(
   GuiConst_INT8U ScrollBoxIndex)
{
  GuiConst_INT16U ScrollBottomLine;
  GuiConst_INT16S RemScrollTopLine;
  GuiConst_INT16S RemMarkerStartLine;

  RemScrollTopLine = ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine;
  RemMarkerStartLine = ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0];

  if (ScrollBoxesAry[ScrollBoxIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
  {
    ScrollBottomLine = (ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine +
       ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines - 1);

    if (ScrollBoxesAry[ScrollBoxIndex].ScrollBoxType)
    {
      if (ScrollBottomLine >= (ScrollBoxesAry[ScrollBoxIndex].NumberOfLines - 1))
      {
        if ((ScrollBoxesAry[ScrollBoxIndex].WrapMode == 1)
#ifdef GuiConst_SCROLL_MODE_WRAP_AROUND
                                                           ||
            (ScrollBoxesAry[ScrollBoxIndex].WrapMode == 2)
#endif
                                                          )
          ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine = 0;
      }
      else
        ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine++;
    }
    else if (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] >= 0)
    {
      if (ScrollBoxesAry[ScrollBoxIndex].ScrollMode == 0)
      {
        if (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] ==
           (ScrollBoxesAry[ScrollBoxIndex].NumberOfLines - 1))
        {
          if ((ScrollBoxesAry[ScrollBoxIndex].WrapMode == 1)
#ifdef GuiConst_SCROLL_MODE_WRAP_AROUND
                                                             ||
              (ScrollBoxesAry[ScrollBoxIndex].WrapMode == 2)
#endif
                                                            )
          {
            ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine = 0;
            ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] = 0;
          }
        }
        else if (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] <
           ScrollBottomLine - ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs)
          ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0]++;
        else
        {
          ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine++;
          GuiLib_LIMIT_MAX(ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine,
             ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
             ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines);
          ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0]++;
        }
      }
      else
      {
        if (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] ==
           (ScrollBoxesAry[ScrollBoxIndex].NumberOfLines - 1))
        {
          if ((ScrollBoxesAry[ScrollBoxIndex].WrapMode == 1)
#ifdef GuiConst_SCROLL_MODE_WRAP_AROUND
                                                             ||
              (ScrollBoxesAry[ScrollBoxIndex].WrapMode == 2)
#endif
                                                            )
          {
            ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
               -ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs;
            ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] = 0;
          }
        }
        else
        {
          ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine++;
          ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0]++;
        }
      }
    }
  }

  if ((ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine != RemScrollTopLine) ||
      (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] != RemMarkerStartLine))
  {
    GuiLib_ScrollBox_Redraw(ScrollBoxIndex);
    return (1);
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_Up(
   GuiConst_INT8U ScrollBoxIndex)
{
  GuiConst_INT16S RemScrollTopLine;
  GuiConst_INT16S RemMarkerStartLine;

  RemScrollTopLine = ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine;
  RemMarkerStartLine = ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0];

  if (ScrollBoxesAry[ScrollBoxIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
  {
    if (ScrollBoxesAry[ScrollBoxIndex].ScrollBoxType)
    {
      if (ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine == 0)
      {
        if ((ScrollBoxesAry[ScrollBoxIndex].WrapMode == 1)
#ifdef GuiConst_SCROLL_MODE_WRAP_AROUND
                                                           ||
            (ScrollBoxesAry[ScrollBoxIndex].WrapMode == 2)
#endif
                                                          )
          ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
             ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
             ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines;
          GuiLib_LIMIT_MIN(ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine, 0);
      }
      else
        ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine--;
    }
    else if (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] >= 0)
    {
      if (ScrollBoxesAry[ScrollBoxIndex].ScrollMode == 0)
      {
        if (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] == 0)
        {
          if ((ScrollBoxesAry[ScrollBoxIndex].WrapMode == 1)
#ifdef GuiConst_SCROLL_MODE_WRAP_AROUND
                                                             ||
              (ScrollBoxesAry[ScrollBoxIndex].WrapMode == 2)
#endif
                                                            )
          {
            ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
               ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
               ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines;
            GuiLib_LIMIT_MIN(ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine, 0);
            ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] =
               ScrollBoxesAry[ScrollBoxIndex].NumberOfLines - 1;
          }
        }
        else if (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] >
                 ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine +
                 ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs)
          ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0]--;
        else
        {
          ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine--;
          GuiLib_LIMIT_MIN(ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine, 0);
          ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0]--;
        }
      }
      else
      {
        if (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] == 0)
        {
          if ((ScrollBoxesAry[ScrollBoxIndex].WrapMode == 1)
#ifdef GuiConst_SCROLL_MODE_WRAP_AROUND
                                                             ||
              (ScrollBoxesAry[ScrollBoxIndex].WrapMode == 2)
#endif
                                                            )
          {
            ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
               ScrollBoxesAry[ScrollBoxIndex].NumberOfLines - 1 -
               ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs;
            ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] =
               ScrollBoxesAry[ScrollBoxIndex].NumberOfLines - 1;
          }
        }
        else
        {
          ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine--;
          ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0]--;
        }
      }
    }
  }

  if ((ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine != RemScrollTopLine) ||
      (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] != RemMarkerStartLine))
  {
    GuiLib_ScrollBox_Redraw(ScrollBoxIndex);
    return (1);
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_Home(
   GuiConst_INT8U ScrollBoxIndex)
{
  GuiConst_INT16S RemScrollTopLine;
  GuiConst_INT16S RemMarkerStartLine;

  RemScrollTopLine = ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine;
  RemMarkerStartLine = ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0];

  if (ScrollBoxesAry[ScrollBoxIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
  {
    if (ScrollBoxesAry[ScrollBoxIndex].ScrollBoxType)
      ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine = 0;
    else
    {
      if (ScrollBoxesAry[ScrollBoxIndex].ScrollMode == 0)
        ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine = 0;
      else
        ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
           -ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs;
      if (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] >= 0)
        ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] = 0;
    }
  }

  if ((ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine != RemScrollTopLine) ||
      (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] != RemMarkerStartLine))
  {
    GuiLib_ScrollBox_Redraw(ScrollBoxIndex);
    return (1);
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_End(
   GuiConst_INT8U ScrollBoxIndex)
{
  GuiConst_INT16S RemScrollTopLine;
  GuiConst_INT16S RemMarkerStartLine;

  RemScrollTopLine = ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine;
  RemMarkerStartLine = ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0];

  if (ScrollBoxesAry[ScrollBoxIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
  {
    if (ScrollBoxesAry[ScrollBoxIndex].ScrollBoxType)
      ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine = GuiLib_GET_MAX(
         ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
         ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines, 0);
    else
    {
      if (ScrollBoxesAry[ScrollBoxIndex].ScrollMode == 0)
        ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine = GuiLib_GET_MAX(
           ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
           ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines, 0);
      else
        ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
           ScrollBoxesAry[ScrollBoxIndex].NumberOfLines - 1 -
           ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs;
      if (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] >= 0)
        ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] =
           ScrollBoxesAry[ScrollBoxIndex].NumberOfLines - 1;
    }
  }

  if ((ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine != RemScrollTopLine) ||
      (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] != RemMarkerStartLine))
  {
    GuiLib_ScrollBox_Redraw(ScrollBoxIndex);
    return (1);
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_To_Line(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16U NewLine)
{
  GuiConst_INT16S RemScrollTopLine;
  GuiConst_INT16S RemMarkerStartLine;
  GuiConst_INT16S TopLine;

  GuiLib_LIMIT_MAX(NewLine, ScrollBoxesAry[ScrollBoxIndex].NumberOfLines - 1);

  RemScrollTopLine = ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine;
  RemMarkerStartLine = ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0];

  if (ScrollBoxesAry[ScrollBoxIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
  {
    if (ScrollBoxesAry[ScrollBoxIndex].ScrollBoxType)
    {
      TopLine = ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
                ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines;
      GuiLib_LIMIT_MIN(TopLine, 0);
      if (NewLine > TopLine)
        ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine = TopLine;
      else
        ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine = NewLine;
    }
    else if (ScrollBoxesAry[ScrollBoxIndex].ScrollMode == 0)
    {
      if ((NewLine < ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine) ||
          (NewLine >= (ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine +
           ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines)))
      {
        if (NewLine > (ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
            ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines +
            ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs))
          ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
             ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
             ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines;
        else
          ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
             NewLine - ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs;

        GuiLib_LIMIT_MINMAX(ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine,
           NewLine - ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines + 1,
           NewLine + ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines - 1);
        GuiLib_LIMIT_MINMAX(ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine, 0,
           ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
           ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines);
      }
    }
    else
      ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
         NewLine - ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs;
    ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] = NewLine;
  }

  if ((ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine != RemScrollTopLine) ||
      (ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] != RemMarkerStartLine))
  {
    GuiLib_ScrollBox_Redraw(ScrollBoxIndex);
    return (1);
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_SetLineMarker(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16U ScrollLineMarkerIndex,
   GuiConst_INT16S StartLine,
   GuiConst_INT16U Size)
{
  if ((ScrollBoxIndex < GuiConst_SCROLLITEM_BOXES_MAX) &&
     (ScrollBoxesAry[ScrollBoxIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED) &&
     (ScrollLineMarkerIndex < GuiConst_SCROLLITEM_MARKERS_MAX))
  {
    ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[ScrollLineMarkerIndex] =
       GuiLib_GET_MINMAX(StartLine, -1,
       (GuiConst_INT16S)ScrollBoxesAry[ScrollBoxIndex].NumberOfLines - 1);
    if ((ScrollLineMarkerIndex == 0) && (Size > 1))
      Size = 1;
    ScrollBoxesAry[ScrollBoxIndex].MarkerSize[ScrollLineMarkerIndex] = Size;
    if (ScrollLineMarkerIndex == 0)
      ScrollBox_SetTopLine(ScrollBoxIndex);
    return (1);
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT16S GuiLib_ScrollBox_GetActiveLine(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16U ScrollLineMarkerIndex)
{
  if ((ScrollBoxesAry[ScrollBoxIndex].InUse ==
       GuiLib_SCROLL_STRUCTURE_USED) &&
      (ScrollLineMarkerIndex < GuiConst_SCROLLITEM_MARKERS_MAX))
    return (ScrollBoxesAry[ScrollBoxIndex].
       MarkerStartLine[ScrollLineMarkerIndex]);
  else
    return (-1);
}

//------------------------------------------------------------------------------
GuiConst_INT16S GuiLib_ScrollBox_GetActiveLineCount(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16U ScrollLineMarkerIndex)
{
  if ((ScrollBoxesAry[ScrollBoxIndex].InUse ==
       GuiLib_SCROLL_STRUCTURE_USED) &&
      (ScrollLineMarkerIndex < GuiConst_SCROLLITEM_MARKERS_MAX))
    return (ScrollBoxesAry[ScrollBoxIndex].MarkerSize[ScrollLineMarkerIndex]);
  else
    return (-1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_SetIndicator(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16S StartLine)
{
#ifdef GuiConst_SCROLLITEM_INDICATOR_NONE
  return (0);
#else
  if (ScrollBoxIndex >= GuiConst_SCROLLITEM_BOXES_MAX)
    return (0);

  ScrollBoxesAry[ScrollBoxIndex].IndicatorLine = GuiLib_GET_MINMAX(
     StartLine, -1,
     (GuiConst_INT16S)ScrollBoxesAry[ScrollBoxIndex].NumberOfLines - 1);

  return (1);
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_SetTopLine(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16S TopLine)
{
  if ((ScrollBoxIndex < GuiConst_SCROLLITEM_BOXES_MAX) &&
     (ScrollBoxesAry[ScrollBoxIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED))
  {
    if (ScrollBoxesAry[ScrollBoxIndex].ScrollMode == 0)
      ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
         GuiLib_GET_MINMAX(TopLine, 0,
         ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
         ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines);

    return (1);
  }
  else
    return (0);
}

//------------------------------------------------------------------------------
GuiConst_INT16S GuiLib_ScrollBox_GetTopLine(
   GuiConst_INT8U ScrollBoxIndex)
{
  if (ScrollBoxesAry[ScrollBoxIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine);
  else
    return (-1);
}
#endif

#ifdef GuiConst_ITEM_GRAPH_INUSE
#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
static void Graph_SetClipping(
   GuiConst_INT8U GraphIndex)
{
  GuiLib_SetClipping(GraphAry[GraphIndex].GraphItem.X1,
                     GraphAry[GraphIndex].GraphItem.Y1,
                     GraphAry[GraphIndex].GraphItem.X2,
                     GraphAry[GraphIndex].GraphItem.Y2);
}

//------------------------------------------------------------------------------
static void Graph_ResetClipping(void)
{
  GuiLib_ResetClipping();
}
#endif

//------------------------------------------------------------------------------
static void Graph_CalcScaleX(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex)
{
  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].Scale =
     (10000 * (GraphAry[GraphIndex].GraphItem.X2 -
               GraphAry[GraphIndex].GraphItem.X1 -
               GraphAry[GraphIndex].OriginOffsetX)) /
     (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
      NumbersMaxValue -
      GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
      NumbersMinValue);
}

//------------------------------------------------------------------------------
static void Graph_CalcScaleY(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex)
{
  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].Scale =
     (10000 * (GraphAry[GraphIndex].GraphItem.Y2 -
               GraphAry[GraphIndex].GraphItem.Y1 -
               GraphAry[GraphIndex].OriginOffsetY)) /
     (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
      NumbersMaxValue -
      GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
      NumbersMinValue);
}

//------------------------------------------------------------------------------
static GuiConst_INT32S Graph_CalcNumbersMaxValue(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex,
   GuiConst_INT8U AxisType)
{
  GuiConst_INT32S MV;

  MV = GraphAry[GraphIndex].GraphAxes[AxisIndex][AxisType].NumbersMaxValue;
  if (GraphAry[GraphIndex].GraphAxes[AxisIndex][AxisType].TicksMinor)
    MV -= GraphAry[GraphIndex].GraphAxes[AxisIndex][AxisType].NumbersAtEnd *
          GraphAry[GraphIndex].GraphAxes[AxisIndex][AxisType].NumbersStepMinor;
  else if (GraphAry[GraphIndex].GraphAxes[AxisIndex][AxisType].TicksMajor)
    MV -= GraphAry[GraphIndex].GraphAxes[AxisIndex][AxisType].NumbersAtEnd *
          GraphAry[GraphIndex].GraphAxes[AxisIndex][AxisType].NumbersStepMajor;
  return(MV);
}

//------------------------------------------------------------------------------
static void Graph_DrawXAxis(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex)
{
  GuiConst_INT16S X, Y;
  GuiConst_INT16S DX, DY;
  GuiConst_INT32S TX, TY;
  GuiConst_INT16S HW;
  GuiConst_INT32S MV;
  GuiConst_INT16U StrLen;

  Y = GraphAry[GraphIndex].OrigoY +
      GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].Offset;

  MV = Graph_CalcNumbersMaxValue(GraphIndex, AxisIndex, GuiLib_GRAPHAXIS_X);

  if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].Line)
  {
    X = GraphAry[GraphIndex].GraphItem.X1;
    if (!GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
        LineNegative)
      X += GraphAry[GraphIndex].OriginOffsetX;

    GuiLib_FillBox(X,
                   Y,
                   GraphAry[GraphIndex].GraphItem.X2,
                   Y,
                   GraphAry[GraphIndex].ForeColor);
  }

  if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].Arrow)
  {
    DX = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
         ArrowLength;
    HW = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
         ArrowWidth / 2;
    for (DY = -HW; DY <= HW; DY++)
      GuiLib_Line(GraphAry[GraphIndex].GraphItem.X2 - DX,
                  Y + DY,
                  GraphAry[GraphIndex].GraphItem.X2,
                  Y,
                  GraphAry[GraphIndex].ForeColor);
  }

  Graph_CalcScaleX(GraphIndex, AxisIndex);

  if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].TicksMajor)
  {
    HW = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
         TicksMajorWidth / 2;
    TX = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
         NumbersMinValue;
    while (TX <= MV)
    {
      DX = GraphAry[GraphIndex].OrigoX + ((TX -
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
           NumbersMinValue) *
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].Scale) /
           10000;
      if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
          NumbersAtOrigo || (TX != 0))
        GuiLib_FillBox(
           DX - HW,
           Y,
           DX - HW +
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
           TicksMajorWidth - 1,
           Y + GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
           TicksMajorLength - 1,
           GraphAry[GraphIndex].ForeColor);
      TX += GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
            NumbersStepMajor;
    }
  }

  if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].TicksMinor)
  {
    HW = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
         TicksMinorWidth / 2;
    TX = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
         NumbersMinValue;
    while (TX <= MV)
    {
      DX = GraphAry[GraphIndex].OrigoX + ((TX -
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
           NumbersMinValue) *
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].Scale) /
           10000;
      if ((GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
          NumbersAtOrigo || (TX != 0)) &&
         ((!GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
            TicksMajor) ||
         (TX % GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
         NumbersStepMajor != 0)))
        GuiLib_FillBox(
           DX - HW,
           Y,
           DX - HW +
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
           TicksMinorWidth - 1,
           Y + GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
           TicksMinorLength - 1,
           GraphAry[GraphIndex].ForeColor);
      TX += GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
            NumbersStepMinor;
    }
  }

  if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].Numbers)
  {
    memcpy(&CurItem, &GraphAry[GraphIndex].GraphItem, sizeof(GuiLib_ItemRec));

    DY = Y;
    if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
       TicksMajor)
      DY += GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
         TicksMajorLength;
    SetCurFont(GraphAry[GraphIndex].GraphItem.FontIndex);
#ifdef GuiConst_AVRGCC_COMPILER
    DY += pgm_read_byte(&CurFont->BaseLine);
#else
    DY += CurFont->BaseLine;
#endif
    DY += GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
       NumbersOffset;

    TX = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
       NumbersMinValue;
    while (TX <= MV)
    {
      DX = GraphAry[GraphIndex].OrigoX + ((TX -
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
           NumbersMinValue) *
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].Scale) /
           10000;
      if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
          NumbersAtOrigo || (TX != 0))
        GuiLib_DrawVar(
           DX,
           DY,
           GraphAry[GraphIndex].GraphItem.FontIndex,
           #ifdef GuiConst_CHARMODE_ANSI
           0,
           #endif
           &TX,
           GuiLib_VAR_SIGNED_LONG,
           GuiLib_FORMAT_DEC,
           0,
           GuiLib_FORMAT_ALIGNMENT_RIGHT,
           0,   // Decimals
           0,   // ShowSign,
           0,   // ZeroPadding,
           0,   // TrailingZeros,
           0,   // ThousandsSeparator,
           GuiLib_ALIGN_CENTER,
           GraphAry[GraphIndex].GraphItem.Ps,
           1,   // Transparent
           ((GraphAry[GraphIndex].GraphItem.BitFlags &
            GuiLib_BITFLAG_UNDERLINE) > 0),
           0,
           0,
           0,
           0,
           GraphAry[GraphIndex].GraphItem.ForeColor,
           GraphAry[GraphIndex].GraphItem.BackColor);
      TX += GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
         NumbersStepMajor;
    }
  }
}

//------------------------------------------------------------------------------
static void Graph_DrawYAxis(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex)
{
  GuiConst_INT16S X, Y;
  GuiConst_INT16S DX, DY;
  GuiConst_INT32S TX, TY;
  GuiConst_INT16S HW;
  GuiConst_INT16S MidY;
  GuiConst_INT32S MV;
  GuiConst_INT16U StrLen;

  X = GraphAry[GraphIndex].OrigoX +
      GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].Offset;

  MV = Graph_CalcNumbersMaxValue(GraphIndex, AxisIndex, GuiLib_GRAPHAXIS_Y);

  if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].Line)
  {
    Y = GraphAry[GraphIndex].GraphItem.Y2;
    if (!GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
        LineNegative)
      Y -= GraphAry[GraphIndex].OriginOffsetY;

    GuiLib_FillBox(X,
                   GraphAry[GraphIndex].GraphItem.Y1,
                   X,
                   Y,
                   GraphAry[GraphIndex].ForeColor);
  }

  if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].Arrow)
  {
    DY = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
         ArrowLength;
    HW = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
         ArrowWidth / 2;
    for (DX = -HW; DX <= HW; DX++)
      GuiLib_Line(X - DX,
                  GraphAry[GraphIndex].GraphItem.Y1 + DY,
                  X,
                  GraphAry[GraphIndex].GraphItem.Y1,
                  GraphAry[GraphIndex].ForeColor);
  }

  Graph_CalcScaleY(GraphIndex, AxisIndex);

  if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].TicksMajor)
  {
    HW = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
         TicksMajorWidth / 2;
    TY = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
         NumbersMinValue;
    while (TY <= MV)
    {
      DY = GraphAry[GraphIndex].OrigoY - ((TY -
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
           NumbersMinValue) *
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].Scale) /
           10000;
      if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
          NumbersAtOrigo || (TY != 0))
        GuiLib_FillBox(
           X - GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
           TicksMajorLength + 1,
           DY - HW,
           X,
           DY - HW +
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
           TicksMajorWidth - 1,
           GraphAry[GraphIndex].ForeColor);
      TY += GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
            NumbersStepMajor;
    }
  }

  if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].TicksMinor)
  {
    HW = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
         TicksMinorWidth / 2;
    TY = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
         NumbersMinValue;
    while (TY <= MV)
    {
      DY = GraphAry[GraphIndex].OrigoY - ((TY -
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
           NumbersMinValue) *
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].Scale) /
           10000;
      if ((GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
          NumbersAtOrigo || (TY != 0)) &&
         ((!GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
         TicksMajor) ||
         (TY % GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
         NumbersStepMajor != 0)))
        GuiLib_FillBox(
           X - GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
           TicksMinorLength + 1,
           DY - HW,
           X,
           DY - HW +
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
           TicksMinorWidth - 1,
           GraphAry[GraphIndex].ForeColor);
      TY += GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
            NumbersStepMinor;
    }
  }

  if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].Numbers)
  {
    memcpy(&CurItem, &GraphAry[GraphIndex].GraphItem, sizeof(GuiLib_ItemRec));

    SetCurFont(GraphAry[GraphIndex].GraphItem.FontIndex);
#ifdef GuiConst_AVRGCC_COMPILER
    MidY = (pgm_read_byte(&CurFont->BaseLine) -
            pgm_read_byte(&CurFont->TopLine)) / 2;
#else
    MidY = (CurFont->BaseLine - CurFont->TopLine) / 2;
#endif

    DX = X;
    if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
       TicksMajor)
      DX -= GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
         TicksMajorLength;
    DX -= GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
       NumbersOffset;

    TY = GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
       NumbersMinValue;
    while (TY <= MV)
    {
      DY = GraphAry[GraphIndex].OrigoY - ((TY -
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
           NumbersMinValue) *
           GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].Scale) /
           10000;
      if (GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
          NumbersAtOrigo || (TY != 0))
        GuiLib_DrawVar(
           DX,
           DY + MidY,
           GraphAry[GraphIndex].GraphItem.FontIndex,
           #ifdef GuiConst_CHARMODE_ANSI
           0,
           #endif
           &TY,
           GuiLib_VAR_SIGNED_LONG,
           GuiLib_FORMAT_DEC,
           0,
           GuiLib_FORMAT_ALIGNMENT_RIGHT,
           0,   // Decimals
           0,   // ShowSign,
           0,   // ZeroPadding,
           0,   // TrailingZeros,
           0,   // ThousandsSeparator,
           GuiLib_ALIGN_RIGHT,
           GraphAry[GraphIndex].GraphItem.Ps,
           1,   // Transparent
           ((GraphAry[GraphIndex].GraphItem.BitFlags &
            GuiLib_BITFLAG_UNDERLINE) > 0),
           0,
           0,
           0,
           0,
           GraphAry[GraphIndex].GraphItem.ForeColor,
           GraphAry[GraphIndex].GraphItem.BackColor);
      TY += GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
         NumbersStepMajor;
    }
  }
}

//------------------------------------------------------------------------------
static void Graph_DrawDataPoint(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex,
   GuiConst_INT16U DataIndex,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S LastX,
   GuiConst_INT16S LastY)
{
  GuiConst_INT16S DX1, DY1;
  GuiConst_INT16S DX2, DY2;
  GuiConst_INT16S HW;
  GuiConst_INT16S AY;

  switch (GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Representation)
  {
    case GuiLib_GRAPH_DATATYPE_DOT:
      if (GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Width == 1)
      {
        if (GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Thickness == 0)
          GuiLib_Dot(
             X, Y,
             GraphAry[GraphIndex].GraphDataSets[DataSetIndex].BackColor);
        else
          GuiLib_Dot(
             X, Y,
             GraphAry[GraphIndex].GraphDataSets[DataSetIndex].ForeColor);
      }
      else if (GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Thickness == 0)
        GuiLib_Circle(
           X, Y,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Width / 2,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].BackColor,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].BackColor);
      else
        GuiLib_Circle(
           X, Y,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Width / 2,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].ForeColor,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].BackColor);
      break;

    case GuiLib_GRAPH_DATATYPE_LINE:
      if (DataIndex ==
          GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataFirst)
        GuiLib_Dot(
           X, Y,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].ForeColor);
      else
        GuiLib_Line(
           LastX, LastY,
           X, Y,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].ForeColor);
      break;

    case GuiLib_GRAPH_DATATYPE_BAR:
      HW = GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Width / 2;
      AY = GraphAry[GraphIndex].OrigoY + GraphAry[GraphIndex].GraphAxes
           [GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexX]
           [GuiLib_GRAPHAXIS_X].Offset;
      DX1 = X - HW;
      DY1 = AY;
      DX2 = X + HW;
      DY2 = Y;
      if (GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Thickness == 0)
      {
        if (!GraphAry[GraphIndex].GraphDataSets[DataSetIndex].
             BackColorTransparent)
          GuiLib_FillBox(
             DX1, DY1,
             DX2, DY2,
             GraphAry[GraphIndex].GraphDataSets[DataSetIndex].BackColor);
      }
      else
      {
        OrderCoord(&DX1, &DX2);
        OrderCoord(&DY1, &DY2);
        DrawBorderBox(
           DX1, DY1,
           DX2, DY2,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].ForeColor,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].BackColor,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].
              BackColorTransparent,
           GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Thickness);
      }
      break;

    case GuiLib_GRAPH_DATATYPE_CROSS:
      HW = GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Width / 2;
      DX1 = X - HW;
      DY1 = Y - HW;
      DX2 = X + HW;
      DY2 = Y + HW;
      GuiLib_Line(
         DX1, DY1,
         DX2, DY2,
         GraphAry[GraphIndex].GraphDataSets[DataSetIndex].ForeColor);
      GuiLib_Line(
         DX1, DY2,
         DX2, DY1,
         GraphAry[GraphIndex].GraphDataSets[DataSetIndex].ForeColor);
      break;

    case GuiLib_GRAPH_DATATYPE_X:
      HW = GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Width / 2;
      DX1 = X - HW;
      DY1 = Y - HW;
      DX2 = X + HW;
      DY2 = Y + HW;
      GuiLib_Line(
         DX1, Y,
         DX2, Y,
         GraphAry[GraphIndex].GraphDataSets[DataSetIndex].ForeColor);
      GuiLib_Line(
         X, DY1,
         X, DY2,
         GraphAry[GraphIndex].GraphDataSets[DataSetIndex].ForeColor);
      break;
  }
}

//------------------------------------------------------------------------------
static void Graph_DrawDataSet(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex)
{
  GuiConst_INT16S DX, DY;
  GuiConst_INT16S LastDX, LastDY;
  GuiConst_INT32S TX, TY;
  GuiConst_INT16U DataIndex;
  GuiConst_INT16U DataCount;

  Graph_CalcScaleX(GraphIndex,
                   GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexX);
  Graph_CalcScaleY(GraphIndex,
                   GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexY);

  DataIndex = GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataFirst;
  for (DataCount = 1;
       DataCount <= GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataCount;
       DataCount++)
  {
    TX =
       (*GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataPtr)[DataIndex].X;
    TY =
       (*GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataPtr)[DataIndex].Y;

    LastDX = DX;
    LastDY = DY;
    DX = GraphAry[GraphIndex].OrigoX + ((TX -
         GraphAry[GraphIndex].GraphAxes[
         GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexX]
         [GuiLib_GRAPHAXIS_X].NumbersMinValue) *
         GraphAry[GraphIndex].GraphAxes[
         GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexX]
         [GuiLib_GRAPHAXIS_X].Scale) / 10000;
    DY = GraphAry[GraphIndex].OrigoY - ((TY -
         GraphAry[GraphIndex].GraphAxes[
         GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexY]
         [GuiLib_GRAPHAXIS_Y].NumbersMinValue) *
         GraphAry[GraphIndex].GraphAxes[
         GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexY]
         [GuiLib_GRAPHAXIS_Y].Scale) / 10000;

    Graph_DrawDataPoint(
       GraphIndex,
       DataSetIndex,
       DataIndex,
       DX, DY,
       LastDX, LastDY);

    if (DataIndex >= GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataCount)
      DataIndex = 0;
    else
      DataIndex++;
  }
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_Close(
   GuiConst_INT8U GraphIndex)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);

  GraphAry[GraphIndex].InUse = GuiLib_GRAPH_STRUCTURE_UNDEF;
  GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_X] = 0;
  GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_Y] = 0;
  GraphAry[GraphIndex].GraphDataSetCnt = 0;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_AddDataSet(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex,
   void *DataPtr,
   GuiConst_INT16U DataSize,
   GuiConst_INT16U DataCount,
   GuiConst_INT16U DataFirst)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !(GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED))
    return (0);
  if ((DataSetIndex >= GuiConst_GRAPH_DATASETS_MAX) ||
      (DataSetIndex >= GraphAry[GraphIndex].GraphDataSetCnt))
    return (0);
  if (DataCount > DataSize)
    return (0);
  if (DataFirst >= DataSize)
    return (0);

  GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataPtr =
     (GraphDataPtr)DataPtr;
  GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataSize = DataSize;
  GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataFirst = DataFirst;
  GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataCount = DataCount;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_RemoveDataSet(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((DataSetIndex >= GuiConst_GRAPH_DATASETS_MAX) ||
      (DataSetIndex >= GraphAry[GraphIndex].GraphDataSetCnt))
    return (0);

  GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataCount = 0;
  GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataSize = 0;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_AddDataPoint(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex,
   GuiConst_INT32S DataPointX,
   GuiConst_INT32S DataPointY)
{
  GuiConst_INT16U I;

  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((DataSetIndex >= GuiConst_GRAPH_DATASETS_MAX) ||
      (DataSetIndex >= GraphAry[GraphIndex].GraphDataSetCnt))
    return (0);
  if (GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataCount >=
      GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataSize)
    return (0);

  I = (GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataFirst +
       GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataCount) %
       GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataSize;
  GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataCount++;

  (*GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataPtr)[I].X = DataPointX;
  (*GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataPtr)[I].Y = DataPointY;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_ShowDataSet(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((DataSetIndex >= GuiConst_GRAPH_DATASETS_MAX) ||
      (DataSetIndex >= GraphAry[GraphIndex].GraphDataSetCnt))
    return (0);

  GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Visible = 1;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_HideDataSet(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((DataSetIndex >= GuiConst_GRAPH_DATASETS_MAX) ||
      (DataSetIndex >= GraphAry[GraphIndex].GraphDataSetCnt))
    return (0);

  GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Visible = 0;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_ShowXAxis(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((AxisIndex >= GuiConst_GRAPH_AXES_MAX) ||
      (AxisIndex >= GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_X]))
    return (0);

  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].Visible = 1;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_HideXAxis(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((AxisIndex >= GuiConst_GRAPH_AXES_MAX) ||
      (AxisIndex >= GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_X]))
    return (0);

  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].Visible = 0;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_ShowYAxis(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((AxisIndex >= GuiConst_GRAPH_AXES_MAX) ||
      (AxisIndex >= GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_Y]))
    return (0);

  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].Visible = 1;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_HideYAxis(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((AxisIndex >= GuiConst_GRAPH_AXES_MAX) ||
      (AxisIndex >= GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_Y]))
    return (0);

  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].Visible = 0;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_SetXAxisRange(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex,
   GuiConst_INT32S MinValue,
   GuiConst_INT32S MaxValue)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((AxisIndex >= GuiConst_GRAPH_AXES_MAX) ||
      (AxisIndex >= GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_X]))
    return (0);

  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
     NumbersMinValue = MinValue;
  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
     NumbersMaxValue = MaxValue;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_SetYAxisRange(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex,
   GuiConst_INT32S MinValue,
   GuiConst_INT32S MaxValue)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((AxisIndex >= GuiConst_GRAPH_AXES_MAX) ||
      (AxisIndex >= GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_Y]))
    return (0);

  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
     NumbersMinValue = MinValue;
  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
     NumbersMaxValue = MaxValue;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_OffsetXAxisOrigin(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex,
   GuiConst_INT32S Offset)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((AxisIndex >= GuiConst_GRAPH_AXES_MAX) ||
      (AxisIndex >= GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_X]))
    return (0);

  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
     NumbersMinValue += Offset;
  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_X].
     NumbersMaxValue += Offset;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_OffsetYAxisOrigin(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex,
   GuiConst_INT32S Offset)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((AxisIndex >= GuiConst_GRAPH_AXES_MAX) ||
      (AxisIndex >= GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_Y]))
    return (0);

  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
     NumbersMinValue += Offset;
  GraphAry[GraphIndex].GraphAxes[AxisIndex][GuiLib_GRAPHAXIS_Y].
     NumbersMaxValue += Offset;

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_Redraw(
   GuiConst_INT8U GraphIndex)
{
  GuiConst_INT16S I;

  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  Graph_SetClipping(GraphIndex);
#endif

  GuiLib_Graph_DrawAxes(GraphIndex);

  for (I = GraphAry[GraphIndex].GraphDataSetCnt - 1; I >= 0; I--)
    GuiLib_Graph_DrawDataSet(GraphIndex, I);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  Graph_ResetClipping();
#endif

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_DrawAxes(
   GuiConst_INT8U GraphIndex)
{
  GuiConst_INT16S I;

  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  Graph_SetClipping(GraphIndex);
#endif

  if (!(GraphAry[GraphIndex].GraphItem.BitFlags & GuiLib_BITFLAG_TRANSPARENT))
    GuiLib_FillBox(GraphAry[GraphIndex].GraphItem.X1,
                   GraphAry[GraphIndex].GraphItem.Y1,
                   GraphAry[GraphIndex].GraphItem.X2,
                   GraphAry[GraphIndex].GraphItem.Y2,
                   GraphAry[GraphIndex].GraphItem.BackColor);

  for (I = GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_X] - 1;
       I >= 0; I--)
    if (GraphAry[GraphIndex].GraphAxes[I][GuiLib_GRAPHAXIS_X].Visible)
      Graph_DrawXAxis(GraphIndex, I);

  for (I = GraphAry[GraphIndex].GraphAxesCnt[GuiLib_GRAPHAXIS_Y] - 1;
       I >= 0; I--)
    if (GraphAry[GraphIndex].GraphAxes[I][GuiLib_GRAPHAXIS_Y].Visible)
      Graph_DrawYAxis(GraphIndex, I);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  Graph_ResetClipping();
#endif

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_DrawDataSet(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex)
{
  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((DataSetIndex >= GuiConst_GRAPH_DATASETS_MAX) ||
      (DataSetIndex >= GraphAry[GraphIndex].GraphDataSetCnt))
    return (0);
  if (!GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Visible)
    return (0);
  if (GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataSize == 0)
    return (0);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  Graph_SetClipping(GraphIndex);
#endif

  Graph_DrawDataSet(GraphIndex, DataSetIndex);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  Graph_ResetClipping();
#endif

  return (1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Graph_DrawDataPoint(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex,
   GuiConst_INT16U DataIndex)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S DX, DY;
  GuiConst_INT16S LastDX, LastDY;
  GuiConst_INT32S TX, TY;

  if ((GraphIndex >= GuiConst_GRAPH_MAX) ||
      !GraphAry[GraphIndex].InUse == GuiLib_SCROLL_STRUCTURE_USED)
    return (0);
  if ((DataSetIndex >= GuiConst_GRAPH_DATASETS_MAX) ||
      (DataSetIndex >= GraphAry[GraphIndex].GraphDataSetCnt))
    return (0);
  if (GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataSize == 0)
    return (0);
  if (!GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Visible)
    return (0);
  if (DataIndex >= GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataCount)
    return (0);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  Graph_SetClipping(GraphIndex);
#endif

  Graph_CalcScaleX(GraphIndex,
                   GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexX);
  Graph_CalcScaleY(GraphIndex,
                   GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexY);

  TX = (*GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataPtr)[DataIndex].X;
  TY = (*GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataPtr)[DataIndex].Y;
  DX = GraphAry[GraphIndex].OrigoX + ((TX -
       GraphAry[GraphIndex].GraphAxes[
       GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexX]
       [GuiLib_GRAPHAXIS_X].NumbersMinValue) *
       GraphAry[GraphIndex].GraphAxes[
       GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexX]
       [GuiLib_GRAPHAXIS_X].Scale) / 10000;
  DY = GraphAry[GraphIndex].OrigoY - ((TY -
       GraphAry[GraphIndex].GraphAxes[
       GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexY]
       [GuiLib_GRAPHAXIS_Y].NumbersMinValue) *
       GraphAry[GraphIndex].GraphAxes[
       GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexY]
       [GuiLib_GRAPHAXIS_Y].Scale) / 10000;
  if (GraphAry[GraphIndex].GraphDataSets[DataSetIndex].Representation ==
      GuiLib_GRAPH_DATATYPE_LINE)
  {
    if (DataIndex == 0)
      DataIndex = GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataCount - 1;
    else
      DataIndex--;

    TX =
       (*GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataPtr)[DataIndex].X;
    TY =
       (*GraphAry[GraphIndex].GraphDataSets[DataSetIndex].DataPtr)[DataIndex].Y;
    LastDX = GraphAry[GraphIndex].OrigoX + ((TX -
            GraphAry[GraphIndex].GraphAxes[
            GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexX]
            [GuiLib_GRAPHAXIS_X].NumbersMinValue) *
            GraphAry[GraphIndex].GraphAxes[
            GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexX]
            [GuiLib_GRAPHAXIS_X].Scale) / 10000;
    LastDY = GraphAry[GraphIndex].OrigoY - ((TY -
             GraphAry[GraphIndex].GraphAxes[
             GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexY]
             [GuiLib_GRAPHAXIS_Y].NumbersMinValue) *
             GraphAry[GraphIndex].GraphAxes[
             GraphAry[GraphIndex].GraphDataSets[DataSetIndex].AxisIndexY]
             [GuiLib_GRAPHAXIS_Y].Scale) / 10000;
  }
  else
  {
    LastDX = DX;
    LastDY = DY;
  }

  Graph_DrawDataPoint(
     GraphIndex,
     DataSetIndex,
     DataIndex,
     DX, DY,
     LastDX, LastDY);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  Graph_ResetClipping();
#endif

  return (1);
}
#endif

#ifdef GuiConst_TEXTBOX_FIELDS_ON
//------------------------------------------------------------------------------
static GuiConst_INT16S TextBox_Scroll_CalcEndPos(
   GuiConst_INT8U TextBoxIndex,
   GuiConst_INT8U PerLine)
{
  GuiConst_INT16S H;

  H = TextboxScrollItems[TextBoxIndex].Y2 -
      TextboxScrollItems[TextBoxIndex].Y1 + 1;
  if (PerLine)
    return (TextboxScrollItems[TextBoxIndex].TextBoxLineDist *
       (TextboxScrollItems[TextBoxIndex].TextBoxLines -
       (H / TextboxScrollItems[TextBoxIndex].TextBoxLineDist)));
  else
    return (TextboxScrollItems[TextBoxIndex].TextBoxLines *
       TextboxScrollItems[TextBoxIndex].TextBoxLineDist - H);
}
#endif

//------------------------------------------------------------------------------
static GuiConst_INT8U TextBox_Scroll_To(
   GuiConst_INT8U TextBoxIndex,
   GuiConst_INT16S NewPos,
   GuiConst_INT8U PerLine,
   GuiConst_INT8U AbsoluteMove)
{
#ifdef GuiConst_TEXTBOX_FIELDS_ON
  if ((TextBoxIndex < GuiConst_TEXTBOX_FIELDS_MAX) &&
      (TextboxScrollItems[TextBoxIndex].TextBoxScrollIndex != 0xFF))
  {
    if (PerLine)
      NewPos *= TextboxScrollItems[TextBoxIndex].TextBoxLineDist;

    switch (AbsoluteMove)
    {
      case 0:
        TextboxScrollItems[TextBoxIndex].TextBoxScrollPos += NewPos;
        break;

      case 1:
        TextboxScrollItems[TextBoxIndex].TextBoxScrollPos = NewPos;
        break;

      case 2:
        TextboxScrollItems[TextBoxIndex].TextBoxScrollPos =
           TextBox_Scroll_CalcEndPos(TextBoxIndex, PerLine);
        break;
    }
    memcpy(&CurItem, &TextboxScrollItems[TextBoxIndex], sizeof(GuiLib_ItemRec));

    GuiDisplay_Lock();

    DisplayLevel = 0;
    SwapColors = 0;
    DrawItem(GuiLib_COL_INVERT_OFF);

    GuiDisplay_Unlock();
    return (1);
  }
  else
    return (0);
#else
  Dummy1_8U = TextBoxIndex;   // To avoid compiler warning
  Dummy1_16S = NewPos;   // To avoid compiler warning
  Dummy2_8U = PerLine;   // To avoid compiler warning
  Dummy3_8U = AbsoluteMove;   // To avoid compiler warning
  return (0);
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_Up(
   GuiConst_INT8U TextBoxIndex)
{
  return (TextBox_Scroll_To(TextBoxIndex, -1, 1, 0));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_Down(
   GuiConst_INT8U TextBoxIndex)
{
  return (TextBox_Scroll_To(TextBoxIndex, 1, 1, 0));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_Home(
   GuiConst_INT8U TextBoxIndex)
{
  return (TextBox_Scroll_To(TextBoxIndex, 0, 1, 1));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_End(
   GuiConst_INT8U TextBoxIndex)
{
  return (TextBox_Scroll_To(TextBoxIndex, 0, 1, 2));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_To_Line(
   GuiConst_INT8U TextBoxIndex,
   GuiConst_INT16S NewLine)
{
  return (TextBox_Scroll_To(TextBoxIndex, NewLine, 1, 1));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_Up_Pixel(
   GuiConst_INT8U TextBoxIndex)
{
  return (TextBox_Scroll_To(TextBoxIndex, -1, 0, 0));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_Down_Pixel(
   GuiConst_INT8U TextBoxIndex)
{
  return (TextBox_Scroll_To(TextBoxIndex, 1, 0, 0));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_Home_Pixel(
   GuiConst_INT8U TextBoxIndex)
{
  return (TextBox_Scroll_To(TextBoxIndex, 0, 0, 1));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_End_Pixel(
   GuiConst_INT8U TextBoxIndex)
{
  return (TextBox_Scroll_To(TextBoxIndex, 0, 0, 2));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_To_PixelLine(
   GuiConst_INT8U TextBoxIndex,
   GuiConst_INT16S NewPixelLine)
{
  return (TextBox_Scroll_To(TextBoxIndex, NewPixelLine, 0, 1));
}

//------------------------------------------------------------------------------
static GuiConst_INT8U TextBox_Scroll_Get_Pos(
   GuiConst_INT8U TextBoxIndex,
   GuiConst_INT8U PerLine)
{
#ifdef GuiConst_TEXTBOX_FIELDS_ON
  GuiConst_INT16S P;

  if ((TextBoxIndex < GuiConst_TEXTBOX_FIELDS_MAX) &&
      (TextboxScrollItems[TextBoxIndex].TextBoxScrollIndex != 0xFF))
  {
    P = TextBox_Scroll_CalcEndPos(TextBoxIndex, PerLine);
    if (TextboxScrollItems[TextBoxIndex].TextBoxScrollPos == 0)
      return (GuiLib_TEXTBOX_SCROLL_AT_HOME);
    else if (TextboxScrollItems[TextBoxIndex].TextBoxScrollPos == P)
      return (GuiLib_TEXTBOX_SCROLL_AT_END);
    else if (TextboxScrollItems[TextBoxIndex].TextBoxScrollPos < 0)
      return (GuiLib_TEXTBOX_SCROLL_ABOVE_HOME);
    else if (TextboxScrollItems[TextBoxIndex].TextBoxScrollPos > P)
      return (GuiLib_TEXTBOX_SCROLL_BELOW_END);
    else
      return (GuiLib_TEXTBOX_SCROLL_INSIDE_BLOCK);
  }
  else
    return (GuiLib_TEXTBOX_SCROLL_ILLEGAL_NDX);
#else
  Dummy1_8U = TextBoxIndex;   // To avoid compiler warning
  Dummy2_8U = PerLine;   // To avoid compiler warning
  return (GuiLib_TEXTBOX_SCROLL_ILLEGAL_NDX);
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_Get_Pos(
   GuiConst_INT8U TextBoxIndex)
{
  return (TextBox_Scroll_Get_Pos(TextBoxIndex, 1));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_Get_Pos_Pixel(
   GuiConst_INT8U TextBoxIndex)
{
  return (TextBox_Scroll_Get_Pos(TextBoxIndex, 0));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_TextBox_Scroll_FitsInside(
   GuiConst_INT8U TextBoxIndex)
{
#ifdef GuiConst_TEXTBOX_FIELDS_ON
  if ((TextBoxIndex < GuiConst_TEXTBOX_FIELDS_MAX) &&
      (TextboxScrollItems[TextBoxIndex].TextBoxScrollIndex != 0xFF))
    return (TextBox_Scroll_CalcEndPos(TextBoxIndex, 0) < 0);
  else
    return (0);
#else
  Dummy1_8U = TextBoxIndex;   // To avoid compiler warning
  return (0);
#endif
}

#ifdef GuiConst_REMOTE_DATA
//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_RemoteCheck(void)
{
  union
  {
    GuiConst_INT8U Bytes[4];
    GuiConst_INT32U IdInFile;
  } RemoteIdUnion;

  GuiLib_RemoteDataReadBlock(0, 4, RemoteIdUnion.Bytes);
  return (RemoteIdUnion.IdInFile == GuiConst_REMOTE_ID);
}
#endif

//------------------------------------------------------------------------------
static void ScrollBox_SetTopLine_Custom(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16S TopLine)
{
  if (ScrollBoxesAry[ScrollBoxIndex].ScrollMode == 0)
  {
      ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
         GuiLib_GET_MINMAX(TopLine, 0,
         ScrollBoxesAry[ScrollBoxIndex].NumberOfLines -
         ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines);
  }
  else
    ScrollBoxesAry[ScrollBoxIndex].ScrollTopLine =
       ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] -
       ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs;
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_ScrollBox_Init_Custom_SetTopLine(
   GuiConst_INT8U ScrollBoxIndex,
   void (*DataFuncPtr) (GuiConst_INT16S LineIndex),
   GuiConst_INT16S NoOfLines,
   GuiConst_INT16S ActiveLine,
   GuiConst_INT16S TopLine)
{
  GuiConst_INT16U  StructToCallIndex;
  GuiLib_StructPtr StructToCall;
  GuiLib_ItemRec   RemCurItem;
#ifdef GuiConst_CURSOR_SUPPORT_ON
  GuiConst_INT8U RemCursorInUse;
#endif

  GuiLib_LIMIT_MINMAX(ActiveLine, -1, NoOfLines - 1);

  if (ScrollBoxesAry[ScrollBoxIndex].InUse == GuiLib_SCROLL_STRUCTURE_READ)
  {
    memcpy(&RemCurItem, &CurItem, sizeof(GuiLib_ItemRec));

    GlobalScrollBoxIndex = ScrollBoxIndex;

    memcpy(&CurItem, &ScrollBoxesAry[ScrollBoxIndex].ScrollBoxItem,
       sizeof(GuiLib_ItemRec));
    StructToCallIndex = ScrollBoxesAry[ScrollBoxIndex].LineStructIndex;
    if (StructToCallIndex != 0xFFFF)
    {
      DisplayLevel++;
      StructToCall = (GuiLib_StructPtr)
#ifdef GuiConst_REMOTE_STRUCT_DATA
         GetRemoteStructData(StructToCallIndex);
#else
#ifdef GuiConst_AVRGCC_COMPILER
         pgm_read_word(&GuiStruct_StructPtrList[StructToCallIndex]);
#else
         GuiStruct_StructPtrList[StructToCallIndex];
#endif
#endif
      NextScrollLineReading = 1;
      InitialDrawing = 1;
#ifdef GuiConst_CURSOR_SUPPORT_ON
      RemCursorInUse = CursorInUse;
      ScrollBoxesAry[ScrollBoxIndex].ContainsCursorFields = 0;
      CursorInUse = (GuiLib_ActiveCursorFieldNo >= 0);
      CursorFieldFound = -1;
      CursorActiveFieldFound = 0;
#endif
      DrawStructure(StructToCall, GuiLib_COL_INVERT_OFF);
#ifdef GuiConst_CURSOR_SUPPORT_ON
      InitialDrawing = 0;
      if (CursorFieldFound == -1)
        CursorInUse = 0;
      else
      {
        ScrollBoxesAry[ScrollBoxIndex].ContainsCursorFields = 1;
        if (CursorActiveFieldFound == 0)
        {
          GuiLib_ActiveCursorFieldNo = CursorFieldFound;

          DrawCursorItem(1);
        }
      }
      CursorInUse = CursorInUse | RemCursorInUse;
#endif
      NextScrollLineReading = 0;
      DisplayLevel--;
    }

    SetCurFont(CurItem.FontIndex);
    if ((ScrollBoxesAry[ScrollBoxIndex].LineSizeY == 0) &&
        (ScrollBoxesAry[ScrollBoxIndex].LineSizeY2 == 0))
    {
      ScrollBoxesAry[ScrollBoxIndex].LineSizeY = CurFont->BaseLine;
      ScrollBoxesAry[ScrollBoxIndex].LineSizeY2 =
         CurFont->YSize - CurFont->BaseLine - 1;
    }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
    GuiLib_SetClipping(CurItem.ClipRectX1,CurItem.ClipRectY1,
                       CurItem.ClipRectX2,CurItem.ClipRectY2);
#endif

    StructToCallIndex = ScrollBoxesAry[ScrollBoxIndex].MakeUpStructIndex;
    if (StructToCallIndex != 0xFFFF)
    {
      DisplayLevel++;
      memcpy(&CurItem, &ScrollBoxesAry[ScrollBoxIndex].ScrollBoxItem,
         sizeof(GuiLib_ItemRec));
      CurItem.RX = ScrollBoxesAry[GlobalScrollBoxIndex].X1;
      CurItem.RY = ScrollBoxesAry[GlobalScrollBoxIndex].Y1;
      StructToCall = (GuiLib_StructPtr)
#ifdef GuiConst_REMOTE_STRUCT_DATA
         GetRemoteStructData(StructToCallIndex);
#else
#ifdef GuiConst_AVRGCC_COMPILER
         pgm_read_word(&GuiStruct_StructPtrList[StructToCallIndex]);
#else
         GuiStruct_StructPtrList[StructToCallIndex];
#endif
#endif
      DrawStructure(StructToCall,GuiLib_COL_INVERT_OFF);
      DisplayLevel--;
    }

    ScrollBoxesAry[ScrollBoxIndex].ScrollLineDataFunc = DataFuncPtr;
    ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[0] = ActiveLine;
    ScrollBoxesAry[ScrollBoxIndex].LastMarkerLine = ActiveLine;
    ScrollBoxesAry[ScrollBoxIndex].NumberOfLines = NoOfLines;
    if ((ScrollBoxesAry[ScrollBoxIndex].ScrollMode == 0) &&
        (NoOfLines < ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines))
      ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines = NoOfLines;
    ScrollBoxesAry[ScrollBoxIndex].InUse = GuiLib_SCROLL_STRUCTURE_USED;
    ScrollBox_SetTopLine_Custom(ScrollBoxIndex, TopLine);
    GuiLib_ScrollBox_Redraw(ScrollBoxIndex);
    //ScrollBoxesAry[ScrollBoxIndex].LastScrollTopLine = 0xFFFF; 

    memcpy(&CurItem, &RemCurItem, sizeof(GuiLib_ItemRec));

    return (1);
  }
  else
    return (0);
}

