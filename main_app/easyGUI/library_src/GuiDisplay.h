/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2011 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*                       easyGUI display driver unit                        */
/*                               v6.0.3.001                                 */
/*                                                                          */
/* ************************************************************************ */

#ifndef __GUIDISPLAY_H
#define __GUIDISPLAY_H

#include "GuiConst.h"
#ifndef GuiConst_CODEVISION_COMPILER
#include "GuiLib.h"
#endif

/**
 * To support double-buffering, we've introduced a macro that allows us to 
 * alternate between using and not using the double-buffering. Initial tests 
 * have shown the double-buffering to work well and remove a flickering that was 
 * seen when using scroll boxes, but further testing is required to prove that. 
 *  
 * @author 11/27/2012 Adrianusv
 */
#define		GUILIB_USE_DOUBLE_BUFFERING

/**
 * These have all become dummy functions in our project so define them this way. 
 * We use a display task to control the renetrancy. Read in top of the 
 * GuiDisplay.c file. And there is nothing to do in the init. 
 *  
 * @author bobd (5/13/2009) 
 */
/*
extern void GuiDisplay_Lock (void);
extern void GuiDisplay_Unlock (void);
extern void GuiDisplay_Init (void);
*/

#ifdef	GUILIB_USE_DOUBLE_BUFFERING
	extern void GuiDisplay_Refresh (void);
#else
	/**
	 * And as opposed to removing the function call [is called from within 
	 * GuiLib.c file and we do not want to modify that file - we make this 
	 * define here 
	 *  
	 * @author bobd (5/13/2009) 
	 */
	#define GuiDisplay_Refresh() {}
#endif

/**
 * And as opposed to removing the function call [is called from within GuiLib.c 
 * file and we do not want to modify that file - we make this define here 
 *  
 * @author bobd (5/13/2009) 
 */
#define GuiDisplay_Lock() {}
#define GuiDisplay_Unlock() {}
#define GuiDisplay_Init() {}

#define	GuiConst_DISPLAY_BUFFER_EASYGUI

#ifndef GuiConst_DISPLAY_BUFFER_EASYGUI
extern GuiConst_INTCOLOR GuiDisplay_BufferRead(
   GuiConst_INT32U Offset,
   GuiConst_INT32U Count);
extern void GuiDisplay_BufferWrite(
   GuiConst_INT32U Offset,
   GuiConst_INTCOLOR *Pixel,
   GuiConst_INT32S Count);
#endif

#endif
