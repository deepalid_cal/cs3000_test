/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2008 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*          4 bit (16 color/grayscale) graphics primitives library          */
/*                      Horizontal display bytes layout                     */
/*                               v5.4.3.015                                 */
/*                                                                          */
/*     GuiLib.c include file - do NOT reference it in your linker setup     */
/*                                                                          */
/* ************************************************************************ */

// 2012.02.13 ajv : Since we declare GuiLib_DisplayBuf in lcd_init.h, we need
// to ensure we include it here so it's available in easyGUI's code.
#include	"lcd_init.h"

//------------------------------------------------------------------------------
/*
GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
*/

#ifdef GuiConst_BIT_TOPLEFT
const GuiConst_INT8U LinePattern2Left[2] = {0xFF, 0xF0};
const GuiConst_INT8U LinePattern2Right[2] = {0x0F, 0xFF};
const GuiConst_INT8U LinePattern2Pixel[2] = {0x0F, 0xF0};

const GuiConst_INT8U Color4BitPixelPattern[4][16] =
  {{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   {0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70,
    0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0},
   {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F},
   {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
    0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF}};
#else
const GuiConst_INT8U LinePattern2Left[2] = {0xFF, 0x0F};
const GuiConst_INT8U LinePattern2Right[2] = {0xF0, 0xFF};
const GuiConst_INT8U LinePattern2Pixel[2] = {0xF0, 0x0F};

const GuiConst_INT8U Color4BitPixelPattern[4][16] =
  {{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F},
   {0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70,
    0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0},
   {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
    0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF}};
#endif

const GuiConst_INT8U Color4BitPattern[16] =
   {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
    0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF};

//==============================================================================

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
static void SetClipping(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  if (X1 < 0)
    X1 = 0;
  if (Y1 < 0)
    Y1 = 0;
  if (X2 > GuiConst_DISPLAY_WIDTH_HW - 1)
    X2 = GuiConst_DISPLAY_WIDTH_HW - 1;
  if (Y2 > GuiConst_DISPLAY_HEIGHT_HW - 1)
    Y2 = GuiConst_DISPLAY_HEIGHT_HW - 1;

  ClippingX1 = X1;
  ClippingY1 = Y1;
  ClippingX2 = X2;
  ClippingY2 = Y2;
}
#endif

//------------------------------------------------------------------------------
static void MarkDisplayBoxRepaint(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
  X1 = 2 * (X1 / 4);
  X2 = 2 * (X2 / 4) + 1;
#else
  X1 = X1 / 2;
  X2 = X2 / 2;
#endif

  while (Y1 <= Y2)
  {
    if ((GuiLib_DisplayRepaint[Y1].ByteEnd == -1) ||
        (X1 < GuiLib_DisplayRepaint[Y1].ByteBegin))
      GuiLib_DisplayRepaint[Y1].ByteBegin = X1;
    if (X2 > GuiLib_DisplayRepaint[Y1].ByteEnd)
      GuiLib_DisplayRepaint[Y1].ByteEnd = X2;

    Y1++;
  }
}

//------------------------------------------------------------------------------
static void ClearDisplay(void)
{
  memset(GuiLib_DisplayBuf, Color4BitPattern[GuiConst_PIXEL_OFF],
     GuiConst_DISPLAY_BYTES);
}

//------------------------------------------------------------------------------
static void MakeDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S Xb;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X, &Y, &X, &Y))
#endif
  {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    Xb = (X / 2) - (2 * ((X / 2) % 2)) + 1;
#else
    Xb = X / 2;
#endif
    GuiLib_DisplayBuf[Y][Xb] =
       (GuiLib_DisplayBuf[Y][Xb] & ~LinePattern2Pixel[X % 2]) |
       (Color4BitPattern[Color] & LinePattern2Pixel[X % 2]);

    MarkDisplayBoxRepaint(X, Y, X, Y);
  }
}

//------------------------------------------------------------------------------
static GuiConst_INTCOLOR ReadDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y)
{
  if ((X < 0) || (X >= GuiConst_DISPLAY_WIDTH_HW) ||
      (Y < 0) || (Y >= GuiConst_DISPLAY_HEIGHT_HW))
    return (0);
  else
#ifdef GuiConst_BIT_TOPLEFT
    return ((GuiLib_DisplayBuf[Y][X / 2] & LinePattern2Pixel[X % 2]) >>
       (4 * X % 2));
#else
    return ((GuiLib_DisplayBuf[Y][X / 2] & LinePattern2Pixel[X % 2]) >>
       (4 * (1 - X % 2)));
#endif
}

//------------------------------------------------------------------------------
static void HorzLine(
   GuiConst_INT16S X1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S X, X1b, X2b, Xb;

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
  X1b = (X1 / 2) - (2 * ((X1 / 2) % 2)) + 1;
  X2b = (X2 / 2) - (2 * ((X2 / 2) % 2)) + 1;
#else
  X1b = X1 / 2;
  X2b = X2 / 2;
#endif

  if (X1b == X2b)
    GuiLib_DisplayBuf[Y][X1b] =
       (GuiLib_DisplayBuf[Y][X1b] &
       ~(LinePattern2Left[X1 % 2] & LinePattern2Right[X2 % 2])) |
       (Color4BitPattern[Color] &
       (LinePattern2Left[X1 % 2] & LinePattern2Right[X2 % 2]));
  else
  {
    GuiLib_DisplayBuf[Y][X1b] =
       (GuiLib_DisplayBuf[Y][X1b] & ~LinePattern2Left[X1 % 2]) |
       (Color4BitPattern[Color] & LinePattern2Left[X1 % 2]);

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    for (X = X1 / 2 + 1; X < X2 / 2; X++)
    {
      if (X % 2 == 0)
        Xb = X + 1;
      else
        Xb = X - 1;
      GuiLib_DisplayBuf[Y][Xb] = Color4BitPattern[Color];
    }
#else
    for (X = X1b + 1; X < X2b; X++)
      GuiLib_DisplayBuf[Y][X] = Color4BitPattern[Color];
#endif

    GuiLib_DisplayBuf[Y][X2b] =
       (GuiLib_DisplayBuf[Y][X2b] & ~LinePattern2Right[X2 % 2]) |
       (Color4BitPattern[Color] & LinePattern2Right[X2 % 2]);
  }
}

//------------------------------------------------------------------------------
static void VertLine(
   GuiConst_INT16S X,
   GuiConst_INT16S Y1,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S Xb;
  GuiConst_INT8U Pattern;

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
  Xb = (X / 2) - (2 * ((X / 2) % 2)) + 1;
#else
  Xb = X / 2;
#endif

#ifdef GuiConst_BIT_TOPLEFT
  if ((X % 2) == 0)
    Pattern = Color;
  else
    Pattern = Color << 4;
#else
  if ((X % 2) == 0)
    Pattern = Color << 4;
  else
    Pattern = Color;
#endif
  while (Y1 <= Y2)
  {
    GuiLib_DisplayBuf[Y1][Xb] =
       (GuiLib_DisplayBuf[Y1][Xb] & ~LinePattern2Pixel[X % 2]) | Pattern;
    Y1++;
  }
}

//------------------------------------------------------------------------------
static void DrawChar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiLib_FontRecPtr Font,
#ifdef GuiConst_REMOTE_FONT_DATA
   GuiConst_INT32S CharNdx,
#else
   GuiConst_INT8U PrefixRom * CharPtr,
#endif
   GuiConst_INTCOLOR Color)
{
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *PixelData;
  GuiConst_INT8U * CharPtr;
#else
  GuiConst_INT8U PrefixRom *PixelData;
#endif
  GuiConst_INT16U PixelPattern;
  GuiConst_INT16S N;
  GuiConst_INT8U YHeight;
  GuiConst_INT16S Bx;
  GuiConst_INT16S PY, Y2;
  GuiConst_INT8U PixN;
  GuiConst_INT16S Xb;
  GuiConst_INT8U Pattern;
  GuiConst_INT8U PixelLineSize;
#ifndef GuiConst_FONT_UNCOMPRESSED
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *LineCtrl;
#else
  GuiConst_INT8U PrefixRom *LineCtrl;
#endif
  GuiConst_INT8U LineCtrlByte;
  GuiConst_INT16S LineRepeat;
  GuiConst_INT16S M;
  GuiConst_INT8U Finished;
#endif
#ifdef GuiConst_ADV_FONTS_ON
  GuiConst_INT8U PixelShade, PixelShadeInv;
  GuiConst_INTCOLOR PixelColor;
  GuiConst_INT8U BitOffset;
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (ClippingTotal)
    return;
#endif

#ifdef GuiConst_REMOTE_FONT_DATA
  if (CharNdx != CurRemoteFont)
  {
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx + 1] -
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       GuiLib_RemoteFontBuffer);
    CurRemoteFont = CharNdx;
  }
  CharPtr = &GuiLib_RemoteFontBuffer[0];
#endif

  if ((*(CharPtr + GuiLib_CHR_XWIDTH_OFS) == 0) ||
      (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) == 0))
    return;

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST(Color);

#ifdef GuiConst_FONT_UNCOMPRESSED
  PixelLineSize = Font->LineSize;
  #ifdef GuiConst_ROTATED_90DEGREE
  YHeight = Font->XSize;
  #else
  YHeight = Font->YSize;
  #endif
  PixelData = CharPtr + GuiLib_CHR_LINECTRL_OFS + 1;
#else
  #ifdef GuiConst_ROTATED_90DEGREE
  PixelLineSize = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  YHeight = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  #else
  PixelLineSize = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  YHeight = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  #endif
  LineCtrl = CharPtr + GuiLib_CHR_LINECTRL_OFS;
  N = ((GuiConst_INT16S)YHeight + 7) / 8;
  if (N == 0)
    N++;
  PixelData = LineCtrl + N;
#ifdef GuiConst_ADV_FONTS_ON
  if (Font->ColorDepth == 4)
    PixelLineSize = (PixelLineSize + 1) / 2;
  else
#endif
    PixelLineSize = (PixelLineSize + 7) / 8;
#endif

#ifdef GuiConst_FONT_UNCOMPRESSED

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #else
  X -= Font->XSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #else
  Y -= Font->XSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
    #else
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->XSize - 1;
    #else
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #endif
#endif

#else

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #endif
#endif

#endif

  PY = 0;
#ifndef GuiConst_FONT_UNCOMPRESSED
  LineCtrlByte = *LineCtrl;
  LineCtrlByte >>= 1;
  LineCtrl++;
#endif
  while (PY < YHeight)
  {
#ifndef GuiConst_FONT_UNCOMPRESSED
    LineRepeat = 0;
    do
    {
      LineRepeat++;
      Finished = (((LineCtrlByte & 0x01) == 0) || (PY >= YHeight - 1));

      PY++;
      if (PY % 8 == 7)
      {
        LineCtrlByte = *LineCtrl;
        LineCtrl++;
      }
      else
        LineCtrlByte >>= 1;
    }
    while (!Finished);
#endif

#ifdef GuiConst_ADV_FONTS_ON
    if (Font->ColorDepth == 4)
      Bx = X;
    else
#endif
      Bx = X;

    for (N = 0; N < PixelLineSize; N++)
    {
      PixelPattern = *PixelData;

      if (PixelPattern != 0)
      {
#ifdef GuiConst_ADV_FONTS_ON
        if (Font->ColorDepth == 4)
        {
          for (PixN = 0; PixN < 2; PixN++)
          {
            if (PixN == 0)
              PixelShade = PixelPattern & 0x0F;
            else
              PixelShade = (PixelPattern & 0xF0) >> 4;
            if (
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                (Bx + PixN >= ClippingX1) && (Bx + PixN <= ClippingX2) &&
#endif
               (PixelShade > 0))
            {
              PixelShadeInv = 15-PixelShade;
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
              Xb = ((Bx + PixN) / 2) - (2 * (((Bx + PixN) / 2) & 0x01)) + 1;
#else
              Xb = (Bx + PixN) / 2;
#endif
              Y2 = Y;
#ifdef GuiConst_BIT_TOPLEFT
              if (((Bx + PixN) % 2) == 0)
                BitOffset = 0;
              else
                BitOffset = 4;
#else
              if (((Bx + PixN) % 2) == 0)
                BitOffset = 4;
              else
                BitOffset = 0;
#endif
#ifndef GuiConst_FONT_UNCOMPRESSED
              for (M = 0; M < LineRepeat; M++)
#endif
              {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                if ((Y2 >= ClippingY1) && (Y2 <= ClippingY2))
#endif
                {
                  if (PixelShade == 0x0F)
                    GuiLib_DisplayBuf[Y2][Xb] =
                       (GuiLib_DisplayBuf[Y2][Xb] &
                       ~LinePattern2Pixel[(Bx + PixN) % 2]) |
                       (Color << BitOffset);
                  else
                  {
                    PixelColor =
                       (GuiLib_DisplayBuf[Y2][Xb] >> BitOffset) & 0x0F;
                    GuiLib_DisplayBuf[Y2][Xb] =
                       (GuiLib_DisplayBuf[Y2][Xb] &
                       ~LinePattern2Pixel[(Bx + PixN) % 2]) |
                       (((PixelShade * Color + PixelShadeInv * PixelColor) /
                       15) << BitOffset);
                  }
                }
                Y2++;
              }
            }
          }
        }
        else
#endif
        {
          for (PixN = 0; PixN < 8; PixN++)
          {
            if (
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                (Bx + PixN >= ClippingX1) && (Bx + PixN <= ClippingX2) &&
#endif
#ifdef GuiConst_BIT_TOPLEFT
               ((PixelPattern >> PixN) & 0x01))
#else
               ((PixelPattern >> (7-PixN)) & 0x01))
#endif
            {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
              Xb = ((Bx + PixN) / 2) - (2 * (((Bx + PixN) / 2) & 0x01)) + 1;
#else
              Xb = (Bx + PixN) / 2;
#endif
              Y2 = Y;

#ifdef GuiConst_BIT_TOPLEFT
              if (((Bx + PixN) % 2) == 0)
                Pattern = Color;
              else
                Pattern = Color << 4;
#else
              if (((Bx + PixN) % 2) == 0)
                Pattern = Color << 4;
              else
                Pattern = Color;
#endif
#ifndef GuiConst_FONT_UNCOMPRESSED
              for (M = 0; M < LineRepeat; M++)
#endif
              {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                if ((Y2 >= ClippingY1) && (Y2 <= ClippingY2))
#endif
                {
                  GuiLib_DisplayBuf[Y2][Xb] =
                     (GuiLib_DisplayBuf[Y2][Xb] &
                     ~LinePattern2Pixel[(Bx + PixN) % 2]) |
                     Pattern;
                }
                Y2++;
              }
            }
          }
        }
      }

      PixelData++;
#ifdef GuiConst_ADV_FONTS_ON
      if (Font->ColorDepth == 4)
        Bx+=2;
      else
#endif
        Bx+=8;
    }

#ifdef GuiConst_FONT_UNCOMPRESSED
    PY++;
    Y++;
#else
    Y += LineRepeat;
#endif
  }
}

#ifdef GuiConst_BITMAP_SUPPORT_ON
//------------------------------------------------------------------------------
static void ShowBitmapArea(
#ifdef GuiConst_REMOTE_BITMAP_DATA
   GuiConst_INT8U * PixelDataPtr,
#else
   GuiConst_INT8U PrefixRom * PixelDataPtr,
#endif
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT32S TranspColor,
   GuiConst_INT8U BitmapType)
{
  GuiConst_INT16S SizeX;
  GuiConst_INT16S SizeY;
  GuiConst_INT16S ByteSizeX;
  GuiConst_INT8U PrefixRom *PixelDataPtr2;
  GuiConst_INT8U B;
  GuiConst_INT16S X1b, X2b, Xb, Xbyte;
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
  GuiConst_INT16S Xbyte2;
#endif
  GuiConst_INT8U BitOffsetX1, BitOffsetX2;

  SizeX = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeX += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  ByteSizeX = (SizeX + 1) / 2;

  BitmapWriteX2 = X + SizeX - 1;
  BitmapWriteY2 = Y + SizeY - 1;

  GuiLib_COORD_ADJUST(X, Y);
  if (TranspColor != -1)
    GuiLib_COLOR_ADJUST(TranspColor);

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    GuiLib_COORD_ADJUST(X1, Y1);
    GuiLib_COORD_ADJUST(X2, Y2);
    OrderCoord(&X1, &X2);
    OrderCoord(&Y1, &Y2);
  }

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #endif
#endif

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    if ((X1 > X + SizeX - 1) || (X2 < X) || (Y1 > Y + SizeY - 1) || (Y2 < Y))
      return;
    if (X1 < X)
      X1 = X;
    if (X2 > X + SizeX - 1)
      X2 = X + SizeX - 1;
    if (Y1 < Y)
      Y1 = Y;
    if (Y2 > Y + SizeY - 1)
      Y2 = Y + SizeY - 1;
  }
  else
  {
    X2 = X + SizeX - 1;
    Y2 = Y + SizeY - 1;

    OrderCoord(&X, &X2);
    OrderCoord(&Y, &Y2);

    X1 = X;
    Y1 = Y;
  }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);   // Before changing Y1

    if (X < 0)
      Xb = -((1 - X) / 2);
    else
      Xb = X / 2;
    X1b = X1 / 2;
    X2b = X2 / 2;

    PixelDataPtr += ByteSizeX * (Y1 - Y) + X1b - Xb;

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    Xb = Xb - (2 * (Xb % 2)) + 1;
    X1b = X1b - (2 * (X1b % 2)) + 1;
    X2b = X2b - (2 * (X2b % 2)) + 1;
#endif

    BitOffsetX1 = X1 % 2;
    BitOffsetX2 = X2 % 2;

    while (Y1 <= Y2)
    {
      if (X % 2 == 0)
      {
        if (X1b == X2b)
          GuiLib_DisplayBuf[Y1][X1b] =
             (GuiLib_DisplayBuf[Y1][X1b] &
             ~(LinePattern2Left[BitOffsetX1] &
             LinePattern2Right[BitOffsetX2])) |
             (*PixelDataPtr &
             (LinePattern2Left[BitOffsetX1] & LinePattern2Right[BitOffsetX2]));
        else
        {
          PixelDataPtr2 = PixelDataPtr;

          GuiLib_DisplayBuf[Y1][X1b] =
             (GuiLib_DisplayBuf[Y1][X1b] & ~LinePattern2Left[BitOffsetX1]) |
             (*PixelDataPtr & LinePattern2Left[BitOffsetX1]);
          PixelDataPtr2++;

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
          for (Xbyte = X1 / 2 + 1; Xbyte < X2 / 2; Xbyte++)
          {
            if (Xbyte % 2 == 0)
              Xbyte2 = Xbyte + 1;
            else
              Xbyte2 = Xbyte - 1;
            GuiLib_DisplayBuf[Y1][Xbyte2] = *PixelDataPtr2;
#else
          for (Xbyte = X1b + 1; Xbyte < X2b; Xbyte++)
          {
            GuiLib_DisplayBuf[Y1][Xbyte] = *PixelDataPtr2;
#endif
            PixelDataPtr2++;
          }

          GuiLib_DisplayBuf[Y1][X2b] =
             (GuiLib_DisplayBuf[Y1][X2b] & ~LinePattern2Right[BitOffsetX2]) |
             (*PixelDataPtr2 & LinePattern2Right[BitOffsetX2]);
        }
      }
      else
      {
        PixelDataPtr2 = PixelDataPtr;

        if (X1b == Xb)
#ifdef GuiConst_BIT_TOPLEFT
          B = ((*PixelDataPtr2) & 0x0F) << 4;
#else
          B = (*PixelDataPtr2) >> 4;
#endif
        else
#ifdef GuiConst_BIT_TOPLEFT
          B = ((*(PixelDataPtr2 - 1)) >> 4) +
              (((*PixelDataPtr2) & 0x0F) << 4);
#else
          B = (((*(PixelDataPtr2 - 1)) & 0x0F) << 4) +
              ((*PixelDataPtr2) >> 4);
#endif
        GuiLib_DisplayBuf[Y1][X1b] =
           (GuiLib_DisplayBuf[Y1][X1b] & ~LinePattern2Left[BitOffsetX1]) |
           (B & LinePattern2Left[BitOffsetX1]);
        PixelDataPtr2++;

        if (X2b > X1b)
        {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
          for (Xbyte = X1 / 2 + 1; Xbyte < X2 / 2; Xbyte++)
          {
            if (Xbyte % 2 == 0)
              Xbyte2 = Xbyte + 1;
            else
              Xbyte2 = Xbyte - 1;
#else
          for (Xbyte = X1b + 1; Xbyte < X2b; Xbyte++)
          {
#endif
#ifdef GuiConst_BIT_TOPLEFT
            B = ((*(PixelDataPtr2 - 1)) >> 4) +
                (((*PixelDataPtr2) & 0x0F) << 4);
#else
            B = (((*(PixelDataPtr2 - 1)) & 0x0F) << 4) +
                ((*PixelDataPtr2) >> 4);
#endif
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
            GuiLib_DisplayBuf[Y1][Xbyte2] = B;
#else
            GuiLib_DisplayBuf[Y1][Xbyte] = B;
#endif
            PixelDataPtr2++;
          }

#ifdef GuiConst_BIT_TOPLEFT
            B = ((*(PixelDataPtr2 - 1)) >> 4) +
                (((*PixelDataPtr2) & 0x0F) << 4);
#else
            B = (((*(PixelDataPtr2 - 1)) & 0x0F) << 4) +
                ((*PixelDataPtr2) >> 4);
#endif
          GuiLib_DisplayBuf[Y1][X2b] =
             (GuiLib_DisplayBuf[Y1][X2b] & ~LinePattern2Right[BitOffsetX2]) |
             (B & LinePattern2Right[BitOffsetX2]);
        }
      }

      PixelDataPtr += ByteSizeX;
      Y1++;
    }
  }
}
#endif

//==============================================================================

//------------------------------------------------------------------------------
void GuiLib_InvertBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiConst_INT16S X, X1b, X2b, Xb;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);

  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);   // Before changing Y1
    while (Y1 <= Y2)
    {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
      X1b = (X1 / 2) - (2 * ((X1 / 2) % 2)) + 1;
      X2b = (X2 / 2) - (2 * ((X2 / 2) % 2)) + 1;
#else
      X1b = X1 / 2;
      X2b = X2 / 2;
#endif

      if (X1b == X2b)
        GuiLib_DisplayBuf[Y1][X1b] ^=
           LinePattern2Left[X1 % 2] & LinePattern2Right[X2 % 2];
      else
      {
        GuiLib_DisplayBuf[Y1][X1b] ^= LinePattern2Left[X1 % 2];

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
        for (X = X1 / 2 + 1; X < X2 / 2; X++)
        {
          if (X % 2 == 0)
            Xb = X + 1;
          else
            Xb = X - 1;
          GuiLib_DisplayBuf[Y1][Xb] = ~GuiLib_DisplayBuf[Y1][Xb];
        }
#else
        for (X = X1b + 1; X < X2b; X++)
          GuiLib_DisplayBuf[Y1][X] = ~GuiLib_DisplayBuf[Y1][X];
#endif

        GuiLib_DisplayBuf[Y1][X2b] ^= LinePattern2Right[X2 % 2];
      }

      Y1++;
    }
  }
}

