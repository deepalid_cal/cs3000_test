/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2011 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*                       easyGUI display driver unit                        */
/*                               v6.0.3.001                                 */
/*                                                                          */
/* ************************************************************************ */

#include "GuiConst.h"
#include "GuiDisplay.h"
#include "GuiLib.h"
#include <stdlib.h>

// Select LCD controller type by removing double slashes
//
// Many of the drivers listed here supports more than one display controller.
// To find your driver simply make a text search in this file. Omit eventual
// letters after the display controller type number. Example: For an NJU6450A
// display controller make a search for "NJU6450" - make sure it is a plain
// text search, not a word search.
//
// If your display controller is not found you can contact support at
// sales@ibissolutions.com. We will then quickly supply a driver for it.
//
// You only need to use the relevant display driver, all others may be deleted.
// Make a copy of this file before modifying it.
//
// -----------------------------------------------------------------------------
// #define LCD_CONTROLLER_TYPE_AT32AP7000
// #define LCD_CONTROLLER_TYPE_AT91RM9200_ST7565
// #define LCD_CONTROLLER_TYPE_AT91SAM9263
// #define LCD_CONTROLLER_TYPE_BVGP
// #define LCD_CONTROLLER_TYPE_D51E5TA7601
// #define LCD_CONTROLLER_TYPE_FSA506
// #define LCD_CONTROLLER_TYPE_H8S2378
// #define LCD_CONTROLLER_TYPE_HD61202_2H
// #define LCD_CONTROLLER_TYPE_HD61202_4H
// #define LCD_CONTROLLER_TYPE_HD61202_2H2V
// #define LCD_CONTROLLER_TYPE_HX8238_AT32AP7000
// #define LCD_CONTROLLER_TYPE_HX8238_TMP92CH21
// #define LCD_CONTROLLER_TYPE_HX8312
// #define LCD_CONTROLLER_TYPE_HX8345
// #define LCD_CONTROLLER_TYPE_HX8346
// #define LCD_CONTROLLER_TYPE_HX8347
// #define LCD_CONTROLLER_TYPE_ILI9341
// #define LCD_CONTROLLER_TYPE_LC7981
// #define LCD_CONTROLLER_TYPE_LH155BA
// #define LCD_CONTROLLER_TYPE_LH75401
// #define LCD_CONTROLLER_TYPE_LH7A400
// #define LCD_CONTROLLER_TYPE_LPC2478
// #define LCD_CONTROLLER_TYPE_LPC3230
// #define LCD_CONTROLLER_TYPE_MPC5606S
// #define LCD_CONTROLLER_TYPE_MX257
// #define LCD_CONTROLLER_TYPE_NJU6450A
// #define LCD_CONTROLLER_TYPE_PCF8548
// #define LCD_CONTROLLER_TYPE_PCF8813
// #define LCD_CONTROLLER_TYPE_PIC24FJ256
// #define LCD_CONTROLLER_TYPE_PXA320
// #define LCD_CONTROLLER_TYPE_R61509
// #define LCD_CONTROLLER_TYPE_RA8822
// #define LCD_CONTROLLER_TYPE_S1D13505
// #define LCD_CONTROLLER_TYPE_S1D13506
// #define LCD_CONTROLLER_TYPE_S1D13700
#define LCD_CONTROLLER_TYPE_S1D13705
// #define LCD_CONTROLLER_TYPE_S1D13706
// #define LCD_CONTROLLER_TYPE_S1D13715
// #define LCD_CONTROLLER_TYPE_S1D13743
// #define LCD_CONTROLLER_TYPE_S1D13748
// #define LCD_CONTROLLER_TYPE_S1D13781
// #define LCD_CONTROLLER_TYPE_S1D13A04
// #define LCD_CONTROLLER_TYPE_S1D13A05
// #define LCD_CONTROLLER_TYPE_S1D15721
// #define LCD_CONTROLLER_TYPE_S6B0741
// #define LCD_CONTROLLER_TYPE_S6B33BL
// #define LCD_CONTROLLER_TYPE_S6D0139
// #define LCD_CONTROLLER_TYPE_S6E63D6
// #define LCD_CONTROLLER_TYPE_SBN1661_2H_SBN0080
// #define LCD_CONTROLLER_TYPE_SED1335
// #define LCD_CONTROLLER_TYPE_SEPS525
// #define LCD_CONTROLLER_TYPE_SH1101A
// #define LCD_CONTROLLER_TYPE_SH1123
// #define LCD_CONTROLLER_TYPE_SPFD5408
// #define LCD_CONTROLLER_TYPE_SPLC502
// #define LCD_CONTROLLER_TYPE_SSD0323
// #define LCD_CONTROLLER_TYPE_SSD1289
// #define LCD_CONTROLLER_TYPE_SSD1305
// #define LCD_CONTROLLER_TYPE_SSD1322
// #define LCD_CONTROLLER_TYPE_SSD1339
// #define LCD_CONTROLLER_TYPE_SSD1815
// #define LCD_CONTROLLER_TYPE_SSD1815_2H
// #define LCD_CONTROLLER_TYPE_SSD1848
// #define LCD_CONTROLLER_TYPE_SSD1858
// #define LCD_CONTROLLER_TYPE_SSD1926
// #define LCD_CONTROLLER_TYPE_SSD1963
// #define LCD_CONTROLLER_TYPE_SSD2119
// #define LCD_CONTROLLER_TYPE_ST7529
// #define LCD_CONTROLLER_TYPE_ST7541
// #define LCD_CONTROLLER_TYPE_ST7561
// #define LCD_CONTROLLER_TYPE_ST7565
// #define LCD_CONTROLLER_TYPE_ST7586
// #define LCD_CONTROLLER_TYPE_ST7637
// #define LCD_CONTROLLER_TYPE_ST7715
// #define LCD_CONTROLLER_TYPE_ST7920
// #define LCD_CONTROLLER_TYPE_T6963
// #define LCD_CONTROLLER_TYPE_TLS8201
// #define LCD_CONTROLLER_TYPE_TMPA900
// #define LCD_CONTROLLER_TYPE_TS7390
// #define LCD_CONTROLLER_TYPE_UC1608
// #define LCD_CONTROLLER_TYPE_UC1610
// #define LCD_CONTROLLER_TYPE_UC1611
// #define LCD_CONTROLLER_TYPE_UC1617
// #define LCD_CONTROLLER_TYPE_UC1698
// #define LCD_CONTROLLER_TYPE_UPD161607

// ============================================================================
//
// COMMON PART - NOT DEPENDENT ON DISPLAY CONTROLLER TYPE
//
// Do not delete this part of the GuiDisplay.c file.
//
// If your operating system uses pre-emptive execution, i.e. it interrupts tasks
// at random instances, and transfers control to other tasks, display writing
// must be protected from this, by writing code for the two protection
// functions:
//
//   o  GuiDisplay_Lock     Prevent the OS from switching tasks
//   o  GuiDisplay_Unlock   Open up normal task execution again
//
// If your operating system does not use pre-emptive execution, i.e. if you
// must specifically release control in one tasks in order for other tasks to
// be serviced, or if you don't employ an operating system at all, you can just
// leave the two protection functions empty. Failing to write proper code for
// the protection functions will result in a system with unpredictable
// behavior.
//
// ============================================================================

/**
 * We have established a convention within this project. ALL reads and writes to 
 * the display must be accomplished by using the Display_Processing_Task. This 
 * is our solution to the GUI Library re-entrancy problem. For some reason I 
 * liked that better than the MUTEX approach. Honestly at this writing I can't 
 * remember why. But the display command queue is in place and seems to work 
 * well. 
 * 
 * @author bobd (6/7/2009)
 */
/*
void GuiDisplay_Lock (void)
{
}

void GuiDisplay_Unlock (void)
{
}
*/

// ============================================================================
//
// DISPLAY DRIVER PART
//
// You only need to use the relevant display driver, all others may be deleted.
// Modify the driver so that it fits your specific hardware.
// Make a copy of this file before modifying it.
//
// ============================================================================

#ifdef LCD_CONTROLLER_TYPE_AT32AP7000

// ============================================================================
//
// AT32AP7000 DISPLAY CONTROLLER
//
// This driver uses linux lcdfb driver, included in kernel version 2.6.22 and
// higher.
// Frame buffer support must be enabled in kernel, and correct resolution set.
// LCD display in 24 bit TFT interface.
// Frame memory buffer - 32 bit width, data in 24 bit packet format.
// Big Endian memory organization.
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 8, 16 or 24 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// Hardwired connection for TX14D12V display
// AT32LP7000 - TX14D12V
// LCD_VSYNC  - HSYNC
// LCD_HSYNC  - VSYNC
// LCD_DVAL   - DTMG
// LCD_PCLK   - DCLK

// LCDD[23:16] - B5:B0  blue component,
// LCDD[15:8]  - G5:G0  green component
// LCDD[7:0]   - R5:R0  red component.
// If the LCD Module has lower color resolution, only the most significant bits
// of each component are used.

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/termios.h>
#include <linux/input.h>
#include <linux/fb.h>

// Set start address of frame buffer
#define FRAME_ADDRESS  0x00000000
GuiConst_INT8U *fb0;                // mmap pointer to framebuffer

// Initialize the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT32U i;
  struct fb_var_screeninfo vscreen;   // Framebuffer dimensions

  // Open the framebuffer device using the open() call
  GuiConst_INT32S fd = open("/dev/fb0", O_RDWR);

  // Retrieve some of the framebuffer settings using the ioctl() call
  ioctl(fd, FBIOGET_VSCREENINFO, &vscreen);

  // Set the resolution and color depth (bits per pixel)
  vscreen.bits_per_pixel = GuiConst_COLOR_SIZE;
  vscreen.xres = GuiConst_DISPLAY_WIDTH;
  vscreen.yres = GuiConst_DISPLAY_HEIGHT;

  // Save the framebuffer settings using the ioctl() call
  ioctl(fd, FBIOPUT_VSCREENINFO, &vscreen);

  // Add the missing parameters to mmap
  fb0 = (GuiConst_INT8U *)
     mmap(0, GuiConst_DISPLAY_BYTES,PROT_WRITE, MAP_SHARED, fd, FRAME_ADDRESS);

  if (fb0 == MAP_FAILED)
  {
    perror("mmap fb0");
    exit(3);

    // Close the framebuffer device so other applications can use it
    close(fd);

    // Free mapped region
    munmap(fb0, GuiConst_DISPLAY_BYTES);
  }
  else
  {
    // Clear GuiLib_DisplayBuf by copying the data into frame buffer
    for (i = 0; i < GuiConst_DISPLAY_BYTES; i++)
      fb0[i] = 0x00;   // Color
  }
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT32U i;
  GuiConst_INT16S LineNo, LastByte, N;

  GuiDisplay_Lock ();

  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++) // Loop through lines
  {
    if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
    // Something to redraw on this line
    {
      // Calculate address in frame buffer for 8bit pointer
      i = LineNo * GuiConst_BYTES_PR_LINE +
             GuiLib_DisplayRepaint[LineNo].ByteBegin*GuiConst_COLOR_BYTE_SIZE;

      // Calculate last byte
      LastByte = GuiConst_BYTES_PR_LINE/GuiConst_COLOR_BYTE_SIZE - 1;
      if (GuiLib_DisplayRepaint[LineNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[LineNo].ByteEnd;

      // Write display data
      for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin * GuiConst_COLOR_BYTE_SIZE;
           N <= LastByte*GuiConst_COLOR_BYTE_SIZE;
           N++)
        fb0[i++] = GuiLib_DisplayBuf[LineNo][N];

      // Reset repaint parameters
      GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_AT32AP7000

#ifdef LCD_CONTROLLER_TYPE_AT91RM9200_ST7565

// ============================================================================
//
// AT91RM9200 + ST7565 DISPLAY CONTROLLER linux version
//
// This driver uses one ST7565 display controllers with one microcontroller
// AT91RM9200.
// LCD display in 8 bit parallel interface.
// Graphic modes up to 128x65 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: As you like
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// Add these linux header files at the beginning of GuiLib.h
//#include <stdio.h>
//#include <stdarg.h>
//#include <memory.h>
//#include <errno.h>
//#include <sys/types.h>
//#include <sys/socket.h>
//#include <sys/mman.h>
//#include <sys/ioctl.h>
//#include <fcntl.h>
//#include <signal.h>
//#include <unistd.h>

// Memory mapping definitions
#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)

// AT91RM9200 PIO definitions
#define AT91_SYS  0xFFFFF000
#define AT91_PIOA 0xFFFFF400
#define AT91_PIOB 0xFFFFF600
#define AT91_PIOC 0xFFFFF800
#define AT91_PIOD 0xFFFFFA00

#define PMC_PCER  0x0010

#define PIO_PER   0x0000
#define PIO_PDR   0x0004
#define PIO_OER   0x0010
#define PIO_ODR   0x0014
#define PIO_SODR  0x0030
#define PIO_CODR  0x0034
#define PIO_PDSR  0x003c
#define PIO_IDR   0x0044  // Interrupt Disable Register
#define PIO_PUDR  0x0060  // Pull-up Disable Register
#define PIO_PUER  0x0064  // Pull-up Enable Register
#define PIO_MDDR  0x0054  // Multi-driver Disable Register

#define rsCOMMAND 0
#define rsDATA    1

#define PIOA_OFFSET  0x400
#define PIOB_OFFSET  0x600
#define PIOC_OFFSET  0x800
#define PIOD_OFFSET  0xa00

#define PIOA_ID  2
#define PIOB_ID  3
#define PIOC_ID  4
#define PIOD_ID  5

#define PMC_OFFSET 0xc00

// set according your hardware
#define CS_PIN   1<<8
#define RS_PIN   1<<9
#define WR_PIN   1<<10
#define RD_PIN   1<<11
#define FIRS_PIN 1<<12
#define PS_PIN   1<<13

GuiConst_INT32S gpio_fd;
void *map_base;

// Set according your hardware
GuiConst_INT32S dest_addr = PIOD_OFFSET;   // PORT D
GuiConst_INT32S id = PIOD_ID;              // PORT D

GuiConst_INT32U ControlPins;

unsigned long readval, writeval;

// Sets the pins in the mask
void High(GuiConst_INT32U mask)
{
  *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_SODR) & MAP_MASK))) = mask;
}

// Clears the pins in the mask
void Low(GuiConst_INT32U mask)
{
  *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_CODR) & MAP_MASK))) = mask;
}

GuiConst_INT32S LcdLowLevelInit(void)
{
  if((gpio_fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1)
  {
    printf("gpio: Error opening /dev/mem\n");
    return -1;
  }

  printf("mapping one page\n");

  // Map one page
  map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, gpio_fd, AT91_SYS & ~MAP_MASK);
  if(map_base == (void *) -1)
  {
    printf("gpio: Error mapping memory\n");
    close(gpio_fd);
    return -1;
  }

  ControlPins = RS_PIN | WR_PIN | RD_PIN | FIRS_PIN | PS_PIN | CS_PIN;

  printf("setting control pins\n");

  *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_PER) & MAP_MASK))) = ControlPins;
  *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_OER) & MAP_MASK))) = ControlPins;

  High(WR_PIN);
  High(RD_PIN);

  // 8080 MPU Interface
  Low(FIRS_PIN);

  // Parallel data input
  High(PS_PIN);

  // Chip select low for active
  Low(FIRS_PIN);

  // Databus as input with pull-up
  *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_ODR) & MAP_MASK))) = 0xff;
  *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_PER) & MAP_MASK))) = 0xff;
  *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_PUER) & MAP_MASK))) = 0xff;
  *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_IDR) & MAP_MASK))) = 0xff;

  printf("disabling multidrive\n");

  // Disable multidrive
  *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_MDDR) & MAP_MASK))) = 0xFF | ControlPins;

  printf("enabling pio clock\n");

  // First, enable PIO clock
  writeval = 1 << id;
  *((GuiConst_INT32U *)(map_base + ((PMC_OFFSET + PMC_PCER) & MAP_MASK))) = writeval;

  printf("initialization finished\n");

  return 0;
}

GuiConst_INT32S LcdRelease(void)
{
  if (munmap(map_base, MAP_SIZE) == -1)
  {
    printf("Error unmapping memory\n");
    close(gpio_fd);
    return -1;
  }
  close(gpio_fd);
    return 0;
}

void LcdWrite(GuiConst_INT8U rs, GuiConst_INT8U dataBus)
{
  // Set RS according to parameter
  if(rs == rsDATA)
    High(RS_PIN);
  else
    Low(RS_PIN);

  // Set data on databus
  High(dataBus);
  Low((~dataBus)&0xff);

  // Enable databus output
  *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_OER) & MAP_MASK))) = 0xFF;

  Low(WR_PIN);
  High(WR_PIN);

  // Disable databus output
  *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_ODR) & MAP_MASK))) = 0xFF;
}

GuiConst_INT8U LcdRead(GuiConst_INT8U rs)
{
  GuiConst_INT32S retval;
  GuiConst_INT32U readval;

  // Set RS according to parameter
  if(rs == rsDATA)
    High(RS_PIN);
  else
    Low(RS_PIN);

  Low(RD_PIN);

  // Then read port value
  readval = *((GuiConst_INT32U *)(map_base + ((dest_addr + PIO_PDSR) & MAP_MASK)));
  retval = (readval) & 0xff;

  High(RD_PIN);
  return retval;
}

// High level functions
//---------------------------------------------------------------------------------
#define sgwrby(a,d)      LcdWrite((a),(d))
#define sgrdby(a)        LcdRead((a))

#define GHWWR            1
#define GHWRD            1
#define GHWSTA           0
#define GHWCMD           0

// Controller status bits
#define GSTA_BUSY        0x80
#define GSTA_ADC         0x40
#define GSTA_OFF         0x20

#define GSTA_RESET       0x10

// Controller commands ( 1 byte )
#define GCTRL_OFF        0xAE // Display off
#define GCTRL_ON         0xAF // Display on
#define GCTRL_XADR_H     0x10 // Set X adr | (132-0) MSB
#define GCTRL_XADR_L     0x00 // Set X adr | (132-0) LSB

#define GCTRL_YADR       0xB0 // Set Y adr | (15-0)
#define GCTRL_DSTART     0x40 // Set Initial display line (0)

#define GCTRL_RESET      0xE2 // Reset controller

#define GCTRL_SHL        0xC0 // Norm/reverse SHL | (0x00 or 0x08)
#define GCTRL_NORM_REV   0xA6 // Normal/Reverse display | (0-1)

#define GCTRL_POWER      0x28 // Power-Ctrl VC,VR,VF | (0-7)
#define GCTRL_REG_RES    0x20 // Regulator resistor ration select | (0-7)

#define GCTRL_ADC        0xA0 // Set Alternative DireCtion | (0-1)
#define GCTRL_E_ON       0xA4 // Normal display / Entire display on  | (0-1)

#define GCTRL_REF_VOLT   0x81 // Set reference voltage, next byte = (0-63)
#define GCTRL_LCD_BIAS   0xA2 // Set bias | (2-3)

// Controller commands (2 byte)
#define GCTRL_INDICATOR  0xAC  // Set static indicator mode (0=off,1=On)
                               // next byte : (0=Off 1,2= Blinking 3=On)

// Chip wait for not busy
// Send a command to controller + data ready
void ghw_wait(void)
{
  if ((sgrdby(GHWSTA) & (GSTA_BUSY | GSTA_RESET)) != 0)
  {
    GuiConst_INT8U timeout = 127;
    do
    {
      if(--timeout == 0)
        return 1;
    }while((sgrdby(GHWSTA) & (GSTA_BUSY | GSTA_RESET)) != 0);
  }
}

// Send a command to controller + data ready
void ghw_cmd( GuiConst_INT8U cmd )
{
  ghw_wait();
  sgwrby(GHWCMD, cmd);
}

// Send a double byte command to controller + data ready
void ghw_dcmd( GuiConst_INT8U cmd1, GuiConst_INT8U cmd2 )
{
  ghw_wait();
  sgwrby(GHWCMD, cmd1);
  ghw_wait();
  sgwrby(GHWCMD, cmd2);
}

// Wait here for LCD power to stabilize
// If this condition is violated then the rest of the initialization may fail.
// The required time depends on the actual display and power circuit.
// Here multiple writes is used as delay.

void ghw_cmdw( GuiConst_INT8U cmd )
{
  GuiConst_INT32U tmp;

  for (tmp = 0; tmp < 255; tmp++)
    ghw_cmd( cmd );
}

// Data write
void lcdDataByte( GuiConst_INT8U val )
{
  ghw_wait();
  sgwrby(GHWWR,val);                    // Write data
}

// Set page Wait for controller + data ready
void lcdSelectPage(GuiConst_INT8U y)
{
  y = (y & 0xF) | GCTRL_YADR;
  ghw_wait();
  sgwrby(GHWCMD,y);                     // Set Y koordinate
}

// Set column Wait for controller + data ready
void lcdSelectColumn(GuiConst_INT8U x)
{
  ghw_wait();
  sgwrby(GHWCMD,((x>>4) & 0xF) | GCTRL_XADR_H);
  ghw_wait();
  sgwrby(GHWCMD,(x & 0xF) | GCTRL_XADR_L);
}

// Set contrast (Normalized value range [0 : 99] )
void ghw_cont_set(GuiConst_INT8U contrast)
{
  ghw_dcmd(GCTRL_REF_VOLT , (GuiConst_INT8U)((((GuiConst_INT32U) contrast) *16) / 25));
}

// Initialize display, clear ram
void GuiDisplay_Init(void)
{
  GuiConst_INT32U delay;

  LcdLowLevelInit();

  sgwrby(GHWCMD, GCTRL_RESET);   // soft reset

  if ((sgrdby(GHWSTA) & (GSTA_BUSY | GSTA_RESET)) != 0)
  {
    GuiConst_INT8U timeout = 127;
    do
    {
      if(--timeout == 0)
        return 1;
    }while((sgrdby(GHWSTA) & (GSTA_BUSY | GSTA_RESET)) != 0);
  }

  ghw_cmd(GCTRL_DSTART);         // LCD start line = 0
  ghw_cmd(GCTRL_LCD_BIAS | 3);   // LCD bias ratio | (2-3)
  ghw_cmd(GCTRL_NORM_REV | 0);   // True / inverse display | (0-1)
  ghw_cmd(GCTRL_REG_RES  | 0x5); // Regulator resistor ratio select | (0-7)
  ghw_cont_set(45);              // Set default contrast level (0-99)
  ghw_cmdw(GCTRL_POWER | 0x4);   // Turn stepup generator on
  ghw_cmdw(GCTRL_POWER | 0x6);   // Turn resistor ladder on
  ghw_cmdw(GCTRL_POWER | 0x7);   // Turn buffers on
  ghw_cmd(GCTRL_SHL | 0);        // Normal horizontal direction
  ghw_cmd(GCTRL_ADC | 0);        // Normal vertical direction
  ghw_cmd(GCTRL_E_ON | 0);       // Clear entire display On, (0 = normal, 1 = on)
  ghw_cmd(GCTRL_ON);             // Display On
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT32S PageNo;
  GuiConst_INT32S LastByte;
  GuiConst_INT32S N;

  // Lock GUI ressources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      lcdSelectPage(PageNo);

      // Select column
      lcdSelectColumn(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);
      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI ressources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_AT91RM9200_ST7565

#ifdef LCD_CONTROLLER_TYPE_AT91SAM9263
// ============================================================================
//
// AT91SAM9263 MICROCONTROLLER WITH BUILT-IN DISPLAY CONTROLLER
//
// This driver uses the internal lcd controller of AT91SAM9263 microcontroller.
// LCD driver of AT91SAM9263 in 24 bit RGB parallel interface for TFT type
// Graphic modes up to 640x480 pixels.
//
// Compatible display controllers:
//   AT91SAM9261
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 24 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Uncomment for AT91SAM9261
// #define AT91SAM9261

// Hardware connection for TFT display
// AT91SAM9263         - generic TFT display
// LCDVSYNC            - VSYNC
// LCDHSYNC            - HSYNC
// LCDDOTCK            - Pixel CLK
// LCDDEN              - DATA_ENABLE
// LCDVD[0]-LCDVD[7]   - R0-R7
// LCDVD[8]-LCDVD[15]  - G0-G7
// LCDVD[16]-LCDVD[23] - B0-B7

// Adjust master clock frequency
#define MASTER_CLOCK           100000000

// Adjust start address of framebuffer in SDRAM memory
// Must be word aligned
#define SDRAM_BUFFER           0x20000000

// Adjust according your display type
#define VPW                    1  // Vertical Pulse Width
#define VBP                    4  // Vertical Back Porch
#define VFP                    2  // Vertical Front Porch
#define VHDLY                  0
#define HPW                    5  // Horizontal Pulse Width
#define HBP                    17 // Horizontal Back Porch
#define HFP                    11 // Horizontal Front Porch

#define HORIZONTAL_SIZE        GuiConst_DISPLAY_WIDTH_HW
#define VERTICAL_SIZE          GuiConst_DISPLAY_HEIGHT_HW
#define FRAMERATE              60 // In Hz
#define BPP                    24 // Bits per pixel
#define IFWIDTH                24 // Display interface
#define BURSTLENGTH            4

// Control bits definition
#define LCDC_CLKVAL                (0x1FF << 12)  // 9-bit Divider for pixel clock freq
#define LCDC_DISTYPE_STNMONO        0             // STN Mono
#define LCDC_DISTYPE_STNCOLOR       1             // STN Color
#define LCDC_DISTYPE_TFT            2             // TFT
#define LCDC_SCANMOD_SINGLESCAN     (0 <<  2)     // Single Scan
#define LCDC_SCANMOD_DUALSCAN       (1 <<  2)     // Dual Scan
#define LCDC_ONEBITSPERPIXEL        (0 <<  5)     // 1 Bits
#define LCDC_TWOBITSPERPIXEL        (1 <<  5)     // 2 Bits
#define LCDC_FOURBITSPERPIXEL       (2 <<  5)     // 4 Bits
#define LCDC_EIGTHBITSPERPIXEL      (3 <<  5)     // 8 Bits
#define LCDC_SIXTEENBITSPERPIXEL    (4 <<  5)     // 16 Bits
#define LCDC_TWENTYFOURBITSPERPIXEL (5 <<  5)     // 24 Bits
#define LCDC_INVVD_NORMALPOL        (0 <<  8)     // Normal Polarity
#define LCDC_INVVD_INVERTEDPOL      (1 <<  8)     // Inverted Polarity
#define LCDC_INVFRAME_NORMALPOL     (0 <<  9)     // Normal Polarity
#define LCDC_INVFRAME_INVERTEDPOL   (1 <<  9)     // Inverted Polarity
#define LCDC_INVLINE_NORMALPOL      (0 << 10)     // Normal Polarity
#define LCDC_INVLINE_INVERTEDPOL    (1 << 10)     // Inverted Polarity
#define LCDC_INVCLK_NORMALPOL       (0 << 11)     // Normal Polarity
#define LCDC_INVCLK_INVERTEDPOL     (1 << 11)     // Inverted Polarity
#define LCDC_INVDVAL_NORMALPOL      (0 << 12)     // Normal Polarity
#define LCDC_INVDVAL_INVERTEDPOL    (1 << 12)     // Inverted Polarity
#define LCDC_CLKMOD_ACTIVEONLYDISP  (0 << 15)     // Active during display period
#define LCDC_CLKMOD_ALWAYSACTIVE    (1 << 15)     // Always Active
#define LCDC_MEMOR_BIGIND           (0 << 31)     // Big Endian
#define LCDC_MEMOR_LITTLEIND        (1 << 31)     // Little Endian
#define LCDC_LINEVAL                (0x7FF <<  0) // Vertical Size of LCD Module
#define LCDC_HOZVAL                 (0x7FF << 21) // Horizontal Size of LCD Module
#define LCDC_VFP                    (0xFF <<  0)  // Vertical Front Porch
#define LCDC_VBP                    (0xFF <<  8)  // Vertical Back Porch
#define LCDC_VPW                    (0x3F << 16)  // Vertical Sync. Pulse Width
#define LCDC_VHDLY                  (0xF << 24)   // Vertical to Horizontal Delay
#define LCDC_HBP                    (0xFF <<  0)  // Horizontal Back Porch
#define LCDC_HPW                    (0x3F <<  8)  // Horizontal Sync. Pulse Width
#define LCDC_HFP                    (0x3FF << 22) // Horizontal Front Porch
#define LCDC_PS_NOTDIVIDED          0             // System Freq.
#define LCDC_PS_DIVIDEDBYTWO        1             // System Freq divided by 2
#define LCDC_PS_DIVIDEDBYFOUR       2             // System Freq divided by 4
#define LCDC_PS_DIVIDEDBYEIGHT      3             // System Freq divided by 8
#define LCDC_POL_NEGATIVEPULSE      (0 <<  2)     // Negative Pulse
#define LCDC_POL_POSITIVEPULSE      (1 <<  2)     // Positive Pulse
#define LCDC_ENA_PWMGEMDISABLED     (0 <<  3)     // PWM Generator Disabled
#define LCDC_ENA_PWMGEMENABLED      (1 <<  3)     // PWM Generator Enabled
#define LCDC_DMAEN                  (1 <<  0)     // DAM Enable
#define LCDC_PWR                    (1 <<  0)     // LCD Module Power Control

// Parallel Input Output Controller registers
typedef struct _AT91S_PIO {
  volatile GuiConst_INT32U PIO_PER;      // PIO Enable Register
  volatile GuiConst_INT32U PIO_PDR;      // PIO Disable Register
  volatile GuiConst_INT32U PIO_PSR;      // PIO Status Register
  volatile GuiConst_INT32U Reserved0[1]; //
  volatile GuiConst_INT32U PIO_OER;      // Output Enable Register
  volatile GuiConst_INT32U PIO_ODR;      // Output Disable Registerr
  volatile GuiConst_INT32U PIO_OSR;      // Output Status Register
  volatile GuiConst_INT32U Reserved1[1];
  volatile GuiConst_INT32U PIO_IFER;     // Input Filter Enable Register
  volatile GuiConst_INT32U PIO_IFDR;     // Input Filter Disable Register
  volatile GuiConst_INT32U PIO_IFSR;     // Input Filter Status Register
  volatile GuiConst_INT32U Reserved2[1];
  volatile GuiConst_INT32U PIO_SODR;     // Set Output Data Register
  volatile GuiConst_INT32U PIO_CODR;     // Clear Output Data Register
  volatile GuiConst_INT32U PIO_ODSR;     // Output Data Status Register
  volatile GuiConst_INT32U PIO_PDSR;     // Pin Data Status Register
  volatile GuiConst_INT32U PIO_IER;      // Interrupt Enable Register
  volatile GuiConst_INT32U PIO_IDR;      // Interrupt Disable Register
  volatile GuiConst_INT32U PIO_IMR;      // Interrupt Mask Register
  volatile GuiConst_INT32U PIO_ISR;      // Interrupt Status Register
  volatile GuiConst_INT32U PIO_MDER;     // Multi-driver Enable Register
  volatile GuiConst_INT32U PIO_MDDR;     // Multi-driver Disable Register
  volatile GuiConst_INT32U PIO_MDSR;     // Multi-driver Status Register
  volatile GuiConst_INT32U Reserved3[1];
  volatile GuiConst_INT32U PIO_PPUDR;    // Pull-up Disable Register
  volatile GuiConst_INT32U PIO_PPUER;    // Pull-up Enable Register
  volatile GuiConst_INT32U PIO_PPUSR;    // Pull-up Status Register
  volatile GuiConst_INT32U Reserved4[1];
  volatile GuiConst_INT32U PIO_ASR;      // Select A Register
  volatile GuiConst_INT32U PIO_BSR;      // Select B Register
  volatile GuiConst_INT32U PIO_ABSR;     // AB Select Status Register
  volatile GuiConst_INT32U Reserved5[9];
  volatile GuiConst_INT32U PIO_OWER;     // Output Write Enable Register
  volatile GuiConst_INT32U PIO_OWDR;     // Output Write Disable Register
  volatile GuiConst_INT32U PIO_OWSR;     // Output Write Status Register
} AT91S_PIO, *AT91PS_PIO;

// LCD Controller registers definition
typedef struct _AT91S_LCDC {
  volatile GuiConst_INT32U LCDC_BA1;     // DMA Base Address Register 1
  volatile GuiConst_INT32U LCDC_BA2;     // DMA Base Address Register 2
  volatile GuiConst_INT32U LCDC_FRMP1;   // DMA Frame Pointer Register 1
  volatile GuiConst_INT32U LCDC_FRMP2;   // DMA Frame Pointer Register 2
  volatile GuiConst_INT32U LCDC_FRMA1;   // DMA Frame Address Register 1
  volatile GuiConst_INT32U LCDC_FRMA2;   // DMA Frame Address Register 2
  volatile GuiConst_INT32U LCDC_FRMCFG;  // DMA Frame Configuration Register
  volatile GuiConst_INT32U LCDC_DMACON;  // DMA Control Register
  volatile GuiConst_INT32U LCDC_DMA2DCFG;// DMA 2D addressing configuration
  volatile GuiConst_INT32U Reserved0[503];
  volatile GuiConst_INT32U LCDC_LCDCON1; // LCD Control 1 Register
  volatile GuiConst_INT32U LCDC_LCDCON2; // LCD Control 2 Register
  volatile GuiConst_INT32U LCDC_TIM1;    // LCD Timing Config 1 Register
  volatile GuiConst_INT32U LCDC_TIM2;    // LCD Timing Config 2 Register
  volatile GuiConst_INT32U LCDC_LCDFRCFG;// LCD Frame Config Register
  volatile GuiConst_INT32U LCDC_FIFO;    // LCD FIFO Register
  volatile GuiConst_INT32U LCDC_MVAL;    // LCD Mode Toggle Rate Value Register
  volatile GuiConst_INT32U LCDC_DP1_2;   // Dithering Pattern DP1_2 Register
  volatile GuiConst_INT32U LCDC_DP4_7;   // Dithering Pattern DP4_7 Register
  volatile GuiConst_INT32U LCDC_DP3_5;   // Dithering Pattern DP3_5 Register
  volatile GuiConst_INT32U LCDC_DP2_3;   // Dithering Pattern DP2_3 Register
  volatile GuiConst_INT32U LCDC_DP5_7;   // Dithering Pattern DP5_7 Register
  volatile GuiConst_INT32U LCDC_DP3_4;   // Dithering Pattern DP3_4 Register
  volatile GuiConst_INT32U LCDC_DP4_5;   // Dithering Pattern DP4_5 Register
  volatile GuiConst_INT32U LCDC_DP6_7;   // Dithering Pattern DP6_7 Register
  volatile GuiConst_INT32U LCDC_PWRCON;  // Power Control Register
  volatile GuiConst_INT32U LCDC_CTRSTCON;// Contrast Control Register
  volatile GuiConst_INT32U LCDC_CTRSTVAL;// Contrast Value Register
  volatile GuiConst_INT32U LCDC_IER;     // Interrupt Enable Register
  volatile GuiConst_INT32U LCDC_IDR;     // Interrupt Disable Register
  volatile GuiConst_INT32U LCDC_IMR;     // Interrupt Mask Register
  volatile GuiConst_INT32U LCDC_ISR;     // Interrupt Enable Register
  volatile GuiConst_INT32U LCDC_ICR;     // Interrupt Clear Register
  volatile GuiConst_INT32U LCDC_GPR;     // General Purpose Register
  volatile GuiConst_INT32U LCDC_ITR;     // Interrupts Test Register
  volatile GuiConst_INT32U LCDC_IRR;     // Interrupts Raw Status Register
  volatile GuiConst_INT32U Reserved1[230];
  volatile GuiConst_INT32U LCDC_LUT_ENTRY[256]; // LUT Entries Register
} AT91S_LCDC, *AT91PS_LCDC;

// Power controll register
typedef struct _AT91S_PMC {
  volatile GuiConst_INT32U PMC_SCER;     // System Clock Enable Register
  volatile GuiConst_INT32U PMC_SCDR;     // System Clock Disable Register
  volatile GuiConst_INT32U PMC_SCSR;     // System Clock Status Register
  volatile GuiConst_INT32U Reserved0[1];
  volatile GuiConst_INT32U PMC_PCER;     // Peripheral Clock Enable Register
  volatile GuiConst_INT32U PMC_PCDR;     // Peripheral Clock Disable Register
  volatile GuiConst_INT32U PMC_PCSR;     // Peripheral Clock Status Register
  volatile GuiConst_INT32U Reserved1[1];
  volatile GuiConst_INT32U PMC_MOR;      // Main Oscillator Register
  volatile GuiConst_INT32U PMC_MCFR;     // Main Clock  Frequency Register
  volatile GuiConst_INT32U PMC_PLLAR;    // PLL A Register
  volatile GuiConst_INT32U PMC_PLLBR;    // PLL B Register
  volatile GuiConst_INT32U PMC_MCKR;     // Master Clock Register
  volatile GuiConst_INT32U Reserved2[3];
  volatile GuiConst_INT32U PMC_PCKR[8];  // Programmable Clock Register
  volatile GuiConst_INT32U PMC_IER;      // Interrupt Enable Register
  volatile GuiConst_INT32U PMC_IDR;      // Interrupt Disable Register
  volatile GuiConst_INT32U PMC_SR;       // Status Register
  volatile GuiConst_INT32U PMC_IMR;      // Interrupt Mask Register
} AT91S_PMC, *AT91PS_PMC;

// Base address definitions
#ifdef AT91SAM9261
#define AT91C_BASE_PIOC ((AT91PS_PIO) 0xFFFFF800) // (PIOC) Base Address
#define AT91C_BASE_LCDC ((AT91PS_LCDC) 0x00600000)// (LCDC) Base Address
#else
#define AT91C_BASE_PIOC ((AT91PS_PIO) 0xFFFFF600) // (PIOC) Base Address
#define AT91C_BASE_LCDC ((AT91PS_LCDC) 0x00700000)// (LCDC) Base Address
#endif
#define AT91C_BASE_PMC  (AT91PS_PMC) 0xFFFFFC00)  // (PMC) Base Address


// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT32U i, clkval;
  GuiConst_INT32U dotClockFrequency;
  GuiConst_INT8U * framePtr;

  // ====================================
  // Initialize Clock and SDRAM!
  // ====================================

  // Set pointers to register's structures
  AT91PS_PIO  pPIOC = AT91C_BASE_PIOC;
  AT91PS_LCDC pLcdc = AT91C_BASE_LCDC;
  AT91PS_PMC  pPMC  = AT91C_BASE_PMC;

  // Set pointer to frame buffer address
  framePtr =(GuiConst_INT8U *)SDRAM_BUFFER;

  // Enable LCD clock in PMC
  pPMC->PMC_PCER |= 1 << 26;

  // Configure the PIOs for lcd interface
  pPIOC->PIO_PDR |= 0x0FFFFFFF;

  // Turn off LCD Controller (Power and DMA)
  pLcdc->LCDC_DMACON = 0;
  pLcdc->LCDC_PWRCON = 0;

  // Calculate lcd clock
  dotClockFrequency =
     (HORIZONTAL_SIZE * VERTICAL_SIZE * FRAMERATE * BPP ) / IFWIDTH ;
  clkval = (MASTER_CLOCK / (2 * dotClockFrequency)) - 1;

  // LCDCON1
  pLcdc->LCDC_LCDCON1 = (clkval<<12) & LCDC_CLKVAL;

  // LCDCON2
  pLcdc->LCDC_LCDCON2 = LCDC_DISTYPE_TFT | LCDC_SCANMOD_SINGLESCAN |
                        LCDC_TWENTYFOURBITSPERPIXEL | LCDC_INVVD_NORMALPOL |
                        LCDC_INVFRAME_NORMALPOL | LCDC_INVLINE_NORMALPOL |
                        LCDC_INVCLK_NORMALPOL | LCDC_INVDVAL_NORMALPOL |
                        LCDC_CLKMOD_ALWAYSACTIVE | LCDC_MEMOR_LITTLEIND;

  // LCDFRCFG
  pLcdc->LCDC_LCDFRCFG = ((VERTICAL_SIZE - 1) & LCDC_LINEVAL) |
                         (((HORIZONTAL_SIZE - 1) << 21) & LCDC_HOZVAL);

  // Front and back porch
  pLcdc->LCDC_TIM1 = (VFP & LCDC_VFP) | ((VBP << 8) & LCDC_VBP) |
                     ((VPW << 16) & LCDC_VPW) | ((VHDLY << 24) & LCDC_VHDLY);

  pLcdc->LCDC_TIM2 = (HBP & LCDC_HBP) | ((HPW << 8) & LCDC_HPW) |
                     ((HFP << 22) & LCDC_HFP);

  // Interrupts disabled
  pLcdc->LCDC_IDR = 0xFFFFFFFF;

  // Contrast
  pLcdc->LCDC_CTRSTVAL = 0xF0;
  pLcdc->LCDC_CTRSTCON = LCDC_PS_DIVIDEDBYEIGHT | LCDC_POL_POSITIVEPULSE |
                         LCDC_ENA_PWMGEMENABLED;

  // DMA base address
  pLcdc->LCDC_BA1 = (GuiConst_INT32U)SDRAM_BUFFER;  //Frame Buffer in SDRAM

  // DMA frame configuration
  pLcdc->LCDC_FRMCFG = ((VERTICAL_SIZE * HORIZONTAL_SIZE * BPP) / 32) |
                       ((BURSTLENGTH-1) << 24);

  // FIFO Configuration
  pLcdc->LCDC_FIFO = 2048 - (2*BURSTLENGTH + 3);

  // DMA Enable
  pLcdc->LCDC_DMACON = LCDC_DMAEN;

  // LCD Enable
  pLcdc->LCDC_PWRCON = LCDC_PWR | 0x0c;

  // Clear frame buffer by copying the data into frame buffer
  for (i = 0; i < GuiConst_DISPLAY_BYTES; i++)
    *framePtr++ = 0x00;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT32U X, Y, LastByte;
  GuiConst_INT8U *framePtr;
  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // set frame buffer base address
      framePtr = (GuiConst_INT8U *)SDRAM_BUFFER;
      // Pointer offset calculation
      framePtr += Y * GuiConst_BYTES_PR_LINE +
                  GuiLib_DisplayRepaint[Y].ByteBegin * 3;
      // Set amount of data to be changed
      LastByte = GuiConst_BYTES_PR_LINE/3 - 1;

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write data for one line to framebuffer from GuiLib_DisplayBuf
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin * 3; X <= LastByte * 3; X++)
        *framePtr++= GuiLib_DisplayBuf[Y][X];

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_AT91SAM9263

#ifdef LCD_CONTROLLER_TYPE_BVGP

// ============================================================================
//
// BVGP DISPLAY CONTROLLER
//
// This driver uses one BVGP display controller.
// BINem LCD display 16 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 232x312 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Controller port pins definition
#define BUS_DATA                  PX
#define BUS_MODE_0_UP             PY.0 = 1
#define BUS_MODE_0_DOWN           PY.0 = 0
#define BUS_MODE_1_UP             PY.1 = 1
#define BUS_MODE_1_DOWN           PY.1 = 0

#define CHIP_ENABLE               PY.2 = 0     // CS pin
#define CHIP_DISABLE              PY.2 = 1     // CS pin
#define CHIP_RESET                PY.3 = 1     // RES pin
#define CHIP_RUN                  PY.3 = 0     // RES pin
#define WRITE_ACTIVE              PY.4 = 0     // WR pin
#define WRITE_PASIVE              PY.4 = 1     // WR pin
#define READ_ACTIVE               PY.5 = 0     // RD pin
#define READ_PASIVE               PY.5 = 1     // RD pin
#define BUSY                      PY.6         // BUSY/READY pin

// Commands
#define CMD_SET_CONFIG_REGISTER   0x00A3
#define CMD_SET_CURSOR_PAGE_0     0x00B1
#define CMD_SET_CURSOR_PAGE_1     0x01B1
#define CMD_SEND_BITMAP           0x00E1
#define CMD_CLEAR_SCREEN          0x01A9
#define CMD_REFRESH_DISPLAY       0x04A9
#define CMD_REFRESH_PAGE          0x14A9
#define CMD_POWER_OFF             0x00F2

// This constants must be altered according to speed of your �-controller
#define DELAY_100MS               1000
#define DELAY_100uS               1

void microsecond100_delay(GuiConst_INT32U cycles)
{
  GuiConst_INT32U i;

  for (i = 0; i < cycles; i++);
}

// Command writing
void Write(GuiConst_INT16U Value)
{
  while(BUSY);
  CHIP_ENABLE;
  BUS_DATA = Value;
  WRITE_ACTIVE;
  microsecond100_delay(DELAY_100uS);
  WRITE_PASIVE;
  microsecond100_delay(DELAY_100uS);
  CHIP_DISABLE;
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  // Set 16 bit data bus mode
  BUS_MODE_0_UP;
  BUS_MODE_1_UP;

  // initialize pins
  WRITE_PASIVE;
  READ_PASIVE;
  CHIP_DISABLE;
  CHIP_RUN;

  // Reset controller
  CHIP_RESET;
  microsecond100_delay(DELAY_100MS);
  CHIP_RUN;

  // Set configuration register to blind mode
  // and data alignment mode to byte alignment
  Write(CMD_SET_CONFIG_REGISTER);
  Write(0x0001);

  // Clear full image memory
  Write(CMD_CLEAR_SCREEN);
}

// Refresh display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S N, LastByte;
  GuiConst_INT16U Y, WordData;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
      // Something to redraw on this page
    {
      // Select page
      Write(CMD_SET_CURSOR_PAGE_0);
      // Select column
      Write((GuiConst_INT16U)(GuiLib_DisplayRepaint[Y].ByteBegin * 8));
      // Select row
      Write(Y);

      // Calculate the last byte
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;
      // Write command send data to image memory
      Write(CMD_SEND_BITMAP);
      // Send number of bytes
      Write((GuiConst_INT16U)
         (LastByte + 1 - GuiLib_DisplayRepaint[Y].ByteBegin));
      // Send image data
      for (N = GuiLib_DisplayRepaint[Y].ByteBegin; N <= LastByte; N += 2)
      {
        // align data to word
        if (N==LastByte)
          WordData = 0;
        else
          WordData = (GuiConst_INT16U)GuiLib_DisplayBuf[Y][N+1];
        WordData = WordData << 8;
        WordData |= (GuiConst_INT16U)GuiLib_DisplayBuf[Y][N];
        Write(WordData);
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Refresh page 0
  Write(CMD_REFRESH_PAGE);
  Write(0);

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_BVGP

#ifdef LCD_CONTROLLER_TYPE_D51E5TA7601
// ============================================================================
//
// D51E5TA7601 DISPLAY CONTROLLER
//
// This driver uses one D51E5TA7601 display controller with on-chip oscillator.
// LCD display in 16 bit 68-system parallel interface or 16 bit SPI interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x480 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16,18 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Function Spi_SendData must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Uncomment for 16 bit SPI interface
//#define SPI_INTERFACE

// Hardwired connection
// IM[3:0]
// 0 0 0 0 M68,16-bit bus interface
// 1 0 0 0 M68,18-bit bus interface
// 0 1 0 0 16 bit serial stream, IM0 is used as ID bit
// SPI3 = 1, 3 wire SPI interface
// In SPI interface connect RS, E_WRB and RWB_RDB connect to 0 or 1

#define DISP_DATA            Pxx           // for 16 bit Pins DB[17:10],DB[8:1],
                                           // for 18 bit Pins DB[17:0]
#define RESET_ON             Pxx.x = 0     // RSTB pin
#define RESET_OFF            Pxx.x = 1     // RSTB pin
#define CHIP_SELECT          Pxx.x = 0     // CSB pin
#define CHIP_DESELECT        Pxx.x = 1     // CSB pin
#define MODE_DATA            Pxx.x = 1     // RS pin
#define MODE_COMMAND         Pxx.x = 0     // RS pin
#define ENABLE_HIGH          Pxx.x = 1     // E_WRB pin
#define ENABLE_LOW           Pxx.x = 0     // E_WRB pin
#define MODE_WRITE           Pxx.x = 0     // RWB_RDB pin
#define MODE_READ            Pxx.x = 1     // RWB_RDB pin

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

#ifdef GuiConst_COLOR_DEPTH_18
// Command writing in 18 bit parallel interface
void LCD_Cmd(GuiConst_INT32U Val)
{
  GuiConst_INT32U temp;

  MODE_COMMAND;
  MODE_WRITE;
  temp = (Val & 0x0000FF00)<<2;
  temp |= (Val & 0x000000FF)<<1;
  DISP_DATA = temp;
  CHIP_SELECT;
  ENABLE_HIGH;
  ENABLE_LOW;
  CHIP_DESELECT;
}

// Register data writing in 18 bit parallel interface
void LCD_Data(GuiConst_INT32U Val)
{
  GuiConst_INT32U temp;

  MODE_DATA;
  MODE_WRITE;
  temp = (Val & 0x0000FF00)<<2;
  temp |= (Val & 0x000000FF)<<1;
  DISP_DATA = Val;
  CHIP_SELECT;
  ENABLE_HIGH;
  ENABLE_LOW;
  CHIP_DESELECT;
}

// Display data writing in 18 bit parallel interface
void LCD_DisplayData(GuiConst_INT32U Val)
{
  MODE_DATA;
  MODE_WRITE;
  DISP_DATA = Val;
  CHIP_SELECT;
  ENABLE_HIGH;
  ENABLE_LOW;
  CHIP_DESELECT;
}
#else
#ifdef SPI_INTERFACE
// 16bit SPI interface
// One byte SPI transter function
void Spi_SendData(GuiConst_INT8U Spi_Data)
{
  // Something hardware related
  // The data is transferred with the MSB first.
  // Data is valid on the rising edge of SCL
  // Wait for end of transfer
}

// Command writing in SPI mode
void LCD_Cmd(GuiConst_INT16U Val)
{
  CHIP_SELECT;

  // Header byte
  Spi_SendData(0x70);  // 01110 -ID code,0 - IM0, 0 - RS, 0 - RWB

  Spi_SendData((GuiConst_INT8U)(Val>>8));
  Spi_SendData((GuiConst_INT8U)Val);

  CHIP_DESELECT;
}

// Data writing in SPI mode
void LCD_Data(GuiConst_INT16U Val)
{
  CHIP_SELECT;

  // Header byte
  Spi_SendData(0x72);  // 01110 -ID code,0 - IM0, 1 - RS, 0 - RWB

  Spi_SendData((GuiConst_INT8U)(Val>>8));
  Spi_SendData((GuiConst_INT8U)Val);

  CHIP_DESELECT;
}
#else
// 16 bit parallel interface
// Command writing in 16 bit parallel interface
void LCD_Cmd(GuiConst_INT16U Val)
{
  MODE_COMMAND;
  MODE_WRITE;
  DISP_DATA = Val;
  CHIP_SELECT;
  ENABLE_HIGH;
  ENABLE_LOW;
  CHIP_DESELECT;
}

// Data writing in 16 bit parallel interface
void LCD_Data(GuiConst_INT16U Val)
{
  MODE_DATA;
  MODE_WRITE;
  DISP_DATA = Val;
  CHIP_SELECT;
  ENABLE_HIGH;
  ENABLE_LOW;
  CHIP_DESELECT;
}
#endif
#endif

void GuiDisplay_Init(void)
{
  GuiConst_INT32U j;

  // Start screen initialisation
  millisecond_delay(1);
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;
  millisecond_delay(1);

  // START OSCILLATION (R00h)
  LCD_Cmd(0x0000);
  // b0: 0 - stop osc, 1 - start osc
  LCD_Data(0x0001);
  millisecond_delay(10);

  // DRIVER OUTPUT CONTROL (R01h)
  LCD_Cmd(0x0001);
  // b15: 0 - VSYNC active low, 1 - VSYNC active high
  // b14: 0 - HSYNC active low, 1 - HSYNC active high
  // b13: 0 - DOTCLK on rising edge, 1 - DOTCLK on falling edge
  // b12: 0 - Enable input active low, 1 - Enable input active high
  // b5-b0: Number of lines / 8
  LCD_Data(0x0000 | (GuiConst_BYTE_LINES / 8));

  // LCD DRIVING WAVEFORM CONTROL (R02h)
  LCD_Cmd(0x0002);
  // b9-b8: 0 - frame inversion, 1 - mixed, 2 - active with VCOM = low, 3 - active with VCOM = high;
  LCD_Data(0x0000);


  // ENTRY MODE (R03h)
  LCD_Cmd(0x0003);
  // b12: 0 - RGB, 1 - BGR
  // b5: 0-Vertical address decrement , 1 - Vertical address increment
  // b4: 0-horizontal address decrement , 1 - Horizontal address increment
  LCD_Data(0x0030);

  // DISPLAY CONTROL (R07h)
  LCD_Cmd(0x0007);
  // b8: 0 - normal display mode, 1 - partial display mode
  LCD_Data(0x0000);

  // BLANK PERIOD CONTROL (R08h)
  LCD_Cmd(0x0008);
  // b11-b8: Front porch range 2 - 16
  // b3-b0:  Back porch  range 2 - 16
  LCD_Data(0x0202);

  // FRAME CYCLE CONTROL (R0Ah)
  LCD_Cmd(0x000A);
  // b10-b8: 0 - frame freq 120Hz, 7 - frame freq 50Hz
  // b3-b0: Clock per  one H period
  LCD_Data(0x0700);

  // EXTERNAL DISPLAY INTERFACE CONTROL (R0Bh)
  LCD_Cmd(0x000B);
  // b8; 0 - system interface, 1 - RGB interface
  // b5-b4: 00 - Internal clock operation, 01 - External clock operation
  LCD_Data(0x0000);

  // GATE SCAN POSITION CONTROL (R0Dh)
  LCD_Cmd(0x000D);
  // b5-b0: start line / 8
  LCD_Data(0x0000);

  // POWER CONTROL1 (R10h)
  LCD_Cmd(0x0010);
  // b8: 0 - normal operation, 1 - stand-by
  // b0: 0 - normal operation, 1 - stand-by
  LCD_Data(0x0000);

  // Power Control 2 (R11h)
  LCD_Cmd(0x0011);
  // b10-b8: boosting factor
  // b2-b0: Source driver output slew rate control
  LCD_Data(0x0000);

  // Power Control 3 (R12h)
  LCD_Cmd(0x0012);
  // b3-b0: VCI1 voltage range 0 - 16
  LCD_Data(0x0008);

  // Power control 4 (R13h)
  LCD_Cmd(0x0013);
  // b9-b8: Frequency of Step-up circuit3
  // b5-b4: Frequency of Step-up circuit2
  // b1-b0: Frequency of Step-up circuit1
  // 0 - freq*4, 1 - freq*2, 2 - freq*1, 3 - freq*0.5
  LCD_Data(0x0000);

  // Power Control 5 (R14h)
  LCD_Cmd(0x0014);
  // b5-b0: the voltage for the Gamma voltage
  LCD_Data(0x0000);

  // Power Control 6 (R15h)
  LCD_Cmd(0x0015);
  // b6: 0 - OTP value is selected, 1 - Register(R15h[5:0]:VMH) is selected.
  // b15-b8: amplitude of the VCOM
  // b5-b0: upper level of the VCOM
  LCD_Data(0x324F);

  // HORIZONTAL END RAM ADDRESS POSITION (R44h)
  LCD_Cmd(0x0044);
  LCD_Data(GuiConst_DISPLAY_WIDTH_HW);

  // HORIZONTAL START RAM ADDRESS POSITION (R45h)
  LCD_Cmd(0x0045);
  LCD_Data(0x0000);

  // VERTICAL END RAM ADDRESS POSITION (R46h)
  LCD_Cmd(0x0046);
  LCD_Data(GuiConst_BYTE_LINES);

  // VERTICAL START RAM ADDRESS POSITION (R47h)
  LCD_Cmd(0x0047);
  LCD_Data(0x0000);

  // Power Control 3 (R12h)
  LCD_Cmd(0x0012);
  // b14: 0 - manual power on, 1 - automatic power on
  LCD_Data(0x4000);

  millisecond_delay(10);

  // DISPLAY CONTROL (R07h)
  LCD_Cmd(0x0007);
  // b4: 0 - Gate output off, 1 - Gate output on
  // b1-b0: 10 - white display
  LCD_Data(0x0012);

  millisecond_delay(100);

  // DISPLAY CONTROL (R07h)
  LCD_Cmd(0x0007);
  // b4: 0 - Gate output off, 1 - Gate output on
  // b1-b0: 11 - normal display
  LCD_Data(0x0013);

  // Select line
  LCD_Cmd(0x0020);
  LCD_Data(0x0000);

  // Select start column
  LCD_Cmd(0x0021);
  LCD_Data(0x0000);

  // Write data to display RAM
  LCD_Cmd(0x0022);

#ifdef GuiConst_COLOR_DEPTH_18
  // Clear display RAM
  for (j = 0; j < GuiConst_DISPLAY_BYTES/3; j++)
    LCD_DisplayData(0x00000000);
#else
  // Clear display RAM
  for (j = 0; j < GuiConst_DISPLAY_BYTES/2; j++)
    LCD_Data(0x0000);
#endif
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;
#ifdef GuiConst_COLOR_DEPTH_18
  GuiConst_INT32U Pixel;
#endif

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(0x0020);
      LCD_Data(Y);

      // Select start column
      LCD_Cmd(0x0021);
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin);

#ifdef GuiConst_COLOR_DEPTH_18
      LastByte = GuiConst_BYTES_PR_LINE/3 - 1;
#else
      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
#endif
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      LCD_Cmd(0x0022);

#ifdef GuiConst_COLOR_DEPTH_18
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin * 3;
               X <= (LastByte * 3 + 2);
               X = X + 3)
      {
        Pixel = GuiLib_DisplayBuf[Y][X];
        Pixel |= (GuiConst_INT32U)GuiLib_DisplayBuf[Y][X+1]<<6;
        Pixel |= (GuiConst_INT32U)GuiLib_DisplayBuf[Y][X+2]<<12;
        LCD_DisplayData(Pixel);
      }
#else
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        LCD_Data((GuiConst_INT16U)GuiLib_DisplayBuf.Words[Y][X]);
      }
#endif

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}


#endif // LCD_CONTROLLER_TYPE_D51E5TA7601

#ifdef LCD_CONTROLLER_TYPE_FSA506

// ============================================================================
//
// FSA506 DISPLAY CONTROLLER
//
// This driver uses one FSA506 display controller with on-chip oscillator.
// LCD display in 16 bit 80-system parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 480x272 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired connection
// X_IM0 = 1; 0 - 9/18bit bus,  1 - 8/16bit bus
// X_IM1 = 0; 0 - 16/18bit bus, 1 - 8/9bit bus
// X_IM2 = 0; 0 - 8080 typ bus, 1 - 6800 typ bus
// X_IM3 = 0;
// X_IM4 = 0; 0 - LSB first, 1 - MSB first
// X_IM5 = 0; 0 - 16bit data RGB 565, 1 - 18bit data RGB 666

#define DISP_DATA            Pxx           // Pins X_DI15 - X_DI00
#define RESET_ON             Pxx.x = 0     // X_RESETB pin
#define RESET_OFF            Pxx.x = 1     // X_RESETB pin
#define CHIP_SELECT          Pxx.x = 0     // X_CS pin
#define CHIP_DESELECT        Pxx.x = 1     // X_CS pin
#define MODE_DATA            Pxx.x = 1     // X_RS pin
#define MODE_COMMAND         Pxx.x = 0     // X_RS pin
#define MODE_WRITE_OFF       Pxx.x = 1     // X_WR pin
#define MODE_WRITE_ON        Pxx.x = 0     // X_WR pin
#define MODE_READ_OFF        Pxx.x = 1     // X_RD pin
#define MODE_READ_ON         Pxx.x = 0     // X_RD pin

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Command writing in parallel interface
void LCD_Cmd(GuiConst_INT16U Val)
{
  MODE_COMMAND;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

// Data writing in parallel interface
void LCD_Data(GuiConst_INT16U Val)
{
  MODE_DATA;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

void GuiDisplay_Init(void)
{
  GuiConst_INT32U j;

  // Reset controller
  RESET_ON;
  millisecond_delay(10);
  RESET_OFF;

  // Initialize controller with default values
  // PLL frequency range bit5 - 0: 20MHz ~ 100MHz, 1: 100MHz ~ 300MHz
  // Output Driving Capability bits 2,1 - 00: 4mA, 01: 8mA, 10: 12mA, 11: 16mA
  LCD_Cmd(0x0040);
  LCD_Data(0x0012);

  // PLL Programmable pre-divider, 6bit(1~63)
  LCD_Cmd(0x0041);
  LCD_Data(0x0001);

  // PLL Programmable loop divider, 6bit(1~63)
  LCD_Cmd(0x0042);
  LCD_Data(0x0001);

  // MSB of X start position
  LCD_Cmd(0x0000);
  LCD_Data(0x0000);

  // LSB of X start position
  LCD_Cmd(0x0001);
  LCD_Data(0x0000);

  // MSB of X end position
  LCD_Cmd(0x0002);
  LCD_Data(GuiConst_DISPLAY_WIDTH_HW>>8);

  // LSB of X end position
  LCD_Cmd(0x0003);
  LCD_Data((GuiConst_INT8U)GuiConst_DISPLAY_WIDTH_HW);

  // MSB of Y start position
  LCD_Cmd(0x0004);
  LCD_Data(0x0000);

  // LSB of Y start position
  LCD_Cmd(0x0005);
  LCD_Data(0x0000);

  // MSB of Y end position
  LCD_Cmd(0x0006);
  LCD_Data(GuiConst_DISPLAY_HEIGHT_HW>>8);

  // LSB of Y end position
  LCD_Cmd(0x0007);
  LCD_Data((GuiConst_INT8U)GuiConst_DISPLAY_HEIGHT_HW);

  // X panel size MSB
  LCD_Cmd(0x0008);
  LCD_Data(GuiConst_DISPLAY_WIDTH_HW>>8);

  // X panel size LSB
  LCD_Cmd(0x0009);
  LCD_Data((GuiConst_INT8U)GuiConst_DISPLAY_WIDTH_HW);

  // System clock bits 1,0: 0 - clk/2, 1 - clk/4, 2 - clk/8
  LCD_Cmd(0x0010);
  LCD_Data(0x000D);

  // RGB order 0x00 - RGB, 0x55 - BGR
  LCD_Cmd(0x0011);
  LCD_Data(0x0000);

  // MSB of output H sync. pulse start position
  LCD_Cmd(0x0012);
  LCD_Data(0x0000);

  // LSB of output H sync. pulse start position
  LCD_Cmd(0x0013);
  LCD_Data(0x0000);

  // MSB of output H sync. pulse width
  LCD_Cmd(0x0014);
  LCD_Data(0x0000);

  // LSB of output H sync. pulse width
  LCD_Cmd(0x0015);
  LCD_Data(0x0010);

  // MSB of output DE horizontal start position
  LCD_Cmd(0x0016);
  LCD_Data(0x0000);

  // LSB of output DE horizontal start position
  LCD_Cmd(0x0017),
  LCD_Data(0x0044);

  // MSB of output DE horizontal active region in pixel
  LCD_Cmd(0x0018);
  LCD_Data(GuiConst_DISPLAY_WIDTH_HW>>8);

  // LSB of output DE horizontal active region in pixel
  LCD_Cmd(0x0019);
  LCD_Data((GuiConst_INT8U)GuiConst_DISPLAY_WIDTH_HW);

  // MSB of output H total in pixel
  LCD_Cmd(0x001A);
  LCD_Data(0x0001);

  // LSB of output H total in pixel
  LCD_Cmd(0x001B);
  LCD_Data(0x00B8);

  // MSB of output V sync. pulse start position
  LCD_Cmd(0x001C);
  LCD_Data(0x0000);

  // LSB of output V sync. pulse start position
  LCD_Cmd(0x001D);
  LCD_Data(0x0000);

  // MSB of output V sync. pulse width
  LCD_Cmd(0x001E);
  LCD_Data(0x0000);

  // LSB of output V sync. pulse width
  LCD_Cmd(0x001F);
  LCD_Data(0x0008);

  // MSB of output DE vertical start position
  LCD_Cmd(0x0020);
  LCD_Data(0x0000);

  // LSB of output DE vertical start position
  LCD_Cmd(0x0021);
  LCD_Data(0x0013);

  // MSB of output DE vertical active region in line
  LCD_Cmd(0x0022);
  LCD_Data(GuiConst_DISPLAY_HEIGHT_HW>>8);

  // LSB of output DE vertical active region in line
  LCD_Cmd(0x0023);
  LCD_Data((GuiConst_INT8U)GuiConst_DISPLAY_HEIGHT_HW);

  // MSB of output V total in line
  LCD_Cmd(0x0024);
  LCD_Data(0x0001);

  // LSB of output V total in line
  LCD_Cmd(0x0025);
  LCD_Data(0x0009);

  // Load output timing related setting (H sync., V sync. and DE) to take effect
  LCD_Cmd(0x0029);
  LCD_Data(0x0001);

  // bit 3 - Output pin X_DCON level control, bit 2 - inverse clock
  LCD_Cmd(0x002D);
  LCD_Data(0x0008);

  // MSB of image horizontal physical resolution in memory
  LCD_Cmd(0x0034);
  LCD_Data(GuiConst_DISPLAY_WIDTH_HW>>8);

  // LSB of image horizontal physical resolution in memory
  LCD_Cmd(0x0035);
  LCD_Data((GuiConst_INT8U)GuiConst_DISPLAY_WIDTH_HW);

  // MSB of image vertical physical resolution in memory
  LCD_Cmd(0x0036);
  LCD_Data(GuiConst_DISPLAY_HEIGHT_HW>>8);

  // LSB of image vertical physical resolution in memory
  LCD_Cmd(0x0037);
  LCD_Data((GuiConst_INT8U)GuiConst_DISPLAY_HEIGHT_HW);

  millisecond_delay(100);

  // Write data to display RAM
  LCD_Cmd(0xC1);
  // Clear display RAM
  for (j = 0; j < GuiConst_DISPLAY_BYTES/2; j++)
    LCD_Data(0x0000);
  // Disable writing to display RAM
  LCD_Cmd(0x80);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;
  GuiConst_INT32U Address;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Calculate address in memory
      Address = Y * GuiConst_DISPLAY_WIDTH_HW +
                GuiLib_DisplayRepaint[Y].ByteBegin;

      // Set address in display RAM memory
      LCD_Cmd(0x000A);
      LCD_Data((GuiConst_INT8U)(Address>>16));
      LCD_Cmd(0x000B);
      LCD_Data((GuiConst_INT8U)(Address>>8));
      LCD_Cmd(0x000C);
      LCD_Data((GuiConst_INT8U)Address);

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write data to display RAM
      LCD_Cmd(0xC1);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        LCD_Data((GuiConst_INT16U)GuiLib_DisplayBuf.Words[Y][X]);
      // Disable writing to display RAM
      LCD_Cmd(0x80);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_FSA506

#ifdef LCD_CONTROLLER_TYPE_H8S2378

// ============================================================================
//
// H8S2378 DISPLAY CONTROLLER
//
// This driver uses Direct Drive LCD technology of H8S2378 microcontroller +
// FreeRTOS operating system.
// LCD display in direct RGB interface
// Buffer is in external SRAM
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB color
//   Color depth: 16 bit
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// Hardwired connections
// RGB format is RGB 5-6-5
// Unused R0 and B0 connect to 0

#define  XTAL_FREQUENCY (16000000L)
#define  MDCLK_PIN (0)
#define  ICLK_MUL (2)
#define  PCLK_MUL (2)
#define  BCLK_MUL (2)
#define  ICLK_FREQUENCY (XTAL_FREQUENCY * ICLK_MUL)
#define  PCLK_FREQUENCY (XTAL_FREQUENCY * PCLK_MUL)
#define  BCLK_FREQUENCY (XTAL_FREQUENCY * BCLK_MUL)

// FREERTOS FILES AND DEFINITIONS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
// H8S mcu specific definitions for RTOS
//#include "portmacro.h"
//#include "port.c"

#define RLCD_QueueHandle            xQueueHandle
#define RLCD_SemaphoreHandle        xSemaphoreHandle
#define RLCD_TaskHandle             xTaskHandle

#define RLCD_TaskCreate(func, name, stksize, param, priority, handle) \
        xTaskCreate(func, name, stksize, param, priority, &handle);

#define RLCD_TaskDelayUntil(start, duration) \
        vTaskDelayUntil(&start, duration);

#define RLCD_TaskDelay              vTaskDelay
#define RLCD_TaskSuspend            vTaskSuspend
#define RLCD_TaskYield              taskYIELD
#define RLCD_TaskResume             vTaskResume
#define RLCD_TaskResumeFromISR      xTaskResumeFromISR
#define RLCD_GetTaskHandle          xTaskGetCurrentTaskHandle
#define RLCD_GetTaskTickCount       xTaskGetTickCount
#define RLCD_Malloc                 pvPortMalloc
#define RLCD_TaskPrioritySet        vTaskPrioritySet
#define RLCD_TaskPriorityGet        uxTaskPriorityGet

#define RLCD_WaitEvent
#define RLCD_SignalEvent

#define RLCD_QueueCreate(queue) \
        queue = xQueueCreate(32, (unsigned portBASE_TYPE)sizeof(EVENT_MSG));

#define RLCD_QueueSend(retval, queue, msg) \
        retval = (sI16)xQueueSend(queue, (void *)&msg, 0UL);

#define RLCD_QueueReceive(retval, queue, msg, timeout, dummyPtr) \
        retval = (sI16)xQueueReceive(queue, &msg, timeout);

#define RLCD_BinarySemaphoreCreate  vSemaphoreCreateBinary

#define RLCD_BinarySemaphoreTake(sema, timeout) xSemaphoreTake(sema, timeout)

#define RLCD_BinarySemaphoreGive    xSemaphoreGive

#define RLCD_OSStart                vTaskStartScheduler
#define RLCD_ENTER_CRITICAL         portENTER_CRITICAL()
#define RLCD_EXIT_CRITICAL          portEXIT_CRITICAL()
#define RLCD_ENTER_SWITCHING_ISR    portENTER_SWITCHING_ISR
#define RLCD_EXIT_SWITCHING_ISR     portEXIT_SWITCHING_ISR

#define demotaskSTACK_SIZE          configMINIMAL_STACK_SIZE
#define mainBUSMONITOR_PRIORITY      ( tskIDLE_PRIORITY + 2 )
#define mainDEMOTASKS_PRIORITY      ( tskIDLE_PRIORITY + 1 )
#define TICK_RATE_MS                portTICK_RATE_MS
#define TickType                    portTickType

RLCD_TaskHandle ExDMACHnd;

// Platform specific macros provided by users
#define  TRAP_VECT           8
#define  TMR_VECT            72

#define  TMR_INT_PRIORITY()  INTC.IPRH.BIT._TMR0 = configKERNEL_INTERRUPT_PRIORITY;
#define  CLR_TMR_INT()       TMR0.TCSR.BIT.CMFA = 0;
#define  CONFIG_TMR()        MSTPCR.BIT._TMR = 0; TMR0.TCR.BIT.CMIEA = 1; TMR0.TCR.BIT.CCLR = 1; TMR0.TCORA = (unsigned char)(((configCPU_CLOCK_HZ / configTICK_RATE_HZ )-1) / 8192); TMR0.TCR.BIT.CKS = 3;

// Definition of I/O Register for H8S/2378R
//#include "iodefine.h"

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

// Memory for DDR shadowing...used by DDR_xxxx Macros
GuiConst_INT8U P1DDRS, P2DDRS, P3DDRS, P4DDRS, P5DDRS, P6DDRS, P8DDRS, PADDRS, PBDDRS,
   PCDDRS,PDDDRS, PEDDRS, PFDDRS,  PGDDRS, PHDDRS, PIDDRS, PJDDRS, PKDDRS, PLDDRS, PMDDRS;

// Convert 'a' to a string
#define TO_STR1(a) #a
#define TO_STR(a) TO_STR1(a)

// Define Pin Macros to ease implementation (secondary macros _1, _2 used for expansion ONLY
#define MERGE2(a,b) a ## b
#define MERGE3(a,b,c) a ## b ## c

// Set signal DDR to output
#define DDRo2(port,pin) ( ## MERGE2(P,port) ## .DDR = (unsigned char)( ## MERGE3(P,port,DDRS) |= (1 << pin)))
#define DDRo(signal) DDRo2(signal##_PORT, signal##_PIN)

// Set signal DDR to input
#define DDRi2(port,pin) ( ## MERGE2(P,port) ## .DDR = (unsigned char)( ## MERGE3(P,port,DDRS) &= ~(1 << pin)))
#define DDRi(signal) DDRi2(signal##_PORT, signal##_PIN)

// Write signal level (for output)
#define PINo2(port,pin) MERGE2(P,port) ## MERGE2(.DR.BIT.B,pin)
#define PINo(signal) PINo2(signal##_PORT, signal##_PIN)

// Read signal level (for input)
#define PINi2(port, pin) MERGE2(P,port) ## MERGE2(.PORT.BIT.B,pin)
#define PINi(signal) PINi2(signal##_PORT, signal##_PIN)

// Signals buffer control (for inputs)
#define ICRi2(port, pin) MERGE2(P,port) ## MERGE2(.ICR.BIT.B,pin)
#define ICRi(signal) ICRi2(signal##_PORT, signal##_PIN)

#define size_ra(ra) (sizeof(ra)/sizeof(ra[0]))

#define HW_BYTE_SWAPPED_BUS
#define MEM_DECL

#ifdef MEM_DECL
#define EXTERN
#else
#define EXTERN extern
#endif

// LCD PANEL AND MCU PARAMETERS - Set according your hardware

#define STATIC_ENABLE_DD
// Number of RAM frames to allocate
#define LCD_FRAMES 1
// Specify the maximum number of regions that the screen can be split into
#define MAX_FRAME_REGIONS 1

// Define this if need High to Low memory Lines (default is Low to High)
//#define V_LINES_INVERT

// Define this if need High to Low memory Pixels (default is Low to High)
//#define H_DOT_INVERT

// Use the defintions below to change the MCU ports used for interfacing to the panel
// Define which CS is used for the frame buffer
#define FRAME_CS        6
// Number of bus cycles to run frame RAM at (based on memory access speed and xtal frequency)
#define FRAME_BUS_CYCLES 2

// Control vsync
#define VSYNC_PORT      1
#define VSYNC_PIN       4

// Control hsync
#define HSYNC_PORT      1
#define HSYNC_PIN       6

// Control dotclock
#define DOTCLK_PORT     8
#define DOTCLK_PIN      4

// Control LCD enable pin
#define LCD_ENABLE_PORT 1
#define LCD_ENABLE_PIN  1

#define TB2_OUT_PORT 1
#define TB2_OUT_PIN 7

// ExDMA Channel used for direct drive
#define EXDMAC_DD EXDMAC2
#define EXDMAC_DD_INTC (INTC.IPRI.BIT._EXDMAC2) // INTC bits assoicated with ExDMA Channel
#define EXDMAC_DD_VECT      86    // IRQ vector for channel
#define EXDMAC_DD_REQ_PORT  8
#define EXDMAC_DD_REQ_PIN   0

// TPU Channel Used for the Dot Clock Signal and period (period will not drive output)
#define DOTCLK_TPU_INTC     (INTC.IPRG.BIT._TPU5) // INTC bits assoicated with TPU Channel
#define DOTCLK_TPU_CHANNEL  5
#define DOTCLK_TPU_PIN      A
#define DOTCLK_TPU_VECT     68    // IRQ vector for channel
#define DOTPER_TPU_CHANNEL  5     // must be same as DOTCLK_TPU_CHANNEL
#define DOTPER_TPU_PIN      B
#define DOTPER_TPU_VECT     69    // IRQ vector for channel

// TPU Channel used for Horizontal Period (period will not drive output)
#define HPER_TPU_INTC       (INTC.IPRG.BIT._TPU2) // INTC bits assoicated with TPU Channel
#define HPER_TPU_CHANNEL    2
#define HPER_TPU_PIN        B
#define HPER_TPU_VECT       53    // IRQ vector for channel

// TPU Channel used for Horizontal Synch Signal (same channel as HPER_TPU_CHANNEL preferred)
#define HSYNC_TPU_INTC      (INTC.IPRG.BIT._TPU2) // INTC bits assoicated with TPU Channel
#define HSYNC_TPU_CHANNEL   2
#define HSYNC_TPU_PIN       A
#define HSYNC_TPU_VECT      52    // IRQ vector for channel

// TPU Channel used for Vertical Synch Signal (same channel as HPER_TPU_CHANNEL preferred)
#define VSYNC_TPU_INTC      (INTC.IPRF.BIT._TPU1) // INTC bits assoicated with TPU Channel
#define VSYNC_TPU_CHANNEL   1
#define VSYNC_TPU_PIN       A
#define VSYNC_TPU_VECT      48    // IRQ vector for channel

// TPU Channel used for Horizontal Data Enable Signal (ExDMA request) must be seperate channel because of PWM1 operation
// this channel will be syncd with HPER_TPU_CHANNEL to provide timebase
#define HDEN_TPU_INTC       (INTC.IPRF.BIT._TPU0) // INTC bits assoicated with TPU Channel
#define HDEN_TPU_CHANNEL    0
#define HDEN_TPU_PIN        A     // must be A or C because of PWM1 operation
#define HDEN_TPU_VECT       40    // IRQ vector for channel
#define HDEN2_TPU_CHANNEL   0     // must be same as HDEN_TPU_CHANNEL
#define HDEN2_TPU_PIN       B     // must be complementary pin to HDEN_TPU_PIN A->B, C->D
#define HDEN2_TPU_VECT      41    // IRQ vector for channel

// DOT CLOCK Inversion Control (only works with TPU generated DOTCLK modes
// ExDMA generated must invert in hardware if necessary
// Define this symbol if dot inversion necessary
#define DOT_INVERT

// Vertical pulse width (inactive pulse width)
#define V_LINES_PULSE 2
// Vertical back porch (Lines Vsynch Active till display lines)
#define V_LINES_BACK_PORCH (7-V_LINES_PULSE) // 7-V_LINES_PULSE because of datasheet spec
// Vertical lines (active data lines)
#define V_LINES_DISPLAY GuiConst_DISPLAY_HEIGHT
// Minimum Vertical Front Porch (post display till Vsych inactive0)
#define V_LINES_FRONT_PORCH 6

// Horizontal pulse width (dot clocks inactive)
#define H_DOT_PULSE 5
// Horizontal back porch (dot clocks Hsync active till display data)
#define H_DOT_BACK_PORCH (35 - H_DOT_PULSE)
// Horizonal pixels (active data)
#define H_DOT_DISPLAY GuiConst_DISPLAY_WIDTH
// Horizonatl front porch (post data till Hsynch inactive)
#define H_DOT_FRONT_PORCH 30

// Specify Desired system behavior...will adjust vertical front porch to meet request.
#define DOT_CLOCK_FREQUENCY_DATA 16000000L
#define DOT_CLOCK_FREQUENCY_BLANK DOT_CLOCK_FREQUENCY_DATA

#define DESIRED_FRAME_RATE 60
#define MINIMUM_MCU_ACCESS_PCT 30

// LCD DRIVER INTERNAL MACROS and CHECKING

// Number of bus cycles in the dot clock
#define DOT_BUS_CYCLES (BCLK_FREQUENCY/DOT_CLOCK_FREQUENCY_DATA)
// Number of TPU cycles in the dot clock
#define DOT_TPU_CYCLES (PCLK_FREQUENCY/DOT_CLOCK_FREQUENCY_BLANK)

// Define ratio of bus clock to peripheral clock 1x=1, 2x=2, 4x=4
// #define BCLK_PCLK_RATIO (BCLK_MUL/PCLK_MUL)
#define DOT_TO_PCLK(DOT_CLOCKS) ((DOT_CLOCKS) * (PCLK_FREQUENCY/DOT_CLOCK_FREQUENCY_BLANK))
#define BCLK_TO_DOTB(BCLK_CLOCKS) ((BCLK_CLOCKS) / (BCLK_FREQUENCY/DOT_CLOCK_FREQUENCY_BLANK))
#define DOTD_TO_DOTB(DOTD_CLOCKS) ((DOTD_CLOCKS) * (BCLK_FREQUENCY/DOT_CLOCK_FREQUENCY_DATA) / (PCLK_FREQUENCY/DOT_CLOCK_FREQUENCY_BLANK))

#ifndef H_DOT_FRONT_PORCH_BLANK
#define H_DOT_FRONT_PORCH_BLANK 0
#endif

#define FRAME_AST(cs) MERGE2(BSC.ASTCR.BIT.AST, cs)
#define FRAME_BSC_AST FRAME_AST(FRAME_CS)

#define FRAME_W(cs) MERGE2(BSC.WCR.BIT.W, cs)
#define FRAME_BSC_W FRAME_W(FRAME_CS)

// ExDMA Constants and BUS cycle additions
#define DRAM_REFRESH_BCLK (6)      // BClock cycles to refresh DRAM (includes RAS)
#define DRAM_RAS_BCLK (2)          // Additional BClock cyles for RAS (on page change)
#define ExDMA_REQ_TO_LCD_LATCH (8) // BClock Cycles from request to data valid if nothing else on bus
#define EXDMA_NEGOTIATE (3)        // BClock Cycles to renogitiate for the bus
#define ExDMA_SEGMENTS (4)         // Number of segments to break up transfer (H8S 256 transfer/request block limit)

// Number of extra dot clocks required during block transfer
#define ExDMA_BLK_EXTRA (ExDMA_REQ_TO_LCD_LATCH + (EXDMA_NEGOTIATE * (ExDMA_SEGMENTS-1)))  // Number of extra dot clocks required during block transfer
// Factor in all extras and bus rates to determine number of dot clocks in ExDMA transfer (round up to integer dot clock value).
#define H_DOT_DISPLAY_DATA (DOTD_TO_DOTB(H_DOT_DISPLAY) + BCLK_TO_DOTB(ExDMA_BLK_EXTRA) + 1)

#define V_LINES_PANEL (V_LINES_PULSE+V_LINES_BACK_PORCH+V_LINES_DISPLAY+V_LINES_FRONT_PORCH)
#define H_DOT_PERIOD_DATA (H_DOT_PULSE+H_DOT_BACK_PORCH+H_DOT_DISPLAY_DATA+H_DOT_FRONT_PORCH)
#define H_DOT_PERIOD_BLANK (H_DOT_PULSE+H_DOT_BACK_PORCH+H_DOT_DISPLAY+H_DOT_FRONT_PORCH+H_DOT_FRONT_PORCH_BLANK)
#define H_DOT_PERIOD_PANEL ((V_LINES_DISPLAY*H_DOT_PERIOD_DATA)+((V_LINES_PANEL-V_LINES_DISPLAY)*H_DOT_PERIOD_BLANK))
#define VLINES_EXTEND ((DOT_CLOCK_FREQUENCY_BLANK-(H_DOT_PERIOD_PANEL*DESIRED_FRAME_RATE))/(H_DOT_PERIOD_BLANK*DESIRED_FRAME_RATE))
#define V_LINES_PERIOD (V_LINES_PANEL+VLINES_EXTEND)
#define H_DOTS_BLANK ((V_LINES_PERIOD - V_LINES_DISPLAY) * (H_DOT_PERIOD_BLANK))
#define H_DOTS_DATA (V_LINES_DISPLAY * H_DOT_PERIOD_DATA)
#define MCU_ACCESS_PCT ((H_DOTS_BLANK * 100)/(H_DOTS_BLANK+H_DOTS_DATA))

#define TPU_CHANNEL(channel) MERGE2(TPU,channel)
#define TPU_DD(signal) TPU_CHANNEL(signal##_TPU_CHANNEL)
#define TPU_IO(channel,pin) MERGE2(TPU,channel) ## MERGE2(.TIOR.BIT.IO,pin)
#define TPU_DD_IO(signal) TPU_IO(signal##_TPU_CHANNEL,signal##_TPU_PIN)
#define TPU_TGR(channel,pin) MERGE2(TPU,channel) ## MERGE2(.TGR,pin)
#define TPU_DD_TGR(signal) TPU_TGR(signal##_TPU_CHANNEL,signal##_TPU_PIN)
#define TPU_TGF(channel,pin) MERGE2(TPU,channel) ## MERGE2(.TSR.BIT.TGF,pin)
#define TPU_DD_TGF(signal) TPU_TGF(signal##_TPU_CHANNEL,signal##_TPU_PIN)
#define TPU_TGIE(channel,pin) MERGE2(TPU,channel) ## MERGE2(.TIER.BIT.TGIE,pin)
#define TPU_DD_TGIE(signal) TPU_TGIE(signal##_TPU_CHANNEL,signal##_TPU_PIN)
#define TPU_TCRA 0x20 // clear on channel A
#define TPU_TCRB 0x40 // clear on channel B
#define TPU_TCRC 0xA0 // clear on channel C
#define TPU_TCRD 0xC0 // clear on channel D
#define TPU_TCR(pin) MERGE2(TPU_TCR,pin)
#define TPU_DD_TCR(signal) TPU_TCR(signal##_TPU_PIN)


// LCD DRIVER INTERNAL MEMORY ALOCATION

typedef struct
{
  GuiConst_INT16U raster[GuiConst_DISPLAY_HEIGHT * GuiConst_DISPLAY_WIDTH];
} frame_type;

// Section definition for SRAM used as LCD Frame(s)
#pragma section LCD_Frames
  frame_type frames[LCD_FRAMES];  // SRAM allocated for LCD frames
#pragma section

static GuiConst_INT16U Frame;     // index to frame buffer
static GuiConst_INT16S Hcount;    // line counter
// Number or lines in the frame...changes dynamically to set desired frame rate.
static GuiConst_INT16S VLinesPeriod;
// Frame rate corresponding to the VLinesPeriod
static GuiConst_INT16S FrameRate=DESIRED_FRAME_RATE;

typedef struct
{
  GuiConst_INT16U *pLine;         // pointer to starting line of region
  GuiConst_INT16S LineNext;       // value to change pLine by to get to start of next line
  GuiConst_INT16S Height;         // number of lines in this region
} line_list_type;

// Information about raster data to display (allocate spare block to ensure proper list termination)
static line_list_type LineList[MAX_FRAME_REGIONS+1];
// Pointer to current LineList record
static line_list_type *pLineList;
// Copy of LineList record that Driver modifies and uses for ExDMA transfer
static line_list_type ActiveLineList;

GuiConst_INT16U LCD_Vcount;       // frame counter
GuiConst_INT16U LCD_BusActive;    // flag indicating if we're running ExDMA on this line

// LCD DRIVER FUNCTIONS

// Direct Drive interrupt service routine during the vertical blanking period.
// During the blanking period, we interrupt at the BEGINNING of the data enable period.
// This allows us maximum time to set up the TPU units prior to the start of the next line.
__interrupt(vect=HDEN_TPU_VECT) void LCD_DD_BLANK_isr(void)
{
  // Configure behavior for the NEXT line
  if (Hcount >= VLinesPeriod)
  {
    TPU_DD_IO(VSYNC) = 0x1;       // Vsynch init low, Low on match
    Hcount = 0;
  }
  else
  switch (Hcount)
  {
    case (V_LINES_PULSE):
    {
      TPU_DD_IO(VSYNC) = 0x6;     // Vsynch init High, High on match
      break;
    }

    case (V_LINES_PULSE+V_LINES_BACK_PORCH):
    {
      TPU_DD_IO(HDEN) = 0x5;      // Enable Active Data Mask init high, Low on Match
      LCD_BusActive = 1;

      // Configure RAM for ExDMA access timing
#if DOT_BUS_CYCLES != FRAME_BUS_CYCLES
  #if DOT_BUS_CYCLES <= 2
          FRAME_BSC_AST = 0x0;    // 0 =2 cycle external access and disables waits, 1=use wait states
  #else
          FRAME_BSC_AST = 0x1;    // 0 =2 cycle external access and disables waits, 1=use wait states
          FRAME_BSC_W = DOT_BUS_CYCLES-3;  // wait states
  #endif
#endif
      // Reset line list
      pLineList = LineList;
      // Copy initial line list for the driver and increment pointer
      ActiveLineList = *pLineList++;
      // Offset Height so I can compare against "Hcount"
      ActiveLineList.Height += V_LINES_PULSE+V_LINES_BACK_PORCH-1;

      TPU_DD_TGF(HDEN2) = 0;      // clear pending interrupt request
      TPU_DD_TGIE(HDEN2) = 1;     // start using HDEN2
      TPU_DD_TGIE(DOTCLK) = 1;    // Signal scheduler to suspend RAM access tasks
      break;
    }

    case (V_LINES_PULSE+V_LINES_BACK_PORCH+1):
    {
      // Stop using HDEN (re-enabeld by HDEN2 ISR
      TPU_DD_TGIE(HDEN) = 0;
      // Period timeout during data transfer
      TPU_DD_TGR(HPER) = DOT_TO_PCLK(H_DOT_PERIOD_DATA)-1;
      // End of enable for data transfer
      TPU_DD_TGR(HDEN2) = DOT_TO_PCLK(H_DOT_PULSE+H_DOT_BACK_PORCH+H_DOT_DISPLAY_DATA)-1;
      break;
    }

    case (V_LINES_PULSE+V_LINES_BACK_PORCH+V_LINES_DISPLAY+1):
    {
      // Period timeout during blanking
      TPU_DD_TGR(HPER) = DOT_TO_PCLK(H_DOT_PERIOD_BLANK)-1;
      // End of enable during blanking (generates interrupts, so must be accurate even during blanking)
      TPU_DD_TGR(HDEN2) = DOT_TO_PCLK(H_DOT_PULSE+H_DOT_BACK_PORCH+H_DOT_DISPLAY)-1;
      break;
    }
  }

  if ((Hcount < (V_LINES_PULSE+V_LINES_BACK_PORCH)) || (Hcount > (V_LINES_PULSE+V_LINES_BACK_PORCH+V_LINES_DISPLAY)))
  Hcount++;                        // increment the horizontal line counter

  TPU_DD_TGF(HDEN) = 0;            // clear interrupt request
}

// Direct Drive interrupt service routine during the active data piece of the frame.
// During the data portion, we interrupt at the END of the data enable period.
// We use this configure the ExDMA for the next line of the transfer.
__interrupt(vect=HDEN2_TPU_VECT) void LCD_DD_DATA_isr(void)
{
  if (Hcount < (V_LINES_PULSE+V_LINES_BACK_PORCH+V_LINES_DISPLAY))
  {
    EXDMAC_DD.EDSAR = ActiveLineList.pLine;           // start of next line transfer
    EXDMAC_DD.EDTCR = ((H_DOT_DISPLAY/ExDMA_SEGMENTS)<<16)+ExDMA_SEGMENTS; // of words to transfer
    EXDMAC_DD.EDMDR.BIT.EDA = 1;                      // re-start DMA transfer
    ActiveLineList.pLine += ActiveLineList.LineNext;  // Active portion, need to update line pointer

    #if (MAX_FRAME_REGIONS > 1)
      // Check if ready for next region...conditionally compiled based on need
      if (Hcount >= ActiveLineList.Height)
      {
        ActiveLineList = *pLineList++;
        ActiveLineList.Height += Hcount;
      }
    #endif

    if (Hcount >= (V_LINES_PULSE+V_LINES_BACK_PORCH+V_LINES_DISPLAY-1))
    {
      // This is last line to display...turn on HDEN
      TPU_DD_TGF(HDEN) = 0;               // clear pending interrupt request
      TPU_DD_TGIE(HDEN) = 1;              // start using HDEN
    }
  }
  else
  {
    TPU_DD_IO(HDEN) = 0x6;                // Disable Active Data Mask init high, High on Match
    LCD_BusActive = 0;
    // Configure RAM for MCU access timing
#if DOT_BUS_CYCLES != FRAME_BUS_CYCLES
  #if FRAME_BUS_CYCLES <= 2
      FRAME_BSC_AST = 0x0;              // 0 =2 cycle external access and disables waits, 1=use wait states
  #else
      FRAME_BSC_AST = 0x1;              // 0 =2 cycle external access and disables waits, 1=use wait states
      FRAME_BSC_W = FRAME_BUS_CYCLES-3; // wait states
  #endif
#endif
    TPU_DD_TGIE(HDEN2) = 0;      // stop using HDEN2
    TPU_DD_TGIE(DOTCLK) = 1;     // Signal scheduler to resume RAM access tasks
    LCD_Vcount++;                //  increment v count here to give maximum time while bus idle
  }

  Hcount++;                      // increment the horizontal line counter
  TPU_DD_TGF(HDEN2) = 0;         // clear interrupt request
}

// This task calls the scheduler to wake the task that blocks other
// tasks that are accessing RAM. If RAM access tasks are not blocked,
// the CPU will stall during ExDMA block transfers.
// The IRQ source is a TPU channel that is configured to interrupt once/frame
// at the end of the vertical blanking time
// This is implemented as a called function to ensure
// a proper stack for the context switch ISR
static portBASE_TYPE task_dd_helper(void)
{
  // Resume the ExMemory control task.
  (void)RLCD_TaskResumeFromISR( ExDMACHnd );
  return(pdTRUE);
}

__interrupt(vect=DOTCLK_TPU_VECT) __regsave void TASK_DD_isr(void)
{
  RLCD_ENTER_SWITCHING_ISR();
  // Clear interupt and request to interupt
  TPU_DD_TGIE(DOTCLK) = 0;
  TPU_DD_TGF(DOTCLK) = 0;
  RLCD_EXIT_SWITCHING_ISR( task_dd_helper() );
}

// SetActiveFrame defines which frame buffer to display
// Parameters:
// Region: Region of display (horizontal strip).
// Ranging from 0 (bottom) to MAX_FRAME_REGIONS (user defined MACRO top).
// LineCount: Number of lines for this region.
// pSource: Address of first pixel of first line of region.
// LineStep: Distance (in pixels/GuiConst_INT16U�s) from first pixel of
// first line to first pixel of second line (source regions can be wider than the panel)
// Returns:  0 = ok, -1 = NG.
GuiConst_INT16S LCDSetLineSource(GuiConst_INT16S Region, GuiConst_INT16S LineCount, GuiConst_INT16U *pSource, GuiConst_INT16S LineStep)
{
  GuiConst_INT32S PixelStart;

  if (Region >= MAX_FRAME_REGIONS)
    return(-1);

  // pSource must be within LCD_Frames section
  if ((pSource < (GuiConst_INT16U *)__sectop("BLCD_Frames")) ||
      (pSource > (GuiConst_INT16U *)__secend("BLCD_Frames")))
    return(-1);

  if ((&pSource[(GuiConst_INT32S)LineCount*(GuiConst_INT32S)LineStep] < (GuiConst_INT16U *)__sectop("BLCD_Frames")) ||
      (&pSource[(GuiConst_INT32S)LineCount*(GuiConst_INT32S)LineStep] > (GuiConst_INT16U *)__secend("BLCD_Frames")))
    return(-1);

  // Only allow region to be up to panel size
  if (LineCount > V_LINES_PANEL)
    return(-1);

  // Set line step and determine pixel start offset
  #ifndef H_DOT_INVERT
    PixelStart = 0;                           // Move to start of line
  #else
    PixelStart = H_DOT_PANEL-1;               // Move to end of line
  #endif
  #ifndef V_LINES_INVERT
    LineList[Region].LineNext = LineStep;     // Increment through lines
  #else
    LineList[Region].LineNext = -LineStep;    // Decrement through lines
    PixelStart += ((LineCount-1) * LineStep); // Move to end of frame
  #endif

  LineList[Region].pLine = pSource + PixelStart;
  LineList[Region].Height = LineCount;

  return(0);
}

// Returns current frame rate in Hz.
GuiConst_INT16S LCDGetFrameRate(void)
{
  return(FrameRate);
}

// Set desired Frame Rate in Hz.
GuiConst_INT16S LCDSetFrameRate(GuiConst_INT16S rate)
{
  // first calculate how many DOTCLK cycles remaining per second at desired rate
  GuiConst_INT32S VLinesExtend = DOT_CLOCK_FREQUENCY_BLANK -
                                 (H_DOT_PERIOD_PANEL* (GuiConst_INT32S)rate);
  // now divide remaining DOTCLK into "rate" number of lines
  VLinesExtend /= (H_DOT_PERIOD_BLANK* (GuiConst_INT32S)rate);
  // if postive, set the value for use
  if ((VLinesExtend > 0) && (rate > 0))
  {
    GuiConst_INT32S HDotsBlank;
    VLinesPeriod = V_LINES_PANEL+(GuiConst_INT16S)VLinesExtend;
    HDotsBlank = (VLinesPeriod - V_LINES_DISPLAY) * (H_DOT_PERIOD_BLANK);
    FrameRate = rate;
    return ((HDotsBlank * 100L)/(HDotsBlank + H_DOTS_DATA));
  }
  else
  {
    return((GuiConst_INT16S)VLinesExtend);
  }
}


// RTOS
#define MAX_EXMEMORY 16
static RLCD_TaskHandle ExMemoryList[MAX_EXMEMORY];

RLCD_TaskHandle easyGuiRefreshHnd;
RLCD_TaskHandle easyGuiCodeHnd;

// Let's the system know that this task has completed using the external memory.
void ExMemoryRelease(RLCD_TaskHandle handle)
{
  GuiConst_INT8U index;

  // Search for our slot and set to NULL
  for (index=0; index < MAX_EXMEMORY; index++)
  {
    if (ExMemoryList[index] == handle)
      ExMemoryList[index] = NULL;
  }
}

// Lets the system know that this task is currently using the external memory.
// The idea being that when the ExDMA block transfer is started, that we suspend
// this task until the next Vertical Blanking period.
void ExMemoryAcquire(RLCD_TaskHandle handle)
{
  GuiConst_INT8U index;
  GuiConst_INT8U return_flag = FALSE;

  for (;;)
  {
    // Search for an open slot
    for (index=0; index < MAX_EXMEMORY; index++)
    {
      RLCD_ENTER_CRITICAL;
      if (ExMemoryList[index] == NULL)
      {
        // Success, store and return
        ExMemoryList[index] = handle;
        return_flag = TRUE;
      }
      RLCD_EXIT_CRITICAL;
      if (return_flag)
      {
        // If the bus is already busy, we must manually suspend ourselves,
        // The ExMemory Monitor task will resume us.
        if (LCD_BusActive != 0)
          RLCD_TaskSuspend( NULL );
        return;
      }
    }
    // No open slot found...we must wait
    RLCD_TaskYield();
  }
}

// ExMemoryMonitor
static void ExMemoryMonitor(void *pvParameters)
{
  GuiConst_INT8U list_index, index;

  // Initialize list of tasks to suspend on data transfer
  for (index=0; index < MAX_EXMEMORY; index++)
    ExMemoryList[index] = NULL;

  for ( ;; )
  {
    if (LCD_BusActive != 0)
    {
      // Suspend tasks currently using external memory
      for (index=0; index < MAX_EXMEMORY; index++)
      {
        if (ExMemoryList[index] != NULL)
          RLCD_TaskSuspend( ExMemoryList[index] );
      }
    }
    else
    {
      // Resume tasks currently using external memory
      for (index=0; index < MAX_EXMEMORY; index++)
      {
        if (ExMemoryList[index] != NULL)
          RLCD_TaskResume( ExMemoryList[index] );
      }
    }
    RLCD_TaskSuspend( NULL ); // Suspend ourselves.
  }
}

// easyGui refresh task
static void easyGuiRefresh(void *pvParameters)
{
  GuiConst_INT32U i;
  GuiConst_INT16S LineNo, LastByte, N;
  GuiConst_INT16U *pMem = frames[0].raster;
  GuiConst_INT16S rate;

  GuiConst_INT16U vlast = 0;

  // Set time between two calls to 250mS
  const TickType xBlockTime = (TickType) (250 * TICK_RATE_MS);

  // wait for 2 frame before initializing memory
  while ( LCD_Vcount < 2)
    ;

  // Set frame buffer 0
  LCDSetLineSource(0, V_LINES_DISPLAY, frames[0].raster, GuiConst_DISPLAY_WIDTH);

  for( ;; )
  {
    // Let system know we're accessing External Memory
    ExMemoryAcquire(RLCD_GetTaskHandle());

    vlast=LCD_Vcount;

    rate = LCDGetFrameRate();
    (void)LCDSetFrameRate(10);    // temporarily drop frame rate

    GuiDisplay_Lock ();

    for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++) // Loop through lines
    {
      if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
      // Something to redraw on this line
      {
        // Calculate address in frame buffer
        i = LineNo;
        i = i * GuiConst_DISPLAY_WIDTH + GuiLib_DisplayRepaint[LineNo].ByteBegin;

        // Calculate last byte
        LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
        if (GuiLib_DisplayRepaint[LineNo].ByteEnd < LastByte)
          LastByte = GuiLib_DisplayRepaint[LineNo].ByteEnd;

        // Write display data
        for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin; N <= LastByte; N++)
          pMem[i++] = GuiLib_DisplayBuf.Words[LineNo][N];

        // Reset repaint parameters
        GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
      }
    }

    GuiDisplay_Unlock ();

    (void)LCDSetFrameRate(rate);  // restore frame rate

    ExMemoryRelease(RLCD_GetTaskHandle());

    // Block this task for xBlockTime
    RLCD_TaskDelay(xBlockTime);
  }
}


// easyGui code task
static void easyGuiCode(void *pvParameters)
{
  for( ;; )
  {
    // EasyGui code here


    // ExMemoryAcquire(RLCD_GetTaskHandle());
    // Code which is accessing external memory put between these two functions
    // ExMemoryRelease(RLCD_GetTaskHandle());
  }
}


void GuiDisplay_Init(void)
{
  GuiConst_INT16S region;
  GuiConst_INT32U i;

  // HardwareSetup
  // Frequency multiplication set immediately after STCS setting
  SCKCR.BIT.STCS = 1;

  // Multiply input clock x2 (16.00 x 2 = 32MHz)
  PLLCR.BIT.STC = 0x01;

  // Disable module stop mode for DA, DMAC, TPU, PPG
  MSTPCR.BYTE.H &= 0x00;

  // Set to interrupt control select mode 2
  INTC.INTCR.BIT.INTM = 0x02;
  // Set interrupt priority levels
  // Initialise all priorities to lowest
  INTC.IPRA.WORD = 0x0000;
  INTC.IPRB.WORD = 0x0000;
  INTC.IPRC.WORD = 0x0000;
  INTC.IPRD.WORD = 0x0000;
  INTC.IPRE.WORD = 0x0000;
  INTC.IPRF.WORD = 0x0000;
  INTC.IPRG.WORD = 0x0000;
  INTC.IPRH.WORD = 0x0000;
  INTC.IPRI.WORD = 0x0000;
  INTC.IPRJ.WORD = 0x0000;

  // Enable external bus
  SYSCR.BIT.EXPE = 1;

  // Set all memory areas to 16 bits wide
  BSC.ABWCR.BYTE = 0x0;        // 0 = 16bits wide, 0xff = 8 bits wide
  BSC.ASTCR.BYTE = 0x00;       // 0 = 2 cycle external access and disables waits
  BSC.ASTCR.BIT.AST2 = 1;      // wait states on flash

  // Turn on address lines
  PC.DDR = PCDDRS = 0xFF;
  PB.DDR = PBDDRS = 0xFF;
  PA.DDR = PADDRS = 0xff;
  PG.DDR = PGDDRS = 0x03;
  PH.DDR = 0x04;

  // Enable RLCD NAND gate to drive
  DDRo(TB2_OUT);
  PINo(TB2_OUT) = 1;
  // Drive panel enable static low
  DDRo(LCD_ENABLE);
  PINo(LCD_ENABLE) = 0;

  // Initalizes the exDMA, and timer output TOICA0
  // to generate the exDMA request signal

  PINo(HSYNC) = 1;              // Hsync active low , so set high.
  DDRo(HSYNC);                  // set Hsync to output
  PINo(VSYNC) = 1;              // Vsync active low , so set high
  DDRo(VSYNC);                  // set Vsync to output

  PINo(DOTCLK) = 1;  // Set to idle state of DOTCLK
  DDRo(DOTCLK);      // Set EDACK2 to output DOTCLK when we're bit banging

  EXDMAC_DD.EDMDR.BIT.EDA = 0;    // stops DMA transfer
  EXDMAC_DD.EDMDR.BIT.AMS = 1;    // single address mode selected
  EXDMAC_DD.EDMDR.BIT.MDS = 0x3;  // 0x2= EXTERN REQ, 0x3=block
  EXDMAC_DD.EDMDR.BIT.IRF = 0;    // must read this bit (int req flag) before clearing it
  EXDMAC_DD.EDMDR.BIT.SDIR = 0;   // single address direction read to DACK
  EXDMAC_DD.EDMDR.BIT.DTSIZE = 1; // dma size: 1 = 16 bit,  0= 8 bit
                                  //
  #ifndef H_DOT_INVERT
    EXDMAC_DD.EDACR.BIT.SAT = 0x2;// increment source address after each xfer
  #else
    EXDMAC_DD.EDACR.BIT.SAT = 0x3;// decrement source address after each xfer
  #endif

  // Initialize line list with default values

  for (region=0; region < size_ra(LineList); region++)
    LCDSetLineSource(region, V_LINES_DISPLAY, frames[Frame].raster, GuiConst_DISPLAY_WIDTH);

  // Stop all LCD_DD channels
  TPU.TSTR.BYTE &= ~((1u<<HPER_TPU_CHANNEL)|(1u<<HSYNC_TPU_CHANNEL)
                   |(1u<<VSYNC_TPU_CHANNEL)|(1u<<HDEN_TPU_CHANNEL) | (1u<<DOTCLK_TPU_CHANNEL));

  // Configure Vsynch
  TPU_DD(VSYNC).TMDR.BYTE = 0xC3;    // use PWM mode 2
  TPU_DD(VSYNC).TCR.BYTE = 0x60;     // Configure for HPER Synch clear (may be overwritten by HPER)
  TPU_DD_IO(VSYNC) = 0x6;            // Vsynch init high, high on match.
  TPU_DD_TGR(VSYNC) = 0;             // Vsynch pulse is multiples of Hperiod

  // Configure Horizontal Synch
  TPU_DD(HSYNC).TMDR.BYTE = 0xC3;    // use PWM mode 2
  TPU_DD(HSYNC).TCR.BYTE = 0x60;     // Configure for HPER Synch clear (may be overwritten by HPER)
  TPU_DD_IO(HSYNC) = 0x2;            // Hsynch init low, high on match.
  TPU_DD_TGR(HSYNC) = DOT_TO_PCLK(H_DOT_PULSE)-1; // Hynch Pulse Width

  // Configure Data Enable (ExDMA Request)
  TPU_DD(HDEN).TMDR.BYTE = 0xC2;     // PWM Mode 1 operation
  TPU_DD(HDEN).TCR.BYTE = 0x60;      // Clear from the HPER Synchronous Input
  TPU_DD_IO(HDEN) = 0x6;             // Active Data Mask init high, High on Match
  // Extra -1 because in controlling the external mux we don't want to switch CLK as it is changing
  // to avoid potential "glitch". Output of MUX should be same before/after.
  TPU_DD_TGR(HDEN) = DOT_TO_PCLK(H_DOT_PULSE+H_DOT_BACK_PORCH)-1-1;
  TPU_DD_IO(HDEN2) = 0x6;            // Active Data Mask init high, High on Match
  TPU_DD_TGR(HDEN2) = DOT_TO_PCLK(H_DOT_PULSE+H_DOT_BACK_PORCH+H_DOT_DISPLAY)-1;

  // Configure Horizontal Period
  TPU_DD(HPER).TMDR.BYTE = 0xC3;                // use PWM mode 2
  TPU_DD(HPER).TCR.BYTE = TPU_DD_TCR(HPER);     // Set TCR to clear TCNT on HPER
  TPU_DD_IO(HPER) = 0x0;                        // only used for timer
  TPU_DD_TGR(HPER) = DOT_TO_PCLK(H_DOT_PERIOD_BLANK)-1; // period timeout

  // Configure Dot Clock & Dot Period (must be same channel)
  TPU_DD(DOTCLK).TMDR.BYTE = 0xC3;              // use PWM mode 2
  TPU_DD(DOTCLK).TCR.BYTE = TPU_DD_TCR(DOTPER); // Set TCR to clear TCNT on DOTPER
#ifdef DOT_INVERT
  TPU_DD_IO(DOTCLK) = 0x5;                      // Dot Clock init hi, Low on match
#else
  TPU_DD_IO(DOTCLK) = 0x2;                      // Dot Clock init low, High on match
#endif
  TPU_DD_TGR(DOTCLK) = DOT_TO_PCLK(0.5)-1;      // set falling edge time
  TPU_DD_IO(DOTPER) = 0x0;                      // Used for timeout
  TPU_DD_TGR(DOTPER) = DOT_TO_PCLK(1)-1;        // Dot clock period

  // Run ISR at end of Active Data Mask
  TPU_DD_TGIE(HDEN) = 1;
  HDEN_TPU_INTC = configDIRECT_DRIVE_INTERRUPT_PRIORITY;

  // Run Task Control ISR at start of Vertical Active period (via DOTCLK IRQ)
  DOTCLK_TPU_INTC = configKERNEL_INTERRUPT_PRIORITY;

  // Synch horizontal channels
  TPU.TSYR.BYTE =  (1u<<HPER_TPU_CHANNEL)|(1u<<HSYNC_TPU_CHANNEL)|
                   (1u<<VSYNC_TPU_CHANNEL)|(1u<<HDEN_TPU_CHANNEL);
  // Start all LCD_DD channels
  TPU.TSTR.BYTE |= (1u<<HPER_TPU_CHANNEL)|(1u<<HSYNC_TPU_CHANNEL)|
                   (1u<<VSYNC_TPU_CHANNEL)|(1u<<HDEN_TPU_CHANNEL) | (1u<<DOTCLK_TPU_CHANNEL);

  Hcount = 0;              // ensure first IRQ starts new frame
  VLinesPeriod = V_LINES_PERIOD;

  // Clear frame buffer
  for (i = 0; i <  GuiConst_DISPLAY_BYTES/2; i++)
   frames[0].raster[i] = 0x0000;


  // Create ExMemoryMonitor task
  (void)RLCD_TaskCreate(ExMemoryMonitor, "a:ExDMAC",
     demotaskSTACK_SIZE, NULL, mainBUSMONITOR_PRIORITY, ExDMACHnd);

  // Create Display buffer refresh task
  (void)RLCD_TaskCreate(easyGuiRefresh, "e:easyGuiRfr",
     demotaskSTACK_SIZE, NULL, mainDEMOTASKS_PRIORITY, easyGuiRefreshHnd);

  // Create easyGui code task
  (void)RLCD_TaskCreate(easyGuiCode, "e:easyGuiCode",
     demotaskSTACK_SIZE, NULL, mainDEMOTASKS_PRIORITY, easyGuiCodeHnd);

  // Start scheduler
  RLCD_OSStart();
}

// Empty refresh function
void GuiDisplay_Refresh(void)
{
  // No action
  // Refreshing is moved to easyGuiRefresh task
}

#endif // LCD_CONTROLLER_TYPE_H8S2378

#ifdef LCD_CONTROLLER_TYPE_HD61202_2H

// ============================================================================
//
// HD61202_2H DISPLAY CONTROLLER
//
// This driver uses two HD61202 display controllers.
// LCD display 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 128x64 pixels.
//
// Compatible display controllers:
//   AX6108/AX6107, KS0108B/KS0107B, KS0708/KS0707, NT7108/NT7107,
//   S6B0108/S6B0107, S6B0108A/S6B0107A, S6B0708/S6B0707, S6B2108/S6B2107,
//   SBN0064
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 2
//   Number of display controllers, vertically: 1
//
// Port addresses (Px) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define CTRL_BYTE_FIRST_PAGE 0xB8
#define CTRL_BYTE_FIRST_POS  0x40
#define RESET_CS1            _P30= 0
#define SET_CS1              _P30= 1
#define RESET_CS2            _P31= 0
#define SET_CS2              _P31= 1
#define RESET_E              _P32= 0
#define SET_E                _P32= 1
#define RESET_DI             _P33= 0
#define SET_DI               _P33= 1
#define RESET_RW             _P34= 0
#define SET_RW               _P34= 1
#define DB                   P4
#define DISPLAY_LIGHT_ON     _P51 = 1
#define DISPLAY_LIGHT_OFF    _P51 = 0
#define DB_INPUT             PM4 = 0xFF
#define DB_OUTPUT            PM4 = 0x00

// Waits until controller is ready
// Remember to select display controller in advance
#define WAIT                                                             \
{                                                                        \
  DB_INPUT;                                                              \
  do {                                                                   \
  RESET_E;                                                               \
  SET_RW;                                                                \
  RESET_DI;                                                              \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  SET_E;                                                                 \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  } while ((DB & 0x90) != 0);                                            \
  RESET_E;                                                               \
  DB_OUTPUT;                                                             \
}

// Hitachi HD61202 COMMAND writing
// Remember to select display controller in advance
// Remember to call WAIT before writing
#define WRITE_KONTROL_REG(Val)                                           \
{                                                                        \
  WAIT;                                                                  \
  RESET_RW;                                                              \
  RESET_DI;                                                              \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  SET_E;                                                                 \
  DB= Val;                                                               \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  RESET_E;                                                               \
}

// Hitachi HD61202 DATA write
// Remember to select display controller in advance
// Remember to call WAIT before writing
#define WRITE_DATA_REG(val)                                              \
{                                                                        \
  WAIT;                                                                  \
  RESET_RW;                                                              \
  SET_DI;                                                                \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  SET_E;                                                                 \
  DB= val;                                                               \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  RESET_E;                                                               \
}

// Hitachi HD61202 controller selection
// Selects controller 1 or 2
void ControllerSelect (GuiConst_INT8U index)
{
  switch (index)
  {
    case 1:
      SET_CS1;
      RESET_CS2;
      break;

    case 2:
      RESET_CS1;
      SET_CS2;
      break;

    default:
      RESET_CS1;
      RESET_CS2;
  }
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT8U ContrIndex;
  GuiConst_INT16S X, Y;

  // Turn display light on
  DISPLAY_LIGHT_ON;

  // Display off while initializing
  ControllerSelect (1);
  WRITE_KONTROL_REG (0x3E);

  ControllerSelect (2);
  WRITE_KONTROL_REG (0x3E);

  // Display start line in RAM is 000000b
  ControllerSelect (1);
  WRITE_KONTROL_REG (0xC0);

  ControllerSelect (2);
  WRITE_KONTROL_REG (0xC0);

  // Clear display memory
  for (ContrIndex = 1; ContrIndex <= 2; ContrIndex++) // Both controllers
  {
    ControllerSelect (ContrIndex);
    for (Y = 0; Y < GuiConst_BYTE_LINES; Y++) // Loop through lines
    {
      WRITE_KONTROL_REG (CTRL_BYTE_FIRST_PAGE + Y); // Select page
      WRITE_KONTROL_REG (CTRL_BYTE_FIRST_POS); // Select first byte
      for (X = 0; X < GuiConst_BYTES_PR_SECTION; X++) // Loop through bytes
        WRITE_DATA_REG (0x00);
    }
  }

  // Finished initializing, set display on
  ControllerSelect (1);
  WRITE_KONTROL_REG (0x3F);

  ControllerSelect (2);
  WRITE_KONTROL_REG (0x3F);

  ControllerSelect (0);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S LineNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  // Walk through all lines
  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++)
  {
    if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      if (GuiLib_DisplayRepaint[LineNo].ByteBegin <
          GuiConst_BYTES_PR_SECTION)
      // Something to redraw in first section
      {
        // Select controller
        ControllerSelect (1);
        // Select page
        WRITE_KONTROL_REG (CTRL_BYTE_FIRST_PAGE + LineNo);

        // Select starting byte
        WRITE_KONTROL_REG (CTRL_BYTE_FIRST_POS +
                           GuiLib_DisplayRepaint[LineNo].ByteBegin);
        // Write display data
        LastByte = GuiConst_BYTES_PR_SECTION - 1;
        if (GuiLib_DisplayRepaint[LineNo].ByteEnd < LastByte)
          LastByte = GuiLib_DisplayRepaint[LineNo].ByteEnd;
        for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin;
             N <= LastByte; N++)
          WRITE_DATA_REG (GuiLib_DisplayBuf[LineNo][N]);

        // Reset repaint parameters
        if (GuiLib_DisplayRepaint[LineNo].ByteEnd >=
            GuiConst_BYTES_PR_SECTION)
          // Something to redraw in second section
          GuiLib_DisplayRepaint[LineNo].ByteBegin =
            GuiConst_BYTES_PR_SECTION;
        else // Done with this line
          GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
      }

      if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
        // Something to redraw in second section
      {
        // Select controller
        ControllerSelect (2);
        // Select page
        WRITE_KONTROL_REG (CTRL_BYTE_FIRST_PAGE + LineNo);

        // Select starting byte
        WRITE_KONTROL_REG (CTRL_BYTE_FIRST_POS +
                           GuiLib_DisplayRepaint[LineNo].ByteBegin -
                           GuiConst_BYTES_PR_SECTION);
        // Write display data
        for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin;
             N <= GuiLib_DisplayRepaint[LineNo].ByteEnd; N++)
          WRITE_DATA_REG (GuiLib_DisplayBuf[LineNo][N]);

        // Reset repaint parameters
        GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
      }
    }
  }

  // Finished drawing
  ControllerSelect (0);

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_HD61202

#ifdef LCD_CONTROLLER_TYPE_HD61202_4H

// ============================================================================
//
// HD61202_4H DISPLAY CONTROLLER
//
// This driver uses four HD61202 display controllers.
// LCD display 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 256x64 pixels.
//
// Compatible display controllers:
//   AX6108/AX6107, KS0108B/KS0107B, KS0708/KS0707, NT7108/NT7107,
//   S6B0108/S6B0107, S6B0108A/S6B0107A, S6B0708/S6B0707, S6B2108/S6B2107,
//   SBN0064
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 4
//   Number of display controllers, vertically: 1
//
// Port addresses (Px) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define CTRL_BYTE_FIRST_PAGE 0xB8
#define CTRL_BYTE_FIRST_POS  0x40
#define RESET_CS1            _P30= 0
#define SET_CS1              _P30= 1
#define RESET_CS2            _P31= 0
#define SET_CS2              _P31= 1
#define RESET_CS3            _P30= 0
#define SET_CS3              _P30= 1
#define RESET_CS4            _P31= 0
#define SET_CS4              _P31= 1
#define RESET_E              _P32= 0
#define SET_E                _P32= 1
#define RESET_DI             _P33= 0
#define SET_DI               _P33= 1
#define RESET_RW             _P34= 0
#define SET_RW               _P34= 1
#define DB                   P4
#define DISPLAY_LIGHT_ON     _P51 = 1
#define DISPLAY_LIGHT_OFF    _P51 = 0
#define DB_INPUT             PM4 = 0xFF
#define DB_OUTPUT            PM4 = 0x00

// Waits until controller is ready
// Remember to select display controller in advance
#define WAIT                                                             \
{                                                                        \
  DB_INPUT;                                                              \
  do {                                                                   \
  RESET_E;                                                               \
  SET_RW;                                                                \
  RESET_DI;                                                              \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  SET_E;                                                                 \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  } while ((DB & 0x90) != 0);                                            \
  RESET_E;                                                               \
  DB_OUTPUT;                                                             \
}

// Hitachi HD61202 COMMAND writing
// Remember to select display controller in advance
// Remember to call WAIT before writing
#define WRITE_KONTROL_REG(Val)                                           \
{                                                                        \
  WAIT;                                                                  \
  RESET_RW;                                                              \
  RESET_DI;                                                              \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  SET_E;                                                                 \
  DB= Val;                                                               \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  RESET_E;                                                               \
}

// Hitachi HD61202 DATA write
// Remember to select display controller in advance
// Remember to call WAIT before writing
#define WRITE_DATA_REG(val)                                              \
{                                                                        \
  WAIT;                                                                  \
  RESET_RW;                                                              \
  SET_DI;                                                                \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  SET_E;                                                                 \
  DB= val;                                                               \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  RESET_E;                                                               \
}

// Hitachi HD61202 controller selection
// Selects controller from 1 to 4
void ControllerSelect (GuiConst_INT8U index)
{
  switch (index)
  {
    case 1:
      SET_CS1;
      RESET_CS2;
      RESET_CS3;
      RESET_CS4;
      break;

    case 2:
      RESET_CS1;
      SET_CS2;
      RESET_CS3;
      RESET_CS4;
      break;

    case 3:
      RESET_CS1;
      RESET_CS2;
      SET_CS3;
      RESET_CS4;
      break;

    case 4:
      RESET_CS1;
      RESET_CS2;
      RESET_CS3;
      SET_CS4;
      break;

    default:
      RESET_CS1;
      RESET_CS2;
      RESET_CS3;
      RESET_CS4;
  }
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT8U ContrIndex;
  GuiConst_INT16S X, Y;

  // Turn display light on
  DISPLAY_LIGHT_ON;

  // Display off while initializing
  for (ContrIndex = 1; ContrIndex <= GuiConst_CONTROLLER_COUNT_HORZ; ContrIndex++)
  {
    ControllerSelect (ContrIndex);
    WRITE_KONTROL_REG (0x3E);
  }

  // Display start line in RAM is 000000b
  for (ContrIndex = 1; ContrIndex <= GuiConst_CONTROLLER_COUNT_HORZ; ContrIndex++)
  {
    ControllerSelect (ContrIndex);
    WRITE_KONTROL_REG (0xC0);
  }

  // Clear display memory
  for (ContrIndex = 1; ContrIndex <= 4; ContrIndex++) // Both controllers
  {
    ControllerSelect (ContrIndex);
    for (Y = 0; Y < GuiConst_BYTE_LINES; Y++) // Loop through lines
    {
      WRITE_KONTROL_REG (CTRL_BYTE_FIRST_PAGE + Y); // Select page
      WRITE_KONTROL_REG (CTRL_BYTE_FIRST_POS); // Select first byte
      for (X = 0; X < GuiConst_BYTES_PR_SECTION; X++) // Loop through bytes
        WRITE_DATA_REG (0x00);
    }
  }

  // Finished initializing, set display on
  for (ContrIndex = 1; ContrIndex <= GuiConst_CONTROLLER_COUNT_HORZ; ContrIndex++)
  {
    ControllerSelect (ContrIndex);
    WRITE_KONTROL_REG (0x3F);
  }

  ControllerSelect (0);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S LineNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;
  GuiConst_INT8U ContrIndex;

  // Lock GUI resources
  GuiDisplay_Lock ();

  // Walk through all lines
  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++)
  {
    if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
    {
      // Something to redraw on this page
      for (ContrIndex = 1; ContrIndex <= GuiConst_CONTROLLER_COUNT_HORZ; ContrIndex++)
      {
        //  loop through all controllers
        if (GuiLib_DisplayRepaint[LineNo].ByteBegin <
            GuiConst_BYTES_PR_SECTION * ContrIndex)
        // Something to redraw in this section
        {
          // Select controller
          ControllerSelect (ContrIndex);
          // Select page
          WRITE_KONTROL_REG(CTRL_BYTE_FIRST_PAGE + LineNo);
          // Select starting byte
          WRITE_KONTROL_REG(CTRL_BYTE_FIRST_POS +
                            GuiLib_DisplayRepaint[LineNo].ByteBegin -
                            GuiConst_BYTES_PR_SECTION * (ContrIndex - 1));
          // Write display data
          LastByte = (GuiConst_BYTES_PR_SECTION * ContrIndex) - 1;
          if (GuiLib_DisplayRepaint[LineNo].ByteEnd < LastByte)
            LastByte = GuiLib_DisplayRepaint[LineNo].ByteEnd;
          for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin; N <= LastByte; N++)
            WRITE_DATA_REG(GuiLib_DisplayBuf[LineNo][N]);

          // Reset repaint parameters
          if (GuiLib_DisplayRepaint[LineNo].ByteEnd >=
              GuiConst_BYTES_PR_SECTION*ContrIndex)
          // Something to redraw in next section
            GuiLib_DisplayRepaint[LineNo].ByteBegin =
               GuiConst_BYTES_PR_SECTION*ContrIndex;
          else // Done with this line
            GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
        }
      }
    }
  }

  // Finished drawing
  ControllerSelect (0);

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_HD61202_4H

#ifdef LCD_CONTROLLER_TYPE_HD61202_2H2V

// ============================================================================
//
// HD61202_2H2V DISPLAY CONTROLLER
//
// This driver uses four HD61202 display controllers.
// LCD display 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 128x128 pixels.
//
// Compatible display controllers:
//   AX6108/AX6107, KS0108B/KS0107B, KS0708/KS0707, NT7108/NT7107,
//   S6B0108/S6B0107, S6B0108A/S6B0107A, S6B0708/S6B0707, S6B2108/S6B2107,
//   SBN0064
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 2
//   Number of display controllers, vertically: 2
//
// Port addresses (Px) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define CTRL_BYTE_FIRST_PAGE 0xB8
#define CTRL_BYTE_FIRST_POS  0x40
#define RESET_CS1            _P30= 0
#define SET_CS1              _P30= 1
#define RESET_CS2            _P31= 0
#define SET_CS2              _P31= 1
#define RESET_CS3            _P30= 0
#define SET_CS3              _P30= 1
#define RESET_CS4            _P31= 0
#define SET_CS4              _P31= 1
#define RESET_E              _P32= 0
#define SET_E                _P32= 1
#define RESET_DI             _P33= 0
#define SET_DI               _P33= 1
#define RESET_RW             _P34= 0
#define SET_RW               _P34= 1
#define DB                   P4
#define DISPLAY_LIGHT_ON     _P51 = 1
#define DISPLAY_LIGHT_OFF    _P51 = 0
#define DB_INPUT             PM4 = 0xFF
#define DB_OUTPUT            PM4 = 0x00

// Waits until controller is ready
// Remember to select display controller in advance
#define WAIT                                                             \
{                                                                        \
  DB_INPUT;                                                              \
  do {                                                                   \
  RESET_E;                                                               \
  SET_RW;                                                                \
  RESET_DI;                                                              \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  SET_E;                                                                 \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  } while ((DB & 0x90) != 0);                                            \
  RESET_E;                                                               \
  DB_OUTPUT;                                                             \
}

// Hitachi HD61202 COMMAND writing
// Remember to select display controller in advance
// Remember to call WAIT before writing
#define WRITE_KONTROL_REG(Val)                                           \
{                                                                        \
  WAIT;                                                                  \
  RESET_RW;                                                              \
  RESET_DI;                                                              \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  SET_E;                                                                 \
  DB= Val;                                                               \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  RESET_E;                                                               \
}

// Hitachi HD61202 DATA write
// Remember to select display controller in advance
// Remember to call WAIT before writing
#define WRITE_DATA_REG(val)                                              \
{                                                                        \
  WAIT;                                                                  \
  RESET_RW;                                                              \
  SET_DI;                                                                \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  SET_E;                                                                 \
  DB= val;                                                               \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  RESET_E;                                                               \
}

// Hitachi HD61202 controller selection
// Selects controller from 1 to 4
void ControllerSelect (GuiConst_INT8U index)
{
  switch (index)
  {
    case 1:
      SET_CS1;
      RESET_CS2;
      RESET_CS3;
      RESET_CS4;
      break;

    case 2:
      RESET_CS1;
      SET_CS2;
      RESET_CS3;
      RESET_CS4;
      break;

    case 3:
      RESET_CS1;
      RESET_CS2;
      SET_CS3;
      RESET_CS4;
      break;

    case 4:
      RESET_CS1;
      RESET_CS2;
      RESET_CS3;
      SET_CS4;
      break;

    default:
      RESET_CS1;
      RESET_CS2;
      RESET_CS3;
      RESET_CS4;
  }
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT8U ContrIndex;
  GuiConst_INT16S X, Y;

  // Turn display light on
  DISPLAY_LIGHT_ON;

  // Display off while initializing
  for (ContrIndex = 1; ContrIndex <= 4; ContrIndex++)
  {
    ControllerSelect (ContrIndex);
    WRITE_KONTROL_REG (0x3E);
  }

  // Display start line in RAM is 000000b
  for (ContrIndex = 1; ContrIndex <= 4; ContrIndex++)
  {
    ControllerSelect (ContrIndex);
    WRITE_KONTROL_REG (0xC0);
  }

  // Clear display memory
  for (ContrIndex = 1; ContrIndex <= 4; ContrIndex++) // Both controllers
  {
    ControllerSelect (ContrIndex);
    for (Y = 0; Y < GuiConst_LINES_PR_SECTION; Y++) // Loop through lines
    {
      WRITE_KONTROL_REG (CTRL_BYTE_FIRST_PAGE + Y); // Select page
      WRITE_KONTROL_REG (CTRL_BYTE_FIRST_POS); // Select first byte
      for (X = 0; X < GuiConst_BYTES_PR_SECTION; X++) // Loop through bytes
        WRITE_DATA_REG (0x00);
    }
  }

  // Finished initializing, set display on
  for (ContrIndex = 1; ContrIndex <= 4; ContrIndex++)
  {
    ControllerSelect (ContrIndex);
    WRITE_KONTROL_REG (0x3F);
  }

  ControllerSelect (0);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S LineNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;
  GuiConst_INT8U ContrIndex;

  // Lock GUI resources
  GuiDisplay_Lock ();

  // Walk through all lines
  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++)
  {
    if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
    {
      // Something to redraw on this page
      for (ContrIndex = 1; ContrIndex <= CONTROLLER_COUNT_HORZ; ContrIndex++)
      {
        //  loop through all horizontal controllers
        if (GuiLib_DisplayRepaint[LineNo].ByteBegin <
            GuiConst_BYTES_PR_SECTION * ContrIndex)
        // Something to redraw in this section
        {
          if(LineNo < GuiConst_LINES_PR_SECTION)
          {
            // Select controller
            ControllerSelect(ContrIndex);
            // Select page
            WRITE_KONTROL_REG(CTRL_BYTE_FIRST_PAGE + LineNo);
          }
          else
          {
            // Select controller
            ControllerSelect(ContrIndex+2);
            // Select page
            WRITE_KONTROL_REG(
               CTRL_BYTE_FIRST_PAGE + LineNo - GuiConst_LINES_PR_SECTION);
          }
          // Select starting byte
          WRITE_KONTROL_REG(
             CTRL_BYTE_FIRST_POS + GuiLib_DisplayRepaint[LineNo].ByteBegin);
          // Write display data
          LastByte = (GuiConst_BYTES_PR_SECTION * ContrIndex) - 1;
          if (GuiLib_DisplayRepaint[LineNo].ByteEnd < LastByte)
            LastByte = GuiLib_DisplayRepaint[LineNo].ByteEnd;
          for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin; N <= LastByte; N++)
            WRITE_DATA_REG(GuiLib_DisplayBuf[LineNo][N]);

          // Reset repaint parameters
          if (GuiLib_DisplayRepaint[LineNo].ByteEnd >=
              GuiConst_BYTES_PR_SECTION*ContrIndex)
          // Something to redraw in next section
            GuiLib_DisplayRepaint[LineNo].ByteBegin =
               GuiConst_BYTES_PR_SECTION*ContrIndex;
          else // Done with this line
            GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
        }
      }
    }
  }

  // Finished drawing
  ControllerSelect (0);

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_HD61202_2H2V

#ifdef LCD_CONTROLLER_TYPE_HX8238_AT32AP7000

// ============================================================================
//
// HX8238 DISPLAY CONTROLLER + AT32AP7000 microcontroller
//
// This driver uses one AT32AP7000 microcontroller + one HX8238 display
// controller.
// LCD driver HX8238 in 24 bit RGB parallel interface.
// LCD driver of AT32AP7000 in 24 RGB parallel interface.
// Graphic modes up to 240x320 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 24 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// AT32AP7000 specific headers files
#include <avr32/io.h>
#include <string.h>

// Example for hardware connection for PowerTip PH320240T (driver HX8238)
// AT32AP7000 - PowerTip PH320240T (driver HX8238)
// NPCS0             -         SPENA
// SPCK              -         SPCLK
// MOSI              -         SPDAT
// VSYNC             -         VSYNC
// HSYNC             -         HSYNC
// PCLK              -         DOTCLK
// DVAL              -         ENB
// LCDD[0]-LCDD[7]   -         R0-R7
// LCDD[16]-LCDD[23] -         B0-B7
// LCDD[8]-LCDD[15]  -         G0-G7

//PORTC pin 1        -         RESET
// For different pin adjust PIN_MASK accordingly in hex format
#define PIN_MASK               0x00000001

// These defines should be adjusted to match the application
// CPU core speed in Hz
#define CPUHZ                  20000000
// Number of bits in each SPI package
#define SPI_BITS               8
// SPI master speed in Hz
#define SPI_MASTER_SPEED       125000
// Timeout for spi read and write blocking functions
#define SPI_TIMEOUT            10000

// HX8238 register access
#define INDEX_REGISTER         0
#define INSTRUCTION            1

// HX8238 registers definitions
#define DRIVER_OUTPUT_CONTROL  0x0001
#define LCD_AC_CONTROL         0x0002
#define POWER_CONTROL_1        0x0003
#define DATA_FILTER_CONTROL    0x0004
#define FUNCTION_CONTROL       0x0005
#define CONTRAST_CONTROL       0x000A
#define FRAME_CYCLE_CONTROL    0x000B
#define POWER_CONTROL_2        0x000D
#define POWER_CONTROL_3        0x000E
#define GATE_SCAN_START        0x000F
#define HORIZONTAL_PORCH       0x0016
#define VERTICAL_PORCH         0x0017
#define POWER_CONTROL_4        0x001E
#define STN_MONO 0
#define STN_COLOR 1
#define TFT 2

// LCD configuration parameters
#define SINGLE_SCAN            0
#define DUAL_SCAN              1

#define IF_WIDTH4              0
#define IF_WIDTH8              1
#define IF_WIDTH16             2

#define BIG_ENDIAN             0
#define LITTLE_ENDIAN          1
#define WIN_CE                 2

#define MODE_2D_ON             1
#define MODE_2D_OFF            0

#define BPP_1                  1
#define BPP_2                  2
#define BPP_4                  4
#define BPP_8                  8
#define BPP_16                 16
#define BPP_24                 24
#define BPP_32                 32

#define NORMAL                 0
#define INVERTED               1

#define ENABLED                1
#define DISABLED               0

#define PRE_NONE               0
#define PRE_HALF               1
#define PRE_FOURTH             2
#define PRE_EIGTH              3

#define PARTLY_ACTIVE          0
#define ALWAYS_ACTIVE          1

#define EACH_FRAME             0
#define MVAL_DEFINED           1

// Error codes used by SPI driver
enum {
  SPI_ERROR = -1,
  SPI_OK = 0,
  SPI_ERROR_TIMEOUT = 1,
  SPI_ERROR_ARGUMENT,
  SPI_ERROR_OVERRUN,
  SPI_ERROR_MODE_FAULT,
  SPI_ERROR_OVERRUN_AND_MODE_FAULT
};

// Parameters for setting options for slave chips
struct spi_options_t {
  // Which register to setup
  GuiConst_INT8U reg;
  // Prefered baudrate for the SPI
  GuiConst_INT32S baudrate;
  // Number of bits in each character (8 - 16)
  GuiConst_INT8U bits;
  // Delay before first clock pulse after selecting slave (in MCK)
  GuiConst_INT8U spck_delay;
  // Delay between each transfer/character (in MCK)
  GuiConst_INT8U trans_delay;
  // Set this chip to stay active after last transfer to it
  GuiConst_INT8U stay_act;
  // Which of the four SPI-modes to use when transmitting
  GuiConst_INT8U spi_mode;
  // Fdiv Downsample MCK by a factor of N before passing it to the baudrate
  // generator (will give downsample all communcation to 1/N of the speed,
  // and increase delays by a factor of N (N=32).
  GuiConst_INT8U fdiv;
  // Modfdis This bit disables the mode fault detection
  // (without this bit SPI in master mode will disable
  // itself if another master tries to address it)
  GuiConst_INT8U modfdis;
};

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Calculate the baudrate division
// param options Pointer to a spi_options_t struct
// param cpuHz the clock frequency in Hz for the peripheral bus
// retval >= 0 on success, retval  < 0 on error
GuiConst_INT32S getBaudDiv(struct spi_options_t * options, GuiConst_INT32S cpuHz)
{
  GuiConst_INT32S baudDiv = 0;

  if (options->fdiv == 0)
    baudDiv = cpuHz / (options->baudrate);
  else
    baudDiv = cpuHz / (32 * options->baudrate);

  if (baudDiv > (AVR32_SPI_CSR0_SCBR_MASK<<AVR32_SPI_CSR0_SCBR_OFFSET) ||
      baudDiv <= 0)
    return -SPI_ERROR_ARGUMENT;

  return baudDiv;
}

// Resets the SPI controller
// param spi Pointer to the correct avr32_spi_t struct
void spi_reset(volatile avr32_spi_t *spi)
{
  spi->cr = AVR32_SPI_CR_SWRST_MASK;
}

// Setup the SPI module in master mode
// param spi Pointer to the correct avr32_spi_t struct
// param options Pointer struct containing setup for a chip register
// retval SPI_OK on success
// retval SPI_ERROR_ARGUMENT when invalid arguments are passed
GuiConst_INT32S spi_initMaster(volatile avr32_spi_t *spi, struct spi_options_t *options)
{
  if (options->modfdis > 1 || options->fdiv > 1)
    return SPI_ERROR_ARGUMENT;

  // Reset
  spi->cr = AVR32_SPI_CR_SWRST_MASK;

  // Master Mode
  spi->mr = AVR32_SPI_MR_MSTR_MASK | AVR32_SPI_MR_PCS_MASK |
    (options->fdiv<<AVR32_SPI_MR_FDIV_OFFSET)|
    (options->modfdis<<AVR32_SPI_MR_MODFDIS_OFFSET);

  return SPI_OK;
}

// How and when are the slave chip selected (master mode only)
// param spi Pointer to the correct avr32_spi_t struct
// param variable_ps Target slave is selected in transfer register
// for every character to transmit
// param pcs_decode The four chip select lines are decoded externaly;
// values 0 to 14 can be given to select_chip()
// param delay Delay in MCK cyles (or NxMCK with FDIV) between chip selects
// retval SPI_OK on success
// retval SPI_ERROR_ARGUMENT when invalid arguments are passed
GuiConst_INT32S spi_selectionMode(volatile avr32_spi_t *spi,
   GuiConst_INT8U variable_ps,
   GuiConst_INT8U pcs_decode,
   GuiConst_INT8U delay)
{
  if (variable_ps > 1 || pcs_decode > 1)
    return SPI_ERROR_ARGUMENT;

  // Unset bitfields
  spi->mr &= ~(AVR32_SPI_MR_PS_MASK| AVR32_SPI_MR_PCSDEC_MASK |
     AVR32_SPI_MR_DLYBCS_MASK);
  // Set selction bits
  spi->mr |= (variable_ps << AVR32_SPI_MR_PS_OFFSET)|
     (pcs_decode << AVR32_SPI_MR_PCSDEC_OFFSET)|
     (delay << AVR32_SPI_MR_DLYBCS_OFFSET);

  return SPI_OK;
}

// Select slave chip
// param spi Pointer to the correct avr32_spi_t struct
// param chip Slave chip number (normal: 0-3, extarnaly decoded signal: 0-14)
// retval SPI_OK on success
// retval SPI_ERROR_ARGUMENT when invalid arguments are passed
GuiConst_INT32S spi_selectChip(volatile avr32_spi_t *spi, GuiConst_INT8U chip)
{
  // Assert all lines; no peripheral is selected
  spi->mr |= AVR32_SPI_MR_PCS_MASK;

  if ((spi->mr & AVR32_SPI_MR_PCSDEC_MASK) != 0)
  {
    // The signal is decoded; allow up to 15 chips
    if (chip > 14)
      return SPI_ERROR_ARGUMENT;

    spi->mr = (spi->mr & ~AVR32_SPI_MR_PCS_MASK)| (chip << AVR32_SPI_MR_PCS_OFFSET);
  }
  else
  {
    if (chip > 3)
      return SPI_ERROR_ARGUMENT;

    spi->mr &= ~(1<<(AVR32_SPI_MR_PCS_OFFSET + chip));
  }

  return SPI_OK;
}

// Set options for a specific slave chip. The baudrate field has to be written
// before transfer in master mode. Four similar registers exist,
// one for each slave. When using encoded slave addressing, reg=0 sets options
// for slave 0-3, reg=1 for slave 4-7 and so on.
// param spi Pointer to the correct avr32_spi_t struct
// param options Pointer struct containing setup for a chip register
// param cpuHz clock speed in Hz for the peripheral bus
// retval SPI_OK on success
// retval SPI_ERROR_ARGUMENT when invalid arguments are passed
GuiConst_INT32S spi_setupChipReg(volatile avr32_spi_t *spi,
                                 struct spi_options_t *options,
                                 GuiConst_INT32U cpuHz)
{
  GuiConst_INT32S baudDiv = -1;
  unsigned long csr = 0;

  if (options->bits > 16 || options->bits < 8 || options->stay_act > 1)
    return SPI_ERROR_ARGUMENT;

  baudDiv = getBaudDiv(options, cpuHz);

  if (baudDiv < 0)
    return -baudDiv;

  // Will use CSR0 offsets; these are the same for CSR0 - CSR3
  csr = ((options->bits - 8)<<AVR32_SPI_CSR0_BITS_OFFSET)|
     (baudDiv<<AVR32_SPI_CSR0_SCBR_OFFSET)|
     (options->spck_delay<<AVR32_SPI_CSR0_DLYBS_OFFSET)|
     (options->trans_delay<<AVR32_SPI_CSR0_DLYBCT_OFFSET)|
     (options->stay_act<<AVR32_SPI_CSR0_CSAAT_OFFSET);

  switch (options->spi_mode)
  {
    case 0:
      csr |= (1<<AVR32_SPI_CSR0_NCPHA_OFFSET);
    case 1:
      break;
    case 2:
      csr |= (1<<AVR32_SPI_CSR0_NCPHA_OFFSET);
    case 3:
      csr |= (1<<AVR32_SPI_CSR0_CPOL_OFFSET);
      break;
    default: // Not in legal range
      return SPI_ERROR_ARGUMENT;
  }

  switch (options->reg)
  {
    case 0:
      spi->csr0 = csr;
      break;
    case 1:
      spi->csr1 = csr;
      break;
    case 2:
      spi->csr2 = csr;
      break;
    case 3:
      spi->csr3 = csr;
      break;
    default:
      return SPI_ERROR_ARGUMENT;
  }

  return SPI_OK;
}

// Enables a disabled SPI
// param spi Pointer to the correct avr32_spi_t struct
void spi_enable(volatile avr32_spi_t *spi)
{
  spi->cr = AVR32_SPI_CR_SPIEN_MASK;
}

// Writes one block of data to the selected (fixed select) slave, will
// block program execution until timeout occurs if transmitter is busy
// param spi Pointer to the correct avr32_spi_t struct
// param data The block to write
// retval SPI_OK on success
// retval SPI_TIMEOUT on timeout
GuiConst_INT32S spi_write(volatile avr32_spi_t *spi, GuiConst_INT16U data)
{
  GuiConst_INT32U timeout = SPI_TIMEOUT;

  while ((spi->sr & AVR32_SPI_SR_TXEMPTY_MASK) == 0 && timeout > 0)
    --timeout;

  if (timeout == 0)
    return SPI_ERROR_TIMEOUT;

  spi->tdr = data & 0x0000FFFF;

  return SPI_OK;
}

// Map over PIO for setup of peripherals
typedef GuiConst_INT8U avr32_piomap_t[][2];

// Initialise SPI in master mode
// param spi Pointer to the correct avr32_spi_t struct
// param cpuHz CPU clock frequency in Hz
void init_spiMaster(volatile avr32_spi_t *spi, long cpuHz)
{
  struct spi_options_t spiOptions;

  spiOptions.reg = 0;
  spiOptions.baudrate = SPI_MASTER_SPEED;
  spiOptions.bits = SPI_BITS;
  spiOptions.spck_delay = 0;
  spiOptions.trans_delay = 4;
  spiOptions.stay_act = 0;
  spiOptions.spi_mode = 1;
  spiOptions.fdiv = 0;
  spiOptions.modfdis = 0;

  // Initialize as master
  spi_initMaster(spi, &spiOptions);

  // Set master mode; variable_ps, pcs_decode, delay
  spi_selectionMode(spi, 0, 0, 0);

  // Select slave chip 0 (SPI_NPCS0)
  spi_selectChip(spi, 0);

  spi_setupChipReg(spi, &spiOptions, cpuHz);

  spi_enable(spi);
}

// Set the pins under module control.
// param piomap a map describing how the setup of the PIO
// param size number of elements in the map
// retval SPI_OK on success
// retval SPI_ERROR_ARGUMENT on bad values in piomap
GuiConst_INT32S pio_enable_module(avr32_piomap_t piomap, GuiConst_INT32U size)
{
  GuiConst_INT32S i;
  volatile struct avr32_pio_t *pio;

  // Get the base address for the port
  switch(**piomap/32)
  {
    case 0:
      pio = &AVR32_PIOA;
      break;
    case 1:
      pio = &AVR32_PIOB;
      break;
    case 2:
      pio = &AVR32_PIOC;
      break;
    case 3:
      pio = &AVR32_PIOD;
      break;
    case 4:
      pio = &AVR32_PIOE;
      break;
    default :
      return SPI_ERROR_ARGUMENT;
  }

  for (i = 0; i < size; i++)
  {
    pio->pdr |= (1<<((**piomap) % 32));
    pio->pudr |= (1<<((**piomap) % 32));

    switch(*(*piomap+1))
    {
      case 0:
        pio->asr |= (1<<((**piomap) % 32));
        break;
      case 1:
        pio->bsr |= (1<<((**piomap) % 32));
        break;
      default:
        return SPI_ERROR_ARGUMENT;
    }
    ++piomap;
  }
  return SPI_OK;
}

// Send command or data to HX8238
void HX8238_SpiSend (GuiConst_INT8U RS, GuiConst_INT16U Data)
{
  volatile avr32_spi_t *spi = &AVR32_SPI0;

  if(RS)
    spi_write(spi,0x72);
  else
    spi_write(spi,0x70);

  spi_write(spi,(GuiConst_INT8U)(Data>>8));
  spi_write(spi,(GuiConst_INT8U)Data);
}

// Struct that defines the configuration of the LCD controller
typedef struct lcdc_configuration_s
{
  // Base address for the upper panel (in dual scan mode) or complete frame
  GuiConst_INT32U dmabaddr1;
  // Base address of lower panel (dual scan mode only
  GuiConst_INT32U dmabaddr2;
  // Burst length of DMA controller
  GuiConst_INT8U burst_length;
  // Number of columns on the display (in pixels)
  GuiConst_INT16U xres;
  // Number of rows on the display (in pixels)
  GuiConst_INT16U yres;

  // Enables or disables the 2D addressing mode
  // If 2D addressing is activated the values in \ref virtual_xres and \ref virtual_yres
  // must be set according to the virtual frame size.
  // arg MODE_2D_ON
  // arg MODE_2D_OFF
  GuiConst_INT8U set2dmode;

  // Virtual horizontal size of the display (in pixels)
  // Use this in 2D addressing mode to set the size of the frame buffer
  GuiConst_INT32U virtual_xres;

  // Virtual vertical size of the display (in pixels)
  // Use this value in 2D addressing mode to set the size of the frame buffer
  GuiConst_INT32U virtual_yres;

  // Frame rate of the display
  GuiConst_INT16U frame_rate;

  // LCD controller clock
  // Frequency in MHz at which the LCD module runs.
  // This can be set in the generic clock setup.
  GuiConst_INT32U lcdcclock;

  // Delay in frame periods between applying control signals to the LCD module and setting PWR high, and between setting PWR low and removing control signals from LCD module */
  GuiConst_INT16U guard_time;

  // LCD configuration register 2 - Memory organization
  // arg BIG_ENDIAN
  // arg LITTLE_ENDIAN
  // arg WIN_CE
  GuiConst_INT8U memor;

  // Interface width (only valid for STN mode)
  // arg IF_WIDTH4 (only valid in STN single scan mode)
  // arg IF_WIDTH8
  // arg IF_WIDTH16 (only valid in dual scan mode)
  GuiConst_INT8U ifwidth;

  // Scan mode
  // arg SINGLE_SCAN
  // arg DUAL_SCAN
  GuiConst_INT8U scanmod;

  // Display type
  // arg STN_MONO
  // arg STN_COLOR
  // arg TFT
  GuiConst_INT8U distype;

  // Data polarity
  // arg NORMAL
  // arg INVERTED
  GuiConst_INT8U invvd;

  // Vertical sync polarity
  // arg NORMAL (active high)
  // arg INVERTED (active low)
  GuiConst_INT8U invframe;

  // Horizontal sync polarity
  // arg NORMAL (active high)
  // arg INVERTED (active low)
  GuiConst_INT8U invline;

  // Pixel clock polarity
  // arg NORMAL (data fetched at falling edge)
  // arg INVERTED (data fetched at rising edge)
  GuiConst_INT8U invclk;

  // Data valid polarity
  // arg NORMAL (active high)
  // arg INVERTED (active low)
  GuiConst_INT8U invdval;

  // Pixel clock mode
  // arg PARTLY_ACTIVE (only active during display period)
  // arg ALWAYS_ACTIVE (needed for TFT mode)
  GuiConst_INT8U clkmod;

  // Bits per pixel
  // arg BPP_1
  // arg BPP_2
  // arg BPP_4
  // arg BPP_8
  // arg BPP_16
  // arg BPP_24 (packed 24bpp)
  // arg BPP_32 (unpacked 24bpp)
  GuiConst_INT8U pixelsize;

  // Contrast value
  // PWM compare value used to adjust the analog value obtained after an external
  // filter to control the contrast of the display.
  GuiConst_INT8U ctrstval;

  // Contrast PWM prescaler
  // This Prescaler divides the LCD controller clock for the contrast PWM generator
  // arg PRE_NONE No prescaling
  // arg PRE_HALF LCD controller clock divided by 1/2
  // arg PRE_FOURTH LCD controller clock divided by 1/4
  // arg PRE_EIGTH LCD controller clock divided by 1/8
  GuiConst_INT8U ctrst_ps;

  // Contrast PWM polarity
  // This value defines the polarity of the contrast PWM output. If NORMAL, the output pulses are high level
  // (the output will be high whenever the value in the counter is less than the value in
  // the compare register CONTRAST_VAL \ref ctrstval ). If INVERTED, the output pulses are low level.
  // arg NORMAL
  // arg INVERTED
  GuiConst_INT8U ctrst_pol;

  // Contrast PWM generator enable
  // arg ENABLED
  // arg DISABLED
  GuiConst_INT8U ctrst_ena;

  // Toggle rate
  // Toggle the polarity after each frame (EACH_FRAME) or by a specified value (MVAL_DEFINED).
  // arg EACH_FRAME
  // arg MVAL_DEFINED
  GuiConst_INT8U mmode;

  // Toggle rate value.
  // If Toggle rate is set to MVAL_DEFINED this value sets toggle rate to mval + 1 line periods.
  // arg MVAL_DEFINED
  // arg EACH_FRAME
  GuiConst_INT8U mval;

  // Horizontal sync pulse width
  // Width of the HSYNC pulse, given in pixel clock cycles. Width is (HPW+1) PCLK cycles.
  GuiConst_INT16U hpw;

  // Horizontal back porch
  // Number of idle pixel clock cycles at the beginning of the line. Idle period is (HBP+1) pixel clock cycles.
  GuiConst_INT8U hbp; // Wait before of line

  // Horizontal front porch
  // Number of idle pixel clock cycles at the end of the line. Idle period is (HFP+1) pixel clock cycles.
  GuiConst_INT8U hfp; // Wait end of line

  // Vertical sync pulse width
  // In TFT mode, these bits equal the vertical synchronization pulse width, given in number of lines.
  // VSYNC width is equal to (VPW+1) lines. In STN mode, these bits should be set to 0.
  GuiConst_INT8U vpw;

  // Vertical back porch
  // In TFT mode, these bits equal the number of idle lines at the beginning of the frame. In STN mode, these bits should be set to 0.
  GuiConst_INT8U vbp;   // Wait before of frame

  // Vertical front porch
  // In TFT mode, these bits equal the number of idle lines at the end of the frame. In STN mode, these bits should be set to 0.
  GuiConst_INT8U vfp;   // Wait end of frame

  // Vertical to horizontal delay
  // In TFT mode, this is the delay between VSYNC rising or falling edge and HSYNC rising edge.
  // Delay is (VHDLY+1) pixel clock cycles. In STN mode, these bits should be set to 0.
  GuiConst_INT8U vhdly;

} lcdc_conf_t;

// Configures the LCD module
// lcdc_conf Pointer to LCD controller configuration structure
// retval 0 = success, -1 = invalid argument
GuiConst_INT32S lcdc_init(lcdc_conf_t* lcdc_conf)
{
  volatile avr32_lcdc_t *plcdc = &AVR32_LCDC;
  GuiConst_INT8U valid_data_lines = 0;
  GuiConst_INT8U pixel_size = 0;
  GuiConst_INT8U clkval = 0;
  GuiConst_INT32U pixel_clock_theo = 0;
  GuiConst_INT16U lineval, hozval;

  // Turn off LCD Controller (core first then DMA)
  plcdc->pwrcon &= ~(1 << AVR32_LCDC_PWRCON_PWR_OFFSET);
  plcdc->dmacon &= ~(1 << AVR32_LCDC_DMACON_DMAEN_OFFSET);

  // LCDFRCFG
  if (lcdc_conf->distype != TFT)
  {
    switch (lcdc_conf->scanmod)
    {
      case SINGLE_SCAN:
        switch (lcdc_conf->ifwidth)
        {
          case IF_WIDTH4:
            valid_data_lines = 4;
            break;
          case IF_WIDTH8:
            valid_data_lines = 8;
            break;
          default:
            return -1;
        }

      case DUAL_SCAN:
        switch (lcdc_conf->ifwidth)
        {
          case IF_WIDTH8:
            valid_data_lines = 4;
            break;
          case IF_WIDTH16:
            valid_data_lines = 8;
            break;
          default:
            return -1;
        }
      default:
        return -1;
    }
  }

  lineval = lcdc_conf->yres - 1;
  switch (lcdc_conf->distype)
  {
    case STN_MONO:
      hozval = (lcdc_conf->xres / valid_data_lines) - 1;
      break;
    case STN_COLOR:
      hozval = (lcdc_conf->xres * 3 / valid_data_lines) - 1;
      break;
    case TFT:
      hozval = lcdc_conf->xres - 1;
      break;
    default:
      return -1;
  }
  plcdc->lcdfrmcfg = (lineval & AVR32_LCDC_LINEVAL_MASK) |
     ((hozval << AVR32_LCDC_HOZVAL) & AVR32_LCDC_HOZVAL_MASK);

  // Calculation of theoretical pixel clock
  switch (lcdc_conf->distype)
  {
    case STN_MONO:
      pixel_clock_theo = lcdc_conf->frame_rate * lcdc_conf->xres * lcdc_conf->yres / valid_data_lines;
      break;
    case STN_COLOR:
      pixel_clock_theo = lcdc_conf->frame_rate * lcdc_conf->xres * lcdc_conf->yres * 3 / valid_data_lines;
      break;
    case TFT:
      pixel_clock_theo = lcdc_conf->frame_rate * lcdc_conf->xres * lcdc_conf->yres;
      break;
    default:
      return -1;
  }
  clkval = (lcdc_conf->lcdcclock / (2 * pixel_clock_theo));
  if (clkval == 0)
    plcdc->lcdcon1 = 1; // bypass pixel clock
  else
    plcdc->lcdcon1 = ((clkval - 1) << AVR32_LCDC_LCDCON1_CLKVAL) & AVR32_LCDC_LCDCON1_CLKVAL_MASK;

  // LCDCON2
  switch(lcdc_conf->pixelsize)
  {
    case 1:  pixel_size = 0;break;
    case 2:  pixel_size = 1;break;
    case 4:  pixel_size = 2;break;
    case 8:  pixel_size = 3;break;
    case 16: pixel_size = 4;break;
    case 24: pixel_size = 5;break;
    case 32: pixel_size = 6;break;
    default: return -1;
  }
  plcdc->lcdcon2 = (lcdc_conf->distype & AVR32_LCDC_LCDCON2_DISTYPE_MASK) |
     ((lcdc_conf->scanmod << AVR32_LCDC_LCDCON2_SCANMOD) &
     AVR32_LCDC_LCDCON2_SCANMOD_MASK) |
     ((lcdc_conf->ifwidth << AVR32_LCDC_LCDCON2_IFWIDTH) &
     AVR32_LCDC_LCDCON2_IFWIDTH_MASK) |
     ((lcdc_conf->invvd << AVR32_LCDC_LCDCON2_INVVD) &
     AVR32_LCDC_LCDCON2_INVVD_MASK) |
     ((lcdc_conf->invframe << AVR32_LCDC_LCDCON2_INVFRAME) &
     AVR32_LCDC_LCDCON2_INVFRAME_MASK) |
     ((pixel_size << AVR32_LCDC_LCDCON2_PIXELSIZE) &
     AVR32_LCDC_LCDCON2_PIXELSIZE_MASK) |
     ((lcdc_conf->invline << AVR32_LCDC_LCDCON2_INVLINE) &
     AVR32_LCDC_LCDCON2_INVLINE_MASK) |
     ((lcdc_conf->invclk << AVR32_LCDC_LCDCON2_INVCLK) &
     AVR32_LCDC_LCDCON2_INVCLK_MASK) |
     ((lcdc_conf->invdval << AVR32_LCDC_LCDCON2_INVDVAL) &
     AVR32_LCDC_LCDCON2_INVDVAL_MASK) |
     ((lcdc_conf->clkmod << AVR32_LCDC_LCDCON2_CLKMOD) &
     AVR32_LCDC_LCDCON2_CLKMOD_MASK) |
     ((lcdc_conf->memor << AVR32_LCDC_LCDCON2_MEMOR) &
     AVR32_LCDC_LCDCON2_MEMOR_MASK);

  // Timings
  plcdc->lcdtim1 = (lcdc_conf->vfp & AVR32_LCDC_LCDTIM1_VFP_MASK) |
     ((lcdc_conf->vbp << AVR32_LCDC_LCDTIM1_VBP) & AVR32_LCDC_LCDTIM1_VBP_MASK) |
     (((lcdc_conf->vpw - 1) << AVR32_LCDC_LCDTIM1_VPW) &
     AVR32_LCDC_LCDTIM1_VPW_MASK) |
     ((lcdc_conf->vhdly << AVR32_LCDC_LCDTIM1_VHDLY) &
     AVR32_LCDC_LCDTIM1_VHDLY_MASK);

  plcdc->lcdtim2 = (lcdc_conf->hbp & AVR32_LCDC_HBP_MASK) |
     (((lcdc_conf->hpw - 1) << AVR32_LCDC_LCDTIM2_HPW) &
     AVR32_LCDC_LCDTIM2_HPW_MASK) |
     ((lcdc_conf->hfp << AVR32_LCDC_LCDTIM2_HFP) & AVR32_LCDC_LCDTIM2_HFP_MASK);

  // Interrupts
  plcdc->idr = 0xFFFFFFFF;

  // Toggle rate
  plcdc->lcdmval = (lcdc_conf->mval & AVR32_LCDC_LCDMVAL_MVAL_MASK) |
     ((lcdc_conf->mmode << AVR32_LCDC_LCDMVAL_MMODE_OFFSET) &
     AVR32_LCDC_LCDMVAL_MMODE_MASK);

  // Contrast
  plcdc->contrast_val = lcdc_conf->ctrstval;
  plcdc->contrast_ctr =
     (lcdc_conf->ctrst_ps & AVR32_LCDC_CONTRAST_CTR_PS_MASK) |
     ((lcdc_conf->ctrst_pol << AVR32_LCDC_CONTRAST_CTR_POL_OFFSET) &
     AVR32_LCDC_CONTRAST_CTR_POL_MASK) |
     ((lcdc_conf->ctrst_ena << AVR32_LCDC_CONTRAST_CTR_ENA_OFFSET) &
     AVR32_LCDC_CONTRAST_CTR_ENA_MASK);

  // Setup FIFO
  GuiConst_INT32S lcd_fifo_size = lcdc_conf->scanmod ? 256 : 512;
  plcdc->lcdfifo = lcd_fifo_size - (2 * lcdc_conf->burst_length + 3);

  // DMA base address
  plcdc->dmabaddr1 = lcdc_conf->dmabaddr1;
  if(lcdc_conf->scanmod == DUAL_SCAN)
    plcdc->dmabaddr2 = lcdc_conf->dmabaddr2;

  // DMA frame configuration, The frame size is measured in words
  plcdc->dmafrmcfg =
     ((((lcdc_conf->xres * lcdc_conf->yres * lcdc_conf->pixelsize) + 31 )/ 32) &
     AVR32_LCDC_DMAFRMCFG_FRMSIZE_MASK) |
     (((lcdc_conf->burst_length - 1) << AVR32_LCDC_DMAFRMCFG_BRSTLEN) &
     AVR32_LCDC_DMAFRMCFG_BRSTLEN_MASK);

  // 2D configuration
  if(lcdc_conf->set2dmode)
    // Assumed is that the frame starts at a word boundary -> no pixel offset
    // needed
    plcdc->dma2dcfg = ((lcdc_conf->virtual_xres - lcdc_conf->xres) *
       (lcdc_conf->pixelsize / 8)) & AVR32_LCDC_DMA2DCFG_ADDRINC_MASK;

  // Wait for DMA engine to become idle
 while (plcdc->dmacon & AVR32_LCDC_DMACON_DMABUSY);

  // And enable DMA with updated configuration
  if(lcdc_conf->set2dmode)
    plcdc->dmacon = (1 << AVR32_LCDC_DMACON_DMAEN_OFFSET) |
       (1 << AVR32_LCDC_DMACON_DMAUPDT_OFFSET) |
       (1 << AVR32_LCDC_DMACON_DMA2DEN_OFFSET);
  else
    plcdc->dmacon = (1 << AVR32_LCDC_DMACON_DMAEN_OFFSET) |
       (1 << AVR32_LCDC_DMACON_DMAUPDT_OFFSET);

  // Enable LCD
  plcdc->pwrcon |= (lcdc_conf->guard_time <<
     AVR32_LCDC_PWRCON_GUARD_TIME_OFFSET) & AVR32_LCDC_PWRCON_GUARD_TIME_MASK;

  // Wait for the LCDC core to become idle and enable it
  while (plcdc->PWRCON.busy == 1);
  plcdc->PWRCON.pwr = 1;

  return 0;
}

// LCD controller configuration for PowerTip PH320240T
lcdc_conf_t ph320240t_conf = {
  dmabaddr1: 0x10000000,
  dmabaddr2: 0,
  burst_length: 4,
  xres: 320,
  yres: 240,
  set2dmode: MODE_2D_OFF,
  virtual_xres: 320,
  virtual_yres: 240,
  frame_rate: 75,
  lcdcclock: 40000000,
  guard_time: 2,
  memor: BIG_ENDIAN,
  ifwidth: 0,
  scanmod: SINGLE_SCAN,
  distype: TFT,
  invvd: INVERTED,
  invframe: INVERTED,
  invline: INVERTED,
  invclk: INVERTED,
  invdval: INVERTED,
  clkmod: ALWAYS_ACTIVE,
  pixelsize: BPP_24,
  ctrstval: 0x0f,
  ctrst_ena: ENABLED,
  ctrst_pol: NORMAL,
  ctrst_ps: PRE_HALF,
  mval: 0,
  mmode: EACH_FRAME,
  hpw: 16,
  hbp: 15,
  hfp: 33,
  vpw: 1,
  vbp: 10,
  vfp: 10,
  vhdly: 0,
};

// Sets up the pins for the LCD
void lcd_pio_config(void)
{
  avr32_piomap_t piomap = {
     { AVR32_LCDC_CC_0_0_PIN, AVR32_LCDC_CC_0_0_FUNCTION },
     { AVR32_LCDC_DVAL_0_0_PIN, AVR32_LCDC_DVAL_0_0_FUNCTION },
     { AVR32_LCDC_HSYNC_0_PIN, AVR32_LCDC_HSYNC_0_FUNCTION },
     { AVR32_LCDC_MODE_0_0_PIN, AVR32_LCDC_MODE_0_0_FUNCTION },
     { AVR32_LCDC_PCLK_0_PIN, AVR32_LCDC_PCLK_0_FUNCTION },
     { AVR32_LCDC_PWR_0_PIN, AVR32_LCDC_PWR_0_FUNCTION },
     { AVR32_LCDC_VSYNC_0_PIN, AVR32_LCDC_VSYNC_0_FUNCTION },
     { AVR32_LCDC_DATA_0_0_PIN, AVR32_LCDC_DATA_0_0_FUNCTION },
     { AVR32_LCDC_DATA_1_0_PIN, AVR32_LCDC_DATA_1_0_FUNCTION },
     { AVR32_LCDC_DATA_2_0_PIN, AVR32_LCDC_DATA_1_0_FUNCTION },
     { AVR32_LCDC_DATA_3_0_PIN, AVR32_LCDC_DATA_1_0_FUNCTION },
     { AVR32_LCDC_DATA_4_0_PIN, AVR32_LCDC_DATA_1_0_FUNCTION },
     { AVR32_LCDC_DATA_5_PIN, AVR32_LCDC_DATA_5_FUNCTION },
     { AVR32_LCDC_DATA_6_PIN, AVR32_LCDC_DATA_6_FUNCTION },
     { AVR32_LCDC_DATA_7_PIN, AVR32_LCDC_DATA_7_FUNCTION },
     { AVR32_LCDC_DATA_8_0_PIN, AVR32_LCDC_DATA_8_0_FUNCTION },
     { AVR32_LCDC_DATA_9_0_PIN, AVR32_LCDC_DATA_9_0_FUNCTION },
     { AVR32_LCDC_DATA_10_0_PIN, AVR32_LCDC_DATA_10_0_FUNCTION },
     { AVR32_LCDC_DATA_11_0_PIN, AVR32_LCDC_DATA_11_0_FUNCTION },
     { AVR32_LCDC_DATA_12_0_PIN, AVR32_LCDC_DATA_12_0_FUNCTION },
     { AVR32_LCDC_DATA_13_PIN, AVR32_LCDC_DATA_13_FUNCTION },
     { AVR32_LCDC_DATA_14_PIN, AVR32_LCDC_DATA_14_FUNCTION },
     { AVR32_LCDC_DATA_15_PIN, AVR32_LCDC_DATA_15_FUNCTION },
     { AVR32_LCDC_DATA_16_0_PIN, AVR32_LCDC_DATA_16_0_FUNCTION },
     { AVR32_LCDC_DATA_17_0_PIN, AVR32_LCDC_DATA_17_0_FUNCTION },
     { AVR32_LCDC_DATA_18_0_PIN, AVR32_LCDC_DATA_18_0_FUNCTION },
     { AVR32_LCDC_DATA_19_0_PIN, AVR32_LCDC_DATA_19_0_FUNCTION },
     { AVR32_LCDC_DATA_20_0_PIN, AVR32_LCDC_DATA_20_0_FUNCTION },
     { AVR32_LCDC_DATA_21_0_PIN, AVR32_LCDC_DATA_21_0_FUNCTION },
     { AVR32_LCDC_DATA_22_PIN, AVR32_LCDC_DATA_22_FUNCTION },
     { AVR32_LCDC_DATA_23_PIN, AVR32_LCDC_DATA_23_FUNCTION }
  };
  pio_enable_module(piomap, 31);
}

// Initialises the modules and the display
void GuiDisplay_Init(void)
{
  // Set up a pio structure to point at PIOC's base address
  volatile avr32_pio_t *pioc = &AVR32_PIOC;
  // Set up a spi structure to point at SPI's base address
  volatile avr32_spi_t *spi = &AVR32_SPI0;
  volatile avr32_pm_t *ppm = &AVR32_PM;

  avr32_piomap_t spi_piomap = {     \
     {AVR32_SPI0_SCK_0_PIN, AVR32_SPI0_SCK_0_FUNCTION},  \
     {AVR32_SPI0_MISO_0_PIN, AVR32_SPI0_MISO_0_FUNCTION}, \
     {AVR32_SPI0_MOSI_0_PIN, AVR32_SPI0_MOSI_0_FUNCTION}, \
     {AVR32_SPI0_NPCS_0_PIN, AVR32_SPI0_NPCS_0_FUNCTION}, \
     {AVR32_SPI0_NPCS_1_PIN, AVR32_SPI0_NPCS_1_FUNCTION}, \
     {AVR32_SPI0_NPCS_2_PIN, AVR32_SPI0_NPCS_2_FUNCTION}, \
     {AVR32_SPI0_NPCS_3_PIN, AVR32_SPI0_NPCS_3_FUNCTION}, \
  };

  // Init PIO
  pio_enable_module(spi_piomap, 7);
  // Init SPI interface in master mode
  init_spiMaster(spi, CPUHZ);

  // Reset HX8238
  pioc->per = PIN_MASK;
  pioc->oer = PIN_MASK;
  pioc->ower = PIN_MASK;
  pioc->codr = PIN_MASK;
  millisecond_delay(10);
  pioc->sodr = PIN_MASK;

  // HX8238 Initialization , example for PowerTip PH320240T
  HX8238_SpiSend (INDEX_REGISTER,DRIVER_OUTPUT_CONTROL);
  HX8238_SpiSend (INSTRUCTION,0x7300);

  HX8238_SpiSend (INDEX_REGISTER,LCD_AC_CONTROL);
  HX8238_SpiSend (INSTRUCTION,0x0200);

  HX8238_SpiSend (INDEX_REGISTER,POWER_CONTROL_1);
  HX8238_SpiSend (INSTRUCTION,0x6364);

  HX8238_SpiSend (INDEX_REGISTER,DATA_FILTER_CONTROL);
  HX8238_SpiSend (INSTRUCTION,0x04C7);

  HX8238_SpiSend (INDEX_REGISTER,FUNCTION_CONTROL);
  HX8238_SpiSend (INSTRUCTION,0xFC80);

  HX8238_SpiSend (INDEX_REGISTER,CONTRAST_CONTROL);
  HX8238_SpiSend (INSTRUCTION,0x4008);

  HX8238_SpiSend (INDEX_REGISTER,POWER_CONTROL_2);
  HX8238_SpiSend (INSTRUCTION,0x3229);

  HX8238_SpiSend (INDEX_REGISTER,POWER_CONTROL_3);
  HX8238_SpiSend (INSTRUCTION,0x3200);

  HX8238_SpiSend (INDEX_REGISTER,POWER_CONTROL_4);
  HX8238_SpiSend (INSTRUCTION,0x00D2);

  // AT32AP7000 LCD driver initialization
  lcd_pio_config();
  // Power manager setup
  // Enable CLOCK for LCDC in HSBMASK
  ppm->hsb_mask |= 0x00000080;
  // Enable generic clock PLL0 for LCD controller pixel clock
  ppm->gcctrl[7] |= 0x00000006;
  // Initialization of lcd controller with data for PowewrTip PH320240T
  lcdc_init(&ph320240t_conf);

  // Clear framebuffer
  memset((void *)ph320240t_conf.dmabaddr1, 0, GuiConst_DISPLAY_BYTES);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT32U i, Address;
  GuiConst_INT16S X, Y, LastByte;
  GuiConst_INT8U *framePtr;

  GuiDisplay_Lock ();

/*
  // Simple solution
  GuiConst_INT8U *displaybuffer;

  // load address of GuiLib_DisplayBuf
  displaybuffer = (GuiConst_INT8U *)GuiLib_DisplayBuf;

  // Set pointer to non-cached framebuffer address, pixel size is packed 24 bit.
  framePtr = (GuiConst_INT8U *) (ph320240t_conf.dmabaddr1 | 0xA0000000);

  // Load framebuffer with data from easyGui display buffer
  for (i = 0; i < GuiConst_DISPLAY_BYTES; i++)
    *framePtr++ = *displaybuffer++;
*/

  // Set pointer to non-cached framebuffer address
  // Framebuffer pixel size is packed 24Bit.
  framePtr = (GuiConst_INT8U *) (ph320240t_conf.dmabaddr1 | 0xA0000000);

  // Standard easyGui solution
  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Pointer offset calculation
      Address =
         Y * GuiConst_BYTES_PR_LINE + GuiLib_DisplayRepaint[Y].ByteBegin * 3;

      // Set amount of data to be changed
      LastByte = GuiConst_BYTES_PR_LINE/3 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      i = 0;

      // Write data for one line to framebuffer from GuiLib_DisplayBuf
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin * 3;
               X <= (LastByte * 3 + 2);
               X++)
      {
        *(framePtr + Address + i) = GuiLib_DisplayBuf[Y][X];
        i++;
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_HX8238_AT32AP7000

#ifdef LCD_CONTROLLER_TYPE_HX8238_TMP92CH21

// ============================================================================
//
// HX8238 + TMP92CH21 DISPLAY CONTROLLER
//
// This driver uses one TMP92CH21 microcontroller + one HX8238 display
// controller.
// LCD driver HX8238 in 24 bit RGB parallel interface.
// LCD driver of TMP92CH21 in 8 RGB parallel interface.
// Graphic modes up to 240x320 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 8 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardware settings for HX8238
// CM = 0, 262k color mode
// RL = 1, first S0-2
// TB = 1, normal scan
// BGR = 0, RGB order
// REV = 0, minumum voltage at black
// SWD0 = 1, SWD1 = 1, SWD2 = 1,
// SEL0 = 0, SEL1 = 0, SEL2 = 0,
// CPE = 1, enable internal charge pump
// PINV = 1, in polarity in phase with VCOM
// REGVDD = 1, System Vdd > 2.5V
// SHUT = 0, normal operation
// RR0-RR4 = 1
// GG0-GG4 = 1
// BB0-BB5 = 1

// Hardware connection between
// TMP92CH21 and HX8238
// 8 RGB data
// LD0       -   RR5
// LD1       -   RR6
// LD2       -   RR7
// LD3       -   GG5
// LD4       -   GG6
// LD5       -   GG7
// LD6       -   BB6
// LD7       -   BB7
// TFT interface control signal
// LBCD      -   VSYNC
// LCP1      -   HSYNC
// LCP0      -   DOTCLK
// LLP       -   DEN

// Bit handling macros
#define SETBIT(x,y)    (x |= (y))
#define CLRBIT(x,y)    (x &= (~(y)))
#define CHKBIT(x,y)    (x & (y))
#define FLPBIT(x,y)    (x ^= (y))

// HX8238 register access
#define INDEX_REGISTER 0
#define INSTRUCTION    1

// TMP92CH21 ports
#ifndef _IO29CH21_H
#define _IO29CH21_H

#define IOP1 *((IOBPTR)0x04)  // Port 1
#define P1CR *((IOBPTR)0x06)  // P1 control
#define P1FC *((IOBPTR)0x07)  // P1 function

#define IOP2 *((IOBPTR)0x08)  // Port 2
#define P2FC2 *((IOBPTR)0x09)  // P2 function 2
#define P2CR *((IOBPTR)0x0A)  // P2 control
#define P2FC *((IOBPTR)0x0B)  // P2 function

#define IOP3 *((IOBPTR)0x0C)  // Port 3
#define P3CR *((IOBPTR)0x0E)  // P3 control
#define P3FC *((IOBPTR)0x0F)  // P3 function

#define IOP4 *((IOBPTR)0x10)  // Port 4
#define P4FC *((IOBPTR)0x13)  // P4 function

#define IOP5 *((IOBPTR)0x14)  // Port 5
#define P5FC *((IOBPTR)0x17)  // P5 function

#define IOP6 *((IOBPTR)0x18)  // Port 6
#define P6CR *((IOBPTR)0x1A)  // P6 control
#define P6FC *((IOBPTR)0x1B)  // P6 function

#define IOP7 *((IOBPTR)0x1C)  // Port 7
#define P7CR *((IOBPTR)0x1E)  // P7 control
#define P7FC *((IOBPTR)0x1F)  // P7 function

#define IOP8 *((IOBPTR)0x20)  // Port 8
#define P8FC2 *((IOBPTR)0x21)  // P8 function 2
#define P8FC *((IOBPTR)0x23)  // P8 function

#define IOP9 *((IOBPTR)0x24)  // Port 9
#define P9FC2 *((IOBPTR)0x25)  // P9 function 2
#define P9CR *((IOBPTR)0x26)  // P9 control
#define P9FC *((IOBPTR)0x27)  // P9 function

#define IOPA *((IOBPTR)0x28)  // Port A
#define PACR *((IOBPTR)0x2A)  // PA control
#define PAFC *((IOBPTR)0x2B)  // PA function

#define IOPC *((IOBPTR)0x30)  // Port C
#define PCCR *((IOBPTR)0x32)  // PC control
#define PCFC *((IOBPTR)0x33)  // PC function

#define IOPF *((IOBPTR)0x3C)  // Port F
#define PFFC2 *((IOBPTR)0x3D)  // PF function 2
#define PFCR *((IOBPTR)0x3E)  // PF control
#define PFFC *((IOBPTR)0x3F)  // PF function

#define IOPG *((IOBPTR)0x40)  // Port G

#define IOPJ *((IOBPTR)0x4C)  // Port J
#define PJCR *((IOBPTR)0x4E)  // PJ control
#define PJFC *((IOBPTR)0x4F)  // PJ function

#define IOPK *((IOBPTR)0x50)  // Port K
#define PKFC *((IOBPTR)0x53)  // PK function

#define IOPL *((IOBPTR)0x54)  // Port L
#define PLCR *((IOBPTR)0x56)  // PL control
#define PLFC *((IOBPTR)0x57)  // PL function

#define IOPM *((IOBPTR)0x58)  // Port M
#define PMFC *((IOBPTR)0x5B)  // PM function

// TMP92CH21 LCD Controller registers
#define LCDMODE0 *((IOBPTR)0x280) // LCD MODE 0 register
#define LCDMODE1 *((IOBPTR)0x281) // LCD MODE 1 register
#define LCDFFP   *((IOBPTR)0x282) // LCD Frame frequency register
#define LCDDVM   *((IOBPTR)0x283) // LCD Divide FRM register
#define LCDSIZE  *((IOBPTR)0x284) // LCD SIZE register
#define LCDCTL0  *((IOBPTR)0x285) // LCD Control 0 register
#define LCDCTL1  *((IOBPTR)0x286) // LCD Control 1 register
#define LCDSCC   *((IOBPTR)0x287) // LCD Source Clock Counter register
#define LCDCCR0  *((IOBPTR)0x288) // LCD Clock Counter register 0
#define LCDCCR1  *((IOBPTR)0x289) // LCD Clock Counter register 1
#define LCDCCR2  *((IOBPTR)0x28A) // LCD Clock Counter register 2
#define LCDRP10  *((IOBPTR)0x291) // LCD Red palette register 1/0
#define LCDRP32  *((IOBPTR)0x292) // LCD Red palette register 3/2
#define LCDRP54  *((IOBPTR)0x293) // LCD Red palette register 5/4
#define LCDRP76  *((IOBPTR)0x294) // LCD Red palette register 7/6
#define LCDGP10  *((IOBPTR)0x295) // LCD Green palette register 1/0
#define LCDGP32  *((IOBPTR)0x296) // LCD Green palette register 3/2
#define LCDGP54  *((IOBPTR)0x297) // LCD Green palette register 5/4
#define LCDGP76  *((IOBPTR)0x298) // LCD Green palette register 7/6
#define LCDBP10  *((IOBPTR)0x299) // LCD Blue palette register 1/0
#define LCDBP32  *((IOBPTR)0x29A) // LCD Blue palette register 3/2
#define LSARAL   *((IOBPTR)0x2A0) // LCD start Address register A-Area(L)
#define LSARAM   *((IOBPTR)0x2A1) // LCD start Address register A-Area(M)
#define LSARAH   *((IOBPTR)0x2A2) // LCD start Address register A-Area(H)
#define CMNAL    *((IOBPTR)0x2A3) // LCD Common Number register A-Area(L)
#define CMNAH    *((IOBPTR)0x2A4) // LCD Common Number register A-Area(H)
#define LSARBL   *((IOBPTR)0x2A6) // LCD start Address register B-Area(L)
#define LSARBM   *((IOBPTR)0x2A7) // LCD start Address register B-Area(M)
#define LSARBH   *((IOBPTR)0x2A8) // LCD start Address register B-Area(H)
#define CMNBL    *((IOBPTR)0x2A9) // LCD Common Number register B-Area(L)
#define CMNBH    *((IOBPTR)0x2AA) // LCD Common Number register B-Area(H)
#define LSARCL   *((IOBPTR)0x2AC) // LCD start Address register C-Area(L)
#define LSARCM   *((IOBPTR)0x2AD) // LCD start Address register C-Area(M)
#define LSARCH   *((IOBPTR)0x2AE) // LCD start Address register C-Area(H)

#endif

// Allocations for SPI signals at 8 bits port
// Fill actual pin position on the port and port name, also enable port as an output if needed
#define CSB        1<<
#define SCK        1<<
#define SDI        1<<
#define SPIPORT

// Allocation for RESB pin at 8 bit port
// Fill actual pin position on the port and port name, also enable port as an output if needed
#define RESB       1<<
#define RESBPORT

// HX8238 registers definitions
#define DRIVER_OUTPUT_CONTROL  0x0001
#define LCD_AC_CONTROL         0x0002
#define POWER_CONTROL_1        0x0003
#define DATA_FILTER_CONTROL    0x0004
#define FUNCTION_CONTROL       0x0005
#define CONTRAST_CONTROL       0x000A
#define FRAME_CYCLE_CONTROL    0x000B
#define POWER_CONTROL_2        0x000D
#define POWER_CONTROL_3        0x000E
#define GATE_SCAN_START        0x000F
#define HORIZONTAL_PORCH       0x0016
#define VERTICAL_PORCH         0x0017
#define POWER_CONTROL_4        0x001E

#define GAMMA_CONTROL_1        0x0030
#define GAMMA_CONTROL_2        0x0031
#define GAMMA_CONTROL_3        0x0032
#define GAMMA_CONTROL_4        0x0033
#define GAMMA_CONTROL_5        0x0034
#define GAMMA_CONTROL_6        0x0035
#define GAMMA_CONTROL_7        0x0036
#define GAMMA_CONTROL_8        0x0037
#define GAMMA_CONTROL_9        0x003A
#define GAMMA_CONTROL_10       0x003B

void SPI_send (GuiConst_INT8U sendval)
{
  GuiConst_INT8U count;

  if (sendval & 0x80)
    SETBIT(SPIPORT,SDI);
  else
    CLRBIT(SPIPORT,SDI);
  CLRBIT(SPIPORT,SCK);
  SETBIT(SPIPORT,SCK);

  for (count = 0; count < 7; count++)
  {
    sendval <<= 1;

    if (sendval & 0x80)
      SETBIT(SPIPORT,SDI);
    else
      CLRBIT(SPIPORT,SDI);
    CLRBIT(SPIPORT,SCK);
    SETBIT(SPIPORT,SCK);
  }
}

void HX8238_SpiSend (GuiConst_INT8U RS, GuiConst_INT16U Data)
{
  GuiConst_INT8U Header;

  if(RS)
    Header = 0x72;
  else
    Header = 0x70;

  SETBIT(SPIPORT,SCK);     // Initialise clock state
  CLRBIT(SPIPORT,CSB);     // Set CS active low

  SPI_send(Header);
  SPI_send((GuiConst_INT8U)(Data>>8));
  SPI_send((GuiConst_INT8U)Data);

  SETBIT(SPIPORT,CSB);     // Set CS inactive
}

void GuiDisplay_Init(void)
{
  // Reset HX8238
  CLRBIT(RESBPORT,RESB);
  SETBIT(RESBPORT,RESB);

  // HX8238 Initialization
  HX8238_SpiSend (INDEX_REGISTER,LCD_AC_CONTROL);
  HX8238_SpiSend (INSTRUCTION,0x0200);

  HX8238_SpiSend (INDEX_REGISTER,POWER_CONTROL_1);
  HX8238_SpiSend (INSTRUCTION,0x6364);

  HX8238_SpiSend (INDEX_REGISTER,DATA_FILTER_CONTROL);
  HX8238_SpiSend (INSTRUCTION,0x0447);

  HX8238_SpiSend (INDEX_REGISTER,POWER_CONTROL_2);
  HX8238_SpiSend (INSTRUCTION,0x3221);

  HX8238_SpiSend (INDEX_REGISTER,POWER_CONTROL_3);
  HX8238_SpiSend (INSTRUCTION,0x3100);


  HX8238_SpiSend (INDEX_REGISTER,FUNCTION_CONTROL);
  // bit 15: GHN =  1; Normal gate operation
  // bit 14: XDK =  0; 2 stage pumping from VCI
  // bit 13: GDIS = 1; VGL will discharge in stand by
  // bit 12: LPF =  0; Low pass filter disabled in YUV mode
  // bit 11: DEP =  1; DEN signal is positive polarity signal
  // bit 10: CKP =  1; Data is latched by CLK rising edge
  // bit  9: VSP =  0; VSYNC is negative polarity
  // bit  8: HSP =  1; HSYNC is positive polarity
  // bit  7: DEO =  0; VSYNC/HSYNC are also needed in DE mode
  // bit  6: DIT =  0; Dithering turned off
  // bit  5:        0;
  // bit  4: PWM =  0; PWM disabled
  // bit  3:        0;
  // bits 2-0: FB2-0 = 100; Feedback level = 0.6V
  HX8238_SpiSend (INSTRUCTION,0xAD04);

  HX8238_SpiSend (INDEX_REGISTER,VERTICAL_PORCH);
  // bits 15,14: STH = 0;
  // bits 13-7:  HBP = 0001001; Horizontal front porch
  // bits 6-0:   VBP = 0000010; Vertical front porch 2 clock cycle of HSYNC
  HX8238_SpiSend (INSTRUCTION,0x0482);

  // adjust gamma curve
  HX8238_SpiSend (INDEX_REGISTER,GAMMA_CONTROL_1);
  HX8238_SpiSend (INSTRUCTION,0x0000);

  HX8238_SpiSend (INDEX_REGISTER,GAMMA_CONTROL_2);
  HX8238_SpiSend (INSTRUCTION,0x0407);

  HX8238_SpiSend (INDEX_REGISTER,GAMMA_CONTROL_3);
  HX8238_SpiSend (INSTRUCTION,0x0202);

  HX8238_SpiSend (INDEX_REGISTER,GAMMA_CONTROL_4);
  HX8238_SpiSend (INSTRUCTION,0x0000);

  HX8238_SpiSend (INDEX_REGISTER,GAMMA_CONTROL_5);
  HX8238_SpiSend (INSTRUCTION,0x0505);

  HX8238_SpiSend (INDEX_REGISTER,GAMMA_CONTROL_7);
  HX8238_SpiSend (INSTRUCTION,0x0707);

  HX8238_SpiSend (INDEX_REGISTER,GAMMA_CONTROL_8);
  HX8238_SpiSend (INSTRUCTION,0x0000);

  HX8238_SpiSend (INDEX_REGISTER,GAMMA_CONTROL_9);
  HX8238_SpiSend (INSTRUCTION,0x0904);

  HX8238_SpiSend (INDEX_REGISTER,GAMMA_CONTROL_10);
  HX8238_SpiSend (INSTRUCTION,0x0904);

  // TMP92CH21 LCD driver initialization

  // PORT settings
  PLFC = 0xff;      // LD7-LD0 set
  PLCR = 0xf0;      // Output mode
  PKFC = 0x00;      // LBCD, LLP, LCP0
  PCCR = 0xc0;      // PC6: LDIV (for TFT) PC7: LCP1
  PCFC = 0xc0;      // PC6: LDIV

  // LCD settings
  LCDSCC   = 100;   // Counter set (refresh rate:50Hz at fc = 40MHz)
  LCDCCR0  = 0x02;  // Dummy number of lines - this number should be the same as
                    // VBP in VERTICAL_PORCH register in HX8238
  LCDCCR1  = 0x00;  //
  LCDCCR2  = 0x00;  // Active level widht of LLP
  LCDSIZE  = 0x65;  // 320 com � 240 seg
  LCDFFP   = 0x49;  // 320 com
  LCDMODE0 = 0x58;  // External SRAM, LD bus speed 2xfsys, TFT 256 color
  LCDMODE1 = 0x22;  // LLP mode2, no bus inversion, no auto LD bus, interupt on LP,  8bit A type bus
  LCDCTL0  = 0x02;  // (FP bit8=1)
  LCDCTL1  = 0x00;  // LCP0-rising, LCP1-rising, LBCD phase low, LBCD enable width = LCP1_1CLK

  LSARCL   = 0x00;  // C area (enable) start address 0x400000 of frame buffer
  LSARCM   = 0x00;
  LSARCH   = 0x40;
  LSARAL   = 0x00;  // A area (disable)
  LSARAM   = 0x00;
  LSARAH   = 0x00;
  LSARBL   = 0x00;  // B area (disable)
  LSARBM   = 0x00;
  LSARBH   = 0x00;
  CMNAL    = 0x00;  // A area Row number
  CMNAH    = 0x00;
  CMNBL    = 0x00;  // B area Row number
  CMNBH    = 0x00;
  LCDCTL0  = 0x01;  // LCDC start bit0 = 1

// In GuiGraph8.c must be adjusted this line of code
// This array must start at address 0x400000 in RAM
// GuiConst_INT8U GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
}

void GuiDisplay_Refresh (void)
{
  // No action
}

#endif // LCD_CONTROLLER_TYPE_HX8238_TMP92CH21

#ifdef LCD_CONTROLLER_TYPE_HX8312

// ============================================================================
//
// HX8312 DISPLAY CONTROLLER
//
// This driver uses one HX8312 display controller with on-chip oscillator.
// LCD display in 16 bit 8080 parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x320 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardware pin settings for 16 bit color depth
// C86 = 0 - 8080 type interface
// PSX BWS1 BWS0 DTX2 DTX1
// 0    1    0    0    0   - 16 bit parallel collective interface
// 1    1    0    x    x   - 16 bit serial interface

// Uncomment for microcontroller Cortex M3 with HX8312 on evaluation board EVB525
//#define CORTEX_M3

#ifdef CORTEX_M3
  #include "stm32f10x_lib.h"

  // Sets or reset LCD control lines
  void LCD_CtrlLinesWrite(GPIO_TypeDef* GPIOx, GuiConst_INT16U CtrlPins, BitAction BitVal)
  {
    // Set or Reset the control line
    GPIO_WriteBit(GPIOx, CtrlPins, BitVal);
  }

  //Configures LCD control lines in Output Push-Pull mode.
  void LCD_CtrlLinesConfig(void)
  {
    GPIO_InitTypeDef GPIO_InitStructure;

    // Configure NCS (PB.02) in Output Push-Pull mode
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    // Configure NWR(RNW), RS (PD.15, PD.07) in Output Push-Pull mode
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_15;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    LCD_CtrlLinesWrite(GPIOD, CtrlPin_NWR, Bit_SET);
    LCD_CtrlLinesWrite(GPIOD, CtrlPin_RS, Bit_SET);
  }

  //Configures the SPI2 interface
  void LCD_SPIConfig(void)
  {
    SPI_InitTypeDef    SPI_InitStructure;
    GPIO_InitTypeDef   GPIO_InitStructure;

    // Enable GPIOB clock
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    // Enable SPI2 clock
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

    // Configure SPI2 pins: NSS, SCK, MISO and MOSI
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    SPI_DeInit(SPI2);

    // SPI2 Config
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;

    SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;

    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_Init(SPI2, &SPI_InitStructure);

    // SPI2 enable
    SPI_Cmd(SPI2, ENABLE);
  }

  // LCD Control pins
  #define CtrlPin_NCS          GPIO_Pin_2    // PB.02
  #define CtrlPin_RS           GPIO_Pin_7    // PD.07
  #define CtrlPin_NWR          GPIO_Pin_15   // PD.15

  #define CHIP_SELECT          LCD_CtrlLinesWrite(GPIOB, CtrlPin_NCS, Bit_RESET)
  #define CHIP_DESELECT        LCD_CtrlLinesWrite(GPIOB, CtrlPin_NCS, Bit_SET)
  #define WRITE_ON             LCD_CtrlLinesWrite(GPIOD, CtrlPin_NWR, Bit_RESET)
  #define WRITE_OFF            LCD_CtrlLinesWrite(GPIOD, CtrlPin_NWR, Bit_SET)
  #define MODE_COMAND          LCD_CtrlLinesWrite(GPIOD, CtrlPin_RS, Bit_RESET)
  #define MODE_DATA            LCD_CtrlLinesWrite(GPIOD, CtrlPin_RS, Bit_SET)
#else
  #define DISP_DATA            Pxx           // PIN D15 - D0
  #define RESET_ON             Pxx.x = 0     // NRESET pin
  #define RESET_OFF            Pxx.x = 1     // NRESET pin
  #define CHIP_SELECT          Pxx.x = 0     // NCS pin
  #define CHIP_DESELECT        Pxx.x = 1     // NCS pin
  #define MODE_COMMAND         Pxx.x = 0     // RS pin
  #define MODE_DATA            Pxx.x = 1     // RS pin
  #define WRITE_ON             Pxx.x = 0     // NWR(RNW) pin
  #define WRITE_OFF            Pxx.x = 1     // NWR(RNW) pin
  #define READ_ON              Pxx.x = 0     // NRD(E) pin
  #define READ_OFF             Pxx.x = 1     // NWD(E) pin
#endif

// LCD Registers
#define R0             0x00
#define R1             0x01
#define R2             0x02
#define R3             0x03
#define R4             0x04
#define R5             0x05
#define R6             0x06
#define R7             0x07
#define R8             0x08
#define R9             0x09
#define R10            0x0A
#define R12            0x0C
#define R13            0x0D
#define R14            0x0E
#define R15            0x0F
#define R16            0x10
#define R17            0x11
#define R18            0x12
#define R19            0x13
#define R20            0x14
#define R21            0x15
#define R22            0x16
#define R23            0x17
#define R24            0x18
#define R25            0x19
#define R26            0x1A
#define R27            0x1B
#define R28            0x1C
#define R29            0x1D
#define R30            0x1E
#define R31            0x1F
#define R32            0x20
#define R33            0x21
#define R34            0x22
#define R36            0x24
#define R37            0x25
#define R40            0x28
#define R41            0x29
#define R43            0x2B
#define R45            0x2D
#define R48            0x30
#define R49            0x31
#define R50            0x32
#define R51            0x33
#define R52            0x34
#define R53            0x35
#define R54            0x36
#define R55            0x37
#define R56            0x38
#define R57            0x39
#define R59            0x3B
#define R60            0x3C
#define R61            0x3D
#define R62            0x3E
#define R63            0x3F
#define R64            0x40
#define R65            0x41
#define R66            0x42
#define R67            0x43
#define R68            0x44
#define R69            0x45
#define R70            0x46
#define R71            0x47
#define R72            0x48
#define R73            0x49
#define R74            0x4A
#define R75            0x4B
#define R76            0x4C
#define R77            0x4D
#define R78            0x4E
#define R79            0x4F
#define R80            0x50
#define R81            0x51
#define R82            0x52
#define R83            0x53
#define R96            0x60
#define R97            0x61
#define R106           0x6A
#define R118           0x76
#define R128           0x80
#define R129           0x81
#define R130           0x82
#define R131           0x83
#define R132           0x84
#define R133           0x85
#define R134           0x86
#define R135           0x87
#define R136           0x88
#define R137           0x89
#define R139           0x8B
#define R140           0x8C
#define R141           0x8D
#define R143           0x8F
#define R144           0x90
#define R145           0x91
#define R146           0x92
#define R147           0x93
#define R148           0x94
#define R149           0x95
#define R150           0x96
#define R151           0x97
#define R152           0x98
#define R153           0x99
#define R154           0x9A
#define R157           0x9D
#define R192           0xC0
#define R193           0xC1
#define R229           0xE5

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Command writing
void LCD_Cmd(GuiConst_INT8U LCD_Reg, GuiConst_INT8U LCD_RegValue)
{
  GuiConst_INT16U tmp = 0;

  tmp = LCD_Reg << 8;
  tmp |= LCD_RegValue;

  MODE_COMAND;
  CHIP_SELECT;
  WRITE_ON;

#ifdef CORTEX_M3
  SPI_SendData(SPI2, tmp);
  while(SPI_GetFlagStatus(SPI2, SPI_FLAG_BSY) != RESET);
#else
  DISP_DATA = tmp;
#endif

  WRITE_OFF;
  CHIP_DESELCT;
}

// Data writing
void LCD_Data(GuiConst_INT16U RGB_Code)
{
  MODE_DATA;
  CHIP_SELECT;
  WRITE_ON;

#ifdef CORTEX_M3
  SPI_SendData(SPI2, RGB_Code);
  while(SPI_GetFlagStatus(SPI2, SPI_FLAG_BSY) != RESET);
#else
  DISP_DATA = RGB_Code;
#endif

  WRITE_OFF;
  CHIP_DESELECT;
}

void GuiDisplay_Init(void)
{
  GuiConst_INT32U j;

#ifdef CORTEX_M3
  // Configure the LCD Control pins
  LCD_CtrlLinesConfig();
  // Configure the SPI2 interface
  LCD_SPIConfig();
#else
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;
#endif

  // Enable the LCD Oscillator
  LCD_Cmd(R1, 0x10);
  LCD_Cmd(R0, 0xA0);
  LCD_Cmd(R3, 0x01);
  millisecond_delay(10);            // Delay 10 ms
  LCD_Cmd(R3, 0x00);
  LCD_Cmd(R43, 0x04);

  LCD_Cmd(R40, 0x18);
  LCD_Cmd(R26, 0x05);
  LCD_Cmd(R37, 0x05);
  LCD_Cmd(R25, 0x00);

  // LCD Power On
  LCD_Cmd(R28, 0x73);
  LCD_Cmd(R36, 0x74);
  LCD_Cmd(R30, 0x01);
  LCD_Cmd(R24, 0xC1);
  millisecond_delay(10);            // Delay 10 ms
  LCD_Cmd(R24, 0xE1);
  LCD_Cmd(R24, 0xF1);
  millisecond_delay(60);            // Delay 60 ms
  LCD_Cmd(R24, 0xF5);
  millisecond_delay(60);            // Delay 60 ms
  LCD_Cmd(R27, 0x09);
  millisecond_delay(10);            // Delay 10 ms
  LCD_Cmd(R31, 0x11);
  LCD_Cmd(R32, 0x0E);
  LCD_Cmd(R30, 0x81);
  millisecond_delay(10);            // Delay 10 ms

  // Chip Set
  LCD_Cmd(R157, 0x00);
  LCD_Cmd(R192, 0x00);

  LCD_Cmd(R14, 0x00);
  LCD_Cmd(R15, 0x00);
  LCD_Cmd(R16, 0x00);
  LCD_Cmd(R17, 0x00);
  LCD_Cmd(R18, 0x00);
  LCD_Cmd(R19, 0x00);
  LCD_Cmd(R20, 0x00);
  LCD_Cmd(R21, 0x00);
  LCD_Cmd(R22, 0x00);
  LCD_Cmd(R23, 0x00);

  LCD_Cmd(R52, 0x01);
  LCD_Cmd(R53, 0x00);

  LCD_Cmd(R75, 0x00);
  LCD_Cmd(R76, 0x00);
  LCD_Cmd(R78, 0x00);
  LCD_Cmd(R79, 0x00);
  LCD_Cmd(R80, 0x00);

  LCD_Cmd(R60, 0x00);
  LCD_Cmd(R61, 0x00);
  LCD_Cmd(R62, 0x01);
  LCD_Cmd(R63, 0x3F);
  LCD_Cmd(R64, 0x02);
  LCD_Cmd(R65, 0x02);
  LCD_Cmd(R66, 0x00);
  LCD_Cmd(R67, 0x00);
  LCD_Cmd(R68, 0x00);
  LCD_Cmd(R69, 0x00);
  LCD_Cmd(R70, 0xEF);
  LCD_Cmd(R71, 0x00);
  LCD_Cmd(R72, 0x00);
  LCD_Cmd(R73, 0x01);
  LCD_Cmd(R74, 0x3F);

  LCD_Cmd(R29, 0x08);  // R29: Gate scan direction setting

  LCD_Cmd(R134, 0x00);
  LCD_Cmd(R135, 0x30);
  LCD_Cmd(R136, 0x02);
  LCD_Cmd(R137, 0x05);

  LCD_Cmd(R141, 0x01); // R141: Register set-up mode for one line clock
  LCD_Cmd(R139, 0x20); // R139: One line SYSCLK number in one-line
  LCD_Cmd(R51, 0x01);  // R51: N line inversion setting
  LCD_Cmd(R55, 0x01);  // R55: Scanning method setting
  LCD_Cmd(R118, 0x00);

  // Gamma Set
  LCD_Cmd(R143, 0x10);
  LCD_Cmd(R144, 0x67);
  LCD_Cmd(R145, 0x07);
  LCD_Cmd(R146, 0x65);
  LCD_Cmd(R147, 0x07);
  LCD_Cmd(R148, 0x01);
  LCD_Cmd(R149, 0x76);
  LCD_Cmd(R150, 0x56);
  LCD_Cmd(R151, 0x00);
  LCD_Cmd(R152, 0x06);
  LCD_Cmd(R153, 0x03);
  LCD_Cmd(R154, 0x00);

  // Display On
  LCD_Cmd(R1, 0x50);
  LCD_Cmd(R5, 0x04);

  LCD_Cmd(R0, 0x80);
  LCD_Cmd(R59, 0x01);
  millisecond_delay(40);   // Delay 40 ms
  LCD_Cmd(R0, 0x20);

  for(j = 0; j < GuiConst_DISPLAY_BYTES; j++)
    LCD_Data(0x0000);
}

void GuiDisplay_Refresh (void)
{
#ifndef CORTEX_M3

  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(R67, (GuiConst_INT8U)(Y >> 8));
      LCD_Cmd(R68, (GuiConst_INT8U)Y);

      // Select start column
      LCD_Cmd(R66, (GuiConst_INT8U)GuiLib_DisplayRepaint[Y].ByteBegin);

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        LCD_Data(GuiLib_DisplayBuf.Words[Y][X]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
#endif
}

#endif // LCD_CONTROLLER_TYPE_HX8312

#ifdef LCD_CONTROLLER_TYPE_HX8345

// ============================================================================
//
// HX8345 DISPLAY CONTROLLER
//
// This driver uses one HX8345 display controller with on-chip oscillator.
// LCD display in 16 bit 8080 parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 128x160 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardware pin settings
// IM0 - 0, IM1 - 1, IM2 - 0, IM3 - 0
// DB9, DB0 - VSS or VCC

#define DISP_DATA            Pxx           // Pins DB17-DB10, DB8-DB1

#define RESET_ON             Pxx.x = 0     // NRESET pin
#define RESET_OFF            Pxx.x = 1     // NRESET pin
#define CHIP_SELECT          Pxx.x = 0     // NCS pin
#define CHIP_DESELECT        Pxx.x = 1     // NCS pin
#define MODE_DATA            Pxx.x = 1     // RS pin
#define MODE_COMMAND         Pxx.x = 0     // RS pin
#define WRITE_HIGH           Pxx.x = 1     // E_NWR pin
#define WRITE_LOW            Pxx.x = 0     // E_NWR pin
#define READ_HIGH            Pxx.x = 1     // RW_NRD pin
#define READ_LOW             Pxx.x = 0     // RW_NRD pin

// Command writing
void LCD_Cmd(GuiConst_INT16U Val)
{
  CHIP_SELECT;
  MODE_COMMAND;
  READ_HIGH;
  DISP_DATA = Val;
  WRITE_LOW;
  WRITE_HIGH;
  CHIP_DESELECT;
}

// Data writing
void LCD_Data(GuiConst_INT16U Val)
{
  CHIP_SELECT;
  MODE_DATA;
  READ_HIGH;
  DISP_DATA = Val;
  WRITE_LOW;
  WRITE_HIGH;
  CHIP_DESELECT;
}

void LCD_CLS(GuiConst_INT16U color)
{
  GuiConst_INT16U j;

  for(j = 0; j < GuiConst_DISPLAY_BYTES/2; j++)
  {
    LCD_Data(color);
  }
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void GuiDisplay_Init(void)
{
  CHIP_DESELECT;
  READ_HIGH;
  WRITE_HIGH;

  // Start screen initialisation
  RESET_ON;
  millisecond_delay(200);
  RESET_OFF;
  millisecond_delay(500);

  // Driver Output Control Register (R01h)
  LCD_Cmd(0x0001);
  // 10.bit SM = 1 - reverse gate scan order
  // 9.bit GS = 1, reverse gate driver shift direction from G160 - G1
  // 8.bit SS = 1 - reverse source driver shift direction from S384 - S1
  // 4-0.bit - number of scan lines / 8
  LCD_Data(0x0113);

  // LCD Driving Waveform Control Register (R02h)
  LCD_Cmd(0x0002);
  LCD_Data(0x0700);

  // Entry Mode Register (R05h)
  LCD_Cmd(0x0005);
  // 12.bit BGR = 1 - reverse RGB order, BGR = 0 - normal order
  // 5.bit I/D1 = 1 - AC increment
  // 4.bit I/D0 = 1 - AC increment
  // 3.bit AM = 0 - data written horizontally, AM = 1 - data written vertically
  LCD_Data(0x0030);

  // 16-bit Compare Register (R06h)
  LCD_Cmd(0x0006);
  LCD_Data(0x0000);

  // Display Control Register 2 (R08h)
  LCD_Cmd(0x0008);
  // 15.bit VSPL: The polarity of active VSYNC pin
  // 14.bit HSPL: The polarity of active HSYNC pin
  // 13.bit DPL:  The polarity of active DOTCLK pin
  // 11-8.bit FP3-0: number of line for front porch
  // 3-0.bit  BP3-0: numberof line for back porch
  LCD_Data(0x0212);

  // External Display Interface Control Register 1 (R0Ah)
  LCD_Cmd(0x000A);
  // 5.bit DM1 = 0, 4.bit DM0 = 0 - system interface
  // 8.bit RM = 0 - GRAM access system interface
  LCD_Data(0x0000);

  // Frame Cycle Control Register (R0Bh)
  LCD_Cmd(0x000B);
  LCD_Data(0x1800);

  // Gate Scan Position Register (R0Fh)
  LCD_Cmd(0x000F);
  // 4-0.bit SCN4-0: start position of the gate driver in 8 gates step
  LCD_Data(0x0000);

  // CP/WM 18/16-bit Selection Register (R10h)
  LCD_Cmd(0x0010);
  // 0.bit F = 0 - 16-bit mode.
  LCD_Data(0x0000);

  // Vertical Scroll Control Register (R11h)
  LCD_Cmd(0x0011);
  LCD_Data(0x0000);

  // First Display Window Driving Position Register (R14h)
  LCD_Cmd(0x0014);
  LCD_Data(0x9F00);

  // Second Display Window Driving Position Register (R15h)
  LCD_Cmd(0x0015);
  LCD_Data(0x9F00);

  // Horizontal RAM Address Position Register (R16h)
  LCD_Cmd(0x0016);
  LCD_Data(0x7f00);

  // Vertical RAM Address Position Register (R17h)
  LCD_Cmd(0x0017);
  LCD_Data(0x9f00);

  // Start Oscillation Register (R00h)
  LCD_Cmd(0x0000);  // 0.bit, OSD_EN = 1 - enable oscillator
  LCD_Data(0x0001);

  // Power Control Register 4 (R0Dh)
  LCD_Cmd(0x000D);
  // 4.bit PON: 0 - off, 1 - on the operation of step-up circuit 3
  LCD_Data(0x0000);

  millisecond_delay(50);

  // Power Control Register 5 (0Eh)
  LCD_Cmd(0x000E);
  LCD_Data(0x0000);

  millisecond_delay(50);

  // Display Control Register 1 (R07h)
  LCD_Cmd(0x0007);
  LCD_Data(0x0000);

  millisecond_delay(50);

  // Power Control Register 3 (R0Ch)
  LCD_Cmd(0x000C);
  LCD_Data(0x0000);

  // Power control Register 2 (R09h)
  LCD_Cmd(0x0009);
  LCD_Data(0x0008);

  millisecond_delay(50);

  // Power Control Register 4 (R0Dh)
  LCD_Cmd(0x000D);
  // 4.bit PON: 0 - off, 1 - on the operation of step-up circuit 3
  LCD_Data(0x0005);

  millisecond_delay(50);

  // Power Control Register 5 (0Eh)
  LCD_Cmd(0x000E);
  LCD_Data(0x101a);

  millisecond_delay(50);

  // Power Control Register 4 (R0Dh)
  LCD_Cmd(0x000D);
  // 3-0.bit VRH3-0: the magnification of amplification for VREG1OUT voltage
  // 4.bit PON: 0 - off, 1 - on the operation of step-up circuit 3
  LCD_Data(0x0015);

  millisecond_delay(50);

  // Power control Register 1 (R03h)
  LCD_Cmd(0x0003);
  // 4-2.bit AP2�0: Constant Current of Operational Amplifier
  // 1.bit SLP: 1 - sleep, 0 - operate
  // 0.bit STB: 1 - stand by, 0 - operate
  LCD_Data(0x0010);

  millisecond_delay(50);

  // Power control Register 2 (R09h)
  LCD_Cmd(0x0009);
  // 9-8.bit DCM1-0: Set the set-up frequency
  // 3.bit DK: ON - 0 /OFF - 1 the operation of step-up circuit
  // 2-0.bit SAP2�0: Fixed Current of Operational Amplifier
  LCD_Data(0x0100);

  millisecond_delay(50);

  // Power control Register 1 (R03h)
  LCD_Cmd(0x0003);
  // 4-2.bit AP2�0: Constant Current of Operational Amplifier
  // 1.bit SLP: 1 - sleep, 0 - operate
  // 0.bit STB: 1 - stand by, 0 - operate
  LCD_Data(0x0130);

  millisecond_delay(50);

  // Power Control Register 5 (0Eh)
  LCD_Cmd(0x000E);
  LCD_Data(0x311b);

  millisecond_delay(50);

  // Power control Register 2 (R09h)
  LCD_Cmd(0x0009);
  // 9-8.bit DCM1-0: Set the set-up frequency
  // 3.bit DK: ON - 0 /OFF - 1 the operation of step-up circuit
  // 2-0.bit SAP2�0: Fixed Current of Operational Amplifier
  LCD_Data(0x0100);

  millisecond_delay(100);

  // Display Control Register 1 (R07h)
  LCD_Cmd(0x0007);
  // 5.bit GON=0, 4.bit DTE=0, 1.bit-0.bit D1-0=01
  LCD_Data(0x0005);

  millisecond_delay(50);

  // Display Control Register 1 (R07h)
  LCD_Cmd(0x0007);
  // 5.bit GON=1, 4.bit DTE=0, 1.bit-0.bit D1-0=01
  LCD_Data(0x0025);

  // Display Control Register 1 (R07h)
  LCD_Cmd(0x0007);
  // 5.bit GON=1, 4.bit DTE=0, 1.bit-0.bit D1-0=11
  LCD_Data(0x0027);

  millisecond_delay(50);

  // Display Control Register 1 (R07h)
  LCD_Cmd(0x0007);
  // 5.bit GON=1, 4.bit DTE=1, 1.bit-0.bit D1-0=11
  LCD_Data(0x0037);

  // GRAM Mask Address Set (R20h)
  LCD_Cmd(0x0020);
  // must be 0
  LCD_Data(0x0000);

  // GRAM Address Set (R21h)
  LCD_Cmd(0x0021);
  LCD_Data(0x0000);

  // GRAM write (R22h)
  LCD_Cmd(0x0022);
   // Initialize with black color
  LCD_CLS(0x0000);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
       // Something to redraw in this line
    {
      // GRAM Address Set
      LCD_Cmd(0x0021);
      LCD_Data(Y * 128 + GuiLib_DisplayRepaint[Y].ByteBegin);

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      LCD_Cmd(0x0022);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        LCD_Data(GuiLib_DisplayBuf.Words[Y][X]);
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_HX8345

#ifdef LCD_CONTROLLER_TYPE_HX8346

// ============================================================================
//
// HX8346 DISPLAY CONTROLLER
//
// This driver uses one HX8346 display controller with on-chip oscillator.
// LCD display register-content interface mode, 16 bit 6800 parallel
// interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x320 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardware pin settings for 16 bit 65k color register-content interface mode
// P68 BS2 BS1 BS0 IF1 IF0
// 1   0   0   0   0   1

#define DISP_DATA            Pxx           // PIN D15 - D0

#define RESET_ON             Pxx.x = 0     // NRESET pin
#define RESET_OFF            Pxx.x = 1     // NRESET pin
#define CHIP_SELECT          Pxx.x = 0     // NCS pin
#define CHIP_DESELECT        Pxx.x = 1     // NCS pin
#define MODE_DATA            Pxx.x = 1     // DNC_SCL pin
#define MODE_COMMAND         Pxx.x = 0     // DNC_SCL pin
#define MODE_READ            Pxx.x = 1     // NWR_RNW pin
#define MODE_WRITE           Pxx.x = 0     // NWR_RNW pin
#define RW_ENABLED           Pxx.x = 1     // NRD_E pin
#define RW_DISABLED          Pxx.x = 0     // NRD_E pin

#define MODE_CONTROL         0x0001
#define COLUMN_START_HI      0x0002
#define COLUMN_START_LOW     0x0003
#define COLUMN_END_HI        0x0004
#define COLUMN_END_LOW       0x0005
#define ROW_START_HI         0x0006
#define ROW_START_LOW        0x0007
#define ROW_END_HI           0x0008
#define ROW_END_LOW          0x0009
#define AREA_ROW_START_HI    0x000A
#define AREA_ROW_START_LOW   0x000B
#define AREA_ROW_END_HI      0x000C
#define AREA_ROW_END_LOW     0x000D
#define MEMORY_ACCESS_CTRL   0x0016
#define GATE_SCAN_CTRL       0x0018
#define OSC_CONTROL_2        0x0019
#define OSC_CONTROL_1        0x001A
#define POWER_CONTROL_1      0x001B
#define POWER_CONTROL_2      0x001C
#define POWER_CONTROL_3      0x001D
#define POWER_CONTROL_4      0x001E
#define POWER_CONTROL_5      0x001F
#define POWER_CONTROL_6      0x0020
#define POWER_CONTROL_7      0x0021
#define SRAM_CONTROL         0x0022
#define CYCLE_CONTROL_1      0x0023
#define CYCLE_CONTROL_2      0x0024
#define CYCLE_CONTROL_3      0x0025
#define DISPLAY_CONTROL_1    0x0026
#define DISPLAY_CONTROL_2    0x0027
#define DISPLAY_CONTROL_3    0x0028
#define DISPLAY_CONTROL_4    0x0029
#define DISPLAY_CONTROL_5    0x002A
#define DISPLAY_CONTROL_6    0x002C
#define DISPLAY_CONTROL_7    0x002D
#define DISPLAY_CONTROL_8    0x002F
#define DISPLAY_CONTROL_9    0x0030
#define DISPLAY_CONTROL_16   0x0037
#define RGB_CONTROL          0x0038
#define BGP_CONTROL          0x0042
#define VCOM_CONTROL_1       0x0043
#define VCOM_CONTROL_2       0x0044
#define VCOM_CONTROL_3       0x0045

// Command writing
#define LCD_Cmd(Val)                                                     \
{                                                                        \
  MODE_COMMAND;                                                          \
  MODE_WRITE;                                                            \
  CHIP_SELECT;                                                           \
  RW_ENABLED;                                                            \
  DISP_DATA = Val;                                                       \
  RW_DISABLED;                                                           \
  CHIP_DESELECT;                                                         \
}

// Data writing
#define LCD_Data(Val)                                                    \
{                                                                        \
  MODE_DATA;                                                             \
  MODE_WRITE;                                                            \
  CHIP_SELECT;                                                           \
  RW_ENABLED;                                                            \
  DISP_DATA = Val;                                                       \
  RW_DISABLED;                                                           \
  CHIP_DESELECT;                                                         \
}

void LCD_CLS(GuiConst_INT16U color)
{
  GuiConst_INT16U j;

  for(j=0;j<GuiConst_DISPLAY_BYTES;j++)
  {
    LCD_Data(color);
  }
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void GuiDisplay_Init(void)
{
  // Start screen initialisation
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;

  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0000);

  LCD_Cmd(POWER_CONTROL_1);
  LCD_Data(0x0008);

  LCD_Cmd(VCOM_CONTROL_1);
  LCD_Data(0x0000);

  millisecond_delay(10);

  // Set BT[2-0](bits 7-4) according capacitor connection for VGH, VGL voltage
  // generation.
  LCD_Cmd(POWER_CONTROL_6);
  LCD_Data(0x0000);

  // Set VRH[3-0](bits 3-0) - magnification of amplification for VREG1 voltage
  // for gamma voltage setting.
  LCD_Cmd(POWER_CONTROL_5);
  LCD_Data(0x0000);

  // Set VCM[6-0](bits 6-0) - VCOMH voltage from 0.4 to 0.98 times of VREG1
  // voltage.
  LCD_Cmd(VCOM_CONTROL_2);
  LCD_Data(0x0000);

  // Set VDV[4-0](bits 4-0) - VCOM amplitude from 0.6 to 1.23 times of VREG1
  LCD_Cmd(VCOM_CONTROL_3);
  LCD_Data(0x0000);

  // Set VC2[2-0](bits 7-5) - reference voltage VCI2 for VGH,VGL voltage
  // adjusting.
  // Set VC1[2-0](bits 2-0) - ratio of VBGP for VLCD voltage adjusting
  LCD_Cmd(POWER_CONTROL_3);
  LCD_Data(0x0007);

  // Set VC3[2-0](bits 2-0) - reference voltage VREG3 for VGL voltage adjusting
  LCD_Cmd(POWER_CONTROL_4);
  LCD_Data(0x0000);

  // Set AP[2-0](bits 2-0) - Constant Current of Operational Amplifier
  LCD_Cmd(POWER_CONTROL_2);
  LCD_Data(0x0004);

  // Set PON="1"(bit 5), DK="0"(bit 4)
  LCD_Cmd(POWER_CONTROL_1);
  LCD_Data(0x0010);

  millisecond_delay(40);

  // Set VCOMG="1" bit 7
  LCD_Cmd(VCOM_CONTROL_1);
  LCD_Data(0x0080);

  millisecond_delay(100);

  // Set SAPS1[3-0](bits 3-0) - source output driving ability
  LCD_Cmd(DISPLAY_CONTROL_8);
  LCD_Data(0x0000);

  // Set GON = "0" (bit 5), DTE = "0"(bit 4), D1-0 = "01"(bit 3-2)
  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0004);

  millisecond_delay(100);

  // Set GON = "1"(bit 5), DTE = "0"(bit 4), D1-0 = "01"(bit 3-2)
  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0024);

  // Set GON = "1"(bit 5), DTE = "0"(bit 4), D1-0 = "11"(bit 3-2)
  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x002C);

  millisecond_delay(100);

  // Set GON = "1"(bit 5), DTE = "1"(bit 4), D1-0 = "11"(bit 3-2)
  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x003C);

  // Horizontal RAM address start position - 0
  LCD_Cmd(COLUMN_START_HI);
  LCD_Data(0x0000);
  LCD_Cmd(COLUMN_START_LOW);
  LCD_Data(0x0000);

  // Horizontal RAM address end position - 239
  LCD_Cmd(COLUMN_END_HI);
  LCD_Data(0x0000);
  LCD_Cmd(COLUMN_END_LOW);
  LCD_Data(0x00EF);

  // Vertical RAM address start position - 0
  LCD_Cmd(ROW_START_HI);
  LCD_Data(0x0000);
  LCD_Cmd(ROW_START_LOW);
  LCD_Data(0x0000);

  // Vertical RAM address end position - 319
  LCD_Cmd(ROW_END_HI);
  LCD_Data(0x0001);
  LCD_Cmd(ROW_END_LOW);
  LCD_Data(0x003F);

  // End address of line pointer - 319
  LCD_Cmd(AREA_ROW_END_HI);
  LCD_Data(0x0001);
  LCD_Cmd(AREA_ROW_END_LOW);
  LCD_Data(0x003F);

  // Initialize with black color
  LCD_Cmd(SRAM_CONTROL);
  LCD_CLS(0xFFFF);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(ROW_START_HI);
      LCD_Data(Y>>8);
      LCD_Cmd(ROW_START_LOW);
      LCD_Data(Y);

      // Select start column
      LCD_Cmd(COLUMN_START_HI);
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin >> 8);
      LCD_Cmd(COLUMN_START_LOW);
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin);

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      LCD_Cmd(SRAM_CONTROL);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        LCD_Data(GuiLib_DisplayBuf.Words[Y][X]);
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_HX8346

#ifdef LCD_CONTROLLER_TYPE_HX8347

// ============================================================================
//
// HX8347 DISPLAY CONTROLLER
//
// This driver uses one HX8347 display controller with on-chip oscillator.
// LCD display register-content interface mode, 16 bit 8080 parallel
// interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x320 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardware pin settings for 16 bit 65k color register-content interface mode type I.
// HX8347-A   P68 BS2 BS1 BS0
// HX8347-D   IM3 IM2 IM1 IM0
//            0   0   0   0

#define DISP_DATA            Pxx           // PIN D15 - D0

#define RESET_ON             Pxx.x = 0     // NRESET pin
#define RESET_OFF            Pxx.x = 1     // NRESET pin
#define CHIP_SELECT          Pxx.x = 0     // NCS pin
#define CHIP_DESELECT        Pxx.x = 1     // NCS pin
#define MODE_DATA            Pxx.x = 1     // DNC_SCL pin
#define MODE_COMMAND         Pxx.x = 0     // DNC_SCL pin
#define WRITE_HIGH           Pxx.x = 1     // NWR_SCL pin
#define WRITE_LOW            Pxx.x = 0     // NWR_SCL pin
#define READ_HIGH            Pxx.x = 1     // NRD pin
#define READ_LOW             Pxx.x = 0     // NRD pin

#define MODE_CONTROL         0x0001
#define COLUMN_START_HI      0x0002
#define COLUMN_START_LOW     0x0003
#define COLUMN_END_HI        0x0004
#define COLUMN_END_LOW       0x0005
#define ROW_START_HI         0x0006
#define ROW_START_LOW        0x0007
#define ROW_END_HI           0x0008
#define ROW_END_LOW          0x0009
#define AREA_ROW_START_HI    0x000A
#define AREA_ROW_START_LOW   0x000B
#define AREA_ROW_END_HI      0x000C
#define AREA_ROW_END_LOW     0x000D
#define MEMORY_ACCESS_CTRL   0x0016
#define GATE_SCAN_CTRL       0x0018
#define OSC_CONTROL_2        0x0019
#define OSC_CONTROL_1        0x001A
#define POWER_CONTROL_1      0x001B
#define POWER_CONTROL_2      0x001C
#define POWER_CONTROL_3      0x001D
#define POWER_CONTROL_4      0x001E
#define POWER_CONTROL_5      0x001F
#define POWER_CONTROL_6      0x0020
#define POWER_CONTROL_7      0x0021
#define SRAM_CONTROL         0x0022
#define CYCLE_CONTROL_1      0x0023
#define CYCLE_CONTROL_2      0x0024
#define CYCLE_CONTROL_3      0x0025
#define DISPLAY_CONTROL_1    0x0026
#define DISPLAY_CONTROL_2    0x0027
#define DISPLAY_CONTROL_3    0x0028
#define DISPLAY_CONTROL_4    0x0029
#define DISPLAY_CONTROL_5    0x002A
#define DISPLAY_CONTROL_6    0x002C
#define DISPLAY_CONTROL_7    0x002D
#define DISPLAY_CONTROL_8    0x002F
#define DISPLAY_CONTROL_9    0x0030
#define DISPLAY_CONTROL_16   0x0037
#define RGB_CONTROL          0x0038
#define BGP_CONTROL          0x0042
#define VCOM_CONTROL_1       0x0043
#define VCOM_CONTROL_2       0x0044
#define VCOM_CONTROL_3       0x0045

// Command writing
void LCD_Cmd(GuiConst_INT16U Val)
{
  CHIP_SELECT;
  MODE_COMMAND;
  READ_HIGH;
  DISP_DATA = Val;
  WRITE_LOW;
  WRITE_HIGH;
  CHIP_DESELECT;
}

// Data writing
void LCD_Data(GuiConst_INT16U Val)
{
  CHIP_SELECT;
  MODE_DATA;
  READ_HIGH;
  DISP_DATA = Val;
  WRITE_LOW;
  WRITE_HIGH;
  CHIP_DESELECT;
}

// Data reading
GuiConst_INT16U LCD_ReadData(void)
{
  GuiConst_INT16U Data;

  CHIP_SELECT;
  MODE_DATA;
  WRITE_HIGH;
  READ_LOW;
  READ_LOW;  // instead of nop command
  Data = DISP_DATA;
  READ_HIGH;
  CHIP_DESELECT;

  return Data;
}

void LCD_CLS(GuiConst_INT16U color)
{
  GuiConst_INT16U j;

  for(j = 0; j < GuiConst_DISPLAY_BYTES; j++)
  {
    LCD_Data(color);
  }
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void GuiDisplay_Init(void)
{
  // Start screen initialisation
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;

  millisecond_delay(5);

  LCD_Cmd(0x00);
  if ((LCD_ReadData() & 0x00FF) == 0x0047);
  {
    // HX8347-D version

    // POWER_CONTROL 1, Set BT[2-0](bits 7-4) according capacitor connection
    // for VGH, VGL voltage generation.
    LCD_Cmd(0x001A);
    LCD_Data(0x0000);

    // POWER_CONTROL 2 Set VRH[5-0](bits 6-0) - magnification of amplification
    // for VREG1 voltage for gamma voltage setting.
    LCD_Cmd(0x001B);
    LCD_Data(0x0000);

    // VCOM 1 Set VMF[7-0] - High level voltage of VCOM
    LCD_Cmd(0x0023);
    LCD_Data(0x0080);

    // VCOM 2 Set VMH[7-0] - Low level voltage of VCOM
    LCD_Cmd(0x0024);
    LCD_Data(0x00F0);

    // VCOM 3 Set VML[7-0] - Low level voltage of VCOM
    LCD_Cmd(0x0025);
    LCD_Data(0x00FF);

    // OSC 2 Set OSC_EN="1"
    LCD_Cmd(0x0019);
    LCD_Data(0x0001);

    // POWER CONTROL 3 Set AP="011"
    LCD_Cmd(0x001C);
    LCD_Data(0x0003);

    // POWER CONTROL 6 Set STB="0"
    LCD_Cmd(0x001F);
    LCD_Data(0x0008);

    // POWER CONTROL 6 Set DK="0"
    LCD_Cmd(0x001F);
    LCD_Data(0x0000);

    // POWER CONTROL 6 Set PON="1"
    LCD_Cmd(0x001F);
    LCD_Data(0x0010);

    // POWER CONTROL 6 Set VCOMG="1"
    LCD_Cmd(0x001F);
    LCD_Data(0x0050);

    millisecond_delay(5);

    // Display control 1 Set GON = "1"
    LCD_Cmd(0x0028);
    LCD_Data(0x0020);

    // Display control 1 Set DTE = "1"
    LCD_Cmd(0x0028);
    LCD_Data(0x0030);

    // Display control 1 Set D1-0 = "10"
    LCD_Cmd(0x0028);
    LCD_Data(0x0038);

    // Wait 2 frames or more
    millisecond_delay(100);

    // Display control 1 Set D1-0 = "11"
    LCD_Cmd(0x0028);
    LCD_Data(0x003C);
  }
  else
  {
    // HX8347-A version
    LCD_Cmd(DISPLAY_CONTROL_1);
    LCD_Data(0x0000);

    LCD_Cmd(POWER_CONTROL_1);
    LCD_Data(0x0008);

    LCD_Cmd(VCOM_CONTROL_1);
    LCD_Data(0x0000);

    millisecond_delay(10);

    // Set BT[2-0](bits 7-4) according capacitor connection for VGH, VGL voltage
    // generation.
    LCD_Cmd(POWER_CONTROL_6);
    LCD_Data(0x0000);

    // Set VRH[3-0](bits 3-0) - magnification of amplification for VREG1 voltage
    // for gamma voltage setting.
    LCD_Cmd(POWER_CONTROL_5);
    LCD_Data(0x0000);

    // Set VCM[6-0](bits 6-0) - VCOMH voltage from 0.4 to 0.98 times of VREG1
    // voltage.
    LCD_Cmd(VCOM_CONTROL_2);
    LCD_Data(0x0000);

    // Set VDV[4-0](bits 4-0) - VCOM amplitude from 0.6 to 1.23 times of VREG1
    LCD_Cmd(VCOM_CONTROL_3);
    LCD_Data(0x0000);

    // Set VC2[2-0](bits 7-5) - reference voltage VCI2 for VGH,VGL voltage
    // adjusting.
    // Set VC1[2-0](bits 2-0) - ratio of VBGP for VLCD voltage adjusting
    LCD_Cmd(POWER_CONTROL_3);
    LCD_Data(0x0007);

    // Set VC3[2-0](bits 2-0) - reference voltage VREG3 for VGL voltage adjusting
    LCD_Cmd(POWER_CONTROL_4);
    LCD_Data(0x0000);

    // Set AP[2-0](bits 2-0) - Constant Current of Operational Amplifier
    LCD_Cmd(POWER_CONTROL_2);
    LCD_Data(0x0004);

    // Set PON="1"(bit 5), DK="0"(bit 4)
    LCD_Cmd(POWER_CONTROL_1);
    LCD_Data(0x0010);

    millisecond_delay(40);

    // Set VCOMG="1" bit 7
    LCD_Cmd(VCOM_CONTROL_1);
    LCD_Data(0x0080);

    millisecond_delay(100);

    // Set SAPS1[3-0](bits 3-0) - source output driving ability
    LCD_Cmd(DISPLAY_CONTROL_8);
    LCD_Data(0x0000);

    // Set GON = "0" (bit 5), DTE = "0"(bit 4), D1-0 = "01"(bit 3-2)
    LCD_Cmd(DISPLAY_CONTROL_1);
    LCD_Data(0x0004);

    millisecond_delay(100);

    // Set GON = "1"(bit 5), DTE = "0"(bit 4), D1-0 = "01"(bit 3-2)
    LCD_Cmd(DISPLAY_CONTROL_1);
    LCD_Data(0x0024);

    // Set GON = "1"(bit 5), DTE = "0"(bit 4), D1-0 = "11"(bit 3-2)
    LCD_Cmd(DISPLAY_CONTROL_1);
    LCD_Data(0x002C);

    millisecond_delay(100);

    // Set GON = "1"(bit 5), DTE = "1"(bit 4), D1-0 = "11"(bit 3-2)
    LCD_Cmd(DISPLAY_CONTROL_1);
    LCD_Data(0x003C);
  }

  // Horizontal RAM address start position - 0
  LCD_Cmd(COLUMN_START_HI);
  LCD_Data(0x0000);
  LCD_Cmd(COLUMN_START_LOW);
  LCD_Data(0x0000);

  // Horizontal RAM address end position - 239
  LCD_Cmd(COLUMN_END_HI);
  LCD_Data(0x0000);
  LCD_Cmd(COLUMN_END_LOW);
  LCD_Data(0x00EF);

  // Vertical RAM address start position - 0
  LCD_Cmd(ROW_START_HI);
  LCD_Data(0x0000);
  LCD_Cmd(ROW_START_LOW);
  LCD_Data(0x0000);

  // Vertical RAM address end position - 319
  LCD_Cmd(ROW_END_HI);
  LCD_Data(0x0001);
  LCD_Cmd(ROW_END_LOW);
  LCD_Data(0x003F);

  // End address of line pointer - 319
  LCD_Cmd(AREA_ROW_END_HI);
  LCD_Data(0x0001);
  LCD_Cmd(AREA_ROW_END_LOW);
  LCD_Data(0x003F);

  // Initialize with black color
  LCD_Cmd(SRAM_CONTROL);
  LCD_CLS(0xFFFF);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(ROW_START_HI);
      LCD_Data(Y>>8);
      LCD_Cmd(ROW_START_LOW);
      LCD_Data(Y);

      // Select start column
      LCD_Cmd(COLUMN_START_HI);
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin >> 8);
      LCD_Cmd(COLUMN_START_LOW);
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin);

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      LCD_Cmd(SRAM_CONTROL);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        LCD_Data(GuiLib_DisplayBuf.Words[Y][X]);
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_HX8347

#ifdef LCD_CONTROLLER_TYPE_ILI9341
// ============================================================================
//
// ILI9341 DISPLAY CONTROLLER
//
// This driver uses one ILI9341 display controller with on-chip oscillator.
// While designed with SDT028ATFT, this driver should be compatible with any
// ILI9341 based display.
// Supports 3/4-wire SPI connections and 8/16/18-bit bus interfaces (8080-I & -II)
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x320 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16,18 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Function SendDataByte() and SendDataBlock() must be completed to correspond
// to your �-controller hardware and compiler syntax.
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
// ONE_MS must be set according to the speed of your �-controller
//
// ============================================================================

// Define if using serial or bus interface (comment out following if not serial)
#define ILI9341_SERIAL_IF

// Need to define the following according to your hardware configuration

// Reset line Signal
#define SGL_RESX_H  Pxx.x = 1
#define SGL_RESX_L  Pxx.x = 0

// Parallel I/F Signals
// D/CX signal
#define SGL_DCX_CMD   Pxx.x = 0
#define SGL_DCX_DATA  Pxx.x = 1

// WRX signal
#define SGL_WRX_H  Pxx.x = 1
#define SGL_WRX_L  Pxx.x = 0

// Common Signals
// CSX signal
#define SGL_CSX_H  Pxx.x = 1
#define SGL_CSX_L  Pxx.x = 0

// The Display Buffer can be a simple byte array or a union with a byte arrary
// depending on colour depth.
#ifdef GuiConst_COLOR_DEPTH_16
  #define ILI9341_DisplayBuffer GuiLib_DisplayBuf.Bytes
#else // GuiConst_COLOR_DEPTH_18
  #define ILI9341_DisplayBuffer GuiLib_DisplayBuf
#endif


// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

#define COMMAND_BYTE 0
#define DATA_BYTE    1

#ifdef ILI9341_SERIAL_IF
void SendDataByte(GuiConst_INT8U Data, GuiConst_INT8U cmd)
{
  // Drop CSX
  SGL_CSX_L;

  // Before data is transferred need to check if the  byte is a command or data.
  // This depends on whether serial interface is 3-wire or 4-wire
  if (cmd == COMMAND_BYTE)
  {
    // if 3-wire interface - set first bit to '0'
    // else
    // if 4-wire interface - set SGL_DCX_CMD
  }
  else
  {
    // if 3-wire interface - set first bit to '1'
    // else
    // if 4-wire interface - set SGL_DCX_DATA
  }

  // The data is transferred with the MSB first.

  // Wait for end of transfer

  // Lift CSX
  SGL_CSX_H;
}
void SendDataBlock(GuiConst_INT8U *Data, GuiConst_INT16S count)
{
  // Drop CSX
  SGL_CSX_L;

  // if 3-wire interface need to send a 9 bits at a time - set first bit to '1'
  // else
  // if 4-wire interface - set SGL_DCX_DATA

  // The data is transferred with the MSB first.

  // Wait for end of transfer

  // Lift CSX
  SGL_CSX_H;
}
#else // Using a parallel interface
void SendDataByte(GuiConst_INT8U Data, GuiConst_INT8U cmd)
{
  // Drop CSX
  SGL_CSX_L;

  // Set DCX as required
  if (cmd == COMMAND_BYTE)
    SGL_DCX_CMD;
  else
    SGL_DCX_DATA;

  // Drop WRX while writing
  SGL_WRX_L;

  // WRITE DATA TO PARALLEL BUS HERE
  // NOTE: Depending on the physical wiring between the �-controller and
  // and the ILI9341, you may need to bit shift the data

  // Finished so pull WRX back up
  SGL_WRX_H;

  // Lift CSX
  SGL_CSX_H;
}
void SendDataBlock(GuiConst_INT8U *Data, GuiConst_INT16S count)
{
  // Drop CSX
  SGL_CSX_L;

  // Set DCX to data
  SGL_DCX_DATA;

  // Drop WRX while writing
  SGL_WRX_L;

  // WRITE DATA TO PARALLEL BUS HERE
  // NOTE: Depending on the physical wiring between the �-controller and
  // and the ILI9341, you may need to bit shift the data

  // Finished so pull WRX back up
  SGL_WRX_H;

  // Lift CSX
  SGL_CSX_H;
}
#endif


void WriteCommand(GuiConst_INT8U cmd)
{
  // Write a command byte

  SendDataByte(cmd, COMMAND_BYTE);

}

void WriteCommandData(GuiConst_INT8U data)
{
  // Write a command Parameter Byte

  SendDataByte(data, DATA_BYTE);

}

void WriteDisplayData(GuiConst_INT8U *data)
{
  // Write Display data
  SendDataBlock(&data, GuiConst_COLOR_BYTE_SIZE);

}

void GuiDisplay_Init(void)
{
  // Default configuration recommended by DisplayTech for the SDT028ATFT
  //************* Reset LCD Driver ****************//
  SGL_RESX_H;
  millisecond_delay(1); // Delay 1ms
  SGL_RESX_L;
  millisecond_delay(10); // Delay 10ms // This delay time is necessary
  SGL_RESX_H;
  millisecond_delay(120); // Delay 120 ms
  //----------------------------------------------------
  WriteCommand(0xEF);
    WriteCommandData(0x03);
    WriteCommandData(0x80);
    WriteCommandData(0x02);

  WriteCommand(0xB1); // Frame control
    WriteCommandData(0x00);
    WriteCommandData(0x1A);
  //--------------------------------------------------
  WriteCommand(0xCF);
    WriteCommandData(0x00);
    WriteCommandData(0xAA);
    WriteCommandData(0XB0);

  WriteCommand(0xED);
    WriteCommandData(0x67);
    WriteCommandData(0x03);
    WriteCommandData(0X12);
    WriteCommandData(0x81);

  WriteCommand(0xE8);
    WriteCommandData(0x85);
    //  WriteCommandData(0x00); // Alternative configuration
    WriteCommandData(0x01);
    WriteCommandData(0x78);

  WriteCommand(0xCB);
    WriteCommandData(0x39);
    WriteCommandData(0x2C);
    WriteCommandData(0x00);
    WriteCommandData(0x34);
    WriteCommandData(0x02);

  WriteCommand(0xF7);
    WriteCommandData(0x20);

  WriteCommand(0xEA);
    WriteCommandData(0x00);
    WriteCommandData(0x00);

  WriteCommand(0xC0);//Power control
    WriteCommandData(0x25);

  WriteCommand(0xC1);//Power control
    WriteCommandData(0x11);

  WriteCommand(0xC5);//Vcomh & vcoml control
    WriteCommandData(0x5C);
    WriteCommandData(0x4C);

  WriteCommand(0xC7);//vcom adjust control
    //  WriteCommandData(0x8F);  // alternate configuration
    WriteCommandData(0x94);

  WriteCommand(0x36);// Memory Access Control
    WriteCommandData(0x48);

  WriteCommand(0xF2);
    WriteCommandData(0x00);

  WriteCommand(0x2A);  // column select
    WriteCommandData(0x00);
    WriteCommandData(0x00);
    WriteCommandData(0x00);
    WriteCommandData(0xEF);

  WriteCommand(0x2B); //page select
    WriteCommandData(0x00);
    WriteCommandData(0x00);
    WriteCommandData(0x01);
    WriteCommandData(0x3F);


  WriteCommand(0x26); // Gamma select G2.2
    WriteCommandData(0x01);

  WriteCommand(0xE0);  // Positive  gamma
    WriteCommandData(0x0F);
    WriteCommandData(0x27);
    WriteCommandData(0x23);
    WriteCommandData(0x0b);
    WriteCommandData(0x0f);
    WriteCommandData(0x05);
    WriteCommandData(0x54);
    WriteCommandData(0x74);
    WriteCommandData(0x45);
    WriteCommandData(0x0a);
    WriteCommandData(0x17);
    WriteCommandData(0x0a);
    WriteCommandData(0x1c);
    WriteCommandData(0x0e);
    WriteCommandData(0x08);
  WriteCommand(0xE1);  // negative  gamma
    WriteCommandData(0x08);
    WriteCommandData(0x1a);
    WriteCommandData(0x1e);
    WriteCommandData(0x03);
    WriteCommandData(0x0f);
    WriteCommandData(0x05);
    WriteCommandData(0x2e);
    WriteCommandData(0x25);
    WriteCommandData(0x3b);
    WriteCommandData(0x01);
    WriteCommandData(0x06);
    WriteCommandData(0x05);
    WriteCommandData(0x25);
    WriteCommandData(0x33);
    WriteCommandData(0x0F);

  //----------------------RGB �--------
  WriteCommand(0x3A);//  RGB & CPU 18 0X66 /16 0X55 bit select
#ifdef GuiConst_COLOR_DEPTH_18
    WriteCommandData(0x66);  // Select 18-bit per pixel
#else // GuiConst_COLOR_DEPTH_16
    WriteCommandData(0x55);  // Select 16-bit per pixel
#endif

  WriteCommand(0xf6); //INTERFACE control
    WriteCommandData(0x01);
    WriteCommandData(0x01); // determines method of display data transferring - MDT [0:1]- here set to "01"
                            // NOTE that data transfer code in this driver assumes this setting.
    WriteCommandData(0x06);

  WriteCommand(0xB0); // RGB Interface Signal Control
    WriteCommandData(0xc0);
  //------------------------------------------
  //WriteCommand(0xB5); // Blanking Porch Control -- add here if needed

  WriteCommand(0x11); // Sleep OUT
  millisecond_delay(120);
  WriteCommand(0x29); //display on
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16U Column;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // CAUTION: May need to watch for Endian Issues here

      // Set the columns (Pixels) where we need to write
      WriteCommand(0x2A);

      // Write the start column
      // NOTE: ByteStart/ByteEnd/LastByte refer to pixels, not BYTES!!!
      WriteCommandData((GuiLib_DisplayRepaint[Y].ByteStart>>8)&0xFF);
      WriteCommandData(GuiLib_DisplayRepaint[Y].ByteStart&0xFF);

      LastByte = GuiConst_BYTES_PR_LINE/GuiConst_COLOR_BYTE_SIZE - 1;

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write the end column
      WriteCommandData((LastByte>>8)&0xFF);
      WriteCommandData(LastByte&0xFF);

      // Select the row (page)
      WriteCommand(0x2B);
      WriteCommandData((Y>>8)&0xFF); // Start Row MSB first
      WriteCommandData(Y&0xFF);
      WriteCommandData((Y>>8)&0xFF); // End Row MSB first (in this case the same as start)
      WriteCommandData(Y&0xFF);

      // Now write the pixels
      WriteCommand(0x2C);

      for (X = GuiLib_DisplayRepaint[Y].ByteBegin * GuiConst_COLOR_BYTE_SIZE;
           X <= (LastByte * GuiConst_COLOR_BYTE_SIZE + GuiConst_COLOR_BYTE_SIZE -1);
           X = X + GuiConst_COLOR_BYTE_SIZE)
      {
        WriteDisplayData(&ILI9341_DisplayBuffer[Y][X]);
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}


#endif // LCD_CONTROLLER_TYPE_ILI9341

#ifdef LCD_CONTROLLER_TYPE_LC7981

// ============================================================================
//
// LC7981 DISPLAY CONTROLLER
//
// This driver uses one LC7981 display controller.
// LCD display 8 bit 6800 type parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 256x256 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses  must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define DISP_DATA            Pxx           // PIN D7 - D0

#define RESET_ON             Pxx.x = 0     // RES pin
#define RESET_OFF            Pxx.x = 1     // RES pin
#define CHIP_ENABLED         Pxx.x = 0     // CS pin
#define CHIP_DISABLED        Pxx.x = 1     // CS pin
#define MODE_DATA            Pxx.x = 0     // RS pin
#define MODE_COMMAND         Pxx.x = 1     // RS pin
#define MODE_READ            Pxx.x = 1     // R/W pin
#define MODE_WRITE           Pxx.x = 0     // R/W pin
#define E_HIGH               Pxx.x = 1     // E pin
#define E_LOW                Pxx.x = 0     // E pin
#define NOP                  asm("nop")    // nop code

// instructions for the LCD Instruction Register
#define CMD_MODE             0x00
#define CMD_CHAR_PITCH       0x01
#define CMD_NUM_CHARS        0x02
#define CMD_TIME_DIVISION    0x03
#define CMD_CURSOR_POS       0x04
#define CMD_DISPLAY_START_LA 0x08
#define CMD_DISPLAY_START_HA 0x09
#define CMD_CURSOR_LA        0x0A
#define CMD_CURSOR_HA        0x0B
#define CMD_WRITE_DATA       0x0C

// Bits of the LCD Mode Control Register (DB5-DB0)
#define MODE_ON_OFF          32
#define MODE_MASTER_SLAVE    16
#define MODE_BLINK           8
#define MODE_CURSOR          4
#define MODE_MODE            2
#define MODE_EXTERNAL        1
// Horizontal character pitches of 8
#define CHAR_PITCH_HP_8      0x07

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void LCD_Cmd(GuiConst_INT8U Val)
{
  millisecond_delay(3);          // wait for complete of previous command
  CHIP_ENABLED;
  MODE_COMMAND;
  MODE_WRITE;
  E_HIGH;
  DISP_DATA = Val;
  //NOP;             // maybe required for some microcontrollers
  E_LOW;
  CHIP_DISABLED;
}

void LCD_Data(GuiConst_INT8U Val)
{
  millisecond_delay(3);          // wait for complete of previous command
  CHIP_ENABLED;
  MODE_DATA;
  MODE_WRITE;
  E_HIGH;
  DISP_DATA = Val;
  //NOP;             // maybe required for some microcontrollers
  E_LOW;
  CHIP_DISABLED;
}

void GuiDisplay_Init (void)
{
  GuiConst_INT16S N;

  RESET_ON;
  millisecond_delay(100);
  RESET_OFF;

  // Send mode configuration command with Toggle Display On, Master, Mode Graphics bits set
  LCD_Cmd(CMD_MODE);
  LCD_Data(MODE_ON_OFF | MODE_MASTER_SLAVE | MODE_MODE);
  // Send the set character pitch command with horizontal character pitch of 8
  LCD_Cmd(CMD_CHAR_PITCH);
  LCD_Data(CHAR_PITCH_HP_8);
  // Send number of bytes that can be painted horizontally - must be even number from 2 - 256
  LCD_Cmd(CMD_NUM_CHARS);
  LCD_Data(GuiConst_BYTES_PR_LINE-1);
  // Set the time division
  LCD_Cmd(CMD_TIME_DIVISION);
  LCD_Data(GuiConst_BYTE_LINES-1);
  // Set the display low/high start address to 0x00
  LCD_Cmd(CMD_DISPLAY_START_LA);
  LCD_Data(0);
  LCD_Cmd(CMD_DISPLAY_START_HA);
  LCD_Data(0);
  // Reset the cursor to home 0x00
  LCD_Cmd(CMD_CURSOR_LA);
  LCD_Data(0);
  LCD_Cmd(CMD_CURSOR_HA);
  LCD_Data(0);

  // ERASE LCD MEMORY
  LCD_Cmd(CMD_WRITE_DATA);
  for (N = 0; N < GuiConst_DISPLAY_BYTES; N++)
  {
    LCD_Data(0x00);
  }
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte, Address;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
       // Something to redraw in this line
    {
      // Calculate address in memory
      Address = Y * GuiConst_BYTES_PR_LINE + GuiLib_DisplayRepaint[Y].ByteBegin;
      LCD_Cmd(CMD_CURSOR_LA);
      LCD_Data((GuiConst_INT8U)Address);
      LCD_Cmd(CMD_CURSOR_HA);
      LCD_Data((GuiConst_INT8U)(Address>>8));

      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      LCD_Cmd(CMD_WRITE_DATA);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        LCD_Data(GuiLib_DisplayBuf[Y][X]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_LC7981

#ifdef LCD_CONTROLLER_TYPE_LH155BA

// ============================================================================
//
// LH155BA DISPLAY CONTROLLER
//
// This driver uses one LH155BA display controller.
// LCD display 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 128x64 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (PTT) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define ENABLE_DISPLAY       (PTT_PTT7=0)
#define DISABLE_DISPLAY      (PTT_PTT7=1)
#define ENABLE_READ          (PTT_PTT4=0)
#define DISABLE_READ         (PTT_PTT4=1)
#define ENABLE_WRITE         (PTT_PTT5=0)
#define DISABLE_WRITE        (PTT_PTT5=1)
#define RS_COMMAND           (PTT_PTT6=1)
#define RS_DATA              (PTT_PTT6=0)
#define DISPLAY_ON           (PORT_BIT7=1)
#define DISPLAY_OFF          (PORT_BIT7=0)
#define DISP_DATA            (PORTB)

// Commands
#define CMD_X_ADDRESS        0x00
#define CMD_Y_ADDRESS_L      0x20
#define CMD_Y_ADDRESS_H      0x30
#define CMD_START_LINE_L     0x40
#define CMD_START_LINE_H     0x50
#define CMD_ALTERNING_L      0x60
#define CMD_ALTERNING_H      0x70
#define CMD_DISPLAY_CTRL_1   0x80
#define CMD_DISPLAY_CTRL_2   0x90
#define CMD_INCR_CTRL        0xA0
#define CMD_POWER_CTRL_1     0xB0
#define CMD_READ_INT_REG     0xC0
#define CMD_POWER_CTRL_2     0xD0
#define CMD_POWER_CTRL_3     0xE0
#define CMD_SET_RE_FLAG      0xF0

// DISPLAY_CONTROL_1 bits
#define LCD_ON_OFF           0x01
#define ALLON                0x02
#define SEGON                0x04
#define SHIFT                0x08

// DISPLAY_CONTROL_2 bits
#define REF                  0x01
#define SWAP                 0x02
#define NLIN                 0x04
#define REV                  0x08

// INCREMENT_CONTROL bits
#define AXI                  0x01
#define AYI                  0x02
#define AIM                  0x04

// POWER_CONTROL_1 bits
#define ACL                  0x01
#define PON                  0x02
#define PC_HALT              0x04
#define BIAS                 0x08

// POWER_CONTROL_3 bits
#define ICON                 0x01
#define EXA                  0x02
#define SEGPON               0x08

// POWER_CONTROL_3 in extended mode
#define BOOST_PER_4          0x00
#define BOOST_PER_3          0x01
#define BOOST_PER_2          0x02
#define DUTY_1_64            0x00
#define DUTY_1_48            0x04
#define DUTY_1_32            0x08
#define DUTY_1_16            0x0c

// Wait routine
void WAIT (void)
{
  asm ("nop");
  asm ("nop");
  asm ("nop");
}

// Command writing
// Remember to call WAIT before writing
void WRITE_COMMAND(byte Val)
{
  RS_COMMAND;
  asm ("nop");
  asm ("nop");
  ENABLE_DISPLAY;
  asm ("nop");
  asm ("nop");
  ENABLE_WRITE;
  asm ("nop");
  asm ("nop");
  DISP_DATA = Val;
  asm ("nop");
  asm ("nop");
  asm ("nop");
  DISABLE_WRITE;
  asm ("nop");
  asm ("nop");
  DISABLE_DISPLAY;
  asm ("nop");
  asm ("nop");
}

// Data writing
// Remember to call WAIT before writing
void WRITE_DATA(byte Val)
{
  RS_DATA;
  asm ("nop");
  asm ("nop");
  ENABLE_DISPLAY;
  asm ("nop");
  asm ("nop");
  ENABLE_WRITE;
  asm ("nop");
  asm ("nop");
  DISP_DATA = Val;
  asm ("nop");
  asm ("nop");
  asm ("nop");
  asm ("nop");
  asm ("nop");
  asm ("nop");
  DISABLE_WRITE;
  asm ("nop");
  asm ("nop");
  DISABLE_DISPLAY;
  asm ("nop");
  asm ("nop");
}

void GuiDisplay_Init (void)
{
  GuiConst_INT16S N;

  DISPLAY_OFF;

  // Initialise R/W levels
  DISABLE_READ;
  DISABLE_WRITE;
  DISABLE_DISPLAY;

  // Enable display
  DISPLAY_ON;

  // Turn display background light on
  DISPLAY_LIGHT_ON;

  WAIT();
  WRITE_COMMAND(CMD_SET_RE_FLAG | 0x01);

  WAIT();
  WRITE_COMMAND(CMD_POWER_CTRL_3 | BOOST_PER_3 | DUTY_1_64);

  WAIT();
  WRITE_COMMAND(CMD_SET_RE_FLAG);//RESET RE

  WAIT();
  WRITE_COMMAND(CMD_POWER_CTRL_2 | 0x0e);

  WAIT();
  WRITE_COMMAND(CMD_POWER_CTRL_1 | BIAS);

  WAIT();
  WRITE_COMMAND(CMD_POWER_CTRL_1 | BIAS | PON);

  WAIT();
  WRITE_COMMAND(CMD_INCR_CTRL | AYI | AXI);

  WAIT();
  WRITE_COMMAND(CMD_DISPLAY_CTRL_2 | SWAP | REF);

  // Graphic Home Address Set: 0x0000
  WAIT();
  WRITE_COMMAND(CMD_X_ADDRESS);

  WAIT();
  WRITE_COMMAND(CMD_Y_ADDRESS_L);

  WAIT();
  WRITE_COMMAND(CMD_Y_ADDRESS_H);

  // ERASE LCD MEMORY
  for (N = 0; N < GuiConst_BYTES; N++)
  {
    WRITE_DATA( 0x00 );
  }
  WAIT();

  WAIT();
  WRITE_COMMAND(CMD_X_ADDRESS);

  WAIT();
  WRITE_COMMAND(CMD_Y_ADDRESS_L);

  WAIT();
  WRITE_COMMAND(CMD_Y_ADDRESS_H);

  // FINISHED ERASING LCD MEMORY

  WAIT();
  WRITE_COMMAND(CMD_DISPLAY_CTRL_1 | LCD_ON_OFF);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;
  GuiConst_INT8Ur coord_x, coord_y_l, coord_y_h;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
       // Something to redraw in this line
    {
      // Address Pointer Set
      coord_x = GuiLib_DisplayRepaint[Y].ByteBegin;

      coord_y_l = (Y & 0x0f);
      coord_y_h = (Y & 0x70) >> 4;

      WAIT();
      WRITE_COMMAND(CMD_X_ADDRESS | coord_x);

      WAIT();
      WRITE_COMMAND(CMD_Y_ADDRESS_L | coord_y_l);

      WAIT();
      WRITE_COMMAND(CMD_Y_ADDRESS_H | coord_y_h);

      // Write display data
      LastByte = GuiLib_MIN(GuiLib_DisplayRepaint[Y].ByteEnd,
                            GuiConst_BYTES_PR_LINE - 1);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        WRITE_DATA(GuiLib_DisplayBuf[Y][X]);

      WAIT();

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_LH155BA

#ifdef LCD_CONTROLLER_TYPE_LH75401

// ============================================================================
//
// LH75401 DISPLAY CONTROLLER
//
// This driver uses one LH75401 controller with built-in LCD interface.
// Frame buffer is for 16 bit memory width.
// Horizontal resolution must by divisible by 16.
// Graphic modes up to 640x480 pixels.
//
// Compatible display controllers:
//   LH75400, LH75410, LH75411
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 12 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// == Use BlueStreak Setup Tool for value generation of these registers ======
// RCPC Registers
#define LCDPrescaler   0x00000000
#define INTCONFIG      0x00000000

// LCD Registers
#define LCDTiming0     0x073B0B4C
#define LCDTiming1     0x070D04EF
#define LCDTiming2     0x013F3806
#define LCDControl     0x00000028
#define LCDPnlType     0x00000004

// ============================================================================

// Base addresses of register blocks
#define RCPC_BASE      0xFFFE2000
#define IOCON_BASE     0xFFFE5000
#define LCD_BASE       0xFFFF4000
#define ALI_BASE       0xFFFE4000

// RCPC Registers
typedef struct
{
  volatile GuiConst_INT32U RCPC_CONTROL;         // RCPC Control
  volatile GuiConst_INT32U RCPC_ID;              // Identification
  volatile GuiConst_INT32U RCPC_REMAP;           // Remap Control
  volatile GuiConst_INT32U RCPC_SOFTRESET;       // Soft Reset
  volatile GuiConst_INT32U RCPC_RESETSTATUS;     // Reset Status
  volatile GuiConst_INT32U RCPC_RESETSTATUSCLR;  // Reset Status Clear
  volatile GuiConst_INT32U RCPC_SYSCLKPRESCALE;  // System Clock Prescale
  volatile GuiConst_INT32U reserved1[2];
  volatile GuiConst_INT32U RCPC_PERIPHCLKCTRL0;  // Peripheral Clock Ctrl 0
  volatile GuiConst_INT32U RCPC_PERIPHCLKCTRL1;  // Peripheral Clock Ctrl 1
  volatile GuiConst_INT32U RCPC_AHBCLKCTRL;      // AHB Clock Ctrl
  volatile GuiConst_INT32U reserved2[4];
  volatile GuiConst_INT32U RCPC_SSPCLKPRESCALE;  // SSP clock Prescale
  volatile GuiConst_INT32U RCPC_LCDCLKPRESCALE;  // LCD clock Prescale
  volatile GuiConst_INT32U reserved3[14];
  volatile GuiConst_INT32U RCPC_INTCONFIG;       // Ext. Interrupt Config
  volatile GuiConst_INT32U RCPC_INTCLEAR;        // Ext. Interrupt Clear
} RCPC_REGS_T;

// IOCON Registers
typedef struct
{
  volatile GuiConst_INT32U IOCON_EBI_MUX;        // EBI pin multiplexing
  volatile GuiConst_INT32U IOCON_PD_MUX;         // port D pin multiplexing
  volatile GuiConst_INT32U IOCON_PE_MUX;         // port E pin multiplexing
  volatile GuiConst_INT32U IOCON_TIMER_MUX;      // timer pin multiplexing
  volatile GuiConst_INT32U IOCON_LCD_MUX;        // LCD pin multiplexing
  volatile GuiConst_INT32U IOCON_PARES_MUX;      // port A resistor multiplexing
  volatile GuiConst_INT32U IOCON_PBRES_MUX;      // port B resistor multiplexing
  volatile GuiConst_INT32U IOCON_PCRES_MUX;      // port C resistor multiplexing
  volatile GuiConst_INT32U IOCON_PDRES_MUX;      // port D resistor multiplexing
  volatile GuiConst_INT32U IOCON_PERES_MUX;      // port E resistor multiplexing
  volatile GuiConst_INT32U IOCON_ADC_MUX;        // ADC pin multiplexing
}IOCON_REGS_T;

// LCDC Registers
typedef struct
{
  volatile GuiConst_INT32U LCD_TIMING0;          // LCD timing 0 register
  volatile GuiConst_INT32U LCD_TIMING1;          // LCD timing 1 register
  volatile GuiConst_INT32U LCD_TIMING2;          // LCD timing 2 register
  volatile GuiConst_INT32U spare0;               //
  volatile GuiConst_INT32U LCD_UPBASE;           // LCD upper framebuffer addr. reg.
  volatile GuiConst_INT32U LCD_LPBASE;           // LCD lower framebuffer addr. reg.
  volatile GuiConst_INT32U LCD_INTENB;           // LCD int enable register
  volatile GuiConst_INT32U LCD_CONTROL;          // LCD control register
  volatile GuiConst_INT32U LCD_STATUS;           // LCD status register
  volatile GuiConst_INT32U LCD_INTR;             // LCD interrupt register
  volatile GuiConst_INT32U LCD_UPCUR;            // LCD upper framebuffer current addr. reg.
  volatile GuiConst_INT32U LCD_LPCUR;            // LCD lower framebuffer current addr. reg.
  volatile GuiConst_INT32U spare1[116];          //
  volatile GuiConst_INT32U LCD_PALETTE[128];     // LCD palette registers
} LCDC_REGS_T;

// LCDINTENB register bit definition
#define LCD_INTENB_FUIEN   0x00000002            // FIFO Underflow Interrupt Enable
#define LCD_INTENB_BUIEN   0x00000004            // Next Base Update Interrupt Enable
#define LCD_INTENB_VCIEN   0x00000008            // Vertical Compare Interrupt Enable
#define LCD_INTENB_MBEIEN  0x00000010            // AHB Master Error Interrupt Enable
// LCDCONTROL register bit definition
#define LCD_CONTROL_ENB    0x00000001            // LCD Controller Enable
#define LCD_CONTROL_PWR    0x00000800            // LCD Power Enable
// LCDSTATUS register bit definition
#define LCD_STATUS_FUI     0x00000002            // FIFO Underflow Interrupt status bit
#define LCD_STATUS_BUI     0x00000004            // LCD Next Base Address Update Interrupt status bit
#define LCD_STATUS_VCI     0x00000008            // Vertical Compare Interrupt status bit
#define LCD_STATUS_MBEI    0x00000010            // Master Bus Error Interrupt status bit
// LCDINTR register bit definition
#define LCD_INTR_FUIM      0x00000002            // FIFO underflow interrupt status bit
#define LCD_INTR_BUIM      0x00000004            // LCD next base address update interrupt status bit
#define LCD_INTR_VCIM      0x00000008            // Vertical compare interrupt status bit
#define LCD_INTR_MBEIM     0x00000010            // AHB master error interrupt status bit

#ifdef ALISetup
// ALI Registers
typedef struct
{
  volatile GuiConst_INT32U ALI_SETUP;
  volatile GuiConst_INT32U ALI_CONTROL;
  volatile GuiConst_INT32U ALI_TIMING1;
  volatile GuiConst_INT32U ALI_TIMING2;
} ALI_REGS_T;
#endif

// Frame buffer from which data are transfered to LCD display through DMA
GuiConst_INT16U framebuffer[GuiConst_DISPLAY_WIDTH*GuiConst_DISPLAY_HEIGHT];

/***********************************************************************
 * Function: lcd_initialize
 *
 * Disables the LCD controller and sets the values into the proper LCD
 * registers. This routine assumes that the LCD clock IS ENABLED  and
 * the LCD output pins have been correctly configured in the bootup code.
 * Note: If an attempt is made to read/write to the LCD controller without
 * first enabling the LCDC clock, the controller will hang.
 *
 **********************************************************************/
void lcd_initialize (void)
{
  GuiConst_INT32U i;

  LCDC_REGS_T *lcdptr;
  RCPC_REGS_T *rcpcptr;
  IOCON_REGS_T *ioconptr;
#ifdef ALISetup
  ALI_REGS_T *aliptr; // not used in TFT mode
#endif

  // Get pointer to LCDC regs, RCPC regs and IOCON regs
  lcdptr = (LCDC_REGS_T* )LCD_BASE;
  rcpcptr = (RCPC_REGS_T* )RCPC_BASE;
  ioconptr = (IOCON_REGS_T* )IOCON_BASE;
#ifdef ALISetup
  aliptr = (ALI_REGS_T* )ALI_BASE; // not used in TFT mode
#endif

  // unlock the RCPC controller
  rcpcptr->RCPC_CONTROL |= (1<<9);

  // HCLK prescaler setting, fix HCLK to be 50Mhz
  rcpcptr->RCPC_SYSCLKPRESCALE = 1;

  // Enable the LCD and SSP the clocks
  rcpcptr->RCPC_PERIPHCLKCTRL1 = 0;

  // The LCDC clock should already be enabled, setup the LCD prescaler
  rcpcptr->RCPC_LCDCLKPRESCALE = LCDPrescaler;

  // set panel type
  ioconptr->IOCON_LCD_MUX = LCDPnlType;

  // make sure the LCD controller is disabled
  lcdptr->LCD_CONTROL &= ~LCD_CONTROL_ENB;

  // load the timing registers
  lcdptr->LCD_TIMING0 = LCDTiming0;
  lcdptr->LCD_TIMING1 = LCDTiming1;
  lcdptr->LCD_TIMING2 = LCDTiming2;
  lcdptr->LCD_CONTROL = LCDControl;

#ifdef ALISetup
  // If AD-TFT panel, set up those registers
  aliptr->ALI_SETUP = ALISetup;
  aliptr->ALI_TIMING1 = ALITiming1;
  aliptr->ALI_TIMING2 = ALITiming2;
  aliptr->ALI_CONTROL = ALIControl;
#endif

  // clear framebuffer
  for (i = 0; i < GuiConst_DISPLAY_WIDTH * GuiConst_DISPLAY_HEIGHT; i++)
    framebuffer[i] = 0;

  // Intialize the frame buffer address
  lcdptr->LCD_UPBASE = (GuiConst_INT32U) framebuffer;

  // Disable interrupts(user may choose to enable some interrupts at this time)
  lcdptr->LCD_INTENB = 0;

  // Clear out the color palette
  for (i = 0; i < 256; i++)
  {
    lcdptr->LCD_PALETTE[i] = 0;
  }
}

void lcd_turnon (void)
{
  LCDC_REGS_T *lcdptr;

  // Turn on power to the panel. This only has value if the LCDVDENB
  // controls power to the panel. Most LCD panels want to see data
  // within 10ms after power is applied. The LCD controller is also enabled.
  lcdptr = (LCDC_REGS_T *)LCD_BASE;
  lcdptr->LCD_CONTROL |= LCD_CONTROL_PWR | LCD_CONTROL_ENB;
}

void lcd_turnoff (void)
{
  LCDC_REGS_T *lcdptr;

  // Turn off power to the panel and disable the LCD controller
  lcdptr = (LCDC_REGS_T *)LCD_BASE;
  lcdptr->LCD_CONTROL &= ~(LCD_CONTROL_PWR | LCD_CONTROL_ENB);
}

void GuiDisplay_Init(void)
{
  lcd_initialize ();
  lcd_turnon ();
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y,i;
  GuiConst_INT16S LastByte;
  GuiConst_INT32U Address;
  GuiConst_INT16U *bufferaddr;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Address Pointer Set
      Address = Y * GuiConst_BYTES_PR_LINE/2 +
      GuiLib_DisplayRepaint[Y].ByteBegin;


      // Set amount of data to be changed
      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      i = 0;
      // Get pointer to start of framebuffer
      bufferaddr = framebuffer;

      // Write data for one line to framebuffer from GuiLib_DisplayBuf
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        *(bufferaddr + Address+i) = GuiLib_DisplayBuf.Words[Y][X];
        i++;
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();

}

#endif // LCD_CONTROLLER_TYPE_LH75401

#ifdef LCD_CONTROLLER_TYPE_LH7A400

// ============================================================================
//
// LH7A400 DISPLAY CONTROLLER
//
// This driver uses one LH7A400 controller with built-in LCD interface.
// LCD driver in 16 bit RGB (565) parallel interface for TFT type.
// Frame buffer is for 16 bit memory width.
// Graphic modes up to 640x480 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// Set base clock frequency of LH7A400
#define LH7A400_BASE_CLOCK         (14745600)
// Set minimum frequency od lcd display
#define LCD_MIN_FREQUENCY          (4500000)
// Set nominal frequency od lcd display
#define LCD_NOMINAL_FREQUENCY      (6325000)
// Set maximum frequency od lcd display
#define LCD_MAX_FREQUENCY          (6800000)

// Set LCD display parameters
// Define to value + 1 because it will be subtracted in the macros
#define LCD_HORIZONTAL_BACK_PORCH  (21)
#define LCD_HORIZONTAL_SYNC_WIDTH  (13)
#define LCD_HORIZONTAL_FRONT_PORCH (11)
#define LCD_VERTICAL_SYNC_WIDTH    (1)
#define LCD_VERTICAL_FRONT_PORCH   (5)
#define LCD_VERTICAL_BACK_PORCH    (5)

// _BIT(n) sets the bit at position 'n'
#define _BIT(n)                    (((GuiConst_INT32U)(1)) << (n))
// _SBF(f,v) sets the bit field starting at position 'f' to value 'v'
#define _SBF(f,v)                  (((GuiConst_INT32U)(v)) << (f))
// _BITMASK constructs a symbol with 'field_width' least significant bits set
#define _BITMASK(field_width)      ( _BIT(field_width) - 1)

// AHB BASES
#define AHB_BASE                   (0x80002000)
#define LCD_BASE                   (AHB_BASE + 0x1000)

// APB BASES
#define APB_BASE                   (0x80000000)
#define GPIO_BASE                  (APB_BASE + 0x0E00)
#define CLKSC_BASE                 (APB_BASE + 0x0400)

// LCD Controller STN and TFT Panel Format Registers
typedef struct
{
  volatile GuiConst_INT32U lcdtiming0;
  volatile GuiConst_INT32U lcdtiming1;
  volatile GuiConst_INT32U lcdtiming2;
  volatile GuiConst_INT32U lcdtiming3;
  volatile GuiConst_INT32U lcdupbase;
  volatile GuiConst_INT32U lcdlpbase;
  volatile GuiConst_INT32U lcdintrenable;
  volatile GuiConst_INT32U lcdctrl;
  volatile GuiConst_INT32U lcdstatus;
  volatile GuiConst_INT32U lcdinterrupt;
  volatile GuiConst_INT32U lcdupcurr;
  volatile GuiConst_INT32U lcdlpcurr;
  volatile GuiConst_INT32U lcdlpoverflow;
  volatile GuiConst_INT32U reservedclcdc[115];
  volatile GuiConst_INT32U lcdpalette[128];
} CLCDCREGS;

// GPIO Register Structures
typedef struct
{
  volatile GuiConst_INT32U padr;
  volatile GuiConst_INT32U pbdr;
  volatile GuiConst_INT32U pcdr;
  volatile GuiConst_INT32U pddr;
  volatile GuiConst_INT32U paddr;
  volatile GuiConst_INT32U pbddr;
  volatile GuiConst_INT32U pcddr;
  volatile GuiConst_INT32U pdddr;
  volatile GuiConst_INT32U pedr;
  volatile GuiConst_INT32U peddr;
  volatile GuiConst_INT32U kscan;
  volatile GuiConst_INT32U pinmux;
  volatile GuiConst_INT32U pfdr;
  volatile GuiConst_INT32U pfddr;
  volatile GuiConst_INT32U pgdr;
  volatile GuiConst_INT32U pgddr;
  volatile GuiConst_INT32U phdr;
  volatile GuiConst_INT32U phddr;
  volatile GuiConst_INT32U reserved1;
  volatile GuiConst_INT32U inttype1;
  volatile GuiConst_INT32U inttype2;
  volatile GuiConst_INT32U gpiofeoi;
  volatile GuiConst_INT32U gpiointen;
  volatile GuiConst_INT32U intstatus;
  volatile GuiConst_INT32U rawintstatus;
  volatile GuiConst_INT32U gpiodb;
  volatile GuiConst_INT32U papindr;
  volatile GuiConst_INT32U pbpindr;
  volatile GuiConst_INT32U pcpindr;
  volatile GuiConst_INT32U pdpindr;
  volatile GuiConst_INT32U pepindr;
  volatile GuiConst_INT32U pfpindr;
  volatile GuiConst_INT32U pgpindr;
  volatile GuiConst_INT32U phpindr;
} GPIOREGS;

// Clock and power registers structure
typedef struct {
  volatile GuiConst_INT32U pwrsr;
  volatile GuiConst_INT32U pwrcnt;
  volatile GuiConst_INT32U halt;
  volatile GuiConst_INT32U stby;
  volatile GuiConst_INT32U bleoi;
  volatile GuiConst_INT32U mceoi;
  volatile GuiConst_INT32U teoi;
  volatile GuiConst_INT32U stfclr;
  volatile GuiConst_INT32U clkset;
  volatile GuiConst_INT32U scrreg0;
  volatile GuiConst_INT32U scrreg1;
  volatile GuiConst_INT32U clktest;
  volatile GuiConst_INT32U usbreset;
  volatile GuiConst_INT32U apbwait;
} CLKSCREGS;

// Color LCD Controller (CLCDC)
#define CLCDC  ((CLCDCREGS *)(LCD_BASE))
// General Purpose Input/Output (GPIO)
#define GPIO   ((GPIOREGS *)(GPIO_BASE))
// Clock and State Controller
#define CLKSC  ((CLKSCREGS *)(CLKSC_BASE))

// GPIO PINMUX Register
#define GPIO_PINMUX_PEOCON    _BIT(0)
#define GPIO_PINMUX_PDOCON    _BIT(1)
#define GPIO_PINMUX_CODECON   _BIT(2)
#define GPIO_PINMUX_UART3ON   _BIT(3)
#define GPIO_PINMUX_CLK12EN   _BIT(4)
#define GPIO_PINMUX_CLK0EN    _BIT(5)

// NOTE: Ensure the argument to the following macros is greater than zero
// Color LCD Controller LCDTiming0 Register Bit Field constants
// LCDTiming0 register field position LSBs
// Pixels per line
// Calculates PPL field value from actual pixels per line
#define CLCDC_LCDTIMING0_PPL_WIDTH 6
#define CLCDC_LCDTIMING0_PPL(n) _SBF(2,(((n)/16)-1)&_BITMASK(CLCDC_LCDTIMING0_PPL_WIDTH))
// Horizontal Synchronization Pulse Width
// Calculates HSW field value from width of CLLP signal in CLCP periods
#define CLCDC_LCDTIMING0_HSW_WIDTH 8
#define CLCDC_LCDTIMING0_HSW(n) _SBF(8,((n)-1)&_BITMASK(CLCDC_LCDTIMING0_HSW_WIDTH))
// Horizontal Front Porch
// Calculates HFP field value from CLCP periods
#define CLCDC_LCDTIMING0_HFP_WIDTH 8
#define CLCDC_LCDTIMING0_HFP(n) _SBF(16,(((n)-1)&_BITMASK(CLCDC_LCDTIMING0_HFP_WIDTH)))
// Horizontal Back Porch
// Calculates HBP field value from CLCP periods
#define CLCDC_LCDTIMING0_HBP_WIDTH 8
#define CLCDC_LCDTIMING0_HBP(n) _SBF(24,(((n)-1)&_BITMASK(CLCDC_LCDTIMING0_HBP_WIDTH)))

// Color LCD Controller LCDTiming1 Register Bit Field constants
// Lines per Panel
// Calculates LPP field value from actual lines per panel
#define CLCDC_LCDTIMING1_LPP_WIDTH 10
#define CLCDC_LCDTIMING1_LPP(n) _SBF(0,((n)-1)&_BITMASK(CLCDC_LCDTIMING1_LPP_WIDTH))
// Vertical Synchronization Pulse
// Calculates VSW field value from vertical sync lines
#define CLCDC_LCDTIMING1_VSW_WIDTH 6
#define CLCDC_LCDTIMING1_VSW(n) _SBF(10,((n)-1)&_BITMASK(CLCDC_LCDTIMING1_VSW_WIDTH))
// Vertical Front Porch
// Calculates VFP field value from inactive lines at end of frame
#define CLCDC_LCDTIMING1_VFP_WIDTH 8
#define CLCDC_LCDTIMING1_VFP(n) _SBF(16,((n)-1)&_BITMASK(CLCDC_LCDTIMING1_VFP_WIDTH))
// Vertical Back Porch
// Calculates VBP field value from inactive lines at start of frame
#define CLCDC_LCDTIMING1_VBP_WIDTH 8
#define CLCDC_LCDTIMING1_VBP(n) _SBF(24,((n)-1)&_BITMASK(CLCDC_LCDTIMING1_VBP_WIDTH))

// Color LCD Controller LCDTiming2 Register Bit Field constants
// Panel Clock Divisor
#define CLCDC_LCDTIMING2_PCD_MIN 2
#define CLCDC_LCDTIMING2_PCD_WIDTH 5
#define CLCDC_LCDTIMING2_PCD(n) _SBF(0,((n)-2)&_BITMASK(CLCDC_LCDTIMING2_PCD_WIDTH))
// Clock Selector
#define CLCDC_LCDTIMING2_CLKSEL _BIT(5)
// AC Bias Pin Frequency
#define CLCDC_LCDTIMING2_ACB_WIDTH 5
#define CLCDC_LCDTIMING2_ACB(n) _SBF(6,((n)-1)&_BITMASK(CLCDC_LCDTIMING2_ACB_WIDTH))
// Invert Vertical Synchronization
#define CLCDC_LCDTIMING2_IVS _BIT(11)
// Invert Horizontal Synchronization
#define CLCDC_LCDTIMING2_IHS _BIT(12)
// Invert Panel Clock
#define CLCDC_LCDTIMING2_IPC _BIT(13)
// Invert Output Enable
#define CLCDC_LCDTIMING2_IOE _BIT(14)
// Clocks Per Line
#define CLCDC_LCDTIMING2_CPL_WIDTH 10
#define CLCDC_LCDTIMING2_CPL(n) _SBF(16,(n)&_BITMASK(CLCDC_LCDTIMING2_CPL_WIDTH))
// Bypass Pixel Clock Divider
#define CLCDC_LCDTIMING2_BCD _BIT(26)

// Color LCD Controller LCDTiming3 Register Bit Field constants
// Line End Signal Delay
// Calculates LED field value from number of CLCDCLK periods
#define CLCDC_LCDTIMING3_LED_WIDTH 7
#define CLCDC_LCDTIMING3_LED(n) _SBF(0,((n)-1)&_BITMASK(CLCDC_LCDTIMING3_LED_WIDTH))
// Line End Enable
#define CLCDC_LCDTIMING3_LEE _BIT(16)

// Color LCD Controller
// FIFO underflow
#define CLCDC_LCDSTATUS_FUF     _BIT(1)
// LCD Next addr. Base Update
#define CLCDC_LCDSTATUS_LNBU    _BIT(2)
// Vertical Compare
#define CLCDC_LCDSTATUS_VCOMP   _BIT(3)
// Master Bus Error
#define CLCDC_LCDSTATUS_MBERROR _BIT(4)

// Color LCD Controller Control Register Bit Field constants
// register field position LSBs LCDCtrl
// LCD Controller Enable
#define CLCDC_LCDCTRL_ENABLE    _BIT(0)
// Bits per pixel
#define CLCDC_LCDCTRL_BPP1      _SBF(1,0)
#define CLCDC_LCDCTRL_BPP2      _SBF(1,1)
#define CLCDC_LCDCTRL_BPP4      _SBF(1,2)
#define CLCDC_LCDCTRL_BPP8      _SBF(1,3)
#define CLCDC_LCDCTRL_BPP16     _SBF(1,4)
#define CLCDC_LCDCTRL_BPP24     _SBF(1,5)
// STN LCD Mono or Color
#define CLCDC_LCDCTRL_BW_COLOR  _SBF(4,0)
#define CLCDC_LCDCTRL_BW_MONO   _SBF(4,1)
// TFT LCD
#define CLCDC_LCDCTRL_TFT       _BIT(5)
// Monochrome LCD has 8-bit interface
#define CLCDC_LCDCTRL_MON8      _BIT(6)
// Dual Panel STN
#define CLCDC_LCDCTRL_DUAL      _BIT(7)
// Swap Red and Blue (RGB to BGR)
#define CLCDC_LCDCTRL_BGR       _BIT(8)
// Big Endian Byte Order
#define CLCDC_LCDCTRL_BEBO      _BIT(9)
// Big Endian Pixel Order
#define CLCDC_LCDCTRL_BEPO      _BIT(10)
// LCD Power Enable
#define CLCDC_LCDCTRL_PWR       _BIT(11)
// VCOMP Interrupt 2 bits, generate Interrupt at:
// 00 - start of vertical synch, 01 - start of back porch
// 10 - start of active video, 11 - start of front porch
#define CLCDC_LCDCTRL_VCOMP_VS  (0)
#define CLCDC_LCDCTRL_VCOMP_BP  (1)
#define CLCDC_LCDCTRL_VCOMP_AV  (2)
#define CLCDC_LCDCTRL_VCOMP_FP  (3)
#define CLCDC_LCDCTRL_VCOMP(n)  _SBF(12,(n))

// LCD DMA FIFO Test Mode Enable
#define CLCDC_LCDCTRL_LDMAFIFOTME _BIT(15)
// LCD DMA FIFO Watermark Level
#define CLCDC_LCDCTRL_WATERMARK   _BIT(16)


#define LCDC_LCDTIMING0    ( CLCDC_LCDTIMING0_PPL(GuiConst_DISPLAY_WIDTH_HW)  | \
                             CLCDC_LCDTIMING0_HSW(LCD_HORIZONTAL_SYNC_WIDTH)  | \
                             CLCDC_LCDTIMING0_HFP(LCD_HORIZONTAL_FRONT_PORCH) | \
                             CLCDC_LCDTIMING0_HBP(LCD_HORIZONTAL_BACK_PORCH))

#define LCDC_LCDTIMING1    ( CLCDC_LCDTIMING1_LPP(GuiConst_DISPLAY_HEIGHT_HW) | \
                             CLCDC_LCDTIMING1_VSW(LCD_VERTICAL_SYNC_WIDTH)    | \
                             CLCDC_LCDTIMING1_VFP(LCD_VERTICAL_FRONT_PORCH)   | \
                             CLCDC_LCDTIMING1_VBP(LCD_VERTICAL_BACK_PORCH))

// invert horizontal sync and pixel clock for display if needed
#define LCDC_LCDTIMING2    ( CLCDC_LCDTIMING2_IHS | CLCDC_LCDTIMING2_IPC | \
                             (CLCDC_LCDTIMING2_CPL(GuiConst_DISPLAY_WIDTH_HW-1)))

#define LCDC_LCDTIMING3    (0)

#define LCDC_LCDINTRENABLE (0)

#define LCDC_LCDCTRL       ( CLCDC_LCDCTRL_ENABLE | CLCDC_LCDCTRL_BW_COLOR | \
                             CLCDC_LCDCTRL_TFT | CLCDC_LCDCTRL_VCOMP(CLCDC_LCDCTRL_VCOMP_VS) | \
                             CLCDC_LCDCTRL_WATERMARK)

#define LCDICP_LCDCONTROL  ( LCDICP_CONTROL_SPSEN | LCDICP_CONTROL_CLSEN )

#define LCDICP_LCDSETUP    ( LCDICP_SETUP_EN | LCDICP_SETUP_PPL(GuiConst_DISPLAY_WIDTH_HW) | \
                             LCDICP_SETUP_VERT_NORMAL | LCDICP_SETUP_HORIZ_NORMAL | \
                             LCDICP_SETUP_MODE_HRTFT )

#define LCDICP_LCDTIMING1  ( LCDICP_TIMING1_PSDEL_CLSDEL(9) | \
                             LCDICP_TIMING1_REVDEL(3) | LCDICP_TIMING1_LPDEL(14) )

#define LCDICP_LCDTIMING2  ( LCDICP_TIMING2_SPLVALUE( (LCD_HORIZONTAL_BACK_PORCH \
                             + LCD_HORIZONTAL_SYNC_WIDTH + 2) ) | LCDICP_TIMING2_PSDEL2_CLSDEL2(209))

// Compute hclk based on current system clock settings
GuiConst_INT32U LH7A400_get_hclk(void)
{
  GuiConst_INT32U m1;
  GuiConst_INT32U m2;
  GuiConst_INT32U p;
  GuiConst_INT32U ps_pow;
  GuiConst_INT32U hclkdiv;
  GuiConst_INT32U gclk;
  GuiConst_INT32U hclk;

  m1 = (CLKSC->clkset >> 7) & 0xf;
  m2 = (CLKSC->clkset >> 11) & 0x1f;
  p = (CLKSC->clkset >> 2) & 0x1f;
  ps_pow = 1 << ( (CLKSC->clkset >> 18) & 0x3);
  hclkdiv = CLKSC->clkset & 0x3;
  gclk = ((m1 + 2) * (m2 + 2) * (LH7A400_BASE_CLOCK / 100) / ((p+2) * ps_pow))* 100;
  hclk = gclk / (hclkdiv + 1);

  return hclk;
}

// Set the current display bits per pixel
void lcd_set_bits_per_pixel(GuiConst_INT32U bpp)
{
  switch (bpp)
  {
    case 1:
      CLCDC->lcdctrl &= ~_SBF(1,_BITMASK(3));
      CLCDC->lcdctrl |= CLCDC_LCDCTRL_BPP1;
      break;
    case 2:
      CLCDC->lcdctrl &= ~_SBF(1,_BITMASK(3));
      CLCDC->lcdctrl |= CLCDC_LCDCTRL_BPP2;
      break;
    case 4:
      CLCDC->lcdctrl &= ~_SBF(1,_BITMASK(3));
      CLCDC->lcdctrl |= CLCDC_LCDCTRL_BPP4;
      break;
    case 8:
      CLCDC->lcdctrl &= ~_SBF(1,_BITMASK(3));
      CLCDC->lcdctrl |= CLCDC_LCDCTRL_BPP8;
      break;
    case 16:
      CLCDC->lcdctrl &= ~_SBF(1,_BITMASK(3));
      CLCDC->lcdctrl |= CLCDC_LCDCTRL_BPP16;
      break;
  }
}

void GuiDisplay_Init(void)
{
  GuiConst_INT32U pcd_value;
  GuiConst_INT32U hclk;
  GuiConst_INT32U clk_ok = 1;

  // Disable the LCD controller
  CLCDC->lcdctrl = 0;

  // Before initializing the LCD controller, make sure the signals
  // from the SOC are LCD outputs. The signals, by default, are
  // GPIOs and must be configued for the LCD prior to use.
  GPIO->pinmux |= (GPIO_PINMUX_PEOCON | GPIO_PINMUX_PDOCON);

  // Get actual frequency
  hclk = LH7A400_get_hclk();

  pcd_value = (hclk / LCD_NOMINAL_FREQUENCY);

   if (pcd_value < 2)
   {
     // hclk is too slow to use pixel clock divider;
     // see if we can use hclk as the pixel clock
      if (hclk < LCD_MIN_FREQUENCY || hclk > LCD_MIN_FREQUENCY)
         clk_ok = 0;
      else
         CLCDC->lcdtiming2 = (GuiConst_INT32U)(LCDC_LCDTIMING2 | \
                             CLCDC_LCDTIMING2_PCD(0) | CLCDC_LCDTIMING2_BCD);
   }

   else if (pcd_value > 31)
   {
     // can't make pixel clock from hclk, select base clock and try again
      pcd_value = (LH7A400_BASE_CLOCK / LCD_NOMINAL_FREQUENCY) - 1;
      if (pcd_value < 0)
      {
        if (LCD_NOMINAL_FREQUENCY > LH7A400_BASE_CLOCK ||
            LCD_MAX_FREQUENCY < LH7A400_BASE_CLOCK)
              clk_ok = 0;
      }
      CLCDC->lcdtiming2 = (GuiConst_INT32U)(LCDC_LCDTIMING2 | \
                          CLCDC_LCDTIMING2_PCD(pcd_value) | CLCDC_LCDTIMING2_CLKSEL);
   }
   else
   {
      if (pcd_value < 2)
        pcd_value = 2;
      // Can get a good pixel clock from hclk
      CLCDC->lcdtiming2 = (GuiConst_INT32U)(LCDC_LCDTIMING2 | CLCDC_LCDTIMING2_PCD(pcd_value));
   }

  if (clk_ok == 1)
  {
    // Set up LCD registers
    CLCDC->lcdtiming0 = (GuiConst_INT32U)LCDC_LCDTIMING0;
    CLCDC->lcdtiming1 = (GuiConst_INT32U)LCDC_LCDTIMING1;
    CLCDC->lcdtiming3 = (GuiConst_INT32U)LCDC_LCDTIMING3;
    CLCDC->lcdintrenable = (GuiConst_INT32U)LCDC_LCDINTRENABLE;

    // Set 16 bit color depth
    lcd_set_bits_per_pixel(16);

    // Set start of frame buffer
    CLCDC->lcdupbase = (GuiConst_INT32U)&GuiLib_DisplayBuf.Words[0][0];

    // start the control signals
    CLCDC->lcdctrl = (GuiConst_INT32U)(LCDC_LCDCTRL  | CLCDC_LCDCTRL_PWR);
  }
}

// EasyGui display buffer is mapped into frame buffer of LH7A400
void GuiDisplay_Refresh (void)
{
  // No action
}

#endif // LCD_CONTROLLER_TYPE_LH7A400

#ifdef LCD_CONTROLLER_TYPE_LPC2478

// ============================================================================
//
// LPC2478 MICROCONTROLLER WITH BUILT-IN DISPLAY CONTROLLER
//
// This driver uses internal lcd controller of LPC2478 microcontroller.
// LCD driver of LPC2478 in 24 bit RGB parallel interface for TFT type
// display or 4bit parallel interface for STN type display.
// Graphic modes up to 1024x768 pixels.
//
// Compatible display controllers:
//   LPC1787
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale, palette or RGB
//   Color depth: 1 bit (B/W), 8 bits, 16 bits, 24 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardware connection for TFT display
// LPC2478                       generic TFT display
// LCDFP               -         VSYNC
// LCDLP               -         HSYNC
// LCDDCLK             -         Pixel CLK
// LCDENA              -         DATA_ENABLE
// LCDVD[0]-LCDVD[7]   -         R0-R7 (for 16bit RGB mode connect R0,R1,R2 to 0)
// LCDVD[8]-LCDVD[15]  -         G0-G7 (for 16bit RGB mode connect G0,G1 to 0)
// LCDVD[16]-LCDVD[23] -         B0-B7 (for 16bit RGB mode connect B0,B1,B2 to 0)

// Hardware connection for STN display
// LCDPWR              -         DISP
// LCDDCLK             -         CL2
// LCDFP               -         FRM
// LCDLP               -         CL1
// LCDVD[3:0]          -         UD[3:0]

// Power Control registers
#define PCON           (*(volatile unsigned long *)0xE01FC0C0)
#define PCONP          (*(volatile unsigned long *)0xE01FC0C4)
// LCD controller power control bit
#define PCLCD          20

// Clock control registers
// CPU Clock Configuration register
#define CCLKCFG        (*(volatile unsigned long *)0xE01FC104)

// Pin function registers
// Pin Function Select Register 0
#define PINSEL0        (*(volatile unsigned long *)0xE002C000)
// Pin Function Select Register 3
#define PINSEL3        (*(volatile unsigned long *)0xE002C00C)
// Pin Function Select Register 4
#define PINSEL4        (*(volatile unsigned long *)0xE002C010)
// Pin Function Select Register 9
#define PINSEL9        (*(volatile unsigned long *)0xE002C024)
// Pin Function Select Register 11
#define PINSEL11       (*(volatile unsigned long *)0xE002C02C)

// Pin connect block registers
// Pin mode select register 0
#define PINMODE0        (*(volatile unsigned long *)0xE002C040)
// Pin mode select register 3
#define PINMODE3        (*(volatile unsigned long *)0xE002C04C)
// Pin mode select register 4
#define PINMODE4        (*(volatile unsigned long *)0xE002C050)
// Pin mode select register 9
#define PINMODE9        (*(volatile unsigned long *)0xE002C064)

// LCD Port Enable
#define LCDPE           1

// LCD controller registers
// LCD Configuration and clocking control
#define LCD_CFG        (*(volatile unsigned long *)0xE01FC1B8)
// Horizontal Timing Control
#define LCD_TIMH       (*(volatile unsigned long *)0xFFE10000)
// Vertical Timing Control
#define LCD_TIMV       (*(volatile unsigned long *)0xFFE10004)
// Clock and Signal Polarity Control register
#define LCD_POL        (*(volatile unsigned long *)0xFFE10008)
// Line End Control register
#define LCD_LE         (*(volatile unsigned long *)0xFFE1000C)
// Upper Panel Frame Base Address register
#define LCD_UPBASE     (*(volatile unsigned long *)0xFFE10010)
// Lower Panel Frame Base Address register
#define LCD_LPBASE     (*(volatile unsigned long *)0xFFE10014)
// LCD Control register
#define LCD_CTRL       (*(volatile unsigned long *)0xFFE10018)
// Interrupt Mask register
#define LCD_INTMSK     (*(volatile unsigned long *)0xFFE1001C)
// Raw Interrupt Status register
#define LCD_INTRAW     (*(volatile unsigned long *)0xFFE10020)
// Masked Interrupt Status register
#define LCD_INTSTAT    (*(volatile unsigned long *)0xFFE10024)
// Interrupt Clear register
#define LCD_INTCLR     (*(volatile unsigned long *)0xFFE10028)
// Upper Panel Current Address Value register
#define LCD_UPCURR     (*(volatile unsigned long *)0xFFE1002C)
// Lower Panel Current Address Value register
#define LCD_LPCURR     (*(volatile unsigned long *)0xFFE10030)
// 256x16-bit Color Palette registers
#define LCD_PAL        (*(volatile unsigned long *)0xFFE10200)
// Cursor Image registers
#define CRSR_IMG       (*(volatile unsigned long *)0xFFE10800)
// Cursor Control register
#define CRSR_CTRL      (*(volatile unsigned long *)0xFFE10C00)

// LCD controller enable bit
#define LCDEN          0
// LCD controller power bit
#define LCDPWR         11
// Bypass pixel clock divider - for TFT display must be 1
#ifdef GuiConst_COLOR_DEPTH_1
#define BCD            0
#else
#define BCD            1
#endif
// Adjust these values according your display
// PLL clock prescaler selection - only odd numbers 0,1,3,5,..255
// 0 - PLL/1, 1 - PLL/2, 3 - PLL/4
#define PLLDIV         0
// LCD panel clock prescaler selection,  range 0 - 31
// LCD freq = MCU freq /(CLKDIV+1)
#define CLKDIV         7
// Horizontal back porch in pixels, range 3 - 256
#ifdef GuiConst_COLOR_DEPTH_1
#define HBP            5
#else
#define HBP            38
#endif
// Horizontal front porch in pixels, range 3 - 256
#ifdef GuiConst_COLOR_DEPTH_1
#define HFP            5
#else
#define HFP            20
#endif
// Horizontal synchronization pulse width in pixels, range 3 - 256
#ifdef GuiConst_COLOR_DEPTH_1
#define HSW            3
#else
#define HSW            30
#endif
// Pixels-per-line in pixels, must be multiple of 16
#define PPL            GuiConst_DISPLAY_WIDTH_HW
// Vertical back porch in lines, range 1 - 256
#ifdef GuiConst_COLOR_DEPTH_1
#define VBP            2
#else
#define VBP            15
#endif
// Vertical front porch in lines, range 1 - 256
#ifdef GuiConst_COLOR_DEPTH_1
#define VFP            2
#else
#define VFP            5
#endif
// Vertical synchronization pulse width in lines, range 1 - 31
#ifdef GuiConst_COLOR_DEPTH_1
#define VSW            1
#else
#define VSW            3
#endif
// Lines per panel
#define LPP            GuiConst_BYTE_LINES
// Clocks per line
#ifdef GuiConst_COLOR_DEPTH_1
#define CPL            GuiConst_DISPLAY_WIDTH_HW/4
#else
#define CPL            GuiConst_DISPLAY_WIDTH_HW
#endif
// Invert output enable
// 0 = LCDENAB output pin is active HIGH in TFT mode
// 1 = LCDENAB output pin is active LOW in TFT mode
#define IOE            0
// Invert panel clock
// 0 = Data is driven on the LCD data lines on the rising edge of LCDDCLK
// 1 = Data is driven on the LCD data lines on the falling edge of LCDDCLK
#define IPC            1
// Invert horizontal synchronization
// 0 = LCDLP pin is active HIGH and inactive LOW
// 1 = LCDLP pin is active LOW and inactive HIGH
#ifdef GuiConst_COLOR_DEPTH_1
#define IHS            0
#else
#define IHS            1
#endif
// IVS Invert vertical synchronization
// 0 = LCDFP pin is active HIGH and inactive LOW
// 1 = LCDFP pin is active LOW and inactive HIGH
#ifdef GuiConst_COLOR_DEPTH_1
#define IVS            0
#else
#define IVS            1
#endif
// Lower five bits of panel clock divisor
#define PCD_LO         2
// CLKSEL Clock Select
// 0 = the clock source for the LCD block is CCLK
// 1 = the clock source for the LCD block is LCDDCLK
#define CLKSEL         0
// Set start address of frame buffer in RAM memory
// 0x4000 0000 - 0x4000 FFFF RAM (64 kB)
// 0x8000 0000 - 0x80FF FFFF Static memory bank 0 (16 MB)
// 0x8100 0000 - 0x81FF FFFF Static memory bank 1 (16 MB)
// 0xA000 0000 - 0xAFFF FFFF Dynamic memory bank 0 (256 MB)
// 0xB000 0000 - 0xBFFF FFFF Dynamic memory bank 1 (256 MB)
#define FRAME_ADDRESS  0x80000000
// Address of pallete registers
#define PALETTE_RAM_ADDR 0xFFE10200

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT32U i;
#ifdef GuiConst_COLOR_DEPTH_24
  GuiConst_INT32U *framePtr;
#else
#ifdef GuiConst_COLOR_DEPTH_16
  GuiConst_INT16U *framePtr;
#else
  GuiConst_INT32U PaletteData;
  GuiConst_INT8U *framePtr;
  GuiConst_INT32U * pDst;
#endif
#endif

  // Disable power
  LCD_CTRL &= ~(1 << LCDEN);
  millisecond_delay(100);
  LCD_CTRL &= ~(1 << LCDPWR);

  // ====================================
  // Initialize Clock(PLL),EMC and SDRAM!
  // ====================================

  // Enabling LCDVD pins function
  PINSEL0 &= 0xFFF000FF;
  PINSEL0 |= 0x00055500;
  PINMODE0&= 0xFFFC00FF;
  PINMODE0|= 0x0002AA00;

  // Enabling LCDVD pins function
  PINSEL3 &= 0xF00000FF;
  PINSEL3 |= 0x05555500;
  PINMODE3&= 0xF00000FF;
  PINMODE3|= 0x0AAAAA00;

  // Enabling LCDVD pins function
  PINSEL4 &= 0xF0300000;
  PINSEL4 |= 0x054FFFFF;
  PINMODE4&= 0xF0300000;
  PINMODE4|= 0x0A8AAAAA;

  // Enabling LCDVD pins function
  PINSEL9 &= 0xF0FFFFFF;
  PINSEL9 |= 0x0A000000;
  PINMODE9&= 0xF0FFFFFF;
  PINMODE9|= 0x0A000000;

  // Enabling LCDVD pins function
  PINSEL11&= 0xFFFFFFF0;
  PINSEL11|= 0x0000000F;

  // Enable LCD controller clock
  PCONP |= 1<<PCLCD;

  // Set start address of frame buffer in RAM memory
  LCD_LPBASE = FRAME_ADDRESS;
  LCD_UPBASE = FRAME_ADDRESS;

  // LCD Control register
#ifdef GuiConst_COLOR_DEPTH_24
  // TFT single panel,24bit, normal output
  LCD_CTRL = 0x0000002A;
#else
#ifdef GuiConst_COLOR_DEPTH_16
  // TFT single panel,16bit, normal output
  LCD_CTRL = 0x00000028;
#else
#ifdef GuiConst_COLOR_DEPTH_8
  // TFT single panel,8bit, normal output
  LCD_CTRL = 0x00000026;
#else
  // STN single panel, monochrome, 4bit bus, normal output
  LCD_CTRL = 0x00000010;
#endif
#endif
#endif

  // LCD Configuration register init pixel clock
  LCD_CFG |= (CLKDIV);

  // Clock and Signal Polarity register
  LCD_POL |= (BCD << 26)|((CPL-1) << 16)|(IOE << 14)|(IPC << 13)|
             (IHS << 12)|(IVS << 11)|(CLKSEL << 5)| PCD_LO;

  // Horizontal Timing register
  LCD_TIMH |= ((HBP-1) << 24)|((HFP-1) << 16)|((HSW-1) << 8)|((PPL/16-1) << 2);

  // Vertical Timing register
  LCD_TIMV |= (VBP << 24)|(VFP << 16)|((VSW-1) << 10)|(LPP-1);

#ifdef GuiConst_COLOR_DEPTH_24
  framePtr = (GuiConst_INT32U *)FRAME_ADDRESS;
#else
#ifdef GuiConst_COLOR_DEPTH_16
  framePtr = (GuiConst_INT32U *)FRAME_ADDRESS;
#else
  framePtr = (GuiConst_INT8U *)FRAME_ADDRESS;
  pDst = (GuiConst_INT32U *)PALETTE_RAM_ADDR;
  for (i = 0; i < 128; i++)
  {
#ifdef GuiConst_COLOR_DEPTH_1
    PaletteData = 0xFFFF0000;
#else
    PaletteData = GuiStruct_Palette[2*i][0];
    PaletteData |= (GuiConst_INT32U)GuiStruct_Palette[2*i][1]<<8;
    PaletteData |= (GuiConst_INT32U)GuiStruct_Palette[2*i+1][0]<<16;
    PaletteData |= (GuiConst_INT32U)GuiStruct_Palette[2*i+1][1]<<24;
#endif
    *pDst++ = PaletteData;
  }
#endif
#endif

  // Clear frame buffer by copying the data into frame buffer
#ifdef GuiConst_COLOR_DEPTH_24
  for (i = 0; i < GuiConst_DISPLAY_BYTES/3; i++)
    *framePtr++ = 0x00000000; // Color
#else
#ifdef GuiConst_COLOR_DEPTH_16
    for (i = 0; i < GuiConst_DISPLAY_BYTES/2; i++)
    *framePtr++ = 0x0000; // Color
#else
  for (i = 0; i < GuiConst_DISPLAY_BYTES; i++)
    *framePtr++ = 0x00;  // Color
#endif
#endif

  millisecond_delay(100);
  // Power-up sequence
  LCD_CTRL |= 1<<LCDEN;
  // Apply contrast voltage to LCD panel
  // (not controlled or supplied by the LCD controller)
  millisecond_delay(100);
  LCD_CTRL |= 1<<LCDPWR;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT32U Pixel, X, Y, LastByte;
#ifdef GuiConst_COLOR_DEPTH_24
  GuiConst_INT32U *framePtr;
#else
#ifdef GuiConst_COLOR_DEPTH_16
  GuiConst_INT16U *framePtr;
#else
  // Valid also for monochrome STN display
  GuiConst_INT8U *framePtr;
#endif
#endif

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
#ifdef GuiConst_COLOR_DEPTH_24
      // Set video ram base address
      framePtr = (GuiConst_INT32U *)FRAME_ADDRESS;
      // Pointer offset calculation
      framePtr += Y * GuiConst_BYTES_PR_LINE/3 +
                  GuiLib_DisplayRepaint[Y].ByteBegin;
      // Set amount of data to be changed
      LastByte = GuiConst_BYTES_PR_LINE/3 - 1;
#else
#ifdef GuiConst_COLOR_DEPTH_16
      // Set video ram base address
      framePtr = (GuiConst_INT16U *)FRAME_ADDRESS;
      // Pointer offset calculation
      framePtr += Y * GuiConst_BYTES_PR_LINE/2 +
                  GuiLib_DisplayRepaint[Y].ByteBegin;
      // Set amount of data to be changed
      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
#else
      // Valid also for monochrome STN display
      // Set video ram base address
      framePtr = (GuiConst_INT8U *)FRAME_ADDRESS;
      // Pointer offset calculation
      framePtr += Y * GuiConst_BYTES_PR_LINE +
                  GuiLib_DisplayRepaint[Y].ByteBegin;
      // Set amount of data to be changed
      LastByte = GuiConst_BYTES_PR_LINE - 1;
#endif
#endif
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write data for one line to framebuffer from GuiLib_DisplayBuf
#ifdef GuiConst_COLOR_DEPTH_24
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin * 3;
               X <= (LastByte * 3 + 2);
               X = X + 3)
      {
        Pixel = GuiLib_DisplayBuf[Y][X];
        Pixel |= (GuiConst_INT32U)GuiLib_DisplayBuf[Y][X+1]<<8;
        Pixel |= (GuiConst_INT32U)GuiLib_DisplayBuf[Y][X+2]<<16;
        *framePtr++ = Pixel;
      }
#else
#ifdef GuiConst_COLOR_DEPTH_16
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        *framePtr++ = GuiLib_DisplayBuf.Words[Y][X];
      }
#else
      // Valid also for monochrome STN display
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        *framePtr++ = GuiLib_DisplayBuf[Y][X];
#endif
#endif
      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_LPC2478

#ifdef LCD_CONTROLLER_TYPE_LPC3230

// ============================================================================
//
// LPC3230 DISPLAY CONTROLLER
//
// This driver uses the LPC32X0 lcd controller build into various micro
// controllers.
// LCD driver of LPC2478 in 24 bit RGB parallel interface for TFT type
// display or 4bit parallel interface for STN type display.
// Graphic modes up to 1024x768 pixels.
//
// Compatible display controllers:
//   LPC3250
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale, palette or RGB
//   Color depth: 1 bit (B/W), 8 bits, 16 bits, 24 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardware connection for TFT display
// LPC3230                       generic TFT display
// ------------------            ------------------------------------------------
// LCDPWR              -         Power
// LCDDCLK             -         Pixel CLK
// LCDENAB             -         DATA_ENABLE
// LCDFP               -         VSYNC
// LCDLE               -         Line end
// LCDLP               -         HSYNC
// LCDVD[0]-LCDVD[7]   -         R0-R7 (for 16bit RGB mode connect R0,R1,R2 to 0)
// LCDVD[8]-LCDVD[15]  -         G0-G7 (for 16bit RGB mode connect G0,G1 to 0)
// LCDVD[16]-LCDVD[23] -         B0-B7 (for 16bit RGB mode connect B0,B1,B2 to 0)

// Hardware connection for STN display
// LCDPWR              -         Power
// LCDDCLK             -         Pixel CLK
// LCDENAB             -         DATA_ENABLE
// LCDFP               -         VSYNC
// LCDLE               -         Line end
// LCDLP               -         HSYNC
// LCDVD[3:0]          -         UD[3:0]

// LCD controller registers
// LCD Configuration and clocking control
#define LCD_CFG        (*(volatile unsigned long *)0x40004054)
// Horizontal Timing Control
#define LCD_TIMH       (*(volatile unsigned long *)0x31040000)
// Vertical Timing Control
#define LCD_TIMV       (*(volatile unsigned long *)0x31040004)
// Clock and Signal Polarity Control register
#define LCD_POL        (*(volatile unsigned long *)0x31040008)
// Line End Control register
#define LCD_LE         (*(volatile unsigned long *)0x3104000C)
// Upper Panel Frame Base Address register
#define LCD_UPBASE     (*(volatile unsigned long *)0x31040010)
// Lower Panel Frame Base Address register
#define LCD_LPBASE     (*(volatile unsigned long *)0x31040014)
// LCD Control register
#define LCD_CTRL       (*(volatile unsigned long *)0x31040018)
// Interrupt Mask register
#define LCD_INTMSK     (*(volatile unsigned long *)0x3104001C)
// Raw Interrupt Status register
#define LCD_INTRAW     (*(volatile unsigned long *)0x31040020)
// Masked Interrupt Status register
#define LCD_INTSTAT    (*(volatile unsigned long *)0x31040024)
// Interrupt Clear register
#define LCD_INTCLR     (*(volatile unsigned long *)0x31040028)
// Upper Panel Current Address Value register
#define LCD_UPCURR     (*(volatile unsigned long *)0x3104002C)
// Lower Panel Current Address Value register
#define LCD_LPCURR     (*(volatile unsigned long *)0x31040030)
// 256x16-bit Color Palette registers
#define LCD_PAL        (*(volatile unsigned long *)0x31040200)
// Cursor Image registers
#define CRSR_IMG       (*(volatile unsigned long *)0x31040800)
// Cursor Control register
#define CRSR_CTRL      (*(volatile unsigned long *)0x31040C00)
// Cursor Control register
#define CRSR_CFG       (*(volatile unsigned long *)0x31040C04)
// Cursor Control register
#define CRSR_PAL0      (*(volatile unsigned long *)0x31040C08)
// Cursor Control register
#define CRSR_PAL1      (*(volatile unsigned long *)0x31040C0C)
// Cursor Control register
#define CRSR_XY        (*(volatile unsigned long *)0x31040C10)
// Cursor Control register
#define CRSR_CLIP      (*(volatile unsigned long *)0x31040C14)
// Cursor Control register
#define CRSR_INTMSK    (*(volatile unsigned long *)0x31040C20)
// Cursor Control register
#define CRSR_INTCLR    (*(volatile unsigned long *)0x31040C24)
// Cursor Control register
#define CRSR_INTRAW    (*(volatile unsigned long *)0x31040C28)
// Cursor Control register
#define CRSR_INTSTAT   (*(volatile unsigned long *)0x31040C2C)

// LCD controller enable bit
#define LCDEN          0
// LCD controller power bit
#define LCDPWR         11
// Bypass pixel clock divider - for TFT display must be 1
#ifdef GuiConst_COLOR_DEPTH_1
#define BCD            0
#else
#define BCD            1
#endif
// LCD panel clock prescaler selection,  range 0 - 31
// LCD freq = MCU freq /(CLKDIV+1)
#define CLKDIV         7
// Horizontal back porch in pixels, range 3 - 256
#ifdef GuiConst_COLOR_DEPTH_1
#define HBP            5
#else
#define HBP            38
#endif
// Horizontal front porch in pixels, range 3 - 256
#ifdef GuiConst_COLOR_DEPTH_1
#define HFP            5
#else
#define HFP            20
#endif
// Horizontal synchronization pulse width in pixels, range 3 - 256
#ifdef GuiConst_COLOR_DEPTH_1
#define HSW            3
#else
#define HSW            30
#endif
// Pixels-per-line in pixels, must be multiple of 16
#define PPL            GuiConst_DISPLAY_WIDTH_HW
// Vertical back porch in lines, range 1 - 256
#ifdef GuiConst_COLOR_DEPTH_1
#define VBP            2
#else
#define VBP            15
#endif
// Vertical front porch in lines, range 1 - 256
#ifdef GuiConst_COLOR_DEPTH_1
#define VFP            2
#else
#define VFP            5
#endif
// Vertical synchronization pulse width in lines, range 1 - 31
#ifdef GuiConst_COLOR_DEPTH_1
#define VSW            1
#else
#define VSW            3
#endif
// Lines per panel
#define LPP            GuiConst_BYTE_LINES
// Clocks per line
#ifdef GuiConst_COLOR_DEPTH_1
#define CPL            GuiConst_DISPLAY_WIDTH_HW/4
#else
#define CPL            GuiConst_DISPLAY_WIDTH_HW
#endif
// Invert output enable
// 0 = LCDENAB output pin is active HIGH in TFT mode
// 1 = LCDENAB output pin is active LOW in TFT mode
#define IOE            0
// Invert panel clock
// 0 = Data is driven on the LCD data lines on the rising edge of LCDDCLK
// 1 = Data is driven on the LCD data lines on the falling edge of LCDDCLK
#define IPC            1
// Invert horizontal synchronization
// 0 = LCDLP pin is active HIGH and inactive LOW
// 1 = LCDLP pin is active LOW and inactive HIGH
#ifdef GuiConst_COLOR_DEPTH_1
#define IHS            0
#else
#define IHS            1
#endif
// IVS Invert vertical synchronization
// 0 = LCDFP pin is active HIGH and inactive LOW
// 1 = LCDFP pin is active LOW and inactive HIGH
#ifdef GuiConst_COLOR_DEPTH_1
#define IVS            0
#else
#define IVS            1
#endif
// Lower five bits of panel clock divisor
#define PCD_LO         2
// CLKSEL Clock Select
// 0 = the clock source for the LCD block is CCLK
// 1 = the clock source for the LCD block is LCDDCLK
#define CLKSEL         0
// Set start address of frame buffer in RAM memory
// 0x4000 0000 - 0x4000 FFFF RAM (64 kB)
// 0x8000 0000 - 0x80FF FFFF Static memory bank 0 (16 MB)
// 0x8100 0000 - 0x81FF FFFF Static memory bank 1 (16 MB)
// 0xA000 0000 - 0xAFFF FFFF Dynamic memory bank 0 (256 MB)
// 0xB000 0000 - 0xBFFF FFFF Dynamic memory bank 1 (256 MB)
#define FRAME_ADDRESS  0x80000000
// Address of pallete registers
#define PALETTE_RAM_ADDR 0xFFE10200

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT32U i;
#ifdef GuiConst_COLOR_DEPTH_24
  GuiConst_INT32U *framePtr;
#else
#ifdef GuiConst_COLOR_DEPTH_16
  GuiConst_INT16U *framePtr;
#else
  GuiConst_INT32U PaletteData;
  GuiConst_INT8U *framePtr;
  GuiConst_INT32U * pDst;
#endif
#endif

  // Disable power
  LCD_CTRL &= ~(1 << LCDEN);
  millisecond_delay(100);
  LCD_CTRL &= ~(1 << LCDPWR);

  // ====================================
  // Initialize Clock(PLL), EMC and SDRAM
  // ====================================

  // Enable LCD pins
  LCD_CFG &= 0xFFFFFE3F;   // Clear bits first
  // Enable relevant mode
//  LCD_CFG |= 0x00000040;   // TFT single panel, 24 bit, normal output
//  LCD_CFG |= 0x000000C0;   // TFT single panel, 16 bit, normal output
//  LCD_CFG |= 0x00000100;   // STN single panel, monochrome, 4 bit bus, normal output

  // Set start address of frame buffer in RAM memory
  LCD_LPBASE = FRAME_ADDRESS;
  LCD_UPBASE = FRAME_ADDRESS;

  // LCD Control register
#ifdef GuiConst_COLOR_DEPTH_24
  // TFT single panel, 24 bit, normal output
  LCD_CTRL = 0x0000002A;
#else
#ifdef GuiConst_COLOR_DEPTH_16
  // TFT single panel, 16 bit, normal output
  LCD_CTRL = 0x00000028;
#else
#ifdef GuiConst_COLOR_DEPTH_8
  // TFT single panel, 8 bit, normal output
  LCD_CTRL = 0x00000026;
#else
  // STN single panel, monochrome, 4 bit bus, normal output
  LCD_CTRL = 0x00000010;
#endif
#endif
#endif

  // LCD Configuration register init pixel clock
  LCD_CFG |= (CLKDIV);

  // Clock and Signal Polarity register
  LCD_POL |= (BCD << 26) | ((CPL-1) << 16) | (IOE << 14) | (IPC << 13) |
             (IHS << 12) | (IVS << 11) | (CLKSEL << 5) | PCD_LO;

  // Horizontal Timing register
  LCD_TIMH |= ((HBP-1) << 24) | ((HFP-1) << 16) | ((HSW-1) << 8) |
              ((PPL/16-1) << 2);

  // Vertical Timing register
  LCD_TIMV |= (VBP << 24) | (VFP << 16) | ((VSW-1) << 10) | (LPP-1);

#ifdef GuiConst_COLOR_DEPTH_24
  framePtr = (GuiConst_INT32U *)FRAME_ADDRESS;
#else
#ifdef GuiConst_COLOR_DEPTH_16
  framePtr = (GuiConst_INT32U *)FRAME_ADDRESS;
#else
  framePtr = (GuiConst_INT8U *)FRAME_ADDRESS;
  pDst = (GuiConst_INT32U *)PALETTE_RAM_ADDR;
  for (i = 0; i < 128; i++)
  {
#ifdef GuiConst_COLOR_DEPTH_1
    PaletteData = 0xFFFF0000;
#else
    PaletteData = GuiStruct_Palette[2 * i][0];
    PaletteData |= (GuiConst_INT32U)GuiStruct_Palette[2 * i][1] << 8;
    PaletteData |= (GuiConst_INT32U)GuiStruct_Palette[2 * i + 1][0] << 16;
    PaletteData |= (GuiConst_INT32U)GuiStruct_Palette[2 * i + 1][1] << 24;
#endif
    *pDst++ = PaletteData;
  }
#endif
#endif

  // Clear frame buffer by copying the data into frame buffer
#ifdef GuiConst_COLOR_DEPTH_24
  for (i = 0; i < GuiConst_DISPLAY_BYTES / 3; i++)
    *framePtr++ = 0x00000000; // Color
#else
#ifdef GuiConst_COLOR_DEPTH_16
    for (i = 0; i < GuiConst_DISPLAY_BYTES / 2; i++)
    *framePtr++ = 0x0000; // Color
#else
  for (i = 0; i < GuiConst_DISPLAY_BYTES; i++)
    *framePtr++ = 0x00;  // Color
#endif
#endif

  millisecond_delay(100);
  // Power-up sequence
  LCD_CTRL |= 1 << LCDEN;
  // Apply contrast voltage to LCD panel
  // (not controlled or supplied by the LCD controller)
  millisecond_delay(100);
  LCD_CTRL |= 1 << LCDPWR;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT32U Pixel, X, Y, LastByte;
#ifdef GuiConst_COLOR_DEPTH_24
  GuiConst_INT32U *framePtr;
#else
#ifdef GuiConst_COLOR_DEPTH_16
  GuiConst_INT16U *framePtr;
#else
  // Valid also for monochrome STN display
  GuiConst_INT8U *framePtr;
#endif
#endif

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
#ifdef GuiConst_COLOR_DEPTH_24
      // Set video ram base address
      framePtr = (GuiConst_INT32U *)FRAME_ADDRESS;
      // Pointer offset calculation
      framePtr += Y * GuiConst_BYTES_PR_LINE/3 +
                  GuiLib_DisplayRepaint[Y].ByteBegin;
      // Set amount of data to be changed
      LastByte = GuiConst_BYTES_PR_LINE/3 - 1;
#else
#ifdef GuiConst_COLOR_DEPTH_16
      // Set video ram base address
      framePtr = (GuiConst_INT16U *)FRAME_ADDRESS;
      // Pointer offset calculation
      framePtr += Y * GuiConst_BYTES_PR_LINE/2 +
                  GuiLib_DisplayRepaint[Y].ByteBegin;
      // Set amount of data to be changed
      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
#else
      // Valid also for monochrome STN display
      // Set video ram base address
      framePtr = (GuiConst_INT8U *)FRAME_ADDRESS;
      // Pointer offset calculation
      framePtr += Y * GuiConst_BYTES_PR_LINE +
                  GuiLib_DisplayRepaint[Y].ByteBegin;
      // Set amount of data to be changed
      LastByte = GuiConst_BYTES_PR_LINE - 1;
#endif
#endif
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write data for one line to framebuffer from GuiLib_DisplayBuf
#ifdef GuiConst_COLOR_DEPTH_24
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin * 3;
               X <= (LastByte * 3 + 2);
               X = X + 3)
      {
        Pixel = GuiLib_DisplayBuf[Y][X];
        Pixel |= (GuiConst_INT32U)GuiLib_DisplayBuf[Y][X+1] << 8;
        Pixel |= (GuiConst_INT32U)GuiLib_DisplayBuf[Y][X+2] << 16;
        *framePtr++ = Pixel;
      }
#else
#ifdef GuiConst_COLOR_DEPTH_16
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        *framePtr++ = GuiLib_DisplayBuf.Words[Y][X];
      }
#else
      // Valid also for monochrome STN display
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        *framePtr++ = GuiLib_DisplayBuf[Y][X];
#endif
#endif
      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_LPC3230

#ifdef LCD_CONTROLLER_TYPE_MPC5606S

// ============================================================================
//
// Freescale MPC5606S MicroController
//
// This driver uses one MPC5606S with DMA write to the display RAM.
// Modify driver accordingly dependent on the specific TFT LCD panel.
// Graphic modes up to Wide VGA (800 x 480 pixels).
// Support RGB colour output to RGB888 (or RGB565), 1 Graphic layer (L0).
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at left
//   Number of color planes: 1
//   Color mode: Direct RGB Color
//   Color depth: 24 bit, 16 bit
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//   RGB Format:
//     24-bit: LSB - Blue, Green, Red - MSB
//     16-bit: 5-bits Red, 6-bits Green, 5-bits Blue
//
// NOTE: This driver accesses the easyGUI display buffer directly and there
// is no protection against reads to this buffer when it is being written
// by the application. If this should cause anomalies on your display you can
// create a separate buffer and load it in the GuiDisplay_Refresh() routine
// below. You could also consider using the DMA_TRANS_FINISH interupt signal
// in the INT_STATUS register to time writing to the DMA buffer.
//
// ============================================================================

// Set the base address of the DCU memory map
#define DCU_BASE 0xFFE7C000 // CHECK THIS VALUE FOR SPECIFIC HARDWARE DESIGN

// Control register addressing
// Select the layer easyGUI will use by setting the following to the address
// of the CtrlDescLX_1 register (default is layer 0 - CtrlDescL0_1)

// Other layers are disabled after reset. easyGUI does not configure them at all.

#define CTRLDESC_BASE 0x000

#define CTRLDESCLX_1  CTRLDESC_BASE+0x000
#define CTRLDESCLX_2  CTRLDESC_BASE+0x004
#define CTRLDESCLX_3  CTRLDESC_BASE+0x008
#define CTRLDESCLX_4  CTRLDESC_BASE+0x00C
#define CTRLDESCLX_5  CTRLDESC_BASE+0x010
#define CTRLDESCLX_6  CTRLDESC_BASE+0x014
#define CTRLDESCLX_7  CTRLDESC_BASE+0x018

// define the other register addresses
#define CTRLDESCCURSOR_1             0x1C0
#define CTRLDESCCURSOR_2             0x1C4
#define CTRLDESCCURSOR_3             0x1C8
#define CTRLDESCCURSOR_4             0x1CC

#define DCU_MODE                     0x1D0
#define BGND                         0x1D4
#define DISP_SIZE                    0x1D8
#define HSYN_PARA                    0x1DC
#define VSYN_PARA                    0x1E0
#define SYNPOL                       0x1E4
#define THRESHOLD                    0x1E8
#define INT_STATUS                   0x1EC // Read only register
#define INT_MASK                     0x1F0

#define COLBAR_1                     0x1F4
#define COLBAR_2                     0x1F8
#define COLBAR_3                     0x1FC
#define COLBAR_4                     0x200
#define COLBAR_5                     0x204
#define COLBAR_6                     0x208
#define COLBAR_7                     0x20C
#define COLBAR_8                     0x210

#define DIV_RATIO                    0x214
#define SIGN_CALC_1                  0x218
#define SIGN_CALC_2                  0x21C
#define CRC_VAL                      0x220 // Read only register
#define PDI_STATUS                   0x224 // Read only register
#define MASK_PDI_STATUS              0x228
#define PARR_ERR_STATUS              0x22C // Read only register
#define MASK_PARR_ERR_STATUS         0x230
#define THRESHOLD_INP_BUF_1          0x234
#define THRESHOLD_INP_BUF_2          0x238
#define LUMA_COMP                    0x23C
#define CHROMA_RED                   0x240
#define CHROMA_GREEN                 0x244
#define CHROMA_BLUE                  0x248
#define CRC_POS                      0x24C // Read only register

// 15 foreground colour registers - only need 1 for our 1 layer
#define FG0_fcolor                   0x250
#define FG0_bcolor                   0x254

#define GLOBAL_PROTECTION            0x300
#define SOFTLOCKBIT_L0               0x304
#define SOFTLOCKBIT_L1               0x308
#define SOFTLOCKBIT_DISP_SIZE        0x30C
#define SOFTLOCKBIT_VSYNCHSYNC_PARA  0x310
#define SOFTLOCKBIT_POL              0x314
#define SOFTLOCKBIT_L0_TRANSP        0x318
#define SOFTLOCKBIT_L1_TRANSP        0x31C

// The following macro writes the config data to the specified register
#define DCU_WriteRegister(Register, data) *(DCU_BASE+Register) = data

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  DCU_WriteRegister(DCU_MODE, 0x00000001); // Reset the DCU (if using the DCU elsewhere delete this line)

  DCU_WriteRegister(CTRLDESCLX_1, 0x32007800);  // Resolution 800 x 480
  DCU_WriteRegister(CTRLDESCLX_2, 0x00000000);  // Origin coordinates of 0,0
  DCU_WriteRegister(CTRLDESCLX_3, &GuiLib_DisplayBuf); // Address of start of layer data
  DCU_WriteRegister(CTRLDESCLX_4, 0x00002001);  // Layer enabled, 16 bits per pixel (0x0000A001 if 24bpp)
  DCU_WriteRegister(CTRLDESCLX_5, 0x00000000);  // maximum Chroma Keying values for RGB
  DCU_WriteRegister(CTRLDESCLX_6, 0x00000000);  // minimum Chroma Keying values for RGB
  DCU_WriteRegister(CTRLDESCLX_7, 0x00000000);  // TILE size (Not used)

  DCU_WriteRegister(CTRLDESCCURSOR_1, 0x00002001); // Cursor not supported
  DCU_WriteRegister(CTRLDESCCURSOR_2, 0x00000000);
  DCU_WriteRegister(CTRLDESCCURSOR_3, 0x00000000);
  DCU_WriteRegister(CTRLDESCCURSOR_4, 0x00000000);

  DCU_WriteRegister(BGND, 0x00000000);      // Background black by default
  DCU_WriteRegister(DISP_SIZE, 0x32007800); // Display size 800 x 480 (Note 800/16 pixels = 50)
  DCU_WriteRegister(HSYN_PARA, 0xC0180300); // Set as default reset value
  DCU_WriteRegister(VSYN_PARA, 0xC0180300); // Set as default reset value
  DCU_WriteRegister(SYNPOL, 0x00000000);    // Set as default reset value
  DCU_WriteRegister(THRESHOLD, 0x501E0000); // Set as default reset value
  DCU_WriteRegister(INT_MASK, 0xFFF2F000);  // Mask all interrupts

  // Leave test colour bars as set during reset to default values
  DCU_WriteRegister(COLBAR_1, 0x000000FF); // Black
  DCU_WriteRegister(COLBAR_2, 0xFF0000FF); // Blue
  DCU_WriteRegister(COLBAR_3, 0xFFFF00FF); // Cyan
  DCU_WriteRegister(COLBAR_4, 0x00FF00FF); // Green
  DCU_WriteRegister(COLBAR_5, 0x00FFFFFF); // Yellow
  DCU_WriteRegister(COLBAR_6, 0x0000FFFF); // Red
  DCU_WriteRegister(COLBAR_7, 0xFF00FFFF); // Purple
  DCU_WriteRegister(COLBAR_8, 0xFFFFFFFF); // White

  DCU_WriteRegister(DIV_RATIO, 0xF8000000);            // Change according to TFT display
  DCU_WriteRegister(SIGN_CALC_1, 0x00000000);          // CRC calculation (Not used)
  DCU_WriteRegister(SIGN_CALC_2, 0x00000000);          // CRC calculation (Not used)
  DCU_WriteRegister(MASK_PDI_STATUS, 0xFFC00000);      // Mask PDI Interrupts
  DCU_WriteRegister(MASK_PARR_ERR_STATUS, 0xFFFFE000); // Mask Parameter Error Interrupts
  DCU_WriteRegister(THRESHOLD_INP_BUF_1, 0x00FF00FF);  // Set as default reset value
  DCU_WriteRegister(THRESHOLD_INP_BUF_2, 0x00FF00FF);  // Set as default reset value
  DCU_WriteRegister(LUMA_COMP, 0x2A4548A9);            // Set as default reset value
  DCU_WriteRegister(CHROMA_RED, 0x00008CC0);           // Set as default reset value
  DCU_WriteRegister(CHROMA_GREEN, 0x1CF00660);         // Set as default reset value
  DCU_WriteRegister(CHROMA_BLUE, 0x90200000);          // Set as default reset value

  DCU_WriteRegister(FG0_fcolor, 0x00000000); // For blending (Not Used)
  DCU_WriteRegister(FG0_bcolor, 0x00000000); // For blending (Not Used)

  DCU_WriteRegister(GLOBAL_PROTECTION, 0x00000000);           // Soft Lock Protection Not Used
  DCU_WriteRegister(SOFTLOCKBIT_L0, 0x00000000);              // Soft Lock Protection Not Used
  DCU_WriteRegister(SOFTLOCKBIT_L1, 0x00000000);              // Soft Lock Protection Not Used
  DCU_WriteRegister(SOFTLOCKBIT_DISP_SIZE, 0x00000000);       // Soft Lock Protection Not Used
  DCU_WriteRegister(SOFTLOCKBIT_VSYNCHSYNC_PARA, 0x00000000); // Soft Lock Protection Not Used
  DCU_WriteRegister(SOFTLOCKBIT_POL, 0x00000000);             // Soft Lock Protection Not Used
  DCU_WriteRegister(SOFTLOCKBIT_L0_TRANSP, 0x00000000);       // Soft Lock Protection Not Used
  DCU_WriteRegister(SOFTLOCKBIT_L1_TRANSP, 0x00000000);       // Soft Lock Protection Not Used

  // If gamma correction is required, write here the gamma tables at addresses
  // (Must be done before DCU enabled)

  // Gamma_R address space 0x0800 � 0x0BFF
  // Gamma_G address space 0x0C00 � 0x0FFF
  // Gamma_B address space 0x1000 � 0x13FF


  // If gamma correction required set to 0xA0000000
  // If using Raster, set to 0x8002000
  DCU_WriteRegister(DCU_MODE, 0x80000000); // Enable DCU and Gamma Correction Off
}

// Refresh display buffer to display
void GuiDisplay_Refresh (void)
{
  // Display data is taken from easyGUI buffer to the display by DMA
  // This function does no action, but this stub is necessary

  return;
}

#endif // LCD_CONTROLLER_TYPE_MPC5606S

#ifdef LCD_CONTROLLER_TYPE_MX257

// ============================================================================
//
// MX257 DISPLAY CONTROLLER
//
// This driver uses the internal LCD display controller of the MX257
// microcontroller.
// Modify driver accordingly for other interface types.
// Graphic modes up to 800x600 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (_P00, _P01, etc.) must be altered to correspond to your
// microcontroller hardware and compiler syntax.

// ============================================================================

#define REG(x)             (*((volatile GuiConst_INT32U *)x))

// LCD registers
#define LSAR               REG(0x53FB_0000)  // LCDC Screen Start Address Register
#define LSR                REG(0x53FB_0004)  // LCDC Size Register
#define LVPWR              REG(0x53FB_0008)  // LCDC Virtual Page Width Register
#define LPCR               REG(0x53FB_0018)  // LCDC Panel Configuration RegisterLCDC
#define LHCR               REG(0x53FB_001C)  // LCDC Horizontal Configuration Register
#define LVCR               REG(0x53FB_0020)  // LCDC Vertical Configuration Register
#define LDCR               REG(0x53FB_0030)  // LCDC DMA Control Register
#define LRMCR              REG(0x53FB_0034)  // LCDC Refresh Mode Control Register

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  // Set EMI interface if easyGui display buffer will be in external RAM

  // Set start address of refresh buffer
  LSSAR = (GuiConst_INT32U)GuiLib_DisplayBuf;

  // Set display height and width
  LSR =  GuiConst_DISPLAY_HEIGHT;
  LSR |= ((GuiConst_DISPLAY_WIDTH/16)<<20);

  // LCDC Virtual Page Width
  LVPWR = GuiConst_DISPLAY_WIDTH/32;

  // LCDC Panel Configuration Register
  // bit 31: 0 - passive display, 1 - active display:
  // bit 30: 0 - monochrome display, 1 - color display:
  // bits 29�28: 00 1-bit width, 10 4-bit width, 11 8-bit width
  // bits 27�25: 000 1 bpp, 001 2 bpp, 010 4 bpp, 011 8 bpp
  //             100 12 bpp, 101 16 bpp, 110 18 bpp, 111 24 bpp
  // bit 24: Pixel Polarity, 0 Active high, 1 Active low
  // bit 23: First Line Marker Polarity, 0 Active high, 1 Active low
  // bit 22: Line Pulse Polarity, 0 Active high, 1 Active low
  // bit 21: LCD Shift Clock Polarity, 0 Active high, 1 Active low
  // bit 20: Output Enable Polarity, 0 Active high, 1 Active low
  // bit 19: LSCLK Idle Enable, 0 Disable LSCLK, 1 Enable LSCLK
  // bit 18: 0 Little endian(set this  one), 1 Big endian mode
  // bit 17: Swap Select, SWAP_SEL = 0, byte 3 (bits 31�24), byte 2 (bits 23�16),
  //         byte 1 (bits 15�8), byte 0 (bits 7�0) data swapped to byte 1, byte 0,
  //         byte 3 and byte 2 respectively.
  //         SWAP_SEL = 1, byte 3, byte 2, byte 1,
  //         byte 0 data swapped to byte 0, byte 1, byte 2 and byte 3 respectively.
  // bit 16: Reverse Vertical Scan, 0 normal direction, 1 reverse direction
  // bit 15: ACD Clock Source Select,
  //         0 Use FRM as clock source for ACD count
  //         1 Use LP/HSYN as clock source for ACD count
  // bits 14�8: Alternate Crystal Direction.
  // bit 7: LSCLK Select mode
  //        0 Disable OE and LSCLK in TFT mode when no data output
  //        1 Always enable LSCLK in TFT mode even if there is no data output
  // bit 6: Sharp Panel Enable, 0 Disable Sharp signals, 1 Enable Sharp signals
  // bits 5�0: Pixel Clock Divider, LCDC_CLK = LSCLK / (PDC + 1)
  //           PCD value must be set such that LSCLK frequency is no more than
  //           one third or one fourth of HCLK frequency
  LPCR = 0x20020000;

  // Bits 31�26: Horizontal Sync Pulse Width(min 50ns)
  // Bits 15�8: Wait_1 Between OE and HSYNC
  // Bits 7�0: Wait_2 Between OE and HSYNC
  LHCR = 0x10000000;

  // Bits 31�26: Vertical Sync Pulse Width
  // Bits 15�8: Wait_1 Between Frames 1
  // Bits 7�0: Wait_2 Between Frames 2
  LVCR = 0;

  // Bit 31: Dynamic DMA burst lengt
  // 0 - Dynamic(recommended), 1 - Fixed(reset value)
  LDCR = 0;

  // Bit 0: Turn off self refresh
  LRMCR = 0;
}

// Refreshes display buffer to display
// GuiLib_DisplayBuf is directly refreshed with lcd controller
void GuiDisplay_Refresh (void)
{
  // No action
}

#endif // LCD_CONTROLLER_TYPE_MX257

#ifdef LCD_CONTROLLER_TYPE_NJU6450A

// ============================================================================
//
// NJU6450A DISPLAY CONTROLLER
//
// This driver uses two NJU6450A display controllers, with each controlling
// half (left or right) of the display.
// LCD display 8-bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 122x32 pixels.
//
// Compatible display controllers:
//   AX6120, NJU6450, NJU6452, PT6520, SED1520
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 2
//   Number of display controllers, vertically: 1
//
// Port addresses (_P00, _P01, etc.) must be altered to correspond to your
// �-controller hardware and compiler syntax.
//
// ============================================================================

#define CTRL_BYTE_FIRST_PAGE 0xB8

#define A0_CMD      (P5OUT &= 0xFE)
#define A0_DATA     (P5OUT |= 0x01)
#define E1_ENABLE   (P5OUT |= 0x02)
#define E1_DISABLE  (P5OUT &= 0xFD)
#define E2_ENABLE   (P5OUT |= 0x04)
#define E2_DISABLE  (P5OUT &= 0xFB)
#define RW_WRITE    (P4OUT &= 0x7F)
#define RW_READ     (P4OUT |= 0x80)
#define DB_W        (P1OUT)
#define DB_R        (P1IN)

// Waits until controller 1 (left) is ready
// Remember to select display controller in advance
// Remember to deselect display controller afterwards
#define WAIT_1                                                           \
{                                                                        \
  P1DIR = 0x00;                                                          \
  E2_DISABLE;                                                            \
  do {                                                                   \
  E1_DISABLE;                                                            \
  E1_ENABLE;                                                             \
  RW_READ;                                                               \
  A0_CMD;                                                                \
  } while ((DB_R & 0x80) != 0);                                          \
  E1_DISABLE;                                                            \
  P1DIR = 0xFF;                                                          \
}

// Waits until controller 2 (right) is ready
// Remember to select display controller in advance
// Remember to deselect display controller afterwards
#define WAIT_2                                                           \
{                                                                        \
  P1DIR = 0x00;                                                          \
  E1_DISABLE;                                                            \
  do {                                                                   \
  E2_DISABLE;                                                            \
  E2_ENABLE;                                                             \
  RW_READ;                                                               \
  A0_CMD;                                                                \
  } while ((DB_R & 0x80) != 0);                                          \
  E2_DISABLE;                                                            \
  P1DIR = 0xFF;                                                          \
}

// COMMAND write
// Remember to select display controller in advance
// Remember to deselect display controller afterwards
#define WRITE_COMMAND_REG(val)                                           \
{                                                                        \
  RW_WRITE;                                                              \
  A0_CMD;                                                                \
  DB_W = val;                                                            \
}

// DATA write
// Remember to select display controller in advance
// Remember to deselect display controller afterwards
#define WRITE_DATA_REG(val)                                              \
{                                                                        \
  RW_WRITE;                                                              \
  A0_DATA;                                                               \
  DB_W = val;                                                            \
}

// CONTROLLER 1 (left) select
#define SELECT_CONTROLLER_1                                              \
{                                                                        \
  E2_DISABLE;                                                            \
  E1_ENABLE;                                                             \
}

// CONTROLLER 2 (right) select
#define SELECT_CONTROLLER_2                                              \
{                                                                        \
  E1_DISABLE;                                                            \
  E2_ENABLE;                                                             \
}

// CONTROLLERS deselect
#define DESELECT_CONTROLLERS                                             \
{                                                                        \
  E1_DISABLE;                                                            \
  E2_DISABLE;                                                            \
}

// CONTROLLERS select
#define SELECT_CONTROLLERS                                               \
{                                                                        \
  E1_ENABLE;                                                             \
  E2_ENABLE;                                                             \
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S X, Y;

  // Make sure both controllers are ready
  DESELECT_CONTROLLERS;
  WAIT_1;
  WAIT_2;

  // Display off while initializing
  SELECT_CONTROLLER_1;
  WRITE_COMMAND_REG(0xAE);
  SELECT_CONTROLLER_2;
  WRITE_COMMAND_REG(0xAE);
  DESELECT_CONTROLLERS;

  // Set display start line in RAM as 0
  SELECT_CONTROLLER_1;
  WRITE_COMMAND_REG(0xC0);
  SELECT_CONTROLLER_2;
  WRITE_COMMAND_REG(0xC0);
  DESELECT_CONTROLLERS;

  // Set static drive off
  SELECT_CONTROLLER_1;
  WRITE_COMMAND_REG(0xA4);
  SELECT_CONTROLLER_2;
  WRITE_COMMAND_REG(0xA4);
  DESELECT_CONTROLLERS;

  // Set duty cycle as 1/32
  SELECT_CONTROLLER_1;
  WRITE_COMMAND_REG(0xA9);
  SELECT_CONTROLLER_2;
  WRITE_COMMAND_REG(0xA9);
  DESELECT_CONTROLLERS;

  // Set ADC direction as rightward output
  SELECT_CONTROLLER_1;
  WRITE_COMMAND_REG(0xA0);
  SELECT_CONTROLLER_2;
  WRITE_COMMAND_REG(0xA0);
  DESELECT_CONTROLLERS;

  // Set read-modify-write off
  SELECT_CONTROLLER_1;
  WRITE_COMMAND_REG(0xEE);
  SELECT_CONTROLLER_2;
  WRITE_COMMAND_REG(0xEE);
  DESELECT_CONTROLLERS;

  // Clear display memory
  // Controllers 1 & 2
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)   // Loop through pages
  {
    // Select page
    SELECT_CONTROLLERS;
    WRITE_COMMAND_REG(CTRL_BYTE_FIRST_PAGE + Y);
    DESELECT_CONTROLLERS;

    // Select column 0
    SELECT_CONTROLLERS;
    WRITE_COMMAND_REG(0x00);
    DESELECT_CONTROLLERS;

    for (X = 0; X < GuiConst_BYTES_PR_SECTION; X++)   // Loop through bytes
    {
      SELECT_CONTROLLERS;
      WRITE_DATA_REG(0x00);
      DESELECT_CONTROLLERS;
    }
  }

  // Finished initializing, set display on
  SELECT_CONTROLLER_1;
  WRITE_COMMAND_REG(0xAF);
  SELECT_CONTROLLER_2;
  WRITE_COMMAND_REG(0xAF);
  DESELECT_CONTROLLERS;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S LineNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  // Walk through all lines
  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++)
  {
    if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      if (GuiLib_DisplayRepaint[LineNo].ByteBegin <
          GuiConst_BYTES_PR_SECTION)
      // Something to redraw in first section
      {
        // Select page
        SELECT_CONTROLLER_1;
        WRITE_COMMAND_REG(CTRL_BYTE_FIRST_PAGE + LineNo);
        DESELECT_CONTROLLERS;

        // Select starting byte
        SELECT_CONTROLLER_1;
        WRITE_COMMAND_REG(GuiLib_DisplayRepaint[LineNo].ByteBegin);
        DESELECT_CONTROLLERS;

        // Write display data
        LastByte = GuiConst_BYTES_PR_SECTION - 1;
        if (GuiLib_DisplayRepaint[LineNo].ByteEnd < LastByte)
          LastByte = GuiLib_DisplayRepaint[LineNo].ByteEnd;
        for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin;
             N <= LastByte;
             N++)
        {
          SELECT_CONTROLLER_1;
          WRITE_DATA_REG(GuiLib_DisplayBuf[LineNo][N]);
          DESELECT_CONTROLLERS;
        }

        // Reset repaint parameters
        if (GuiLib_DisplayRepaint[LineNo].ByteEnd >=
            GuiConst_BYTES_PR_SECTION)
          // Something to redraw in second section
          GuiLib_DisplayRepaint[LineNo].ByteBegin =
            GuiConst_BYTES_PR_SECTION;
        else // Done with this line
          GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
      }

      if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
        // Something to redraw in second section
      {
        // Select page
        SELECT_CONTROLLER_2;
        WRITE_COMMAND_REG(CTRL_BYTE_FIRST_PAGE + LineNo);
        DESELECT_CONTROLLERS;

        // Select starting byte
        SELECT_CONTROLLER_2;
        WRITE_COMMAND_REG(
           GuiLib_DisplayRepaint[LineNo].ByteBegin - GuiConst_BYTES_PR_SECTION);
        DESELECT_CONTROLLERS;

        // Write display data
        for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin;
             N <= GuiLib_DisplayRepaint[LineNo].ByteEnd; N++)
        {
          SELECT_CONTROLLER_2;
          WRITE_DATA_REG(GuiLib_DisplayBuf[LineNo][N]);
          DESELECT_CONTROLLERS;
        }

        // Reset repaint parameters
        GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
      }
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_NJU6450A

#ifdef LCD_CONTROLLER_TYPE_PCF8548

// ============================================================================
//
// PCF8548 DISPLAY CONTROLLER
//
// This driver uses one PCF8548 controller with on-chip generation of LCD supply
// and bias voltages.
// LCD display I2C bus interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 102x65 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at the bottom
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (P_x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Set pin addresses according your hardware
#define RES_0         P_X &= ~0x01;
#define RES_1         P_X |=  0x01;
#define SDA_1         P_Y |=  0x02;
#define SDA_0         P_Y &= ~0x02;
#define SCL_1         P_Z |=  0x04;
#define SCL_0         P_Z &= ~0x04;

// Bit handling macros
#define SETBIT(x,y)   (x |= (y))
#define CLRBIT(x,y)   (x &= (~(y)))
#define CHKBIT(x,y)   (x & (y))
#define FLPBIT(x,y)   (x ^= (y))

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// I2C start routine
void I2CSTART(void)
{
  SCL_1;
  SDA_1;
  SDA_0;
  millisecond_delay(50);
  SCL_0;
  millisecond_delay(5);
}

// I2C stop routine
void I2CSTOP(void)
{
  SDA_0;
  SCL_0;
  SCL_1;
  millisecond_delay(50);
  SDA_1;
  millisecond_delay(5);
}

// I2C write one byte routine
void I2CWRITE(GuiConst_INT8U data)
{
  GuiConst_INT8U bit;

  // send 7.bit
  if(CHKBIT(data,0x80))
    SDA_1;
  else
    SDA_0;

  millisecond_delay(5);
  SCL_1;
  millisecond_delay(50);
  SCL_0;
  millisecond_delay(5);

  // send other bits
  for(bit=0;bit<7;bit++)
  {
    data=(data<<1);

    if(CHKBIT(data,0x80))
    {
      SDA_1;
    }
    else
    {
      SDA_0;
    }

    millisecond_delay(5);
    SCL_1;
    millisecond_delay(50);
    SCL_0;
    millisecond_delay(5);
  }

  millisecond_delay(50);

  SCL_1;
  millisecond_delay(50);
  SCL_0;
  millisecond_delay(50);
  SDA_0;
  millisecond_delay(5);
}

// Initialize the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT16U i;

  // Initialize display
  millisecond_delay(10);
  RES_0;
  millisecond_delay(10);
  RES_1;
  SCL_1;
  SDA_1;

  I2CSTART();
  I2CWRITE(0x78);   // ADDRESS
  I2CWRITE(0x00);   // D/C=0
  I2CWRITE(0x20);   // MX=0,MY=0,PD=0,V=0,H=0
  I2CWRITE(0x05);   // PRS=0
  I2CWRITE(0x0C);   // D=1, E=0
  I2CWRITE(0x11);   // S1=0,S0=1
  I2CWRITE(0x21);   // MX=0,MY=0,PD=0,V=0,H=1
  I2CWRITE(0x05);   // TC1=0,TC0=1
  I2CWRITE(0x09);   // DO=0,TRS=0,BRS=1
  I2CWRITE(0x14);   // BS2=1,BS1=0,BS0=0
  I2CWRITE(0xA0);   // VOP=20H
  I2CWRITE(0x30);   // MX=1,MY=0,PD=0,V=0,H=0
  I2CWRITE(0x40);   // Y address=0
  I2CWRITE(0x80);   // X address=0
  I2CSTOP();

  // Clear display
  I2CSTART();
  I2CWRITE(0x78);   // I2C ADDRESS
  I2CWRITE(0x40);   // D/C=1 DATA
  for (i = 0; i < 1000; i++)
    I2CWRITE(0x00); // Clear byte
  I2CSTOP();

}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT8U PageNo;
  GuiConst_INT8U ColNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      I2CSTART();
      I2CWRITE(0x78); // ADDRESS
      I2CWRITE(0x00); // D/C=0

      // Select page
      I2CWRITE(0x40 + PageNo);

      // Select column
      ColNo = GuiLib_DisplayRepaint[PageNo].ByteBegin;
      I2CWRITE(0x84 + ColNo);
      I2CSTOP();

      I2CSTART();
      I2CWRITE(0x78); // ADDRESS
      I2CWRITE(0x40); // D/C=1

      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        I2CWRITE(GuiLib_DisplayBuf[PageNo][N]);

      I2CSTOP();

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }
  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_PCF8548

#ifdef LCD_CONTROLLER_TYPE_PCF8813

// ============================================================================
//
// PCF8813 DISPLAY CONTROLLER
//
// This driver uses one PCF8813 display controller with on-chip oscillator.
// LCD display in 8 bit parallel 8080 type interface or SPI interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 102x68 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at bottom
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Function Spi_SendData or port pins must be altered to correspond to your
// �-controller hardware and compiler syntax.
//
// ============================================================================

// Hardwired connection
// PS2, PS1, PS0
//  0    1    0  8080 type parallel
//  0    0    1  4 line SPI

#define DISP_DATA                 Pxx

#define RESET_ON                  Pxx.x = 0     // RES pin
#define RESET_OFF                 Pxx.x = 1     // RES pin
#define CHIP_SELECT               Pxx.x = 0     // SCE/SCLH pin
#define CHIP_DESELECT             Pxx.x = 1     // SCE/SCLH pin
#define MODE_DATA                 Pxx.x = 1     // DC pin
#define MODE_COMMAND              Pxx.x = 0     // DC pin
#define MODE_WRITE_OFF            Pxx.x = 1     // R/W/WR pin
#define MODE_WRITE_ON             Pxx.x = 0     // R/W/WR pin
#define MODE_READ_OFF             Pxx.x = 1     // E/RD pin
#define MODE_READ_ON              Pxx.x = 0     // R/RD pin
#define NOP                       asm ("nop")   // nop code - adjust according
                                                // to your compiler syntax
void Spi_SendData(void* LCD_DestAddress,
                  void *LCD_SrcAddress,
                  GuiConst_INT16U LCD_DataLength)
{
  // Something hardware related
}

void lcdPortWriteByte(GuiConst_INT8U A0,
                      GuiConst_INT8U v)
{
  // 8 bit parallel 8080 type interface
  MODE_READ_ON;
  if(A0)
    MODE_DATA;
  else
    MODE_COMMAND;
  CHIP_SELECT;
  MODE_WRITE_ON;
  DISP_DATA = v;
  // NOP;
  MODE_WRITE_OFF;
  CHIP_DESELECT;

  // Spi interface
  //Spi_SendData(&A0,&v,1);
}

#define lcdExtendedInstruction             lcdPortWriteByte(0,0x21)
#define lcdStandardInstruction             lcdPortWriteByte(0,0x20)
#define lcdHigherPRS                       lcdPortWriteByte(0,0x11)
#define lcdSetVPR(vpr)                     lcdPortWriteByte(0,0x80 | ((vpr) & 0x7F))
#define lcdSetHVG(stage)                   lcdPortWriteByte(0,0x08 | ((stage) & 0x03))
#define lcdSetBias(bias)                   lcdPortWriteByte(0,0x10 | ((bias) & 0x07))
#define lcdSetTC(temp)                     lcdPortWriteByte(0,0x04 | ((temp) & 0x03))
#define lcdDisplayOn                       lcdPortWriteByte(0,0x0c)
#define lcdSelectPage(pageno)              lcdPortWriteByte(0,0x40 | ((pageno) & 0x0f))
#define lcdSelectColumn(columnno)          lcdPortWriteByte(0,0x80 | ((columnno) & 0x7f))
#define lcdDataByte(dataval)               lcdPortWriteByte(1,dataval)

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  // Reset controller
  RESET_ON;
  millisecond_delay(10);
  RESET_OFF;

  lcdExtendedInstruction;
  lcdHigherPRS;
  lcdSetVPR(16);
  lcdStandardInstruction;
  lcdDisplayOn;

  // Clear display memory
  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Select page
    lcdSelectPage(PageNo);
    // Select column
    lcdSelectColumn(0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
      lcdDataByte(0x00);
  }
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      lcdSelectPage(PageNo);

      // Select column
      lcdSelectColumn(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_PCF8813

#ifdef LCD_CONTROLLER_TYPE_PIC24FJ256

// ============================================================================
//
// PIC24FJ256 DISPLAY CONTROLLER
//
// This driver uses the internal GFX display controller of PIC24FJ256
// microcontroller without Graphics Processing Units.
// LCD display in 16 bit RGB parallel interface RGB(565).
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at left
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// =========================================================================
#include <p24Fxxxx.h>

// Mapping of display buffer into easyGui display buffer
#define BUFFER_ADDRESS ((GuiConst_INT32U)&GuiLib_DisplayBuf.Words[0][0])

#define H_Front_Porch 10   // clocks
#define H_Back_Porch  20   // clocks
#define H_Sync_Pulse  10   // clocks
#define V_Front_Porch 2    // lines
#define V_Back_Porch  1    // lines
#define V_Sync_Pulse  3    // lines

// Display Glass Type bits
// 3 = Color STN type
// 2 = Mono STN type
// 1 = TFT type
// 0 = Display off
#define DPMODE(n)         (n << 0)
//Display bits-per-pixel Setting bits
// 4 = 16 bits-per-pixel
// 3 = 8 bits-per-pixel
// 2 = 4 bits-per-pixel
// 1 = 2 bits-per-pixel
// 0 = 1 bit-per-pixel
#define DPBPP(n)          (n << 5)
// Pixel clock polarity: 0 - data is sampled on positive edge, 1 - on negative edge
#define DPCLKPOL(n)       (n << 7)
// Display Enable Port Enable (GEN) bit, 1 = GEN port enabled, 0 = GEN port disabled
#define DPENOE(n)         (n << 2)
// Display Enable Signal (GEN) Polarity bit, 1 = Active-high, 0 = Active-low
#define DPENPOL(n)        (n << 6)
// Display Vertical Synchronization Port Enable bit, 1 = VSYNC enabled, 0 = VSYNC disabled
#define DPVSOE(n)         (n << 1)
// Display Horizontal Synchronization Port Enable bit, 1 = HSYNC enabled, 0 = HSYNC disabled
#define DPHSOE(n)         (n << 0)
// Display VSYNC Polarity bit, 1 = Active-high, 0 = Active-low
#define DPVSPOL(n)        (n << 5)
// Display HSYNC Polarity bit, 1 = Active-high, 0 = Active-low (HSYNC)
#define DPHSPOL(n)        (n << 4)
// Number of Lines Before the First Active (displayed) Line bits
#define ACTLINE           (V_Front_Porch + V_Back_Porch + V_Sync_Pulse)
// Number of Pixels Before the First Active (displayed) Pixel bits (in DISPCLKs)
#define ACTPIX            (H_Front_Porch + H_Back_Porch + H_Sync_Pulse)
// Display Pin Output Pad Enable bit, 1 = Enable pads, 0 = Disable pads
#define DPPINOE(n)        (n << 9)
// Display Power Sequencer Control bit
// 1 = Set display power sequencer control port (GPWR) to �1�
// 0 = Set display power sequencer control port (GPWR) to �0�
#define DPPOWER(n)        (n << 8)
// Display Power Sequencer Control Port (GPWR) Enable bit
// 1 = GPWR port enabled
// 0 = GPWR port disabled
#define DPPWROE(n)        (n << 3)
// Module Enable bit, 1 = Display module enabled, 0 = Display module disabled
#define G1EN(n)           (n << 15)

void GuiDisplay_Init(void)
{
  // NOTE !!!
  // If you use external memory you must initialize it before!

  // Switch off the module
  G1CON1   = 0;
  G1CON2   = 0;
  G1CON3   = 0;
  G1IE     = 0;
  G1IR     = 0;
  G1CLUT   = 0;
  G1MRGN   = 0;
  G1CLUTWR = 0;
  G1CHRX   = 0;
  G1CHRY   = 0;
  G1CMDL   = 0;
  G1CMDH   = 0;
  G1ACTDA  = 0;
  G1VSYNC  = 0;
  G1HSYNC  = 0;
  G1DBLCON = 0;

  // Display Buffer Start Address Register Low
  G1DPADRL = (GuiConst_INT16U)BUFFER_ADDRESS;
  // Display Buffer Start Address Register High
  G1DPADRH =(GuiConst_INT16U)(BUFFER_ADDRESS >> 16);

  // Display Control Register 2
  G1CON2 = DPBPP(4) | DPMODE(1);

  // Data I/O Pad Enable Register
  G1DBEN = 0xFFFF; // Use all data pins

  // Display Buffer Width Register
  G1DPW = GuiConst_DISPLAY_WIDTH_HW;

  // Display BUFFER Height Register
  G1DPH = GuiConst_DISPLAY_HEIGHT_HW;

  // Display Total Width Register
  G1DPWT = GuiConst_DISPLAY_WIDTH_HW + H_Front_Porch +
           H_Back_Porch + H_Sync_Pulse;

  // Display Total Height Register
  G1DPHT = GuiConst_DISPLAY_HEIGHT_HW + V_Front_Porch +
           V_Back_Porch+ V_Sync_Pulse;

  // Display Control Register 3
  G1CON3 = DPCLKPOL(0) | DPENOE(1) | DPENPOL(1) | DPVSOE(1) | DPHSOE(1) |
           DPVSPOL(0) | DPHSPOL(0);

  // Active Display Area Register
  G1ACTDA = (ACTLINE << 8) | ACTPIX;

  // Vertical Synchronization Control Register
  G1VSYNC = (V_Sync_Pulse << 8) | V_Front_Porch;

  // Horizontal Synchronization Control Register
  G1HSYNC = (H_Sync_Pulse << 8)  | H_Front_Porch;

  // Display Blanking Control Register
  G1DBLCON = (ACTLINE << 8) | ACTPIX;

  // Display Control Register 3
  //  Enable pin output pads.
  G1CON3 |= DPPINOE(1);

  // Display Control Register 3
  // Set display power signal to �1�.
  G1CON3 |= DPPOWER(1);

  // Display Control Register 3
  // Use the GPWR pin as the display power signal.
  G1CON3 |= DPPWROE(1);

  // Display Control Register 1
  // Enable display controller
  G1CON1 =  G1EN(1);
}

// Refreshes display buffer to display
// easyGui display buffer is mapped into frame buffer of PIC24JF256,
// Refresh routine is therefore not used.
void GuiDisplay_Refresh (void)
{
  // No action
}

#endif //  LCD_CONTROLLER_TYPE_PIC24FJ256

#ifdef LCD_CONTROLLER_TYPE_PXA320

// ============================================================================
//
// PXA320 DISPLAY CONTROLLER
//
// This driver uses the internal display controller of PXA320 microcontroller.
// LCD display in 16 bit RGB parallel interface RGB(565) with only base plane,
// no overlay and cursor planes.
// Modify driver accordingly for other interface types.
// Graphic modes up to 800x480 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at left
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// =========================================================================
//
// Hardwired connection
// LDD(0-4)   - B(0-4)
// LDD(5-10)  - G(0-5)
// LDD(11-15) - R(0-4)
// FCLK_RD    - VSYNC
// LCLK_A0    - HSYNC
// PCLK_WR    - DCLK
// L_BIAS     - DTMG
//
// Adjust start address of frame buffer according your needs
// The address must be aligned on 128 bit(16 byte)boundary - bits 4-0 must be zeros
#define FRAME_BUFFER_START 0x84000000 // DDR SDRAM memory Chip select 0

#define REG(x)            (*((volatile GuiConst_INT32U *)x))
#define CLEAR_REG         0x00000000
// D0 clock enable register
#define D0CKEN_A          REG(0x4134000C)  // Clock Enable Register
// LCD registes
#define LCCR0             REG(0x44000000)  // LCD Controller Control Register 0
#define LCCR1             REG(0x44000004)  // LCD Controller Control Register 1
#define LCCR2             REG(0x44000008)  // LCD Controller Control Register 2
#define LCCR3             REG(0x4400000C)  // LCD Controller Control Register 3
#define DFBR0             REG(0x44000020)  // DMA Channel 0 Frame Branch Register
#define DFBR1             REG(0x44000024)  // DMA Channel 1 Frame Branch Register
#define LCSR              REG(0x44000038)  // LCD Controller Status Register
#define LCSR0             REG(0x44000038)  // LCD Controller Status Register
#define LCSR1             REG(0x44000034)  // LCD Controller Status Register
#define LIIDR             REG(0x4400003C)  // LCD Controller Interrupt ID Register
#define TMEDRGBR          REG(0x44000040)  // TMED RGB Seed Register
#define TMEDCR            REG(0x44000044)  // TMED Control Register
// DMA registers
#define FDADR0            REG(0x44000200)  // DMA Channel 0 Frame Descriptor Address Register
#define FSADR0            REG(0x44000204)  // DMA Channel 0 Frame Source Address Register
#define FIDR0             REG(0x44000208)  // DMA Channel 0 Frame ID Register
#define LDCMD0            REG(0x4400020C)  // DMA Channel 0 Command Register
#define FDADR1            REG(0x44000210)  // DMA Channel 1 Frame Descriptor Address Register
#define FSADR1            REG(0x44000214)  // DMA Channel 1 Frame Source Address Register
#define FIDR1             REG(0x44000218)  // DMA Channel 1 Frame ID Register
#define LDCMD1            REG(0x4400021C)  // DMA Channel 1 Command Register
// Multi-function pin registers
#define GPIO6_2_LDD_0     REG(0x40E10494) // LCD data pin 0
#define GPIO7_2_LDD_1     REG(0x40E10498) // LCD data pin 1
#define GPIO8_2_LDD_2     REG(0x40E1049C) // LCD data pin 2
#define GPIO9_2_LDD_3     REG(0x40E104A0) // LCD data pin 3
#define GPIO10_2_LDD_4    REG(0x40E104A4) // LCD data pin 4
#define GPIO11_2_LDD_5    REG(0x40E104A8) // LCD data pin 5
#define GPIO12_2_LDD_6    REG(0x40E104AC) // LCD data pin 6
#define GPIO13_2_LDD_7    REG(0x40E104B0) // LCD data pin 7
#define GPIO63_LDD_8      REG(0x40E104B4) // LCD data pin 8
#define GPIO64_LDD_9      REG(0x40E104B8) // LCD data pin 9
#define GPIO65_LDD_10     REG(0x40E104BC) // LCD data pin 10
#define GPIO66_LDD_11     REG(0x40E104C0) // LCD data pin 11
#define GPIO67_LDD_12     REG(0x40E104C4) // LCD data pin 12
#define GPIO68_LDD_13     REG(0x40E104C8) // LCD data pin 13
#define GPIO69_LDD_14     REG(0x40E104CC) // LCD data pin 14
#define GPIO70_LDD_15     REG(0x40E104D0) // LCD data pin 15
#define GPIO71_LDD_16     REG(0x40E104D4) // LCD data pin 16
#define GPIO72_LDD_17     REG(0x40E104D8) // LCD data pin 17
#define GPIO14_2_LCD_FCLK REG(0x40E104E0) // LCD Frame clock
#define GPIO15_2_LCD_LCLK REG(0x40E104E4) // LCD line clock
#define GPIO16_2_LCD_PCLK REG(0x40E104E8) // LCD Pixel clock
#define GPIO17_2_LCD_BIAS REG(0x40E104EC) // LCD AC bias
//
#define LCDCLK_EN         (1 << 1)        // Enable lcd clock
//
#define PDFOR_1           (0 << 30)       // Pixel data format 1 - 16 bit, no transparency
#define PDFOR_2           (1 << 30)       // Pixel data format 2
#define PDFOR_3           (2 << 30)       // Pixel data format 3
#define PDFOR_4           (3 << 30)       // Pixel data format 4
// Display color modes
#define LCCR3_1BPP        (0 << 24)
#define LCCR3_2BPP        (1 << 24)
#define LCCR3_4BPP        (2 << 24)
#define LCCR3_8BPP        (3 << 24)
#define LCCR3_16BPP       (4 << 24)
#define LCCR3_18BPP       (6 << 24)       // Packed pixel format
#define LCCR3_19BPP       (8 << 24)       // Packed pixel format
#define LCCR3_24BPP       (9 << 24)
#define LCCR3_25BPP       (10<< 24)
//
#define OUTPUT_EN_POL(n)  (n << 23)       // 0 - L_BIAS active high, 1 - L_BIAS active low
// Pixel clock polarity: 0 - data is sampled on rising edge, 1 - on falling edge
#define PCP(n)            (n << 22)
// Horizontal sync polarity: 0 - active high, 1 - active low
#define HSP(n)            (n << 21)
// Vertical sync polarity: 0 - active high, 1 - active low
#define VSP(n)            (n << 20)
// Pixel clock frequency = LCD controller freq / (2 * PCD + 1)
#define PCD               0               // Pixel clock divisor range 0 - 255
// Display timing parameters
#define BFW               10              // Vertical Back Porch
#define EFW               10              // Vertical Front Porch
#define VSW               10              // Vertical Sync Pulse Width
#define LPP               (GuiConst_DISPLAY_HEIGHT_HW - 1) // Lines per panel
#define BLW               10              // Horizontal Back Porch
#define ELW               10              // Horizontal Front Porch
#define HSW               10              // Horizontal Sync Pulse Width
#define PPL               (GuiConst_DISPLAY_WIDTH_HW - 1) // Pixels per line
// Control parameters in LCCR0 register
#define ENB               (1 << 0)        // LCD Controller enable
#define LDM               (1 << 3)        // LCD Disable Done Mask
#define SOFM0             (1 << 4)        // Start of frame mask
#define IUM               (1 << 5)        // Input FIFO underrun mask
#define EOFM0             (1 << 6)        // End of Frame mask
#define LCD_ACT           (1 << 7)        // Active display Select
#define DIS               (1 << 10)       // LCD Disable
#define QDM               (1 << 11)       // LCD Quick Disable mask
#define PDD(n)            (n << 12)       // Palette DMA request delay
#define BSM0              (1 << 20)       // Branch interrupt mask
#define OUM               (1 << 21)       // Output FIFO underrun mask
#define LCDT              (1 << 22)       // LCD panel type with internal buffer
#define RDSTM             (1 << 23)       // Read status interrupt mask
#define CMDIM             (1 << 24)       // Command interrupt mask
#define OUC               (1 << 25)       // Overlay Underlay control bit
#define LDDALT            (1 << 26)       // LDD alternate mapping control RGB(565)
#define DELAY_BIAS        (1 << 27)       // L_BIAS is delay by 1 LCD clock in active mode
// 0 - Pull-up and pulll-down are controlled with alternate function
// 1 - Pull-up and pulll-down are controlled with pull enablinf bits
#define PUL_SELL_FUNCT(n) (n << 15)
// 0 - fast 1mA, 1 - fast 2mA, 2 - fast 3mA, 3 - fast 4mA,
// 4 - slow 6mA, 5 - fast 6mA, 6 - slow 10mA, 7 fast 10mA
#define DRIVE_STRENGHT(n) (n << 10)
#define SLEEP_OE_N(n)     (n << 7)        // 0 - enable output during sleep modes, 1 - disable
#define EDGE_CLEAR(n)     (n << 6)        // 0 - enable edge detection , 1 - disable
#define ALTER_FUNCT(n)    (n << 0)        // Range 0 - 7, 1 - LCD interface function

// PXA LCD DMA descriptor
// The address of descriptor must be aligned on 128 bit(16 byte)boundary -
// bits 4-0 must be zeros
struct dma_descriptor{
  GuiConst_INT32U fdadr;
  GuiConst_INT32U fsadr;
  GuiConst_INT32U fidr;
  GuiConst_INT32U ldcmd;
}dma_descriptor_0;

void GuiDisplay_Init(void)
{
  GuiConst_INT32U i, temp;
  GuiConst_INT32U *fbp;

  // Set pointer to frame buffer
  fbp = (GuiConst_INT32U *)FRAME_BUFFER_START;

  // Initialize power,clock modules and sdram interface at first!!

  // Pin direction is controlled by hardware with selecting alternative function
  GPIO6_2_LDD_0     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO7_2_LDD_1     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO8_2_LDD_2     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO9_2_LDD_3     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO10_2_LDD_4    = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO11_2_LDD_5    = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO12_2_LDD_6    = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO13_2_LDD_7    = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO63_LDD_8      = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO64_LDD_9      = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO65_LDD_10     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO66_LDD_11     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO67_LDD_12     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO68_LDD_13     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO69_LDD_14     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO70_LDD_15     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO71_LDD_16     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO72_LDD_17     = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO14_2_LCD_FCLK = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO15_2_LCD_LCLK = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO16_2_LCD_PCLK = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);
  GPIO17_2_LCD_BIAS = CLEAR_REG | DRIVE_STRENGHT(2) | EDGE_CLEAR(1) | ALTER_FUNCT(1);

  // Enable clock for LCD module
  temp = D0CKEN_A;
  temp |= LCDCLK_EN;
  D0CKEN_A = temp;

  // Disable LCD controller before setting
  LCCR0 &= ~ENB;

  LCCR3 = CLEAR_REG | PDFOR_1 | LCCR3_16BPP | PCP(1) | HSP(1)| VSP(1)| PCD;
  LCCR2 = CLEAR_REG | (BFW << 24) | (EFW << 16) | (VSW << 10) | LPP;
  LCCR1 = CLEAR_REG | (BLW << 24) | (ELW << 16) | (HSW << 10) | PPL;

  // Fill descriptor with data for base channel
  dma_descriptor_0.fdadr = (GuiConst_INT32U)&dma_descriptor_0;
  dma_descriptor_0.fsadr = FRAME_BUFFER_START;
  dma_descriptor_0.fidr = FRAME_BUFFER_START;
  dma_descriptor_0.ldcmd = 0;

  // Set address of first descriptor for base channel
  FDADR0 = (GuiConst_INT32U)&dma_descriptor_0;

  // Set LCCR0 reginster and enable LCD module
  LCCR0 = CLEAR_REG | LDDALT | CMDIM | RDSTM | OUM | BSM0 | QDM |
          LCD_ACT | EOFM0 | IUM | SOFM0 | LDM | ENB;

  // Clear frame buffer
  for (i = 0; i < (GuiConst_DISPLAY_BYTES / 4); i++)
    *fbp++ = 0x00000000;
}


// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT32S X;
  GuiConst_INT32S Y;
  GuiConst_INT32S LastByte;
  GuiConst_INT32U Offset;
  GuiConst_INT32U* lcd_memory_ptr;
  GuiConst_INT32U i;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Offset Pointer Set
      Offset = Y * GuiConst_BYTES_PR_LINE +
               GuiLib_DisplayRepaint[Y].ByteBegin * 2;

      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Pointer to start of frame buffer in memory
      lcd_memory_ptr = (GuiConst_INT32U*)FRAME_BUFFER_START;
      lcd_memory_ptr += Offset;

      for (X = (GuiLib_DisplayRepaint[Y].ByteBegin & 0xfffffffe);
           X <= LastByte; X += 2)
      {
        // Prepare 32bit value for writing to 32bit SDRAM
        i = GuiLib_DisplayBuf.Words[Y][X];
        i |= GuiLib_DisplayBuf.Words[Y][X+1]<<16;
        *lcd_memory_ptr++ = i;
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock ();
}
#endif // LCD_CONTROLLER_TYPE_PXA320

#ifdef LCD_CONTROLLER_TYPE_R61509

// ============================================================================
//
// R61509 DISPLAY CONTROLLER
//
// This driver uses one R61509 display controller with on-chip oscillator.
// LCD display 8 or 16 bit 80-system parallel interface, 2 or 1 transfer mode.
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x432 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at left
//   Number of color planes: 1
//   Color mode: Grayscale or RGB
//   Color depth: 1, 2 or 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired connection
// IM2 = 0, IM1 = 1, IM0 = 0: 80-system 16 bit interface
// IM2 = 0, IM1 = 1, IM0 = 1: 80-system 8 bit interface
// For 8 bit interface connect unused pins D8-D1 to 0 or 1

#define DISP_DATA            Pxx           // PIN D17 - D10, D8-D1 for 16 bit
                                           // interface
                                           // PIN D17 - D10 for 8 bit interface

#define RESET_ON             Pxx.x = 0     // RESET pin
#define RESET_OFF            Pxx.x = 1     // RESET pin
#define CHIP_SELECT          Pxx.x = 0     // CS pin
#define CHIP_DESELECT        Pxx.x = 1     // CS pin
#define MODE_DATA            Pxx.x = 1     // RS pin
#define MODE_COMMAND         Pxx.x = 0     // RS pin
#define MODE_WRITE_OFF       Pxx.x = 1     // WR pin
#define MODE_WRITE_ON        Pxx.x = 0     // WR pin
#define MODE_READ_OFF        Pxx.x = 1     // RD pin
#define MODE_READ_ON         Pxx.x = 0     // RD pin
#define NOP                  asm ("nop")   // nop code - adjust according to
                                           // your compiler syntax

/*
// Command writing for 16 bit interface
#define LCD_Cmd(Val)                                                     \
{                                                                        \
  MODE_COMMAND;                                                          \
  DISP_DATA = Val;                                                       \
  CHIP_SELECT;                                                           \
  MODE_WRITE_ON;                                                         \
  MODE_WRITE_OFF;                                                        \
  CHIP_DESELECT;                                                         \
}

// Data writing for 16 bit interface
#define LCD_Data(Val)                                                    \
{                                                                        \
  MODE_DATA;                                                             \
  DISP_DATA = Val;                                                       \
  CHIP_SELECT;                                                           \
  MODE_WRITE_ON;                                                         \
  MODE_WRITE_OFF;                                                        \
  CHIP_DESELECT;                                                         \
}
*/

// Command writing for 8 bit interface
void LCD_Cmd(GuiConst_INT16U Val)
{
  MODE_COMMAND;
  DISP_DATA = (GuiConst_INT8U)(Val>>8);
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  NOP;   // Some delay

  DISP_DATA = (GuiConst_INT8U)(Val);
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

// Data writing for 8 bit interface
void LCD_Data(GuiConst_INT16U Val)
{
  MODE_DATA;
  DISP_DATA = (GuiConst_INT8U)(Val>>8);
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  NOP;   // Some delay

  DISP_DATA = (GuiConst_INT8U)(Val);
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

// Clear display
void LCD_CLS(GuiConst_INT16U color)
{
  GuiConst_INT32U i, j;

  i = GuiConst_DISPLAY_WIDTH * GuiConst_DISPLAY_HEIGHT;

  for (j = 0; j < i; j++)
    LCD_Data(color);
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void GuiDisplay_Init(void)
{
  MODE_READ_OFF;  // We don't use read signal

  RESET_ON;
  millisecond_delay(10);
  RESET_OFF;

  // Data transfer synchronisation only in 8 bit interface
  // For 16 bit interface comment it
  LCD_Cmd(0x0000);
  LCD_Cmd(0x0000);

  // Start screen initialization

  // Driver output
  LCD_Cmd(0x0001);
  LCD_Data(0x0100);

  // LCD driving wave control
  LCD_Cmd(0x0002);
  LCD_Data(0x0100);

  // Entry mode
  LCD_Cmd(0x0003);
  LCD_Data(0x1030);

  // Display control 2
  LCD_Cmd(0x0008);
  LCD_Data(0x0808);

  // Low power control 2
  LCD_Cmd(0x000B);
  LCD_Data(0x0010);

  // External display interface co
  LCD_Cmd(0x000F);
  LCD_Data(0x0000);

  // Panel interface control 1
  LCD_Cmd(0x0010);
  LCD_Data(0x001F);

  // Panel interface control 2
  LCD_Cmd(0x0011);
  LCD_Data(0x0001);

  // Panel interface control 3
  LCD_Cmd(0x0012);
  LCD_Data(0x0000);

  // Panel interface control 4
  LCD_Cmd(0x0020);
  LCD_Data(0x021E);

  // Panel interface control 5
  LCD_Cmd(0x0021);
  LCD_Data(0x0000);

  // Panel interface control 6
  LCD_Cmd(0x0022);
  LCD_Data(0x0500);

  // Window horizontal start RAM address
  LCD_Cmd(0x0210);
  LCD_Data(0x0000);

  // Window horizontal stop RAM address
  LCD_Cmd(0x0211);
  LCD_Data(0x00EF);

  // Window vertical start RAM address
  LCD_Cmd(0x0212);
  LCD_Data(0x0000);

  // Window vertical stop RAM address
  LCD_Cmd(0x0213);
  LCD_Data(0x018F);

  // Gamma control 1
  LCD_Cmd(0x0300);
  LCD_Data(0x0706);

  // Gamma control 2
  LCD_Cmd(0x0301);
  LCD_Data(0x0607);

  // Gamma control 3
  LCD_Cmd(0x0302);
  LCD_Data(0x0301);

  // Gamma control 4
  LCD_Cmd(0x0303);
  LCD_Data(0x0202);

  // Gamma control 5
  LCD_Cmd(0x0304);
  LCD_Data(0x0303);

  // Gamma control 6
  LCD_Cmd(0x0305);
  LCD_Data(0x0207);

  // Gamma control 7
  LCD_Cmd(0x0306);
  LCD_Data(0x0808);

  // Gamma control 8
  LCD_Cmd(0x0307);
  LCD_Data(0x0706);

  // Gamma control 9
  LCD_Cmd(0x0308);
  LCD_Data(0x0607);

  // Gamma control 10
  LCD_Cmd(0x0309);
  LCD_Data(0x0301);

  // Gamma control 11
  LCD_Cmd(0x030A);
  LCD_Data(0x0303);

  // Gamma control 12
  LCD_Cmd(0x030B);
  LCD_Data(0x0202);

  // Gamma control 13
  LCD_Cmd(0x030C);
  LCD_Data(0x0207);

  // Gamma control 14
  LCD_Cmd(0x030D);
  LCD_Data(0x1F1F);

  // Base image number of line
  LCD_Cmd(0x0400);
  LCD_Data(0x3100);

  // Base image display control
  LCD_Cmd(0x0401);
  LCD_Data(0x0001);

  // Base image vertical scroll control
  LCD_Cmd(0x0404);
  LCD_Data(0x0000);

  // Display control 1
  LCD_Cmd(0x0007);
  LCD_Data(0x0001);

  // Power control 6
  LCD_Cmd(0x0110);
  LCD_Data(0x0001);

  // Power sequence control 1
  LCD_Cmd(0x0112);
  LCD_Data(0x0060);

  // Power control 1
  LCD_Cmd(0x0100);
  LCD_Data(0x17B0);

  // Power control 2
  LCD_Cmd(0x0101);
  LCD_Data(0x0007);

  // Power control 3
  LCD_Cmd(0x0102);
  LCD_Data(0x01A8);

  // Power control 4
  LCD_Cmd(0x0103);
  LCD_Data(0x2E00);

  // Vcom high voltage 1
  LCD_Cmd(0x0281);
  LCD_Data(0x0008);

  // Power control 3
  LCD_Cmd(0x0102);
  LCD_Data(0x01B8);

  millisecond_delay(150);

  // Display control 1
  LCD_Cmd(0x0007);
  LCD_Data(0x0021);

  millisecond_delay(1);

  // Power control 6
  LCD_Cmd(0x0110);
  LCD_Data(0x0001);

  // Power control 1
  LCD_Cmd(0x0100);
  LCD_Data(0x16B0);

  // Power control 2
  LCD_Cmd(0x0101);
  LCD_Data(0x0117);

  // Power control 3
  LCD_Cmd(0x0102);
  LCD_Data(0x01B8);

  // Power control 4
  LCD_Cmd(0x0103);
  LCD_Data(0x2E00);

  // Vcom high voltage 1
  LCD_Cmd(0x0281);
  LCD_Data(0x0008);

  // Display control 1
  LCD_Cmd(0x0007);
  LCD_Data(0x0061);

  millisecond_delay(50);

  // Display control 1
  LCD_Cmd(0x0007);
  LCD_Data(0x0173);

  // Initialize with black color
  LCD_Cmd(0x0202);
  LCD_CLS(0x0000);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;
#ifdef GuiConst_COLOR_DEPTH_1
  GuiConst_INT8U OneByte, i;
#else
#ifdef GuiConst_COLOR_DEPTH_2
  GuiConst_INT8U OneByte, i;
#endif
#endif

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(0x0201);
      LCD_Data(Y);

      // Select start column
      LCD_Cmd(0x0200);

#ifdef GuiConst_COLOR_DEPTH_1
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin * 8);
      LastByte = GuiConst_BYTES_PR_LINE - 1;
#else
#ifdef GuiConst_COLOR_DEPTH_2
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin * 4);
      LastByte = GuiConst_BYTES_PR_LINE - 1;
#else
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin);
      LastByte = GuiConst_BYTES_PR_LINE / 2 - 1;
#endif
#endif

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      LCD_Cmd(0x0202);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
#ifdef GuiConst_COLOR_DEPTH_1
        OneByte = GuiLib_DisplayBuf[Y][X];

        for( i = 0; i < 8; i++)
        {
          if (OneByte & 0x01)
            LCD_Data(0x0FFFF);
          else
            LCD_Data(0x0000);

          OneByte >>= 1;
        }
#else
#ifdef GuiConst_COLOR_DEPTH_2
        OneByte = GuiLib_DisplayBuf[Y][X];

        for( i = 0; i < 4; i++)
        {
          if ((OneByte & 0x03) == 0)
            LCD_Data(0x0000);
          else if ((OneByte & 0x03) == 1)
            LCD_Data(0xA534);
          else if ((OneByte & 0x03) == 2)
            LCD_Data(0x528A);
          else
            LCD_Data(0xFFFF);

          OneByte >>= 2;
        }
#else
        LCD_Data(GuiLib_DisplayBuf.Words[Y][X]);
#endif
#endif
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_R61509

#ifdef LCD_CONTROLLER_TYPE_RA8822

// ============================================================================
//
// RA8822 DISPLAY CONTROLLER
//
// This driver uses one RA8822 display controller with built-in PLL module.
// LCD display 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x160 pixels.
//
// Compatible display controllers:
//   RA8803
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Pin ports definitions
#define LCD_RS                Px.0
#define LCD_WR                Px.1
#define LCD_RD                Px.2
#define LCD_CS1               Px.3
#define LCD_CS2               Px.4
#define LCD_BUSY              Px.5
#define LCD_INT               Px.6
#define LCD_RST               Px.7
#define LCD_cmdReg            Py
#define LCD_cmdData           Py
#define LCD_DATA              Py
#define LCD_READY             Px

// Commands
#define CMD_CA_LSB            0x00
#define CMD_CA_MSB            0x10
#define CMD_MUX_TEMP          0x20
#define CMD_POWER             0x28
#define CMD_ADV_PROD_CONFIG   0x30
#define CMD_START_LINE        0x40
#define CMD_GAIN_POT          0x81
#define CMD_RAM_ADDR_CTRL     0x88
#define CMD_ALL_PIXEL_ON      0xA4
#define CMD_INVERSE           0xA6
#define CMD_DISABLE           0xAE
#define CMD_ENABLE            0xAF
#define CMD_PAGE_ADDR         0xB0
#define CMD_MAPPING_CTRL      0xC0
#define CMD_RESET             0xE2
#define CMD_NOP               0xE3
#define CMD_BIAS              0xE8
#define CMD_CURSOR_RESET      0xEE
#define CMD_CURSOR_SET        0xEF
#define CMD_TEST_CTRL         0xE4

// Time delay routine
void delay(int i)
{
  int k;

  for (k = 0; k < i; k++);
}

// Wait till driver is busy
void LCD_ChkBusy(void)
{
  do
  {
  }
  while (LCD_BUSY == 1);
}

// Write data to the driver register
void LCD_CmdWrite(GuiConst_INT8U cmdReg,GuiConst_INT8U cmdData)
{
  //LCD_ChkBusy();
  LCD_cmdReg = cmdReg;
  LCD_CS1 =0; // RA8803/8822 Chip Enable.
  LCD_RD = 1; //
  LCD_RS = 0; // RS = 0;
  LCD_WR = 0; // WR = 0;
  delay(10);
  LCD_WR = 1; // WR = 1;
  LCD_RS = 1; // RS = 1;
  LCD_CS1 =1; // RA8803/8822 Chip Disable.

  //LCD_ChkBusy();
  delay(1);
  LCD_cmdReg = cmdData;
  LCD_CS1 =0; // RA8803/8822 Chip Enable.
  LCD_RD = 1; //
  LCD_RS = 0; // RS = 0;
  LCD_WR = 0; // WR = 0;
  delay(10);
  LCD_WR = 1; // WR = 1;
  LCD_RS = 1; // RS = 1;
  LCD_CS1 =1; // RA8803/8822 Chip Disable.
  delay(1);
}

// Write data to the display RAM
void LCD_DataWrite(GuiConst_INT8U WrData)
{
  LCD_ChkBusy();
  LCD_DATA = WrData;
  LCD_CS1 =0; // RA8803/8822 Chip Enable.
  LCD_RD = 1; //
  LCD_RS = 1; // RS = 1;
  LCD_WR = 0; // WR = 0;
  LCD_WR = 0; // WR = 0;
  LCD_WR = 0; // WR = 0;
  delay(1);
  LCD_WR = 1; // WR = 1;
  LCD_RS = 1; // RS = 1;
  LCD_CS1 =1; // RA8803/8822 Chip Disable.
}

// Read value from register
GuiConst_INT8U LCD_CmdRead(GuiConst_INT8U cmdReg)
{
  GuiConst_INT8U REG_Read;

  LCD_ChkBusy();
  LCD_cmdReg = cmdReg;
  LCD_CS1 =0;   // RA8803/8822 Chip Enable.
  LCD_RD = 1;   //
  LCD_RS = 0;   // RS = 0;
  LCD_WR = 0;   // WR = 0;
  delay(10);
  LCD_WR = 1;   // WR = 1;
  LCD_RS = 1;   // RS = 1;
  LCD_CS1 =1;   // RA8803/8822 Chip Disable.

  LCD_ChkBusy();
  LCD_DATA = 0xff;
  LCD_CS1 =0;   // RA8803/8822 Chip Enable.
  LCD_WR = 1;   // WR = 1;
  LCD_RS = 0;   // RS = 0;
  LCD_RD = 0;   // RD = 0;
  delay(100);
  REG_Read = LCD_DATA;
  LCD_RD = 1;   // RD = 1;
  LCD_RS = 1;   // RS = 1;
  LCD_CS1 =1;   // RA8803/8822 Chip Disable.
  return(REG_Read);
}

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT8U READ_REG;

  // RA8803/8822 Reset
  LCD_READY = 0xff;   // LCD_RS/WR/RD/CS1/CS2 normal - sleep high.
  LCD_CS1 = 0;        // RA8803/8822 Chip Enable.
  delay(1000);
  LCD_RST = 0;        // LCD RESET-pin active low.
  delay(11000);       // Reset 250ms
  LCD_RST = 1;        // LCD RESET-pin Normal high.
  delay(1000);

  // LCD Control Register(WLCR)
  LCD_CmdWrite(0x00,0xC1); // 3.bit:0-graphic,1-character; 2.bit display 0-off, 1-on, 1.bit 0-no blinking
  // Misc.Register(MIR)
  LCD_CmdWrite(0x01,0xf1); // 1,0bits: 4.Mhz clock
  // Advance Power Setup Register(APSR)
  LCD_CmdWrite(0x02,0x10); // 3.bit 0-ROM font disable
  // Advance Display Setup Register(ADSR)
  LCD_CmdWrite(0x03,0x80); // 2,1,0.bit scrolling disable
  // Cursor Control Register(CCR)
  LCD_CmdWrite(0x10,0x69); // 3.bit-cursor auto increase: 1-enable, 0-disable
  // Distance of Word or Lines Register(DWLR)
  LCD_CmdWrite(0x11,0x22);
  // Memory Access Mode Register(AWRR)
  LCD_CmdWrite(0x12,0x91); // 7.bit-cursor autoshifting: 0-vertical,1-horizontal, 6,5,4.bits 1 page mode
  // Active Window Right Register(AWRR)
  LCD_CmdWrite(0x20,0x1D); // x = 240 bits
  // Active Window Bottom Register(AWBR)
  LCD_CmdWrite(0x30,0x9F); // y = 160 bits
  // Active Window Left Register(AWLR)
  LCD_CmdWrite(0x40,0x00);
  // Active Window Top Register(AWTR)
  LCD_CmdWrite(0x50,0x00);
  // Display Window Right Register(DWRR)
  LCD_CmdWrite(0x21,0x1D); // x = 240bits
  // Display Window Bottom Register(DWBR)
  LCD_CmdWrite(0x31,0x9F); // y - 160bits
  // Display Window Left Register(DWLR)
  LCD_CmdWrite(0x41,0x00);
  // Display Window Top Register(DWTR)
  LCD_CmdWrite(0x51,0x00);
  // Cursor Position X Register(CPXR)
  LCD_CmdWrite(0x60,0x00);
  // Begin Segment Position Register(BGSG)
  LCD_CmdWrite(0x61,0x00);
  // Cursor Position Y Register(CPYR)
  LCD_CmdWrite(0x70,0x00);
  // Shift action range, Begin Common Register(BGCM)
  LCD_CmdWrite(0x71,0x00);
  // Shift action range, End Common Register(EDCM)
  LCD_CmdWrite(0x72,0xEF);
  // Blink Time Register(BTR)
  LCD_CmdWrite(0x80,0x33);
  // Frame Rate Polarity Change at Common_FA Register(FDCA)
  LCD_CmdWrite(0x81,0x00);
  // Frame Rate Polarity Change at Common_FB Register(FDCB)
  LCD_CmdWrite(0x91,0x00);
  // Shift Clock Control Register(SCCR)
  LCD_CmdWrite(0x90,0x0C); // SCCR = (SCLK x DW)/(Seg x Com x FRM)
  // Interrupt Setup & Status Register(FRCB)
  LCD_CmdWrite(0xA0,0x11);
  // Key Scan Control Register(KSCR)
  LCD_CmdWrite(0xA1,0x00);
  // Key Scan Data Register(KSDR)
  LCD_CmdWrite(0xA2,0x00);
  // Key Scan Data Expand Register(KSER)
  LCD_CmdWrite(0xA3,0x00);
  // Interrupt Column Setup Register(INTX)
  LCD_CmdWrite(0xB0,0x27);
  // Interrupt Row Setup Register(INTY)
  LCD_CmdWrite(0xB1,0xEF);
  // Touch Panel Control Register(TPCR)
  LCD_CmdWrite(0xC0,0xD0);
  // ADC Status Register(ADCS)
  LCD_CmdWrite(0xC1,0x0A);
  // Touch Panel Segment High Byte Data Register(TPXR)
  LCD_CmdWrite(0xC8,0x80);
  // Touch Panel Common High Byte Data Register(TPYR)
  LCD_CmdWrite(0xC9,0x80);
  // Touch Panel Segment/Common Low Byte Data Register(TPZR)
  LCD_CmdWrite(0xCA,0x00);
  // LCD Contrast Control Register (LCCR)
  LCD_CmdWrite(0xD0,0x0C);
  // Pattern Data Register(PNTR)
  LCD_CmdWrite(0xE0,0x00);
  // Font Control Register(FCR)
  LCD_CmdWrite(0xF0,0xA0);
  // Font Size Control Register
  LCD_CmdWrite(0xF1,0x0F);

  // Clear display
  LCD_CmdWrite(0xE0,0x00);
  READ_REG = LCD_CmdRead(0xF0);
  READ_REG &= 0xF7;
  READ_REG |= 0x08;
  LCD_CmdWrite(0xF0,READ_REG);
  delay(1000);

  // Display on
  LCD_CmdWrite(0x00,0xC5);
  // Set cursor at 0,0 position
  LCD_CmdWrite(0x60,0x00); // Active Window Top Register(AWTR)
  LCD_CmdWrite(0x70,0x00); // Active Window Top Register(AWTR)
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT8U PageNo;
  GuiConst_INT8U ColNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
      // Something to redraw on this page
    {
      // Select page
      LCD_CmdWrite(0x70,PageNo);
      // Select column
      ColNo = GuiLib_DisplayRepaint[PageNo].ByteBegin;
      LCD_CmdWrite(0x60,ColNo);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        LCD_DataWrite(GuiLib_DisplayBuf[PageNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}
#endif // LCD_CONTROLLER_TYPE_RA8822

#ifdef LCD_CONTROLLER_TYPE_S1D13505

// ============================================================================
//
// S1D13505 DISPLAY CONTROLLER
//
// This driver uses one S1D13505 display controller.
// LCD display memory mapped interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 800x600 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB or Palette
//   Color depth: Up to 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

#pragma memory = dataseg(DISPLAYREG)
unsigned short DisplayReg[23];
#pragma memory = default

#pragma memory = dataseg(DISPLAYDATA)
GuiConst_INT16U DisplayData[sizeof(GuiLib_DisplayBuf)/2];
GuiConst_INT16U DisplayDataB[sizeof(GuiLib_DisplayBuf)/2];
#pragma memory = default

void StartDMA(void * SourceAdress,
              void * DestinationAdress,
              unsigned short Cnt,
              GuiConst_INT8S SrcUp_Dwn,
              GuiConst_INT8S DestUp_Dwn)
{
  // DMA controller settings
  if (SrcUp_Dwn > 0)
    DTCR0A = 0x56;   // Word(40)+Increment Source Adress(10)+24Bit adress(6)
  else if (SrcUp_Dwn == 0)
    DTCR0A = 0x46;   // Word(40)+Increment Source Adress(10)+24Bit adress(6)
  else if (SrcUp_Dwn < 0)
    DTCR0A = 0x76;   // Word(40)+Increment Source Adress(10)+24Bit adress(6)
  if (DestUp_Dwn > 0)
    DTCR0B = 0x10;   // Word(40)+Increment Source Adress(10)+24Bit adress(6)
  else if (DestUp_Dwn == 0)
    DTCR0B = 0x00;   // Word(40)+Increment Source Adress(10)+24Bit adress(6)
  else if (DestUp_Dwn < 0)
    DTCR0B = 0x30;   // Word(40)+Increment Source Adress(10)+24Bit adress(6)

  // MAR0AR = 0 // Source
  MAR0AE = (((long)SourceAdress)>>16) & 0xFF;
  MAR0AH = (((long)SourceAdress)>>8) & 0xFF;
  MAR0AL = ((long)SourceAdress) & 0xFF;
  // MAR0BR = 0 // Destination
  MAR0BE = (((long)DestinationAdress)>>16) & 0xFF;
  MAR0BH = (((long)DestinationAdress)>>8) & 0xFF;
  MAR0BL = ((long)DestinationAdress) & 0xFF;

  ETCR0AH = (Cnt>>8)  & 0xFF; //Number of bytes to transfer
  ETCR0AL = Cnt & 0xFF;

  while (DTCR0B>0x80);
  DTCR0B  |= 0x80;
  while (DTCR0A>0x80);
  DTCR0A  |= 0x80;
  while (DTCR0A>0x80);
}

void GuiDisplay_Init(void)
{
  GuiConst_INT16U i,j;

  // LCD Controller Settings
  // 16 bit register writing - two controller registers set for each command
  DisplayReg[13] = 0x0000; // Enable controller bus / Power save status
  DisplayReg[1] = 0x0025;  // Panel type / MOD
  DisplayReg[2] = 0x0A27;  // Horizontal non display time / Horizontal pixels
  DisplayReg[3] = 0x0100;  // Pulse width of newline / Delay from horizontal non display time to newline
  DisplayReg[4] = 0x00EF;  // Vertical lines
  DisplayReg[5] = 0x0006;  // Delay from vertical non display time to newframe / Vertical non display period
  DisplayReg[6] = 0x0900;  // Enable/disable CRT/LCD, Color depth / Pulse width of newframe
  DisplayReg[7] = 0x03FF;  // Screen 1 number of lines
  DisplayReg[8] = 0x0000;  // Screen 1 start address
  DisplayReg[9] = 0x0000;  // Screen 2 start address / Screen 1 start address
  DisplayReg[10] = 0x0000; // Screen 2 start address
  DisplayReg[11] = 0x0050; // Memory address offset
  DisplayReg[12] = 0x0200; // Clock configuration / Pixel panning
  DisplayReg[17] = 0x2058; // Performance enhancement
  DisplayReg[18] = 0x0000; // LUT - table register

  // Load palette data
  for (i = 0; i < GuiConst_PALETTE_SIZE; i++)
    for (j = 0; j < GuiConst_COLOR_BYTE_SIZE; j++)
      DisplayReg[19] = GuiStruct_Palette[i][j];
}

// Refreshes display buffer to display
void GuiDisplay_Refresh(void)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S LastByte;

  // Lock GUI resources
  GuiDisplay_Lock();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Calculate last byte of scan line
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;
      // Write display data
      StartDMA(
         GuiLib_DisplayBuf[Y][GuiLib_DisplayRepaint[Y].ByteBegin],
         DisplayData[Y][GuiLib_DisplayRepaint[Y].ByteBegin],
         GuiConst_COLOR_BYTE_SIZE *
         (LastByte - GuiLib_DisplayRepaint[Y].ByteBegin + 1),1,1);
      DACR = 0xBF;

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock();
}

#endif // LCD_CONTROLLER_TYPE_S1D13505

#ifdef LCD_CONTROLLER_TYPE_S1D13506

// ============================================================================
//
// S1D13506 DISPLAY CONTROLLER
//
// This driver uses one S1D13506 display controller.
// LCD display in memory mapped interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 800x600 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: Up to 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================
// Hardwired setting
// MD11 MD3 MD2 MD1
// 0    0   1   1    Generic bus mode
// MD4 = 1           little endian
// MD7   MD6
// 0     0           symmetrical 256K�16 DRAM
// 0     1           symmetrical 1M�16 DRAM
// 1     0           asymmetrical 256K�16 DRAM
// 1     1           asymmetrical 1M�16 DRAM

// REGISTER_OFFSET - starting address of the S1D13506 registers.
#define REGISTER_OFFSET ((GuiConst_INT8U *) 0x14000000)

// DISP_MEM_OFFSET - first address of the display buffer memory.
#define DISP_MEM_OFFSET ((GuiConst_INT16U *) 0x4000000)

void GuiDisplay_Init(void)
{
  GuiConst_INT32U i;
  GuiConst_INT8U * pRegs = REGISTER_OFFSET;
  GuiConst_INT16U * pMem  = DISP_MEM_OFFSET;

  // Enable access to the S1D13506 by setting bit 7 to 0.
  *(pRegs + 0x001) = 0x00;
  // Disable the display outputs
  *(pRegs + 0x1FC) = 0x00;

  // Set the  GPIOs to input
  *(pRegs + 0x004) = 0x00;
  *(pRegs + 0x008) = 0x00;

  // Program the Clock Source
  // Example for single input clock attached CLKI.
  // MCLK and LCD PCLK = CLKI/2 will be derived from CLKI.
  *(pRegs + 0x010) = 0x00;
  *(pRegs + 0x014) = 0x10;
  *(pRegs + 0x018) = 0x02;
  *(pRegs + 0x01C) = 0x02;

  // Setup CPU wait states. (MCLK is more than twice BusCLK)
  *(pRegs + 0x01E) = 0x01;

  // Configure the memory interface.
  // EDO, 2-CAS#
  *(pRegs + 0x020) = 0x01;
  // 32 ms (or less) refresh time
  *(pRegs + 0x021) = 0x06;
  // DRAM timing control
  *(pRegs + 0x02A) = 0x01;
  *(pRegs + 0x02B) = 0x01;

  // Set LCD panel type and panel timing registers
  // Panel type register
  *(pRegs + 0x030) = 0x25;
  *(pRegs + 0x031) = 0x00;
  // Horizontal Display Width Register
  *(pRegs + 0x032) = GuiConst_DISPLAY_WIDTH_HW / 8 - 1;
  // Horizontal Non-Display Period Register
  *(pRegs + 0x034) = 0x1F;
  // TFT FPLINE Start Position Register
  *(pRegs + 0x035) = 0x01;
  // TFT FPLINE Pulse Width Register
  *(pRegs + 0x036) = 0x0B;
  // Vertical Display Height Register 0
  *(pRegs + 0x038) = (GuiConst_INT8U)GuiConst_BYTE_LINES;
  // Vertical Display Height Register 1
  *(pRegs + 0x039) = (GuiConst_INT8U)(GuiConst_BYTE_LINES>>8);
  // Vertical Non-Display Period Register
  *(pRegs + 0x03A) = 0x2C;
  // TFT FPFRAME Start Position Register
  *(pRegs + 0x03B) = 0x0A;
  // TFT FPFRAME Pulse Width Register
  *(pRegs + 0x03C) = 0x01;

  // Set LCD display output format, memory start locations and FIFO values.
  // Display Mode Register, color depth 16bpp
  *(pRegs + 0x040) = 0x05;
  // Miscellaneous Register - Dual panel buffer disabled
  *(pRegs + 0x041) = 0x01;
  // Set the LCD start address
  *(pRegs + 0x042) = 0x00;
  *(pRegs + 0x043) = 0x00;
  *(pRegs + 0x044) = 0x00;
  // LCD memory address offset
  *(pRegs + 0x046) = (GuiConst_INT8U)GuiConst_DISPLAY_WIDTH_HW;
  *(pRegs + 0x047) = (GuiConst_INT8U)(GuiConst_DISPLAY_WIDTH_HW>>8);

  // Pixel Panning Register - No pixel pan
  *(pRegs + 0x048) = 0x00;
  // FIFO values sets automatically by hardware
  *(pRegs + 0x04A) = 0x00;
  *(pRegs + 0x04B) = 0x00;

  // Set CRT/TV timing control registers
  *(pRegs + 0x050) = 0x00;
  *(pRegs + 0x052) = 0x00;
  *(pRegs + 0x053) = 0x00;
  *(pRegs + 0x054) = 0x00;
  *(pRegs + 0x056) = 0x00;
  *(pRegs + 0x057) = 0x00;
  *(pRegs + 0x058) = 0x00;
  *(pRegs + 0x059) = 0x00;
  *(pRegs + 0x05A) = 0x00;
  *(pRegs + 0x05B) = 0x00;

  // Set CRT/TV output format, memory start locations and FIFO values.
  *(pRegs + 0x060) = 0x00;
  *(pRegs + 0x062) = 0x00;
  *(pRegs + 0x063) = 0x00;
  *(pRegs + 0x064) = 0x00;
  *(pRegs + 0x066) = 0x00;
  *(pRegs + 0x067) = 0x00;
  *(pRegs + 0x068) = 0x00;
  *(pRegs + 0x06A) = 0x00;
  *(pRegs + 0x06B) = 0x00;

  // Set CRT/TV Ink Layer/HW Cursor, Position, Color and FIFO registers,
  *(pRegs + 0x080) = 0x00;
  *(pRegs + 0x081) = 0x00;
  *(pRegs + 0x082) = 0x00;
  *(pRegs + 0x083) = 0x00;
  *(pRegs + 0x084) = 0x00;
  *(pRegs + 0x085) = 0x00;
  *(pRegs + 0x086) = 0x00;
  *(pRegs + 0x087) = 0x00;
  *(pRegs + 0x088) = 0x00;
  *(pRegs + 0x08A) = 0x00;
  *(pRegs + 0x08B) = 0x00;
  *(pRegs + 0x08C) = 0x00;
  *(pRegs + 0x08E) = 0x00;

  // Set the 2D acceleration (BitBLT) registers to a known state
  *(pRegs + 0x100) = 0x00;
  *(pRegs + 0x101) = 0x00;
  *(pRegs + 0x102) = 0x00;
  *(pRegs + 0x103) = 0x00;
  *(pRegs + 0x104) = 0x00;
  *(pRegs + 0x105) = 0x00;
  *(pRegs + 0x106) = 0x00;
  *(pRegs + 0x108) = 0x00;
  *(pRegs + 0x109) = 0x00;
  *(pRegs + 0x10A) = 0x00;
  *(pRegs + 0x10C) = 0x00;
  *(pRegs + 0x10D) = 0x00;
  *(pRegs + 0x110) = 0x00;
  *(pRegs + 0x111) = 0x00;
  *(pRegs + 0x112) = 0x00;
  *(pRegs + 0x113) = 0x00;
  *(pRegs + 0x114) = 0x00;
  *(pRegs + 0x115) = 0x00;
  *(pRegs + 0x116) = 0x00;
  *(pRegs + 0x118) = 0x00;
  *(pRegs + 0x119) = 0x00;

  // Turn off power save mode
  *(pRegs + 0x1F0) = 0;

  // Disable the watchdog timer
  *(pRegs + 0x1F4) = 0;

  // Enable the display
  *(pRegs + 0x1FC) = 0x01;

  // Clear display memory
  for (i = 0; i < GuiConst_DISPLAY_BYTES / 2; i++)
    *pMem++ = 0x0000;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT32U i;
  GuiConst_INT16S LineNo, LastByte, N;
  GuiConst_INT16U *pMem = DISP_MEM_OFFSET;

  GuiDisplay_Lock ();

  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++) // Loop through lines
  {
    if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
    // Something to redraw on this line
    {
      // Calculate address in frame buffer
      i = LineNo;
      i = i * GuiConst_DISPLAY_WIDTH + GuiLib_DisplayRepaint[LineNo].ByteBegin;

      // Calculate last byte
      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[LineNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[LineNo].ByteEnd;

      // Write display data
      for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin; N <= LastByte; N++)
        pMem[i++] = GuiLib_DisplayBuf.Words[LineNo][N];

      // Reset repaint parameters
      GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S1D13506

#ifdef LCD_CONTROLLER_TYPE_S1D13700

// ============================================================================
//
// S1D13700 DISPLAY CONTROLLER
//
// This driver uses one S1D13700 display controller.
// LCD display 8 bit parallel 8080 type interface direct or indirect mode.
// Modify driver accordingly for other interface types.
// Graphic modes up to 640x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// Uncomment it for direct interface mode, let it commented for indirect mode!
//#define DIRECT_MODE

// Common section
// LCD display constants
#define CHRWTH   8   // Character width
#define CHRHIG   8   // Character hight
#define WF    0x80   // Two frame AC drive

// Parameters for System Control Registers
// Parameter 1: Int.CGROM, 8 pix.char hight, Single panel drv, No scr. topline correction
#define PAR1  0x30
// Parameter 2: Horizontal char size, Two frame AC drive
#define FX    (CHRWTH-1)|WF
// Parameter 3: Vertical char size
#define FY    CHRHIG-1
// Parameter 4: Character bytes pr row
#define CR    (GuiConst_DISPLAY_WIDTH_HW/CHRWTH-1)*((CHRWTH+7)>>3)
// Parameter 5: Total character bytes pr row
#define TCR   0x2D // TCR calculation is rather complex, see display controller manual
// Parameter 6: Frame height (no of vertical pixels - 1)
#define LF    GuiConst_DISPLAY_HEIGHT_HW-1
// Parameter 7: Horizontal address range LSB
#define APL   GuiConst_DISPLAY_WIDTH_HW/CHRWTH
// Parameter 8: Horizontal address range MSB
#define APH   0x00
// Scroll - parameters
#define SAD1L   0x00  // Parameter  1: Screen block 1 address low byte
#define SAD1H   0x00  // Parameter  2: Screen block 1 address high byte
#define SL_1    LF    // Parameter  3: Number of lines pr. scrolling screen
#define SAD2L   0x80  // Parameter  4: Screen block 2 address low byte
#define SAD2H   0x25  // Parameter  5: Screen block 2 address high byte
#define SL_2    LF    // Parameter  6: Number of lines pr. scrolling screen
#define SAD3L   0x00  // Parameter  7: Screen block 3 address low byte
#define SAD3H   0x00  // Parameter  8: Screen block 3 address high byte
#define SAD4L   0x00  // Parameter  9: Screen block 4 address low byte
#define SAD4H   0x00  // Parameter 10: Screen block 4 address high byte
#define POWSAVE 0x00  // No power save
// General constants/macros
#define ALLHIGH 0xFF  // Bit 0-7 = '1'
#define TAH8    0     // Address hold time
#define TDS8    0     // Data setup time
#define TCYC8   0     // System cycle time
#define TCC     0     // Strobe pulse width
#define TRES    500   // 2000 ~ Reset pulse 1,3 ms >= 1 ms :  20(33n+68)

#ifdef DIRECT_MODE
  // DIRECT MODE
  // LCD-Memory start address
  #define LCD_MEMORY_START_ADDR         (GuiConst_INT8U*)0x100000
  #define LCD_MEMORY_2_START_ADDR       (GuiConst_INT8U*)0x102580
  // SYSTEM CONTROL REGISTERS
  #define LCD_MEMORY_CNF_REG            *((volatile GuiConst_INT8U*)0x108000) // Memory Configuration Register
  #define LCD_HORIZONTAL_CHAR_SIZE_REG  *((volatile GuiConst_INT8U*)0x108001) // Horizontal Character Size Register
  #define LCD_VERTICAL_CHAR_SIZE_REG    *((volatile GuiConst_INT8U*)0x108002) // Vertical Character Size Register
  #define LCD_CHAR_BYTES_PER_ROW_REG    *((volatile GuiConst_INT8U*)0x108003) // Character Bytes Per Row Register
  #define LCD_TOT_CH_BYTES_PER_ROW_REG  *((volatile GuiConst_INT8U*)0x108004) // Total Character Bytes Per Row Register
  #define LCD_FRAME_HEIGHT_REG          *((volatile GuiConst_INT8U*)0x108005) // Frame Height Register
  #define LCD_HORIZON_ADDR_RANGE_REG_0  *((volatile GuiConst_INT8U*)0x108006) // Horizontal Address Range Register 0
  #define LCD_HORIZON_ADDR_RANGE_REG_1  *((volatile GuiConst_INT8U*)0x108007) // Horizontal Address Range Register 1
  #define LCD_POWER_SAVE_MODE_REG       *((volatile GuiConst_INT8U*)0x108008) // Power Save Mode Register
  // DISPLAY CONTROL REGISTERS
  #define LCD_DISPLAY_ENABLE_REG        *((volatile GuiConst_INT8U*)0x108009) // Display Enable Register
  #define LCD_DISPLAY_ATTRIBUTE_REG     *((volatile GuiConst_INT8U*)0x10800a) // Display Attribute Register
  #define LCD_SCREEN_B1_START_ADR_REG_0 *((volatile GuiConst_INT8U*)0x10800b) // Screen Block 1 Start Address Register 0
  #define LCD_SCREEN_B1_START_ADR_REG_1 *((volatile GuiConst_INT8U*)0x10800c) // Screen Block 1 Start Address Register 1
  #define LCD_SCREEN_B1_SIZE_REG        *((volatile GuiConst_INT8U*)0x10800d) // Screen Block 1 Size Register
  #define LCD_SCREEN_B2_START_ADR_REG_0 *((volatile GuiConst_INT8U*)0x10800e) // Screen Block 2 Start Address Register 0
  #define LCD_SCREEN_B2_START_ADR_REG_1 *((volatile GuiConst_INT8U*)0x10800f) // Screen Block 2 Start Address Register 1
  #define LCD_SCREEN_B2_SIZE_REG        *((volatile GuiConst_INT8U*)0x108010) // Screen Block 2 Size Register
  #define LCD_SCREEN_B3_START_ADR_REG_0 *((volatile GuiConst_INT8U*)0x108011) // Screen Block 3 Start Address Register 0
  #define LCD_SCREEN_B3_START_ADR_REG_1 *((volatile GuiConst_INT8U*)0x108012) // Screen Block 3 Start Address Register 1
  #define LCD_SCREEN_B4_START_ADR_REG_0 *((volatile GuiConst_INT8U*)0x108013) // Screen Block 4 Start Address Register 0
  #define LCD_SCREEN_B4_START_ADR_REG_1 *((volatile GuiConst_INT8U*)0x108014) // Screen Block 4 Start Address Register 1
  #define LCD_CURSOR_WIDTH_REG          *((volatile GuiConst_INT8U*)0x108015) // Cursor Width Register
  #define LCD_CURSOR_HEIGHT_REG         *((volatile GuiConst_INT8U*)0x108016) // Cursor Height Register
  #define LCD_CURSOR_SHIFT_DIR_REG      *((volatile GuiConst_INT8U*)0x108017) // Cursor Shift Direction Register
  #define LCD_OVERLAY_REG               *((volatile GuiConst_INT8U*)0x108018) // Overlay Register
  #define LCD_CHAR_GEN_RAM_ST_ADR_REG_0 *((volatile GuiConst_INT8U*)0x108019) // Character Generator RAM Start Address Register 0
  #define LCD_CHAR_GEN_RAM_ST_ADR_REG_1 *((volatile GuiConst_INT8U*)0x10801a) // Character Generator RAM Start Address Register 1
  #define LCD_HORIZON_PIXEL_SCROLL_REG  *((volatile GuiConst_INT8U*)0x10801b) // Horizontal Pixel Scroll Register
  // DRAWING CONTROL REGISTERS
  #define LCD_CURSOR_WRITE_REG_0        *((volatile GuiConst_INT8U*)0x10801c) // Cursor Write Register 0
  #define LCD_CURSOR_WRITE_REG_1        *((volatile GuiConst_INT8U*)0x10801d) // Cursor Write Register 1
  #define LCD_CURSOR_READ_REG_0         *((volatile GuiConst_INT8U*)0x10801e) // Cursor Read Register 0
  #define LCD_CURSOR_READ_REG_1         *((volatile GuiConst_INT8U*)0x10801f) // Cursor Read Register 1
  // GRAYSCALE REGISTER
  #define LCD_BIT_PER_PIXEL_SELECT_REG  *((volatile GuiConst_INT8U*)0x108020) // Bit-Per-Pixel Select Register
#else
  // INDIRECT MODE
  // pins definition
  #define DISP_DATA                 Pxx           // DB7-DB0 pin
  #define RESET_ON                  Pxx.x = 0     // RESET pin
  #define RESET_OFF                 Pxx.x = 1     // RESET pin
  #define CHIP_ENABLED              Pxx.x = 0     // CS pin
  #define CHIP_DISABLED             Pxx.x = 1     // CS pin
  #define MODE_COMMAND              Pxx.x = 1     // A0 pin
  #define MODE_DATA                 Pxx.x = 0     // A0 pin
  #define WR_LOW                    Pxx.x = 0     // WR pin
  #define WR_HIGH                   Pxx.x = 1     // WR pin
  #define RD_LOW                    Pxx.x = 0     // RD pin
  #define RD_HIGH                   Pxx.x = 1     // RD pin
  #define NOP                       asm ("nop")   // nop code
  // commands
  #define SYSTEM_SET                0x40
  #define SLEEP_IN                  0x53
  #define DISPLAY_ON                0x59
  #define DISPLAY_OFF               0x58
  #define SCROLL                    0x44
  #define CSRFORM                   0x5D
  #define CSRDIR_RIGHT              0x4C
  #define CSRDIR_LEFT               0x4D
  #define CSRDIR_UP                 0x4E
  #define CSRDIR_DOWN               0x4F
  #define OVLAY                     0x5B
  #define CGRAM_ADR                 0x5C
  #define HDOT_SCR                  0x5A
  #define GRAYSCALE_MODE            0x60
  #define CSRW                      0x46
  #define WRITE_DATA                0x42

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

  // 8 bit parallel 8080 type interface
  void LCD_Cmd(GuiConst_INT8U v)
  {
    MODE_COMMAND;
    RD_HIGH;
    WR_LOW;
    CHIP_ENABLED;
    DISP_DATA = v;
    //NOP;             // May be required for some microcontrollers
    CHIP_DISABLED;
    WR_HIGH;
  }

  void LCD_Data(GuiConst_INT8U v)
  {
    MODE_DATA;
    RD_HIGH;
    WR_LOW;
    CHIP_ENABLED;
    DISP_DATA = v;
    //NOP;             // May be required for some microcontrollers
    CHIP_DISABLED;
    WR_HIGH;
  }
#endif

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16U i;

#ifdef DIRECT_MODE
  GuiConst_INT8U* lcd_memory_ptr;

  // POWER SAVE
  LCD_POWER_SAVE_MODE_REG       = POWSAVE; // No power save

  // Setup display (SYSTEM SET)
  LCD_MEMORY_CNF_REG            = PAR1;
  LCD_HORIZONTAL_CHAR_SIZE_REG  = FX;
  LCD_VERTICAL_CHAR_SIZE_REG    = FY;
  LCD_CHAR_BYTES_PER_ROW_REG    = CR;
  LCD_TOT_CH_BYTES_PER_ROW_REG  = TCR;
  LCD_FRAME_HEIGHT_REG          = LF;
  LCD_HORIZON_ADDR_RANGE_REG_0  = APL;
  LCD_HORIZON_ADDR_RANGE_REG_1  = APH;

  // Scroll setup (SCROLL)
  LCD_SCREEN_B1_START_ADR_REG_0 = SAD1L;
  LCD_SCREEN_B1_START_ADR_REG_1 = SAD1H;
  LCD_SCREEN_B1_SIZE_REG        = SL_1;
  LCD_SCREEN_B2_START_ADR_REG_0 = SAD2L;
  LCD_SCREEN_B2_START_ADR_REG_1 = SAD2H;
  LCD_SCREEN_B2_SIZE_REG        = SL_2;
  LCD_SCREEN_B3_START_ADR_REG_0 = SAD3L;
  LCD_SCREEN_B3_START_ADR_REG_1 = SAD3H;
  LCD_SCREEN_B4_START_ADR_REG_0 = SAD4L;
  LCD_SCREEN_B4_START_ADR_REG_1 = SAD4H;

  // Horizontal Pixel Scroll setup (HDOT SCR)
  LCD_HORIZON_PIXEL_SCROLL_REG  = 0x00;

  // Overlay setup (OVLAY)
  LCD_OVERLAY_REG               = 0x0d; // Exclusive OR between layer 1 og 2

  // Cursor format (CSRFORM)
  LCD_CURSOR_WIDTH_REG          = 0x07; // 8 pixels wide
  LCD_CURSOR_HEIGHT_REG         = 0x87; // Block cursor, 8 pixels high

  // Cursor direction setup (CSRDIR)
  LCD_CURSOR_SHIFT_DIR_REG      = 0x00; // Shift direction = right

  // Enable display (DISP ON/OFF)
  LCD_DISPLAY_ATTRIBUTE_REG     = 0x14; // Block 1 and 2 = ON, No cursor

  // Clear display RAM
  // Address area 0 - 9599 in display controller RAM is used for pixel polarity
  // determination, while the following 9600 bytes are used as display buffer
  // RAM.
  // By writing 0xFF in the first address area the polarity will be "1" in
  // display RAM for black pixels on the display.
  // The reason for not using the lower RAM area as display RAM is that it
  // enables the cursor, which we don't want.
  lcd_memory_ptr = LCD_MEMORY_START_ADDR;
  for (i=0; i<9600; i++)
    // Fill lower "polarity bit" RAM area
    *(lcd_memory_ptr + i) = 0xff;

  lcd_memory_ptr = LCD_MEMORY_2_START_ADDR;
  for (i=9600; i<19200; i++)
    // Fill upper "display bit" RAM area with white pixels
    *(lcd_memory_ptr + i) = 0x00;

  // Turn display on
  LCD_DISPLAY_ENABLE_REG = 0x01; // Display ON
#else
  // Setup display (SYSTEM SET)
  LCD_Cmd(SYSTEM_SET);
  LCD_Data(PAR1);
  LCD_Data(FX);
  LCD_Data(FY);
  LCD_Data(CR);
  LCD_Data(TCR);
  LCD_Data(LF);
  LCD_Data(APL);
  LCD_Data(APH);

  // Scroll setup (SCROLL)
  LCD_Cmd(SCROLL);
  LCD_Data(SAD1L);
  LCD_Data(SAD1H);
  LCD_Data(SL_1);
  LCD_Data(SAD2L);
  LCD_Data(SAD2H);
  LCD_Data(SL_2);
  LCD_Data(SAD3L);
  LCD_Data(SAD3H);
  LCD_Data(SAD4L);
  LCD_Data(SAD4H);

  // Horizontal Pixel Scroll setup (HDOT SCR)
  LCD_Cmd(HDOT_SCR);
  LCD_Data(0x00);

  // Overlay setup (OVLAY)
  LCD_Cmd(OVLAY);
  LCD_Data(0x0D); // exlusive OR between layer 1 and 2
  //LCD_Data(0x0C); // simple OR between layer 1 and 2

  // Cursor format (CSRFORM)
  LCD_Cmd(CSRFORM);
  LCD_Data(0x07); // 8 pixels wide
  LCD_Data(0x87); // Block cursor, 8 pixels high

  // Cursor direction setup (CSRDIR)
  LCD_Cmd(CSRDIR_RIGHT);   // Shift direction = right

  // Enable display (DISP ON/OFF)
  LCD_Cmd(DISPLAY_ON);
  LCD_Data(0x14); // Block 1 and 2 = ON, No cursor

  // Clear display RAM
  // Address area 0 - 9599 in display controller RAM is used for pixel polarity
  // determination, while the following 9600 bytes are used as display buffer
  // RAM.
  // By writing 0xFF in the first address area the polarity will be "1" in
  // display RAM for black pixels on the display.
  // The reason for not using the lower RAM area as display RAM is that it
  // enables the cursor, which we don't want.
  LCD_Cmd(CSRW);
  LCD_Data(0x00);
  LCD_Data(0x00);
  LCD_Cmd(WRITE_DATA);
  for (i=0; i<9600; i++)
    // Fill lower "polarity bit" RAM area
    LCD_Data(0xff);

  LCD_Cmd(CSRW);
  LCD_Data(SAD2L);
  LCD_Data(SAD2H);
  LCD_Cmd(WRITE_DATA);
  for (i=9600; i<19200; i++)
    // Fill upper "display bit" RAM area with white pixels
    LCD_Data(0x00);
#endif
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X;
  GuiConst_INT16S Y;
  GuiConst_INT16S LastByte;
  GuiConst_INT16U Address;
#ifdef DIRECT_MODE
  GuiConst_INT8U* lcd_memory_ptr;
  GuiConst_INT16U i;
#endif

  GuiDisplay_Lock ();

  // Pointer to start of 32K LCD Memory
#ifdef DIRECT_MODE
  lcd_memory_ptr = LCD_MEMORY_START_ADDR;
#endif

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Address Pointer Set
      Address = Y * GuiConst_BYTES_PR_LINE +
         GuiLib_DisplayRepaint[Y].ByteBegin + SAD2H * 256 + SAD2L;

      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

#ifdef DIRECT_MODE
      i = 0;
#else
      LCD_Cmd(CSRW);
      LCD_Data((GuiConst_INT8U)Address);
      LCD_Data((GuiConst_INT8U)(Address>>8));
      LCD_Cmd(WRITE_DATA);
#endif
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
#ifdef DIRECT_MODE
        *(lcd_memory_ptr + Address+i) = GuiLib_DisplayBuf[Y][X];
        i++;
#else
        LCD_Data(GuiLib_DisplayBuf[Y][X]);
#endif
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S1D13700

#ifdef LCD_CONTROLLER_TYPE_S1D13705

// ============================================================================
//
// S1D13705 DISPLAY CONTROLLER
//
// This driver uses one S1D13705 display controller.
// LCD display memory mapped interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 800x600 pixels.
//
// Compatible display controllers:
//   S1D13704, S1D13706, S1D13A04, SED1374, SED1375, SSD1905
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Palette
//   Color depth: 8 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================
#if 0
extern GuiConst_INT8U DisplayReg[32];
GuiConst_INT8U RevisionReg;

 GuiConst_INT8U
  xhuge DisplayData[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];

void GuiDisplay_Init(void)
{
  GuiConst_INT16U i,j;

  // LCD Controller Settings
  RevisionReg = DisplayReg[0];  // Revision Code Register
  DisplayReg[0x01] = 0x23;      // Mode Register 0 Register
  DisplayReg[0x02] = 0xC0;      // Mode Register 1 Register
  DisplayReg[0x03] = 0x03;      // Mode Register 2 Register
  DisplayReg[0x04] = 0x27;      // Horizontal Panel Size Register
  DisplayReg[0x05] = 0xEF;      // Vertical Panel Size Register (LSB)
  DisplayReg[0x06] = 0x00;      // Vertical Panel Size Register (MSB)
  DisplayReg[0x07] = 0x00;      // FPLINE Start Position Register
  DisplayReg[0x08] = 0x00;      // Horizontal Non-Display Period Register
  DisplayReg[0x09] = 0x04;      // FPFRAME Start Position Register
  DisplayReg[0x0A] = 0x03;      // Vertical Non-Display Period Register
  DisplayReg[0x0B] = 0x00;      // MOD Rate Register
  DisplayReg[0x0C] = 0x00;      // Screen 1 Start Address Register (LSB)
  DisplayReg[0x0D] = 0x00;      // Screen 1 Start Address Register (MSB)
  DisplayReg[0x0E] = 0x00;      // Screen 2 Start Address Register (LSB)
  DisplayReg[0x0F] = 0x00;      // Screen 2 Start Address Register (MSB)
  DisplayReg[0x10] = 0x00;      // Screen Start Address Overflow Register
  DisplayReg[0x11] = 0x00;      // Memory Address Offset Register
  DisplayReg[0x12] = 0xFF;      // Screen 1 Vertical Size Register (LSB)
  DisplayReg[0x13] = 0x03;      // Screen 1 Vertical Size Register (MSB)
  DisplayReg[0x14] = 0x00;      // Not Used
  DisplayReg[0x16] = 0x00;      // Not Used
//  DisplayReg[0x17] = 0x00;      // Look-Up Table Data Register
  DisplayReg[0x18] = 0x00;      // GPIO Configuration Control Register
  DisplayReg[0x19] = 0x00;      // GPIO Status/Control Register
  DisplayReg[0x1A] = 0x00;      // Scratch Pad Register
  DisplayReg[0x1B] = 0x00;      // SwivelView Mode Register
  DisplayReg[0x1C] = 0x00;      // Line Byte Count Register
  DisplayReg[0x1D] = 0x00;      // Not Used
  DisplayReg[0x1E] = 0x00;      // Not Used
  DisplayReg[0x1F] = 0x00;      // Not Used
  DisplayReg[0x15] = 0x00;      // Look-Up Table Address Register
  // Load palette data
   for (i = 0; i < GuiConst_PALETTE_SIZE; i++)
     for (j = 0; j < GuiConst_COLOR_BYTE_SIZE; j++)
       DisplayReg[0x17] = GuiStruct_Palette[i][j];
}
#endif

#ifdef	GUILIB_USE_DOUBLE_BUFFERING

#include  "lcd_init.h"

// Refreshes display buffer to display
void GuiDisplay_Refresh(void)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S X;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
  			#ifdef	GUILIB_USE_DOUBLE_BUFFERING
  			  primary_display_buf[Y][X] = gui_lib_display_buf[Y][X];
  			#else
  			  DisplayData[Y][X] = GuiLib_DisplayBuf[Y][X];
  			#endif
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock ();
}

#endif	// GUILIB_USE_DOUBLE_BUFFERING

#endif // LCD_CONTROLLER_TYPE_S1D13705

#ifdef LCD_CONTROLLER_TYPE_S1D13706

// ============================================================================
//
// S1D13706 DISPLAY CONTROLLER
//
// This driver uses one S1D13706 display controller.
// LCD display in memory mapped or generic 2 type interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   S1D13704, S1D13705, S1D13A04, SED1374, SED1375, SSD1905, SSD1906
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Palette or RGB
//   Color depth: 2, 8 or 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Make sure 2ms elapses from system power on, before init routine is executed.
//
// ============================================================================

// Uncomment for memory mapped interface
//#define MEM_MAPPED

#ifdef MEM_MAPPED

// Set according to hardware connection
// Toshiba TMPR3905/12
#define REGISTER_ADDRESS    0x00000
#define DISPLAY_ADDRESS     0x20000
// Nec VR4102/vr4111
// #define REGISTER_ADDRESS  0x0A000000
// #define DISPLAY_ADDRESS   0x0A200000
// Nec VR4181
// #define REGISTER_ADDRESS  0x133C0000
// #define DISPLAY_ADDRESS   0x133E0000
// Motorola MC68030
// #define REGISTER_ADDRESS  0x10A00000
// #define DISPLAY_ADDRESS   0x10E00000
// Motorola REDCAP2
// #define REGISTER_ADDRESS  0x41000000
// #define DISPLAY_ADDRESS   0x41020000
// SA-1110
// #define REGISTER_ADDRESS  0x40000000
// #define DISPLAY_ADDRESS   0x40020000

// REGISTER_OFFSET points to the starting address of the S1D13706 registers
#define REGISTER_OFFSET  ((GuiConst_INT8U *) REGISTER_ADDRESS)
// DISP_MEM_OFFSET points to the starting address of the display buffer memory
#define DISP_MEM_OFFSET  ((GuiConst_INT8U *) DISPLAY_ADDRESS)

#else
// Generic type 2 interface, little endian type

// Hardwired setting of S1D13706 display controller
// CNF4 = 0; CNF2 = 1;  CNF1 = 0;  CNF0 = 0;
// BS# = 1;
// RD/WR# = 1;
// A16 = 0;

// Example for Microchip dsPI33 - hardwired connection
// dsPIC3            S1D13706
// PORTA[7-0]        DB[7-0], DB[15-8] parallel to DB[7-0]
// PORTD[13-0]       A[13-0]
// PORTE[0]          A[14]
// PORTE[1]          A[15]
// PORTE[2]          M/R#
// PORTE[3]          RD#
// PORTE[4]          WE#
// PORTE[5]          CS#
// inverted PORTD[0] BHE#
// RESET             RESET#
// OSC2 or CLKO      BUSCLK

// Example for Microchip dsPI33
// Initializes PORTS
void LCD_Init(void)
{
  // Set PORTD[13-0] as output
  TRISD &= 0xC000;
  // Set PORTE[5-0] as output
  TRISE &= 0xFFC0;
  // Set high control signals
  PORTE |= 0x0038;
}

// Example for Microchip dsPI33 - adjust according your microcontroller
void LCD_Cmd(GuiConst_INT16U Address, GuiConst_INT8U Data)
{
  // Set PORTA[7-0] as output
  TRISA &= 0xFF00;
  // Set data
  PORTA &= 0xFF00;
  PORTA |= Data;
  // Set address A13-A0
  PORTD &= 0xC000;
  PORTD |= Address;
  // Select register M/R#, A15, A14 low
  PORTE &= 0xFFF8;
  // Select register RD# high
  PORTE |= 0x0008;
  // Set A15, A14
  PORTE |= Address>>14;
  // Set CS#, WE# low
  PORTE &= 0xFFCF;
  PORTE &= 0xFFCF; // just delay
  // Set CS#, WE# high
  PORTE |= 0x0030;
}

// Example for Microchip dsPI33 - adjust according your microcontroller
void LCD_Data(GuiConst_INT16U Address, GuiConst_INT8U Data)
{
  // Set PORTA[7-0] as output
  TRISA &= 0xFF00;
  // Set data
  PORTA &= 0xFF00;
  PORTA |= Data;
  // Set address A13-A0
  PORTD &= 0xC000;
  PORTD |= Address;
  // Select register M/R# and RD# high
  PORTE |= 0x000C;
  // Set A15, A14
  PORTE &= 0xFFFC;
  PORTE |= Address>>14;
  // Set CS#, WE#  low
  PORTE &= 0xFFCF;
  PORTE &= 0xFFCF; // just delay
  // Set CS#, WE# high
  PORTE |= 0x0030;
}

// Example for Microchip dsPI33 - adjust according your microcontroller
GuiConst_INT8U LCD_Read(GuiConst_INT16U Address)
{
  // Set PORTA[7-0] as input
  TRISA |= 0x00FF;
  // Set address A13-A0
  PORTD &= 0xC000;
  PORTD |= Address;
  // Select register M/R#, A15, A14 low
  PORTE &= 0xFFF8;
  // Select  WE# high
  PORTE |= 0x0010;
  // Set A15, A14
  PORTE |= Address>>14;
  // Set CS#, RD#  low
  PORTE &= 0xFFD7;
  PORTE &= 0xFFD7;
  // Set CS#, RD#  high
  PORTE |= 0x0028;
  return ((GuiConst_INT8U)PORTA);
}

#endif

#define S1D_PHYSICAL_VMEM_ADDR      0x00000000L
#define S1D_PHYSICAL_VMEM_SIZE      0x14000L
#define S1D_PHYSICAL_REG_ADDR       0x00000000L
#define S1D_PHYSICAL_REG_SIZE       0x100
#define S1D_DISPLAY_PCLK            6250
#define S1D_FRAME_RATE              70
#define S1D_POWER_DELAY_ON          50
#define S1D_POWER_DELAY_OFF         1200
#define S1D_REGDELAYOFF             0xFFFE
#define S1D_REGDELAYON              0xFFFF

#ifdef GuiConst_COLOR_DEPTH_2
// This is default 2-bit color palette data for LUT tables
// Only first 4. entries of LUT table are used for colors
GuiConst_INT8U LUT2_Color[4*3] =
{
  //R     G     B
  0x00, 0x00, 0x00, // 1.color
  0xFF, 0x00, 0x00, // 2.color
  0x00, 0xFF, 0x00, // 3.color
  0x00, 0x00, 0xFF  // 4.color
};
#endif
#ifdef GuiConst_COLOR_DEPTH_8
// This is default 8-bit color palette data for LUT tables, which can be
// overridden with easygui palette data
GuiConst_INT8U LUT8_Color[256*3] =
{
// Primary and secondary colors
0x00, 0x00, 0x00,  0x00, 0x00, 0xAA,  0x00, 0xAA, 0x00,  0x00, 0xAA, 0xAA,
0xAA, 0x00, 0x00,  0xAA, 0x00, 0xAA,  0xAA, 0xAA, 0x00,  0xAA, 0xAA, 0xAA,
0x55, 0x55, 0x55,  0x00, 0x00, 0xFF,  0x00, 0xFF, 0x00,  0x00, 0xFF, 0xFF,
0xFF, 0x00, 0x00,  0xFF, 0x00, 0xFF,  0xFF, 0xFF, 0x00,  0xFF, 0xFF, 0xFF,

// Gray shades
0x00, 0x00, 0x00,  0x10, 0x10, 0x10,  0x20, 0x20, 0x20,  0x30, 0x30, 0x30,
0x40, 0x40, 0x40,  0x50, 0x50, 0x50,  0x60, 0x60, 0x60,  0x70, 0x70, 0x70,
0x80, 0x80, 0x80,  0x90, 0x90, 0x90,  0xA0, 0xA0, 0xA0,  0xB0, 0xB0, 0xB0,
0xC0, 0xC0, 0xC0,  0xD0, 0xD0, 0xD0,  0xE0, 0xE0, 0xE0,  0xFF, 0xFF, 0xFF,

// Black to red
0x00, 0x00, 0x00,  0x10, 0x00, 0x00,  0x20, 0x00, 0x00,  0x30, 0x00, 0x00,
0x40, 0x00, 0x00,  0x50, 0x00, 0x00,  0x60, 0x00, 0x00,  0x70, 0x00, 0x00,
0x80, 0x00, 0x00,  0x90, 0x00, 0x00,  0xA0, 0x00, 0x00,  0xB0, 0x00, 0x00,
0xC0, 0x00, 0x00,  0xD0, 0x00, 0x00,  0xE0, 0x00, 0x00,  0xFF, 0x00, 0x00,

// Black to green
0x00, 0x00, 0x00,  0x00, 0x10, 0x00,  0x00, 0x20, 0x00,  0x00, 0x30, 0x00,
0x00, 0x40, 0x00,  0x00, 0x50, 0x00,  0x00, 0x60, 0x00,  0x00, 0x70, 0x00,
0x00, 0x80, 0x00,  0x00, 0x90, 0x00,  0x00, 0xA0, 0x00,  0x00, 0xB0, 0x00,
0x00, 0xC0, 0x00,  0x00, 0xD0, 0x00,  0x00, 0xE0, 0x00,  0x00, 0xFF, 0x00,

// Black to blue
0x00, 0x00, 0x00,  0x00, 0x00, 0x10,  0x00, 0x00, 0x20,  0x00, 0x00, 0x30,
0x00, 0x00, 0x40,  0x00, 0x00, 0x50,  0x00, 0x00, 0x60,  0x00, 0x00, 0x70,
0x00, 0x00, 0x80,  0x00, 0x00, 0x90,  0x00, 0x00, 0xA0,  0x00, 0x00, 0xB0,
0x00, 0x00, 0xC0,  0x00, 0x00, 0xD0,  0x00, 0x00, 0xE0,  0x00, 0x00, 0xFF,

// Blue to cyan (blue and green)
0x00, 0x00, 0xFF,  0x00, 0x10, 0xFF,  0x00, 0x20, 0xFF,  0x00, 0x30, 0xFF,
0x00, 0x40, 0xFF,  0x00, 0x50, 0xFF,  0x00, 0x60, 0xFF,  0x00, 0x70, 0xFF,
0x00, 0x80, 0xFF,  0x00, 0x90, 0xFF,  0x00, 0xA0, 0xFF,  0x00, 0xB0, 0xFF,
0x00, 0xC0, 0xFF,  0x00, 0xD0, 0xFF,  0x00, 0xE0, 0xFF,  0x00, 0xFF, 0xFF,

// Cyan (blue and green) to green
0x00, 0xFF, 0xFF,  0x00, 0xFF, 0xE0,  0x00, 0xFF, 0xD0,  0x00, 0xFF, 0xC0,
0x00, 0xFF, 0xB0,  0x00, 0xFF, 0xA0,  0x00, 0xFF, 0x90,  0x00, 0xFF, 0x80,
0x00, 0xFF, 0x70,  0x00, 0xFF, 0x60,  0x00, 0xFF, 0x50,  0x00, 0xFF, 0x40,
0x00, 0xFF, 0x30,  0x00, 0xFF, 0x20,  0x00, 0xFF, 0x10,  0x00, 0xFF, 0x00,

// Green to yellow (red and green)
0x00, 0xFF, 0x00,  0x10, 0xFF, 0x00,  0x20, 0xFF, 0x00,  0x30, 0xFF, 0x00,
0x40, 0xFF, 0x00,  0x50, 0xFF, 0x00,  0x60, 0xFF, 0x00,  0x70, 0xFF, 0x00,
0x80, 0xFF, 0x00,  0x90, 0xFF, 0x00,  0xA0, 0xFF, 0x00,  0xB0, 0xFF, 0x00,
0xC0, 0xFF, 0x00,  0xD0, 0xFF, 0x00,  0xE0, 0xFF, 0x00,  0xFF, 0xFF, 0x00,

// Yellow (red and green) to red
0xFF, 0xFF, 0x00,  0xFF, 0xE0, 0x00,  0xFF, 0xD0, 0x00,  0xFF, 0xC0, 0x00,
0xFF, 0xB0, 0x00,  0xFF, 0xA0, 0x00,  0xFF, 0x90, 0x00,  0xFF, 0x80, 0x00,
0xFF, 0x70, 0x00,  0xFF, 0x60, 0x00,  0xFF, 0x50, 0x00,  0xFF, 0x40, 0x00,
0xFF, 0x30, 0x00,  0xFF, 0x20, 0x00,  0xFF, 0x10, 0x00,  0xFF, 0x00, 0x00,

// Red to magenta (blue and red)
0xFF, 0x00, 0x00,  0xFF, 0x00, 0x10,  0xFF, 0x00, 0x20,  0xFF, 0x00, 0x30,
0xFF, 0x00, 0x40,  0xFF, 0x00, 0x50,  0xFF, 0x00, 0x60,  0xFF, 0x00, 0x70,
0xFF, 0x00, 0x80,  0xFF, 0x00, 0x90,  0xFF, 0x00, 0xA0,  0xFF, 0x00, 0xB0,
0xFF, 0x00, 0xC0,  0xFF, 0x00, 0xD0,  0xFF, 0x00, 0xE0,  0xFF, 0x00, 0xFF,

// Magenta (blue and red) to blue
0xFF, 0x00, 0xFF,  0xE0, 0x00, 0xFF,  0xD0, 0x00, 0xFF,  0xC0, 0x00, 0xFF,
0xB0, 0x00, 0xFF,  0xA0, 0x00, 0xFF,  0x90, 0x00, 0xFF,  0x80, 0x00, 0xFF,
0x70, 0x00, 0xFF,  0x60, 0x00, 0xFF,  0x50, 0x00, 0xFF,  0x40, 0x00, 0xFF,
0x30, 0x00, 0xFF,  0x20, 0x00, 0xFF,  0x10, 0x00, 0xFF,  0x00, 0x00, 0xFF,

// Black to magenta (blue and red)
0x00, 0x00, 0x00,  0x10, 0x00, 0x10,  0x20, 0x00, 0x20,  0x30, 0x00, 0x30,
0x40, 0x00, 0x40,  0x50, 0x00, 0x50,  0x60, 0x00, 0x60,  0x70, 0x00, 0x70,
0x80, 0x00, 0x80,  0x90, 0x00, 0x90,  0xA0, 0x00, 0xA0,  0xB0, 0x00, 0xB0,
0xC0, 0x00, 0xC0,  0xD0, 0x00, 0xD0,  0xE0, 0x00, 0xE0,  0xFF, 0x00, 0xFF,

// Black to cyan (blue and green)
0x00, 0x00, 0x00,  0x00, 0x10, 0x10,  0x00, 0x20, 0x20,  0x00, 0x30, 0x30,
0x00, 0x40, 0x40,  0x00, 0x50, 0x50,  0x00, 0x60, 0x60,  0x00, 0x70, 0x70,
0x00, 0x80, 0x80,  0x00, 0x90, 0x90,  0x00, 0xA0, 0xA0,  0x00, 0xB0, 0xB0,
0x00, 0xC0, 0xC0,  0x00, 0xD0, 0xD0,  0x00, 0xE0, 0xE0,  0x00, 0xFF, 0xFF,

// Red to white
0xFF, 0x00, 0x00,  0xFF, 0x10, 0x10,  0xFF, 0x20, 0x20,  0xFF, 0x30, 0x30,
0xFF, 0x40, 0x40,  0xFF, 0x50, 0x50,  0xFF, 0x60, 0x60,  0xFF, 0x70, 0x70,
0xFF, 0x80, 0x80,  0xFF, 0x90, 0x90,  0xFF, 0xA0, 0xA0,  0xFF, 0xB0, 0xB0,
0xFF, 0xC0, 0xC0,  0xFF, 0xD0, 0xD0,  0xFF, 0xE0, 0xE0,  0xFF, 0xFF, 0xFF,

// Green to white
0x00, 0xFF, 0x00,  0x10, 0xFF, 0x10,  0x20, 0xFF, 0x20,  0x30, 0xFF, 0x30,
0x40, 0xFF, 0x40,  0x50, 0xFF, 0x50,  0x60, 0xFF, 0x60,  0x70, 0xFF, 0x70,
0x80, 0xFF, 0x80,  0x90, 0xFF, 0x90,  0xA0, 0xFF, 0xA0,  0xB0, 0xFF, 0xB0,
0xC0, 0xFF, 0xC0,  0xD0, 0xFF, 0xD0,  0xE0, 0xFF, 0xE0,  0xFF, 0xFF, 0xFF,

// Blue to white
0x00, 0x00, 0xFF,  0x10, 0x10, 0xFF,  0x20, 0x20, 0xFF,  0x30, 0x30, 0xFF,
0x40, 0x40, 0xFF,  0x50, 0x50, 0xFF,  0x60, 0x60, 0xFF,  0x70, 0x70, 0xFF,
0x80, 0x80, 0xFF,  0x90, 0x90, 0xFF,  0xA0, 0xA0, 0xFF,  0xB0, 0xB0, 0xFF,
0xC0, 0xC0, 0xFF,  0xD0, 0xD0, 0xFF,  0xE0, 0xE0, 0xFF,  0xFF, 0xFF, 0xFF
};
#endif

struct RegInit
{
  GuiConst_INT16U index;
  GuiConst_INT16U value;
};

// Use 13706CFG PC-software generator for creation of register's values
// according your demands
struct RegInit S1D13706Mode[0x100] =
{
  {0x04,0x00},   // BUSCLK MEMCLK Config Register
  {0x05,0x43},   // PCLK Config  Register
  {0x10,0x61},   // PANEL Type Register
  {0x11,0x00},   // MOD Rate Register
  {0x12,0x2B},   // Horizontal Total Register
  {0x14,0x27},   // Horizontal Display Period Register
  {0x16,0x00},   // Horizontal Display Period Start Pos Register 0
  {0x17,0x00},   // Horizontal Display Period Start Pos Register 1
  {0x18,0xFA},   // Vertical Total Register 0
  {0x19,0x00},   // Vertical Total Register 1
  {0x1C,0xEF},   // Vertical Display Period Register 0
  {0x1D,0x00},   // Vertical Display Period Register 1
  {0x1E,0x00},   // Vertical Display Period Start Pos Register 0
  {0x1F,0x00},   // Vertical Display Period Start Pos Register 1
  {0x20,0x87},   // Horizontal Sync Pulse Width Register
  {0x22,0x00},   // Horizontal Sync Pulse Start Pos Register 0
  {0x23,0x00},   // Horizontal Sync Pulse Start Pos Register 1
  {0x24,0x80},   // Vertical Sync Pulse Width Register
  {0x26,0x00},   // Vertical Sync Pulse Start Pos Register 0
  {0x27,0x00},   // Vertical Sync Pulse Start Pos Register 1
#ifdef GuiConst_COLOR_DEPTH_2
  {0x70,0x01},   // Display Mode Register
#else
#ifdef GuiConst_COLOR_DEPTH_8
  {0x70,0x03},   // Display Mode Register
#else
  {0x70,0x04},   // Display Mode Register
#endif
#endif
  {0x71,0x00},   // Special Effects Register
  {0x74,0x00},   // Main Window Display Start Address Register 0
  {0x75,0x00},   // Main Window Display Start Address Register 1
  {0x76,0x00},   // Main Window Display Start Address Register 2
#ifdef GuiConst_COLOR_DEPTH_2
  {0x78,GuiConst_DISPLAY_WIDTH_HW/16}, // Main Window Address Offset Register 0
#else
#ifdef GuiConst_COLOR_DEPTH_8
  {0x78,GuiConst_DISPLAY_WIDTH_HW/4}, // Main Window Address Offset Register 0
#else
  {0x78,GuiConst_DISPLAY_WIDTH_HW/2}, // Main Window Address Offset Register 0
#endif
#endif
  {0x79,0x00},   // Main Window Address Offset Register 1
  {0x7C,0x00},   // Sub Window Display Start Address Register 0
  {0x7D,0x00},   // Sub Window Display Start Address Register 1
  {0x7E,0x00},   // Sub Window Display Start Address Register 2
  {0x80,0x50},   // Sub Window Address Offset Register 0
  {0x81,0x00},   // Sub Window Address Offset Register 1
  {0x84,0x00},   // Sub Window X Start Pos Register 0
  {0x85,0x00},   // Sub Window X Start Pos Register 1
  {0x88,0x00},   // Sub Window Y Start Pos Register 0
  {0x89,0x00},   // Sub Window Y Start Pos Register 1
  {0x8C,0x4F},   // Sub Window X End Pos Register 0
  {0x8D,0x00},   // Sub Window X End Pos Register 1
  {0x90,0xEF},   // Sub Window Y End Pos Register 0
  {0x91,0x00},   // Sub Window Y End Pos Register 1
  {0xA0,0x00},   // Power Save Config Register
  {0xA1,0x00},   // CPU Access Control Register
  {0xA2,0x00},   // Software Reset Register
  {0xA3,0x00},   // BIG Endian Support Register
  {0xA4,0x00},   // Scratch Pad Register 0
  {0xA5,0x00},   // Scratch Pad Register 1
  {0xA8,0x00},   // GPIO Config Register 0
  {0xA9,0x80},   // GPIO Config Register 1
  {0xAC,0x00},   // GPIO Status Control Register 0
  {0xAD,0x00},   // GPIO Status Control Register 1
  {0xB0,0x00},   // PWM CV Clock Control Register
  {0xB1,0x00},   // PWM CV Clock Config Register
  {0xB2,0x00},   // CV Clock Burst Length Register
  {0xB3,0x00},   // PWM Clock Duty Cycle Register
  {0xffff,0xffff}// End of the table
};

// Initialises the module and the display
void GuiDisplay_Init(void)
{
#ifdef MEM_MAPPED
  volatile GuiConst_INT8U * pRegs = REGISTER_OFFSET;
#endif
  GuiConst_INT16U idx = 0;
  GuiConst_INT16U tmp, tmp1;
  GuiConst_INT16U i, j;

  // Blank display
#ifdef MEM_MAPPED
  *(pRegs + 0x70) |= 0x80;
#else
  LCD_Init();
  tmp = LCD_Read(0x70);
  LCD_Cmd(0x70,(GuiConst_INT8U)(tmp|0x80));
#endif

  // Write to all S1D13706 registers
  while(1)
  {
    if( (S1D13706Mode[idx].index==0xffff) && (S1D13706Mode[idx].value==0xffff) )
      break;
#ifdef MEM_MAPPED
    *(pRegs + S1D13706Mode[idx].index) = (GuiConst_INT8U)(S1D13706Mode[idx].value);
#else
    LCD_Cmd(S1D13706Mode[idx].index,(GuiConst_INT8U)S1D13706Mode[idx].value);
#endif
    idx++;
  }

#ifdef GuiConst_COLOR_DEPTH_2
  // Update LookUp Table
  for (idx = 0; idx < 3; idx++)
  {
#ifdef MEM_MAPPED
    *(pRegs + 0x0A) = LUT2_Color[3*idx];     // red
    *(pRegs + 0x09) = LUT2_Color[3*idx+1];   // green
    *(pRegs + 0x08) = LUT2_Color[3*idx+2];   // blue
    *(pRegs + 0x0B) = (GuiConst_INT8U) idx;  // write index to update LUT entry
#else
    LCD_Cmd(0x0A, LUT2_Color[3*idx]);     // red
    LCD_Cmd(0x09, LUT2_Color[3*idx+1]);   // green
    LCD_Cmd(0x08, LUT2_Color[3*idx+2]);   // blue
    LCD_Cmd(0x0B, (GuiConst_INT8U) idx);  // write index to update LUT entry
#endif
  }
#endif
#ifdef GuiConst_COLOR_DEPTH_8
  // Load palette data from Easygui or use pre-built 256 color palette set
  for (i = 0; i < GuiConst_PALETTE_SIZE; i++)
    for (j = 0; j < GuiConst_COLOR_BYTE_SIZE; j++)
      LUT8_Color[3*i+j] = GuiStruct_Palette[i][j];

  // Update LookUp Table
  for (idx = 0; idx < 256; idx++)
  {
#ifdef MEM_MAPPED
    *(pRegs + 0x0A) = LUT8_Color[3*idx];     // red
    *(pRegs + 0x09) = LUT8_Color[3*idx+1];   // green
    *(pRegs + 0x08) = LUT8_Color[3*idx+2];   // blue
    *(pRegs + 0x0B) = (GuiConst_INT8U) idx;  // write index to update LUT entry
#else
    LCD_Cmd(0x0A, LUT8_Color[3*idx]);     // red
    LCD_Cmd(0x09, LUT8_Color[3*idx+1]);   // green
    LCD_Cmd(0x08, LUT8_Color[3*idx+2]);   // blue
    LCD_Cmd(0x0B, (GuiConst_INT8U) idx);  // write index to update LUT entry
#endif
  }
#endif

  // Power Up LCD
  // a. If s/w power save mode is on (REG[A0h] bit 0 = 1), turn off s/w power
  //    save mode.
  // b. If LCD power is off (REG[ADh] bit 7 = 0), delay for xx milli-seconds
  //    (depending on panel specification).
  // c. Turn on LCD power (REG[ADh] bit 7 = 1).

#ifdef MEM_MAPPED
  tmp = *(pRegs + 0xa0);
#else
  tmp = LCD_Read(0xa0);
#endif

  if( tmp & 0x01 )
#ifdef MEM_MAPPED
    *(pRegs + 0xa0) = (GuiConst_INT8U) (tmp & ~0x01);
#else
    LCD_Cmd(0xa0,(GuiConst_INT8U) (tmp & ~0x01));
#endif

#ifdef MEM_MAPPED
  if( !( *(pRegs + 0xad) & 0x80) )
#else
  if( !(LCD_Read(0xad) & 0x80) )
#endif
  {
    // Delay at least 2 vertical periods for this panel
#ifdef MEM_MAPPED
    tmp1 = *(pRegs + 0xa0) & 0x80;  // VNDP status
#else
    tmp1 = LCD_Read(0xa0) & 0x80;
#endif

    for ( idx=0; idx<5; idx++)
    {
      tmp = tmp1;

      while(1)
      {
#ifdef MEM_MAPPED
        tmp1 = *(pRegs + 0xa0) & 0x80;  // VNDP status
#else
        tmp1 = LCD_Read(0xa0) & 0x80;
#endif
        if ( tmp1 != tmp )
          break;
      }
    }
  }

#ifdef MEM_MAPPED
  *(pRegs + 0xad) = (GuiConst_INT8U) (*(pRegs + 0xad)|0x80);
#else
  LCD_Cmd(0xad, (LCD_Read(0xad)|0x80));
#endif

  // Disable "blanking" display (set REG[70h] bit 7 to 0)
#ifdef MEM_MAPPED
  tmp = *(pRegs + 0x70);
  *(pRegs + 0x70) = (GuiConst_INT8U) (tmp & ~0x80);
#else
  tmp = LCD_Read(0x70);
  LCD_Cmd(0x70,(GuiConst_INT8U) (tmp & ~0x80));
#endif
}

// Refreshes display buffer to display
void GuiDisplay_Refresh(void)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S X;
  GuiConst_INT16S LastByte;
#ifdef MEM_MAPPED
  volatile GuiConst_INT8U * pDisp;
#else
  GuiConst_INT16U Address;
#endif
  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Calculate start address in display memory
#ifdef MEM_MAPPED
      pDisp = DISP_MEM_OFFSET;
      pDisp +=
#else
      Address =
#endif
#ifdef GuiConst_COLOR_DEPTH_16
         ((Y*GuiConst_BYTES_PR_LINE) + GuiLib_DisplayRepaint[Y].ByteBegin * 2);
#else
#ifdef GuiConst_COLOR_DEPTH_8
         ((Y*GuiConst_BYTES_PR_LINE) + GuiLib_DisplayRepaint[Y].ByteBegin);
#else
         ((Y*GuiConst_BYTES_PR_LINE) + GuiLib_DisplayRepaint[Y].ByteBegin / 4);
#endif
#endif

      // Calculate last byte
#ifdef GuiConst_COLOR_DEPTH_16
      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
#else
      LastByte = GuiConst_BYTES_PR_LINE - 1;
#endif

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
#ifdef GuiConst_COLOR_DEPTH_16
#ifdef MEM_MAPPED
        *(pDisp++) = (GuiConst_INT8U)(GuiLib_DisplayBuf.Words[Y][X]>>8);
        *(pDisp++) = (GuiConst_INT8U)GuiLib_DisplayBuf.Words[Y][X];
#else
        LCD_Data(Address++,(GuiConst_INT8U)(GuiLib_DisplayBuf.Words[Y][X]>>8));
        LCD_Data(Address++,(GuiConst_INT8U)GuiLib_DisplayBuf.Words[Y][X]);
#endif
#else
#ifdef MEM_MAPPED
        *(pDisp++) = GuiLib_DisplayBuf[Y][X];
#else
        LCD_Data(Address++, GuiLib_DisplayBuf[Y][X]);
#endif
#endif
      }
      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S1D13706

#ifdef LCD_CONTROLLER_TYPE_S1D13715

// ============================================================================
//
// S1D13715 DISPLAY CONTROLLER
//
// This driver uses one S1D13715 display controller with built-in pll module.
// LCD controller in 16 bit 8080 type 2 interface, indirect memory access mode
// or 16 bit 8080 type 1 interface direct memory access mode.
// Output is LCD1 in generic TFT 16 RGB interface 5-6-5.
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// Uncomment for direct mode
// #define DIRECT_MODE

// Hardwired connection for CS mode 1, 8080 direct type 1 interface

// DB[15-0] = connect to D15-D0 data bus of microcontroller
// AB[18-1] = connect to A18-A1 of address bits of microcontroller
// M/R      = connect to A19 pin of address bit of microcontroller
// CS       = connect to CS pin of microcontroller
// WE       = connect to WR pin of microcontroller
// BE0      = connect to UBE pin of microcontroller
// BE1      = connect to LBE pin of microcontroller
// RD       = connect to RD pin of microcontroller
// WAIT     = connect to WAIT pin of microcontroller
// CNF0     = Low, WAIT not used
// CNF1     = Low, GPIO outputs
// CNF2     = Low, Direct type 1
// CNF3     = Low  Direct type 1
// CNF4     = High Direct type 1
// CNF5     = Low, Little Endian
// CNF6     = Low, 1 CS mode

// Hardwired connection for CS mode 1, 8080 indirect type 2 interface

// AB[18-2] = Low
// M/R      = Low
// WE       = High
// CNF0     = Low, WAIT not used
// CNF1     = Low, GPIO outputs
// CNF2     = Low, indirect type 2
// CNF3     = High, Indirect type 2
// CNF4     = Low, Indirect type 2
// CNF5     = Low, Little Endian
// CNF6     = Low, 1 CS mode

#ifdef DIRECT_MODE
  // Adjust according your controller mapping in memory space and CS connection
  #define REGISTER_ADDRESS          0x00000
  #define RESET_ON                  Pxx.x = 0     // RESET pin
  #define RESET_OFF                 Pxx.x = 1     // RESET pin
#else
  #define DISP_DATA                 Pxx           // DB[15-0] pins
  #define RESET_ON                  Pxx.x = 0     // RESET pin
  #define RESET_OFF                 Pxx.x = 1     // RESET pin
  #define CHIP_ENABLED              Pxx.x = 0     // CS pin
  #define CHIP_DISABLED             Pxx.x = 1     // CS pin
  #define WEU_LOW                   Pxx.x = 0     // BE1 pin
  #define WEU_HIGH                  Pxx.x = 1     // BE1 pin
  #define WEL_LOW                   Pxx.x = 0     // BE0 pin
  #define WEL_HIGH                  Pxx.x = 1     // BE0 pin
  #define RD_LOW                    Pxx.x = 0     // RD pin
  #define RD_HIGH                   Pxx.x = 1     // RD pin
  #define MODE_COMMAND              Pxx.x = 0     // AB1 pin
  #define MODE_DATA                 Pxx.x = 1     // AB1 pin
  #define NOP                       asm ("nop")   // nop code
#endif

#define SRAM_OFFSET               0x80000
#define HDPS                      12            // Horizontal Display Period Start Position in pixels
#define HPW                       5             // Horizontal Pulse (FPLINE) Width in pixels
#define HPP                       5             // Horizontal Pulse (FPLINE) Start Position in pixels
#define VDPS                      12            // Vertical Display Period Start Position in lines
#define VPW                       5             // Vertical Pulse (FPFRAME) Width in lines
#define VPP                       5             // Vertical Pulse (FPFRAME) Start Position in lines

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

#ifndef DIRECT_MODE
void WRITE_COMMAND(GuiConst_INT16U address)
{
  RD_HIGH;
  MODE_COMMAND;
  CHIP_ENABLED;
  DISP_DATA = address;
  WEU_LOW;
  WEL_LOW;
  //NOP;
  WEU_HIGH;
  WEL_HIGH;
  CHIP_DISABLED;
}

void WRITE_DATA(GuiConst_INT16U data)
{
  RD_HIGH;
  MODE_DATA;
  CHIP_ENABLED;
  DISP_DATA = data;
  WEU_LOW;
  WEL_LOW;
  //NOP;
  WEU_HIGH;
  WEL_HIGH;
  CHIP_DISABLED;
}
#endif

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT16U X;

#ifdef DIRECT_MODE
  volatile GuiConst_INT16U * pRegs = REGISTER_ADDRESS;
#endif

  // Reset controller
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;

#ifdef DIRECT_MODE
  // Software reset setting register
  *(pRegs + 0x0016) = 0xFFFF;

  // PLL frequency = 54MHz
  // PLL Setting Register 0
    *(pRegs + 0x000E) = 0xF199;

  // PLL Setting Register 1
  *(pRegs + 0x0010) = 0x2000;

  // PLL Setting Register 2
  // Enable PLL - 0x0000, disable PLL - 0x0001
  *(pRegs + 0x0012) = 0x0000;

  // Wait for stable PLL
  millisecond_delay(100);

  // System clock setting register
  *(pRegs + 0x0018) = 0x0000;

  // Miscellaneous Configuration Register
  // Disable power save mode 0.bit = 0
  *(pRegs + 0x0014) = 0x0010;

  // Bus Timeout Setting Register
  *(pRegs + 0x0006) = 0x0000;

  // LCD Interface Clock Setting Register
  *(pRegs + 0x0030) = 0x0503;

  // LCD Interface Configuration Register
  // Bits 1-0: 00 - RGB interface, 10 - parallel interface
  // Bits 6-4: 000 - 9 bits, 001 - 12 bits, 010 - 16 bits, 011 - 18 bits, 100 - 24 bits width
  // Bit 7: 0 - signals change at the rising edge of FPSHIFT, 1 - at the falling edge of FPSHIFT
  // Bit 15-10: 0 - General TFT, 1 - HR-TFT, 2 - Casio TFT, 3 - TFT type 2, 4 - TFT type 3
  *(pRegs + 0x0032) = 0x0020;

  // LCD1 Horizontal Total Register
  *(pRegs + 0x0040) = (( GuiConst_DISPLAY_WIDTH_HW + HDPS*2 )/ 8 - 1);

  // LCD1 Horizontal Display Period Register
  *(pRegs + 0x0042) = (GuiConst_DISPLAY_WIDTH_HW / 2 - 1);

  // LCD1 Horizontal Display Period Start Position Register
  *(pRegs + 0x0044) = (HDPS-9);

  // LCD1 Horizontal Pulse Register
  // 7. bit: 0 - horizontal sync signal FPLINE is active low, 1 - active high
  *(pRegs + 0x0046) = (0x0000 | (HPW-1));

  // LCD1 FPLINE Pulse Position Register
  *(pRegs + 0x0048) = (HPP-1);

  // LCD1 Vertical Total Register
  *(pRegs + 0x004A) = (GuiConst_BYTE_LINES + VDPS*2  - 1);

  // LCD1 Vertical Display Period Register
  *(pRegs + 0x004C) = (GuiConst_BYTE_LINES  - 1);

  // LCD1 Vertical Display Period Start Position Register
  *(pRegs + 0x004E) = (VDPS);

  // LCD1 Vertical FPFRAME Register
  // 7. bit: 0 - horizontal sync signal FPFRAME is active low, 1 - active high
  *(pRegs + 0x0050) = (0x0000 | (VPW-1));

  // LCD1 Vertical FPFRAME Pulse Position Register
  *(pRegs + 0x0052) = (VPP-1);

  // Display Mode Setting Register 0
  // bits [1-0]:color depth LUT1: 0 - 8bit, 1 - 16bit, 3 - 32bit
  // 4.bit: 0 - LUT1 bypass disable, 1 - LUT1 bypass enable
  // bits[9-8]: 0 - Main window, 1 - Main window + PIP, 3 - Main window + PIP + Overlay
  // 12.bit: 1 - double buffering enabled, 0 - double buffering disabled
  *(pRegs + 0x0200) = 0x0011;

  // Display Mode Setting Register 1
  // bits[1-0]: Swivel view mode 0 - normal, 1 - rotated 90, 2 - rotated 180, 3 - rotated 270
  // 3.bit: 0 - display mirror disabled, 1 - diaplay mirror enabled
  // 8.bit: 0 - normal output, 1 - forced low or high
  // 9.bit: 0 - normal, 1 - inverted output
  // bits[12-10]: 0 - all off, 1 - LCD1, 2 - LCD2
  *(pRegs + 0x0202) = 0x0400;

  // Main1 Window display start address register 0
  *(pRegs + 0x0210) = 0x0000;

  // Main1 Window display start address register 1
  *(pRegs + 0x0212) = 0x0000;

  // Main1 Window Line address offset register
  *(pRegs + 0x0216) = (GuiConst_DISPLAY_WIDTH_HW*GuiConst_COLOR_SIZE/8);

  millisecond_delay(10);

  // Clear display memory
  pRegs += SRAM_OFFSET;
  // Write memory data
  for (X = 0; X < GuiConst_DISPLAY_BYTES/2; X++)   // Loop through bytes
    *(pRegs++) = 0x0000;

#else
  // Software reset setting register
  WRITE_COMMAND(0x0016);
  WRITE_DATA(0xFFFF);

  // PLL frequency = 54MHz
  // PLL Setting Register 0
  WRITE_COMMAND(0x000E);
  WRITE_DATA(0xF199);

  // PLL Setting Register 1
  WRITE_COMMAND(0x0010);
  WRITE_DATA(0x2000);

  // PLL Setting Register 2
  WRITE_COMMAND(0x0012);
  // Enable PLL - 0x0000, disable PLL - 0x0001
  WRITE_DATA(0x0000);

  // Wait for stable PLL
  millisecond_delay(100);

  // System clock setting register
  WRITE_COMMAND(0x0018);
  WRITE_DATA(0x0000);

  // Miscellaneous Configuration Register
  WRITE_COMMAND(0x0014);
  // Disable power save mode 0.bit = 0
  WRITE_DATA(0x0010);

  // Bus Timeout Setting Register
  WRITE_COMMAND(0x0006);
  WRITE_DATA(0x0000);

  // Indirect Interface Auto Increment Register
  WRITE_COMMAND(0x0026);
  // Increment only when world access take place
  WRITE_DATA(0x0001);

  // LCD Interface Clock Setting Register
  WRITE_COMMAND(0x0030);
  WRITE_DATA(0x0503);

  // LCD Interface Configuration Register
  WRITE_COMMAND(0x0032);
  // Bits 1-0: 00 - RGB interface, 10 - parallel interface
  // Bits 6-4: 000 - 9 bits, 001 - 12 bits, 010 - 16 bits, 011 - 18 bits, 100 - 24 bits width
  // Bit 7: 0 - signals change at the rising edge of FPSHIFT, 1 - at the falling edge of FPSHIFT
  // Bit 15-10: 0 - General TFT, 1 - HR-TFT, 2 - Casio TFT, 3 - TFT type 2, 4 - TFT type 3
  WRITE_DATA(0x0020);

  // LCD1 Horizontal Total Register
  WRITE_COMMAND(0x0040);
  WRITE_DATA(( GuiConst_DISPLAY_WIDTH_HW + HDPS*2 )/ 8 - 1);

  // LCD1 Horizontal Display Period Register
  WRITE_COMMAND(0x0042);
  WRITE_DATA(GuiConst_DISPLAY_WIDTH_HW / 2 - 1);

  // LCD1 Horizontal Display Period Start Position Register
  WRITE_COMMAND(0x0044);
  WRITE_DATA(HDPS-9);

  // LCD1 Horizontal Pulse Register
  WRITE_COMMAND(0x0046);
  // 7. bit: 0 - horizontal sync signal FPLINE is active low, 1 - active high
  WRITE_DATA(0x0000 | (HPW-1));

  // LCD1 FPLINE Pulse Position Register
  WRITE_COMMAND(0x0048);
  WRITE_DATA(HPP-1);

  // LCD1 Vertical Total Register
  WRITE_COMMAND(0x004A);
  WRITE_DATA(GuiConst_BYTE_LINES + VDPS*2  - 1);

  // LCD1 Vertical Display Period Register
  WRITE_COMMAND(0x004C);
  WRITE_DATA(GuiConst_BYTE_LINES  - 1);

  // LCD1 Vertical Display Period Start Position Register
  WRITE_COMMAND(0x004E);
  WRITE_DATA(VDPS);

  // LCD1 Vertical FPFRAME Register
  WRITE_COMMAND(0x0050);
  // 7. bit: 0 - horizontal sync signal FPFRAME is active low, 1 - active high
  WRITE_DATA(0x0000 | (VPW-1));

  // LCD1 Vertical FPFRAME Pulse Position Register
  WRITE_COMMAND(0x0052);
  WRITE_DATA(VPP-1);

  // Display Mode Setting Register 0
  WRITE_COMMAND(0x0200);
  // bits [1-0]:color depth LUT1: 0 - 8bit, 1 - 16bit, 3 - 32bit
  // 4.bit: 0 - LUT1 bypass disable, 1 - LUT1 bypass enable
  // bits[9-8]: 0 - Main window, 1 - Main window + PIP, 3 - Main window + PIP + Overlay
  // 12.bit: 1 - double buffering enabled, 0 - double buffering disabled
  WRITE_DATA(0x0011);

  // Display Mode Setting Register 1
  WRITE_COMMAND(0x0202);
  // bits[1-0]: Swivel view mode 0 - normal, 1 - rotated 90, 2 - rotated 180, 3 - rotated 270
  // 3.bit: 0 - display mirror disabled, 1 - diaplay mirror enabled
  // 8.bit: 0 - normal output, 1 - forced low or high
  // 9.bit: 0 - normal, 1 - inverted output
  // bits[12-10]: 0 - all off, 1 - LCD1, 2 - LCD2
  WRITE_DATA(0x0400);

  // Main1 Window display start address register 0
  WRITE_COMMAND(0x0210);
  WRITE_DATA(0x0000);

  // Main1 Window display start address register 1
  WRITE_COMMAND(0x0212);
  WRITE_DATA(0x0000);

  // Main1 Window Line address offset register
  WRITE_COMMAND(0x0216);
  WRITE_DATA(GuiConst_DISPLAY_WIDTH_HW*GuiConst_COLOR_SIZE/8);

  millisecond_delay(10);

  // Clear display memory
  // Memory start lower word of  address
  WRITE_COMMAND(0x0022);
  WRITE_DATA(0x0000);
  // Memory start higher word of address
  WRITE_COMMAND(0x0024);
  WRITE_DATA(0x0000);

  // Memory data port address
  WRITE_COMMAND(0x0028);
  for (X = 0; X < GuiConst_DISPLAY_BYTES/2; X++)   // Loop through bytes
    WRITE_DATA(0x0000);
#endif
}

// Refreshes display buffer to display
void GuiDisplay_Refresh(void)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S X;
  GuiConst_INT16S LastByte;
  GuiConst_INT32U Address;

#ifdef DIRECT_MODE
  volatile GuiConst_INT16U * pRegs;
#endif

  GuiDisplay_Lock ();

  // Walk through all lines
  for(Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      Address = (Y * GuiConst_BYTES_PR_LINE/2 +
                 GuiLib_DisplayRepaint[Y].ByteBegin) * 2;

#ifdef DIRECT_MODE
      // Calculate start address
      pRegs = (GuiConst_INT16U*)(REGISTER_ADDRESS + SRAM_OFFSET);
      pRegs += Address;
#else
      // Memory start lower word of address
      WRITE_COMMAND(0x0022);
      WRITE_DATA((GuiConst_INT16U)Address);
      // Memory start higher word of address
      WRITE_COMMAND(0x0024);
      WRITE_DATA((GuiConst_INT16U)(Address>>15));
#endif

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

#ifdef DIRECT_MODE
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        *pRegs++ = GuiLib_DisplayBuf.Words[Y][X];
#else
      // Memory data access port address
      WRITE_COMMAND(0x0028);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        WRITE_DATA(GuiLib_DisplayBuf.Words[Y][X]);
#endif

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S1D13715

#ifdef LCD_CONTROLLER_TYPE_S1D13743

// ============================================================================
//
// S1D13743 DISPLAY CONTROLLER
//
// This driver uses one S1D13743 display controller.
// LCD display 8 bit 8080 type interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 352x440 pixels for S1D13743.
// Graphic modes up to 800x480 pixels for S1D13742.
//
// Compatible display controllers:
//   S1D13742
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 24 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// 8 bit bus - CNF1 = 0

// Uncomment for Microchip PMP interface
#define PIC32_PMP_PORT
//
#ifdef PIC32_PMP_PORT
#include <plib.h>
// Connections between PMP port and driver for PMP master mode 2
// 8 bit interface
// PMA0       -      D/C#
// PMD[7-0]  -      MD[7-0]
// PMCS2      -      CS#
// PMRD       -      WE
// PMWR       -      RD

// Reset pin on PORT A
#define RESET_PIN   BIT_0

// Configuration Bit settings
// SYSCLK = 80 MHz (8MHz Crystal/ FPLLIDIV * FPLLMUL / FPLLODIV)
// PBCLK = 40 MHz
// Primary Osc w/PLL (XT+,HS+,EC+PLL)
// WDT OFF

#pragma config FPLLMUL = MUL_20, FPLLIDIV = DIV_2, FPLLODIV = DIV_1, FWDTEN = OFF
#pragma config POSCMOD = HS, FNOSC = PRIPLL, FPBDIV = DIV_8

#define SYS_FREQ    (80000000L)

// Pre-defined bit masks are located in the PIC32MX PMP peripheral library header file
// pmp.h
#define CONTROL     (PMP_ON | PMP_IDLE_CON | PMP_MUX_OFF | PMP_READ_WRITE_EN |\
                     PMP_CS2_CS1_EN | PMP_LATCH_POL_HI | PMP_CS2_POL_LO | PMP_CS1_POL_LO |\
                     PMP_WRITE_POL_LO | PMP_READ_POL_LO)

#define MODE        (PMP_IRQ_OFF | PMP_AUTO_ADDR_OFF | PMP_DATA_BUS_8 | PMP_MODE_MASTER2 |\
                     PMP_WAIT_BEG_3 | PMP_WAIT_MID_7 | PMP_WAIT_END_3 )

#define PORT        (PMP_PEN_ALL)

#define INTERRUPT   (PMP_INT_OFF)

#else

#define DISP_DATA                 Pxx

#define RESET_ON                  Pxx.x = 0     // RES pin
#define RESET_OFF                 Pxx.x = 1     // RES pin
#define CHIP_ENABLED              Pxx.x = 0     // CS pin
#define CHIP_DISABLED             Pxx.x = 1     // CS pin
#define MODE_COMMAND              Pxx.x = 0     // D/C pin
#define MODE_DATA                 Pxx.x = 1     // D/C pin
#define RW_WR_LOW                 Pxx.x = 0     // R/W(WR) pin
#define RW_WR_HIGH                Pxx.x = 1     // R/W(WR) pin
#define E_RD_LOW                  Pxx.x = 0     // E(RD) pin
#define E_RD_HIGH                 Pxx.x = 1     // E(RD) pin
#define NOP                       asm ("nop")   // nop code

#endif

// LCD Register Names
#define REG04_PLLMDIV                0x04       // PLL M-Divider Register 0
#define REG06_PLL0                   0x06       // PLL Setting Register 0
#define REG08_PLL1                   0x08       // PLL Setting Register 1
#define REG0A_PLL2                   0x0A       // PLL Setting Register 2
#define REG0C_PLL3                   0x0C       // PLL Setting Register 3
#define REG0E_PLL4                   0x0E       // PLL Setting Register 4
#define REG12_CLKSRC                 0x12       // Clock Source Select Register
#define REG14_PNLTYPE                0x14       // Panel Type Register
#define REG16_HDISP                  0x16       // Horizontal Display Width Register
#define REG18_HNDP                   0x18       // Horizontal Non-Display Period Register
#define REG1A_VDISP0                 0x1A       // Vertical Display Height Register 0
#define REG1C_VDISP1                 0x1C       // Vertical Display Height Register 1
#define REG1E_VNDP                   0x1E       // Vertical Non-Display Period Register
#define REG20_HSW                    0x20       // HS Pulse Width Register
#define REG22_HPS                    0x22       // HS Pulse Start Position Register 0
#define REG24_VSW                    0x24       // VS Pulse Width Register
#define REG26_VPS                    0x26       // VS Pulse Start Position Register 0
#define REG28_PCLKPOL                0x28       // PCLK Polarity Register
#define REG2A_INMODE                 0x2A       // Input Mode Register
#define REG2C_YUVTRANS0              0x2C       // Input YUV/RGB Translate Mode Register 0
#define REG2E_YUVTRANS1              0x2E       // YUV/RGB Translate Mode Register 1
#define REG30_UDATAFIX               0x30       // U Data Fix Register
#define REG32_VDATAFIX               0x32       // V Data Fix Register
#define REG34_DISPMODE               0x34       // Display Mode Register
#define REG36_EFFECTS                0x36       // Special Effects Register
#define REG38_X_START_LOW            0x38       // Window X Start Position Register 0
#define REG3A_X_START_HIGH           0x3A       // Window X Start Position Register 1
#define REG3C_Y_START_LOW            0x3C       // Window Y Start Position Register 0
#define REG3E_Y_START_HIGH           0x3E       // Window Y Start Position Register 1
#define REG40_X_END_LOW              0x40       // Window X End Position Register 0
#define REG42_X_END_HIGH             0x42       // Window X End Position Register 1
#define REG44_Y_END_LOW              0x44       // Window Y End Position Register 0
#define REG46_Y_END_HIGH             0x46       // Window Y End Position Register 1
#define REG48_MEMORY_LOW             0x48       // Memory Data Port Register 0
#define REG49_MEMORY_HIGH            0x49       // Memory Data Port Register 1
#define REG4A_MEMRDADDR0             0x4A       // Memory Read Address Register 0
#define REG4C_MEMRDADDR1             0x4C       // Memory Read Address Register 1
#define REG4E_MEMRDADDR2             0x4E       // Memory Read Address Register 2
#define REG50_GAMMAEN                0x50       // Gamma Correction Enable Register
#define REG52_GAMMATBLIDX            0x52       // Gamma Correction Table Index Register
#define REG54_GAMMATBLDATA           0x54       // Gamma Correction Table Data Register
#define REG56_PWRSAVE                0x56       // Power Save Register
#define REG58_NDPCTRL                0x58       // Non-Display Period Control / Status Register

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void WRITE_COMMAND(GuiConst_INT8U v)
{
#ifdef PIC32_PMP_PORT
  PMPSetAddress(0x0000);
  PMPMasterWrite(v);
#else
  MODE_COMMAND;
  E_RD_HIGH;
  CHIP_ENABLED;
  RW_WR_LOW;
  DISP_DATA = v;
  //NOP;             // May be required for some microcontrollers
  RW_WR_HIGH;
  CHIP_DISABLED;
#endif
}

void WRITE_DATA(GuiConst_INT8U v)
{
#ifdef PIC32_PMP_PORT
  PMPSetAddress(0x0001);
  PMPMasterWrite(v);
#else
  MODE_DATA;
  E_RD_HIGH;
  CHIP_ENABLED;
  RW_WR_LOW;
  DISP_DATA = v;
  //NOP;             // May be required for some microcontrollers
  RW_WR_HIGH;
  CHIP_DISABLED;
#endif
}

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT16U X;

#ifdef PIC32_PMP_PORT
  // Configure cache, wait states and peripheral bus clock
  SYSTEMConfig(SYS_FREQ, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);

  // Setup the PMP
  mPMPOpen(CONTROL, MODE, PORT, INTERRUPT);

  // Reset driver chip
  // Set reset pin for driver chip as output
  PORTSetPinsDigitalOut(IOPORT_A, RESET_PIN);
  mPORTAClearBits(RESET_PIN);
  millisecond_delay(1);
  mPORTASetBits(RESET_PIN);
#else
  // Reset controller
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;
#endif

  // Power Save Register
  WRITE_COMMAND(REG56_PWRSAVE);
  WRITE_DATA(0x02);

  // PLL M-Divider Register
  WRITE_COMMAND(REG04_PLLMDIV);
  WRITE_DATA(0x13);

  // PLL Setting Register 0
  WRITE_COMMAND(REG06_PLL0);
  WRITE_DATA(0xF8);

  // PLL Setting Register 1
  WRITE_COMMAND(REG08_PLL1);
  WRITE_DATA(0x80);

  // PLL Setting Register 2
  WRITE_COMMAND(REG0A_PLL2);
  WRITE_DATA(0x28);

  //PLL Setting Register 3
  WRITE_COMMAND(REG0C_PLL3);
  WRITE_DATA(0x00);

  // PLL Setting Register 4
  WRITE_COMMAND(REG0E_PLL4);
  WRITE_DATA(0x3B);

  // Clock Source Select Register
  WRITE_COMMAND(REG12_CLKSRC);
  WRITE_DATA(0x29);

  // Power Save Register
  WRITE_COMMAND(REG56_PWRSAVE);
  WRITE_DATA(0x00);

  // Panel Type Register
  WRITE_COMMAND(REG14_PNLTYPE);
  WRITE_DATA(0x01);  // 0x00 18bit, 0x01 24 bit

  // Horizontal Display Width Register
  WRITE_COMMAND(REG16_HDISP);
  WRITE_DATA(GuiConst_DISPLAY_WIDTH_HW/8);

  // Horizontal Non-Display Period Register
  WRITE_COMMAND(REG18_HNDP);
  WRITE_DATA(0x50);

  // Vertical Display Height Register 0
  WRITE_COMMAND(REG1A_VDISP0);
  WRITE_DATA(GuiConst_DISPLAY_HEIGHT_HW);

  // Vertical Display Height Register 1
  WRITE_COMMAND(REG1C_VDISP1);
  WRITE_DATA(0x00);

  // Vertical Non-Display Period Register
  WRITE_COMMAND(REG1E_VNDP);
  WRITE_DATA(0x17);

  // HS Pulse Width Register
  WRITE_COMMAND(REG20_HSW);
  WRITE_DATA(0x90);

  // HS Pulse Start Position Register 0
  WRITE_COMMAND(REG22_HPS);
  WRITE_DATA(0x10);

  // VS Pulse Width Register
  WRITE_COMMAND(REG24_VSW);
  WRITE_DATA(0x82);

  // VS Pulse Start Position Register 0
  WRITE_COMMAND(REG26_VPS);
  WRITE_DATA(0x10);

  // PCLK Polarity Register
  WRITE_COMMAND(REG28_PCLKPOL);
  WRITE_DATA(0x00);

  // Input Mode Register
  WRITE_COMMAND(REG2A_INMODE);
  WRITE_DATA(0x03);           // For S1D13742 0x83 for 18 bit output

  // Input YUV/RGB Translate Mode Register 0
  WRITE_COMMAND(REG2C_YUVTRANS0);
  WRITE_DATA(0x00);

  // YUV/RGB Translate Mode Register 1
  WRITE_COMMAND(REG2E_YUVTRANS1);
  WRITE_DATA(0x05);

  // U Data Fix Register
  WRITE_COMMAND(REG30_UDATAFIX);
  WRITE_DATA(0x00);

  // V Data Fix Register
  WRITE_COMMAND(REG32_VDATAFIX);
  WRITE_DATA(0x00);

  // Display Mode Register
  WRITE_COMMAND(REG34_DISPMODE);
  WRITE_DATA(0x08);    // for S1D13742 0x00

  // Special Effects Register
  WRITE_COMMAND(REG36_EFFECTS);
  WRITE_DATA(0x00);

  // Window X Start Position Register 0
  WRITE_COMMAND(REG38_X_START_LOW);
  WRITE_DATA(0x00);

  // Window X Start Position Register 1
  WRITE_COMMAND(REG3A_X_START_HIGH);
  WRITE_DATA(0x00);

  // Window Y Start Position Register 0
  WRITE_COMMAND(REG3C_Y_START_LOW);
  WRITE_DATA(0x00);

  // Window Y Start Position Register 1
  WRITE_COMMAND(REG3E_Y_START_HIGH);
  WRITE_DATA(0x00);

  // Window X End Position Register 0
  WRITE_COMMAND(REG40_X_END_LOW);
  WRITE_DATA((GuiConst_INT8U)GuiConst_DISPLAY_WIDTH_HW);

  // Window X End Position Register 1
  WRITE_COMMAND(REG42_X_END_HIGH);
  WRITE_DATA((GuiConst_INT8U)(GuiConst_DISPLAY_WIDTH_HW>>8));

  // Window Y End Position Register 0
  WRITE_COMMAND(REG44_Y_END_LOW);
  WRITE_DATA((GuiConst_INT8U)GuiConst_DISPLAY_HEIGHT_HW);

  // Window Y End Position Register 1
  WRITE_COMMAND(REG46_Y_END_HIGH);
  WRITE_DATA((GuiConst_INT8U)(GuiConst_DISPLAY_HEIGHT_HW>>8));

  millisecond_delay(10);

  // Clear display memory
  WRITE_COMMAND(REG48_MEMORY_LOW);
  for (X = 0; X < GuiConst_DISPLAY_BYTES; X++)   // Loop through bytes
    WRITE_DATA(0x00);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh(void)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S X;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for(Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      WRITE_COMMAND(REG3C_Y_START_LOW);
      WRITE_DATA((GuiConst_INT8U)Y);
      WRITE_COMMAND(REG3E_Y_START_HIGH);
      WRITE_DATA((GuiConst_INT8U)(Y>>8));

      // Select column
      WRITE_COMMAND(REG38_X_START_LOW);
      WRITE_DATA((GuiConst_INT8U)GuiLib_DisplayRepaint[Y].ByteBegin);
      WRITE_COMMAND(REG3A_X_START_HIGH);
      WRITE_DATA((GuiConst_INT8U)(GuiLib_DisplayRepaint[Y].ByteBegin >> 8));

      LastByte = GuiConst_DISPLAY_WIDTH_HW;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      WRITE_COMMAND(REG48_MEMORY_LOW);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        WRITE_DATA(GuiLib_DisplayBuf[Y][X * 3]);
        WRITE_DATA(GuiLib_DisplayBuf[Y][X * 3 + 1]);
        WRITE_DATA(GuiLib_DisplayBuf[Y][X * 3 + 2]);
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S1D13743

#ifdef LCD_CONTROLLER_TYPE_S1D13748

// ============================================================================
//
// S1D13748 DISPLAY CONTROLLER
//
// This driver uses one S1D13748 display controller.
// LCD controller in 16 bit 8080 type interface, direct memory access mode.
// Output is LCD1 in generic TFT 16 RGB interface 5-6-5.
// Reference input frequency 1MHz.
// Modify driver accordingly for other interface types.
// Graphic modes up to 640x480 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// Hardwired connection
// CNF0 = VSS,
// CNF1 = VSS - 8080 interface,
// CNF2 = VCC - Big Endian data storing in display buffer

#define DISP_DATA                 Pxx

#define RESET_ON                  Pxx.x = 0     // RESET pin
#define RESET_OFF                 Pxx.x = 1     // RESET pin
#define CHIP_ENABLED              Pxx.x = 0     // CS pin
#define CHIP_DISABLED             Pxx.x = 1     // CS pin
#define RW_WR_LOW                 Pxx.x = 0     // R/W(WR) pin
#define RW_WR_HIGH                Pxx.x = 1     // R/W(WR) pin
#define E_RD_LOW                  Pxx.x = 0     // E(RD) pin
#define E_RD_HIGH                 Pxx.x = 1     // E(RD) pin
#define AB1_LOW                   Pxx.x = 0     // AB1 pin
#define AB1_HIGH                  Pxx.x = 1     // AB1 pin
#define AB2_LOW                   Pxx.x = 0     // AB2 pin
#define AB2_HIGH                  Pxx.x = 1     // AB2 pin
#define AB3_LOW                   Pxx.x = 0     // AB3 pin
#define AB3_HIGH                  Pxx.x = 1     // AB3 pin

#define NOP                       asm ("nop")   // nop code
#define PLL_CLK                   33            // Output PLL frequency in MHz from 33 to 58
#define HDPS                      12            // Horizontal Display Period Start Position in pixels
#define HPW                       5             // Horizontal Pulse (FPLINE) Width in pixels
#define HPP                       5             // Horizontal Pulse (FPLINE) Start Position in pixels
#define VDPS                      12            // Vertical Display Period Start Position in lines
#define VPW                       5             // Vertical Pulse (FPFRAME) Width in lines
#define VPP                       5             // Vertical Pulse (FPFRAME) Start Position in lines

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void WRITE_COMMAND(GuiConst_INT16U address)
{
  E_RD_HIGH;

  AB1_LOW;
  AB2_LOW;
  AB3_LOW;

  CHIP_ENABLED;

  RW_WR_LOW;
  DISP_DATA = address << 1;
  // For fast microcontrollers with RW_WR_LOW period  < 2 SYSCLK add dumy nop code
  // NOP;
  RW_WR_HIGH;

  CHIP_DISABLED;
}

void WRITE_DATA(GuiConst_INT16U data)
{
  E_RD_HIGH;

  AB1_LOW;
  AB2_HIGH;
  AB3_LOW;

  CHIP_ENABLED;

  RW_WR_LOW;
  DISP_DATA = data;
  // For fast microcontrollers with RW_WR_LOW period  < 2 SYSCLK add dumy nop code
  // NOP;
  RW_WR_HIGH;

  // For fast microcontrollers with register access cycles < 6 SYSCLK add dumy nop code
  // NOP;
  // NOP;
  // NOP;
  CHIP_DISABLED;
}

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT16U X;

  // Reset controller
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;

  // System clock setting register
  WRITE_COMMAND(0x0018);
  // Clock divide ratio 0000 - 1:1, 0001 - 2:1, 0002 - 3:1, 0003 - 4:1
  WRITE_DATA(0x0000);

  // PLL Setting Register 0
  WRITE_COMMAND(0x000C);
  // Setting example for 1MHz input clock
  WRITE_DATA((PLL_CLK - 1) << 8);

  // PLL Setting Register 1
  WRITE_COMMAND(0x000E);
  WRITE_DATA(0x0002);

  // PLL Setting Register 2
  WRITE_COMMAND(0x0010);
  WRITE_DATA(0x0810);

  // PLL Setting Register 3
  WRITE_COMMAND(0x0012);
  // enable PLL - 0x0001, disable PLL - 0x0000
  WRITE_DATA(0x0001);

  // wait for stable PLL
  millisecond_delay(1);

  // Miscellaneous Configuration Register
  WRITE_COMMAND(0x0014);
  // Disable power save mode 0.bit = 0
  WRITE_DATA(0x04D0);

  // LCD Interface Configuration Register
  WRITE_COMMAND(0x0032);
  // bits 1-0: 00 - RGB interface, 10 - parallel interface
  // bits 6-4: 000 - 9 bits, 001 - 12 bits, 010 - 16 bits, 011 - 18 bits, 100 - 24 bits width
  // bit 7: 0 - signals change at the rising edge of FPSHIFT, 1 - at the falling edge of FPSHIFT
  // bit 9: 0 - FPDRDY signal is not reversed, 1 - FPDRDY signal is reversed
  WRITE_DATA(0x0020);

  // LCD1 Horizontal Total Register
  WRITE_COMMAND(0x0040);
  WRITE_DATA(( GuiConst_DISPLAY_WIDTH_HW + HDPS*2 )/ 8 - 1);

  // LCD1 Horizontal Display Period Register
  WRITE_COMMAND(0x0042);
  WRITE_DATA(GuiConst_DISPLAY_WIDTH_HW / 2 - 1);

  // LCD1 Horizontal Display Period Start Position Register
  WRITE_COMMAND(0x0044);
  WRITE_DATA(HDPS-9);

  // LCD1 Horizontal Pulse Register
  WRITE_COMMAND(0x0046);
  // 7. bit: 0 - horizontal sync signal FPLINE is active low, 1 - active high
  WRITE_DATA(0x0000 | (HPW-1));

  // LCD1 Horizontal Pulse Start Position Register
  WRITE_COMMAND(0x0048);
  WRITE_DATA(HPP-1);

  // LCD1 Vertical Total Register
  WRITE_COMMAND(0x004A);
  WRITE_DATA(GuiConst_BYTE_LINES + VDPS*2  - 1);

  // LCD1 Vertical Display Period Register
  WRITE_COMMAND(0x004C);
  WRITE_DATA(GuiConst_BYTE_LINES  - 1);

  // LCD1 Vertical Display Period Start Position Register
  WRITE_COMMAND(0x004E);
  WRITE_DATA(VDPS);

  // LCD1 Vertical Pulse Register
  WRITE_COMMAND(0x0050);
  // 7. bit: 0 - horizontal sync signal FPFRAME is active low, 1 - active high
  WRITE_DATA(0x0000 | (VPW-1));

  // LCD1 Vertical Pulse Start Position Register
  WRITE_COMMAND(0x0052);
  WRITE_DATA(VPP-1);

  // LCD1 Vsync Output Register
  WRITE_COMMAND(0x0068);
  WRITE_DATA(0x0000);

  // Display Mode Setting Register 1
  WRITE_COMMAND(0x0202);
  // 0.bit: 0 - main window is disabled, 1 - enabled
  // 10..bit: 0 - all off, 1 - LCD1 output enabled
  WRITE_DATA(0x0400);

  // Background Color Setting Register
  WRITE_COMMAND(0x0206);
  // RGB color 5-6-5
  WRITE_DATA(0x0000);

  // Main1 Window X Start Position Register
  WRITE_COMMAND(0x0218);
  WRITE_DATA(0x00);

  // Main1 Window Y Start Position Register
  WRITE_COMMAND(0x021A);
  WRITE_DATA(0x0000);

  // Main1 Window Image Horizontal Size Register
  WRITE_COMMAND(0x0246);
  WRITE_DATA(GuiConst_DISPLAY_WIDTH_HW - 1);

  // Main1 Window Image Vertical Size Register
  WRITE_COMMAND(0x0248);
  WRITE_DATA(GuiConst_BYTE_LINES - 1);

  millisecond_delay(10);

  // Clear display memory
  // Memory start address 1
  WRITE_COMMAND(0x184);
  WRITE_DATA(0x00);
  // Memory start address 0
  WRITE_COMMAND(0x182);
  WRITE_DATA(0x00);
  // Memory data port address
  WRITE_COMMAND(0x18C);
  for (X = 0; X < GuiConst_DISPLAY_BYTES; X++)   // Loop through bytes
    WRITE_DATA(0x00);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh(void)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S X;
  GuiConst_INT16S LastByte;
  GuiConst_INT32U Address;

  GuiDisplay_Lock ();

  // Walk through all lines
  for(Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Calculate start address
      Address = (Y * GuiConst_BYTES_PR_LINE / 2 +
                 GuiLib_DisplayRepaint[Y].ByteBegin) * 2;

      // Memory start address 1
      WRITE_COMMAND(0x184);
      WRITE_DATA((GuiConst_INT16U)(Address>>15));
      // Memory start address 0
      WRITE_COMMAND(0x182);
      WRITE_DATA((GuiConst_INT16U)(Address<<1));

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Memory data port address
      WRITE_COMMAND(0x18C);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        WRITE_DATA(GuiLib_DisplayBuf.Words[Y][X]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S1D13748

#ifdef LCD_CONTROLLER_TYPE_S1D13781

// ============================================================================
//
// S1D13781 DISPLAY CONTROLLER
//
// This driver uses one S1D13781 display controller with built-in pll module.
// LCD controller is in indirect 16 bit access mode 1 or in SPI 16 bit mode.
// Output is LCD in generic TFT 16 RGB interface 5-6-5.
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// Hardwired connection
// RESET tied to RESET of microcontroller

// For 16bit indirect mode 1
// CNF2 CNF1 CNF0
//  0    1    1
// AB[18-2] = 0

// For SPI mode
// CNF2 CNF1 CNF0
//  1    1    1
// AB[18-1] = 0
// DB[15-2] = 0
// RD# = 1, UB# = 1, LB# = 1

// Uncomment for dsPIC33
#include "p33Fxxxx.h"

// Uncomment for SPI interface
#define SPI_INTERFACE

#ifdef SPI_INTERFACE

#define CHIP_ENABLED              PORTE &= 0x01 // CS# pin (bit 0 of PORTE, example for dsPIC33)
#define CHIP_DISABLED             PORTE |= 0x01 // CS# pin

#else

#define DISP_DATA                 PORTB         // DB[15-0] pins (bits 15-0 of port B, example for dsPIC33)
#define CHIP_ENABLED              PORTE &= 0x01 // CS# pin (bit 0 of PORTE, example for dsPIC33)
#define CHIP_DISABLED             PORTE |= 0x01 // CS# pin
#define WR_LOW                    PORTE &= 0x02 // WR# pin (bit 1 of PORTE, example for dsPIC33)
#define WR_HIGH                   PORTE |= 0x02 // WR# pin
#define RD_LOW                    PORTE &= 0x03 // RD# pin (bit 2 of PORTE, example for dsPIC33)
#define RD_HIGH                   PORTE |= 0x03 // RD# pin
#define MODE_COMMAND              PORTE &= 0x04 // AB1 pin (bit 3 of PORTE, example for dsPIC33)
#define MODE_DATA                 PORTE |= 0x04 // AB1 pin

#endif

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

#ifdef SPI_INTERFACE

// Initialization of ports (example for port E of dsPIC33)
void initPorts()
{
  // Set CS as output
  TRISE &= 0xFE;
  CHIP_DISABLED;

  // setup the SPI1 port
  PORTF = 0x00;
  TRISF = 0x7F;

  // set the RB0 pin to Digital Mode (all others to analog)
  AD1PCFGLbits.PCFG2 = 1;
  // set PORTF to output except for SDI1
  TRISF = 0x0080;

  // Setup the SPI peripheral
  // Disable the SPI module
  SPI1STAT = 0x0000;
  SPI1CON2 = 0x0000;
  // bit 12 DISSCK: 0 = Internal SPI clock is enabled
  // bit 11 DISSDO: 0 = SDOx pin is controlled by the module
  // bit 10 MODE16: 1 = (16 bits), 0 = (8 bits)
  // bit 9 SMP: SPIx Data Input Sample Phase bit(1) Master mode:
  //  1 = Input data sampled at end of data output time
  //  0 = Input data sampled at middle of data output time
  // bit 8 CKE: SPIx Clock Edge Select bit(2)
  //  1 = Serial output data changes on transition from active clock state to Idle clock state
  //  0 = Serial output data changes on transition from Idle clock state to active clock state
  // bit 7 SSEN: Slave Select Enable, 0 = SSx pin is not used by the module
  // bit 6 CKP: Clock Polarity Select bit
  //  1 = Idle state for clock is a high level; active state is a low level
  //  0 = Idle state for clock is a low level; active state is a high level
  // bit 5 MSTEN: Master Mode Enable bit, 1 = Master mode, 0 = Slave mode
  // bit 4-2 SPRE<2:0>: Secondary Prescale bits
  //  111 = Secondary prescale 1:1
  //  110 = Secondary prescale 2:1
  //  �
  //  000 = Secondary prescale 8:1
  // bit 1-0 PPRE<1:0>: Primary Prescale bits
  //  11 = Primary prescale 1:1
  //  10 = Primary prescale 4:1
  //  01 = Primary prescale 16:1
  SPI1CON1 = 0x056F;
  SPI1CON1bits.CKE = 0x01;
  SPI1CON1bits.CKP = 0x00;
  // Enable the SPI module
  SPI1STAT = 0x8000;
}

void WRITE_SPI(GuiConst_INT32U address, GuiConst_INT16U data)
{
  GuiConst_INT16U temp;

  CHIP_ENABLED;

  // dummy read of the SPI1BUF register to clear the SPIRBF flag
  temp = SPI1BUF;
  // write command + ADR19-ADR16 to the SPI peripheral
  SPI1BUF = 0x8800 | (GuiConst_INT16U)(address>>16);
  // wait for the data to be sent out
  while (!SPI1STATbits.SPIRBF);
  // write ADR15-ADR0 to the SPI peripheral
  SPI1BUF = (GuiConst_INT16U)address;
  // wait for the data to be sent out
  while (!SPI1STATbits.SPIRBF);
  // write data to the SPI peripheral
  SPI1BUF = data;
  // wait for the data to be sent out
  while (!SPI1STATbits.SPIRBF);

  CHIP_DISABLED;
}

void WRITE_SPI_COMMAND(GuiConst_INT32U address)
{
  GuiConst_INT16U temp;

  // dummy read of the SPI1BUF register to clear the SPIRBF flag
  temp = SPI1BUF;
  // write command + ADR19-ADR16 to the SPI peripheral
  SPI1BUF = 0x8800 | (GuiConst_INT16U)(address>>16);
  // wait for the data to be sent out
  while (!SPI1STATbits.SPIRBF);
  // write ADR15-ADR0 to the SPI peripheral
  SPI1BUF = (GuiConst_INT16U)address;
  // wait for the data to be sent out
  while (!SPI1STATbits.SPIRBF);
}

void WRITE_SPI_DATA(GuiConst_INT16U data)
{
  SPI1BUF = data;
  // wait for the data to be sent out
  while (!SPI1STATbits.SPIRBF);
}

#else

// Initialization of ports (example for ports B and E of dsPIC33)
void initPorts()
{
  // Set as outputs
  TRISB  = 0x0000;
  TRISE &= 0xF0;
  // Set high all control pins
  PORTE |= 0x0F;
}

void WRITE_COMMAND(GuiConst_INT32U address)
{
  RD_HIGH;
  MODE_COMMAND;
  CHIP_ENABLED;
  DISP_DATA = (GuiConst_INT16U)address;
  WR_LOW;
  WR_HIGH;
  DISP_DATA = (GuiConst_INT16U)(address>>16);
  WR_LOW;
  WR_HIGH;
  CHIP_DISABLED;
}

void WRITE_DATA(GuiConst_INT16U data)
{
  RD_HIGH;
  MODE_DATA;
  CHIP_ENABLED;
  DISP_DATA = data;
  WR_LOW;
  WR_HIGH;
  CHIP_DISABLED;
}

#endif

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT32U X;

  initPorts();

#ifdef SPI_INTERFACE

  // Reset controller
  WRITE_SPI(0x60806,0x0100);

  millisecond_delay(1);

  // Power Save Register
  WRITE_SPI(0x60804,0x0000);

  // PLL Setting Register 0
  WRITE_SPI(0x60810,0x0000);

  // PLL Setting Register 1
  WRITE_SPI(0x60812,0x0017);

  // PLL Setting Register 2
  WRITE_SPI(0x60814,0x0035);

  // PLL Setting Register 0
  WRITE_SPI(0x60810,0x0001);

  millisecond_delay(10);

  // Internal Clock Configuration Register
  WRITE_SPI(0x60816,0x0007);

  // Power Save Register
  WRITE_SPI(0x60804,0x0002);

  // Panel Setting Register
  WRITE_SPI(0x60820,0x004B);    // TFT - 16bit

  // Display Setting Register
  WRITE_SPI(0x60822,0x0001);  // Panel interface enable

  // Horizontal Display Width Register
  WRITE_SPI(0x60824,GuiConst_DISPLAY_WIDTH_HW/8);

  // Horizontal Non-Display Period Register
  WRITE_SPI(0x60826,0x0050);

  // Vertical Display Height Register
  WRITE_SPI(0x60828,GuiConst_BYTE_LINES);

  // Vertical Non-Display Period Register
  WRITE_SPI(0x6082A,0x0017);

  // HS Pulse Width Register
  WRITE_SPI(0x6082C,0x0090);

  // HS Pulse Start Position Register
  WRITE_SPI(0x6082E,0x0010);

  // VS Pulse Width Register
  WRITE_SPI(0x60830,0x0082);

  // VS Pulse Start Position Register
  WRITE_SPI(0x60832,0x0011);

  // Main Layer Setting Register
  WRITE_SPI(0x60840,0x0001);  // RGB 565 format without LUT

  // Clear display buffer
  CHIP_ENABLED;
  WRITE_SPI_COMMAND(0x00000);
  for (X = 0; X < GuiConst_DISPLAY_BYTES/2; X++)   // Loop through bytes
    WRITE_SPI_DATA(0x0000);
  CHIP_DISABLED;

#else

  // Reset controller
  WRITE_COMMAND(0x60806);
  WRITE_DATA(0x0100);

  millisecond_delay(1);

  // Power Save Register
  WRITE_COMMAND(0x60804);
  WRITE_DATA(0x0000);

  // PLL Setting Register 0
  WRITE_COMMAND(0x60810);
  WRITE_DATA(0x0000);

  // PLL Setting Register 1
  WRITE_COMMAND(0x60812);
  WRITE_DATA(0x0017);

  // PLL Setting Register 2
  WRITE_COMMAND(0x60814);
  WRITE_DATA(0x0035);

  // PLL Setting Register 0
  WRITE_COMMAND(0x60810);
  WRITE_DATA(0x0001);

  millisecond_delay(10);

  // Internal Clock Configuration Register
  WRITE_COMMAND(0x60816);
  WRITE_DATA(0x0007);

  // Power Save Register
  WRITE_COMMAND(0x60804);
  WRITE_DATA(0x0002);

  // Panel Setting Register
  WRITE_COMMAND(0x60820);
  WRITE_DATA(0x004B);    // TFT - 16bit

  // Display Setting Register
  WRITE_COMMAND(0x60822);
  WRITE_DATA(0x0001);  // Panel interface enable

  // Horizontal Display Width Register
  WRITE_COMMAND(0x60824);
  WRITE_DATA(GuiConst_DISPLAY_WIDTH_HW/8);

  // Horizontal Non-Display Period Register
  WRITE_COMMAND(0x60826);
  WRITE_DATA(0x0050);

  // Vertical Display Height Register
  WRITE_COMMAND(0x60828);
  WRITE_DATA(GuiConst_BYTE_LINES);

  // Vertical Non-Display Period Register
  WRITE_COMMAND(0x6082A);
  WRITE_DATA(0x0017);

  // HS Pulse Width Register
  WRITE_COMMAND(0x6082C);
  WRITE_DATA(0x0090);

  // HS Pulse Start Position Register
  WRITE_COMMAND(0x6082E);
  WRITE_DATA(0x0010);

  // VS Pulse Width Register
  WRITE_COMMAND(0x60830);
  WRITE_DATA(0x0082);

  // VS Pulse Start Position Register
  WRITE_COMMAND(0x60832);
  WRITE_DATA(0x0011);

  // Main Layer Setting Register
  WRITE_COMMAND(0x60840);
  WRITE_DATA(0x0001);  // RGB 565 format without LUT

  // Clear display buffer
  WRITE_COMMAND(0x00000);
  for (X = 0; X < GuiConst_DISPLAY_BYTES/2; X++)   // Loop through bytes
    WRITE_DATA(0x0000);

#endif
}

// Refreshes display buffer to display
void GuiDisplay_Refresh(void)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S X;
  GuiConst_INT16S LastByte;
  GuiConst_INT32U Address;

  GuiDisplay_Lock ();

  // Walk through all lines
  for(Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      Address = (Y * GuiConst_BYTES_PR_LINE/2 +
                 GuiLib_DisplayRepaint[Y].ByteBegin) * 2;

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

     // Memory data address
#ifdef SPI_INTERFACE
      CHIP_ENABLED;
      WRITE_SPI_COMMAND(Address);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        WRITE_SPI_DATA(GuiConst_INT16U data);
      CHIP_DISABLED;
#else
      WRITE_COMMAND(Address);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        WRITE_DATA(GuiLib_DisplayBuf.Words[Y][X]);
#endif
      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S1D13781

#ifdef LCD_CONTROLLER_TYPE_S1D13A04

// ============================================================================
//
// S1D13A04 DISPLAY CONTROLLER
//
// This driver uses one S1D13A04 display controller.
// LCD display memory mapped interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB or Palette
//   Color depth: 8 or 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontal: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// Use 13A04CFG PC-software generator for creation of register's values
#define S1D_DISPLAY_WIDTH               320
#define S1D_DISPLAY_HEIGHT              240
#define S1D_DISPLAY_BPP                 16
#define S1D_DISPLAY_SCANLINE_BYTES      640
#define S1D_DISPLAY_FRAME_RATE          70
#define S1D_DISPLAY_PCLK                6250000L
#define S1D_PHYSICAL_REG_ADDR           0x62000014L
#define S1D_PHYSICAL_VMEM_ADDR          0x62080000L
#define S1D_PHYSICAL_REG_SIZE           36864L
#define S1D_PHYSICAL_VMEM_SIZE          114688L
#define S1D_PHYSICAL_VMEM_REQUIRED      76800L
#define S1D_PALETTE_SIZE                256
#define S1D_FRAME_RATE                  70
#define S1D_POWER_DELAY_OFF             1200
#define S1D_POWER_DELAY_ON              50
#define S1D_HWBLT
#define S1D_SWBLT
#define S1D_REGRESERVED                 0xF0
#define S1D_REGDELAYOFF                 0xFD
#define S1D_REGDELAYON                  0xFE

#define S1D_WRITE_PALETTE(p,i,r,g,b)  \
  (((volatile S1D_VALUE*)(p))[0x18/sizeof(S1D_VALUE)] = (S1D_VALUE)(i)<<24 | (S1D_VALUE)(r)<<16 | (S1D_VALUE)(g)<<8 | (b))

#define S1D_READ_PALETTE(p,i,r,g,b)   \
  {  \
    S1D_VALUE reg;  \
    ((volatile S1D_VALUE*)(p))[0x1C/sizeof(S1D_VALUE)] = (S1D_VALUE)(i)<<24;  \
    reg = ((volatile S1D_VALUE*)(p))[0x1C/sizeof(S1D_VALUE)];  \
    (r) = (reg>>16)&0xFF;  \
    (g) = (reg>>8)&0xFF;  \
    (b) = reg&0xFF;  \
  }

typedef unsigned char S1D_INDEX;
typedef unsigned long S1D_VALUE;
typedef struct
{
    S1D_INDEX Index;
    S1D_VALUE Value;
} S1D_REGS;

S1D_REGS aS1DRegs[] =
{
    { 0x14,             0x00000000 },       // Power Save Configuration Register
    { 0x64,             0x00010000 },       // GPIO Status and Control Register
    { 0x04,             0x00000000 },       // Memory Clock Configuration Register
    { 0x08,             0x00000043 },       // Pixel Clock Configuration Register
    { 0x0C,             0x000000D0 },       // Panel Type and MOD Rate Register
    { 0x10,             0x00000008 },       // Display Settings Register
    { 0x20,             0x0000002B },       // Horizontal Total Register
    { 0x24,             0x00000027 },       // Horizontal Display Period Register
    { 0x28,             0x00000000 },       // Horizontal Display Period Start Position Register
    { 0x2C,             0x00870000 },       // FPLINE Register
    { 0x30,             0x000000FA },       // Vertical Total Register
    { 0x34,             0x000000EF },       // Vertical Display Period Register
    { 0x38,             0x00000000 },       // Vertical Display Period Start Position Register
    { 0x3C,             0x00800001 },       // FPFRAME Register
    { 0x40,             0x00000000 },       // Main Window Display Start Address Register
    { 0x44,             0x00000050 },       // Main Window Line Address Offset Register
    { 0x50,             0x00000000 },       // PIP+ Window Display Start Address Register
    { 0x54,             0x00000050 },       // PIP+ Window Line Address Offset Register
    { 0x58,             0x00000000 },       // PIP+ Window X Positions Register
    { 0x5C,             0x00000000 },       // PIP+ Window Y Positions Register
    { 0x60,             0x00000000 },       // Special Purpose Register
    { 0x70,             0x00000000 },       // PWM Clock Configuration Register
    { 0x74,             0x00000000 },       // PWMOUT Duty Cycle Register
    { 0x80,             0x00000000 },       // Scratch Pad A Register
    { 0x84,             0x00000000 },       // Scratch Pad B Register
    { 0x88,             0x00000000 },       // Scratch Pad C Register
    { S1D_REGDELAYON,   0x00000032 },       // LCD Panel Power On Delay (in ms)
    { 0x64,             0x00010001 },       // GPIO Status and Control Register
    { 0x14,             0x00000000 }        // Power Save Configuration Register
};
// Replace code above with your settings generated with 13A04CFG PC-software generator
//********************************************************************************************

typedef struct tagLutEntry
{
 GuiConst_INT32U Index;
 GuiConst_INT32U Red;
 GuiConst_INT32U Green;
 GuiConst_INT32U Blue;
} LutEntry;

LutEntry Lut1BppColor[2] = {{0x00, 0x00, 0x00, 0x00},
                            {0x01, 0xFF, 0xFF, 0xFF}};

LutEntry Lut2BppColor[4] = {{0x00, 0x00, 0x00, 0x00},
                            {0x01, 0x00, 0x00, 0xFF},
                            {0x02, 0xFF, 0x00, 0x00},
                            {0x03, 0xFF, 0xFF, 0xFF}};

LutEntry Lut4BppColor[16] = {{0x00, 0x00, 0x00, 0x00},
                             {0x01, 0x00, 0x00, 0xAA},
                             {0x02, 0x00, 0xAA, 0x00},
                             {0x03, 0x00, 0xAA, 0xAA},
                             {0x04, 0xAA, 0x00, 0x00},
                             {0x05, 0xAA, 0x00, 0xAA},
                             {0x06, 0xAA, 0xAA, 0x00},
                             {0x07, 0xAA, 0xAA, 0xAA},
                             {0x08, 0x00, 0x00, 0x00},
                             {0x09, 0x00, 0x00, 0xFF},
                             {0x0A, 0x00, 0xFF, 0x00},
                             {0x0B, 0x00, 0xFF, 0xFF},
                             {0x0C, 0xFF, 0x00, 0x00},
                             {0x0D, 0xFF, 0x00, 0xFF},
                             {0x0E, 0xFF, 0xFF, 0x00},
                             {0x0F, 0xFF, 0xFF, 0xFF}};

LutEntry Lut8BppColor[256] = {{0x00, 0x00, 0x00, 0x00},
                              {0x01, 0x00, 0x00, 0xAA},
                              {0x02, 0x00, 0xAA, 0x00},
                              {0x03, 0x00, 0xAA, 0xAA},
                              {0x04, 0xAA, 0x00, 0x00},
                              {0x05, 0xAA, 0x00, 0xAA},
                              {0x06, 0xAA, 0xAA, 0x00},
                              {0x07, 0xAA, 0xAA, 0xAA},
                              {0x08, 0x55, 0x55, 0x55},
                              {0x09, 0x00, 0x00, 0xFF},
                              {0x0A, 0x00, 0xFF, 0x00},
                              {0x0B, 0x00, 0xFF, 0xFF},
                              {0x0C, 0xFF, 0x00, 0x00},
                              {0x0D, 0xFF, 0x00, 0xFF},
                              {0x0E, 0xFF, 0xFF, 0x00},
                              {0x0F, 0xFF, 0xFF, 0xFF},
                              {0x10, 0x00, 0x00, 0x00},
                              {0x11, 0x11, 0x11, 0x11},
                              {0x12, 0x22, 0x22, 0x22},
                              {0x13, 0x33, 0x33, 0x33},
                              {0x14, 0x44, 0x44, 0x44},
                              {0x15, 0x55, 0x55, 0x55},
                              {0x16, 0x66, 0x66, 0x66},
                              {0x17, 0x77, 0x77, 0x77},
                              {0x18, 0x89, 0x89, 0x89},
                              {0x19, 0x9A, 0x9A, 0x9A},
                              {0x1A, 0xAB, 0xAB, 0xAB},
                              {0x1B, 0xBC, 0xBC, 0xBC},
                              {0x1C, 0xCD, 0xCD, 0xCD},
                              {0x1D, 0xDE, 0xDE, 0xDE},
                              {0x1E, 0xEF, 0xEF, 0xEF},
                              {0x1F, 0xFF, 0xFF, 0xFF},
                              {0x20, 0x00, 0x00, 0x00},
                              {0x21, 0x11, 0x00, 0x00},
                              {0x22, 0x22, 0x00, 0x00},
                              {0x23, 0x33, 0x00, 0x00},
                              {0x24, 0x44, 0x00, 0x00},
                              {0x25, 0x55, 0x00, 0x00},
                              {0x26, 0x66, 0x00, 0x00},
                              {0x27, 0x77, 0x00, 0x00},
                              {0x28, 0x89, 0x00, 0x00},
                              {0x29, 0x9A, 0x00, 0x00},
                              {0x2A, 0xAB, 0x00, 0x00},
                              {0x2B, 0xBC, 0x00, 0x00},
                              {0x2C, 0xCD, 0x00, 0x00},
                              {0x2D, 0xDE, 0x00, 0x00},
                              {0x2E, 0xEF, 0x00, 0x00},
                              {0x2F, 0xFF, 0x00, 0x00},
                              {0x30, 0x00, 0x00, 0x00},
                              {0x31, 0x00, 0x11, 0x00},
                              {0x32, 0x00, 0x22, 0x00},
                              {0x33, 0x00, 0x33, 0x00},
                              {0x34, 0x00, 0x44, 0x00},
                              {0x35, 0x00, 0x55, 0x00},
                              {0x36, 0x00, 0x66, 0x00},
                              {0x37, 0x00, 0x77, 0x00},
                              {0x38, 0x00, 0x89, 0x00},
                              {0x39, 0x00, 0x9A, 0x00},
                              {0x3A, 0x00, 0xAB, 0x00},
                              {0x3B, 0x00, 0xBC, 0x00},
                              {0x3C, 0x00, 0xCD, 0x00},
                              {0x3D, 0x00, 0xDE, 0x00},
                              {0x3E, 0x00, 0xEF, 0x00},
                              {0x3F, 0x00, 0xFF, 0x00},
                              {0x40, 0x00, 0x00, 0x00},
                              {0x41, 0x00, 0x00, 0x11},
                              {0x42, 0x00, 0x00, 0x22},
                              {0x43, 0x00, 0x00, 0x33},
                              {0x44, 0x00, 0x00, 0x44},
                              {0x45, 0x00, 0x00, 0x55},
                              {0x46, 0x00, 0x00, 0x66},
                              {0x47, 0x00, 0x00, 0x77},
                              {0x48, 0x00, 0x00, 0x89},
                              {0x49, 0x00, 0x00, 0x9A},
                              {0x4A, 0x00, 0x00, 0xAB},
                              {0x4B, 0x00, 0x00, 0xBC},
                              {0x4C, 0x00, 0x00, 0xCD},
                              {0x4D, 0x00, 0x00, 0xDE},
                              {0x4E, 0x00, 0x00, 0xEF},
                              {0x4F, 0x00, 0x00, 0xFF},
                              {0x50, 0x00, 0x00, 0xFF},
                              {0x51, 0x00, 0x11, 0xFF},
                              {0x52, 0x00, 0x22, 0xFF},
                              {0x53, 0x00, 0x33, 0xFF},
                              {0x54, 0x00, 0x44, 0xFF},
                              {0x55, 0x00, 0x55, 0xFF},
                              {0x56, 0x00, 0x66, 0xFF},
                              {0x57, 0x00, 0x77, 0xFF},
                              {0x58, 0x00, 0x89, 0xFF},
                              {0x59, 0x00, 0x9A, 0xFF},
                              {0x5A, 0x00, 0xAB, 0xFF},
                              {0x5B, 0x00, 0xBC, 0xFF},
                              {0x5C, 0x00, 0xCD, 0xFF},
                              {0x5D, 0x00, 0xDE, 0xFF},
                              {0x5E, 0x00, 0xEF, 0xFF},
                              {0x5F, 0x00, 0xFF, 0xFF},
                              {0x60, 0x00, 0xFF, 0xFF},
                              {0x61, 0x00, 0xFF, 0xEF},
                              {0x62, 0x00, 0xFF, 0xDE},
                              {0x63, 0x00, 0xFF, 0xCD},
                              {0x64, 0x00, 0xFF, 0xBC},
                              {0x65, 0x00, 0xFF, 0xAB},
                              {0x66, 0x00, 0xFF, 0x9A},
                              {0x67, 0x00, 0xFF, 0x89},
                              {0x68, 0x00, 0xFF, 0x77},
                              {0x69, 0x00, 0xFF, 0x66},
                              {0x6A, 0x00, 0xFF, 0x55},
                              {0x6B, 0x00, 0xFF, 0x44},
                              {0x6C, 0x00, 0xFF, 0x33},
                              {0x6D, 0x00, 0xFF, 0x22},
                              {0x6E, 0x00, 0xFF, 0x11},
                              {0x6F, 0x00, 0xFF, 0x00},
                              {0x70, 0x00, 0xFF, 0x00},
                              {0x71, 0x11, 0xFF, 0x00},
                              {0x72, 0x22, 0xFF, 0x00},
                              {0x73, 0x33, 0xFF, 0x00},
                              {0x74, 0x44, 0xFF, 0x00},
                              {0x75, 0x55, 0xFF, 0x00},
                              {0x76, 0x66, 0xFF, 0x00},
                              {0x77, 0x77, 0xFF, 0x00},
                              {0x78, 0x89, 0xFF, 0x00},
                              {0x79, 0x9A, 0xFF, 0x00},
                              {0x7A, 0xAB, 0xFF, 0x00},
                              {0x7B, 0xBC, 0xFF, 0x00},
                              {0x7C, 0xCD, 0xFF, 0x00},
                              {0x7D, 0xDE, 0xFF, 0x00},
                              {0x7E, 0xEF, 0xFF, 0x00},
                              {0x7F, 0xFF, 0xFF, 0x00},
                              {0x80, 0xFF, 0xFF, 0x00},
                              {0x81, 0xFF, 0xEF, 0x00},
                              {0x82, 0xFF, 0xDE, 0x00},
                              {0x83, 0xFF, 0xCD, 0x00},
                              {0x84, 0xFF, 0xBC, 0x00},
                              {0x85, 0xFF, 0xAB, 0x00},
                              {0x86, 0xFF, 0x9A, 0x00},
                              {0x87, 0xFF, 0x89, 0x00},
                              {0x88, 0xFF, 0x77, 0x00},
                              {0x89, 0xFF, 0x66, 0x00},
                              {0x8A, 0xFF, 0x55, 0x00},
                              {0x8B, 0xFF, 0x44, 0x00},
                              {0x8C, 0xFF, 0x33, 0x00},
                              {0x8D, 0xFF, 0x22, 0x00},
                              {0x8E, 0xFF, 0x11, 0x00},
                              {0x8F, 0xFF, 0x00, 0x00},
                              {0x90, 0xFF, 0x00, 0x00},
                              {0x91, 0xFF, 0x00, 0x11},
                              {0x92, 0xFF, 0x00, 0x22},
                              {0x93, 0xFF, 0x00, 0x33},
                              {0x94, 0xFF, 0x00, 0x44},
                              {0x95, 0xFF, 0x00, 0x55},
                              {0x96, 0xFF, 0x00, 0x66},
                              {0x97, 0xFF, 0x00, 0x77},
                              {0x98, 0xFF, 0x00, 0x89},
                              {0x99, 0xFF, 0x00, 0x9A},
                              {0x9A, 0xFF, 0x00, 0xAB},
                              {0x9B, 0xFF, 0x00, 0xBC},
                              {0x9C, 0xFF, 0x00, 0xCD},
                              {0x9D, 0xFF, 0x00, 0xDE},
                              {0x9E, 0xFF, 0x00, 0xEF},
                              {0x9F, 0xFF, 0x00, 0xFF},
                              {0xA0, 0xFF, 0x00, 0xFF},
                              {0xA1, 0xEF, 0x00, 0xFF},
                              {0xA2, 0xDE, 0x00, 0xFF},
                              {0xA3, 0xCD, 0x00, 0xFF},
                              {0xA4, 0xBC, 0x00, 0xFF},
                              {0xA5, 0xAB, 0x00, 0xFF},
                              {0xA6, 0x9A, 0x00, 0xFF},
                              {0xA7, 0x89, 0x00, 0xFF},
                              {0xA8, 0x77, 0x00, 0xFF},
                              {0xA9, 0x66, 0x00, 0xFF},
                              {0xAA, 0x55, 0x00, 0xFF},
                              {0xAB, 0x44, 0x00, 0xFF},
                              {0xAC, 0x33, 0x00, 0xFF},
                              {0xAD, 0x22, 0x00, 0xFF},
                              {0xAE, 0x11, 0x00, 0xFF},
                              {0xAF, 0x00, 0x00, 0xFF},
                              {0xB0, 0x00, 0x00, 0x00},
                              {0xB1, 0x11, 0x00, 0x11},
                              {0xB2, 0x22, 0x00, 0x22},
                              {0xB3, 0x33, 0x00, 0x33},
                              {0xB4, 0x44, 0x00, 0x44},
                              {0xB5, 0x55, 0x00, 0x55},
                              {0xB6, 0x66, 0x00, 0x66},
                              {0xB7, 0x77, 0x00, 0x77},
                              {0xB8, 0x89, 0x00, 0x89},
                              {0xB9, 0x9A, 0x00, 0x9A},
                              {0xBA, 0xAB, 0x00, 0xAB},
                              {0xBB, 0xBC, 0x00, 0xBC},
                              {0xBC, 0xCD, 0x00, 0xCD},
                              {0xBD, 0xDE, 0x00, 0xDE},
                              {0xBE, 0xEF, 0x00, 0xEF},
                              {0xBF, 0xFF, 0x00, 0xFF},
                              {0xC0, 0x00, 0x00, 0x00},
                              {0xC1, 0x00, 0x11, 0x11},
                              {0xC2, 0x00, 0x22, 0x22},
                              {0xC3, 0x00, 0x33, 0x33},
                              {0xC4, 0x00, 0x44, 0x44},
                              {0xC5, 0x00, 0x55, 0x55},
                              {0xC6, 0x00, 0x66, 0x66},
                              {0xC7, 0x00, 0x77, 0x77},
                              {0xC8, 0x00, 0x89, 0x89},
                              {0xC9, 0x00, 0x9A, 0x9A},
                              {0xCA, 0x00, 0xAB, 0xAB},
                              {0xCB, 0x00, 0xBC, 0xBC},
                              {0xCC, 0x00, 0xCD, 0xCD},
                              {0xCD, 0x00, 0xDE, 0xDE},
                              {0xCE, 0x00, 0xEF, 0xEF},
                              {0xCF, 0x00, 0xFF, 0xFF},
                              {0xD0, 0xFF, 0x00, 0x00},
                              {0xD1, 0xFF, 0x11, 0x11},
                              {0xD2, 0xFF, 0x22, 0x22},
                              {0xD3, 0xFF, 0x33, 0x33},
                              {0xD4, 0xFF, 0x44, 0x44},
                              {0xD5, 0xFF, 0x55, 0x55},
                              {0xD6, 0xFF, 0x66, 0x66},
                              {0xD7, 0xFF, 0x77, 0x77},
                              {0xD8, 0xFF, 0x89, 0x89},
                              {0xD9, 0xFF, 0x9A, 0x9A},
                              {0xDA, 0xFF, 0xAB, 0xAB},
                              {0xDB, 0xFF, 0xBC, 0xBC},
                              {0xDC, 0xFF, 0xCD, 0xCD},
                              {0xDD, 0xFF, 0xDE, 0xDE},
                              {0xDE, 0xFF, 0xEF, 0xEF},
                              {0xDF, 0xFF, 0xFF, 0xFF},
                              {0xE0, 0x00, 0xFF, 0x00},
                              {0xE1, 0x11, 0xFF, 0x11},
                              {0xE2, 0x22, 0xFF, 0x22},
                              {0xE3, 0x33, 0xFF, 0x33},
                              {0xE4, 0x44, 0xFF, 0x44},
                              {0xE5, 0x55, 0xFF, 0x55},
                              {0xE6, 0x66, 0xFF, 0x66},
                              {0xE7, 0x77, 0xFF, 0x77},
                              {0xE8, 0x89, 0xFF, 0x89},
                              {0xE9, 0x9A, 0xFF, 0x9A},
                              {0xEA, 0xAB, 0xFF, 0xAB},
                              {0xEB, 0xBC, 0xFF, 0xBC},
                              {0xEC, 0xCD, 0xFF, 0xCD},
                              {0xED, 0xDE, 0xFF, 0xDE},
                              {0xEE, 0xEF, 0xFF, 0xEF},
                              {0xEF, 0xFF, 0xFF, 0xFF},
                              {0xF0, 0x00, 0x00, 0xFF},
                              {0xF1, 0x11, 0x11, 0xFF},
                              {0xF2, 0x22, 0x22, 0xFF},
                              {0xF3, 0x33, 0x33, 0xFF},
                              {0xF4, 0x44, 0x44, 0xFF},
                              {0xF5, 0x55, 0x55, 0xFF},
                              {0xF6, 0x66, 0x66, 0xFF},
                              {0xF7, 0x77, 0x77, 0xFF},
                              {0xF8, 0x89, 0x89, 0xFF},
                              {0xF9, 0x9A, 0x9A, 0xFF},
                              {0xFA, 0xAB, 0xAB, 0xFF},
                              {0xFB, 0xBC, 0xBC, 0xFF},
                              {0xFC, 0xCD, 0xCD, 0xFF},
                              {0xFD, 0xDE, 0xDE, 0xFF},
                              {0xFE, 0xEF, 0xEF, 0xFF},
                              {0xFF, 0xFF, 0xFF, 0xFF}};

LutEntry Lut1BppMono[2]   ={{0x00, 0x00, 0x00, 0x00},
                            {0x01, 0xFF, 0xFF, 0xFF}};

LutEntry Lut2BppMono[4]   ={{0x00, 0x00, 0x00, 0x00},
                            {0x01, 0x55, 0x55, 0x55},
                            {0x02, 0xAA, 0xAA, 0xAA},
                            {0x03, 0xFF, 0xFF, 0xFF}};

LutEntry Lut4BppMono[16]  ={{0x00, 0x00, 0x00, 0x00},
                            {0x01, 0x11, 0x11, 0x11},
                            {0x02, 0x22, 0x22, 0x22},
                            {0x03, 0x33, 0x33, 0x33},
                            {0x04, 0x44, 0x44, 0x44},
                            {0x05, 0x55, 0x55, 0x55},
                            {0x06, 0x66, 0x66, 0x66},
                            {0x07, 0x77, 0x77, 0x77},
                            {0x08, 0x88, 0x88, 0x88},
                            {0x09, 0x99, 0x99, 0x99},
                            {0x0A, 0xAA, 0xAA, 0xAA},
                            {0x0B, 0xBB, 0xBB, 0xBB},
                            {0x0C, 0xCC, 0xCC, 0xCC},
                            {0x0D, 0xDD, 0xDD, 0xDD},
                            {0x0E, 0xEE, 0xEE, 0xEE},
                            {0x0F, 0xFF, 0xFF, 0xFF}};

LutEntry Lut8BppMono[256] ={{0x00, 0x00, 0x00, 0x00},
                            {0x01, 0x04, 0x04, 0x04},
                            {0x02, 0x08, 0x08, 0x08},
                            {0x03, 0x0C, 0x0C, 0x0C},
                            {0x04, 0x10, 0x10, 0x10},
                            {0x05, 0x14, 0x14, 0x14},
                            {0x06, 0x18, 0x18, 0x18},
                            {0x07, 0x1C, 0x1C, 0x1C},
                            {0x08, 0x20, 0x20, 0x20},
                            {0x09, 0x24, 0x24, 0x24},
                            {0x0A, 0x28, 0x28, 0x28},
                            {0x0B, 0x2C, 0x2C, 0x2C},
                            {0x0C, 0x30, 0x30, 0x30},
                            {0x0D, 0x34, 0x34, 0x34},
                            {0x0E, 0x38, 0x38, 0x38},
                            {0x0F, 0x3C, 0x3C, 0x3C},
                            {0x10, 0x40, 0x40, 0x40},
                            {0x11, 0x44, 0x44, 0x44},
                            {0x12, 0x48, 0x48, 0x48},
                            {0x13, 0x4C, 0x4C, 0x4C},
                            {0x14, 0x50, 0x50, 0x50},
                            {0x15, 0x54, 0x54, 0x54},
                            {0x16, 0x58, 0x58, 0x58},
                            {0x17, 0x5C, 0x5C, 0x5C},
                            {0x18, 0x60, 0x60, 0x60},
                            {0x19, 0x64, 0x64, 0x64},
                            {0x1A, 0x68, 0x68, 0x68},
                            {0x1B, 0x6C, 0x6C, 0x6C},
                            {0x1C, 0x70, 0x70, 0x70},
                            {0x1D, 0x74, 0x74, 0x74},
                            {0x1E, 0x78, 0x78, 0x78},
                            {0x1F, 0x7C, 0x7C, 0x7C},
                            {0x20, 0x80, 0x80, 0x80},
                            {0x21, 0x84, 0x84, 0x84},
                            {0x22, 0x88, 0x88, 0x88},
                            {0x23, 0x8C, 0x8C, 0x8C},
                            {0x24, 0x90, 0x90, 0x90},
                            {0x25, 0x94, 0x94, 0x94},
                            {0x26, 0x98, 0x98, 0x98},
                            {0x27, 0x9C, 0x9C, 0x9C},
                            {0x28, 0xA0, 0xA0, 0xA0},
                            {0x29, 0xA4, 0xA4, 0xA4},
                            {0x2A, 0xA8, 0xA8, 0xA8},
                            {0x2B, 0xAC, 0xAC, 0xAC},
                            {0x2C, 0xB0, 0xB0, 0xB0},
                            {0x2D, 0xB4, 0xB4, 0xB4},
                            {0x2E, 0xB8, 0xB8, 0xB8},
                            {0x2F, 0xBC, 0xBC, 0xBC},
                            {0x30, 0xC0, 0xC0, 0xC0},
                            {0x31, 0xC4, 0xC4, 0xC4},
                            {0x32, 0xC8, 0xC8, 0xC8},
                            {0x33, 0xCC, 0xCC, 0xCC},
                            {0x34, 0xD0, 0xD0, 0xD0},
                            {0x35, 0xD4, 0xD4, 0xD4},
                            {0x36, 0xD8, 0xD8, 0xD8},
                            {0x37, 0xDC, 0xDC, 0xDC},
                            {0x38, 0xE0, 0xE0, 0xE0},
                            {0x39, 0xE4, 0xE4, 0xE4},
                            {0x3A, 0xE8, 0xE8, 0xE8},
                            {0x3B, 0xEC, 0xEC, 0xEC},
                            {0x3C, 0xF0, 0xF0, 0xF0},
                            {0x3D, 0xF4, 0xF4, 0xF4},
                            {0x3E, 0xF8, 0xF8, 0xF8},
                            {0x3F, 0xFC, 0xFC, 0xFC},
                            {0x40, 0x00, 0x00, 0x00},
                            {0x41, 0x04, 0x04, 0x04},
                            {0x42, 0x08, 0x08, 0x08},
                            {0x43, 0x0C, 0x0C, 0x0C},
                            {0x44, 0x10, 0x10, 0x10},
                            {0x45, 0x14, 0x14, 0x14},
                            {0x46, 0x18, 0x18, 0x18},
                            {0x47, 0x1C, 0x1C, 0x1C},
                            {0x48, 0x20, 0x20, 0x20},
                            {0x49, 0x24, 0x24, 0x24},
                            {0x4A, 0x28, 0x28, 0x28},
                            {0x4B, 0x2C, 0x2C, 0x2C},
                            {0x4C, 0x30, 0x30, 0x30},
                            {0x4D, 0x34, 0x34, 0x34},
                            {0x4E, 0x38, 0x38, 0x38},
                            {0x4F, 0x3C, 0x3C, 0x3C},
                            {0x50, 0x40, 0x40, 0x40},
                            {0x51, 0x44, 0x44, 0x44},
                            {0x52, 0x48, 0x48, 0x48},
                            {0x53, 0x4C, 0x4C, 0x4C},
                            {0x54, 0x50, 0x50, 0x50},
                            {0x55, 0x54, 0x54, 0x54},
                            {0x56, 0x58, 0x58, 0x58},
                            {0x57, 0x5C, 0x5C, 0x5C},
                            {0x58, 0x60, 0x60, 0x60},
                            {0x59, 0x64, 0x64, 0x64},
                            {0x5A, 0x68, 0x68, 0x68},
                            {0x5B, 0x6C, 0x6C, 0x6C},
                            {0x5C, 0x70, 0x70, 0x70},
                            {0x5D, 0x74, 0x74, 0x74},
                            {0x5E, 0x78, 0x78, 0x78},
                            {0x5F, 0x7C, 0x7C, 0x7C},
                            {0x60, 0x80, 0x80, 0x80},
                            {0x61, 0x84, 0x84, 0x84},
                            {0x62, 0x88, 0x88, 0x88},
                            {0x63, 0x8C, 0x8C, 0x8C},
                            {0x64, 0x90, 0x90, 0x90},
                            {0x65, 0x94, 0x94, 0x94},
                            {0x66, 0x98, 0x98, 0x98},
                            {0x67, 0x9C, 0x9C, 0x9C},
                            {0x68, 0xA0, 0xA0, 0xA0},
                            {0x69, 0xA4, 0xA4, 0xA4},
                            {0x6A, 0xA8, 0xA8, 0xA8},
                            {0x6B, 0xAC, 0xAC, 0xAC},
                            {0x6C, 0xB0, 0xB0, 0xB0},
                            {0x6D, 0xB4, 0xB4, 0xB4},
                            {0x6E, 0xB8, 0xB8, 0xB8},
                            {0x6F, 0xBC, 0xBC, 0xBC},
                            {0x70, 0xC0, 0xC0, 0xC0},
                            {0x71, 0xC4, 0xC4, 0xC4},
                            {0x72, 0xC8, 0xC8, 0xC8},
                            {0x73, 0xCC, 0xCC, 0xCC},
                            {0x74, 0xD0, 0xD0, 0xD0},
                            {0x75, 0xD4, 0xD4, 0xD4},
                            {0x76, 0xD8, 0xD8, 0xD8},
                            {0x77, 0xDC, 0xDC, 0xDC},
                            {0x78, 0xE0, 0xE0, 0xE0},
                            {0x79, 0xE4, 0xE4, 0xE4},
                            {0x7A, 0xE8, 0xE8, 0xE8},
                            {0x7B, 0xEC, 0xEC, 0xEC},
                            {0x7C, 0xF0, 0xF0, 0xF0},
                            {0x7D, 0xF4, 0xF4, 0xF4},
                            {0x7E, 0xF8, 0xF8, 0xF8},
                            {0x7F, 0xFC, 0xFC, 0xFC},
                            {0x80, 0x00, 0x00, 0x00},
                            {0x81, 0x04, 0x04, 0x04},
                            {0x82, 0x08, 0x08, 0x08},
                            {0x83, 0x0C, 0x0C, 0x0C},
                            {0x84, 0x10, 0x10, 0x10},
                            {0x85, 0x14, 0x14, 0x14},
                            {0x86, 0x18, 0x18, 0x18},
                            {0x87, 0x1C, 0x1C, 0x1C},
                            {0x88, 0x20, 0x20, 0x20},
                            {0x89, 0x24, 0x24, 0x24},
                            {0x8A, 0x28, 0x28, 0x28},
                            {0x8B, 0x2C, 0x2C, 0x2C},
                            {0x8C, 0x30, 0x30, 0x30},
                            {0x8D, 0x34, 0x34, 0x34},
                            {0x8E, 0x38, 0x38, 0x38},
                            {0x8F, 0x3C, 0x3C, 0x3C},
                            {0x90, 0x40, 0x40, 0x40},
                            {0x91, 0x44, 0x44, 0x44},
                            {0x92, 0x48, 0x48, 0x48},
                            {0x93, 0x4C, 0x4C, 0x4C},
                            {0x94, 0x50, 0x50, 0x50},
                            {0x95, 0x54, 0x54, 0x54},
                            {0x96, 0x58, 0x58, 0x58},
                            {0x97, 0x5C, 0x5C, 0x5C},
                            {0x98, 0x60, 0x60, 0x60},
                            {0x99, 0x64, 0x64, 0x64},
                            {0x9A, 0x68, 0x68, 0x68},
                            {0x9B, 0x6C, 0x6C, 0x6C},
                            {0x9C, 0x70, 0x70, 0x70},
                            {0x9D, 0x74, 0x74, 0x74},
                            {0x9E, 0x78, 0x78, 0x78},
                            {0x9F, 0x7C, 0x7C, 0x7C},
                            {0xA0, 0x80, 0x80, 0x80},
                            {0xA1, 0x84, 0x84, 0x84},
                            {0xA2, 0x88, 0x88, 0x88},
                            {0xA3, 0x8C, 0x8C, 0x8C},
                            {0xA4, 0x90, 0x90, 0x90},
                            {0xA5, 0x94, 0x94, 0x94},
                            {0xA6, 0x98, 0x98, 0x98},
                            {0xA7, 0x9C, 0x9C, 0x9C},
                            {0xA8, 0xA0, 0xA0, 0xA0},
                            {0xA9, 0xA4, 0xA4, 0xA4},
                            {0xAA, 0xA8, 0xA8, 0xA8},
                            {0xAB, 0xAC, 0xAC, 0xAC},
                            {0xAC, 0xB0, 0xB0, 0xB0},
                            {0xAD, 0xB4, 0xB4, 0xB4},
                            {0xAE, 0xB8, 0xB8, 0xB8},
                            {0xAF, 0xBC, 0xBC, 0xBC},
                            {0xB0, 0xC0, 0xC0, 0xC0},
                            {0xB1, 0xC4, 0xC4, 0xC4},
                            {0xB2, 0xC8, 0xC8, 0xC8},
                            {0xB3, 0xCC, 0xCC, 0xCC},
                            {0xB4, 0xD0, 0xD0, 0xD0},
                            {0xB5, 0xD4, 0xD4, 0xD4},
                            {0xB6, 0xD8, 0xD8, 0xD8},
                            {0xB7, 0xDC, 0xDC, 0xDC},
                            {0xB8, 0xE0, 0xE0, 0xE0},
                            {0xB9, 0xE4, 0xE4, 0xE4},
                            {0xBA, 0xE8, 0xE8, 0xE8},
                            {0xBB, 0xEC, 0xEC, 0xEC},
                            {0xBC, 0xF0, 0xF0, 0xF0},
                            {0xBD, 0xF4, 0xF4, 0xF4},
                            {0xBE, 0xF8, 0xF8, 0xF8},
                            {0xBF, 0xFC, 0xFC, 0xFC},
                            {0xC0, 0x00, 0x00, 0x00},
                            {0xC1, 0x04, 0x04, 0x04},
                            {0xC2, 0x08, 0x08, 0x08},
                            {0xC3, 0x0C, 0x0C, 0x0C},
                            {0xC4, 0x10, 0x10, 0x10},
                            {0xC5, 0x14, 0x14, 0x14},
                            {0xC6, 0x18, 0x18, 0x18},
                            {0xC7, 0x1C, 0x1C, 0x1C},
                            {0xC8, 0x20, 0x20, 0x20},
                            {0xC9, 0x24, 0x24, 0x24},
                            {0xCA, 0x28, 0x28, 0x28},
                            {0xCB, 0x2C, 0x2C, 0x2C},
                            {0xCC, 0x30, 0x30, 0x30},
                            {0xCD, 0x34, 0x34, 0x34},
                            {0xCE, 0x38, 0x38, 0x38},
                            {0xCF, 0x3C, 0x3C, 0x3C},
                            {0xD0, 0x40, 0x40, 0x40},
                            {0xD1, 0x44, 0x44, 0x44},
                            {0xD2, 0x48, 0x48, 0x48},
                            {0xD3, 0x4C, 0x4C, 0x4C},
                            {0xD4, 0x50, 0x50, 0x50},
                            {0xD5, 0x54, 0x54, 0x54},
                            {0xD6, 0x58, 0x58, 0x58},
                            {0xD7, 0x5C, 0x5C, 0x5C},
                            {0xD8, 0x60, 0x60, 0x60},
                            {0xD9, 0x64, 0x64, 0x64},
                            {0xDA, 0x68, 0x68, 0x68},
                            {0xDB, 0x6C, 0x6C, 0x6C},
                            {0xDC, 0x70, 0x70, 0x70},
                            {0xDD, 0x74, 0x74, 0x74},
                            {0xDE, 0x78, 0x78, 0x78},
                            {0xDF, 0x7C, 0x7C, 0x7C},
                            {0xE0, 0x80, 0x80, 0x80},
                            {0xE1, 0x84, 0x84, 0x84},
                            {0xE2, 0x88, 0x88, 0x88},
                            {0xE3, 0x8C, 0x8C, 0x8C},
                            {0xE4, 0x90, 0x90, 0x90},
                            {0xE5, 0x94, 0x94, 0x94},
                            {0xE6, 0x98, 0x98, 0x98},
                            {0xE7, 0x9C, 0x9C, 0x9C},
                            {0xE8, 0xA0, 0xA0, 0xA0},
                            {0xE9, 0xA4, 0xA4, 0xA4},
                            {0xEA, 0xA8, 0xA8, 0xA8},
                            {0xEB, 0xAC, 0xAC, 0xAC},
                            {0xEC, 0xB0, 0xB0, 0xB0},
                            {0xED, 0xB4, 0xB4, 0xB4},
                            {0xEE, 0xB8, 0xB8, 0xB8},
                            {0xEF, 0xBC, 0xBC, 0xBC},
                            {0xF0, 0xC0, 0xC0, 0xC0},
                            {0xF1, 0xC4, 0xC4, 0xC4},
                            {0xF2, 0xC8, 0xC8, 0xC8},
                            {0xF3, 0xCC, 0xCC, 0xCC},
                            {0xF4, 0xD0, 0xD0, 0xD0},
                            {0xF5, 0xD4, 0xD4, 0xD4},
                            {0xF6, 0xD8, 0xD8, 0xD8},
                            {0xF7, 0xDC, 0xDC, 0xDC},
                            {0xF8, 0xE0, 0xE0, 0xE0},
                            {0xF9, 0xE4, 0xE4, 0xE4},
                            {0xFA, 0xE8, 0xE8, 0xE8},
                            {0xFB, 0xEC, 0xEC, 0xEC},
                            {0xFC, 0xF0, 0xF0, 0xF0},
                            {0xFD, 0xF4, 0xF4, 0xF4},
                            {0xFE, 0xF8, 0xF8, 0xF8},
                            {0xFF, 0xFC, 0xFC, 0xFC}};

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT32U   *pVideoMemAddr;      // Physical address of video memory.
  GuiConst_INT32U   *pRegisterAddr;      // Physical address of registers.
  GuiConst_INT32U    nActiveEntries=0;   // Number of items in pActiveLut.
  LutEntry          *pActiveLut;         // Pointer to current/active LUT.
  GuiConst_INT32U    DisplayBufferSize;  // Size of the SRAM display buffer.
  GuiConst_INT32U    i;

  // Set register base addresses
  pVideoMemAddr = (GuiConst_INT32U*)S1D_PHYSICAL_VMEM_ADDR;
  pRegisterAddr = (GuiConst_INT32U*)S1D_PHYSICAL_REG_ADDR;

  // Initialize all registers
  for (i = 0; i < sizeof(aS1DRegs) / sizeof(aS1DRegs[0]); i++)
  {
    if (aS1DRegs[i].Index < S1D_REGRESERVED)
      pRegisterAddr[aS1DRegs[i].Index/sizeof(S1D_VALUE)] = aS1DRegs[i].Value;
    else
    {
      switch (aS1DRegs[i].Index)
      {
        case S1D_REGDELAYOFF:
        case S1D_REGDELAYON:
        millisecond_delay((GuiConst_INT32U) aS1DRegs[i].Value);
        break;

        default:
        break;
      }
    }
  }

  // Update lookup table
  if (pRegisterAddr[0x0C / sizeof(S1D_VALUE)] & 0x00000040) // Color
  {
    switch (pRegisterAddr[0x10 / sizeof(S1D_VALUE)] & 0x0000000F) // BPP
    {
      case 1:
        nActiveEntries = 2;
        pActiveLut = Lut1BppColor;
        break;
      case 2:
        nActiveEntries = 4;
        pActiveLut = Lut2BppColor;
        break;
      case 4:
        nActiveEntries = 16;
        pActiveLut = Lut4BppColor;
        break;
      case 8:
        nActiveEntries = 256;
        pActiveLut = Lut8BppColor;
        break;
      case 16:
        // 16 BPP does not need a LUT.
        break;
    }
  }
  else // Mono
  {
    switch (pRegisterAddr[0x10 / sizeof(S1D_VALUE)] & 0x0000000F) // BPP
    {
      case 1:
        nActiveEntries = 2;
        pActiveLut = Lut1BppMono;
        break;
      case 2:
        nActiveEntries = 4;
        pActiveLut = Lut2BppMono;
        break;
      case 4:
        nActiveEntries = 16;
        pActiveLut = Lut4BppMono;
        break;
      case 8:
        nActiveEntries = 256;
        pActiveLut = Lut8BppMono;
        break;
      case 16:
        // 16 BPP does not need a LUT.
        break;
    }
  }

  for (i = 0; i < nActiveEntries; i++)
    S1D_WRITE_PALETTE(pRegisterAddr, pActiveLut[i].Index,
      pActiveLut[i].Red, pActiveLut[i].Green, pActiveLut[i].Blue);

  // Clear video buffer
  DisplayBufferSize = ((pRegisterAddr[0x00] & 0x0000FF00) >> 8) * 4096;
  DisplayBufferSize /= sizeof(pVideoMemAddr[0]); // Convert to 32-bit increments
  pVideoMemAddr[i] = 0;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh(void)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S X;
  GuiConst_INT16S LastByte;
  GuiConst_INT8U * pDisp;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    // Set address to start of video buffer
    pDisp = (GuiConst_INT8U*)S1D_PHYSICAL_VMEM_ADDR;

    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Set last byte to redraw
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;
      // Set start address on line
      pDisp += ((Y * GuiConst_BYTES_PR_LINE) +
                 GuiLib_DisplayRepaint[Y].ByteBegin);
      // Write display data
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
         *(pDisp++) = GuiLib_DisplayBuf[Y][X];

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S1D13A04

#ifdef LCD_CONTROLLER_TYPE_S1D13A05

// ============================================================================
//
// S1D13A05 DISPLAY CONTROLLER
//
// This driver uses one S1D13A05 display controller.
// LCD display memory mapped interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
// Panel: 320x240x8bpp 70Hz Color 8-Bit STN, Format 2 (PCLK=6.250MHz).
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB or Palette
//   Color depth: 8 or 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontal: 1
//   Number of display controllers, vertically: 1
//
// ============================================================================

// Use 13A05CFG PC-software generator for creation of register's values
#define S1D_13A05
#define S1D_DISPLAY_WIDTH               320
#define S1D_DISPLAY_HEIGHT              240
#define S1D_DISPLAY_BPP                 8
#define S1D_DISPLAY_SCANLINE_BYTES      320
#define S1D_DISPLAY_FRAME_RATE          70
#define S1D_DISPLAY_PCLK                6250000L
#define S1D_PHYSICAL_REG_ADDR           0x00000000L
#define S1D_PHYSICAL_BLT_ADDR           0x00010000L
#define S1D_PHYSICAL_VMEM_ADDR          0x00000000L
#define S1D_PHYSICAL_REG_SIZE           36864L
#define S1D_PHYSICAL_VMEM_SIZE          262144L
#define S1D_PHYSICAL_VMEM_REQUIRED      76800L
#define S1D_PALETTE_SIZE                256
#define S1D_POWER_DELAY_OFF             1200
#define S1D_POWER_DELAY_ON              50
#define S1D_HWBLT
#define S1D_SWBLT
#define S1D_REGRESERVED                 0xF0
#define S1D_REGDELAYOFF                 0xFD
#define S1D_REGDELAYON                  0xFE

#define S1D_WRITE_PALETTE(p,i,r,g,b)  \
  (((volatile S1D_VALUE*)(p))[0x18/sizeof(S1D_VALUE)] = (S1D_VALUE)(i)<<24 | (S1D_VALUE)(r)<<16 | (S1D_VALUE)(g)<<8 | (b))

#define S1D_READ_PALETTE(p,i,r,g,b)   \
  {  \
    S1D_VALUE reg;  \
    ((volatile S1D_VALUE*)(p))[0x1C/sizeof(S1D_VALUE)] = (S1D_VALUE)(i)<<24;  \
    reg = ((volatile S1D_VALUE*)(p))[0x1C/sizeof(S1D_VALUE)];  \
    (r) = (reg>>16)&0xFF;  \
    (g) = (reg>>8)&0xFF;  \
    (b) = reg&0xFF;  \
  }

typedef unsigned char S1D_INDEX;
typedef unsigned long S1D_VALUE;
typedef struct
{
    S1D_INDEX Index;
    S1D_VALUE Value;
} S1D_REGS;

S1D_REGS aS1DRegs[] =
{
  { 0x64,             0x28D70000 },   // GPIO Status and Control Register
  { 0x68,             0x00000000 },   // GPO Control Register
  { 0x04,             0x00000000 },   // Memory Clock Configuration Register
  { 0x08,             0x00000043 },   // Pixel Clock Configuration Register
  { 0x0C,             0x000000D0 },   // Panel Type and MOD Rate Register
  { 0x10,             0x00000008 },   // Display Settings Register
  { 0x20,             0x0000002B },   // Horizontal Total Register
  { 0x24,             0x00000027 },   // Horizontal Display Period Register
  { 0x28,             0x00000000 },   // Horizontal Display Period Start Position Register
  { 0x2C,             0x00870156 },   // FPLINE Register
  { 0x30,             0x000000FA },   // Vertical Total Register
  { 0x34,             0x000000EF },   // Vertical Display Period Register
  { 0x38,             0x00000000 },   // Vertical Display Period Start Position Register
  { 0x3C,             0x00800000 },   // FPFRAME Register
  { 0x40,             0x00000000 },   // Main Window Display Start Address Register
  { 0x44,             0x00000050 },   // Main Window Line Address Offset Register
  { 0x48,             0x00000000 },   // Extended Panel Type Register
  { 0x50,             0x00000000 },   // PIP+ Window Display Start Address Register
  { 0x54,             0x00000050 },   // PIP+ Window Line Address Offset Register
  { 0x58,             0x00000000 },   // PIP+ Window X Positions Register
  { 0x5C,             0x00000000 },   // PIP+ Window Y Positions Register
  { 0x60,             0x00000000 },   // Special Purpose Register
  { 0x70,             0x00000000 },   // PWM Clock Configuration Register
  { 0x74,             0x00000000 },   // PWMOUT Duty Cycle Register
  { 0x80,             0x00000000 },   // Scratch Pad A Register
  { 0x84,             0x00000000 },   // Scratch Pad B Register
  { 0x88,             0x00000000 },   // Scratch Pad C Register
  { 0xA0,             0x0000012C },   // HR-TFT CLS Width Register
  { 0xA4,             0x00000032 },   // HR-TFT PS1 Rising Edge Register
  { 0xA8,             0x00000064 },   // HR-TFT PS2 Rising Edge Register
  { 0xAC,             0x0000000A },   // HR-TFT PS2 Toggle Width Register
  { 0xB0,             0x00000064 },   // HR-TFT PS3 Signal Width Register
  { 0xB4,             0x0000000A },   // HR-TFT REV Toggle Point Register
  { 0xB8,             0x00000007 },   // HR-TFT PS1/2 End Register
  { 0xBC,             0x00000000 },   // Type 2 TFT Configuration Register
  { 0xC0,             0x09180E09 },   // Casio TFT Timing Register
  { 0xD8,             0x00000000 },   // Type 3 TFT Configuration Register 0
  { 0xDC,             0x00000000 },   // Type 3 TFT Configuration Register 1
  { 0xE0,             0x00000000 },   // Type 3 TFT PCLK Divide Register
  { 0xE4,             0x00000000 },   // Type 3 TFT Partial Mode Display Area Control Register
  { 0xE8,             0x00000000 },   // Type 3 TFT Partial Area 0 Positions Register
  { 0xEC,             0x00000000 },   // Type 3 TFT Partial Area 1 Positions Register
  { 0xF0,             0x00000000 },   // Type 3 TFT Partial Area 2 Positions Register
  { 0xF4,             0x00000000 },   // Type 3 TFT Command Store Register
  { 0xF8,             0x00000000 },   // Type 3 TFT Miscellaneous Register
  { 0x14,             0x00000000 },   // Power Save Configuration Register
  { S1D_REGDELAYON,   0x00000032 },   // LCD Panel Power On Delay (in ms)
  { 0x68,             0x00000000 },   // GPO Control Register
  { 0x64,             0x28D70001 }    // GPIO Status and Control Register
};
// Replace code above with your settings generated with 13A05CFG PC-software generator
//********************************************************************************************

typedef struct tagLutEntry
{
  GuiConst_INT32U Index;
  GuiConst_INT32U Red;
  GuiConst_INT32U Green;
  GuiConst_INT32U Blue;
} LutEntry;

LutEntry Lut1BppColor[2] = {{0x00, 0x00, 0x00, 0x00},
                            {0x01, 0xFF, 0xFF, 0xFF}};

LutEntry Lut2BppColor[4] = {{0x00, 0x00, 0x00, 0x00},
                            {0x01, 0x00, 0x00, 0xFF},
                            {0x02, 0xFF, 0x00, 0x00},
                            {0x03, 0xFF, 0xFF, 0xFF}};

LutEntry Lut4BppColor[16] = {{0x00, 0x00, 0x00, 0x00},
                             {0x01, 0x00, 0x00, 0xAA},
                             {0x02, 0x00, 0xAA, 0x00},
                             {0x03, 0x00, 0xAA, 0xAA},
                             {0x04, 0xAA, 0x00, 0x00},
                             {0x05, 0xAA, 0x00, 0xAA},
                             {0x06, 0xAA, 0xAA, 0x00},
                             {0x07, 0xAA, 0xAA, 0xAA},
                             {0x08, 0x00, 0x00, 0x00},
                             {0x09, 0x00, 0x00, 0xFF},
                             {0x0A, 0x00, 0xFF, 0x00},
                             {0x0B, 0x00, 0xFF, 0xFF},
                             {0x0C, 0xFF, 0x00, 0x00},
                             {0x0D, 0xFF, 0x00, 0xFF},
                             {0x0E, 0xFF, 0xFF, 0x00},
                             {0x0F, 0xFF, 0xFF, 0xFF}};

LutEntry Lut8BppColor[256] = {{0x00, 0x00, 0x00, 0x00},
                              {0x01, 0x00, 0x00, 0xAA},
                              {0x02, 0x00, 0xAA, 0x00},
                              {0x03, 0x00, 0xAA, 0xAA},
                              {0x04, 0xAA, 0x00, 0x00},
                              {0x05, 0xAA, 0x00, 0xAA},
                              {0x06, 0xAA, 0xAA, 0x00},
                              {0x07, 0xAA, 0xAA, 0xAA},
                              {0x08, 0x55, 0x55, 0x55},
                              {0x09, 0x00, 0x00, 0xFF},
                              {0x0A, 0x00, 0xFF, 0x00},
                              {0x0B, 0x00, 0xFF, 0xFF},
                              {0x0C, 0xFF, 0x00, 0x00},
                              {0x0D, 0xFF, 0x00, 0xFF},
                              {0x0E, 0xFF, 0xFF, 0x00},
                              {0x0F, 0xFF, 0xFF, 0xFF},
                              {0x10, 0x00, 0x00, 0x00},
                              {0x11, 0x11, 0x11, 0x11},
                              {0x12, 0x22, 0x22, 0x22},
                              {0x13, 0x33, 0x33, 0x33},
                              {0x14, 0x44, 0x44, 0x44},
                              {0x15, 0x55, 0x55, 0x55},
                              {0x16, 0x66, 0x66, 0x66},
                              {0x17, 0x77, 0x77, 0x77},
                              {0x18, 0x89, 0x89, 0x89},
                              {0x19, 0x9A, 0x9A, 0x9A},
                              {0x1A, 0xAB, 0xAB, 0xAB},
                              {0x1B, 0xBC, 0xBC, 0xBC},
                              {0x1C, 0xCD, 0xCD, 0xCD},
                              {0x1D, 0xDE, 0xDE, 0xDE},
                              {0x1E, 0xEF, 0xEF, 0xEF},
                              {0x1F, 0xFF, 0xFF, 0xFF},
                              {0x20, 0x00, 0x00, 0x00},
                              {0x21, 0x11, 0x00, 0x00},
                              {0x22, 0x22, 0x00, 0x00},
                              {0x23, 0x33, 0x00, 0x00},
                              {0x24, 0x44, 0x00, 0x00},
                              {0x25, 0x55, 0x00, 0x00},
                              {0x26, 0x66, 0x00, 0x00},
                              {0x27, 0x77, 0x00, 0x00},
                              {0x28, 0x89, 0x00, 0x00},
                              {0x29, 0x9A, 0x00, 0x00},
                              {0x2A, 0xAB, 0x00, 0x00},
                              {0x2B, 0xBC, 0x00, 0x00},
                              {0x2C, 0xCD, 0x00, 0x00},
                              {0x2D, 0xDE, 0x00, 0x00},
                              {0x2E, 0xEF, 0x00, 0x00},
                              {0x2F, 0xFF, 0x00, 0x00},
                              {0x30, 0x00, 0x00, 0x00},
                              {0x31, 0x00, 0x11, 0x00},
                              {0x32, 0x00, 0x22, 0x00},
                              {0x33, 0x00, 0x33, 0x00},
                              {0x34, 0x00, 0x44, 0x00},
                              {0x35, 0x00, 0x55, 0x00},
                              {0x36, 0x00, 0x66, 0x00},
                              {0x37, 0x00, 0x77, 0x00},
                              {0x38, 0x00, 0x89, 0x00},
                              {0x39, 0x00, 0x9A, 0x00},
                              {0x3A, 0x00, 0xAB, 0x00},
                              {0x3B, 0x00, 0xBC, 0x00},
                              {0x3C, 0x00, 0xCD, 0x00},
                              {0x3D, 0x00, 0xDE, 0x00},
                              {0x3E, 0x00, 0xEF, 0x00},
                              {0x3F, 0x00, 0xFF, 0x00},
                              {0x40, 0x00, 0x00, 0x00},
                              {0x41, 0x00, 0x00, 0x11},
                              {0x42, 0x00, 0x00, 0x22},
                              {0x43, 0x00, 0x00, 0x33},
                              {0x44, 0x00, 0x00, 0x44},
                              {0x45, 0x00, 0x00, 0x55},
                              {0x46, 0x00, 0x00, 0x66},
                              {0x47, 0x00, 0x00, 0x77},
                              {0x48, 0x00, 0x00, 0x89},
                              {0x49, 0x00, 0x00, 0x9A},
                              {0x4A, 0x00, 0x00, 0xAB},
                              {0x4B, 0x00, 0x00, 0xBC},
                              {0x4C, 0x00, 0x00, 0xCD},
                              {0x4D, 0x00, 0x00, 0xDE},
                              {0x4E, 0x00, 0x00, 0xEF},
                              {0x4F, 0x00, 0x00, 0xFF},
                              {0x50, 0x00, 0x00, 0xFF},
                              {0x51, 0x00, 0x11, 0xFF},
                              {0x52, 0x00, 0x22, 0xFF},
                              {0x53, 0x00, 0x33, 0xFF},
                              {0x54, 0x00, 0x44, 0xFF},
                              {0x55, 0x00, 0x55, 0xFF},
                              {0x56, 0x00, 0x66, 0xFF},
                              {0x57, 0x00, 0x77, 0xFF},
                              {0x58, 0x00, 0x89, 0xFF},
                              {0x59, 0x00, 0x9A, 0xFF},
                              {0x5A, 0x00, 0xAB, 0xFF},
                              {0x5B, 0x00, 0xBC, 0xFF},
                              {0x5C, 0x00, 0xCD, 0xFF},
                              {0x5D, 0x00, 0xDE, 0xFF},
                              {0x5E, 0x00, 0xEF, 0xFF},
                              {0x5F, 0x00, 0xFF, 0xFF},
                              {0x60, 0x00, 0xFF, 0xFF},
                              {0x61, 0x00, 0xFF, 0xEF},
                              {0x62, 0x00, 0xFF, 0xDE},
                              {0x63, 0x00, 0xFF, 0xCD},
                              {0x64, 0x00, 0xFF, 0xBC},
                              {0x65, 0x00, 0xFF, 0xAB},
                              {0x66, 0x00, 0xFF, 0x9A},
                              {0x67, 0x00, 0xFF, 0x89},
                              {0x68, 0x00, 0xFF, 0x77},
                              {0x69, 0x00, 0xFF, 0x66},
                              {0x6A, 0x00, 0xFF, 0x55},
                              {0x6B, 0x00, 0xFF, 0x44},
                              {0x6C, 0x00, 0xFF, 0x33},
                              {0x6D, 0x00, 0xFF, 0x22},
                              {0x6E, 0x00, 0xFF, 0x11},
                              {0x6F, 0x00, 0xFF, 0x00},
                              {0x70, 0x00, 0xFF, 0x00},
                              {0x71, 0x11, 0xFF, 0x00},
                              {0x72, 0x22, 0xFF, 0x00},
                              {0x73, 0x33, 0xFF, 0x00},
                              {0x74, 0x44, 0xFF, 0x00},
                              {0x75, 0x55, 0xFF, 0x00},
                              {0x76, 0x66, 0xFF, 0x00},
                              {0x77, 0x77, 0xFF, 0x00},
                              {0x78, 0x89, 0xFF, 0x00},
                              {0x79, 0x9A, 0xFF, 0x00},
                              {0x7A, 0xAB, 0xFF, 0x00},
                              {0x7B, 0xBC, 0xFF, 0x00},
                              {0x7C, 0xCD, 0xFF, 0x00},
                              {0x7D, 0xDE, 0xFF, 0x00},
                              {0x7E, 0xEF, 0xFF, 0x00},
                              {0x7F, 0xFF, 0xFF, 0x00},
                              {0x80, 0xFF, 0xFF, 0x00},
                              {0x81, 0xFF, 0xEF, 0x00},
                              {0x82, 0xFF, 0xDE, 0x00},
                              {0x83, 0xFF, 0xCD, 0x00},
                              {0x84, 0xFF, 0xBC, 0x00},
                              {0x85, 0xFF, 0xAB, 0x00},
                              {0x86, 0xFF, 0x9A, 0x00},
                              {0x87, 0xFF, 0x89, 0x00},
                              {0x88, 0xFF, 0x77, 0x00},
                              {0x89, 0xFF, 0x66, 0x00},
                              {0x8A, 0xFF, 0x55, 0x00},
                              {0x8B, 0xFF, 0x44, 0x00},
                              {0x8C, 0xFF, 0x33, 0x00},
                              {0x8D, 0xFF, 0x22, 0x00},
                              {0x8E, 0xFF, 0x11, 0x00},
                              {0x8F, 0xFF, 0x00, 0x00},
                              {0x90, 0xFF, 0x00, 0x00},
                              {0x91, 0xFF, 0x00, 0x11},
                              {0x92, 0xFF, 0x00, 0x22},
                              {0x93, 0xFF, 0x00, 0x33},
                              {0x94, 0xFF, 0x00, 0x44},
                              {0x95, 0xFF, 0x00, 0x55},
                              {0x96, 0xFF, 0x00, 0x66},
                              {0x97, 0xFF, 0x00, 0x77},
                              {0x98, 0xFF, 0x00, 0x89},
                              {0x99, 0xFF, 0x00, 0x9A},
                              {0x9A, 0xFF, 0x00, 0xAB},
                              {0x9B, 0xFF, 0x00, 0xBC},
                              {0x9C, 0xFF, 0x00, 0xCD},
                              {0x9D, 0xFF, 0x00, 0xDE},
                              {0x9E, 0xFF, 0x00, 0xEF},
                              {0x9F, 0xFF, 0x00, 0xFF},
                              {0xA0, 0xFF, 0x00, 0xFF},
                              {0xA1, 0xEF, 0x00, 0xFF},
                              {0xA2, 0xDE, 0x00, 0xFF},
                              {0xA3, 0xCD, 0x00, 0xFF},
                              {0xA4, 0xBC, 0x00, 0xFF},
                              {0xA5, 0xAB, 0x00, 0xFF},
                              {0xA6, 0x9A, 0x00, 0xFF},
                              {0xA7, 0x89, 0x00, 0xFF},
                              {0xA8, 0x77, 0x00, 0xFF},
                              {0xA9, 0x66, 0x00, 0xFF},
                              {0xAA, 0x55, 0x00, 0xFF},
                              {0xAB, 0x44, 0x00, 0xFF},
                              {0xAC, 0x33, 0x00, 0xFF},
                              {0xAD, 0x22, 0x00, 0xFF},
                              {0xAE, 0x11, 0x00, 0xFF},
                              {0xAF, 0x00, 0x00, 0xFF},
                              {0xB0, 0x00, 0x00, 0x00},
                              {0xB1, 0x11, 0x00, 0x11},
                              {0xB2, 0x22, 0x00, 0x22},
                              {0xB3, 0x33, 0x00, 0x33},
                              {0xB4, 0x44, 0x00, 0x44},
                              {0xB5, 0x55, 0x00, 0x55},
                              {0xB6, 0x66, 0x00, 0x66},
                              {0xB7, 0x77, 0x00, 0x77},
                              {0xB8, 0x89, 0x00, 0x89},
                              {0xB9, 0x9A, 0x00, 0x9A},
                              {0xBA, 0xAB, 0x00, 0xAB},
                              {0xBB, 0xBC, 0x00, 0xBC},
                              {0xBC, 0xCD, 0x00, 0xCD},
                              {0xBD, 0xDE, 0x00, 0xDE},
                              {0xBE, 0xEF, 0x00, 0xEF},
                              {0xBF, 0xFF, 0x00, 0xFF},
                              {0xC0, 0x00, 0x00, 0x00},
                              {0xC1, 0x00, 0x11, 0x11},
                              {0xC2, 0x00, 0x22, 0x22},
                              {0xC3, 0x00, 0x33, 0x33},
                              {0xC4, 0x00, 0x44, 0x44},
                              {0xC5, 0x00, 0x55, 0x55},
                              {0xC6, 0x00, 0x66, 0x66},
                              {0xC7, 0x00, 0x77, 0x77},
                              {0xC8, 0x00, 0x89, 0x89},
                              {0xC9, 0x00, 0x9A, 0x9A},
                              {0xCA, 0x00, 0xAB, 0xAB},
                              {0xCB, 0x00, 0xBC, 0xBC},
                              {0xCC, 0x00, 0xCD, 0xCD},
                              {0xCD, 0x00, 0xDE, 0xDE},
                              {0xCE, 0x00, 0xEF, 0xEF},
                              {0xCF, 0x00, 0xFF, 0xFF},
                              {0xD0, 0xFF, 0x00, 0x00},
                              {0xD1, 0xFF, 0x11, 0x11},
                              {0xD2, 0xFF, 0x22, 0x22},
                              {0xD3, 0xFF, 0x33, 0x33},
                              {0xD4, 0xFF, 0x44, 0x44},
                              {0xD5, 0xFF, 0x55, 0x55},
                              {0xD6, 0xFF, 0x66, 0x66},
                              {0xD7, 0xFF, 0x77, 0x77},
                              {0xD8, 0xFF, 0x89, 0x89},
                              {0xD9, 0xFF, 0x9A, 0x9A},
                              {0xDA, 0xFF, 0xAB, 0xAB},
                              {0xDB, 0xFF, 0xBC, 0xBC},
                              {0xDC, 0xFF, 0xCD, 0xCD},
                              {0xDD, 0xFF, 0xDE, 0xDE},
                              {0xDE, 0xFF, 0xEF, 0xEF},
                              {0xDF, 0xFF, 0xFF, 0xFF},
                              {0xE0, 0x00, 0xFF, 0x00},
                              {0xE1, 0x11, 0xFF, 0x11},
                              {0xE2, 0x22, 0xFF, 0x22},
                              {0xE3, 0x33, 0xFF, 0x33},
                              {0xE4, 0x44, 0xFF, 0x44},
                              {0xE5, 0x55, 0xFF, 0x55},
                              {0xE6, 0x66, 0xFF, 0x66},
                              {0xE7, 0x77, 0xFF, 0x77},
                              {0xE8, 0x89, 0xFF, 0x89},
                              {0xE9, 0x9A, 0xFF, 0x9A},
                              {0xEA, 0xAB, 0xFF, 0xAB},
                              {0xEB, 0xBC, 0xFF, 0xBC},
                              {0xEC, 0xCD, 0xFF, 0xCD},
                              {0xED, 0xDE, 0xFF, 0xDE},
                              {0xEE, 0xEF, 0xFF, 0xEF},
                              {0xEF, 0xFF, 0xFF, 0xFF},
                              {0xF0, 0x00, 0x00, 0xFF},
                              {0xF1, 0x11, 0x11, 0xFF},
                              {0xF2, 0x22, 0x22, 0xFF},
                              {0xF3, 0x33, 0x33, 0xFF},
                              {0xF4, 0x44, 0x44, 0xFF},
                              {0xF5, 0x55, 0x55, 0xFF},
                              {0xF6, 0x66, 0x66, 0xFF},
                              {0xF7, 0x77, 0x77, 0xFF},
                              {0xF8, 0x89, 0x89, 0xFF},
                              {0xF9, 0x9A, 0x9A, 0xFF},
                              {0xFA, 0xAB, 0xAB, 0xFF},
                              {0xFB, 0xBC, 0xBC, 0xFF},
                              {0xFC, 0xCD, 0xCD, 0xFF},
                              {0xFD, 0xDE, 0xDE, 0xFF},
                              {0xFE, 0xEF, 0xEF, 0xFF},
                              {0xFF, 0xFF, 0xFF, 0xFF}};

LutEntry Lut1BppMono[2] = {{0x00, 0x00, 0x00, 0x00},
                           {0x01, 0xFF, 0xFF, 0xFF}};

LutEntry Lut2BppMono[4] = {{0x00, 0x00, 0x00, 0x00},
                           {0x01, 0x55, 0x55, 0x55},
                           {0x02, 0xAA, 0xAA, 0xAA},
                           {0x03, 0xFF, 0xFF, 0xFF}};

LutEntry Lut4BppMono[16] = {{0x00, 0x00, 0x00, 0x00},
                            {0x01, 0x11, 0x11, 0x11},
                            {0x02, 0x22, 0x22, 0x22},
                            {0x03, 0x33, 0x33, 0x33},
                            {0x04, 0x44, 0x44, 0x44},
                            {0x05, 0x55, 0x55, 0x55},
                            {0x06, 0x66, 0x66, 0x66},
                            {0x07, 0x77, 0x77, 0x77},
                            {0x08, 0x88, 0x88, 0x88},
                            {0x09, 0x99, 0x99, 0x99},
                            {0x0A, 0xAA, 0xAA, 0xAA},
                            {0x0B, 0xBB, 0xBB, 0xBB},
                            {0x0C, 0xCC, 0xCC, 0xCC},
                            {0x0D, 0xDD, 0xDD, 0xDD},
                            {0x0E, 0xEE, 0xEE, 0xEE},
                            {0x0F, 0xFF, 0xFF, 0xFF}};

LutEntry Lut8BppMono[256] = {{0x00, 0x00, 0x00, 0x00},
                             {0x01, 0x04, 0x04, 0x04},
                             {0x02, 0x08, 0x08, 0x08},
                             {0x03, 0x0C, 0x0C, 0x0C},
                             {0x04, 0x10, 0x10, 0x10},
                             {0x05, 0x14, 0x14, 0x14},
                             {0x06, 0x18, 0x18, 0x18},
                             {0x07, 0x1C, 0x1C, 0x1C},
                             {0x08, 0x20, 0x20, 0x20},
                             {0x09, 0x24, 0x24, 0x24},
                             {0x0A, 0x28, 0x28, 0x28},
                             {0x0B, 0x2C, 0x2C, 0x2C},
                             {0x0C, 0x30, 0x30, 0x30},
                             {0x0D, 0x34, 0x34, 0x34},
                             {0x0E, 0x38, 0x38, 0x38},
                             {0x0F, 0x3C, 0x3C, 0x3C},
                             {0x10, 0x40, 0x40, 0x40},
                             {0x11, 0x44, 0x44, 0x44},
                             {0x12, 0x48, 0x48, 0x48},
                             {0x13, 0x4C, 0x4C, 0x4C},
                             {0x14, 0x50, 0x50, 0x50},
                             {0x15, 0x54, 0x54, 0x54},
                             {0x16, 0x58, 0x58, 0x58},
                             {0x17, 0x5C, 0x5C, 0x5C},
                             {0x18, 0x60, 0x60, 0x60},
                             {0x19, 0x64, 0x64, 0x64},
                             {0x1A, 0x68, 0x68, 0x68},
                             {0x1B, 0x6C, 0x6C, 0x6C},
                             {0x1C, 0x70, 0x70, 0x70},
                             {0x1D, 0x74, 0x74, 0x74},
                             {0x1E, 0x78, 0x78, 0x78},
                             {0x1F, 0x7C, 0x7C, 0x7C},
                             {0x20, 0x80, 0x80, 0x80},
                             {0x21, 0x84, 0x84, 0x84},
                             {0x22, 0x88, 0x88, 0x88},
                             {0x23, 0x8C, 0x8C, 0x8C},
                             {0x24, 0x90, 0x90, 0x90},
                             {0x25, 0x94, 0x94, 0x94},
                             {0x26, 0x98, 0x98, 0x98},
                             {0x27, 0x9C, 0x9C, 0x9C},
                             {0x28, 0xA0, 0xA0, 0xA0},
                             {0x29, 0xA4, 0xA4, 0xA4},
                             {0x2A, 0xA8, 0xA8, 0xA8},
                             {0x2B, 0xAC, 0xAC, 0xAC},
                             {0x2C, 0xB0, 0xB0, 0xB0},
                             {0x2D, 0xB4, 0xB4, 0xB4},
                             {0x2E, 0xB8, 0xB8, 0xB8},
                             {0x2F, 0xBC, 0xBC, 0xBC},
                             {0x30, 0xC0, 0xC0, 0xC0},
                             {0x31, 0xC4, 0xC4, 0xC4},
                             {0x32, 0xC8, 0xC8, 0xC8},
                             {0x33, 0xCC, 0xCC, 0xCC},
                             {0x34, 0xD0, 0xD0, 0xD0},
                             {0x35, 0xD4, 0xD4, 0xD4},
                             {0x36, 0xD8, 0xD8, 0xD8},
                             {0x37, 0xDC, 0xDC, 0xDC},
                             {0x38, 0xE0, 0xE0, 0xE0},
                             {0x39, 0xE4, 0xE4, 0xE4},
                             {0x3A, 0xE8, 0xE8, 0xE8},
                             {0x3B, 0xEC, 0xEC, 0xEC},
                             {0x3C, 0xF0, 0xF0, 0xF0},
                             {0x3D, 0xF4, 0xF4, 0xF4},
                             {0x3E, 0xF8, 0xF8, 0xF8},
                             {0x3F, 0xFC, 0xFC, 0xFC},
                             {0x40, 0x00, 0x00, 0x00},
                             {0x41, 0x04, 0x04, 0x04},
                             {0x42, 0x08, 0x08, 0x08},
                             {0x43, 0x0C, 0x0C, 0x0C},
                             {0x44, 0x10, 0x10, 0x10},
                             {0x45, 0x14, 0x14, 0x14},
                             {0x46, 0x18, 0x18, 0x18},
                             {0x47, 0x1C, 0x1C, 0x1C},
                             {0x48, 0x20, 0x20, 0x20},
                             {0x49, 0x24, 0x24, 0x24},
                             {0x4A, 0x28, 0x28, 0x28},
                             {0x4B, 0x2C, 0x2C, 0x2C},
                             {0x4C, 0x30, 0x30, 0x30},
                             {0x4D, 0x34, 0x34, 0x34},
                             {0x4E, 0x38, 0x38, 0x38},
                             {0x4F, 0x3C, 0x3C, 0x3C},
                             {0x50, 0x40, 0x40, 0x40},
                             {0x51, 0x44, 0x44, 0x44},
                             {0x52, 0x48, 0x48, 0x48},
                             {0x53, 0x4C, 0x4C, 0x4C},
                             {0x54, 0x50, 0x50, 0x50},
                             {0x55, 0x54, 0x54, 0x54},
                             {0x56, 0x58, 0x58, 0x58},
                             {0x57, 0x5C, 0x5C, 0x5C},
                             {0x58, 0x60, 0x60, 0x60},
                             {0x59, 0x64, 0x64, 0x64},
                             {0x5A, 0x68, 0x68, 0x68},
                             {0x5B, 0x6C, 0x6C, 0x6C},
                             {0x5C, 0x70, 0x70, 0x70},
                             {0x5D, 0x74, 0x74, 0x74},
                             {0x5E, 0x78, 0x78, 0x78},
                             {0x5F, 0x7C, 0x7C, 0x7C},
                             {0x60, 0x80, 0x80, 0x80},
                             {0x61, 0x84, 0x84, 0x84},
                             {0x62, 0x88, 0x88, 0x88},
                             {0x63, 0x8C, 0x8C, 0x8C},
                             {0x64, 0x90, 0x90, 0x90},
                             {0x65, 0x94, 0x94, 0x94},
                             {0x66, 0x98, 0x98, 0x98},
                             {0x67, 0x9C, 0x9C, 0x9C},
                             {0x68, 0xA0, 0xA0, 0xA0},
                             {0x69, 0xA4, 0xA4, 0xA4},
                             {0x6A, 0xA8, 0xA8, 0xA8},
                             {0x6B, 0xAC, 0xAC, 0xAC},
                             {0x6C, 0xB0, 0xB0, 0xB0},
                             {0x6D, 0xB4, 0xB4, 0xB4},
                             {0x6E, 0xB8, 0xB8, 0xB8},
                             {0x6F, 0xBC, 0xBC, 0xBC},
                             {0x70, 0xC0, 0xC0, 0xC0},
                             {0x71, 0xC4, 0xC4, 0xC4},
                             {0x72, 0xC8, 0xC8, 0xC8},
                             {0x73, 0xCC, 0xCC, 0xCC},
                             {0x74, 0xD0, 0xD0, 0xD0},
                             {0x75, 0xD4, 0xD4, 0xD4},
                             {0x76, 0xD8, 0xD8, 0xD8},
                             {0x77, 0xDC, 0xDC, 0xDC},
                             {0x78, 0xE0, 0xE0, 0xE0},
                             {0x79, 0xE4, 0xE4, 0xE4},
                             {0x7A, 0xE8, 0xE8, 0xE8},
                             {0x7B, 0xEC, 0xEC, 0xEC},
                             {0x7C, 0xF0, 0xF0, 0xF0},
                             {0x7D, 0xF4, 0xF4, 0xF4},
                             {0x7E, 0xF8, 0xF8, 0xF8},
                             {0x7F, 0xFC, 0xFC, 0xFC},
                             {0x80, 0x00, 0x00, 0x00},
                             {0x81, 0x04, 0x04, 0x04},
                             {0x82, 0x08, 0x08, 0x08},
                             {0x83, 0x0C, 0x0C, 0x0C},
                             {0x84, 0x10, 0x10, 0x10},
                             {0x85, 0x14, 0x14, 0x14},
                             {0x86, 0x18, 0x18, 0x18},
                             {0x87, 0x1C, 0x1C, 0x1C},
                             {0x88, 0x20, 0x20, 0x20},
                             {0x89, 0x24, 0x24, 0x24},
                             {0x8A, 0x28, 0x28, 0x28},
                             {0x8B, 0x2C, 0x2C, 0x2C},
                             {0x8C, 0x30, 0x30, 0x30},
                             {0x8D, 0x34, 0x34, 0x34},
                             {0x8E, 0x38, 0x38, 0x38},
                             {0x8F, 0x3C, 0x3C, 0x3C},
                             {0x90, 0x40, 0x40, 0x40},
                             {0x91, 0x44, 0x44, 0x44},
                             {0x92, 0x48, 0x48, 0x48},
                             {0x93, 0x4C, 0x4C, 0x4C},
                             {0x94, 0x50, 0x50, 0x50},
                             {0x95, 0x54, 0x54, 0x54},
                             {0x96, 0x58, 0x58, 0x58},
                             {0x97, 0x5C, 0x5C, 0x5C},
                             {0x98, 0x60, 0x60, 0x60},
                             {0x99, 0x64, 0x64, 0x64},
                             {0x9A, 0x68, 0x68, 0x68},
                             {0x9B, 0x6C, 0x6C, 0x6C},
                             {0x9C, 0x70, 0x70, 0x70},
                             {0x9D, 0x74, 0x74, 0x74},
                             {0x9E, 0x78, 0x78, 0x78},
                             {0x9F, 0x7C, 0x7C, 0x7C},
                             {0xA0, 0x80, 0x80, 0x80},
                             {0xA1, 0x84, 0x84, 0x84},
                             {0xA2, 0x88, 0x88, 0x88},
                             {0xA3, 0x8C, 0x8C, 0x8C},
                             {0xA4, 0x90, 0x90, 0x90},
                             {0xA5, 0x94, 0x94, 0x94},
                             {0xA6, 0x98, 0x98, 0x98},
                             {0xA7, 0x9C, 0x9C, 0x9C},
                             {0xA8, 0xA0, 0xA0, 0xA0},
                             {0xA9, 0xA4, 0xA4, 0xA4},
                             {0xAA, 0xA8, 0xA8, 0xA8},
                             {0xAB, 0xAC, 0xAC, 0xAC},
                             {0xAC, 0xB0, 0xB0, 0xB0},
                             {0xAD, 0xB4, 0xB4, 0xB4},
                             {0xAE, 0xB8, 0xB8, 0xB8},
                             {0xAF, 0xBC, 0xBC, 0xBC},
                             {0xB0, 0xC0, 0xC0, 0xC0},
                             {0xB1, 0xC4, 0xC4, 0xC4},
                             {0xB2, 0xC8, 0xC8, 0xC8},
                             {0xB3, 0xCC, 0xCC, 0xCC},
                             {0xB4, 0xD0, 0xD0, 0xD0},
                             {0xB5, 0xD4, 0xD4, 0xD4},
                             {0xB6, 0xD8, 0xD8, 0xD8},
                             {0xB7, 0xDC, 0xDC, 0xDC},
                             {0xB8, 0xE0, 0xE0, 0xE0},
                             {0xB9, 0xE4, 0xE4, 0xE4},
                             {0xBA, 0xE8, 0xE8, 0xE8},
                             {0xBB, 0xEC, 0xEC, 0xEC},
                             {0xBC, 0xF0, 0xF0, 0xF0},
                             {0xBD, 0xF4, 0xF4, 0xF4},
                             {0xBE, 0xF8, 0xF8, 0xF8},
                             {0xBF, 0xFC, 0xFC, 0xFC},
                             {0xC0, 0x00, 0x00, 0x00},
                             {0xC1, 0x04, 0x04, 0x04},
                             {0xC2, 0x08, 0x08, 0x08},
                             {0xC3, 0x0C, 0x0C, 0x0C},
                             {0xC4, 0x10, 0x10, 0x10},
                             {0xC5, 0x14, 0x14, 0x14},
                             {0xC6, 0x18, 0x18, 0x18},
                             {0xC7, 0x1C, 0x1C, 0x1C},
                             {0xC8, 0x20, 0x20, 0x20},
                             {0xC9, 0x24, 0x24, 0x24},
                             {0xCA, 0x28, 0x28, 0x28},
                             {0xCB, 0x2C, 0x2C, 0x2C},
                             {0xCC, 0x30, 0x30, 0x30},
                             {0xCD, 0x34, 0x34, 0x34},
                             {0xCE, 0x38, 0x38, 0x38},
                             {0xCF, 0x3C, 0x3C, 0x3C},
                             {0xD0, 0x40, 0x40, 0x40},
                             {0xD1, 0x44, 0x44, 0x44},
                             {0xD2, 0x48, 0x48, 0x48},
                             {0xD3, 0x4C, 0x4C, 0x4C},
                             {0xD4, 0x50, 0x50, 0x50},
                             {0xD5, 0x54, 0x54, 0x54},
                             {0xD6, 0x58, 0x58, 0x58},
                             {0xD7, 0x5C, 0x5C, 0x5C},
                             {0xD8, 0x60, 0x60, 0x60},
                             {0xD9, 0x64, 0x64, 0x64},
                             {0xDA, 0x68, 0x68, 0x68},
                             {0xDB, 0x6C, 0x6C, 0x6C},
                             {0xDC, 0x70, 0x70, 0x70},
                             {0xDD, 0x74, 0x74, 0x74},
                             {0xDE, 0x78, 0x78, 0x78},
                             {0xDF, 0x7C, 0x7C, 0x7C},
                             {0xE0, 0x80, 0x80, 0x80},
                             {0xE1, 0x84, 0x84, 0x84},
                             {0xE2, 0x88, 0x88, 0x88},
                             {0xE3, 0x8C, 0x8C, 0x8C},
                             {0xE4, 0x90, 0x90, 0x90},
                             {0xE5, 0x94, 0x94, 0x94},
                             {0xE6, 0x98, 0x98, 0x98},
                             {0xE7, 0x9C, 0x9C, 0x9C},
                             {0xE8, 0xA0, 0xA0, 0xA0},
                             {0xE9, 0xA4, 0xA4, 0xA4},
                             {0xEA, 0xA8, 0xA8, 0xA8},
                             {0xEB, 0xAC, 0xAC, 0xAC},
                             {0xEC, 0xB0, 0xB0, 0xB0},
                             {0xED, 0xB4, 0xB4, 0xB4},
                             {0xEE, 0xB8, 0xB8, 0xB8},
                             {0xEF, 0xBC, 0xBC, 0xBC},
                             {0xF0, 0xC0, 0xC0, 0xC0},
                             {0xF1, 0xC4, 0xC4, 0xC4},
                             {0xF2, 0xC8, 0xC8, 0xC8},
                             {0xF3, 0xCC, 0xCC, 0xCC},
                             {0xF4, 0xD0, 0xD0, 0xD0},
                             {0xF5, 0xD4, 0xD4, 0xD4},
                             {0xF6, 0xD8, 0xD8, 0xD8},
                             {0xF7, 0xDC, 0xDC, 0xDC},
                             {0xF8, 0xE0, 0xE0, 0xE0},
                             {0xF9, 0xE4, 0xE4, 0xE4},
                             {0xFA, 0xE8, 0xE8, 0xE8},
                             {0xFB, 0xEC, 0xEC, 0xEC},
                             {0xFC, 0xF0, 0xF0, 0xF0},
                             {0xFD, 0xF4, 0xF4, 0xF4},
                             {0xFE, 0xF8, 0xF8, 0xF8},
                             {0xFF, 0xFC, 0xFC, 0xFC}};

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT32U   *pVideoMemAddr;      // Physical address of video memory.
  GuiConst_INT32U   *pRegisterAddr;      // Physical address of registers.
  GuiConst_INT32U    nActiveEntries=0;   // Number of items in pActiveLut.
  LutEntry          *pActiveLut;         // Pointer to current/active LUT.
  GuiConst_INT32U    DisplayBufferSize;  // Size of the SRAM display buffer.
  GuiConst_INT32U    i;

  // Set register base addresses
  pVideoMemAddr = (GuiConst_INT32U*)S1D_PHYSICAL_VMEM_ADDR;
  pRegisterAddr = (GuiConst_INT32U*)S1D_PHYSICAL_REG_ADDR;

  // Initialize all registers
  for (i = 0; i < sizeof(aS1DRegs)/sizeof(aS1DRegs[0]); i++)
  {
    if (aS1DRegs[i].Index < S1D_REGRESERVED )
      pRegisterAddr[aS1DRegs[i].Index/sizeof(S1D_VALUE)] = aS1DRegs[i].Value;
    else
    {
      switch (aS1DRegs[i].Index)
      {
        case S1D_REGDELAYOFF:
        case S1D_REGDELAYON:
        millisecond_delay((GuiConst_INT32U) aS1DRegs[i].Value);
        break;

        default:
        break;
      }
    }
  }

  // Update lookup table
  if (pRegisterAddr[0x0C / sizeof(S1D_VALUE)] & 0x00000040) // Color
  {
    switch (pRegisterAddr[0x10 / sizeof(S1D_VALUE)] & 0x0000000F) // BPP
    {
      case 1:
        nActiveEntries = 2;
        pActiveLut = Lut1BppColor;
     break;
      case 2:
        nActiveEntries = 4;
        pActiveLut = Lut2BppColor;
        break;
      case 4:
        nActiveEntries = 16;
        pActiveLut = Lut4BppColor;
        break;
      case 8:
        nActiveEntries = 256;
        pActiveLut = Lut8BppColor;
        break;
      case 16:
        // 16 BPP does not need a LUT.
        break;
    }
  }
  else // Mono
  {
    switch (pRegisterAddr[0x10 / sizeof(S1D_VALUE)] & 0x0000000F) // BPP
    {
      case 1:
        nActiveEntries = 2;
        pActiveLut = Lut1BppMono;
        break;
      case 2:
        nActiveEntries = 4;
        pActiveLut = Lut2BppMono;
        break;
      case 4:
        nActiveEntries = 16;
        pActiveLut = Lut4BppMono;
        break;
      case 8:
        nActiveEntries = 256;
        pActiveLut = Lut8BppMono;
        break;
      case 16:
        // 16 BPP does not need a LUT.
        break;
    }
  }

  for (i = 0; i < nActiveEntries; i++
    S1D_WRITE_PALETTE(pRegisterAddr, pActiveLut[i].Index,
      pActiveLut[i].Red, pActiveLut[i].Green, pActiveLut[i].Blue);

  // Clear video buffer
  DisplayBufferSize = ((pRegisterAddr[0x00] & 0x0000FF00) >> 8) * 4096;
  DisplayBufferSize /= sizeof(pVideoMemAddr[0]); // Convert to 32-bit increments
    pVideoMemAddr[i] = 0;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh(void)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S X;
  GuiConst_INT16S LastByte;
  GuiConst_INT8U * pDisp;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    // Set address to start of video buffer
    pDisp = (GuiConst_INT8U*)S1D_PHYSICAL_VMEM_ADDR;

    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Set last byte to redraw
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;
      // Set start address on line
      pDisp += ((Y * GuiConst_BYTES_PR_LINE) +
                 GuiLib_DisplayRepaint[Y].ByteBegin);
      // Write display data
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
         *(pDisp++) = GuiLib_DisplayBuf[Y][X];

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S1D13A05

#ifdef LCD_CONTROLLER_TYPE_S1D15721

// ============================================================================
//
// S1D15721 DISPLAY CONTROLLER
//
// This driver uses one S1D15721 display controller with on-chip oscillator.
// LCD display 8 bit parallel interface, connection type 68 MCU series.
// Modify driver accordingly for other interface types.
// Graphic modes up to 128x81 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at bottom
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define DISP_DATA                 Px           // Data port

#define CHIP_SELECT               Py.7 = 0     // CS pin
#define CHIP_DESELECT             Py.7 = 1     // CS pin
#define CHIP_RESET                Py.6 = 0     // RES pin
#define CHIP_RUN                  Py.6 = 1     // RES pin
#define MODE_DATA                 Py.5 = 1     // A0 pin
#define MODE_COMMAND              Py.4 = 0     // A0 pin
#define MODE_READ                 Py.3 = 1     // R/W pin
#define MODE_WRITE                Py.3 = 0     // R/W pin
#define RW_ENABLED                Py.2 = 1     // E pin
#define RW_DISABLED               Py.2 = 0     // E pin

#define CMD_DISPLAY_ON            0xAF         //
#define CMD_DISPLAY_OFF           0xAE         //
#define CMD_MODE_NORMAL           0xA6         //
#define CMD_MODE_INVERSE          0xA7         //
#define CMD_LIGHT_ON              0xA5         //
#define CMD_LIGHT_OFF             0xA4         //
#define CMD_SCAN_NORMAL           0xC4         //
#define CMD_SCAN_REVERSE          0xC5         //
#define CMD_START_LINE            0x8A         //
#define CMD_PAGE_ADDR             0xB1         //
#define CMD_COLUMN_ADDR           0x13         //
#define CMD_DISPLAY_DATA          0x1D         //
#define CMD_DATA_COLUMN_DIR       0x84         //
#define CMD_DATA_PAGE_DIR         0x85         //
#define CMD_COLUMN_DIR_NORMAL     0xA0         //
#define CMD_COLUMN_DIR_REVERSE    0xA1         //
#define CMD_LINE_INVERT_DRIVE     0x36         //
#define CMD_LINE_INVERT_ON        0xE5         //
#define CMD_LINE_INVERT_OFF       0xE4         //
#define CMD_DISPLAY_MODE          0x66         //
#define CMD_GRAYSCALE_PATERN      0x39         //
#define CMD_DUTY_SET              0x6D         //
#define CMD_RW_MODIFY             0xE0         //
#define CMD_RW_MODIFY_RESET       0xEE         //
#define CMD_OSCILATOR_ON          0xAA         //
#define CMD_OSCILATOR_OFF         0xAB         //
#define CMD_SET_OSC_FREQ          0x5F         //
#define CMD_POWER_CONTROL         0x25         //
#define CMD_LCD_VOLTAGE           0x2B         //
#define CMD_LCD_BIAS              0xA2         //
#define CMD_ELECTRONIC_VOLUME     0x81         //
#define CMD_DISCARGE_OFF          0xEA         //
#define CMD_DISCARGE_ON           0xEB         //
#define CMD_POWER_SAVE_OFF        0xA8         //
#define CMD_POWER_SAVE_ON         0xA9         //
#define CMD_TEMP_GRADIENT         0x4E         //
#define CMD_STATUS_READ           0x8E         //
#define CMD_TEMP_SENSOR_OFF       0x68         //
#define CMD_TEMP_SENSOR_ON        0x69         //
#define CMD_MLS_SELECT            0xE7         //
#define CMD_NOP                   0xE3         //
#define DATA_BINARY               0x01         //
#define DATA_GRAYSCALE            0x00         //
#define DATA_1_81_DUTY            0x13         //

// Wait states maybe by required between steps depending
// on the speed of microcontroller

// Command writing
#define WRITE_COMMAND(Val)                                               \
{                                                                        \
  MODE_COMMAND;                                                          \
  MODE_WRITE;                                                            \
  RW_ENABLED;                                                            \
  CHIP_SELECT;                                                           \
  DISP_DATA = Val;                                                       \
  CHIP_DESELECT;                                                         \
  RW_DISABLED;                                                           \
}

// Data writing
#define WRITE_DATA(Val)                                                  \
{                                                                        \
  MODE_DATA;                                                             \
  MODE_WRITE;                                                            \
  RW_ENABLED;                                                            \
  CHIP_SELECT;                                                           \
  DISP_DATA = Val;                                                       \
  CHIP_DESELECT;                                                         \
  RW_DISABLED;                                                           \
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S X;

  // 30mS delay may be required when built-in oscilator is used

  // Reset controller
  CHIP_RESET;
  CHIP_RUN;

  // Column address set direction
  WRITE_COMMAND(CMD_COLUMN_DIR_NORMAL);
  // Common output status select
  WRITE_COMMAND(CMD_SCAN_NORMAL);
  // Display normal/reverse
  WRITE_COMMAND(CMD_MODE_NORMAL);
  // Display all lighting ON/OFF
  WRITE_COMMAND(CMD_LIGHT_OFF);
  // Display mode set grayscale/binary
  WRITE_COMMAND(CMD_DISPLAY_MODE);
  WRITE_DATA(DATA_BINARY);
  // Set the duty
  WRITE_COMMAND(CMD_DUTY_SET);
  WRITE_DATA(DATA_1_81_DUTY);
  WRITE_DATA(0x00);
  // LCD voltage select
  WRITE_COMMAND(CMD_LCD_VOLTAGE);
  WRITE_DATA(0x00);
  // Set electronic volume
  WRITE_COMMAND(CMD_ELECTRONIC_VOLUME);
  WRITE_DATA(0x00);
  // Set temperature gradient
  WRITE_COMMAND(CMD_TEMP_GRADIENT);
  WRITE_DATA(0x00);
  // Inline invert drive
  WRITE_COMMAND(CMD_LINE_INVERT_DRIVE);
  WRITE_DATA(0x00);
  // Inline invert ON/OFF
  WRITE_COMMAND(CMD_LINE_INVERT_OFF);
  // Built-in oscilator frequency select
  WRITE_COMMAND(CMD_SET_OSC_FREQ);
  WRITE_DATA(0x00);
  // Built-in oscilator ON/OFF
  WRITE_COMMAND(CMD_OSCILATOR_ON);
  // LCD bias set
  WRITE_COMMAND(CMD_LCD_BIAS);
  WRITE_DATA(0x00);
  // Power control set
  WRITE_COMMAND(CMD_POWER_CONTROL);
  WRITE_DATA(0x07);
  // Display start line
  WRITE_COMMAND(CMD_START_LINE);
  WRITE_DATA(0x00);
  // Display data input direction
  WRITE_COMMAND(CMD_DATA_COLUMN_DIR);
  // Page address set
  WRITE_COMMAND(CMD_PAGE_ADDR);
  WRITE_DATA(0x00);
  // Column address set
  WRITE_COMMAND(CMD_COLUMN_ADDR);
  WRITE_DATA(0x00);
  // Display ON
  WRITE_COMMAND(CMD_DISPLAY_ON);

  // Clear display memory
  WRITE_COMMAND(CMD_DISPLAY_DATA);
  for (X = 0; X < GuiConst_DISPLAY_BYTES; X++)   // Loop through bytes
  WRITE_DATA(0x00);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
      // Something to redraw on this page
    {
      // Select page
      WRITE_COMMAND(CMD_PAGE_ADDR);
      WRITE_DATA(Y);
      // Select start column
      WRITE_COMMAND(CMD_COLUMN_ADDR);
      WRITE_DATA(GuiLib_DisplayRepaint[Y].ByteBegin);
      // Select stop column
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;
      // Write display data
      WRITE_COMMAND(CMD_DISPLAY_DATA);
      for (N = GuiLib_DisplayRepaint[Y].ByteBegin; N <= LastByte; N++)
        WRITE_DATA(GuiLib_DisplayBuf[Y][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S1D15721

#ifdef LCD_CONTROLLER_TYPE_S6B0741

// ============================================================================
//
// S6B0741 DISPLAY CONTROLLER
//
// This driver uses one S6B0741 display controller with on-chip oscillator.
// LCD display 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 128x129 pixels.
//
// Compatible display controllers:
//   NT7508
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 2
//   Color mode: Grayscale
//   Color depth: 2 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Pxx.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define DISP_DATA                 Pxx

#define CHIP_SELECT               Pxx.x = 0     // CSB pin
#define CHIP_DESELECT             Pxx.x = 1     // CSB pin
#define CHIP_RESET                Pxx.x = 0     // RESETB pin
#define CHIP_RUN                  Pxx.x = 1     // RESETB pin
#define MODE_DATA                 Pxx.x = 1     // RS pin
#define MODE_COMMAND              Pxx.x = 0     // RS pin
#define MODE_READ                 Pxx.x = 1     // RW_WR pin
#define MODE_WRITE                Pxx.x = 0     // RW_WR pin
#define RW_ENABLED                Pxx.x = 1     // E_RD pin
#define RW_DISABLED               Pxx.x = 0     // E_RD pin

#define CMD_ICON_OFF              0xA2
#define CMD_ICON_ON               0xA3
#define CMD_PAGE_ADDR             0xB0
#define CMD_COL_ADDR              0x10
#define CMD_MODIFY_READ_OFF       0xE0
#define CMD_MODIFY_READ_ON        0xEE
#define CMD_DISPLAY_OFF           0xAE
#define CMD_DISPLAY_ON            0xAF
#define CMD_LINE_REG              0x40
#define CMD_COM0                  0x44
#define CMD_PARTIAL_DUTY_RATIO    0x48
#define CMD_NLINE_INV_ON          0x4C
#define CMD_NLINE_INV_OFF         0xE4
#define CMD_REVERSE_DISPLAY_OFF   0xA6
#define CMD_REVERSE_DISPLAY_ON    0xA7
#define CMD_DISPLAY_PIX_NORMAL    0xA4
#define CMD_DISPLAY_PIX_DARK      0xA5
#define CMD_POWER_CTRL            0x28
#define VAL_POWER_CTRL_VC_OFF     0x00
#define VAL_POWER_CTRL_VC_ON      0x04
#define VAL_POWER_CTRL_VR_OFF     0x00
#define VAL_POWER_CTRL_VR_ON      0x02
#define VAL_POWER_CTRL_VF_OFF     0x00
#define VAL_POWER_CTRL_VF_ON      0x01
#define CMD_DC_DC_STEPUP          0x64
#define VAL_DC_DC_STEPUP_3        0x00
#define VAL_DC_DC_STEPUP_4        0x01
#define VAL_DC_DC_STEPUP_5        0x02
#define VAL_DC_DC_STEPUP_6        0x03
#define CMD_REG_RESIST            0x20
#define VAL_REG_RESIST_2_3        0x00
#define VAL_REG_RESIST_3_0        0x01
#define VAL_REG_RESIST_3_7        0x02
#define VAL_REG_RESIST_4_4        0x03
#define VAL_REG_RESIST_5_1        0x04
#define VAL_REG_RESIST_5_8        0x05
#define VAL_REG_RESIST_6_5        0x06
#define VAL_REG_RESIST_7_2        0x07
#define CMD_ELECTR_VOL            0x81
#define CMD_LCD_BIAS              0x50
#define VAL_LCD_BIAS_5            0x00
#define VAL_LCD_BIAS_6            0x01
#define VAL_LCD_BIAS_7            0x02
#define VAL_LCD_BIAS_8            0x03
#define VAL_LCD_BIAS_9            0x04
#define VAL_LCD_BIAS_10           0x05
#define VAL_LCD_BIAS_11           0x06
#define VAL_LCD_BIAS_12           0x07
#define CMD_SHL_NORM              0xC0
#define CMD_SHL_REVERSE           0xC8
#define CMD_ADC_NORM              0xA0
#define CMD_ADC_REVERSE           0xA1
#define CMD_OSC_ON                0xAB
#define CMD_POWER_SAVE_NORMAL     0xA8
#define CMD_POWER_SAVE_SLEEP      0xA9
#define CMD_POWER_SAVE_OFF        0xE1
#define CMD_RESET                 0xE2
#define CMD_DATA_DIR_DISP_LEN     0xE8
#define CMD_NOP                   0xE3
#define CMD_TEST                  0xF0
#define CMD_PWM_FRC_MODE          0x90
#define VAL_FRC_4FRC              0x00
#define VAL_FRC_3FRC              0x04
#define VAL_PWM_9PWM              0x00
#define VAL_PWM_12PWM             0x02
#define VAL_PWM_15PWM             0x03
#define CMD_GRAY_SCALE_MODE       0x88

// Command writing
#define WRITE_COMMAND(Val)                                               \
{                                                                        \
  MODE_COMMAND;                                                          \
  MODE_WRITE;                                                            \
  RW_ENABLED;                                                            \
  CHIP_SELECT;                                                           \
  DISP_DATA = Val;                                                       \
  CHIP_DESELECT;                                                         \
  RW_DISABLED;                                                           \
}

// Data writing
#define WRITE_DATA(Val)                                                  \
{                                                                        \
  MODE_DATA;                                                             \
  MODE_WRITE;                                                            \
  RW_ENABLED;                                                            \
  CHIP_SELECT;                                                           \
  DISP_DATA = Val;                                                       \
  CHIP_DESELECT;                                                         \
  RW_DISABLED;                                                           \
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  // Reset controller
  CHIP_RUN;

  // Set display duty rate
  WRITE_COMMAND(CMD_ICON_OFF);
  WRITE_COMMAND(CMD_PARTIAL_DUTY_RATIO);
  WRITE_COMMAND(GuiConst_DISPLAY_HEIGHT_HW);   // Duty = display height

  // SEG output direction
  WRITE_COMMAND(CMD_ADC_NORM);

  // COM output direction
  WRITE_COMMAND(CMD_SHL_NORM);

  // Initial row
  WRITE_COMMAND(CMD_COM0);
  WRITE_COMMAND(0);

  // Turn on oscillator
  WRITE_COMMAND(CMD_OSC_ON);

  // Set DC-DC step-up (adjust as needed)
  WRITE_COMMAND(CMD_DC_DC_STEPUP + VAL_DC_DC_STEPUP_3);

  // Set regulator resistor (adjust as needed)
  WRITE_COMMAND(CMD_REG_RESIST + VAL_REG_RESIST_2_3);

  // Set electronic volumen (adjust as needed)
  WRITE_COMMAND(CMD_ELECTR_VOL + 0);

  // Set LCD bias (adjust as needed)
  WRITE_COMMAND(CMD_LCD_BIAS + VAL_LCD_BIAS_5);

  // Set power control
  WRITE_COMMAND(CMD_POWER_CTRL +
     VAL_POWER_CTRL_VC_OFF + VAL_POWER_CTRL_VR_OFF + VAL_POWER_CTRL_VF_OFF);

  // Display start line in RAM is 000000b
  WRITE_COMMAND(CMD_LINE_REG);
  WRITE_COMMAND(0);

  // Clear display memory
  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Select page
    WRITE_COMMAND(CMD_PAGE_ADDR + PageNo);
    // Select column
    WRITE_COMMAND(CMD_COL_ADDR + 0);
    WRITE_COMMAND(0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
      WRITE_DATA(0);
  }
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT8U PageNo;
  GuiConst_INT8U ColNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      WRITE_COMMAND(CMD_PAGE_ADDR + PageNo);
      // Select column
      ColNo = GuiLib_DisplayRepaint[PageNo].ByteBegin;
      WRITE_COMMAND(CMD_COL_ADDR + ((ColNo & 0x70) >> 4));
      WRITE_COMMAND(ColNo & 0x0F);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
      {
        WRITE_DATA(GuiLib_DisplayBuf[0][PageNo][N]);
        WRITE_DATA(GuiLib_DisplayBuf[1][PageNo][N]);
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S6B0741

#ifdef LCD_CONTROLLER_TYPE_S6B33BL

// ============================================================================
//
// S6B33BL DISPLAY CONTROLLER
//
// This driver uses one S6B33BL display controller with on-chip oscillator.
// LCD display in 8080-system parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 144x177 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================
// Hardwired connection
// PS  MPU[1]  MPU[0] MPU interface mode
// H   L       L      8080-series 8bit interface
//
// CS2         L      chip select 2
// REG_ENB     L      enable internal regulator

#define DISP_DATA            Pxx           // PIN D7 - D0

#define RESET_ON             Pxx.x = 0     // RSTB pin
#define RESET_OFF            Pxx.x = 1     // RSTB pin
#define CHIP_SELECT          Pxx.x = 0     // CS1B pin
#define CHIP_DESELECT        Pxx.x = 1     // CS1B pin
#define MODE_DATA            Pxx.x = 1     // (DI)RS pin
#define MODE_COMMAND         Pxx.x = 0     // (DI)RS pin
#define MODE_WRITE_OFF       Pxx.x = 1     // WRB(RW) pin
#define MODE_WRITE_ON        Pxx.x = 0     // WRB(RW) pin
#define MODE_READ_OFF        Pxx.x = 1     // RDB(E) pin
#define MODE_READ_ON         Pxx.x = 0     // RDB(E) pin

// Command writing
void LCD_Cmd(GuiConst_INT8U Val)
{
  MODE_COMMAND;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

// Data writing
void LCD_Data(GuiConst_INT8U Val)
{
  MODE_DATA;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void GuiDisplay_Init(void)
{
  GuiConst_INT32U j;

  // Set Read signal OFF
  MODE_READ_OFF;

  // RESET driver
  RESET_ON;
  millisecond_delay(10);
  RESET_OFF;

  // Standby Mode OFF
  LCD_Cmd(0x2C);
  millisecond_delay(100);

  // Oscillation Mode Set
  LCD_Cmd(0x02);
  // Internal oscilator ON
  LCD_Cmd(0x01);

  // Set AMP ON, DCDC3 ON, DCDC2 ON, DCDC1 ON sequence
  LCD_Cmd(0x26);
  // DB0 - DCD1 ON
  LCD_Cmd(0x01);

  LCD_Cmd(0x26);
  // DB3 - AMP ON
  // DB0 - DCD1 ON
  LCD_Cmd(0x09);

  LCD_Cmd(0x26);
  // DB3 - AMP ON
  // DB1 - DCD2 ON
  // DB0 - DCD1 ON
  LCD_Cmd(0x0B);

  LCD_Cmd(0x26);
  // DB3 - AMP ON
  // DB2 - DCD3 ON
  // DB1 - DCD2 ON
  // DB0 - DCD1 ON
  LCD_Cmd(0x0F);

  // Set column
  LCD_Cmd(0x43);
  // Start column
  LCD_Cmd(0);
  // End column
  LCD_Cmd(GuiConst_DISPLAY_WIDTH_HW);

  // Set line
  LCD_Cmd(0x42);
  // Start line
  LCD_Cmd(0);
  // End line
  LCD_Cmd(GuiConst_BYTE_LINES);

  // Contrast Control(1)
  LCD_Cmd(0x2A);
  // range 0 - 255
  LCD_Cmd(0x80);

  // Addressing Mode Set
  LCD_Cmd(0x30);
  // DB6-DB5 : 0 - 65k color, 1 - 4k color, 2 - 256 color
  LCD_Cmd(0x00);

  // Entry Mode Set
  LCD_Cmd(0x40);
  // DB1 - 0: Y address counter mode, 1: X address counter mode
  LCD_Cmd(0x00);

  // Specified Display Pattern
  LCD_Cmd(0x53);
  // DB1-DB0 : 0 - Normal display, 1 - reverse, 2 - whole ON,, 3 - whole OFF
  LCD_Cmd(0x00);

  // Driver Output Mode Set
  LCD_Cmd(0x10);
  // Segment direction DB2
  // Swap segment output DB1
  // Display Duty DB5-DB4 : 0 - 1/128, 1 - 1/144, 2 - 1/160, 3 - 1/176
  LCD_Cmd(0x00);

  // DC-DC step-up select
  LCD_Cmd(0x20);
  // DB1-DB0 : 0 - 1x, 1 - 1.5x, 2 - 2x
  LCD_Cmd(0x00);

  // Bias Set
  LCD_Cmd(0x22);
  // DB1-DB0 : 0 - 1/4, 1 - 1/5, 2 - 1/6, 3 - 1/7
  LCD_Cmd(0x00);

  // DCDC Clock Division Set
  LCD_Cmd(0x24);
  // DB1-DB0 : 0 - fOSC/4, 1 - fOSC/8, 2 - fOSC/16, 3 - fOSC/32
  LCD_Cmd(0x20);

  // Clear display buffer
  for (j = 0; j < GuiConst_DISPLAY_BYTES; j++)
    LCD_Data(0x00);

  // Display ON
  LCD_Cmd(0x51);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Set line
      LCD_Cmd(0x42);
      // Start line
      LCD_Cmd(Y);
      // End line
      LCD_Cmd(Y);

      // Set column
      LCD_Cmd(0x43);
      // Start column
      LCD_Cmd(GuiLib_DisplayRepaint[Y].ByteBegin);

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // End column
      LCD_Cmd(LastByte);

      // Write display data
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        LCD_Data((GuiConst_INT8U)(GuiLib_DisplayBuf.Words[Y][X]>>8));
        LCD_Data((GuiConst_INT8U)GuiLib_DisplayBuf.Words[Y][X]);
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S6B33BL

#ifdef LCD_CONTROLLER_TYPE_S6D0139

// ============================================================================
//
// S6D0139 DISPLAY CONTROLLER
//
// This driver uses one S6D0139 display controller with on-chip oscillator.
// LCD display 8 bit 80-system parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x320 pixels.
//
// Compatible display controllers:
//   S6D0129
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired connection
// IM3  IM2  IM1  IM0/ID  MPU interface mode
// VSS  VSS  VDD3 VDD3    80-system 8-bit bus interface

#define DISP_DATA            Pxx           // PIN D17 - D10

#define RESET_ON             Pxx.x = 0     // RESET pin
#define RESET_OFF            Pxx.x = 1     // RESET pin
#define CHIP_SELECT          Pxx.x = 0     // CSB pin
#define CHIP_DESELECT        Pxx.x = 1     // CSB pin
#define MODE_DATA            Pxx.x = 1     // RS pin
#define MODE_COMMAND         Pxx.x = 0     // RS pin
#define MODE_WRITE_OFF       Pxx.x = 1     // WRB pin
#define MODE_WRITE_ON        Pxx.x = 0     // WRB pin
#define MODE_READ_OFF        Pxx.x = 1     // RDB pin
#define MODE_READ_ON         Pxx.x = 0     // RDB pin

// Control register addresses to load into index register
#define START_OSCILATION     0x0000
#define DRIVER_CONTROL       0x0001
#define LCD_DRIVING_CONTROL  0x0002
#define ENTRY_MODE           0x0003
#define DISPLAY_CONTROL_1    0x0007
#define DISPLAY_CONTROL_2    0x0008
#define FRAME_CYCLE_CONTROL  0X000B
#define EXTERNAL_INTERFACE   0x000C
#define POWER_CONTROL_1      0x0010
#define POWER_CONTROL_2      0x0011
#define POWER_CONTROL_3      0x0013
#define POWER_CONTROL_4      0x0014
#define GAMMA_CONTROL_0      0x0030
#define GAMMA_CONTROL_1      0x0031
#define GAMMA_CONTROL_2      0x0032
#define GAMMA_CONTROL_3      0x0033
#define GAMMA_CONTROL_4      0x0034
#define GAMMA_CONTROL_5      0x0035
#define GAMMA_CONTROL_6      0x0036
#define GAMMA_CONTROL_7      0x0037
#define GAMMA_CONTROL_8      0x0038
#define GAMMA_CONTROL_9      0x0039
#define GATE_SCAN_POSITION   0x0040
#define SCREEN_1_DRV_END     0x0042
#define SCREEN_1_DRV_START   0x0043
#define SCREEN_2_DRV_END     0x0044
#define SCREEN_2_DRV_START   0x0045
#define HORIZONTAL_START_END 0x0046
#define VERTICAL_END         0x0047
#define VERTICAL_START       0x0048
#define HORIZONTAL_ADDRESS   0x0020
#define VERTICAL_ADDRESS     0x0021
#define GRAM_DATA            0x0022

// Command writing
void LCD_Cmd(GuiConst_INT16U Val)
{
  MODE_COMMAND;
  DISP_DATA = (GuiConst_INT8U)(Val>>8);
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
  CHIP_DESELECT;  // Some delay

  DISP_DATA = (GuiConst_INT8U)(Val);
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

// Data writing
void LCD_Data(GuiConst_INT16U Val)
{
  MODE_DATA;
  DISP_DATA = (GuiConst_INT8U)(Val>>8);
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
  CHIP_DESELECT;  // Some delay

  DISP_DATA = (GuiConst_INT8U)(Val);
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

/*
// For memory mapped interface on AT91RM9200 and S6D0139 in 16 bit mode

// AT91RM9200 PIO definitions
#define AT91_SYS    0xFFFFF000
#define AT91_PIOA   0xFFFFF400
#define AT91_PIOB   0xFFFFF600
#define AT91_PIOC   0xFFFFF800
#define AT91_PIOD   0xFFFFFA00

#define PMC_PCER    0x0010

#define PIO_PER     0x00
#define PIO_PDR     0x04
#define PIO_OER     0x10
#define PIO_ODR     0x14
#define PIO_SODR    0x30
#define PIO_CODR    0x34
#define PIO_PDSR    0x3c
#define PIO_IDR     0x44         // Interrupt Disable Register
#define PIO_PUDR    0x60         // Pull-up Disable Register
#define PIO_PUER    0x64         // Pull-up Enable Register
#define PIO_MDDR    0x54         // Multi-driver Disable Register

#define PIOA_ID     2
#define PIOB_ID     3
#define PIOC_ID     4
#define PIOD_ID     5

#define PMC_OFFSET  0xc00

// pin definition
#define DATA        0x0000FFFF
#define RESET       0x00000004
#define CHIP_SELECT 0x00000010
#define RS          0x00000020
#define WRITE       0x00000040
#define READ        0x00000080

// writing macros
#define SET_DATA_REG        *((GuiConst_INT32U *)(AT91_PIOB + PIO_SODR))
#define CLR_DATA_REG        *((GuiConst_INT32U *)(AT91_PIOB + PIO_CODR))
#define SET_CONTROL_REG     *((GuiConst_INT32U *)(AT91_PIOC + PIO_SODR))
#define CLR_CONTROL_REG     *((GuiConst_INT32U *)(AT91_PIOC + PIO_CODR))

void LcdLowLevelInit(void)
{
  // enable pio registers
  *((GuiConst_INT32U *)(AT91_PIOC + PIO_PER) = RESET | CHIP_SELECT | RS | WRITE | READ;
  *((GuiConst_INT32U *)(AT91_PIOB + PIO_PER) = DATA;

  // enable control output
  *((GuiConst_INT32U *)(AT91_PIOC + PIO_OER) = RESET | CHIP_SELECT | RS | WRITE | READ;
  // enable data output
  *((GuiConst_INT32U *)(AT91_PIOB + PIO_OER) = DATA;

  // disable multidrive
  *((GuiConst_INT32U *)(AT91_PIOC + PIO_MDDR) = RESET | CHIP_SELECT | RS | WRITE | READ;
  *((GuiConst_INT32U *)(AT91_PIOB + PIO_MDDR) = DATA;

  // enable PIO clocks
  *((GuiConst_INT32U *)(AT91_SYS + PMC_OFFSET + PMC_PCER) = (1 << PIOB_ID) | (1 << PIOC_ID);
}

void Lcd_Cmd(GuiConst_INT32U Val)
{
  CLR_CONTROL_REG = RS;
  // set data on databus
  SET_DATA_REG = Val;
  CLR_DATA_REG = (~Val)&0xFFFFFFFF;
  CLR_CONTROL_REG = CHIP_SELECT;
  CLR_CONTROL_REG = WRITE;
  SET_CONTROL_REG = WRITE;
  SET_CONTROL_REG = CHIP_SELECT;
}

void Lcd_Data(GuiConst_INT32U Val)
{
  SET_CONTROL_REG = RS;
  // set data on databus
  SET_DATA_REG = Val;
  CLR_DATA_REG = (~Val)&0xFFFFFFFF;
  CLR_CONTROL_REG = CHIP_SELECT;
  CLR_CONTROL_REG = WRITE;
  SET_CONTROL_REG = WRITE;
  SET_CONTROL_REG = CHIP_SELECT;
}
*/

void LCD_Clear(GuiConst_INT16U color)
{
  GuiConst_INT32U j;

  for (j = 0; j < GuiConst_DISPLAY_BYTES; j++)
  {
    LCD_Data(color);
  }
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void LCD_Init()
{
  LcdLowLevelInit();      // This function only for AT91RM9200

  LCD_Cmd(POWER_CONTROL_2);
  LCD_Data(0x2F04);

  LCD_Cmd(POWER_CONTROL_4);
  LCD_Data(0x0015);

  LCD_Cmd(POWER_CONTROL_1);
  LCD_Data(0x3D00);

  LCD_Cmd(POWER_CONTROL_3);
  LCD_Data(0x0040);

  millisecond_delay(10);

  LCD_Cmd(POWER_CONTROL_3);
  LCD_Data(0x0060);

  millisecond_delay(50);

  LCD_Cmd(POWER_CONTROL_3);
  LCD_Data(0x0070);

  millisecond_delay(40);

  LCD_Cmd(GAMMA_CONTROL_0);
  LCD_Data(0x0000);

  LCD_Cmd(GAMMA_CONTROL_1);
  LCD_Data(0x0300);

  LCD_Cmd(GAMMA_CONTROL_2);
  LCD_Data(0x0007);

  LCD_Cmd(GAMMA_CONTROL_3);
  LCD_Data(0x0303);

  LCD_Cmd(GAMMA_CONTROL_4);
  LCD_Data(0x0004);

  LCD_Cmd(GAMMA_CONTROL_5);
  LCD_Data(0x0000);

  LCD_Cmd(GAMMA_CONTROL_6);
  LCD_Data(0x0700);

  LCD_Cmd(GAMMA_CONTROL_7);
  LCD_Data(0x0302);

  LCD_Cmd(GAMMA_CONTROL_8);
  LCD_Data(0x1600);

  LCD_Cmd(GAMMA_CONTROL_9);
  LCD_Data(0x0010);

  LCD_Cmd(DRIVER_CONTROL);  // Shift direction from S720 to S0 - 0x0127,
  LCD_Data(0x0127);         // Shift direction from S0 to S720 - 0x0027

  LCD_Cmd(LCD_DRIVING_CONTROL);
  LCD_Data(0x0700);

  LCD_Cmd(ENTRY_MODE);
  LCD_Data(0x0030);       // 68/80-system 8-bit interface (TRI=0, DFM=0)

  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0000);

  LCD_Cmd(DISPLAY_CONTROL_2);
  LCD_Data(0x0404);

  LCD_Cmd(FRAME_CYCLE_CONTROL);
  LCD_Data(0x0200);

  LCD_Cmd(EXTERNAL_INTERFACE);
  LCD_Data(0x0000);

  LCD_Cmd(GATE_SCAN_POSITION);
  LCD_Data(0x0000);

  LCD_Cmd(SCREEN_1_DRV_START);
  LCD_Data(0x013F);

  LCD_Cmd(SCREEN_1_DRV_END);
  LCD_Data(0x0000);

  LCD_Cmd(SCREEN_2_DRV_END);
  LCD_Data(0x0000);

  LCD_Cmd(SCREEN_2_DRV_START);
  LCD_Data(0x0000);

  LCD_Cmd(HORIZONTAL_START_END);
  LCD_Data(0xEF00);

  LCD_Cmd(VERTICAL_END);
  LCD_Data(0x013F);

  LCD_Cmd(VERTICAL_START);
  LCD_Data(0x0000);

  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0011);

  millisecond_delay(40);

  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0017);

  LCD_Cmd(HORIZONTAL_ADDRESS);
  LCD_Data(0x0000);

  LCD_Cmd(VERTICAL_ADDRESS);
  LCD_Data(0x0000);

  LCD_Cmd(GRAM_DATA);
}

void Display_Off()
{
  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0012);

  millisecond_delay(40);

  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0000);

  LCD_Cmd(POWER_CONTROL_1);
  LCD_Data(0x0000);

  LCD_Cmd(POWER_CONTROL_4);
  LCD_Data(0x0000);

  LCD_Cmd(POWER_CONTROL_2);
  LCD_Data(0x0000);

  LCD_Cmd(POWER_CONTROL_3);
  LCD_Data(0x0060);

  LCD_Cmd(POWER_CONTROL_3);
  LCD_Data(0x0040);

  LCD_Cmd(POWER_CONTROL_3);
  LCD_Data(0x0000);
}

void Display_On()
{
  LCD_Cmd(POWER_CONTROL_2);
  LCD_Data(0x2804);

  LCD_Cmd(POWER_CONTROL_4);
  LCD_Data(0x0815);

  LCD_Cmd(POWER_CONTROL_1);
  LCD_Data(0x3D00);

  LCD_Cmd(POWER_CONTROL_3);
  LCD_Data(0x0040);

  millisecond_delay(10);

  LCD_Cmd(POWER_CONTROL_3);
  LCD_Data(0x0060);

  millisecond_delay(50);

  LCD_Cmd(POWER_CONTROL_3);
  LCD_Data(0x0070);

  millisecond_delay(40);

  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0011);

  millisecond_delay(40);

  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0017);
}

void SleepMode()
{
  Display_Off();

  millisecond_delay(20);

  LCD_Cmd(POWER_CONTROL_1);
  LCD_Data(0x0002);
}

void SleepExit()
{
  LCD_Cmd(POWER_CONTROL_1);
  LCD_Data(0x0000);

  millisecond_delay(40);

  Display_On();
}

void StandbyMode()
{
  Display_Off();

  millisecond_delay(20);

  LCD_Cmd(POWER_CONTROL_1);
  LCD_Data(0x0001);
}

void StandbyExit()
{
  LCD_Cmd(START_OSCILATION);
  LCD_Data(0x0001);

  millisecond_delay(10);

  LCD_Cmd(POWER_CONTROL_1);
  LCD_Data(0x0000);

  millisecond_delay(40);

  LCD_Init();
}

void GuiDisplay_Init(void)
{
  // Start screen initialisation

/*
  // RESET only for AT91RM9200
  CLR_CONTROL_REG  = RESET;
  millisecond_delay(10);
  SET_CONTROL_REG = RESET;
*/

  // RESET for others micro
  RESET_ON;
  millisecond_delay(10);
  RESET_OFF;


  LCD_Init();
  LCD_Clear(0x0000);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(VERTICAL_ADDRESS);
      LCD_Data(Y);

      // Select start column
      LCD_Cmd(HORIZONTAL_ADDRESS);
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin);

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      LCD_Cmd(GRAM_DATA);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        LCD_Data((GuiConst_INT16U)GuiLib_DisplayBuf.Words[Y][X]);
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S6D0139

#ifdef LCD_CONTROLLER_TYPE_S6E63D6

// ============================================================================
//
// S6E63D6 DISPLAY CONTROLLER
//
// This driver uses one S6E63D6 display controller with on-chip oscillator.
// LCD display in 8, 16 bit 80-system parallel interface, or SPI interface
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x320 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Palette or RGB
//   Color depth: 4 bits, 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// Function Spi_SendData must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired connection
// MDDI_EN - 0, �Low� = MDDI Disable, �High� = MDDI Enable
// S_PB    - 0, �Low� = Parallel Interface, �High� = Serial Interface
// For parallel interface:
// ID_MIB  - 0, �Low� = Intel 80x-system, �High� = Motorola 68x-system
// For SPI serial interface:
// ID_MIB = 0, ID bit

// Uncomment for serial SPI interface
// #define INTERFACE_SPI
// Uncomment for 8 bit 8080 parallel interface
#define INTERFACE_8BIT
// Uncomment for 16 bit 8080 parallel interface
// #define INTERFACE_16BIT

#define DISP_DATA            Pxx           // PIN DB 17-10(only fo 16 bit interface), DB 8-1
#define RESET_ON             Pxx.x = 0     // RESETB pin
#define RESET_OFF            Pxx.x = 1     // RESETB pin
#define CHIP_SELECT          Pxx.x = 0     // CSB pin
#define CHIP_DESELECT        Pxx.x = 1     // CSB pin
#define MODE_DATA            Pxx.x = 1     // RS pin
#define MODE_COMMAND         Pxx.x = 0     // RS pin
#define MODE_WRITE_OFF       Pxx.x = 1     // WRB pin
#define MODE_WRITE_ON        Pxx.x = 0     // WRB pin
#define MODE_READ_OFF        Pxx.x = 1     // RDB pin
#define MODE_READ_ON         Pxx.x = 0     // RDB pin

// Constant for selecting 16 bit interface
#define SET_16_BIT_INTERFACE 0x23
// Constant for selecting 16 bit interface
#define SET_8_BIT_INTERFACE  0x24

// Control register addresses to load into index register
#define NOP                  0x00
#define DISPLAY_CONTROL      0x01
#define LCD_DRIVING_CONTROL  0x02
#define ENTRY_MODE           0x03
#define CLOCK_CONTROL        0x04
#define DISPLAY_CONTROL_1    0x05
#define DISPLAY_CONTROL_2    0x06
#define PANEL_CONTROL_1      0x07
#define PANEL_CONTROL_2      0x08
#define PANEL_CONTROL_3      0x09
#define PANEL_CONTROL_4      0x0A
#define STAND_BY             0x10
#define POWER_GEN1           0x12
#define POWER_GEN2           0x13
#define POWER_STEP_CONTROL_1 0x14
#define START_OSCILLATION    0x18
#define SOURCE_CONTROL       0x1A
#define HORIZONTAL_ADDRESS   0x20
#define VERTICAL_ADDRESS     0x21
#define GRAM_DATA            0x22
#define SELECT_DATA_BUS_1    0x23
#define VERTICAL_START       0x35
#define VERTICAL_END         0x36
#define HORIZONTAL_START_END 0x37
#define GAMMA_CONTROL_0      0x70
#define GAMMA_CONTROL_1      0x71
#define GAMMA_CONTROL_2      0x72
#define GAMMA_CONTROL_3      0x73
#define GAMMA_CONTROL_4      0x74
#define GAMMA_CONTROL_5      0x75
#define GAMMA_CONTROL_6      0x76
#define GAMMA_CONTROL_7      0x77
#define GAMMA_CONTROL_8      0x78
#define GAMMA_CONTROL_9      0x79
#define GAMMA_SELECT         0x80

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

#ifdef INTERFACE_SPI
// One byte SPI transter function
void Spi_SendData(GuiConst_INT8U Spi_Data)
{
  // Something hardware related
  // The data is transferred with the MSB first.
  // Wait for end of transfer
}

// Command writing in SPI mode
void LCD_Cmd(GuiConst_INT16U Val)
{
  CHIP_SELECT;

  // Header byte
  Spi_SendData(0x70);  // 01110 -ID code,0 - ID_MIB, 0 - RS, 0 - WR

  Spi_SendData((GuiConst_INT8U)(Val>>8));
  Spi_SendData((GuiConst_INT8U)Val);

  CHIP_DESELECT;
}

// Data writing in SPI mode
void LCD_Data(GuiConst_INT16U Val)
{
  CHIP_SELECT;

  // Header byte
  Spi_SendData(0x72);  // 01110 -ID code,0 - ID_MIB, 1 - RS, 0 - WR

  Spi_SendData((GuiConst_INT8U)(Val>>8));
  Spi_SendData((GuiConst_INT8U)Val);

  CHIP_DESELECT;
}

#endif

#ifdef INTERFACE_8BIT
void LCD_Cmd(GuiConst_INT8U Val)
{
  MODE_COMMAND;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

// Data writing in parallel interface
void LCD_Data(GuiConst_INT8U Val)
{
  MODE_DATA;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}
#endif

#ifdef INTERFACE_16BIT
// Command writing in parallel interface
void LCD_Cmd(GuiConst_INT16U Val)
{
  MODE_COMMAND;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

// Data writing in parallel interface
void LCD_Data(GuiConst_INT16U Val)
{
  MODE_DATA;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}
#endif

void GuiDisplay_Init(void)
{
  GuiConst_INT32U j;

  // Reset controller
  RESET_ON;
  millisecond_delay(10);
  RESET_OFF;

#ifdef INTERFACE_8BIT
  // Select 8 bit interface
  LCD_Cmd(SET_8_BIT_INTERFACE);

  LCD_Cmd(POWER_GEN1);
  LCD_Data(0x00);
  LCD_Data(0x08);

  LCD_Cmd(POWER_STEP_CONTROL_1);
  LCD_Data(0x42);
  LCD_Data(0x00);

  LCD_Cmd(POWER_GEN2);
  LCD_Data(0x14);
  LCD_Data(0x62);

  LCD_Cmd(START_OSCILLATION);
  LCD_Data(0x00);
  LCD_Data(0x1F);

  LCD_Cmd(SOURCE_CONTROL);
  LCD_Data(0x00);
  LCD_Data(0x05);

  // Release standby mode
  LCD_Cmd(STAND_BY);
  LCD_Data(0x00);
  LCD_Data(0x00);

  millisecond_delay(120);

  LCD_Cmd(GRAM_DATA);

  // Clear display buffer
#ifdef GuiConst_COLOR_DEPTH_4
  for (j = 0; j < (GuiConst_DISPLAY_BYTES * 2); j++)
#else
  for (j = 0; j < GuiConst_DISPLAY_BYTES; j++)
#endif
    LCD_Data(0x00);

  // Display on
  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x00);
  LCD_Data(0x01);

#else

  // Select 16 bit interface
  LCD_Cmd(SET_16_BIT_INTERFACE);

  // Initialize controller with default values
  LCD_Cmd(POWER_GEN1);
  LCD_Data(0x0008);

  LCD_Cmd(POWER_STEP_CONTROL_1);
  LCD_Data(0x4200);

  LCD_Cmd(POWER_GEN2);
  LCD_Data(0x1462);

  LCD_Cmd(START_OSCILLATION);
  LCD_Data(0x001F);

  LCD_Cmd(SOURCE_CONTROL);
  LCD_Data(0x0005);

  // Release standby mode
  LCD_Cmd(STAND_BY);
  LCD_Data(0x0000);

  millisecond_delay(120);

  LCD_Cmd(GRAM_DATA);

  // Clear display buffer
#ifdef GuiConst_COLOR_DEPTH_4
  for (j = 0; j < (GuiConst_DISPLAY_BYTES * 2); j++)
#else
  for (j = 0; j < GuiConst_DISPLAY_BYTES / 2; j++)
#endif
    LCD_Data(0x0000);

  // Display on
  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0001);

#endif
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;
#ifdef GuiConst_COLOR_DEPTH_4
  GuiConst_INT8U PaletteIndex, ColorR, ColorG, ColorB;
#endif

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(VERTICAL_ADDRESS);
#ifdef INTERFACE_8BIT
      LCD_Data((GuiConst_INT8U)(Y>>8));
      LCD_Data((GuiConst_INT8U)Y);
#else
      LCD_Data(Y);
#endif

      // Select start column
      LCD_Cmd(HORIZONTAL_ADDRESS);
#ifdef GuiConst_COLOR_DEPTH_4
#ifdef INTERFACE_8BIT
      LCD_Data((GuiConst_INT8U)((GuiLib_DisplayRepaint[Y].ByteBegin * 2) >> 8));
      LCD_Data((GuiConst_INT8U)(GuiLib_DisplayRepaint[Y].ByteBegin * 2));
#else
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin * 2);
#endif
      LastByte = GuiConst_BYTES_PR_LINE - 1;
#else
#ifdef INTERFACE_8BIT
      LCD_Data((GuiConst_INT8U)(GuiLib_DisplayRepaint[Y].ByteBegin >> 8));
      LCD_Data((GuiConst_INT8U)GuiLib_DisplayRepaint[Y].ByteBegin);
#else
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin);
#endif
      LastByte = GuiConst_BYTES_PR_LINE / 2 - 1;
#endif

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      LCD_Cmd(GRAM_DATA);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
#ifdef GuiConst_COLOR_DEPTH_4
        // for bit 0 at right
        PaletteIndex = GuiLib_DisplayBuf[Y][X] >> 4;
        ColorR = GuiStruct_Palette[PaletteIndex][0] & 0xF8;
        ColorG = GuiStruct_Palette[PaletteIndex][1] & 0xFC;
        ColorB = GuiStruct_Palette[PaletteIndex][2] & 0xF8;
#ifdef INTERFACE_8BIT
        LCD_Data(ColorR | (ColorG >> 5));
        LCD_Data((ColorG << 3) | (ColorB >> 3));
#else
        LCD_Data(((GuiConst_INT16U)ColorR << 8) |
           ((GuiConst_INT16U)ColorG << 3) |
           ((GuiConst_INT16U)ColorB >> 3));
#endif
        PaletteIndex = GuiLib_DisplayBuf[Y][X] & 0x0F;
        ColorR = GuiStruct_Palette[PaletteIndex][0] & 0xF8;
        ColorG = GuiStruct_Palette[PaletteIndex][1] & 0xFC;
        ColorB = GuiStruct_Palette[PaletteIndex][2] & 0xF8;
#ifdef INTERFACE_8BIT
        LCD_Data(ColorR | (ColorG >> 5));
        LCD_Data((ColorG << 3) | (ColorB >> 3));
#else
        LCD_Data(((GuiConst_INT16U)ColorR << 8) |
           ((GuiConst_INT16U)ColorG << 3) |
           ((GuiConst_INT16U)ColorB >> 3));
#endif
#else
#ifdef INTERFACE_8BIT
        LCD_Data(GuiLib_DisplayBuf.Bytes[Y][X*2]);
        LCD_Data(GuiLib_DisplayBuf.Bytes[Y][X*2+1]);
#else
        LCD_Data((GuiConst_INT16U)GuiLib_DisplayBuf.Words[Y][X]);
#endif
#endif
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_S6E63D6

#ifdef LCD_CONTROLLER_TYPE_SBN1661_2H_SBN0080

// ============================================================================
//
// SBN1661 + SBN0080 DISPLAY CONTROLLER
//
// This driver uses two SBN1661 and one SBN0080 display controller.
// For versions M02D must be suplied external clock.
// LCD display in 8-bit 8080 type parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 160x32 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define DB                            Pxx              // Data DB0 - DB7

#define A0_CMD                        Pxx.x = 0        // A0 pin
#define A0_DATA                       Pxx.x = 1        // A0 pin
#define CS1_LOW                       Pxx.x = 0        // CS1 pin
#define CS1_HIGH                      Pxx.x = 1        // CS1 pin
#define CS2_LOW                       Pxx.x = 0        // CS2 pin
#define CS2_HIGH                      Pxx.x = 1        // CS2 pin
#define WRITE_LOW                     Pxx.x = 0        // R/W pin
#define WRITE_HIGH                    Pxx.x = 1        // R/W pin
#define READ_LOW                      Pxx.x = 0        // E pin
#define READ_HIGH                     Pxx.x = 1        // E pin
#define RESET_LOW                     Pxx.x = 0        // RST pin
#define RESET_HIGH                    Pxx.x = 1        // RST pin

#define NOP                           asm ("nop")   // nop code - adjust according
                                                    // to your compiler syntax
#define START_LINE                    0
#define MAX_MEMORY_COLUMNS            80
#define FIRST_CONTROLLER_END_COLUMN   61
#define SECOND_CONTROLLER_END_COLUMN  141
#define CTRL_BYTE_FIRST_PAGE          0xB8

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               1000
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// CONTROLLER 1 select (segments s1-s61)
#define SELECT_CONTROLLER_1                                              \
{                                                                        \
  CS1_LOW;                                                               \
  CS2_LOW;                                                               \
}

// CONTROLLER 2 select (segments s62-s141)
#define SELECT_CONTROLLER_2                                              \
{                                                                        \
  CS1_LOW;                                                               \
  CS2_HIGH;                                                              \
}

// CONTROLLER 3 select (segments 142-160)
#define SELECT_CONTROLLER_3                                              \
{                                                                        \
  CS1_HIGH;                                                              \
  CS2_LOW;                                                               \
}

// CONTROLLERS deselect
#define DESELECT_CONTROLLERS                                             \
{                                                                        \
  CS1_HIGH;                                                              \
  CS2_HIGH;                                                              \
}

// COMMAND write
// Remember to select display controller in advance
void WRITE_COMMAND_REG(GuiConst_INT8U val)
{
  A0_CMD;
  READ_HIGH;
  WRITE_LOW;
  DB = val;
  NOP;          // write low pulse must be > 200ns
  NOP;          // adjust with number of NOP commands
  WRITE_HIGH;
  DESELECT_CONTROLLERS;
  NOP;          // cycle time between two consecutive commands must be > 1000ns
  NOP;          // adjust with number of NOP commands
}

// DATA write
// Remember to select display controller in advance
void WRITE_DATA_REG(GuiConst_INT8U val)
{
  A0_DATA;
  READ_HIGH;
  WRITE_LOW;
  DB = val;
  NOP;         // write low pulse must be > 200ns
  NOP;         // adjust with number of NOP commands
  WRITE_HIGH;
  DESELECT_CONTROLLERS;
  NOP;         // cycle time between two consecutive commands must be > 1000ns
  NOP;         // adjust with number of NOP commands
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S X, Y;

  DESELECT_CONTROLLERS;

  // Positive RESET pulse set 8080 interface mode
  RESET_HIGH;
  millisecond_delay(1);
  RESET_LOW;

  // Select start line
  SELECT_CONTROLLER_1;
  WRITE_COMMAND_REG(0xC0|START_LINE);
  SELECT_CONTROLLER_2;
  WRITE_COMMAND_REG(0xC0|START_LINE);
  SELECT_CONTROLLER_3;
  WRITE_COMMAND_REG(0xC0|START_LINE);

  // Display duty 1/32 - 0xA9, display duty 1/16 - 0xA8
  SELECT_CONTROLLER_1;
  WRITE_COMMAND_REG(0xA9);
  SELECT_CONTROLLER_2;
  WRITE_COMMAND_REG(0xA9);
  SELECT_CONTROLLER_3;
  WRITE_COMMAND_REG(0xA9);

  // Unselect only if you want inverted mapping of output segments
  //SELECT_CONTROLLER_1;
  //WRITE_COMMAND_REG(0xA1);
  //SELECT_CONTROLLER_2;
  //WRITE_COMMAND_REG(0xA1);
  //SELECT_CONTROLLER_3;
  //WRITE_COMMAND_REG(0xA1);

  // Clear display memory
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)   // Loop through pages
  {
    // Select page
    SELECT_CONTROLLER_1;
    WRITE_COMMAND_REG(CTRL_BYTE_FIRST_PAGE + Y);

    // Select column 0
    SELECT_CONTROLLER_1;
    WRITE_COMMAND_REG(0x00);

    for (X = 0; X < MAX_MEMORY_COLUMNS; X++)   // Loop through bytes
    {
      SELECT_CONTROLLER_1;
      WRITE_DATA_REG(0x00);
    }

    // Select page
    SELECT_CONTROLLER_2;
    WRITE_COMMAND_REG(CTRL_BYTE_FIRST_PAGE + Y);

    // Select column 0
    SELECT_CONTROLLER_2;
    WRITE_COMMAND_REG(0x00);

    for (X = 0; X < MAX_MEMORY_COLUMNS; X++)   // Loop through bytes
    {
      SELECT_CONTROLLER_2;
      WRITE_DATA_REG(0x00);
    }

    // Select page
    SELECT_CONTROLLER_3;
    WRITE_COMMAND_REG(CTRL_BYTE_FIRST_PAGE + Y);

    // Select column 0
    SELECT_CONTROLLER_3;
    WRITE_COMMAND_REG(0x00);

    for (X = 0; X < MAX_MEMORY_COLUMNS; X++)   // Loop through bytes
    {
      SELECT_CONTROLLER_3;
      WRITE_DATA_REG(0x00);
    }
  }

  // Finished initializing, set display on
  SELECT_CONTROLLER_1;
  WRITE_COMMAND_REG(0xAF);
  SELECT_CONTROLLER_2;
  WRITE_COMMAND_REG(0xAF);
  SELECT_CONTROLLER_3;
  WRITE_COMMAND_REG(0xAF);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S LineNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  // Walk through all pages
  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++)
  {
    if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      if (GuiLib_DisplayRepaint[LineNo].ByteBegin < FIRST_CONTROLLER_END_COLUMN)
         // Something to redraw in first section
      {
        // Select page
        SELECT_CONTROLLER_1;
        WRITE_COMMAND_REG(CTRL_BYTE_FIRST_PAGE + LineNo);

        // Select starting column
        SELECT_CONTROLLER_1;
        WRITE_COMMAND_REG(GuiLib_DisplayRepaint[LineNo].ByteBegin);

        // Write display data
        LastByte = FIRST_CONTROLLER_END_COLUMN - 1;
        if (GuiLib_DisplayRepaint[LineNo].ByteEnd < LastByte)
          LastByte = GuiLib_DisplayRepaint[LineNo].ByteEnd;
        for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin; N <= LastByte; N++)
        {
          SELECT_CONTROLLER_1;
          WRITE_DATA_REG(GuiLib_DisplayBuf[LineNo][N]);
        }

        // Reset repaint parameters
        if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= FIRST_CONTROLLER_END_COLUMN)
          // Something to redraw in second section
          GuiLib_DisplayRepaint[LineNo].ByteBegin = FIRST_CONTROLLER_END_COLUMN;
        else // Done with this line
          GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
      }

      if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
         // Something to redraw in second section
      {
        // Select page
        SELECT_CONTROLLER_2;
        WRITE_COMMAND_REG(CTRL_BYTE_FIRST_PAGE + LineNo);

        // Select starting column
        SELECT_CONTROLLER_2;
        WRITE_COMMAND_REG(
           GuiLib_DisplayRepaint[LineNo].ByteBegin -
           FIRST_CONTROLLER_END_COLUMN);

        // Write display data
        for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin;
             N <= GuiLib_DisplayRepaint[LineNo].ByteEnd; N++)
        {
          SELECT_CONTROLLER_2;
          WRITE_DATA_REG(GuiLib_DisplayBuf[LineNo][N]);
        }

        // Reset repaint parameters
        if (GuiLib_DisplayRepaint[LineNo].ByteEnd >=
            SECOND_CONTROLLER_END_COLUMN)
          // Something to redraw in second section
          GuiLib_DisplayRepaint[LineNo].ByteBegin =
             SECOND_CONTROLLER_END_COLUMN;
        else // Done with this line
          GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
      }

      if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
         // Something to redraw in third section
      {
        // Select page
        SELECT_CONTROLLER_3;
        WRITE_COMMAND_REG(CTRL_BYTE_FIRST_PAGE + LineNo);

        // Select starting column
        SELECT_CONTROLLER_3;
        WRITE_COMMAND_REG(
           GuiLib_DisplayRepaint[LineNo].ByteBegin -
           SECOND_CONTROLLER_END_COLUMN);

        // Write display data
        for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin;
             N <= GuiLib_DisplayRepaint[LineNo].ByteEnd; N++)
        {
          SELECT_CONTROLLER_3;
          WRITE_DATA_REG(GuiLib_DisplayBuf[LineNo][N]);
        }

        GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
      }
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SBN1661_2H_SBN0080

#ifdef LCD_CONTROLLER_TYPE_SED1335

// ============================================================================
//
// SED1335 DISPLAY CONTROLLER
//
// This driver uses one SED1335 display controller.
// LCD display 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 640x256 pixels.
//
// Compatible display controllers:
//   RA8835, S1D13305, S1D13700
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// LCD display constants
#define CHRWTH  8      // Character width
#define CHRHIG  8      // Character hight
#define WF      0x80   // Two frame AC drive

// LCD data bus and control bus I/O pointers
#define LCD_DATA_BUS (*(GuiConst_INT8U*)0x800000)   // Memory mapped LCD data bus ADR
#define LCD_CNTR_BUS (*(GuiConst_INT8U*)0x800001)   // Memory mapped LCD control bus ADR

// Control bus macros:
#define RESET  0xDF  // Reset soon after power on
#define CMDWR  0xF5  // Command Write
#define DATWR  0xE1  // Data and parameter Write
#define WRCMD  0xF3  // _WR goes high after CMDWR
#define WRDAT  0xE3  // _WR goes high after DATWR

// Data bus macros - Command set
#define SYSTEMSET 0x40  // Initialize device and display, 8 parameters
#define SLEEPIN   0x53  // Enter standby mode, 0 parameters
#define DISPOFF   0x58  // Enable and disable display and display flashing, 1 parameter
#define DISPON    0x59  // Enable and disable display and display flashing, 1 parameter
#define SCROLL    0x44  // Set display start address and display regions, 10 parameters
#define CSRFORM   0x5D  // Set cursor type, 2 parameters
#define CGRAMADR  0x5C  // Set start address of character generator RAM, 2 parameters
#define CSRDIRRGT 0x4C  // Set direction of cursor movement to right, 0 parameters
#define CSRDIRLFT 0x4D  // Set direction of cursor movement to left, 0 parameters
#define CSRDIRUP  0x4E  // Set direction of cursor movement to up, 0 parameters
#define CSRDIRDWN 0x4F  // Set direction of cursor movement to down, 0 parameters
#define HDOTSCR   0x5A  // Set horizontal scroll position, 1 parameter
#define OVLAY     0x5B  // Set display overlay format, 1 parameter
#define CSRW      0x46  // Set cursor address, 2 parameters
#define MWRITE    0x42  // Write to display memory, n parameters

// Systemset - parameters
#define P1        0x30 // Parameter 1: Int.CGROM,No D6,8 pix.char hight,Single panel drv,Scr. topline correction
#define FX        (CHRWTH-1)|WF // Parameter 2: Horizontal char size, two frame AC drive
#define FY        CHRHIG-1 // Parameter 3: Vertival char size
#define CR        (GuiConst_DISPLAY_WIDTH_HW/CHRWTH-1)*((CHRWTH+7)>>3) // Parameter 4: (Chars pr. line - 1) * horizontal bytes pr. char
#define TCR       0x2D // Parameter 5: (Fclk/((LF+1)*Ffr)-1)/9=(10e6/((239+1)*70)-1)/9=66 ~ 0x42 > 4+CR
#define LF        GuiConst_DISPLAY_HEIGHT_HW-1 // Parameter 6: Height in lines of a frame LF+1
#define APL       GuiConst_DISPLAY_WIDTH_HW/CHRWTH // Parameter 7: Horizontal address range LSB
#define APH       0x00 // Parameter 8: Horizontal address range LSB

// Scroll - parameters
#define SAD1L     0x00  // Parameter  1: Screen block 1 address low byte
#define SAD1H     0x00  // Parameter  2: Screen block 1 address high byte
#define SL_1      LF    // Parameter  3: Number of lines pr. scrolling screen
#define SAD2L     0x60  // Parameter  4: Screen block 2 address low byte
#define SAD2H     0x09  // Parameter  5: Screen block 2 address high byte
#define SL_2      LF    // Parameter  6: Number of lines pr. scrolling screen
#define SAD3L     0x00  // Parameter  7: Screen block 3 address low byte
#define SAD3H     0x00  // Parameter  8: Screen block 3 address high byte
#define SAD4L     0x00  // Parameter  9: Screen block 4 address low byte
#define SAD4H     0x00  // Parameter 10: Screen block 4 address high byte

// General macros
#define ALLHIGH   0xFF  // Bit 0-7 = '1'
#define TAH8      0     // Address hold time
#define TDS8      0     // Data setup time
#define TCYC8     0     // System cycle time
#define TCC       0     // Strobe pulse width
#define TRES      500   // 2000 ~ Reset pulse 1,3 ms >= 1 ms :  20(33n+68)

void lcdDatWr (GuiConst_INT8U parameter);
void lcdCmdWr (GuiConst_INT8U parameter);
void lcdCrsLayOne (void);
void lcdCrsLayTwo (void);
void lcdClrLayOne (void);
void lcdClrLayTwo (void);

void lcdDatWr (GuiConst_INT8U parameter)
{
  LCD_DATA_BUS = parameter;   // [D0:D7]=parameter
  LCD_CNTR_BUS = DATWR;       // A0=0, _RD=1, _WR=0, _CS=0
  LCD_CNTR_BUS = WRDAT;       // A0=0, _RD=1, _WR=1, _CS=0
  LCD_CNTR_BUS = ALLHIGH;     // A0=1, _RD=1, _WR=1, _CS=1
}

void lcdCmdWr (GuiConst_INT8U command)
{
  LCD_DATA_BUS = command;   // [D0:D7]=command
  LCD_CNTR_BUS = CMDWR;     // A0=1, _RD=1, _WR=0, _CS=0
  LCD_CNTR_BUS = WRCMD;     // A0=1, _RD=1, _WR=1, _CS=0
  LCD_CNTR_BUS = ALLHIGH;   // A0=1, _RD=1, _WR=1, _CS=1
}

void lcdCrsLayOne (void)
{
  lcdCmdWr (CSRW);
  lcdDatWr (0);
  lcdDatWr (0);
}

void lcdCrsLayOne30 (void)
{
  lcdCmdWr (CSRW);
  lcdDatWr (APL);
  lcdDatWr (0);
}

void lcdCrsLayTwo (void)
{
  lcdCmdWr (CSRW);
  lcdDatWr (SAD2L);
  lcdDatWr (SAD2H);
}

void lcdClrLayOne (void)
{
  GuiConst_INT8U i;
  GuiConst_INT8U j;
  lcdCrsLayOne ();
  lcdCmdWr (MWRITE);
  for (i = 30; i != 0; --i)
    for (j = APL; j != 0; --j)
      lcdDatWr (0);
}

void lcdClrLayTwo (void)
{
  GuiConst_INT8U i;
  GuiConst_INT8U j;
  lcdCrsLayTwo ();
  lcdCmdWr (MWRITE);
  for (i = GuiConst_DISPLAY_HEIGHT_HW; i != 0; --i)
    for (j = APL; j != 0; --j)
      lcdDatWr (0);
}

void GuiDisplay_Init (void)
{

  // Setup display
  LCD_CNTR_BUS = ALLHIGH;   // _RES=1
  lcdCmdWr (SYSTEMSET);
  lcdDatWr (P1);
  lcdDatWr (FX);
  lcdDatWr (FY);
  lcdDatWr (CR);
  lcdDatWr (TCR);
  lcdDatWr (LF);
  lcdDatWr (APL);
  lcdDatWr (APH);
  lcdCmdWr (SCROLL);
  lcdDatWr (SAD1L);
  lcdDatWr (SAD1H);
  lcdDatWr (SL_1);
  lcdDatWr (SAD2L);
  lcdDatWr (SAD2H);
  lcdDatWr (SL_2);
  lcdDatWr (SAD3L);
  lcdDatWr (SAD3H);
  lcdDatWr (SAD4L);
  lcdDatWr (SAD4H);
  lcdCmdWr (HDOTSCR);
  lcdDatWr (0x00);
  lcdCmdWr (OVLAY);
  lcdDatWr (0x00);
  lcdCmdWr (CSRFORM);
  lcdDatWr (0x07);
  lcdDatWr (0x87);
  lcdCmdWr (CSRDIRRGT);
  lcdCmdWr (DISPON);
  lcdDatWr (0x14);
  lcdClrLayOne ();
  lcdClrLayTwo ();
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X;
  GuiConst_INT16S Y;
  GuiConst_INT16S LastByte;
  GuiConst_INT16U Address;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Address Pointer Set
      Address = Y * GuiConst_BYTES_PR_LINE +
         GuiLib_DisplayRepaint[Y].ByteBegin + SAD2H * 256 + SAD2L;

      lcdCmdWr (CSRW);
      lcdDatWr (Address % 256); // Low part of address
      lcdDatWr (Address / 256); // High part of address

      // Write display data
      lcdCmdWr (MWRITE);
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        lcdDatWr (GuiLib_DisplayBuf[Y][X]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Finished drawing
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SED1335

#ifdef LCD_CONTROLLER_TYPE_SEPS525

// ============================================================================
//
// SEPS525 DISPLAY CONTROLLER
//
// This driver uses one SEPS525 display controller with on-chip oscillator.
// LCD display in 16 bit 80-system parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 160x128 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired connection
// CPU = 0, 80-system bus interface
// PS = 1, Parallel interface
// DB[0] = 0, DB[9] = 0
// Connect RESETB to RST pin of microcontroller

#define DISP_DATA                     Pxx              // DB[17:10], DB[8:1]

#define MODE_COMMAND                  Pxx.x = 0        // RS pin
#define MODE_DATA                     Pxx.x = 1        // RS pin
#define CSB_LOW                       Pxx.x = 0        // CSB pin
#define CSB_HIGH                      Pxx.x = 1        // CSB pin
#define WRITE_LOW                     Pxx.x = 0        // WRB/RWB pin
#define WRITE_HIGH                    Pxx.x = 1        // WRB/RWB pin
#define READ_LOW                      Pxx.x = 0        // RDB/E pin
#define READ_HIGH                     Pxx.x = 1        // RDB/E pin
#define RESET_LOW                     Pxx.x = 0        // RESETB pin
#define RESET_HIGH                    Pxx.x = 1        // RESETB pin

// All command registers are accessed through upper byte DB[15-8]
// Command writing
void LCD_Cmd(GuiConst_INT16U Val)
{
  MODE_COMMAND;
  DISP_DATA = Val;
  CSB_LOW;
  WRITE_LOW;
  WRITE_HIGH;
  CSB_HIGH;
}

// Data writing
void LCD_Data(GuiConst_INT16U Val)
{
  MODE_DATA;
  DISP_DATA = Val;
  CSB_LOW;
  WRITE_LOW;
  WRITE_HIGH;
  CSB_HIGH;
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT32U i;

  READ_HIGH;

  // SOFT_RESET (05h)
  LCD_Cmd(0x0500);
  LCD_Data(0x0100);
  millisecond_delay(1);
  LCD_Data(0x0000);

  millisecond_delay(1);

  // REDUCE_CURRENT (04h)
  LCD_Cmd(0x0400);
  LCD_Data(0x0100);
  millisecond_delay(1);
  LCD_Data(0x0000);

  millisecond_delay(1);

  // OSCILATOR_CONTROL (02h)
  LCD_Cmd(0x0200);
  // Bit7 = 0 - export internal clock, Bit7 = 1 - export �0� level
  // Bit6 = 0 - Osc with external resistor, Bit6 = 1 - Osc with internal resistor
  // Bit1 = 0 - Internal CLK, Bit7 = 1 - External CLK
  // Bit0 = 0 - CLK OFF, Bit0 = 1 - CLK ON
  LCD_Data(0xC100);

  // IREF (80h)
  LCD_Cmd(0x8000);
  // Bit0 = 0, Reference voltage controlled by external resistor
  // Bit0 = 1, Reference voltage controlled by internal resistor
  LCD_Data(0x0100);

  // CLOCK_DIV (03h)
  LCD_Cmd(0x0300);
  // Bits[7:4] : OSC frequency setting
  // Bits[3:0] : Display frequency divide ration
  LCD_Data(0x3000);

  // PRECHARGE_TIME_R (08h)
  // Range 0 - 15
  LCD_Cmd(0x0800);
  LCD_Data(0x0000);

  // PRECHARGE_TIME_G (09h)
  // Range 0 - 15
  LCD_Cmd(0x0900);
  LCD_Data(0x0000);

  // PRECHARGE_TIME_B (0Ah)
  // Range 0 - 15
  LCD_Cmd(0x0A00);
  LCD_Data(0x0000);

  // PRECHARGE_CURRENT_R (0Bh)
  // Setting value * 8uA
  LCD_Cmd(0x0B00);
  LCD_Data(0x0000);

  // PRECHARGE_CURRENT_G (0Ch)
  // Setting value * 8uA
  LCD_Cmd(0x0C00);
  LCD_Data(0x0000);

  // PRECHARGE_CURRENT_B (0Dh)
  // Setting value * 8uA
  LCD_Cmd(0x0D00);
  LCD_Data(0x0000);

  // DRIVING_CURRENT_R (10h)
  // setting value * 1uA
  LCD_Cmd(0x1000);
  LCD_Data(0x0000);

  // DRIVING_CURRENT_G (11h)
  // setting value * 1uA
  LCD_Cmd(0x1100);
  LCD_Data(0x0000);

  // DRIVING_CURRENT_B (12h)
  // setting value * 1uA
  LCD_Cmd(0x1200);
  LCD_Data(0x0000);

  // DISPLAY_MODE_SET(13h)
  LCD_Cmd(0x1300);
  // Bit7 = 0 - RGB, Bit7 = 1 - BGR
  // Bit6 = 0, Bit5 = 0 - Row scan direction 0 - 127
  // Bit6 = 0, Bit5 = 1 - Row scan direction 127 - 0
  // Bit6 = 1, Bit5 = 0 - Row scan direction 0,2,4 ... 1,3,5
  // Bit6 = 1, Bit5 = 1 - Row scan direction 1,3,5 ... 0,2,4
  // Bit4 = 0 - Column shift direction D0 to D159,
  // Bit4 = 1 - Column shift direction D159 to D0
  // Bit2 = 0 - One screen mode, Bit2 = 1, Two screen mode
  // Bit1 = 0, Bit0 = 0 -  Normal Display
  // Bit1 = 0, Bit0 = 1 - All Low Display
  // Bit1 = 1, Bit0 = 0 - All High Display
  LCD_Data(0x0000);

  // RGB_IF (14h)
  LCD_Cmd(0x1400);
  // Bit5 = 0, Bit4 = 0 - 18_Bit RGB interface
  // Bit5 = 0, Bit4 = 1 - 16_Bit RGB interface
  // Bit5 = 1, Bit4 = 0 -  6_Bit RGB interface
  // Bit0 = 0, External RGB interface, Bit0 = 1, External MPU interface
  LCD_Data(0x0100);

  // RGB_POL (15h)
  LCD_Cmd(0x1500);
  // Bit5 = 1 - VSYNC output enable
  // Bit4 = 0 - Dot clock polarity sampled at rising edge
  // Bit3 = 0 - Enable polarity: active low
  LCD_Data(0x0000);

  // MEMORY_WRITE_MODE (16h)
  LCD_Cmd(0x1600);
  // Bit6  Bit5  Bit4
  // 0     0     X   - 18_bit Single transfer, 262k support
  // 0     1     X   - 16_bit Single transfer, 65k support
  // 1     0     X   - 9_bit Dual transfer, 262k support
  // 1     1     0   - 8_bit Dual transfer, 65k support
  // 1     1     1   - 8_bit Triple transfer, 262k support
  // Bit2 = 0, Horizontal address counter is decreased
  // Bit2 = 1, Horizontal address counter is increased
  // Bit1 = 0, Vertical address counter is decreased
  // Bit1 = 1, Vertical address counter is increased
  // Bit0 = 0, The data is continuously written horizontally
  // Bit0 = 1, The data is continuously written vertically
  LCD_Data(0x2600);

  // WINDOW MX1_ADDR (17h)
  LCD_Cmd(0x1700);
  LCD_Data(0x0000);

  // WINDOW MX2_ADDR (18h)
  LCD_Cmd(0x1800);
  LCD_Data(GuiConst_DISPLAY_WIDTH_HW<<8);

  // WINDOW MY1_ADDR (19h)
  LCD_Cmd(0x1900);
  LCD_Data(0x0000);

  // WINDOW MY2_ADDR (1Ah)
  LCD_Cmd(0x1A00);
  LCD_Data(GuiConst_DISPLAY_HEIGHT_HW<<8);

  // DISPLAY DUTY RATIO (28h)
  LCD_Cmd(0x2800);
  LCD_Data(0x7F00);

  // DISPLAY START LINE (29h)
  LCD_Cmd(0x2900);
  LCD_Data(0x0000);

  // D1_DDRAM_FAC (2Eh)
  // First screen display horizontal address for display
  LCD_Cmd(0x2E00);
  LCD_Data(0x0000);

  // D1_DDRAM_FAR (2Fh)
  // First screen display vertical address for display
  LCD_Cmd(0x2F00);
  LCD_Data(0x0000);

  // Clear display memory
  // DDRAM_DATA_ACCESS_PORT (22h)
  LCD_Cmd(0x2200);
  for (i = 0; i < (GuiConst_DISPLAY_BYTES/2); i++);
    LCD_Data(0x0000);

  // DISP_ON_OFF (06h)
  LCD_Cmd(0x0600);
  LCD_Data(0x0100);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S LastByte;
  GuiConst_INT16S X, Y;

  // Lock GUI resources
  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      // MEMORY_ACCESSPOINTER Y (21h)
      LCD_Cmd(0x2100);
      LCD_Data((GuiConst_INT16U)Y<<8);

      // Select start column
      // MEMORY_ACCESSPOINTER X (20h)
      LCD_Cmd(0x2000);
      LCD_Data((GuiConst_INT16U)GuiLib_DisplayRepaint[Y].ByteBegin << 8);

      LastByte = GuiConst_BYTES_PR_LINE / 2 - 1;

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      // DDRAM_DATA_ACCESS_PORT (22h)
      LCD_Cmd(0x2200);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        LCD_Data(GuiLib_DisplayBuf.Words[Y][X]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SEPS525

#ifdef LCD_CONTROLLER_TYPE_SH1101A

// ============================================================================
//
// SH1101A DISPLAY CONTROLLER
//
// This driver uses one SH1101A display controller with on-chip oscillator.
// LCD display SPI interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 132x64 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// function Spi_SendData must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

void Spi_SendData(void* LCD_DestAddress,
                  void *LCD_SrcAddress,
                  GuiConst_INT16U LCD_DataLength)
{
  // Something hardware related
}

void lcdPortWriteByte(GuiConst_INT8U A0,
                      GuiConst_INT8U v)
{
  Spi_SendData(&A0,&v,1);
}

#define lcdSetADCreverse                                     lcdPortWriteByte(0,0xA1)
#define lcdSetSequential          lcdPortWriteByte(0,0xDA);  lcdPortWriteByte(0,0x02)
#define lcdSetReverseScan                                    lcdPortWriteByte(0,0xC8)
#define lcdSelectMux(ratio)       lcdPortWriteByte(0,0xA8);  lcdPortWriteByte(0,(ratio) & 0x3F)
#define lcdSetClock(ratio)        lcdPortWriteByte(0,0xD5);  lcdPortWriteByte(0,0x50 | ((ratio) & 0x0F))
#define lcdDeselectVCom(ratio)    lcdPortWriteByte(0,0xDB);  lcdPortWriteByte(0,ratio)
#define lcdSetContrast(level)     lcdPortWriteByte(0,0x81);  lcdPortWriteByte(0,level)
#define lcdSetDCOn                lcdPortWriteByte(0,0xAD);  lcdPortWriteByte(0,0x8B)
#define lcdSetDisplayOn                                      lcdPortWriteByte(0,0xAF)
#define lcdSelectStartLine(lineno)                           lcdPortWriteByte(0,0x40 | ((lineno) & 0x3f))
#define lcdSelectPage(pageno)                                lcdPortWriteByte(0,0xb0 | ((pageno) & 0x0f))
#define lcdSelectColumnHighNibble(columnno)                  lcdPortWriteByte(0,0x10 | ((columnno >> 4) & 0x0f))
#define lcdSelectColumnLowNibble(columnno)                   lcdPortWriteByte(0,0x00 | ((columnno) & 0x0f))
#define lcdDataByte(dataval)                                 lcdPortWriteByte(1,dataval)

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  // Reset chip

  //lcdSetADCreverse;           // Optional if you don't want default value
  lcdSetSequential;
  //lcdSetReverseScan;          // Optional if you don't want default value
  //lcdSelectMux(ratio);        // Optional if you don't want default value
  //lcdSetClock(ratio);         // Optional if you don't want default value
  //lcdDeselectVCom(ratio);     // Optional if you don't want default value
  //lcdSetContrast(level);      // Optional if you don't want default value

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Select page
    lcdSelectPage(PageNo);
    // Select column
    lcdSelectColumnHighNibble(0);
    lcdSelectColumnLowNibble(0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
      lcdDataByte(0x00);
  }

  lcdSetDCOn;
  lcdSetDisplayOn;
  //lcdSelectStartLine(lineno);   // Optional if you don't want default value
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      lcdSelectPage(PageNo);

      // Select column
      lcdSelectColumnHighNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      lcdSelectColumnLowNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SH1101A

#ifdef LCD_CONTROLLER_TYPE_SH1123

// ============================================================================
//
// SH1123 DISPLAY CONTROLLER
//
// This driver uses one SH1123 display controller with on-chip oscillator.
// LCD display in SPI interface, or 8 bit parallel 8080 type interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 254x64 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at left
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 4 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) and function Spi_SendData must be altered
// to correspond to your �-controller hardware and compiler syntax.
//
// ============================================================================

#define DISP_DATA                 Pxx

#define CHIP_SELECT               Pxx.x = 0     // CS pin
#define CHIP_DESELECT             Pxx.x = 1     // CS pin
#define MODE_DATA                 Pxx.x = 1     // A0 pin
#define MODE_COMMAND              Pxx.x = 0     // A0 pin
#define WRITE_LOW                 Pxx.x = 0     // WR pin
#define WRITE_HIGH                Pxx.x = 1     // WR pin
#define READ_LOW                  Pxx.x = 0     // RD pin
#define READ_HIGH                 Pxx.x = 1     // RD pin
#define RESET_ON                  Pxx.x = 0     // RESET pin
#define RESET_OFF                 Pxx.x = 1     // RESET pin
#define NOP                       asm ("nop")   // nop code - adjust according
                                                // to your compiler syntax

void Spi_SendData(void* LCD_DestAddress,
                  void *LCD_SrcAddress,
                  GuiConst_INT16U LCD_DataLength)
{
  // Something hardware related
}

void lcdPortWriteByte(GuiConst_INT8U A0,
                      GuiConst_INT8U v)
{
  // 8 bit parallel 8080 type interface
  READ_HIGH;
  if(A0)
    MODE_DATA;
  else
    MODE_COMMAND;
  CHIP_SELECT;
  WRITE_LOW;
  DISP_DATA = v;
  //NOP;         // Maybe required for microcontrollers with instruction
                 // cycle < 100ns
  WRITE_HIGH;
  CHIP_DESELECT;

  // Spi interface
  //Spi_SendData(&A0,&v,1);
}

#define lcdSelectColumnHighNibble(columnno)                  lcdPortWriteByte(0,0x10 | ((columnno >> 4) & 0x07))
#define lcdSelectColumnLowNibble(columnno)                   lcdPortWriteByte(0,0x00 | ((columnno) & 0x0f))
#define lcdSelectStartLine(lineno)                           lcdPortWriteByte(0,0x40 | ((lineno) & 0x3f))
#define lcdSetContrast(level)     lcdPortWriteByte(0,0x81);  lcdPortWriteByte(0,level)
#define lcdSetADCreverse                                     lcdPortWriteByte(0,0xA1)
#define lcdSetReverseDisplay                                 lcdPortWriteByte(0,0xA7)
#define lcdSelectMux(ratio)       lcdPortWriteByte(0,0xA8);  lcdPortWriteByte(0,(ratio & 0x3F))
#define lcdSetDCOn(freq)          lcdPortWriteByte(0,0xAD);  lcdPortWriteByte(0,0x81 | ((freq<<1) & 0x0E))
#define lcdSetDisplayOn                                      lcdPortWriteByte(0,0xAF)
#define lcdSelectLine(line)       lcdPortWriteByte(0,0xB0);  lcdPortWriteByte(0,(line & 0x3f))
#define lcdSetReverseScan                                    lcdPortWriteByte(0,0xC8)
#define lcdSetClock(ratio)        lcdPortWriteByte(0,0xD5);  lcdPortWriteByte(0,0x50 | ((ratio) & 0x0F))
#define lcdSetSequential          lcdPortWriteByte(0,0xDA);  lcdPortWriteByte(0,0x02)
#define lcdDeselectVCom(ratio)    lcdPortWriteByte(0,0xDB);  lcdPortWriteByte(0,ratio)
#define lcdDataByte(dataval)                                 lcdPortWriteByte(1,dataval)

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S LineNo;
  GuiConst_INT16S X;

  // Reset chip
  RESET_ON;
  RESET_OFF;

  //lcdSetADCreverse;           // Optional if you don't want default value
  lcdSetSequential;
  //lcdSetReverseScan;          // Optional if you don't want default value
  //lcdSelectMux(ratio);        // Optional if you don't want default value
  //lcdSetClock(ratio);         // Optional if you don't want default value
  //lcdDeselectVCom(ratio);     // Optional if you don't want default value
  //lcdSetContrast(level);      // Optional if you don't want default value

  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++) // Loop through pages
  {
    // Select line
    lcdSelectLine(LineNo);
    // Select column
    lcdSelectColumnHighNibble(0);
    lcdSelectColumnLowNibble(0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
      lcdDataByte(0x00);
  }

  lcdSetDCOn(0);
  lcdSetDisplayOn;
  //lcdSelectStartLine(lineno);   // Optional if you don't want default value
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S LineNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select line
      lcdSelectLine(LineNo);

      // Select column
      lcdSelectColumnHighNibble(GuiLib_DisplayRepaint[LineNo].ByteBegin);
      lcdSelectColumnLowNibble(GuiLib_DisplayRepaint[LineNo].ByteBegin);

      // Calculate last byte
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[LineNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[LineNo].ByteEnd;

      // Write display data
      for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin; N <= LastByte; N++)
        lcdDataByte(GuiLib_DisplayBuf[LineNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SH1123

#ifdef LCD_CONTROLLER_TYPE_SPFD5408

// ============================================================================
//
// SPFD5408 DISPLAY CONTROLLER
//
// This driver uses one SPFD5408 display controller with on-chip oscillator.
// LCD display in 8 or 16 bit 80-system parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x320 pixels.
//
// Compatible display controllers:
//   R61505, ILI9320
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 or 18 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Uncomment it for ILI9320 driver
// #define ILI9320

// Hardwired connection
// IM3 = 0; IM2 = 0, IM1 = 1, IM0 = 0: 80-system 16 bit interface
// IM3 = 0; IM2 = 0, IM1 = 1, IM0 = 1: 80-system 8 bit interface
// For 8 bit interface connect unused pins D8-D1 to 0 or 1

                                           // PIN D17 - D10 for 8 bit interface
#define DISP_DATA            Pxx           // PIN D17 - D10, D8-D1

#define RESET_ON             Pxx.x = 0     // RESET pin
#define RESET_OFF            Pxx.x = 1     // RESET pin
#define CHIP_SELECT          Pxx.x = 0     // CS pin
#define CHIP_DESELECT        Pxx.x = 1     // CS pin
#define MODE_DATA            Pxx.x = 1     // RS pin
#define MODE_COMMAND         Pxx.x = 0     // RS pin
#define MODE_WRITE_OFF       Pxx.x = 1     // WR pin
#define MODE_WRITE_ON        Pxx.x = 0     // WR pin
#define MODE_READ_OFF        Pxx.x = 1     // RD pin
#define MODE_READ_ON         Pxx.x = 0     // RD pin
#define NOP                  asm ("nop")   // nop code - adjust according to
                                           // your compiler syntax

// Control Register Addresses to load into SPFD5408 Index register
#define ID_READ              0x0000
#define DRIVER_CONTROL       0x0001
#define LCD_AC_CONTROL       0x0002
#define ENTRY_MODE           0x0003
#define RESIZING_CONTROL     0x0004
#define DISPLAY_CONTROL_1    0x0007
#define DISPLAY_CONTROL_2    0x0008
#define DISPLAY_CONTROL_3    0x0009
#define DISPLAY_CONTROL_4    0x000A
#define EXT_INTERFACE_1      0x000C
#define FRAME_MAKER_POSITION 0x000D
#define EXT_INTERFACE_2      0x000F
#define POWER_CONTROL_1      0x0010
#define POWER_CONTROL_2      0x0011
#define POWER_CONTROL_3      0x0012
#define POWER_CONTROL_4      0x0013
#define POWER_CONTROL_5      0x0017
#define HORIZONTAL_ADDRESS   0x0020
#define VERTICAL_ADDRESS     0x0021
#define GRAM_DATA            0x0022
#ifdef ILI9320
#define POWER_CONTROL_7      0x0029
#define FRAME_COLOR_CONTROL  0x002B
#else
#define NVM_READ_DATA_1      0x0028
#define NVM_READ_DATA_2      0x0029
#define NVM_READ_DATA_3      0x002A
#endif
#define FRAME_COLOR_CONTROL  0x002B
#define HORIZONTAL_START_ADR 0x0050
#define HORIZONTAL_END_ADR   0x0051
#define VERTICAL_START_ADR   0x0052
#define VERTICAL_END_ADR     0x0053
#define DRIVER_OUT_CONTROL   0x0060
#define IMAGE_CONTROL        0x0061
#define SCROLL_CONTROL       0x006A
#define DISPLAY_POSITION_1   0x0080
#define GRAM_START_LINE_1    0x0081
#define GRAM_END_LINE_1      0x0082
#define DISPLAY_POSITION_2   0x0083
#define GRAM_START_LINE_2    0x0084
#define GRAM_END_LINE_2      0X0085
#define FRAME_CYCLE_CONTROL  0X0090
#define PANEL_CONTROL_2      0X0092
#define PANEL_CONTROL_3      0X0093
#define FRAME_CYCLE_CONTROL2 0X0095
#define PANEL_CONTROL_5      0X0097
#ifdef ILI9320
#define PANEL_CONTROL_6      0X0098
#else
#define PANEL_CONTROL        0X0098
#define NVM_CONTROL_1        0x00A0
#define NVM_CONTROL_2        0x00A1
#endif

/*
// Command writing for 16bit interface
#define LCD_Cmd(Val)                                                     \
{                                                                        \
  MODE_COMMAND;                                                          \
  DISP_DATA = Val;                                                       \
  CHIP_SELECT;                                                           \
  MODE_WRITE_ON;                                                         \
  MODE_WRITE_OFF;                                                        \
  CHIP_DESELECT;                                                         \
}

// Data writing for 16 bit interface
#define LCD_Data(Val)                                                    \
{                                                                        \
  MODE_DATA;                                                             \
  DISP_DATA = Val;                                                       \
  CHIP_SELECT;                                                           \
  MODE_WRITE_ON;                                                         \
  MODE_WRITE_OFF;                                                        \
  CHIP_DESELECT;                                                         \
}
*/

// Command writing for 8 bit interface
void LCD_Cmd(GuiConst_INT16U Val)
{
  MODE_COMMAND;
  DISP_DATA = (GuiConst_INT8U)(Val>>8);
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  NOP;   // Some delay

  DISP_DATA = (GuiConst_INT8U)(Val);
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

// Data writing for 8 bit interface
void LCD_Data(GuiConst_INT16U Val)
{
  MODE_DATA;
  DISP_DATA = (GuiConst_INT8U)(Val>>8);
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  NOP;   // Some delay

  DISP_DATA = (GuiConst_INT8U)(Val);
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

/*
// Memory mapped example

// LCD data bus and control bus pointers
#define LCD_DATA_BUS (*(GuiConst_INT16U*)0x0x80000001)
#define LCD_CMD_BUS  (*(GuiConst_INT16U*)0x0x80000000)

// Command writing
#define LCD_Cmd(Val)                                                     \
{                                                                        \
  LCD_CMD_BUS = Val;                                                     \
}

// Data writing
#define LCD_Data(Val)                                                    \
{                                                                        \
  LCD_DATA_BUS = Val;                                                    \
}
*/

void LCD_CLS(GuiConst_INT16U color)
{
  GuiConst_INT32U j;

#ifdef GuiConst_COLOR_DEPTH_1
  for (j = 0; j < (GuiConst_DISPLAY_BYTES*8); j++)
#else
#ifdef GuiConst_COLOR_DEPTH_4
  for (j = 0; j < (GuiConst_DISPLAY_BYTES*2); j++)
#else
  for (j = 0; j < GuiConst_DISPLAY_BYTES; j++)
#endif
#endif
  {
    LCD_Data(color);
  }
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void GuiDisplay_Init(void)
{
  MODE_READ_OFF;  // We don't use read signal

  // Start screen initialisation
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;

  // Data transfer synchronisation only in 8 bit interface
  // For 16 bit interface comment it
  LCD_Cmd(0x0000);
  LCD_Cmd(0x0000);

#ifdef ILI9320
  //Start oscillator
  LCD_Cmd(ID_READ);
  LCD_Data(0x9321);
#endif

  LCD_Cmd(DRIVER_CONTROL);
  LCD_Data(0x0000);

  LCD_Cmd(LCD_AC_CONTROL);
#ifdef ILI9320
  LCD_Data(0x0400);
#else
  LCD_Data(0x0701);
#endif

  LCD_Cmd(ENTRY_MODE);
  LCD_Data(0x0030);

  LCD_Cmd(RESIZING_CONTROL);
  LCD_Data(0x0000);

  LCD_Cmd(DISPLAY_CONTROL_2);
  LCD_Data(0x0207);

  LCD_Cmd(DISPLAY_CONTROL_3);
  LCD_Data(0x0000);

  LCD_Cmd(DISPLAY_CONTROL_4);
  LCD_Data(0x0000);

  LCD_Cmd(EXT_INTERFACE_1);
  LCD_Data(0x0000);

  LCD_Cmd(FRAME_MAKER_POSITION);
  LCD_Data(0x0000);

  LCD_Cmd(EXT_INTERFACE_2);
  LCD_Data(0x0000);

  LCD_Cmd(DISPLAY_CONTROL_1);
  LCD_Data(0x0101);

  LCD_Cmd(POWER_CONTROL_1);
  LCD_Data(0x10B0);

  LCD_Cmd(POWER_CONTROL_2);
  LCD_Data(0x0007);

  LCD_Cmd(POWER_CONTROL_5);
  LCD_Data(0x0001);

  LCD_Cmd(POWER_CONTROL_3);
#ifdef ILI9320
  LCD_Data(0x011B);
#else
  LCD_Data(0x013B);
#endif

  LCD_Cmd(POWER_CONTROL_4);
  LCD_Data(0x0B00);

#ifdef ILI9320
  LCD_Cmd(POWER_CONTROL_7);
  LCD_Data(0x000F);

  LCD_Cmd(FRAME_COLOR_CONTROL);
  LCD_Data(0x0000);
#else
  LCD_Cmd(NVM_READ_DATA_2);
  LCD_Data(0x0012);

  LCD_Cmd(NVM_READ_DATA_3);
  LCD_Data(0x0095);
#endif

  LCD_Cmd(HORIZONTAL_START_ADR);
  LCD_Data(0x0000);

  LCD_Cmd(HORIZONTAL_END_ADR);
  LCD_Data(0x00EF);

  LCD_Cmd(VERTICAL_START_ADR);
  LCD_Data(0x0000);

  LCD_Cmd(VERTICAL_END_ADR);
  LCD_Data(0x013F);

  LCD_Cmd(DRIVER_OUT_CONTROL);
  LCD_Data(0x2700);

  LCD_Cmd(IMAGE_CONTROL);
  LCD_Data(0x0001);

  LCD_Cmd(SCROLL_CONTROL);
  LCD_Data(0x0000);

  LCD_Cmd(DISPLAY_POSITION_1);
  LCD_Data(0x0000);

  LCD_Cmd(GRAM_START_LINE_1);
  LCD_Data(0x0000);

  LCD_Cmd(GRAM_END_LINE_1);
  LCD_Data(0x0000);

  LCD_Cmd(DISPLAY_POSITION_2);
  LCD_Data(0x0000);

  LCD_Cmd(GRAM_START_LINE_2);
  LCD_Data(0x0000);

  LCD_Cmd(GRAM_END_LINE_2);
  LCD_Data(0x0000);

  LCD_Cmd(FRAME_CYCLE_CONTROL);
  LCD_Data(0x0013);

  LCD_Cmd(PANEL_CONTROL_2 );
  LCD_Data(0x0000);

  LCD_Cmd(PANEL_CONTROL_3);
  LCD_Data(0x0003);

  LCD_Cmd(FRAME_CYCLE_CONTROL2);
  LCD_Data(0x0110);

  LCD_Cmd(PANEL_CONTROL_5);
  LCD_Data(0x0000);

#ifdef ILI9320
  LCD_Cmd(PANEL_CONTROL_6);
#else
  LCD_Cmd(PANEL_CONTROL);
#endif
  LCD_Data(0x0000);

  LCD_Cmd(DISPLAY_CONTROL_1);
#ifdef ILI9320
  LCD_Data(0x0133);
#else
  LCD_Data(0x0173);
#endif

  // Initialize with black color
  LCD_Cmd(GRAM_DATA);
  LCD_CLS(0x0000);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;
#ifdef GuiConst_COLOR_DEPTH_1
  GuiConst_INT8U OneByte, i;
#endif
#ifdef GuiConst_COLOR_DEPTH_4
  GuiConst_INT8U PaletteIndex, ColorR, ColorG, ColorB;
#endif

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(VERTICAL_ADDRESS);
      LCD_Data(Y);

      // Select start column
      LCD_Cmd(HORIZONTAL_ADDRESS);
#ifdef GuiConst_COLOR_DEPTH_1
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin * 8);
      LastByte = GuiConst_BYTES_PR_LINE - 1;
#else
#ifdef GuiConst_COLOR_DEPTH_4
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin * 2);
      LastByte = GuiConst_BYTES_PR_LINE - 1;
#else
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin);
      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
#endif
#endif

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      LCD_Cmd(GRAM_DATA);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
#ifdef GuiConst_COLOR_DEPTH_1
        // for bit 0 at right
        OneByte = GuiLib_DisplayBuf[Y][X];

        for (i = 0; i < 8; i++)
        {
          if (OneByte & 0x80)
            LCD_Data(0x0FFFF);
          else
            LCD_Data(0x0000);

          OneByte <<= 1;
        }
#else
#ifdef GuiConst_COLOR_DEPTH_4
        // for bit 0 at right
        PaletteIndex = GuiLib_DisplayBuf[Y][X]>>4;
        ColorR = GuiStruct_Palette[PaletteIndex][0] & 0xF8;
        ColorG = GuiStruct_Palette[PaletteIndex][1] & 0xFC;
        ColorB = GuiStruct_Palette[PaletteIndex][2] & 0xF8;
        LCD_Data(((GuiConst_INT16U)ColorR << 8) |
           ((GuiConst_INT16U)ColorG << 3) |
           ((GuiConst_INT16U)ColorB >> 3));

        PaletteIndex = GuiLib_DisplayBuf[Y][X] & 0x0F;
        ColorR = GuiStruct_Palette[PaletteIndex][0] & 0xF8;
        ColorG = GuiStruct_Palette[PaletteIndex][1] & 0xFC;
        ColorB = GuiStruct_Palette[PaletteIndex][2] & 0xF8;
        LCD_Data(((GuiConst_INT16U)ColorR << 8) |
           ((GuiConst_INT16U)ColorG << 3) |
           ((GuiConst_INT16U)ColorB >> 3));
#else
        LCD_Data(GuiLib_DisplayBuf.Words[Y][X]);
#endif
#endif
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SPFD5408

#ifdef LCD_CONTROLLER_TYPE_SPLC502

// ============================================================================
//
// SPLC502 DISPLAY CONTROLLER
//
// This driver uses one SPLC502 display controller with on-chip oscillator.
// LCD display 8 bit pararel interface - MPU 68000 series type.
// Modify driver accordingly for other interface types.
// Graphic modes up to 132x65 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define RESET_ON   PORTG &= ~0x02
#define RESET_OFF  PORTG |=  0x02
#define CS1_LOW    PORTG &= ~0x04
#define CS1_HIGH   PORTG |=  0x04
#define E_LOW      PORTG &= ~0x08
#define E_HIGH     PORTG |=  0x08
#define A0_LOW     PORTG &= ~0x20
#define A0_HIGH    PORTG |=  0x20
#define RW_LOW     PORTG &= ~0x40
#define RW_HIGH    PORTG |=  0x40
#define DB         PORTA

void lcdPortWriteByte(GuiConst_INT8U A0, GuiConst_INT8U Val)
{
  CS1_LOW;
  RW_LOW;
  if(A0)
    A0_HIGH;
  else
    A0_LOW;
  E_HIGH;
  DB = Val;
  E_LOW;
  CS1_HIGH;
}

#define lcdReset                                          lcdPortWriteByte(0,0xe2)
#define lcdV5ResistorRatio(ratio)                         lcdPortWriteByte(0,0x20 | ((ratio) & 0x07))
#define lcdPowerController(booster, vreg, vfollow)        lcdPortWriteByte(0,0x28 | ((booster) ? 4 : 0) | ((vreg) ? 2 : 0) | ((vfollow) ? 1 : 0))
#define lcdElectronicVol(level) lcdPortWriteByte(0,0x81); lcdPortWriteByte(0,(level) & 0x3f)
#define lcdDrivingMode(level)   lcdPortWriteByte(0,0xD2); lcdPortWriteByte(0,(level<<6))
#define lcdSelectLCDBias(ratio)                           lcdPortWriteByte(0,0xA2 | ((ratio) & 0x01))
#define lcdDisplaySw(onOff)                               lcdPortWriteByte(0,0xae | ((onOff) ? 1 : 0))
#define lcdSelectStartLine(lineno)                        lcdPortWriteByte(0,0x40 | ((lineno) & 0x3f))
#define lcdSelectPage(pageno)                             lcdPortWriteByte(0,0xb0 | ((pageno) & 0x0f))
#define lcdSelectColumnHighNibble(columnno)               lcdPortWriteByte(0,0x10 | ((columnno >> 4) & 0x0f))
#define lcdSelectColumnLowNibble(columnno)                lcdPortWriteByte(0,0x00 | ((columnno) & 0x0f))
#define lcdDataByte(dataval)                              lcdPortWriteByte(1,dataval)

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  // Reset controller
  lcdReset;
  lcdV5ResistorRatio(4);
  lcdPowerController(1,1,1);
  lcdElectronicVol(0x24);

  // Display start line in RAM is 000000b
  lcdSelectStartLine(0);

  // Clear display memory
  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Select page
    lcdSelectPage(PageNo);
    // Select column
    lcdSelectColumnHighNibble(0);
    lcdSelectColumnLowNibble(0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
      lcdDataByte(0x00);
  }

  lcdDisplaySw(1);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      lcdSelectPage(PageNo);

      // Select column
      lcdSelectColumnHighNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      lcdSelectColumnLowNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SPLC502

#ifdef LCD_CONTROLLER_TYPE_SSD0323

// ============================================================================
//
// SSD0323 DISPLAY CONTROLLER
//
// This driver uses one SSD032 display controller with on-chip oscillator.
// OLED/PLED display 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 128x80 pixels.
//
// Compatible display controllers:
//   SSD01303, SSD1325
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 4 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define DISP_DATA                 P1

#define CHIP_SELECT               P0.1 = 0     // CS pin
#define CHIP_DESELECT             P0.1 = 1     // CS pin
#define CHIP_RESET                P0.2 = 0     // RES pin
#define CHIP_RUN                  P0.2 = 1     // RES pin
#define MODE_DATA                 P0.3 = 1     // D/C pin
#define MODE_COMMAND              P0.3 = 0     // D/C pin
// for 68000 interface mode
#define MODE_READ                 P0.4 = 1     // R/W pin for 68000 mode
#define MODE_WRITE                P0.4 = 0     // R/W pin for 68000 mode
#define RW_ENABLED                P0.5 = 1     // E pin for 68000 mode
#define RW_DISABLED               P0.5 = 0     // E pin for 68000 mode
// for 8080 interface mode
#define WRITE_OFF                 P0.4 = 1     // WR pin for 8080 mode
#define WRITE_ON                  P0.4 = 0     // WR pin for 8080 mode
#define READ_OFF                  P0.5 = 1     // RD pin for 8080 mode
#define READ_ON                   P0.5 = 0     // RD pin for 8080 mode

#define CMD_COLUMN_ADDR           0x15
#define CMD_ROW_ADDR              0x75
#define CMD_CONTRAST              0x81
#define CMD_CURRENT_RANGE_QUARTER 0x84
#define CMD_CURRENT_RANGE_HALF    0x85
#define CMD_CURRENT_RANGE_FULL    0x86
#define CMD_REMAP                 0xA0
#define CMD_DISPLAY_START_LINE    0xA1
#define CMD_DISPLAY_OFFSET        0xA2
#define CMD_DISPLAY_MODE_NORMAL   0xA4
#define CMD_DISPLAY_MODE_ALL_ON   0xA5
#define CMD_DISPLAY_MODE_ALL_OFF  0xA6
#define CMD_DISPLAY_MODE_INVERSE  0xA7
#define CMD_MULTIPLEX_RATIO       0xA8
#define CMD_MASTER_CONFIG         0xAD
#define CMD_DISPLAY_OFF           0xAE
#define CMD_DISPLAY_ON            0xAF
#define CMD_PRECHARGE_ENABLE      0xB0
#define CMD_PRECHARGE_LEVEL       0xB4
#define CMD_SEGMENT_LOW_VOLT      0xBF
#define CMD_VCOMH_VOLT            0xBE
#define CMD_PRECHARGE_VOLT        0xBC
#define CMD_PHASE_LENGTH          0xB1
#define CMD_ROW_PERIOD            0xB2
#define CMD_DIVIDE_OSCILL_CLOCK   0xB3
#define CMD_GRAY_SCALE            0xB8
#define CMD_BIASING_CURRENT       0xCF

/* Commented the following lines out using C-style comment block to prevent a
   warning about a comment within a commment due to the '\' character
 
// Command writing for 68000 mode
//#define WRITE_COMMAND(Val)                                               \
//{                                                                        \
//  MODE_COMMAND;                                                          \
//  MODE_WRITE;                                                            \
//  RW_ENABLED;                                                            \
//  CHIP_SELECT;                                                           \
//  DISP_DATA = Val;                                                       \
//  CHIP_DESELECT;                                                         \
//  RW_DISABLED;                                                           \
//}

// Data writing for 68000 mode
//#define WRITE_DATA(Val)                                                  \
//{                                                                        \
//  MODE_DATA;                                                             \
//  MODE_WRITE;                                                            \
//  RW_ENABLED;                                                            \
//  CHIP_SELECT;                                                           \
//  DISP_DATA = Val;                                                       \
//  CHIP_DESELECT;                                                         \
//  RW_DISABLED;                                                           \
//}
*/

// Command writing for 8080 mode
#define WRITE_COMMAND(Val)                                               \
{                                                                        \
  READ_OFF;                                                              \
  MODE_COMMAND;                                                          \
  CHIP_SELECT;                                                           \
  WRITE_ON;                                                              \
  DISP_DATA = Val;                                                       \
  WRITE_OFF                                                              \
  CHIP_DESELECT;                                                         \
}

// Data writing for 8080 mode
#define WRITE_DATA(Val)                                                  \
{                                                                        \
  READ_OFF;                                                              \
  MODE_DATA;                                                             \
  CHIP_SELECT;                                                           \
  WRITE_ON;                                                              \
  DISP_DATA = Val;                                                       \
  WRITE_OFF;                                                             \
  CHIP_DESELECT;                                                         \
}

// for spi interface
//
//void WriteCmd(GuiConst_INT8U Val)
//{
//  MODE_COMMAND;
//  CHIP_SELECT;
//  // Something hardware related
//  CHIP_DESELECT;
//}

//void WriteData(GuiConst_INT8U Val)
//{
//  MODE_DATA;
//  CHIP_SELECT;
//  // Something hardware related
//  CHIP_DESELECT;
//}

//#define WRITE_COMMAND(Val)                            WriteCmd(Val)
//#define WRITE_DATA(Val)                               WriteData(Val)


// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S X;

  // Reset controller
  CHIP_RESET;
  CHIP_RUN;

  // Column address
  WRITE_COMMAND(CMD_COLUMN_ADDR);
  WRITE_COMMAND(0x00);
  WRITE_COMMAND((GuiConst_DISPLAY_WIDTH_HW>>1)-1);

  // Row address
  WRITE_COMMAND(CMD_ROW_ADDR);
  WRITE_COMMAND(0x00);
  WRITE_COMMAND(GuiConst_DISPLAY_HEIGHT_HW-1);

  // Contrast
  WRITE_COMMAND(CMD_CONTRAST);
  WRITE_COMMAND(0x3F);   // Adjust as necessary

  // Current range
  WRITE_COMMAND(CMD_CURRENT_RANGE_FULL);

  // Re-map
  WRITE_COMMAND(CMD_REMAP);
  WRITE_COMMAND(0x52);   // Set coordinates (0,0) in upper-left corner

  // Display start line
  WRITE_COMMAND(CMD_DISPLAY_START_LINE);
  WRITE_COMMAND(0x00);

  // Display offset
  WRITE_COMMAND(CMD_DISPLAY_OFFSET);
  WRITE_COMMAND(0x40);   // Used with Re-map of 0x52

  // Display mode
  WRITE_COMMAND(CMD_DISPLAY_MODE_NORMAL);

  // Multiplex ratio
  WRITE_COMMAND(CMD_MULTIPLEX_RATIO);
  WRITE_COMMAND(0x4F);

  // Phase length
  WRITE_COMMAND(CMD_PHASE_LENGTH);
  WRITE_COMMAND(0x53);

  // Row period
  WRITE_COMMAND(CMD_ROW_PERIOD);
  WRITE_COMMAND(0x25);

  // Display clock divide
  WRITE_COMMAND(CMD_DIVIDE_OSCILL_CLOCK);
  WRITE_COMMAND(0x02);

  // VSL
  WRITE_COMMAND(CMD_SEGMENT_LOW_VOLT);
  WRITE_COMMAND(0x0E);

  // CCOMH
  WRITE_COMMAND(CMD_VCOMH_VOLT);
  WRITE_COMMAND(0x11);

  // VP
  WRITE_COMMAND(CMD_PRECHARGE_VOLT);
  WRITE_COMMAND(0x18);

  // Gamma
  WRITE_COMMAND(CMD_GRAY_SCALE);
  WRITE_COMMAND(0x01);
  WRITE_COMMAND(0x11);
  WRITE_COMMAND(0x11);
  WRITE_COMMAND(0x11);
  WRITE_COMMAND(0x11);
  WRITE_COMMAND(0x11);
  WRITE_COMMAND(0x11);
  WRITE_COMMAND(0x11);

  // DC-DC
  WRITE_COMMAND(CMD_MASTER_CONFIG);
  WRITE_COMMAND(0x03);

  // Display ON
  WRITE_COMMAND(CMD_DISPLAY_ON);

  // Clear display memory
  WRITE_COMMAND(CMD_COLUMN_ADDR);
  WRITE_COMMAND(0x00);
  WRITE_COMMAND((GuiConst_DISPLAY_WIDTH_HW>>1)-1);
  WRITE_COMMAND(CMD_ROW_ADDR);
  WRITE_COMMAND(0x00);
  WRITE_COMMAND(GuiConst_DISPLAY_HEIGHT_HW-1);
  for (X = 0; X < GuiConst_DISPLAY_BYTES; X++)   // Loop through bytes
    WRITE_DATA(0x00);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S Y;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      WRITE_COMMAND(CMD_ROW_ADDR);
      WRITE_COMMAND(Y);
      WRITE_COMMAND(GuiConst_DISPLAY_HEIGHT_HW-1);
      // Select column
      WRITE_COMMAND(CMD_COLUMN_ADDR);
      WRITE_COMMAND(GuiLib_DisplayRepaint[Y].ByteBegin);
      WRITE_COMMAND((GuiConst_DISPLAY_WIDTH_HW>>1)-1);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;
      for (N = GuiLib_DisplayRepaint[Y].ByteBegin; N <= LastByte; N++)
        WRITE_DATA(GuiLib_DisplayBuf[Y][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SSD0323

#ifdef LCD_CONTROLLER_TYPE_SSD1289

// ============================================================================
//
// SSD1289 DISPLAY CONTROLLER
//
// This driver uses one SSD1289 display controller with on-chip oscillator.
// LCD display 8 bit 6800 parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x320 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define DISP_DATA            Pxx           // PIN D1 - D8

#define CHIP_SELECT          Pxx.x = 0     // CS pin
#define CHIP_DESELECT        Pxx.x = 1     // CS pin
#define MODE_DATA            Pxx.x = 1     // DC pin
#define MODE_COMMAND         Pxx.x = 0     // DC pin
#define MODE_READ            Pxx.x = 1     // RW_WR pin
#define MODE_WRITE           Pxx.x = 0     // RW_WR pin
#define RW_ENABLED           Pxx.x = 1     // E_RD pin
#define RW_DISABLED          Pxx.x = 0     // E_RD pin

#define X_START              0
#define Y_START              0
#define X_END                239
#define Y_END                319
#define OSC_START            0x00
#define DRIVER_OUT           0x01
#define LCD_DRIVE_AC         0x02
#define POWER_CONTROL1       0x03
#define POWER_CONTROL2       0x0C
#define POWER_CONTROL3       0x0D
#define POWER_CONTROL4       0x0E
#define POWER_CONTROL5       0x1E
#define DISPLAY_CONTROL      0x07
#define ENTRY_MODE           0x11
#define SLEEP_MODE           0x10
#define HORIZONTAL_RAM_START 0x44
#define VERTICAL_RAM_START   0x45
#define VERTICAL_RAM_END     0x46
#define RAM_WRITE            0x22
#define LCD_X                0x4E
#define LCD_Y                0x4F

// Command writing
#define LCD_Cmd(Val)                                                     \
{                                                                        \
  MODE_COMMAND;                                                          \
  MODE_WRITE;                                                            \
  RW_ENABLED;                                                            \
  CHIP_SELECT;                                                           \
  DISP_DATA = Val;                                                       \
  CHIP_DESELECT;                                                         \
  RW_DISABLED;                                                           \
}

// Data writing
#define LCD_Data(Val)                                                    \
{                                                                        \
  MODE_DATA;                                                             \
  MODE_WRITE;                                                            \
  RW_ENABLED;                                                            \
  CHIP_SELECT;                                                           \
  DISP_DATA = Val;                                                       \
  CHIP_DESELECT;                                                         \
  RW_DISABLED;                                                           \
}

void LCD_CLS(GuiConst_INT16U color)
{
  GuiConst_INT16U j;

  for(j=0;j<GuiConst_DISPLAY_BYTES;j++)
  {
    LCD_Data((GuiConst_INT8U)(color>>8));
    LCD_Data((GuiConst_INT8U)color);
  }
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void GuiDisplay_Init(void)
{
  // Start screen initialisation

  LCD_Cmd(OSC_START);              // Start Oscillation
  LCD_Data(0x00);
  LCD_Data(0x01);

  LCD_Cmd(DRIVER_OUT);             // Driver Output Control
  LCD_Data(0x23);
  LCD_Data(0x3F);

  LCD_Cmd(LCD_DRIVE_AC);           // LCD Drive AC Control
  LCD_Data(0x06);
  LCD_Data(0x00);

  LCD_Cmd(POWER_CONTROL1);         // Power Control (1)
  LCD_Data(0xA8);
  LCD_Data(0xA6);

  LCD_Cmd(DISPLAY_CONTROL);        // Display Control
  LCD_Data(0x00);
  LCD_Data(0x33);

  LCD_Cmd(ENTRY_MODE);             // Set Display color mode for 65k color
  LCD_Data(0x68);
  LCD_Data(0x30);

  LCD_Cmd(POWER_CONTROL2);         // Power Control (2)
  LCD_Data(0x00);
  LCD_Data(0x05);

  LCD_Cmd(POWER_CONTROL3);         // Power Control (3)
  LCD_Data(0x30);
  LCD_Data(0x0B);

  LCD_Cmd(POWER_CONTROL4);         // Power Control (4)
  LCD_Data(0x20);
  LCD_Data(0x00);

  LCD_Cmd(SLEEP_MODE);             // Exit Sleep Mode
  LCD_Data(0x00);
  LCD_Data(0x00);
  millisecond_delay(30);                    // Delay 30ms

  LCD_Cmd(POWER_CONTROL5);         // Power Control (5)
  LCD_Data(0x00);
  LCD_Data(0xA8);

  LCD_Cmd(HORIZONTAL_RAM_START);   // Horizontal RAM address position start/end setup
  LCD_Data(0xEF);                  // Dcimal 239
  LCD_Data(0x00);                  // Dcimal 0, i.e. horizontal ranges from 0 -> 239
                                   // POR value is 0xEF00 anyway.
                                   // This address must be set before RAM write

  LCD_Cmd(VERTICAL_RAM_START);     // Vertical RAM address start position setting
  LCD_Data(0x00);                  // 0x0000 = decimal 0
  LCD_Data(0x00);

  LCD_Cmd(VERTICAL_RAM_END);       // Vertical RAM address end position setting
  LCD_Data(0x01);                  // 0x013F = decimal 319
  LCD_Data(0x3F);

  LCD_Cmd(RAM_WRITE);
  LCD_CLS(0xFFFF);                 // Initialize with black color (16 Bits)
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select page
      LCD_Cmd(LCD_Y);
      LCD_Data((GuiConst_INT8U)(Y>>8));
      LCD_Data((GuiConst_INT8U)Y);

      // Select start column
      LCD_Cmd(LCD_X);
      LCD_Data((GuiConst_INT8U)(GuiLib_DisplayRepaint[Y].ByteBegin >> 8));
      LCD_Data((GuiConst_INT8U)GuiLib_DisplayRepaint[Y].ByteBegin);

      LastByte = GuiConst_BYTES_PR_LINE / 2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      LCD_Cmd(RAM_WRITE);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        LCD_Data((GuiConst_INT8U)(GuiLib_DisplayBuf.Words[Y][X] >> 8));
        LCD_Data((GuiConst_INT8U)GuiLib_DisplayBuf.Words[Y][X]);
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SSD1289

#ifdef LCD_CONTROLLER_TYPE_SSD1305

// ============================================================================
//
// SSD1305 DISPLAY CONTROLLER
//
// This driver uses one SSD1305 display controller with on-chip oscillator.
// LCD display in 8 bit parallel 8080 type interface,
// Page address mode, monochrome mode.
// Modify driver accordingly for other interface types.
// Graphic modes up to 132x64 pixels.
//
// Compatible display controllers:
//   SSD1303
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// port pins must be altered to correspond to your �-controller hardware and
// compiler syntax.
//
// ============================================================================

// Uncomment for SSD1303
#define SSD1303

#define DISP_DATA                 Pxx

#define CHIP_SELECT               Pxx.x = 0     // CS pin
#define CHIP_DESELECT             Pxx.x = 1     // CS pin
#define MODE_DATA                 Pxx.x = 1     // D/C pin
#define MODE_COMMAND              Pxx.x = 0     // D/C pin
#define MODE_WRITE_OFF            Pxx.x = 1     // WR pin
#define MODE_WRITE_ON             Pxx.x = 0     // WR pin
#define MODE_READ_OFF             Pxx.x = 1     // RD pin
#define MODE_READ_ON              Pxx.x = 0     // RD pin
#define RESET_OFF                 Pxx.x = 1     // RES pin
#define RESET_ON                  Pxx.x = 0     // RES pin

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void lcdPortWriteByte(GuiConst_INT8U A0,
                      GuiConst_INT8U v)
{
  // 8 bit parallel 8080 type interface
  MODE_READ_OFF;
  if(A0)
    MODE_DATA;
  else
    MODE_COMMAND;
  CHIP_SELECT;
  MODE_WRITE_ON;
  DISP_DATA = v;
  // Maybe required for microcontrollers with instruction cycle < 100ns
//  millisecond_delay(100);
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

#define lcdSetColumnHighNibble(columnno)                       lcdPortWriteByte(0,0x10 | ((columnno >> 4) & 0x0f))
#define lcdSetColumnLowNibble(columnno)                        lcdPortWriteByte(0,0x00 | ((columnno) & 0x0f))
#define lcdSetStartLine(lineno)                                lcdPortWriteByte(0,0x40 | ((lineno) & 0x3f))
#define lcdSetContrast(level)        lcdPortWriteByte(0,0x81); lcdPortWriteByte(0,(level) & 0x3f)
#define lcdSetBrightness(level)      lcdPortWriteByte(0,0x82); lcdPortWriteByte(0,(level) & 0x3f)
#define lcdSetSegmentRemap                                     lcdPortWriteByte(0,0xA1)
#define lcdMemoryAdressMode(mode)                              lcdPortWriteByte(0,0x20 | ((mode) & 0x03))
#define lcdColumnStartEnd(Start,End) lcdPortWriteByte(0,0x21); lcdPortWriteByte(0,Start); lcdPortWriteByte(0,End);
#define lcdPageStartEnd(Start,End)   lcdPortWriteByte(0,0x22); lcdPortWriteByte(0,Start); lcdPortWriteByte(0,End);
#define lcdSetInverseDisplay                                   lcdPortWriteByte(0,0xA7)
#define lcdNormalDisplayON                                     lcdPortWriteByte(0,0xaf)
#define lcdDimDisplayON                                        lcdPortWriteByte(0,0xac)
#define lcdSetPage(pageno)                                     lcdPortWriteByte(0,0xb0 | ((pageno) & 0x0f))
#define lcdDataByte(dataval)                                   lcdPortWriteByte(1,dataval)
#ifdef SSD1303
#define lcdSetInternalDC             lcdPortWriteByte(0,0xAD); lcdPortWriteByte(0,0x8B);
#else
#define lcdSetInternalDC             lcdPortWriteByte(0,0xAD); lcdPortWriteByte(0,0x8f); lcdPortWriteByte(0,0xAF);
#endif

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  // Reset controller
  RESET_ON;
  millisecond_delay(10);
  RESET_OFF;

  // Page address mode, internal voltage converter
  lcdSetInternalDC;

  // Clear display memory
  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Set page
    lcdSetPage(PageNo);
    // Set column
    lcdSetColumnHighNibble(0);
    lcdSetColumnLowNibble(0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
      lcdDataByte(0x00);
  }

  lcdNormalDisplayON;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Set page
      lcdSetPage(PageNo);

      // Set column
      lcdSetColumnLowNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      lcdSetColumnHighNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SSD1305

#ifdef LCD_CONTROLLER_TYPE_SSD1322

// ============================================================================
//
// SSD1322 DISPLAY CONTROLLER
//
// This driver uses one SSD1322 display controller with on-chip oscillator.
// LCD display in 8 bit 6800 parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 480x128 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at left
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 4 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired setting
// BS1 BS0
// 1   1      8-bit 6800 parallel

#define DISP_DATA            Pxx           // PIN D0 - D7

#define RESET_ON             Pxx.x = 0     // RES pin
#define RESET_OFF            Pxx.x = 1     // RES pin
#define CHIP_SELECT          Pxx.x = 0     // CS pin
#define CHIP_DESELECT        Pxx.x = 1     // CS pin
#define MODE_DATA            Pxx.x = 1     // DC pin
#define MODE_COMMAND         Pxx.x = 0     // DC pin
#define MODE_READ            Pxx.x = 1     // RW_WR pin
#define MODE_WRITE           Pxx.x = 0     // RW_WR pin
#define RW_ENABLED           Pxx.x = 1     // E_RD pin
#define RW_DISABLED          Pxx.x = 0     // E_RD pin

#define ENABLE_GRAYSCALE_TAB 0x00
#define SET_COLUMN_START_END 0x15
#define RAM_WRITE            0x5C
#define SET_ROW_START_END    0x75
#define DISPLAY_CONTROL      0xA0
#define SET_START_LINE       0xA1
#define SET_DISPLAY_OFFSET   0xA2
#define SET_DISPLAY_OFF      0xA4
#define SET_NORMAL_DISPLAY   0xA6
#define SET_INVERSE_DISPLAY  0xA7
#define ENABLE_PARTIAL_MODE  0xA8
#define EXIT_PARTIAL_MODE    0xA9
#define DISPLAY_OFF          0xAE
#define DISPLAY_ON           0xAF
#define SET_RESET_PERIOD     0xB1
#define SET_CLOCK_OSC        0xB3
#define SET_PRECHARGE_PERIOD 0xB6
#define SET_GRAYSCALE_TABLE  0xB8
#define SET_DEFAULT_GRAY_TAB 0xB9
#define SET_PRECHARGE_VOLT   0xBB
#define SET_VCOMH            0xBE
#define SET_CONTRAST         0xC1
#define CONTRAST_CONTROL     0xC7
#define SET_MUX_RATIO        0xCA

// Command writing
#define LCD_Cmd(Val)                                                     \
{                                                                        \
  MODE_COMMAND;                                                          \
  MODE_WRITE;                                                            \
  CHIP_SELECT;                                                           \
  RW_ENABLED;                                                            \
  DISP_DATA = Val;                                                       \
  RW_DISABLED;                                                           \
  CHIP_DESELECT;                                                         \
}

// Data writing
#define LCD_Data(Val)                                                    \
{                                                                        \
  MODE_DATA;                                                             \
  MODE_WRITE;                                                            \
  CHIP_SELECT;                                                           \
  RW_ENABLED;                                                            \
  DISP_DATA = Val;                                                       \
  RW_DISABLED;                                                           \
  CHIP_DESELECT;                                                         \
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void GuiDisplay_Init(void)
{
  GuiConst_INT16U i;

  // Reset controler
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;

  // Controller initialization
  LCD_Cmd(DISPLAY_CONTROL);
  LCD_Data(0x04);   // Enable Nibble Re-map
  LCD_Data(0x01);   // Disable Dual COM mode

  // Load default values for grayscale table
  LCD_Cmd(SET_DEFAULT_GRAY_TAB);

  // Clear display
  LCD_Cmd(RAM_WRITE);
  for( i=0; i<GuiConst_DISPLAY_BYTES; i++)
    LCD_Data(0x00);

  // Switch display on
  LCD_Cmd(DISPLAY_ON);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(SET_ROW_START_END);
      LCD_Data((GuiConst_INT8U)Y);
      LCD_Data((GuiConst_INT8U)Y);

      // Set start and end column
      LCD_Cmd(SET_COLUMN_START_END);
      LCD_Data((GuiConst_INT8U)GuiLib_DisplayRepaint[Y].ByteBegin / 2);

      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      LCD_Data((GuiConst_INT8U)LastByte/2);

      // Align start byte to RAM column address
      if(GuiLib_DisplayRepaint[Y].ByteBegin % 2)
        GuiLib_DisplayRepaint[Y].ByteBegin--;

      // Write display data
      LCD_Cmd(RAM_WRITE);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        LCD_Data(GuiLib_DisplayBuf[Y][X]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SSD1322

#ifdef LCD_CONTROLLER_TYPE_SSD1339

// ============================================================================
//
// SSD1339 DISPLAY CONTROLLER
//
// This driver uses one SSD1339 display controller with on-chip oscillator.
// LCD display 8 bit 6800 parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 132x132 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 8 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Pins BS2,BS1,BS0 must be set to 1,0,0 for 8 bit 6800 type interface

#define DISP_DATA            Pxx           // PIN D0 - D7

#define RESET_ON             Pxx.x = 0     // RES pin
#define RESET_OFF            Pxx.x = 1     // RES pin
#define CHIP_SELECT          Pxx.x = 0     // CS pin
#define CHIP_DESELECT        Pxx.x = 1     // CS pin
#define MODE_DATA            Pxx.x = 1     // DC pin
#define MODE_COMMAND         Pxx.x = 0     // DC pin
#define MODE_READ            Pxx.x = 1     // RW_WR pin
#define MODE_WRITE           Pxx.x = 0     // RW_WR pin
#define RW_ENABLED           Pxx.x = 1     // E_RD pin
#define RW_DISABLED          Pxx.x = 0     // E_RD pin

#define COLUMN_START_ADDRESS 0x00
#define COLUMN_END_ADDRESS   0x83          // For 128 version set 0x7F
#define ROW_START_ADDRESS    0x00
#define ROW_END_ADDRESS      0x83          // For 128 version set 0x7F
#define SET_COLUMN_ADDRESS   0x15
#define SET_ROW_ADDRESS      0x75
#define RAM_WRITE            0x5C
#define DISPLAY_CONTROL      0xA0
#define SET_START_LINE       0xA1
#define SET_DISPLAY_OFFSET   0xA2
#define SET_NORMAL_DISPLAY   0xA6
#define SET_INVERSE_DISPLAY  0xA7
#define MASTER_CONTROL       0xAD
#define DISPLAY_OFF          0xAE
#define DISPLAY_ON           0xAF
#define POWER_MODE           0xB0
#define SET_RESET_PERIOD     0xB1
#define SET_CLOCK_OSC        0xB3
#define SET_LUT_TABLE        0xB8
#define SET_COLOR_VOLTAGE    0xBB
#define SET_VCOMH            0xBE
#define SET_CONTRAST         0xC1
#define CONTRAST_CONTROL     0xC7
#define SET_MUX_RATIO        0xCA
#define CLEAR_WINDOW         0x8E

// Command writing
#define LCD_Cmd(Val)                                                     \
{                                                                        \
  MODE_COMMAND;                                                          \
  MODE_WRITE;                                                            \
  RW_ENABLED;                                                            \
  CHIP_SELECT;                                                           \
  DISP_DATA = Val;                                                       \
  CHIP_DESELECT;                                                         \
  RW_DISABLED;                                                           \
}

// Data writing
#define LCD_Data(Val)                                                    \
{                                                                        \
  MODE_DATA;                                                             \
  MODE_WRITE;                                                            \
  RW_ENABLED;                                                            \
  CHIP_SELECT;                                                           \
  DISP_DATA = Val;                                                       \
  CHIP_DESELECT;                                                         \
  RW_DISABLED;                                                           \
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void GuiDisplay_Init(void)
{
  // Reset controler
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;

  // Screen initialisation
  LCD_Cmd(DISPLAY_CONTROL);
  LCD_Data(0x00);                 // Bits[A7-A6]- 00 8 bit color mode

  LCD_Cmd(MASTER_CONTROL);
  LCD_Data(0x8F);

  LCD_Cmd(POWER_MODE);
  LCD_Data(0x00);

  // Clear display
  LCD_Cmd(CLEAR_WINDOW);
  LCD_Data(COLUMN_START_ADDRESS);
  LCD_Data(ROW_START_ADDRESS);
  LCD_Data(COLUMN_END_ADDRESS);
  LCD_Data(ROW_END_ADDRESS);

  // Switch display on
  LCD_Cmd(DISPLAY_ON);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select page
      LCD_Cmd(SET_ROW_ADDRESS);
      LCD_Data((GuiConst_INT8U)Y);
      LCD_Data((GuiConst_INT8U)Y);

      // Select start column
      LCD_Cmd(SET_COLUMN_ADDRESS);
      LCD_Data((GuiConst_INT8U)GuiLib_DisplayRepaint[Y].ByteBegin);

      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      LCD_Data((GuiConst_INT8U)LastByte);

      // Write display data
      LCD_Cmd(RAM_WRITE);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        LCD_Data((GuiConst_INT8U)GuiLib_DisplayBuf[Y][X]);
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SSD1339

#ifdef LCD_CONTROLLER_TYPE_SSD1815

// ============================================================================
//
// SSD1815 DISPLAY CONTROLLER
//
// This driver uses one SSD1815 display controller with on-chip oscillator.
// LCD display SPI interface, 8 bit parallel 8080 or 6800 type interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 132x65 pixels.
//
// Compatible display controllers:
//   IST3015, KS0713, KS0715, KS0717, KS0718, KS0719, KS0723, KS0724, KS0728,
//   KS0741, KS0755, KS0759, NJU6570, NJU6575, NJU6673, NJU6675, NJU6676,
//   NJU6677, NJU6678, NJU6679, NT7501, NT7502, NT7532, NT7534, S1D10605,
//   S1D10606, S1D10607, S1D10608, S1D10609, S1D15600, S1D15601, S1D15602,
//   S1D15605, S1D15705, S1D15707, S1D15708, S1D15710, S6B0713, S6B0715,
//   S6B0716, S6B0717, S6B0718, S6B0719, S6B0721, S6B0723, S6B0724, S6B0725,
//   S6B0728, S6B0755, S6B0759, S6B1713, SED1530, SED1565, SPLC501, SSD1805,
//   SSD1852, ST7565S, ST7567, TL0313, TL0324, UC1601, UC1606
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// function Spi_SendData or port pins must be altered to correspond to your
// �-controller hardware and compiler syntax.
//
// ============================================================================

// Uncomment for your type of interface
// #define INT_SPI
// #define INT_8080
#define INT_6800

// Uncomment for SSD1815 - default
// #define SSD1815
// Uncomment for NJU6676
#define NJU6676
// Uncomment for S6B0719
// #define S6B0719

#define DISP_DATA                 Pxx

#define RESET_LOW                 Pxx.x = 0     // RES pin
#define RESET_HIGH                Pxx.x = 1     // RES pin
#define CHIP_SELECT               Pxx.x = 0     // CS1 pin
#define CHIP_DESELECT             Pxx.x = 1     // CS1 pin
#define MODE_DATA                 Pxx.x = 1     // CD pin
#define MODE_COMMAND              Pxx.x = 0     // CD pin
#define WRITE_RW_LOW              Pxx.x = 0     // WR/(RW) pin
#define WRITE_RW_HIGH             Pxx.x = 1     // WR/(RW) pin
#define READ_EN_LOW               Pxx.x = 0     // RD/(E) pin
#define READ_EN_HIGH              Pxx.x = 1     // RD/(E) pin
#define NOP                       asm ("nop")   // nop code

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

#ifdef INT_SPI
void Spi_SendData(void* LCD_DestAddress,
                  void *LCD_SrcAddress,
                  GuiConst_INT16U LCD_DataLength)
{
  // Something hardware related
}
#endif


void lcdPortWriteByte(GuiConst_INT8U A0,
                      GuiConst_INT8U v)
{
#ifdef INT_6800
  // 8bit parallel 6800 type interface
  READ_EN_LOW;
  if(A0)
    MODE_DATA;
  else
    MODE_COMMAND;
  CHIP_SELECT;
  WRITE_RW_LOW;
  DISP_DATA = v;
  READ_EN_HIGH;
  NOP;
  READ_EN_LOW;
  CHIP_DESELECT;
#endif
#ifdef INT_8080
  // 8 bit parallel 8080 type interface
  WRITE_RW_HIGH;
  READ_EN_HIGH;
  if(A0)
    MODE_DATA;
  else
    MODE_COMMAND;
  CHIP_SELECT;
  DISP_DATA = v;
  WRITE_RW_LOW;
  NOP;
  WRITE_RW_HIGH;
  CHIP_DESELECT;
#endif
#ifdef INT_SPI
  // Spi interface
  Spi_SendData(&A0,&v,1);
#endif
}


GuiConst_INT8U lcdReadStatus(void)
{
#ifdef INT_6800
  GuiConst_INT8U v;

  // 8bit parallel 6800 type interface
  READ_EN_LOW;
  MODE_COMMAND;
  CHIP_SELECT;
  WRITE_RW_HIGH;
  READ_EN_HIGH;
  NOP;
  v = DISP_DATA;
  READ_EN_LOW;
  CHIP_DESELECT;
  return v;
#endif
}

#ifdef NJU6676
#define lcdDriverOn                                        lcdPortWriteByte(0,0xe7)
#define lcdSelectStartLine(line)                           lcdPortWriteByte(0,0x40 | ((line) & 0x3f))
#endif
#ifdef  S6B0719
#define lcdV5ResistorRatio(ratio)                          lcdPortWriteByte(0,0x20 | ((ratio) & 0x07))
#define lcdSetDuty(ratio)        lcdPortWriteByte(0,0x48); lcdPortWriteByte(0,(ratio) & 0x7f)
#define lcdSetInitialCOM1(line)  lcdPortWriteByte(0,0x44); lcdPortWriteByte(0,(line) & 0x7F)
#define lcdOscilatorOn                                     lcdPortWriteByte(0,0xAB)
#define lcdSelectDC(ratio)                                 lcdPortWriteByte(0,0x64 | ((ratio) & 0x03))
#define lcdSelectStartLine(line) lcdPortWriteByte(0,0x40); lcdPortWriteByte(0,(line) & 0x7F)
#endif
#ifdef SSD1815
#define lcdV5ResistorRatio(ratio)                          lcdPortWriteByte(0,0x20 | ((ratio) & 0x07))
#define lcdSelectStartLine(line)                           lcdPortWriteByte(0,0x40 | ((line) & 0x3f))
#endif

// Common registers
#define lcdElectronicVol(level)  lcdPortWriteByte(0,0x81); lcdPortWriteByte(0,(level) & 0x3f)
#define lcdPowerController(booster, vreg, vfollow)         lcdPortWriteByte(0,0x28 | ((booster) ? 4 : 0) | ((vreg) ? 2 : 0) | ((vfollow) ? 1 : 0))
#define lcdSetADCreverse                                   lcdPortWriteByte(0,0xA1)
#define lcdDisplayOn                                       lcdPortWriteByte(0,0xaf)
#define lcdReset                                           lcdPortWriteByte(0,0xe2)
#define lcdSelectPage(page)                                lcdPortWriteByte(0,0xb0 | ((page) & 0x0f))
#define lcdSelectColumnHighNibble(column)                  lcdPortWriteByte(0,0x10 | ((column >> 4) & 0x0f))
#define lcdSelectColumnLowNibble(column)                   lcdPortWriteByte(0,0x00 | ((column) & 0x0f))
#define lcdDataByte(dataval)                               lcdPortWriteByte(1,dataval)


// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  RESET_LOW;
  millisecond_delay(1);   // some delay
  RESET_HIGH;

  // Reset controller
  lcdReset;

#ifdef NJU6676
 lcdElectronicVol(0x24);
 lcdPowerController(1,1,1);
 millisecond_delay(10);   // some delay
 lcdDriverOn;
#endif
#ifdef S6B0719
  lcdOscilatorOn;
  lcdSelectDC(1);
  lcdV5ResistorRatio(4);
  lcdElectronicVol(0x24);
  lcdPowerController(1,1,1);
#endif
#ifdef SSD1815
  lcdV5ResistorRatio(4);
  lcdElectronicVol(0x24);
  lcdPowerController(1,1,1);
#endif

  // Clear display memory
  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Select page
    lcdSelectPage(PageNo);
    // Select column
    lcdSelectColumnHighNibble(0);
    lcdSelectColumnLowNibble(0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
      lcdDataByte(0x00);
  }

  lcdDisplayOn;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      lcdSelectPage(PageNo);

      // Select column
      lcdSelectColumnHighNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      lcdSelectColumnLowNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SSD1815

#ifdef LCD_CONTROLLER_TYPE_SSD1815_2H

// ============================================================================
//
// SSD1815_2H DISPLAY CONTROLLER
//
// This driver uses two SSD1815 display controller with on-chip oscillator.
// LCD display SPI interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 264x65 pixels.
//
// Compatible display controllers:
//   IST3015, KS0713, KS0715, KS0717, KS0718, KS0719, KS0723, KS0724, KS0728,
//   KS0741, KS0755, KS0759, NJU6570, NJU6575, NJU6673, NJU6675, NJU6676,
//   NJU6677, NJU6678, NJU6679, NT7501, NT7502, NT7532, NT7534, S1D10605,
//   S1D10606, S1D10607, S1D10608, S1D10609, S1D15600, S1D15601, S1D15602,
//   S1D15605, S1D15705, S1D15707, S1D15708, S1D15710, S6B0713, S6B0715,
//   S6B0716, S6B0717, S6B0718, S6B0719, S6B0721, S6B0723, S6B0724, S6B0725,
//   S6B0728, S6B0755, S6B0759, S6B1713, SED1530, SED1565, SPLC501, SSD1805,
//   SSD1852, ST7565S, ST7567, TL0313, TL0324, UC1601, UC1606
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 2
//   Number of display controllers, vertically: 1
//
// function Spi_SendData must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define DISP_DATA                 Pxx

#define SET_CSA                   Px.y = 0      // CS-M pin
#define RESET_CSA                 Px.y = 1      // CS-M pin
#define SET_CSB                   Px.y = 0      // CS-S pin
#define RESET_CSB                 Px.y = 1      // CS-S pin
#define MODE_DATA                 Pxx.x = 1     // CD pin
#define MODE_COMMAND              Pxx.x = 0     // CD pin
#define MODE_WRITE_OFF            Pxx.x = 0     // WR0 pin (WR)
#define MODE_WRITE_ON             Pxx.x = 1     // WR0 pin (WR)
#define MODE_READ_OFF             Pxx.x = 0     // WR1 pin (RD)
#define MODE_READ_ON              Pxx.x = 1     // WR1 pin (RD)


void Spi_SendData(void* LCD_DestAddress,
                  void *LCD_SrcAddress,
                  GuiConst_INT16U LCD_DataLength)
{
  // Something hardware related
}

/* SPI interface
void lcdPortWriteByte(GuiConst_INT8U A0,
                      GuiConst_INT8U v)
{
  Spi_SendData(&A0,&v,1);
}
*/

// 8 bit parallel 8080 type interface
void lcdPortWriteByte(GuiConst_INT8U A0,
                      GuiConst_INT8U v)
{
  MODE_READ_ON;
  if (A0)
    MODE_DATA;
  else
    MODE_COMMAND;
  MODE_WRITE_OFF;
  DISP_DATA = v;
  //Delay(100);   // Maybe required for microcontrollers with instruction cycle
                  // < 100ns
  MODE_WRITE_ON;
}

#define lcdReset                                          lcdPortWriteByte(0,0xe2)
#define lcdV5ResistorRatio(ratio)                         lcdPortWriteByte(0,0x20 | ((ratio) & 0x07))
#define lcdPowerController(booster, vreg, vfollow)        lcdPortWriteByte(0,0x28 | ((booster) ? 4 : 0) | ((vreg) ? 2 : 0) | ((vfollow) ? 1 : 0))
#define lcdElectronicVol(level) lcdPortWriteByte(0,0x81); lcdPortWriteByte(0,(level) & 0x3f)
#define lcdDisplaySw(onOff)                               lcdPortWriteByte(0,0xae | ((onOff) ? 1 : 0))
#define lcdSelectStartLine(lineno)                        lcdPortWriteByte(0,0x40 | ((lineno) & 0x3f))
#define lcdSelectPage(pageno)                             lcdPortWriteByte(0,0xb0 | ((pageno) & 0x0f))
#define lcdSelectColumnHighNibble(columnno)               lcdPortWriteByte(0,0x10 | ((columnno >> 4) & 0x0f))
#define lcdSelectColumnLowNibble(columnno)                lcdPortWriteByte(0,0x00 | ((columnno) & 0x0f))
#define lcdDataByte(dataval)                              lcdPortWriteByte(1,dataval)

// SSD1815 controller selection
// Selects controller 1 or 2
void ControllerSelect (GuiConst_INT8U index)
{
  switch (index)
  {
    case 1:
      SET_CS1;
      RESET_CS2;
      break;

    case 2:
      RESET_CS1;
      SET_CS2;
      break;

    default:
      RESET_CS1;
      RESET_CS2;
  }
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT8U ContrIndex;
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  for (ContrIndex = 1; ContrIndex <= 2; ContrIndex++) // Both controllers
  {
    // Reset controller
    ControllerSelect(ContrIndex);
    lcdReset;
    lcdV5ResistorRatio(4);
    lcdPowerController(1,1,1);
    lcdElectronicVol(0x24);

    // Display start line in RAM is 000000b
    lcdSelectStartLine(0);
  }

  // Clear display memory
  for (ContrIndex = 1; ContrIndex <= 2; ContrIndex++) // Both controllers
  {
    ControllerSelect (ContrIndex);

    for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
    {
      // Select page
      lcdSelectPage(PageNo);
      // Select column
      lcdSelectColumnHighNibble(0);
      lcdSelectColumnLowNibble(0);
      // Write display data
      for (X = 0; X < GuiConst_BYTES_PR_SECTION; X++) // Loop through bytes
        lcdDataByte(0x00);
    }
  }

  for (ContrIndex = 1; ContrIndex <= 2; ContrIndex++) // Both controllers
  {
    ControllerSelect (ContrIndex);
    lcdDisplaySw(1);
  }
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      if (GuiLib_DisplayRepaint[PageNo].ByteBegin < GuiConst_BYTES_PR_SECTION)
      // Something to redraw in first section
      {
        // Select controller
        ControllerSelect (1);

        // Select page
        lcdSelectPage(PageNo);

        // Select column
        lcdSelectColumnHighNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
        lcdSelectColumnLowNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
        // Write display data
        LastByte = GuiConst_BYTES_PR_SECTION - 1;
        if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
          LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
        for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
          lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);

        // Reset repaint parameters
        if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= GuiConst_BYTES_PR_SECTION)
          // Something to redraw in second section
          GuiLib_DisplayRepaint[PageNo].ByteBegin = GuiConst_BYTES_PR_SECTION;
        else // Done with this line
          GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
      }

      if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
      // Something to redraw in second section
      {
        // Select controller
        ControllerSelect (2);

        // Select page
        lcdSelectPage(PageNo);

        // Select column
        lcdSelectColumnHighNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin -
                                  GuiConst_BYTES_PR_SECTION);
        lcdSelectColumnLowNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin -
                                 GuiConst_BYTES_PR_SECTION);

        // Write display data
        for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin;
             N <= GuiLib_DisplayRepaint[PageNo].ByteEnd;
             N++)
          lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);

        // Reset repaint parameters
        GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
      }
    }
  }

  // Finished drawing
  ControllerSelect (0);

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SSD1815_2H

#ifdef LCD_CONTROLLER_TYPE_SSD1848

// ============================================================================
//
// SSD1848 DISPLAY CONTROLLER
//
// This driver uses one SSD1848 display controller with on-chip oscillator.
// LCD display in 8 bit 8080 parallel or 6800 parallel or 4 wire serial
// interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 130x130 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Monochome or Grayscale
//   Color depth: 1 or 2 bit
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired connection
// PS0 = 0, PS1 = 1 - 4-wire SPI interface
// PS0 = 1, PS1 = 0 - 8080 parallel interface
// PS0 = 1, PS1 = 1 - 6800 parallel interface

#define PIC24F_SPI

#ifdef PIC24F_SPI
#include <p24fxxxx.h>
#endif

#ifdef PIC24F_SPI
#define RESET_ON                  PORTAbits.RA0 = 0   // RES pin
#define RESET_OFF                 PORTAbits.RA0 = 1   // RES pin
#define CHIP_ENABLED              PORTAbits.RA1 = 0   // CS pin
#define CHIP_DISABLED             PORTAbits.RA1 = 1   // CS pin
#define MODE_COMMAND              PORTAbits.RA2 = 0   // D/C pin
#define MODE_DATA                 PORTAbits.RA2 = 1   // D/C pin
#else
#define DISP_DATA                 Pxx
#define RESET_ON                  Pxx.x = 0        // RES pin
#define RESET_OFF                 Pxx.x = 1        // RES pin
#define CHIP_ENABLED              Pxx.x = 0        // CS pin
#define CHIP_DISABLED             Pxx.x = 1        // CS pin
#define MODE_COMMAND              Pxx.x = 0        // D/C pin
#define MODE_DATA                 Pxx.x = 1        // D/C pin
#define RW_WR_LOW                 Pxx.x = 0        // R/W(WR) pin
#define RW_WR_HIGH                Pxx.x = 1        // R/W(WR) pin
#define E_RD_LOW                  Pxx.x = 0        // E(RD) pin
#define E_RD_HIGH                 Pxx.x = 1        // E(RD) pin
#define NOP                       asm ("nop")      // nop code
#endif

#define COLUMN_ADDRESS            0x15
#define PAGE_ADDRESS              0x75
#define COM_SCAN_DIRECTION        0xBB
#define DATA_SCAN_GRAYSCALE       0xBC
#define DISPLAY_CONTROL           0xCA
#define POWER_CONTROL             0x20
#define CONTRAST                  0x81
#define DISPLAY_ON                0xAF
#define OSCILATOR_ON              0xD1
#define TEMPERATURE_COEF          0x82
#define WRITE_DATA                0x5C
#define BIAS_RATIO                0xFB
#define EXIT_SLEEP                0x94
#define GRAYSCALE_MODE            0xF7

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// SPI type interface starts
void Spi_SendData(GuiConst_INT8U Data)
{
  // Something hardware related - use spi interface of your microcontroller
  // Connect SCK pin of your microcontroller to D6 pin of display
  // Connect SDA pin of your microcontroller to D7 pin of display
#ifdef PIC24F_SPI
  SPI1BUF = Data;
  while (SPI1STATbits.SPITBF);
#endif
}

// 4 lines SPI interface
void LCD_Cmd(GuiConst_INT8U v)
{
  MODE_COMMAND;
  CHIP_ENABLED;
  Spi_SendData(v);
  // Remember to wait for ending of transfer
  CHIP_DISABLED;
}

void LCD_Data(GuiConst_INT8U v)
{
  MODE_DATA;
  CHIP_ENABLED;
  Spi_SendData(v);
  // Remember to wait for ending of transfer
  CHIP_DISABLED;
}
// SPI type interface ends

/*
// 8 bit parallel 8080 type interface starts
void LCD_Cmd(GuiConst_INT8U v)
{
  MODE_COMMAND;
  E_RD_HIGH;
  RW_WR_LOW;
  CHIP_ENABLED;
  DISP_DATA = v;
  //NOP;             // May be required for some microcontrollers
  CHIP_DISABLED;
  RW_WR_HIGH;
}

void LCD_Data(GuiConst_INT8U v)
{
  MODE_DATA;
  E_RD_HIGH;
  RW_WR_LOW;
  CHIP_ENABLED;
  DISP_DATA = v;
  //NOP;             // May be required for some microcontrollers
  CHIP_DISABLED;
  RW_WR_HIGH;
}
// 8 bit parallel 8080 type interface ends
*/

/*
// 8 bit parallel 6800 type interface starts
void LCD_Cmd(GuiConst_INT8U Val)
{
  MODE_COMMAND;
  RW_WR_LOW;
  E_RD_HIGH;
  CHIP_ENABLED;
  DISP_DATA = Val;
  //NOP;             // Maybe required for some microcontrollers
  CHIP_DISABLED;
  E_RD_LOW;
}

void LCD_Data(GuiConst_INT8U Val)
{
  MODE_DATA;
  RW_WR_LOW;
  E_RD_HIGH;
  CHIP_ENABLED;
  DISP_DATA = Val;
  //NOP;             // Maybe required for some microcontrollers
  CHIP_DISABLED;
  E_RD_LOW;
}
// 8 bit parallel 6800 type interface ends
*/

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

#ifdef PIC24F_SPI
  // Initialize pins RA2(D/C), RA1(CS), RA0(RES) as output(output is 0)
  TRISA &= ~0x0003;
  // 8bit master mode, SCK frequency = Input frequency /(4x8),
  // Sampling on rising edge of SCK
  // Bit12  Bit11  Bit10  Bit9 Bit8 Bit7 Bit6 Bit5  Bit4  Bit3  Bit2  Bit 1 Bit0
  // DISSCK DISSDO MODE16 SMP  CKE  SSEN CKP  MSTEN SPRE2 SPRE1 SPRE0 PPRE1 PPRE0
  // 0      0      0      0    0    0    0    1     0     0     0     1     0
  SPI1CON1 = 0x0022;
  SPI1CON2 = 0x0000;
  // Ebable SPI interface SPIEN = 1;
  SPI1STAT = 0x8000;
#endif

  RESET_ON;
  millisecond_delay(10);
  RESET_OFF;

  LCD_Cmd(OSCILATOR_ON);
  LCD_Cmd(EXIT_SLEEP);
  LCD_Cmd(POWER_CONTROL);   // Turn on the reference voltage generator, internal regulator and voltage follower; Select booster l
  LCD_Data(0x0B);
  LCD_Cmd(COM_SCAN_DIRECTION);
  LCD_Data(0x01);           // set COM scaning 0-64, 127-65
  LCD_Cmd(DISPLAY_CONTROL);
  LCD_Data(0x1F);           // 128Mux ([128 / 4] �1 = 31(decimal) / 1F(Hex))
  LCD_Cmd(BIAS_RATIO);      // Set Biasing ratio
  LCD_Data(0x00);           // 1/13
  LCD_Cmd(CONTRAST);        // Set target gain and contrast.
  LCD_Data(0x14);           // Contrast = 20
  LCD_Data(0x05);           // IR5 => gain = 8.52

#ifdef GuiConst_COLOR_DEPTH_1
  LCD_Cmd(GRAYSCALE_MODE)   // Selection for monochrome mode
  LCD_Data(0x00);           // After reset is grayscale as default
  LCD_Data(0x0E);           //
  LCD_Data(0x41);           //
#endif

  // Clear display memory
  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Select page
    LCD_Cmd(PAGE_ADDRESS);
    LCD_Data(PageNo);   // Start page
    LCD_Data(PageNo);   // End page
    // Select column
    LCD_Cmd(COLUMN_ADDRESS);
    LCD_Data(0);   // Start column
#ifdef GuiConst_COLOR_DEPTH_1
    LCD_Data(GuiConst_BYTES_PR_LINE * 2 - 1); // End column
#else
    LCD_Data(GuiConst_BYTES_PR_LINE-1);       // End column
#endif
    // Write display data
    LCD_Cmd(WRITE_DATA);
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
    {
      LCD_Data(0x00);
    }
  }

  LCD_Cmd(DISPLAY_ON);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      LCD_Cmd(PAGE_ADDRESS);
      LCD_Data(PageNo);   // Start page
      LCD_Data(PageNo);   // End page
      // Select column
      LCD_Cmd(COLUMN_ADDRESS);
#ifdef GuiConst_COLOR_DEPTH_1
      LCD_Data(GuiLib_DisplayRepaint[PageNo].ByteBegin * 2); // Start column
      LCD_Data(GuiConst_BYTES_PR_LINE * 2 - 1);              // End column
#else
      LCD_Data(GuiLib_DisplayRepaint[PageNo].ByteBegin);     // Start column
      LCD_Data(GuiConst_BYTES_PR_LINE-1);                    // End column
#endif

      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      // Write display data
      LCD_Cmd(WRITE_DATA);
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
      {
        LCD_Data(GuiLib_DisplayBuf[PageNo][N]);
      }
      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SSD1848

#ifdef LCD_CONTROLLER_TYPE_SSD1858

// ============================================================================
//
// SSD1858 DISPLAY CONTROLLER
//
// This driver uses one SSD1858 display controller with on-chip oscillator.
// LCD display 8 bit 8080 type interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 104x64 pixels.
//
// Compatible display controllers:
//   SSD1857, SSD1859
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 2
//   Color mode: Grayscale
//   Color depth: 2 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Function Spi_SendData must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define DISP_DATA                 Pxx

#define RESET_ON                  Pxx.x = 0     // RES pin
#define RESET_OFF                 Pxx.x = 1     // RES pin
#define CHIP_ENABLED              Pxx.x = 0     // CS pin
#define CHIP_DISABLED             Pxx.x = 1     // CS pin
#define MODE_COMMAND              Pxx.x = 0     // D/C pin
#define MODE_DATA                 Pxx.x = 1     // D/C pin
#define RW_WR_LOW                 Pxx.x = 0     // R/W(WR) pin
#define RW_WR_HIGH                Pxx.x = 1     // R/W(WR) pin
#define E_RD_LOW                  Pxx.x = 0     // E(RD) pin
#define E_RD_HIGH                 Pxx.x = 1     // E(RD) pin
#define NOP                       asm ("nop")   // nop code

#define lcdSelectColumnHighNibble(columnno)                  lcdPortWriteByte(0,0x10 | ((columnno >> 4) & 0x0f))
#define lcdSelectColumnLowNibble(columnno)                   lcdPortWriteByte(0,0x00 | ((columnno) & 0x0f))
#define lcdResistorRatio(ratio)                              lcdPortWriteByte(0,0x20 | ((ratio) & 0x07))
#define lcdVoltageController(booster, vreg, vfollow)         lcdPortWriteByte(0,0x28 | ((booster) ? 4 : 0) | ((vreg) ? 2 : 0) | ((vfollow) ? 1 : 0))
#define lcdTemperatureRatio(ratio)                           lcdPortWriteByte(0,0x38 | ((ratio) & 0x07))
#define lcdSelectStartLine(lineno) lcdPortWriteByte(0,0x40); lcdPortWriteByte(0,(lineno) & 0x7F)
#define lcdSetInitialCOM1(lineno)  lcdPortWriteByte(0,0x44); lcdPortWriteByte(0,(lineno) & 0x7F)
#define lcdMultiplexRatio(ratio)   lcdPortWriteByte(0,0x48); lcdPortWriteByte(0,(ratio) & 0x7f)
#define lcdSelectLCDBias(ratio)                              lcdPortWriteByte(0,0x50 | ((ratio) & 0x07))
#define lcdSelectBoost(ratio)                                lcdPortWriteByte(0,0x64 | ((ratio) & 0x03))
#define lcdElectronicVol(level)    lcdPortWriteByte(0,0x81); lcdPortWriteByte(0,(level) & 0x3f)
#define lcdSelectGrayReg(reg)                                lcdPortWriteByte(0,0x88 | (reg & 0x07))
#define lcdReset                                             lcdPortWriteByte(0,0xe2)
#define lcdSelectPage(pageno)                                lcdPortWriteByte(0,0xb0 | ((pageno) & 0x0f))
#define lcdDisplaySw(onOff)                                  lcdPortWriteByte(0,0xae | ((onOff) ? 1 : 0))
#define lcdOscilatorOn                                       lcdPortWriteByte(0,0xAB)
#define lcdSegmentReverse                                    lcdPortWriteByte(0,0xA1)
#define lcdScanReverse                                       lcdPortWriteByte(0,0xC8)
#define lcdSetGrayValue(value)                               lcdPortWriteByte(0,value)
#define lcdDataByte(dataval)                                 lcdPortWriteByte(1,dataval)

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void Spi_SendData(void* LCD_DestAddress,
                  void *LCD_SrcAddress,
                  GuiConst_INT16U LCD_DataLength)
{
  // Something hardware related
}


/*
// 4 lines SPI interface
void lcdPortWriteByte(GuiConst_INT8U A0,
                      GuiConst_INT8U v)
{
  Spi_SendData(&A0,&v,1);
}
*/


// 8 bit parallel 8080 type interface
void lcdPortWriteByte(GuiConst_INT8U A0,
                      GuiConst_INT8U v)
{
  if(A0)
    MODE_DATA;
  else
    MODE_COMMAND;
  E_RD_HIGH;
  RW_WR_LOW;
  CHIP_ENABLED;
  DISP_DATA = v;
  //NOP;   // Maybe required for some microcontrollers
  CHIP_DISABLED;
  RW_WR_HIGH;
}

/*
// 8 bit parallel 6800 type interface
void lcdPortWriteByte(GuiConst_INT8U A0, GuiConst_INT8U Val)
{
  if(A0)
    MODE_DATA;
  else
    MODE_COMMAND;
  RW_WR_LOW;
  E_RD_HIGH;
  CHIP_ENABLED;
  DISP_DATA = Val;
  //NOP;   // Maybe required for some microcontrollers
  CHIP_DISABLED;
  E_RD_LOW;
}
*/

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  RESET_ON;
  millisecond_delay(10);
  RESET_OFF;

  lcdOscilatorOn;
  lcdVoltageController(1,1,1);

  // set if you don't want default settings
  //lcdSelectBoost(ratio);
  //lcdMultiplexRatio(ratio);
  //lcdSelectLCDBias(ratio);
  //lcdElectronicVol(level);
  //lcdTemperatureRatio(ratio);

  // set grayscale palette registers
  lcdSelectGrayReg(0);
  lcdSetGrayValue(0x00);
  lcdSelectGrayReg(1);
  lcdSetGrayValue(0x00);
  lcdSelectGrayReg(2);
  lcdSetGrayValue(0x33);
  lcdSelectGrayReg(3);
  lcdSetGrayValue(0x33);
  lcdSelectGrayReg(4);
  lcdSetGrayValue(0x66);
  lcdSelectGrayReg(5);
  lcdSetGrayValue(0x66);
  lcdSelectGrayReg(6);
  lcdSetGrayValue(0x99);
  lcdSelectGrayReg(7);
  lcdSetGrayValue(0x99);

  // Clear display memory
  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Select page
    lcdSelectPage(PageNo);
    // Select column
    lcdSelectColumnHighNibble(0);
    lcdSelectColumnLowNibble(0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
    {
      lcdDataByte(0x00);
      lcdDataByte(0x00);
    }
  }

  lcdDisplaySw(1);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
      // Something to redraw on this page
    {
      // Select page
      lcdSelectPage(PageNo);

      // Select column
      lcdSelectColumnHighNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      lcdSelectColumnLowNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
      {
        lcdDataByte(GuiLib_DisplayBuf[0][PageNo][N]);
        lcdDataByte(GuiLib_DisplayBuf[1][PageNo][N]);
      }
      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SSD1858

#ifdef LCD_CONTROLLER_TYPE_SSD1926

// ============================================================================
//
// SSD1926 DISPLAY CONTROLLER
//
// This driver uses one SSD1926 display controller with on-chip pll synthesizer.
// Host mcu interface in 16 bit 8080 parallel indirect addressing mode
// or Generic type 1# bus
// LCD panel interface in 18bit TFT interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired connection for 16 bit indirect 8080 interface
// AD_MODE = 1;
// CNF2 = 1;
// CNF1 = 0;
// CNF0 = 0;
// CNF4 = 0; little endian
// CNF6 = 0; recommended for indirect addressing mode
// AB[18:4,2,1,0] = 0
// M/R = 0;
// RD/WR = 0;
// WE1 = 0;

// Hardware pin settings for 16 bit color depth
// C86 = 0 - 8080 type interface
// PSX BWS1 BWS0 DTX2 DTX1
// 0    1    0    0    0   - 16 bit parallel collective interface
// 1    1    0    x    x   - 16 bit serial interface

// Uncomment for microcontroller STM32
#define STM32

#ifdef STM32
// Hardwired connection of SSD926 for 16 bit Generic #1 interface
// AD_MODE = 0;
// CNF2 = 0;
// CNF1 = 1;
// CNF0 = 1;
// CNF4 = 0; little endian
// CNF6 = 0; recommended for indirect addressing mode
// AB0 = 0

// Hardware connection between
// STM32                    SSD1926
// A1 - A18                 AB1 - AB18
// D0 - D15                 D0 - D15
// NE3                      CS#
// NOE                      RD#, RD/WR#
// NWE                      WE0#, WE1#

#include "stm32f10x.h"

#define Bank1_SRAM3_ADDR    ((GuiConst_INT32U)0x68000000)
// A19 pin
#define MR_HIGH             ((GuiConst_INT32U)0x00080000)

void LCD_Reg(GuiConst_INT32U address, GuiConst_INT16U data)
{
  *(GuiConst_INT16U *)(Bank1_SRAM3_ADDR + address) = data;
}

void LCD_Mem(GuiConst_INT32U address, GuiConst_INT16U data)
{
  *(GuiConst_INT16U *)(Bank1_SRAM3_ADDR + MR_HIGH + address) = data;
}

#else

#define DISP_DATA                 Pxx              // D[15-0]
#define CHIP_ENABLED              Pxx.x = 0        // CS pin
#define CHIP_DISABLED             Pxx.x = 1        // CS pin
#define MODE_COMMAND              Pxx.x = 0        // D/C pin AB3
#define MODE_DATA                 Pxx.x = 1        // D/C pin AB3
#define WR_LOW                    Pxx.x = 0        // WE0 pin
#define WR_HIGH                   Pxx.x = 1        // WE0 pin
#define RD_LOW                    Pxx.x = 0        // RD pin
#define RD_HIGH                   Pxx.x = 1        // RD pin
#define NOP                       asm ("nop")      // nop code

// Write register address - byte mode for litle endian
// As registers are 8bit wide, only lower byte from data word is used
void LCD_CmdReg(GuiConst_INT16U address)
{
  MODE_COMMAND;
  RD_HIGH;
  WR_LOW;

  DISP_DATA = address>>8;
  CHIP_ENABLED;
  //NOP;             // May be required for some microcontrollers
  CHIP_DISABLED;

  DISP_DATA = address<<8;
  CHIP_ENABLED;
  //NOP;             // May be required for some microcontrollers
  CHIP_DISABLED;

  WR_HIGH;
}

// Write memory address - word mode for litle endian
void LCD_CmdMem(GuiConst_INT16U address)
{
  MODE_COMMAND;
  RD_HIGH;
  WR_LOW;

  DISP_DATA = (address>>8)|0x8000;
  CHIP_ENABLED;
  //NOP;             // May be required for some microcontrollers
  CHIP_DISABLED;

  DISP_DATA = (address<<8)|0x0001;
  CHIP_ENABLED;
  //NOP;             // May be required for some microcontrollers
  CHIP_DISABLED;

  WR_HIGH;
}

// Write 16 bit data to register or memory
void LCD_Data(GuiConst_INT16U data)
{
  MODE_DATA;
  RD_HIGH;
  DISP_DATA = data;
  WR_LOW;
  CHIP_ENABLED;
  //NOP;             // May be required for some microcontrollers
  CHIP_DISABLED;
  WR_HIGH;
}
#endif

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT32U i;

#ifdef STM32

  FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
  FSMC_NORSRAMTimingInitTypeDef  p;
  GPIO_InitTypeDef GPIO_InitStructure;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOG |
                         RCC_APB2Periph_GPIOE | RCC_APB2Periph_GPIOF, ENABLE);

  // GPIO Configuration - SRAM Data lines configuration
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_8 | GPIO_Pin_9 |
                                GPIO_Pin_10 | GPIO_Pin_14 | GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOD, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 |
                                GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 |
                                GPIO_Pin_15;
  GPIO_Init(GPIOE, &GPIO_InitStructure);

  // SRAM Address lines configuration
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 |
                                GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_12 | GPIO_Pin_13 |
                                GPIO_Pin_14 | GPIO_Pin_15;
  GPIO_Init(GPIOF, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 |
                                GPIO_Pin_4 | GPIO_Pin_5;
  GPIO_Init(GPIOG, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13;
  GPIO_Init(GPIOD, &GPIO_InitStructure);

  // NOE and NWE configuration
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 |GPIO_Pin_5;
  GPIO_Init(GPIOD, &GPIO_InitStructure);

  // NE3 configuration
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_Init(GPIOG, &GPIO_InitStructure);

  // NBL0, NBL1 configuration
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
  GPIO_Init(GPIOE, &GPIO_InitStructure);

  // FSMC Configuration
  p.FSMC_AddressSetupTime = 0;
  p.FSMC_AddressHoldTime = 0;
  p.FSMC_DataSetupTime = 2;
  p.FSMC_BusTurnAroundDuration = 0;
  p.FSMC_CLKDivision = 0;
  p.FSMC_DataLatency = 0;
  p.FSMC_AccessMode = FSMC_AccessMode_A;

  FSMC_NORSRAMInitStructure.FSMC_Bank = FSMC_Bank1_NORSRAM3;
  FSMC_NORSRAMInitStructure.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable;
  FSMC_NORSRAMInitStructure.FSMC_MemoryType = FSMC_MemoryType_SRAM;
  FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;
  FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode = FSMC_BurstAccessMode_Disable;
  FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
  FSMC_NORSRAMInitStructure.FSMC_WrapMode = FSMC_WrapMode_Disable;
  FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;
  FSMC_NORSRAMInitStructure.FSMC_WriteOperation = FSMC_WriteOperation_Enable;
  FSMC_NORSRAMInitStructure.FSMC_WaitSignal = FSMC_WaitSignal_Disable;
  FSMC_NORSRAMInitStructure.FSMC_ExtendedMode = FSMC_ExtendedMode_Disable;
  FSMC_NORSRAMInitStructure.FSMC_WriteBurst = FSMC_WriteBurst_Disable;
  FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct = &p;
  FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct = &p;

  FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure);

  // Enable FSMC Bank1_SRAM Bank
  FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM3, ENABLE);

  // Software Reset Register
  LCD_Reg(0x00A2,0x0001); // Reset registers

  // Clock Configuration Registers
  // Example for input clock frequency = 2MHz
  // PLL output clock frequency = Input clock frequency * (M / N) = 80MHz

  // PLL Clock Setting Register 0 - 0x126h, PLL Clock Setting Register 1 - 0x127h
  LCD_Reg(0x0126,0xC805);

  // PLL Clock Setting Register 2 - 0x12Bh
  LCD_Reg(0x012A,0xAE00);
  // PLL Clock Setting Register 0 - 0x126h, PLL Clock Setting Register 1 - 0x127h
  LCD_Reg(0x0126,0xC885);

  // Memory Clock Configuration Register - 0x04h
  // MCLK Divide Select Bits [4:0],
  // MCLK Frequency = PLL output frequency / (MCLK divide value + 1)
  LCD_Reg(0x0004,0x0000);

  // PCLK frequency = MCLK frequency * (PCLK Frequency Ratio + 1) / 1048576
  // PCLK Frequency Ratio Register - 0x158h, PCLK Frequency Ratio Register 1 - 0x159h
  LCD_Reg(0x0158,0x0000);

  // PCLK Frequency Ratio Register 2
  LCD_Reg(0x015A,0x0000);

  // Look-Up Table Registers are bypased for this color mode

  // Panel Configuration Registers

  // Panel Type Register - 0x10h, MOD Rate Register - 0x11h
  LCD_Reg(0x0010,0x0021); // generic TFT panel 18bit width

  // Horizontal Display Period Register
  // The HDP must be a minimum of 32 pixels and can be increased by multiples of 8.
  // HDP = Horizontal Display Period = [((REG[14h] bits 6-0) + 1) x 8] pixels
  LCD_Reg(0x0014,(GuiConst_DISPLAY_WIDTH_HW/8-1) & 0x00FF);

  // HDPS = Horizontal Display Period Start Position =
  // [(REG[17h] bits 2-0, REG[16h] bits 7-0) + 5] pixels
  // Horizontal Display Period Start Position Register 0 - 0x16h
  // Horizontal Display Period Start Position Register 1 - 0x17h
  LCD_Reg(0x0016,0x0000);

  // HT = Horizontal Total =
  // [((REG[12h] bits 7-0) + 1) x 8 + (REG[13h] bits 2-0)] pixels
  // The following conditions must be fulfilled for all panel timings:
  // HDPS + HDP < HT
  // Horizontal Total Register 1 - 0x12h, Horizontal Total Register 0 - 0x13h
  LCD_Reg(0x0012,((GuiConst_DISPLAY_WIDTH_HW/8) & 0x00FF) | 0x0700); // + 8 pixels

  // HPS = LLINE Pulse Start Position =
  // [(REG[23h] bits 2-0, REG[22h] bits 7-0) + 1] pixels
  // LLINE Pulse Start Position Register 0 - 0x22h
  // LLINE Pulse Start Position Register 1 - 0x23h
  LCD_Reg(0x0022,0x0000);

  // HPW = LLINE Pulse Width = [(REG[20h] bits 6-0)+ 1] pixels
  // LLINE Pulse Width Register - 0x20h,
  // Bit 7 LLINE Pulse Polarity, 0 - active low
  // LLINE Pulse Start Sub-pixel Position Register 0x21h
  LCD_Reg(0x0020,0x0008);

  // VDP = Vertical Display Period =
  // [(REG[1Dh]bits1-0,REG[1Ch]bits7-0)+ 1] lines
  // * The VDP must be a minimum of 2 lines
  // Vertical Display Period Register 0 - 0x1Ch, Vertical Display Period Register 1 - 0x1Dh
  LCD_Reg(0x001C,GuiConst_BYTE_LINES-1);

  // VDPS = Vertical Display Period Start Position =
  // [(REG[1Fh]bits2-0,REG[1Eh]bits7-0)] lines
  // Vertical Display Period Start Position Register 0 - 0x1Eh
  // Vertical Display Period Start Position Register 1 - 0x1Fh
  LCD_Reg(0x001E,0x0000);

  // VT = Vertical Total = [(REG[19h] bits 2-0, REG[18h] bits 7-0) + 1] lines
  // The following conditions must be fulfilled for all panel timings:
  // VDPS + VDP < VT
  // Vertical Total Register 0 - 0x18h, Vertical Total Register 1 - 0x19h
  LCD_Reg(0x0018,GuiConst_BYTE_LINES+1);

  // VPS = LFRAME Pulse Start Position =
  // [(REG[27h] bits 2-0, REG[26h] bits 7-0)] x HT +
  // (REG[31h] bits 2-0, REG[30h] bits 7-0) pixels
  // LFRAME Pulse Start Position Register 0 - 0x26h, LFRAME Pulse Start Position Register 1 - 0x27h
  LCD_Reg(0x0026,0x0000);

  // VPW = LFRAME Pulse Width =
  // [(REG[24h]bits2-0)+ 1] x HT + (REG[35h] bits 2-0, REG[34h] bits 7-0) �
  // (REG[31h] bits 2-0, REG[30h] bits 7-0) pixels
  // LFRAME Pulse Width Register - 0x24h
  LCD_Reg(0x0024,0x0000); // Bit 7 LFRAME Pulse Polarity, 0 = active low

  //LFRAME Pulse Start Offset Register 0 - 0x30h, LFRAME Pulse Start Offset Register 1 - 0x31h
  LCD_Reg(0x0030,0x0000);

  // LFRAME Pulse Stop Offset Register 0 - 0x34h, LFRAME Pulse Stop Offset Register 1 - 0x35h
  LCD_Reg(0x0034,0x0000);

  // LSHIFT Polarity Register
  LCD_Reg(0x0038,0x0000);  // Bit 0 LSHIFT Polarity Swap, 0 - signal is rising trigger

  // Power Up Registers
  // We don't use power save modes

  // Display Mode Registers
  // Display Mode Register
  // Bits 2-0 Bit-per-pixel Select Bits [2:0]
  LCD_Reg(0x0070,0x0004);  // 16 bit color depth for tft panel

  // TFT FRC Enable Bit Register
  // bit 0 TFT FRC Enable Bit Register
  LCD_Reg(0x0346,0x0000);

  // Main Window Registers
  // Address is in double words
  // Main Window Display Start Address Register 0 - 74h
  // Main Window Display Start Address Register 1 - 75h
  LCD_Reg(0x0074,0x0000);

  // Main Window Display Start Address Register 2
  LCD_Reg(0x0076,0x0000);

  // Main Window Line Address Offset bits 9-0 =
  // Display Width in pixels � (32 � bpp)
  // Main Window Line Address Offset Register 0 - 78h
  // Main Window Line Address Offset Register 1 - 79h
  LCD_Reg(0x0078,GuiConst_DISPLAY_WIDTH_HW/2);

  // RGB Setting Register
  LCD_Reg(0x01A4,0x00C0);  // RGB mode for main and floating window

  for (i = 0; i < (GuiConst_DISPLAY_BYTES/2); i++);
    LCD_Mem(i*2,0x0000);

#else

  // Software Reset Register
  LCD_CmdReg(0x00A2);
  LCD_Data(0x0001); // Reset registers

  // Clock Configuration Registers
  // Example for input clock frequency = 2MHz
  // PLL output clock frequency = Input clock frequency * (M / N) = 80MHz

  // PLL Clock Setting Register 0
  LCD_CmdReg(0x0126);
  LCD_Data(0x0005);
  // PLL Clock Setting Register 1
  LCD_CmdReg(0x0127);
  LCD_Data(0x00C8);
  // PLL Clock Setting Register 2
  LCD_CmdReg(0x012B);
  LCD_Data(0x00AE);
  // PLL Clock Setting Register 0
  LCD_CmdReg(0x0126);
  LCD_Data(0x0085);

  // Memory Clock Configuration Register
  // MCLK Divide Select Bits [4:0],
  // MCLK Frequency = PLL output frequency / (MCLK divide value + 1)
  LCD_CmdReg(0x0004);
  LCD_Data(0x0000);

  // PCLK frequency = MCLK frequency * (PCLK Frequency Ratio + 1) / 1048576
  // PCLK Frequency Ratio Register 0
  LCD_CmdReg(0x0158);
  LCD_Data(0x0000);

  // PCLK Frequency Ratio Register 1
  LCD_CmdReg(0x0159);
  LCD_Data(0x0000);

  // PCLK Frequency Ratio Register 2
  LCD_CmdReg(0x015A);
  LCD_Data(0x0000);

  // Look-Up Table Registers are bypased for this color mode

  // Panel Configuration Registers

  // Panel Type Register
  LCD_CmdReg(0x0010);
  LCD_Data(0x0021); // generic TFT panel 18bit width

  // Horizontal Display Period Register
  // The HDP must be a minimum of 32 pixels and can be increased by multiples of 8.
  // HDP = Horizontal Display Period = [((REG[14h] bits 6-0) + 1) x 8] pixels
  LCD_CmdReg(0x0014);
  LCD_Data(GuiConst_DISPLAY_WIDTH_HW/8-1);

  // HDPS = Horizontal Display Period Start Position =
  // [(REG[17h] bits 2-0, REG[16h] bits 7-0) + 5] pixels
  // Horizontal Display Period Start Position Register 0
  LCD_CmdReg(0x0016);
  LCD_Data(0x0000);

  // Horizontal Display Period Start Position Register 1
  LCD_CmdReg(0x0017);
  LCD_Data(0x0000);

  // HT = Horizontal Total =
  // [((REG[12h] bits 7-0) + 1) x 8 + (REG[13h] bits 2-0)] pixels
  // The following conditions must be fulfilled for all panel timings:
  // HDPS + HDP < HT
  // Horizontal Total Register 1
  LCD_CmdReg(0x0012);
  LCD_Data(GuiConst_DISPLAY_WIDTH_HW/8);

  // Horizontal Total Register 0
  LCD_CmdReg(0x0013);
  LCD_Data(0x0007);  // + 8 pixel

  // HPS = LLINE Pulse Start Position =
  // [(REG[23h] bits 2-0, REG[22h] bits 7-0) + 1] pixels
  // LLINE Pulse Start Position Register 0
  LCD_CmdReg(0x0022);
  LCD_Data(0x0000);

  // LLINE Pulse Start Position Register 1
  LCD_CmdReg(0x0023);
  LCD_Data(0x0000);

  // HPW = LLINE Pulse Width = [(REG[20h] bits 6-0)+ 1] pixels
  // LLINE Pulse Width Register
  LCD_CmdReg(0x0020);
  LCD_Data(0x0008); // Bit 7 LLINE Pulse Polarity, 0 - active low

  // VDP = Vertical Display Period =
  // [(REG[1Dh]bits1-0,REG[1Ch]bits7-0)+ 1] lines
  // * The VDP must be a minimum of 2 lines
  // Vertical Display Period Register 0
  // LCD_CmdReg(0x001C);
  LCD_Data((GuiConst_INT8U)(GuiConst_BYTE_LINES-1));

  // Vertical Display Period Register 1
  // LCD_CmdReg(0x001D);
  LCD_Data((GuiConst_BYTE_LINES-1)>>8);

  // VDPS = Vertical Display Period Start Position =
  // [(REG[1Fh]bits2-0,REG[1Eh]bits7-0)] lines
  // Vertical Display Period Start Position Register 0
  LCD_CmdReg(0x001E);
  LCD_Data(0x0000);

  // Vertical Display Period Start Position Register 1
  LCD_CmdReg(0x001F);
  LCD_Data(0x0000);

  // VT = Vertical Total = [(REG[19h] bits 2-0, REG[18h] bits 7-0) + 1] lines
  // The following conditions must be fulfilled for all panel timings:
  // VDPS + VDP < VT
  // Vertical Total Register 0
  LCD_CmdReg(0x0018);
  LCD_Data((GuiConst_INT8U)(GuiConst_BYTE_LINES+1));

  // Vertical Total Register 1
  LCD_CmdReg(0x0019);
  LCD_Data((GuiConst_BYTE_LINES+1)>>8);

  // VPS = LFRAME Pulse Start Position =
  // [(REG[27h] bits 2-0, REG[26h] bits 7-0)] x HT +
  // (REG[31h] bits 2-0, REG[30h] bits 7-0) pixels
  // LFRAME Pulse Start Position Register 0
  LCD_CmdReg(0x0026);
  LCD_Data(0x0000);

  // LFRAME Pulse Start Position Register 1
  LCD_CmdReg(0x0027);
  LCD_Data(0x0000);

  // VPW = LFRAME Pulse Width =
  // [(REG[24h]bits2-0)+ 1] x HT + (REG[35h] bits 2-0, REG[34h] bits 7-0) �
  // (REG[31h] bits 2-0, REG[30h] bits 7-0) pixels
  // LFRAME Pulse Width Register
  LCD_CmdReg(0x0024);
  LCD_Data(0x0000); // Bit 7 LFRAME Pulse Polarity, 0 = active low

  //LFRAME Pulse Start Offset Register 0
  LCD_CmdReg(0x0030);
  LCD_Data(0x0000);

  //LFRAME Pulse Start Offset Register 1
  LCD_CmdReg(0x0031);
  LCD_Data(0x0000);

  // LFRAME Pulse Stop Offset Register 0
  LCD_CmdReg(0x0034);
  LCD_Data(0x0000);

  // LFRAME Pulse Stop Offset Register 1
  LCD_CmdReg(0x0035);
  LCD_Data(0x0000);

  // LSHIFT Polarity Register
  LCD_CmdReg(0x0038);
  LCD_Data(0x0000);  // Bit 0 LSHIFT Polarity Swap, 0 - signal is rising trigger

  // Power Up Registers
  // We don't use power save modes

  // Display Mode Registers
  // Display Mode Register
  // Bits 2-0 Bit-per-pixel Select Bits [2:0]
  LCD_CmdReg(0x0070);
  LCD_Data(0x0004);  // 16 bit color depth for tft panel

  // TFT FRC Enable Bit Register
  // bit 0 TFT FRC Enable Bit Register
  LCD_CmdReg(0x0346);
  LCD_Data(0x0000);

  // Main Window Registers
  // Address is in double words
  // Main Window Display Start Address Register 0
  LCD_CmdReg(0x0074);
  LCD_Data(0x0000);

  // Main Window Display Start Address Register 1
  LCD_CmdReg(0x0075);
  LCD_Data(0x0000);

  // Main Window Display Start Address Register 2
  LCD_CmdReg(0x0076);
  LCD_Data(0x0000);

  // Main Window Line Address Offset bits 9-0 =
  // Display Width in pixels � (32 � bpp)
  // Main Window Line Address Offset Register 0
  LCD_CmdReg(0x0078); // bits 7 - 0
  LCD_Data((GuiConst_INT8U)(GuiConst_DISPLAY_WIDTH_HW/2));

  // Main Window Line Address Offset Register 1
  LCD_CmdReg(0x0079); // bits 1,0
  LCD_Data((GuiConst_DISPLAY_WIDTH_HW/2)>>8);

  // RGB Setting Register
  LCD_CmdReg(0x01A4);
  LCD_Data(0x00C0);  // RGB mode for main and floating window

  // Clear display memory
  LCD_CmdMem(0x0000);
  for (i = 0; i < (GuiConst_DISPLAY_BYTES/2); i++)
    LCD_Data(0x0000);

#endif

}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT32U Address;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;
  GuiConst_INT16S PageNo;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Calculate address in buffer
#ifdef STM32
      Address = PageNo * GuiConst_BYTES_PR_LINE;
#else
      Address = PageNo * GuiConst_BYTES_PR_LINE +
                GuiLib_DisplayRepaint[PageNo].ByteBegin * 2;
      LCD_CmdMem(Address);
#endif

      // Calculate end byte
      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
      {
#ifdef STM32
        LCD_Mem(Address + N*2, GuiLib_DisplayBuf.Words[PageNo][N]);
#else
        LCD_Data(GuiLib_DisplayBuf.Words[PageNo][N]);
#endif
      }
      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SSD1926

#ifdef LCD_CONTROLLER_TYPE_SSD1963

// ============================================================================
//
// SSD1963 DISPLAY CONTROLLER
//
// This driver uses one SSD1963 display controller with built-in oscillator.
// Host mcu interface in 16 bit (565 format) or 8 bit 8080 parallel addressing
// mode.
// LCD panel interface in 24 bit TFT interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 864x480 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired connection for 8 or 16 bit indirect 8080 interface
// CONF = 1;

// Uncomment for 8 bit bus
#define 8BIT_BUS

// Uncomment for microcontroller AVR32UC + driver HX8238 combination
//#define AVR32UC_HX8238

#ifdef AVR32UC_HX8238
// Connection for 16 bit memory mapped interface
// AVR32UC          SSD1963              HX8238
// RESET_N          RESET#               RESB
// D[15-0]   -      D[15-0]
// NCS0      -      CS#
// A0        -      D/C#
// NRW       -      R/W#(WR#)
// NRD       -      E(RD)#
//                  LFRAME               VSYNC
//                  LLINE                HSYNC
//                  LSHIFT               DOTCLK
//                  LDATA[23-16]         R[7-0]
//                  LDATA[15-8]          B[7-0]
//                  LDATA[7-0]           G[7-0]
// NPCS0                                 CSB
// SPCK                                  SCK
// MOSI                                  SDI

#include <avr32/io.h>

// CPU core speed in Hz
#define CPUHZ                  20000000
//AVR32_EBI_SMC0 ADDRESS
#define LCD_BASE               0xC0000000

// Number of bits in each SPI package
#define SPI_BITS               8
// SPI master speed in Hz
#define SPI_MASTER_SPEED       125000
// Timeout for spi read and write blocking functions
#define SPI_TIMEOUT            10000

// HX8238 register access
#define INDEX_REGISTER         0
#define INSTRUCTION            1

#define Max(a,b)               (((a)>(b)) ? (a) : (b))
#define STRINGZ(x)             #x
#define ASTRINGZ(x)            STRINGZ(x)
#define TPASTE2(a,b)           a##b
#define ATPASTE2(a,b)          TPASTE2( a, b)

#define FALSE                  0
#define TRUE                   1
#define DISABLED               0
#define ENABLED                1

#define GPIO                   AVR32_GPIO

//! A type definition of pins and modules connectivity.
typedef struct
{
  GuiConst_INT8U pin;           // Module pin.
  GuiConst_INT8U function;      // Module function.
} gpio_map_t[];

// SMC Peripheral Memory Size in log2(Bytes)
#define EXT_SM_SIZE             17        // (65536 * 2) = 2^17 bytes
// SMC Data Bus Width
#define SMC_DBW                 16
// Whether 8-bit SM chips are connected on the SMC
#define SMC_8_BIT_CHIPS         FALSE
// NWE setup length = (128* NWE_SETUP[5] + NWE_SETUP[4:0]), Unit: ns.
#define NWE_SETUP               0
// NCS setup length = (128* NCS_WR_SETUP[5] + NCS_WR_SETUP[4:0]), Unit: ns.
#define NCS_WR_SETUP            0
// NRD setup length = (128* NRD_SETUP[5] + NRD_SETUP[4:0]), Unit: ns.
#define NRD_SETUP               0
// NCS setup length = (128* NCS_RD_SETUP[5] + NCS_RD_SETUP[4:0]), Unit: ns.
#define NCS_RD_SETUP            0
// NCS pulse length = (256* NCS_WR_PULSE[6] + NCS_WR_PULSE[5:0]), Unit: ns.
#define NCS_WR_PULSE            82
// NWE pulse length = (256* NWE_PULSE[6] + NWE_PULSE[5:0]), Unit: ns.
#define NWE_PULSE               82
// NCS pulse length = (256* NCS_RD_PULSE[6] + NCS_RD_PULSE[5:0]), Unit: ns.
#define NCS_RD_PULSE            82
// NRD pulse length = (256* NRD_PULSE[6] + NRD_PULSE[5:0]), Unit: ns.
#define NRD_PULSE               82
// Write cycle length = (NWE_CYCLE[8:7]*256 + NWE_CYCLE[6:0]), Unit: ns.
#define NCS_WR_HOLD             82
#define NWE_HOLD                82
#define NWE_CYCLE               Max((NCS_WR_SETUP + NCS_WR_PULSE + NCS_WR_HOLD),(NWE_SETUP + NWE_PULSE + NWE_HOLD))
// Read cycle length = (NRD_CYCLE[8:7]*256 + NRD_CYCLE[6:0]), Unit: ns.
#define NCS_RD_HOLD             0
#define NRD_HOLD                0
#define NRD_CYCLE               Max((NCS_RD_SETUP + NCS_RD_PULSE + NCS_RD_HOLD),(NRD_SETUP + NRD_PULSE + NRD_HOLD))
// Data float time
#define TDF_CYCLES              0
#define TDF_OPTIM               DISABLED
// Page mode
#define PAGE_MODE               DISABLED
#define PAGE_SIZE               0
// Whether read is controlled by NCS or by NRD
#define NCS_CONTROLLED_READ     FALSE
// Whether write is controlled by NCS or by NWE
#define NCS_CONTROLLED_WRITE    TRUE
// Whether to use the NWAIT pin
#define NWAIT_MODE              AVR32_SMC_EXNW_MODE_DISABLED

#define GPIO_SUCCESS            0
#define GPIO_INVALID_ARGUMENT   1

GuiConst_INT32S gpio_enable_module_pin(GuiConst_INT32U pin, GuiConst_INT32U function)
{
  volatile avr32_gpio_port_t * gpio_port = &GPIO.port[pin >> 5];

  // Enable the correct function.
  switch (function)
  {
    case 0: // A function.
      gpio_port->pmr0c = 1 << (pin & 0x1F);
      gpio_port->pmr1c = 1 << (pin & 0x1F);
      break;

    case 1: // B function.
      gpio_port->pmr0s = 1 << (pin & 0x1F);
      gpio_port->pmr1c = 1 << (pin & 0x1F);
      break;

    case 2: // C function.
      gpio_port->pmr0c = 1 << (pin & 0x1F);
      gpio_port->pmr1s = 1 << (pin & 0x1F);
      break;

    default:
      return GPIO_INVALID_ARGUMENT;
  }

  // Disable GPIO control.
  gpio_port->gperc = 1 << (pin & 0x1F);

  return GPIO_SUCCESS;
}

GuiConst_INT32S gpio_enable_module(const gpio_map_t gpiomap, GuiConst_INT32U size)
{
  int status = GPIO_SUCCESS;
  GuiConst_INT32U i;

  for (i = 0; i < size; i++)
  {
    status |= gpio_enable_module_pin(gpiomap->pin, gpiomap->function);
    gpiomap++;
  }

  return status;
}

static GuiConst_INT8U smc_tab_cs_size[6];

void smc_init(GuiConst_INT32U hsb_hz)
{
  GuiConst_INT32U hsb_mhz_up = (hsb_hz + 999999) / 1000000;

  // Setup SMC for NCS0
  AVR32_SMC.cs[0].setup = (((NWE_SETUP * hsb_mhz_up + 999) / 1000) << AVR32_SMC_SETUP0_NWE_SETUP_OFFSET) |
                          (((NCS_WR_SETUP * hsb_mhz_up + 999) / 1000) << AVR32_SMC_SETUP0_NCS_WR_SETUP_OFFSET) |
                          (((NRD_SETUP * hsb_mhz_up + 999) / 1000) << AVR32_SMC_SETUP0_NRD_SETUP_OFFSET) |
                          (((NCS_RD_SETUP * hsb_mhz_up + 999) / 1000) << AVR32_SMC_SETUP0_NCS_RD_SETUP_OFFSET);

  AVR32_SMC.cs[0].pulse = (((NWE_PULSE * hsb_mhz_up + 999) / 1000) << AVR32_SMC_PULSE0_NWE_PULSE_OFFSET) |
                          (((NCS_WR_PULSE * hsb_mhz_up + 999) / 1000) << AVR32_SMC_PULSE0_NCS_WR_PULSE_OFFSET) |
                          (((NRD_PULSE * hsb_mhz_up + 999) / 1000) << AVR32_SMC_PULSE0_NRD_PULSE_OFFSET) |
                          (((NCS_RD_PULSE * hsb_mhz_up + 999) / 1000) << AVR32_SMC_PULSE0_NCS_RD_PULSE_OFFSET);

  AVR32_SMC.cs[0].cycle = (((NWE_CYCLE * hsb_mhz_up + 999) / 1000) << AVR32_SMC_CYCLE0_NWE_CYCLE_OFFSET) |
                          (((NRD_CYCLE * hsb_mhz_up + 999) / 1000) << AVR32_SMC_CYCLE0_NRD_CYCLE_OFFSET);


  AVR32_SMC.cs[0].mode  = (((NCS_CONTROLLED_READ) ? AVR32_SMC_MODE0_READ_MODE_NCS_CONTROLLED :
                          AVR32_SMC_MODE0_READ_MODE_NRD_CONTROLLED) << AVR32_SMC_MODE0_READ_MODE_OFFSET) |
                          +(((NCS_CONTROLLED_WRITE) ? AVR32_SMC_MODE0_WRITE_MODE_NCS_CONTROLLED :
                          AVR32_SMC_MODE0_WRITE_MODE_NWE_CONTROLLED) << AVR32_SMC_MODE0_WRITE_MODE_OFFSET) |
                          (NWAIT_MODE << AVR32_SMC_MODE0_EXNW_MODE_OFFSET) |
                          (((SMC_8_BIT_CHIPS) ? AVR32_SMC_MODE0_BAT_BYTE_WRITE :
                          AVR32_SMC_MODE0_BAT_BYTE_SELECT) << AVR32_SMC_MODE0_BAT_OFFSET) |
                          (((SMC_DBW <= 8 ) ? AVR32_SMC_MODE0_DBW_8_BITS  :
                          (SMC_DBW <= 16) ? AVR32_SMC_MODE0_DBW_16_BITS :
                          AVR32_SMC_MODE0_DBW_32_BITS) << AVR32_SMC_MODE0_DBW_OFFSET) |
                          (TDF_CYCLES << AVR32_SMC_MODE0_TDF_CYCLES_OFFSET) |
                          (TDF_OPTIM << AVR32_SMC_MODE0_TDF_MODE_OFFSET) |
                          (PAGE_MODE << AVR32_SMC_MODE0_PMEN_OFFSET) |
                          (PAGE_SIZE << AVR32_SMC_MODE0_PS_OFFSET);

  smc_tab_cs_size[0] = EXT_SM_SIZE;

  // Put the multiplexed MCU pins used for the SM under control of the SMC.
  static const gpio_map_t SMC_EBI_GPIO_MAP =
  {
    // Enable data pins.
    {ATPASTE2(AVR32_EBI_DATA_0,_PIN),ATPASTE2(AVR32_EBI_DATA_0,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_1,_PIN),ATPASTE2(AVR32_EBI_DATA_1,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_2,_PIN),ATPASTE2(AVR32_EBI_DATA_2,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_3,_PIN),ATPASTE2(AVR32_EBI_DATA_3,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_4,_PIN),ATPASTE2(AVR32_EBI_DATA_4,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_5,_PIN),ATPASTE2(AVR32_EBI_DATA_5,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_6,_PIN),ATPASTE2(AVR32_EBI_DATA_6,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_7,_PIN),ATPASTE2(AVR32_EBI_DATA_7,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_8,_PIN),ATPASTE2(AVR32_EBI_DATA_8,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_9,_PIN),ATPASTE2(AVR32_EBI_DATA_9,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_10,_PIN),ATPASTE2(AVR32_EBI_DATA_10,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_11,_PIN),ATPASTE2(AVR32_EBI_DATA_11,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_12,_PIN),ATPASTE2(AVR32_EBI_DATA_12,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_13,_PIN),ATPASTE2(AVR32_EBI_DATA_13,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_14,_PIN),ATPASTE2(AVR32_EBI_DATA_14,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_DATA_15,_PIN),ATPASTE2(AVR32_EBI_DATA_15,_FUNCTION)},
    // Enable address pins.
    {ATPASTE2(AVR32_EBI_ADDR_0,_PIN),ATPASTE2(AVR32_EBI_ADDR_0,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_1,_PIN),ATPASTE2(AVR32_EBI_ADDR_1,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_2,_PIN),ATPASTE2(AVR32_EBI_ADDR_2,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_3,_PIN),ATPASTE2(AVR32_EBI_ADDR_3,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_4,_PIN),ATPASTE2(AVR32_EBI_ADDR_4,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_5,_PIN),ATPASTE2(AVR32_EBI_ADDR_5,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_6,_PIN),ATPASTE2(AVR32_EBI_ADDR_6,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_7,_PIN),ATPASTE2(AVR32_EBI_ADDR_7,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_8,_PIN),ATPASTE2(AVR32_EBI_ADDR_8,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_9,_PIN),ATPASTE2(AVR32_EBI_ADDR_9,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_10,_PIN),ATPASTE2(AVR32_EBI_ADDR_10,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_11,_PIN),ATPASTE2(AVR32_EBI_ADDR_11,_FUNCTION}),
    {ATPASTE2(AVR32_EBI_ADDR_12,_PIN),ATPASTE2(AVR32_EBI_ADDR_12,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_13,_PIN),ATPASTE2(AVR32_EBI_ADDR_13,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_14,_PIN),ATPASTE2(AVR32_EBI_ADDR_14,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_15,_PIN),ATPASTE2(AVR32_EBI_ADDR_15,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_16,_PIN),ATPASTE2(AVR32_EBI_ADDR_16,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_17,_PIN),ATPASTE2(AVR32_EBI_ADDR_17,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_18,_PIN),ATPASTE2(AVR32_EBI_ADDR_18,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_19,_PIN),ATPASTE2(AVR32_EBI_ADDR_19,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_20_0,_PIN),ATPASTE2(AVR32_EBI_ADDR_20_0,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_21_0,_PIN),ATPASTE2(AVR32_EBI_ADDR_21_0,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_22_0,_PIN),ATPASTE2(AVR32_EBI_ADDR_22_0,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_ADDR_23,_PIN),ATPASTE2(AVR32_EBI_ADDR_23,_FUNCTION)},
    // Enable control pins.
    {ATPASTE2(AVR32_EBI_NWE0_0,_PIN),ATPASTE2(AVR32_EBI_NWE0_0,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_NWE1_0,_PIN),ATPASTE2(AVR32_EBI_NWE1_0,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_NRD_0,_PIN),ATPASTE2(AVR32_EBI_NRD_0,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_NWAIT_0,_PIN),ATPASTE2(AVR32_EBI_NWAIT_0,_FUNCTION)},
    {ATPASTE2(AVR32_EBI_NCS_0_0,_PIN),ATPASTE2(AVR32_EBI_NCS_0_0,_FUNCTION)},
  };

  gpio_enable_module(SMC_EBI_GPIO_MAP, sizeof(SMC_EBI_GPIO_MAP) / sizeof(SMC_EBI_GPIO_MAP[0]));
}

// SPI controller

// Time-out value (number of attempts).
#define SPI_TIMEOUT            10000

// Status codes used by the SPI driver.
typedef enum
{
  SPI_ERROR = -1,
  SPI_OK = 0,
  SPI_ERROR_TIMEOUT = 1,
  SPI_ERROR_ARGUMENT,
  SPI_ERROR_OVERRUN,
  SPI_ERROR_MODE_FAULT,
  SPI_ERROR_OVERRUN_AND_MODE_FAULT
} spi_status_t;

typedef union
{
  GuiConst_INT32U               cr;
  avr32_spi_cr_t                CR;
} u_avr32_spi_cr_t;

typedef union
{
  GuiConst_INT32U               mr;
  avr32_spi_mr_t                MR;
} u_avr32_spi_mr_t;

typedef union
{
  GuiConst_INT32U               tdr;
  avr32_spi_tdr_t               TDR;
} u_avr32_spi_tdr_t;

typedef union
{
  GuiConst_INT32U               ier;
  avr32_spi_ier_t               IER;
} u_avr32_spi_ier_t;

typedef union
{
  GuiConst_INT32U               idr;
  avr32_spi_idr_t               IDR;
} u_avr32_spi_idr_t;

typedef union
{
  GuiConst_INT32U               csr;
  avr32_spi_csr0_t              CSR;
} u_avr32_spi_csr_t;

//! Option structure for SPI channels.
typedef struct
{
  //! The SPI channel to set up.
  GuiConst_INT8U reg;
  //! Preferred baudrate for the SPI.
  GuiConst_INT32U baudrate;
  //! Number of bits in each character (8 to 16).
  GuiConst_INT8U bits;
  //! Delay before first clock pulse after selecting slave (in PBA clock periods).
  GuiConst_INT8U spck_delay;
  //! Delay between each transfer/character (in PBA clock periods).
  GuiConst_INT8U trans_delay;
  //! Sets this chip to stay active after last transfer to it.
  GuiConst_INT8U stay_act;
  //! Which SPI mode to use when transmitting.
  GuiConst_INT8U spi_mode;
  //! Disables the mode fault detection.
  //! With this bit cleared, the SPI master mode will disable itself if another
  //! master tries to address it.
  GuiConst_INT8U modfdis;
} spi_options_t;

// Calculate the baudrate division
// param options Pointer to a spi_options_t struct
// param cpuHz the clock frequency in Hz for the peripheral bus
// retval >= 0 on success, retval  < 0 on error
static GuiConst_INT32S getBaudDiv(const spi_options_t *options, GuiConst_INT32U pba_hz)
{
  int baudDiv = (pba_hz + options->baudrate / 2) / options->baudrate;

  if (baudDiv <= 0 || baudDiv > 255) {
    return -1;
  }

  return baudDiv;
}

// Resets the SPI controller
// param spi Pointer to the correct avr32_spi_t struct
void spi_reset(volatile avr32_spi_t *spi)
{
  spi->cr = AVR32_SPI_CR_SWRST_MASK;
}

// Setup the SPI module in master mode
// param spi Pointer to the correct avr32_spi_t struct
// param options Pointer struct containing setup for a chip register
// retval SPI_OK on success
// retval SPI_ERROR_ARGUMENT when invalid arguments are passed
spi_status_t spi_initMaster(volatile avr32_spi_t *spi, const spi_options_t *options)
{
  u_avr32_spi_mr_t u_avr32_spi_mr;

  if (options->modfdis > 1) {
    return SPI_ERROR_ARGUMENT;
  }

  // Reset.
  spi->cr = AVR32_SPI_CR_SWRST_MASK;

  // Master Mode.
  u_avr32_spi_mr.mr = spi->mr;
  u_avr32_spi_mr.MR.mstr = 1;
  u_avr32_spi_mr.MR.modfdis = options->modfdis;
  u_avr32_spi_mr.MR.llb = 0;
  u_avr32_spi_mr.MR.pcs = (1 << AVR32_SPI_MR_PCS_SIZE) - 1;
  spi->mr = u_avr32_spi_mr.mr;

  return SPI_OK;
}

// How and when are the slave chip selected (master mode only)
// param spi Pointer to the correct avr32_spi_t struct
// param variable_ps Target slave is selected in transfer register
// for every character to transmit
// param pcs_decode The four chip select lines are decoded externaly;
// values 0 to 14 can be given to select_chip()
// param delay Delay in MCK cyles (or NxMCK with FDIV) between chip selects
// retval SPI_OK on success
// retval SPI_ERROR_ARGUMENT when invalid arguments are passed
spi_status_t spi_selectionMode(volatile avr32_spi_t *spi,
                               GuiConst_INT8U variable_ps,
                               GuiConst_INT8U pcs_decode,
                               GuiConst_INT8U delay)
{
  u_avr32_spi_mr_t u_avr32_spi_mr;

  if (variable_ps > 1 ||
      pcs_decode > 1) {
    return SPI_ERROR_ARGUMENT;
  }

  u_avr32_spi_mr.mr = spi->mr;
  u_avr32_spi_mr.MR.ps = variable_ps;
  u_avr32_spi_mr.MR.pcsdec = pcs_decode;
  u_avr32_spi_mr.MR.dlybcs = delay;
  spi->mr = u_avr32_spi_mr.mr;

  return SPI_OK;
}

// Select slave chip
// param spi Pointer to the correct avr32_spi_t struct
// param chip Slave chip number (normal: 0-3, extarnaly decoded signal: 0-14)
// retval SPI_OK on success
// retval SPI_ERROR_ARGUMENT when invalid arguments are passed
spi_status_t spi_selectChip(volatile avr32_spi_t *spi, GuiConst_INT8U chip)
{
  // Assert all lines; no peripheral is selected.
  spi->mr |= AVR32_SPI_MR_PCS_MASK;

  if (spi->mr & AVR32_SPI_MR_PCSDEC_MASK)
  {
    // The signal is decoded; allow up to 15 chips.
    if (chip > 14)
      return SPI_ERROR_ARGUMENT;

    spi->mr &= ~AVR32_SPI_MR_PCS_MASK | (chip << AVR32_SPI_MR_PCS_OFFSET);
  }
  else
  {
    if (chip > 3)
      return SPI_ERROR_ARGUMENT;

    spi->mr &= ~(1 << (AVR32_SPI_MR_PCS_OFFSET + chip));
  }

  return SPI_OK;
}

// Set options for a specific slave chip. The baudrate field has to be written
// before transfer in master mode. Four similar registers exist,
// one for each slave. When using encoded slave addressing, reg=0 sets options
// for slave 0-3, reg=1 for slave 4-7 and so on.
// param spi Pointer to the correct avr32_spi_t struct
// param options Pointer struct containing setup for a chip register
// param cpuHz clock speed in Hz for the peripheral bus
// retval SPI_OK on success
// retval SPI_ERROR_ARGUMENT when invalid arguments are passed
spi_status_t spi_setupChipReg(volatile avr32_spi_t *spi,
                              const spi_options_t *options,
                              GuiConst_INT32U pba_hz)
{
  u_avr32_spi_csr_t u_avr32_spi_csr;

  if (options->spi_mode > 3 || options->stay_act > 1 ||
      options->bits < 8 || options->bits > 16)
  {
    return SPI_ERROR_ARGUMENT;
  }

  GuiConst_INT32S baudDiv = getBaudDiv(options, pba_hz);

  if (baudDiv < 0)
  {
    return SPI_ERROR_ARGUMENT;
  }

  // Will use CSR0 offsets; these are the same for CSR0 to CSR3.
  u_avr32_spi_csr.csr = 0;
  u_avr32_spi_csr.CSR.cpol = options->spi_mode >> 1;
  u_avr32_spi_csr.CSR.ncpha = (options->spi_mode & 0x1) ^ 0x1;
  u_avr32_spi_csr.CSR.csaat = options->stay_act;
  u_avr32_spi_csr.CSR.bits = options->bits - 8;
  u_avr32_spi_csr.CSR.scbr = baudDiv;
  u_avr32_spi_csr.CSR.dlybs = options->spck_delay;
  u_avr32_spi_csr.CSR.dlybct = options->trans_delay;

  switch(options->reg)
  {
    case 0:
      spi->csr0 = u_avr32_spi_csr.csr;
      break;
    case 1:
      spi->csr1 = u_avr32_spi_csr.csr;
      break;
    case 2:
      spi->csr2 = u_avr32_spi_csr.csr;
      break;
    case 3:
      spi->csr3 = u_avr32_spi_csr.csr;
      break;
    default:
      return SPI_ERROR_ARGUMENT;
  }

  return SPI_OK;
}

// Enables a disabled SPI
// param spi Pointer to the correct avr32_spi_t struct
void spi_enable(volatile avr32_spi_t *spi)
{
  spi->cr = AVR32_SPI_CR_SPIEN_MASK;
}

// Writes one block of data to the selected (fixed select) slave, will
// block program execution until timeout occurs if transmitter is busy
// param spi Pointer to the correct avr32_spi_t struct
// param data The block to write
// retval SPI_OK on success
// retval SPI_TIMEOUT on timeout
spi_status_t spi_write(volatile avr32_spi_t *spi, GuiConst_INT16U data)
{
  GuiConst_INT32U timeout = SPI_TIMEOUT;

  while (!(spi->sr & AVR32_SPI_SR_TDRE_MASK))
  {
    if (!timeout--)
      return SPI_ERROR_TIMEOUT;
  }

  spi->tdr = data << AVR32_SPI_TDR_TD_OFFSET;

  return SPI_OK;
}

// Map over PIO for setup of peripherals
typedef GuiConst_INT8U avr32_piomap_t[][2];

// Initialise SPI in master mode
// param spi Pointer to the correct avr32_spi_t struct
// param cpuHz CPU clock frequency in Hz
void init_spiMaster(volatile avr32_spi_t *spi, GuiConst_INT32S cpuHz)
{
  spi_options_t spiOptions =
  {
  .reg = 0,
  .baudrate = SPI_MASTER_SPEED,
  .bits = SPI_BITS,
  .spck_delay = 0,
  .trans_delay = 4,
  .stay_act = 0,
  .spi_mode = 1,
  .modfdis = 0,
  };

  // Initialize as master
  spi_initMaster(spi, &spiOptions);

  // Set master mode; variable_ps, pcs_decode, delay
  spi_selectionMode(spi, 0, 0, 0);

  // Select slave chip 0 (SPI_NPCS0)
  spi_selectChip(spi, 0);

  spi_setupChipReg(spi, &spiOptions, cpuHz);

  spi_enable(spi);
}

// Send command or data to HX8238
void HX8238_SpiSend (GuiConst_INT8U RS, GuiConst_INT16U Data)
{
  volatile avr32_spi_t *spi = &AVR32_SPI0;

  if(RS)
    spi_write(spi,0x72);
  else
    spi_write(spi,0x70);

  spi_write(spi,(GuiConst_INT8U)(Data>>8));
  spi_write(spi,(GuiConst_INT8U)Data);
}

//Write register address - 16 bit memory mapped interface
void LCD_Cmd(GuiConst_INT16U address)
{
    *(GuiConst_INT16U *)LCD_BASE = address ;
}

// Write 16 bit data to register or memory - 16 bit memory mapped interface
void LCD_Data(GuiConst_INT16U data)
{
    *(GuiConst_INT16U *)(LCD_BASE + 2) = data;
}

#else
// Adjust according your hardware and compiler syntax

#define DISP_DATA                 Pxx              // D[15-0] (for 8 bit bus only D[7-0])
#define CHIP_ENABLED              Pxx.x = 0        // CS pin
#define CHIP_DISABLED             Pxx.x = 1        // CS pin
#define MODE_COMMAND              Pxx.x = 0        // D/C pin
#define MODE_DATA                 Pxx.x = 1        // D/C pin
#define WR_LOW                    Pxx.x = 0        // RW(WR) pin
#define WR_HIGH                   Pxx.x = 1        // RW(WR) pin
#define RD_LOW                    Pxx.x = 0        // E(RD) pin
#define RD_HIGH                   Pxx.x = 1        // E(RD) pin

// Write register address - 8 bit or 16 bit interface(it used only lower 8 bits)
void LCD_Cmd(GuiConst_INT16U address)
{
  MODE_COMMAND;
  RD_HIGH;
  WR_LOW;
  DISP_DATA = address;
  CHIP_ENABLED;
  //NOP;
  CHIP_DISABLED;
  WR_HIGH;
}

// Write 16 bit data to register or memory - 16 bit interface
// or 8 bit data to register - 8 bit interface(it used only lower 8 bits)
void LCD_Data(GuiConst_INT16U data)
{
  MODE_DATA;
  RD_HIGH;
  WR_LOW;
  DISP_DATA = data;
  CHIP_ENABLED;
  //NOP;
  CHIP_DISABLED;
  WR_HIGH;
}

#ifdef 8BIT_BUS
// Write 16 bit data (format 565) to memory - 8 bit interface in 3 cycles
void LCD_Data_8BUS(GuiConst_INT16U data)
{
  MODE_DATA;
  RD_HIGH;
  WR_LOW;

  // Red color 5 bits
  DISP_DATA = (data>>8) & 0x00F8;
  CHIP_ENABLED;
  //NOP;
  CHIP_DISABLED;

  // Green color 6 bits
  DISP_DATA = (data>>3) & 0x00FC;;
  CHIP_ENABLED;
  //NOP;
  CHIP_DISABLED;

  // Blue color 5 bits
  DISP_DATA = data<<3;
  CHIP_ENABLED;
  //NOP;
  CHIP_DISABLED;

  WR_HIGH;
}
#endif

#endif

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT32U i;

#ifdef AVR32UC_HX8238
  // Set up a spi structure to point at SPI's base address
  volatile avr32_spi_t *spi = &AVR32_SPI0;
  gpio_map_t spi_piomap =
  {
    {AVR32_SPI0_SCK_0_0_PIN, AVR32_SPI0_SCK_0_0_FUNCTION},
    {AVR32_SPI0_MISO_0_0_PIN, AVR32_SPI0_MISO_0_0_FUNCTION},
    {AVR32_SPI0_MOSI_0_0_PIN, AVR32_SPI0_MOSI_0_0_FUNCTION},
    {AVR32_SPI0_NPCS_0_0_PIN, AVR32_SPI0_NPCS_0_0_FUNCTION},
    {AVR32_SPI0_NPCS_1_0_PIN, AVR32_SPI0_NPCS_1_0_FUNCTION},
    {AVR32_SPI0_NPCS_2_0_PIN, AVR32_SPI0_NPCS_2_0_FUNCTION},
    {AVR32_SPI0_NPCS_3_0_PIN, AVR32_SPI0_NPCS_3_0_FUNCTION},
  };

  // Init GPIO
  gpio_enable_module(spi_piomap, 7);
  // Init SPI interface in master mode
  init_spiMaster(spi, CPUHZ);

  // HX8238 Initialization , example for Palm Tech. PT0353224T
  // Driver output control
  HX8238_SpiSend (INDEX_REGISTER,0x0001);
  HX8238_SpiSend (INSTRUCTION,0x6300);
  // LCD driving waveform control
  HX8238_SpiSend (INDEX_REGISTER,0x0002);
  HX8238_SpiSend (INSTRUCTION,0x0200);
  // Power control 1
  HX8238_SpiSend (INDEX_REGISTER,0x0003);
  HX8238_SpiSend (INSTRUCTION,0x7166);
  // Input data and Color filter control
  HX8238_SpiSend (INDEX_REGISTER,0x0004);
  HX8238_SpiSend (INSTRUCTION,0x0447);
  // Function control
  HX8238_SpiSend (INDEX_REGISTER,0x0005);
  HX8238_SpiSend (INSTRUCTION,0xBCD4);
  // Contrast brightness control
  HX8238_SpiSend (INDEX_REGISTER,0x000A);
  HX8238_SpiSend (INSTRUCTION,0x3F08);
  // Frame Cycle control
  HX8238_SpiSend (INDEX_REGISTER,0x000B);
  HX8238_SpiSend (INSTRUCTION,0xD400);
  // Power control 2
  HX8238_SpiSend (INDEX_REGISTER,0x000D);
  HX8238_SpiSend (INSTRUCTION,0x123A);
  // Power control 3
  HX8238_SpiSend (INDEX_REGISTER,0x000E);
  HX8238_SpiSend (INSTRUCTION,0x3100);
  // Gate scan starting position
  HX8238_SpiSend (INDEX_REGISTER,0x000F);
  HX8238_SpiSend (INSTRUCTION,0x0000);
  // Horizontal Porch
  HX8238_SpiSend (INDEX_REGISTER,0x0016);
  HX8238_SpiSend (INSTRUCTION,0x9F86);
  // Vertical Porch
  HX8238_SpiSend (INDEX_REGISTER,0x0017);
  HX8238_SpiSend (INSTRUCTION,0x2212);
  // Power control 4
  HX8238_SpiSend (INDEX_REGISTER,0x001E);
  HX8238_SpiSend (INSTRUCTION,0x00E0);

  // Initialize SMC controller
  smc_init(CPUHZ);
#endif

  // Software Reset
  LCD_Cmd(0x01);

  millisecond_delay(10);

  // Set PLL MN
  // N[7:0] : Multiplier (N) of PLL.
  // M[3:0] : Divider (M) of PLL.
  // C[2] : Effectuate MN value,
  // 0 Ignore the multiplier (N) and divider (M) values in A[7:0] and B[7:0]
  // 1 Effectuate the multiplier and divider value
  // Example For a 10MHz reference clock to obtain 115MHz PLL frequency
  LCD_Cmd(0xE2);
  LCD_Data(0x21); // (N=34)
  LCD_Data(0x02); // (M=3)
  LCD_Data(0x54); // (Dummy Byte)

  // Set PLL
  LCD_Cmd(0xE0);
  // Before the start, the system was operated with the crystal oscillator or clock input.
  // A[1] : Lock PLL. After PLL enabled for 100us, can start to lock PLL
  // 0 Use reference clock as system clock, 1 Use PLL output as system clock
  // A[0] : 0 Disable PLL, 1 Enable PLL
  LCD_Data(0x01);
  millisecond_delay(10);
  LCD_Data(0x03);

  // Set LSHIFT Frequency
  LCD_Cmd(0xE6);
  // Configure the pixel clock to PLL freq x ((LCDC_FPR + 1) / 2 on 20)
  // Example to obtain PCLK = 5.3MHz with PLL Frequency = 120MHz,
  //LCDC_FPR[19:16]
  LCD_Data(0x00);
  // LCDC_FPR[15:8]
  LCD_Data(0xB4);
  // LCDC_FPR[7:0]
  LCD_Data(0xE7);

  // Set Address Mode
   LCD_Cmd(0x36);
  // A[7] : Page address order, 0 Top to bottom, 1 Bottom to top
  // A[6] : Column address order, 0 Left to right, 1 Right to left
  // A[5] : Page / Column order, 0 Normal mode, 1 Reverse mode
  // A[4] : Refresh line address order, 0 from top to bottom, 1 from bottom to top
  // A[3] : RGB / BGR order, 0 RGB, 1 BGR
  // A[2] : Display data refresh, 0 from left to right side, 1 from right to left side
  // A[1] : Flip Horizontal, 0 Normal, 1 Flipped
  // A[0] : Flip Vertical, 0 Normal, 1 Flipped
  LCD_Data(0x00);

  // Set Pixel Format
  LCD_Cmd(0x3A);
  // A[6:4] : 001: 3-bit/pixel, 010: 8-bit/pixel, 011: 12-bit/pixel,
  //          101: 16-bit/pixel,110: 18-bit/pixel,111: 24-bit/pixel
  LCD_Data(0x50);

  // Set LCD Mode
  LCD_Cmd(0xB0);
  // Bits settings
  // 5 : TFT panel data width, 0 18-bit, 1 24-bit
  // 4 : TFT color depth enhancement, 0 Disable FRC or dithering, 1 Enable FRC or dithering
  // 3 : TFT FRC enable, 0 TFT dithering enable, 1 TFT FRC enable
  // 2 : LSHIFT (dot clock) polarity, 0 Rising edge, 1 Falling edge
  // 1 : LLINE (horizontal sync pulse) polarity, 0 Active low, 1 Active high
  // 0 : LFRAME (vertical sync pulse) polarity, 0 Active low, 1 Active high
  LCD_Data(0x20);
  // Bits settings
  // 7 : LCD panel mode, 0 Hsync+Vsync+DE mode, 1 TTL mode
  // 6:5 : TFT type, 00, 01 TFT mode, 10 Serial RGB mode, 11 Serial RGB+dummy mode
  LCD_Data(0x00);
  // Horizontal panel size = (HPS + 1) pixels
  // HPS[10:8] : Set the horizontal panel size
  LCD_Data((GuiConst_DISPLAY_WIDTH - 1)>>8);
  // HPS[7:0] : Set the horizontal panel size
  LCD_Data((GuiConst_INT8U)(GuiConst_DISPLAY_WIDTH - 1));
  // Vertical panel size = (VPS + 1) lines
  // VPS[10:8] : Set the vertical panel size
  LCD_Data((GuiConst_BYTE_LINES - 1)>>8);
  // VPS[7:0] : Set the vertical panel size
  LCD_Data((GuiConst_INT8U)(GuiConst_BYTE_LINES - 1));
  // Bits settings
  // 5:3 : Even line RGB sequence, 000 RGB, 001 RBG, 010 GRB, 011 GBR, 100 BRG, 101 BGR
  // 2:1 : Odd line RGB sequence, 000 RGB, 001 RBG, 010 GRB, 011 GBR, 100 BRG, 101 BGR
  LCD_Data(0x00);

  // Set Horizontal Period
  LCD_Cmd(0xB4);
  // Horizontal total period = (HT + 1) pixels
  // HT[10:8]
  LCD_Data((GuiConst_DISPLAY_WIDTH + 0x30)>>8);
  // HT[7:0]
  LCD_Data((GuiConst_INT8U)(GuiConst_DISPLAY_WIDTH + 0x30));
  // Horizontal Sync Pulse Start Position = (HPS + 1) pixels
  // HPS[10:8]
  LCD_Data(0x00);
  // HPS[7:0]
  LCD_Data(0x10);
  // (LLINE) Horizontal Sync Pulse Width = (HPW + 1) pixels
  // HPW[6:0]
  LCD_Data(0x20);
  // Horizontal Display Period Start Position = LPS pixels
  // LPS[10:8]
  LCD_Data(0x00);
  // LPS[7:0]
  LCD_Data(0x20);
  // LPSPP[1:0] : Set the horizontal sync pulse subpixel start position
  LCD_Data(0x00);

  // Set Vertical Period
  LCD_Cmd(0xB6);
  // Vertical Total = (VT + 1) lines
  // VT[10:8]
  LCD_Data((GuiConst_BYTE_LINES + 0x10)>>8);
  // VT[7:0]
  LCD_Data((GuiConst_INT8U)(GuiConst_BYTE_LINES + 0x10));
  // Vertical Sync Pulse Start Position = VPS lines
  // VPS[10:8]
  LCD_Data(0x00);
  // VPS[7:0]
  LCD_Data(0x04);
  // (LFRAME)Vertical Sync Pulse Width = (VPW + 1) lines
  // VPW[6:0]
  LCD_Data(0x01);
  // Vertical Display Period Start Position = FPS lines
  // FPS[10:8]
  LCD_Data(0x00);
  // FPS[7:0]
  LCD_Data(0x06);

  // Set GPIO Configuration
  LCD_Cmd(0xB8);
  // A[7] - A[4] : GPIO3 configuration, 0 GPIO is controlled by host, 1 GPIO is controlled by LCDC
  LCD_Data(0xF0);
  // B[0] : GPIO0 direction
  // 0 GPIO0 is used to control the panel power with Enter or Exit Sleep Mode
  // 1 GPIO0 is used as normal GPIO
  LCD_Data(0x00);

  // Set Post Proc
  LCD_Cmd(0xBC);
  // A[7:0] : Set the contrast value
  // B[7:0] : Set the brightness value
  // C[7:0] : Set the saturation value
  // D[0] : 0 Disable the postprocessor, 1 Enable the postprocessor
  LCD_Data(0x40);
  LCD_Data(0x40);
  LCD_Data(0x40);
  LCD_Data(0x00);

  // Set Gamma Curve
  LCD_Cmd(0x26);
  // 0 - No gamma curve, 1 - Gamma curve 0, 2 - Gamma curve 1, 4 - Gamma Curve 2, 8 - Gamma curve 3
  LCD_Data(0x08);

  // Set Pixel Data Interface
  LCD_Cmd(0xF0);
  // A[2:0] : 0: 8-bit,  1: 12-bit, 2: 16-bit packed, 3: 16-bit (565 format),
  //          4: 18-bit, 5: 24-bit, 6: 9-bit
#ifdef 8BIT_BUS
  LCD_Data(0x00);
#else
  LCD_Data(0x03);
#endif

  // Enter Normal Mode
  LCD_Cmd(0x13);

  // Set Column Address
  LCD_Cmd(0x2A);
  // Start column
  LCD_Data(0x00);
  LCD_Data(0x00);
  // End column
  LCD_Data(GuiConst_DISPLAY_WIDTH>>8);
  LCD_Data((GuiConst_INT8U)GuiConst_DISPLAY_WIDTH);

  // Set Page Address
  LCD_Cmd(0x2B);
  // Start row
  LCD_Data(0x00);
  LCD_Data(0x00);
  // End row
  LCD_Data(GuiConst_BYTE_LINES>>8);
  LCD_Data((GuiConst_INT8U)GuiConst_BYTE_LINES);

  // Clear display memory
  LCD_Cmd(0x2C);
  for (i = 0; i < (GuiConst_DISPLAY_BYTES/2); i++);
    LCD_Data(0x0000);

  // Set display on
  LCD_Cmd(0x29);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S LastByte;
  GuiConst_INT16S X, Y;

  // Lock GUI resources
  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(0x2B);
      // Start row
      LCD_Data(Y>>8);
      LCD_Data((GuiConst_INT8U)Y);
      // End row
      LCD_Data(Y>>8);
      LCD_Data((GuiConst_INT8U)Y);

      // Select start and end column
      LCD_Cmd(0x2A);
      // Start column
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin >> 8);
      LCD_Data((GuiConst_INT8U)GuiLib_DisplayRepaint[Y].ByteBegin);

      LastByte = GuiConst_BYTES_PR_LINE / 2 - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // End column
      LCD_Data(LastByte>>8);
      LCD_Data((GuiConst_INT8U)LastByte);

      // Write display data
      LCD_Cmd(0x2C);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
#ifdef 8BIT_BUS
        LCD_Data_8BUS(GuiLib_DisplayBuf.Words[Y][X]);
#else
        LCD_Data(GuiLib_DisplayBuf.Words[Y][X]);
#endif
      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}
#endif // LCD_CONTROLLER_TYPE_SSD1963

#ifdef LCD_CONTROLLER_TYPE_SSD2119

// ============================================================================
//
// SSD2119 DISPLAY CONTROLLER
//
// This driver uses one SSD2119 display controller with built-in oscillator.
// Host mcu interface in 16 bit 8080 parallel addressing mode.
// LCD panel interface in 18bit TFT interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired connection of SSD2119
// PS3 PS2 PS1 PS0
// 0   0   1   0   16-bit 8080 parallel interface D[17~10],D[8~1]
// 0   0   1   1   8-bit 8080 parallel interface D[17-10]

// Connection only for EMI interface of STR912F and display controller
// EMI AD[0-15] - D[0-15]
// EMI WRXn     - WR pin
// EMI RD       - RD pin
// EMI nCS0     - CS pin
// EMI AD16     - RS pin
// Uncomment for SSD2119 connected to EMI interface (bank 0)
// of STR912F microcontroller in 16 bit multiplexed mode
#define STR912F

#ifdef STR912F
  #include "91x_lib.h"
  // Adjust your port
  #define LCD_CONTROL_PORT          GPIO9
  // Adjust your pin
  #define LCD_RESET_PIN             GPIO_Pin_0
#else
  #define DISP_DATA                 Pxx              // For 16 bit D[17-10],D[8-1]
                                                     // For 8 bit only D[17-10]
  #define CHIP_ENABLED              Pxx.x = 0        // CS pin
  #define CHIP_DISABLED             Pxx.x = 1        // CS pin
  #define MODE_COMMAND              Pxx.x = 0        // DC pin
  #define MODE_DATA                 Pxx.x = 1        // DC pin
  #define WR_LOW                    Pxx.x = 0        // WR pin
  #define WR_HIGH                   Pxx.x = 1        // WR pin
  #define RD_LOW                    Pxx.x = 0        // RD pin
  #define RD_HIGH                   Pxx.x = 1        // RD pin
  #define RESET_LOW                 Pxx.x = 0        // RESET pin
  #define RESET_HIGH                Pxx.x = 1        // RESET pin
  #define NOP                       asm ("nop")      // nop code
  // For 2x8bit mcu parallel interface
  #define DataBus_H                 Port1
  #define DataBus_L                 Port2
#endif

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

#ifdef STR912F
// Write register address - 16 bit memory mapped interface
void LCD_Cmd(GuiConst_INT16U address)
{
  *(GuiConst_INT16U *)0x3C000000 = address ;
}

// Write 16 bit data to register or memory - 16 bit memory mapped interface
void LCD_Data(GuiConst_INT16U data)
{
  *(GuiConst_INT16U *)0x3C010000 = data ;
}
#else
/*
// Write register address - 16bit interface
void LCD_Cmd(GuiConst_INT16U address)
{
  MODE_COMMAND;
  RD_HIGH;
  WR_LOW;
  DISP_DATA = address;
  CHIP_ENABLED;
  //NOP;
  CHIP_DISABLED;
  WR_HIGH;
}

// Write 16 bit data to register or memory - 16 bit interface
void LCD_Data(GuiConst_INT16U data)
{
  MODE_DATA;
  RD_HIGH;
  WR_LOW;
  DISP_DATA = data;
  CHIP_ENABLED;
  //NOP;
  CHIP_DISABLED;
  WR_HIGH;
}
*/

// For 8 bit mcu interface
// Write 16 bit register address - 8bit interface
void LCD_Cmd(GuiConst_INT16U address)
{
  MODE_COMMAND;
  RD_HIGH;

  CHIP_ENABLED;
  WR_LOW;
  DISP_DATA = (GuiConst_INT8U)(address>>8);
  //NOP;
  WR_HIGH;
  CHIP_DISABLED;

  CHIP_ENABLED;
  WR_LOW;
  DISP_DATA = (GuiConst_INT8U)address;
  //NOP;
  WR_HIGH;
  CHIP_DISABLED;
}

// Write 16 bit data to register or memory - 8bit interface

void LCD_Data(GuiConst_INT16U data)
{
  MODE_DATA;
  RD_HIGH;

  CHIP_ENABLED;
  WR_LOW;
  DISP_DATA = (GuiConst_INT8U)(data>>8);
  //NOP;
  WR_HIGH;
  CHIP_DISABLED;

  CHIP_ENABLED;
  WR_LOW;
  DISP_DATA = (GuiConst_INT8U)data;
  //NOP;
  WR_HIGH;
  CHIP_DISABLED;
}

/*
// For 8 bit mcu with writing at the same time to two 8bit ports
// Write 16 bit register address - 2 x 8 bit interface
void LCD_Cmd(GuiConst_INT16U address)
{
  MODE_COMMAND;
  RD_HIGH;
  WR_LOW;
  DataBus_H = (GuiConst_INT8U)(address>>8);
  DataBus_L = (GuiConst_INT8U)address;
  CHIP_ENABLED;
  //NOP;
  CHIP_DISABLED;
  WR_HIGH;
}

// For 8 bit mcu with writing at the same time to two 8bit ports
// Write 16 bit data to register or memory - 2x8bit interface
void LCD_Data(GuiConst_INT16U data)
{
  MODE_DATA;
  RD_HIGH;
  WR_LOW;
  DataBus_H = (GuiConst_INT8U)(data>>8);
  DataBus_L = (GuiConst_INT8U)data;
  CHIP_ENABLED;
  //NOP;
  CHIP_DISABLED;
  WR_HIGH;
}
*/
#endif

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT32U i;

#ifdef STR912F
  GPIO_InitTypeDef  GPIO_InitStructure;
  EMI_InitTypeDef   EMI_InitStruct;

  // -------- Configure the system clocks ----------
  // Default configuration
  SCU_MCLKSourceConfig(SCU_MCLK_OSC);

  // Wait state insertion :This function should be executed from SRAM when
  // booting from bank1 to avoid  Read-While-Write from the Same Bank.
  // Insert 2 Wait States for read
  FMI_Config(FMI_READ_WAIT_STATE_2, FMI_WRITE_WAIT_STATE_0, FMI_PWD_ENABLE,\
                FMI_LVD_ENABLE, FMI_FREQ_HIGH);
  // PLL factors Configuration based on OSC/Crystal value = 25Mhz
  SCU_PLLFactorsConfig(192, 25, 2);
  // PLL Enable and wait for Locking
  SCU_PLLCmd(ENABLE);
  // RCLK @96Mhz
  SCU_RCLKDivisorConfig(SCU_RCLK_Div1);
  // AHB @96Mhz
  SCU_HCLKDivisorConfig(SCU_HCLK_Div1);
  // FMI @96Mhz
  SCU_FMICLKDivisorConfig(SCU_FMICLK_Div1);
  // APB @48Mhz
  SCU_PCLKDivisorConfig(SCU_PCLK_Div2);
  // MCLK @96Mhz
  SCU_MCLKSourceConfig(SCU_MCLK_PLL);

  // Enable the clock for EMI
  SCU_AHBPeriphClockConfig(__EMI | __EMI_MEM_CLK, ENABLE);

  // BCLK=96Mhz
  SCU_EMIBCLKDivisorConfig(SCU_EMIBCLK_Div1);

  // Enable the Mux mode
  SCU_EMIModeConfig(SCU_EMI_MUX);

  // ALE length and polarity definition
  SCU_EMIALEConfig(SCU_EMIALE_LEN2, SCU_EMIALE_POLHigh);

  // Enable the GPIO7 Clock
  SCU_APBPeriphClockConfig(__GPIO7 , ENABLE);

  // Enable the GPIO5 Clock
  SCU_APBPeriphClockConfig(__GPIO5 , ENABLE);

  // ------- Configure the GPIO ports ---------
  GPIO_EMIConfig(ENABLE);

  // GPIO7 Configuration
  GPIO_DeInit(GPIO7);
  GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 |
                                GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
  GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt2 ;
  GPIO_Init (GPIO7, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;

  // EMI-A7 mode 16bits
  GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt3;
  GPIO_Init (GPIO7, &GPIO_InitStructure);

  // GPIO5 Configuration
  GPIO_DeInit(GPIO5);
  GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_4 ;
  GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
  GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt3;
  GPIO_Init (GPIO5, &GPIO_InitStructure);

  // EMI default configuration : Reset configuration
  EMI_DeInit();

  // EMI configuration
  EMI_StructInit(&EMI_InitStruct);

  // Number of bus turnaround cycles added between read and write accesses
  EMI_InitStruct.EMI_Bank_IDCY = 0xF;

  // Number of wait states for read accesses
  EMI_InitStruct.EMI_Bank_WSTRD = 0x1F;

  // Number of wait states for write accesses
  EMI_InitStruct.EMI_Bank_WSTWR = 0x1F;

  // Output enable assertion delay from chip select assertion
  EMI_InitStruct.EMI_Bank_WSTROEN = 0x03;

  // Write enable assertion delay from chip select assertion
  EMI_InitStruct.EMI_Bank_WSTWEN = 0x01;

  // This member Controls the memory width
  EMI_InitStruct.EMI_Bank_MemWidth = EMI_Width_HalfWord;

  // Write protection feature
  EMI_InitStruct.EMI_Bank_WriteProtection =  EMI_Bank_NonWriteProtect;

  // Use Bank0 (CS0)
  EMI_Init( EMI_Bank0, &EMI_InitStruct);

  // GPIO Configuration of LCD control port
  GPIO_DeInit(LCD_CONTROL_PORT);
  GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
  GPIO_InitStructure.GPIO_Pin = LCD_RESET_PIN;
  GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull ;
  GPIO_Init (LCD_CONTROL_PORT, &GPIO_InitStructure);

  // Reset display controller
  GPIO_WriteBit(LCD_CONTROL_PORT, LCD_RESET_PIN, Bit_RESET);
  millisecond_delay(1);
  GPIO_WriteBit(LCD_CONTROL_PORT, LCD_RESET_PIN, Bit_SET);
#else
  RESET_LOW;
  millisecond_delay(1);
  RESET_HIGH;
#endif

  // Vcom OTP
  LCD_Cmd(0x0028);
  LCD_Data(0x0006);

  // Power On sequence
  // Oscillation Start
  LCD_Cmd(0x0000);
  LCD_Data(0x0001);

  // Driver Output Control
  LCD_Cmd(0x0001);
  LCD_Data(0x72ef);

  // LCD drive AC control
  LCD_Cmd(0x0002);
  LCD_Data(0x0600);

  // Power control 1
  LCD_Cmd(0x0003);
  LCD_Data(0x6064);

  // Sleep out
  LCD_Cmd(0x0010);
  LCD_Data(0x0000);

  // Delay
  millisecond_delay(30);

  // Entry Mode set
  LCD_Cmd(0x0011);
  LCD_Data(0x6230);

  // Display Control
  LCD_Cmd(0x0007);
  LCD_Data(0x0033);

  // Power Control 2 - Set Vcix2
  LCD_Cmd(0x000C);
  LCD_Data(0x0005);

  // Start Initial Sequence
  // Frame Frequency Control
  LCD_Cmd(0x0025);
  LCD_Data(0xC000);

  // Set GDDRAM address X
  LCD_Cmd(0x004E);
  LCD_Data(0x0000);

  // Set GDDRAM address Y
  LCD_Cmd(0x004F);
  LCD_Data(0x0000);

  // Adjust the Gamma curve registers
  LCD_Cmd(0x0030);
  LCD_Data(0x0200);  // 0x0000 (alternative values)
  LCD_Cmd(0x0031);
  LCD_Data(0x0301);  // 0x0400
  LCD_Cmd(0x0032);
  LCD_Data(0x0304);  // 0x0106
  LCD_Cmd(0x0033);
  LCD_Data(0x0305);  // 0x0106
  LCD_Cmd(0x0034);
  LCD_Data(0x0201);  // 0x0002
  LCD_Cmd(0x0035);
  LCD_Data(0x0000);  // 0x0702
  LCD_Cmd(0x0036);
  LCD_Data(0x0707);  // 0x0707
  LCD_Cmd(0x0037);
  LCD_Data(0x0700);  // 0x0203
  LCD_Cmd(0x003a);
  LCD_Data(0x0f0f);  // 0x1400
  LCD_Cmd(0x003b);
  LCD_Data(0x1F00);  // 0x0F03

  // Power Control 3 - Set Vlcd
  LCD_Cmd(0x000D);
  LCD_Data(0x0005);

  // Power Control 5 - Set VcomH
  LCD_Cmd(0x001E);
  LCD_Data(0x00BD);

  // Power Control 4 - Set Vcom
  LCD_Cmd(0x000E);
  LCD_Data(0x3200);

  //Vertical RAM Address Position
  LCD_Cmd(0x0044);
  LCD_Data(0xEF00);

  //Horizontal RAM Address Position start
  LCD_Cmd(0x0045);
  LCD_Data(0x0000);

  //Horizontal RAM Address Position end
  LCD_Cmd(0x0046);
  LCD_Data(0x013F);

  // Clear display memory
  // Write data to GDDRAM
  LCD_Cmd(0x0022);
  for (i = 0; i < (GuiConst_DISPLAY_BYTES/2); i++);
    LCD_Data(0x0000);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S LastByte;
  GuiConst_INT16S X, Y;

  // Lock GUI resources
  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      // Set GDDRAM address Y
      LCD_Cmd(0x004F);
      LCD_Data(Y);

      // Select start column
      // Set GDDRAM address X
      LCD_Cmd(0x004E);
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin);

      LastByte = GuiConst_BYTES_PR_LINE / 2 - 1;

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write display data
      LCD_Cmd(0x0022);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        LCD_Data(GuiLib_DisplayBuf.Words[Y][X]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_SSD2119

#ifdef LCD_CONTROLLER_TYPE_ST7529

// ============================================================================
//
// ST7529 DISPLAY CONTROLLER
//
// This driver uses one ST7529 display controller, in 3 bytes for 3 pixels mode.
// LCD display 8 bit parallel interface.
// Graphic modes up to 255x160 pixels.
// Modify driver accordingly for other interface types.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W), 5 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Pxx.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define DISP_DATA                 Pxx

#define CHIP_SELECT               Pxx.x = 0     // XCS pin    x
#define CHIP_DESELECT             Pxx.x = 1     // XCS pin    x
#define CHIP_RESET                Pxx.x = 0     // RST pin    x
#define CHIP_RUN                  Pxx.x = 1     // RST pin    x
#define MODE_DATA                 Pxx.x = 1     // A0 pin
#define MODE_COMMAND              Pxx.x = 0     // A0 pin
#define MODE_READ                 Pxx.x = 1     // RW_WR pin
#define MODE_WRITE                Pxx.x = 0     // RW_WR pin
#define RW_ENABLED                Pxx.x = 1     // E_RD pin
#define RW_DISABLED               Pxx.x = 0     // E_RD pin

// EXT mode:
#define CMD_EXT_IN                0x30
#define CMD_EXT_OUT               0x31
// EXT = 0:
#define CMD_DISON                 0xAF
#define CMD_DISOFF                0xAE
#define CMD_DISNOR                0xA6
#define CMD_DISINV                0xA7
#define CMD_COMSCN                0xBB
#define CMD_DISCTRL               0xCA
#define CMD_SLPIN                 0x95
#define CMD_SLPOUT                0x94
#define CMD_LASET                 0x75
#define CMD_CASET                 0x15
#define CMD_DATSDR                0xBC
#define CMD_RAMWR                 0x5C
#define CMD_RAMRD                 0x5D
#define CMD_PTLIN                 0xA8
#define CMD_PTLOUT                0xA9
#define CMD_RMWIN                 0xE0
#define CMD_RMWOUT                0xEE
#define CMD_ASCSET                0xAA
#define CMD_SCSTART               0xAB
#define CMD_OSCON                 0xD1
#define CMD_OSCOFF                0xD2
#define CMD_PWRCTRL               0x20
#define CMD_VOLCTRL               0x81
#define CMD_VOLUP                 0xD6
#define CMD_VOLDOWN               0xD7
#define CMD_EPSRRD1               0x7C
#define CMD_EPSRRD2               0x7D
#define CMD_NOP                   0x25
#define CMD_EPINT                 0x07
// EXT = 1:
#define CMD_GRAY_1_SET            0x20
#define CMD_GRAY_2_SET            0x21
#define CMD_WT_SET                0x22
#define CMD_ANASET                0x32
#define CMD_DITHOFF               0x34
#define CMD_DITHON                0x35
#define CMD_EPCTIN                0xCD
#define CMD_EPCOUT                0xCC
#define CMD_EPMWR                 0xFC
#define CMD_EPMRD                 0xFD

// Command writing
#define WRITE_COMMAND(Val)                                               \
{                                                                        \
  MODE_COMMAND;                                                          \
  MODE_WRITE;                                                            \
  RW_ENABLED;                                                            \
  CHIP_SELECT;                                                           \
  DISP_DATA = Val;                                                       \
  CHIP_DESELECT;                                                         \
  RW_DISABLED;                                                           \
}

// Data writing
#define WRITE_DATA(Val)                                                  \
{                                                                        \
  MODE_DATA;                                                             \
  MODE_WRITE;                                                            \
  RW_ENABLED;                                                            \
  CHIP_SELECT;                                                           \
  DISP_DATA = Val;                                                       \
  CHIP_DESELECT;                                                         \
  RW_DISABLED;                                                           \
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT32S X;

  // Reset controller
  CHIP_RESET;
  CHIP_RUN;

  // EXT = 0
  WRITE_COMMAND(CMD_EXT_IN);

  // Sleep out
  WRITE_COMMAND(CMD_SLPOUT);

  // Oscillator on
  WRITE_COMMAND(CMD_OSCON);

  // Power control set
  WRITE_COMMAND(CMD_PWRCTRL);
  WRITE_DATA(0x08);   // Booster must be on first
  millisecond_delay(1);
  WRITE_COMMAND(CMD_PWRCTRL);
  WRITE_DATA(0x0B);   // Booster, regulator, follower on

  // Electronic control
  WRITE_COMMAND(CMD_VOLCTRL);   // Vop = 14.0V
  WRITE_DATA(0x04);
  WRITE_DATA(0x04);

  // Display control
  WRITE_COMMAND(CMD_DISCTRL);
  WRITE_DATA(0x00);   // CL = X1
  WRITE_DATA((GuiConst_DISPLAY_HEIGHT_HW / 4)-1);   // Duty = display height
  WRITE_DATA(0x00);   // FR inverse-set value

  // Normal display
  WRITE_COMMAND(CMD_DISNOR);

  // COM scan direction
  WRITE_COMMAND(CMD_COMSCN);
  WRITE_DATA(0x01);   // 0->79, 159->80

  // Data scan direction
  WRITE_COMMAND(CMD_DATSDR);
  WRITE_DATA(0x00);   // Normal
  WRITE_DATA(0x00);   // P1,P2,P3 arrangement
  WRITE_DATA(0x02);   // 3B3PD1 grayscale mode (3 bytes for 3 pixels)

  // Line address set
  WRITE_COMMAND(CMD_LASET);
  WRITE_DATA(0x00);   // Start line = 0
  WRITE_DATA(GuiConst_DISPLAY_HEIGHT_HW - 1);   // Display height-1

  // Column address set
  WRITE_COMMAND(CMD_CASET);
  WRITE_DATA(0x00);   // Start column = 0
  WRITE_DATA((GuiConst_DISPLAY_WIDTH_HW / 3) - 1);   // Display width/3-1

  // EXT = 1
  WRITE_COMMAND(CMD_EXT_OUT);

  // Analog circuit set
  WRITE_COMMAND(CMD_ANASET);
  WRITE_DATA(0x00);   // OSC frequency = 12.7kHz
  WRITE_DATA(0x01);   // Booster efficiency = 6KHz
  WRITE_DATA(0x00);   // Bias = 1/14

  // Dithering off
  WRITE_COMMAND(CMD_DITHOFF);

  // EXT = 0
  WRITE_COMMAND(CMD_EXT_IN);

  // Clear display memory
  WRITE_COMMAND(CMD_CASET);
  WRITE_DATA(0x00);   // Start column = 0
  WRITE_DATA((GuiConst_DISPLAY_WIDTH_HW / 3) - 1);   // Display width/3-1
  WRITE_COMMAND(CMD_LASET);
  WRITE_DATA(0x00);   // Start line = 0
  WRITE_DATA(GuiConst_DISPLAY_HEIGHT_HW - 1);   // Display height-1
  WRITE_COMMAND(CMD_RAMWR);
#ifdef GuiConst_COLOR_DEPTH_1
  for (X = 0; X < GuiConst_DISPLAY_BYTES*8; X++)   // Loop through bytes
#else
  for (X = 0; X < GuiConst_DISPLAY_BYTES; X++)   // Loop through bytes
#endif
    WRITE_DATA(0x00);

  // Display ON
  WRITE_COMMAND(CMD_DISON);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT8U Col, Output;
  GuiConst_INT16S X;
  GuiConst_INT16S Y;
  GuiConst_INT16S FirstColumn;
  GuiConst_INT16S LastColumn;
  GuiConst_INT16S StartByte, LastByte;
  GuiConst_INT8U Data;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw on this page
    {
#ifdef GuiConst_COLOR_DEPTH_1
      FirstColumn = 0;
      LastColumn = GuiConst_DISPLAY_WIDTH_HW / 3-1;
#else
      FirstColumn = GuiLib_DisplayRepaint[Y].ByteBegin / 3;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < GuiConst_BYTES_PR_LINE - 1)
        LastColumn = GuiLib_DisplayRepaint[Y].ByteEnd / 3;
      else
        LastColumn = (GuiConst_BYTES_PR_LINE - 1) / 3;
#endif
      // EXT = 0
      WRITE_COMMAND(CMD_EXT_IN);
      // Select line
      WRITE_COMMAND(CMD_LASET);
      WRITE_DATA(Y);
      WRITE_DATA(GuiConst_DISPLAY_HEIGHT_HW - 1);   // Display height-1
      // Select column
      WRITE_COMMAND(CMD_CASET);
      WRITE_DATA(FirstColumn);
      WRITE_DATA(LastColumn);
      // Write display data
      WRITE_COMMAND(CMD_RAMWR);
#ifdef GuiConst_COLOR_DEPTH_1
      for (X = 0; X <= LastColumn; X++)
      {
        Data = GuiLib_DisplayBuf[Y][X];
        for (Col = 0; Col < 8; Col++)
        {
          if(Data & 0x80)
            Output = 0x00;
          else
            Output = 0xF0;

          WRITE_DATA(Output);

          Data <<= 1;
        }
      }
#else
      for (X = 3 * FirstColumn; X <= 3 * LastColumn + 2; X++)
        WRITE_DATA(GuiLib_DisplayBuf[Y][X]);
#endif
      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_ST7529

#ifdef LCD_CONTROLLER_TYPE_ST7541

// ============================================================================
//
// ST7541 DISPLAY CONTROLLER
//
// This driver uses one ST7541 display controller with on-chip oscillator.
// LCD display 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 128x128 pixels
//
// Compatible display controllers:
//   ST7571
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 2
//   Color mode: Grayscale
//   Color depth: 2 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Uncomment for ST7571
// #define ST7571

#define RESET_ON   PORTG &= ~0x02
#define RESET_OFF  PORTG |=  0x02
#define CS1_LOW    PORTG &= ~0x04
#define CS1_HIGH   PORTG |=  0x04
#define E_LOW      PORTG &= ~0x08
#define E_HIGH     PORTG |=  0x08
#define A0_LOW     PORTG &= ~0x20
#define A0_HIGH    PORTG |=  0x20
#define RW_LOW     PORTG &= ~0x40
#define RW_HIGH    PORTG |=  0x40
#define DB         PORTA

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void lcdPortWriteByte(GuiConst_INT8U A0, GuiConst_INT8U Val)
{
  CS1_LOW;
  RW_LOW;
  if(A0)
    A0_HIGH;
  else
    A0_LOW;
  E_HIGH;
  DB = Val;
  E_LOW;
  CS1_HIGH;
}

#define lcdModeSet(frame,boost)    lcdPortWriteByte(0,0x38); lcdPortWriteByte(0,(frame<<4)|(boost<<2))
#define lcdSelectColumnHighNibble(columnno)                  lcdPortWriteByte(0,0x10 | ((columnno >> 4) & 0x0f))
#define lcdSelectColumnLowNibble(columnno)                   lcdPortWriteByte(0,0x00 | ((columnno) & 0x0f))
#define lcdDisplaySw(onOff)                                  lcdPortWriteByte(0,0xae | ((onOff) ? 1 : 0))
#define lcdSelectStartLine(lineno) lcdPortWriteByte(0,0x40); lcdPortWriteByte(0,(lineno) & 0x7F)
#define lcdSetInitialCOM1(lineno)  lcdPortWriteByte(0,0x44); lcdPortWriteByte(0,(lineno) & 0x7F)
#define lcdSetDuty(ratio)          lcdPortWriteByte(0,0x48); lcdPortWriteByte(0,(ratio) & 0x7f)
#define lcdPowerController(booster, vreg, vfollow)           lcdPortWriteByte(0,0x28 | ((booster) ? 4 : 0) | ((vreg) ? 2 : 0) | ((vfollow) ? 1 : 0))
#define lcdSelectDC(ratio)                                   lcdPortWriteByte(0,0x64 | ((ratio) & 0x03))
#define lcdV5ResistorRatio(ratio)                            lcdPortWriteByte(0,0x20 | ((ratio) & 0x07))
#define lcdElectronicVol(level)    lcdPortWriteByte(0,0x81); lcdPortWriteByte(0,(level) & 0x3f)
#define lcdSelectLCDBias(ratio)                              lcdPortWriteByte(0,0x50 | ((ratio) & 0x07))
#define lcdSetADCreverse                                     lcdPortWriteByte(0,0xA1)
#define lcdSetSHLreverse                                     lcdPortWriteByte(0,0xC8)
#define lcdOscilatorOn                                       lcdPortWriteByte(0,0xAB)
#define lcdReset                                             lcdPortWriteByte(0,0xe2)
#define lcdSelectPage(pageno)                                lcdPortWriteByte(0,0xb0 | ((pageno) & 0x0f))
#define lcdSelectGrayReg(reg)                                lcdPortWriteByte(0,0x88 | (reg & 0x07))
#define lcdSetGrayValue(value)                               lcdPortWriteByte(0,value)
#define lcdDataByte(dataval)                                 lcdPortWriteByte(1,dataval)

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  RESET_ON;
  millisecond_delay(10);
  RESET_OFF;

  // Reset controller
  lcdReset;

#ifdef ST7571
  // Set FR frequency 85Hz and Booster Efficiency Level 3
  lcdModeSet(11,2);
#endif

  lcdOscilatorOn;
  lcdV5ResistorRatio(4);
  lcdElectronicVol(0x24);
  //lcdSelectLCDBias(ratio);
  lcdSelectDC(0);

#ifndef ST7571
  // Set grayscale palette registers
  lcdSelectGrayReg(0);
  lcdSetGrayValue(0x00);
  lcdSelectGrayReg(1);
  lcdSetGrayValue(0x00);
  lcdSelectGrayReg(2);
  lcdSetGrayValue(0x33);
  lcdSelectGrayReg(3);
  lcdSetGrayValue(0x33);
  lcdSelectGrayReg(4);
  lcdSetGrayValue(0x66);
  lcdSelectGrayReg(5);
  lcdSetGrayValue(0x66);
  lcdSelectGrayReg(6);
  lcdSetGrayValue(0x99);
  lcdSelectGrayReg(7);
  lcdSetGrayValue(0x99);
#endif

  // Power on sequence
  millisecond_delay(200);
  lcdPowerController(1,0,0);
  lcdSelectDC(3);
  millisecond_delay(200);
  lcdPowerController(1,1,0);
  millisecond_delay(200);
  lcdPowerController(1,1,1);

  lcdSelectStartLine(0);

  // Clear display memory
  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Select page
    lcdSelectPage(PageNo);
    // Select column
    lcdSelectColumnHighNibble(0);
    lcdSelectColumnLowNibble(0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
    {
      lcdDataByte(0x00);
      lcdDataByte(0x00);
    }
  }

  lcdDisplaySw(1);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      lcdSelectPage(PageNo);

      // Select column
      lcdSelectColumnHighNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      lcdSelectColumnLowNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
      {
        lcdDataByte(GuiLib_DisplayBuf[0][PageNo][N]);
        lcdDataByte(GuiLib_DisplayBuf[1][PageNo][N]);
      }
      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_ST7541

#ifdef LCD_CONTROLLER_TYPE_ST7561

// ============================================================================
//
// ST7561 DISPLAY CONTROLLER
//
// This driver uses one ST7561 display controller with on-chip oscillator.
// LCD display 8 bit parallel interface type 68000.
// Modify driver accordingly for other interface types.
// Graphic modes up to 396x132 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define RESET_ON   PORTG &= ~0x02
#define RESET_OFF  PORTG |=  0x02
#define CS1_LOW    PORTG &= ~0x04
#define CS1_HIGH   PORTG |=  0x04
#define E_LOW      PORTG &= ~0x08
#define E_HIGH     PORTG |=  0x08
#define A0_LOW     PORTG &= ~0x20
#define A0_HIGH    PORTG |=  0x20
#define RW_LOW     PORTG &= ~0x40
#define RW_HIGH    PORTG |=  0x40
#define DB         PORTA

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void lcdPortWriteByte(GuiConst_INT8U A0, GuiConst_INT8U Val)
{
  CS1_LOW;
  RW_LOW;
  if(A0)
    A0_HIGH;
  else
    A0_LOW;
  E_HIGH;
  DB = Val;
  E_LOW;
  CS1_HIGH;
}

#define lcdDisplayOn                                         lcdPortWriteByte(0,0xAF)
#define lcdDisplayOff                                        lcdPortWriteByte(0,0xAE)
#define lcdReverse                                           lcdPortWriteByte(0,0xA7)
#define lcdNonReverse                                        lcdPortWriteByte(0,0xA6)
#define lcdNormal                                            lcdPortWriteByte(0,0xA6)
#define lcdAllLightOn                                        lcdPortWriteByte(0,0xA5)
#define lcdAllLightOff                                       lcdPortWriteByte(0,0xA4)
#define lcdScanMode(mode)          lcdPortWriteByte(0,0xC4); lcdPortWriteByte(0,mode & 0x03)
#define lcdSelectStartLine(lineno) lcdPortWriteByte(0,0x8A); lcdPortWriteByte(0,lineno & 0x7F)
#define lcdSelectPage(pageno)      lcdPortWriteByte(0,0xB1); lcdPortWriteByte(1,pageno & 0x3F)
#define lcdSelectColumn(columnno)  lcdPortWriteByte(0,0x13); lcdPortWriteByte(1,(GuiConst_INT8U)(columnno >> 8));\
lcdPortWriteByte(1,(GuiConst_INT8U)columnno);
#define lcdDataByte(dataval)       lcdPortWriteByte(0,0x1D); lcdPortWriteByte(1,dataval)
#define lcdDirColumn                                         lcdPortWriteByte(0,0x84)
#define lcdDirPage                                           lcdPortWriteByte(0,0x85)
#define lcdColumnNormal                                      lcdPortWriteByte(0,0xA0)
#define lcdCoumnnReverse                                     lcdPortWriteByte(0,0xA1)
#define lcdNumberOfLines(lines)    lcdPortWriteByte(0,0x6D); lcdPortWriteByte(1,lines); lcdPortWriteByte(1,0)
#define lcdOscilatorOn                                       lcdPortWriteByte(0,0xAB)
#define lcdOscilatorOff                                      lcdPortWriteByte(0,0xAA)
#define lcdSelectFreq(p1,p2)       lcdPortWriteByte(0,0x5F); lcdPortWriteByte(1,p1); lcdPortWriteByte(1,p2)
#define lcdPowerController(booster2,booster1,v3,vlcd);       lcdPortWriteByte(0,0x25); \
lcdPortWriteByte(1,0x00|(booster2 ? 8 : 0)|(booster1 ? 4 : 0)|(v3 ? 2 : 0)|(vlcd ? 1 : 0))
#define lcdStepUpFreq(value)       lcdPortWriteByte(0,0x55); lcdPortWriteByte(1,value & 0x07)
#define lcdSetBooster(ratio)       lcdPortWriteByte(0,0x2B); lcdPortWriteByte(1,ratio & 0x07)
#define lcdSelectLcdBias(ratio)    lcdPortWriteByte(0,0xA2); lcdPortWriteByte(1,ratio & 0x07)
#define lcdElectronicVol(level)    lcdPortWriteByte(0,0x81); lcdPortWriteByte(1,(GuiConst_INT8U)level);\
lcdPortWriteByte(1,(GuiConst_INT8U)(level >> 8));


// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  RESET_ON;
  millisecond_delay(10);
  RESET_OFF;

  lcdNumberOfLines(0x20); // 132 lines
  lcdSelectFreq(0,0);     // 408 kHz
  lcdOscilatorOn;
  lcdStepUpFreq(0);       // 0-40kHz, 1-30kHz, 2-20kHz, 3-15kHz, 4-10kHz, 5-5kHz, 6-2,5kHz, 7-1.2kHz
  lcdSelectlcdBias(4);    // 0-1/12, 1-1/11, 2-1/10, 3-1/9, 4-1/8, 5-1/7, 6-1/6, 7-1/5
  lcdSetBuster(1);        // 0-2x, 1-3x, 2-4x, 3-5x, 4-6x
  lcdElectronicVol(255);  // Range 0 - 512
  lcdPowerController(1,1,1,1); // Built-in power enable

  // Clear display memory
  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Select page
    lcdSelectPage(PageNo);
    // Select column
    lcdSelectColumn(0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
      lcdDataByte(0x00);
  }

  millisecond_delay(300);
  lcdDisplayOn;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      lcdSelectPage(PageNo);

      // Select column
      lcdSelectColumn(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);
      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_ST7561

#ifdef LCD_CONTROLLER_TYPE_ST7565

// ============================================================================
//
// ST7565 DISPLAY CONTROLLER
//
// This driver uses two ST7565 display controllers.
// LCD display 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 264x65 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 2
//   Number of display controllers, vertically: 1
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define RESET_CS1  PORTG &= ~0x02
#define SET_CS1    PORTG |=  0x02
#define RESET_CS2  PORTG &= ~0x04
#define SET_CS2    PORTG |=  0x04
#define RESET_E    PORTG &= ~0x08
#define SET_E      PORTG |=  0x08
#define RESET_A0   PORTG &= ~0x20
#define SET_A0     PORTG |=  0x20
#define RESET_RW   PORTG &= ~0x40
#define SET_RW     PORTG |=  0x40
#define DB         PORTA

#define lcdReset                                          lcdPortWriteByte(0,0xe2)
#define lcdV5ResistorRatio(ratio)                         lcdPortWriteByte(0,0x20 | ((ratio) & 0x07))
#define lcdPowerController(booster, vreg, vfollow)        lcdPortWriteByte(0,0x28 | ((booster) ? 4 : 0) | ((vreg) ? 2 : 0) | ((vfollow) ? 1 : 0))
#define lcdElectronicVol(level) lcdPortWriteByte(0,0x81); lcdPortWriteByte(0,(level) & 0x3f)
#define lcdDisplaySw(onOff)                               lcdPortWriteByte(0,0xae | ((onOff) ? 1 : 0))
#define lcdSelectStartLine(lineno)                        lcdPortWriteByte(0,0x40 | ((lineno) & 0x3f))
#define lcdSelectPage(pageno)                             lcdPortWriteByte(0,0xb0 | ((pageno) & 0x0f))
#define lcdSelectColumnHighNibble(columnno)               lcdPortWriteByte(0,0x10 | ((columnno >> 4) & 0x0f))
#define lcdSelectColumnLowNibble(columnno)                lcdPortWriteByte(0,0x00 | ((columnno) & 0x0f))
#define lcdDataByte(dataval)                              lcdPortWriteByte(1,dataval)

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void lcdPortWriteByte(GuiConst_INT8U A0, GuiConst_INT8U Val)
{
  millisecond_delay(1);
  RESET_RW;
  if(A0)
    SET_A0;
  else
    RESET_A0;
  millisecond_delay(1);
  SET_E;
  DB = Val;
  millisecond_delay(1);
  RESET_E;
}


// Selects controller 1 or 2
void ControllerSelect (GuiConst_INT8U index)
{
  switch (index)
  {
    case 1:
      SET_CS1;
      RESET_CS2;
      break;

    case 2:
      RESET_CS1;
      SET_CS2;
      break;

    default:
      RESET_CS1;
      RESET_CS2;
  }
}


// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X,C;

  for(C = 1; C <= 2; C++)
  {
    ControllerSelect (C);

    // Reset controller
    lcdReset;
    lcdV5ResistorRatio(4);
    lcdPowerController(1,1,1);
    lcdElectronicVol(0x24);

    // Display start line in RAM is 000000b
    lcdSelectStartLine(0);

    // Clear display memory
    for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
    {
      // Select page
      lcdSelectPage(PageNo);
      // Select column
      lcdSelectColumnHighNibble(0);
      lcdSelectColumnLowNibble(0);
      // Write display data
      for (X = 0; X < GuiConst_BYTES_PR_SECTION; X++) // Loop through bytes
        lcdDataByte(0x00);
    }
    lcdDisplaySw(1);
  }
  ControllerSelect (0);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select controller
      ControllerSelect (1);
      // Select page
      lcdSelectPage(PageNo);

      // Select column
      lcdSelectColumnHighNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      lcdSelectColumnLowNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      // Write display data
      LastByte = GuiConst_BYTES_PR_SECTION - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);

      // Reset repaint parameters
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= GuiConst_BYTES_PR_SECTION)
      // Something to redraw in second section
        GuiLib_DisplayRepaint[PageNo].ByteBegin = GuiConst_BYTES_PR_SECTION;
      else // Done with this line
        GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;

      if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
      // Something to redraw in second section
      {
        // Select controller
        ControllerSelect (2);
        // Select page
        lcdSelectPage(PageNo);

        // Select column
        lcdSelectColumnHighNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin
        - GuiConst_BYTES_PR_SECTION);
        lcdSelectColumnLowNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin
        - GuiConst_BYTES_PR_SECTION);
        // Write display data
        for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin;
        N <= GuiLib_DisplayRepaint[PageNo].ByteEnd; N++)
          lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);

        // Reset repaint parameters
        GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
      }
    }
  }

  // Finished drawing
  ControllerSelect (0);

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_ST7565

#ifdef LCD_CONTROLLER_TYPE_ST7586

// ============================================================================
//
// ST7586 DISPLAY CONTROLLER
//
// This driver uses one ST7586 display controller with on-chip oscillator.
// LCD display 8 bit parallel interface type 8080.
// Modify driver accordingly for other interface types.
// Graphic modes up to 384x160 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired setting
//   IF3 IF2 IF1 MPU interface type
//   H   H   L   80 series 8-bit parallel
//   H   L   L   68 series 8-bit parallel
//   L   H   H   8-bit serial (4-Line)
//   L   H   L   9-bit serial (3-Line)

#define DISP_DATA                 Pxx           // D[7-0] pins
#define RESET_ON                  Pxx.x = 0     // RSTB pin
#define RESET_OFF                 Pxx.x = 1     // RSTB pin
#define CHIP_ENABLED              Pxx.x = 0     // CSB pin
#define CHIP_DISABLED             Pxx.x = 1     // CSB pin
#define MODE_COMMAND              Pxx.x = 0     // A0 pin
#define MODE_DATA                 Pxx.x = 1     // A0 pin
#define WR_LOW                    Pxx.x = 0     // EWR pin
#define WR_HIGH                   Pxx.x = 1     // EWR pin
#define RD_LOW                    Pxx.x = 0     // ERD pin
#define RD_HIGH                   Pxx.x = 1     // ERD pin

// 8 bit parallel 8080 type interface
void LCD_Cmd(GuiConst_INT8U v)
{
  MODE_COMMAND;
  RD_HIGH;
  CHIP_ENABLED;
  WR_LOW;
  DISP_DATA = v;
  WR_HIGH;
  CHIP_DISABLED;
}

void LCD_Data(GuiConst_INT8U v)
{
  MODE_DATA;
  RD_HIGH;
  CHIP_ENABLED;
  WR_LOW;
  DISP_DATA = v;
  WR_HIGH;
  CHIP_DISABLED;
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT32U i;

  // Reset controller
  RESET_ON;
  millisecond_delay(100);
  RESET_OFF;

  // Set sleep out mode
   LCD_Cmd(0x11);

  // Analog Control enable
  LCD_Cmd(0xD0);
  LCD_Data(0x1D);

  // DDRAM enable
  LCD_Cmd(0x39);

  // DDRAM Interface enable
  LCD_Cmd(0x3A);
  LCD_Data(0x02);

  // Normal display
  LCD_Cmd(0x21);

  // Partial mode off
  LCD_Cmd(0x13);

  // Display control
  LCD_Cmd(0x36);
  // bit7=0: COM0-COM159, bit7=1: COM159-COOM0
  // bit6=0: SEG0-SEG383, bit6=1: SEG383-SEG0
  LCD_Data(0x00);

  // Start line
  LCD_Cmd(0x37);
  LCD_Data(0x00);

  // Display duty
  LCD_Cmd(0xB0);
  LCD_Data(0x9F);

  // First COM
  LCD_Cmd(0xB1);
  LCD_Data(0x00);

  // FOSC divider
  LCD_Cmd(0xB3);
  // 0 - no division, 1 - x/2, 2 - x/4, 3 - x/8
  LCD_Data(0x00);

  // Set Vop  V0=3.6+(Vop[8:0]+VOF[6:0]+VopIncStep-VopDecStep)x0.04
  LCD_Cmd(0xC0);
  LCD_Data(0x42);
  LCD_Data(0x01);

  // Vop offset
  LCD_Cmd(0xC7);
  LCD_Data(0x00);

  // Set Bias
  LCD_Cmd(0xC3);
  // 0 - 1/14, 1 - 1/13, 2 - 1/12, 3 - 1/11, 4 - 1/10, 5 - 1/9, 6 - 1/7
  LCD_Data(0x04);

  // Set booster level
  LCD_Cmd(0xC4);
  // booster level = value + 1
  LCD_Data(0x07);

  // Clear display memory
  // Write display data
  LCD_Cmd(0x2C);
  for (i = 0; i < (GuiConst_DISPLAY_BYTES*5/4); i++)
    LCD_Data(0x00);

  // Display ON
  LCD_Cmd(0x29);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S StartByte, LastByte;
  GuiConst_INT32U DataBlock;
  GuiConst_INT8U Output, Col;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(0x2B);
      // Start line
      LCD_Data(0x00);
      LCD_Data(Y);
      // End line
      LCD_Data(0x00);
      LCD_Data(Y);

      // Must by multiple of 3
      StartByte = (GuiLib_DisplayRepaint[Y].ByteBegin / 3) * 3;

      // Select column
      LCD_Cmd(0x2A);
      // Start column
      LCD_Data(0);
      // Conversion to column numbering in DRAM
      LCD_Data(StartByte / 3 * 8);
      // End column
      LCD_Data(0x00);
      LCD_Data(0x7F);

      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;
      // Must by multiple of 3
      LastByte = (LastByte / 3) * 3;

      // Write display data
      LCD_Cmd(0x2C);
      for (X = StartByte; X <= LastByte; X += 3)
      {
        DataBlock = GuiLib_DisplayBuf[Y][X];
        DataBlock <<= 8;
        DataBlock |= GuiLib_DisplayBuf[Y][X+1];
        DataBlock <<= 8;
        DataBlock |= GuiLib_DisplayBuf[Y][X+2];
        DataBlock <<= 8;

        for (Col = 0; Col < 8; Col++)
        {
          Output = 0x00;

          if(DataBlock & 0x80000000)
            Output = 0xC0;

          if(DataBlock & 0x40000000)
            Output |= 0x18;

          if(DataBlock & 0x20000000)
            Output |= 0x03;

          LCD_Data(Output);

          DataBlock <<= 3;
        }
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_ST7586

#ifdef LCD_CONTROLLER_TYPE_ST7637

// ============================================================================
//
// ST7637 DISPLAY CONTROLLER
//
// This driver uses one ST7637 display controller with on-chip oscillator.
// LCD display in 16 bit 80-system parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 132x132 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired setting
//   IF3 IF2 IF1 MPU interface type
//   H   H   H   80 series 16-bit parallel

#define DISP_DATA            Pxx           // Pins D15 - D0
#define RESET_ON             Pxx.x = 0     // RST pin
#define RESET_OFF            Pxx.x = 1     // RST pin
#define CHIP_SELECT          Pxx.x = 0     // CS pin
#define CHIP_DESELECT        Pxx.x = 1     // CS pin
#define MODE_DATA            Pxx.x = 1     // A0 pin
#define MODE_COMMAND         Pxx.x = 0     // A0 pin
#define MODE_WRITE_OFF       Pxx.x = 1     // RW_WR pin
#define MODE_WRITE_ON        Pxx.x = 0     // RW_WR pin
#define MODE_READ_OFF        Pxx.x = 1     // E_RD pin
#define MODE_READ_ON         Pxx.x = 0     // E_RD pin

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Command writing in parallel interface
void LCD_Cmd(GuiConst_INT16U Val)
{
  MODE_COMMAND;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

// Data writing in parallel interface
void LCD_Data(GuiConst_INT16U Val)
{
  MODE_DATA;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

void GuiDisplay_Init(void)
{
  GuiConst_INT32U j;

  // Set E_RD signal high
  MODE_READ_OFF;

  // Reset controller
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;

  millisecond_delay(120);

  // Display off
  LCD_Cmd(0x28);

  // Sleep Out
  LCD_Cmd(0x11);

  millisecond_delay(50);

  // Set Vop by initial Module
  LCD_Cmd(0xC0);

  // Vop = 14.2V
  LCD_Data(0x09);
  LCD_Data(0x01); // Base on the module

  // Bias select
  LCD_Cmd(0xC3);
  LCD_Data(0x03); // 1/9 Bias, base on Module

  // Setting Booster times
  LCD_Cmd(0xC4);
  LCD_Data(0x07); // Booster X 8

  // Booster off
  LCD_Cmd(0xC5);
  LCD_Data(0x01); // BE = 0x01 (Level 2)

  // Vg with booster x2 control
  LCD_Cmd(0xCB);
  LCD_Data(0x01); // Vg from Vdd2

  // Analog circuit setting
  LCD_Cmd(0xD0);
  LCD_Data(0x1D);

  // Color mode = 65k
  LCD_Cmd(0x3A);
  LCD_Data(0x05);

  // Memory Access Control
  LCD_Cmd(0x36);
  LCD_Data(0x00);

  // Duty = 132 duty
  LCD_Cmd(0xB0);
  LCD_Data(0x83);

  // Display Inversion OFF
  LCD_Cmd(0x20);

  // Gamma table values
  LCD_Cmd(0xF9);
  LCD_Data(0x00);
  LCD_Data(0x02);
  LCD_Data(0x04);
  LCD_Data(0x06);
  LCD_Data(0x08);
  LCD_Data(0x0A);
  LCD_Data(0x0C);
  LCD_Data(0x0E);
  LCD_Data(0x10);
  LCD_Data(0x12);
  LCD_Data(0x14);
  LCD_Data(0x16);
  LCD_Data(0x18);
  LCD_Data(0x1A);
  LCD_Data(0x1C);
  LCD_Data(0x1E);

  // Write data to display RAM
  LCD_Cmd(0x2C);
  // Clear display RAM
  for (j = 0; j < GuiConst_DISPLAY_BYTES/2; j++)
    LCD_Data(0x0000);

  // Display On
  LCD_Cmd(0x29);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;
  GuiConst_INT32U Address;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(0x2B);
      // Start line
      LCD_Data(Y);
      // End line
      LCD_Data(Y);

      // Select column
      LCD_Cmd(0x2A);
      // Start column
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin);
      // End column
      LCD_Data(0x83);

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write data to display RAM
      LCD_Cmd(0x2C);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        LCD_Data((GuiConst_INT16U)GuiLib_DisplayBuf.Words[Y][X]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_ST7637

#ifdef LCD_CONTROLLER_TYPE_ST7715

// ============================================================================
//
// ST7715 DISPLAY CONTROLLER
//
// This driver uses one ST7715 display controller with on-chip oscillator.
// LCD display in 16 bit 80-system parallel interface
// or serial spi interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 132x132 pixels.
//
// Compatible display controllers:
//   ST7715R
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired setting
//  IM2 IM1 IM0  MPU interface type
//  1   0   1    80 series 16-bit parallel
//  0   0   0    serial spi interface
//
// SPI4W pin connected to 1 for 4 wire SPI interface
//
// EXCT pin connected to 1 for using all commands

// Uncomment for SPI interface
//#define SPI_INTERFACE

#ifdef SPI_INTERFACE

#define RESET_ON             Pxx.x = 0     // RESX pin
#define RESET_OFF            Pxx.x = 1     // RESX pin
#define CHIP_SELECT          Pxx.x = 0     // CSX pin
#define CHIP_DESELECT        Pxx.x = 1     // CSX pin
#define MODE_DATA            Pxx.x = 1     // D/CX pin
#define MODE_COMMAND         Pxx.x = 0     // D/CX pin

// Comment out if not using spi hardware of microcontroller
#define HARDWARE_SPI

#ifdef HARDWARE_SPI
// Implement hardware related SPI function
void SPI_send (GuiConst_INT8U sendval)
{
  spi_hardware(GuiConst_INT8U sendval);
}
#else
// Using pin generated SPI function
// Allocations for SPI signals at 8 bits port
// Fill actual pin position on the port and port name, also enable port as an output if needed
#define CSB        1<<
#define SCK        1<<
#define SDI        1<<
#define SPIPORT

// Bit handling macros
#define SETBIT(x,y)    (x |= (y))
#define CLRBIT(x,y)    (x &= (~(y)))
#define CHKBIT(x,y)    (x & (y))
#define FLPBIT(x,y)    (x ^= (y))

void SPI_send (GuiConst_INT8U sendval)
{
  GuiConst_INT8U count;

  if (sendval & 0x80)
    SETBIT(SPIPORT,SDI);
  else
    CLRBIT(SPIPORT,SDI);
  CLRBIT(SPIPORT,SCK);
  SETBIT(SPIPORT,SCK);

  for (count = 0; count < 7; count++)
  {
    sendval <<= 1;

    if (sendval & 0x80)
      SETBIT(SPIPORT,SDI);
    else
      CLRBIT(SPIPORT,SDI);
    CLRBIT(SPIPORT,SCK);
    SETBIT(SPIPORT,SCK);
  }
}
#endif

// Command writing in serial interface
void LCD_Cmd(GuiConst_INT16U Val)
{
  MODE_COMMAND;
  CHIP_SELECT;
  SPI_send ((GuiConst_INT8U) Val);
  CHIP_DESELECT;
}

// Data writing in serial interface
void LCD_Data(GuiConst_INT16U Val)
{
  MODE_DATA;
  CHIP_SELECT;
  SPI_send ((GuiConst_INT8U) (Val>>8));
  SPI_send ((GuiConst_INT8U) Val);
  CHIP_DESELECT;
}

#else
// Parallel interface
#define DISP_DATA            Pxx           // Pins D15 - D0
#define RESET_ON             Pxx.x = 0     // RESX pin
#define RESET_OFF            Pxx.x = 1     // RESX pin
#define CHIP_SELECT          Pxx.x = 0     // CSX pin
#define CHIP_DESELECT        Pxx.x = 1     // CSX pin
#define MODE_DATA            Pxx.x = 1     // D/CX pin
#define MODE_COMMAND         Pxx.x = 0     // D/CX pin
#define MODE_WRITE_OFF       Pxx.x = 1     // WRX pin
#define MODE_WRITE_ON        Pxx.x = 0     // WRX pin
#define MODE_READ_OFF        Pxx.x = 1     // RDX pin
#define MODE_READ_ON         Pxx.x = 0     // RDX pin

// Command writing in parallel interface
void LCD_Cmd(GuiConst_INT16U Val)
{
  MODE_COMMAND;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

// Data writing in parallel interface
void LCD_Data(GuiConst_INT16U Val)
{
  MODE_DATA;
  DISP_DATA = Val;
  CHIP_SELECT;
  MODE_WRITE_ON;
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

#endif

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

void GuiDisplay_Init(void)
{
  GuiConst_INT32U j;

  // Set RDX signal high
  MODE_READ_OFF;

  // Reset controller
  RESET_ON;
  millisecond_delay(1);
  RESET_OFF;

  millisecond_delay(120);

  // Sleep Out
  LCD_Cmd(0x11);

  millisecond_delay(50);

  // Set Vop by initial Module
  LCD_Cmd(0xC0);
  // Vop = 14.2V
  LCD_Data(0x02);
  LCD_Data(0x70); // Base on the module

  // Color mode = 65k
  LCD_Cmd(0x3A);
  LCD_Data(0x05);

  // Memory Access Control
  LCD_Cmd(0x36);
  LCD_Data(0x00);

  // Write data to display RAM
  LCD_Cmd(0x2C);
  // Clear display RAM
  for (j = 0; j < GuiConst_DISPLAY_BYTES/2; j++)
    LCD_Data(0x0000);

  // Display On
  LCD_Cmd(0x29);
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;
  GuiConst_INT32U Address;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Select line
      LCD_Cmd(0x2B);
      // Dummy parameter
      LCD_Data(0x00);
      // Start line
      LCD_Data(Y);
      // Dummy parameter
      LCD_Data(0x00);
      // End line
      LCD_Data(Y);

      // Select column
      LCD_Cmd(0x2A);
      // Dummy parameter
      LCD_Data(0x00);
      // Start column
      LCD_Data(GuiLib_DisplayRepaint[Y].ByteBegin);
      // Dummy parameter
      LCD_Data(0x00);
      // End column
      LCD_Data(0x83);

      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write data to display RAM
      LCD_Cmd(0x2C);
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
        LCD_Data((GuiConst_INT16U)GuiLib_DisplayBuf.Words[Y][X]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_ST7715

#ifdef LCD_CONTROLLER_TYPE_ST7920

// ============================================================================
//
// ST7920 DISPLAY CONTROLLER
//
// This driver uses one ST7920 display controller with on-chip oscillator.
// LCD display in 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 64x256 pixels (without extension ST7921 32x64 pixels)
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardwired connection
// PSB = 1 - parallel mode

#define DB         PORTA                  // D7-D0 pins
#define RESET_ON   PORTG &= ~0x02         // XRESET pin
#define RESET_OFF  PORTG |=  0x02         // XRESET pin
#define E_LOW      PORTG &= ~0x08         // E pin
#define E_HIGH     PORTG |=  0x08         // E pin
#define A0_LOW     PORTG &= ~0x20         // RS pin
#define A0_HIGH    PORTG |=  0x20         // RS pin
#define RW_LOW     PORTG &= ~0x40         // RW pin
#define RW_HIGH    PORTG |=  0x40         // RW pin
#define NOP        asm ("nop")            // nop code, adjust according to compiler syntax

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Write command or data
void lcdPortWriteByte(GuiConst_INT8U A0, GuiConst_INT8U Val)
{
  RW_LOW;
  if(A0)
    A0_HIGH;
  else
    A0_LOW;
  E_HIGH;
  DB = Val;
  // NOP;
  E_LOW;
}

// Command definition
#define lcdSetAddress(line, column) lcdPortWriteByte(0,0x80|((line)&0x3f)); lcdPortWriteByte(0,0x80|((column)&0x0f))
#define lcdFunctionSet(dl, re, gr)  lcdPortWriteByte(0,0x20 | ((dl) ? 16 : 0) | ((re) ? 4 : 0) | ((gr) ? 2 : 0))
#define lcdEntryMode(id, shift)     lcdPortWriteByte(0,0x04 | ((id) ? 2 : 0) | ((shift) ? 1 : 0))
#define lcdClearDDRAM               lcdPortWriteByte(0,0x01)
#define lcdDisplayOn                lcdPortWriteByte(0,0x0c)
#define lcdDataByte(dataval)        lcdPortWriteByte(1,dataval)

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  // Hardware reset
  RESET_ON;
  millisecond_delay(40);
  RESET_OFF;

  // Select 8 bit mpu interface
  lcdFunctionSet(1,0,0);
  millisecond_delay(1);

  // Clear DDRAM and set id and shift mode
  lcdClearDDRAM;

  // Select extended instruction set
  lcdFunctionSet(1,1,1);
  millisecond_delay(1);

  // Select graphic display on
  lcdFunctionSet(1,0,1);
  millisecond_delay(1);

  // Clear graphic display memory
  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Set page and column
    lcdSetAddress(PageNo, 0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++) // Loop through bytes
      lcdDataByte(0x00);
  }
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Set page and column
      if(GuiLib_DisplayRepaint[PageNo].ByteBegin % 2)
      {
        lcdSetAddress(PageNo, GuiLib_DisplayRepaint[PageNo].ByteBegin - 1);
      }
      else
      {
        lcdSetAddress(PageNo, GuiLib_DisplayRepaint[PageNo].ByteBegin);
      }

      // Calculate end byte
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
      {
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
        if((LastByte%2)==0)
          LastByte++;
      }

      // Write data
      if(GuiLib_DisplayRepaint[PageNo].ByteBegin % 2)
      {
        for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin - 1;
             N <= LastByte;
             N++)
          lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);
      }
      else
      {
        for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
          lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);
      }
     // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_ST7920

#ifdef LCD_CONTROLLER_TYPE_T6963

// ============================================================================
//
// T6963 DISPLAY CONTROLLER
//
// This driver uses one T6963 controller.
// LCD display 8-bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x128 pixels.
// Combination of number of columns and number of lines must not cause
// the frequency to exceed 5.5MHz.
//
// Compatible display controllers:
//   AX6963, WG24064
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (_Pxx) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

#define CTRL_BYTE_FIRST_PAGE 0xB8
#define CTRL_BYTE_FIRST_POS  0x40

#define ENABLE_DISPLAY       _P30 = 0
#define DISABLE_DISPLAY      _P30 = 1
#define ENABLE_READ          _P31 = 0
#define DISABLE_READ         _P31 = 1
#define ENABLE_WRITE         _P32 = 0
#define DISABLE_WRITE        _P32 = 1
#define CD_COMMAND           _P33 = 1
#define CD_DATA              _P33 = 0
#define DISPLAY_ON           _P34 = 1
#define DISPLAY_OFF          _P34 = 0
#define CLEAR_FS1            _P35 = 0
#define CLEAR_FS2            _P36 = 0

#define DISP_DATA            P4

#define DISPLAY_LIGHT_ON     _P51 = 1
#define DISPLAY_LIGHT_OFF    _P51 = 0

#define DISPLAY_REVERSE_ON   _P55 = 1
#define DISPLAY_REVERSE_OFF  _P55 = 0

#define DISPLAY_NEG_VOLT_ON  _P54 = 0

// Toshiba T6963 wait routine
// Waits until controller is ready
static GuiConst_INT8U _tmp_;
#define WAIT                                                             \
{                                                                        \
  PM4 = 0xFF; /* All input */                                            \
  CD_COMMAND;                                                            \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
                                                                         \
  do                                                                     \
  {                                                                      \
    ENABLE_READ;                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    _tmp_ = DISP_DATA;                                                   \
    DISABLE_READ;                                                        \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
    asm ("nop");                                                         \
  }                                                                      \
  while ((_tmp_ & 0x03) != 0x03);                                        \
}

// Toshiba T6963 command writing
// Remember to call WAIT before writing
#define WRITE_COMMAND(Val)                                               \
{                                                                        \
  PM4 = 0x00; /* All output */                                           \
  CD_COMMAND;                                                            \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
                                                                         \
  ENABLE_WRITE;                                                          \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  DISP_DATA = Val;                                                       \
  DISABLE_WRITE;                                                         \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
}

// Toshiba T6963 data writing
// Remember to call WAIT before writing
#define WRITE_DATA(Val)                                                  \
{                                                                        \
  PM4 = 0x00; /* All output */                                           \
  CD_DATA;                                                               \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
                                                                         \
  ENABLE_WRITE;                                                          \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  DISP_DATA = Val;                                                       \
  DISABLE_WRITE;                                                         \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
  asm ("nop");                                                           \
}

void GuiDisplay_Init (void)
{
  GuiConst_INT16S N;

  // Initialise R/W levels
  DISABLE_READ;
  DISABLE_WRITE;

  // Select font 8x8
  CLEAR_FS1;
  CLEAR_FS2;

  // Enable display
  DISPLAY_ON;

  // Invert display colors
  DISPLAY_REVERSE_OFF;

  // Turn display background light on
  DISPLAY_LIGHT_ON;

  // Mode set: "OR" mode + internal character generator mode
  WRITE_COMMAND( 0x80 );
  WAIT;

  // Graphic Home Address Set: 0x0000
  WRITE_DATA( 0x00 );
  WAIT;
  WRITE_DATA( 0x00 );
  WAIT;
  WRITE_COMMAND( 0x42 );
  WAIT;

  // Graphic Area Set: 30 bytes each line
  WRITE_DATA( GuiConst_BYTES_PR_LINE );
  WAIT;
  WRITE_DATA( 0x00 );
  WAIT;
  WRITE_COMMAND( 0x43 );
  WAIT;

  // ERASE LCD MEMORY

  // Address Pointer Set
  WRITE_DATA( 0x00 );
  WAIT;
  WRITE_DATA( 0x00 );
  WAIT;
  WRITE_COMMAND( 0x24 );
  WAIT;

  // Data Auto Read/Write: Data Auto Write Set
  WRITE_COMMAND( 0xB0 );
  WAIT;

  // Fill memory with zeroes
  for (N = 0; N < GuiConst_DISPLAY_BYTES; N++)
  {
    WRITE_DATA( 0x00 );
    WAIT;
  }

  // Data Auto Read/Write: Auto Reset
  WRITE_COMMAND( 0xB2 );
  WAIT;
  // FINISHED ERASING LCD MEMORY

  // Display Mode Set: Graphic Display On
  //                   Text Display    Off
  //                   Cursor Display  Off
  //                   Cursor Blink    Off
  WRITE_COMMAND( 0x98 );
  WAIT;

  // Enable negative LCD drive voltage
  DISPLAY_NEG_VOLT_ON;
}

void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT16S LastByte;
  GuiConst_INT16U Address;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // Address Pointer Set
      Address = Y * GuiConst_BYTES_PR_LINE + GuiLib_DisplayRepaint[Y].ByteBegin;

      WRITE_DATA( Address % 256);   // Low part of address
      WAIT;
      WRITE_DATA( Address / 256);   // High part of address
      WAIT;
      WRITE_COMMAND( 0x24 );
      WAIT;

      // Data Auto Read/Write: Data Auto Write Set
      WRITE_COMMAND( 0xB0 );
      WAIT;

      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        WRITE_DATA(GuiLib_DisplayBuf[Y][X]);
        WAIT;
      }

      // Data Auto Read/Write: Auto Reset
      WRITE_COMMAND( 0xB2 );
      WAIT;

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();

}

#endif // LCD_CONTROLLER_TYPE_T6963

#ifdef LCD_CONTROLLER_TYPE_TLS8201

// ============================================================================
//
// TLS8201 DISPLAY CONTROLLER
//
// This driver uses one TLS8201 display controller with on-chip oscillator.
// LCD display in 8 bit parallel 8080 type interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 132x65 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port pins must be altered to correspond to your �-controller hardware
// and compiler syntax.
//
// ============================================================================

// Hardwired connections
// PSB = 1, parallel interface is selected
// C86 = 0, 8080-series interface is selected
// E-RD = 1;

#define DISP_DATA                 Pxx

#define CHIP_SELECT               Pxx.x = 0     // CS1B pin
#define CHIP_DESELECT             Pxx.x = 1     // CS1B pin
#define MODE_DATA                 Pxx.x = 1     // RS pin
#define MODE_COMMAND              Pxx.x = 0     // RS pin
#define MODE_WRITE_ON             Pxx.x = 0     // RW-WR pin
#define MODE_WRITE_OFF            Pxx.x = 1     // RW/WR pin
#define MODE_RESET_ON             Pxx.x = 0     // RESET pin
#define MODE_RESET_OFF            Pxx.x = 1     // RESET pin
#define NOP                       asm ("nop")   // nop code

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// 8 bit parallel 8080 type interface
void lcdPortWriteByte(GuiConst_INT8U A0,
                      GuiConst_INT8U v)
{
  if(A0)
    MODE_DATA;
  else
    MODE_COMMAND;

  CHIP_SELECT;
  MODE_WRITE_ON;
  DISP_DATA = v;
  // NOP; // maybe required for microcontrollers with instruction cycle < 100ns
  MODE_WRITE_OFF;
  CHIP_DESELECT;
}

#define lcdReset                                          lcdPortWriteByte(0,0xe2)
#define lcdV5ResistorRatio(ratio)                         lcdPortWriteByte(0,0x20 | ((ratio) & 0x07))
#define lcdPowerController(booster, vreg, vfollow)        lcdPortWriteByte(0,0x28 | ((booster) ? 4 : 0) | ((vreg) ? 2 : 0) | ((vfollow) ? 1 : 0))
#define lcdElectronicVol(level) lcdPortWriteByte(0,0x81); lcdPortWriteByte(0,(level) & 0x3f)
#define lcdDisplaySw(onOff)                               lcdPortWriteByte(0,0xae | ((onOff) ? 1 : 0))
#define lcdSelectStartLine(lineno)                        lcdPortWriteByte(0,0x40 | ((lineno) & 0x3f))
#define lcdSelectPage(pageno)                             lcdPortWriteByte(0,0xb0 | ((pageno) & 0x0f))
#define lcdSelectColumnHighNibble(columnno)               lcdPortWriteByte(0,0x10 | ((columnno >> 4) & 0x0f))
#define lcdSelectColumnLowNibble(columnno)                lcdPortWriteByte(0,0x00 | ((columnno) & 0x0f))
#define lcdSelectLCDBias(ratio)                           lcdPortWriteByte(0,0xa2 | ((ratio) & 0x01))
#define lcdSetADCreverse                                  lcdPortWriteByte(0,0xa1)
#define lcdBoosterRatio(ratio)  lcdPortWriteByte(0,0xf8); lcdPortWriteByte(0,(ratio) & 0x03)
#define lcdNormalComDirection                             lcdPortWriteByte(0,0xc0)
#define lcdReverseComDirection                            lcdPortWriteByte(0,0xc8)
#define lcdNormalDisplay                                  lcdPortWriteByte(0,0xa6)
#define lcdReverseDisplay                                 lcdPortWriteByte(0,0xa7)
#define lcdDataByte(dataval)                              lcdPortWriteByte(1,dataval)

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S X;

  // Reset controller
  MODE_RESET_ON;
  millisecond_delay(1);
  MODE_RESET_OFF;

  lcdDisplaySw(0);           // Display off
  lcdSelectLCDBias(0);       // 1/9 bias
  lcdSetADCreverse;          // ADC select - Reverse
  lcdNormalComDirection;     // Common output Normal
  lcdNormalDisplay;          // Normal display

  lcdPowerController(1,0,0); // V/C on
  millisecond_delay(1);
  lcdPowerController(1,1,0); // V/C on, V/R on
  millisecond_delay(1);
  lcdPowerController(1,1,1); // V/C on, V/R on, V/F on
  lcdBoosterRatio(0);        // 4 booster
  lcdV5ResistorRatio(5);     // Internal resistor ratio
  lcdElectronicVol(25);      // Electronic volume mode set * electronic volume
  lcdSelectStartLine(0);     // Display start first line

  // Clear display memory
  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    // Select page
    lcdSelectPage(PageNo);
    // Select column
    lcdSelectColumnHighNibble(0);
    lcdSelectColumnLowNibble(0);
    // Write display data
    for (X = 0; X < GuiConst_BYTES_PR_LINE; X++)   // Loop through bytes
      lcdDataByte(0x00);
  }

  lcdDisplaySw(1);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      lcdSelectPage(PageNo);

      // Select column
      lcdSelectColumnHighNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      lcdSelectColumnLowNibble(GuiLib_DisplayRepaint[PageNo].ByteBegin);
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        lcdDataByte(GuiLib_DisplayBuf[PageNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_TLS8201

#ifdef LCD_CONTROLLER_TYPE_TMPA900
// ============================================================================
//
// TMPA900 microcontroller with built-in display controller
//
// This driver uses the internal lcd controller of the TMPA900 microcontroller.
// LCD driver in 16 bit RGB(565) parallel interface for TFT type
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Px.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================

// Hardware connection for TFT 16bit interface
// TMPA900 - TFT display   Data bit in RAM
// CLFP    - VSYNC
// CLLP    - HSYNC
// CLCP    - Pixel CLK
// CLAC    - DATA_ENABLE
// CLD[0]  - BLUE[4]       D15
// CLD[17] - BLUE[3]       D14
// CLD[16] - BLUE[2]       D13
// CLD[15] - BLUE[1]       D12
// CLD[14] - BLUE[0]       D11
// CLD[13] - GREEN[5]      D10
// CLD[11] - GREEN[4]      D9
// CLD[10] - GREEN[3]      D8
// CLD[9]  - GREEN[2]      D7
// CLD[8]  - GREEN[1]      D6
// CLD[7]  - GREEN[0]      D5
// CLD[5]  - RED[4]        D4
// CLD[4]  - RED[3]        D3
// CLD[3]  - RED[2]        D2
// CLD[2]  - RED[1]        D1
// CLD[1]  - RED[0]        D0

//                ******** NOTE! *********
// Set start address of frame display buffer in external SDRAM!
#define FRAME_ADDRESS  0x00000000

// LCDC registers
#define LCDC_TIMING0   (*(volatile unsigned long *)0xF4200000)
#define LCDC_TIMING1   (*(volatile unsigned long *)0xF4200004)
#define LCDC_TIMING2   (*(volatile unsigned long *)0xF4200008)
#define LCDC_TIMING3   (*(volatile unsigned long *)0xF420000C)
#define LCDC_UPBASE    (*(volatile unsigned long *)0xF4200010)
#define LCDC_LPBASE    (*(volatile unsigned long *)0xF4200014)
#define LCDC_IMSC      (*(volatile unsigned long *)0xF4200018)
#define LCDC_CONTROL   (*(volatile unsigned long *)0xF420001C)
#define LCDC_ICR       (*(volatile unsigned long *)0xF4200028)
#define LCDC_PALETTE   (*(volatile unsigned long *)0xF4200200)
// LCDCOP register
#define LCDC_STN64CR   (*(volatile unsigned long *)0xF00B0000)
// Port registers
#define GPIO_K_FR1     (*(volatile unsigned long *)0xF0809424)
#define GPIO_K_FR2     (*(volatile unsigned long *)0xF0809428)
#define GPIO_J_FR1     (*(volatile unsigned long *)0xF0808424)
#define GPIO_J_FR2     (*(volatile unsigned long *)0xF0808428)
// SMC registers
#define SMC_TIMEOUT    (*(volatile unsigned long *)0xF00A0050)

// LCD timing parameter
// Horizontal timing, LCDTiming0
#define HBP            (48)                        // Horizontal back porch  0-255
#define HFP            (20)                        // Horizontal front porch 0-255
#define HSW            (20)                        // Horizontal sync pulse width 0-255
#define PPL            ((GuiConst_DISPLAY_WIDTH_HW / 16) - 1) // Pixel per line value 0-255

// Vertical timing, LCDTiming1
#define VBP            (6)                         // Vertical back porch  0-255
#define VFP            (4)                         // Vertical front porch 0-255
#define VSW            (12)                        // Vertical sync pulse lines value 0-63
#define LPP            (GuiConst_DISPLAY_HEIGHT_HW - 1) // Lines per panel value 0-1023

// Clock timing, LCDTiming2
#define PCD            (16)                        // Pixel clock divisor: Pixel clock = HCLK/_PCD; _PCD > 3 !
#define PCD_HI         ((PCD - 2) >> 5)            // PCD value, upper 5 bits
#define PCD_LO         ((PCD - 2) & 0x1F)          // PCD value, lower 5 bits

#define CPL            ((GuiConst_DISPLAY_WIDTH_HW - 1) & 0x3FF)  // Clocks per line value 0-1023
#define ACB            (0)                         // AC bias frequency: 0-31; not needed for TFT

void GuiDisplay_Init(void)
{
  //                           ******** NOTE! *********
  // Before LCDC controller first must be initialized external SDRAM controller!

  // Setup static memory controller to avoid LCDC underflow
  SMC_TIMEOUT  = 0x1;                  // Setup timout counter for bus

  // Initialize Ports
  GPIO_J_FR1 = 0xFF;                   // Function register set => Used for LCD data
  GPIO_J_FR2 = 0x00;

  GPIO_K_FR1 = 0xFF;                   // Function register set => Used for LCD data
  GPIO_K_FR2 = 0x00;

  // Initialize LCDC controller
  LCDC_STN64CR = 0x2;                  // For TFT 16-bit

  LCDC_CONTROL = 0;                    // Initailly disable controller

  LCDC_TIMING0 = (HBP << 24)           // Initialize horizontal timing register
               | (HFP << 16)
               | (HSW <<  8)
               | (PPL <<  2)           // bits:7-2: Pixel per line value 0-63
               | (0   <<  0);          // bits:1-0: Undefined, write as 0

  LCDC_TIMING1 = (VBP << 24)           // Initialize vertical timing register
               | (VFP << 16)
               | (VSW << 10)
               | (LPP <<  0);

  LCDC_TIMING2 =  0                    // Initialize clock timing register
               | (PCD_HI       << 27)  // Pixel clock divisor high
               | (0            << 26)  // BCD: 0 => don't bypass pixel clock divisor
               | (CPL          << 16)  // Clocks per line value
               | (0            << 15)  // Undefined, always write 0
               | (0            << 14)  // IOE: 0 => LCLAC output high in TFT mode
               | (1            << 13)  // IPC: Invert panel clock, 1 => LCLCP falling edge
               | (0            << 12)  // HIS: Invert horizontal sync, 0 => LCLLP pin HIGH active
               | (0            << 11)  // IVS: Invert vertical sync, 0 => LCLFP pin HIGH active
               | ((ACB & 0x1F) << 6)   // AC bias frequency parameter (not used for TFT)
               | (0            << 5)   // Unused, write as 0
               | (PCD_LO       << 0);  // Pixel clock divisor low value


  LCDC_TIMING3 =  0                    // Initialize parameter for line end control signal
               | (0 << 16)             // LEE: CLLE disabled, held low
               | (0 <<  0);            // LED: Delay for CLLE, 0-63

  // Disable all interrupts
  LCDC_IMSC = 0x0;

  // Clear all interrupts
  LCDC_ICR  = 0xFFFFFFFF;

  // 16.bit LCD FIFO watermark, 0 => 4 words space; 1 => 8 words space
  // 15-14.bits, not used, must be 0
  // 13-12.bits LcdVComp interrupt occurence; not used, don't care
  // 11.bit, not used, must be 1
  // 10-9.bits, not used, must be 0
  // 8.bit, RGB, 0 => RGB normal; 1 => swap red/blue
  // 7.bit, LcdDual: 0 => Single Panel LCD (used for STN only, set to 0 for TFT)
  // 6.bit, LcdMono8: 0 => 4 bit interface for Monochrome LCD, don't care for TFT
  // 5.bit, LcdTFT: 1 => TFT panel used
  // 4.bit, LcdBW:  0 => Color LCD
  // 3-1.bits, LCD bits per pixel value: 000 = 1 bpp, 001 = 2 bpp,  010 = 4 bpp
  //                                     011 = 8 bpp, 100 = 16 bpp, 101 = 24 bpp
  // 0.bit, Enable LCD controller: 0=> disable, 1 => enable
  // Do not enable display until video buffer address is set!
  LCDC_CONTROL = 0x00000828;

  // Set start address for display data
  // Needs to be set, before LCDC is enabled
  LCDC_UPBASE = (GuiConst_INT32U)FRAME_ADDRESS;
  // Needs to be set, before LCDC is enabled, not used for TFT
  LCDC_LPBASE = (GuiConst_INT32U)FRAME_ADDRESS;

  // Second solution is to put directly address of easyGui display buffer
  // In this case let GuiDisplay_Refresh function empty.
  // LCDC_UPBASE = (GuiConst_INT32U)&GuiLib_DisplayBuf.Words[0][0];
  // LCDC_LPBASE = (GuiConst_INT32U)&GuiLib_DisplayBuf.Words[0][0];

  // Enable LCDC controller
  LCDC_CONTROL |= 0x1;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT16S X, Y, LastByte;
  GuiConst_INT16U *framePtr;

  GuiDisplay_Lock ();

  // Walk through all lines
  for (Y = 0; Y < GuiConst_BYTE_LINES; Y++)
  {
    if (GuiLib_DisplayRepaint[Y].ByteEnd >= 0)
    // Something to redraw in this line
    {
      // set video ram base address
      framePtr = (GuiConst_INT16U *)FRAME_ADDRESS;
      // Pointer offset calculation
      framePtr += Y * GuiConst_BYTES_PR_LINE / 2 +
                  GuiLib_DisplayRepaint[Y].ByteBegin;
      // Set amount of data to be changed
      LastByte = GuiConst_BYTES_PR_LINE / 2 - 1;

      if (GuiLib_DisplayRepaint[Y].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[Y].ByteEnd;

      // Write data for one line to framebuffer from GuiLib_DisplayBuf
      for (X = GuiLib_DisplayRepaint[Y].ByteBegin; X <= LastByte; X++)
      {
        *framePtr++ = GuiLib_DisplayBuf.Words[Y][X];
      }

      // Reset repaint parameters
      GuiLib_DisplayRepaint[Y].ByteEnd = -1;
    }
  }
  GuiDisplay_Unlock ();

}

#endif // LCD_CONTROLLER_TYPE_TMPA900

#ifdef LCD_CONTROLLER_TYPE_TS7390
// ============================================================================
//
// TS7390 BOARD DISPLAY CONTROLLER
//
// This driver uses linux ts7kvfb driver module.
// Frame buffer support must be enabled in kernel, and 16 bit resolution set.
// LCD display in 16 bit TFT interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bit
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Pxx.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// ============================================================================
#include<unistd.h>
#include <sys/stat.h>
#include<sys/types.h>
#include<sys/mman.h>
#include<sys/time.h>
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include <sys/termios.h>
#include<math.h>
#include <sys/select.h>
#include <linux/input.h>
#include <linux/fb.h>

// Set start address of frame buffer
#define FRAME_ADDRESS  0x601000000

GuiConst_INT16U *fb;

void GuiDisplay_Init(void)
{
  GuiConst_INT32U i;
  GuiConst_INT32S fd;
  GuiConst_INT16U *ts, *backlight, *regs;

  fd = -1;

  // Open the framebuffer device using the open() call
  fd = open("/dev/mem", O_RDWR|O_SYNC);

  // Add the missing parameters to mmap for video buffer pointer
  fb = (GuiConst_INT16U *)mmap(0, GuiConst_DISPLAY_BYTES, PROT_READ|PROT_WRITE, MAP_SHARED, fd, FRAME_ADDRESS);
  if (fb == MAP_FAILED)
  {
    perror("mmap fb");
    exit(3);
  }

   // Add the missing parameters to mmap for asic registers pointer
  regs = (GuiConst_INT16U *)mmap(0, 4096, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0x600ff000);
  if (regs == MAP_FAILED)
  {
    perror("mmap regs");
    exit(3);
  }

  // Set backlight
  ts = regs + 0x20;
  backlight = regs + 0x43;
  regs += (0x30 / sizeof(GuiConst_INT16U));

  regs[4] = 0x300;
  *backlight |= 0x20;
  usleep(500000);
  *backlight |= 0x40;

  // Clear video buffer
  for (i = 0; i < GuiConst_DISPLAY_BYTES / 2; i++)
    fb[i] = 0x0000;
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT32U i;
  GuiConst_INT16S LineNo, LastByte, N;

  GuiDisplay_Lock ();

  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++) // Loop through lines
  {
    if (GuiLib_DisplayRepaint[LineNo].ByteEnd >= 0)
    // Something to redraw on this line
    {
      // Calculate address in frame buffer
      i = LineNo;
      i = i * GuiConst_DISPLAY_WIDTH + GuiLib_DisplayRepaint[LineNo].ByteBegin;

      // Calculate last byte
      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[LineNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[LineNo].ByteEnd;

      // Write display data
      for (N = GuiLib_DisplayRepaint[LineNo].ByteBegin; N <= LastByte; N++)
        fb[i++] = GuiLib_DisplayBuf.Words[LineNo][N];

      // Reset repaint parameters
      GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
    }
  }

  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_TS7390

#ifdef LCD_CONTROLLER_TYPE_UC1608

// ============================================================================
//
// UC1608 DISPLAY CONTROLLER
//
// This driver uses one UC1608 display controller.
// LCD display 8 bit parallel interface.
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x128 pixels.
// Vertical resolution can be 96 or 128 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 1 bit (B/W)
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Pxx.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// Make sure 15-20ms elapses from system power on, before init routine is
// executed.
//
// ============================================================================

#define DISP_DATA                 Pxx

#define CHIP_SELECT               Pxx.x = 1     // CS pin
#define CHIP_DESELECT             Pxx.x = 0     // CS pin
#define CHIP_RESET                Pxx.x = 0     // RST pin
#define CHIP_RUN                  Pxx.x = 1     // RST pin
#define MODE_DATA                 Pxx.x = 1     // CD pin
#define MODE_COMMAND              Pxx.x = 0     // CD pin
#define MODE_WRITE_OFF            Pxx.x = 0     // WR0 pin (WR)
#define MODE_WRITE_ON             Pxx.x = 1     // WR0 pin (WR)
#define MODE_READ_OFF             Pxx.x = 0     // WR1 pin (RD)
#define MODE_READ_ON              Pxx.x = 1     // WR1 pin (RD)

// Commands
#define CMD_CA_LSB                0x00
#define CMD_CA_MSB                0x10
#define CMD_MUX_TEMP              0x20
#define CMD_POWER                 0x28
#define CMD_ADV_PROD_CONFIG       0x30
#define CMD_START_LINE            0x40
#define CMD_GAIN_POT              0x81
#define CMD_RAM_ADDR_CTRL         0x88
#define CMD_ALL_PIXEL_ON          0xA4
#define CMD_INVERSE               0xA6
#define CMD_DISABLE               0xAE
#define CMD_ENABLE                0xAF
#define CMD_PAGE_ADDR             0xB0
#define CMD_MAPPING_CTRL          0xC0
#define CMD_RESET                 0xE2
#define CMD_NOP                   0xE3
#define CMD_BIAS                  0xE8
#define CMD_CURSOR_RESET          0xEE
#define CMD_CURSOR_SET            0xEF

// Waits until controller is ready
#define WAIT                                                             \
{                                                                        \
  MODE_COMMAND;                                                          \
  do {                                                                   \
  MODE_READ_OFF;                                                         \
  MODE_READ_ON;                                                          \
  } while ((DISP_DATA & 0x90) != 0x80);                                  \
  MODE_READ_OFF;                                                         \
}

// Command writing
#define WRITE_COMMAND(Val)                                               \
{                                                                        \
  MODE_COMMAND;                                                          \
  MODE_WRITE_OFF;                                                        \
  DISP_DATA = Val;                                                       \
  MODE_WRITE_ON;                                                         \
}

// Data writing
#define WRITE_DATA(Val)                                                  \
{                                                                        \
  MODE_DATA;                                                             \
  MODE_WRITE_OFF;                                                        \
  DISP_DATA = Val;                                                       \
  MODE_WRITE_ON;                                                         \
}

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT32S X;

  // Enable controller
  CHIP_SELECT;

  // Wait until controller has reset
  WAIT;

  // Display off
  WRITE_COMMAND(CMD_DISABLE);

  // LCD bias ratio
  WRITE_COMMAND(CMD_BIAS | 0x02);

  // Gain and potentiometer
  WRITE_COMMAND(CMD_GAIN_POT);
  WRITE_COMMAND(0xC0);

  // MUX rate and temperature compensation
  WRITE_COMMAND(CMD_MUX_TEMP | (4 * (GuiConst_DISPLAY_HEIGHT_HW / 128)));

  // Power control
  WRITE_COMMAND(CMD_POWER | 0x07)

  // Start line = 0
  WRITE_COMMAND(CMD_START_LINE)

  // RAM access control
  WRITE_COMMAND(CMD_RAM_ADDR_CTRL);

  // LCD mapping control
  WRITE_COMMAND(CMD_MAPPING_CTRL);

  // Clear display memory
  // Select colomn 0
  WRITE_COMMAND(CMD_CA_LSB);
  WRITE_COMMAND(CMD_CA_MSB);
  // Select page 0
  WRITE_COMMAND(CMD_PAGE_ADDR + 0);
  // Write data
  for (X = 0; X < GuiConst_DISPLAY_BYTES; X++)   // Loop through bytes
    WRITE_DATA(0x00);

  // Display on
  WRITE_COMMAND(CMD_ENABLE);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT8U PageNo;
  GuiConst_INT8U ColNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
      // Something to redraw on this page
    {
      // Select page
      WRITE_COMMAND(CMD_PAGE_ADDR + PageNo);
      // Select column
      ColNo = GuiLib_DisplayRepaint[PageNo].ByteBegin;
      WRITE_COMMAND(CMD_CA_LSB + (ColNo & 0x0F));
      WRITE_COMMAND(CMD_CA_MSB + ((ColNo & 0xF0) >> 4));
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        WRITE_DATA(GuiLib_DisplayBuf[PageNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_UC1608

#ifdef LCD_CONTROLLER_TYPE_UC1610

// ============================================================================
//
// UC1610 DISPLAY CONTROLLER
//
// This driver uses one UC1610 display controller.
// LCD display 8 bit parallel interface, 8080 bus type mode.
// Modify driver accordingly for other interface types.
// Graphic modes up to 160x128 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 2 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Pxx.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// Make sure 150ms elapses from system power on, before init routine is
// executed.
//
// ============================================================================

// Hardwired connections for 8080 interface must be
// BM1 = 1, BM0 = 0;
// CS1 = 1;
// RST = VCC;

#define DISP_DATA                 Pxx

#define CHIP_DESELECT             Pxx.x = 1     // CS0 pin
#define CHIP_SELECT               Pxx.x = 0     // CS0 pin
#define CHIP_RESET                Pxx.x = 0     // RST pin
#define CHIP_RUN                  Pxx.x = 1     // RST pin
#define MODE_DATA                 Pxx.x = 1     // CD pin
#define MODE_COMMAND              Pxx.x = 0     // CD pin
#define MODE_WRITE_LOW            Pxx.x = 0     // WR0 pin (WR)
#define MODE_WRITE_HIGH           Pxx.x = 1     // WR0 pin (WR)
#define MODE_READ_LOW             Pxx.x = 0     // WR1 pin (RD)
#define MODE_READ_HIGH            Pxx.x = 1     // WR1 pin (RD)

// Commands
#define CMD_COLUMN_LSB            0x00
#define CMD_COLUMN_MSB            0x10
#define CMD_TEMP                  0x24
#define CMD_PANEL                 0x28
#define CMD_PUMP                  0x2C
#define CMD_PAGE_ADR              0x60
#define CMD_BIAS_POT              0x81
#define CMD_WINDOW_CTRL           0x84
#define CMD_RAM_ADDR_CTRL         0x88
#define CMD_FIXED_LINES           0x90
#define CMD_FRAME_RATE            0xA0
#define CMD_ALL_PIXEL_ON          0xA5
#define CMD_INVERSE               0xA7
#define CMD_DISABLE               0xA8
#define CMD_ENABLE                0xAF
#define CMD_LCD_MAP_CTRL          0xC0
#define CMD_GRAYSCALE_MODE        0xD0
#define CMD_RESET                 0xE2
#define CMD_NOP                   0xE3
#define CMD_BIAS                  0xE8

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Command writing
#define WRITE_COMMAND(Val)                                               \
{                                                                        \
  MODE_READ_HIGH;                                                        \
  CHIP_SELECT;                                                           \
  MODE_COMMAND;                                                          \
  MODE_WRITE_LOW;                                                        \
  DISP_DATA = Val;                                                       \
  MODE_WRITE_HIGH;                                                       \
  CHIP_DESELECT;                                                         \
}

// Data writing
#define WRITE_DATA(Val)                                                  \
{                                                                        \
  MODE_READ_HIGH;                                                        \
  CHIP_SELECT;                                                           \
  MODE_DATA;                                                             \
  MODE_WRITE_LOW;                                                        \
  DISP_DATA = Val;                                                       \
  MODE_WRITE_HIGH;                                                       \
  CHIP_DESELECT;                                                         \
}

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT32S X;

  // Reset controller
  WRITE_COMMAND(CMD_RESET);
  millisecond_delay(1);

  // TEMP temperature compensation 0 - 0.0%, 1 - 0.10%, 2 - 0.15%, 3 - 0.20%, /1C
  WRITE_COMMAND(CMD_TEMP | (0));

  // Set bias of potentiometer
  WRITE_COMMAND(CMD_BIAS_POT);
  WRITE_COMMAND(0x7F);

  // Clear display memory
  for (X = 0; X < GuiConst_DISPLAY_BYTES; X++)   // Loop through bytes
    WRITE_DATA(0x00);

  // Display on
  WRITE_COMMAND(CMD_ENABLE);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT8U PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      WRITE_COMMAND(CMD_PAGE_ADR | (PageNo & 0x1F));

      // Select column
      WRITE_COMMAND(CMD_COLUMN_LSB  |
         (GuiLib_DisplayRepaint[PageNo].ByteBegin & 0x0F));
      WRITE_COMMAND(CMD_COLUMN_MSB  |
         ((GuiLib_DisplayRepaint[PageNo].ByteBegin >> 4) & 0x0F));

      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        WRITE_DATA(GuiLib_DisplayBuf[PageNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_UC1610

#ifdef LCD_CONTROLLER_TYPE_UC1611

// ============================================================================
//
// UC1611 DISPLAY CONTROLLER
//
// This driver uses one UC1611 display controller.
// LCD display 8 bit or 16 bit parallel interface 8080 bus type mode.
// Modify driver accordingly for other interface types.
// Graphic modes up to 240x160 pixels for UC1611.
// Graphic modes up to 256x160 pixels for UC1611S.
// Vertical resolution can be 64, 96, 128 or 160 pixels.
//
// Compatible display controllers:
//   UC1611S
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Vertical bytes
//   Bit 0 at top
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 4 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Pxx.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// Make sure 15-20ms elapses from system power on, before init routine is
// executed.
//
// ============================================================================

// Uncomment for UC1611S
//#define UC1611S
// Uncomment for 16 bit interface
//#define BUS_16

#define DISP_DATA                 Pxx           // D15-D0 (16 bit interface)
                                                //  D7-D0 ( 8 bit interface)
#define CHIP_SELECT               Pxx.x = 1     // CS1 pin, CS0 = 0;
#define CHIP_DESELECT             Pxx.x = 0     // CS1 pin  CS0 = 0;
#define CHIP_RESET                Pxx.x = 0     // RST pin
#define CHIP_RUN                  Pxx.x = 1     // RST pin
#define MODE_DATA                 Pxx.x = 1     // CD pin
#define MODE_COMMAND              Pxx.x = 0     // CD pin
#define MODE_WRITE_OFF            Pxx.x = 0     // WR0 pin (WR)
#define MODE_WRITE_ON             Pxx.x = 1     // WR0 pin (WR)
#define MODE_READ_OFF             Pxx.x = 0     // WR1 pin (RD)
#define MODE_READ_ON              Pxx.x = 1     // WR1 pin (RD)

// Commands
#define CMD_CA_LSB                0x00
#define CMD_CA_MSB                0x10
#define CMD_TEMP                  0x24
#define CMD_PANEL                 0x28
#define CMD_PUMP                  0x2C
#define CMD_ADV_PROD_CONFIG       0x30
#define CMD_START_LINE_LSB        0x40
#define CMD_START_LINE_MSB        0x50
#define CMD_GAIN_POT              0x81
#define CMD_RAM_ADDR_CTRL         0x88
#define CMD_FIXED_LINES           0x90
#define CMD_FRAME_RATE            0xA0
#define CMD_ALL_PIXEL_ON          0xA4
#define CMD_INVERSE               0xA6
#define CMD_DISABLE               0xA8
#define CMD_ENABLE                0xA9
#define CMD_PAGE_ADDR_LSB         0x60
#define CMD_PAGE_ADDR_MSB         0x70
#define CMD_LCD_CTRL              0xC0
#define CMD_INLINE_INV            0xD8
#define CMD_GRAYSCALE_MODE        0xD0
#define CMD_RESET                 0xE2
#define CMD_NOP                   0xE3
#define CMD_BIAS                  0xE8
#define CMD_CURSOR_RESET          0xEE
#define CMD_CURSOR_SET            0xEF

// Waits until controller is ready
#define WAIT                                                             \
{                                                                        \
  CHIP_SELECT;                                                           \
  MODE_COMMAND;                                                          \
  do {                                                                   \
  MODE_READ_OFF;                                                         \
  MODE_READ_ON;                                                          \
#ifdef BUS_16
  } while ((DISP_DATA & 0x0090) != 0x80);                                \
#else
  } while ((DISP_DATA & 0x90) != 0x80);                                  \
#endif
  MODE_READ_OFF;                                                         \
  CHIP_DESELECT;                                                         \
}

// Command writing
#define WRITE_COMMAND(Val)                                               \
{                                                                        \
  CHIP_SELECT;                                                           \
  MODE_COMMAND;                                                          \
  MODE_WRITE_OFF;                                                        \
  DISP_DATA = Val;                                                       \
  MODE_WRITE_ON;                                                         \
  CHIP_DESELECT;                                                         \
}

// Data writing
#define WRITE_DATA(Val)                                                  \
{                                                                        \
  CHIP_SELECT;                                                           \
  MODE_DATA;                                                             \
  MODE_WRITE_OFF;                                                        \
  DISP_DATA = Val;                                                       \
  MODE_WRITE_ON;                                                         \
  CHIP_DESELECT;                                                         \
}

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT32S X;

  // Reset controller
  WRITE_COMMAND(CMD_RESET);

  // Wait for controller to be ready
  WAIT;

  // LCD bias ratio
  WRITE_COMMAND(CMD_BIAS | 0x02);

  // Gain and potentiometer
  WRITE_COMMAND(CMD_GAIN_POT);
#ifdef UC1611S
  // For UC1611S only electronic potenciometer register
  WRITE_COMMAND(0xEA);
#else
  WRITE_COMMAND(0xD0);
#endif

  // TEMP temperature compensation 0 - 0.0%, 1 - 0.05%, 2 - 0.1%, 3 - 0.2%, /1C
  WRITE_COMMAND(CMD_TEMP | (0));

  // Panel control
  WRITE_COMMAND(CMD_PANEL | 0x03);

  // Pump control  0 - ext., 1 - 6x, 2 - 7x, 3 - 8x
  WRITE_COMMAND(CMD_PUMP | 0x03);

  // Start line = 0
  WRITE_COMMAND(CMD_START_LINE_LSB);
  WRITE_COMMAND(CMD_START_LINE_MSB);

  // RAM access control
  WRITE_COMMAND(CMD_RAM_ADDR_CTRL);

  // LCD control
  WRITE_COMMAND(CMD_LCD_CTRL);
  // For UC1611S is used double byte command
#ifdef UC1611S
  WRITE_COMMAND(0x00);
#endif

#ifndef UC1611S
  // Only for UC1611
  // GRAYSCALE MODE control 2 - 16 gray scale, 1 - 8 gray scale, 0 - B/W
  WRITE_COMMAND(CMD_GRAYSCALE_MODE | 2);
#endif

  // Clear display memory
  // Select colomn 0
  WRITE_COMMAND(CMD_CA_LSB);
  WRITE_COMMAND(CMD_CA_MSB);
  // Select page 0
  WRITE_COMMAND(CMD_PAGE_ADDR_LSB);
  WRITE_COMMAND(CMD_PAGE_ADDR_MSB);
  // Write data
  for (X = 0; X < GuiConst_DISPLAY_BYTES; X++)   // Loop through bytes
    WRITE_DATA(0x00);

  // Display on
#ifdef UC1611S
  // For UC1611S set also grayscale mode
  // 0x06 - 16 shade, 0x04 - 8 shade, 0x02 - 4 shade, 0x00 - B/W
  WRITE_COMMAND(CMD_ENABLE | 0x06);
#else
  // For UC1611 1. and 2. bit are only enable bits
  WRITE_COMMAND(CMD_ENABLE | 0x06);
#endif
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT8U PageNo;
  GuiConst_INT8U ColNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
      // Something to redraw on this page
    {
      // Select page
      WRITE_COMMAND(CMD_PAGE_ADDR_LSB  | (PageNo & 0x0F));
      WRITE_COMMAND(CMD_PAGE_ADDR_MSB  | ((PageNo & 0xF0) >> 4));
      // Select column
      ColNo = GuiLib_DisplayRepaint[PageNo].ByteBegin;
      WRITE_COMMAND(CMD_CA_LSB + (ColNo & 0x0F));
      WRITE_COMMAND(CMD_CA_MSB + ((ColNo & 0xF0) >> 4));
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        WRITE_DATA(GuiLib_DisplayBuf[PageNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_UC1611

#ifdef LCD_CONTROLLER_TYPE_UC1617

// ============================================================================
//
// UC1617 DISPLAY CONTROLLER
//
// This driver uses one UC1617 display controller.
// LCD display 8 bit parallel interface, 8080 bus type mode.
// Modify driver accordingly for other interface types.
// Graphic modes up to 128x128 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at left
//   Number of color planes: 1
//   Color mode: Grayscale
//   Color depth: 2 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Pxx.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// Make sure 150ms elapses from system power on, before init routine is
// executed.
//
// ============================================================================

// Hardwired connections for 8080 interface must be
// BM0 = 1, BM0 = 0;
// CS1 = 1;
// RST = VCC;

#define DISP_DATA                 Pxx

#define CHIP_DESELECT             Pxx.x = 1     // CS0 pin
#define CHIP_SELECT               Pxx.x = 0     // CS0 pin
#define CHIP_RESET                Pxx.x = 0     // RST pin
#define CHIP_RUN                  Pxx.x = 1     // RST pin
#define MODE_DATA                 Pxx.x = 1     // CD pin
#define MODE_COMMAND              Pxx.x = 0     // CD pin
#define MODE_WRITE_LOW            Pxx.x = 0     // WR0 pin (WR)
#define MODE_WRITE_HIGH           Pxx.x = 1     // WR0 pin (WR)
#define MODE_READ_LOW             Pxx.x = 0     // WR1 pin (RD)
#define MODE_READ_HIGH            Pxx.x = 1     // WR1 pin (RD)

// Commands
#define CMD_PAGE_ADR              0x00
#define CMD_TEMP                  0x24
#define CMD_PANEL                 0x28
#define CMD_PUMP                  0x2C
#define CMD_ADV_CTRL              0x32
#define CMD_ROW_LSB               0x60
#define CMD_ROW_MSB               0x70
#define CMD_BIAS_POT              0x81
#define CMD_WINDOW_CTRL           0x84
#define CMD_RAM_ADDR_CTRL         0x88
#define CMD_FIXED_LINES           0x90
#define CMD_FRAME_RATE            0xA0
#define CMD_ALL_PIXEL_ON          0xA5
#define CMD_INVERSE               0xA7
#define CMD_DISABLE               0xA8
#define CMD_ENABLE                0xAF
#define CMD_LCD_MAP_CTRL          0xC0
#define CMD_INLINE_INV            0xC8
#define CMD_GRAYSCALE_MODE        0xD0
#define CMD_RESET                 0xE2
#define CMD_NOP                   0xE3
#define CMD_BIAS                  0xE8

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Command writing
#define WRITE_COMMAND(Val)                                               \
{                                                                        \
  MODE_READ_HIGH;                                                        \
  CHIP_SELECT;                                                           \
  MODE_COMMAND;                                                          \
  MODE_WRITE_LOW;                                                        \
  DISP_DATA = Val;                                                       \
  MODE_WRITE_HIGH;                                                       \
  CHIP_DESELECT;                                                         \
}

// Data writing
#define WRITE_DATA(Val)                                                  \
{                                                                        \
  MODE_READ_HIGH;                                                        \
  CHIP_SELECT;                                                           \
  MODE_DATA;                                                             \
  MODE_WRITE_LOW;                                                        \
  DISP_DATA = Val;                                                       \
  MODE_WRITE_HIGH;                                                       \
  CHIP_DESELECT;                                                         \
}

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT32S X;

  // Reset controller
  WRITE_COMMAND(CMD_RESET);
  millisecond_delay(1);

  // Turn off SRAM power saving
  WRITE_COMMAND(CMD_ADV_CTRL);
  WRITE_COMMAND(0x10);

  // TEMP temperature compensation 0 - 0.0%, 1 - 0.10%, 2 - 0.15%, 3 - 0.20%, /1C
  WRITE_COMMAND(CMD_TEMP | (0));

  // Set bias of potentiometer
  WRITE_COMMAND(CMD_BIAS_POT);
  WRITE_COMMAND(0x7F);

  // Clear display memory
  for (X = 0; X < GuiConst_DISPLAY_BYTES; X++)   // Loop through bytes
    WRITE_DATA(0x00);

  // Display on
  WRITE_COMMAND(CMD_ENABLE);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT8U RowNo;
  GuiConst_INT8U PageNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (RowNo = 0; RowNo < GuiConst_BYTE_LINES; RowNo++) // Loop through lines
  {
    if (GuiLib_DisplayRepaint[RowNo].ByteEnd >= 0)
    // Something to redraw on this line
    {
      // Select row
      WRITE_COMMAND(CMD_ROW_LSB  | (RowNo & 0x0F));
      WRITE_COMMAND(CMD_ROW_MSB  | ((RowNo>>4) & 0x07));
      // Select page
      PageNo = GuiLib_DisplayRepaint[RowNo].ByteBegin;
      WRITE_COMMAND(CMD_PAGE_ADR + (PageNo & 0x1F));
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE - 1;
      if (GuiLib_DisplayRepaint[RowNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[RowNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[RowNo].ByteBegin; N <= LastByte; N++)
        WRITE_DATA(GuiLib_DisplayBuf[RowNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[RowNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_UC1617

#ifdef LCD_CONTROLLER_TYPE_UC1698

// ============================================================================
//
// UC1698 DISPLAY CONTROLLER
//
// This driver uses one UC1698 display controller.
// LCD display 16 bit parallel interface, 8080 bus type mode.
// Modify driver accordingly for other interface types.
// Graphic modes up to 160x128 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 16 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//
// Port addresses (Pxx.x) must be altered to correspond to your �-controller
// hardware and compiler syntax.
//
// Make sure 15-20ms elapses from system power on, before init routine is
// executed.
//
// ============================================================================

// for 16 bit 8080 interface set pin BM1 = 1, BM0 = 0
#define DISP_DATA                 Pxx           // DB15-DB0

#define CHIP_SELECT               Pxx.x = 1     // CS1 pin
#define CHIP_DESELECT             Pxx.x = 0     // CS1 pin
#define CHIP_RESET                Pxx.x = 0     // RST pin
#define CHIP_RUN                  Pxx.x = 1     // RST pin
#define MODE_DATA                 Pxx.x = 1     // CD pin
#define MODE_COMMAND              Pxx.x = 0     // CD pin
#define MODE_WRITE_OFF            Pxx.x = 0     // WR0 pin (WR)
#define MODE_WRITE_ON             Pxx.x = 1     // WR0 pin (WR)
#define MODE_READ_OFF             Pxx.x = 0     // WR1 pin (RD)
#define MODE_READ_ON              Pxx.x = 1     // WR1 pin (RD)

// Commands
#define CMD_CA_LSB                0x0000
#define CMD_CA_MSB                0x0010
#define CMD_TEMP                  0x0024
#define CMD_POWER_CONTROL         0x0028
#define CMD_ADV_PROD_CONFIG       0x0030
#define CMD_START_LINE_LSB        0x0040
#define CMD_START_LINE_MSB        0x0050
#define CMD_GAIN_POT              0x0081
#define CMD_PARTIAL_DISPLAY       0x0084
#define CMD_RAM_ADDR_CTRL         0x0088
#define CMD_FIXED_LINES           0x0090
#define CMD_FRAME_RATE            0x00A0
#define CMD_ALL_PIXEL_ON          0x00A4
#define CMD_INVERSE               0x00A6
#define CMD_DISABLE               0x00A8
#define CMD_ENABLE                0x00AF
#define CMD_PAGE_ADDR_LSB         0x0060
#define CMD_PAGE_ADDR_MSB         0x0070
#define CMD_LCD_CTRL              0x00C0
#define CMD_INLINE_INV            0x00C8
#define CMD_BGR_COLOR_PATTERN     0x00D0
#define CMD_RGB_COLOR_PATTERN     0x00D1
#define CMD_COLOR_MODE            0x00D4
#define CMD_COM_SCAN_FUNCTION     0x00D8
#define CMD_RESET                 0x00E2
#define CMD_NOP                   0x00E3
#define CMD_BIAS                  0x00E8
#define CMD_SET_COM_END           0x00F1
#define CMD_PARTIAL_DISPLAY_START 0x00F2
#define CMD_PARTIAL_DISPLAY_END   0x00F3
#define CMD_WINDOW_COLUMN_START   0x00F4
#define CMD_WINDOW_ROW_START      0x00F5
#define CMD_WINDOW_COLUMN_END     0x00F6
#define CMD_WINDOW_ROW_END        0x00F7
#define CMD_WINDOW_MODE           0x00F8

// Command writing
void WRITE_COMMAND(GuiConst_INT16U Val)
{
  MODE_READ_ON;
  CHIP_SELECT;
  MODE_COMMAND;
  MODE_WRITE_OFF;
  DISP_DATA = Val;
  MODE_WRITE_ON;
  CHIP_DESELECT;
}

// Data writing
void WRITE_DATA(GuiConst_INT16U Val)
{
  MODE_READ_ON;
  CHIP_SELECT;
  MODE_DATA;
  MODE_WRITE_OFF;
  DISP_DATA = Val;
  MODE_WRITE_ON;
  CHIP_DESELECT;
}

// Delay routine
// This constant must be adjusted according to speed of microcontroller
#define ONE_MS               100
// Param msec is delay in miliseconds
void millisecond_delay(GuiConst_INT32U msec)
{
  GuiConst_INT32U j;

  for (j = 0; j < (ONE_MS * msec) ; j++);
}

// Initialises the module and the display
void GuiDisplay_Init(void)
{
  GuiConst_INT32S X;

  CHIP_RESET;
  millisecond_delay(1);
  CHIP_RUN;

  millisecond_delay(150);

  // Display off
  WRITE_COMMAND(CMD_DISABLE);

  //TEMP temperature compensation 0 - 0.0%, 1 - 0.05%, 2 - 0.15%, 3 - 0.25%, /1C
  WRITE_COMMAND(CMD_TEMP | 0);

  // LCD control LY-2.bit mirror Y axis, LX-1.bit mirror X axis,
  WRITE_COMMAND(CMD_LCD_CTRL | 0);

  // Line rates at Mux Rate = 109 ~ 160: 0 - 25.2 Klps 1 - 30.5 Klps 2 - 37.0 Klps 3b - 44.8 Klps
  WRITE_COMMAND(CMD_FRAME_RATE | 2);

  // 4K color - 1, 65K color - 2
  WRITE_COMMAND(CMD_COLOR_MODE | 2);

  // LCD bias ratio 0 = 5; 1 = 10; 2 = 11; 3 = 12
  WRITE_COMMAND(CMD_BIAS | 2);

  // Gain and potentiometer range 0 - 255
  WRITE_COMMAND(CMD_GAIN_POT);
  WRITE_COMMAND(0x40);

  // Power control 1.bit: 0 - External VLCD, 1 - Internal VLCD
  //               0.bit: 0 - LCD <13nF, 1 - 13nF < LCD < 22nF
  WRITE_COMMAND(CMD_POWER_CONTROL | 0x01);

  // Clear display memory
  for (X = 0; X < GuiConst_DISPLAY_BYTES; X++)   // Loop through bytes
  {
    WRITE_DATA(0x0000);
  }
  // Display on
  WRITE_COMMAND(CMD_ENABLE);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  GuiConst_INT8U PageNo;
  GuiConst_INT8U ColNo;
  GuiConst_INT16S LastByte;
  GuiConst_INT16S N;

  // Lock GUI resources
  GuiDisplay_Lock ();

  for (PageNo = 0; PageNo < GuiConst_BYTE_LINES; PageNo++) // Loop through pages
  {
    if (GuiLib_DisplayRepaint[PageNo].ByteEnd >= 0)
    // Something to redraw on this page
    {
      // Select page
      WRITE_COMMAND(CMD_PAGE_ADDR_LSB  | (PageNo & 0x0F));
      WRITE_COMMAND(CMD_PAGE_ADDR_MSB  | ((PageNo & 0xF0) >> 4));
      // Select column
      ColNo = GuiLib_DisplayRepaint[PageNo].ByteBegin;
      WRITE_COMMAND(CMD_CA_LSB + (ColNo & 0x0F));
      WRITE_COMMAND(CMD_CA_MSB + ((ColNo & 0x70) >> 4));
      // Write display data
      LastByte = GuiConst_BYTES_PR_LINE/2 - 1;
      if (GuiLib_DisplayRepaint[PageNo].ByteEnd < LastByte)
        LastByte = GuiLib_DisplayRepaint[PageNo].ByteEnd;
      for (N = GuiLib_DisplayRepaint[PageNo].ByteBegin; N <= LastByte; N++)
        WRITE_DATA(GuiLib_DisplayBuf.Words[PageNo][N]);

      // Reset repaint parameters
      GuiLib_DisplayRepaint[PageNo].ByteEnd = -1;
    }
  }

  // Free GUI resources
  GuiDisplay_Unlock ();
}

#endif // LCD_CONTROLLER_TYPE_UC1698

#ifdef LCD_CONTROLLER_TYPE_UPD161607

// ============================================================================
//
// �PD161607 DISPLAY CONTROLLER
//
// This driver uses one �PD161607 display controller.
// LCD display SPI interface, mode 2 is used for commands.
// Frame transfer is used for display data.
// Modify driver accordingly for other interface types.
// Graphic modes up to 320x240 pixels.
//
// Compatible display controllers:
//   None
//
// easyGUI setup should be (Parameters window, Display controller tab page):
//   Horizontal bytes
//   Bit 0 at right
//   Number of color planes: 1
//   Color mode: RGB
//   Color depth: 18 bits
//   Display orientation: Normal, Upside down, 90� left, 90� right
//   Display words with reversed bytes: Off
//   Number of display controllers, horizontally: 1
//   Number of display controllers, vertically: 1
//   RGB format depending on hardware wiring (e.g. BBBBBB�� GGGGGG�� RRRRRR��)
//
// There should be a 1ms delay both before and after resetting the �PD161607
//
// Spi_SendData function must be altered to correspond to your �-controller
// hardware and compiler syntax. It shall send one byte to the display
// controller.
//
// Enter suitable delay code for the 30�s and 20ms wait periods.
//
// Make sure 2ms elapses from system power on, before init routine is
// executed.
//
// ============================================================================

GuiConst_INT8U DisplayFrameTransferEnabled;

void Spi_SendData(GuiConst_INT8U B)
{
  ;   // Enter suitable SPI transmission code
}

void lcdPortWriteReg(GuiConst_INT8U CmdNo,
                     GuiConst_INT8U CmdData)
{
  Spi_SendData(0);         // Register command write
  Spi_SendData(CmdNo);     // Register number
  Spi_SendData(1);         // Register data write
  Spi_SendData(CmdData);   // Register data value
}

// Initialises the module and the display
void GuiDisplay_Init (void)
{
  DisplayFrameTransferEnabled = 0;   // No display frame transfer yet

  // Command reset register
  lcdPortWriteReg(3, 0x01);

  // Control register 2
  lcdPortWriteReg(1, 0x00);

  // Gamma adjustment registers
  lcdPortWriteReg(100, 0x0F);
  lcdPortWriteReg(101, 0x3F);
  lcdPortWriteReg(102, 0x3F);
  lcdPortWriteReg(103, 0x00);
  lcdPortWriteReg(104, 0x00);
  lcdPortWriteReg(105, 0x30);
  lcdPortWriteReg(106, 0x04);
  lcdPortWriteReg(107, 0x37);
  lcdPortWriteReg(108, 0x17);
  lcdPortWriteReg(109, 0x00);
  lcdPortWriteReg(110, 0x40);
  lcdPortWriteReg(111, 0x30);
  lcdPortWriteReg(112, 0x04);
  lcdPortWriteReg(113, 0x37);
  lcdPortWriteReg(114, 0x17);
  lcdPortWriteReg(115, 0x00);
  lcdPortWriteReg(116, 0x40);

  // RGB interface register
  lcdPortWriteReg(2, 0x40);

  // Horizontal back porch set register
  lcdPortWriteReg(75, 0x04);
  // Vertical back porch set register
  lcdPortWriteReg(76, 0x01);
  // Dummy line control select register
  lcdPortWriteReg(77, 0x01);

  // Equalize internal start position set register
  lcdPortWriteReg(80, 0x00);
  // Equalize internal end position set register
  lcdPortWriteReg(81, 0x00);

  // Amplifier drive start position set register
  lcdPortWriteReg(82, 0x2E);
  // Amplifier drive end position set register
  lcdPortWriteReg(83, 0xC4);

  // GOE1 start position set register
  lcdPortWriteReg(86, 0x15);
  // GOE1 end position set register
  lcdPortWriteReg(87, 0xED);

  // Bias adjustment registers
  lcdPortWriteReg(95, 0x3F);
  lcdPortWriteReg(96, 0x22);

  // Power supply control registers 2-5
  lcdPortWriteReg(25, 0x76);
  lcdPortWriteReg(26, 0x54);
  lcdPortWriteReg(27, 0x67);
  lcdPortWriteReg(28, 0x60);

  // Gate scan setting register
  lcdPortWriteReg(29, 0x04);

  // Common setting register
  lcdPortWriteReg(30, 0x1C);

  // Common amplitude setting register
  lcdPortWriteReg(31, 0xA9);

  // Common center voltage setting register
  lcdPortWriteReg(32, 0x00);

  // Power supply rising select register
  lcdPortWriteReg(33, 0x20);

  // Power supply control register 1
  lcdPortWriteReg(24, 0x77);

  // Make a 30�s wait
  ;   // Enter suitable delay code

  DisplayFrameTransferEnabled = 1;   // Display frame transfer may begin

  // GOE1 output control register
  lcdPortWriteReg(59, 0x01);

  // Make a 20ms wait
  ;   // Enter suitable delay code

  // Control register 1
  lcdPortWriteReg(0, 0x00);
}

// Refreshes display buffer to display
void GuiDisplay_Refresh (void)
{
  // As the �PD161607 display controller uses direct frame transfer, not
  // address + data transfer, the GuiLib_DisplayRepaint info on display data
  // updated since last refresh is not used.
  //
  // Instead a frame transfer shall be made from the internal easyGUI display
  // buffer (GuiLib_DisplayBuf in GuiGraphXX.c) to the display controller.
  //
  // The transfer shall consist of GuiConst_DISPLAY_BYTES bytes, organized in
  // GuiConst_DISPLAY_HEIGHT_HW scan lines, each consisting of
  // GuiConst_DISPLAY_WIDTH_HW pixels, each consisting of
  // GuiConst_COLOR_BYTE_SIZE bytes.
  // Starting address of the frame transfer is &GuiLib_DisplayBuf[0][0].

  if (DisplayFrameTransferEnabled)
  {
    // Lock GUI resources
    GuiDisplay_Lock ();

    // Make frame transfer
    // Enter suitable code

    // Free GUI resources
    GuiDisplay_Unlock ();
  }
}

#endif // LCD_CONTROLLER_TYPE_UPD161607

// ============================================================================
//
// DISPLAY DRIVER PART ENDS
//
// ============================================================================


#ifndef GuiConst_DISPLAY_BUFFER_EASYGUI
// ============================================================================
//
// DIRECT DISPLAY BUFFER HANDLING PART
// ONLY USED FOR DIRECT WRITING DISPLAY BUFFER MODE
//
// This part of the GuiDisplay.c file is only used when the display buffer
// reading/writing mode is set to "Direct writing display buffer" (easyGUI
// Parameters window, Display controller page).
//
// The two functions below should be filled with commands for reading data
// from, and writing data to the display controller memory. Any method for
// reading/writing data can be used.
//
// Please observe that these functions are used in multiple instances
// throughout the GuiGraphXX.c files (XX = color depth), so any function code
// must be kept efficient, as slow and inefficient code will have a major
// impact on the speed of the finished system.
//
// ============================================================================

// Enter your display buffer reading code in the following function
GuiConst_INTCOLOR GuiDisplay_BufferRead(
   GuiConst_INT32U Offset,   // Pixel index
   GuiConst_INT32U Count)    // Number of pixels to read
                             // Currently (easyGUI v6.0.0) always 1
{
  // Insert some code!
}

// Enter your display buffer writing code in the following function
void GuiDisplay_BufferWrite(
   GuiConst_INT32U Offset,     // Pixel index
   GuiConst_INTCOLOR *Pixel,   // Address of first pixel data to write
   GuiConst_INT32S Count)      // Number of pixels to write
                               //   Count>0: Write COUNT number of pixels
                               //   Count<0: Repeat Pixel -Count times
{
  // Insert some code!
}
#endif

