/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2008 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*                         Graphics include library                         */
/*                               v5.4.3.015                                 */
/*                                                                          */
/*     GuiLib.c include file - do NOT reference it in your linker setup     */
/*                                                                          */
/* ************************************************************************ */

//==============================================================================
#define GuiLib_DEG              10
#define GuiLib_DEG360           360 * GuiLib_DEG

#define GuiLib_RAD              4096
#define GuiLib_RAD_QUARTER_PI   3217
#define GuiLib_RAD_HALF_PI      6434
#define GuiLib_RAD_PI           12868
#define GuiLib_RAD_2_PI         25736
#define GuiLib_RAD_TO_DEG       573

//==============================================================================

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_DegToRad(
   GuiConst_INT32S Angle)
{
  return ((GuiLib_RAD * Angle) / GuiLib_RAD_TO_DEG);
}

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_RadToDeg(
   GuiConst_INT32S Angle)
{
  return ((GuiLib_RAD_TO_DEG * Angle) / GuiLib_RAD);
}

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_SinRad(
   GuiConst_INT32S Angle)
{
  GuiConst_INT32S A, B, X2;

  Angle %= GuiLib_RAD_2_PI;

  if (Angle > GuiLib_RAD_PI)
    X2 = GuiLib_RAD_2_PI - Angle;
  else
    X2 = Angle;
  if (X2 > GuiLib_RAD_HALF_PI)
    X2 = GuiLib_RAD_PI - X2;

  A = ((((X2 * X2) >> 12) * X2) / 6) >> 12;
  B = ((((A * X2) >> 12) * X2) / 20) >> 12;
  B = X2 - A + B - ((((B * X2) / 42) * X2) >> 24);

  if (Angle > GuiLib_RAD_PI)
    return (-B);
  else
    return (B);
}

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_SinDeg(
   GuiConst_INT32S Angle)
{
  return (GuiLib_SinRad(GuiLib_DegToRad(Angle)));
}

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_CosRad(
   GuiConst_INT32S Angle)
{
  GuiConst_INT32S A, B, C, X2;

  Angle %= GuiLib_RAD_2_PI;

  if (Angle > GuiLib_RAD_PI)
    Angle = GuiLib_RAD_2_PI - Angle;
  if (Angle > GuiLib_RAD_HALF_PI)
    X2 = GuiLib_RAD_PI - Angle;
  else
    X2 = Angle;

  A = (X2 * X2) >> 13;
  B = ((((A * X2) >> 12) * X2) / 12) >> 12;
  C = (((B * X2) / 30) * X2) >> 24;
  C = GuiLib_RAD - A + B - C + (((((C * X2) / 8) * X2) / 7) >> 24);
  if (Angle > GuiLib_RAD_HALF_PI)
    return (-C);
  else
    return (C);
}

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_CosDeg(
   GuiConst_INT32S Angle)
{
  return (GuiLib_CosRad(GuiLib_DegToRad(Angle)));
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_Sqrt(
   GuiConst_INT32U X)
{
  GuiConst_INT32U X1, X2;

  if (X == 0)
    return (0);

  X1 = (X / 2) + 1;
  X2 = (X1 + (X / X1)) / 2;
  while (X2 < X1)
  {
    X1 = X2;
    X2 = (X1 + (X / X1)) / 2;
  }
  return (X1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_GetRedRgbColor(
   GuiConst_INT32U RgbColor)
{
  return (RgbColor & 0x000000FF);
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_SetRedRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT8U RedColor)
{
  return ((RgbColor & 0x00FFFF00) | RedColor);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_GetGreenRgbColor(
   GuiConst_INT32U RgbColor)
{
  return (GuiConst_INT8U)((RgbColor & 0x0000FF00) >> 8);
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_SetGreenRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT8U GreenColor)
{
  return ((RgbColor & 0x00FF00FF) | ((GuiConst_INT32U)GreenColor << 8));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_GetBlueRgbColor(
   GuiConst_INT32U RgbColor)
{
  return (GuiConst_INT8U)((RgbColor & 0x00FF0000) >> 16);
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_SetBlueRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT8U BlueColor)
{
  return ((RgbColor & 0xFF00FFFF) | ((GuiConst_INT32U)BlueColor << 16));
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_RgbToPixelColor(
   GuiConst_INT32U RgbColor)
{
#ifdef GuiConst_COLOR_MODE_GRAY
  return ((GuiConst_COLOR_MAX *
     (GuiConst_INT16U)GuiLib_RgbColorToGrayScale(RgbColor)) / 255);
#else
#ifdef GuiConst_COLOR_MODE_PALETTE
  GuiConst_INT16U ColorR, ColorG, ColorB;
  GuiConst_INT16U I, I2;
  GuiConst_INT16U Rating, NewRating;
  GuiConst_INT32U PColor;

  ColorR = RgbColor & 0x000000FF;
  ColorG = (RgbColor & 0x0000FF00) >> 8;
  ColorB = (RgbColor & 0x00FF0000) >> 16;
#ifndef GuiConst_COLOR_RGB_STANDARD
  ColorR = (GuiConst_COLORCODING_R_MAX * ColorR) / 255;
  ColorG = (GuiConst_COLORCODING_G_MAX * ColorG) / 255;
  ColorB = (GuiConst_COLORCODING_B_MAX * ColorB) / 255;
#endif
  Rating = 0xFFFF;
  for (I = 0; I < GuiConst_PALETTE_SIZE; I++)
  {
    PColor = (GuiConst_INT32U)GuiStruct_Palette[I][0] |
            ((GuiConst_INT32U)GuiStruct_Palette[I][1] << 8) |
            ((GuiConst_INT32U)GuiStruct_Palette[I][2] << 16);
    NewRating =
       abs(ColorR - ((PColor & GuiConst_COLORCODING_R_MASK) >>
       GuiConst_COLORCODING_R_START)) +
       abs(ColorG - ((PColor & GuiConst_COLORCODING_G_MASK) >>
       GuiConst_COLORCODING_G_START)) +
       abs(ColorB - ((PColor & GuiConst_COLORCODING_B_MASK) >>
       GuiConst_COLORCODING_B_START));
    if (NewRating < Rating)
    {
      Rating = NewRating;
      I2 = I;
    }
  }
  return (I2);
#else
#ifdef GuiConst_COLOR_RGB_STANDARD
  return (RgbColor);
#else
  return ((((GuiConst_COLORCODING_R_MAX *
     (RgbColor & 0x000000FF)) / 255) << GuiConst_COLORCODING_R_START) |
     (((GuiConst_COLORCODING_G_MAX *
     ((RgbColor & 0x0000FF00) >> 8)) / 255) << GuiConst_COLORCODING_G_START) |
     (((GuiConst_COLORCODING_B_MAX *
     ((RgbColor & 0x00FF0000) >> 16)) / 255) << GuiConst_COLORCODING_B_START));
#endif
#endif
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_PixelToRgbColor(
   GuiConst_INTCOLOR PixelColor)
{
#ifdef GuiConst_COLOR_MODE_GRAY
  GuiConst_INT16U GrayValue;

  GrayValue = (255 * (GuiConst_INT16U)PixelColor) / GuiConst_COLOR_MAX;
  return (GuiLib_GrayScaleToRgbColor((GuiConst_INT8U)GrayValue));
#else
  GuiConst_INT32U PColor;
#ifndef GuiConst_COLOR_RGB_STANDARD
  GuiConst_INT32U ColorR, ColorG, ColorB;
#endif

#ifdef GuiConst_COLOR_MODE_PALETTE
  PColor = (GuiConst_INT32U)GuiStruct_Palette[PixelColor][0] |
          ((GuiConst_INT32U)GuiStruct_Palette[PixelColor][1] << 8) |
          ((GuiConst_INT32U)GuiStruct_Palette[PixelColor][2] << 16);
#else
  PColor = PixelColor;
#endif

#ifdef GuiConst_COLOR_RGB_STANDARD
  return (PColor);
#else
  ColorR =
     (PColor & GuiConst_COLORCODING_R_MASK) >> GuiConst_COLORCODING_R_START;
  ColorG =
     (PColor & GuiConst_COLORCODING_G_MASK) >> GuiConst_COLORCODING_G_START;
  ColorB =
     (PColor & GuiConst_COLORCODING_B_MASK) >> GuiConst_COLORCODING_B_START;
  ColorR = (255 * ColorR) / GuiConst_COLORCODING_R_MAX;
  ColorG = (255 * ColorG) / GuiConst_COLORCODING_G_MAX;
  ColorB = (255 * ColorB) / GuiConst_COLORCODING_B_MAX;
  return (ColorR | (ColorG << 8) | (ColorB << 16));
#endif
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_RgbColorToGrayScale(
   GuiConst_INT32U RgbColor)
{
  return (((RgbColor & 0x000000FF) +
     ((RgbColor & 0x0000FF00) >> 8) +
     ((RgbColor & 0x00FF0000) >> 16)) / 3);
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_GrayScaleToRgbColor(
   GuiConst_INT8U GrayValue)
{
  return ((GuiConst_INT32U)GrayValue |
         ((GuiConst_INT32U)GrayValue << 8) |
         ((GuiConst_INT32U)GrayValue << 16));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_PixelColorToGrayScale(
   GuiConst_INTCOLOR PixelColor)
{
  return (GuiLib_RgbColorToGrayScale(GuiLib_PixelToRgbColor(PixelColor)));
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_GrayScaleToPixelColor(
   GuiConst_INT8U GrayValue)
{
  return (GuiLib_RgbToPixelColor(GuiLib_GrayScaleToRgbColor(GrayValue)));
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_BrightenRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT16U Amount)
{
  GuiConst_INT32U ColorR, ColorG, ColorB;

  if (Amount == 0)
    return (RgbColor);
  else
  {
    ColorR = 255 - ((GuiConst_INT32U)(255 - GuiLib_GetRedRgbColor(RgbColor)) *
       (1000 - Amount) / 1000);
    ColorG = 255 - ((GuiConst_INT32U)(255 - GuiLib_GetGreenRgbColor(RgbColor)) *
       (1000 - Amount) / 1000);
    ColorB = 255 - ((GuiConst_INT32U)(255 - GuiLib_GetBlueRgbColor(RgbColor)) *
       (1000 - Amount) / 1000);
    return (ColorR | (ColorG << 8) | (ColorB << 16));
  }
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_BrightenPixelColor(
   GuiConst_INTCOLOR PixelColor,
   GuiConst_INT16U Amount)
{
  return (GuiLib_RgbToPixelColor(
     GuiLib_BrightenRgbColor(GuiLib_PixelToRgbColor(PixelColor), Amount)));
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_DarkenRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT16U Amount)
{
  GuiConst_INT32U ColorR, ColorG, ColorB;

  if (Amount == 0)
    return (RgbColor);
  else
  {
    ColorR = (GuiConst_INT32U)GuiLib_GetRedRgbColor(RgbColor) *
       (1000 - Amount) / 1000;
    ColorG = (GuiConst_INT32U)GuiLib_GetGreenRgbColor(RgbColor) *
       (1000 - Amount) / 1000;
    ColorB = (GuiConst_INT32U)GuiLib_GetBlueRgbColor(RgbColor) *
       (1000 - Amount) / 1000;
    return (ColorR | (ColorG << 8) | (ColorB << 16));
  }
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_DarkenPixelColor(
   GuiConst_INTCOLOR PixelColor,
   GuiConst_INT16U Amount)
{
  return (GuiLib_RgbToPixelColor(
     GuiLib_DarkenRgbColor(GuiLib_PixelToRgbColor(PixelColor), Amount)));
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_AccentuateRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT16U Amount)
{
  if (Amount == 0)
    return (RgbColor);
  else if (GuiLib_RgbColorToGrayScale(RgbColor) <= 127)
    return (GuiLib_BrightenRgbColor(RgbColor, Amount));
  else
    return (GuiLib_DarkenRgbColor(RgbColor, Amount));
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_AccentuatePixelColor(
   GuiConst_INTCOLOR PixelColor,
   GuiConst_INT16U Amount)
{
  return (GuiLib_RgbToPixelColor(
     GuiLib_AccentuateRgbColor(GuiLib_PixelToRgbColor(PixelColor), Amount)));
}

//------------------------------------------------------------------------------
void GuiLib_Ellipse(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U Radius1,
   GuiConst_INT16U Radius2,
   GuiConst_INT32S BorderColor,
   GuiConst_INT32S FillColor)
{
  GuiConst_INT16S PX, PY;
  GuiConst_INT16S L1, L2, LP1, LP2, LX, LY;
  GuiConst_INT16S PX2, PY2;
  GuiConst_INTCOLOR BColor;
  GuiConst_INT8U Filling, LFilling, First;
  GuiConst_INT16S R1, R2;
  GuiConst_INT32S R22;

  Filling = (FillColor != GuiLib_NO_COLOR);
  if (BorderColor == GuiLib_NO_COLOR)
  {
    if (!Filling)
      return;
    else
      BColor = (GuiConst_INTCOLOR)FillColor;
  }
  else
    BColor = (GuiConst_INTCOLOR)BorderColor;

  if (Radius1 == 0)
    GuiLib_VLine(X, Y - Radius2, Y + Radius2, BColor);
  else if (Radius2 == 0)
    GuiLib_HLine(X - Radius1, X + Radius1, Y, BColor);
  else if (Radius1 == 1)
  {
    PY = Radius2 / 2;
    GuiLib_VLine(X - 1, Y - PY, Y + PY, BColor);
    GuiLib_VLine(X, Y - Radius2, Y - PY - 1, BColor);
    if (Filling)
      GuiLib_VLine(X, Y - PY, Y + PY, (GuiConst_INTCOLOR)FillColor);
    GuiLib_VLine(X, Y + PY + 1, Y + Radius2, BColor);
    GuiLib_VLine(X + 1, Y - PY, Y + PY, BColor);
  }
  else if (Radius2 == 1)
  {
    PX = Radius1 / 2;
    GuiLib_HLine(X - PX, X + PX, Y - 1, BColor);
    GuiLib_HLine(X - Radius1, X - PX - 1, Y, BColor);
    if (Filling)
      GuiLib_HLine(X - PX, X + PX, Y, (GuiConst_INTCOLOR)FillColor);
    GuiLib_HLine(X + PX + 1, X + Radius1, Y, BColor);
    GuiLib_HLine(X - PX, X + PX, Y + 1, BColor);
  }
  else
  {
    R1 = Radius1 << 3;
    R2 = Radius2 << 3;

    if (Radius1 >= Radius2)
    {
      R22 = (GuiConst_INT32S)R2 * R2;

      PX = 0;
      First = 1;
      for (PY=R2-4; PY>=4; PY-=8)
      {
        PX2 = PX + 8;
        PX = (GuiConst_INT16S)((R1 * GuiLib_Sqrt((0x00000400 -
           ((((GuiConst_INT32S)PY * PY) << 10) / R22)) << 8)) >> 9) + 4;
        LY = (PY + 4) >> 3;
        if (First)
        {
          L1 = X - (PX >> 3);
          L2 = X + (PX >> 3);
          GuiLib_HLine(L1, L2, Y - LY, BColor);
          GuiLib_HLine(L1, L2, Y + LY, BColor);
          First = 0;
        }
        else
        {
          LP1 = X - (PX2 >> 3);
          LP2 = X + (PX2 >> 3);
          L1 = X - (PX >> 3);
          L2 = X + (PX >> 3);
          if (LP1 < L1)
          {
            LP1 = L1;
            LP2 = L2;
          }
          LFilling = Filling & (LP2 - LP1 >= 2);
          GuiLib_HLine(L1, LP1, Y - LY, BColor);
          if (LFilling)
            GuiLib_HLine(LP1 + 1, LP2 - 1, Y - LY,
               (GuiConst_INTCOLOR)FillColor);
          GuiLib_HLine(LP2, L2, Y - LY, BColor);

          GuiLib_HLine(L1, LP1, Y + LY, BColor);
          if (LFilling)
            GuiLib_HLine(LP1 + 1, LP2 - 1, Y + LY,
               (GuiConst_INTCOLOR)FillColor);
          GuiLib_HLine(LP2, L2, Y + LY, BColor);
        }
      }
      L1 = (PX + 8) >> 3;
      if (L1 > Radius1)
        L1 = Radius1;
      GuiLib_HLine(X - Radius1, X - L1, Y, BColor);
      if (Filling & (L1 >= 1))
        GuiLib_HLine(X - L1 + 1, X + L1 - 1, Y, (GuiConst_INTCOLOR)FillColor);
      GuiLib_HLine(X + L1, X + Radius1, Y, BColor);
    }
    else
    {
      R22 = (GuiConst_INT32S)R1 * R1;

      PY = 0;
      First = 1;
      for (PX=R1-4; PX>=4; PX-=8)
      {
        PY2 = PY + 8;
        PY = (GuiConst_INT16S)((R2 * GuiLib_Sqrt((0x00000400 -
           ((((GuiConst_INT32S)PX * PX) << 10) / R22)) << 8)) >> 9) + 4;
        LX = (PX + 4) >> 3;
        if (First)
        {
          L1 = Y - (PY >> 3);
          L2 = Y + (PY >> 3);
          GuiLib_VLine(X - LX, L1, L2, BColor);
          GuiLib_VLine(X + LX, L1, L2, BColor);
          First = 0;
        }
        else
        {
          LP1 = Y - (PY2 >> 3);
          LP2 = Y + (PY2 >> 3);
          L1 = Y - (PY >> 3);
          L2 = Y + (PY >> 3);
          if (LP1 < L1)
          {
            LP1 = L1;
            LP2 = L2;
          }
          LFilling = Filling & (LP2 - LP1 >= 2);
          GuiLib_VLine(X - LX, L1, LP1, BColor);
          if (LFilling)
            GuiLib_VLine(X - LX, LP1 + 1, LP2 - 1,
               (GuiConst_INTCOLOR)FillColor);
          GuiLib_VLine(X - LX, LP2, L2, BColor);
          GuiLib_VLine(X + LX, L1, LP1, BColor);
          if (LFilling)
            GuiLib_VLine(X + LX, LP1 + 1, LP2 - 1,
               (GuiConst_INTCOLOR)FillColor);
          GuiLib_VLine(X + LX, LP2, L2, BColor);
        }
      }
      L1 = (PY + 8) >> 3;
      if (L1 > Radius2)
        L1 = Radius2;
      GuiLib_VLine(X, Y - Radius2, Y - L1, BColor);
      if (Filling & (L1 >= 1))
        GuiLib_VLine(X, Y - L1 + 1, Y + L1 - 1, (GuiConst_INTCOLOR)FillColor);
      GuiLib_VLine(X, Y + L1, Y + Radius2, BColor);
    }
  }
}

//------------------------------------------------------------------------------
void GuiLib_Circle(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U Radius,
   GuiConst_INT32S BorderColor,
   GuiConst_INT32S FillColor)
{
  GuiLib_Ellipse(X, Y, Radius, Radius, BorderColor, FillColor);
}

//==============================================================================

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
void GuiLib_ResetClipping(void)
{
  ClippingTotal = 0;
  SetClipping(
     0, 0, GuiConst_DISPLAY_WIDTH_HW - 1, GuiConst_DISPLAY_HEIGHT_HW - 1);
}

//------------------------------------------------------------------------------
void GuiLib_SetClipping(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  ClippingTotal = ((X1 > X2) || (Y1 > Y2));

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

  SetClipping(X1, Y1, X2, Y2);
}
#endif

//------------------------------------------------------------------------------
void GuiLib_ResetDisplayRepaint(void)
{
  GuiConst_INT16S LineNo;

  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++)
    GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
}

//------------------------------------------------------------------------------
void GuiLib_MarkDisplayBoxRepaint(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);
}

//------------------------------------------------------------------------------
void GuiLib_ClearDisplay(void)
{
  ClearDisplay();
  MarkDisplayBoxRepaint(
     0, 0, GuiConst_DISPLAY_WIDTH_HW - 1, GuiConst_DISPLAY_HEIGHT_HW - 1);
}

//------------------------------------------------------------------------------
void GuiLib_Dot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST(Color);

  MakeDot(X, Y, Color);
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_GetDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y)
{
  GuiLib_COORD_ADJUST(X, Y);

  return (ReadDot(X, Y));
}

//------------------------------------------------------------------------------
void GuiLib_Line(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S X, Y;
  GuiConst_INT32S Slope;
  GuiConst_INT16S Offset;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  GuiLib_COLOR_ADJUST(Color);

  if (X1 == X2)
  {
    OrderCoord(&Y1, &Y2);
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
    {
      VertLine(X1, Y1, Y2, Color);
      MarkDisplayBoxRepaint(X1, Y1, X1, Y2);
    }
  }
  else if (Y1 == Y2)
  {
    OrderCoord(&X1, &X2);
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
    {
      HorzLine(X1, X2, Y1, Color);
      MarkDisplayBoxRepaint(X1, Y1, X2, Y1);
    }
  }
  else
  {
    Slope = labs ((10000 * (GuiConst_INT32S) (Y2 - Y1))) /
       labs ((GuiConst_INT32S) (X2 - X1));

    if (Slope <= 10000)
    {
      if (OrderCoord(&X1, &X2))
        SwapCoord(&Y1, &Y2);

      for (X = 0; X <= X2 - X1; X++)
      {
        Offset = (((GuiConst_INT32S) X * Slope) + 5000) / 10000;
        if (Y1 < Y2)
          MakeDot(X1 + X, Y1 + Offset, Color);
        else
          MakeDot(X1 + X, Y1 - Offset, Color);
      }
    }
    else
    {
      if (OrderCoord(&Y1, &Y2))
        SwapCoord(&X1, &X2);

      for (Y = 0; Y <= Y2 - Y1; Y++)
      {
        Offset = (((GuiConst_INT32S) Y * 10000) + (Slope / 2)) / Slope;
        if (X1 < X2)
          MakeDot(X1 + Offset, Y1 + Y, Color);
        else
          MakeDot(X1 - Offset, Y1 + Y, Color);
      }
    }
  }
}

//------------------------------------------------------------------------------
void GuiLib_LinePattern(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT8U LinePattern,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT32S Slope;
  GuiConst_INT16S Offset;
  GuiConst_INT8U MaskTemp;
  GuiConst_INT8U MaskCtr = 0;
  GuiConst_INT8U Vertical;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  GuiLib_COLOR_ADJUST(Color);

  Vertical = (X1 == X2);
  if (Vertical)
  {
    Offset = 0;
    Slope = 0;
  }
  else
    Slope = labs ((10000 * (GuiConst_INT32S)(Y2 - Y1))) /
            labs ((GuiConst_INT32S)(X2 - X1));

  if (Vertical || (Slope > 10000))
  {
    if (OrderCoord(&Y1, &Y2))
      SwapCoord(&X1, &X2);

    for (Y = 0; Y <= Y2 - Y1; Y++)
    {
      if (MaskCtr == 0 )
      {
        MaskCtr = 8;
        MaskTemp = LinePattern;
      }
      if (MaskTemp & 0x01)
      {
        if (!Vertical)
          Offset = (((GuiConst_INT32S) Y * 10000) + (Slope / 2)) / Slope;
        if (X1 < X2)
          MakeDot(X1 + Offset, Y1 + Y, Color);
        else
          MakeDot(X1 - Offset, Y1 + Y, Color);
      }
      MaskTemp >>= 1;
      MaskCtr--;
    }
  }
  else
  {
    if (OrderCoord(&X1, &X2))
      SwapCoord(&Y1, &Y2);

    for (X = 0; X <= X2 - X1; X++)
    {
      if (MaskCtr == 0 )
      {
        MaskCtr = 8;
        MaskTemp = LinePattern;
      }

      if (MaskTemp & 0x01)
      {
        Offset = (((GuiConst_INT32S) X * Slope) + 5000) / 10000;
        if (Y1 < Y2)
          MakeDot(X1 + X, Y1 + Offset, Color);
        else
          MakeDot(X1 + X, Y1 - Offset, Color);
      }
      MaskTemp >>= 1;
      MaskCtr--;
    }
  }
}

//------------------------------------------------------------------------------
void GuiLib_HLine(
   GuiConst_INT16S X1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiLib_Line(X1, Y, X2, Y, Color);
}

//------------------------------------------------------------------------------
void GuiLib_VLine(
   GuiConst_INT16S X,
   GuiConst_INT16S Y1,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiLib_Line(X, Y1, X, Y2, Color);
}

//------------------------------------------------------------------------------
void GuiLib_Box(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S X1C;
  GuiConst_INT16S Y1C;
  GuiConst_INT16S X2C;
  GuiConst_INT16S Y2C;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  GuiLib_COLOR_ADJUST(Color);

  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

  X1C = X1;
  Y1C = Y1;
  X2C = X2;
  Y2C = Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect(&X1C, &Y1C, &X2C, &Y2C))
#endif
  {
    X1C = X1;
    Y1C = Y1;
    X2C = X1;
    Y2C = Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if (CheckRect(&X1C, &Y1C, &X1C, &Y2C))
#endif
      VertLine(X1C, Y1C, Y2C, Color);
    X1C = X2;
    Y1C = Y1;
    X2C = X2;
    Y2C = Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if (CheckRect(&X2C, &Y1C, &X2C, &Y2C))
#endif
      VertLine(X2C, Y1C, Y2C, Color);
    if (X2 - X1 > 1)
    {
      X1C = X1 + 1;
      Y1C = Y1;
      X2C = X2 - 1;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
      if (CheckRect(&X1C, &Y1C, &X2C, &Y1C))
#endif
        HorzLine(X1C, X2C, Y1C, Color);
      X1C = X1 + 1;
      Y1C = Y2;
      X2C = X2 - 1;
      Y2C = Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
      if (CheckRect(&X1C, &Y2C, &X2C, &Y2C))
#endif
        HorzLine(X1C, X2C, Y2C, Color);
    }
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    CheckRect(&X1, &Y1, &X2, &Y2);
#endif
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);
  }
}

//------------------------------------------------------------------------------
void GuiLib_FillBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  GuiLib_COLOR_ADJUST(Color);

  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);
#ifdef GuiConst_BYTE_HORIZONTAL
    while (Y1 <= Y2)
    {
      HorzLine(X1, X2, Y1, Color);
      Y1++;
    }
#else
    while (X1 <= X2)
    {
      VertLine(X1, Y1, Y2, Color);
      X1++;
    }
#endif
  }
}

//------------------------------------------------------------------------------
void GuiLib_BorderBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR BorderColor,
   GuiConst_INTCOLOR FillColor)
{
  GuiLib_Box(X1, Y1, X2, Y2, BorderColor);
  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);
  if (((X2 - X1) >= 2) && ((Y2 - Y1) >= 2))
    GuiLib_FillBox(X1+1, Y1+1, X2-1, Y2-1, FillColor);
}

#ifdef GuiConst_BITMAP_SUPPORT_ON
//------------------------------------------------------------------------------
void GuiLib_ShowBitmap(
   GuiConst_INT16U BitmapIndex,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT32S TranspColor)
{
#ifdef GuiConst_REMOTE_BITMAP_DATA
  if (BitmapIndex != CurRemoteBitmap)
  {
    GuiLib_RemoteDataReadBlock(
     (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex],
     (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex + 1] -
     (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex],
     GuiLib_RemoteBitmapBuffer);
  CurRemoteBitmap = BitmapIndex;
  }
  ShowBitmapArea(&GuiLib_RemoteBitmapBuffer[0],
   X, Y, 0, 0, 0, 0, TranspColor, GuiLib_FULL_BITMAP);
#else
#ifdef GuiConst_AVRGCC_COMPILER
  ShowBitmapArea(
     (GuiConst_INT8U PrefixRom *)pgm_read_word(&GuiStruct_BitmapPtrList[BitmapIndex]),
      X, Y, 0, 0, 0, 0, TranspColor, GuiLib_FULL_BITMAP);
#else
  ShowBitmapArea(
     (GuiConst_INT8U PrefixRom *)GuiStruct_BitmapPtrList[BitmapIndex],
      X, Y, 0, 0, 0, 0, TranspColor, GuiLib_FULL_BITMAP);
#endif
#endif
}

//------------------------------------------------------------------------------
void GuiLib_ShowBitmapAt(
   GuiConst_INT8U * BitmapPtr,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT32S TranspColor)
{
  ShowBitmapArea(
   BitmapPtr, X, Y, 0, 0, 0, 0, TranspColor, GuiLib_FULL_BITMAP);
}

//------------------------------------------------------------------------------
void GuiLib_ShowBitmapArea(
   GuiConst_INT16U BitmapIndex,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S AX1,
   GuiConst_INT16S AY1,
   GuiConst_INT16S AX2,
   GuiConst_INT16S AY2,
   GuiConst_INT32S TranspColor)
{
#ifdef GuiConst_REMOTE_BITMAP_DATA
  if (BitmapIndex != CurRemoteBitmap)
  {
  GuiLib_RemoteDataReadBlock(
     (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex],
     (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex + 1] -
     (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex],
     GuiLib_RemoteBitmapBuffer);
  CurRemoteBitmap = BitmapIndex;
  }
  ShowBitmapArea(&GuiLib_RemoteBitmapBuffer[0],
   X, Y, AX1, AY1, AX2, AY2, TranspColor, GuiLib_AREA_BITMAP);
#else
#ifdef GuiConst_AVRGCC_COMPILER
  ShowBitmapArea(
     (GuiConst_INT8U PrefixRom *)pgm_read_word(&GuiStruct_BitmapPtrList[BitmapIndex]),
      X, Y, AX1, AY1, AX2, AY2, TranspColor, GuiLib_AREA_BITMAP);
#else
  ShowBitmapArea(
     (GuiConst_INT8U PrefixRom *)GuiStruct_BitmapPtrList[BitmapIndex],
      X, Y, AX1, AY1, AX2, AY2, TranspColor, GuiLib_AREA_BITMAP);
#endif
#endif
}

//------------------------------------------------------------------------------
void GuiLib_ShowBitmapAreaAt(
   GuiConst_INT8U * BitmapPtr,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S AX1,
   GuiConst_INT16S AY1,
   GuiConst_INT16S AX2,
   GuiConst_INT16S AY2,
   GuiConst_INT32S TranspColor)
{
  ShowBitmapArea(
     BitmapPtr, X, Y, AX1, AY1, AX2, AY2, TranspColor, GuiLib_AREA_BITMAP);
}
#endif

