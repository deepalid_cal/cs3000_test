/***********************************************************************
 * $Id:: mem_setup.c 4681 2010-08-27 23:18:08Z usb10132                $
 *
 * Project: Memory setup code
 *
 * Description:
 *     Memory interface setup for the board
 *
 ***********************************************************************
 * Software that is described herein is for illustrative purposes only  
 * which provides customers with programming information regarding the  
 * products. This software is supplied "AS IS" without any warranties.  
 * NXP Semiconductors assumes no responsibility or liability for the 
 * use of the software, conveys no license or title under any patent, 
 * copyright, or mask work right to the product. NXP Semiconductors 
 * reserves the right to make changes in the software without 
 * notification. NXP Semiconductors also make no representation or 
 * warranty that such application will be suitable for the specified 
 * use without further testing or modification. 
 **********************************************************************/

#include "startup.h"
#include "dram_configs.h"
#include "lpc32xx_emc.h"
#include "lpc32xx_clkpwr_driver.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// By defing this the debugger will have a local variable so you can inpect the values actually computed.
// Default is to comment out.
//#define ALLOW_DEBUGGER_TO_SEE

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* External static memory timings used for chip select 0 (see the users
   guide for what these values do). Optimizing these values will help
   with NOR program and boot speed. These should be programmed to work
   with the selected bus (HCLK) speed. */
#define EMCSTATICWAITWEN_CLKS	0xF
#define EMCSTATICWAITOEN_CLKS	0xF
#define EMCSTATICWAITRD_CLKS	0x1F
#define EMCSTATICWAITPAGE_CLKS	0x1F
#define EMCSTATICWAITWR_CLKS	0x1F
#define EMCSTATICWAITTURN_CLKS	0xF

void setup_CS1_SRAM_R1LV0416DSB_timing( void )
{
	// The Renesas R1LV0416DSB is on CS1 and is a 55ns battery backed SRAM. The battery backed does not influence the
	// settings here. These are the settings with a 104MHz HCLK.
	// 
	EMC->emcstatic_regs[ 1 ].emcstatic_config = EMC_STC_BLS_EN_BIT | EMC_STC_MEMWIDTH_16;

	// According to the data sheet this is 0ns. The minimum we can set is 0. Which is ONE clk or about 9.6ns.
	EMC->emcstatic_regs[ 1 ].emcstatic_WE_delay = 0;

	// Again I think the implication from the data sheet is this is 0ns. And this one truly becomes 0.
	EMC->emcstatic_regs[ 1 ].emcstatic_OE_delay = 0;

	// The access time is 55ns. We'll make it 5 clks. Which is actually 6 clocks by register definition, or about 57ns.
	EMC->emcstatic_regs[ 1 ].emcstatic_RD_access_delay = 5;

	EMC->emcstatic_regs[ 1 ].emcstatic_page_RD_delay = 5;  // this is not used I think

	// This register adds 2 to the number we put here. The SRAM needs 55ns. Or 6 clocks. So set to 4.
	EMC->emcstatic_regs[ 1 ].emcstatic_WR_cycle_delay = 4;

	// To avoid bus contention the Renesas part needs 20ns to relase the bus to Hi-Z.
	EMC->emcstatic_regs[ 1 ].emcstatic_turn_around_delay = 2;  // again actually +1 clock cycles
}

/* ---------------------------------------------------------- */
void setup_SDRAM_timing( void )
{
	UNS_32 dramclk;

	#ifdef ALLOW_DEBUGGER_TO_SEE
		UNS_32	lval;
	#endif

    /* Mirror IRAM at address 0x0 */
    CLKPWR->clkpwr_bootmap = CLKPWR_BOOTMAP_SEL_BIT;

	/* Enable HCLK and SDRAM bus clocks before EMC accesses */
	CLKPWR->clkpwr_sdramclk_ctrl = 0;

	/* Enable EMC interface */
	EMC->emccontrol = EMC_DYN_SDRAM_CTRL_EN;
	#ifdef ALLOW_DEBUGGER_TO_SEE
		lval = EMC_DYN_SDRAM_CTRL_EN;
	#endif

	EMC->emcconfig = 0;

	/* Set a very long dynamic refresh time so the controller isn't
	   always busy refreshing */
	EMC->emcdynamicrefresh = 0x7ff;

	/* Only 1 SDRAM is intialized based on selected memory option */
	dramclk = clkpwr_get_clock_rate(CLKPWR_SDRAMDDR_CLK);
#if SDRAM_OPTION==SDRAM_SDR_LP
	sdr_sdram_setup(dramclk, 1);
#elif SDRAM_OPTION==SDRAM_SDR_ST
	sdr_sdram_setup(dramclk, 0);
#elif SDRAM_OPTION==SDRAM_DDR_LP
	ddr_sdram_lp_setup(dramclk);
#elif SDRAM_OPTION==SDRAM_DDR_ST
	ddr_sdram_st_setup(dramclk);
#endif

	/* Enable buffers in AHB ports */
	EMC->emcahn_regs [0].emcahbcontrol = EMC_AHB_PORTBUFF_EN;
	#ifdef ALLOW_DEBUGGER_TO_SEE
		lval = EMC_AHB_PORTBUFF_EN;
	#endif
	EMC->emcahn_regs [2].emcahbcontrol = EMC_AHB_PORTBUFF_EN;
	EMC->emcahn_regs [3].emcahbcontrol = EMC_AHB_PORTBUFF_EN;
	EMC->emcahn_regs [4].emcahbcontrol = EMC_AHB_PORTBUFF_EN;

	/* Enable port timeouts */
	EMC->emcahn_regs [0].emcahbtimeout = EMC_AHB_SET_TIMEOUT(32);
	#ifdef ALLOW_DEBUGGER_TO_SEE
		lval = EMC_AHB_SET_TIMEOUT(32);
	#endif
	EMC->emcahn_regs [2].emcahbtimeout = EMC_AHB_SET_TIMEOUT(32);
	EMC->emcahn_regs [3].emcahbtimeout = EMC_AHB_SET_TIMEOUT(32);
	EMC->emcahn_regs [4].emcahbtimeout = EMC_AHB_SET_TIMEOUT(32);
}
