/* file = gpio_setup.h                        04.08.2011  rmd */

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _INC_GPIO_SETUP_H
#define _INC_GPIO_SETUP_H

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 5/2/2017 rmd : NOTE - this file is, somehow, included into the COMMSERVER build. Through
// some chain of includes that I don't know the specifics of.

// 6/26/2014 ajv : Required since this file is shared between the CS3000 and the
// comm server
#include	"cs3000_comm_server_common.h"

#include	"lpc_types.h"

#include	"cs_common.h"

#ifndef	_MSC_VER
	#include	"lpc32xx_gpio.h"

	#include	"lpc3250_chip.h"
#endif
	
	
	
	
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 1/18/2017 rmd : Instead of consuming one of the hardware revision values (REV H would be
// 0, REV J would be 1, leaving only revisions 2 and 3 remaining), we are using the flash
// part recognition as our flag for the type of display we are interfaced to. This is needed
// to manage the display images between the two displays. We agreed the REV J hardware would
// not have the SST25VF serial flash chips. Only the REV H hardware will have the SST25VF.
// So when we see otherwise we KNOW we are interfaced to an AZ Display ATM3224H. At least
// for now that's the story.
#define		DISPLAY_MODEL_KYOCERA_KG057			(0x10)
#define		DISPLAY_MODEL_AZDISPLAYS_ATM3224H	(0x20)

// 1/18/2017 rmd : This is the REV H and REV J hardware. Even though the REV H platform
// includes the USB hardware, the two GPIO pins that serve as the ENUM and SUSPEND inputs to
// the MAXIM USB tranceiver, are reprogrammed as inputs to the LPC3250 in THIS code. And
// LUCKILY they have pull-downs on them. So when this code, which programs those GPIO pins
// to inputs, runs on the older REV H hardware, we read the two pull-downs. And can
// interpret that as our BASELINE ID.
//
// 1/19/2017 rmd : NOTE - the code counts on these defines to be 0, 1, 2, 3 when reading the
// gpio to learn the board revision.
#define		BOARD_HARDWARE_ID_BASELINE		(0)
#define		BOARD_HARDWARE_ID_ONE			(1)
#define		BOARD_HARDWARE_ID_TWO			(2)
#define		BOARD_HARDWARE_ID_THREE			(3)

// 1/18/2017 rmd : This is set during startup during the spi serial flash chip
// initialization. And must be done BEFORE the lcd_init function runs.
extern UNS_32	display_model_is;

// 1/18/2017 rmd : This is set very early on during the GPIO. And identifies the hardware
// platform the code is running with.
extern UNS_32	board_hardware_id;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 2011.08.10 rmd : I had to move the serial definitions here to compile. Getting some crazy circular references between
// TPL_OUT and SERIAL.H. This solved it. Functional but not pretty!

// The Calsense LPC3250 application uses a USB channel and 4 UART channels. We would be using the 4 standard UART's
// (3,4,5,6) but UART 4's TX line is consumed by the LCD controller. So we use one of the so called high speed UART's for
// the interface to the TP board. We use UART 1 for that.

// DO NOT change the order of these around unless you investigate thoroughly all uses. We
// count on their value in some instances.

#define UPORT_TP			(0)			// LPC3250 UART 1	(a high speed 14X Clock UART)
#define UPORT_A				(1)			// LPC3250 UART 3	(a standard 16X Clock UART)
#define UPORT_B				(2)			// LPC3250 UART 6	(a standard 16X Clock UART)
#define UPORT_RRE   		(3)			// LPC3250 UART 5	(a standard 16X Clock UART)
#define nlu_UPORT_USB		(4)			// USB CDC device	(USB stack)  -->>> no longer used
#define UPORT_M1			(5)			// RS-485 port on TP board

// 8/8/2013 rmd : This would refer to the total physical ports on the CS3000 pcb. Five.
#define UPORT_TOTAL_PHYSICAL_PORTS		(nlu_UPORT_USB + 1)

#define UPORT_TOTAL_SYSTEM_PORTS		(UPORT_M1 + 1)

#define UPORTS							UNS_32

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#ifndef _MSC_VER

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// 6/10/2013 rmd : Set to an output, driven low. Has an installed 47K pull down resistor.
#define		CS_NOT_USED_PORT_0_0	P0_GPOP0_I2SRXCLK1

#define		CS_LED_RED				P0_GPOP1_I2SRXWS1

#define		CS_LED_YELLOW			P0_GPOP2_I2SRXSDA0

#define		CS_LED_GREEN			P0_GPOP3_I2SRXCLK0

// 6/10/2013 rmd : Set to an output, driven low. Has an installed 47K pull down resistor.
#define		CS_NOT_USED_PORT_0_4	P0_GPOP4_I2SRXWS0

// 6/10/2013 rmd : Set to an output, driven low. Has an installed 47K pull down resistor.
#define		CS_NOT_USED_PORT_0_5	P0_GPOP5_I2STXSDA0

// 1/19/2017 rmd : In the REV H hardware/firmware these two were set as OUTPUTS. To drive
// the USB ENUM and SUSPEND signals. Starting now we are going to break the USB port. And
// these two pins will be programmed as GPIO INPUTS. And be used to read the BOARD_ID
// resistors.
#define		BOARD_ID_0				P0_GPOP6_I2STXCLK0

#define		BOARD_ID_1				P0_GPOP7_I2STXWS0


#define	BOARD_ID_0_is_HIGH		((P0_INP_STATE & BOARD_ID_0) == BOARD_ID_0)
#define	BOARD_ID_0_is_LOW		((P0_INP_STATE & BOARD_ID_0) == 0)

#define	BOARD_ID_1_is_HIGH		((P0_INP_STATE & BOARD_ID_1) == BOARD_ID_1)
#define	BOARD_ID_1_is_LOW		((P0_INP_STATE & BOARD_ID_1) == 0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		PORT_A_RESET_PIN		_BIT(P1_MUX_SET_P1_23_BIT)

#define		PORT_A_RESET_LOW		(P1_OUTP_CLR = PORT_A_RESET_PIN)

#define		PORT_A_RESET_HIGH		(P1_OUTP_SET = PORT_A_RESET_PIN)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define		PORT_A_RTS_PIN			_BIT( P3_OUTP_SET_GPO_23_BIT )

#define		PORT_B_ENABLE_PIN		_BIT( P3_OUTP_SET_GPO_22_BIT )

#define		PORT_A_ENABLE_PIN		_BIT( P3_OUTP_SET_GPO_19_BIT )


// 6/10/2013 rmd : When moving the pcb from Rev E to Rev G we moved the A_DTR signal from
// U3_DTR to GPO_20. Not sure why except that the GPIO is easier to manage.
#define		PORT_A_DTR_PIN			_BIT( P3_OUTP_SET_GPO_20_BIT )

#define		WP_FLASH_1_PIN			_BIT( P3_OUTP_SET_GPO_17_BIT )

#define		BACKLIGHT_ENABLE_PIN	_BIT( P3_OUTP_SET_GPO_14_BIT )

#define		PORT_RRE_CONFIG_PIN		_BIT( P3_OUTP_SET_GPO_11_BIT )

#define		PORT_RRE_SLEEP_PIN		_BIT( P3_OUTP_SET_GPO_09_BIT )
#define		PORT_RRE_RESET_PIN		_BIT( P3_OUTP_SET_GPO_08_BIT )

#define		PORT_B_RESET_PIN		_BIT( P3_OUTP_SET_GPO_06_BIT )

#define		WP_FLASH_0_PIN			_BIT( P3_OUTP_SET_GPO_05_BIT )

#define		PORT_B_DTR_PIN			_BIT( P3_OUTP_SET_GPO_04_BIT )
#define		PORT_B_RTS_PIN			_BIT( P3_OUTP_SET_GPO_01_BIT )



#define	PORT_A_RTS_HIGH				(P3_OUTP_SET = PORT_A_RTS_PIN)
#define	PORT_A_RTS_LOW				(P3_OUTP_CLR = PORT_A_RTS_PIN)
#define	PORT_A_RTS_is_HIGH			((P3_OUTP_STATE & PORT_A_RTS_PIN) == PORT_A_RTS_PIN)
#define	PORT_A_RTS_is_LOW			((P3_OUTP_STATE & PORT_A_RTS_PIN) == 0)

// ----------

#define	PORT_B_ENABLE_HIGH			(P3_OUTP_SET = PORT_B_ENABLE_PIN)
#define	PORT_B_ENABLE_LOW			(P3_OUTP_CLR = PORT_B_ENABLE_PIN)
#define	PORT_B_ENABLE_is_HIGH		((P3_OUTP_STATE & PORT_B_ENABLE_PIN) == PORT_B_ENABLE_PIN)
#define	PORT_B_ENABLE_is_LOW		((P3_OUTP_STATE & PORT_B_ENABLE_PIN) == 0)

// ----------

#define	PORT_A_ENABLE_HIGH			(P3_OUTP_SET = PORT_A_ENABLE_PIN)
#define	PORT_A_ENABLE_LOW			(P3_OUTP_CLR = PORT_A_ENABLE_PIN)
#define	PORT_A_ENABLE_is_HIGH		((P3_OUTP_STATE & PORT_A_ENABLE_PIN) == PORT_A_ENABLE_PIN)
#define	PORT_A_ENABLE_is_LOW		((P3_OUTP_STATE & PORT_A_ENABLE_PIN) == 0)

// ----------

#define	PORT_A_DTR_HIGH				(P3_OUTP_SET = PORT_A_DTR_PIN)
#define	PORT_A_DTR_LOW				(P3_OUTP_CLR = PORT_A_DTR_PIN)
#define	PORT_A_DTR_is_HIGH			((P3_OUTP_STATE & PORT_A_DTR_PIN) == PORT_A_DTR_PIN)
#define	PORT_A_DTR_is_LOW			((P3_OUTP_STATE & PORT_A_DTR_PIN) == 0)

#define	BACKLIGHT_ON				(P3_OUTP_SET = BACKLIGHT_ENABLE_PIN)
#define	BACKLIGHT_OFF				(P3_OUTP_CLR = BACKLIGHT_ENABLE_PIN)
#define	BACKLIGHT_is_ON				((P3_OUTP_STATE & BACKLIGHT_ENABLE_PIN) == BACKLIGHT_ENABLE_PIN)
#define	BACKLIGHT_is_OFF			((P3_OUTP_STATE & BACKLIGHT_ENABLE_PIN) == 0)

#define	PORT_RRE_CONFIG_HIGH		(P3_OUTP_SET = PORT_RRE_CONFIG_PIN)
#define	PORT_RRE_CONFIG_LOW			(P3_OUTP_CLR = PORT_RRE_CONFIG_PIN)
#define	PORT_RRE_CONFIG_is_HIGH		((P3_OUTP_STATE & PORT_RRE_CONFIG_PIN) == PORT_RRE_CONFIG_PIN)
#define	PORT_RRE_CONFIG_is_LOW		((P3_OUTP_STATE & PORT_RRE_CONFIG_PIN) == 0)

#define	PORT_RRE_SLEEP_HIGH			(P3_OUTP_SET = PORT_RRE_SLEEP_PIN)
#define	PORT_RRE_SLEEP_LOW			(P3_OUTP_CLR = PORT_RRE_SLEEP_PIN)
#define	PORT_RRE_SLEEP_is_HIGH		((P3_OUTP_STATE & PORT_RRE_SLEEP_PIN) == PORT_RRE_SLEEP_PIN)
#define	PORT_RRE_SLEEP_is_LOW		((P3_OUTP_STATE & PORT_RRE_SLEEP_PIN) == 0)

#define	PORT_RRE_RESET_HIGH			(P3_OUTP_SET = PORT_RRE_RESET_PIN)
#define	PORT_RRE_RESET_LOW			(P3_OUTP_CLR = PORT_RRE_RESET_PIN)
#define	PORT_RRE_RESET_is_HIGH		((P3_OUTP_STATE & PORT_RRE_RESET_PIN) == PORT_RRE_RESET_PIN)
#define	PORT_RRE_RESET_is_LOW		((P3_OUTP_STATE & PORT_RRE_RESET_PIN) == 0)

#define	PORT_B_RESET_HIGH			(P3_OUTP_SET = PORT_B_RESET_PIN)
#define	PORT_B_RESET_LOW			(P3_OUTP_CLR = PORT_B_RESET_PIN)
#define	PORT_B_RESET_is_HIGH		((P3_OUTP_STATE & PORT_B_RESET_PIN) == PORT_B_RESET_PIN)
#define	PORT_B_RESET_is_LOW			((P3_OUTP_STATE & PORT_B_RESET_PIN) == 0)

#define	PORT_B_DTR_HIGH				(P3_OUTP_SET = PORT_B_DTR_PIN)
#define	PORT_B_DTR_LOW				(P3_OUTP_CLR = PORT_B_DTR_PIN)
#define	PORT_B_DTR_is_HIGH			((P3_OUTP_STATE & PORT_B_DTR_PIN) == PORT_B_DTR_PIN)
#define	PORT_B_DTR_is_LOW			((P3_OUTP_STATE & PORT_B_DTR_PIN) == 0)

#define	PORT_B_RTS_HIGH				(P3_OUTP_SET = PORT_B_RTS_PIN)
#define	PORT_B_RTS_LOW				(P3_OUTP_CLR = PORT_B_RTS_PIN)
#define	PORT_B_RTS_is_HIGH			((P3_OUTP_STATE & PORT_B_RTS_PIN) == PORT_B_RTS_PIN)
#define	PORT_B_RTS_is_LOW			((P3_OUTP_STATE & PORT_B_RTS_PIN) == 0)


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define	PORT_A_RI_PIN			P3_IN_STATE_GPI_28

#define	PORT_RRE_CD_PIN			P3_IN_STATE_GPI_23

#define	PORT_RRE_CTS_PIN		P3_IN_STATE_GPI_22

#define	EPSON_FOUT				P3_IN_STATE_GPI_19

#define	PORT_A_CTS_PIN			P3_IN_STATE_GPI_16

#define	EPSON_INT_B				P3_IN_STATE_GPI_07

// 1/19/2017 rmd : This input can be setup to fire an INTERRUPT. And is available. It is no
// longer used as of the the REV J hardware. Well actually it was provisioned for USB but
// was NEVER used!
#define	nlu_USB_DETECT_PIN			P3_IN_STATE_GPI_06

#define	PORT_A_CD_PIN			P3_IN_STATE_GPI_05

#define	EPSON_INT_A				P3_IN_STATE_GPI_04

#define	PORT_B_RI_PIN			P3_IN_STATE_GPI_03

#define	PORT_B_CD_PIN			P3_IN_STATE_GPI_02

#define	POWER_FAIL_INT			P3_IN_STATE_GPI_01

#define	PORT_B_CTS_PIN			P3_IN_STATE_GPI_00


#define	PORT_A_CTS_is_HIGH		((P3_INP_STATE & PORT_A_CTS_PIN) == PORT_A_CTS_PIN)
#define	PORT_A_CTS_is_LOW		((P3_INP_STATE & PORT_A_CTS_PIN) == 0)

#define	PORT_A_CD_is_HIGH		((P3_INP_STATE & PORT_A_CD_PIN) == PORT_A_CD_PIN)
#define	PORT_A_CD_is_LOW		((P3_INP_STATE & PORT_A_CD_PIN) == 0)

#define	PORT_A_RI_is_HIGH		((P3_INP_STATE & PORT_A_RI_PIN) == PORT_A_RI_PIN)
#define	PORT_A_RI_is_LOW		((P3_INP_STATE & PORT_A_RI_PIN) == 0)

#define	PORT_B_CTS_is_HIGH		((P3_INP_STATE & PORT_B_CTS_PIN) == PORT_B_CTS_PIN)
#define	PORT_B_CTS_is_LOW		((P3_INP_STATE & PORT_B_CTS_PIN) == 0)

#define	PORT_B_CD_is_HIGH		((P3_INP_STATE & PORT_B_CD_PIN) == PORT_B_CD_PIN)
#define	PORT_B_CD_is_LOW		((P3_INP_STATE & PORT_B_CD_PIN) == 0)

#define	PORT_B_RI_is_HIGH		((P3_INP_STATE & PORT_B_RI_PIN) == PORT_B_RI_PIN)
#define	PORT_B_RI_is_LOW		((P3_INP_STATE & PORT_B_RI_PIN) == 0)

#define	PORT_RRE_CTS_is_HIGH	((P3_INP_STATE & PORT_RRE_CTS_PIN) == PORT_RRE_CTS_PIN)
#define	PORT_RRE_CTS_is_LOW		((P3_INP_STATE & PORT_RRE_CTS_PIN) == 0)

#define	PORT_RRE_CD_is_HIGH		((P3_INP_STATE & PORT_RRE_CD_PIN) == PORT_RRE_CD_PIN)
#define	PORT_RRE_CD_is_LOW		((P3_INP_STATE & PORT_RRE_CD_PIN) == 0)

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */


#define CS_LED_RED_ON		(P0_OUTP_SET = CS_LED_RED)
#define CS_LED_RED_OFF		(P0_OUTP_CLR = CS_LED_RED)

#define CS_LED_YELLOW_ON	(P0_OUTP_SET = CS_LED_YELLOW)
#define CS_LED_YELLOW_OFF	(P0_OUTP_CLR = CS_LED_YELLOW)

#define CS_LED_GREEN_ON		(P0_OUTP_SET = CS_LED_GREEN)
#define CS_LED_GREEN_OFF	(P0_OUTP_CLR = CS_LED_GREEN)





/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

void gpio_setup( void );

void GPIO_SETUP_learn_and_set_board_hardware_id( void );

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

