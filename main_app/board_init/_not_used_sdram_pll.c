// Copyright (c) 2009 Rowley Associates Limited.
//
// This file may be distributed under the terms of the License Agreement
// provided with this software.
//
// THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/*

	This file is the Rowley version of the SDRAM intiialization code for the Phytec LPC3250 board. NXP also
	provided a method for doing this. The NXP method is more complete feeling and more explicit. And more flexible
	being that it can be adopted easily for different SDRAM parts.

	This method however does appear to work. It is very direct and to the point.

*/
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"sdram_pll.h"

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// The Calsense board and the Phytec board both run at this.
#define HCLK_104MHz

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#if !defined(HCLK_104MHz) && !defined(HCLK_13MHz)
	#error HCLK_104MHz or HCLK_13Mhz must be defined
#endif

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
__attribute__ ((section (".init"))) void clocks_init( void )
{  
  HCLKDIV_CTRL = (15<<2)|(1<<0); // PERIPH_CLK is ARM PLL Clock/16; HCLK is ARM PLL Clock/2
  HCLKPLL_CTRL = (1<<16)|(1<<14)|(15<<1); // Enable|ByPass Post Divider|Feedback Divider clock by PLL_CLKOUT|M==16
  while(!(HCLKPLL_CTRL&1)); // Wait for PLL to Lock
  PWR_CTRL = 0x00000016; //  Normal Run mode
}

/* ---------------------------------------------------------- */
__attribute__ ((section (".init"))) void sdram_delay( volatile unsigned int delay )
{
   while ( delay )
   {
      delay--;
   }
}

/* ---------------------------------------------------------- */
// This sdram code is supplied with the Nohau board 
__attribute__ ((section (".init"))) void sdram_init( void )
{
   SDRAMCLK_CTRL         = 0x0001C000; // HCLKDELAY_DELAY = 7
   EMCControl           = 0x00000001; // SDRAM enabled
   EMCConfig            = 0x00000000; // Little endian mode
   EMCAHBControl0       = 0x00000001; // Enable
   //EMCAHBControl2       = 0x00000001; // Enable
   EMCAHBControl3       = 0x00000001; // Enable
   EMCAHBControl4       = 0x00000001; // Enable
   EMCDynamicConfig0    = 0x00005682; // 32-bit low-power SDRAM 128MB(4M*32) | low power SDR SDRAM

#ifdef HCLK_13MHz
   EMCDynamicRasCas0    = 0x00000301;
#endif
#ifdef HCLK_104MHz
   EMCDynamicRasCas0    = 0x00000302;
#endif

   EMCDynamicReadConfig = 0x00000011;

#ifdef HCLK_13MHz
   EMCDynamictRP        = 0x00000000;
#endif
#ifdef HCLK_104MHz
   EMCDynamictRP        = 0x00000001;
#endif

#ifdef HCLK_13MHz
   EMCDynamictRAS       = 0x00000000;
#endif
#ifdef HCLK_104MHz
   EMCDynamictRAS       = 0x00000004;
#endif

#ifdef HCLK_13MHz
   EMCDynamictSREX      = 0x00000001;
#endif
#ifdef HCLK_104MHz
   EMCDynamictSREX      = 0x00000008;
#endif

   EMCDynamictWR        = 0x00000001;

#ifdef HCLK_13MHz
   EMCDynamictRC        = 0x00000001;
#endif
#ifdef HCLK_104MHz
   EMCDynamictRC        = 0x00000007;
#endif

#ifdef HCLK_13MHz
   EMCDynamictRFC       = 0x00000001;
#endif
#ifdef HCLK_104MHz
   EMCDynamictRFC       = 0x00000008;
#endif

#ifdef HCLK_13MHz
   EMCDynamictXSR       = 0x00000001;
#endif
#ifdef HCLK_104MHz
   EMCDynamictXSR       = 0x00000008;
#endif

#ifdef HCLK_13MHz
   EMCDynamictRRD       = 0x00000000;
#endif
#ifdef HCLK_104MHz
   EMCDynamictRRD       = 0x00000002;
#endif

   EMCDynamictMRD       = 0x00000002;
   EMCDynamictCDLR      = 0x00000001;
   EMCDynamicControl    = 0x00000183; // SDRAM NOP|IMMC|CS|CE
   sdram_delay(100);
   EMCDynamicControl    = 0x00000112; // SDRAM PALL|IMMC|CS|CE
   EMCDynamicRefresh    = 0x00000002; // CS
   sdram_delay(100);

#ifdef HCLK_13MHz
   EMCDynamicRefresh    = 0x0000000C;
#endif
#ifdef HCLK_104MHz
   EMCDynamicRefresh    = 0x00000032;
#endif

//#ifdef HCLK_13MHz
//   MPMCDynamicRasCas0    = 0x00000301;
//#endif
//#ifdef HCLK_104MHz
//   MPMCDynamicRasCas0    = 0x00000303;
//#endif

//   MPMCDynamicConfig0    = 0x00005482;
   EMCDynamicControl    = 0x00000083; // SDRAM NORMAL|IMMC|CS|CE
   *(volatile unsigned int *)(0x80018000);
   EMCDynamicControl    = 0x00000083; // SDRAM NORMAL|IMMC|CS|CE
   *(volatile unsigned int *)(0x8102C000);
   EMCDynamicControl    = 0x00000003; // SDRAM NORMAL|IMMC
   EMCAHBTimeOut0       = 0x00000064;
   //EMCAHBTimeOut2       = 0x00000190;
   EMCAHBTimeOut3       = 0x00000190;
   EMCAHBTimeOut4       = 0x00000190;
}

/* ---------------------------------------------------------- */
__attribute__ ((section (".init"))) void init_sdram_pll( void )
{
	sdram_init();
    clocks_init();
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

