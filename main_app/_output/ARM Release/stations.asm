	.file	"stations.c"
	.text
.Ltext0:
	.section	.text.nm_STATION_set_box_index,"ax",%progbits
	.align	2
	.type	nm_STATION_set_box_index, %function
nm_STATION_set_box_index:
.LFB4:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	sub	sp, sp, #44
.LCFI1:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L2
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #156
	str	r3, [sp, #32]
	mov	r3, #4
	str	r3, [sp, #36]
	ldr	r3, .L2+4
	mov	ip, #0
	mov	r1, #11
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #84
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_station
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L3:
	.align	2
.L2:
	.word	53266
	.word	.LC0
.LFE4:
	.size	nm_STATION_set_box_index, .-nm_STATION_set_box_index
	.section	.text.nm_STATION_set_station_number,"ax",%progbits
	.align	2
	.type	nm_STATION_set_station_number, %function
nm_STATION_set_station_number:
.LFB3:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	sub	sp, sp, #44
.LCFI3:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L5
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #156
	str	r3, [sp, #32]
	mov	r3, #3
	str	r3, [sp, #36]
	ldr	r3, .L5+4
	mov	ip, #0
	mov	r1, #175
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #80
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_station
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L6:
	.align	2
.L5:
	.word	53265
	.word	.LC1
.LFE3:
	.size	nm_STATION_set_station_number, .-nm_STATION_set_station_number
	.section	.text.nm_STATION_set_ET_factor,"ax",%progbits
	.align	2
	.type	nm_STATION_set_ET_factor, %function
nm_STATION_set_ET_factor:
.LFB10:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI4:
	sub	sp, sp, #44
.LCFI5:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #400
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #156
	mov	r1, #100
	str	r3, [sp, #32]
	mov	r3, #10
	stmib	sp, {r1, r2}
	ldr	r2, .L8
	str	r3, [sp, #36]
	ldr	r3, .L8+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #108
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_station
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L9:
	.align	2
.L8:
	.word	53252
	.word	.LC2
.LFE10:
	.size	nm_STATION_set_ET_factor, .-nm_STATION_set_ET_factor
	.section	.text.nm_STATION_set_distribution_uniformity,"ax",%progbits
	.align	2
	.type	nm_STATION_set_distribution_uniformity, %function
nm_STATION_set_distribution_uniformity:
.LFB12:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI6:
	sub	sp, sp, #44
.LCFI7:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #100
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #156
	mov	r1, #70
	str	r3, [sp, #32]
	mov	r3, #12
	stmib	sp, {r1, r2}
	ldr	r2, .L11
	str	r3, [sp, #36]
	ldr	r3, .L11+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #116
	mov	r2, lr
	mov	r3, #40
	bl	SHARED_set_uint32_station
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L12:
	.align	2
.L11:
	.word	53254
	.word	.LC3
.LFE12:
	.size	nm_STATION_set_distribution_uniformity, .-nm_STATION_set_distribution_uniformity
	.section	.text.nm_STATION_set_square_footage,"ax",%progbits
	.align	2
	.global	nm_STATION_set_square_footage
	.type	nm_STATION_set_square_footage, %function
nm_STATION_set_square_footage:
.LFB18:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI8:
	sub	sp, sp, #44
.LCFI9:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	ldr	r1, .L14
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #156
	mov	r1, #1000
	str	r3, [sp, #32]
	mov	r3, #21
	stmib	sp, {r1, r2}
	ldr	r2, .L14+4
	str	r3, [sp, #36]
	ldr	r3, .L14+8
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #168
	mov	r2, lr
	mov	r3, #1
	bl	SHARED_set_uint32_station
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L15:
	.align	2
.L14:
	.word	99999
	.word	53269
	.word	.LC4
.LFE18:
	.size	nm_STATION_set_square_footage, .-nm_STATION_set_square_footage
	.section	.text.nm_STATION_set_no_water_days,"ax",%progbits
	.align	2
	.global	nm_STATION_set_no_water_days
	.type	nm_STATION_set_no_water_days, %function
nm_STATION_set_no_water_days:
.LFB13:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI10:
	sub	sp, sp, #44
.LCFI11:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L17
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #156
	str	r3, [sp, #32]
	mov	r3, #13
	str	r3, [sp, #36]
	ldr	r3, .L17+4
	mov	ip, #0
	mov	r1, #31
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #120
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_station
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L18:
	.align	2
.L17:
	.word	53255
	.word	.LC5
.LFE13:
	.size	nm_STATION_set_no_water_days, .-nm_STATION_set_no_water_days
	.section	.text.nm_STATION_set_total_run_minutes,"ax",%progbits
	.align	2
	.global	nm_STATION_set_total_run_minutes
	.type	nm_STATION_set_total_run_minutes, %function
nm_STATION_set_total_run_minutes:
.LFB7:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI12:
	sub	sp, sp, #44
.LCFI13:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	ldr	r1, .L20
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #156
	mov	r1, #10
	str	r3, [sp, #32]
	mov	r3, #7
	stmib	sp, {r1, r2}
	ldr	r2, .L20+4
	str	r3, [sp, #36]
	ldr	r3, .L20+8
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #96
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_station
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L21:
	.align	2
.L20:
	.word	9999
	.word	53249
	.word	.LC6
.LFE7:
	.size	nm_STATION_set_total_run_minutes, .-nm_STATION_set_total_run_minutes
	.section	.text.nm_STATION_set_decoder_output,"ax",%progbits
	.align	2
	.global	nm_STATION_set_decoder_output
	.type	nm_STATION_set_decoder_output, %function
nm_STATION_set_decoder_output:
.LFB6:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI14:
	sub	sp, sp, #44
.LCFI15:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L23
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #156
	str	r3, [sp, #32]
	mov	r3, #6
	str	r3, [sp, #36]
	ldr	r3, .L23+4
	mov	ip, #0
	mov	r1, #1
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #92
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_station
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L24:
	.align	2
.L23:
	.word	53260
	.word	.LC7
.LFE6:
	.size	nm_STATION_set_decoder_output, .-nm_STATION_set_decoder_output
	.section	.text.nm_STATION_set_decoder_serial_number,"ax",%progbits
	.align	2
	.global	nm_STATION_set_decoder_serial_number
	.type	nm_STATION_set_decoder_serial_number, %function
nm_STATION_set_decoder_serial_number:
.LFB5:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI16:
	sub	sp, sp, #44
.LCFI17:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	mov	r4, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r1, .L26
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	str	r2, [sp, #8]
	str	r3, [sp, #28]
	add	r3, r0, #156
	str	r3, [sp, #32]
	mov	r3, #5
	ldr	r2, .L26+4
	str	r3, [sp, #36]
	ldr	r3, .L26+8
	mov	ip, #0
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #88
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_station
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L27:
	.align	2
.L26:
	.word	2097151
	.word	53259
	.word	.LC8
.LFE5:
	.size	nm_STATION_set_decoder_serial_number, .-nm_STATION_set_decoder_serial_number
	.section	.text.STATION_get_GID_station_group,"ax",%progbits
	.align	2
	.global	STATION_get_GID_station_group
	.type	STATION_get_GID_station_group, %function
STATION_get_GID_station_group:
.LFB63:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI18:
	ldr	r4, .L29
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L29+4
	ldr	r3, .L29+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	STATION_GROUP_get_last_group_ID
	ldr	r1, .L29+12
	mov	r2, #0
	str	r1, [sp, #4]
	add	r1, r5, #148
	str	r1, [sp, #8]
	ldr	r1, .L29+16
	str	r2, [sp, #0]
	str	r1, [sp, #12]
	add	r1, r5, #136
	mov	r3, r0
	mov	r0, r5
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L30:
	.align	2
.L29:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2952
	.word	nm_STATION_set_GID_station_group
	.word	.LC10
.LFE63:
	.size	STATION_get_GID_station_group, .-STATION_get_GID_station_group
	.section	.text.nm_STATION_set_in_use,"ax",%progbits
	.align	2
	.global	nm_STATION_set_in_use
	.type	nm_STATION_set_in_use, %function
nm_STATION_set_in_use:
.LFB2:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI19:
	sub	sp, sp, #36
.LCFI20:
	str	r2, [sp, #0]
	mov	r2, #53248
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	mov	lr, r1
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	add	r1, r0, #76
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	r2, lr
	str	r3, [sp, #20]
	add	r3, r0, #156
	str	r3, [sp, #24]
	mov	r3, #2
	str	r3, [sp, #28]
	ldr	r3, .L32
	str	r3, [sp, #32]
	mov	r3, #1
	bl	SHARED_set_bool_station
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L33:
	.align	2
.L32:
	.word	.LC11
.LFE2:
	.size	nm_STATION_set_in_use, .-nm_STATION_set_in_use
	.section	.text.nm_STATION_set_physically_available,"ax",%progbits
	.align	2
	.global	nm_STATION_set_physically_available
	.type	nm_STATION_set_physically_available, %function
nm_STATION_set_physically_available:
.LFB1:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI21:
	sub	sp, sp, #36
.LCFI22:
	str	r2, [sp, #0]
	ldr	r2, .L35
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #72
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #156
	str	r3, [sp, #24]
	mov	r3, #1
	str	r3, [sp, #28]
	ldr	r3, .L35+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_station
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L36:
	.align	2
.L35:
	.word	53261
	.word	.LC12
.LFE1:
	.size	nm_STATION_set_physically_available, .-nm_STATION_set_physically_available
	.section	.text.nm_STATION_set_moisture_balance_percent,"ax",%progbits
	.align	2
	.global	nm_STATION_set_moisture_balance_percent
	.type	nm_STATION_set_moisture_balance_percent, %function
nm_STATION_set_moisture_balance_percent:
.LFB17:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI23:
	sub	sp, sp, #44
.LCFI24:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	ldr	r2, .L38
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	mov	lr, r1	@ float
	str	r3, [sp, #28]
	add	r3, r0, #156
	str	r3, [sp, #32]
	mov	r3, #20
	str	r3, [sp, #36]
	ldr	r3, .L38+4
	mov	r1, #1065353216
	str	r1, [sp, #0]	@ float
	mov	r1, #1056964608
	str	r1, [sp, #4]	@ float
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #160
	mov	r2, lr	@ float
	ldr	r3, .L38+8
	bl	SHARED_set_float_station
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L39:
	.align	2
.L38:
	.word	53268
	.word	.LC13
	.word	-1027080192
.LFE17:
	.size	nm_STATION_set_moisture_balance_percent, .-nm_STATION_set_moisture_balance_percent
	.section	.text.nm_STATION_set_GID_manual_program_B,"ax",%progbits
	.align	2
	.global	nm_STATION_set_GID_manual_program_B
	.type	nm_STATION_set_GID_manual_program_B, %function
nm_STATION_set_GID_manual_program_B:
.LFB16:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI25:
	mov	r5, r1
	sub	sp, sp, #28
.LCFI26:
	mov	r1, r0
	ldr	r7, [r1, #144]!
	mov	r4, r3
	str	r3, [sp, #0]
	ldr	r3, [sp, #56]
	mov	r8, r2
	str	r3, [sp, #4]
	ldr	r3, [sp, #60]
	mov	r2, r5
	str	r3, [sp, #8]
	ldr	r3, [sp, #64]
	mov	r6, r0
	str	r3, [sp, #12]
	add	r3, r0, #156
	str	r3, [sp, #16]
	mov	r3, #19
	str	r3, [sp, #20]
	ldr	r3, .L44
	str	r3, [sp, #24]
	mvn	r3, #0
	bl	SHARED_set_GID_station
	cmp	r0, #1
	bne	.L40
	cmp	r8, #0
	beq	.L40
	cmp	r5, #0
	ldr	sl, [r6, #84]
	ldr	r8, [r6, #80]
	beq	.L42
	cmp	r7, #0
	beq	.L43
	mov	r0, r7
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r6, r0
	mov	r0, r5
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r1, r8
	mov	r2, r6
	str	r4, [sp, #56]
	mov	r3, r0
	mov	r0, sl
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_station_moved_from_one_group_to_another_idx
.L43:
	mov	r0, r5
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r1, r8
	mov	r3, r4
	mov	r2, r0
	mov	r0, sl
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_station_added_to_group_idx
.L42:
	mov	r0, r7
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r1, r8
	mov	r3, r4
	mov	r2, r0
	mov	r0, sl
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_station_removed_from_group_idx
.L40:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L45:
	.align	2
.L44:
	.word	.LC14
.LFE16:
	.size	nm_STATION_set_GID_manual_program_B, .-nm_STATION_set_GID_manual_program_B
	.section	.text.nm_STATION_set_GID_manual_program_A,"ax",%progbits
	.align	2
	.global	nm_STATION_set_GID_manual_program_A
	.type	nm_STATION_set_GID_manual_program_A, %function
nm_STATION_set_GID_manual_program_A:
.LFB15:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI27:
	mov	r5, r1
	sub	sp, sp, #28
.LCFI28:
	mov	r1, r0
	ldr	r7, [r1, #140]!
	mov	r4, r3
	str	r3, [sp, #0]
	ldr	r3, [sp, #56]
	mov	r8, r2
	str	r3, [sp, #4]
	ldr	r3, [sp, #60]
	mov	r2, r5
	str	r3, [sp, #8]
	ldr	r3, [sp, #64]
	mov	r6, r0
	str	r3, [sp, #12]
	add	r3, r0, #156
	str	r3, [sp, #16]
	mov	r3, #18
	str	r3, [sp, #20]
	ldr	r3, .L50
	str	r3, [sp, #24]
	mvn	r3, #0
	bl	SHARED_set_GID_station
	cmp	r0, #1
	bne	.L46
	cmp	r8, #0
	beq	.L46
	cmp	r5, #0
	ldr	sl, [r6, #84]
	ldr	r8, [r6, #80]
	beq	.L48
	cmp	r7, #0
	beq	.L49
	mov	r0, r7
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r6, r0
	mov	r0, r5
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r1, r8
	mov	r2, r6
	str	r4, [sp, #56]
	mov	r3, r0
	mov	r0, sl
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_station_moved_from_one_group_to_another_idx
.L49:
	mov	r0, r5
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r1, r8
	mov	r3, r4
	mov	r2, r0
	mov	r0, sl
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_station_added_to_group_idx
.L48:
	mov	r0, r7
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r1, r8
	mov	r3, r4
	mov	r2, r0
	mov	r0, sl
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_station_removed_from_group_idx
.L46:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L51:
	.align	2
.L50:
	.word	.LC15
.LFE15:
	.size	nm_STATION_set_GID_manual_program_A, .-nm_STATION_set_GID_manual_program_A
	.section	.text.nm_STATION_set_GID_station_group,"ax",%progbits
	.align	2
	.global	nm_STATION_set_GID_station_group
	.type	nm_STATION_set_GID_station_group, %function
nm_STATION_set_GID_station_group:
.LFB14:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI29:
	mov	r5, r1
	sub	sp, sp, #28
.LCFI30:
	mov	r1, r0
	ldr	r7, [r1, #136]!
	mov	r4, r3
	str	r3, [sp, #0]
	ldr	r3, [sp, #56]
	mov	r8, r2
	str	r3, [sp, #4]
	ldr	r3, [sp, #60]
	mov	r2, r5
	str	r3, [sp, #8]
	ldr	r3, [sp, #64]
	mov	r6, r0
	str	r3, [sp, #12]
	add	r3, r0, #156
	str	r3, [sp, #16]
	mov	r3, #17
	str	r3, [sp, #20]
	ldr	r3, .L56
	str	r3, [sp, #24]
	mvn	r3, #0
	bl	SHARED_set_GID_station
	cmp	r0, #1
	bne	.L52
	cmp	r8, #0
	beq	.L52
	cmp	r5, #0
	ldr	sl, [r6, #84]
	ldr	r8, [r6, #80]
	beq	.L54
	cmp	r7, #0
	beq	.L55
	mov	r0, r7
	bl	STATION_GROUP_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r6, r0
	mov	r0, r5
	bl	STATION_GROUP_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r1, r8
	mov	r2, r6
	str	r4, [sp, #56]
	mov	r3, r0
	mov	r0, sl
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_station_moved_from_one_group_to_another_idx
.L55:
	mov	r0, r5
	bl	STATION_GROUP_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r1, r8
	mov	r3, r4
	mov	r2, r0
	mov	r0, sl
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_station_added_to_group_idx
.L54:
	mov	r0, r7
	bl	STATION_GROUP_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r1, r8
	mov	r3, r4
	mov	r2, r0
	mov	r0, sl
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_station_removed_from_group_idx
.L52:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L57:
	.align	2
.L56:
	.word	.LC10
.LFE14:
	.size	nm_STATION_set_GID_station_group, .-nm_STATION_set_GID_station_group
	.section	.text.nm_STATION_set_expected_flow_rate,"ax",%progbits
	.align	2
	.global	nm_STATION_set_expected_flow_rate
	.type	nm_STATION_set_expected_flow_rate, %function
nm_STATION_set_expected_flow_rate:
.LFB11:
	@ args = 12, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI31:
	sub	sp, sp, #48
.LCFI32:
	str	r2, [sp, #8]
	ldr	r2, .L61
	mov	ip, r1
	str	r2, [sp, #12]
	ldr	r2, [sp, #60]
	mov	r5, r3
	str	r2, [sp, #20]
	ldr	r2, [sp, #64]
	mov	r3, #4000
	str	r2, [sp, #24]
	ldr	r2, [sp, #68]
	str	r3, [sp, #0]
	str	r2, [sp, #28]
	add	r2, r0, #156
	str	r2, [sp, #32]
	mov	r2, #11
	str	r2, [sp, #36]
	ldr	r2, .L61+4
	mov	r3, #1
	str	r2, [sp, #40]
	add	r1, r0, #112
	mov	r2, ip
	mov	r4, r0
	str	r3, [sp, #4]
	str	r5, [sp, #16]
	bl	SHARED_set_uint32_station
	cmp	r0, #1
	bne	.L58
	cmp	r5, #11
	beq	.L58
.LBB18:
	ldr	r5, .L61+8
	mov	r1, #400
	ldr	r2, .L61+12
	ldr	r3, .L61+16
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	add	r1, sp, #44
	bl	STATION_PRESERVES_get_index_using_ptr_to_station_struct
	cmp	r0, #1
	bne	.L60
	ldr	r2, [sp, #44]
	ldr	r3, .L61+20
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #140]	@ zero_extendqisi2
	bic	r2, r2, #15
	strb	r2, [r3, #140]
.L60:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
.L58:
.LBE18:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, pc}
.L62:
	.align	2
.L61:
	.word	53253
	.word	.LC16
	.word	station_preserves_recursive_MUTEX
	.word	.LC17
	.word	1107
	.word	station_preserves
.LFE11:
	.size	nm_STATION_set_expected_flow_rate, .-nm_STATION_set_expected_flow_rate
	.section	.text.nm_STATION_set_ignore_moisture_balance_at_next_irrigation,"ax",%progbits
	.align	2
	.global	nm_STATION_set_ignore_moisture_balance_at_next_irrigation
	.type	nm_STATION_set_ignore_moisture_balance_at_next_irrigation, %function
nm_STATION_set_ignore_moisture_balance_at_next_irrigation:
.LFB19:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI33:
	sub	sp, sp, #36
.LCFI34:
	str	r2, [sp, #0]
	ldr	r2, .L64
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #172
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #156
	str	r3, [sp, #24]
	mov	r3, #22
	str	r3, [sp, #28]
	ldr	r3, .L64+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_station
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L65:
	.align	2
.L64:
	.word	53270
	.word	.LC18
.LFE19:
	.size	nm_STATION_set_ignore_moisture_balance_at_next_irrigation, .-nm_STATION_set_ignore_moisture_balance_at_next_irrigation
	.section	.text.init_file_station_info,"ax",%progbits
	.align	2
	.global	init_file_station_info
	.type	init_file_station_info, %function
init_file_station_info:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI35:
	ldr	r3, .L67
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, .L67+4
	ldr	r1, .L67+8
	ldr	r3, [r3, #0]
	mov	r2, #7
	str	r3, [sp, #4]
	ldr	r3, .L67+12
	str	r3, [sp, #8]
	ldr	r3, .L67+16
	str	r3, [sp, #12]
	ldr	r3, .L67+20
	str	r3, [sp, #16]
	mov	r3, #9
	str	r3, [sp, #20]
	ldr	r3, .L67+24
	bl	FLASH_FILE_initialize_list_and_find_or_create_group_list_file
	add	sp, sp, #24
	ldmfd	sp!, {pc}
.L68:
	.align	2
.L67:
	.word	.LANCHOR2
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR0
	.word	nm_station_structure_updater
	.word	nm_STATION_set_default_values_resulting_in_station_0_for_this_box
	.word	.LANCHOR3
	.word	.LANCHOR1
.LFE23:
	.size	init_file_station_info, .-init_file_station_info
	.section	.text.save_file_station_info,"ax",%progbits
	.align	2
	.global	save_file_station_info
	.type	save_file_station_info, %function
save_file_station_info:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #180
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI36:
	ldr	r1, .L70
	str	r3, [sp, #0]
	ldr	r3, .L70+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, #7
	str	r3, [sp, #4]
	mov	r3, #9
	str	r3, [sp, #8]
	ldr	r3, .L70+8
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L71:
	.align	2
.L70:
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR1
.LFE24:
	.size	save_file_station_info, .-save_file_station_info
	.section	.text.nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar,"ax",%progbits
	.align	2
	.global	nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar
	.type	nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar, %function
nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar:
.LFB28:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI37:
	sub	sp, sp, #28
.LCFI38:
	add	r5, sp, #8
	mov	r4, r0
	mov	r0, r5
	bl	EPSON_obtain_latest_complete_time_and_date
	ldr	r0, [r4, #136]
	cmp	r0, #0
	beq	.L74
	bl	STATION_GROUP_get_group_with_this_GID
.L74:
	ldr	r3, .L75
	str	r5, [sp, #0]
	ldr	r3, [r3, #0]
	mov	r2, r0
	fmsr	s14, r3	@ int
	mov	r0, #0
	mov	r1, r4
	mov	r3, r0
	fuitos	s15, s14
	fsts	s15, [sp, #4]
	bl	nm_WATERSENSE_calculate_run_minutes_for_this_station
	ldr	r3, .L75+4
	ldrh	r1, [sp, #12]
	ldr	r6, [r3, #0]
	add	r6, r6, #100
	mov	r5, r0	@ float
	mov	r0, r4
	bl	PERCENT_ADJUST_get_station_percentage_100u
	mov	r1, r4
	mov	r3, r5	@ float
	str	r6, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, #0
	mov	r2, r0
	bl	nm_WATERSENSE_calculate_adjusted_run_time
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, pc}
.L76:
	.align	2
.L75:
	.word	GuiVar_StationInfoDU
	.word	GuiVar_StationInfoETFactor
.LFE28:
	.size	nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar, .-nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar
	.section	.text.STATION_copy_group_name_into_copy_station_GuiVar_and_set_offsets,"ax",%progbits
	.align	2
	.global	STATION_copy_group_name_into_copy_station_GuiVar_and_set_offsets
	.type	STATION_copy_group_name_into_copy_station_GuiVar_and_set_offsets, %function
STATION_copy_group_name_into_copy_station_GuiVar_and_set_offsets:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI39:
	ldr	r4, .L78
	mov	r1, r0
	mov	r2, #28
	mov	r0, r4
	bl	strlcpy
	ldr	r3, .L78+4
	mov	r0, r4
	ldr	r1, [r3, #4]
	mov	r2, #2
	bl	GuiLib_GetTextWidth
	ldr	r3, .L78+8
	mov	r0, r0, asl #15
	mov	r0, r0, lsr #16
	str	r0, [r3, #0]
	ldr	r3, .L78+12
	rsb	r0, r0, #0
	str	r0, [r3, #0]
	ldmfd	sp!, {r4, pc}
.L79:
	.align	2
.L78:
	.word	GuiVar_StationCopyGroupName
	.word	GuiFont_FontList
	.word	GuiVar_StationCopyTextOffset_Right
	.word	GuiVar_StationCopyTextOffset_Left
.LFE31:
	.size	STATION_copy_group_name_into_copy_station_GuiVar_and_set_offsets, .-STATION_copy_group_name_into_copy_station_GuiVar_and_set_offsets
	.section	.text.STATION_this_group_has_no_stations_assigned_to_it,"ax",%progbits
	.align	2
	.global	STATION_this_group_has_no_stations_assigned_to_it
	.type	STATION_this_group_has_no_stations_assigned_to_it, %function
STATION_this_group_has_no_stations_assigned_to_it:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI40:
	subs	r4, r1, #0
	mov	r5, r0
	beq	.L81
	ldr	r3, .L89
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L89+4
	ldr	r3, .L89+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L89+12
	bl	nm_ListGetFirst
	subs	r7, r0, #0
	moveq	r6, r7
	beq	.L82
	mov	r6, #1
.L84:
	mov	r0, r7
	blx	r4
	mov	r1, r7
	cmp	r0, r5
	ldr	r0, .L89+12
	moveq	r6, #0
	bl	nm_ListGetNext
	subs	r7, r0, #0
	bne	.L84
.L82:
	ldr	r3, .L89
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L85
.L81:
	ldr	r0, .L89+4
	ldr	r1, .L89+16
	bl	Alert_func_call_with_null_ptr_with_filename
	mov	r6, r4
.L85:
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L90:
	.align	2
.L89:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	1663
	.word	.LANCHOR1
	.word	1687
.LFE33:
	.size	STATION_this_group_has_no_stations_assigned_to_it, .-STATION_this_group_has_no_stations_assigned_to_it
	.section	.text.STATION_get_index_using_ptr_to_station_struct,"ax",%progbits
	.align	2
	.global	STATION_get_index_using_ptr_to_station_struct
	.type	STATION_get_index_using_ptr_to_station_struct, %function
STATION_get_index_using_ptr_to_station_struct:
.LFB38:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L96
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI41:
	ldr	r2, .L96+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L96+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L96+12
	bl	nm_ListGetFirst
	mov	r5, #0
	ldr	r6, .L96+12
	mov	r1, r0
	b	.L92
.L94:
	cmp	r1, r4
	beq	.L93
	ldr	r0, .L96+12
	bl	nm_ListGetNext
	add	r5, r5, #1
	mov	r1, r0
.L92:
	ldr	r3, [r6, #8]
	cmp	r5, r3
	bcc	.L94
	mov	r5, #0
.L93:
	ldr	r3, .L96
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L97:
	.align	2
.L96:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2003
	.word	.LANCHOR1
.LFE38:
	.size	STATION_get_index_using_ptr_to_station_struct, .-STATION_get_index_using_ptr_to_station_struct
	.section	.text.STATION_get_num_stations_assigned_to_groups,"ax",%progbits
	.align	2
	.global	STATION_get_num_stations_assigned_to_groups
	.type	STATION_get_num_stations_assigned_to_groups, %function
STATION_get_num_stations_assigned_to_groups:
.LFB41:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L101
	stmfd	sp!, {r4, lr}
.LCFI42:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L101+4
	ldr	r3, .L101+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L101+12
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L99
.L100:
	ldr	r3, [r1, #136]
	ldr	r0, .L101+12
	cmp	r3, #0
	addne	r4, r4, #1
	bl	nm_ListGetNext
	mov	r1, r0
.L99:
	cmp	r1, #0
	bne	.L100
	ldr	r3, .L101
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L102:
	.align	2
.L101:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2090
	.word	.LANCHOR1
.LFE41:
	.size	STATION_get_num_stations_assigned_to_groups, .-STATION_get_num_stations_assigned_to_groups
	.section	.text.STATION_get_unique_box_index_count,"ax",%progbits
	.align	2
	.global	STATION_get_unique_box_index_count
	.type	STATION_get_unique_box_index_count, %function
STATION_get_unique_box_index_count:
.LFB43:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L109
	stmfd	sp!, {r4, r5, lr}
.LCFI43:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L109+4
	ldr	r3, .L109+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L109+12
	bl	nm_ListGetFirst
	subs	r1, r0, #0
	moveq	r4, r1
	beq	.L104
	mov	r5, #12
	mov	r4, #0
.L106:
	ldr	r3, [r1, #72]
	cmp	r3, #0
	beq	.L105
	ldr	r3, [r1, #76]
	cmp	r3, #0
	beq	.L105
	ldr	r3, [r1, #84]
	cmp	r3, r5
	addne	r4, r4, #1
	movne	r5, r3
.L105:
	ldr	r0, .L109+12
	bl	nm_ListGetNext
	subs	r1, r0, #0
	bne	.L106
.L104:
	ldr	r3, .L109
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.L110:
	.align	2
.L109:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2167
	.word	.LANCHOR1
.LFE43:
	.size	STATION_get_unique_box_index_count, .-STATION_get_unique_box_index_count
	.section	.text.nm_STATION_get_physically_available,"ax",%progbits
	.align	2
	.global	nm_STATION_get_physically_available
	.type	nm_STATION_get_physically_available, %function
nm_STATION_get_physically_available:
.LFB45:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	add	r3, r0, #148
	stmfd	sp!, {r0, r1, lr}
.LCFI44:
	str	r3, [sp, #0]
	ldr	r3, .L112
	add	r1, r0, #72
	str	r3, [sp, #4]
	mov	r2, #0
	ldr	r3, .L112+4
	bl	SHARED_get_bool_32_bit_change_bits_group
	ldmfd	sp!, {r2, r3, pc}
.L113:
	.align	2
.L112:
	.word	.LC12
	.word	nm_STATION_set_physically_available
.LFE45:
	.size	nm_STATION_get_physically_available, .-nm_STATION_get_physically_available
	.section	.text.STATION_station_is_available_for_use,"ax",%progbits
	.align	2
	.global	STATION_station_is_available_for_use
	.type	STATION_station_is_available_for_use, %function
STATION_station_is_available_for_use:
.LFB44:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI45:
	ldr	r5, .L118
	mov	r4, r0
	mov	r1, #400
	ldr	r2, .L118+4
	ldr	r3, .L118+8
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r4, #148
	str	r3, [sp, #0]
	ldr	r3, .L118+12
	mov	r0, r4
	str	r3, [sp, #4]
	add	r1, r4, #76
	mov	r2, #1
	ldr	r3, .L118+16
	bl	SHARED_get_bool_32_bit_change_bits_group
	cmp	r0, #0
	moveq	r4, r0
	beq	.L115
	mov	r0, r4
	bl	nm_STATION_get_physically_available
	sub	r0, r0, #1
	rsbs	r4, r0, #0
	adc	r4, r4, r0
.L115:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L119:
	.align	2
.L118:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2234
	.word	.LC11
	.word	nm_STATION_set_in_use
.LFE44:
	.size	STATION_station_is_available_for_use, .-STATION_station_is_available_for_use
	.section	.text.STATION_get_first_available_station,"ax",%progbits
	.align	2
	.global	STATION_get_first_available_station
	.type	STATION_get_first_available_station, %function
STATION_get_first_available_station:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L128
	stmfd	sp!, {r4, lr}
.LCFI46:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L128+4
	ldr	r3, .L128+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L128+12
	bl	nm_ListGetFirst
.L127:
	subs	r4, r0, #0
	beq	.L121
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	bne	.L121
	mov	r1, r4
	ldr	r0, .L128+12
	bl	nm_ListGetNext
	b	.L127
.L121:
	ldr	r3, .L128
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L129:
	.align	2
.L128:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2137
	.word	.LANCHOR1
.LFE42:
	.size	STATION_get_first_available_station, .-STATION_get_first_available_station
	.section	.text.STATION_get_num_stations_in_use,"ax",%progbits
	.align	2
	.global	STATION_get_num_stations_in_use
	.type	STATION_get_num_stations_in_use, %function
STATION_get_num_stations_in_use:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L133
	stmfd	sp!, {r4, r5, lr}
.LCFI47:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L133+4
	ldr	r3, .L133+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L133+12
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r5, r0
	b	.L131
.L132:
	mov	r0, r5
	bl	STATION_station_is_available_for_use
	mov	r1, r5
	add	r4, r4, r0
	ldr	r0, .L133+12
	bl	nm_ListGetNext
	mov	r5, r0
.L131:
	cmp	r5, #0
	bne	.L132
	ldr	r3, .L133
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.L134:
	.align	2
.L133:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2065
	.word	.LANCHOR1
.LFE40:
	.size	STATION_get_num_stations_in_use, .-STATION_get_num_stations_in_use
	.section	.text.STATION_get_num_of_stations_physically_available,"ax",%progbits
	.align	2
	.global	STATION_get_num_of_stations_physically_available
	.type	STATION_get_num_of_stations_physically_available, %function
STATION_get_num_of_stations_physically_available:
.LFB39:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L139
	stmfd	sp!, {r4, r5, lr}
.LCFI48:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L139+4
	ldr	r3, .L139+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L139+12
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r5, r0
	b	.L136
.L138:
	mov	r0, r5
	bl	nm_STATION_get_physically_available
	mov	r1, r5
	cmp	r0, #1
	ldr	r0, .L139+12
	addeq	r4, r4, #1
	bl	nm_ListGetNext
	mov	r5, r0
.L136:
	cmp	r5, #0
	bne	.L138
	ldr	r3, .L139
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.L140:
	.align	2
.L139:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2033
	.word	.LANCHOR1
.LFE39:
	.size	STATION_get_num_of_stations_physically_available, .-STATION_get_num_of_stations_physically_available
	.section	.text.nm_STATION_get_station_number_0,"ax",%progbits
	.align	2
	.global	nm_STATION_get_station_number_0
	.type	nm_STATION_get_station_number_0, %function
nm_STATION_get_station_number_0:
.LFB46:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI49:
	mov	r4, r0
	mov	r1, r4
	ldr	r0, .L144
	bl	nm_OnList
	cmp	r0, #1
	ldreq	r0, [r4, #80]
	ldmeqfd	sp!, {r4, pc}
.LBB21:
	ldr	r0, .L144+4
	ldr	r1, .L144+8
	ldr	r2, .L144+12
	ldr	r3, .L144+16
	bl	Alert_item_not_on_list_with_filename
	mov	r0, #0
.LBE21:
	ldmfd	sp!, {r4, pc}
.L145:
	.align	2
.L144:
	.word	.LANCHOR1
	.word	.LC19
	.word	.LC20
	.word	.LC9
	.word	2325
.LFE46:
	.size	nm_STATION_get_station_number_0, .-nm_STATION_get_station_number_0
	.section	.text.nm_STATION_get_box_index_0,"ax",%progbits
	.align	2
	.global	nm_STATION_get_box_index_0
	.type	nm_STATION_get_box_index_0, %function
nm_STATION_get_box_index_0:
.LFB47:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI50:
	ldr	r3, .L147
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L147+4
	add	r1, r0, #84
	str	r3, [sp, #12]
	mov	r3, #11
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L148:
	.align	2
.L147:
	.word	nm_STATION_set_box_index
	.word	.LC0
.LFE47:
	.size	nm_STATION_get_box_index_0, .-nm_STATION_get_box_index_0
	.section	.text.nm_STATION_get_decoder_serial_number,"ax",%progbits
	.align	2
	.global	nm_STATION_get_decoder_serial_number
	.type	nm_STATION_get_decoder_serial_number, %function
nm_STATION_get_decoder_serial_number:
.LFB48:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI51:
	ldr	r3, .L150
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L150+4
	add	r1, r0, #88
	str	r3, [sp, #12]
	ldr	r3, .L150+8
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L151:
	.align	2
.L150:
	.word	nm_STATION_set_decoder_serial_number
	.word	.LC8
	.word	2097151
.LFE48:
	.size	nm_STATION_get_decoder_serial_number, .-nm_STATION_get_decoder_serial_number
	.section	.text.nm_STATION_get_decoder_output,"ax",%progbits
	.align	2
	.global	nm_STATION_get_decoder_output
	.type	nm_STATION_get_decoder_output, %function
nm_STATION_get_decoder_output:
.LFB49:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI52:
	ldr	r3, .L153
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L153+4
	add	r1, r0, #92
	str	r3, [sp, #12]
	mov	r3, #1
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L154:
	.align	2
.L153:
	.word	nm_STATION_set_decoder_output
	.word	.LC7
.LFE49:
	.size	nm_STATION_get_decoder_output, .-nm_STATION_get_decoder_output
	.section	.text.STATION_get_total_run_minutes_10u,"ax",%progbits
	.align	2
	.global	STATION_get_total_run_minutes_10u
	.type	STATION_get_total_run_minutes_10u, %function
STATION_get_total_run_minutes_10u:
.LFB50:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI53:
	ldr	r4, .L156
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L156+4
	ldr	r3, .L156+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #10
	str	r3, [sp, #0]
	ldr	r3, .L156+12
	add	r1, r5, #96
	str	r3, [sp, #4]
	add	r3, r5, #148
	str	r3, [sp, #8]
	ldr	r3, .L156+16
	mov	r2, #0
	str	r3, [sp, #12]
	mov	r0, r5
	ldr	r3, .L156+20
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L157:
	.align	2
.L156:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2436
	.word	nm_STATION_set_total_run_minutes
	.word	.LC6
	.word	9999
.LFE50:
	.size	STATION_get_total_run_minutes_10u, .-STATION_get_total_run_minutes_10u
	.section	.text.nm_STATION_get_cycle_minutes_10u,"ax",%progbits
	.align	2
	.global	nm_STATION_get_cycle_minutes_10u
	.type	nm_STATION_get_cycle_minutes_10u, %function
nm_STATION_get_cycle_minutes_10u:
.LFB51:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI54:
	ldr	r3, .L161
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L161+4
	ldr	r3, .L161+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	mov	r3, #50
	moveq	r4, r3
	beq	.L159
	str	r3, [sp, #0]
	ldr	r3, .L161+12
	mov	r0, r4
	str	r3, [sp, #4]
	add	r3, r4, #148
	str	r3, [sp, #8]
	ldr	r3, .L161+16
	add	r1, r4, #100
	str	r3, [sp, #12]
	mov	r2, #1
	ldr	r3, .L161+20
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r4, r0
.L159:
	ldr	r3, .L161
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L162:
	.align	2
.L161:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2479
	.word	nm_STATION_set_cycle_minutes
	.word	.LC21
	.word	24000
.LFE51:
	.size	nm_STATION_get_cycle_minutes_10u, .-nm_STATION_get_cycle_minutes_10u
	.section	.text.nm_STATION_get_watersense_cycle_max_10u,"ax",%progbits
	.align	2
	.global	nm_STATION_get_watersense_cycle_max_10u
	.type	nm_STATION_get_watersense_cycle_max_10u, %function
nm_STATION_get_watersense_cycle_max_10u:
.LFB52:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI55:
	ldr	r6, .L167
	subs	r4, r0, #0
	mov	r1, #400
	ldr	r0, [r6, #0]
	ldr	r2, .L167+4
	mov	r3, #2528
	ldrne	r5, [r4, #100]
	moveq	r5, #50
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	beq	.L165
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #0
	beq	.L165
	mov	r0, r4
	bl	STATION_get_GID_station_group
	cmp	r0, #0
	beq	.L165
	bl	STATION_GROUP_get_cycle_time_10u_for_this_gid
	mov	r5, r0
.L165:
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L168:
	.align	2
.L167:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
.LFE52:
	.size	nm_STATION_get_watersense_cycle_max_10u, .-nm_STATION_get_watersense_cycle_max_10u
	.section	.text.nm_STATION_set_cycle_minutes,"ax",%progbits
	.align	2
	.global	nm_STATION_set_cycle_minutes
	.type	nm_STATION_set_cycle_minutes, %function
nm_STATION_set_cycle_minutes:
.LFB8:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI56:
	sub	sp, sp, #44
.LCFI57:
	mov	r5, r0
	mov	r4, r1
	mov	r6, r2
	mov	r8, r3
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #0
	ldreq	r7, .L173
	beq	.L170
	mov	r0, r5
	bl	nm_STATION_get_watersense_cycle_max_10u
	mov	r7, r0
.L170:
	cmp	r4, r7
	bls	.L171
	mov	r1, r4
	ldr	r0, .L173+4
	mov	r2, r7
	bl	Alert_Message_va
	mov	r4, r7
.L171:
	mov	r3, #50
	stmib	sp, {r3, r6}
	ldr	r3, .L173+8
	mov	r0, r5
	str	r3, [sp, #12]
	ldr	r3, [sp, #68]
	add	r1, r5, #100
	str	r3, [sp, #20]
	ldr	r3, [sp, #72]
	mov	r2, r4
	str	r3, [sp, #24]
	ldr	r3, [sp, #76]
	str	r7, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r5, #156
	str	r3, [sp, #32]
	mov	r3, #8
	str	r3, [sp, #36]
	ldr	r3, .L173+12
	str	r8, [sp, #16]
	str	r3, [sp, #40]
	mov	r3, #1
	bl	SHARED_set_uint32_station
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L174:
	.align	2
.L173:
	.word	24000
	.word	.LC22
	.word	53250
	.word	.LC21
.LFE8:
	.size	nm_STATION_set_cycle_minutes, .-nm_STATION_set_cycle_minutes
	.section	.text.nm_STATION_get_soak_minutes,"ax",%progbits
	.align	2
	.global	nm_STATION_get_soak_minutes
	.type	nm_STATION_get_soak_minutes, %function
nm_STATION_get_soak_minutes:
.LFB53:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI58:
	ldr	r3, .L178
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L178+4
	ldr	r3, .L178+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	mov	r3, #20
	moveq	r4, r3
	beq	.L176
	str	r3, [sp, #0]
	ldr	r3, .L178+12
	mov	r0, r4
	str	r3, [sp, #4]
	add	r3, r4, #148
	str	r3, [sp, #8]
	ldr	r3, .L178+16
	add	r1, r4, #104
	str	r3, [sp, #12]
	mov	r2, #5
	mov	r3, #720
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r4, r0
.L176:
	ldr	r3, .L178
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L179:
	.align	2
.L178:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2574
	.word	nm_STATION_set_soak_minutes
	.word	.LC23
.LFE53:
	.size	nm_STATION_get_soak_minutes, .-nm_STATION_get_soak_minutes
	.section	.text.STATION_get_ET_factor_100u,"ax",%progbits
	.align	2
	.global	STATION_get_ET_factor_100u
	.type	STATION_get_ET_factor_100u, %function
STATION_get_ET_factor_100u:
.LFB55:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI59:
	ldr	r4, .L181
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L181+4
	ldr	r3, .L181+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #100
	str	r3, [sp, #0]
	ldr	r3, .L181+12
	add	r1, r5, #108
	str	r3, [sp, #4]
	add	r3, r5, #148
	str	r3, [sp, #8]
	ldr	r3, .L181+16
	mov	r2, #0
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #400
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L182:
	.align	2
.L181:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2670
	.word	nm_STATION_set_ET_factor
	.word	.LC2
.LFE55:
	.size	STATION_get_ET_factor_100u, .-STATION_get_ET_factor_100u
	.section	.text.nm_STATION_get_expected_flow_rate_gpm,"ax",%progbits
	.align	2
	.global	nm_STATION_get_expected_flow_rate_gpm
	.type	nm_STATION_get_expected_flow_rate_gpm, %function
nm_STATION_get_expected_flow_rate_gpm:
.LFB56:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI60:
	ldr	r3, .L184
	mov	r2, #1
	str	r3, [sp, #4]
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L184+4
	add	r1, r0, #112
	str	r3, [sp, #12]
	mov	r3, #4000
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L185:
	.align	2
.L184:
	.word	nm_STATION_set_expected_flow_rate
	.word	.LC16
.LFE56:
	.size	nm_STATION_get_expected_flow_rate_gpm, .-nm_STATION_get_expected_flow_rate_gpm
	.section	.text.nm_STATION_get_distribution_uniformity_100u,"ax",%progbits
	.align	2
	.global	nm_STATION_get_distribution_uniformity_100u
	.type	nm_STATION_get_distribution_uniformity_100u, %function
nm_STATION_get_distribution_uniformity_100u:
.LFB57:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI61:
	mov	r3, #70
	str	r3, [sp, #0]
	ldr	r3, .L187
	add	r1, r0, #116
	str	r3, [sp, #4]
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L187+4
	mov	r2, #40
	str	r3, [sp, #12]
	mov	r3, #100
	bl	SHARED_get_uint32_32_bit_change_bits_group
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L188:
	.align	2
.L187:
	.word	nm_STATION_set_distribution_uniformity
	.word	.LC3
.LFE57:
	.size	nm_STATION_get_distribution_uniformity_100u, .-nm_STATION_get_distribution_uniformity_100u
	.section	.text.nm_STATION_get_watersense_soak_min,"ax",%progbits
	.align	2
	.global	nm_STATION_get_watersense_soak_min
	.type	nm_STATION_get_watersense_soak_min, %function
nm_STATION_get_watersense_soak_min:
.LFB54:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI62:
	ldr	r6, .L193
	subs	r4, r0, #0
	mov	r1, #400
	ldr	r0, [r6, #0]
	ldr	r2, .L193+4
	ldr	r3, .L193+8
	ldrne	r5, [r4, #104]
	moveq	r5, #20
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	beq	.L191
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #0
	beq	.L191
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r7, r0, #0
	beq	.L191
	mov	r0, r4
	bl	nm_STATION_get_distribution_uniformity_100u
	mov	r1, r0
	mov	r0, r7
	bl	STATION_GROUP_get_soak_time_for_this_gid
	mov	r5, r0
.L191:
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L194:
	.align	2
.L193:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2623
.LFE54:
	.size	nm_STATION_get_watersense_soak_min, .-nm_STATION_get_watersense_soak_min
	.section	.text.nm_STATION_set_soak_minutes,"ax",%progbits
	.align	2
	.global	nm_STATION_set_soak_minutes
	.type	nm_STATION_set_soak_minutes, %function
nm_STATION_set_soak_minutes:
.LFB9:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI63:
	sub	sp, sp, #44
.LCFI64:
	mov	r7, r3
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #0
	moveq	r3, #5
	beq	.L196
	mov	r0, r4
	bl	nm_STATION_get_watersense_soak_min
	mov	r3, r0
.L196:
	mov	r2, #720
	mov	ip, #20
	stmia	sp, {r2, ip}
	ldr	r2, .L198
	mov	r0, r4
	str	r2, [sp, #12]
	ldr	r2, [sp, #64]
	add	r1, r4, #104
	str	r2, [sp, #20]
	ldr	r2, [sp, #68]
	str	r6, [sp, #8]
	str	r2, [sp, #24]
	ldr	r2, [sp, #72]
	str	r7, [sp, #16]
	str	r2, [sp, #28]
	add	r2, r4, #156
	str	r2, [sp, #32]
	mov	r2, #9
	str	r2, [sp, #36]
	ldr	r2, .L198+4
	str	r2, [sp, #40]
	mov	r2, r5
	bl	SHARED_set_uint32_station
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L199:
	.align	2
.L198:
	.word	53251
	.word	.LC23
.LFE9:
	.size	nm_STATION_set_soak_minutes, .-nm_STATION_set_soak_minutes
	.section	.text.nm_STATION_set_default_values_resulting_in_station_0_for_this_box,"ax",%progbits
	.align	2
	.type	nm_STATION_set_default_values_resulting_in_station_0_for_this_box, %function
nm_STATION_set_default_values_resulting_in_station_0_for_this_box:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI65:
	sub	sp, sp, #20
.LCFI66:
	mov	r4, r0
	mov	r8, r1
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #0
	mov	r7, r0
	beq	.L201
	ldr	sl, .L203
	add	r6, r4, #148
	mov	r0, r6
	ldr	r1, [sl, #0]
	bl	SHARED_clear_all_32_bit_change_bits
	add	r0, r4, #152
	ldr	r1, [sl, #0]
	bl	SHARED_clear_all_32_bit_change_bits
	add	r5, r4, #156
	add	r0, r4, #164
	ldr	r1, [sl, #0]
	bl	SHARED_clear_all_32_bit_change_bits
	mov	r0, r5
	ldr	r1, [sl, #0]
	bl	SHARED_set_all_32_bit_change_bits
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_physically_available
	mov	r0, r4
	mov	r1, r7
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_box_index
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_station_number
	mov	r0, r4
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_in_use
.LBB22:
	str	r5, [sp, #12]
	mov	r5, #0
	mov	r2, r5
	mov	r0, r4
	ldr	r1, .L203+4
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	str	r5, [sp, #16]
	bl	SHARED_set_name_32_bit_change_bits
.LBE22:
	mov	r0, r4
	mov	r1, r5
	mov	r2, r5
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_decoder_serial_number
	mov	r0, r4
	mov	r1, r5
	mov	r2, r5
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_decoder_output
	mov	r0, r4
	mov	r2, r5
	mov	r1, #10
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_total_run_minutes
	mov	r0, r4
	mov	r2, r5
	mov	r1, #50
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_cycle_minutes
	mov	r0, r4
	mov	r2, r5
	mov	r1, #20
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_soak_minutes
	mov	r0, r4
	mov	r2, r5
	mov	r1, #100
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_ET_factor
	mov	r0, r4
	mov	r2, r5
	mov	r1, #1
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_expected_flow_rate
	mov	r0, r4
	mov	r2, r5
	mov	r1, #70
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_distribution_uniformity
	mov	r0, r4
	mov	r1, r5
	mov	r2, r5
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_no_water_days
	mov	r0, r4
	mov	r2, r5
	mov	r1, #1056964608
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_moisture_balance_percent
	mov	r0, r4
	mov	r2, r5
	mov	r1, #1000
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_square_footage
	mov	r0, r4
	mov	r1, r5
	mov	r2, r5
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_ignore_moisture_balance_at_next_irrigation
	mov	r0, r4
	mov	r1, r5
	mov	r2, r5
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_GID_station_group
	mov	r0, r4
	mov	r1, r5
	mov	r2, r5
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_GID_manual_program_A
	mov	r0, r4
	mov	r1, r5
	mov	r2, r5
	mov	r3, #11
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_GID_manual_program_B
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L201:
	ldr	r0, .L203+8
	mov	r1, #688
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_func_call_with_null_ptr_with_filename
.L204:
	.align	2
.L203:
	.word	list_program_data_recursive_MUTEX
	.word	.LC24
	.word	.LC9
.LFE25:
	.size	nm_STATION_set_default_values_resulting_in_station_0_for_this_box, .-nm_STATION_set_default_values_resulting_in_station_0_for_this_box
	.section	.text.nm_station_structure_updater,"ax",%progbits
	.align	2
	.type	nm_station_structure_updater, %function
nm_station_structure_updater:
.LFB22:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI67:
	fstmfdd	sp!, {d8}
.LCFI68:
	mov	r4, r0
	sub	sp, sp, #12
.LCFI69:
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #7
	mov	r5, r0
	bne	.L206
	ldr	r0, .L241+4
	mov	r1, r4
	add	sp, sp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	Alert_Message_va
.L206:
	ldr	r0, .L241+8
	mov	r1, #7
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	bne	.L207
	ldr	r0, .L241+12
	bl	nm_ListGetFirst
	mvn	r4, #0
	mov	r1, r0
	b	.L208
.L209:
	str	r4, [r1, #156]
	ldr	r0, .L241+12
	bl	nm_ListGetNext
	mov	r1, r0
.L208:
	cmp	r1, #0
	bne	.L209
	b	.L210
.L207:
	cmp	r4, #1
	bne	.L211
.L210:
	ldr	r0, .L241+12
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r4, r0
	b	.L212
.L213:
	add	r3, r4, #148
	mov	r0, r4
	str	r3, [sp, #8]
	mov	r1, #1056964608
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6}
	bl	nm_STATION_set_moisture_balance_percent
	mov	r1, r4
	ldr	r0, .L241+12
	bl	nm_ListGetNext
	mov	r4, r0
.L212:
	cmp	r4, #0
	bne	.L213
	b	.L214
.L211:
	cmp	r4, #2
	bne	.L215
.L214:
	ldr	r0, .L241+12
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L216
.L217:
	str	r4, [r1, #164]
	ldr	r0, .L241+12
	bl	nm_ListGetNext
	mov	r1, r0
.L216:
	cmp	r1, #0
	bne	.L217
	b	.L218
.L215:
	cmp	r4, #3
	bne	.L219
.L218:
	ldr	r0, .L241+12
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r4, r0
	b	.L220
.L222:
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	add	r7, r4, #148
	cmp	r0, #0
	beq	.L221
	mov	r0, r4
	bl	nm_STATION_get_watersense_cycle_max_10u
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	mov	r1, r0
	mov	r0, r4
	bl	nm_STATION_set_cycle_minutes
	mov	r0, r4
	bl	nm_STATION_get_watersense_soak_min
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	mov	r1, r0
	mov	r0, r4
	bl	nm_STATION_set_soak_minutes
.L221:
	mov	r0, r4
	mov	r1, #1056964608
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_STATION_set_moisture_balance_percent
	mov	r0, r4
	mov	r1, #1000
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_STATION_set_square_footage
	ldr	r0, .L241+12
	mov	r1, r4
	bl	nm_ListGetNext
	mov	r4, r0
.L220:
	cmp	r4, #0
	bne	.L222
	b	.L223
.L219:
	cmp	r4, #4
	bne	.L224
.L223:
	ldr	r0, .L241+12
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r6, r4	@ movhi
	mov	r1, r0
	b	.L225
.L226:
	str	r4, [r1, #172]
	strh	r4, [r1, #176]	@ movhi
	strb	r6, [r1, #178]
	ldr	r0, .L241+12
	bl	nm_ListGetNext
	mov	r1, r0
.L225:
	cmp	r1, #0
	bne	.L226
	b	.L227
.L224:
	cmp	r4, #5
	bne	.L228
.L227:
	ldr	r0, .L241+12
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r4, r0
	b	.L229
.L230:
	add	r3, r4, #148
	mov	r0, r4
	str	r3, [sp, #8]
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6}
	bl	nm_STATION_set_ignore_moisture_balance_at_next_irrigation
	mov	r1, r4
	ldr	r0, .L241+12
	bl	nm_ListGetNext
	mov	r4, r0
.L229:
	cmp	r4, #0
	bne	.L230
	b	.L231
.L228:
	cmp	r4, #6
	bne	.L232
.L231:
	ldr	r0, .L241+12
	bl	nm_ListGetFirst
	flds	s16, .L241
	mov	r6, #1
	mov	r4, r0
	b	.L233
.L236:
	flds	s15, [r4, #160]
	fcmpes	s15, s16
	fmstat
	bpl	.L234
	add	r3, r4, #148
	mov	r1, #0
	str	r3, [sp, #8]
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r5, r6}
	bl	nm_STATION_set_ignore_moisture_balance_at_next_irrigation
.L234:
	mov	r1, r4
	ldr	r0, .L241+12
	bl	nm_ListGetNext
	mov	r4, r0
.L233:
	cmp	r4, #0
	bne	.L236
	b	.L240
.L232:
	ldr	r0, .L241+16
	add	sp, sp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	Alert_Message
.L240:
	add	sp, sp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L242:
	.align	2
.L241:
	.word	1056964608
	.word	.LC25
	.word	.LC26
	.word	.LANCHOR1
	.word	.LC27
.LFE22:
	.size	nm_station_structure_updater, .-nm_station_structure_updater
	.section	.text.STATION_get_no_water_days,"ax",%progbits
	.align	2
	.global	STATION_get_no_water_days
	.type	STATION_get_no_water_days, %function
STATION_get_no_water_days:
.LFB58:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI70:
	ldr	r4, .L244
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L244+4
	ldr	r3, .L244+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L244+12
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r5, #148
	str	r3, [sp, #8]
	ldr	r3, .L244+16
	add	r1, r5, #120
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #31
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L245:
	.align	2
.L244:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2792
	.word	nm_STATION_set_no_water_days
	.word	.LC5
.LFE58:
	.size	STATION_get_no_water_days, .-STATION_get_no_water_days
	.section	.text.nm_STATION_get_description,"ax",%progbits
	.align	2
	.global	nm_STATION_get_description
	.type	nm_STATION_get_description, %function
nm_STATION_get_description:
.LFB61:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI71:
	mov	r4, r0
	mov	r1, r4
	ldr	r0, .L249
	bl	nm_OnList
	cmp	r0, #1
	addeq	r0, r4, #20
	ldmeqfd	sp!, {r4, pc}
	ldr	r0, .L249+4
	ldr	r1, .L249+8
	ldr	r2, .L249+12
	mov	r3, #2896
	bl	Alert_item_not_on_list_with_filename
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L250:
	.align	2
.L249:
	.word	.LANCHOR1
	.word	.LC19
	.word	.LC20
	.word	.LC9
.LFE61:
	.size	nm_STATION_get_description, .-nm_STATION_get_description
	.section	.text.nm_STATION_get_number_of_manual_programs_station_is_assigned_to,"ax",%progbits
	.align	2
	.global	nm_STATION_get_number_of_manual_programs_station_is_assigned_to
	.type	nm_STATION_get_number_of_manual_programs_station_is_assigned_to, %function
nm_STATION_get_number_of_manual_programs_station_is_assigned_to:
.LFB62:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI72:
	mov	r4, r0
	mov	r1, r4
	ldr	r0, .L254
	bl	nm_OnList
	cmp	r0, #1
	bne	.L252
	ldr	r0, [r4, #140]
	ldr	r3, [r4, #144]
	adds	r0, r0, #0
	movne	r0, #1
	cmp	r3, #0
	addne	r0, r0, #1
	ldmfd	sp!, {r4, pc}
.L252:
	ldr	r0, .L254+4
	ldr	r1, .L254+8
	ldr	r2, .L254+12
	ldr	r3, .L254+16
	bl	Alert_item_not_on_list_with_filename
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L255:
	.align	2
.L254:
	.word	.LANCHOR1
	.word	.LC19
	.word	.LC20
	.word	.LC9
	.word	2918
.LFE62:
	.size	nm_STATION_get_number_of_manual_programs_station_is_assigned_to, .-nm_STATION_get_number_of_manual_programs_station_is_assigned_to
	.section	.text.STATION_get_GID_manual_program_A,"ax",%progbits
	.align	2
	.global	STATION_get_GID_manual_program_A
	.type	STATION_get_GID_manual_program_A, %function
STATION_get_GID_manual_program_A:
.LFB64:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI73:
	ldr	r4, .L257
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L257+4
	ldr	r3, .L257+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	MANUAL_PROGRAMS_get_last_group_ID
	ldr	r1, .L257+12
	mov	r2, #0
	str	r1, [sp, #4]
	add	r1, r5, #148
	str	r1, [sp, #8]
	ldr	r1, .L257+16
	str	r2, [sp, #0]
	str	r1, [sp, #12]
	add	r1, r5, #140
	mov	r3, r0
	mov	r0, r5
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L258:
	.align	2
.L257:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2973
	.word	nm_STATION_set_GID_manual_program_A
	.word	.LC15
.LFE64:
	.size	STATION_get_GID_manual_program_A, .-STATION_get_GID_manual_program_A
	.section	.text.STATION_get_GID_manual_program_B,"ax",%progbits
	.align	2
	.global	STATION_get_GID_manual_program_B
	.type	STATION_get_GID_manual_program_B, %function
STATION_get_GID_manual_program_B:
.LFB65:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI74:
	ldr	r4, .L260
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L260+4
	ldr	r3, .L260+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	MANUAL_PROGRAMS_get_last_group_ID
	ldr	r1, .L260+12
	mov	r2, #0
	str	r1, [sp, #4]
	add	r1, r5, #148
	str	r1, [sp, #8]
	ldr	r1, .L260+16
	str	r2, [sp, #0]
	str	r1, [sp, #12]
	add	r1, r5, #144
	mov	r3, r0
	mov	r0, r5
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L261:
	.align	2
.L260:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2994
	.word	nm_STATION_set_GID_manual_program_B
	.word	.LC14
.LFE65:
	.size	STATION_get_GID_manual_program_B, .-STATION_get_GID_manual_program_B
	.section	.text.STATION_does_this_station_belong_to_this_manual_program,"ax",%progbits
	.align	2
	.global	STATION_does_this_station_belong_to_this_manual_program
	.type	STATION_does_this_station_belong_to_this_manual_program, %function
STATION_does_this_station_belong_to_this_manual_program:
.LFB66:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI75:
	ldr	r6, .L266
	mov	r4, r0
	ldr	r2, .L266+4
	ldr	r3, .L266+8
	mov	r5, r1
	ldr	r0, [r6, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L266+12
	mov	r1, r4
	bl	nm_OnList
	subs	r7, r0, #0
	beq	.L263
	ldr	r3, [r4, #144]
	ldr	r7, [r4, #140]
	cmp	r5, r3
	moveq	r7, #1
	beq	.L264
	rsb	lr, r7, r5
	rsbs	r7, lr, #0
	adc	r7, r7, lr
	b	.L264
.L263:
	ldr	r0, .L266+16
	ldr	r1, .L266+20
	ldr	r2, .L266+4
	ldr	r3, .L266+24
	bl	Alert_item_not_on_list_with_filename
.L264:
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r7
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L267:
	.align	2
.L266:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3017
	.word	.LANCHOR1
	.word	.LC19
	.word	.LC20
	.word	3033
.LFE66:
	.size	STATION_does_this_station_belong_to_this_manual_program, .-STATION_does_this_station_belong_to_this_manual_program
	.section	.text.STATION_get_moisture_balance_percent,"ax",%progbits
	.align	2
	.global	STATION_get_moisture_balance_percent
	.type	STATION_get_moisture_balance_percent, %function
STATION_get_moisture_balance_percent:
.LFB67:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI76:
	ldr	r4, .L269
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L269+4
	ldr	r3, .L269+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #1056964608
	str	r3, [sp, #0]	@ float
	ldr	r3, .L269+12
	add	r1, r5, #160
	str	r3, [sp, #4]
	add	r3, r5, #148
	str	r3, [sp, #8]
	ldr	r3, .L269+16
	ldr	r2, .L269+20
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #1065353216
	bl	SHARED_get_float_32_bit_change_bits_group
	mov	r5, r0	@ float
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5	@ float
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L270:
	.align	2
.L269:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3046
	.word	nm_STATION_set_moisture_balance_percent
	.word	.LC13
	.word	-1027080192
.LFE67:
	.size	STATION_get_moisture_balance_percent, .-STATION_get_moisture_balance_percent
	.section	.text.STATION_get_moisture_balance,"ax",%progbits
	.align	2
	.global	STATION_get_moisture_balance
	.type	STATION_get_moisture_balance, %function
STATION_get_moisture_balance:
.LFB68:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI77:
	fstmfdd	sp!, {d8}
.LCFI78:
	ldr	r4, .L272+4
	mov	r1, #400
	ldr	r2, .L272+8
	ldr	r3, .L272+12
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	STATION_get_moisture_balance_percent
	fmsr	s17, r0
	mov	r0, r5
	bl	STATION_GROUP_get_soil_storage_capacity_inches_100u
	fmsr	s15, r0	@ int
	ldr	r0, [r4, #0]
	fuitos	s16, s15
	flds	s15, .L272
	fdivs	s16, s16, s15
	bl	xQueueGiveMutexRecursive
	fmuls	s16, s17, s16
	fmrs	r0, s16
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, pc}
.L273:
	.align	2
.L272:
	.word	1120403456
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3067
.LFE68:
	.size	STATION_get_moisture_balance, .-STATION_get_moisture_balance
	.section	.text.STATION_get_square_footage,"ax",%progbits
	.align	2
	.global	STATION_get_square_footage
	.type	STATION_get_square_footage, %function
STATION_get_square_footage:
.LFB69:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI79:
	ldr	r4, .L275
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L275+4
	ldr	r3, .L275+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #1000
	str	r3, [sp, #0]
	ldr	r3, .L275+12
	add	r1, r5, #168
	str	r3, [sp, #4]
	add	r3, r5, #148
	str	r3, [sp, #8]
	ldr	r3, .L275+16
	mov	r2, #1
	str	r3, [sp, #12]
	mov	r0, r5
	ldr	r3, .L275+20
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L276:
	.align	2
.L275:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3081
	.word	nm_STATION_set_square_footage
	.word	.LC4
	.word	99999
.LFE69:
	.size	STATION_get_square_footage, .-STATION_get_square_footage
	.section	.text.nm_STATION_copy_station_into_guivars,"ax",%progbits
	.align	2
	.global	nm_STATION_copy_station_into_guivars
	.type	nm_STATION_copy_station_into_guivars, %function
nm_STATION_copy_station_into_guivars:
.LFB30:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI80:
	fstmfdd	sp!, {d8}
.LCFI81:
	mov	r4, r0
	sub	sp, sp, #24
.LCFI82:
	mov	r0, sp
	bl	EPSON_obtain_latest_complete_time_and_date
	cmp	r4, #0
	beq	.L278
.LBB23:
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L286+8
	flds	s16, .L286
	str	r0, [r3, #0]
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L286+12
	bl	strlcpy
	ldr	r1, [r4, #80]
	ldr	r3, .L286+16
	add	r0, r1, #1
	ldr	r2, .L286+20
	str	r0, [r3, #0]
	mov	r3, #4
	bl	STATION_get_station_number_string
	mov	r0, r4
	bl	nm_STATION_get_box_index_0
	ldr	r3, .L286+24
	str	r0, [r3, #0]
	mov	r0, r4
	bl	nm_STATION_get_decoder_serial_number
	ldr	r3, .L286+28
	str	r0, [r3, #0]
	mov	r0, r4
	bl	nm_STATION_get_decoder_output
	ldr	r3, .L286+32
	str	r0, [r3, #0]
	mov	r0, r4
	bl	STATION_get_ET_factor_100u
	ldr	r3, .L286+36
	sub	r0, r0, #100
	str	r0, [r3, #0]
	mov	r0, r4
	bl	STATION_get_total_run_minutes_10u
	ldr	r3, .L286+40
	fmsr	s14, r0	@ int
	mov	r0, r4
	fuitos	s15, s14
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	bl	nm_STATION_get_distribution_uniformity_100u
	ldr	r3, .L286+44
	str	r0, [r3, #0]
	mov	r0, r4
	bl	nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar
	ldr	r3, .L286+48
	str	r0, [r3, #0]	@ float
	mov	r0, r4
	bl	nm_STATION_get_cycle_minutes_10u
	ldr	r3, .L286+52
	fmsr	s14, r0	@ int
	mov	r0, r4
	fuitos	s15, s14
	fdivs	s16, s15, s16
	fsts	s16, [r3, #0]
	bl	nm_STATION_get_soak_minutes
	ldr	r3, .L286+56
	str	r0, [r3, #0]
	mov	r0, r4
	bl	nm_STATION_get_expected_flow_rate_gpm
	ldr	r3, .L286+60
	str	r0, [r3, #0]
	mov	r0, r4
	bl	STATION_get_no_water_days
	ldr	r3, .L286+64
	str	r0, [r3, #0]
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	ldr	r2, .L286+68
	ldr	r3, .L286+72
	cmp	r0, #0
	movne	r1, #1
	strne	r1, [r2, #0]
	strne	r1, [r3, #0]
	streq	r0, [r2, #0]
	streq	r0, [r3, #0]
	mov	r0, r4
	bl	STATION_get_moisture_balance_percent
	flds	s15, .L286+4
	ldr	r3, .L286+76
	fmsr	s14, r0
	mov	r0, r4
	fmuls	s15, s14, s15
	fsts	s15, [r3, #0]
	bl	STATION_get_moisture_balance
	ldr	r3, .L286+80
	str	r0, [r3, #0]	@ float
	mov	r0, r4
	bl	STATION_get_square_footage
	ldr	r3, .L286+84
	ldrh	r1, [sp, #4]
	str	r0, [r3, #0]
	mov	r0, r4
	bl	PERCENT_ADJUST_get_station_percentage_100u
	ldrh	r1, [sp, #4]
	mov	r5, r0
	mov	r0, r4
	bl	PERCENT_ADJUST_get_station_remaining_days
	ldr	r3, .L286+88
	cmp	r0, #0
	cmpne	r5, #100
	moveq	r2, #0
	movne	r2, #1
	mov	r6, r0
	streq	r2, [r3, #0]
	beq	.L282
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L286+72
	sub	r0, r5, #100
	str	r2, [r3, #0]
	bl	abs
	ldr	r3, .L286+92
	cmp	r5, #100
	movls	r5, #0
	movhi	r5, #1
	str	r0, [r3, #0]
	ldr	r3, .L286+96
	str	r5, [r3, #0]
	ldr	r3, .L286+100
	str	r6, [r3, #0]
.L282:
	bl	STATION_update_cycle_too_short_warning
	ldr	r5, [r4, #136]
	ldr	r3, .L286+104
	cmp	r5, #0
	str	r5, [r3, #0]
	beq	.L283
	mov	r0, r5
	bl	STATION_GROUP_get_group_with_this_GID
	mov	r5, r0
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L286+108
	bl	strlcpy
	mov	r0, r5
	bl	STATION_GROUP_get_precip_rate_in_per_hr
	ldr	r3, .L286+112
	str	r0, [r3, #0]	@ float
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	ldr	r3, .L286+116
	mov	r1, r5
	rsbs	r0, r0, #1
	movcc	r0, #0
	str	r0, [r3, #0]
	mov	r0, r4
	bl	nm_IRRITIME_this_program_is_scheduled_to_run
	ldr	r3, .L286+120
	str	r0, [r3, #0]
	b	.L284
.L283:
	mov	r1, r5
	ldr	r0, .L286+124
	bl	GuiLib_GetTextPtr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L286+108
	bl	strlcpy
	ldr	r3, .L286+120
	str	r5, [r3, #0]
.L284:
	ldr	r0, .L286+108
	bl	STATION_copy_group_name_into_copy_station_GuiVar_and_set_offsets
	mov	r0, r4
	add	r1, sp, #20
	bl	STATION_PRESERVES_get_index_using_ptr_to_station_struct
	cmp	r0, #1
	bne	.L277
	ldr	r2, [sp, #20]
	ldr	r3, .L286+128
	add	r3, r3, r2, asl #7
	ldrh	r1, [r3, #140]
	ldr	r2, .L286+132
	mov	r1, r1, lsr #6
	and	r1, r1, #15
	str	r1, [r2, #0]
	ldrb	r2, [r3, #140]	@ zero_extendqisi2
	ldr	r3, .L286+136
	mov	r2, r2, lsr #4
	and	r2, r2, #3
	str	r2, [r3, #0]
	b	.L277
.L278:
.LBE23:
	ldr	r0, .L286+140
	ldr	r1, .L286+144
	bl	Alert_func_call_with_null_ptr_with_filename
.L277:
	add	sp, sp, #24
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, pc}
.L287:
	.align	2
.L286:
	.word	1092616192
	.word	1120403456
	.word	g_GROUP_ID
	.word	GuiVar_GroupName
	.word	GuiVar_StationInfoNumber
	.word	GuiVar_StationInfoNumber_str
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_StationDecoderSerialNumber
	.word	GuiVar_StationDecoderOutput
	.word	GuiVar_StationInfoETFactor
	.word	GuiVar_StationInfoTotalMinutes
	.word	GuiVar_StationInfoDU
	.word	GuiVar_StationInfoEstMin
	.word	GuiVar_StationInfoCycleTime
	.word	GuiVar_StationInfoSoakInTime
	.word	GuiVar_StationInfoExpectedFlow
	.word	GuiVar_StationInfoNoWaterDays
	.word	GuiVar_StationInfoShowPercentOfET
	.word	GuiVar_StationInfoShowEstMin
	.word	GuiVar_StationInfoMoistureBalancePercent
	.word	GuiVar_StationInfoMoistureBalance
	.word	GuiVar_StationInfoSquareFootage
	.word	GuiVar_StationInfoShowPercentAdjust
	.word	GuiVar_PercentAdjustPercent
	.word	GuiVar_PercentAdjustPositive
	.word	GuiVar_PercentAdjustNumberOfDays
	.word	g_STATION_group_ID
	.word	GuiVar_StationInfoStationGroup
	.word	GuiVar_StationGroupPrecipRate
	.word	GuiVar_StationInfoShowCopyStation
	.word	GuiVar_StationInfoGroupHasStartTime
	.word	1001
	.word	station_preserves
	.word	GuiVar_StationInfoIStatus
	.word	GuiVar_StationInfoFlowStatus
	.word	.LC9
	.word	1501
.LFE30:
	.size	nm_STATION_copy_station_into_guivars, .-nm_STATION_copy_station_into_guivars
	.section	.text.nm_STATION_get_Budget_data,"ax",%progbits
	.align	2
	.global	nm_STATION_get_Budget_data
	.type	nm_STATION_get_Budget_data, %function
nm_STATION_get_Budget_data:
.LFB73:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r4, r5, lr}
.LCFI83:
	mov	r4, r1
	mov	r5, r0
	bne	.L289
	ldr	r0, .L290
	bl	RemovePathFromFileName
	mov	r2, #3264
	mov	r1, r0
	ldr	r0, .L290+4
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_Message_va
.L289:
	ldr	r3, [r0, #84]
	str	r3, [r1, #0]
	ldr	r3, [r0, #80]
	str	r3, [r1, #4]
	ldr	r3, [r0, #72]
	str	r3, [r1, #8]
	ldr	r3, [r0, #76]
	str	r3, [r1, #12]
	ldr	r3, [r0, #96]
	str	r3, [r1, #16]
	bl	WEATHER_get_station_uses_daily_et
	ldr	r3, [r5, #108]
	str	r3, [r4, #24]
	ldr	r3, [r5, #116]
	str	r3, [r4, #28]
	ldr	r3, [r5, #112]
	str	r3, [r4, #36]
	str	r0, [r4, #20]
	mov	r0, r5
	bl	STATION_GROUP_get_station_precip_rate_in_per_hr_100000u
	ldr	r3, [r5, #120]
	str	r3, [r4, #44]
	ldr	r3, [r5, #136]
	str	r3, [r4, #48]
	fmsr	s14, r0	@ int
	mov	r0, r5
	fuitos	s15, s14
	fsts	s15, [r4, #40]
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	ldr	r3, [r5, #140]
	str	r3, [r4, #56]
	ldr	r3, [r5, #144]
	str	r3, [r4, #60]
	str	r0, [r4, #52]
	ldmfd	sp!, {r4, r5, pc}
.L291:
	.align	2
.L290:
	.word	.LC9
	.word	.LC28
.LFE73:
	.size	nm_STATION_get_Budget_data, .-nm_STATION_get_Budget_data
	.section	.text.nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag,"ax",%progbits
	.align	2
	.global	nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag
	.type	nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag, %function
nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag:
.LFB78:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, [r0, #172]
	bx	lr
.LFE78:
	.size	nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag, .-nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag
	.section	.text.STATION_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	STATION_get_change_bits_ptr
	.type	STATION_get_change_bits_ptr, %function
STATION_get_change_bits_ptr:
.LFB79:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r0
	add	r2, r3, #152
	mov	r0, r1
	add	r1, r3, #148
	add	r3, r3, #156
	b	SHARED_get_32_bit_change_bits_ptr
.LFE79:
	.size	STATION_get_change_bits_ptr, .-STATION_get_change_bits_ptr
	.section	.text.STATION_decrement_no_water_days_and_return_value_after_decrement,"ax",%progbits
	.align	2
	.global	STATION_decrement_no_water_days_and_return_value_after_decrement
	.type	STATION_decrement_no_water_days_and_return_value_after_decrement, %function
STATION_decrement_no_water_days_and_return_value_after_decrement:
.LFB59:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L298
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, lr}
.LCFI84:
	ldr	r2, .L298+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L298+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L298+12
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #1
	mov	r5, r0
	bne	.L295
	ldr	r6, [r4, #120]
	cmp	r6, #0
	beq	.L296
	bl	FLOWSENSE_get_controller_index
	mov	r1, #6
	mov	r7, r0
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	sub	r1, r6, #1
	mov	r2, r5
	mov	r3, #6
	str	r7, [sp, #0]
	str	r5, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_no_water_days
.L296:
	ldr	r4, [r4, #120]
	b	.L297
.L295:
	ldr	r0, .L298+16
	ldr	r1, .L298+20
	ldr	r2, .L298+4
	ldr	r3, .L298+24
	bl	Alert_item_not_on_list_with_filename
	mov	r4, #0
.L297:
	ldr	r3, .L298
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, pc}
.L299:
	.align	2
.L298:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2813
	.word	.LANCHOR1
	.word	.LC19
	.word	.LC20
	.word	2828
.LFE59:
	.size	STATION_decrement_no_water_days_and_return_value_after_decrement, .-STATION_decrement_no_water_days_and_return_value_after_decrement
	.section	.text.STATION_chain_down_NOW_days_midnight_maintenance,"ax",%progbits
	.align	2
	.global	STATION_chain_down_NOW_days_midnight_maintenance
	.type	STATION_chain_down_NOW_days_midnight_maintenance, %function
STATION_chain_down_NOW_days_midnight_maintenance:
.LFB60:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L304
	stmfd	sp!, {r4, lr}
.LCFI85:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L304+4
	ldr	r3, .L304+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L304+12
	bl	nm_ListGetFirst
	b	.L303
.L302:
	mov	r0, r4
	bl	STATION_decrement_no_water_days_and_return_value_after_decrement
	ldr	r0, .L304+12
	mov	r1, r4
	bl	nm_ListGetNext
.L303:
	cmp	r0, #0
	mov	r4, r0
	bne	.L302
	ldr	r3, .L304
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L305:
	.align	2
.L304:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	2849
	.word	.LANCHOR1
.LFE60:
	.size	STATION_chain_down_NOW_days_midnight_maintenance, .-STATION_chain_down_NOW_days_midnight_maintenance
	.section	.text.STATION_store_stations_in_use,"ax",%progbits
	.align	2
	.global	STATION_store_stations_in_use
	.type	STATION_store_stations_in_use, %function
STATION_store_stations_in_use:
.LFB37:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L310
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI86:
	mov	r7, r0
	mov	r6, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L310+4
	mov	r8, r2
	ldr	r2, .L310+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L310+12
	bl	nm_ListGetFirst
	mov	r5, #0
	mov	r4, r0
	b	.L307
.L309:
	mov	r0, r4
	bl	nm_STATION_get_physically_available
	cmp	r0, #1
	mov	sl, r0
	bne	.L308
	mov	r1, r8
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r9, r0
	mov	r0, r5
	bl	STATION_SELECTION_GRID_station_is_selected
	mov	r2, sl
	mov	r3, r7
	stmia	sp, {r6, sl}
	str	r9, [sp, #8]
	mov	r1, r0
	mov	r0, r4
	bl	nm_STATION_set_in_use
.L308:
	mov	r1, r4
	ldr	r0, .L310+12
	bl	nm_ListGetNext
	add	r5, r5, #1
	mov	r4, r0
.L307:
	cmp	r4, #0
	bne	.L309
	ldr	r3, .L310
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	xQueueGiveMutexRecursive
.L311:
	.align	2
.L310:
	.word	list_program_data_recursive_MUTEX
	.word	1964
	.word	.LC9
	.word	.LANCHOR1
.LFE37:
	.size	STATION_store_stations_in_use, .-STATION_store_stations_in_use
	.section	.text.STATION_store_manual_programs_assignment_changes,"ax",%progbits
	.align	2
	.global	STATION_store_manual_programs_assignment_changes
	.type	STATION_store_manual_programs_assignment_changes, %function
STATION_store_manual_programs_assignment_changes:
.LFB36:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI87:
	ldr	r9, [sp, #48]
	mov	r8, r3
	ldr	r3, .L321
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L321+4
	mov	r6, r2
	ldr	r2, .L321+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L321+12
	bl	nm_ListGetFirst
	mov	sl, #0
	mov	r4, r0
.L317:
	mov	r1, r9
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	fp, r0
	mov	r0, sl
	bl	STATION_SELECTION_GRID_station_is_selected
	ldr	r3, [r4, #140]
	cmp	r0, #1
	mov	r2, r0
	bne	.L313
	cmp	r3, #0
	ldr	r1, [r4, #144]
	bne	.L314
	cmp	r1, r5
	stmneia	sp, {r6, r8, fp}
	movne	r0, r4
	movne	r1, r5
	bne	.L320
.L314:
	cmp	r1, #0
	bne	.L315
	cmp	r3, r5
	beq	.L315
	stmia	sp, {r6, r8, fp}
	mov	r0, r4
	mov	r1, r5
	b	.L319
.L313:
	cmp	r3, r5
	bne	.L316
	mov	r0, r4
	mov	r1, #0
	mov	r2, #1
	stmia	sp, {r6, r8, fp}
.L320:
	mov	r3, r7
	bl	nm_STATION_set_GID_manual_program_A
	b	.L315
.L316:
	ldr	r3, [r4, #144]
	cmp	r3, r5
	bne	.L315
	mov	r0, r4
	mov	r1, #0
	stmia	sp, {r6, r8, fp}
.L319:
	mov	r2, #1
	mov	r3, r7
	bl	nm_STATION_set_GID_manual_program_B
.L315:
	mov	r1, r4
	ldr	r0, .L321+12
	bl	nm_ListGetNext
	add	sl, sl, #1
	cmp	sl, #768
	mov	r4, r0
	bne	.L317
	ldr	r3, .L321
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	xQueueGiveMutexRecursive
.L322:
	.align	2
.L321:
	.word	list_program_data_recursive_MUTEX
	.word	1885
	.word	.LC9
	.word	.LANCHOR1
.LFE36:
	.size	STATION_store_manual_programs_assignment_changes, .-STATION_store_manual_programs_assignment_changes
	.section	.text.STATION_adjust_group_settings_after_changing_group_assignment,"ax",%progbits
	.align	2
	.global	STATION_adjust_group_settings_after_changing_group_assignment
	.type	STATION_adjust_group_settings_after_changing_group_assignment, %function
STATION_adjust_group_settings_after_changing_group_assignment:
.LFB34:
	@ args = 8, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI88:
	sub	sp, sp, #24
.LCFI89:
	mov	r4, r0
	mov	r8, r1
	mov	r9, r2
	mov	r7, r3
	ldr	sl, [sp, #64]
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #0
	beq	.L323
	mov	r0, r4
	mov	r1, sl
	bl	STATION_get_change_bits_ptr
	cmp	r8, #0
	mov	r6, r0
	beq	.L325
	mov	r0, r8
	bl	STATION_GROUP_get_cycle_time_10u_for_this_gid
	mov	r5, r0
	mov	r0, r4
	bl	nm_STATION_get_distribution_uniformity_100u
	mov	r1, r0
	mov	r0, r8
	bl	STATION_GROUP_get_soak_time_for_this_gid
	str	r0, [sp, #20]
.L325:
	mov	r0, r9
	bl	STATION_GROUP_get_cycle_time_10u_for_this_gid
	mov	fp, r0
	mov	r0, r4
	bl	nm_STATION_get_distribution_uniformity_100u
	mov	r1, r0
	mov	r0, r9
	bl	STATION_GROUP_get_soak_time_for_this_gid
	mov	r3, r0
	mov	r0, r4
	str	r3, [sp, #12]
	bl	nm_STATION_get_cycle_minutes_10u
	mov	r1, r0
	mov	r0, r4
	str	r1, [sp, #16]
	bl	nm_STATION_get_soak_minutes
	cmp	r8, #0
	ldr	r1, [sp, #16]
	ldr	r3, [sp, #12]
	beq	.L331
	cmp	r9, #0
	moveq	r5, #20
	moveq	fp, #50
	beq	.L326
	cmp	r1, r5
	beq	.L327
	cmp	r1, fp
	movcc	fp, r1
.L327:
	ldr	r2, [sp, #20]
	cmp	r0, r3
	movcs	r5, r0
	movcc	r5, r3
	cmp	r0, r2
	bne	.L326
.L331:
	mov	r5, r3
.L326:
	ldr	r3, [sp, #60]
	mov	r0, r4
	stmib	sp, {r3, r6}
	mov	r1, fp
	mov	r2, #1
	mov	r3, sl
	str	r7, [sp, #0]
	bl	nm_STATION_set_cycle_minutes
	ldr	r2, [sp, #60]
	mov	r0, r4
	stmib	sp, {r2, r6}
	mov	r1, r5
	mov	r2, #1
	mov	r3, sl
	str	r7, [sp, #0]
	bl	nm_STATION_set_soak_minutes
.L323:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.LFE34:
	.size	STATION_adjust_group_settings_after_changing_group_assignment, .-STATION_adjust_group_settings_after_changing_group_assignment
	.section	.text.STATION_store_group_assignment_changes,"ax",%progbits
	.align	2
	.global	STATION_store_group_assignment_changes
	.type	STATION_store_group_assignment_changes, %function
STATION_store_group_assignment_changes:
.LFB35:
	@ args = 12, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI90:
	sub	sp, sp, #24
.LCFI91:
	str	r3, [sp, #16]
	ldr	r3, [sp, #68]
	mov	r9, r0
	str	r3, [sp, #20]
	ldr	r3, .L338
	mov	fp, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L338+4
	mov	r6, r2
	ldr	r2, .L338+8
	ldr	r8, [sp, #60]
	ldr	sl, [sp, #64]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L338+12
	bl	nm_ListGetFirst
	mov	r5, #0
	mov	r4, r0
.L336:
	cmp	r4, #0
	beq	.L334
	mov	r0, r4
	blx	r9
	ldr	r1, [sp, #20]
	mov	r7, r0
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	str	r0, [sp, #12]
	mov	r0, r5
	bl	STATION_SELECTION_GRID_station_is_selected
	cmp	r0, #1
	mov	r2, r0
	bne	.L335
	cmp	r7, r6
	beq	.L334
	ldr	r3, [sp, #12]
	stmia	sp, {r8, sl}
	str	r3, [sp, #8]
	mov	r0, r4
	ldr	r3, [sp, #16]
	mov	r1, r6
	blx	fp
	ldr	r3, .L338+16
	cmp	r9, r3
	bne	.L334
	ldr	r3, [sp, #20]
	mov	r0, r4
	str	r3, [sp, #4]
	mov	r1, r7
	mov	r2, r6
	mov	r3, r8
	str	sl, [sp, #0]
	bl	STATION_adjust_group_settings_after_changing_group_assignment
	b	.L334
.L335:
	cmp	r7, r6
	bne	.L334
	mov	r0, r4
	bl	STATION_station_is_available_for_use
	cmp	r0, #1
	mov	r2, r0
	bne	.L334
	ldr	r3, [sp, #12]
	stmia	sp, {r8, sl}
	str	r3, [sp, #8]
	mov	r0, r4
	mov	r1, #0
	ldr	r3, [sp, #16]
	blx	fp
.L334:
	mov	r1, r4
	ldr	r0, .L338+12
	bl	nm_ListGetNext
	add	r5, r5, #1
	cmp	r5, #768
	mov	r4, r0
	bne	.L336
	ldr	r3, .L338
	ldr	r0, [r3, #0]
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	xQueueGiveMutexRecursive
.L339:
	.align	2
.L338:
	.word	list_program_data_recursive_MUTEX
	.word	1825
	.word	.LC9
	.word	.LANCHOR1
	.word	STATION_get_GID_station_group
.LFE35:
	.size	STATION_store_group_assignment_changes, .-STATION_store_group_assignment_changes
	.section	.text.STATION_build_data_to_send,"ax",%progbits
	.align	2
	.global	STATION_build_data_to_send
	.type	STATION_build_data_to_send, %function
STATION_build_data_to_send:
.LFB27:
	@ args = 4, pretend = 0, frame = 44
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI92:
	mov	r8, r3
	sub	sp, sp, #72
.LCFI93:
	mov	r3, #0
	str	r3, [sp, #68]
	ldr	r3, .L358
	str	r1, [sp, #40]
	mov	r4, r0
	mov	r6, r2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L358+4
	ldr	r3, .L358+8
	ldr	r9, [sp, #108]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L358+12
	bl	nm_ListGetFirst
	b	.L356
.L344:
	mov	r0, r5
	mov	r1, r8
	bl	STATION_get_change_bits_ptr
	ldr	r3, [r0, #0]
	cmp	r3, #0
	ldrne	r3, [sp, #68]
	addne	r3, r3, #1
	strne	r3, [sp, #68]
	bne	.L343
.L342:
	ldr	r0, .L358+12
	mov	r1, r5
	bl	nm_ListGetNext
.L356:
	cmp	r0, #0
	mov	r5, r0
	bne	.L344
.L343:
	ldr	r3, [sp, #68]
	cmp	r3, #0
	beq	.L354
	mov	r3, #0
	str	r3, [sp, #68]
	mov	r0, r4
	mov	r3, r6
	add	r1, sp, #60
	ldr	r2, [sp, #40]
	str	r9, [sp, #0]
	bl	PDATA_allocate_space_for_num_changed_groups_in_pucp
	ldr	r3, [r6, #0]
	cmp	r3, #0
	str	r0, [sp, #36]
	beq	.L345
	ldr	r0, .L358+12
	bl	nm_ListGetFirst
	ldr	r2, [sp, #40]
	add	r2, r2, #16
	str	r2, [sp, #52]
	mov	r5, r0
	b	.L346
.L352:
	mov	r0, r5
	mov	r1, r8
	bl	STATION_get_change_bits_ptr
	ldr	r3, [r0, #0]
	mov	r7, r0
	cmp	r3, #0
	beq	.L347
	ldr	r3, [r6, #0]
	cmp	r3, #0
	beq	.L348
	ldr	r3, [sp, #52]
	ldr	r2, [sp, #36]
	add	sl, r3, r2
	cmp	sl, r9
	bcs	.L348
	add	r3, r5, #84
	mov	r1, r3
	mov	r2, #4
	mov	r0, r4
	str	r3, [sp, #44]
	bl	PDATA_copy_var_into_pucp
	add	r2, r5, #80
	mov	r1, r2
	str	r2, [sp, #48]
	mov	r0, r4
	mov	r2, #4
	bl	PDATA_copy_var_into_pucp
	mov	r1, #4
	mov	r2, r1
	add	r3, sp, #64
	mov	r0, r4
	bl	PDATA_copy_bitfield_info_into_pucp
	add	r3, r5, #20
	str	r3, [sp, #4]
	mov	r3, #48
	add	fp, r5, #164
	mov	r2, #0
	str	r3, [sp, #8]
	add	r1, sp, #56
	mov	r3, r7
	mov	r0, r4
	str	r2, [sp, #56]
	str	sl, [sp, #12]
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r2, [sp, #40]
	ldr	r3, [sp, #36]
	mov	sl, #4
	add	r3, r3, r2
	str	r3, [sp, #32]
	ldr	r2, [sp, #32]
	add	r3, r5, #72
	str	r3, [sp, #4]
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	sl, [sp, #8]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, r0, #16
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #1
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #76
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #2
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r3, [sp, #48]
	ldr	r2, [sp, #32]
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #3
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r3, [sp, #44]
	ldr	r2, [sp, #32]
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, sl
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #88
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #5
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #92
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #6
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #96
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #7
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #100
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #8
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #104
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #9
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #108
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #10
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #112
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #11
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #116
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #12
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #120
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #13
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #136
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #17
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #140
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #18
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #144
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #19
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #160
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #20
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #168
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #21
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #172
	stmib	sp, {r3, sl}
	add	r1, sp, #56
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	mov	r2, #22
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r7, ip, r0
	cmp	r7, #16
	bls	.L355
	b	.L357
.L348:
	mov	r3, #0
	str	r3, [r6, #0]
	b	.L351
.L357:
	ldr	r3, [sp, #68]
	ldr	r0, [sp, #64]
	add	r3, r3, #1
	add	r1, sp, #56
	mov	r2, sl
	str	r3, [sp, #68]
	bl	memcpy
	ldr	r3, [sp, #36]
	add	r3, r3, r7
	str	r3, [sp, #36]
	b	.L347
.L355:
	ldr	r3, [r4, #0]
	sub	r3, r3, #16
	str	r3, [r4, #0]
.L347:
	mov	r1, r5
	ldr	r0, .L358+12
	bl	nm_ListGetNext
	mov	r5, r0
.L346:
	cmp	r5, #0
	bne	.L352
.L351:
	ldr	r3, [sp, #68]
	cmp	r3, #0
	beq	.L353
	ldr	r0, [sp, #60]
	add	r1, sp, #68
	mov	r2, #4
	bl	memcpy
	b	.L345
.L353:
	ldr	r2, [r4, #0]
	sub	r2, r2, #4
	str	r2, [r4, #0]
.L354:
	str	r3, [sp, #36]
.L345:
	ldr	r3, .L358
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [sp, #36]
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L359:
	.align	2
.L358:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	837
	.word	.LANCHOR1
.LFE27:
	.size	STATION_build_data_to_send, .-STATION_build_data_to_send
	.section	.text.nm_STATION_create_new_station,"ax",%progbits
	.align	2
	.global	nm_STATION_create_new_station
	.type	nm_STATION_create_new_station, %function
nm_STATION_create_new_station:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, lr}
.LCFI94:
	mov	r5, r0
	ldr	r0, .L366
	mov	r7, r1
	mov	r8, r2
	bl	nm_ListGetFirst
	b	.L365
.L364:
	mov	r0, r4
	bl	nm_STATION_get_box_index_0
	cmp	r5, r0
	bcc	.L362
	mov	r0, r4
	bl	nm_STATION_get_box_index_0
	cmp	r5, r0
	bne	.L363
	mov	r0, r4
	bl	nm_STATION_get_station_number_0
	cmp	r7, r0
	bcc	.L362
.L363:
	ldr	r0, .L366
	mov	r1, r4
	bl	nm_ListGetNext
.L365:
	cmp	r0, #0
	mov	r4, r0
	bne	.L364
.L362:
	mov	r6, #1
	ldr	r2, .L366+4
	mov	r3, #180
	ldr	r1, .L366+8
	ldr	r0, .L366
	str	r4, [sp, #4]
	str	r6, [sp, #0]
	bl	nm_GROUP_create_new_group
	mov	r1, r8
	mov	r4, r0
	bl	STATION_get_change_bits_ptr
	mov	r8, r0
	bl	FLOWSENSE_get_controller_index
	mov	r1, r7
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r0, r6, r8}
	mov	r0, r4
	bl	nm_STATION_set_station_number
	bl	FLOWSENSE_get_controller_index
	mov	r1, r5
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r0, r6, r8}
	mov	r0, r4
	bl	nm_STATION_set_box_index
	mov	r0, r4
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, pc}
.L367:
	.align	2
.L366:
	.word	.LANCHOR1
	.word	nm_STATION_set_default_values_resulting_in_station_0_for_this_box
	.word	.LANCHOR3
.LFE26:
	.size	nm_STATION_create_new_station, .-nm_STATION_create_new_station
	.section	.text.nm_STATION_store_changes,"ax",%progbits
	.align	2
	.global	nm_STATION_store_changes
	.type	nm_STATION_store_changes, %function
nm_STATION_store_changes:
.LFB20:
	@ args = 12, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI95:
	subs	r6, r0, #0
	sub	sp, sp, #24
.LCFI96:
	mov	r5, r1
	mov	r4, r2
	mov	r7, r3
	ldr	r9, [sp, #60]
	ldr	sl, [sp, #68]
	beq	.L369
	ldr	r0, .L402
	mov	r1, r6
	add	r2, sp, #20
	bl	nm_GROUP_find_this_group_in_list
	ldr	r0, [sp, #20]
	cmp	r0, #0
	beq	.L370
	ldr	r1, [sp, #64]
	bl	STATION_get_change_bits_ptr
	cmp	r4, #2
	mov	r8, r0
	beq	.L371
	tst	sl, #1
	beq	.L372
.L371:
	mov	r0, r6
	ldr	fp, [sp, #20]
	bl	nm_GROUP_get_name
.LBB24:
	mov	r2, #0
	add	r3, fp, #156
	str	r3, [sp, #12]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	str	r2, [sp, #16]
.LBE24:
	mov	r1, r0
.LBB26:
	mov	r0, fp
	bl	SHARED_set_name_32_bit_change_bits
	cmp	r0, #1
	bne	.L372
	cmp	r5, #0
	bne	.L372
.LBB25:
	str	r5, [sp, #0]
	mov	r3, #48
	stmib	sp, {r3, r4, r7}
	ldr	r0, .L402+4
	ldr	r1, [fp, #84]
	ldr	r2, [fp, #80]
	mov	r3, r5
	bl	Alert_ChangeLine_Station
.L372:
.LBE25:
.LBE26:
	ldr	r3, [sp, #64]
	cmp	r3, #2
	beq	.L373
	tst	sl, #2
	beq	.L374
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #72]
	mov	r3, r4
	bl	nm_STATION_set_physically_available
.L374:
	tst	sl, #4
	beq	.L375
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #76]
	mov	r3, r4
	bl	nm_STATION_set_in_use
.L375:
	tst	sl, #8
	beq	.L376
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #80]
	mov	r3, r4
	bl	nm_STATION_set_station_number
.L376:
	tst	sl, #16
	beq	.L377
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #84]
	mov	r3, r4
	bl	nm_STATION_set_box_index
.L377:
	tst	sl, #32
	beq	.L378
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #88]
	mov	r3, r4
	bl	nm_STATION_set_decoder_serial_number
.L378:
	tst	sl, #64
	beq	.L379
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #92]
	mov	r3, r4
	bl	nm_STATION_set_decoder_output
.L379:
	tst	sl, #131072
	beq	.L380
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #136]
	mov	r3, r4
	bl	nm_STATION_set_GID_station_group
.L380:
	tst	sl, #262144
	beq	.L381
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #140]
	mov	r3, r4
	bl	nm_STATION_set_GID_manual_program_A
.L381:
	tst	sl, #524288
	beq	.L373
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #144]
	mov	r3, r4
	bl	nm_STATION_set_GID_manual_program_B
.L373:
	cmp	r4, #2
	beq	.L382
	tst	sl, #128
	beq	.L383
.L382:
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #96]
	mov	r3, r4
	bl	nm_STATION_set_total_run_minutes
	cmp	r4, #2
	beq	.L384
.L383:
	tst	sl, #256
	beq	.L385
.L384:
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #100]
	mov	r3, r4
	bl	nm_STATION_set_cycle_minutes
	cmp	r4, #2
	beq	.L386
.L385:
	tst	sl, #512
	beq	.L387
.L386:
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #104]
	mov	r3, r4
	bl	nm_STATION_set_soak_minutes
	cmp	r4, #2
	beq	.L388
.L387:
	tst	sl, #1024
	beq	.L389
.L388:
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #108]
	mov	r3, r4
	bl	nm_STATION_set_ET_factor
	cmp	r4, #2
	beq	.L390
.L389:
	tst	sl, #2048
	beq	.L391
.L390:
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #112]
	mov	r3, r4
	bl	nm_STATION_set_expected_flow_rate
	cmp	r4, #2
	beq	.L392
.L391:
	tst	sl, #4096
	beq	.L393
.L392:
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #116]
	mov	r3, r4
	bl	nm_STATION_set_distribution_uniformity
	cmp	r4, #2
	beq	.L394
.L393:
	tst	sl, #8192
	beq	.L395
.L394:
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #120]
	mov	r3, r4
	bl	nm_STATION_set_no_water_days
	cmp	r4, #2
	beq	.L396
.L395:
	tst	sl, #1048576
	beq	.L397
.L396:
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #160]	@ float
	mov	r3, r4
	bl	nm_STATION_set_moisture_balance_percent
	cmp	r4, #2
	beq	.L398
.L397:
	tst	sl, #2097152
	beq	.L399
.L398:
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #168]
	mov	r3, r4
	bl	nm_STATION_set_square_footage
	cmp	r4, #2
	beq	.L400
.L399:
	tst	sl, #4194304
	beq	.L368
.L400:
	rsbs	r2, r5, #1
	stmia	sp, {r7, r9}
	ldr	r0, [sp, #20]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r6, #172]
	mov	r3, r4
	bl	nm_STATION_set_ignore_moisture_balance_at_next_irrigation
	b	.L368
.L370:
	ldr	r0, .L402+8
	ldr	r1, .L402+12
	bl	Alert_group_not_found_with_filename
	b	.L368
.L369:
	ldr	r0, .L402+8
	ldr	r1, .L402+16
	bl	Alert_func_call_with_null_ptr_with_filename
.L368:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L403:
	.align	2
.L402:
	.word	.LANCHOR1
	.word	53256
	.word	.LC17
	.word	1910
	.word	1918
.LFE20:
	.size	nm_STATION_store_changes, .-nm_STATION_store_changes
	.section	.text.nm_STATION_get_pointer_to_station,"ax",%progbits
	.align	2
	.global	nm_STATION_get_pointer_to_station
	.type	nm_STATION_get_pointer_to_station, %function
nm_STATION_get_pointer_to_station:
.LFB80:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI97:
	mov	r5, r0
	ldr	r0, .L410
	mov	r4, r1
	bl	nm_ListGetFirst
	b	.L409
.L408:
	ldr	r3, [r1, #84]
	cmp	r3, r5
	bne	.L406
	ldr	r3, [r1, #80]
	cmp	r3, r4
	beq	.L407
.L406:
	ldr	r0, .L410
	bl	nm_ListGetNext
.L409:
	cmp	r0, #0
	mov	r1, r0
	bne	.L408
.L407:
	mov	r0, r1
	ldmfd	sp!, {r4, r5, pc}
.L411:
	.align	2
.L410:
	.word	.LANCHOR1
.LFE80:
	.size	nm_STATION_get_pointer_to_station, .-nm_STATION_get_pointer_to_station
	.section	.text.STATION_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	STATION_extract_and_store_changes_from_GuiVars
	.type	STATION_extract_and_store_changes_from_GuiVars, %function
STATION_extract_and_store_changes_from_GuiVars:
.LFB32:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI98:
	ldr	r7, .L413+8
	ldr	r9, .L413+12
	sub	sp, sp, #24
.LCFI99:
	ldr	r3, .L413+16
	mov	r2, r7
	mov	r1, #400
	ldr	r0, [r9, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r7
	ldr	r2, .L413+20
	mov	r0, #180
	bl	mem_malloc_debug
	ldr	r8, .L413+24
	ldr	sl, .L413+28
	ldr	r1, [r8, #0]
	mov	r5, #2
	sub	r1, r1, #1
	mov	r6, #1
	mov	fp, #0
	mov	r4, r0
	ldr	r0, [sl, #0]
	bl	nm_STATION_get_pointer_to_station
	mov	r2, #180
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	mov	r2, #48
	add	r0, r4, #20
	ldr	r1, .L413+32
	bl	strlcpy
	ldr	r3, .L413+36
	flds	s15, .L413
	ldr	ip, .L413+40
	ldr	r3, [r3, #0]
	ldr	r1, [ip, #0]
	str	r3, [r4, #88]
	ldr	r3, .L413+44
	ldr	r3, [r3, #0]
	str	r3, [r4, #92]
	ldr	r3, .L413+48
	flds	s14, [r3, #0]
	ldr	r3, .L413+52
	fmuls	s14, s14, s15
	ftouizs	s14, s14
	fsts	s14, [r4, #96]	@ int
	flds	s14, [r3, #0]
	ldr	r3, .L413+56
	ldr	r3, [r3, #0]
	fmuls	s15, s14, s15
	str	r3, [r4, #104]
	ldr	r3, .L413+60
	ldr	r3, [r3, #0]
	ftouizs	s15, s15
	add	r3, r3, #100
	str	r3, [r4, #108]
	ldr	r3, .L413+64
	ldr	r3, [r3, #0]
	fsts	s15, [r4, #100]	@ int
	str	r3, [r4, #112]
	ldr	r3, .L413+68
	flds	s15, .L413+4
	ldr	r3, [r3, #0]
	str	r3, [r4, #116]
	ldr	r3, .L413+72
	ldr	r3, [r3, #0]
	str	r3, [r4, #120]
	ldr	r3, .L413+76
	flds	s14, [r3, #0]
	ldr	r3, .L413+80
	fdivs	s15, s14, s15
	ldr	r3, [r3, #0]
	str	r3, [r4, #168]
	fsts	s15, [r4, #160]
	str	ip, [sp, #12]
	str	r1, [sp, #16]
	bl	FLOWSENSE_get_controller_index
	ldr	r1, [sp, #16]
	mov	r2, r5
	str	r6, [sp, #0]
	stmib	sp, {r5, fp}
	mov	r3, r0
	mov	r0, r4
	bl	nm_STATION_store_changes
	ldr	r2, .L413+84
	mov	r0, r4
	mov	r1, r7
	bl	mem_free_debug
	ldr	r1, [r8, #0]
	ldr	r0, [sl, #0]
	sub	r1, r1, #1
	bl	nm_STATION_get_pointer_to_station
	ldr	r7, .L413+88
	mov	r4, r0
	bl	STATION_get_GID_station_group
	ldr	r8, [r7, #0]
	str	r0, [sp, #20]
	bl	FLOWSENSE_get_controller_index
	mov	r1, r5
	mov	sl, r0
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r1, r8
	mov	r2, r6
	mov	r3, r5
	str	sl, [sp, #0]
	str	r6, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_GID_station_group
	ldr	r7, [r7, #0]
	bl	FLOWSENSE_get_controller_index
	ldr	r1, [sp, #20]
	mov	r2, r7
	str	r6, [sp, #0]
	str	r5, [sp, #4]
	mov	r3, r0
	mov	r0, r4
	bl	STATION_adjust_group_settings_after_changing_group_assignment
	ldr	r0, [r9, #0]
	bl	xQueueGiveMutexRecursive
	ldr	ip, [sp, #12]
	str	fp, [ip, #0]
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L414:
	.align	2
.L413:
	.word	1092616192
	.word	1120403456
	.word	.LC9
	.word	list_program_data_recursive_MUTEX
	.word	1574
	.word	1578
	.word	GuiVar_StationInfoNumber
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_GroupName
	.word	GuiVar_StationDecoderSerialNumber
	.word	g_GROUP_creating_new
	.word	GuiVar_StationDecoderOutput
	.word	GuiVar_StationInfoTotalMinutes
	.word	GuiVar_StationInfoCycleTime
	.word	GuiVar_StationInfoSoakInTime
	.word	GuiVar_StationInfoETFactor
	.word	GuiVar_StationInfoExpectedFlow
	.word	GuiVar_StationInfoDU
	.word	GuiVar_StationInfoNoWaterDays
	.word	GuiVar_StationInfoMoistureBalancePercent
	.word	GuiVar_StationInfoSquareFootage
	.word	1609
	.word	g_STATION_group_ID
.LFE32:
	.size	STATION_extract_and_store_changes_from_GuiVars, .-STATION_extract_and_store_changes_from_GuiVars
	.section	.text.STATION_find_first_available_station_and_init_station_number_GuiVars,"ax",%progbits
	.align	2
	.global	STATION_find_first_available_station_and_init_station_number_GuiVars
	.type	STATION_find_first_available_station_and_init_station_number_GuiVars, %function
STATION_find_first_available_station_and_init_station_number_GuiVars:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L420
	stmfd	sp!, {r4, r5, lr}
.LCFI100:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L420+4
	ldr	r2, .L420+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L420+12
	ldr	r4, [r3, #8]
	cmp	r4, #0
	beq	.L416
	ldr	r3, .L420+16
	ldr	r1, [r3, #0]
	ldr	r3, .L420+20
	sub	r1, r1, #1
	ldr	r0, [r3, #0]
	bl	nm_STATION_get_pointer_to_station
	subs	r4, r0, #0
	beq	.L417
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	bne	.L416
.L417:
	bl	STATION_get_first_available_station
	subs	r4, r0, #0
	beq	.L416
	bl	nm_STATION_get_box_index_0
	ldr	r5, .L420+20
	str	r0, [r5, #0]
	mov	r0, r4
	bl	nm_STATION_get_station_number_0
	ldr	r3, .L420+16
	add	r2, r0, #1
	str	r2, [r3, #0]
	mov	r1, r0
	ldr	r2, .L420+24
	ldr	r0, [r5, #0]
	mov	r3, #4
	bl	STATION_get_station_number_string
.L416:
	ldr	r3, .L420
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r4, #0
	ldmnefd	sp!, {r4, r5, pc}
	ldr	r0, .L420+28
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_Message
.L421:
	.align	2
.L420:
	.word	list_program_data_recursive_MUTEX
	.word	1311
	.word	.LC9
	.word	.LANCHOR1
	.word	GuiVar_StationInfoNumber
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_StationInfoNumber_str
	.word	.LC29
.LFE29:
	.size	STATION_find_first_available_station_and_init_station_number_GuiVars, .-STATION_find_first_available_station_and_init_station_number_GuiVars
	.global	__umodsi3
	.section	.text.nm_STATION_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	nm_STATION_extract_and_store_changes_from_comm
	.type	nm_STATION_extract_and_store_changes_from_comm, %function
nm_STATION_extract_and_store_changes_from_comm:
.LFB21:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI101:
	mov	r7, r0
	sub	sp, sp, #44
.LCFI102:
	str	r1, [sp, #16]
	str	r2, [sp, #20]
	mov	r8, r3
	mov	r1, r7
	add	r0, sp, #28
	mov	r2, #4
	mov	r9, #0
	bl	memcpy
	add	r7, r7, #4
	mov	fp, #4
	str	r9, [sp, #12]
	mov	sl, r8
	b	.L423
.L450:
	mov	r0, r9
	mov	r1, #25
	bl	__umodsi3
	cmp	r0, #0
	bne	.L424
.LBB27:
	bl	xTaskGetCurrentTaskHandle
	ldr	r3, .L454
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	xQueueGetMutexHolder
	rsb	r5, r0, r4
	rsbs	r4, r5, #0
	adc	r4, r4, r5
	cmp	r4, #0
	beq	.L425
	ldr	r3, .L454
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L425:
	mov	r0, #5
	bl	vTaskDelay
	cmp	r4, #0
	beq	.L424
	ldr	r3, .L454
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L454+4
	ldr	r3, .L454+8
	bl	xQueueTakeMutexRecursive_debug
.L424:
.LBE27:
	mov	r1, r7
	mov	r2, #4
	add	r0, sp, #36
	bl	memcpy
	add	r1, r7, #4
	mov	r2, #4
	add	r0, sp, #40
	bl	memcpy
	add	r1, r7, #8
	mov	r2, #4
	add	r0, sp, #32
	bl	memcpy
	add	r1, r7, #12
	mov	r2, #4
	add	r0, sp, #24
	bl	memcpy
	ldr	r0, [sp, #36]
	ldr	r1, [sp, #40]
	bl	nm_STATION_get_pointer_to_station
	add	r4, r7, #16
	add	r5, fp, #16
	subs	r8, r0, #0
	bne	.L426
	ldr	r0, [sp, #36]
	ldr	r1, [sp, #40]
	mov	r2, sl
	bl	nm_STATION_create_new_station
	cmp	sl, #16
	mov	r8, r0
	bne	.L427
	ldr	r3, .L454
	add	r0, r0, #148
	ldr	r1, [r3, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L427:
	cmp	r8, #0
	beq	.L428
	mov	r3, #1
	str	r3, [sp, #12]
.L426:
	ldr	r1, .L454+4
	ldr	r2, .L454+12
	mov	r0, #180
	bl	mem_malloc_debug
	mov	r1, #0
	mov	r2, #180
	mov	r6, r0
	bl	memset
	mov	r0, r6
	mov	r1, r8
	mov	r2, #180
	bl	memcpy
	ldr	r3, [sp, #24]
	tst	r3, #1
	beq	.L429
	mov	r1, r4
	add	r0, r6, #20
	mov	r2, #48
	bl	memcpy
	add	r4, r7, #64
	add	r5, fp, #64
.L429:
	ldr	r3, [sp, #24]
	tst	r3, #2
	beq	.L430
	mov	r1, r4
	add	r0, r6, #72
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L430:
	ldr	r3, [sp, #24]
	tst	r3, #4
	beq	.L431
	mov	r1, r4
	add	r0, r6, #76
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L431:
	ldr	r3, [sp, #24]
	tst	r3, #8
	beq	.L432
	mov	r1, r4
	add	r0, r6, #80
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L432:
	ldr	r3, [sp, #24]
	tst	r3, #16
	beq	.L433
	mov	r1, r4
	add	r0, r6, #84
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L433:
	ldr	r3, [sp, #24]
	tst	r3, #32
	beq	.L434
	mov	r1, r4
	add	r0, r6, #88
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L434:
	ldr	r3, [sp, #24]
	tst	r3, #64
	beq	.L435
	mov	r1, r4
	add	r0, r6, #92
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L435:
	ldr	r3, [sp, #24]
	tst	r3, #128
	beq	.L436
	mov	r1, r4
	add	r0, r6, #96
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L436:
	ldr	r3, [sp, #24]
	tst	r3, #256
	beq	.L437
	mov	r1, r4
	add	r0, r6, #100
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L437:
	ldr	r3, [sp, #24]
	tst	r3, #512
	beq	.L438
	mov	r1, r4
	add	r0, r6, #104
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L438:
	ldr	r3, [sp, #24]
	tst	r3, #1024
	beq	.L439
	mov	r1, r4
	add	r0, r6, #108
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L439:
	ldr	r3, [sp, #24]
	tst	r3, #2048
	beq	.L440
	mov	r1, r4
	add	r0, r6, #112
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L440:
	ldr	r3, [sp, #24]
	tst	r3, #4096
	beq	.L441
	mov	r1, r4
	add	r0, r6, #116
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L441:
	ldr	r3, [sp, #24]
	tst	r3, #8192
	beq	.L442
	mov	r1, r4
	add	r0, r6, #120
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L442:
	ldr	r3, [sp, #24]
	tst	r3, #131072
	beq	.L443
	mov	r1, r4
	add	r0, r6, #136
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L443:
	ldr	r3, [sp, #24]
	tst	r3, #262144
	beq	.L444
	mov	r1, r4
	add	r0, r6, #140
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L444:
	ldr	r3, [sp, #24]
	tst	r3, #524288
	beq	.L445
	mov	r1, r4
	add	r0, r6, #144
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L445:
	ldr	r3, [sp, #24]
	tst	r3, #1048576
	beq	.L446
	mov	r1, r4
	add	r0, r6, #160
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L446:
	ldr	r3, [sp, #24]
	tst	r3, #2097152
	beq	.L447
	mov	r1, r4
	add	r0, r6, #168
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L447:
	ldr	r3, [sp, #24]
	tst	r3, #4194304
	beq	.L448
	mov	r1, r4
	add	r0, r6, #172
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L448:
	ldr	r3, [sp, #20]
	mov	r0, r6
	stmia	sp, {r3, sl}
	ldr	r3, [sp, #24]
	ldr	r1, [sp, #12]
	str	r3, [sp, #8]
	ldr	r2, [sp, #16]
	mov	r3, #0
	bl	nm_STATION_store_changes
	mov	r0, r6
	ldr	r1, .L454+4
	ldr	r2, .L454+16
	bl	mem_free_debug
	b	.L449
.L428:
	ldr	r0, .L454+4
	mov	r1, #2336
	bl	Alert_group_not_found_with_filename
	mov	r3, #1
	str	r3, [sp, #12]
.L449:
	add	r9, r9, #1
	mov	fp, r5
	mov	r7, r4
.L423:
	ldr	r3, [sp, #28]
	cmp	r9, r3
	bcc	.L450
	cmp	fp, #0
	mov	r8, sl
	beq	.L451
	cmp	sl, #1
	cmpne	sl, #15
	beq	.L452
	cmp	sl, #16
	bne	.L453
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L453
.L452:
	mov	r0, #9
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L453
.L451:
	ldr	r0, .L454+4
	ldr	r1, .L454+20
	bl	Alert_bit_set_with_no_data_with_filename
.L453:
	mov	r0, fp
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L455:
	.align	2
.L454:
	.word	list_program_data_recursive_MUTEX
	.word	.LC17
	.word	2043
	.word	2120
	.word	2317
	.word	2370
.LFE21:
	.size	nm_STATION_extract_and_store_changes_from_comm, .-nm_STATION_extract_and_store_changes_from_comm
	.section	.text.nm_STATION_get_pointer_to_decoder_based_station,"ax",%progbits
	.align	2
	.global	nm_STATION_get_pointer_to_decoder_based_station
	.type	nm_STATION_get_pointer_to_decoder_based_station, %function
nm_STATION_get_pointer_to_decoder_based_station:
.LFB81:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI103:
	subs	r5, r1, #0
	mov	r4, r0
	mov	r6, r2
	moveq	r1, r5
	beq	.L457
	ldr	r0, .L463
	bl	nm_ListGetFirst
	b	.L462
.L460:
	ldr	r3, [r1, #84]
	cmp	r3, r4
	bne	.L459
	ldr	r3, [r1, #88]
	cmp	r3, r5
	bne	.L459
	ldr	r3, [r1, #92]
	cmp	r3, r6
	beq	.L457
.L459:
	ldr	r0, .L463
	bl	nm_ListGetNext
.L462:
	cmp	r0, #0
	mov	r1, r0
	bne	.L460
.L457:
	mov	r0, r1
	ldmfd	sp!, {r4, r5, r6, pc}
.L464:
	.align	2
.L463:
	.word	.LANCHOR1
.LFE81:
	.size	nm_STATION_get_pointer_to_decoder_based_station, .-nm_STATION_get_pointer_to_decoder_based_station
	.section	.text.STATION_get_next_available_station,"ax",%progbits
	.align	2
	.global	STATION_get_next_available_station
	.type	STATION_get_next_available_station, %function
STATION_get_next_available_station:
.LFB82:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L472
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI104:
	ldr	r2, .L472+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L472+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [r4, #0]
	ldr	r0, [r5, #0]
	sub	r1, r1, #1
	bl	nm_STATION_get_pointer_to_station
	subs	r6, r0, #0
	beq	.L466
.L470:
	mov	r1, r6
	ldr	r0, .L472+12
	bl	nm_ListGetNext
	subs	r6, r0, #0
	bne	.L467
	ldr	r0, .L472+12
	bl	nm_ListGetFirst
	mov	r6, r0
.L467:
	mov	r0, r6
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L470
	ldr	r3, [r6, #84]
	str	r3, [r5, #0]
	ldr	r3, [r6, #80]
	add	r3, r3, #1
	str	r3, [r4, #0]
	b	.L469
.L466:
	ldr	r0, .L472+4
	ldr	r1, .L472+16
	bl	Alert_station_not_found_with_filename
.L469:
	ldr	r3, .L472
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, pc}
.L473:
	.align	2
.L472:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3667
	.word	.LANCHOR1
	.word	3702
.LFE82:
	.size	STATION_get_next_available_station, .-STATION_get_next_available_station
	.section	.text.STATION_get_prev_available_station,"ax",%progbits
	.align	2
	.global	STATION_get_prev_available_station
	.type	STATION_get_prev_available_station, %function
STATION_get_prev_available_station:
.LFB83:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L481
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI105:
	ldr	r2, .L481+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L481+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [r4, #0]
	ldr	r0, [r5, #0]
	sub	r1, r1, #1
	bl	nm_STATION_get_pointer_to_station
	subs	r6, r0, #0
	beq	.L475
.L479:
	mov	r1, r6
	ldr	r0, .L481+12
	bl	nm_ListGetPrev
	subs	r6, r0, #0
	bne	.L476
	ldr	r0, .L481+12
	bl	nm_ListGetLast
	mov	r6, r0
.L476:
	mov	r0, r6
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L479
	cmp	r6, #0
	beq	.L475
	ldr	r3, [r6, #84]
	str	r3, [r5, #0]
	ldr	r3, [r6, #80]
	add	r3, r3, #1
	str	r3, [r4, #0]
	b	.L478
.L475:
	ldr	r0, .L481+4
	ldr	r1, .L481+16
	bl	Alert_station_not_found_with_filename
	mov	r6, #0
.L478:
	ldr	r3, .L481
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, pc}
.L482:
	.align	2
.L481:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3739
	.word	.LANCHOR1
	.word	3777
.LFE83:
	.size	STATION_get_prev_available_station, .-STATION_get_prev_available_station
	.section	.text.STATION_clean_house_processing,"ax",%progbits
	.align	2
	.global	STATION_clean_house_processing
	.type	STATION_clean_house_processing, %function
STATION_clean_house_processing:
.LFB84:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L487
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI106:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L487+4
	ldr	r3, .L487+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L487+12
	ldr	r1, .L487+4
	ldr	r2, .L487+16
	b	.L486
.L485:
	ldr	r1, .L487+4
	mov	r2, #3808
	bl	mem_free_debug
	ldr	r0, .L487+12
	ldr	r1, .L487+4
	ldr	r2, .L487+20
.L486:
	bl	nm_ListRemoveHead_debug
	cmp	r0, #0
	bne	.L485
	ldr	r4, .L487
	str	r0, [sp, #0]
	str	r0, [sp, #4]
	ldr	r1, .L487+24
	ldr	r2, .L487+28
	mov	r3, #180
	ldr	r0, .L487+12
	bl	nm_GROUP_create_new_group
	mov	r3, #180
	str	r3, [sp, #0]
	ldr	r3, [r4, #0]
	mov	r0, #1
	str	r3, [sp, #4]
	mov	r3, #9
	str	r3, [sp, #8]
	ldr	r1, .L487+32
	mov	r2, #7
	ldr	r3, .L487+12
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldr	r0, [r4, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L488:
	.align	2
.L487:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3800
	.word	.LANCHOR1
	.word	3804
	.word	3810
	.word	.LANCHOR3
	.word	nm_STATION_set_default_values_resulting_in_station_0_for_this_box
	.word	.LANCHOR0
.LFE84:
	.size	STATION_clean_house_processing, .-STATION_clean_house_processing
	.section	.text.STATION_set_not_physically_available_based_upon_communication_scan_results,"ax",%progbits
	.align	2
	.global	STATION_set_not_physically_available_based_upon_communication_scan_results
	.type	STATION_set_not_physically_available_based_upon_communication_scan_results, %function
STATION_set_not_physically_available_based_upon_communication_scan_results:
.LFB85:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI107:
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L493
	mov	r1, #400
	ldr	r2, .L493+4
	ldr	r6, .L493+8
	mov	r5, #92
	mov	r8, #1
	mov	r7, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L493+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L493+16
	bl	nm_ListGetFirst
	mov	r4, r0
	b	.L490
.L492:
	ldr	r3, [r4, #84]
	mla	r3, r5, r3, r6
	ldr	sl, [r3, #16]
	cmp	sl, #0
	bne	.L491
	mov	r1, #10
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r1, sl
	mov	r2, #1
	mov	r3, #10
	stmia	sp, {r7, r8}
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_physically_available
.L491:
	mov	r1, r4
	ldr	r0, .L493+16
	bl	nm_ListGetNext
	mov	r4, r0
.L490:
	cmp	r4, #0
	bne	.L492
	ldr	r3, .L493
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L494:
	.align	2
.L493:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	chain
	.word	3846
	.word	.LANCHOR1
.LFE85:
	.size	STATION_set_not_physically_available_based_upon_communication_scan_results, .-STATION_set_not_physically_available_based_upon_communication_scan_results
	.section	.text.STATION_for_stations_on_this_box_set_physically_available_based_upon_discovery_results,"ax",%progbits
	.align	2
	.global	STATION_for_stations_on_this_box_set_physically_available_based_upon_discovery_results
	.type	STATION_for_stations_on_this_box_set_physically_available_based_upon_discovery_results, %function
STATION_for_stations_on_this_box_set_physically_available_based_upon_discovery_results:
.LFB86:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, lr}
.LCFI108:
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L504
	mov	r1, #400
	ldr	r2, .L504+4
	ldr	r7, .L504+8
	mov	r6, #1
	mov	r5, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L504+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L504+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L504+4
	ldr	r3, .L504+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L504+24
	bl	nm_ListGetFirst
	mov	r4, r0
	b	.L496
.L501:
	ldr	r3, [r4, #84]
	cmp	r3, r5
	bne	.L497
	ldr	r2, [r4, #88]
	cmp	r2, #0
	beq	.L497
	ldr	r3, .L504+28
.L500:
	ldr	r1, [r3, #220]
	cmp	r1, #0
	beq	.L498
	cmp	r1, r2
	bne	.L499
	mov	r1, #12
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r1, #1
	stmia	sp, {r5, r6}
	mov	r2, r1
	mov	r3, #12
	str	r0, [sp, #8]
	mov	r0, r4
	b	.L503
.L499:
	add	r3, r3, #60
	cmp	r3, r7
	bne	.L500
.L498:
	mov	r1, #13
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r1, #0
	mov	r2, #1
	mov	r3, #13
	stmia	sp, {r5, r6}
	str	r0, [sp, #8]
	mov	r0, r4
.L503:
	bl	nm_STATION_set_physically_available
.L497:
	mov	r1, r4
	ldr	r0, .L504+24
	bl	nm_ListGetNext
	mov	r4, r0
.L496:
	cmp	r4, #0
	bne	.L501
	ldr	r3, .L504+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L504
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L505:
	.align	2
.L504:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	tpmicro_data+4800
	.word	3895
	.word	tpmicro_data_recursive_MUTEX
	.word	3897
	.word	.LANCHOR1
	.word	tpmicro_data
.LFE86:
	.size	STATION_for_stations_on_this_box_set_physically_available_based_upon_discovery_results, .-STATION_for_stations_on_this_box_set_physically_available_based_upon_discovery_results
	.section	.text.STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token
	.type	STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token, %function
STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token:
.LFB87:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI109:
	ldr	r4, .L510
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L510+4
	ldr	r3, .L510+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L510+12
	bl	nm_ListGetFirst
	b	.L509
.L508:
	add	r0, r5, #152
	ldr	r1, [r4, #0]
	bl	SHARED_set_all_32_bit_change_bits
	ldr	r0, .L510+12
	mov	r1, r5
	bl	nm_ListGetNext
.L509:
	cmp	r0, #0
	mov	r5, r0
	bne	.L508
	ldr	r3, .L510
	ldr	r4, .L510+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L510+4
	ldr	r3, .L510+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L510+24
	ldr	r0, [r4, #0]
	mov	r2, #1
	str	r2, [r3, #444]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L511:
	.align	2
.L510:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3960
	.word	.LANCHOR1
	.word	comm_mngr_recursive_MUTEX
	.word	3982
	.word	comm_mngr
.LFE87:
	.size	STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token, .-STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token
	.section	.text.STATION_on_all_stations_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	STATION_on_all_stations_set_or_clear_commserver_change_bits
	.type	STATION_on_all_stations_set_or_clear_commserver_change_bits, %function
STATION_on_all_stations_set_or_clear_commserver_change_bits:
.LFB88:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI110:
	ldr	r4, .L518
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L518+4
	ldr	r3, .L518+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L518+12
	bl	nm_ListGetFirst
	b	.L517
.L516:
	cmp	r5, #51
	ldr	r1, [r4, #0]
	add	r0, r6, #156
	bne	.L514
	bl	SHARED_clear_all_32_bit_change_bits
	b	.L515
.L514:
	bl	SHARED_set_all_32_bit_change_bits
.L515:
	ldr	r0, .L518+12
	mov	r1, r6
	bl	nm_ListGetNext
.L517:
	cmp	r0, #0
	mov	r6, r0
	bne	.L516
	ldr	r3, .L518
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L519:
	.align	2
.L518:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3994
	.word	.LANCHOR1
.LFE88:
	.size	STATION_on_all_stations_set_or_clear_commserver_change_bits, .-STATION_on_all_stations_set_or_clear_commserver_change_bits
	.section	.text.STATION_set_no_water_days_by_station,"ax",%progbits
	.align	2
	.global	STATION_set_no_water_days_by_station
	.type	STATION_set_no_water_days_by_station, %function
STATION_set_no_water_days_by_station:
.LFB77:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, lr}
.LCFI111:
	mov	r4, r3
	ldr	r3, .L523
	mov	r6, r2
	mov	r7, r1
	ldr	r2, .L523+4
	mov	r1, #400
	mov	r5, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L523+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r6
	mov	r0, r7
	bl	nm_STATION_get_pointer_to_station
	mov	r6, r0
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L521
	bl	FLOWSENSE_get_controller_index
	mov	r1, r4
	mov	r7, r0
	mov	r0, r6
	bl	STATION_get_change_bits_ptr
	mov	r2, #1
	mov	r1, r5
	mov	r3, r4
	str	r7, [sp, #0]
	str	r2, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r6
	bl	nm_STATION_set_no_water_days
.L521:
	ldr	r3, .L523
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r4, #1
	bne	.L522
	mov	r0, #34
	bl	STATION_on_all_stations_set_or_clear_commserver_change_bits
	ldr	r3, .L523+12
	mov	r1, #2
	str	r4, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L523+16
	ldr	r2, .L523+20
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
.L522:
	ldr	r0, .L523+24
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	postBackground_Calculation_Event
.L524:
	.align	2
.L523:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3490
	.word	weather_preserves
	.word	cics
	.word	6000
	.word	4369
.LFE77:
	.size	STATION_set_no_water_days_by_station, .-STATION_set_no_water_days_by_station
	.section	.text.STATION_set_no_water_days_by_box,"ax",%progbits
	.align	2
	.global	STATION_set_no_water_days_by_box
	.type	STATION_set_no_water_days_by_box, %function
STATION_set_no_water_days_by_box:
.LFB76:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L530
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI112:
	mov	r6, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L530+4
	mov	r5, r2
	ldr	r2, .L530+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L530+12
	bl	nm_ListGetFirst
	mov	r8, #1
	mov	r4, r0
	b	.L526
.L528:
	mov	r0, r4
	bl	nm_STATION_get_box_index_0
	cmp	r0, r7
	bne	.L527
	mov	r0, r4
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L527
	bl	FLOWSENSE_get_controller_index
	mov	r1, r5
	mov	sl, r0
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r1, r6
	mov	r2, #0
	mov	r3, r5
	str	sl, [sp, #0]
	str	r8, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_no_water_days
.L527:
	mov	r1, r4
	ldr	r0, .L530+12
	bl	nm_ListGetNext
	mov	r4, r0
.L526:
	cmp	r4, #0
	bne	.L528
	ldr	r3, .L530
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r7
	mov	r1, r6
	bl	Alert_set_no_water_days_by_box
	cmp	r5, #1
	bne	.L529
	mov	r0, #34
	bl	STATION_on_all_stations_set_or_clear_commserver_change_bits
	ldr	r3, .L530+16
	mov	r1, #2
	str	r5, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L530+20
	ldr	r2, .L530+24
	ldr	r0, [r3, #152]
	mov	r3, r4
	bl	xTimerGenericCommand
.L529:
	ldr	r0, .L530+28
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	postBackground_Calculation_Event
.L531:
	.align	2
.L530:
	.word	list_program_data_recursive_MUTEX
	.word	3430
	.word	.LC9
	.word	.LANCHOR1
	.word	weather_preserves
	.word	cics
	.word	6000
	.word	4369
.LFE76:
	.size	STATION_set_no_water_days_by_box, .-STATION_set_no_water_days_by_box
	.section	.text.STATION_set_no_water_days_by_group,"ax",%progbits
	.align	2
	.global	STATION_set_no_water_days_by_group
	.type	STATION_set_no_water_days_by_group, %function
STATION_set_no_water_days_by_group:
.LFB75:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L540
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI113:
	mov	r7, r0
	sub	sp, sp, #60
.LCFI114:
	ldr	r0, [r3, #0]
	mov	r6, r1
	ldr	r3, .L540+4
	mov	r1, #400
	mov	r5, r2
	ldr	r2, .L540+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L540+12
	bl	nm_ListGetFirst
	mov	r8, #1
	mov	r4, r0
	b	.L533
.L535:
	mov	r0, r4
	bl	STATION_get_GID_station_group
	cmp	r0, r6
	bne	.L534
	mov	r0, r4
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L534
	bl	FLOWSENSE_get_controller_index
	mov	r1, r5
	mov	sl, r0
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r1, r7
	mov	r2, #0
	mov	r3, r5
	str	sl, [sp, #0]
	str	r8, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_no_water_days
.L534:
	mov	r1, r4
	ldr	r0, .L540+12
	bl	nm_ListGetNext
	mov	r4, r0
.L533:
	cmp	r4, #0
	bne	.L535
	mov	r0, r6
	bl	STATION_GROUP_get_group_with_this_GID
	cmp	r0, #0
	addeq	r0, sp, #12
	ldreq	r1, .L540+16
	beq	.L539
	bl	nm_GROUP_get_name
	mov	r1, r0
	add	r0, sp, #12
.L539:
	mov	r2, #48
	bl	strlcpy
	ldr	r3, .L540
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	r0, sp, #12
	mov	r1, r6
	mov	r2, r7
	bl	Alert_set_no_water_days_by_group
	cmp	r5, #1
	bne	.L538
	mov	r0, #34
	bl	STATION_on_all_stations_set_or_clear_commserver_change_bits
	ldr	r3, .L540+20
	mov	r1, #2
	str	r5, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L540+24
	ldr	r2, .L540+28
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
.L538:
	ldr	r0, .L540+32
	bl	postBackground_Calculation_Event
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L541:
	.align	2
.L540:
	.word	list_program_data_recursive_MUTEX
	.word	3355
	.word	.LC9
	.word	.LANCHOR1
	.word	.LC30
	.word	weather_preserves
	.word	cics
	.word	6000
	.word	4369
.LFE75:
	.size	STATION_set_no_water_days_by_group, .-STATION_set_no_water_days_by_group
	.section	.text.STATION_set_no_water_days_for_all_stations,"ax",%progbits
	.align	2
	.global	STATION_set_no_water_days_for_all_stations
	.type	STATION_set_no_water_days_for_all_stations, %function
STATION_set_no_water_days_for_all_stations:
.LFB74:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L547
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, lr}
.LCFI115:
	ldr	r2, .L547+4
	mov	r6, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L547+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L547+12
	bl	nm_ListGetFirst
	mov	r7, #1
	mov	r4, r0
	b	.L543
.L545:
	mov	r0, r4
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L544
	bl	FLOWSENSE_get_controller_index
	mov	r1, r5
	mov	r8, r0
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r1, r6
	mov	r2, #0
	mov	r3, r5
	str	r8, [sp, #0]
	str	r7, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_no_water_days
.L544:
	mov	r1, r4
	ldr	r0, .L547+12
	bl	nm_ListGetNext
	mov	r4, r0
.L543:
	cmp	r4, #0
	bne	.L545
	ldr	r3, .L547
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	bl	Alert_set_no_water_days_all_stations
	cmp	r5, #1
	bne	.L546
	mov	r0, #34
	bl	STATION_on_all_stations_set_or_clear_commserver_change_bits
	ldr	r3, .L547+16
	mov	r1, #2
	str	r5, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L547+20
	ldr	r2, .L547+24
	ldr	r0, [r3, #152]
	mov	r3, r4
	bl	xTimerGenericCommand
.L546:
	ldr	r0, .L547+28
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	postBackground_Calculation_Event
.L548:
	.align	2
.L547:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3295
	.word	.LANCHOR1
	.word	weather_preserves
	.word	cics
	.word	6000
	.word	4369
.LFE74:
	.size	STATION_set_no_water_days_for_all_stations, .-STATION_set_no_water_days_for_all_stations
	.section	.text.STATION_set_ignore_moisture_balance_at_next_irrigation_by_station,"ax",%progbits
	.align	2
	.global	STATION_set_ignore_moisture_balance_at_next_irrigation_by_station
	.type	STATION_set_ignore_moisture_balance_at_next_irrigation_by_station, %function
STATION_set_ignore_moisture_balance_at_next_irrigation_by_station:
.LFB72:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L552
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI116:
	mov	r5, r1
	mov	r6, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r4, r2
	ldr	r3, .L552+4
	ldr	r2, .L552+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r0, r6
	bl	nm_STATION_get_pointer_to_station
	mov	r5, r0
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L550
	bl	FLOWSENSE_get_controller_index
	mov	r1, r4
	mov	r6, r0
	mov	r0, r5
	bl	STATION_get_change_bits_ptr
	mov	r1, #1
	mov	r2, r1
	mov	r3, r4
	str	r6, [sp, #0]
	str	r1, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r5
	bl	nm_STATION_set_ignore_moisture_balance_at_next_irrigation
.L550:
	ldr	r3, .L552
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r4, #1
	bne	.L549
	mov	r0, #34
	bl	STATION_on_all_stations_set_or_clear_commserver_change_bits
	ldr	r3, .L552+12
	mov	r1, #2
	str	r4, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L552+16
	ldr	r2, .L552+20
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
.L549:
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, pc}
.L553:
	.align	2
.L552:
	.word	list_program_data_recursive_MUTEX
	.word	3221
	.word	.LC9
	.word	weather_preserves
	.word	cics
	.word	6000
.LFE72:
	.size	STATION_set_ignore_moisture_balance_at_next_irrigation_by_station, .-STATION_set_ignore_moisture_balance_at_next_irrigation_by_station
	.section	.text.STATION_set_ignore_moisture_balance_at_next_irrigation_by_group,"ax",%progbits
	.align	2
	.global	STATION_set_ignore_moisture_balance_at_next_irrigation_by_group
	.type	STATION_set_ignore_moisture_balance_at_next_irrigation_by_group, %function
STATION_set_ignore_moisture_balance_at_next_irrigation_by_group:
.LFB71:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L562
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI117:
	ldr	r2, .L562+4
	sub	sp, sp, #60
.LCFI118:
	mov	r6, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L562+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L562+12
	bl	nm_ListGetFirst
	mov	r7, #1
	mov	r4, r0
	b	.L555
.L557:
	mov	r0, r4
	bl	STATION_get_GID_station_group
	cmp	r0, r6
	bne	.L556
	mov	r0, r4
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L556
	bl	FLOWSENSE_get_controller_index
	mov	r1, r5
	mov	r8, r0
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r1, #1
	mov	r2, #0
	mov	r3, r5
	str	r8, [sp, #0]
	str	r7, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_ignore_moisture_balance_at_next_irrigation
.L556:
	mov	r1, r4
	ldr	r0, .L562+12
	bl	nm_ListGetNext
	mov	r4, r0
.L555:
	cmp	r4, #0
	bne	.L557
	mov	r0, r6
	bl	STATION_GROUP_get_group_with_this_GID
	cmp	r0, #0
	addeq	r0, sp, #12
	ldreq	r1, .L562+16
	beq	.L561
	bl	nm_GROUP_get_name
	mov	r1, r0
	add	r0, sp, #12
.L561:
	mov	r2, #48
	bl	strlcpy
	ldr	r3, .L562
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	r0, sp, #12
	mov	r1, r6
	bl	Alert_reset_moisture_balance_by_group
	cmp	r5, #1
	bne	.L554
	mov	r0, #34
	bl	STATION_on_all_stations_set_or_clear_commserver_change_bits
	ldr	r3, .L562+20
	mov	r1, #2
	str	r5, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L562+24
	ldr	r2, .L562+28
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
.L554:
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L563:
	.align	2
.L562:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	3156
	.word	.LANCHOR1
	.word	.LC30
	.word	weather_preserves
	.word	cics
	.word	6000
.LFE71:
	.size	STATION_set_ignore_moisture_balance_at_next_irrigation_by_group, .-STATION_set_ignore_moisture_balance_at_next_irrigation_by_group
	.section	.text.STATION_set_ignore_moisture_balance_at_next_irrigation_all_stations,"ax",%progbits
	.align	2
	.global	STATION_set_ignore_moisture_balance_at_next_irrigation_all_stations
	.type	STATION_set_ignore_moisture_balance_at_next_irrigation_all_stations, %function
STATION_set_ignore_moisture_balance_at_next_irrigation_all_stations:
.LFB70:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L569
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, lr}
.LCFI119:
	ldr	r2, .L569+4
	mov	r1, #400
	mov	r5, r0
	ldr	r0, [r3, #0]
	mov	r3, #3104
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L569+8
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r4, r0
	b	.L565
.L567:
	mov	r0, r4
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L566
	bl	FLOWSENSE_get_controller_index
	mov	r1, r5
	mov	r7, r0
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r1, #1
	mov	r2, #0
	mov	r3, r5
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_ignore_moisture_balance_at_next_irrigation
.L566:
	mov	r1, r4
	ldr	r0, .L569+8
	bl	nm_ListGetNext
	mov	r4, r0
.L565:
	cmp	r4, #0
	bne	.L567
	ldr	r3, .L569
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	Alert_reset_moisture_balance_all_stations
	cmp	r5, #1
	bne	.L564
	mov	r0, #34
	bl	STATION_on_all_stations_set_or_clear_commserver_change_bits
	ldr	r3, .L569+12
	mov	r1, #2
	str	r5, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L569+16
	ldr	r2, .L569+20
	ldr	r0, [r3, #152]
	mov	r3, r4
	bl	xTimerGenericCommand
.L564:
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, pc}
.L570:
	.align	2
.L569:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	.LANCHOR1
	.word	weather_preserves
	.word	cics
	.word	6000
.LFE70:
	.size	STATION_set_ignore_moisture_balance_at_next_irrigation_all_stations, .-STATION_set_ignore_moisture_balance_at_next_irrigation_all_stations
	.section	.text.nm_STATION_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_STATION_update_pending_change_bits
	.type	nm_STATION_update_pending_change_bits, %function
nm_STATION_update_pending_change_bits:
.LFB89:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI120:
	ldr	r5, .L578
	mov	r1, #400
	ldr	r2, .L578+4
	mov	r3, #4032
	mov	r7, r0
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L578+8
	bl	nm_ListGetFirst
	mov	r8, #0
	mov	r6, #1
	mov	r4, r0
	b	.L572
.L575:
	ldr	r3, [r4, #164]
	cmp	r3, #0
	beq	.L573
	cmp	r7, #0
	beq	.L574
	ldr	r2, [r4, #156]
	mov	r1, #2
	orr	r3, r2, r3
	str	r3, [r4, #156]
	ldr	r3, .L578+12
	ldr	r2, .L578+16
	str	r6, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L578+20
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L577
.L574:
	add	r0, r4, #164
	ldr	r1, [r5, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L577:
	mov	r8, #1
.L573:
	mov	r1, r4
	ldr	r0, .L578+8
	bl	nm_ListGetNext
	mov	r4, r0
.L572:
	cmp	r4, #0
	bne	.L575
	ldr	r3, .L578
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r8, #0
	beq	.L571
	mov	r0, #9
	mov	r1, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L571:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L579:
	.align	2
.L578:
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	.LANCHOR1
	.word	weather_preserves
	.word	60000
	.word	cics
.LFE89:
	.size	nm_STATION_update_pending_change_bits, .-nm_STATION_update_pending_change_bits
	.section	.text.STATIONS_load_all_the_stations_for_ftimes,"ax",%progbits
	.align	2
	.global	STATIONS_load_all_the_stations_for_ftimes
	.type	STATIONS_load_all_the_stations_for_ftimes, %function
STATIONS_load_all_the_stations_for_ftimes:
.LFB90:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L587
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI121:
	ldr	r4, .L587+4
	sub	sp, sp, #28
.LCFI122:
	str	r3, [sp, #20]	@ float
	ldr	r3, .L587+8
	str	r3, [sp, #24]	@ float
	ldr	r3, [r4, #8]
	cmp	r3, #768
	bcc	.L581
	ldr	r0, .L587+12
	bl	Alert_Message
	b	.L580
.L581:
	ldr	r3, .L587+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L587+20
	ldr	r3, .L587+24
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	nm_ListGetFirst
	ldr	sl, .L587+28
	mov	r8, #0
	mov	r6, sl
	ldr	r9, .L587+32
	mov	r4, r0
	b	.L583
.L586:
	ldr	r3, [r4, #72]
	cmp	r3, #0
	beq	.L584
	ldr	r3, [r4, #76]
	cmp	r3, #0
	beq	.L584
	ldr	r0, [r4, #136]
	bl	FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID
	mov	r5, #112
	mla	r5, r8, r5, sl
	str	r0, [r5, #32]
	ldr	r0, [r0, #180]
	bl	FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID
	cmp	r0, #0
	str	r0, [r5, #28]
	beq	.L584
	ldr	r3, [r5, #32]
	cmp	r3, #0
	bne	.L585
	ldr	r3, [r4, #140]
	cmp	r3, #0
	bne	.L585
	ldr	r3, [r4, #144]
	cmp	r3, #0
	beq	.L584
.L585:
	mov	r7, #112
	mul	r7, r8, r7
	mov	r1, #0
	ldrb	r3, [r6, r7]	@ zero_extendqisi2
	add	r5, r6, r7
	orr	r3, r3, #1
	strb	r3, [r6, r7]
	ldr	r3, [r4, #80]
	add	r0, r7, #48
	str	r3, [r5, #44]
	ldr	r3, [r4, #84]
	mov	r2, #11
	str	r3, [r5, #48]
	ldr	r3, .L587+20
	add	r0, r6, r0
	str	r3, [sp, #8]
	ldr	r3, .L587+36
	stmia	sp, {r1, r9}
	str	r3, [sp, #12]
	mov	r3, r1
	bl	RC_uint32_with_filename
	flds	s12, [r4, #96]	@ int
	flds	s15, [sp, #24]
	flds	s14, [sp, #20]
	ldr	r1, [r4, #80]
	fuitos	s13, s12
	flds	s12, [r4, #100]	@ int
	add	r2, sp, #16
	ldr	r0, [r4, #84]
	add	fp, r5, #104
	add	r8, r8, #1
	fmuls	s15, s13, s15
	fuitos	s13, s12
	fdivs	s15, s15, s14
	ftouizs	s15, s15
	fsts	s15, [r5, #52]	@ int
	flds	s15, [sp, #24]
	flds	s14, [sp, #20]
	fmuls	s15, s13, s15
	flds	s13, [r4, #104]	@ int
	fdivs	s15, s15, s14
	fuitos	s14, s13
	ftouizs	s15, s15
	fsts	s15, [r5, #68]	@ int
	flds	s15, [sp, #24]
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r5, #76]	@ int
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	ldr	r2, [sp, #16]
	ldr	r3, .L587+40
	ldr	r0, [r4, #140]
	add	r3, r3, r2, asl #7
	ldrh	r3, [r3, #136]
	str	r3, [r5, #56]
	ldr	r3, [r4, #108]
	str	r3, [r5, #88]
	ldr	r3, [r4, #116]
	str	r3, [r5, #92]
	ldr	r3, [r4, #112]
	strh	r3, [r5, #102]	@ movhi
	bl	FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID
	str	r0, [r5, #36]
	ldr	r0, [r4, #144]
	bl	FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID
	ldr	r3, [r5, #32]
	ldr	r2, [r3, #160]
	ldrb	r3, [r5, #105]	@ zero_extendqisi2
	and	r2, r2, #1
	bic	r3, r3, #8
	orr	r3, r3, r2, asl #3
	strb	r3, [r5, #105]
	ldr	r3, [r5, #32]
	ldr	r2, [r3, #68]
	ldrb	r3, [r5, #104]	@ zero_extendqisi2
	and	r2, r2, #3
	bic	r3, r3, #48
	orr	r3, r3, r2, asl #4
	strb	r3, [r5, #104]
	ldr	r3, [r5, #32]
	ldr	r2, [r3, #172]
	ldrb	r3, [r5, #105]	@ zero_extendqisi2
	and	r2, r2, #3
	bic	r3, r3, #192
	orr	r3, r3, r2, asl #6
	strb	r3, [r5, #105]
	ldr	r3, [r5, #32]
	str	r0, [r5, #40]
	ldr	r2, [r3, #176]
	ldrb	r3, [r5, #106]	@ zero_extendqisi2
	and	r2, r2, #3
	bic	r3, r3, #3
	orr	r3, r2, r3
	add	r0, r7, #104
	strb	r3, [r5, #106]
	add	r0, r6, r0
	bl	FOAL_IRRI_translate_alert_actions_to_flow_checking_group
	ldrb	r3, [r5, #106]	@ zero_extendqisi2
	bic	r3, r3, #12
	and	r0, r0, #3
	orr	r3, r3, r0, asl #2
	strb	r3, [r5, #106]
.L584:
	mov	r1, r4
	ldr	r0, .L587+4
	bl	nm_ListGetNext
	mov	r4, r0
.L583:
	cmp	r4, #0
	bne	.L586
	ldr	r3, .L587+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L580:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L588:
	.align	2
.L587:
	.word	1092616192
	.word	.LANCHOR1
	.word	1114636288
	.word	.LC31
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	4112
	.word	ft_stations
	.word	.LC32
	.word	4147
	.word	station_preserves
.LFE90:
	.size	STATIONS_load_all_the_stations_for_ftimes, .-STATIONS_load_all_the_stations_for_ftimes
	.section	.text.STATION_calculate_chain_sync_crc,"ax",%progbits
	.align	2
	.global	STATION_calculate_chain_sync_crc
	.type	STATION_calculate_chain_sync_crc, %function
STATION_calculate_chain_sync_crc:
.LFB91:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L594
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI123:
	ldr	r5, .L594+4
	mov	r1, #400
	ldr	r2, .L594+8
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L594+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #8]
	mov	r0, #180
	mul	r0, r3, r0
	mov	r1, sp
	ldr	r2, .L594+8
	ldr	r3, .L594+16
	bl	mem_oabia
	subs	r6, r0, #0
	beq	.L590
	ldr	r3, [sp, #0]
	add	r6, sp, #8
	mov	r0, r5
	str	r3, [r6, #-4]!
	bl	nm_ListGetFirst
	mov	r7, #0
	mov	r5, r0
	b	.L591
.L592:
	add	r8, r5, #20
	mov	r0, r8
	bl	strlen
	mov	r1, r8
	mov	r2, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #72
	mov	r2, #4
	mov	r8, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #76
	mov	r2, #4
	add	r0, r8, r0
	add	r7, r0, r7
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #80
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #84
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #88
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #92
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #96
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #100
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #104
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #108
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #112
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #116
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #120
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #136
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #140
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #144
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #160
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #168
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r1, r5
	add	r7, r7, r0
	ldr	r0, .L594+4
	bl	nm_ListGetNext
	mov	r5, r0
.L591:
	cmp	r5, #0
	bne	.L592
	mov	r1, r7
	ldr	r0, [sp, #0]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L594+20
	add	r4, r4, #43
	ldr	r1, .L594+8
	ldr	r2, .L594+24
	mov	r6, #1
	str	r0, [r3, r4, asl #2]
	ldr	r0, [sp, #0]
	bl	mem_free_debug
	b	.L593
.L590:
	ldr	r3, .L594+28
	ldr	r0, .L594+32
	ldr	r1, [r3, r4, asl #4]
	bl	Alert_Message_va
.L593:
	ldr	r3, .L594
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L595:
	.align	2
.L594:
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR1
	.word	.LC9
	.word	4251
	.word	4263
	.word	cscs
	.word	4354
	.word	chain_sync_file_pertinants
	.word	.LC33
.LFE91:
	.size	STATION_calculate_chain_sync_crc, .-STATION_calculate_chain_sync_crc
	.global	station_info_list_hdr
	.global	station_info_list_item_sizes
	.section	.bss.station_info_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	station_info_list_hdr, %object
	.size	station_info_list_hdr, 20
station_info_list_hdr:
	.space	20
	.section	.rodata.STATION_INFO_FILENAME,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	STATION_INFO_FILENAME, %object
	.size	STATION_INFO_FILENAME, 20
STATION_INFO_FILENAME:
	.ascii	"STATION INFORMATION\000"
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"BoxIndex\000"
.LC1:
	.ascii	"StationNumber\000"
.LC2:
	.ascii	"ET_Factor\000"
.LC3:
	.ascii	"DistributionUniformity\000"
.LC4:
	.ascii	"SquareFootage\000"
.LC5:
	.ascii	"NoWaterDays\000"
.LC6:
	.ascii	"TotalMinutesRun\000"
.LC7:
	.ascii	"DecoderOutput\000"
.LC8:
	.ascii	"DecoderSerialNumber\000"
.LC9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/stations.c\000"
.LC10:
	.ascii	"StationGroupID\000"
.LC11:
	.ascii	"InUse\000"
.LC12:
	.ascii	"CardConnected\000"
.LC13:
	.ascii	"MoistureBalance\000"
.LC14:
	.ascii	"ManualProgramB_ID\000"
.LC15:
	.ascii	"ManualProgramA_ID\000"
.LC16:
	.ascii	"ExpectedFlowRate\000"
.LC17:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/shared_stations.c\000"
.LC18:
	.ascii	"ClearAccumulatedRainFlag\000"
.LC19:
	.ascii	"pstation\000"
.LC20:
	.ascii	"station_info_list_hdr\000"
.LC21:
	.ascii	"CycleMinutes\000"
.LC22:
	.ascii	"CYCLE TIME: special range - from %u to %u\000"
.LC23:
	.ascii	"SoakMinutes\000"
.LC24:
	.ascii	"(description not yet set)\000"
.LC25:
	.ascii	"STATIONS file unexpd update %u\000"
.LC26:
	.ascii	"STATIONS file update : to revision %u from %u\000"
.LC27:
	.ascii	"STATIONS updater error\000"
.LC28:
	.ascii	"NULL pointer parameter : %s, %u\000"
.LC29:
	.ascii	"No stations available\000"
.LC30:
	.ascii	"<Unknown Group>\000"
.LC31:
	.ascii	"FTIMES ERROR: too many stations in network!\000"
.LC32:
	.ascii	"Box Index\000"
.LC33:
	.ascii	"SYNC: no mem to calc checksum %s\000"
	.section	.rodata.STATION_NAME,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	STATION_NAME, %object
	.size	STATION_NAME, 8
STATION_NAME:
	.ascii	"Station\000"
	.section	.rodata.station_info_list_item_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	station_info_list_item_sizes, %object
	.size	station_info_list_item_sizes, 32
station_info_list_item_sizes:
	.word	160
	.word	160
	.word	164
	.word	168
	.word	172
	.word	180
	.word	180
	.word	180
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI0-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI4-.LFB10
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI6-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI8-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI10-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI12-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI14-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI16-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.byte	0x4
	.4byte	.LCFI18-.LFB63
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI19-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI20-.LCFI19
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI21-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI23-.LFB17
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI25-.LFB16
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI26-.LCFI25
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI27-.LFB15
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI29-.LFB14
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI31-.LFB11
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI32-.LCFI31
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI33-.LFB19
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI35-.LFB23
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI36-.LFB24
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI37-.LFB28
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI38-.LCFI37
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI39-.LFB31
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI40-.LFB33
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI41-.LFB38
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI42-.LFB41
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI43-.LFB43
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.byte	0x4
	.4byte	.LCFI44-.LFB45
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.byte	0x4
	.4byte	.LCFI45-.LFB44
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI46-.LFB42
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI47-.LFB40
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI48-.LFB39
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.byte	0x4
	.4byte	.LCFI49-.LFB46
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.byte	0x4
	.4byte	.LCFI50-.LFB47
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.byte	0x4
	.4byte	.LCFI51-.LFB48
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.byte	0x4
	.4byte	.LCFI52-.LFB49
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.byte	0x4
	.4byte	.LCFI53-.LFB50
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.byte	0x4
	.4byte	.LCFI54-.LFB51
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.byte	0x4
	.4byte	.LCFI55-.LFB52
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI56-.LFB8
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI57-.LCFI56
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.byte	0x4
	.4byte	.LCFI58-.LFB53
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.byte	0x4
	.4byte	.LCFI59-.LFB55
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.byte	0x4
	.4byte	.LCFI60-.LFB56
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.byte	0x4
	.4byte	.LCFI61-.LFB57
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.byte	0x4
	.4byte	.LCFI62-.LFB54
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE86:
.LSFDE88:
	.4byte	.LEFDE88-.LASFDE88
.LASFDE88:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI63-.LFB9
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI64-.LCFI63
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE88:
.LSFDE90:
	.4byte	.LEFDE90-.LASFDE90
.LASFDE90:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI65-.LFB25
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI66-.LCFI65
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE90:
.LSFDE92:
	.4byte	.LEFDE92-.LASFDE92
.LASFDE92:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI67-.LFB22
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI68-.LCFI67
	.byte	0xe
	.uleb128 0x1c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI69-.LCFI68
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE92:
.LSFDE94:
	.4byte	.LEFDE94-.LASFDE94
.LASFDE94:
	.4byte	.Lframe0
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.byte	0x4
	.4byte	.LCFI70-.LFB58
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE94:
.LSFDE96:
	.4byte	.LEFDE96-.LASFDE96
.LASFDE96:
	.4byte	.Lframe0
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.byte	0x4
	.4byte	.LCFI71-.LFB61
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE96:
.LSFDE98:
	.4byte	.LEFDE98-.LASFDE98
.LASFDE98:
	.4byte	.Lframe0
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.byte	0x4
	.4byte	.LCFI72-.LFB62
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE98:
.LSFDE100:
	.4byte	.LEFDE100-.LASFDE100
.LASFDE100:
	.4byte	.Lframe0
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.byte	0x4
	.4byte	.LCFI73-.LFB64
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE100:
.LSFDE102:
	.4byte	.LEFDE102-.LASFDE102
.LASFDE102:
	.4byte	.Lframe0
	.4byte	.LFB65
	.4byte	.LFE65-.LFB65
	.byte	0x4
	.4byte	.LCFI74-.LFB65
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE102:
.LSFDE104:
	.4byte	.LEFDE104-.LASFDE104
.LASFDE104:
	.4byte	.Lframe0
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.byte	0x4
	.4byte	.LCFI75-.LFB66
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE104:
.LSFDE106:
	.4byte	.LEFDE106-.LASFDE106
.LASFDE106:
	.4byte	.Lframe0
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.byte	0x4
	.4byte	.LCFI76-.LFB67
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE106:
.LSFDE108:
	.4byte	.LEFDE108-.LASFDE108
.LASFDE108:
	.4byte	.Lframe0
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.byte	0x4
	.4byte	.LCFI77-.LFB68
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI78-.LCFI77
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.align	2
.LEFDE108:
.LSFDE110:
	.4byte	.LEFDE110-.LASFDE110
.LASFDE110:
	.4byte	.Lframe0
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.byte	0x4
	.4byte	.LCFI79-.LFB69
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE110:
.LSFDE112:
	.4byte	.LEFDE112-.LASFDE112
.LASFDE112:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI80-.LFB30
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI81-.LCFI80
	.byte	0xe
	.uleb128 0x18
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI82-.LCFI81
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE112:
.LSFDE114:
	.4byte	.LEFDE114-.LASFDE114
.LASFDE114:
	.4byte	.Lframe0
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.byte	0x4
	.4byte	.LCFI83-.LFB73
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE114:
.LSFDE116:
	.4byte	.LEFDE116-.LASFDE116
.LASFDE116:
	.4byte	.Lframe0
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.align	2
.LEFDE116:
.LSFDE118:
	.4byte	.LEFDE118-.LASFDE118
.LASFDE118:
	.4byte	.Lframe0
	.4byte	.LFB79
	.4byte	.LFE79-.LFB79
	.align	2
.LEFDE118:
.LSFDE120:
	.4byte	.LEFDE120-.LASFDE120
.LASFDE120:
	.4byte	.Lframe0
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.byte	0x4
	.4byte	.LCFI84-.LFB59
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE120:
.LSFDE122:
	.4byte	.LEFDE122-.LASFDE122
.LASFDE122:
	.4byte	.Lframe0
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.byte	0x4
	.4byte	.LCFI85-.LFB60
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE122:
.LSFDE124:
	.4byte	.LEFDE124-.LASFDE124
.LASFDE124:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI86-.LFB37
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x82
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE124:
.LSFDE126:
	.4byte	.LEFDE126-.LASFDE126
.LASFDE126:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI87-.LFB36
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x82
	.uleb128 0xa
	.byte	0x81
	.uleb128 0xb
	.byte	0x80
	.uleb128 0xc
	.align	2
.LEFDE126:
.LSFDE128:
	.4byte	.LEFDE128-.LASFDE128
.LASFDE128:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI88-.LFB34
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI89-.LCFI88
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE128:
.LSFDE130:
	.4byte	.LEFDE130-.LASFDE130
.LASFDE130:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI90-.LFB35
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI91-.LCFI90
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE130:
.LSFDE132:
	.4byte	.LEFDE132-.LASFDE132
.LASFDE132:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI92-.LFB27
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI93-.LCFI92
	.byte	0xe
	.uleb128 0x6c
	.align	2
.LEFDE132:
.LSFDE134:
	.4byte	.LEFDE134-.LASFDE134
.LASFDE134:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI94-.LFB26
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE134:
.LSFDE136:
	.4byte	.LEFDE136-.LASFDE136
.LASFDE136:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI95-.LFB20
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI96-.LCFI95
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE136:
.LSFDE138:
	.4byte	.LEFDE138-.LASFDE138
.LASFDE138:
	.4byte	.Lframe0
	.4byte	.LFB80
	.4byte	.LFE80-.LFB80
	.byte	0x4
	.4byte	.LCFI97-.LFB80
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE138:
.LSFDE140:
	.4byte	.LEFDE140-.LASFDE140
.LASFDE140:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI98-.LFB32
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI99-.LCFI98
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE140:
.LSFDE142:
	.4byte	.LEFDE142-.LASFDE142
.LASFDE142:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI100-.LFB29
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE142:
.LSFDE144:
	.4byte	.LEFDE144-.LASFDE144
.LASFDE144:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI101-.LFB21
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI102-.LCFI101
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE144:
.LSFDE146:
	.4byte	.LEFDE146-.LASFDE146
.LASFDE146:
	.4byte	.Lframe0
	.4byte	.LFB81
	.4byte	.LFE81-.LFB81
	.byte	0x4
	.4byte	.LCFI103-.LFB81
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE146:
.LSFDE148:
	.4byte	.LEFDE148-.LASFDE148
.LASFDE148:
	.4byte	.Lframe0
	.4byte	.LFB82
	.4byte	.LFE82-.LFB82
	.byte	0x4
	.4byte	.LCFI104-.LFB82
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE148:
.LSFDE150:
	.4byte	.LEFDE150-.LASFDE150
.LASFDE150:
	.4byte	.Lframe0
	.4byte	.LFB83
	.4byte	.LFE83-.LFB83
	.byte	0x4
	.4byte	.LCFI105-.LFB83
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE150:
.LSFDE152:
	.4byte	.LEFDE152-.LASFDE152
.LASFDE152:
	.4byte	.Lframe0
	.4byte	.LFB84
	.4byte	.LFE84-.LFB84
	.byte	0x4
	.4byte	.LCFI106-.LFB84
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE152:
.LSFDE154:
	.4byte	.LEFDE154-.LASFDE154
.LASFDE154:
	.4byte	.Lframe0
	.4byte	.LFB85
	.4byte	.LFE85-.LFB85
	.byte	0x4
	.4byte	.LCFI107-.LFB85
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE154:
.LSFDE156:
	.4byte	.LEFDE156-.LASFDE156
.LASFDE156:
	.4byte	.Lframe0
	.4byte	.LFB86
	.4byte	.LFE86-.LFB86
	.byte	0x4
	.4byte	.LCFI108-.LFB86
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE156:
.LSFDE158:
	.4byte	.LEFDE158-.LASFDE158
.LASFDE158:
	.4byte	.Lframe0
	.4byte	.LFB87
	.4byte	.LFE87-.LFB87
	.byte	0x4
	.4byte	.LCFI109-.LFB87
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE158:
.LSFDE160:
	.4byte	.LEFDE160-.LASFDE160
.LASFDE160:
	.4byte	.Lframe0
	.4byte	.LFB88
	.4byte	.LFE88-.LFB88
	.byte	0x4
	.4byte	.LCFI110-.LFB88
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE160:
.LSFDE162:
	.4byte	.LEFDE162-.LASFDE162
.LASFDE162:
	.4byte	.Lframe0
	.4byte	.LFB77
	.4byte	.LFE77-.LFB77
	.byte	0x4
	.4byte	.LCFI111-.LFB77
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE162:
.LSFDE164:
	.4byte	.LEFDE164-.LASFDE164
.LASFDE164:
	.4byte	.Lframe0
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.byte	0x4
	.4byte	.LCFI112-.LFB76
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE164:
.LSFDE166:
	.4byte	.LEFDE166-.LASFDE166
.LASFDE166:
	.4byte	.Lframe0
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.byte	0x4
	.4byte	.LCFI113-.LFB75
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI114-.LCFI113
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE166:
.LSFDE168:
	.4byte	.LEFDE168-.LASFDE168
.LASFDE168:
	.4byte	.Lframe0
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.byte	0x4
	.4byte	.LCFI115-.LFB74
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE168:
.LSFDE170:
	.4byte	.LEFDE170-.LASFDE170
.LASFDE170:
	.4byte	.Lframe0
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.byte	0x4
	.4byte	.LCFI116-.LFB72
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE170:
.LSFDE172:
	.4byte	.LEFDE172-.LASFDE172
.LASFDE172:
	.4byte	.Lframe0
	.4byte	.LFB71
	.4byte	.LFE71-.LFB71
	.byte	0x4
	.4byte	.LCFI117-.LFB71
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI118-.LCFI117
	.byte	0xe
	.uleb128 0x54
	.align	2
.LEFDE172:
.LSFDE174:
	.4byte	.LEFDE174-.LASFDE174
.LASFDE174:
	.4byte	.Lframe0
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.byte	0x4
	.4byte	.LCFI119-.LFB70
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE174:
.LSFDE176:
	.4byte	.LEFDE176-.LASFDE176
.LASFDE176:
	.4byte	.Lframe0
	.4byte	.LFB89
	.4byte	.LFE89-.LFB89
	.byte	0x4
	.4byte	.LCFI120-.LFB89
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE176:
.LSFDE178:
	.4byte	.LEFDE178-.LASFDE178
.LASFDE178:
	.4byte	.Lframe0
	.4byte	.LFB90
	.4byte	.LFE90-.LFB90
	.byte	0x4
	.4byte	.LCFI121-.LFB90
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI122-.LCFI121
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE178:
.LSFDE180:
	.4byte	.LEFDE180-.LASFDE180
.LASFDE180:
	.4byte	.Lframe0
	.4byte	.LFB91
	.4byte	.LFE91-.LFB91
	.byte	0x4
	.4byte	.LCFI123-.LFB91
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE180:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/shared_stations.c"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/stations.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x7a2
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF91
	.byte	0x1
	.4byte	.LASF92
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF93
	.byte	0x1
	.2byte	0x129
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x41b
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x2
	.2byte	0x90b
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x220
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x1de
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x3d2
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x487
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x607
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST4
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x4cf
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x2b4
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST6
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x26b
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST7
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x246
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST8
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF11
	.byte	0x2
	.2byte	0xb84
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LLST9
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x19e
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST10
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x158
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST11
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x5e1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST12
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x59c
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST13
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x557
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST14
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x515
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST15
	.uleb128 0x6
	.4byte	0x2a
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST16
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x63a
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST17
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF19
	.byte	0x2
	.2byte	0x225
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST18
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF20
	.byte	0x2
	.2byte	0x23a
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST19
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF21
	.byte	0x2
	.2byte	0x4f1
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST20
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF22
	.byte	0x2
	.2byte	0x5f6
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST21
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF23
	.byte	0x2
	.2byte	0x674
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST22
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF24
	.byte	0x2
	.2byte	0x7c9
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST23
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF25
	.byte	0x2
	.2byte	0x822
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST24
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF26
	.byte	0x2
	.2byte	0x86d
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST25
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF27
	.byte	0x2
	.2byte	0x8e7
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LLST26
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF28
	.byte	0x2
	.2byte	0x8b4
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LLST27
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF29
	.byte	0x2
	.2byte	0x855
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST28
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF30
	.byte	0x2
	.2byte	0x809
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST29
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF31
	.byte	0x2
	.2byte	0x7e9
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST30
	.uleb128 0x6
	.4byte	0x34
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LLST31
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF32
	.byte	0x2
	.2byte	0x937
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LLST32
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF33
	.byte	0x2
	.2byte	0x948
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LLST33
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF34
	.byte	0x2
	.2byte	0x959
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LLST34
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF35
	.byte	0x2
	.2byte	0x980
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LLST35
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF36
	.byte	0x2
	.2byte	0x9ab
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LLST36
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF37
	.byte	0x2
	.2byte	0x9c7
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LLST37
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x2fd
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST38
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF39
	.byte	0x2
	.2byte	0xa0a
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LLST39
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF40
	.byte	0x2
	.2byte	0xa6a
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LLST40
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF41
	.byte	0x2
	.2byte	0xa96
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LLST41
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF42
	.byte	0x2
	.2byte	0xabd
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LLST42
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF43
	.byte	0x2
	.2byte	0xa26
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LLST43
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x370
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST44
	.uleb128 0x4
	.4byte	.LASF45
	.byte	0x2
	.2byte	0x25e
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST45
	.uleb128 0x4
	.4byte	.LASF46
	.byte	0x2
	.2byte	0x146
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST46
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF47
	.byte	0x2
	.2byte	0xae4
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LLST47
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF48
	.byte	0x2
	.2byte	0xb46
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LLST48
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF49
	.byte	0x2
	.2byte	0xb59
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LLST49
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF50
	.byte	0x2
	.2byte	0xb99
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LLST50
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF51
	.byte	0x2
	.2byte	0xbae
	.4byte	.LFB65
	.4byte	.LFE65
	.4byte	.LLST51
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF52
	.byte	0x2
	.2byte	0xbc3
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LLST52
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF53
	.byte	0x2
	.2byte	0xbe2
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	.LLST53
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF54
	.byte	0x2
	.2byte	0xbf7
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LLST54
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF55
	.byte	0x2
	.2byte	0xc05
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LLST55
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF56
	.byte	0x2
	.2byte	0x553
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST56
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF57
	.byte	0x2
	.2byte	0xcb9
	.4byte	.LFB73
	.4byte	.LFE73
	.4byte	.LLST57
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF58
	.byte	0x2
	.2byte	0xdda
	.4byte	.LFB78
	.4byte	.LFE78
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF59
	.byte	0x2
	.2byte	0xde0
	.4byte	.LFB79
	.4byte	.LFE79
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF60
	.byte	0x2
	.2byte	0xaf9
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LLST58
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF61
	.byte	0x2
	.2byte	0xb17
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LLST59
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF62
	.byte	0x2
	.2byte	0x79c
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST60
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF63
	.byte	0x2
	.2byte	0x751
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST61
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF64
	.byte	0x2
	.2byte	0x69f
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST62
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF65
	.byte	0x2
	.2byte	0x70f
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST63
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF66
	.byte	0x2
	.2byte	0x31d
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST64
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF67
	.byte	0x2
	.2byte	0x2d5
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST65
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF68
	.byte	0x1
	.2byte	0x688
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST66
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF69
	.byte	0x2
	.2byte	0xdfc
	.4byte	.LFB80
	.4byte	.LFE80
	.4byte	.LLST67
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF70
	.byte	0x2
	.2byte	0x61e
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST68
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF71
	.byte	0x2
	.2byte	0x519
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST69
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF72
	.byte	0x1
	.2byte	0x79a
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST70
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF73
	.byte	0x2
	.2byte	0xe18
	.4byte	.LFB81
	.4byte	.LFE81
	.4byte	.LLST71
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF74
	.byte	0x2
	.2byte	0xe4f
	.4byte	.LFB82
	.4byte	.LFE82
	.4byte	.LLST72
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF75
	.byte	0x2
	.2byte	0xe97
	.4byte	.LFB83
	.4byte	.LFE83
	.4byte	.LLST73
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF76
	.byte	0x2
	.2byte	0xeca
	.4byte	.LFB84
	.4byte	.LFE84
	.4byte	.LLST74
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF77
	.byte	0x2
	.2byte	0xef3
	.4byte	.LFB85
	.4byte	.LFE85
	.4byte	.LLST75
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF78
	.byte	0x2
	.2byte	0xf22
	.4byte	.LFB86
	.4byte	.LFE86
	.4byte	.LLST76
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF79
	.byte	0x2
	.2byte	0xf72
	.4byte	.LFB87
	.4byte	.LFE87
	.4byte	.LLST77
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF80
	.byte	0x2
	.2byte	0xf96
	.4byte	.LFB88
	.4byte	.LFE88
	.4byte	.LLST78
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF81
	.byte	0x2
	.2byte	0xd9c
	.4byte	.LFB77
	.4byte	.LFE77
	.4byte	.LLST79
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF82
	.byte	0x2
	.2byte	0xd60
	.4byte	.LFB76
	.4byte	.LFE76
	.4byte	.LLST80
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF83
	.byte	0x2
	.2byte	0xd11
	.4byte	.LFB75
	.4byte	.LFE75
	.4byte	.LLST81
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF84
	.byte	0x2
	.2byte	0xcd9
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LLST82
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF85
	.byte	0x2
	.2byte	0xc8f
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LLST83
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF86
	.byte	0x2
	.2byte	0xc4a
	.4byte	.LFB71
	.4byte	.LFE71
	.4byte	.LLST84
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF87
	.byte	0x2
	.2byte	0xc1a
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	.LLST85
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF88
	.byte	0x2
	.2byte	0xfb8
	.4byte	.LFB89
	.4byte	.LFE89
	.4byte	.LLST86
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF89
	.byte	0x2
	.2byte	0xff6
	.4byte	.LFB90
	.4byte	.LFE90
	.4byte	.LLST87
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF90
	.byte	0x2
	.2byte	0x1084
	.4byte	.LFB91
	.4byte	.LFE91
	.4byte	.LLST88
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB4
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB10
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI5
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB12
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB18
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI9
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB13
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI11
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB7
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB6
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI15
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB5
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI17
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB63
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE63
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB2
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI20
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB1
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI22
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB17
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI24
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB16
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI26
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB15
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI28
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB14
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI30
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB11
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI32
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB19
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI34
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB23
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB24
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB28
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI38
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB31
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB33
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB38
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB41
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB43
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB45
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LFE45
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB44
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LFE44
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB42
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB40
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB39
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB46
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI49
	.4byte	.LFE46
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB47
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LFE47
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB48
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LFE48
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB49
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI52
	.4byte	.LFE49
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB50
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LFE50
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB51
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LFE51
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB52
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI55
	.4byte	.LFE52
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB8
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI57
	.4byte	.LFE8
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB53
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI58
	.4byte	.LFE53
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB55
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI59
	.4byte	.LFE55
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB56
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LFE56
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB57
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI61
	.4byte	.LFE57
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB54
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI62
	.4byte	.LFE54
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST44:
	.4byte	.LFB9
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI64
	.4byte	.LFE9
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST45:
	.4byte	.LFB25
	.4byte	.LCFI65
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI65
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI66
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST46:
	.4byte	.LFB22
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI67
	.4byte	.LCFI68
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI68
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI69
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST47:
	.4byte	.LFB58
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI70
	.4byte	.LFE58
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST48:
	.4byte	.LFB61
	.4byte	.LCFI71
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI71
	.4byte	.LFE61
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST49:
	.4byte	.LFB62
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI72
	.4byte	.LFE62
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST50:
	.4byte	.LFB64
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI73
	.4byte	.LFE64
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST51:
	.4byte	.LFB65
	.4byte	.LCFI74
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI74
	.4byte	.LFE65
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST52:
	.4byte	.LFB66
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI75
	.4byte	.LFE66
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST53:
	.4byte	.LFB67
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI76
	.4byte	.LFE67
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST54:
	.4byte	.LFB68
	.4byte	.LCFI77
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI77
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI78
	.4byte	.LFE68
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST55:
	.4byte	.LFB69
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI79
	.4byte	.LFE69
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST56:
	.4byte	.LFB30
	.4byte	.LCFI80
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI80
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI81
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI82
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST57:
	.4byte	.LFB73
	.4byte	.LCFI83
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI83
	.4byte	.LFE73
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST58:
	.4byte	.LFB59
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI84
	.4byte	.LFE59
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST59:
	.4byte	.LFB60
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI85
	.4byte	.LFE60
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST60:
	.4byte	.LFB37
	.4byte	.LCFI86
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI86
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST61:
	.4byte	.LFB36
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI87
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST62:
	.4byte	.LFB34
	.4byte	.LCFI88
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI88
	.4byte	.LCFI89
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI89
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST63:
	.4byte	.LFB35
	.4byte	.LCFI90
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI90
	.4byte	.LCFI91
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI91
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST64:
	.4byte	.LFB27
	.4byte	.LCFI92
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI92
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI93
	.4byte	.LFE27
	.2byte	0x3
	.byte	0x7d
	.sleb128 108
	.4byte	0
	.4byte	0
.LLST65:
	.4byte	.LFB26
	.4byte	.LCFI94
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI94
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST66:
	.4byte	.LFB20
	.4byte	.LCFI95
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI95
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI96
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST67:
	.4byte	.LFB80
	.4byte	.LCFI97
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI97
	.4byte	.LFE80
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST68:
	.4byte	.LFB32
	.4byte	.LCFI98
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI98
	.4byte	.LCFI99
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI99
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST69:
	.4byte	.LFB29
	.4byte	.LCFI100
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI100
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST70:
	.4byte	.LFB21
	.4byte	.LCFI101
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI101
	.4byte	.LCFI102
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI102
	.4byte	.LFE21
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST71:
	.4byte	.LFB81
	.4byte	.LCFI103
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI103
	.4byte	.LFE81
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST72:
	.4byte	.LFB82
	.4byte	.LCFI104
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI104
	.4byte	.LFE82
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST73:
	.4byte	.LFB83
	.4byte	.LCFI105
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI105
	.4byte	.LFE83
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST74:
	.4byte	.LFB84
	.4byte	.LCFI106
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI106
	.4byte	.LFE84
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST75:
	.4byte	.LFB85
	.4byte	.LCFI107
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI107
	.4byte	.LFE85
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST76:
	.4byte	.LFB86
	.4byte	.LCFI108
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI108
	.4byte	.LFE86
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST77:
	.4byte	.LFB87
	.4byte	.LCFI109
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI109
	.4byte	.LFE87
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST78:
	.4byte	.LFB88
	.4byte	.LCFI110
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI110
	.4byte	.LFE88
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST79:
	.4byte	.LFB77
	.4byte	.LCFI111
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI111
	.4byte	.LFE77
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST80:
	.4byte	.LFB76
	.4byte	.LCFI112
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI112
	.4byte	.LFE76
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST81:
	.4byte	.LFB75
	.4byte	.LCFI113
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI113
	.4byte	.LCFI114
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI114
	.4byte	.LFE75
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST82:
	.4byte	.LFB74
	.4byte	.LCFI115
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI115
	.4byte	.LFE74
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST83:
	.4byte	.LFB72
	.4byte	.LCFI116
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI116
	.4byte	.LFE72
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST84:
	.4byte	.LFB71
	.4byte	.LCFI117
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI117
	.4byte	.LCFI118
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI118
	.4byte	.LFE71
	.2byte	0x3
	.byte	0x7d
	.sleb128 84
	.4byte	0
	.4byte	0
.LLST85:
	.4byte	.LFB70
	.4byte	.LCFI119
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI119
	.4byte	.LFE70
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST86:
	.4byte	.LFB89
	.4byte	.LCFI120
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI120
	.4byte	.LFE89
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST87:
	.4byte	.LFB90
	.4byte	.LCFI121
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI121
	.4byte	.LCFI122
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI122
	.4byte	.LFE90
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST88:
	.4byte	.LFB91
	.4byte	.LCFI123
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI123
	.4byte	.LFE91
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2ec
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.4byte	.LFB65
	.4byte	.LFE65-.LFB65
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.4byte	.LFB79
	.4byte	.LFE79-.LFB79
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB80
	.4byte	.LFE80-.LFB80
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB81
	.4byte	.LFE81-.LFB81
	.4byte	.LFB82
	.4byte	.LFE82-.LFB82
	.4byte	.LFB83
	.4byte	.LFE83-.LFB83
	.4byte	.LFB84
	.4byte	.LFE84-.LFB84
	.4byte	.LFB85
	.4byte	.LFE85-.LFB85
	.4byte	.LFB86
	.4byte	.LFE86-.LFB86
	.4byte	.LFB87
	.4byte	.LFE87-.LFB87
	.4byte	.LFB88
	.4byte	.LFE88-.LFB88
	.4byte	.LFB77
	.4byte	.LFE77-.LFB77
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.4byte	.LFB71
	.4byte	.LFE71-.LFB71
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.4byte	.LFB89
	.4byte	.LFE89-.LFB89
	.4byte	.LFB90
	.4byte	.LFE90-.LFB90
	.4byte	.LFB91
	.4byte	.LFE91-.LFB91
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LFB65
	.4byte	.LFE65
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB73
	.4byte	.LFE73
	.4byte	.LFB78
	.4byte	.LFE78
	.4byte	.LFB79
	.4byte	.LFE79
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB80
	.4byte	.LFE80
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB81
	.4byte	.LFE81
	.4byte	.LFB82
	.4byte	.LFE82
	.4byte	.LFB83
	.4byte	.LFE83
	.4byte	.LFB84
	.4byte	.LFE84
	.4byte	.LFB85
	.4byte	.LFE85
	.4byte	.LFB86
	.4byte	.LFE86
	.4byte	.LFB87
	.4byte	.LFE87
	.4byte	.LFB88
	.4byte	.LFE88
	.4byte	.LFB77
	.4byte	.LFE77
	.4byte	.LFB76
	.4byte	.LFE76
	.4byte	.LFB75
	.4byte	.LFE75
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LFB71
	.4byte	.LFE71
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	.LFB89
	.4byte	.LFE89
	.4byte	.LFB90
	.4byte	.LFE90
	.4byte	.LFB91
	.4byte	.LFE91
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF55:
	.ascii	"STATION_get_square_footage\000"
.LASF58:
	.ascii	"nm_STATION_get_ignore_moisture_balance_at_next_irri"
	.ascii	"gation_flag\000"
.LASF37:
	.ascii	"nm_STATION_get_watersense_cycle_max_10u\000"
.LASF56:
	.ascii	"nm_STATION_copy_station_into_guivars\000"
.LASF40:
	.ascii	"STATION_get_ET_factor_100u\000"
.LASF53:
	.ascii	"STATION_get_moisture_balance_percent\000"
.LASF35:
	.ascii	"STATION_get_total_run_minutes_10u\000"
.LASF78:
	.ascii	"STATION_for_stations_on_this_box_set_physically_ava"
	.ascii	"ilable_based_upon_discovery_results\000"
.LASF93:
	.ascii	"nm_STATION_set_description\000"
.LASF86:
	.ascii	"STATION_set_ignore_moisture_balance_at_next_irrigat"
	.ascii	"ion_by_group\000"
.LASF41:
	.ascii	"nm_STATION_get_expected_flow_rate_gpm\000"
.LASF61:
	.ascii	"STATION_chain_down_NOW_days_midnight_maintenance\000"
.LASF65:
	.ascii	"STATION_store_group_assignment_changes\000"
.LASF14:
	.ascii	"nm_STATION_set_moisture_balance_percent\000"
.LASF24:
	.ascii	"STATION_get_index_using_ptr_to_station_struct\000"
.LASF2:
	.ascii	"nm_STATION_set_box_index\000"
.LASF84:
	.ascii	"STATION_set_no_water_days_for_all_stations\000"
.LASF70:
	.ascii	"STATION_extract_and_store_changes_from_GuiVars\000"
.LASF63:
	.ascii	"STATION_store_manual_programs_assignment_changes\000"
.LASF81:
	.ascii	"STATION_set_no_water_days_by_station\000"
.LASF92:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/stations.c\000"
.LASF50:
	.ascii	"STATION_get_GID_manual_program_A\000"
.LASF51:
	.ascii	"STATION_get_GID_manual_program_B\000"
.LASF21:
	.ascii	"nm_STATION_calculate_and_copy_estimated_minutes_int"
	.ascii	"o_GuiVar\000"
.LASF9:
	.ascii	"nm_STATION_set_decoder_output\000"
.LASF38:
	.ascii	"nm_STATION_set_cycle_minutes\000"
.LASF12:
	.ascii	"nm_STATION_set_in_use\000"
.LASF87:
	.ascii	"STATION_set_ignore_moisture_balance_at_next_irrigat"
	.ascii	"ion_all_stations\000"
.LASF7:
	.ascii	"nm_STATION_set_no_water_days\000"
.LASF57:
	.ascii	"nm_STATION_get_Budget_data\000"
.LASF18:
	.ascii	"nm_STATION_set_ignore_moisture_balance_at_next_irri"
	.ascii	"gation\000"
.LASF85:
	.ascii	"STATION_set_ignore_moisture_balance_at_next_irrigat"
	.ascii	"ion_by_station\000"
.LASF83:
	.ascii	"STATION_set_no_water_days_by_group\000"
.LASF0:
	.ascii	"nm_STATION_set_expected_flow_rate\000"
.LASF1:
	.ascii	"nm_STATION_get_station_number_0\000"
.LASF62:
	.ascii	"STATION_store_stations_in_use\000"
.LASF64:
	.ascii	"STATION_adjust_group_settings_after_changing_group_"
	.ascii	"assignment\000"
.LASF10:
	.ascii	"nm_STATION_set_decoder_serial_number\000"
.LASF49:
	.ascii	"nm_STATION_get_number_of_manual_programs_station_is"
	.ascii	"_assigned_to\000"
.LASF19:
	.ascii	"init_file_station_info\000"
.LASF74:
	.ascii	"STATION_get_next_available_station\000"
.LASF44:
	.ascii	"nm_STATION_set_soak_minutes\000"
.LASF73:
	.ascii	"nm_STATION_get_pointer_to_decoder_based_station\000"
.LASF20:
	.ascii	"save_file_station_info\000"
.LASF45:
	.ascii	"nm_STATION_set_default_values_resulting_in_station_"
	.ascii	"0_for_this_box\000"
.LASF66:
	.ascii	"STATION_build_data_to_send\000"
.LASF13:
	.ascii	"nm_STATION_set_physically_available\000"
.LASF33:
	.ascii	"nm_STATION_get_decoder_serial_number\000"
.LASF4:
	.ascii	"nm_STATION_set_ET_factor\000"
.LASF71:
	.ascii	"STATION_find_first_available_station_and_init_stati"
	.ascii	"on_number_GuiVars\000"
.LASF42:
	.ascii	"nm_STATION_get_distribution_uniformity_100u\000"
.LASF52:
	.ascii	"STATION_does_this_station_belong_to_this_manual_pro"
	.ascii	"gram\000"
.LASF29:
	.ascii	"STATION_get_first_available_station\000"
.LASF90:
	.ascii	"STATION_calculate_chain_sync_crc\000"
.LASF59:
	.ascii	"STATION_get_change_bits_ptr\000"
.LASF31:
	.ascii	"STATION_get_num_of_stations_physically_available\000"
.LASF17:
	.ascii	"nm_STATION_set_GID_station_group\000"
.LASF23:
	.ascii	"STATION_this_group_has_no_stations_assigned_to_it\000"
.LASF27:
	.ascii	"nm_STATION_get_physically_available\000"
.LASF39:
	.ascii	"nm_STATION_get_soak_minutes\000"
.LASF47:
	.ascii	"STATION_get_no_water_days\000"
.LASF28:
	.ascii	"STATION_station_is_available_for_use\000"
.LASF77:
	.ascii	"STATION_set_not_physically_available_based_upon_com"
	.ascii	"munication_scan_results\000"
.LASF88:
	.ascii	"nm_STATION_update_pending_change_bits\000"
.LASF48:
	.ascii	"nm_STATION_get_description\000"
.LASF82:
	.ascii	"STATION_set_no_water_days_by_box\000"
.LASF80:
	.ascii	"STATION_on_all_stations_set_or_clear_commserver_cha"
	.ascii	"nge_bits\000"
.LASF6:
	.ascii	"nm_STATION_set_square_footage\000"
.LASF69:
	.ascii	"nm_STATION_get_pointer_to_station\000"
.LASF60:
	.ascii	"STATION_decrement_no_water_days_and_return_value_af"
	.ascii	"ter_decrement\000"
.LASF72:
	.ascii	"nm_STATION_extract_and_store_changes_from_comm\000"
.LASF3:
	.ascii	"nm_STATION_set_station_number\000"
.LASF25:
	.ascii	"STATION_get_num_stations_assigned_to_groups\000"
.LASF30:
	.ascii	"STATION_get_num_stations_in_use\000"
.LASF8:
	.ascii	"nm_STATION_set_total_run_minutes\000"
.LASF54:
	.ascii	"STATION_get_moisture_balance\000"
.LASF67:
	.ascii	"nm_STATION_create_new_station\000"
.LASF68:
	.ascii	"nm_STATION_store_changes\000"
.LASF22:
	.ascii	"STATION_copy_group_name_into_copy_station_GuiVar_an"
	.ascii	"d_set_offsets\000"
.LASF91:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF43:
	.ascii	"nm_STATION_get_watersense_soak_min\000"
.LASF11:
	.ascii	"STATION_get_GID_station_group\000"
.LASF5:
	.ascii	"nm_STATION_set_distribution_uniformity\000"
.LASF34:
	.ascii	"nm_STATION_get_decoder_output\000"
.LASF79:
	.ascii	"STATION_set_bits_on_all_stations_to_cause_distribut"
	.ascii	"ion_in_the_next_token\000"
.LASF32:
	.ascii	"nm_STATION_get_box_index_0\000"
.LASF76:
	.ascii	"STATION_clean_house_processing\000"
.LASF16:
	.ascii	"nm_STATION_set_GID_manual_program_A\000"
.LASF15:
	.ascii	"nm_STATION_set_GID_manual_program_B\000"
.LASF26:
	.ascii	"STATION_get_unique_box_index_count\000"
.LASF89:
	.ascii	"STATIONS_load_all_the_stations_for_ftimes\000"
.LASF46:
	.ascii	"nm_station_structure_updater\000"
.LASF36:
	.ascii	"nm_STATION_get_cycle_minutes_10u\000"
.LASF75:
	.ascii	"STATION_get_prev_available_station\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
