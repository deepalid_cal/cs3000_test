	.file	"tpl_out.c"
	.text
.Ltext0:
	.section	.text.nm_nm_CalcCRCForAllPacketsOfOutgoingMessage,"ax",%progbits
	.align	2
	.global	nm_nm_CalcCRCForAllPacketsOfOutgoingMessage
	.type	nm_nm_CalcCRCForAllPacketsOfOutgoingMessage, %function
nm_nm_CalcCRCForAllPacketsOfOutgoingMessage:
.LFB0:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, lr}
.LCFI0:
	add	r5, r0, #12
	mov	r0, r5
	bl	nm_ListGetFirst
	b	.L4
.L3:
.LBB4:
	ldr	r6, [r4, #20]
	ldr	r7, [r4, #16]
	sub	r6, r6, #4
	mov	r1, r6
	mov	r0, r7
	bl	CRC_calculate_32bit_big_endian
	mov	r1, sp
	mov	r2, #4
	str	r0, [sp, #0]
	add	r0, r7, r6
	bl	memcpy
	mov	r0, r5
	mov	r1, r4
	bl	nm_ListGetNext
.L4:
.LBE4:
	cmp	r0, #0
.LBB5:
	mov	r4, r0
.LBE5:
	bne	.L3
	ldmfd	sp!, {r3, r4, r5, r6, r7, pc}
.LFE0:
	.size	nm_nm_CalcCRCForAllPacketsOfOutgoingMessage, .-nm_nm_CalcCRCForAllPacketsOfOutgoingMessage
	.section	.text.nm_destroy_OM,"ax",%progbits
	.align	2
	.global	nm_destroy_OM
	.type	nm_destroy_OM, %function
nm_destroy_OM:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI1:
	mov	r2, #0
	mvn	r5, #0
	mov	r4, r0
	mov	r3, r2
	str	r5, [sp, #0]
	mov	r1, #3
	ldr	r0, [r0, #36]
	bl	xTimerGenericCommand
	mov	r2, #0
	mov	r3, r2
	mov	r1, #3
	ldr	r0, [r4, #40]
	str	r5, [sp, #0]
	bl	xTimerGenericCommand
	mov	r1, #0
	ldr	r0, [r4, #32]
	mov	r2, r5
	mov	r3, r1
	bl	xQueueGenericReceive
	add	r6, r4, #12
	b	.L6
.L7:
	ldr	r0, [r5, #16]
	ldr	r1, .L8
	mov	r2, #196
	bl	mem_free_debug
	mov	r0, r5
	ldr	r1, .L8
	mov	r2, #198
	bl	mem_free_debug
.L6:
	mov	r0, r6
	ldr	r1, .L8
	mov	r2, #194
	bl	nm_ListRemoveHead_debug
	subs	r5, r0, #0
	bne	.L7
	mov	r1, r5
	mov	r2, r5
	mov	r3, r5
	ldr	r0, [r4, #32]
	bl	xQueueGenericSend
	ldr	r0, [r4, #32]
	bl	vQueueDelete
	mov	r1, r4
	ldr	r2, .L8
	mov	r3, #207
	ldr	r0, .L8+4
	bl	nm_ListRemove_debug
	ldr	r1, .L8
	mov	r0, r4
	mov	r2, #209
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	mem_free_debug
.L9:
	.align	2
.L8:
	.word	.LC0
	.word	.LANCHOR0
.LFE1:
	.size	nm_destroy_OM, .-nm_destroy_OM
	.section	.text.process_waiting_for_status_resp_timer_expired,"ax",%progbits
	.align	2
	.type	process_waiting_for_status_resp_timer_expired, %function
process_waiting_for_status_resp_timer_expired:
.LFB5:
	@ args = 0, pretend = 0, frame = 76
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI2:
	sub	sp, sp, #76
.LCFI3:
	bl	pvTimerGetTimerID
	ldr	r3, .L26
	mov	r1, #0
	mvn	r2, #0
	mov	r4, r0
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, .L26+4
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	bne	.L11
	ldr	r3, .L26+8
	ldrb	r3, [r3, #116]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L12
	ldr	r0, .L26+12
	ldr	r1, .L26+16
	ldr	r2, .L26+20
	ldr	r3, .L26+24
	bl	Alert_item_not_on_list_with_filename
	b	.L12
.L11:
	ldr	r3, [r4, #76]
	cmp	r3, #2000
	bne	.L13
	ldr	r0, .L26+28
	bl	Alert_Message
	b	.L12
.L13:
	ldr	r3, [r4, #80]
	cmp	r3, #3
	beq	.L14
	ldr	r2, .L26+8
	ldrb	r2, [r2, #116]	@ zero_extendqisi2
	tst	r2, #2
	beq	.L14
	cmp	r3, #5
	bne	.L15
	ldr	r3, .L26+32
	ldrh	r0, [r3, #0]
	bl	Alert_status_timer_expired_idx
	b	.L14
.L15:
	ldr	r2, .L26+36
	ldr	r0, .L26+40
	ldr	r1, [r2, r3, asl #2]
	bl	Alert_Message_va
.L14:
	ldr	r3, [r4, #52]
	ldr	r2, [r4, #64]
	cmp	r3, r2
	bcc	.L16
	ldr	r3, .L26+44
	ldr	r2, [r3, #8]
	add	r2, r2, #1
	str	r2, [r3, #8]
	ldr	r3, [r4, #80]
	cmp	r3, #3
	cmpne	r3, #5
	bne	.L17
	ldrb	r2, [r4, #49]	@ zero_extendqisi2
	add	r0, sp, #76
	strb	r2, [sp, #4]
	ldrb	r2, [r4, #48]	@ zero_extendqisi2
	str	r3, [sp, #12]
	strb	r2, [sp, #5]
	ldrb	r2, [r4, #47]	@ zero_extendqisi2
	mov	r3, #256
	str	r3, [r0, #-76]!
	mov	r0, sp
	strb	r2, [sp, #6]
	bl	COMM_MNGR_post_event_with_details
.L17:
	mov	r0, r4
	bl	nm_destroy_OM
	b	.L12
.L16:
	mov	r1, #0
	add	r3, r3, #1
	str	r3, [r4, #52]
	mvn	r2, #0
	mov	r3, r1
	ldr	r0, [r4, #32]
	bl	xQueueGenericReceive
	add	r6, r4, #12
	mov	r0, r6
	bl	nm_ListGetFirst
	mov	r5, #0
	mov	r1, r0
	b	.L18
.L22:
	ldr	r3, [r4, #60]
	add	r5, r5, #1
	cmp	r5, r3
	bne	.L19
	ldr	r3, [r1, #16]
	mov	r0, r4
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	orr	r2, r2, #64
	strb	r2, [r3, #0]
	mov	r3, #1
	str	r3, [r1, #12]
	ldr	r3, .L26+44
	ldr	r2, [r3, #24]
	add	r2, r2, #1
	str	r2, [r3, #24]
	bl	nm_nm_CalcCRCForAllPacketsOfOutgoingMessage
	mov	r3, #768
	strh	r3, [sp, #40]	@ movhi
	ldr	r3, .L26+48
	mov	r2, #0
	ldr	r0, [r3, #0]
	add	r1, sp, #40
	mov	r3, r2
	str	r4, [sp, #44]
	bl	xQueueGenericSend
	cmp	r0, #1
	bne	.L20
	b	.L21
.L19:
	mov	r0, r6
	bl	nm_ListGetNext
	mov	r1, r0
.L18:
	cmp	r1, #0
	bne	.L22
	b	.L25
.L20:
	ldr	r0, .L26+20
	bl	RemovePathFromFileName
	ldr	r2, .L26+52
	mov	r1, r0
	ldr	r0, .L26+56
	bl	Alert_Message_va
.L21:
	mov	r1, #0
	ldr	r0, [r4, #32]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
.L12:
	ldr	r3, .L26
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, pc}
.L25:
	ldr	r0, .L26+60
	bl	Alert_Message
	b	.L21
.L27:
	.align	2
.L26:
	.word	list_tpl_out_messages_MUTEX
	.word	.LANCHOR0
	.word	config_c
	.word	.LC1
	.word	.LC2
	.word	.LC0
	.word	387
	.word	.LC3
	.word	last_contact
	.word	.LANCHOR1
	.word	.LC4
	.word	.LANCHOR2
	.word	TPL_OUT_event_queue
	.word	566
	.word	.LC5
	.word	.LC6
.LFE5:
	.size	process_waiting_for_status_resp_timer_expired, .-process_waiting_for_status_resp_timer_expired
	.section	.text.process_om_existence_timer_expired,"ax",%progbits
	.align	2
	.type	process_om_existence_timer_expired, %function
process_om_existence_timer_expired:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI4:
	bl	pvTimerGetTimerID
	ldr	r3, .L31
	mov	r1, #0
	mvn	r2, #0
	mov	r4, r0
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, .L31+4
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	bne	.L29
	ldr	r3, .L31+8
	ldrb	r3, [r3, #116]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L30
	ldr	r0, .L31+12
	ldr	r1, .L31+16
	ldr	r2, .L31+20
	ldr	r3, .L31+24
	bl	Alert_item_not_on_list_with_filename
	b	.L30
.L29:
	ldr	r3, .L31+28
	mov	r0, r4
	ldr	r2, [r3, #8]
	add	r2, r2, #1
	str	r2, [r3, #8]
	bl	nm_destroy_OM
.L30:
	ldr	r3, .L31
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	ldmfd	sp!, {r4, lr}
	b	xQueueGenericSend
.L32:
	.align	2
.L31:
	.word	list_tpl_out_messages_MUTEX
	.word	.LANCHOR0
	.word	config_c
	.word	.LC1
	.word	.LC2
	.word	.LC0
	.word	623
	.word	.LANCHOR2
.LFE6:
	.size	process_om_existence_timer_expired, .-process_om_existence_timer_expired
	.section	.text.__check_a_message_for_outgoing_packets_to_send,"ax",%progbits
	.align	2
	.type	__check_a_message_for_outgoing_packets_to_send, %function
__check_a_message_for_outgoing_packets_to_send:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L55
	mov	r1, #0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI5:
	mvn	r2, #0
	mov	r4, r0
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, .L55+4
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	bne	.L34
	ldr	r0, .L55+8
	ldr	r1, .L55+12
	ldr	r2, .L55+16
	ldr	r3, .L55+20
	bl	Alert_item_not_on_list_with_filename
	b	.L35
.L34:
	mov	r1, #0
	mvn	r2, #0
	mov	r3, r1
	ldr	r0, [r4, #32]
	bl	xQueueGenericReceive
	add	r8, r4, #12
	mov	r0, r8
	bl	nm_ListGetFirst
.LBB6:
	ldr	r7, .L55+24
	mov	sl, #0
.LBE6:
	mov	r5, r0
	b	.L36
.L49:
	ldr	r3, [r5, #12]
	cmp	r3, #1
	bne	.L37
.LBB7:
	ldr	r6, [r5, #16]
	ldrb	r3, [r6, #0]	@ zero_extendqisi2
	ands	r3, r3, #64
	beq	.L38
	ldr	r3, [r4, #68]
	cmp	r3, #0
	movne	r3, r4
	moveq	r3, #0
.L38:
	ldr	r2, [r4, #76]
	cmp	r2, #2000
	ldr	r2, [r4, #80]
	bne	.L39
	sub	r2, r2, #2
	cmp	r2, #1
	ldrhi	r0, .L55+28
	bhi	.L53
	ldr	r0, [r4, #72]
	add	r2, r5, #16
	ldmia	r2, {r1-r2}
	mov	r3, #0
	b	.L52
.L39:
	sub	r2, r2, #3
	cmp	r2, #3
	ldrls	pc, [pc, r2, asl #2]
	b	.L42
.L46:
	.word	.L43
	.word	.L44
	.word	.L43
	.word	.L54
.L43:
	add	r1, r5, #16
	ldmia	r1, {r0-r1}
	mov	r2, r3
	bl	PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain
	b	.L41
.L44:
	ldr	r2, [r4, #56]
	cmp	r2, #0
	ldrne	r0, .L55+32
	bne	.L53
.L54:
	ldr	r0, [r4, #72]
	add	r2, r5, #16
	ldmia	r2, {r1-r2}
.L52:
	bl	Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy
	b	.L41
.L53:
	bl	Alert_Message
	b	.L41
.L42:
	ldr	r0, .L55+16
	bl	RemovePathFromFileName
	ldr	r2, .L55+36
	mov	r1, r0
	ldr	r0, .L55+40
	bl	Alert_Message_va
.L41:
	ldr	r3, [r7, #20]
	add	r3, r3, #1
	str	r3, [r7, #20]
	ldrb	r3, [r6, #0]	@ zero_extendqisi2
	tst	r3, #64
	bicne	r3, r3, #64
	strneb	r3, [r6, #0]
	ldrneb	r3, [r6, #11]	@ zero_extendqisi2
	strne	r3, [r4, #60]
	str	sl, [r5, #12]
.L37:
.LBE7:
	mov	r1, r5
	mov	r0, r8
	bl	nm_ListGetNext
	mov	r5, r0
.L36:
	cmp	r5, #0
	bne	.L49
	mov	r3, r5
	ldr	r0, [r4, #32]
	mov	r1, r5
	mov	r2, r5
	bl	xQueueGenericSend
	ldr	r3, [r4, #76]
	cmp	r3, #2000
	bne	.L35
	ldr	r3, [r4, #80]
	sub	r3, r3, #2
	cmp	r3, #1
	bhi	.L35
	mov	r0, r4
	bl	nm_destroy_OM
.L35:
	ldr	r3, .L55
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGenericSend
.L56:
	.align	2
.L55:
	.word	list_tpl_out_messages_MUTEX
	.word	.LANCHOR0
	.word	.LC7
	.word	.LC2
	.word	.LC0
	.word	958
	.word	.LANCHOR2
	.word	.LC8
	.word	.LC9
	.word	1094
	.word	.LC10
.LFE8:
	.size	__check_a_message_for_outgoing_packets_to_send, .-__check_a_message_for_outgoing_packets_to_send
	.section	.text.TPL_OUT_destroy_all_outgoing_3000_scan_and_token_messages,"ax",%progbits
	.align	2
	.global	TPL_OUT_destroy_all_outgoing_3000_scan_and_token_messages
	.type	TPL_OUT_destroy_all_outgoing_3000_scan_and_token_messages, %function
TPL_OUT_destroy_all_outgoing_3000_scan_and_token_messages:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L66
	mov	r1, #0
	stmfd	sp!, {r4, lr}
.LCFI6:
	ldr	r0, [r3, #0]
	mvn	r2, #0
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, .L66+4
	bl	nm_ListGetFirst
	ldr	r4, .L66+8
	mov	r1, r0
	b	.L64
.L62:
	ldr	r3, [r1, #80]
	cmp	r3, #5
	cmpne	r3, #3
	bne	.L59
	mov	r0, r1
	bl	nm_destroy_OM
	ldr	r0, .L66+4
	bl	nm_ListGetFirst
	b	.L65
.L59:
	ldr	r0, .L66+4
	bl	nm_ListGetNext
.L65:
	mov	r1, r0
.L64:
	cmp	r1, #0
	beq	.L61
	ldr	r3, [r1, #76]
	cmp	r3, r4
	beq	.L62
	b	.L63
.L61:
	ldr	r3, .L66
	mov	r2, r1
	ldr	r0, [r3, #0]
	mov	r3, r1
	ldmfd	sp!, {r4, lr}
	b	xQueueGenericSend
.L63:
	b	.L63
.L67:
	.align	2
.L66:
	.word	list_tpl_out_messages_MUTEX
	.word	.LANCHOR0
	.word	3000
.LFE2:
	.size	TPL_OUT_destroy_all_outgoing_3000_scan_and_token_messages, .-TPL_OUT_destroy_all_outgoing_3000_scan_and_token_messages
	.section	.text.TPL_OUT_queue_a_message_to_start_resp_timer,"ax",%progbits
	.align	2
	.global	TPL_OUT_queue_a_message_to_start_resp_timer
	.type	TPL_OUT_queue_a_message_to_start_resp_timer, %function
TPL_OUT_queue_a_message_to_start_resp_timer:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI7:
	mov	r4, r0
	sub	sp, sp, #36
.LCFI8:
	ldr	r0, .L70
	bl	Alert_Message
	cmp	r4, #0
	beq	.L68
.LBB8:
	mov	r3, #1024
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, .L70+4
	mov	r2, #0
	ldr	r0, [r3, #0]
	mov	r1, sp
	mov	r3, r2
	str	r4, [sp, #4]
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L68
	ldr	r0, .L70+8
	bl	RemovePathFromFileName
	mov	r2, #280
	mov	r1, r0
	ldr	r0, .L70+12
	bl	Alert_Message_va
.L68:
.LBE8:
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L71:
	.align	2
.L70:
	.word	.LC11
	.word	TPL_OUT_event_queue
	.word	.LC0
	.word	.LC5
.LFE3:
	.size	TPL_OUT_queue_a_message_to_start_resp_timer, .-TPL_OUT_queue_a_message_to_start_resp_timer
	.global	__udivsi3
	.section	.text.__start_waiting_for_status_resp_timer,"ax",%progbits
	.align	2
	.global	__start_waiting_for_status_resp_timer
	.type	__start_waiting_for_status_resp_timer, %function
__start_waiting_for_status_resp_timer:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L75
	mov	r1, #0
	stmfd	sp!, {r0, r4, lr}
.LCFI9:
	mvn	r2, #0
	mov	r4, r0
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, .L75+4
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #1
	bne	.L73
	ldr	r3, [r4, #68]
	cmp	r3, #0
	beq	.L74
	mov	r0, #1000
	mul	r0, r3, r0
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, #0
	mov	r2, r0
	ldr	r0, [r4, #36]
	bl	xTimerGenericCommand
	b	.L74
.L73:
	ldr	r0, .L75+8
	ldr	r1, .L75+12
	ldr	r2, .L75+16
	mov	r3, #308
	bl	Alert_item_not_on_list_with_filename
.L74:
	ldr	r3, .L75
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	xQueueGenericSend
.L76:
	.align	2
.L75:
	.word	list_tpl_out_messages_MUTEX
	.word	.LANCHOR0
	.word	.LC12
	.word	.LC2
	.word	.LC0
.LFE4:
	.size	__start_waiting_for_status_resp_timer, .-__start_waiting_for_status_resp_timer
	.global	__umodsi3
	.section	.text.TPL_OUT_task,"ax",%progbits
	.align	2
	.global	TPL_OUT_task
	.type	TPL_OUT_task, %function
TPL_OUT_task:
.LFB10:
	@ args = 0, pretend = 0, frame = 120
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L136
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI10:
	ldr	r4, .L136+4
	rsb	r3, r3, r0
	sub	sp, sp, #124
.LCFI11:
	mov	r3, r3, lsr #5
	mov	r1, #0
	mov	r2, #32
	ldr	r0, .L136+8
	str	r3, [sp, #40]
	bl	memset
	mov	r1, #0
	mov	r3, r1
	mvn	r2, #0
	ldr	r0, [r4, #0]
	bl	xQueueGenericReceive
	ldr	r0, .L136+12
	mov	r1, #0
	bl	nm_ListInit
	mov	r1, #0
	mov	r2, r1
	ldr	r0, [r4, #0]
	mov	r3, r1
	bl	xQueueGenericSend
	mov	r2, #0
	str	r2, [sp, #32]
.L124:
	ldr	r3, .L136+16
	add	r1, sp, #44
	ldr	r0, [r3, #0]
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	mov	r4, r0
	bne	.L78
	ldrh	r3, [sp, #44]
	cmp	r3, #512
	beq	.L80
	bhi	.L83
	cmp	r3, #256
	bne	.L78
	b	.L134
.L83:
	cmp	r3, #768
	beq	.L81
	cmp	r3, #1024
	bne	.L78
	b	.L135
.L134:
	ldr	r2, [sp, #32]
	cmp	r2, #0
	bne	.L84
	bl	xTaskGetTickCount
	bl	srand
	bl	rand
	ldr	r3, .L136+20
	str	r4, [sp, #32]
	strh	r0, [r3, #0]	@ movhi
.L84:
	ldr	r3, [sp, #52]
	ldrb	r2, [sp, #64]	@ zero_extendqisi2
	str	r3, [sp, #28]
	ldrb	r3, [sp, #66]	@ zero_extendqisi2
	str	r2, [sp, #4]
	str	r3, [sp, #16]
	ldrb	r3, [sp, #69]	@ zero_extendqisi2
	ldrb	r2, [sp, #68]	@ zero_extendqisi2
	str	r3, [sp, #12]
.LBB15:
.LBB16:
	ldr	r3, .L136+4
	mov	r1, #0
.LBE16:
.LBE15:
	str	r2, [sp, #8]
.LBB19:
.LBB17:
	ldr	r0, [r3, #0]
	mvn	r2, #0
	mov	r3, r1
.LBE17:
.LBE19:
	ldr	r6, [sp, #60]
	ldr	r5, [sp, #56]
	ldrb	r9, [sp, #65]	@ zero_extendqisi2
	ldrb	fp, [sp, #67]	@ zero_extendqisi2
	add	r8, sp, #72
	ldmia	r8, {r7-r8}
.LBB20:
.LBB18:
	bl	xQueueGenericReceive
	ldr	r3, .L136+8
	ldr	r4, .L136+12
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	b	.L85
.L86:
	ldr	r0, .L136+12
	bl	nm_ListGetFirst
	bl	nm_destroy_OM
.L85:
	ldr	r3, [r4, #8]
	cmp	r3, #9
	bhi	.L86
	ldr	r1, .L136+24
	ldr	r2, .L136+28
	mov	r0, #84
	bl	mem_malloc_debug
	ldr	r2, [sp, #16]
	ldr	r3, [sp, #4]
	strb	r2, [r0, #44]
	strb	r3, [r0, #46]
	ldr	r2, [sp, #12]
	ldr	r3, [sp, #8]
	str	r6, [r0, #72]
	mov	r6, #0
	mov	r4, r0
	strb	r2, [r0, #47]
	strb	r3, [r0, #48]
	strb	r9, [r0, #45]
	strb	fp, [r0, #49]
	str	r6, [r0, #52]
	str	r6, [r0, #56]
	str	r7, [r0, #76]
	str	r8, [r0, #80]
	mov	r0, #1
	bl	xQueueCreateMutex
	ldr	r3, .L136+32
	mov	r1, #5
	ldr	r3, [r3, #132]
	str	r0, [r4, #32]
	ldr	r0, .L136+36
	mul	r0, r3, r0
	bl	__udivsi3
	ldr	r3, .L136+40
	mov	r2, r6
	str	r3, [sp, #0]
	mov	r3, r4
	mov	r1, r0
	ldr	r0, .L136+44
	bl	xTimerCreate
	cmp	r0, r6
	str	r0, [r4, #40]
	bne	.L87
	ldr	r0, .L136+24
	bl	RemovePathFromFileName
	ldr	r2, .L136+48
	mov	r1, r0
	ldr	r0, .L136+52
	bl	Alert_Message_va
.L87:
	ldr	r3, .L136+56
	ldr	r0, .L136+60
	str	r3, [sp, #0]
	ldr	r1, .L136+64
	mov	r2, #0
	mov	r3, r4
	bl	xTimerCreate
	cmp	r0, #0
	str	r0, [r4, #36]
	bne	.L88
	ldr	r0, .L136+24
	bl	RemovePathFromFileName
	mov	r2, #1232
	mov	r1, r0
	ldr	r0, .L136+52
	bl	Alert_Message_va
.L88:
	ldr	r3, [r4, #76]
	cmp	r3, #2000
	ldr	r3, [r4, #80]
	bne	.L89
	sub	r3, r3, #2
	cmp	r3, #1
	movls	r3, #0
	strls	r3, [r4, #64]
	strls	r3, [r4, #68]
	bls	.L91
	ldr	r0, .L136+68
	bl	Alert_Message
	b	.L91
.L89:
	sub	r3, r3, #3
	mov	r6, #0
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L92
.L97:
	.word	.L129
	.word	.L129
	.word	.L129
	.word	.L129
.L92:
	ldr	r0, .L136+72
	bl	Alert_Message
.L129:
	str	r6, [r4, #64]
	str	r6, [r4, #68]
.L91:
	ldr	r2, .L136+20
	mov	r1, #496
	ldrh	r3, [r2, #0]
	mov	r0, r5
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [r2, #0]	@ movhi
	strh	r3, [r4, #50]	@ movhi
	bl	__udivsi3
	mov	r1, #496
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	str	r0, [sp, #20]
	mov	r0, r5
	bl	__umodsi3
	mov	r1, #0
	cmp	r0, #0
	ldrne	r2, [sp, #20]
	ldr	r0, [r4, #32]
	addne	r3, r2, #1
	movne	r3, r3, asl #16
	movne	r3, r3, lsr #16
	strne	r3, [sp, #20]
	mvn	r2, #0
	mov	r3, r1
	bl	xQueueGenericReceive
	add	r3, r4, #12
	mov	r0, r3
	mov	r1, #0
	str	r3, [sp, #36]
	bl	nm_ListInit
	mov	r3, #0
	str	r3, [sp, #24]
	b	.L99
.L104:
	cmp	r5, #496
	movls	r8, r5, asl #16
	movls	r8, r8, lsr #16
	movhi	r8, #496
	add	r7, r8, #16
	mov	r7, r7, asl #16
	mov	r7, r7, lsr #16
	ldr	r1, .L136+24
	ldr	r2, .L136+76
	mov	r0, r7
	bl	mem_malloc_debug
	ldr	r1, .L136+24
	ldr	r2, .L136+80
	rsb	r5, r8, r5
	mov	r6, r0
	mov	r0, #24
	bl	mem_malloc_debug
	mov	r3, #1
	cmp	r5, #0
	str	r3, [r0, #12]
	moveq	r3, #64
	movne	r3, #0
	str	r7, [r0, #20]
	str	r6, [r0, #16]
	strb	r3, [r6, #0]
	ldr	r3, [r4, #76]
	mov	sl, r0
	cmp	r3, #2000
	bne	.L101
	ldr	r3, [r4, #80]
	sub	r2, r3, #2
	cmp	r2, #1
	bhi	.L102
	ldrb	r2, [r6, #0]	@ zero_extendqisi2
	and	r3, r3, #15
	bic	r2, r2, #30
	orr	r3, r2, r3, asl #1
	strb	r3, [r6, #0]
	ldr	r3, [r4, #80]
	cmp	r3, #3
	bne	.L130
	ldr	r2, [sp, #12]
	ldr	r3, [sp, #8]
	strb	r2, [r6, #2]
	strb	r3, [r6, #3]
	ldr	r2, [sp, #16]
	ldr	r3, [sp, #4]
	strb	fp, [r6, #4]
	strb	r2, [r6, #5]
	strb	r9, [r6, #6]
	strb	r3, [r6, #7]
	b	.L102
.L101:
	mov	r0, r6
	ldr	r1, [r4, #80]
	bl	set_this_packets_CS3000_message_class
.L130:
	ldr	r2, [sp, #16]
	ldr	r3, [sp, #4]
	strb	r2, [r6, #2]
	strb	r3, [r6, #4]
	ldr	r2, [sp, #12]
	ldr	r3, [sp, #8]
	strb	r9, [r6, #3]
	strb	r2, [r6, #5]
	strb	r3, [r6, #6]
	strb	fp, [r6, #7]
.L102:
	add	r1, r4, #50
	mov	r2, #2
	add	r0, r6, #8
	bl	memcpy
	ldr	r2, [sp, #20]
	ldr	r1, [sp, #28]
	strb	r2, [r6, #10]
	ldr	r2, [sp, #24]
	add	r0, r6, #12
	add	r3, r2, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	str	r3, [sp, #24]
	and	r3, r3, #255
	strb	r3, [r6, #11]
	mov	r2, r8
	str	r3, [r4, #60]
	bl	memcpy
	ldr	r3, [sp, #28]
	ldr	r0, [sp, #36]
	add	r3, r3, r8
	mov	r1, sl
	str	r3, [sp, #28]
	bl	nm_ListInsertTail
.L99:
	cmp	r5, #0
	bne	.L104
	mov	r0, r4
	bl	nm_nm_CalcCRCForAllPacketsOfOutgoingMessage
	mov	r1, r4
	ldr	r0, .L136+12
	bl	nm_ListInsertTail
	ldr	r6, [r4, #40]
	bl	xTaskGetTickCount
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, r5
	mov	r3, r5
	mov	r2, r0
	mov	r0, r6
	bl	xTimerGenericCommand
	mov	r1, r5
	mov	r2, r5
	mov	r3, r5
	ldr	r0, [r4, #32]
	bl	xQueueGenericSend
	ldr	r3, .L136+4
	mov	r1, r5
	mov	r2, r5
	ldr	r0, [r3, #0]
	mov	r3, r5
	bl	xQueueGenericSend
	mov	r0, r4
	bl	__check_a_message_for_outgoing_packets_to_send
.LBE18:
.LBE20:
	ldr	r0, [sp, #52]
	ldr	r1, .L136+24
	ldr	r2, .L136+84
	b	.L133
.L81:
	ldr	r0, [sp, #48]
	bl	__check_a_message_for_outgoing_packets_to_send
	b	.L78
.L80:
.LBB21:
	ldr	r3, .L136+4
	mov	r1, #0
	ldr	r0, [r3, #0]
	mvn	r2, #0
	mov	r3, r1
.LBE21:
	ldr	r5, [sp, #52]
.LBB24:
	bl	xQueueGenericReceive
	ldr	r0, .L136+12
	bl	nm_ListGetFirst
	mov	r1, r0
	b	.L105
.L107:
	ldrb	r3, [r5, #8]	@ zero_extendqisi2
	ldrb	r0, [r5, #9]	@ zero_extendqisi2
	ldrh	r2, [r1, #50]
	orr	r3, r3, r0, asl #8
	cmp	r2, r3
	beq	.L106
	ldr	r0, .L136+12
	bl	nm_ListGetNext
	mov	r1, r0
.L105:
	cmp	r1, #0
	bne	.L107
.LBB22:
	str	r1, [sp, #116]
	ldrb	r3, [r5, #4]	@ zero_extendqisi2
	mov	r2, #3
	strb	r3, [sp, #120]
	ldrb	r3, [r5, #3]	@ zero_extendqisi2
	add	r1, sp, #120
	strb	r3, [sp, #121]
	ldrb	r3, [r5, #2]	@ zero_extendqisi2
	add	r0, sp, #116
	strb	r3, [sp, #122]
	bl	memcpy
	ldr	r0, .L136+88
	ldr	r1, [sp, #116]
	bl	Alert_Message_va
	ldr	r3, .L136+8
	ldr	r2, [r3, #28]
	add	r2, r2, #1
	str	r2, [r3, #28]
	b	.L110
.L106:
.LBE22:
	mvn	r3, #0
	mov	r2, #0
	str	r3, [sp, #0]
	mov	r4, r1
	ldr	r0, [r1, #36]
	mov	r3, r2
	mov	r1, #1
	bl	xTimerGenericCommand
	ldrb	r3, [r5, #11]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L109
	ldr	r3, .L136+8
	mov	r0, r4
	ldr	r2, [r3, #4]
	add	r2, r2, #1
	str	r2, [r3, #4]
	bl	nm_destroy_OM
	b	.L110
.L109:
	ldr	r3, .L136+32
	ldrb	r3, [r3, #116]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L111
.LBB23:
	mov	r3, #0
	str	r3, [sp, #116]
	ldrb	r3, [r4, #49]	@ zero_extendqisi2
	add	r1, sp, #120
	strb	r3, [sp, #120]
	ldrb	r3, [r4, #48]	@ zero_extendqisi2
	mov	r2, #3
	strb	r3, [sp, #121]
	ldrb	r3, [r4, #47]	@ zero_extendqisi2
	add	r0, sp, #116
	strb	r3, [sp, #122]
	bl	memcpy
	ldr	r2, [r4, #80]
	ldr	r3, .L136+92
	ldr	r0, .L136+96
	ldr	r1, [r3, r2, asl #2]
	ldr	r2, [sp, #116]
	bl	Alert_Message_va
.L111:
.LBE23:
	ldr	r6, .L136+8
	mov	r1, #0
	ldr	r3, [r6, #16]
	mvn	r2, #0
	add	r3, r3, #1
	str	r3, [r6, #16]
	ldr	r3, [r4, #56]
	ldr	r0, [r4, #32]
	add	r3, r3, #1
	str	r3, [r4, #56]
	add	r7, r4, #12
	mov	r3, r1
	bl	xQueueGenericReceive
	mov	r0, r7
	bl	nm_ListGetFirst
	subs	fp, r0, #0
	bne	.L112
	ldr	r0, .L136+24
	bl	RemovePathFromFileName
	ldr	r2, .L136+100
	mov	r1, r0
	ldr	r0, .L136+104
	b	.L132
.L112:
	add	sl, r5, #12
	mov	r5, #0
	mov	r8, r5
	mov	r1, fp
.L117:
	add	r9, r8, #1
	mov	r9, r9, asl #16
	mov	r8, r9, lsr #16
	and	r9, r9, #458752
	movs	r9, r9, lsr #16
	subne	r9, r9, #1
	ldr	r3, .L136+108
	movne	r9, r9, asl #16
	movne	r9, r9, lsr #16
	moveq	r9, #7
	ldrb	r2, [r3, r9]	@ zero_extendqisi2
	ldrb	r3, [sl, #0]	@ zero_extendqisi2
	tst	r2, r3
	bne	.L115
	mov	r3, #1
	str	r3, [r1, #12]
	ldr	r3, [r6, #24]
	mov	r5, r8
	add	r3, r3, #1
	str	r3, [r6, #24]
.L115:
	mov	r0, r7
	bl	nm_ListGetNext
	cmp	r9, #7
	addeq	sl, sl, #1
	cmp	r0, #0
	mov	r1, r0
	bne	.L117
	cmp	r5, #0
	mov	fp, r0
	beq	.L118
	mov	r0, r7
	bl	nm_ListGetFirst
	b	.L131
.L123:
	add	fp, fp, #1
	mov	fp, fp, asl #16
	mov	fp, fp, lsr #16
	cmp	fp, r5
	bne	.L120
	ldr	r5, [r1, #16]
	ldrb	r3, [r5, #11]	@ zero_extendqisi2
	cmp	r3, fp
	beq	.L121
	ldr	r0, .L136+112
	bl	Alert_Message
.L121:
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	orr	r3, r3, #64
	strb	r3, [r5, #0]
	b	.L122
.L120:
	mov	r0, r7
	bl	nm_ListGetNext
.L131:
	cmp	r0, #0
	mov	r1, r0
	bne	.L123
.L122:
	mov	r0, r4
	bl	nm_nm_CalcCRCForAllPacketsOfOutgoingMessage
	ldr	r2, .L136+16
	mov	r3, #768
	ldr	r0, [r2, #0]
	mov	r2, #0
	strh	r3, [sp, #80]	@ movhi
	add	r1, sp, #80
	mov	r3, r2
	str	r4, [sp, #84]
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L113
	ldr	r0, .L136+24
	bl	RemovePathFromFileName
	ldr	r2, .L136+116
	mov	r1, r0
	ldr	r0, .L136+120
	b	.L132
.L118:
	mov	r1, r5
	mov	r2, r5
	mov	r3, r5
	ldr	r0, [r4, #32]
	bl	xQueueGenericSend
	mov	r0, r4
	bl	nm_destroy_OM
	mov	r1, r5
	mvn	r2, #0
	mov	r3, r5
	ldr	r0, [r4, #32]
	bl	xQueueGenericReceive
	ldr	r0, .L136+24
	bl	RemovePathFromFileName
	mov	r2, #928
	mov	r1, r0
	ldr	r0, .L136+124
.L132:
	bl	Alert_Message_va
.L113:
	mov	r1, #0
	ldr	r0, [r4, #32]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
.L110:
	ldr	r3, .L136+4
	mov	r1, #0
	mov	r2, r1
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericSend
.LBE24:
	ldr	r0, [sp, #52]
	ldr	r1, .L136+24
	ldr	r2, .L136+128
.L133:
	bl	mem_free_debug
	b	.L78
.L135:
	ldr	r0, [sp, #48]
	bl	__start_waiting_for_status_resp_timer
.L78:
	bl	xTaskGetTickCount
	ldr	r3, .L136+132
	ldr	r2, [sp, #40]
	str	r0, [r3, r2, asl #2]
	b	.L124
.L137:
	.align	2
.L136:
	.word	Task_Table
	.word	list_tpl_out_messages_MUTEX
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	TPL_OUT_event_queue
	.word	.LANCHOR3
	.word	.LC0
	.word	1187
	.word	config_c
	.word	60000
	.word	process_om_existence_timer_expired
	.word	.LC13
	.word	1222
	.word	.LC14
	.word	process_waiting_for_status_resp_timer_expired
	.word	.LC15
	.word	6000
	.word	.LC16
	.word	.LC17
	.word	1386
	.word	1390
	.word	1592
	.word	.LC23
	.word	.LANCHOR1
	.word	.LC18
	.word	778
	.word	.LC19
	.word	_Masks
	.word	.LC20
	.word	913
	.word	.LC21
	.word	.LC22
	.word	1609
	.word	task_last_execution_stamp
.LFE10:
	.size	TPL_OUT_task, .-TPL_OUT_task
	.global	cs3000_msg_class_text
	.global	OutgoingMessages
	.global	OutgoingStats
	.section	.bss.OutgoingMessages,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	OutgoingMessages, %object
	.size	OutgoingMessages, 20
OutgoingMessages:
	.space	20
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/tpl_out.c\000"
.LC1:
	.ascii	"om\000"
.LC2:
	.ascii	"OutgoingMessages\000"
.LC3:
	.ascii	"TPL_OUT : unexp status timer fired\000"
.LC4:
	.ascii	"TPL_OUT: status timer expired : msg_class=%s\000"
.LC5:
	.ascii	"TPL_OUT queue full! : %s, %u\000"
.LC6:
	.ascii	"Didn't find OM packet to send\000"
.LC7:
	.ascii	"ptr_outgoing_message\000"
.LC8:
	.ascii	"TPL_OUT: unexp 2000e msg class\000"
.LC9:
	.ascii	"UNXP resend: scan packet\000"
.LC10:
	.ascii	"OM UNK Message Class : %s, %u\000"
.LC11:
	.ascii	"OM: should not be starting status timer!\000"
.LC12:
	.ascii	"pom\000"
.LC13:
	.ascii	"OM exist timer\000"
.LC14:
	.ascii	"Timer NOT CREATED : %s, %u\000"
.LC15:
	.ascii	"OM status resp timer\000"
.LC16:
	.ascii	"TPLOUT unexp 2000 class\000"
.LC17:
	.ascii	"OM class unknown\000"
.LC18:
	.ascii	"TPL_OUT: status packet rqsting resend : msg_class=%"
	.ascii	"s, to ser num=%u\000"
.LC19:
	.ascii	"OM has no blocks : %s, %u\000"
.LC20:
	.ascii	"OM alignment off\000"
.LC21:
	.ascii	"TPL_OUT queue full : %s, %u\000"
.LC22:
	.ascii	"Abnormal OM STATUS rcvd : %s, %u\000"
.LC23:
	.ascii	"TPL_OUT: no OM for incoming status found, from sn=%"
	.ascii	"u\000"
.LC24:
	.ascii	"nlu 0\000"
.LC25:
	.ascii	"CS3000_TO_COMMSERVER_VIA_HUB\000"
.LC26:
	.ascii	"CS3000_FROM_COMMSERVER\000"
.LC27:
	.ascii	"CS3000_SCAN\000"
.LC28:
	.ascii	"CS3000_SCAN_RESP\000"
.LC29:
	.ascii	"CS3000_TOKEN\000"
.LC30:
	.ascii	"CS3000_TOKEN_RESP\000"
.LC31:
	.ascii	"CS3000_TO_TPMICRO\000"
.LC32:
	.ascii	"CS3000_FROM_TPMICRO\000"
.LC33:
	.ascii	"nlu 9\000"
.LC34:
	.ascii	"nlu 10\000"
.LC35:
	.ascii	"CS3000_FROM_COMMSERVER_CODE_DIST\000"
.LC36:
	.ascii	"CS3000_FROM_CONTROLLER_CODE_DIST\000"
.LC37:
	.ascii	"CS3000_FROM_HUB_CODE_DIST\000"
.LC38:
	.ascii	"CS3000_FROM_HUB_CODE_DIST_ADDR\000"
.LC39:
	.ascii	"CS3000_TO_HUB_CODE_DIST\000"
.LC40:
	.ascii	"CS3000_TEST_PACKET\000"
.LC41:
	.ascii	"CS3000_TEST_PACKET_ECHO\000"
	.section	.bss.OutgoingStats,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	OutgoingStats, %object
	.size	OutgoingStats, 32
OutgoingStats:
	.space	32
	.section	.rodata.cs3000_msg_class_text,"a",%progbits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	cs3000_msg_class_text, %object
	.size	cs3000_msg_class_text, 72
cs3000_msg_class_text:
	.word	.LC24
	.word	.LC25
	.word	.LC26
	.word	.LC27
	.word	.LC28
	.word	.LC29
	.word	.LC30
	.word	.LC31
	.word	.LC32
	.word	.LC33
	.word	.LC34
	.word	.LC35
	.word	.LC36
	.word	.LC37
	.word	.LC38
	.word	.LC39
	.word	.LC40
	.word	.LC41
	.section	.bss.next_mid,"aw",%nobits
	.align	1
	.set	.LANCHOR3,. + 0
	.type	next_mid, %object
	.size	next_mid, 2
next_mid:
	.space	2
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI2-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI4-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI5-.LFB8
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI7-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI9-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI10-.LFB10
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0xa0
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/tpl_out.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xe7
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF11
	.byte	0x1
	.4byte	.LASF12
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x28d
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x76
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xad
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x152
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x249
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x3af
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST4
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xd5
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x106
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST6
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x11e
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST7
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x47d
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x5ff
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB5
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB6
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB8
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB3
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI8
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB4
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB10
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI11
	.4byte	.LFE10
	.2byte	0x3
	.byte	0x7d
	.sleb128 160
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF8:
	.ascii	"__here_is_an_incoming_status\000"
.LASF9:
	.ascii	"build_outgoing_message\000"
.LASF4:
	.ascii	"__check_a_message_for_outgoing_packets_to_send\000"
.LASF1:
	.ascii	"nm_destroy_OM\000"
.LASF2:
	.ascii	"process_waiting_for_status_resp_timer_expired\000"
.LASF5:
	.ascii	"TPL_OUT_destroy_all_outgoing_3000_scan_and_token_me"
	.ascii	"ssages\000"
.LASF11:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF10:
	.ascii	"TPL_OUT_task\000"
.LASF12:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/tpl_out.c\000"
.LASF6:
	.ascii	"TPL_OUT_queue_a_message_to_start_resp_timer\000"
.LASF7:
	.ascii	"__start_waiting_for_status_resp_timer\000"
.LASF0:
	.ascii	"nm_nm_CalcCRCForAllPacketsOfOutgoingMessage\000"
.LASF3:
	.ascii	"process_om_existence_timer_expired\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
