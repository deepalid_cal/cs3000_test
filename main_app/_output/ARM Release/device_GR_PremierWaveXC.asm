	.file	"device_GR_PremierWaveXC.c"
	.text
.Ltext0:
	.section	.text.gr_sizeof_read_list,"ax",%progbits
	.align	2
	.global	gr_sizeof_read_list
	.type	gr_sizeof_read_list, %function
gr_sizeof_read_list:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #9
	bx	lr
.LFE0:
	.size	gr_sizeof_read_list, .-gr_sizeof_read_list
	.section	.text.gr_sizeof_write_list,"ax",%progbits
	.align	2
	.global	gr_sizeof_write_list
	.type	gr_sizeof_write_list, %function
gr_sizeof_write_list:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
.LFE1:
	.size	gr_sizeof_write_list, .-gr_sizeof_write_list
	.section	.text.gr_programming_help,"ax",%progbits
	.align	2
	.global	gr_programming_help
	.type	gr_programming_help, %function
gr_programming_help:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
.LFE2:
	.size	gr_programming_help, .-gr_programming_help
	.section	.text.gr_initialize_detail_struct,"ax",%progbits
	.align	2
	.global	gr_initialize_detail_struct
	.type	gr_initialize_detail_struct, %function
gr_initialize_detail_struct:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldr	r4, .L5
	ldr	r5, .L5+4
	str	r4, [r0, #164]
	mov	r1, r5
	mov	r0, r4
	mov	r2, #16
	bl	strlcpy
	mov	r1, r5
	add	r0, r4, #16
	mov	r2, #31
	bl	strlcpy
	mov	r1, r5
	add	r0, r4, #47
	mov	r2, #31
	bl	strlcpy
	mov	r1, r5
	add	r0, r4, #78
	mov	r2, #31
	bl	strlcpy
	mov	r1, r5
	add	r0, r4, #109
	mov	r2, #31
	bl	strlcpy
	add	r0, r4, #140
	mov	r1, r5
	mov	r2, #10
	ldmfd	sp!, {r4, r5, lr}
	b	strlcpy
.L6:
	.align	2
.L5:
	.word	dev_details
	.word	.LC0
.LFE3:
	.size	gr_initialize_detail_struct, .-gr_initialize_detail_struct
	.section	.text.PWXC_initialize_the_connection_process,"ax",%progbits
	.align	2
	.global	PWXC_initialize_the_connection_process
	.type	PWXC_initialize_the_connection_process, %function
PWXC_initialize_the_connection_process:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI1:
	mov	r6, r0
	beq	.L8
	ldr	r0, .L13
	bl	Alert_Message
.L8:
	ldr	r3, .L13+4
	ldr	r2, .L13+8
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #44]
	cmp	r3, #0
	bne	.L9
	ldr	r0, .L13+12
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	Alert_Message
.L9:
	ldr	r4, .L13+16
	mov	r0, r6
	mov	r5, #0
	str	r6, [r4, #4]
	str	r5, [r4, #8]
	bl	GENERIC_GR_card_power_is_on
	ldr	r7, .L13+20
	mvn	r8, #0
	subs	r6, r0, #0
	beq	.L10
	str	r8, [sp, #0]
	ldr	r0, [r7, #40]
	mov	r1, #2
	ldr	r2, .L13+24
	mov	r3, r5
	b	.L12
.L10:
	ldr	r0, [r4, #4]
	bl	power_up_device
	ldr	r0, [r7, #40]
	ldr	r2, .L13+24
	mov	r1, #2
	mov	r3, r6
	str	r8, [sp, #0]
.L12:
	bl	xTimerGenericCommand
	mov	r3, #3
	str	r3, [r4, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L14:
	.align	2
.L13:
	.word	.LC1
	.word	config_c
	.word	port_device_table
	.word	.LC2
	.word	.LANCHOR0
	.word	cics
	.word	3000
.LFE4:
	.size	PWXC_initialize_the_connection_process, .-PWXC_initialize_the_connection_process
	.section	.text.PWXC_connection_processing,"ax",%progbits
	.align	2
	.global	PWXC_connection_processing
	.type	PWXC_connection_processing, %function
PWXC_connection_processing:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI2:
	sub	r0, r0, #121
	cmp	r0, #1
	bls	.L16
	ldr	r0, .L21
	bl	Alert_Message
	b	.L17
.L16:
	ldr	r4, .L21+4
	ldr	r3, [r4, #0]
	cmp	r3, #3
	bne	.L15
	ldr	r3, .L21+8
	ldr	r2, .L21+12
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #44]
	cmp	r3, #0
	beq	.L17
	mov	r0, #1
	blx	r3
	subs	r3, r0, #0
	beq	.L20
	ldr	r1, .L21+16
	mov	r2, #49
	ldr	r0, .L21+20
	bl	strlcpy
	ldr	r0, .L21+24
	bl	Alert_Message
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities
.L20:
	ldr	r2, [r4, #8]
	cmp	r2, #239
	bhi	.L17
	add	r2, r2, #1
	str	r2, [r4, #8]
	mvn	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L21+28
	mov	r1, #2
	ldr	r0, [r2, #40]
	mov	r2, #200
	bl	xTimerGenericCommand
	b	.L15
.L17:
	ldr	r3, .L21+4
	ldr	r0, [r3, #4]
	bl	power_down_device
	ldr	r3, .L21+28
	mov	r2, #1
	mov	r0, #123
	str	r2, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	CONTROLLER_INITIATED_post_event
.L15:
	add	sp, sp, #4
	ldmfd	sp!, {r4, pc}
.L22:
	.align	2
.L21:
	.word	.LC3
	.word	.LANCHOR0
	.word	config_c
	.word	port_device_table
	.word	.LC4
	.word	GuiVar_CommTestStatus
	.word	.LC5
	.word	cics
.LFE5:
	.size	PWXC_connection_processing, .-PWXC_connection_processing
	.global	gr_xml_write_list
	.global	gr_xml_read_list
	.section	.bss.pwxc_cs,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	pwxc_cs, %object
	.size	pwxc_cs, 12
pwxc_cs:
	.space	12
	.section	.rodata.gr_xml_write_list,"a",%progbits
	.align	2
	.type	gr_xml_write_list, %object
	.size	gr_xml_write_list, 0
gr_xml_write_list:
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"\000"
.LC1:
	.ascii	"Why is the PremierWave XC not on PORT A?\000"
.LC2:
	.ascii	"Unexpected NULL: is_connected function\000"
.LC3:
	.ascii	"Connection Process : UNXEXP EVENT\000"
.LC4:
	.ascii	"Cell Connection\000"
.LC5:
	.ascii	"PremierWave XC Cell Connected\000"
.LC6:
	.ascii	"!\000"
.LC7:
	.ascii	">>\000"
.LC8:
	.ascii	"ble)#\000"
.LC9:
	.ascii	"xml)#\000"
	.section	.rodata.gr_xml_read_list,"a",%progbits
	.align	2
	.type	gr_xml_read_list, %object
	.size	gr_xml_read_list, 144
gr_xml_read_list:
	.word	.LC0
	.word	0
	.word	113
	.word	.LC6
	.word	.LC6
	.word	0
	.word	10
	.word	.LC7
	.word	.LC7
	.word	0
	.word	28
	.word	.LC8
	.word	.LC8
	.word	0
	.word	17
	.word	.LC8
	.word	.LC8
	.word	0
	.word	136
	.word	.LC9
	.word	.LC9
	.word	0
	.word	131
	.word	.LC9
	.word	.LC9
	.word	dev_analyze_xcr_dump_device
	.word	34
	.word	.LC8
	.word	.LC8
	.word	dev_final_device_analysis
	.word	35
	.word	.LC0
	.word	.LC8
	.word	dev_cli_disconnect
	.word	29
	.word	.LC0
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI2-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_GR_PremierWaveXC.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x99
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF6
	.byte	0x1
	.4byte	.LASF7
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x133
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x138
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x141
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x15c
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x187
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1c3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB5
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"gr_sizeof_write_list\000"
.LASF3:
	.ascii	"gr_initialize_detail_struct\000"
.LASF5:
	.ascii	"PWXC_connection_processing\000"
.LASF7:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_GR_PremierWaveXC.c\000"
.LASF4:
	.ascii	"PWXC_initialize_the_connection_process\000"
.LASF2:
	.ascii	"gr_programming_help\000"
.LASF6:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"gr_sizeof_read_list\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
