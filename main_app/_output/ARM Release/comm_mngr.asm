	.file	"comm_mngr.c"
	.text
.Ltext0:
	.section	.text.nm_number_of_live_ones,"ax",%progbits
	.align	2
	.type	nm_number_of_live_ones, %function
nm_number_of_live_ones:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L5
	ldr	r2, .L5+4
	add	r1, r3, #1104
	mov	r0, #0
.L3:
	ldr	ip, [r3, #16]
	cmp	ip, #0
	beq	.L2
	ldr	ip, [r2, #168]
	cmp	ip, #3
	addls	r0, r0, #1
.L2:
	add	r3, r3, #92
	cmp	r3, r1
	add	r2, r2, #4
	bne	.L3
	bx	lr
.L6:
	.align	2
.L5:
	.word	chain
	.word	.LANCHOR0
.LFE4:
	.size	nm_number_of_live_ones, .-nm_number_of_live_ones
	.section	.text.flowsense_device_connection_timer_callback,"ax",%progbits
	.align	2
	.type	flowsense_device_connection_timer_callback, %function
flowsense_device_connection_timer_callback:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L8
	mov	r2, #1
	str	r2, [r3, #480]
	bx	lr
.L9:
	.align	2
.L8:
	.word	.LANCHOR0
.LFE9:
	.size	flowsense_device_connection_timer_callback, .-flowsense_device_connection_timer_callback
	.section	.text.token_arrival_timer_callback,"ax",%progbits
	.align	2
	.type	token_arrival_timer_callback, %function
token_arrival_timer_callback:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L11
	mov	r2, #1
	str	r2, [r3, #12]
	bx	lr
.L12:
	.align	2
.L11:
	.word	.LANCHOR0
.LFE11:
	.size	token_arrival_timer_callback, .-token_arrival_timer_callback
	.global	__udivsi3
	.section	.text.start_message_resp_timer,"ax",%progbits
	.align	2
	.type	start_message_resp_timer, %function
start_message_resp_timer:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, lr}
.LCFI0:
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L14
	mov	r1, #2
	mov	r2, r0
	ldr	r0, [r3, #388]
	mov	r3, #0
	bl	xTimerGenericCommand
	ldmfd	sp!, {r3, pc}
.L15:
	.align	2
.L14:
	.word	.LANCHOR0
.LFE16:
	.size	start_message_resp_timer, .-start_message_resp_timer
	.section	.text.stop_message_resp_timer,"ax",%progbits
	.align	2
	.type	stop_message_resp_timer, %function
stop_message_resp_timer:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mvn	r3, #0
	stmfd	sp!, {r0, lr}
.LCFI1:
	str	r3, [sp, #0]
	ldr	r3, .L17
	mov	r2, #0
	ldr	r0, [r3, #388]
	mov	r1, #1
	mov	r3, r2
	bl	xTimerGenericCommand
	ldmfd	sp!, {r3, pc}
.L18:
	.align	2
.L17:
	.word	.LANCHOR0
.LFE17:
	.size	stop_message_resp_timer, .-stop_message_resp_timer
	.section	.text.test_last_incoming_resp_integrity,"ax",%progbits
	.align	2
	.type	test_last_incoming_resp_integrity, %function
test_last_incoming_resp_integrity:
.LFB23:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI2:
	stmia	sp, {r0, r1}
	ldr	r1, .L21
	mov	r0, sp
	mov	r5, r2
	bl	CommAddressesAreEqual
	mov	r4, sp
	cmp	r0, #0
	bne	.L19
.LBB39:
	str	r0, [sp, #8]
	str	r0, [sp, #12]
	ldr	r1, .L21
	mov	r2, #3
	add	r0, sp, #8
	bl	memcpy
	mov	r1, sp
	mov	r2, #3
	add	r0, sp, #12
	bl	memcpy
	ldr	r0, .L21+4
	mov	r1, r5
	ldr	r2, [sp, #8]
	ldr	r3, [sp, #12]
	bl	Alert_Message_va
.L19:
.LBE39:
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, pc}
.L22:
	.align	2
.L21:
	.word	.LANCHOR1+15
	.word	.LC0
.LFE23:
	.size	test_last_incoming_resp_integrity, .-test_last_incoming_resp_integrity
	.section	.text.nm_set_next_contact_index_and_command,"ax",%progbits
	.align	2
	.type	nm_set_next_contact_index_and_command, %function
nm_set_next_contact_index_and_command:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI3:
	ldr	r4, .L55
	ldrh	r3, [r4, #0]
	cmp	r3, #11
	bls	.L24
	ldr	r0, .L55+4
	bl	Alert_Message
	mov	r3, #0
	strh	r3, [r4, #0]	@ movhi
.L24:
	ldr	r3, .L55+8
	ldr	r0, [r3, #0]
	cmp	r0, #1
	beq	.L26
	cmp	r0, #2
	bne	.L46
	b	.L53
.L26:
	ldr	r3, .L55
	ldrh	r3, [r3, #0]
	cmp	r3, #10
	bhi	.L41
	ldr	r2, .L55+12
	add	r3, r3, #1
	strh	r3, [r2, #0]	@ movhi
	ldmfd	sp!, {r4, pc}
.L53:
.LBB42:
.LBB43:
	ldr	r2, [r3, #4]
	ldr	r1, .L55+16
	cmp	r2, r1
	beq	.L31
	ldr	r1, .L55+20
	cmp	r2, r1
	beq	.L32
	ldr	r1, .L55+24
	cmp	r2, r1
	bne	.L51
	b	.L54
.L32:
	add	r3, r3, #36
	mov	r2, #0
.L34:
	ldr	r0, [r3, #4]!
	cmp	r0, #0
	ldrne	r3, .L55+12
	strneh	r2, [r3, #0]	@ movhi
	movne	r2, #62
	strneh	r2, [r3, #2]	@ movhi
	bne	.L52
.L33:
	add	r2, r2, #1
	cmp	r2, #12
	bne	.L34
	b	.L49
.L54:
	add	r2, r3, #36
	mov	r3, #0
.L37:
	ldr	r0, [r2, #4]!
	cmp	r0, #0
	ldrne	r2, .L55+12
	strneh	r3, [r2, #0]	@ movhi
	movne	r3, #60
	bne	.L50
.L36:
	add	r3, r3, #1
	cmp	r3, #12
	bne	.L37
	b	.L49
.L31:
	ldr	r3, .L55
	ldr	r1, .L55+28
	ldrh	r3, [r3, #0]
	mov	r2, #92
.L40:
	add	r3, r3, #1
	mov	r3, r3, asl #16
	cmp	r3, #720896
	movls	r3, r3, lsr #16
	movhi	r3, #0
	mla	r0, r2, r3, r1
	ldr	r0, [r0, #16]
	cmp	r0, #0
	beq	.L40
	ldr	r2, .L55+12
	strh	r3, [r2, #0]	@ movhi
	mov	r3, #80
.L50:
	strh	r3, [r2, #2]	@ movhi
.L52:
	mov	r0, #1
	ldmfd	sp!, {r4, pc}
.L46:
.LBE43:
.LBE42:
	ldr	r0, .L55+32
	bl	Alert_Message
.L51:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L41:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L49:
.LBB45:
.LBB44:
	ldr	r2, .L55+16
	ldr	r3, .L55+8
	str	r2, [r3, #4]
	ldmfd	sp!, {r4, pc}
.L56:
	.align	2
.L55:
	.word	.LANCHOR1
	.word	.LC1
	.word	.LANCHOR0
	.word	.LANCHOR2
	.word	2222
	.word	3333
	.word	1111
	.word	chain
	.word	.LC2
.LBE44:
.LBE45:
.LFE26:
	.size	nm_set_next_contact_index_and_command, .-nm_set_next_contact_index_and_command
	.section	.text.power_up_device,"ax",%progbits
	.align	2
	.global	power_up_device
	.type	power_up_device, %function
power_up_device:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	str	lr, [sp, #-4]!
.LCFI4:
	mov	r1, r0
	bne	.L58
	ldr	r3, .L61
	ldr	r2, .L61+4
	ldr	r3, [r3, #80]
	mov	ip, #56
	mla	r3, ip, r3, r2
	ldr	r3, [r3, #32]
	cmp	r3, #0
	bne	.L60
	ldr	pc, [sp], #4
.L58:
	cmp	r0, #2
	ldrne	pc, [sp], #4
	ldr	r3, .L61
	ldr	r2, .L61+4
	ldr	r3, [r3, #84]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #32]
	cmp	r3, #0
	ldreq	pc, [sp], #4
	mov	r1, #1
.L60:
	blx	r3
	ldr	pc, [sp], #4
.L62:
	.align	2
.L61:
	.word	config_c
	.word	port_device_table
.LFE0:
	.size	power_up_device, .-power_up_device
	.section	.text.power_down_device,"ax",%progbits
	.align	2
	.global	power_down_device
	.type	power_down_device, %function
power_down_device:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	mov	r3, r0
	ldreq	r3, .L69
	str	lr, [sp, #-4]!
.LCFI5:
	ldreq	r3, [r3, #80]
	beq	.L68
	cmp	r3, #2
	ldrne	pc, [sp], #4
	ldr	r3, .L69
	ldr	r3, [r3, #84]
.L68:
	ldr	r2, .L69+4
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #32]
	cmp	r3, #0
	ldreq	pc, [sp], #4
	mov	r1, #0
	blx	r3
	ldr	pc, [sp], #4
.L70:
	.align	2
.L69:
	.word	config_c
	.word	port_device_table
.LFE1:
	.size	power_down_device, .-power_down_device
	.section	.text.COMM_MNGR_network_is_available_for_normal_use,"ax",%progbits
	.align	2
	.global	COMM_MNGR_network_is_available_for_normal_use
	.type	COMM_MNGR_network_is_available_for_normal_use, %function
COMM_MNGR_network_is_available_for_normal_use:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L87
	stmfd	sp!, {r4, lr}
.LCFI6:
	ldr	r2, [r3, #0]
	cmp	r2, #2
	movne	r4, #0
	bne	.L72
	ldr	r4, [r3, #4]
	ldr	r2, .L87+4
	rsb	r2, r2, r4
	rsbs	r4, r2, #0
	adc	r4, r4, r2
.L72:
	ldr	r3, [r3, #8]
	cmp	r3, #0
	movne	r4, #0
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	ldr	r2, .L87
	cmp	r0, #0
	beq	.L74
	ldr	r3, .L87+8
	add	r1, r3, #1104
.L76:
	ldr	r0, [r3, #16]
	cmp	r0, #0
	beq	.L75
	ldr	r0, [r2, #92]
	cmp	r0, #3
	movls	r4, #0
.L75:
	add	r3, r3, #92
	cmp	r3, r1
	add	r2, r2, #4
	bne	.L76
	ldr	r3, .L87
	ldr	r3, [r3, #88]
	b	.L86
.L74:
	ldr	r3, [r2, #140]
	cmp	r3, #3
	ldr	r3, [r2, #144]
	movls	r4, #0
.L86:
	cmp	r3, #3
	movls	r4, #0
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L88:
	.align	2
.L87:
	.word	.LANCHOR0
	.word	2222
	.word	chain
.LFE2:
	.size	COMM_MNGR_network_is_available_for_normal_use, .-COMM_MNGR_network_is_available_for_normal_use
	.section	.text.COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members,"ax",%progbits
	.align	2
	.global	COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members
	.type	COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members, %function
COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	ldr	r4, .L93
	ldr	r2, .L93+4
	mov	r3, #364
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L93+8
	mov	r5, #0
	add	r2, r3, #1104
.L91:
	ldr	r1, [r3, #16]
	add	r3, r3, #92
	cmp	r1, #0
	addne	r5, r5, #1
	cmp	r3, r2
	bne	.L91
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L94:
	.align	2
.L93:
	.word	comm_mngr_recursive_MUTEX
	.word	.LC3
	.word	chain
.LFE5:
	.size	COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members, .-COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members
	.section	.text.COMM_MNGR_alert_if_chain_members_should_not_be_referenced,"ax",%progbits
	.align	2
	.global	COMM_MNGR_alert_if_chain_members_should_not_be_referenced
	.type	COMM_MNGR_alert_if_chain_members_should_not_be_referenced, %function
COMM_MNGR_alert_if_chain_members_should_not_be_referenced:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI8:
	mov	r5, r0
	mov	r4, r1
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	ldmnefd	sp!, {r4, r5, pc}
	mov	r0, r5
	bl	RemovePathFromFileName
	mov	r2, r4
	mov	r1, r0
	ldr	r0, .L97
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_Message_va
.L98:
	.align	2
.L97:
	.word	.LC4
.LFE6:
	.size	COMM_MNGR_alert_if_chain_members_should_not_be_referenced, .-COMM_MNGR_alert_if_chain_members_should_not_be_referenced
	.section	.text.COMM_MNGR_token_timeout_seconds,"ax",%progbits
	.align	2
	.global	COMM_MNGR_token_timeout_seconds
	.type	COMM_MNGR_token_timeout_seconds, %function
COMM_MNGR_token_timeout_seconds:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #35
	bx	lr
.LFE24:
	.size	COMM_MNGR_token_timeout_seconds, .-COMM_MNGR_token_timeout_seconds
	.section	.text.COMM_MNGR_start_a_scan,"ax",%progbits
	.align	2
	.global	COMM_MNGR_start_a_scan
	.type	COMM_MNGR_start_a_scan, %function
COMM_MNGR_start_a_scan:
.LFB29:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI9:
	ldr	r4, .L109
	sub	sp, sp, #24
.LCFI10:
	ldr	r0, [r4, #36]
	bl	Alert_starting_scan_with_reason
	ldr	r3, .L109+4
	mov	r0, sp
	str	r3, [sp, #0]
	mov	r3, #1000
	str	r3, [sp, #20]
	bl	CODE_DISTRIBUTION_post_event_with_details
	ldr	r3, .L109+8
	ldr	r2, .L109+12
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L109+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r4, #8]
	mov	r2, #0
	cmp	r3, r2
	str	r2, [r4, #32]
	beq	.L101
	ldr	r3, [r4, #24]
	add	r3, r3, #1
	str	r3, [r4, #24]
	ldr	r3, .L109+20
	add	r1, r3, #1104
.L102:
	str	r2, [r3, #16]
	add	r3, r3, #92
	cmp	r3, r1
	bne	.L102
.L101:
	ldr	r4, .L109
	mov	r5, #0
	mov	r1, r5
	mov	r2, #48
	str	r5, [r4, #8]
	add	r0, r4, #40
	bl	memset
	mov	r1, r5
	mov	r2, #48
	add	r0, r4, #168
	bl	memset
	mov	r1, r5
	mov	r2, #48
	str	r5, [r4, #88]
	add	r0, r4, #92
	bl	memset
	bl	FLOWSENSE_we_are_the_master_of_a_multiple_controller_network
	cmp	r0, r5
	beq	.L103
	ldr	r3, .L109+24
	ldr	r1, .L109+28
	ldr	r2, [r3, #32]
	strh	r5, [r1, #0]	@ movhi
	ldr	r1, .L109+20
	add	r2, r2, #1
	str	r2, [r3, #32]
	mov	r2, #1
	str	r2, [r4, #0]
	str	r2, [r1, #16]
	str	r2, [r4, #264]
	str	r2, [r4, #312]
	ldr	r2, [r3, #8]
	ldr	r0, .L109+32
	add	r2, r2, #1
	str	r2, [r3, #8]
	ldr	r2, [r3, #4]
	ldr	r0, [r0, #48]
	add	r2, r2, #1
	str	r2, [r3, #4]
	ldr	r2, [r3, #12]
	str	r0, [r1, #20]
	add	r2, r2, #1
	str	r2, [r3, #12]
	str	r5, [r4, #156]
	bl	kick_out_the_next_scan_message
	b	.L104
.L103:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	bne	.L105
	ldr	r3, .L109+20
	add	r2, r3, #1104
	mov	r4, r3
.L106:
	str	r0, [r3, #16]
	add	r3, r3, #92
	cmp	r3, r2
	mov	r5, #0
	bne	.L106
	bl	FLOWSENSE_get_controller_index
	mov	r7, #92
	mov	r6, #1
	mla	r0, r7, r0, r4
	str	r6, [r0, #16]
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L109+32
	ldr	r3, [r3, #48]
	mla	r4, r7, r0, r4
	str	r3, [r4, #20]
	ldr	r4, .L109
	str	r5, [r4, #156]
	str	r5, [r4, #148]
.LBB48:
	bl	STATION_set_not_physically_available_based_upon_communication_scan_results
	bl	LIGHTS_set_not_physically_available_based_upon_communication_scan_results
.LBE48:
	ldr	r3, .L109+36
	mov	r0, #2
	stmia	r4, {r0, r3}
	ldr	r0, .L109+40
	str	r6, [r4, #360]
	bl	postBackground_Calculation_Event
	str	r5, [r4, #400]
	b	.L104
.L105:
	ldr	r3, .L109+36
	mov	r1, #2
	stmia	r4, {r1, r3}
	ldr	r0, .L109+40
	bl	postBackground_Calculation_Event
.L104:
	ldr	r3, .L109+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L110:
	.align	2
.L109:
	.word	.LANCHOR0
	.word	4488
	.word	comm_mngr_recursive_MUTEX
	.word	.LC3
	.word	1534
	.word	chain
	.word	.LANCHOR3
	.word	.LANCHOR1
	.word	config_c
	.word	2222
	.word	4369
.LFE29:
	.size	COMM_MNGR_start_a_scan, .-COMM_MNGR_start_a_scan
	.section	.text.kick_out_the_next_scan_message,"ax",%progbits
	.align	2
	.type	kick_out_the_next_scan_message, %function
kick_out_the_next_scan_message:
.LFB32:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L138
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI11:
	ldr	r2, .L138+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L138+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r8, .L138+12
	ldr	r3, .L138+16
	ldr	r6, [r8, #32]
	ldr	r2, [r3, #0]
	cmp	r6, #0
	add	r2, r2, #1
	str	r2, [r3, #0]
	beq	.L112
	bl	COMM_MNGR_start_a_scan
	b	.L113
.L112:
	bl	nm_set_next_contact_index_and_command
	subs	r5, r0, #0
	beq	.L114
.LBB49:
	ldr	r4, .L138+20
	mov	r5, #3
	str	r5, [r4, #8]
	mov	r0, r5
	ldr	r1, .L138+4
	ldr	r2, .L138+24
	bl	mem_malloc_debug
	mov	r3, #40
	mov	r2, r5
	ldr	r1, .L138+28
	str	r0, [r4, #4]
	strh	r3, [r0, #0]	@ movhi
	mov	r3, #1
	strb	r3, [r0, #2]
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	strb	r6, [r4, #16]
	add	r3, r3, #65
	strb	r3, [r4, #15]
	strb	r6, [r4, #17]
	add	r0, r4, #12
	bl	memcpy
.LBE49:
	mov	lr, r4
	ldmia	lr!, {r0, r1, r2, r3}
	ldr	ip, .L138+32
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [lr, #0]
	ldr	r0, .L138+36
	str	r3, [ip, #0]
	bl	start_message_resp_timer
	ldmib	r4, {r1-r2}
	str	r5, [sp, #4]
	ldrh	r3, [r4, #16]
	mov	r0, r6
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, [r4, #12]
	bl	COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity
	b	.L113
.L114:
	bl	nm_number_of_live_ones
	mov	r4, r0
	bl	FLOWSENSE_get_num_controllers_in_chain
	cmp	r4, r0
	bne	.L115
	ldr	r4, .L138+40
	bl	Alert_leaving_forced
	mov	r6, #65
	mov	r7, r4
	str	r5, [r8, #24]
.L117:
	ldr	r3, [r7, #16]
	cmp	r3, #0
	beq	.L116
	ldr	r3, [r8, #264]
	cmp	r3, #0
	bne	.L116
	ldr	r0, .L138+44
	mov	r1, r6
	bl	Alert_Message_va
	mov	r5, #1
.L116:
	add	r6, r6, #1
	cmp	r6, #77
	add	r7, r7, #92
	add	r8, r8, #4
	bne	.L117
	ldr	r8, .L138+12
	ldr	r7, .L138+40
	mov	r6, #65
	mov	r2, #0
.L119:
	ldr	r3, [r7, #16]
	cmp	r3, #0
	beq	.L118
	ldr	r3, [r8, #312]
	cmp	r3, #0
	bne	.L118
	ldr	r0, .L138+48
	mov	r1, r6
	bl	Alert_Message_va
	mov	r2, #1
.L118:
	add	r6, r6, #1
	cmp	r6, #77
	add	r7, r7, #92
	add	r8, r8, #4
	bne	.L119
	orrs	r3, r2, r5
	beq	.L120
	ldr	r3, .L138+52
	ldr	r1, .L138+40
	add	ip, r3, #48
	mov	r0, #0
.L122:
	ldr	lr, [r3, #4]!
	cmp	lr, #0
	strne	r0, [r1, #16]
	cmp	r3, ip
	add	r1, r1, #92
	bne	.L122
	cmp	r5, #0
	beq	.L123
	mov	r0, #4
	bl	CODE_DISTRIBUTION_try_to_start_a_code_distribution
	cmp	r0, #0
	bne	.L127
	ldr	r3, .L138+56
.L125:
	str	r0, [r4, #16]
	add	r4, r4, #92
	cmp	r4, r3
	bne	.L125
	b	.L127
.L123:
	cmp	r2, #0
	beq	.L120
	mov	r0, #5
	bl	CODE_DISTRIBUTION_try_to_start_a_code_distribution
	cmp	r0, #0
	bne	.L127
	ldr	r3, .L138+56
.L128:
	str	r5, [r4, #16]
	add	r4, r4, #92
	cmp	r4, r3
	bne	.L128
.L127:
	ldr	r3, .L138+12
	ldr	r2, .L138+60
	mov	r1, #2
	stmia	r3, {r1, r2}
	b	.L126
.L120:
.LBB50:
	bl	STATION_set_not_physically_available_based_upon_communication_scan_results
	bl	LIGHTS_set_not_physically_available_based_upon_communication_scan_results
	ldr	r3, .L138+52
	add	r2, r3, #48
.L131:
.LBE50:
.LBB51:
	ldr	r1, [r3, #4]!
	cmp	r1, #0
	beq	.L129
	bl	STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token
	bl	POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token
	bl	MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token
	bl	WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token
	bl	NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token
	bl	IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token
	bl	MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	bl	STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	bl	LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token
	bl	WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	b	.L130
.L129:
	cmp	r3, r2
	bne	.L131
.L130:
.LBE51:
	ldr	r3, .L138+12
	ldr	lr, .L138+64
	mov	r2, #2
	stmia	r3, {r2, lr}
	mov	r2, #1
	str	r2, [r3, #360]
	b	.L126
.L115:
	ldr	ip, .L138+60
	mov	r3, #2
	stmia	r8, {r3, ip}
.L126:
	ldr	r0, .L138+68
	bl	CODE_DISTRIBUTION_post_event
	ldr	r3, .L138+12
	mov	r2, #0
	str	r2, [r3, #400]
.L113:
	ldr	r3, .L138
	ldr	r0, [r3, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L139:
	.align	2
.L138:
	.word	comm_mngr_recursive_MUTEX
	.word	.LC3
	.word	2140
	.word	.LANCHOR0
	.word	.LANCHOR3
	.word	.LANCHOR2
	.word	1255
	.word	config_c+48
	.word	.LANCHOR1
	.word	1800
	.word	chain
	.word	.LC5
	.word	.LC6
	.word	.LANCHOR0+36
	.word	chain+1104
	.word	2222
	.word	1111
	.word	4539
.LFE32:
	.size	kick_out_the_next_scan_message, .-kick_out_the_next_scan_message
	.section	.text.tpmicro_is_available_for_token_generation,"ax",%progbits
	.align	2
	.global	tpmicro_is_available_for_token_generation
	.type	tpmicro_is_available_for_token_generation, %function
tpmicro_is_available_for_token_generation:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L145
	ldr	r0, [r3, #4]
	cmp	r0, #0
	bne	.L142
	ldr	r2, [r3, #92]
	cmp	r2, #0
	bxne	lr
	ldr	r0, [r3, #88]
	cmp	r0, #0
	bxeq	lr
	ldr	r3, .L145+4
	ldr	r0, [r3, #12]
	adds	r0, r0, #0
	movne	r0, #1
	bx	lr
.L142:
	mov	r0, #0
	bx	lr
.L146:
	.align	2
.L145:
	.word	tpmicro_comm
	.word	tpmicro_data
.LFE30:
	.size	tpmicro_is_available_for_token_generation, .-tpmicro_is_available_for_token_generation
	.section	.text.setup_to_check_if_tpmicro_needs_a_code_update,"ax",%progbits
	.align	2
	.global	setup_to_check_if_tpmicro_needs_a_code_update
	.type	setup_to_check_if_tpmicro_needs_a_code_update, %function
setup_to_check_if_tpmicro_needs_a_code_update:
.LFB37:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L148
	ldr	r0, .L148+4
	mov	r1, #1
	mov	r2, #0
	str	r1, [r3, #92]
	str	r2, [r3, #72]
	str	r1, [r3, #76]
	str	r2, [r3, #88]
	b	FLASH_STORAGE_request_code_image_version_stamp
.L149:
	.align	2
.L148:
	.word	tpmicro_comm
	.word	TPMICRO_APP_FILENAME
.LFE37:
	.size	setup_to_check_if_tpmicro_needs_a_code_update, .-setup_to_check_if_tpmicro_needs_a_code_update
	.section	.text.COMM_MNGR_device_exchange_results_to_key_process_task,"ax",%progbits
	.align	2
	.global	COMM_MNGR_device_exchange_results_to_key_process_task
	.type	COMM_MNGR_device_exchange_results_to_key_process_task, %function
COMM_MNGR_device_exchange_results_to_key_process_task:
.LFB39:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L151
	stmfd	sp!, {r0, r1, lr}
.LCFI12:
	mov	r3, #0
	str	r0, [sp, #0]
	mov	r1, sp
	ldr	r0, [r2, #0]
	mvn	r2, #0
	str	r3, [sp, #4]
	bl	xQueueGenericSend
	ldmfd	sp!, {r2, r3, pc}
.L152:
	.align	2
.L151:
	.word	Key_To_Process_Queue
.LFE39:
	.size	COMM_MNGR_device_exchange_results_to_key_process_task, .-COMM_MNGR_device_exchange_results_to_key_process_task
	.section	.text.COMM_MNGR_post_event_with_details,"ax",%progbits
	.align	2
	.global	COMM_MNGR_post_event_with_details
	.type	COMM_MNGR_post_event_with_details, %function
COMM_MNGR_post_event_with_details:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L155
	mov	r2, #0
	mov	r1, r0
	str	lr, [sp, #-4]!
.LCFI13:
	ldr	r0, [r3, #0]
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	ldreq	pc, [sp], #4
	ldr	r0, .L155+4
	ldr	lr, [sp], #4
	b	Alert_Message
.L156:
	.align	2
.L155:
	.word	COMM_MNGR_task_queue
	.word	.LC7
.LFE40:
	.size	COMM_MNGR_post_event_with_details, .-COMM_MNGR_post_event_with_details
	.section	.text.message_rescan_timer_callback,"ax",%progbits
	.align	2
	.type	message_rescan_timer_callback, %function
message_rescan_timer_callback:
.LFB13:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI14:
	mov	r3, #1536
	sub	sp, sp, #40
.LCFI15:
	str	r3, [sp, #0]
	mov	r0, sp
	mov	r3, #2
	str	r3, [sp, #36]
	bl	COMM_MNGR_post_event_with_details
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.LFE13:
	.size	message_rescan_timer_callback, .-message_rescan_timer_callback
	.section	.text.CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages,"ax",%progbits
	.align	2
	.global	CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages
	.type	CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages, %function
CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages:
.LFB35:
	@ args = 16, pretend = 4, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	sp, sp, #4
.LCFI16:
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI17:
	ldr	r6, .L159
	sub	sp, sp, #40
.LCFI18:
	mov	r5, r2
	mov	r7, r0
	mov	r4, r1
	mov	r0, r2
	mov	r1, r6
	ldr	r2, .L159+4
	str	r3, [sp, #64]
	bl	mem_malloc_debug
	mov	r1, r4
	mov	r2, r5
	mov	r8, r0
	bl	memcpy
	mov	r1, r6
	ldr	r2, .L159+8
	mov	r0, #40
	bl	mem_malloc_debug
	add	r3, sp, #72
	mov	r4, r0
	str	r8, [r0, #12]
	str	r5, [r0, #16]
	ldr	r5, .L159+12
	ldmia	r3, {r0, r1}
	add	r3, sp, #64
	ldmia	r3, {r2-r3}
	str	r0, [r4, #20]
	str	r2, [r4, #32]
	str	r3, [r4, #36]
	ldr	r0, [r5, #0]
	mov	r2, r6
	mov	r3, #2624
	strh	r1, [r4, #24]	@ movhi
	str	r7, [r4, #28]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L159+16
	bl	nm_ListInsertTail
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	add	r0, sp, #40
	mov	r3, #768
	str	r3, [r0, #-40]!
	mov	r0, sp
	bl	COMM_MNGR_post_event_with_details
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	add	sp, sp, #4
	bx	lr
.L160:
	.align	2
.L159:
	.word	.LC3
	.word	2601
	.word	2610
	.word	list_incoming_messages_recursive_MUTEX
	.word	.LANCHOR0+424
.LFE35:
	.size	CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages, .-CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages
	.section	.text.COMM_MNGR_post_event,"ax",%progbits
	.align	2
	.global	COMM_MNGR_post_event
	.type	COMM_MNGR_post_event, %function
COMM_MNGR_post_event:
.LFB41:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI19:
	sub	sp, sp, #40
.LCFI20:
	add	r3, sp, #40
	str	r0, [r3, #-40]!
	mov	r0, sp
	bl	COMM_MNGR_post_event_with_details
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.LFE41:
	.size	COMM_MNGR_post_event, .-COMM_MNGR_post_event
	.section	.text.COMM_MNGR_task,"ax",%progbits
	.align	2
	.global	COMM_MNGR_task
	.type	COMM_MNGR_task, %function
COMM_MNGR_task:
.LFB38:
	@ args = 0, pretend = 0, frame = 160
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L325
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI21:
	ldr	r6, .L325+4
	rsb	r3, r3, r0
	sub	sp, sp, #168
.LCFI22:
	mov	r3, r3, lsr #5
	str	r3, [sp, #20]
	ldr	r3, .L325+8
	ldr	r5, .L325+12
	mov	r4, #0
	ldr	r2, .L325+16
	str	r4, [r3, #0]
	ldr	r0, [r6, #0]
	ldr	r3, .L325+20
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	add	r0, r5, #424
	bl	nm_ListInit
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r6, .L325+24
	mvn	r2, #0
	mov	r3, r4
	mov	r1, r4
	ldr	r0, [r6, #0]
	bl	xQueueGenericReceive
	add	r0, r5, #404
	mov	r1, r4
	bl	nm_ListInit
	mov	r1, r4
	mov	r2, r4
	mov	r3, r4
	ldr	r0, [r6, #0]
	bl	xQueueGenericSend
	mov	r3, #1
	str	r3, [r5, #160]
	str	r3, [r5, #156]
	ldr	r3, .L325+28
	ldr	r1, .L325+32
	str	r3, [sp, #0]
	mov	r2, r4
	mov	r3, r4
	mov	r0, r4
	bl	xTimerCreate
	ldr	r3, .L325+36
	mov	r1, #200
	mov	r2, r4
	str	r0, [r5, #20]
	str	r3, [sp, #0]
	mov	r0, r4
	mov	r3, r4
	bl	xTimerCreate
	ldr	r3, .L325+40
	mov	r1, #140
	mov	r2, r4
	str	r0, [r5, #16]
	str	r3, [sp, #0]
	mov	r0, r4
	mov	r3, r4
	bl	xTimerCreate
	ldr	r3, .L325+44
	mov	r1, #200
	mov	r2, r4
	str	r0, [r5, #392]
	str	r3, [sp, #0]
	mov	r0, r4
	mov	r3, r4
	bl	xTimerCreate
	ldr	r3, .L325+48
	ldr	r1, .L325+52
	mov	r2, r4
	str	r0, [r5, #388]
	str	r3, [sp, #0]
	mov	r0, r4
	mov	r3, r4
	bl	xTimerCreate
	ldr	r3, .L325+56
	mov	r1, #20
	mov	r2, r4
	str	r0, [r5, #452]
	str	r3, [sp, #0]
	mov	r0, r4
	mov	r3, r4
	bl	xTimerCreate
	cmp	r0, r4
	str	r0, [r5, #384]
	bne	.L163
	ldr	r0, .L325+16
	bl	RemovePathFromFileName
	ldr	r2, .L325+60
	mov	r1, r0
	ldr	r0, .L325+64
	bl	Alert_Message_va
.L163:
	mov	r4, #0
	mov	r6, #1
	str	r4, [r5, #0]
	str	r4, [r5, #8]
	str	r4, [r5, #12]
	str	r4, [r5, #32]
	str	r4, [r5, #164]
	str	r6, [r5, #396]
	str	r4, [r5, #400]
	ldr	r7, [r5, #392]
	bl	xTaskGetTickCount
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, r4
	mov	r3, r4
	mov	r2, r0
	mov	r0, r7
	bl	xTimerGenericCommand
	ldr	r3, .L325+316
	mov	r2, #10
	str	r2, [r3, #16]
	str	r6, [r3, #0]
	str	r6, [r3, #4]
	ldr	r3, .L325+68
	str	r4, [r5, #472]
	ldr	r1, .L325+52
	str	r3, [sp, #0]
	mov	r2, r4
	mov	r3, r4
	mov	r0, r4
	bl	xTimerCreate
	str	r4, [r5, #480]
	str	r0, [r5, #476]
.LBB75:
	bl	FLOWSENSE_we_are_the_master_of_a_multiple_controller_network
	cmp	r0, r4
	beq	.L289
	ldr	r3, .L325+72
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L325+16
	mov	r3, #408
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L325+76
.L167:
	ldr	r2, [r3, #108]
	cmp	r2, #0
	beq	.L165
	ldr	r2, [r3, #176]
	sub	r2, r2, #4
	cmp	r2, #6
	bls	.L290
.L165:
	add	r6, r6, #1
	cmp	r6, #12
	add	r3, r3, #92
	bne	.L167
	mov	r6, #0
	mov	r4, r6
	b	.L166
.L290:
	mov	r4, #1
.L166:
	ldr	r3, .L325+72
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE75:
	cmp	r4, #0
	beq	.L289
	ldr	r0, .L325+80
	add	r1, r6, #65
	bl	Alert_Message_va
.L289:
	ldr	r5, .L325+76
.LBB76:
.LBB77:
.LBB78:
.LBB79:
	add	r9, sp, #132
	add	r2, r9, #3
	str	r2, [sp, #24]
.L308:
.LBE79:
.LBE78:
.LBE77:
.LBE76:
	ldr	r3, .L325+84
	add	r4, sp, #28
	ldr	r0, [r3, #0]
	mov	r1, r4
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	bne	.L168
	ldr	r3, [sp, #28]
	cmp	r3, #2304
	beq	.L175
	bhi	.L183
	cmp	r3, #768
	beq	.L172
	bhi	.L184
	cmp	r3, #512
	beq	.L170
	ldr	r2, .L325+300
	cmp	r3, r2
	beq	.L171
	cmp	r3, #256
	bne	.L168
	b	.L320
.L184:
	cmp	r3, #1536
	beq	.L174
	bhi	.L185
	cmp	r3, #1024
	bne	.L168
	b	.L321
.L185:
	cmp	r3, #1792
	beq	.L175
	cmp	r3, #2048
	bne	.L168
	b	.L322
.L183:
	cmp	r3, #4608
	beq	.L179
	bhi	.L186
	cmp	r3, #3840
	beq	.L178
	bhi	.L187
	cmp	r3, #2816
	bne	.L168
	b	.L323
.L187:
	cmp	r3, #4352
	beq	.L179
	ldr	r2, .L325+348
	cmp	r3, r2
	bne	.L168
	b	.L179
.L186:
	cmp	r3, #5120
	beq	.L180
	bhi	.L188
	cmp	r3, #4864
	bne	.L168
	b	.L180
.L188:
	cmp	r3, #5376
	beq	.L181
	cmp	r3, #5632
	bne	.L168
	b	.L324
.L181:
	bl	CONFIG_this_controller_originates_commserver_messages
	ldr	r4, .L325+332
	cmp	r0, #0
	bne	.L189
	ldr	r3, [r4, #80]
	cmp	r3, #0
	beq	.L189
	mov	r0, #1
	bl	power_up_device
.L189:
	ldr	r3, [r4, #84]
	cmp	r3, #1
	bls	.L190
	mov	r0, #2
	bl	power_up_device
.L190:
	bl	FLOWSENSE_we_are_the_master_of_a_multiple_controller_network
	cmp	r0, #0
	beq	.L191
	ldr	r3, .L325+332
	ldr	r3, [r3, #84]
	cmp	r3, #3
	movne	r4, #0
	bne	.L192
	bl	CONFIG_this_controller_is_a_configured_hub
	rsbs	r4, r0, #1
	movcc	r4, #0
.L192:
	bl	nm_number_of_live_ones
	mov	r6, r0
	bl	FLOWSENSE_get_num_controllers_in_chain
	cmp	r6, r0
	bne	.L193
	ldr	r3, .L325+76
.L196:
	ldr	r2, [r3, #108]
	cmp	r2, #0
	beq	.L194
	ldr	r2, [r3, #176]
	cmp	r2, #3
	ldr	r2, [r3, #180]
	moveq	r4, #1
	cmp	r2, #3
	moveq	r4, #1
.L194:
	ldr	r2, .L325+88
	add	r3, r3, #92
	cmp	r3, r2
	bne	.L196
	cmp	r4, #0
	beq	.L191
.L193:
	ldr	r0, .L325+92
	bl	Alert_Message
	ldr	r3, .L325+12
	ldr	r4, [r3, #476]
	bl	xTaskGetTickCount
	mvn	r3, #0
	mov	r1, #0
	str	r3, [sp, #0]
	mov	r3, r1
	mov	r2, r0
	mov	r0, r4
	bl	xTimerGenericCommand
	b	.L197
.L191:
	ldr	r3, .L325+12
	mov	r2, #1
	str	r2, [r3, #480]
.L197:
	bl	TPMICRO_COMM_create_timer_and_start_messaging
	b	.L168
.L172:
.LBB85:
	ldr	r3, .L325+12
	ldr	r3, [r3, #0]
	str	r3, [sp, #8]
	sub	r3, r3, #1
	cmp	r3, #1
	movhi	r3, #0
	bhi	.L309
	ldr	r3, .L325+316
	ldr	r2, [r3, #4]
	cmp	r2, #0
	movne	r2, #0
	strne	r2, [sp, #12]
	bne	.L198
	ldr	r3, [r3, #88]
	adds	r3, r3, #0
	movne	r3, #1
.L309:
	str	r3, [sp, #12]
.L198:
	bl	tpmicro_is_available_for_token_generation
	subs	sl, r0, #0
	beq	.L310
	ldr	r3, [sp, #8]
	cmp	r3, #2
	movne	sl, #0
	moveq	sl, #1
.L310:
	ldr	r3, .L325+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L325+16
	ldr	r3, .L325+96
	str	sl, [sp, #16]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L325+100
	ldr	r1, .L325+16
	mov	r2, #2688
	b	.L312
.L245:
	ldr	r3, [r4, #32]
	cmp	r3, #2000
	bne	.L201
	ldr	r3, [r4, #36]
	cmp	r3, #2
	bne	.L202
	ldrb	r1, [r4, #22]	@ zero_extendqisi2
	ldrb	r2, [r4, #21]	@ zero_extendqisi2
	ldrb	r3, [r4, #20]	@ zero_extendqisi2
	ldr	r0, .L325+104
	bl	Alert_Message_va
	ldr	r2, [r4, #16]
	ldr	r3, [r4, #12]
	str	r2, [sp, #0]
	add	r2, r4, #20
	ldmia	r2, {r0, r1}
	ldr	r2, [r4, #28]
	bl	RADIO_TEST_echo_inbound_2000e_test_message_back_to_where_it_came_from
	b	.L203
.L202:
	cmp	r3, #3
	ldrne	r0, .L325+108
	bne	.L311
	ldr	r0, [r4, #16]
	add	r1, sp, #124
	ldr	r2, .L325+16
	ldr	r3, .L325+112
	bl	mem_oabia
	cmp	r0, #0
	beq	.L203
	ldr	r2, [r4, #16]
	ldr	r0, [sp, #124]
	ldr	r1, [r4, #12]
	bl	memcpy
	ldr	r3, [r4, #16]
	add	r0, sp, #68
	str	r3, [sp, #128]
	mov	r3, #16384
	str	r3, [sp, #68]
	add	r3, sp, #124
	ldmia	r3, {r2-r3}
	str	r2, [sp, #80]
	str	r3, [sp, #84]
	ldr	r3, [r4, #28]
	str	r3, [sp, #88]
	add	r3, r4, #32
	ldmia	r3, {r2-r3}
	str	r2, [sp, #72]
	str	r3, [sp, #76]
	bl	RADIO_TEST_post_event_with_details
	b	.L203
.L201:
	ldr	r2, .L325+116
	cmp	r3, r2
	bne	.L203
	ldr	r3, [r4, #36]
	sub	r3, r3, #3
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L205
.L210:
	.word	.L206
	.word	.L207
	.word	.L208
	.word	.L209
.L206:
	ldr	r3, [sp, #12]
	cmp	r3, #0
	ldreq	r0, .L325+120
	beq	.L311
	ldr	r7, .L325+12
	ldr	r6, [r7, #20]
	bl	xTaskGetTickCount
	mvn	r3, #0
	mov	r1, #0
	str	r3, [sp, #0]
	mov	r3, r1
	mov	r2, r0
	mov	r0, r6
	bl	xTimerGenericCommand
.LBB82:
	ldr	r3, [r4, #12]
	ldrh	r3, [r3, #0]
	cmp	r3, #40
	ldrne	r0, .L325+124
	bne	.L311
	mov	r3, #0
	str	r3, [r7, #148]
.LBB81:
.LBB80:
	bl	FLOWSENSE_get_controller_letter
	ldr	r1, .L325+16
	ldr	r2, .L325+128
	str	r0, [sp, #140]
	mov	r0, #26
	bl	mem_malloc_debug
	mov	r3, #41
	add	r1, sp, #168
	strh	r3, [r1, #-2]!	@ movhi
	mov	r2, #2
	mov	r6, r0
	bl	memcpy
	mov	r2, #4
	add	r1, sp, #140
	add	r0, r6, #2
	bl	memcpy
	ldr	r1, .L325+132
	mov	r0, #-2147483648
	bl	convert_code_image_date_to_version_number
	add	r1, sp, #168
	mov	r2, #4
	str	r0, [r1, #-24]!
	add	r0, r6, #6
	bl	memcpy
	ldr	r1, .L325+132
	mov	r0, #-2147483648
	bl	convert_code_image_time_to_edit_count
	add	r1, sp, #168
	mov	r2, #4
	str	r0, [r1, #-20]!
	add	r0, r6, #10
	bl	memcpy
	ldr	r1, .L325+136
	mov	r2, #4
	add	r0, r6, #14
	bl	memcpy
	ldr	r1, .L325+140
	mov	r2, #4
	add	r0, r6, #18
	bl	memcpy
	mov	r3, #3
	strb	r3, [r6, #22]
	ldr	r3, [r7, #152]
	ldr	r2, .L325+12
	cmp	r3, #0
	movne	r3, #1
	strb	r3, [r6, #23]
	ldr	r3, [r2, #156]
	add	r1, r6, #24
	cmp	r3, #0
	movne	r3, #1
	strb	r3, [r6, #24]
	mov	r3, #0
	str	r3, [r2, #156]
	ldr	r2, [sp, #140]
	cmp	r2, #65
	ldr	r2, .L325+332
	beq	.L217
	ldr	r3, [r2, #80]
	sub	r0, r3, #3
	rsbs	r3, r0, #0
	adc	r3, r3, r0
.L217:
	ldr	r2, [r2, #84]
	mov	r0, r9
	cmp	r2, #3
	moveq	r3, #1
	strb	r3, [r1, #1]
	mov	r2, #3
	ldr	r1, .L325+144
	bl	memcpy
	add	r1, r4, #20
	mov	r2, #3
	ldr	r0, [sp, #24]
	bl	memcpy
	ldr	r3, .L325+148
	ldr	r0, [r4, #28]
	ldr	r2, [r3, #8]
	mov	r1, r6
	add	r2, r2, #1
	str	r2, [r3, #8]
	mov	r3, #4
	str	r3, [sp, #4]
	ldrh	r3, [sp, #136]
	mov	r2, #26
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, [sp, #132]
	bl	COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity
.LBE80:
.LBE81:
	ldr	r3, .L325+12
	mov	r2, #0
	str	r2, [r3, #140]
	str	r2, [r3, #144]
	b	.L203
.L207:
.LBE82:
	ldr	r3, .L325+148
	ldr	r2, [r3, #12]
	add	r2, r2, #1
	str	r2, [r3, #12]
	ldr	r2, [sp, #8]
	cmp	r2, #1
	ldrne	r0, .L325+152
	bne	.L311
	bl	stop_message_resp_timer
.LBB83:
	ldr	r3, .L325+284
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L325+16
	mov	r3, #880
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L325+316
	ldr	r3, [r3, #88]
	cmp	r3, #0
	bne	.L220
	ldr	r0, .L325+156
	bl	Alert_Message
.L220:
	add	r0, sp, #168
	mov	r8, #0
	add	r1, r4, #20
	mov	r2, #3
	str	r8, [r0, #-24]!
	bl	memcpy
	ldr	r6, [r4, #12]
	add	r0, sp, #148
	add	r1, r6, #2
	mov	r2, #4
	bl	memcpy
	ldr	r3, .L325+296
	ldr	r7, [sp, #148]
	ldrh	r3, [r3, #0]
	sub	r7, r7, #65
	cmp	r3, #11
	bls	.L221
	ldr	r0, .L325+16
	bl	RemovePathFromFileName
	ldr	r2, .L325+160
	mov	r8, #1
	mov	r1, r0
	ldr	r0, .L325+164
	bl	Alert_Message_va
.L221:
	cmp	r7, #11
	bls	.L222
	ldr	r0, .L325+16
	bl	RemovePathFromFileName
	ldr	r2, .L325+168
	mov	r1, r0
	ldr	r0, .L325+172
	bl	Alert_Message_va
	ldr	r3, .L325+296
	mov	r2, #92
	ldrh	r3, [r3, #0]
	mla	r3, r2, r3, r5
	mov	r2, #0
	str	r2, [r3, #16]
	b	.L223
.L222:
	cmp	r8, #0
	bne	.L223
	ldr	fp, .L325+296
	ldrh	r3, [fp, #0]
	cmp	r3, r7
	beq	.L224
	ldr	r0, .L325+16
	bl	RemovePathFromFileName
	mov	r2, #952
	mov	r1, r0
	ldr	r0, .L325+176
	bl	Alert_Message_va
	ldrh	r2, [fp, #0]
	mov	r3, #92
	mla	r2, r3, r2, r5
	mla	r7, r3, r7, r5
	str	r8, [r2, #16]
	str	r8, [r7, #16]
	b	.L223
.L224:
	mov	r2, #92
	mla	r2, r7, r2, r5
	ldr	r3, [sp, #144]
	ldr	r2, [r2, #20]
	mov	r8, #1
	cmp	r3, r2
	ldrne	r2, .L325+12
	addne	r1, r7, #10
	movne	r0, #1
	strne	r0, [r2, r1, asl #2]
	mov	r2, #92
	mla	r2, r7, r2, r5
	ldr	r2, [r2, #16]
	cmp	r2, #0
	ldreq	r2, .L325+12
	addeq	r1, r7, #10
	moveq	r0, #1
	streq	r0, [r2, r1, asl #2]
	ldr	r2, .L325+76
	mov	r1, #92
	mla	r7, r1, r7, r2
	add	r0, sp, #140
	str	r3, [r7, #20]
	str	r8, [r7, #16]
	add	r1, r6, #6
	mov	r2, #4
	bl	memcpy
	mov	r2, #4
	add	r1, r6, #10
	add	r0, sp, #152
	bl	memcpy
	ldr	r1, .L325+132
	mov	r0, #-2147483648
	bl	convert_code_image_date_to_version_number
	ldr	r1, .L325+132
	add	r7, r6, #14
	mov	fp, r0
	mov	r0, #-2147483648
	bl	convert_code_image_time_to_edit_count
	ldr	r3, [sp, #140]
	ldr	r2, .L325+296
	cmp	fp, r3
	ldr	r3, .L325+12
	bne	.L227
	ldr	r1, [sp, #152]
	cmp	r0, r1
	ldreqh	r2, [r2, #0]
	addeq	r2, r2, #66
	streq	r8, [r3, r2, asl #2]
	beq	.L228
.L227:
	ldrh	r0, [r2, #0]
	mov	r1, #0
	add	r2, r0, #66
	str	r1, [r3, r2, asl #2]
	bl	Alert_main_code_needs_updating_at_slave_idx
.L228:
	mov	r1, r7
	mov	r2, #4
	add	r0, sp, #156
	bl	memcpy
	add	r1, r6, #18
	mov	r2, #4
	add	r0, sp, #160
	bl	memcpy
	ldr	r1, .L325+316
	ldr	r2, [sp, #156]
	ldr	r3, [r1, #80]
	cmp	r2, r3
	ldr	r3, .L325+12
	ldr	r2, .L325+296
	bne	.L229
	ldr	r1, [r1, #84]
	ldr	r0, [sp, #160]
	cmp	r0, r1
	ldreqh	r2, [r2, #0]
	moveq	r1, #1
	addeq	r2, r2, #78
	streq	r1, [r3, r2, asl #2]
	beq	.L230
.L229:
	ldrh	r0, [r2, #0]
	mov	r1, #0
	add	r2, r0, #78
	str	r1, [r3, r2, asl #2]
	bl	Alert_tpmicro_code_needs_updating_at_slave_idx
.L230:
	ldrb	r3, [r6, #23]	@ zero_extendqisi2
	ldrb	r7, [r6, #22]	@ zero_extendqisi2
	cmp	r3, #0
	ldr	r3, .L325+296
	ldrne	r2, .L325+12
	ldrneh	r1, [r3, #0]
	movne	r0, #1
	addne	r1, r1, #10
	strne	r0, [r2, r1, asl #2]
	ldrh	r1, [r3, #0]
	ldr	r3, .L325+12
	add	r2, r1, #10
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, #0
	beq	.L232
	ldr	r0, .L325+180
	add	r1, r1, #65
	bl	Alert_Message_va
	bl	CONTROLLER_INITIATED_update_comm_server_registration_info
	bl	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation
	bl	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights
.L232:
	cmp	r7, #1
	bls	.L233
	ldrb	r3, [r6, #24]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L234
	ldr	r1, .L325+12
	ldr	r3, [r1, #160]
	cmp	r3, #0
	bne	.L234
	ldr	r2, .L325+296
	ldrh	r2, [r2, #0]
	add	r0, r2, #10
	ldr	r1, [r1, r0, asl #2]
	ldr	r0, .L325+184
	cmp	r1, #0
	movne	r1, #92
	mlane	r2, r1, r2, r5
	strne	r3, [r2, #16]
	bl	DIALOG_draw_ok_dialog
	mov	r0, #800
	bl	vTaskDelay
	mov	r0, #38
	bl	SYSTEM_application_requested_restart
.L234:
	cmp	r7, #2
	beq	.L233
	ldrb	r3, [r6, #25]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L223
.L233:
	ldr	r3, .L325+188
	add	r0, sp, #100
	str	r3, [sp, #100]
	add	r3, r3, #162
	str	r3, [sp, #120]
	bl	CODE_DISTRIBUTION_post_event_with_details
.L223:
	ldr	r3, .L325+284
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE83:
	bl	kick_out_the_next_scan_message
	b	.L203
.L208:
	cmp	sl, #0
	beq	.L236
	ldr	r6, .L325+12
	ldr	r3, [r6, #8]
	cmp	r3, #0
	beq	.L237
	bl	Alert_leaving_forced
	mov	r3, #0
	str	r3, [r6, #8]
.L237:
	ldr	r6, [r6, #20]
	bl	xTaskGetTickCount
	mvn	r3, #0
	mov	r1, #0
	str	r3, [sp, #0]
	mov	r3, r1
	mov	r2, r0
	mov	r0, r6
	bl	xTimerGenericCommand
	ldr	r3, [r4, #12]
	ldrh	r3, [r3, #0]
	cmp	r3, #60
	bne	.L238
	mov	r0, r4
	bl	IRRI_COMM_process_incoming__clean_house_request
	b	.L203
.L238:
	cmp	r3, #80
	bne	.L239
	mov	r0, r4
	bl	IRRI_COMM_process_incoming__irrigation_token
	b	.L203
.L239:
	cmp	r3, #62
	bne	.L240
	mov	r0, r4
	bl	IRRI_COMM_process_incoming__crc_error_clean_house_request
	b	.L203
.L240:
	ldr	r0, .L325+16
	bl	RemovePathFromFileName
	ldr	r2, .L325+192
	mov	r1, r0
	ldr	r0, .L325+196
	bl	Alert_Message_va
	b	.L203
.L236:
	ldr	r0, .L325+200
	b	.L311
.L209:
	ldr	r3, [sp, #16]
	cmp	r3, #0
	ldreq	r0, .L325+204
	beq	.L311
	bl	stop_message_resp_timer
	add	r3, r4, #20
	ldmia	r3, {r0, r1}
	ldr	r2, .L325+208
	bl	test_last_incoming_resp_integrity
	ldr	r3, .L325+296
	ldr	r6, .L325+12
	ldrh	r3, [r3, #0]
	mov	r2, #0
	add	r3, r3, #42
	str	r2, [r6, r3, asl #2]
	ldr	r3, [r4, #12]
	ldrh	r3, [r3, #0]
	cmp	r3, #61
	bne	.L242
	mov	r0, r4
	bl	FOAL_COMM_process_incoming__clean_house_request__response
	b	.L243
.L242:
	cmp	r3, #81
	bne	.L244
	mov	r0, r4
	bl	FOAL_COMM_process_incoming__irrigation_token__response
	b	.L243
.L244:
	ldr	r0, .L325+16
	bl	RemovePathFromFileName
	ldr	r2, .L325+212
	mov	r1, r0
	ldr	r0, .L325+196
	bl	Alert_Message_va
.L243:
	mov	r3, #0
	str	r3, [r6, #400]
.LBB84:
	ldr	r0, .L325+300
	bl	COMM_MNGR_post_event
	b	.L203
.L205:
.LBE84:
	ldr	r0, .L325+216
.L311:
	bl	Alert_Message
.L203:
	ldr	r0, [r4, #12]
	ldr	r1, .L325+16
	ldr	r2, .L325+220
	bl	mem_free_debug
	mov	r0, r4
	ldr	r1, .L325+16
	ldr	r2, .L325+224
	bl	mem_free_debug
	ldr	r0, .L325+100
	ldr	r1, .L325+16
	ldr	r2, .L325+228
.L312:
	bl	nm_ListRemoveHead_debug
	cmp	r0, #0
	mov	r4, r0
	bne	.L245
	ldr	r3, .L325+4
	b	.L316
.L170:
.LBE85:
	ldr	r3, .L325+12
	str	r0, [r3, #396]
.LBB86:
	ldr	r0, .L325+300
	bl	COMM_MNGR_post_event
	b	.L168
.L171:
.LBE86:
.LBB87:
	ldr	r3, .L325+284
	ldr	r4, .L325+12
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L325+232
	ldr	r2, .L325+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r4, #396]
	cmp	r3, #1
	bne	.L246
	ldr	r3, [r4, #0]
	cmp	r3, #2
	bne	.L247
	ldr	r3, [r4, #480]
	cmp	r3, #0
	beq	.L248
	bl	tpmicro_is_available_for_token_generation
	cmp	r0, #0
	beq	.L248
	ldr	r3, [r4, #400]
	mov	r6, r4
	cmp	r3, #0
	bne	.L248
	bl	CODE_DISTRIBUTION_comm_mngr_should_be_idle
	cmp	r0, #0
	bne	.L248
	ldr	r4, [r4, #32]
	cmp	r4, #0
	beq	.L249
	bl	COMM_MNGR_start_a_scan
	b	.L248
.L249:
	ldr	r3, [r6, #164]
	cmp	r3, #0
	beq	.L250
	ldr	r3, [r6, #376]
	ldr	r2, .L325+340
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #48]
	cmp	r3, #0
	beq	.L267
	ldr	r2, [r6, #0]
	str	r2, [r6, #380]
	mov	r2, #5
	str	r2, [r6, #0]
	blx	r3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #1
	ldr	r0, [r6, #20]
	mov	r2, r4
	mov	r3, r4
	bl	xTimerGenericCommand
	b	.L267
.L250:
	ldr	r3, .L325+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L248
.LBB88:
	bl	FLOWSENSE_we_are_the_master_of_a_multiple_controller_network
	subs	r2, r0, #0
	beq	.L252
	ldr	r2, [r6, #8]
	cmp	r2, #0
	beq	.L252
	ldr	r0, [r6, #16]
	bl	xTimerIsTimerActive
	cmp	r0, #0
	bne	.L254
	ldr	r3, [r6, #24]
	cmp	r3, #4
	movls	r3, #300
	bls	.L255
	cmp	r3, #9
	movls	r3, #900
	bls	.L255
	cmp	r3, #14
	movhi	r3, #14400
	movls	r3, #3600
.L255:
	mov	r0, #1000
	mul	r0, r3, r0
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L325+12
	mov	r1, #2
	mov	r2, r0
	ldr	r0, [r3, #16]
	mov	r3, #0
	b	.L313
.L252:
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r0, [r6, #16]
	mov	r1, #1
	mov	r3, r2
.L313:
	bl	xTimerGenericCommand
.L254:
.LBE88:
	ldr	r4, .L325+12
	mov	r3, #0
	str	r3, [r4, #160]
	bl	FLOWSENSE_we_are_the_master_of_a_multiple_controller_network
	cmp	r0, #0
	beq	.L256
	ldr	r3, [r4, #8]
	cmp	r3, #0
	bne	.L256
	bl	nm_number_of_live_ones
	mov	r6, r0
	bl	FLOWSENSE_get_num_controllers_in_chain
	subs	r0, r6, r0
	movne	r0, #1
	str	r0, [r4, #12]
.L256:
	ldr	r3, [r4, #12]
	ldr	r6, .L325+12
	cmp	r3, #0
	beq	.L257
.LBB89:
	ldr	r3, [r6, #8]
	cmp	r3, #0
	bne	.L258
	bl	Alert_entering_forced
	bl	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation
	bl	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights
	mov	r3, #1
	str	r3, [r6, #8]
.L258:
.LBE89:
	ldr	r3, .L325+12
	mov	r2, #0
	str	r2, [r3, #12]
.L257:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	beq	.L259
	ldr	r6, .L325+12
	ldr	r3, [r6, #8]
	cmp	r3, #0
	bne	.L259
	bl	nm_set_next_contact_index_and_command
	cmp	r0, #0
	beq	.L248
	ldr	r3, [r6, #28]
	cmp	r3, #0
	beq	.L260
	ldr	r7, .L325+76
	mov	r4, #65
.L262:
.LBB90:
	ldr	r3, [r7, #16]
	cmp	r3, #0
	beq	.L261
	ldr	r2, [r6, #216]
	cmp	r2, #0
	beq	.L261
	ldr	r0, .L325+236
	mov	r1, r4
	bl	Alert_Message_va
.L261:
	add	r4, r4, #1
	cmp	r4, #77
	add	r7, r7, #92
	add	r6, r6, #4
	bne	.L262
	ldr	r4, .L325+12
	mov	r1, #0
	add	r0, r4, #216
	mov	r2, #48
	bl	memset
	mov	r3, #0
	str	r3, [r4, #28]
.L260:
.LBE90:
	ldr	r4, .L325+248
	ldr	r1, .L325+144
	mov	r2, #3
	add	r0, r4, #12
	bl	memcpy
	mov	r0, r4
	ldrh	r3, [r0], #15
	mov	r1, #92
	mla	r1, r3, r1, r5
	mov	r2, #3
	add	r1, r1, #20
	bl	memcpy
	ldrh	r3, [r4, #2]
	cmp	r3, #60
	bne	.L263
	bl	FOAL_COMM_build_token__clean_house_request
	b	.L264
.L263:
	cmp	r3, #80
	bne	.L265
	bl	FOAL_COMM_build_token__irrigation_token
	b	.L264
.L326:
	.align	2
.L325:
	.word	Task_Table
	.word	list_incoming_messages_recursive_MUTEX
	.word	.LANCHOR4
	.word	.LANCHOR0
	.word	.LC3
	.word	2963
	.word	list_packets_waiting_for_token_MUTEX
	.word	token_arrival_timer_callback
	.word	30000
	.word	message_rescan_timer_callback
	.word	token_rate_timer_callback
	.word	message_resp_timer_callback
	.word	commserver_msg_receipt_error_timer_callback
	.word	6000
	.word	device_exchange_timer_callback
	.word	3025
	.word	.LC8
	.word	flowsense_device_connection_timer_callback
	.word	chain_members_recursive_MUTEX
	.word	chain
	.word	.LC9
	.word	COMM_MNGR_task_queue
	.word	chain+1012
	.word	.LC10
	.word	2683
	.word	.LANCHOR0+424
	.word	.LC11
	.word	.LC12
	.word	2711
	.word	3000
	.word	.LC14
	.word	.LC13
	.word	665
	.word	CS3000_APP_FILENAME
	.word	tpmicro_comm+80
	.word	tpmicro_comm+84
	.word	config_c+48
	.word	.LANCHOR3
	.word	.LC20
	.word	.LC15
	.word	927
	.word	.LC16
	.word	937
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	595
	.word	4488
	.word	2815
	.word	.LC21
	.word	.LC22
	.word	.LC24
	.word	.LC23
	.word	2859
	.word	.LC25
	.word	2887
	.word	2889
	.word	2892
	.word	1799
	.word	.LC26
	.word	.LC27
	.word	35000
	.word	.LANCHOR2
	.word	.LC28
	.word	.LC29
	.word	2474
	.word	2498
	.word	2517
	.word	.LC30
	.word	.LC31
	.word	.LC32
	.word	comm_mngr_recursive_MUTEX
	.word	2414
	.word	.LC1
	.word	.LANCHOR1
	.word	513
	.word	.LC33
	.word	.LC34
	.word	.LC35
	.word	tpmicro_comm
	.word	.LC36
	.word	.LC3
	.word	3379
	.word	config_c
	.word	ci_and_comm_mngr_activity_recursive_MUTEX
	.word	port_device_table
	.word	.LC37
	.word	4368
	.word	36866
	.word	36869
	.word	.LANCHOR0
	.word	task_last_execution_stamp
.L265:
	cmp	r3, #62
	beq	.L264
	ldr	r0, .L325+240
	bl	Alert_Message
.L264:
	ldr	r0, .L325+244
	bl	start_message_resp_timer
	ldr	lr, .L325+248
	ldr	ip, .L325+296
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [lr, #0]
	ldmib	r4, {r1-r2}
	str	r3, [ip, #0]
	mov	r3, #5
	str	r3, [sp, #4]
	ldrh	r3, [r4, #16]
	mov	r0, #0
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, [r4, #12]
	bl	COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity
	ldr	r3, .L325+360
	mov	r2, #1
	str	r2, [r3, #400]
	mov	r2, #0
	str	r2, [r3, #396]
	b	.L248
.L259:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L248
	ldr	r4, .L325+360
	ldr	r3, [r4, #8]
	cmp	r3, #0
	bne	.L248
	ldr	r0, [r4, #20]
	bl	xTimerIsTimerActive
	subs	r6, r0, #0
	bne	.L248
	ldr	r4, [r4, #20]
	bl	xTaskGetTickCount
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, r6
	mov	r3, r6
	mov	r2, r0
	mov	r0, r4
	bl	xTimerGenericCommand
	b	.L248
.L247:
	cmp	r3, #1
	beq	.L248
	cmp	r3, #0
	beq	.L248
	cmp	r3, #5
	bne	.L266
	ldr	r3, [r4, #164]
	cmp	r3, #0
	beq	.L248
	ldr	r3, .L325+360
	ldr	r2, .L325+340
	ldr	r3, [r3, #376]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #48]
	cmp	r3, #0
	beq	.L267
	blx	r3
.L267:
	ldr	r3, .L325+360
	mov	r2, #0
	str	r2, [r3, #164]
	b	.L248
.L266:
	ldr	r0, .L325+252
	bl	Alert_Message
.L248:
	ldr	r3, .L325+360
	ldr	r4, [r3, #392]
	bl	xTaskGetTickCount
	mvn	r3, #0
	mov	r1, #0
	str	r3, [sp, #0]
	mov	r3, r1
	mov	r2, r0
	mov	r0, r4
	bl	xTimerGenericCommand
.L246:
	ldr	r3, .L325+284
.L316:
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L168
.L320:
	ldmib	r4, {r0, r1, r6}
.LBE87:
.LBB91:
.LBB92:
	ldr	r4, .L325+296
.LBE92:
.LBE91:
.LBB96:
	str	r0, [r9, #0]
.LBE96:
.LBB97:
.LBB94:
	ldr	r0, .L325+256
.LBE94:
.LBE97:
.LBB98:
	strh	r1, [r9, #4]	@ movhi
.LBE98:
.LBB99:
.LBB95:
	bl	Alert_Message
	ldr	r3, .L325+284
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L325+324
	ldr	r3, .L325+260
	bl	xQueueTakeMutexRecursive_debug
	bl	stop_message_resp_timer
	ldrh	r3, [r4, #0]
	cmp	r3, #11
	bls	.L268
	ldr	r0, .L325+292
	bl	Alert_Message
	mov	r3, #0
	strh	r3, [r4, #0]	@ movhi
.L268:
	cmp	r6, #3
	bne	.L269
	ldr	r3, .L325+360
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L270
	ldr	r0, .L325+324
	bl	RemovePathFromFileName
	ldr	r2, .L325+264
	mov	r1, r0
	ldr	r0, .L325+272
	bl	Alert_Message_va
.L270:
	ldr	r3, .L325+296
	ldrh	r3, [r3, #0]
	b	.L318
.L269:
	cmp	r6, #5
	bne	.L272
	ldr	r3, .L325+360
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L273
	ldr	r0, .L325+324
	bl	RemovePathFromFileName
	ldr	r2, .L325+268
	mov	r1, r0
	ldr	r0, .L325+272
	bl	Alert_Message_va
.L273:
	ldmia	r9, {r0, r1}
	ldr	r2, .L325+276
	bl	test_last_incoming_resp_integrity
	ldr	r3, .L325+296
	ldrh	r2, [r3, #0]
.LBB93:
	ldr	r3, .L325+360
	add	r1, r2, #42
	ldr	r0, [r3, r1, asl #2]
	add	r2, r2, #54
	add	r0, r0, #1
	str	r0, [r3, r1, asl #2]
	ldr	r1, [r3, r2, asl #2]
	add	r1, r1, #1
	str	r1, [r3, r2, asl #2]
.LBE93:
	mov	r2, #0
	str	r2, [r3, #400]
	b	.L319
.L272:
	ldr	r0, .L325+280
	bl	Alert_Message
	b	.L246
.L321:
.LBE95:
.LBE99:
.LBB100:
	ldr	r3, .L325+284
	ldr	r4, .L325+296
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L325+288
	ldr	r2, .L325+324
	bl	xQueueTakeMutexRecursive_debug
	ldrh	r3, [r4, #0]
	cmp	r3, #11
	bls	.L274
	ldr	r0, .L325+292
	bl	Alert_Message
	mov	r3, #0
	strh	r3, [r4, #0]	@ movhi
.L274:
	ldr	r4, .L325+360
	ldr	r6, .L325+296
	ldr	r3, [r4, #0]
	cmp	r3, #1
	bne	.L275
	ldrh	r3, [r6, #0]
.L318:
	mov	r2, #92
	mla	r3, r2, r3, r5
	mov	r2, #0
	str	r2, [r3, #16]
	bl	kick_out_the_next_scan_message
	b	.L246
.L275:
	ldrh	r0, [r6, #0]
	bl	Alert_msg_response_timeout_idx
	ldrh	r3, [r6, #0]
.LBB101:
	add	r2, r3, #42
	ldr	r1, [r4, r2, asl #2]
	add	r3, r3, #54
	add	r1, r1, #1
	str	r1, [r4, r2, asl #2]
	ldr	r2, [r4, r3, asl #2]
	add	r2, r2, #1
	str	r2, [r4, r3, asl #2]
.LBE101:
	mov	r3, #0
	str	r3, [r4, #400]
.L319:
.LBB102:
	ldr	r0, .L325+300
	bl	COMM_MNGR_post_event
	b	.L246
.L174:
.LBE102:
.LBE100:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L277
	ldr	r0, .L325+304
	bl	Alert_Message
.L277:
	ldr	r3, .L325+360
	ldr	r2, [sp, #64]
	str	r2, [r3, #36]
	mov	r2, #1
	str	r2, [r3, #32]
	b	.L168
.L324:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L278
	bl	FLOWSENSE_get_controller_index
	cmp	r0, #0
	beq	.L278
	ldr	r3, .L325+360
	mov	r2, #1
	str	r2, [r3, #148]
	str	r2, [r3, #152]
	mov	r2, #0
	str	r2, [r3, #140]
	str	r2, [r3, #144]
	b	.L168
.L278:
	ldr	r0, .L325+308
	b	.L317
.L175:
	add	r0, sp, #28
	bl	TPMICRO_COMM_kick_out_the_next_tpmicro_msg
	b	.L168
.L322:
	add	r1, sp, #44
	ldmia	r1, {r0-r1}
	bl	TPMICRO_COMM_parse_incoming_message_from_tp_micro
	b	.L168
.L323:
	ldr	r3, .L325+316
	ldr	r3, [r3, #4]
	cmp	r3, #0
	ldreq	r0, .L325+312
	beq	.L317
	mov	r0, r4
	bl	TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg
	b	.L168
.L317:
	bl	Alert_Message
	b	.L168
.L178:
	ldr	r4, .L325+316
	ldr	r3, [r4, #92]
	cmp	r3, #0
	bne	.L280
	ldr	r3, [r4, #4]
	cmp	r3, #0
	bne	.L280
	ldr	r0, .L325+320
	bl	Alert_Message
.L280:
	ldr	r3, [sp, #52]
	str	r3, [r4, #80]
	ldr	r3, [sp, #56]
	str	r3, [r4, #84]
	mov	r3, #1
	str	r3, [r4, #88]
	b	.L168
.L179:
	ldr	r3, .L325+336
	ldr	r2, .L325+324
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L325+328
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [sp, #60]
	sub	r3, r2, #1
	cmp	r3, #1
	movhi	r2, #0
	ldr	r3, .L325+360
	bhi	.L314
	cmp	r2, #1
	ldr	r2, .L325+332
	ldreq	r2, [r2, #80]
	ldrne	r2, [r2, #84]
.L314:
	str	r2, [r3, #376]
	ldr	r1, .L325+340
	ldr	r3, [r3, #376]
	mov	r0, #56
	mla	r3, r0, r3, r1
	ldr	r2, .L325+360
	ldr	r1, [r3, #48]
	cmp	r1, #0
	beq	.L284
	ldr	r3, [r3, #52]
	cmp	r3, #0
	beq	.L284
	ldr	r3, [sp, #60]
	str	r3, [r2, #368]
	ldr	r3, [sp, #28]
	str	r3, [r2, #364]
	mov	r3, #1
	str	r3, [r2, #164]
	b	.L285
.L284:
	mov	r0, #36864
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
.L285:
	ldr	r3, .L325+336
	b	.L316
.L180:
	ldr	r4, .L325+360
	ldr	r2, .L325+340
	ldr	r3, [r4, #376]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #52]
	cmp	r3, #0
	beq	.L286
	add	r0, sp, #28
	blx	r3
	b	.L168
.L286:
	ldr	r0, .L325+344
	bl	Alert_Message
	ldr	r3, [r4, #364]
	ldr	r2, .L325+348
	cmp	r3, #4352
	cmpne	r3, r2
	ldreq	r0, .L325+352
	ldrne	r0, .L325+356
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	ldr	r3, .L325+360
	mov	r1, #100
	ldr	r2, [r3, #380]
	ldr	r0, [r3, #368]
	str	r2, [r3, #0]
	mov	r2, #0
	bl	RCVD_DATA_enable_hunting_mode
.L168:
	bl	xTaskGetTickCount
	ldr	r3, .L325+364
	ldr	r2, [sp, #20]
	str	r0, [r3, r2, asl #2]
	b	.L308
.LFE38:
	.size	COMM_MNGR_task, .-COMM_MNGR_task
	.section	.text.device_exchange_timer_callback,"ax",%progbits
	.align	2
	.type	device_exchange_timer_callback, %function
device_exchange_timer_callback:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #5120
	b	COMM_MNGR_post_event
.LFE18:
	.size	device_exchange_timer_callback, .-device_exchange_timer_callback
	.section	.text.message_resp_timer_callback,"ax",%progbits
	.align	2
	.type	message_resp_timer_callback, %function
message_resp_timer_callback:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #1024
	b	COMM_MNGR_post_event
.LFE15:
	.size	message_resp_timer_callback, .-message_resp_timer_callback
	.section	.text.token_rate_timer_callback,"ax",%progbits
	.align	2
	.type	token_rate_timer_callback, %function
token_rate_timer_callback:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #512
	b	COMM_MNGR_post_event
.LFE12:
	.size	token_rate_timer_callback, .-token_rate_timer_callback
	.section	.text.task_control_ENTER_device_exchange_hammer,"ax",%progbits
	.align	2
	.global	task_control_ENTER_device_exchange_hammer
	.type	task_control_ENTER_device_exchange_hammer, %function
task_control_ENTER_device_exchange_hammer:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L331
	mov	r2, #1
	str	lr, [sp, #-4]!
.LCFI23:
	str	r2, [r3, #0]
	bl	CONTROLLER_INITIATED_force_end_of_transaction
	ldr	r3, .L331+4
	mov	r2, #4
	str	r2, [r3, #0]
	ldr	lr, [sp], #4
	b	CODE_DISTRIBUTION_stop_and_cleanup
.L332:
	.align	2
.L331:
	.word	.LANCHOR4
	.word	cics
.LFE42:
	.size	task_control_ENTER_device_exchange_hammer, .-task_control_ENTER_device_exchange_hammer
	.section	.text.task_control_EXIT_device_exchange_hammer,"ax",%progbits
	.align	2
	.global	task_control_EXIT_device_exchange_hammer
	.type	task_control_EXIT_device_exchange_hammer, %function
task_control_EXIT_device_exchange_hammer:
.LFB43:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L337
	str	lr, [sp, #-4]!
.LCFI24:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L334
	ldr	r3, .L337+4
	ldr	r2, [r3, #0]
	cmp	r2, #4
	bne	.L335
	mov	r2, #1
	str	r2, [r3, #0]
	mov	r0, #120
	bl	CONTROLLER_INITIATED_post_event
.L335:
	ldr	r3, .L337+8
	mov	r1, #100
	ldr	r2, [r3, #0]
	ldr	r0, [r3, #368]
	cmp	r2, #5
	ldreq	r2, [r3, #380]
	streq	r2, [r3, #0]
	mov	r2, #0
	bl	RCVD_DATA_enable_hunting_mode
.L334:
	ldr	r3, .L337
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	pc, [sp], #4
.L338:
	.align	2
.L337:
	.word	.LANCHOR4
	.word	cics
	.word	.LANCHOR0
.LFE43:
	.size	task_control_EXIT_device_exchange_hammer, .-task_control_EXIT_device_exchange_hammer
	.global	in_device_exchange_hammer
	.global	next_contact
	.global	last_contact
	.global	comm_mngr
	.global	comm_stats
	.section	.bss.in_device_exchange_hammer,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	in_device_exchange_hammer, %object
	.size	in_device_exchange_hammer, 4
in_device_exchange_hammer:
	.space	4
	.section	.bss.comm_mngr,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	comm_mngr, %object
	.size	comm_mngr, 484
comm_mngr:
	.space	484
	.section	.bss.comm_stats,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	comm_stats, %object
	.size	comm_stats, 36
comm_stats:
	.space	36
	.section	.bss.next_contact,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	next_contact, %object
	.size	next_contact, 20
next_contact:
	.space	20
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"COMM MNGR %s address discrepancy; to: %d from: %d\000"
.LC1:
	.ascii	"last contact index out of range\000"
.LC2:
	.ascii	"Error calling next contact\000"
.LC3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/comm_mngr.c\000"
.LC4:
	.ascii	"SHOULD NOT BE USING CHAIN_MEMBERS : %s, %u\000"
.LC5:
	.ascii	"Controller %c has older MAIN CODE\000"
.LC6:
	.ascii	"Controller %c has older TPMICRO CODE\000"
.LC7:
	.ascii	"CommMngr queue overflow!\000"
.LC8:
	.ascii	"Timer NOT CREATED : %s, %u\000"
.LC9:
	.ascii	"Central device connected to FL slave %c\000"
.LC10:
	.ascii	"delaying startup scan by 30 seconds\000"
.LC11:
	.ascii	"Test Message rcvd from 2000e %c%c%c\000"
.LC12:
	.ascii	"Incoming 2000e based message not processed\000"
.LC13:
	.ascii	"UNK command in scan msg\000"
.LC14:
	.ascii	"Incoming SCAN not processed\000"
.LC15:
	.ascii	"Scanning w/o valid code d&t!\000"
.LC16:
	.ascii	"Scan resp: contact out of range : %s, %u\000"
.LC17:
	.ascii	"Scan resp: char out of range : %s, %u\000"
.LC18:
	.ascii	"Unexpected scan response : %s, %u\000"
.LC19:
	.ascii	"Controller %c is a NEW chain member\000"
.LC20:
	.ascii	"Incoming SCAN_RESP not processed\000"
.LC21:
	.ascii	"Unknown command : %s, %u\000"
.LC22:
	.ascii	"Incoming TOKEN not processed\000"
.LC23:
	.ascii	"token resp\000"
.LC24:
	.ascii	"Incoming TOKEN_RESP not processed\000"
.LC25:
	.ascii	"UNK incoming 3000 message class to process\000"
.LC26:
	.ascii	"Daily Token Response Errors: for %c, %u errors\000"
.LC27:
	.ascii	"COMM_MNGR: unk command to use\000"
.LC28:
	.ascii	"Unexpd COMM_MNGR state!\000"
.LC29:
	.ascii	"OM say status didn't arrive!\000"
.LC30:
	.ascii	"CommMngr : conflicting state : %s, %u\000"
.LC31:
	.ascii	"transport msg\000"
.LC32:
	.ascii	"COMM_MNGR : unexpected msg_class from tpl\000"
.LC33:
	.ascii	"slave rqsted scan\000"
.LC34:
	.ascii	"Why are we using these flags?\000"
.LC35:
	.ascii	"COMM MNGR unexpected isp file read event\000"
.LC36:
	.ascii	"Unexp TPMICRO file version event\000"
.LC37:
	.ascii	"No device exchange process function\000"
	.section	.bss.last_contact,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	last_contact, %object
	.size	last_contact, 20
last_contact:
	.space	20
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI0-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI1-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI2-.LFB23
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI3-.LFB26
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI4-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI5-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI7-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI8-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI9-.LFB29
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI11-.LFB32
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI12-.LFB39
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI13-.LFB40
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI14-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI16-.LFB35
	.byte	0xe
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI19-.LFB41
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI20-.LCFI19
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI21-.LFB38
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xe
	.uleb128 0xcc
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI23-.LFB42
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI24-.LFB43
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE54:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/comm_mngr.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x2f4
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF44
	.byte	0x1
	.4byte	.LASF45
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x504
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xc2
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x588
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x4dd
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x5a8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1b9
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x4c1
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x31e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x26d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x136
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x99f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1d8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x187
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x962
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x142
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x1cd
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x1eb
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x24f
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST0
	.uleb128 0x6
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x25b
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST1
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x4a8
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST2
	.uleb128 0x7
	.4byte	0x21
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST3
	.uleb128 0x7
	.4byte	0x2a
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.byte	0xd6
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST5
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.byte	0xeb
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST6
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x159
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST7
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x17c
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST8
	.uleb128 0xa
	.4byte	0x57
	.4byte	.LFB24
	.4byte	.LFE24
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x5dc
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST9
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x84f
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST10
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x6d9
	.4byte	.LFB30
	.4byte	.LFE30
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.2byte	0xb59
	.4byte	.LFB37
	.4byte	.LFE37
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF28
	.byte	0x1
	.2byte	0xd93
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST11
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.2byte	0xd9f
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST12
	.uleb128 0x6
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x1ff
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF31
	.byte	0x1
	.2byte	0xa10
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST14
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.2byte	0xdad
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST15
	.uleb128 0x2
	.4byte	.LASF33
	.byte	0x1
	.2byte	0xa4e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x27a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x351
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x702
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x20c
	.byte	0x1
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.2byte	0xb7b
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST16
	.uleb128 0x5
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x265
	.4byte	.LFB18
	.4byte	.LFE18
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x247
	.4byte	.LFB15
	.4byte	.LFE15
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x1f7
	.4byte	.LFB12
	.4byte	.LFE12
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF42
	.byte	0x1
	.2byte	0xdc4
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST17
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF43
	.byte	0x1
	.2byte	0xde1
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST18
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB16
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB17
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB23
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB26
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB0
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB1
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB5
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB6
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB29
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI10
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB32
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB39
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB40
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI15
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB35
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI18
	.4byte	.LFE35
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB41
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI20
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB38
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI22
	.4byte	.LFE38
	.2byte	0x3
	.byte	0x7d
	.sleb128 204
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB42
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB43
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xf4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF35:
	.ascii	"process_incoming_scan_RESP\000"
.LASF36:
	.ascii	"attempt_to_kick_out_the_next_token\000"
.LASF44:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF38:
	.ascii	"COMM_MNGR_task\000"
.LASF39:
	.ascii	"device_exchange_timer_callback\000"
.LASF30:
	.ascii	"message_rescan_timer_callback\000"
.LASF22:
	.ascii	"COMM_MNGR_number_of_controllers_in_the_chain_based_"
	.ascii	"upon_chain_members\000"
.LASF3:
	.ascii	"if_any_NEW_in_the_chain_then_distribute_all_groups\000"
.LASF5:
	.ascii	"power_up_device\000"
.LASF14:
	.ascii	"nm_number_of_live_ones\000"
.LASF10:
	.ascii	"transport_says_last_OM_failed\000"
.LASF18:
	.ascii	"stop_message_resp_timer\000"
.LASF0:
	.ascii	"nm_set_next_contact_index_and_command\000"
.LASF24:
	.ascii	"COMM_MNGR_start_a_scan\000"
.LASF4:
	.ascii	"transition_to_chain_down\000"
.LASF13:
	.ascii	"process_message_resp_timer_expired\000"
.LASF19:
	.ascii	"test_last_incoming_resp_integrity\000"
.LASF1:
	.ascii	"hide_hardware_based_upon_scan_results\000"
.LASF2:
	.ascii	"build_up_the_next_scan_message\000"
.LASF20:
	.ascii	"power_down_device\000"
.LASF21:
	.ascii	"COMM_MNGR_network_is_available_for_normal_use\000"
.LASF34:
	.ascii	"build_and_send_a_scan_resp\000"
.LASF8:
	.ascii	"post_attempt_to_send_next_token\000"
.LASF28:
	.ascii	"COMM_MNGR_device_exchange_results_to_key_process_ta"
	.ascii	"sk\000"
.LASF26:
	.ascii	"tpmicro_is_available_for_token_generation\000"
.LASF45:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/comm_mngr.c\000"
.LASF33:
	.ascii	"process_incoming_messages_list\000"
.LASF7:
	.ascii	"process_incoming_scan\000"
.LASF42:
	.ascii	"task_control_ENTER_device_exchange_hammer\000"
.LASF12:
	.ascii	"an_FL_slave_has_a_central_device\000"
.LASF37:
	.ascii	"rescan_timer_maintenance\000"
.LASF11:
	.ascii	"contact_timer_milliseconds\000"
.LASF43:
	.ascii	"task_control_EXIT_device_exchange_hammer\000"
.LASF41:
	.ascii	"token_rate_timer_callback\000"
.LASF27:
	.ascii	"setup_to_check_if_tpmicro_needs_a_code_update\000"
.LASF32:
	.ascii	"COMM_MNGR_post_event\000"
.LASF25:
	.ascii	"kick_out_the_next_scan_message\000"
.LASF16:
	.ascii	"token_arrival_timer_callback\000"
.LASF17:
	.ascii	"start_message_resp_timer\000"
.LASF23:
	.ascii	"COMM_MNGR_alert_if_chain_members_should_not_be_refe"
	.ascii	"renced\000"
.LASF31:
	.ascii	"CENT_COMM_make_copy_of_and_add_message_or_packet_to"
	.ascii	"_list_of_incoming_messages\000"
.LASF29:
	.ascii	"COMM_MNGR_post_event_with_details\000"
.LASF40:
	.ascii	"message_resp_timer_callback\000"
.LASF15:
	.ascii	"flowsense_device_connection_timer_callback\000"
.LASF6:
	.ascii	"COMM_MNGR_token_timeout_seconds\000"
.LASF9:
	.ascii	"bump_number_of_failures_to_respond_to_token\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
