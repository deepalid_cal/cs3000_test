	.file	"irrigation_system.c"
	.text
.Ltext0:
	.section	.text.nm_SYSTEM_set_budget_in_use,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_budget_in_use, %function
nm_SYSTEM_set_budget_in_use:
.LFB13:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	sub	sp, sp, #36
.LCFI1:
	str	r2, [sp, #0]
	ldr	r2, .L2
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #352
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #156
	str	r3, [sp, #24]
	mov	r3, #14
	str	r3, [sp, #28]
	ldr	r3, .L2+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L3:
	.align	2
.L2:
	.word	49369
	.word	.LC0
.LFE13:
	.size	nm_SYSTEM_set_budget_in_use, .-nm_SYSTEM_set_budget_in_use
	.section	.text.nm_SYSTEM_set_system_in_use,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_system_in_use, %function
nm_SYSTEM_set_system_in_use:
.LFB19:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI2:
	sub	sp, sp, #36
.LCFI3:
	str	r2, [sp, #0]
	ldr	r2, .L5
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #356
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #156
	str	r3, [sp, #24]
	mov	r3, #19
	str	r3, [sp, #28]
	ldr	r3, .L5+4
	str	r3, [sp, #32]
	mov	r3, #1
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L6:
	.align	2
.L5:
	.word	49414
	.word	.LC1
.LFE19:
	.size	nm_SYSTEM_set_system_in_use, .-nm_SYSTEM_set_system_in_use
	.section	.text.nm_SYSTEM_set_system_used_for_irri,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_system_used_for_irri, %function
nm_SYSTEM_set_system_used_for_irri:
.LFB0:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI4:
	sub	sp, sp, #36
.LCFI5:
	str	r2, [sp, #0]
	ldr	r2, .L8
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	ldr	r2, .L8+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	str	r2, [sp, #32]
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	add	r1, r0, #72
	str	r3, [sp, #20]
	add	r3, r0, #156
	str	r3, [sp, #24]
	mov	r2, lr
	mov	r3, #1
	str	r3, [sp, #28]
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L9:
	.align	2
.L8:
	.word	49185
	.word	.LC2
.LFE0:
	.size	nm_SYSTEM_set_system_used_for_irri, .-nm_SYSTEM_set_system_used_for_irri
	.section	.text.nm_SYSTEM_set_capacity_in_use,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_capacity_in_use, %function
nm_SYSTEM_set_capacity_in_use:
.LFB1:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI6:
	sub	sp, sp, #36
.LCFI7:
	str	r2, [sp, #0]
	ldr	r2, .L11
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #76
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #156
	str	r3, [sp, #24]
	mov	r3, #2
	str	r3, [sp, #28]
	ldr	r3, .L11+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L12:
	.align	2
.L11:
	.word	49186
	.word	.LC3
.LFE1:
	.size	nm_SYSTEM_set_capacity_in_use, .-nm_SYSTEM_set_capacity_in_use
	.section	.text.nm_SYSTEM_set_budget_mode,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_budget_mode, %function
nm_SYSTEM_set_budget_mode:
.LFB14:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI8:
	sub	sp, sp, #44
.LCFI9:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L14
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #156
	str	r3, [sp, #32]
	mov	r3, #18
	str	r3, [sp, #36]
	ldr	r3, .L14+4
	mov	ip, #0
	mov	r1, #1
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #244
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L15:
	.align	2
.L14:
	.word	49396
	.word	.LC4
.LFE14:
	.size	nm_SYSTEM_set_budget_mode, .-nm_SYSTEM_set_budget_mode
	.section	.text.nm_SYSTEM_set_budget_number_of_annual_periods,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_budget_number_of_annual_periods, %function
nm_SYSTEM_set_budget_number_of_annual_periods:
.LFB16:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI10:
	sub	sp, sp, #44
.LCFI11:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #12
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	stmib	sp, {r1, r2}
	ldr	r2, .L17
	str	r3, [sp, #28]
	add	r3, r0, #156
	str	r3, [sp, #32]
	mov	r3, #16
	str	r3, [sp, #36]
	ldr	r3, .L17+4
	str	r1, [sp, #0]
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #252
	mov	r2, lr
	mov	r3, #6
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L18:
	.align	2
.L17:
	.word	49371
	.word	.LC5
.LFE16:
	.size	nm_SYSTEM_set_budget_number_of_annual_periods, .-nm_SYSTEM_set_budget_number_of_annual_periods
	.section	.text.nm_SYSTEM_set_budget_meter_read_time,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_budget_meter_read_time, %function
nm_SYSTEM_set_budget_meter_read_time:
.LFB15:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI12:
	sub	sp, sp, #44
.LCFI13:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	ldr	r1, .L20
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	ldr	r1, .L20+4
	str	r3, [sp, #28]
	add	r3, r0, #156
	str	r3, [sp, #32]
	mov	r3, #15
	stmib	sp, {r1, r2}
	ldr	r2, .L20+8
	str	r3, [sp, #36]
	ldr	r3, .L20+12
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #248
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L21:
	.align	2
.L20:
	.word	86399
	.word	43200
	.word	49370
	.word	.LC6
.LFE15:
	.size	nm_SYSTEM_set_budget_meter_read_time, .-nm_SYSTEM_set_budget_meter_read_time
	.section	.text.nm_SYSTEM_set_capacity_with_pump,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_capacity_with_pump, %function
nm_SYSTEM_set_capacity_with_pump:
.LFB2:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI14:
	sub	sp, sp, #44
.LCFI15:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #4000
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #156
	mov	r1, #200
	str	r3, [sp, #32]
	mov	r3, #3
	stmib	sp, {r1, r2}
	ldr	r2, .L23
	str	r3, [sp, #36]
	ldr	r3, .L23+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #80
	mov	r2, lr
	mov	r3, #1
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L24:
	.align	2
.L23:
	.word	49187
	.word	.LC7
.LFE2:
	.size	nm_SYSTEM_set_capacity_with_pump, .-nm_SYSTEM_set_capacity_with_pump
	.section	.text.nm_SYSTEM_set_capacity_without_pump,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_capacity_without_pump, %function
nm_SYSTEM_set_capacity_without_pump:
.LFB3:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI16:
	sub	sp, sp, #44
.LCFI17:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #4000
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #156
	mov	r1, #200
	str	r3, [sp, #32]
	mov	r3, #4
	stmib	sp, {r1, r2}
	ldr	r2, .L26
	str	r3, [sp, #36]
	ldr	r3, .L26+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #84
	mov	r2, lr
	mov	r3, #1
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L27:
	.align	2
.L26:
	.word	49188
	.word	.LC8
.LFE3:
	.size	nm_SYSTEM_set_capacity_without_pump, .-nm_SYSTEM_set_capacity_without_pump
	.section	.text.nm_SYSTEM_set_mlb_during_irri,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_mlb_during_irri, %function
nm_SYSTEM_set_mlb_during_irri:
.LFB4:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI18:
	sub	sp, sp, #44
.LCFI19:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #4000
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #156
	mov	r1, #400
	str	r3, [sp, #32]
	mov	r3, #5
	stmib	sp, {r1, r2}
	ldr	r2, .L29
	str	r3, [sp, #36]
	ldr	r3, .L29+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #88
	mov	r2, lr
	mov	r3, #1
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L30:
	.align	2
.L29:
	.word	49189
	.word	.LC9
.LFE4:
	.size	nm_SYSTEM_set_mlb_during_irri, .-nm_SYSTEM_set_mlb_during_irri
	.section	.text.nm_SYSTEM_set_mlb_during_mvor,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_mlb_during_mvor, %function
nm_SYSTEM_set_mlb_during_mvor:
.LFB5:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI20:
	sub	sp, sp, #44
.LCFI21:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #4000
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #156
	mov	r1, #150
	str	r3, [sp, #32]
	mov	r3, #6
	stmib	sp, {r1, r2}
	ldr	r2, .L32
	str	r3, [sp, #36]
	ldr	r3, .L32+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #92
	mov	r2, lr
	mov	r3, #1
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L33:
	.align	2
.L32:
	.word	49190
	.word	.LC10
.LFE5:
	.size	nm_SYSTEM_set_mlb_during_mvor, .-nm_SYSTEM_set_mlb_during_mvor
	.section	.text.nm_SYSTEM_set_mlb_all_other_times,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_mlb_all_other_times, %function
nm_SYSTEM_set_mlb_all_other_times:
.LFB6:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI22:
	sub	sp, sp, #44
.LCFI23:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #4000
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #156
	mov	r1, #150
	str	r3, [sp, #32]
	mov	r3, #7
	stmib	sp, {r1, r2}
	ldr	r2, .L35
	str	r3, [sp, #36]
	ldr	r3, .L35+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #96
	mov	r2, lr
	mov	r3, #1
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L36:
	.align	2
.L35:
	.word	49191
	.word	.LC11
.LFE6:
	.size	nm_SYSTEM_set_mlb_all_other_times, .-nm_SYSTEM_set_mlb_all_other_times
	.section	.text.nm_SYSTEM_set_flow_checking_in_use,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_flow_checking_in_use, %function
nm_SYSTEM_set_flow_checking_in_use:
.LFB7:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI24:
	sub	sp, sp, #36
.LCFI25:
	str	r2, [sp, #0]
	ldr	r2, .L39
	mov	ip, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #44]
	add	r1, r0, #100
	str	r3, [sp, #12]
	ldr	r3, [sp, #48]
	mov	r2, ip
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	mov	r4, r0
	str	r3, [sp, #20]
	add	r3, r0, #156
	str	r3, [sp, #24]
	mov	r3, #8
	str	r3, [sp, #28]
	ldr	r3, .L39+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L37
	ldr	r0, [r4, #16]
	add	sp, sp, #36
	ldmfd	sp!, {r4, lr}
	b	SYSTEM_PRESERVES_request_a_resync
.L37:
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L40:
	.align	2
.L39:
	.word	49192
	.word	.LC12
.LFE7:
	.size	nm_SYSTEM_set_flow_checking_in_use, .-nm_SYSTEM_set_flow_checking_in_use
	.section	.text.nm_SYSTEM_set_budget_flow_type,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_budget_flow_type, %function
nm_SYSTEM_set_budget_flow_type:
.LFB18:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #9
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI26:
	mov	r5, r0
	sub	sp, sp, #68
.LCFI27:
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	bhi	.L42
.LBB23:
	mov	r0, r1
	bl	GetFlowTypeStr
	add	r6, sp, #36
	mov	r1, #32
	ldr	r2, .L44
	ldr	r3, .L44+4
	str	r0, [sp, #0]
	mov	r0, r6
	bl	snprintf
	ldr	r3, [sp, #92]
	add	r1, r4, #92
	str	r3, [sp, #8]
	ldr	r3, [sp, #96]
	add	r4, r4, #49408
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	add	r4, r4, #44
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	mov	r0, r5
	str	r3, [sp, #20]
	add	r3, r5, #156
	str	r3, [sp, #24]
	mov	r3, #20
	str	r3, [sp, #28]
	add	r1, r5, r1, asl #2
	mov	r2, r8
	mov	r3, #1
	str	r7, [sp, #0]
	str	r4, [sp, #4]
	str	r6, [sp, #32]
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	b	.L41
.L42:
.LBE23:
	ldr	r0, .L44+8
	mov	r1, #1552
	bl	Alert_index_out_of_range_with_filename
.L41:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L45:
	.align	2
.L44:
	.word	.LC13
	.word	.LC14
	.word	.LC15
.LFE18:
	.size	nm_SYSTEM_set_budget_flow_type, .-nm_SYSTEM_set_budget_flow_type
	.section	.text.nm_SYSTEM_set_mvor_schedule_open_time,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_mvor_schedule_open_time, %function
nm_SYSTEM_set_mvor_schedule_open_time:
.LFB11:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #6
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI28:
	mov	r5, r0
	sub	sp, sp, #76
.LCFI29:
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	bhi	.L47
.LBB26:
	add	r6, sp, #44
	str	r1, [sp, #0]
	mov	r0, r6
	mov	r1, #32
	ldr	r2, .L49
	ldr	r3, .L49+4
	bl	snprintf
	ldr	r3, .L49+8
	add	r1, r4, #47
	str	r3, [sp, #0]
	stmib	sp, {r3, r7}
	ldr	r3, [sp, #100]
	add	r4, r4, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	add	r4, r4, #203
	str	r3, [sp, #20]
	ldr	r3, [sp, #108]
	mov	r0, r5
	str	r3, [sp, #24]
	ldr	r3, [sp, #112]
	add	r1, r5, r1, asl #2
	str	r3, [sp, #28]
	add	r3, r5, #156
	str	r3, [sp, #32]
	mov	r3, #12
	str	r3, [sp, #36]
	mov	r2, r8
	mov	r3, #0
	str	r4, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L46
.L47:
.LBE26:
	ldr	r0, .L49+12
	ldr	r1, .L49+16
	bl	Alert_index_out_of_range_with_filename
.L46:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L50:
	.align	2
.L49:
	.word	.LC16
	.word	.LC17
	.word	86400
	.word	.LC15
	.word	1221
.LFE11:
	.size	nm_SYSTEM_set_mvor_schedule_open_time, .-nm_SYSTEM_set_mvor_schedule_open_time
	.section	.text.nm_SYSTEM_set_mvor_schedule_close_time,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_mvor_schedule_close_time, %function
nm_SYSTEM_set_mvor_schedule_close_time:
.LFB12:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #6
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI30:
	mov	r5, r0
	sub	sp, sp, #76
.LCFI31:
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	bhi	.L52
.LBB29:
	add	r6, sp, #44
	str	r1, [sp, #0]
	mov	r0, r6
	mov	r1, #32
	ldr	r2, .L54
	ldr	r3, .L54+4
	bl	snprintf
	ldr	r3, .L54+8
	add	r1, r4, #54
	str	r3, [sp, #0]
	stmib	sp, {r3, r7}
	ldr	r3, [sp, #100]
	add	r4, r4, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	add	r4, r4, #210
	str	r3, [sp, #20]
	ldr	r3, [sp, #108]
	mov	r0, r5
	str	r3, [sp, #24]
	ldr	r3, [sp, #112]
	add	r1, r5, r1, asl #2
	str	r3, [sp, #28]
	add	r3, r5, #156
	str	r3, [sp, #32]
	mov	r3, #13
	str	r3, [sp, #36]
	mov	r2, r8
	mov	r3, #0
	str	r4, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L51
.L52:
.LBE29:
	ldr	r0, .L54+12
	ldr	r1, .L54+16
	bl	Alert_index_out_of_range_with_filename
.L51:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L55:
	.align	2
.L54:
	.word	.LC16
	.word	.LC18
	.word	86400
	.word	.LC15
	.word	1277
.LFE12:
	.size	nm_SYSTEM_set_mvor_schedule_close_time, .-nm_SYSTEM_set_mvor_schedule_close_time
	.section	.text.nm_SYSTEM_set_budget_period,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_set_budget_period
	.type	nm_SYSTEM_set_budget_period, %function
nm_SYSTEM_set_budget_period:
.LFB17:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #23
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI32:
	mov	r5, r0
	sub	sp, sp, #76
.LCFI33:
	mov	r4, r1
	mov	fp, r2
	mov	r9, r3
	bhi	.L57
.LBB32:
	add	r6, sp, #44
	add	r3, r1, #1
	str	r3, [sp, #0]
	mov	r0, r6
	ldr	r3, .L59
	mov	r1, #32
	ldr	r2, .L59+4
	bl	snprintf
	mov	r0, #1
	mov	r1, r0
	ldr	r2, .L59+8
	bl	DMYToDate
	mov	r1, #12
	ldr	r2, .L59+12
	add	r7, r4, #64
	add	r7, r5, r7, asl #2
	add	r4, r4, #49152
	add	r4, r4, #220
	mov	sl, r0
	mov	r0, #31
	bl	DMYToDate
	ldr	r2, .L59+8
	mov	r8, r0
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	ldr	r2, [sp, #112]
	mov	r3, sl, asl #16
	str	r2, [sp, #16]
	ldr	r2, [sp, #116]
	mov	r8, r8, asl #16
	str	r2, [sp, #20]
	ldr	r2, [sp, #120]
	mov	r8, r8, lsr #16
	str	r2, [sp, #24]
	ldr	r2, [sp, #124]
	mov	r1, r7
	str	r2, [sp, #28]
	add	r2, r5, #156
	str	r2, [sp, #32]
	mov	r2, #17
	str	r2, [sp, #36]
	mov	r3, r3, lsr #16
	mov	r2, fp
	str	r8, [sp, #0]
	str	r4, [sp, #12]
	str	r6, [sp, #40]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	stmib	sp, {r0, r9}
	mov	r0, r5
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L56
.L57:
.LBE32:
	ldr	r0, .L59+16
	ldr	r1, .L59+20
	bl	Alert_index_out_of_range_with_filename
.L56:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L60:
	.align	2
.L59:
	.word	.LC19
	.word	.LC16
	.word	2011
	.word	2042
	.word	.LC15
	.word	1490
.LFE17:
	.size	nm_SYSTEM_set_budget_period, .-nm_SYSTEM_set_budget_period
	.section	.text.nm_irrigation_system_structure_updater,"ax",%progbits
	.align	2
	.type	nm_irrigation_system_structure_updater, %function
nm_irrigation_system_structure_updater:
.LFB22:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI34:
	sub	sp, sp, #36
.LCFI35:
	mov	r4, r0
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #8
	mov	r5, r0
	bne	.L62
	ldr	r0, .L101
	mov	r1, r4
	bl	Alert_Message_va
	b	.L61
.L62:
	ldr	r0, .L101+4
	mov	r1, #8
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	bne	.L64
	ldr	r0, .L101+8
	bl	nm_ListGetFirst
	mvn	r4, #0
	mov	r1, r0
	b	.L65
.L66:
	str	r4, [r1, #156]
	ldr	r0, .L101+8
	bl	nm_ListGetNext
	mov	r1, r0
.L65:
	cmp	r1, #0
	bne	.L66
	b	.L67
.L64:
	cmp	r4, #1
	bne	.L68
.L67:
	ldr	r0, .L101+8
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L69
.L70:
	str	r4, [r1, #184]
	ldr	r0, .L101+8
	bl	nm_ListGetNext
	mov	r1, r0
.L69:
	cmp	r1, #0
	bne	.L70
	b	.L71
.L68:
	cmp	r4, #2
	bne	.L72
.L71:
	ldr	r0, .L101+8
	bl	nm_ListGetFirst
	mov	sl, #11
	mov	r4, r0
	b	.L73
.L75:
	add	r7, r4, #148
	mov	r6, #0
	mov	r8, #1
.L74:
	mov	r1, r6
	mov	r0, r4
	ldr	r2, .L101+12
	mov	r3, #0
	str	sl, [sp, #0]
	stmib	sp, {r5, r8}
	str	r7, [sp, #12]
	bl	nm_SYSTEM_set_mvor_schedule_open_time
	mov	r1, r6
	mov	r0, r4
	ldr	r2, .L101+12
	mov	r3, #0
	add	r6, r6, #1
	str	sl, [sp, #0]
	stmib	sp, {r5, r8}
	str	r7, [sp, #12]
	bl	nm_SYSTEM_set_mvor_schedule_close_time
	cmp	r6, #7
	bne	.L74
	mov	r1, r4
	ldr	r0, .L101+8
	bl	nm_ListGetNext
	mov	r4, r0
.L73:
	cmp	r4, #0
	bne	.L75
	ldr	r3, .L101+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L101+20
	ldr	r3, .L101+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r8, .L101+28
	ldr	r6, .L101+32
	mov	r7, r4
.L76:
	mov	r0, #1
	mov	r1, r0
	ldr	r2, .L101+36
	bl	DMYToDate
	add	r3, r4, r8
	add	r4, r4, #14208
	add	r4, r4, #16
	add	r3, r3, #14080
	cmp	r4, r6
	str	r7, [r3, #56]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	str	r0, [r3, #52]
	bne	.L76
	ldr	r3, .L101+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L79
.L72:
	cmp	r4, #3
	beq	.L79
	cmp	r4, #4
	bne	.L80
.L79:
	ldr	r0, .L101+8
	bl	nm_ListGetFirst
	mov	sl, #11
	mov	r4, r0
	b	.L81
.L83:
	add	r7, r4, #148
	mov	r6, #0
	mov	r8, #1
.L82:
	mov	r1, r6
	mov	r0, r4
	ldr	r2, .L101+12
	mov	r3, #0
	str	sl, [sp, #0]
	stmib	sp, {r5, r8}
	str	r7, [sp, #12]
	bl	nm_SYSTEM_set_mvor_schedule_open_time
	mov	r1, r6
	mov	r0, r4
	ldr	r2, .L101+12
	mov	r3, #0
	add	r6, r6, #1
	str	sl, [sp, #0]
	stmib	sp, {r5, r8}
	str	r7, [sp, #12]
	bl	nm_SYSTEM_set_mvor_schedule_close_time
	cmp	r6, #7
	bne	.L82
	mov	r1, r4
	ldr	r0, .L101+8
	bl	nm_ListGetNext
	mov	r4, r0
.L81:
	cmp	r4, #0
	bne	.L83
	ldr	r3, .L101+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L101+20
	ldr	r3, .L101+40
	bl	xQueueTakeMutexRecursive_debug
	ldr	r8, .L101+28
	ldr	r6, .L101+32
	mov	r7, r4
.L84:
	mov	r0, #1
	mov	r1, r0
	ldr	r2, .L101+36
	bl	DMYToDate
	add	r3, r4, r8
	add	r4, r4, #14208
	add	r4, r4, #16
	add	r3, r3, #14080
	cmp	r4, r6
	str	r7, [r3, #56]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	str	r0, [r3, #52]
	bne	.L84
	ldr	r3, .L101+16
	mov	r4, #5
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L80:
	sub	r3, r4, #5
	cmp	r3, #1
	bhi	.L85
	ldr	r0, .L101+8
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r4, r0
	b	.L86
.L87:
	add	r3, r4, #148
	mov	r0, r4
	str	r3, [sp, #8]
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6}
	bl	nm_SYSTEM_set_system_in_use
	mov	r1, r4
	ldr	r0, .L101+8
	bl	nm_ListGetNext
	mov	r4, r0
.L86:
	cmp	r4, #0
	bne	.L87
	b	.L88
.L85:
	cmp	r4, #7
	bne	.L89
.L88:
	ldr	r0, .L101+8
	bl	nm_ListGetFirst
	mov	fp, #0
	mov	r4, r0
	b	.L90
.L94:
	mov	r1, #0
	add	r7, r4, #148
	mov	sl, #1
	mov	r2, r1
	mov	r0, r4
	mov	r3, #11
	str	r5, [sp, #0]
	str	sl, [sp, #4]
	str	r7, [sp, #8]
	bl	nm_SYSTEM_set_budget_in_use
	mov	r1, #0
	mov	r2, r1
	mov	r0, r4
	mov	r3, #11
	stmia	sp, {r5, sl}
	str	r7, [sp, #8]
	bl	nm_SYSTEM_set_budget_mode
	mov	r0, r4
	ldr	r1, .L101+44
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, sl}
	str	r7, [sp, #8]
	bl	nm_SYSTEM_set_budget_meter_read_time
	add	r0, sp, #16
	bl	EPSON_obtain_latest_complete_time_and_date
	ldrh	r6, [sp, #24]
	ldrh	r9, [sp, #26]
	mov	r8, #0
.L92:
	mov	r1, r6
	mov	r2, r9
	mov	r0, #1
	bl	DMYToDate
	mov	r3, #11
	stmia	sp, {r3, r5, sl}
	mov	r1, r8
	add	r6, r6, #1
	mov	r3, #0
	str	r7, [sp, #12]
	add	r8, r8, #1
	mov	r2, r0
	mov	r0, r4
	bl	nm_SYSTEM_set_budget_period
	cmp	r6, #12
	addhi	r9, r9, #1
	movhi	r6, #1
	cmp	r8, #24
	bne	.L92
	mov	r6, #0
	mov	r8, #11
	mov	sl, #1
.L93:
	ldr	r3, .L101+48
	mov	r1, r6
	mov	r2, r3, lsr r6
	mov	r0, r4
	and	r2, r2, #1
	mov	r3, #0
	add	r6, r6, #1
	str	r8, [sp, #0]
	stmib	sp, {r5, sl}
	str	r7, [sp, #12]
	bl	nm_SYSTEM_set_budget_flow_type
	cmp	r6, #10
	bne	.L93
	str	fp, [r4, #360]	@ float
	str	fp, [r4, #364]	@ float
	mov	r1, r4
	ldr	r0, .L101+8
	bl	nm_ListGetNext
	mov	r4, r0
.L90:
	cmp	r4, #0
	bne	.L94
	b	.L61
.L89:
	cmp	r4, #8
	beq	.L61
	ldr	r0, .L101+52
	bl	Alert_Message
.L61:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L102:
	.align	2
.L101:
	.word	.LC20
	.word	.LC21
	.word	.LANCHOR0
	.word	86400
	.word	system_preserves_recursive_MUTEX
	.word	.LC22
	.word	463
	.word	system_preserves
	.word	56896
	.word	2011
	.word	513
	.word	43200
	.word	1015
	.word	.LC23
.LFE22:
	.size	nm_irrigation_system_structure_updater, .-nm_irrigation_system_structure_updater
	.section	.text.nm_SYSTEM_set_flow_checking_range.part.4,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_flow_checking_range.part.4, %function
nm_SYSTEM_set_flow_checking_range.part.4:
.LFB101:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI36:
	sub	sp, sp, #76
.LCFI37:
	add	r6, sp, #44
	mov	r4, r0
	mov	r5, r1
	mov	r8, r2
	mov	r7, r3
	str	r1, [sp, #0]
	ldr	r2, .L105
	mov	r1, #32
	ldr	r3, .L105+4
	mov	r0, r6
	bl	snprintf
	mov	r3, #4000
	str	r3, [sp, #0]
	ldr	r3, .L105+8
	sub	r2, r5, #1
	ldr	r3, [r3, r2, asl #2]
	add	r1, r5, #25
	stmib	sp, {r3, r7}
	ldr	r3, [sp, #100]
	add	r5, r5, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	add	r5, r5, #40
	str	r3, [sp, #20]
	ldr	r3, [sp, #108]
	mov	r0, r4
	str	r3, [sp, #24]
	ldr	r3, [sp, #112]
	add	r1, r4, r1, asl #2
	str	r3, [sp, #28]
	add	r3, r4, #156
	str	r3, [sp, #32]
	mov	r3, #9
	str	r3, [sp, #36]
	mov	r2, r8
	mov	r3, #10
	str	r5, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L103
	ldr	r0, [r4, #16]
	bl	SYSTEM_PRESERVES_request_a_resync
.L103:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L106:
	.align	2
.L105:
	.word	.LC16
	.word	.LC24
	.word	.LANCHOR1
.LFE101:
	.size	nm_SYSTEM_set_flow_checking_range.part.4, .-nm_SYSTEM_set_flow_checking_range.part.4
	.section	.text.nm_SYSTEM_set_flow_checking_tolerance_plus.part.5,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_flow_checking_tolerance_plus.part.5, %function
nm_SYSTEM_set_flow_checking_tolerance_plus.part.5:
.LFB102:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI38:
	sub	sp, sp, #76
.LCFI39:
	add	r6, sp, #44
	mov	r4, r0
	mov	r5, r1
	mov	r8, r2
	mov	r7, r3
	str	r1, [sp, #0]
	ldr	r2, .L109
	mov	r1, #32
	ldr	r3, .L109+4
	mov	r0, r6
	bl	snprintf
	mov	r3, #200
	str	r3, [sp, #0]
	ldr	r3, .L109+8
	sub	r2, r5, #1
	ldr	r3, [r3, r2, asl #2]
	add	r1, r5, #28
	stmib	sp, {r3, r7}
	ldr	r3, [sp, #100]
	add	r5, r5, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	add	r5, r5, #43
	str	r3, [sp, #20]
	ldr	r3, [sp, #108]
	mov	r0, r4
	str	r3, [sp, #24]
	ldr	r3, [sp, #112]
	add	r1, r4, r1, asl #2
	str	r3, [sp, #28]
	add	r3, r4, #156
	str	r3, [sp, #32]
	mov	r3, #10
	str	r3, [sp, #36]
	mov	r2, r8
	mov	r3, #1
	str	r5, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L107
	ldr	r0, [r4, #16]
	bl	SYSTEM_PRESERVES_request_a_resync
.L107:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L110:
	.align	2
.L109:
	.word	.LC16
	.word	.LC25
	.word	.LANCHOR2
.LFE102:
	.size	nm_SYSTEM_set_flow_checking_tolerance_plus.part.5, .-nm_SYSTEM_set_flow_checking_tolerance_plus.part.5
	.section	.text.nm_SYSTEM_set_flow_checking_tolerance_minus.part.6,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_flow_checking_tolerance_minus.part.6, %function
nm_SYSTEM_set_flow_checking_tolerance_minus.part.6:
.LFB103:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI40:
	sub	sp, sp, #76
.LCFI41:
	add	r6, sp, #44
	mov	r4, r0
	mov	r5, r1
	mov	r8, r2
	mov	r7, r3
	str	r1, [sp, #0]
	ldr	r2, .L113
	mov	r1, #32
	ldr	r3, .L113+4
	mov	r0, r6
	bl	snprintf
	mov	r3, #200
	str	r3, [sp, #0]
	ldr	r3, .L113+8
	sub	r2, r5, #1
	ldr	r3, [r3, r2, asl #2]
	add	r1, r5, #32
	stmib	sp, {r3, r7}
	ldr	r3, [sp, #100]
	add	r5, r5, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	add	r5, r5, #47
	str	r3, [sp, #20]
	ldr	r3, [sp, #108]
	mov	r0, r4
	str	r3, [sp, #24]
	ldr	r3, [sp, #112]
	add	r1, r4, r1, asl #2
	str	r3, [sp, #28]
	add	r3, r4, #156
	str	r3, [sp, #32]
	mov	r3, #11
	str	r3, [sp, #36]
	mov	r2, r8
	mov	r3, #1
	str	r5, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L111
	ldr	r0, [r4, #16]
	bl	SYSTEM_PRESERVES_request_a_resync
.L111:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L114:
	.align	2
.L113:
	.word	.LC16
	.word	.LC26
	.word	.LANCHOR2
.LFE103:
	.size	nm_SYSTEM_set_flow_checking_tolerance_minus.part.6, .-nm_SYSTEM_set_flow_checking_tolerance_minus.part.6
	.section	.text.nm_SYSTEM_set_default_values,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_set_default_values, %function
nm_SYSTEM_set_default_values:
.LFB25:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI42:
	sub	sp, sp, #36
.LCFI43:
	mov	r4, r0
	mov	r8, r1
	bl	FLOWSENSE_get_controller_index
	mov	r6, r0
	add	r0, sp, #16
	bl	EPSON_obtain_latest_complete_time_and_date
	cmp	r4, #0
	beq	.L116
.LBB33:
	ldr	r7, .L130
	add	r5, r4, #148
	ldr	r1, [r7, #0]
	mov	r0, r5
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r7, #0]
	add	r0, r4, #152
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r7, #0]
	add	r0, r4, #184
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r7, #0]
	add	r0, r4, #156
	bl	SHARED_set_all_32_bit_change_bits
	mov	r0, r4
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r8}
	ldr	sl, .L130+4
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_system_used_for_irri
	mov	r0, r4
	mov	r1, #200
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_capacity_with_pump
	mov	r0, r4
	mov	r1, #200
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_capacity_without_pump
	mov	r0, r4
	mov	r1, #400
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_mlb_during_irri
	mov	r0, r4
	mov	r1, #150
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_mlb_during_mvor
	mov	r0, r4
	mov	r1, #150
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_mlb_all_other_times
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_flow_checking_in_use
	mov	r7, #0
	mov	r9, #11
.L117:
	add	r7, r7, #1
	str	r9, [sp, #0]
	stmib	sp, {r6, r8}
	str	r5, [sp, #12]
	mov	r0, r4
	mov	r1, r7
	ldr	r2, [sl, #4]!
	mov	r3, #0
	bl	nm_SYSTEM_set_flow_checking_range.part.4
	cmp	r7, #3
	bne	.L117
	ldr	r9, .L130+8
	mov	r7, #0
	mov	fp, #11
.L118:
	ldr	sl, [r9, #4]!
	add	r7, r7, #1
	mov	r0, r4
	mov	r1, r7
	mov	r2, sl
	mov	r3, #0
	str	fp, [sp, #0]
	stmib	sp, {r6, r8}
	str	r5, [sp, #12]
	bl	nm_SYSTEM_set_flow_checking_tolerance_plus.part.5
	mov	r0, r4
	mov	r1, r7
	mov	r2, sl
	mov	r3, #0
	str	fp, [sp, #0]
	stmib	sp, {r6, r8}
	str	r5, [sp, #12]
	bl	nm_SYSTEM_set_flow_checking_tolerance_minus.part.6
	cmp	r7, #4
	bne	.L118
	mov	r3, #1
	str	r3, [r4, #160]
	str	r3, [r4, #164]
	mov	r3, #10
	str	r3, [r4, #168]
	mov	r3, #500
	mov	r1, #0
	str	r3, [r4, #172]
	mov	r3, #6
	mov	r2, r1
	str	r3, [r4, #176]
	str	r3, [r4, #180]
	mov	r0, r4
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_budget_mode
	mov	r0, r4
	ldr	r1, .L130+12
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_budget_meter_read_time
	mov	r0, r4
	mov	r1, #12
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_budget_number_of_annual_periods
	ldrh	r7, [sp, #24]
	ldrh	r9, [sp, #26]
	mov	sl, #0
	mov	fp, #11
.L120:
	mov	r1, r7
	mov	r2, r9
	mov	r0, #1
	bl	DMYToDate
	add	r7, r7, #1
	mov	r1, sl
	mov	r3, #0
	str	fp, [sp, #0]
	stmib	sp, {r6, r8}
	str	r5, [sp, #12]
	add	sl, sl, #1
	mov	r2, r0
	mov	r0, r4
	bl	nm_SYSTEM_set_budget_period
	cmp	r7, #12
	addhi	r9, r9, #1
	movhi	r7, #1
	cmp	sl, #24
	bne	.L120
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r6, r8}
	ldr	sl, .L130+16
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_budget_in_use
	mov	r7, #0
	mov	r9, #11
.L122:
	mov	r3, sl, lsr r7
	tst	r3, #1
	beq	.L121
	mov	r0, r4
	mov	r1, r7
	mov	r2, #1
	mov	r3, #0
	str	r9, [sp, #0]
	stmib	sp, {r6, r8}
	str	r5, [sp, #12]
	bl	nm_SYSTEM_set_budget_flow_type
.L121:
	add	r7, r7, #1
	cmp	r7, #10
	bne	.L122
	mov	r7, #0
	mov	sl, #11
.L123:
	mov	r1, r7
	mov	r0, r4
	ldr	r2, .L130+20
	mov	r3, #0
	str	sl, [sp, #0]
	stmib	sp, {r6, r8}
	str	r5, [sp, #12]
	bl	nm_SYSTEM_set_mvor_schedule_open_time
	mov	r1, r7
	mov	r0, r4
	ldr	r2, .L130+20
	mov	r3, #0
	add	r7, r7, #1
	str	sl, [sp, #0]
	stmib	sp, {r6, r8}
	str	r5, [sp, #12]
	bl	nm_SYSTEM_set_mvor_schedule_close_time
	cmp	r7, #7
	bne	.L123
	mov	r0, r4
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_SYSTEM_set_system_in_use
	b	.L115
.L116:
.LBE33:
	ldr	r0, .L130+24
	mov	r1, #820
	bl	Alert_func_call_with_null_ptr_with_filename
.L115:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L131:
	.align	2
.L130:
	.word	list_system_recursive_MUTEX
	.word	.LANCHOR1-4
	.word	.LANCHOR2-4
	.word	43200
	.word	1015
	.word	86400
	.word	.LC22
.LFE25:
	.size	nm_SYSTEM_set_default_values, .-nm_SYSTEM_set_default_values
	.section	.text.init_file_irrigation_system,"ax",%progbits
	.align	2
	.global	init_file_irrigation_system
	.type	init_file_irrigation_system, %function
init_file_irrigation_system:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI44:
	ldr	r3, .L133
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, .L133+4
	ldr	r1, .L133+8
	ldr	r3, [r3, #0]
	mov	r2, #8
	str	r3, [sp, #4]
	ldr	r3, .L133+12
	str	r3, [sp, #8]
	ldr	r3, .L133+16
	str	r3, [sp, #12]
	ldr	r3, .L133+20
	str	r3, [sp, #16]
	mov	r3, #10
	str	r3, [sp, #20]
	ldr	r3, .L133+24
	bl	FLASH_FILE_initialize_list_and_find_or_create_group_list_file
	add	sp, sp, #24
	ldmfd	sp!, {pc}
.L134:
	.align	2
.L133:
	.word	.LANCHOR4
	.word	list_system_recursive_MUTEX
	.word	.LANCHOR3
	.word	nm_irrigation_system_structure_updater
	.word	nm_SYSTEM_set_default_values
	.word	.LANCHOR5
	.word	.LANCHOR0
.LFE23:
	.size	init_file_irrigation_system, .-init_file_irrigation_system
	.section	.text.save_file_irrigation_system,"ax",%progbits
	.align	2
	.global	save_file_irrigation_system
	.type	save_file_irrigation_system, %function
save_file_irrigation_system:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #408
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI45:
	ldr	r1, .L136
	str	r3, [sp, #0]
	ldr	r3, .L136+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, #8
	str	r3, [sp, #4]
	mov	r3, #10
	str	r3, [sp, #8]
	ldr	r3, .L136+8
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L137:
	.align	2
.L136:
	.word	.LANCHOR3
	.word	list_system_recursive_MUTEX
	.word	.LANCHOR0
.LFE24:
	.size	save_file_irrigation_system, .-save_file_irrigation_system
	.section	.text.nm_SYSTEM_create_new_group,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_create_new_group
	.type	nm_SYSTEM_create_new_group, %function
nm_SYSTEM_create_new_group:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, lr}
.LCFI46:
	ldr	r0, .L143
	bl	nm_ListGetFirst
	b	.L142
.L141:
	ldr	r3, [r1, #356]
	cmp	r3, #0
	beq	.L140
	ldr	r0, .L143
	bl	nm_ListGetNext
.L142:
	cmp	r0, #0
	mov	r1, r0
	bne	.L141
.L140:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r1, [sp, #4]
	ldr	r2, .L143+4
	ldr	r1, .L143+8
	mov	r3, #408
	ldr	r0, .L143
	bl	nm_GROUP_create_new_group
	ldmfd	sp!, {r2, r3, pc}
.L144:
	.align	2
.L143:
	.word	.LANCHOR0
	.word	nm_SYSTEM_set_default_values
	.word	.LANCHOR5
.LFE26:
	.size	nm_SYSTEM_create_new_group, .-nm_SYSTEM_create_new_group
	.section	.text.SYSTEM_get_inc_by_for_MLB_and_capacity,"ax",%progbits
	.align	2
	.global	SYSTEM_get_inc_by_for_MLB_and_capacity
	.type	SYSTEM_get_inc_by_for_MLB_and_capacity, %function
SYSTEM_get_inc_by_for_MLB_and_capacity:
.LFB49:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #64
	bhi	.L147
	cmp	r0, #32
	movls	r0, #1
	movhi	r0, #5
	bx	lr
.L147:
	mov	r0, #10
	bx	lr
.LFE49:
	.size	SYSTEM_get_inc_by_for_MLB_and_capacity, .-SYSTEM_get_inc_by_for_MLB_and_capacity
	.section	.text.SYSTEM_get_group_with_this_GID,"ax",%progbits
	.align	2
	.global	SYSTEM_get_group_with_this_GID
	.type	SYSTEM_get_group_with_this_GID, %function
SYSTEM_get_group_with_this_GID:
.LFB50:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI47:
	ldr	r4, .L150
	ldr	r3, .L150+4
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L150+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L150+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L151:
	.align	2
.L150:
	.word	list_program_data_recursive_MUTEX
	.word	2252
	.word	.LC22
	.word	.LANCHOR0
.LFE50:
	.size	SYSTEM_get_group_with_this_GID, .-SYSTEM_get_group_with_this_GID
	.section	.text.SYSTEM_get_group_at_this_index,"ax",%progbits
	.align	2
	.global	SYSTEM_get_group_at_this_index
	.type	SYSTEM_get_group_at_this_index, %function
SYSTEM_get_group_at_this_index:
.LFB51:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI48:
	ldr	r4, .L153
	ldr	r2, .L153+4
	mov	r3, #2288
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L153+8
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L154:
	.align	2
.L153:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	.LANCHOR0
.LFE51:
	.size	SYSTEM_get_group_at_this_index, .-SYSTEM_get_group_at_this_index
	.section	.text.FDTO_SYSTEM_load_system_name_into_guivar,"ax",%progbits
	.align	2
	.global	FDTO_SYSTEM_load_system_name_into_guivar
	.type	FDTO_SYSTEM_load_system_name_into_guivar, %function
FDTO_SYSTEM_load_system_name_into_guivar:
.LFB48:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI49:
	ldr	r4, .L157
	mov	r3, #2208
	mov	r1, #400
	ldr	r2, .L157+4
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_name
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r5, #0
	ldreq	r3, .L157+8
	streqb	r5, [r3, #0]
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r0, .L157+8
	mov	r1, r5
	mov	r2, #65
	ldmfd	sp!, {r4, r5, lr}
	b	strlcpy
.L158:
	.align	2
.L157:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	GuiVar_GroupName
.LFE48:
	.size	FDTO_SYSTEM_load_system_name_into_guivar, .-FDTO_SYSTEM_load_system_name_into_guivar
	.section	.text.BUDGET_FLOW_TYPES_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	BUDGET_FLOW_TYPES_copy_group_into_guivars
	.type	BUDGET_FLOW_TYPES_copy_group_into_guivars, %function
BUDGET_FLOW_TYPES_copy_group_into_guivars:
.LFB36:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI50:
	ldr	r4, .L160
	mov	r1, #400
	ldr	r2, .L160+4
	ldr	r3, .L160+8
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	SYSTEM_get_group_at_this_index
	mov	r5, r0
	bl	nm_GROUP_load_common_guivars
	ldr	r3, .L160+12
	mov	r2, #2
	str	r2, [r3, #0]
	ldr	r2, [r5, #400]
	ldr	r3, .L160+16
	adds	r2, r2, #0
	movne	r2, #1
	str	r2, [r3, #0]
	ldr	r2, [r5, #404]
	ldr	r3, .L160+20
	ldr	r0, [r4, #0]
	adds	r2, r2, #0
	movne	r2, #1
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L161:
	.align	2
.L160:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	1706
	.word	GuiVar_BudgetFlowTypeIrrigation
	.word	GuiVar_BudgetFlowTypeMVOR
	.word	GuiVar_BudgetFlowTypeNonController
.LFE36:
	.size	BUDGET_FLOW_TYPES_copy_group_into_guivars, .-BUDGET_FLOW_TYPES_copy_group_into_guivars
	.section	.text.BUDGET_SETUP_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	BUDGET_SETUP_copy_group_into_guivars
	.type	BUDGET_SETUP_copy_group_into_guivars, %function
BUDGET_SETUP_copy_group_into_guivars:
.LFB35:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L169
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI51:
	ldr	r2, .L169+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L169+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	ldr	r5, .L169+12
.LBB34:
.LBB35:
	ldr	r8, .L169+16
	ldr	r7, .L169+20
.LBB36:
	ldr	r6, .L169+24
.LBE36:
.LBE35:
.LBE34:
	mov	r4, r0
	bl	nm_GROUP_load_common_guivars
	ldr	r3, [r4, #352]
	mov	r0, r4
	str	r3, [r5, #0]
	bl	nm_GROUP_get_group_ID
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldr	r2, .L169+28
	ldr	r3, [r0, #468]
	ands	r3, r3, #122880
	movne	r3, #1
	str	r3, [r2, #0]
	ldr	r2, [r4, #252]
	cmp	r3, #0
	streq	r3, [r5, #0]
	ldr	r3, .L169+32
	subs	r2, r2, #6
	movne	r2, #1
	mov	r5, #0
	str	r2, [r3, #0]
.L165:
.LBB39:
.LBB38:
	mov	r3, #472
	mla	r3, r5, r3, r8
	ldr	r0, [r3, #24]
	add	r3, r3, #20
	cmp	r0, #0
	beq	.L164
	ldr	r3, [r3, #24]
	ldr	r2, [r7, #0]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L164
.LBB37:
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	mov	sl, r0
	bl	POC_get_budget_gallons_entry_option
	str	r0, [r6, #0]
	mov	r0, sl
	bl	POC_get_budget_percent_ET
	ldr	r3, .L169+36
	str	r0, [r3, #0]
.L164:
.LBE37:
.LBE38:
	add	r5, r5, #1
	cmp	r5, #12
	bne	.L165
.LBE39:
	ldr	r2, [r4, #244]
	ldr	r3, .L169+40
	str	r2, [r3, #0]
	ldr	r2, .L169+44
	mov	r3, #0
.L166:
	ldr	r1, [r4, #256]
	add	r4, r4, #4
	str	r1, [r3, r2]
	add	r3, r3, #4
	cmp	r3, #96
	bne	.L166
	ldr	r3, .L169
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L170:
	.align	2
.L169:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	1629
	.word	GuiVar_BudgetInUse
	.word	poc_preserves
	.word	g_GROUP_ID
	.word	GuiVar_BudgetEntryOptionIdx
	.word	GuiVar_BudgetSysHasPOC
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	GuiVar_BudgetETPercentageUsed
	.word	GuiVar_BudgetModeIdx
	.word	g_BUDGET_SETUP_meter_read_date
.LFE35:
	.size	BUDGET_SETUP_copy_group_into_guivars, .-BUDGET_SETUP_copy_group_into_guivars
	.global	__udivsi3
	.section	.text.MVOR_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	MVOR_copy_group_into_guivars
	.type	MVOR_copy_group_into_guivars, %function
MVOR_copy_group_into_guivars:
.LFB34:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L174+4
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI52:
	ldr	r2, .L174+8
	mov	r1, #400
	ldr	r3, .L174+12
	mov	r4, r0
	ldr	r0, [ip, #0]
	str	ip, [sp, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	ldr	r4, .L174+16
	mov	r5, r0
	bl	nm_GROUP_load_common_guivars
	ldr	r2, .L174+20
	ldr	fp, [r5, #188]
	ldr	r3, .L174+24
	mov	r0, fp
	str	r2, [r3, #0]	@ float
	mov	r1, #60
	bl	__udivsi3
	ldr	r9, [r5, #192]
	ldr	r3, .L174+28
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r0, r9
	bl	__udivsi3
	ldr	sl, [r5, #196]
	ldr	r3, .L174+32
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r0, sl
	bl	__udivsi3
	ldr	r8, [r5, #200]
	ldr	r3, .L174+36
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r0, r8
	bl	__udivsi3
	ldr	r7, [r5, #204]
	ldr	r3, .L174+40
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r0, r7
	bl	__udivsi3
	ldr	r6, [r5, #208]
	ldr	r3, .L174+44
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r0, r6
	bl	__udivsi3
	ldr	r3, .L174+48
	mov	r1, #60
	str	r0, [r3, #0]
	ldr	r3, [r5, #212]
	mov	r0, r3
	str	r3, [sp, #4]
	bl	__udivsi3
	ldr	r2, .L174+52
	subs	fp, fp, r4
	movne	fp, #1
	subs	r9, r9, r4
	movne	r9, #1
	subs	sl, sl, r4
	movne	sl, #1
	subs	r8, r8, r4
	movne	r8, #1
	subs	r7, r7, r4
	movne	r7, #1
	ldr	r3, [sp, #4]
	subs	r6, r6, r4
	movne	r6, #1
	subs	r3, r3, r4
	movne	r3, #1
	mov	r1, #60
	str	r0, [r2, #0]
	ldr	r2, .L174+56
	str	fp, [r2, #0]
	ldr	r2, .L174+60
	ldr	fp, [r5, #216]
	str	r9, [r2, #0]
	ldr	r2, .L174+64
	mov	r0, fp
	str	sl, [r2, #0]
	ldr	r2, .L174+68
	str	r8, [r2, #0]
	ldr	r2, .L174+72
	str	r7, [r2, #0]
	ldr	r2, .L174+76
	str	r6, [r2, #0]
	ldr	r2, .L174+80
	str	r3, [r2, #0]
	bl	__udivsi3
	ldr	r9, [r5, #220]
	ldr	r3, .L174+84
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r0, r9
	bl	__udivsi3
	ldr	sl, [r5, #224]
	ldr	r3, .L174+88
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r0, sl
	bl	__udivsi3
	ldr	r8, [r5, #228]
	ldr	r3, .L174+92
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r0, r8
	bl	__udivsi3
	ldr	r7, [r5, #232]
	ldr	r3, .L174+96
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r0, r7
	bl	__udivsi3
	ldr	r6, [r5, #236]
	ldr	r3, .L174+100
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r0, r6
	bl	__udivsi3
	ldr	r3, .L174+104
	ldr	r5, [r5, #240]
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r0, r5
	bl	__udivsi3
	ldr	r3, .L174+108
	subs	fp, fp, r4
	movne	fp, #1
	subs	r9, r9, r4
	movne	r9, #1
	subs	sl, sl, r4
	movne	sl, #1
	subs	r8, r8, r4
	movne	r8, #1
	subs	r7, r7, r4
	movne	r7, #1
	subs	r6, r6, r4
	movne	r6, #1
	subs	r4, r5, r4
	movne	r4, #1
	str	r0, [r3, #0]
	ldr	r3, .L174+112
	str	fp, [r3, #0]
	ldr	r3, .L174+116
	str	r9, [r3, #0]
	ldr	r3, .L174+120
	str	sl, [r3, #0]
	ldr	r3, .L174+124
	str	r8, [r3, #0]
	ldr	r3, .L174+128
	ldr	ip, [sp, #0]
	str	r7, [r3, #0]
	ldr	r3, .L174+132
	ldr	r0, [ip, #0]
	str	r6, [r3, #0]
	ldr	r3, .L174+136
	str	r4, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L174+140
	ldr	r2, .L174+8
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L174+144
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L174+148
	ldr	r0, [r3, #0]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldr	r2, .L174+152
	ldr	r3, .L174+156
	cmp	r0, #0
	beq	.L172
	ldrb	r1, [r0, #468]	@ zero_extendqisi2
	tst	r1, #96
	moveq	r1, #0
	movne	r1, #1
	str	r1, [r2, #0]
	ldrb	r2, [r0, #468]	@ zero_extendqisi2
	mov	r2, r2, lsr #6
	and	r2, r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L174+160
	ldr	r3, [r0, r3]
	fmsr	s15, r3	@ int
	ldr	r3, .L174+164
	fuitos	s14, s15
	flds	s15, .L174
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	b	.L173
.L172:
	str	r0, [r3, #0]
	ldr	r3, .L174+164
	str	r0, [r2, #0]
	mov	r2, #0
	str	r2, [r3, #0]	@ float
.L173:
	ldr	r3, .L174+140
	ldr	r0, [r3, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	xQueueGiveMutexRecursive
.L175:
	.align	2
.L174:
	.word	1163984896
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	1546
	.word	86400
	.word	1082130432
	.word	GuiVar_MVORRunTime
	.word	GuiVar_MVOROpen0
	.word	GuiVar_MVOROpen1
	.word	GuiVar_MVOROpen2
	.word	GuiVar_MVOROpen3
	.word	GuiVar_MVOROpen4
	.word	GuiVar_MVOROpen5
	.word	GuiVar_MVOROpen6
	.word	GuiVar_MVOROpen0Enabled
	.word	GuiVar_MVOROpen1Enabled
	.word	GuiVar_MVOROpen2Enabled
	.word	GuiVar_MVOROpen3Enabled
	.word	GuiVar_MVOROpen4Enabled
	.word	GuiVar_MVOROpen5Enabled
	.word	GuiVar_MVOROpen6Enabled
	.word	GuiVar_MVORClose0
	.word	GuiVar_MVORClose1
	.word	GuiVar_MVORClose2
	.word	GuiVar_MVORClose3
	.word	GuiVar_MVORClose4
	.word	GuiVar_MVORClose5
	.word	GuiVar_MVORClose6
	.word	GuiVar_MVORClose0Enabled
	.word	GuiVar_MVORClose1Enabled
	.word	GuiVar_MVORClose2Enabled
	.word	GuiVar_MVORClose3Enabled
	.word	GuiVar_MVORClose4Enabled
	.word	GuiVar_MVORClose5Enabled
	.word	GuiVar_MVORClose6Enabled
	.word	system_preserves_recursive_MUTEX
	.word	1594
	.word	g_GROUP_ID
	.word	GuiVar_MVORInEffect
	.word	GuiVar_MVORState
	.word	14124
	.word	GuiVar_MVORTimeRemaining
.LFE34:
	.size	MVOR_copy_group_into_guivars, .-MVOR_copy_group_into_guivars
	.section	.text.FLOW_CHECKING_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	FLOW_CHECKING_copy_group_into_guivars
	.type	FLOW_CHECKING_copy_group_into_guivars, %function
FLOW_CHECKING_copy_group_into_guivars:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI53:
	ldr	r4, .L177
	mov	r1, #400
	ldr	r2, .L177+4
	ldr	r3, .L177+8
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	SYSTEM_get_group_at_this_index
	mov	r5, r0
	bl	nm_GROUP_load_common_guivars
	ldr	r2, [r5, #100]
	ldr	r3, .L177+12
	ldr	r0, [r4, #0]
	str	r2, [r3, #0]
	ldr	r2, [r5, #104]
	ldr	r3, .L177+16
	str	r2, [r3, #0]
	ldr	r2, [r5, #108]
	ldr	r3, .L177+20
	str	r2, [r3, #0]
	ldr	r2, [r5, #112]
	ldr	r3, .L177+24
	str	r2, [r3, #0]
	ldr	r2, [r5, #116]
	ldr	r3, .L177+28
	str	r2, [r3, #0]
	ldr	r2, [r5, #120]
	ldr	r3, .L177+32
	str	r2, [r3, #0]
	ldr	r2, [r5, #124]
	ldr	r3, .L177+36
	str	r2, [r3, #0]
	ldr	r2, [r5, #128]
	ldr	r3, .L177+40
	str	r2, [r3, #0]
	ldr	r2, [r5, #132]
	ldr	r3, .L177+44
	rsb	r2, r2, #0
	str	r2, [r3, #0]
	ldr	r2, [r5, #136]
	ldr	r3, .L177+48
	rsb	r2, r2, #0
	str	r2, [r3, #0]
	ldr	r2, [r5, #140]
	ldr	r3, .L177+52
	rsb	r2, r2, #0
	str	r2, [r3, #0]
	ldr	r2, [r5, #144]
	ldr	r3, .L177+56
	rsb	r2, r2, #0
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L178:
	.align	2
.L177:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	1515
	.word	GuiVar_SystemFlowCheckingInUse
	.word	GuiVar_SystemFlowCheckingRange1
	.word	GuiVar_SystemFlowCheckingRange2
	.word	GuiVar_SystemFlowCheckingRange3
	.word	GuiVar_SystemFlowCheckingTolerancePlus1
	.word	GuiVar_SystemFlowCheckingTolerancePlus2
	.word	GuiVar_SystemFlowCheckingTolerancePlus3
	.word	GuiVar_SystemFlowCheckingTolerancePlus4
	.word	GuiVar_SystemFlowCheckingToleranceMinus1
	.word	GuiVar_SystemFlowCheckingToleranceMinus2
	.word	GuiVar_SystemFlowCheckingToleranceMinus3
	.word	GuiVar_SystemFlowCheckingToleranceMinus4
.LFE33:
	.size	FLOW_CHECKING_copy_group_into_guivars, .-FLOW_CHECKING_copy_group_into_guivars
	.section	.text.SYSTEM_CAPACITY_fill_guivars,"ax",%progbits
	.align	2
	.type	SYSTEM_CAPACITY_fill_guivars, %function
SYSTEM_CAPACITY_fill_guivars:
.LFB31:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI54:
	ldr	sl, [sp, #28]
	mov	r8, r3
	ldr	r3, .L184
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	ldr	r3, .L184+4
	mov	r1, #400
	ldr	r2, .L184+8
	ldr	r4, [sp, #32]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L184+12
	ldr	r3, [r3, #8]
	cmp	r3, r5
	movls	r3, #0
	bls	.L183
	mov	r0, r5
	bl	SYSTEM_get_group_at_this_index
	ldr	r3, [r0, #356]
	mov	r5, r0
	cmp	r3, #0
	beq	.L183
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r5, #76]
	str	r3, [r6, #0]
	ldr	r3, [r5, #80]
	str	r3, [r8, #0]
	ldr	r3, [r5, #84]
	str	r3, [sl, #0]
	mov	r3, #1
.L183:
	str	r3, [r4, #0]
	ldr	r3, .L184
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L185:
	.align	2
.L184:
	.word	list_system_recursive_MUTEX
	.word	1460
	.word	.LC22
	.word	.LANCHOR0
.LFE31:
	.size	SYSTEM_CAPACITY_fill_guivars, .-SYSTEM_CAPACITY_fill_guivars
	.section	.text.SYSTEM_CAPACITY_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	SYSTEM_CAPACITY_copy_group_into_guivars
	.type	SYSTEM_CAPACITY_copy_group_into_guivars, %function
SYSTEM_CAPACITY_copy_group_into_guivars:
.LFB32:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI55:
	ldr	r4, .L187
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L187+4
	ldr	r3, .L187+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L187+12
	mov	r0, #0
	str	r3, [sp, #0]
	ldr	r3, .L187+16
	ldr	r1, .L187+20
	str	r3, [sp, #4]
	ldr	r2, .L187+24
	ldr	r3, .L187+28
	bl	SYSTEM_CAPACITY_fill_guivars
	ldr	r3, .L187+32
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, .L187+36
	ldr	r1, .L187+40
	str	r3, [sp, #4]
	ldr	r2, .L187+44
	ldr	r3, .L187+48
	bl	SYSTEM_CAPACITY_fill_guivars
	ldr	r3, .L187+52
	mov	r0, #2
	str	r3, [sp, #0]
	ldr	r3, .L187+56
	ldr	r1, .L187+60
	str	r3, [sp, #4]
	ldr	r2, .L187+64
	ldr	r3, .L187+68
	bl	SYSTEM_CAPACITY_fill_guivars
	ldr	r3, .L187+72
	mov	r0, #3
	str	r3, [sp, #0]
	ldr	r3, .L187+76
	ldr	r1, .L187+80
	str	r3, [sp, #4]
	ldr	r2, .L187+84
	ldr	r3, .L187+88
	bl	SYSTEM_CAPACITY_fill_guivars
	ldr	r0, [r4, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L188:
	.align	2
.L187:
	.word	list_program_data_recursive_MUTEX
	.word	.LC22
	.word	1500
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupName_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupName_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupName_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingC_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupName_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_3
.LFE32:
	.size	SYSTEM_CAPACITY_copy_group_into_guivars, .-SYSTEM_CAPACITY_copy_group_into_guivars
	.section	.text.MAINLINE_BREAK_fill_guivars,"ax",%progbits
	.align	2
	.type	MAINLINE_BREAK_fill_guivars, %function
MAINLINE_BREAK_fill_guivars:
.LFB29:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI56:
	ldr	sl, [sp, #28]
	mov	r8, r3
	ldr	r3, .L194
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	ldr	r3, .L194+4
	mov	r1, #400
	ldr	r2, .L194+8
	ldr	r4, [sp, #32]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L194+12
	ldr	r3, [r3, #8]
	cmp	r3, r5
	movls	r3, #0
	bls	.L193
	mov	r0, r5
	bl	SYSTEM_get_group_at_this_index
	ldr	r3, [r0, #356]
	mov	r5, r0
	cmp	r3, #0
	beq	.L193
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r5, #88]
	str	r3, [r6, #0]
	ldr	r3, [r5, #92]
	str	r3, [r8, #0]
	ldr	r3, [r5, #96]
	str	r3, [sl, #0]
	mov	r3, #1
.L193:
	str	r3, [r4, #0]
	ldr	r3, .L194
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L195:
	.align	2
.L194:
	.word	list_system_recursive_MUTEX
	.word	1409
	.word	.LC22
	.word	.LANCHOR0
.LFE29:
	.size	MAINLINE_BREAK_fill_guivars, .-MAINLINE_BREAK_fill_guivars
	.section	.text.MAINLINE_BREAK_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	MAINLINE_BREAK_copy_group_into_guivars
	.type	MAINLINE_BREAK_copy_group_into_guivars, %function
MAINLINE_BREAK_copy_group_into_guivars:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L197
	stmfd	sp!, {r0, r1, lr}
.LCFI57:
	ldr	r2, .L197+4
	str	r3, [sp, #0]
	ldr	r3, .L197+8
	mov	r0, #0
	str	r3, [sp, #4]
	ldr	r1, .L197+12
	ldr	r3, .L197+16
	bl	MAINLINE_BREAK_fill_guivars
	ldr	r3, .L197+20
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, .L197+24
	ldr	r1, .L197+28
	str	r3, [sp, #4]
	ldr	r2, .L197+32
	ldr	r3, .L197+36
	bl	MAINLINE_BREAK_fill_guivars
	ldr	r3, .L197+40
	mov	r0, #2
	str	r3, [sp, #0]
	ldr	r3, .L197+44
	ldr	r1, .L197+48
	str	r3, [sp, #4]
	ldr	r2, .L197+52
	ldr	r3, .L197+56
	bl	MAINLINE_BREAK_fill_guivars
	ldr	r3, .L197+60
	mov	r0, #3
	str	r3, [sp, #0]
	ldr	r3, .L197+64
	ldr	r1, .L197+68
	str	r3, [sp, #4]
	ldr	r2, .L197+72
	ldr	r3, .L197+76
	bl	MAINLINE_BREAK_fill_guivars
	ldmfd	sp!, {r2, r3, pc}
.L198:
	.align	2
.L197:
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupName_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupName_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupName_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingC_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupName_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_3
.LFE30:
	.size	MAINLINE_BREAK_copy_group_into_guivars, .-MAINLINE_BREAK_copy_group_into_guivars
	.section	.text.SYSTEM_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	SYSTEM_copy_group_into_guivars
	.type	SYSTEM_copy_group_into_guivars, %function
SYSTEM_copy_group_into_guivars:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI58:
	ldr	r4, .L200
	mov	r1, #400
	ldr	r2, .L200+4
	ldr	r3, .L200+8
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	SYSTEM_get_group_at_this_index
	mov	r5, r0
	bl	nm_GROUP_load_common_guivars
	ldr	r2, [r5, #356]
	ldr	r3, .L200+12
	ldr	r0, [r4, #0]
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L201:
	.align	2
.L200:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	1393
	.word	GuiVar_SystemInUse
.LFE28:
	.size	SYSTEM_copy_group_into_guivars, .-SYSTEM_copy_group_into_guivars
	.section	.text.SYSTEM_get_index_for_group_with_this_GID,"ax",%progbits
	.align	2
	.global	SYSTEM_get_index_for_group_with_this_GID
	.type	SYSTEM_get_index_for_group_with_this_GID, %function
SYSTEM_get_index_for_group_with_this_GID:
.LFB52:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI59:
	ldr	r4, .L203
	ldr	r2, .L203+4
	ldr	r3, .L203+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L203+12
	bl	nm_GROUP_get_index_for_group_with_this_GID
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L204:
	.align	2
.L203:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	2324
	.word	.LANCHOR0
.LFE52:
	.size	SYSTEM_get_index_for_group_with_this_GID, .-SYSTEM_get_index_for_group_with_this_GID
	.section	.text.SYSTEM_get_last_group_ID,"ax",%progbits
	.align	2
	.global	SYSTEM_get_last_group_ID
	.type	SYSTEM_get_last_group_ID, %function
SYSTEM_get_last_group_ID:
.LFB53:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI60:
	ldr	r4, .L206
	mov	r1, #400
	ldr	r2, .L206+4
	ldr	r0, [r4, #0]
	ldr	r3, .L206+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L206+12
	ldr	r0, [r4, #0]
	ldr	r3, [r3, #0]
	ldr	r5, [r3, #12]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L207:
	.align	2
.L206:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	2359
	.word	.LANCHOR0
.LFE53:
	.size	SYSTEM_get_last_group_ID, .-SYSTEM_get_last_group_ID
	.section	.text.SYSTEM_get_list_count,"ax",%progbits
	.align	2
	.global	SYSTEM_get_list_count
	.type	SYSTEM_get_list_count, %function
SYSTEM_get_list_count:
.LFB54:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI61:
	ldr	r4, .L209
	mov	r1, #400
	ldr	r2, .L209+4
	ldr	r0, [r4, #0]
	ldr	r3, .L209+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L209+12
	ldr	r0, [r4, #0]
	ldr	r5, [r3, #8]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L210:
	.align	2
.L209:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	2394
	.word	.LANCHOR0
.LFE54:
	.size	SYSTEM_get_list_count, .-SYSTEM_get_list_count
	.section	.text.SYSTEM_num_systems_in_use,"ax",%progbits
	.align	2
	.global	SYSTEM_num_systems_in_use
	.type	SYSTEM_num_systems_in_use, %function
SYSTEM_num_systems_in_use:
.LFB55:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L217
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI62:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L217+4
	ldr	r3, .L217+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L217+12
	bl	nm_ListGetFirst
	mov	r5, #0
	mov	r4, r5
	mov	r6, r0
	b	.L212
.L214:
	ldr	r3, [r6, #356]
	cmp	r3, #0
	beq	.L216
	cmp	r5, #0
	add	r4, r4, #1
	beq	.L213
	ldr	r0, .L217+16
	bl	Alert_Message
.L216:
	mov	r5, #1
.L213:
	mov	r1, r6
	ldr	r0, .L217+12
	bl	nm_ListGetNext
	mov	r6, r0
.L212:
	cmp	r6, #0
	bne	.L214
	ldr	r3, .L217
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r4, #4
	bls	.L215
	ldr	r0, .L217+20
	bl	Alert_Message
	mov	r4, #1
.L215:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L218:
	.align	2
.L217:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	2427
	.word	.LANCHOR0
	.word	.LC27
	.word	.LC28
.LFE55:
	.size	SYSTEM_num_systems_in_use, .-SYSTEM_num_systems_in_use
	.section	.text.SYSTEM_load_system_name_into_scroll_box_guivar,"ax",%progbits
	.align	2
	.global	SYSTEM_load_system_name_into_scroll_box_guivar
	.type	SYSTEM_load_system_name_into_scroll_box_guivar, %function
SYSTEM_load_system_name_into_scroll_box_guivar:
.LFB47:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L224
	stmfd	sp!, {r0, r4, lr}
.LCFI63:
	ldr	r2, .L224+4
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L224+8
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	cmp	r4, r0
	bcs	.L220
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	mov	r4, r0
	mov	r1, r4
	ldr	r0, .L224+12
	bl	nm_OnList
	cmp	r0, #1
	bne	.L221
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L224+16
	bl	strlcpy
	b	.L222
.L221:
	ldr	r0, .L224+4
	ldr	r1, .L224+20
	bl	Alert_group_not_found_with_filename
	b	.L222
.L220:
	bl	SYSTEM_num_systems_in_use
	cmp	r4, r0
	bne	.L223
	mov	r1, #0
	ldr	r0, .L224+24
	bl	GuiLib_GetTextPtr
	mov	r1, #48
	ldr	r2, .L224+28
	mov	r3, r0
	ldr	r0, .L224+16
	bl	snprintf
	b	.L222
.L223:
	mov	r1, #0
	ldr	r0, .L224+32
	bl	GuiLib_GetTextPtr
	mov	r1, #0
	mov	r4, r0
	ldr	r0, .L224+36
	bl	GuiLib_GetTextPtr
	mov	r1, #48
	ldr	r2, .L224+40
	mov	r3, r4
	str	r0, [sp, #0]
	ldr	r0, .L224+16
	bl	snprintf
.L222:
	ldr	r3, .L224
	ldr	r0, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L225:
	.align	2
.L224:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	2156
	.word	.LANCHOR0
	.word	GuiVar_itmGroupName
	.word	2168
	.word	943
	.word	.LC29
	.word	874
	.word	977
	.word	.LC30
.LFE47:
	.size	SYSTEM_load_system_name_into_scroll_box_guivar, .-SYSTEM_load_system_name_into_scroll_box_guivar
	.section	.text.SYSTEM_get_used_for_irri_bool,"ax",%progbits
	.align	2
	.global	SYSTEM_get_used_for_irri_bool
	.type	SYSTEM_get_used_for_irri_bool, %function
SYSTEM_get_used_for_irri_bool:
.LFB56:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI64:
	ldr	r4, .L227
	mov	r5, r0
	ldr	r3, .L227+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L227+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L227+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r2, #1
	add	r3, r0, #148
	str	r3, [sp, #0]
	ldr	r3, .L227+16
	add	r1, r0, #72
	str	r3, [sp, #4]
	ldr	r3, .L227+20
	bl	SHARED_get_bool_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L228:
	.align	2
.L227:
	.word	list_system_recursive_MUTEX
	.word	2500
	.word	.LC22
	.word	.LANCHOR0
	.word	.LC2
	.word	nm_SYSTEM_set_system_used_for_irri
.LFE56:
	.size	SYSTEM_get_used_for_irri_bool, .-SYSTEM_get_used_for_irri_bool
	.section	.text.SYSTEM_get_mlb_during_irri_gpm,"ax",%progbits
	.align	2
	.global	SYSTEM_get_mlb_during_irri_gpm
	.type	SYSTEM_get_mlb_during_irri_gpm, %function
SYSTEM_get_mlb_during_irri_gpm:
.LFB57:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI65:
	ldr	r4, .L230
	mov	r5, r0
	ldr	r3, .L230+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L230+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L230+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #400
	str	r3, [sp, #0]
	ldr	r3, .L230+16
	mov	r2, #1
	str	r3, [sp, #4]
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L230+20
	add	r1, r0, #88
	str	r3, [sp, #12]
	mov	r3, #4000
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L231:
	.align	2
.L230:
	.word	list_system_recursive_MUTEX
	.word	2541
	.word	.LC22
	.word	.LANCHOR0
	.word	nm_SYSTEM_set_mlb_during_irri
	.word	.LC9
.LFE57:
	.size	SYSTEM_get_mlb_during_irri_gpm, .-SYSTEM_get_mlb_during_irri_gpm
	.section	.text.SYSTEM_get_mlb_during_mvor_opened_gpm,"ax",%progbits
	.align	2
	.global	SYSTEM_get_mlb_during_mvor_opened_gpm
	.type	SYSTEM_get_mlb_during_mvor_opened_gpm, %function
SYSTEM_get_mlb_during_mvor_opened_gpm:
.LFB58:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI66:
	ldr	r4, .L233
	mov	r5, r0
	ldr	r3, .L233+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L233+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L233+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #150
	str	r3, [sp, #0]
	ldr	r3, .L233+16
	mov	r2, #1
	str	r3, [sp, #4]
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L233+20
	add	r1, r0, #92
	str	r3, [sp, #12]
	mov	r3, #4000
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L234:
	.align	2
.L233:
	.word	list_system_recursive_MUTEX
	.word	2585
	.word	.LC22
	.word	.LANCHOR0
	.word	nm_SYSTEM_set_mlb_during_mvor
	.word	.LC10
.LFE58:
	.size	SYSTEM_get_mlb_during_mvor_opened_gpm, .-SYSTEM_get_mlb_during_mvor_opened_gpm
	.section	.text.SYSTEM_get_mlb_during_all_other_times_gpm,"ax",%progbits
	.align	2
	.global	SYSTEM_get_mlb_during_all_other_times_gpm
	.type	SYSTEM_get_mlb_during_all_other_times_gpm, %function
SYSTEM_get_mlb_during_all_other_times_gpm:
.LFB59:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI67:
	ldr	r4, .L236
	mov	r5, r0
	ldr	r3, .L236+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L236+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L236+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #150
	str	r3, [sp, #0]
	ldr	r3, .L236+16
	mov	r2, #1
	str	r3, [sp, #4]
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L236+20
	add	r1, r0, #96
	str	r3, [sp, #12]
	mov	r3, #4000
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L237:
	.align	2
.L236:
	.word	list_system_recursive_MUTEX
	.word	2629
	.word	.LC22
	.word	.LANCHOR0
	.word	nm_SYSTEM_set_mlb_all_other_times
	.word	.LC11
.LFE59:
	.size	SYSTEM_get_mlb_during_all_other_times_gpm, .-SYSTEM_get_mlb_during_all_other_times_gpm
	.section	.text.SYSTEM_get_capacity_in_use_bool,"ax",%progbits
	.align	2
	.global	SYSTEM_get_capacity_in_use_bool
	.type	SYSTEM_get_capacity_in_use_bool, %function
SYSTEM_get_capacity_in_use_bool:
.LFB60:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI68:
	ldr	r4, .L239
	mov	r5, r0
	ldr	r3, .L239+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L239+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L239+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r2, #0
	add	r3, r0, #148
	str	r3, [sp, #0]
	ldr	r3, .L239+16
	add	r1, r0, #76
	str	r3, [sp, #4]
	ldr	r3, .L239+20
	bl	SHARED_get_bool_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L240:
	.align	2
.L239:
	.word	list_system_recursive_MUTEX
	.word	2673
	.word	.LC22
	.word	.LANCHOR0
	.word	.LC3
	.word	nm_SYSTEM_set_capacity_in_use
.LFE60:
	.size	SYSTEM_get_capacity_in_use_bool, .-SYSTEM_get_capacity_in_use_bool
	.section	.text.SYSTEM_get_capacity_with_pump_gpm,"ax",%progbits
	.align	2
	.global	SYSTEM_get_capacity_with_pump_gpm
	.type	SYSTEM_get_capacity_with_pump_gpm, %function
SYSTEM_get_capacity_with_pump_gpm:
.LFB61:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI69:
	ldr	r4, .L242
	mov	r5, r0
	ldr	r3, .L242+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L242+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L242+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #200
	str	r3, [sp, #0]
	ldr	r3, .L242+16
	mov	r2, #1
	str	r3, [sp, #4]
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L242+20
	add	r1, r0, #80
	str	r3, [sp, #12]
	mov	r3, #4000
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L243:
	.align	2
.L242:
	.word	list_system_recursive_MUTEX
	.word	2714
	.word	.LC22
	.word	.LANCHOR0
	.word	nm_SYSTEM_set_capacity_with_pump
	.word	.LC7
.LFE61:
	.size	SYSTEM_get_capacity_with_pump_gpm, .-SYSTEM_get_capacity_with_pump_gpm
	.section	.text.SYSTEM_get_capacity_without_pump_gpm,"ax",%progbits
	.align	2
	.global	SYSTEM_get_capacity_without_pump_gpm
	.type	SYSTEM_get_capacity_without_pump_gpm, %function
SYSTEM_get_capacity_without_pump_gpm:
.LFB62:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI70:
	ldr	r4, .L245
	mov	r5, r0
	ldr	r3, .L245+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L245+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L245+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #200
	str	r3, [sp, #0]
	ldr	r3, .L245+16
	mov	r2, #1
	str	r3, [sp, #4]
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L245+20
	add	r1, r0, #84
	str	r3, [sp, #12]
	mov	r3, #4000
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L246:
	.align	2
.L245:
	.word	list_system_recursive_MUTEX
	.word	2757
	.word	.LC22
	.word	.LANCHOR0
	.word	nm_SYSTEM_set_capacity_without_pump
	.word	.LC8
.LFE62:
	.size	SYSTEM_get_capacity_without_pump_gpm, .-SYSTEM_get_capacity_without_pump_gpm
	.section	.text.SYSTEM_get_flow_checking_in_use,"ax",%progbits
	.align	2
	.global	SYSTEM_get_flow_checking_in_use
	.type	SYSTEM_get_flow_checking_in_use, %function
SYSTEM_get_flow_checking_in_use:
.LFB63:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI71:
	ldr	r4, .L248
	mov	r5, r0
	ldr	r3, .L248+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L248+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L248+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r2, #0
	add	r3, r0, #148
	str	r3, [sp, #0]
	ldr	r3, .L248+16
	add	r1, r0, #100
	str	r3, [sp, #4]
	ldr	r3, .L248+20
	bl	SHARED_get_bool_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L249:
	.align	2
.L248:
	.word	list_system_recursive_MUTEX
	.word	2804
	.word	.LC22
	.word	.LANCHOR0
	.word	.LC12
	.word	nm_SYSTEM_set_flow_checking_in_use
.LFE63:
	.size	SYSTEM_get_flow_checking_in_use, .-SYSTEM_get_flow_checking_in_use
	.section	.text.SYSTEM_get_flow_check_ranges_gpm,"ax",%progbits
	.align	2
	.global	SYSTEM_get_flow_check_ranges_gpm
	.type	SYSTEM_get_flow_check_ranges_gpm, %function
SYSTEM_get_flow_check_ranges_gpm:
.LFB64:
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L257
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI72:
	ldr	r2, .L257+4
	mov	r4, r0
	sub	sp, sp, #68
.LCFI73:
	ldr	r0, [r3, #0]
	mov	r6, r1
	ldr	r3, .L257+8
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L257+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	beq	.L251
	ldr	r9, .L257+16
	mov	r8, r4
	mov	r5, #0
	add	sl, sp, #20
	b	.L254
.L256:
	mov	r5, r7
.L254:
	mov	r1, #48
	ldr	r2, .L257+20
	ldr	r3, .L257+24
	add	r7, r5, #1
	mov	r0, sl
	str	r7, [sp, #0]
	bl	snprintf
	add	r3, r5, #26
	add	r3, r4, r3, asl #2
	mov	r0, r4
	ldr	fp, [r9, #4]!
	str	r3, [sp, #16]
	bl	nm_GROUP_get_name
	ldr	r2, .L257+4
	ldr	r3, [sp, #16]
	str	r2, [sp, #8]
	ldr	r2, .L257+28
	mov	r1, #10
	str	r2, [sp, #12]
	mov	r2, #4000
	stmia	sp, {r0, sl}
	mov	r0, r3
	mov	r3, fp
	bl	RC_uint32_with_filename
	subs	r3, r0, #0
	bne	.L252
	ldr	fp, [r8, #104]
	str	r3, [sp, #16]
	bl	FLOWSENSE_get_controller_index
.LBB40:
	sub	r2, r5, #1
	cmp	r2, #2
	ldr	r3, [sp, #16]
	bhi	.L253
	mov	r2, #4
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #8]
.LBE40:
	add	r2, r4, #148
.LBB41:
	str	r0, [sp, #4]
	str	r2, [sp, #12]
	mov	r0, r4
	mov	r1, r5
	mov	r2, fp
	bl	nm_SYSTEM_set_flow_checking_range.part.4
	b	.L252
.L253:
	ldr	r0, .L257+32
	ldr	r1, .L257+36
	bl	Alert_index_out_of_range_with_filename
.L252:
.LBE41:
	cmp	r7, #3
.LBB42:
	add	r8, r8, #4
.LBE42:
	bne	.L256
	mov	r0, r6
	add	r1, r4, #104
	mov	r2, #12
	bl	memcpy
	b	.L255
.L251:
	ldr	r1, .L257+40
	ldr	r0, .L257+4
	bl	Alert_group_not_found_with_filename
	mov	r1, #30
	mov	r2, #65
	mov	r3, #100
	stmia	r6, {r1, r2, r3}
.L255:
	ldr	r3, .L257
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L258:
	.align	2
.L257:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	2831
	.word	.LANCHOR0
	.word	.LANCHOR1-4
	.word	.LC16
	.word	.LC24
	.word	2843
	.word	.LC15
	.word	967
	.word	2860
.LFE64:
	.size	SYSTEM_get_flow_check_ranges_gpm, .-SYSTEM_get_flow_check_ranges_gpm
	.section	.text.SYSTEM_get_flow_check_tolerances_plus_gpm,"ax",%progbits
	.align	2
	.global	SYSTEM_get_flow_check_tolerances_plus_gpm
	.type	SYSTEM_get_flow_check_tolerances_plus_gpm, %function
SYSTEM_get_flow_check_tolerances_plus_gpm:
.LFB65:
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L266
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI74:
	ldr	r2, .L266+4
	mov	r4, r0
	sub	sp, sp, #68
.LCFI75:
	ldr	r0, [r3, #0]
	mov	r5, r1
	ldr	r3, .L266+8
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L266+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	beq	.L260
	ldr	r9, .L266+16
	mov	r8, r4
	mov	r6, #0
	add	sl, sp, #20
	b	.L263
.L265:
	mov	r6, r7
.L263:
	mov	r1, #48
	ldr	r2, .L266+20
	ldr	r3, .L266+24
	add	r7, r6, #1
	mov	r0, sl
	str	r7, [sp, #0]
	bl	snprintf
	add	r3, r6, #29
	add	r3, r4, r3, asl #2
	mov	r0, r4
	ldr	fp, [r9, #4]!
	str	r3, [sp, #16]
	bl	nm_GROUP_get_name
	ldr	r2, .L266+4
	ldr	r3, [sp, #16]
	str	r2, [sp, #8]
	ldr	r2, .L266+28
	mov	r1, #1
	str	r2, [sp, #12]
	mov	r2, #200
	stmia	sp, {r0, sl}
	mov	r0, r3
	mov	r3, fp
	bl	RC_uint32_with_filename
	subs	r3, r0, #0
	bne	.L261
	ldr	fp, [r8, #116]
	str	r3, [sp, #16]
	bl	FLOWSENSE_get_controller_index
.LBB43:
	sub	r2, r6, #1
	cmp	r2, #3
	ldr	r3, [sp, #16]
	bhi	.L262
	mov	r2, #4
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #8]
.LBE43:
	add	r2, r4, #148
.LBB44:
	str	r0, [sp, #4]
	str	r2, [sp, #12]
	mov	r0, r4
	mov	r1, r6
	mov	r2, fp
	bl	nm_SYSTEM_set_flow_checking_tolerance_plus.part.5
	b	.L261
.L262:
	ldr	r0, .L266+32
	ldr	r1, .L266+36
	bl	Alert_index_out_of_range_with_filename
.L261:
.LBE44:
	cmp	r7, #4
.LBB45:
	add	r8, r8, #4
.LBE45:
	bne	.L265
	mov	r0, r5
	add	r1, r4, #116
	mov	r2, #16
	bl	memcpy
	b	.L264
.L260:
	ldr	r0, .L266+4
	ldr	r1, .L266+40
	bl	Alert_group_not_found_with_filename
	mov	r3, #5
	str	r3, [r5, #0]
	mov	r3, #10
	str	r3, [r5, #4]
	str	r3, [r5, #8]
	mov	r3, #15
	str	r3, [r5, #12]
.L264:
	ldr	r3, .L266
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L267:
	.align	2
.L266:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	2882
	.word	.LANCHOR0
	.word	.LANCHOR2-4
	.word	.LC16
	.word	.LC25
	.word	2894
	.word	.LC15
	.word	1068
	.word	2911
.LFE65:
	.size	SYSTEM_get_flow_check_tolerances_plus_gpm, .-SYSTEM_get_flow_check_tolerances_plus_gpm
	.section	.text.SYSTEM_get_flow_check_tolerances_minus_gpm,"ax",%progbits
	.align	2
	.global	SYSTEM_get_flow_check_tolerances_minus_gpm
	.type	SYSTEM_get_flow_check_tolerances_minus_gpm, %function
SYSTEM_get_flow_check_tolerances_minus_gpm:
.LFB66:
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L275
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI76:
	ldr	r2, .L275+4
	mov	r4, r0
	sub	sp, sp, #68
.LCFI77:
	ldr	r0, [r3, #0]
	mov	r5, r1
	ldr	r3, .L275+8
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L275+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	beq	.L269
	ldr	r9, .L275+16
	mov	r8, r4
	mov	r6, #0
	add	sl, sp, #20
	b	.L272
.L274:
	mov	r6, r7
.L272:
	mov	r1, #48
	ldr	r2, .L275+20
	ldr	r3, .L275+24
	add	r7, r6, #1
	mov	r0, sl
	str	r7, [sp, #0]
	bl	snprintf
	add	r3, r6, #33
	add	r3, r4, r3, asl #2
	mov	r0, r4
	ldr	fp, [r9, #4]!
	str	r3, [sp, #16]
	bl	nm_GROUP_get_name
	ldr	r2, .L275+4
	ldr	r3, [sp, #16]
	str	r2, [sp, #8]
	ldr	r2, .L275+28
	mov	r1, #1
	str	r2, [sp, #12]
	mov	r2, #200
	stmia	sp, {r0, sl}
	mov	r0, r3
	mov	r3, fp
	bl	RC_uint32_with_filename
	subs	r3, r0, #0
	bne	.L270
	ldr	fp, [r8, #132]
	str	r3, [sp, #16]
	bl	FLOWSENSE_get_controller_index
.LBB46:
	sub	r2, r6, #1
	cmp	r2, #3
	ldr	r3, [sp, #16]
	bhi	.L271
	mov	r2, #4
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #8]
.LBE46:
	add	r2, r4, #148
.LBB47:
	str	r0, [sp, #4]
	str	r2, [sp, #12]
	mov	r0, r4
	mov	r1, r6
	mov	r2, fp
	bl	nm_SYSTEM_set_flow_checking_tolerance_minus.part.6
	b	.L270
.L271:
	ldr	r0, .L275+32
	ldr	r1, .L275+36
	bl	Alert_index_out_of_range_with_filename
.L270:
.LBE47:
	cmp	r7, #4
.LBB48:
	add	r8, r8, #4
.LBE48:
	bne	.L274
	mov	r0, r5
	add	r1, r4, #132
	mov	r2, #16
	bl	memcpy
	b	.L273
.L269:
	ldr	r0, .L275+4
	ldr	r1, .L275+40
	bl	Alert_group_not_found_with_filename
	mov	r3, #5
	str	r3, [r5, #0]
	mov	r3, #10
	str	r3, [r5, #4]
	str	r3, [r5, #8]
	mov	r3, #15
	str	r3, [r5, #12]
.L273:
	ldr	r3, .L275
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L276:
	.align	2
.L275:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	2933
	.word	.LANCHOR0
	.word	.LANCHOR2-4
	.word	.LC16
	.word	.LC26
	.word	2945
	.word	.LC15
	.word	1165
	.word	2962
.LFE66:
	.size	SYSTEM_get_flow_check_tolerances_minus_gpm, .-SYSTEM_get_flow_check_tolerances_minus_gpm
	.section	.text.SYSTEM_get_required_station_cycles_before_flow_checking,"ax",%progbits
	.align	2
	.global	SYSTEM_get_required_station_cycles_before_flow_checking
	.type	SYSTEM_get_required_station_cycles_before_flow_checking, %function
SYSTEM_get_required_station_cycles_before_flow_checking:
.LFB67:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI78:
	ldr	r4, .L278
	mov	r5, r0
	ldr	r3, .L278+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L278+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L278+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	lr, #0
	mov	r3, #6
	stmia	sp, {r3, lr}
	mov	r2, #1
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L278+16
	add	r1, r0, #176
	str	r3, [sp, #12]
	mov	r3, #8
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L279:
	.align	2
.L278:
	.word	list_system_recursive_MUTEX
	.word	3002
	.word	.LC22
	.word	.LANCHOR0
	.word	.LC31
.LFE67:
	.size	SYSTEM_get_required_station_cycles_before_flow_checking, .-SYSTEM_get_required_station_cycles_before_flow_checking
	.section	.text.SYSTEM_get_required_cell_iterations_before_flow_checking,"ax",%progbits
	.align	2
	.global	SYSTEM_get_required_cell_iterations_before_flow_checking
	.type	SYSTEM_get_required_cell_iterations_before_flow_checking, %function
SYSTEM_get_required_cell_iterations_before_flow_checking:
.LFB68:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI79:
	ldr	r4, .L281
	mov	r5, r0
	ldr	r3, .L281+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L281+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L281+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r5, #0
	mov	r3, #6
	stmia	sp, {r3, r5}
	mov	r2, #1
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L281+16
	add	r1, r0, #180
	str	r3, [sp, #12]
	mov	r3, #8
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L282:
	.align	2
.L281:
	.word	list_system_recursive_MUTEX
	.word	3049
	.word	.LC22
	.word	.LANCHOR0
	.word	.LC32
.LFE68:
	.size	SYSTEM_get_required_cell_iterations_before_flow_checking, .-SYSTEM_get_required_cell_iterations_before_flow_checking
	.section	.text.SYSTEM_get_allow_table_to_lock,"ax",%progbits
	.align	2
	.global	SYSTEM_get_allow_table_to_lock
	.type	SYSTEM_get_allow_table_to_lock, %function
SYSTEM_get_allow_table_to_lock:
.LFB69:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI80:
	ldr	r4, .L284
	mov	r5, r0
	ldr	r3, .L284+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L284+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L284+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r2, #1
	add	r3, r0, #148
	str	r3, [sp, #0]
	ldr	r3, .L284+16
	add	r1, r0, #160
	str	r3, [sp, #4]
	mov	r3, #0
	bl	SHARED_get_bool_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L285:
	.align	2
.L284:
	.word	list_system_recursive_MUTEX
	.word	3076
	.word	.LC22
	.word	.LANCHOR0
	.word	.LC33
.LFE69:
	.size	SYSTEM_get_allow_table_to_lock, .-SYSTEM_get_allow_table_to_lock
	.section	.text.SYSTEM_get_derate_table_gpm_slot_size,"ax",%progbits
	.align	2
	.global	SYSTEM_get_derate_table_gpm_slot_size
	.type	SYSTEM_get_derate_table_gpm_slot_size, %function
SYSTEM_get_derate_table_gpm_slot_size:
.LFB70:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI81:
	ldr	r4, .L287
	mov	r5, r0
	ldr	r3, .L287+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L287+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L287+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L287+16
	add	r1, r0, #164
	str	r3, [sp, #12]
	mov	r3, #20
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L288:
	.align	2
.L287:
	.word	list_system_recursive_MUTEX
	.word	3101
	.word	.LC22
	.word	.LANCHOR0
	.word	.LC34
.LFE70:
	.size	SYSTEM_get_derate_table_gpm_slot_size, .-SYSTEM_get_derate_table_gpm_slot_size
	.section	.text.SYSTEM_get_derate_table_max_stations_ON,"ax",%progbits
	.align	2
	.global	SYSTEM_get_derate_table_max_stations_ON
	.type	SYSTEM_get_derate_table_max_stations_ON, %function
SYSTEM_get_derate_table_max_stations_ON:
.LFB71:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI82:
	ldr	r4, .L290
	mov	r5, r0
	ldr	r3, .L290+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L290+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L290+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r2, #10
	mov	r3, #0
	stmia	sp, {r2, r3}
	mov	r2, #1
	add	r3, r0, #148
	str	r3, [sp, #8]
	ldr	r3, .L290+16
	add	r1, r0, #168
	str	r3, [sp, #12]
	mov	r3, #24
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L291:
	.align	2
.L290:
	.word	list_system_recursive_MUTEX
	.word	3131
	.word	.LC22
	.word	.LANCHOR0
	.word	.LC35
.LFE71:
	.size	SYSTEM_get_derate_table_max_stations_ON, .-SYSTEM_get_derate_table_max_stations_ON
	.section	.text.SYSTEM_get_derate_table_number_of_gpm_slots,"ax",%progbits
	.align	2
	.global	SYSTEM_get_derate_table_number_of_gpm_slots
	.type	SYSTEM_get_derate_table_number_of_gpm_slots, %function
SYSTEM_get_derate_table_number_of_gpm_slots:
.LFB72:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI83:
	ldr	r4, .L293
	mov	r5, r0
	ldr	r3, .L293+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L293+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L293+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #500
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	add	ip, r0, #148
	str	ip, [sp, #8]
	ldr	ip, .L293+16
	add	r1, r0, #172
	str	ip, [sp, #12]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L294:
	.align	2
.L293:
	.word	list_system_recursive_MUTEX
	.word	3161
	.word	.LC22
	.word	.LANCHOR0
	.word	.LC36
.LFE72:
	.size	SYSTEM_get_derate_table_number_of_gpm_slots, .-SYSTEM_get_derate_table_number_of_gpm_slots
	.section	.text.SYSTEM_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	SYSTEM_get_change_bits_ptr
	.type	SYSTEM_get_change_bits_ptr, %function
SYSTEM_get_change_bits_ptr:
.LFB73:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r0
	add	r2, r3, #152
	mov	r0, r1
	add	r1, r3, #148
	add	r3, r3, #156
	b	SHARED_get_32_bit_change_bits_ptr
.LFE73:
	.size	SYSTEM_get_change_bits_ptr, .-SYSTEM_get_change_bits_ptr
	.section	.text.nm_SYSTEM_store_changes,"ax",%progbits
	.align	2
	.type	nm_SYSTEM_store_changes, %function
nm_SYSTEM_store_changes:
.LFB20:
	@ args = 12, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI84:
	mov	r5, r0
	sub	sp, sp, #28
.LCFI85:
	mov	r6, r1
	mov	r4, r2
	ldr	r0, .L355
	mov	r1, r5
	add	r2, sp, #24
	mov	r7, r3
	ldr	r9, [sp, #64]
	ldr	r8, [sp, #68]
	ldr	sl, [sp, #72]
	bl	nm_GROUP_find_this_group_in_list
	ldr	r3, [sp, #24]
	cmp	r3, #0
	beq	.L297
	cmp	r6, #1
	bne	.L298
	sub	r3, r8, #15
	cmp	r3, #1
	bls	.L298
	mov	r0, r5
	bl	nm_GROUP_get_name
	mov	r3, #48
	mov	r2, #0
	stmia	sp, {r3, r4, r7}
	mov	r3, r2
	mov	r1, r0
	mov	r0, #49152
	bl	Alert_ChangeLine_Group
.L298:
	mov	r1, r8
	ldr	r0, [sp, #24]
	bl	SYSTEM_get_change_bits_ptr
	cmp	r4, #2
	mov	r8, r0
	beq	.L299
	tst	sl, #1
	beq	.L300
.L299:
	mov	r0, r5
	bl	nm_GROUP_get_name
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	mov	r1, r0
	ldr	r0, [sp, #24]
	add	r3, r0, #156
	str	r3, [sp, #12]
	mov	r3, #0
	rsb	ip, r3, r6
	rsbs	r2, ip, #0
	str	r3, [sp, #16]
	adc	r2, r2, ip
	mov	r3, r4
	bl	SHARED_set_name_32_bit_change_bits
	cmp	r4, #2
	beq	.L301
.L300:
	tst	sl, #4
	beq	.L302
.L301:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #76]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_capacity_in_use
	cmp	r4, #2
	beq	.L303
.L302:
	tst	sl, #8
	beq	.L304
.L303:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #80]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_capacity_with_pump
	cmp	r4, #2
	beq	.L305
.L304:
	tst	sl, #16
	beq	.L306
.L305:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #84]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_capacity_without_pump
	cmp	r4, #2
	beq	.L307
.L306:
	tst	sl, #32
	beq	.L308
.L307:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #88]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_mlb_during_irri
	cmp	r4, #2
	beq	.L309
.L308:
	tst	sl, #64
	beq	.L310
.L309:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #92]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_mlb_during_mvor
	cmp	r4, #2
	beq	.L311
.L310:
	tst	sl, #128
	beq	.L312
.L311:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #96]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_mlb_all_other_times
	cmp	r4, #2
	beq	.L313
.L312:
	tst	sl, #256
	beq	.L314
.L313:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #100]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_flow_checking_in_use
	cmp	r4, #2
	bne	.L314
.L316:
	mov	ip, r5
	mov	fp, #0
	b	.L315
.L314:
	tst	sl, #512
	bne	.L316
	b	.L317
.L315:
	rsbs	r3, r6, #1
	add	fp, fp, #1
	stmia	sp, {r4, r7, r9}
	ldr	r0, [sp, #24]
	str	r8, [sp, #12]
	movcc	r3, #0
	ldr	r2, [ip, #104]
	mov	r1, fp
	str	ip, [sp, #20]
	bl	nm_SYSTEM_set_flow_checking_range.part.4
	ldr	ip, [sp, #20]
	cmp	fp, #3
	add	ip, ip, #4
	bne	.L315
	cmp	r4, #2
	bne	.L317
.L319:
	mov	ip, r5
	mov	fp, #0
	b	.L318
.L317:
	tst	sl, #1024
	bne	.L319
	b	.L320
.L318:
	rsbs	r3, r6, #1
	add	fp, fp, #1
	stmia	sp, {r4, r7, r9}
	ldr	r0, [sp, #24]
	str	r8, [sp, #12]
	movcc	r3, #0
	ldr	r2, [ip, #116]
	mov	r1, fp
	str	ip, [sp, #20]
	bl	nm_SYSTEM_set_flow_checking_tolerance_plus.part.5
	ldr	ip, [sp, #20]
	cmp	fp, #4
	add	ip, ip, #4
	bne	.L318
	cmp	r4, #2
	bne	.L320
.L322:
	mov	ip, r5
	mov	fp, #0
	b	.L321
.L320:
	tst	sl, #2048
	bne	.L322
	b	.L323
.L321:
	rsbs	r3, r6, #1
	add	fp, fp, #1
	stmia	sp, {r4, r7, r9}
	ldr	r0, [sp, #24]
	str	r8, [sp, #12]
	movcc	r3, #0
	ldr	r2, [ip, #132]
	mov	r1, fp
	str	ip, [sp, #20]
	bl	nm_SYSTEM_set_flow_checking_tolerance_minus.part.6
	ldr	ip, [sp, #20]
	cmp	fp, #4
	add	ip, ip, #4
	bne	.L321
	cmp	r4, #2
	bne	.L323
.L325:
	mov	ip, r5
	mov	fp, #0
	b	.L324
.L323:
	tst	sl, #4096
	bne	.L325
	b	.L326
.L324:
	rsbs	r3, r6, #1
	stmia	sp, {r4, r7, r9}
	ldr	r0, [sp, #24]
	str	r8, [sp, #12]
	mov	r1, fp
	ldr	r2, [ip, #188]
	movcc	r3, #0
	str	ip, [sp, #20]
	bl	nm_SYSTEM_set_mvor_schedule_open_time
	ldr	ip, [sp, #20]
	add	fp, fp, #1
	cmp	fp, #7
	add	ip, ip, #4
	bne	.L324
	cmp	r4, #2
	bne	.L326
.L328:
	mov	ip, r5
	mov	fp, #0
	b	.L327
.L326:
	tst	sl, #8192
	bne	.L328
	b	.L329
.L327:
	rsbs	r3, r6, #1
	stmia	sp, {r4, r7, r9}
	ldr	r0, [sp, #24]
	str	r8, [sp, #12]
	mov	r1, fp
	ldr	r2, [ip, #216]
	movcc	r3, #0
	str	ip, [sp, #20]
	bl	nm_SYSTEM_set_mvor_schedule_close_time
	ldr	ip, [sp, #20]
	add	fp, fp, #1
	cmp	fp, #7
	add	ip, ip, #4
	bne	.L327
	cmp	r4, #2
	beq	.L330
.L329:
	tst	sl, #16384
	beq	.L331
.L330:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #352]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_budget_in_use
	cmp	r4, #2
	beq	.L332
.L331:
	tst	sl, #32768
	beq	.L333
.L332:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #248]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_budget_meter_read_time
	cmp	r4, #2
	beq	.L334
.L333:
	tst	sl, #65536
	beq	.L335
.L334:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #252]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_budget_number_of_annual_periods
	cmp	r4, #2
	bne	.L335
.L337:
	mov	ip, r5
	mov	fp, #0
	b	.L336
.L335:
	tst	sl, #131072
	bne	.L337
	b	.L338
.L336:
	rsbs	r3, r6, #1
	stmia	sp, {r4, r7, r9}
	ldr	r0, [sp, #24]
	str	r8, [sp, #12]
	mov	r1, fp
	ldr	r2, [ip, #256]
	movcc	r3, #0
	str	ip, [sp, #20]
	bl	nm_SYSTEM_set_budget_period
	ldr	ip, [sp, #20]
	add	fp, fp, #1
	cmp	fp, #24
	add	ip, ip, #4
	bne	.L336
	cmp	r4, #2
	beq	.L339
.L338:
	tst	sl, #262144
	beq	.L340
.L339:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #244]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_budget_mode
	cmp	r4, #2
	bne	.L340
.L342:
	mov	ip, r5
	mov	fp, #0
	b	.L341
.L340:
	tst	sl, #1048576
	bne	.L342
	b	.L343
.L341:
	rsbs	r3, r6, #1
	stmia	sp, {r4, r7, r9}
	ldr	r0, [sp, #24]
	str	r8, [sp, #12]
	mov	r1, fp
	ldr	r2, [ip, #368]
	movcc	r3, #0
	str	ip, [sp, #20]
	bl	nm_SYSTEM_set_budget_flow_type
	ldr	ip, [sp, #20]
	add	fp, fp, #1
	cmp	fp, #10
	add	ip, ip, #4
	bne	.L341
	cmp	r4, #2
	beq	.L344
.L343:
	tst	sl, #524288
	beq	.L345
.L344:
	ldr	r1, [sp, #24]
	ldr	r3, [r1, #356]
	cmp	r3, #0
	beq	.L346
	ldr	r3, [r5, #356]
	cmp	r3, #0
	bne	.L346
	ldr	r0, .L355
	ldr	r2, .L355+4
	ldr	r3, .L355+8
	bl	nm_ListRemove_debug
	ldr	r0, .L355
	ldr	r1, [sp, #24]
	bl	nm_ListInsertTail
.L346:
	rsbs	r2, r6, #1
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #356]
	movcc	r2, #0
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_SYSTEM_set_system_in_use
.L345:
	bl	SYSTEM_PRESERVES_synchronize_preserves_to_file
	b	.L296
.L297:
	ldr	r0, .L355+4
	ldr	r1, .L355+12
	bl	Alert_group_not_found_with_filename
.L296:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L356:
	.align	2
.L355:
	.word	.LANCHOR0
	.word	.LC15
	.word	1889
	.word	1914
.LFE20:
	.size	nm_SYSTEM_store_changes, .-nm_SYSTEM_store_changes
	.section	.text.BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars
	.type	BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars, %function
BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars:
.LFB46:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, lr}
.LCFI86:
	ldr	r5, .L358
	ldr	r6, .L358+4
	mov	r2, r5
	ldr	r3, .L358+8
	ldr	r0, [r6, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r2, .L358+12
	mov	r0, #408
	bl	mem_malloc_debug
	ldr	r3, .L358+16
	mov	r7, #1
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_group_with_this_GID
	mov	r2, #408
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, .L358+20
	str	r7, [r4, #368]
	ldr	r3, [r3, #0]
	str	r7, [r4, #372]
	str	r3, [r4, #400]
	ldr	r3, .L358+24
	str	r7, [r4, #376]
	ldr	r3, [r3, #0]
	str	r7, [r4, #384]
	str	r3, [r4, #404]
	str	r7, [r4, #388]
	str	r7, [r4, #392]
	str	r7, [r4, #396]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #2
	mov	r1, #0
	str	r2, [sp, #4]
	str	r1, [sp, #8]
	str	r7, [sp, #0]
	mov	r3, r0
	mov	r0, r4
	bl	nm_SYSTEM_store_changes
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2128
	bl	mem_free_debug
	ldr	r0, [r6, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L359:
	.align	2
.L358:
	.word	.LC22
	.word	list_system_recursive_MUTEX
	.word	2103
	.word	2105
	.word	g_GROUP_ID
	.word	GuiVar_BudgetFlowTypeMVOR
	.word	GuiVar_BudgetFlowTypeNonController
.LFE46:
	.size	BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars, .-BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars
	.section	.text.BUDGET_SETUP_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	BUDGET_SETUP_extract_and_store_changes_from_GuiVars
	.type	BUDGET_SETUP_extract_and_store_changes_from_GuiVars, %function
BUDGET_SETUP_extract_and_store_changes_from_GuiVars:
.LFB45:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI87:
	ldr	r3, .L370
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L370+4
	ldr	r3, .L370+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L370+12
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r1, .L370+4
	ldr	r2, .L370+16
	mov	r0, #408
	bl	mem_malloc_debug
	ldr	r3, .L370+20
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_group_with_this_GID
	mov	r2, #408
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldr	r2, .L370+24
	ldr	r3, [r0, #468]
	ands	r3, r3, #122880
	movne	r3, #1
	cmp	r3, #1
	str	r3, [r2, #0]
	bne	.L361
	ldr	r3, .L370+28
	ldr	r3, [r3, #0]
	str	r3, [r4, #352]
	ldr	r3, .L370+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L362
	ldr	r3, .L370+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	moveq	r3, #6
	beq	.L369
.L362:
	mov	r3, #12
.L369:
	str	r3, [r4, #252]
.LBB49:
.LBB50:
	ldr	r8, .L370+20
.LBE50:
.LBE49:
	ldr	r3, .L370+40
.LBB55:
.LBB53:
.LBB51:
	ldr	r9, .L370+32
.LBE51:
.LBE53:
.LBE55:
	mov	r5, #0
	str	r3, [r4, #248]
.L366:
.LBB56:
.LBB54:
	ldr	r2, .L370+44
	mov	r3, #472
	mla	r3, r5, r3, r2
	ldr	r0, [r3, #24]
	add	r3, r3, #20
	cmp	r0, #0
	beq	.L365
	ldr	r3, [r3, #24]
	ldr	r2, [r8, #0]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L365
.LBB52:
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	ldr	sl, [r9, #0]
	mov	r7, r0
	bl	FLOWSENSE_get_controller_index
	mov	r1, #2
	mov	r6, r0
	mov	r0, r7
	bl	POC_get_change_bits_ptr
	str	r6, [sp, #0]
	mov	r6, #1
	mov	r1, sl
	mov	r2, r6
	mov	r3, #2
	str	r6, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r7
	bl	nm_POC_set_budget_gallons_entry_option
	ldr	sl, [r9, #0]
	cmp	sl, #2
	bne	.L365
	ldr	r3, .L370+48
	ldr	r3, [r3, #0]
	str	r3, [sp, #12]
	bl	FLOWSENSE_get_controller_index
	mov	r1, sl
	mov	fp, r0
	mov	r0, r7
	bl	POC_get_change_bits_ptr
	ldr	r3, [sp, #12]
	mov	r2, r6
	mov	r1, r3
	mov	r3, sl
	str	fp, [sp, #0]
	str	r6, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r7
	bl	nm_POC_set_budget_calculation_percent_of_et
.L365:
.LBE52:
.LBE54:
	add	r5, r5, #1
	cmp	r5, #12
	bne	.L366
.LBE56:
	ldr	r5, .L370+12
	ldr	r1, [r5, #0]
	cmp	r1, #1
	bne	.L367
	mov	r0, r4
	bl	BUDGET_calc_POC_monthly_budget_using_et
	mov	r3, #0
	str	r3, [r5, #0]
.L367:
	ldr	r3, .L370+52
	ldr	r3, [r3, #0]
	str	r3, [r4, #244]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r1, #0
	mov	r2, #2
	str	r2, [sp, #4]
	str	r1, [sp, #8]
	mov	r3, r0
	mov	r0, r4
	bl	nm_SYSTEM_store_changes
.L361:
	mov	r0, r4
	ldr	r1, .L370+4
	ldr	r2, .L370+56
	bl	mem_free_debug
	ldr	r3, .L370
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	xQueueGiveMutexRecursive
.L371:
	.align	2
.L370:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	1993
	.word	g_BUDGET_using_et_calculate_budget
	.word	1999
	.word	g_GROUP_ID
	.word	GuiVar_BudgetSysHasPOC
	.word	GuiVar_BudgetInUse
	.word	GuiVar_BudgetEntryOptionIdx
	.word	GuiVar_BudgetPeriodsPerYearIdx
	.word	43200
	.word	poc_preserves
	.word	GuiVar_BudgetETPercentageUsed
	.word	GuiVar_BudgetModeIdx
	.word	2090
.LFE45:
	.size	BUDGET_SETUP_extract_and_store_changes_from_GuiVars, .-BUDGET_SETUP_extract_and_store_changes_from_GuiVars
	.section	.text.MVOR_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	MVOR_extract_and_store_changes_from_GuiVars
	.type	MVOR_extract_and_store_changes_from_GuiVars, %function
MVOR_extract_and_store_changes_from_GuiVars:
.LFB44:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI88:
	ldr	r5, .L373
	ldr	r6, .L373+4
	mov	r2, r5
	ldr	r3, .L373+8
	ldr	r0, [r6, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r2, .L373+12
	mov	r0, #408
	bl	mem_malloc_debug
	ldr	r3, .L373+16
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_group_with_this_GID
	mov	r2, #408
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, .L373+20
	ldr	r2, [r3, #0]
	mov	r3, #60
	mul	r2, r3, r2
	str	r2, [r4, #188]
	ldr	r2, .L373+24
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #192]
	ldr	r2, .L373+28
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #196]
	ldr	r2, .L373+32
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #200]
	ldr	r2, .L373+36
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #204]
	ldr	r2, .L373+40
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #208]
	ldr	r2, .L373+44
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #212]
	ldr	r2, .L373+48
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #216]
	ldr	r2, .L373+52
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #220]
	ldr	r2, .L373+56
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #224]
	ldr	r2, .L373+60
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #228]
	ldr	r2, .L373+64
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #232]
	ldr	r2, .L373+68
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #236]
	ldr	r2, .L373+72
	ldr	r2, [r2, #0]
	mul	r3, r2, r3
	str	r3, [r4, #240]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r1, #0
	mov	r2, #2
	str	r2, [sp, #4]
	str	r1, [sp, #8]
	mov	r3, r0
	mov	r0, r4
	bl	nm_SYSTEM_store_changes
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L373+76
	bl	mem_free_debug
	ldr	r0, [r6, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L374:
	.align	2
.L373:
	.word	.LC22
	.word	list_system_recursive_MUTEX
	.word	1946
	.word	1950
	.word	g_GROUP_ID
	.word	GuiVar_MVOROpen0
	.word	GuiVar_MVOROpen1
	.word	GuiVar_MVOROpen2
	.word	GuiVar_MVOROpen3
	.word	GuiVar_MVOROpen4
	.word	GuiVar_MVOROpen5
	.word	GuiVar_MVOROpen6
	.word	GuiVar_MVORClose0
	.word	GuiVar_MVORClose1
	.word	GuiVar_MVORClose2
	.word	GuiVar_MVORClose3
	.word	GuiVar_MVORClose4
	.word	GuiVar_MVORClose5
	.word	GuiVar_MVORClose6
	.word	1978
.LFE44:
	.size	MVOR_extract_and_store_changes_from_GuiVars, .-MVOR_extract_and_store_changes_from_GuiVars
	.section	.text.FLOW_CHECKING_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	FLOW_CHECKING_extract_and_store_changes_from_GuiVars
	.type	FLOW_CHECKING_extract_and_store_changes_from_GuiVars, %function
FLOW_CHECKING_extract_and_store_changes_from_GuiVars:
.LFB43:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI89:
	ldr	r5, .L376
	ldr	r6, .L376+4
	mov	r2, r5
	ldr	r3, .L376+8
	ldr	r0, [r6, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r2, .L376+12
	mov	r0, #408
	bl	mem_malloc_debug
	ldr	r3, .L376+16
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_group_with_this_GID
	mov	r2, #408
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, .L376+20
	ldr	r3, [r3, #0]
	str	r3, [r4, #100]
	ldr	r3, .L376+24
	ldr	r3, [r3, #0]
	str	r3, [r4, #104]
	ldr	r3, .L376+28
	ldr	r3, [r3, #0]
	str	r3, [r4, #108]
	ldr	r3, .L376+32
	ldr	r3, [r3, #0]
	str	r3, [r4, #112]
	ldr	r3, .L376+36
	ldr	r3, [r3, #0]
	str	r3, [r4, #116]
	ldr	r3, .L376+40
	ldr	r3, [r3, #0]
	str	r3, [r4, #120]
	ldr	r3, .L376+44
	ldr	r3, [r3, #0]
	str	r3, [r4, #124]
	ldr	r3, .L376+48
	ldr	r3, [r3, #0]
	str	r3, [r4, #128]
	ldr	r3, .L376+52
	ldr	r0, [r3, #0]
	bl	abs
	ldr	r3, .L376+56
	str	r0, [r4, #132]
	ldr	r0, [r3, #0]
	bl	abs
	ldr	r3, .L376+60
	str	r0, [r4, #136]
	ldr	r0, [r3, #0]
	bl	abs
	ldr	r3, .L376+64
	str	r0, [r4, #140]
	ldr	r0, [r3, #0]
	bl	abs
	str	r0, [r4, #144]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r1, #0
	mov	r2, #2
	str	r2, [sp, #4]
	str	r1, [sp, #8]
	mov	r3, r0
	mov	r0, r4
	bl	nm_SYSTEM_store_changes
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L376+68
	bl	mem_free_debug
	ldr	r0, [r6, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L377:
	.align	2
.L376:
	.word	.LC22
	.word	list_system_recursive_MUTEX
	.word	1903
	.word	1907
	.word	g_GROUP_ID
	.word	GuiVar_SystemFlowCheckingInUse
	.word	GuiVar_SystemFlowCheckingRange1
	.word	GuiVar_SystemFlowCheckingRange2
	.word	GuiVar_SystemFlowCheckingRange3
	.word	GuiVar_SystemFlowCheckingTolerancePlus1
	.word	GuiVar_SystemFlowCheckingTolerancePlus2
	.word	GuiVar_SystemFlowCheckingTolerancePlus3
	.word	GuiVar_SystemFlowCheckingTolerancePlus4
	.word	GuiVar_SystemFlowCheckingToleranceMinus1
	.word	GuiVar_SystemFlowCheckingToleranceMinus2
	.word	GuiVar_SystemFlowCheckingToleranceMinus3
	.word	GuiVar_SystemFlowCheckingToleranceMinus4
	.word	1932
.LFE43:
	.size	FLOW_CHECKING_extract_and_store_changes_from_GuiVars, .-FLOW_CHECKING_extract_and_store_changes_from_GuiVars
	.section	.text.SYSTEM_CAPACITY_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	SYSTEM_CAPACITY_extract_and_store_individual_group_change, %function
SYSTEM_CAPACITY_extract_and_store_individual_group_change:
.LFB41:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI90:
	ldr	r4, [sp, #40]
	mov	r5, r3
	ldr	r3, .L380
	mov	r8, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	mov	r1, #400
	ldr	r2, .L380+4
	ldr	r3, .L380+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	sl, [r4, #0]
	cmp	sl, #1
	bne	.L379
	ldr	r1, .L380+4
	ldr	r2, .L380+12
	mov	r0, #408
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r8
	bl	SYSTEM_get_group_at_this_index
	mov	r2, #408
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r7, #0]
	str	r3, [r4, #76]
	ldr	r3, [r6, #0]
	str	r3, [r4, #80]
	ldr	r3, [r5, #0]
	str	r3, [r4, #84]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #2
	mov	r1, #0
	str	r2, [sp, #4]
	str	r1, [sp, #8]
	str	sl, [sp, #0]
	mov	r3, r0
	mov	r0, r4
	bl	nm_SYSTEM_store_changes
	mov	r0, r4
	ldr	r1, .L380+4
	ldr	r2, .L380+16
	bl	mem_free_debug
.L379:
	ldr	r3, .L380
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L381:
	.align	2
.L380:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	1855
	.word	1859
	.word	1875
.LFE41:
	.size	SYSTEM_CAPACITY_extract_and_store_individual_group_change, .-SYSTEM_CAPACITY_extract_and_store_individual_group_change
	.section	.text.SYSTEM_CAPACITY_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	SYSTEM_CAPACITY_extract_and_store_changes_from_GuiVars
	.type	SYSTEM_CAPACITY_extract_and_store_changes_from_GuiVars, %function
SYSTEM_CAPACITY_extract_and_store_changes_from_GuiVars:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI91:
	ldr	r4, .L383
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L383+4
	mov	r3, #1888
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L383+8
	mov	r0, #0
	str	r3, [sp, #0]
	ldr	r1, .L383+12
	ldr	r2, .L383+16
	ldr	r3, .L383+20
	bl	SYSTEM_CAPACITY_extract_and_store_individual_group_change
	ldr	r3, .L383+24
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r1, .L383+28
	ldr	r2, .L383+32
	ldr	r3, .L383+36
	bl	SYSTEM_CAPACITY_extract_and_store_individual_group_change
	ldr	r3, .L383+40
	mov	r0, #2
	str	r3, [sp, #0]
	ldr	r1, .L383+44
	ldr	r2, .L383+48
	ldr	r3, .L383+52
	bl	SYSTEM_CAPACITY_extract_and_store_individual_group_change
	ldr	r3, .L383+56
	mov	r0, #3
	str	r3, [sp, #0]
	ldr	r1, .L383+60
	ldr	r2, .L383+64
	ldr	r3, .L383+68
	bl	SYSTEM_CAPACITY_extract_and_store_individual_group_change
	ldr	r0, [r4, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L384:
	.align	2
.L383:
	.word	list_program_data_recursive_MUTEX
	.word	.LC22
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingC_3
.LFE42:
	.size	SYSTEM_CAPACITY_extract_and_store_changes_from_GuiVars, .-SYSTEM_CAPACITY_extract_and_store_changes_from_GuiVars
	.section	.text.MAINLINE_BREAK_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	MAINLINE_BREAK_extract_and_store_individual_group_change, %function
MAINLINE_BREAK_extract_and_store_individual_group_change:
.LFB39:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI92:
	ldr	r4, [sp, #40]
	mov	r5, r3
	ldr	r3, .L387
	mov	r8, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	mov	r1, #400
	ldr	r2, .L387+4
	ldr	r3, .L387+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	sl, [r4, #0]
	cmp	sl, #1
	bne	.L386
	ldr	r1, .L387+4
	ldr	r2, .L387+12
	mov	r0, #408
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r8
	bl	SYSTEM_get_group_at_this_index
	mov	r2, #408
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r7, #0]
	str	r3, [r4, #88]
	ldr	r3, [r6, #0]
	str	r3, [r4, #92]
	ldr	r3, [r5, #0]
	str	r3, [r4, #96]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #2
	mov	r1, #0
	str	r2, [sp, #4]
	str	r1, [sp, #8]
	str	sl, [sp, #0]
	mov	r3, r0
	mov	r0, r4
	bl	nm_SYSTEM_store_changes
	mov	r0, r4
	ldr	r1, .L387+4
	ldr	r2, .L387+16
	bl	mem_free_debug
.L386:
	ldr	r3, .L387
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L388:
	.align	2
.L387:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	1811
	.word	1815
	.word	1831
.LFE39:
	.size	MAINLINE_BREAK_extract_and_store_individual_group_change, .-MAINLINE_BREAK_extract_and_store_individual_group_change
	.section	.text.MAINLINE_BREAK_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	MAINLINE_BREAK_extract_and_store_changes_from_GuiVars
	.type	MAINLINE_BREAK_extract_and_store_changes_from_GuiVars, %function
MAINLINE_BREAK_extract_and_store_changes_from_GuiVars:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L390
	stmfd	sp!, {r0, lr}
.LCFI93:
	ldr	r1, .L390+4
	str	r3, [sp, #0]
	mov	r0, #0
	ldr	r2, .L390+8
	ldr	r3, .L390+12
	bl	MAINLINE_BREAK_extract_and_store_individual_group_change
	ldr	r3, .L390+16
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r1, .L390+20
	ldr	r2, .L390+24
	ldr	r3, .L390+28
	bl	MAINLINE_BREAK_extract_and_store_individual_group_change
	ldr	r3, .L390+32
	mov	r0, #2
	str	r3, [sp, #0]
	ldr	r1, .L390+36
	ldr	r2, .L390+40
	ldr	r3, .L390+44
	bl	MAINLINE_BREAK_extract_and_store_individual_group_change
	ldr	r3, .L390+48
	mov	r0, #3
	str	r3, [sp, #0]
	ldr	r1, .L390+52
	ldr	r2, .L390+56
	ldr	r3, .L390+60
	bl	MAINLINE_BREAK_extract_and_store_individual_group_change
	ldmfd	sp!, {r3, pc}
.L391:
	.align	2
.L390:
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingC_3
.LFE40:
	.size	MAINLINE_BREAK_extract_and_store_changes_from_GuiVars, .-MAINLINE_BREAK_extract_and_store_changes_from_GuiVars
	.section	.text.SYSTEM_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	SYSTEM_extract_and_store_changes_from_GuiVars
	.type	SYSTEM_extract_and_store_changes_from_GuiVars, %function
SYSTEM_extract_and_store_changes_from_GuiVars:
.LFB38:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI94:
	ldr	r5, .L393
	ldr	r7, .L393+4
	ldr	r3, .L393+8
	mov	r1, #400
	mov	r2, r5
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r2, .L393+12
	mov	r0, #408
	bl	mem_malloc_debug
	ldr	r3, .L393+16
	ldr	r8, .L393+20
	mov	r6, #0
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_group_with_this_GID
	mov	r2, #408
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r1, .L393+24
	mov	r2, #48
	add	r0, r4, #20
	bl	strlcpy
	ldr	r3, .L393+28
	ldr	sl, [r8, #0]
	ldr	r3, [r3, #0]
	str	r3, [r4, #356]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r1, sl
	mov	r2, #2
	stmib	sp, {r2, r6}
	mov	r3, r0
	mov	r0, r4
	bl	nm_SYSTEM_store_changes
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L393+32
	bl	mem_free_debug
	ldr	r0, [r7, #0]
	bl	xQueueGiveMutexRecursive
	str	r6, [r8, #0]
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, sl, pc}
.L394:
	.align	2
.L393:
	.word	.LC22
	.word	list_system_recursive_MUTEX
	.word	1775
	.word	1779
	.word	g_GROUP_ID
	.word	g_GROUP_creating_new
	.word	GuiVar_GroupName
	.word	GuiVar_SystemInUse
	.word	1795
.LFE38:
	.size	SYSTEM_extract_and_store_changes_from_GuiVars, .-SYSTEM_extract_and_store_changes_from_GuiVars
	.section	.text.nm_SYSTEM_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_extract_and_store_changes_from_comm
	.type	nm_SYSTEM_extract_and_store_changes_from_comm, %function
nm_SYSTEM_extract_and_store_changes_from_comm:
.LFB21:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI95:
	mov	r8, r0
	sub	sp, sp, #36
.LCFI96:
	str	r1, [sp, #12]
	str	r2, [sp, #16]
	mov	r1, r8
	add	r0, sp, #24
	mov	r2, #4
	mov	r9, #0
	mov	r7, r3
	add	r8, r8, #4
	bl	memcpy
	mov	r5, #4
	mov	fp, r9
	b	.L396
.L420:
	mov	r1, r8
	mov	r2, #4
	add	r0, sp, #32
	bl	memcpy
	add	r1, r8, #4
	mov	r2, #4
	add	r0, sp, #28
	bl	memcpy
	add	r1, r8, #8
	mov	r2, #4
	add	r0, sp, #20
	bl	memcpy
	ldr	r0, .L425
	ldr	r1, [sp, #32]
	mov	r2, #1
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	add	r4, r8, #12
	subs	sl, r0, #0
	bne	.L397
	mov	r0, r7
	bl	nm_SYSTEM_create_new_group
	ldr	r3, [sp, #32]
	str	r3, [r0, #16]
	mov	sl, r0
	bl	SYSTEM_PRESERVES_synchronize_preserves_to_file
	cmp	r7, #16
	bne	.L424
	ldr	r3, .L425+4
	add	r0, sl, #148
	ldr	r1, [r3, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L424:
	mov	fp, #1
.L397:
	ldr	r1, .L425+8
	mov	r2, #2080
	mov	r0, #408
	bl	mem_malloc_debug
	mov	r1, #0
	mov	r2, #408
	mov	r6, r0
	bl	memset
	mov	r0, r6
	mov	r1, sl
	mov	r2, #408
	bl	memcpy
	ldr	r3, [sp, #20]
	tst	r3, #1
	addeq	r5, r5, #12
	beq	.L399
	mov	r1, r4
	add	r0, r6, #20
	mov	r2, #48
	bl	memcpy
	add	r4, r8, #60
	add	r5, r5, #60
.L399:
	ldr	r3, [sp, #20]
	tst	r3, #2
	beq	.L400
	mov	r1, r4
	add	r0, r6, #72
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L400:
	ldr	r3, [sp, #20]
	tst	r3, #4
	beq	.L401
	mov	r1, r4
	add	r0, r6, #76
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L401:
	ldr	r3, [sp, #20]
	tst	r3, #8
	beq	.L402
	mov	r1, r4
	add	r0, r6, #80
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L402:
	ldr	r3, [sp, #20]
	tst	r3, #16
	beq	.L403
	mov	r1, r4
	add	r0, r6, #84
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L403:
	ldr	r3, [sp, #20]
	tst	r3, #32
	beq	.L404
	mov	r1, r4
	add	r0, r6, #88
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L404:
	ldr	r3, [sp, #20]
	tst	r3, #64
	beq	.L405
	mov	r1, r4
	add	r0, r6, #92
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L405:
	ldr	r3, [sp, #20]
	tst	r3, #128
	beq	.L406
	mov	r1, r4
	add	r0, r6, #96
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L406:
	ldr	r3, [sp, #20]
	tst	r3, #256
	beq	.L407
	mov	r1, r4
	add	r0, r6, #100
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L407:
	ldr	r3, [sp, #20]
	tst	r3, #512
	beq	.L408
	mov	r1, r4
	add	r0, r6, #104
	mov	r2, #12
	bl	memcpy
	add	r4, r4, #12
	add	r5, r5, #12
.L408:
	ldr	r3, [sp, #20]
	tst	r3, #1024
	beq	.L409
	mov	r1, r4
	add	r0, r6, #116
	mov	r2, #16
	bl	memcpy
	add	r4, r4, #16
	add	r5, r5, #16
.L409:
	ldr	r3, [sp, #20]
	tst	r3, #2048
	beq	.L410
	mov	r1, r4
	add	r0, r6, #132
	mov	r2, #16
	bl	memcpy
	add	r4, r4, #16
	add	r5, r5, #16
.L410:
	ldr	r3, [sp, #20]
	tst	r3, #4096
	beq	.L411
	mov	r1, r4
	add	r0, r6, #188
	mov	r2, #28
	bl	memcpy
	add	r4, r4, #28
	add	r5, r5, #28
.L411:
	ldr	r3, [sp, #20]
	tst	r3, #8192
	beq	.L412
	mov	r1, r4
	add	r0, r6, #216
	mov	r2, #28
	bl	memcpy
	add	r4, r4, #28
	add	r5, r5, #28
.L412:
	ldr	r3, [sp, #20]
	tst	r3, #16384
	beq	.L413
	mov	r1, r4
	add	r0, r6, #352
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L413:
	ldr	r3, [sp, #20]
	tst	r3, #32768
	beq	.L414
	mov	r1, r4
	add	r0, r6, #248
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L414:
	ldr	r3, [sp, #20]
	tst	r3, #65536
	beq	.L415
	mov	r1, r4
	add	r0, r6, #252
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L415:
	ldr	r3, [sp, #20]
	tst	r3, #131072
	beq	.L416
	mov	r1, r4
	add	r0, r6, #256
	mov	r2, #96
	bl	memcpy
	add	r4, r4, #96
	add	r5, r5, #96
.L416:
	ldr	r3, [sp, #20]
	tst	r3, #262144
	beq	.L417
	mov	r1, r4
	add	r0, r6, #244
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L417:
	ldr	r3, [sp, #20]
	tst	r3, #524288
	beq	.L418
	mov	r1, r4
	add	r0, r6, #356
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L418:
	ldr	r3, [sp, #20]
	tst	r3, #1048576
	beq	.L419
	mov	r1, r4
	add	r0, r6, #368
	mov	r2, #40
	bl	memcpy
	add	r4, r4, #40
	add	r5, r5, #40
.L419:
	ldr	r3, [sp, #16]
	mov	r0, r6
	stmia	sp, {r3, r7}
	ldr	r3, [sp, #20]
	mov	r1, fp
	str	r3, [sp, #8]
	ldr	r2, [sp, #12]
	mov	r3, #0
	bl	nm_SYSTEM_store_changes
	mov	r0, r6
	ldr	r1, .L425+8
	ldr	r2, .L425+12
	bl	mem_free_debug
	add	r9, r9, #1
	mov	r8, r4
.L396:
	ldr	r3, [sp, #24]
	cmp	r9, r3
	bcc	.L420
	cmp	r5, #0
	beq	.L421
	cmp	r7, #1
	cmpne	r7, #15
	beq	.L422
	cmp	r7, #16
	bne	.L423
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L423
.L422:
	mov	r0, #10
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L423
.L421:
	ldr	r0, .L425+8
	ldr	r1, .L425+16
	bl	Alert_bit_set_with_no_data_with_filename
.L423:
	mov	r0, r5
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L426:
	.align	2
.L425:
	.word	.LANCHOR0
	.word	list_system_recursive_MUTEX
	.word	.LC15
	.word	2283
	.word	2335
.LFE21:
	.size	nm_SYSTEM_extract_and_store_changes_from_comm, .-nm_SYSTEM_extract_and_store_changes_from_comm
	.section	.text.SYSTEM_extract_and_store_group_name_from_GuiVars,"ax",%progbits
	.align	2
	.global	SYSTEM_extract_and_store_group_name_from_GuiVars
	.type	SYSTEM_extract_and_store_group_name_from_GuiVars, %function
SYSTEM_extract_and_store_group_name_from_GuiVars:
.LFB37:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L429
	stmfd	sp!, {r4, r5, lr}
.LCFI97:
	ldr	r0, [r3, #0]
	sub	sp, sp, #20
.LCFI98:
	mov	r1, #400
	ldr	r2, .L429+4
	ldr	r3, .L429+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L429+12
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_group_at_this_index
	subs	r4, r0, #0
	beq	.L428
	bl	FLOWSENSE_get_controller_index
	mov	r1, #2
	mov	r5, r0
	mov	r0, r4
	bl	SYSTEM_get_change_bits_ptr
	add	r3, r4, #156
	str	r3, [sp, #12]
	mov	r3, #0
	mov	r2, #1
	str	r3, [sp, #16]
	ldr	r1, .L429+16
	mov	r3, #2
	str	r5, [sp, #0]
	str	r2, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	SHARED_set_name_32_bit_change_bits
.L428:
	ldr	r3, .L429
	ldr	r0, [r3, #0]
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L430:
	.align	2
.L429:
	.word	list_program_data_recursive_MUTEX
	.word	.LC22
	.word	1742
	.word	g_GROUP_list_item_index
	.word	GuiVar_GroupName
.LFE37:
	.size	SYSTEM_extract_and_store_group_name_from_GuiVars, .-SYSTEM_extract_and_store_group_name_from_GuiVars
	.section	.text.SYSTEM_build_data_to_send,"ax",%progbits
	.align	2
	.global	SYSTEM_build_data_to_send
	.type	SYSTEM_build_data_to_send, %function
SYSTEM_build_data_to_send:
.LFB27:
	@ args = 4, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI99:
	mov	r7, r3
	sub	sp, sp, #60
.LCFI100:
	mov	r3, #0
	str	r3, [sp, #56]
	ldr	r3, .L449
	str	r1, [sp, #36]
	mov	r4, r0
	mov	r6, r2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L449+4
	ldr	r3, .L449+8
	ldr	r8, [sp, #96]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L449+12
	bl	nm_ListGetFirst
	b	.L447
.L435:
	mov	r0, r5
	mov	r1, r7
	bl	SYSTEM_get_change_bits_ptr
	ldr	r3, [r0, #0]
	cmp	r3, #0
	ldrne	r3, [sp, #56]
	addne	r3, r3, #1
	strne	r3, [sp, #56]
	bne	.L434
.L433:
	ldr	r0, .L449+12
	mov	r1, r5
	bl	nm_ListGetNext
.L447:
	cmp	r0, #0
	mov	r5, r0
	bne	.L435
.L434:
	ldr	r3, [sp, #56]
	cmp	r3, #0
	beq	.L445
	mov	r3, #0
	str	r3, [sp, #56]
	mov	r0, r4
	mov	r3, r6
	add	r1, sp, #48
	ldr	r2, [sp, #36]
	str	r8, [sp, #0]
	bl	PDATA_allocate_space_for_num_changed_groups_in_pucp
	ldr	r3, [r6, #0]
	cmp	r3, #0
	str	r0, [sp, #32]
	beq	.L436
	ldr	r0, .L449+12
	bl	nm_ListGetFirst
	ldr	r2, [sp, #36]
	add	r2, r2, #12
	str	r2, [sp, #40]
	mov	r5, r0
	b	.L437
.L443:
	mov	r0, r5
	mov	r1, r7
	bl	SYSTEM_get_change_bits_ptr
	subs	r9, r0, #0
	beq	.L438
	ldr	r3, [r6, #0]
	cmp	r3, #0
	beq	.L439
	ldr	r3, [sp, #40]
	ldr	r2, [sp, #32]
	add	fp, r3, r2
	cmp	fp, r8
	bcs	.L439
	add	r1, r5, #16
	mov	r2, #4
	mov	r0, r4
	bl	PDATA_copy_var_into_pucp
	mov	r1, #4
	mov	r2, r1
	add	r3, sp, #52
	mov	r0, r4
	bl	PDATA_copy_bitfield_info_into_pucp
	add	r3, r5, #20
	str	r3, [sp, #4]
	mov	r3, #48
	add	sl, r5, #184
	mov	r2, #0
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r3, r9
	mov	r0, r4
	str	r2, [sp, #44]
	str	sl, [sp, #0]
	str	fp, [sp, #12]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r3, [sp, #32]
	ldr	r2, [sp, #36]
	add	r1, sp, #44
	add	fp, r3, r2
	add	r3, r5, #72
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	mov	r2, #1
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, r0, #12
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #76
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #44
	mov	r2, #2
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #80
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r2, #3
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #84
	str	r3, [sp, #4]
	mov	r2, #4
	add	r1, sp, #44
	str	sl, [sp, #0]
	str	r2, [sp, #8]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #88
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r2, #5
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #92
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #44
	mov	r2, #6
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #96
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r2, #7
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #100
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #44
	mov	r2, #8
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #104
	str	r3, [sp, #4]
	mov	r3, #12
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r2, #9
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #116
	str	r3, [sp, #4]
	mov	r2, #16
	str	r2, [sp, #8]
	add	r1, sp, #44
	mov	r2, #10
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #132
	str	r3, [sp, #4]
	mov	r3, #16
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r2, #11
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #188
	str	r3, [sp, #4]
	mov	r2, #28
	str	r2, [sp, #8]
	add	r1, sp, #44
	mov	r2, #12
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #216
	str	r3, [sp, #4]
	mov	r3, #28
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r2, #13
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #352
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #44
	mov	r2, #14
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #248
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r2, #15
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #252
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #44
	mov	r2, #16
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #256
	str	r3, [sp, #4]
	mov	r3, #96
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r2, #17
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #244
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r2, #18
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #356
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #44
	mov	r2, #19
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r9
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #368
	str	r3, [sp, #4]
	mov	r3, #40
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r2, #20
	mov	r3, r9
	str	sl, [sp, #0]
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	add	ip, ip, r0
	add	fp, ip, fp
	mov	r0, r4
	str	ip, [sp, #28]
	str	fp, [sp, #12]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	sl, ip, r0
	cmp	sl, #12
	bls	.L446
	b	.L448
.L439:
	mov	r3, #0
	str	r3, [r6, #0]
	b	.L442
.L448:
	ldr	r3, [sp, #56]
	ldr	r0, [sp, #52]
	add	r3, r3, #1
	add	r1, sp, #44
	mov	r2, #4
	str	r3, [sp, #56]
	bl	memcpy
	ldr	r3, [sp, #32]
	add	r3, r3, sl
	str	r3, [sp, #32]
	b	.L438
.L446:
	ldr	r3, [r4, #0]
	sub	r3, r3, #12
	str	r3, [r4, #0]
.L438:
	mov	r1, r5
	ldr	r0, .L449+12
	bl	nm_ListGetNext
	mov	r5, r0
.L437:
	cmp	r5, #0
	bne	.L443
.L442:
	ldr	r3, [sp, #56]
	cmp	r3, #0
	beq	.L444
	ldr	r0, [sp, #48]
	add	r1, sp, #56
	mov	r2, #4
	bl	memcpy
	b	.L436
.L444:
	ldr	r2, [r4, #0]
	sub	r2, r2, #4
	str	r2, [r4, #0]
.L445:
	str	r3, [sp, #32]
.L436:
	ldr	r3, .L449
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [sp, #32]
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L450:
	.align	2
.L449:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	949
	.word	.LANCHOR0
.LFE27:
	.size	SYSTEM_build_data_to_send, .-SYSTEM_build_data_to_send
	.section	.text.SYSTEM_clean_house_processing,"ax",%progbits
	.align	2
	.global	SYSTEM_clean_house_processing
	.type	SYSTEM_clean_house_processing, %function
SYSTEM_clean_house_processing:
.LFB74:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L455
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI101:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L455+4
	ldr	r3, .L455+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L455+12
	ldr	r1, .L455+4
	ldr	r2, .L455+16
	b	.L454
.L453:
	ldr	r1, .L455+4
	ldr	r2, .L455+20
	bl	mem_free_debug
	ldr	r0, .L455+12
	ldr	r1, .L455+4
	ldr	r2, .L455+24
.L454:
	bl	nm_ListRemoveHead_debug
	cmp	r0, #0
	bne	.L453
	ldr	r4, .L455
	str	r0, [sp, #0]
	str	r0, [sp, #4]
	ldr	r1, .L455+28
	ldr	r2, .L455+32
	mov	r3, #408
	ldr	r0, .L455+12
	bl	nm_GROUP_create_new_group
	mov	r3, #408
	str	r3, [sp, #0]
	ldr	r3, [r4, #0]
	ldr	r1, .L455+36
	str	r3, [sp, #4]
	mov	r3, #10
	str	r3, [sp, #8]
	mov	r2, #8
	ldr	r3, .L455+12
	mov	r0, #1
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	SYSTEM_PRESERVES_synchronize_preserves_to_file
.L456:
	.align	2
.L455:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	3203
	.word	.LANCHOR0
	.word	3207
	.word	3211
	.word	3213
	.word	.LANCHOR5
	.word	nm_SYSTEM_set_default_values
	.word	.LANCHOR3
.LFE74:
	.size	SYSTEM_clean_house_processing, .-SYSTEM_clean_house_processing
	.section	.text.IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token
	.type	IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token, %function
IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token:
.LFB75:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI102:
	ldr	r4, .L461
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L461+4
	ldr	r3, .L461+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L461+12
	bl	nm_ListGetFirst
	b	.L460
.L459:
	add	r0, r5, #152
	ldr	r1, [r4, #0]
	bl	SHARED_set_all_32_bit_change_bits
	ldr	r0, .L461+12
	mov	r1, r5
	bl	nm_ListGetNext
.L460:
	cmp	r0, #0
	mov	r5, r0
	bne	.L459
	ldr	r3, .L461
	ldr	r4, .L461+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L461+4
	mov	r3, #3264
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L461+20
	ldr	r0, [r4, #0]
	mov	r2, #1
	str	r2, [r3, #444]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L462:
	.align	2
.L461:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	3242
	.word	.LANCHOR0
	.word	comm_mngr_recursive_MUTEX
	.word	comm_mngr
.LFE75:
	.size	IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token, .-IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token
	.section	.text.SYSTEM_on_all_systems_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	SYSTEM_on_all_systems_set_or_clear_commserver_change_bits
	.type	SYSTEM_on_all_systems_set_or_clear_commserver_change_bits, %function
SYSTEM_on_all_systems_set_or_clear_commserver_change_bits:
.LFB76:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI103:
	ldr	r4, .L469
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L469+4
	ldr	r3, .L469+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L469+12
	bl	nm_ListGetFirst
	b	.L468
.L467:
	cmp	r5, #51
	ldr	r1, [r4, #0]
	add	r0, r6, #156
	bne	.L465
	bl	SHARED_clear_all_32_bit_change_bits
	b	.L466
.L465:
	bl	SHARED_set_all_32_bit_change_bits
.L466:
	ldr	r0, .L469+12
	mov	r1, r6
	bl	nm_ListGetNext
.L468:
	cmp	r0, #0
	mov	r6, r0
	bne	.L467
	ldr	r3, .L469
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L470:
	.align	2
.L469:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	3276
	.word	.LANCHOR0
.LFE76:
	.size	SYSTEM_on_all_systems_set_or_clear_commserver_change_bits, .-SYSTEM_on_all_systems_set_or_clear_commserver_change_bits
	.section	.text.nm_SYSTEM_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_update_pending_change_bits
	.type	nm_SYSTEM_update_pending_change_bits, %function
nm_SYSTEM_update_pending_change_bits:
.LFB77:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI104:
	ldr	r5, .L478
	mov	r1, #400
	ldr	r2, .L478+4
	ldr	r3, .L478+8
	mov	r7, r0
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L478+12
	bl	nm_ListGetFirst
	mov	r8, #0
	mov	r6, #1
	mov	r4, r0
	b	.L472
.L475:
	ldr	r3, [r4, #184]
	cmp	r3, #0
	beq	.L473
	cmp	r7, #0
	beq	.L474
	ldr	r2, [r4, #156]
	mov	r1, #2
	orr	r3, r2, r3
	str	r3, [r4, #156]
	ldr	r3, .L478+16
	ldr	r2, .L478+20
	str	r6, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L478+24
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L477
.L474:
	add	r0, r4, #184
	ldr	r1, [r5, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L477:
	mov	r8, #1
.L473:
	mov	r1, r4
	ldr	r0, .L478+12
	bl	nm_ListGetNext
	mov	r4, r0
.L472:
	cmp	r4, #0
	bne	.L475
	ldr	r3, .L478
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r8, #0
	beq	.L471
	mov	r0, #10
	mov	r1, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L471:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L479:
	.align	2
.L478:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	3313
	.word	.LANCHOR0
	.word	weather_preserves
	.word	60000
	.word	cics
.LFE77:
	.size	nm_SYSTEM_update_pending_change_bits, .-nm_SYSTEM_update_pending_change_bits
	.section	.text.SYSTEM_at_least_one_system_has_flow_checking,"ax",%progbits
	.align	2
	.global	SYSTEM_at_least_one_system_has_flow_checking
	.type	SYSTEM_at_least_one_system_has_flow_checking, %function
SYSTEM_at_least_one_system_has_flow_checking:
.LFB78:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L486
	stmfd	sp!, {r4, lr}
.LCFI105:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L486+4
	ldr	r3, .L486+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L486+12
	bl	nm_ListGetFirst
	b	.L485
.L483:
	ldr	r3, [r1, #100]
	cmp	r3, #0
	bne	.L484
	ldr	r0, .L486+12
	bl	nm_ListGetNext
.L485:
	cmp	r0, #0
	mov	r1, r0
	bne	.L483
	mov	r4, r0
	b	.L482
.L484:
	mov	r4, #1
.L482:
	ldr	r3, .L486
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L487:
	.align	2
.L486:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	3377
	.word	.LANCHOR0
.LFE78:
	.size	SYSTEM_at_least_one_system_has_flow_checking, .-SYSTEM_at_least_one_system_has_flow_checking
	.section	.text.SYSTEM_get_flow_checking_status,"ax",%progbits
	.align	2
	.global	SYSTEM_get_flow_checking_status
	.type	SYSTEM_get_flow_checking_status, %function
SYSTEM_get_flow_checking_status:
.LFB79:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L497
	stmfd	sp!, {r4, lr}
.LCFI106:
	ldr	r2, .L497+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L497+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L497+12
	ldr	r2, .L497+16
	mla	r4, r2, r4, r3
	ldrb	r3, [r4, #480]	@ zero_extendqisi2
	movs	r3, r3, lsr #7
	movne	r4, #0
	bne	.L489
	ldrb	r4, [r4, #481]	@ zero_extendqisi2
	tst	r4, #1
	movne	r4, #1
	bne	.L489
	tst	r4, #4
	movne	r4, #3
	bne	.L489
	tst	r4, #8
	movne	r4, #4
	bne	.L489
	tst	r4, #16
	movne	r4, #5
	bne	.L489
	tst	r4, #32
	movne	r4, #6
	bne	.L489
	mov	r4, r4, lsr #1
	ands	r4, r4, #1
	movne	r4, #2
.L489:
	ldr	r3, .L497
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L498:
	.align	2
.L497:
	.word	system_preserves_recursive_MUTEX
	.word	.LC22
	.word	3437
	.word	system_preserves
	.word	14224
.LFE79:
	.size	SYSTEM_get_flow_checking_status, .-SYSTEM_get_flow_checking_status
	.section	.text.SYSTEM_get_highest_flow_checking_status_for_any_system,"ax",%progbits
	.align	2
	.global	SYSTEM_get_highest_flow_checking_status_for_any_system
	.type	SYSTEM_get_highest_flow_checking_status_for_any_system, %function
SYSTEM_get_highest_flow_checking_status_for_any_system:
.LFB80:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI107:
	mov	r4, #0
	mov	r5, r4
.L500:
	mov	r0, r4
	bl	SYSTEM_get_flow_checking_status
	add	r4, r4, #1
	cmp	r5, r0
	movcc	r5, r0
	cmp	r4, #4
	bne	.L500
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.LFE80:
	.size	SYSTEM_get_highest_flow_checking_status_for_any_system, .-SYSTEM_get_highest_flow_checking_status_for_any_system
	.section	.text.SYSTEM_clear_mainline_break,"ax",%progbits
	.align	2
	.global	SYSTEM_clear_mainline_break
	.type	SYSTEM_clear_mainline_break, %function
SYSTEM_clear_mainline_break:
.LFB81:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L508
	stmfd	sp!, {r4, lr}
.LCFI108:
	ldr	r2, .L508+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L508+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	r4, r0, #0
	beq	.L503
	add	r0, r4, #516
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	cmp	r0, #0
	beq	.L504
	ldr	r2, .L508+12
	mov	r3, #0
.L506:
	ldrh	r1, [r2, #2]!
	cmp	r1, #0
	ldreq	r2, .L508+16
	addeq	r3, r2, r3, asl #1
	ldreq	r2, [r4, #0]
	streqh	r2, [r3, #192]	@ movhi
	beq	.L504
.L505:
	add	r3, r3, #1
	cmp	r3, #4
	bne	.L506
	b	.L504
.L503:
	ldr	r0, .L508+4
	bl	RemovePathFromFileName
	ldr	r2, .L508+20
	mov	r1, r0
	ldr	r0, .L508+24
	bl	Alert_Message_va
.L504:
	ldr	r3, .L508
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L509:
	.align	2
.L508:
	.word	system_preserves_recursive_MUTEX
	.word	.LC22
	.word	3543
	.word	irri_comm+190
	.word	irri_comm
	.word	3567
	.word	.LC37
.LFE81:
	.size	SYSTEM_clear_mainline_break, .-SYSTEM_clear_mainline_break
	.section	.text.SYSTEM_check_for_mvor_schedule_start,"ax",%progbits
	.align	2
	.global	SYSTEM_check_for_mvor_schedule_start
	.type	SYSTEM_check_for_mvor_schedule_start, %function
SYSTEM_check_for_mvor_schedule_start:
.LFB82:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L518
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI109:
	ldr	r2, .L518+4
	sub	sp, sp, #48
.LCFI110:
	mov	r1, #400
	mov	r5, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L518+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L518+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L518+4
	ldr	r3, .L518+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L518+20
	bl	nm_ListGetFirst
	ldr	r6, .L518+24
	mov	r4, r0
	b	.L511
.L516:
	ldrb	r3, [r5, #18]	@ zero_extendqisi2
	add	r3, r3, #47
	ldr	r2, [r4, r3, asl #2]
	ldr	r3, [r5, #0]
	cmp	r2, r3
	bne	.L512
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	mov	r0, sp
	bl	strlcpy
	ldrb	r2, [r5, #18]	@ zero_extendqisi2
	add	r3, r2, #54
	ldr	r3, [r4, r3, asl #2]
	cmp	r3, r6
	moveq	r0, sp
	moveq	r1, #17
	beq	.L517
	add	r2, r2, #47
	ldr	r2, [r4, r2, asl #2]
	cmp	r3, r2
	moveq	r0, sp
	moveq	r1, #18
	beq	.L517
	movls	r0, sp
	movls	r1, #19
	bls	.L517
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	ldrb	r3, [r5, #18]	@ zero_extendqisi2
	mov	r1, #0
	add	r2, r3, #54
	ldr	r2, [r4, r2, asl #2]
	add	r3, r3, #47
	ldr	r3, [r4, r3, asl #2]
	add	r2, r2, #15
	rsb	r2, r3, r2
	mov	r3, #4
	bl	FOAL_IRRI_initiate_or_cancel_a_master_valve_override
	b	.L512
.L517:
	bl	Alert_MVOR_skipped
.L512:
	mov	r1, r4
	ldr	r0, .L518+20
	bl	nm_ListGetNext
	mov	r4, r0
.L511:
	cmp	r4, #0
	bne	.L516
	ldr	r3, .L518+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L518
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, pc}
.L519:
	.align	2
.L518:
	.word	system_preserves_recursive_MUTEX
	.word	.LC22
	.word	3586
	.word	list_system_recursive_MUTEX
	.word	3588
	.word	.LANCHOR0
	.word	86400
.LFE82:
	.size	SYSTEM_check_for_mvor_schedule_start, .-SYSTEM_check_for_mvor_schedule_start
	.section	.text.SYSTEM_this_system_is_in_use,"ax",%progbits
	.align	2
	.global	SYSTEM_this_system_is_in_use
	.type	SYSTEM_this_system_is_in_use, %function
SYSTEM_this_system_is_in_use:
.LFB83:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI111:
	ldr	r4, .L521
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L521+4
	ldr	r3, .L521+8
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r5, #148
	str	r3, [sp, #0]
	ldr	r3, .L521+12
	add	r1, r5, #356
	str	r3, [sp, #4]
	mov	r2, #1
	ldr	r3, .L521+16
	mov	r0, r5
	bl	SHARED_get_bool_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L522:
	.align	2
.L521:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	3643
	.word	.LC1
	.word	nm_SYSTEM_set_system_in_use
.LFE83:
	.size	SYSTEM_this_system_is_in_use, .-SYSTEM_this_system_is_in_use
	.section	.text.SYSTEM_get_budget_in_use,"ax",%progbits
	.align	2
	.global	SYSTEM_get_budget_in_use
	.type	SYSTEM_get_budget_in_use, %function
SYSTEM_get_budget_in_use:
.LFB84:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI112:
	ldr	r4, .L524
	mov	r5, r0
	ldr	r3, .L524+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L524+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L524+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r2, #0
	add	r3, r0, #148
	str	r3, [sp, #0]
	ldr	r3, .L524+16
	add	r1, r0, #352
	str	r3, [sp, #4]
	ldr	r3, .L524+20
	bl	SHARED_get_bool_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L525:
	.align	2
.L524:
	.word	list_system_recursive_MUTEX
	.word	3688
	.word	.LC22
	.word	.LANCHOR0
	.word	.LC0
	.word	nm_SYSTEM_set_budget_in_use
.LFE84:
	.size	SYSTEM_get_budget_in_use, .-SYSTEM_get_budget_in_use
	.section	.text.SYSTEM_get_budget_details,"ax",%progbits
	.align	2
	.global	SYSTEM_get_budget_details
	.type	SYSTEM_get_budget_details, %function
SYSTEM_get_budget_details:
.LFB85:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L529
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI113:
	ldr	r2, .L529+4
	sub	sp, sp, #24
.LCFI114:
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L529+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, #0
	mov	r2, #112
	mov	r0, r5
	bl	memset
	ldr	r3, .L529+12
	add	r6, r4, #148
	str	r3, [sp, #4]
	add	r1, r4, #352
	mov	r2, #0
	ldr	r3, .L529+16
	mov	r0, r4
	str	r6, [sp, #0]
	bl	SHARED_get_bool_32_bit_change_bits_group
	ldr	r3, .L529+20
	add	r1, r4, #248
	str	r3, [sp, #0]
	ldr	r3, .L529+24
	mov	r2, #0
	stmib	sp, {r3, r6}
	ldr	r3, .L529+28
	mov	r8, r5
	str	r3, [sp, #12]
	ldr	r3, .L529+32
	mov	r7, #0
	str	r0, [r5, #0]
	mov	r0, r4
	bl	SHARED_get_uint32_32_bit_change_bits_group
	ldr	r2, .L529+36
	mov	r3, #12
	stmib	sp, {r2, r6}
	ldr	r2, .L529+40
	add	r1, r4, #252
	str	r2, [sp, #12]
	mov	r2, #6
	str	r3, [sp, #0]
	str	r0, [r5, #4]
	mov	r0, r4
	bl	SHARED_get_uint32_32_bit_change_bits_group
	str	r0, [r5, #8]
.L527:
	mov	r0, #1
	mov	r1, r0
	ldr	r2, .L529+44
	bl	DMYToDate
	mov	r1, #12
	ldr	r2, .L529+48
	add	fp, r7, #64
	add	fp, r4, fp, asl #2
	mov	sl, r0
	mov	r0, #31
	bl	DMYToDate
	ldr	r2, .L529+44
	mov	sl, sl, asl #16
	mov	sl, sl, lsr #16
	mov	r9, r0
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	ldr	r3, .L529+52
	mov	r9, r9, asl #16
	str	r3, [sp, #12]
	ldr	r3, .L529+56
	mov	r1, r7
	str	r3, [sp, #20]
	mov	r9, r9, lsr #16
	mov	r2, fp
	mov	r3, #24
	str	sl, [sp, #0]
	str	r9, [sp, #4]
	str	r6, [sp, #16]
	add	r7, r7, #1
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	str	r0, [sp, #8]
	mov	r0, r4
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	cmp	r7, #24
	str	r0, [r8, #12]
	add	r8, r8, #4
	bne	.L527
	ldr	r3, .L529+60
	mov	r2, #0
	stmib	sp, {r3, r6}
	ldr	r3, .L529+64
	mov	r0, r4
	str	r3, [sp, #12]
	add	r1, r4, #244
	mov	r3, #1
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	ldr	r3, .L529
	str	r0, [r5, #108]
	ldr	r0, [r3, #0]
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	xQueueGiveMutexRecursive
.L530:
	.align	2
.L529:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	3714
	.word	.LC0
	.word	nm_SYSTEM_set_budget_in_use
	.word	43200
	.word	nm_SYSTEM_set_budget_meter_read_time
	.word	.LC6
	.word	86399
	.word	nm_SYSTEM_set_budget_number_of_annual_periods
	.word	.LC5
	.word	2011
	.word	2042
	.word	nm_SYSTEM_set_budget_period
	.word	.LC19
	.word	nm_SYSTEM_set_budget_mode
	.word	.LC4
.LFE85:
	.size	SYSTEM_get_budget_details, .-SYSTEM_get_budget_details
	.section	.text.SYSTEM_get_budget_period_index,"ax",%progbits
	.align	2
	.global	SYSTEM_get_budget_period_index
	.type	SYSTEM_get_budget_period_index, %function
SYSTEM_get_budget_period_index:
.LFB86:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI115:
	sub	sp, sp, #24
.LCFI116:
	stmia	sp, {r1, r2}
	ldr	r3, [r0, #4]
	mov	r6, sp
	str	r3, [sp, #8]
	str	r3, [sp, #16]
	mov	r4, r0
	mov	r5, #0
	b	.L534
.L536:
.LBB57:
	mov	r5, r7
.L534:
	ldr	r3, [r4, #12]
	mov	r0, sp
	strh	r3, [sp, #12]	@ movhi
	ldr	r3, [r4, #16]
	add	r1, sp, #8
	strh	r3, [sp, #20]	@ movhi
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	add	r7, r5, #1
	cmp	r0, #0
	beq	.L532
	add	r0, sp, #16
	mov	r1, sp
	bl	DT1_IsBiggerThan_DT2
	cmp	r0, #0
	bne	.L535
.L532:
	cmp	r7, #23
	add	r4, r4, #4
	bne	.L536
	mvn	r0, #0
	b	.L533
.L535:
	mov	r0, r5
.L533:
.LBE57:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE86:
	.size	SYSTEM_get_budget_period_index, .-SYSTEM_get_budget_period_index
	.section	.text.SYSTEM_at_least_one_system_has_budget_enabled,"ax",%progbits
	.align	2
	.global	SYSTEM_at_least_one_system_has_budget_enabled
	.type	SYSTEM_at_least_one_system_has_budget_enabled, %function
SYSTEM_at_least_one_system_has_budget_enabled:
.LFB87:
	@ args = 0, pretend = 0, frame = 120
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI117:
	sub	sp, sp, #120
.LCFI118:
	add	r0, sp, #112
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, .L543
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L543+4
	ldr	r3, .L543+8
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
.LBB58:
	mov	r4, #0
.LBE58:
	mov	r5, r0
.LBB59:
	b	.L538
.L541:
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	subs	r6, r0, #0
	beq	.L539
	bl	nm_GROUP_get_group_ID
	cmp	r0, #0
	beq	.L539
	mov	r0, r6
	mov	r1, sp
	bl	SYSTEM_get_budget_details
	ldr	r3, [sp, #0]
	cmp	r3, #0
	beq	.L539
	add	r3, sp, #120
	mov	r0, sp
	ldmdb	r3, {r1, r2}
	bl	SYSTEM_get_budget_period_index
	cmp	r0, #0
	beq	.L542
.L539:
	add	r4, r4, #1
.L538:
	cmp	r4, r5
	bne	.L541
	mov	r4, #0
	b	.L540
.L542:
	mov	r4, #1
.L540:
.LBE59:
	ldr	r3, .L543
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #120
	ldmfd	sp!, {r4, r5, r6, pc}
.L544:
	.align	2
.L543:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	3815
.LFE87:
	.size	SYSTEM_at_least_one_system_has_budget_enabled, .-SYSTEM_at_least_one_system_has_budget_enabled
	.section	.text.nm_SYSTEM_get_budget,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_get_budget
	.type	nm_SYSTEM_get_budget, %function
nm_SYSTEM_get_budget:
.LFB88:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #23
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI119:
	mov	r6, r1
	bls	.L546
	ldr	r0, .L551
	bl	Alert_Message_va
	mov	r5, #0
	b	.L547
.L546:
	bl	nm_GROUP_get_group_ID
.LBB60:
	ldr	r7, .L551+4
.LBE60:
	mov	r4, #0
	mov	r5, r4
.LBB61:
	mov	r8, #472
.LBE61:
	mov	sl, r0
.L549:
.LBB62:
	mla	r9, r8, r4, r7
	ldr	r3, [r9, #24]
	add	r9, r9, #20
	cmp	r3, #0
	beq	.L548
	ldr	r3, [r9, #24]
	ldr	r3, [r3, #0]
	cmp	r3, sl
	bne	.L548
	mov	r0, r4
	bl	POC_use_for_budget
	cmp	r0, #0
	beq	.L548
	ldr	r0, [r9, #4]
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	mov	r1, r6
	bl	POC_get_budget
	add	r5, r5, r0
.L548:
.LBE62:
	add	r4, r4, #1
	cmp	r4, #12
	bne	.L549
.L547:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L552:
	.align	2
.L551:
	.word	.LC38
	.word	poc_preserves
.LFE88:
	.size	nm_SYSTEM_get_budget, .-nm_SYSTEM_get_budget
	.section	.text.nm_SYSTEM_get_used,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_get_used
	.type	nm_SYSTEM_get_used, %function
nm_SYSTEM_get_used:
.LFB89:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI120:
	bl	nm_GROUP_get_group_ID
	ldr	r5, .L557
	mov	r4, #0
	mov	r6, r4
	mov	r7, r0
.L555:
.LBB63:
	ldr	r3, [r5, #24]
	cmp	r3, #0
	beq	.L554
	ldr	r3, [r5, #44]
	ldr	r3, [r3, #0]
	cmp	r3, r7
	bne	.L554
	mov	r0, r4
	bl	POC_use_for_budget
	cmp	r0, #0
	beq	.L554
	mov	r0, r4
	bl	nm_BUDGET_get_used
	fmsr	s12, r6	@ int
	fuitod	d7, s12
	fmdrr	d6, r0, r1
	faddd	d7, d7, d6
	ftouizd	s13, d7
	fmrs	r6, s13	@ int
.L554:
	add	r4, r4, #1
	cmp	r4, #12
	add	r5, r5, #472
	bne	.L555
.LBE63:
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L558:
	.align	2
.L557:
	.word	poc_preserves
.LFE89:
	.size	nm_SYSTEM_get_used, .-nm_SYSTEM_get_used
	.section	.text.nm_SYSTEM_get_budget_flow_type,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_get_budget_flow_type
	.type	nm_SYSTEM_get_budget_flow_type, %function
nm_SYSTEM_get_budget_flow_type:
.LFB90:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #9
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI121:
	mov	r2, r1
	mov	r4, #0
	bhi	.L560
	ldr	ip, .L562
	add	r1, r1, #92
	str	ip, [sp, #4]
	add	ip, r0, #148
	str	ip, [sp, #8]
	ldr	ip, .L562+4
	add	r1, r0, r1, asl #2
	mov	r3, #10
	str	r4, [sp, #0]
	str	ip, [sp, #12]
	bl	SHARED_get_bool_from_array_32_bit_change_bits_group
	b	.L561
.L560:
	ldr	r0, .L562+8
	bl	Alert_Message_va
	mov	r0, r4
.L561:
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L563:
	.align	2
.L562:
	.word	nm_SYSTEM_set_budget_flow_type
	.word	.LC14
	.word	.LC39
.LFE90:
	.size	nm_SYSTEM_get_budget_flow_type, .-nm_SYSTEM_get_budget_flow_type
	.section	.text.nm_SYSTEM_get_Vp,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_get_Vp
	.type	nm_SYSTEM_get_Vp, %function
nm_SYSTEM_get_Vp:
.LFB91:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, [r0, #360]	@ float
	bx	lr
.LFE91:
	.size	nm_SYSTEM_get_Vp, .-nm_SYSTEM_get_Vp
	.section	.text.nm_SYSTEM_set_Vp,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_set_Vp
	.type	nm_SYSTEM_set_Vp, %function
nm_SYSTEM_set_Vp:
.LFB92:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r1, [r0, #360]	@ float
	bx	lr
.LFE92:
	.size	nm_SYSTEM_set_Vp, .-nm_SYSTEM_set_Vp
	.section	.text.nm_SYSTEM_get_poc_ratio,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_get_poc_ratio
	.type	nm_SYSTEM_get_poc_ratio, %function
nm_SYSTEM_get_poc_ratio:
.LFB93:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, [r0, #364]	@ float
	bx	lr
.LFE93:
	.size	nm_SYSTEM_get_poc_ratio, .-nm_SYSTEM_get_poc_ratio
	.section	.text.nm_SYSTEM_set_poc_ratio,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_set_poc_ratio
	.type	nm_SYSTEM_set_poc_ratio, %function
nm_SYSTEM_set_poc_ratio:
.LFB94:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r1, [r0, #364]	@ float
	bx	lr
.LFE94:
	.size	nm_SYSTEM_set_poc_ratio, .-nm_SYSTEM_set_poc_ratio
	.section	.text.IRRIGATION_SYSTEM_load_ftimes_list,"ax",%progbits
	.align	2
	.global	IRRIGATION_SYSTEM_load_ftimes_list
	.type	IRRIGATION_SYSTEM_load_ftimes_list, %function
IRRIGATION_SYSTEM_load_ftimes_list:
.LFB95:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L573
	stmfd	sp!, {r0, r4, lr}
.LCFI122:
	ldr	r2, .L573+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L573+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L573+12
	bl	nm_ListGetFirst
	b	.L572
.L571:
	ldr	r3, [r4, #356]
	cmp	r3, #0
	beq	.L570
	ldr	r3, [r4, #72]
	cmp	r3, #0
	beq	.L570
	mov	r0, #144
	mov	r1, sp
	ldr	r2, .L573+4
	ldr	r3, .L573+16
	bl	mem_oabia
	cmp	r0, #0
	beq	.L570
	mov	r1, #0
	ldr	r0, [sp, #0]
	mov	r2, #144
	bl	memset
	ldr	r3, [r4, #16]
	ldr	r1, [sp, #0]
	ldr	r0, .L573+20
	str	r3, [r1, #12]
	ldr	r3, [r4, #76]
	str	r3, [r1, #16]
	ldr	r3, [r4, #80]
	str	r3, [r1, #20]
	ldr	r3, [r4, #84]
	str	r3, [r1, #24]
	bl	nm_ListInsertTail
.L570:
	ldr	r0, .L573+12
	mov	r1, r4
	bl	nm_ListGetNext
.L572:
	cmp	r0, #0
	mov	r4, r0
	bne	.L571
	ldr	r3, .L573
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r3, r4, pc}
.L574:
	.align	2
.L573:
	.word	list_system_recursive_MUTEX
	.word	.LC22
	.word	3996
	.word	.LANCHOR0
	.word	4006
	.word	ft_system_groups_list_hdr
.LFE95:
	.size	IRRIGATION_SYSTEM_load_ftimes_list, .-IRRIGATION_SYSTEM_load_ftimes_list
	.section	.text.IRRIGATION_SYSTEM_calculate_chain_sync_crc,"ax",%progbits
	.align	2
	.global	IRRIGATION_SYSTEM_calculate_chain_sync_crc
	.type	IRRIGATION_SYSTEM_calculate_chain_sync_crc, %function
IRRIGATION_SYSTEM_calculate_chain_sync_crc:
.LFB96:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L580
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI123:
	ldr	r5, .L580+4
	mov	r1, #400
	ldr	r2, .L580+8
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L580+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #8]
	mov	r0, #408
	mul	r0, r3, r0
	mov	r1, sp
	ldr	r2, .L580+8
	ldr	r3, .L580+16
	bl	mem_oabia
	subs	r6, r0, #0
	beq	.L576
	ldr	r3, [sp, #0]
	add	r6, sp, #8
	mov	r0, r5
	str	r3, [r6, #-4]!
	bl	nm_ListGetFirst
	mov	r7, #0
	mov	r5, r0
	b	.L577
.L578:
	add	r8, r5, #20
	mov	r0, r8
	bl	strlen
	mov	r1, r8
	mov	r2, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #72
	mov	r2, #4
	mov	r8, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #76
	mov	r2, #4
	add	r0, r8, r0
	add	r7, r0, r7
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #80
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #84
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #88
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #92
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #96
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #100
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #104
	mov	r2, #12
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #116
	mov	r2, #16
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #132
	mov	r2, #16
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #188
	mov	r2, #28
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #216
	mov	r2, #28
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #352
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #248
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #252
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #256
	mov	r2, #96
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #244
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #356
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #368
	mov	r2, #40
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r1, r5
	add	r7, r7, r0
	ldr	r0, .L580+4
	bl	nm_ListGetNext
	mov	r5, r0
.L577:
	cmp	r5, #0
	bne	.L578
	mov	r1, r7
	ldr	r0, [sp, #0]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L580+20
	add	r4, r4, #43
	ldr	r1, .L580+8
	ldr	r2, .L580+24
	mov	r6, #1
	str	r0, [r3, r4, asl #2]
	ldr	r0, [sp, #0]
	bl	mem_free_debug
	b	.L579
.L576:
	ldr	r3, .L580+28
	ldr	r0, .L580+32
	ldr	r1, [r3, r4, asl #4]
	bl	Alert_Message_va
.L579:
	ldr	r3, .L580
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L581:
	.align	2
.L580:
	.word	list_system_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC22
	.word	4059
	.word	4071
	.word	cscs
	.word	4179
	.word	chain_sync_file_pertinants
	.word	.LC40
.LFE96:
	.size	IRRIGATION_SYSTEM_calculate_chain_sync_crc, .-IRRIGATION_SYSTEM_calculate_chain_sync_crc
	.global	irrigation_system_list_item_sizes
	.global	IRRIGATION_SYSTEM_DEFAULT_NAME
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_DEFAULT_NAME,"a",%progbits
	.set	.LANCHOR5,. + 0
	.type	IRRIGATION_SYSTEM_DEFAULT_NAME, %object
	.size	IRRIGATION_SYSTEM_DEFAULT_NAME, 9
IRRIGATION_SYSTEM_DEFAULT_NAME:
	.ascii	"Mainline\000"
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"BudgetInUse\000"
.LC1:
	.ascii	"InUse\000"
.LC2:
	.ascii	"UsedForIrrig\000"
.LC3:
	.ascii	"CapacityInUse\000"
.LC4:
	.ascii	"BudgetMode\000"
.LC5:
	.ascii	"BudgetAnnualPeriods\000"
.LC6:
	.ascii	"BudgetMeterReadTime\000"
.LC7:
	.ascii	"CapacityWithPump\000"
.LC8:
	.ascii	"CapacityWithoutPump\000"
.LC9:
	.ascii	"MLBDuringIrrig\000"
.LC10:
	.ascii	"MLBDuringMVOR\000"
.LC11:
	.ascii	"MLBAllOtherTimes\000"
.LC12:
	.ascii	"FlowCheckingInUse\000"
.LC13:
	.ascii	"%s%s\000"
.LC14:
	.ascii	"BudgetFlowType\000"
.LC15:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/shared_irrigation_system.c\000"
.LC16:
	.ascii	"%s%d\000"
.LC17:
	.ascii	"MVORScheduleOpen\000"
.LC18:
	.ascii	"MVORScheduleClose\000"
.LC19:
	.ascii	"BudgetMeterReadDate\000"
.LC20:
	.ascii	"IRRI SYS file unexpd update %u\000"
.LC21:
	.ascii	"IRRI SYS file update : to revision %u from %u\000"
.LC22:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/irrigation_system.c\000"
.LC23:
	.ascii	"IRRI SYS updater error\000"
.LC24:
	.ascii	"FlowCheckingRange\000"
.LC25:
	.ascii	"FlowTolerancePlus\000"
.LC26:
	.ascii	"FlowToleranceMinus\000"
.LC27:
	.ascii	"Mainlines list unexpected order!\000"
.LC28:
	.ascii	"More than 4 mainlines IN USE!\000"
.LC29:
	.ascii	"%s\000"
.LC30:
	.ascii	" <%s %s>\000"
.LC31:
	.ascii	"DerateNumberOfStation\000"
.LC32:
	.ascii	"DerateNumberOfCell\000"
.LC33:
	.ascii	"DerateAllowedToLock\000"
.LC34:
	.ascii	"DerateSlotSize\000"
.LC35:
	.ascii	"DerateMaxStationOn\000"
.LC36:
	.ascii	"DerateNumberOfSlots\000"
.LC37:
	.ascii	"System not found : %s, %u\000"
.LC38:
	.ascii	"Invalid budget index: %d\000"
.LC39:
	.ascii	"Invalid flow type index: %d\000"
.LC40:
	.ascii	"SYNC: no mem to calc checksum %s\000"
	.section	.rodata.IRRIGATION_SYSTEM_FILENAME,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	IRRIGATION_SYSTEM_FILENAME, %object
	.size	IRRIGATION_SYSTEM_FILENAME, 18
IRRIGATION_SYSTEM_FILENAME:
	.ascii	"IRRIGATION_SYSTEM\000"
	.section	.bss.system_group_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	system_group_list_hdr, %object
	.size	system_group_list_hdr, 20
system_group_list_hdr:
	.space	20
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.section	.rodata.irrigation_system_list_item_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	irrigation_system_list_item_sizes, %object
	.size	irrigation_system_list_item_sizes, 36
irrigation_system_list_item_sizes:
	.word	184
	.word	184
	.word	188
	.word	244
	.word	356
	.word	356
	.word	360
	.word	360
	.word	408
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI0-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI2-.LFB19
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI4-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI6-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI8-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI10-.LFB16
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI12-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI14-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI16-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI18-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI20-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI22-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI24-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI26-.LFB18
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI28-.LFB11
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI29-.LCFI28
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI30-.LFB12
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI32-.LFB17
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xe
	.uleb128 0x70
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI34-.LFB22
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI35-.LCFI34
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB101
	.4byte	.LFE101-.LFB101
	.byte	0x4
	.4byte	.LCFI36-.LFB101
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB102
	.4byte	.LFE102-.LFB102
	.byte	0x4
	.4byte	.LCFI38-.LFB102
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI39-.LCFI38
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB103
	.4byte	.LFE103-.LFB103
	.byte	0x4
	.4byte	.LCFI40-.LFB103
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI41-.LCFI40
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI42-.LFB25
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI44-.LFB23
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI45-.LFB24
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI46-.LFB26
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.byte	0x4
	.4byte	.LCFI47-.LFB50
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.byte	0x4
	.4byte	.LCFI48-.LFB51
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.byte	0x4
	.4byte	.LCFI49-.LFB48
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI50-.LFB36
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI51-.LFB35
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI52-.LFB34
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI53-.LFB33
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI54-.LFB31
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI55-.LFB32
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI56-.LFB29
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI57-.LFB30
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI58-.LFB28
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.byte	0x4
	.4byte	.LCFI59-.LFB52
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.byte	0x4
	.4byte	.LCFI60-.LFB53
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.byte	0x4
	.4byte	.LCFI61-.LFB54
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.byte	0x4
	.4byte	.LCFI62-.LFB55
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.byte	0x4
	.4byte	.LCFI63-.LFB47
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.byte	0x4
	.4byte	.LCFI64-.LFB56
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE86:
.LSFDE88:
	.4byte	.LEFDE88-.LASFDE88
.LASFDE88:
	.4byte	.Lframe0
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.byte	0x4
	.4byte	.LCFI65-.LFB57
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE88:
.LSFDE90:
	.4byte	.LEFDE90-.LASFDE90
.LASFDE90:
	.4byte	.Lframe0
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.byte	0x4
	.4byte	.LCFI66-.LFB58
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE90:
.LSFDE92:
	.4byte	.LEFDE92-.LASFDE92
.LASFDE92:
	.4byte	.Lframe0
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.byte	0x4
	.4byte	.LCFI67-.LFB59
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE92:
.LSFDE94:
	.4byte	.LEFDE94-.LASFDE94
.LASFDE94:
	.4byte	.Lframe0
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.byte	0x4
	.4byte	.LCFI68-.LFB60
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE94:
.LSFDE96:
	.4byte	.LEFDE96-.LASFDE96
.LASFDE96:
	.4byte	.Lframe0
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.byte	0x4
	.4byte	.LCFI69-.LFB61
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE96:
.LSFDE98:
	.4byte	.LEFDE98-.LASFDE98
.LASFDE98:
	.4byte	.Lframe0
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.byte	0x4
	.4byte	.LCFI70-.LFB62
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE98:
.LSFDE100:
	.4byte	.LEFDE100-.LASFDE100
.LASFDE100:
	.4byte	.Lframe0
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.byte	0x4
	.4byte	.LCFI71-.LFB63
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE100:
.LSFDE102:
	.4byte	.LEFDE102-.LASFDE102
.LASFDE102:
	.4byte	.Lframe0
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.byte	0x4
	.4byte	.LCFI72-.LFB64
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI73-.LCFI72
	.byte	0xe
	.uleb128 0x68
	.align	2
.LEFDE102:
.LSFDE104:
	.4byte	.LEFDE104-.LASFDE104
.LASFDE104:
	.4byte	.Lframe0
	.4byte	.LFB65
	.4byte	.LFE65-.LFB65
	.byte	0x4
	.4byte	.LCFI74-.LFB65
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI75-.LCFI74
	.byte	0xe
	.uleb128 0x68
	.align	2
.LEFDE104:
.LSFDE106:
	.4byte	.LEFDE106-.LASFDE106
.LASFDE106:
	.4byte	.Lframe0
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.byte	0x4
	.4byte	.LCFI76-.LFB66
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI77-.LCFI76
	.byte	0xe
	.uleb128 0x68
	.align	2
.LEFDE106:
.LSFDE108:
	.4byte	.LEFDE108-.LASFDE108
.LASFDE108:
	.4byte	.Lframe0
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.byte	0x4
	.4byte	.LCFI78-.LFB67
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE108:
.LSFDE110:
	.4byte	.LEFDE110-.LASFDE110
.LASFDE110:
	.4byte	.Lframe0
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.byte	0x4
	.4byte	.LCFI79-.LFB68
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE110:
.LSFDE112:
	.4byte	.LEFDE112-.LASFDE112
.LASFDE112:
	.4byte	.Lframe0
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.byte	0x4
	.4byte	.LCFI80-.LFB69
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE112:
.LSFDE114:
	.4byte	.LEFDE114-.LASFDE114
.LASFDE114:
	.4byte	.Lframe0
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.byte	0x4
	.4byte	.LCFI81-.LFB70
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE114:
.LSFDE116:
	.4byte	.LEFDE116-.LASFDE116
.LASFDE116:
	.4byte	.Lframe0
	.4byte	.LFB71
	.4byte	.LFE71-.LFB71
	.byte	0x4
	.4byte	.LCFI82-.LFB71
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE116:
.LSFDE118:
	.4byte	.LEFDE118-.LASFDE118
.LASFDE118:
	.4byte	.Lframe0
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.byte	0x4
	.4byte	.LCFI83-.LFB72
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE118:
.LSFDE120:
	.4byte	.LEFDE120-.LASFDE120
.LASFDE120:
	.4byte	.Lframe0
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.align	2
.LEFDE120:
.LSFDE122:
	.4byte	.LEFDE122-.LASFDE122
.LASFDE122:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI84-.LFB20
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI85-.LCFI84
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE122:
.LSFDE124:
	.4byte	.LEFDE124-.LASFDE124
.LASFDE124:
	.4byte	.Lframe0
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.byte	0x4
	.4byte	.LCFI86-.LFB46
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE124:
.LSFDE126:
	.4byte	.LEFDE126-.LASFDE126
.LASFDE126:
	.4byte	.Lframe0
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.byte	0x4
	.4byte	.LCFI87-.LFB45
	.byte	0xe
	.uleb128 0x34
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x83
	.uleb128 0xa
	.byte	0x82
	.uleb128 0xb
	.byte	0x81
	.uleb128 0xc
	.byte	0x80
	.uleb128 0xd
	.align	2
.LEFDE126:
.LSFDE128:
	.4byte	.LEFDE128-.LASFDE128
.LASFDE128:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.byte	0x4
	.4byte	.LCFI88-.LFB44
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE128:
.LSFDE130:
	.4byte	.LEFDE130-.LASFDE130
.LASFDE130:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI89-.LFB43
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE130:
.LSFDE132:
	.4byte	.LEFDE132-.LASFDE132
.LASFDE132:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI90-.LFB41
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE132:
.LSFDE134:
	.4byte	.LEFDE134-.LASFDE134
.LASFDE134:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI91-.LFB42
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE134:
.LSFDE136:
	.4byte	.LEFDE136-.LASFDE136
.LASFDE136:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI92-.LFB39
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE136:
.LSFDE138:
	.4byte	.LEFDE138-.LASFDE138
.LASFDE138:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI93-.LFB40
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE138:
.LSFDE140:
	.4byte	.LEFDE140-.LASFDE140
.LASFDE140:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI94-.LFB38
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE140:
.LSFDE142:
	.4byte	.LEFDE142-.LASFDE142
.LASFDE142:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI95-.LFB21
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI96-.LCFI95
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE142:
.LSFDE144:
	.4byte	.LEFDE144-.LASFDE144
.LASFDE144:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI97-.LFB37
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI98-.LCFI97
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE144:
.LSFDE146:
	.4byte	.LEFDE146-.LASFDE146
.LASFDE146:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI99-.LFB27
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI100-.LCFI99
	.byte	0xe
	.uleb128 0x60
	.align	2
.LEFDE146:
.LSFDE148:
	.4byte	.LEFDE148-.LASFDE148
.LASFDE148:
	.4byte	.Lframe0
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.byte	0x4
	.4byte	.LCFI101-.LFB74
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE148:
.LSFDE150:
	.4byte	.LEFDE150-.LASFDE150
.LASFDE150:
	.4byte	.Lframe0
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.byte	0x4
	.4byte	.LCFI102-.LFB75
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE150:
.LSFDE152:
	.4byte	.LEFDE152-.LASFDE152
.LASFDE152:
	.4byte	.Lframe0
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.byte	0x4
	.4byte	.LCFI103-.LFB76
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE152:
.LSFDE154:
	.4byte	.LEFDE154-.LASFDE154
.LASFDE154:
	.4byte	.Lframe0
	.4byte	.LFB77
	.4byte	.LFE77-.LFB77
	.byte	0x4
	.4byte	.LCFI104-.LFB77
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE154:
.LSFDE156:
	.4byte	.LEFDE156-.LASFDE156
.LASFDE156:
	.4byte	.Lframe0
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.byte	0x4
	.4byte	.LCFI105-.LFB78
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE156:
.LSFDE158:
	.4byte	.LEFDE158-.LASFDE158
.LASFDE158:
	.4byte	.Lframe0
	.4byte	.LFB79
	.4byte	.LFE79-.LFB79
	.byte	0x4
	.4byte	.LCFI106-.LFB79
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE158:
.LSFDE160:
	.4byte	.LEFDE160-.LASFDE160
.LASFDE160:
	.4byte	.Lframe0
	.4byte	.LFB80
	.4byte	.LFE80-.LFB80
	.byte	0x4
	.4byte	.LCFI107-.LFB80
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE160:
.LSFDE162:
	.4byte	.LEFDE162-.LASFDE162
.LASFDE162:
	.4byte	.Lframe0
	.4byte	.LFB81
	.4byte	.LFE81-.LFB81
	.byte	0x4
	.4byte	.LCFI108-.LFB81
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE162:
.LSFDE164:
	.4byte	.LEFDE164-.LASFDE164
.LASFDE164:
	.4byte	.Lframe0
	.4byte	.LFB82
	.4byte	.LFE82-.LFB82
	.byte	0x4
	.4byte	.LCFI109-.LFB82
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI110-.LCFI109
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE164:
.LSFDE166:
	.4byte	.LEFDE166-.LASFDE166
.LASFDE166:
	.4byte	.Lframe0
	.4byte	.LFB83
	.4byte	.LFE83-.LFB83
	.byte	0x4
	.4byte	.LCFI111-.LFB83
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE166:
.LSFDE168:
	.4byte	.LEFDE168-.LASFDE168
.LASFDE168:
	.4byte	.Lframe0
	.4byte	.LFB84
	.4byte	.LFE84-.LFB84
	.byte	0x4
	.4byte	.LCFI112-.LFB84
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE168:
.LSFDE170:
	.4byte	.LEFDE170-.LASFDE170
.LASFDE170:
	.4byte	.Lframe0
	.4byte	.LFB85
	.4byte	.LFE85-.LFB85
	.byte	0x4
	.4byte	.LCFI113-.LFB85
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI114-.LCFI113
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE170:
.LSFDE172:
	.4byte	.LEFDE172-.LASFDE172
.LASFDE172:
	.4byte	.Lframe0
	.4byte	.LFB86
	.4byte	.LFE86-.LFB86
	.byte	0x4
	.4byte	.LCFI115-.LFB86
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI116-.LCFI115
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE172:
.LSFDE174:
	.4byte	.LEFDE174-.LASFDE174
.LASFDE174:
	.4byte	.Lframe0
	.4byte	.LFB87
	.4byte	.LFE87-.LFB87
	.byte	0x4
	.4byte	.LCFI117-.LFB87
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI118-.LCFI117
	.byte	0xe
	.uleb128 0x88
	.align	2
.LEFDE174:
.LSFDE176:
	.4byte	.LEFDE176-.LASFDE176
.LASFDE176:
	.4byte	.Lframe0
	.4byte	.LFB88
	.4byte	.LFE88-.LFB88
	.byte	0x4
	.4byte	.LCFI119-.LFB88
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.align	2
.LEFDE176:
.LSFDE178:
	.4byte	.LEFDE178-.LASFDE178
.LASFDE178:
	.4byte	.Lframe0
	.4byte	.LFB89
	.4byte	.LFE89-.LFB89
	.byte	0x4
	.4byte	.LCFI120-.LFB89
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE178:
.LSFDE180:
	.4byte	.LEFDE180-.LASFDE180
.LASFDE180:
	.4byte	.Lframe0
	.4byte	.LFB90
	.4byte	.LFE90-.LFB90
	.byte	0x4
	.4byte	.LCFI121-.LFB90
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE180:
.LSFDE182:
	.4byte	.LEFDE182-.LASFDE182
.LASFDE182:
	.4byte	.Lframe0
	.4byte	.LFB91
	.4byte	.LFE91-.LFB91
	.align	2
.LEFDE182:
.LSFDE184:
	.4byte	.LEFDE184-.LASFDE184
.LASFDE184:
	.4byte	.Lframe0
	.4byte	.LFB92
	.4byte	.LFE92-.LFB92
	.align	2
.LEFDE184:
.LSFDE186:
	.4byte	.LEFDE186-.LASFDE186
.LASFDE186:
	.4byte	.Lframe0
	.4byte	.LFB93
	.4byte	.LFE93-.LFB93
	.align	2
.LEFDE186:
.LSFDE188:
	.4byte	.LEFDE188-.LASFDE188
.LASFDE188:
	.4byte	.Lframe0
	.4byte	.LFB94
	.4byte	.LFE94-.LFB94
	.align	2
.LEFDE188:
.LSFDE190:
	.4byte	.LEFDE190-.LASFDE190
.LASFDE190:
	.4byte	.Lframe0
	.4byte	.LFB95
	.4byte	.LFE95-.LFB95
	.byte	0x4
	.4byte	.LCFI122-.LFB95
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE190:
.LSFDE192:
	.4byte	.LEFDE192-.LASFDE192
.LASFDE192:
	.4byte	.Lframe0
	.4byte	.LFB96
	.4byte	.LFE96-.LFB96
	.byte	0x4
	.4byte	.LCFI123-.LFB96
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE192:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/shared_irrigation_system.c"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/irrigation_system.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x81d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF96
	.byte	0x1
	.4byte	.LASF97
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x5d9
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x494
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x4cc
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF98
	.byte	0x1
	.2byte	0x59c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x38d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x3f2
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x453
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x504
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x617
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x13f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x186
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x528
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x574
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST5
	.uleb128 0x4
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x54e
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST6
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x1cd
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST7
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x216
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST8
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x25f
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST9
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x2a8
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST10
	.uleb128 0x4
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x2f1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST11
	.uleb128 0x4
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x33a
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST12
	.uleb128 0x5
	.4byte	0x21
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST13
	.uleb128 0x5
	.4byte	0x2a
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST14
	.uleb128 0x5
	.4byte	0x33
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST15
	.uleb128 0x5
	.4byte	0x3c
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST16
	.uleb128 0x4
	.4byte	.LASF19
	.byte	0x2
	.2byte	0x168
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST17
	.uleb128 0x5
	.4byte	0x46
	.4byte	.LFB101
	.4byte	.LFE101
	.4byte	.LLST18
	.uleb128 0x5
	.4byte	0x4f
	.4byte	.LFB102
	.4byte	.LFE102
	.4byte	.LLST19
	.uleb128 0x5
	.4byte	0x58
	.4byte	.LFB103
	.4byte	.LFE103
	.4byte	.LLST20
	.uleb128 0x4
	.4byte	.LASF20
	.byte	0x2
	.2byte	0x2b1
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST21
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF21
	.byte	0x2
	.2byte	0x280
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST22
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF22
	.byte	0x2
	.2byte	0x28f
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST23
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF23
	.byte	0x2
	.2byte	0x34c
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST24
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF24
	.byte	0x2
	.2byte	0x8b3
	.4byte	.LFB49
	.4byte	.LFE49
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF25
	.byte	0x2
	.2byte	0x8c8
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LLST25
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF26
	.byte	0x2
	.2byte	0x8ec
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LLST26
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF27
	.byte	0x2
	.2byte	0x89a
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LLST27
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF28
	.byte	0x2
	.2byte	0x6a4
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST28
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF29
	.byte	0x2
	.2byte	0x654
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST29
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF30
	.byte	0x2
	.2byte	0x602
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST30
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF31
	.byte	0x2
	.2byte	0x5e7
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST31
	.uleb128 0x4
	.4byte	.LASF32
	.byte	0x2
	.2byte	0x5b0
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST32
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF33
	.byte	0x2
	.2byte	0x5d6
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST33
	.uleb128 0x4
	.4byte	.LASF34
	.byte	0x2
	.2byte	0x57d
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST34
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF35
	.byte	0x2
	.2byte	0x5a3
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST35
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF36
	.byte	0x2
	.2byte	0x56d
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST36
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF37
	.byte	0x2
	.2byte	0x910
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LLST37
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF38
	.byte	0x2
	.2byte	0x933
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LLST38
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF39
	.byte	0x2
	.2byte	0x956
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LLST39
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF40
	.byte	0x2
	.2byte	0x964
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LLST40
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF41
	.byte	0x2
	.2byte	0x868
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LLST41
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF42
	.byte	0x2
	.2byte	0x9bc
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LLST42
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF43
	.byte	0x2
	.2byte	0x9e5
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LLST43
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF44
	.byte	0x2
	.2byte	0xa11
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LLST44
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF45
	.byte	0x2
	.2byte	0xa3d
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LLST45
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF46
	.byte	0x2
	.2byte	0xa69
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LLST46
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF47
	.byte	0x2
	.2byte	0xa92
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LLST47
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF48
	.byte	0x2
	.2byte	0xabd
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LLST48
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF49
	.byte	0x2
	.2byte	0xaec
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LLST49
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF50
	.byte	0x2
	.2byte	0xb05
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LLST50
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF51
	.byte	0x2
	.2byte	0xb38
	.4byte	.LFB65
	.4byte	.LFE65
	.4byte	.LLST51
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF52
	.byte	0x2
	.2byte	0xb6b
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LLST52
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF53
	.byte	0x2
	.2byte	0xbb2
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	.LLST53
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF54
	.byte	0x2
	.2byte	0xbe1
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LLST54
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF55
	.byte	0x2
	.2byte	0xbfc
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LLST55
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF56
	.byte	0x2
	.2byte	0xc15
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	.LLST56
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF57
	.byte	0x2
	.2byte	0xc33
	.4byte	.LFB71
	.4byte	.LFE71
	.4byte	.LLST57
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF58
	.byte	0x2
	.2byte	0xc51
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LLST58
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF59
	.byte	0x2
	.2byte	0xc6f
	.4byte	.LFB73
	.4byte	.LFE73
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x65f
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST59
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF61
	.byte	0x2
	.2byte	0x831
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LLST60
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF62
	.byte	0x2
	.2byte	0x7c3
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LLST61
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF63
	.byte	0x2
	.2byte	0x794
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LLST62
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF64
	.byte	0x2
	.2byte	0x76b
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST63
	.uleb128 0x4
	.4byte	.LASF65
	.byte	0x2
	.2byte	0x73b
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST64
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF66
	.byte	0x2
	.2byte	0x75a
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST65
	.uleb128 0x4
	.4byte	.LASF67
	.byte	0x2
	.2byte	0x70f
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST66
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF68
	.byte	0x2
	.2byte	0x72e
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST67
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF69
	.byte	0x2
	.2byte	0x6eb
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST68
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x795
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST69
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF71
	.byte	0x2
	.2byte	0x6ca
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST70
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF72
	.byte	0x2
	.2byte	0x38e
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST71
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF73
	.byte	0x2
	.2byte	0xc75
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LLST72
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF74
	.byte	0x2
	.2byte	0xca4
	.4byte	.LFB75
	.4byte	.LFE75
	.4byte	.LLST73
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF75
	.byte	0x2
	.2byte	0xcc8
	.4byte	.LFB76
	.4byte	.LFE76
	.4byte	.LLST74
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF76
	.byte	0x2
	.2byte	0xce9
	.4byte	.LFB77
	.4byte	.LFE77
	.4byte	.LLST75
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF77
	.byte	0x2
	.2byte	0xd27
	.4byte	.LFB78
	.4byte	.LFE78
	.4byte	.LLST76
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF78
	.byte	0x2
	.2byte	0xd5b
	.4byte	.LFB79
	.4byte	.LFE79
	.4byte	.LLST77
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF79
	.byte	0x2
	.2byte	0xd96
	.4byte	.LFB80
	.4byte	.LFE80
	.4byte	.LLST78
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF80
	.byte	0x2
	.2byte	0xdcb
	.4byte	.LFB81
	.4byte	.LFE81
	.4byte	.LLST79
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF81
	.byte	0x2
	.2byte	0xdf6
	.4byte	.LFB82
	.4byte	.LFE82
	.4byte	.LLST80
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF82
	.byte	0x2
	.2byte	0xe35
	.4byte	.LFB83
	.4byte	.LFE83
	.4byte	.LLST81
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF83
	.byte	0x2
	.2byte	0xe60
	.4byte	.LFB84
	.4byte	.LFE84
	.4byte	.LLST82
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF84
	.byte	0x2
	.2byte	0xe7c
	.4byte	.LFB85
	.4byte	.LFE85
	.4byte	.LLST83
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF85
	.byte	0x2
	.2byte	0xebe
	.4byte	.LFB86
	.4byte	.LFE86
	.4byte	.LLST84
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF86
	.byte	0x2
	.2byte	0xedb
	.4byte	.LFB87
	.4byte	.LFE87
	.4byte	.LLST85
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF87
	.byte	0x2
	.2byte	0xf09
	.4byte	.LFB88
	.4byte	.LFE88
	.4byte	.LLST86
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF88
	.byte	0x2
	.2byte	0xf2f
	.4byte	.LFB89
	.4byte	.LFE89
	.4byte	.LLST87
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF89
	.byte	0x2
	.2byte	0xf4c
	.4byte	.LFB90
	.4byte	.LFE90
	.4byte	.LLST88
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF90
	.byte	0x2
	.2byte	0xf6b
	.4byte	.LFB91
	.4byte	.LFE91
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF91
	.byte	0x2
	.2byte	0xf75
	.4byte	.LFB92
	.4byte	.LFE92
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF92
	.byte	0x2
	.2byte	0xf7f
	.4byte	.LFB93
	.4byte	.LFE93
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF93
	.byte	0x2
	.2byte	0xf89
	.4byte	.LFB94
	.4byte	.LFE94
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF94
	.byte	0x2
	.2byte	0xf8f
	.4byte	.LFB95
	.4byte	.LFE95
	.4byte	.LLST89
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF95
	.byte	0x2
	.2byte	0xfc4
	.4byte	.LFB96
	.4byte	.LFE96
	.4byte	.LLST90
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB13
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB19
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI3
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB0
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI5
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB1
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB14
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB16
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI11
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB15
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB2
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI15
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB3
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI17
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB4
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB5
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI21
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB6
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI23
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB7
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI25
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB18
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI27
	.4byte	.LFE18
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB11
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI29
	.4byte	.LFE11
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB12
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI31
	.4byte	.LFE12
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB17
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI33
	.4byte	.LFE17
	.2byte	0x3
	.byte	0x7d
	.sleb128 112
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB22
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI35
	.4byte	.LFE22
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB101
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI37
	.4byte	.LFE101
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB102
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI39
	.4byte	.LFE102
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB103
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI41
	.4byte	.LFE103
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB25
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI43
	.4byte	.LFE25
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB23
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB24
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB26
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB50
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LFE50
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB51
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LFE51
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI49
	.4byte	.LFE48
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB36
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB35
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB34
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI52
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB33
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB31
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB32
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI55
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB29
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB30
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI57
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB28
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI58
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB52
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI59
	.4byte	.LFE52
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB53
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LFE53
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB54
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI61
	.4byte	.LFE54
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB55
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI62
	.4byte	.LFE55
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB47
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LFE47
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB56
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI64
	.4byte	.LFE56
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB57
	.4byte	.LCFI65
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI65
	.4byte	.LFE57
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST44:
	.4byte	.LFB58
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI66
	.4byte	.LFE58
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST45:
	.4byte	.LFB59
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI67
	.4byte	.LFE59
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST46:
	.4byte	.LFB60
	.4byte	.LCFI68
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI68
	.4byte	.LFE60
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST47:
	.4byte	.LFB61
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI69
	.4byte	.LFE61
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST48:
	.4byte	.LFB62
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI70
	.4byte	.LFE62
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST49:
	.4byte	.LFB63
	.4byte	.LCFI71
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI71
	.4byte	.LFE63
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST50:
	.4byte	.LFB64
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI72
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI73
	.4byte	.LFE64
	.2byte	0x3
	.byte	0x7d
	.sleb128 104
	.4byte	0
	.4byte	0
.LLST51:
	.4byte	.LFB65
	.4byte	.LCFI74
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI74
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI75
	.4byte	.LFE65
	.2byte	0x3
	.byte	0x7d
	.sleb128 104
	.4byte	0
	.4byte	0
.LLST52:
	.4byte	.LFB66
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI76
	.4byte	.LCFI77
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI77
	.4byte	.LFE66
	.2byte	0x3
	.byte	0x7d
	.sleb128 104
	.4byte	0
	.4byte	0
.LLST53:
	.4byte	.LFB67
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI78
	.4byte	.LFE67
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST54:
	.4byte	.LFB68
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI79
	.4byte	.LFE68
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST55:
	.4byte	.LFB69
	.4byte	.LCFI80
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI80
	.4byte	.LFE69
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST56:
	.4byte	.LFB70
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI81
	.4byte	.LFE70
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST57:
	.4byte	.LFB71
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI82
	.4byte	.LFE71
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST58:
	.4byte	.LFB72
	.4byte	.LCFI83
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI83
	.4byte	.LFE72
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST59:
	.4byte	.LFB20
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI84
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI85
	.4byte	.LFE20
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST60:
	.4byte	.LFB46
	.4byte	.LCFI86
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI86
	.4byte	.LFE46
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST61:
	.4byte	.LFB45
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI87
	.4byte	.LFE45
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST62:
	.4byte	.LFB44
	.4byte	.LCFI88
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI88
	.4byte	.LFE44
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST63:
	.4byte	.LFB43
	.4byte	.LCFI89
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI89
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST64:
	.4byte	.LFB41
	.4byte	.LCFI90
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI90
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST65:
	.4byte	.LFB42
	.4byte	.LCFI91
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI91
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST66:
	.4byte	.LFB39
	.4byte	.LCFI92
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI92
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST67:
	.4byte	.LFB40
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI93
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST68:
	.4byte	.LFB38
	.4byte	.LCFI94
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI94
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST69:
	.4byte	.LFB21
	.4byte	.LCFI95
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI95
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI96
	.4byte	.LFE21
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST70:
	.4byte	.LFB37
	.4byte	.LCFI97
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI97
	.4byte	.LCFI98
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI98
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST71:
	.4byte	.LFB27
	.4byte	.LCFI99
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI99
	.4byte	.LCFI100
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI100
	.4byte	.LFE27
	.2byte	0x3
	.byte	0x7d
	.sleb128 96
	.4byte	0
	.4byte	0
.LLST72:
	.4byte	.LFB74
	.4byte	.LCFI101
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI101
	.4byte	.LFE74
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST73:
	.4byte	.LFB75
	.4byte	.LCFI102
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI102
	.4byte	.LFE75
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST74:
	.4byte	.LFB76
	.4byte	.LCFI103
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI103
	.4byte	.LFE76
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST75:
	.4byte	.LFB77
	.4byte	.LCFI104
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI104
	.4byte	.LFE77
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST76:
	.4byte	.LFB78
	.4byte	.LCFI105
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI105
	.4byte	.LFE78
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST77:
	.4byte	.LFB79
	.4byte	.LCFI106
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI106
	.4byte	.LFE79
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST78:
	.4byte	.LFB80
	.4byte	.LCFI107
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI107
	.4byte	.LFE80
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST79:
	.4byte	.LFB81
	.4byte	.LCFI108
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI108
	.4byte	.LFE81
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST80:
	.4byte	.LFB82
	.4byte	.LCFI109
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI109
	.4byte	.LCFI110
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI110
	.4byte	.LFE82
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST81:
	.4byte	.LFB83
	.4byte	.LCFI111
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI111
	.4byte	.LFE83
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST82:
	.4byte	.LFB84
	.4byte	.LCFI112
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI112
	.4byte	.LFE84
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST83:
	.4byte	.LFB85
	.4byte	.LCFI113
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI113
	.4byte	.LCFI114
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI114
	.4byte	.LFE85
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST84:
	.4byte	.LFB86
	.4byte	.LCFI115
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI115
	.4byte	.LCFI116
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI116
	.4byte	.LFE86
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST85:
	.4byte	.LFB87
	.4byte	.LCFI117
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI117
	.4byte	.LCFI118
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI118
	.4byte	.LFE87
	.2byte	0x3
	.byte	0x7d
	.sleb128 136
	.4byte	0
	.4byte	0
.LLST86:
	.4byte	.LFB88
	.4byte	.LCFI119
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI119
	.4byte	.LFE88
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST87:
	.4byte	.LFB89
	.4byte	.LCFI120
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI120
	.4byte	.LFE89
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST88:
	.4byte	.LFB90
	.4byte	.LCFI121
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI121
	.4byte	.LFE90
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST89:
	.4byte	.LFB95
	.4byte	.LCFI122
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI122
	.4byte	.LFE95
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST90:
	.4byte	.LFB96
	.4byte	.LCFI123
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI123
	.4byte	.LFE96
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x31c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB101
	.4byte	.LFE101-.LFB101
	.4byte	.LFB102
	.4byte	.LFE102-.LFB102
	.4byte	.LFB103
	.4byte	.LFE103-.LFB103
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.4byte	.LFB65
	.4byte	.LFE65-.LFB65
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.4byte	.LFB71
	.4byte	.LFE71-.LFB71
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.4byte	.LFB77
	.4byte	.LFE77-.LFB77
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.4byte	.LFB79
	.4byte	.LFE79-.LFB79
	.4byte	.LFB80
	.4byte	.LFE80-.LFB80
	.4byte	.LFB81
	.4byte	.LFE81-.LFB81
	.4byte	.LFB82
	.4byte	.LFE82-.LFB82
	.4byte	.LFB83
	.4byte	.LFE83-.LFB83
	.4byte	.LFB84
	.4byte	.LFE84-.LFB84
	.4byte	.LFB85
	.4byte	.LFE85-.LFB85
	.4byte	.LFB86
	.4byte	.LFE86-.LFB86
	.4byte	.LFB87
	.4byte	.LFE87-.LFB87
	.4byte	.LFB88
	.4byte	.LFE88-.LFB88
	.4byte	.LFB89
	.4byte	.LFE89-.LFB89
	.4byte	.LFB90
	.4byte	.LFE90-.LFB90
	.4byte	.LFB91
	.4byte	.LFE91-.LFB91
	.4byte	.LFB92
	.4byte	.LFE92-.LFB92
	.4byte	.LFB93
	.4byte	.LFE93-.LFB93
	.4byte	.LFB94
	.4byte	.LFE94-.LFB94
	.4byte	.LFB95
	.4byte	.LFE95-.LFB95
	.4byte	.LFB96
	.4byte	.LFE96-.LFB96
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB101
	.4byte	.LFE101
	.4byte	.LFB102
	.4byte	.LFE102
	.4byte	.LFB103
	.4byte	.LFE103
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LFB65
	.4byte	.LFE65
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	.LFB71
	.4byte	.LFE71
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LFB73
	.4byte	.LFE73
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LFB75
	.4byte	.LFE75
	.4byte	.LFB76
	.4byte	.LFE76
	.4byte	.LFB77
	.4byte	.LFE77
	.4byte	.LFB78
	.4byte	.LFE78
	.4byte	.LFB79
	.4byte	.LFE79
	.4byte	.LFB80
	.4byte	.LFE80
	.4byte	.LFB81
	.4byte	.LFE81
	.4byte	.LFB82
	.4byte	.LFE82
	.4byte	.LFB83
	.4byte	.LFE83
	.4byte	.LFB84
	.4byte	.LFE84
	.4byte	.LFB85
	.4byte	.LFE85
	.4byte	.LFB86
	.4byte	.LFE86
	.4byte	.LFB87
	.4byte	.LFE87
	.4byte	.LFB88
	.4byte	.LFE88
	.4byte	.LFB89
	.4byte	.LFE89
	.4byte	.LFB90
	.4byte	.LFE90
	.4byte	.LFB91
	.4byte	.LFE91
	.4byte	.LFB92
	.4byte	.LFE92
	.4byte	.LFB93
	.4byte	.LFE93
	.4byte	.LFB94
	.4byte	.LFE94
	.4byte	.LFB95
	.4byte	.LFE95
	.4byte	.LFB96
	.4byte	.LFE96
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF83:
	.ascii	"SYSTEM_get_budget_in_use\000"
.LASF96:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF27:
	.ascii	"FDTO_SYSTEM_load_system_name_into_guivar\000"
.LASF69:
	.ascii	"SYSTEM_extract_and_store_changes_from_GuiVars\000"
.LASF71:
	.ascii	"SYSTEM_extract_and_store_group_name_from_GuiVars\000"
.LASF28:
	.ascii	"BUDGET_FLOW_TYPES_copy_group_into_guivars\000"
.LASF0:
	.ascii	"nm_SYSTEM_set_budget_flow_type\000"
.LASF50:
	.ascii	"SYSTEM_get_flow_check_ranges_gpm\000"
.LASF68:
	.ascii	"MAINLINE_BREAK_extract_and_store_changes_from_GuiVa"
	.ascii	"rs\000"
.LASF89:
	.ascii	"nm_SYSTEM_get_budget_flow_type\000"
.LASF55:
	.ascii	"SYSTEM_get_allow_table_to_lock\000"
.LASF84:
	.ascii	"SYSTEM_get_budget_details\000"
.LASF44:
	.ascii	"SYSTEM_get_mlb_during_mvor_opened_gpm\000"
.LASF17:
	.ascii	"nm_SYSTEM_set_mlb_all_other_times\000"
.LASF62:
	.ascii	"BUDGET_SETUP_extract_and_store_changes_from_GuiVars"
	.ascii	"\000"
.LASF97:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/irrigation_system.c\000"
.LASF52:
	.ascii	"SYSTEM_get_flow_check_tolerances_minus_gpm\000"
.LASF46:
	.ascii	"SYSTEM_get_capacity_in_use_bool\000"
.LASF18:
	.ascii	"nm_SYSTEM_set_flow_checking_in_use\000"
.LASF12:
	.ascii	"nm_SYSTEM_set_budget_meter_read_time\000"
.LASF41:
	.ascii	"SYSTEM_load_system_name_into_scroll_box_guivar\000"
.LASF95:
	.ascii	"IRRIGATION_SYSTEM_calculate_chain_sync_crc\000"
.LASF48:
	.ascii	"SYSTEM_get_capacity_without_pump_gpm\000"
.LASF33:
	.ascii	"SYSTEM_CAPACITY_copy_group_into_guivars\000"
.LASF5:
	.ascii	"nm_SYSTEM_set_flow_checking_tolerance_minus\000"
.LASF29:
	.ascii	"BUDGET_SETUP_copy_group_into_guivars\000"
.LASF30:
	.ascii	"MVOR_copy_group_into_guivars\000"
.LASF32:
	.ascii	"SYSTEM_CAPACITY_fill_guivars\000"
.LASF25:
	.ascii	"SYSTEM_get_group_with_this_GID\000"
.LASF24:
	.ascii	"SYSTEM_get_inc_by_for_MLB_and_capacity\000"
.LASF37:
	.ascii	"SYSTEM_get_index_for_group_with_this_GID\000"
.LASF65:
	.ascii	"SYSTEM_CAPACITY_extract_and_store_individual_group_"
	.ascii	"change\000"
.LASF98:
	.ascii	"nm_SYSTEM_set_budget_period\000"
.LASF80:
	.ascii	"SYSTEM_clear_mainline_break\000"
.LASF3:
	.ascii	"nm_SYSTEM_set_flow_checking_range\000"
.LASF16:
	.ascii	"nm_SYSTEM_set_mlb_during_mvor\000"
.LASF66:
	.ascii	"SYSTEM_CAPACITY_extract_and_store_changes_from_GuiV"
	.ascii	"ars\000"
.LASF20:
	.ascii	"nm_SYSTEM_set_default_values\000"
.LASF79:
	.ascii	"SYSTEM_get_highest_flow_checking_status_for_any_sys"
	.ascii	"tem\000"
.LASF2:
	.ascii	"nm_SYSTEM_set_mvor_schedule_close_time\000"
.LASF42:
	.ascii	"SYSTEM_get_used_for_irri_bool\000"
.LASF57:
	.ascii	"SYSTEM_get_derate_table_max_stations_ON\000"
.LASF54:
	.ascii	"SYSTEM_get_required_cell_iterations_before_flow_che"
	.ascii	"cking\000"
.LASF67:
	.ascii	"MAINLINE_BREAK_extract_and_store_individual_group_c"
	.ascii	"hange\000"
.LASF61:
	.ascii	"BUDGET_FLOW_TYPES_extract_and_store_changes_from_Gu"
	.ascii	"iVars\000"
.LASF81:
	.ascii	"SYSTEM_check_for_mvor_schedule_start\000"
.LASF93:
	.ascii	"nm_SYSTEM_set_poc_ratio\000"
.LASF72:
	.ascii	"SYSTEM_build_data_to_send\000"
.LASF70:
	.ascii	"nm_SYSTEM_extract_and_store_changes_from_comm\000"
.LASF35:
	.ascii	"MAINLINE_BREAK_copy_group_into_guivars\000"
.LASF11:
	.ascii	"nm_SYSTEM_set_budget_number_of_annual_periods\000"
.LASF15:
	.ascii	"nm_SYSTEM_set_mlb_during_irri\000"
.LASF31:
	.ascii	"FLOW_CHECKING_copy_group_into_guivars\000"
.LASF90:
	.ascii	"nm_SYSTEM_get_Vp\000"
.LASF88:
	.ascii	"nm_SYSTEM_get_used\000"
.LASF92:
	.ascii	"nm_SYSTEM_get_poc_ratio\000"
.LASF45:
	.ascii	"SYSTEM_get_mlb_during_all_other_times_gpm\000"
.LASF34:
	.ascii	"MAINLINE_BREAK_fill_guivars\000"
.LASF75:
	.ascii	"SYSTEM_on_all_systems_set_or_clear_commserver_chang"
	.ascii	"e_bits\000"
.LASF77:
	.ascii	"SYSTEM_at_least_one_system_has_flow_checking\000"
.LASF78:
	.ascii	"SYSTEM_get_flow_checking_status\000"
.LASF85:
	.ascii	"SYSTEM_get_budget_period_index\000"
.LASF73:
	.ascii	"SYSTEM_clean_house_processing\000"
.LASF59:
	.ascii	"SYSTEM_get_change_bits_ptr\000"
.LASF91:
	.ascii	"nm_SYSTEM_set_Vp\000"
.LASF22:
	.ascii	"save_file_irrigation_system\000"
.LASF43:
	.ascii	"SYSTEM_get_mlb_during_irri_gpm\000"
.LASF4:
	.ascii	"nm_SYSTEM_set_flow_checking_tolerance_plus\000"
.LASF74:
	.ascii	"IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_"
	.ascii	"distribution_in_the_next_token\000"
.LASF10:
	.ascii	"nm_SYSTEM_set_budget_mode\000"
.LASF8:
	.ascii	"nm_SYSTEM_set_system_used_for_irri\000"
.LASF38:
	.ascii	"SYSTEM_get_last_group_ID\000"
.LASF53:
	.ascii	"SYSTEM_get_required_station_cycles_before_flow_chec"
	.ascii	"king\000"
.LASF94:
	.ascii	"IRRIGATION_SYSTEM_load_ftimes_list\000"
.LASF56:
	.ascii	"SYSTEM_get_derate_table_gpm_slot_size\000"
.LASF6:
	.ascii	"nm_SYSTEM_set_budget_in_use\000"
.LASF82:
	.ascii	"SYSTEM_this_system_is_in_use\000"
.LASF86:
	.ascii	"SYSTEM_at_least_one_system_has_budget_enabled\000"
.LASF47:
	.ascii	"SYSTEM_get_capacity_with_pump_gpm\000"
.LASF1:
	.ascii	"nm_SYSTEM_set_mvor_schedule_open_time\000"
.LASF7:
	.ascii	"nm_SYSTEM_set_system_in_use\000"
.LASF19:
	.ascii	"nm_irrigation_system_structure_updater\000"
.LASF51:
	.ascii	"SYSTEM_get_flow_check_tolerances_plus_gpm\000"
.LASF14:
	.ascii	"nm_SYSTEM_set_capacity_without_pump\000"
.LASF40:
	.ascii	"SYSTEM_num_systems_in_use\000"
.LASF64:
	.ascii	"FLOW_CHECKING_extract_and_store_changes_from_GuiVar"
	.ascii	"s\000"
.LASF58:
	.ascii	"SYSTEM_get_derate_table_number_of_gpm_slots\000"
.LASF63:
	.ascii	"MVOR_extract_and_store_changes_from_GuiVars\000"
.LASF13:
	.ascii	"nm_SYSTEM_set_capacity_with_pump\000"
.LASF49:
	.ascii	"SYSTEM_get_flow_checking_in_use\000"
.LASF21:
	.ascii	"init_file_irrigation_system\000"
.LASF26:
	.ascii	"SYSTEM_get_group_at_this_index\000"
.LASF9:
	.ascii	"nm_SYSTEM_set_capacity_in_use\000"
.LASF39:
	.ascii	"SYSTEM_get_list_count\000"
.LASF76:
	.ascii	"nm_SYSTEM_update_pending_change_bits\000"
.LASF23:
	.ascii	"nm_SYSTEM_create_new_group\000"
.LASF36:
	.ascii	"SYSTEM_copy_group_into_guivars\000"
.LASF60:
	.ascii	"nm_SYSTEM_store_changes\000"
.LASF87:
	.ascii	"nm_SYSTEM_get_budget\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
