	.file	"e_status.c"
	.text
.Ltext0:
	.section	.text.FDTO_STATUS_update_screen,"ax",%progbits
	.align	2
	.type	FDTO_STATUS_update_screen, %function
FDTO_STATUS_update_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI0:
	ldr	r4, .L64+36
	sub	sp, sp, #108
.LCFI1:
	ldr	r5, [r4, #0]
	bl	WEATHER_get_et_gage_is_in_use
	cmp	r5, r0
	moveq	r4, #0
	beq	.L2
	bl	WEATHER_get_et_gage_is_in_use
	str	r0, [r4, #0]
	mov	r4, #1
.L2:
	ldr	r5, .L64+40
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L64+44
	ldr	r3, .L64+48
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L64+52
	ldr	r0, [r5, #0]
	ldr	r2, [r3, #84]
	ldr	r3, .L64+56
	ldr	r5, .L64+60
	ldr	r1, [r3, #0]
	cmp	r1, r2
	strne	r2, [r3, #0]
	movne	r4, #1
	bl	xQueueGiveMutexRecursive
	ldr	r6, [r5, #0]
	bl	WEATHER_get_rain_bucket_is_in_use
	cmp	r6, r0
	beq	.L4
	bl	WEATHER_get_rain_bucket_is_in_use
	mov	r4, #1
	str	r0, [r5, #0]
.L4:
	ldr	r5, .L64+64
	ldr	r6, [r5, #0]
	bl	WEATHER_get_wind_gage_in_use
	cmp	r6, r0
	beq	.L5
	bl	WEATHER_get_wind_gage_in_use
	mov	r4, #1
	str	r0, [r5, #0]
.L5:
	ldr	r5, .L64+68
	ldr	r6, [r5, #0]
	bl	WEATHER_get_rain_switch_in_use
	cmp	r6, r0
	beq	.L6
	bl	WEATHER_get_rain_switch_in_use
	mov	r4, #1
	str	r0, [r5, #0]
.L6:
	ldr	r5, .L64+72
	ldr	r6, [r5, #0]
	bl	WEATHER_get_freeze_switch_in_use
	cmp	r6, r0
	beq	.L7
	bl	WEATHER_get_freeze_switch_in_use
	mov	r4, #1
	str	r0, [r5, #0]
.L7:
	ldr	r5, .L64+76
	ldr	r6, [r5, #0]
	bl	POC_at_least_one_POC_has_a_flow_meter
	cmp	r0, #1
	movne	r0, #0
	bne	.L8
	bl	SYSTEM_num_systems_in_use
	adds	r0, r0, #0
	movne	r0, #1
.L8:
	str	r0, [r5, #0]
	ldr	r5, .L64+80
	cmp	r6, r0
	movne	r4, #1
	ldr	r6, [r5, #0]
	bl	IRRI_LIGHTS_get_number_of_energized_lights
.LBB8:
	ldr	r3, .L64+84
	mov	r1, #400
	ldr	r2, .L64+44
.LBE8:
	adds	r0, r0, #0
	movne	r0, #1
	cmp	r6, r0
	str	r0, [r5, #0]
.LBB9:
	ldr	r0, [r3, #0]
	mov	r3, #166
.LBE9:
	movne	r4, #1
.LBB10:
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L64+88
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L64+44
	mov	r3, #168
	bl	xQueueTakeMutexRecursive_debug
	mov	r5, #0
	mov	r6, r5
.L13:
	mov	r0, r6
	bl	SYSTEM_get_group_at_this_index
	cmp	r0, #0
	beq	.L11
	bl	nm_GROUP_get_group_ID
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	r7, r0, #0
	beq	.L11
	add	r0, r7, #516
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	cmp	r0, #0
	bne	.L51
	ldrb	r3, [r7, #468]	@ zero_extendqisi2
	tst	r3, #96
	movne	r5, #1
.L11:
	add	r6, r6, #1
	cmp	r6, #4
	bne	.L13
	mov	r3, #0
	b	.L12
.L51:
	mov	r3, #1
.L12:
	ldr	r2, .L64+92
	ldr	r1, [r2, #0]
	cmp	r1, r3
	strne	r3, [r2, #0]
	bne	.L60
	ldr	r3, .L64+96
	ldr	r2, [r3, #0]
	cmp	r2, r5
	moveq	r6, #0
	beq	.L15
	str	r5, [r3, #0]
.L60:
	mov	r6, #1
.L15:
	ldr	r3, .L64+88
	mov	r5, #0
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L64+84
	mov	r7, r5
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE10:
.LBB11:
	ldr	r3, .L64+100
	ldr	r8, [r3, #0]
	ldr	r3, .L64+104
	add	r2, r3, #48
.L18:
	ldr	r1, [r3, #4]!
	cmp	r1, #0
	ldr	r1, [r3, #48]
	movne	r7, #1
	cmp	r1, #0
	movne	r5, #1
	cmp	r3, r2
	bne	.L18
	ldr	r3, .L64+108
	ldr	r3, [r3, #8]
	cmp	r3, #0
	movne	r2, #0
	bne	.L63
	bl	STATION_get_num_of_stations_physically_available
	cmp	r0, #0
	beq	.L21
	bl	STATION_get_num_stations_in_use
	cmp	r0, #0
	bne	.L22
.L21:
	mov	r2, #1
.L63:
	ldr	r3, .L64+100
	str	r2, [r3, #0]
	b	.L20
.L22:
	cmp	r7, #0
	movne	r2, #2
	bne	.L63
	cmp	r5, #0
	movne	r2, #3
	bne	.L63
	ldr	r3, .L64+112
	ldr	r3, [r3, #112]
	cmp	r3, #0
	movne	r2, #4
	bne	.L63
	ldr	r3, .L64+116
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r2, #5
	bne	.L63
	ldr	r3, .L64+120
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r2, #6
	bne	.L63
.LBB12:
	ldr	r3, .L64+124
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L64+44
	ldr	r3, .L64+128
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L64+132
	bl	nm_ListGetFirst
	mov	r7, r5
	mov	r1, r0
	b	.L28
.L30:
	ldrb	r3, [r1, #53]	@ zero_extendqisi2
	ldr	r0, .L64+132
	tst	r3, #1
	movne	r5, r1
	addne	r7, r7, #1
	bl	nm_ListGetNext
	mov	r1, r0
.L28:
	cmp	r1, #0
	bne	.L30
	mov	r2, #65
	ldr	r0, .L64+136
	ldr	r1, .L64+140
	bl	strlcpy
	cmp	r7, #0
	ldr	r2, .L64+100
	beq	.L31
	cmp	r7, #1
	bne	.L32
	cmp	r5, #0
	beq	.L33
.LBB13:
	mov	r3, #7
	str	r3, [r2, #0]
	ldrb	r1, [r5, #40]	@ zero_extendqisi2
	add	r2, sp, #12
	mov	r3, #48
	ldr	r0, [r5, #36]
	bl	STATION_get_station_number_string
	add	r1, sp, #60
	mov	r7, r0
	ldr	r0, [r5, #36]
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	mov	r3, r7
	mov	r1, #65
	ldr	r2, .L64+144
	str	r0, [sp, #0]
	ldr	r0, .L64+148
	bl	snprintf
	flds	s10, [r5, #44]	@ int
	fldd	d7, .L64
	ldr	r1, [r5, #48]
	fsitod	d6, s10
	ldr	r0, .L64+136
	fdivd	d5, d6, d7
	fmrrd	r2, r3, d5
	fmsr	s11, r1	@ int
	mov	r1, #65
	str	r3, [sp, #0]
	fuitod	d6, s11
	mov	r3, r2
	ldr	r2, .L64+152
	fdivd	d7, d6, d7
	fstd	d7, [sp, #4]
	bl	snprintf
	b	.L33
.L32:
.LBE13:
	mov	r3, #7
	str	r3, [r2, #0]
	ldr	r0, .L64+148
	mov	r1, #65
	ldr	r2, .L64+156
	mov	r3, r7
	b	.L62
.L31:
	ldr	r3, .L64+132
	ldr	r3, [r3, #8]
	cmp	r3, #0
	moveq	r3, #8
	streq	r3, [r2, #0]
	beq	.L33
	cmp	r3, #1
	ldr	r1, .L64+160
	bls	.L35
	ldr	r1, [r1, #0]
	ldr	r0, .L64+148
	cmp	r1, #1
	mov	r1, #7
	str	r1, [r2, #0]
	ldrne	r2, .L64+164
	ldreq	r2, .L64+168
	mov	r1, #65
.L62:
	bl	snprintf
	b	.L33
.L35:
	ldr	r3, [r1, #0]
	ldr	r0, .L64+148
	cmp	r3, #1
	mov	r3, #7
	str	r3, [r2, #0]
	ldreq	r1, .L64+172
	ldrne	r1, .L64+176
	mov	r2, #65
	bl	strlcpy
.L33:
	ldr	r3, .L64+124
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L20:
.LBE12:
	ldr	r3, .L64+100
	ldr	r5, .L64+40
	mov	r1, #400
	ldr	r7, [r3, #0]
	ldr	r2, .L64+44
	mov	r3, #416
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L64+52
	ldr	r0, [r5, #0]
	ldrh	r2, [r3, #36]
	fmsr	s13, r2	@ int
	ldr	r2, .L64+180
	fsitod	d7, s13
	fldd	d6, .L64+8
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	flds	s15, [r3, #92]	@ int
	ldr	r3, [r3, #52]
	fuitos	s12, s15
	flds	s15, .L64+16
	fmsr	s11, r3	@ int
	fsts	s14, [r2, #0]
	ldr	r3, .L64+184
	ldr	r2, .L64+188
	fdivs	s12, s12, s15
	fldd	d7, .L64+20
	fcvtds	d6, s12
	fmuld	d6, d6, d7
	ftouizd	s10, d6
	fuitod	d6, s11
	fsts	s10, [r2, #0]	@ int
	fdivd	d7, d6, d7
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L64+84
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L64+44
	mov	r3, #436
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L64+88
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L64+44
	ldr	r3, .L64+192
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L64+76
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L38
	ldr	sl, .L64+196
	mov	r5, #0
	str	r5, [sl, #0]
.L40:
	mov	r0, r5
	bl	SYSTEM_get_group_at_this_index
	cmp	r0, #0
	beq	.L39
	bl	nm_GROUP_get_group_ID
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	cmp	r0, #0
	beq	.L39
	flds	s15, [r0, #304]
	ldr	r3, [sl, #0]
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	add	r3, r2, r3
	str	r3, [sl, #0]
.L39:
	add	r5, r5, #1
	cmp	r5, #4
	bne	.L40
	bl	SYSTEM_get_highest_flow_checking_status_for_any_system
	ldr	r3, .L64+200
	str	r0, [r3, #0]
.L38:
	ldr	r5, .L64+204
	ldr	sl, [r5, #0]
	bl	BUDGET_in_effect_for_any_system
	cmp	r0, sl
	str	r0, [r5, #0]
	movne	r7, #1
	bne	.L41
	subs	r7, r7, r8
	movne	r7, #1
.L41:
	ldr	r3, .L64+88
	ldr	r8, .L64+208
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L64+84
	ldr	r5, .L64+212
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L64+216
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L64+44
	ldr	r3, .L64+220
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_get_controller_index
	add	r0, r0, #39
	ldr	r3, [r8, r0, asl #2]
	cmp	r3, #16
	movls	r3, #0
	strls	r3, [r5, #0]	@ float
	bls	.L43
	bl	FLOWSENSE_get_controller_index
	add	r0, r0, #39
	ldr	r3, [r8, r0, asl #2]
	fmsr	s13, r3	@ int
	fuitod	d7, s13
	fldd	d6, .L64+28
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r5, #0]
.L43:
	ldr	r3, .L64+216
.LBE11:
	orr	r4, r6, r4
.LBB14:
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE14:
	orrs	r4, r4, r7
	beq	.L44
	mov	r0, #0
	bl	FDTO_Redraw_Screen
	ldr	r0, .L64+224
	bl	postBackground_Calculation_Event
	b	.L1
.L44:
	bl	GuiLib_Refresh
.L1:
	add	sp, sp, #108
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L65:
	.align	2
.L64:
	.word	0
	.word	1078853632
	.word	0
	.word	1086556160
	.word	1149861888
	.word	0
	.word	1079574528
	.word	0
	.word	1083129856
	.word	GuiVar_StatusETGageConnected
	.word	weather_preserves_recursive_MUTEX
	.word	.LC0
	.word	542
	.word	weather_preserves
	.word	GuiVar_ETGageRunawayGage
	.word	GuiVar_StatusRainBucketConnected
	.word	GuiVar_StatusWindGageConnected
	.word	GuiVar_StatusRainSwitchConnected
	.word	GuiVar_StatusFreezeSwitchConnected
	.word	GuiVar_StatusShowSystemFlow
	.word	GuiVar_LightsAreEnergized
	.word	system_preserves_recursive_MUTEX
	.word	list_system_recursive_MUTEX
	.word	GuiVar_StatusMLBInEffect
	.word	GuiVar_StatusMVORInEffect
	.word	GuiVar_StatusIrrigationActivityState
	.word	irri_comm+92
	.word	comm_mngr
	.word	tpmicro_comm
	.word	GuiVar_StatusRainSwitchState
	.word	GuiVar_StatusFreezeSwitchState
	.word	irri_irri_recursive_MUTEX
	.word	310
	.word	irri_irri
	.word	GuiVar_StatusCycleLeft
	.word	.LC1
	.word	.LC2
	.word	GuiVar_StatusOverviewStationStr
	.word	.LC3
	.word	.LC4
	.word	GuiVar_StatusWindPaused
	.word	.LC6
	.word	.LC5
	.word	.LC7
	.word	.LC8
	.word	GuiVar_StatusETGageReading
	.word	GuiVar_StatusRainBucketReading
	.word	GuiVar_ETGagePercentFull
	.word	438
	.word	GuiVar_StatusSystemFlowActual
	.word	GuiVar_StatusSystemFlowStatus
	.word	GuiVar_BudgetsInEffect
	.word	tpmicro_data
	.word	GuiVar_StatusCurrentDraw
	.word	tpmicro_data_recursive_MUTEX
	.word	486
	.word	4369
.LFE2:
	.size	FDTO_STATUS_update_screen, .-FDTO_STATUS_update_screen
	.section	.text.FDTO_STATUS_jump_to_first_available_cursor_pos,"ax",%progbits
	.align	2
	.type	FDTO_STATUS_jump_to_first_available_cursor_pos, %function
FDTO_STATUS_jump_to_first_available_cursor_pos:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L74
	mov	r1, r0
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #71
	bne	.L73
	ldr	r3, .L74+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #24
	bne	.L73
	ldr	r3, .L74+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #23
	bne	.L73
	ldr	r3, .L74+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #22
	bne	.L73
	ldr	r3, .L74+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #21
	bne	.L73
.LBB17:
	ldr	r3, .L74+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldreq	r3, .L74+24
	movne	r0, #20
	ldreq	r0, [r3, #0]
	addeq	r0, r0, #30
.L73:
.LBE17:
.LBB18:
	b	FDTO_Cursor_Select
.L75:
	.align	2
.L74:
	.word	GuiVar_AccessControlEnabled
	.word	GuiVar_StatusNOWDaysInEffect
	.word	GuiVar_StatusElectricalErrors
	.word	GuiVar_StatusFlowErrors
	.word	GuiVar_StatusMVORInEffect
	.word	GuiVar_StatusMLBInEffect
	.word	GuiVar_StatusIrrigationActivityState
.LBE18:
.LFE3:
	.size	FDTO_STATUS_jump_to_first_available_cursor_pos, .-FDTO_STATUS_jump_to_first_available_cursor_pos
	.section	.text.STATUS_update_screen,"ax",%progbits
	.align	2
	.global	STATUS_update_screen
	.type	STATUS_update_screen, %function
STATUS_update_screen:
.LFB4:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI2:
	mov	r3, #2
	sub	sp, sp, #36
.LCFI3:
	str	r3, [sp, #0]
	ldr	r3, .L77
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L78:
	.align	2
.L77:
	.word	FDTO_STATUS_update_screen
.LFE4:
	.size	STATUS_update_screen, .-STATUS_update_screen
	.section	.text.FDTO_STATUS_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_STATUS_draw_screen
	.type	FDTO_STATUS_draw_screen, %function
FDTO_STATUS_draw_screen:
.LFB5:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L87
	stmfd	sp!, {r4, lr}
.LCFI4:
	ldr	r3, [r3, #0]
	sub	sp, sp, #20
.LCFI5:
	cmp	r3, #0
	beq	.L80
	ldr	r3, .L87+4
	ldr	r0, [r3, #0]
	cmp	r0, #0
	bne	.L81
	bl	FDTO_STATUS_jump_to_first_available_cursor_pos
	b	.L82
.L81:
	mov	r1, #0
	bl	FDTO_Cursor_Select
.L82:
	ldr	r3, .L87+8
	mvn	r2, #0
	str	r2, [r3, #0]
.L80:
	ldr	r3, .L87
	ldr	r4, .L87+12
	ldr	r2, [r3, #0]
	cmp	r2, #0
	beq	.L83
	ldrsh	r2, [r4, #0]
	cmp	r2, #0
	bne	.L83
	str	r2, [r3, #0]
	ldr	r0, .L87+16
	bl	Alert_Message
.L83:
	ldrsh	r3, [r4, #0]
	cmp	r3, #0
	beq	.L84
	ldr	r3, .L87
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L85
.L84:
	bl	FDTO_STATUS_update_screen
.L85:
	ldr	r3, .L87+12
	ldrsh	r4, [r3, #0]
	cmp	r4, #0
	bne	.L79
.LBB19:
	mov	r0, sp
	bl	EPSON_obtain_latest_complete_time_and_date
	mov	r0, sp
	mov	r1, r4
	bl	BUDGET_calculate_ratios
.L79:
.LBE19:
	add	sp, sp, #20
	ldmfd	sp!, {r4, pc}
.L88:
	.align	2
.L87:
	.word	GuiVar_StatusShowLiveScreens
	.word	.LANCHOR0
	.word	g_MAIN_MENU_active_menu_item
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LC9
.LFE5:
	.size	FDTO_STATUS_draw_screen, .-FDTO_STATUS_draw_screen
	.section	.text.STATUS_process_screen,"ax",%progbits
	.align	2
	.global	STATUS_process_screen
	.type	STATUS_process_screen, %function
STATUS_process_screen:
.LFB6:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	str	lr, [sp, #-4]!
.LCFI6:
	mov	r3, r0
	sub	sp, sp, #36
.LCFI7:
	beq	.L93
	bhi	.L97
	cmp	r0, #0
	beq	.L91
	cmp	r0, #1
	bne	.L90
	b	.L260
.L97:
	cmp	r0, #4
	beq	.L95
	bcc	.L94
	cmp	r0, #67
	bne	.L90
	b	.L261
.L93:
	ldr	r2, .L266
	ldrh	r2, [r2, #0]
	cmp	r2, #0
	bne	.L98
	ldr	r1, .L266+4
	ldr	r3, .L266+8
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [r1, #0]
	mov	r0, sp
	str	r3, [sp, #20]
	str	r2, [sp, #24]
	bl	Display_Post_Command
	b	.L89
.L98:
	mov	r3, r2, asl #16
	mov	r3, r3, asr #16
	sub	r3, r3, #19
	cmp	r3, #56
	ldrls	pc, [pc, r3, asl #2]
	b	.L197
.L112:
	.word	.L101
	.word	.L102
	.word	.L102
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L103
	.word	.L104
	.word	.L197
	.word	.L197
	.word	.L106
	.word	.L107
	.word	.L107
	.word	.L108
	.word	.L108
	.word	.L197
	.word	.L109
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L107
	.word	.L107
	.word	.L107
	.word	.L107
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L102
	.word	.L110
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L106
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L197
	.word	.L111
.L103:
	mov	r3, #93
	str	r3, [sp, #8]
	ldr	r3, .L266+12
	str	r3, [sp, #20]
	mov	r3, #54
	str	r3, [sp, #32]
	ldr	r3, .L266+16
	b	.L206
.L104:
	mov	r3, #67
	str	r3, [sp, #8]
	ldr	r3, .L266+20
	str	r3, [sp, #20]
	mov	r3, #20
	str	r3, [sp, #32]
	ldr	r3, .L266+24
	b	.L206
.L106:
	mov	r3, #94
	str	r3, [sp, #8]
	ldr	r3, .L266+28
	str	r3, [sp, #20]
	mov	r3, #53
	str	r3, [sp, #32]
	ldr	r3, .L266+32
	b	.L206
.L107:
	mov	r3, #98
	str	r3, [sp, #8]
	ldr	r3, .L266+36
	str	r3, [sp, #20]
	mov	r3, #55
	str	r3, [sp, #32]
	ldr	r3, .L266+40
	b	.L206
.L108:
	mov	r3, #85
	str	r3, [sp, #8]
	ldr	r3, .L266+44
	str	r3, [sp, #20]
	mov	r3, #22
	str	r3, [sp, #32]
	ldr	r3, .L266+48
	b	.L206
.L109:
	bl	NETWORK_CONFIG_get_controller_off
	cmp	r0, #0
	beq	.L114
	mov	r3, #61
	str	r3, [sp, #8]
	ldr	r3, .L266+52
	str	r3, [sp, #20]
	mov	r3, #21
	str	r3, [sp, #32]
	ldr	r3, .L266+56
	b	.L206
.L114:
	ldr	r3, .L266+60
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bne	.L115
	mov	r3, #55
	str	r3, [sp, #8]
	ldr	r3, .L266+64
	str	r3, [sp, #20]
	mov	r3, #20
	str	r3, [sp, #32]
	ldr	r3, .L266+68
	b	.L206
.L115:
	cmp	r3, #2
	bne	.L116
	mov	r3, #39
	str	r3, [sp, #8]
	ldr	r3, .L266+72
	str	r3, [sp, #20]
	mov	r3, #25
	str	r3, [sp, #32]
	ldr	r3, .L266+76
.L206:
	str	r3, [sp, #16]
	b	.L113
.L116:
	mov	r3, #54
	str	r3, [sp, #8]
	ldr	r3, .L266+80
	str	r3, [sp, #20]
	mov	r3, #21
	str	r3, [sp, #32]
	ldr	r3, .L266+84
	b	.L206
.L102:
	mov	r3, #97
	str	r3, [sp, #8]
	ldr	r3, .L266+88
	str	r3, [sp, #20]
	mov	r3, #50
	str	r3, [sp, #32]
	ldr	r3, .L266+92
	b	.L206
.L110:
	mov	r3, #47
	str	r3, [sp, #8]
	ldr	r3, .L266+96
	str	r3, [sp, #20]
	mov	r3, #20
	str	r3, [sp, #32]
	ldr	r3, .L266+100
	str	r3, [sp, #16]
	ldr	r3, .L266+104
	str	r3, [sp, #12]
	b	.L113
.L111:
	mov	r3, #95
	str	r3, [sp, #8]
	ldr	r3, .L266+108
	str	r3, [sp, #20]
	mov	r3, #56
	str	r3, [sp, #32]
	ldr	r3, .L266+112
	b	.L206
.L101:
	mov	r3, #75
	str	r3, [sp, #8]
	ldr	r3, .L266+116
	str	r3, [sp, #20]
	mov	r3, #9
	str	r3, [sp, #32]
	ldr	r3, .L266+120
	b	.L206
.L95:
	ldr	r3, .L266
	ldrsh	r3, [r3, #0]
	cmp	r3, #23
	beq	.L150
	bgt	.L126
	cmp	r3, #20
	beq	.L153
	bgt	.L127
	cmp	r3, #19
	b	.L210
.L127:
	cmp	r3, #21
	beq	.L152
	cmp	r3, #22
	bne	.L156
	b	.L151
.L126:
	cmp	r3, #38
	bgt	.L128
	cmp	r3, #37
	bge	.L124
	cmp	r3, #24
	bne	.L156
	b	.L149
.L128:
	cmp	r3, #75
.L210:
	bne	.L156
	b	.L197
.L124:
	ldr	r3, .L266+124
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #24
	bne	.L207
.L149:
	ldr	r3, .L266+128
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #23
	bne	.L207
.L150:
	ldr	r3, .L266+132
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L221
.L151:
	ldr	r3, .L266+136
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L218
.L152:
	ldr	r3, .L266+140
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L213
.L153:
	ldr	r3, .L266+144
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #75
	bne	.L207
	ldr	r3, .L266+148
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L197
	b	.L215
.L260:
	ldr	r2, .L266
	ldrsh	r2, [r2, #0]
	cmp	r2, #23
	beq	.L161
	bgt	.L164
	cmp	r2, #20
	beq	.L225
	bgt	.L165
	cmp	r2, #19
	bne	.L156
	b	.L262
.L165:
	cmp	r2, #21
	beq	.L229
	cmp	r2, #22
	b	.L247
.L164:
	cmp	r2, #38
	bgt	.L166
	cmp	r2, #37
	bge	.L163
	cmp	r2, #24
	b	.L245
.L166:
	cmp	r2, #75
	b	.L249
.L262:
	mov	r0, #0
	b	.L173
.L161:
	ldr	r2, .L266+132
	ldr	r2, [r2, #0]
	cmp	r2, #0
	bne	.L156
	ldr	r2, .L266+136
	ldr	r2, [r2, #0]
	cmp	r2, #0
	bne	.L156
	ldr	r2, .L266+140
	ldr	r2, [r2, #0]
	cmp	r2, #0
	bne	.L156
	ldr	r2, .L266+148
	ldr	r0, [r2, #0]
	cmp	r0, #0
	bne	.L156
.L173:
	ldr	r2, .L266+4
	mov	r1, r3
	str	r0, [r2, #0]
	b	.L208
.L163:
	ldr	r3, .L266+124
	ldr	r3, [r3, #0]
	cmp	r3, #0
.L245:
	bne	.L156
	ldr	r3, .L266+128
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L156
	ldr	r3, .L266+132
	ldr	r3, [r3, #0]
	cmp	r3, #0
.L247:
	bne	.L156
	ldr	r3, .L266+136
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L156
.L229:
	ldr	r3, .L266+140
	ldr	r3, [r3, #0]
	cmp	r3, #0
.L249:
	bne	.L156
.L225:
	ldr	r3, .L266+148
	ldr	r0, [r3, #0]
	cmp	r0, #0
	beq	.L220
.L156:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L89
.L91:
	ldr	r2, .L266
	ldrsh	r3, [r2, #0]
	cmp	r3, #53
	beq	.L182
	bgt	.L185
	cmp	r3, #51
	bgt	.L181
	cmp	r3, #50
	bge	.L180
	cmp	r3, #40
	bne	.L194
	b	.L263
.L185:
	cmp	r3, #71
	bgt	.L199
	cmp	r3, #70
	bge	.L197
	sub	r3, r3, #60
	cmp	r3, #1
	bhi	.L194
	b	.L197
.L263:
	ldr	r3, .L266+152
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #50
	bne	.L207
	ldr	r3, .L266+156
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #51
	bne	.L207
	ldr	r3, .L266+160
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #52
	bne	.L207
	ldr	r3, .L266+164
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L217
.L180:
	ldr	r3, .L266+168
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #60
	moveq	r0, #61
	b	.L207
.L181:
	ldr	r3, .L266+164
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L182
.L217:
	mov	r0, #53
	b	.L207
.L182:
	mov	r0, #70
	b	.L207
.L94:
	ldr	r2, .L266
	ldrsh	r3, [r2, #0]
	cmp	r3, #71
	beq	.L197
	bgt	.L199
	cmp	r3, #0
	beq	.L195
	cmp	r3, #70
	bne	.L194
	b	.L264
.L199:
	cmp	r3, #75
	bne	.L194
	b	.L265
.L195:
	ldr	r3, .L266+4
	mov	r1, #1
	str	r1, [r3, #0]
	ldr	r3, .L266+124
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #24
	bne	.L208
	ldr	r3, .L266+128
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #23
	bne	.L208
	ldr	r3, .L266+132
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L202
.L221:
	mov	r0, #22
	b	.L207
.L202:
	ldr	r3, .L266+136
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L203
.L218:
	mov	r0, #21
	b	.L207
.L203:
	ldr	r3, .L266+140
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L204
.L213:
	mov	r0, #20
	b	.L207
.L204:
	ldr	r3, .L266+148
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L205
.L215:
	mov	r0, #19
	b	.L207
.L205:
	ldr	r3, .L266+172
	ldr	r0, [r3, #0]
	add	r0, r0, #30
	b	.L207
.L264:
	mov	r0, #71
	b	.L207
.L197:
	bl	bad_key_beep
	b	.L89
.L265:
	mov	r3, #19
	strh	r3, [r2, #0]	@ movhi
.L194:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L89
.L261:
	mov	r0, #0
.L220:
	ldr	r3, .L266+4
	str	r0, [r3, #0]
.L207:
	mov	r1, #1
.L208:
	bl	CURSOR_Select
	b	.L89
.L90:
	mov	r0, r3
	bl	KEY_process_global_keys
	b	.L89
.L113:
	ldr	r2, .L266+4
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L266
	mov	r0, #36
	ldrsh	r1, [r2, #0]
	ldr	r2, .L266+176
	str	r1, [r2, #0]
	ldr	r2, .L266+180
	ldr	r1, .L266+184
	ldr	r2, [r2, #0]
	mla	r2, r0, r2, r1
	mov	r0, sp
	str	r3, [r2, #4]
	mov	r2, #2
	stmia	sp, {r2, r3}
	mov	r3, #1
	str	r3, [sp, #24]
	bl	Change_Screen
.L89:
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L267:
	.align	2
.L266:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_StatusShowLiveScreens
	.word	FDTO_STATUS_jump_to_first_available_cursor_pos
	.word	FDTO_REAL_TIME_COMMUNICATIONS_draw_report
	.word	REAL_TIME_COMMUNICATIONS_process_report
	.word	FDTO_STATIONS_IN_USE_draw_screen
	.word	STATIONS_IN_USE_process_screen
	.word	FDTO_REAL_TIME_ELECTRICAL_draw_report
	.word	REAL_TIME_ELECTRICAL_process_report
	.word	FDTO_REAL_TIME_WEATHER_draw_report
	.word	REAL_TIME_WEATHER_process_report
	.word	FDTO_IRRI_DETAILS_draw_report
	.word	IRRI_DETAILS_process_report
	.word	FDTO_TURN_OFF_draw_screen
	.word	TURN_OFF_process_screen
	.word	GuiVar_StatusTypeOfSchedule
	.word	FDTO_STATION_GROUP_draw_menu
	.word	STATION_GROUP_process_menu
	.word	FDTO_MANUAL_PROGRAMS_draw_menu
	.word	MANUAL_PROGRAMS_process_menu
	.word	FDTO_SCHEDULE_draw_menu
	.word	SCHEDULE_process_menu
	.word	FDTO_REAL_TIME_SYSTEM_FLOW_draw_report
	.word	REAL_TIME_SYSTEM_FLOW_process_report
	.word	FDTO_POC_draw_menu
	.word	POC_process_menu
	.word	nm_POC_load_group_name_into_guivar
	.word	FDTO_REAL_TIME_LIGHTS_draw_report
	.word	REAL_TIME_LIGHTS_process_report
	.word	FDTO_BUDGET_REPORT_draw_report
	.word	BUDGET_REPORT_process_menu
	.word	GuiVar_StatusNOWDaysInEffect
	.word	GuiVar_StatusElectricalErrors
	.word	GuiVar_StatusFlowErrors
	.word	GuiVar_StatusMVORInEffect
	.word	GuiVar_StatusMLBInEffect
	.word	GuiVar_LightsAreEnergized
	.word	GuiVar_BudgetsInEffect
	.word	GuiVar_StatusETGageConnected
	.word	GuiVar_ETGageRunawayGage
	.word	GuiVar_StatusRainBucketConnected
	.word	GuiVar_StatusWindGageConnected
	.word	GuiVar_StatusShowSystemFlow
	.word	GuiVar_StatusIrrigationActivityState
	.word	.LANCHOR0
	.word	screen_history_index
	.word	ScreenHistory
.LFE6:
	.size	STATUS_process_screen, .-STATUS_process_screen
	.global	g_STATUS_last_cursor_position
	.section	.bss.g_STATUS_last_cursor_position,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_STATUS_last_cursor_position, %object
	.size	g_STATUS_last_cursor_position, 4
g_STATUS_last_cursor_position:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_status.c\000"
.LC1:
	.ascii	"\000"
.LC2:
	.ascii	"Station %s on %s is watering\000"
.LC3:
	.ascii	"%.1f min left of a %.1f cycle\000"
.LC4:
	.ascii	"%d valves are on\000"
.LC5:
	.ascii	"There are %d stations PAUSED\000"
.LC6:
	.ascii	"There are %d stations WAITING or SOAKING\000"
.LC7:
	.ascii	"There is 1 station PAUSED\000"
.LC8:
	.ascii	"There is 1 station WAITING or SOAKING\000"
.LC9:
	.ascii	"CURSOR: status redraw caused cursor reset\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x88
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x1c
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI6-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_status.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x9a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF6
	.byte	0x1
	.4byte	.LASF7
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x288
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x96
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0xf0
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x207
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	0x21
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x2b1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x2c0
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x2f5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x3
	.byte	0x7d
	.sleb128 136
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB6
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"FDTO_STATUS_draw_screen\000"
.LASF7:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_status.c\000"
.LASF3:
	.ascii	"STATUS_update_screen\000"
.LASF0:
	.ascii	"FDTO_STATUS_jump_to_first_available_cursor_pos\000"
.LASF8:
	.ascii	"FDTO_STATUS_update_screen\000"
.LASF2:
	.ascii	"FDTO_STATUS_update_irrigation_activity\000"
.LASF1:
	.ascii	"FDTO_STATUS_update_major_alert\000"
.LASF6:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"STATUS_process_screen\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
