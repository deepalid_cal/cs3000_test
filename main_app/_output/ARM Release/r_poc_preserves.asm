	.file	"r_poc_preserves.c"
	.text
.Ltext0:
	.section	.text.FDTO_POC_PRESERVES_redraw_report,"ax",%progbits
	.align	2
	.type	FDTO_POC_PRESERVES_redraw_report, %function
FDTO_POC_PRESERVES_redraw_report:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L2
	str	lr, [sp, #-4]!
.LCFI0:
	ldr	r0, [r3, #0]
	bl	POC_copy_preserve_info_into_guivars
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L3:
	.align	2
.L2:
	.word	GuiVar_POCPreservesIndex
.LFE0:
	.size	FDTO_POC_PRESERVES_redraw_report, .-FDTO_POC_PRESERVES_redraw_report
	.section	.text.FDTO_POC_PRESERVES_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_POC_PRESERVES_draw_report
	.type	FDTO_POC_PRESERVES_draw_report, %function
FDTO_POC_PRESERVES_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	str	lr, [sp, #-4]!
.LCFI1:
	mov	r2, r0
	bne	.L5
	mov	r0, #90
	mov	r1, #0
	bl	GuiLib_ShowScreen
	ldr	r3, .L6
	mov	r2, #0
	str	r2, [r3, #0]
.L5:
	ldr	lr, [sp], #4
	b	FDTO_POC_PRESERVES_redraw_report
.L7:
	.align	2
.L6:
	.word	GuiVar_POCPreservesIndex
.LFE1:
	.size	FDTO_POC_PRESERVES_draw_report, .-FDTO_POC_PRESERVES_draw_report
	.section	.text.POC_PRESERVES_process_report,"ax",%progbits
	.align	2
	.global	POC_PRESERVES_process_report
	.type	POC_PRESERVES_process_report, %function
POC_PRESERVES_process_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	stmfd	sp!, {r4, lr}
.LCFI2:
	mov	r3, r0
	sub	sp, sp, #44
.LCFI3:
	mov	r2, r1
	beq	.L11
	bhi	.L12
	cmp	r0, #16
	beq	.L10
	cmp	r0, #20
	bne	.L9
	b	.L15
.L12:
	cmp	r0, #80
	beq	.L10
	cmp	r0, #84
	bne	.L9
.L10:
	cmp	r3, #20
	beq	.L15
	cmp	r3, #16
	movne	r0, r3
	moveq	r0, #80
	b	.L13
.L15:
	mov	r0, #84
.L13:
	mov	r4, #1
	ldr	r1, .L18
	mov	r2, #0
	mov	r3, #11
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	bl	process_uns32
	ldr	r3, .L18+4
	add	r0, sp, #8
	str	r4, [sp, #8]
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L8
.L11:
	ldr	r3, .L18+8
	mov	r2, #11
	str	r2, [r3, #0]
	b	.L17
.L9:
	mov	r0, r3
	mov	r1, r2
.L17:
	bl	KEY_process_global_keys
.L8:
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L19:
	.align	2
.L18:
	.word	GuiVar_POCPreservesIndex
	.word	FDTO_POC_PRESERVES_redraw_report
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	POC_PRESERVES_process_report, .-POC_PRESERVES_process_report
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_poc_preserves.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x59
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x2b
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x43
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x5c
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"POC_PRESERVES_process_report\000"
.LASF4:
	.ascii	"FDTO_POC_PRESERVES_redraw_report\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_poc_preserves.c\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"FDTO_POC_PRESERVES_draw_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
