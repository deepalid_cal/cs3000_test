	.file	"scrollbox.c"
	.text
.Ltext0:
	.section	.text.FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw,"ax",%progbits
	.align	2
	.type	FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw, %function
FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	rsbs	r0, r0, #1
	movcc	r0, #0
	str	r0, [r3, #0]
	bx	lr
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
.LFE5:
	.size	FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw, .-FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.section	.text.FDTO_SCROLL_BOX_redraw,"ax",%progbits
	.align	2
	.type	FDTO_SCROLL_BOX_redraw, %function
FDTO_SCROLL_BOX_redraw:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	and	r0, r0, #255
	bl	GuiLib_ScrollBox_Redraw
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.LFE3:
	.size	FDTO_SCROLL_BOX_redraw, .-FDTO_SCROLL_BOX_redraw
	.section	.text.FDTO_SCROLL_BOX_down,"ax",%progbits
	.align	2
	.type	FDTO_SCROLL_BOX_down, %function
FDTO_SCROLL_BOX_down:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	and	r4, r0, #255
	mov	r0, r4
	bl	GuiLib_ScrollBox_Down
	cmp	r0, #1
	bne	.L6
.LBB4:
	bl	good_key_beep
	ldr	r3, .L8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L7
	mov	r0, r4
	bl	GuiLib_ScrollBox_Redraw
	bl	GuiLib_Refresh
.L7:
	mov	r0, #1
.LBE4:
	ldmfd	sp!, {r4, lr}
.LBB5:
	b	vTaskDelay
.L6:
.LBE5:
	ldmfd	sp!, {r4, lr}
	b	bad_key_beep
.L9:
	.align	2
.L8:
	.word	.LANCHOR0
.LFE0:
	.size	FDTO_SCROLL_BOX_down, .-FDTO_SCROLL_BOX_down
	.section	.text.FDTO_SCROLL_BOX_up,"ax",%progbits
	.align	2
	.type	FDTO_SCROLL_BOX_up, %function
FDTO_SCROLL_BOX_up:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	and	r4, r0, #255
	mov	r0, r4
	bl	GuiLib_ScrollBox_Up
	cmp	r0, #1
	bne	.L11
.LBB8:
	bl	good_key_beep
	ldr	r3, .L13
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L12
	mov	r0, r4
	bl	GuiLib_ScrollBox_Redraw
	bl	GuiLib_Refresh
.L12:
	mov	r0, #1
.LBE8:
	ldmfd	sp!, {r4, lr}
.LBB9:
	b	vTaskDelay
.L11:
.LBE9:
	ldmfd	sp!, {r4, lr}
	b	bad_key_beep
.L14:
	.align	2
.L13:
	.word	.LANCHOR0
.LFE1:
	.size	FDTO_SCROLL_BOX_up, .-FDTO_SCROLL_BOX_up
	.section	.text.FDTO_SCROLL_BOX_to_line,"ax",%progbits
	.align	2
	.global	FDTO_SCROLL_BOX_to_line
	.type	FDTO_SCROLL_BOX_to_line, %function
FDTO_SCROLL_BOX_to_line:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI3:
	mov	r1, r1, asl #16
	and	r4, r0, #255
	mov	r0, r4
	mov	r1, r1, lsr #16
	bl	GuiLib_ScrollBox_To_Line
	cmp	r0, #1
	ldmnefd	sp!, {r4, pc}
.LBB12:
	ldr	r3, .L18
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L17
	ldr	r2, .L18+4
	mov	r0, r4
	str	r3, [r2, #0]
	bl	GuiLib_ScrollBox_Redraw
	bl	GuiLib_Refresh
.L17:
	mov	r0, #1
.LBE12:
	ldmfd	sp!, {r4, lr}
.LBB13:
	b	vTaskDelay
.L19:
	.align	2
.L18:
	.word	.LANCHOR0
	.word	GuiVar_ScrollBoxHorizScrollPos
.LBE13:
.LFE2:
	.size	FDTO_SCROLL_BOX_to_line, .-FDTO_SCROLL_BOX_to_line
	.section	.text.FDTO_SCROLL_BOX_redraw_retaining_topline,"ax",%progbits
	.align	2
	.global	FDTO_SCROLL_BOX_redraw_retaining_topline
	.type	FDTO_SCROLL_BOX_redraw_retaining_topline, %function
FDTO_SCROLL_BOX_redraw_retaining_topline:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #0
	str	lr, [sp, #-4]!
.LCFI4:
	bne	.L21
	cmp	r2, #0
	beq	.L26
	bl	DIALOG_a_dialog_is_visible
	cmp	r0, #0
	bne	.L26
	b	.L25
.L21:
	cmp	r2, #0
	beq	.L23
	bl	DIALOG_a_dialog_is_visible
	cmp	r0, #0
	ldrne	pc, [sp], #4
.L25:
	ldr	lr, [sp], #4
	b	FDTO_Redraw_Screen
.L23:
	and	r0, r0, #255
	bl	GuiLib_ScrollBox_Redraw
.L26:
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.LFE4:
	.size	FDTO_SCROLL_BOX_redraw_retaining_topline, .-FDTO_SCROLL_BOX_redraw_retaining_topline
	.section	.text.SCROLL_BOX_redraw,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_redraw
	.type	SCROLL_BOX_redraw, %function
SCROLL_BOX_redraw:
.LFB6:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI5:
	mov	r3, #2
	sub	sp, sp, #36
.LCFI6:
	str	r3, [sp, #0]
	ldr	r3, .L28
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L29:
	.align	2
.L28:
	.word	FDTO_SCROLL_BOX_redraw
.LFE6:
	.size	SCROLL_BOX_redraw, .-SCROLL_BOX_redraw
	.section	.text.SCROLL_BOX_redraw_line,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_redraw_line
	.type	SCROLL_BOX_redraw_line, %function
SCROLL_BOX_redraw_line:
.LFB7:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI7:
	mov	r3, #3
	sub	sp, sp, #36
.LCFI8:
	str	r3, [sp, #0]
	ldr	r3, .L31
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	str	r1, [sp, #28]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L32:
	.align	2
.L31:
	.word	GuiLib_ScrollBox_RedrawLine
.LFE7:
	.size	SCROLL_BOX_redraw_line, .-SCROLL_BOX_redraw_line
	.section	.text.SCROLL_BOX_up_or_down,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_up_or_down
	.type	SCROLL_BOX_up_or_down, %function
SCROLL_BOX_up_or_down:
.LFB8:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI9:
	ldr	r2, .L36
	sub	sp, sp, #36
.LCFI10:
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L36+4
	cmp	r1, #4
	cmpne	r1, #84
	movne	r3, r2
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L37:
	.align	2
.L36:
	.word	FDTO_SCROLL_BOX_down
	.word	FDTO_SCROLL_BOX_up
.LFE8:
	.size	SCROLL_BOX_up_or_down, .-SCROLL_BOX_up_or_down
	.section	.text.SCROLL_BOX_to_line,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_to_line
	.type	SCROLL_BOX_to_line, %function
SCROLL_BOX_to_line:
.LFB9:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI11:
	mov	r3, #3
	sub	sp, sp, #36
.LCFI12:
	str	r3, [sp, #0]
	ldr	r3, .L39
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	str	r1, [sp, #28]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L40:
	.align	2
.L39:
	.word	FDTO_SCROLL_BOX_to_line
.LFE9:
	.size	SCROLL_BOX_to_line, .-SCROLL_BOX_to_line
	.section	.text.SCROLL_BOX_close_all_scroll_boxes,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_close_all_scroll_boxes
	.type	SCROLL_BOX_close_all_scroll_boxes, %function
SCROLL_BOX_close_all_scroll_boxes:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI13:
	mov	r4, #0
.L43:
	mov	r0, r4
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	cmp	r0, #0
	blt	.L42
	mov	r0, r4
	bl	GuiLib_ScrollBox_Close
.L42:
	add	r4, r4, #1
	cmp	r4, #3
	bne	.L43
	ldr	r2, .L45
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L45+4
	str	r3, [r2, #0]
	ldmfd	sp!, {r4, pc}
.L46:
	.align	2
.L45:
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	GuiVar_ScrollBoxHorizScrollMarker
.LFE10:
	.size	SCROLL_BOX_close_all_scroll_boxes, .-SCROLL_BOX_close_all_scroll_boxes
	.section	.text.SCROLL_BOX_toggle_scroll_box_automatic_redraw,"ax",%progbits
	.align	2
	.global	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.type	SCROLL_BOX_toggle_scroll_box_automatic_redraw, %function
SCROLL_BOX_toggle_scroll_box_automatic_redraw:
.LFB11:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI14:
	mov	r3, #2
	sub	sp, sp, #36
.LCFI15:
	str	r3, [sp, #0]
	ldr	r3, .L48
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L49:
	.align	2
.L48:
	.word	FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw
.LFE11:
	.size	SCROLL_BOX_toggle_scroll_box_automatic_redraw, .-SCROLL_BOX_toggle_scroll_box_automatic_redraw
	.section	.bss.g_SCROLL_BOX_prevent_redraw,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_SCROLL_BOX_prevent_redraw, %object
	.size	g_SCROLL_BOX_prevent_redraw, 4
g_SCROLL_BOX_prevent_redraw:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI5-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI7-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI9-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI11-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI13-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI14-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE22:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/scrollbox.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x122
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF11
	.byte	0x1
	.4byte	.LASF12
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x4c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x78
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.byte	0xa3
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x11b
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF3
	.byte	0x1
	.byte	0xcc
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x6
	.4byte	0x29
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x6
	.4byte	0x31
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xf8
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x134
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST5
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x13f
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST6
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x161
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST7
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x176
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST8
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x182
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST9
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x1b8
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST10
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB6
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI6
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB7
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI8
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB9
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI12
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB10
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB11
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI15
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x74
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"FDTO_SCROLL_BOX_up\000"
.LASF2:
	.ascii	"FDTO_SCROLL_BOX_toggle_scroll_box_automatic_redraw\000"
.LASF12:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/s"
	.ascii	"crollbox.c\000"
.LASF7:
	.ascii	"SCROLL_BOX_up_or_down\000"
.LASF10:
	.ascii	"SCROLL_BOX_toggle_scroll_box_automatic_redraw\000"
.LASF11:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"FDTO_SCROLL_BOX_redraw\000"
.LASF4:
	.ascii	"FDTO_SCROLL_BOX_redraw_retaining_topline\000"
.LASF0:
	.ascii	"FDTO_SCROLL_BOX_down\000"
.LASF13:
	.ascii	"FDTO_SCROLL_BOX_to_line\000"
.LASF9:
	.ascii	"SCROLL_BOX_close_all_scroll_boxes\000"
.LASF5:
	.ascii	"SCROLL_BOX_redraw\000"
.LASF6:
	.ascii	"SCROLL_BOX_redraw_line\000"
.LASF8:
	.ascii	"SCROLL_BOX_to_line\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
