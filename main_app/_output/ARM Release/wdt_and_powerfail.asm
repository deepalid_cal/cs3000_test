	.file	"wdt_and_powerfail.c"
	.text
.Ltext0:
	.section	.text.powerfail_isr,"ax",%progbits
	.align	2
	.type	powerfail_isr, %function
powerfail_isr:
.LFB1:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L3
	stmfd	sp!, {r0, r1, lr}
.LCFI0:
	ldr	r2, [r3, #0]
	add	r1, sp, #8
	bic	r2, r2, #8388608
	str	r2, [r3, #0]
	mov	r2, #26
	str	r2, [r1, #-8]!
	ldr	r2, .L3+4
	mov	r3, #0
	ldr	r0, [r2, #0]
	mov	r1, sp
	add	r2, sp, #4
	str	r3, [sp, #4]
	bl	xQueueGenericSendFromISR
	ldr	r3, [sp, #4]
	cmp	r3, #1
	bne	.L1
.LBB5:
	bl	vTaskSwitchContext
.L1:
.LBE5:
	ldmfd	sp!, {r2, r3, pc}
.L4:
	.align	2
.L3:
	.word	1073807360
	.word	.LANCHOR0
.LFE1:
	.size	powerfail_isr, .-powerfail_isr
	.section	.text.init_restart_info,"ax",%progbits
	.align	2
	.global	init_restart_info
	.type	init_restart_info, %function
init_restart_info:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	mov	r6, r0
	beq	.L6
	ldr	r4, .L27
	ldr	r1, .L27+4
	mov	r0, r4
	mov	r2, #12
	bl	strncmp
	subs	r5, r0, #0
	beq	.L7
.L6:
	ldr	r0, .L27+8
	mov	r1, r6
	bl	Alert_battery_backed_var_initialized
	mov	r1, #0
	mov	r2, #300
	ldr	r0, .L27
	bl	memset
	ldr	r0, .L27
	ldr	r1, .L27+4
	mov	r2, #16
	bl	strlcpy
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, pc}
.L7:
.LBB6:
	ldr	r3, [r4, #92]
	cmp	r3, #0
	beq	.L9
	ldr	r0, .L27+12
	add	r1, r4, #150
	add	r2, r4, #102
	bl	Alert_Message_va
	mov	r1, r5
	mov	r2, #48
	add	r0, r4, #150
	str	r5, [r4, #92]
	bl	memset
	add	r0, r4, #102
	mov	r1, r5
	mov	r2, #48
	bl	memset
.L9:
	ldr	r4, .L27
	ldr	r3, [r4, #200]
	cmp	r3, #0
	beq	.L10
	mov	r5, #0
	ldr	r0, .L27+16
	add	r1, r4, #234
	add	r2, r4, #210
	bl	Alert_Message_va
	mov	r1, r5
	mov	r2, #64
	add	r0, r4, #234
	str	r5, [r4, #200]
	bl	memset
	add	r0, r4, #210
	mov	r1, r5
	mov	r2, #24
	bl	memset
.L10:
	ldr	r4, .L27
	ldr	r3, .L27+20
	ldr	r2, [r4, #68]
	ldr	r3, [r3, #28]
	cmp	r2, #1
	and	r3, r3, #1
	bne	.L11
	ldr	r0, [r4, #80]
	cmp	r0, #26
	bne	.L12
	cmp	r3, #0
	add	r0, r4, #72
	ldr	r1, [r4, #64]
	beq	.L13
	bl	Alert_power_fail_brownout_idx
	b	.L14
.L13:
	bl	Alert_power_fail_idx
	b	.L14
.L12:
	bl	Alert_reboot_request
	ldr	r3, [r4, #80]
	sub	r3, r3, #27
	cmp	r3, #14
	ldrls	pc, [pc, r3, asl #2]
	b	.L14
.L21:
	.word	.L15
	.word	.L16
	.word	.L17
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L18
	.word	.L19
	.word	.L20
.L16:
	ldr	r0, .L27+24
	bl	Alert_Message
	ldr	r3, .L27+28
	mov	r2, #0
	strb	r2, [r3, #129]
	mov	r2, #1
	b	.L26
.L17:
	ldr	r0, .L27+32
	b	.L25
.L15:
	ldr	r0, .L27+36
.L25:
	bl	Alert_Message
	ldr	r3, .L27+28
	mov	r2, #1
	strb	r2, [r3, #129]
	mov	r2, #0
.L26:
	strb	r2, [r3, #128]
	b	.L14
.L18:
	ldr	r0, .L27+40
	bl	Alert_Message
	ldr	r3, .L27+28
	mov	r2, #1
	str	r2, [r3, #136]
	b	.L14
.L19:
	ldr	r0, .L27+44
	bl	Alert_Message
	mov	r2, #1
	ldr	r3, .L27+28
	b	.L24
.L20:
	ldr	r0, .L27+48
	bl	Alert_Message
	ldr	r3, .L27+28
	mov	r2, #1
	str	r2, [r3, #136]
.L24:
	str	r2, [r3, #140]
	b	.L14
.L11:
	cmp	r3, #1
	bne	.L22
	bl	Alert_watchdog_timeout
	b	.L14
.L22:
	ldr	r0, .L27+52
	bl	Alert_Message
.L14:
	ldr	r4, .L27
	bl	Alert_divider_large
	mov	r3, #0
	add	r0, r4, #16
	str	r3, [r4, #68]
	str	r3, [r4, #80]
	str	r3, [r4, #88]
	str	r3, [r4, #84]
	bl	strlen
	ldr	r1, .L27+56
	mov	r2, r0
	add	r0, r4, #16
	bl	strncmp
	cmp	r0, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	add	r0, r4, #16
	ldr	r1, .L27+56
	ldr	r2, [r4, #64]
	bl	Alert_firmware_update_idx
	mov	r0, #1
.LBE6:
	ldmfd	sp!, {r4, r5, r6, pc}
.L28:
	.align	2
.L27:
	.word	restart_info
	.word	.LANCHOR1
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	1073987584
	.word	.LC3
	.word	weather_preserves
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	-2147483584
.LFE0:
	.size	init_restart_info, .-init_restart_info
	.section	.text.SYSTEM_application_requested_restart,"ax",%progbits
	.align	2
	.global	SYSTEM_application_requested_restart
	.type	SYSTEM_application_requested_restart, %function
SYSTEM_application_requested_restart:
.LFB3:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L31
	stmfd	sp!, {r0, lr}
.LCFI2:
	add	r1, sp, #4
	str	r0, [r1, #-4]!
	mvn	r2, #0
	ldr	r0, [r3, #0]
	mov	r1, sp
	mov	r3, #0
	bl	xQueueGenericSend
.L30:
	b	.L30
.L32:
	.align	2
.L31:
	.word	.LANCHOR0
.LFE3:
	.size	SYSTEM_application_requested_restart, .-SYSTEM_application_requested_restart
	.section	.text.system_shutdown_task,"ax",%progbits
	.align	2
	.global	system_shutdown_task
	.type	system_shutdown_task, %function
system_shutdown_task:
.LFB4:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, lr}
.LCFI3:
	ldr	r4, .L45
	mov	r1, #4
	mov	r2, #0
	mov	r0, #10
	bl	xQueueGenericCreate
	ldr	r5, .L45+4
	str	r0, [r4, #0]
.LBB7:
	mov	r0, #87
	bl	xDisable_ISR
	mov	r1, #0
	mov	r2, #1
	ldr	r3, .L45+8
	mov	r0, #87
	str	r1, [sp, #0]
	bl	xSetISR_Vector
	mov	r0, #87
	bl	xEnable_ISR
.L42:
.LBE7:
	ldr	r0, [r4, #0]
	add	r1, sp, #4
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	bne	.L34
.LBB8:
	ldr	r3, .L45+12
	mov	r2, #0
	str	r2, [r3, #8]
	sub	r3, r3, #81920
	mov	r2, #16384
	str	r2, [r3, #8]
	mov	r2, #524288
	str	r2, [r3, #8]
	mov	r2, #4194304
	str	r2, [r3, #8]
	bl	SERIAL_add_daily_stats_to_monthly_battery_backed
	ldr	r3, .L45+16
	ldr	r0, [r3, #0]
	cmp	r0, #0
	beq	.L35
	bl	vTaskSuspend
.L35:
	ldr	r3, .L45+20
	ldr	r0, [r3, #0]
	cmp	r0, #0
	beq	.L36
	bl	vTaskSuspend
.L36:
	ldr	r3, .L45+24
	ldr	r0, [r3, #0]
	cmp	r0, #0
	beq	.L37
	bl	vTaskSuspend
.L37:
	ldr	r7, .L45+28
	ldr	r6, .L45+32
	ldr	r5, .L45+36
.LBE8:
	mov	r4, #0
.L39:
.LBB9:
	ldr	r0, [r4, r7]
	cmp	r0, #0
	beq	.L38
	ldr	r3, [r6, #0]
	cmp	r0, r3
	beq	.L38
	ldr	r3, [r5, #0]
	cmp	r0, r3
	beq	.L38
	bl	vTaskSuspend
.L38:
	add	r4, r4, #4
	cmp	r4, #96
	bne	.L39
	ldr	r4, .L45+40
	mov	r3, #1
	str	r3, [r4, #68]
	add	r0, r4, #72
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, [sp, #4]
	str	r3, [r4, #80]
	mov	r4, #30
.L40:
	mov	r0, #2
	bl	vTaskDelay
	subs	r4, r4, #1
	bne	.L40
	ldr	r3, .L45+12
	ldr	r2, [r3, #16]
	sub	r2, r2, #2
	str	r2, [r3, #8]
.L41:
	b	.L41
.L34:
.LBE9:
	bl	xTaskGetTickCount
	str	r0, [r5, #0]
	b	.L42
.L46:
	.align	2
.L45:
	.word	.LANCHOR0
	.word	pwrfail_task_last_x_stamp
	.word	powerfail_isr
	.word	1073987584
	.word	timer_task_handle
	.word	epson_rtc_task_handle
	.word	wdt_task_handle
	.word	created_task_handles
	.word	flash_storage_0_task_handle
	.word	flash_storage_1_task_handle
	.word	restart_info
.LFE4:
	.size	system_shutdown_task, .-system_shutdown_task
	.global	__udivsi3
	.section	.text.wdt_monitoring_task,"ax",%progbits
	.align	2
	.global	wdt_monitoring_task
	.type	wdt_monitoring_task, %function
wdt_monitoring_task:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI4:
	ldr	r4, .L66
	mov	r1, #1
	mov	r5, #0
	mov	r0, #17
	bl	clkpwr_clk_en_dis
	str	r5, [r4, #4]
	mov	r0, #5
	bl	clkpwr_get_base_clock_rate
	mov	r3, #10
	mul	r3, r0, r3
	str	r3, [r4, #16]
	mov	r3, #20
	str	r3, [r4, #12]
	ldr	r3, .L66+4
	str	r3, [r4, #24]
	mov	r3, #32
	str	r3, [r4, #20]
	mov	r3, #5
	str	r3, [r4, #4]
	bl	xTaskGetTickCount
	ldr	r3, .L66+8
	mov	r4, r5
	ldr	r5, .L66+12
	str	r0, [r3, #0]
	bl	xTaskGetTickCount
	ldr	r3, .L66+16
	str	r0, [r3, #0]
.L48:
	bl	xTaskGetTickCount
	str	r0, [r5, r4]
	add	r4, r4, #4
	cmp	r4, #96
	bne	.L48
	bl	xTaskGetTickCount
	ldr	r3, .L66+20
	ldr	r2, .L66+24
	mov	r7, #0
	str	r7, [r2, #0]
	str	r0, [r3, #0]
.L65:
.LBB10:
	mov	r0, #100
	bl	vTaskDelay
	bl	xTaskGetTickCount
	ldr	r3, .L66+20
	ldr	r5, [r3, #0]
	cmp	r0, r5
	mov	r4, r0
	bls	.L50
	ldr	r6, .L66+24
	rsb	r5, r5, r0
	mov	r1, #5
	ldr	r0, [r6, #0]
	bl	__udivsi3
	cmp	r5, r0
	bls	.L50
	ldr	r3, .L66+28
	add	r1, r5, r5, asl #2
	cmp	r1, r3
	str	r1, [r6, #0]
	bls	.L50
	ldr	r0, .L66+32
	bl	Alert_Message_va
.L50:
	ldr	r3, .L66+20
	cmp	r7, #5
	str	r4, [r3, #0]
	bhi	.L51
	add	r7, r7, #1
	cmp	r7, #6
	bne	.L52
	mov	r0, #5
	bl	clkpwr_get_base_clock_rate
	ldr	r3, .L66
	mov	r0, r0, asl #3
	str	r0, [r3, #16]
.L51:
	ldr	r6, .L66+36
.LBB11:
	ldr	r9, .L66+12
.LBE11:
.LBE10:
	mov	r5, #0
	mov	r8, #1
.L55:
.LBB13:
.LBB12:
	ldr	r3, [r6, #0]
	cmp	r3, #0
	beq	.L53
	ldr	r3, .L66+40
	ldr	fp, [r5, r3]
	cmp	fp, #0
	beq	.L53
	ldr	r3, [r6, #4]
	cmp	r3, #1
	bne	.L53
	ldr	sl, [r5, r9]
	cmp	r4, sl
	strcc	r4, [r5, r9]
	bcc	.L53
	ldr	r0, [r6, #8]
	mov	r1, #5
	bl	__udivsi3
	rsb	sl, sl, r4
	cmp	sl, r0
	bls	.L53
	mov	r0, fp
	bl	pcTaskGetTaskName
	add	r1, sl, sl, asl #2
	bl	Alert_task_frozen
	mov	r8, #0
.L53:
	add	r5, r5, #4
	cmp	r5, #96
	add	r6, r6, #32
	bne	.L55
	ldr	r3, .L66+8
	ldr	r5, [r3, #0]
	cmp	r4, r5
	strcc	r4, [r3, #0]
	bcc	.L57
	rsb	r5, r5, r4
	cmp	r5, #200
	bls	.L57
	ldr	r3, .L66+44
	mov	r8, #0
	ldr	r0, [r3, #0]
	bl	pcTaskGetTaskName
	add	r1, r5, r5, asl #2
	bl	Alert_task_frozen
.L57:
	ldr	r3, .L66+16
	ldr	r2, [r3, #0]
	cmp	r4, r2
	strcc	r4, [r3, #0]
	bcc	.L59
	rsb	r4, r2, r4
	cmp	r4, #200
	bls	.L59
	ldr	r3, .L66+48
	ldr	r0, [r3, #0]
	bl	pcTaskGetTaskName
	add	r1, r4, r4, asl #2
	bl	Alert_task_frozen
	b	.L65
.L59:
.LBE12:
	cmp	r8, #0
	beq	.L65
.L52:
	ldr	r3, .L66
	mov	r2, #0
	str	r2, [r3, #8]
	b	.L65
.L67:
	.align	2
.L66:
	.word	1073987584
	.word	65535
	.word	rtc_task_last_x_stamp
	.word	task_last_execution_stamp
	.word	pwrfail_task_last_x_stamp
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	750
	.word	.LC10
	.word	Task_Table
	.word	created_task_handles
	.word	epson_rtc_task_handle
	.word	powerfail_task_handle
.LBE13:
.LFE5:
	.size	wdt_monitoring_task, .-wdt_monitoring_task
	.section	.text.freeze_and_restart_app,"ax",%progbits
	.align	2
	.global	freeze_and_restart_app
	.type	freeze_and_restart_app, %function
freeze_and_restart_app:
.LFB6:
	@ Volatile: function does not return.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.L69:
	b	.L69
.LFE6:
	.size	freeze_and_restart_app, .-freeze_and_restart_app
	.section	.text.undefined_instruction_handler,"ax",%progbits
	.align	2
	.global	undefined_instruction_handler
	.type	undefined_instruction_handler, %function
undefined_instruction_handler:
.LFB7:
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
@ 1034 "C:/CS3000/cs3_branches/chain_sync/main_app/src/wdt_and_powerfail/wdt_and_powerfail.c" 1
	sub lr, lr, #8
mov r5, lr
@ 0 "" 2
	ldr	r4, .L72
	mov	r3, #1
	add	r0, r4, #96
	str	r3, [r4, #92]
	bl	EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	ldr	r3, [r5, #0]
	mov	r1, #48
	ldr	r2, .L72+4
	str	r3, [sp, #0]
	add	r0, r4, #150
	mov	r3, r5
	bl	snprintf
	mov	r0, #0
	bl	pcTaskGetTaskName
	mov	r1, #48
	ldr	r2, .L72+8
	mov	r3, r0
	add	r0, r4, #102
	bl	snprintf
.L71:
	b	.L71
.L73:
	.align	2
.L72:
	.word	restart_info
	.word	.LC11
	.word	.LC12
.LFE7:
	.size	undefined_instruction_handler, .-undefined_instruction_handler
	.section	.text.prefetch_abort_handler,"ax",%progbits
	.align	2
	.global	prefetch_abort_handler
	.type	prefetch_abort_handler, %function
prefetch_abort_handler:
.LFB8:
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
@ 1077 "C:/CS3000/cs3_branches/chain_sync/main_app/src/wdt_and_powerfail/wdt_and_powerfail.c" 1
	sub lr, lr, #8
mov r5, lr
@ 0 "" 2
	ldr	r4, .L76
	mov	r3, #1
	add	r0, r4, #96
	str	r3, [r4, #92]
	bl	EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	ldr	r3, [r5, #0]
	mov	r1, #48
	ldr	r2, .L76+4
	str	r3, [sp, #0]
	add	r0, r4, #150
	mov	r3, r5
	bl	snprintf
	mov	r0, #0
	bl	pcTaskGetTaskName
	mov	r1, #48
	ldr	r2, .L76+8
	mov	r3, r0
	add	r0, r4, #102
	bl	snprintf
.L75:
	b	.L75
.L77:
	.align	2
.L76:
	.word	restart_info
	.word	.LC13
	.word	.LC12
.LFE8:
	.size	prefetch_abort_handler, .-prefetch_abort_handler
	.section	.text.data_abort_handler,"ax",%progbits
	.align	2
	.global	data_abort_handler
	.type	data_abort_handler, %function
data_abort_handler:
.LFB9:
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
@ 1120 "C:/CS3000/cs3_branches/chain_sync/main_app/src/wdt_and_powerfail/wdt_and_powerfail.c" 1
	sub lr, lr, #8
mov r5, lr
@ 0 "" 2
	ldr	r4, .L80
	mov	r3, #1
	add	r0, r4, #96
	str	r3, [r4, #92]
	bl	EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	ldr	r3, [r5, #0]
	mov	r1, #48
	ldr	r2, .L80+4
	str	r3, [sp, #0]
	add	r0, r4, #150
	mov	r3, r5
	bl	snprintf
	mov	r0, #0
	bl	pcTaskGetTaskName
	mov	r1, #48
	ldr	r2, .L80+8
	mov	r3, r0
	add	r0, r4, #102
	bl	snprintf
.L79:
	b	.L79
.L81:
	.align	2
.L80:
	.word	restart_info
	.word	.LC14
	.word	.LC12
.LFE9:
	.size	data_abort_handler, .-data_abort_handler
	.section	.text.__assert,"ax",%progbits
	.align	2
	.global	__assert
	.type	__assert, %function
__assert:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, lr}
.LCFI5:
	ldr	r4, .L84
	mov	r6, r1
	mov	r3, #1
	mov	r5, r0
	add	r0, r4, #204
	mov	r7, r2
	str	r3, [r4, #200]
	bl	EXCEPTION_USE_ONLY_obtain_latest_time_and_date
	mov	r0, r6
	bl	RemovePathFromFileName
	mov	r1, #64
	ldr	r2, .L84+4
	mov	r3, r5
	stmia	sp, {r0, r7}
	add	r0, r4, #234
	bl	snprintf
	mov	r0, #0
	bl	pcTaskGetTaskName
	mov	r1, #24
	ldr	r2, .L84+8
	mov	r3, r0
	add	r0, r4, #210
	bl	snprintf
.L83:
	b	.L83
.L85:
	.align	2
.L84:
	.word	restart_info
	.word	.LC15
	.word	.LC12
.LFE10:
	.size	__assert, .-__assert
	.section	.bss.greatest_diff_ms.8136,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	greatest_diff_ms.8136, %object
	.size	greatest_diff_ms.8136, 4
greatest_diff_ms.8136:
	.space	4
	.section	.rodata.RESTART_INFO_VERIFY_STRING_PRE,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	RESTART_INFO_VERIFY_STRING_PRE, %object
	.size	RESTART_INFO_VERIFY_STRING_PRE, 12
RESTART_INFO_VERIFY_STRING_PRE:
	.ascii	"RESTART v03\000"
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Restart Info\000"
.LC1:
	.ascii	"EXCEPTION: %s (%s)\000"
.LC2:
	.ascii	"%s (%s)\000"
.LC3:
	.ascii	"Factory Reset for NEW panel\000"
.LC4:
	.ascii	"Factory Reset for OLD panel\000"
.LC5:
	.ascii	"Factory Reset by user KEYPAD\000"
.LC6:
	.ascii	"Reset: HUB set to distribute tpmicro binary\000"
.LC7:
	.ascii	"Reset: HUB set to distribute main binary\000"
.LC8:
	.ascii	"Reset: HUB to distribute both binaries\000"
.LC9:
	.ascii	"DIRTY POWER or RESET BUTTON PUSHED\000"
.LC10:
	.ascii	"New Monitoring Delta: %u ms\000"
.LC11:
	.ascii	"Undef Instr at 0x%08lX 0x%08lX\000"
.LC12:
	.ascii	"%s\000"
.LC13:
	.ascii	"Prefetch Abort at 0x%08lX 0x%08lX\000"
.LC14:
	.ascii	"Data Abort at 0x%08lX 0x%08lX\000"
.LC15:
	.ascii	"ASSERT: %s, %s, %d\000"
	.section	.bss.last_x_stamp.8135,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	last_x_stamp.8135, %object
	.size	last_x_stamp.8135, 4
last_x_stamp.8135:
	.space	4
	.section	.bss.SYSTEM_shutdown_event_queue,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	SYSTEM_shutdown_event_queue, %object
	.size	SYSTEM_shutdown_event_queue, 4
SYSTEM_shutdown_event_queue:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI5-.LFB10
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE18:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/wdt_and_powerfail/wdt_and_powerfail.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xf3
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF9
	.byte	0x1
	.4byte	.LASF10
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x174
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x148
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x38
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x17f
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x18e
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x26e
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x3ff
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x406
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x431
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x45c
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x48a
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB10
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x64
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF10:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/wdt_"
	.ascii	"and_powerfail/wdt_and_powerfail.c\000"
.LASF6:
	.ascii	"prefetch_abort_handler\000"
.LASF7:
	.ascii	"data_abort_handler\000"
.LASF2:
	.ascii	"system_shutdown_task\000"
.LASF4:
	.ascii	"freeze_and_restart_app\000"
.LASF9:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"undefined_instruction_handler\000"
.LASF1:
	.ascii	"SYSTEM_application_requested_restart\000"
.LASF0:
	.ascii	"init_restart_info\000"
.LASF8:
	.ascii	"__assert\000"
.LASF12:
	.ascii	"powerfail_isr\000"
.LASF11:
	.ascii	"init_powerfail_isr\000"
.LASF3:
	.ascii	"wdt_monitoring_task\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
