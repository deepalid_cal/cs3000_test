	.file	"cs_mem_part.c"
	.text
.Ltext0:
	.section	.text.init_mem_partitioning,"ax",%progbits
	.align	2
	.global	init_mem_partitioning
	.type	init_mem_partitioning, %function
init_mem_partitioning:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
.LBB4:
	ldr	r7, .L11
	ldr	r8, .L11+4
.LBE4:
	mov	r5, #0
	mov	sl, r5
.LBB5:
	mov	r9, #20
.L9:
.LBE5:
	ldr	r4, [r7, #0]
	ldr	r6, [r7, #-4]
	mul	r0, r4, r6
	bl	malloc
	subs	fp, r0, #0
	bne	.L2
	bl	freeze_and_restart_app
.L2:
.LBB6:
	cmp	r4, #0
	beq	.L3
	cmp	r6, #3
	bls	.L3
	cmp	r4, #1
	streq	sl, [fp, #0]
	beq	.L5
	add	r3, fp, r6
	mov	r2, fp
	mov	r1, #0
	sub	r0, r4, #1
	b	.L6
.L7:
	str	r3, [r2, #0]
	add	r1, r1, #1
	mov	r2, r3
	add	r3, r3, r6
.L6:
	cmp	r1, r0
	bcc	.L7
	str	sl, [r2, #0]
.L5:
	mul	r2, r9, r5
	add	r3, r8, r2
	str	fp, [r8, r2]
	str	fp, [r3, #4]
	str	r4, [r3, #16]
	str	r4, [r3, #12]
	str	r6, [r3, #8]
	b	.L8
.L3:
.LBE6:
	bl	freeze_and_restart_app
.L8:
	add	r5, r5, #1
	cmp	r5, #11
	add	r7, r7, #8
	bne	.L9
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L12:
	.align	2
.L11:
	.word	.LANCHOR0+4
	.word	.LANCHOR1
.LFE1:
	.size	init_mem_partitioning, .-init_mem_partitioning
	.section	.text.OSMemGet,"ax",%progbits
	.align	2
	.global	OSMemGet
	.type	OSMemGet, %function
OSMemGet:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, [r0, #16]
	mov	r3, r0
	cmp	r2, #0
	beq	.L14
	ldr	r0, [r0, #4]
	sub	r2, r2, #1
	ldr	ip, [r0, #0]
	str	r2, [r3, #16]
	str	ip, [r3, #4]
	mov	r3, #0
	str	r3, [r1, #0]
	bx	lr
.L14:
	mov	r3, #72
	str	r3, [r1, #0]
	mov	r0, r2
	bx	lr
.LFE2:
	.size	OSMemGet, .-OSMemGet
	.section	.text.OSMemPut,"ax",%progbits
	.align	2
	.global	OSMemPut
	.type	OSMemPut, %function
OSMemPut:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L19
	mov	r3, r0, asl #16
	mov	r3, r3, lsr #16
	mov	r0, #20
	mla	r3, r0, r3, r2
	ldr	r0, [r3, #12]
	ldr	r2, [r3, #16]
	cmp	r2, r0
	ldrcc	r0, [r3, #4]
	addcc	r2, r2, #1
	strcc	r0, [r1, #0]
	movcs	r0, #75
	strcc	r1, [r3, #4]
	strcc	r2, [r3, #16]
	movcc	r0, #0
	bx	lr
.L20:
	.align	2
.L19:
	.word	.LANCHOR1
.LFE3:
	.size	OSMemPut, .-OSMemPut
	.global	OSMemTbl
	.global	partitions
	.section	.rodata.partitions,"a",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	partitions, %object
	.size	partitions, 88
partitions:
	.word	96
	.word	1024
	.word	208
	.word	1024
	.word	560
	.word	256
	.word	1152
	.word	32
	.word	2112
	.word	384
	.word	4352
	.word	32
	.word	8448
	.word	32
	.word	40688
	.word	6
	.word	116416
	.word	2
	.word	147456
	.word	2
	.word	1433628
	.word	1
	.section	.bss.OSMemTbl,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	OSMemTbl, %object
	.size	OSMemTbl, 220
OSMemTbl:
	.space	220
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cs_mem_part.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x62
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0xbe
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0xf8
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x146
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x17b
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"OSMemPut\000"
.LASF0:
	.ascii	"init_mem_partitioning\000"
.LASF1:
	.ascii	"OSMemGet\000"
.LASF5:
	.ascii	"__os_mem_create\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/util"
	.ascii	"s/cs_mem_part.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
