	.file	"hashids.c"
	.text
.Ltext0:
	.global	__umodsi3
	.section	.text.hashids_shuffle.part.0,"ax",%progbits
	.align	2
	.type	hashids_shuffle.part.0, %function
hashids_shuffle.part.0:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI0:
	mov	r8, #0
	mov	r4, r0
	mov	r5, r2
	mov	sl, r3
	sub	r7, r1, #1
	mov	r6, r8
	b	.L2
.L69:
	and	r3, r7, #31
	sub	r3, r3, #1
	cmp	r3, #30
	ldrls	pc, [pc, r3, asl #2]
	b	.L3
.L35:
	.word	.L4
	.word	.L5
	.word	.L6
	.word	.L7
	.word	.L8
	.word	.L9
	.word	.L10
	.word	.L11
	.word	.L12
	.word	.L13
	.word	.L14
	.word	.L15
	.word	.L16
	.word	.L17
	.word	.L18
	.word	.L19
	.word	.L20
	.word	.L21
	.word	.L22
	.word	.L23
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
	.word	.L34
.L34:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L33:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L32:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L31:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L30:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L29:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L28:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L27:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L26:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L25:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L24:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L23:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L22:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L21:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L20:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L19:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L18:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L17:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L16:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L15:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L14:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L13:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L12:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L11:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L10:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L9:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L8:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L7:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L6:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L5:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L4:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	subs	r7, r7, #1
	strb	r9, [r4, r0]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L3:
	cmp	r6, sl
	moveq	r6, #0
	ldrb	r0, [r5, r6]	@ zero_extendqisi2
	mov	r1, r7
	add	r8, r8, r0
	add	r0, r0, r6
	add	r0, r0, r8
	bl	__umodsi3
	ldrb	r9, [r4, r7]	@ zero_extendqisi2
	add	r6, r6, #1
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	strb	r3, [r4, r7]
	sub	r7, r7, #1
	strb	r9, [r4, r0]
.L2:
	cmp	r7, #0
	bne	.L69
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.LFE4:
	.size	hashids_shuffle.part.0, .-hashids_shuffle.part.0
	.global	__udivsi3
	.section	.text.hashids_init3,"ax",%progbits
	.align	2
	.global	hashids_init3
	.type	hashids_init3, %function
hashids_init3:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI1:
	fstmfdd	sp!, {d8}
.LCFI2:
	mov	r5, r0
	mov	r0, r2
	mov	r4, r3
	mov	r6, r1
	mov	r7, r2
	bl	strlen
	ldr	r3, [r4, #0]
	mov	r8, #0
	strb	r8, [r3, #0]
	mov	sl, r8
	add	fp, r0, #1
	b	.L104
.L106:
	ldrb	r9, [r7, sl]	@ zero_extendqisi2
	ldr	r0, [r4, #0]
	mov	r1, r9
	bl	strchr
	add	sl, sl, #1
	cmp	r0, #0
	ldreq	r3, [r4, #0]
	streqb	r9, [r3, r8]
	addeq	r8, r8, #1
.L104:
	cmp	sl, fp
	bcc	.L106
	ldr	r3, [r4, #0]
	mov	r7, #0
	cmp	r8, #15
	strb	r7, [r3, r8]
	str	r8, [r4, #12]
	bhi	.L107
	ldr	r0, .L124+4
	bl	Alert_Message
	mov	r0, r7
	b	.L108
.L107:
	ldr	r0, [r4, #0]
	mov	r1, #32
	bl	strchr
	cmp	r0, #0
	bne	.L109
	ldr	r0, [r4, #0]
	mov	r1, #9
	bl	strchr
	cmp	r0, #0
	beq	.L110
.L109:
	ldr	r0, .L124+8
	bl	Alert_Message
	mov	r0, #0
	b	.L108
.L110:
	cmp	r5, #0
	moveq	r2, r5
	beq	.L111
	mov	r0, r5
	bl	strlen
	mov	r2, r0
.L111:
	mov	r1, r5
	str	r2, [r4, #20]
	ldr	r0, [r4, #16]
	bl	strncpy
	ldr	r0, .L124+12
	bl	strlen
	flds	s12, [r4, #12]	@ int
	flds	s16, .L124
	mov	r5, #0
	mov	r7, r5
	fuitos	s15, s12
	ldr	r8, .L124+12
	fdivs	s15, s15, s16
	fcvtds	d6, s15
	fmrrd	r0, r1, d6
	bl	ceil
	b	.L112
.L114:
	ldrb	sl, [r8, r7]	@ zero_extendqisi2
	ldr	r0, [r4, #0]
	mov	r1, sl
	bl	strchr
	subs	r9, r0, #0
	beq	.L113
	ldr	r3, [r4, #24]
	strb	sl, [r3, r5]
	ldr	r0, [r4, #0]
	bl	strlen
	ldr	r2, [r4, #0]
	add	r1, r9, #1
	rsb	r2, r9, r2
	add	r5, r5, #1
	add	r2, r0, r2
	mov	r0, r9
	bl	memmove
.L113:
	add	r7, r7, #1
.L112:
	ldr	r0, .L124+12
	bl	strlen
	cmp	r7, r0
	bcc	.L114
	ldr	r3, [r4, #12]
	cmp	r5, #0
	rsb	r3, r5, r3
	str	r5, [r4, #28]
	str	r3, [r4, #12]
	beq	.L115
	ldr	r3, [r4, #20]
	ldr	r0, [r4, #24]
.LBB18:
	cmp	r3, #0
.LBE18:
	ldr	r2, [r4, #16]
.LBB19:
	beq	.L115
	mov	r1, r5
	bl	hashids_shuffle.part.0
.L115:
.LBE19:
	ldr	r3, [r4, #28]
	cmp	r3, #0
	beq	.L116
	flds	s13, [r4, #12]	@ int
	fmsr	s12, r3	@ int
	fuitos	s14, s13
	fuitos	s15, s12
	fdivs	s15, s14, s15
	fcmpes	s15, s16
	fmstat
	ble	.L117
.L116:
.LBB20:
	flds	s13, [r4, #12]	@ int
	fuitos	s15, s13
	fdivs	s16, s15, s16
	fcvtds	d7, s16
	fmrrd	r0, r1, d7
	bl	ceil
	ldr	r5, [r4, #28]
	fmdrr	d6, r0, r1
	ftouizd	s13, d6
	fmrs	r3, s13	@ int
	cmp	r3, #1
	moveq	r3, #2
	cmp	r3, r5
	bls	.L120
.LBB21:
	rsb	r5, r5, r3
	mov	r2, r5
	ldr	r1, [r4, #0]
	ldr	r0, [r4, #24]
	bl	strncat
	ldr	r2, [r4, #12]
	ldr	r0, [r4, #0]
	add	r2, r2, #1
	add	r1, r0, r5
	rsb	r2, r5, r2
	bl	memmove
	ldr	r3, [r4, #28]
	add	r3, r3, r5
	str	r3, [r4, #28]
	ldr	r3, [r4, #12]
	rsb	r5, r5, r3
	str	r5, [r4, #12]
	b	.L117
.L120:
.LBE21:
	ldr	r2, [r4, #24]
	mov	r1, #0
	strb	r1, [r2, r3]
	str	r3, [r4, #28]
.L117:
.LBE20:
	add	r1, r4, #12
	ldmia	r1, {r1, r2, r3}
	ldr	r0, [r4, #0]
.LBB22:
	cmp	r3, #0
	beq	.L121
	bl	hashids_shuffle.part.0
.L121:
.LBE22:
	ldr	r5, [r4, #12]
.LBB23:
	mov	r1, #12
	mov	r0, r5
	bl	__umodsi3
	mov	r1, #12
	adds	r7, r0, #0
	mov	r0, r5
	movne	r7, #1
	bl	__udivsi3
.LBE23:
	ldr	r1, [r4, #0]
.LBB24:
	add	r2, r7, r0
.LBE24:
	str	r2, [r4, #36]
	ldr	r0, [r4, #32]
	bl	strncpy
	ldr	r2, [r4, #12]
	ldr	r3, [r4, #36]
	ldr	r0, [r4, #0]
	add	r2, r2, #1
	rsb	r2, r3, r2
	add	r1, r0, r3
	bl	memmove
	ldr	r2, [r4, #12]
	ldr	r3, [r4, #36]
	mov	r0, r4
	rsb	r3, r3, r2
	str	r3, [r4, #12]
	str	r6, [r4, #40]
.L108:
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L125:
	.align	2
.L124:
	.word	1080033280
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE2:
	.size	hashids_init3, .-hashids_init3
	.global	__umoddi3
	.global	__udivdi3
	.section	.text.hashids_encode,"ax",%progbits
	.align	2
	.global	hashids_encode
	.type	hashids_encode, %function
hashids_encode:
.LFB3:
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI3:
	fstmfdd	sp!, {d8}
.LCFI4:
	mov	r4, r0
	sub	sp, sp, #44
.LCFI5:
	str	r2, [sp, #20]
	mov	r6, r1
	ldr	r2, [r4, #12]
	ldr	r0, [r0, #4]
	ldr	r1, [r4, #0]
	mov	r8, r3
	bl	strncpy
	mov	r2, #0
	mov	r3, #0
	mov	r7, r8
	mov	sl, r8
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	mov	r5, #0
	mov	r8, r2
	mov	r9, r3
	b	.L127
.L128:
	add	r2, r5, #100
	ldmia	r7!, {r0-r1}
	mov	r3, #0
	bl	__umoddi3
	add	r5, r5, #1
	adds	r8, r8, r0
	adc	r9, r9, r1
.L127:
	ldr	r3, [sp, #20]
	cmp	r5, r3
	bne	.L128
	mov	r5, #0
	str	r8, [sp, #8]
	str	r9, [sp, #12]
	ldr	r2, [r4, #12]
	add	r1, sp, #8
	ldmia	r1, {r0-r1}
	mov	r3, r5
	bl	__umoddi3
	ldr	r3, [r4, #0]
	mov	r2, r6
	mov	r8, sl
	ldrb	r3, [r3, r0]	@ zero_extendqisi2
	strb	r3, [r2], #1
	str	r2, [sp, #16]
	ldr	r2, [r4, #8]
	strb	r3, [r2, #0]
	ldr	r3, [r4, #8]
	strb	r5, [r3, #1]
	ldr	r2, [r4, #12]
	ldr	r0, [r4, #8]
	sub	r2, r2, #1
	ldr	r1, [r4, #16]
	bl	strncat
	ldr	r2, [r4, #20]
	ldr	r0, [r4, #8]
	add	r3, r2, #1
	add	r3, r0, r3
	str	r3, [sp, #36]
	ldr	r3, [r4, #12]
	sub	fp, r3, #1
	rsb	fp, r2, fp
	cmp	fp, r5
	strleb	r5, [r0, r3]
	ble	.L130
	ldr	r1, [r4, #0]
	mov	r2, fp
	bl	strncat
.L130:
	ldr	r5, [sp, #16]
	mov	r9, #0
	str	r6, [sp, #40]
	b	.L131
.L138:
	ldr	r3, [r8, #0]
	ldr	r2, [r8, #4]
	cmp	fp, #0
	str	r3, [sp, #28]
	str	r2, [sp, #32]
	add	r8, r8, #8
	ble	.L132
	ldr	r0, [sp, #36]
	ldr	r1, [r4, #4]
	mov	r2, fp
	bl	strncpy
.L132:
	ldr	r1, [r4, #12]
	ldmib	r4, {r0, r2}
.LBB25:
	cmp	r1, #0
	beq	.L133
	mov	r3, r1
	bl	hashids_shuffle.part.0
.L133:
.LBE25:
	ldr	r3, [sp, #28]
	ldr	r2, [sp, #32]
.LBB26:
	mov	sl, r5
.LBE26:
	str	r3, [sp, #0]
	str	r2, [sp, #4]
.L134:
	ldr	r2, [r4, #12]
	ldmia	sp, {r0-r1}
	mov	r3, #0
	bl	__umoddi3
	ldr	r3, [r4, #4]
	add	r6, sl, #1
.LBB27:
	mov	r7, sl
.LBE27:
	ldrb	r3, [r3, r0]	@ zero_extendqisi2
	ldmia	sp, {r0-r1}
	strb	r3, [sl, #0]
	str	r3, [sp, #24]
	ldr	r2, [r4, #12]
	mov	r3, #0
	bl	__udivdi3
	mov	sl, r6
	stmia	sp, {r0, r1}
	ldmia	sp, {r2-r3}
	orrs	r3, r2, r3
	bne	.L134
	rsb	r2, r5, r6
	add	r2, r2, r2, lsr #31
	mov	r2, r2, asr #1
	mov	ip, r6
	mov	r3, #0
	b	.L135
.L136:
	ldrb	r0, [ip, #-1]	@ zero_extendqisi2
	ldrb	r1, [r5, r3]	@ zero_extendqisi2
	strb	r0, [r5, r3]
	add	r3, r3, #1
	strb	r1, [ip, #-1]!
.L135:
	cmp	r3, r2
	bne	.L136
	ldr	r2, [sp, #20]
	add	sl, r9, #1
	cmp	sl, r2
	bcs	.L137
	ldr	r3, [sp, #24]
	ldr	r0, [sp, #28]
	add	r2, r3, r9
	ldr	r1, [sp, #32]
	mov	r3, #0
	bl	__umoddi3
	ldr	r2, [r4, #28]
	mov	r3, #0
	bl	__umoddi3
	ldr	r3, [r4, #24]
	add	r6, r7, #2
	ldrb	r3, [r3, r0]	@ zero_extendqisi2
	strb	r3, [r7, #1]
.L137:
	mov	r5, r6
	mov	r9, sl
.L131:
	ldr	r2, [sp, #20]
	cmp	r9, r2
	bne	.L138
	ldr	r6, [sp, #40]
	ldr	r3, [r4, #40]
	rsb	r5, r6, r5
	cmp	r5, r3
	bcs	.L139
	add	r3, sp, #8
	ldmia	r3, {r2-r3}
	ldrb	r0, [r6, #0]	@ zero_extendqisi2
	mov	r1, #0
	adds	r0, r0, r2
	adc	r1, r1, r3
	ldr	r2, [r4, #36]
	mov	r3, #0
	bl	__umoddi3
	mov	r2, r5
	mov	r1, r6
	mov	r7, r0
	ldr	r0, [sp, #16]
	bl	memmove
	ldr	r3, [r4, #32]
	ldrb	r3, [r3, r7]	@ zero_extendqisi2
	add	r7, r5, #1
	strb	r3, [r6, #0]
	ldr	r3, [r4, #40]
	cmp	r7, r3
	movcs	r5, r7
	bcs	.L139
	add	r3, sp, #8
	ldmia	r3, {r2-r3}
	ldrb	r0, [r6, #2]	@ zero_extendqisi2
	flds	s16, .L146
	mov	r1, #0
	adds	r0, r0, r2
	adc	r1, r1, r3
	ldr	r2, [r4, #36]
	mov	r3, #0
	bl	__umoddi3
	ldr	r3, [r4, #32]
	add	r5, r5, #2
	ldrb	r3, [r3, r0]	@ zero_extendqisi2
	strb	r3, [r6, r7]
	ldr	r3, [r4, #12]
	fmsr	s12, r3	@ int
.LBB28:
	and	r7, r3, #1
	add	r7, r7, r3, lsr #1
.LBE28:
	fuitos	s15, s12
	fmuls	s15, s15, s16
	fcvtds	d6, s15
	fmrrd	r0, r1, d6
	bl	floor
	fmdrr	d7, r0, r1
	ftouizd	s15, d7
	fmrs	r9, s15	@ int
	b	.L140
.L143:
	ldr	r1, [r4, #4]
	ldr	r2, [r4, #12]
	ldr	r0, [r4, #8]
	bl	strncpy
	ldr	r1, [r4, #12]
	ldmib	r4, {r0, r2}
.LBB29:
	cmp	r1, #0
	beq	.L141
	mov	r3, r1
	bl	hashids_shuffle.part.0
.L141:
.LBE29:
	ldr	r3, [r4, #40]
	rsb	r3, r5, r3
	fmsr	s12, r3	@ int
.LBB30:
	and	r8, r3, #1
	add	r8, r8, r3, lsr #1
.LBE30:
	cmp	r8, r7
	movcs	r8, r7
	fuitos	s15, s12
	fmuls	s15, s15, s16
	fcvtds	d6, s15
	fmrrd	r0, r1, d6
	bl	floor
	fmdrr	d7, r0, r1
	ftouizd	s15, d7
	fmrs	sl, s15	@ int
	cmp	sl, r9
	movcs	sl, r9
	add	r3, sl, r8
	tst	r3, #1
	bne	.L142
	ldr	r3, [r4, #12]
	tst	r3, #1
	addne	r8, r8, #1
	subne	sl, sl, #1
.L142:
	mov	r2, r5
	add	r0, r6, r8
	mov	r1, r6
	bl	memmove
	ldr	r3, [r4, #12]
	ldr	r1, [r4, #4]
	rsb	r3, r8, r3
	add	r1, r1, r3
	mov	r2, r8
	mov	r0, r6
	bl	memmove
	add	r0, r8, r5
	add	r0, r6, r0
	ldr	r1, [r4, #4]
	mov	r2, sl
	bl	memmove
	add	r8, r8, sl
	add	r5, r5, r8
.L140:
	ldr	r3, [r4, #40]
	cmp	r5, r3
	bcc	.L143
.L139:
	mov	r3, #0
	strb	r3, [r6, r5]
	mov	r0, r5
	add	sp, sp, #44
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L147:
	.align	2
.L146:
	.word	1056964608
.LFE3:
	.size	hashids_encode, .-hashids_encode
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Hashids: Alphabet is too short\000"
.LC1:
	.ascii	"Hashids: Alphabet contains whitespace characters\000"
.LC2:
	.ascii	"cfhistuCFHISTU\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI0-.LFB4
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/hashids/hashids.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x68
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x48
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x39
	.byte	0x3
	.uleb128 0x3
	.4byte	0x21
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x9f
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x12a
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB4
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI5
	.4byte	.LFE3
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"hashids_shuffle\000"
.LASF1:
	.ascii	"hashids_div_ceil_size_t\000"
.LASF3:
	.ascii	"hashids_encode\000"
.LASF2:
	.ascii	"hashids_init3\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/hashids/"
	.ascii	"hashids.c\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
