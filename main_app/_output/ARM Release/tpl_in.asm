	.file	"tpl_in.c"
	.text
.Ltext0:
	.section	.text.process_im_existence_timer_expired,"ax",%progbits
	.align	2
	.global	process_im_existence_timer_expired
	.type	process_im_existence_timer_expired, %function
process_im_existence_timer_expired:
.LFB6:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI0:
	mov	r3, #1280
	strh	r3, [sp, #0]	@ movhi
	bl	pvTimerGetTimerID
	ldr	r3, .L3
	mov	r2, #0
	mov	r1, sp
	str	r0, [sp, #4]
	ldr	r0, [r3, #0]
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L1
.LBB4:
	ldr	r0, .L3+4
	bl	RemovePathFromFileName
	mov	r2, #308
	mov	r1, r0
	ldr	r0, .L3+8
	bl	Alert_Message_va
.L1:
.LBE4:
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.L4:
	.align	2
.L3:
	.word	TPL_IN_event_queue
	.word	.LC0
	.word	.LC1
.LFE6:
	.size	process_im_existence_timer_expired, .-process_im_existence_timer_expired
	.section	.text.nm_destroy_this_IM,"ax",%progbits
	.align	2
	.global	nm_destroy_this_IM
	.type	nm_destroy_this_IM, %function
nm_destroy_this_IM:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r2, #0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI1:
	mov	r1, #3
	str	r2, [sp, #0]
	mov	r4, r0
	mov	r3, r2
	ldr	r0, [r0, #56]
	bl	xTimerGenericCommand
	cmp	r0, #0
	bne	.L6
	ldr	r0, .L11
	bl	RemovePathFromFileName
	mov	r2, #92
	mov	r1, r0
	ldr	r0, .L11+4
	bl	Alert_Message_va
.L6:
	mov	r2, #0
	ldr	r0, [r4, #60]
	mov	r1, #3
	mov	r3, r2
	str	r2, [sp, #0]
	bl	xTimerGenericCommand
	cmp	r0, #0
	bne	.L7
	ldr	r0, .L11
	bl	RemovePathFromFileName
	mov	r2, #97
	mov	r1, r0
	ldr	r0, .L11+4
	bl	Alert_Message_va
.L7:
	mov	r2, #0
	ldr	r0, [r4, #64]
	mov	r1, #3
	mov	r3, r2
	str	r2, [sp, #0]
	bl	xTimerGenericCommand
	cmp	r0, #0
	bne	.L8
	ldr	r0, .L11
	bl	RemovePathFromFileName
	mov	r2, #102
	mov	r1, r0
	ldr	r0, .L11+4
	bl	Alert_Message_va
.L8:
	mov	r1, #0
	ldr	r0, [r4, #52]
	mvn	r2, #0
	mov	r3, r1
	bl	xQueueGenericReceive
	add	r6, r4, #32
	b	.L9
.L10:
	ldr	r0, [r5, #16]
	ldr	r1, .L11
	mov	r2, #113
	bl	mem_free_debug
	mov	r0, r5
	ldr	r1, .L11
	mov	r2, #115
	bl	mem_free_debug
.L9:
	mov	r0, r6
	ldr	r1, .L11
	mov	r2, #111
	bl	nm_ListRemoveHead_debug
	subs	r5, r0, #0
	bne	.L10
	mov	r1, r5
	mov	r2, r5
	mov	r3, r5
	ldr	r0, [r4, #52]
	bl	xQueueGenericSend
	ldr	r0, [r4, #52]
	bl	vQueueDelete
	mov	r1, r4
	ldr	r2, .L11
	mov	r3, #124
	ldr	r0, .L11+8
	bl	nm_ListRemove_debug
	ldr	r1, .L11
	mov	r0, r4
	mov	r2, #126
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	mem_free_debug
.L12:
	.align	2
.L11:
	.word	.LC0
	.word	.LC2
	.word	.LANCHOR0
.LFE0:
	.size	nm_destroy_this_IM, .-nm_destroy_this_IM
	.section	.text.TPL_IN_destroy_all_incoming_3000_scan_response_and_token_response_messages,"ax",%progbits
	.align	2
	.global	TPL_IN_destroy_all_incoming_3000_scan_response_and_token_response_messages
	.type	TPL_IN_destroy_all_incoming_3000_scan_response_and_token_response_messages, %function
TPL_IN_destroy_all_incoming_3000_scan_response_and_token_response_messages:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L17
	mov	r1, #0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI2:
	ldr	r0, [r3, #0]
	mvn	r2, #0
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, .L17+4
	bl	nm_ListGetFirst
	ldr	r5, .L17+8
	mov	r4, r0
	b	.L14
.L16:
	ldr	r0, .L17+4
	mov	r1, r4
	bl	nm_ListGetNext
	ldr	r3, [r4, #76]
	cmp	r3, r5
	mov	r6, r0
	bne	.L15
	ldr	r3, [r4, #80]
	cmp	r3, #4
	cmpne	r3, #6
	bne	.L15
	mov	r0, r4
	bl	nm_destroy_this_IM
.L15:
	mov	r4, r6
.L14:
	cmp	r4, #0
	bne	.L16
	ldr	r3, .L17
	mov	r1, r4
	ldr	r0, [r3, #0]
	mov	r2, r4
	mov	r3, r4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGenericSend
.L18:
	.align	2
.L17:
	.word	list_tpl_in_messages_MUTEX
	.word	.LANCHOR0
	.word	3000
.LFE1:
	.size	TPL_IN_destroy_all_incoming_3000_scan_response_and_token_response_messages, .-TPL_IN_destroy_all_incoming_3000_scan_response_and_token_response_messages
	.section	.text.nm_TPL_IN_destroy_all_incoming_3000_scan_and_token_messages,"ax",%progbits
	.align	2
	.global	nm_TPL_IN_destroy_all_incoming_3000_scan_and_token_messages
	.type	nm_TPL_IN_destroy_all_incoming_3000_scan_and_token_messages, %function
nm_TPL_IN_destroy_all_incoming_3000_scan_and_token_messages:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI3:
	ldr	r0, .L23
	bl	nm_ListGetFirst
	ldr	r5, .L23+4
	mov	r4, r0
	b	.L20
.L22:
	ldr	r0, .L23
	mov	r1, r4
	bl	nm_ListGetNext
	ldr	r3, [r4, #76]
	cmp	r3, r5
	mov	r6, r0
	bne	.L21
	ldr	r3, [r4, #80]
	cmp	r3, #3
	cmpne	r3, #5
	bne	.L21
	mov	r0, r4
	bl	nm_destroy_this_IM
.L21:
	mov	r4, r6
.L20:
	cmp	r4, #0
	bne	.L22
	ldmfd	sp!, {r4, r5, r6, pc}
.L24:
	.align	2
.L23:
	.word	.LANCHOR0
	.word	3000
.LFE2:
	.size	nm_TPL_IN_destroy_all_incoming_3000_scan_and_token_messages, .-nm_TPL_IN_destroy_all_incoming_3000_scan_and_token_messages
	.section	.text.nm_TPL_IN_destroy_all_incoming_2000_comm_test_messages,"ax",%progbits
	.align	2
	.global	nm_TPL_IN_destroy_all_incoming_2000_comm_test_messages
	.type	nm_TPL_IN_destroy_all_incoming_2000_comm_test_messages, %function
nm_TPL_IN_destroy_all_incoming_2000_comm_test_messages:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI4:
	ldr	r0, .L29
	bl	nm_ListGetFirst
	mov	r4, r0
	b	.L26
.L28:
	ldr	r0, .L29
	mov	r1, r4
	bl	nm_ListGetNext
	ldr	r3, [r4, #76]
	cmp	r3, #2000
	mov	r5, r0
	bne	.L27
	ldr	r3, [r4, #80]
	sub	r3, r3, #2
	cmp	r3, #1
	bhi	.L27
	mov	r0, r4
	bl	nm_destroy_this_IM
.L27:
	mov	r4, r5
.L26:
	cmp	r4, #0
	bne	.L28
	ldmfd	sp!, {r4, r5, pc}
.L30:
	.align	2
.L29:
	.word	.LANCHOR0
.LFE3:
	.size	nm_TPL_IN_destroy_all_incoming_2000_comm_test_messages, .-nm_TPL_IN_destroy_all_incoming_2000_comm_test_messages
	.section	.text.nm_MIDOnTheListOfIncomingMessages,"ax",%progbits
	.align	2
	.global	nm_MIDOnTheListOfIncomingMessages
	.type	nm_MIDOnTheListOfIncomingMessages, %function
nm_MIDOnTheListOfIncomingMessages:
.LFB4:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, lr}
.LCFI5:
	strh	r0, [sp, #0]	@ movhi
	ldr	r0, .L36
	bl	nm_ListGetFirst
	b	.L35
.L34:
	ldrh	r2, [r1, #12]
	ldrh	r3, [sp, #0]
	cmp	r2, r3
	beq	.L33
	ldr	r0, .L36
	bl	nm_ListGetNext
.L35:
	cmp	r0, #0
	mov	r1, r0
	bne	.L34
.L33:
	mov	r0, r1
	ldmfd	sp!, {r3, pc}
.L37:
	.align	2
.L36:
	.word	.LANCHOR0
.LFE4:
	.size	nm_MIDOnTheListOfIncomingMessages, .-nm_MIDOnTheListOfIncomingMessages
	.section	.text.start_existence_timer,"ax",%progbits
	.align	2
	.global	start_existence_timer
	.type	start_existence_timer, %function
start_existence_timer:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI6:
	ldr	r4, [r0, #56]
	bl	xTaskGetTickCount
	mvn	r3, #0
	mov	r1, #0
	str	r3, [sp, #0]
	mov	r3, r1
	mov	r2, r0
	mov	r0, r4
	bl	xTimerGenericCommand
	ldmfd	sp!, {r3, r4, pc}
.LFE5:
	.size	start_existence_timer, .-start_existence_timer
	.section	.text.tpl_in_existence_timer_expired,"ax",%progbits
	.align	2
	.global	tpl_in_existence_timer_expired
	.type	tpl_in_existence_timer_expired, %function
tpl_in_existence_timer_expired:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L42
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	ldr	r2, [r3, #20]
	ldr	r5, .L42+4
	mov	r1, #0
	add	r2, r2, #1
	str	r2, [r3, #20]
	mov	r4, r0
	mov	r3, r1
	mvn	r2, #0
	ldr	r0, [r5, #0]
	bl	xQueueGenericReceive
	ldr	r0, .L42+8
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	bne	.L40
	ldr	r0, .L42+12
	ldr	r1, .L42+16
	ldr	r2, .L42+20
	ldr	r3, .L42+24
	bl	Alert_item_not_on_list_with_filename
	b	.L41
.L40:
	mov	r0, r4
	bl	nm_destroy_this_IM
.L41:
	ldr	r0, [r5, #0]
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGenericSend
.L43:
	.align	2
.L42:
	.word	.LANCHOR1
	.word	list_tpl_in_messages_MUTEX
	.word	.LANCHOR0
	.word	.LC3
	.word	.LC4
	.word	.LC0
	.word	326
.LFE7:
	.size	tpl_in_existence_timer_expired, .-tpl_in_existence_timer_expired
	.section	.text.im_StopBothPacketTimers,"ax",%progbits
	.align	2
	.global	im_StopBothPacketTimers
	.type	im_StopBothPacketTimers, %function
im_StopBothPacketTimers:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI8:
	mov	r2, #0
	mvn	r5, #0
	mov	r3, r2
	mov	r4, r0
	str	r5, [sp, #0]
	mov	r1, #1
	ldr	r0, [r0, #60]
	bl	xTimerGenericCommand
	mov	r2, #0
	mov	r1, #1
	mov	r3, r2
	str	r5, [sp, #0]
	mov	r6, r0
	ldr	r0, [r4, #64]
	bl	xTimerGenericCommand
	cmp	r6, #0
	cmpne	r0, #0
	bne	.L44
.LBB7:
	ldr	r0, .L46
	bl	RemovePathFromFileName
	ldr	r2, .L46+4
	mov	r1, r0
	ldr	r0, .L46+8
.LBE7:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
.LBB8:
	b	Alert_Message_va
.L44:
.LBE8:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, pc}
.L47:
	.align	2
.L46:
	.word	.LC0
	.word	347
	.word	.LC5
.LFE8:
	.size	im_StopBothPacketTimers, .-im_StopBothPacketTimers
	.section	.text.nm_nm_build_status_and_send_it,"ax",%progbits
	.align	2
	.global	nm_nm_build_status_and_send_it
	.type	nm_nm_build_status_and_send_it, %function
nm_nm_build_status_and_send_it:
.LFB11:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI9:
	mov	r4, r0
	sub	sp, sp, #32
.LCFI10:
	ldr	r0, .L80
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	bne	.L49
	ldr	r0, .L80+4
	ldr	r1, .L80+8
	ldr	r2, .L80+12
	ldr	r3, .L80+16
	bl	Alert_item_not_on_list_with_filename
	b	.L48
.L49:
	ldrb	r3, [r4, #24]	@ zero_extendqisi2
	ldr	r2, [r4, #40]
	cmp	r3, r2
	moveq	r6, #0
	beq	.L51
	subs	r6, r3, #1
	addmi	r6, r3, #6
	mov	r6, r6, asr #3
	add	r6, r6, #1
	mov	r6, r6, asl #16
	mov	r6, r6, lsr #16
.L51:
	ldr	r3, [r4, #84]
	cmp	r3, #0
	beq	.L52
.LBB9:
	add	r7, r6, #16
	ldr	r2, .L80+20
	mov	r0, r7
	ldr	r1, .L80+12
	bl	mem_malloc_debug
	mov	r3, #128
	strb	r3, [r0, #0]
	ldr	r3, .L80+24
	ldr	r2, [r4, #76]
	mov	r5, r0
	cmp	r2, r3
	bne	.L75
	ldr	r3, [r4, #80]
	sub	r3, r3, #3
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L54
.L58:
	.word	.L55
	.word	.L76
	.word	.L56
	.word	.L57
.L55:
	mov	r1, #4
	b	.L53
.L56:
	mov	r1, #6
	b	.L53
.L57:
	mov	r1, #5
	b	.L53
.L54:
	ldr	r0, .L80+28
	bl	Alert_Message
.L75:
	mov	r1, #0
	b	.L53
.L76:
	mov	r1, #3
.L53:
	mov	r0, r5
	bl	set_this_packets_CS3000_message_class
	add	r1, r4, #17
	mov	r2, #3
	add	r0, r5, #2
	bl	memcpy
	add	r1, r4, #14
	mov	r2, #3
	add	r0, r5, #5
	bl	memcpy
	mov	r2, #2
	add	r0, r5, #8
	add	r1, r4, #12
	bl	memcpy
	ldr	r3, .L80+32
	mov	r2, #0
	cmp	r6, r2
	strb	r2, [r5, #10]
	ldreq	r2, [r3, #4]
	strb	r6, [r5, #11]
	addeq	r2, r2, #1
	add	r9, r5, #12
	streq	r2, [r3, #4]
	beq	.L67
.LBB10:
	ldr	r1, [r3, #8]
	add	r1, r1, #1
	str	r1, [r3, #8]
	ldr	r1, [r4, #32]
	strb	r2, [r5, #12]
	cmp	r1, r2
	bne	.L77
	ldr	r0, .L80+36
	bl	Alert_Message
	b	.L61
.L66:
	ands	fp, r8, #7
	ldrb	ip, [r1, #12]	@ zero_extendqisi2
	subne	fp, fp, #1
	movne	fp, fp, asl #16
	movne	fp, fp, lsr #16
	moveq	fp, #7
	cmp	ip, r8
	bne	.L63
	ldrb	r0, [sl, #0]	@ zero_extendqisi2
	ldrb	ip, [r3, fp]	@ zero_extendqisi2
	orr	r0, ip, r0
	strb	r0, [sl, #0]
	mov	r0, r2
	str	r2, [sp, #12]
	str	r3, [sp, #16]
	bl	nm_ListGetNext
	ldr	r2, [sp, #12]
	ldr	r3, [sp, #16]
	subs	r1, r0, #0
	beq	.L64
	mov	r0, #0
.L63:
	cmp	fp, #7
	add	r8, r8, #1
	moveq	ip, #0
	mov	r8, r8, asl #16
	streqb	ip, [sl, #1]
	mov	r8, r8, lsr #16
	addeq	sl, sl, #1
	b	.L60
.L77:
	ldr	r3, .L80+40
	mov	r0, #1
	mov	sl, r9
	mov	r8, r0
	add	r2, r4, #32
.L60:
	ldrb	ip, [r4, #24]	@ zero_extendqisi2
	cmp	ip, r8
	bcs	.L66
	cmp	r0, #1
	bne	.L64
.L61:
	ldr	r0, .L80+44
	bl	Alert_Message
.L64:
	add	r9, r9, r6
.L67:
.LBE10:
	add	r1, r6, #12
	mov	r0, r5
	bl	CRC_calculate_32bit_big_endian
	add	r1, sp, #32
	mov	r2, #4
	str	r0, [r1, #-4]!
	mov	r0, r9
	bl	memcpy
	ldr	r0, [r4, #20]
	mov	r1, r5
	mov	r2, r7
	mov	r3, #0
	bl	Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy
	mov	r0, r5
	ldr	r1, .L80+12
	ldr	r2, .L80+48
	bl	mem_free_debug
.L52:
.LBE9:
	cmp	r6, #0
	bne	.L68
	ldr	r3, [r4, #28]
	cmp	r3, #0
.LBB11:
	moveq	r5, r4
	ldreq	r1, [r5, #32]!
.LBE11:
	beq	.L70
	b	.L79
.L71:
.LBB12:
	ldr	r3, [r1, #20]
	mov	r0, r5
	add	r6, r6, r3
	bl	nm_ListGetNext
	mov	r1, r0
.L70:
	cmp	r1, #0
	bne	.L71
	mov	r0, r6
	ldr	r1, .L80+12
	ldr	r2, .L80+52
	bl	mem_malloc_debug
	mov	sl, r4
	ldr	r7, [sl, #32]!
	mov	r5, r0
	mov	r8, r0
	b	.L72
.L73:
	ldr	r1, [r7, #16]
	ldr	r2, [r7, #20]
	mov	r0, r8
	bl	memcpy
	ldr	r3, [r7, #20]
	mov	r1, r7
	mov	r0, sl
	add	r8, r8, r3
	bl	nm_ListGetNext
	mov	r7, r0
.L72:
	cmp	r7, #0
	bne	.L73
	mov	r3, #1
	str	r3, [r4, #28]
	ldrb	r3, [r4, #16]	@ zero_extendqisi2
	add	r1, sp, #20
	strb	r3, [sp, #20]
	ldrb	r3, [r4, #15]	@ zero_extendqisi2
	ldr	r2, [r4, #20]
	strb	r3, [sp, #21]
	ldrb	r3, [r4, #14]	@ zero_extendqisi2
	ldr	ip, [r4, #80]
	strb	r3, [sp, #22]
	ldrb	r3, [r4, #19]	@ zero_extendqisi2
	strb	r3, [sp, #23]
	ldrb	r3, [r4, #18]	@ zero_extendqisi2
	strb	r3, [sp, #24]
	ldrb	r3, [r4, #17]	@ zero_extendqisi2
	strb	r3, [sp, #25]
	ldmia	r1, {r0, r1}
	ldr	r3, [r4, #76]
	str	r0, [sp, #4]
	strh	r1, [sp, #8]	@ movhi
	mov	r0, r2
	mov	r1, r5
	mov	r2, r6
	str	ip, [sp, #0]
	bl	CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages
	ldr	r2, .L80+56
	mov	r0, r5
	ldr	r1, .L80+12
	bl	mem_free_debug
	ldr	r3, .L80+32
	ldr	r2, [r3, #12]
	add	r2, r2, #1
	str	r2, [r3, #12]
	b	.L48
.L79:
.LBE12:
	ldr	r3, .L80+32
	ldr	r2, [r3, #16]
	add	r2, r2, #1
	str	r2, [r3, #16]
	b	.L48
.L68:
	mov	r0, r4
	bl	start_existence_timer
	ldr	r3, [r4, #72]
	cmp	r3, #0
	beq	.L48
	ldr	r3, [r4, #84]
	cmp	r3, #0
	beq	.L48
	ldr	r4, [r4, #64]
	bl	xTaskGetTickCount
	mvn	r3, #0
	mov	r1, #0
	str	r3, [sp, #0]
	mov	r3, r1
	mov	r2, r0
	mov	r0, r4
	bl	xTimerGenericCommand
.L48:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L81:
	.align	2
.L80:
	.word	.LANCHOR0
	.word	.LC6
	.word	.LC4
	.word	.LC0
	.word	455
	.word	483
	.word	3000
	.word	.LC7
	.word	.LANCHOR1
	.word	.LC8
	.word	.LANCHOR2
	.word	.LC9
	.word	645
	.word	667
	.word	709
.LFE11:
	.size	nm_nm_build_status_and_send_it, .-nm_nm_build_status_and_send_it
	.section	.text.process_waiting_for_response_to_status_timer_expired,"ax",%progbits
	.align	2
	.global	process_waiting_for_response_to_status_timer_expired
	.type	process_waiting_for_response_to_status_timer_expired, %function
process_waiting_for_response_to_status_timer_expired:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI11:
	bl	pvTimerGetTimerID
	ldr	r3, .L85
	mov	r1, #0
	ldr	r2, [r3, #28]
	add	r2, r2, #1
	str	r2, [r3, #28]
	ldr	r3, .L85+4
	mvn	r2, #0
	mov	r4, r0
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, .L85+8
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	bne	.L83
	ldr	r0, .L85+12
	ldr	r1, .L85+16
	ldr	r2, .L85+20
	mov	r3, #412
	bl	Alert_item_not_on_list_with_filename
	b	.L84
.L83:
	mov	r0, r4
	bl	im_StopBothPacketTimers
	ldrh	r3, [r4, #68]
	cmp	r3, #1
	bhi	.L84
	mov	r1, #0
	add	r3, r3, #1
	mvn	r2, #0
	strh	r3, [r4, #68]	@ movhi
	ldr	r0, [r4, #52]
	mov	r3, r1
	bl	xQueueGenericReceive
	mov	r0, r4
	bl	nm_nm_build_status_and_send_it
	mov	r1, #0
	ldr	r0, [r4, #52]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
.L84:
	ldr	r3, .L85+4
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	ldmfd	sp!, {r4, lr}
	b	xQueueGenericSend
.L86:
	.align	2
.L85:
	.word	.LANCHOR1
	.word	list_tpl_in_messages_MUTEX
	.word	.LANCHOR0
	.word	.LC6
	.word	.LC4
	.word	.LC0
.LFE10:
	.size	process_waiting_for_response_to_status_timer_expired, .-process_waiting_for_response_to_status_timer_expired
	.section	.text.process_waiting_for_status_rqst_timer_expired,"ax",%progbits
	.align	2
	.global	process_waiting_for_status_rqst_timer_expired
	.type	process_waiting_for_status_rqst_timer_expired, %function
process_waiting_for_status_rqst_timer_expired:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI12:
	bl	pvTimerGetTimerID
	ldr	r3, .L90
	mov	r1, #0
	ldr	r2, [r3, #24]
	add	r2, r2, #1
	str	r2, [r3, #24]
	ldr	r3, .L90+4
	mvn	r2, #0
	mov	r4, r0
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, .L90+8
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	bne	.L88
	ldr	r0, .L90+12
	ldr	r1, .L90+16
	ldr	r2, .L90+20
	ldr	r3, .L90+24
	bl	Alert_item_not_on_list_with_filename
	b	.L89
.L88:
	mov	r0, r4
	bl	im_StopBothPacketTimers
	ldrh	r3, [r4, #68]
	cmp	r3, #1
	bhi	.L89
	mov	r1, #0
	add	r3, r3, #1
	mvn	r2, #0
	strh	r3, [r4, #68]	@ movhi
	ldr	r0, [r4, #52]
	mov	r3, r1
	bl	xQueueGenericReceive
	mov	r0, r4
	bl	nm_nm_build_status_and_send_it
	mov	r1, #0
	ldr	r0, [r4, #52]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
.L89:
	ldr	r3, .L90+4
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	ldmfd	sp!, {r4, lr}
	b	xQueueGenericSend
.L91:
	.align	2
.L90:
	.word	.LANCHOR1
	.word	list_tpl_in_messages_MUTEX
	.word	.LANCHOR0
	.word	.LC6
	.word	.LC4
	.word	.LC0
	.word	373
.LFE9:
	.size	process_waiting_for_status_rqst_timer_expired, .-process_waiting_for_status_rqst_timer_expired
	.global	__udivsi3
	.section	.text.add_packet_to_incoming_messages,"ax",%progbits
	.align	2
	.global	add_packet_to_incoming_messages
	.type	add_packet_to_incoming_messages, %function
add_packet_to_incoming_messages:
.LFB12:
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI13:
	sub	sp, sp, #76
.LCFI14:
	mov	r7, r0
	mov	r5, r1
	mov	r0, r1
	add	r1, sp, #68
	mov	r4, r2
	bl	get_this_packets_message_class
	ldr	r3, [sp, #68]
	cmp	r3, #2000
	bne	.L114
	ldr	r3, [sp, #72]
	sub	r3, r3, #2
	cmp	r3, #1
	bls	.L114
	ldr	r0, .L122
	bl	Alert_Message
	mov	r1, #1
	b	.L93
.L114:
	mov	r1, #0
.L93:
	ldrb	r2, [r5, #10]	@ zero_extendqisi2
	cmp	r2, #60
	bls	.L94
	ldr	r1, .L122+4
	add	r0, sp, #4
	bl	sprintf
	add	r0, sp, #4
	bl	Alert_Message
	mov	r1, #1
.L94:
	sub	r6, r4, #16
	cmp	r6, #496
	bls	.L95
	add	r0, sp, #4
	ldr	r1, .L122+8
	mov	r2, r4
	bl	sprintf
	add	r0, sp, #4
	bl	Alert_Message
	b	.L92
.L95:
	cmp	r1, #0
	bne	.L92
	ldr	r3, .L122+12
	mvn	r2, #0
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericReceive
	ldrb	r3, [r5, #8]	@ zero_extendqisi2
	ldrb	r0, [r5, #9]	@ zero_extendqisi2
	orr	r0, r3, r0, asl #8
	bl	nm_MIDOnTheListOfIncomingMessages
	subs	r4, r0, #0
	bne	.L97
	ldr	r2, [sp, #68]
	ldr	r3, .L122+16
	cmp	r2, r3
	bne	.L98
	ldr	r3, [sp, #72]
	cmp	r3, #3
	cmpne	r3, #5
	bne	.L98
	bl	nm_TPL_IN_destroy_all_incoming_3000_scan_and_token_messages
.L98:
	ldr	r3, [sp, #68]
	cmp	r3, #2000
	bne	.L99
	ldr	r3, [sp, #72]
	sub	r3, r3, #2
	cmp	r3, #1
	bhi	.L99
	bl	nm_TPL_IN_destroy_all_incoming_2000_comm_test_messages
.L99:
	ldr	r3, .L122+20
	ldr	r1, .L122+24
	ldr	r2, [r3, #0]
	mov	r0, #88
	add	r2, r2, #1
	str	r2, [r3, #0]
	ldr	r2, .L122+28
	bl	mem_malloc_debug
	add	r1, r5, #2
	mov	r2, #6
	mov	r8, #1000
	mov	r4, r0
	add	r0, r0, #14
	bl	memcpy
	mov	r2, #2
	add	r1, r5, #8
	str	r7, [r4, #20]
	add	r0, r4, #12
	bl	memcpy
	ldrb	r3, [r5, #10]	@ zero_extendqisi2
	mov	r7, #0
	strb	r3, [r4, #24]
	str	r7, [r4, #28]
	mov	r0, #1
	bl	xQueueCreateMutex
	mov	r1, r7
	str	r0, [r4, #52]
	add	r0, r4, #32
	bl	nm_ListInit
	ldr	r3, .L122+32
	mov	r1, #5
	ldr	r2, [r3, #128]
	ldr	r0, [r3, #124]
	mul	r2, r8, r2
	add	r0, r0, #1
	mul	r0, r2, r0
	add	r0, r0, #4992
	add	r0, r0, #8
	bl	__udivsi3
	ldr	r3, .L122+36
	mov	r2, r7
	str	r3, [sp, #0]
	mov	r3, r4
	mov	r1, r0
	ldr	r0, .L122+40
	bl	xTimerCreate
	ldr	r3, .L122+44
	mov	r1, r8
	mov	r2, r7
	str	r0, [r4, #56]
	str	r3, [sp, #0]
	ldr	r0, .L122+48
	mov	r3, r4
	bl	xTimerCreate
	ldr	r3, .L122+52
	mov	r2, r7
	mov	r1, #2000
	str	r0, [r4, #60]
	str	r3, [sp, #0]
	ldr	r0, .L122+56
	mov	r3, r4
	bl	xTimerCreate
	ldr	r2, [sp, #68]
	ldr	r3, .L122+16
	strh	r7, [r4, #68]	@ movhi
	cmp	r2, r3
	str	r7, [r4, #84]
	str	r0, [r4, #64]
	bne	.L100
	ldr	r3, [sp, #72]
	cmp	r3, #3
	ldreq	r3, .L122+60
	ldreq	r2, [r3, #4]
	addeq	r2, r2, #1
	streq	r2, [r3, #4]
.L100:
	ldr	r0, .L122+64
	mov	r1, r4
	bl	nm_ListInsertTail
	cmp	r0, #0
	beq	.L101
	ldr	r0, .L122+68
	bl	Alert_Message
	b	.L101
.L97:
	bl	im_StopBothPacketTimers
.L101:
	mov	r1, #0
	mov	r3, r1
	ldr	r0, [r4, #52]
	mvn	r2, #0
	bl	xQueueGenericReceive
	ldr	r3, [r4, #40]
	cmp	r3, #0
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	bne	.L102
	tst	r3, #32
	moveq	r3, #0
	movne	r3, #1
	str	r3, [r4, #72]
	add	r3, sp, #68
	ldmia	r3, {r2-r3}
	str	r2, [r4, #76]
	str	r3, [r4, #80]
	b	.L103
.L102:
	tst	r3, #32
	ldr	r3, [r4, #72]
	beq	.L104
	cmp	r3, #0
	ldreq	r0, .L122+72
	bne	.L105
	b	.L120
.L104:
	cmp	r3, #1
	bne	.L105
	ldr	r0, .L122+76
.L120:
	bl	Alert_Message
.L105:
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	ldr	r2, [r4, #76]
	tst	r3, #32
	movne	r3, #1
	strne	r3, [r4, #72]
	ldr	r3, [sp, #68]
	cmp	r2, r3
	bne	.L107
	ldr	r2, [r4, #80]
	ldr	r3, [sp, #72]
	cmp	r2, r3
	beq	.L103
.L107:
	ldr	r0, .L122+80
	bl	Alert_Message
.L103:
	add	r8, r4, #32
	mov	r0, r8
	bl	nm_ListGetFirst
.L121:
	subs	r7, r0, #0
	beq	.L115
	ldrb	r2, [r7, #12]	@ zero_extendqisi2
	ldrb	r3, [r5, #11]	@ zero_extendqisi2
	cmp	r2, r3
	beq	.L116
	bhi	.L117
	mov	r0, r8
	mov	r1, r7
	bl	nm_ListGetNext
	b	.L121
.L115:
	mov	r3, r7
	b	.L108
.L116:
	mov	r3, #0
	b	.L108
.L117:
	mov	r3, #1
.L108:
	cmp	r7, #0
	orreq	r3, r3, #1
	tst	r3, #255
	beq	.L110
	ldr	r1, .L122+24
	mov	r2, #1072
	mov	r0, r6
	bl	mem_malloc_debug
	add	r1, r5, #12
	mov	r2, r6
	mov	sl, r0
	bl	memcpy
	ldr	r1, .L122+24
	ldr	r2, .L122+84
	mov	r0, #24
	bl	mem_malloc_debug
	ldrb	r3, [r5, #11]	@ zero_extendqisi2
	mov	r2, r7
	mov	r1, r0
	strb	r3, [r0, #12]
	str	sl, [r0, #16]
	str	r6, [r0, #20]
	mov	r0, r8
	bl	nm_ListInsert
.L110:
	mov	r0, r4
	bl	start_existence_timer
	ldrb	r5, [r5, #0]	@ zero_extendqisi2
	ands	r5, r5, #64
	beq	.L111
	mov	r0, r4
	bl	nm_nm_build_status_and_send_it
	b	.L112
.L111:
	ldr	r3, [r4, #72]
	cmp	r3, #0
	beq	.L112
	ldr	r3, [r4, #84]
	cmp	r3, #0
	beq	.L112
	ldr	r6, [r4, #60]
	bl	xTaskGetTickCount
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, r5
	mov	r3, r5
	mov	r2, r0
	mov	r0, r6
	bl	xTimerGenericCommand
.L112:
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	ldr	r0, [r4, #52]
	bl	xQueueGenericSend
	ldr	r3, .L122+12
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
.L92:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L123:
	.align	2
.L122:
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	list_tpl_in_messages_MUTEX
	.word	3000
	.word	.LANCHOR1
	.word	.LC0
	.word	877
	.word	config_c
	.word	process_im_existence_timer_expired
	.word	.LC13
	.word	process_waiting_for_status_rqst_timer_expired
	.word	.LC14
	.word	process_waiting_for_response_to_status_timer_expired
	.word	.LC15
	.word	comm_stats
	.word	.LANCHOR0
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	1080
.LFE12:
	.size	add_packet_to_incoming_messages, .-add_packet_to_incoming_messages
	.section	.text.TPL_IN_task,"ax",%progbits
	.align	2
	.global	TPL_IN_task
	.type	TPL_IN_task, %function
TPL_IN_task:
.LFB13:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI15:
	ldr	r6, .L130
	sub	sp, sp, #20
.LCFI16:
	rsb	r6, r6, r0
	mov	r1, #0
	mov	r2, #32
	ldr	r0, .L130+4
	bl	memset
	ldr	r0, .L130+8
	mov	r1, #0
	bl	nm_ListInit
	ldr	r5, .L130+12
	ldr	r4, .L130+16
	mov	r6, r6, lsr #5
.L128:
	ldr	r0, [r5, #0]
	mov	r1, sp
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	bne	.L125
	ldrh	r3, [sp, #0]
	cmp	r3, #1024
	beq	.L126
	cmp	r3, #1280
	bne	.L125
	b	.L129
.L126:
	add	r2, sp, #8
	ldmia	r2, {r1-r2}
	ldr	r0, [sp, #16]
	bl	add_packet_to_incoming_messages
	ldr	r0, [sp, #8]
	ldr	r1, .L130+20
	ldr	r2, .L130+24
	bl	mem_free_debug
	b	.L125
.L129:
	ldr	r0, [sp, #4]
	bl	tpl_in_existence_timer_expired
.L125:
	bl	xTaskGetTickCount
	str	r0, [r4, r6, asl #2]
	b	.L128
.L131:
	.align	2
.L130:
	.word	Task_Table
	.word	.LANCHOR1
	.word	.LANCHOR0
	.word	TPL_IN_event_queue
	.word	task_last_execution_stamp
	.word	.LC0
	.word	1178
.LFE13:
	.size	TPL_IN_task, .-TPL_IN_task
	.section	.text.TPL_IN_make_a_copy_and_direct_incoming_packet,"ax",%progbits
	.align	2
	.global	TPL_IN_make_a_copy_and_direct_incoming_packet
	.type	TPL_IN_make_a_copy_and_direct_incoming_packet, %function
TPL_IN_make_a_copy_and_direct_incoming_packet:
.LFB14:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI17:
	ldrb	r6, [r1, #0]	@ zero_extendqisi2
	sub	sp, sp, #56
.LCFI18:
	ands	r7, r6, #128
	mov	r8, r0
	mov	r5, r1
	mov	r4, r2
	bne	.L133
.LBB13:
	mov	r0, r2
	ldr	r1, .L137
	ldr	r2, .L137+4
	bl	mem_malloc_debug
	mov	r1, r5
	mov	r2, r4
	mov	r6, r0
	bl	memcpy
	mov	r3, #1024
	strh	r3, [sp, #36]	@ movhi
	ldr	r3, .L137+8
	add	r1, sp, #36
	ldr	r0, [r3, #0]
	mov	r2, r7
	mov	r3, r7
	str	r6, [sp, #44]
	str	r4, [sp, #48]
	str	r8, [sp, #52]
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L132
	ldr	r0, .L137+12
	bl	Alert_Message
	b	.L132
.L133:
.LBE13:
	ands	r6, r6, #64
	beq	.L135
	ldr	r0, .L137
	bl	RemovePathFromFileName
	ldr	r2, .L137+16
	mov	r1, r0
	ldr	r0, .L137+20
	b	.L136
.L135:
.LBB14:
	mov	r0, r2
	ldr	r1, .L137
	ldr	r2, .L137+24
	bl	mem_malloc_debug
	mov	r1, r5
	mov	r2, r4
	mov	r7, r0
	bl	memcpy
	mov	r3, #512
	strh	r3, [sp, #0]	@ movhi
	ldr	r3, .L137+28
	mov	r1, sp
	ldr	r0, [r3, #0]
	mov	r2, r6
	mov	r3, r6
	str	r7, [sp, #8]
	str	r4, [sp, #12]
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L132
	ldr	r0, .L137
	bl	RemovePathFromFileName
	ldr	r2, .L137+32
	mov	r1, r0
	ldr	r0, .L137+36
.L136:
	bl	Alert_Message_va
.L132:
.LBE14:
	add	sp, sp, #56
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L138:
	.align	2
.L137:
	.word	.LC0
	.word	1244
	.word	TPL_IN_event_queue
	.word	.LC20
	.word	1273
	.word	.LC21
	.word	1281
	.word	TPL_OUT_event_queue
	.word	1294
	.word	.LC22
.LFE14:
	.size	TPL_IN_make_a_copy_and_direct_incoming_packet, .-TPL_IN_make_a_copy_and_direct_incoming_packet
	.global	IncomingMessages
	.global	IncomingStats
	.global	_Masks
	.section	.bss.IncomingStats,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	IncomingStats, %object
	.size	IncomingStats, 32
IncomingStats:
	.space	32
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/tpl_in.c\000"
.LC1:
	.ascii	"TPL_IN queue full : %s, %u\000"
.LC2:
	.ascii	"Timer Queue Full! : %s, %u\000"
.LC3:
	.ascii	"pim\000"
.LC4:
	.ascii	"IncomingMessages\000"
.LC5:
	.ascii	"Timer Queue Full : %s, %u\000"
.LC6:
	.ascii	"im\000"
.LC7:
	.ascii	"Making IM status for unk class\000"
.LC8:
	.ascii	"No packets in IM\000"
.LC9:
	.ascii	"No bits set in Status\000"
.LC10:
	.ascii	"TPL_IN unexp 2000e msg class\000"
.LC11:
	.ascii	"IM Blocks=%d (too big)\000"
.LC12:
	.ascii	"TPL_IN Length=%d (too big)\000"
.LC13:
	.ascii	"existence timer\000"
.LC14:
	.ascii	"status rqst timer\000"
.LC15:
	.ascii	"status response timer\000"
.LC16:
	.ascii	"Failure adding new IM to list\000"
.LC17:
	.ascii	"IM varying active bit (0x01)\000"
.LC18:
	.ascii	"IM varying active bit (0x02)\000"
.LC19:
	.ascii	"IM varying class detected\000"
.LC20:
	.ascii	"TPL_IN queue full\000"
.LC21:
	.ascii	"RCVD_DATA: UNEXP quit COMMS? : %s, %u\000"
.LC22:
	.ascii	"TPL_OUT queue full. : %s, %u\000"
	.section	.bss.IncomingMessages,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	IncomingMessages, %object
	.size	IncomingMessages, 20
IncomingMessages:
	.space	20
	.section	.rodata._Masks,"a",%progbits
	.set	.LANCHOR2,. + 0
	.type	_Masks, %object
	.size	_Masks, 8
_Masks:
	.byte	1
	.byte	2
	.byte	4
	.byte	8
	.byte	16
	.byte	32
	.byte	64
	.byte	-128
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI0-.LFB6
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI5-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI6-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI7-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI8-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI9-.LFB11
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI11-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI12-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI13-.LFB12
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x68
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI15-.LFB13
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI17-.LFB14
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/tpl_in.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x15f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF15
	.byte	0x1
	.4byte	.LASF16
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x125
	.byte	0x1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x151
	.byte	0x1
	.uleb128 0x3
	.4byte	0x21
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x54
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x82
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xa1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xbc
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST4
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xe9
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.byte	0xfb
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST6
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x139
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x3
	.4byte	0x2b
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1b5
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST9
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x18e
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x167
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST11
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x2ec
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x47a
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x4ca
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB6
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB11
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI10
	.4byte	.LFE11
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB9
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI14
	.4byte	.LFE12
	.2byte	0x3
	.byte	0x7d
	.sleb128 104
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI16
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI18
	.4byte	.LFE14
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"nm_destroy_this_IM\000"
.LASF0:
	.ascii	"process_im_existence_timer_expired\000"
.LASF5:
	.ascii	"nm_TPL_IN_destroy_all_incoming_2000_comm_test_messa"
	.ascii	"ges\000"
.LASF6:
	.ascii	"nm_MIDOnTheListOfIncomingMessages\000"
.LASF14:
	.ascii	"TPL_IN_make_a_copy_and_direct_incoming_packet\000"
.LASF8:
	.ascii	"tpl_in_existence_timer_expired\000"
.LASF13:
	.ascii	"TPL_IN_task\000"
.LASF16:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/tpl_in.c\000"
.LASF15:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF9:
	.ascii	"nm_nm_build_status_and_send_it\000"
.LASF1:
	.ascii	"im_StopBothPacketTimers\000"
.LASF4:
	.ascii	"nm_TPL_IN_destroy_all_incoming_3000_scan_and_token_"
	.ascii	"messages\000"
.LASF7:
	.ascii	"start_existence_timer\000"
.LASF10:
	.ascii	"process_waiting_for_response_to_status_timer_expire"
	.ascii	"d\000"
.LASF11:
	.ascii	"process_waiting_for_status_rqst_timer_expired\000"
.LASF12:
	.ascii	"add_packet_to_incoming_messages\000"
.LASF3:
	.ascii	"TPL_IN_destroy_all_incoming_3000_scan_response_and_"
	.ascii	"token_response_messages\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
