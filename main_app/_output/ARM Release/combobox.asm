	.file	"combobox.c"
	.text
.Ltext0:
	.section	.text.FDTO_COMBOBOX_add_items,"ax",%progbits
	.align	2
	.global	FDTO_COMBOBOX_add_items
	.type	FDTO_COMBOBOX_add_items, %function
FDTO_COMBOBOX_add_items:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	str	r0, [r3, #0]
	bx	lr
.L3:
	.align	2
.L2:
	.word	GuiVar_ComboBoxItemIndex
.LFE1:
	.size	FDTO_COMBOBOX_add_items, .-FDTO_COMBOBOX_add_items
	.section	.text.FDTO_COMBOBOX_hide,"ax",%progbits
	.align	2
	.global	FDTO_COMBOBOX_hide
	.type	FDTO_COMBOBOX_hide, %function
FDTO_COMBOBOX_hide:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	mov	r0, #0
	ldr	lr, [sp], #4
	b	FDTO_Redraw_Screen
.LFE6:
	.size	FDTO_COMBOBOX_hide, .-FDTO_COMBOBOX_hide
	.section	.text.COMBOBOX_refresh_item,"ax",%progbits
	.align	2
	.global	COMBOBOX_refresh_item
	.type	COMBOBOX_refresh_item, %function
COMBOBOX_refresh_item:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, r0
	mov	r0, #0
	b	SCROLL_BOX_redraw_line
.LFE3:
	.size	COMBOBOX_refresh_item, .-COMBOBOX_refresh_item
	.section	.text.COMBOBOX_refresh_items,"ax",%progbits
	.align	2
	.global	COMBOBOX_refresh_items
	.type	COMBOBOX_refresh_items, %function
COMBOBOX_refresh_items:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	b	SCROLL_BOX_redraw
.LFE4:
	.size	COMBOBOX_refresh_items, .-COMBOBOX_refresh_items
	.section	.text.FDTO_COMBOBOX_show,"ax",%progbits
	.align	2
	.global	FDTO_COMBOBOX_show
	.type	FDTO_COMBOBOX_show, %function
FDTO_COMBOBOX_show:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI1:
	mov	r6, r3
	ldr	r3, .L8
	mov	r0, r0, asl #16
	mov	r4, r1
	mov	r5, r2
	ldrsh	r1, [r3, #0]
	mov	r0, r0, lsr #16
	mov	r2, #1
	bl	GuiLib_ShowScreen
	mov	r6, r6, asl #16
	mov	r2, r5, asl #16
	mov	r3, r6, asr #16
	mov	r1, r4
	mov	r2, r2, asr #16
	mov	r0, #0
	str	r3, [sp, #0]
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_Refresh
.L9:
	.align	2
.L8:
	.word	GuiLib_ActiveCursorFieldNo
.LFE5:
	.size	FDTO_COMBOBOX_show, .-FDTO_COMBOBOX_show
	.section	.text.FDTO_COMBO_BOX_show_no_yes_dropdown,"ax",%progbits
	.align	2
	.global	FDTO_COMBO_BOX_show_no_yes_dropdown
	.type	FDTO_COMBO_BOX_show_no_yes_dropdown, %function
FDTO_COMBO_BOX_show_no_yes_dropdown:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r2
	ldr	r2, .L11
	str	r0, [r2, #0]
	ldr	r2, .L11+4
	mov	r0, #740
	str	r1, [r2, #0]
	ldr	r1, .L11+8
	mov	r2, #2
	b	FDTO_COMBOBOX_show
.L12:
	.align	2
.L11:
	.word	GuiVar_ComboBox_X1
	.word	GuiVar_ComboBox_Y1
	.word	FDTO_COMBOBOX_add_items
.LFE0:
	.size	FDTO_COMBO_BOX_show_no_yes_dropdown, .-FDTO_COMBO_BOX_show_no_yes_dropdown
	.section	.text.COMBO_BOX_key_press,"ax",%progbits
	.align	2
	.global	COMBO_BOX_key_press
	.type	COMBO_BOX_key_press, %function
COMBO_BOX_key_press:
.LFB7:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r4, lr}
.LCFI2:
	mov	r3, r0
	sub	sp, sp, #36
.LCFI3:
	mov	r4, r1
	beq	.L15
	bhi	.L17
	cmp	r0, #0
	beq	.L15
	cmp	r0, #2
	b	.L21
.L17:
	cmp	r0, #80
	beq	.L15
	cmp	r0, #84
	beq	.L15
	cmp	r0, #67
.L21:
	bne	.L14
	b	.L22
.L15:
	mov	r0, #0
	mov	r1, r3
	bl	SCROLL_BOX_up_or_down
	b	.L13
.L22:
	bl	good_key_beep
	cmp	r4, #0
	beq	.L19
.LBB4:
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	str	r0, [r4, #0]
.L19:
.LBE4:
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L23
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	b	.L13
.L14:
	bl	bad_key_beep
.L13:
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L24:
	.align	2
.L23:
	.word	FDTO_COMBOBOX_hide
.LFE7:
	.size	COMBO_BOX_key_press, .-COMBO_BOX_key_press
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI0-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI1-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI2-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/combobox.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xae
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF7
	.byte	0x1
	.4byte	.LASF8
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.byte	0x45
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x37
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x84
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x54
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x5d
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0x72
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0x25
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0x97
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB6
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB5
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB7
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"FDTO_COMBOBOX_add_items\000"
.LASF8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/c"
	.ascii	"ombobox.c\000"
.LASF3:
	.ascii	"COMBOBOX_refresh_items\000"
.LASF1:
	.ascii	"FDTO_COMBOBOX_hide\000"
.LASF6:
	.ascii	"COMBO_BOX_key_press\000"
.LASF4:
	.ascii	"FDTO_COMBOBOX_show\000"
.LASF2:
	.ascii	"COMBOBOX_refresh_item\000"
.LASF7:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"FDTO_COMBO_BOX_show_no_yes_dropdown\000"
.LASF9:
	.ascii	"COMBOBOX_get_item_index\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
