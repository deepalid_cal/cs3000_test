	.file	"e_manual_programs.c"
	.text
.Ltext0:
	.section	.text.MANUAL_PROGRAMS_add_new_group,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_add_new_group, %function
MANUAL_PROGRAMS_add_new_group:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r4, .L2
	ldr	r2, .L2+4
	ldr	r3, .L2+8
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L2+12
	ldr	r1, .L2+16
	bl	nm_GROUP_create_new_group_from_UI
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L3:
	.align	2
.L2:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	491
	.word	nm_MANUAL_PROGRAMS_create_new_group
	.word	MANUAL_PROGRAMS_copy_group_into_guivars
.LFE5:
	.size	MANUAL_PROGRAMS_add_new_group, .-MANUAL_PROGRAMS_add_new_group
	.section	.text.MANUAL_PROGRAMS_process_water_day,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_process_water_day, %function
MANUAL_PROGRAMS_process_water_day:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r4, .L17
	ldrsh	r3, [r4, #0]
	sub	r3, r3, #1
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L5
.L13:
	.word	.L6
	.word	.L7
	.word	.L8
	.word	.L9
	.word	.L10
	.word	.L11
	.word	.L12
.L6:
	ldr	r0, .L17+4
	b	.L16
.L7:
	ldr	r0, .L17+8
.L16:
	bl	process_bool
	b	.L14
.L8:
	ldr	r0, .L17+12
	b	.L16
.L9:
	ldr	r0, .L17+16
	b	.L16
.L10:
	ldr	r0, .L17+20
	b	.L16
.L11:
	ldr	r0, .L17+24
	b	.L16
.L12:
	ldr	r0, .L17+28
	b	.L16
.L5:
	bl	bad_key_beep
.L14:
	bl	Refresh_Screen
	ldrsh	r3, [r4, #0]
	cmp	r3, #6
	ldmgtfd	sp!, {r4, pc}
	mov	r0, #0
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Down
.L18:
	.align	2
.L17:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_ManualPWaterDay_Sun
	.word	GuiVar_ManualPWaterDay_Mon
	.word	GuiVar_ManualPWaterDay_Tue
	.word	GuiVar_ManualPWaterDay_Wed
	.word	GuiVar_ManualPWaterDay_Thu
	.word	GuiVar_ManualPWaterDay_Fri
	.word	GuiVar_ManualPWaterDay_Sat
.LFE1:
	.size	MANUAL_PROGRAMS_process_water_day, .-MANUAL_PROGRAMS_process_water_day
	.section	.text.MANUAL_PROGRAMS_process_start_time,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_process_start_time, %function
MANUAL_PROGRAMS_process_start_time:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI2:
	mov	r3, #1
	mov	r4, r2
	mov	r2, #10
	stmia	sp, {r2, r3}
	mov	r5, r1
	mov	r3, #1440
	mov	r2, #0
	bl	process_uns32
	ldr	r3, [r5, #0]
	cmp	r3, #1440
	movcs	r3, #0
	movcc	r3, #1
	str	r3, [r4, #0]
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.LFE2:
	.size	MANUAL_PROGRAMS_process_start_time, .-MANUAL_PROGRAMS_process_start_time
	.global	__udivsi3
	.section	.text.MANUAL_PROGRAMS_update_will_run,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_update_will_run, %function
MANUAL_PROGRAMS_update_will_run:
.LFB0:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L109
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI3:
	ldr	r3, [r3, #0]
	sub	sp, sp, #40
.LCFI4:
	str	r3, [sp, #8]
	ldr	r3, .L109+4
	add	r0, sp, #16
	ldr	r3, [r3, #0]
	str	r3, [sp, #12]
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, .L109+8
	ldrh	r4, [sp, #20]
	ldr	r3, [r3, #0]
	cmp	r3, r4
	movcc	sl, #0
	bcc	.L21
	ldr	r2, .L109+12
	ldr	sl, [r2, #0]
	cmp	sl, r4
	movhi	sl, #0
	movls	sl, #1
.L21:
	add	r2, r4, #1
	cmp	r3, r2
	movcc	r8, #0
	bcc	.L22
	ldr	r2, .L109+12
	ldr	r8, [r2, #0]
	cmp	r8, r3
	movhi	r8, #0
	movls	r8, #1
.L22:
	ldr	r3, .L109+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L23
	ldr	r3, .L109+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L24
	ldr	r3, .L109+24
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L24
	ldr	r3, .L109+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L24
	ldr	r3, .L109+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L24
	ldr	r3, .L109+36
	ldr	r5, [r3, #0]
	cmp	r5, #0
	beq	.L97
.L24:
	ldr	r3, .L109+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L26
	ldr	r0, [sp, #16]
	mov	r1, #60
	bl	__udivsi3
	ldr	r3, .L109+40
	ldr	r3, [r3, #0]
	cmp	r3, r0
	bhi	.L93
.L26:
	ldr	r3, .L109+24
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L27
	ldr	r0, [sp, #16]
	mov	r1, #60
	bl	__udivsi3
	ldr	r3, .L109+44
	ldr	r3, [r3, #0]
	cmp	r3, r0
	bhi	.L93
.L27:
	ldr	r3, .L109+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L28
	ldr	r0, [sp, #16]
	mov	r1, #60
	bl	__udivsi3
	ldr	r3, .L109+48
	ldr	r3, [r3, #0]
	cmp	r3, r0
	bhi	.L93
.L28:
	ldr	r3, .L109+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L29
	ldr	r0, [sp, #16]
	mov	r1, #60
	bl	__udivsi3
	ldr	r3, .L109+52
	ldr	r3, [r3, #0]
	cmp	r3, r0
	bhi	.L93
.L29:
	ldr	r3, .L109+36
	ldr	r7, [r3, #0]
	cmp	r7, #0
	moveq	r5, #1
	beq	.L25
	ldr	r0, [sp, #16]
	mov	r1, #60
	bl	__udivsi3
	ldr	r3, .L109+56
	mov	r5, #1
	ldr	r3, [r3, #0]
	cmp	r3, r0
	movls	r7, #0
	bls	.L25
	b	.L97
.L93:
	mov	r5, #1
.L97:
	mov	r7, r5
.L25:
	cmp	sl, #0
	moveq	r6, sl
	beq	.L30
	add	r3, sp, #36
	str	r3, [sp, #0]
	mov	r0, r4
	add	r3, sp, #32
	add	r1, sp, #24
	add	r2, sp, #28
	bl	DateToDMY
	ldr	r6, [sp, #36]
	cmp	r6, #0
	ldreq	r3, .L109+60
	beq	.L102
	cmp	r6, #1
	bne	.L33
	ldr	r3, .L109+64
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L30
	b	.L85
.L33:
	cmp	r6, #2
	bne	.L34
	ldr	r3, .L109+68
.L102:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r6, #1
	bne	.L30
	b	.L85
.L34:
	cmp	r6, #3
	ldreq	r3, .L109+72
	beq	.L98
	cmp	r6, #4
	ldreq	r3, .L109+76
	beq	.L98
	cmp	r6, #5
	ldreq	r3, .L109+80
	beq	.L98
	cmp	r6, #6
	bne	.L85
	ldr	r3, .L109+84
.L98:
	ldr	r6, [r3, #0]
	cmp	r6, #0
	movne	r6, #1
	moveq	r6, #0
	b	.L30
.L85:
	mov	r6, #0
.L30:
	cmp	r8, #0
	moveq	r4, r8
	beq	.L38
	ldr	r2, .L109+12
	ldrh	r3, [sp, #20]
	ldr	fp, [r2, #0]
	mov	r9, #0
	cmp	r3, fp
	addcs	fp, r3, #1
	mov	r4, r9
.L48:
	add	ip, r9, fp
	add	r3, sp, #36
	str	r3, [sp, #0]
	mov	r0, ip
	add	r3, sp, #32
	add	r1, sp, #24
	add	r2, sp, #28
	str	ip, [sp, #4]
	bl	DateToDMY
	ldr	r3, [sp, #36]
	ldr	ip, [sp, #4]
	cmp	r3, #0
	ldreq	r3, .L109+60
	beq	.L99
	cmp	r3, #1
	ldreq	r3, .L109+64
	beq	.L99
	cmp	r3, #2
	ldreq	r3, .L109+68
	beq	.L99
	cmp	r3, #3
	ldreq	r3, .L109+72
	beq	.L99
	cmp	r3, #4
	ldreq	r3, .L109+76
	beq	.L99
	cmp	r3, #5
	ldreq	r3, .L109+80
	beq	.L99
	cmp	r3, #6
	bne	.L41
	ldr	r3, .L109+84
.L99:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r4, #1
.L41:
	ldr	r3, .L109+8
	ldr	r3, [r3, #0]
	cmp	ip, r3
	bcs	.L38
	add	r9, r9, #1
	cmp	r9, #7
	bne	.L48
.L38:
	ldr	r3, .L109+88
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L49
	ldr	r3, .L109+92
	flds	s15, [r3, #0]
	fcmpezs	s15
	fmstat
	ble	.L49
	tst	r7, sl
	ldr	r3, .L109
	beq	.L51
	cmp	r6, #0
	bne	.L52
.L51:
	tst	r5, r8
	beq	.L53
	cmp	r4, #0
	beq	.L53
.L52:
	mov	r2, #1
	b	.L100
.L53:
	mov	r2, #0
	b	.L100
.L49:
	ldr	r3, .L109
	mov	r2, #0
.L100:
	str	r2, [r3, #0]
	ldr	r3, .L109
	ldr	r2, [r3, #0]
	cmp	r2, #0
	bne	.L55
	ldr	r3, .L109+92
	flds	s15, [r3, #0]
	fcmpzs	s15
	fmstat
	moveq	r1, #3
	beq	.L103
	ldr	r3, .L109+88
	ldr	r3, [r3, #0]
	cmp	r3, #0
	moveq	r1, #4
	beq	.L103
	cmp	r5, #0
	beq	.L106
.L58:
	orrs	r3, r8, sl
	beq	.L105
.L59:
	orrs	r3, r4, r6
	ldreq	r3, .L109+4
	streq	r2, [r3, #0]
	beq	.L55
	and	r1, sl, #255
	eor	r3, r8, #1
	tst	r1, r3
	beq	.L61
	cmp	r7, #0
	bne	.L62
.L106:
	mov	r1, #1
	b	.L103
.L62:
	cmp	r6, #0
	bne	.L55
.L105:
	mov	r1, #2
.L103:
	ldr	r3, .L109+4
	b	.L101
.L61:
	and	r8, r8, #255
	eor	sl, sl, #1
	tst	r8, sl
	ldr	r3, .L109+4
	bne	.L108
.L63:
	tst	r8, r1
	beq	.L65
	cmp	r7, #0
	movne	r6, #0
	andeq	r6, r6, #1
	cmp	r6, #0
	beq	.L65
.L108:
	cmp	r4, #0
	moveq	r1, #1
	beq	.L101
.L65:
	mov	r1, #5
.L101:
	str	r1, [r3, #0]
.L55:
	ldr	r3, [sp, #8]
	cmp	r3, r2
	bne	.L67
	ldr	r3, .L109+4
	ldr	r2, [sp, #12]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L20
.L67:
	mov	r0, #0
	bl	Redraw_Screen
	b	.L20
.L23:
	ldr	r0, [sp, #16]
	mov	r1, #60
	bl	__udivsi3
	ldr	r3, .L109+96
	ldr	r3, [r3, #0]
	cmp	r3, r0
	bhi	.L93
	b	.L24
.L20:
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L110:
	.align	2
.L109:
	.word	GuiVar_ManualPWillRun
	.word	GuiVar_ManualPWillNotRunReason
	.word	g_MANUAL_PROGRAMS_stop_date
	.word	g_MANUAL_PROGRAMS_start_date
	.word	GuiVar_ManualPStartTime1Enabled
	.word	GuiVar_ManualPStartTime2Enabled
	.word	GuiVar_ManualPStartTime3Enabled
	.word	GuiVar_ManualPStartTime4Enabled
	.word	GuiVar_ManualPStartTime5Enabled
	.word	GuiVar_ManualPStartTime6Enabled
	.word	GuiVar_ManualPStartTime2
	.word	GuiVar_ManualPStartTime3
	.word	GuiVar_ManualPStartTime4
	.word	GuiVar_ManualPStartTime5
	.word	GuiVar_ManualPStartTime6
	.word	GuiVar_ManualPWaterDay_Sun
	.word	GuiVar_ManualPWaterDay_Mon
	.word	GuiVar_ManualPWaterDay_Tue
	.word	GuiVar_ManualPWaterDay_Wed
	.word	GuiVar_ManualPWaterDay_Thu
	.word	GuiVar_ManualPWaterDay_Fri
	.word	GuiVar_ManualPWaterDay_Sat
	.word	GuiVar_StationSelectionGridStationCount
	.word	GuiVar_ManualPRunTime
	.word	GuiVar_ManualPStartTime1
.LFE0:
	.size	MANUAL_PROGRAMS_update_will_run, .-MANUAL_PROGRAMS_update_will_run
	.section	.text.MANUAL_PROGRAMS_process_group,"ax",%progbits
	.align	2
	.type	MANUAL_PROGRAMS_process_group, %function
MANUAL_PROGRAMS_process_group:
.LFB6:
	@ args = 0, pretend = 0, frame = 92
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L173
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI5:
	ldrsh	r2, [r3, #0]
	ldr	r3, .L173+4
	sub	sp, sp, #100
.LCFI6:
	cmp	r2, r3
	mov	r5, r0
	mov	r4, r1
	bne	.L165
	cmp	r0, #67
	beq	.L114
	cmp	r0, #2
	bne	.L115
	ldr	r3, .L173+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L115
.L114:
	bl	MANUAL_PROGRAMS_extract_and_store_group_name_from_GuiVars
.L115:
	mov	r0, r5
	mov	r1, r4
	ldr	r2, .L173+12
	bl	KEYBOARD_process_key
	b	.L111
.L165:
	cmp	r0, #4
	beq	.L122
	bhi	.L126
	cmp	r0, #1
	beq	.L119
	bcc	.L118
	cmp	r0, #2
	beq	.L120
	cmp	r0, #3
	bne	.L117
	b	.L121
.L126:
	cmp	r0, #67
	beq	.L124
	bhi	.L127
	cmp	r0, #16
	beq	.L123
	cmp	r0, #20
	bne	.L117
	b	.L123
.L127:
	cmp	r0, #80
	beq	.L125
	cmp	r0, #84
	bne	.L117
	b	.L125
.L120:
	ldr	r4, .L173+8
	ldrsh	r3, [r4, #0]
	cmp	r3, #7
	bgt	.L132
	cmp	r3, #1
	bge	.L134
	cmp	r3, #0
	beq	.L129
	b	.L161
.L132:
	sub	r3, r3, #17
	cmp	r3, #1
	bhi	.L161
	b	.L172
.L129:
	mov	r0, #168
	mov	r1, #27
	bl	GROUP_process_show_keyboard
	b	.L111
.L172:
	bl	MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars
	mov	r3, #3
	str	r3, [sp, #56]
	mov	r3, #67
	str	r3, [sp, #64]
	ldr	r3, .L173+16
	add	r0, sp, #56
	str	r3, [sp, #76]
	ldr	r3, .L173+20
	str	r3, [sp, #72]
	mov	r3, #1
	str	r3, [sp, #80]
	ldrsh	r3, [r4, #0]
	sub	ip, r3, #17
	rsbs	r3, ip, #0
	adc	r3, r3, ip
	str	r3, [sp, #84]
	ldr	r3, .L173+24
	ldr	r3, [r3, #0]
	str	r3, [sp, #88]
	bl	Change_Screen
	b	.L111
.L125:
	ldr	r3, .L173+8
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r3, #15
	ldrls	pc, [pc, r3, asl #2]
	b	.L133
.L144:
	.word	.L134
	.word	.L134
	.word	.L134
	.word	.L134
	.word	.L134
	.word	.L134
	.word	.L134
	.word	.L135
	.word	.L136
	.word	.L137
	.word	.L138
	.word	.L139
	.word	.L140
	.word	.L141
	.word	.L142
	.word	.L143
.L134:
	bl	MANUAL_PROGRAMS_process_water_day
	b	.L145
.L135:
	mov	r0, r5
	ldr	r1, .L173+28
	ldr	r2, .L173+32
	b	.L168
.L136:
	ldr	r1, .L173+36
	ldr	r2, .L173+40
	mov	r0, r5
.L168:
	bl	MANUAL_PROGRAMS_process_start_time
	b	.L145
.L137:
	mov	r0, r5
	ldr	r1, .L173+44
	ldr	r2, .L173+48
	b	.L168
.L138:
	mov	r0, r5
	ldr	r1, .L173+52
	ldr	r2, .L173+56
	b	.L168
.L139:
	mov	r0, r5
	ldr	r1, .L173+60
	ldr	r2, .L173+64
	b	.L168
.L140:
	mov	r0, r5
	ldr	r1, .L173+68
	ldr	r2, .L173+72
	b	.L168
.L141:
.LBB7:
	ldr	r4, .L173+76
	add	r0, sp, #92
	bl	EPSON_obtain_latest_time_and_date
	ldrh	r2, [sp, #96]
	ldr	r3, [r4, #0]
	cmp	r3, r2
	bcs	.L146
	bl	good_key_beep
	ldrh	r3, [sp, #96]
	str	r3, [r4, #0]
	b	.L147
.L146:
	mov	r0, #1
	mov	r3, #0
	stmia	sp, {r0, r3}
	ldr	r3, .L173+80
	mov	r0, r5
	mov	r1, r4
	ldr	r3, [r3, #0]
	bl	process_uns32
.L147:
	ldr	r5, .L173+76
	mov	r4, #225
	mov	r3, #125
	str	r4, [sp, #0]
	mov	r1, #48
	ldr	r2, [r5, #0]
	add	r0, sp, #8
	bl	GetDateStr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L173+84
	bl	strlcpy
	ldr	r3, .L173+80
	ldr	r2, [r5, #0]
	ldr	r1, [r3, #0]
	cmp	r1, r2
	bcs	.L145
	str	r2, [r3, #0]
	add	r0, sp, #8
	str	r4, [sp, #0]
	mov	r1, #48
	b	.L169
.L142:
.LBE7:
.LBB8:
	add	r0, sp, #92
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, .L173+76
	mov	r1, #12
	ldr	r2, .L173+88
	mov	r0, #31
	ldr	r6, [r3, #0]
	bl	DMYToDate
	ldr	r4, .L173+80
	mov	r1, #1
	mov	r2, #0
	stmia	sp, {r1, r2}
	mov	r1, r4
	mov	r2, r6
	mov	r3, r0, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r5
	bl	process_uns32
	mov	r3, #225
	str	r3, [sp, #0]
	ldr	r2, [r4, #0]
	add	r0, sp, #8
	mov	r1, #48
.L169:
	mov	r3, #125
	bl	GetDateStr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L173+92
	bl	strlcpy
	b	.L145
.L143:
.LBE8:
	mov	r3, #1065353216
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r5
	ldr	r1, .L173+96
	mov	r2, #0
	ldr	r3, .L173+100
	bl	process_fl
	b	.L145
.L133:
	bl	bad_key_beep
.L145:
	bl	Refresh_Screen
	bl	MANUAL_PROGRAMS_update_will_run
	b	.L111
.L123:
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	ldr	r3, .L173+104
	ldr	r2, .L173+108
	str	r3, [sp, #0]
	ldr	r3, .L173+112
	mov	r1, r0
	mov	r0, r5
	bl	GROUP_process_NEXT_and_PREV
	b	.L111
.L122:
	ldr	r3, .L173+8
	ldrsh	r0, [r3, #0]
	sub	r3, r0, #1
	cmp	r3, #17
	ldrls	pc, [pc, r3, asl #2]
	b	.L148
.L155:
	.word	.L149
	.word	.L149
	.word	.L149
	.word	.L149
	.word	.L149
	.word	.L149
	.word	.L149
	.word	.L150
	.word	.L150
	.word	.L150
	.word	.L151
	.word	.L151
	.word	.L151
	.word	.L152
	.word	.L152
	.word	.L159
	.word	.L160
	.word	.L160
.L149:
	mov	r0, #0
	b	.L171
.L150:
	mov	r0, #1
	mov	r1, r0
.L170:
	bl	CURSOR_Select
	b	.L111
.L151:
	sub	r0, r0, #3
.L171:
	mov	r1, #1
	b	.L170
.L152:
	mov	r0, #11
	b	.L171
.L148:
	mov	r0, #1
	b	.L167
.L118:
	ldr	r3, .L173+8
	ldrsh	r0, [r3, #0]
	sub	r3, r0, #1
	cmp	r3, #17
	ldrls	pc, [pc, r3, asl #2]
	b	.L121
.L162:
	.word	.L157
	.word	.L157
	.word	.L157
	.word	.L157
	.word	.L157
	.word	.L157
	.word	.L157
	.word	.L158
	.word	.L158
	.word	.L158
	.word	.L159
	.word	.L159
	.word	.L159
	.word	.L160
	.word	.L160
	.word	.L121
	.word	.L161
	.word	.L161
.L157:
	mov	r0, #8
	b	.L171
.L158:
	add	r0, r0, #3
	b	.L171
.L159:
	mov	r0, #14
	b	.L171
.L160:
	mov	r0, #16
	b	.L171
.L161:
	bl	bad_key_beep
	b	.L111
.L119:
	ldr	r3, .L173+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L124
.L167:
	bl	CURSOR_Up
	b	.L111
.L121:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L111
.L124:
	ldr	r0, .L173+116
	bl	KEY_process_BACK_from_editing_screen
	b	.L111
.L117:
	mov	r0, r5
	mov	r1, r4
	bl	KEY_process_global_keys
.L111:
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, pc}
.L174:
	.align	2
.L173:
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_MANUAL_PROGRAMS_draw_menu
	.word	FDTO_STATION_SELECTION_GRID_draw_screen
	.word	STATION_SELECTION_GRID_process_screen
	.word	GuiVar_MenuScreenToShow
	.word	GuiVar_ManualPStartTime1
	.word	GuiVar_ManualPStartTime1Enabled
	.word	GuiVar_ManualPStartTime2
	.word	GuiVar_ManualPStartTime2Enabled
	.word	GuiVar_ManualPStartTime3
	.word	GuiVar_ManualPStartTime3Enabled
	.word	GuiVar_ManualPStartTime4
	.word	GuiVar_ManualPStartTime4Enabled
	.word	GuiVar_ManualPStartTime5
	.word	GuiVar_ManualPStartTime5Enabled
	.word	GuiVar_ManualPStartTime6
	.word	GuiVar_ManualPStartTime6Enabled
	.word	g_MANUAL_PROGRAMS_start_date
	.word	g_MANUAL_PROGRAMS_stop_date
	.word	GuiVar_ManualPStartDateStr
	.word	2042
	.word	GuiVar_ManualPEndDateStr
	.word	GuiVar_ManualPRunTime
	.word	1139802112
	.word	MANUAL_PROGRAMS_copy_group_into_guivars
	.word	MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars
	.word	MANUAL_PROGRAMS_get_group_at_this_index
	.word	FDTO_MANUAL_PROGRAMS_return_to_menu
.LFE6:
	.size	MANUAL_PROGRAMS_process_group, .-MANUAL_PROGRAMS_process_group
	.section	.text.FDTO_MANUAL_PROGRAMS_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_MANUAL_PROGRAMS_return_to_menu, %function
FDTO_MANUAL_PROGRAMS_return_to_menu:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L176
	ldr	r1, .L176+4
	b	FDTO_GROUP_return_to_menu
.L177:
	.align	2
.L176:
	.word	.LANCHOR0
	.word	MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars
.LFE7:
	.size	FDTO_MANUAL_PROGRAMS_return_to_menu, .-FDTO_MANUAL_PROGRAMS_return_to_menu
	.section	.text.FDTO_MANUAL_PROGRAMS_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_MANUAL_PROGRAMS_draw_menu
	.type	FDTO_MANUAL_PROGRAMS_draw_menu, %function
FDTO_MANUAL_PROGRAMS_draw_menu:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L182
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI7:
	ldr	r2, .L182+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	mov	r3, #796
	bl	xQueueTakeMutexRecursive_debug
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	ldr	r3, .L182+8
	ldr	r1, .L182+12
	str	r3, [sp, #0]
	ldr	r3, .L182+16
	str	r3, [sp, #4]
	mov	r3, #1
	str	r3, [sp, #8]
	mov	r3, #39
	mov	r2, r0
	mov	r0, r4
	bl	FDTO_GROUP_draw_menu
	bl	MANUAL_PROGRAMS_update_will_run
	ldr	r3, .L182+20
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L179
	ldr	r3, .L182+24
	ldr	r0, .L182+12
	ldr	r1, [r3, #0]
	cmp	r1, #0
	movne	r1, #17
	moveq	r1, #18
	bl	FDTO_STATION_SELECTION_GRID_redraw_calling_screen
.L179:
	ldr	r3, .L182
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L183:
	.align	2
.L182:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	MANUAL_PROGRAMS_load_group_name_into_guivar
	.word	.LANCHOR0
	.word	MANUAL_PROGRAMS_copy_group_into_guivars
	.word	g_STATION_SELECTION_GRID_user_pressed_back
	.word	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
.LFE8:
	.size	FDTO_MANUAL_PROGRAMS_draw_menu, .-FDTO_MANUAL_PROGRAMS_draw_menu
	.section	.text.MANUAL_PROGRAMS_process_menu,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_process_menu
	.type	MANUAL_PROGRAMS_process_menu, %function
MANUAL_PROGRAMS_process_menu:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r6, r7, lr}
.LCFI8:
	ldr	r4, .L185
	mov	r6, r0
	mov	r7, r1
	ldr	r2, .L185+4
	mov	r1, #400
	mov	r3, #828
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	ldr	r2, .L185+8
	mov	r1, r7
	str	r2, [sp, #0]
	ldr	r2, .L185+12
	str	r2, [sp, #4]
	ldr	r2, .L185+16
	str	r2, [sp, #8]
	ldr	r2, .L185+20
	str	r2, [sp, #12]
	ldr	r2, .L185+24
	mov	r3, r0
	mov	r0, r6
	bl	GROUP_process_menu
	ldr	r0, [r4, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L186:
	.align	2
.L185:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	MANUAL_PROGRAMS_process_group
	.word	MANUAL_PROGRAMS_add_new_group
	.word	MANUAL_PROGRAMS_get_group_at_this_index
	.word	MANUAL_PROGRAMS_copy_group_into_guivars
	.word	.LANCHOR0
.LFE9:
	.size	MANUAL_PROGRAMS_process_menu, .-MANUAL_PROGRAMS_process_menu
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_manual_programs.c\000"
	.section	.bss.g_MANUAL_PROGRAMS_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_MANUAL_PROGRAMS_editing_group, %object
	.size	g_MANUAL_PROGRAMS_editing_group, 4
g_MANUAL_PROGRAMS_editing_group:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI0-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI3-.LFB0
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI5-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x74
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI7-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI8-.LFB9
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_manual_programs.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xd0
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF10
	.byte	0x1
	.4byte	.LASF11
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1db
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1e9
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x186
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x1b3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.byte	0x50
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST3
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1bb
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x203
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x5
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x302
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x31a
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x33a
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST6
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB5
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB0
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI4
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI6
	.4byte	.LFE6
	.2byte	0x3
	.byte	0x7d
	.sleb128 116
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB8
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB9
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"MANUAL_PROGRAMS_process_start_date\000"
.LASF0:
	.ascii	"MANUAL_PROGRAMS_add_new_group\000"
.LASF1:
	.ascii	"MANUAL_PROGRAMS_process_water_day\000"
.LASF2:
	.ascii	"MANUAL_PROGRAMS_process_start_time\000"
.LASF10:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"MANUAL_PROGRAMS_update_will_run\000"
.LASF6:
	.ascii	"MANUAL_PROGRAMS_process_group\000"
.LASF9:
	.ascii	"MANUAL_PROGRAMS_process_menu\000"
.LASF8:
	.ascii	"FDTO_MANUAL_PROGRAMS_draw_menu\000"
.LASF11:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_manual_programs.c\000"
.LASF7:
	.ascii	"FDTO_MANUAL_PROGRAMS_return_to_menu\000"
.LASF4:
	.ascii	"MANUAL_PROGRAMS_process_end_date\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
