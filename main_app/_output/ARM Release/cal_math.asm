	.file	"cal_math.c"
	.text
.Ltext0:
	.section	.text.__round_UNS16,"ax",%progbits
	.align	2
	.global	__round_UNS16
	.type	__round_UNS16, %function
__round_UNS16:
.LFB0:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	fmsr	s13, r0
	stmfd	sp!, {r0, r1, lr}
.LCFI0:
	fcvtds	d6, s13
	mov	r2, sp
	fmrrd	r0, r1, d6
	bl	modf
	fldd	d7, .L8
	fmdrr	d6, r0, r1
	fcmped	d6, d7
	fldd	d7, [sp, #0]
	fmstat
	flddge	d6, .L8+8
	ftouizdlt	s12, d7
	fadddge	d7, d7, d6
	fmrslt	r0, s12	@ int
	ftouizdge	s13, d7
	fmrsge	r0, s13	@ int
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	ldmfd	sp!, {r2, r3, pc}
.L9:
	.align	2
.L8:
	.word	0
	.word	1071644672
	.word	0
	.word	1072693248
.LFE0:
	.size	__round_UNS16, .-__round_UNS16
	.section	.text.roundf,"ax",%progbits
	.align	2
	.global	roundf
	.type	roundf, %function
roundf:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L15
	fmsr	s15, r0
.LBB2:
	mov	r1, r0
.LBE2:
	and	r3, r0, r3
	mov	r3, r3, lsr #23
	sub	r3, r3, #127
	cmp	r3, #22
	bgt	.L11
	cmp	r3, #0
	bge	.L12
	and	r2, r0, #-2147483648
	cmn	r3, #1
	orreq	r2, r2, #1065353216
	b	.L13
.L12:
.LBB3:
	ldr	r0, .L15+4
	mov	r0, r0, asr r3
	tst	r0, r1
	movne	r2, #4194304
	addne	r2, r1, r2, asr r3
	bicne	r2, r2, r0
	bne	.L13
	b	.L14
.L11:
.LBE3:
	cmp	r3, #128
	faddseq	s15, s15, s15
	b	.L14
.L13:
	fmsr	s15, r2
.L14:
	fmrs	r0, s15
	bx	lr
.L16:
	.align	2
.L15:
	.word	2139095040
	.word	8388607
.LFE1:
	.size	roundf, .-roundf
	.section	.text.__round_float,"ax",%progbits
	.align	2
	.global	__round_float
	.type	__round_float, %function
__round_float:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	fmsr	s14, r1	@ int
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI2:
	fmsr	s16, r0
	fuitod	d9, s14
	ldr	r5, .L18
	mov	r4, #0
	mov	r0, r4
	mov	r1, r5
	fmrrd	r2, r3, d9
	bl	pow
	fcvtds	d8, s16
	fmdrr	d7, r0, r1
	fmuld	d8, d8, d7
	fcvtsd	s15, d8
	fmrs	r0, s15
	bl	roundf
	mov	r1, r5
	fmrrd	r2, r3, d9
	fmsr	s16, r0
	mov	r0, r4
	bl	pow
	fcvtds	d8, s16
	fmdrr	d7, r0, r1
	fdivd	d8, d8, d7
	fcvtsd	s15, d8
	fmrs	r0, s15
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, pc}
.L19:
	.align	2
.L18:
	.word	1076101120
.LFE2:
	.size	__round_float, .-__round_float
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x24
	.byte	0x5
	.uleb128 0x54
	.uleb128 0x5
	.byte	0x5
	.uleb128 0x52
	.uleb128 0x7
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x9
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cal_math.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x59
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x26
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x6e
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xb0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"__round_float\000"
.LASF0:
	.ascii	"__round_UNS16\000"
.LASF1:
	.ascii	"roundf\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/util"
	.ascii	"s/cal_math.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
