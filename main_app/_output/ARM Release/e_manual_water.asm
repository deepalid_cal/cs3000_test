	.file	"e_manual_water.c"
	.text
.Ltext0:
	.section	.text.MANUAL_WATER_update_program,"ax",%progbits
	.align	2
	.type	MANUAL_WATER_update_program, %function
MANUAL_WATER_update_program:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L6
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r2, .L6+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #215
	bl	xQueueTakeMutexRecursive_debug
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r4, r0
	bne	.L2
	mov	r1, #0
	ldr	r0, .L6+8
	bl	GuiLib_GetTextPtr
	b	.L5
.L2:
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	subs	r4, r0, #0
	bne	.L4
	ldr	r0, .L6+4
	mov	r1, #227
	bl	Alert_group_not_found_with_filename
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	mov	r4, r0
.L4:
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L6+12
	str	r0, [r3, #0]
	mov	r0, r4
	bl	nm_GROUP_get_name
.L5:
	mov	r1, r0
	mov	r2, #49
	ldr	r0, .L6+16
	bl	strlcpy
	ldr	r3, .L6
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r4, lr}
	b	Refresh_Screen
.L7:
	.align	2
.L6:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	879
	.word	.LANCHOR0
	.word	GuiVar_ManualWaterProg
.LFE2:
	.size	MANUAL_WATER_update_program, .-MANUAL_WATER_update_program
	.section	.text.MANUAL_WATER_process_program,"ax",%progbits
	.align	2
	.type	MANUAL_WATER_process_program, %function
MANUAL_WATER_process_program:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI1:
	ldr	r4, .L9
	mov	r6, r0
	mov	r1, #400
	ldr	r2, .L9+4
	mov	r3, #245
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r5, .L9+8
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r1, r5
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r3, r0
	mov	r0, r6
	bl	process_uns32
	ldr	r0, [r5, #0]
	bl	MANUAL_WATER_update_program
	ldr	r0, [r4, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L10:
	.align	2
.L9:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR1
.LFE3:
	.size	MANUAL_WATER_process_program, .-MANUAL_WATER_process_program
	.section	.text.MANUAL_WATER_copy_settings_into_guivars,"ax",%progbits
	.align	2
	.type	MANUAL_WATER_copy_settings_into_guivars, %function
MANUAL_WATER_copy_settings_into_guivars:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L18
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI2:
	ldr	r7, .L18+4
	mov	r1, #400
	ldr	r2, .L18+8
	mov	r5, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L18+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, .L18+16
	ldr	r1, [r7, #0]
	ldr	r0, [r4, #0]
	sub	r1, r1, #1
	bl	nm_STATION_get_pointer_to_station
	subs	r6, r0, #0
	beq	.L12
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L18+20
	bl	strlcpy
	ldr	r1, [r7, #0]
	mov	r3, #4
	ldr	r2, .L18+24
	sub	r1, r1, #1
	ldr	r0, [r4, #0]
	bl	STATION_get_station_number_string
	ldr	r1, .L18+28
	ldr	r0, [r4, #0]
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	mov	r0, r6
	bl	nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar
	ldr	r3, .L18+32
	str	r0, [r3, #0]	@ float
	b	.L13
.L12:
	ldr	r1, .L18+36
	mov	r2, #48
	ldr	r0, .L18+20
	bl	strlcpy
	ldr	r1, .L18+36
	mov	r2, #4
	ldr	r0, .L18+24
	bl	strlcpy
	mov	r2, #49
	ldr	r0, .L18+28
	ldr	r1, .L18+36
	bl	strlcpy
	ldr	r3, .L18+32
	mov	r2, #0
	str	r2, [r3, #0]	@ float
.L13:
	cmp	r5, #1
	bne	.L14
	ldr	r4, .L18+40
	ldr	r5, [r4, #0]
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r5, r0
	bne	.L15
	mov	r1, #0
	ldr	r0, .L18+44
	bl	GuiLib_GetTextPtr
	mov	r1, r0
	ldr	r0, .L18+48
	b	.L17
.L15:
	ldr	r0, [r4, #0]
	bl	STATION_GROUP_get_group_at_this_index
	subs	r4, r0, #0
	beq	.L16
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L18+52
	str	r0, [r3, #0]
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r1, r0
	ldr	r0, .L18+56
.L17:
	mov	r2, #49
	bl	strlcpy
	b	.L14
.L16:
	ldr	r0, .L18+8
	ldr	r1, .L18+60
	bl	Alert_group_not_found_with_filename
.L14:
	ldr	r3, .L18
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L19:
	.align	2
.L18:
	.word	list_program_data_recursive_MUTEX
	.word	GuiVar_StationInfoNumber
	.word	.LC0
	.word	291
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_StationDescription
	.word	GuiVar_StationInfoNumber_str
	.word	GuiVar_StationInfoControllerName
	.word	GuiVar_ManualWaterRunTime
	.word	.LC1
	.word	.LANCHOR1
	.word	879
	.word	GuiVar_ComboBoxItemString
	.word	.LANCHOR0
	.word	GuiVar_ManualWaterProg
	.word	334
.LFE6:
	.size	MANUAL_WATER_copy_settings_into_guivars, .-MANUAL_WATER_copy_settings_into_guivars
	.section	.text.FDTO_MANUAL_WATER_show_program_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_MANUAL_WATER_show_program_dropdown, %function
FDTO_MANUAL_WATER_show_program_dropdown:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI3:
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r3, .L21
	ldr	r1, .L21+4
	ldr	r3, [r3, #0]
	add	r2, r0, #1
	ldr	r0, .L21+8
	ldr	lr, [sp], #4
	b	FDTO_COMBOBOX_show
.L22:
	.align	2
.L21:
	.word	.LANCHOR1
	.word	nm_MANUAL_WATER_load_program_name_into_guivar
	.word	735
.LFE5:
	.size	FDTO_MANUAL_WATER_show_program_dropdown, .-FDTO_MANUAL_WATER_show_program_dropdown
	.section	.text.nm_MANUAL_WATER_load_program_name_into_guivar,"ax",%progbits
	.align	2
	.type	nm_MANUAL_WATER_load_program_name_into_guivar, %function
nm_MANUAL_WATER_load_program_name_into_guivar:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI4:
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r4, r0
	bcs	.L24
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	cmp	r0, #0
	beq	.L25
	bl	nm_GROUP_get_name
	b	.L26
.L25:
.LBB6:
	ldr	r0, .L27
	ldr	r1, .L27+4
.LBE6:
	ldmfd	sp!, {r4, lr}
.LBB7:
	b	Alert_group_not_found_with_filename
.L24:
.LBE7:
	ldr	r0, .L27+8
	mov	r1, #0
	bl	GuiLib_GetTextPtr
.L26:
	mov	r1, r0
	ldr	r0, .L27+12
	mov	r2, #49
	ldmfd	sp!, {r4, lr}
	b	strlcpy
.L28:
	.align	2
.L27:
	.word	.LC0
	.word	269
	.word	879
	.word	GuiVar_ComboBoxItemString
.LFE4:
	.size	nm_MANUAL_WATER_load_program_name_into_guivar, .-nm_MANUAL_WATER_load_program_name_into_guivar
	.section	.text.FDTO_MANUAL_WATER_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_MANUAL_WATER_draw_screen
	.type	FDTO_MANUAL_WATER_draw_screen, %function
FDTO_MANUAL_WATER_draw_screen:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L32
	stmfd	sp!, {r4, r5, lr}
.LCFI5:
	ldrnesh	r4, [r3, #0]
	mov	r5, r0
	bne	.L31
	bl	STATION_find_first_available_station_and_init_station_number_GuiVars
	ldr	r3, .L32+4
	mov	r2, #0
	mov	r4, #3
	str	r2, [r3, #0]
.L31:
	mov	r0, r5
	bl	MANUAL_WATER_copy_settings_into_guivars
	mov	r0, #42
	mov	r1, r4
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L33:
	.align	2
.L32:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR2
.LFE7:
	.size	FDTO_MANUAL_WATER_draw_screen, .-FDTO_MANUAL_WATER_draw_screen
	.section	.text.MANUAL_WATER_process_screen,"ax",%progbits
	.align	2
	.global	MANUAL_WATER_process_screen
	.type	MANUAL_WATER_process_screen, %function
MANUAL_WATER_process_screen:
.LFB8:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L94+16
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI6:
	ldrsh	r2, [r3, #0]
	ldr	r3, .L94+20
	sub	sp, sp, #48
.LCFI7:
	cmp	r2, r3
	mov	r4, r0
	bne	.L82
	ldr	r5, .L94+24
	mov	r1, r5
	bl	COMBO_BOX_key_press
	cmp	r4, #2
	cmpne	r4, #67
	bne	.L34
	ldr	r0, [r5, #0]
	bl	MANUAL_WATER_update_program
	b	.L34
.L82:
	cmp	r0, #4
	beq	.L43
	bhi	.L47
	cmp	r0, #1
	beq	.L40
	bcc	.L39
	cmp	r0, #2
	beq	.L41
	cmp	r0, #3
	bne	.L38
	b	.L42
.L47:
	cmp	r0, #67
	beq	.L45
	bhi	.L48
	cmp	r0, #16
	beq	.L44
	cmp	r0, #20
	bne	.L38
	b	.L44
.L48:
	cmp	r0, #80
	beq	.L46
	cmp	r0, #84
	bne	.L38
	b	.L46
.L41:
	ldr	r3, .L94+28
	ldrh	r4, [r3, #0]
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	beq	.L51
	cmp	r3, #5
	beq	.L50
	cmp	r3, #3
	bne	.L74
	b	.L50
.L51:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L94+32
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L34
.L50:
.LBB11:
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	bne	.L52
	bl	bad_key_beep
	ldr	r0, .L94+36
	b	.L87
.L52:
	ldr	r3, .L94+40
	ldr	r5, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	cmp	r5, r0
	bne	.L53
	ldr	r3, .L94+44
	ldr	r3, [r3, #112]
	cmp	r3, #1
	bne	.L53
	bl	bad_key_beep
	mov	r0, #604
.L87:
	bl	DIALOG_draw_ok_dialog
	b	.L34
.L53:
	bl	IRRI_COMM_if_any_2W_cable_is_over_heated
	cmp	r0, #0
	beq	.L54
	bl	bad_key_beep
	mov	r0, #640
	b	.L87
.L54:
	cmp	r4, #3
	bne	.L55
	ldr	r4, .L94+48
	ldr	r5, .L94+40
	ldr	r1, [r4, #0]
	ldr	r0, [r5, #0]
	sub	r1, r1, #1
	bl	nm_STATION_get_pointer_to_station
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	r6, r0, #0
	bne	.L56
	bl	bad_key_beep
	ldr	r0, .L94+52
	bl	RemovePathFromFileName
	mov	r2, #150
	mov	r1, r0
	ldr	r0, .L94+56
	bl	Alert_Message_va
	b	.L34
.L56:
	add	r0, r6, #516
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	cmp	r0, #0
	beq	.L57
	bl	bad_key_beep
	mov	r0, #612
	b	.L87
.L57:
	ldrb	r3, [r6, #468]	@ zero_extendqisi2
	ands	r2, r3, #64
	beq	.L58
	bl	bad_key_beep
	ldr	r0, .L94+60
	b	.L87
.L58:
	ldr	r3, .L94+64
	flds	s15, .L94
	str	r2, [r3, #24]
	ldr	r2, [r5, #0]
	str	r2, [r3, #28]
	ldr	r2, [r4, #0]
	sub	r2, r2, #1
	str	r2, [r3, #32]
	ldr	r2, .L94+68
	flds	s14, [r2, #0]
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r3, #36]	@ int
	b	.L59
.L55:
	ldr	r3, .L94+24
	ldr	r4, [r3, #0]
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r3, .L94+64
	cmp	r4, r0
	movcc	r2, #1
	strcc	r2, [r3, #24]
	ldrcc	r2, .L94+72
	movcs	r2, #2
	ldrcc	r2, [r2, #0]
	strcs	r2, [r3, #24]
	strcc	r2, [r3, #40]
	b	.L59
.L46:
.LBE11:
	ldr	r3, .L94+28
	ldrsh	r3, [r3, #0]
	cmp	r3, #2
	beq	.L63
	cmp	r3, #4
	beq	.L64
	cmp	r3, #0
	bne	.L74
	ldr	r5, .L94+76
	bl	good_key_beep
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L94+52
	ldr	r3, .L94+80
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L94+40
	ldr	r1, .L94+48
	cmp	r4, #84
	b	.L92
.L63:
.LBB12:
	cmp	r1, #144
	movhi	r3, #8
	bhi	.L67
	cmp	r1, #72
	movhi	r3, #4
	bhi	.L67
	cmp	r1, #48
	movls	r3, #1
	movhi	r3, #2
.L67:
	ldr	r5, .L94+68
	flds	s15, .L94+4
	add	r1, sp, #48
	flds	s14, [r5, #0]
	mov	r2, #0
	mov	r0, r4
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fstmdbs	r1!, {s15}	@ int
	str	r3, [sp, #0]
	ldr	r3, .L94+84
	str	r2, [sp, #4]
	bl	process_uns32
	flds	s13, [sp, #44]	@ int
	fuitod	d7, s13
	fldd	d6, .L94+8
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r5, #0]
	bl	Refresh_Screen
	b	.L34
.L64:
.LBE12:
	mov	r0, r4
	b	.L89
.L44:
	bl	good_key_beep
	ldr	r3, .L94+28
	ldrh	r2, [r3, #0]
	cmp	r2, #5
	bhi	.L34
	ldrsh	r3, [r3, #0]
	mov	r2, #1
	mov	r3, r2, asl r3
	tst	r3, #13
	bne	.L68
	tst	r3, #48
	bne	.L69
	b	.L34
.L68:
	ldr	r5, .L94+76
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L94+52
	ldr	r3, .L94+88
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L94+40
	ldr	r1, .L94+48
	cmp	r4, #20
.L92:
	bne	.L70
	bl	STATION_get_next_available_station
	b	.L71
.L70:
	bl	STATION_get_prev_available_station
.L71:
	mov	r0, #0
	bl	MANUAL_WATER_copy_settings_into_guivars
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #0
	bl	Redraw_Screen
	b	.L34
.L69:
	cmp	r4, #20
	movne	r0, #80
	moveq	r0, #84
.L89:
	bl	MANUAL_WATER_process_program
	b	.L34
.L43:
	ldr	r3, .L94+28
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L74
	cmp	r3, #2
	movne	r0, #1
	bne	.L40
	b	.L93
.L74:
	bl	bad_key_beep
	b	.L34
.L93:
	ldr	r3, .L94+92
	ldr	r0, [r3, #0]
	b	.L88
.L39:
	ldr	r3, .L94+28
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L42
	ldr	r2, .L94+92
	mov	r0, #2
	str	r3, [r2, #0]
.L88:
	mov	r1, #1
	bl	CURSOR_Select
	b	.L34
.L40:
	bl	CURSOR_Up
	b	.L34
.L42:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L34
.L45:
	ldr	r3, .L94+96
	mov	r2, #7
	str	r2, [r3, #0]
.L38:
	mov	r0, r4
	bl	KEY_process_global_keys
	b	.L34
.L59:
.LBB13:
	ldr	r3, .L94+64
	mov	r2, #1
	str	r2, [r3, #20]
	bl	IRRI_DETAILS_jump_to_irrigation_details
.L34:
.LBE13:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, pc}
.L95:
	.align	2
.L94:
	.word	1114636288
	.word	1092616192
	.word	0
	.word	1076101120
	.word	GuiLib_CurStructureNdx
	.word	735
	.word	.LANCHOR1
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_MANUAL_WATER_show_program_dropdown
	.word	591
	.word	GuiVar_StationInfoBoxIndex
	.word	tpmicro_comm
	.word	GuiVar_StationInfoNumber
	.word	.LC0
	.word	.LC2
	.word	611
	.word	irri_comm
	.word	GuiVar_ManualWaterRunTime
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	415
	.word	9999
	.word	455
	.word	.LANCHOR2
	.word	GuiVar_MenuScreenToShow
.LFE8:
	.size	MANUAL_WATER_process_screen, .-MANUAL_WATER_process_screen
	.section	.bss.g_MANUAL_WATER_program_GID,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_MANUAL_WATER_program_GID, %object
	.size	g_MANUAL_WATER_program_GID, 4
g_MANUAL_WATER_program_GID:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_manual_water.c\000"
.LC1:
	.ascii	"\000"
.LC2:
	.ascii	"System not found : %s, %u\000"
	.section	.bss.g_MANUAL_WATER_program_index,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_MANUAL_WATER_program_index, %object
	.size	g_MANUAL_WATER_program_index, 4
g_MANUAL_WATER_program_index:
	.space	4
	.section	.bss.g_MANUAL_WATER_last_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_MANUAL_WATER_last_cursor_pos, %object
	.size	g_MANUAL_WATER_last_cursor_pos, 4
g_MANUAL_WATER_last_cursor_pos:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI2-.LFB6
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI3-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI5-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI6-.LFB8
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_manual_water.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xbf
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF9
	.byte	0x1
	.4byte	.LASF10
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0xff
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x4d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0xd3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0xf3
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x11d
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x117
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x5
	.4byte	0x21
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x157
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST5
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.byte	0x6e
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x16f
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST6
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB6
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB7
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB8
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI7
	.4byte	.LFE8
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF6:
	.ascii	"MANUAL_WATER_process_run_key\000"
.LASF4:
	.ascii	"MANUAL_WATER_copy_settings_into_guivars\000"
.LASF5:
	.ascii	"FDTO_MANUAL_WATER_show_program_dropdown\000"
.LASF10:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_manual_water.c\000"
.LASF9:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"MANUAL_WATER_update_program\000"
.LASF0:
	.ascii	"nm_MANUAL_WATER_load_program_name_into_guivar\000"
.LASF1:
	.ascii	"MANUAL_WATER_process_total_min_10u\000"
.LASF3:
	.ascii	"MANUAL_WATER_process_program\000"
.LASF7:
	.ascii	"FDTO_MANUAL_WATER_draw_screen\000"
.LASF8:
	.ascii	"MANUAL_WATER_process_screen\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
