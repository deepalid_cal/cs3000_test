	.file	"tpmicro_data.c"
	.text
.Ltext0:
	.section	.text.nm_tpmicro_data_updater,"ax",%progbits
	.align	2
	.type	nm_tpmicro_data_updater, %function
nm_tpmicro_data_updater:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	subs	r2, r0, #0
	str	lr, [sp, #-4]!
.LCFI0:
	bne	.L2
	ldr	r0, .L3
	mov	r1, r2
	ldr	lr, [sp], #4
	b	Alert_Message_va
.L2:
	ldr	r0, .L3+4
	mov	r1, #0
	bl	Alert_Message_va
	ldr	r0, .L3+8
	ldr	lr, [sp], #4
	b	Alert_Message
.L4:
	.align	2
.L3:
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE2:
	.size	nm_tpmicro_data_updater, .-nm_tpmicro_data_updater
	.section	.text.nm_tpmicro_data_structure_init,"ax",%progbits
	.align	2
	.type	nm_tpmicro_data_structure_init, %function
nm_tpmicro_data_structure_init:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L6
	ldr	r2, .L6+4
	mov	r1, #0
	b	memset
.L7:
	.align	2
.L6:
	.word	.LANCHOR0
	.word	5484
.LFE1:
	.size	nm_tpmicro_data_structure_init, .-nm_tpmicro_data_structure_init
	.section	.text.init_tpmicro_data_on_boot,"ax",%progbits
	.align	2
	.global	init_tpmicro_data_on_boot
	.type	init_tpmicro_data_on_boot, %function
init_tpmicro_data_on_boot:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r4, .L9
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L9+4
	mov	r3, #47
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L9+8
	mov	r2, #1
	str	r2, [r3, #0]
	str	r2, [r3, #4]
	mov	r1, #5056
	mov	r2, #3
	str	r2, [r3, r1]
	add	r1, r1, #4
	str	r2, [r3, r1]
	str	r2, [r3, #204]
	ldr	r2, .L9+12
	mov	r5, #0
	str	r5, [r3, r2]
	add	r2, r2, #12
	str	r5, [r3, r2]
	sub	r2, r2, #56
	str	r5, [r3, r2]
	add	r2, r2, #8
	str	r5, [r3, r2]
	add	r2, r2, #4
	str	r5, [r3, r2]
	add	r2, r2, #4
	str	r5, [r3, r2]
	add	r2, r2, #4
	str	r5, [r3, r2]
	add	r2, r2, #4
	str	r5, [r3, r2]
	add	r2, r2, #24
	str	r5, [r3, r2]
	sub	r2, r2, #20
	str	r5, [r3, r2]
	add	r2, r2, #4
	str	r5, [r3, r2]
	str	r5, [r3, #12]
	str	r5, [r3, #16]
	str	r5, [r3, #32]
	str	r5, [r3, #20]
	mov	r1, r5
	mov	r2, #128
	ldr	r0, .L9+16
	bl	memset
	mov	r1, r5
	mov	r2, #264
	ldr	r0, .L9+20
	bl	memset
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L10:
	.align	2
.L9:
	.word	tpmicro_data_recursive_MUTEX
	.word	.LC3
	.word	.LANCHOR0
	.word	5064
	.word	.LANCHOR0+5092
	.word	.LANCHOR0+5220
.LFE0:
	.size	init_tpmicro_data_on_boot, .-init_tpmicro_data_on_boot
	.section	.text.init_file_tpmicro_data,"ax",%progbits
	.align	2
	.global	init_file_tpmicro_data
	.type	init_file_tpmicro_data, %function
init_file_tpmicro_data:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI2:
	ldr	r3, .L12
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, .L12+4
	ldr	r1, .L12+8
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r3, [sp, #4]
	ldr	r3, .L12+12
	str	r3, [sp, #8]
	ldr	r3, .L12+16
	str	r3, [sp, #12]
	mov	r3, #8
	str	r3, [sp, #16]
	ldr	r3, .L12+20
	bl	FLASH_FILE_find_or_create_variable_file
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.L13:
	.align	2
.L12:
	.word	5484
	.word	tpmicro_data_recursive_MUTEX
	.word	.LANCHOR1
	.word	nm_tpmicro_data_updater
	.word	nm_tpmicro_data_structure_init
	.word	.LANCHOR0
.LFE3:
	.size	init_file_tpmicro_data, .-init_file_tpmicro_data
	.section	.text.save_file_tpmicro_data,"ax",%progbits
	.align	2
	.global	save_file_tpmicro_data
	.type	save_file_tpmicro_data, %function
save_file_tpmicro_data:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L15
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI3:
	ldr	r1, .L15+4
	str	r3, [sp, #0]
	ldr	r3, .L15+8
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r3, [sp, #4]
	mov	r3, #8
	str	r3, [sp, #8]
	ldr	r3, .L15+12
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L16:
	.align	2
.L15:
	.word	5484
	.word	.LANCHOR1
	.word	tpmicro_data_recursive_MUTEX
	.word	.LANCHOR0
.LFE4:
	.size	save_file_tpmicro_data, .-save_file_tpmicro_data
	.section	.text.TPMICRO_DATA_extract_reported_errors_out_of_msg_from_tpmicro,"ax",%progbits
	.align	2
	.global	TPMICRO_DATA_extract_reported_errors_out_of_msg_from_tpmicro
	.type	TPMICRO_DATA_extract_reported_errors_out_of_msg_from_tpmicro, %function
TPMICRO_DATA_extract_reported_errors_out_of_msg_from_tpmicro:
.LFB5:
	@ args = 0, pretend = 0, frame = 132
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI4:
.LBB2:
	ldr	r7, .L21
.LBE2:
	mov	r4, r0
	sub	sp, sp, #132
.LCFI5:
	ldr	r1, [r4, #0]
	add	r0, sp, #128
	mov	r2, #4
	bl	memcpy
	ldr	r3, [r4, #0]
.LBB3:
	ldr	r5, .L21+4
.LBE3:
	add	r3, r3, #4
	str	r3, [r4, #0]
	mov	r4, #0
.L19:
	mov	r6, #1
	ldr	r3, [sp, #128]
	mov	r6, r6, asl r4
	tst	r6, r3
	beq	.L18
.LBB4:
	mov	r1, #128
	ldr	r2, .L21+8
	ldr	r3, [r7, r4, asl #2]
	mov	r0, sp
	bl	snprintf
	mov	r0, sp
	bl	Alert_message_on_tpmicro_pile_M
	ldr	r2, .L21+12
	mov	r3, #340
	ldr	r0, [r5, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L21+16
	ldr	r0, [r5, #0]
	ldr	r2, [r3, #8]
	orr	r6, r6, r2
	str	r6, [r3, #8]
	bl	xQueueGiveMutexRecursive
.L18:
.LBE4:
	add	r4, r4, #1
	cmp	r4, #32
	bne	.L19
	mov	r0, #4
	add	sp, sp, #132
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L22:
	.align	2
.L21:
	.word	.LANCHOR2
	.word	tpmicro_data_recursive_MUTEX
	.word	.LC4
	.word	.LC3
	.word	.LANCHOR0
.LFE5:
	.size	TPMICRO_DATA_extract_reported_errors_out_of_msg_from_tpmicro, .-TPMICRO_DATA_extract_reported_errors_out_of_msg_from_tpmicro
	.section	.text.find_two_wire_decoder,"ax",%progbits
	.align	2
	.global	find_two_wire_decoder
	.type	find_two_wire_decoder, %function
find_two_wire_decoder:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI6:
	subs	r4, r0, #0
	beq	.L24
	ldr	r2, .L29
	mov	r3, #0
	mov	r1, r2
.L27:
	ldr	r0, [r2, #220]
	cmp	r0, r4
	bne	.L25
	mov	r0, #60
	mla	r3, r0, r3, r1
	add	r0, r3, #220
	ldmfd	sp!, {r4, pc}
.L25:
	add	r3, r3, #1
	cmp	r3, #80
	add	r2, r2, #60
	bne	.L27
	ldr	r0, .L29+4
	mov	r1, r4
	bl	Alert_Message_va
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L24:
	ldr	r0, .L29+8
	bl	Alert_Message
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L30:
	.align	2
.L29:
	.word	.LANCHOR0
	.word	.LC5
	.word	.LC6
.LFE6:
	.size	find_two_wire_decoder, .-find_two_wire_decoder
	.global	TPMICRO_DATA_FILENAME
	.global	tpmicro_data
	.section	.rodata.TPMICRO_DATA_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	TPMICRO_DATA_FILENAME, %object
	.size	TPMICRO_DATA_FILENAME, 13
TPMICRO_DATA_FILENAME:
	.ascii	"TPMICRO_DATA\000"
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"TPMICRO DATA file unexpd update %u\000"
.LC1:
	.ascii	"TPMICRO DATA file update : to revision %u from %u\000"
.LC2:
	.ascii	"TPMICRO DATA updater error\000"
.LC3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/tp_b"
	.ascii	"oard/tpmicro_data.c\000"
.LC4:
	.ascii	"from error bf - %s\000"
.LC5:
	.ascii	"Decoder not found sn: %08x\000"
.LC6:
	.ascii	"Looking for Decoder with ser num of 0!\000"
.LC7:
	.ascii	"outgoing message too big\000"
.LC8:
	.ascii	"flow readings buffer overflow\000"
.LC9:
	.ascii	"flow reading out of range\000"
.LC10:
	.ascii	"asked to turn on too many stations\000"
.LC11:
	.ascii	"asked to turn on invalid stations\000"
.LC12:
	.ascii	"crc failed - main\000"
.LC13:
	.ascii	"incoming message has invalid route\000"
.LC14:
	.ascii	"incoming message too big\000"
.LC15:
	.ascii	"close to main stack overflow\000"
.LC16:
	.ascii	"out of memory pool\000"
.LC17:
	.ascii	"bumped to next memory block size\000"
.LC18:
	.ascii	"queue full\000"
.LC19:
	.ascii	"main ring buffer overflow\000"
.LC20:
	.ascii	"m1 ring buffer overflow\000"
.LC21:
	.ascii	"m2 ring buffer overflow\000"
.LC22:
	.ascii	"_m1 packet too big\000"
.LC23:
	.ascii	"_m2 packet too big\000"
.LC24:
	.ascii	"uart dma error\000"
.LC25:
	.ascii	"serial data xmit error\000"
.LC26:
	.ascii	"ee mirror test failed\000"
.LC27:
	.ascii	"bad msg from main\000"
.LC28:
	.ascii	"crc failed - m1\000"
.LC29:
	.ascii	"crc failed - m2\000"
.LC30:
	.ascii	"prior turn-ON request incomplete\000"
.LC31:
	.ascii	"i2c states don't match\000"
.LC32:
	.ascii	"too many POCs!\000"
	.section	.rodata.error_strings,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	error_strings, %object
	.size	error_strings, 104
error_strings:
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	.LC23
	.word	.LC24
	.word	.LC25
	.word	.LC26
	.word	.LC27
	.word	.LC28
	.word	.LC29
	.word	.LC30
	.word	.LC31
	.word	.LC32
	.section	.bss.tpmicro_data,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	tpmicro_data, %object
	.size	tpmicro_data, 5484
tpmicro_data:
	.space	5484
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x98
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI6-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/tp_board/tpmicro_data.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xa9
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF7
	.byte	0x1
	.4byte	.LASF8
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0xa2
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x98
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x2d
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xd7
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xe5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x137
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x16a
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x3
	.byte	0x7d
	.sleb128 152
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB6
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"init_file_tpmicro_data\000"
.LASF4:
	.ascii	"save_file_tpmicro_data\000"
.LASF0:
	.ascii	"nm_tpmicro_data_updater\000"
.LASF1:
	.ascii	"nm_tpmicro_data_structure_init\000"
.LASF8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/tp_b"
	.ascii	"oard/tpmicro_data.c\000"
.LASF5:
	.ascii	"TPMICRO_DATA_extract_reported_errors_out_of_msg_fro"
	.ascii	"m_tpmicro\000"
.LASF6:
	.ascii	"find_two_wire_decoder\000"
.LASF7:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"init_tpmicro_data_on_boot\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
