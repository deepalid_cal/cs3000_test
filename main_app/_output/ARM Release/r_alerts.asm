	.file	"r_alerts.c"
	.text
.Ltext0:
	.section	.text.ALERTS_get_currently_displayed_pile,"ax",%progbits
	.align	2
	.type	ALERTS_get_currently_displayed_pile, %function
ALERTS_get_currently_displayed_pile:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L7
	str	lr, [sp, #-4]!
.LCFI0:
	ldr	r1, [r3, #0]
	cmp	r1, #0
	beq	.L3
	cmp	r1, #1
	ldreq	r0, .L7+4
	ldreq	pc, [sp], #4
	cmp	r1, #2
	ldreq	r0, .L7+8
	ldreq	pc, [sp], #4
	cmp	r1, #3
	beq	.L6
.LBB4:
	ldr	r0, .L7+12
	bl	Alert_Message_va
.L3:
.LBE4:
	ldr	r0, .L7+16
	ldr	pc, [sp], #4
.L6:
	ldr	r0, .L7+20
	ldr	pc, [sp], #4
.L8:
	.align	2
.L7:
	.word	.LANCHOR0
	.word	alerts_struct_changes
	.word	alerts_struct_tp_micro
	.word	.LC0
	.word	alerts_struct_user
	.word	alerts_struct_engineering
.LFE6:
	.size	ALERTS_get_currently_displayed_pile, .-ALERTS_get_currently_displayed_pile
	.section	.text.nm_ALERTS_parse_alert_and_return_length,"ax",%progbits
	.align	2
	.global	nm_ALERTS_parse_alert_and_return_length
	.type	nm_ALERTS_parse_alert_and_return_length, %function
nm_ALERTS_parse_alert_and_return_length:
.LFB0:
	@ args = 4, pretend = 0, frame = 44
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI1:
	sub	sp, sp, #52
.LCFI2:
	mov	r4, r1
	mov	sl, r2
	add	r1, sp, #50
	add	r2, sp, #80
	mov	r7, r3
	mov	r3, #2
	mov	r5, r0
	bl	ALERTS_pull_object_off_pile
	mov	r3, #6
	add	r1, sp, #24
	add	r2, sp, #80
	ldrh	r8, [sp, #50]
	mov	r6, r0
	mov	r0, r5
	bl	ALERTS_pull_object_off_pile
	ldr	r3, [sp, #24]
	cmp	r4, #200
	bic	r3, r3, #-16777216
	bic	r3, r3, #12582912
	str	r3, [sp, #24]
	add	r6, r0, r6
	bne	.L10
	ldr	r3, .L13
	ldrh	r2, [sp, #28]
	ldr	r3, [r3, #0]
	cmp	r3, #2
	bne	.L11
.LBB5:
	add	r3, sp, #44
	mov	r0, r2
	str	r3, [sp, #0]
	add	r1, sp, #32
	add	r2, sp, #36
	add	r3, sp, #40
	bl	DateToDMY
	ldr	r3, [sp, #32]
	ldr	r0, .L13+4
	str	r3, [sp, #0]
	mov	r1, #6
	ldr	r3, [sp, #36]
	ldr	r2, .L13+8
	bl	snprintf
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r3, #0
	b	.L12
.L11:
.LBE5:
	mov	r3, #250
	str	r3, [sp, #0]
	mov	r1, #16
	mov	r3, #150
	add	r0, sp, #8
	bl	GetDateStr
	mov	r2, #6
	mov	r1, r0
	ldr	r0, .L13+4
	bl	strlcpy
	mov	r3, #0
	str	r3, [sp, #0]
.L12:
	mov	r1, #16
	ldr	r2, [sp, #24]
	add	r0, sp, #8
	str	r3, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r2, #10
	mov	r1, r0
	ldr	r0, .L13+12
	bl	strlcpy
.L10:
	ldr	r3, [sp, #80]
	mov	r0, r8
	str	r3, [sp, #4]
	mov	r1, r5
	mov	r2, r4
	mov	r3, sl
	str	r7, [sp, #0]
	bl	nm_ALERT_PARSING_parse_alert_and_return_length
	add	r0, r6, r0
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L14:
	.align	2
.L13:
	.word	.LANCHOR0
	.word	GuiVar_AlertDate
	.word	.LC1
	.word	GuiVar_AlertTime
.LFE0:
	.size	nm_ALERTS_parse_alert_and_return_length, .-nm_ALERTS_parse_alert_and_return_length
	.global	__udivsi3
	.section	.text.nm_ALERTS_roll_and_add_new_display_start_index,"ax",%progbits
	.align	2
	.global	nm_ALERTS_roll_and_add_new_display_start_index
	.type	nm_ALERTS_roll_and_add_new_display_start_index, %function
nm_ALERTS_roll_and_add_new_display_start_index:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI3:
	ldr	r3, .L19
	mov	r5, r2
	ldr	r2, [r0, #16]
	ldr	r6, [r1, #3644]
	add	r3, r3, r2, asl #3
	mov	r4, r1
	ldr	r0, [r3, #4]
	mov	r1, #9
	bl	__udivsi3
	sub	r0, r0, #1
	cmp	r6, r0
	bls	.L16
	ldr	r3, .L19+4
	mov	r2, #0
	str	r2, [r3, #32]
	mov	r3, #5
	str	r3, [r4, #3644]
	ldr	r0, .L19+8
	ldr	r1, .L19+12
	mov	r2, #452
	bl	__assert
	b	.L17
.L16:
	cmp	r6, #0
	beq	.L17
	add	r3, r6, #908
	add	r3, r3, #2
	add	r3, r4, r3, asl #1
.L18:
	ldrh	r2, [r3, #-2]
	cmp	r6, #1
	strh	r2, [r3], #-2	@ movhi
	subne	r6, r6, #1
	bne	.L18
.L17:
	ldr	r3, .L19+16
	strh	r5, [r4, r3]	@ movhi
	ldr	r3, [r4, #3644]
	add	r3, r3, #1
	str	r3, [r4, #3644]
	ldmfd	sp!, {r4, r5, r6, pc}
.L20:
	.align	2
.L19:
	.word	alert_piles
	.word	alerts_struct_engineering
	.word	.LC2
	.word	.LC3
	.word	1820
.LFE3:
	.size	nm_ALERTS_roll_and_add_new_display_start_index, .-nm_ALERTS_roll_and_add_new_display_start_index
	.section	.text.ALERT_this_alert_is_to_be_displayed,"ax",%progbits
	.align	2
	.global	ALERT_this_alert_is_to_be_displayed
	.type	ALERT_this_alert_is_to_be_displayed, %function
ALERT_this_alert_is_to_be_displayed:
.LFB5:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L24
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI4:
	ldr	r3, [r3, #0]
	mov	r5, r0
	cmp	r3, #0
	mov	r4, r1
	str	r2, [sp, #0]
	movne	r0, #1
	bne	.L22
	add	r1, sp, #10
	mov	r2, sp
	mov	r3, #2
	bl	ALERTS_pull_object_off_pile
	mov	r0, r5
	add	r1, sp, #4
	mov	r2, sp
	mov	r3, #6
	bl	ALERTS_pull_object_off_pile
	rsbs	r0, r4, #1
	movcc	r0, #0
.L22:
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L25:
	.align	2
.L24:
	.word	.LANCHOR0
.LFE5:
	.size	ALERT_this_alert_is_to_be_displayed, .-ALERT_this_alert_is_to_be_displayed
	.section	.text.nm_ALERTS_restart_display_start_indicies,"ax",%progbits
	.align	2
	.type	nm_ALERTS_restart_display_start_indicies, %function
nm_ALERTS_restart_display_start_indicies:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI5:
	ldr	r8, .L30
	mov	r5, #0
	mov	r7, r0
	mov	r4, r1
	str	r5, [r1, #3644]
	mov	r6, r1
	b	.L27
.L29:
	mov	r0, r7
	ldr	r1, [r8, #0]
	ldrh	r2, [r6], #2
	bl	ALERT_this_alert_is_to_be_displayed
	cmp	r0, #1
	bne	.L28
	ldr	r3, [r4, #3644]
	ldrh	r1, [r6, #-2]
	add	r2, r3, #908
	add	r2, r4, r2, asl #1
	add	r3, r3, #1
	strh	r1, [r2, #4]	@ movhi
	str	r3, [r4, #3644]
.L28:
	add	r5, r5, #1
.L27:
	ldr	r3, [r7, #28]
	cmp	r5, r3
	bcc	.L29
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L31:
	.align	2
.L30:
	.word	.LANCHOR1
.LFE1:
	.size	nm_ALERTS_restart_display_start_indicies, .-nm_ALERTS_restart_display_start_indicies
	.section	.text.nm_ALERTS_change_filter,"ax",%progbits
	.align	2
	.global	nm_ALERTS_change_filter
	.type	nm_ALERTS_change_filter, %function
nm_ALERTS_change_filter:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L34
	cmp	r1, #0
	stmfd	sp!, {r4, lr}
.LCFI6:
	str	r0, [r3, #0]
	ldmnefd	sp!, {r4, pc}
	ldr	r4, .L34+4
	ldr	r0, .L34+8
	mov	r1, r4
	bl	nm_ALERTS_restart_display_start_indicies
	mov	r3, #1
	str	r3, [r4, #3652]
	ldmfd	sp!, {r4, pc}
.L35:
	.align	2
.L34:
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	alerts_struct_user
.LFE4:
	.size	nm_ALERTS_change_filter, .-nm_ALERTS_change_filter
	.section	.text.ALERTS_get_display_structure_for_pile,"ax",%progbits
	.align	2
	.global	ALERTS_get_display_structure_for_pile
	.type	ALERTS_get_display_structure_for_pile, %function
ALERTS_get_display_structure_for_pile:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L42
	str	lr, [sp, #-4]!
.LCFI7:
	cmp	r0, r3
	beq	.L38
	ldr	r3, .L42+4
	cmp	r0, r3
	ldreq	r0, .L42+8
	ldreq	pc, [sp], #4
	ldr	r3, .L42+12
	cmp	r0, r3
	ldreq	r0, .L42+16
	ldreq	pc, [sp], #4
	ldr	r3, .L42+20
	cmp	r0, r3
	beq	.L41
	ldr	r0, .L42+24
	bl	Alert_Message
.L38:
	ldr	r0, .L42+28
	ldr	pc, [sp], #4
.L41:
	ldr	r0, .L42+32
	ldr	pc, [sp], #4
.L43:
	.align	2
.L42:
	.word	alerts_struct_user
	.word	alerts_struct_changes
	.word	.LANCHOR3
	.word	alerts_struct_tp_micro
	.word	.LANCHOR4
	.word	alerts_struct_engineering
	.word	.LC4
	.word	.LANCHOR2
	.word	.LANCHOR5
.LFE7:
	.size	ALERTS_get_display_structure_for_pile, .-ALERTS_get_display_structure_for_pile
	.section	.text.nm_ALERTS_draw_scroll_line,"ax",%progbits
	.align	2
	.type	nm_ALERTS_draw_scroll_line, %function
nm_ALERTS_draw_scroll_line:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI8:
	mov	r0, r0, asl #16
	mov	r5, r0, asr #16
	bl	ALERTS_get_currently_displayed_pile
	mov	r4, r0
	bl	ALERTS_get_display_structure_for_pile
	mov	r1, #200
	ldr	r2, .L45
	add	r3, r0, #3632
	ldrh	r3, [r3, #8]
	add	r5, r5, r3
	add	r5, r5, #908
	add	r0, r0, r5, asl #1
	ldrh	r3, [r0, #4]
	mov	r0, r4
	str	r3, [sp, #0]
	mov	r3, #256
	bl	nm_ALERTS_parse_alert_and_return_length
	ldmfd	sp!, {r3, r4, r5, pc}
.L46:
	.align	2
.L45:
	.word	GuiVar_AlertString
.LFE8:
	.size	nm_ALERTS_draw_scroll_line, .-nm_ALERTS_draw_scroll_line
	.section	.text.nm_ALERTS_refresh_all_start_indicies,"ax",%progbits
	.align	2
	.global	nm_ALERTS_refresh_all_start_indicies
	.type	nm_ALERTS_refresh_all_start_indicies, %function
nm_ALERTS_refresh_all_start_indicies:
.LFB2:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, lr}
.LCFI9:
	mov	r5, r0
	bl	ALERTS_get_display_structure_for_pile
	mov	r1, #0
	ldr	r2, .L51
	mov	r7, r0
	bl	memset
	ldr	r4, [r5, #28]
	cmp	r4, #0
	beq	.L47
	ldr	r3, [r5, #20]
	add	r6, sp, #8
	add	r7, r7, r4, asl #1
	str	r3, [r6, #-4]!
.L49:
	ldr	r3, [sp, #4]
	mov	r2, #0
	strh	r3, [r7, #-2]!	@ movhi
	mov	r1, #100
	str	r3, [sp, #0]
	mov	r0, r5
	mov	r3, r2
	bl	nm_ALERTS_parse_alert_and_return_length
	mov	r1, r6
	mov	r2, r0
	mov	r0, r5
	bl	ALERTS_inc_index
	subs	r4, r4, #1
	bne	.L49
.L47:
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, pc}
.L52:
	.align	2
.L51:
	.word	1820
.LFE2:
	.size	nm_ALERTS_refresh_all_start_indicies, .-nm_ALERTS_refresh_all_start_indicies
	.section	.text.FDTO_ALERTS_redraw_scrollbox,"ax",%progbits
	.align	2
	.global	FDTO_ALERTS_redraw_scrollbox
	.type	FDTO_ALERTS_redraw_scrollbox, %function
FDTO_ALERTS_redraw_scrollbox:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI10:
	bl	ALERTS_get_currently_displayed_pile
	mov	r5, r0
	bl	ALERTS_get_display_structure_for_pile
	ldr	r3, .L64
	mov	r1, #400
	ldr	r2, .L64+4
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L64+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r6, [r4, #3652]
	cmp	r6, #1
	bne	.L54
	mov	r0, r5
	mov	r1, r4
	bl	nm_ALERTS_restart_display_start_indicies
	mov	r3, #0
	str	r3, [r4, #3652]
	str	r3, [r4, #3640]
	str	r6, [r4, #3648]
.L54:
	ldr	r6, [r4, #3648]
	cmp	r6, #1
	bne	.L55
	ldr	r5, [r4, #3644]
	ldr	r3, .L64+12
	mov	r0, #0
	cmp	r5, #18
	str	r0, [r3, #0]
	bhi	.L56
	bl	GuiLib_ScrollBox_Close
	mov	r0, r6
	bl	FDTO_Redraw_Screen
	b	.L55
.L56:
.LBB8:
	ldr	r3, .L64+16
	ldr	r3, [r3, #0]
	cmp	r5, r3
	beq	.L57
	ldr	r3, .L64+20
	ldr	r6, [r3, #0]
	cmp	r6, #0
	bne	.L58
	mov	r1, r6
	mov	r0, r6
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r7, r0
	mov	r0, r6
	bl	GuiLib_ScrollBox_GetTopLine
	cmp	r7, #0
	movlt	r7, r6
	mov	r8, r0
	blt	.L60
	addne	r7, r7, #1
	cmp	r7, r5
	movcs	r7, r5
.L60:
	add	r3, r0, #17
	cmp	r7, r3
	addgt	r8, r0, #1
	bgt	.L62
	cmp	r0, #0
	ble	.L62
	add	r3, r7, r0
	cmp	r3, r5
	subeq	r8, r0, #1
.L62:
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	mov	r8, r8, asl #16
	mov	r2, r5, asl #16
	mov	r3, r7, asl #16
	mov	r8, r8, asr #16
	ldr	r1, .L64+24
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	mov	r0, #0
	str	r8, [sp, #0]
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	b	.L58
.L57:
	bl	GuiLib_ScrollBox_Redraw
.L58:
	bl	GuiLib_Refresh
.L55:
.LBE8:
	ldr	r3, .L64
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	GuiLib_Refresh
	ldr	r3, .L64+16
	ldr	r2, [r4, #3644]
	str	r2, [r3, #0]
	mov	r3, #0
	str	r3, [r4, #3648]
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, pc}
.L65:
	.align	2
.L64:
	.word	alerts_pile_recursive_MUTEX
	.word	.LC3
	.word	875
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	.LANCHOR6
	.word	g_DIALOG_ok_dialog_visible
	.word	nm_ALERTS_draw_scroll_line
.LFE10:
	.size	FDTO_ALERTS_redraw_scrollbox, .-FDTO_ALERTS_redraw_scrollbox
	.section	.text.FDTO_ALERTS_draw_alerts,"ax",%progbits
	.align	2
	.global	FDTO_ALERTS_draw_alerts
	.type	FDTO_ALERTS_draw_alerts, %function
FDTO_ALERTS_draw_alerts:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI11:
	mvn	r1, #0
	mov	r2, #1
	mov	r8, r0
	mov	r0, #74
	bl	GuiLib_ShowScreen
	bl	ALERTS_get_currently_displayed_pile
	mov	r7, r0
	bl	ALERTS_get_display_structure_for_pile
	ldr	r3, .L74
	cmp	r0, r3
	mov	r4, r0
	moveq	r1, #892
	beq	.L67
	ldr	r3, .L74+4
	cmp	r0, r3
	ldreq	r1, .L74+8
	beq	.L67
	ldr	r1, .L74+12
	ldr	r3, .L74+16
	ldr	r2, .L74+20
	cmp	r0, r1
	movne	r1, r2
	moveq	r1, r3
.L67:
	ldr	r3, .L74+24
	ldr	r6, .L74+28
	ldrsh	r3, [r3, #0]
	mov	r5, #0
	cmp	r3, #0
	moveq	r0, #108
	movne	r0, #128
	bl	FDTO_show_screen_name_in_title_bar
	ldr	r3, .L74+32
	ldr	r2, .L74+36
	str	r5, [r3, #0]
	mov	r1, #400
	ldr	r3, .L74+40
	mov	sl, #1
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r7
	mov	r1, r4
	str	sl, [r4, #3652]
	bl	nm_ALERTS_restart_display_start_indicies
	ldr	r1, [r4, #3644]
	ldr	r3, .L74+44
	mov	r0, r8
	str	r1, [r3, #0]
	ldr	r2, .L74+48
	str	r5, [r4, #3652]
	str	r5, [r4, #3640]
	str	sl, [r4, #3648]
	bl	FDTO_REPORTS_draw_report
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	str	r5, [r4, #3648]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L75:
	.align	2
.L74:
	.word	.LANCHOR3
	.word	.LANCHOR4
	.word	1075
	.word	.LANCHOR5
	.word	918
	.word	878
	.word	GuiLib_LanguageIndex
	.word	alerts_pile_recursive_MUTEX
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	.LC3
	.word	986
	.word	.LANCHOR6
	.word	nm_ALERTS_draw_scroll_line
.LFE11:
	.size	FDTO_ALERTS_draw_alerts, .-FDTO_ALERTS_draw_alerts
	.section	.text.ALERTS_process_report,"ax",%progbits
	.align	2
	.global	ALERTS_process_report
	.type	ALERTS_process_report, %function
ALERTS_process_report:
.LFB12:
	@ args = 0, pretend = 0, frame = 256
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI12:
	sub	sp, sp, #260
.LCFI13:
	mov	r7, r1
	mov	r4, r0
	bl	ALERTS_get_currently_displayed_pile
	mov	r6, r0
	bl	ALERTS_get_display_structure_for_pile
	mov	r5, r0
	cmp	r4, #4
	ldrls	pc, [pc, r4, asl #2]
	b	.L77
.L80:
	.word	.L78
	.word	.L79
	.word	.L77
	.word	.L79
	.word	.L78
.L79:
	ldr	r3, .L94
	ldr	r2, .L94+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L94+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	add	r3, r5, #3632
	mov	r1, r0
	ldrh	r7, [r3, #8]
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r1, #200
	add	r2, sp, #4
	add	r7, r7, r0
	add	r7, r7, #908
	add	r5, r5, r7, asl #1
	ldrh	r3, [r5, #4]
	mov	r0, r6
	str	r3, [sp, #0]
	mov	r3, #256
	bl	nm_ALERTS_parse_alert_and_return_length
	ldr	r3, .L94+12
	add	r0, sp, #4
	ldr	r1, [r3, #4]
	mov	r2, #2
	bl	GuiLib_GetTextWidth
	add	r3, r0, #79
	cmp	r3, #308
	mov	r5, r0
	bcc	.L81
	cmp	r4, #1
	bne	.L82
	ldr	r3, .L94+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	b	.L92
.L82:
	cmp	r4, #3
	bne	.L81
	ldr	r3, .L94+16
	sub	r5, r5, #228
	ldr	r0, [r3, #0]
	bl	abs
	cmp	r0, r5
.L92:
	bge	.L81
	bl	good_key_beep
	ldr	r3, .L94+16
	mov	r2, #0
	ldr	r3, [r3, #0]
	b	.L85
.L88:
	cmp	r4, #1
	addeq	r3, r3, #1
	subne	r3, r3, #1
	add	r2, r2, #1
.L85:
	cmp	r2, #3
	movhi	r0, #0
	movls	r0, #1
	cmp	r3, #0
	movgt	r0, #0
	cmp	r0, #0
	bne	.L88
	ldr	r2, .L94+16
	str	r3, [r2, #0]
	bl	SCROLL_BOX_redraw
	b	.L89
.L81:
	bl	bad_key_beep
.L89:
	ldr	r3, .L94
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L76
.L78:
	ldr	r3, .L94+16
	mov	r2, #0
	str	r2, [r3, #0]
	mov	r0, r4
	mov	r1, r7
	b	.L93
.L77:
	mov	r0, r4
	mov	r1, r7
	mov	r2, #0
.L93:
	mov	r3, r2
	bl	REPORTS_process_report
.L76:
	add	sp, sp, #260
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L95:
	.align	2
.L94:
	.word	alerts_pile_recursive_MUTEX
	.word	.LC3
	.word	1060
	.word	GuiFont_FontList
	.word	GuiVar_ScrollBoxHorizScrollPos
.LFE12:
	.size	ALERTS_process_report, .-ALERTS_process_report
	.global	alerts_display_struct_engineering
	.global	alerts_display_struct_tp_micro
	.global	alerts_display_struct_changes
	.global	alerts_display_struct_user
	.global	g_ALERTS_filter_to_show
	.global	g_ALERTS_pile_to_show
	.global	g_ALERTS_line_count
	.section	.bss.alerts_display_struct_user,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	alerts_display_struct_user, %object
	.size	alerts_display_struct_user, 3656
alerts_display_struct_user:
	.space	3656
	.section	.bss.alerts_display_struct_changes,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	alerts_display_struct_changes, %object
	.size	alerts_display_struct_changes, 3656
alerts_display_struct_changes:
	.space	3656
	.section	.bss.alerts_display_struct_tp_micro,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	alerts_display_struct_tp_micro, %object
	.size	alerts_display_struct_tp_micro, 3656
alerts_display_struct_tp_micro:
	.space	3656
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Invalid alert pile to show: %d\000"
.LC1:
	.ascii	"%d/%02d  \000"
.LC2:
	.ascii	"0\000"
.LC3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_alerts.c\000"
.LC4:
	.ascii	"Invalid pile\000"
	.section	.bss.g_ALERTS_pile_to_show,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_ALERTS_pile_to_show, %object
	.size	g_ALERTS_pile_to_show, 4
g_ALERTS_pile_to_show:
	.space	4
	.section	.bss.alerts_display_struct_engineering,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	alerts_display_struct_engineering, %object
	.size	alerts_display_struct_engineering, 3656
alerts_display_struct_engineering:
	.space	3656
	.section	.bss.g_ALERTS_line_count,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	g_ALERTS_line_count, %object
	.size	g_ALERTS_line_count, 4
g_ALERTS_line_count:
	.space	4
	.section	.bss.g_ALERTS_filter_to_show,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_ALERTS_filter_to_show, %object
	.size	g_ALERTS_filter_to_show, 4
g_ALERTS_filter_to_show:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI0-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI5-.LFB1
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI6-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI7-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI8-.LFB8
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI9-.LFB2
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI10-.LFB10
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI11-.LFB11
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI12-.LFB12
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x118
	.align	2
.LEFDE22:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_alerts.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x125
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF13
	.byte	0x1
	.4byte	.LASF14
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x2a1
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x2ff
	.byte	0x1
	.uleb128 0x3
	.4byte	0x21
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xe5
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x1a5
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x260
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x6
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x14d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST4
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x23b
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x2c0
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST6
	.uleb128 0x6
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x2f1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST7
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x16e
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST8
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x361
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST9
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x3a2
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST10
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x409
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST11
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB6
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI2
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB1
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB7
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB8
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB2
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB10
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB11
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB12
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI13
	.4byte	.LFE12
	.2byte	0x3
	.byte	0x7d
	.sleb128 280
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x74
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF11:
	.ascii	"FDTO_ALERTS_draw_alerts\000"
.LASF1:
	.ascii	"FDTO_ALERTS_redraw_retaining_topline\000"
.LASF6:
	.ascii	"ALERTS_get_display_structure_for_pile\000"
.LASF12:
	.ascii	"ALERTS_process_report\000"
.LASF2:
	.ascii	"nm_ALERTS_parse_alert_and_return_length\000"
.LASF13:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"nm_ALERTS_change_filter\000"
.LASF0:
	.ascii	"ALERTS_get_currently_displayed_pile\000"
.LASF3:
	.ascii	"nm_ALERTS_roll_and_add_new_display_start_index\000"
.LASF8:
	.ascii	"nm_ALERTS_draw_scroll_line\000"
.LASF10:
	.ascii	"FDTO_ALERTS_redraw_scrollbox\000"
.LASF9:
	.ascii	"nm_ALERTS_refresh_all_start_indicies\000"
.LASF4:
	.ascii	"ALERT_this_alert_is_to_be_displayed\000"
.LASF14:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_alerts.c\000"
.LASF7:
	.ascii	"nm_ALERTS_restart_display_start_indicies\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
