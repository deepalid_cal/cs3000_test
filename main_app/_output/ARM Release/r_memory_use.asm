	.file	"r_memory_use.c"
	.text
.Ltext0:
	.section	.text.FDTO_MEM_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_MEM_draw_report
	.type	FDTO_MEM_draw_report, %function
FDTO_MEM_draw_report:
.LFB0:
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
	mov	r2, r0
	sub	sp, sp, #116
.LCFI1:
	bne	.L2
	mov	r0, #87
	mvn	r1, #0
	bl	GuiLib_ShowScreen
.L2:
	ldr	sl, .L5
	mov	r0, #0
	mov	r1, #0
	mov	r9, #0
	mov	r7, #38
	add	r5, sp, #52
	str	r0, [sp, #44]
	str	r1, [sp, #48]
.L3:
	ldmia	sl, {r3, fp}
	add	r1, sp, #44
	ldmia	r1, {r0-r1}
	mul	r2, r3, fp
	mov	r4, #0
	adds	r0, r0, r2
	adc	r1, r1, #0
	str	r0, [sp, #44]
	str	r1, [sp, #48]
	ldr	r2, .L5+4
	mov	r1, #64
	mov	r0, r5
	bl	snprintf
	mov	r6, #3
	mov	r8, #15
	mov	r1, r7
	mov	r0, #105
	mov	r2, #1
	mvn	r3, #0
	str	r5, [sp, #0]
	str	r6, [sp, #4]
	str	r6, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	str	r4, [sp, #20]
	str	r4, [sp, #24]
	str	r4, [sp, #28]
	str	r4, [sp, #32]
	str	r4, [sp, #36]
	str	r8, [sp, #40]
	bl	GuiLib_DrawStr
	ldr	ip, .L5+8
	mov	r1, #64
	ldr	r3, [r9, ip]
	ldr	r2, .L5+12
	mov	r0, r5
	bl	snprintf
	mov	r1, r7
	mov	r0, #143
	mov	r2, #1
	mvn	r3, #0
	stmia	sp, {r5, r6}
	str	r6, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	str	r4, [sp, #20]
	str	r4, [sp, #24]
	str	r4, [sp, #28]
	str	r4, [sp, #32]
	str	r4, [sp, #36]
	str	r8, [sp, #40]
	bl	GuiLib_DrawStr
	ldr	ip, .L5+16
	mov	r1, #64
	ldr	r3, [r9, ip]
	ldr	r2, .L5+12
	mov	r0, r5
	bl	snprintf
	mov	r1, r7
	mov	r0, #201
	mov	r2, #1
	mvn	r3, #0
	stmia	sp, {r5, r6}
	str	r6, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	str	r4, [sp, #20]
	str	r4, [sp, #24]
	str	r4, [sp, #28]
	str	r4, [sp, #32]
	str	r4, [sp, #36]
	str	r8, [sp, #40]
	bl	GuiLib_DrawStr
	mov	r1, #64
	ldr	r2, .L5+20
	mov	r3, fp
	mov	r0, r5
	bl	snprintf
	mov	r1, r7
	ldr	r0, .L5+24
	mov	r2, #1
	mvn	r3, #0
	add	r7, r7, r8
	stmia	sp, {r5, r6}
	str	r6, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	str	r4, [sp, #20]
	str	r4, [sp, #24]
	str	r4, [sp, #28]
	str	r4, [sp, #32]
	str	r4, [sp, #36]
	str	r8, [sp, #40]
	bl	GuiLib_DrawStr
	cmp	r7, #203
	add	sl, sl, #8
	add	r9, r9, #4
	bne	.L3
	ldr	r3, .L5+28
	mov	r1, #64
	ldr	r3, [r3, #0]
	ldr	r2, .L5+32
	str	r3, [sp, #0]
	ldr	r3, .L5+36
	mov	r0, r5
	ldr	r3, [r3, #0]
	bl	snprintf
	mov	r0, r5
	bl	strlen
	mov	sl, #6
	mov	r7, #2
	mov	r1, #207
	mov	r2, #1
	str	r5, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	str	r4, [sp, #24]
	str	r4, [sp, #28]
	str	r4, [sp, #32]
	str	r4, [sp, #36]
	str	r8, [sp, #40]
	mul	r3, sl, r0
	mov	r0, #160
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	str	r3, [sp, #20]
	mvn	r3, #0
	bl	GuiLib_DrawStr
	add	r1, sp, #44
	ldmia	r1, {r0-r1}
	ldr	r3, .L5+40
	stmia	sp, {r0-r1}
	ldr	r2, .L5+44
	mov	r1, #64
	ldr	r3, [r3, #0]
	mov	r0, r5
	bl	snprintf
	mov	r0, r5
	bl	strlen
	mov	r1, #219
	mov	r2, #1
	mvn	r3, #0
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	str	r4, [sp, #24]
	str	r4, [sp, #28]
	str	r4, [sp, #32]
	str	r4, [sp, #36]
	str	r8, [sp, #40]
	mul	sl, r0, sl
	mov	r0, #160
	mov	sl, sl, asl #16
	mov	sl, sl, asr #16
	str	sl, [sp, #20]
	bl	GuiLib_DrawStr
	bl	GuiLib_Refresh
	add	sp, sp, #116
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L6:
	.align	2
.L5:
	.word	partitions
	.word	.LC0
	.word	mem_current_allocations_of
	.word	.LC1
	.word	mem_max_allocations_of
	.word	.LC2
	.word	270
	.word	mem_count
	.word	.LC3
	.word	mem_numalloc
	.word	mem_maxalloc
	.word	.LC4
.LFE0:
	.size	FDTO_MEM_draw_report, .-FDTO_MEM_draw_report
	.section	.text.MEM_process_report,"ax",%progbits
	.align	2
	.global	MEM_process_report
	.type	MEM_process_report, %function
MEM_process_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	str	lr, [sp, #-4]!
.LCFI2:
	bne	.L10
	ldr	r3, .L11
	mov	r2, #11
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	ldr	lr, [sp], #4
	b	TECH_SUPPORT_draw_dialog
.L10:
	ldr	lr, [sp], #4
	b	KEY_process_global_keys
.L12:
	.align	2
.L11:
	.word	GuiVar_MenuScreenToShow
.LFE1:
	.size	MEM_process_report, .-MEM_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%6lu\000"
.LC1:
	.ascii	"%4u\000"
.LC2:
	.ascii	"%4lu\000"
.LC3:
	.ascii	"%ld bytes in %ld allocs\000"
.LC4:
	.ascii	"(max %ld out of %ld)\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x98
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_memory_use.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x46
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x28
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x62
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 152
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_memory_use.c\000"
.LASF0:
	.ascii	"FDTO_MEM_draw_report\000"
.LASF1:
	.ascii	"MEM_process_report\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
