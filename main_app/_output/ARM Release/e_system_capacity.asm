	.file	"e_system_capacity.c"
	.text
.Ltext0:
	.section	.text.SYSTEM_CAPACITY_process_capacity,"ax",%progbits
	.align	2
	.type	SYSTEM_CAPACITY_process_capacity, %function
SYSTEM_CAPACITY_process_capacity:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI0:
	mov	r5, r2
	mov	r4, r0
	mov	r0, r1
	mov	r6, r3
	bl	SYSTEM_get_inc_by_for_MLB_and_capacity
	cmp	r5, #0
	beq	.L2
	str	r0, [sp, #0]
	mov	r2, #1
	mov	r0, r4
	mov	r1, r6
	mov	r3, #4000
	str	r2, [sp, #4]
	bl	process_uns32
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, pc}
.L2:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	bad_key_beep
.LFE0:
	.size	SYSTEM_CAPACITY_process_capacity, .-SYSTEM_CAPACITY_process_capacity
	.section	.text.FDTO_SYSTEM_CAPACITY_show_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_SYSTEM_CAPACITY_show_dropdown, %function
FDTO_SYSTEM_CAPACITY_show_dropdown:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L5
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L6:
	.align	2
.L5:
	.word	.LANCHOR0
.LFE1:
	.size	FDTO_SYSTEM_CAPACITY_show_dropdown, .-FDTO_SYSTEM_CAPACITY_show_dropdown
	.section	.text.FDTO_SYSTEM_CAPACITY_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_SYSTEM_CAPACITY_draw_screen
	.type	FDTO_SYSTEM_CAPACITY_draw_screen, %function
FDTO_SYSTEM_CAPACITY_draw_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L10
	str	lr, [sp, #-4]!
.LCFI1:
	ldrnesh	r1, [r3, #0]
	bne	.L9
	bl	SYSTEM_CAPACITY_copy_group_into_guivars
	mov	r1, #0
.L9:
	mov	r0, #38
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L11:
	.align	2
.L10:
	.word	GuiLib_ActiveCursorFieldNo
.LFE2:
	.size	FDTO_SYSTEM_CAPACITY_draw_screen, .-FDTO_SYSTEM_CAPACITY_draw_screen
	.global	__modsi3
	.section	.text.SYSTEM_CAPACITY_process_screen,"ax",%progbits
	.align	2
	.global	SYSTEM_CAPACITY_process_screen
	.type	SYSTEM_CAPACITY_process_screen, %function
SYSTEM_CAPACITY_process_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L69
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #48
.LCFI3:
	cmp	r3, #740
	mov	r4, r0
	bne	.L58
	ldr	r3, .L69+4
	ldr	r1, [r3, #0]
	bl	COMBO_BOX_key_press
	b	.L12
.L58:
	cmp	r0, #4
	beq	.L21
	bhi	.L24
	cmp	r0, #1
	beq	.L18
	bcc	.L17
	cmp	r0, #2
	beq	.L19
	cmp	r0, #3
	bne	.L16
	b	.L68
.L24:
	cmp	r0, #67
	beq	.L22
	bhi	.L25
	cmp	r0, #16
	beq	.L21
	cmp	r0, #20
	bne	.L16
	b	.L17
.L25:
	cmp	r0, #80
	beq	.L23
	cmp	r0, #84
	bne	.L16
	b	.L23
.L19:
	ldr	r3, .L69+8
	ldrh	r3, [r3, #0]
	mov	r3, r3, asl #16
	mov	r2, r3, asr #16
	cmp	r2, #3
	ldrls	pc, [pc, r2, asl #2]
	b	.L26
.L31:
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
.L27:
	ldr	r1, .L69+12
	b	.L59
.L28:
	ldr	r1, .L69+16
	b	.L59
.L29:
	ldr	r1, .L69+20
	b	.L59
.L30:
	ldr	r1, .L69+24
.L59:
	ldr	r2, .L69+4
	str	r1, [r2, #0]
.L26:
	cmp	r3, #196608
	bhi	.L56
	bl	good_key_beep
	mov	r3, #3
	str	r3, [sp, #12]
	ldr	r3, .L69+28
	add	r0, sp, #12
	str	r3, [sp, #32]
	mov	r3, #158
	str	r3, [sp, #36]
	ldr	r3, .L69+8
	ldrh	r3, [r3, #0]
	cmp	r3, #0
	movne	r3, r3, asl #16
	movne	r3, r3, asr #16
	addne	r3, r3, r3, asl #3
	movne	r3, r3, asl #1
	addne	r3, r3, #46
	moveq	r3, #46
	str	r3, [sp, #40]
	bl	Display_Post_Command
	b	.L12
.L23:
	ldr	r3, .L69+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #23
	ldrls	pc, [pc, r3, asl #2]
	b	.L34
.L47:
	.word	.L35
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L34
	.word	.L34
	.word	.L34
	.word	.L34
	.word	.L34
	.word	.L34
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L34
	.word	.L34
	.word	.L34
	.word	.L34
	.word	.L34
	.word	.L34
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
.L35:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L69+12
	b	.L60
.L36:
	ldr	r1, .L69+16
	mov	r3, #1
	mov	r0, r4
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L60:
	mov	r2, #0
	bl	process_uns32
	b	.L48
.L37:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L69+20
	b	.L60
.L38:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L69+24
	b	.L60
.L39:
	ldr	r3, .L69+12
	mov	r0, r4
	ldr	r2, [r3, #0]
	ldr	r3, .L69+32
	b	.L61
.L40:
	ldr	r3, .L69+16
	mov	r0, r4
	ldr	r2, [r3, #0]
	ldr	r3, .L69+36
	b	.L61
.L41:
	ldr	r3, .L69+20
	mov	r0, r4
	ldr	r2, [r3, #0]
	ldr	r3, .L69+40
	b	.L61
.L42:
	ldr	r3, .L69+24
	mov	r0, r4
	ldr	r2, [r3, #0]
	ldr	r3, .L69+44
	b	.L61
.L43:
	ldr	r3, .L69+12
	mov	r0, r4
	ldr	r2, [r3, #0]
	ldr	r3, .L69+48
	b	.L61
.L44:
	ldr	r3, .L69+16
	mov	r0, r4
	ldr	r2, [r3, #0]
	ldr	r3, .L69+52
	b	.L61
.L45:
	ldr	r3, .L69+20
	mov	r0, r4
	ldr	r2, [r3, #0]
	ldr	r3, .L69+56
	b	.L61
.L46:
	ldr	r3, .L69+24
	mov	r0, r4
	ldr	r2, [r3, #0]
	ldr	r3, .L69+60
.L61:
	bl	SYSTEM_CAPACITY_process_capacity
	b	.L48
.L34:
	bl	bad_key_beep
.L48:
	mov	r0, #0
	bl	Redraw_Screen
	b	.L12
.L21:
	ldr	r3, .L69+8
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	movs	r0, r0, asl #16
	beq	.L56
.L49:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L12
.L17:
	ldr	r3, .L69+8
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	bl	SYSTEM_num_systems_in_use
	sub	r0, r0, #1
	cmp	r4, r0
	beq	.L56
.L50:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L12
.L18:
	ldr	r3, .L69+8
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #3
	movls	r0, r0, asr #16
	addls	r0, r0, #19
	bls	.L67
	sub	r2, r3, #10
	mov	r2, r2, asl #16
	cmp	r2, #196608
	bls	.L66
.L52:
	sub	r3, r3, #20
	mov	r3, r3, asl #16
	cmp	r3, #196608
	bhi	.L56
.L66:
	mov	r0, r0, asr #16
	sub	r0, r0, #10
.L67:
	mov	r1, r4
	b	.L64
.L68:
	ldr	r3, .L69+8
	ldrh	r2, [r3, #0]
	mov	r0, r2, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #3
	bls	.L65
.L54:
	sub	r1, r3, #10
	mov	r1, r1, asl #16
	cmp	r1, #196608
	bhi	.L55
.L65:
	mov	r0, r0, asr #16
	add	r0, r0, #10
	b	.L63
.L55:
	sub	r3, r3, #20
	mov	r3, r3, asl #16
	cmp	r3, #196608
	bhi	.L56
	mov	r2, r2, asl #16
	mov	r0, r2, asr #16
	sub	r0, r0, #19
.L63:
	mov	r1, #1
.L64:
	bl	CURSOR_Select
	b	.L12
.L56:
	bl	bad_key_beep
	b	.L12
.L22:
	ldr	r3, .L69+64
	mov	r2, #3
	str	r2, [r3, #0]
	str	r1, [sp, #8]
	bl	SYSTEM_CAPACITY_extract_and_store_changes_from_GuiVars
	ldr	r1, [sp, #8]
.L16:
	mov	r0, r4
	bl	KEY_process_global_keys
.L12:
	add	sp, sp, #48
	ldmfd	sp!, {r4, pc}
.L70:
	.align	2
.L69:
	.word	GuiLib_CurStructureNdx
	.word	.LANCHOR0
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	FDTO_SYSTEM_CAPACITY_show_dropdown
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_GroupSettingC_3
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	SYSTEM_CAPACITY_process_screen, .-SYSTEM_CAPACITY_process_screen
	.section	.bss.g_SYSTEM_CAPACITY_combo_box_guivar,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_SYSTEM_CAPACITY_combo_box_guivar, %object
	.size	g_SYSTEM_CAPACITY_combo_box_guivar, 4
g_SYSTEM_CAPACITY_combo_box_guivar:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_system_capacity.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x32
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x43
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x49
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x5d
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"FDTO_SYSTEM_CAPACITY_show_dropdown\000"
.LASF2:
	.ascii	"FDTO_SYSTEM_CAPACITY_draw_screen\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_system_capacity.c\000"
.LASF3:
	.ascii	"SYSTEM_CAPACITY_process_screen\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"SYSTEM_CAPACITY_process_capacity\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
