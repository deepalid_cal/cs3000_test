	.file	"device_common.c"
	.text
.Ltext0:
	.global	__udivsi3
	.section	.text.dev_get_and_process_command,"ax",%progbits
	.align	2
	.type	dev_get_and_process_command, %function
dev_get_and_process_command:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #8]
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI0:
	sub	r3, r3, #6
	cmp	r3, #2
	mov	r4, r0
	mov	r6, r1
	mov	r5, r2
	bhi	.L2
	ldr	r0, [r0, #136]
	mov	r1, #5
	add	r0, r0, #500
	bl	__udivsi3
	bl	vTaskDelay
.L2:
	ldr	r3, [r4, #8]
	ldr	r2, .L9
	mov	r1, #48
	mla	r3, r1, r3, r2
	mov	r0, r4
	ldr	r3, [r3, #20]
	mov	r1, r6
	blx	r3
	subs	r4, r0, #0
	moveq	r0, r4
	beq	.L3
.LBB16:
.LBB17:
	ldr	r3, .L9+4
	ldr	r7, .L9+8
	ldr	r3, [r3, #0]
	ldr	r8, .L9+12
	ldr	r2, [r3, #132]
	ldr	r3, .L9+8
	cmp	r2, #0
	movne	r2, #100
	ldr	r0, [r3, #368]
	movne	r1, #500
	moveq	r1, #100
	bl	RCVD_DATA_enable_hunting_mode
	mov	r0, r4
	bl	strlen
	ldr	r2, [r7, #368]
	ldr	r3, .L9+16
	mla	r8, r2, r8, r3
	ldr	r3, .L9+20
	str	r5, [r8, r3]
	mov	r6, r0
	mov	r0, r5
	bl	strlen
	ldr	r3, .L9+24
	cmp	r6, #0
	str	r0, [r8, r3]
	beq	.L6
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L6
	mov	r2, #2
	mov	r3, #1
	stmia	sp, {r2, r3}
	ldr	r0, [r7, #368]
	mov	r1, r4
	mov	r2, r6
	mov	r3, #0
	bl	AddCopyOfBlockToXmitList
.L6:
.LBE17:
.LBE16:
	mov	r0, r4
	ldr	r1, .L9+28
	ldr	r2, .L9+32
	bl	mem_free_debug
	mov	r0, #1
.L3:
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L10:
	.align	2
.L9:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	comm_mngr
	.word	4280
	.word	SerDrvrVars_s
	.word	4216
	.word	4220
	.word	.LC0
	.word	2946
.LFE28:
	.size	dev_get_and_process_command, .-dev_get_and_process_command
	.section	.text.dev_increment_list_item_pointer,"ax",%progbits
	.align	2
	.type	dev_increment_list_item_pointer, %function
dev_increment_list_item_pointer:
.LFB36:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #12]
	cmp	r3, #82
	beq	.L13
	cmp	r3, #87
	beq	.L14
	cmp	r3, #77
	bxne	lr
.L13:
.LBB20:
	ldr	r3, [r0, #104]
	add	r3, r3, r1
	str	r3, [r0, #104]
	b	.L15
.L14:
.LBE20:
	ldr	r3, [r0, #108]
	add	r3, r3, r1
	str	r3, [r0, #108]
.L15:
	ldr	r3, [r0, #156]
	add	r1, r3, r1, asl #4
	str	r1, [r0, #156]
	bx	lr
.LFE36:
	.size	dev_increment_list_item_pointer, .-dev_increment_list_item_pointer
	.section	.text.dev_decrement_list_item_pointer,"ax",%progbits
	.align	2
	.type	dev_decrement_list_item_pointer, %function
dev_decrement_list_item_pointer:
.LFB37:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #12]
	cmp	r3, #82
	beq	.L18
	cmp	r3, #87
	beq	.L19
	cmp	r3, #77
	bxne	lr
.L18:
	ldr	r3, [r0, #104]
	cmp	r3, #0
.LBB23:
	subne	r3, r3, #1
	strne	r3, [r0, #104]
.LBE23:
	bxeq	lr
	b	.L20
.L19:
	ldr	r3, [r0, #108]
	cmp	r3, #0
	bxeq	lr
	sub	r3, r3, #1
	str	r3, [r0, #108]
.L20:
	ldr	r3, [r0, #156]
	sub	r3, r3, #16
	str	r3, [r0, #156]
	bx	lr
.LFE37:
	.size	dev_decrement_list_item_pointer, .-dev_decrement_list_item_pointer
	.section	.text.unlikely.dev_command_escape_char_handler.constprop.4,"ax",%progbits
	.align	2
	.type	dev_command_escape_char_handler.constprop.4, %function
dev_command_escape_char_handler.constprop.4:
.LFB48:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	mov	r5, r0
	mov	r4, r1
	bl	strlen
	mov	r0, r0, asl #1
	add	r0, r0, #3
	cmp	r0, #148
	bhi	.L22
.LBB26:
.LBB27:
	mov	r1, #0
	mov	r0, r4
	mov	r2, #148
	bl	memset
	mov	r0, r4
	ldr	r1, .L33
	b	.L32
.L28:
	mov	r0, r6
	bl	isalnum
	cmp	r0, #0
	bne	.L31
.L24:
	ldrb	r3, [r5, #-1]	@ zero_extendqisi2
	mov	r0, r4
	cmp	r3, #34
	bne	.L29
	ldr	r1, .L33+4
	mov	r2, #1
	bl	memcpy
	add	r4, r4, #1
.L31:
	mov	r0, r4
.L29:
	mov	r1, r7
.L32:
	mov	r2, #1
	bl	memcpy
	mov	r7, r5
	ldrb	r6, [r5], #1	@ zero_extendqisi2
	add	r4, r4, #1
	cmp	r6, #0
	bne	.L28
	mov	r0, r4
	ldr	r1, .L33
	mov	r2, #1
	bl	memcpy
	add	r0, r4, #1
	mov	r1, r6
	mov	r2, #1
.LBE27:
.LBE26:
	ldmfd	sp!, {r4, r5, r6, r7, lr}
.LBB29:
.LBB28:
	b	memcpy
.L22:
.LBE28:
.LBE29:
	ldr	r0, .L33+8
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	Alert_Message
.L34:
	.align	2
.L33:
	.word	.LC1
	.word	.LC2
	.word	.LC3
.LFE48:
	.size	dev_command_escape_char_handler.constprop.4, .-dev_command_escape_char_handler.constprop.4
	.section	.text.dev_get_command_text,"ax",%progbits
	.align	2
	.global	dev_get_command_text
	.type	dev_get_command_text, %function
dev_get_command_text:
.LFB23:
	@ args = 0, pretend = 0, frame = 296
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	ldr	r5, [r0, #168]
	sub	sp, sp, #344
.LCFI3:
	mov	r4, r0
	ldr	sl, [r0, #172]
	ldr	r6, [r0, #176]
	ldr	r8, [r0, #180]
	mov	r7, r1
	add	r0, sp, #48
	mov	r1, #0
	mov	r2, #148
	bl	memset
	mov	r3, #1
	str	r3, [r4, #132]
	mov	r3, #0
	str	r3, [r4, #136]
	cmp	r7, #170
	ldrls	pc, [pc, r7, asl #2]
	b	.L36
.L197:
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
	.word	.L48
	.word	.L49
	.word	.L50
	.word	.L51
	.word	.L52
	.word	.L53
	.word	.L54
	.word	.L55
	.word	.L56
	.word	.L57
	.word	.L58
	.word	.L59
	.word	.L60
	.word	.L61
	.word	.L62
	.word	.L63
	.word	.L64
	.word	.L65
	.word	.L66
	.word	.L36
	.word	.L36
	.word	.L67
	.word	.L68
	.word	.L69
	.word	.L70
	.word	.L71
	.word	.L72
	.word	.L73
	.word	.L74
	.word	.L75
	.word	.L76
	.word	.L77
	.word	.L78
	.word	.L79
	.word	.L80
	.word	.L81
	.word	.L82
	.word	.L83
	.word	.L84
	.word	.L85
	.word	.L86
	.word	.L87
	.word	.L88
	.word	.L89
	.word	.L90
	.word	.L91
	.word	.L92
	.word	.L93
	.word	.L94
	.word	.L95
	.word	.L96
	.word	.L97
	.word	.L98
	.word	.L99
	.word	.L100
	.word	.L101
	.word	.L102
	.word	.L103
	.word	.L104
	.word	.L105
	.word	.L106
	.word	.L107
	.word	.L108
	.word	.L109
	.word	.L110
	.word	.L111
	.word	.L112
	.word	.L113
	.word	.L114
	.word	.L115
	.word	.L116
	.word	.L117
	.word	.L118
	.word	.L119
	.word	.L120
	.word	.L121
	.word	.L122
	.word	.L123
	.word	.L124
	.word	.L125
	.word	.L126
	.word	.L127
	.word	.L128
	.word	.L129
	.word	.L130
	.word	.L131
	.word	.L132
	.word	.L133
	.word	.L134
	.word	.L135
	.word	.L136
	.word	.L137
	.word	.L138
	.word	.L139
	.word	.L140
	.word	.L141
	.word	.L142
	.word	.L143
	.word	.L144
	.word	.L145
	.word	.L146
	.word	.L147
	.word	.L148
	.word	.L149
	.word	.L150
	.word	.L151
	.word	.L152
	.word	.L153
	.word	.L154
	.word	.L155
	.word	.L156
	.word	.L157
	.word	.L158
	.word	.L159
	.word	.L160
	.word	.L161
	.word	.L162
	.word	.L163
	.word	.L164
	.word	.L165
	.word	.L166
	.word	.L167
	.word	.L168
	.word	.L36
	.word	.L169
	.word	.L170
	.word	.L171
	.word	.L172
	.word	.L173
	.word	.L174
	.word	.L175
	.word	.L176
	.word	.L177
	.word	.L178
	.word	.L179
	.word	.L180
	.word	.L181
	.word	.L182
	.word	.L183
	.word	.L184
	.word	.L185
	.word	.L186
	.word	.L187
	.word	.L183
	.word	.L184
	.word	.L185
	.word	.L186
	.word	.L188
	.word	.L183
	.word	.L184
	.word	.L185
	.word	.L186
	.word	.L189
	.word	.L190
	.word	.L191
	.word	.L192
	.word	.L193
	.word	.L194
	.word	.L195
	.word	.L196
.L37:
	ldr	r1, .L339
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r0, sp, #48
	add	r1, r5, #153
	b	.L302
.L38:
	add	r0, sp, #48
	ldr	r1, .L339+4
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L39:
	add	r0, sp, #48
	ldr	r1, .L339+8
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L40:
	add	r0, sp, #48
	ldr	r1, .L339+12
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L41:
	add	r0, sp, #48
	ldr	r1, .L339+16
	mov	r2, #148
	bl	strlcpy
	ldr	r3, [r4, #8]
	cmp	r3, #7
	bne	.L200
	add	r0, sp, #48
	ldr	r1, .L339+20
	mov	r2, #148
	bl	strlcat
	b	.L276
.L200:
	cmp	r3, #6
	cmpne	r3, #8
	bne	.L276
	add	r0, sp, #48
	ldr	r1, .L339+24
	mov	r2, #148
	bl	strlcat
	b	.L276
.L42:
	add	r0, sp, #48
	ldr	r1, .L339+28
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L43:
	add	r0, sp, #48
	ldr	r1, .L339+32
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L44:
	add	r0, sp, #48
	ldr	r1, .L339+36
	mov	r2, #148
	bl	strlcpy
	mov	r3, #0
	str	r3, [r4, #132]
	b	.L276
.L45:
	add	r0, sp, #48
	ldr	r1, .L339+40
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L46:
	add	r0, sp, #48
	ldr	r1, .L339+44
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L47:
	add	r0, sp, #48
	ldr	r1, .L339+48
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L48:
	add	r0, sp, #48
	ldr	r1, .L339+52
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L49:
	add	r0, sp, #48
	ldr	r1, .L339+56
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L50:
	add	r0, sp, #48
	ldr	r1, .L339+60
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+64
	str	r3, [r4, #136]
	b	.L276
.L51:
	add	r0, sp, #48
	ldr	r1, .L339+68
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L52:
	str	r3, [r4, #132]
.L53:
	add	r0, sp, #48
	ldr	r1, .L339+72
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L54:
	add	r0, sp, #48
	ldr	r1, .L339+72
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+64
	str	r3, [r4, #136]
	b	.L276
.L55:
	add	r0, sp, #48
	ldr	r1, .L339+76
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L56:
	add	r0, sp, #48
	ldr	r1, .L339+80
	mov	r2, #148
	bl	strlcpy
	ldr	r3, [r4, #268]
	b	.L324
.L57:
	add	r0, sp, #48
	ldr	r1, .L339+84
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L58:
	add	r0, sp, #48
	ldr	r1, .L339+88
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L59:
	add	r0, sp, #48
	ldr	r1, .L339+92
	mov	r2, #148
	bl	strlcpy
	ldr	r3, [r5, #564]
	b	.L324
.L60:
	add	r0, sp, #48
	ldr	r1, .L339+96
	mov	r2, #148
	bl	strlcpy
	ldr	r3, [r5, #568]
	b	.L324
.L61:
	add	r0, sp, #48
	ldr	r1, .L339+100
	mov	r2, #148
	bl	strlcpy
	ldr	r3, [r5, #572]
	b	.L324
.L62:
	ldr	r1, .L339+104
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r1, r5, #340
	add	r0, sp, #48
	add	r1, r1, #3
	b	.L302
.L63:
	add	r0, sp, #48
	ldr	r1, .L339+108
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L64:
	add	r0, sp, #48
	ldr	r1, .L339+112
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+64
	str	r3, [r4, #136]
	b	.L276
.L65:
	add	r0, sp, #48
	ldr	r1, .L339+116
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+64
	str	r3, [r4, #136]
	b	.L276
.L66:
	str	r3, [r4, #132]
	b	.L299
.L67:
	ldr	r6, [r5, #580]
	cmp	r6, #0
	beq	.L260
	add	r5, r5, #496
	add	r5, r5, #1
	mov	r0, r5
	bl	strlen
	cmp	r0, #0
	beq	.L211
	ldr	r1, .L339+460
	mov	r0, r5
	mov	r2, #64
	bl	strncmp
	cmp	r0, #0
	addne	r0, sp, #48
	ldrne	r1, .L339+120
	bne	.L317
.L211:
	add	r0, sp, #48
	ldr	r1, .L339+124
	b	.L313
.L68:
	add	r0, sp, #48
	ldr	r1, .L339+128
	mov	r2, #148
	bl	strlcpy
	ldr	r3, [r5, #576]
.L324:
	cmp	r3, #0
	add	r0, sp, #48
	ldrne	r1, .L339+132
	ldreq	r1, .L339+136
	b	.L302
.L70:
	str	r3, [r4, #132]
.L69:
	add	r0, sp, #48
	ldr	r1, .L339+140
	mov	r2, #148
	bl	strlcpy
	mov	r3, #500
	str	r3, [r4, #136]
	b	.L276
.L71:
	add	r0, sp, #48
	ldr	r1, .L339+144
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L72:
	add	r0, sp, #48
	ldr	r1, .L339+148
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L73:
	ldr	r1, .L339+456
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r0, sp, #48
	add	r1, r5, #644
	b	.L302
.L74:
	add	r0, sp, #48
	ldr	r1, .L339+480
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L75:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #63
	bl	strlcpy
	b	.L276
.L76:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #86
	bl	strlcpy
	b	.L276
.L77:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	add	r1, r1, #45
	b	.L297
.L78:
	ldr	r3, [r4, #268]
	add	r0, sp, #48
	cmp	r3, #0
	ldrne	r1, .L339+480
	ldreq	r1, .L339+496
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L79:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #144
	bl	strlcpy
	b	.L276
.L80:
	add	r0, sp, #48
	ldr	r1, .L339+152
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L81:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #75
	bl	strlcpy
	b	.L276
.L82:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #148
	bl	strlcpy
	b	.L276
.L88:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	add	r1, r1, #169
	b	.L297
.L89:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	add	r1, r1, #175
	b	.L297
.L90:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	add	r1, r1, #181
	b	.L297
.L91:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	add	r1, r1, #187
	b	.L297
.L92:
	ldr	r3, [r4, #268]
	add	r0, sp, #48
	cmp	r3, #0
	ldrne	r1, .L339+496
	ldreq	r1, .L339+480
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L83:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #71
	bl	strlcpy
	b	.L276
.L84:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	add	r1, r1, #21
	b	.L297
.L85:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	add	r1, r1, #27
	b	.L297
.L86:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	add	r1, r1, #33
	b	.L297
.L87:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	add	r1, r1, #39
	b	.L297
.L93:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	mov	r1, #5
	ldr	r2, .L339+484
	ldreq	r3, [sl, #0]
	ldrne	r3, [r8, #0]
	bl	snprintf
	b	.L276
.L94:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #79
	bl	strlcpy
	b	.L276
.L95:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #113
	bl	strlcpy
	b	.L276
.L96:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #119
	bl	strlcpy
	b	.L276
.L97:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #125
	bl	strlcpy
	b	.L276
.L98:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #131
	bl	strlcpy
	b	.L276
.L99:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	ldreq	r1, [r4, #184]
	ldrne	r1, [r4, #192]
	mov	r2, #148
	add	r1, r1, #137
	bl	strlcpy
	b	.L276
.L100:
	add	r0, sp, #48
	ldr	r1, .L339+480
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L101:
	add	r0, sp, #48
	ldr	r1, .L339+152
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L102:
	add	r0, sp, #48
	ldr	r1, .L339+152
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L103:
	add	r0, sp, #48
	ldr	r1, .L339+496
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L104:
	add	r0, sp, #48
	ldr	r1, .L339+504
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L105:
	add	r0, sp, #48
	ldr	r1, .L339+488
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L106:
	add	r0, sp, #48
	ldr	r1, .L339+156
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L107:
	add	r0, sp, #48
	ldr	r1, .L339+500
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L108:
	add	r0, sp, #48
	ldr	r1, .L339+492
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L109:
	add	r0, sp, #48
	ldr	r1, .L339+160
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L110:
	add	r6, r5, #98
	mov	r0, r6
	bl	strlen
	cmp	r0, #0
	beq	.L240
	mov	r0, r6
	ldr	r1, .L339+460
	mov	r2, #18
	bl	strncmp
	cmp	r0, #0
	beq	.L240
	ldr	r1, .L339+468
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r0, sp, #48
	mov	r1, r6
	b	.L302
.L240:
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	str	r3, [sp, #0]
	ldrb	r3, [r5, #1]	@ zero_extendqisi2
	str	r3, [sp, #4]
	ldrb	r3, [r5, #3]	@ zero_extendqisi2
	str	r3, [sp, #8]
	ldrb	r3, [r5, #4]	@ zero_extendqisi2
	str	r3, [sp, #12]
	ldrb	r3, [r5, #6]	@ zero_extendqisi2
	str	r3, [sp, #16]
	ldrb	r3, [r5, #7]	@ zero_extendqisi2
	str	r3, [sp, #20]
	ldrb	r3, [r5, #9]	@ zero_extendqisi2
	str	r3, [sp, #24]
	ldrb	r3, [r5, #10]	@ zero_extendqisi2
	str	r3, [sp, #28]
	ldrb	r3, [r5, #12]	@ zero_extendqisi2
	str	r3, [sp, #32]
	ldrb	r3, [r5, #13]	@ zero_extendqisi2
	str	r3, [sp, #36]
	ldrb	r3, [r5, #15]	@ zero_extendqisi2
	str	r3, [sp, #40]
	ldrb	r3, [r5, #16]	@ zero_extendqisi2
	b	.L316
.L111:
	ldr	r1, .L339+164
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r1, r5, #332
	add	r0, sp, #48
	add	r1, r1, #2
	b	.L302
.L112:
	add	r0, sp, #48
	ldr	r1, .L339+168
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+64
	str	r3, [r4, #136]
	b	.L276
.L113:
	add	r0, sp, #48
	ldr	r1, .L339+172
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+64
	str	r3, [r4, #136]
	b	.L276
.L114:
	add	r0, sp, #48
	ldr	r1, .L339+176
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+64
	str	r3, [r4, #136]
	b	.L276
.L115:
	ldr	r1, .L339+448
	mov	r2, #148
	add	r0, sp, #48
	bl	strlcpy
	add	r1, r5, #604
	mov	r2, #148
	add	r0, sp, #48
	bl	strlcat
	ldr	r3, [r5, #600]
	mov	r1, #148
	ldr	r2, .L339+452
	add	r0, sp, #196
	bl	snprintf
	add	r0, sp, #48
	add	r1, sp, #196
	mov	r2, #148
	bl	strlcat
	ldr	r3, .L339+472
	str	r3, [r4, #136]
	b	.L276
.L116:
	ldr	r3, [r5, #584]
	cmp	r3, #0
	addne	r0, r5, #231
	ldrne	r1, .L339+180
	bne	.L330
	b	.L252
.L117:
	ldr	r3, [r5, #584]
	cmp	r3, #0
	beq	.L252
	ldr	r1, .L339+184
	add	r0, r5, #231
.L330:
	bl	strstr
	cmp	r0, #0
	bne	.L253
	b	.L252
.L118:
	ldr	r1, .L339+188
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r1, r5, #324
	add	r0, sp, #48
	add	r1, r1, #1
	b	.L302
.L119:
	ldr	r6, [r5, #584]
	add	r0, sp, #48
	cmp	r6, #0
	beq	.L294
	ldr	r1, .L339+192
	mov	r2, #148
	bl	strlcpy
	add	r1, r5, #320
	add	r0, sp, #196
	add	r1, r1, #1
	mov	r2, #148
	bl	strlcpy
	add	r0, sp, #196
	ldr	r1, .L339+196
	bl	strtok
	b	.L319
.L120:
	ldr	r3, [r5, #584]
	cmp	r3, #0
	addne	r0, r5, #231
	ldrne	r1, .L339+180
	bne	.L338
	b	.L252
.L121:
	ldr	r3, [r5, #584]
	cmp	r3, #0
	beq	.L252
	ldr	r1, .L339+184
	add	r0, r5, #231
.L338:
	bl	strstr
	cmp	r0, #0
	beq	.L252
	add	r6, r5, #166
	mov	r0, r6
	bl	strlen
	cmp	r0, #0
	beq	.L253
	mov	r0, r6
	ldr	r1, .L339+460
	mov	r2, #65
	bl	strncmp
	cmp	r0, #0
	beq	.L253
	ldr	r1, .L339+388
	mov	r2, #148
	add	r0, sp, #48
	bl	strlcpy
	ldr	r1, .L339+200
	add	r0, r5, #236
	mov	r2, #11
	bl	strncmp
	cmp	r0, #0
	movne	r1, r6
	add	r0, sp, #48
	bne	.L301
	ldr	r1, .L339+204
	mov	r2, #148
	bl	strlcat
	mov	r0, r6
.L318:
	add	r1, sp, #196
	bl	dev_command_escape_char_handler.constprop.4
.L319:
	add	r0, sp, #48
	add	r1, sp, #196
.L301:
	mov	r2, #148
	bl	strlcat
	b	.L295
.L253:
	add	r0, sp, #48
	ldr	r1, .L339+208
	b	.L313
.L252:
	add	r0, sp, #48
	ldr	r1, .L339+72
	mov	r2, #148
	bl	strlcpy
	mov	r6, #0
	b	.L263
.L122:
	ldr	r1, .L339+212
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r0, sp, #48
	add	r1, r5, #236
	b	.L302
.L123:
	add	r0, sp, #48
	ldr	r1, .L339+216
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L124:
	add	r0, sp, #48
	ldr	r1, .L339+220
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+64
	str	r3, [r4, #136]
	b	.L276
.L125:
	add	r0, sp, #48
	ldr	r1, .L339+224
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L126:
	add	r0, sp, #48
	ldr	r1, .L339+228
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L127:
	add	r0, sp, #48
	ldr	r1, .L339+232
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L128:
	add	r0, sp, #48
	ldr	r1, .L339+236
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L129:
	add	r0, sp, #48
	ldr	r1, .L339+240
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L130:
	add	r0, sp, #48
	ldr	r1, .L339+244
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L131:
	add	r0, sp, #48
	ldr	r1, .L339+248
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L132:
	add	r0, sp, #48
	ldr	r1, .L339+252
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L133:
	add	r0, sp, #48
	ldr	r1, .L339+496
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L134:
	add	r0, sp, #48
	ldr	r1, .L339+256
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L135:
	add	r0, sp, #48
	mov	r1, #255
	mov	r2, #1
	bl	memset
	mov	r3, #0
	str	r3, [r4, #132]
	b	.L276
.L136:
	add	r0, sp, #48
	ldr	r1, .L339+260
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L137:
	ldr	r6, [r5, #588]
	cmp	r6, #0
	beq	.L260
	add	r5, r5, #247
	mov	r0, r5
	bl	strlen
	cmp	r0, #0
	beq	.L257
	mov	r0, r5
	ldr	r1, .L339+460
	mov	r2, #64
	bl	strncmp
	cmp	r0, #0
	beq	.L257
	ldr	r1, .L339+264
	mov	r0, r5
	mov	r2, #64
	bl	strncmp
	subs	r6, r0, #0
	ldrne	r1, .L339+268
	add	r0, sp, #48
	bne	.L317
	b	.L294
.L257:
	add	r0, sp, #48
	ldr	r1, .L339+272
	b	.L313
.L138:
	ldr	r1, .L339+276
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r0, sp, #48
	add	r1, r5, #356
	b	.L302
.L139:
	add	r0, sp, #48
	ldr	r1, .L339+280
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L140:
	add	r0, sp, #48
	ldr	r1, .L339+284
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L141:
	ldr	r6, [r5, #592]
	cmp	r6, #0
	beq	.L260
	add	r5, r5, #432
	add	r5, r5, #1
	mov	r0, r5
	bl	strlen
	cmp	r0, #0
	beq	.L261
	mov	r0, r5
	ldr	r1, .L339+460
	mov	r2, #64
	bl	strncmp
	cmp	r0, #0
	beq	.L261
	mov	r0, r5
	ldr	r1, .L339+264
	mov	r2, #64
	bl	strncmp
	subs	r6, r0, #0
	add	r0, sp, #48
	beq	.L294
	ldr	r1, .L339+288
.L317:
	mov	r2, #148
	bl	strlcpy
	mov	r0, r5
	b	.L318
.L261:
	ldr	r1, .L339+292
	add	r0, sp, #48
.L313:
	mov	r2, #148
	bl	strlcpy
.L295:
	mov	r6, #1
	b	.L263
.L260:
	add	r0, sp, #48
.L294:
	ldr	r1, .L339+72
	mov	r2, #148
	bl	strlcpy
.L263:
	ldr	r3, .L339+472
	cmp	r6, #0
	str	r3, [r4, #136]
	beq	.L276
	b	.L283
.L142:
	add	r0, sp, #48
	ldr	r1, .L339+296
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L143:
	add	r0, sp, #48
	ldr	r1, .L339+300
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L144:
	add	r0, sp, #48
	ldr	r1, .L339+304
	b	.L314
.L145:
	add	r0, sp, #48
	ldr	r1, .L339+308
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L146:
	add	r0, sp, #48
	ldr	r1, .L339+312
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L147:
	add	r5, r5, #120
	mov	r0, r5
	bl	strlen
	cmp	r0, #0
	beq	.L264
	mov	r0, r5
	ldr	r1, .L339+460
	mov	r2, #33
	bl	strncmp
	cmp	r0, #0
	beq	.L264
	add	r0, sp, #48
	ldr	r1, .L339+316
	mov	r2, #148
	bl	strlcpy
	mov	r0, r5
	add	r1, sp, #196
	bl	dev_command_escape_char_handler.constprop.4
	b	.L321
.L264:
	add	r0, sp, #48
	ldr	r1, .L339+320
	b	.L314
.L148:
	ldr	r3, [r4, #8]
	add	r0, sp, #48
	cmp	r3, #4
	cmpne	r3, #9
	ldreq	r1, .L339+324
	ldrne	r1, .L339+328
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L149:
	ldr	r1, .L339+332
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r0, sp, #48
	add	r1, r5, #231
	b	.L302
.L150:
	ldr	r3, [r4, #8]
	cmp	r3, #7
	bne	.L267
	add	r0, sp, #48
	ldr	r1, .L339+336
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L267:
	cmp	r3, #6
	cmpne	r3, #8
	bne	.L276
	add	r0, sp, #48
	ldr	r1, .L339+340
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L151:
	add	r0, sp, #48
	ldr	r1, .L339+344
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L152:
	add	r5, r5, #368
	add	r5, r5, #1
	mov	r0, r5
	bl	strlen
	cmp	r0, #0
	beq	.L268
	mov	r0, r5
	ldr	r1, .L339+460
	mov	r2, #64
	bl	strncmp
	cmp	r0, #0
	beq	.L268
	mov	r0, r5
	ldr	r1, .L339+264
	mov	r2, #64
	bl	strncmp
	cmp	r0, #0
	add	r0, sp, #48
	beq	.L269
	mov	r2, #148
	ldr	r1, .L339+348
	bl	strlcpy
	mov	r0, r5
	add	r1, sp, #196
	bl	dev_command_escape_char_handler.constprop.4
	add	r0, sp, #48
	add	r1, sp, #196
	mov	r2, #148
	bl	strlcat
	b	.L283
.L269:
	ldr	r1, .L339+72
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L268:
	add	r0, sp, #48
	ldr	r1, .L339+352
	b	.L296
.L153:
	add	r0, sp, #48
	ldr	r1, .L339+356
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L154:
	add	r0, sp, #48
	ldr	r1, .L339+360
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+64
	str	r3, [r4, #136]
	b	.L276
.L155:
	ldr	r1, .L339+364
	mov	r2, #148
	add	r0, sp, #48
	bl	strlcpy
	add	r1, r5, #308
	add	r1, r1, #3
	mov	r2, #148
	add	r0, sp, #196
	bl	strlcpy
	ldr	r1, .L339+368
	add	r0, sp, #196
	bl	strtok
	add	r0, sp, #48
	add	r1, sp, #196
	b	.L302
.L156:
	ldr	r3, [r5, #584]
	cmp	r3, #0
	beq	.L270
	ldr	r1, .L339+180
	add	r0, r5, #231
	bl	strstr
	cmp	r0, #0
	addne	r0, sp, #48
	ldrne	r1, .L339+372
	bne	.L314
.L270:
	mov	r0, r4
	mov	r1, #16
	bl	dev_increment_list_item_pointer
	ldr	r1, .L339+364
	mov	r2, #148
	add	r0, sp, #48
	bl	strlcpy
	add	r1, r5, #308
	add	r1, r1, #3
	add	r0, sp, #196
	mov	r2, #148
	bl	strlcpy
	add	r0, sp, #196
	ldr	r1, .L339+368
	bl	strtok
.L321:
	add	r0, sp, #48
	add	r1, sp, #196
	b	.L315
.L157:
	add	r0, sp, #48
	ldr	r1, .L339+376
	b	.L314
.L158:
	add	r0, sp, #48
	ldr	r1, .L339+380
	b	.L314
.L340:
	.align	2
.L339:
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	2500
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	.LC23
	.word	.LC26
	.word	.LC27
	.word	.LC28
	.word	.LC29
	.word	.LC30
	.word	.LC31
	.word	.LC32
	.word	.LC33
	.word	.LC34
	.word	.LC36
	.word	.LC37
	.word	.LC38
	.word	.LC24
	.word	.LC25
	.word	.LC39
	.word	.LC40
	.word	.LC41
	.word	.LC45
	.word	.LC49
	.word	.LC52
	.word	.LC55
	.word	.LC56
	.word	.LC57
	.word	.LC58
	.word	.LC61
	.word	.LC63
	.word	.LC64
	.word	.LC65
	.word	.LC66
	.word	.LC68
	.word	.LC69
	.word	.LC62
	.word	.LC70
	.word	.LC71
	.word	.LC72
	.word	.LC73
	.word	.LC74
	.word	.LC75
	.word	.LC76
	.word	.LC77
	.word	.LC78
	.word	.LC79
	.word	.LC80
	.word	.LC81
	.word	.LC82
	.word	.LC83
	.word	.LC84
	.word	.LC85
	.word	.LC86
	.word	.LC87
	.word	.LC88
	.word	.LC89
	.word	.LC90
	.word	.LC91
	.word	.LC92
	.word	.LC93
	.word	.LC94
	.word	.LC95
	.word	.LC96
	.word	.LC97
	.word	.LC98
	.word	.LC99
	.word	.LC100
	.word	.LC101
	.word	.LC102
	.word	.LC103
	.word	.LC104
	.word	.LC105
	.word	.LC106
	.word	.LC107
	.word	.LC108
	.word	.LC109
	.word	.LC110
	.word	.LC111
	.word	.LC112
	.word	.LC113
	.word	.LC67
	.word	.LC114
	.word	.LC115
	.word	.LC116
	.word	.LC117
	.word	.LC118
	.word	.LC119
	.word	.LC120
	.word	.LC121
	.word	.LC122
	.word	.LC123
	.word	.LC124
	.word	2500
	.word	.LC125
	.word	.LC126
	.word	.LC59
	.word	.LC60
	.word	.LC42
	.word	.LC35
	.word	.LC54
	.word	.LC53
	.word	1500
	.word	.LC127
	.word	.LC43
	.word	.LC46
	.word	.LC48
	.word	.LC51
	.word	.LC44
	.word	.LC50
	.word	.LC47
	.word	.LC128
	.word	.LC21
	.word	.LC0
	.word	2674
.L159:
	ldr	r1, .L339+384
	add	r0, sp, #48
.L314:
	mov	r2, #148
	bl	strlcpy
	b	.L303
.L160:
	ldr	r1, .L339+388
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r1, r5, #324
	add	r0, sp, #48
	add	r1, r1, #1
.L315:
	mov	r2, #148
	bl	strlcat
.L303:
	ldr	r3, .L339+436
	b	.L298
.L161:
	add	r0, sp, #48
	ldr	r1, .L339+392
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L162:
	add	r0, r5, #324
	ldr	r1, .L339+396
	add	r0, r0, #3
	bl	strstr
	cmp	r0, #0
	ldrne	r1, .L339+400
	ldreq	r1, .L339+404
	add	r0, sp, #48
.L296:
	mov	r2, #148
	bl	strlcpy
	b	.L275
.L163:
	add	r0, sp, #48
	ldr	r1, .L339+408
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+436
	str	r3, [r4, #136]
	b	.L276
.L165:
	str	r3, [r4, #132]
.L164:
	add	r0, sp, #48
	ldr	r1, .L339+412
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+436
	str	r3, [r4, #136]
	b	.L276
.L166:
	add	r0, sp, #48
	ldr	r1, .L339+416
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L167:
	add	r0, sp, #48
	ldr	r1, .L339+420
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L168:
	add	r0, sp, #48
	ldr	r1, .L339+424
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L169:
	add	r0, sp, #48
	ldr	r1, .L339+428
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L170:
	add	r0, sp, #48
	ldr	r1, .L339+432
	mov	r2, #148
	bl	strlcpy
	ldr	r3, .L339+436
	str	r3, [r4, #136]
	b	.L276
.L171:
	add	r0, sp, #48
	ldr	r1, .L339+480
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L172:
	add	r0, sp, #48
	ldr	r1, .L339+440
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L173:
	add	r0, sp, #48
	ldr	r1, .L339+444
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L174:
	ldr	r1, .L339+448
	mov	r2, #148
	add	r0, sp, #48
	bl	strlcpy
	add	r1, r6, #124
	mov	r2, #148
	add	r0, sp, #48
	bl	strlcat
	ldr	r3, [r6, #120]
	mov	r1, #148
	ldr	r2, .L339+452
	add	r0, sp, #196
	bl	snprintf
	add	r0, sp, #48
	add	r1, sp, #196
	mov	r2, #148
	bl	strlcat
	ldr	r3, .L339+472
	str	r3, [r4, #136]
	b	.L276
.L175:
	ldr	r1, .L339+456
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r0, sp, #48
	add	r1, r6, #164
	b	.L302
.L176:
	add	r5, r6, #98
	mov	r0, r5
	bl	strlen
	cmp	r0, #0
	beq	.L274
	mov	r0, r5
	ldr	r1, .L339+460
	mov	r2, #18
	bl	strncmp
	cmp	r0, #0
	beq	.L274
	ldr	r1, .L339+468
	add	r0, sp, #48
	mov	r2, #148
	bl	strlcpy
	add	r0, sp, #48
	mov	r1, r5
.L302:
	mov	r2, #148
	bl	strlcat
	b	.L275
.L274:
	ldrb	r3, [r6, #0]	@ zero_extendqisi2
	str	r3, [sp, #0]
	ldrb	r3, [r6, #1]	@ zero_extendqisi2
	str	r3, [sp, #4]
	ldrb	r3, [r6, #3]	@ zero_extendqisi2
	str	r3, [sp, #8]
	ldrb	r3, [r6, #4]	@ zero_extendqisi2
	str	r3, [sp, #12]
	ldrb	r3, [r6, #6]	@ zero_extendqisi2
	str	r3, [sp, #16]
	ldrb	r3, [r6, #7]	@ zero_extendqisi2
	str	r3, [sp, #20]
	ldrb	r3, [r6, #9]	@ zero_extendqisi2
	str	r3, [sp, #24]
	ldrb	r3, [r6, #10]	@ zero_extendqisi2
	str	r3, [sp, #28]
	ldrb	r3, [r6, #12]	@ zero_extendqisi2
	str	r3, [sp, #32]
	ldrb	r3, [r6, #13]	@ zero_extendqisi2
	str	r3, [sp, #36]
	ldrb	r3, [r6, #15]	@ zero_extendqisi2
	str	r3, [sp, #40]
	ldrb	r3, [r6, #16]	@ zero_extendqisi2
.L316:
	str	r3, [sp, #44]
	add	r0, sp, #48
	mov	r1, #148
	ldr	r2, .L339+464
	ldr	r3, .L339+468
	bl	snprintf
.L275:
	ldr	r3, .L339+472
.L298:
	str	r3, [r4, #136]
	b	.L283
.L177:
	add	r0, sp, #48
	ldr	r1, .L339+476
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L178:
	add	r0, sp, #48
	ldr	r1, .L339+492
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L179:
	ldr	r1, [r4, #192]
	add	r0, sp, #48
	add	r1, r1, #210
	b	.L297
.L180:
	ldr	r3, [r4, #192]
	add	r0, sp, #48
	ldr	r3, [r3, #376]
	mov	r1, #148
	ldr	r2, .L339+484
	bl	snprintf
	ldr	r3, [r4, #192]
	ldr	r3, [r3, #376]
	cmp	r3, #2
	beq	.L278
	cmp	r3, #3
	beq	.L279
	cmp	r3, #0
	bne	.L276
	mov	r0, r4
	mov	r1, #17
	bl	dev_increment_list_item_pointer
	b	.L276
.L278:
	mov	r0, r4
	mov	r1, #7
	bl	dev_increment_list_item_pointer
	b	.L276
.L279:
	mov	r0, r4
	mov	r1, #12
	bl	dev_increment_list_item_pointer
	b	.L276
.L181:
	ldr	r3, [r4, #192]
	add	r0, sp, #48
	mov	r1, #148
	ldr	r2, .L339+484
	ldr	r3, [r3, #380]
	bl	snprintf
	b	.L276
.L182:
	ldr	r3, [r4, #192]
	add	r0, sp, #48
	ldr	r3, [r3, #384]
	mov	r1, #148
	ldr	r2, .L339+484
	add	r3, r3, #1
	bl	snprintf
	b	.L276
.L183:
	ldr	r3, [r4, #192]
	add	r0, sp, #48
	ldr	r2, [r3, #400]
	cmp	r2, #0
	bne	.L280
	ldr	r3, [r3, #396]
	cmp	r3, #0
	beq	.L281
.L280:
	ldr	r1, .L339+480
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L281:
	ldr	r1, .L339+496
	mov	r2, #148
	bl	strlcpy
	mov	r0, r4
	mov	r1, #3
	bl	dev_increment_list_item_pointer
	b	.L276
.L184:
	add	r0, sp, #48
	ldr	r1, .L339+496
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L185:
	ldr	r3, [r4, #192]
	add	r0, sp, #48
	mov	r1, #148
	ldr	r2, .L339+484
	ldr	r3, [r3, #372]
	bl	snprintf
	b	.L276
.L186:
	ldr	r1, [r4, #192]
	add	r0, sp, #48
	ldr	r3, [r1, #372]
	cmp	r3, #0
	addeq	r1, r1, #243
	addne	r1, r1, #308
.L297:
	mov	r2, #148
	bl	strlcpy
	b	.L283
.L187:
	add	r0, sp, #48
	ldr	r1, .L339+504
	mov	r2, #148
	bl	strlcpy
	mov	r0, r4
	mov	r1, #10
	bl	dev_increment_list_item_pointer
	b	.L276
.L188:
	ldr	r3, [r4, #192]
	add	r0, sp, #48
	mov	r1, #148
	ldr	r2, .L339+484
	ldr	r3, [r3, #388]
	bl	snprintf
	mov	r0, r4
	mov	r1, #5
	bl	dev_increment_list_item_pointer
	b	.L276
.L189:
	ldr	r3, [r4, #192]
	add	r0, sp, #48
	mov	r1, #148
	ldr	r2, .L339+484
	ldr	r3, [r3, #392]
	bl	snprintf
	b	.L276
.L190:
	add	r0, sp, #48
	ldr	r1, .L339+504
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L191:
	add	r0, sp, #48
	ldr	r1, .L339+488
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L192:
	add	r0, sp, #48
	ldr	r1, .L339+492
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L193:
	add	r0, sp, #48
	ldr	r1, .L339+496
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L194:
	add	r0, sp, #48
	ldr	r1, .L339+496
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L195:
	add	r0, sp, #48
	ldr	r1, .L339+500
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L196:
	add	r0, sp, #48
	ldr	r1, .L339+504
	mov	r2, #148
	bl	strlcpy
	b	.L276
.L36:
	ldr	r0, .L339+508
	bl	Alert_Message
.L299:
	mov	r4, #0
	b	.L209
.L283:
	add	r0, sp, #48
	ldr	r1, .L339+512
	mov	r2, #148
	bl	strlcat
.L276:
	add	r0, sp, #48
	bl	strlen
	ldr	r1, .L339+516
	ldr	r2, .L339+520
	add	r0, r0, #1
	bl	mem_malloc_debug
	mov	r4, r0
	add	r0, sp, #48
	bl	strlen
	add	r1, sp, #48
	add	r2, r0, #1
	mov	r0, r4
	bl	memcpy
.L209:
	mov	r0, r4
	add	sp, sp, #344
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.LFE23:
	.size	dev_get_command_text, .-dev_get_command_text
	.section	.text.FDTO_e_SHARED_show_obtain_ip_automatically_dropdown,"ax",%progbits
	.align	2
	.global	FDTO_e_SHARED_show_obtain_ip_automatically_dropdown
	.type	FDTO_e_SHARED_show_obtain_ip_automatically_dropdown, %function
FDTO_e_SHARED_show_obtain_ip_automatically_dropdown:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L342
	mov	r0, #217
	ldr	r2, [r3, #0]
	mov	r1, #39
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L343:
	.align	2
.L342:
	.word	GuiVar_ENObtainIPAutomatically
.LFE0:
	.size	FDTO_e_SHARED_show_obtain_ip_automatically_dropdown, .-FDTO_e_SHARED_show_obtain_ip_automatically_dropdown
	.section	.text.FDTO_e_SHARED_show_subnet_dropdown,"ax",%progbits
	.align	2
	.global	FDTO_e_SHARED_show_subnet_dropdown
	.type	FDTO_e_SHARED_show_subnet_dropdown, %function
FDTO_e_SHARED_show_subnet_dropdown:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L345
	ldr	r0, .L345+4
	ldr	r1, .L345+8
	ldr	r3, [r3, #0]
	mov	r2, #25
	b	FDTO_COMBOBOX_show
.L346:
	.align	2
.L345:
	.word	GuiVar_ENNetmask
	.word	726
	.word	FDTO_COMBOBOX_add_items
.LFE1:
	.size	FDTO_e_SHARED_show_subnet_dropdown, .-FDTO_e_SHARED_show_subnet_dropdown
	.section	.text.e_SHARED_show_keyboard,"ax",%progbits
	.align	2
	.global	e_SHARED_show_keyboard
	.type	e_SHARED_show_keyboard, %function
e_SHARED_show_keyboard:
.LFB2:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI4:
	ldr	r9, [sp, #40]
	mov	r7, r3
	mov	r5, r1
	mov	r4, r2
	mov	r6, r0
	ldr	sl, [sp, #32]
	ldr	r8, [sp, #36]
	bl	good_key_beep
	cmp	r9, #0
	movne	r1, r4
	ldreq	r1, .L351
	mov	r2, r7
	ldr	r0, .L351+4
	bl	strlcpy
	mov	r2, r7
	mov	r0, r4
	mov	r1, #0
	bl	memset
	ldr	r3, .L351+8
	mov	r0, r6
	ldrsh	r3, [r3, #0]
	mov	r1, r5
	str	r3, [sl, #0]
	mov	r2, r7
	mov	r3, r8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	KEYBOARD_draw_keyboard
.L352:
	.align	2
.L351:
	.word	.LC129
	.word	GuiVar_GroupName
	.word	GuiLib_ActiveCursorFieldNo
.LFE2:
	.size	e_SHARED_show_keyboard, .-e_SHARED_show_keyboard
	.section	.text.e_SHARED_get_easyGUI_string_at_index,"ax",%progbits
	.align	2
	.global	e_SHARED_get_easyGUI_string_at_index
	.type	e_SHARED_get_easyGUI_string_at_index, %function
e_SHARED_get_easyGUI_string_at_index:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r0, r1, r0
	mov	r0, r0, asl #16
	mov	r1, #0
	mov	r0, r0, lsr #16
	mov	r2, r1
	b	GuiLib_GetTextLanguagePtr
.LFE3:
	.size	e_SHARED_get_easyGUI_string_at_index, .-e_SHARED_get_easyGUI_string_at_index
	.section	.text.e_SHARED_get_index_of_easyGUI_string,"ax",%progbits
	.align	2
	.global	e_SHARED_get_index_of_easyGUI_string
	.type	e_SHARED_get_index_of_easyGUI_string, %function
e_SHARED_get_index_of_easyGUI_string:
.LFB4:
	@ args = 4, pretend = 0, frame = 68
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI5:
	mov	ip, r0
	sub	sp, sp, #68
.LCFI6:
	mov	r4, r1
	mov	r5, r2
	mov	r1, ip
	mov	r2, #65
	mov	r0, sp
	mov	r6, r3
	ldr	r7, [sp, #88]
	bl	strlcpy
	mov	r0, sp
	mov	r1, #13
	bl	strchr
	cmp	r0, #0
	beq	.L356
	mov	r0, sp
	mov	r1, #13
	bl	strchr
	mov	r1, #0
	mov	r2, #1
	bl	memset
	b	.L356
.L359:
	add	r0, r6, r5
	mov	r1, #0
	mov	r0, r0, asl #16
	mov	r2, r1
	mov	r0, r0, lsr #16
	bl	GuiLib_GetTextLanguagePtr
	mov	r2, r4
	mov	r1, r0
	mov	r0, sp
	bl	strncmp
	cmp	r0, #0
	moveq	r0, r6
	beq	.L358
.L357:
	add	r6, r6, #1
.L356:
	cmp	r6, r7
	bls	.L359
	mvn	r0, #0
.L358:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE4:
	.size	e_SHARED_get_index_of_easyGUI_string, .-e_SHARED_get_index_of_easyGUI_string
	.section	.text.e_SHARED_start_device_communication,"ax",%progbits
	.align	2
	.global	e_SHARED_start_device_communication
	.type	e_SHARED_start_device_communication, %function
e_SHARED_start_device_communication:
.LFB5:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #1
	cmp	r1, #87
	str	lr, [sp, #-4]!
.LCFI7:
	str	r3, [r2, #0]
	sub	sp, sp, #40
.LCFI8:
	moveq	r3, #4608
	beq	.L364
	cmp	r1, #77
	ldreq	r3, .L365
	movne	r3, #4352
.L364:
	str	r0, [sp, #32]
	mov	r0, sp
	str	r3, [sp, #0]
	bl	COMM_MNGR_post_event_with_details
	ldr	r0, .L365+4
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, .L365+8
	mov	r2, #0
	str	r2, [r3, #0]
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L366:
	.align	2
.L365:
	.word	4368
	.word	.LANCHOR2
	.word	.LANCHOR3
.LFE5:
	.size	e_SHARED_start_device_communication, .-e_SHARED_start_device_communication
	.section	.text.e_SHARED_network_connect_delay_is_over,"ax",%progbits
	.align	2
	.global	e_SHARED_network_connect_delay_is_over
	.type	e_SHARED_network_connect_delay_is_over, %function
e_SHARED_network_connect_delay_is_over:
.LFB6:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI9:
	mov	r4, r0
	mov	r0, sp
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, .L368
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	orr	r0, r2, r0, asl #8
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	orr	r0, r0, r2, asl #16
	orr	r0, r0, r3, asl #24
	ldr	r3, [sp, #0]
	rsb	r0, r0, r3
	ldr	r3, .L368+4
	cmp	r0, r4
	str	r0, [r3, #0]
	movcc	r0, #0
	movcs	r0, #1
	ldmfd	sp!, {r2, r3, r4, pc}
.L369:
	.align	2
.L368:
	.word	.LANCHOR2
	.word	.LANCHOR3
.LFE6:
	.size	e_SHARED_network_connect_delay_is_over, .-e_SHARED_network_connect_delay_is_over
	.section	.text.e_SHARED_get_info_text_from_programming_struct,"ax",%progbits
	.align	2
	.global	e_SHARED_get_info_text_from_programming_struct
	.type	e_SHARED_get_info_text_from_programming_struct, %function
e_SHARED_get_info_text_from_programming_struct:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L371
	ldr	r0, .L371+4
	ldr	r1, [r3, #0]
	mov	r2, #49
	add	r1, r1, #24
	b	strlcpy
.L372:
	.align	2
.L371:
	.word	.LANCHOR1
	.word	GuiVar_CommOptionInfoText
.LFE7:
	.size	e_SHARED_get_info_text_from_programming_struct, .-e_SHARED_get_info_text_from_programming_struct
	.section	.text.e_SHARED_string_validation,"ax",%progbits
	.align	2
	.global	e_SHARED_string_validation
	.type	e_SHARED_string_validation, %function
e_SHARED_string_validation:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI10:
	mov	r5, r0
	mov	r4, r1
	bl	strlen
	cmp	r0, #0
	ldmnefd	sp!, {r4, r5, pc}
	ldr	r0, .L377
	bl	strlen
	mov	r2, r4
	cmp	r4, r0
	ldrhi	r1, .L377
	ldrls	r1, .L377+4
	mov	r0, r5
	ldmfd	sp!, {r4, r5, lr}
	b	strlcpy
.L378:
	.align	2
.L377:
	.word	.LC35
	.word	.LC130
.LFE8:
	.size	e_SHARED_string_validation, .-e_SHARED_string_validation
	.section	.text.e_SHARED_index_keycode_for_gui,"ax",%progbits
	.align	2
	.global	e_SHARED_index_keycode_for_gui
	.type	e_SHARED_index_keycode_for_gui, %function
e_SHARED_index_keycode_for_gui:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	r0, r0, #36864
	bx	lr
.LFE9:
	.size	e_SHARED_index_keycode_for_gui, .-e_SHARED_index_keycode_for_gui
	.section	.text.dev_strip_crlf_characters,"ax",%progbits
	.align	2
	.global	dev_strip_crlf_characters
	.type	dev_strip_crlf_characters, %function
dev_strip_crlf_characters:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI11:
	mov	r1, #13
	mov	r4, r0
	bl	strchr
	cmp	r0, #0
	beq	.L381
	mov	r1, #13
	mov	r0, r4
	bl	strchr
	mov	r1, #0
	mov	r2, #1
	bl	memset
.L381:
	mov	r0, r4
	mov	r1, #10
	bl	strchr
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
	mov	r0, r4
	mov	r1, #10
	bl	strchr
	mov	r1, #0
	mov	r2, #1
	ldmfd	sp!, {r4, lr}
	b	memset
.LFE14:
	.size	dev_strip_crlf_characters, .-dev_strip_crlf_characters
	.section	.text.dev_extract_text_from_buffer,"ax",%progbits
	.align	2
	.global	dev_extract_text_from_buffer
	.type	dev_extract_text_from_buffer, %function
dev_extract_text_from_buffer:
.LFB15:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI12:
	ldr	r7, [sp, #24]
	mov	r6, r0
	mov	r5, r1
	mov	r4, r2
	mov	r1, #0
	mov	r2, r3
	mov	r0, r7
	mov	r8, r3
	bl	memset
	mov	r1, r5
	mov	r0, r6
	bl	strstr
	subs	r1, r0, #0
	beq	.L384
	add	r1, r1, r4
	mov	r2, r8
	mov	r0, r7
	bl	strlcpy
	mov	r0, r7
	mov	r1, #13
	bl	strchr
	cmp	r0, #0
	beq	.L385
	mov	r1, #13
	mov	r0, r7
	bl	strchr
	mov	r1, #0
	mov	r2, #1
	bl	memset
.L385:
	mov	r0, r7
	mov	r1, #10
	bl	strchr
	cmp	r0, #0
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, pc}
	mov	r0, r7
	mov	r1, #10
	bl	strchr
	mov	r1, #0
	mov	r2, #1
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	memset
.L384:
	ldr	r0, .L387
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	Alert_Message
.L388:
	.align	2
.L387:
	.word	.LC131
.LFE15:
	.size	dev_extract_text_from_buffer, .-dev_extract_text_from_buffer
	.section	.text.dev_extract_delimited_text_from_buffer,"ax",%progbits
	.align	2
	.global	dev_extract_delimited_text_from_buffer
	.type	dev_extract_delimited_text_from_buffer, %function
dev_extract_delimited_text_from_buffer:
.LFB16:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI13:
	ldr	r7, [sp, #32]
	ldr	r6, [sp, #36]
	mov	r5, r0
	mov	r9, r2
	mov	r4, r1
	mov	r2, r7
	mov	r1, #0
	mov	r0, r6
	mov	sl, r3
	ldr	r8, [sp, #40]
	bl	memset
	mov	r0, r5
	mov	r1, r9
	bl	strstr
	cmp	r0, #0
	beq	.L390
	add	sl, r0, sl
	mov	r1, sl
	mov	r2, r7
	mov	r0, r6
	bl	strlcpy
	mov	r0, r6
	mov	r1, r4
	bl	strstr
	cmp	r0, #0
	bne	.L393
	mov	r0, r6
	ldr	r1, .L394
	bl	strstr
	cmp	r0, #0
	bne	.L393
	mov	r0, r6
	ldr	r1, .L394+4
	bl	strstr
	cmp	r0, #0
	bne	.L393
	sub	r0, r7, #1
	ldrb	r2, [sl, r0]	@ zero_extendqisi2
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L392
	adds	r0, r6, r0
	beq	.L392
.L393:
	mov	r1, #0
	mov	r2, #1
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	memset
.L392:
	mov	r0, r6
	mov	r1, #0
	mov	r2, r7
	bl	memset
	ldr	r0, .L394+8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	Alert_Message
.L390:
	ldr	r0, .L394+12
	mov	r1, r8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	Alert_Message_va
.L395:
	.align	2
.L394:
	.word	.LC21
	.word	.LC132
	.word	.LC133
	.word	.LC134
.LFE16:
	.size	dev_extract_delimited_text_from_buffer, .-dev_extract_delimited_text_from_buffer
	.section	.text.dev_analyze_enable_show_ip,"ax",%progbits
	.align	2
	.global	dev_analyze_enable_show_ip
	.type	dev_analyze_enable_show_ip, %function
dev_analyze_enable_show_ip:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #8]
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI14:
	ldr	r5, [r0, #164]
	cmp	r3, #7
	mov	r4, r0
	bne	.L397
	ldr	r0, [r0, #148]
	ldr	r1, .L405
	ldr	r2, [r4, #152]
	bl	find_string_in_block
	cmp	r0, #0
	beq	.L398
	ldr	r3, .L405+4
	ldr	r1, .L405+8
	str	r3, [sp, #4]
	mov	r2, #22
	mov	r3, #16
	str	r5, [sp, #0]
	bl	dev_extract_text_from_buffer
	b	.L396
.L398:
	mov	r0, r5
	ldr	r1, .L405+12
	mov	r2, #16
	bl	strlcpy
	ldr	r0, .L405+16
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_Message
.L397:
	cmp	r3, #6
	cmpne	r3, #8
	bne	.L396
	ldr	r0, [r0, #148]
	ldr	r1, .L405+20
	ldr	r2, [r4, #152]
	bl	find_string_in_block
	cmp	r0, #0
	beq	.L400
	mov	r3, #18
	str	r3, [sp, #0]
	add	r3, r4, #247
	str	r3, [sp, #4]
	ldr	r3, .L405+24
	ldr	r1, .L405+28
	str	r3, [sp, #8]
	ldr	r2, .L405+32
	mov	r3, #22
	bl	dev_extract_delimited_text_from_buffer
.L400:
	add	r5, r4, #247
	b	.L401
.L402:
	bl	toupper
	strb	r0, [r5, #-1]
.L401:
	ldrb	r0, [r5], #1	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L402
	ldr	r3, [r4, #8]
	cmp	r3, #6
	bne	.L396
	ldr	r0, [r4, #148]
	ldr	r1, .L405+36
	ldr	r2, [r4, #152]
	bl	find_string_in_block
	cmp	r0, #0
	beq	.L396
	mov	r3, #49
	str	r3, [sp, #0]
	ldr	r3, .L405+40
	ldr	r4, .L405+44
	str	r3, [sp, #8]
	ldr	r1, .L405+28
	ldr	r2, .L405+32
	mov	r3, #22
	str	r4, [sp, #4]
	bl	dev_extract_delimited_text_from_buffer
	b	.L403
.L404:
	bl	toupper
	strb	r0, [r4, #-1]
.L403:
	ldrb	r0, [r4], #1	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L404
.L396:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, pc}
.L406:
	.align	2
.L405:
	.word	.LC135
	.word	.LC137
	.word	.LC136
	.word	.LC138
	.word	.LC139
	.word	.LC140
	.word	.LC142
	.word	.LC21
	.word	.LC141
	.word	.LC143
	.word	.LC144
	.word	wen_details
.LFE12:
	.size	dev_analyze_enable_show_ip, .-dev_analyze_enable_show_ip
	.section	.text.dev_extract_ip_octets,"ax",%progbits
	.align	2
	.global	dev_extract_ip_octets
	.type	dev_extract_ip_octets, %function
dev_extract_ip_octets:
.LFB17:
	@ args = 4, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI15:
	sub	sp, sp, #20
.LCFI16:
	mov	r5, r1
	mov	r4, r2
	ldr	r7, [sp, #44]
	mov	r6, r0
	mov	r2, #6
	mov	r0, r1
	mov	r1, #0
	mov	r8, r3
	bl	memset
	mov	r1, #0
	mov	r2, #6
	mov	r0, r4
	bl	memset
	mov	r1, #0
	mov	r2, #6
	mov	r0, r8
	bl	memset
	mov	r1, #0
	mov	r2, #6
	mov	r0, r7
	bl	memset
	mov	r1, r6
	mov	r2, #17
	mov	r0, sp
	bl	strlcpy
	mov	r2, #17
	ldr	r1, .L410
	mov	r0, sp
	bl	strlcat
	ldr	r1, .L410
	mov	r0, sp
	bl	strtok
	mov	r2, #4
	mov	r1, r0
	mov	r0, r5
	bl	strlcpy
	ldr	r1, .L410
	mov	r0, #0
	bl	strtok
	mov	r2, #4
	mov	r1, r0
	mov	r0, r4
	bl	strlcpy
	ldr	r1, .L410
	mov	r0, #0
	bl	strtok
	mov	r2, #4
	mov	r1, r0
	mov	r0, r8
	bl	strlcpy
	ldr	r1, .L410
	mov	r0, #0
	bl	strtok
	mov	r2, #4
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L408
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L408
	ldrb	r3, [r8, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L408
	ldrb	r3, [r7, #0]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L407
.L408:
	ldr	r0, .L410+4
	bl	Alert_Message
.L407:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L411:
	.align	2
.L410:
	.word	.LC145
	.word	.LC146
.LFE17:
	.size	dev_extract_ip_octets, .-dev_extract_ip_octets
	.section	.text.dev_count_true_mask_bits,"ax",%progbits
	.align	2
	.global	dev_count_true_mask_bits
	.type	dev_count_true_mask_bits, %function
dev_count_true_mask_bits:
.LFB18:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI17:
	sub	sp, sp, #20
.LCFI18:
	mov	r1, r0
	mov	r2, #17
	mov	r0, sp
	bl	strlcpy
	mov	r2, #17
	ldr	r1, .L426
	mov	r0, sp
	bl	strlcat
	ldr	r1, .L426
	mov	r0, sp
	bl	strtok
	bl	atoi
	mov	r5, #4
	mov	r4, #0
.L424:
	cmp	r0, #240
	beq	.L417
	bhi	.L422
	cmp	r0, #192
	beq	.L415
	cmp	r0, #224
	beq	.L416
	cmp	r0, #128
	bne	.L413
	b	.L414
.L422:
	cmp	r0, #252
	beq	.L419
	bhi	.L423
	cmp	r0, #248
	bne	.L413
	b	.L418
.L423:
	cmp	r0, #254
	beq	.L420
	cmp	r0, #255
	bne	.L413
	add	r4, r4, #1
.L420:
	add	r4, r4, #1
.L419:
	add	r4, r4, #1
.L418:
	add	r4, r4, #1
.L417:
	add	r4, r4, #1
.L416:
	add	r4, r4, #1
.L415:
	add	r4, r4, #1
.L414:
	add	r4, r4, #1
.L413:
	ldr	r1, .L426
	mov	r0, #0
	bl	strtok
	bl	atoi
	subs	r5, r5, #1
	bne	.L424
	mov	r0, r4
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, pc}
.L427:
	.align	2
.L426:
	.word	.LC145
.LFE18:
	.size	dev_count_true_mask_bits, .-dev_count_true_mask_bits
	.section	.text.dev_cli_disconnect,"ax",%progbits
	.align	2
	.global	dev_cli_disconnect
	.type	dev_cli_disconnect, %function
dev_cli_disconnect:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L432
	ldr	r1, .L432+4
	ldr	r0, [r3, #368]
	ldr	r3, .L432+8
	cmp	r0, #1
	ldreq	r1, [r1, #80]
	ldrne	r1, [r1, #84]
	mov	r2, #56
	mla	r3, r2, r1, r3
	stmfd	sp!, {r4, lr}
.LCFI19:
	ldr	r4, [r3, #24]
	eor	r1, r4, #1
	bl	SetDTR
	mov	r0, #100
	bl	vTaskDelay
	ldr	r3, .L432
	mov	r1, r4
	ldr	r0, [r3, #368]
	ldmfd	sp!, {r4, lr}
	b	SetDTR
.L433:
	.align	2
.L432:
	.word	comm_mngr
	.word	config_c
	.word	port_device_table
.LFE26:
	.size	dev_cli_disconnect, .-dev_cli_disconnect
	.section	.text.dev_exit_device_mode,"ax",%progbits
	.align	2
	.type	dev_exit_device_mode, %function
dev_exit_device_mode:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L435
	ldr	r2, .L435+4
	ldr	r3, [r3, #0]
	mov	r1, #56
	ldr	r3, [r3, #8]
	stmfd	sp!, {r4, lr}
.LCFI20:
	mla	r3, r1, r3, r2
	ldr	r4, .L435+8
	ldr	r1, [r3, #4]
	ldr	r0, [r4, #368]
	bl	SERIAL_set_baud_rate_on_A_or_B
	ldr	r3, .L435+12
	str	r3, [r4, #372]
	ldmfd	sp!, {r4, lr}
	b	dev_cli_disconnect
.L436:
	.align	2
.L435:
	.word	.LANCHOR1
	.word	port_device_table
	.word	comm_mngr
	.word	6000
.LFE25:
	.size	dev_exit_device_mode, .-dev_exit_device_mode
	.section	.text.dev_update_info,"ax",%progbits
	.align	2
	.global	dev_update_info
	.type	dev_update_info, %function
dev_update_info:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI21:
	add	r0, r0, #24
	mov	r4, r2
	mov	r2, #40
	bl	strlcpy
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	b	COMM_MNGR_device_exchange_results_to_key_process_task
.LFE29:
	.size	dev_update_info, .-dev_update_info
	.section	.text.dev_set_write_operation,"ax",%progbits
	.align	2
	.type	dev_set_write_operation, %function
dev_set_write_operation:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #87
	stmfd	sp!, {r4, lr}
.LCFI22:
	ldr	r1, .L447
	mov	r4, r0
	str	r3, [r0, #12]
	mov	r2, #8
	add	r0, r0, #16
	bl	strlcpy
	ldr	r2, .L447+4
	mov	r0, r4
	ldr	r1, .L447+8
	bl	dev_update_info
	mov	r3, #0
	str	r3, [r4, #108]
	str	r3, [r4, #104]
	ldr	r3, .L447+12
	mov	r2, #1000
	str	r2, [r3, #372]
	mov	r3, #1
	str	r3, [r4, #140]
	ldr	r3, [r4, #8]
	cmp	r3, #4
	bne	.L439
	mov	r0, r4
	bl	en_copy_active_values_to_static_values
	ldr	r3, [r4, #268]
	cmp	r3, #0
	beq	.L440
	ldr	r3, .L447+16
	str	r3, [r4, #156]
	bl	en_sizeof_dhcp_write_list
	b	.L446
.L440:
	ldr	r3, .L447+20
	str	r3, [r4, #156]
	bl	en_sizeof_static_write_list
	b	.L446
.L439:
	cmp	r3, #6
	bne	.L442
	ldr	r3, .L447+24
	str	r3, [r4, #156]
	bl	wen_sizeof_write_list
	b	.L446
.L442:
	cmp	r3, #8
	bne	.L443
	ldr	r3, .L447+28
	str	r3, [r4, #156]
	bl	PW_XE_sizeof_write_list
	b	.L446
.L443:
	cmp	r3, #9
	bne	.L444
	mov	r0, r4
	bl	WIBOX_copy_active_values_to_static_values
	ldr	r3, [r4, #268]
	cmp	r3, #0
	beq	.L445
	ldr	r3, .L447+32
	str	r3, [r4, #156]
	bl	WIBOX_sizeof_dhcp_write_list
	b	.L446
.L445:
	ldr	r3, .L447+36
	str	r3, [r4, #156]
	bl	WIBOX_sizeof_static_write_list
	b	.L446
.L444:
	ldr	r3, .L447+40
	str	r3, [r4, #156]
	bl	gr_sizeof_write_list
.L446:
	mov	r3, #0
	str	r0, [r4, #160]
	str	r3, [r4, #144]
	ldmfd	sp!, {r4, pc}
.L448:
	.align	2
.L447:
	.word	.LC147
	.word	36870
	.word	.LC148
	.word	comm_mngr
	.word	en_dhcp_write_list
	.word	en_static_write_list
	.word	wen_write_list
	.word	PW_XE_write_list
	.word	WIBOX_dhcp_write_list
	.word	WIBOX_static_write_list
	.word	gr_xml_write_list
.LFE31:
	.size	dev_set_write_operation, .-dev_set_write_operation
	.section	.text.dev_set_read_operation,"ax",%progbits
	.align	2
	.type	dev_set_read_operation, %function
dev_set_read_operation:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI23:
	mov	r2, #8
	mov	r4, r0
	str	r1, [r0, #12]
	ldr	r1, .L456
	add	r0, r0, #16
	bl	strlcpy
	ldr	r2, .L456+4
	mov	r0, r4
	ldr	r1, .L456+8
	bl	dev_update_info
	mov	r3, #0
	str	r3, [r4, #104]
	str	r3, [r4, #108]
	ldr	r3, .L456+12
	mov	r2, #1000
	str	r2, [r3, #372]
	ldr	r3, [r4, #8]
	cmp	r3, #4
	bne	.L450
	ldr	r3, .L456+16
	str	r3, [r4, #156]
	bl	en_sizeof_setup_read_list
	b	.L455
.L450:
	cmp	r3, #6
	bne	.L452
	ldr	r3, .L456+20
	str	r3, [r4, #156]
	bl	wen_sizeof_read_list
	b	.L455
.L452:
	cmp	r3, #8
	bne	.L453
	ldr	r3, .L456+24
	str	r3, [r4, #156]
	bl	PW_XE_sizeof_read_list
	b	.L455
.L453:
	cmp	r3, #9
	bne	.L454
	ldr	r3, .L456+28
	str	r3, [r4, #156]
	bl	WIBOX_sizeof_setup_read_list
	b	.L455
.L454:
	ldr	r3, .L456+32
	str	r3, [r4, #156]
	bl	gr_sizeof_read_list
.L455:
	mov	r3, #0
	str	r0, [r4, #160]
	str	r3, [r4, #144]
	ldmfd	sp!, {r4, pc}
.L457:
	.align	2
.L456:
	.word	.LC149
	.word	36867
	.word	.LC150
	.word	comm_mngr
	.word	en_setup_read_list
	.word	wen_read_list
	.word	PW_XE_read_list
	.word	WIBOX_setup_read_list
	.word	gr_xml_read_list
.LFE30:
	.size	dev_set_read_operation, .-dev_set_read_operation
	.section	.text.dev_handle_error,"ax",%progbits
	.align	2
	.type	dev_handle_error, %function
dev_handle_error:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI24:
	mov	r6, r3
	ldr	r3, [r0, #0]
	mov	r4, r0
	cmp	r3, #0
	mov	r5, r1
	bne	.L459
	mov	r3, #2
	str	r3, [r0, #0]
	str	r2, [r0, #4]
	mov	r1, r6
	mov	r2, #40
	add	r0, r0, #64
	bl	strlcpy
	ldr	r3, [r4, #12]
	mov	r0, r4
	cmp	r3, #87
	mov	r1, r6
	ldreq	r2, .L462
	ldrne	r2, .L462+4
	bl	dev_update_info
.L459:
	ldr	r0, .L462+8
	mov	r1, r5
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_Message_va
.L463:
	.align	2
.L462:
	.word	36870
	.word	36867
	.word	.LC151
.LFE10:
	.size	dev_handle_error, .-dev_handle_error
	.section	.text.dev_enter_device_mode,"ax",%progbits
	.align	2
	.type	dev_enter_device_mode, %function
dev_enter_device_mode:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #12]
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI25:
	cmp	r3, #87
	mov	r4, r0
	add	r0, r0, #24
	bne	.L465
	ldr	r1, .L474
	mov	r2, #40
	bl	strlcpy
	ldr	r0, .L474+4
	b	.L472
.L465:
	ldr	r1, .L474+8
	mov	r2, #40
	bl	strlcpy
	ldr	r0, .L474+12
.L472:
	ldr	r5, .L474+16
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	mov	r1, #9600
	ldr	r0, [r5, #368]
	bl	SERIAL_set_baud_rate_on_A_or_B
	ldr	r0, [r5, #368]
	bl	power_down_device
	mov	r6, #40
.L468:
	ldr	r0, [r5, #368]
	bl	GENERIC_GR_card_power_is_on
	cmp	r0, #0
	beq	.L467
	mov	r0, #50
	bl	vTaskDelay
	subs	r6, r6, #1
	bne	.L468
	b	.L473
.L467:
	mov	r0, #100
	bl	vTaskDelay
	ldr	r3, .L474+16
	ldr	r0, [r3, #368]
	bl	power_up_device
	mov	r3, #500
	str	r3, [r4, #136]
	ldmfd	sp!, {r4, r5, r6, pc}
.L473:
	mov	r0, r4
	mov	r1, #1
	mov	r2, #3
	ldr	r3, .L474+20
	bl	dev_handle_error
	b	.L467
.L475:
	.align	2
.L474:
	.word	.LC152
	.word	36870
	.word	.LC153
	.word	36867
	.word	comm_mngr
	.word	.LC154
.LFE24:
	.size	dev_enter_device_mode, .-dev_enter_device_mode
	.section	.text.dev_process_list_receive,"ax",%progbits
	.align	2
	.type	dev_process_list_receive, %function
dev_process_list_receive:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #12]
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI26:
	cmp	r3, #87
	ldr	r3, [r1, #0]
	ldreq	r5, [r0, #108]
	ldrne	r5, [r0, #104]
	cmp	r3, #4864
	mov	r4, r0
	mov	r6, r1
	mov	r7, r2
	bne	.L479
	cmp	r5, #2
	bne	.L480
	ldr	r3, .L502
	add	r1, r4, #16
	ldr	r2, [r3, #0]
	ldr	r3, [r0, #272]
	ldr	r0, .L502+4
	rsb	r2, r3, r2
	add	r2, r2, r2, asl #2
	bl	Alert_Message_va
.L480:
	ldr	r0, [r6, #16]
	cmp	r0, #0
	beq	.L493
	ldr	r2, [r6, #20]
	cmp	r2, #0
	beq	.L493
	ldr	r1, [r7, #0]
	bl	find_string_in_block
	cmp	r0, #0
	beq	.L482
	ldr	r3, [r6, #16]
	str	r3, [r4, #148]
	ldr	r3, [r6, #20]
	str	r3, [r4, #152]
	ldr	r3, [r7, #4]
	cmp	r3, #0
	beq	.L483
	mov	r0, r4
	blx	r3
.L483:
.LBB37:
	mov	r0, r4
	ldr	r1, [r7, #8]
	ldr	r2, [r7, #12]
	bl	dev_get_and_process_command
	adds	r4, r0, #0
	movne	r4, #1
	b	.L484
.L482:
.LBE37:
.LBB38:
	ldr	r0, [r7, #0]
	ldr	r1, .L502+8
	bl	strstr
	subs	r5, r0, #0
	beq	.L485
	ldr	r0, [r6, #16]
	ldr	r1, .L502+12
	ldr	r2, [r6, #20]
	bl	find_string_in_block
	subs	r5, r0, #0
	beq	.L485
.LBB39:
	mov	r0, r4
	mov	r1, #16
	ldr	r2, .L502+16
	bl	dev_get_and_process_command
	subs	r5, r0, #0
	beq	.L485
.LBE39:
	mov	r0, r4
	bl	dev_decrement_list_item_pointer
	mov	r5, #1
.L485:
	ldr	r0, [r7, #0]
	ldr	r1, .L502+20
	bl	strstr
	cmp	r0, #0
	beq	.L486
	ldr	r0, [r6, #16]
	ldr	r1, .L502+24
	ldr	r2, [r6, #20]
	bl	find_string_in_block
	cmp	r0, #0
	beq	.L486
.LBB40:
	mov	r0, r4
	mov	r1, #16
	ldr	r2, .L502+16
	bl	dev_get_and_process_command
	subs	r5, r0, #0
	beq	.L486
.LBE40:
	mov	r0, r4
	bl	dev_decrement_list_item_pointer
	mov	r5, #1
.L486:
	ldr	r0, [r7, #0]
	ldr	r1, .L502+28
	bl	strstr
	cmp	r0, #0
	beq	.L487
	ldr	r0, [r6, #16]
	ldr	r1, .L502+32
	ldr	r2, [r6, #20]
	bl	find_string_in_block
	cmp	r0, #0
	beq	.L487
.LBB41:
	mov	r0, r4
	mov	r1, #16
	ldr	r2, .L502+16
	bl	dev_get_and_process_command
	cmp	r0, #0
	beq	.L488
.LBE41:
	mov	r0, r4
	bl	dev_decrement_list_item_pointer
	b	.L499
.L487:
.LBE38:
	cmp	r5, #0
	bne	.L499
.L488:
	ldr	r0, .L502+36
	bl	Alert_Message
	mov	r4, #0
	b	.L484
.L499:
	mov	r4, #1
.L484:
	ldr	r0, [r6, #16]
	ldr	r1, .L502+40
	ldr	r2, .L502+44
	bl	mem_free_debug
	b	.L481
.L479:
	cmp	r3, #5120
	bne	.L489
	ldr	r3, [r0, #132]
	cmp	r3, #0
	beq	.L490
	cmp	r5, #2
	bne	.L501
	ldr	r3, .L502
	add	r1, r4, #16
	ldr	r2, [r3, #0]
	ldr	r3, [r0, #272]
	ldr	r0, .L502+48
	rsb	r2, r3, r2
	add	r2, r2, r2, asl #2
	bl	Alert_Message_va
	ldr	r6, .L502+52
	ldr	r3, [r4, #8]
	mov	r7, #56
	mla	r3, r7, r3, r6
	ldr	r5, .L502+56
	ldr	r3, [r3, #32]
	ldr	r0, [r5, #368]
	mov	r1, #0
	blx	r3
	mov	r0, #50
	bl	vTaskDelay
	ldr	r3, [r4, #8]
	ldr	r0, [r5, #368]
	mla	r6, r7, r3, r6
	mov	r1, #1
	ldr	r3, [r6, #32]
	blx	r3
	mov	r0, #400
	bl	vTaskDelay
	b	.L501
.L490:
	ldr	r3, [r2, #4]
	cmp	r3, #0
	beq	.L491
	blx	r3
.L491:
.LBB42:
	mov	r0, r4
	ldr	r1, [r7, #8]
	ldr	r2, [r7, #12]
	bl	dev_get_and_process_command
	adds	r4, r0, #0
	movne	r4, #1
	b	.L481
.L489:
.LBE42:
	mov	r1, #4
	mov	r2, #3
	ldr	r3, .L502+60
	bl	dev_handle_error
	cmp	r5, #2
	bne	.L501
	ldr	r3, .L502
	ldr	r0, .L502+64
	ldr	r2, [r3, #0]
	ldr	r3, [r4, #272]
	add	r1, r4, #16
	rsb	r2, r3, r2
	add	r2, r2, r2, asl #2
	bl	Alert_Message_va
	b	.L501
.L493:
	mov	r4, #1
	b	.L481
.L501:
	mov	r4, #0
.L481:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L503:
	.align	2
.L502:
	.word	my_tick_count
	.word	.LC155
	.word	.LC156
	.word	.LC157
	.word	.LC158
	.word	.LC159
	.word	.LC160
	.word	.LC161
	.word	.LC162
	.word	.LC163
	.word	.LC0
	.word	3703
	.word	.LC164
	.word	port_device_table
	.word	comm_mngr
	.word	.LC165
	.word	.LC166
.LFE40:
	.size	dev_process_list_receive, .-dev_process_list_receive
	.section	.text.dev_analyze_firmware_version,"ax",%progbits
	.align	2
	.global	dev_analyze_firmware_version
	.type	dev_analyze_firmware_version, %function
dev_analyze_firmware_version:
.LFB13:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI27:
	ldr	r6, .L523
	sub	sp, sp, #80
.LCFI28:
	mov	r3, #1
	add	r5, r0, #232
	str	r3, [r0, #112]
	mov	r4, r0
	mov	r1, r5
	mov	r2, #16
	add	r0, sp, #56
	bl	strlcpy
	mov	r2, #16
	ldr	r1, .L523+4
	add	r0, sp, #56
	bl	strlcat
	ldr	r1, .L523+4
	add	r0, sp, #56
	bl	strtok
	mov	r1, #86
	mov	r7, #48
	mov	r8, r0
	bl	strchr
	ldr	r3, [r4, #8]
	mul	r3, r7, r3
	ldr	sl, [r6, r3]
	cmp	r0, #0
	addne	r8, r8, #1
	mov	r0, r8
	bl	atoi
	cmp	sl, r0
	ldrne	r3, [r4, #8]
	mulne	r7, r3, r7
	ldrne	r6, [r6, r7]
	bne	.L521
	ldr	r1, .L523+4
	mov	r0, #0
	bl	strtok
	ldr	r3, [r4, #8]
	mla	r3, r7, r3, r6
	ldr	sl, [r3, #4]
	mov	r8, r0
	bl	atoi
	cmp	sl, r0
	ldrne	r3, [r4, #8]
	mlane	r6, r7, r3, r6
	ldrne	r6, [r6, #4]
	bne	.L521
.L508:
	ldr	r1, .L523+4
	mov	r0, #0
	bl	strtok
	ldr	r3, [r4, #8]
	mla	r3, r7, r3, r6
	ldr	sl, [r3, #8]
	mov	r8, r0
	bl	atoi
	cmp	sl, r0
	moveq	r3, #0
	beq	.L507
	ldr	r3, [r4, #8]
	mla	r6, r7, r3, r6
	ldr	r6, [r6, #8]
.L521:
	mov	r0, r8
	bl	atoi
	cmp	r6, r0
	movhi	r3, #0
	movls	r3, #1
	strhi	r3, [r4, #112]
.L507:
	ldr	r2, [r4, #112]
	eor	r3, r3, #1
	cmp	r2, #0
	moveq	r3, #0
	andne	r3, r3, #1
	cmp	r3, #0
	beq	.L509
	ldr	r2, [r4, #8]
	ldr	r3, .L523
	mov	r1, #48
	mla	r1, r2, r1, r3
	add	r6, sp, #72
	add	r1, r1, #12
	mov	r2, #6
	mov	r0, r6
	bl	strlcpy
	mov	r0, #0
	ldr	r1, .L523+4
	bl	strtok
	mov	r7, #0
	mov	r8, r0
	b	.L510
.L513:
	ldrb	r3, [r8, r7]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L511
	ldrb	r2, [r7, r6]	@ zero_extendqisi2
	cmp	r3, r2
	bhi	.L509
	cmp	r2, r3
	bls	.L512
	mov	r3, #0
.L511:
	str	r3, [r4, #112]
	b	.L509
.L512:
	add	r7, r7, #1
.L510:
	mov	r0, r6
	bl	strlen
	cmp	r7, r0
	bcc	.L513
.L509:
	ldr	r3, [r4, #112]
	cmp	r3, #0
	bne	.L504
	ldr	r3, [r4, #8]
	mov	r2, #48
	ldr	r1, .L523
	mul	r2, r3, r2
	add	r0, sp, #16
	add	r3, r1, r2
	ldr	r2, [r1, r2]
	mov	r1, #40
	str	r2, [sp, #0]
	ldr	r2, [r3, #4]
	ldr	r3, [r3, #8]
	str	r2, [sp, #4]
	str	r3, [sp, #8]
	add	r3, sp, #72
	str	r3, [sp, #12]
	ldr	r2, .L523+8
	mov	r3, r5
	bl	snprintf
	mov	r0, r4
	mov	r1, #5
	mov	r2, #1
	add	r3, sp, #16
	bl	dev_handle_error
.L504:
	add	sp, sp, #80
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L524:
	.align	2
.L523:
	.word	.LANCHOR0
	.word	.LC145
	.word	.LC167
.LFE13:
	.size	dev_analyze_firmware_version, .-dev_analyze_firmware_version
	.section	.text.dev_final_device_analysis,"ax",%progbits
	.align	2
	.global	dev_final_device_analysis
	.type	dev_final_device_analysis, %function
dev_final_device_analysis:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI29:
	mov	r4, r0
	ldr	r6, [r4, #140]
	mov	r5, #1
	cmp	r6, #0
	ldr	r0, [r0, #164]
	strne	r5, [r4, #128]
	ldmnefd	sp!, {r4, r5, r6, pc}
	ldr	r3, [r4, #8]
	str	r5, [r4, #124]
	cmp	r3, #7
	bne	.L528
	add	r0, r0, #140
	ldr	r1, .L530
	bl	strstr
	cmp	r0, #0
	streq	r5, [r4, #128]
	beq	.L528
	str	r6, [r4, #128]
	ldr	r0, .L530+4
	bl	Alert_Message
.L528:
	ldr	r3, [r4, #8]
	sub	r3, r3, #7
	cmp	r3, #1
	ldmhifd	sp!, {r4, r5, r6, pc}
	mov	r0, r4
	bl	dev_analyze_firmware_version
	ldr	r3, [r4, #112]
	cmp	r3, #0
	streq	r3, [r4, #124]
	ldmfd	sp!, {r4, r5, r6, pc}
.L531:
	.align	2
.L530:
	.word	.LC12
	.word	.LC168
.LFE19:
	.size	dev_final_device_analysis, .-dev_final_device_analysis
	.section	.text.dev_state_machine,"ax",%progbits
	.align	2
	.type	dev_state_machine, %function
dev_state_machine:
.LFB41:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #12]
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, sl, lr}
.LCFI30:
	cmp	r3, #87
	mov	r4, r0
	mov	r8, r1
	beq	.L561
	ldr	r3, [r0, #140]
	ldr	r6, .L577
	cmp	r3, #0
	ldr	r3, .L577+4
	ldr	r7, .L577+8
	movne	r6, r3
	sub	r3, r3, #1
	movne	r7, r3
	b	.L533
.L561:
	ldr	r6, .L577+4
	ldr	r7, .L577+12
.L533:
	ldr	r5, .L577+16
	ldr	r3, [r5, #372]
	cmp	r3, #4000
	beq	.L536
	ldr	sl, .L577+20
	cmp	r3, sl
	beq	.L537
	cmp	r3, #1000
	bne	.L566
	ldr	r3, [r4, #156]
	ldr	r1, [r3, #12]
.LBB43:
	ldr	r3, [r8, #0]
	cmp	r3, #4864
	bne	.L538
	ldr	r0, [r8, #16]
	cmp	r0, #0
	beq	.L538
	ldr	r3, [r8, #20]
	cmp	r3, #0
	beq	.L538
	bl	strstr
	cmp	r0, #0
	bne	.L539
.L538:
.LBE43:
	ldr	r3, [r4, #116]
	cmp	r3, #120
	bls	.L540
	b	.L576
.L568:
	ldr	r2, [r8, #16]
	mov	r0, r4
	str	r2, [r4, #148]
	ldr	r2, [r8, #20]
	str	r2, [r4, #152]
	blx	r3
.L542:
	mov	r3, #0
	str	r3, [r4, #116]
	ldr	r3, [r4, #156]
	mov	r0, r4
	ldr	r1, [r3, #8]
	ldr	r2, [r3, #12]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	dev_get_and_process_command
.L540:
	add	r3, r3, #1
	str	r3, [r4, #116]
	ldr	r3, [r4, #156]
	mov	r0, r4
	ldr	r1, [r3, #8]
	ldr	r2, [r3, #12]
	bl	dev_get_and_process_command
	ldr	r3, [r4, #8]
	ldr	r2, .L577+16
	cmp	r3, #4
	cmpne	r3, #9
	mvn	r1, #0
	str	r1, [sp, #0]
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [r2, #384]
	mov	r1, #2
	moveq	r2, #100
	movne	r2, #25
	bne	.L570
	b	.L573
.L576:
	ldr	r0, .L577+24
	bl	Alert_Message
	bl	dev_exit_device_mode
	mov	r0, r4
	ldr	r1, .L577+28
	b	.L571
.L536:
	mov	r0, r4
	mov	r1, #1
	bl	dev_increment_list_item_pointer
	mov	r0, r4
	mov	r1, r8
	ldr	r2, [r4, #156]
	bl	dev_process_list_receive
	cmp	r0, #0
	bne	.L554
	bl	dev_exit_device_mode
	ldr	r3, [r4, #160]
	ldr	r1, [r4, #104]
	sub	r3, r3, #1
	cmp	r1, r3
	add	r5, r4, #24
	bcs	.L546
	ldr	r0, .L577+32
	bl	Alert_Message_va
	mov	r0, r5
	ldr	r1, .L577+36
	mov	r2, #40
	bl	strlcpy
	ldr	r6, .L577
	b	.L547
.L546:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L548
	ldr	r2, [r4, #140]
	cmp	r2, #0
	beq	.L549
	str	r3, [r4, #140]
	ldr	r3, [r4, #128]
	mov	r0, r5
	cmp	r3, #0
	ldreq	r1, .L577+40
	beq	.L569
	ldr	r1, .L577+44
	mov	r2, #40
	bl	strlcpy
	ldr	r6, .L577+12
	b	.L547
.L549:
	ldr	r3, [r4, #124]
	mov	r0, r5
	cmp	r3, #0
	ldreq	r1, .L577+48
	beq	.L569
	ldr	r1, .L577+52
	mov	r2, #40
	bl	strlcpy
	ldr	r6, .L577+8
	b	.L547
.L569:
	mov	r2, #40
	bl	strlcpy
	b	.L547
.L548:
	mov	r0, r5
	add	r1, r4, #64
	mov	r2, #40
	bl	strlcpy
	ldr	r3, [r4, #4]
	sub	r3, r3, #1
	cmp	r3, #1
	movls	r6, r7
.L547:
	mov	r0, r4
	add	r1, r4, #24
.L571:
	mov	r2, r6
.L572:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	dev_update_info
.L537:
	mov	r0, r4
	mov	r1, #1
	bl	dev_increment_list_item_pointer
	mov	r0, r4
	mov	r1, r8
	ldr	r2, [r4, #156]
	bl	dev_process_list_receive
	cmp	r0, #0
	bne	.L554
	bl	dev_exit_device_mode
	ldr	r3, [r4, #160]
	ldr	r1, [r4, #108]
	sub	r3, r3, #1
	cmp	r1, r3
	add	r5, r4, #24
	bcs	.L555
	ldr	r0, .L577+56
	bl	Alert_Message_va
	mov	r2, #40
	mov	r0, r5
	ldr	r1, .L577+60
	bl	strlcpy
	ldr	r2, .L577+4
	b	.L556
.L555:
	ldr	r3, [r4, #0]
	mov	r0, r5
	cmp	r3, #0
	bne	.L557
	mov	r2, #40
	ldr	r1, .L577+64
	bl	strlcpy
	ldr	r2, .L577+12
	b	.L556
.L557:
	mov	r2, #40
	add	r1, r4, #64
	bl	strlcpy
	ldr	r2, [r4, #4]
	sub	r2, r2, #1
	cmp	r2, #1
	movhi	r2, r6
	movls	r2, r7
.L556:
	mov	r0, r4
	add	r1, r4, #24
	b	.L572
.L554:
	ldr	r3, [r4, #132]
	mvn	r2, #0
	cmp	r3, #0
	str	r2, [sp, #0]
	mov	r1, #2
	ldr	r0, [r5, #384]
	moveq	r2, #20
	beq	.L570
	ldr	r2, .L577+68
.L573:
	mov	r3, #0
.L570:
	bl	xTimerGenericCommand
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L566:
	mov	r3, #73
	str	r3, [r4, #12]
	add	r0, r4, #16
	ldr	r1, .L577+72
	mov	r2, #8
	bl	strlcpy
	ldr	r0, .L577+76
	mov	r1, #4
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_Message_va
.L539:
	ldr	r3, [r4, #12]
	mov	r0, r4
	cmp	r3, #87
	movne	sl, #4000
	str	sl, [r5, #372]
	mov	r1, #1
	bl	dev_increment_list_item_pointer
	ldr	r3, [r4, #156]
	ldr	r3, [r3, #4]
	cmp	r3, #0
	bne	.L568
	b	.L542
.L578:
	.align	2
.L577:
	.word	36866
	.word	36869
	.word	36865
	.word	36868
	.word	comm_mngr
	.word	5000
	.word	.LC169
	.word	.LC170
	.word	.LC171
	.word	.LC172
	.word	.LC174
	.word	.LC173
	.word	.LC176
	.word	.LC175
	.word	.LC177
	.word	.LC178
	.word	.LC129
	.word	18000
	.word	.LC179
	.word	.LC180
.LFE41:
	.size	dev_state_machine, .-dev_state_machine
	.section	.text.dev_device_write_progress,"ax",%progbits
	.align	2
	.global	dev_device_write_progress
	.type	dev_device_write_progress, %function
dev_device_write_progress:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, .L580
	ldr	r2, .L580+4
	b	dev_update_info
.L581:
	.align	2
.L580:
	.word	.LC181
	.word	36870
.LFE21:
	.size	dev_device_write_progress, .-dev_device_write_progress
	.section	.text.dev_device_read_progress,"ax",%progbits
	.align	2
	.global	dev_device_read_progress
	.type	dev_device_read_progress, %function
dev_device_read_progress:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, .L583
	ldr	r2, .L583+4
	b	dev_update_info
.L584:
	.align	2
.L583:
	.word	.LC182
	.word	36867
.LFE20:
	.size	dev_device_read_progress, .-dev_device_read_progress
	.section	.text.dev_analyze_xcr_dump_device,"ax",%progbits
	.align	2
	.global	dev_analyze_xcr_dump_device
	.type	dev_analyze_xcr_dump_device, %function
dev_analyze_xcr_dump_device:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI31:
	ldr	r1, .L590
	mov	r4, r0
	ldr	r2, .L590+4
	bl	dev_update_info
	ldr	r0, [r4, #148]
	ldr	r1, .L590+8
	ldr	r2, [r4, #152]
	bl	find_string_in_block
	cmp	r0, #0
	beq	.L586
	mov	r3, #32
	str	r3, [sp, #0]
	add	r3, r4, #200
	str	r3, [sp, #4]
	ldr	r3, .L590+12
	ldr	r1, .L590+16
	str	r3, [sp, #8]
	ldr	r2, .L590+20
	mov	r3, #10
	bl	dev_extract_delimited_text_from_buffer
	b	.L587
.L586:
	ldr	r0, [r4, #148]
	ldr	r1, .L590+24
	ldr	r2, [r4, #152]
	bl	find_string_in_block
	cmp	r0, #0
	beq	.L587
	mov	r0, r4
	mov	r1, #6
	mov	r2, #3
	ldr	r3, .L590+28
	bl	dev_handle_error
.L587:
	ldr	r5, [r4, #8]
	cmp	r5, #7
	bne	.L588
	ldr	r0, [r4, #148]
	ldr	r1, .L590+32
	ldr	r2, [r4, #152]
	bl	find_string_in_block
	cmp	r0, #0
	beq	.L588
	mov	r3, #18
	str	r3, [sp, #0]
	add	r3, r4, #247
	str	r3, [sp, #4]
	ldr	r3, .L590+36
	ldr	r1, .L590+16
	str	r3, [sp, #8]
	ldr	r2, .L590+40
	mov	r3, r5
	bl	dev_extract_delimited_text_from_buffer
.L588:
	ldr	r0, [r4, #148]
	ldr	r1, .L590+44
	ldr	r2, [r4, #152]
	bl	find_string_in_block
	cmp	r0, #0
	beq	.L585
	mov	r3, #15
	str	r3, [sp, #0]
	ldr	r3, .L590+48
	add	r4, r4, #232
	str	r3, [sp, #8]
	ldr	r1, .L590+16
	ldr	r2, .L590+40
	mov	r3, #7
	str	r4, [sp, #4]
	bl	dev_extract_delimited_text_from_buffer
.L585:
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L591:
	.align	2
.L590:
	.word	.LC183
	.word	36867
	.word	.LC184
	.word	.LC187
	.word	.LC185
	.word	.LC186
	.word	.LC188
	.word	.LC189
	.word	.LC190
	.word	.LC192
	.word	.LC191
	.word	.LC193
	.word	.LC194
.LFE11:
	.size	dev_analyze_xcr_dump_device, .-dev_analyze_xcr_dump_device
	.section	.text.DEVICE_initialize_device_exchange,"ax",%progbits
	.align	2
	.global	DEVICE_initialize_device_exchange
	.type	DEVICE_initialize_device_exchange, %function
DEVICE_initialize_device_exchange:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L613
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI32:
	ldr	r4, [r3, #0]
.LBB50:
	ldr	r5, [r4, #8]
	cmp	r5, #4
	bne	.L593
	ldr	r0, [r4, #184]
	cmp	r0, #0
	beq	.L594
	ldr	r1, .L613+4
	mov	r2, r5
	bl	strncmp
	cmp	r0, #0
	bne	.L594
	ldr	r0, [r4, #188]
	cmp	r0, #0
	ldrne	r1, .L613+4
	movne	r2, r5
	bne	.L612
	b	.L594
.L593:
	cmp	r5, #9
	bne	.L596
	ldr	r0, [r4, #192]
	cmp	r0, #0
	beq	.L594
	ldr	r1, .L613+4
	mov	r2, #4
	bl	strncmp
	cmp	r0, #0
	bne	.L594
	ldr	r0, [r4, #196]
	cmp	r0, #0
	beq	.L594
	ldr	r1, .L613+4
	mov	r2, #4
.L612:
	bl	strncmp
	cmp	r0, #0
	b	.L607
.L596:
	ldr	r3, [r4, #12]
	cmp	r3, #73
	cmpne	r3, #82
	beq	.L605
	cmp	r3, #77
	beq	.L605
	cmp	r3, #87
.L607:
	beq	.L605
.L594:
.LBE50:
.LBB51:
	ldr	r2, .L613+8
.LBE51:
	ldr	r3, .L613
.LBB52:
	ldr	r2, [r2, #368]
.LBE52:
	ldr	r4, [r3, #0]
.LBB53:
	cmp	r2, #1
	ldr	r2, .L613+12
	mov	r3, #0
	ldreq	r2, [r2, #80]
	ldrne	r2, [r2, #84]
	str	r3, [r4, #0]
	str	r3, [r4, #164]
	str	r3, [r4, #168]
	str	r3, [r4, #176]
	str	r3, [r4, #172]
	str	r3, [r4, #184]
	str	r3, [r4, #188]
	str	r3, [r4, #192]
	str	r3, [r4, #196]
	str	r3, [r4, #140]
	mov	r3, #1
	str	r3, [r4, #268]
	str	r2, [r4, #8]
	ldr	r1, .L613+16
	mov	r2, #32
	add	r0, r4, #200
	bl	strlcpy
	ldr	r1, .L613+16
	mov	r2, #15
	add	r0, r4, #232
	bl	strlcpy
	ldr	r1, .L613+16
	mov	r2, #18
	add	r0, r4, #247
	bl	strlcpy
	ldr	r3, [r4, #8]
	ldr	r2, .L613+20
	mov	r1, #48
	mla	r3, r1, r3, r2
	mov	r0, r4
	ldr	r3, [r3, #40]
	blx	r3
.L605:
.LBE53:
	ldr	r3, .L613
.LBB54:
	ldr	r6, .L613+8
.LBE54:
	ldr	r4, [r3, #0]
.LBB55:
	mov	r5, #0
	mov	r0, r4
	ldr	r1, .L613+16
	str	r5, [r0], #16
	mov	r2, #8
	bl	strlcpy
	ldr	r3, [r6, #364]
	mov	r0, r4
	cmp	r3, #4608
	ldr	r1, .L613+16
	bne	.L599
	ldr	r2, .L613+24
	bl	dev_update_info
	ldr	r3, [r4, #8]
	ldr	r2, .L613+20
	mov	r1, #48
	mla	r3, r1, r3, r2
	mov	r0, r4
	ldr	r3, [r3, #36]
	blx	r3
	b	.L600
.L599:
	ldr	r2, .L613+28
	bl	dev_update_info
	ldr	r2, [r6, #364]
	ldr	r3, .L613+32
	mov	r1, #48
	cmp	r2, r3
	ldr	r3, .L613+20
	ldr	r2, [r4, #8]
	moveq	r0, r4
	mlaeq	r3, r1, r2, r3
	mlane	r3, r1, r2, r3
	ldreq	r3, [r3, #32]
	ldrne	r3, [r3, #32]
	moveq	r1, #77
	strne	r5, [r4, #124]
	movne	r0, r4
	movne	r1, #82
	blx	r3
	mov	r3, #0
	str	r3, [r4, #112]
.L600:
	mov	r3, #0
	str	r3, [r4, #128]
	str	r3, [r4, #136]
	str	r3, [r4, #116]
	str	r3, [r4, #120]
.LBE55:
	ldr	r4, .L613
	ldr	r0, [r4, #0]
	bl	dev_enter_device_mode
	ldr	r0, [r4, #0]
	ldr	r5, [r0, #0]
	cmp	r5, #0
	bne	.L592
	ldr	r3, [r0, #156]
	ldr	r1, [r3, #8]
	ldr	r2, [r3, #12]
	bl	dev_get_and_process_command
	ldr	r2, .L613+36
	ldr	r3, [r4, #0]
	ldr	r2, [r2, #0]
	mvn	r1, #0
	str	r2, [r3, #272]
	ldr	r3, [r3, #8]
	ldr	r2, .L613+8
	cmp	r3, #4
	cmpne	r3, #9
	str	r1, [sp, #0]
	movne	r3, #0
	moveq	r3, #1
	ldr	r0, [r2, #384]
	mov	r1, #2
	moveq	r2, #100
	moveq	r3, r5
	movne	r2, #25
	bl	xTimerGenericCommand
.L592:
	ldmfd	sp!, {r3, r4, r5, r6, pc}
.L614:
	.align	2
.L613:
	.word	.LANCHOR1
	.word	.LC195
	.word	comm_mngr
	.word	config_c
	.word	.LC129
	.word	.LANCHOR0
	.word	36870
	.word	36867
	.word	4368
	.word	my_tick_count
.LFE42:
	.size	DEVICE_initialize_device_exchange, .-DEVICE_initialize_device_exchange
	.section	.text.DEVICE_exchange_processing,"ax",%progbits
	.align	2
	.global	DEVICE_exchange_processing
	.type	DEVICE_exchange_processing, %function
DEVICE_exchange_processing:
.LFB43:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI33:
	ldr	r4, .L617
	mvn	r3, #0
	mov	r2, #0
	str	r3, [sp, #0]
	mov	r5, r0
	mov	r3, r2
	ldr	r0, [r4, #384]
	mov	r1, #1
	bl	xTimerGenericCommand
	ldr	r3, [r4, #0]
	cmp	r3, #5
	bne	.L615
	ldr	r3, .L617+4
	mov	r1, r5
	ldr	r0, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	dev_state_machine
.L615:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, pc}
.L618:
	.align	2
.L617:
	.word	comm_mngr
	.word	.LANCHOR1
.LFE43:
	.size	DEVICE_exchange_processing, .-DEVICE_exchange_processing
	.global	dev_details
	.global	dev_state
	.global	dev_state_struct
	.section	.bss.dev_state_struct,"aw",%nobits
	.align	2
	.type	dev_state_struct, %object
	.size	dev_state_struct, 276
dev_state_struct:
	.space	276
	.section	.rodata.device_handler,"a",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	device_handler, %object
	.size	device_handler, 528
device_handler:
	.word	0
	.word	0
	.word	0
	.ascii	"\000"
	.space	5
	.space	2
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.ascii	"\000"
	.space	5
	.space	2
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.ascii	"\000"
	.space	5
	.space	2
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.ascii	"\000"
	.space	5
	.space	2
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	6
	.word	1
	.word	0
	.ascii	"3\000"
	.space	4
	.space	2
	.word	dev_get_command_text
	.word	dev_enter_device_mode
	.word	dev_exit_device_mode
	.word	dev_set_read_operation
	.word	dev_set_write_operation
	.word	en_initialize_detail_struct
	.word	dev_state_machine
	.word	0
	.word	0
	.word	0
	.ascii	"\000"
	.space	5
	.space	2
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	7
	.word	9
	.word	0
	.ascii	"0R19\000"
	.space	1
	.space	2
	.word	dev_get_command_text
	.word	dev_enter_device_mode
	.word	dev_exit_device_mode
	.word	dev_set_read_operation
	.word	dev_set_write_operation
	.word	wen_initialize_detail_struct
	.word	dev_state_machine
	.word	7
	.word	9
	.word	0
	.ascii	"0R11\000"
	.space	1
	.space	2
	.word	dev_get_command_text
	.word	dev_enter_device_mode
	.word	dev_exit_device_mode
	.word	dev_set_read_operation
	.word	dev_set_write_operation
	.word	gr_initialize_detail_struct
	.word	dev_state_machine
	.word	7
	.word	10
	.word	0
	.ascii	"0T8\000"
	.space	2
	.space	2
	.word	dev_get_command_text
	.word	dev_enter_device_mode
	.word	dev_exit_device_mode
	.word	dev_set_read_operation
	.word	dev_set_write_operation
	.word	PW_XE_initialize_detail_struct
	.word	dev_state_machine
	.word	6
	.word	7
	.word	0
	.ascii	"0\000"
	.space	4
	.space	2
	.word	dev_get_command_text
	.word	dev_enter_device_mode
	.word	dev_exit_device_mode
	.word	dev_set_read_operation
	.word	dev_set_write_operation
	.word	WIBOX_initialize_detail_struct
	.word	dev_state_machine
	.space	48
	.section	.bss.common_PROGRAMMING_reboot_time,"aw",%nobits
	.set	.LANCHOR2,. + 0
	.type	common_PROGRAMMING_reboot_time, %object
	.size	common_PROGRAMMING_reboot_time, 6
common_PROGRAMMING_reboot_time:
	.space	6
	.section	.data.dev_state,"aw",%progbits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	dev_state, %object
	.size	dev_state, 4
dev_state:
	.word	dev_state_struct
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_common.c\000"
.LC1:
	.ascii	"\"\000"
.LC2:
	.ascii	"\\\000"
.LC3:
	.ascii	"Inadequate string space\000"
.LC4:
	.ascii	"mode \000"
.LC5:
	.ascii	"accept mode disable\015\000"
.LC6:
	.ascii	"accept\015\000"
.LC7:
	.ascii	"address 64.73.242.99\015\000"
.LC8:
	.ascii	"tcp keep alive \000"
.LC9:
	.ascii	"1200000\015\000"
.LC10:
	.ascii	"60000\015\000"
.LC11:
	.ascii	"antenna diversity disable\015\000"
.LC12:
	.ascii	"apn 10429.mcs\015\000"
.LC13:
	.ascii	"baud rate 115200\015\000"
.LC14:
	.ascii	"connect mode always\015\000"
.LC15:
	.ascii	"cellular\015\000"
.LC16:
	.ascii	"xyz\000"
.LC17:
	.ascii	"clock\015\000"
.LC18:
	.ascii	"clock timezone PST8PDT\015\000"
.LC19:
	.ascii	"configure\015\000"
.LC20:
	.ascii	"connect\015\000"
.LC21:
	.ascii	"\015\000"
.LC22:
	.ascii	"modem control enable\015\000"
.LC23:
	.ascii	"dhcp \000"
.LC24:
	.ascii	"enable\000"
.LC25:
	.ascii	"disable\000"
.LC26:
	.ascii	"diagnostics\015\000"
.LC27:
	.ascii	"disconnect\015\000"
.LC28:
	.ascii	"encryption ccmp \000"
.LC29:
	.ascii	"encryption tkip \000"
.LC30:
	.ascii	"encryption wep \000"
.LC31:
	.ascii	"eap-ttls option \000"
.LC32:
	.ascii	"echo commands disable\015\000"
.LC33:
	.ascii	"edit default_infrastructure_profile\015\000"
.LC34:
	.ascii	"enable\015\000"
.LC35:
	.ascii	"Not Set\000"
.LC36:
	.ascii	"credentials \000"
.LC37:
	.ascii	"no credentials\000"
.LC38:
	.ascii	"validate certificate \000"
.LC39:
	.ascii	"exit\015\000"
.LC40:
	.ascii	"8\015\000"
.LC41:
	.ascii	"flow control hardware\015\000"
.LC42:
	.ascii	"default gateway \000"
.LC43:
	.ascii	"Y\000"
.LC44:
	.ascii	"N\000"
.LC45:
	.ascii	"00\015\000"
.LC46:
	.ascii	"%d\015\000"
.LC47:
	.ascii	"1\015\000"
.LC48:
	.ascii	"7\015\000"
.LC49:
	.ascii	"5\015\000"
.LC50:
	.ascii	"6\015\000"
.LC51:
	.ascii	"0\015\000"
.LC52:
	.ascii	"host 1\015\000"
.LC53:
	.ascii	"hostname \000"
.LC54:
	.ascii	"%s%c%c%c%c%c%c%c%c%c%c%c%c\000"
.LC55:
	.ascii	"ieee 802.1x \000"
.LC56:
	.ascii	"if eth0\015\000"
.LC57:
	.ascii	"if wlan0\015\000"
.LC58:
	.ascii	"if wwan0\015\000"
.LC59:
	.ascii	"ip address \000"
.LC60:
	.ascii	"/%d\015\000"
.LC61:
	.ascii	"WEP\000"
.LC62:
	.ascii	"no key\000"
.LC63:
	.ascii	"WPA\000"
.LC64:
	.ascii	"tx key index \000"
.LC65:
	.ascii	"key size \000"
.LC66:
	.ascii	" bits\000"
.LC67:
	.ascii	"key \000"
.LC68:
	.ascii	"Passphrase\000"
.LC69:
	.ascii	"text \000"
.LC70:
	.ascii	"key type \000"
.LC71:
	.ascii	"max length 1000\015\000"
.LC72:
	.ascii	"line 1\015\000"
.LC73:
	.ascii	"line 2\015\000"
.LC74:
	.ascii	"link\015\000"
.LC75:
	.ascii	"log\015\000"
.LC76:
	.ascii	"login\015\000"
.LC77:
	.ascii	"modem\015\000"
.LC78:
	.ascii	"QU\015\000"
.LC79:
	.ascii	"GM\015\000"
.LC80:
	.ascii	"NC\015\000"
.LC81:
	.ascii	"no\015\000"
.LC82:
	.ascii	"output filesystem\015\000"
.LC83:
	.ascii	"configured and ignored\000"
.LC84:
	.ascii	"passphrase \000"
.LC85:
	.ascii	"no passphrase\000"
.LC86:
	.ascii	"peap option \000"
.LC87:
	.ascii	"port 16001\015\000"
.LC88:
	.ascii	"priority 2\015\000"
.LC89:
	.ascii	"password \000"
.LC90:
	.ascii	"no password\000"
.LC91:
	.ascii	"reload\015\000"
.LC92:
	.ascii	"9\015\000"
.LC93:
	.ascii	"security\015\000"
.LC94:
	.ascii	"show\015\000"
.LC95:
	.ascii	"show status\015\000"
.LC96:
	.ascii	"network name \000"
.LC97:
	.ascii	"no network name\000"
.LC98:
	.ascii	"x\000"
.LC99:
	.ascii	"!\000"
.LC100:
	.ascii	"suite \000"
.LC101:
	.ascii	"synchronization method network\015\000"
.LC102:
	.ascii	"synchronization method sntp\015\000"
.LC103:
	.ascii	"tunnel 1\015\000"
.LC104:
	.ascii	"username \000"
.LC105:
	.ascii	"no username\000"
.LC106:
	.ascii	"verbose response disable\015\000"
.LC107:
	.ascii	"wep\015\000"
.LC108:
	.ascii	"authentication \000"
.LC109:
	.ascii	"/\000"
.LC110:
	.ascii	"key 1\000"
.LC111:
	.ascii	"key 2\000"
.LC112:
	.ascii	"key 3\000"
.LC113:
	.ascii	"key 4\000"
.LC114:
	.ascii	"wlan profiles\015\000"
.LC115:
	.ascii	"PSK\000"
.LC116:
	.ascii	"authentication psk\000"
.LC117:
	.ascii	"authentication 802.1x\000"
.LC118:
	.ascii	"wpax\015\000"
.LC119:
	.ascii	"write\015\000"
.LC120:
	.ascii	"xcr dump device\015\000"
.LC121:
	.ascii	"xcr dump interface:wlan0\015\000"
.LC122:
	.ascii	"xcr dump \"wlan profile:default_infrastructure_prof"
	.ascii	"ile\"\015\000"
.LC123:
	.ascii	"xsr dump interface:wlan0\015\000"
.LC124:
	.ascii	"xml\015\000"
.LC125:
	.ascii	"yes\015\000"
.LC126:
	.ascii	"xcr dump interface:eth0\015\000"
.LC127:
	.ascii	"4\015\000"
.LC128:
	.ascii	"Unknown Device Command\000"
.LC129:
	.ascii	"\000"
.LC130:
	.ascii	"NA\000"
.LC131:
	.ascii	"DEV2\000"
.LC132:
	.ascii	"\012\000"
.LC133:
	.ascii	"DEV3\000"
.LC134:
	.ascii	"Extract Failure: Missing anchor text - %s\000"
.LC135:
	.ascii	"Packet D\000"
.LC136:
	.ascii	"IP A\000"
.LC137:
	.ascii	"IP ADD\000"
.LC138:
	.ascii	"0.0.0.0\000"
.LC139:
	.ascii	"DEV1\000"
.LC140:
	.ascii	"Interface           : eth0\000"
.LC141:
	.ascii	"MAC Ad\000"
.LC142:
	.ascii	"ETH0 MAC\000"
.LC143:
	.ascii	"Interface           : wlan0\000"
.LC144:
	.ascii	"WLAN0 MAC\000"
.LC145:
	.ascii	".\000"
.LC146:
	.ascii	"DEV5\000"
.LC147:
	.ascii	"WRITE\000"
.LC148:
	.ascii	"Beginning write...\000"
.LC149:
	.ascii	"READ\000"
.LC150:
	.ascii	"Beginning read...\000"
.LC151:
	.ascii	"ERR %d\000"
.LC152:
	.ascii	"Preparing to program...\000"
.LC153:
	.ascii	"Preparing to read...\000"
.LC154:
	.ascii	"Device failed to power-off\000"
.LC155:
	.ascii	"First %s Command Delay: %d ms\000"
.LC156:
	.ascii	"password (N)\000"
.LC157:
	.ascii	"Set DNS Server IP addr \000"
.LC158:
	.ascii	"?\000"
.LC159:
	.ascii	"Your choice ?\000"
.LC160:
	.ascii	"Enable DHCP FQDN\000"
.LC161:
	.ascii	"Auto increment source port\000"
.LC162:
	.ascii	"Show IP addr after\000"
.LC163:
	.ascii	"DEV13\000"
.LC164:
	.ascii	"First %s Command Timeout: %d ms\000"
.LC165:
	.ascii	"Unknown event\000"
.LC166:
	.ascii	"First %s Unknown Delay: %d ms\000"
.LC167:
	.ascii	"FW Ver: %s. Needed: %d.%d.%d.%s\000"
.LC168:
	.ascii	"DEV6\000"
.LC169:
	.ascii	"DEV14\000"
.LC170:
	.ascii	"Device not responding\000"
.LC171:
	.ascii	"DEV18 (%d)\000"
.LC172:
	.ascii	"Read ended early\000"
.LC173:
	.ascii	"Write completed\000"
.LC174:
	.ascii	"Write completed. Settings Invalid.\000"
.LC175:
	.ascii	"Read completed\000"
.LC176:
	.ascii	"Read completed. Settings Invalid.\000"
.LC177:
	.ascii	"DEV19 (%d)\000"
.LC178:
	.ascii	"Write ended early\000"
.LC179:
	.ascii	"IDLE\000"
.LC180:
	.ascii	"Size of dev_state: %d\000"
.LC181:
	.ascii	"Writing device settings...\000"
.LC182:
	.ascii	"Reading device settings...\000"
.LC183:
	.ascii	"Reading device info...\000"
.LC184:
	.ascii	"long name\000"
.LC185:
	.ascii	"<\000"
.LC186:
	.ascii	"Lantronix\000"
.LC187:
	.ascii	"MODEL\000"
.LC188:
	.ascii	"Error executing command\000"
.LC189:
	.ascii	"Device read failed. Retry.\000"
.LC190:
	.ascii	"l number\000"
.LC191:
	.ascii	"<value>\000"
.LC192:
	.ascii	"SER NUM\000"
.LC193:
	.ascii	"e version\000"
.LC194:
	.ascii	"FW VER\000"
.LC195:
	.ascii	"PVS\000"
	.section	.bss.dev_details,"aw",%nobits
	.align	2
	.type	dev_details, %object
	.size	dev_details, 152
dev_details:
	.space	152
	.section	.bss.common_PROGRAMMING_seconds_since_reboot,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	common_PROGRAMMING_seconds_since_reboot, %object
	.size	common_PROGRAMMING_seconds_since_reboot, 4
common_PROGRAMMING_seconds_since_reboot:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI0-.LFB28
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.byte	0x4
	.4byte	.LCFI1-.LFB48
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI2-.LFB23
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x174
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI5-.LFB4
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI7-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI9-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI10-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI11-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI12-.LFB15
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI13-.LFB16
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI14-.LFB12
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI15-.LFB17
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI17-.LFB18
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI19-.LFB26
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI20-.LFB25
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI21-.LFB29
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI22-.LFB31
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI23-.LFB30
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI24-.LFB10
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI25-.LFB24
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI26-.LFB40
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI27-.LFB13
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xe
	.uleb128 0x6c
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI29-.LFB19
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI30-.LFB41
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI31-.LFB11
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI32-.LFB42
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI33-.LFB43
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE72:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_common.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x35f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF44
	.byte	0x1
	.4byte	.LASF45
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0xd1d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0xd47
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0xce5
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0xd8a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x46f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0xb24
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x1
	.2byte	0xb5a
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB36
	.4byte	.LFE36
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	0x2a
	.4byte	.LFB37
	.4byte	.LFE37
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	0x45
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x4ca
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.byte	0xa5
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.byte	0xab
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.byte	0xb1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.byte	0xcb
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.byte	0xda
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x10b
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x134
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x14a
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x153
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST7
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x166
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x2ca
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x2df
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST9
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x30c
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST10
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x202
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST11
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x355
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST12
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x37b
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST13
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.2byte	0xafe
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST14
	.uleb128 0x3
	.4byte	.LASF25
	.byte	0x1
	.2byte	0xae2
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST15
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.2byte	0xb8d
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST16
	.uleb128 0x3
	.4byte	.LASF27
	.byte	0x1
	.2byte	0xbd7
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST17
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x1
	.2byte	0xb9b
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST18
	.uleb128 0x3
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x1af
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST19
	.uleb128 0x3
	.4byte	.LASF30
	.byte	0x1
	.2byte	0xa8d
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST20
	.uleb128 0x2
	.4byte	.LASF31
	.byte	0x1
	.2byte	0xda3
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF32
	.byte	0x1
	.2byte	0xdf3
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST21
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x245
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST22
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x3ae
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST23
	.uleb128 0x3
	.4byte	.LASF35
	.byte	0x1
	.2byte	0xebe
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST24
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x456
	.4byte	.LFB21
	.4byte	.LFE21
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x44f
	.4byte	.LFB20
	.4byte	.LFE20
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x1ce
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST25
	.uleb128 0x2
	.4byte	.LASF39
	.byte	0x1
	.2byte	0xcab
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF40
	.byte	0x1
	.2byte	0xc7d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF41
	.byte	0x1
	.2byte	0xc40
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x10e0
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST26
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x1116
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST27
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB28
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB48
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE48
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB23
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI3
	.4byte	.LFE23
	.2byte	0x3
	.byte	0x7d
	.sleb128 372
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI6
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI8
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB8
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB14
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB15
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB16
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB12
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB17
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI16
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB18
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI18
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB26
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB25
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB29
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB31
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB30
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB10
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB40
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB13
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI28
	.4byte	.LFE13
	.2byte	0x3
	.byte	0x7d
	.sleb128 108
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB19
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB41
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB11
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB42
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB43
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x13c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF39:
	.ascii	"dev_verify_state_struct\000"
.LASF40:
	.ascii	"dev_initialize_state_struct\000"
.LASF21:
	.ascii	"dev_extract_ip_octets\000"
.LASF44:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF10:
	.ascii	"e_SHARED_get_easyGUI_string_at_index\000"
.LASF34:
	.ascii	"dev_final_device_analysis\000"
.LASF31:
	.ascii	"dev_handle_alternate_response\000"
.LASF5:
	.ascii	"dev_setup_for_termination_string_hunt\000"
.LASF1:
	.ascii	"dev_decrement_list_item_pointer\000"
.LASF26:
	.ascii	"dev_update_info\000"
.LASF6:
	.ascii	"dev_get_command_text\000"
.LASF33:
	.ascii	"dev_analyze_firmware_version\000"
.LASF18:
	.ascii	"dev_extract_text_from_buffer\000"
.LASF17:
	.ascii	"dev_strip_crlf_characters\000"
.LASF11:
	.ascii	"e_SHARED_get_index_of_easyGUI_string\000"
.LASF41:
	.ascii	"dev_set_state_struct_for_new_device_exchange\000"
.LASF32:
	.ascii	"dev_process_list_receive\000"
.LASF22:
	.ascii	"dev_count_true_mask_bits\000"
.LASF19:
	.ascii	"dev_extract_delimited_text_from_buffer\000"
.LASF14:
	.ascii	"e_SHARED_get_info_text_from_programming_struct\000"
.LASF43:
	.ascii	"DEVICE_exchange_processing\000"
.LASF7:
	.ascii	"FDTO_e_SHARED_show_obtain_ip_automatically_dropdown"
	.ascii	"\000"
.LASF13:
	.ascii	"e_SHARED_network_connect_delay_is_over\000"
.LASF37:
	.ascii	"dev_device_read_progress\000"
.LASF36:
	.ascii	"dev_device_write_progress\000"
.LASF8:
	.ascii	"FDTO_e_SHARED_show_subnet_dropdown\000"
.LASF45:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_common.c\000"
.LASF42:
	.ascii	"DEVICE_initialize_device_exchange\000"
.LASF12:
	.ascii	"e_SHARED_start_device_communication\000"
.LASF35:
	.ascii	"dev_state_machine\000"
.LASF16:
	.ascii	"e_SHARED_index_keycode_for_gui\000"
.LASF3:
	.ascii	"dev_process_list_send\000"
.LASF29:
	.ascii	"dev_handle_error\000"
.LASF4:
	.ascii	"dev_command_escape_char_handler\000"
.LASF15:
	.ascii	"e_SHARED_string_validation\000"
.LASF9:
	.ascii	"e_SHARED_show_keyboard\000"
.LASF28:
	.ascii	"dev_set_read_operation\000"
.LASF0:
	.ascii	"dev_increment_list_item_pointer\000"
.LASF38:
	.ascii	"dev_analyze_xcr_dump_device\000"
.LASF30:
	.ascii	"dev_enter_device_mode\000"
.LASF27:
	.ascii	"dev_set_write_operation\000"
.LASF2:
	.ascii	"dev_start_prompt_received\000"
.LASF25:
	.ascii	"dev_exit_device_mode\000"
.LASF23:
	.ascii	"dev_cli_disconnect\000"
.LASF20:
	.ascii	"dev_analyze_enable_show_ip\000"
.LASF24:
	.ascii	"dev_get_and_process_command\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
