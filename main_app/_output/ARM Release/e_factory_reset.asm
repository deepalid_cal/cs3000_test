	.file	"e_factory_reset.c"
	.text
.Ltext0:
	.section	.text.FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart,"ax",%progbits
	.align	2
	.global	FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart
	.type	FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart, %function
FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L3
	stmfd	sp!, {r4, lr}
.LCFI0:
	mov	r1, #10
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	vTaskPrioritySet
	mov	r0, #1
	bl	FLASH_STORAGE_delete_all_files
	mov	r0, #34
	bl	NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits
	sub	r3, r4, #28
	cmp	r3, #1
	bhi	.L2
	bl	NETWORK_CONFIG_brute_force_set_network_ID_to_zero
.L2:
	bl	save_file_configuration_network
	mov	r0, #200
	bl	vTaskDelay
	mov	r0, #1
	bl	init_battery_backed_weather_preserves
	mov	r0, #1
	bl	init_battery_backed_station_preserves
	mov	r0, #1
	bl	init_battery_backed_system_preserves
	mov	r0, #1
	bl	init_battery_backed_poc_preserves
	mov	r0, #1
	bl	init_battery_backed_foal_irri
	mov	r0, #1
	bl	init_battery_backed_chain_members
	mov	r0, #1
	bl	init_battery_backed_general_use
	mov	r0, #1
	bl	init_battery_backed_lights_preserves
	mov	r0, #1
	bl	init_battery_backed_foal_lights
	bl	ALERTS_restart_all_piles
	ldr	r3, .L3+4
	mov	r2, #1
	mov	r0, r4
	strb	r2, [r3, #130]
	ldmfd	sp!, {r4, lr}
	b	SYSTEM_application_requested_restart
.L4:
	.align	2
.L3:
	.word	flash_storage_1_task_handle
	.word	weather_preserves
.LFE0:
	.size	FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart, .-FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart
	.section	.text.FDTO_FACTORY_RESET_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_FACTORY_RESET_draw_screen
	.type	FDTO_FACTORY_RESET_draw_screen, %function
FDTO_FACTORY_RESET_draw_screen:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r1, #1
	str	lr, [sp, #-4]!
.LCFI1:
	mov	r0, #20
	mov	r2, r1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.LFE1:
	.size	FDTO_FACTORY_RESET_draw_screen, .-FDTO_FACTORY_RESET_draw_screen
	.section	.text.FACTORY_RESET_process_screen,"ax",%progbits
	.align	2
	.global	FACTORY_RESET_process_screen
	.type	FACTORY_RESET_process_screen, %function
FACTORY_RESET_process_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	str	lr, [sp, #-4]!
.LCFI2:
	mov	r3, r0
	mov	r2, r1
	beq	.L9
	bhi	.L12
	cmp	r0, #1
	bne	.L7
	b	.L15
.L12:
	cmp	r0, #3
	beq	.L10
	cmp	r0, #67
	bne	.L7
	b	.L16
.L9:
	bl	good_key_beep
	ldr	r3, .L17
	ldrsh	r3, [r3, #0]
	cmp	r3, #1
	bne	.L13
	ldr	r0, .L17+4
	bl	DIALOG_draw_ok_dialog
	mov	r0, #27
	ldr	lr, [sp], #4
	b	FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart
.L13:
	ldr	lr, [sp], #4
	b	ALERTS_restart_all_piles
.L15:
	ldr	lr, [sp], #4
	b	CURSOR_Up
.L10:
	ldr	r3, .L17
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L14
	mov	r0, #1
	ldr	lr, [sp], #4
	b	CURSOR_Down
.L14:
	ldr	lr, [sp], #4
	b	bad_key_beep
.L16:
	ldr	r3, .L17+8
	mov	r2, #11
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	ldr	lr, [sp], #4
	b	TECH_SUPPORT_draw_dialog
.L7:
	mov	r0, r3
	mov	r1, r2
	ldr	lr, [sp], #4
	b	KEY_process_global_keys
.L18:
	.align	2
.L17:
	.word	GuiLib_ActiveCursorFieldNo
	.word	618
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	FACTORY_RESET_process_screen, .-FACTORY_RESET_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_factory_reset.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x2f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xb0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xb7
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"FACTORY_RESET_process_screen\000"
.LASF0:
	.ascii	"FACTORY_RESET_init_all_battery_backed_delete_all_fi"
	.ascii	"les_and_restart\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_factory_reset.c\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"FDTO_FACTORY_RESET_draw_screen\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
