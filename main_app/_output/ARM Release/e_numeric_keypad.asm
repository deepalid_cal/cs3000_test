	.file	"e_numeric_keypad.c"
	.text
.Ltext0:
	.section	.text.show_keypad,"ax",%progbits
	.align	2
	.type	show_keypad, %function
show_keypad:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	mov	r3, #2
	sub	sp, sp, #36
.LCFI1:
	str	r3, [sp, #0]
	ldr	r3, .L2
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L3:
	.align	2
.L2:
	.word	FDTO_show_keypad
.LFE3:
	.size	show_keypad, .-show_keypad
	.section	.text.FDTO_show_keypad,"ax",%progbits
	.align	2
	.type	FDTO_show_keypad, %function
FDTO_show_keypad:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L6
	cmp	r0, #1
	moveq	r2, #98
	str	lr, [sp, #-4]!
.LCFI2:
	mov	r0, #616
	streqh	r2, [r3, #0]	@ movhi
	mov	r2, #1
	ldrsh	r1, [r3, #0]
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L7:
	.align	2
.L6:
	.word	GuiLib_ActiveCursorFieldNo
.LFE2:
	.size	FDTO_show_keypad, .-FDTO_show_keypad
	.section	.text.process_OK_button,"ax",%progbits
	.align	2
	.type	process_OK_button, %function
process_OK_button:
.LFB9:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
.LBB17:
	ldr	r3, .L20
.LBE17:
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI3:
.LBB18:
	ldr	r3, [r3, #0]
.LBE18:
	sub	sp, sp, #36
.LCFI4:
.LBB19:
	cmp	r3, #0
.LBE19:
	mov	r5, r0
.LBB20:
	bne	.L9
	ldr	r4, .L20+4
	ldr	r0, .L20+8
	ldr	r6, [r4, #0]
	bl	atoi
	ldr	r3, [r4, #0]
	ldr	r2, .L20+12
	ldr	r2, [r2, #0]
	str	r0, [r6, #0]
	ldr	r1, [r3, #0]
	cmp	r1, r2
	bcs	.L10
	b	.L19
.L9:
	cmp	r3, #1
	bne	.L12
	ldr	r4, .L20+16
	ldr	r0, .L20+8
	ldr	r6, [r4, #0]
	bl	atoi
	ldr	r3, [r4, #0]
	ldr	r2, .L20+20
	ldr	r2, [r2, #0]
	str	r0, [r6, #0]
	ldr	r1, [r3, #0]
	cmp	r1, r2
	bge	.L10
.L19:
	str	r2, [r3, #0]
	b	.L11
.L12:
	cmp	r3, #2
	bne	.L13
	ldr	r4, .L20+24
	ldr	r0, .L20+8
	ldr	r6, [r4, #0]
	bl	atof
	ldr	r3, [r4, #0]
	ldr	r2, .L20+28
	fmdrr	d6, r0, r1
	fcvtsd	s15, d6
	fsts	s15, [r6, #0]
	flds	s15, [r2, #0]
	flds	s14, [r3, #0]
	fcmpes	s14, s15
	fmstat
	fstsmi	s15, [r3, #0]
	bmi	.L11
	b	.L10
.L13:
	ldr	r4, .L20+32
	ldr	r0, .L20+8
	ldr	r6, [r4, #0]
	bl	atof
	ldr	r3, [r4, #0]
	ldr	r2, .L20+36
	fldd	d7, [r2, #0]
	stmia	r6, {r0-r1}
	fldd	d6, [r3, #0]
	fcmped	d6, d7
	fmstat
	fstdmi	d7, [r3, #0]
	bmi	.L11
.L10:
	bl	good_key_beep
.L16:
.LBE20:
.LBB21:
	ldr	r3, .L20+40
	mov	r0, sp
	ldr	r2, [r3, #0]
	ldr	r3, .L20+44
	str	r5, [sp, #20]
	strh	r2, [r3, #0]	@ movhi
	mov	r3, #2
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #24]
	bl	Display_Post_Command
.LBE21:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, pc}
.L11:
.LBB22:
	bl	bad_key_beep
	b	.L16
.L21:
	.align	2
.L20:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	GuiVar_NumericKeypadValue
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LANCHOR4
	.word	.LANCHOR5
	.word	.LANCHOR6
	.word	.LANCHOR7
	.word	.LANCHOR8
	.word	.LANCHOR9
	.word	GuiLib_ActiveCursorFieldNo
.LBE22:
.LFE9:
	.size	process_OK_button, .-process_OK_button
	.section	.text.validate_upper_range.constprop.1,"ax",%progbits
	.align	2
	.type	validate_upper_range.constprop.1, %function
validate_upper_range.constprop.1:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L46
	stmfd	sp!, {r0, r1, lr}
.LCFI5:
	ldr	r3, [r3, #0]
	ldr	r0, .L46+4
	cmp	r3, #0
	bne	.L23
	bl	atoi
	ldr	r3, .L46+8
	ldr	r3, [r3, #0]
	cmp	r0, r3
	bls	.L24
	b	.L42
.L23:
	cmp	r3, #1
	bne	.L26
	bl	atoi
	ldr	r3, .L46+12
	ldr	r3, [r3, #0]
	cmp	r0, r3
	ldr	r0, .L46+4
	bgt	.L43
.L27:
	ldr	r1, .L46+16
	bl	strstr
	cmp	r0, #0
	beq	.L24
	ldr	r0, .L46+4
	bl	atoi
	ldr	r3, .L46+20
	ldr	r3, [r3, #0]
	cmp	r0, r3
	bge	.L24
.L42:
	ldr	r0, .L46+4
.L43:
	mov	r1, #11
	ldr	r2, .L46+24
	bl	snprintf
	b	.L25
.L26:
	cmp	r3, #2
	bne	.L28
	bl	atof
	ldr	r3, .L46+28
	flds	s14, [r3, #0]
	fcvtds	d7, s14
	fmdrr	d6, r0, r1
	fcmped	d6, d7
	fmstat
	bgt	.L41
.L39:
	ldr	r0, .L46+4
	ldr	r1, .L46+16
	bl	strstr
	cmp	r0, #0
	beq	.L24
	ldr	r0, .L46+4
	bl	atof
	ldr	r3, .L46+32
	flds	s14, [r3, #0]
	fcvtds	d7, s14
	b	.L45
.L28:
	bl	atof
	ldr	r3, .L46+36
	fldd	d7, [r3, #0]
	fmdrr	d6, r0, r1
	fcmped	d6, d7
	fmstat
	bgt	.L41
.L40:
	ldr	r0, .L46+4
	ldr	r1, .L46+16
	bl	strstr
	cmp	r0, #0
	beq	.L24
	ldr	r0, .L46+4
	bl	atof
	ldr	r3, .L46+40
	fldd	d7, [r3, #0]
.L45:
	fmdrr	d6, r0, r1
	fcmped	d6, d7
	fmstat
	bpl	.L24
.L41:
	fstd	d7, [sp, #0]
	ldr	r3, .L46+44
	mov	r1, #11
	ldr	r2, .L46+48
	ldr	r3, [r3, #0]
	ldr	r0, .L46+4
	bl	snprintf
	b	.L25
.L24:
	add	sp, sp, #8
	ldr	lr, [sp], #4
	b	good_key_beep
.L25:
	add	sp, sp, #8
	ldr	lr, [sp], #4
	b	bad_key_beep
.L47:
	.align	2
.L46:
	.word	.LANCHOR0
	.word	GuiVar_NumericKeypadValue
	.word	.LANCHOR10
	.word	.LANCHOR11
	.word	.LC1
	.word	.LANCHOR4
	.word	.LC0
	.word	.LANCHOR12
	.word	.LANCHOR6
	.word	.LANCHOR14
	.word	.LANCHOR8
	.word	.LANCHOR13
	.word	.LC2
.LFE18:
	.size	validate_upper_range.constprop.1, .-validate_upper_range.constprop.1
	.section	.text.process_key_press,"ax",%progbits
	.align	2
	.type	process_key_press, %function
process_key_press:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #99
	stmfd	sp!, {r4, lr}
.LCFI6:
	mov	r4, r0
	bne	.L49
	ldr	r0, .L65
	ldr	r1, .L65+4
	mov	r2, #11
	bl	strncmp
	cmp	r0, #0
	beq	.L59
.L50:
	bl	good_key_beep
	ldr	r0, .L65
	bl	strlen
	cmp	r0, #1
	ldrls	r1, .L65+4
	ldr	r0, .L65
	movls	r2, #11
	bls	.L64
	bl	strlen
	mov	r2, r0
	ldr	r0, .L65
	mov	r1, r0
.L64:
	bl	strlcpy
	b	.L51
.L49:
	cmp	r0, #59
	bne	.L53
	ldr	r4, .L65
	ldr	r1, .L65+8
	mov	r0, r4
	bl	strstr
	cmp	r0, #0
	mov	r0, r4
	bne	.L54
	bl	strlen
	mov	r1, r4
	add	r2, r0, #1
	add	r0, r4, #1
	bl	memmove
	mov	r3, #45
	strb	r3, [r4, #0]
	b	.L62
.L54:
	add	r1, r4, #1
	mov	r2, #11
	bl	memmove
	b	.L62
.L53:
	cmp	r4, #61
	ldr	r0, .L65
	ldr	r1, .L65+12
	bne	.L56
	bl	strstr
	cmp	r0, #0
	bne	.L59
	bl	good_key_beep
	ldr	r1, .L65+12
	mov	r2, #11
	ldr	r0, .L65
	bl	strlcat
	b	.L51
.L56:
	bl	strstr
	cmp	r0, #0
	beq	.L58
.LBB23:
	ldr	r1, .L65+12
	ldr	r0, .L65
	bl	strstr
	bl	strlen
	ldr	r3, .L65+16
	ldr	r3, [r3, #0]
	sub	r0, r0, #1
	cmp	r0, r3
	beq	.L59
.L58:
	sub	r4, r4, #50
	cmp	r4, #10
	ldrls	r3, .L65+20
.LBE23:
	ldr	r1, .L65+4
	mov	r2, #11
	ldr	r0, .L65
.LBB24:
	ldrlsb	r4, [r3, r4]	@ zero_extendqisi2
.LBE24:
	movhi	r4, #0
	bl	strncmp
	mov	r1, #11
	ldr	r2, .L65+24
	mov	r3, r4
	cmp	r0, #0
	ldr	r0, .L65
	bne	.L61
	bl	snprintf
	b	.L62
.L61:
	bl	sp_strlcat
.L62:
	bl	validate_upper_range.constprop.1
.L51:
	mov	r0, #0
	ldmfd	sp!, {r4, lr}
	b	show_keypad
.L59:
	bl	bad_key_beep
	b	.L51
.L66:
	.align	2
.L65:
	.word	GuiVar_NumericKeypadValue
	.word	.LC3
	.word	.LC1
	.word	.LC4
	.word	.LANCHOR13
	.word	.LANCHOR15
	.word	.LC5
.LFE10:
	.size	process_key_press, .-process_key_press
	.section	.text.NUMERIC_KEYPAD_draw_uint32_keypad,"ax",%progbits
	.align	2
	.global	NUMERIC_KEYPAD_draw_uint32_keypad
	.type	NUMERIC_KEYPAD_draw_uint32_keypad, %function
NUMERIC_KEYPAD_draw_uint32_keypad:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, r0
.LBB25:
	ldr	r0, .L68
.LBE25:
	str	lr, [sp, #-4]!
.LCFI7:
.LBB26:
	ldrsh	ip, [r0, #0]
	ldr	r0, .L68+4
	str	ip, [r0, #0]
	ldr	ip, .L68+8
	mov	r0, #0
	str	r0, [ip, #0]
	ldr	ip, .L68+12
	str	r0, [ip, #0]
	ldr	ip, .L68+16
	str	r0, [ip, #0]
	ldr	ip, .L68+20
	str	r0, [ip, #0]
.LBE26:
	ldr	r0, .L68+24
	str	r3, [r0, #0]
	ldr	r0, .L68+28
	ldr	r3, [r3, #0]
	str	r1, [r0, #0]
	ldr	r1, .L68+32
	ldr	r0, .L68+36
	str	r2, [r1, #0]
	mov	r1, #11
	ldr	r2, .L68+40
	bl	snprintf
	mov	r0, #1
	ldr	lr, [sp], #4
	b	show_keypad
.L69:
	.align	2
.L68:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR9
	.word	.LANCHOR0
	.word	.LANCHOR13
	.word	GuiVar_NumericKeypadShowPlusMinus
	.word	GuiVar_NumericKeypadShowDecimalPoint
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR10
	.word	GuiVar_NumericKeypadValue
	.word	.LC6
.LFE12:
	.size	NUMERIC_KEYPAD_draw_uint32_keypad, .-NUMERIC_KEYPAD_draw_uint32_keypad
	.section	.text.NUMERIC_KEYPAD_draw_int32_keypad,"ax",%progbits
	.align	2
	.global	NUMERIC_KEYPAD_draw_int32_keypad
	.type	NUMERIC_KEYPAD_draw_int32_keypad, %function
NUMERIC_KEYPAD_draw_int32_keypad:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, r0
.LBB27:
	ldr	r0, .L71
.LBE27:
	stmfd	sp!, {r4, lr}
.LCFI8:
.LBB28:
	ldrsh	ip, [r0, #0]
	ldr	r0, .L71+4
	mov	r4, #1
	str	ip, [r0, #0]
	ldr	r0, .L71+8
	ldr	ip, .L71+12
	str	r4, [r0, #0]
	mov	r0, #0
	str	r0, [ip, #0]
	ldr	ip, .L71+16
	str	r0, [ip, #0]
	ldr	r0, .L71+20
.LBE28:
	mov	ip, r1, lsr #31
.LBB29:
	str	ip, [r0, #0]
.LBE29:
	ldr	r0, .L71+24
	str	r3, [r0, #0]
	ldr	r0, .L71+28
	ldr	r3, [r3, #0]
	str	r1, [r0, #0]
	ldr	r1, .L71+32
	ldr	r0, .L71+36
	str	r2, [r1, #0]
	mov	r1, #11
	ldr	r2, .L71+40
	bl	snprintf
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	b	show_keypad
.L72:
	.align	2
.L71:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR9
	.word	.LANCHOR0
	.word	.LANCHOR13
	.word	GuiVar_NumericKeypadShowPlusMinus
	.word	GuiVar_NumericKeypadShowDecimalPoint
	.word	.LANCHOR3
	.word	.LANCHOR4
	.word	.LANCHOR11
	.word	GuiVar_NumericKeypadValue
	.word	.LC0
.LFE13:
	.size	NUMERIC_KEYPAD_draw_int32_keypad, .-NUMERIC_KEYPAD_draw_int32_keypad
	.section	.text.NUMERIC_KEYPAD_draw_float_keypad,"ax",%progbits
	.align	2
	.global	NUMERIC_KEYPAD_draw_float_keypad
	.type	NUMERIC_KEYPAD_draw_float_keypad, %function
NUMERIC_KEYPAD_draw_float_keypad:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
.LBB30:
	ldr	ip, .L74
.LBE30:
	fmsr	s15, r1
	stmfd	sp!, {r0, r1, lr}
.LCFI9:
	flds	s14, [r0, #0]
.LBB31:
	ldrsh	lr, [ip, #0]
.LBE31:
	fcmpezs	s15
.LBB32:
	ldr	ip, .L74+4
	str	lr, [ip, #0]
	ldr	ip, .L74+8
.LBE32:
	fmstat
.LBB33:
	mov	lr, #2
	str	lr, [ip, #0]
	ldr	ip, .L74+12
.LBE33:
	movpl	lr, #0
	movmi	lr, #1
.LBB34:
	str	r3, [ip, #0]
	ldr	ip, .L74+16
.LBE34:
	adds	r1, r3, #0
.LBB35:
	str	lr, [ip, #0]
	ldr	ip, .L74+20
.LBE35:
	movne	r1, #1
.LBB36:
	str	r1, [ip, #0]
.LBE36:
	ldr	r1, .L74+24
	str	r0, [r1, #0]
	ldr	r1, .L74+28
	ldr	r0, .L74+32
	fsts	s15, [r1, #0]
	fcvtds	d7, s14
	ldr	r1, .L74+36
	str	r2, [r1, #0]	@ float
	mov	r1, #11
	ldr	r2, .L74+40
	fstd	d7, [sp, #0]
	bl	snprintf
	mov	r0, #1
	add	sp, sp, #8
	ldr	lr, [sp], #4
	b	show_keypad
.L75:
	.align	2
.L74:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR9
	.word	.LANCHOR0
	.word	.LANCHOR13
	.word	GuiVar_NumericKeypadShowPlusMinus
	.word	GuiVar_NumericKeypadShowDecimalPoint
	.word	.LANCHOR5
	.word	.LANCHOR6
	.word	GuiVar_NumericKeypadValue
	.word	.LANCHOR12
	.word	.LC7
.LFE14:
	.size	NUMERIC_KEYPAD_draw_float_keypad, .-NUMERIC_KEYPAD_draw_float_keypad
	.section	.text.NUMERIC_KEYPAD_draw_double_keypad,"ax",%progbits
	.align	2
	.global	NUMERIC_KEYPAD_draw_double_keypad
	.type	NUMERIC_KEYPAD_draw_double_keypad, %function
NUMERIC_KEYPAD_draw_double_keypad:
.LFB15:
	@ args = 12, pretend = 4, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	fmdrr	d7, r1, r2
.LBB37:
	ldr	r2, .L77
.LBE37:
	sub	sp, sp, #4
.LCFI10:
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI11:
	fcmpezd	d7
.LBB38:
	ldrsh	r1, [r2, #0]
	ldr	r2, .L77+4
.LBE38:
	fmstat
	str	r3, [sp, #20]
.LBB39:
	str	r1, [r2, #0]
	ldr	r2, .L77+8
	mov	r1, #3
	str	r1, [r2, #0]
.LBE39:
	ldr	r3, [sp, #28]
.LBB40:
	ldr	r2, .L77+12
.LBE40:
	movpl	r1, #0
	movmi	r1, #1
.LBB41:
	str	r3, [r2, #0]
	ldr	r2, .L77+16
.LBE41:
	add	r5, sp, #20
	ldmia	r5, {r4-r5}
.LBB42:
	str	r1, [r2, #0]
	ldr	r2, .L77+20
.LBE42:
	adds	r1, r3, #0
	movne	r1, #1
.LBB43:
	str	r1, [r2, #0]
.LBE43:
	ldr	r2, .L77+24
	str	r0, [r2, #0]
	ldr	r2, .L77+28
	ldmia	r0, {r0-r1}
	fstd	d7, [r2, #0]
	ldr	r2, .L77+32
	stmia	r2, {r4-r5}
	ldr	r2, .L77+36
	stmia	sp, {r0-r1}
	ldr	r0, .L77+40
	mov	r1, #11
	bl	snprintf
	mov	r0, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	add	sp, sp, #4
	b	show_keypad
.L78:
	.align	2
.L77:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR9
	.word	.LANCHOR0
	.word	.LANCHOR13
	.word	GuiVar_NumericKeypadShowPlusMinus
	.word	GuiVar_NumericKeypadShowDecimalPoint
	.word	.LANCHOR7
	.word	.LANCHOR8
	.word	.LANCHOR14
	.word	.LC7
	.word	GuiVar_NumericKeypadValue
.LFE15:
	.size	NUMERIC_KEYPAD_draw_double_keypad, .-NUMERIC_KEYPAD_draw_double_keypad
	.section	.text.NUMERIC_KEYPAD_process_key,"ax",%progbits
	.align	2
	.global	NUMERIC_KEYPAD_process_key
	.type	NUMERIC_KEYPAD_process_key, %function
NUMERIC_KEYPAD_process_key:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #3
	mov	r3, r0
	beq	.L84
	bhi	.L88
	cmp	r0, #1
	ldr	r1, .L119
	beq	.L82
	bhi	.L83
	b	.L117
.L88:
	cmp	r0, #67
	beq	.L86
	bhi	.L89
	cmp	r0, #4
	bne	.L80
	b	.L118
.L89:
	cmp	r0, #80
	beq	.L87
	cmp	r0, #84
	bne	.L80
	b	.L87
.L83:
	ldrsh	r0, [r1, #0]
	cmp	r0, #98
	bne	.L112
	b	.L86
.L118:
.LBB51:
	ldr	r3, .L119
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	sub	r2, r0, #6422528
	cmp	r2, #65536
	mov	r3, r0, lsr #16
	bls	.L98
.L91:
	sub	r3, r3, #53
	mov	r3, r3, asl #16
	cmp	r3, #524288
	bhi	.L110
	mov	r0, r0, asr #16
	sub	r0, r0, #3
.L113:
	mov	r1, #1
.L114:
.LBE51:
.LBB52:
	b	CURSOR_Select
.L117:
.LBE52:
.LBB53:
	ldrh	r3, [r1, #0]
	sub	r2, r3, #50
	mov	r2, r2, asl #16
	mov	r0, r3, asl #16
	cmp	r2, #327680
	mov	r0, r0, asr #16
	addls	r0, r0, #3
	bls	.L113
	cmp	r0, #56
	bne	.L94
	ldr	r3, .L119+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L98
	b	.L115
.L94:
	cmp	r0, #57
	beq	.L98
.L96:
	cmp	r0, #58
	bne	.L97
	ldr	r3, .L119+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L116
.L98:
	mov	r0, #60
	b	.L113
.L97:
	sub	r3, r3, #59
	mov	r3, r3, asl #16
	cmp	r3, #131072
	movls	r0, #98
	bhi	.L110
	b	.L113
.L82:
.LBE53:
.LBB54:
	ldrsh	r2, [r1, #0]
	ldrh	r0, [r1, #0]
	cmp	r2, #99
	moveq	r0, #98
	moveq	r1, r3
	beq	.L114
	sub	r3, r0, #51
	mov	r3, r3, asl #16
	cmp	r2, #54
	cmpne	r3, #65536
	bls	.L101
	cmp	r2, #55
	beq	.L101
	cmp	r2, #57
	beq	.L101
	cmp	r2, #58
	beq	.L101
	cmp	r2, #61
	bne	.L102
.L101:
	mov	r0, #1
.LBE54:
.LBB55:
	b	CURSOR_Up
.L102:
	cmp	r2, #60
	bne	.L110
	ldr	r3, .L119+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L110
.L115:
	mov	r0, #59
	b	.L113
.L84:
.LBE55:
.LBB56:
	ldr	r3, .L119
	ldrh	r2, [r3, #0]
	ldrsh	r3, [r3, #0]
	cmp	r3, #98
	moveq	r0, #99
	beq	.L113
	sub	r2, r2, #50
	mov	r2, r2, asl #16
	cmp	r3, #53
	cmpne	r2, #65536
	bls	.L106
	cmp	r3, #54
	beq	.L106
	cmp	r3, #56
	beq	.L106
	cmp	r3, #57
	beq	.L106
	cmp	r3, #59
	bne	.L107
.L106:
	mov	r0, #1
.LBE56:
.LBB57:
	b	CURSOR_Down
.L107:
	cmp	r3, #60
	bne	.L110
	ldr	r3, .L119+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L110
.L116:
	mov	r0, #61
	b	.L113
.L87:
.LBE57:
	ldr	r3, .L119+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L110
	mov	r0, #59
.L112:
	b	process_key_press
.L110:
	b	bad_key_beep
.L86:
	mov	r0, r2
	b	process_OK_button
.L80:
	mov	r0, r3
	b	KEY_process_global_keys
.L120:
	.align	2
.L119:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_NumericKeypadShowPlusMinus
	.word	GuiVar_NumericKeypadShowDecimalPoint
.LFE16:
	.size	NUMERIC_KEYPAD_process_key, .-NUMERIC_KEYPAD_process_key
	.section	.bss.min_double_value,"aw",%nobits
	.align	2
	.set	.LANCHOR8,. + 0
	.type	min_double_value, %object
	.size	min_double_value, 8
min_double_value:
	.space	8
	.section	.bss.ptr_to_int32_value,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	ptr_to_int32_value, %object
	.size	ptr_to_int32_value, 4
ptr_to_int32_value:
	.space	4
	.section	.bss.g_NUMERIC_KEYPAD_num_decimals_to_display,"aw",%nobits
	.align	2
	.set	.LANCHOR13,. + 0
	.type	g_NUMERIC_KEYPAD_num_decimals_to_display, %object
	.size	g_NUMERIC_KEYPAD_num_decimals_to_display, 4
g_NUMERIC_KEYPAD_num_decimals_to_display:
	.space	4
	.section	.bss.g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed,"aw",%nobits
	.align	2
	.set	.LANCHOR9,. + 0
	.type	g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed, %object
	.size	g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed, 4
g_NUMERIC_KEYPAD_cursor_position_when_keypad_displayed:
	.space	4
	.section	.bss.min_int32_value,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	min_int32_value, %object
	.size	min_int32_value, 4
min_int32_value:
	.space	4
	.section	.bss.min_uint32_value,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	min_uint32_value, %object
	.size	min_uint32_value, 4
min_uint32_value:
	.space	4
	.section	.bss.ptr_to_double_value,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	ptr_to_double_value, %object
	.size	ptr_to_double_value, 4
ptr_to_double_value:
	.space	4
	.section	.bss.g_NUMERIC_KEYPAD_type,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_NUMERIC_KEYPAD_type, %object
	.size	g_NUMERIC_KEYPAD_type, 4
g_NUMERIC_KEYPAD_type:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%d\000"
.LC1:
	.ascii	"-\000"
.LC2:
	.ascii	"%*.f\000"
.LC3:
	.ascii	"0\000"
.LC4:
	.ascii	".\000"
.LC5:
	.ascii	"%c\000"
.LC6:
	.ascii	"%u\000"
.LC7:
	.ascii	"%.*f\000"
	.section	.bss.ptr_to_uint32_value,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	ptr_to_uint32_value, %object
	.size	ptr_to_uint32_value, 4
ptr_to_uint32_value:
	.space	4
	.section	.rodata.CSWTCH.45,"a",%progbits
	.set	.LANCHOR15,. + 0
	.type	CSWTCH.45, %object
	.size	CSWTCH.45, 11
CSWTCH.45:
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	0
	.byte	48
	.section	.bss.ptr_to_float_value,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	ptr_to_float_value, %object
	.size	ptr_to_float_value, 4
ptr_to_float_value:
	.space	4
	.section	.bss.max_float_value,"aw",%nobits
	.align	2
	.set	.LANCHOR12,. + 0
	.type	max_float_value, %object
	.size	max_float_value, 4
max_float_value:
	.space	4
	.section	.bss.max_int32_value,"aw",%nobits
	.align	2
	.set	.LANCHOR11,. + 0
	.type	max_int32_value, %object
	.size	max_int32_value, 4
max_int32_value:
	.space	4
	.section	.bss.min_float_value,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	min_float_value, %object
	.size	min_float_value, 4
min_float_value:
	.space	4
	.section	.bss.max_uint32_value,"aw",%nobits
	.align	2
	.set	.LANCHOR10,. + 0
	.type	max_uint32_value, %object
	.size	max_uint32_value, 4
max_uint32_value:
	.space	4
	.section	.bss.max_double_value,"aw",%nobits
	.align	2
	.set	.LANCHOR14,. + 0
	.type	max_double_value, %object
	.size	max_double_value, 8
max_double_value:
	.space	8
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI3-.LFB9
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI5-.LFB18
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI6-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI7-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI8-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI9-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI10-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.align	2
.LEFDE18:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_numeric_keypad.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x12d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF17
	.byte	0x1
	.4byte	.LASF18
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0xd3
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x152
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x277
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x165
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x74
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x13b
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x123
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1e9
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST2
	.uleb128 0x5
	.4byte	0x44
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x1f3
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x298
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x2bf
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x2e3
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x30b
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST8
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x178
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x1a5
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x1c7
	.byte	0x1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x321
	.4byte	.LFB16
	.4byte	.LFE16
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB9
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI4
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB18
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB10
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB12
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB13
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB14
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB15
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI11
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x64
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF14:
	.ascii	"process_left_arrow\000"
.LASF16:
	.ascii	"NUMERIC_KEYPAD_process_key\000"
.LASF1:
	.ascii	"hide_keypad\000"
.LASF4:
	.ascii	"validate_upper_range\000"
.LASF12:
	.ascii	"NUMERIC_KEYPAD_draw_double_keypad\000"
.LASF3:
	.ascii	"process_up_arrow\000"
.LASF6:
	.ascii	"FDTO_show_keypad\000"
.LASF18:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_numeric_keypad.c\000"
.LASF17:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF8:
	.ascii	"process_key_press\000"
.LASF5:
	.ascii	"show_keypad\000"
.LASF7:
	.ascii	"process_OK_button\000"
.LASF15:
	.ascii	"process_right_arrow\000"
.LASF11:
	.ascii	"NUMERIC_KEYPAD_draw_float_keypad\000"
.LASF2:
	.ascii	"init_common_globals\000"
.LASF9:
	.ascii	"NUMERIC_KEYPAD_draw_uint32_keypad\000"
.LASF10:
	.ascii	"NUMERIC_KEYPAD_draw_int32_keypad\000"
.LASF13:
	.ascii	"process_down_arrow\000"
.LASF0:
	.ascii	"validate_lower_range\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
