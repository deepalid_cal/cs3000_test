	.file	"e_percent_adjust.c"
	.text
.Ltext0:
	.section	.text.PERCENT_ADJUST_process_percentage,"ax",%progbits
	.align	2
	.type	PERCENT_ADJUST_process_percentage, %function
PERCENT_ADJUST_process_percentage:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #72
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI0:
	mov	r5, r2
	mov	r4, r3
	movhi	r1, #4
	bhi	.L2
	cmp	r1, #48
	movls	r1, #1
	movhi	r1, #2
.L2:
	mov	r3, #0
	str	r1, [sp, #0]
	str	r3, [sp, #4]
	mov	r1, r5
	mvn	r2, #79
	mov	r3, #100
	bl	process_int32
	ldr	r0, [r5, #0]
	cmp	r0, #0
	bne	.L3
	str	r0, [r4, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	Redraw_Screen
.L3:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L4
	bl	Refresh_Screen
	mov	r3, #1
	str	r3, [r4, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, pc}
.L4:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	Refresh_Screen
.LFE0:
	.size	PERCENT_ADJUST_process_percentage, .-PERCENT_ADJUST_process_percentage
	.section	.text.PERCENT_ADJUST_process_days,"ax",%progbits
	.align	2
	.type	PERCENT_ADJUST_process_days, %function
PERCENT_ADJUST_process_days:
.LFB1:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #72
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	movhi	r1, #4
	sub	sp, sp, #64
.LCFI2:
	mov	r4, r2
	mov	r5, r3
	bhi	.L9
	cmp	r1, #48
	movls	r1, #1
	movhi	r1, #2
.L9:
	mov	r2, #0
	str	r1, [sp, #0]
	ldr	r3, .L12
	mov	r1, r4
	str	r2, [sp, #4]
	bl	process_uns32
	add	r0, sp, #56
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, [r4, #0]
	ldrh	r2, [sp, #60]
	mov	r1, #225
	str	r1, [sp, #0]
	add	r2, r2, r3
	add	r0, sp, #8
	mov	r3, #125
	mov	r1, #48
	bl	GetDateStr
	mov	r2, #49
	mov	r1, r0
	mov	r0, r5
	bl	strlcpy
	bl	Refresh_Screen
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, pc}
.L13:
	.align	2
.L12:
	.word	366
.LFE1:
	.size	PERCENT_ADJUST_process_days, .-PERCENT_ADJUST_process_days
	.section	.text.FDTO_PERCENT_ADJUST_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_PERCENT_ADJUST_draw_screen
	.type	FDTO_PERCENT_ADJUST_draw_screen, %function
FDTO_PERCENT_ADJUST_draw_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L17
	str	lr, [sp, #-4]!
.LCFI3:
	ldrnesh	r1, [r3, #0]
	bne	.L16
	bl	PERCENT_ADJUST_copy_group_into_guivars
	mov	r1, #0
.L16:
	mov	r0, #48
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L18:
	.align	2
.L17:
	.word	GuiLib_ActiveCursorFieldNo
.LFE2:
	.size	FDTO_PERCENT_ADJUST_draw_screen, .-FDTO_PERCENT_ADJUST_draw_screen
	.global	__modsi3
	.section	.text.PERCENT_ADJUST_process_screen,"ax",%progbits
	.align	2
	.global	PERCENT_ADJUST_process_screen
	.type	PERCENT_ADJUST_process_screen, %function
PERCENT_ADJUST_process_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r0, r4, lr}
.LCFI4:
	mov	r4, r0
	beq	.L24
	bhi	.L27
	cmp	r0, #1
	beq	.L22
	bcc	.L21
	cmp	r0, #3
	beq	.L23
	cmp	r0, #4
	bne	.L20
	b	.L24
.L27:
	cmp	r0, #67
	beq	.L25
	bhi	.L28
	cmp	r0, #20
	bne	.L20
	b	.L21
.L28:
	cmp	r0, #80
	beq	.L26
	cmp	r0, #84
	bne	.L20
.L26:
	ldr	r3, .L62
	ldrsh	r3, [r3, #0]
	cmp	r3, #19
	ldrls	pc, [pc, r3, asl #2]
	b	.L56
.L50:
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
	.word	.L48
	.word	.L49
.L30:
	mov	r0, r4
	ldr	r2, .L62+4
	ldr	r3, .L62+8
	b	.L57
.L31:
	ldr	r2, .L62+12
	ldr	r3, .L62+16
	mov	r0, r4
.L57:
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	PERCENT_ADJUST_process_percentage
.L32:
	mov	r0, r4
	ldr	r2, .L62+20
	ldr	r3, .L62+24
	b	.L57
.L33:
	mov	r0, r4
	ldr	r2, .L62+28
	ldr	r3, .L62+32
	b	.L57
.L34:
	mov	r0, r4
	ldr	r2, .L62+36
	ldr	r3, .L62+40
	b	.L57
.L35:
	mov	r0, r4
	ldr	r2, .L62+44
	ldr	r3, .L62+48
	b	.L57
.L36:
	mov	r0, r4
	ldr	r2, .L62+52
	ldr	r3, .L62+56
	b	.L57
.L37:
	mov	r0, r4
	ldr	r2, .L62+60
	ldr	r3, .L62+64
	b	.L57
.L38:
	mov	r0, r4
	ldr	r2, .L62+68
	ldr	r3, .L62+72
	b	.L57
.L39:
	mov	r0, r4
	ldr	r2, .L62+76
	ldr	r3, .L62+80
	b	.L57
.L40:
	mov	r0, r4
	ldr	r2, .L62+84
	ldr	r3, .L62+88
	b	.L60
.L41:
	mov	r0, r4
	ldr	r2, .L62+92
	ldr	r3, .L62+96
	b	.L60
.L42:
	mov	r0, r4
	ldr	r2, .L62+100
	ldr	r3, .L62+104
	b	.L60
.L43:
	mov	r0, r4
	ldr	r2, .L62+108
	ldr	r3, .L62+112
	b	.L60
.L44:
	mov	r0, r4
	ldr	r2, .L62+116
	ldr	r3, .L62+120
	b	.L60
.L45:
	mov	r0, r4
	ldr	r2, .L62+124
	ldr	r3, .L62+128
	b	.L60
.L46:
	mov	r0, r4
	ldr	r2, .L62+132
	ldr	r3, .L62+136
	b	.L60
.L47:
	mov	r0, r4
	ldr	r2, .L62+140
	ldr	r3, .L62+144
	b	.L60
.L48:
	mov	r0, r4
	ldr	r2, .L62+148
	ldr	r3, .L62+152
	b	.L60
.L49:
	ldr	r2, .L62+156
	ldr	r3, .L62+160
	mov	r0, r4
.L60:
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	PERCENT_ADJUST_process_days
.L24:
	ldr	r3, .L62
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	movs	r0, r0, asl #16
	beq	.L56
.L51:
	mov	r0, #1
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Up
.L21:
	ldr	r3, .L62
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	bl	STATION_GROUP_get_num_groups_in_use
	sub	r0, r0, #1
	cmp	r4, r0
	beq	.L56
.L52:
	mov	r0, #1
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Down
.L22:
	ldr	r3, .L62
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #9
	movls	r0, r0, asr #16
	addls	r0, r0, #9
	bls	.L61
	sub	r3, r3, #10
	mov	r3, r3, asl #16
	cmp	r3, #589824
	bhi	.L56
	mov	r0, r0, asr #16
	sub	r0, r0, #10
.L61:
	mov	r1, r4
	b	.L59
.L23:
	ldr	r3, .L62
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #9
	movls	r0, r0, asr #16
	addls	r0, r0, #10
	bls	.L58
	sub	r3, r3, #10
	mov	r3, r3, asl #16
	cmp	r3, #589824
	bhi	.L56
	mov	r0, r0, asr #16
	sub	r0, r0, #9
.L58:
	mov	r1, #1
.L59:
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Select
.L56:
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	bad_key_beep
.L25:
	ldr	r3, .L62+164
	mov	r2, #1
	str	r2, [r3, #0]
	str	r1, [sp, #0]
	bl	PERCENT_ADJUST_extract_and_store_changes_from_GuiVars
	ldr	r1, [sp, #0]
.L20:
	mov	r0, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	KEY_process_global_keys
.L63:
	.align	2
.L62:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_PercentAdjustPercent_0
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_PercentAdjustPercent_1
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_PercentAdjustPercent_2
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_PercentAdjustPercent_3
	.word	GuiVar_GroupSettingC_3
	.word	GuiVar_PercentAdjustPercent_4
	.word	GuiVar_GroupSettingC_4
	.word	GuiVar_PercentAdjustPercent_5
	.word	GuiVar_GroupSettingC_5
	.word	GuiVar_PercentAdjustPercent_6
	.word	GuiVar_GroupSettingC_6
	.word	GuiVar_PercentAdjustPercent_7
	.word	GuiVar_GroupSettingC_7
	.word	GuiVar_PercentAdjustPercent_8
	.word	GuiVar_GroupSettingC_8
	.word	GuiVar_PercentAdjustPercent_9
	.word	GuiVar_GroupSettingC_9
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_PercentAdjustEndDate_0
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_PercentAdjustEndDate_1
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_PercentAdjustEndDate_2
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_PercentAdjustEndDate_3
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_PercentAdjustEndDate_4
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_PercentAdjustEndDate_5
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_PercentAdjustEndDate_6
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_PercentAdjustEndDate_7
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_PercentAdjustEndDate_8
	.word	GuiVar_GroupSettingB_9
	.word	GuiVar_PercentAdjustEndDate_9
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	PERCENT_ADJUST_process_screen, .-PERCENT_ADJUST_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_percent_adjust.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x45
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x6d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x90
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xa4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"PERCENT_ADJUST_process_screen\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_percent_adjust.c\000"
.LASF2:
	.ascii	"FDTO_PERCENT_ADJUST_draw_screen\000"
.LASF1:
	.ascii	"PERCENT_ADJUST_process_days\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"PERCENT_ADJUST_process_percentage\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
