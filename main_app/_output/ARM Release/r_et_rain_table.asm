	.file	"r_et_rain_table.c"
	.text
.Ltext0:
	.section	.text.FDTO_ET_RAIN_TABLE_process_et_value,"ax",%progbits
	.align	2
	.type	FDTO_ET_RAIN_TABLE_process_et_value, %function
FDTO_ET_RAIN_TABLE_process_et_value:
.LFB0:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI0:
	ldr	r3, .L2+12
	mov	r2, #1
	mov	r4, r1
	mov	r6, r0
	str	r2, [r3, #0]
	mov	r0, r1
	add	r1, sp, #8
	bl	WEATHER_TABLES_get_et_table_entry_for_index
	ldrh	r3, [sp, #8]
	add	r1, sp, #16
	fmsr	s13, r3	@ int
	ldr	r3, .L2+16
	mov	r2, #0
	fsitod	d7, s13
	fldd	d6, .L2
	mov	r5, #0
	mov	r0, r6
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fstmdbs	r1!, {s14}
	str	r3, [sp, #0]	@ float
	mov	r3, #1056964608
	str	r5, [sp, #4]
	bl	process_fl
	flds	s14, [sp, #12]
	flds	s15, .L2+8
	mov	r0, r4
	add	r1, sp, #8
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	strh	r3, [sp, #8]	@ movhi
	mov	r3, #2
	strh	r3, [sp, #10]	@ movhi
	bl	WEATHER_TABLES_update_et_table_entry_for_index
	mov	r1, r4, asl #16
	mov	r1, r1, lsr #16
	mov	r0, r5
	bl	GuiLib_ScrollBox_RedrawLine
	bl	GuiLib_Refresh
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, pc}
.L3:
	.align	2
.L2:
	.word	0
	.word	1086556160
	.word	1176256512
	.word	.LANCHOR0
	.word	1008981770
.LFE0:
	.size	FDTO_ET_RAIN_TABLE_process_et_value, .-FDTO_ET_RAIN_TABLE_process_et_value
	.section	.text.ET_RAIN_TABLE_load_guivars_for_scroll_line,"ax",%progbits
	.align	2
	.type	ET_RAIN_TABLE_load_guivars_for_scroll_line, %function
ET_RAIN_TABLE_load_guivars_for_scroll_line:
.LFB1:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r0, r0, asl #16
	sub	sp, sp, #60
.LCFI2:
	mov	r4, r0, asr #16
	bl	WEATHER_TABLES_get_et_table_date
	mov	r3, #250
	str	r3, [sp, #0]
	mov	r1, #48
	mov	r3, #100
	rsb	r2, r4, r0
	add	r0, sp, #4
	bl	GetDateStr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L10+16
	bl	strlcpy
	add	r1, sp, #52
	mov	r0, r4
	bl	WEATHER_TABLES_get_et_table_entry_for_index
	ldrh	r3, [sp, #52]
	ldrh	r2, [sp, #54]
	fmsr	s13, r3	@ int
	ldr	r3, .L10+20
	mov	r0, r4
	fsitod	d7, s13
	fldd	d6, .L10
	add	r1, sp, #56
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	ldr	r3, .L10+24
	str	r2, [r3, #0]
	bl	WEATHER_TABLES_get_rain_table_entry_for_index
	ldrh	r3, [sp, #56]
	cmp	r4, #0
	fmsr	s13, r3	@ int
	ldr	r3, .L10+28
	movne	r2, #0
	fsitod	d7, s13
	fldd	d6, .L10+8
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	ldr	r3, .L10+32
	bne	.L9
	ldr	r2, .L10+36
	ldr	r1, [r2, #60]
	cmp	r1, #0
	movne	r2, #1
	bne	.L9
	ldr	r2, [r2, #56]
	adds	r2, r2, #0
	movne	r2, #1
.L9:
	str	r2, [r3, #0]
	ldrh	r2, [sp, #58]
	ldr	r3, .L10+40
	str	r2, [r3, #0]
	add	sp, sp, #60
	ldmfd	sp!, {r4, pc}
.L11:
	.align	2
.L10:
	.word	0
	.word	1086556160
	.word	0
	.word	1079574528
	.word	GuiVar_ETRainTableDate
	.word	GuiVar_ETRainTableET
	.word	GuiVar_ETRainTableETStatus
	.word	GuiVar_ETRainTableRain
	.word	GuiVar_ETRainTableShutdown
	.word	weather_preserves
	.word	GuiVar_ETRainTableRainStatus
.LFE1:
	.size	ET_RAIN_TABLE_load_guivars_for_scroll_line, .-ET_RAIN_TABLE_load_guivars_for_scroll_line
	.global	__udivsi3
	.section	.text.FDTO_ET_RAIN_TABLE_update_report,"ax",%progbits
	.align	2
	.global	FDTO_ET_RAIN_TABLE_update_report
	.type	FDTO_ET_RAIN_TABLE_update_report, %function
FDTO_ET_RAIN_TABLE_update_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI3:
	ldr	r4, .L14
	ldr	r6, .L14+4
	ldr	r2, .L14+8
	mov	r3, #134
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldrh	r0, [r6, #36]
	mov	r1, #100
	bl	__udivsi3
	ldr	r3, .L14+12
	ldrh	r2, [r6, #40]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	str	r0, [r3, #0]
	ldr	r3, .L14+16
	ldr	r0, [r4, #0]
	str	r2, [r3, #0]
	ldr	r2, [r6, #52]
	ldr	r3, .L14+20
	str	r2, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r5, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_Refresh
.L15:
	.align	2
.L14:
	.word	weather_preserves_recursive_MUTEX
	.word	weather_preserves
	.word	.LC0
	.word	GuiVar_ETRainTableCurrentET
	.word	GuiVar_ETRainTableTable
	.word	GuiVar_ETRainTableReport
.LFE2:
	.size	FDTO_ET_RAIN_TABLE_update_report, .-FDTO_ET_RAIN_TABLE_update_report
	.section	.text.FDTO_ET_RAIN_TABLE_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_ET_RAIN_TABLE_draw_report
	.type	FDTO_ET_RAIN_TABLE_draw_report, %function
FDTO_ET_RAIN_TABLE_draw_report:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L17
	stmfd	sp!, {r4, lr}
.LCFI4:
	mov	r4, r0
	mov	r0, #0
	str	r0, [r3, #0]
	bl	FDTO_ET_RAIN_TABLE_update_report
	mov	r0, #77
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	r2, .L17+4
	mov	r0, r4
	mov	r1, #60
	ldmfd	sp!, {r4, lr}
	b	FDTO_REPORTS_draw_report
.L18:
	.align	2
.L17:
	.word	.LANCHOR0
	.word	ET_RAIN_TABLE_load_guivars_for_scroll_line
.LFE3:
	.size	FDTO_ET_RAIN_TABLE_draw_report, .-FDTO_ET_RAIN_TABLE_draw_report
	.section	.text.ET_RAIN_TABLE_process_report,"ax",%progbits
	.align	2
	.global	ET_RAIN_TABLE_process_report
	.type	ET_RAIN_TABLE_process_report, %function
ET_RAIN_TABLE_process_report:
.LFB4:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #80
	stmfd	sp!, {r4, r5, lr}
.LCFI5:
	mov	r3, r0
	sub	sp, sp, #36
.LCFI6:
	mov	r5, r1
	beq	.L22
	cmp	r0, #84
	beq	.L22
	cmp	r0, #67
	bne	.L25
	b	.L26
.L22:
	mov	r2, #3
	str	r2, [sp, #0]
	ldr	r2, .L27
	mov	r0, #0
	mov	r1, r0
	str	r2, [sp, #20]
	str	r3, [sp, #24]
	bl	GuiLib_ScrollBox_GetActiveLine
	str	r0, [sp, #28]
	mov	r0, sp
	bl	Display_Post_Command
	b	.L19
.L26:
	ldr	r4, .L27+4
	ldr	r3, [r4, #0]
	cmp	r3, #1
	bne	.L24
	mov	r0, #7
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	mov	r3, #0
	str	r3, [r4, #0]
.L24:
	ldr	r3, .L27+8
	mov	r2, #9
	str	r2, [r3, #0]
	mov	r0, #67
	mov	r1, r5
	bl	KEY_process_global_keys
	b	.L19
.L25:
	mov	r2, #0
	mov	r3, r2
	bl	REPORTS_process_report
.L19:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, pc}
.L28:
	.align	2
.L27:
	.word	FDTO_ET_RAIN_TABLE_process_et_value
	.word	.LANCHOR0
	.word	GuiVar_MenuScreenToShow
.LFE4:
	.size	ET_RAIN_TABLE_process_report, .-ET_RAIN_TABLE_process_report
	.section	.bss.g_ET_RAIN_TABLE_changes_made,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_ET_RAIN_TABLE_changes_made, %object
	.size	g_ET_RAIN_TABLE_changes_made, 4
g_ET_RAIN_TABLE_changes_made:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_et_rain_table.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI5-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_et_rain_table.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x80
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x26
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x52
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x82
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xa3
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xbf
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI6
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"FDTO_ET_RAIN_TABLE_update_report\000"
.LASF1:
	.ascii	"ET_RAIN_TABLE_load_guivars_for_scroll_line\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_et_rain_table.c\000"
.LASF4:
	.ascii	"ET_RAIN_TABLE_process_report\000"
.LASF3:
	.ascii	"FDTO_ET_RAIN_TABLE_draw_report\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"FDTO_ET_RAIN_TABLE_process_et_value\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
