	.file	"r_irri_details.c"
	.text
.Ltext0:
	.section	.text.FDTO_IRRI_DETAILS_update_flow_and_current,"ax",%progbits
	.align	2
	.type	FDTO_IRRI_DETAILS_update_flow_and_current, %function
FDTO_IRRI_DETAILS_update_flow_and_current:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L9+4
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L9+8
	ldr	r3, .L9+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L9+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L9+8
	mov	r3, #296
	bl	xQueueTakeMutexRecursive_debug
	bl	POC_at_least_one_POC_has_a_flow_meter
	cmp	r0, #1
	movne	r0, #0
	bne	.L2
	bl	SYSTEM_num_systems_in_use
	adds	r0, r0, #0
	movne	r0, #1
.L2:
	ldr	r3, .L9+20
	cmp	r0, #1
	str	r0, [r3, #0]
	bne	.L3
	ldr	r5, .L9+24
	mov	r4, #0
	str	r4, [r5, #0]
.L5:
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	cmp	r0, #0
	beq	.L4
	bl	nm_GROUP_get_group_ID
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	cmp	r0, #0
	beq	.L4
	flds	s15, [r0, #304]
	ldr	r3, [r5, #0]
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	add	r3, r2, r3
	str	r3, [r5, #0]
.L4:
	add	r4, r4, #1
	cmp	r4, #4
	bne	.L5
	bl	SYSTEM_get_highest_flow_checking_status_for_any_system
	ldr	r3, .L9+28
	str	r0, [r3, #0]
.L3:
	ldr	r3, .L9+16
	ldr	r4, .L9+32
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L9+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r1, #400
	ldr	r2, .L9+8
	ldr	r3, .L9+36
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L9+40
	add	r0, r0, #39
	ldr	r3, [r3, r0, asl #2]
	ldr	r0, [r4, #0]
	fmsr	s15, r3	@ int
	ldr	r3, .L9+44
	fuitos	s14, s15
	flds	s15, .L9
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L10:
	.align	2
.L9:
	.word	1148846080
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	294
	.word	list_system_recursive_MUTEX
	.word	GuiVar_StatusShowSystemFlow
	.word	GuiVar_StatusSystemFlowActual
	.word	GuiVar_StatusSystemFlowStatus
	.word	tpmicro_data_recursive_MUTEX
	.word	333
	.word	tpmicro_data
	.word	GuiVar_StatusCurrentDraw
.LFE2:
	.size	FDTO_IRRI_DETAILS_update_flow_and_current, .-FDTO_IRRI_DETAILS_update_flow_and_current
	.section	.text.FDTO_IRRI_DETAILS_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_IRRI_DETAILS_draw_report
	.type	FDTO_IRRI_DETAILS_draw_report, %function
FDTO_IRRI_DETAILS_draw_report:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r4, .L19
	subs	r5, r0, #0
	movne	r3, #0
	strne	r3, [r4, #0]
	mov	r0, #85
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	r3, [r4, #0]
	ldr	r4, .L19+4
	cmp	r3, #0
	bne	.L13
	ldr	r3, .L19+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L19+12
	mov	r3, #420
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L19+16
	ldr	r3, [r3, #8]
	b	.L17
.L13:
	ldr	r3, .L19+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L19+12
	ldr	r3, .L19+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L19+28
	ldr	r3, [r3, #24]
.L17:
	str	r3, [r4, #0]
	ldr	r3, .L19+4
	mov	r0, r5
	ldr	r1, [r3, #0]
	ldr	r2, .L19+32
	bl	FDTO_REPORTS_draw_report
	ldr	r3, .L19
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldreq	r3, .L19+8
	ldrne	r3, .L19+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r4, r5, lr}
	b	FDTO_IRRI_DETAILS_update_flow_and_current
.L20:
	.align	2
.L19:
	.word	GuiVar_IrriDetailsShowFOAL
	.word	.LANCHOR0
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	irri_irri
	.word	irri_irri_recursive_MUTEX
	.word	426
	.word	foal_irri
	.word	nm_FDTO_IRRI_DETAILS_load_guivars_for_scroll_line
.LFE4:
	.size	FDTO_IRRI_DETAILS_draw_report, .-FDTO_IRRI_DETAILS_draw_report
	.section	.text.nm_FDTO_IRRI_DETAILS_load_guivars_for_scroll_line,"ax",%progbits
	.align	2
	.type	nm_FDTO_IRRI_DETAILS_load_guivars_for_scroll_line, %function
nm_FDTO_IRRI_DETAILS_load_guivars_for_scroll_line:
.LFB1:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L51+8
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI2:
	ldr	r6, [r3, #0]
	mov	r0, r0, asl #16
	cmp	r6, #0
	sub	sp, sp, #20
.LCFI3:
	mov	r5, r0, asr #16
	bne	.L22
	ldr	r3, .L51+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L51+16
	mov	r3, #106
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L51+20
	bl	nm_ListGetFirst
	mov	r4, r0
	b	.L23
.L25:
	cmp	r6, r5
	beq	.L24
	mov	r1, r4
	ldr	r0, .L51+20
	bl	nm_ListGetNext
	add	r6, r6, #1
	mov	r4, r0
.L23:
	cmp	r4, #0
	bne	.L25
	b	.L26
.L24:
	ldr	r0, [r4, #36]
	ldr	r3, .L51+24
	ldrb	r1, [r4, #40]	@ zero_extendqisi2
	str	r0, [r3, #0]
	mov	r2, sp
	mov	r3, #16
	bl	STATION_get_station_number_string
	mov	r1, #4
	ldr	r2, .L51+28
	mov	r5, #0
	mov	r3, r0
	ldr	r0, .L51+32
	bl	snprintf
	flds	s13, [r4, #32]	@ int
	flds	s15, .L51
	ldrb	r2, [r4, #52]	@ zero_extendqisi2
	fuitos	s14, s13
	ldr	r3, .L51+36
	flds	s13, [r4, #48]	@ int
	mov	r2, r2, lsr #4
	str	r2, [r3, #0]
	ldr	r3, .L51+40
	ldr	r2, .L51+44
	fdivs	s14, s14, s15
	fsts	s14, [r3, #0]
	fuitos	s14, s13
	ldr	r3, .L51+48
	fdivs	s14, s14, s15
	fsts	s14, [r3, #0]
	ldrb	r3, [r4, #53]	@ zero_extendqisi2
	ands	r1, r3, #1
	ldr	r3, .L51+52
	beq	.L27
	flds	s13, [r4, #44]	@ int
	str	r5, [r2, #0]	@ float
	mov	r2, #1
	ldr	r0, [r4, #36]
	fsitos	s14, s13
	ldrb	r1, [r4, #40]	@ zero_extendqisi2
	ldr	r4, .L51+56
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	ldr	r3, .L51+60
	str	r2, [r3, #0]
	add	r2, sp, #16
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	cmp	r0, #1
	strne	r5, [r4, #0]	@ float
	bne	.L32
	b	.L49
.L27:
	str	r5, [r3, #0]	@ float
	ldr	r3, [r4, #44]
	fmsr	s13, r3	@ int
	cmp	r3, #0
	ldr	r3, .L51+56
	fsitos	s14, s13
	movgt	r1, #2
	str	r5, [r3, #0]	@ float
	fdivs	s15, s14, s15
	fsts	s15, [r2, #0]
	ldr	r2, .L51+60
	str	r1, [r2, #0]
	b	.L32
.L49:
	ldr	r5, .L51+64
	ldr	r2, .L51+16
	mov	r3, #153
	ldr	r0, [r5, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [sp, #16]
	ldr	r3, .L51+68
	ldr	r0, [r5, #0]
	add	r3, r3, r2, asl #7
	ldrh	r3, [r3, #142]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, .L51+4
	fdivs	s15, s14, s15
	fsts	s15, [r4, #0]
	bl	xQueueGiveMutexRecursive
.L32:
	ldr	r3, .L51+72
	mov	r2, #0
	strb	r2, [r3, #0]
.L26:
	ldr	r3, .L51+12
	b	.L48
.L22:
	ldr	r3, .L51+76
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L51+16
	mov	r3, #195
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L51+80
	bl	nm_ListGetFirst
	mov	r6, #0
	mov	r4, r0
	b	.L34
.L36:
	cmp	r6, r5
	beq	.L35
	mov	r1, r4
	ldr	r0, .L51+80
	bl	nm_ListGetNext
	add	r6, r6, #1
	mov	r4, r0
.L34:
	cmp	r4, #0
	bne	.L36
	b	.L37
.L35:
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	ldr	r3, .L51+24
	ldrb	r1, [r4, #90]	@ zero_extendqisi2
	str	r0, [r3, #0]
	mov	r2, sp
	mov	r3, #16
	bl	STATION_get_station_number_string
	ldr	r2, .L51+28
	mov	r1, #4
	mov	r5, #0
	mov	r3, r0
	ldr	r0, .L51+32
	bl	snprintf
	flds	s13, [r4, #52]	@ int
	flds	s15, .L51
	ldrh	r2, [r4, #72]
	fuitos	s14, s13
	ldr	r3, .L51+36
	flds	s13, [r4, #56]	@ int
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	str	r2, [r3, #0]
	ldr	r3, .L51+40
	fdivs	s14, s14, s15
	ldr	r2, .L51+44
	fsts	s14, [r3, #0]
	fuitos	s14, s13
	ldr	r3, .L51+48
	fdivs	s14, s14, s15
	fsts	s14, [r3, #0]
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	tst	r3, #64
	ldr	r3, .L51+52
	beq	.L38
	flds	s13, [r4, #48]	@ int
	str	r5, [r2, #0]	@ float
	mov	r2, #1
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	fsitos	s14, s13
	ldrb	r1, [r4, #90]	@ zero_extendqisi2
	ldr	r4, .L51+56
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	ldr	r3, .L51+60
	str	r2, [r3, #0]
	add	r2, sp, #16
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	cmp	r0, #1
	strne	r5, [r4, #0]	@ float
	bne	.L43
	b	.L50
.L38:
	str	r5, [r3, #0]	@ float
	ldr	r3, [r4, #60]
	ldr	r1, .L51+60
	fmsr	s13, r3	@ int
	cmp	r3, #0
	movne	r3, #2
	str	r3, [r1, #0]
	fuitos	s14, s13
	fdivs	s15, s14, s15
	fsts	s15, [r2, #0]
	ldr	r2, .L51+56
	str	r5, [r2, #0]	@ float
	b	.L43
.L50:
	ldr	r5, .L51+64
	ldr	r2, .L51+16
	mov	r3, #241
	ldr	r0, [r5, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [sp, #16]
	ldr	r3, .L51+68
	ldr	r0, [r5, #0]
	add	r3, r3, r2, asl #7
	ldrh	r3, [r3, #142]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, .L51+4
	fdivs	s15, s14, s15
	fsts	s15, [r4, #0]
	bl	xQueueGiveMutexRecursive
.L43:
	ldr	r3, .L51+72
	mov	r2, #0
	strb	r2, [r3, #0]
.L37:
	ldr	r3, .L51+76
.L48:
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, pc}
.L52:
	.align	2
.L51:
	.word	1114636288
	.word	1148846080
	.word	GuiVar_IrriDetailsShowFOAL
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC0
	.word	irri_irri
	.word	GuiVar_IrriDetailsController
	.word	.LC1
	.word	GuiVar_IrriDetailsStation
	.word	GuiVar_IrriDetailsReason
	.word	GuiVar_IrriDetailsProgLeft
	.word	GuiVar_IrriDetailsRunSoak
	.word	GuiVar_IrriDetailsProgCycle
	.word	GuiVar_IrriDetailsRunCycle
	.word	GuiVar_IrriDetailsCurrentDraw
	.word	GuiVar_IrriDetailsStatus
	.word	station_preserves_recursive_MUTEX
	.word	station_preserves
	.word	GuiVar_StationDescription
	.word	irri_irri_recursive_MUTEX
	.word	foal_irri+16
.LFE1:
	.size	nm_FDTO_IRRI_DETAILS_load_guivars_for_scroll_line, .-nm_FDTO_IRRI_DETAILS_load_guivars_for_scroll_line
	.section	.text.IRRI_DETAILS_process_report,"ax",%progbits
	.align	2
	.global	IRRI_DETAILS_process_report
	.type	IRRI_DETAILS_process_report, %function
IRRI_DETAILS_process_report:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #81
	str	lr, [sp, #-4]!
.LCFI4:
	bne	.L56
.LBB4:
	bl	good_key_beep
	ldr	r3, .L57
	mov	r0, #0
	ldr	r2, [r3, #0]
	rsbs	r2, r2, #1
	movcc	r2, #0
	str	r2, [r3, #0]
.LBE4:
	ldr	lr, [sp], #4
.LBB5:
	b	Redraw_Screen
.L56:
.LBE5:
	mov	r2, #0
	mov	r3, r2
	ldr	lr, [sp], #4
	b	REPORTS_process_report
.L58:
	.align	2
.L57:
	.word	GuiVar_IrriDetailsShowFOAL
.LFE5:
	.size	IRRI_DETAILS_process_report, .-IRRI_DETAILS_process_report
	.section	.text.IRRI_DETAILS_jump_to_irrigation_details,"ax",%progbits
	.align	2
	.global	IRRI_DETAILS_jump_to_irrigation_details
	.type	IRRI_DETAILS_jump_to_irrigation_details, %function
IRRI_DETAILS_jump_to_irrigation_details:
.LFB0:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L60
	mov	r3, #22
	str	lr, [sp, #-4]!
.LCFI5:
	mov	r0, #2
	sub	sp, sp, #36
.LCFI6:
	str	r3, [r2, #0]
	mov	r1, #10
	mov	r2, #85
	stmia	sp, {r0, r1, r2}
	ldr	r2, .L60+4
	str	r3, [sp, #32]
	ldr	r3, .L60+8
	str	r2, [sp, #20]
	mov	r0, sp
	mov	r2, #1
	str	r2, [sp, #24]
	str	r3, [sp, #16]
	bl	Change_Screen
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L61:
	.align	2
.L60:
	.word	GuiVar_MenuScreenToShow
	.word	FDTO_IRRI_DETAILS_draw_report
	.word	IRRI_DETAILS_process_report
.LFE0:
	.size	IRRI_DETAILS_jump_to_irrigation_details, .-IRRI_DETAILS_jump_to_irrigation_details
	.section	.text.FDTO_IRRI_DETAILS_redraw_scrollbox,"ax",%progbits
	.align	2
	.global	FDTO_IRRI_DETAILS_redraw_scrollbox
	.type	FDTO_IRRI_DETAILS_redraw_scrollbox, %function
FDTO_IRRI_DETAILS_redraw_scrollbox:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI7:
	ldr	r4, .L63
	mov	r1, #400
	ldr	r2, .L63+4
	mov	r3, #376
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r6, .L63+8
	ldr	r3, .L63+12
	ldr	r2, [r6, #0]
	ldr	r5, [r3, #8]
	mov	r0, #0
	subs	r2, r5, r2
	mov	r1, r5
	movne	r2, #1
	bl	FDTO_SCROLL_BOX_redraw_retaining_topline
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	bl	FDTO_IRRI_DETAILS_update_flow_and_current
	str	r5, [r6, #0]
	ldmfd	sp!, {r4, r5, r6, pc}
.L64:
	.align	2
.L63:
	.word	irri_irri_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
	.word	irri_irri
.LFE3:
	.size	FDTO_IRRI_DETAILS_redraw_scrollbox, .-FDTO_IRRI_DETAILS_redraw_scrollbox
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_irri_details.c\000"
.LC1:
	.ascii	"%s\000"
	.section	.bss.g_IRRI_DETAILS_line_count,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_IRRI_DETAILS_line_count, %object
	.size	g_IRRI_DETAILS_line_count, 4
g_IRRI_DETAILS_line_count:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI5-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI7-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_irri_details.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x9e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1ce
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x11c
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x198
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	.LASF1
	.byte	0x1
	.byte	0x5a
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x38
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST4
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x166
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB0
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI6
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB3
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"FDTO_IRRI_DETAILS_draw_report\000"
.LASF3:
	.ascii	"IRRI_DETAILS_jump_to_irrigation_details\000"
.LASF1:
	.ascii	"nm_FDTO_IRRI_DETAILS_load_guivars_for_scroll_line\000"
.LASF4:
	.ascii	"FDTO_IRRI_DETAILS_redraw_scrollbox\000"
.LASF7:
	.ascii	"IRRI_DETAILS_process_report\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"FDTO_IRRI_DETAILS_update_flow_and_current\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_irri_details.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
