	.file	"e_budgets.c"
	.text
.Ltext0:
	.section	.text.BUDGETS_show_warnings,"ax",%progbits
	.align	2
	.type	BUDGETS_show_warnings, %function
BUDGETS_show_warnings:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L4
	flds	s15, [r3, #0]
	fcmpzs	s15
	fmstat
	moveq	r3, #1
	beq	.L2
	ldr	r3, .L4+4
	flds	s15, [r3, #0]
	fcmpzs	s15
	fmstat
	movne	r3, #0
	moveq	r3, #1
.L2:
	ldr	r2, .L4+8
	mov	r0, #0
	str	r3, [r2, #0]
	b	Redraw_Screen
.L5:
	.align	2
.L4:
	.word	GuiVar_BudgetForThisPeriod
	.word	GuiVar_BudgetAnnualValue
	.word	GuiVar_BudgetZeroWarning
.LFE2:
	.size	BUDGETS_show_warnings, .-BUDGETS_show_warnings
	.section	.text.BUDGET_SETUP_update_montly_budget,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_update_montly_budget, %function
BUDGET_SETUP_update_montly_budget:
.LFB4:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	fstmfdd	sp!, {d8}
.LCFI1:
	flds	s16, .L12
	mov	r4, #1
	sub	sp, sp, #20
.LCFI2:
.L7:
	mov	r0, r4
	bl	ET_DATA_et_number_for_the_month
	add	r4, r4, #1
	cmp	r4, #13
	fmsr	s14, r0
	fadds	s16, s16, s14
	bne	.L7
	ldr	r3, .L12+4
	mov	r0, sp
	flds	s15, [r3, #0]
	ldr	r5, .L12+8
	mov	r4, #0
	fdivs	s16, s15, s16
	bl	EPSON_obtain_latest_complete_time_and_date
.L9:
	ldrh	r0, [sp, #8]
	add	r0, r4, r0
	cmp	r0, #12
	subhi	r0, r0, #12
	bl	ET_DATA_et_number_for_the_month
	add	r4, r4, #1
	cmp	r4, #12
	fmsr	s14, r0
	fmuls	s15, s14, s16
	fmrs	r3, s15
	str	r3, [r5, #4]!	@ float
	bne	.L9
	add	sp, sp, #20
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, pc}
.L13:
	.align	2
.L12:
	.word	0
	.word	.LANCHOR0
	.word	.LANCHOR1-4
.LFE4:
	.size	BUDGET_SETUP_update_montly_budget, .-BUDGET_SETUP_update_montly_budget
	.section	.text.BUDGET_SETUP_handle_start_date_change,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_handle_start_date_change, %function
BUDGET_SETUP_handle_start_date_change:
.LFB7:
	@ args = 0, pretend = 0, frame = 160
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI3:
	fstmfdd	sp!, {d8}
.LCFI4:
	ldr	r3, .L25+4
	ldr	sl, .L25+8
	ldr	r8, .L25+12
	sub	sp, sp, #168
.LCFI5:
	mov	r4, r2
	str	r3, [sp, #160]	@ float
	ldr	r2, .L25+16
	ldr	r3, .L25+20
	mov	r9, r0
	mov	fp, r1
	ldr	r0, [sl, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, #400
	ldr	r2, .L25+16
	ldr	r3, .L25+24
	ldr	r0, [r8, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L25+28
	ldr	r6, .L25+32
	ldr	r0, [r3, #0]
	bl	POC_get_group_at_this_index
	ldr	r7, .L25+36
	mov	r1, #400
	ldr	r2, .L25+16
	ldr	r3, .L25+40
	mov	r5, r0
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, .L25+16
	mov	r3, #532
	mov	r1, #400
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	nm_GROUP_get_group_ID
	add	r1, sp, #164
	bl	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	ldr	r3, [r0, #24]
	ldr	r0, [r7, #0]
	ldr	r3, [r3, #468]
	mov	r3, r3, lsr #13
	and	r3, r3, #15
	str	r3, [sp, #8]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	bl	POC_get_GID_irrigation_system
	bl	SYSTEM_get_group_with_this_GID
	mov	r5, r0
	ldr	r0, [r8, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	r1, sp, #12
	bl	SYSTEM_get_budget_details
	ldr	r0, [sl, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r4, #0
	ldr	r3, .L25+44
	ldreq	r3, [r3, #4]
	subeq	r2, r3, #69
	subeq	r3, r3, #1
	beq	.L16
.L15:
	ldr	r2, [sp, #20]
	cmp	r4, r2
	sub	r2, r4, #1
	ldrcs	r3, [r3, r2, asl #2]
	addcs	r2, r3, #1
	addcs	r3, r3, #68
	bcs	.L16
	add	r0, r4, #1
	ldr	r1, [r3, r2, asl #2]
	ldr	r3, [r3, r0, asl #2]
	add	r2, r1, #1
	sub	r0, r3, #68
	add	r1, r1, #68
	sub	r3, r3, #1
	cmp	r0, r2
	movcs	r2, r0
	cmp	r3, r1
	movcs	r3, r1
.L16:
	ldr	r6, .L25+44
	mov	r7, #0
	add	r1, r6, r4, asl #2
	mov	r8, #1
	mov	r0, r9
	str	r8, [sp, #0]
	str	r7, [sp, #4]
	bl	process_uns32
	mov	r3, #125
	str	r7, [sp, #0]
	mov	r1, #16
	ldr	r2, [r6, r4, asl #2]
	add	r0, sp, #144
	bl	GetDateStr
	mov	r2, #16
	ldr	r6, .L25+48
	mov	r1, r0
	mov	r0, fp
	bl	strlcpy
	ldr	ip, [sp, #8]
	ldr	r2, [r6, #0]
	cmp	ip, r8
	movls	r3, #0
	movhi	r3, #1
	cmp	r2, r7
	movne	r3, #0
	cmp	r3, r7
	beq	.L18
	ldr	r0, .L25+52
	bl	DIALOG_draw_ok_dialog
	str	r8, [r6, #0]
.L18:
	ldr	r7, .L25+32
	flds	s17, .L25
	ldr	r6, .L25+36
	mov	r1, #400
	ldr	r2, .L25+16
	mov	r3, #600
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, #400
	ldr	r2, .L25+16
	ldr	r3, .L25+56
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	NETWORK_CONFIG_get_water_units
	cmp	r0, #0
	fldsne	s15, [sp, #160]
	ldr	r0, [r6, #0]
	fdivsne	s17, s17, s15
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r7, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r1, .L25+44
	mov	r3, #0
	mov	r2, r3
.L20:
	add	ip, sp, #12
	add	r0, ip, r2
	ldr	ip, [r2, r1]
	add	r3, r3, #1
	cmp	r3, #23
	str	ip, [r0, #12]
	add	r2, r2, #4
	bls	.L20
	str	r3, [sp, #164]
	ldr	r3, .L25+60
	ldr	r6, [r3, #0]
	cmp	r6, #0
	bne	.L21
	add	r0, sp, #124
	bl	EPSON_obtain_latest_complete_time_and_date
	ldr	r3, .L25+64
	mov	r0, r5
	flds	s16, [r3, #0]
	bl	nm_GROUP_get_group_ID
	ldrh	r3, [sp, #128]
	add	r1, sp, #12
	strh	r3, [sp, #0]	@ movhi
	add	r2, sp, #124
	ldr	r3, [sp, #124]
	str	r6, [sp, #4]
	bl	nm_BUDGET_predicted_volume
	ldr	r3, .L25+68
	fmsr	s15, r0
	fmacs	s16, s17, s15
	fsts	s16, [r3, #0]
	b	.L22
.L21:
	ldr	r3, .L25+44
	ldr	r1, [sp, #16]
	ldr	r0, [r3, r4, asl #2]
	add	r2, sp, #124
	bl	DateAndTimeToDTCS
	mov	r0, r5
	bl	nm_GROUP_get_group_ID
	mov	r3, #0
	str	r3, [sp, #4]
	ldrh	r3, [sp, #128]
	add	r1, sp, #12
	strh	r3, [sp, #0]	@ movhi
	add	r2, sp, #124
	ldr	r3, [sp, #124]
	bl	nm_BUDGET_predicted_volume
	ldr	r3, .L25+68
	fmsr	s15, r0
	fmuls	s17, s17, s15
	fsts	s17, [r3, #0]
.L22:
	bl	BUDGETS_show_warnings
	add	sp, sp, #168
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L26:
	.align	2
.L25:
	.word	1065353216
	.word	1144718131
	.word	list_system_recursive_MUTEX
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	523
	.word	525
	.word	g_GROUP_list_item_index
	.word	system_preserves_recursive_MUTEX
	.word	poc_preserves_recursive_MUTEX
	.word	531
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	586
	.word	601
	.word	GuiVar_BudgetPeriodIdx
	.word	GuiVar_BudgetUseThisPeriod
	.word	GuiVar_BudgetExpectedUseThisPeriod
.LFE7:
	.size	BUDGET_SETUP_handle_start_date_change, .-BUDGET_SETUP_handle_start_date_change
	.section	.text.FDTO_BUDGETS_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_BUDGETS_return_to_menu, %function
FDTO_BUDGETS_return_to_menu:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L28
	ldr	r1, .L28+4
	b	FDTO_GROUP_return_to_menu
.L29:
	.align	2
.L28:
	.word	g_POC_editing_group
	.word	BUDGETS_extract_and_store_changes_from_GuiVars
.LFE15:
	.size	FDTO_BUDGETS_return_to_menu, .-FDTO_BUDGETS_return_to_menu
	.section	.text.FDTO_POC_show_budget_period_idx_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_budget_period_idx_dropdown, %function
FDTO_POC_show_budget_period_idx_dropdown:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L31
	ldr	r0, .L31+4
	ldr	r1, .L31+8
	ldr	r3, [r3, #0]
	mov	r2, #12
	b	FDTO_COMBOBOX_show
.L32:
	.align	2
.L31:
	.word	GuiVar_BudgetPeriodIdx
	.word	723
	.word	FDTO_COMBOBOX_add_items
.LFE12:
	.size	FDTO_POC_show_budget_period_idx_dropdown, .-FDTO_POC_show_budget_period_idx_dropdown
	.section	.text.BUDGETS_update_guivars,"ax",%progbits
	.align	2
	.type	BUDGETS_update_guivars, %function
BUDGETS_update_guivars:
.LFB3:
	@ args = 0, pretend = 0, frame = 156
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI6:
	fstmfdd	sp!, {d8}
.LCFI7:
	flds	s16, .L42
	ldr	r3, .L42+4
	sub	sp, sp, #164
.LCFI8:
	str	r3, [sp, #156]	@ float
	ldr	r3, .L42+8
	mov	r1, #400
	ldr	r2, .L42+12
	mov	r4, r0
	ldr	r0, [r3, #0]
	mov	r3, #190
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L42+16
	ldr	r6, .L42+20
	ldr	r0, [r3, #0]
	bl	POC_get_group_at_this_index
	bl	POC_get_GID_irrigation_system
	bl	SYSTEM_get_group_with_this_GID
	mov	r7, #0
	mov	r5, r0
	bl	NETWORK_CONFIG_get_water_units
	ldr	r2, .L42+24
	ldr	r3, .L42+28
	cmp	r0, #0
	movne	r1, #0
	strne	r1, [r2, #0]
	movne	r2, #1
	streq	r0, [r3, #0]
	strne	r2, [r3, #0]
	fldsne	s15, [sp, #156]
	ldr	r3, .L42+32
	moveq	r1, #1
	streq	r1, [r2, #0]
	str	r4, [r3, #0]
	mov	r1, #16
	mov	r3, #125
	str	r7, [sp, #0]
	add	r0, sp, #140
	ldr	r2, [r6, r4, asl #2]
	fdivsne	s16, s16, s15
	bl	GetDateStr
	mov	r2, #16
	mov	r1, r0
	ldr	r0, .L42+36
	bl	strlcpy
	add	r3, r4, #1
	str	r7, [sp, #0]
	mov	r1, #16
	ldr	r2, [r6, r3, asl #2]
	add	r0, sp, #140
	mov	r3, #125
	bl	GetDateStr
	mov	r2, #16
	mov	r1, r0
	ldr	r0, .L42+40
	bl	strlcpy
	ldr	r3, .L42+44
	add	r3, r3, r4, asl #2
	flds	s15, [r3, #0]
	ldr	r3, .L42+48
	fmuls	s15, s16, s15
	fsts	s15, [r3, #0]
	ldr	r3, .L42+52
	flds	s15, [r3, #0]
	ldr	r3, .L42+56
	fmuls	s15, s16, s15
	fsts	s15, [r3, #0]
	ldr	r3, .L42+60
	ldr	r3, [r3, #0]
	cmp	r3, r7
	beq	.L36
	cmp	r4, r7
	bne	.L37
	ldr	r6, .L42+64
	ldr	r2, .L42+12
	ldr	r0, [r6, #0]
	mov	r1, #400
	mov	r3, #229
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L42+68
	add	r1, sp, #160
	ldr	r0, [r3, #0]
	bl	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	ldr	r0, [sp, #160]
	bl	nm_BUDGET_get_used
	fcvtds	d7, s16
	ldr	r3, .L42+72
	fmdrr	d6, r0, r1
	ldr	r0, [r6, #0]
	fmuld	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L38
.L37:
	ldr	r3, .L42+72
	mov	r2, #0
	str	r2, [r3, #0]	@ float
.L38:
	mov	r0, r5
	add	r1, sp, #8
	bl	SYSTEM_get_budget_details
	cmp	r4, #0
	bne	.L39
	add	r0, sp, #120
	bl	EPSON_obtain_latest_complete_time_and_date
	mov	r0, r5
	bl	nm_SYSTEM_get_used
	mov	r6, r0
	mov	r0, r5
	bl	nm_GROUP_get_group_ID
	ldrh	r3, [sp, #124]
	add	r1, sp, #8
	strh	r3, [sp, #0]	@ movhi
	add	r2, sp, #120
	ldr	r3, [sp, #120]
	str	r4, [sp, #4]
	bl	nm_BUDGET_predicted_volume
	fmsr	s13, r6	@ int
	fuitos	s15, s13
	fmsr	s14, r0
	fadds	s15, s15, s14
	b	.L41
.L39:
	ldr	r3, .L42+20
	ldr	r1, [sp, #12]
	ldr	r0, [r3, r4, asl #2]
	add	r2, sp, #120
	bl	DateAndTimeToDTCS
	mov	r0, r5
	bl	nm_GROUP_get_group_ID
	mov	r3, #0
	str	r3, [sp, #4]
	ldrh	r3, [sp, #124]
	add	r1, sp, #8
	strh	r3, [sp, #0]	@ movhi
	add	r2, sp, #120
	ldr	r3, [sp, #120]
	bl	nm_BUDGET_predicted_volume
	fmsr	s15, r0
.L41:
	fmuls	s16, s16, s15
	ldr	r3, .L42+76
	ldr	r0, [sp, #116]
	fsts	s16, [r3, #0]
	bl	BUDGETS_show_warnings
.L36:
	ldr	r3, .L42+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #164
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L43:
	.align	2
.L42:
	.word	1065353216
	.word	1144718131
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	g_GROUP_list_item_index
	.word	.LANCHOR2
	.word	GuiVar_BudgetUnitGallon
	.word	GuiVar_BudgetUnitHCF
	.word	GuiVar_BudgetPeriodIdx
	.word	GuiVar_BudgetPeriodStart
	.word	GuiVar_BudgetPeriodEnd
	.word	.LANCHOR1
	.word	GuiVar_BudgetForThisPeriod
	.word	.LANCHOR0
	.word	GuiVar_BudgetAnnualValue
	.word	GuiVar_IsMaster
	.word	poc_preserves_recursive_MUTEX
	.word	g_GROUP_ID
	.word	GuiVar_BudgetUseThisPeriod
	.word	GuiVar_BudgetExpectedUseThisPeriod
.LFE3:
	.size	BUDGETS_update_guivars, .-BUDGETS_update_guivars
	.section	.text.FDTO_POC_close_budget_period_idx_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_close_budget_period_idx_dropdown, %function
FDTO_POC_close_budget_period_idx_dropdown:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, #0
	stmfd	sp!, {r4, lr}
.LCFI9:
	ldr	r4, .L45
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	str	r0, [r4, #0]
	bl	FDTO_COMBOBOX_hide
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	BUDGETS_update_guivars
.L46:
	.align	2
.L45:
	.word	GuiVar_BudgetPeriodIdx
.LFE13:
	.size	FDTO_POC_close_budget_period_idx_dropdown, .-FDTO_POC_close_budget_period_idx_dropdown
	.section	.text.BUDGET_SETUP_handle_units_change,"ax",%progbits
	.align	2
	.type	BUDGET_SETUP_handle_units_change, %function
BUDGET_SETUP_handle_units_change:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI10:
	bne	.L48
	ldr	r4, .L53
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L51
	mov	r0, r4
	bl	process_bool
	ldr	r2, [r4, #0]
	ldr	r3, .L53+4
	rsbs	r2, r2, #1
	movcc	r2, #0
	b	.L52
.L48:
	ldr	r4, .L53+4
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L51
	mov	r0, r4
	bl	process_bool
	ldr	r2, [r4, #0]
	ldr	r3, .L53
	rsbs	r2, r2, #1
	movcc	r2, #0
.L52:
	str	r2, [r3, #0]
.LBB9:
	ldr	r3, .L53
	ldr	r5, [r3, #0]
	subs	r5, r5, #1
	movne	r5, #1
	bl	FLOWSENSE_get_controller_index
	mov	r4, r0
	mov	r0, #2
	bl	NETWORK_CONFIG_get_change_bits_ptr
	mov	r1, #1
	mov	r2, #2
	mov	r3, r4
	str	r1, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, r5
	bl	NETWORK_CONFIG_set_water_units
	ldr	r3, .L53+8
	ldr	r0, [r3, #0]
	bl	BUDGETS_update_guivars
	mov	r0, #0
.LBE9:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
.LBB10:
	b	Redraw_Screen
.L51:
.LBE10:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	bad_key_beep
.L54:
	.align	2
.L53:
	.word	GuiVar_BudgetUnitGallon
	.word	GuiVar_BudgetUnitHCF
	.word	GuiVar_BudgetPeriodIdx
.LFE9:
	.size	BUDGET_SETUP_handle_units_change, .-BUDGET_SETUP_handle_units_change
	.section	.text.FDTO_BUDGETS_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_BUDGETS_draw_menu
	.type	FDTO_BUDGETS_draw_menu, %function
FDTO_BUDGETS_draw_menu:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L60
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI11:
	ldr	r2, .L60+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L60+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #1
	bl	POC_populate_pointers_of_POCs_for_display
	cmp	r4, #1
	ldr	r6, .L60+12
	mov	r5, r0
	bne	.L56
	ldr	r3, .L60+16
	mov	r4, #0
	str	r4, [r3, #0]
	ldr	r3, .L60+20
	mov	r0, r4
	str	r4, [r3, #0]
	str	r4, [r6, #0]
	bl	POC_get_ptr_to_physically_available_poc
	bl	POC_get_index_using_ptr_to_poc_struct
	ldr	r3, .L60+24
	mov	r1, r4
	str	r0, [r3, #0]
	bl	BUDGETS_copy_group_into_guivars
	mov	r0, r4
	bl	BUDGETS_update_guivars
	ldr	r3, .L60+28
	str	r4, [r3, #0]
	mvn	r4, #0
	b	.L57
.L56:
	ldr	r3, .L60+32
	mov	r0, #0
	ldrsh	r4, [r3, #0]
	bl	GuiLib_ScrollBox_GetTopLine
	str	r0, [r6, #0]
.L57:
	mov	r1, r4
	mov	r2, #1
	mov	r0, #9
	bl	GuiLib_ShowScreen
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	bl	POC_get_menu_index_for_displayed_poc
	ldr	r3, .L60+16
	mov	r2, r5, asl #16
	ldr	r3, [r3, #0]
	ldr	r1, .L60+12
	cmp	r3, #1
	mov	r2, r2, asr #16
	mov	r4, r0
	bne	.L58
	ldrsh	r3, [r1, #0]
	mov	r0, #0
	str	r3, [sp, #0]
	ldr	r1, .L60+36
	mvn	r3, #0
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	mov	r1, r4, asl #16
	mov	r1, r1, asr #16
	mov	r0, #0
	bl	GuiLib_ScrollBox_SetIndicator
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
	b	.L59
.L58:
	ldrsh	r1, [r1, #0]
	mov	r3, r0, asl #16
	str	r1, [sp, #0]
	mov	r0, #0
	ldr	r1, .L60+36
	mov	r3, r3, asr #16
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L59:
	ldr	r3, .L60
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_Refresh
.L61:
	.align	2
.L60:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	1203
	.word	g_POC_top_line
	.word	g_POC_editing_group
	.word	g_POC_current_list_item_index
	.word	g_GROUP_list_item_index
	.word	.LANCHOR3
	.word	GuiLib_ActiveCursorFieldNo
	.word	POC_load_poc_name_into_guivar
.LFE16:
	.size	FDTO_BUDGETS_draw_menu, .-FDTO_BUDGETS_draw_menu
	.section	.text.FDTO_BUDGETS_after_keypad,"ax",%progbits
	.align	2
	.type	FDTO_BUDGETS_after_keypad, %function
FDTO_BUDGETS_after_keypad:
.LFB11:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L68
	stmfd	sp!, {r0, r4, lr}
.LCFI12:
	str	r3, [sp, #0]	@ float
	ldr	r3, .L68+4
	mov	r4, r0
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldr	r3, .L68+8
	ldr	r3, [r3, #0]
	bne	.L63
	cmp	r3, #0
	ldr	r3, .L68+12
	fldseq	s15, [sp, #0]
	ldr	r1, .L68+16
	fldseq	s14, [r3, #0]
	ldr	r2, .L68+20
	ldrne	r1, [r1, #0]
	fmulseq	s15, s14, s15
	ldreq	r1, [r1, #0]
	ldrne	r3, [r3, #0]	@ float
	addne	r2, r2, r1, asl #2
	addeq	r2, r2, r1, asl #2
	strne	r3, [r2, #0]	@ float
	fstseq	s15, [r2, #0]
	b	.L65
.L63:
	ldr	r2, .L68+24
	cmp	r3, #0
	fldseq	s15, [sp, #0]
	fldseq	s14, [r2, #0]
	ldr	r3, .L68+28
	ldrne	r2, [r2, #0]	@ float
	fmulseq	s15, s14, s15
	strne	r2, [r3, #0]	@ float
	fstseq	s15, [r3, #0]
	bl	BUDGET_SETUP_update_montly_budget
.L65:
	mov	r0, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	FDTO_BUDGETS_draw_menu
.L69:
	.align	2
.L68:
	.word	1144718131
	.word	GuiVar_BudgetEntryOptionIdx
	.word	GuiVar_BudgetUnitGallon
	.word	GuiVar_BudgetForThisPeriod
	.word	GuiVar_BudgetPeriodIdx
	.word	.LANCHOR1
	.word	GuiVar_BudgetAnnualValue
	.word	.LANCHOR0
.LFE11:
	.size	FDTO_BUDGETS_after_keypad, .-FDTO_BUDGETS_after_keypad
	.section	.text.BUDGETS_process_menu,"ax",%progbits
	.align	2
	.global	BUDGETS_process_menu
	.type	BUDGETS_process_menu, %function
BUDGETS_process_menu:
.LFB17:
	@ args = 0, pretend = 0, frame = 160
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI13:
	fstmfdd	sp!, {d8}
.LCFI14:
	ldr	r6, .L173+12
	sub	sp, sp, #168
.LCFI15:
	ldr	r5, [r6, #0]
	mov	r4, r0
	cmp	r5, #1
	mov	r3, r1
	bne	.L71
.LBB26:
.LBB27:
	ldr	ip, .L173+16
	ldr	r2, .L173+20
	ldrsh	ip, [ip, #0]
	str	r2, [sp, #156]	@ float
	cmp	ip, #616
	beq	.L73
	ldr	r1, .L173+24
	cmp	ip, r1
	bne	.L153
	b	.L170
.L73:
	ldr	r2, .L173+28
	bl	NUMERIC_KEYPAD_process_key
	b	.L70
.L170:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L76
	bl	good_key_beep
	ldr	r3, .L173+32
	add	r0, sp, #120
	str	r5, [sp, #120]
	str	r3, [sp, #140]
	bl	Display_Post_Command
	b	.L70
.L76:
	bl	COMBO_BOX_key_press
	b	.L70
.L153:
	cmp	r0, #4
	beq	.L82
	bhi	.L86
	cmp	r0, #1
	beq	.L79
	bcc	.L78
	cmp	r0, #2
	beq	.L80
	cmp	r0, #3
	bne	.L148
	b	.L171
.L86:
	cmp	r0, #67
	beq	.L84
	bhi	.L87
	cmp	r0, #16
	beq	.L83
	cmp	r0, #20
	bne	.L148
	b	.L83
.L87:
	cmp	r0, #80
	beq	.L85
	cmp	r0, #84
	bne	.L148
	b	.L85
.L80:
	ldr	r3, .L173+116
	ldrsh	r3, [r3, #0]
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L98
.L93:
	.word	.L89
	.word	.L98
	.word	.L98
	.word	.L90
	.word	.L103
	.word	.L104
.L89:
	ldr	r3, .L173+108
	ldr	r4, [r3, #0]
	cmp	r4, #1
	beq	.L94
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #120]
	ldr	r3, .L173+36
	add	r0, sp, #120
	str	r3, [sp, #140]
	bl	Display_Post_Command
	b	.L112
.L94:
	bl	good_key_beep
	ldr	r3, .L173+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldrne	r0, .L173+44
	movne	r1, #0
	ldrne	r2, .L173+48
	bne	.L167
.L96:
	flds	s15, [sp, #156]
	flds	s14, .L173
	ldr	r0, .L173+44
	mov	r1, #0
	fdivs	s14, s14, s15
	mov	r3, r4
	fmrs	r2, s14
.L158:
	bl	NUMERIC_KEYPAD_draw_float_keypad
	b	.L112
.L90:
	bl	good_key_beep
	ldr	r3, .L173+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L97
	ldr	r0, .L173+52
	ldr	r2, .L173+56
	mov	r1, #0
.L167:
	mov	r3, #0
	b	.L158
.L97:
	flds	s15, [sp, #156]
	flds	s14, .L173+4
	ldr	r0, .L173+52
	mov	r1, #0
	fdivs	s14, s14, s15
	mov	r3, #1
	fmrs	r2, s14
	b	.L158
.L85:
	ldr	r1, .L173+116
	ldrsh	r1, [r1, #0]
	cmp	r1, #5
	ldrls	pc, [pc, r1, asl #2]
	b	.L98
.L105:
	.word	.L99
	.word	.L100
	.word	.L101
	.word	.L102
	.word	.L103
	.word	.L104
.L99:
	ldr	r1, .L173+108
	ldr	r1, [r1, #0]
	cmp	r1, #1
	bne	.L106
.LBB28:
	str	r2, [sp, #160]	@ float
	ldr	r2, .L173+60
	ldr	r2, [r2, #0]
	cmp	r2, #0
	fldsne	s15, [sp, #160]
	fldsne	s17, .L173
	fldsne	s16, .L173+8
	fdivsne	s17, s17, s15
	bne	.L108
.L107:
.LBB29:
	cmp	r3, #64
	ldrhi	r3, .L173+64
	bhi	.L109
	cmp	r3, #32
	movls	r3, #100
	movhi	r3, #500
.L109:
.LBE29:
	fmsr	s14, r3	@ int
	flds	s17, .L173
	mov	r1, #0
	fuitos	s16, s14
.L108:
	ldr	r3, .L173+40
	ldr	r6, [r3, #0]
	cmp	r6, #0
	beq	.L110
.LBB30:
	ldr	r5, .L173+68
	ldr	r0, [r5, #0]	@ float
	bl	__round_float
	ftouizs	s17, s17
	ftouizs	s16, s16
	add	r1, sp, #168
	mov	r2, #0
	fmrs	r3, s17	@ int
	fmsr	s15, r0
	mov	r0, r4
	ftouizs	s15, s15
	fstmdbs	r1!, {s15}	@ int
	fsts	s16, [sp, #0]	@ int
	str	r2, [sp, #4]
	bl	process_uns32_r
	flds	s14, [sp, #164]	@ int
	ldr	r3, .L173+44
	fuitos	s15, s14
	fsts	s15, [r5, #0]
	b	.L160
.L110:
.LBE30:
	ldr	r5, .L173+44
	ldr	r0, [r5, #0]	@ float
	bl	__round_float
	fmrs	r3, s17
	mov	r1, r5
	mov	r2, #0
	str	r0, [r5, #0]	@ float
	mov	r0, r4
	fsts	s16, [sp, #0]
	str	r6, [sp, #4]
	bl	process_fl
	flds	s15, [sp, #160]
	flds	s14, [r5, #0]
	ldr	r3, .L173+68
	fmuls	s15, s14, s15
.L160:
	fsts	s15, [r3, #0]
	bl	BUDGET_SETUP_update_montly_budget
	b	.L162
.L106:
.LBE28:
.LBB31:
	ldr	r3, .L173+128
	ldr	r5, .L173+124
	ldr	r0, [r3, #0]
	bl	POC_get_group_at_this_index
	bl	POC_get_GID_irrigation_system
	bl	SYSTEM_get_group_with_this_GID
	add	r1, sp, #8
	bl	SYSTEM_get_budget_details
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, [sp, #16]
	mov	r2, #0
	mov	r0, r4
	mov	r1, r5
	sub	r3, r3, #1
	str	r2, [sp, #4]
	bl	process_uns32
	ldr	r0, [r5, #0]
	bl	BUDGETS_update_guivars
	b	.L112
.L100:
.LBE31:
	ldr	r3, .L173+124
	mov	r0, r4
	ldr	r1, .L173+72
	ldr	r2, [r3, #0]
	b	.L163
.L101:
	ldr	r3, .L173+124
	ldr	r1, .L173+76
	ldr	r2, [r3, #0]
	mov	r0, r4
	add	r2, r2, #1
.L163:
	bl	BUDGET_SETUP_handle_start_date_change
	b	.L112
.L102:
.LBB32:
	str	r2, [sp, #160]	@ float
	ldr	r2, .L173+60
	ldr	r2, [r2, #0]
	cmp	r2, #0
	beq	.L113
	flds	s15, [sp, #160]
	flds	s17, .L173+4
	mov	r1, #1
	flds	s16, .L173+8
	fdivs	s17, s17, s15
	b	.L114
.L113:
.LBB33:
	cmp	r3, #48
	mov	r0, #0
	ldr	r1, .L173+80
	mov	r2, #0
	ldrhi	r3, .L173+84
	movls	r3, #1073741824
.L118:
	bl	pow
.LBE33:
	flds	s17, .L173+4
.LBB34:
	fmdrr	d7, r0, r1
.LBE34:
	mov	r1, #0
.LBB35:
	ftouizd	s16, d7
.LBE35:
	fuitos	s16, s16
.L114:
	ldr	r3, .L173+40
	ldr	r6, [r3, #0]
	cmp	r6, #0
	beq	.L119
.LBB36:
	ldr	r6, .L173+124
	ldr	r5, .L173+88
	ldr	r3, [r6, #0]
	add	r3, r5, r3, asl #2
	ldr	r0, [r3, #0]	@ float
	bl	__round_float
	ftouizs	s17, s17
	ftouizs	s16, s16
	add	r1, sp, #168
	mov	r2, #0
	fmrs	r3, s17	@ int
	fmsr	s15, r0
	mov	r0, r4
	ftouizs	s15, s15
	fstmdbs	r1!, {s15}	@ int
	fsts	s16, [sp, #0]	@ int
	str	r2, [sp, #4]
	bl	process_uns32_r
	flds	s14, [sp, #164]	@ int
	ldr	r3, [r6, #0]
	fuitos	s15, s14
	add	r5, r5, r3, asl #2
	ldr	r3, .L173+52
	fsts	s15, [r5, #0]
	b	.L161
.L119:
.LBE36:
	ldr	r5, .L173+52
	ldr	r0, [r5, #0]	@ float
	bl	__round_float
	mov	r2, #0
	fmrs	r3, s17
	mov	r1, r5
	str	r0, [r5, #0]	@ float
	mov	r0, r4
	fsts	s16, [sp, #0]
	str	r6, [sp, #4]
	bl	process_fl
	flds	s15, [sp, #160]
	flds	s14, [r5, #0]
	ldr	r3, .L173+124
	fmuls	s15, s14, s15
	ldr	r2, [r3, #0]
	ldr	r3, .L173+88
	add	r3, r3, r2, asl #2
.L161:
	fsts	s15, [r3, #0]
.L162:
	bl	BUDGETS_show_warnings
	b	.L112
.L103:
.LBE32:
	mov	r0, #4
	b	.L164
.L104:
	mov	r0, #5
.L164:
	bl	BUDGET_SETUP_handle_units_change
	b	.L112
.L98:
	bl	bad_key_beep
.L112:
	bl	Refresh_Screen
	b	.L70
.L83:
.LBB37:
	bl	POC_get_menu_index_for_displayed_poc
	cmp	r4, #20
	mov	r5, r0
	mov	r4, r5, asl #16
	mov	r0, #0
	mov	r4, r4, asr #16
	bne	.L121
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	mov	r1, r4
	bl	SCROLL_BOX_to_line
	cmp	r5, #11
	bhi	.L125
	ldr	r3, .L173+92
	add	r5, r5, #1
	ldr	r3, [r3, r5, asl #2]
	cmp	r3, #0
	movne	r0, #0
	movne	r1, r0
	bne	.L169
	b	.L126
.L121:
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	mov	r1, r4
	bl	SCROLL_BOX_to_line
	cmp	r5, #0
	beq	.L125
	ldr	r3, .L173+92
	sub	r5, r5, #1
	ldr	r3, [r3, r5, asl #2]
	cmp	r3, #0
	beq	.L126
	mov	r0, #0
	mov	r1, #4
.L169:
	bl	SCROLL_BOX_up_or_down
	b	.L126
.L174:
	.align	2
.L173:
	.word	1317997848
	.word	1287568416
	.word	1036831949
	.word	g_POC_editing_group
	.word	GuiLib_CurStructureNdx
	.word	1144718131
	.word	723
	.word	FDTO_BUDGETS_after_keypad
	.word	FDTO_POC_close_budget_period_idx_dropdown
	.word	FDTO_POC_show_budget_period_idx_dropdown
	.word	GuiVar_BudgetUnitGallon
	.word	GuiVar_BudgetAnnualValue
	.word	1317997848
	.word	GuiVar_BudgetForThisPeriod
	.word	1287568416
	.word	GuiVar_BudgetUnitHCF
	.word	5000
	.word	.LANCHOR0
	.word	GuiVar_BudgetPeriodStart
	.word	GuiVar_BudgetPeriodEnd
	.word	1076101120
	.word	1074266112
	.word	.LANCHOR1
	.word	POC_MENU_items
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	813
	.word	GuiVar_BudgetEntryOptionIdx
	.word	FDTO_BUDGETS_return_to_menu
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_POC_current_list_item_index
	.word	GuiVar_BudgetPeriodIdx
	.word	g_GROUP_list_item_index
	.word	.LANCHOR3
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.L125:
	bl	bad_key_beep
.L126:
	ldr	r4, .L173+92
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	ldr	r3, [r4, r5, asl #2]
	cmp	r3, #0
	beq	.L127
	bl	BUDGETS_extract_and_store_changes_from_GuiVars
	ldr	r0, [r4, r5, asl #2]
	bl	POC_get_index_using_ptr_to_poc_struct
	ldr	r5, .L173+128
	ldr	r4, .L173+96
	ldr	r6, .L173+124
	ldr	r2, .L173+100
	ldr	r3, .L173+104
	mov	r1, #400
	str	r0, [r5, #0]
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, #0]
	ldr	r1, [r6, #0]
	bl	BUDGETS_copy_group_into_guivars
	ldr	r0, [r6, #0]
	bl	BUDGETS_update_guivars
	ldr	r3, .L173+132
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	b	.L128
.L127:
	bl	bad_key_beep
.L128:
	mov	r0, #0
	b	.L165
.L82:
.LBE37:
	ldr	r4, .L173+116
	ldrsh	r3, [r4, #0]
	cmp	r3, #4
	beq	.L130
	cmp	r3, #5
	bne	.L154
	b	.L172
.L130:
	ldr	r3, .L173+108
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L154
	b	.L133
.L172:
	ldr	r3, .L173+108
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L133
	mov	r1, r5
	mov	r0, #3
	bl	FDTO_Cursor_Select
	b	.L70
.L133:
	bl	GuiLib_Cursor_Hide
	strh	r5, [r4, #0]	@ movhi
.L154:
	mov	r0, r5
	b	.L155
.L79:
	ldr	r5, .L173+116
	ldrsh	r3, [r5, #0]
	cmp	r3, #0
	beq	.L84
	cmp	r3, #4
	bne	.L155
	ldr	r3, .L173+108
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L155
	bl	GuiLib_Cursor_Hide
	mov	r0, r4
	strh	r4, [r5, #0]	@ movhi
.L155:
	bl	CURSOR_Up
	b	.L70
.L78:
	ldr	r3, .L173+116
	ldrsh	r2, [r3, #0]
	cmp	r2, #0
	bne	.L156
	ldr	r2, .L173+108
	ldr	r2, [r2, #0]
	cmp	r2, #0
	movne	r2, #3
	strneh	r2, [r3, #0]	@ movhi
.L156:
	mov	r0, r5
	b	.L166
.L171:
	ldr	r3, .L173+116
	ldrsh	r2, [r3, #0]
	cmp	r2, #0
	bne	.L157
	ldr	r2, .L173+108
	ldr	r2, [r2, #0]
	cmp	r2, #0
	movne	r2, #3
	strneh	r2, [r3, #0]	@ movhi
.L157:
	mov	r0, #1
.L166:
	bl	CURSOR_Down
	b	.L70
.L84:
	ldr	r0, .L173+112
	bl	KEY_process_BACK_from_editing_screen
	b	.L70
.L71:
.LBE27:
.LBE26:
	cmp	r0, #20
	bhi	.L144
	mov	r7, #1
	mov	r2, r7, asl r0
	tst	r2, #1114112
	bne	.L147
	ands	r5, r2, #17
	movne	r1, r0
	bne	.L145
	tst	r2, #12
	beq	.L144
	bl	good_key_beep
	mov	r1, r5
	mov	r0, r5
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L173+120
	str	r0, [r3, #0]
	bl	POC_get_ptr_to_physically_available_poc
	bl	POC_get_index_using_ptr_to_poc_struct
	ldr	r3, .L173+128
	str	r7, [r6, #0]
	str	r0, [r3, #0]
	ldr	r3, .L173+116
	mov	r0, r5
	strh	r5, [r3, #0]	@ movhi
	b	.L165
.L147:
	cmp	r0, #20
	movne	r1, #4
	moveq	r1, #0
.L145:
	mov	r0, #0
	bl	SCROLL_BOX_up_or_down
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L173+120
	ldr	r4, .L173+124
	str	r0, [r3, #0]
	bl	POC_get_ptr_to_physically_available_poc
	bl	POC_get_index_using_ptr_to_poc_struct
	ldr	r3, .L173+128
	ldr	r1, [r4, #0]
	str	r0, [r3, #0]
	bl	BUDGETS_copy_group_into_guivars
	ldr	r0, [r4, #0]
	bl	BUDGETS_update_guivars
	ldr	r3, .L173+132
	mov	r0, #0
	str	r0, [r3, #0]
.L165:
	bl	Redraw_Screen
	b	.L70
.L144:
	cmp	r4, #67
	bne	.L148
	ldr	r2, .L173+136
	ldr	r1, .L173+140
	ldr	r2, [r2, #0]
	mov	r0, #36
	mla	r2, r0, r2, r1
	ldr	r1, [r2, #4]
	ldr	r2, .L173+144
	str	r1, [r2, #0]
.L148:
	mov	r0, r4
	mov	r1, r3
	bl	KEY_process_global_keys
.L70:
	add	sp, sp, #168
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE17:
	.size	BUDGETS_process_menu, .-BUDGETS_process_menu
	.global	g_BUDGET_SETUP_annual_budget
	.global	g_BUDGET_SETUP_budget
	.global	g_BUDGET_SETUP_meter_read_date
	.section	.bss.g_BUDGET_SETUP_meter_read_date,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_BUDGET_SETUP_meter_read_date, %object
	.size	g_BUDGET_SETUP_meter_read_date, 96
g_BUDGET_SETUP_meter_read_date:
	.space	96
	.section	.bss.g_BUDGET_SETUP_budget,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_BUDGET_SETUP_budget, %object
	.size	g_BUDGET_SETUP_budget, 96
g_BUDGET_SETUP_budget:
	.space	96
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budgets.c\000"
	.section	.bss.date_change_warning_flag,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	date_change_warning_flag, %object
	.size	date_change_warning_flag, 4
date_change_warning_flag:
	.space	4
	.section	.bss.g_BUDGET_SETUP_annual_budget,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_BUDGET_SETUP_annual_budget, %object
	.size	g_BUDGET_SETUP_annual_budget, 4
g_BUDGET_SETUP_annual_budget:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI0-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI3-.LFB7
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0xd4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI6-.LFB3
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x1c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0xc0
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI9-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI10-.LFB9
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI11-.LFB16
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI12-.LFB11
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI13-.LFB17
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x1c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0xc4
	.align	2
.LEFDE20:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_budgets.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x13a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF18
	.byte	0x1
	.4byte	.LASF19
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x5c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x29d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x286
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.byte	0xa0
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x11c
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1f3
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST1
	.uleb128 0x6
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x4a4
	.4byte	.LFB15
	.4byte	.LFE15
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x36b
	.4byte	.LFB12
	.4byte	.LFE12
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.4byte	.LASF8
	.byte	0x1
	.byte	0xab
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x5
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x370
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST3
	.uleb128 0x8
	.4byte	0x29
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST4
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x4ad
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST5
	.uleb128 0x5
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x349
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST6
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x389
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x160
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x1a0
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.byte	0x72
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x2e3
	.byte	0x1
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x4ef
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST7
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB4
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI2
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB7
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI5
	.4byte	.LFE7
	.2byte	0x3
	.byte	0x7d
	.sleb128 212
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI8
	.4byte	.LFE3
	.2byte	0x3
	.byte	0x7d
	.sleb128 192
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB13
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB16
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB17
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI15
	.4byte	.LFE17
	.2byte	0x3
	.byte	0x7d
	.sleb128 196
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF15:
	.ascii	"BUDGETS_process_NEXT_and_PREV\000"
.LASF4:
	.ascii	"BUDGET_SETUP_update_montly_budget\000"
.LASF17:
	.ascii	"BUDGETS_process_menu\000"
.LASF1:
	.ascii	"BUDGET_SETUP_handle_units_change\000"
.LASF12:
	.ascii	"BUDGET_SETUP_handle_annual_budget_change\000"
.LASF8:
	.ascii	"BUDGETS_update_guivars\000"
.LASF7:
	.ascii	"FDTO_POC_show_budget_period_idx_dropdown\000"
.LASF19:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budgets.c\000"
.LASF6:
	.ascii	"FDTO_BUDGETS_return_to_menu\000"
.LASF18:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF14:
	.ascii	"POC_get_inc_by_for_monthly_budget\000"
.LASF3:
	.ascii	"BUDGETS_show_warnings\000"
.LASF11:
	.ascii	"BUDGETS_process_group\000"
.LASF16:
	.ascii	"FDTO_BUDGETS_draw_menu\000"
.LASF5:
	.ascii	"BUDGET_SETUP_handle_start_date_change\000"
.LASF9:
	.ascii	"FDTO_POC_close_budget_period_idx_dropdown\000"
.LASF10:
	.ascii	"FDTO_BUDGETS_after_keypad\000"
.LASF0:
	.ascii	"BUDGETS_get_inc_by_for_yearly_budget\000"
.LASF2:
	.ascii	"BUDGET_SETUP_handle_period_change\000"
.LASF13:
	.ascii	"BUDGET_SETUP_handle_budget_change\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
