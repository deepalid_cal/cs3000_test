	.file	"e_historical_et_setup.c"
	.text
.Ltext0:
	.section	.text.FDTO_HISTORICAL_ET_populate_city_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_populate_city_dropdown, %function
FDTO_HISTORICAL_ET_populate_city_dropdown:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI0:
	ldr	r7, .L4
	mov	r8, r0, asr #16
	mov	r4, #0
	ldr	r6, .L4+4
	ldr	r5, .L4+8
	b	.L2
.L3:
	ldr	r3, [r6, r8, asl #2]
	mov	r1, #76
	ldr	r0, .L4+12
	mla	r1, r3, r1, r5
	mov	r2, #49
	bl	strlcpy
	add	r4, r4, #1
.L2:
	ldr	r3, [r7, #0]
	cmp	r4, r3
	bcc	.L3
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L5:
	.align	2
.L4:
	.word	.LANCHOR1
	.word	.LANCHOR0
	.word	ET_PredefinedData
	.word	GuiVar_ComboBoxItemString
.LFE18:
	.size	FDTO_HISTORICAL_ET_populate_city_dropdown, .-FDTO_HISTORICAL_ET_populate_city_dropdown
	.section	.text.FDTO_HISTORICAL_ET_populate_county_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_populate_county_dropdown, %function
FDTO_HISTORICAL_ET_populate_county_dropdown:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI1:
	ldr	r7, .L9
	mov	r8, r0, asr #16
	mov	r4, #0
	ldr	r6, .L9+4
	ldr	r5, .L9+8
	b	.L7
.L8:
	ldr	r3, [r6, r8, asl #2]
	mov	r1, #28
	ldr	r0, .L9+12
	mla	r1, r3, r1, r5
	mov	r2, #49
	bl	strlcpy
	add	r4, r4, #1
.L7:
	ldr	r3, [r7, #0]
	cmp	r4, r3
	bcc	.L8
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L10:
	.align	2
.L9:
	.word	.LANCHOR3
	.word	.LANCHOR2
	.word	Counties
	.word	GuiVar_ComboBoxItemString
.LFE15:
	.size	FDTO_HISTORICAL_ET_populate_county_dropdown, .-FDTO_HISTORICAL_ET_populate_county_dropdown
	.section	.text.FDTO_HISTORICAL_ET_populate_state_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_populate_state_dropdown, %function
FDTO_HISTORICAL_ET_populate_state_dropdown:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI2:
	ldr	r7, .L14
	mov	r8, r0, asr #16
	mov	r4, #0
	ldr	r6, .L14+4
	ldr	r5, .L14+8
	b	.L12
.L13:
	ldr	r3, [r6, r8, asl #2]
	mov	r1, #24
	ldr	r0, .L14+12
	mla	r1, r3, r1, r5
	mov	r2, #49
	bl	strlcpy
	add	r4, r4, #1
.L12:
	ldr	r3, [r7, #0]
	cmp	r4, r3
	bcc	.L13
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L15:
	.align	2
.L14:
	.word	.LANCHOR5
	.word	.LANCHOR4
	.word	States
	.word	GuiVar_ComboBoxItemString
.LFE12:
	.size	FDTO_HISTORICAL_ET_populate_state_dropdown, .-FDTO_HISTORICAL_ET_populate_state_dropdown
	.section	.text.HISTORICAL_ET_qsort,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_qsort, %function
HISTORICAL_ET_qsort:
.LFB1:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI3:
	mov	r6, r0
	mov	r8, r1
.LBB10:
	mov	r7, #24
.LBE10:
	str	r2, [sp, #4]
.L26:
	ldr	r3, [sp, #4]
	cmp	r8, r3
	bge	.L16
.LBB11:
	mla	r3, r7, r8, r6
	ldr	r0, .L30
	mov	r1, r3
	mov	r2, #24
	str	r3, [sp, #0]
	add	r4, r8, #1
	bl	strlcpy
	mov	fp, r8
	ldr	r5, [sp, #4]
	b	.L28
.L20:
	add	r4, r4, #1
	cmp	r4, r5
	add	r8, r8, #24
	ble	.L25
	b	.L19
.L27:
	mla	sl, r7, r4, r6
	mov	r8, #0
.L25:
	add	r0, sl, r8
	ldr	r1, .L30
	mov	r2, #24
	bl	strncmp
	cmp	r0, #0
	ble	.L20
.L19:
	mov	r8, #0
	mla	sl, r7, r5, r6
	b	.L21
.L23:
	sub	r5, r5, #1
.L21:
	cmp	r4, r5
	bgt	.L28
	add	r9, sl, r8
	mov	r0, r9
	ldr	r1, .L30
	mov	r2, #24
	bl	strncmp
	sub	r8, r8, #24
	cmp	r0, #0
	bgt	.L23
	cmp	r4, r5
	bge	.L28
	mov	r3, #24
	mla	r8, r3, r4, r6
	mov	r2, r3
	mov	r1, r8
	ldr	r0, .L30+4
	bl	strlcpy
	mov	r1, r9
	mov	r2, #24
	mov	r0, r8
	bl	strlcpy
	mov	r0, r9
	ldr	r1, .L30+4
	mov	r2, #24
	bl	strlcpy
	add	r4, r4, #1
	sub	r5, r5, #1
.L28:
	cmp	r4, r5
	ble	.L27
	mla	r4, r7, r5, r6
	ldr	r1, [sp, #0]
	mov	r2, #24
	ldr	r0, .L30+4
	bl	strlcpy
	mov	r1, r4
	mov	r2, #24
	ldr	r0, [sp, #0]
	bl	strlcpy
	ldr	r1, .L30+4
	mov	r2, #24
	mov	r0, r4
	bl	strlcpy
.LBE11:
	mov	r0, r6
	mov	r1, fp
	sub	r2, r5, #1
	bl	HISTORICAL_ET_qsort
	add	r8, r5, #1
	b	.L26
.L16:
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L31:
	.align	2
.L30:
	.word	.LANCHOR6
	.word	.LANCHOR7
.LFE1:
	.size	HISTORICAL_ET_qsort, .-HISTORICAL_ET_qsort
	.section	.text.HISTORICAL_ET_sort_states,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_sort_states, %function
HISTORICAL_ET_sort_states:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI4:
	ldr	r7, .L40
	ldr	r6, .L40+4
	mov	r5, #0
.L33:
	add	r0, r7, r5
	add	r1, r6, r5
	mov	r2, #24
	add	r5, r5, #24
	bl	strlcpy
	cmp	r5, #312
	ldr	r4, .L40
	bne	.L33
	ldr	r7, .L40+8
	mov	r0, r4
	mov	r1, #0
	mov	r2, #12
	bl	HISTORICAL_ET_qsort
	mov	r3, #0
	str	r3, [r7, #0]
	add	r8, r4, #312
	b	.L34
.L36:
	mov	r1, r6
	mov	r0, r4
	mov	r2, #24
	bl	strncmp
	add	r6, r6, #24
	cmp	r0, #0
	ldreq	r3, [r7, #0]
	streq	r5, [sl, r3, asl #2]
	addeq	r3, r3, #1
	add	r5, r5, #1
	streq	r3, [r7, #0]
	cmp	r5, #13
	bne	.L36
	add	r4, r4, #24
	cmp	r4, r8
	beq	.L37
.L34:
	ldr	r6, .L40+4
	mov	r5, #0
	ldr	sl, .L40+12
	b	.L36
.L37:
	ldr	r2, .L40+16
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L40+20
	str	r3, [r2, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L41:
	.align	2
.L40:
	.word	.LANCHOR8
	.word	States
	.word	.LANCHOR5
	.word	.LANCHOR4
	.word	.LANCHOR9
	.word	.LANCHOR10
.LFE3:
	.size	HISTORICAL_ET_sort_states, .-HISTORICAL_ET_sort_states
	.section	.text.HISTORICAL_ET_sort_counties,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_sort_counties, %function
HISTORICAL_ET_sort_counties:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI5:
	ldr	r6, .L51
	ldr	sl, .L51+4
	ldr	r8, .L51+8
	mov	r5, #0
	mov	r4, r5
	mov	r7, r6
.L44:
	ldr	r2, [r6, #24]
	ldr	r3, [sl, #0]
	cmp	r2, r3
	bne	.L43
	mov	r2, #24
	mov	r1, #28
	mla	r0, r2, r4, r8
	mla	r1, r5, r1, r7
	bl	strlcpy
	add	r4, r4, #1
.L43:
	add	r5, r5, #1
	cmp	r5, #65
	add	r6, r6, #28
	bne	.L44
	ldr	r0, .L51+8
	mov	r1, #0
	sub	r2, r4, #1
	bl	HISTORICAL_ET_qsort
	ldr	r3, .L51+12
	mov	r6, #0
	str	r6, [r3, #0]
	ldr	r7, .L51+8
	ldr	fp, .L51
	ldr	r9, .L51+4
	b	.L45
.L47:
	mov	r1, #28
	mov	r0, r7
	mla	r1, r5, r1, fp
	mov	r2, #24
	bl	strncmp
	cmp	r0, #0
	bne	.L46
	ldr	r3, [r9, #0]
	ldr	r2, [r8, #24]
	cmp	r2, r3
	ldreq	r3, [sl, #0]
	ldreq	r2, .L51+16
	streq	r5, [r2, r3, asl #2]
	addeq	r3, r3, #1
	streq	r3, [sl, #0]
.L46:
	add	r5, r5, #1
	cmp	r5, #65
	add	r8, r8, #28
	bne	.L47
	add	r6, r6, #1
	add	r7, r7, #24
.L45:
	cmp	r6, r4
	ldrne	r8, .L51
	movne	r5, #0
	ldrne	sl, .L51+12
	bne	.L47
.L48:
	ldr	r3, .L51+20
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L51+24
	mov	r2, #0
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L52:
	.align	2
.L51:
	.word	Counties
	.word	.LANCHOR11
	.word	.LANCHOR12
	.word	.LANCHOR3
	.word	.LANCHOR2
	.word	.LANCHOR9
	.word	.LANCHOR10
.LFE4:
	.size	HISTORICAL_ET_sort_counties, .-HISTORICAL_ET_sort_counties
	.section	.text.HISTORICAL_ET_sort_cities,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_sort_cities, %function
HISTORICAL_ET_sort_cities:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI6:
	ldr	r6, .L62
	ldr	sl, .L62+4
	ldr	r8, .L62+8
	mov	r5, #0
	mov	r4, r5
	mov	r7, r6
.L55:
	ldr	r2, [r6, #24]
	ldr	r3, [sl, #0]
	cmp	r2, r3
	bne	.L54
	mov	r2, #24
	mov	r1, #76
	mla	r0, r2, r4, r8
	mla	r1, r5, r1, r7
	bl	strlcpy
	add	r4, r4, #1
.L54:
	add	r5, r5, #1
	cmp	r5, #154
	add	r6, r6, #76
	bne	.L55
	ldr	r0, .L62+8
	mov	r1, #0
	sub	r2, r4, #1
	bl	HISTORICAL_ET_qsort
	ldr	r3, .L62+12
	mov	r6, #0
	str	r6, [r3, #0]
	ldr	r7, .L62+8
	ldr	fp, .L62
	ldr	r9, .L62+4
	b	.L56
.L58:
	mov	r1, #76
	mov	r0, r7
	mla	r1, r5, r1, fp
	mov	r2, #24
	bl	strncmp
	cmp	r0, #0
	bne	.L57
	ldr	r3, [r9, #0]
	ldr	r2, [r8, #24]
	cmp	r2, r3
	ldreq	r3, [sl, #0]
	ldreq	r2, .L62+16
	streq	r5, [r2, r3, asl #2]
	addeq	r3, r3, #1
	streq	r3, [sl, #0]
.L57:
	add	r5, r5, #1
	cmp	r5, #154
	add	r8, r8, #76
	bne	.L58
	add	r6, r6, #1
	add	r7, r7, #24
.L56:
	cmp	r6, r4
	ldrne	r8, .L62
	movne	r5, #0
	ldrne	sl, .L62+12
	bne	.L58
.L59:
	ldr	r3, .L62+20
	mov	r2, #1
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L63:
	.align	2
.L62:
	.word	ET_PredefinedData
	.word	.LANCHOR13
	.word	.LANCHOR14
	.word	.LANCHOR1
	.word	.LANCHOR0
	.word	.LANCHOR10
.LFE5:
	.size	HISTORICAL_ET_sort_cities, .-HISTORICAL_ET_sort_cities
	.section	.text.HISTORICAL_ET_process_user_et,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_process_user_et, %function
HISTORICAL_ET_process_user_et:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L65
	stmfd	sp!, {r0, r1, lr}
.LCFI7:
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r2, #0
	ldr	r3, .L65+4
	bl	process_fl
	add	sp, sp, #8
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L66:
	.align	2
.L65:
	.word	1008981770
	.word	1101004800
.LFE10:
	.size	HISTORICAL_ET_process_user_et, .-HISTORICAL_ET_process_user_et
	.section	.text.FDTO_HISTORICAL_ET_show_city_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_show_city_dropdown, %function
FDTO_HISTORICAL_ET_show_city_dropdown:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI8:
	bl	HISTORICAL_ET_sort_cities
	ldr	r3, .L70
	ldr	r2, .L70+4
	ldr	r1, [r3, #0]
	mov	r3, #0
	b	.L68
.L69:
	add	r3, r3, #1
.L68:
	ldr	r0, [r2, #4]!
	cmp	r0, r1
	bne	.L69
	ldr	r2, .L70+8
	ldr	r0, .L70+12
	ldr	r1, .L70+16
	ldr	r2, [r2, #0]
	ldr	lr, [sp], #4
	b	FDTO_COMBOBOX_show
.L71:
	.align	2
.L70:
	.word	.LANCHOR15
	.word	.LANCHOR0-4
	.word	.LANCHOR1
	.word	730
	.word	FDTO_HISTORICAL_ET_populate_city_dropdown
.LFE19:
	.size	FDTO_HISTORICAL_ET_show_city_dropdown, .-FDTO_HISTORICAL_ET_show_city_dropdown
	.section	.text.FDTO_HISTORICAL_ET_show_county_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_show_county_dropdown, %function
FDTO_HISTORICAL_ET_show_county_dropdown:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI9:
	bl	HISTORICAL_ET_sort_counties
	ldr	r3, .L75
	ldr	r2, .L75+4
	ldr	r1, [r3, #0]
	mov	r3, #0
	b	.L73
.L74:
	add	r3, r3, #1
.L73:
	ldr	r0, [r2, #4]!
	cmp	r0, r1
	bne	.L74
	ldr	r2, .L75+8
	ldr	r0, .L75+12
	ldr	r1, .L75+16
	ldr	r2, [r2, #0]
	ldr	lr, [sp], #4
	b	FDTO_COMBOBOX_show
.L76:
	.align	2
.L75:
	.word	.LANCHOR13
	.word	.LANCHOR2-4
	.word	.LANCHOR3
	.word	731
	.word	FDTO_HISTORICAL_ET_populate_county_dropdown
.LFE16:
	.size	FDTO_HISTORICAL_ET_show_county_dropdown, .-FDTO_HISTORICAL_ET_show_county_dropdown
	.section	.text.FDTO_HISTORICAL_ET_show_state_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_show_state_dropdown, %function
FDTO_HISTORICAL_ET_show_state_dropdown:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI10:
	bl	HISTORICAL_ET_sort_states
	ldr	r3, .L80
	ldr	r2, .L80+4
	ldr	r1, [r3, #0]
	mov	r3, #0
	b	.L78
.L79:
	add	r3, r3, #1
.L78:
	ldr	r0, [r2, #4]!
	cmp	r0, r1
	bne	.L79
	ldr	r2, .L80+8
	ldr	r0, .L80+12
	ldr	r1, .L80+16
	ldr	r2, [r2, #0]
	ldr	lr, [sp], #4
	b	FDTO_COMBOBOX_show
.L81:
	.align	2
.L80:
	.word	.LANCHOR11
	.word	.LANCHOR4-4
	.word	.LANCHOR5
	.word	733
	.word	FDTO_HISTORICAL_ET_populate_state_dropdown
.LFE13:
	.size	FDTO_HISTORICAL_ET_show_state_dropdown, .-FDTO_HISTORICAL_ET_show_state_dropdown
	.section	.text.FDTO_HISTORICAL_ET_show_source_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_show_source_dropdown, %function
FDTO_HISTORICAL_ET_show_source_dropdown:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L83
	ldr	r1, .L83+4
	ldr	r3, [r3, #0]
	mov	r0, #732
	mov	r2, #2
	b	FDTO_COMBOBOX_show
.L84:
	.align	2
.L83:
	.word	GuiVar_HistoricalETUseYourOwn
	.word	FDTO_COMBOBOX_add_items
.LFE11:
	.size	FDTO_HISTORICAL_ET_show_source_dropdown, .-FDTO_HISTORICAL_ET_show_source_dropdown
	.section	.text.FDTO_HISTORICAL_ET_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_HISTORICAL_ET_draw_screen
	.type	FDTO_HISTORICAL_ET_draw_screen, %function
FDTO_HISTORICAL_ET_draw_screen:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L88
	str	lr, [sp, #-4]!
.LCFI11:
	ldrnesh	r1, [r3, #0]
	bne	.L87
	bl	WEATHER_copy_historical_et_settings_into_GuiVars
	mov	r1, #0
.L87:
	mov	r0, #27
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L89:
	.align	2
.L88:
	.word	GuiLib_ActiveCursorFieldNo
.LFE21:
	.size	FDTO_HISTORICAL_ET_draw_screen, .-FDTO_HISTORICAL_ET_draw_screen
	.section	.text.HISTORICAL_ET_update_et_numbers,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_update_et_numbers, %function
HISTORICAL_ET_update_et_numbers:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L92+8
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI12:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldmnefd	sp!, {r4, r5, r6, pc}
.LBB14:
	ldr	r3, .L92+12
	mov	r1, #24
	ldr	r2, [r3, #0]
	ldr	r3, .L92+16
	ldr	r0, .L92+20
	mla	r1, r2, r1, r3
	mov	r2, #25
	bl	strlcpy
	ldr	r3, .L92+24
	ldr	r6, .L92+28
	ldr	r2, [r3, #0]
	ldr	r3, .L92+32
	mov	r1, #28
	mla	r1, r2, r1, r3
	ldr	r0, .L92+36
	mov	r2, #25
	bl	strlcpy
	ldr	r4, .L92+40
	ldr	r1, [r6, #0]
	mov	r5, #76
	mla	r1, r5, r1, r4
	mov	r2, #25
	ldr	r0, .L92+44
	bl	strlcpy
	ldr	r3, [r6, #0]
	fldd	d7, .L92
	mla	r4, r5, r3, r4
	ldr	r3, .L92+48
	flds	s11, [r4, #28]	@ int
	fuitod	d6, s11
	flds	s11, [r4, #32]	@ int
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	flds	s11, [r4, #36]	@ int
	ldr	r3, .L92+52
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	flds	s11, [r4, #40]	@ int
	ldr	r3, .L92+56
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	flds	s11, [r4, #44]	@ int
	ldr	r3, .L92+60
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	flds	s11, [r4, #48]	@ int
	ldr	r3, .L92+64
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	flds	s11, [r4, #52]	@ int
	ldr	r3, .L92+68
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	flds	s11, [r4, #56]	@ int
	ldr	r3, .L92+72
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	flds	s11, [r4, #60]	@ int
	ldr	r3, .L92+76
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	flds	s11, [r4, #64]	@ int
	ldr	r3, .L92+80
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	flds	s11, [r4, #68]	@ int
	ldr	r3, .L92+84
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	ldr	r3, .L92+88
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	ldr	r3, [r4, #72]
	fmsr	s11, r3	@ int
	ldr	r3, .L92+92
	fuitod	d6, s11
	fdivd	d7, d6, d7
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
.LBE14:
	ldmfd	sp!, {r4, r5, r6, lr}
.LBB15:
	b	Refresh_Screen
.L93:
	.align	2
.L92:
	.word	0
	.word	1079574528
	.word	GuiVar_HistoricalETUseYourOwn
	.word	.LANCHOR11
	.word	States
	.word	GuiVar_HistoricalETState
	.word	.LANCHOR13
	.word	.LANCHOR15
	.word	Counties
	.word	GuiVar_HistoricalETCounty
	.word	ET_PredefinedData
	.word	GuiVar_HistoricalETCity
	.word	GuiVar_HistoricalET1
	.word	GuiVar_HistoricalET2
	.word	GuiVar_HistoricalET3
	.word	GuiVar_HistoricalET4
	.word	GuiVar_HistoricalET5
	.word	GuiVar_HistoricalET6
	.word	GuiVar_HistoricalET7
	.word	GuiVar_HistoricalET8
	.word	GuiVar_HistoricalET9
	.word	GuiVar_HistoricalET10
	.word	GuiVar_HistoricalET11
	.word	GuiVar_HistoricalET12
.LBE15:
.LFE2:
	.size	HISTORICAL_ET_update_et_numbers, .-HISTORICAL_ET_update_et_numbers
	.section	.text.FDTO_HISTORICAL_ET_close_city_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_close_city_dropdown, %function
FDTO_HISTORICAL_ET_close_city_dropdown:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, #0
	str	lr, [sp, #-4]!
.LCFI13:
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L95
	ldr	r2, [r3, r0, asl #2]
	ldr	r3, .L95+4
	str	r2, [r3, #0]
	bl	HISTORICAL_ET_update_et_numbers
	ldr	lr, [sp], #4
	b	FDTO_COMBOBOX_hide
.L96:
	.align	2
.L95:
	.word	.LANCHOR0
	.word	.LANCHOR15
.LFE20:
	.size	FDTO_HISTORICAL_ET_close_city_dropdown, .-FDTO_HISTORICAL_ET_close_city_dropdown
	.section	.text.FDTO_HISTORICAL_ET_close_county_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_close_county_dropdown, %function
FDTO_HISTORICAL_ET_close_county_dropdown:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI14:
	ldr	r4, .L99
	mov	r0, #0
	mov	r1, r0
	ldr	r6, [r4, #0]
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r5, .L99+4
	ldr	r3, [r5, r0, asl #2]
	cmp	r6, r3
	beq	.L98
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, [r5, r0, asl #2]
	str	r3, [r4, #0]
	bl	HISTORICAL_ET_sort_cities
	ldr	r3, .L99+8
	ldr	r2, [r3, #0]
	ldr	r3, .L99+12
	str	r2, [r3, #0]
	bl	HISTORICAL_ET_update_et_numbers
.L98:
	ldmfd	sp!, {r4, r5, r6, lr}
	b	FDTO_COMBOBOX_hide
.L100:
	.align	2
.L99:
	.word	.LANCHOR13
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR15
.LFE17:
	.size	FDTO_HISTORICAL_ET_close_county_dropdown, .-FDTO_HISTORICAL_ET_close_county_dropdown
	.section	.text.FDTO_HISTORICAL_ET_close_state_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HISTORICAL_ET_close_state_dropdown, %function
FDTO_HISTORICAL_ET_close_state_dropdown:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI15:
	ldr	r4, .L103
	mov	r0, #0
	mov	r1, r0
	ldr	r6, [r4, #0]
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r5, .L103+4
	ldr	r3, [r5, r0, asl #2]
	cmp	r6, r3
	beq	.L102
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, [r5, r0, asl #2]
	str	r3, [r4, #0]
	bl	HISTORICAL_ET_sort_counties
	ldr	r3, .L103+8
	ldr	r2, [r3, #0]
	ldr	r3, .L103+12
	str	r2, [r3, #0]
	bl	HISTORICAL_ET_sort_cities
	ldr	r3, .L103+16
	ldr	r2, [r3, #0]
	ldr	r3, .L103+20
	str	r2, [r3, #0]
	bl	HISTORICAL_ET_update_et_numbers
.L102:
	ldmfd	sp!, {r4, r5, r6, lr}
	b	FDTO_COMBOBOX_hide
.L104:
	.align	2
.L103:
	.word	.LANCHOR11
	.word	.LANCHOR4
	.word	.LANCHOR2
	.word	.LANCHOR13
	.word	.LANCHOR0
	.word	.LANCHOR15
.LFE14:
	.size	FDTO_HISTORICAL_ET_close_state_dropdown, .-FDTO_HISTORICAL_ET_close_state_dropdown
	.section	.text.unlikely.HISTORICAL_ET_process_reference_et_location.constprop.1,"ax",%progbits
	.align	2
	.type	HISTORICAL_ET_process_reference_et_location.constprop.1, %function
HISTORICAL_ET_process_reference_et_location.constprop.1:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI16:
	mov	r4, r1
	ldr	r1, [r1, #0]
	mov	r6, #0
	mov	r5, r2
	mov	r7, r3
	mov	r3, r2
	mov	r2, r6
.L107:
	ldr	ip, [r3], #4
	cmp	r1, ip
	moveq	r6, r2
	add	r2, r2, #1
	cmp	r2, r7
	bls	.L107
	cmp	r0, #80
	beq	.L109
	cmp	r0, #84
	bne	.L108
	bl	good_key_beep
	cmp	r6, r7
	moveq	r6, #0
	beq	.L108
	addcc	r6, r6, #1
	b	.L108
.L109:
	bl	good_key_beep
	cmp	r6, #0
	subne	r6, r6, #1
	moveq	r6, r7
.L108:
	ldr	r3, [r5, r6, asl #2]
	str	r3, [r4, #0]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE24:
	.size	HISTORICAL_ET_process_reference_et_location.constprop.1, .-HISTORICAL_ET_process_reference_et_location.constprop.1
	.section	.text.HISTORICAL_ET_process_screen,"ax",%progbits
	.align	2
	.global	HISTORICAL_ET_process_screen
	.type	HISTORICAL_ET_process_screen, %function
HISTORICAL_ET_process_screen:
.LFB22:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L199
	ldr	r2, .L199+4
	ldrsh	r3, [r3, #0]
	stmfd	sp!, {r4, r5, lr}
.LCFI17:
	cmp	r3, r2
	sub	sp, sp, #44
.LCFI18:
	mov	r4, r0
	mov	r5, r1
	beq	.L119
	bgt	.L122
	sub	r2, r2, #122
	cmp	r3, r2
	beq	.L117
	add	r2, r2, #121
	cmp	r3, r2
	bne	.L116
	b	.L195
.L122:
	cmp	r3, #732
	beq	.L120
	ldr	r2, .L199+8
	cmp	r3, r2
	bne	.L116
	b	.L196
.L117:
	cmp	r0, #67
	beq	.L123
	cmp	r0, #2
	bne	.L124
	ldr	r3, .L199+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L124
.L123:
	ldr	r3, .L199+16
	ldr	r2, .L199+12
	ldrh	r3, [r3, #0]
	strh	r3, [r2, #0]	@ movhi
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #1
	ldreq	r0, .L199+20
	beq	.L187
	cmp	r3, #2
	ldreq	r0, .L199+24
	beq	.L187
	cmp	r3, #3
	bne	.L124
	ldr	r0, .L199+28
.L187:
	ldr	r1, .L199+32
	mov	r2, #24
	bl	strlcpy
.L124:
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L199+36
	bl	KEYBOARD_process_key
	b	.L115
.L120:
	ldr	r1, .L199+40
	b	.L130
.L196:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L130
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L199+44
	b	.L188
.L119:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L130
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L199+48
.L188:
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L115
.L195:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L130
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L199+52
	b	.L188
.L130:
	bl	COMBO_BOX_key_press
	b	.L115
.L116:
	cmp	r4, #3
	beq	.L135
	bhi	.L138
	cmp	r4, #1
	beq	.L133
	ldr	r3, .L199+12
	bhi	.L134
	b	.L197
.L138:
	cmp	r4, #80
	beq	.L137
	cmp	r4, #84
	beq	.L137
	cmp	r4, #4
	bne	.L185
	b	.L198
.L134:
	ldrsh	r3, [r3, #0]
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L181
.L144:
	.word	.L140
	.word	.L141
	.word	.L142
	.word	.L143
.L140:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L199+56
	b	.L188
.L141:
	ldr	r3, .L199+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L145
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L199+60
	b	.L188
.L145:
	bl	good_key_beep
	ldr	r1, .L199+20
	mov	r2, #65
	ldr	r0, .L199+32
	bl	strlcpy
	ldr	r3, .L199+16
	mov	r2, #1
	str	r2, [r3, #0]
	mov	r0, #96
	mov	r1, #44
	b	.L193
.L142:
	ldr	r3, .L199+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L146
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L199+64
	b	.L188
.L146:
	bl	good_key_beep
	ldr	r1, .L199+24
	mov	r2, #65
	ldr	r0, .L199+32
	bl	strlcpy
	ldr	r3, .L199+16
	mov	r2, #2
	mov	r0, #96
	mov	r1, #58
	str	r2, [r3, #0]
.L193:
	mov	r2, #24
	mov	r3, #0
	bl	KEYBOARD_draw_keyboard
	b	.L115
.L143:
	ldr	r3, .L199+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L147
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L199+68
	b	.L188
.L147:
	bl	good_key_beep
	ldr	r1, .L199+28
	mov	r2, #65
	ldr	r0, .L199+32
	bl	strlcpy
	ldr	r3, .L199+16
	mov	r2, #3
	str	r2, [r3, #0]
	mov	r0, #96
	mov	r1, #72
	b	.L193
.L137:
	ldr	r3, .L199+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #16
	ldrls	pc, [pc, r3, asl #2]
	b	.L181
.L166:
	.word	.L149
	.word	.L150
	.word	.L151
	.word	.L152
	.word	.L153
	.word	.L154
	.word	.L155
	.word	.L156
	.word	.L157
	.word	.L158
	.word	.L159
	.word	.L160
	.word	.L161
	.word	.L162
	.word	.L163
	.word	.L164
	.word	.L165
.L149:
	ldr	r0, .L199+40
	bl	process_bool
	mov	r0, #0
	bl	Redraw_Screen
	b	.L115
.L150:
	ldr	r3, .L199+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L181
.LBB16:
	bl	HISTORICAL_ET_sort_states
	ldr	r3, .L199+72
	ldr	r2, .L199+76
	ldr	r3, [r3, #0]
	mov	r0, r4
	sub	r3, r3, #1
	ldr	r1, .L199+80
	bl	HISTORICAL_ET_process_reference_et_location.constprop.1
	bl	HISTORICAL_ET_sort_counties
	ldr	r3, .L199+84
	ldr	r2, [r3, #0]
	ldr	r3, .L199+88
	str	r2, [r3, #0]
	b	.L194
.L151:
.LBE16:
	ldr	r3, .L199+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L181
.LBB17:
	ldr	r3, .L199+92
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L169
	bl	HISTORICAL_ET_sort_counties
.L169:
	ldr	r3, .L199+96
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L181
.L170:
	mov	r0, r4
	ldr	r1, .L199+88
	ldr	r2, .L199+84
	sub	r3, r3, #1
	bl	HISTORICAL_ET_process_reference_et_location.constprop.1
.L194:
	bl	HISTORICAL_ET_sort_cities
	ldr	r3, .L199+100
	ldr	r2, [r3, #0]
	ldr	r3, .L199+104
	str	r2, [r3, #0]
	b	.L192
.L152:
.LBE17:
	ldr	r3, .L199+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L181
.LBB18:
	ldr	r3, .L199+108
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L172
	bl	HISTORICAL_ET_sort_cities
.L172:
	ldr	r3, .L199+112
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L181
.L173:
	mov	r0, r4
	ldr	r1, .L199+104
	ldr	r2, .L199+100
	sub	r3, r3, #1
	bl	HISTORICAL_ET_process_reference_et_location.constprop.1
.L192:
	bl	HISTORICAL_ET_update_et_numbers
	b	.L115
.L153:
.LBE18:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L199+116
	mov	r0, r4
	mov	r2, #10
	mov	r3, #500
	bl	process_uns32
	bl	Refresh_Screen
	b	.L115
.L154:
	mov	r0, r4
	ldr	r1, .L199+120
	b	.L191
.L155:
	mov	r0, r4
	ldr	r1, .L199+124
	b	.L191
.L156:
	mov	r0, r4
	ldr	r1, .L199+128
	b	.L191
.L157:
	mov	r0, r4
	ldr	r1, .L199+132
	b	.L191
.L158:
	mov	r0, r4
	ldr	r1, .L199+136
	b	.L191
.L159:
	mov	r0, r4
	ldr	r1, .L199+140
	b	.L191
.L160:
	mov	r0, r4
	ldr	r1, .L199+144
	b	.L191
.L161:
	mov	r0, r4
	ldr	r1, .L199+148
	b	.L191
.L162:
	mov	r0, r4
	ldr	r1, .L199+152
	b	.L191
.L163:
	mov	r0, r4
	ldr	r1, .L199+156
	b	.L191
.L164:
	mov	r0, r4
	ldr	r1, .L199+160
	b	.L191
.L165:
	ldr	r1, .L199+164
	mov	r0, r4
.L191:
	bl	HISTORICAL_ET_process_user_et
	b	.L115
.L198:
	ldr	r3, .L199+12
	ldrsh	r0, [r3, #0]
	cmp	r0, #10
	bgt	.L178
	cmp	r0, #5
	movge	r0, r4
	bge	.L190
	cmp	r0, #0
	beq	.L181
	b	.L174
.L178:
	cmp	r0, #16
	suble	r0, r0, #6
	ble	.L190
.L174:
	mov	r0, #1
	b	.L189
.L197:
	ldrsh	r0, [r3, #0]
	sub	r3, r0, #5
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L135
.L182:
	.word	.L180
	.word	.L180
	.word	.L180
	.word	.L180
	.word	.L180
	.word	.L180
	.word	.L181
	.word	.L181
	.word	.L181
	.word	.L181
	.word	.L181
	.word	.L181
.L180:
	add	r0, r0, #6
.L190:
	mov	r1, #1
	bl	CURSOR_Select
	b	.L115
.L181:
	bl	bad_key_beep
	b	.L115
.L133:
	mov	r0, r4
.L189:
	bl	CURSOR_Up
	b	.L115
.L135:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L115
.L185:
	cmp	r4, #67
	bne	.L183
	bl	WEATHER_extract_and_store_historical_et_changes_from_GuiVars
	ldr	r3, .L199+168
	mov	r2, #4
	str	r2, [r3, #0]
.L183:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L115:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L200:
	.align	2
.L199:
	.word	GuiLib_CurStructureNdx
	.word	731
	.word	733
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR16
	.word	GuiVar_HistoricalETUserState
	.word	GuiVar_HistoricalETUserCounty
	.word	GuiVar_HistoricalETUserCity
	.word	GuiVar_GroupName
	.word	FDTO_HISTORICAL_ET_draw_screen
	.word	GuiVar_HistoricalETUseYourOwn
	.word	FDTO_HISTORICAL_ET_close_state_dropdown
	.word	FDTO_HISTORICAL_ET_close_county_dropdown
	.word	FDTO_HISTORICAL_ET_close_city_dropdown
	.word	FDTO_HISTORICAL_ET_show_source_dropdown
	.word	FDTO_HISTORICAL_ET_show_state_dropdown
	.word	FDTO_HISTORICAL_ET_show_county_dropdown
	.word	FDTO_HISTORICAL_ET_show_city_dropdown
	.word	.LANCHOR5
	.word	.LANCHOR4
	.word	.LANCHOR11
	.word	.LANCHOR2
	.word	.LANCHOR13
	.word	.LANCHOR9
	.word	.LANCHOR3
	.word	.LANCHOR0
	.word	.LANCHOR15
	.word	.LANCHOR10
	.word	.LANCHOR1
	.word	GuiVar_ETGageMaxPercent
	.word	GuiVar_HistoricalETUser1
	.word	GuiVar_HistoricalETUser2
	.word	GuiVar_HistoricalETUser3
	.word	GuiVar_HistoricalETUser4
	.word	GuiVar_HistoricalETUser5
	.word	GuiVar_HistoricalETUser6
	.word	GuiVar_HistoricalETUser7
	.word	GuiVar_HistoricalETUser8
	.word	GuiVar_HistoricalETUser9
	.word	GuiVar_HistoricalETUser10
	.word	GuiVar_HistoricalETUser11
	.word	GuiVar_HistoricalETUser12
	.word	GuiVar_MenuScreenToShow
.LFE22:
	.size	HISTORICAL_ET_process_screen, .-HISTORICAL_ET_process_screen
	.global	g_HISTORICAL_ET_city_index
	.global	g_HISTORICAL_ET_county_index
	.global	g_HISTORICAL_ET_state_index
	.section	.bss.g_state_display_order,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_state_display_order, %object
	.size	g_state_display_order, 52
g_state_display_order:
	.space	52
	.section	.bss.swapArray.3824,"aw",%nobits
	.set	.LANCHOR7,. + 0
	.type	swapArray.3824, %object
	.size	swapArray.3824, 24
swapArray.3824:
	.space	24
	.section	.bss.g_cursor_pos_of_direct_entry_field,"aw",%nobits
	.align	2
	.set	.LANCHOR16,. + 0
	.type	g_cursor_pos_of_direct_entry_field, %object
	.size	g_cursor_pos_of_direct_entry_field, 4
g_cursor_pos_of_direct_entry_field:
	.space	4
	.section	.bss.g_city_display_order,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_city_display_order, %object
	.size	g_city_display_order, 616
g_city_display_order:
	.space	616
	.section	.bss.g_state_display_count,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	g_state_display_count, %object
	.size	g_state_display_count, 4
g_state_display_count:
	.space	4
	.section	.bss.g_counties_sorted,"aw",%nobits
	.align	2
	.set	.LANCHOR9,. + 0
	.type	g_counties_sorted, %object
	.size	g_counties_sorted, 4
g_counties_sorted:
	.space	4
	.section	.bss.g_city_display_count,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_city_display_count, %object
	.size	g_city_display_count, 4
g_city_display_count:
	.space	4
	.section	.bss.g_HISTORICAL_ET_city_index,"aw",%nobits
	.align	2
	.set	.LANCHOR15,. + 0
	.type	g_HISTORICAL_ET_city_index, %object
	.size	g_HISTORICAL_ET_city_index, 4
g_HISTORICAL_ET_city_index:
	.space	4
	.section	.bss.g_cities_sorted,"aw",%nobits
	.align	2
	.set	.LANCHOR10,. + 0
	.type	g_cities_sorted, %object
	.size	g_cities_sorted, 4
g_cities_sorted:
	.space	4
	.section	.bss.pivot.3825,"aw",%nobits
	.set	.LANCHOR6,. + 0
	.type	pivot.3825, %object
	.size	pivot.3825, 24
pivot.3825:
	.space	24
	.section	.bss.CitiesCopy.3880,"aw",%nobits
	.set	.LANCHOR14,. + 0
	.type	CitiesCopy.3880, %object
	.size	CitiesCopy.3880, 3696
CitiesCopy.3880:
	.space	3696
	.section	.bss.g_county_display_order,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_county_display_order, %object
	.size	g_county_display_order, 260
g_county_display_order:
	.space	260
	.section	.bss.StatesCopy.3849,"aw",%nobits
	.set	.LANCHOR8,. + 0
	.type	StatesCopy.3849, %object
	.size	StatesCopy.3849, 312
StatesCopy.3849:
	.space	312
	.section	.bss.g_HISTORICAL_ET_state_index,"aw",%nobits
	.align	2
	.set	.LANCHOR11,. + 0
	.type	g_HISTORICAL_ET_state_index, %object
	.size	g_HISTORICAL_ET_state_index, 4
g_HISTORICAL_ET_state_index:
	.space	4
	.section	.bss.CountiesCopy.3864,"aw",%nobits
	.set	.LANCHOR12,. + 0
	.type	CountiesCopy.3864, %object
	.size	CountiesCopy.3864, 480
CountiesCopy.3864:
	.space	480
	.section	.bss.g_HISTORICAL_ET_county_index,"aw",%nobits
	.align	2
	.set	.LANCHOR13,. + 0
	.type	g_HISTORICAL_ET_county_index, %object
	.size	g_HISTORICAL_ET_county_index, 4
g_HISTORICAL_ET_county_index:
	.space	4
	.section	.bss.g_county_display_count,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_county_display_count, %object
	.size	g_county_display_count, 4
g_county_display_count:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI0-.LFB18
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI1-.LFB15
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI2-.LFB12
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI5-.LFB4
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI6-.LFB5
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI7-.LFB10
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI8-.LFB19
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI9-.LFB16
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI10-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI11-.LFB21
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI12-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI13-.LFB20
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI14-.LFB17
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI15-.LFB14
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI16-.LFB24
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI17-.LFB22
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE36:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_historical_et_setup.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1c5
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF23
	.byte	0x1
	.4byte	.LASF24
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x94
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x14c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x15d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x174
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x116
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1e7
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1be
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x192
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST2
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.byte	0x5b
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF9
	.byte	0x1
	.byte	0x87
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST3
	.uleb128 0x5
	.4byte	.LASF10
	.byte	0x1
	.byte	0xad
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST4
	.uleb128 0x5
	.4byte	.LASF11
	.byte	0x1
	.byte	0xcb
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x5
	.4byte	.LASF12
	.byte	0x1
	.byte	0xf1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST6
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x187
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST7
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x1f1
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST8
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x1c8
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST9
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x19c
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST10
	.uleb128 0x6
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x18d
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x20b
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST11
	.uleb128 0x8
	.4byte	0x21
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST12
	.uleb128 0x4
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x201
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST13
	.uleb128 0x4
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x1d8
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST14
	.uleb128 0x4
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x1ac
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST15
	.uleb128 0x8
	.4byte	0x44
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST16
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x21e
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST17
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB18
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB15
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB12
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB10
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB19
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB16
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB13
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB21
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB2
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB20
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB17
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB24
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB22
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI18
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xac
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF24:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_historical_et_setup.c\000"
.LASF23:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF18:
	.ascii	"FDTO_HISTORICAL_ET_close_city_dropdown\000"
.LASF6:
	.ascii	"FDTO_HISTORICAL_ET_populate_county_dropdown\000"
.LASF22:
	.ascii	"HISTORICAL_ET_process_screen\000"
.LASF0:
	.ascii	"HISTORICAL_ET_update_et_numbers\000"
.LASF4:
	.ascii	"HISTORICAL_ET_process_reference_et_location\000"
.LASF14:
	.ascii	"FDTO_HISTORICAL_ET_show_city_dropdown\000"
.LASF21:
	.ascii	"FDTO_HISTORICAL_ET_draw_screen\000"
.LASF8:
	.ascii	"HISTORICAL_ET_qsort_partition\000"
.LASF10:
	.ascii	"HISTORICAL_ET_sort_states\000"
.LASF1:
	.ascii	"HISTORICAL_ET_process_state_index\000"
.LASF12:
	.ascii	"HISTORICAL_ET_sort_cities\000"
.LASF3:
	.ascii	"HISTORICAL_ET_process_city_index\000"
.LASF19:
	.ascii	"FDTO_HISTORICAL_ET_close_county_dropdown\000"
.LASF9:
	.ascii	"HISTORICAL_ET_qsort\000"
.LASF20:
	.ascii	"FDTO_HISTORICAL_ET_close_state_dropdown\000"
.LASF2:
	.ascii	"HISTORICAL_ET_process_county_index\000"
.LASF17:
	.ascii	"FDTO_HISTORICAL_ET_show_source_dropdown\000"
.LASF11:
	.ascii	"HISTORICAL_ET_sort_counties\000"
.LASF16:
	.ascii	"FDTO_HISTORICAL_ET_show_state_dropdown\000"
.LASF7:
	.ascii	"FDTO_HISTORICAL_ET_populate_state_dropdown\000"
.LASF15:
	.ascii	"FDTO_HISTORICAL_ET_show_county_dropdown\000"
.LASF13:
	.ascii	"HISTORICAL_ET_process_user_et\000"
.LASF5:
	.ascii	"FDTO_HISTORICAL_ET_populate_city_dropdown\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
