	.file	"device_GR_AIRLINK.c"
	.text
.Ltext0:
	.section	.text.__setup_for_crlf_delimited_string_hunt,"ax",%progbits
	.align	2
	.type	__setup_for_crlf_delimited_string_hunt, %function
__setup_for_crlf_delimited_string_hunt:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI0:
	ldr	r5, .L2
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r5, #368]
	mov	r2, #100
	bl	RCVD_DATA_enable_hunting_mode
	ldr	r3, .L2+4
	ldr	r2, .L2+8
	mov	r6, #1
	str	r6, [r2, r3]
	mov	r0, r4
	bl	strlen
	mov	r3, #2
	stmia	sp, {r3, r6}
	mov	r1, r4
	mov	r3, #0
	mov	r2, r0
	ldr	r0, [r5, #368]
	bl	AddCopyOfBlockToXmitList
	ldmfd	sp!, {r2, r3, r4, r5, r6, pc}
.L3:
	.align	2
.L2:
	.word	comm_mngr
	.word	12768
	.word	SerDrvrVars_s
.LFE0:
	.size	__setup_for_crlf_delimited_string_hunt, .-__setup_for_crlf_delimited_string_hunt
	.section	.text.process_read_state.constprop.3,"ax",%progbits
	.align	2
	.type	process_read_state.constprop.3, %function
process_read_state.constprop.3:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	mov	r4, r0
	mov	r6, r3
	ldr	r3, [r4, #0]
	mov	r0, r1
	cmp	r3, #4864
	mov	r5, r2
	bne	.L5
.LBB6:
	ldr	r1, [r4, #16]
	cmp	r1, #0
	beq	.L6
	mov	r2, #49
	bl	strlcpy
	ldr	r0, [r4, #16]
	ldr	r1, .L9
	mov	r2, #123
	bl	mem_free_debug
.L6:
	mov	r0, #4
	bl	vTaskDelay
	cmp	r5, #0
	beq	.L7
	mov	r0, r5
	bl	__setup_for_crlf_delimited_string_hunt
.L7:
	ldr	r3, .L9+4
	str	r6, [r3, #372]
.LBE6:
	ldmfd	sp!, {r4, r5, r6, pc}
.L5:
	ldr	r0, .L9+8
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	ldr	r3, .L9+4
	mov	r1, #100
	ldr	r0, [r3, #368]
	mov	r2, #0
	ldmfd	sp!, {r4, r5, r6, lr}
	b	RCVD_DATA_enable_hunting_mode
.L10:
	.align	2
.L9:
	.word	.LC0
	.word	comm_mngr
	.word	36866
.LFE8:
	.size	process_read_state.constprop.3, .-process_read_state.constprop.3
	.section	.text.GR_AIRLINK_initialize_device_exchange,"ax",%progbits
	.align	2
	.global	GR_AIRLINK_initialize_device_exchange
	.type	GR_AIRLINK_initialize_device_exchange, %function
GR_AIRLINK_initialize_device_exchange:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L12
	mov	r3, #0
	mvn	r1, #0
	stmfd	sp!, {r0, lr}
.LCFI2:
	str	r3, [r2, #372]
	str	r1, [sp, #0]
	mov	r1, #2
	ldr	r0, [r2, #384]
	mov	r2, #4
	bl	xTimerGenericCommand
	ldmfd	sp!, {r3, pc}
.L13:
	.align	2
.L12:
	.word	comm_mngr
.LFE1:
	.size	GR_AIRLINK_initialize_device_exchange, .-GR_AIRLINK_initialize_device_exchange
	.global	__udivsi3
	.section	.text.GR_AIRLINK_exchange_processing,"ax",%progbits
	.align	2
	.global	GR_AIRLINK_exchange_processing
	.type	GR_AIRLINK_exchange_processing, %function
GR_AIRLINK_exchange_processing:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI3:
	ldr	r5, .L56+16
	mvn	r3, #0
	mov	r2, #0
	str	r3, [sp, #0]
	mov	r4, r0
	mov	r3, r2
	ldr	r0, [r5, #384]
	mov	r1, #1
	bl	xTimerGenericCommand
	ldr	r3, [r5, #0]
	cmp	r3, #5
	bne	.L14
	ldr	r3, [r5, #364]
	cmp	r3, #4352
	bne	.L14
.LBB9:
	ldr	r3, [r5, #372]
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L46
.L29:
	.word	.L17
	.word	.L18
	.word	.L19
	.word	.L20
	.word	.L21
	.word	.L22
	.word	.L23
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
.L17:
	ldr	r3, [r4, #0]
	cmp	r3, #4864
	bne	.L30
	ldr	r0, .L56+20
	bl	Alert_Message
.L30:
	ldr	r0, .L56+24
	bl	__setup_for_crlf_delimited_string_hunt
	ldr	r3, .L56+16
	mov	r2, #1
	str	r2, [r3, #372]
	b	.L46
.L18:
	mov	r0, r4
	ldr	r1, .L56+28
	ldr	r2, .L56+32
	mov	r3, #2
	b	.L55
.L19:
	mov	r0, r4
	ldr	r1, .L56+36
	ldr	r2, .L56+40
	mov	r3, #3
	b	.L55
.L20:
	mov	r0, r4
	ldr	r1, .L56+44
	ldr	r2, .L56+48
	mov	r3, #4
	b	.L55
.L21:
	mov	r0, r4
	ldr	r1, .L56+52
	ldr	r2, .L56+56
	mov	r3, #5
	b	.L55
.L22:
	mov	r0, r4
	ldr	r1, .L56+60
	ldr	r2, .L56+64
	mov	r3, #6
	b	.L55
.L23:
	mov	r0, r4
	ldr	r1, .L56+68
	ldr	r2, .L56+72
	mov	r3, #7
	b	.L55
.L24:
	mov	r0, r4
	ldr	r1, .L56+76
	ldr	r2, .L56+80
	mov	r3, #8
	b	.L55
.L25:
	mov	r0, r4
	ldr	r1, .L56+84
	ldr	r2, .L56+88
	mov	r3, #9
	bl	process_read_state.constprop.3
	ldr	r0, .L56+84
	bl	atoi
	cmp	r0, #0
	beq	.L46
	cmn	r0, #70
	ldrge	r0, .L56+84
	ldrge	r1, .L56+92
	bge	.L54
.L31:
	cmn	r0, #80
	ldrge	r0, .L56+84
	ldrge	r1, .L56+96
	bge	.L54
.L32:
	cmn	r0, #85
	ldrge	r0, .L56+84
	ldrge	r1, .L56+100
	bge	.L54
.L33:
	cmn	r0, #95
	ldrge	r1, .L56+104
	ldr	r0, .L56+84
	ldrlt	r1, .L56+108
.L54:
	mov	r2, #49
	bl	strlcat
	b	.L46
.L26:
	ldr	r1, .L56+112
	ldr	r0, .L56+28
	mov	r2, #5
	bl	strncmp
	ldr	r1, .L56+116
	cmp	r0, #0
	mov	r0, r4
	bne	.L35
	ldr	r2, .L56+120
	mov	r3, #10
	bl	process_read_state.constprop.3
	mov	r4, #2000
	b	.L36
.L35:
	mov	r2, #0
	mov	r3, #10
	bl	process_read_state.constprop.3
	mov	r4, #20
.L36:
	ldr	r0, .L56+116
	bl	atof
	fmdrr	d7, r0, r1
	fcvtsd	s11, d7
	fcmpzs	s11
	fmstat
	beq	.L16
	flds	s15, .L56
	fcmpes	s11, s15
	fmstat
	ldrge	r0, .L56+116
	ldrge	r1, .L56+92
	bge	.L53
.L50:
	fcvtds	d6, s11
	fldd	d7, .L56+4
	fcmped	d6, d7
	fmstat
	ldrge	r0, .L56+116
	ldrge	r1, .L56+96
	bge	.L53
.L51:
	flds	s15, .L56+12
	ldr	r0, .L56+116
	fcmpes	s11, s15
	fmstat
	ldrge	r1, .L56+124
	ldrlt	r1, .L56+128
.L53:
	mov	r2, #49
	bl	strlcat
	b	.L16
.L27:
	ldr	r0, .L56+28
	ldr	r1, .L56+112
	mov	r2, #5
	bl	strncmp
	cmp	r0, #0
	beq	.L43
	ldr	r0, .L56+132
	ldr	r1, .L56+136
	mov	r2, #49
	bl	strncpy
	mov	r3, #4864
	str	r3, [r4, #0]
	mov	r3, #0
	str	r3, [r4, #16]
.L43:
	ldr	r1, .L56+132
	ldr	r2, .L56+140
	mov	r0, r4
	mov	r3, #11
.L55:
	bl	process_read_state.constprop.3
	b	.L46
.L28:
	ldr	r3, [r4, #0]
	cmp	r3, #4864
	bne	.L44
	ldr	r1, [r4, #16]
	mov	r2, #49
	ldr	r0, .L56+144
	bl	strlcpy
	ldr	r0, [r4, #16]
	ldr	r1, .L56+148
	ldr	r2, .L56+152
	bl	mem_free_debug
.L44:
	ldr	r3, .L56+16
	mov	r1, #100
	ldr	r0, [r3, #368]
	mov	r2, #0
	bl	RCVD_DATA_enable_hunting_mode
	ldr	r0, .L56+156
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
.L46:
	mov	r4, #2000
.L16:
	mov	r1, #5
	mov	r0, r4
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L56+16
	mov	r1, #2
	mov	r2, r0
	ldr	r0, [r3, #384]
	mov	r3, #0
	bl	xTimerGenericCommand
.L14:
.LBE9:
	ldmfd	sp!, {r3, r4, r5, pc}
.L57:
	.align	2
.L56:
	.word	-1077936128
	.word	-1717986918
	.word	-1072195175
	.word	-1055916032
	.word	comm_mngr
	.word	.LC1
	.word	.LC2
	.word	GuiVar_GRModel
	.word	.LC3
	.word	GuiVar_GRPhoneNumber
	.word	.LC4
	.word	GuiVar_GRIPAddress
	.word	.LC5
	.word	GuiVar_GRSIMID
	.word	.LC6
	.word	GuiVar_GRNetworkState
	.word	.LC7
	.word	GuiVar_GRCarrier
	.word	.LC8
	.word	GuiVar_GRService
	.word	.LC9
	.word	GuiVar_GRRSSI
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	GuiVar_GRECIO
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	GuiVar_GRRadioSerialNum
	.word	.LC20
	.word	.LC21
	.word	GuiVar_GRFirmwareVersion
	.word	.LC0
	.word	363
	.word	36865
.LFE5:
	.size	GR_AIRLINK_exchange_processing, .-GR_AIRLINK_exchange_processing
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_GR_AIRLINK.c\000"
.LC1:
	.ascii	"DEVEX: unexpected event\000"
.LC2:
	.ascii	"ati0\015\012\000"
.LC3:
	.ascii	"at*netphone?\015\012\000"
.LC4:
	.ascii	"at*netip?\015\012\000"
.LC5:
	.ascii	"at+iccid?\015\012\000"
.LC6:
	.ascii	"at*netstate?\015\012\000"
.LC7:
	.ascii	"at*netop?\015\012\000"
.LC8:
	.ascii	"at*netserv?\015\012\000"
.LC9:
	.ascii	"at*netrssi?\015\012\000"
.LC10:
	.ascii	"at+ecio?\015\012\000"
.LC11:
	.ascii	" (excellent)\000"
.LC12:
	.ascii	" (good)     \000"
.LC13:
	.ascii	" (fair)     \000"
.LC14:
	.ascii	" (weak)     \000"
.LC15:
	.ascii	" (very weak)\000"
.LC16:
	.ascii	"LS300\000"
.LC17:
	.ascii	"at*globalid?\015\012\000"
.LC18:
	.ascii	" (marginal) \000"
.LC19:
	.ascii	" (poor)     \000"
.LC20:
	.ascii	"(not avail on Raven X)\000"
.LC21:
	.ascii	"ati1\015\012\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI1-.LFB8
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI3-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_GR_AIRLINK.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x84
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x183
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x71
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x1
	.byte	0x3b
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	0x2a
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x58
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0xa0
	.byte	0x1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x199
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB8
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"GR_AIRLINK_initialize_device_exchange\000"
.LASF2:
	.ascii	"read_state_machine\000"
.LASF4:
	.ascii	"GR_AIRLINK_exchange_processing\000"
.LASF0:
	.ascii	"write_state_machine\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_GR_AIRLINK.c\000"
.LASF7:
	.ascii	"__setup_for_crlf_delimited_string_hunt\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"process_read_state\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
