	.file	"e_about.c"
	.text
.Ltext0:
	.section	.text.ABOUT_build_model_number,"ax",%progbits
	.align	2
	.type	ABOUT_build_model_number, %function
ABOUT_build_model_number:
.LFB3:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L32
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI0:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L2
	ldr	r3, .L32+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L32+8
	ldr	r3, .L32+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L32+16
	b	.L3
.L2:
	ldr	r3, .L32+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L32+8
	ldr	r3, .L32+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L32+28
	ldr	r2, .L32+32
	ldr	r3, [r3, #0]
	mov	r1, #92
	mla	r3, r1, r3, r2
.L3:
	mov	r2, #0
	mov	r4, r2
.L5:
	ldrb	r1, [r3, r2, asl #2]	@ zero_extendqisi2
	tst	r1, #1
	beq	.L4
	tst	r1, #2
	addne	r4, r4, #8
.L4:
	add	r2, r2, #1
	cmp	r2, #6
	bne	.L5
	ldr	r2, .L32+36
	ldr	r1, [r2, #0]
	ldr	r2, .L32+40
	adds	r1, r1, #0
	movne	r1, #1
	str	r1, [r2, #0]
	ldr	r2, .L32+44
	mov	r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #24]	@ zero_extendqisi2
	ldr	r2, .L32+48
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #24]	@ zero_extendqisi2
	ldr	r2, .L32+52
	mov	r1, r1, lsr #1
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldr	r1, [r3, #52]
	ldr	r2, .L32+56
	str	r1, [r2, #0]
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldr	r2, .L32+60
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldr	r2, .L32+64
	mov	r1, r1, lsr #1
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	ldr	r2, .L32+68
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	ldr	r2, .L32+72
	mov	r1, r1, lsr #1
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #8]	@ zero_extendqisi2
	ldr	r2, .L32+76
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #8]	@ zero_extendqisi2
	ldr	r2, .L32+80
	mov	r1, r1, lsr #1
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #12]	@ zero_extendqisi2
	ldr	r2, .L32+84
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #12]	@ zero_extendqisi2
	ldr	r2, .L32+88
	mov	r1, r1, lsr #1
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #16]	@ zero_extendqisi2
	ldr	r2, .L32+92
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #16]	@ zero_extendqisi2
	ldr	r2, .L32+96
	mov	r1, r1, lsr #1
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #20]	@ zero_extendqisi2
	ldr	r2, .L32+100
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #20]	@ zero_extendqisi2
	ldr	r2, .L32+104
	mov	r1, r1, lsr #1
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #28]	@ zero_extendqisi2
	ldr	r2, .L32+108
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r1, [r3, #28]	@ zero_extendqisi2
	ldr	r2, .L32+112
	mov	r1, r1, lsr #1
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldr	r1, [r3, #40]
	ldr	r2, .L32+116
	str	r1, [r2, #0]
	ldr	r1, [r3, #44]
	ldr	r2, .L32+120
	str	r1, [r2, #0]
	ldr	r1, [r3, #32]
	ldr	r2, .L32+124
	str	r1, [r2, #0]
	ldr	r2, [r3, #36]
	ldr	r3, .L32+128
	str	r2, [r3, #0]
	ldr	r3, .L32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldrne	r3, .L32+4
	ldreq	r3, .L32+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBB7:
	ldr	r3, .L32+132
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r0, .L32+136
	ldr	r1, .L32+140
	mov	r2, #49
	bl	strlcpy
	cmp	r4, #0
	beq	.L8
.LBB8:
	mov	r1, #8
	ldr	r2, .L32+144
	mov	r0, sp
	mov	r3, r4
	bl	snprintf
	ldr	r0, .L32+136
	mov	r1, sp
	mov	r2, #49
	bl	strlcat
.L8:
.LBE8:
	ldr	r3, .L32+56
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L9
	ldr	r0, .L32+136
	ldr	r1, .L32+148
	mov	r2, #49
	bl	strlcat
.L9:
	ldr	r3, .L32
	ldr	r2, .L32+152
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldrne	r3, .L32+156
	ldrne	r1, [r3, #80]
	strne	r1, [r2, #0]
	ldrne	r2, [r3, #84]
	bne	.L30
.L10:
	ldr	r3, .L32+28
	ldr	r1, .L32+160
	ldr	r3, [r3, #0]
	mov	r0, #92
	mla	r3, r0, r3, r1
	ldr	r1, [r3, #84]
	str	r1, [r2, #0]
	ldr	r2, [r3, #88]
.L30:
	ldr	r3, .L32+164
	str	r2, [r3, #0]
	ldr	r3, .L32+152
	ldr	r2, [r3, #0]
	ldr	r3, .L32+168
	adds	r2, r2, #0
	movne	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L32+164
	ldr	r2, [r3, #0]
	ldr	r3, .L32+172
	adds	r2, r2, #0
	movne	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L32+116
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L12
	ldr	r3, .L32+120
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L12
	ldr	r3, .L32+176
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r3, #1
	bne	.L13
	ldr	r3, .L32+180
	ldr	r3, [r3, #0]
	adds	r3, r3, #0
	movne	r3, #1
.L13:
	ldr	r2, .L32+184
	str	r3, [r2, #0]
	ldr	r3, .L32+188
	ldr	r2, [r3, #0]
	ldr	r3, .L32+192
	str	r2, [r3, #0]
	b	.L14
.L12:
	ldr	r2, .L32+184
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L32+192
	str	r3, [r2, #0]
.L14:
	ldr	r3, .L32+124
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L15
	ldr	r3, .L32+128
	ldr	r3, [r3, #0]
	adds	r3, r3, #0
	movne	r3, #1
.L15:
	ldr	r2, .L32+196
	str	r3, [r2, #0]
	ldr	r3, .L32+108
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L16
	ldr	r3, .L32+112
	ldr	r3, [r3, #0]
	adds	r3, r3, #0
	movne	r3, #1
.L16:
	ldr	r2, .L32+200
	str	r3, [r2, #0]
	ldr	r3, .L32+176
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldrne	r0, .L32+136
	ldrne	r1, .L32+204
	bne	.L31
.L17:
	ldr	r3, .L32+180
	ldr	r0, .L32+136
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldrne	r1, .L32+208
	ldreq	r1, .L32+212
.L31:
	mov	r2, #49
	bl	strlcat
	ldr	r3, .L32+152
	ldr	r3, [r3, #0]
	cmp	r3, #10
	bhi	.L20
	mov	r2, #1
	mov	r2, r2, asl r3
	ldr	r3, .L32+216
	and	r3, r2, r3
	cmp	r3, #0
	ldrne	r3, .L32+132
	ldrne	r2, [r3, #0]
	addne	r2, r2, #1
	strne	r2, [r3, #0]
.L20:
	ldr	r3, .L32+164
	ldr	r2, [r3, #0]
	ldr	r3, .L32+132
	cmp	r2, #10
	bhi	.L22
	mov	r1, #1
	mov	r1, r1, asl r2
	ldr	r2, .L32+216
	and	r2, r1, r2
	cmp	r2, #0
	ldrne	r2, [r3, #0]
	addne	r2, r2, #1
	strne	r2, [r3, #0]
.L22:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L1
	ldr	r0, .L32+136
	mov	r1, #49
	ldr	r2, .L32+220
	bl	sp_strlcat
.L1:
.LBE7:
	ldmfd	sp!, {r2, r3, r4, pc}
.L33:
	.align	2
.L32:
	.word	.LANCHOR0
	.word	tpmicro_data_recursive_MUTEX
	.word	.LC0
	.word	533
	.word	tpmicro_comm+116
	.word	chain_members_recursive_MUTEX
	.word	539
	.word	.LANCHOR1
	.word	chain+28
	.word	comm_mngr
	.word	GuiVar_AboutTPMicroCard
	.word	GuiVar_AboutTPFuseCard
	.word	GuiVar_AboutTPPOCCard
	.word	GuiVar_AboutTPPOCTerminal
	.word	GuiVar_AboutTP2WireTerminal
	.word	GuiVar_AboutTPSta01_08Card
	.word	GuiVar_AboutTPSta01_08Terminal
	.word	GuiVar_AboutTPSta09_16Card
	.word	GuiVar_AboutTPSta09_16Terminal
	.word	GuiVar_AboutTPSta17_24Card
	.word	GuiVar_AboutTPSta17_24Terminal
	.word	GuiVar_AboutTPSta25_32Card
	.word	GuiVar_AboutTPSta25_32Terminal
	.word	GuiVar_AboutTPSta33_40Card
	.word	GuiVar_AboutTPSta33_40Terminal
	.word	GuiVar_AboutTPSta41_48Card
	.word	GuiVar_AboutTPSta41_48Terminal
	.word	GuiVar_AboutTPDashLCard
	.word	GuiVar_AboutTPDashLTerminal
	.word	GuiVar_AboutTPDashMCard
	.word	GuiVar_AboutTPDashMTerminal
	.word	GuiVar_AboutTPDashWCard
	.word	GuiVar_AboutTPDashWTerminal
	.word	GuiVar_AboutAntennaHoles
	.word	GuiVar_AboutModelNumber
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	GuiVar_CommOptionPortAIndex
	.word	config_c
	.word	chain
	.word	GuiVar_CommOptionPortBIndex
	.word	GuiVar_CommOptionPortAInstalled
	.word	GuiVar_CommOptionPortBInstalled
	.word	GuiVar_AboutSSEOption
	.word	GuiVar_AboutSSEDOption
	.word	GuiVar_AboutMSSEOption
	.word	GuiVar_AboutWMOption
	.word	GuiVar_AboutMOption
	.word	GuiVar_AboutWOption
	.word	GuiVar_AboutLOption
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	1774
	.word	.LC7
.LFE3:
	.size	ABOUT_build_model_number, .-ABOUT_build_model_number
	.section	.text.ABOUT_copy_settings_into_guivars,"ax",%progbits
	.align	2
	.type	ABOUT_copy_settings_into_guivars, %function
ABOUT_copy_settings_into_guivars:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	ldr	r4, .L48
	mov	r1, #65
	ldr	r2, [r4, #0]
	ldr	r0, .L48+4
	bl	NETWORK_CONFIG_get_controller_name
	bl	NETWORK_CONFIG_get_network_id
	ldr	r3, .L48+8
	mov	r1, #49
	ldr	r2, .L48+12
	str	r0, [r3, #0]
	ldr	r3, .L48+16
	ldr	r0, .L48+20
	bl	snprintf
	ldr	r3, .L48+24
	ldr	r2, .L48+28
	ldr	r3, [r3, #8]
	ldr	r1, .L48+32
	str	r3, [r2, #0]
	ldr	r2, .L48+36
	ldr	r0, [r2, #0]
	ldr	r2, .L48+40
	add	r0, r0, r0, asl #2
	cmp	r0, r2
	ldr	r2, .L48+44
	bls	.L47
.L35:
	cmp	r3, #0
	beq	.L38
.L47:
	ldr	r3, .L48+48
	ldr	ip, [r3, #48]
	str	ip, [r2, #0]
	ldrb	r2, [r3, #52]	@ zero_extendqisi2
	and	ip, r2, #1
	str	ip, [r1, #0]
	ldr	ip, .L48+52
	mov	r1, r2, lsr #1
	and	r1, r1, #1
	str	r1, [ip, #0]
	ldr	ip, .L48+56
	cmp	r1, #0
	mov	r2, r2, lsr #2
	ldr	r1, .L48+60
	and	r2, r2, #1
	str	r2, [ip, #0]
	eoreq	r2, r2, #1
	movne	r2, #0
	str	r2, [r1, #0]
	ldrb	r1, [r3, #52]	@ zero_extendqisi2
	ldr	r2, .L48+64
	mov	r1, r1, lsr #3
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r2, [r3, #53]	@ zero_extendqisi2
	b	.L44
.L38:
	ldr	ip, [r4, #0]
	ldr	r4, .L48+68
	mov	r5, #92
	mla	r5, ip, r5, r4
	ldr	r6, [r5, #20]
	str	r6, [r2, #0]
	ldrb	r2, [r5, #24]	@ zero_extendqisi2
	and	r5, r2, #1
	str	r5, [r1, #0]
	ldr	r5, .L48+52
	mov	r1, r2, lsr #1
	and	r1, r1, #1
	str	r1, [r5, #0]
	ldr	r5, .L48+56
	mov	r2, r2, lsr #2
	and	r2, r2, #1
	cmp	r1, #0
	str	r2, [r5, #0]
	eoreq	r3, r2, #1
	ldr	r2, .L48+60
	str	r3, [r2, #0]
	mov	r3, #92
	mla	r3, ip, r3, r4
	ldr	r2, .L48+64
	ldrb	r1, [r3, #24]	@ zero_extendqisi2
	mov	r1, r1, lsr #3
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r2, [r3, #25]	@ zero_extendqisi2
.L44:
	ldr	r3, .L48+72
	mov	r2, r2, lsr #4
	and	r2, r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L48+40
	cmp	r0, r3
	ldr	r3, .L48+76
	movhi	r0, #0
	movls	r0, #1
	str	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	ABOUT_build_model_number
.L49:
	.align	2
.L48:
	.word	.LANCHOR1
	.word	GuiVar_GroupName
	.word	GuiVar_AboutNetworkID
	.word	.LC8
	.word	restart_info+16
	.word	GuiVar_AboutVersion
	.word	comm_mngr
	.word	.LANCHOR0
	.word	GuiVar_AboutFLOption
	.word	my_tick_count
	.word	9999
	.word	GuiVar_AboutSerialNumber
	.word	config_c
	.word	GuiVar_AboutSSEOption
	.word	GuiVar_AboutSSEDOption
	.word	GuiVar_AboutWMOption
	.word	GuiVar_AboutHubOption
	.word	chain
	.word	GuiVar_AboutAquaponicsOption
	.word	GuiVar_AboutDebug
.LFE4:
	.size	ABOUT_copy_settings_into_guivars, .-ABOUT_copy_settings_into_guivars
	.section	.text.ABOUT_process_purchased_option,"ax",%progbits
	.align	2
	.type	ABOUT_process_purchased_option, %function
ABOUT_process_purchased_option:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI2:
	bl	process_bool
	bl	ABOUT_build_model_number
	mov	r0, #0
	ldr	lr, [sp], #4
	b	Redraw_Screen
.LFE6:
	.size	ABOUT_process_purchased_option, .-ABOUT_process_purchased_option
	.section	.text.FDTO_ABOUT_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_ABOUT_draw_screen
	.type	FDTO_ABOUT_draw_screen, %function
FDTO_ABOUT_draw_screen:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L54
	stmfd	sp!, {r4, lr}
.LCFI3:
	ldrnesh	r1, [r3, #0]
	mov	r4, r0
	bne	.L53
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L54+4
	str	r0, [r3, #0]
	bl	ABOUT_copy_settings_into_guivars
	ldr	r3, .L54+8
	mov	r1, #0
	str	r4, [r3, #0]
.L53:
	mov	r0, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, lr}
	b	GuiLib_Refresh
.L55:
	.align	2
.L54:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR1
	.word	.LANCHOR2
.LFE5:
	.size	FDTO_ABOUT_draw_screen, .-FDTO_ABOUT_draw_screen
	.section	.text.ABOUT_store_changes,"ax",%progbits
	.align	2
	.global	ABOUT_store_changes
	.type	ABOUT_store_changes, %function
ABOUT_store_changes:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L67
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI4:
	ldr	r4, [r3, #0]
	sub	sp, sp, #40
.LCFI5:
	cmp	r4, #0
	mov	r6, r0
	mov	r5, r1
	beq	.L57
.LBB9:
	ldr	r2, .L67+4
	mov	r3, #1
	stmia	sp, {r2, r3}
	ldr	r3, .L67+8
	str	r0, [sp, #12]
	str	r3, [sp, #8]
	mov	r3, #0
	str	r3, [sp, #20]
	str	r3, [sp, #24]
	str	r3, [sp, #28]
	str	r3, [sp, #32]
	ldr	r3, .L67+12
	str	r1, [sp, #16]
	str	r3, [sp, #36]
	ldr	r3, .L67+16
	ldr	r0, .L67+20
	ldr	r1, [r3, #0]
	ldr	r3, .L67+24
	bl	SHARED_set_uint32_controller
.LBE9:
	ldr	r3, .L67+28
	ldr	r2, [r3, #0]
	ldr	r3, .L67+32
	ldrb	r1, [r3, #52]	@ zero_extendqisi2
.LBB10:
	sub	r0, r0, #1
	rsbs	r4, r0, #0
	adc	r4, r4, r0
.LBE10:
	and	r0, r1, #1
	cmp	r2, r0
	beq	.L58
	bic	r1, r1, #1
	cmp	r2, #0
	moveq	r2, r1
	orrne	r2, r1, #1
	add	r4, r4, #1
	strb	r2, [r3, #52]
.L58:
	ldr	r3, .L67+36
	ldr	r2, [r3, #0]
	ldr	r3, .L67+32
	ldrb	r1, [r3, #52]	@ zero_extendqisi2
	mov	r0, r1, lsr #1
	and	r0, r0, #1
	cmp	r2, r0
	bne	.L59
	ldr	r0, .L67+40
	mov	r1, r1, lsr #2
	ldr	r0, [r0, #0]
	and	r1, r1, #1
	cmp	r0, r1
	beq	.L60
.L59:
	ldr	r1, .L67+40
	ldrb	r0, [r3, #52]	@ zero_extendqisi2
	ldr	r1, [r1, #0]
	cmp	r2, #0
	movne	r2, #2
	moveq	r2, #0
	and	r0, r0, #249
	cmp	r1, #0
	orr	r2, r0, r2
	movne	r1, #4
	moveq	r1, #0
	orr	r2, r1, r2
	add	r4, r4, #2
	strb	r2, [r3, #52]
.L60:
	ldr	r7, .L67+32
	ldr	r8, .L67+44
	ldrb	r0, [r7, #52]	@ zero_extendqisi2
	ldr	r3, [r8, #0]
	mov	r0, r0, lsr #3
	and	r0, r0, #1
	cmp	r3, r0
	beq	.L61
	bl	GetNoYesStr
	add	r4, r4, #1
	mov	sl, r0
	ldr	r0, [r8, #0]
	bl	GetNoYesStr
	mov	r1, sl
	mov	r2, r0
	ldr	r0, .L67+48
	bl	Alert_Message_va
	ldr	r3, [r8, #0]
	ldrb	r2, [r7, #52]	@ zero_extendqisi2
	cmp	r3, #0
	bic	r2, r2, #8
	movne	r3, #8
	moveq	r3, #0
	orr	r3, r3, r2
	strb	r3, [r7, #52]
.L61:
	ldr	r7, .L67+32
	ldr	r8, .L67+52
	ldrb	r0, [r7, #53]	@ zero_extendqisi2
	ldr	r3, [r8, #0]
	mov	r0, r0, lsr #4
	and	r0, r0, #1
	cmp	r3, r0
	beq	.L62
	bl	GetNoYesStr
	add	r4, r4, #1
	mov	sl, r0
	ldr	r0, [r8, #0]
	bl	GetNoYesStr
	mov	r1, sl
	mov	r2, r0
	ldr	r0, .L67+56
	bl	Alert_Message_va
	ldr	r3, [r8, #0]
	ldrb	r2, [r7, #53]	@ zero_extendqisi2
	cmp	r3, #0
	bic	r2, r2, #16
	movne	r3, #16
	moveq	r3, #0
	orr	r3, r3, r2
	strb	r3, [r7, #53]
	b	.L63
.L62:
	cmp	r4, #0
	beq	.L57
.L63:
	mov	r0, #0
	mov	r1, r0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L57:
	bl	FLOWSENSE_get_controller_index
	mov	r7, #1
	mov	r8, r0
	mov	r0, #2
	bl	NETWORK_CONFIG_get_change_bits_ptr
	mov	r1, r8
	mov	r2, r7
	mov	r3, r6
	str	r5, [sp, #0]
	str	r7, [sp, #4]
	str	r0, [sp, #8]
	ldr	r0, .L67+60
	bl	NETWORK_CONFIG_set_controller_name
	add	r0, r0, r4
	cmp	r0, #0
	beq	.L64
	ldr	r4, .L67+64
	mov	r3, #239
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L67+68
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L67+72
	ldr	r0, [r4, #0]
	str	r7, [r3, #204]
	bl	xQueueGiveMutexRecursive
.L64:
	ldr	r3, .L67
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L56
	ldr	r3, .L67+76
	mov	r0, #2
	ldr	r4, [r3, #0]
	bl	NETWORK_CONFIG_get_change_bits_ptr
	mov	r1, #1
	mov	r2, r6
	mov	r3, r5
	str	r1, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, r4
	bl	NETWORK_CONFIG_set_network_id
.L56:
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L68:
	.align	2
.L67:
	.word	GuiVar_AboutDebug
	.word	50000
	.word	57348
	.word	.LC9
	.word	GuiVar_AboutSerialNumber
	.word	config_c+48
	.word	99999
	.word	GuiVar_AboutFLOption
	.word	config_c
	.word	GuiVar_AboutSSEOption
	.word	GuiVar_AboutSSEDOption
	.word	GuiVar_AboutHubOption
	.word	.LC10
	.word	GuiVar_AboutAquaponicsOption
	.word	.LC11
	.word	GuiVar_GroupName
	.word	irri_comm_recursive_MUTEX
	.word	.LC0
	.word	irri_comm
	.word	GuiVar_AboutNetworkID
.LFE1:
	.size	ABOUT_store_changes, .-ABOUT_store_changes
	.section	.text.ABOUT_process_screen,"ax",%progbits
	.align	2
	.global	ABOUT_process_screen
	.type	ABOUT_process_screen, %function
ABOUT_process_screen:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L164
	ldr	r2, .L164+4
	ldrsh	r3, [r3, #0]
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI6:
	cmp	r3, r2
	mov	r4, r0
	mov	r5, r1
	beq	.L71
	cmp	r3, #616
	bne	.L147
	b	.L161
.L71:
	ldr	r2, .L164+8
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	KEYBOARD_process_key
.L161:
	ldr	r2, .L164+8
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	NUMERIC_KEYPAD_process_key
.L147:
	cmp	r0, #4
	beq	.L78
	bhi	.L82
	cmp	r0, #1
	beq	.L75
	bcc	.L74
	cmp	r0, #2
	beq	.L76
	cmp	r0, #3
	bne	.L73
	b	.L162
.L82:
	cmp	r0, #80
	beq	.L80
	bhi	.L83
	cmp	r0, #16
	beq	.L79
	cmp	r0, #20
	bne	.L73
	b	.L79
.L83:
	cmp	r0, #81
	beq	.L81
	cmp	r0, #84
	bne	.L73
	b	.L80
.L79:
	ldr	r3, .L164+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L140
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L140
.L85:
	ldr	r3, .L164+16
	ldr	r0, .L164+20
	ldr	r2, [r3, #0]
	mov	r1, #92
	mov	r3, r2
.L91:
	cmp	r4, #20
	bne	.L86
	add	r3, r3, #1
	cmp	r3, #11
	movhi	r3, #0
	b	.L87
.L86:
	cmp	r3, #0
	subne	r3, r3, #1
	moveq	r3, #11
.L87:
	mla	ip, r1, r3, r0
	ldr	ip, [ip, #16]
	cmp	ip, #0
	beq	.L88
	ldr	r1, .L164+16
	cmp	r3, r2
	str	r3, [r1, #0]
	bne	.L89
	b	.L140
.L88:
	cmp	r3, r2
	bne	.L91
	ldr	r2, .L164+16
	str	r3, [r2, #0]
	b	.L140
.L89:
	bl	good_key_beep
	bl	ABOUT_copy_settings_into_guivars
	mov	r0, #0
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	Redraw_Screen
.L76:
	ldr	r3, .L164+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L140
.L101:
	.word	.L93
	.word	.L94
	.word	.L95
	.word	.L96
	.word	.L97
	.word	.L97
	.word	.L97
	.word	.L98
	.word	.L99
	.word	.L100
.L93:
	bl	good_key_beep
	mov	r0, #39
	mov	r1, #27
	mov	r2, #48
	mov	r3, #0
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	KEYBOARD_draw_keyboard
.L96:
	bl	good_key_beep
	ldr	r3, .L164+28
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L102
	ldr	r3, .L164+32
	ldr	r3, [r3, #0]
	cmp	r3, #1
	ldrne	r0, .L164+36
	bne	.L153
.L102:
	ldr	r0, .L164+40
.L153:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	DIALOG_draw_ok_dialog
.L94:
	bl	good_key_beep
	ldr	r0, .L164+44
	ldr	r1, .L164+48
	ldr	r2, .L164+52
	b	.L154
.L95:
	bl	good_key_beep
	ldr	r0, .L164+56
	mov	r1, #0
	mvn	r2, #0
.L154:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	NUMERIC_KEYPAD_draw_uint32_keypad
.L98:
	ldr	r0, .L164+60
	b	.L156
.L99:
	ldr	r0, .L164+64
.L156:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	ABOUT_process_purchased_option
.L100:
	ldr	r0, .L164+68
	b	.L156
.L97:
.LBB13:
	cmp	r3, #4
	bne	.L104
	ldr	r5, .L164+72
	ldr	r4, [r5, #0]
	cmp	r4, #0
	bne	.L109
	bl	good_key_beep
	mov	r3, #1
	str	r3, [r5, #0]
	ldr	r3, .L164+28
	b	.L150
.L104:
	cmp	r3, #5
	bne	.L107
	ldr	r5, .L164+28
	ldr	r4, [r5, #0]
	cmp	r4, #0
	bne	.L109
	bl	good_key_beep
	mov	r3, #1
	str	r3, [r5, #0]
	ldr	r3, .L164+72
.L150:
	str	r4, [r3, #0]
	ldr	r3, .L164+32
.L151:
	str	r4, [r3, #0]
	b	.L106
.L107:
	ldr	r5, .L164+32
	ldr	r4, [r5, #0]
	cmp	r4, #0
	bne	.L109
	bl	good_key_beep
	mov	r3, #1
	str	r3, [r5, #0]
	ldr	r3, .L164+72
	str	r4, [r3, #0]
	ldr	r3, .L164+28
	b	.L151
.L109:
	bl	bad_key_beep
.L106:
	bl	ABOUT_build_model_number
	b	.L115
.L81:
.LBE13:
	ldr	r3, .L164+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #2
	bne	.L148
	bl	good_key_beep
	ldr	r3, .L164+56
	mov	r2, #0
	str	r2, [r3, #0]
	bl	Refresh_Screen
	b	.L80
.L148:
	bl	KEY_process_global_keys
.L80:
	ldr	r3, .L164+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #1
	beq	.L113
	cmp	r3, #2
	bne	.L149
	b	.L163
.L113:
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L164+44
	ldr	r2, .L164+48
	ldr	r3, .L164+52
	b	.L152
.L163:
	ldr	r1, .L164+56
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r2, #0
	mov	r0, r4
	mvn	r3, #0
	str	r2, [sp, #4]
.L152:
	bl	process_uns32
	b	.L115
.L149:
	bl	bad_key_beep
.L115:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	Refresh_Screen
.L78:
	ldr	r3, .L164+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L123
	ldr	r3, .L164+24
	ldrsh	r3, [r3, #0]
	sub	r2, r3, #1
	cmp	r2, #7
	ldrls	pc, [pc, r2, asl #2]
	b	.L123
.L121:
	.word	.L118
	.word	.L118
	.word	.L123
	.word	.L119
	.word	.L123
	.word	.L123
	.word	.L119
	.word	.L138
.L118:
	ldr	r2, .L164+76
	mov	r0, #0
	str	r3, [r2, #0]
	b	.L155
.L119:
	ldr	r3, .L164+76
	ldr	r2, [r3, #0]
	cmp	r2, #3
	b	.L160
.L75:
	ldr	r3, .L164+24
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #5
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L123
.L128:
	.word	.L138
	.word	.L139
	.word	.L157
	.word	.L141
.L123:
	mov	r0, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Up
.L74:
	ldr	r3, .L164+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L137
	ldr	r3, .L164+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #8
	ldrls	pc, [pc, r3, asl #2]
	b	.L137
.L135:
	.word	.L131
	.word	.L132
	.word	.L132
	.word	.L137
	.word	.L137
	.word	.L137
	.word	.L140
	.word	.L139
	.word	.L140
.L131:
	ldr	r3, .L164+76
	ldr	r2, [r3, #0]
	cmp	r2, #2
.L160:
	movhi	r2, #1
	strhi	r2, [r3, #0]
	ldr	r0, [r3, #0]
	b	.L155
.L132:
	ldr	r2, .L164+76
	str	r3, [r2, #0]
.L157:
	mov	r0, #4
	b	.L155
.L162:
	ldr	r3, .L164+24
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #4
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L137
.L143:
	.word	.L138
	.word	.L139
	.word	.L140
	.word	.L141
	.word	.L142
.L138:
	mov	r0, #7
	b	.L155
.L139:
	mov	r0, #8
	b	.L155
.L141:
	mov	r0, #5
	b	.L155
.L142:
	mov	r0, #6
.L155:
	mov	r1, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Select
.L140:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	bad_key_beep
.L137:
	mov	r0, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Down
.L73:
	cmp	r4, #67
	bne	.L144
	bl	FLOWSENSE_get_controller_index
	mov	r1, r0
	mov	r0, #2
	bl	ABOUT_store_changes
	ldr	r3, .L164+80
	mov	r2, #11
	str	r2, [r3, #0]
.L144:
	mov	r0, r4
	mov	r1, r5
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	KEY_process_global_keys
.L165:
	.align	2
.L164:
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	FDTO_ABOUT_draw_screen
	.word	GuiVar_AboutDebug
	.word	.LANCHOR1
	.word	chain
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_AboutSSEOption
	.word	GuiVar_AboutSSEDOption
	.word	1639
	.word	1638
	.word	GuiVar_AboutSerialNumber
	.word	50000
	.word	99999
	.word	GuiVar_AboutNetworkID
	.word	GuiVar_AboutFLOption
	.word	GuiVar_AboutHubOption
	.word	GuiVar_AboutAquaponicsOption
	.word	GuiVar_AboutWMOption
	.word	.LANCHOR2
	.word	GuiVar_MenuScreenToShow
.LFE8:
	.size	ABOUT_process_screen, .-ABOUT_process_screen
	.section	.bss.g_ABOUT_controller_index,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_ABOUT_controller_index, %object
	.size	g_ABOUT_controller_index, 4
g_ABOUT_controller_index:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_about.c\000"
.LC1:
	.ascii	"CS3000\000"
.LC2:
	.ascii	"-%d\000"
.LC3:
	.ascii	"-2W\000"
.LC4:
	.ascii	"-S\000"
.LC5:
	.ascii	"-SD\000"
.LC6:
	.ascii	"-WM\000"
.LC7:
	.ascii	"%d\000"
.LC8:
	.ascii	"%s\000"
.LC9:
	.ascii	"Serial Number\000"
.LC10:
	.ascii	"Change: -HUB from %s to %s (keypad)\000"
.LC11:
	.ascii	"Change: -AQUAPONICS from %s to %s (keypad)\000"
	.section	.bss.g_ABOUT_prev_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_ABOUT_prev_cursor_pos, %object
	.size	g_ABOUT_prev_cursor_pos, 4
g_ABOUT_prev_cursor_pos:
	.space	4
	.section	.bss.g_ABOUT_chain_is_down,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_ABOUT_chain_is_down, %object
	.size	g_ABOUT_chain_is_down, 4
g_ABOUT_chain_is_down:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI2-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI3-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI4-.LFB1
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI6-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_about.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xb2
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF9
	.byte	0x1
	.4byte	.LASF10
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x73
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x118
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x206
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x27e
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x2f2
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x2d7
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xa5
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST4
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x301
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x340
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB6
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB1
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI5
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB8
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"ABOUT_copy_model_number_into_guivar\000"
.LASF8:
	.ascii	"ABOUT_process_screen\000"
.LASF6:
	.ascii	"ABOUT_store_changes\000"
.LASF0:
	.ascii	"ABOUT_set_serial_number\000"
.LASF9:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"FDTO_ABOUT_draw_screen\000"
.LASF3:
	.ascii	"ABOUT_copy_settings_into_guivars\000"
.LASF7:
	.ascii	"ABOUT_process_enclosure_radio_buttons\000"
.LASF4:
	.ascii	"ABOUT_process_purchased_option\000"
.LASF2:
	.ascii	"ABOUT_build_model_number\000"
.LASF10:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_about.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
