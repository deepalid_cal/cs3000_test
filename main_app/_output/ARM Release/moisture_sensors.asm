	.file	"moisture_sensors.c"
	.text
.Ltext0:
	.section	.text.nm_MOISTURE_SENSOR_set_decoder_serial_number,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_decoder_serial_number
	.type	nm_MOISTURE_SENSOR_set_decoder_serial_number, %function
nm_MOISTURE_SENSOR_set_decoder_serial_number:
.LFB2:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	sub	sp, sp, #44
.LCFI1:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	mov	r4, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r1, .L2
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	str	r2, [sp, #8]
	str	r3, [sp, #28]
	add	r3, r0, #80
	str	r3, [sp, #32]
	mov	r3, #2
	ldr	r2, .L2+4
	str	r3, [sp, #36]
	ldr	r3, .L2+8
	mov	ip, #0
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #92
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L3:
	.align	2
.L2:
	.word	2097151
	.word	49419
	.word	.LC0
.LFE2:
	.size	nm_MOISTURE_SENSOR_set_decoder_serial_number, .-nm_MOISTURE_SENSOR_set_decoder_serial_number
	.section	.text.nm_MOISTURE_SENSOR_set_physically_available,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_physically_available
	.type	nm_MOISTURE_SENSOR_set_physically_available, %function
nm_MOISTURE_SENSOR_set_physically_available:
.LFB3:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI2:
	sub	sp, sp, #36
.LCFI3:
	str	r2, [sp, #0]
	ldr	r2, .L5
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #96
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #80
	str	r3, [sp, #24]
	mov	r3, #3
	str	r3, [sp, #28]
	ldr	r3, .L5+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L6:
	.align	2
.L5:
	.word	49420
	.word	.LC1
.LFE3:
	.size	nm_MOISTURE_SENSOR_set_physically_available, .-nm_MOISTURE_SENSOR_set_physically_available
	.section	.text.nm_MOISTURE_SENSOR_set_name,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_name
	.type	nm_MOISTURE_SENSOR_set_name, %function
nm_MOISTURE_SENSOR_set_name:
.LFB0:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI4:
	ldr	ip, [sp, #24]
	str	ip, [sp, #0]
	ldr	ip, [sp, #28]
	str	ip, [sp, #4]
	ldr	ip, [sp, #32]
	str	ip, [sp, #8]
	add	ip, r0, #80
	str	ip, [sp, #12]
	mov	ip, #0
	str	ip, [sp, #16]
	bl	SHARED_set_name_32_bit_change_bits
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.LFE0:
	.size	nm_MOISTURE_SENSOR_set_name, .-nm_MOISTURE_SENSOR_set_name
	.section	.text.nm_MOISTURE_SENSOR_set_box_index,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_box_index
	.type	nm_MOISTURE_SENSOR_set_box_index, %function
nm_MOISTURE_SENSOR_set_box_index:
.LFB1:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI5:
	sub	sp, sp, #44
.LCFI6:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L9
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #80
	str	r3, [sp, #32]
	mov	r3, #1
	str	r3, [sp, #36]
	ldr	r3, .L9+4
	mov	ip, #0
	mov	r1, #11
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #88
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L10:
	.align	2
.L9:
	.word	49418
	.word	.LC2
.LFE1:
	.size	nm_MOISTURE_SENSOR_set_box_index, .-nm_MOISTURE_SENSOR_set_box_index
	.section	.text.nm_MOISTURE_SENSOR_set_in_use,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_in_use
	.type	nm_MOISTURE_SENSOR_set_in_use, %function
nm_MOISTURE_SENSOR_set_in_use:
.LFB4:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI7:
	sub	sp, sp, #36
.LCFI8:
	str	r2, [sp, #0]
	ldr	r2, .L12
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #100
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #80
	str	r3, [sp, #24]
	mov	r3, #4
	str	r3, [sp, #28]
	ldr	r3, .L12+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L13:
	.align	2
.L12:
	.word	49421
	.word	.LC3
.LFE4:
	.size	nm_MOISTURE_SENSOR_set_in_use, .-nm_MOISTURE_SENSOR_set_in_use
	.section	.text.nm_MOISTURE_SENSOR_set_moisture_control_mode,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_moisture_control_mode
	.type	nm_MOISTURE_SENSOR_set_moisture_control_mode, %function
nm_MOISTURE_SENSOR_set_moisture_control_mode:
.LFB5:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI9:
	sub	sp, sp, #44
.LCFI10:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #2
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	stmib	sp, {r1, r2}
	ldr	r2, .L15
	str	r3, [sp, #28]
	add	r3, r0, #80
	str	r3, [sp, #32]
	mov	r3, #5
	str	r3, [sp, #36]
	ldr	r3, .L15+4
	str	r1, [sp, #0]
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #104
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L16:
	.align	2
.L15:
	.word	49422
	.word	.LC4
.LFE5:
	.size	nm_MOISTURE_SENSOR_set_moisture_control_mode, .-nm_MOISTURE_SENSOR_set_moisture_control_mode
	.section	.text.nm_MOISTURE_SENSOR_set_low_temp_point,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_low_temp_point
	.type	nm_MOISTURE_SENSOR_set_low_temp_point, %function
nm_MOISTURE_SENSOR_set_low_temp_point:
.LFB6:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI11:
	sub	sp, sp, #44
.LCFI12:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #72
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #80
	mov	r1, #35
	str	r3, [sp, #32]
	mov	r3, #8
	stmib	sp, {r1, r2}
	ldr	r2, .L18
	str	r3, [sp, #36]
	ldr	r3, .L18+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #116
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L19:
	.align	2
.L18:
	.word	49425
	.word	.LC5
.LFE6:
	.size	nm_MOISTURE_SENSOR_set_low_temp_point, .-nm_MOISTURE_SENSOR_set_low_temp_point
	.section	.text.nm_MOISTURE_SENSOR_set_additional_soak_seconds,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_additional_soak_seconds
	.type	nm_MOISTURE_SENSOR_set_additional_soak_seconds, %function
nm_MOISTURE_SENSOR_set_additional_soak_seconds:
.LFB7:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI13:
	sub	sp, sp, #44
.LCFI14:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #3600
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #80
	mov	r1, #1200
	str	r3, [sp, #32]
	mov	r3, #9
	stmib	sp, {r1, r2}
	ldr	r2, .L21
	str	r3, [sp, #36]
	ldr	r3, .L21+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #120
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L22:
	.align	2
.L21:
	.word	49426
	.word	.LC6
.LFE7:
	.size	nm_MOISTURE_SENSOR_set_additional_soak_seconds, .-nm_MOISTURE_SENSOR_set_additional_soak_seconds
	.section	.text.nm_MOISTURE_SENSOR_set_moisture_high_set_point_float,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_moisture_high_set_point_float
	.type	nm_MOISTURE_SENSOR_set_moisture_high_set_point_float, %function
nm_MOISTURE_SENSOR_set_moisture_high_set_point_float:
.LFB8:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI15:
	sub	sp, sp, #44
.LCFI16:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1	@ float
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #1065353216
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]	@ float
	str	r3, [sp, #28]
	add	r3, r0, #80
	str	r3, [sp, #32]
	mov	r3, #10
	ldr	r1, .L24
	str	r2, [sp, #8]
	str	r3, [sp, #36]
	ldr	r2, .L24+4
	ldr	r3, .L24+8
	str	r1, [sp, #4]	@ float
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #204
	mov	r2, lr	@ float
	mov	r3, #0
	bl	SHARED_set_float_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L25:
	.align	2
.L24:
	.word	1050924810
	.word	49622
	.word	.LC7
.LFE8:
	.size	nm_MOISTURE_SENSOR_set_moisture_high_set_point_float, .-nm_MOISTURE_SENSOR_set_moisture_high_set_point_float
	.section	.text.nm_MOISTURE_SENSOR_set_moisture_low_set_point_float,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_moisture_low_set_point_float
	.type	nm_MOISTURE_SENSOR_set_moisture_low_set_point_float, %function
nm_MOISTURE_SENSOR_set_moisture_low_set_point_float:
.LFB9:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI17:
	sub	sp, sp, #44
.LCFI18:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1	@ float
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #1065353216
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]	@ float
	str	r3, [sp, #28]
	add	r3, r0, #80
	str	r3, [sp, #32]
	mov	r3, #11
	ldr	r1, .L27
	str	r2, [sp, #8]
	str	r3, [sp, #36]
	ldr	r2, .L27+4
	ldr	r3, .L27+8
	str	r1, [sp, #4]	@ float
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #208
	mov	r2, lr	@ float
	mov	r3, #0
	bl	SHARED_set_float_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L28:
	.align	2
.L27:
	.word	1050924810
	.word	49623
	.word	.LC8
.LFE9:
	.size	nm_MOISTURE_SENSOR_set_moisture_low_set_point_float, .-nm_MOISTURE_SENSOR_set_moisture_low_set_point_float
	.section	.text.nm_MOISTURE_SENSOR_set_high_temp_point,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_high_temp_point
	.type	nm_MOISTURE_SENSOR_set_high_temp_point, %function
nm_MOISTURE_SENSOR_set_high_temp_point:
.LFB10:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI19:
	sub	sp, sp, #44
.LCFI20:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #72
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #80
	mov	r1, #45
	str	r3, [sp, #32]
	mov	r3, #12
	stmib	sp, {r1, r2}
	ldr	r2, .L30
	str	r3, [sp, #36]
	ldr	r3, .L30+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #212
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_int32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L31:
	.align	2
.L30:
	.word	49624
	.word	.LC9
.LFE10:
	.size	nm_MOISTURE_SENSOR_set_high_temp_point, .-nm_MOISTURE_SENSOR_set_high_temp_point
	.section	.text.nm_MOISTURE_SENSOR_set_high_temp_action,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_high_temp_action
	.type	nm_MOISTURE_SENSOR_set_high_temp_action, %function
nm_MOISTURE_SENSOR_set_high_temp_action:
.LFB11:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI21:
	sub	sp, sp, #44
.LCFI22:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L33
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #80
	str	r3, [sp, #32]
	mov	r3, #13
	str	r3, [sp, #36]
	ldr	r3, .L33+4
	mov	ip, #0
	mov	r1, #1
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #216
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L34:
	.align	2
.L33:
	.word	49625
	.word	.LC10
.LFE11:
	.size	nm_MOISTURE_SENSOR_set_high_temp_action, .-nm_MOISTURE_SENSOR_set_high_temp_action
	.section	.text.nm_MOISTURE_SENSOR_set_low_temp_action,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_low_temp_action
	.type	nm_MOISTURE_SENSOR_set_low_temp_action, %function
nm_MOISTURE_SENSOR_set_low_temp_action:
.LFB12:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI23:
	sub	sp, sp, #44
.LCFI24:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L36
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #80
	str	r3, [sp, #32]
	mov	r3, #14
	str	r3, [sp, #36]
	ldr	r3, .L36+4
	mov	ip, #0
	mov	r1, #1
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #220
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L37:
	.align	2
.L36:
	.word	49626
	.word	.LC11
.LFE12:
	.size	nm_MOISTURE_SENSOR_set_low_temp_action, .-nm_MOISTURE_SENSOR_set_low_temp_action
	.section	.text.nm_MOISTURE_SENSOR_set_high_ec_point,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_high_ec_point
	.type	nm_MOISTURE_SENSOR_set_high_ec_point, %function
nm_MOISTURE_SENSOR_set_high_ec_point:
.LFB13:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI25:
	sub	sp, sp, #44
.LCFI26:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1	@ float
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	ldr	r1, .L39
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]	@ float
	str	r3, [sp, #28]
	add	r3, r0, #80
	str	r3, [sp, #32]
	mov	r3, #15
	ldr	r1, .L39+4
	str	r2, [sp, #8]
	str	r3, [sp, #36]
	ldr	r2, .L39+8
	ldr	r3, .L39+12
	str	r1, [sp, #4]	@ float
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #224
	mov	r2, lr	@ float
	mov	r3, #0
	bl	SHARED_set_float_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L40:
	.align	2
.L39:
	.word	1092616192
	.word	1086324736
	.word	49627
	.word	.LC12
.LFE13:
	.size	nm_MOISTURE_SENSOR_set_high_ec_point, .-nm_MOISTURE_SENSOR_set_high_ec_point
	.section	.text.nm_MOISTURE_SENSOR_set_low_ec_point,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_low_ec_point
	.type	nm_MOISTURE_SENSOR_set_low_ec_point, %function
nm_MOISTURE_SENSOR_set_low_ec_point:
.LFB14:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI27:
	sub	sp, sp, #44
.LCFI28:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1	@ float
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	ldr	r1, .L42
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r2, [sp, #8]
	str	r3, [sp, #28]
	add	r3, r0, #80
	str	r3, [sp, #32]
	mov	r3, #16
	ldr	r2, .L42+4
	str	r3, [sp, #36]
	ldr	r3, .L42+8
	str	r1, [sp, #0]	@ float
	mov	r1, #1065353216
	str	r1, [sp, #4]	@ float
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #228
	mov	r2, lr	@ float
	mov	r3, #0
	bl	SHARED_set_float_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L43:
	.align	2
.L42:
	.word	1092616192
	.word	49628
	.word	.LC13
.LFE14:
	.size	nm_MOISTURE_SENSOR_set_low_ec_point, .-nm_MOISTURE_SENSOR_set_low_ec_point
	.section	.text.nm_MOISTURE_SENSOR_set_high_ec_action,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_high_ec_action
	.type	nm_MOISTURE_SENSOR_set_high_ec_action, %function
nm_MOISTURE_SENSOR_set_high_ec_action:
.LFB15:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI29:
	sub	sp, sp, #44
.LCFI30:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L45
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #80
	str	r3, [sp, #32]
	mov	r3, #17
	str	r3, [sp, #36]
	ldr	r3, .L45+4
	mov	ip, #0
	mov	r1, #1
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #232
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L46:
	.align	2
.L45:
	.word	49629
	.word	.LC14
.LFE15:
	.size	nm_MOISTURE_SENSOR_set_high_ec_action, .-nm_MOISTURE_SENSOR_set_high_ec_action
	.section	.text.nm_MOISTURE_SENSOR_set_low_ec_action,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_low_ec_action
	.type	nm_MOISTURE_SENSOR_set_low_ec_action, %function
nm_MOISTURE_SENSOR_set_low_ec_action:
.LFB16:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI31:
	sub	sp, sp, #44
.LCFI32:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L48
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #80
	str	r3, [sp, #32]
	mov	r3, #18
	str	r3, [sp, #36]
	ldr	r3, .L48+4
	mov	ip, #0
	mov	r1, #1
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #236
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L49:
	.align	2
.L48:
	.word	49630
	.word	.LC15
.LFE16:
	.size	nm_MOISTURE_SENSOR_set_low_ec_action, .-nm_MOISTURE_SENSOR_set_low_ec_action
	.section	.text.nm_MOISTURE_SENSOR_set_default_values,"ax",%progbits
	.align	2
	.type	nm_MOISTURE_SENSOR_set_default_values, %function
nm_MOISTURE_SENSOR_set_default_values:
.LFB22:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, lr}
.LCFI33:
	mov	r4, r0
	mov	r7, r1
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #0
	mov	r6, r0
	beq	.L51
	ldr	r8, .L54
	add	r5, r4, #72
	mov	r0, r5
	ldr	r1, [r8, #0]
	bl	SHARED_clear_all_32_bit_change_bits
	add	r0, r4, #76
	ldr	r1, [r8, #0]
	bl	SHARED_clear_all_32_bit_change_bits
	add	r0, r4, #84
	ldr	r1, [r8, #0]
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r3, .L54+4
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L52
	add	r0, r4, #80
	ldr	r1, [r8, #0]
	bl	SHARED_set_all_32_bit_change_bits
.L52:
	mov	r0, r4
	mov	r1, r6
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_box_index
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_decoder_serial_number
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_physically_available
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_in_use
	mov	r0, r4
	mov	r1, #2
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_moisture_control_mode
	mov	r0, r4
	mov	r1, #35
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_low_temp_point
	mov	r0, r4
	mov	r1, #1200
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_additional_soak_seconds
	mov	r0, r4
	ldr	r1, .L54+8
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_moisture_high_set_point_float
	mov	r0, r4
	mov	r1, #0
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_moisture_low_set_point_float
	mov	r0, r4
	mov	r1, #45
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_high_temp_point
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_high_temp_action
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_low_temp_action
	mov	r0, r4
	ldr	r1, .L54+12
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_high_ec_point
	mov	r0, r4
	mov	r1, #1065353216
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_low_ec_point
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_high_ec_action
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r6, r7}
	str	r5, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_low_ec_action
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L51:
	ldr	r0, .L54+16
	ldr	r1, .L54+20
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	Alert_func_call_with_null_ptr_with_filename
.L55:
	.align	2
.L54:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LANCHOR0
	.word	1050924810
	.word	1086324736
	.word	.LC16
	.word	1046
.LFE22:
	.size	nm_MOISTURE_SENSOR_set_default_values, .-nm_MOISTURE_SENSOR_set_default_values
	.section	.text.nm_moisture_sensor_structure_updater,"ax",%progbits
	.align	2
	.type	nm_moisture_sensor_structure_updater, %function
nm_moisture_sensor_structure_updater:
.LFB19:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L80+4
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI34:
	sub	sp, sp, #20
.LCFI35:
	str	r3, [sp, #12]	@ float
	ldr	r3, .L80+8
	mov	r4, r0
	str	r3, [sp, #16]	@ float
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #4
	mov	r5, r0
	bne	.L57
	ldr	r0, .L80+12
	mov	r1, r4
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	Alert_Message_va
.L57:
	ldr	r0, .L80+16
	mov	r1, #4
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	bne	.L58
	ldr	r0, .L80+20
	bl	nm_ListGetFirst
	mov	r6, #72
	mov	r4, #10
	mov	r1, r0
	b	.L59
.L62:
	ldr	r3, [r1, #108]
	str	r4, [r1, #112]
	cmp	r3, #39
	fmsrls	s12, r3	@ int
	fldsls	s15, [sp, #12]
	strhi	r6, [r1, #108]
	ldr	r0, .L80+20
	fuitosls	s14, s12
	fmulsls	s15, s14, s15
	ftouizsls	s15, s15
	fmrsls	r3, s15	@ int
	strls	r3, [r1, #108]
	bl	nm_ListGetNext
	mov	r1, r0
.L59:
	cmp	r1, #0
	bne	.L62
	b	.L63
.L58:
	cmp	r4, #1
	bne	.L64
.L63:
	ldr	r0, .L80+20
	bl	nm_ListGetFirst
	mov	r6, #0
	mvn	r4, #0
	mov	r1, r0
	b	.L65
.L66:
	str	r6, [r1, #184]
	str	r4, [r1, #188]
	ldr	r0, .L80+20
	bl	nm_ListGetNext
	mov	r1, r0
.L65:
	cmp	r1, #0
	bne	.L66
	b	.L67
.L64:
	cmp	r4, #2
	bne	.L68
.L67:
	ldr	r0, .L80+20
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r4, r0
	b	.L69
.L71:
	ldr	r3, [r4, #108]
	add	r7, r4, #72
	sub	r2, r3, #10
	cmp	r2, #90
	fmsrls	s12, r3	@ int
	fldsls	s15, [sp, #12]
	fldsls	s14, [sp, #16]
	fldshi	s15, .L80
	fuitosls	s13, s12
	mov	r0, r4
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	fdivsls	s15, s13, s15
	fdivsls	s15, s15, s14
	fmrs	r1, s15
	bl	nm_MOISTURE_SENSOR_set_moisture_high_set_point_float
	mov	r0, r4
	mov	r1, #0
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_MOISTURE_SENSOR_set_moisture_low_set_point_float
	ldr	r0, .L80+20
	mov	r1, r4
	bl	nm_ListGetNext
	mov	r4, r0
.L69:
	cmp	r4, #0
	bne	.L71
	b	.L72
.L68:
	cmp	r4, #3
	bne	.L73
.L72:
	ldr	r0, .L80+20
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r4, r0
	b	.L74
.L75:
	add	r7, r4, #72
	mov	r0, r4
	mov	r1, #2
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_MOISTURE_SENSOR_set_moisture_control_mode
	mov	r0, r4
	mov	r1, #45
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_MOISTURE_SENSOR_set_high_temp_point
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_MOISTURE_SENSOR_set_high_temp_action
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_MOISTURE_SENSOR_set_low_temp_action
	ldr	r1, .L80+24
	mov	r0, r4
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_MOISTURE_SENSOR_set_high_ec_point
	mov	r0, r4
	mov	r1, #1065353216
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_MOISTURE_SENSOR_set_low_ec_point
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_MOISTURE_SENSOR_set_high_ec_action
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_MOISTURE_SENSOR_set_low_ec_action
	ldr	r0, .L80+20
	mov	r1, r4
	bl	nm_ListGetNext
	mov	r4, r0
.L74:
	cmp	r4, #0
	bne	.L75
	b	.L79
.L73:
	ldr	r0, .L80+28
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	Alert_Message
.L79:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L81:
	.align	2
.L80:
	.word	1050924810
	.word	1075419546
	.word	1120403456
	.word	.LC17
	.word	.LC18
	.word	.LANCHOR0
	.word	1086324736
	.word	.LC19
.LFE19:
	.size	nm_moisture_sensor_structure_updater, .-nm_moisture_sensor_structure_updater
	.section	.text.init_file_moisture_sensor,"ax",%progbits
	.align	2
	.global	init_file_moisture_sensor
	.type	init_file_moisture_sensor, %function
init_file_moisture_sensor:
.LFB20:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI36:
	ldr	r4, .L85
	sub	sp, sp, #28
.LCFI37:
	mov	r3, #0
	str	r3, [sp, #24]	@ float
	ldr	r3, .L85+4
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, [r4, #0]
	ldr	r1, .L85+8
	str	r3, [sp, #4]
	ldr	r3, .L85+12
	mov	r2, #4
	str	r3, [sp, #8]
	ldr	r3, .L85+16
	str	r3, [sp, #12]
	ldr	r3, .L85+20
	str	r3, [sp, #16]
	mov	r3, #18
	str	r3, [sp, #20]
	ldr	r3, .L85+24
	bl	FLASH_FILE_initialize_list_and_find_or_create_group_list_file
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L85+28
	mov	r3, #908
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L85+24
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L83
.L84:
	ldr	r3, [sp, #24]	@ float
	str	r4, [r1, #164]
	str	r3, [r1, #152]	@ float
	ldr	r3, [sp, #24]	@ float
	str	r4, [r1, #168]
	str	r4, [r1, #176]
	str	r4, [r1, #156]
	str	r3, [r1, #160]	@ float
	str	r4, [r1, #240]
	str	r4, [r1, #244]
	str	r4, [r1, #248]
	ldr	r0, .L85+24
	bl	nm_ListGetNext
	mov	r1, r0
.L83:
	cmp	r1, #0
	bne	.L84
	ldr	r3, .L85
	ldr	r0, [r3, #0]
	add	sp, sp, #28
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L86:
	.align	2
.L85:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LANCHOR2
	.word	.LANCHOR1
	.word	nm_moisture_sensor_structure_updater
	.word	nm_MOISTURE_SENSOR_set_default_values
	.word	.LANCHOR3
	.word	.LANCHOR0
	.word	.LC16
.LFE20:
	.size	init_file_moisture_sensor, .-init_file_moisture_sensor
	.section	.text.save_file_moisture_sensor,"ax",%progbits
	.align	2
	.global	save_file_moisture_sensor
	.type	save_file_moisture_sensor, %function
save_file_moisture_sensor:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #252
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI38:
	ldr	r1, .L88
	str	r3, [sp, #0]
	ldr	r3, .L88+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, #4
	str	r3, [sp, #4]
	mov	r3, #18
	str	r3, [sp, #8]
	ldr	r3, .L88+8
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L89:
	.align	2
.L88:
	.word	.LANCHOR1
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LANCHOR0
.LFE21:
	.size	save_file_moisture_sensor, .-save_file_moisture_sensor
	.section	.text.nm_MOISTURE_SENSOR_create_new_group,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_create_new_group
	.type	nm_MOISTURE_SENSOR_create_new_group, %function
nm_MOISTURE_SENSOR_create_new_group:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r2, #1
	mov	r3, #0
	stmfd	sp!, {r0, r1, lr}
.LCFI39:
	ldr	r1, .L91
	stmia	sp, {r2, r3}
	ldr	r0, .L91+4
	ldr	r2, .L91+8
	mov	r3, #252
	bl	nm_GROUP_create_new_group
	ldmfd	sp!, {r2, r3, pc}
.L92:
	.align	2
.L91:
	.word	.LANCHOR3
	.word	.LANCHOR0
	.word	nm_MOISTURE_SENSOR_set_default_values
.LFE23:
	.size	nm_MOISTURE_SENSOR_create_new_group, .-nm_MOISTURE_SENSOR_create_new_group
	.section	.text.MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct
	.type	MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct, %function
MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L98
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI40:
	ldr	r2, .L98+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L98+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L98+12
	bl	nm_ListGetFirst
	mov	r5, #0
	ldr	r6, .L98+12
	mov	r1, r0
	b	.L94
.L96:
	cmp	r1, r4
	beq	.L95
	ldr	r0, .L98+12
	bl	nm_ListGetNext
	add	r5, r5, #1
	mov	r1, r0
.L94:
	ldr	r3, [r6, #8]
	cmp	r5, r3
	bcc	.L96
	mov	r5, #0
.L95:
	ldr	r3, .L98
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L99:
	.align	2
.L98:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	1772
	.word	.LANCHOR0
.LFE30:
	.size	MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct, .-MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct
	.section	.text.nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number
	.type	nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number, %function
nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI41:
	mov	r5, r0
	ldr	r0, .L106
	mov	r4, r1
	bl	nm_ListGetFirst
	b	.L105
.L104:
	ldr	r3, [r1, #88]
	cmp	r3, r5
	bne	.L102
	ldr	r3, [r1, #92]
	cmp	r3, r4
	beq	.L103
.L102:
	ldr	r0, .L106
	bl	nm_ListGetNext
.L105:
	cmp	r0, #0
	mov	r1, r0
	bne	.L104
.L103:
	mov	r0, r1
	ldmfd	sp!, {r4, r5, pc}
.L107:
	.align	2
.L106:
	.word	.LANCHOR0
.LFE31:
	.size	nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number, .-nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number
	.section	.text.MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number
	.type	MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number, %function
MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number:
.LFB32:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI42:
	subs	r4, r0, #0
	moveq	r5, r4
	beq	.L109
	ldr	r3, .L115
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L115+4
	ldr	r3, .L115+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L115+12
	bl	nm_ListGetFirst
	b	.L114
.L112:
	ldr	r3, [r5, #92]
	cmp	r3, r4
	beq	.L111
	ldr	r0, .L115+12
	mov	r1, r5
	bl	nm_ListGetNext
.L114:
	cmp	r0, #0
	mov	r5, r0
	bne	.L112
.L111:
	ldr	r3, .L115
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L109:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L116:
	.align	2
.L115:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	1834
	.word	.LANCHOR0
.LFE32:
	.size	MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number, .-MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number
	.section	.text.MOISTURE_SENSOR_this_decoder_is_in_use_and_physically_available,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_this_decoder_is_in_use_and_physically_available
	.type	MOISTURE_SENSOR_this_decoder_is_in_use_and_physically_available, %function
MOISTURE_SENSOR_this_decoder_is_in_use_and_physically_available:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI43:
	ldr	r4, .L122
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L122+4
	ldr	r3, .L122+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number
	subs	r5, r0, #0
	beq	.L118
	ldr	r3, [r5, #100]
	cmp	r3, #0
	moveq	r5, r3
	beq	.L118
	ldr	r5, [r5, #96]
	adds	r5, r5, #0
	movne	r5, #1
.L118:
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L123:
	.align	2
.L122:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	1863
.LFE33:
	.size	MOISTURE_SENSOR_this_decoder_is_in_use_and_physically_available, .-MOISTURE_SENSOR_this_decoder_is_in_use_and_physically_available
	.section	.text.MOISTURE_SENSOR_get_group_at_this_index,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_group_at_this_index
	.type	MOISTURE_SENSOR_get_group_at_this_index, %function
MOISTURE_SENSOR_get_group_at_this_index:
.LFB34:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI44:
	ldr	r4, .L125
	ldr	r2, .L125+4
	mov	r3, #1904
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L125+8
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L126:
	.align	2
.L125:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	.LANCHOR0
.LFE34:
	.size	MOISTURE_SENSOR_get_group_at_this_index, .-MOISTURE_SENSOR_get_group_at_this_index
	.section	.text.nm_MOISTURE_SENSOR_load_group_name_into_guivar,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_load_group_name_into_guivar
	.type	nm_MOISTURE_SENSOR_load_group_name_into_guivar, %function
nm_MOISTURE_SENSOR_load_group_name_into_guivar:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, lr}
.LCFI45:
	mov	r0, r0, asr #16
	bl	MOISTURE_SENSOR_get_group_at_this_index
	mov	r4, r0
	mov	r1, r4
	ldr	r0, .L129
	bl	nm_OnList
	cmp	r0, #0
	beq	.L128
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L129+4
	ldmfd	sp!, {r4, lr}
	b	strlcpy
.L128:
	ldr	r0, .L129+8
	ldr	r1, .L129+12
	ldmfd	sp!, {r4, lr}
	b	Alert_group_not_found_with_filename
.L130:
	.align	2
.L129:
	.word	.LANCHOR0
	.word	GuiVar_itmGroupName
	.word	.LC16
	.word	1757
.LFE29:
	.size	nm_MOISTURE_SENSOR_load_group_name_into_guivar, .-nm_MOISTURE_SENSOR_load_group_name_into_guivar
	.section	.text.MOISTURE_SENSOR_copy_latest_readings_into_guivars,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_copy_latest_readings_into_guivars
	.type	MOISTURE_SENSOR_copy_latest_readings_into_guivars, %function
MOISTURE_SENSOR_copy_latest_readings_into_guivars:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI46:
	mov	r5, r0
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r4, .L133
	mov	r1, #400
	ldr	r2, .L133+4
	ldr	r3, .L133+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	MOISTURE_SENSOR_get_group_at_this_index
	ldr	r3, .L133+12
	ldr	r2, [r0, #152]	@ float
	str	r2, [r3, #0]	@ float
	ldr	r2, [r0, #156]
	ldr	r3, .L133+16
	str	r2, [r3, #0]
	ldr	r2, [r0, #160]	@ float
	ldr	r3, .L133+20
	ldr	r0, [r4, #0]
	str	r2, [r3, #0]	@ float
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L134:
	.align	2
.L133:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	1596
	.word	GuiVar_MoisReading_Current
	.word	GuiVar_MoisTemp_Current
	.word	GuiVar_MoisEC_Current
.LFE26:
	.size	MOISTURE_SENSOR_copy_latest_readings_into_guivars, .-MOISTURE_SENSOR_copy_latest_readings_into_guivars
	.section	.text.MOISTURE_SENSOR_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_copy_group_into_guivars
	.type	MOISTURE_SENSOR_copy_group_into_guivars, %function
MOISTURE_SENSOR_copy_group_into_guivars:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI47:
	ldr	r4, .L136
	mov	r1, #400
	ldr	r2, .L136+4
	ldr	r3, .L136+8
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	MOISTURE_SENSOR_get_group_at_this_index
	mov	r5, r0
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L136+12
	str	r0, [r3, #0]
	mov	r0, r5
	bl	nm_GROUP_get_name
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L136+16
	bl	strlcpy
	ldr	r2, [r5, #88]
	ldr	r3, .L136+20
	str	r2, [r3, #0]
	ldr	r2, [r5, #92]
	ldr	r3, .L136+24
	str	r2, [r3, #0]
	ldr	r2, [r5, #100]
	ldr	r3, .L136+28
	str	r2, [r3, #0]
	ldr	r2, [r5, #96]
	ldr	r3, .L136+32
	str	r2, [r3, #0]
	ldr	r2, [r5, #104]
	ldr	r3, .L136+36
	str	r2, [r3, #0]
	ldr	r2, [r5, #204]	@ float
	ldr	r3, .L136+40
	str	r2, [r3, #0]	@ float
	ldr	r2, [r5, #208]	@ float
	ldr	r3, .L136+44
	str	r2, [r3, #0]	@ float
	ldr	r2, [r5, #120]
	ldr	r3, .L136+48
	str	r2, [r3, #0]
	ldr	r2, [r5, #212]
	ldr	r3, .L136+52
	str	r2, [r3, #0]
	ldr	r2, [r5, #116]
	ldr	r3, .L136+56
	str	r2, [r3, #0]
	ldr	r2, [r5, #216]
	ldr	r3, .L136+60
	str	r2, [r3, #0]
	ldr	r2, [r5, #220]
	ldr	r3, .L136+64
	str	r2, [r3, #0]
	ldr	r2, [r5, #224]	@ float
	ldr	r3, .L136+68
	str	r2, [r3, #0]	@ float
	ldr	r2, [r5, #228]	@ float
	ldr	r3, .L136+72
	str	r2, [r3, #0]	@ float
	ldr	r2, [r5, #232]
	ldr	r3, .L136+76
	str	r2, [r3, #0]
	ldr	r2, [r5, #236]
	ldr	r3, .L136+80
	str	r2, [r3, #0]
	ldr	r3, .L136+84
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r2, [r5, #172]
	ldr	r3, .L136+88
	sub	r1, r2, #122
	rsbs	r2, r1, #0
	adc	r2, r2, r1
	str	r2, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L136+92
	ldr	r2, [r5, #152]	@ float
	str	r0, [r3, #0]
	ldr	r3, .L136+96
	ldr	r0, [r4, #0]
	str	r2, [r3, #0]	@ float
	ldr	r2, [r5, #156]
	ldr	r3, .L136+100
	str	r2, [r3, #0]
	ldr	r2, [r5, #160]	@ float
	ldr	r3, .L136+104
	str	r2, [r3, #0]	@ float
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L137:
	.align	2
.L136:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	1495
	.word	g_GROUP_ID
	.word	GuiVar_GroupName
	.word	GuiVar_MoisBoxIndex
	.word	GuiVar_MoisDecoderSN
	.word	GuiVar_MoisInUse
	.word	GuiVar_MoisPhysicallyAvailable
	.word	GuiVar_MoisControlMode
	.word	GuiVar_MoisSetPoint_High
	.word	GuiVar_MoisSetPoint_Low
	.word	GuiVar_MoisAdditionalSoakSeconds
	.word	GuiVar_MoisTempPoint_High
	.word	GuiVar_MoisTempPoint_Low
	.word	GuiVar_MoisTempAction_High
	.word	GuiVar_MoisTempAction_Low
	.word	GuiVar_MoisECPoint_High
	.word	GuiVar_MoisECPoint_Low
	.word	GuiVar_MoisECAction_High
	.word	GuiVar_MoisECAction_Low
	.word	GuiVar_MoistureTabToDisplay
	.word	GuiVar_MoisSensorIncludesEC
	.word	GuiVar_FLControllerIndex_0
	.word	GuiVar_MoisReading_Current
	.word	GuiVar_MoisTemp_Current
	.word	GuiVar_MoisEC_Current
.LFE25:
	.size	MOISTURE_SENSOR_copy_group_into_guivars, .-MOISTURE_SENSOR_copy_group_into_guivars
	.section	.text.MOISTURE_SENSOR_get_group_with_this_GID,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_group_with_this_GID
	.type	MOISTURE_SENSOR_get_group_with_this_GID, %function
MOISTURE_SENSOR_get_group_with_this_GID:
.LFB35:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI48:
	ldr	r4, .L139
	ldr	r3, .L139+4
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L139+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L139+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L140:
	.align	2
.L139:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	1918
	.word	.LC16
	.word	.LANCHOR0
.LFE35:
	.size	MOISTURE_SENSOR_get_group_with_this_GID, .-MOISTURE_SENSOR_get_group_with_this_GID
	.section	.text.MOISTURE_SENSOR_get_gid_of_group_at_this_index,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_gid_of_group_at_this_index
	.type	MOISTURE_SENSOR_get_gid_of_group_at_this_index, %function
MOISTURE_SENSOR_get_gid_of_group_at_this_index:
.LFB36:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI49:
	ldr	r4, .L144
	ldr	r2, .L144+4
	ldr	r3, .L144+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L144+12
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	subs	r5, r0, #0
	ldrne	r5, [r5, #16]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L145:
	.align	2
.L144:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	1954
	.word	.LANCHOR0
.LFE36:
	.size	MOISTURE_SENSOR_get_gid_of_group_at_this_index, .-MOISTURE_SENSOR_get_gid_of_group_at_this_index
	.section	.text.MOISTURE_SENSOR_get_index_for_group_with_this_serial_number,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_index_for_group_with_this_serial_number
	.type	MOISTURE_SENSOR_get_index_for_group_with_this_serial_number, %function
MOISTURE_SENSOR_get_index_for_group_with_this_serial_number:
.LFB37:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L154
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI50:
	ldr	r2, .L154+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L154+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	moveq	r5, r4
	beq	.L147
	ldr	r0, .L154+12
	bl	nm_ListGetFirst
	mov	r5, #1
	mov	r6, r0
	b	.L148
.L149:
	ldr	r3, [r6, #92]
	cmp	r3, r4
	beq	.L147
	mov	r1, r6
	ldr	r0, .L154+12
	bl	nm_ListGetNext
	add	r5, r5, #1
	mov	r6, r0
.L148:
	cmp	r6, #0
	bne	.L149
	b	.L153
.L147:
	ldr	r3, .L154
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L153:
	ldr	r0, .L154+4
	ldr	r1, .L154+16
	bl	Alert_group_not_found_with_filename
	mov	r5, r6
	b	.L147
.L155:
	.align	2
.L154:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2001
	.word	.LANCHOR0
	.word	2030
.LFE37:
	.size	MOISTURE_SENSOR_get_index_for_group_with_this_serial_number, .-MOISTURE_SENSOR_get_index_for_group_with_this_serial_number
	.section	.text.MOISTURE_SENSOR_get_last_group_ID,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_last_group_ID
	.type	MOISTURE_SENSOR_get_last_group_ID, %function
MOISTURE_SENSOR_get_last_group_ID:
.LFB38:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI51:
	ldr	r4, .L157
	mov	r1, #400
	ldr	r2, .L157+4
	ldr	r0, [r4, #0]
	ldr	r3, .L157+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L157+12
	ldr	r0, [r4, #0]
	ldr	r3, [r3, #0]
	ldr	r5, [r3, #12]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L158:
	.align	2
.L157:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2067
	.word	.LANCHOR0
.LFE38:
	.size	MOISTURE_SENSOR_get_last_group_ID, .-MOISTURE_SENSOR_get_last_group_ID
	.section	.text.MOISTURE_SENSOR_get_list_count,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_list_count
	.type	MOISTURE_SENSOR_get_list_count, %function
MOISTURE_SENSOR_get_list_count:
.LFB39:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI52:
	ldr	r4, .L160
	mov	r1, #400
	ldr	r2, .L160+4
	ldr	r0, [r4, #0]
	ldr	r3, .L160+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L160+12
	ldr	r0, [r4, #0]
	ldr	r5, [r3, #8]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L161:
	.align	2
.L160:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2102
	.word	.LANCHOR0
.LFE39:
	.size	MOISTURE_SENSOR_get_list_count, .-MOISTURE_SENSOR_get_list_count
	.section	.text.MOISTURE_SENSOR_get_physically_available,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_physically_available
	.type	MOISTURE_SENSOR_get_physically_available, %function
MOISTURE_SENSOR_get_physically_available:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI53:
	ldr	r4, .L163
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L163+4
	ldr	r3, .L163+8
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r5, #72
	str	r3, [sp, #0]
	ldr	r3, .L163+12
	add	r1, r5, #96
	str	r3, [sp, #4]
	mov	r2, #0
	ldr	r3, .L163+16
	mov	r0, r5
	bl	SHARED_get_bool_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L164:
	.align	2
.L163:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2116
	.word	.LC1
	.word	nm_MOISTURE_SENSOR_set_physically_available
.LFE40:
	.size	MOISTURE_SENSOR_get_physically_available, .-MOISTURE_SENSOR_get_physically_available
	.section	.text.MOISTURE_SENSOR_get_decoder_serial_number,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_decoder_serial_number
	.type	MOISTURE_SENSOR_get_decoder_serial_number, %function
MOISTURE_SENSOR_get_decoder_serial_number:
.LFB41:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI54:
	ldr	r4, .L166
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L166+4
	ldr	r3, .L166+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L166+12
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r5, #72
	str	r3, [sp, #8]
	ldr	r3, .L166+16
	add	r1, r5, #92
	str	r3, [sp, #12]
	mov	r0, r5
	ldr	r3, .L166+20
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L167:
	.align	2
.L166:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2135
	.word	nm_MOISTURE_SENSOR_set_decoder_serial_number
	.word	.LC0
	.word	2097151
.LFE41:
	.size	MOISTURE_SENSOR_get_decoder_serial_number, .-MOISTURE_SENSOR_get_decoder_serial_number
	.section	.text.MOISTURE_SENSOR_get_num_of_moisture_sensors_connected,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_num_of_moisture_sensors_connected
	.type	MOISTURE_SENSOR_get_num_of_moisture_sensors_connected, %function
MOISTURE_SENSOR_get_num_of_moisture_sensors_connected:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L172
	stmfd	sp!, {r4, lr}
.LCFI55:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L172+4
	mov	r3, #2160
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L172+8
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L169
.L171:
	ldr	r3, [r1, #96]
	cmp	r3, #0
	beq	.L170
	ldr	r3, [r1, #100]
	cmp	r3, #0
	addne	r4, r4, #1
.L170:
	ldr	r0, .L172+8
	bl	nm_ListGetNext
	mov	r1, r0
.L169:
	cmp	r1, #0
	bne	.L171
	ldr	r3, .L172
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L173:
	.align	2
.L172:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	.LANCHOR0
.LFE42:
	.size	MOISTURE_SENSOR_get_num_of_moisture_sensors_connected, .-MOISTURE_SENSOR_get_num_of_moisture_sensors_connected
	.section	.text.MOISTURE_SENSOR_fill_out_recorder_record,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_fill_out_recorder_record
	.type	MOISTURE_SENSOR_fill_out_recorder_record, %function
MOISTURE_SENSOR_fill_out_recorder_record:
.LFB43:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L177
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI56:
	ldr	r2, .L177+4
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L177+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L177+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L177+4
	ldr	r3, .L177+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L177+20
	mov	r1, r4
	bl	nm_OnList
	subs	r6, r0, #0
	beq	.L175
	ldr	r3, [r4, #152]	@ float
	str	r3, [r5, #12]	@ float
	ldr	r3, [r4, #156]
	strh	r3, [r5, #6]	@ movhi
	ldr	r3, [r4, #160]	@ float
	str	r3, [r5, #16]	@ float
	ldr	r3, [r4, #92]
	str	r3, [r5, #8]
	ldr	r3, [r4, #172]
	mov	r4, #1
	strb	r3, [r5, #20]
	b	.L176
.L175:
	ldr	r0, .L177+24
	bl	Alert_Message
	mov	r4, r6
.L176:
	ldr	r3, .L177+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L177
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L178:
	.align	2
.L177:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2194
	.word	moisture_sensor_recorder_recursive_MUTEX
	.word	2197
	.word	.LANCHOR0
	.word	.LC20
.LFE43:
	.size	MOISTURE_SENSOR_fill_out_recorder_record, .-MOISTURE_SENSOR_fill_out_recorder_record
	.section	.text.MOISTURE_SENSOR_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_change_bits_ptr
	.type	MOISTURE_SENSOR_get_change_bits_ptr, %function
MOISTURE_SENSOR_get_change_bits_ptr:
.LFB44:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r0
	add	r2, r3, #76
	mov	r0, r1
	add	r1, r3, #72
	add	r3, r3, #80
	b	SHARED_get_32_bit_change_bits_ptr
.LFE44:
	.size	MOISTURE_SENSOR_get_change_bits_ptr, .-MOISTURE_SENSOR_get_change_bits_ptr
	.section	.text.nm_MOISTURE_SENSOR_store_changes,"ax",%progbits
	.align	2
	.type	nm_MOISTURE_SENSOR_store_changes, %function
nm_MOISTURE_SENSOR_store_changes:
.LFB17:
	@ args = 12, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI57:
	ldr	r9, [sp, #48]
	mov	r5, r0
	mov	r8, r1
	mov	r4, r2
	ldr	r0, .L216
	mov	r1, r5
	add	r2, sp, #12
	mov	r7, r3
	ldr	sl, [sp, #56]
	bl	nm_GROUP_find_this_group_in_list
	ldr	r0, [sp, #12]
	cmp	r0, #0
	beq	.L181
	ldr	r1, [sp, #52]
	bl	MOISTURE_SENSOR_get_change_bits_ptr
	cmp	r4, #2
	mov	r6, r0
	beq	.L182
	tst	sl, #1
	beq	.L183
.L182:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	add	r1, r5, #20
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_name
	cmp	r4, #2
	beq	.L184
.L183:
	tst	sl, #2
	beq	.L185
.L184:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #88]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_box_index
	cmp	r4, #2
	beq	.L186
.L185:
	tst	sl, #4
	beq	.L187
.L186:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #92]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_decoder_serial_number
	cmp	r4, #2
	beq	.L188
.L187:
	tst	sl, #8
	beq	.L189
.L188:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #96]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_physically_available
	cmp	r4, #2
	beq	.L190
.L189:
	tst	sl, #16
	beq	.L191
.L190:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #100]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_in_use
	cmp	r4, #2
	beq	.L192
.L191:
	tst	sl, #32
	beq	.L193
.L192:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #104]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_moisture_control_mode
	cmp	r4, #2
	beq	.L194
.L193:
	tst	sl, #256
	beq	.L195
.L194:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #116]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_low_temp_point
	cmp	r4, #2
	beq	.L196
.L195:
	tst	sl, #512
	beq	.L197
.L196:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #120]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_additional_soak_seconds
	cmp	r4, #2
	beq	.L198
.L197:
	tst	sl, #1024
	beq	.L199
.L198:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #204]	@ float
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_moisture_high_set_point_float
	cmp	r4, #2
	beq	.L200
.L199:
	tst	sl, #2048
	beq	.L201
.L200:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #208]	@ float
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_moisture_low_set_point_float
	cmp	r4, #2
	beq	.L202
.L201:
	tst	sl, #4096
	beq	.L203
.L202:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #212]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_high_temp_point
	cmp	r4, #2
	beq	.L204
.L203:
	tst	sl, #8192
	beq	.L205
.L204:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #216]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_high_temp_action
	cmp	r4, #2
	beq	.L206
.L205:
	tst	sl, #16384
	beq	.L207
.L206:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #220]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_low_temp_action
	cmp	r4, #2
	beq	.L208
.L207:
	tst	sl, #32768
	beq	.L209
.L208:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #224]	@ float
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_high_ec_point
	cmp	r4, #2
	beq	.L210
.L209:
	tst	sl, #65536
	beq	.L211
.L210:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #228]	@ float
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_low_ec_point
	cmp	r4, #2
	beq	.L212
.L211:
	tst	sl, #131072
	beq	.L213
.L212:
	rsbs	r2, r8, #1
	movcc	r2, #0
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #232]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_high_ec_action
	cmp	r4, #2
	beq	.L214
.L213:
	tst	sl, #262144
	beq	.L180
.L214:
	rsbs	r2, r8, #1
	ldr	r0, [sp, #12]
	ldr	r1, [r5, #236]
	movcc	r2, #0
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r6, [sp, #8]
	bl	nm_MOISTURE_SENSOR_set_low_ec_action
	b	.L180
.L181:
	ldr	r0, .L216+4
	ldr	r1, .L216+8
	bl	Alert_group_not_found_with_filename
.L180:
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, pc}
.L217:
	.align	2
.L216:
	.word	.LANCHOR0
	.word	.LC21
	.word	1209
.LFE17:
	.size	nm_MOISTURE_SENSOR_store_changes, .-nm_MOISTURE_SENSOR_store_changes
	.section	.text.MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars
	.type	MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars, %function
MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI58:
	ldr	r5, .L219
	ldr	r8, .L219+4
	mov	r2, r5
	ldr	r3, .L219+8
	ldr	r0, [r8, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r2, .L219+12
	mov	r0, #252
	bl	mem_malloc_debug
	ldr	r3, .L219+16
	ldr	r6, .L219+20
	mov	r7, #0
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number
	mov	r2, #252
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r1, .L219+24
	mov	r2, #48
	add	r0, r4, #20
	bl	strlcpy
	ldr	r3, .L219+28
	ldr	sl, [r6, #0]
	ldr	r3, [r3, #0]
	str	r3, [r4, #104]
	ldr	r3, .L219+32
	ldr	r3, [r3, #0]	@ float
	str	r3, [r4, #204]	@ float
	ldr	r3, .L219+36
	ldr	r3, [r3, #0]	@ float
	str	r3, [r4, #208]	@ float
	ldr	r3, .L219+40
	ldr	r3, [r3, #0]
	str	r3, [r4, #120]
	ldr	r3, .L219+44
	ldr	r3, [r3, #0]
	str	r3, [r4, #212]
	ldr	r3, .L219+48
	ldr	r3, [r3, #0]
	str	r3, [r4, #116]
	ldr	r3, .L219+52
	ldr	r3, [r3, #0]
	str	r3, [r4, #216]
	ldr	r3, .L219+56
	ldr	r3, [r3, #0]
	str	r3, [r4, #220]
	ldr	r3, .L219+60
	ldr	r3, [r3, #0]	@ float
	str	r3, [r4, #224]	@ float
	ldr	r3, .L219+64
	ldr	r3, [r3, #0]	@ float
	str	r3, [r4, #228]	@ float
	ldr	r3, .L219+68
	ldr	r3, [r3, #0]
	str	r3, [r4, #232]
	ldr	r3, .L219+72
	ldr	r3, [r3, #0]
	str	r3, [r4, #236]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r1, sl
	mov	r2, #2
	stmib	sp, {r2, r7}
	mov	r3, r0
	mov	r0, r4
	bl	nm_MOISTURE_SENSOR_store_changes
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L219+76
	str	r7, [r6, #0]
	bl	mem_free_debug
	ldr	r0, [r8, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L220:
	.align	2
.L219:
	.word	.LC16
	.word	moisture_sensor_items_recursive_MUTEX
	.word	1669
	.word	1673
	.word	GuiVar_MoisDecoderSN
	.word	g_GROUP_creating_new
	.word	GuiVar_GroupName
	.word	GuiVar_MoisControlMode
	.word	GuiVar_MoisSetPoint_High
	.word	GuiVar_MoisSetPoint_Low
	.word	GuiVar_MoisAdditionalSoakSeconds
	.word	GuiVar_MoisTempPoint_High
	.word	GuiVar_MoisTempPoint_Low
	.word	GuiVar_MoisTempAction_High
	.word	GuiVar_MoisTempAction_Low
	.word	GuiVar_MoisECPoint_High
	.word	GuiVar_MoisECPoint_Low
	.word	GuiVar_MoisECAction_High
	.word	GuiVar_MoisECAction_Low
	.word	1720
.LFE28:
	.size	MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars, .-MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars
	.section	.text.nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm
	.type	nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm, %function
nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm:
.LFB18:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI59:
	mov	r8, r0
	sub	sp, sp, #40
.LCFI60:
	str	r1, [sp, #16]
	str	r2, [sp, #20]
	mov	r1, r8
	add	r0, sp, #36
	mov	r2, #4
	mov	fp, #0
	mov	r9, r3
	add	r8, r8, #4
	bl	memcpy
	mov	sl, #4
	str	fp, [sp, #12]
	b	.L222
.L246:
	mov	r1, r8
	mov	r2, #4
	add	r0, sp, #24
	bl	memcpy
	add	r1, r8, #4
	mov	r2, #4
	add	r0, sp, #28
	bl	memcpy
	add	r1, r8, #8
	mov	r2, #4
	add	r0, sp, #32
	bl	memcpy
	ldr	r0, [sp, #24]
	bl	MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number
	add	r4, r8, #12
	add	r5, sl, #12
	subs	r7, r0, #0
	bne	.L223
	ldr	r6, .L250
	mov	r0, r6
	bl	nm_ListGetFirst
	subs	r7, r0, #0
	beq	.L224
	ldr	r3, [r6, #8]
	cmp	r3, #1
	bne	.L224
	ldr	r3, [r7, #92]
	cmp	r3, #0
	bne	.L224
	ldr	r3, .L250+4
	add	r0, r7, #80
	ldr	r1, [r3, #0]
	bl	SHARED_set_all_32_bit_change_bits
	b	.L225
.L224:
	bl	nm_MOISTURE_SENSOR_create_new_group
	mov	r3, #1
	str	r3, [sp, #12]
	mov	r7, r0
.L225:
	cmp	r9, #16
	bne	.L226
	ldr	r3, .L250+4
	add	r0, r7, #72
	ldr	r1, [r3, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L226:
	cmp	r7, #0
	beq	.L227
.L223:
	ldr	r1, .L250+8
	ldr	r2, .L250+12
	mov	r0, #252
	bl	mem_malloc_debug
	mov	r1, #0
	mov	r2, #252
	mov	r6, r0
	bl	memset
	mov	r0, r6
	mov	r1, r7
	mov	r2, #252
	bl	memcpy
	ldr	r3, [sp, #32]
	tst	r3, #1
	beq	.L228
	mov	r1, r4
	add	r0, r6, #20
	mov	r2, #48
	bl	memcpy
	add	r4, r8, #60
	add	r5, sl, #60
.L228:
	ldr	r3, [sp, #32]
	tst	r3, #2
	beq	.L229
	mov	r1, r4
	add	r0, r6, #88
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L229:
	ldr	r3, [sp, #32]
	tst	r3, #4
	beq	.L230
	mov	r1, r4
	add	r0, r6, #92
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L230:
	ldr	r3, [sp, #32]
	tst	r3, #8
	beq	.L231
	mov	r1, r4
	add	r0, r6, #96
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L231:
	ldr	r3, [sp, #32]
	tst	r3, #16
	beq	.L232
	mov	r1, r4
	add	r0, r6, #100
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L232:
	ldr	r3, [sp, #32]
	tst	r3, #32
	beq	.L233
	mov	r1, r4
	add	r0, r6, #104
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L233:
	ldr	r3, [sp, #32]
	tst	r3, #256
	beq	.L234
	mov	r1, r4
	add	r0, r6, #116
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L234:
	ldr	r3, [sp, #32]
	tst	r3, #512
	beq	.L235
	mov	r1, r4
	add	r0, r6, #120
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L235:
	ldr	r3, [sp, #32]
	tst	r3, #1024
	beq	.L236
	mov	r1, r4
	add	r0, r6, #204
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L236:
	ldr	r3, [sp, #32]
	tst	r3, #2048
	beq	.L237
	mov	r1, r4
	add	r0, r6, #208
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L237:
	ldr	r3, [sp, #32]
	tst	r3, #4096
	beq	.L238
	mov	r1, r4
	add	r0, r6, #212
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L238:
	ldr	r3, [sp, #32]
	tst	r3, #8192
	beq	.L239
	mov	r1, r4
	add	r0, r6, #216
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L239:
	ldr	r3, [sp, #32]
	tst	r3, #16384
	beq	.L240
	mov	r1, r4
	add	r0, r6, #220
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L240:
	ldr	r3, [sp, #32]
	tst	r3, #32768
	beq	.L241
	mov	r1, r4
	add	r0, r6, #224
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L241:
	ldr	r3, [sp, #32]
	tst	r3, #65536
	beq	.L242
	mov	r1, r4
	add	r0, r6, #228
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L242:
	ldr	r3, [sp, #32]
	tst	r3, #131072
	beq	.L243
	mov	r1, r4
	add	r0, r6, #232
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L243:
	ldr	r3, [sp, #32]
	tst	r3, #262144
	beq	.L244
	mov	r1, r4
	add	r0, r6, #236
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L244:
	ldr	r3, [sp, #20]
	mov	r0, r6
	stmia	sp, {r3, r9}
	ldr	r3, [sp, #32]
	ldr	r1, [sp, #12]
	str	r3, [sp, #8]
	ldr	r2, [sp, #16]
	mov	r3, #0
	bl	nm_MOISTURE_SENSOR_store_changes
	mov	r0, r6
	ldr	r1, .L250+8
	ldr	r2, .L250+16
	bl	mem_free_debug
	b	.L245
.L227:
	ldr	r0, .L250+8
	ldr	r1, .L250+20
	bl	Alert_group_not_found_with_filename
.L245:
	add	fp, fp, #1
	mov	sl, r5
	mov	r8, r4
.L222:
	ldr	r3, [sp, #36]
	cmp	fp, r3
	bcc	.L246
	cmp	sl, #0
	beq	.L247
	cmp	r9, #1
	cmpne	r9, #15
	beq	.L248
	cmp	r9, #16
	bne	.L249
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L249
.L248:
	mov	r0, #18
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L249
.L247:
	ldr	r0, .L250+8
	ldr	r1, .L250+24
	bl	Alert_bit_set_with_no_data_with_filename
.L249:
	mov	r0, sl
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L251:
	.align	2
.L250:
	.word	.LANCHOR0
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC21
	.word	1417
	.word	1573
	.word	1592
	.word	1626
.LFE18:
	.size	nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm, .-nm_MOISTURE_SENSOR_extract_and_store_changes_from_comm
	.section	.text.MOISTURE_SENSOR_extract_and_store_group_name_from_GuiVars,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_extract_and_store_group_name_from_GuiVars
	.type	MOISTURE_SENSOR_extract_and_store_group_name_from_GuiVars, %function
MOISTURE_SENSOR_extract_and_store_group_name_from_GuiVars:
.LFB27:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L254
	stmfd	sp!, {r4, r5, lr}
.LCFI61:
	ldr	r0, [r3, #0]
	sub	sp, sp, #20
.LCFI62:
	mov	r1, #400
	ldr	r2, .L254+4
	ldr	r3, .L254+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L254+12
	ldr	r0, [r3, #0]
	bl	MOISTURE_SENSOR_get_group_at_this_index
	subs	r4, r0, #0
	beq	.L253
	bl	FLOWSENSE_get_controller_index
	mov	r1, #2
	mov	r5, r0
	mov	r0, r4
	bl	MOISTURE_SENSOR_get_change_bits_ptr
	add	r3, r4, #80
	str	r3, [sp, #12]
	mov	r3, #0
	mov	r2, #1
	str	r3, [sp, #16]
	ldr	r1, .L254+16
	mov	r3, #2
	str	r5, [sp, #0]
	str	r2, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	SHARED_set_name_32_bit_change_bits
.L253:
	ldr	r3, .L254
	ldr	r0, [r3, #0]
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L255:
	.align	2
.L254:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	1635
	.word	g_GROUP_list_item_index
	.word	GuiVar_GroupName
.LFE27:
	.size	MOISTURE_SENSOR_extract_and_store_group_name_from_GuiVars, .-MOISTURE_SENSOR_extract_and_store_group_name_from_GuiVars
	.section	.text.MOISTURE_SENSOR_build_data_to_send,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_build_data_to_send
	.type	MOISTURE_SENSOR_build_data_to_send, %function
MOISTURE_SENSOR_build_data_to_send:
.LFB24:
	@ args = 4, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI63:
	mov	r8, r3
	sub	sp, sp, #68
.LCFI64:
	mov	r3, #0
	str	r3, [sp, #64]
	ldr	r3, .L274
	str	r1, [sp, #40]
	mov	r4, r0
	mov	r6, r2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L274+4
	ldr	r3, .L274+8
	ldr	r9, [sp, #104]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L274+12
	bl	nm_ListGetFirst
	b	.L272
.L260:
	mov	r0, r5
	mov	r1, r8
	bl	MOISTURE_SENSOR_get_change_bits_ptr
	ldr	r3, [r0, #0]
	cmp	r3, #0
	ldrne	r3, [sp, #64]
	addne	r3, r3, #1
	strne	r3, [sp, #64]
	bne	.L259
.L258:
	ldr	r0, .L274+12
	mov	r1, r5
	bl	nm_ListGetNext
.L272:
	cmp	r0, #0
	mov	r5, r0
	bne	.L260
.L259:
	ldr	r3, [sp, #64]
	cmp	r3, #0
	beq	.L270
	mov	r3, #0
	str	r3, [sp, #64]
	mov	r0, r4
	mov	r3, r6
	add	r1, sp, #56
	ldr	r2, [sp, #40]
	str	r9, [sp, #0]
	bl	PDATA_allocate_space_for_num_changed_groups_in_pucp
	ldr	r3, [r6, #0]
	cmp	r3, #0
	str	r0, [sp, #36]
	beq	.L261
	ldr	r0, .L274+12
	bl	nm_ListGetFirst
	ldr	r2, [sp, #40]
	add	r2, r2, #12
	str	r2, [sp, #48]
	mov	r5, r0
	b	.L262
.L268:
	mov	r0, r5
	mov	r1, r8
	bl	MOISTURE_SENSOR_get_change_bits_ptr
	ldr	r3, [r0, #0]
	mov	r7, r0
	cmp	r3, #0
	beq	.L263
	ldr	r3, [r6, #0]
	cmp	r3, #0
	beq	.L264
	ldr	r3, [sp, #48]
	ldr	r2, [sp, #36]
	add	sl, r3, r2
	cmp	sl, r9
	bcs	.L264
	add	r3, r5, #92
	mov	r1, r3
	mov	r2, #4
	mov	r0, r4
	str	r3, [sp, #44]
	bl	PDATA_copy_var_into_pucp
	mov	r1, #4
	mov	r2, r1
	add	r3, sp, #60
	mov	r0, r4
	bl	PDATA_copy_bitfield_info_into_pucp
	add	r3, r5, #20
	str	r3, [sp, #4]
	mov	r3, #48
	add	fp, r5, #84
	mov	r2, #0
	str	r3, [sp, #8]
	add	r1, sp, #52
	mov	r3, r7
	mov	r0, r4
	str	r2, [sp, #52]
	str	sl, [sp, #12]
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r3, [sp, #40]
	ldr	r2, [sp, #36]
	mov	sl, #4
	add	r2, r2, r3
	add	r3, r5, #88
	str	r3, [sp, #4]
	str	r2, [sp, #32]
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	sl, [sp, #8]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, r0, #12
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #1
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r2, [sp, #44]
	ldr	ip, [sp, #28]
	stmib	sp, {r2, sl}
	ldr	r2, [sp, #32]
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #2
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #96
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #3
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #100
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, sl
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #104
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #5
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #116
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #8
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #120
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #9
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #204
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #10
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #208
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #11
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #212
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #12
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #216
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #13
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #220
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #14
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #224
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #15
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #228
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #16
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #232
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #17
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #236
	stmib	sp, {r3, sl}
	add	r1, sp, #52
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	r9, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	mov	r2, #18
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r7, ip, r0
	cmp	r7, #12
	bls	.L271
	b	.L273
.L264:
	mov	r3, #0
	str	r3, [r6, #0]
	b	.L267
.L273:
	ldr	r3, [sp, #64]
	ldr	r0, [sp, #60]
	add	r3, r3, #1
	add	r1, sp, #52
	mov	r2, sl
	str	r3, [sp, #64]
	bl	memcpy
	ldr	r3, [sp, #36]
	add	r3, r3, r7
	str	r3, [sp, #36]
	b	.L263
.L271:
	ldr	r3, [r4, #0]
	sub	r3, r3, #12
	str	r3, [r4, #0]
.L263:
	mov	r1, r5
	ldr	r0, .L274+12
	bl	nm_ListGetNext
	mov	r5, r0
.L262:
	cmp	r5, #0
	bne	.L268
.L267:
	ldr	r3, [sp, #64]
	cmp	r3, #0
	beq	.L269
	ldr	r0, [sp, #56]
	add	r1, sp, #64
	mov	r2, #4
	bl	memcpy
	b	.L261
.L269:
	ldr	r2, [r4, #0]
	sub	r2, r2, #4
	str	r2, [r4, #0]
.L270:
	str	r3, [sp, #36]
.L261:
	ldr	r3, .L274
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [sp, #36]
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L275:
	.align	2
.L274:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	1141
	.word	.LANCHOR0
.LFE24:
	.size	MOISTURE_SENSOR_build_data_to_send, .-MOISTURE_SENSOR_build_data_to_send
	.section	.text.MOISTURE_SENSOR_clean_house_processing,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_clean_house_processing
	.type	MOISTURE_SENSOR_clean_house_processing, %function
MOISTURE_SENSOR_clean_house_processing:
.LFB45:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L281
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI65:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L281+4
	ldr	r3, .L281+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L281+12
	ldr	r1, .L281+4
	ldr	r2, .L281+16
	b	.L280
.L278:
	ldr	r1, .L281+4
	ldr	r2, .L281+20
	bl	mem_free_debug
	ldr	r0, .L281+12
	ldr	r1, .L281+4
	ldr	r2, .L281+24
.L280:
	bl	nm_ListRemoveHead_debug
	cmp	r0, #0
	bne	.L278
	ldr	r4, .L281
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r3, #252
	str	r3, [sp, #0]
	ldr	r3, [r4, #0]
	mov	r0, #1
	str	r3, [sp, #4]
	mov	r3, #18
	str	r3, [sp, #8]
	ldr	r1, .L281+28
	mov	r2, #4
	ldr	r3, .L281+12
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldr	r3, .L281+32
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L281+4
	ldr	r3, .L281+36
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L281+40
	ldr	r0, [r3, #0]
	cmp	r0, #0
	beq	.L279
	ldr	r1, .L281+4
	ldr	r2, .L281+44
	bl	mem_free_debug
.L279:
	mov	r1, #0
	mov	r2, #28
	ldr	r0, .L281+40
	bl	memset
	ldr	r3, .L281+32
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L282:
	.align	2
.L281:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2255
	.word	.LANCHOR0
	.word	2257
	.word	2261
	.word	2263
	.word	.LANCHOR1
	.word	moisture_sensor_recorder_recursive_MUTEX
	.word	2277
	.word	msrcs
	.word	2281
.LFE45:
	.size	MOISTURE_SENSOR_clean_house_processing, .-MOISTURE_SENSOR_clean_house_processing
	.section	.text.MOISTURE_SENSOR_set_not_physically_available_based_upon_communication_scan_results,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_set_not_physically_available_based_upon_communication_scan_results
	.type	MOISTURE_SENSOR_set_not_physically_available_based_upon_communication_scan_results, %function
MOISTURE_SENSOR_set_not_physically_available_based_upon_communication_scan_results:
.LFB46:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI66:
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L287
	mov	r1, #400
	ldr	r2, .L287+4
	ldr	r6, .L287+8
	mov	r5, #92
	mov	r8, #1
	mov	r7, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L287+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L287+16
	bl	nm_ListGetFirst
	mov	r4, r0
	b	.L284
.L286:
	ldr	r3, [r4, #88]
	mla	r3, r5, r3, r6
	ldr	sl, [r3, #16]
	cmp	sl, #0
	bne	.L285
	mov	r1, #10
	mov	r0, r4
	bl	MOISTURE_SENSOR_get_change_bits_ptr
	mov	r1, sl
	mov	r2, #1
	mov	r3, #10
	stmia	sp, {r7, r8}
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_MOISTURE_SENSOR_set_physically_available
.L285:
	mov	r1, r4
	ldr	r0, .L287+16
	bl	nm_ListGetNext
	mov	r4, r0
.L284:
	cmp	r4, #0
	bne	.L286
	ldr	r3, .L287
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L288:
	.align	2
.L287:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	chain
	.word	2309
	.word	.LANCHOR0
.LFE46:
	.size	MOISTURE_SENSOR_set_not_physically_available_based_upon_communication_scan_results, .-MOISTURE_SENSOR_set_not_physically_available_based_upon_communication_scan_results
	.section	.text.MOISTURE_SENSOR_set_physically_available_or_create_new_moisture_sensors_as_needed_based_upon_the_discovery_results,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_set_physically_available_or_create_new_moisture_sensors_as_needed_based_upon_the_discovery_results
	.type	MOISTURE_SENSOR_set_physically_available_or_create_new_moisture_sensors_as_needed_based_upon_the_discovery_results, %function
MOISTURE_SENSOR_set_physically_available_or_create_new_moisture_sensors_as_needed_based_upon_the_discovery_results:
.LFB47:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI67:
	ldr	r9, .L302
	sub	sp, sp, #60
.LCFI68:
	bl	FLOWSENSE_get_controller_index
	mov	r1, #400
	ldr	r2, .L302+4
	ldr	r3, .L302+8
	ldr	r7, .L302+12
	ldr	sl, .L302+16
	mov	r4, r0
	ldr	r0, [r9, #0]
	bl	xQueueTakeMutexRecursive_debug
	b	.L290
.L295:
	ldrb	r3, [r7, #224]	@ zero_extendqisi2
	cmp	r3, #3
	bne	.L291
	bl	MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number
	subs	r6, r0, #0
	beq	.L292
	mov	r1, #12
	bl	MOISTURE_SENSOR_get_change_bits_ptr
	mov	r1, #12
	mov	r5, #1
	mov	r8, r0
	mov	r0, r6
	bl	MOISTURE_SENSOR_get_change_bits_ptr
	mov	r2, r5
	mov	r1, r5
	mov	r3, #12
	str	r4, [sp, #0]
	str	r5, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r6
	bl	nm_MOISTURE_SENSOR_set_physically_available
	mov	r0, r6
	mov	r1, #12
	bl	MOISTURE_SENSOR_get_change_bits_ptr
	mov	r1, r5
	mov	r2, r5
	mov	r3, #12
	stmia	sp, {r4, r5}
	str	r0, [sp, #8]
	mov	r0, r6
	bl	nm_MOISTURE_SENSOR_set_in_use
	mov	r0, r6
	mov	r1, r4
	mov	r2, #0
	mov	r3, #12
	stmia	sp, {r4, r5, r8}
	bl	nm_MOISTURE_SENSOR_set_box_index
	b	.L291
.L292:
	ldr	r0, .L302+16
	bl	nm_ListGetFirst
	subs	r5, r0, #0
	beq	.L293
	ldr	r3, [sl, #8]
	cmp	r3, #1
	bne	.L293
	ldr	r3, [r5, #92]
	cmp	r3, #0
	bne	.L293
	add	r0, r5, #80
	ldr	r1, [r9, #0]
	bl	SHARED_set_all_32_bit_change_bits
	b	.L294
.L293:
	bl	nm_MOISTURE_SENSOR_create_new_group
	mov	r5, r0
.L294:
	mov	r1, #12
	mov	r0, r5
	bl	MOISTURE_SENSOR_get_change_bits_ptr
	mov	r6, #1
	mov	r1, r4
	mov	r2, #0
	mov	r3, #12
	str	r4, [sp, #0]
	mov	r8, r0
	mov	r0, r5
	stmib	sp, {r6, r8}
	bl	nm_MOISTURE_SENSOR_set_box_index
	mov	r0, r5
	ldr	r1, [r7, #220]
	mov	r2, #0
	mov	r3, #12
	stmia	sp, {r4, r6, r8}
	bl	nm_MOISTURE_SENSOR_set_decoder_serial_number
	ldr	r2, .L302+20
	add	r3, r4, #65
	str	r3, [sp, #0]
	mov	r1, #48
	ldr	r3, [r7, #220]
	add	r0, sp, #12
	bl	snprintf
	mov	r0, r5
	add	r1, sp, #12
	mov	r2, #0
	mov	r3, #12
	stmia	sp, {r4, r6, r8}
	bl	nm_MOISTURE_SENSOR_set_name
	mov	r0, r5
	mov	r1, r6
	mov	r2, r6
	mov	r3, #12
	stmia	sp, {r4, r6, r8}
	bl	nm_MOISTURE_SENSOR_set_physically_available
	mov	r0, r5
	mov	r1, r6
	mov	r2, #0
	mov	r3, #12
	stmia	sp, {r4, r6, r8}
	bl	nm_MOISTURE_SENSOR_set_in_use
.L291:
	add	r7, r7, #60
.L290:
	ldr	r0, [r7, #220]
	cmp	r0, #0
	bne	.L295
	ldr	r0, .L302+16
	bl	nm_ListGetFirst
	ldr	r7, .L302+24
	mov	r6, #1
	mov	r5, r0
	b	.L296
.L300:
	ldr	r3, [r5, #88]
	cmp	r3, r4
	bne	.L297
	ldr	r3, .L302+12
.L299:
	ldr	r2, [r3, #220]
	cmp	r2, #0
	beq	.L298
	ldr	r1, [r5, #92]
	cmp	r2, r1
	beq	.L297
	add	r3, r3, #60
	cmp	r3, r7
	bne	.L299
.L298:
	mov	r1, #13
	mov	r0, r5
	bl	MOISTURE_SENSOR_get_change_bits_ptr
	mov	r1, #0
	mov	r2, #1
	mov	r3, #13
	stmia	sp, {r4, r6}
	str	r0, [sp, #8]
	mov	r0, r5
	bl	nm_MOISTURE_SENSOR_set_physically_available
.L297:
	mov	r1, r5
	ldr	r0, .L302+16
	bl	nm_ListGetNext
	mov	r5, r0
.L296:
	cmp	r5, #0
	bne	.L300
	ldr	r3, .L302
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L303:
	.align	2
.L302:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2361
	.word	tpmicro_data
	.word	.LANCHOR0
	.word	.LC22
	.word	tpmicro_data+4800
.LFE47:
	.size	MOISTURE_SENSOR_set_physically_available_or_create_new_moisture_sensors_as_needed_based_upon_the_discovery_results, .-MOISTURE_SENSOR_set_physically_available_or_create_new_moisture_sensors_as_needed_based_upon_the_discovery_results
	.section	.text.MOISTURE_SENSORS_check_each_sensor_for_a_crossed_threshold,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSORS_check_each_sensor_for_a_crossed_threshold
	.type	MOISTURE_SENSORS_check_each_sensor_for_a_crossed_threshold, %function
MOISTURE_SENSORS_check_each_sensor_for_a_crossed_threshold:
.LFB48:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L342
	stmfd	sp!, {r0, r4, r5, r6, r7, lr}
.LCFI69:
	ldr	r2, .L342+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L342+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L342+12
	bl	nm_ListGetFirst
	ldr	r7, .L342+16
	mov	r6, #0
	mov	r5, #2
	mov	r4, r0
	b	.L305
.L325:
	ldr	r3, [r4, #164]
	cmp	r3, #0
	beq	.L306
	bl	xTaskGetTickCount
	ldr	r3, [r4, #168]
	rsb	r3, r3, r0
	cmp	r3, r7
	bhi	.L306
	ldr	r3, [r4, #240]
	cmp	r3, #0
	bne	.L307
	flds	s15, [r4, #152]
	flds	s14, [r4, #204]
	fcmpes	s15, s14
	fmstat
	ble	.L334
	mov	r2, #1
	str	r2, [r4, #240]
	ldr	r2, [r4, #104]
	cmp	r2, #1
	bne	.L310
	str	r3, [sp, #0]
	fmrs	r1, s15
	ldr	r0, [r4, #92]
	ldr	r2, [r4, #208]	@ float
	b	.L336
.L334:
	flds	s13, [r4, #208]
	fcmpes	s15, s13
	fmstat
	bpl	.L310
	mov	r3, #1
	str	r3, [r4, #240]
	ldr	r3, [r4, #104]
	cmp	r3, #1
	streq	r3, [sp, #0]
	bne	.L310
	b	.L339
.L307:
	cmp	r3, #1
	bne	.L310
	flds	s15, [r4, #152]
	flds	s13, [r4, #208]
	fcmpes	s15, s13
	fmstat
	ble	.L310
	flds	s14, [r4, #204]
	fcmpes	s15, s14
	fmstat
	bpl	.L310
	ldr	r3, [r4, #104]
	str	r6, [r4, #240]
	cmp	r3, #1
	bne	.L310
	str	r5, [sp, #0]
.L339:
	fmrs	r1, s15
	fmrs	r2, s13
	ldr	r0, [r4, #92]
.L336:
	fmrs	r3, s14
	bl	Alert_soil_moisture_crossed_threshold
.L310:
	ldr	r2, [r4, #244]
	cmp	r2, #0
	bne	.L314
	ldr	r1, [r4, #156]
	ldr	r3, [r4, #212]
	cmp	r1, r3
	ble	.L315
	mov	r0, #1
	str	r0, [r4, #244]
	ldr	r0, [r4, #216]
	cmp	r0, #1
	streq	r2, [sp, #0]
	ldreq	r0, [r4, #92]
	ldreq	r2, [r4, #116]
	bne	.L316
	b	.L337
.L315:
	ldr	r2, [r4, #116]
	cmp	r1, r2
	bcs	.L316
	mov	r0, #1
	str	r0, [r4, #244]
	ldr	r0, [r4, #220]
	cmp	r0, #1
	streq	r0, [sp, #0]
	bne	.L316
	b	.L340
.L314:
	cmp	r2, #1
	bne	.L316
	ldr	r1, [r4, #156]
	ldr	r2, [r4, #116]
	cmp	r1, r2
	bls	.L316
	ldr	r3, [r4, #212]
	cmp	r1, r3
	bge	.L316
	ldr	r0, [r4, #216]
	str	r6, [r4, #244]
	cmp	r0, #1
	beq	.L317
	ldr	r0, [r4, #220]
	cmp	r0, #1
	bne	.L316
.L317:
	str	r5, [sp, #0]
.L340:
	ldr	r0, [r4, #92]
.L337:
	bl	Alert_soil_temperature_crossed_threshold
.L316:
	ldr	r3, [r4, #248]
	cmp	r3, #0
	bne	.L318
	flds	s15, [r4, #160]
	flds	s14, [r4, #224]
	fcmpes	s15, s14
	fmstat
	ble	.L335
	mov	r2, #1
	str	r2, [r4, #248]
	ldr	r2, [r4, #232]
	cmp	r2, #1
	bne	.L306
	str	r3, [sp, #0]
	fmrs	r1, s15
	ldr	r0, [r4, #92]
	ldr	r2, [r4, #228]	@ float
	b	.L338
.L335:
	flds	s13, [r4, #228]
	fcmpes	s15, s13
	fmstat
	bpl	.L306
	mov	r3, #1
	str	r3, [r4, #248]
	ldr	r3, [r4, #236]
	cmp	r3, #1
	streq	r3, [sp, #0]
	bne	.L306
	b	.L341
.L318:
	cmp	r3, #1
	bne	.L306
	flds	s15, [r4, #160]
	flds	s13, [r4, #228]
	fcmpes	s15, s13
	fmstat
	ble	.L306
	flds	s14, [r4, #224]
	fcmpes	s15, s14
	fmstat
	bpl	.L306
	ldr	r3, [r4, #232]
	str	r6, [r4, #248]
	cmp	r3, #1
	beq	.L324
	ldr	r3, [r4, #236]
	cmp	r3, #1
	bne	.L306
.L324:
	str	r5, [sp, #0]
.L341:
	fmrs	r1, s15
	fmrs	r2, s13
	ldr	r0, [r4, #92]
.L338:
	fmrs	r3, s14
	bl	Alert_soil_conductivity_crossed_threshold
.L306:
	mov	r1, r4
	ldr	r0, .L342+12
	bl	nm_ListGetNext
	mov	r4, r0
.L305:
	cmp	r4, #0
	bne	.L325
	ldr	r3, .L342
	ldr	r0, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L343:
	.align	2
.L342:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2520
	.word	.LANCHOR0
	.word	35999
.LFE48:
	.size	MOISTURE_SENSORS_check_each_sensor_for_a_crossed_threshold, .-MOISTURE_SENSORS_check_each_sensor_for_a_crossed_threshold
	.section	.text.MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token
	.type	MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token, %function
MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token:
.LFB49:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI70:
	ldr	r4, .L348
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L348+4
	ldr	r3, .L348+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L348+12
	bl	nm_ListGetFirst
	b	.L347
.L346:
	add	r0, r5, #76
	ldr	r1, [r4, #0]
	bl	SHARED_set_all_32_bit_change_bits
	ldr	r0, .L348+12
	mov	r1, r5
	bl	nm_ListGetNext
.L347:
	cmp	r0, #0
	mov	r5, r0
	bne	.L346
	ldr	r3, .L348
	ldr	r4, .L348+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L348+4
	ldr	r3, .L348+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L348+24
	ldr	r0, [r4, #0]
	mov	r2, #1
	str	r2, [r3, #444]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L349:
	.align	2
.L348:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2643
	.word	.LANCHOR0
	.word	comm_mngr_recursive_MUTEX
	.word	2665
	.word	comm_mngr
.LFE49:
	.size	MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token, .-MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token
	.section	.text.MOISTURE_SENSOR_on_all_moisture_sensors_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_on_all_moisture_sensors_set_or_clear_commserver_change_bits
	.type	MOISTURE_SENSOR_on_all_moisture_sensors_set_or_clear_commserver_change_bits, %function
MOISTURE_SENSOR_on_all_moisture_sensors_set_or_clear_commserver_change_bits:
.LFB50:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI71:
	ldr	r4, .L356
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L356+4
	ldr	r3, .L356+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L356+12
	bl	nm_ListGetFirst
	b	.L355
.L354:
	cmp	r5, #51
	ldr	r1, [r4, #0]
	add	r0, r6, #80
	bne	.L352
	bl	SHARED_clear_all_32_bit_change_bits
	b	.L353
.L352:
	bl	SHARED_set_all_32_bit_change_bits
.L353:
	ldr	r0, .L356+12
	mov	r1, r6
	bl	nm_ListGetNext
.L355:
	cmp	r0, #0
	mov	r6, r0
	bne	.L354
	ldr	r3, .L356
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L357:
	.align	2
.L356:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2677
	.word	.LANCHOR0
.LFE50:
	.size	MOISTURE_SENSOR_on_all_moisture_sensors_set_or_clear_commserver_change_bits, .-MOISTURE_SENSOR_on_all_moisture_sensors_set_or_clear_commserver_change_bits
	.section	.text.nm_MOISTURE_SENSOR_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_update_pending_change_bits
	.type	nm_MOISTURE_SENSOR_update_pending_change_bits, %function
nm_MOISTURE_SENSOR_update_pending_change_bits:
.LFB51:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L365
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI72:
	ldr	r2, .L365+4
	mov	r1, #400
	mov	r7, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L365+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L365+12
	bl	nm_ListGetFirst
	mov	r8, #0
	ldr	r6, .L365+16
	mov	r5, #1
	mov	r4, r0
	b	.L359
.L362:
	ldr	r3, [r4, #84]
	cmp	r3, #0
	beq	.L360
	cmp	r7, #0
	beq	.L361
	ldr	r2, [r4, #80]
	mov	r1, #2
	orr	r3, r2, r3
	str	r3, [r4, #80]
	ldr	r3, .L365+20
	ldr	r2, .L365+24
	str	r5, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L365+28
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L364
.L361:
	add	r0, r4, #84
	ldr	r1, [r6, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L364:
	mov	r8, #1
.L360:
	mov	r1, r4
	ldr	r0, .L365+12
	bl	nm_ListGetNext
	mov	r4, r0
.L359:
	cmp	r4, #0
	bne	.L362
	ldr	r3, .L365
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r8, #0
	beq	.L358
	mov	r0, #18
	mov	r1, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L358:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L366:
	.align	2
.L365:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	2714
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	weather_preserves
	.word	60000
	.word	cics
.LFE51:
	.size	nm_MOISTURE_SENSOR_update_pending_change_bits, .-nm_MOISTURE_SENSOR_update_pending_change_bits
	.section	.text.nm_MOISTURE_SENSOR_set_new_reading_slave_side_variables,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_set_new_reading_slave_side_variables
	.type	nm_MOISTURE_SENSOR_set_new_reading_slave_side_variables, %function
nm_MOISTURE_SENSOR_set_new_reading_slave_side_variables:
.LFB52:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI73:
	subs	r4, r1, #0
	mov	r5, r0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r0, .L369
	bl	nm_OnList
	cmp	r0, #0
	ldmeqfd	sp!, {r4, r5, pc}
	add	r0, r4, #124
	mov	r1, r5
	mov	r2, #21
	bl	memcpy
	mov	r3, #1
	str	r3, [r4, #148]
	ldmfd	sp!, {r4, r5, pc}
.L370:
	.align	2
.L369:
	.word	.LANCHOR0
.LFE52:
	.size	nm_MOISTURE_SENSOR_set_new_reading_slave_side_variables, .-nm_MOISTURE_SENSOR_set_new_reading_slave_side_variables
	.section	.text.MOISTURE_SENSOR_find_a_sensor_reading_to_send_to_the_master,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_find_a_sensor_reading_to_send_to_the_master
	.type	MOISTURE_SENSOR_find_a_sensor_reading_to_send_to_the_master, %function
MOISTURE_SENSOR_find_a_sensor_reading_to_send_to_the_master:
.LFB53:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI74:
	ldr	r2, .L377
	str	r3, [r0, #0]
	ldr	r3, .L377+4
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L377+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L377+12
	bl	nm_ListGetFirst
	b	.L376
.L375:
	ldr	r3, [r4, #148]
	cmp	r3, #0
	beq	.L373
	mov	r0, #25
	mov	r1, sp
	ldr	r2, .L377
	ldr	r3, .L377+16
	bl	mem_oabia
	cmp	r0, #0
	beq	.L373
	ldr	r6, [sp, #0]
	add	r1, r4, #92
	mov	r2, #4
	mov	r0, r6
	bl	memcpy
	ldr	r3, [r5, #0]
	ldr	r0, [sp, #0]
	add	r3, r3, #4
	add	r0, r0, #4
	str	r3, [r5, #0]
	add	r1, r4, #124
	mov	r2, #21
	str	r0, [sp, #0]
	bl	memcpy
	ldr	r3, [r5, #0]
	add	r3, r3, #21
	str	r3, [r5, #0]
	mov	r3, #0
	str	r3, [r4, #148]
	b	.L374
.L373:
	ldr	r0, .L377+12
	mov	r1, r4
	bl	nm_ListGetNext
.L376:
	cmp	r0, #0
	mov	r4, r0
	bne	.L375
	mov	r6, r0
.L374:
	ldr	r3, .L377+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r3, r4, r5, r6, pc}
.L378:
	.align	2
.L377:
	.word	.LC16
	.word	moisture_sensor_items_recursive_MUTEX
	.word	2798
	.word	.LANCHOR0
	.word	2812
.LFE53:
	.size	MOISTURE_SENSOR_find_a_sensor_reading_to_send_to_the_master, .-MOISTURE_SENSOR_find_a_sensor_reading_to_send_to_the_master
	.global	__divsi3
	.global	__umodsi3
	.section	.text.MOISTURE_SENSOR_extract_moisture_reading_from_token_response,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_extract_moisture_reading_from_token_response
	.type	MOISTURE_SENSOR_extract_moisture_reading_from_token_response, %function
MOISTURE_SENSOR_extract_moisture_reading_from_token_response:
.LFB55:
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI75:
	fstmfdd	sp!, {d8}
.LCFI76:
	ldr	r3, .L409+24
	sub	sp, sp, #72
.LCFI77:
	str	r3, [sp, #28]	@ float
	ldr	r3, .L409+28
	mov	r5, r0
	str	r3, [sp, #32]	@ float
	ldr	r3, .L409+32
	mov	r2, #4
	str	r3, [sp, #36]	@ float
	ldr	r3, .L409+36
	mov	r4, r1
	str	r3, [sp, #40]	@ float
	ldr	r3, .L409+40
	ldr	r1, [r5, #0]
	str	r3, [sp, #44]	@ float
	ldr	r3, .L409+44
	add	r0, sp, #24
	str	r3, [sp, #48]	@ float
	ldr	r3, .L409+48
	str	r3, [sp, #52]	@ float
	mov	r3, #1065353216
	str	r3, [sp, #56]	@ float
	ldr	r3, .L409+52
	str	r3, [sp, #60]	@ float
	ldr	r3, .L409+56
	str	r3, [sp, #64]	@ float
	ldr	r3, .L409+60
	str	r3, [sp, #68]	@ float
	bl	memcpy
	ldr	r1, [r5, #0]
	mov	r0, sp
	add	r1, r1, #4
	str	r1, [r5, #0]
	mov	r2, #21
	bl	memcpy
	ldr	r3, [r5, #0]
	add	r3, r3, #21
	str	r3, [r5, #0]
	ldrb	r3, [sp, #0]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L380
	ldr	r0, .L409+64
	ldr	r1, [sp, #24]
	bl	Alert_Message_va
.L380:
	ldrb	r3, [sp, #0]	@ zero_extendqisi2
	tst	r3, #31
	moveq	r4, #1
	beq	.L381
	ldr	r3, .L409+68
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L409+72
	ldr	r3, .L409+76
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	ldr	r1, [sp, #24]
	bl	nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number
	subs	r4, r0, #0
	beq	.L382
	add	r0, sp, #1
	bl	atoi
	add	r3, sp, #1
	mov	r7, r0
.L383:
	mov	r5, r3
	ldrb	r2, [r5, #0]	@ zero_extendqisi2
	add	r3, r3, #1
	cmp	r2, #32
	bne	.L383
	mov	r0, r3
	bl	atoi
	mov	r6, r0
	b	.L384
.L398:
	mov	r5, r8
.L384:
	ldrb	r3, [r5, #1]	@ zero_extendqisi2
	add	r8, r5, #1
	cmp	r3, #32
	bne	.L398
	add	r0, r8, #1
	bl	atoi
	mov	r5, r0
	b	.L385
.L399:
	mov	r8, r3
.L385:
	ldrb	r2, [r8, #1]	@ zero_extendqisi2
	add	r3, r8, #1
	cmp	r2, #13
	bne	.L399
	mov	r2, #0
	str	r2, [r4, #172]
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	ldr	r3, .L409+80
	sub	r1, r7, #30
	cmp	r1, r3
	movhi	r3, #0
	movls	r3, #1
	cmp	r0, #120
	str	r0, [r4, #172]
	bne	.L386
	cmp	r6, r2
	movne	r3, r2
	b	.L387
.L386:
	cmp	r0, #122
	movne	r3, r2
	bne	.L387
	ldr	r2, .L409+84
	cmp	r6, r2
	movhi	r3, #0
.L387:
	ldr	r2, .L409+88
	cmp	r5, r2
	bgt	.L388
	cmp	r3, #0
	beq	.L388
	flds	s14, [sp, #32]
	fmsr	s15, r7	@ int
	flds	s8, [sp, #36]
.LBB4:
	mov	sl, #0
	mvn	r7, #0
.LBE4:
	fuitod	d6, s15
	fcvtds	d7, s14
	fcvtds	d4, s8
.LBB5:
	mov	r8, sl
.LBE5:
	fdivd	d6, d6, d7
	flds	s14, [sp, #40]
	flds	s5, [sp, #44]
	flds	s16, [sp, #48]
	fcvtds	d7, s14
	fmuld	d5, d6, d6
	fcvtsd	s17, d6
	fmuld	d3, d5, d6
	fmuld	d7, d7, d5
	fcvtds	d5, s5
	fmscd	d7, d4, d3
	fmacd	d7, d5, d6
	fcvtds	d6, s16
	fsubd	d7, d7, d6
	fcvtsd	s16, d7
	fsts	s16, [r4, #152]
	bl	xTaskGetTickCount
	cmp	r6, #700
	addhi	r6, r6, r6, asl #2
	subhi	r6, r6, #2800
	fmsr	s13, r6	@ int
	flds	s15, [sp, #28]
	mov	r3, #1
	str	r3, [r4, #164]
	fuitos	s14, s13
.LBB6:
	ldr	r3, .L409+92
	mov	r1, #400
	ldr	r2, .L409+72
.LBE6:
	ldr	r6, [r4, #92]
	fdivs	s15, s14, s15
	str	r0, [r4, #168]
.LBB7:
	ldr	r0, [r3, #0]
.LBE7:
	fsts	s15, [r4, #160]
.LBB8:
	ldr	r3, .L409+96
	bl	xQueueTakeMutexRecursive_debug
	b	.L390
.L393:
	mov	r0, r8
	bl	STATION_GROUP_get_group_at_this_index
	mov	r9, r0
	bl	STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group
	cmp	r0, r6
	bne	.L391
	cmn	r7, #1
	mov	r0, r9
	bne	.L392
	bl	STATION_GROUP_get_soil_type
	mov	r7, r0
	b	.L391
.L392:
	bl	STATION_GROUP_get_soil_type
	cmp	r7, r0
	beq	.L391
	cmp	sl, #0
	bne	.L391
	ldr	r0, .L409+100
	bl	Alert_Message
	mov	sl, #1
.L391:
	add	r8, r8, #1
.L390:
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r8, r0
	bcc	.L393
	ldr	r3, .L409+92
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r7, #1
	bls	.L403
	cmp	r7, #2
	beq	.L404
	sub	r3, r7, #3
	cmp	r3, #1
	bls	.L405
	flds	s15, .L409
	flds	s14, .L409+4
	sub	r7, r7, #5
	cmp	r7, #2
	fcpyscc	s15, s14
	b	.L394
.L403:
	flds	s15, .L409+8
	b	.L394
.L404:
	flds	s15, .L409+12
	b	.L394
.L405:
	flds	s15, .L409+16
.L394:
.LBE8:
	flds	s14, [sp, #60]
	flds	s12, [sp, #56]
	flds	s11, [sp, #64]
	flds	s13, [sp, #68]
	fmuls	s16, s14, s16
	flds	s10, [r4, #160]
	fdivs	s15, s15, s11
	fsubs	s17, s17, s13
	cmp	r5, #900
	addgt	r5, r5, r5, asl #2
	subgt	r5, r5, #3600
	sub	r0, r5, #400
	fmuls	s16, s16, s10
	mov	r1, #10
	ldr	r5, .L409+104
	fsubs	s15, s12, s15
	fmuls	s17, s15, s17
	fdivs	s16, s16, s17
	fsts	s16, [r4, #160]
	bl	__divsi3
	flds	s16, [sp, #52]
	mov	r1, #10
	fmsr	s15, r0	@ int
	ldr	r0, [r5, #0]
	fsitos	s14, s15
	flds	s15, .L409+20
	add	r0, r0, #1
	str	r0, [r5, #0]
	fmacs	s15, s14, s16
	ftosizs	s15, s15
	fsts	s15, [r4, #156]	@ int
	bl	__umodsi3
	subs	r6, r0, #0
	bne	.L407
	mov	r0, r4
	bl	MOISTURE_SENSOR_RECORDER_add_a_record
	ldr	r0, [r4, #152]	@ float
	ldr	r1, [r4, #156]
	ldr	r2, [r4, #160]	@ float
	bl	Alert_moisture_reading_obtained
	str	r6, [r5, #0]
	b	.L407
.L388:
	mov	r1, r7
	mov	r2, r5
	mov	r3, r6
	bl	Alert_moisture_reading_out_of_range
	b	.L407
.L382:
	ldr	r0, .L409+108
	bl	Alert_Message
	b	.L396
.L407:
	mov	r4, #1
.L396:
	ldr	r3, .L409+68
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L381:
	mov	r0, r4
	add	sp, sp, #72
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L410:
	.align	2
.L409:
	.word	1065353216
	.word	1070386381
	.word	1066192077
	.word	1067450368
	.word	1068708659
	.word	1107296256
	.word	1120403456
	.word	1112014848
	.word	915425464
	.word	974138848
	.word	1022309591
	.word	1029248647
	.word	1072064102
	.word	1117782016
	.word	1076468122
	.word	1082340147
	.word	.LC23
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	3086
	.word	4970
	.word	3000
	.word	950
	.word	list_program_data_recursive_MUTEX
	.word	2895
	.word	.LC24
	.word	.LANCHOR4
	.word	.LC25
.LFE55:
	.size	MOISTURE_SENSOR_extract_moisture_reading_from_token_response, .-MOISTURE_SENSOR_extract_moisture_reading_from_token_response
	.section	.text.MOISTURE_SENSORS_initialize__saved__irrigation_control_variables_for_this_moisture_sensor,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSORS_initialize__saved__irrigation_control_variables_for_this_moisture_sensor
	.type	MOISTURE_SENSORS_initialize__saved__irrigation_control_variables_for_this_moisture_sensor, %function
MOISTURE_SENSORS_initialize__saved__irrigation_control_variables_for_this_moisture_sensor:
.LFB56:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI78:
	subs	r4, r0, #0
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	ldr	r3, .L417
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L417+4
	ldr	r3, .L417+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L417+12
	bl	nm_ListGetFirst
	mov	r5, #0
	mov	r6, r5
	mov	r7, r0
	b	.L413
.L415:
	ldr	r3, [r7, #92]
	cmp	r3, r4
	bne	.L414
	ldr	r3, [r7, #192]
	str	r6, [r7, #184]
	cmp	r3, #0
	str	r6, [r7, #188]
	str	r6, [r7, #176]
	beq	.L416
	ldr	r0, .L417+16
	bl	Alert_Message
	str	r6, [r7, #192]
.L416:
	mov	r5, #1
.L414:
	mov	r1, r7
	ldr	r0, .L417+12
	bl	nm_ListGetNext
	mov	r7, r0
.L413:
	cmp	r7, #0
	bne	.L415
	ldr	r3, .L417
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r5, #0
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	mov	r0, #18
	mov	r1, #2
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L418:
	.align	2
.L417:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	3379
	.word	.LANCHOR0
	.word	.LC26
.LFE56:
	.size	MOISTURE_SENSORS_initialize__saved__irrigation_control_variables_for_this_moisture_sensor, .-MOISTURE_SENSORS_initialize__saved__irrigation_control_variables_for_this_moisture_sensor
	.section	.text.MOISTURE_SENSORS_initialize__transient__control_variables_for_all_sensors,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSORS_initialize__transient__control_variables_for_all_sensors
	.type	MOISTURE_SENSORS_initialize__transient__control_variables_for_all_sensors, %function
MOISTURE_SENSORS_initialize__transient__control_variables_for_all_sensors:
.LFB57:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L422
	stmfd	sp!, {r4, r5, lr}
.LCFI79:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L422+4
	ldr	r3, .L422+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L422+12
	bl	nm_ListGetFirst
	mov	r5, #0
	mov	r4, #1
	mov	r1, r0
	b	.L420
.L421:
	str	r5, [r1, #192]
	str	r4, [r1, #196]
	str	r4, [r1, #200]
	ldr	r0, .L422+12
	bl	nm_ListGetNext
	mov	r1, r0
.L420:
	cmp	r1, #0
	bne	.L421
	ldr	r3, .L422
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L423:
	.align	2
.L422:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	3454
	.word	.LANCHOR0
.LFE57:
	.size	MOISTURE_SENSORS_initialize__transient__control_variables_for_all_sensors, .-MOISTURE_SENSORS_initialize__transient__control_variables_for_all_sensors
	.section	.text.MOISTURE_SENSORS_update__transient__control_variables_for_this_station,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSORS_update__transient__control_variables_for_this_station
	.type	MOISTURE_SENSORS_update__transient__control_variables_for_this_station, %function
MOISTURE_SENSORS_update__transient__control_variables_for_this_station:
.LFB58:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #92]
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI80:
	cmp	r3, #0
	mov	r5, r0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldr	r3, .L434
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L434+4
	ldr	r3, .L434+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L434+12
	bl	nm_ListGetFirst
	b	.L433
.L432:
	ldr	r2, [r4, #92]
	ldr	r3, [r5, #92]
	cmp	r2, r3
	bne	.L427
	mov	r3, #1
	str	r3, [r4, #192]
	ldrb	r3, [r5, #74]	@ zero_extendqisi2
	tst	r3, #64
	bne	.L428
	ldr	r3, [r5, #60]
	cmp	r3, #0
	beq	.L429
.L428:
	mov	r3, #0
	str	r3, [r4, #196]
.L429:
	ldr	r6, .L434+16
	mov	r1, #400
	ldr	r0, [r6, #0]
	ldr	r2, .L434+4
	ldr	r3, .L434+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [r5, #44]
	ldr	r3, .L434+24
	ldr	r0, [r6, #0]
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #69]	@ zero_extendqisi2
	ldr	r3, [r4, #188]
	cmp	r2, r3
	movcc	r3, #0
	strcc	r3, [r4, #200]
	bl	xQueueGiveMutexRecursive
	b	.L431
.L427:
	ldr	r0, .L434+12
	mov	r1, r4
	bl	nm_ListGetNext
.L433:
	cmp	r0, #0
	mov	r4, r0
	bne	.L432
.L431:
	ldr	r3, .L434
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L435:
	.align	2
.L434:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	3492
	.word	.LANCHOR0
	.word	station_preserves_recursive_MUTEX
	.word	3515
	.word	station_preserves
.LFE58:
	.size	MOISTURE_SENSORS_update__transient__control_variables_for_this_station, .-MOISTURE_SENSORS_update__transient__control_variables_for_this_station
	.section	.text.MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_the_list,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_the_list
	.type	MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_the_list, %function
MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_the_list:
.LFB59:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI81:
	ldr	r4, [r0, #92]
	mov	r5, r0
	cmp	r4, #0
	beq	.L437
	ldr	r3, .L445
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L445+4
	mov	r3, #3568
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L445+8
	bl	nm_ListGetFirst
	b	.L444
.L441:
	ldr	r2, [r4, #92]
	ldr	r3, [r5, #92]
	cmp	r2, r3
	bne	.L439
	ldr	r3, [r4, #184]
	cmp	r3, #0
	moveq	r4, r3
	beq	.L440
	ldr	r6, .L445+12
	mov	r1, #400
	ldr	r0, [r6, #0]
	ldr	r2, .L445+4
	ldr	r3, .L445+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [r5, #44]
	ldr	r3, .L445+20
	ldr	r0, [r6, #0]
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #69]	@ zero_extendqisi2
	ldr	r3, [r4, #188]
	cmp	r2, r3
	movcc	r4, #0
	movcs	r4, #1
	bl	xQueueGiveMutexRecursive
	b	.L440
.L439:
	ldr	r0, .L445+8
	mov	r1, r4
	bl	nm_ListGetNext
.L444:
	cmp	r0, #0
	mov	r4, r0
	bne	.L441
.L440:
	ldr	r3, .L445
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L437:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L446:
	.align	2
.L445:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	.LANCHOR0
	.word	station_preserves_recursive_MUTEX
	.word	3582
	.word	station_preserves
.LFE59:
	.size	MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_the_list, .-MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_the_list
	.section	.text.MOISTURE_SENSORS_check_each_sensor_to_possibly_update__saved__control_variables,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSORS_check_each_sensor_to_possibly_update__saved__control_variables
	.type	MOISTURE_SENSORS_check_each_sensor_to_possibly_update__saved__control_variables, %function
MOISTURE_SENSORS_check_each_sensor_to_possibly_update__saved__control_variables:
.LFB60:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L458
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI82:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L458+4
	ldr	r3, .L458+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L458+12
	bl	nm_ListGetFirst
	mov	r5, #0
	ldr	r7, .L458+16
	mov	r6, #1
	mov	r4, r0
	b	.L448
.L453:
	ldr	r3, [r4, #192]
	cmp	r3, #0
	beq	.L449
	ldr	r3, [r4, #196]
	cmp	r3, #0
	beq	.L449
	ldr	r3, [r4, #200]
	cmp	r3, #0
	beq	.L449
	ldr	r3, [r4, #164]
	cmp	r3, #0
	beq	.L450
	bl	xTaskGetTickCount
	ldr	r3, [r4, #168]
	rsb	r3, r3, r0
	cmp	r3, r7
	bhi	.L450
	flds	s14, [r4, #152]
	flds	s15, [r4, #204]
	mov	r5, #1
	fcmpes	s14, s15
	fmstat
	ldrlt	r3, [r4, #188]
	strge	r6, [r4, #184]
	addlt	r3, r3, #1
	strlt	r3, [r4, #188]
	b	.L449
.L450:
	ldr	r3, [r4, #176]
	add	r3, r3, #1
	cmp	r3, #300
	str	r3, [r4, #176]
	bls	.L449
	ldr	r3, [r4, #188]
	ldr	r0, .L458+20
	add	r3, r3, #1
	str	r3, [r4, #188]
	ldr	r1, [r4, #92]
	bl	Alert_Message_va
.L449:
	mov	r1, r4
	ldr	r0, .L458+12
	bl	nm_ListGetNext
	mov	r4, r0
.L448:
	cmp	r4, #0
	bne	.L453
	ldr	r3, .L458
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r5, #0
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	mov	r0, #18
	mov	r1, r4
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L459:
	.align	2
.L458:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	3642
	.word	.LANCHOR0
	.word	35999
	.word	.LC27
.LFE60:
	.size	MOISTURE_SENSORS_check_each_sensor_to_possibly_update__saved__control_variables, .-MOISTURE_SENSORS_check_each_sensor_to_possibly_update__saved__control_variables
	.section	.text.MOISTURE_SENSORS_this_ilc_has_irrigated_the_required_cycles,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSORS_this_ilc_has_irrigated_the_required_cycles
	.type	MOISTURE_SENSORS_this_ilc_has_irrigated_the_required_cycles, %function
MOISTURE_SENSORS_this_ilc_has_irrigated_the_required_cycles:
.LFB61:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI83:
	ldr	r4, [r0, #92]
	mov	r5, r0
	cmp	r4, #0
	beq	.L461
	ldr	r3, .L468
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L468+4
	mov	r3, #3744
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L468+8
	bl	nm_ListGetFirst
	b	.L467
.L465:
	ldr	r2, [r4, #92]
	ldr	r3, [r5, #92]
	cmp	r2, r3
	bne	.L463
	ldr	r6, .L468+12
	mov	r1, #400
	ldr	r0, [r6, #0]
	ldr	r2, .L468+4
	ldr	r3, .L468+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [r5, #44]
	ldr	r3, .L468+20
	ldr	r0, [r6, #0]
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #69]	@ zero_extendqisi2
	ldr	r3, [r4, #188]
	cmp	r2, r3
	movcc	r4, #0
	movcs	r4, #1
	bl	xQueueGiveMutexRecursive
	b	.L464
.L463:
	ldr	r0, .L468+8
	mov	r1, r4
	bl	nm_ListGetNext
.L467:
	cmp	r0, #0
	mov	r4, r0
	bne	.L465
.L464:
	ldr	r3, .L468
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L461:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L469:
	.align	2
.L468:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC16
	.word	.LANCHOR0
	.word	station_preserves_recursive_MUTEX
	.word	3753
	.word	station_preserves
.LFE61:
	.size	MOISTURE_SENSORS_this_ilc_has_irrigated_the_required_cycles, .-MOISTURE_SENSORS_this_ilc_has_irrigated_the_required_cycles
	.section	.text.MOISTURE_SENSOR_calculate_chain_sync_crc,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_calculate_chain_sync_crc
	.type	MOISTURE_SENSOR_calculate_chain_sync_crc, %function
MOISTURE_SENSOR_calculate_chain_sync_crc:
.LFB62:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L475
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI84:
	ldr	r5, .L475+4
	mov	r1, #400
	ldr	r2, .L475+8
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L475+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #8]
	mov	r0, #252
	mul	r0, r3, r0
	mov	r1, sp
	ldr	r2, .L475+8
	ldr	r3, .L475+16
	bl	mem_oabia
	subs	r6, r0, #0
	beq	.L471
	ldr	r3, [sp, #0]
	add	r6, sp, #8
	mov	r0, r5
	str	r3, [r6, #-4]!
	bl	nm_ListGetFirst
	mov	r7, #0
	mov	r5, r0
	b	.L472
.L473:
	add	r8, r5, #20
	mov	r0, r8
	bl	strlen
	mov	r1, r8
	mov	r2, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #88
	mov	r2, #4
	mov	r8, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #92
	mov	r2, #4
	add	r0, r8, r0
	add	r7, r0, r7
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #96
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #100
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #104
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #116
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #120
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #204
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #208
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #212
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #216
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #220
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #224
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #228
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #232
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #236
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r1, r5
	add	r7, r7, r0
	ldr	r0, .L475+4
	bl	nm_ListGetNext
	mov	r5, r0
.L472:
	cmp	r5, #0
	bne	.L473
	mov	r1, r7
	ldr	r0, [sp, #0]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L475+20
	add	r4, r4, #43
	ldr	r1, .L475+8
	ldr	r2, .L475+24
	mov	r6, #1
	str	r0, [r3, r4, asl #2]
	ldr	r0, [sp, #0]
	bl	mem_free_debug
	b	.L474
.L471:
	ldr	r3, .L475+28
	ldr	r0, .L475+32
	ldr	r1, [r3, r4, asl #4]
	bl	Alert_Message_va
.L474:
	ldr	r3, .L475
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L476:
	.align	2
.L475:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC16
	.word	3809
	.word	3821
	.word	cscs
	.word	3916
	.word	chain_sync_file_pertinants
	.word	.LC28
.LFE62:
	.size	MOISTURE_SENSOR_calculate_chain_sync_crc, .-MOISTURE_SENSOR_calculate_chain_sync_crc
	.global	moisture_sensor_list_item_sizes
	.global	MOISTURE_SENSOR_DEFAULT_GROUP_NAME
	.section	.bss.moisture_sensor_group_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	moisture_sensor_group_list_hdr, %object
	.size	moisture_sensor_group_list_hdr, 20
moisture_sensor_group_list_hdr:
	.space	20
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"DecoderSerialNumber\000"
.LC1:
	.ascii	"PhysicallyAvailable\000"
.LC2:
	.ascii	"BoxIndex\000"
.LC3:
	.ascii	"InUse\000"
.LC4:
	.ascii	"MoistureControlMode\000"
.LC5:
	.ascii	"LowTempPoint\000"
.LC6:
	.ascii	"AdditionalSoakSeconds\000"
.LC7:
	.ascii	"MoistureHighSetPoint_float\000"
.LC8:
	.ascii	"MoistureLowSetPoint_float\000"
.LC9:
	.ascii	"HighTempPoint\000"
.LC10:
	.ascii	"HighTempAction\000"
.LC11:
	.ascii	"LowTempAction\000"
.LC12:
	.ascii	"HighECPoint\000"
.LC13:
	.ascii	"LowECPoint\000"
.LC14:
	.ascii	"HighECAction\000"
.LC15:
	.ascii	"LowECAction\000"
.LC16:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/moisture_sensors.c\000"
.LC17:
	.ascii	"MOISTURE_SENSOR file unexpd update %u\000"
.LC18:
	.ascii	"MOISTURE_SENSOR file update : to revision %u from %"
	.ascii	"u\000"
.LC19:
	.ascii	"MOISTURE_SENSOR updater error\000"
.LC20:
	.ascii	"Moisture Report: Sensor not on list!\000"
.LC21:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/shared_moisture_sensors.c\000"
.LC22:
	.ascii	"Decoder %07d @ %c\000"
.LC23:
	.ascii	"Moisture Sensor reading problem @ S/N %u\000"
.LC24:
	.ascii	"Moisture Sensor assigned to different soil types\000"
.LC25:
	.ascii	"MOISTURE: list item not found!\000"
.LC26:
	.ascii	"MOISTURE SENSOR: WARNING - sensor with multiple sta"
	.ascii	"tion groups and skewed start times!!\000"
.LC27:
	.ascii	"MOISTURE SENSOR: questionable sensor override, sn %"
	.ascii	"d, additional cycle irrigated\000"
.LC28:
	.ascii	"SYNC: no mem to calc checksum %s\000"
	.section	.rodata.MOISTURE_SENSOR_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	MOISTURE_SENSOR_FILENAME, %object
	.size	MOISTURE_SENSOR_FILENAME, 13
MOISTURE_SENSOR_FILENAME:
	.ascii	"MOIS_SENSORS\000"
	.section	.rodata.moisture_sensor_list_item_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	moisture_sensor_list_item_sizes, %object
	.size	moisture_sensor_list_item_sizes, 20
moisture_sensor_list_item_sizes:
	.word	184
	.word	184
	.word	204
	.word	212
	.word	252
	.section	.rodata.MOISTURE_SENSOR_DEFAULT_GROUP_NAME,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	MOISTURE_SENSOR_DEFAULT_GROUP_NAME, %object
	.size	MOISTURE_SENSOR_DEFAULT_GROUP_NAME, 16
MOISTURE_SENSOR_DEFAULT_GROUP_NAME:
	.ascii	"MOISTURE SENSOR\000"
	.section	.bss.record_count.9637,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	record_count.9637, %object
	.size	record_count.9637, 4
record_count.9637:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI4-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI5-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI7-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI9-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI11-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI13-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI15-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI17-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI19-.LFB10
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI20-.LCFI19
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI21-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI23-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI25-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI26-.LCFI25
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI27-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI29-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI31-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI32-.LCFI31
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI33-.LFB22
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI34-.LFB19
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI35-.LCFI34
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI36-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI38-.LFB21
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI39-.LFB23
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI40-.LFB30
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI41-.LFB31
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI42-.LFB32
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI43-.LFB33
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI44-.LFB34
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI45-.LFB29
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI46-.LFB26
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI47-.LFB25
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI48-.LFB35
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI49-.LFB36
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI50-.LFB37
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI51-.LFB38
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI52-.LFB39
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI53-.LFB40
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI54-.LFB41
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI55-.LFB42
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI56-.LFB43
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI57-.LFB17
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x83
	.uleb128 0x9
	.byte	0x82
	.uleb128 0xa
	.byte	0x81
	.uleb128 0xb
	.byte	0x80
	.uleb128 0xc
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI58-.LFB28
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI59-.LFB18
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI60-.LCFI59
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI61-.LFB27
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI62-.LCFI61
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE86:
.LSFDE88:
	.4byte	.LEFDE88-.LASFDE88
.LASFDE88:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI63-.LFB24
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI64-.LCFI63
	.byte	0xe
	.uleb128 0x68
	.align	2
.LEFDE88:
.LSFDE90:
	.4byte	.LEFDE90-.LASFDE90
.LASFDE90:
	.4byte	.Lframe0
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.byte	0x4
	.4byte	.LCFI65-.LFB45
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE90:
.LSFDE92:
	.4byte	.LEFDE92-.LASFDE92
.LASFDE92:
	.4byte	.Lframe0
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.byte	0x4
	.4byte	.LCFI66-.LFB46
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE92:
.LSFDE94:
	.4byte	.LEFDE94-.LASFDE94
.LASFDE94:
	.4byte	.Lframe0
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.byte	0x4
	.4byte	.LCFI67-.LFB47
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI68-.LCFI67
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE94:
.LSFDE96:
	.4byte	.LEFDE96-.LASFDE96
.LASFDE96:
	.4byte	.Lframe0
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.byte	0x4
	.4byte	.LCFI69-.LFB48
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE96:
.LSFDE98:
	.4byte	.LEFDE98-.LASFDE98
.LASFDE98:
	.4byte	.Lframe0
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.byte	0x4
	.4byte	.LCFI70-.LFB49
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE98:
.LSFDE100:
	.4byte	.LEFDE100-.LASFDE100
.LASFDE100:
	.4byte	.Lframe0
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.byte	0x4
	.4byte	.LCFI71-.LFB50
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE100:
.LSFDE102:
	.4byte	.LEFDE102-.LASFDE102
.LASFDE102:
	.4byte	.Lframe0
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.byte	0x4
	.4byte	.LCFI72-.LFB51
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE102:
.LSFDE104:
	.4byte	.LEFDE104-.LASFDE104
.LASFDE104:
	.4byte	.Lframe0
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.byte	0x4
	.4byte	.LCFI73-.LFB52
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE104:
.LSFDE106:
	.4byte	.LEFDE106-.LASFDE106
.LASFDE106:
	.4byte	.Lframe0
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.byte	0x4
	.4byte	.LCFI74-.LFB53
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE106:
.LSFDE108:
	.4byte	.LEFDE108-.LASFDE108
.LASFDE108:
	.4byte	.Lframe0
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.byte	0x4
	.4byte	.LCFI75-.LFB55
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI76-.LCFI75
	.byte	0xe
	.uleb128 0x28
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xa
	.byte	0x4
	.4byte	.LCFI77-.LCFI76
	.byte	0xe
	.uleb128 0x70
	.align	2
.LEFDE108:
.LSFDE110:
	.4byte	.LEFDE110-.LASFDE110
.LASFDE110:
	.4byte	.Lframe0
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.byte	0x4
	.4byte	.LCFI78-.LFB56
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE110:
.LSFDE112:
	.4byte	.LEFDE112-.LASFDE112
.LASFDE112:
	.4byte	.Lframe0
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.byte	0x4
	.4byte	.LCFI79-.LFB57
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE112:
.LSFDE114:
	.4byte	.LEFDE114-.LASFDE114
.LASFDE114:
	.4byte	.Lframe0
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.byte	0x4
	.4byte	.LCFI80-.LFB58
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE114:
.LSFDE116:
	.4byte	.LEFDE116-.LASFDE116
.LASFDE116:
	.4byte	.Lframe0
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.byte	0x4
	.4byte	.LCFI81-.LFB59
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE116:
.LSFDE118:
	.4byte	.LEFDE118-.LASFDE118
.LASFDE118:
	.4byte	.Lframe0
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.byte	0x4
	.4byte	.LCFI82-.LFB60
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE118:
.LSFDE120:
	.4byte	.LEFDE120-.LASFDE120
.LASFDE120:
	.4byte	.Lframe0
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.byte	0x4
	.4byte	.LCFI83-.LFB61
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE120:
.LSFDE122:
	.4byte	.LEFDE122-.LASFDE122
.LASFDE122:
	.4byte	.Lframe0
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.byte	0x4
	.4byte	.LCFI84-.LFB62
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE122:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/shared_moisture_sensors.c"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/moisture_sensors.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x539
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF62
	.byte	0x1
	.4byte	.LASF63
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1b0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x1d6
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x169
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST2
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x18a
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST3
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1fa
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x21e
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x244
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x26a
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x290
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x2b6
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x2dc
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x302
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST11
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x328
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST12
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x34e
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST13
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x374
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST14
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x39a
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST15
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x3c0
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST16
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x2
	.2byte	0x3c0
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST17
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x2
	.2byte	0x297
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST18
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF19
	.byte	0x2
	.2byte	0x373
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST19
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF20
	.byte	0x2
	.2byte	0x3b4
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST20
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF21
	.byte	0x2
	.2byte	0x42d
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST21
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF22
	.byte	0x2
	.2byte	0x6e2
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST22
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF23
	.byte	0x2
	.2byte	0x702
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST23
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF24
	.byte	0x2
	.2byte	0x71b
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST24
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF25
	.byte	0x2
	.2byte	0x73f
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST25
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF26
	.byte	0x2
	.2byte	0x76c
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST26
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF27
	.byte	0x2
	.2byte	0x6d1
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST27
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF28
	.byte	0x2
	.2byte	0x634
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST28
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF29
	.byte	0x2
	.2byte	0x5d3
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST29
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF30
	.byte	0x2
	.2byte	0x77a
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST30
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF31
	.byte	0x2
	.2byte	0x79a
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST31
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF32
	.byte	0x2
	.2byte	0x7c5
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST32
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF33
	.byte	0x2
	.2byte	0x80f
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST33
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF34
	.byte	0x2
	.2byte	0x832
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST34
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF35
	.byte	0x2
	.2byte	0x840
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST35
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF36
	.byte	0x2
	.2byte	0x853
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST36
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF37
	.byte	0x2
	.2byte	0x868
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST37
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF38
	.byte	0x2
	.2byte	0x884
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST38
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF39
	.byte	0x2
	.2byte	0x8bb
	.4byte	.LFB44
	.4byte	.LFE44
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x3e6
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST39
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF41
	.byte	0x2
	.2byte	0x681
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST40
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x4d4
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST41
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF43
	.byte	0x2
	.2byte	0x65f
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST42
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF44
	.byte	0x2
	.2byte	0x450
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST43
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF45
	.byte	0x2
	.2byte	0x8c1
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LLST44
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF46
	.byte	0x2
	.2byte	0x8f2
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LLST45
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF47
	.byte	0x2
	.2byte	0x921
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LLST46
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF48
	.byte	0x2
	.2byte	0x9cb
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LLST47
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF49
	.byte	0x2
	.2byte	0xa4d
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LLST48
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF50
	.byte	0x2
	.2byte	0xa71
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LLST49
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF51
	.byte	0x2
	.2byte	0xa92
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LLST50
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF52
	.byte	0x2
	.2byte	0xad0
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LLST51
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF53
	.byte	0x2
	.2byte	0xadb
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LLST52
	.uleb128 0x5
	.4byte	.LASF64
	.byte	0x2
	.2byte	0xb37
	.byte	0x1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF54
	.byte	0x2
	.2byte	0xb91
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LLST53
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF55
	.byte	0x2
	.2byte	0xd1c
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LLST54
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF56
	.byte	0x2
	.2byte	0xd75
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LLST55
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF57
	.byte	0x2
	.2byte	0xd9a
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LLST56
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF58
	.byte	0x2
	.2byte	0xdd8
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LLST57
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF59
	.byte	0x2
	.2byte	0xe28
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LLST58
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF60
	.byte	0x2
	.2byte	0xe87
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LLST59
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF61
	.byte	0x2
	.2byte	0xeca
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LLST60
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB0
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB1
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI8
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI10
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI12
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI14
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI16
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI18
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI20
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB11
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB12
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI24
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB13
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI26
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB14
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI28
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB15
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI30
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB16
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI32
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB22
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB19
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI35
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB20
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI37
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB21
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB23
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB30
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB31
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB32
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB33
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB34
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB29
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB26
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB25
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB35
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB36
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI49
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB37
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB38
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB39
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI52
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB40
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB41
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB42
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI55
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB43
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB17
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI57
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB28
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI58
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB18
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI59
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI60
	.4byte	.LFE18
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB27
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI61
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI62
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB24
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI64
	.4byte	.LFE24
	.2byte	0x3
	.byte	0x7d
	.sleb128 104
	.4byte	0
	.4byte	0
.LLST44:
	.4byte	.LFB45
	.4byte	.LCFI65
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI65
	.4byte	.LFE45
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST45:
	.4byte	.LFB46
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI66
	.4byte	.LFE46
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST46:
	.4byte	.LFB47
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI67
	.4byte	.LCFI68
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI68
	.4byte	.LFE47
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST47:
	.4byte	.LFB48
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI69
	.4byte	.LFE48
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST48:
	.4byte	.LFB49
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI70
	.4byte	.LFE49
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST49:
	.4byte	.LFB50
	.4byte	.LCFI71
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI71
	.4byte	.LFE50
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST50:
	.4byte	.LFB51
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI72
	.4byte	.LFE51
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST51:
	.4byte	.LFB52
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI73
	.4byte	.LFE52
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST52:
	.4byte	.LFB53
	.4byte	.LCFI74
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI74
	.4byte	.LFE53
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST53:
	.4byte	.LFB55
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI75
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI76
	.4byte	.LCFI77
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	.LCFI77
	.4byte	.LFE55
	.2byte	0x3
	.byte	0x7d
	.sleb128 112
	.4byte	0
	.4byte	0
.LLST54:
	.4byte	.LFB56
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI78
	.4byte	.LFE56
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST55:
	.4byte	.LFB57
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI79
	.4byte	.LFE57
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST56:
	.4byte	.LFB58
	.4byte	.LCFI80
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI80
	.4byte	.LFE58
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST57:
	.4byte	.LFB59
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI81
	.4byte	.LFE59
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST58:
	.4byte	.LFB60
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI82
	.4byte	.LFE60
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST59:
	.4byte	.LFB61
	.4byte	.LCFI83
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI83
	.4byte	.LFE61
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST60:
	.4byte	.LFB62
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI84
	.4byte	.LFE62
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x204
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF60:
	.ascii	"MOISTURE_SENSORS_this_ilc_has_irrigated_the_require"
	.ascii	"d_cycles\000"
.LASF50:
	.ascii	"MOISTURE_SENSOR_on_all_moisture_sensors_set_or_clea"
	.ascii	"r_commserver_change_bits\000"
.LASF23:
	.ascii	"nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sen"
	.ascii	"sor_with_this_box_index_and_decoder_serial_number\000"
.LASF38:
	.ascii	"MOISTURE_SENSOR_fill_out_recorder_record\000"
.LASF19:
	.ascii	"init_file_moisture_sensor\000"
.LASF1:
	.ascii	"nm_MOISTURE_SENSOR_set_physically_available\000"
.LASF17:
	.ascii	"nm_MOISTURE_SENSOR_set_default_values\000"
.LASF9:
	.ascii	"nm_MOISTURE_SENSOR_set_moisture_low_set_point_float"
	.ascii	"\000"
.LASF45:
	.ascii	"MOISTURE_SENSOR_clean_house_processing\000"
.LASF41:
	.ascii	"MOISTURE_SENSOR_extract_and_store_changes_from_GuiV"
	.ascii	"ars\000"
.LASF0:
	.ascii	"nm_MOISTURE_SENSOR_set_decoder_serial_number\000"
.LASF51:
	.ascii	"nm_MOISTURE_SENSOR_update_pending_change_bits\000"
.LASF35:
	.ascii	"MOISTURE_SENSOR_get_physically_available\000"
.LASF55:
	.ascii	"MOISTURE_SENSORS_initialize__saved__irrigation_cont"
	.ascii	"rol_variables_for_this_moisture_sensor\000"
.LASF6:
	.ascii	"nm_MOISTURE_SENSOR_set_low_temp_point\000"
.LASF4:
	.ascii	"nm_MOISTURE_SENSOR_set_in_use\000"
.LASF12:
	.ascii	"nm_MOISTURE_SENSOR_set_low_temp_action\000"
.LASF14:
	.ascii	"nm_MOISTURE_SENSOR_set_low_ec_point\000"
.LASF18:
	.ascii	"nm_moisture_sensor_structure_updater\000"
.LASF25:
	.ascii	"MOISTURE_SENSOR_this_decoder_is_in_use_and_physical"
	.ascii	"ly_available\000"
.LASF52:
	.ascii	"nm_MOISTURE_SENSOR_set_new_reading_slave_side_varia"
	.ascii	"bles\000"
.LASF59:
	.ascii	"MOISTURE_SENSORS_check_each_sensor_to_possibly_upda"
	.ascii	"te__saved__control_variables\000"
.LASF57:
	.ascii	"MOISTURE_SENSORS_update__transient__control_variabl"
	.ascii	"es_for_this_station\000"
.LASF39:
	.ascii	"MOISTURE_SENSOR_get_change_bits_ptr\000"
.LASF34:
	.ascii	"MOISTURE_SENSOR_get_list_count\000"
.LASF58:
	.ascii	"MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_"
	.ascii	"the_list\000"
.LASF28:
	.ascii	"MOISTURE_SENSOR_copy_latest_readings_into_guivars\000"
.LASF48:
	.ascii	"MOISTURE_SENSORS_check_each_sensor_for_a_crossed_th"
	.ascii	"reshold\000"
.LASF2:
	.ascii	"nm_MOISTURE_SENSOR_set_name\000"
.LASF43:
	.ascii	"MOISTURE_SENSOR_extract_and_store_group_name_from_G"
	.ascii	"uiVars\000"
.LASF15:
	.ascii	"nm_MOISTURE_SENSOR_set_high_ec_action\000"
.LASF44:
	.ascii	"MOISTURE_SENSOR_build_data_to_send\000"
.LASF10:
	.ascii	"nm_MOISTURE_SENSOR_set_high_temp_point\000"
.LASF54:
	.ascii	"MOISTURE_SENSOR_extract_moisture_reading_from_token"
	.ascii	"_response\000"
.LASF22:
	.ascii	"MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct\000"
.LASF27:
	.ascii	"nm_MOISTURE_SENSOR_load_group_name_into_guivar\000"
.LASF13:
	.ascii	"nm_MOISTURE_SENSOR_set_high_ec_point\000"
.LASF3:
	.ascii	"nm_MOISTURE_SENSOR_set_box_index\000"
.LASF7:
	.ascii	"nm_MOISTURE_SENSOR_set_additional_soak_seconds\000"
.LASF21:
	.ascii	"nm_MOISTURE_SENSOR_create_new_group\000"
.LASF33:
	.ascii	"MOISTURE_SENSOR_get_last_group_ID\000"
.LASF16:
	.ascii	"nm_MOISTURE_SENSOR_set_low_ec_action\000"
.LASF63:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/moisture_sensors.c\000"
.LASF40:
	.ascii	"nm_MOISTURE_SENSOR_store_changes\000"
.LASF5:
	.ascii	"nm_MOISTURE_SENSOR_set_moisture_control_mode\000"
.LASF29:
	.ascii	"MOISTURE_SENSOR_copy_group_into_guivars\000"
.LASF64:
	.ascii	"MOISTURE_SENSOR_find_soil_bulk_density\000"
.LASF42:
	.ascii	"nm_MOISTURE_SENSOR_extract_and_store_changes_from_c"
	.ascii	"omm\000"
.LASF8:
	.ascii	"nm_MOISTURE_SENSOR_set_moisture_high_set_point_floa"
	.ascii	"t\000"
.LASF11:
	.ascii	"nm_MOISTURE_SENSOR_set_high_temp_action\000"
.LASF49:
	.ascii	"MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to"
	.ascii	"_cause_distribution_in_the_next_token\000"
.LASF46:
	.ascii	"MOISTURE_SENSOR_set_not_physically_available_based_"
	.ascii	"upon_communication_scan_results\000"
.LASF26:
	.ascii	"MOISTURE_SENSOR_get_group_at_this_index\000"
.LASF37:
	.ascii	"MOISTURE_SENSOR_get_num_of_moisture_sensors_connect"
	.ascii	"ed\000"
.LASF62:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF30:
	.ascii	"MOISTURE_SENSOR_get_group_with_this_GID\000"
.LASF20:
	.ascii	"save_file_moisture_sensor\000"
.LASF31:
	.ascii	"MOISTURE_SENSOR_get_gid_of_group_at_this_index\000"
.LASF36:
	.ascii	"MOISTURE_SENSOR_get_decoder_serial_number\000"
.LASF24:
	.ascii	"MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_"
	.ascii	"number\000"
.LASF61:
	.ascii	"MOISTURE_SENSOR_calculate_chain_sync_crc\000"
.LASF56:
	.ascii	"MOISTURE_SENSORS_initialize__transient__control_var"
	.ascii	"iables_for_all_sensors\000"
.LASF32:
	.ascii	"MOISTURE_SENSOR_get_index_for_group_with_this_seria"
	.ascii	"l_number\000"
.LASF53:
	.ascii	"MOISTURE_SENSOR_find_a_sensor_reading_to_send_to_th"
	.ascii	"e_master\000"
.LASF47:
	.ascii	"MOISTURE_SENSOR_set_physically_available_or_create_"
	.ascii	"new_moisture_sensors_as_needed_based_upon_the_disco"
	.ascii	"very_results\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
