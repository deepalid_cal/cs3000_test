	.file	"device_GENERIC_GR_card.c"
	.text
.Ltext0:
	.section	.text.__GENERIC_gr_card_power_control,"ax",%progbits
	.align	2
	.global	__GENERIC_gr_card_power_control
	.type	__GENERIC_gr_card_power_control, %function
__GENERIC_gr_card_power_control:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, lr}
.LCFI0:
	bne	.L2
	ldr	r4, .L9
	cmp	r1, #0
	ldr	r3, [r4, #12]
	beq	.L3
	tst	r3, #524288
	ldmnefd	sp!, {r4, pc}
	ldr	r3, .L9+4
	ldr	r2, [r3, #80]
	ldr	r3, .L9+8
	ldr	r2, [r3, r2, asl #2]
	bl	Alert_device_powered_on_or_off
	mov	r3, #524288
	b	.L8
.L3:
	tst	r3, #524288
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L9+4
	ldr	r2, [r3, #80]
	ldr	r3, .L9+8
	ldr	r2, [r3, r2, asl #2]
	bl	Alert_device_powered_on_or_off
	mov	r3, #524288
	b	.L7
.L2:
	cmp	r0, #2
	bne	.L5
	ldr	r4, .L9
	cmp	r1, #0
	ldr	r3, [r4, #12]
	beq	.L6
	tst	r3, #4194304
	ldmnefd	sp!, {r4, pc}
	ldr	r3, .L9+4
	ldr	r2, [r3, #84]
	ldr	r3, .L9+8
	ldr	r2, [r3, r2, asl #2]
	bl	Alert_device_powered_on_or_off
	mov	r3, #4194304
.L8:
	str	r3, [r4, #4]
	ldmfd	sp!, {r4, pc}
.L6:
	tst	r3, #4194304
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L9+4
	ldr	r2, [r3, #84]
	ldr	r3, .L9+8
	ldr	r2, [r3, r2, asl #2]
	bl	Alert_device_powered_on_or_off
	mov	r3, #4194304
.L7:
	str	r3, [r4, #8]
	ldmfd	sp!, {r4, pc}
.L5:
	ldr	r0, .L9+12
	ldmfd	sp!, {r4, lr}
	b	Alert_Message
.L10:
	.align	2
.L9:
	.word	1073905664
	.word	config_c
	.word	comm_device_names
	.word	.LC0
.LFE0:
	.size	__GENERIC_gr_card_power_control, .-__GENERIC_gr_card_power_control
	.section	.text.GENERIC_GR_card_power_is_on,"ax",%progbits
	.align	2
	.global	GENERIC_GR_card_power_is_on
	.type	GENERIC_GR_card_power_is_on, %function
GENERIC_GR_card_power_is_on:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	bne	.L12
	ldr	r3, .L16
	ldr	r3, [r3, #12]
	tst	r3, #524288
	b	.L15
.L12:
	cmp	r0, #2
	bne	.L14
	ldr	r3, .L16
	ldr	r3, [r3, #12]
	tst	r3, #4194304
.L15:
	moveq	r0, #0
	movne	r0, #1
	bx	lr
.L14:
	mov	r0, #0
	bx	lr
.L17:
	.align	2
.L16:
	.word	1073905664
.LFE1:
	.size	GENERIC_GR_card_power_is_on, .-GENERIC_GR_card_power_is_on
	.section	.text.__GENERIC_gr_card_is_connected,"ax",%progbits
	.align	2
	.global	__GENERIC_gr_card_is_connected
	.type	__GENERIC_gr_card_is_connected, %function
__GENERIC_gr_card_is_connected:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	ldr	r3, .L26
	ldr	r1, .L26+4
	mov	r2, #56
	bne	.L19
	ldr	r1, [r1, #80]
	mla	r3, r2, r1, r3
	ldr	r3, [r3, #12]
	cmp	r3, #0
	ldr	r3, .L26+8
	ldr	r3, [r3, #0]
	beq	.L20
	tst	r3, #32
	moveq	r3, #0
	movne	r3, #1
	b	.L21
.L20:
	tst	r3, #32
	movne	r3, #0
	moveq	r3, #1
.L21:
	ldr	r2, .L26+12
	b	.L25
.L19:
	ldr	r1, [r1, #84]
	mla	r3, r2, r1, r3
	ldr	r3, [r3, #12]
	cmp	r3, #0
	ldr	r3, .L26+8
	ldr	r3, [r3, #0]
	beq	.L23
	tst	r3, #4
	moveq	r3, #0
	movne	r3, #1
	b	.L24
.L23:
	tst	r3, #4
	movne	r3, #0
	moveq	r3, #1
.L24:
	ldr	r2, .L26+16
.L25:
	ldr	r1, .L26+20
	strb	r3, [r1, r2]
	ldr	r2, .L26+24
	ldr	r3, .L26+20
	mla	r0, r2, r0, r3
	add	r0, r0, #20
	ldrb	r0, [r0, #3]	@ zero_extendqisi2
	bx	lr
.L27:
	.align	2
.L26:
	.word	port_device_table
	.word	config_c
	.word	1073905664
	.word	4303
	.word	8583
	.word	SerDrvrVars_s
	.word	4280
.LFE3:
	.size	__GENERIC_gr_card_is_connected, .-__GENERIC_gr_card_is_connected
	.section	.text.__GENERIC_gr_card_disconnect,"ax",%progbits
	.align	2
	.global	__GENERIC_gr_card_disconnect
	.type	__GENERIC_gr_card_disconnect, %function
__GENERIC_gr_card_disconnect:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r1, .L38
	cmp	r0, #1
	ldreq	r1, [r1, #80]
	ldrne	r1, [r1, #84]
	ldr	r3, .L38+4
	mov	r2, #56
	mla	r3, r2, r1, r3
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	ldr	r4, [r3, #24]
	mov	r5, r0
	eor	r1, r4, #1
	bl	SetDTR
	mov	r6, #400
.L32:
	mov	r0, r5
	bl	__GENERIC_gr_card_is_connected
	cmp	r0, #0
	beq	.L34
	mov	r0, #5
	bl	vTaskDelay
	subs	r6, r6, #1
	bne	.L32
	b	.L31
.L34:
	mov	r6, #1
.L31:
	mov	r0, #50
	bl	vTaskDelay
	mov	r0, r5
	mov	r1, r4
	bl	SetDTR
	mov	r0, #50
	bl	vTaskDelay
	cmp	r6, #0
	ldrne	r0, .L38+8
	ldreq	r0, .L38+12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_Message
.L39:
	.align	2
.L38:
	.word	config_c
	.word	port_device_table
	.word	.LC1
	.word	.LC2
.LFE2:
	.size	__GENERIC_gr_card_disconnect, .-__GENERIC_gr_card_disconnect
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"GR PWR CONTROL: invalid port\000"
.LC1:
	.ascii	"Disconnect : successful\000"
.LC2:
	.ascii	"Disconnect : failed\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_GENERIC_GR_card.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x28
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x63
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xe7
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x7d
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"__GENERIC_gr_card_disconnect\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_GENERIC_GR_card.c\000"
.LASF1:
	.ascii	"GENERIC_GR_card_power_is_on\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"__GENERIC_gr_card_is_connected\000"
.LASF0:
	.ascii	"__GENERIC_gr_card_power_control\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
