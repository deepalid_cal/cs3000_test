	.file	"df_storage_mngr.c"
	.text
.Ltext0:
	.section	.text.convert_code_image_date_to_version_number,"ax",%progbits
	.align	2
	.global	convert_code_image_date_to_version_number
	.type	convert_code_image_date_to_version_number, %function
convert_code_image_date_to_version_number:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L15
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	cmp	r1, r3
	addeq	r4, r0, #64
	beq	.L3
	ldr	r3, .L15+4
	cmp	r1, r3
	bne	.L4
	add	r4, r0, #208
.L3:
	ldr	r6, .L15+8
	mov	r5, #0
.L8:
	mov	r0, r4
	ldr	r1, [r6, #4]!
	mov	r2, #3
	bl	strncmp
	add	r5, r5, #1
	cmp	r0, #0
	bne	.L5
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	cmp	r3, #32
	addne	r0, r4, #4
	bne	.L13
	b	.L14
.L5:
	cmp	r5, #12
	bne	.L8
	b	.L4
.L14:
	add	r0, r4, #5
.L13:
	bl	atoi
	mov	r6, r0
	add	r0, r4, #7
	bl	atoi
	mov	r1, r5
	mov	r2, r0
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, lr}
	b	DMYToDate
.L4:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L16:
	.align	2
.L15:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2-4
.LFE0:
	.size	convert_code_image_date_to_version_number, .-convert_code_image_date_to_version_number
	.section	.text.convert_code_image_time_to_edit_count,"ax",%progbits
	.align	2
	.global	convert_code_image_time_to_edit_count
	.type	convert_code_image_time_to_edit_count, %function
convert_code_image_time_to_edit_count:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L30
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	cmp	r1, r3
	addeq	r4, r0, #64
	beq	.L19
	ldr	r3, .L30+4
	cmp	r1, r3
	bne	.L20
	add	r4, r0, #208
.L19:
	ldrb	r3, [r4, #14]	@ zero_extendqisi2
	cmp	r3, #32
	addne	r0, r4, #14
	addeq	r0, r4, #15
	bl	atoi
	ldrb	r3, [r4, #17]	@ zero_extendqisi2
	cmp	r3, #32
	mov	r6, r0
	addne	r0, r4, #17
	addeq	r0, r4, #18
	bl	atoi
	ldrb	r3, [r4, #20]	@ zero_extendqisi2
	cmp	r3, #32
	mov	r5, r0
	addne	r0, r4, #20
	addeq	r0, r4, #21
	bl	atoi
	mov	r1, r5
	mov	r2, r0
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, lr}
	b	HMSToTime
.L20:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L31:
	.align	2
.L30:
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE1:
	.size	convert_code_image_time_to_edit_count, .-convert_code_image_time_to_edit_count
	.section	.text.DfTakeStorageMgrMutex,"ax",%progbits
	.align	2
	.global	DfTakeStorageMgrMutex
	.type	DfTakeStorageMgrMutex, %function
DfTakeStorageMgrMutex:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L34
	mov	r2, r1
	mov	r1, #76
	mla	r3, r1, r0, r3
	mov	r1, #0
	ldr	r3, [r3, #20]
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericReceive
	cmp	r0, #1
	mov	r4, r0
	beq	.L33
	ldr	r0, .L34+4
	bl	Alert_Message
.L33:
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L35:
	.align	2
.L34:
	.word	ssp_define
	.word	.LC0
.LFE2:
	.size	DfTakeStorageMgrMutex, .-DfTakeStorageMgrMutex
	.section	.text.DfGiveStorageMgrMutex,"ax",%progbits
	.align	2
	.global	DfGiveStorageMgrMutex
	.type	DfGiveStorageMgrMutex, %function
DfGiveStorageMgrMutex:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L38
	mov	r2, #76
	mla	r3, r2, r0, r3
	mov	r1, #0
	ldr	r3, [r3, #20]
	str	lr, [sp, #-4]!
.LCFI3:
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
	cmp	r0, #1
	ldreq	pc, [sp], #4
	ldr	r0, .L38+4
	ldr	lr, [sp], #4
	b	Alert_Message
.L39:
	.align	2
.L38:
	.word	ssp_define
	.word	.LC1
.LFE3:
	.size	DfGiveStorageMgrMutex, .-DfGiveStorageMgrMutex
	.section	.text.DfOrCatBits,"ax",%progbits
	.align	2
	.global	DfOrCatBits
	.type	DfOrCatBits, %function
DfOrCatBits:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
.L41:
	ldr	ip, [r0, r3]
	ldr	r2, [r1, r3]
	orr	r2, ip, r2
	str	r2, [r0, r3]
	add	r3, r3, #4
	cmp	r3, #128
	bne	.L41
	bx	lr
.LFE11:
	.size	DfOrCatBits, .-DfOrCatBits
	.section	.text.DfClearCatBits,"ax",%progbits
	.align	2
	.global	DfClearCatBits
	.type	DfClearCatBits, %function
DfClearCatBits:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
.L44:
	ldr	ip, [r1, r3]
	ldr	r2, [r0, r3]
	bic	r2, r2, ip
	str	r2, [r0, r3]
	add	r3, r3, #4
	cmp	r3, #128
	bne	.L44
	bx	lr
.LFE12:
	.size	DfClearCatBits, .-DfClearCatBits
	.section	.text.DfDirEntryCompare,"ax",%progbits
	.align	2
	.global	DfDirEntryCompare
	.type	DfDirEntryCompare, %function
DfDirEntryCompare:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ands	r3, r2, #1
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI4:
	mov	r6, r0
	mov	r5, r1
	mov	r4, r2
	moveq	r0, r3
	beq	.L47
	mov	r2, #32
	bl	strncmp
	adds	r0, r0, #0
	movne	r0, #1
.L47:
	tst	r4, #2
	beq	.L48
	ldr	r2, [r6, #36]
	ldr	r3, [r5, #36]
	cmp	r2, r3
	orrne	r0, r0, #2
.L48:
	tst	r4, #4
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldr	r2, [r6, #44]
	ldr	r3, [r5, #44]
	cmp	r2, r3
	orrne	r0, r0, #4
	ldmfd	sp!, {r4, r5, r6, pc}
.LFE13:
	.size	DfDirEntryCompare, .-DfDirEntryCompare
	.section	.text.DF_test_cluster_bit,"ax",%progbits
	.align	2
	.global	DF_test_cluster_bit
	.type	DF_test_cluster_bit, %function
DF_test_cluster_bit:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r3, r1, #31
	mov	r1, r1, lsr #5
	ldr	r2, [r0, r1, asl #2]
	mov	r1, #1
	ands	r2, r2, r1, asl r3
	moveq	r0, #0
	movne	r0, #1
	bx	lr
.LFE16:
	.size	DF_test_cluster_bit, .-DF_test_cluster_bit
	.section	.text.DfFindCluster,"ax",%progbits
	.align	2
	.global	DfFindCluster
	.type	DfFindCluster, %function
DfFindCluster:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI5:
	ldr	r7, .L60
	mov	r6, r3
	ldr	r3, [r3, #0]
	mov	r4, r1
	cmp	r3, r7
	and	r8, r2, #255
	bhi	.L59
.LBB6:
	ldr	r2, .L60+4
	mov	r1, #76
	mla	r0, r1, r0, r2
.LBE6:
	ldr	r5, [r0, #40]
	cmp	r3, r5
	movcs	r5, r3
	b	.L55
.L58:
.LBB7:
	mov	r0, r4
	mov	r1, r5
	bl	DF_test_cluster_bit
	cmp	r8, r0
	addne	r5, r5, #1
	bne	.L55
.L56:
	str	r5, [r6, #0]
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L55:
	cmp	r5, r7
	bls	.L58
	mvn	r0, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L59:
.LBE7:
	mvn	r0, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L61:
	.align	2
.L60:
	.word	1023
	.word	ssp_define
.LFE4:
	.size	DfFindCluster, .-DfFindCluster
	.section	.text.DF_set_cluster_bit,"ax",%progbits
	.align	2
	.global	DF_set_cluster_bit
	.type	DF_set_cluster_bit, %function
DF_set_cluster_bit:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r2, r2, #255
	cmp	r2, #1
	mov	r3, r1, lsr #5
	ldreq	ip, [r0, r3, asl #2]
	ldrne	r2, [r0, r3, asl #2]
	and	r1, r1, #31
	movne	ip, #1
	orreq	r1, ip, r2, asl r1
	bicne	r1, r2, ip, asl r1
	str	r1, [r0, r3, asl #2]
	bx	lr
.LFE17:
	.size	DF_set_cluster_bit, .-DF_set_cluster_bit
	.section	.text.DF_read_page,"ax",%progbits
	.align	2
	.global	DF_read_page
	.type	DF_read_page, %function
DF_read_page:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI6:
	mov	r1, r1, asl #12
	bl	SPI_FLASH_fast_read_as_much_as_needed_to_buffer
	ldr	pc, [sp], #4
.LFE18:
	.size	DF_read_page, .-DF_read_page
	.section	.text.DfFindDirEntry_NM,"ax",%progbits
	.align	2
	.global	DfFindDirEntry_NM
	.type	DfFindDirEntry_NM, %function
DfFindDirEntry_NM:
.LFB15:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI7:
	ldr	r5, [sp, #36]
	mov	r6, r0
	mov	sl, r1
	mov	fp, r2
	mov	r0, #184
	ldr	r2, .L75
	ldr	r1, .L75+4
	mov	r7, r3
	bl	mem_malloc_debug
	ldr	r3, .L75+8
	mov	r2, #76
	mla	r3, r2, r6, r3
	ldr	r2, [r3, #72]
	cmp	r5, r2
	ldrcc	r8, [r3, #32]
	ldrcc	r9, [r3, #36]
	addcc	r5, r5, r8
	mov	r4, r0
	bcc	.L69
	b	.L74
.L72:
	mov	r0, r6
	mov	r1, r5
	mov	r2, r4
	mov	r3, #184
	bl	DF_read_page
	cmp	r0, #0
	bne	.L73
	ldrb	r3, [r4, #51]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L71
	mov	r0, sl
	mov	r1, r4
	mov	r2, r7
	bl	DfDirEntryCompare
	cmp	r0, #0
	cmpne	r7, #0
	bne	.L71
	mov	r0, fp
	mov	r1, r4
	mov	r2, #184
	bl	memcpy
	rsb	r5, r8, r5
	b	.L70
.L71:
	add	r5, r5, #1
.L69:
	cmp	r5, r9
	bls	.L72
	b	.L73
.L74:
	ldr	r0, .L75+12
	bl	Alert_Message
.L73:
	mvn	r5, #0
.L70:
	mov	r0, r4
	ldr	r1, .L75+4
	ldr	r2, .L75+16
	bl	mem_free_debug
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L76:
	.align	2
.L75:
	.word	1281
	.word	.LC2
	.word	ssp_define
	.word	.LC3
	.word	1323
.LFE15:
	.size	DfFindDirEntry_NM, .-DfFindDirEntry_NM
	.section	.text.DfGetFreeDirEntry_NM,"ax",%progbits
	.align	2
	.global	DfGetFreeDirEntry_NM
	.type	DfGetFreeDirEntry_NM, %function
DfGetFreeDirEntry_NM:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L83
	mov	r2, #76
	mla	r3, r2, r0, r3
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI8:
	ldr	r5, [r3, #32]
	mov	r6, r0
	mov	r7, r1
	mov	r4, r5
	ldr	r8, [r3, #36]
	b	.L78
.L81:
	mov	r0, r6
	mov	r1, r4
	mov	r2, r7
	mov	r3, #56
	bl	DF_read_page
	cmp	r0, #0
	bne	.L82
	ldrb	r3, [r7, #51]	@ zero_extendqisi2
	cmp	r3, #1
	addeq	r4, r4, #1
	beq	.L78
.L80:
	rsb	r0, r5, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L78:
	cmp	r4, r8
	bls	.L81
	mvn	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L82:
	mvn	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L84:
	.align	2
.L83:
	.word	ssp_define
.LFE9:
	.size	DfGetFreeDirEntry_NM, .-DfGetFreeDirEntry_NM
	.section	.text.DfGetFreeSpace_NM,"ax",%progbits
	.align	2
	.global	DfGetFreeSpace_NM
	.type	DfGetFreeSpace_NM, %function
DfGetFreeSpace_NM:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L91
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI9:
	mov	r4, #76
	mla	r4, r0, r4, r3
	mov	r3, #164
	ldr	r5, [r4, #24]
	ldr	r1, [r4, #28]
	mov	r2, r5
	bl	DF_read_page
	subs	r6, r0, #0
	bne	.L90
	add	r5, r5, #32
.LBB8:
	ldr	r4, [r4, #40]
	ldr	r7, .L91+4
	b	.L87
.L89:
	mov	r1, r4
	mov	r0, r5
	bl	DF_test_cluster_bit
	add	r4, r4, #1
	cmp	r0, #0
	addeq	r6, r6, #1
.L87:
	cmp	r4, r7
	bls	.L89
.LBE8:
	mov	r0, r6, asl #12
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L90:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L92:
	.align	2
.L91:
	.word	ssp_define
	.word	1023
.LFE8:
	.size	DfGetFreeSpace_NM, .-DfGetFreeSpace_NM
	.section	.text.DfReadFileFromDF_NM,"ax",%progbits
	.align	2
	.global	DfReadFileFromDF_NM
	.type	DfReadFileFromDF_NM, %function
DfReadFileFromDF_NM:
.LFB7:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI10:
	ldr	r5, [r1, #40]
	mov	r3, #0
	add	r6, sp, #8
	mov	sl, r0
	mov	r4, r1
	mov	r9, r2
	add	fp, r1, #56
	str	r3, [r6, #-4]!
	mov	r8, r2
	b	.L103
.L98:
	mov	r3, r6
	mov	r0, sl
	mov	r1, fp
	mov	r2, #1
	bl	DfFindCluster
	subs	r3, r0, #0
	ldrne	r0, .L105
	bne	.L104
	cmp	r5, #4096
	movcc	r7, r5
	movcs	r7, #4096
	mov	r3, r7
	mov	r0, sl
	ldr	r1, [sp, #4]
	mov	r2, r8
	bl	DF_read_page
	subs	r3, r0, #0
	beq	.L96
	cmn	r3, #10
	ldrne	r0, .L105+4
	bne	.L104
	b	.L103
.L96:
	ldr	r2, [sp, #4]
	rsb	r5, r7, r5
	add	r2, r2, #1
	str	r2, [sp, #4]
	add	r8, r8, r7
	b	.L103
.L104:
	str	r3, [sp, #0]
	bl	Alert_Message
	ldr	r3, [sp, #0]
.L103:
	adds	r2, r5, #0
	movne	r2, #1
	cmp	r3, #0
	movne	r2, #0
	cmp	r2, #0
	bne	.L98
	cmp	r3, #0
	bne	.L99
.LBB9:
	mov	r0, r9
	ldr	r1, [r4, #40]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, [r4, #52]
	cmp	r0, r3
	moveq	r3, #0
	mvnne	r3, #7
.L99:
.LBE9:
	mov	r0, r3
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L106:
	.align	2
.L105:
	.word	.LC5
	.word	.LC4
.LFE7:
	.size	DfReadFileFromDF_NM, .-DfReadFileFromDF_NM
	.section	.text.DF_write_page,"ax",%progbits
	.align	2
	.global	DF_write_page
	.type	DF_write_page, %function
DF_write_page:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI11:
	mov	r1, r1, asl #12
	bl	SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming
	ldr	pc, [sp], #4
.LFE19:
	.size	DF_write_page, .-DF_write_page
	.section	.text.DfDelFileFromDF_NM,"ax",%progbits
	.align	2
	.global	DfDelFileFromDF_NM
	.type	DfDelFileFromDF_NM, %function
DfDelFileFromDF_NM:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L110
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI12:
	mov	r6, #76
	mla	r6, r0, r6, r3
	add	sl, r2, #56
	ldr	r8, [r6, #24]
	mov	r5, r0
	mov	r7, r1
	add	r0, r8, #32
	mov	r1, sl
	mov	r4, r2
	bl	DfClearCatBits
	mov	r2, #128
	mov	r1, #0
	mov	r0, sl
	bl	memset
	mov	r1, #160
	mov	r0, r8
	bl	CRC_calculate_32bit_big_endian
	ldr	r1, [r6, #28]
	mov	r2, r8
	mov	r3, #164
	str	r0, [r8, #160]
	mov	r0, r5
	bl	DF_write_page
	cmp	r0, #0
	ldmnefd	sp!, {r4, r5, r6, r7, r8, sl, pc}
	ldr	r1, [r6, #32]
	strb	r0, [r4, #51]
	add	r1, r7, r1
	mov	r0, r5
	mov	r2, r4
	mov	r3, #184
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	DF_write_page
.L111:
	.align	2
.L110:
	.word	ssp_define
.LFE14:
	.size	DfDelFileFromDF_NM, .-DfDelFileFromDF_NM
	.section	.text.DfWriteFileToDF_NM,"ax",%progbits
	.align	2
	.global	DfWriteFileToDF_NM
	.type	DfWriteFileToDF_NM, %function
DfWriteFileToDF_NM:
.LFB10:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI13:
	mov	r4, r0
	sub	sp, sp, #20
.LCFI14:
	mov	r5, r1
	mov	r7, r2
	mov	r0, #184
	ldr	r1, .L138
	ldr	r2, .L138+4
	bl	mem_malloc_debug
	subs	fp, r0, #0
	beq	.L128
	ldr	r3, [r5, #40]
	cmp	r3, #0
	mvneq	sl, #3
	beq	.L117
	b	.L135
.L128:
	mvn	sl, #1
	b	.L117
.L137:
	cmn	r0, #1
	mvnne	sl, #2
	bne	.L117
	b	.L136
.L121:
	ldr	r3, [sp, #12]
	mov	r0, r4
	ldr	r1, [r3, #24]
	mov	r2, #0
	add	r1, r1, #32
	mov	r3, r8
	bl	DfFindCluster
	subs	sl, r0, #0
	ldrne	r0, .L138+8
	bne	.L134
	cmp	r6, #4096
	movcc	r5, r6
	movcs	r5, #4096
	mov	r0, r4
	ldr	r1, [sp, #16]
	mov	r2, r7
	mov	r3, r5
	bl	DF_write_page
	subs	sl, r0, #0
	beq	.L120
	cmn	sl, #10
	beq	.L132
	ldr	r0, .L138+12
.L134:
	bl	Alert_Message
	b	.L132
.L120:
	ldr	r0, [sp, #4]
	ldr	r1, [sp, #16]
	mov	r2, #1
	bl	DF_set_cluster_bit
	ldr	r3, [sp, #16]
	rsb	r6, r5, r6
	add	r3, r3, #1
	add	r7, r7, r5
	str	r3, [sp, #16]
.L132:
	adds	r3, r6, #0
	movne	r3, #1
	cmp	sl, #0
	movne	r3, #0
	cmp	r3, #0
	bne	.L121
	cmp	sl, #0
	mov	r5, r9
	bne	.L117
	mov	r3, #1
	strb	r3, [r9, #51]
	ldr	r3, .L138+16
	mov	r6, #76
	mla	r6, r4, r6, r3
	ldr	r3, [sp, #8]
	ldr	r1, [r6, #32]
	mov	r0, r4
	add	r1, r3, r1
	mov	r2, r9
	mov	r3, #184
	bl	DF_write_page
	subs	sl, r0, #0
	bne	.L117
	ldr	r5, [r6, #24]
	ldr	r1, [sp, #4]
	add	r0, r5, #32
	bl	DfOrCatBits
	mov	r1, #160
	mov	r0, r5
	bl	CRC_calculate_32bit_big_endian
	ldr	r1, [r6, #28]
	mov	r2, r5
	mov	r3, #164
	str	r0, [r5, #160]
	mov	r0, r4
	bl	DF_write_page
	mov	sl, r0
.L117:
	mov	r0, fp
	ldr	r1, .L138
	ldr	r2, .L138+20
	bl	mem_free_debug
	mov	r0, sl
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L135:
	mov	r0, r4
	mov	r1, fp
	bl	DfGetFreeDirEntry_NM
	cmn	r0, #1
	str	r0, [sp, #8]
	mvneq	sl, #4
	beq	.L117
	mov	r0, r4
	ldr	r6, [r5, #40]
	bl	DfGetFreeSpace_NM
	cmp	r6, r0
	bhi	.L128
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, r4
	mov	r3, #7
	mov	r1, r5
	mov	r2, fp
	bl	DfFindDirEntry_NM
	ldr	r3, .L138+24
	ldr	sl, [r3, #68]
	cmp	sl, #0
	mvnne	sl, #9
	bne	.L117
	b	.L137
.L136:
	add	r1, r5, #56
	mov	r2, #128
	str	r1, [sp, #4]
	mov	r0, r1
	mov	r1, sl
	bl	memset
	ldr	r1, [r5, #40]
	mov	r0, r7
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L138+16
	mov	r2, #76
	mla	r1, r2, r4, r3
	add	r8, sp, #20
	ldr	r6, [r5, #40]
	str	sl, [r8, #-4]!
	str	r1, [sp, #12]
	mov	r9, r5
	str	r0, [r5, #52]
	b	.L132
.L139:
	.align	2
.L138:
	.word	.LC2
	.word	913
	.word	.LC6
	.word	.LC7
	.word	ssp_define
	.word	1065
	.word	restart_info
.LFE10:
	.size	DfWriteFileToDF_NM, .-DfWriteFileToDF_NM
	.section	.text.init_FLASH_DRIVE_file_system,"ax",%progbits
	.align	2
	.global	init_FLASH_DRIVE_file_system
	.type	init_FLASH_DRIVE_file_system, %function
init_FLASH_DRIVE_file_system:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI15:
	ldr	r6, .L148
	mov	r5, r0
	mov	r7, #76
	mul	r7, r5, r7
	and	r8, r1, #255
	mvn	r1, #0
	bl	DfTakeStorageMgrMutex
	add	r3, r6, r7
	ldr	r4, [r3, #24]
	ldr	r1, [r3, #28]
	mov	r2, r4
	mov	r3, #164
	mov	r0, r5
	bl	DF_read_page
	mov	r0, r4
	mov	r1, #160
	bl	CRC_calculate_32bit_big_endian
	cmp	r8, #1
	mov	sl, r0
	beq	.L146
	add	r1, r7, #44
	mov	r2, #27
	mov	r0, r4
	add	r1, r6, r1
	bl	strncmp
	cmp	r0, #0
	movne	r2, #2
	bne	.L141
	ldr	r3, [r4, #160]
	cmp	sl, r3
	movne	r2, #1
	bne	.L141
	b	.L142
.L146:
	mov	r2, #3
.L141:
	ldr	r3, .L148+4
	sub	r2, r2, #1
	ldr	r2, [r3, r2, asl #2]
	ldr	r0, .L148+8
	mov	r1, r5
	bl	Alert_Message_va
	ldr	r1, .L148+12
	ldr	r2, .L148+16
	mov	r0, #184
	bl	mem_malloc_debug
	mov	r2, #184
	mov	r1, #0
	mov	r7, r0
	bl	memset
	ldr	r3, .L148
	mov	r2, #76
	mla	r3, r2, r5, r3
	ldr	r6, [r3, #32]
	ldr	r8, [r3, #36]
	b	.L143
.L145:
	mov	r0, r5
	mov	r1, r6
	mov	r2, r7
	mov	r3, #184
	bl	DF_write_page
	subs	sl, r0, #0
	bne	.L144
	add	r6, r6, #1
.L143:
	cmp	r6, r8
	bls	.L145
	mov	sl, #0
.L144:
	mov	r6, #76
	mul	r6, r5, r6
	mov	r0, r7
	ldr	r1, .L148+12
	mov	r2, #632
	ldr	r7, .L148
	bl	mem_free_debug
	mov	r1, #0
	mov	r2, #164
	mov	r0, r4
	bl	memset
	add	r1, r6, #44
	add	r1, r7, r1
	mov	r2, #27
	mov	r0, r4
	bl	memcpy
	mov	r0, r4
	mov	r1, #160
	bl	CRC_calculate_32bit_big_endian
	cmp	sl, #0
	str	r0, [r4, #160]
	bne	.L142
	add	r6, r7, r6
	mov	r0, r5
	ldr	r1, [r6, #28]
	mov	r2, r4
	mov	r3, #164
	bl	DF_write_page
.L142:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	DfGiveStorageMgrMutex
.L149:
	.align	2
.L148:
	.word	ssp_define
	.word	.LANCHOR3
	.word	.LC8
	.word	.LC2
	.word	610
.LFE6:
	.size	init_FLASH_DRIVE_file_system, .-init_FLASH_DRIVE_file_system
	.global	format_reason
	.global	TPMICRO_APP_FILENAME
	.global	CS3000_APP_FILENAME
	.section	.rodata.CS3000_APP_FILENAME,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	CS3000_APP_FILENAME, %object
	.size	CS3000_APP_FILENAME, 12
CS3000_APP_FILENAME:
	.ascii	"CS_3000_APP\000"
	.section	.data.format_reason,"aw",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	format_reason, %object
	.size	format_reason, 12
format_reason:
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"DF_MNGR: couldn't take mutex!\000"
.LC1:
	.ascii	"DF_MNGR: couldn't give mutex!\000"
.LC2:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/../commo"
	.ascii	"n_includes/df_storage_mngr.c\000"
.LC3:
	.ascii	"DF Find Dir Entry: start index out of range\000"
.LC4:
	.ascii	"DF MNGR: error reading cluster\000"
.LC5:
	.ascii	"DF MNGR: error finding cluster\000"
.LC6:
	.ascii	"DF MNGR: error finding avail cluster\000"
.LC7:
	.ascii	"DF MNGR: error during write.\000"
.LC8:
	.ascii	"FORMATTING FLASH DRIVE : chip %d, reason is %s\000"
.LC9:
	.ascii	"CRC Failed\000"
.LC10:
	.ascii	"ID String Failed\000"
.LC11:
	.ascii	"Forced\000"
.LC12:
	.ascii	"Jan\000"
.LC13:
	.ascii	"Feb\000"
.LC14:
	.ascii	"Mar\000"
.LC15:
	.ascii	"Apr\000"
.LC16:
	.ascii	"May\000"
.LC17:
	.ascii	"Jun\000"
.LC18:
	.ascii	"Jul\000"
.LC19:
	.ascii	"Aug\000"
.LC20:
	.ascii	"Sep\000"
.LC21:
	.ascii	"Oct\000"
.LC22:
	.ascii	"Nov\000"
.LC23:
	.ascii	"Dec\000"
	.section	.rodata.TPMICRO_APP_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	TPMICRO_APP_FILENAME, %object
	.size	TPMICRO_APP_FILENAME, 13
TPMICRO_APP_FILENAME:
	.ascii	"TP_MICRO_APP\000"
	.section	.rodata.df_month_strings,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	df_month_strings, %object
	.size	df_month_strings, 48
df_month_strings:
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	.LC23
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI4-.LFB13
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI5-.LFB4
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI6-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI7-.LFB15
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI8-.LFB9
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI9-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI10-.LFB7
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI11-.LFB19
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI12-.LFB14
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI13-.LFB10
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI15-.LFB6
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE36:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/df_storage_mngr.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1b6
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF18
	.byte	0x1
	.4byte	.LASF19
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x155
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x201
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x4d
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x9e
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x119
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x137
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x438
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x44d
	.4byte	.LFB12
	.4byte	.LFE12
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x475
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x53b
	.4byte	.LFB16
	.4byte	.LFE16
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.4byte	0x21
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x55a
	.4byte	.LFB17
	.4byte	.LFE17
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x571
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST6
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x4eb
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST7
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x343
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST8
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x321
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST9
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x2af
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST10
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x582
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST11
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x4a5
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST12
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x379
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST13
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x228
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST14
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB13
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB18
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB15
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB9
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB7
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB19
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB14
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB10
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI14
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB6
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xac
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"DfOrCatBits\000"
.LASF8:
	.ascii	"DF_set_cluster_bit\000"
.LASF19:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/../commo"
	.ascii	"n_includes/df_storage_mngr.c\000"
.LASF20:
	.ascii	"DfFindCluster\000"
.LASF12:
	.ascii	"DfGetFreeSpace_NM\000"
.LASF14:
	.ascii	"DF_write_page\000"
.LASF11:
	.ascii	"DfGetFreeDirEntry_NM\000"
.LASF17:
	.ascii	"init_FLASH_DRIVE_file_system\000"
.LASF18:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"DfClearCatBits\000"
.LASF9:
	.ascii	"DF_read_page\000"
.LASF16:
	.ascii	"DfWriteFileToDF_NM\000"
.LASF0:
	.ascii	"convert_code_image_date_to_version_number\000"
.LASF15:
	.ascii	"DfDelFileFromDF_NM\000"
.LASF6:
	.ascii	"DfDirEntryCompare\000"
.LASF13:
	.ascii	"DfReadFileFromDF_NM\000"
.LASF2:
	.ascii	"DfTakeStorageMgrMutex\000"
.LASF3:
	.ascii	"DfGiveStorageMgrMutex\000"
.LASF1:
	.ascii	"convert_code_image_time_to_edit_count\000"
.LASF7:
	.ascii	"DF_test_cluster_bit\000"
.LASF10:
	.ascii	"DfFindDirEntry_NM\000"
.LASF21:
	.ascii	"DfCountAvailClusters\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
