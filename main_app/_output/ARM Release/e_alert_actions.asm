	.file	"e_alert_actions.c"
	.text
.Ltext0:
	.section	.text.FDTO_ALERT_ACTIONS_show_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_ALERT_ACTIONS_show_dropdown, %function
FDTO_ALERT_ACTIONS_show_dropdown:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	ldr	r0, .L2+4
	ldr	r3, [r3, #0]
	ldr	r1, .L2+8
	ldr	r3, [r3, #0]
	mov	r2, #3
	b	FDTO_COMBOBOX_show
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	718
	.word	FDTO_COMBOBOX_add_items
.LFE0:
	.size	FDTO_ALERT_ACTIONS_show_dropdown, .-FDTO_ALERT_ACTIONS_show_dropdown
	.section	.text.FDTO_ALERT_ACTIONS_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_ALERT_ACTIONS_draw_screen
	.type	FDTO_ALERT_ACTIONS_draw_screen, %function
FDTO_ALERT_ACTIONS_draw_screen:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L7
	str	lr, [sp, #-4]!
.LCFI0:
	ldrnesh	r1, [r3, #0]
	bne	.L6
	bl	ALERT_ACTIONS_copy_group_into_guivars
	mov	r1, #0
.L6:
	mov	r0, #8
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L8:
	.align	2
.L7:
	.word	GuiLib_ActiveCursorFieldNo
.LFE1:
	.size	FDTO_ALERT_ACTIONS_draw_screen, .-FDTO_ALERT_ACTIONS_draw_screen
	.global	__modsi3
	.section	.text.ALERT_ACTIONS_process_screen,"ax",%progbits
	.align	2
	.global	ALERT_ACTIONS_process_screen
	.type	ALERT_ACTIONS_process_screen, %function
ALERT_ACTIONS_process_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L89
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldrsh	r2, [r3, #0]
	ldr	r3, .L89+4
	sub	sp, sp, #44
.LCFI2:
	cmp	r2, r3
	mov	r4, r0
	mov	r5, r1
	bne	.L80
	ldr	r3, .L89+8
	ldr	r1, [r3, #0]
	bl	COMBO_BOX_key_press
	b	.L9
.L80:
	cmp	r0, #4
	beq	.L18
	bhi	.L21
	cmp	r0, #1
	beq	.L15
	bcc	.L14
	cmp	r0, #2
	beq	.L16
	cmp	r0, #3
	bne	.L13
	b	.L88
.L21:
	cmp	r0, #67
	beq	.L19
	bhi	.L22
	cmp	r0, #16
	beq	.L18
	cmp	r0, #20
	bne	.L13
	b	.L14
.L22:
	cmp	r0, #80
	beq	.L20
	cmp	r0, #84
	bne	.L13
	b	.L20
.L16:
	ldr	r3, .L89+12
	ldrsh	r2, [r3, #0]
	ldr	r3, .L89+8
	cmp	r2, #19
	ldrls	pc, [pc, r2, asl #2]
	b	.L23
.L44:
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
.L24:
	ldr	r2, .L89+16
	b	.L81
.L25:
	ldr	r2, .L89+20
	b	.L81
.L26:
	ldr	r2, .L89+24
	b	.L81
.L27:
	ldr	r2, .L89+28
	b	.L81
.L28:
	ldr	r2, .L89+32
	b	.L81
.L29:
	ldr	r2, .L89+36
	b	.L81
.L30:
	ldr	r2, .L89+40
	b	.L81
.L31:
	ldr	r2, .L89+44
	b	.L81
.L32:
	ldr	r2, .L89+48
	b	.L81
.L33:
	ldr	r2, .L89+52
	b	.L81
.L34:
	ldr	r2, .L89+56
	b	.L81
.L35:
	ldr	r2, .L89+60
	b	.L81
.L36:
	ldr	r2, .L89+64
	b	.L81
.L37:
	ldr	r2, .L89+68
	b	.L81
.L38:
	ldr	r2, .L89+72
	b	.L81
.L39:
	ldr	r2, .L89+76
	b	.L81
.L40:
	ldr	r2, .L89+80
	b	.L81
.L41:
	ldr	r2, .L89+84
	b	.L81
.L42:
	ldr	r2, .L89+88
	b	.L81
.L43:
	ldr	r2, .L89+92
.L81:
	str	r2, [r3, #0]
.L23:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L78
	bl	good_key_beep
	ldr	r3, .L89+12
	ldr	r4, .L89+96
	ldrh	r0, [r3, #0]
	ldr	r3, .L89+100
	cmp	r0, #0
	streq	r0, [r3, #0]
	beq	.L82
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	cmp	r0, #10
	moveq	r2, #94
	streq	r2, [r3, #0]
	moveq	r3, #0
	streq	r3, [r4, #0]
	beq	.L47
.L48:
	cmp	r0, #9
	movgt	r2, #94
	movle	r2, #0
	str	r2, [r3, #0]
	mov	r1, #10
	bl	__modsi3
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	add	r0, r0, r0, asl #3
	mov	r0, r0, asl #1
.L82:
	str	r0, [r4, #0]
.L47:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L89+104
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L9
.L20:
	ldr	r3, .L89+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #19
	ldrls	pc, [pc, r3, asl #2]
	b	.L50
.L71:
	.word	.L51
	.word	.L52
	.word	.L53
	.word	.L54
	.word	.L55
	.word	.L56
	.word	.L57
	.word	.L58
	.word	.L59
	.word	.L60
	.word	.L61
	.word	.L62
	.word	.L63
	.word	.L64
	.word	.L65
	.word	.L66
	.word	.L67
	.word	.L68
	.word	.L69
	.word	.L70
.L51:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+16
	b	.L83
.L52:
	ldr	r1, .L89+20
	mov	r3, #1
	mov	r0, r4
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L83:
	mov	r2, #0
	mov	r3, #2
	bl	process_uns32
	b	.L72
.L53:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+24
	b	.L83
.L54:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+28
	b	.L83
.L55:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+32
	b	.L83
.L56:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+36
	b	.L83
.L57:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+40
	b	.L83
.L58:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+44
	b	.L83
.L59:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+48
	b	.L83
.L60:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+52
	b	.L83
.L61:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+56
	b	.L83
.L62:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+60
	b	.L83
.L63:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+64
	b	.L83
.L64:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+68
	b	.L83
.L65:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+72
	b	.L83
.L66:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+76
	b	.L83
.L67:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+80
	b	.L83
.L68:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+84
	b	.L83
.L69:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+88
	b	.L83
.L70:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L89+92
	b	.L83
.L50:
	bl	bad_key_beep
.L72:
	mov	r0, #0
	bl	Redraw_Screen
	b	.L9
.L18:
	ldr	r3, .L89+12
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	movs	r0, r0, asl #16
	beq	.L78
.L73:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L9
.L14:
	ldr	r3, .L89+12
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	bl	STATION_GROUP_get_num_groups_in_use
	sub	r0, r0, #1
	cmp	r4, r0
	beq	.L78
.L74:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L9
.L15:
	ldr	r3, .L89+12
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #9
	movls	r0, r0, asr #16
	addls	r0, r0, #9
	bls	.L87
	sub	r3, r3, #10
	mov	r3, r3, asl #16
	cmp	r3, #589824
	bhi	.L78
	mov	r0, r0, asr #16
	sub	r0, r0, #10
.L87:
	mov	r1, r4
	b	.L86
.L88:
	ldr	r3, .L89+12
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #9
	movls	r0, r0, asr #16
	addls	r0, r0, #10
	bls	.L85
	sub	r3, r3, #10
	mov	r3, r3, asl #16
	cmp	r3, #589824
	bhi	.L78
	mov	r0, r0, asr #16
	sub	r0, r0, #9
.L85:
	mov	r1, #1
.L86:
	bl	CURSOR_Select
	b	.L9
.L78:
	bl	bad_key_beep
	b	.L9
.L19:
	ldr	r3, .L89+108
	mov	r2, #3
	str	r2, [r3, #0]
	bl	ALERT_ACTIONS_extract_and_store_changes_from_GuiVars
.L13:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L9:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L90:
	.align	2
.L89:
	.word	GuiLib_CurStructureNdx
	.word	718
	.word	.LANCHOR0
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSettingB_9
	.word	GuiVar_ComboBox_Y1
	.word	GuiVar_ComboBox_X1
	.word	FDTO_ALERT_ACTIONS_show_dropdown
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	ALERT_ACTIONS_process_screen, .-ALERT_ACTIONS_process_screen
	.section	.bss.g_ALERT_ACTIONS_combo_box_guivar,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_ALERT_ACTIONS_combo_box_guivar, %object
	.size	g_ALERT_ACTIONS_combo_box_guivar, 4
g_ALERT_ACTIONS_combo_box_guivar:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_alert_actions.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x58
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x4a
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x50
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x73
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_alert_actions.c\000"
.LASF0:
	.ascii	"FDTO_ALERT_ACTIONS_draw_screen\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"FDTO_ALERT_ACTIONS_show_dropdown\000"
.LASF1:
	.ascii	"ALERT_ACTIONS_process_screen\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
