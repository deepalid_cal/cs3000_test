	.file	"e_freeze_switch.c"
	.text
.Ltext0:
	.section	.text.FDTO_FREEZE_SWITCH_enter_freeze_switch_scrollbox,"ax",%progbits
	.align	2
	.type	FDTO_FREEZE_SWITCH_enter_freeze_switch_scrollbox, %function
FDTO_FREEZE_SWITCH_enter_freeze_switch_scrollbox:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	bl	good_key_beep
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	ldr	r3, .L2
	ldr	r1, .L2+4
	ldrsh	r2, [r3, #0]
	mov	r0, #1
	mov	r3, #0
	bl	GuiLib_ScrollBox_Init
	mov	r0, #1
	bl	GuiLib_Cursor_Select
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	FREEZE_SWITCH_populate_freeze_switch_scrollbox
.LFE2:
	.size	FDTO_FREEZE_SWITCH_enter_freeze_switch_scrollbox, .-FDTO_FREEZE_SWITCH_enter_freeze_switch_scrollbox
	.section	.text.FREEZE_SWITCH_populate_freeze_switch_scrollbox,"ax",%progbits
	.align	2
	.type	FREEZE_SWITCH_populate_freeze_switch_scrollbox, %function
FREEZE_SWITCH_populate_freeze_switch_scrollbox:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r4, .L5
	mov	r0, r0, asl #16
	mov	r5, r0, asr #16
	add	r3, r4, r5, asl #3
	ldr	r0, [r3, #4]
	ldr	r1, .L5+4
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	ldr	r2, [r4, r5, asl #3]
	ldr	r3, .L5+8
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, pc}
.L6:
	.align	2
.L5:
	.word	.LANCHOR1
	.word	GuiVar_FreezeSwitchControllerName
	.word	GuiVar_FreezeSwitchSelected
.LFE0:
	.size	FREEZE_SWITCH_populate_freeze_switch_scrollbox, .-FREEZE_SWITCH_populate_freeze_switch_scrollbox
	.section	.text.FDTO_FREEZE_SWITCH_leave_freeze_switch_scrollbox,"ax",%progbits
	.align	2
	.type	FDTO_FREEZE_SWITCH_leave_freeze_switch_scrollbox, %function
FDTO_FREEZE_SWITCH_leave_freeze_switch_scrollbox:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI2:
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	ldr	r3, .L8
	ldr	r1, .L8+4
	ldrsh	r2, [r3, #0]
	mov	r0, #1
	mvn	r3, #0
	bl	GuiLib_ScrollBox_Init
	mov	r0, #1
	ldr	lr, [sp], #4
	b	FDTO_Cursor_Up
.L9:
	.align	2
.L8:
	.word	.LANCHOR0
	.word	FREEZE_SWITCH_populate_freeze_switch_scrollbox
.LFE3:
	.size	FDTO_FREEZE_SWITCH_leave_freeze_switch_scrollbox, .-FDTO_FREEZE_SWITCH_leave_freeze_switch_scrollbox
	.section	.text.FDTO_FREEZE_SWITCH_show_switch_connected_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_FREEZE_SWITCH_show_switch_connected_dropdown, %function
FDTO_FREEZE_SWITCH_show_switch_connected_dropdown:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	mov	r5, r0
	mov	r4, r1
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L11
	mov	r1, r4
	str	r0, [r3, #0]
	ldr	r3, .L11+4
	mov	r0, r5
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L12:
	.align	2
.L11:
	.word	.LANCHOR2
	.word	.LANCHOR3
.LFE5:
	.size	FDTO_FREEZE_SWITCH_show_switch_connected_dropdown, .-FDTO_FREEZE_SWITCH_show_switch_connected_dropdown
	.section	.text.FDTO_FREEZE_SWITCH_show_freeze_switch_in_use_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_FREEZE_SWITCH_show_freeze_switch_in_use_dropdown, %function
FDTO_FREEZE_SWITCH_show_freeze_switch_in_use_dropdown:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L14
	mov	r0, #199
	ldr	r3, [r3, #0]
	mov	r1, #18
	ldr	r2, [r3, #0]
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L15:
	.align	2
.L14:
	.word	.LANCHOR3
.LFE4:
	.size	FDTO_FREEZE_SWITCH_show_freeze_switch_in_use_dropdown, .-FDTO_FREEZE_SWITCH_show_freeze_switch_in_use_dropdown
	.section	.text.FDTO_FREEZE_SWITCH_close_freeze_switch_connected_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_FREEZE_SWITCH_close_freeze_switch_connected_dropdown, %function
FDTO_FREEZE_SWITCH_close_freeze_switch_connected_dropdown:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI4:
	ldr	r4, .L17
	mov	r1, #0
	mov	r0, #1
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r6, [r4, #0]
	mov	r5, r0
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, [r4, #0]
	str	r0, [r6, #0]
	ldr	r6, [r3, #0]
	ldr	r3, .L17+4
	add	r5, r3, r5, asl #3
	ldr	r4, [r5, #4]
	bl	FLOWSENSE_get_controller_index
	mov	r5, r0
	mov	r0, #2
	bl	WEATHER_get_change_bits_ptr
	mov	r2, #1
	mov	r1, r4
	mov	r3, #2
	str	r5, [sp, #0]
	str	r2, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r6
	bl	nm_WEATHER_set_freeze_switch_connected
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	FDTO_COMBOBOX_hide
.L18:
	.align	2
.L17:
	.word	.LANCHOR3
	.word	.LANCHOR1
.LFE6:
	.size	FDTO_FREEZE_SWITCH_close_freeze_switch_connected_dropdown, .-FDTO_FREEZE_SWITCH_close_freeze_switch_connected_dropdown
	.section	.text.FDTO_FREEZE_SWITCH_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_FREEZE_SWITCH_draw_screen
	.type	FDTO_FREEZE_SWITCH_draw_screen, %function
FDTO_FREEZE_SWITCH_draw_screen:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L29
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI5:
	ldrnesh	r1, [r3, #0]
	mov	r4, r0
	bne	.L21
	bl	WEATHER_copy_freeze_switch_settings_into_GuiVars
	mov	r1, #0
.L21:
	mov	r0, #22
	mov	r2, #1
	bl	GuiLib_ShowScreen
.LBB4:
	ldr	r3, .L29+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L22
	cmp	r4, #1
	beq	.L23
	ldr	r3, .L29
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	ldrne	r3, .L29+8
	ldrne	r4, [r3, #0]
	bne	.L27
.L23:
	ldr	r3, .L29+12
	ldr	r4, .L29+16
	mov	r5, #0
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L29+20
	mov	r3, #91
	str	r5, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L29+24
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L29+20
	mov	r3, #93
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L29+20
	mov	r1, #97
	bl	COMM_MNGR_alert_if_chain_members_should_not_be_referenced
	ldr	r6, .L29+28
	ldr	r7, .L29+32
	mov	r8, r6
.L26:
	ldr	r3, [r6, #16]
	cmp	r3, #1
	bne	.L25
	mov	r3, #92
	mla	r3, r5, r3, r8
	ldr	r2, [r3, #60]
	add	r3, r3, #28
	cmp	r2, #0
	beq	.L25
	ldr	r3, [r3, #36]
	cmp	r3, #0
	beq	.L25
	mov	r0, r5
	ldr	sl, [r4, #0]
	bl	WEATHER_get_freeze_switch_connected_to_this_controller
	ldr	r3, [r4, #0]
	add	r2, r7, r3, asl #3
	add	r3, r3, #1
	str	r5, [r2, #4]
	str	r3, [r4, #0]
	str	r0, [r7, sl, asl #3]
.L25:
	add	r5, r5, #1
	cmp	r5, #12
	add	r6, r6, #92
	bne	.L26
	ldr	r3, .L29+24
	mvn	r4, #0
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L29+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L27:
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	ldr	r2, .L29+16
	mov	r3, r4, asl #16
	mov	r0, #1
	ldr	r1, .L29+36
	ldrsh	r2, [r2, #0]
	mov	r3, r3, asr #16
	bl	GuiLib_ScrollBox_Init
.L22:
.LBE4:
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LBB5:
	b	GuiLib_Refresh
.L30:
	.align	2
.L29:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_FreezeSwitchInUse
	.word	.LANCHOR2
	.word	chain_members_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC0
	.word	weather_control_recursive_MUTEX
	.word	chain
	.word	.LANCHOR1
	.word	FREEZE_SWITCH_populate_freeze_switch_scrollbox
.LBE5:
.LFE7:
	.size	FDTO_FREEZE_SWITCH_draw_screen, .-FDTO_FREEZE_SWITCH_draw_screen
	.section	.text.FREEZE_SWITCH_process_screen,"ax",%progbits
	.align	2
	.global	FREEZE_SWITCH_process_screen
	.type	FREEZE_SWITCH_process_screen, %function
FREEZE_SWITCH_process_screen:
.LFB8:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L72
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI6:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #48
.LCFI7:
	cmp	r3, #740
	mov	r4, r0
	mov	r5, r1
	bne	.L60
	ldr	r3, .L72+4
	ldr	r1, [r3, #0]
	ldr	r3, .L72+8
	cmp	r1, r3
	beq	.L36
.L34:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L36
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #12]
	ldr	r3, .L72+12
	add	r0, sp, #12
	str	r3, [sp, #32]
	bl	Display_Post_Command
	mov	r0, #0
	b	.L65
.L36:
	bl	COMBO_BOX_key_press
	b	.L31
.L60:
	cmp	r0, #4
	beq	.L39
	bhi	.L42
	cmp	r0, #1
	beq	.L39
	bcc	.L38
	cmp	r0, #2
	beq	.L40
	cmp	r0, #3
	bne	.L37
	b	.L38
.L42:
	cmp	r0, #20
	beq	.L38
	bhi	.L43
	cmp	r0, #16
	bne	.L37
	b	.L39
.L43:
	cmp	r0, #80
	beq	.L41
	cmp	r0, #84
	bne	.L37
	b	.L41
.L40:
	ldr	r3, .L72+16
	ldrsh	r4, [r3, #0]
	cmp	r4, #0
	beq	.L45
	cmp	r4, #1
	bne	.L64
	b	.L69
.L45:
	bl	good_key_beep
	ldr	r3, .L72+4
	ldr	r2, .L72+8
	str	r2, [r3, #0]
	mov	r3, #1
	str	r3, [sp, #12]
	ldr	r3, .L72+20
	b	.L66
.L69:
	bl	good_key_beep
	mov	r0, r4
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r2, .L72+24
	ldr	r3, .L72+4
	add	r2, r2, r0, asl #3
	str	r2, [r3, #0]
	mov	r3, #3
	str	r3, [sp, #12]
	ldr	r3, .L72+28
	cmp	r0, #0
	str	r3, [sp, #32]
	mov	r3, #256
	str	r3, [sp, #36]
	movne	r3, r0, asl #1
	movne	r0, r0, asl #4
	rsbne	r0, r3, r0
	addne	r0, r0, #47
	moveq	r0, #47
	str	r0, [sp, #40]
	b	.L68
.L41:
	ldr	r3, .L72+16
	ldrsh	r4, [r3, #0]
	cmp	r4, #0
	beq	.L49
	cmp	r4, #1
	bne	.L64
	b	.L70
.L49:
	ldr	r0, .L72+8
	bl	process_bool
	mov	r0, r4
.L65:
	bl	Redraw_Screen
	b	.L31
.L70:
	bl	good_key_beep
	mov	r1, #0
	mov	r0, r4
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L72+24
	ldr	r5, [r3, r0, asl #3]
	rsbs	r5, r5, #1
	movcc	r5, #0
	str	r5, [r3, r0, asl #3]
	add	r3, r3, r0, asl #3
	ldr	r6, [r3, #4]
	bl	FLOWSENSE_get_controller_index
	mov	r7, r0
	mov	r0, #2
	bl	WEATHER_get_change_bits_ptr
	mov	r1, r6
	mov	r2, r4
	mov	r3, #2
	str	r7, [sp, #0]
	str	r4, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r5
	bl	nm_WEATHER_set_freeze_switch_connected
	mov	r0, r4
	bl	SCROLL_BOX_redraw
	b	.L31
.L39:
	ldr	r3, .L72+16
	ldrsh	r4, [r3, #0]
	cmp	r4, #1
	bne	.L64
	mov	r1, #0
	mov	r0, r4
	bl	GuiLib_ScrollBox_GetActiveLine
	cmp	r0, #0
	streq	r4, [sp, #12]
	ldreq	r3, .L72+32
	movne	r0, r4
	movne	r1, #4
	bne	.L67
	b	.L66
.L38:
	ldr	r3, .L72+16
	ldrsh	r0, [r3, #0]
	cmp	r0, #0
	beq	.L55
	cmp	r0, #1
	bne	.L64
	b	.L71
.L55:
	ldr	r3, .L72+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L64
	str	r3, [sp, #12]
	ldr	r3, .L72+36
.L66:
	str	r3, [sp, #32]
.L68:
	add	r0, sp, #12
	bl	Display_Post_Command
	b	.L31
.L71:
	mov	r1, #0
.L67:
	bl	SCROLL_BOX_up_or_down
	b	.L31
.L64:
	bl	bad_key_beep
	b	.L31
.L37:
	cmp	r4, #67
	bne	.L58
	bl	WEATHER_extract_and_store_freeze_switch_changes_from_GuiVars
	ldr	r3, .L72+40
	mov	r2, #4
	str	r2, [r3, #0]
.L58:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L31:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L73:
	.align	2
.L72:
	.word	GuiLib_CurStructureNdx
	.word	.LANCHOR3
	.word	GuiVar_FreezeSwitchInUse
	.word	FDTO_FREEZE_SWITCH_close_freeze_switch_connected_dropdown
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_FREEZE_SWITCH_show_freeze_switch_in_use_dropdown
	.word	.LANCHOR1
	.word	FDTO_FREEZE_SWITCH_show_switch_connected_dropdown
	.word	FDTO_FREEZE_SWITCH_leave_freeze_switch_scrollbox
	.word	FDTO_FREEZE_SWITCH_enter_freeze_switch_scrollbox
	.word	GuiVar_MenuScreenToShow
.LFE8:
	.size	FREEZE_SWITCH_process_screen, .-FREEZE_SWITCH_process_screen
	.section	.bss.g_FREEZE_SWITCH_count,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_FREEZE_SWITCH_count, %object
	.size	g_FREEZE_SWITCH_count, 4
g_FREEZE_SWITCH_count:
	.space	4
	.section	.bss.g_FREEZE_SWITCH_active_line,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_FREEZE_SWITCH_active_line, %object
	.size	g_FREEZE_SWITCH_active_line, 4
g_FREEZE_SWITCH_active_line:
	.space	4
	.section	.bss.g_FREEZE_SWITCH,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_FREEZE_SWITCH, %object
	.size	g_FREEZE_SWITCH, 96
g_FREEZE_SWITCH:
	.space	96
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_freeze_switch.c\000"
	.section	.bss.g_FREEZE_SWITCH_combo_box_guivar,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_FREEZE_SWITCH_combo_box_guivar, %object
	.size	g_FREEZE_SWITCH_combo_box_guivar, 4
g_FREEZE_SWITCH_combo_box_guivar:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI3-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI4-.LFB6
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI5-.LFB7
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI6-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_freeze_switch.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xbf
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x8b
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x3b
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0x97
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.byte	0xb4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.byte	0xae
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0xbc
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x1
	.byte	0x4b
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xca
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.byte	0xef
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST6
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB7
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB8
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI7
	.4byte	.LFE8
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"FDTO_FREEZE_SWITCH_show_freeze_switch_in_use_dropdo"
	.ascii	"wn\000"
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_freeze_switch.c\000"
.LASF10:
	.ascii	"FDTO_FREEZE_SWITCH_draw_freeze_switch_scrollbox\000"
.LASF3:
	.ascii	"FDTO_FREEZE_SWITCH_show_switch_connected_dropdown\000"
.LASF1:
	.ascii	"FREEZE_SWITCH_populate_freeze_switch_scrollbox\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF7:
	.ascii	"FREEZE_SWITCH_process_screen\000"
.LASF0:
	.ascii	"FDTO_FREEZE_SWITCH_enter_freeze_switch_scrollbox\000"
.LASF2:
	.ascii	"FDTO_FREEZE_SWITCH_leave_freeze_switch_scrollbox\000"
.LASF6:
	.ascii	"FDTO_FREEZE_SWITCH_draw_screen\000"
.LASF5:
	.ascii	"FDTO_FREEZE_SWITCH_close_freeze_switch_connected_dr"
	.ascii	"opdown\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
