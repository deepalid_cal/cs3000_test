	.file	"e_sr_programming.c"
	.text
.Ltext0:
	.section	.text.FDTO_SR_PROGRAMMING_populate_mode_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_SR_PROGRAMMING_populate_mode_dropdown, %function
FDTO_SR_PROGRAMMING_populate_mode_dropdown:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	mov	r0, r0, asl #16
	ldr	r2, [r3, r0, asr #14]
	ldr	r3, .L2+4
	str	r2, [r3, #0]
	bx	lr
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	GuiVar_ComboBoxItemIndex
.LFE12:
	.size	FDTO_SR_PROGRAMMING_populate_mode_dropdown, .-FDTO_SR_PROGRAMMING_populate_mode_dropdown
	.section	.text.display_warning_if_repeater_links_to_itself,"ax",%progbits
	.align	2
	.type	display_warning_if_repeater_links_to_itself, %function
display_warning_if_repeater_links_to_itself:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, .L9
	ldr	r3, .L9+4
	ldr	r1, [r1, #0]
	ldr	r2, [r3, #0]
	cmp	r1, #7
	movne	r1, #0
	bne	.L8
	ldr	r1, .L9+8
	ldr	r0, [r1, #0]
	ldr	r1, .L9+12
	ldr	r0, [r0, #0]
	ldr	r1, [r1, #0]
	ldr	r1, [r1, #0]
	rsb	r0, r1, r0
	rsbs	r1, r0, #0
	adc	r1, r1, r0
.L8:
	str	r1, [r3, #0]
	ldr	r3, .L9+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bxeq	lr
	mov	r0, #0
	b	Redraw_Screen
.L10:
	.align	2
.L9:
	.word	GuiVar_SRMode
	.word	GuiVar_SRRepeaterLinkedToItself
	.word	.LANCHOR1
	.word	.LANCHOR2
.LFE0:
	.size	display_warning_if_repeater_links_to_itself, .-display_warning_if_repeater_links_to_itself
	.section	.text.set_sr_mode,"ax",%progbits
	.align	2
	.type	set_sr_mode, %function
set_sr_mode:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #7
	ldr	r3, .L35
	beq	.L13
	bhi	.L16
	sub	r2, r0, #2
	cmp	r2, #1
	bhi	.L12
	b	.L13
.L16:
	cmp	r0, #1000
	beq	.L14
	ldr	r2, .L35+4
	cmp	r0, r2
	bne	.L12
	b	.L34
.L14:
	ldr	r2, [r3, #0]
	cmp	r2, #2
	beq	.L30
	cmp	r2, #3
	bne	.L23
	b	.L22
.L34:
	ldr	r2, [r3, #0]
	cmp	r2, #2
	beq	.L22
	cmp	r2, #3
	bne	.L30
	b	.L23
.L22:
	mov	r2, #7
	b	.L32
.L23:
	mov	r2, #2
	b	.L32
.L30:
	mov	r2, #3
	b	.L32
.L13:
	str	r0, [r3, #0]
	b	.L20
.L12:
	mov	r2, #99
.L32:
	str	r2, [r3, #0]
.L20:
	ldr	r3, .L35
	ldr	r2, .L35+8
	ldr	r1, [r3, #0]
	ldr	r3, .L35+12
	cmp	r1, #3
	beq	.L25
	cmp	r1, #7
	bne	.L31
	ldr	r1, .L35+16
	str	r1, [r2, #0]
	ldr	r2, .L35+20
	str	r2, [r3, #0]
	ldr	r3, .L35+24
	ldr	r1, [r3, #0]
	cmp	r1, #4
	movhi	r1, #0
	strhi	r1, [r3, #0]
	ldr	r3, [r2, #0]
	cmp	r3, #0
	moveq	r2, #1
	ldreq	r3, .L35+20
	beq	.L33
	b	.L28
.L25:
	ldr	r1, .L35+28
	str	r1, [r2, #0]
	ldr	r2, .L35+32
	b	.L33
.L31:
	ldr	r1, .L35+36
	str	r1, [r2, #0]
	ldr	r2, .L35+40
.L33:
	str	r2, [r3, #0]
.L28:
	b	display_warning_if_repeater_links_to_itself
.L36:
	.align	2
.L35:
	.word	GuiVar_SRMode
	.word	1001
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	GuiVar_SRSubnetRcvRepeater
	.word	GuiVar_SRSubnetXmtRepeater
	.word	GuiVar_SRRepeater
	.word	GuiVar_SRSubnetRcvSlave
	.word	GuiVar_SRSubnetXmtSlave
	.word	GuiVar_SRSubnetRcvMaster
	.word	GuiVar_SRSubnetXmtMaster
.LFE1:
	.size	set_sr_mode, .-set_sr_mode
	.section	.text.get_subnet_string_from_id,"ax",%progbits
	.align	2
	.type	get_subnet_string_from_id, %function
get_subnet_string_from_id:
.LFB5:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #0]
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI0:
	cmp	r3, #9
	mov	r5, r0
	mov	r4, r1
	mov	r0, sp
	mov	r1, #2
	bhi	.L38
	ldr	r2, .L41
	bl	snprintf
	b	.L39
.L38:
	cmp	r3, #10
	bne	.L40
	ldr	r2, .L41+4
	bl	snprintf
	b	.L39
.L40:
	ldr	r2, .L41+8
	bl	snprintf
	ldr	r0, .L41+12
	mov	r1, r5
	bl	Alert_Message_va
.L39:
	mov	r0, r4
	mov	r1, sp
	mov	r2, #2
	bl	memcpy
	ldmfd	sp!, {r3, r4, r5, pc}
.L42:
	.align	2
.L41:
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	.LC3
.LFE5:
	.size	get_subnet_string_from_id, .-get_subnet_string_from_id
	.section	.text.clear_info_text,"ax",%progbits
	.align	2
	.type	clear_info_text, %function
clear_info_text:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L44
	ldr	r1, .L44+4
	mov	r2, #49
	b	strlcpy
.L45:
	.align	2
.L44:
	.word	GuiVar_CommOptionInfoText
	.word	.LC4
.LFE7:
	.size	clear_info_text, .-clear_info_text
	.section	.text.start_sr_device_communication,"ax",%progbits
	.align	2
	.type	start_sr_device_communication, %function
start_sr_device_communication:
.LFB3:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r2, #1
	cmp	r1, #87
	ldr	r3, .L51
	movne	r1, #4352
	moveq	r1, #4608
	cmp	r0, r2
	str	lr, [sp, #-4]!
.LCFI1:
	movne	r0, r2
	sub	sp, sp, #40
.LCFI2:
	moveq	r0, #2
	str	r0, [sp, #32]
	str	r2, [r3, #0]
	mov	r0, sp
	str	r1, [sp, #0]
	bl	COMM_MNGR_post_event_with_details
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L52:
	.align	2
.L51:
	.word	.LANCHOR3
.LFE3:
	.size	start_sr_device_communication, .-start_sr_device_communication
	.section	.text.FDTO_SR_PROGRAMMING_show_receives_from_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_SR_PROGRAMMING_show_receives_from_dropdown, %function
FDTO_SR_PROGRAMMING_show_receives_from_dropdown:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L57
	ldr	r0, .L57+4
	ldr	r3, [r3, #0]
	ldr	r1, .L57+8
	cmp	r3, #7
	ldreq	r3, .L57+12
	ldrne	r3, .L57+16
	mov	r2, #10
	ldr	r3, [r3, #0]
	b	FDTO_COMBOBOX_show
.L58:
	.align	2
.L57:
	.word	GuiVar_SRMode
	.word	745
	.word	FDTO_COMBOBOX_add_items
	.word	GuiVar_SRSubnetRcvRepeater
	.word	GuiVar_SRSubnetRcvSlave
.LFE14:
	.size	FDTO_SR_PROGRAMMING_show_receives_from_dropdown, .-FDTO_SR_PROGRAMMING_show_receives_from_dropdown
	.section	.text.FDTO_SR_PROGRAMMING_show_mode_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_SR_PROGRAMMING_show_mode_dropdown, %function
FDTO_SR_PROGRAMMING_show_mode_dropdown:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L65
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L64
	cmp	r3, #3
	moveq	r3, #1
	beq	.L61
	cmp	r3, #7
	moveq	r3, #2
	beq	.L61
.L64:
	mov	r3, #0
.L61:
	ldr	r1, .L65+4
	mov	r0, #744
	mov	r2, #3
	b	FDTO_COMBOBOX_show
.L66:
	.align	2
.L65:
	.word	GuiVar_SRMode
	.word	FDTO_SR_PROGRAMMING_populate_mode_dropdown
.LFE13:
	.size	FDTO_SR_PROGRAMMING_show_mode_dropdown, .-FDTO_SR_PROGRAMMING_show_mode_dropdown
	.section	.text.get_subnet_id_from_string,"ax",%progbits
	.align	2
	.type	get_subnet_id_from_string, %function
get_subnet_id_from_string:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI3:
	bl	atoi
	cmp	r0, #9
	mov	r1, r0
	bls	.L70
	cmp	r0, #15
	beq	.L71
	ldr	r0, .L72
	bl	Alert_Message_va
.L71:
	mov	r1, #10
.L70:
	mov	r0, r1
	ldr	pc, [sp], #4
.L73:
	.align	2
.L72:
	.word	.LC5
.LFE4:
	.size	get_subnet_id_from_string, .-get_subnet_id_from_string
	.section	.text.FDTO_SR_PROGRAMMING_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_SR_PROGRAMMING_draw_screen
	.type	FDTO_SR_PROGRAMMING_draw_screen, %function
FDTO_SR_PROGRAMMING_draw_screen:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI4:
	mov	r4, r0
	bne	.L75
.LBB19:
	ldr	r3, .L79
	ldr	r6, .L79+4
.LBE19:
	sub	ip, r1, #1
	rsbs	r5, ip, #0
.LBB20:
	ldr	r1, .L79+8
	mov	r2, #9
	ldr	r0, .L79+12
.LBE20:
	adc	r5, r5, ip
.LBB21:
	str	r3, [r6, #0]
	bl	strlcpy
	mov	r0, #2
	bl	set_sr_mode
	ldr	r3, .L79+16
	ldr	r2, .L79+20
	str	r5, [r3, #0]
	ldr	r3, .L79+24
	mov	r1, #10
	str	r4, [r3, #0]
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L79+28
	str	r3, [r2, #0]
	ldr	r2, .L79+32
	str	r3, [r2, #0]
	ldr	r2, .L79+36
	str	r3, [r2, #0]
	ldr	r2, .L79+40
	str	r3, [r2, #0]
	ldr	r2, .L79+44
	str	r1, [r2, #0]
	ldr	r2, .L79+48
	str	r3, [r2, #0]
	ldr	r2, .L79+52
	str	r3, [r2, #0]
	bl	clear_info_text
.LBE21:
	ldr	r0, .L79+56
	bl	e_SHARED_index_keycode_for_gui
	mov	r1, r4
	str	r0, [r6, #0]
	b	.L76
.L75:
	ldr	r3, .L79+60
	ldrsh	r1, [r3, #0]
	cmn	r1, #1
	ldreq	r3, .L79+64
	ldreq	r1, [r3, #0]
.L76:
	mov	r1, r1, asl #16
	mov	r0, #53
	mov	r1, r1, asr #16
	mov	r2, #1
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
	cmp	r4, #1
	ldmnefd	sp!, {r4, r5, r6, pc}
	bl	DEVICE_EXCHANGE_draw_dialog
	mov	r0, r5
	mov	r1, #82
	ldmfd	sp!, {r4, r5, r6, lr}
	b	start_sr_device_communication
.L80:
	.align	2
.L79:
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	.LC6
	.word	GuiVar_SRSerialNumber
	.word	GuiVar_SRPort
	.word	GuiVar_SRRepeater
	.word	GuiVar_SRGroup
	.word	GuiVar_SRSubnetRcvMaster
	.word	GuiVar_SRSubnetRcvSlave
	.word	GuiVar_SRSubnetRcvRepeater
	.word	GuiVar_SRSubnetXmtMaster
	.word	GuiVar_SRSubnetXmtSlave
	.word	GuiVar_SRSubnetXmtRepeater
	.word	GuiVar_SRTransmitPower
	.word	36867
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR4
.LFE16:
	.size	FDTO_SR_PROGRAMMING_draw_screen, .-FDTO_SR_PROGRAMMING_draw_screen
	.section	.text.FDTO_SR_PROGRAMMING_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_SR_PROGRAMMING_process_device_exchange_key, %function
FDTO_SR_PROGRAMMING_process_device_exchange_key:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI5:
.LBB28:
	ldr	r4, .L86
.LBE28:
	mov	r6, r0
.LBB29:
	ldr	r2, [r4, #0]
.LBE29:
	mov	r7, r1
.LBB30:
	ldr	r0, .L86+4
	mov	r1, #49
	add	r2, r2, #14
	bl	snprintf
.LBE30:
	sub	r3, r6, #36864
	cmp	r3, #6
	ldmhifd	sp!, {r4, r5, r6, r7, pc}
	mov	r5, #1
	mov	r3, r5, asl r3
	tst	r3, #109
	bne	.L83
	tst	r3, #18
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
.LBB31:
.LBB32:
	ldr	r1, [r4, #0]
	cmp	r1, #0
	beq	.L85
	add	r1, r1, #157
	mov	r2, #9
	ldr	r0, .L86+8
	bl	strlcpy
	ldr	r1, [r4, #0]
	mov	r2, #49
	add	r1, r1, #144
	ldr	r0, .L86+12
	bl	strlcpy
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #132]
	add	r0, r0, #4
	bl	atoi
	bl	set_sr_mode
	ldr	r0, [r4, #0]
	add	r0, r0, #176
	bl	atoi
	ldr	r3, .L86+16
	ldr	r2, .L86+20
	str	r0, [r3, #0]
	ldr	r3, [r4, #0]
	ldrb	r1, [r3, #181]	@ zero_extendqisi2
	ldr	r0, [r3, #132]
	sub	r1, r1, #65
	str	r1, [r2, #0]
	add	r0, r0, #24
	bl	atoi
	ldr	r3, .L86+24
	str	r0, [r3, #0]
	ldr	r3, .L86+28
	ldr	r6, [r3, #0]
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #132]
	add	r0, r0, #65
	bl	get_subnet_id_from_string
	mov	r2, r5
	mov	r1, r0
	mov	r0, r6
	bl	memset
	ldr	r3, .L86+32
	ldr	r6, [r3, #0]
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #132]
	add	r0, r0, #68
	bl	get_subnet_id_from_string
	mov	r2, r5
	mov	r1, r0
	mov	r0, r6
	bl	memset
.L85:
.LBE32:
	ldr	r0, .L86+36
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L86+40
	str	r0, [r3, #0]
	bl	DIALOG_close_ok_dialog
	mov	r0, #0
	mov	r1, r7
.LBE31:
	ldmfd	sp!, {r4, r5, r6, r7, lr}
.LBB33:
	b	FDTO_SR_PROGRAMMING_draw_screen
.L83:
.LBE33:
	mov	r0, r6
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L86+40
	str	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	DEVICE_EXCHANGE_draw_dialog
.L87:
	.align	2
.L86:
	.word	sr_state
	.word	GuiVar_CommOptionInfoText
	.word	GuiVar_SRSerialNumber
	.word	GuiVar_SRFirmwareVer
	.word	GuiVar_SRGroup
	.word	GuiVar_SRRepeater
	.word	GuiVar_SRTransmitPower
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
.LFE15:
	.size	FDTO_SR_PROGRAMMING_process_device_exchange_key, .-FDTO_SR_PROGRAMMING_process_device_exchange_key
	.section	.text.SR_PROGRAMMING_process_screen,"ax",%progbits
	.align	2
	.global	SR_PROGRAMMING_process_screen
	.type	SR_PROGRAMMING_process_screen, %function
SR_PROGRAMMING_process_screen:
.LFB17:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L182
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI6:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #48
.LCFI7:
	cmp	r3, #744
	mov	r4, r0
	mov	r2, r1
	beq	.L90
	ldr	ip, .L182+4
	cmp	r3, ip
	bne	.L156
	b	.L177
.L90:
	add	r1, sp, #44
	bl	COMBO_BOX_key_press
	cmp	r4, #2
	cmpne	r4, #67
	bne	.L88
	ldr	r2, [sp, #44]
	ldr	r3, .L182+8
	ldr	r0, [r3, r2, asl #2]
	bl	set_sr_mode
	mov	r0, #0
	bl	Redraw_Screen
	b	.L88
.L177:
	add	r1, sp, #44
	bl	COMBO_BOX_key_press
	cmp	r4, #2
	cmpne	r4, #67
	bne	.L88
	ldr	r3, .L182+12
	ldr	r3, [r3, #0]
	cmp	r3, #7
	ldreq	r2, .L182+16
	ldr	r3, [sp, #44]
	ldrne	r2, .L182+20
	str	r3, [r2, #0]
	bl	display_warning_if_repeater_links_to_itself
	b	.L165
.L156:
	cmp	r0, #16
	beq	.L101
	bhi	.L105
	cmp	r0, #2
	beq	.L98
	bhi	.L106
	cmp	r0, #0
	beq	.L96
	cmp	r0, #1
	bne	.L95
	b	.L178
.L106:
	cmp	r0, #3
	beq	.L99
	cmp	r0, #4
	bne	.L95
	b	.L179
.L105:
	cmp	r0, #80
	beq	.L103
	bhi	.L107
	cmp	r0, #20
	beq	.L101
	cmp	r0, #67
	bne	.L95
	b	.L180
.L107:
	cmp	r0, #84
	beq	.L103
	bcc	.L95
	sub	r3, r0, #36864
	cmp	r3, #6
	bhi	.L95
	ldr	r3, .L182+24
	mov	r2, #0
	str	r2, [r3, #0]
	mov	r3, #3
	str	r3, [sp, #8]
	ldr	r3, .L182+28
	str	r0, [sp, #32]
	str	r3, [sp, #28]
	ldr	r3, .L182+32
	ldr	r3, [r3, #0]
	sub	lr, r3, #1
	rsbs	r3, lr, #0
	adc	r3, r3, lr
	str	r3, [sp, #36]
	b	.L166
.L98:
	ldr	r4, .L182+36
	ldr	r0, .L182+40
	ldr	r5, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r5, r0
	beq	.L167
	ldr	r0, .L182+44
	ldr	r4, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r4, r0
	beq	.L167
	ldr	r4, .L182+48
	bl	clear_info_text
	ldrsh	r3, [r4, #0]
	sub	r3, r3, #1
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L167
.L114:
	.word	.L110
	.word	.L167
	.word	.L167
	.word	.L111
	.word	.L167
	.word	.L167
	.word	.L112
	.word	.L113
.L113:
.LBB42:
	ldr	r3, .L182+12
	ldr	r3, [r3, #0]
	cmp	r3, #7
	movhi	r0, #1
	movhi	r1, r0
	bhi	.L159
	ldr	r2, .L182+52
	ldr	r2, [r2, #0]
	cmp	r2, #10
	movhi	r0, #2
	bhi	.L173
	cmp	r3, #7
	bne	.L118
	ldr	r3, .L182+56
	ldr	r3, [r3, #0]
	cmp	r3, #4
	movhi	r0, #3
	bls	.L181
	b	.L173
.L118:
	cmp	r3, #3
	bne	.L120
	ldr	r3, .L182+20
	ldr	r3, [r3, #0]
	cmp	r3, #9
	bls	.L120
	b	.L176
.L181:
	ldr	r3, .L182+16
	ldr	r3, [r3, #0]
	cmp	r3, #9
	bls	.L121
.L176:
	mov	r0, #4
	b	.L173
.L121:
	ldr	r3, .L182+60
	ldr	r3, [r3, #0]
	cmp	r3, #9
	movhi	r0, #5
	bhi	.L173
.L120:
	ldr	r5, .L182+64
	ldr	r3, [r5, #0]
	cmp	r3, #10
	bls	.L122
	mov	r0, #6
.L173:
	mov	r1, #1
.L159:
	bl	CURSOR_Select
.LBE42:
	ldr	r0, .L182+68
	bl	DIALOG_draw_ok_dialog
	b	.L167
.L155:
.LBB43:
	ldr	r0, [r3, #132]
	ldr	r6, .L182+12
	mov	r1, #3
	ldr	r2, .L182+72
	ldr	r3, [r6, #0]
	add	r0, r0, #4
	bl	snprintf
	ldr	r0, [r4, #0]
	ldr	r3, .L182+52
	mov	r1, #5
	ldr	r2, .L182+76
	ldr	r3, [r3, #0]
	add	r0, r0, #176
	bl	snprintf
	ldr	r3, .L182+56
	ldr	r0, [r4, #0]
	ldr	r3, [r3, #0]
	mov	r1, #3
	add	r3, r3, #65
	ldr	r2, .L182+80
	and	r3, r3, #255
	add	r0, r0, #181
	bl	snprintf
	ldr	r3, [r4, #0]
	mov	r1, #5
	ldr	r0, [r3, #132]
	ldr	r2, .L182+76
	ldr	r3, [r5, #0]
	add	r0, r0, #24
	bl	snprintf
	ldr	r5, [r6, #0]
	ldr	r3, [r4, #0]
	cmp	r5, #3
	beq	.L124
	cmp	r5, #7
	bne	.L157
	ldr	r1, [r3, #132]
	ldr	r3, .L182+84
	add	r1, r1, #65
	ldr	r0, [r3, #0]
	bl	get_subnet_string_from_id
	ldr	r3, [r4, #0]
	ldr	r1, [r3, #132]
	ldr	r3, .L182+88
	add	r1, r1, #68
	ldr	r0, [r3, #0]
	bl	get_subnet_string_from_id
	b	.L126
.L124:
	ldr	r1, [r3, #132]
	ldr	r3, .L182+84
	add	r1, r1, #65
	ldr	r0, [r3, #0]
	bl	get_subnet_string_from_id
	ldr	r3, [r4, #0]
	mov	r1, r5
	ldr	r0, [r3, #132]
	ldr	r2, .L182+92
	add	r0, r0, #68
	b	.L160
.L157:
	ldr	r0, [r3, #132]
	mov	r1, #3
	ldr	r2, .L182+96
	add	r0, r0, #65
	bl	snprintf
	ldr	r3, [r4, #0]
	ldr	r2, .L182+96
	ldr	r0, [r3, #132]
	mov	r1, #3
	add	r0, r0, #68
.L160:
	bl	snprintf
.L126:
.LBE43:
	ldr	r3, .L182+32
	mov	r1, #87
	ldr	r0, [r3, #0]
	b	.L168
.L112:
	bl	good_key_beep
	ldr	r3, .L182+100
	ldrsh	r2, [r4, #0]
	mov	r1, #82
	str	r2, [r3, #0]
	ldr	r3, .L182+32
	ldr	r0, [r3, #0]
.L168:
	bl	start_sr_device_communication
	b	.L88
.L110:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L182+104
.L174:
	str	r3, [sp, #28]
.L166:
	add	r0, sp, #8
	bl	Display_Post_Command
	b	.L88
.L111:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L182+108
	b	.L174
.L179:
	bl	clear_info_text
	ldr	r3, .L182+48
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #3
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L127
.L133:
	.word	.L128
	.word	.L129
	.word	.L130
	.word	.L131
	.word	.L127
	.word	.L138
.L128:
	mov	r0, #1
	mov	r1, r0
	b	.L172
.L130:
	mov	r0, #3
	b	.L170
.L131:
.LBB44:
	ldr	r3, .L182+12
	ldr	r0, [r3, #0]
	cmp	r0, #2
	bne	.L175
	b	.L170
.L129:
.LBE44:
	mov	r0, #2
	b	.L170
.L127:
	mov	r0, #1
	b	.L171
.L178:
	bl	clear_info_text
	mov	r0, r4
.L171:
	bl	CURSOR_Up
	b	.L88
.L96:
	bl	clear_info_text
	ldr	r3, .L182+48
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #2
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L169
.L140:
	.word	.L136
	.word	.L137
	.word	.L138
	.word	.L169
	.word	.L169
	.word	.L167
.L136:
.LBB45:
	ldr	r3, .L182+12
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L138
.L175:
.LBB46:
	mov	r0, #4
	b	.L170
.L138:
.LBE46:
.LBE45:
	mov	r0, #6
	b	.L170
.L137:
	mov	r0, #5
.L170:
	mov	r1, #1
.L172:
	bl	CURSOR_Select
	b	.L88
.L99:
	bl	clear_info_text
.L169:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L88
.L101:
	bl	clear_info_text
.L167:
	bl	bad_key_beep
	b	.L88
.L103:
	bl	clear_info_text
	ldr	r3, .L182+48
	ldrsh	r3, [r3, #0]
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L142
.L150:
	.word	.L143
	.word	.L144
	.word	.L145
	.word	.L146
	.word	.L147
	.word	.L148
	.word	.L149
.L143:
	ldr	r3, .L182+112
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bne	.L142
	ldr	r3, .L182+116
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bne	.L142
	bl	good_key_beep
	ldr	r3, .L182+32
	mov	r1, #82
	ldr	r0, [r3, #0]
	rsbs	r0, r0, #1
	movcc	r0, #0
	str	r0, [r3, #0]
	bl	start_sr_device_communication
	b	.L152
.L144:
	bl	good_key_beep
	cmp	r4, #84
	moveq	r0, #1000
	ldrne	r0, .L182+120
	bl	set_sr_mode
	mov	r0, #0
	bl	Redraw_Screen
	b	.L152
.L145:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L182+52
	mov	r0, r4
	b	.L162
.L146:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L182+56
	mov	r3, #4
	b	.L163
.L147:
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L182+84
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, [r3, #0]
	mov	r3, #9
	b	.L164
.L148:
	mov	r3, #0
	mov	r2, #1
	stmia	sp, {r2, r3}
	ldr	r3, .L182+88
	mov	r0, r4
	ldr	r1, [r3, #0]
	mov	r3, #10
.L164:
	bl	process_uns32
	bl	display_warning_if_repeater_links_to_itself
	b	.L152
.L149:
	ldr	r1, .L182+64
	mov	r3, #1
	mov	r2, #0
	mov	r0, r4
	str	r3, [sp, #0]
	str	r2, [sp, #4]
.L162:
	mov	r3, #10
.L163:
	bl	process_uns32
	b	.L152
.L142:
	bl	bad_key_beep
.L152:
	ldr	r3, .L182+48
	ldrh	r3, [r3, #0]
	cmp	r3, #1
	bls	.L88
.L165:
	bl	Refresh_Screen
	b	.L88
.L180:
	ldr	r3, .L182+124
	mov	r2, #11
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	b	.L88
.L95:
	mov	r0, r4
	mov	r1, r2
	bl	KEY_process_global_keys
	b	.L88
.L122:
	bl	good_key_beep
	ldr	r3, .L182+48
.LBB47:
	ldr	r4, .L182+128
.LBE47:
	ldrsh	r2, [r3, #0]
	ldr	r3, .L182+100
	str	r2, [r3, #0]
.LBB48:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L155
	b	.L126
.L88:
.LBE48:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, pc}
.L183:
	.align	2
.L182:
	.word	GuiLib_CurStructureNdx
	.word	745
	.word	.LANCHOR0
	.word	GuiVar_SRMode
	.word	GuiVar_SRSubnetRcvRepeater
	.word	GuiVar_SRSubnetRcvSlave
	.word	.LANCHOR3
	.word	FDTO_SR_PROGRAMMING_process_device_exchange_key
	.word	GuiVar_SRPort
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36867
	.word	36870
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_SRGroup
	.word	GuiVar_SRRepeater
	.word	GuiVar_SRSubnetXmtRepeater
	.word	GuiVar_SRTransmitPower
	.word	589
	.word	.LC0
	.word	.LC7
	.word	.LC8
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LC1
	.word	.LC9
	.word	.LANCHOR4
	.word	FDTO_SR_PROGRAMMING_show_mode_dropdown
	.word	FDTO_SR_PROGRAMMING_show_receives_from_dropdown
	.word	GuiVar_CommOptionPortAIndex
	.word	GuiVar_CommOptionPortBIndex
	.word	1001
	.word	GuiVar_MenuScreenToShow
	.word	sr_state
.LFE17:
	.size	SR_PROGRAMMING_process_screen, .-SR_PROGRAMMING_process_screen
	.global	CurrentSubnetIDXmtPtr
	.global	CurrentSubnetIDRcvPtr
	.section	.bss.g_SR_PROGRAMMING_previous_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_SR_PROGRAMMING_previous_cursor_pos, %object
	.size	g_SR_PROGRAMMING_previous_cursor_pos, 4
g_SR_PROGRAMMING_previous_cursor_pos:
	.space	4
	.section	.bss.SR_PROGRAMMING_querying_device,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	SR_PROGRAMMING_querying_device, %object
	.size	SR_PROGRAMMING_querying_device, 4
SR_PROGRAMMING_querying_device:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%d\000"
.LC1:
	.ascii	"F\000"
.LC2:
	.ascii	"-\000"
.LC3:
	.ascii	"ERROR: Invalid SubnetID(2): %d\000"
.LC4:
	.ascii	"\000"
.LC5:
	.ascii	"ERROR: Invalid SubnetID(1): %d\000"
.LC6:
	.ascii	"--------\000"
.LC7:
	.ascii	"%2d\000"
.LC8:
	.ascii	"%c\000"
.LC9:
	.ascii	"0\000"
	.section	.bss.CurrentSubnetIDRcvPtr,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	CurrentSubnetIDRcvPtr, %object
	.size	CurrentSubnetIDRcvPtr, 4
CurrentSubnetIDRcvPtr:
	.space	4
	.section	.bss.CurrentSubnetIDXmtPtr,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	CurrentSubnetIDXmtPtr, %object
	.size	CurrentSubnetIDXmtPtr, 4
CurrentSubnetIDXmtPtr:
	.space	4
	.section	.rodata.sr_mode_dropdown_lookup,"a",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	sr_mode_dropdown_lookup, %object
	.size	sr_mode_dropdown_lookup, 12
sr_mode_dropdown_lookup:
	.word	2
	.word	3
	.word	7
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI0-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI4-.LFB16
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI5-.LFB15
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI6-.LFB17
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE22:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_sr_programming.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x144
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF18
	.byte	0x1
	.4byte	.LASF19
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x12a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x241
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x19b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x292
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x262
	.4byte	.LFB12
	.4byte	.LFE12
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF5
	.byte	0x1
	.byte	0xad
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x1
	.byte	0xca
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x17f
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x1a4
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x13b
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x27f
	.4byte	.LFB14
	.4byte	.LFE14
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x268
	.4byte	.LFB13
	.4byte	.LFE13
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x158
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x2b9
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST3
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x1df
	.byte	0x1
	.uleb128 0x7
	.4byte	0x3c
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST4
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x206
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x1ad
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x2ec
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB5
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB16
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB15
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB17
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI7
	.4byte	.LFE17
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x74
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF9:
	.ascii	"start_sr_device_communication\000"
.LASF17:
	.ascii	"SR_PROGRAMMING_process_screen\000"
.LASF6:
	.ascii	"set_sr_mode\000"
.LASF19:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_sr_programming.c\000"
.LASF13:
	.ascii	"get_guivars_from_sr_programming_struct\000"
.LASF16:
	.ascii	"FDTO_SR_PROGRAMMING_draw_screen\000"
.LASF5:
	.ascii	"display_warning_if_repeater_links_to_itself\000"
.LASF18:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF11:
	.ascii	"FDTO_SR_PROGRAMMING_show_mode_dropdown\000"
.LASF10:
	.ascii	"FDTO_SR_PROGRAMMING_show_receives_from_dropdown\000"
.LASF8:
	.ascii	"clear_info_text\000"
.LASF15:
	.ascii	"set_sr_programming_struct_from_guivars\000"
.LASF4:
	.ascii	"FDTO_SR_PROGRAMMING_populate_mode_dropdown\000"
.LASF2:
	.ascii	"get_info_text_from_sr_programming_struct\000"
.LASF1:
	.ascii	"SR_PROGRAMMING_initialize_guivars\000"
.LASF7:
	.ascii	"get_subnet_string_from_id\000"
.LASF14:
	.ascii	"sr_values_in_range\000"
.LASF12:
	.ascii	"get_subnet_id_from_string\000"
.LASF3:
	.ascii	"FDTO_SR_PROGRAMMING_process_device_exchange_key\000"
.LASF0:
	.ascii	"navigate_around_subnet_rcv_field\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
