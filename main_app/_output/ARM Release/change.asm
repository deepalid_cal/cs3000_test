	.file	"change.c"
	.text
.Ltext0:
	.section	.text.Alert_ChangeLine,"ax",%progbits
	.align	2
	.type	Alert_ChangeLine, %function
Alert_ChangeLine:
.LFB0:
	@ args = 20, pretend = 0, frame = 116
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	mov	r4, r0
	sub	sp, sp, #116
.LCFI1:
	mov	r5, r1
	str	r2, [sp, #4]
	mov	r1, r4
	add	r2, sp, #104
	add	r0, sp, #8
	str	r3, [sp, #0]
	bl	ALERTS_load_common
	add	r1, sp, #140
	mov	r2, #4
	add	r3, sp, #104
	bl	ALERTS_load_object
	add	r1, sp, #144
	mov	r2, #4
	add	r3, sp, #104
	bl	ALERTS_load_object
	ldr	r3, .L9
	sub	r2, r4, #49152
	cmp	r2, r3
	movls	r1, r5
	movls	r2, #48
	bls	.L8
	sub	r2, r4, #53248
	cmp	r2, r3
	add	r1, sp, #4
	mov	r2, #4
	add	r3, sp, #104
	bhi	.L4
	bl	ALERTS_load_object
	mov	r1, sp
	mov	r2, #4
.L8:
	add	r3, sp, #104
.L4:
	bl	ALERTS_load_object
	ldr	r3, [sp, #136]
	cmp	r3, #8
	bhi	.L5
	add	r1, sp, #136
	mov	r2, #4
	add	r3, sp, #104
	bl	ALERTS_load_object
	ldr	r1, [sp, #128]
	ldr	r2, [sp, #136]
	add	r3, sp, #104
	bl	ALERTS_load_object
	ldr	r1, [sp, #132]
	ldr	r2, [sp, #136]
	b	.L7
.L5:
	add	r1, sp, #116
	mov	r3, #0
	mov	r2, #4
	str	r3, [r1, #-4]!
.L7:
	add	r3, sp, #104
	bl	ALERTS_load_object
	mov	r0, r4
	add	r2, sp, #104
	ldmia	r2, {r1-r2}
	bl	ALERTS_add_alert
	add	sp, sp, #116
	ldmfd	sp!, {r4, r5, pc}
.L10:
	.align	2
.L9:
	.word	4095
.LFE0:
	.size	Alert_ChangeLine, .-Alert_ChangeLine
	.section	.text.Alert_ChangeLine_Controller,"ax",%progbits
	.align	2
	.global	Alert_ChangeLine_Controller
	.type	Alert_ChangeLine_Controller, %function
Alert_ChangeLine_Controller:
.LFB1:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI2:
	stmia	sp, {r2, r3}
	ldr	r3, [sp, #24]
	mov	ip, r1
	str	r3, [sp, #8]
	ldr	r3, [sp, #28]
	mov	r1, #0
	str	r3, [sp, #12]
	ldr	r3, [sp, #32]
	mov	r2, ip
	str	r3, [sp, #16]
	mov	r3, r1
	bl	Alert_ChangeLine
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.LFE1:
	.size	Alert_ChangeLine_Controller, .-Alert_ChangeLine_Controller
	.section	.text.Alert_ChangeLine_Group,"ax",%progbits
	.align	2
	.global	Alert_ChangeLine_Group
	.type	Alert_ChangeLine_Group, %function
Alert_ChangeLine_Group:
.LFB2:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI3:
	stmia	sp, {r2, r3}
	ldr	r3, [sp, #24]
	mov	r2, #0
	str	r3, [sp, #8]
	ldr	r3, [sp, #28]
	str	r3, [sp, #12]
	ldr	r3, [sp, #32]
	str	r3, [sp, #16]
	mov	r3, r2
	bl	Alert_ChangeLine
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.LFE2:
	.size	Alert_ChangeLine_Group, .-Alert_ChangeLine_Group
	.section	.text.Alert_ChangeLine_Station,"ax",%progbits
	.align	2
	.global	Alert_ChangeLine_Station
	.type	Alert_ChangeLine_Station, %function
Alert_ChangeLine_Station:
.LFB3:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI4:
	str	r3, [sp, #0]
	ldr	r3, [sp, #24]
	mov	lr, r1
	str	r3, [sp, #4]
	ldr	r3, [sp, #28]
	mov	ip, r2
	str	r3, [sp, #8]
	ldr	r3, [sp, #32]
	mov	r1, #0
	str	r3, [sp, #12]
	ldr	r3, [sp, #36]
	mov	r2, lr
	str	r3, [sp, #16]
	mov	r3, ip
	bl	Alert_ChangeLine
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.LFE3:
	.size	Alert_ChangeLine_Station, .-Alert_ChangeLine_Station
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x80
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/alerts/change.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x3c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x99
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xb7
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xd8
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 128
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/aler"
	.ascii	"ts/change.c\000"
.LASF1:
	.ascii	"Alert_ChangeLine_Group\000"
.LASF2:
	.ascii	"Alert_ChangeLine_Station\000"
.LASF0:
	.ascii	"Alert_ChangeLine_Controller\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"Alert_ChangeLine\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
