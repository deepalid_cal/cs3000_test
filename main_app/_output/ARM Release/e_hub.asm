	.file	"e_hub.c"
	.text
.Ltext0:
	.section	.text.FDTO_HUB_show_hub_list,"ax",%progbits
	.align	2
	.type	FDTO_HUB_show_hub_list, %function
FDTO_HUB_show_hub_list:
.LFB1:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, lr}
.LCFI0:
	ldr	r2, .L12
	mov	r1, #0
	ldr	r0, .L12+4
	bl	memset
	ldr	r3, .L12+8
	ldr	r6, .L12+12
	mov	r1, #0
	ldr	r0, [r3, #0]
	mvn	r2, #0
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, [r6, #0]
	bl	uxQueueMessagesWaiting
	subs	r5, r0, #0
	beq	.L2
	bl	good_key_beep
	mov	r4, #0
	mov	r7, r4
.L7:
	mov	r2, #0
	ldr	r0, [r6, #0]
	add	r1, sp, #8
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L3
	ldr	r3, [sp, #8]
	cmn	r3, #1
	moveq	r7, #1
	beq	.L4
	cmp	r7, #0
	beq	.L5
	ldrb	r2, [sp, #9]	@ zero_extendqisi2
	ldrb	r3, [sp, #8]	@ zero_extendqisi2
	str	r2, [sp, #0]
	ldrb	r2, [sp, #10]	@ zero_extendqisi2
	ldr	r1, .L12
	str	r2, [sp, #4]
	ldr	r0, .L12+4
	ldr	r2, .L12+16
	bl	sp_strlcat
	b	.L4
.L5:
	ldr	r0, .L12+4
	ldr	r1, .L12
	ldr	r2, .L12+20
	bl	sp_strlcat
.L4:
	mov	r2, #0
	ldr	r0, [r6, #0]
	add	r1, sp, #8
	mov	r3, r2
	bl	xQueueGenericSend
	b	.L6
.L3:
	ldr	r0, .L12+24
	bl	Alert_Message
.L6:
	add	r4, r4, #1
	cmp	r4, r5
	bne	.L7
	b	.L8
.L2:
	bl	bad_key_beep
.L8:
	ldr	r3, .L12+8
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
	cmp	r5, #0
	beq	.L1
	ldr	r3, .L12+28
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L12+32
	ldr	r0, .L12+36
	str	r2, [r3, #0]
	mvn	r1, #0
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
.L1:
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, pc}
.L13:
	.align	2
.L12:
	.word	513
	.word	GuiVar_HubList
	.word	router_hub_list_MUTEX
	.word	router_hub_list_queue
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	607
.LFE1:
	.size	FDTO_HUB_show_hub_list, .-FDTO_HUB_show_hub_list
	.section	.text.FDTO_HUB_show_is_a_hub_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_HUB_show_is_a_hub_dropdown, %function
FDTO_HUB_show_is_a_hub_dropdown:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L15
	mov	r0, #163
	ldr	r2, [r3, #0]
	mov	r1, #72
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L16:
	.align	2
.L15:
	.word	GuiVar_IsAHub
.LFE0:
	.size	FDTO_HUB_show_is_a_hub_dropdown, .-FDTO_HUB_show_is_a_hub_dropdown
	.section	.text.FDTO_HUB_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_HUB_draw_screen
	.type	FDTO_HUB_draw_screen, %function
FDTO_HUB_draw_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldreq	r3, .L20
	ldrne	r3, .L20+4
	ldreq	r2, [r3, #144]
	ldreq	r3, .L20+8
	str	lr, [sp, #-4]!
.LCFI1:
	ldrnesh	r1, [r3, #0]
	streq	r2, [r3, #0]
	moveq	r1, #0
	mov	r0, #28
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L21:
	.align	2
.L20:
	.word	config_c
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_IsAHub
.LFE3:
	.size	FDTO_HUB_draw_screen, .-FDTO_HUB_draw_screen
	.section	.text.HUB_process_screen,"ax",%progbits
	.align	2
	.global	HUB_process_screen
	.type	HUB_process_screen, %function
HUB_process_screen:
.LFB4:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L52
	ldr	r2, .L52+4
	ldrsh	r3, [r3, #0]
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI2:
	cmp	r3, r2
	sub	sp, sp, #68
.LCFI3:
	mov	r4, r0
	mov	r6, r1
	beq	.L24
	cmp	r3, #740
	bne	.L44
	ldr	r1, .L52+8
	bl	COMBO_BOX_key_press
	b	.L22
.L24:
	cmp	r0, #4
	beq	.L28
	cmp	r0, #67
	beq	.L29
	cmp	r0, #0
	beq	.L28
	b	.L49
.L29:
	bl	good_key_beep
	bl	DIALOG_close_all_dialogs
	b	.L22
.L28:
	mov	r0, #0
	mov	r1, r4
	bl	TEXT_BOX_up_or_down
	b	.L22
.L44:
	cmp	r0, #3
	beq	.L31
	bhi	.L35
	cmp	r0, #1
	beq	.L32
	bhi	.L33
	b	.L31
.L35:
	cmp	r0, #80
	beq	.L34
	cmp	r0, #84
	beq	.L34
	cmp	r0, #4
	bne	.L46
	b	.L32
.L33:
	ldr	r3, .L52+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L37
	cmp	r3, #1
	bne	.L47
	b	.L51
.L37:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #32]
	ldr	r3, .L52+16
	b	.L50
.L51:
	str	r3, [sp, #32]
	ldr	r3, .L52+20
.L50:
	add	r0, sp, #32
	str	r3, [sp, #52]
	bl	Display_Post_Command
	b	.L22
.L47:
	bl	bad_key_beep
	b	.L22
.L34:
	ldr	r3, .L52+12
	ldrsh	r4, [r3, #0]
	cmp	r4, #0
	bne	.L48
	ldr	r0, .L52+8
	bl	process_bool
	mov	r0, r4
	bl	Redraw_Screen
	b	.L41
.L48:
	bl	bad_key_beep
.L41:
	bl	Refresh_Screen
	b	.L22
.L32:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L22
.L31:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L22
.L46:
	cmp	r0, #67
	bne	.L42
.LBB4:
	ldr	r3, .L52+8
	ldr	r8, .L52+24
	ldr	r7, [r3, #0]
	ldr	r3, [r8, #144]
	cmp	r7, r3
	beq	.L43
	bl	FLOWSENSE_get_controller_index
	ldr	r2, .L52+28
	mov	r3, #2
	stmia	sp, {r2, r3}
	ldr	r3, .L52+32
	mov	r5, #0
	mov	r2, #1
	str	r3, [sp, #28]
	mov	r1, r7
	mov	r3, r5
	str	r5, [sp, #12]
	str	r5, [sp, #16]
	str	r5, [sp, #20]
	str	r5, [sp, #24]
	str	r0, [sp, #8]
	add	r0, r8, #144
	bl	SHARED_set_bool_controller
	mov	r0, r5
	mov	r1, r5
	ldr	r5, .L52+36
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	mov	r1, #400
	ldr	r2, .L52+40
	mov	r3, #180
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L52+44
	mov	r2, #1
	str	r2, [r3, #204]
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	bl	CONTROLLER_INITIATED_update_comm_server_registration_info
.L43:
.LBE4:
	ldr	r3, .L52+48
	mov	r2, #11
	str	r2, [r3, #0]
.L42:
	mov	r0, r4
	mov	r1, r6
.L49:
	bl	KEY_process_global_keys
.L22:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L53:
	.align	2
.L52:
	.word	GuiLib_CurStructureNdx
	.word	607
	.word	GuiVar_IsAHub
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_HUB_show_is_a_hub_dropdown
	.word	FDTO_HUB_show_hub_list
	.word	config_c
	.word	57374
	.word	.LC3
	.word	irri_comm_recursive_MUTEX
	.word	.LC4
	.word	irri_comm
	.word	GuiVar_MenuScreenToShow
.LFE4:
	.size	HUB_process_screen, .-HUB_process_screen
	.global	g_HUB_dialog_visible
	.global	g_HUB_cursor_position_when_dialog_displayed
	.section	.bss.g_HUB_dialog_visible,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_HUB_dialog_visible, %object
	.size	g_HUB_dialog_visible, 4
g_HUB_dialog_visible:
	.space	4
	.section	.bss.g_HUB_cursor_position_when_dialog_displayed,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_HUB_cursor_position_when_dialog_displayed, %object
	.size	g_HUB_cursor_position_when_dialog_displayed, 4
g_HUB_cursor_position_when_dialog_displayed:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%c%c%c\012\000"
.LC1:
	.ascii	"%d\012\000"
.LC2:
	.ascii	"HUB: unexp hub list queue ERROR\000"
.LC3:
	.ascii	"Acts as a Hub\000"
.LC4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_hub.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_hub.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x73
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x44
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x3e
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xbe
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x1
	.byte	0x9c
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xd2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"FDTO_HUB_show_is_a_hub_dropdown\000"
.LASF3:
	.ascii	"HUB_process_screen\000"
.LASF0:
	.ascii	"FDTO_HUB_show_hub_list\000"
.LASF6:
	.ascii	"HUB_extract_and_store_changes_from_GuiVars\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"FDTO_HUB_draw_screen\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_hub.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
