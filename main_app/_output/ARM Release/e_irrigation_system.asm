	.file	"e_irrigation_system.c"
	.text
.Ltext0:
	.section	.text.SYSTEM_add_new_group,"ax",%progbits
	.align	2
	.type	SYSTEM_add_new_group, %function
SYSTEM_add_new_group:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r4, .L2
	ldr	r2, .L2+4
	mov	r3, #196
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L2+8
	ldr	r1, .L2+12
	bl	nm_GROUP_create_new_group_from_UI
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L3:
	.align	2
.L2:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	nm_SYSTEM_create_new_group
	.word	SYSTEM_copy_group_into_guivars
.LFE4:
	.size	SYSTEM_add_new_group, .-SYSTEM_add_new_group
	.section	.text.SYSTEM_process_group,"ax",%progbits
	.align	2
	.type	SYSTEM_process_group, %function
SYSTEM_process_group:
.LFB5:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L72
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	ldrsh	r2, [r3, #0]
	ldr	r3, .L72+4
	sub	sp, sp, #48
.LCFI2:
	cmp	r2, r3
	mov	r5, r0
	mov	r4, r1
	bne	.L50
	cmp	r0, #67
	beq	.L7
	cmp	r0, #2
	bne	.L8
	ldr	r3, .L72+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L8
.L7:
	bl	SYSTEM_extract_and_store_group_name_from_GuiVars
.L8:
	mov	r0, r5
	mov	r1, r4
	ldr	r2, .L72+12
	bl	KEYBOARD_process_key
	b	.L4
.L50:
	cmp	r0, #4
	beq	.L15
	bhi	.L19
	cmp	r0, #1
	beq	.L12
	bcc	.L11
	cmp	r0, #2
	beq	.L13
	cmp	r0, #3
	bne	.L10
	b	.L69
.L19:
	cmp	r0, #67
	beq	.L17
	bhi	.L20
	cmp	r0, #16
	beq	.L16
	cmp	r0, #20
	bne	.L10
	b	.L16
.L20:
	cmp	r0, #80
	beq	.L18
	cmp	r0, #84
	bne	.L10
	b	.L18
.L13:
	ldr	r3, .L72+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L22
	cmp	r3, #3
	bne	.L56
	b	.L70
.L22:
	mov	r0, #164
	mov	r1, #27
	bl	GROUP_process_show_keyboard
	b	.L4
.L70:
	mov	r1, #0
	mov	r0, #1
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L72+16
	ldr	r4, .L72+20
	ldr	r7, [r4, #0]
	ldr	r6, [r3, r0, asl #2]
	mov	r0, r6
	bl	POC_get_GID_irrigation_system
	cmp	r7, r0
	bne	.L24
	bl	bad_key_beep
	b	.L57
.L24:
	bl	good_key_beep
	ldr	r7, [r4, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r1, r5
	mov	r4, r0
	mov	r0, r6
	bl	POC_get_change_bits_ptr
	str	r4, [sp, #0]
	mov	r4, #1
	str	r4, [sp, #4]
	mov	r1, r7
	mov	r2, r4
	mov	r3, r5
	str	r0, [sp, #8]
	mov	r0, r6
	b	.L58
.L18:
	ldr	r3, .L72+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #3
	bne	.L56
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L72+16
	cmp	r5, #84
	ldr	r4, .L72+20
	ldr	r6, [r3, r0, asl #2]
	bne	.L27
	mov	r0, r6
	ldr	r5, [r4, #0]
	bl	POC_get_GID_irrigation_system
	cmp	r5, r0
	beq	.L56
	bl	good_key_beep
	ldr	r5, [r4, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r1, #2
	mov	r4, r0
	mov	r0, r6
	bl	POC_get_change_bits_ptr
	str	r4, [sp, #0]
	mov	r4, #1
	mov	r1, r5
	mov	r2, r4
	mov	r3, #2
	str	r4, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r6
.L58:
	bl	nm_POC_set_GID_irrigation_system
	mov	r0, r4
	bl	SCROLL_BOX_redraw
	b	.L4
.L27:
	bl	bad_key_beep
	mov	r0, r6
	ldr	r4, [r4, #0]
	bl	POC_get_GID_irrigation_system
	cmp	r4, r0
	bne	.L4
.L57:
	mov	r1, #0
	mov	r0, #1
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L72+24
	str	r0, [r3, #0]
	ldr	r0, .L72+28
	bl	DIALOG_draw_ok_dialog
	b	.L4
.L16:
	mov	r1, #0
	mov	r0, #1
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L72+24
	str	r0, [r3, #0]
	bl	SYSTEM_num_systems_in_use
	ldr	r3, .L72+32
	ldr	r2, .L72+36
	str	r3, [sp, #0]
	ldr	r3, .L72+40
	mov	r1, r0
	mov	r0, r5
	bl	GROUP_process_NEXT_and_PREV
	mov	r3, #1
	str	r3, [sp, #12]
	ldr	r3, .L72+44
	b	.L62
.L15:
	ldr	r3, .L72+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #3
	bne	.L56
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	subs	r4, r0, #0
	blt	.L56
	mov	r0, #1
	bl	GuiLib_ScrollBox_GetTopLine
	cmp	r4, r0
	moveq	r3, #1
	streq	r3, [sp, #12]
	movne	r0, #1
	movne	r1, r5
	bne	.L59
	b	.L63
.L11:
	ldr	r3, .L72+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L45
	cmp	r3, #3
	mov	r0, #1
	beq	.L68
.L54:
	bl	CURSOR_Down
	b	.L4
.L12:
	ldr	r3, .L72+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L17
	cmp	r3, #3
	bne	.L56
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	subs	r4, r0, #0
	blt	.L56
	mov	r0, r5
	bl	GuiLib_ScrollBox_GetTopLine
	cmp	r4, r0
	movne	r0, r5
	movne	r1, #4
	bne	.L59
	str	r5, [sp, #12]
.L63:
	ldr	r3, .L72+48
.L62:
	str	r3, [sp, #32]
	b	.L60
.L69:
	ldr	r3, .L72+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L45
	cmp	r3, #3
	bne	.L56
	b	.L71
.L45:
	ldr	r3, .L72+52
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L56
	bl	good_key_beep
	mov	r3, #2
	str	r3, [sp, #12]
	ldr	r3, .L72+56
	str	r3, [sp, #32]
	mov	r3, #1
	str	r3, [sp, #36]
.L60:
	add	r0, sp, #12
	bl	Display_Post_Command
	b	.L4
.L71:
	mov	r0, #1
.L68:
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	cmp	r0, #0
	blt	.L56
	ldr	r3, .L72+52
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r0, r3
	movne	r0, #1
	movne	r1, #0
	beq	.L56
.L59:
	bl	SCROLL_BOX_up_or_down
	b	.L4
.L56:
	bl	bad_key_beep
	b	.L4
.L17:
	ldr	r0, .L72+60
	bl	KEY_process_BACK_from_editing_screen
	b	.L4
.L10:
	mov	r0, r5
	mov	r1, r4
	bl	KEY_process_global_keys
.L4:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L73:
	.align	2
.L72:
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_SYSTEM_draw_menu
	.word	POC_MENU_items
	.word	g_GROUP_ID
	.word	.LANCHOR0
	.word	590
	.word	SYSTEM_copy_group_into_guivars
	.word	SYSTEM_extract_and_store_changes_from_GuiVars
	.word	SYSTEM_get_group_at_this_index
	.word	FDTO_SYSTEM_draw_poc_scrollbox
	.word	FDTO_SYSTEM_leave_poc_scrollbox
	.word	.LANCHOR1
	.word	FDTO_SYSTEM_enter_poc_scrollbox
	.word	FDTO_SYSTEM_return_to_menu
.LFE5:
	.size	SYSTEM_process_group, .-SYSTEM_process_group
	.section	.text.FDTO_SYSTEM_enter_poc_scrollbox,"ax",%progbits
	.align	2
	.type	FDTO_SYSTEM_enter_poc_scrollbox, %function
FDTO_SYSTEM_enter_poc_scrollbox:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI3:
	mov	r4, r0
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	ldr	r3, .L77
	ldr	r2, .L77+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	mov	r3, #130
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	bl	POC_populate_pointers_of_POCs_for_display
	ldr	r3, .L77+8
	cmp	r4, #1
	mov	r2, r0, asl #16
	str	r0, [r3, #0]
	mov	r2, r2, asr #16
	bne	.L75
	mov	r0, r4
	ldr	r1, .L77+12
	mov	r3, #0
	bl	GuiLib_ScrollBox_Init
	b	.L76
.L75:
	mov	r0, #1
	ldr	r1, .L77+12
	mov	r3, r2
	str	r2, [sp, #0]
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L76:
	ldr	r3, .L77
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #3
	bl	GuiLib_Cursor_Select
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	GuiLib_Refresh
.L78:
	.align	2
.L77:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR1
	.word	SYSTEM_populate_poc_scrollbox
.LFE2:
	.size	FDTO_SYSTEM_enter_poc_scrollbox, .-FDTO_SYSTEM_enter_poc_scrollbox
	.section	.text.FDTO_SYSTEM_draw_poc_scrollbox,"ax",%progbits
	.align	2
	.type	FDTO_SYSTEM_draw_poc_scrollbox, %function
FDTO_SYSTEM_draw_poc_scrollbox:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI4:
	ldr	r4, .L80
	mov	r1, #400
	ldr	r2, .L80+4
	mov	r3, #113
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	bl	POC_populate_pointers_of_POCs_for_display
	ldr	r5, .L80+8
	str	r0, [r5, #0]
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	ldr	r3, .L80+12
	ldr	r1, .L80+16
	ldrsh	r2, [r5, #0]
	ldrsh	r3, [r3, #0]
	mov	r0, #1
	bl	GuiLib_ScrollBox_Init
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L81:
	.align	2
.L80:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR1
	.word	.LANCHOR0
	.word	SYSTEM_populate_poc_scrollbox
.LFE1:
	.size	FDTO_SYSTEM_draw_poc_scrollbox, .-FDTO_SYSTEM_draw_poc_scrollbox
	.section	.text.FDTO_SYSTEM_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_SYSTEM_return_to_menu, %function
FDTO_SYSTEM_return_to_menu:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI5:
	ldr	r0, .L83
	ldr	r1, .L83+4
	bl	FDTO_GROUP_return_to_menu
	ldr	r3, .L83+8
	mvn	r2, #0
	str	r2, [r3, #0]
	ldr	lr, [sp], #4
	b	FDTO_SYSTEM_draw_poc_scrollbox
.L84:
	.align	2
.L83:
	.word	.LANCHOR2
	.word	SYSTEM_extract_and_store_changes_from_GuiVars
	.word	.LANCHOR0
.LFE6:
	.size	FDTO_SYSTEM_return_to_menu, .-FDTO_SYSTEM_return_to_menu
	.section	.text.SYSTEM_populate_poc_scrollbox,"ax",%progbits
	.align	2
	.type	SYSTEM_populate_poc_scrollbox, %function
SYSTEM_populate_poc_scrollbox:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L88
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, lr}
.LCFI6:
	ldr	r2, .L88+4
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #84
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L88+8
	ldr	r4, [r3, r4, asl #2]
	cmp	r4, #0
	beq	.L86
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #24
	mov	r1, r0
	ldr	r0, .L88+12
	bl	strlcpy
	ldr	r3, .L88+16
	mov	r0, r4
	ldr	r5, [r3, #0]
	bl	POC_get_GID_irrigation_system
	rsb	r3, r0, r5
	rsbs	r0, r3, #0
	adc	r0, r0, r3
	ldr	r3, .L88+20
	str	r0, [r3, #0]
	b	.L87
.L86:
	ldr	r0, .L88+4
	mov	r1, #104
	bl	Alert_group_not_found_with_filename
.L87:
	ldr	r3, .L88
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L89:
	.align	2
.L88:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	POC_MENU_items
	.word	GuiVar_itmPOC
	.word	g_GROUP_ID
	.word	GuiVar_SystemPOCSelected
.LFE0:
	.size	SYSTEM_populate_poc_scrollbox, .-SYSTEM_populate_poc_scrollbox
	.section	.text.FDTO_SYSTEM_leave_poc_scrollbox,"ax",%progbits
	.align	2
	.type	FDTO_SYSTEM_leave_poc_scrollbox, %function
FDTO_SYSTEM_leave_poc_scrollbox:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI7:
	ldr	r4, .L91
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	mov	r1, #400
	ldr	r2, .L91+4
	mov	r3, #154
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	bl	POC_populate_pointers_of_POCs_for_display
	ldr	r3, .L91+8
	ldr	r1, .L91+12
	mov	r2, r0, asl #16
	str	r0, [r3, #0]
	mov	r2, r2, asr #16
	mvn	r3, #0
	mov	r0, #1
	bl	GuiLib_ScrollBox_Init
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #0
	mov	r1, #1
	ldmfd	sp!, {r4, lr}
	b	FDTO_Cursor_Select
.L92:
	.align	2
.L91:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR1
	.word	SYSTEM_populate_poc_scrollbox
.LFE3:
	.size	FDTO_SYSTEM_leave_poc_scrollbox, .-FDTO_SYSTEM_leave_poc_scrollbox
	.section	.text.FDTO_SYSTEM_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_SYSTEM_draw_menu
	.type	FDTO_SYSTEM_draw_menu, %function
FDTO_SYSTEM_draw_menu:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI8:
	ldr	r5, .L95
	mov	r4, r0
	mov	r1, #400
	ldr	r2, .L95+4
	ldr	r3, .L95+8
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	mov	r6, r0
	bl	SYSTEM_num_systems_in_use
	ldr	r3, .L95+12
	mov	r2, r6
	str	r3, [sp, #0]
	ldr	r3, .L95+16
	ldr	r1, .L95+20
	str	r3, [sp, #4]
	mov	r3, #35
	cmp	r0, #3
	movhi	r0, #0
	movls	r0, #1
	str	r0, [sp, #8]
	mov	r0, r4
	bl	FDTO_GROUP_draw_menu
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r4, #0
	ldrne	r3, .L95+24
	mvnne	r2, #0
	strne	r2, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	FDTO_SYSTEM_draw_poc_scrollbox
.L96:
	.align	2
.L95:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	586
	.word	SYSTEM_load_system_name_into_scroll_box_guivar
	.word	SYSTEM_copy_group_into_guivars
	.word	.LANCHOR2
	.word	.LANCHOR0
.LFE7:
	.size	FDTO_SYSTEM_draw_menu, .-FDTO_SYSTEM_draw_menu
	.section	.text.SYSTEM_process_menu,"ax",%progbits
	.align	2
	.global	SYSTEM_process_menu
	.type	SYSTEM_process_menu, %function
SYSTEM_process_menu:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI9:
	ldr	r3, .L99
	mov	r5, r0
	ldr	r2, .L99+4
	mov	r6, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L99+8
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	ldr	r2, .L99+12
	ldr	r4, .L99+16
	str	r2, [sp, #0]
	ldr	r2, .L99+20
	mov	r1, r6
	str	r2, [sp, #4]
	ldr	r2, .L99+24
	str	r2, [sp, #8]
	ldr	r2, .L99+28
	str	r2, [sp, #12]
	mov	r2, r4
	mov	r3, r0
	mov	r0, r5
	bl	GROUP_process_menu
	cmp	r5, #67
	beq	.L98
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r5, r0
	bl	SYSTEM_num_systems_in_use
	cmp	r5, r0
	bcs	.L98
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L98
	mov	r0, #1
	bl	SCROLL_BOX_redraw
.L98:
	ldr	r3, .L99
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L100:
	.align	2
.L99:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	619
	.word	SYSTEM_process_group
	.word	.LANCHOR2
	.word	SYSTEM_add_new_group
	.word	SYSTEM_get_group_at_this_index
	.word	SYSTEM_copy_group_into_guivars
.LFE8:
	.size	SYSTEM_process_menu, .-SYSTEM_process_menu
	.section	.bss.g_SYSTEM_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_SYSTEM_editing_group, %object
	.size	g_SYSTEM_editing_group, 4
g_SYSTEM_editing_group:
	.space	4
	.section	.bss.g_SYSTEM_index_of_poc_when_dialog_displayed,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_SYSTEM_index_of_poc_when_dialog_displayed, %object
	.size	g_SYSTEM_index_of_poc_when_dialog_displayed, 4
g_SYSTEM_index_of_poc_when_dialog_displayed:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_irrigation_system.c\000"
	.section	.bss.g_SYSTEM_num_pocs_physically_available,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_SYSTEM_num_pocs_physically_available, %object
	.size	g_SYSTEM_num_pocs_physically_available, 4
g_SYSTEM_num_pocs_physically_available:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI0-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI1-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI4-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI5-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI6-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI7-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI8-.LFB7
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI9-.LFB8
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_irrigation_system.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xce
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF9
	.byte	0x1
	.4byte	.LASF10
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0xc2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0xd9
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0x7e
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.byte	0x6f
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST3
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x22a
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x50
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST5
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.byte	0x96
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST6
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x248
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x269
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB4
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB5
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI2
	.4byte	.LFE5
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB1
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB0
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB3
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF8:
	.ascii	"SYSTEM_process_menu\000"
.LASF10:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_irrigation_system.c\000"
.LASF0:
	.ascii	"SYSTEM_add_new_group\000"
.LASF5:
	.ascii	"SYSTEM_populate_poc_scrollbox\000"
.LASF4:
	.ascii	"FDTO_SYSTEM_return_to_menu\000"
.LASF9:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF6:
	.ascii	"FDTO_SYSTEM_leave_poc_scrollbox\000"
.LASF1:
	.ascii	"SYSTEM_process_group\000"
.LASF2:
	.ascii	"FDTO_SYSTEM_enter_poc_scrollbox\000"
.LASF7:
	.ascii	"FDTO_SYSTEM_draw_menu\000"
.LASF3:
	.ascii	"FDTO_SYSTEM_draw_poc_scrollbox\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
