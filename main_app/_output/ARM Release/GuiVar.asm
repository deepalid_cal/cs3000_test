	.file	"GuiVar.c"
	.text
.Ltext0:
	.global	GuiVar_WindGageResumeSpeed
	.global	GuiVar_WindGagePauseSpeed
	.global	GuiVar_WindGageInUse
	.global	GuiVar_WindGageInstalledAt
	.global	GuiVar_WENWPAUsername
	.global	GuiVar_WENWPAPEAPOption
	.global	GuiVar_WENWPAPassword
	.global	GuiVar_WENWPAKeyType
	.global	GuiVar_WENWPAIEEE8021X
	.global	GuiVar_WENWPAEncryptionWEP
	.global	GuiVar_WENWPAEncryptionTKIP
	.global	GuiVar_WENWPAEncryptionCCMP
	.global	GuiVar_WENWPAEncryption
	.global	GuiVar_WENWPAEAPTTLSOption
	.global	GuiVar_WENWPAEAPTLSValidateCert
	.global	GuiVar_WENWPAEAPTLSCredentials
	.global	GuiVar_WENWPAAuthentication
	.global	GuiVar_WENWPA2Encryption
	.global	GuiVar_WENWEPTXKey
	.global	GuiVar_WENWEPShowTXKeyIndex
	.global	GuiVar_WENWEPKeyType
	.global	GuiVar_WENWEPKeySizeChanged
	.global	GuiVar_WENWEPKeySize
	.global	GuiVar_WENWEPAuthentication
	.global	GuiVar_WENStatus
	.global	GuiVar_WENSSID
	.global	GuiVar_WENSecurity
	.global	GuiVar_WENRSSI
	.global	GuiVar_WENRadioType
	.global	GuiVar_WENRadioMode
	.global	GuiVar_WENPassphrase
	.global	GuiVar_WENMACAddress
	.global	GuiVar_WENKey
	.global	GuiVar_WENChangePassword
	.global	GuiVar_WENChangePassphrase
	.global	GuiVar_WENChangeKey
	.global	GuiVar_WENChangeCredentials
	.global	GuiVar_WalkThruStationDesc
	.global	GuiVar_WalkThruStation_Str
	.global	GuiVar_WalkThruShowStartInSeconds
	.global	GuiVar_WalkThruRunTime
	.global	GuiVar_WalkThruOrder
	.global	GuiVar_WalkThruDelay
	.global	GuiVar_WalkThruCountingDown
	.global	GuiVar_WalkThruControllerName
	.global	GuiVar_WalkThruBoxIndex
	.global	GuiVar_VolumeSliderPos
	.global	GuiVar_Volume
	.global	GuiVar_UnusedGroupsRemovedGreaterThan1
	.global	GuiVar_UnusedGroupsRemoved
	.global	GuiVar_TwoWireStatsWDTs
	.global	GuiVar_TwoWireStatsTxAcksSent
	.global	GuiVar_TwoWireStatsTempMax
	.global	GuiVar_TwoWireStatsTempCur
	.global	GuiVar_TwoWireStatsS2UCos
	.global	GuiVar_TwoWireStatsS1UCos
	.global	GuiVar_TwoWireStatsRxStatsReqMsgs
	.global	GuiVar_TwoWireStatsRxStatReqMsgs
	.global	GuiVar_TwoWireStatsRxSolCurMeasReqMsgs
	.global	GuiVar_TwoWireStatsRxSolCtrlMsgs
	.global	GuiVar_TwoWireStatsRxPutParamsMsgs
	.global	GuiVar_TwoWireStatsRxMsgs
	.global	GuiVar_TwoWireStatsRxLongMsgs
	.global	GuiVar_TwoWireStatsRxIDReqMsgs
	.global	GuiVar_TwoWireStatsRxGetParamsMsgs
	.global	GuiVar_TwoWireStatsRxEnqMsgs
	.global	GuiVar_TwoWireStatsRxDiscRstMsgs
	.global	GuiVar_TwoWireStatsRxDiscConfMsgs
	.global	GuiVar_TwoWireStatsRxDecRstMsgs
	.global	GuiVar_TwoWireStatsRxDataLpbkMsgs
	.global	GuiVar_TwoWireStatsRxCRCErrs
	.global	GuiVar_TwoWireStatsPORs
	.global	GuiVar_TwoWireStatsEEPCRCS2Params
	.global	GuiVar_TwoWireStatsEEPCRCS1Params
	.global	GuiVar_TwoWireStatsEEPCRCErrStats
	.global	GuiVar_TwoWireStatsEEPCRCComParams
	.global	GuiVar_TwoWireStatsBODs
	.global	GuiVar_TwoWireStaStrB
	.global	GuiVar_TwoWireStaStrA
	.global	GuiVar_TwoWireStaDecoderIndex
	.global	GuiVar_TwoWireStaBIsSet
	.global	GuiVar_TwoWireStaB
	.global	GuiVar_TwoWireStaAIsSet
	.global	GuiVar_TwoWireStaA
	.global	GuiVar_TwoWirePOCUsedForBypass
	.global	GuiVar_TwoWirePOCInUse
	.global	GuiVar_TwoWirePOCDecoderIndex
	.global	GuiVar_TwoWireOutputOnB
	.global	GuiVar_TwoWireOutputOnA
	.global	GuiVar_TwoWireOrphanedStationsExist
	.global	GuiVar_TwoWireOrphanedPOCsExist
	.global	GuiVar_TwoWireNumDiscoveredStaDecoders
	.global	GuiVar_TwoWireNumDiscoveredPOCDecoders
	.global	GuiVar_TwoWireNumDiscoveredMoisDecoders
	.global	GuiVar_TwoWireDiscoveryState
	.global	GuiVar_TwoWireDiscoveryProgressBar
	.global	GuiVar_TwoWireDiscoveredStaDecoders
	.global	GuiVar_TwoWireDiscoveredPOCDecoders
	.global	GuiVar_TwoWireDiscoveredMoisDecoders
	.global	GuiVar_TwoWireDescB
	.global	GuiVar_TwoWireDescA
	.global	GuiVar_TwoWireDecoderType
	.global	GuiVar_TwoWireDecoderSNToAssign
	.global	GuiVar_TwoWireDecoderSN
	.global	GuiVar_TwoWireDecoderFWVersion
	.global	GuiVar_TurnOffControllerIsOff
	.global	GuiVar_TestStationTimeRemaining
	.global	GuiVar_TestStationTimeDisplay
	.global	GuiVar_TestStationStatus
	.global	GuiVar_TestStationOnOff
	.global	GuiVar_TestStationAutoOnTime
	.global	GuiVar_TestStationAutoOffTime
	.global	GuiVar_TestStationAutoMode
	.global	GuiVar_TestRunTime
	.global	GuiVar_TaskList
	.global	GuiVar_SystemPreservesStabilityNext
	.global	GuiVar_SystemPreservesStabilityMostRecent5Sec
	.global	GuiVar_SystemPreserves5SecNext
	.global	GuiVar_SystemPreserves5SecMostRecent
	.global	GuiVar_SystemPOCSelected
	.global	GuiVar_SystemName
	.global	GuiVar_SystemInUse
	.global	GuiVar_SystemFlowCheckingTolerancePlus4
	.global	GuiVar_SystemFlowCheckingTolerancePlus3
	.global	GuiVar_SystemFlowCheckingTolerancePlus2
	.global	GuiVar_SystemFlowCheckingTolerancePlus1
	.global	GuiVar_SystemFlowCheckingToleranceMinus4
	.global	GuiVar_SystemFlowCheckingToleranceMinus3
	.global	GuiVar_SystemFlowCheckingToleranceMinus2
	.global	GuiVar_SystemFlowCheckingToleranceMinus1
	.global	GuiVar_SystemFlowCheckingRange3
	.global	GuiVar_SystemFlowCheckingRange2
	.global	GuiVar_SystemFlowCheckingRange1
	.global	GuiVar_SystemFlowCheckingInUse
	.global	GuiVar_StopKeyReasonInList
	.global	GuiVar_StopKeyPending
	.global	GuiVar_StatusWindPaused
	.global	GuiVar_StatusWindGageReading
	.global	GuiVar_StatusWindGageControllerName
	.global	GuiVar_StatusWindGageConnected
	.global	GuiVar_StatusTypeOfSchedule
	.global	GuiVar_StatusSystemFlowValvesOn
	.global	GuiVar_StatusSystemFlowSystem
	.global	GuiVar_StatusSystemFlowStatus
	.global	GuiVar_StatusSystemFlowShowMLBDetected
	.global	GuiVar_StatusSystemFlowMLBMeasured
	.global	GuiVar_StatusSystemFlowMLBAllowed
	.global	GuiVar_StatusSystemFlowExpected
	.global	GuiVar_StatusSystemFlowActual
	.global	GuiVar_StatusShowSystemFlow
	.global	GuiVar_StatusShowLiveScreens
	.global	GuiVar_StatusRainSwitchState
	.global	GuiVar_StatusRainSwitchConnected
	.global	GuiVar_StatusRainBucketReading
	.global	GuiVar_StatusRainBucketControllerName
	.global	GuiVar_StatusRainBucketConnected
	.global	GuiVar_StatusOverviewStationStr
	.global	GuiVar_StatusNOWDaysInEffect
	.global	GuiVar_StatusNextScheduledIrriType
	.global	GuiVar_StatusNextScheduledIrriGID
	.global	GuiVar_StatusNextScheduledIrriDate
	.global	GuiVar_StatusMVORInEffect
	.global	GuiVar_StatusMLBInEffect
	.global	GuiVar_StatusIrrigationActivityState
	.global	GuiVar_StatusFuseBlown
	.global	GuiVar_StatusFreezeSwitchState
	.global	GuiVar_StatusFreezeSwitchConnected
	.global	GuiVar_StatusFlowErrors
	.global	GuiVar_StatusETGageReading
	.global	GuiVar_StatusETGageControllerName
	.global	GuiVar_StatusETGageConnected
	.global	GuiVar_StatusElectricalErrors
	.global	GuiVar_StatusCycleLeft
	.global	GuiVar_StatusCurrentDraw
	.global	GuiVar_StatusChainDown
	.global	GuiVar_StationSelectionGridStationCount
	.global	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
	.global	GuiVar_StationSelectionGridScreenTitle
	.global	GuiVar_StationSelectionGridMarkerSize
	.global	GuiVar_StationSelectionGridMarkerPos
	.global	GuiVar_StationSelectionGridGroupName
	.global	GuiVar_StationSelectionGridControllerName
	.global	GuiVar_StationInfoTotalMinutes
	.global	GuiVar_StationInfoStationGroup
	.global	GuiVar_StationInfoSquareFootage
	.global	GuiVar_StationInfoSoakInTime
	.global	GuiVar_StationInfoShowPercentOfET
	.global	GuiVar_StationInfoShowPercentAdjust
	.global	GuiVar_StationInfoShowEstMin
	.global	GuiVar_StationInfoShowCopyStation
	.global	GuiVar_StationInfoNumber_str
	.global	GuiVar_StationInfoNumber
	.global	GuiVar_StationInfoNoWaterDays
	.global	GuiVar_StationInfoMoistureBalancePercent
	.global	GuiVar_StationInfoMoistureBalance
	.global	GuiVar_StationInfoIStatus
	.global	GuiVar_StationInfoGroupHasStartTime
	.global	GuiVar_StationInfoFlowStatus
	.global	GuiVar_StationInfoExpectedFlow
	.global	GuiVar_StationInfoETFactor
	.global	GuiVar_StationInfoEstMin
	.global	GuiVar_StationInfoDU
	.global	GuiVar_StationInfoCycleTooShort
	.global	GuiVar_StationInfoCycleTime
	.global	GuiVar_StationInfoControllerName
	.global	GuiVar_StationInfoBoxIndex
	.global	GuiVar_StationGroupUsableRain
	.global	GuiVar_StationGroupSystemGID
	.global	GuiVar_StationGroupSpeciesFactor
	.global	GuiVar_StationGroupSoilType
	.global	GuiVar_StationGroupSoilStorageCapacity
	.global	GuiVar_StationGroupSoilIntakeRate
	.global	GuiVar_StationGroupSlopePercentage
	.global	GuiVar_StationGroupRootZoneDepth
	.global	GuiVar_StationGroupPrecipRate
	.global	GuiVar_StationGroupPlantType
	.global	GuiVar_StationGroupMoistureSensorDecoderSN
	.global	GuiVar_StationGroupMicroclimateFactor
	.global	GuiVar_StationGroupKcsAreCustom
	.global	GuiVar_StationGroupKc9
	.global	GuiVar_StationGroupKc8
	.global	GuiVar_StationGroupKc7
	.global	GuiVar_StationGroupKc6
	.global	GuiVar_StationGroupKc5
	.global	GuiVar_StationGroupKc4
	.global	GuiVar_StationGroupKc3
	.global	GuiVar_StationGroupKc2
	.global	GuiVar_StationGroupKc12
	.global	GuiVar_StationGroupKc11
	.global	GuiVar_StationGroupKc10
	.global	GuiVar_StationGroupKc1
	.global	GuiVar_StationGroupInUse
	.global	GuiVar_StationGroupHeadType
	.global	GuiVar_StationGroupExposure
	.global	GuiVar_StationGroupDensityFactor
	.global	GuiVar_StationGroupAvailableWater
	.global	GuiVar_StationGroupAllowableSurfaceAccum
	.global	GuiVar_StationGroupAllowableDepletion
	.global	GuiVar_StationDescription
	.global	GuiVar_StationDecoderSerialNumber
	.global	GuiVar_StationDecoderOutput
	.global	GuiVar_StationCopyTextOffset_Right
	.global	GuiVar_StationCopyTextOffset_Left
	.global	GuiVar_StationCopyGroupName
	.global	GuiVar_SRTransmitPower
	.global	GuiVar_SRSubnetXmtSlave
	.global	GuiVar_SRSubnetXmtRepeater
	.global	GuiVar_SRSubnetXmtMaster
	.global	GuiVar_SRSubnetRcvSlave
	.global	GuiVar_SRSubnetRcvRepeater
	.global	GuiVar_SRSubnetRcvMaster
	.global	GuiVar_SRSerialNumber
	.global	GuiVar_SRRepeaterLinkedToItself
	.global	GuiVar_SRRepeater
	.global	GuiVar_SRPort
	.global	GuiVar_SRMode
	.global	GuiVar_SRGroup
	.global	GuiVar_SRFirmwareVer
	.global	GuiVar_SpinnerPos
	.global	GuiVar_ShowDivider
	.global	GuiVar_SerialPortXmitTP
	.global	GuiVar_SerialPortXmittingB
	.global	GuiVar_SerialPortXmittingA
	.global	GuiVar_SerialPortXmitLengthB
	.global	GuiVar_SerialPortXmitLengthA
	.global	GuiVar_SerialPortXmitB
	.global	GuiVar_SerialPortXmitA
	.global	GuiVar_SerialPortStateTP
	.global	GuiVar_SerialPortStateB
	.global	GuiVar_SerialPortStateA
	.global	GuiVar_SerialPortRcvdTP
	.global	GuiVar_SerialPortRcvdB
	.global	GuiVar_SerialPortRcvdA
	.global	GuiVar_SerialPortOptionB
	.global	GuiVar_SerialPortOptionA
	.global	GuiVar_SerialPortCTSB
	.global	GuiVar_SerialPortCTSA
	.global	GuiVar_SerialPortCDB
	.global	GuiVar_SerialPortCDA
	.global	GuiVar_ScrollBoxHorizScrollPos
	.global	GuiVar_ScrollBoxHorizScrollMarker
	.global	GuiVar_ScheduleWaterDay_Wed
	.global	GuiVar_ScheduleWaterDay_Tue
	.global	GuiVar_ScheduleWaterDay_Thu
	.global	GuiVar_ScheduleWaterDay_Sun
	.global	GuiVar_ScheduleWaterDay_Sat
	.global	GuiVar_ScheduleWaterDay_Mon
	.global	GuiVar_ScheduleWaterDay_Fri
	.global	GuiVar_ScheduleUsesET
	.global	GuiVar_ScheduleUseETAveraging
	.global	GuiVar_ScheduleTypeScrollItem
	.global	GuiVar_ScheduleType
	.global	GuiVar_ScheduleStopTimeEnabled
	.global	GuiVar_ScheduleStopTime
	.global	GuiVar_ScheduleStartTimeEqualsStopTime
	.global	GuiVar_ScheduleStartTimeEnabled
	.global	GuiVar_ScheduleStartTime
	.global	GuiVar_ScheduleNoStationsInGroup
	.global	GuiVar_ScheduleMowDay
	.global	GuiVar_ScheduleIrrigateOn29thOr31st
	.global	GuiVar_ScheduleBeginDateStr
	.global	GuiVar_ScheduleBeginDate
	.global	GuiVar_RptWalkThruMin
	.global	GuiVar_RptWalkThruGal
	.global	GuiVar_RptTestMin
	.global	GuiVar_RptTestGal
	.global	GuiVar_RptStation
	.global	GuiVar_RptStartTime
	.global	GuiVar_RptScheduledMin
	.global	GuiVar_RptRReMin
	.global	GuiVar_RptRReGal
	.global	GuiVar_RptRainMin
	.global	GuiVar_RptNonCMin
	.global	GuiVar_RptNonCGal
	.global	GuiVar_RptManualPMin
	.global	GuiVar_RptManualPGal
	.global	GuiVar_RptManualMin
	.global	GuiVar_RptManualGal
	.global	GuiVar_RptLastMeasuredCurrent
	.global	GuiVar_RptIrrigMin
	.global	GuiVar_RptIrrigGal
	.global	GuiVar_RptFlow
	.global	GuiVar_RptFlags
	.global	GuiVar_RptDecoderSN
	.global	GuiVar_RptDecoderOutput
	.global	GuiVar_RptDate
	.global	GuiVar_RptCycles
	.global	GuiVar_RptController
	.global	GuiVar_RainSwitchSelected
	.global	GuiVar_RainSwitchInUse
	.global	GuiVar_RainSwitchControllerName
	.global	GuiVar_RainBucketMinimum
	.global	GuiVar_RainBucketMaximumHourly
	.global	GuiVar_RainBucketMaximum24Hour
	.global	GuiVar_RainBucketInUse
	.global	GuiVar_RainBucketInstalledAt
	.global	GuiVar_RadioTestSuccessfulPercent
	.global	GuiVar_RadioTestStartTime
	.global	GuiVar_RadioTestRoundTripMinMS
	.global	GuiVar_RadioTestRoundTripMaxMS
	.global	GuiVar_RadioTestRoundTripAvgMS
	.global	GuiVar_RadioTestPort
	.global	GuiVar_RadioTestPacketsSent
	.global	GuiVar_RadioTestNoResp
	.global	GuiVar_RadioTestInProgress
	.global	GuiVar_RadioTestET2000eAddr2
	.global	GuiVar_RadioTestET2000eAddr1
	.global	GuiVar_RadioTestET2000eAddr0
	.global	GuiVar_RadioTestDeviceIndex
	.global	GuiVar_RadioTestCS3000SN
	.global	GuiVar_RadioTestCommWithET2000e
	.global	GuiVar_RadioTestBadCRC
	.global	GuiVar_POCUsage
	.global	GuiVar_POCType
	.global	GuiVar_POCReedSwitchType3
	.global	GuiVar_POCReedSwitchType2
	.global	GuiVar_POCReedSwitchType1
	.global	GuiVar_POCPreservesIndex
	.global	GuiVar_POCPreservesGroupID
	.global	GuiVar_POCPreservesFileType
	.global	GuiVar_POCPreservesDeliveredPumpCurrent3
	.global	GuiVar_POCPreservesDeliveredPumpCurrent2
	.global	GuiVar_POCPreservesDeliveredPumpCurrent1
	.global	GuiVar_POCPreservesDeliveredMVCurrent3
	.global	GuiVar_POCPreservesDeliveredMVCurrent2
	.global	GuiVar_POCPreservesDeliveredMVCurrent1
	.global	GuiVar_POCPreservesDelivered5SecAvg3
	.global	GuiVar_POCPreservesDelivered5SecAvg2
	.global	GuiVar_POCPreservesDelivered5SecAvg1
	.global	GuiVar_POCPreservesDecoderSN3
	.global	GuiVar_POCPreservesDecoderSN2
	.global	GuiVar_POCPreservesDecoderSN1
	.global	GuiVar_POCPreservesBoxIndex
	.global	GuiVar_POCPreservesAccumPulses3
	.global	GuiVar_POCPreservesAccumPulses2
	.global	GuiVar_POCPreservesAccumPulses1
	.global	GuiVar_POCPreservesAccumMS3
	.global	GuiVar_POCPreservesAccumMS2
	.global	GuiVar_POCPreservesAccumMS1
	.global	GuiVar_POCPreservesAccumGal3
	.global	GuiVar_POCPreservesAccumGal2
	.global	GuiVar_POCPreservesAccumGal1
	.global	GuiVar_POCPhysicallyAvailable
	.global	GuiVar_POCMVType1
	.global	GuiVar_POCGroupID
	.global	GuiVar_POCFMType3IsBermad
	.global	GuiVar_POCFMType2IsBermad
	.global	GuiVar_POCFMType1IsBermad
	.global	GuiVar_POCFlowMeterType3
	.global	GuiVar_POCFlowMeterType2
	.global	GuiVar_POCFlowMeterType1
	.global	GuiVar_POCFlowMeterOffset3
	.global	GuiVar_POCFlowMeterOffset2
	.global	GuiVar_POCFlowMeterOffset1
	.global	GuiVar_POCFlowMeterK3
	.global	GuiVar_POCFlowMeterK2
	.global	GuiVar_POCFlowMeterK1
	.global	GuiVar_POCDecoderSN3
	.global	GuiVar_POCDecoderSN2
	.global	GuiVar_POCDecoderSN1
	.global	GuiVar_POCBypassStages
	.global	GuiVar_POCBudgetYearly
	.global	GuiVar_POCBudgetPercentOfET
	.global	GuiVar_POCBudgetOption
	.global	GuiVar_POCBudget9
	.global	GuiVar_POCBudget8
	.global	GuiVar_POCBudget7
	.global	GuiVar_POCBudget6
	.global	GuiVar_POCBudget5
	.global	GuiVar_POCBudget4
	.global	GuiVar_POCBudget3
	.global	GuiVar_POCBudget2
	.global	GuiVar_POCBudget12
	.global	GuiVar_POCBudget11
	.global	GuiVar_POCBudget10
	.global	GuiVar_POCBudget1
	.global	GuiVar_POCBoxIndex
	.global	GuiVar_PercentAdjustPositive
	.global	GuiVar_PercentAdjustPercent_9
	.global	GuiVar_PercentAdjustPercent_8
	.global	GuiVar_PercentAdjustPercent_7
	.global	GuiVar_PercentAdjustPercent_6
	.global	GuiVar_PercentAdjustPercent_5
	.global	GuiVar_PercentAdjustPercent_4
	.global	GuiVar_PercentAdjustPercent_3
	.global	GuiVar_PercentAdjustPercent_2
	.global	GuiVar_PercentAdjustPercent_1
	.global	GuiVar_PercentAdjustPercent_0
	.global	GuiVar_PercentAdjustPercent
	.global	GuiVar_PercentAdjustNumberOfDays
	.global	GuiVar_PercentAdjustEndDate_9
	.global	GuiVar_PercentAdjustEndDate_8
	.global	GuiVar_PercentAdjustEndDate_7
	.global	GuiVar_PercentAdjustEndDate_6
	.global	GuiVar_PercentAdjustEndDate_5
	.global	GuiVar_PercentAdjustEndDate_4
	.global	GuiVar_PercentAdjustEndDate_3
	.global	GuiVar_PercentAdjustEndDate_2
	.global	GuiVar_PercentAdjustEndDate_1
	.global	GuiVar_PercentAdjustEndDate_0
	.global	GuiVar_OnAtATimeInfoDisplay
	.global	GuiVar_NumericKeypadValue
	.global	GuiVar_NumericKeypadShowPlusMinus
	.global	GuiVar_NumericKeypadShowDecimalPoint
	.global	GuiVar_NoWaterDaysProg
	.global	GuiVar_NoWaterDaysDays
	.global	GuiVar_NetworkAvailable
	.global	GuiVar_MVORTimeRemaining
	.global	GuiVar_MVORState
	.global	GuiVar_MVORRunTime
	.global	GuiVar_MVOROpen6Enabled
	.global	GuiVar_MVOROpen6
	.global	GuiVar_MVOROpen5Enabled
	.global	GuiVar_MVOROpen5
	.global	GuiVar_MVOROpen4Enabled
	.global	GuiVar_MVOROpen4
	.global	GuiVar_MVOROpen3Enabled
	.global	GuiVar_MVOROpen3
	.global	GuiVar_MVOROpen2Enabled
	.global	GuiVar_MVOROpen2
	.global	GuiVar_MVOROpen1Enabled
	.global	GuiVar_MVOROpen1
	.global	GuiVar_MVOROpen0Enabled
	.global	GuiVar_MVOROpen0
	.global	GuiVar_MVORInEffect
	.global	GuiVar_MVORClose6Enabled
	.global	GuiVar_MVORClose6
	.global	GuiVar_MVORClose5Enabled
	.global	GuiVar_MVORClose5
	.global	GuiVar_MVORClose4Enabled
	.global	GuiVar_MVORClose4
	.global	GuiVar_MVORClose3Enabled
	.global	GuiVar_MVORClose3
	.global	GuiVar_MVORClose2Enabled
	.global	GuiVar_MVORClose2
	.global	GuiVar_MVORClose1Enabled
	.global	GuiVar_MVORClose1
	.global	GuiVar_MVORClose0Enabled
	.global	GuiVar_MVORClose0
	.global	GuiVar_MoistureTabToDisplay
	.global	GuiVar_MoisTempPoint_Low
	.global	GuiVar_MoisTempPoint_High
	.global	GuiVar_MoisTempAction_Low
	.global	GuiVar_MoisTempAction_High
	.global	GuiVar_MoisTemp_Current
	.global	GuiVar_MoisSetPoint_Low
	.global	GuiVar_MoisSetPoint_High
	.global	GuiVar_MoisSensorName
	.global	GuiVar_MoisSensorIncludesEC
	.global	GuiVar_MoisReading_Current
	.global	GuiVar_MoisPhysicallyAvailable
	.global	GuiVar_MoisInUse
	.global	GuiVar_MoisECPoint_Low
	.global	GuiVar_MoisECPoint_High
	.global	GuiVar_MoisECAction_Low
	.global	GuiVar_MoisECAction_High
	.global	GuiVar_MoisEC_Current
	.global	GuiVar_MoisDecoderSN
	.global	GuiVar_MoisControlMode
	.global	GuiVar_MoisBoxIndex
	.global	GuiVar_MoisAdditionalSoakSeconds
	.global	GuiVar_MenuScreenToShow
	.global	GuiVar_ManualWaterRunTime
	.global	GuiVar_ManualWaterProg
	.global	GuiVar_ManualPWillRun
	.global	GuiVar_ManualPWillNotRunReason
	.global	GuiVar_ManualPWaterDay_Wed
	.global	GuiVar_ManualPWaterDay_Tue
	.global	GuiVar_ManualPWaterDay_Thu
	.global	GuiVar_ManualPWaterDay_Sun
	.global	GuiVar_ManualPWaterDay_Sat
	.global	GuiVar_ManualPWaterDay_Mon
	.global	GuiVar_ManualPWaterDay_Fri
	.global	GuiVar_ManualPStationRunTimeStationNumber
	.global	GuiVar_ManualPStationRunTimeControllerName
	.global	GuiVar_ManualPStationRunTime
	.global	GuiVar_ManualPStartTime6Enabled
	.global	GuiVar_ManualPStartTime6
	.global	GuiVar_ManualPStartTime5Enabled
	.global	GuiVar_ManualPStartTime5
	.global	GuiVar_ManualPStartTime4Enabled
	.global	GuiVar_ManualPStartTime4
	.global	GuiVar_ManualPStartTime3Enabled
	.global	GuiVar_ManualPStartTime3
	.global	GuiVar_ManualPStartTime2Enabled
	.global	GuiVar_ManualPStartTime2
	.global	GuiVar_ManualPStartTime1Enabled
	.global	GuiVar_ManualPStartTime1
	.global	GuiVar_ManualPStartDateStr
	.global	GuiVar_ManualPRunTimeSingle
	.global	GuiVar_ManualPRunTime
	.global	GuiVar_ManualPInUse
	.global	GuiVar_ManualPEndDateStr
	.global	GuiVar_MainMenuWeatherOptionsExist
	.global	GuiVar_MainMenuWeatherDeviceExists
	.global	GuiVar_MainMenuStationsExist
	.global	GuiVar_MainMenuPOCsExist
	.global	GuiVar_MainMenuMoisSensorExists
	.global	GuiVar_MainMenuManualAvailable
	.global	GuiVar_MainMenuMainLinesAvailable
	.global	GuiVar_MainMenuLightsOptionsExist
	.global	GuiVar_MainMenuHubOptionExists
	.global	GuiVar_MainMenuFlowMetersExist
	.global	GuiVar_MainMenuFlowCheckingInUse
	.global	GuiVar_MainMenuFLOptionExists
	.global	GuiVar_MainMenuCommOptionExists
	.global	GuiVar_MainMenuBypassExists
	.global	GuiVar_MainMenuBudgetsInUse
	.global	GuiVar_MainMenuAquaponicsMode
	.global	GuiVar_MainMenu2WireOptionsExist
	.global	GuiVar_LRTransmitPower
	.global	GuiVar_LRTemperature
	.global	GuiVar_LRSlaveLinksToRepeater
	.global	GuiVar_LRSerialNumber
	.global	GuiVar_LRRepeaterInUse
	.global	GuiVar_LRRadioType
	.global	GuiVar_LRPort
	.global	GuiVar_LRMode
	.global	GuiVar_LRHubFeatureNotAvailable
	.global	GuiVar_LRGroup
	.global	GuiVar_LRFrequency_WholeNum
	.global	GuiVar_LRFrequency_Decimal
	.global	GuiVar_LRFirmwareVer
	.global	GuiVar_LRBeaconRate
	.global	GuiVar_LiveScreensSystemMVORExists
	.global	GuiVar_LiveScreensSystemMLBExists
	.global	GuiVar_LiveScreensPOCFlowName
	.global	GuiVar_LiveScreensPOCFlowMVState
	.global	GuiVar_LiveScreensElectricalControllerName
	.global	GuiVar_LiveScreensCommTPMicroConnected
	.global	GuiVar_LiveScreensCommFLInForced
	.global	GuiVar_LiveScreensCommFLEnabled
	.global	GuiVar_LiveScreensCommFLCommMngrMode
	.global	GuiVar_LiveScreensCommCentralEnabled
	.global	GuiVar_LiveScreensCommCentralConnected
	.global	GuiVar_LiveScreensBypassFlowShowMaxFlow
	.global	GuiVar_LiveScreensBypassFlowMVOpen
	.global	GuiVar_LiveScreensBypassFlowMaxFlow
	.global	GuiVar_LiveScreensBypassFlowLevel
	.global	GuiVar_LiveScreensBypassFlowFlow
	.global	GuiVar_LiveScreensBypassFlowAvgFlow
	.global	GuiVar_LightStartReason
	.global	GuiVar_LightsStopTimeHasBeenEdited
	.global	GuiVar_LightsStopTimeEnabled2
	.global	GuiVar_LightsStopTimeEnabled1
	.global	GuiVar_LightsStopTime2InMinutes
	.global	GuiVar_LightsStopTime1InMinutes
	.global	GuiVar_LightsStartTimeEnabled2
	.global	GuiVar_LightsStartTimeEnabled1
	.global	GuiVar_LightsStartTime2InMinutes
	.global	GuiVar_LightsStartTime1InMinutes
	.global	GuiVar_LightsOutputIsDeenergized
	.global	GuiVar_LightsOutputIndex_0
	.global	GuiVar_LightsOutput
	.global	GuiVar_LightsDate
	.global	GuiVar_LightScheduleState
	.global	GuiVar_LightScheduledStop
	.global	GuiVar_LightScheduledStart
	.global	GuiVar_LightsCalendarHighlightYOffset
	.global	GuiVar_LightsCalendarHighlightXOffset
	.global	GuiVar_LightsBoxIndex_0
	.global	GuiVar_LightsAreEnergized
	.global	GuiVar_LightRemainingMinutes
	.global	GuiVar_LightIsPhysicallyAvailable
	.global	GuiVar_LightIsEnergized
	.global	GuiVar_LightIrriListStop
	.global	GuiVar_LightEditableStop
	.global	GuiVar_LightCalendarWeek2Wed
	.global	GuiVar_LightCalendarWeek2Tue
	.global	GuiVar_LightCalendarWeek2Thu
	.global	GuiVar_LightCalendarWeek2Sun
	.global	GuiVar_LightCalendarWeek2Sat
	.global	GuiVar_LightCalendarWeek2Mon
	.global	GuiVar_LightCalendarWeek2Fri
	.global	GuiVar_LightCalendarWeek1Wed
	.global	GuiVar_LightCalendarWeek1Tue
	.global	GuiVar_LightCalendarWeek1Thu
	.global	GuiVar_LightCalendarWeek1Sun
	.global	GuiVar_LightCalendarWeek1Sat
	.global	GuiVar_LightCalendarWeek1Mon
	.global	GuiVar_LightCalendarWeek1Fri
	.global	GuiVar_KeyboardType
	.global	GuiVar_KeyboardStartY
	.global	GuiVar_KeyboardStartX
	.global	GuiVar_KeyboardShiftEnabled
	.global	GuiVar_itmSubNode
	.global	GuiVar_itmStationNumber
	.global	GuiVar_itmStationName
	.global	GuiVar_itmPOC
	.global	GuiVar_itmMenuItemDesc
	.global	GuiVar_itmMenuItem
	.global	GuiVar_itmGroupName
	.global	GuiVar_itmDecoder_Debug
	.global	GuiVar_IsMaster
	.global	GuiVar_IsAHub
	.global	GuiVar_IrriDetailsStatus
	.global	GuiVar_IrriDetailsStation
	.global	GuiVar_IrriDetailsShowFOAL
	.global	GuiVar_IrriDetailsRunSoak
	.global	GuiVar_IrriDetailsRunCycle
	.global	GuiVar_IrriDetailsReason
	.global	GuiVar_IrriDetailsProgLeft
	.global	GuiVar_IrriDetailsProgCycle
	.global	GuiVar_IrriDetailsCurrentDraw
	.global	GuiVar_IrriDetailsController
	.global	GuiVar_IrriCommChainConfigSerialNum
	.global	GuiVar_IrriCommChainConfigNameAbbrv
	.global	GuiVar_IrriCommChainConfigCurrentSendFromTP
	.global	GuiVar_IrriCommChainConfigCurrentSendFromIrri
	.global	GuiVar_IrriCommChainConfigCurrentFromTP
	.global	GuiVar_IrriCommChainConfigCurrentFromIrri
	.global	GuiVar_IrriCommChainConfigCurrentFromFOAL
	.global	GuiVar_IrriCommChainConfigActive
	.global	GuiVar_HubList
	.global	GuiVar_HistoricalETUseYourOwn
	.global	GuiVar_HistoricalETUserState
	.global	GuiVar_HistoricalETUserCounty
	.global	GuiVar_HistoricalETUserCity
	.global	GuiVar_HistoricalETUser9
	.global	GuiVar_HistoricalETUser8
	.global	GuiVar_HistoricalETUser7
	.global	GuiVar_HistoricalETUser6
	.global	GuiVar_HistoricalETUser5
	.global	GuiVar_HistoricalETUser4
	.global	GuiVar_HistoricalETUser3
	.global	GuiVar_HistoricalETUser2
	.global	GuiVar_HistoricalETUser12
	.global	GuiVar_HistoricalETUser11
	.global	GuiVar_HistoricalETUser10
	.global	GuiVar_HistoricalETUser1
	.global	GuiVar_HistoricalETState
	.global	GuiVar_HistoricalETCounty
	.global	GuiVar_HistoricalETCity
	.global	GuiVar_HistoricalET9
	.global	GuiVar_HistoricalET8
	.global	GuiVar_HistoricalET7
	.global	GuiVar_HistoricalET6
	.global	GuiVar_HistoricalET5
	.global	GuiVar_HistoricalET4
	.global	GuiVar_HistoricalET3
	.global	GuiVar_HistoricalET2
	.global	GuiVar_HistoricalET12
	.global	GuiVar_HistoricalET11
	.global	GuiVar_HistoricalET10
	.global	GuiVar_HistoricalET1
	.global	GuiVar_GRSIMState
	.global	GuiVar_GRSIMID
	.global	GuiVar_GRService
	.global	GuiVar_GRRSSI
	.global	GuiVar_GRRadioSerialNum
	.global	GuiVar_GRPhoneNumber
	.global	GuiVar_GroupSettingC_9
	.global	GuiVar_GroupSettingC_8
	.global	GuiVar_GroupSettingC_7
	.global	GuiVar_GroupSettingC_6
	.global	GuiVar_GroupSettingC_5
	.global	GuiVar_GroupSettingC_4
	.global	GuiVar_GroupSettingC_3
	.global	GuiVar_GroupSettingC_2
	.global	GuiVar_GroupSettingC_1
	.global	GuiVar_GroupSettingC_0
	.global	GuiVar_GroupSettingB_9
	.global	GuiVar_GroupSettingB_8
	.global	GuiVar_GroupSettingB_7
	.global	GuiVar_GroupSettingB_6
	.global	GuiVar_GroupSettingB_5
	.global	GuiVar_GroupSettingB_4
	.global	GuiVar_GroupSettingB_3
	.global	GuiVar_GroupSettingB_2
	.global	GuiVar_GroupSettingB_1
	.global	GuiVar_GroupSettingB_0
	.global	GuiVar_GroupSettingA_9
	.global	GuiVar_GroupSettingA_8
	.global	GuiVar_GroupSettingA_7
	.global	GuiVar_GroupSettingA_6
	.global	GuiVar_GroupSettingA_5
	.global	GuiVar_GroupSettingA_4
	.global	GuiVar_GroupSettingA_3
	.global	GuiVar_GroupSettingA_2
	.global	GuiVar_GroupSettingA_1
	.global	GuiVar_GroupSettingA_0
	.global	GuiVar_GroupSetting_Visible_9
	.global	GuiVar_GroupSetting_Visible_8
	.global	GuiVar_GroupSetting_Visible_7
	.global	GuiVar_GroupSetting_Visible_6
	.global	GuiVar_GroupSetting_Visible_5
	.global	GuiVar_GroupSetting_Visible_4
	.global	GuiVar_GroupSetting_Visible_3
	.global	GuiVar_GroupSetting_Visible_2
	.global	GuiVar_GroupSetting_Visible_1
	.global	GuiVar_GroupSetting_Visible_0
	.global	GuiVar_GroupName_9
	.global	GuiVar_GroupName_8
	.global	GuiVar_GroupName_7
	.global	GuiVar_GroupName_6
	.global	GuiVar_GroupName_5
	.global	GuiVar_GroupName_4
	.global	GuiVar_GroupName_3
	.global	GuiVar_GroupName_2
	.global	GuiVar_GroupName_1
	.global	GuiVar_GroupName_0
	.global	GuiVar_GroupName
	.global	GuiVar_GRNetworkState
	.global	GuiVar_GRModel
	.global	GuiVar_GRMake
	.global	GuiVar_GRIPAddress
	.global	GuiVar_GRGPRSStatus
	.global	GuiVar_GRFirmwareVersion
	.global	GuiVar_GRECIO
	.global	GuiVar_GRCarrier
	.global	GuiVar_FreezeSwitchSelected
	.global	GuiVar_FreezeSwitchInUse
	.global	GuiVar_FreezeSwitchControllerName
	.global	GuiVar_FLPartOfChain
	.global	GuiVar_FlowTableGPM
	.global	GuiVar_FlowTable9Sta
	.global	GuiVar_FlowTable8Sta
	.global	GuiVar_FlowTable7Sta
	.global	GuiVar_FlowTable6Sta
	.global	GuiVar_FlowTable5Sta
	.global	GuiVar_FlowTable4Sta
	.global	GuiVar_FlowTable3Sta
	.global	GuiVar_FlowTable2Sta
	.global	GuiVar_FlowTable10Sta
	.global	GuiVar_FlowRecTimeStamp
	.global	GuiVar_FlowRecFlag
	.global	GuiVar_FlowRecExpected
	.global	GuiVar_FlowRecActual
	.global	GuiVar_FlowCheckingStopIrriTime
	.global	GuiVar_FlowCheckingStable
	.global	GuiVar_FlowCheckingSlotSize
	.global	GuiVar_FlowCheckingSlots
	.global	GuiVar_FlowCheckingPumpTime
	.global	GuiVar_FlowCheckingPumpState
	.global	GuiVar_FlowCheckingPumpMix
	.global	GuiVar_FlowCheckingOnForProbList
	.global	GuiVar_FlowCheckingNumOn
	.global	GuiVar_FlowCheckingMVTime
	.global	GuiVar_FlowCheckingMVState
	.global	GuiVar_FlowCheckingMVJOTime
	.global	GuiVar_FlowCheckingMaxOn
	.global	GuiVar_FlowCheckingLo
	.global	GuiVar_FlowCheckingLFTime
	.global	GuiVar_FlowCheckingIterations
	.global	GuiVar_FlowCheckingINTime
	.global	GuiVar_FlowCheckingHi
	.global	GuiVar_FlowCheckingHasNCMV
	.global	GuiVar_FlowCheckingGroupCnt3
	.global	GuiVar_FlowCheckingGroupCnt2
	.global	GuiVar_FlowCheckingGroupCnt1
	.global	GuiVar_FlowCheckingGroupCnt0
	.global	GuiVar_FlowCheckingFMs
	.global	GuiVar_FlowCheckingFlag6
	.global	GuiVar_FlowCheckingFlag5
	.global	GuiVar_FlowCheckingFlag4
	.global	GuiVar_FlowCheckingFlag3
	.global	GuiVar_FlowCheckingFlag2
	.global	GuiVar_FlowCheckingFlag1
	.global	GuiVar_FlowCheckingFlag0
	.global	GuiVar_FlowCheckingExpOn
	.global	GuiVar_FlowCheckingExp
	.global	GuiVar_FlowCheckingCycles
	.global	GuiVar_FlowCheckingChecked
	.global	GuiVar_FlowCheckingAllowedToLock
	.global	GuiVar_FlowCheckingAllowed
	.global	GuiVar_FLNumControllersInChain
	.global	GuiVar_FLControllerIndex_0
	.global	GuiVar_FinishTimesSystemGroup
	.global	GuiVar_FinishTimesStationGroup
	.global	GuiVar_FinishTimesStartTime
	.global	GuiVar_FinishTimesScrollBoxH
	.global	GuiVar_FinishTimesNotes
	.global	GuiVar_FinishTimesCalcTime
	.global	GuiVar_FinishTimesCalcProgressBar
	.global	GuiVar_FinishTimesCalcPercentComplete
	.global	GuiVar_FinishTimes
	.global	GuiVar_ETRainTableTable
	.global	GuiVar_ETRainTableShutdown
	.global	GuiVar_ETRainTableReport
	.global	GuiVar_ETRainTableRainStatus
	.global	GuiVar_ETRainTableRain
	.global	GuiVar_ETRainTableETStatus
	.global	GuiVar_ETRainTableET
	.global	GuiVar_ETRainTableDate
	.global	GuiVar_ETRainTableCurrentET
	.global	GuiVar_ETGageSkipTonight
	.global	GuiVar_ETGageRunawayGage
	.global	GuiVar_ETGagePercentFull
	.global	GuiVar_ETGageMaxPercent
	.global	GuiVar_ETGageLogPulses
	.global	GuiVar_ETGageInUse
	.global	GuiVar_ETGageInstalledAt
	.global	GuiVar_ENSubnetMask
	.global	GuiVar_ENObtainIPAutomatically
	.global	GuiVar_ENNetmask
	.global	GuiVar_ENModel
	.global	GuiVar_ENMACAddress
	.global	GuiVar_ENIPAddress_4
	.global	GuiVar_ENIPAddress_3
	.global	GuiVar_ENIPAddress_2
	.global	GuiVar_ENIPAddress_1
	.global	GuiVar_ENGateway_4
	.global	GuiVar_ENGateway_3
	.global	GuiVar_ENGateway_2
	.global	GuiVar_ENGateway_1
	.global	GuiVar_ENFirmwareVer
	.global	GuiVar_ENDHCPNameNotSet
	.global	GuiVar_ENDHCPName
	.global	GuiVar_DisplayType
	.global	GuiVar_DeviceExchangeSyncingRadios
	.global	GuiVar_DeviceExchangeProgressBar
	.global	GuiVar_DelayBetweenValvesInUse
	.global	GuiVar_DateTimeYear
	.global	GuiVar_DateTimeTimeZone
	.global	GuiVar_DateTimeShowButton
	.global	GuiVar_DateTimeSec
	.global	GuiVar_DateTimeMonth
	.global	GuiVar_DateTimeMin
	.global	GuiVar_DateTimeHour
	.global	GuiVar_DateTimeFullStr
	.global	GuiVar_DateTimeDayOfWeek
	.global	GuiVar_DateTimeDay
	.global	GuiVar_DateTimeAMPM
	.global	GuiVar_ContrastSliderPos
	.global	GuiVar_Contrast
	.global	GuiVar_ContactStatusSerialNumber
	.global	GuiVar_ContactStatusPort
	.global	GuiVar_ContactStatusNameAbbrv
	.global	GuiVar_ContactStatusFailures
	.global	GuiVar_ContactStatusAlive
	.global	GuiVar_CommTestSuppressConnections
	.global	GuiVar_CommTestStatus
	.global	GuiVar_CommTestShowDebug
	.global	GuiVar_CommTestIPPort
	.global	GuiVar_CommTestIPOctect4
	.global	GuiVar_CommTestIPOctect3
	.global	GuiVar_CommTestIPOctect2
	.global	GuiVar_CommTestIPOctect1
	.global	GuiVar_CommOptionRadioTestAvailable
	.global	GuiVar_CommOptionPortBInstalled
	.global	GuiVar_CommOptionPortBIndex
	.global	GuiVar_CommOptionPortAInstalled
	.global	GuiVar_CommOptionPortAIndex
	.global	GuiVar_CommOptionIsConnected
	.global	GuiVar_CommOptionInfoText
	.global	GuiVar_CommOptionDeviceExchangeResult
	.global	GuiVar_CommOptionCloudCommTestAvailable
	.global	GuiVar_ComboBoxItemString
	.global	GuiVar_ComboBoxItemIndex
	.global	GuiVar_ComboBox_Y1
	.global	GuiVar_ComboBox_X1
	.global	GuiVar_CodeDownloadUpdatingTPMicro
	.global	GuiVar_CodeDownloadState
	.global	GuiVar_CodeDownloadProgressBar
	.global	GuiVar_CodeDownloadPercentComplete
	.global	GuiVar_ChainStatsScans
	.global	GuiVar_ChainStatsPresentlyMakingTokens
	.global	GuiVar_ChainStatsNextContactSerNum
	.global	GuiVar_ChainStatsNextContact
	.global	GuiVar_ChainStatsIRRIIrriTRcvd
	.global	GuiVar_ChainStatsIRRIIrriRGen
	.global	GuiVar_ChainStatsIRRIChainTRcvd
	.global	GuiVar_ChainStatsIRRIChainRGen
	.global	GuiVar_ChainStatsInUseL
	.global	GuiVar_ChainStatsInUseK
	.global	GuiVar_ChainStatsInUseJ
	.global	GuiVar_ChainStatsInUseI
	.global	GuiVar_ChainStatsInUseH
	.global	GuiVar_ChainStatsInUseG
	.global	GuiVar_ChainStatsInUseF
	.global	GuiVar_ChainStatsInUseE
	.global	GuiVar_ChainStatsInUseD
	.global	GuiVar_ChainStatsInUseC
	.global	GuiVar_ChainStatsInUseB
	.global	GuiVar_ChainStatsInUseA
	.global	GuiVar_ChainStatsInForced
	.global	GuiVar_ChainStatsFOALIrriTGen
	.global	GuiVar_ChainStatsFOALIrriRRcvd
	.global	GuiVar_ChainStatsFOALChainTGen
	.global	GuiVar_ChainStatsFOALChainRRcvd
	.global	GuiVar_ChainStatsErrorsL
	.global	GuiVar_ChainStatsErrorsK
	.global	GuiVar_ChainStatsErrorsJ
	.global	GuiVar_ChainStatsErrorsI
	.global	GuiVar_ChainStatsErrorsH
	.global	GuiVar_ChainStatsErrorsG
	.global	GuiVar_ChainStatsErrorsF
	.global	GuiVar_ChainStatsErrorsE
	.global	GuiVar_ChainStatsErrorsD
	.global	GuiVar_ChainStatsErrorsC
	.global	GuiVar_ChainStatsErrorsB
	.global	GuiVar_ChainStatsErrorsA
	.global	GuiVar_ChainStatsControllersInChain
	.global	GuiVar_ChainStatsCommMngrState
	.global	GuiVar_ChainStatsCommMngrMode
	.global	GuiVar_BudgetZeroWarning
	.global	GuiVar_BudgetUseThisPeriod
	.global	GuiVar_BudgetUsedSYS
	.global	GuiVar_BudgetUsed
	.global	GuiVar_BudgetUnitHCF
	.global	GuiVar_BudgetUnitGallon
	.global	GuiVar_BudgetSysHasPOC
	.global	GuiVar_BudgetsInEffect
	.global	GuiVar_BudgetReportText
	.global	GuiVar_BudgetReductionLimit_9
	.global	GuiVar_BudgetReductionLimit_8
	.global	GuiVar_BudgetReductionLimit_7
	.global	GuiVar_BudgetReductionLimit_6
	.global	GuiVar_BudgetReductionLimit_5
	.global	GuiVar_BudgetReductionLimit_4
	.global	GuiVar_BudgetReductionLimit_3
	.global	GuiVar_BudgetReductionLimit_2
	.global	GuiVar_BudgetReductionLimit_1
	.global	GuiVar_BudgetReductionLimit_0
	.global	GuiVar_BudgetPeriodStart
	.global	GuiVar_BudgetPeriodsPerYearIdx
	.global	GuiVar_BudgetPeriodIdx
	.global	GuiVar_BudgetPeriodEnd
	.global	GuiVar_BudgetPeriod_1
	.global	GuiVar_BudgetPeriod_0
	.global	GuiVar_BudgetModeIdx
	.global	GuiVar_BudgetMainlineName
	.global	GuiVar_BudgetInUse
	.global	GuiVar_BudgetForThisPeriod
	.global	GuiVar_BudgetFlowTypeNonController
	.global	GuiVar_BudgetFlowTypeMVOR
	.global	GuiVar_BudgetFlowTypeIrrigation
	.global	GuiVar_BudgetFlowTypeIndex
	.global	GuiVar_BudgetExpectedUseThisPeriod
	.global	GuiVar_BudgetETPercentageUsed
	.global	GuiVar_BudgetEst
	.global	GuiVar_BudgetEntryOptionIdx
	.global	GuiVar_BudgetDaysLeftInPeriod
	.global	GuiVar_BudgetAnnualValue
	.global	GuiVar_BudgetAllocationSYS
	.global	GuiVar_BudgetAllocation
	.global	GuiVar_BacklightSliderPos
	.global	GuiVar_Backlight
	.global	GuiVar_AlertTime
	.global	GuiVar_AlertString
	.global	GuiVar_AlertDate
	.global	GuiVar_ActivationCode
	.global	GuiVar_AccessControlUserRole
	.global	GuiVar_AccessControlUser
	.global	GuiVar_AccessControlPwd
	.global	GuiVar_AccessControlLoggedIn
	.global	GuiVar_AccessControlEnabled
	.global	GuiVar_AboutWOption
	.global	GuiVar_AboutWMOption
	.global	GuiVar_AboutVersion
	.global	GuiVar_AboutTPSta41_48Terminal
	.global	GuiVar_AboutTPSta41_48Card
	.global	GuiVar_AboutTPSta33_40Terminal
	.global	GuiVar_AboutTPSta33_40Card
	.global	GuiVar_AboutTPSta25_32Terminal
	.global	GuiVar_AboutTPSta25_32Card
	.global	GuiVar_AboutTPSta17_24Terminal
	.global	GuiVar_AboutTPSta17_24Card
	.global	GuiVar_AboutTPSta09_16Terminal
	.global	GuiVar_AboutTPSta09_16Card
	.global	GuiVar_AboutTPSta01_08Terminal
	.global	GuiVar_AboutTPSta01_08Card
	.global	GuiVar_AboutTPPOCTerminal
	.global	GuiVar_AboutTPPOCCard
	.global	GuiVar_AboutTPMicroCard
	.global	GuiVar_AboutTPFuseCard
	.global	GuiVar_AboutTPDashWTerminal
	.global	GuiVar_AboutTPDashWCard
	.global	GuiVar_AboutTPDashMTerminal
	.global	GuiVar_AboutTPDashMCard
	.global	GuiVar_AboutTPDashLTerminal
	.global	GuiVar_AboutTPDashLCard
	.global	GuiVar_AboutTP2WireTerminal
	.global	GuiVar_AboutSSEOption
	.global	GuiVar_AboutSSEDOption
	.global	GuiVar_AboutSerialNumber
	.global	GuiVar_AboutNetworkID
	.global	GuiVar_AboutMSSEOption
	.global	GuiVar_AboutMOption
	.global	GuiVar_AboutModelNumber
	.global	GuiVar_AboutLOption
	.global	GuiVar_AboutHubOption
	.global	GuiVar_AboutFLOption
	.global	GuiVar_AboutDebug
	.global	GuiVar_AboutAquaponicsOption
	.global	GuiVar_AboutAntennaHoles
	.section	.bss.GuiVar_FinishTimesScrollBoxH,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesScrollBoxH, %object
	.size	GuiVar_FinishTimesScrollBoxH, 4
GuiVar_FinishTimesScrollBoxH:
	.space	4
	.section	.bss.GuiVar_StationGroupKcsAreCustom,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKcsAreCustom, %object
	.size	GuiVar_StationGroupKcsAreCustom, 4
GuiVar_StationGroupKcsAreCustom:
	.space	4
	.section	.bss.GuiVar_StatusChainDown,"aw",%nobits
	.align	2
	.type	GuiVar_StatusChainDown, %object
	.size	GuiVar_StatusChainDown, 4
GuiVar_StatusChainDown:
	.space	4
	.section	.bss.GuiVar_ETRainTableReport,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableReport, %object
	.size	GuiVar_ETRainTableReport, 4
GuiVar_ETRainTableReport:
	.space	4
	.section	.bss.GuiVar_BudgetDaysLeftInPeriod,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetDaysLeftInPeriod, %object
	.size	GuiVar_BudgetDaysLeftInPeriod, 4
GuiVar_BudgetDaysLeftInPeriod:
	.space	4
	.section	.bss.GuiVar_StationDescription,"aw",%nobits
	.type	GuiVar_StationDescription, %object
	.size	GuiVar_StationDescription, 49
GuiVar_StationDescription:
	.space	49
	.section	.bss.GuiVar_ETRainTableETStatus,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableETStatus, %object
	.size	GuiVar_ETRainTableETStatus, 4
GuiVar_ETRainTableETStatus:
	.space	4
	.section	.bss.GuiVar_MainMenuFLOptionExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuFLOptionExists, %object
	.size	GuiVar_MainMenuFLOptionExists, 4
GuiVar_MainMenuFLOptionExists:
	.space	4
	.section	.bss.GuiVar_StationInfoGroupHasStartTime,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoGroupHasStartTime, %object
	.size	GuiVar_StationInfoGroupHasStartTime, 4
GuiVar_StationInfoGroupHasStartTime:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsBODs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsBODs, %object
	.size	GuiVar_TwoWireStatsBODs, 4
GuiVar_TwoWireStatsBODs:
	.space	4
	.section	.bss.GuiVar_LightCalendarWeek2Sun,"aw",%nobits
	.type	GuiVar_LightCalendarWeek2Sun, %object
	.size	GuiVar_LightCalendarWeek2Sun, 3
GuiVar_LightCalendarWeek2Sun:
	.space	3
	.section	.bss.GuiVar_WENWPAEncryptionTKIP,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEncryptionTKIP, %object
	.size	GuiVar_WENWPAEncryptionTKIP, 4
GuiVar_WENWPAEncryptionTKIP:
	.space	4
	.section	.bss.GuiVar_BudgetsInEffect,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetsInEffect, %object
	.size	GuiVar_BudgetsInEffect, 4
GuiVar_BudgetsInEffect:
	.space	4
	.section	.bss.GuiVar_FlowRecActual,"aw",%nobits
	.align	2
	.type	GuiVar_FlowRecActual, %object
	.size	GuiVar_FlowRecActual, 4
GuiVar_FlowRecActual:
	.space	4
	.section	.bss.GuiVar_itmStationName,"aw",%nobits
	.type	GuiVar_itmStationName, %object
	.size	GuiVar_itmStationName, 49
GuiVar_itmStationName:
	.space	49
	.section	.bss.GuiVar_BudgetReportText,"aw",%nobits
	.type	GuiVar_BudgetReportText, %object
	.size	GuiVar_BudgetReportText, 225
GuiVar_BudgetReportText:
	.space	225
	.section	.bss.GuiVar_FlowCheckingLo,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingLo, %object
	.size	GuiVar_FlowCheckingLo, 4
GuiVar_FlowCheckingLo:
	.space	4
	.section	.bss.GuiVar_ETGageRunawayGage,"aw",%nobits
	.align	2
	.type	GuiVar_ETGageRunawayGage, %object
	.size	GuiVar_ETGageRunawayGage, 4
GuiVar_ETGageRunawayGage:
	.space	4
	.section	.bss.GuiVar_WindGageResumeSpeed,"aw",%nobits
	.align	2
	.type	GuiVar_WindGageResumeSpeed, %object
	.size	GuiVar_WindGageResumeSpeed, 4
GuiVar_WindGageResumeSpeed:
	.space	4
	.section	.bss.GuiVar_DeviceExchangeSyncingRadios,"aw",%nobits
	.align	2
	.type	GuiVar_DeviceExchangeSyncingRadios, %object
	.size	GuiVar_DeviceExchangeSyncingRadios, 4
GuiVar_DeviceExchangeSyncingRadios:
	.space	4
	.section	.bss.GuiVar_LightsOutputIndex_0,"aw",%nobits
	.align	2
	.type	GuiVar_LightsOutputIndex_0, %object
	.size	GuiVar_LightsOutputIndex_0, 4
GuiVar_LightsOutputIndex_0:
	.space	4
	.section	.bss.GuiVar_BudgetMainlineName,"aw",%nobits
	.type	GuiVar_BudgetMainlineName, %object
	.size	GuiVar_BudgetMainlineName, 49
GuiVar_BudgetMainlineName:
	.space	49
	.section	.bss.GuiVar_ScheduleStopTimeEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleStopTimeEnabled, %object
	.size	GuiVar_ScheduleStopTimeEnabled, 4
GuiVar_ScheduleStopTimeEnabled:
	.space	4
	.section	.bss.GuiVar_FlowCheckingExpOn,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingExpOn, %object
	.size	GuiVar_FlowCheckingExpOn, 4
GuiVar_FlowCheckingExpOn:
	.space	4
	.section	.bss.GuiVar_MVORClose3Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose3Enabled, %object
	.size	GuiVar_MVORClose3Enabled, 4
GuiVar_MVORClose3Enabled:
	.space	4
	.section	.bss.GuiVar_CommOptionPortBIndex,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionPortBIndex, %object
	.size	GuiVar_CommOptionPortBIndex, 4
GuiVar_CommOptionPortBIndex:
	.space	4
	.section	.bss.GuiVar_SRSubnetRcvSlave,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetRcvSlave, %object
	.size	GuiVar_SRSubnetRcvSlave, 4
GuiVar_SRSubnetRcvSlave:
	.space	4
	.section	.bss.GuiVar_WENWPAIEEE8021X,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAIEEE8021X, %object
	.size	GuiVar_WENWPAIEEE8021X, 4
GuiVar_WENWPAIEEE8021X:
	.space	4
	.section	.bss.GuiVar_MenuScreenToShow,"aw",%nobits
	.align	2
	.type	GuiVar_MenuScreenToShow, %object
	.size	GuiVar_MenuScreenToShow, 4
GuiVar_MenuScreenToShow:
	.space	4
	.section	.bss.GuiVar_StationGroupSoilType,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSoilType, %object
	.size	GuiVar_StationGroupSoilType, 4
GuiVar_StationGroupSoilType:
	.space	4
	.section	.bss.GuiVar_FreezeSwitchControllerName,"aw",%nobits
	.type	GuiVar_FreezeSwitchControllerName, %object
	.size	GuiVar_FreezeSwitchControllerName, 49
GuiVar_FreezeSwitchControllerName:
	.space	49
	.section	.bss.GuiVar_TestStationStatus,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationStatus, %object
	.size	GuiVar_TestStationStatus, 4
GuiVar_TestStationStatus:
	.space	4
	.section	.bss.GuiVar_TwoWireDecoderSNToAssign,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDecoderSNToAssign, %object
	.size	GuiVar_TwoWireDecoderSNToAssign, 4
GuiVar_TwoWireDecoderSNToAssign:
	.space	4
	.section	.bss.GuiVar_ScheduleTypeScrollItem,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleTypeScrollItem, %object
	.size	GuiVar_ScheduleTypeScrollItem, 4
GuiVar_ScheduleTypeScrollItem:
	.space	4
	.section	.bss.GuiVar_BudgetExpectedUseThisPeriod,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetExpectedUseThisPeriod, %object
	.size	GuiVar_BudgetExpectedUseThisPeriod, 4
GuiVar_BudgetExpectedUseThisPeriod:
	.space	4
	.section	.bss.GuiVar_RptController,"aw",%nobits
	.align	2
	.type	GuiVar_RptController, %object
	.size	GuiVar_RptController, 4
GuiVar_RptController:
	.space	4
	.section	.bss.GuiVar_MoisDecoderSN,"aw",%nobits
	.align	2
	.type	GuiVar_MoisDecoderSN, %object
	.size	GuiVar_MoisDecoderSN, 4
GuiVar_MoisDecoderSN:
	.space	4
	.section	.bss.GuiVar_RptLastMeasuredCurrent,"aw",%nobits
	.align	2
	.type	GuiVar_RptLastMeasuredCurrent, %object
	.size	GuiVar_RptLastMeasuredCurrent, 4
GuiVar_RptLastMeasuredCurrent:
	.space	4
	.section	.bss.GuiVar_SRSubnetRcvMaster,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetRcvMaster, %object
	.size	GuiVar_SRSubnetRcvMaster, 4
GuiVar_SRSubnetRcvMaster:
	.space	4
	.section	.bss.GuiVar_ManualPStartTime1Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime1Enabled, %object
	.size	GuiVar_ManualPStartTime1Enabled, 4
GuiVar_ManualPStartTime1Enabled:
	.space	4
	.section	.bss.GuiVar_LiveScreensCommCentralEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommCentralEnabled, %object
	.size	GuiVar_LiveScreensCommCentralEnabled, 4
GuiVar_LiveScreensCommCentralEnabled:
	.space	4
	.section	.bss.GuiVar_SystemPreserves5SecMostRecent,"aw",%nobits
	.align	2
	.type	GuiVar_SystemPreserves5SecMostRecent, %object
	.size	GuiVar_SystemPreserves5SecMostRecent, 4
GuiVar_SystemPreserves5SecMostRecent:
	.space	4
	.section	.bss.GuiVar_CommOptionInfoText,"aw",%nobits
	.type	GuiVar_CommOptionInfoText, %object
	.size	GuiVar_CommOptionInfoText, 49
GuiVar_CommOptionInfoText:
	.space	49
	.section	.bss.GuiVar_StationGroupAllowableDepletion,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupAllowableDepletion, %object
	.size	GuiVar_StationGroupAllowableDepletion, 4
GuiVar_StationGroupAllowableDepletion:
	.space	4
	.section	.bss.GuiVar_PercentAdjustPositive,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPositive, %object
	.size	GuiVar_PercentAdjustPositive, 4
GuiVar_PercentAdjustPositive:
	.space	4
	.section	.bss.GuiVar_ETRainTableTable,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableTable, %object
	.size	GuiVar_ETRainTableTable, 4
GuiVar_ETRainTableTable:
	.space	4
	.section	.bss.GuiVar_FinishTimesCalcTime,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesCalcTime, %object
	.size	GuiVar_FinishTimesCalcTime, 4
GuiVar_FinishTimesCalcTime:
	.space	4
	.section	.bss.GuiVar_FlowCheckingSlotSize,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingSlotSize, %object
	.size	GuiVar_FlowCheckingSlotSize, 4
GuiVar_FlowCheckingSlotSize:
	.space	4
	.section	.bss.GuiVar_IrriCommChainConfigCurrentSendFromIrri,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigCurrentSendFromIrri, %object
	.size	GuiVar_IrriCommChainConfigCurrentSendFromIrri, 4
GuiVar_IrriCommChainConfigCurrentSendFromIrri:
	.space	4
	.section	.bss.GuiVar_ManualPWillNotRunReason,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWillNotRunReason, %object
	.size	GuiVar_ManualPWillNotRunReason, 4
GuiVar_ManualPWillNotRunReason:
	.space	4
	.section	.bss.GuiVar_MVOROpen2Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen2Enabled, %object
	.size	GuiVar_MVOROpen2Enabled, 4
GuiVar_MVOROpen2Enabled:
	.space	4
	.section	.bss.GuiVar_StationInfoEstMin,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoEstMin, %object
	.size	GuiVar_StationInfoEstMin, 4
GuiVar_StationInfoEstMin:
	.space	4
	.section	.bss.GuiVar_SRMode,"aw",%nobits
	.align	2
	.type	GuiVar_SRMode, %object
	.size	GuiVar_SRMode, 4
GuiVar_SRMode:
	.space	4
	.section	.bss.GuiVar_StationInfoIStatus,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoIStatus, %object
	.size	GuiVar_StationInfoIStatus, 4
GuiVar_StationInfoIStatus:
	.space	4
	.section	.bss.GuiVar_LightsCalendarHighlightXOffset,"aw",%nobits
	.align	2
	.type	GuiVar_LightsCalendarHighlightXOffset, %object
	.size	GuiVar_LightsCalendarHighlightXOffset, 4
GuiVar_LightsCalendarHighlightXOffset:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsEEPCRCS2Params,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsEEPCRCS2Params, %object
	.size	GuiVar_TwoWireStatsEEPCRCS2Params, 4
GuiVar_TwoWireStatsEEPCRCS2Params:
	.space	4
	.section	.bss.GuiVar_CodeDownloadPercentComplete,"aw",%nobits
	.align	2
	.type	GuiVar_CodeDownloadPercentComplete, %object
	.size	GuiVar_CodeDownloadPercentComplete, 4
GuiVar_CodeDownloadPercentComplete:
	.space	4
	.section	.bss.GuiVar_MVORRunTime,"aw",%nobits
	.align	2
	.type	GuiVar_MVORRunTime, %object
	.size	GuiVar_MVORRunTime, 4
GuiVar_MVORRunTime:
	.space	4
	.section	.bss.GuiVar_POCUsage,"aw",%nobits
	.align	2
	.type	GuiVar_POCUsage, %object
	.size	GuiVar_POCUsage, 4
GuiVar_POCUsage:
	.space	4
	.section	.bss.GuiVar_LightsStopTimeEnabled1,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStopTimeEnabled1, %object
	.size	GuiVar_LightsStopTimeEnabled1, 4
GuiVar_LightsStopTimeEnabled1:
	.space	4
	.section	.bss.GuiVar_LightsStopTimeEnabled2,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStopTimeEnabled2, %object
	.size	GuiVar_LightsStopTimeEnabled2, 4
GuiVar_LightsStopTimeEnabled2:
	.space	4
	.section	.bss.GuiVar_FlowCheckingExp,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingExp, %object
	.size	GuiVar_FlowCheckingExp, 4
GuiVar_FlowCheckingExp:
	.space	4
	.section	.bss.GuiVar_BudgetPeriod_0,"aw",%nobits
	.type	GuiVar_BudgetPeriod_0, %object
	.size	GuiVar_BudgetPeriod_0, 17
GuiVar_BudgetPeriod_0:
	.space	17
	.section	.bss.GuiVar_RadioTestRoundTripMaxMS,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestRoundTripMaxMS, %object
	.size	GuiVar_RadioTestRoundTripMaxMS, 4
GuiVar_RadioTestRoundTripMaxMS:
	.space	4
	.section	.bss.GuiVar_TwoWireStaStrB,"aw",%nobits
	.type	GuiVar_TwoWireStaStrB, %object
	.size	GuiVar_TwoWireStaStrB, 5
GuiVar_TwoWireStaStrB:
	.space	5
	.section	.bss.GuiVar_ContactStatusFailures,"aw",%nobits
	.align	2
	.type	GuiVar_ContactStatusFailures, %object
	.size	GuiVar_ContactStatusFailures, 4
GuiVar_ContactStatusFailures:
	.space	4
	.section	.bss.GuiVar_POCPreservesGroupID,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesGroupID, %object
	.size	GuiVar_POCPreservesGroupID, 4
GuiVar_POCPreservesGroupID:
	.space	4
	.section	.bss.GuiVar_StationGroupInUse,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupInUse, %object
	.size	GuiVar_StationGroupInUse, 4
GuiVar_StationGroupInUse:
	.space	4
	.section	.bss.GuiVar_POCFlowMeterOffset1,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterOffset1, %object
	.size	GuiVar_POCFlowMeterOffset1, 4
GuiVar_POCFlowMeterOffset1:
	.space	4
	.section	.bss.GuiVar_POCFlowMeterOffset2,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterOffset2, %object
	.size	GuiVar_POCFlowMeterOffset2, 4
GuiVar_POCFlowMeterOffset2:
	.space	4
	.section	.bss.GuiVar_MVOROpen3Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen3Enabled, %object
	.size	GuiVar_MVOROpen3Enabled, 4
GuiVar_MVOROpen3Enabled:
	.space	4
	.section	.bss.GuiVar_BudgetFlowTypeIndex,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetFlowTypeIndex, %object
	.size	GuiVar_BudgetFlowTypeIndex, 4
GuiVar_BudgetFlowTypeIndex:
	.space	4
	.section	.bss.GuiVar_AboutTP2WireTerminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTP2WireTerminal, %object
	.size	GuiVar_AboutTP2WireTerminal, 4
GuiVar_AboutTP2WireTerminal:
	.space	4
	.section	.bss.GuiVar_RptRReGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptRReGal, %object
	.size	GuiVar_RptRReGal, 8
GuiVar_RptRReGal:
	.space	8
	.section	.bss.GuiVar_RptIrrigMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptIrrigMin, %object
	.size	GuiVar_RptIrrigMin, 4
GuiVar_RptIrrigMin:
	.space	4
	.section	.bss.GuiVar_PercentAdjustPercent_0,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_0, %object
	.size	GuiVar_PercentAdjustPercent_0, 4
GuiVar_PercentAdjustPercent_0:
	.space	4
	.section	.bss.GuiVar_PercentAdjustPercent_1,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_1, %object
	.size	GuiVar_PercentAdjustPercent_1, 4
GuiVar_PercentAdjustPercent_1:
	.space	4
	.section	.bss.GuiVar_PercentAdjustPercent_2,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_2, %object
	.size	GuiVar_PercentAdjustPercent_2, 4
GuiVar_PercentAdjustPercent_2:
	.space	4
	.section	.bss.GuiVar_MVORTimeRemaining,"aw",%nobits
	.align	2
	.type	GuiVar_MVORTimeRemaining, %object
	.size	GuiVar_MVORTimeRemaining, 4
GuiVar_MVORTimeRemaining:
	.space	4
	.section	.bss.GuiVar_GroupName_1,"aw",%nobits
	.type	GuiVar_GroupName_1, %object
	.size	GuiVar_GroupName_1, 49
GuiVar_GroupName_1:
	.space	49
	.section	.bss.GuiVar_GroupName_2,"aw",%nobits
	.type	GuiVar_GroupName_2, %object
	.size	GuiVar_GroupName_2, 49
GuiVar_GroupName_2:
	.space	49
	.section	.bss.GuiVar_GroupName_3,"aw",%nobits
	.type	GuiVar_GroupName_3, %object
	.size	GuiVar_GroupName_3, 49
GuiVar_GroupName_3:
	.space	49
	.section	.bss.GuiVar_PercentAdjustPercent_7,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_7, %object
	.size	GuiVar_PercentAdjustPercent_7, 4
GuiVar_PercentAdjustPercent_7:
	.space	4
	.section	.bss.GuiVar_StationInfoShowPercentOfET,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoShowPercentOfET, %object
	.size	GuiVar_StationInfoShowPercentOfET, 4
GuiVar_StationInfoShowPercentOfET:
	.space	4
	.section	.bss.GuiVar_PercentAdjustPercent_9,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_9, %object
	.size	GuiVar_PercentAdjustPercent_9, 4
GuiVar_PercentAdjustPercent_9:
	.space	4
	.section	.bss.GuiVar_RptCycles,"aw",%nobits
	.align	2
	.type	GuiVar_RptCycles, %object
	.size	GuiVar_RptCycles, 4
GuiVar_RptCycles:
	.space	4
	.section	.bss.GuiVar_MoisSetPoint_High,"aw",%nobits
	.align	2
	.type	GuiVar_MoisSetPoint_High, %object
	.size	GuiVar_MoisSetPoint_High, 4
GuiVar_MoisSetPoint_High:
	.space	4
	.section	.bss.GuiVar_GroupName_9,"aw",%nobits
	.type	GuiVar_GroupName_9, %object
	.size	GuiVar_GroupName_9, 49
GuiVar_GroupName_9:
	.space	49
	.section	.bss.GuiVar_GroupSetting_Visible_9,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_9, %object
	.size	GuiVar_GroupSetting_Visible_9, 4
GuiVar_GroupSetting_Visible_9:
	.space	4
	.section	.bss.GuiVar_MoisTempAction_High,"aw",%nobits
	.align	2
	.type	GuiVar_MoisTempAction_High, %object
	.size	GuiVar_MoisTempAction_High, 4
GuiVar_MoisTempAction_High:
	.space	4
	.section	.bss.GuiVar_SerialPortXmittingB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmittingB, %object
	.size	GuiVar_SerialPortXmittingB, 4
GuiVar_SerialPortXmittingB:
	.space	4
	.section	.bss.GuiVar_FreezeSwitchInUse,"aw",%nobits
	.align	2
	.type	GuiVar_FreezeSwitchInUse, %object
	.size	GuiVar_FreezeSwitchInUse, 4
GuiVar_FreezeSwitchInUse:
	.space	4
	.section	.bss.GuiVar_LightScheduledStart,"aw",%nobits
	.type	GuiVar_LightScheduledStart, %object
	.size	GuiVar_LightScheduledStart, 33
GuiVar_LightScheduledStart:
	.space	33
	.section	.bss.GuiVar_LRPort,"aw",%nobits
	.align	2
	.type	GuiVar_LRPort, %object
	.size	GuiVar_LRPort, 4
GuiVar_LRPort:
	.space	4
	.section	.bss.GuiVar_LightsAreEnergized,"aw",%nobits
	.align	2
	.type	GuiVar_LightsAreEnergized, %object
	.size	GuiVar_LightsAreEnergized, 4
GuiVar_LightsAreEnergized:
	.space	4
	.section	.bss.GuiVar_RadioTestRoundTripMinMS,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestRoundTripMinMS, %object
	.size	GuiVar_RadioTestRoundTripMinMS, 4
GuiVar_RadioTestRoundTripMinMS:
	.space	4
	.section	.bss.GuiVar_DateTimeHour,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeHour, %object
	.size	GuiVar_DateTimeHour, 4
GuiVar_DateTimeHour:
	.space	4
	.section	.bss.GuiVar_POCPhysicallyAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_POCPhysicallyAvailable, %object
	.size	GuiVar_POCPhysicallyAvailable, 4
GuiVar_POCPhysicallyAvailable:
	.space	4
	.section	.bss.GuiVar_SystemPreservesStabilityNext,"aw",%nobits
	.align	2
	.type	GuiVar_SystemPreservesStabilityNext, %object
	.size	GuiVar_SystemPreservesStabilityNext, 4
GuiVar_SystemPreservesStabilityNext:
	.space	4
	.section	.bss.GuiVar_LightCalendarWeek1Mon,"aw",%nobits
	.type	GuiVar_LightCalendarWeek1Mon, %object
	.size	GuiVar_LightCalendarWeek1Mon, 3
GuiVar_LightCalendarWeek1Mon:
	.space	3
	.section	.bss.GuiVar_FLNumControllersInChain,"aw",%nobits
	.align	2
	.type	GuiVar_FLNumControllersInChain, %object
	.size	GuiVar_FLNumControllersInChain, 4
GuiVar_FLNumControllersInChain:
	.space	4
	.section	.bss.GuiVar_LRMode,"aw",%nobits
	.align	2
	.type	GuiVar_LRMode, %object
	.size	GuiVar_LRMode, 4
GuiVar_LRMode:
	.space	4
	.section	.bss.GuiVar_BudgetInUse,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetInUse, %object
	.size	GuiVar_BudgetInUse, 4
GuiVar_BudgetInUse:
	.space	4
	.section	.bss.GuiVar_FlowCheckingHi,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingHi, %object
	.size	GuiVar_FlowCheckingHi, 4
GuiVar_FlowCheckingHi:
	.space	4
	.section	.bss.GuiVar_LRFirmwareVer,"aw",%nobits
	.type	GuiVar_LRFirmwareVer, %object
	.size	GuiVar_LRFirmwareVer, 49
GuiVar_LRFirmwareVer:
	.space	49
	.section	.bss.GuiVar_ChainStatsCommMngrState,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsCommMngrState, %object
	.size	GuiVar_ChainStatsCommMngrState, 4
GuiVar_ChainStatsCommMngrState:
	.space	4
	.section	.bss.GuiVar_StationGroupKc10,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc10, %object
	.size	GuiVar_StationGroupKc10, 4
GuiVar_StationGroupKc10:
	.space	4
	.section	.bss.GuiVar_StationGroupKc11,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc11, %object
	.size	GuiVar_StationGroupKc11, 4
GuiVar_StationGroupKc11:
	.space	4
	.section	.bss.GuiVar_StationGroupKc12,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc12, %object
	.size	GuiVar_StationGroupKc12, 4
GuiVar_StationGroupKc12:
	.space	4
	.section	.bss.GuiVar_AboutTPDashWTerminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashWTerminal, %object
	.size	GuiVar_AboutTPDashWTerminal, 4
GuiVar_AboutTPDashWTerminal:
	.space	4
	.section	.bss.GuiVar_SerialPortXmitTP,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmitTP, %object
	.size	GuiVar_SerialPortXmitTP, 4
GuiVar_SerialPortXmitTP:
	.space	4
	.section	.bss.GuiVar_FinishTimesCalcProgressBar,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesCalcProgressBar, %object
	.size	GuiVar_FinishTimesCalcProgressBar, 4
GuiVar_FinishTimesCalcProgressBar:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsEEPCRCComParams,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsEEPCRCComParams, %object
	.size	GuiVar_TwoWireStatsEEPCRCComParams, 4
GuiVar_TwoWireStatsEEPCRCComParams:
	.space	4
	.section	.bss.GuiVar_LightsDate,"aw",%nobits
	.type	GuiVar_LightsDate, %object
	.size	GuiVar_LightsDate, 49
GuiVar_LightsDate:
	.space	49
	.section	.bss.GuiVar_itmMenuItem,"aw",%nobits
	.type	GuiVar_itmMenuItem, %object
	.size	GuiVar_itmMenuItem, 65
GuiVar_itmMenuItem:
	.space	65
	.section	.bss.GuiVar_RadioTestRoundTripAvgMS,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestRoundTripAvgMS, %object
	.size	GuiVar_RadioTestRoundTripAvgMS, 4
GuiVar_RadioTestRoundTripAvgMS:
	.space	4
	.section	.bss.GuiVar_BudgetETPercentageUsed,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetETPercentageUsed, %object
	.size	GuiVar_BudgetETPercentageUsed, 4
GuiVar_BudgetETPercentageUsed:
	.space	4
	.section	.bss.GuiVar_TwoWireDiscoveredPOCDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDiscoveredPOCDecoders, %object
	.size	GuiVar_TwoWireDiscoveredPOCDecoders, 4
GuiVar_TwoWireDiscoveredPOCDecoders:
	.space	4
	.section	.bss.GuiVar_POCFlowMeterType1,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterType1, %object
	.size	GuiVar_POCFlowMeterType1, 4
GuiVar_POCFlowMeterType1:
	.space	4
	.section	.bss.GuiVar_POCFlowMeterType2,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterType2, %object
	.size	GuiVar_POCFlowMeterType2, 4
GuiVar_POCFlowMeterType2:
	.space	4
	.section	.bss.GuiVar_POCFlowMeterType3,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterType3, %object
	.size	GuiVar_POCFlowMeterType3, 4
GuiVar_POCFlowMeterType3:
	.space	4
	.section	.bss.GuiVar_AccessControlPwd,"aw",%nobits
	.type	GuiVar_AccessControlPwd, %object
	.size	GuiVar_AccessControlPwd, 49
GuiVar_AccessControlPwd:
	.space	49
	.section	.bss.GuiVar_RadioTestStartTime,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestStartTime, %object
	.size	GuiVar_RadioTestStartTime, 4
GuiVar_RadioTestStartTime:
	.space	4
	.section	.bss.GuiVar_RadioTestSuccessfulPercent,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestSuccessfulPercent, %object
	.size	GuiVar_RadioTestSuccessfulPercent, 4
GuiVar_RadioTestSuccessfulPercent:
	.space	4
	.section	.bss.GuiVar_LightsStartTime2InMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStartTime2InMinutes, %object
	.size	GuiVar_LightsStartTime2InMinutes, 4
GuiVar_LightsStartTime2InMinutes:
	.space	4
	.section	.bss.GuiVar_StatusRainBucketReading,"aw",%nobits
	.align	2
	.type	GuiVar_StatusRainBucketReading, %object
	.size	GuiVar_StatusRainBucketReading, 4
GuiVar_StatusRainBucketReading:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxDecRstMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxDecRstMsgs, %object
	.size	GuiVar_TwoWireStatsRxDecRstMsgs, 4
GuiVar_TwoWireStatsRxDecRstMsgs:
	.space	4
	.section	.bss.GuiVar_StatusWindGageConnected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusWindGageConnected, %object
	.size	GuiVar_StatusWindGageConnected, 4
GuiVar_StatusWindGageConnected:
	.space	4
	.section	.bss.GuiVar_BudgetUsed,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetUsed, %object
	.size	GuiVar_BudgetUsed, 4
GuiVar_BudgetUsed:
	.space	4
	.section	.bss.GuiVar_StatusNextScheduledIrriDate,"aw",%nobits
	.type	GuiVar_StatusNextScheduledIrriDate, %object
	.size	GuiVar_StatusNextScheduledIrriDate, 49
GuiVar_StatusNextScheduledIrriDate:
	.space	49
	.section	.bss.GuiVar_FinishTimes,"aw",%nobits
	.type	GuiVar_FinishTimes, %object
	.size	GuiVar_FinishTimes, 65
GuiVar_FinishTimes:
	.space	65
	.section	.bss.GuiVar_StationGroupUsableRain,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupUsableRain, %object
	.size	GuiVar_StationGroupUsableRain, 4
GuiVar_StationGroupUsableRain:
	.space	4
	.section	.bss.GuiVar_TestStationAutoOffTime,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationAutoOffTime, %object
	.size	GuiVar_TestStationAutoOffTime, 4
GuiVar_TestStationAutoOffTime:
	.space	4
	.section	.bss.GuiVar_ScrollBoxHorizScrollPos,"aw",%nobits
	.align	2
	.type	GuiVar_ScrollBoxHorizScrollPos, %object
	.size	GuiVar_ScrollBoxHorizScrollPos, 4
GuiVar_ScrollBoxHorizScrollPos:
	.space	4
	.section	.bss.GuiVar_POCMVType1,"aw",%nobits
	.align	2
	.type	GuiVar_POCMVType1, %object
	.size	GuiVar_POCMVType1, 4
GuiVar_POCMVType1:
	.space	4
	.section	.bss.GuiVar_ScheduleStopTime,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleStopTime, %object
	.size	GuiVar_ScheduleStopTime, 4
GuiVar_ScheduleStopTime:
	.space	4
	.section	.bss.GuiVar_FinishTimesSystemGroup,"aw",%nobits
	.type	GuiVar_FinishTimesSystemGroup, %object
	.size	GuiVar_FinishTimesSystemGroup, 21
GuiVar_FinishTimesSystemGroup:
	.space	21
	.section	.bss.GuiVar_RptWalkThruMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptWalkThruMin, %object
	.size	GuiVar_RptWalkThruMin, 4
GuiVar_RptWalkThruMin:
	.space	4
	.section	.bss.GuiVar_StatusElectricalErrors,"aw",%nobits
	.align	2
	.type	GuiVar_StatusElectricalErrors, %object
	.size	GuiVar_StatusElectricalErrors, 4
GuiVar_StatusElectricalErrors:
	.space	4
	.section	.bss.GuiVar_MainMenuAquaponicsMode,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuAquaponicsMode, %object
	.size	GuiVar_MainMenuAquaponicsMode, 4
GuiVar_MainMenuAquaponicsMode:
	.space	4
	.section	.bss.GuiVar_GroupName,"aw",%nobits
	.type	GuiVar_GroupName, %object
	.size	GuiVar_GroupName, 65
GuiVar_GroupName:
	.space	65
	.section	.bss.GuiVar_DateTimeShowButton,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeShowButton, %object
	.size	GuiVar_DateTimeShowButton, 4
GuiVar_DateTimeShowButton:
	.space	4
	.section	.bss.GuiVar_TwoWireNumDiscoveredStaDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireNumDiscoveredStaDecoders, %object
	.size	GuiVar_TwoWireNumDiscoveredStaDecoders, 4
GuiVar_TwoWireNumDiscoveredStaDecoders:
	.space	4
	.section	.bss.GuiVar_AccessControlEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_AccessControlEnabled, %object
	.size	GuiVar_AccessControlEnabled, 4
GuiVar_AccessControlEnabled:
	.space	4
	.section	.bss.GuiVar_POCFMType1IsBermad,"aw",%nobits
	.align	2
	.type	GuiVar_POCFMType1IsBermad, %object
	.size	GuiVar_POCFMType1IsBermad, 4
GuiVar_POCFMType1IsBermad:
	.space	4
	.section	.bss.GuiVar_BudgetUnitHCF,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetUnitHCF, %object
	.size	GuiVar_BudgetUnitHCF, 4
GuiVar_BudgetUnitHCF:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxMsgs, %object
	.size	GuiVar_TwoWireStatsRxMsgs, 4
GuiVar_TwoWireStatsRxMsgs:
	.space	4
	.section	.bss.GuiVar_StatusSystemFlowSystem,"aw",%nobits
	.type	GuiVar_StatusSystemFlowSystem, %object
	.size	GuiVar_StatusSystemFlowSystem, 49
GuiVar_StatusSystemFlowSystem:
	.space	49
	.section	.bss.GuiVar_ManualPWillRun,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWillRun, %object
	.size	GuiVar_ManualPWillRun, 4
GuiVar_ManualPWillRun:
	.space	4
	.section	.bss.GuiVar_LightCalendarWeek1Wed,"aw",%nobits
	.type	GuiVar_LightCalendarWeek1Wed, %object
	.size	GuiVar_LightCalendarWeek1Wed, 3
GuiVar_LightCalendarWeek1Wed:
	.space	3
	.section	.bss.GuiVar_BacklightSliderPos,"aw",%nobits
	.align	2
	.type	GuiVar_BacklightSliderPos, %object
	.size	GuiVar_BacklightSliderPos, 4
GuiVar_BacklightSliderPos:
	.space	4
	.section	.bss.GuiVar_ScheduleBeginDateStr,"aw",%nobits
	.type	GuiVar_ScheduleBeginDateStr, %object
	.size	GuiVar_ScheduleBeginDateStr, 49
GuiVar_ScheduleBeginDateStr:
	.space	49
	.section	.bss.GuiVar_LRTransmitPower,"aw",%nobits
	.align	2
	.type	GuiVar_LRTransmitPower, %object
	.size	GuiVar_LRTransmitPower, 4
GuiVar_LRTransmitPower:
	.space	4
	.section	.bss.GuiVar_FlowCheckingAllowed,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingAllowed, %object
	.size	GuiVar_FlowCheckingAllowed, 4
GuiVar_FlowCheckingAllowed:
	.space	4
	.section	.bss.GuiVar_ComboBoxItemString,"aw",%nobits
	.type	GuiVar_ComboBoxItemString, %object
	.size	GuiVar_ComboBoxItemString, 49
GuiVar_ComboBoxItemString:
	.space	49
	.section	.bss.GuiVar_ChainStatsFOALChainTGen,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsFOALChainTGen, %object
	.size	GuiVar_ChainStatsFOALChainTGen, 4
GuiVar_ChainStatsFOALChainTGen:
	.space	4
	.section	.bss.GuiVar_StationInfoNumber,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoNumber, %object
	.size	GuiVar_StationInfoNumber, 4
GuiVar_StationInfoNumber:
	.space	4
	.section	.bss.GuiVar_SystemName,"aw",%nobits
	.type	GuiVar_SystemName, %object
	.size	GuiVar_SystemName, 49
GuiVar_SystemName:
	.space	49
	.section	.bss.GuiVar_MVOROpen0Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen0Enabled, %object
	.size	GuiVar_MVOROpen0Enabled, 4
GuiVar_MVOROpen0Enabled:
	.space	4
	.section	.bss.GuiVar_MainMenuBypassExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuBypassExists, %object
	.size	GuiVar_MainMenuBypassExists, 4
GuiVar_MainMenuBypassExists:
	.space	4
	.section	.bss.GuiVar_FlowCheckingINTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingINTime, %object
	.size	GuiVar_FlowCheckingINTime, 4
GuiVar_FlowCheckingINTime:
	.space	4
	.section	.bss.GuiVar_StationDecoderSerialNumber,"aw",%nobits
	.align	2
	.type	GuiVar_StationDecoderSerialNumber, %object
	.size	GuiVar_StationDecoderSerialNumber, 4
GuiVar_StationDecoderSerialNumber:
	.space	4
	.section	.bss.GuiVar_POCBudgetPercentOfET,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudgetPercentOfET, %object
	.size	GuiVar_POCBudgetPercentOfET, 4
GuiVar_POCBudgetPercentOfET:
	.space	4
	.section	.bss.GuiVar_ManualPStationRunTime,"aw",%nobits
	.type	GuiVar_ManualPStationRunTime, %object
	.size	GuiVar_ManualPStationRunTime, 4
GuiVar_ManualPStationRunTime:
	.space	4
	.section	.bss.GuiVar_RptTestMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptTestMin, %object
	.size	GuiVar_RptTestMin, 4
GuiVar_RptTestMin:
	.space	4
	.section	.bss.GuiVar_AboutMSSEOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutMSSEOption, %object
	.size	GuiVar_AboutMSSEOption, 4
GuiVar_AboutMSSEOption:
	.space	4
	.section	.bss.GuiVar_BudgetAllocationSYS,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetAllocationSYS, %object
	.size	GuiVar_BudgetAllocationSYS, 4
GuiVar_BudgetAllocationSYS:
	.space	4
	.section	.bss.GuiVar_GroupSettingC_0,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_0, %object
	.size	GuiVar_GroupSettingC_0, 4
GuiVar_GroupSettingC_0:
	.space	4
	.section	.bss.GuiVar_FreezeSwitchSelected,"aw",%nobits
	.align	2
	.type	GuiVar_FreezeSwitchSelected, %object
	.size	GuiVar_FreezeSwitchSelected, 4
GuiVar_FreezeSwitchSelected:
	.space	4
	.section	.bss.GuiVar_GroupSettingC_1,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_1, %object
	.size	GuiVar_GroupSettingC_1, 4
GuiVar_GroupSettingC_1:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxDiscConfMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxDiscConfMsgs, %object
	.size	GuiVar_TwoWireStatsRxDiscConfMsgs, 4
GuiVar_TwoWireStatsRxDiscConfMsgs:
	.space	4
	.section	.bss.GuiVar_ENSubnetMask,"aw",%nobits
	.type	GuiVar_ENSubnetMask, %object
	.size	GuiVar_ENSubnetMask, 49
GuiVar_ENSubnetMask:
	.space	49
	.section	.bss.GuiVar_StationInfoDU,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoDU, %object
	.size	GuiVar_StationInfoDU, 4
GuiVar_StationInfoDU:
	.space	4
	.section	.bss.GuiVar_GroupSettingC_5,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_5, %object
	.size	GuiVar_GroupSettingC_5, 4
GuiVar_GroupSettingC_5:
	.space	4
	.section	.bss.GuiVar_FlowCheckingCycles,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingCycles, %object
	.size	GuiVar_FlowCheckingCycles, 4
GuiVar_FlowCheckingCycles:
	.space	4
	.section	.bss.GuiVar_DateTimeTimeZone,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeTimeZone, %object
	.size	GuiVar_DateTimeTimeZone, 4
GuiVar_DateTimeTimeZone:
	.space	4
	.section	.bss.GuiVar_WalkThruCountingDown,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruCountingDown, %object
	.size	GuiVar_WalkThruCountingDown, 4
GuiVar_WalkThruCountingDown:
	.space	4
	.section	.bss.GuiVar_LightIsEnergized,"aw",%nobits
	.align	2
	.type	GuiVar_LightIsEnergized, %object
	.size	GuiVar_LightIsEnergized, 4
GuiVar_LightIsEnergized:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingToleranceMinus1,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingToleranceMinus1, %object
	.size	GuiVar_SystemFlowCheckingToleranceMinus1, 4
GuiVar_SystemFlowCheckingToleranceMinus1:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingToleranceMinus2,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingToleranceMinus2, %object
	.size	GuiVar_SystemFlowCheckingToleranceMinus2, 4
GuiVar_SystemFlowCheckingToleranceMinus2:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingToleranceMinus3,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingToleranceMinus3, %object
	.size	GuiVar_SystemFlowCheckingToleranceMinus3, 4
GuiVar_SystemFlowCheckingToleranceMinus3:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingToleranceMinus4,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingToleranceMinus4, %object
	.size	GuiVar_SystemFlowCheckingToleranceMinus4, 4
GuiVar_SystemFlowCheckingToleranceMinus4:
	.space	4
	.section	.bss.GuiVar_ScheduleIrrigateOn29thOr31st,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleIrrigateOn29thOr31st, %object
	.size	GuiVar_ScheduleIrrigateOn29thOr31st, 4
GuiVar_ScheduleIrrigateOn29thOr31st:
	.space	4
	.section	.bss.GuiVar_AboutTPSta01_08Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta01_08Terminal, %object
	.size	GuiVar_AboutTPSta01_08Terminal, 4
GuiVar_AboutTPSta01_08Terminal:
	.space	4
	.section	.bss.GuiVar_TwoWireStaBIsSet,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaBIsSet, %object
	.size	GuiVar_TwoWireStaBIsSet, 4
GuiVar_TwoWireStaBIsSet:
	.space	4
	.section	.bss.GuiVar_SerialPortOptionA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortOptionA, %object
	.size	GuiVar_SerialPortOptionA, 4
GuiVar_SerialPortOptionA:
	.space	4
	.section	.bss.GuiVar_LightsStopTimeHasBeenEdited,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStopTimeHasBeenEdited, %object
	.size	GuiVar_LightsStopTimeHasBeenEdited, 4
GuiVar_LightsStopTimeHasBeenEdited:
	.space	4
	.section	.bss.GuiVar_SerialPortXmittingA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmittingA, %object
	.size	GuiVar_SerialPortXmittingA, 4
GuiVar_SerialPortXmittingA:
	.space	4
	.section	.bss.GuiVar_WindGageInstalledAt,"aw",%nobits
	.type	GuiVar_WindGageInstalledAt, %object
	.size	GuiVar_WindGageInstalledAt, 3
GuiVar_WindGageInstalledAt:
	.space	3
	.section	.bss.GuiVar_CommOptionDeviceExchangeResult,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionDeviceExchangeResult, %object
	.size	GuiVar_CommOptionDeviceExchangeResult, 4
GuiVar_CommOptionDeviceExchangeResult:
	.space	4
	.section	.bss.GuiVar_BudgetZeroWarning,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetZeroWarning, %object
	.size	GuiVar_BudgetZeroWarning, 4
GuiVar_BudgetZeroWarning:
	.space	4
	.section	.bss.GuiVar_LightCalendarWeek2Tue,"aw",%nobits
	.type	GuiVar_LightCalendarWeek2Tue, %object
	.size	GuiVar_LightCalendarWeek2Tue, 3
GuiVar_LightCalendarWeek2Tue:
	.space	3
	.section	.bss.GuiVar_LightIrriListStop,"aw",%nobits
	.type	GuiVar_LightIrriListStop, %object
	.size	GuiVar_LightIrriListStop, 33
GuiVar_LightIrriListStop:
	.space	33
	.section	.bss.GuiVar_CommTestShowDebug,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestShowDebug, %object
	.size	GuiVar_CommTestShowDebug, 4
GuiVar_CommTestShowDebug:
	.space	4
	.section	.bss.GuiVar_AccessControlLoggedIn,"aw",%nobits
	.align	2
	.type	GuiVar_AccessControlLoggedIn, %object
	.size	GuiVar_AccessControlLoggedIn, 4
GuiVar_AccessControlLoggedIn:
	.space	4
	.section	.bss.GuiVar_RptStartTime,"aw",%nobits
	.type	GuiVar_RptStartTime, %object
	.size	GuiVar_RptStartTime, 8
GuiVar_RptStartTime:
	.space	8
	.section	.bss.GuiVar_MainMenuLightsOptionsExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuLightsOptionsExist, %object
	.size	GuiVar_MainMenuLightsOptionsExist, 4
GuiVar_MainMenuLightsOptionsExist:
	.space	4
	.section	.bss.GuiVar_ETGageLogPulses,"aw",%nobits
	.align	2
	.type	GuiVar_ETGageLogPulses, %object
	.size	GuiVar_ETGageLogPulses, 4
GuiVar_ETGageLogPulses:
	.space	4
	.section	.bss.GuiVar_AboutAntennaHoles,"aw",%nobits
	.align	2
	.type	GuiVar_AboutAntennaHoles, %object
	.size	GuiVar_AboutAntennaHoles, 4
GuiVar_AboutAntennaHoles:
	.space	4
	.section	.bss.GuiVar_SerialPortCDA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortCDA, %object
	.size	GuiVar_SerialPortCDA, 4
GuiVar_SerialPortCDA:
	.space	4
	.section	.bss.GuiVar_itmGroupName,"aw",%nobits
	.type	GuiVar_itmGroupName, %object
	.size	GuiVar_itmGroupName, 49
GuiVar_itmGroupName:
	.space	49
	.section	.bss.GuiVar_StationInfoControllerName,"aw",%nobits
	.type	GuiVar_StationInfoControllerName, %object
	.size	GuiVar_StationInfoControllerName, 49
GuiVar_StationInfoControllerName:
	.space	49
	.section	.bss.GuiVar_ENDHCPNameNotSet,"aw",%nobits
	.align	2
	.type	GuiVar_ENDHCPNameNotSet, %object
	.size	GuiVar_ENDHCPNameNotSet, 4
GuiVar_ENDHCPNameNotSet:
	.space	4
	.section	.bss.GuiVar_ENFirmwareVer,"aw",%nobits
	.type	GuiVar_ENFirmwareVer, %object
	.size	GuiVar_ENFirmwareVer, 49
GuiVar_ENFirmwareVer:
	.space	49
	.section	.bss.GuiVar_StationInfoFlowStatus,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoFlowStatus, %object
	.size	GuiVar_StationInfoFlowStatus, 4
GuiVar_StationInfoFlowStatus:
	.space	4
	.section	.bss.GuiVar_ManualPStationRunTimeStationNumber,"aw",%nobits
	.type	GuiVar_ManualPStationRunTimeStationNumber, %object
	.size	GuiVar_ManualPStationRunTimeStationNumber, 4
GuiVar_ManualPStationRunTimeStationNumber:
	.space	4
	.section	.bss.GuiVar_ManualPStartTime6Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime6Enabled, %object
	.size	GuiVar_ManualPStartTime6Enabled, 4
GuiVar_ManualPStartTime6Enabled:
	.space	4
	.section	.bss.GuiVar_ENGateway_1,"aw",%nobits
	.align	2
	.type	GuiVar_ENGateway_1, %object
	.size	GuiVar_ENGateway_1, 4
GuiVar_ENGateway_1:
	.space	4
	.section	.bss.GuiVar_ENGateway_2,"aw",%nobits
	.align	2
	.type	GuiVar_ENGateway_2, %object
	.size	GuiVar_ENGateway_2, 4
GuiVar_ENGateway_2:
	.space	4
	.section	.bss.GuiVar_ENGateway_3,"aw",%nobits
	.align	2
	.type	GuiVar_ENGateway_3, %object
	.size	GuiVar_ENGateway_3, 4
GuiVar_ENGateway_3:
	.space	4
	.section	.bss.GuiVar_FlowCheckingFlag1,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag1, %object
	.size	GuiVar_FlowCheckingFlag1, 4
GuiVar_FlowCheckingFlag1:
	.space	4
	.section	.bss.GuiVar_StationInfoSquareFootage,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoSquareFootage, %object
	.size	GuiVar_StationInfoSquareFootage, 4
GuiVar_StationInfoSquareFootage:
	.space	4
	.section	.bss.GuiVar_FlowCheckingFlag2,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag2, %object
	.size	GuiVar_FlowCheckingFlag2, 4
GuiVar_FlowCheckingFlag2:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxIDReqMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxIDReqMsgs, %object
	.size	GuiVar_TwoWireStatsRxIDReqMsgs, 4
GuiVar_TwoWireStatsRxIDReqMsgs:
	.space	4
	.section	.bss.GuiVar_FlowCheckingFlag3,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag3, %object
	.size	GuiVar_FlowCheckingFlag3, 4
GuiVar_FlowCheckingFlag3:
	.space	4
	.section	.bss.GuiVar_MoisSensorName,"aw",%nobits
	.type	GuiVar_MoisSensorName, %object
	.size	GuiVar_MoisSensorName, 49
GuiVar_MoisSensorName:
	.space	49
	.section	.bss.GuiVar_WENChangeKey,"aw",%nobits
	.align	2
	.type	GuiVar_WENChangeKey, %object
	.size	GuiVar_WENChangeKey, 4
GuiVar_WENChangeKey:
	.space	4
	.section	.bss.GuiVar_RptNonCGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptNonCGal, %object
	.size	GuiVar_RptNonCGal, 8
GuiVar_RptNonCGal:
	.space	8
	.section	.bss.GuiVar_FlowCheckingFlag5,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag5, %object
	.size	GuiVar_FlowCheckingFlag5, 4
GuiVar_FlowCheckingFlag5:
	.space	4
	.section	.bss.GuiVar_CommOptionPortAInstalled,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionPortAInstalled, %object
	.size	GuiVar_CommOptionPortAInstalled, 4
GuiVar_CommOptionPortAInstalled:
	.space	4
	.section	.bss.GuiVar_FlowCheckingFlag6,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag6, %object
	.size	GuiVar_FlowCheckingFlag6, 4
GuiVar_FlowCheckingFlag6:
	.space	4
	.section	.bss.GuiVar_RadioTestBadCRC,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestBadCRC, %object
	.size	GuiVar_RadioTestBadCRC, 4
GuiVar_RadioTestBadCRC:
	.space	4
	.section	.bss.GuiVar_AboutTPSta01_08Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta01_08Card, %object
	.size	GuiVar_AboutTPSta01_08Card, 4
GuiVar_AboutTPSta01_08Card:
	.space	4
	.section	.bss.GuiVar_RptManualGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptManualGal, %object
	.size	GuiVar_RptManualGal, 8
GuiVar_RptManualGal:
	.space	8
	.section	.bss.GuiVar_FlowTable7Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable7Sta, %object
	.size	GuiVar_FlowTable7Sta, 4
GuiVar_FlowTable7Sta:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsS2UCos,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsS2UCos, %object
	.size	GuiVar_TwoWireStatsS2UCos, 4
GuiVar_TwoWireStatsS2UCos:
	.space	4
	.section	.bss.GuiVar_ContactStatusSerialNumber,"aw",%nobits
	.align	2
	.type	GuiVar_ContactStatusSerialNumber, %object
	.size	GuiVar_ContactStatusSerialNumber, 4
GuiVar_ContactStatusSerialNumber:
	.space	4
	.section	.bss.GuiVar_WENChangePassword,"aw",%nobits
	.align	2
	.type	GuiVar_WENChangePassword, %object
	.size	GuiVar_WENChangePassword, 4
GuiVar_WENChangePassword:
	.space	4
	.section	.bss.GuiVar_WalkThruRunTime,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruRunTime, %object
	.size	GuiVar_WalkThruRunTime, 4
GuiVar_WalkThruRunTime:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxPutParamsMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxPutParamsMsgs, %object
	.size	GuiVar_TwoWireStatsRxPutParamsMsgs, 4
GuiVar_TwoWireStatsRxPutParamsMsgs:
	.space	4
	.section	.bss.GuiVar_VolumeSliderPos,"aw",%nobits
	.align	2
	.type	GuiVar_VolumeSliderPos, %object
	.size	GuiVar_VolumeSliderPos, 4
GuiVar_VolumeSliderPos:
	.space	4
	.section	.bss.GuiVar_ETRainTableRainStatus,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableRainStatus, %object
	.size	GuiVar_ETRainTableRainStatus, 4
GuiVar_ETRainTableRainStatus:
	.space	4
	.section	.bss.GuiVar_ENObtainIPAutomatically,"aw",%nobits
	.align	2
	.type	GuiVar_ENObtainIPAutomatically, %object
	.size	GuiVar_ENObtainIPAutomatically, 4
GuiVar_ENObtainIPAutomatically:
	.space	4
	.section	.bss.GuiVar_WENWEPAuthentication,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPAuthentication, %object
	.size	GuiVar_WENWEPAuthentication, 4
GuiVar_WENWEPAuthentication:
	.space	4
	.section	.bss.GuiVar_MVORState,"aw",%nobits
	.align	2
	.type	GuiVar_MVORState, %object
	.size	GuiVar_MVORState, 4
GuiVar_MVORState:
	.space	4
	.section	.bss.GuiVar_StationInfoShowEstMin,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoShowEstMin, %object
	.size	GuiVar_StationInfoShowEstMin, 4
GuiVar_StationInfoShowEstMin:
	.space	4
	.section	.bss.GuiVar_ChainStatsIRRIIrriTRcvd,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsIRRIIrriTRcvd, %object
	.size	GuiVar_ChainStatsIRRIIrriTRcvd, 4
GuiVar_ChainStatsIRRIIrriTRcvd:
	.space	4
	.section	.bss.GuiVar_DateTimeDayOfWeek,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeDayOfWeek, %object
	.size	GuiVar_DateTimeDayOfWeek, 4
GuiVar_DateTimeDayOfWeek:
	.space	4
	.section	.bss.GuiVar_RainBucketMaximumHourly,"aw",%nobits
	.align	2
	.type	GuiVar_RainBucketMaximumHourly, %object
	.size	GuiVar_RainBucketMaximumHourly, 4
GuiVar_RainBucketMaximumHourly:
	.space	4
	.section	.bss.GuiVar_ETRainTableCurrentET,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableCurrentET, %object
	.size	GuiVar_ETRainTableCurrentET, 4
GuiVar_ETRainTableCurrentET:
	.space	4
	.section	.bss.GuiVar_HistoricalETUserState,"aw",%nobits
	.type	GuiVar_HistoricalETUserState, %object
	.size	GuiVar_HistoricalETUserState, 25
GuiVar_HistoricalETUserState:
	.space	25
	.section	.bss.GuiVar_StationInfoCycleTime,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoCycleTime, %object
	.size	GuiVar_StationInfoCycleTime, 4
GuiVar_StationInfoCycleTime:
	.space	4
	.section	.bss.GuiVar_ChainStatsNextContactSerNum,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsNextContactSerNum, %object
	.size	GuiVar_ChainStatsNextContactSerNum, 4
GuiVar_ChainStatsNextContactSerNum:
	.space	4
	.section	.bss.GuiVar_LRSlaveLinksToRepeater,"aw",%nobits
	.align	2
	.type	GuiVar_LRSlaveLinksToRepeater, %object
	.size	GuiVar_LRSlaveLinksToRepeater, 4
GuiVar_LRSlaveLinksToRepeater:
	.space	4
	.section	.bss.GuiVar_DateTimeAMPM,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeAMPM, %object
	.size	GuiVar_DateTimeAMPM, 4
GuiVar_DateTimeAMPM:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxStatsReqMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxStatsReqMsgs, %object
	.size	GuiVar_TwoWireStatsRxStatsReqMsgs, 4
GuiVar_TwoWireStatsRxStatsReqMsgs:
	.space	4
	.section	.bss.GuiVar_RptManualMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptManualMin, %object
	.size	GuiVar_RptManualMin, 4
GuiVar_RptManualMin:
	.space	4
	.section	.bss.GuiVar_ScheduleUseETAveraging,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleUseETAveraging, %object
	.size	GuiVar_ScheduleUseETAveraging, 4
GuiVar_ScheduleUseETAveraging:
	.space	4
	.section	.bss.GuiVar_BudgetSysHasPOC,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetSysHasPOC, %object
	.size	GuiVar_BudgetSysHasPOC, 4
GuiVar_BudgetSysHasPOC:
	.space	4
	.section	.bss.GuiVar_GroupSetting_Visible_0,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_0, %object
	.size	GuiVar_GroupSetting_Visible_0, 4
GuiVar_GroupSetting_Visible_0:
	.space	4
	.section	.bss.GuiVar_LiveScreensBypassFlowFlow,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowFlow, %object
	.size	GuiVar_LiveScreensBypassFlowFlow, 4
GuiVar_LiveScreensBypassFlowFlow:
	.space	4
	.section	.bss.GuiVar_GroupSetting_Visible_1,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_1, %object
	.size	GuiVar_GroupSetting_Visible_1, 4
GuiVar_GroupSetting_Visible_1:
	.space	4
	.section	.bss.GuiVar_GroupSetting_Visible_2,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_2, %object
	.size	GuiVar_GroupSetting_Visible_2, 4
GuiVar_GroupSetting_Visible_2:
	.space	4
	.section	.bss.GuiVar_ManualPInUse,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPInUse, %object
	.size	GuiVar_ManualPInUse, 4
GuiVar_ManualPInUse:
	.space	4
	.section	.bss.GuiVar_LiveScreensBypassFlowMVOpen,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowMVOpen, %object
	.size	GuiVar_LiveScreensBypassFlowMVOpen, 4
GuiVar_LiveScreensBypassFlowMVOpen:
	.space	4
	.section	.bss.GuiVar_StationGroupKc5,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc5, %object
	.size	GuiVar_StationGroupKc5, 4
GuiVar_StationGroupKc5:
	.space	4
	.section	.bss.GuiVar_GroupSetting_Visible_4,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_4, %object
	.size	GuiVar_GroupSetting_Visible_4, 4
GuiVar_GroupSetting_Visible_4:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseH,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseH, %object
	.size	GuiVar_ChainStatsInUseH, 4
GuiVar_ChainStatsInUseH:
	.space	4
	.section	.bss.GuiVar_GroupSetting_Visible_5,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_5, %object
	.size	GuiVar_GroupSetting_Visible_5, 4
GuiVar_GroupSetting_Visible_5:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseI,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseI, %object
	.size	GuiVar_ChainStatsInUseI, 4
GuiVar_ChainStatsInUseI:
	.space	4
	.section	.bss.GuiVar_GroupSetting_Visible_6,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_6, %object
	.size	GuiVar_GroupSetting_Visible_6, 4
GuiVar_GroupSetting_Visible_6:
	.space	4
	.section	.bss.GuiVar_StationGroupKc8,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc8, %object
	.size	GuiVar_StationGroupKc8, 4
GuiVar_StationGroupKc8:
	.space	4
	.section	.bss.GuiVar_GroupSetting_Visible_7,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_7, %object
	.size	GuiVar_GroupSetting_Visible_7, 4
GuiVar_GroupSetting_Visible_7:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseK,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseK, %object
	.size	GuiVar_ChainStatsInUseK, 4
GuiVar_ChainStatsInUseK:
	.space	4
	.section	.bss.GuiVar_GroupSetting_Visible_8,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_8, %object
	.size	GuiVar_GroupSetting_Visible_8, 4
GuiVar_GroupSetting_Visible_8:
	.space	4
	.section	.bss.GuiVar_POCPreservesDeliveredPumpCurrent1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredPumpCurrent1, %object
	.size	GuiVar_POCPreservesDeliveredPumpCurrent1, 4
GuiVar_POCPreservesDeliveredPumpCurrent1:
	.space	4
	.section	.bss.GuiVar_POCPreservesDeliveredPumpCurrent2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredPumpCurrent2, %object
	.size	GuiVar_POCPreservesDeliveredPumpCurrent2, 4
GuiVar_POCPreservesDeliveredPumpCurrent2:
	.space	4
	.section	.bss.GuiVar_POCPreservesDeliveredPumpCurrent3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredPumpCurrent3, %object
	.size	GuiVar_POCPreservesDeliveredPumpCurrent3, 4
GuiVar_POCPreservesDeliveredPumpCurrent3:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingTolerancePlus1,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingTolerancePlus1, %object
	.size	GuiVar_SystemFlowCheckingTolerancePlus1, 4
GuiVar_SystemFlowCheckingTolerancePlus1:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingTolerancePlus2,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingTolerancePlus2, %object
	.size	GuiVar_SystemFlowCheckingTolerancePlus2, 4
GuiVar_SystemFlowCheckingTolerancePlus2:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingTolerancePlus3,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingTolerancePlus3, %object
	.size	GuiVar_SystemFlowCheckingTolerancePlus3, 4
GuiVar_SystemFlowCheckingTolerancePlus3:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingTolerancePlus4,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingTolerancePlus4, %object
	.size	GuiVar_SystemFlowCheckingTolerancePlus4, 4
GuiVar_SystemFlowCheckingTolerancePlus4:
	.space	4
	.section	.bss.GuiVar_LightCalendarWeek1Thu,"aw",%nobits
	.type	GuiVar_LightCalendarWeek1Thu, %object
	.size	GuiVar_LightCalendarWeek1Thu, 3
GuiVar_LightCalendarWeek1Thu:
	.space	3
	.section	.bss.GuiVar_CommOptionPortBInstalled,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionPortBInstalled, %object
	.size	GuiVar_CommOptionPortBInstalled, 4
GuiVar_CommOptionPortBInstalled:
	.space	4
	.section	.bss.GuiVar_WENMACAddress,"aw",%nobits
	.type	GuiVar_WENMACAddress, %object
	.size	GuiVar_WENMACAddress, 49
GuiVar_WENMACAddress:
	.space	49
	.section	.bss.GuiVar_StatusFreezeSwitchState,"aw",%nobits
	.align	2
	.type	GuiVar_StatusFreezeSwitchState, %object
	.size	GuiVar_StatusFreezeSwitchState, 4
GuiVar_StatusFreezeSwitchState:
	.space	4
	.section	.bss.GuiVar_AboutModelNumber,"aw",%nobits
	.type	GuiVar_AboutModelNumber, %object
	.size	GuiVar_AboutModelNumber, 49
GuiVar_AboutModelNumber:
	.space	49
	.section	.bss.GuiVar_LightCalendarWeek2Wed,"aw",%nobits
	.type	GuiVar_LightCalendarWeek2Wed, %object
	.size	GuiVar_LightCalendarWeek2Wed, 3
GuiVar_LightCalendarWeek2Wed:
	.space	3
	.section	.bss.GuiVar_LiveScreensCommFLInForced,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommFLInForced, %object
	.size	GuiVar_LiveScreensCommFLInForced, 4
GuiVar_LiveScreensCommFLInForced:
	.space	4
	.section	.bss.GuiVar_AccessControlUserRole,"aw",%nobits
	.type	GuiVar_AccessControlUserRole, %object
	.size	GuiVar_AccessControlUserRole, 49
GuiVar_AccessControlUserRole:
	.space	49
	.section	.bss.GuiVar_LightCalendarWeek2Fri,"aw",%nobits
	.type	GuiVar_LightCalendarWeek2Fri, %object
	.size	GuiVar_LightCalendarWeek2Fri, 3
GuiVar_LightCalendarWeek2Fri:
	.space	3
	.section	.bss.GuiVar_StationDecoderOutput,"aw",%nobits
	.align	2
	.type	GuiVar_StationDecoderOutput, %object
	.size	GuiVar_StationDecoderOutput, 4
GuiVar_StationDecoderOutput:
	.space	4
	.section	.bss.GuiVar_AboutTPSta17_24Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta17_24Terminal, %object
	.size	GuiVar_AboutTPSta17_24Terminal, 4
GuiVar_AboutTPSta17_24Terminal:
	.space	4
	.section	.bss.GuiVar_ManualPWaterDay_Mon,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Mon, %object
	.size	GuiVar_ManualPWaterDay_Mon, 4
GuiVar_ManualPWaterDay_Mon:
	.space	4
	.section	.bss.GuiVar_BudgetPeriodIdx,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetPeriodIdx, %object
	.size	GuiVar_BudgetPeriodIdx, 4
GuiVar_BudgetPeriodIdx:
	.space	4
	.section	.bss.GuiVar_POCBudget1,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget1, %object
	.size	GuiVar_POCBudget1, 4
GuiVar_POCBudget1:
	.space	4
	.section	.bss.GuiVar_POCBudget2,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget2, %object
	.size	GuiVar_POCBudget2, 4
GuiVar_POCBudget2:
	.space	4
	.section	.bss.GuiVar_RadioTestNoResp,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestNoResp, %object
	.size	GuiVar_RadioTestNoResp, 4
GuiVar_RadioTestNoResp:
	.space	4
	.section	.bss.GuiVar_POCBudget4,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget4, %object
	.size	GuiVar_POCBudget4, 4
GuiVar_POCBudget4:
	.space	4
	.section	.bss.GuiVar_POCBudget5,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget5, %object
	.size	GuiVar_POCBudget5, 4
GuiVar_POCBudget5:
	.space	4
	.section	.bss.GuiVar_POCBudget6,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget6, %object
	.size	GuiVar_POCBudget6, 4
GuiVar_POCBudget6:
	.space	4
	.section	.bss.GuiVar_POCBudget7,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget7, %object
	.size	GuiVar_POCBudget7, 4
GuiVar_POCBudget7:
	.space	4
	.section	.bss.GuiVar_POCBudget8,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget8, %object
	.size	GuiVar_POCBudget8, 4
GuiVar_POCBudget8:
	.space	4
	.section	.bss.GuiVar_POCBudget9,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget9, %object
	.size	GuiVar_POCBudget9, 4
GuiVar_POCBudget9:
	.space	4
	.section	.bss.GuiVar_GRCarrier,"aw",%nobits
	.type	GuiVar_GRCarrier, %object
	.size	GuiVar_GRCarrier, 49
GuiVar_GRCarrier:
	.space	49
	.section	.bss.GuiVar_ChainStatsFOALIrriRRcvd,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsFOALIrriRRcvd, %object
	.size	GuiVar_ChainStatsFOALIrriRRcvd, 4
GuiVar_ChainStatsFOALIrriRRcvd:
	.space	4
	.section	.bss.GuiVar_IrriCommChainConfigCurrentFromFOAL,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigCurrentFromFOAL, %object
	.size	GuiVar_IrriCommChainConfigCurrentFromFOAL, 4
GuiVar_IrriCommChainConfigCurrentFromFOAL:
	.space	4
	.section	.bss.GuiVar_RadioTestCS3000SN,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestCS3000SN, %object
	.size	GuiVar_RadioTestCS3000SN, 4
GuiVar_RadioTestCS3000SN:
	.space	4
	.section	.bss.GuiVar_LightsStartTime1InMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStartTime1InMinutes, %object
	.size	GuiVar_LightsStartTime1InMinutes, 4
GuiVar_LightsStartTime1InMinutes:
	.space	4
	.section	.bss.GuiVar_LiveScreensBypassFlowMaxFlow,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowMaxFlow, %object
	.size	GuiVar_LiveScreensBypassFlowMaxFlow, 4
GuiVar_LiveScreensBypassFlowMaxFlow:
	.space	4
	.section	.bss.GuiVar_PercentAdjustEndDate_1,"aw",%nobits
	.type	GuiVar_PercentAdjustEndDate_1, %object
	.size	GuiVar_PercentAdjustEndDate_1, 49
GuiVar_PercentAdjustEndDate_1:
	.space	49
	.section	.bss.GuiVar_PercentAdjustEndDate_2,"aw",%nobits
	.type	GuiVar_PercentAdjustEndDate_2, %object
	.size	GuiVar_PercentAdjustEndDate_2, 49
GuiVar_PercentAdjustEndDate_2:
	.space	49
	.section	.bss.GuiVar_ScheduleWaterDay_Sun,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Sun, %object
	.size	GuiVar_ScheduleWaterDay_Sun, 4
GuiVar_ScheduleWaterDay_Sun:
	.space	4
	.section	.bss.GuiVar_PercentAdjustEndDate_4,"aw",%nobits
	.type	GuiVar_PercentAdjustEndDate_4, %object
	.size	GuiVar_PercentAdjustEndDate_4, 49
GuiVar_PercentAdjustEndDate_4:
	.space	49
	.section	.bss.GuiVar_PercentAdjustEndDate_5,"aw",%nobits
	.type	GuiVar_PercentAdjustEndDate_5, %object
	.size	GuiVar_PercentAdjustEndDate_5, 49
GuiVar_PercentAdjustEndDate_5:
	.space	49
	.section	.bss.GuiVar_PercentAdjustEndDate_6,"aw",%nobits
	.type	GuiVar_PercentAdjustEndDate_6, %object
	.size	GuiVar_PercentAdjustEndDate_6, 49
GuiVar_PercentAdjustEndDate_6:
	.space	49
	.section	.bss.GuiVar_PercentAdjustEndDate_7,"aw",%nobits
	.type	GuiVar_PercentAdjustEndDate_7, %object
	.size	GuiVar_PercentAdjustEndDate_7, 49
GuiVar_PercentAdjustEndDate_7:
	.space	49
	.section	.bss.GuiVar_TwoWireStatsRxSolCurMeasReqMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxSolCurMeasReqMsgs, %object
	.size	GuiVar_TwoWireStatsRxSolCurMeasReqMsgs, 4
GuiVar_TwoWireStatsRxSolCurMeasReqMsgs:
	.space	4
	.section	.bss.GuiVar_PercentAdjustEndDate_9,"aw",%nobits
	.type	GuiVar_PercentAdjustEndDate_9, %object
	.size	GuiVar_PercentAdjustEndDate_9, 49
GuiVar_PercentAdjustEndDate_9:
	.space	49
	.section	.bss.GuiVar_MVOROpen6Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen6Enabled, %object
	.size	GuiVar_MVOROpen6Enabled, 4
GuiVar_MVOROpen6Enabled:
	.space	4
	.section	.bss.GuiVar_ChainStatsCommMngrMode,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsCommMngrMode, %object
	.size	GuiVar_ChainStatsCommMngrMode, 4
GuiVar_ChainStatsCommMngrMode:
	.space	4
	.section	.bss.GuiVar_DateTimeFullStr,"aw",%nobits
	.type	GuiVar_DateTimeFullStr, %object
	.size	GuiVar_DateTimeFullStr, 65
GuiVar_DateTimeFullStr:
	.space	65
	.section	.bss.GuiVar_ENGateway_4,"aw",%nobits
	.align	2
	.type	GuiVar_ENGateway_4, %object
	.size	GuiVar_ENGateway_4, 4
GuiVar_ENGateway_4:
	.space	4
	.section	.bss.GuiVar_StatusWindGageControllerName,"aw",%nobits
	.type	GuiVar_StatusWindGageControllerName, %object
	.size	GuiVar_StatusWindGageControllerName, 49
GuiVar_StatusWindGageControllerName:
	.space	49
	.section	.bss.GuiVar_LightsStartTimeEnabled2,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStartTimeEnabled2, %object
	.size	GuiVar_LightsStartTimeEnabled2, 4
GuiVar_LightsStartTimeEnabled2:
	.space	4
	.section	.bss.GuiVar_RadioTestET2000eAddr0,"aw",%nobits
	.type	GuiVar_RadioTestET2000eAddr0, %object
	.size	GuiVar_RadioTestET2000eAddr0, 2
GuiVar_RadioTestET2000eAddr0:
	.space	2
	.section	.bss.GuiVar_RadioTestET2000eAddr1,"aw",%nobits
	.type	GuiVar_RadioTestET2000eAddr1, %object
	.size	GuiVar_RadioTestET2000eAddr1, 2
GuiVar_RadioTestET2000eAddr1:
	.space	2
	.section	.bss.GuiVar_RadioTestET2000eAddr2,"aw",%nobits
	.type	GuiVar_RadioTestET2000eAddr2, %object
	.size	GuiVar_RadioTestET2000eAddr2, 2
GuiVar_RadioTestET2000eAddr2:
	.space	2
	.section	.bss.GuiVar_IrriCommChainConfigSerialNum,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigSerialNum, %object
	.size	GuiVar_IrriCommChainConfigSerialNum, 4
GuiVar_IrriCommChainConfigSerialNum:
	.space	4
	.section	.bss.GuiVar_BudgetReductionLimit_0,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_0, %object
	.size	GuiVar_BudgetReductionLimit_0, 4
GuiVar_BudgetReductionLimit_0:
	.space	4
	.section	.bss.GuiVar_WENRadioType,"aw",%nobits
	.align	2
	.type	GuiVar_WENRadioType, %object
	.size	GuiVar_WENRadioType, 4
GuiVar_WENRadioType:
	.space	4
	.section	.bss.GuiVar_CommTestIPOctect2,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestIPOctect2, %object
	.size	GuiVar_CommTestIPOctect2, 4
GuiVar_CommTestIPOctect2:
	.space	4
	.section	.bss.GuiVar_ContactStatusPort,"aw",%nobits
	.align	2
	.type	GuiVar_ContactStatusPort, %object
	.size	GuiVar_ContactStatusPort, 4
GuiVar_ContactStatusPort:
	.space	4
	.section	.bss.GuiVar_RainSwitchInUse,"aw",%nobits
	.align	2
	.type	GuiVar_RainSwitchInUse, %object
	.size	GuiVar_RainSwitchInUse, 4
GuiVar_RainSwitchInUse:
	.space	4
	.section	.bss.GuiVar_ManualPStartTime3Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime3Enabled, %object
	.size	GuiVar_ManualPStartTime3Enabled, 4
GuiVar_ManualPStartTime3Enabled:
	.space	4
	.section	.bss.GuiVar_ManualPWaterDay_Sat,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Sat, %object
	.size	GuiVar_ManualPWaterDay_Sat, 4
GuiVar_ManualPWaterDay_Sat:
	.space	4
	.section	.bss.GuiVar_IrriDetailsCurrentDraw,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsCurrentDraw, %object
	.size	GuiVar_IrriDetailsCurrentDraw, 4
GuiVar_IrriDetailsCurrentDraw:
	.space	4
	.section	.bss.GuiVar_BudgetReductionLimit_3,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_3, %object
	.size	GuiVar_BudgetReductionLimit_3, 4
GuiVar_BudgetReductionLimit_3:
	.space	4
	.section	.bss.GuiVar_StatusSystemFlowMLBMeasured,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowMLBMeasured, %object
	.size	GuiVar_StatusSystemFlowMLBMeasured, 4
GuiVar_StatusSystemFlowMLBMeasured:
	.space	4
	.section	.bss.GuiVar_HistoricalET10,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET10, %object
	.size	GuiVar_HistoricalET10, 4
GuiVar_HistoricalET10:
	.space	4
	.section	.bss.GuiVar_TwoWireOrphanedPOCsExist,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireOrphanedPOCsExist, %object
	.size	GuiVar_TwoWireOrphanedPOCsExist, 4
GuiVar_TwoWireOrphanedPOCsExist:
	.space	4
	.section	.bss.GuiVar_GroupSettingA_0,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_0, %object
	.size	GuiVar_GroupSettingA_0, 4
GuiVar_GroupSettingA_0:
	.space	4
	.section	.bss.GuiVar_GroupSettingA_1,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_1, %object
	.size	GuiVar_GroupSettingA_1, 4
GuiVar_GroupSettingA_1:
	.space	4
	.section	.bss.GuiVar_GroupSettingA_2,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_2, %object
	.size	GuiVar_GroupSettingA_2, 4
GuiVar_GroupSettingA_2:
	.space	4
	.section	.bss.GuiVar_GRGPRSStatus,"aw",%nobits
	.type	GuiVar_GRGPRSStatus, %object
	.size	GuiVar_GRGPRSStatus, 49
GuiVar_GRGPRSStatus:
	.space	49
	.section	.bss.GuiVar_GroupSettingA_4,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_4, %object
	.size	GuiVar_GroupSettingA_4, 4
GuiVar_GroupSettingA_4:
	.space	4
	.section	.bss.GuiVar_GroupSettingA_5,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_5, %object
	.size	GuiVar_GroupSettingA_5, 4
GuiVar_GroupSettingA_5:
	.space	4
	.section	.bss.GuiVar_GroupSettingA_6,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_6, %object
	.size	GuiVar_GroupSettingA_6, 4
GuiVar_GroupSettingA_6:
	.space	4
	.section	.bss.GuiVar_GroupSettingA_7,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_7, %object
	.size	GuiVar_GroupSettingA_7, 4
GuiVar_GroupSettingA_7:
	.space	4
	.section	.bss.GuiVar_GroupSettingA_8,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_8, %object
	.size	GuiVar_GroupSettingA_8, 4
GuiVar_GroupSettingA_8:
	.space	4
	.section	.bss.GuiVar_StatusMLBInEffect,"aw",%nobits
	.align	2
	.type	GuiVar_StatusMLBInEffect, %object
	.size	GuiVar_StatusMLBInEffect, 4
GuiVar_StatusMLBInEffect:
	.space	4
	.section	.bss.GuiVar_GRRadioSerialNum,"aw",%nobits
	.type	GuiVar_GRRadioSerialNum, %object
	.size	GuiVar_GRRadioSerialNum, 49
GuiVar_GRRadioSerialNum:
	.space	49
	.section	.bss.GuiVar_LiveScreensPOCFlowName,"aw",%nobits
	.type	GuiVar_LiveScreensPOCFlowName, %object
	.size	GuiVar_LiveScreensPOCFlowName, 49
GuiVar_LiveScreensPOCFlowName:
	.space	49
	.section	.bss.GuiVar_BudgetUsedSYS,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetUsedSYS, %object
	.size	GuiVar_BudgetUsedSYS, 4
GuiVar_BudgetUsedSYS:
	.space	4
	.section	.bss.GuiVar_StatusETGageControllerName,"aw",%nobits
	.type	GuiVar_StatusETGageControllerName, %object
	.size	GuiVar_StatusETGageControllerName, 49
GuiVar_StatusETGageControllerName:
	.space	49
	.section	.bss.GuiVar_ShowDivider,"aw",%nobits
	.align	2
	.type	GuiVar_ShowDivider, %object
	.size	GuiVar_ShowDivider, 4
GuiVar_ShowDivider:
	.space	4
	.section	.bss.GuiVar_NumericKeypadShowPlusMinus,"aw",%nobits
	.align	2
	.type	GuiVar_NumericKeypadShowPlusMinus, %object
	.size	GuiVar_NumericKeypadShowPlusMinus, 4
GuiVar_NumericKeypadShowPlusMinus:
	.space	4
	.section	.bss.GuiVar_POCDecoderSN1,"aw",%nobits
	.align	2
	.type	GuiVar_POCDecoderSN1, %object
	.size	GuiVar_POCDecoderSN1, 4
GuiVar_POCDecoderSN1:
	.space	4
	.section	.bss.GuiVar_MoisECAction_Low,"aw",%nobits
	.align	2
	.type	GuiVar_MoisECAction_Low, %object
	.size	GuiVar_MoisECAction_Low, 4
GuiVar_MoisECAction_Low:
	.space	4
	.section	.bss.GuiVar_IrriDetailsRunSoak,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsRunSoak, %object
	.size	GuiVar_IrriDetailsRunSoak, 4
GuiVar_IrriDetailsRunSoak:
	.space	4
	.section	.bss.GuiVar_LightRemainingMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_LightRemainingMinutes, %object
	.size	GuiVar_LightRemainingMinutes, 4
GuiVar_LightRemainingMinutes:
	.space	4
	.section	.bss.GuiVar_StatusSystemFlowMLBAllowed,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowMLBAllowed, %object
	.size	GuiVar_StatusSystemFlowMLBAllowed, 4
GuiVar_StatusSystemFlowMLBAllowed:
	.space	4
	.section	.bss.GuiVar_StatusFlowErrors,"aw",%nobits
	.align	2
	.type	GuiVar_StatusFlowErrors, %object
	.size	GuiVar_StatusFlowErrors, 4
GuiVar_StatusFlowErrors:
	.space	4
	.section	.bss.GuiVar_AboutSerialNumber,"aw",%nobits
	.align	2
	.type	GuiVar_AboutSerialNumber, %object
	.size	GuiVar_AboutSerialNumber, 4
GuiVar_AboutSerialNumber:
	.space	4
	.section	.bss.GuiVar_FinishTimesStartTime,"aw",%nobits
	.type	GuiVar_FinishTimesStartTime, %object
	.size	GuiVar_FinishTimesStartTime, 65
GuiVar_FinishTimesStartTime:
	.space	65
	.section	.bss.GuiVar_MVOROpen5Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen5Enabled, %object
	.size	GuiVar_MVOROpen5Enabled, 4
GuiVar_MVOROpen5Enabled:
	.space	4
	.section	.bss.GuiVar_AboutTPSta25_32Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta25_32Card, %object
	.size	GuiVar_AboutTPSta25_32Card, 4
GuiVar_AboutTPSta25_32Card:
	.space	4
	.section	.bss.GuiVar_BudgetPeriodStart,"aw",%nobits
	.type	GuiVar_BudgetPeriodStart, %object
	.size	GuiVar_BudgetPeriodStart, 41
GuiVar_BudgetPeriodStart:
	.space	41
	.section	.bss.GuiVar_TwoWireNumDiscoveredMoisDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireNumDiscoveredMoisDecoders, %object
	.size	GuiVar_TwoWireNumDiscoveredMoisDecoders, 4
GuiVar_TwoWireNumDiscoveredMoisDecoders:
	.space	4
	.section	.bss.GuiVar_FlowTable2Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable2Sta, %object
	.size	GuiVar_FlowTable2Sta, 4
GuiVar_FlowTable2Sta:
	.space	4
	.section	.bss.GuiVar_StationGroupPrecipRate,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupPrecipRate, %object
	.size	GuiVar_StationGroupPrecipRate, 4
GuiVar_StationGroupPrecipRate:
	.space	4
	.section	.bss.GuiVar_WENWPAEAPTLSCredentials,"aw",%nobits
	.type	GuiVar_WENWPAEAPTLSCredentials, %object
	.size	GuiVar_WENWPAEAPTLSCredentials, 65
GuiVar_WENWPAEAPTLSCredentials:
	.space	65
	.section	.bss.GuiVar_LightCalendarWeek2Sat,"aw",%nobits
	.type	GuiVar_LightCalendarWeek2Sat, %object
	.size	GuiVar_LightCalendarWeek2Sat, 3
GuiVar_LightCalendarWeek2Sat:
	.space	3
	.section	.bss.GuiVar_TwoWireDiscoveryProgressBar,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDiscoveryProgressBar, %object
	.size	GuiVar_TwoWireDiscoveryProgressBar, 4
GuiVar_TwoWireDiscoveryProgressBar:
	.space	4
	.section	.bss.GuiVar_AboutTPSta09_16Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta09_16Terminal, %object
	.size	GuiVar_AboutTPSta09_16Terminal, 4
GuiVar_AboutTPSta09_16Terminal:
	.space	4
	.section	.bss.GuiVar_TestStationAutoOnTime,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationAutoOnTime, %object
	.size	GuiVar_TestStationAutoOnTime, 4
GuiVar_TestStationAutoOnTime:
	.space	4
	.section	.bss.GuiVar_POCGroupID,"aw",%nobits
	.align	2
	.type	GuiVar_POCGroupID, %object
	.size	GuiVar_POCGroupID, 4
GuiVar_POCGroupID:
	.space	4
	.section	.bss.GuiVar_FlowTable4Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable4Sta, %object
	.size	GuiVar_FlowTable4Sta, 4
GuiVar_FlowTable4Sta:
	.space	4
	.section	.bss.GuiVar_AboutTPSta41_48Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta41_48Terminal, %object
	.size	GuiVar_AboutTPSta41_48Terminal, 4
GuiVar_AboutTPSta41_48Terminal:
	.space	4
	.section	.bss.GuiVar_itmSubNode,"aw",%nobits
	.align	2
	.type	GuiVar_itmSubNode, %object
	.size	GuiVar_itmSubNode, 4
GuiVar_itmSubNode:
	.space	4
	.section	.bss.GuiVar_LRRadioType,"aw",%nobits
	.align	2
	.type	GuiVar_LRRadioType, %object
	.size	GuiVar_LRRadioType, 4
GuiVar_LRRadioType:
	.space	4
	.section	.bss.GuiVar_POCFMType2IsBermad,"aw",%nobits
	.align	2
	.type	GuiVar_POCFMType2IsBermad, %object
	.size	GuiVar_POCFMType2IsBermad, 4
GuiVar_POCFMType2IsBermad:
	.space	4
	.section	.bss.GuiVar_StationCopyGroupName,"aw",%nobits
	.type	GuiVar_StationCopyGroupName, %object
	.size	GuiVar_StationCopyGroupName, 49
GuiVar_StationCopyGroupName:
	.space	49
	.section	.bss.GuiVar_RptDate,"aw",%nobits
	.type	GuiVar_RptDate, %object
	.size	GuiVar_RptDate, 6
GuiVar_RptDate:
	.space	6
	.section	.bss.GuiVar_WENRadioMode,"aw",%nobits
	.align	2
	.type	GuiVar_WENRadioMode, %object
	.size	GuiVar_WENRadioMode, 4
GuiVar_WENRadioMode:
	.space	4
	.section	.bss.GuiVar_TwoWireDiscoveredStaDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDiscoveredStaDecoders, %object
	.size	GuiVar_TwoWireDiscoveredStaDecoders, 4
GuiVar_TwoWireDiscoveredStaDecoders:
	.space	4
	.section	.bss.GuiVar_FlowRecExpected,"aw",%nobits
	.align	2
	.type	GuiVar_FlowRecExpected, %object
	.size	GuiVar_FlowRecExpected, 4
GuiVar_FlowRecExpected:
	.space	4
	.section	.bss.GuiVar_HistoricalETCity,"aw",%nobits
	.type	GuiVar_HistoricalETCity, %object
	.size	GuiVar_HistoricalETCity, 25
GuiVar_HistoricalETCity:
	.space	25
	.section	.bss.GuiVar_FlowTable6Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable6Sta, %object
	.size	GuiVar_FlowTable6Sta, 4
GuiVar_FlowTable6Sta:
	.space	4
	.section	.bss.GuiVar_SystemPOCSelected,"aw",%nobits
	.align	2
	.type	GuiVar_SystemPOCSelected, %object
	.size	GuiVar_SystemPOCSelected, 4
GuiVar_SystemPOCSelected:
	.space	4
	.section	.bss.GuiVar_ContactStatusNameAbbrv,"aw",%nobits
	.type	GuiVar_ContactStatusNameAbbrv, %object
	.size	GuiVar_ContactStatusNameAbbrv, 49
GuiVar_ContactStatusNameAbbrv:
	.space	49
	.section	.bss.GuiVar_BudgetAllocation,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetAllocation, %object
	.size	GuiVar_BudgetAllocation, 4
GuiVar_BudgetAllocation:
	.space	4
	.section	.bss.GuiVar_GroupSettingA_9,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_9, %object
	.size	GuiVar_GroupSettingA_9, 4
GuiVar_GroupSettingA_9:
	.space	4
	.section	.bss.GuiVar_StationInfoNoWaterDays,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoNoWaterDays, %object
	.size	GuiVar_StationInfoNoWaterDays, 4
GuiVar_StationInfoNoWaterDays:
	.space	4
	.section	.bss.GuiVar_WENWEPShowTXKeyIndex,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPShowTXKeyIndex, %object
	.size	GuiVar_WENWEPShowTXKeyIndex, 4
GuiVar_WENWEPShowTXKeyIndex:
	.space	4
	.section	.bss.GuiVar_RptScheduledMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptScheduledMin, %object
	.size	GuiVar_RptScheduledMin, 4
GuiVar_RptScheduledMin:
	.space	4
	.section	.bss.GuiVar_StatusSystemFlowStatus,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowStatus, %object
	.size	GuiVar_StatusSystemFlowStatus, 4
GuiVar_StatusSystemFlowStatus:
	.space	4
	.section	.bss.GuiVar_FlowTable8Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable8Sta, %object
	.size	GuiVar_FlowTable8Sta, 4
GuiVar_FlowTable8Sta:
	.space	4
	.section	.bss.GuiVar_UnusedGroupsRemoved,"aw",%nobits
	.align	2
	.type	GuiVar_UnusedGroupsRemoved, %object
	.size	GuiVar_UnusedGroupsRemoved, 4
GuiVar_UnusedGroupsRemoved:
	.space	4
	.section	.bss.GuiVar_WENWEPKeyType,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPKeyType, %object
	.size	GuiVar_WENWEPKeyType, 4
GuiVar_WENWEPKeyType:
	.space	4
	.section	.bss.GuiVar_MainMenu2WireOptionsExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenu2WireOptionsExist, %object
	.size	GuiVar_MainMenu2WireOptionsExist, 4
GuiVar_MainMenu2WireOptionsExist:
	.space	4
	.section	.bss.GuiVar_StationSelectionGridShowOnlyStationsInThisGroup,"aw",%nobits
	.align	2
	.type	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup, %object
	.size	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup, 4
GuiVar_StationSelectionGridShowOnlyStationsInThisGroup:
	.space	4
	.section	.bss.GuiVar_IrriDetailsStation,"aw",%nobits
	.type	GuiVar_IrriDetailsStation, %object
	.size	GuiVar_IrriDetailsStation, 4
GuiVar_IrriDetailsStation:
	.space	4
	.section	.bss.GuiVar_POCPreservesAccumGal1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumGal1, %object
	.size	GuiVar_POCPreservesAccumGal1, 4
GuiVar_POCPreservesAccumGal1:
	.space	4
	.section	.bss.GuiVar_POCPreservesAccumGal2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumGal2, %object
	.size	GuiVar_POCPreservesAccumGal2, 4
GuiVar_POCPreservesAccumGal2:
	.space	4
	.section	.bss.GuiVar_POCPreservesAccumGal3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumGal3, %object
	.size	GuiVar_POCPreservesAccumGal3, 4
GuiVar_POCPreservesAccumGal3:
	.space	4
	.section	.bss.GuiVar_ChainStatsFOALChainRRcvd,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsFOALChainRRcvd, %object
	.size	GuiVar_ChainStatsFOALChainRRcvd, 4
GuiVar_ChainStatsFOALChainRRcvd:
	.space	4
	.section	.bss.GuiVar_MVORClose2Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose2Enabled, %object
	.size	GuiVar_MVORClose2Enabled, 4
GuiVar_MVORClose2Enabled:
	.space	4
	.section	.bss.GuiVar_HistoricalET9,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET9, %object
	.size	GuiVar_HistoricalET9, 4
GuiVar_HistoricalET9:
	.space	4
	.section	.bss.GuiVar_CodeDownloadState,"aw",%nobits
	.align	2
	.type	GuiVar_CodeDownloadState, %object
	.size	GuiVar_CodeDownloadState, 4
GuiVar_CodeDownloadState:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxDataLpbkMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxDataLpbkMsgs, %object
	.size	GuiVar_TwoWireStatsRxDataLpbkMsgs, 4
GuiVar_TwoWireStatsRxDataLpbkMsgs:
	.space	4
	.section	.bss.GuiVar_ActivationCode,"aw",%nobits
	.type	GuiVar_ActivationCode, %object
	.size	GuiVar_ActivationCode, 21
GuiVar_ActivationCode:
	.space	21
	.section	.bss.GuiVar_POCPreservesDecoderSN1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDecoderSN1, %object
	.size	GuiVar_POCPreservesDecoderSN1, 4
GuiVar_POCPreservesDecoderSN1:
	.space	4
	.section	.bss.GuiVar_POCPreservesDecoderSN2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDecoderSN2, %object
	.size	GuiVar_POCPreservesDecoderSN2, 4
GuiVar_POCPreservesDecoderSN2:
	.space	4
	.section	.bss.GuiVar_POCPreservesDecoderSN3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDecoderSN3, %object
	.size	GuiVar_POCPreservesDecoderSN3, 4
GuiVar_POCPreservesDecoderSN3:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsS1UCos,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsS1UCos, %object
	.size	GuiVar_TwoWireStatsS1UCos, 4
GuiVar_TwoWireStatsS1UCos:
	.space	4
	.section	.bss.GuiVar_AboutVersion,"aw",%nobits
	.type	GuiVar_AboutVersion, %object
	.size	GuiVar_AboutVersion, 49
GuiVar_AboutVersion:
	.space	49
	.section	.bss.GuiVar_POCFlowMeterOffset3,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterOffset3, %object
	.size	GuiVar_POCFlowMeterOffset3, 4
GuiVar_POCFlowMeterOffset3:
	.space	4
	.section	.bss.GuiVar_WindGagePauseSpeed,"aw",%nobits
	.align	2
	.type	GuiVar_WindGagePauseSpeed, %object
	.size	GuiVar_WindGagePauseSpeed, 4
GuiVar_WindGagePauseSpeed:
	.space	4
	.section	.bss.GuiVar_BudgetFlowTypeMVOR,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetFlowTypeMVOR, %object
	.size	GuiVar_BudgetFlowTypeMVOR, 4
GuiVar_BudgetFlowTypeMVOR:
	.space	4
	.section	.bss.GuiVar_StationGroupRootZoneDepth,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupRootZoneDepth, %object
	.size	GuiVar_StationGroupRootZoneDepth, 4
GuiVar_StationGroupRootZoneDepth:
	.space	4
	.section	.bss.GuiVar_ETRainTableRain,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableRain, %object
	.size	GuiVar_ETRainTableRain, 4
GuiVar_ETRainTableRain:
	.space	4
	.section	.bss.GuiVar_HistoricalETUser3,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser3, %object
	.size	GuiVar_HistoricalETUser3, 4
GuiVar_HistoricalETUser3:
	.space	4
	.section	.bss.GuiVar_StatusNextScheduledIrriGID,"aw",%nobits
	.align	2
	.type	GuiVar_StatusNextScheduledIrriGID, %object
	.size	GuiVar_StatusNextScheduledIrriGID, 4
GuiVar_StatusNextScheduledIrriGID:
	.space	4
	.section	.bss.GuiVar_HistoricalETUser4,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser4, %object
	.size	GuiVar_HistoricalETUser4, 4
GuiVar_HistoricalETUser4:
	.space	4
	.section	.bss.GuiVar_MVOROpen4Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen4Enabled, %object
	.size	GuiVar_MVOROpen4Enabled, 4
GuiVar_MVOROpen4Enabled:
	.space	4
	.section	.bss.GuiVar_AboutTPDashLTerminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashLTerminal, %object
	.size	GuiVar_AboutTPDashLTerminal, 4
GuiVar_AboutTPDashLTerminal:
	.space	4
	.section	.bss.GuiVar_LiveScreensSystemMVORExists,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensSystemMVORExists, %object
	.size	GuiVar_LiveScreensSystemMVORExists, 4
GuiVar_LiveScreensSystemMVORExists:
	.space	4
	.section	.bss.GuiVar_ManualWaterRunTime,"aw",%nobits
	.align	2
	.type	GuiVar_ManualWaterRunTime, %object
	.size	GuiVar_ManualWaterRunTime, 4
GuiVar_ManualWaterRunTime:
	.space	4
	.section	.bss.GuiVar_CommTestIPPort,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestIPPort, %object
	.size	GuiVar_CommTestIPPort, 4
GuiVar_CommTestIPPort:
	.space	4
	.section	.bss.GuiVar_ScheduleBeginDate,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleBeginDate, %object
	.size	GuiVar_ScheduleBeginDate, 4
GuiVar_ScheduleBeginDate:
	.space	4
	.section	.bss.GuiVar_ComboBox_X1,"aw",%nobits
	.align	2
	.type	GuiVar_ComboBox_X1, %object
	.size	GuiVar_ComboBox_X1, 4
GuiVar_ComboBox_X1:
	.space	4
	.section	.bss.GuiVar_GroupSettingA_3,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingA_3, %object
	.size	GuiVar_GroupSettingA_3, 4
GuiVar_GroupSettingA_3:
	.space	4
	.section	.bss.GuiVar_GRPhoneNumber,"aw",%nobits
	.type	GuiVar_GRPhoneNumber, %object
	.size	GuiVar_GRPhoneNumber, 49
GuiVar_GRPhoneNumber:
	.space	49
	.section	.bss.GuiVar_FlowCheckingStopIrriTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingStopIrriTime, %object
	.size	GuiVar_FlowCheckingStopIrriTime, 4
GuiVar_FlowCheckingStopIrriTime:
	.space	4
	.section	.bss.GuiVar_TwoWireDecoderType,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDecoderType, %object
	.size	GuiVar_TwoWireDecoderType, 4
GuiVar_TwoWireDecoderType:
	.space	4
	.section	.bss.GuiVar_StationInfoMoistureBalance,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoMoistureBalance, %object
	.size	GuiVar_StationInfoMoistureBalance, 4
GuiVar_StationInfoMoistureBalance:
	.space	4
	.section	.bss.GuiVar_ChainStatsIRRIIrriRGen,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsIRRIIrriRGen, %object
	.size	GuiVar_ChainStatsIRRIIrriRGen, 4
GuiVar_ChainStatsIRRIIrriRGen:
	.space	4
	.section	.bss.GuiVar_KeyboardShiftEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_KeyboardShiftEnabled, %object
	.size	GuiVar_KeyboardShiftEnabled, 4
GuiVar_KeyboardShiftEnabled:
	.space	4
	.section	.bss.GuiVar_LightsCalendarHighlightYOffset,"aw",%nobits
	.align	2
	.type	GuiVar_LightsCalendarHighlightYOffset, %object
	.size	GuiVar_LightsCalendarHighlightYOffset, 4
GuiVar_LightsCalendarHighlightYOffset:
	.space	4
	.section	.bss.GuiVar_TestRunTime,"aw",%nobits
	.align	2
	.type	GuiVar_TestRunTime, %object
	.size	GuiVar_TestRunTime, 4
GuiVar_TestRunTime:
	.space	4
	.section	.bss.GuiVar_MVOROpen0,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen0, %object
	.size	GuiVar_MVOROpen0, 4
GuiVar_MVOROpen0:
	.space	4
	.section	.bss.GuiVar_MVOROpen1,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen1, %object
	.size	GuiVar_MVOROpen1, 4
GuiVar_MVOROpen1:
	.space	4
	.section	.bss.GuiVar_MVOROpen2,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen2, %object
	.size	GuiVar_MVOROpen2, 4
GuiVar_MVOROpen2:
	.space	4
	.section	.bss.GuiVar_MVOROpen3,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen3, %object
	.size	GuiVar_MVOROpen3, 4
GuiVar_MVOROpen3:
	.space	4
	.section	.bss.GuiVar_StatusETGageReading,"aw",%nobits
	.align	2
	.type	GuiVar_StatusETGageReading, %object
	.size	GuiVar_StatusETGageReading, 4
GuiVar_StatusETGageReading:
	.space	4
	.section	.bss.GuiVar_MVOROpen5,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen5, %object
	.size	GuiVar_MVOROpen5, 4
GuiVar_MVOROpen5:
	.space	4
	.section	.bss.GuiVar_MVOROpen6,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen6, %object
	.size	GuiVar_MVOROpen6, 4
GuiVar_MVOROpen6:
	.space	4
	.section	.bss.GuiVar_MainMenuCommOptionExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuCommOptionExists, %object
	.size	GuiVar_MainMenuCommOptionExists, 4
GuiVar_MainMenuCommOptionExists:
	.space	4
	.section	.bss.GuiVar_TwoWireDiscoveredMoisDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDiscoveredMoisDecoders, %object
	.size	GuiVar_TwoWireDiscoveredMoisDecoders, 4
GuiVar_TwoWireDiscoveredMoisDecoders:
	.space	4
	.section	.bss.GuiVar_SRSubnetXmtSlave,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetXmtSlave, %object
	.size	GuiVar_SRSubnetXmtSlave, 4
GuiVar_SRSubnetXmtSlave:
	.space	4
	.section	.bss.GuiVar_RptFlow,"aw",%nobits
	.align	2
	.type	GuiVar_RptFlow, %object
	.size	GuiVar_RptFlow, 4
GuiVar_RptFlow:
	.space	4
	.section	.bss.GuiVar_TurnOffControllerIsOff,"aw",%nobits
	.align	2
	.type	GuiVar_TurnOffControllerIsOff, %object
	.size	GuiVar_TurnOffControllerIsOff, 4
GuiVar_TurnOffControllerIsOff:
	.space	4
	.section	.bss.GuiVar_AboutDebug,"aw",%nobits
	.align	2
	.type	GuiVar_AboutDebug, %object
	.size	GuiVar_AboutDebug, 4
GuiVar_AboutDebug:
	.space	4
	.section	.bss.GuiVar_LiveScreensBypassFlowShowMaxFlow,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowShowMaxFlow, %object
	.size	GuiVar_LiveScreensBypassFlowShowMaxFlow, 4
GuiVar_LiveScreensBypassFlowShowMaxFlow:
	.space	4
	.section	.bss.GuiVar_Backlight,"aw",%nobits
	.align	2
	.type	GuiVar_Backlight, %object
	.size	GuiVar_Backlight, 4
GuiVar_Backlight:
	.space	4
	.section	.bss.GuiVar_SerialPortStateA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortStateA, %object
	.size	GuiVar_SerialPortStateA, 4
GuiVar_SerialPortStateA:
	.space	4
	.section	.bss.GuiVar_IsAHub,"aw",%nobits
	.align	2
	.type	GuiVar_IsAHub, %object
	.size	GuiVar_IsAHub, 4
GuiVar_IsAHub:
	.space	4
	.section	.bss.GuiVar_StationGroupAllowableSurfaceAccum,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupAllowableSurfaceAccum, %object
	.size	GuiVar_StationGroupAllowableSurfaceAccum, 4
GuiVar_StationGroupAllowableSurfaceAccum:
	.space	4
	.section	.bss.GuiVar_StatusFreezeSwitchConnected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusFreezeSwitchConnected, %object
	.size	GuiVar_StatusFreezeSwitchConnected, 4
GuiVar_StatusFreezeSwitchConnected:
	.space	4
	.section	.bss.GuiVar_SRTransmitPower,"aw",%nobits
	.align	2
	.type	GuiVar_SRTransmitPower, %object
	.size	GuiVar_SRTransmitPower, 4
GuiVar_SRTransmitPower:
	.space	4
	.section	.bss.GuiVar_GroupSetting_Visible_3,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSetting_Visible_3, %object
	.size	GuiVar_GroupSetting_Visible_3, 4
GuiVar_GroupSetting_Visible_3:
	.space	4
	.section	.bss.GuiVar_RptStation,"aw",%nobits
	.type	GuiVar_RptStation, %object
	.size	GuiVar_RptStation, 4
GuiVar_RptStation:
	.space	4
	.section	.bss.GuiVar_StationInfoStationGroup,"aw",%nobits
	.type	GuiVar_StationInfoStationGroup, %object
	.size	GuiVar_StationInfoStationGroup, 49
GuiVar_StationInfoStationGroup:
	.space	49
	.section	.bss.GuiVar_WENWPAPassword,"aw",%nobits
	.type	GuiVar_WENWPAPassword, %object
	.size	GuiVar_WENWPAPassword, 64
GuiVar_WENWPAPassword:
	.space	64
	.section	.bss.GuiVar_StationInfoExpectedFlow,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoExpectedFlow, %object
	.size	GuiVar_StationInfoExpectedFlow, 4
GuiVar_StationInfoExpectedFlow:
	.space	4
	.section	.bss.GuiVar_WENChangeCredentials,"aw",%nobits
	.align	2
	.type	GuiVar_WENChangeCredentials, %object
	.size	GuiVar_WENChangeCredentials, 4
GuiVar_WENChangeCredentials:
	.space	4
	.section	.bss.GuiVar_AboutWMOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutWMOption, %object
	.size	GuiVar_AboutWMOption, 4
GuiVar_AboutWMOption:
	.space	4
	.section	.bss.GuiVar_TaskList,"aw",%nobits
	.type	GuiVar_TaskList, %object
	.size	GuiVar_TaskList, 2001
GuiVar_TaskList:
	.space	2001
	.section	.bss.GuiVar_POCType,"aw",%nobits
	.align	2
	.type	GuiVar_POCType, %object
	.size	GuiVar_POCType, 4
GuiVar_POCType:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsTxAcksSent,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsTxAcksSent, %object
	.size	GuiVar_TwoWireStatsTxAcksSent, 4
GuiVar_TwoWireStatsTxAcksSent:
	.space	4
	.section	.bss.GuiVar_IrriCommChainConfigCurrentFromTP,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigCurrentFromTP, %object
	.size	GuiVar_IrriCommChainConfigCurrentFromTP, 4
GuiVar_IrriCommChainConfigCurrentFromTP:
	.space	4
	.section	.bss.GuiVar_BudgetEntryOptionIdx,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetEntryOptionIdx, %object
	.size	GuiVar_BudgetEntryOptionIdx, 4
GuiVar_BudgetEntryOptionIdx:
	.space	4
	.section	.bss.GuiVar_ENMACAddress,"aw",%nobits
	.type	GuiVar_ENMACAddress, %object
	.size	GuiVar_ENMACAddress, 49
GuiVar_ENMACAddress:
	.space	49
	.section	.bss.GuiVar_ChainStatsErrorsG,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsG, %object
	.size	GuiVar_ChainStatsErrorsG, 4
GuiVar_ChainStatsErrorsG:
	.space	4
	.section	.bss.GuiVar_BudgetForThisPeriod,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetForThisPeriod, %object
	.size	GuiVar_BudgetForThisPeriod, 4
GuiVar_BudgetForThisPeriod:
	.space	4
	.section	.bss.GuiVar_GRService,"aw",%nobits
	.type	GuiVar_GRService, %object
	.size	GuiVar_GRService, 49
GuiVar_GRService:
	.space	49
	.section	.bss.GuiVar_WENChangePassphrase,"aw",%nobits
	.align	2
	.type	GuiVar_WENChangePassphrase, %object
	.size	GuiVar_WENChangePassphrase, 4
GuiVar_WENChangePassphrase:
	.space	4
	.section	.bss.GuiVar_WalkThruDelay,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruDelay, %object
	.size	GuiVar_WalkThruDelay, 4
GuiVar_WalkThruDelay:
	.space	4
	.section	.bss.GuiVar_DateTimeSec,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeSec, %object
	.size	GuiVar_DateTimeSec, 4
GuiVar_DateTimeSec:
	.space	4
	.section	.bss.GuiVar_LRHubFeatureNotAvailable,"aw",%nobits
	.type	GuiVar_LRHubFeatureNotAvailable, %object
	.size	GuiVar_LRHubFeatureNotAvailable, 1
GuiVar_LRHubFeatureNotAvailable:
	.space	1
	.section	.bss.GuiVar_KeyboardType,"aw",%nobits
	.align	2
	.type	GuiVar_KeyboardType, %object
	.size	GuiVar_KeyboardType, 4
GuiVar_KeyboardType:
	.space	4
	.section	.bss.GuiVar_SerialPortCTSA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortCTSA, %object
	.size	GuiVar_SerialPortCTSA, 4
GuiVar_SerialPortCTSA:
	.space	4
	.section	.bss.GuiVar_SerialPortCTSB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortCTSB, %object
	.size	GuiVar_SerialPortCTSB, 4
GuiVar_SerialPortCTSB:
	.space	4
	.section	.bss.GuiVar_LightIsPhysicallyAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_LightIsPhysicallyAvailable, %object
	.size	GuiVar_LightIsPhysicallyAvailable, 4
GuiVar_LightIsPhysicallyAvailable:
	.space	4
	.section	.bss.GuiVar_AboutNetworkID,"aw",%nobits
	.align	2
	.type	GuiVar_AboutNetworkID, %object
	.size	GuiVar_AboutNetworkID, 4
GuiVar_AboutNetworkID:
	.space	4
	.section	.bss.GuiVar_TwoWirePOCUsedForBypass,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWirePOCUsedForBypass, %object
	.size	GuiVar_TwoWirePOCUsedForBypass, 4
GuiVar_TwoWirePOCUsedForBypass:
	.space	4
	.section	.bss.GuiVar_MoisSetPoint_Low,"aw",%nobits
	.align	2
	.type	GuiVar_MoisSetPoint_Low, %object
	.size	GuiVar_MoisSetPoint_Low, 4
GuiVar_MoisSetPoint_Low:
	.space	4
	.section	.bss.GuiVar_WalkThruShowStartInSeconds,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruShowStartInSeconds, %object
	.size	GuiVar_WalkThruShowStartInSeconds, 4
GuiVar_WalkThruShowStartInSeconds:
	.space	4
	.section	.bss.GuiVar_ChainStatsErrorsK,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsK, %object
	.size	GuiVar_ChainStatsErrorsK, 4
GuiVar_ChainStatsErrorsK:
	.space	4
	.section	.bss.GuiVar_NoWaterDaysDays,"aw",%nobits
	.align	2
	.type	GuiVar_NoWaterDaysDays, %object
	.size	GuiVar_NoWaterDaysDays, 4
GuiVar_NoWaterDaysDays:
	.space	4
	.section	.bss.GuiVar_LiveScreensCommFLEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommFLEnabled, %object
	.size	GuiVar_LiveScreensCommFLEnabled, 4
GuiVar_LiveScreensCommFLEnabled:
	.space	4
	.section	.bss.GuiVar_ComboBoxItemIndex,"aw",%nobits
	.align	2
	.type	GuiVar_ComboBoxItemIndex, %object
	.size	GuiVar_ComboBoxItemIndex, 4
GuiVar_ComboBoxItemIndex:
	.space	4
	.section	.bss.GuiVar_SerialPortStateTP,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortStateTP, %object
	.size	GuiVar_SerialPortStateTP, 4
GuiVar_SerialPortStateTP:
	.space	4
	.section	.bss.GuiVar_RptRainMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptRainMin, %object
	.size	GuiVar_RptRainMin, 4
GuiVar_RptRainMin:
	.space	4
	.section	.bss.GuiVar_HubList,"aw",%nobits
	.type	GuiVar_HubList, %object
	.size	GuiVar_HubList, 513
GuiVar_HubList:
	.space	513
	.section	.bss.GuiVar_Contrast,"aw",%nobits
	.align	2
	.type	GuiVar_Contrast, %object
	.size	GuiVar_Contrast, 4
GuiVar_Contrast:
	.space	4
	.section	.bss.GuiVar_WENPassphrase,"aw",%nobits
	.type	GuiVar_WENPassphrase, %object
	.size	GuiVar_WENPassphrase, 65
GuiVar_WENPassphrase:
	.space	65
	.section	.bss.GuiVar_RainBucketInUse,"aw",%nobits
	.align	2
	.type	GuiVar_RainBucketInUse, %object
	.size	GuiVar_RainBucketInUse, 4
GuiVar_RainBucketInUse:
	.space	4
	.section	.bss.GuiVar_HistoricalETUser10,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser10, %object
	.size	GuiVar_HistoricalETUser10, 4
GuiVar_HistoricalETUser10:
	.space	4
	.section	.bss.GuiVar_HistoricalETUser11,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser11, %object
	.size	GuiVar_HistoricalETUser11, 4
GuiVar_HistoricalETUser11:
	.space	4
	.section	.bss.GuiVar_HistoricalETUserCounty,"aw",%nobits
	.type	GuiVar_HistoricalETUserCounty, %object
	.size	GuiVar_HistoricalETUserCounty, 25
GuiVar_HistoricalETUserCounty:
	.space	25
	.section	.bss.GuiVar_StatusWindGageReading,"aw",%nobits
	.align	2
	.type	GuiVar_StatusWindGageReading, %object
	.size	GuiVar_StatusWindGageReading, 4
GuiVar_StatusWindGageReading:
	.space	4
	.section	.bss.GuiVar_StationSelectionGridGroupName,"aw",%nobits
	.type	GuiVar_StationSelectionGridGroupName, %object
	.size	GuiVar_StationSelectionGridGroupName, 49
GuiVar_StationSelectionGridGroupName:
	.space	49
	.section	.bss.GuiVar_StationInfoETFactor,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoETFactor, %object
	.size	GuiVar_StationInfoETFactor, 4
GuiVar_StationInfoETFactor:
	.space	4
	.section	.bss.GuiVar_IrriCommChainConfigCurrentFromIrri,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigCurrentFromIrri, %object
	.size	GuiVar_IrriCommChainConfigCurrentFromIrri, 4
GuiVar_IrriCommChainConfigCurrentFromIrri:
	.space	4
	.section	.bss.GuiVar_StatusNOWDaysInEffect,"aw",%nobits
	.align	2
	.type	GuiVar_StatusNOWDaysInEffect, %object
	.size	GuiVar_StatusNOWDaysInEffect, 4
GuiVar_StatusNOWDaysInEffect:
	.space	4
	.section	.bss.GuiVar_POCBudget3,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget3, %object
	.size	GuiVar_POCBudget3, 4
GuiVar_POCBudget3:
	.space	4
	.section	.bss.GuiVar_SerialPortXmitLengthA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmitLengthA, %object
	.size	GuiVar_SerialPortXmitLengthA, 4
GuiVar_SerialPortXmitLengthA:
	.space	4
	.section	.bss.GuiVar_SerialPortXmitLengthB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmitLengthB, %object
	.size	GuiVar_SerialPortXmitLengthB, 4
GuiVar_SerialPortXmitLengthB:
	.space	4
	.section	.bss.GuiVar_HistoricalETUserCity,"aw",%nobits
	.type	GuiVar_HistoricalETUserCity, %object
	.size	GuiVar_HistoricalETUserCity, 25
GuiVar_HistoricalETUserCity:
	.space	25
	.section	.bss.GuiVar_ScheduleUsesET,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleUsesET, %object
	.size	GuiVar_ScheduleUsesET, 4
GuiVar_ScheduleUsesET:
	.space	4
	.section	.bss.GuiVar_ScheduleWaterDay_Tue,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Tue, %object
	.size	GuiVar_ScheduleWaterDay_Tue, 4
GuiVar_ScheduleWaterDay_Tue:
	.space	4
	.section	.bss.GuiVar_StatusRainBucketControllerName,"aw",%nobits
	.type	GuiVar_StatusRainBucketControllerName, %object
	.size	GuiVar_StatusRainBucketControllerName, 49
GuiVar_StatusRainBucketControllerName:
	.space	49
	.section	.bss.GuiVar_FlowTableGPM,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTableGPM, %object
	.size	GuiVar_FlowTableGPM, 4
GuiVar_FlowTableGPM:
	.space	4
	.section	.bss.GuiVar_TestStationTimeRemaining,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationTimeRemaining, %object
	.size	GuiVar_TestStationTimeRemaining, 4
GuiVar_TestStationTimeRemaining:
	.space	4
	.section	.bss.GuiVar_GRIPAddress,"aw",%nobits
	.type	GuiVar_GRIPAddress, %object
	.size	GuiVar_GRIPAddress, 49
GuiVar_GRIPAddress:
	.space	49
	.section	.bss.GuiVar_AboutTPPOCTerminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPPOCTerminal, %object
	.size	GuiVar_AboutTPPOCTerminal, 4
GuiVar_AboutTPPOCTerminal:
	.space	4
	.section	.bss.GuiVar_LightCalendarWeek1Sun,"aw",%nobits
	.type	GuiVar_LightCalendarWeek1Sun, %object
	.size	GuiVar_LightCalendarWeek1Sun, 3
GuiVar_LightCalendarWeek1Sun:
	.space	3
	.section	.bss.GuiVar_StationGroupMoistureSensorDecoderSN,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupMoistureSensorDecoderSN, %object
	.size	GuiVar_StationGroupMoistureSensorDecoderSN, 4
GuiVar_StationGroupMoistureSensorDecoderSN:
	.space	4
	.section	.bss.GuiVar_ChainStatsErrorsA,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsA, %object
	.size	GuiVar_ChainStatsErrorsA, 4
GuiVar_ChainStatsErrorsA:
	.space	4
	.section	.bss.GuiVar_SystemPreserves5SecNext,"aw",%nobits
	.align	2
	.type	GuiVar_SystemPreserves5SecNext, %object
	.size	GuiVar_SystemPreserves5SecNext, 4
GuiVar_SystemPreserves5SecNext:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingRange3,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingRange3, %object
	.size	GuiVar_SystemFlowCheckingRange3, 4
GuiVar_SystemFlowCheckingRange3:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingRange1,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingRange1, %object
	.size	GuiVar_SystemFlowCheckingRange1, 4
GuiVar_SystemFlowCheckingRange1:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingRange2,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingRange2, %object
	.size	GuiVar_SystemFlowCheckingRange2, 4
GuiVar_SystemFlowCheckingRange2:
	.space	4
	.section	.bss.GuiVar_RptManualPGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptManualPGal, %object
	.size	GuiVar_RptManualPGal, 8
GuiVar_RptManualPGal:
	.space	8
	.section	.bss.GuiVar_DateTimeMonth,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeMonth, %object
	.size	GuiVar_DateTimeMonth, 4
GuiVar_DateTimeMonth:
	.space	4
	.section	.bss.GuiVar_ScheduleStartTimeEqualsStopTime,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleStartTimeEqualsStopTime, %object
	.size	GuiVar_ScheduleStartTimeEqualsStopTime, 4
GuiVar_ScheduleStartTimeEqualsStopTime:
	.space	4
	.section	.bss.GuiVar_ChainStatsControllersInChain,"aw",%nobits
	.type	GuiVar_ChainStatsControllersInChain, %object
	.size	GuiVar_ChainStatsControllersInChain, 49
GuiVar_ChainStatsControllersInChain:
	.space	49
	.section	.bss.GuiVar_ChainStatsErrorsE,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsE, %object
	.size	GuiVar_ChainStatsErrorsE, 4
GuiVar_ChainStatsErrorsE:
	.space	4
	.section	.bss.GuiVar_FlowCheckingNumOn,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingNumOn, %object
	.size	GuiVar_FlowCheckingNumOn, 4
GuiVar_FlowCheckingNumOn:
	.space	4
	.section	.bss.GuiVar_ManualPStartTime1,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime1, %object
	.size	GuiVar_ManualPStartTime1, 4
GuiVar_ManualPStartTime1:
	.space	4
	.section	.bss.GuiVar_ManualPStartTime2,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime2, %object
	.size	GuiVar_ManualPStartTime2, 4
GuiVar_ManualPStartTime2:
	.space	4
	.section	.bss.GuiVar_ManualPStartTime3,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime3, %object
	.size	GuiVar_ManualPStartTime3, 4
GuiVar_ManualPStartTime3:
	.space	4
	.section	.bss.GuiVar_TwoWireStaAIsSet,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaAIsSet, %object
	.size	GuiVar_TwoWireStaAIsSet, 4
GuiVar_TwoWireStaAIsSet:
	.space	4
	.section	.bss.GuiVar_ManualPStartTime5,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime5, %object
	.size	GuiVar_ManualPStartTime5, 4
GuiVar_ManualPStartTime5:
	.space	4
	.section	.bss.GuiVar_ManualPStartTime6,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime6, %object
	.size	GuiVar_ManualPStartTime6, 4
GuiVar_ManualPStartTime6:
	.space	4
	.section	.bss.GuiVar_FlowCheckingIterations,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingIterations, %object
	.size	GuiVar_FlowCheckingIterations, 4
GuiVar_FlowCheckingIterations:
	.space	4
	.section	.bss.GuiVar_HistoricalETUser1,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser1, %object
	.size	GuiVar_HistoricalETUser1, 4
GuiVar_HistoricalETUser1:
	.space	4
	.section	.bss.GuiVar_HistoricalETUser2,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser2, %object
	.size	GuiVar_HistoricalETUser2, 4
GuiVar_HistoricalETUser2:
	.space	4
	.section	.bss.GuiVar_POCPreservesAccumMS1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumMS1, %object
	.size	GuiVar_POCPreservesAccumMS1, 4
GuiVar_POCPreservesAccumMS1:
	.space	4
	.section	.bss.GuiVar_POCPreservesAccumMS2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumMS2, %object
	.size	GuiVar_POCPreservesAccumMS2, 4
GuiVar_POCPreservesAccumMS2:
	.space	4
	.section	.bss.GuiVar_POCPreservesAccumMS3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumMS3, %object
	.size	GuiVar_POCPreservesAccumMS3, 4
GuiVar_POCPreservesAccumMS3:
	.space	4
	.section	.bss.GuiVar_HistoricalETUser6,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser6, %object
	.size	GuiVar_HistoricalETUser6, 4
GuiVar_HistoricalETUser6:
	.space	4
	.section	.bss.GuiVar_HistoricalETUser7,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser7, %object
	.size	GuiVar_HistoricalETUser7, 4
GuiVar_HistoricalETUser7:
	.space	4
	.section	.bss.GuiVar_HistoricalETUser8,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser8, %object
	.size	GuiVar_HistoricalETUser8, 4
GuiVar_HistoricalETUser8:
	.space	4
	.section	.bss.GuiVar_HistoricalETUser9,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser9, %object
	.size	GuiVar_HistoricalETUser9, 4
GuiVar_HistoricalETUser9:
	.space	4
	.section	.bss.GuiVar_LiveScreensPOCFlowMVState,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensPOCFlowMVState, %object
	.size	GuiVar_LiveScreensPOCFlowMVState, 4
GuiVar_LiveScreensPOCFlowMVState:
	.space	4
	.section	.bss.GuiVar_StationGroupMicroclimateFactor,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupMicroclimateFactor, %object
	.size	GuiVar_StationGroupMicroclimateFactor, 4
GuiVar_StationGroupMicroclimateFactor:
	.space	4
	.section	.bss.GuiVar_MainMenuFlowCheckingInUse,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuFlowCheckingInUse, %object
	.size	GuiVar_MainMenuFlowCheckingInUse, 4
GuiVar_MainMenuFlowCheckingInUse:
	.space	4
	.section	.bss.GuiVar_GroupSettingB_0,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_0, %object
	.size	GuiVar_GroupSettingB_0, 4
GuiVar_GroupSettingB_0:
	.space	4
	.section	.bss.GuiVar_GroupSettingB_1,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_1, %object
	.size	GuiVar_GroupSettingB_1, 4
GuiVar_GroupSettingB_1:
	.space	4
	.section	.bss.GuiVar_GroupSettingB_2,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_2, %object
	.size	GuiVar_GroupSettingB_2, 4
GuiVar_GroupSettingB_2:
	.space	4
	.section	.bss.GuiVar_GroupSettingB_3,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_3, %object
	.size	GuiVar_GroupSettingB_3, 4
GuiVar_GroupSettingB_3:
	.space	4
	.section	.bss.GuiVar_GroupSettingB_4,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_4, %object
	.size	GuiVar_GroupSettingB_4, 4
GuiVar_GroupSettingB_4:
	.space	4
	.section	.bss.GuiVar_ManualPRunTimeSingle,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPRunTimeSingle, %object
	.size	GuiVar_ManualPRunTimeSingle, 4
GuiVar_ManualPRunTimeSingle:
	.space	4
	.section	.bss.GuiVar_SRSubnetXmtRepeater,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetXmtRepeater, %object
	.size	GuiVar_SRSubnetXmtRepeater, 4
GuiVar_SRSubnetXmtRepeater:
	.space	4
	.section	.bss.GuiVar_GroupSettingB_7,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_7, %object
	.size	GuiVar_GroupSettingB_7, 4
GuiVar_GroupSettingB_7:
	.space	4
	.section	.bss.GuiVar_GroupSettingB_8,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_8, %object
	.size	GuiVar_GroupSettingB_8, 4
GuiVar_GroupSettingB_8:
	.space	4
	.section	.bss.GuiVar_GroupSettingB_9,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_9, %object
	.size	GuiVar_GroupSettingB_9, 4
GuiVar_GroupSettingB_9:
	.space	4
	.section	.bss.GuiVar_LRSerialNumber,"aw",%nobits
	.type	GuiVar_LRSerialNumber, %object
	.size	GuiVar_LRSerialNumber, 9
GuiVar_LRSerialNumber:
	.space	9
	.section	.bss.GuiVar_RainSwitchSelected,"aw",%nobits
	.align	2
	.type	GuiVar_RainSwitchSelected, %object
	.size	GuiVar_RainSwitchSelected, 4
GuiVar_RainSwitchSelected:
	.space	4
	.section	.bss.GuiVar_MoistureTabToDisplay,"aw",%nobits
	.align	2
	.type	GuiVar_MoistureTabToDisplay, %object
	.size	GuiVar_MoistureTabToDisplay, 4
GuiVar_MoistureTabToDisplay:
	.space	4
	.section	.bss.GuiVar_POCPreservesFileType,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesFileType, %object
	.size	GuiVar_POCPreservesFileType, 4
GuiVar_POCPreservesFileType:
	.space	4
	.section	.bss.GuiVar_PercentAdjustEndDate_8,"aw",%nobits
	.type	GuiVar_PercentAdjustEndDate_8, %object
	.size	GuiVar_PercentAdjustEndDate_8, 49
GuiVar_PercentAdjustEndDate_8:
	.space	49
	.section	.bss.GuiVar_IrriDetailsRunCycle,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsRunCycle, %object
	.size	GuiVar_IrriDetailsRunCycle, 4
GuiVar_IrriDetailsRunCycle:
	.space	4
	.section	.bss.GuiVar_ChainStatsErrorsF,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsF, %object
	.size	GuiVar_ChainStatsErrorsF, 4
GuiVar_ChainStatsErrorsF:
	.space	4
	.section	.bss.GuiVar_AboutLOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutLOption, %object
	.size	GuiVar_AboutLOption, 4
GuiVar_AboutLOption:
	.space	4
	.section	.bss.GuiVar_WalkThruStationDesc,"aw",%nobits
	.type	GuiVar_WalkThruStationDesc, %object
	.size	GuiVar_WalkThruStationDesc, 49
GuiVar_WalkThruStationDesc:
	.space	49
	.section	.bss.GuiVar_POCPreservesBoxIndex,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesBoxIndex, %object
	.size	GuiVar_POCPreservesBoxIndex, 4
GuiVar_POCPreservesBoxIndex:
	.space	4
	.section	.bss.GuiVar_SerialPortOptionB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortOptionB, %object
	.size	GuiVar_SerialPortOptionB, 4
GuiVar_SerialPortOptionB:
	.space	4
	.section	.bss.GuiVar_ChainStatsErrorsI,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsI, %object
	.size	GuiVar_ChainStatsErrorsI, 4
GuiVar_ChainStatsErrorsI:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsTempCur,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsTempCur, %object
	.size	GuiVar_TwoWireStatsTempCur, 4
GuiVar_TwoWireStatsTempCur:
	.space	4
	.section	.bss.GuiVar_ChainStatsErrorsJ,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsJ, %object
	.size	GuiVar_ChainStatsErrorsJ, 4
GuiVar_ChainStatsErrorsJ:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsPORs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsPORs, %object
	.size	GuiVar_TwoWireStatsPORs, 4
GuiVar_TwoWireStatsPORs:
	.space	4
	.section	.bss.GuiVar_BudgetPeriodsPerYearIdx,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetPeriodsPerYearIdx, %object
	.size	GuiVar_BudgetPeriodsPerYearIdx, 4
GuiVar_BudgetPeriodsPerYearIdx:
	.space	4
	.section	.bss.GuiVar_AboutTPDashMCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashMCard, %object
	.size	GuiVar_AboutTPDashMCard, 4
GuiVar_AboutTPDashMCard:
	.space	4
	.section	.bss.GuiVar_FlowCheckingAllowedToLock,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingAllowedToLock, %object
	.size	GuiVar_FlowCheckingAllowedToLock, 4
GuiVar_FlowCheckingAllowedToLock:
	.space	4
	.section	.bss.GuiVar_RainBucketMaximum24Hour,"aw",%nobits
	.align	2
	.type	GuiVar_RainBucketMaximum24Hour, %object
	.size	GuiVar_RainBucketMaximum24Hour, 4
GuiVar_RainBucketMaximum24Hour:
	.space	4
	.section	.bss.GuiVar_BudgetFlowTypeIrrigation,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetFlowTypeIrrigation, %object
	.size	GuiVar_BudgetFlowTypeIrrigation, 4
GuiVar_BudgetFlowTypeIrrigation:
	.space	4
	.section	.bss.GuiVar_AboutTPSta25_32Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta25_32Terminal, %object
	.size	GuiVar_AboutTPSta25_32Terminal, 4
GuiVar_AboutTPSta25_32Terminal:
	.space	4
	.section	.bss.GuiVar_LightsOutputIsDeenergized,"aw",%nobits
	.align	2
	.type	GuiVar_LightsOutputIsDeenergized, %object
	.size	GuiVar_LightsOutputIsDeenergized, 4
GuiVar_LightsOutputIsDeenergized:
	.space	4
	.section	.bss.GuiVar_FlowCheckingFlag0,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag0, %object
	.size	GuiVar_FlowCheckingFlag0, 4
GuiVar_FlowCheckingFlag0:
	.space	4
	.section	.bss.GuiVar_MoisTempAction_Low,"aw",%nobits
	.align	2
	.type	GuiVar_MoisTempAction_Low, %object
	.size	GuiVar_MoisTempAction_Low, 4
GuiVar_MoisTempAction_Low:
	.space	4
	.section	.bss.GuiVar_TwoWireStaA,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaA, %object
	.size	GuiVar_TwoWireStaA, 4
GuiVar_TwoWireStaA:
	.space	4
	.section	.bss.GuiVar_TwoWireStaB,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaB, %object
	.size	GuiVar_TwoWireStaB, 4
GuiVar_TwoWireStaB:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxCRCErrs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxCRCErrs, %object
	.size	GuiVar_TwoWireStatsRxCRCErrs, 4
GuiVar_TwoWireStatsRxCRCErrs:
	.space	4
	.section	.bss.GuiVar_TwoWireOutputOnA,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireOutputOnA, %object
	.size	GuiVar_TwoWireOutputOnA, 4
GuiVar_TwoWireOutputOnA:
	.space	4
	.section	.bss.GuiVar_TwoWireOutputOnB,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireOutputOnB, %object
	.size	GuiVar_TwoWireOutputOnB, 4
GuiVar_TwoWireOutputOnB:
	.space	4
	.section	.bss.GuiVar_RptRReMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptRReMin, %object
	.size	GuiVar_RptRReMin, 4
GuiVar_RptRReMin:
	.space	4
	.section	.bss.GuiVar_LightsOutput,"aw",%nobits
	.align	2
	.type	GuiVar_LightsOutput, %object
	.size	GuiVar_LightsOutput, 4
GuiVar_LightsOutput:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxEnqMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxEnqMsgs, %object
	.size	GuiVar_TwoWireStatsRxEnqMsgs, 4
GuiVar_TwoWireStatsRxEnqMsgs:
	.space	4
	.section	.bss.GuiVar_StationGroupHeadType,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupHeadType, %object
	.size	GuiVar_StationGroupHeadType, 4
GuiVar_StationGroupHeadType:
	.space	4
	.section	.bss.GuiVar_MoisReading_Current,"aw",%nobits
	.align	2
	.type	GuiVar_MoisReading_Current, %object
	.size	GuiVar_MoisReading_Current, 4
GuiVar_MoisReading_Current:
	.space	4
	.section	.bss.GuiVar_MainMenuWeatherDeviceExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuWeatherDeviceExists, %object
	.size	GuiVar_MainMenuWeatherDeviceExists, 4
GuiVar_MainMenuWeatherDeviceExists:
	.space	4
	.section	.bss.GuiVar_FlowRecFlag,"aw",%nobits
	.type	GuiVar_FlowRecFlag, %object
	.size	GuiVar_FlowRecFlag, 17
GuiVar_FlowRecFlag:
	.space	17
	.section	.bss.GuiVar_ChainStatsErrorsB,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsB, %object
	.size	GuiVar_ChainStatsErrorsB, 4
GuiVar_ChainStatsErrorsB:
	.space	4
	.section	.bss.GuiVar_ChainStatsErrorsC,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsC, %object
	.size	GuiVar_ChainStatsErrorsC, 4
GuiVar_ChainStatsErrorsC:
	.space	4
	.section	.bss.GuiVar_ChainStatsErrorsD,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsD, %object
	.size	GuiVar_ChainStatsErrorsD, 4
GuiVar_ChainStatsErrorsD:
	.space	4
	.section	.bss.GuiVar_StationInfoSoakInTime,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoSoakInTime, %object
	.size	GuiVar_StationInfoSoakInTime, 4
GuiVar_StationInfoSoakInTime:
	.space	4
	.section	.bss.GuiVar_TestStationAutoMode,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationAutoMode, %object
	.size	GuiVar_TestStationAutoMode, 4
GuiVar_TestStationAutoMode:
	.space	4
	.section	.bss.GuiVar_ETGageMaxPercent,"aw",%nobits
	.align	2
	.type	GuiVar_ETGageMaxPercent, %object
	.size	GuiVar_ETGageMaxPercent, 4
GuiVar_ETGageMaxPercent:
	.space	4
	.section	.bss.GuiVar_ChainStatsErrorsH,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsH, %object
	.size	GuiVar_ChainStatsErrorsH, 4
GuiVar_ChainStatsErrorsH:
	.space	4
	.section	.bss.GuiVar_POCFlowMeterK1,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterK1, %object
	.size	GuiVar_POCFlowMeterK1, 4
GuiVar_POCFlowMeterK1:
	.space	4
	.section	.bss.GuiVar_POCFlowMeterK2,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterK2, %object
	.size	GuiVar_POCFlowMeterK2, 4
GuiVar_POCFlowMeterK2:
	.space	4
	.section	.bss.GuiVar_POCFlowMeterK3,"aw",%nobits
	.align	2
	.type	GuiVar_POCFlowMeterK3, %object
	.size	GuiVar_POCFlowMeterK3, 4
GuiVar_POCFlowMeterK3:
	.space	4
	.section	.bss.GuiVar_ChainStatsErrorsL,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsErrorsL, %object
	.size	GuiVar_ChainStatsErrorsL, 4
GuiVar_ChainStatsErrorsL:
	.space	4
	.section	.bss.GuiVar_CommOptionRadioTestAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionRadioTestAvailable, %object
	.size	GuiVar_CommOptionRadioTestAvailable, 4
GuiVar_CommOptionRadioTestAvailable:
	.space	4
	.section	.bss.GuiVar_AlertDate,"aw",%nobits
	.type	GuiVar_AlertDate, %object
	.size	GuiVar_AlertDate, 6
GuiVar_AlertDate:
	.space	6
	.section	.bss.GuiVar_FinishTimesNotes,"aw",%nobits
	.type	GuiVar_FinishTimesNotes, %object
	.size	GuiVar_FinishTimesNotes, 101
GuiVar_FinishTimesNotes:
	.space	101
	.section	.bss.GuiVar_SRRepeaterLinkedToItself,"aw",%nobits
	.align	2
	.type	GuiVar_SRRepeaterLinkedToItself, %object
	.size	GuiVar_SRRepeaterLinkedToItself, 4
GuiVar_SRRepeaterLinkedToItself:
	.space	4
	.section	.bss.GuiVar_StationInfoShowCopyStation,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoShowCopyStation, %object
	.size	GuiVar_StationInfoShowCopyStation, 4
GuiVar_StationInfoShowCopyStation:
	.space	4
	.section	.bss.GuiVar_GRECIO,"aw",%nobits
	.type	GuiVar_GRECIO, %object
	.size	GuiVar_GRECIO, 49
GuiVar_GRECIO:
	.space	49
	.section	.bss.GuiVar_FlowRecTimeStamp,"aw",%nobits
	.type	GuiVar_FlowRecTimeStamp, %object
	.size	GuiVar_FlowRecTimeStamp, 17
GuiVar_FlowRecTimeStamp:
	.space	17
	.section	.bss.GuiVar_WalkThruBoxIndex,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruBoxIndex, %object
	.size	GuiVar_WalkThruBoxIndex, 4
GuiVar_WalkThruBoxIndex:
	.space	4
	.section	.bss.GuiVar_WENWPAPEAPOption,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAPEAPOption, %object
	.size	GuiVar_WENWPAPEAPOption, 4
GuiVar_WENWPAPEAPOption:
	.space	4
	.section	.bss.GuiVar_FlowCheckingMaxOn,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingMaxOn, %object
	.size	GuiVar_FlowCheckingMaxOn, 4
GuiVar_FlowCheckingMaxOn:
	.space	4
	.section	.bss.GuiVar_CommOptionIsConnected,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionIsConnected, %object
	.size	GuiVar_CommOptionIsConnected, 4
GuiVar_CommOptionIsConnected:
	.space	4
	.section	.bss.GuiVar_AlertTime,"aw",%nobits
	.type	GuiVar_AlertTime, %object
	.size	GuiVar_AlertTime, 10
GuiVar_AlertTime:
	.space	10
	.section	.bss.GuiVar_WENWPAUsername,"aw",%nobits
	.type	GuiVar_WENWPAUsername, %object
	.size	GuiVar_WENWPAUsername, 64
GuiVar_WENWPAUsername:
	.space	64
	.section	.bss.GuiVar_AboutAquaponicsOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutAquaponicsOption, %object
	.size	GuiVar_AboutAquaponicsOption, 4
GuiVar_AboutAquaponicsOption:
	.space	4
	.section	.bss.GuiVar_SRSubnetRcvRepeater,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetRcvRepeater, %object
	.size	GuiVar_SRSubnetRcvRepeater, 4
GuiVar_SRSubnetRcvRepeater:
	.space	4
	.section	.bss.GuiVar_FLPartOfChain,"aw",%nobits
	.align	2
	.type	GuiVar_FLPartOfChain, %object
	.size	GuiVar_FLPartOfChain, 4
GuiVar_FLPartOfChain:
	.space	4
	.section	.bss.GuiVar_SRPort,"aw",%nobits
	.align	2
	.type	GuiVar_SRPort, %object
	.size	GuiVar_SRPort, 4
GuiVar_SRPort:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsEEPCRCErrStats,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsEEPCRCErrStats, %object
	.size	GuiVar_TwoWireStatsEEPCRCErrStats, 4
GuiVar_TwoWireStatsEEPCRCErrStats:
	.space	4
	.section	.bss.GuiVar_AboutSSEOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutSSEOption, %object
	.size	GuiVar_AboutSSEOption, 4
GuiVar_AboutSSEOption:
	.space	4
	.section	.bss.GuiVar_StationCopyTextOffset_Right,"aw",%nobits
	.align	2
	.type	GuiVar_StationCopyTextOffset_Right, %object
	.size	GuiVar_StationCopyTextOffset_Right, 4
GuiVar_StationCopyTextOffset_Right:
	.space	4
	.section	.bss.GuiVar_DateTimeDay,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeDay, %object
	.size	GuiVar_DateTimeDay, 4
GuiVar_DateTimeDay:
	.space	4
	.section	.bss.GuiVar_ScheduleWaterDay_Wed,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Wed, %object
	.size	GuiVar_ScheduleWaterDay_Wed, 4
GuiVar_ScheduleWaterDay_Wed:
	.space	4
	.section	.bss.GuiVar_StationGroupSystemGID,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSystemGID, %object
	.size	GuiVar_StationGroupSystemGID, 4
GuiVar_StationGroupSystemGID:
	.space	4
	.section	.bss.GuiVar_POCDecoderSN2,"aw",%nobits
	.align	2
	.type	GuiVar_POCDecoderSN2, %object
	.size	GuiVar_POCDecoderSN2, 4
GuiVar_POCDecoderSN2:
	.space	4
	.section	.bss.GuiVar_POCDecoderSN3,"aw",%nobits
	.align	2
	.type	GuiVar_POCDecoderSN3, %object
	.size	GuiVar_POCDecoderSN3, 4
GuiVar_POCDecoderSN3:
	.space	4
	.section	.bss.GuiVar_itmMenuItemDesc,"aw",%nobits
	.type	GuiVar_itmMenuItemDesc, %object
	.size	GuiVar_itmMenuItemDesc, 65
GuiVar_itmMenuItemDesc:
	.space	65
	.section	.bss.GuiVar_HistoricalETUser12,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser12, %object
	.size	GuiVar_HistoricalETUser12, 4
GuiVar_HistoricalETUser12:
	.space	4
	.section	.bss.GuiVar_WENWPAEncryptionWEP,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEncryptionWEP, %object
	.size	GuiVar_WENWPAEncryptionWEP, 4
GuiVar_WENWPAEncryptionWEP:
	.space	4
	.section	.bss.GuiVar_POCFMType3IsBermad,"aw",%nobits
	.align	2
	.type	GuiVar_POCFMType3IsBermad, %object
	.size	GuiVar_POCFMType3IsBermad, 4
GuiVar_POCFMType3IsBermad:
	.space	4
	.section	.bss.GuiVar_ManualPStartTime5Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime5Enabled, %object
	.size	GuiVar_ManualPStartTime5Enabled, 4
GuiVar_ManualPStartTime5Enabled:
	.space	4
	.section	.bss.GuiVar_TestStationTimeDisplay,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationTimeDisplay, %object
	.size	GuiVar_TestStationTimeDisplay, 4
GuiVar_TestStationTimeDisplay:
	.space	4
	.section	.bss.GuiVar_MVORClose6,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose6, %object
	.size	GuiVar_MVORClose6, 4
GuiVar_MVORClose6:
	.space	4
	.section	.bss.GuiVar_IrriCommChainConfigNameAbbrv,"aw",%nobits
	.type	GuiVar_IrriCommChainConfigNameAbbrv, %object
	.size	GuiVar_IrriCommChainConfigNameAbbrv, 49
GuiVar_IrriCommChainConfigNameAbbrv:
	.space	49
	.section	.bss.GuiVar_ENNetmask,"aw",%nobits
	.align	2
	.type	GuiVar_ENNetmask, %object
	.size	GuiVar_ENNetmask, 4
GuiVar_ENNetmask:
	.space	4
	.section	.bss.GuiVar_CommOptionPortAIndex,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionPortAIndex, %object
	.size	GuiVar_CommOptionPortAIndex, 4
GuiVar_CommOptionPortAIndex:
	.space	4
	.section	.bss.GuiVar_LRFrequency_Decimal,"aw",%nobits
	.align	2
	.type	GuiVar_LRFrequency_Decimal, %object
	.size	GuiVar_LRFrequency_Decimal, 4
GuiVar_LRFrequency_Decimal:
	.space	4
	.section	.bss.GuiVar_FlowTable10Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable10Sta, %object
	.size	GuiVar_FlowTable10Sta, 4
GuiVar_FlowTable10Sta:
	.space	4
	.section	.bss.GuiVar_FlowCheckingStable,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingStable, %object
	.size	GuiVar_FlowCheckingStable, 4
GuiVar_FlowCheckingStable:
	.space	4
	.section	.bss.GuiVar_StatusCycleLeft,"aw",%nobits
	.type	GuiVar_StatusCycleLeft, %object
	.size	GuiVar_StatusCycleLeft, 65
GuiVar_StatusCycleLeft:
	.space	65
	.section	.bss.GuiVar_MoisBoxIndex,"aw",%nobits
	.align	2
	.type	GuiVar_MoisBoxIndex, %object
	.size	GuiVar_MoisBoxIndex, 4
GuiVar_MoisBoxIndex:
	.space	4
	.section	.bss.GuiVar_StationGroupKc1,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc1, %object
	.size	GuiVar_StationGroupKc1, 4
GuiVar_StationGroupKc1:
	.space	4
	.section	.bss.GuiVar_StationGroupKc2,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc2, %object
	.size	GuiVar_StationGroupKc2, 4
GuiVar_StationGroupKc2:
	.space	4
	.section	.bss.GuiVar_StationGroupKc3,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc3, %object
	.size	GuiVar_StationGroupKc3, 4
GuiVar_StationGroupKc3:
	.space	4
	.section	.bss.GuiVar_StationGroupKc4,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc4, %object
	.size	GuiVar_StationGroupKc4, 4
GuiVar_StationGroupKc4:
	.space	4
	.section	.bss.GuiVar_LightCalendarWeek2Mon,"aw",%nobits
	.type	GuiVar_LightCalendarWeek2Mon, %object
	.size	GuiVar_LightCalendarWeek2Mon, 3
GuiVar_LightCalendarWeek2Mon:
	.space	3
	.section	.bss.GuiVar_StationGroupKc6,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc6, %object
	.size	GuiVar_StationGroupKc6, 4
GuiVar_StationGroupKc6:
	.space	4
	.section	.bss.GuiVar_StationGroupKc7,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc7, %object
	.size	GuiVar_StationGroupKc7, 4
GuiVar_StationGroupKc7:
	.space	4
	.section	.bss.GuiVar_WENWPAEncryption,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEncryption, %object
	.size	GuiVar_WENWPAEncryption, 4
GuiVar_WENWPAEncryption:
	.space	4
	.section	.bss.GuiVar_StationGroupKc9,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupKc9, %object
	.size	GuiVar_StationGroupKc9, 4
GuiVar_StationGroupKc9:
	.space	4
	.section	.bss.GuiVar_RptIrrigGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptIrrigGal, %object
	.size	GuiVar_RptIrrigGal, 8
GuiVar_RptIrrigGal:
	.space	8
	.section	.bss.GuiVar_LRGroup,"aw",%nobits
	.align	2
	.type	GuiVar_LRGroup, %object
	.size	GuiVar_LRGroup, 4
GuiVar_LRGroup:
	.space	4
	.section	.bss.GuiVar_HistoricalET11,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET11, %object
	.size	GuiVar_HistoricalET11, 4
GuiVar_HistoricalET11:
	.space	4
	.section	.bss.GuiVar_HistoricalET12,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET12, %object
	.size	GuiVar_HistoricalET12, 4
GuiVar_HistoricalET12:
	.space	4
	.section	.bss.GuiVar_GRNetworkState,"aw",%nobits
	.type	GuiVar_GRNetworkState, %object
	.size	GuiVar_GRNetworkState, 49
GuiVar_GRNetworkState:
	.space	49
	.section	.bss.GuiVar_LightScheduleState,"aw",%nobits
	.align	2
	.type	GuiVar_LightScheduleState, %object
	.size	GuiVar_LightScheduleState, 4
GuiVar_LightScheduleState:
	.space	4
	.section	.bss.GuiVar_RadioTestInProgress,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestInProgress, %object
	.size	GuiVar_RadioTestInProgress, 4
GuiVar_RadioTestInProgress:
	.space	4
	.section	.bss.GuiVar_SRSerialNumber,"aw",%nobits
	.type	GuiVar_SRSerialNumber, %object
	.size	GuiVar_SRSerialNumber, 9
GuiVar_SRSerialNumber:
	.space	9
	.section	.bss.GuiVar_TwoWireDescA,"aw",%nobits
	.type	GuiVar_TwoWireDescA, %object
	.size	GuiVar_TwoWireDescA, 49
GuiVar_TwoWireDescA:
	.space	49
	.section	.bss.GuiVar_TwoWireDescB,"aw",%nobits
	.type	GuiVar_TwoWireDescB, %object
	.size	GuiVar_TwoWireDescB, 49
GuiVar_TwoWireDescB:
	.space	49
	.section	.bss.GuiVar_WalkThruOrder,"aw",%nobits
	.align	2
	.type	GuiVar_WalkThruOrder, %object
	.size	GuiVar_WalkThruOrder, 4
GuiVar_WalkThruOrder:
	.space	4
	.section	.bss.GuiVar_StatusWindPaused,"aw",%nobits
	.align	2
	.type	GuiVar_StatusWindPaused, %object
	.size	GuiVar_StatusWindPaused, 4
GuiVar_StatusWindPaused:
	.space	4
	.section	.bss.GuiVar_LRTemperature,"aw",%nobits
	.align	2
	.type	GuiVar_LRTemperature, %object
	.size	GuiVar_LRTemperature, 4
GuiVar_LRTemperature:
	.space	4
	.section	.bss.GuiVar_BudgetReductionLimit_1,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_1, %object
	.size	GuiVar_BudgetReductionLimit_1, 4
GuiVar_BudgetReductionLimit_1:
	.space	4
	.section	.bss.GuiVar_BudgetReductionLimit_2,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_2, %object
	.size	GuiVar_BudgetReductionLimit_2, 4
GuiVar_BudgetReductionLimit_2:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxSolCtrlMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxSolCtrlMsgs, %object
	.size	GuiVar_TwoWireStatsRxSolCtrlMsgs, 4
GuiVar_TwoWireStatsRxSolCtrlMsgs:
	.space	4
	.section	.bss.GuiVar_BudgetReductionLimit_4,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_4, %object
	.size	GuiVar_BudgetReductionLimit_4, 4
GuiVar_BudgetReductionLimit_4:
	.space	4
	.section	.bss.GuiVar_BudgetReductionLimit_5,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_5, %object
	.size	GuiVar_BudgetReductionLimit_5, 4
GuiVar_BudgetReductionLimit_5:
	.space	4
	.section	.bss.GuiVar_HistoricalETState,"aw",%nobits
	.type	GuiVar_HistoricalETState, %object
	.size	GuiVar_HistoricalETState, 25
GuiVar_HistoricalETState:
	.space	25
	.section	.bss.GuiVar_BudgetReductionLimit_7,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_7, %object
	.size	GuiVar_BudgetReductionLimit_7, 4
GuiVar_BudgetReductionLimit_7:
	.space	4
	.section	.bss.GuiVar_ManualPWaterDay_Thu,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Thu, %object
	.size	GuiVar_ManualPWaterDay_Thu, 4
GuiVar_ManualPWaterDay_Thu:
	.space	4
	.section	.bss.GuiVar_NumericKeypadValue,"aw",%nobits
	.type	GuiVar_NumericKeypadValue, %object
	.size	GuiVar_NumericKeypadValue, 11
GuiVar_NumericKeypadValue:
	.space	11
	.section	.bss.GuiVar_BudgetUnitGallon,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetUnitGallon, %object
	.size	GuiVar_BudgetUnitGallon, 4
GuiVar_BudgetUnitGallon:
	.space	4
	.section	.bss.GuiVar_IrriCommChainConfigCurrentSendFromTP,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigCurrentSendFromTP, %object
	.size	GuiVar_IrriCommChainConfigCurrentSendFromTP, 4
GuiVar_IrriCommChainConfigCurrentSendFromTP:
	.space	4
	.section	.bss.GuiVar_ETRainTableShutdown,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableShutdown, %object
	.size	GuiVar_ETRainTableShutdown, 4
GuiVar_ETRainTableShutdown:
	.space	4
	.section	.bss.GuiVar_POCBudget10,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget10, %object
	.size	GuiVar_POCBudget10, 4
GuiVar_POCBudget10:
	.space	4
	.section	.bss.GuiVar_POCBudget11,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget11, %object
	.size	GuiVar_POCBudget11, 4
GuiVar_POCBudget11:
	.space	4
	.section	.bss.GuiVar_POCBudget12,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudget12, %object
	.size	GuiVar_POCBudget12, 4
GuiVar_POCBudget12:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseB,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseB, %object
	.size	GuiVar_ChainStatsInUseB, 4
GuiVar_ChainStatsInUseB:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseC,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseC, %object
	.size	GuiVar_ChainStatsInUseC, 4
GuiVar_ChainStatsInUseC:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseD,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseD, %object
	.size	GuiVar_ChainStatsInUseD, 4
GuiVar_ChainStatsInUseD:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseE,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseE, %object
	.size	GuiVar_ChainStatsInUseE, 4
GuiVar_ChainStatsInUseE:
	.space	4
	.section	.bss.GuiVar_WENKey,"aw",%nobits
	.type	GuiVar_WENKey, %object
	.size	GuiVar_WENKey, 65
GuiVar_WENKey:
	.space	65
	.section	.bss.GuiVar_StationInfoTotalMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoTotalMinutes, %object
	.size	GuiVar_StationInfoTotalMinutes, 4
GuiVar_StationInfoTotalMinutes:
	.space	4
	.section	.bss.GuiVar_MainMenuStationsExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuStationsExist, %object
	.size	GuiVar_MainMenuStationsExist, 4
GuiVar_MainMenuStationsExist:
	.space	4
	.section	.bss.GuiVar_IrriDetailsProgLeft,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsProgLeft, %object
	.size	GuiVar_IrriDetailsProgLeft, 4
GuiVar_IrriDetailsProgLeft:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseJ,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseJ, %object
	.size	GuiVar_ChainStatsInUseJ, 4
GuiVar_ChainStatsInUseJ:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsTempMax,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsTempMax, %object
	.size	GuiVar_TwoWireStatsTempMax, 4
GuiVar_TwoWireStatsTempMax:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseL,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseL, %object
	.size	GuiVar_ChainStatsInUseL, 4
GuiVar_ChainStatsInUseL:
	.space	4
	.section	.bss.GuiVar_GroupSettingC_2,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_2, %object
	.size	GuiVar_GroupSettingC_2, 4
GuiVar_GroupSettingC_2:
	.space	4
	.section	.bss.GuiVar_AboutTPDashMTerminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashMTerminal, %object
	.size	GuiVar_AboutTPDashMTerminal, 4
GuiVar_AboutTPDashMTerminal:
	.space	4
	.section	.bss.GuiVar_MainMenuPOCsExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuPOCsExist, %object
	.size	GuiVar_MainMenuPOCsExist, 4
GuiVar_MainMenuPOCsExist:
	.space	4
	.section	.bss.GuiVar_LightsBoxIndex_0,"aw",%nobits
	.align	2
	.type	GuiVar_LightsBoxIndex_0, %object
	.size	GuiVar_LightsBoxIndex_0, 4
GuiVar_LightsBoxIndex_0:
	.space	4
	.section	.bss.GuiVar_NoWaterDaysProg,"aw",%nobits
	.type	GuiVar_NoWaterDaysProg, %object
	.size	GuiVar_NoWaterDaysProg, 49
GuiVar_NoWaterDaysProg:
	.space	49
	.section	.bss.GuiVar_FlowCheckingMVJOTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingMVJOTime, %object
	.size	GuiVar_FlowCheckingMVJOTime, 4
GuiVar_FlowCheckingMVJOTime:
	.space	4
	.section	.bss.GuiVar_RptDecoderOutput,"aw",%nobits
	.align	2
	.type	GuiVar_RptDecoderOutput, %object
	.size	GuiVar_RptDecoderOutput, 4
GuiVar_RptDecoderOutput:
	.space	4
	.section	.bss.GuiVar_CodeDownloadProgressBar,"aw",%nobits
	.align	2
	.type	GuiVar_CodeDownloadProgressBar, %object
	.size	GuiVar_CodeDownloadProgressBar, 4
GuiVar_CodeDownloadProgressBar:
	.space	4
	.section	.bss.GuiVar_ManualWaterProg,"aw",%nobits
	.type	GuiVar_ManualWaterProg, %object
	.size	GuiVar_ManualWaterProg, 49
GuiVar_ManualWaterProg:
	.space	49
	.section	.bss.GuiVar_ManualPStationRunTimeControllerName,"aw",%nobits
	.type	GuiVar_ManualPStationRunTimeControllerName, %object
	.size	GuiVar_ManualPStationRunTimeControllerName, 49
GuiVar_ManualPStationRunTimeControllerName:
	.space	49
	.section	.bss.GuiVar_AboutHubOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutHubOption, %object
	.size	GuiVar_AboutHubOption, 4
GuiVar_AboutHubOption:
	.space	4
	.section	.bss.GuiVar_BudgetFlowTypeNonController,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetFlowTypeNonController, %object
	.size	GuiVar_BudgetFlowTypeNonController, 4
GuiVar_BudgetFlowTypeNonController:
	.space	4
	.section	.bss.GuiVar_RptDecoderSN,"aw",%nobits
	.align	2
	.type	GuiVar_RptDecoderSN, %object
	.size	GuiVar_RptDecoderSN, 4
GuiVar_RptDecoderSN:
	.space	4
	.section	.bss.GuiVar_ScheduleWaterDay_Sat,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Sat, %object
	.size	GuiVar_ScheduleWaterDay_Sat, 4
GuiVar_ScheduleWaterDay_Sat:
	.space	4
	.section	.bss.GuiVar_LiveScreensBypassFlowLevel,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowLevel, %object
	.size	GuiVar_LiveScreensBypassFlowLevel, 4
GuiVar_LiveScreensBypassFlowLevel:
	.space	4
	.section	.bss.GuiVar_FlowCheckingGroupCnt0,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingGroupCnt0, %object
	.size	GuiVar_FlowCheckingGroupCnt0, 4
GuiVar_FlowCheckingGroupCnt0:
	.space	4
	.section	.bss.GuiVar_FlowCheckingGroupCnt1,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingGroupCnt1, %object
	.size	GuiVar_FlowCheckingGroupCnt1, 4
GuiVar_FlowCheckingGroupCnt1:
	.space	4
	.section	.bss.GuiVar_FlowCheckingGroupCnt2,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingGroupCnt2, %object
	.size	GuiVar_FlowCheckingGroupCnt2, 4
GuiVar_FlowCheckingGroupCnt2:
	.space	4
	.section	.bss.GuiVar_SRRepeater,"aw",%nobits
	.align	2
	.type	GuiVar_SRRepeater, %object
	.size	GuiVar_SRRepeater, 4
GuiVar_SRRepeater:
	.space	4
	.section	.bss.GuiVar_AboutTPSta09_16Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta09_16Card, %object
	.size	GuiVar_AboutTPSta09_16Card, 4
GuiVar_AboutTPSta09_16Card:
	.space	4
	.section	.bss.GuiVar_WENRSSI,"aw",%nobits
	.align	2
	.type	GuiVar_WENRSSI, %object
	.size	GuiVar_WENRSSI, 4
GuiVar_WENRSSI:
	.space	4
	.section	.bss.GuiVar_FLControllerIndex_0,"aw",%nobits
	.align	2
	.type	GuiVar_FLControllerIndex_0, %object
	.size	GuiVar_FLControllerIndex_0, 4
GuiVar_FLControllerIndex_0:
	.space	4
	.section	.bss.GuiVar_SerialPortRcvdTP,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortRcvdTP, %object
	.size	GuiVar_SerialPortRcvdTP, 4
GuiVar_SerialPortRcvdTP:
	.space	4
	.section	.bss.GuiVar_CommTestStatus,"aw",%nobits
	.type	GuiVar_CommTestStatus, %object
	.size	GuiVar_CommTestStatus, 49
GuiVar_CommTestStatus:
	.space	49
	.section	.bss.GuiVar_MoisControlMode,"aw",%nobits
	.align	2
	.type	GuiVar_MoisControlMode, %object
	.size	GuiVar_MoisControlMode, 4
GuiVar_MoisControlMode:
	.space	4
	.section	.bss.GuiVar_ContactStatusAlive,"aw",%nobits
	.align	2
	.type	GuiVar_ContactStatusAlive, %object
	.size	GuiVar_ContactStatusAlive, 4
GuiVar_ContactStatusAlive:
	.space	4
	.section	.bss.GuiVar_LRBeaconRate,"aw",%nobits
	.align	2
	.type	GuiVar_LRBeaconRate, %object
	.size	GuiVar_LRBeaconRate, 4
GuiVar_LRBeaconRate:
	.space	4
	.section	.bss.GuiVar_POCBypassStages,"aw",%nobits
	.align	2
	.type	GuiVar_POCBypassStages, %object
	.size	GuiVar_POCBypassStages, 4
GuiVar_POCBypassStages:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxStatReqMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxStatReqMsgs, %object
	.size	GuiVar_TwoWireStatsRxStatReqMsgs, 4
GuiVar_TwoWireStatsRxStatReqMsgs:
	.space	4
	.section	.bss.GuiVar_HistoricalET5,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET5, %object
	.size	GuiVar_HistoricalET5, 4
GuiVar_HistoricalET5:
	.space	4
	.section	.bss.GuiVar_GRSIMState,"aw",%nobits
	.type	GuiVar_GRSIMState, %object
	.size	GuiVar_GRSIMState, 49
GuiVar_GRSIMState:
	.space	49
	.section	.bss.GuiVar_ManualPEndDateStr,"aw",%nobits
	.type	GuiVar_ManualPEndDateStr, %object
	.size	GuiVar_ManualPEndDateStr, 49
GuiVar_ManualPEndDateStr:
	.space	49
	.section	.bss.GuiVar_TwoWireStatsRxLongMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxLongMsgs, %object
	.size	GuiVar_TwoWireStatsRxLongMsgs, 4
GuiVar_TwoWireStatsRxLongMsgs:
	.space	4
	.section	.bss.GuiVar_RptWalkThruGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptWalkThruGal, %object
	.size	GuiVar_RptWalkThruGal, 8
GuiVar_RptWalkThruGal:
	.space	8
	.section	.bss.GuiVar_StatusSystemFlowActual,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowActual, %object
	.size	GuiVar_StatusSystemFlowActual, 4
GuiVar_StatusSystemFlowActual:
	.space	4
	.section	.bss.GuiVar_StatusIrrigationActivityState,"aw",%nobits
	.align	2
	.type	GuiVar_StatusIrrigationActivityState, %object
	.size	GuiVar_StatusIrrigationActivityState, 4
GuiVar_StatusIrrigationActivityState:
	.space	4
	.section	.bss.GuiVar_GRModel,"aw",%nobits
	.type	GuiVar_GRModel, %object
	.size	GuiVar_GRModel, 49
GuiVar_GRModel:
	.space	49
	.section	.bss.GuiVar_RainBucketInstalledAt,"aw",%nobits
	.type	GuiVar_RainBucketInstalledAt, %object
	.size	GuiVar_RainBucketInstalledAt, 3
GuiVar_RainBucketInstalledAt:
	.space	3
	.section	.bss.GuiVar_GRFirmwareVersion,"aw",%nobits
	.type	GuiVar_GRFirmwareVersion, %object
	.size	GuiVar_GRFirmwareVersion, 49
GuiVar_GRFirmwareVersion:
	.space	49
	.section	.bss.GuiVar_ManualPRunTime,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPRunTime, %object
	.size	GuiVar_ManualPRunTime, 4
GuiVar_ManualPRunTime:
	.space	4
	.section	.bss.GuiVar_FlowCheckingLFTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingLFTime, %object
	.size	GuiVar_FlowCheckingLFTime, 4
GuiVar_FlowCheckingLFTime:
	.space	4
	.section	.bss.GuiVar_MainMenuMainLinesAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuMainLinesAvailable, %object
	.size	GuiVar_MainMenuMainLinesAvailable, 4
GuiVar_MainMenuMainLinesAvailable:
	.space	4
	.section	.bss.GuiVar_SRFirmwareVer,"aw",%nobits
	.type	GuiVar_SRFirmwareVer, %object
	.size	GuiVar_SRFirmwareVer, 49
GuiVar_SRFirmwareVer:
	.space	49
	.section	.bss.GuiVar_WalkThruControllerName,"aw",%nobits
	.type	GuiVar_WalkThruControllerName, %object
	.size	GuiVar_WalkThruControllerName, 49
GuiVar_WalkThruControllerName:
	.space	49
	.section	.bss.GuiVar_BudgetReductionLimit_8,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_8, %object
	.size	GuiVar_BudgetReductionLimit_8, 4
GuiVar_BudgetReductionLimit_8:
	.space	4
	.section	.bss.GuiVar_AccessControlUser,"aw",%nobits
	.type	GuiVar_AccessControlUser, %object
	.size	GuiVar_AccessControlUser, 49
GuiVar_AccessControlUser:
	.space	49
	.section	.bss.GuiVar_ManualPStartTime2Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime2Enabled, %object
	.size	GuiVar_ManualPStartTime2Enabled, 4
GuiVar_ManualPStartTime2Enabled:
	.space	4
	.section	.bss.GuiVar_WalkThruStation_Str,"aw",%nobits
	.type	GuiVar_WalkThruStation_Str, %object
	.size	GuiVar_WalkThruStation_Str, 4
GuiVar_WalkThruStation_Str:
	.space	4
	.section	.bss.GuiVar_GRRSSI,"aw",%nobits
	.type	GuiVar_GRRSSI, %object
	.size	GuiVar_GRRSSI, 49
GuiVar_GRRSSI:
	.space	49
	.section	.bss.GuiVar_AboutTPMicroCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPMicroCard, %object
	.size	GuiVar_AboutTPMicroCard, 4
GuiVar_AboutTPMicroCard:
	.space	4
	.section	.bss.GuiVar_MVORInEffect,"aw",%nobits
	.align	2
	.type	GuiVar_MVORInEffect, %object
	.size	GuiVar_MVORInEffect, 4
GuiVar_MVORInEffect:
	.space	4
	.section	.bss.GuiVar_BudgetPeriod_1,"aw",%nobits
	.type	GuiVar_BudgetPeriod_1, %object
	.size	GuiVar_BudgetPeriod_1, 17
GuiVar_BudgetPeriod_1:
	.space	17
	.section	.bss.GuiVar_StationGroupSoilIntakeRate,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSoilIntakeRate, %object
	.size	GuiVar_StationGroupSoilIntakeRate, 4
GuiVar_StationGroupSoilIntakeRate:
	.space	4
	.section	.bss.GuiVar_ChainStatsPresentlyMakingTokens,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsPresentlyMakingTokens, %object
	.size	GuiVar_ChainStatsPresentlyMakingTokens, 4
GuiVar_ChainStatsPresentlyMakingTokens:
	.space	4
	.section	.bss.GuiVar_ManualPWaterDay_Fri,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Fri, %object
	.size	GuiVar_ManualPWaterDay_Fri, 4
GuiVar_ManualPWaterDay_Fri:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsEEPCRCS1Params,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsEEPCRCS1Params, %object
	.size	GuiVar_TwoWireStatsEEPCRCS1Params, 4
GuiVar_TwoWireStatsEEPCRCS1Params:
	.space	4
	.section	.bss.GuiVar_POCPreservesDelivered5SecAvg1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDelivered5SecAvg1, %object
	.size	GuiVar_POCPreservesDelivered5SecAvg1, 4
GuiVar_POCPreservesDelivered5SecAvg1:
	.space	4
	.section	.bss.GuiVar_POCPreservesDelivered5SecAvg2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDelivered5SecAvg2, %object
	.size	GuiVar_POCPreservesDelivered5SecAvg2, 4
GuiVar_POCPreservesDelivered5SecAvg2:
	.space	4
	.section	.bss.GuiVar_POCPreservesDelivered5SecAvg3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDelivered5SecAvg3, %object
	.size	GuiVar_POCPreservesDelivered5SecAvg3, 4
GuiVar_POCPreservesDelivered5SecAvg3:
	.space	4
	.section	.bss.GuiVar_ETRainTableDate,"aw",%nobits
	.type	GuiVar_ETRainTableDate, %object
	.size	GuiVar_ETRainTableDate, 49
GuiVar_ETRainTableDate:
	.space	49
	.section	.bss.GuiVar_MoisInUse,"aw",%nobits
	.align	2
	.type	GuiVar_MoisInUse, %object
	.size	GuiVar_MoisInUse, 4
GuiVar_MoisInUse:
	.space	4
	.section	.bss.GuiVar_WENWPAEAPTTLSOption,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEAPTTLSOption, %object
	.size	GuiVar_WENWPAEAPTTLSOption, 4
GuiVar_WENWPAEAPTTLSOption:
	.space	4
	.section	.bss.GuiVar_AboutMOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutMOption, %object
	.size	GuiVar_AboutMOption, 4
GuiVar_AboutMOption:
	.space	4
	.section	.bss.GuiVar_AboutTPDashLCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashLCard, %object
	.size	GuiVar_AboutTPDashLCard, 4
GuiVar_AboutTPDashLCard:
	.space	4
	.section	.bss.GuiVar_ManualPWaterDay_Sun,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Sun, %object
	.size	GuiVar_ManualPWaterDay_Sun, 4
GuiVar_ManualPWaterDay_Sun:
	.space	4
	.section	.bss.GuiVar_LightsStopTime2InMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStopTime2InMinutes, %object
	.size	GuiVar_LightsStopTime2InMinutes, 4
GuiVar_LightsStopTime2InMinutes:
	.space	4
	.section	.bss.GuiVar_FlowCheckingPumpState,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingPumpState, %object
	.size	GuiVar_FlowCheckingPumpState, 4
GuiVar_FlowCheckingPumpState:
	.space	4
	.section	.bss.GuiVar_FlowCheckingGroupCnt3,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingGroupCnt3, %object
	.size	GuiVar_FlowCheckingGroupCnt3, 4
GuiVar_FlowCheckingGroupCnt3:
	.space	4
	.section	.bss.GuiVar_StatusRainSwitchConnected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusRainSwitchConnected, %object
	.size	GuiVar_StatusRainSwitchConnected, 4
GuiVar_StatusRainSwitchConnected:
	.space	4
	.section	.bss.GuiVar_LightCalendarWeek1Tue,"aw",%nobits
	.type	GuiVar_LightCalendarWeek1Tue, %object
	.size	GuiVar_LightCalendarWeek1Tue, 3
GuiVar_LightCalendarWeek1Tue:
	.space	3
	.section	.bss.GuiVar_RptTestGal,"aw",%nobits
	.align	2
	.type	GuiVar_RptTestGal, %object
	.size	GuiVar_RptTestGal, 8
GuiVar_RptTestGal:
	.space	8
	.section	.bss.GuiVar_StatusMVORInEffect,"aw",%nobits
	.align	2
	.type	GuiVar_StatusMVORInEffect, %object
	.size	GuiVar_StatusMVORInEffect, 4
GuiVar_StatusMVORInEffect:
	.space	4
	.section	.bss.GuiVar_StationSelectionGridStationCount,"aw",%nobits
	.align	2
	.type	GuiVar_StationSelectionGridStationCount, %object
	.size	GuiVar_StationSelectionGridStationCount, 4
GuiVar_StationSelectionGridStationCount:
	.space	4
	.section	.bss.GuiVar_RptNonCMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptNonCMin, %object
	.size	GuiVar_RptNonCMin, 4
GuiVar_RptNonCMin:
	.space	4
	.section	.bss.GuiVar_GRMake,"aw",%nobits
	.type	GuiVar_GRMake, %object
	.size	GuiVar_GRMake, 49
GuiVar_GRMake:
	.space	49
	.section	.bss.GuiVar_MoisEC_Current,"aw",%nobits
	.align	2
	.type	GuiVar_MoisEC_Current, %object
	.size	GuiVar_MoisEC_Current, 4
GuiVar_MoisEC_Current:
	.space	4
	.section	.bss.GuiVar_LiveScreensCommFLCommMngrMode,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommFLCommMngrMode, %object
	.size	GuiVar_LiveScreensCommFLCommMngrMode, 4
GuiVar_LiveScreensCommFLCommMngrMode:
	.space	4
	.section	.bss.GuiVar_AboutTPPOCCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPPOCCard, %object
	.size	GuiVar_AboutTPPOCCard, 4
GuiVar_AboutTPPOCCard:
	.space	4
	.section	.bss.GuiVar_MoisPhysicallyAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_MoisPhysicallyAvailable, %object
	.size	GuiVar_MoisPhysicallyAvailable, 4
GuiVar_MoisPhysicallyAvailable:
	.space	4
	.section	.bss.GuiVar_StatusETGageConnected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusETGageConnected, %object
	.size	GuiVar_StatusETGageConnected, 4
GuiVar_StatusETGageConnected:
	.space	4
	.section	.bss.GuiVar_ScheduleWaterDay_Fri,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Fri, %object
	.size	GuiVar_ScheduleWaterDay_Fri, 4
GuiVar_ScheduleWaterDay_Fri:
	.space	4
	.section	.bss.GuiVar_itmStationNumber,"aw",%nobits
	.type	GuiVar_itmStationNumber, %object
	.size	GuiVar_itmStationNumber, 4
GuiVar_itmStationNumber:
	.space	4
	.section	.bss.GuiVar_SerialPortRcvdA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortRcvdA, %object
	.size	GuiVar_SerialPortRcvdA, 4
GuiVar_SerialPortRcvdA:
	.space	4
	.section	.bss.GuiVar_StationGroupSlopePercentage,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSlopePercentage, %object
	.size	GuiVar_StationGroupSlopePercentage, 4
GuiVar_StationGroupSlopePercentage:
	.space	4
	.section	.bss.GuiVar_DateTimeMin,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeMin, %object
	.size	GuiVar_DateTimeMin, 4
GuiVar_DateTimeMin:
	.space	4
	.section	.bss.GuiVar_TwoWireStaStrA,"aw",%nobits
	.type	GuiVar_TwoWireStaStrA, %object
	.size	GuiVar_TwoWireStaStrA, 5
GuiVar_TwoWireStaStrA:
	.space	5
	.section	.bss.GuiVar_SerialPortRcvdB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortRcvdB, %object
	.size	GuiVar_SerialPortRcvdB, 4
GuiVar_SerialPortRcvdB:
	.space	4
	.section	.bss.GuiVar_DeviceExchangeProgressBar,"aw",%nobits
	.align	2
	.type	GuiVar_DeviceExchangeProgressBar, %object
	.size	GuiVar_DeviceExchangeProgressBar, 4
GuiVar_DeviceExchangeProgressBar:
	.space	4
	.section	.bss.GuiVar_SystemPreservesStabilityMostRecent5Sec,"aw",%nobits
	.align	2
	.type	GuiVar_SystemPreservesStabilityMostRecent5Sec, %object
	.size	GuiVar_SystemPreservesStabilityMostRecent5Sec, 4
GuiVar_SystemPreservesStabilityMostRecent5Sec:
	.space	4
	.section	.bss.GuiVar_StatusFuseBlown,"aw",%nobits
	.align	2
	.type	GuiVar_StatusFuseBlown, %object
	.size	GuiVar_StatusFuseBlown, 4
GuiVar_StatusFuseBlown:
	.space	4
	.section	.bss.GuiVar_WENStatus,"aw",%nobits
	.type	GuiVar_WENStatus, %object
	.size	GuiVar_WENStatus, 49
GuiVar_WENStatus:
	.space	49
	.section	.bss.GuiVar_WENWPAAuthentication,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAAuthentication, %object
	.size	GuiVar_WENWPAAuthentication, 4
GuiVar_WENWPAAuthentication:
	.space	4
	.section	.bss.GuiVar_StatusRainSwitchState,"aw",%nobits
	.align	2
	.type	GuiVar_StatusRainSwitchState, %object
	.size	GuiVar_StatusRainSwitchState, 4
GuiVar_StatusRainSwitchState:
	.space	4
	.section	.bss.GuiVar_ScheduleNoStationsInGroup,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleNoStationsInGroup, %object
	.size	GuiVar_ScheduleNoStationsInGroup, 4
GuiVar_ScheduleNoStationsInGroup:
	.space	4
	.section	.bss.GuiVar_OnAtATimeInfoDisplay,"aw",%nobits
	.type	GuiVar_OnAtATimeInfoDisplay, %object
	.size	GuiVar_OnAtATimeInfoDisplay, 1
GuiVar_OnAtATimeInfoDisplay:
	.space	1
	.section	.bss.GuiVar_FlowCheckingMVState,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingMVState, %object
	.size	GuiVar_FlowCheckingMVState, 4
GuiVar_FlowCheckingMVState:
	.space	4
	.section	.bss.GuiVar_LightsStartTimeEnabled1,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStartTimeEnabled1, %object
	.size	GuiVar_LightsStartTimeEnabled1, 4
GuiVar_LightsStartTimeEnabled1:
	.space	4
	.section	.bss.GuiVar_POCReedSwitchType1,"aw",%nobits
	.align	2
	.type	GuiVar_POCReedSwitchType1, %object
	.size	GuiVar_POCReedSwitchType1, 4
GuiVar_POCReedSwitchType1:
	.space	4
	.section	.bss.GuiVar_POCReedSwitchType2,"aw",%nobits
	.align	2
	.type	GuiVar_POCReedSwitchType2, %object
	.size	GuiVar_POCReedSwitchType2, 4
GuiVar_POCReedSwitchType2:
	.space	4
	.section	.bss.GuiVar_POCReedSwitchType3,"aw",%nobits
	.align	2
	.type	GuiVar_POCReedSwitchType3, %object
	.size	GuiVar_POCReedSwitchType3, 4
GuiVar_POCReedSwitchType3:
	.space	4
	.section	.bss.GuiVar_GroupSettingC_3,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_3, %object
	.size	GuiVar_GroupSettingC_3, 4
GuiVar_GroupSettingC_3:
	.space	4
	.section	.bss.GuiVar_GroupSettingC_4,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_4, %object
	.size	GuiVar_GroupSettingC_4, 4
GuiVar_GroupSettingC_4:
	.space	4
	.section	.bss.GuiVar_TwoWireDecoderSN,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDecoderSN, %object
	.size	GuiVar_TwoWireDecoderSN, 4
GuiVar_TwoWireDecoderSN:
	.space	4
	.section	.bss.GuiVar_GroupSettingC_6,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_6, %object
	.size	GuiVar_GroupSettingC_6, 4
GuiVar_GroupSettingC_6:
	.space	4
	.section	.bss.GuiVar_GroupSettingC_7,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_7, %object
	.size	GuiVar_GroupSettingC_7, 4
GuiVar_GroupSettingC_7:
	.space	4
	.section	.bss.GuiVar_GroupSettingC_8,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_8, %object
	.size	GuiVar_GroupSettingC_8, 4
GuiVar_GroupSettingC_8:
	.space	4
	.section	.bss.GuiVar_GroupSettingC_9,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingC_9, %object
	.size	GuiVar_GroupSettingC_9, 4
GuiVar_GroupSettingC_9:
	.space	4
	.section	.bss.GuiVar_StationCopyTextOffset_Left,"aw",%nobits
	.align	2
	.type	GuiVar_StationCopyTextOffset_Left, %object
	.size	GuiVar_StationCopyTextOffset_Left, 4
GuiVar_StationCopyTextOffset_Left:
	.space	4
	.section	.bss.GuiVar_ComboBox_Y1,"aw",%nobits
	.align	2
	.type	GuiVar_ComboBox_Y1, %object
	.size	GuiVar_ComboBox_Y1, 4
GuiVar_ComboBox_Y1:
	.space	4
	.section	.bss.GuiVar_BudgetUseThisPeriod,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetUseThisPeriod, %object
	.size	GuiVar_BudgetUseThisPeriod, 4
GuiVar_BudgetUseThisPeriod:
	.space	4
	.section	.bss.GuiVar_FlowCheckingPumpTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingPumpTime, %object
	.size	GuiVar_FlowCheckingPumpTime, 4
GuiVar_FlowCheckingPumpTime:
	.space	4
	.section	.bss.GuiVar_ScheduleWaterDay_Thu,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Thu, %object
	.size	GuiVar_ScheduleWaterDay_Thu, 4
GuiVar_ScheduleWaterDay_Thu:
	.space	4
	.section	.bss.GuiVar_StatusShowSystemFlow,"aw",%nobits
	.align	2
	.type	GuiVar_StatusShowSystemFlow, %object
	.size	GuiVar_StatusShowSystemFlow, 4
GuiVar_StatusShowSystemFlow:
	.space	4
	.section	.bss.GuiVar_MoisTempPoint_Low,"aw",%nobits
	.align	2
	.type	GuiVar_MoisTempPoint_Low, %object
	.size	GuiVar_MoisTempPoint_Low, 4
GuiVar_MoisTempPoint_Low:
	.space	4
	.section	.bss.GuiVar_MVOROpen4,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen4, %object
	.size	GuiVar_MVOROpen4, 4
GuiVar_MVOROpen4:
	.space	4
	.section	.bss.GuiVar_MainMenuWeatherOptionsExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuWeatherOptionsExist, %object
	.size	GuiVar_MainMenuWeatherOptionsExist, 4
GuiVar_MainMenuWeatherOptionsExist:
	.space	4
	.section	.bss.GuiVar_SerialPortXmitA,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmitA, %object
	.size	GuiVar_SerialPortXmitA, 4
GuiVar_SerialPortXmitA:
	.space	4
	.section	.bss.GuiVar_SerialPortXmitB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortXmitB, %object
	.size	GuiVar_SerialPortXmitB, 4
GuiVar_SerialPortXmitB:
	.space	4
	.section	.bss.GuiVar_WENWPAEncryptionCCMP,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEncryptionCCMP, %object
	.size	GuiVar_WENWPAEncryptionCCMP, 4
GuiVar_WENWPAEncryptionCCMP:
	.space	4
	.section	.bss.GuiVar_FinishTimesStationGroup,"aw",%nobits
	.type	GuiVar_FinishTimesStationGroup, %object
	.size	GuiVar_FinishTimesStationGroup, 21
GuiVar_FinishTimesStationGroup:
	.space	21
	.section	.bss.GuiVar_AboutWOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutWOption, %object
	.size	GuiVar_AboutWOption, 4
GuiVar_AboutWOption:
	.space	4
	.section	.bss.GuiVar_UnusedGroupsRemovedGreaterThan1,"aw",%nobits
	.align	2
	.type	GuiVar_UnusedGroupsRemovedGreaterThan1, %object
	.size	GuiVar_UnusedGroupsRemovedGreaterThan1, 4
GuiVar_UnusedGroupsRemovedGreaterThan1:
	.space	4
	.section	.bss.GuiVar_StatusRainBucketConnected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusRainBucketConnected, %object
	.size	GuiVar_StatusRainBucketConnected, 4
GuiVar_StatusRainBucketConnected:
	.space	4
	.section	.bss.GuiVar_TwoWireDiscoveryState,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDiscoveryState, %object
	.size	GuiVar_TwoWireDiscoveryState, 4
GuiVar_TwoWireDiscoveryState:
	.space	4
	.section	.bss.GuiVar_PercentAdjustEndDate_0,"aw",%nobits
	.type	GuiVar_PercentAdjustEndDate_0, %object
	.size	GuiVar_PercentAdjustEndDate_0, 49
GuiVar_PercentAdjustEndDate_0:
	.space	49
	.section	.bss.GuiVar_MoisTemp_Current,"aw",%nobits
	.align	2
	.type	GuiVar_MoisTemp_Current, %object
	.size	GuiVar_MoisTemp_Current, 4
GuiVar_MoisTemp_Current:
	.space	4
	.section	.bss.GuiVar_MVORClose1Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose1Enabled, %object
	.size	GuiVar_MVORClose1Enabled, 4
GuiVar_MVORClose1Enabled:
	.space	4
	.section	.bss.GuiVar_PercentAdjustEndDate_3,"aw",%nobits
	.type	GuiVar_PercentAdjustEndDate_3, %object
	.size	GuiVar_PercentAdjustEndDate_3, 49
GuiVar_PercentAdjustEndDate_3:
	.space	49
	.section	.bss.GuiVar_MoisAdditionalSoakSeconds,"aw",%nobits
	.align	2
	.type	GuiVar_MoisAdditionalSoakSeconds, %object
	.size	GuiVar_MoisAdditionalSoakSeconds, 4
GuiVar_MoisAdditionalSoakSeconds:
	.space	4
	.section	.bss.GuiVar_WENWPAEAPTLSValidateCert,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAEAPTLSValidateCert, %object
	.size	GuiVar_WENWPAEAPTLSValidateCert, 4
GuiVar_WENWPAEAPTLSValidateCert:
	.space	4
	.section	.bss.GuiVar_AlertString,"aw",%nobits
	.type	GuiVar_AlertString, %object
	.size	GuiVar_AlertString, 257
GuiVar_AlertString:
	.space	257
	.section	.bss.GuiVar_CommOptionCloudCommTestAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_CommOptionCloudCommTestAvailable, %object
	.size	GuiVar_CommOptionCloudCommTestAvailable, 4
GuiVar_CommOptionCloudCommTestAvailable:
	.space	4
	.section	.bss.GuiVar_AboutTPSta41_48Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta41_48Card, %object
	.size	GuiVar_AboutTPSta41_48Card, 4
GuiVar_AboutTPSta41_48Card:
	.space	4
	.section	.bss.GuiVar_FlowCheckingPumpMix,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingPumpMix, %object
	.size	GuiVar_FlowCheckingPumpMix, 4
GuiVar_FlowCheckingPumpMix:
	.space	4
	.section	.bss.GuiVar_ContrastSliderPos,"aw",%nobits
	.align	2
	.type	GuiVar_ContrastSliderPos, %object
	.size	GuiVar_ContrastSliderPos, 4
GuiVar_ContrastSliderPos:
	.space	4
	.section	.bss.GuiVar_SpinnerPos,"aw",%nobits
	.align	2
	.type	GuiVar_SpinnerPos, %object
	.size	GuiVar_SpinnerPos, 4
GuiVar_SpinnerPos:
	.space	4
	.section	.bss.GuiVar_RptFlags,"aw",%nobits
	.type	GuiVar_RptFlags, %object
	.size	GuiVar_RptFlags, 65
GuiVar_RptFlags:
	.space	65
	.section	.bss.GuiVar_AboutTPSta17_24Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta17_24Card, %object
	.size	GuiVar_AboutTPSta17_24Card, 4
GuiVar_AboutTPSta17_24Card:
	.space	4
	.section	.bss.GuiVar_MainMenuHubOptionExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuHubOptionExists, %object
	.size	GuiVar_MainMenuHubOptionExists, 4
GuiVar_MainMenuHubOptionExists:
	.space	4
	.section	.bss.GuiVar_CommTestSuppressConnections,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestSuppressConnections, %object
	.size	GuiVar_CommTestSuppressConnections, 4
GuiVar_CommTestSuppressConnections:
	.space	4
	.section	.bss.GuiVar_GroupSettingB_5,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_5, %object
	.size	GuiVar_GroupSettingB_5, 4
GuiVar_GroupSettingB_5:
	.space	4
	.section	.bss.GuiVar_PercentAdjustPercent_3,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_3, %object
	.size	GuiVar_PercentAdjustPercent_3, 4
GuiVar_PercentAdjustPercent_3:
	.space	4
	.section	.bss.GuiVar_GroupSettingB_6,"aw",%nobits
	.align	2
	.type	GuiVar_GroupSettingB_6, %object
	.size	GuiVar_GroupSettingB_6, 4
GuiVar_GroupSettingB_6:
	.space	4
	.section	.bss.GuiVar_PercentAdjustPercent_4,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_4, %object
	.size	GuiVar_PercentAdjustPercent_4, 4
GuiVar_PercentAdjustPercent_4:
	.space	4
	.section	.bss.GuiVar_ScheduleMowDay,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleMowDay, %object
	.size	GuiVar_ScheduleMowDay, 4
GuiVar_ScheduleMowDay:
	.space	4
	.section	.bss.GuiVar_PercentAdjustPercent_5,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_5, %object
	.size	GuiVar_PercentAdjustPercent_5, 4
GuiVar_PercentAdjustPercent_5:
	.space	4
	.section	.bss.GuiVar_PercentAdjustPercent_6,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_6, %object
	.size	GuiVar_PercentAdjustPercent_6, 4
GuiVar_PercentAdjustPercent_6:
	.space	4
	.section	.bss.GuiVar_SRGroup,"aw",%nobits
	.align	2
	.type	GuiVar_SRGroup, %object
	.size	GuiVar_SRGroup, 4
GuiVar_SRGroup:
	.space	4
	.section	.bss.GuiVar_LightCalendarWeek1Fri,"aw",%nobits
	.type	GuiVar_LightCalendarWeek1Fri, %object
	.size	GuiVar_LightCalendarWeek1Fri, 3
GuiVar_LightCalendarWeek1Fri:
	.space	3
	.section	.bss.GuiVar_ENModel,"aw",%nobits
	.type	GuiVar_ENModel, %object
	.size	GuiVar_ENModel, 49
GuiVar_ENModel:
	.space	49
	.section	.bss.GuiVar_PercentAdjustPercent_8,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent_8, %object
	.size	GuiVar_PercentAdjustPercent_8, 4
GuiVar_PercentAdjustPercent_8:
	.space	4
	.section	.bss.GuiVar_FlowCheckingChecked,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingChecked, %object
	.size	GuiVar_FlowCheckingChecked, 4
GuiVar_FlowCheckingChecked:
	.space	4
	.section	.bss.GuiVar_LightCalendarWeek2Thu,"aw",%nobits
	.type	GuiVar_LightCalendarWeek2Thu, %object
	.size	GuiVar_LightCalendarWeek2Thu, 3
GuiVar_LightCalendarWeek2Thu:
	.space	3
	.section	.bss.GuiVar_ETRainTableET,"aw",%nobits
	.align	2
	.type	GuiVar_ETRainTableET, %object
	.size	GuiVar_ETRainTableET, 4
GuiVar_ETRainTableET:
	.space	4
	.section	.bss.GuiVar_AboutSSEDOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutSSEDOption, %object
	.size	GuiVar_AboutSSEDOption, 4
GuiVar_AboutSSEDOption:
	.space	4
	.section	.bss.GuiVar_StationGroupSoilStorageCapacity,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSoilStorageCapacity, %object
	.size	GuiVar_StationGroupSoilStorageCapacity, 4
GuiVar_StationGroupSoilStorageCapacity:
	.space	4
	.section	.bss.GuiVar_SerialPortCDB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortCDB, %object
	.size	GuiVar_SerialPortCDB, 4
GuiVar_SerialPortCDB:
	.space	4
	.section	.bss.GuiVar_ScheduleStartTime,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleStartTime, %object
	.size	GuiVar_ScheduleStartTime, 4
GuiVar_ScheduleStartTime:
	.space	4
	.section	.bss.GuiVar_MoisECAction_High,"aw",%nobits
	.align	2
	.type	GuiVar_MoisECAction_High, %object
	.size	GuiVar_MoisECAction_High, 4
GuiVar_MoisECAction_High:
	.space	4
	.section	.bss.GuiVar_StationSelectionGridMarkerSize,"aw",%nobits
	.align	2
	.type	GuiVar_StationSelectionGridMarkerSize, %object
	.size	GuiVar_StationSelectionGridMarkerSize, 4
GuiVar_StationSelectionGridMarkerSize:
	.space	4
	.section	.bss.GuiVar_BudgetEst,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetEst, %object
	.size	GuiVar_BudgetEst, 4
GuiVar_BudgetEst:
	.space	4
	.section	.bss.GuiVar_MVORClose0Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose0Enabled, %object
	.size	GuiVar_MVORClose0Enabled, 4
GuiVar_MVORClose0Enabled:
	.space	4
	.section	.bss.GuiVar_AboutTPSta33_40Terminal,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta33_40Terminal, %object
	.size	GuiVar_AboutTPSta33_40Terminal, 4
GuiVar_AboutTPSta33_40Terminal:
	.space	4
	.section	.bss.GuiVar_DateTimeYear,"aw",%nobits
	.align	2
	.type	GuiVar_DateTimeYear, %object
	.size	GuiVar_DateTimeYear, 4
GuiVar_DateTimeYear:
	.space	4
	.section	.bss.GuiVar_POCPreservesAccumPulses1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumPulses1, %object
	.size	GuiVar_POCPreservesAccumPulses1, 4
GuiVar_POCPreservesAccumPulses1:
	.space	4
	.section	.bss.GuiVar_POCPreservesAccumPulses2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumPulses2, %object
	.size	GuiVar_POCPreservesAccumPulses2, 4
GuiVar_POCPreservesAccumPulses2:
	.space	4
	.section	.bss.GuiVar_POCPreservesAccumPulses3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesAccumPulses3, %object
	.size	GuiVar_POCPreservesAccumPulses3, 4
GuiVar_POCPreservesAccumPulses3:
	.space	4
	.section	.bss.GuiVar_MVOROpen1Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVOROpen1Enabled, %object
	.size	GuiVar_MVOROpen1Enabled, 4
GuiVar_MVOROpen1Enabled:
	.space	4
	.section	.bss.GuiVar_BudgetPeriodEnd,"aw",%nobits
	.type	GuiVar_BudgetPeriodEnd, %object
	.size	GuiVar_BudgetPeriodEnd, 41
GuiVar_BudgetPeriodEnd:
	.space	41
	.section	.bss.GuiVar_MoisECPoint_Low,"aw",%nobits
	.align	2
	.type	GuiVar_MoisECPoint_Low, %object
	.size	GuiVar_MoisECPoint_Low, 4
GuiVar_MoisECPoint_Low:
	.space	4
	.section	.bss.GuiVar_SerialPortStateB,"aw",%nobits
	.align	2
	.type	GuiVar_SerialPortStateB, %object
	.size	GuiVar_SerialPortStateB, 4
GuiVar_SerialPortStateB:
	.space	4
	.section	.bss.GuiVar_StatusCurrentDraw,"aw",%nobits
	.align	2
	.type	GuiVar_StatusCurrentDraw, %object
	.size	GuiVar_StatusCurrentDraw, 4
GuiVar_StatusCurrentDraw:
	.space	4
	.section	.bss.GuiVar_itmPOC,"aw",%nobits
	.type	GuiVar_itmPOC, %object
	.size	GuiVar_itmPOC, 49
GuiVar_itmPOC:
	.space	49
	.section	.bss.GuiVar_ENDHCPName,"aw",%nobits
	.type	GuiVar_ENDHCPName, %object
	.size	GuiVar_ENDHCPName, 17
GuiVar_ENDHCPName:
	.space	17
	.section	.bss.GuiVar_FlowCheckingMVTime,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingMVTime, %object
	.size	GuiVar_FlowCheckingMVTime, 4
GuiVar_FlowCheckingMVTime:
	.space	4
	.section	.bss.GuiVar_MainMenuBudgetsInUse,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuBudgetsInUse, %object
	.size	GuiVar_MainMenuBudgetsInUse, 4
GuiVar_MainMenuBudgetsInUse:
	.space	4
	.section	.bss.GuiVar_FlowCheckingSlots,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingSlots, %object
	.size	GuiVar_FlowCheckingSlots, 4
GuiVar_FlowCheckingSlots:
	.space	4
	.section	.bss.GuiVar_CommTestIPOctect1,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestIPOctect1, %object
	.size	GuiVar_CommTestIPOctect1, 4
GuiVar_CommTestIPOctect1:
	.space	4
	.section	.bss.GuiVar_MainMenuMoisSensorExists,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuMoisSensorExists, %object
	.size	GuiVar_MainMenuMoisSensorExists, 4
GuiVar_MainMenuMoisSensorExists:
	.space	4
	.section	.bss.GuiVar_FlowCheckingHasNCMV,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingHasNCMV, %object
	.size	GuiVar_FlowCheckingHasNCMV, 4
GuiVar_FlowCheckingHasNCMV:
	.space	4
	.section	.bss.GuiVar_CommTestIPOctect3,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestIPOctect3, %object
	.size	GuiVar_CommTestIPOctect3, 4
GuiVar_CommTestIPOctect3:
	.space	4
	.section	.bss.GuiVar_CommTestIPOctect4,"aw",%nobits
	.align	2
	.type	GuiVar_CommTestIPOctect4, %object
	.size	GuiVar_CommTestIPOctect4, 4
GuiVar_CommTestIPOctect4:
	.space	4
	.section	.bss.GuiVar_ChainStatsFOALIrriTGen,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsFOALIrriTGen, %object
	.size	GuiVar_ChainStatsFOALIrriTGen, 4
GuiVar_ChainStatsFOALIrriTGen:
	.space	4
	.section	.bss.GuiVar_IrriDetailsController,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsController, %object
	.size	GuiVar_IrriDetailsController, 4
GuiVar_IrriDetailsController:
	.space	4
	.section	.bss.GuiVar_TestStationOnOff,"aw",%nobits
	.align	2
	.type	GuiVar_TestStationOnOff, %object
	.size	GuiVar_TestStationOnOff, 4
GuiVar_TestStationOnOff:
	.space	4
	.section	.bss.GuiVar_StationSelectionGridControllerName,"aw",%nobits
	.type	GuiVar_StationSelectionGridControllerName, %object
	.size	GuiVar_StationSelectionGridControllerName, 49
GuiVar_StationSelectionGridControllerName:
	.space	49
	.section	.bss.GuiVar_LiveScreensSystemMLBExists,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensSystemMLBExists, %object
	.size	GuiVar_LiveScreensSystemMLBExists, 4
GuiVar_LiveScreensSystemMLBExists:
	.space	4
	.section	.bss.GuiVar_WENSSID,"aw",%nobits
	.type	GuiVar_WENSSID, %object
	.size	GuiVar_WENSSID, 33
GuiVar_WENSSID:
	.space	33
	.section	.bss.GuiVar_LRRepeaterInUse,"aw",%nobits
	.align	2
	.type	GuiVar_LRRepeaterInUse, %object
	.size	GuiVar_LRRepeaterInUse, 4
GuiVar_LRRepeaterInUse:
	.space	4
	.section	.bss.GuiVar_IrriDetailsReason,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsReason, %object
	.size	GuiVar_IrriDetailsReason, 4
GuiVar_IrriDetailsReason:
	.space	4
	.section	.bss.GuiVar_LiveScreensElectricalControllerName,"aw",%nobits
	.type	GuiVar_LiveScreensElectricalControllerName, %object
	.size	GuiVar_LiveScreensElectricalControllerName, 49
GuiVar_LiveScreensElectricalControllerName:
	.space	49
	.section	.bss.GuiVar_ChainStatsInForced,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInForced, %object
	.size	GuiVar_ChainStatsInForced, 4
GuiVar_ChainStatsInForced:
	.space	4
	.section	.bss.GuiVar_WENSecurity,"aw",%nobits
	.align	2
	.type	GuiVar_WENSecurity, %object
	.size	GuiVar_WENSecurity, 4
GuiVar_WENSecurity:
	.space	4
	.section	.bss.GuiVar_ETGageInstalledAt,"aw",%nobits
	.type	GuiVar_ETGageInstalledAt, %object
	.size	GuiVar_ETGageInstalledAt, 3
GuiVar_ETGageInstalledAt:
	.space	3
	.section	.bss.GuiVar_WENWPA2Encryption,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPA2Encryption, %object
	.size	GuiVar_WENWPA2Encryption, 4
GuiVar_WENWPA2Encryption:
	.space	4
	.section	.bss.GuiVar_MoisECPoint_High,"aw",%nobits
	.align	2
	.type	GuiVar_MoisECPoint_High, %object
	.size	GuiVar_MoisECPoint_High, 4
GuiVar_MoisECPoint_High:
	.space	4
	.section	.bss.GuiVar_ChainStatsIRRIChainRGen,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsIRRIChainRGen, %object
	.size	GuiVar_ChainStatsIRRIChainRGen, 4
GuiVar_ChainStatsIRRIChainRGen:
	.space	4
	.section	.bss.GuiVar_StatusSystemFlowExpected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowExpected, %object
	.size	GuiVar_StatusSystemFlowExpected, 4
GuiVar_StatusSystemFlowExpected:
	.space	4
	.section	.bss.GuiVar_LightStartReason,"aw",%nobits
	.align	2
	.type	GuiVar_LightStartReason, %object
	.size	GuiVar_LightStartReason, 4
GuiVar_LightStartReason:
	.space	4
	.section	.bss.GuiVar_PercentAdjustNumberOfDays,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustNumberOfDays, %object
	.size	GuiVar_PercentAdjustNumberOfDays, 4
GuiVar_PercentAdjustNumberOfDays:
	.space	4
	.section	.bss.GuiVar_POCBudgetYearly,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudgetYearly, %object
	.size	GuiVar_POCBudgetYearly, 4
GuiVar_POCBudgetYearly:
	.space	4
	.section	.bss.GuiVar_BudgetAnnualValue,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetAnnualValue, %object
	.size	GuiVar_BudgetAnnualValue, 4
GuiVar_BudgetAnnualValue:
	.space	4
	.section	.bss.GuiVar_IrriDetailsStatus,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsStatus, %object
	.size	GuiVar_IrriDetailsStatus, 4
GuiVar_IrriDetailsStatus:
	.space	4
	.section	.bss.GuiVar_WENWEPKeySizeChanged,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPKeySizeChanged, %object
	.size	GuiVar_WENWEPKeySizeChanged, 4
GuiVar_WENWEPKeySizeChanged:
	.space	4
	.section	.bss.GuiVar_LightEditableStop,"aw",%nobits
	.type	GuiVar_LightEditableStop, %object
	.size	GuiVar_LightEditableStop, 33
GuiVar_LightEditableStop:
	.space	33
	.section	.bss.GuiVar_ENIPAddress_1,"aw",%nobits
	.align	2
	.type	GuiVar_ENIPAddress_1, %object
	.size	GuiVar_ENIPAddress_1, 4
GuiVar_ENIPAddress_1:
	.space	4
	.section	.bss.GuiVar_ENIPAddress_2,"aw",%nobits
	.align	2
	.type	GuiVar_ENIPAddress_2, %object
	.size	GuiVar_ENIPAddress_2, 4
GuiVar_ENIPAddress_2:
	.space	4
	.section	.bss.GuiVar_ENIPAddress_3,"aw",%nobits
	.align	2
	.type	GuiVar_ENIPAddress_3, %object
	.size	GuiVar_ENIPAddress_3, 4
GuiVar_ENIPAddress_3:
	.space	4
	.section	.bss.GuiVar_ENIPAddress_4,"aw",%nobits
	.align	2
	.type	GuiVar_ENIPAddress_4, %object
	.size	GuiVar_ENIPAddress_4, 4
GuiVar_ENIPAddress_4:
	.space	4
	.section	.bss.GuiVar_LightCalendarWeek1Sat,"aw",%nobits
	.type	GuiVar_LightCalendarWeek1Sat, %object
	.size	GuiVar_LightCalendarWeek1Sat, 3
GuiVar_LightCalendarWeek1Sat:
	.space	3
	.section	.bss.GuiVar_ScheduleStartTimeEnabled,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleStartTimeEnabled, %object
	.size	GuiVar_ScheduleStartTimeEnabled, 4
GuiVar_ScheduleStartTimeEnabled:
	.space	4
	.section	.bss.GuiVar_DisplayType,"aw",%nobits
	.align	2
	.type	GuiVar_DisplayType, %object
	.size	GuiVar_DisplayType, 4
GuiVar_DisplayType:
	.space	4
	.section	.bss.GuiVar_BudgetModeIdx,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetModeIdx, %object
	.size	GuiVar_BudgetModeIdx, 4
GuiVar_BudgetModeIdx:
	.space	4
	.section	.bss.GuiVar_FlowCheckingFMs,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFMs, %object
	.size	GuiVar_FlowCheckingFMs, 4
GuiVar_FlowCheckingFMs:
	.space	4
	.section	.bss.GuiVar_FlowTable3Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable3Sta, %object
	.size	GuiVar_FlowTable3Sta, 4
GuiVar_FlowTable3Sta:
	.space	4
	.section	.bss.GuiVar_StationGroupPlantType,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupPlantType, %object
	.size	GuiVar_StationGroupPlantType, 4
GuiVar_StationGroupPlantType:
	.space	4
	.section	.bss.GuiVar_IrriDetailsShowFOAL,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsShowFOAL, %object
	.size	GuiVar_IrriDetailsShowFOAL, 4
GuiVar_IrriDetailsShowFOAL:
	.space	4
	.section	.bss.GuiVar_StatusSystemFlowValvesOn,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowValvesOn, %object
	.size	GuiVar_StatusSystemFlowValvesOn, 4
GuiVar_StatusSystemFlowValvesOn:
	.space	4
	.section	.bss.GuiVar_ETGageInUse,"aw",%nobits
	.align	2
	.type	GuiVar_ETGageInUse, %object
	.size	GuiVar_ETGageInUse, 4
GuiVar_ETGageInUse:
	.space	4
	.section	.bss.GuiVar_ManualPStartDateStr,"aw",%nobits
	.type	GuiVar_ManualPStartDateStr, %object
	.size	GuiVar_ManualPStartDateStr, 49
GuiVar_ManualPStartDateStr:
	.space	49
	.section	.bss.GuiVar_TwoWirePOCInUse,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWirePOCInUse, %object
	.size	GuiVar_TwoWirePOCInUse, 4
GuiVar_TwoWirePOCInUse:
	.space	4
	.section	.bss.GuiVar_AboutTPFuseCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPFuseCard, %object
	.size	GuiVar_AboutTPFuseCard, 4
GuiVar_AboutTPFuseCard:
	.space	4
	.section	.bss.GuiVar_FlowTable5Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable5Sta, %object
	.size	GuiVar_FlowTable5Sta, 4
GuiVar_FlowTable5Sta:
	.space	4
	.section	.bss.GuiVar_LRFrequency_WholeNum,"aw",%nobits
	.align	2
	.type	GuiVar_LRFrequency_WholeNum, %object
	.size	GuiVar_LRFrequency_WholeNum, 4
GuiVar_LRFrequency_WholeNum:
	.space	4
	.section	.bss.GuiVar_LiveScreensCommTPMicroConnected,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommTPMicroConnected, %object
	.size	GuiVar_LiveScreensCommTPMicroConnected, 4
GuiVar_LiveScreensCommTPMicroConnected:
	.space	4
	.section	.bss.GuiVar_StatusTypeOfSchedule,"aw",%nobits
	.align	2
	.type	GuiVar_StatusTypeOfSchedule, %object
	.size	GuiVar_StatusTypeOfSchedule, 4
GuiVar_StatusTypeOfSchedule:
	.space	4
	.section	.bss.GuiVar_ManualPWaterDay_Tue,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Tue, %object
	.size	GuiVar_ManualPWaterDay_Tue, 4
GuiVar_ManualPWaterDay_Tue:
	.space	4
	.section	.bss.GuiVar_LightsStopTime1InMinutes,"aw",%nobits
	.align	2
	.type	GuiVar_LightsStopTime1InMinutes, %object
	.size	GuiVar_LightsStopTime1InMinutes, 4
GuiVar_LightsStopTime1InMinutes:
	.space	4
	.section	.bss.GuiVar_itmDecoder_Debug,"aw",%nobits
	.type	GuiVar_itmDecoder_Debug, %object
	.size	GuiVar_itmDecoder_Debug, 49
GuiVar_itmDecoder_Debug:
	.space	49
	.section	.bss.GuiVar_RadioTestPort,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestPort, %object
	.size	GuiVar_RadioTestPort, 4
GuiVar_RadioTestPort:
	.space	4
	.section	.bss.GuiVar_ChainStatsNextContact,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsNextContact, %object
	.size	GuiVar_ChainStatsNextContact, 4
GuiVar_ChainStatsNextContact:
	.space	4
	.section	.bss.GuiVar_ScheduleType,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleType, %object
	.size	GuiVar_ScheduleType, 4
GuiVar_ScheduleType:
	.space	4
	.section	.bss.GuiVar_RadioTestDeviceIndex,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestDeviceIndex, %object
	.size	GuiVar_RadioTestDeviceIndex, 4
GuiVar_RadioTestDeviceIndex:
	.space	4
	.section	.bss.GuiVar_LiveScreensCommCentralConnected,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensCommCentralConnected, %object
	.size	GuiVar_LiveScreensCommCentralConnected, 4
GuiVar_LiveScreensCommCentralConnected:
	.space	4
	.section	.bss.GuiVar_POCBudgetOption,"aw",%nobits
	.align	2
	.type	GuiVar_POCBudgetOption, %object
	.size	GuiVar_POCBudgetOption, 4
GuiVar_POCBudgetOption:
	.space	4
	.section	.bss.GuiVar_LiveScreensBypassFlowAvgFlow,"aw",%nobits
	.align	2
	.type	GuiVar_LiveScreensBypassFlowAvgFlow, %object
	.size	GuiVar_LiveScreensBypassFlowAvgFlow, 4
GuiVar_LiveScreensBypassFlowAvgFlow:
	.space	4
	.section	.bss.GuiVar_TwoWireNumDiscoveredPOCDecoders,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireNumDiscoveredPOCDecoders, %object
	.size	GuiVar_TwoWireNumDiscoveredPOCDecoders, 4
GuiVar_TwoWireNumDiscoveredPOCDecoders:
	.space	4
	.section	.bss.GuiVar_FlowTable9Sta,"aw",%nobits
	.align	2
	.type	GuiVar_FlowTable9Sta, %object
	.size	GuiVar_FlowTable9Sta, 4
GuiVar_FlowTable9Sta:
	.space	4
	.section	.bss.GuiVar_TwoWireStaDecoderIndex,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStaDecoderIndex, %object
	.size	GuiVar_TwoWireStaDecoderIndex, 4
GuiVar_TwoWireStaDecoderIndex:
	.space	4
	.section	.bss.GuiVar_StationInfoNumber_str,"aw",%nobits
	.type	GuiVar_StationInfoNumber_str, %object
	.size	GuiVar_StationInfoNumber_str, 4
GuiVar_StationInfoNumber_str:
	.space	4
	.section	.bss.GuiVar_KeyboardStartX,"aw",%nobits
	.align	2
	.type	GuiVar_KeyboardStartX, %object
	.size	GuiVar_KeyboardStartX, 4
GuiVar_KeyboardStartX:
	.space	4
	.section	.bss.GuiVar_KeyboardStartY,"aw",%nobits
	.align	2
	.type	GuiVar_KeyboardStartY, %object
	.size	GuiVar_KeyboardStartY, 4
GuiVar_KeyboardStartY:
	.space	4
	.section	.bss.GuiVar_ScheduleWaterDay_Mon,"aw",%nobits
	.align	2
	.type	GuiVar_ScheduleWaterDay_Mon, %object
	.size	GuiVar_ScheduleWaterDay_Mon, 4
GuiVar_ScheduleWaterDay_Mon:
	.space	4
	.section	.bss.GuiVar_AboutFLOption,"aw",%nobits
	.align	2
	.type	GuiVar_AboutFLOption, %object
	.size	GuiVar_AboutFLOption, 4
GuiVar_AboutFLOption:
	.space	4
	.section	.bss.GuiVar_Volume,"aw",%nobits
	.align	2
	.type	GuiVar_Volume, %object
	.size	GuiVar_Volume, 4
GuiVar_Volume:
	.space	4
	.section	.bss.GuiVar_StationSelectionGridScreenTitle,"aw",%nobits
	.type	GuiVar_StationSelectionGridScreenTitle, %object
	.size	GuiVar_StationSelectionGridScreenTitle, 129
GuiVar_StationSelectionGridScreenTitle:
	.space	129
	.section	.bss.GuiVar_StationGroupSpeciesFactor,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupSpeciesFactor, %object
	.size	GuiVar_StationGroupSpeciesFactor, 4
GuiVar_StationGroupSpeciesFactor:
	.space	4
	.section	.bss.GuiVar_FlowCheckingOnForProbList,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingOnForProbList, %object
	.size	GuiVar_FlowCheckingOnForProbList, 4
GuiVar_FlowCheckingOnForProbList:
	.space	4
	.section	.bss.GuiVar_TwoWireOrphanedStationsExist,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireOrphanedStationsExist, %object
	.size	GuiVar_TwoWireOrphanedStationsExist, 4
GuiVar_TwoWireOrphanedStationsExist:
	.space	4
	.section	.bss.GuiVar_ScrollBoxHorizScrollMarker,"aw",%nobits
	.align	2
	.type	GuiVar_ScrollBoxHorizScrollMarker, %object
	.size	GuiVar_ScrollBoxHorizScrollMarker, 4
GuiVar_ScrollBoxHorizScrollMarker:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseA,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseA, %object
	.size	GuiVar_ChainStatsInUseA, 4
GuiVar_ChainStatsInUseA:
	.space	4
	.section	.bss.GuiVar_PercentAdjustPercent,"aw",%nobits
	.align	2
	.type	GuiVar_PercentAdjustPercent, %object
	.size	GuiVar_PercentAdjustPercent, 4
GuiVar_PercentAdjustPercent:
	.space	4
	.section	.bss.GuiVar_MVORClose4,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose4, %object
	.size	GuiVar_MVORClose4, 4
GuiVar_MVORClose4:
	.space	4
	.section	.bss.GuiVar_FinishTimesCalcPercentComplete,"aw",%nobits
	.align	2
	.type	GuiVar_FinishTimesCalcPercentComplete, %object
	.size	GuiVar_FinishTimesCalcPercentComplete, 4
GuiVar_FinishTimesCalcPercentComplete:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseF,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseF, %object
	.size	GuiVar_ChainStatsInUseF, 4
GuiVar_ChainStatsInUseF:
	.space	4
	.section	.bss.GuiVar_ChainStatsInUseG,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsInUseG, %object
	.size	GuiVar_ChainStatsInUseG, 4
GuiVar_ChainStatsInUseG:
	.space	4
	.section	.bss.GuiVar_StationSelectionGridMarkerPos,"aw",%nobits
	.align	2
	.type	GuiVar_StationSelectionGridMarkerPos, %object
	.size	GuiVar_StationSelectionGridMarkerPos, 4
GuiVar_StationSelectionGridMarkerPos:
	.space	4
	.section	.bss.GuiVar_StopKeyReasonInList,"aw",%nobits
	.align	2
	.type	GuiVar_StopKeyReasonInList, %object
	.size	GuiVar_StopKeyReasonInList, 4
GuiVar_StopKeyReasonInList:
	.space	4
	.section	.bss.GuiVar_StatusNextScheduledIrriType,"aw",%nobits
	.type	GuiVar_StatusNextScheduledIrriType, %object
	.size	GuiVar_StatusNextScheduledIrriType, 49
GuiVar_StatusNextScheduledIrriType:
	.space	49
	.section	.bss.GuiVar_StopKeyPending,"aw",%nobits
	.align	2
	.type	GuiVar_StopKeyPending, %object
	.size	GuiVar_StopKeyPending, 4
GuiVar_StopKeyPending:
	.space	4
	.section	.bss.GuiVar_HistoricalETCounty,"aw",%nobits
	.type	GuiVar_HistoricalETCounty, %object
	.size	GuiVar_HistoricalETCounty, 25
GuiVar_HistoricalETCounty:
	.space	25
	.section	.bss.GuiVar_ManualPStartTime4,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime4, %object
	.size	GuiVar_ManualPStartTime4, 4
GuiVar_ManualPStartTime4:
	.space	4
	.section	.bss.GuiVar_StatusOverviewStationStr,"aw",%nobits
	.type	GuiVar_StatusOverviewStationStr, %object
	.size	GuiVar_StatusOverviewStationStr, 65
GuiVar_StatusOverviewStationStr:
	.space	65
	.section	.bss.GuiVar_MVORClose4Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose4Enabled, %object
	.size	GuiVar_MVORClose4Enabled, 4
GuiVar_MVORClose4Enabled:
	.space	4
	.section	.bss.GuiVar_IsMaster,"aw",%nobits
	.align	2
	.type	GuiVar_IsMaster, %object
	.size	GuiVar_IsMaster, 4
GuiVar_IsMaster:
	.space	4
	.section	.bss.GuiVar_POCPreservesDeliveredMVCurrent1,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredMVCurrent1, %object
	.size	GuiVar_POCPreservesDeliveredMVCurrent1, 4
GuiVar_POCPreservesDeliveredMVCurrent1:
	.space	4
	.section	.bss.GuiVar_POCPreservesDeliveredMVCurrent2,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredMVCurrent2, %object
	.size	GuiVar_POCPreservesDeliveredMVCurrent2, 4
GuiVar_POCPreservesDeliveredMVCurrent2:
	.space	4
	.section	.bss.GuiVar_POCPreservesDeliveredMVCurrent3,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesDeliveredMVCurrent3, %object
	.size	GuiVar_POCPreservesDeliveredMVCurrent3, 4
GuiVar_POCPreservesDeliveredMVCurrent3:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsWDTs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsWDTs, %object
	.size	GuiVar_TwoWireStatsWDTs, 4
GuiVar_TwoWireStatsWDTs:
	.space	4
	.section	.bss.GuiVar_TwoWirePOCDecoderIndex,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWirePOCDecoderIndex, %object
	.size	GuiVar_TwoWirePOCDecoderIndex, 4
GuiVar_TwoWirePOCDecoderIndex:
	.space	4
	.section	.bss.GuiVar_RainSwitchControllerName,"aw",%nobits
	.type	GuiVar_RainSwitchControllerName, %object
	.size	GuiVar_RainSwitchControllerName, 49
GuiVar_RainSwitchControllerName:
	.space	49
	.section	.bss.GuiVar_MVORClose5Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose5Enabled, %object
	.size	GuiVar_MVORClose5Enabled, 4
GuiVar_MVORClose5Enabled:
	.space	4
	.section	.bss.GuiVar_SystemInUse,"aw",%nobits
	.align	2
	.type	GuiVar_SystemInUse, %object
	.size	GuiVar_SystemInUse, 4
GuiVar_SystemInUse:
	.space	4
	.section	.bss.GuiVar_SystemFlowCheckingInUse,"aw",%nobits
	.align	2
	.type	GuiVar_SystemFlowCheckingInUse, %object
	.size	GuiVar_SystemFlowCheckingInUse, 4
GuiVar_SystemFlowCheckingInUse:
	.space	4
	.section	.bss.GuiVar_MainMenuFlowMetersExist,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuFlowMetersExist, %object
	.size	GuiVar_MainMenuFlowMetersExist, 4
GuiVar_MainMenuFlowMetersExist:
	.space	4
	.section	.bss.GuiVar_RadioTestPacketsSent,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestPacketsSent, %object
	.size	GuiVar_RadioTestPacketsSent, 4
GuiVar_RadioTestPacketsSent:
	.space	4
	.section	.bss.GuiVar_ETGageSkipTonight,"aw",%nobits
	.align	2
	.type	GuiVar_ETGageSkipTonight, %object
	.size	GuiVar_ETGageSkipTonight, 4
GuiVar_ETGageSkipTonight:
	.space	4
	.section	.bss.GuiVar_WENWEPKeySize,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPKeySize, %object
	.size	GuiVar_WENWEPKeySize, 4
GuiVar_WENWEPKeySize:
	.space	4
	.section	.bss.GuiVar_WENWEPTXKey,"aw",%nobits
	.align	2
	.type	GuiVar_WENWEPTXKey, %object
	.size	GuiVar_WENWEPTXKey, 4
GuiVar_WENWEPTXKey:
	.space	4
	.section	.bss.GuiVar_CodeDownloadUpdatingTPMicro,"aw",%nobits
	.align	2
	.type	GuiVar_CodeDownloadUpdatingTPMicro, %object
	.size	GuiVar_CodeDownloadUpdatingTPMicro, 4
GuiVar_CodeDownloadUpdatingTPMicro:
	.space	4
	.section	.bss.GuiVar_MVORClose6Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose6Enabled, %object
	.size	GuiVar_MVORClose6Enabled, 4
GuiVar_MVORClose6Enabled:
	.space	4
	.section	.bss.GuiVar_HistoricalET1,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET1, %object
	.size	GuiVar_HistoricalET1, 4
GuiVar_HistoricalET1:
	.space	4
	.section	.bss.GuiVar_HistoricalET2,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET2, %object
	.size	GuiVar_HistoricalET2, 4
GuiVar_HistoricalET2:
	.space	4
	.section	.bss.GuiVar_HistoricalET3,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET3, %object
	.size	GuiVar_HistoricalET3, 4
GuiVar_HistoricalET3:
	.space	4
	.section	.bss.GuiVar_HistoricalET4,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET4, %object
	.size	GuiVar_HistoricalET4, 4
GuiVar_HistoricalET4:
	.space	4
	.section	.bss.GuiVar_StationInfoShowPercentAdjust,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoShowPercentAdjust, %object
	.size	GuiVar_StationInfoShowPercentAdjust, 4
GuiVar_StationInfoShowPercentAdjust:
	.space	4
	.section	.bss.GuiVar_HistoricalET6,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET6, %object
	.size	GuiVar_HistoricalET6, 4
GuiVar_HistoricalET6:
	.space	4
	.section	.bss.GuiVar_HistoricalET7,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET7, %object
	.size	GuiVar_HistoricalET7, 4
GuiVar_HistoricalET7:
	.space	4
	.section	.bss.GuiVar_HistoricalET8,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalET8, %object
	.size	GuiVar_HistoricalET8, 4
GuiVar_HistoricalET8:
	.space	4
	.section	.bss.GuiVar_StationInfoBoxIndex,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoBoxIndex, %object
	.size	GuiVar_StationInfoBoxIndex, 4
GuiVar_StationInfoBoxIndex:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxDiscRstMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxDiscRstMsgs, %object
	.size	GuiVar_TwoWireStatsRxDiscRstMsgs, 4
GuiVar_TwoWireStatsRxDiscRstMsgs:
	.space	4
	.section	.bss.GuiVar_MainMenuManualAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_MainMenuManualAvailable, %object
	.size	GuiVar_MainMenuManualAvailable, 4
GuiVar_MainMenuManualAvailable:
	.space	4
	.section	.bss.GuiVar_WENWPAKeyType,"aw",%nobits
	.align	2
	.type	GuiVar_WENWPAKeyType, %object
	.size	GuiVar_WENWPAKeyType, 4
GuiVar_WENWPAKeyType:
	.space	4
	.section	.bss.GuiVar_DelayBetweenValvesInUse,"aw",%nobits
	.align	2
	.type	GuiVar_DelayBetweenValvesInUse, %object
	.size	GuiVar_DelayBetweenValvesInUse, 4
GuiVar_DelayBetweenValvesInUse:
	.space	4
	.section	.bss.GuiVar_MVORClose0,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose0, %object
	.size	GuiVar_MVORClose0, 4
GuiVar_MVORClose0:
	.space	4
	.section	.bss.GuiVar_MVORClose1,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose1, %object
	.size	GuiVar_MVORClose1, 4
GuiVar_MVORClose1:
	.space	4
	.section	.bss.GuiVar_MVORClose2,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose2, %object
	.size	GuiVar_MVORClose2, 4
GuiVar_MVORClose2:
	.space	4
	.section	.bss.GuiVar_MVORClose3,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose3, %object
	.size	GuiVar_MVORClose3, 4
GuiVar_MVORClose3:
	.space	4
	.section	.bss.GuiVar_ManualPStartTime4Enabled,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPStartTime4Enabled, %object
	.size	GuiVar_ManualPStartTime4Enabled, 4
GuiVar_ManualPStartTime4Enabled:
	.space	4
	.section	.bss.GuiVar_MVORClose5,"aw",%nobits
	.align	2
	.type	GuiVar_MVORClose5, %object
	.size	GuiVar_MVORClose5, 4
GuiVar_MVORClose5:
	.space	4
	.section	.bss.GuiVar_NumericKeypadShowDecimalPoint,"aw",%nobits
	.align	2
	.type	GuiVar_NumericKeypadShowDecimalPoint, %object
	.size	GuiVar_NumericKeypadShowDecimalPoint, 4
GuiVar_NumericKeypadShowDecimalPoint:
	.space	4
	.section	.bss.GuiVar_StatusShowLiveScreens,"aw",%nobits
	.align	2
	.type	GuiVar_StatusShowLiveScreens, %object
	.size	GuiVar_StatusShowLiveScreens, 4
GuiVar_StatusShowLiveScreens:
	.space	4
	.section	.bss.GuiVar_RainBucketMinimum,"aw",%nobits
	.align	2
	.type	GuiVar_RainBucketMinimum, %object
	.size	GuiVar_RainBucketMinimum, 4
GuiVar_RainBucketMinimum:
	.space	4
	.section	.bss.GuiVar_RadioTestCommWithET2000e,"aw",%nobits
	.align	2
	.type	GuiVar_RadioTestCommWithET2000e, %object
	.size	GuiVar_RadioTestCommWithET2000e, 4
GuiVar_RadioTestCommWithET2000e:
	.space	4
	.section	.bss.GuiVar_SRSubnetXmtMaster,"aw",%nobits
	.align	2
	.type	GuiVar_SRSubnetXmtMaster, %object
	.size	GuiVar_SRSubnetXmtMaster, 4
GuiVar_SRSubnetXmtMaster:
	.space	4
	.section	.bss.GuiVar_ETGagePercentFull,"aw",%nobits
	.align	2
	.type	GuiVar_ETGagePercentFull, %object
	.size	GuiVar_ETGagePercentFull, 4
GuiVar_ETGagePercentFull:
	.space	4
	.section	.bss.GuiVar_MoisTempPoint_High,"aw",%nobits
	.align	2
	.type	GuiVar_MoisTempPoint_High, %object
	.size	GuiVar_MoisTempPoint_High, 4
GuiVar_MoisTempPoint_High:
	.space	4
	.section	.bss.GuiVar_TwoWireDecoderFWVersion,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireDecoderFWVersion, %object
	.size	GuiVar_TwoWireDecoderFWVersion, 4
GuiVar_TwoWireDecoderFWVersion:
	.space	4
	.section	.bss.GuiVar_HistoricalETUser5,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUser5, %object
	.size	GuiVar_HistoricalETUser5, 4
GuiVar_HistoricalETUser5:
	.space	4
	.section	.bss.GuiVar_FlowCheckingFlag4,"aw",%nobits
	.align	2
	.type	GuiVar_FlowCheckingFlag4, %object
	.size	GuiVar_FlowCheckingFlag4, 4
GuiVar_FlowCheckingFlag4:
	.space	4
	.section	.bss.GuiVar_StationGroupAvailableWater,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupAvailableWater, %object
	.size	GuiVar_StationGroupAvailableWater, 4
GuiVar_StationGroupAvailableWater:
	.space	4
	.section	.bss.GuiVar_TwoWireStatsRxGetParamsMsgs,"aw",%nobits
	.align	2
	.type	GuiVar_TwoWireStatsRxGetParamsMsgs, %object
	.size	GuiVar_TwoWireStatsRxGetParamsMsgs, 4
GuiVar_TwoWireStatsRxGetParamsMsgs:
	.space	4
	.section	.bss.GuiVar_GRSIMID,"aw",%nobits
	.type	GuiVar_GRSIMID, %object
	.size	GuiVar_GRSIMID, 49
GuiVar_GRSIMID:
	.space	49
	.section	.bss.GuiVar_AboutTPSta33_40Card,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPSta33_40Card, %object
	.size	GuiVar_AboutTPSta33_40Card, 4
GuiVar_AboutTPSta33_40Card:
	.space	4
	.section	.bss.GuiVar_BudgetReductionLimit_6,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_6, %object
	.size	GuiVar_BudgetReductionLimit_6, 4
GuiVar_BudgetReductionLimit_6:
	.space	4
	.section	.bss.GuiVar_IrriCommChainConfigActive,"aw",%nobits
	.align	2
	.type	GuiVar_IrriCommChainConfigActive, %object
	.size	GuiVar_IrriCommChainConfigActive, 4
GuiVar_IrriCommChainConfigActive:
	.space	4
	.section	.bss.GuiVar_ChainStatsScans,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsScans, %object
	.size	GuiVar_ChainStatsScans, 4
GuiVar_ChainStatsScans:
	.space	4
	.section	.bss.GuiVar_ChainStatsIRRIChainTRcvd,"aw",%nobits
	.align	2
	.type	GuiVar_ChainStatsIRRIChainTRcvd, %object
	.size	GuiVar_ChainStatsIRRIChainTRcvd, 4
GuiVar_ChainStatsIRRIChainTRcvd:
	.space	4
	.section	.bss.GuiVar_ManualPWaterDay_Wed,"aw",%nobits
	.align	2
	.type	GuiVar_ManualPWaterDay_Wed, %object
	.size	GuiVar_ManualPWaterDay_Wed, 4
GuiVar_ManualPWaterDay_Wed:
	.space	4
	.section	.bss.GuiVar_BudgetReductionLimit_9,"aw",%nobits
	.align	2
	.type	GuiVar_BudgetReductionLimit_9, %object
	.size	GuiVar_BudgetReductionLimit_9, 4
GuiVar_BudgetReductionLimit_9:
	.space	4
	.section	.bss.GuiVar_StationGroupDensityFactor,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupDensityFactor, %object
	.size	GuiVar_StationGroupDensityFactor, 4
GuiVar_StationGroupDensityFactor:
	.space	4
	.section	.bss.GuiVar_LightScheduledStop,"aw",%nobits
	.type	GuiVar_LightScheduledStop, %object
	.size	GuiVar_LightScheduledStop, 33
GuiVar_LightScheduledStop:
	.space	33
	.section	.bss.GuiVar_POCBoxIndex,"aw",%nobits
	.align	2
	.type	GuiVar_POCBoxIndex, %object
	.size	GuiVar_POCBoxIndex, 4
GuiVar_POCBoxIndex:
	.space	4
	.section	.bss.GuiVar_StatusSystemFlowShowMLBDetected,"aw",%nobits
	.align	2
	.type	GuiVar_StatusSystemFlowShowMLBDetected, %object
	.size	GuiVar_StatusSystemFlowShowMLBDetected, 4
GuiVar_StatusSystemFlowShowMLBDetected:
	.space	4
	.section	.bss.GuiVar_NetworkAvailable,"aw",%nobits
	.align	2
	.type	GuiVar_NetworkAvailable, %object
	.size	GuiVar_NetworkAvailable, 4
GuiVar_NetworkAvailable:
	.space	4
	.section	.bss.GuiVar_POCPreservesIndex,"aw",%nobits
	.align	2
	.type	GuiVar_POCPreservesIndex, %object
	.size	GuiVar_POCPreservesIndex, 4
GuiVar_POCPreservesIndex:
	.space	4
	.section	.bss.GuiVar_RptManualPMin,"aw",%nobits
	.align	2
	.type	GuiVar_RptManualPMin, %object
	.size	GuiVar_RptManualPMin, 4
GuiVar_RptManualPMin:
	.space	4
	.section	.bss.GuiVar_StationGroupExposure,"aw",%nobits
	.align	2
	.type	GuiVar_StationGroupExposure, %object
	.size	GuiVar_StationGroupExposure, 4
GuiVar_StationGroupExposure:
	.space	4
	.section	.bss.GuiVar_StationInfoMoistureBalancePercent,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoMoistureBalancePercent, %object
	.size	GuiVar_StationInfoMoistureBalancePercent, 4
GuiVar_StationInfoMoistureBalancePercent:
	.space	4
	.section	.bss.GuiVar_WindGageInUse,"aw",%nobits
	.align	2
	.type	GuiVar_WindGageInUse, %object
	.size	GuiVar_WindGageInUse, 4
GuiVar_WindGageInUse:
	.space	4
	.section	.bss.GuiVar_HistoricalETUseYourOwn,"aw",%nobits
	.align	2
	.type	GuiVar_HistoricalETUseYourOwn, %object
	.size	GuiVar_HistoricalETUseYourOwn, 4
GuiVar_HistoricalETUseYourOwn:
	.space	4
	.section	.bss.GuiVar_GroupName_0,"aw",%nobits
	.type	GuiVar_GroupName_0, %object
	.size	GuiVar_GroupName_0, 49
GuiVar_GroupName_0:
	.space	49
	.section	.bss.GuiVar_MoisSensorIncludesEC,"aw",%nobits
	.align	2
	.type	GuiVar_MoisSensorIncludesEC, %object
	.size	GuiVar_MoisSensorIncludesEC, 4
GuiVar_MoisSensorIncludesEC:
	.space	4
	.section	.bss.GuiVar_GroupName_4,"aw",%nobits
	.type	GuiVar_GroupName_4, %object
	.size	GuiVar_GroupName_4, 49
GuiVar_GroupName_4:
	.space	49
	.section	.bss.GuiVar_IrriDetailsProgCycle,"aw",%nobits
	.align	2
	.type	GuiVar_IrriDetailsProgCycle, %object
	.size	GuiVar_IrriDetailsProgCycle, 4
GuiVar_IrriDetailsProgCycle:
	.space	4
	.section	.bss.GuiVar_GroupName_5,"aw",%nobits
	.type	GuiVar_GroupName_5, %object
	.size	GuiVar_GroupName_5, 49
GuiVar_GroupName_5:
	.space	49
	.section	.bss.GuiVar_AboutTPDashWCard,"aw",%nobits
	.align	2
	.type	GuiVar_AboutTPDashWCard, %object
	.size	GuiVar_AboutTPDashWCard, 4
GuiVar_AboutTPDashWCard:
	.space	4
	.section	.bss.GuiVar_GroupName_6,"aw",%nobits
	.type	GuiVar_GroupName_6, %object
	.size	GuiVar_GroupName_6, 49
GuiVar_GroupName_6:
	.space	49
	.section	.bss.GuiVar_GroupName_7,"aw",%nobits
	.type	GuiVar_GroupName_7, %object
	.size	GuiVar_GroupName_7, 49
GuiVar_GroupName_7:
	.space	49
	.section	.bss.GuiVar_StationInfoCycleTooShort,"aw",%nobits
	.align	2
	.type	GuiVar_StationInfoCycleTooShort, %object
	.size	GuiVar_StationInfoCycleTooShort, 4
GuiVar_StationInfoCycleTooShort:
	.space	4
	.section	.bss.GuiVar_GroupName_8,"aw",%nobits
	.type	GuiVar_GroupName_8, %object
	.size	GuiVar_GroupName_8, 49
GuiVar_GroupName_8:
	.space	49
	.text
.Letext0:
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
