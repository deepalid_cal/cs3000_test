	.file	"r_rt_system_flow.c"
	.text
.Ltext0:
	.section	.text.REAL_TIME_SYSTEM_FLOW_draw_scroll_line,"ax",%progbits
	.align	2
	.type	REAL_TIME_SYSTEM_FLOW_draw_scroll_line, %function
REAL_TIME_SYSTEM_FLOW_draw_scroll_line:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L9
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r2, .L9+4
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #55
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L9+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L9+4
	mov	r3, #57
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	subs	r4, r0, #0
	beq	.L2
	bl	nm_GROUP_get_name
	mov	r2, #24
	mov	r1, r0
	ldr	r0, .L9+12
	bl	strlcpy
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldr	r2, .L9+16
	ldr	r3, .L9+20
	subs	r4, r0, #0
	beq	.L3
	flds	s15, [r4, #304]
	ldr	r1, .L9+24
	add	r0, r4, #516
	ftouizs	s15, s15
	ldr	r1, [r4, r1]
	str	r1, [r2, #0]
	ldr	r2, [r4, #100]
	fsts	s15, [r3, #0]	@ int
	ldr	r3, .L9+28
	str	r2, [r3, #0]
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	ldr	r3, .L9+32
	ldr	r2, .L9+36
	str	r0, [r3, #0]
	ldrb	r3, [r4, #468]	@ zero_extendqisi2
	tst	r3, #96
	moveq	r3, #0
	movne	r3, #1
	cmp	r0, #0
	str	r3, [r2, #0]
	beq	.L4
	ldrb	r3, [r4, #516]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L5
	mov	r3, #520
	ldrh	r2, [r4, r3]
	ldr	r3, .L9+40
	str	r2, [r3, #0]
	ldr	r3, .L9+44
	b	.L8
.L5:
	ldrb	r3, [r4, #517]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L7
	mov	r3, #524
	ldrh	r2, [r4, r3]
	ldr	r3, .L9+40
	str	r2, [r3, #0]
	ldr	r3, .L9+48
	b	.L8
.L7:
	ldrb	r3, [r4, #518]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L6
	mov	r3, #528
	ldrh	r2, [r4, r3]
	ldr	r3, .L9+40
	str	r2, [r3, #0]
	ldr	r3, .L9+52
.L8:
	ldrh	r2, [r4, r3]
	ldr	r3, .L9+56
	str	r2, [r3, #0]
	b	.L6
.L4:
	cmp	r3, #0
	beq	.L6
	ldrb	r2, [r4, #468]	@ zero_extendqisi2
	ldr	r3, .L9+60
	mov	r2, r2, lsr #6
	and	r2, r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L9+64
	flds	s15, [r3, #0]
	ldr	r3, .L9+68
	ldr	r3, [r4, r3]
	fmsr	s13, r3	@ int
	ldr	r3, .L9+72
	fuitos	s14, s13
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	b	.L6
.L3:
	str	r4, [r3, #0]
	ldr	r1, .L9+32
	ldr	r3, .L9+28
	str	r4, [r1, #0]
	str	r4, [r2, #0]
	str	r4, [r3, #0]
	b	.L6
.L2:
	ldr	r0, .L9+4
	mov	r1, #130
	bl	Alert_group_not_found_with_filename
.L6:
	ldr	r3, .L9+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L9
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L10:
	.align	2
.L9:
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	list_system_recursive_MUTEX
	.word	GuiVar_StatusSystemFlowSystem
	.word	GuiVar_StatusSystemFlowValvesOn
	.word	GuiVar_StatusSystemFlowActual
	.word	14112
	.word	GuiVar_StatusSystemFlowExpected
	.word	GuiVar_StatusSystemFlowShowMLBDetected
	.word	GuiVar_MVORInEffect
	.word	GuiVar_StatusSystemFlowMLBMeasured
	.word	522
	.word	526
	.word	530
	.word	GuiVar_StatusSystemFlowMLBAllowed
	.word	GuiVar_MVORState
	.word	.LANCHOR0
	.word	14124
	.word	GuiVar_MVORTimeRemaining
.LFE0:
	.size	REAL_TIME_SYSTEM_FLOW_draw_scroll_line, .-REAL_TIME_SYSTEM_FLOW_draw_scroll_line
	.section	.text.FDTO_REAL_TIME_SYSTEM_FLOW_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_SYSTEM_FLOW_draw_report
	.type	FDTO_REAL_TIME_SYSTEM_FLOW_draw_report, %function
FDTO_REAL_TIME_SYSTEM_FLOW_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI1:
	mov	r2, r0
	bne	.L12
	mvn	r1, #0
	mov	r0, #97
	bl	GuiLib_ShowScreen
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	bl	SYSTEM_num_systems_in_use
	ldr	r1, .L22
	mov	r2, r0, asl #16
	mov	r0, #0
	mov	r2, r2, asr #16
	mov	r3, r0
	bl	GuiLib_ScrollBox_Init
	b	.L13
.L12:
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
.L13:
	ldr	r3, .L22+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L22+8
	mov	r3, #173
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L22+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L22+8
	mov	r3, #175
	bl	xQueueTakeMutexRecursive_debug
	ldr	sl, .L22+16
	ldr	r3, .L22+20
	mov	r4, #0
	ldr	r7, [r3, #0]
	ldr	r6, [sl, #0]
	str	r4, [r3, #0]
	str	r4, [sl, #0]
	mov	r8, #1
	b	.L14
.L18:
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	cmp	r0, #0
	beq	.L15
	bl	nm_GROUP_get_group_ID
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	r5, r0, #0
	beq	.L15
	add	r0, r5, #516
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	cmp	r0, #0
	ldrne	r3, .L22+20
	movne	r2, #1
	strne	r2, [r3, #0]
	bne	.L17
.L16:
	ldrb	r3, [r5, #468]	@ zero_extendqisi2
	tst	r3, #96
	strne	r8, [sl, #0]
.L15:
	add	r4, r4, #1
.L14:
	bl	SYSTEM_num_systems_in_use
	cmp	r4, r0
	bcc	.L18
.L17:
	ldr	r3, .L22+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L22+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L22+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L19
	ldr	r2, .L22+16
	ldr	r1, [r2, #0]
	cmp	r1, #0
	movne	r1, #0
	strne	r1, [r2, #0]
.L19:
	cmp	r7, r3
	bne	.L20
	ldr	r3, .L22+16
	ldr	r3, [r3, #0]
	cmp	r6, r3
	beq	.L21
.L20:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	FDTO_Redraw_Screen
.L21:
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	GuiLib_Refresh
.L23:
	.align	2
.L22:
	.word	REAL_TIME_SYSTEM_FLOW_draw_scroll_line
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	list_system_recursive_MUTEX
	.word	GuiVar_LiveScreensSystemMVORExists
	.word	GuiVar_LiveScreensSystemMLBExists
.LFE1:
	.size	FDTO_REAL_TIME_SYSTEM_FLOW_draw_report, .-FDTO_REAL_TIME_SYSTEM_FLOW_draw_report
	.section	.text.REAL_TIME_SYSTEM_FLOW_process_report,"ax",%progbits
	.align	2
	.global	REAL_TIME_SYSTEM_FLOW_process_report
	.type	REAL_TIME_SYSTEM_FLOW_process_report, %function
REAL_TIME_SYSTEM_FLOW_process_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI2:
	mov	r4, r0
	beq	.L26
	cmp	r0, #67
	bne	.L34
	b	.L35
.L26:
	ldr	r3, .L36
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L36+4
	mov	r3, #260
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L36+8
	ldr	r2, .L36+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L36+12
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	bl	SYSTEM_get_group_at_this_index
	cmp	r0, #0
	beq	.L28
	bl	nm_GROUP_get_group_ID
	mov	r5, r0
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	r6, r0, #0
	bne	.L29
	bl	bad_key_beep
	ldr	r0, .L36+4
	bl	RemovePathFromFileName
	mov	r2, #276
	mov	r1, r0
	ldr	r0, .L36+16
	bl	Alert_Message_va
	b	.L30
.L29:
	add	r0, r6, #516
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	subs	r7, r0, #0
	beq	.L31
	bl	good_key_beep
	mov	r0, r5
	bl	SYSTEM_clear_mainline_break
	b	.L30
.L31:
	ldrb	r3, [r6, #468]	@ zero_extendqisi2
	tst	r3, #96
	beq	.L28
	bl	good_key_beep
	ldr	r3, .L36+20
	mov	r2, #1
	str	r4, [r3, #216]
	str	r7, [r3, #220]
	str	r7, [r3, #224]
	str	r5, [r3, #228]
	str	r2, [r3, #212]
	b	.L30
.L28:
	bl	bad_key_beep
.L30:
	ldr	r3, .L36
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L36+8
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L35:
	ldr	r3, .L36+24
	ldr	r2, .L36+28
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r4, .L36+32
	ldr	r3, [r3, #4]
	str	r3, [r4, #0]
	bl	KEY_process_global_keys
	ldr	r3, [r4, #0]
	cmp	r3, #10
	ldmnefd	sp!, {r4, r5, r6, r7, pc}
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	LIVE_SCREENS_draw_dialog
.L34:
	mov	r2, #0
	mov	r3, r2
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	REPORTS_process_report
.L37:
	.align	2
.L36:
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	list_system_recursive_MUTEX
	.word	262
	.word	.LC1
	.word	irri_comm
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	REAL_TIME_SYSTEM_FLOW_process_report, .-REAL_TIME_SYSTEM_FLOW_process_report
	.section	.data.SECONDS_PER_HOUR.7968,"aw",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	SECONDS_PER_HOUR.7968, %object
	.size	SECONDS_PER_HOUR.7968, 4
SECONDS_PER_HOUR.7968:
	.word	1163984896
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_system_flow.c\000"
.LC1:
	.ascii	"System not found : %s, %u\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_rt_system_flow.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x59
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x25
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x8b
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xf2
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_system_flow.c\000"
.LASF4:
	.ascii	"REAL_TIME_SYSTEM_FLOW_draw_scroll_line\000"
.LASF1:
	.ascii	"REAL_TIME_SYSTEM_FLOW_process_report\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"FDTO_REAL_TIME_SYSTEM_FLOW_draw_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
