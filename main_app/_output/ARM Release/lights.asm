	.file	"lights.c"
	.text
.Ltext0:
	.section	.text.nm_light_structure_updater,"ax",%progbits
	.align	2
	.type	nm_light_structure_updater, %function
nm_light_structure_updater:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
.LFE10:
	.size	nm_light_structure_updater, .-nm_light_structure_updater
	.section	.text.nm_LIGHTS_set_box_index,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_box_index
	.type	nm_LIGHTS_set_box_index, %function
nm_LIGHTS_set_box_index:
.LFB1:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	sub	sp, sp, #44
.LCFI1:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L3
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #316
	str	r3, [sp, #32]
	mov	r3, #1
	str	r3, [sp, #36]
	ldr	r3, .L3+4
	mov	ip, #0
	mov	r1, #11
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #72
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L4:
	.align	2
.L3:
	.word	49296
	.word	.LC0
.LFE1:
	.size	nm_LIGHTS_set_box_index, .-nm_LIGHTS_set_box_index
	.section	.text.nm_LIGHTS_set_physically_available,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_physically_available
	.type	nm_LIGHTS_set_physically_available, %function
nm_LIGHTS_set_physically_available:
.LFB3:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	sub	sp, sp, #36
.LCFI3:
	str	r2, [sp, #0]
	ldr	r2, .L6
	mov	r5, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #48]
	mov	r2, r5
	str	r3, [sp, #12]
	ldr	r3, [sp, #52]
	add	r1, r0, #80
	str	r3, [sp, #16]
	ldr	r3, [sp, #56]
	mov	r4, r0
	str	r3, [sp, #20]
	add	r3, r0, #316
	str	r3, [sp, #24]
	mov	r3, #3
	str	r3, [sp, #28]
	ldr	r3, .L6+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	ldr	r0, [r4, #72]
	ldr	r1, [r4, #76]
	mov	r2, #0
	mov	r3, r5
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, lr}
	b	LIGHTS_PRESERVES_set_bit_field_bit
.L7:
	.align	2
.L6:
	.word	49298
	.word	.LC1
.LFE3:
	.size	nm_LIGHTS_set_physically_available, .-nm_LIGHTS_set_physically_available
	.section	.text.nm_LIGHTS_set_stop_time_2,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_stop_time_2
	.type	nm_LIGHTS_set_stop_time_2, %function
nm_LIGHTS_set_stop_time_2:
.LFB7:
	@ args = 16, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #13
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI4:
	mov	r5, r0
	sub	sp, sp, #64
.LCFI5:
	mov	r4, r1
	mov	sl, r2
	mov	r8, r3
	bhi	.L9
.LBB6:
	ldr	r3, .L11
	add	r7, r1, #4
	add	r6, sp, #44
	ldr	r3, [r3, r7, asl #2]
	mov	r0, r6
	mov	r1, #17
	ldr	r2, .L11+4
	bl	snprintf
	ldr	r3, .L11+8
	add	r1, r4, #6
	str	r3, [sp, #0]
	stmib	sp, {r3, r8}
	ldr	r3, [sp, #92]
	add	r4, r4, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #96]
	add	r4, r4, #189
	str	r3, [sp, #20]
	ldr	r3, [sp, #100]
	mov	r0, r5
	str	r3, [sp, #24]
	ldr	r3, [sp, #104]
	add	r1, r5, r1, asl #4
	str	r3, [sp, #28]
	add	r3, r5, #316
	str	r3, [sp, #32]
	mov	r2, sl
	mov	r3, #0
	str	r4, [sp, #12]
	str	r7, [sp, #36]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L8
.L9:
.LBE6:
	ldr	r0, .L11+12
	ldr	r1, .L11+16
	bl	Alert_index_out_of_range_with_filename
.L8:
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L12:
	.align	2
.L11:
	.word	.LANCHOR0
	.word	.LC2
	.word	86400
	.word	.LC3
	.word	1050
.LFE7:
	.size	nm_LIGHTS_set_stop_time_2, .-nm_LIGHTS_set_stop_time_2
	.section	.text.nm_LIGHTS_set_stop_time_1,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_stop_time_1
	.type	nm_LIGHTS_set_stop_time_1, %function
nm_LIGHTS_set_stop_time_1:
.LFB6:
	@ args = 16, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #13
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI6:
	mov	r5, r0
	sub	sp, sp, #64
.LCFI7:
	mov	r4, r1
	mov	sl, r2
	mov	r8, r3
	bhi	.L14
.LBB9:
	ldr	r3, .L16
	add	r7, r1, #4
	add	r6, sp, #44
	ldr	r3, [r3, r7, asl #2]
	mov	r0, r6
	mov	r1, #17
	ldr	r2, .L16+4
	bl	snprintf
	ldr	r3, .L16+8
	add	r1, r5, r4, asl #4
	str	r3, [sp, #0]
	stmib	sp, {r3, r8}
	ldr	r3, [sp, #92]
	add	r4, r4, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #96]
	add	r4, r4, #175
	str	r3, [sp, #20]
	ldr	r3, [sp, #100]
	mov	r0, r5
	str	r3, [sp, #24]
	ldr	r3, [sp, #104]
	add	r1, r1, #92
	str	r3, [sp, #28]
	add	r3, r5, #316
	str	r3, [sp, #32]
	mov	r2, sl
	mov	r3, #0
	str	r4, [sp, #12]
	str	r7, [sp, #36]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L13
.L14:
.LBE9:
	ldr	r0, .L16+12
	mov	r1, #960
	bl	Alert_index_out_of_range_with_filename
.L13:
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L17:
	.align	2
.L16:
	.word	.LANCHOR0
	.word	.LC4
	.word	86400
	.word	.LC3
.LFE6:
	.size	nm_LIGHTS_set_stop_time_1, .-nm_LIGHTS_set_stop_time_1
	.section	.text.nm_LIGHTS_set_start_time_2,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_start_time_2
	.type	nm_LIGHTS_set_start_time_2, %function
nm_LIGHTS_set_start_time_2:
.LFB5:
	@ args = 16, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #13
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI8:
	mov	r5, r0
	sub	sp, sp, #64
.LCFI9:
	mov	r4, r1
	mov	sl, r2
	mov	r8, r3
	bhi	.L19
.LBB12:
	ldr	r3, .L21
	add	r7, r1, #4
	add	r6, sp, #44
	ldr	r3, [r3, r7, asl #2]
	mov	r0, r6
	mov	r1, #17
	ldr	r2, .L21+4
	bl	snprintf
	ldr	r3, .L21+8
	add	r1, r5, r4, asl #4
	str	r3, [sp, #0]
	stmib	sp, {r3, r8}
	ldr	r3, [sp, #92]
	add	r4, r4, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #96]
	add	r4, r4, #161
	str	r3, [sp, #20]
	ldr	r3, [sp, #100]
	mov	r0, r5
	str	r3, [sp, #24]
	ldr	r3, [sp, #104]
	add	r1, r1, #88
	str	r3, [sp, #28]
	add	r3, r5, #316
	str	r3, [sp, #32]
	mov	r2, sl
	mov	r3, #0
	str	r4, [sp, #12]
	str	r7, [sp, #36]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L18
.L19:
.LBE12:
	ldr	r0, .L21+12
	ldr	r1, .L21+16
	bl	Alert_index_out_of_range_with_filename
.L18:
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L22:
	.align	2
.L21:
	.word	.LANCHOR0
	.word	.LC5
	.word	86400
	.word	.LC3
	.word	870
.LFE5:
	.size	nm_LIGHTS_set_start_time_2, .-nm_LIGHTS_set_start_time_2
	.section	.text.nm_LIGHTS_set_start_time_1,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_start_time_1
	.type	nm_LIGHTS_set_start_time_1, %function
nm_LIGHTS_set_start_time_1:
.LFB4:
	@ args = 16, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #13
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI10:
	mov	r5, r0
	sub	sp, sp, #64
.LCFI11:
	mov	r4, r1
	mov	sl, r2
	mov	r8, r3
	bhi	.L24
.LBB15:
	ldr	r3, .L26
	add	r7, r1, #4
	add	r6, sp, #44
	ldr	r3, [r3, r7, asl #2]
	mov	r0, r6
	mov	r1, #17
	ldr	r2, .L26+4
	bl	snprintf
	ldr	r3, .L26+8
	add	r1, r5, r4, asl #4
	str	r3, [sp, #0]
	stmib	sp, {r3, r8}
	ldr	r3, [sp, #92]
	add	r4, r4, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #96]
	add	r4, r4, #147
	str	r3, [sp, #20]
	ldr	r3, [sp, #100]
	mov	r0, r5
	str	r3, [sp, #24]
	ldr	r3, [sp, #104]
	add	r1, r1, #84
	str	r3, [sp, #28]
	add	r3, r5, #316
	str	r3, [sp, #32]
	mov	r2, sl
	mov	r3, #0
	str	r4, [sp, #12]
	str	r7, [sp, #36]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L23
.L24:
.LBE15:
	ldr	r0, .L26+12
	mov	r1, #780
	bl	Alert_index_out_of_range_with_filename
.L23:
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L27:
	.align	2
.L26:
	.word	.LANCHOR0
	.word	.LC6
	.word	86400
	.word	.LC3
.LFE4:
	.size	nm_LIGHTS_set_start_time_1, .-nm_LIGHTS_set_start_time_1
	.section	.text.nm_LIGHTS_set_name,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_name
	.type	nm_LIGHTS_set_name, %function
nm_LIGHTS_set_name:
.LFB0:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI12:
	ldr	ip, [sp, #24]
	str	ip, [sp, #0]
	ldr	ip, [sp, #28]
	str	ip, [sp, #4]
	ldr	ip, [sp, #32]
	str	ip, [sp, #8]
	add	ip, r0, #316
	str	ip, [sp, #12]
	mov	ip, #0
	str	ip, [sp, #16]
	bl	SHARED_set_name_32_bit_change_bits
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.LFE0:
	.size	nm_LIGHTS_set_name, .-nm_LIGHTS_set_name
	.section	.text.nm_LIGHTS_set_output_index,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_set_output_index
	.type	nm_LIGHTS_set_output_index, %function
nm_LIGHTS_set_output_index:
.LFB2:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI13:
	sub	sp, sp, #44
.LCFI14:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L30
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #316
	str	r3, [sp, #32]
	mov	r3, #2
	str	r3, [sp, #36]
	ldr	r3, .L30+4
	mov	ip, #0
	mov	r1, #3
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #76
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L31:
	.align	2
.L30:
	.word	49297
	.word	.LC7
.LFE2:
	.size	nm_LIGHTS_set_output_index, .-nm_LIGHTS_set_output_index
	.section	.text.nm_LIGHTS_set_default_values,"ax",%progbits
	.align	2
	.type	nm_LIGHTS_set_default_values, %function
nm_LIGHTS_set_default_values:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, sl, lr}
.LCFI15:
	mov	r4, r0
	mov	r8, r1
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #0
	mov	r6, r0
	beq	.L33
	ldr	r7, .L36
	ldr	r2, .L36+4
	mov	r3, #178
	add	r5, r4, #308
	ldr	r0, [r7, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [r7, #0]
	mov	r0, r5
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r7, #0]
	add	r0, r4, #312
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r7, #0]
	add	r0, r4, #320
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r7, #0]
	add	r0, r4, #316
	bl	SHARED_set_all_32_bit_change_bits
	mov	r0, r4
	ldr	r1, .L36+8
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_LIGHTS_set_name
	mov	r0, r4
	mov	r1, r6
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_LIGHTS_set_box_index
	mov	r1, #0
	mov	r2, r1
	mov	r0, r4
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_LIGHTS_set_output_index
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	stmia	sp, {r6, r8}
	str	r5, [sp, #8]
	bl	nm_LIGHTS_set_physically_available
	mov	r7, #0
	mov	sl, #11
.L34:
	mov	r1, r7
	mov	r0, r4
	ldr	r2, .L36+12
	mov	r3, #0
	str	sl, [sp, #0]
	stmib	sp, {r6, r8}
	str	r5, [sp, #12]
	bl	nm_LIGHTS_set_start_time_1
	mov	r1, r7
	mov	r0, r4
	ldr	r2, .L36+12
	mov	r3, #0
	str	sl, [sp, #0]
	stmib	sp, {r6, r8}
	str	r5, [sp, #12]
	bl	nm_LIGHTS_set_start_time_2
	mov	r1, r7
	mov	r0, r4
	ldr	r2, .L36+12
	mov	r3, #0
	str	sl, [sp, #0]
	stmib	sp, {r6, r8}
	str	r5, [sp, #12]
	bl	nm_LIGHTS_set_stop_time_1
	mov	r1, r7
	mov	r0, r4
	ldr	r2, .L36+12
	mov	r3, #0
	add	r7, r7, #1
	str	sl, [sp, #0]
	stmib	sp, {r6, r8}
	str	r5, [sp, #12]
	bl	nm_LIGHTS_set_stop_time_2
	cmp	r7, #14
	bne	.L34
	ldr	r3, .L36
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L33:
	ldr	r0, .L36+4
	mov	r1, #229
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Alert_func_call_with_null_ptr_with_filename
.L37:
	.align	2
.L36:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	.LANCHOR1
	.word	86400
.LFE13:
	.size	nm_LIGHTS_set_default_values, .-nm_LIGHTS_set_default_values
	.section	.text.init_file_LIGHTS,"ax",%progbits
	.align	2
	.global	init_file_LIGHTS
	.type	init_file_LIGHTS, %function
init_file_LIGHTS:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI16:
	ldr	r3, .L39
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, .L39+4
	ldr	r1, .L39+8
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r3, [sp, #4]
	ldr	r3, .L39+12
	str	r3, [sp, #8]
	ldr	r3, .L39+16
	str	r3, [sp, #12]
	ldr	r3, .L39+20
	str	r3, [sp, #16]
	mov	r3, #16
	str	r3, [sp, #20]
	ldr	r3, .L39+24
	bl	FLASH_FILE_initialize_list_and_find_or_create_group_list_file
	add	sp, sp, #24
	ldmfd	sp!, {pc}
.L40:
	.align	2
.L39:
	.word	.LANCHOR4
	.word	list_lights_recursive_MUTEX
	.word	.LANCHOR2
	.word	nm_light_structure_updater
	.word	nm_LIGHTS_set_default_values
	.word	.LANCHOR1
	.word	.LANCHOR3
.LFE11:
	.size	init_file_LIGHTS, .-init_file_LIGHTS
	.section	.text.save_file_LIGHTS,"ax",%progbits
	.align	2
	.global	save_file_LIGHTS
	.type	save_file_LIGHTS, %function
save_file_LIGHTS:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #324
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI17:
	ldr	r1, .L42
	str	r3, [sp, #0]
	ldr	r3, .L42+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, #0
	str	r3, [sp, #4]
	mov	r3, #16
	str	r3, [sp, #8]
	ldr	r3, .L42+8
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L43:
	.align	2
.L42:
	.word	.LANCHOR2
	.word	list_lights_recursive_MUTEX
	.word	.LANCHOR3
.LFE12:
	.size	save_file_LIGHTS, .-save_file_LIGHTS
	.section	.text.LIGHTS_copy_guivars_to_daily_schedule,"ax",%progbits
	.align	2
	.global	LIGHTS_copy_guivars_to_daily_schedule
	.type	LIGHTS_copy_guivars_to_daily_schedule, %function
LIGHTS_copy_guivars_to_daily_schedule:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L45
	ldr	r2, .L45+4
	ldr	r1, [r3, #0]
	mov	r3, #60
	mul	r1, r3, r1
	str	r1, [r2, r0, asl #4]
	add	r0, r2, r0, asl #4
	ldr	r2, .L45+8
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r0, #4]
	ldr	r2, .L45+12
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r0, #8]
	ldr	r2, .L45+16
	ldr	r2, [r2, #0]
	mul	r3, r2, r3
	str	r3, [r0, #12]
	bx	lr
.L46:
	.align	2
.L45:
	.word	GuiVar_LightsStartTime1InMinutes
	.word	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.word	GuiVar_LightsStartTime2InMinutes
	.word	GuiVar_LightsStopTime1InMinutes
	.word	GuiVar_LightsStopTime2InMinutes
.LFE16:
	.size	LIGHTS_copy_guivars_to_daily_schedule, .-LIGHTS_copy_guivars_to_daily_schedule
	.global	__udivsi3
	.section	.text.LIGHTS_copy_daily_schedule_to_guivars,"ax",%progbits
	.align	2
	.global	LIGHTS_copy_daily_schedule_to_guivars
	.type	LIGHTS_copy_daily_schedule_to_guivars, %function
LIGHTS_copy_daily_schedule_to_guivars:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI18:
	ldr	r4, .L48
	mov	r5, r0
	mov	r1, #60
	ldr	r0, [r4, r0, asl #4]
	bl	__udivsi3
	ldr	r3, .L48+4
	add	r4, r4, r5, asl #4
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r7, r0
	ldr	r0, [r4, #4]
	bl	__udivsi3
	ldr	r3, .L48+8
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r6, r0
	ldr	r0, [r4, #8]
	bl	__udivsi3
	ldr	r3, .L48+12
	mov	r1, #60
	str	r0, [r3, #0]
	mov	r5, r0
	ldr	r0, [r4, #12]
	bl	__udivsi3
	ldr	r3, .L48+16
	subs	r7, r7, #1440
	movne	r7, #1
	subs	r6, r6, #1440
	movne	r6, #1
	subs	r5, r5, #1440
	movne	r5, #1
	str	r0, [r3, #0]
	ldr	r3, .L48+20
	subs	r0, r0, #1440
	str	r7, [r3, #0]
	ldr	r3, .L48+24
	movne	r0, #1
	str	r6, [r3, #0]
	ldr	r3, .L48+28
	str	r5, [r3, #0]
	ldr	r3, .L48+32
	str	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L49:
	.align	2
.L48:
	.word	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.word	GuiVar_LightsStartTime1InMinutes
	.word	GuiVar_LightsStartTime2InMinutes
	.word	GuiVar_LightsStopTime1InMinutes
	.word	GuiVar_LightsStopTime2InMinutes
	.word	GuiVar_LightsStartTimeEnabled1
	.word	GuiVar_LightsStartTimeEnabled2
	.word	GuiVar_LightsStopTimeEnabled1
	.word	GuiVar_LightsStopTimeEnabled2
.LFE17:
	.size	LIGHTS_copy_daily_schedule_to_guivars, .-LIGHTS_copy_daily_schedule_to_guivars
	.section	.text.LIGHTS_get_index_using_ptr_to_light_struct,"ax",%progbits
	.align	2
	.global	LIGHTS_get_index_using_ptr_to_light_struct
	.type	LIGHTS_get_index_using_ptr_to_light_struct, %function
LIGHTS_get_index_using_ptr_to_light_struct:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L55
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI19:
	ldr	r2, .L55+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L55+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L55+12
	bl	nm_ListGetFirst
	mov	r5, #0
	ldr	r6, .L55+12
	mov	r1, r0
	b	.L51
.L53:
	cmp	r1, r4
	beq	.L52
	ldr	r0, .L55+12
	bl	nm_ListGetNext
	add	r5, r5, #1
	mov	r1, r0
.L51:
	ldr	r3, [r6, #8]
	cmp	r5, r3
	bcc	.L53
	mov	r5, #0
.L52:
	ldr	r3, .L55
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L56:
	.align	2
.L55:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	898
	.word	.LANCHOR3
.LFE24:
	.size	LIGHTS_get_index_using_ptr_to_light_struct, .-LIGHTS_get_index_using_ptr_to_light_struct
	.section	.text.nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	.type	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index, %function
nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L63
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI20:
	ldr	r2, .L63+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L63+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L63+12
	bl	nm_ListGetFirst
	b	.L62
.L61:
	ldr	r3, [r6, #72]
	cmp	r3, r5
	bne	.L59
	ldr	r3, [r6, #76]
	cmp	r3, r4
	beq	.L60
.L59:
	ldr	r0, .L63+12
	mov	r1, r6
	bl	nm_ListGetNext
.L62:
	cmp	r0, #0
	mov	r6, r0
	bne	.L61
.L60:
	ldr	r3, .L63
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, pc}
.L64:
	.align	2
.L63:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	946
	.word	.LANCHOR3
.LFE25:
	.size	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index, .-nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	.section	.text.LIGHTS_get_light_at_this_index,"ax",%progbits
	.align	2
	.global	LIGHTS_get_light_at_this_index
	.type	LIGHTS_get_light_at_this_index, %function
LIGHTS_get_light_at_this_index:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI21:
	ldr	r4, .L66
	ldr	r2, .L66+4
	ldr	r3, .L66+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L66+12
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L67:
	.align	2
.L66:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	993
	.word	.LANCHOR3
.LFE26:
	.size	LIGHTS_get_light_at_this_index, .-LIGHTS_get_light_at_this_index
	.section	.text.LIGHTS_load_group_name_into_guivar,"ax",%progbits
	.align	2
	.global	LIGHTS_load_group_name_into_guivar
	.type	LIGHTS_load_group_name_into_guivar, %function
LIGHTS_load_group_name_into_guivar:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI22:
	ldr	r4, .L71
	mov	r0, r0, asl #16
	mov	r5, r0, asr #16
	mov	r1, #400
	ldr	r2, .L71+4
	ldr	r3, .L71+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	LIGHTS_get_light_at_this_index
	mov	r5, r0
	mov	r1, r5
	ldr	r0, .L71+12
	bl	nm_OnList
	cmp	r0, #1
	bne	.L69
	mov	r0, r5
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L71+16
	bl	strlcpy
	b	.L70
.L69:
	ldr	r0, .L71+4
	mov	r1, #880
	bl	Alert_group_not_found_with_filename
.L70:
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L72:
	.align	2
.L71:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	870
	.word	.LANCHOR3
	.word	GuiVar_itmGroupName
.LFE23:
	.size	LIGHTS_load_group_name_into_guivar, .-LIGHTS_load_group_name_into_guivar
	.section	.text.LIGHTS_copy_file_schedule_into_global_daily_schedule,"ax",%progbits
	.align	2
	.global	LIGHTS_copy_file_schedule_into_global_daily_schedule
	.type	LIGHTS_copy_file_schedule_into_global_daily_schedule, %function
LIGHTS_copy_file_schedule_into_global_daily_schedule:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L76
	stmfd	sp!, {r4, lr}
.LCFI23:
	ldr	r2, .L76+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #720
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	LIGHTS_get_light_at_this_index
	ldr	r3, .L76+8
	add	r2, r3, #224
.L74:
	ldr	r1, [r0, #84]
	str	r1, [r3, #0]
	ldr	r1, [r0, #88]
	str	r1, [r3, #4]
	ldr	r1, [r0, #92]
	str	r1, [r3, #8]
	ldr	r1, [r0, #96]
	add	r0, r0, #16
	str	r1, [r3, #12]
	add	r3, r3, #16
	cmp	r3, r2
	bne	.L74
	ldr	r3, .L76
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L77:
	.align	2
.L76:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
.LFE20:
	.size	LIGHTS_copy_file_schedule_into_global_daily_schedule, .-LIGHTS_copy_file_schedule_into_global_daily_schedule
	.section	.text.LIGHTS_copy_light_struct_into_guivars,"ax",%progbits
	.align	2
	.global	LIGHTS_copy_light_struct_into_guivars
	.type	LIGHTS_copy_light_struct_into_guivars, %function
LIGHTS_copy_light_struct_into_guivars:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L81
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI24:
	ldr	r2, .L81+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L81+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	LIGHTS_get_light_at_this_index
	subs	r5, r0, #0
	beq	.L79
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L81+12
	ldr	r6, .L81+16
	ldr	r2, [r5, #80]
	str	r0, [r3, #0]
	ldr	r3, [r5, #72]
	mov	r0, r4
	str	r3, [r6, #0]
	ldr	r3, .L81+20
	str	r2, [r3, #0]
	ldr	r3, [r5, #76]
	ldr	r2, .L81+24
	str	r3, [r2, #0]
	ldr	r2, .L81+28
	add	r3, r3, #1
	str	r3, [r2, #0]
	bl	IRRI_LIGHTS_light_is_energized
	ldr	r3, .L81+32
	ldr	r1, .L81+36
	str	r0, [r3, #0]
	ldr	r0, [r6, #0]
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	b	.L80
.L79:
	ldr	r0, .L81+4
	mov	r1, #696
	bl	Alert_group_not_found_with_filename
.L80:
	ldr	r3, .L81
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L82:
	.align	2
.L81:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	673
	.word	g_GROUP_ID
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightIsPhysicallyAvailable
	.word	GuiVar_LightsOutputIndex_0
	.word	GuiVar_LightsOutput
	.word	GuiVar_LightIsEnergized
	.word	GuiVar_StationInfoControllerName
.LFE19:
	.size	LIGHTS_copy_light_struct_into_guivars, .-LIGHTS_copy_light_struct_into_guivars
	.section	.text.LIGHTS_populate_group_name,"ax",%progbits
	.align	2
	.global	LIGHTS_populate_group_name
	.type	LIGHTS_populate_group_name, %function
LIGHTS_populate_group_name:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L86
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI25:
	ldr	r2, .L86+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L86+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	LIGHTS_get_light_at_this_index
	cmp	r4, #0
	beq	.L84
	ldr	r3, .L86+12
	ldr	r4, [r0, #72]
	ldr	r5, [r3, #0]
	bl	nm_GROUP_get_name
	add	r4, r4, #65
	mov	r1, #65
	ldr	r2, .L86+16
	mov	r3, r4
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	ldr	r0, .L86+20
	bl	snprintf
	b	.L85
.L84:
	bl	nm_GROUP_get_name
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L86+20
	bl	strlcpy
.L85:
	ldr	r3, .L86
	ldr	r0, [r3, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L87:
	.align	2
.L86:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	639
	.word	GuiVar_LightsOutput
	.word	.LC9
	.word	GuiVar_GroupName
.LFE18:
	.size	LIGHTS_populate_group_name, .-LIGHTS_populate_group_name
	.section	.text.LIGHTS_get_list_count,"ax",%progbits
	.align	2
	.global	LIGHTS_get_list_count
	.type	LIGHTS_get_list_count, %function
LIGHTS_get_list_count:
.LFB27:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI26:
	ldr	r4, .L89
	mov	r1, #400
	ldr	r2, .L89+4
	ldr	r0, [r4, #0]
	ldr	r3, .L89+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L89+12
	ldr	r0, [r4, #0]
	ldr	r5, [r3, #8]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L90:
	.align	2
.L89:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1028
	.word	.LANCHOR3
.LFE27:
	.size	LIGHTS_get_list_count, .-LIGHTS_get_list_count
	.section	.text.LIGHTS_get_lights_array_index,"ax",%progbits
	.align	2
	.global	LIGHTS_get_lights_array_index
	.type	LIGHTS_get_lights_array_index, %function
LIGHTS_get_lights_array_index:
.LFB28:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L92
	stmfd	sp!, {r4, r5, lr}
.LCFI27:
	ldr	r5, .L92+4
	sub	sp, sp, #24
.LCFI28:
	str	r3, [sp, #4]
	ldr	r3, .L92+8
	mov	r4, #0
	str	r0, [sp, #20]
	str	r1, [sp, #16]
	str	r3, [sp, #12]
	mov	r1, r4
	mov	r3, r4
	add	r0, sp, #20
	mov	r2, #11
	str	r4, [sp, #0]
	str	r5, [sp, #8]
	bl	RC_uint32_with_filename
	ldr	r3, .L92+12
	add	r0, sp, #16
	stmib	sp, {r3, r5}
	ldr	r3, .L92+16
	mov	r1, r4
	str	r3, [sp, #12]
	mov	r2, #3
	mov	r3, r4
	str	r4, [sp, #0]
	bl	RC_uint32_with_filename
	ldr	r3, [sp, #16]
	ldr	r0, [sp, #20]
	add	r0, r3, r0, asl #2
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, pc}
.L93:
	.align	2
.L92:
	.word	.LC10
	.word	.LC8
	.word	1057
	.word	.LC11
	.word	1058
.LFE28:
	.size	LIGHTS_get_lights_array_index, .-LIGHTS_get_lights_array_index
	.section	.text.LIGHTS_get_box_index,"ax",%progbits
	.align	2
	.global	LIGHTS_get_box_index
	.type	LIGHTS_get_box_index, %function
LIGHTS_get_box_index:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI29:
	ldr	r4, .L95
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L95+4
	ldr	r3, .L95+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_get_controller_letter
	ldr	r3, .L95+12
	add	r1, r5, #72
	str	r3, [sp, #4]
	add	r3, r5, #308
	str	r3, [sp, #8]
	ldr	r3, .L95+16
	mov	r2, #0
	str	r3, [sp, #12]
	mov	r3, #11
	str	r0, [sp, #0]
	mov	r0, r5
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L96:
	.align	2
.L95:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1082
	.word	nm_LIGHTS_set_box_index
	.word	.LC0
.LFE29:
	.size	LIGHTS_get_box_index, .-LIGHTS_get_box_index
	.section	.text.LIGHTS_get_output_index,"ax",%progbits
	.align	2
	.global	LIGHTS_get_output_index
	.type	LIGHTS_get_output_index, %function
LIGHTS_get_output_index:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI30:
	ldr	r4, .L98
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L98+4
	ldr	r3, .L98+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_get_controller_letter
	ldr	r3, .L98+12
	add	r1, r5, #76
	str	r3, [sp, #4]
	add	r3, r5, #308
	str	r3, [sp, #8]
	ldr	r3, .L98+16
	mov	r2, #0
	str	r3, [sp, #12]
	mov	r3, #3
	str	r0, [sp, #0]
	mov	r0, r5
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L99:
	.align	2
.L98:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1115
	.word	nm_LIGHTS_set_box_index
	.word	.LC7
.LFE30:
	.size	LIGHTS_get_output_index, .-LIGHTS_get_output_index
	.section	.text.nm_LIGHTS_create_new_group,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_create_new_group
	.type	nm_LIGHTS_create_new_group, %function
nm_LIGHTS_create_new_group:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI31:
	mov	r4, r0
	ldr	r0, .L106
	mov	r5, r1
	bl	nm_ListGetFirst
	b	.L105
.L104:
	mov	r0, r6
	bl	LIGHTS_get_box_index
	cmp	r4, r0
	bcc	.L102
	mov	r0, r6
	bl	LIGHTS_get_box_index
	cmp	r4, r0
	bne	.L103
	mov	r0, r6
	bl	LIGHTS_get_output_index
	cmp	r5, r0
	bcc	.L102
.L103:
	ldr	r0, .L106
	mov	r1, r6
	bl	nm_ListGetNext
.L105:
	cmp	r0, #0
	mov	r6, r0
	bne	.L104
.L102:
	mov	r3, #1
	stmia	sp, {r3, r6}
	ldr	r1, .L106+4
	ldr	r2, .L106+8
	mov	r3, #324
	ldr	r0, .L106
	bl	nm_GROUP_create_new_group
	ldmfd	sp!, {r2, r3, r4, r5, r6, pc}
.L107:
	.align	2
.L106:
	.word	.LANCHOR3
	.word	.LANCHOR1
	.word	nm_LIGHTS_set_default_values
.LFE14:
	.size	nm_LIGHTS_create_new_group, .-nm_LIGHTS_create_new_group
	.section	.text.LIGHTS_get_physically_available,"ax",%progbits
	.align	2
	.global	LIGHTS_get_physically_available
	.type	LIGHTS_get_physically_available, %function
LIGHTS_get_physically_available:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI32:
	ldr	r4, .L109
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L109+4
	ldr	r3, .L109+8
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r5, #308
	str	r3, [sp, #0]
	ldr	r3, .L109+12
	add	r1, r5, #80
	str	r3, [sp, #4]
	mov	r2, #0
	ldr	r3, .L109+16
	mov	r0, r5
	bl	SHARED_get_bool_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L110:
	.align	2
.L109:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1156
	.word	.LC1
	.word	nm_LIGHTS_set_physically_available
.LFE31:
	.size	LIGHTS_get_physically_available, .-LIGHTS_get_physically_available
	.section	.text.LIGHTS_get_start_time_1,"ax",%progbits
	.align	2
	.global	LIGHTS_get_start_time_1
	.type	LIGHTS_get_start_time_1, %function
LIGHTS_get_start_time_1:
.LFB32:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI33:
	ldr	r6, .L112
	sub	sp, sp, #24
.LCFI34:
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r6, #0]
	mov	r1, #400
	ldr	r2, .L112+4
	ldr	r3, .L112+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L112+12
	add	r1, r4, #4
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	ldr	r3, .L112+16
	add	r2, r5, r4, asl #4
	str	r3, [sp, #12]
	add	r3, r5, #308
	str	r3, [sp, #16]
	ldr	r3, .L112+20
	add	r2, r2, #84
	ldr	r3, [r3, r1, asl #2]
	mov	r0, r5
	mov	r1, r4
	str	r3, [sp, #20]
	mov	r3, #14
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	mov	r4, r0
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, pc}
.L113:
	.align	2
.L112:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1193
	.word	86400
	.word	nm_LIGHTS_set_start_time_1
	.word	.LANCHOR0
.LFE32:
	.size	LIGHTS_get_start_time_1, .-LIGHTS_get_start_time_1
	.section	.text.LIGHTS_get_start_time_2,"ax",%progbits
	.align	2
	.global	LIGHTS_get_start_time_2
	.type	LIGHTS_get_start_time_2, %function
LIGHTS_get_start_time_2:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI35:
	ldr	r6, .L115
	sub	sp, sp, #24
.LCFI36:
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r6, #0]
	mov	r1, #400
	ldr	r2, .L115+4
	ldr	r3, .L115+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L115+12
	add	r1, r4, #4
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	ldr	r3, .L115+16
	add	r2, r5, r4, asl #4
	str	r3, [sp, #12]
	add	r3, r5, #308
	str	r3, [sp, #16]
	ldr	r3, .L115+20
	add	r2, r2, #88
	ldr	r3, [r3, r1, asl #2]
	mov	r0, r5
	mov	r1, r4
	str	r3, [sp, #20]
	mov	r3, #14
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	mov	r4, r0
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, pc}
.L116:
	.align	2
.L115:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1230
	.word	86400
	.word	nm_LIGHTS_set_start_time_2
	.word	.LANCHOR0
.LFE33:
	.size	LIGHTS_get_start_time_2, .-LIGHTS_get_start_time_2
	.section	.text.LIGHTS_get_stop_time_1,"ax",%progbits
	.align	2
	.global	LIGHTS_get_stop_time_1
	.type	LIGHTS_get_stop_time_1, %function
LIGHTS_get_stop_time_1:
.LFB34:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI37:
	ldr	r6, .L118
	sub	sp, sp, #24
.LCFI38:
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r6, #0]
	mov	r1, #400
	ldr	r2, .L118+4
	ldr	r3, .L118+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L118+12
	add	r1, r4, #4
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	ldr	r3, .L118+16
	add	r2, r5, r4, asl #4
	str	r3, [sp, #12]
	add	r3, r5, #308
	str	r3, [sp, #16]
	ldr	r3, .L118+20
	add	r2, r2, #92
	ldr	r3, [r3, r1, asl #2]
	mov	r0, r5
	mov	r1, r4
	str	r3, [sp, #20]
	mov	r3, #14
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	mov	r4, r0
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, pc}
.L119:
	.align	2
.L118:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1267
	.word	86400
	.word	nm_LIGHTS_set_stop_time_1
	.word	.LANCHOR0
.LFE34:
	.size	LIGHTS_get_stop_time_1, .-LIGHTS_get_stop_time_1
	.section	.text.LIGHTS_get_stop_time_2,"ax",%progbits
	.align	2
	.global	LIGHTS_get_stop_time_2
	.type	LIGHTS_get_stop_time_2, %function
LIGHTS_get_stop_time_2:
.LFB35:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI39:
	ldr	r5, .L121
	sub	sp, sp, #24
.LCFI40:
	mov	r6, r0
	mov	r4, r1
	ldr	r0, [r5, #0]
	mov	r1, #400
	ldr	r2, .L121+4
	ldr	r3, .L121+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L121+12
	add	r1, r4, #4
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	ldr	r3, .L121+16
	add	r2, r4, #6
	str	r3, [sp, #12]
	add	r3, r6, #308
	str	r3, [sp, #16]
	ldr	r3, .L121+20
	add	r2, r6, r2, asl #4
	ldr	r3, [r3, r1, asl #2]
	mov	r0, r6
	mov	r1, r4
	str	r3, [sp, #20]
	mov	r3, #14
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	mov	r4, r0
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, pc}
.L122:
	.align	2
.L121:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1304
	.word	86400
	.word	nm_LIGHTS_set_stop_time_2
	.word	.LANCHOR0
.LFE35:
	.size	LIGHTS_get_stop_time_2, .-LIGHTS_get_stop_time_2
	.global	__umodsi3
	.section	.text.LIGHTS_get_day_index,"ax",%progbits
	.align	2
	.global	LIGHTS_get_day_index
	.type	LIGHTS_get_day_index, %function
LIGHTS_get_day_index:
.LFB36:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	r0, r0, #40704
	str	lr, [sp, #-4]!
.LCFI41:
	mov	r1, #14
	sub	r0, r0, #203
	bl	__umodsi3
	ldr	pc, [sp], #4
.LFE36:
	.size	LIGHTS_get_day_index, .-LIGHTS_get_day_index
	.section	.text.LIGHTS_has_a_stop_time,"ax",%progbits
	.align	2
	.global	LIGHTS_has_a_stop_time
	.type	LIGHTS_has_a_stop_time, %function
LIGHTS_has_a_stop_time:
.LFB37:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI42:
	ldr	r5, .L130
	mov	r6, r0
	mov	r4, #0
.L126:
	mov	r0, r6
	mov	r1, r4
	bl	LIGHTS_get_stop_time_1
	cmp	r0, r5
	bne	.L127
	mov	r0, r6
	mov	r1, r4
	bl	LIGHTS_get_stop_time_2
	cmp	r0, r5
	bne	.L128
	add	r4, r4, #1
	cmp	r4, #14
	bne	.L126
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L127:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, pc}
.L128:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, pc}
.L131:
	.align	2
.L130:
	.word	86400
.LFE37:
	.size	LIGHTS_has_a_stop_time, .-LIGHTS_has_a_stop_time
	.section	.text.LIGHTS_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	LIGHTS_get_change_bits_ptr
	.type	LIGHTS_get_change_bits_ptr, %function
LIGHTS_get_change_bits_ptr:
.LFB38:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r0
	add	r2, r3, #312
	mov	r0, r1
	add	r1, r3, #308
	add	r3, r3, #316
	b	SHARED_get_32_bit_change_bits_ptr
.LFE38:
	.size	LIGHTS_get_change_bits_ptr, .-LIGHTS_get_change_bits_ptr
	.section	.text.nm_LIGHTS_store_changes,"ax",%progbits
	.align	2
	.type	nm_LIGHTS_store_changes, %function
nm_LIGHTS_store_changes:
.LFB8:
	@ args = 12, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI43:
	mov	r7, r0
	sub	sp, sp, #24
.LCFI44:
	mov	fp, r1
	mov	r4, r2
	ldr	r0, .L149
	mov	r1, r7
	add	r2, sp, #20
	mov	r5, r3
	ldr	sl, [sp, #60]
	ldr	r6, [sp, #64]
	bl	nm_GROUP_find_this_group_in_list
	ldr	r3, [sp, #20]
	cmp	r3, #0
	beq	.L134
	cmp	fp, #1
	bne	.L135
	sub	r3, r6, #15
	cmp	r3, #1
	bls	.L135
	mov	r0, r7
	bl	nm_GROUP_get_name
	mov	r3, #48
	mov	r2, #0
	stmia	sp, {r3, r4, r5}
	mov	r3, r2
	mov	r1, r0
	mov	r0, #49152
	bl	Alert_ChangeLine_Group
.L135:
	mov	r1, r6
	ldr	r0, [sp, #20]
	bl	LIGHTS_get_change_bits_ptr
	cmp	r4, #2
	mov	r6, r0
	beq	.L136
	ldr	r2, [sp, #68]
	tst	r2, #1
	beq	.L137
.L136:
	mov	r0, r7
	ldr	r8, [sp, #20]
	bl	nm_GROUP_get_name
	rsbs	r2, fp, #1
	movcc	r2, #0
	mov	r3, r4
	stmia	sp, {r5, sl}
	str	r6, [sp, #8]
	mov	r1, r0
	mov	r0, r8
	bl	nm_LIGHTS_set_name
	cmp	r4, #2
	beq	.L138
.L137:
	ldr	r3, [sp, #68]
	tst	r3, #2
	beq	.L139
.L138:
	rsbs	r2, fp, #1
	stmia	sp, {r5, sl}
	ldr	r0, [sp, #20]
	str	r6, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r7, #72]
	mov	r3, r4
	bl	nm_LIGHTS_set_box_index
	cmp	r4, #2
	beq	.L140
.L139:
	ldr	r2, [sp, #68]
	tst	r2, #4
	beq	.L141
.L140:
	rsbs	r2, fp, #1
	stmia	sp, {r5, sl}
	ldr	r0, [sp, #20]
	str	r6, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r7, #76]
	mov	r3, r4
	bl	nm_LIGHTS_set_output_index
	cmp	r4, #2
	beq	.L142
.L141:
	ldr	r3, [sp, #68]
	tst	r3, #8
	beq	.L143
.L142:
	rsbs	r2, fp, #1
	stmia	sp, {r5, sl}
	ldr	r0, [sp, #20]
	str	r6, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r7, #80]
	mov	r3, r4
	bl	nm_LIGHTS_set_physically_available
.L143:
	mov	r9, r7
	mov	r8, #0
	mov	r7, r6
	mov	ip, #1
	mov	r6, r5
	mov	r5, r4
.L146:
	cmp	r5, #2
	beq	.L144
	ldr	r2, [sp, #68]
	add	r3, r8, #4
	ands	r2, r2, ip, asl r3
	beq	.L145
.L144:
	rsbs	r4, fp, #1
	movcc	r4, #0
	stmia	sp, {r5, r6, sl}
	ldr	r0, [sp, #20]
	str	r7, [sp, #12]
	mov	r1, r8
	ldr	r2, [r9, #84]
	mov	r3, r4
	str	ip, [sp, #16]
	bl	nm_LIGHTS_set_start_time_1
	stmia	sp, {r5, r6, sl}
	ldr	r0, [sp, #20]
	str	r7, [sp, #12]
	mov	r1, r8
	ldr	r2, [r9, #88]
	mov	r3, r4
	bl	nm_LIGHTS_set_start_time_2
	stmia	sp, {r5, r6, sl}
	ldr	r0, [sp, #20]
	str	r7, [sp, #12]
	mov	r1, r8
	ldr	r2, [r9, #92]
	mov	r3, r4
	bl	nm_LIGHTS_set_stop_time_1
	stmia	sp, {r5, r6, sl}
	ldr	r0, [sp, #20]
	str	r7, [sp, #12]
	mov	r1, r8
	ldr	r2, [r9, #96]
	mov	r3, r4
	bl	nm_LIGHTS_set_stop_time_2
	ldr	ip, [sp, #16]
.L145:
	add	r8, r8, #1
	cmp	r8, #14
	add	r9, r9, #16
	bne	.L146
	b	.L133
.L134:
	ldr	r0, .L149+4
	ldr	r1, .L149+8
	bl	Alert_group_not_found_with_filename
.L133:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L150:
	.align	2
.L149:
	.word	.LANCHOR3
	.word	.LC3
	.word	1268
.LFE8:
	.size	nm_LIGHTS_store_changes, .-nm_LIGHTS_store_changes
	.section	.text.LIGHTS_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	LIGHTS_extract_and_store_changes_from_GuiVars
	.type	LIGHTS_extract_and_store_changes_from_GuiVars, %function
LIGHTS_extract_and_store_changes_from_GuiVars:
.LFB22:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L154
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, lr}
.LCFI45:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L154+4
	ldr	r2, .L154+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, .L154+12
	ldr	r1, .L154+8
	mov	r0, #324
	bl	mem_malloc_debug
	ldr	r3, .L154+16
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L154+20
	ldr	r1, [r3, #0]
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	mov	r2, #324
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	mov	r2, #48
	ldr	r1, .L154+24
	add	r0, r4, #20
	bl	strlcpy
.LBB16:
	ldr	r3, .L154+28
	mov	r1, #14
	ldr	r0, [r3, #0]
	sub	r0, r0, #40704
	sub	r0, r0, #203
	bl	__umodsi3
.LBE16:
	bl	LIGHTS_copy_guivars_to_daily_schedule
	ldr	r3, .L154+32
	mov	r2, r4
	add	r1, r3, #224
.L152:
	ldr	r0, [r3, #0]
	str	r0, [r2, #84]
	ldr	r0, [r3, #4]
	str	r0, [r2, #88]
	ldr	r0, [r3, #8]
	str	r0, [r2, #92]
	ldr	r0, [r3, #12]
	add	r3, r3, #16
	cmp	r3, r1
	str	r0, [r2, #96]
	add	r2, r2, #16
	bne	.L152
	ldr	r5, .L154+36
	mov	r6, #0
	ldr	r7, [r5, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r1, r7
	mov	r2, #2
	stmib	sp, {r2, r6}
	mov	r3, r0
	mov	r0, r4
	bl	nm_LIGHTS_store_changes
	mov	r0, r4
	ldr	r1, .L154+8
	ldr	r2, .L154+40
	bl	mem_free_debug
	ldr	r3, .L154
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	str	r6, [r5, #0]
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, pc}
.L155:
	.align	2
.L154:
	.word	list_lights_recursive_MUTEX
	.word	801
	.word	.LC8
	.word	805
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	GuiVar_GroupName
	.word	g_LIGHTS_numeric_date
	.word	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.word	g_GROUP_creating_new
	.word	837
.LFE22:
	.size	LIGHTS_extract_and_store_changes_from_GuiVars, .-LIGHTS_extract_and_store_changes_from_GuiVars
	.section	.text.nm_LIGHTS_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_extract_and_store_changes_from_comm
	.type	nm_LIGHTS_extract_and_store_changes_from_comm, %function
nm_LIGHTS_extract_and_store_changes_from_comm:
.LFB9:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI46:
	mov	r8, r0
	sub	sp, sp, #44
.LCFI47:
	str	r1, [sp, #16]
	str	r2, [sp, #20]
	mov	r1, r8
	add	r0, sp, #28
	mov	r2, #4
	mov	fp, #0
	mov	r7, r3
	add	r8, r8, #4
	bl	memcpy
	mov	sl, #4
	str	fp, [sp, #12]
	b	.L157
.L168:
	mov	r1, r8
	mov	r2, #4
	add	r0, sp, #32
	bl	memcpy
	add	r1, r8, #4
	mov	r2, #4
	add	r0, sp, #36
	bl	memcpy
	add	r1, r8, #8
	mov	r2, #4
	add	r0, sp, #40
	bl	memcpy
	add	r1, r8, #12
	mov	r2, #4
	add	r0, sp, #24
	bl	memcpy
	ldr	r0, [sp, #32]
	ldr	r1, [sp, #36]
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	add	r4, r8, #16
	add	r5, sl, #16
	subs	r9, r0, #0
	bne	.L158
	ldr	r0, [sp, #32]
	ldr	r1, [sp, #36]
	bl	nm_LIGHTS_create_new_group
	cmp	r7, #16
	mov	r9, r0
	bne	.L159
	ldr	r3, .L173
	add	r0, r0, #308
	ldr	r1, [r3, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L159:
	cmp	r9, #0
	beq	.L160
	mov	r1, #1
	str	r1, [sp, #12]
.L158:
	ldr	r1, .L173+4
	ldr	r2, .L173+8
	mov	r0, #324
	bl	mem_malloc_debug
	mov	r1, #0
	mov	r2, #324
	mov	r6, r0
	bl	memset
	mov	r0, r6
	mov	r1, r9
	mov	r2, #324
	bl	memcpy
	ldr	r3, [sp, #24]
	tst	r3, #1
	beq	.L161
	mov	r1, r4
	add	r0, r6, #20
	mov	r2, #48
	bl	memcpy
	add	r4, r8, #64
	add	r5, sl, #64
.L161:
	ldr	r3, [sp, #24]
	tst	r3, #2
	beq	.L162
	mov	r1, r4
	add	r0, r6, #72
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L162:
	ldr	r3, [sp, #24]
	tst	r3, #4
	beq	.L163
	mov	r1, r4
	add	r0, r6, #76
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L163:
	ldr	r3, [sp, #24]
	tst	r3, #8
	beq	.L164
	mov	r1, r4
	add	r0, r6, #80
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L164:
	mov	r8, #0
.L166:
	ldr	r3, [sp, #24]
	add	r2, r8, #4
	mov	r1, #1
	ands	r3, r3, r1, asl r2
	beq	.L165
	add	r0, r6, r8, asl #4
	mov	r1, r4
	add	r0, r0, #84
	mov	r2, #16
	bl	memcpy
	add	r4, r4, #16
	add	r5, r5, #16
.L165:
	add	r8, r8, #1
	cmp	r8, #14
	bne	.L166
	ldr	r3, [sp, #20]
	mov	r0, r6
	stmia	sp, {r3, r7}
	ldr	r3, [sp, #24]
	ldr	r1, [sp, #12]
	str	r3, [sp, #8]
	ldr	r2, [sp, #16]
	mov	r3, #0
	bl	nm_LIGHTS_store_changes
	mov	r0, r6
	ldr	r1, .L173+4
	ldr	r2, .L173+12
	bl	mem_free_debug
	b	.L167
.L160:
	ldr	r1, .L173+16
	ldr	r0, .L173+4
	bl	Alert_group_not_found_with_filename
	mov	r1, #1
	str	r1, [sp, #12]
.L167:
	add	fp, fp, #1
	mov	sl, r5
	mov	r8, r4
.L157:
	ldr	r3, [sp, #28]
	cmp	fp, r3
	bcc	.L168
	cmp	sl, #0
	beq	.L169
	cmp	r7, #1
	cmpne	r7, #15
	beq	.L170
	cmp	r7, #16
	bne	.L171
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L171
.L170:
	mov	r0, #16
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L171
.L169:
	ldr	r0, .L173+4
	ldr	r1, .L173+20
	bl	Alert_bit_set_with_no_data_with_filename
.L171:
	mov	r0, sl
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L174:
	.align	2
.L173:
	.word	list_lights_recursive_MUTEX
	.word	.LC3
	.word	1453
	.word	1562
	.word	1581
	.word	1615
.LFE9:
	.size	nm_LIGHTS_extract_and_store_changes_from_comm, .-nm_LIGHTS_extract_and_store_changes_from_comm
	.section	.text.LIGHTS_extract_and_store_group_name_from_GuiVars,"ax",%progbits
	.align	2
	.global	LIGHTS_extract_and_store_group_name_from_GuiVars
	.type	LIGHTS_extract_and_store_group_name_from_GuiVars, %function
LIGHTS_extract_and_store_group_name_from_GuiVars:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L177
	stmfd	sp!, {r4, r5, lr}
.LCFI48:
	ldr	r0, [r3, #0]
	sub	sp, sp, #20
.LCFI49:
	mov	r1, #400
	ldr	r2, .L177+4
	ldr	r3, .L177+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L177+12
	ldr	r0, [r3, #0]
	bl	LIGHTS_get_light_at_this_index
	subs	r4, r0, #0
	beq	.L176
	bl	FLOWSENSE_get_controller_index
	mov	r1, #2
	mov	r5, r0
	mov	r0, r4
	bl	LIGHTS_get_change_bits_ptr
	add	r3, r4, #316
	str	r3, [sp, #12]
	mov	r3, #0
	mov	r2, #1
	str	r3, [sp, #16]
	ldr	r1, .L177+16
	mov	r3, #2
	str	r5, [sp, #0]
	str	r2, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	SHARED_set_name_32_bit_change_bits
.L176:
	ldr	r3, .L177
	ldr	r0, [r3, #0]
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L178:
	.align	2
.L177:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	763
	.word	g_GROUP_list_item_index
	.word	GuiVar_GroupName
.LFE21:
	.size	LIGHTS_extract_and_store_group_name_from_GuiVars, .-LIGHTS_extract_and_store_group_name_from_GuiVars
	.section	.text.LIGHTS_build_data_to_send,"ax",%progbits
	.align	2
	.global	LIGHTS_build_data_to_send
	.type	LIGHTS_build_data_to_send, %function
LIGHTS_build_data_to_send:
.LFB15:
	@ args = 4, pretend = 0, frame = 44
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI50:
	mov	sl, r3
	sub	sp, sp, #72
.LCFI51:
	mov	r3, #0
	str	r3, [sp, #68]
	ldr	r3, .L197
	str	r1, [sp, #48]
	mov	r4, r0
	mov	r6, r2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L197+4
	ldr	r3, .L197+8
	ldr	fp, [sp, #108]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L197+12
	bl	nm_ListGetFirst
	b	.L195
.L183:
	mov	r0, r5
	mov	r1, sl
	bl	LIGHTS_get_change_bits_ptr
	ldr	r3, [r0, #0]
	cmp	r3, #0
	ldrne	r3, [sp, #68]
	addne	r3, r3, #1
	strne	r3, [sp, #68]
	bne	.L182
.L181:
	ldr	r0, .L197+12
	mov	r1, r5
	bl	nm_ListGetNext
.L195:
	cmp	r0, #0
	mov	r5, r0
	bne	.L183
.L182:
	ldr	r9, [sp, #68]
	cmp	r9, #0
	beq	.L184
	mov	r3, #0
	str	r3, [sp, #68]
	mov	r0, r4
	mov	r3, r6
	add	r1, sp, #60
	ldr	r2, [sp, #48]
	str	fp, [sp, #0]
	bl	PDATA_allocate_space_for_num_changed_groups_in_pucp
	ldr	r3, [r6, #0]
	cmp	r3, #0
	mov	r9, r0
	beq	.L184
	ldr	r0, .L197+12
	bl	nm_ListGetFirst
	b	.L196
.L191:
	mov	r0, r5
	mov	r1, sl
	bl	LIGHTS_get_change_bits_ptr
	ldr	r3, [r0, #0]
	mov	r7, r0
	cmp	r3, #0
	beq	.L186
	ldr	r3, [r6, #0]
	cmp	r3, #0
	beq	.L187
	ldr	r1, [sp, #48]
	add	ip, r1, #16
	add	ip, ip, r9
	cmp	ip, fp
	bcs	.L187
	add	r2, r5, #72
	mov	r1, r2
	str	r2, [sp, #32]
	mov	r0, r4
	mov	r2, #4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp
	add	r3, r5, #76
	mov	r1, r3
	mov	r0, r4
	mov	r2, #4
	str	r3, [sp, #44]
	bl	PDATA_copy_var_into_pucp
	mov	r1, #4
	mov	r2, r1
	mov	r0, r4
	add	r3, sp, #64
	bl	PDATA_copy_bitfield_info_into_pucp
	add	r8, sp, #72
	add	r3, r5, #20
	ldr	ip, [sp, #28]
	mov	r1, #0
	str	r1, [r8, #-16]!
	add	r2, r5, #320
	str	r3, [sp, #4]
	mov	r3, #48
	str	r2, [sp, #36]
	str	r2, [sp, #0]
	str	r3, [sp, #8]
	mov	r0, r4
	mov	r1, r8
	mov	r2, #0
	mov	r3, r7
	str	ip, [sp, #12]
	str	r6, [sp, #16]
	str	fp, [sp, #20]
	str	sl, [sp, #24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r3, [sp, #48]
	ldr	r1, [sp, #36]
	ldr	r2, [sp, #32]
	add	r3, r9, r3
	str	r3, [sp, #40]
	mov	ip, #4
	str	r1, [sp, #0]
	str	r2, [sp, #4]
	mov	r1, r8
	mov	r2, #1
	str	ip, [sp, #8]
	str	ip, [sp, #28]
	str	r6, [sp, #16]
	str	fp, [sp, #20]
	str	sl, [sp, #24]
	add	r0, r0, #16
	add	r3, r0, r3
	str	r0, [sp, #52]
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r2, [sp, #44]
	ldr	r3, [sp, #52]
	str	r2, [sp, #4]
	ldr	r2, [sp, #40]
	ldr	r1, [sp, #36]
	ldr	ip, [sp, #28]
	str	r1, [sp, #0]
	mov	r1, r8
	str	ip, [sp, #8]
	str	r6, [sp, #16]
	str	fp, [sp, #20]
	str	sl, [sp, #24]
	add	r0, r3, r0
	add	r3, r0, r2
	str	r0, [sp, #32]
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r2, #2
	mov	r3, r7
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r1, [sp, #36]
	ldr	r3, [sp, #32]
	str	r1, [sp, #0]
	ldr	r1, [sp, #40]
	ldr	ip, [sp, #28]
	mov	r2, #3
	str	ip, [sp, #8]
	str	r6, [sp, #16]
	str	fp, [sp, #20]
	str	sl, [sp, #24]
	add	r0, r3, r0
	add	r3, r5, #80
	str	r3, [sp, #4]
	add	r3, r0, r1
	str	r0, [sp, #32]
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r1, r8
	mov	r3, r7
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r2, [sp, #32]
	mov	ip, #0
	str	r9, [sp, #44]
	mov	r9, r5
	mov	r5, r4
	mov	r4, ip
	add	r0, r2, r0
	str	r0, [sp, #32]
	b	.L188
.L187:
	mov	r3, #0
	str	r3, [r6, #0]
	b	.L189
.L188:
	ldr	r3, [sp, #36]
	mov	r1, #16
	ldr	r2, [sp, #32]
	str	r1, [sp, #8]
	ldr	r1, [sp, #40]
	str	r3, [sp, #0]
	add	r3, r9, r4, asl #4
	add	r3, r3, #84
	str	r3, [sp, #4]
	add	r3, r2, r1
	str	r3, [sp, #12]
	add	r2, r4, #4
	mov	r0, r5
	mov	r1, r8
	mov	r3, r7
	str	r6, [sp, #16]
	str	fp, [sp, #20]
	str	sl, [sp, #24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r2, [sp, #32]
	add	r4, r4, #1
	cmp	r4, #14
	add	r2, r2, r0
	str	r2, [sp, #32]
	bne	.L188
	cmp	r2, #16
	mov	r4, r5
	mov	r5, r9
	ldr	r9, [sp, #44]
	bls	.L190
	ldr	r3, [sp, #68]
	ldr	r0, [sp, #64]
	add	r3, r3, #1
	mov	r1, r8
	mov	r2, #4
	str	r3, [sp, #68]
	bl	memcpy
	ldr	r3, [sp, #32]
	add	r9, r9, r3
	b	.L186
.L190:
	ldr	r3, [r4, #0]
	sub	r3, r3, #16
	str	r3, [r4, #0]
.L186:
	ldr	r0, .L197+12
	mov	r1, r5
	bl	nm_ListGetNext
.L196:
	cmp	r0, #0
	mov	r5, r0
	bne	.L191
.L189:
	ldr	r3, [sp, #68]
	cmp	r3, #0
	beq	.L192
	ldr	r0, [sp, #60]
	add	r1, sp, #68
	mov	r2, #4
	bl	memcpy
	b	.L184
.L192:
	ldr	r2, [r4, #0]
	mov	r9, r3
	sub	r2, r2, #4
	str	r2, [r4, #0]
.L184:
	ldr	r3, .L197
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r9
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L198:
	.align	2
.L197:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	355
	.word	.LANCHOR3
.LFE15:
	.size	LIGHTS_build_data_to_send, .-LIGHTS_build_data_to_send
	.section	.text.nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars
	.type	nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars, %function
nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars:
.LFB39:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L211
	stmfd	sp!, {r4, lr}
.LCFI52:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L211+4
	ldr	r2, .L211+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L211+12
	ldr	r4, [r3, #8]
	cmp	r4, #0
	beq	.L200
	ldr	r3, .L211+16
	ldr	r0, [r3, #0]
	ldr	r3, .L211+20
	ldr	r1, [r3, #0]
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	subs	r4, r0, #0
	beq	.L201
	bl	LIGHTS_get_physically_available
	cmp	r0, #0
	bne	.L202
.L201:
	ldr	r0, .L211+12
	bl	nm_ListGetFirst
.L210:
	subs	r4, r0, #0
	beq	.L203
	bl	LIGHTS_get_physically_available
	cmp	r0, #0
	bne	.L202
	mov	r1, r4
	ldr	r0, .L211+12
	bl	nm_ListGetNext
	b	.L210
.L202:
	mov	r0, r4
	bl	LIGHTS_get_box_index
	ldr	r3, .L211+16
	str	r0, [r3, #0]
	mov	r0, r4
	bl	LIGHTS_get_output_index
	ldr	r3, .L211+20
	str	r0, [r3, #0]
.L200:
	ldr	r3, .L211
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L203:
	ldr	r0, .L211+24
	bl	Alert_Message
	mov	r4, #0
	b	.L200
.L212:
	.align	2
.L211:
	.word	list_lights_recursive_MUTEX
	.word	1412
	.word	.LC8
	.word	.LANCHOR3
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	.LC12
.LFE39:
	.size	nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars, .-nm_LIGHTS_get_first_available_light_and_init_lights_output_GuiVars
	.section	.text.nm_LIGHTS_get_next_available_light,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_get_next_available_light
	.type	nm_LIGHTS_get_next_available_light, %function
nm_LIGHTS_get_next_available_light:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L223
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI53:
	ldr	r2, .L223+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L223+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, #0]
	ldr	r1, [r4, #0]
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	subs	r6, r0, #0
	beq	.L214
.L221:
	ldr	r0, [r6, #8]
	mov	r1, r6
	bl	nm_ListGetNext
	subs	r6, r0, #0
	bne	.L215
	ldr	r0, .L223+12
	bl	nm_ListGetFirst
	mov	r6, r0
.L215:
	ldr	r3, [r6, #72]
	ldr	r2, [r5, #0]
	cmp	r3, r2
	bne	.L216
	ldr	r1, [r6, #76]
	ldr	r2, [r4, #0]
	cmp	r1, r2
	beq	.L217
.L216:
	ldr	r2, [r6, #80]
	cmp	r2, #0
	beq	.L221
.L217:
	str	r3, [r5, #0]
	ldr	r3, [r6, #76]
	str	r3, [r4, #0]
	b	.L219
.L214:
	ldr	r0, .L223+4
	ldr	r1, .L223+16
	bl	Alert_group_not_found_with_filename
.L219:
	ldr	r3, .L223
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, pc}
.L224:
	.align	2
.L223:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1487
	.word	.LANCHOR3
	.word	1526
.LFE40:
	.size	nm_LIGHTS_get_next_available_light, .-nm_LIGHTS_get_next_available_light
	.section	.text.nm_LIGHTS_get_prev_available_light,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_get_prev_available_light
	.type	nm_LIGHTS_get_prev_available_light, %function
nm_LIGHTS_get_prev_available_light:
.LFB41:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L235
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI54:
	ldr	r2, .L235+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L235+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, #0]
	ldr	r1, [r4, #0]
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	subs	r6, r0, #0
	beq	.L226
.L233:
	ldr	r0, [r6, #8]
	mov	r1, r6
	bl	nm_ListGetPrev
	subs	r6, r0, #0
	bne	.L227
	ldr	r0, .L235+12
	bl	nm_ListGetLast
	mov	r6, r0
.L227:
	ldr	r3, [r6, #72]
	ldr	r2, [r5, #0]
	cmp	r3, r2
	bne	.L228
	ldr	r1, [r6, #76]
	ldr	r2, [r4, #0]
	cmp	r1, r2
	beq	.L229
.L228:
	ldr	r2, [r6, #80]
	cmp	r2, #0
	beq	.L233
.L229:
	str	r3, [r5, #0]
	ldr	r3, [r6, #76]
	str	r3, [r4, #0]
	b	.L231
.L226:
	ldr	r0, .L235+4
	ldr	r1, .L235+16
	bl	Alert_group_not_found_with_filename
.L231:
	ldr	r3, .L235
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, pc}
.L236:
	.align	2
.L235:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1564
	.word	.LANCHOR3
	.word	1606
.LFE41:
	.size	nm_LIGHTS_get_prev_available_light, .-nm_LIGHTS_get_prev_available_light
	.section	.text.LIGHTS_clean_house_processing,"ax",%progbits
	.align	2
	.global	LIGHTS_clean_house_processing
	.type	LIGHTS_clean_house_processing, %function
LIGHTS_clean_house_processing:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L241
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI55:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L241+4
	ldr	r3, .L241+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L241+12
	ldr	r1, .L241+4
	mov	r2, #1632
	b	.L240
.L239:
	mov	r0, r4
	ldr	r1, .L241+4
	ldr	r2, .L241+16
	bl	mem_free_debug
	ldr	r0, .L241+12
	ldr	r1, .L241+4
	ldr	r2, .L241+20
.L240:
	bl	nm_ListRemoveHead_debug
	cmp	r0, #0
	mov	r4, r0
	bne	.L239
	ldr	r5, .L241
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r3, #324
	str	r3, [sp, #0]
	ldr	r3, [r5, #0]
	mov	r0, #1
	str	r3, [sp, #4]
	mov	r3, #16
	str	r3, [sp, #8]
	ldr	r1, .L241+24
	mov	r2, r4
	ldr	r3, .L241+12
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L242:
	.align	2
.L241:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1630
	.word	.LANCHOR3
	.word	1636
	.word	1638
	.word	.LANCHOR2
.LFE42:
	.size	LIGHTS_clean_house_processing, .-LIGHTS_clean_house_processing
	.section	.text.LIGHTS_set_not_physically_available_based_upon_communication_scan_results,"ax",%progbits
	.align	2
	.global	LIGHTS_set_not_physically_available_based_upon_communication_scan_results
	.type	LIGHTS_set_not_physically_available_based_upon_communication_scan_results, %function
LIGHTS_set_not_physically_available_based_upon_communication_scan_results:
.LFB43:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L249
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI56:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L249+4
	ldr	r3, .L249+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r6, .L249+12
	mov	r4, #0
	mov	r7, #1
	b	.L244
.L246:
	ldr	r8, [r6, #16]
	cmp	r8, #0
	bne	.L245
	mov	r0, r4
	mov	r1, r5
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	subs	sl, r0, #0
	beq	.L245
	mov	r1, #10
	bl	LIGHTS_get_change_bits_ptr
	mov	r1, r8
	mov	r2, #1
	mov	r3, #10
	stmia	sp, {r4, r7}
	str	r0, [sp, #8]
	mov	r0, sl
	bl	nm_LIGHTS_set_physically_available
.L245:
	add	r5, r5, #1
	cmp	r5, #4
	bne	.L246
	add	r4, r4, #1
	cmp	r4, #12
	add	r6, r6, #92
	beq	.L247
.L244:
	mov	r5, #0
	b	.L246
.L247:
	ldr	r3, .L249
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L250:
	.align	2
.L249:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1669
	.word	chain
.LFE43:
	.size	LIGHTS_set_not_physically_available_based_upon_communication_scan_results, .-LIGHTS_set_not_physically_available_based_upon_communication_scan_results
	.section	.text.LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token
	.type	LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token, %function
LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token:
.LFB44:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI57:
	ldr	r4, .L255
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L255+4
	ldr	r3, .L255+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L255+12
	bl	nm_ListGetFirst
	b	.L254
.L253:
	add	r0, r5, #312
	ldr	r1, [r4, #0]
	bl	SHARED_set_all_32_bit_change_bits
	ldr	r0, .L255+12
	mov	r1, r5
	bl	nm_ListGetNext
.L254:
	cmp	r0, #0
	mov	r5, r0
	bne	.L253
	ldr	r3, .L255
	ldr	r4, .L255+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L255+4
	ldr	r3, .L255+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L255+24
	ldr	r0, [r4, #0]
	mov	r2, #1
	str	r2, [r3, #444]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L256:
	.align	2
.L255:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1707
	.word	.LANCHOR3
	.word	comm_mngr_recursive_MUTEX
	.word	1729
	.word	comm_mngr
.LFE44:
	.size	LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token, .-LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token
	.section	.text.LIGHTS_on_all_lights_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	LIGHTS_on_all_lights_set_or_clear_commserver_change_bits
	.type	LIGHTS_on_all_lights_set_or_clear_commserver_change_bits, %function
LIGHTS_on_all_lights_set_or_clear_commserver_change_bits:
.LFB45:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI58:
	ldr	r4, .L263
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L263+4
	ldr	r3, .L263+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L263+12
	bl	nm_ListGetFirst
	b	.L262
.L261:
	cmp	r5, #51
	ldr	r1, [r4, #0]
	add	r0, r6, #316
	bne	.L259
	bl	SHARED_clear_all_32_bit_change_bits
	b	.L260
.L259:
	bl	SHARED_set_all_32_bit_change_bits
.L260:
	ldr	r0, .L263+12
	mov	r1, r6
	bl	nm_ListGetNext
.L262:
	cmp	r0, #0
	mov	r6, r0
	bne	.L261
	ldr	r3, .L263
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L264:
	.align	2
.L263:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1742
	.word	.LANCHOR3
.LFE45:
	.size	LIGHTS_on_all_lights_set_or_clear_commserver_change_bits, .-LIGHTS_on_all_lights_set_or_clear_commserver_change_bits
	.section	.text.nm_LIGHTS_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_update_pending_change_bits
	.type	nm_LIGHTS_update_pending_change_bits, %function
nm_LIGHTS_update_pending_change_bits:
.LFB46:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI59:
	ldr	r5, .L272
	mov	r1, #400
	ldr	r2, .L272+4
	ldr	r3, .L272+8
	mov	r7, r0
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L272+12
	bl	nm_ListGetFirst
	mov	r8, #0
	mov	r6, #1
	mov	r4, r0
	b	.L266
.L269:
	ldr	r3, [r4, #320]
	cmp	r3, #0
	beq	.L267
	cmp	r7, #0
	beq	.L268
	ldr	r2, [r4, #316]
	mov	r1, #2
	orr	r3, r2, r3
	str	r3, [r4, #316]
	ldr	r3, .L272+16
	ldr	r2, .L272+20
	str	r6, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L272+24
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L271
.L268:
	add	r0, r4, #320
	ldr	r1, [r5, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L271:
	mov	r8, #1
.L267:
	mov	r1, r4
	ldr	r0, .L272+12
	bl	nm_ListGetNext
	mov	r4, r0
.L266:
	cmp	r4, #0
	bne	.L269
	ldr	r3, .L272
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r8, #0
	beq	.L265
	mov	r0, #16
	mov	r1, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L265:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L273:
	.align	2
.L272:
	.word	list_lights_recursive_MUTEX
	.word	.LC8
	.word	1780
	.word	.LANCHOR3
	.word	weather_preserves
	.word	60000
	.word	cics
.LFE46:
	.size	nm_LIGHTS_update_pending_change_bits, .-nm_LIGHTS_update_pending_change_bits
	.section	.text.LIGHTS_calculate_chain_sync_crc,"ax",%progbits
	.align	2
	.global	LIGHTS_calculate_chain_sync_crc
	.type	LIGHTS_calculate_chain_sync_crc, %function
LIGHTS_calculate_chain_sync_crc:
.LFB47:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L279
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI60:
	ldr	r5, .L279+4
	mov	r1, #400
	ldr	r2, .L279+8
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L279+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #8]
	mov	r0, #324
	mul	r0, r3, r0
	mov	r1, sp
	ldr	r2, .L279+8
	ldr	r3, .L279+16
	bl	mem_oabia
	subs	r6, r0, #0
	beq	.L275
	ldr	r3, [sp, #0]
	add	r6, sp, #8
	mov	r0, r5
	str	r3, [r6, #-4]!
	bl	nm_ListGetFirst
	mov	r7, #0
	mov	r5, r0
	b	.L276
.L277:
	add	r8, r5, #20
	mov	r0, r8
	bl	strlen
	mov	r1, r8
	mov	r2, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #72
	mov	r2, #4
	mov	r8, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #76
	mov	r2, #4
	add	r0, r8, r0
	add	r7, r0, r7
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #80
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #84
	mov	r2, #224
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r1, r5
	add	r7, r7, r0
	ldr	r0, .L279+4
	bl	nm_ListGetNext
	mov	r5, r0
.L276:
	cmp	r5, #0
	bne	.L277
	mov	r1, r7
	ldr	r0, [sp, #0]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L279+20
	add	r4, r4, #43
	ldr	r1, .L279+8
	ldr	r2, .L279+24
	mov	r6, #1
	str	r0, [r3, r4, asl #2]
	ldr	r0, [sp, #0]
	bl	mem_free_debug
	b	.L278
.L275:
	ldr	r3, .L279+28
	ldr	r0, .L279+32
	ldr	r1, [r3, r4, asl #4]
	bl	Alert_Message_va
.L278:
	ldr	r3, .L279
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L280:
	.align	2
.L279:
	.word	list_lights_recursive_MUTEX
	.word	.LANCHOR3
	.word	.LC8
	.word	1858
	.word	1870
	.word	cscs
	.word	1927
	.word	chain_sync_file_pertinants
	.word	.LC13
.LFE47:
	.size	LIGHTS_calculate_chain_sync_crc, .-LIGHTS_calculate_chain_sync_crc
	.global	light_list_item_sizes
	.global	g_LIGHTS_initial_date_time
	.global	LIGHTS_DEFAULT_NAME
	.global	light_list_hdr
	.section	.bss.light_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	light_list_hdr, %object
	.size	light_list_hdr, 20
light_list_hdr:
	.space	20
	.section	.rodata.LIGHTS_FILENAME,"a",%progbits
	.set	.LANCHOR2,. + 0
	.type	LIGHTS_FILENAME, %object
	.size	LIGHTS_FILENAME, 7
LIGHTS_FILENAME:
	.ascii	"LIGHTS\000"
	.section	.rodata.LIGHTS_database_field_names,"a",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	LIGHTS_database_field_names, %object
	.size	LIGHTS_database_field_names, 72
LIGHTS_database_field_names:
	.word	.LC14
	.word	.LC0
	.word	.LC7
	.word	.LC1
	.word	.LC15
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	.LC23
	.word	.LC24
	.word	.LC25
	.word	.LC26
	.word	.LC27
	.word	.LC28
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"BoxIndex\000"
.LC1:
	.ascii	"CardConnected\000"
.LC2:
	.ascii	"%sStopTime2\000"
.LC3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/shared_lights.c\000"
.LC4:
	.ascii	"%sStopTime1\000"
.LC5:
	.ascii	"%sStartTime2\000"
.LC6:
	.ascii	"%sStartTime1\000"
.LC7:
	.ascii	"OutputIndex\000"
.LC8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/lights.c\000"
.LC9:
	.ascii	"Light %c%d: %s\000"
.LC10:
	.ascii	"Box Index\000"
.LC11:
	.ascii	"Output Index\000"
.LC12:
	.ascii	"No lights available\000"
.LC13:
	.ascii	"SYNC: no mem to calc checksum %s\000"
.LC14:
	.ascii	"Name\000"
.LC15:
	.ascii	"Day00_\000"
.LC16:
	.ascii	"Day01_\000"
.LC17:
	.ascii	"Day02_\000"
.LC18:
	.ascii	"Day03_\000"
.LC19:
	.ascii	"Day04_\000"
.LC20:
	.ascii	"Day05_\000"
.LC21:
	.ascii	"Day06_\000"
.LC22:
	.ascii	"Day07_\000"
.LC23:
	.ascii	"Day08_\000"
.LC24:
	.ascii	"Day09_\000"
.LC25:
	.ascii	"Day10_\000"
.LC26:
	.ascii	"Day11_\000"
.LC27:
	.ascii	"Day12_\000"
.LC28:
	.ascii	"Day13_\000"
	.section	.bss.g_LIGHTS_initial_date_time,"aw",%nobits
	.type	g_LIGHTS_initial_date_time, %object
	.size	g_LIGHTS_initial_date_time, 6
g_LIGHTS_initial_date_time:
	.space	6
	.section	.rodata.light_list_item_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	light_list_item_sizes, %object
	.size	light_list_item_sizes, 4
light_list_item_sizes:
	.word	324
	.section	.rodata.LIGHTS_DEFAULT_NAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	LIGHTS_DEFAULT_NAME, %object
	.size	LIGHTS_DEFAULT_NAME, 26
LIGHTS_DEFAULT_NAME:
	.ascii	"(description not yet set)\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI4-.LFB7
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI6-.LFB6
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI8-.LFB5
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI10-.LFB4
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI12-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI13-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI15-.LFB13
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x83
	.uleb128 0x8
	.byte	0x82
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI16-.LFB11
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI17-.LFB12
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI18-.LFB17
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI19-.LFB24
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI20-.LFB25
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI21-.LFB26
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI22-.LFB23
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI23-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI24-.LFB19
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI25-.LFB18
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI26-.LFB27
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI27-.LFB28
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI29-.LFB29
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI30-.LFB30
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI31-.LFB14
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI32-.LFB31
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI33-.LFB32
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI35-.LFB33
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI37-.LFB34
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI38-.LCFI37
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI39-.LFB35
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI41-.LFB36
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI42-.LFB37
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI43-.LFB8
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI44-.LCFI43
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI45-.LFB22
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI46-.LFB9
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI47-.LCFI46
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI48-.LFB21
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI50-.LFB15
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI51-.LCFI50
	.byte	0xe
	.uleb128 0x6c
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI52-.LFB39
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI53-.LFB40
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI54-.LFB41
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI55-.LFB42
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI56-.LFB43
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE86:
.LSFDE88:
	.4byte	.LEFDE88-.LASFDE88
.LASFDE88:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.byte	0x4
	.4byte	.LCFI57-.LFB44
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE88:
.LSFDE90:
	.4byte	.LEFDE90-.LASFDE90
.LASFDE90:
	.4byte	.Lframe0
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.byte	0x4
	.4byte	.LCFI58-.LFB45
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE90:
.LSFDE92:
	.4byte	.LEFDE92-.LASFDE92
.LASFDE92:
	.4byte	.Lframe0
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.byte	0x4
	.4byte	.LCFI59-.LFB46
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE92:
.LSFDE94:
	.4byte	.LEFDE94-.LASFDE94
.LASFDE94:
	.4byte	.Lframe0
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.byte	0x4
	.4byte	.LCFI60-.LFB47
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE94:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/shared_lights.c"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/lights.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x421
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF48
	.byte	0x1
	.4byte	.LASF49
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x3eb
	.byte	0x1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x391
	.byte	0x1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x337
	.byte	0x1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x2dd
	.byte	0x1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF4
	.byte	0x2
	.2byte	0x53b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x2
	.byte	0x68
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1df
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x27d
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	0x21
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST2
	.uleb128 0x5
	.4byte	0x2b
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST3
	.uleb128 0x5
	.4byte	0x35
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x5
	.4byte	0x3f
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x19f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST6
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x22e
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST7
	.uleb128 0x6
	.4byte	.LASF10
	.byte	0x2
	.byte	0xa2
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST8
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF11
	.byte	0x2
	.byte	0x6f
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST9
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF12
	.byte	0x2
	.byte	0x7e
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST10
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF13
	.byte	0x2
	.2byte	0x247
	.4byte	.LFB16
	.4byte	.LFE16
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF14
	.byte	0x2
	.2byte	0x25e
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST11
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF15
	.byte	0x2
	.2byte	0x378
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST12
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF16
	.byte	0x2
	.2byte	0x3ae
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST13
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF17
	.byte	0x2
	.2byte	0x3dd
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST14
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF18
	.byte	0x2
	.2byte	0x362
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST15
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF19
	.byte	0x2
	.2byte	0x2ca
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST16
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF20
	.byte	0x2
	.2byte	0x29d
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST17
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF21
	.byte	0x2
	.2byte	0x27b
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST18
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF22
	.byte	0x2
	.2byte	0x400
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST19
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF23
	.byte	0x2
	.2byte	0x41e
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST20
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF24
	.byte	0x2
	.2byte	0x436
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST21
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF25
	.byte	0x2
	.2byte	0x457
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST22
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF26
	.byte	0x2
	.byte	0xfc
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST23
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF27
	.byte	0x2
	.2byte	0x480
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST24
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF28
	.byte	0x2
	.2byte	0x4a1
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST25
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF29
	.byte	0x2
	.2byte	0x4ca
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST26
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF30
	.byte	0x2
	.2byte	0x4ef
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST27
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF31
	.byte	0x2
	.2byte	0x514
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST28
	.uleb128 0x5
	.4byte	0x49
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST29
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF32
	.byte	0x2
	.2byte	0x543
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST30
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF33
	.byte	0x2
	.2byte	0x560
	.4byte	.LFB38
	.4byte	.LFE38
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x44a
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST31
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF35
	.byte	0x2
	.2byte	0x31b
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST32
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x510
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST33
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF37
	.byte	0x2
	.2byte	0x2f7
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST34
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF38
	.byte	0x2
	.2byte	0x13a
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST35
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF39
	.byte	0x2
	.2byte	0x57c
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST36
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF40
	.byte	0x2
	.2byte	0x5cb
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST37
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF41
	.byte	0x2
	.2byte	0x618
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST38
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF42
	.byte	0x2
	.2byte	0x650
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST39
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF43
	.byte	0x2
	.2byte	0x675
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST40
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF44
	.byte	0x2
	.2byte	0x6a5
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LLST41
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF45
	.byte	0x2
	.2byte	0x6ca
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LLST42
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF46
	.byte	0x2
	.2byte	0x6ec
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LLST43
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF47
	.byte	0x2
	.2byte	0x72b
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LLST44
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB7
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI5
	.4byte	.LFE7
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB6
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI7
	.4byte	.LFE6
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI9
	.4byte	.LFE5
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI11
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB0
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB2
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI14
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB13
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB11
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB12
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB24
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB25
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB26
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB23
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB20
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB19
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB18
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB27
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB28
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI28
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB29
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB30
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB14
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB31
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI34
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB33
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI36
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB34
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI38
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB35
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI40
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB36
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB37
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB8
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI44
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB22
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB9
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI47
	.4byte	.LFE9
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB21
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI49
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB15
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI51
	.4byte	.LFE15
	.2byte	0x3
	.byte	0x7d
	.sleb128 108
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB39
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI52
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB40
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB41
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB42
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI55
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB43
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB44
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI57
	.4byte	.LFE44
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB45
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI58
	.4byte	.LFE45
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB46
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI59
	.4byte	.LFE46
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST44:
	.4byte	.LFB47
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LFE47
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x194
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF42:
	.ascii	"LIGHTS_clean_house_processing\000"
.LASF24:
	.ascii	"LIGHTS_get_box_index\000"
.LASF44:
	.ascii	"LIGHTS_set_bits_on_all_lights_to_cause_distribution"
	.ascii	"_in_the_next_token\000"
.LASF36:
	.ascii	"nm_LIGHTS_extract_and_store_changes_from_comm\000"
.LASF49:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/lights.c\000"
.LASF22:
	.ascii	"LIGHTS_get_list_count\000"
.LASF32:
	.ascii	"LIGHTS_has_a_stop_time\000"
.LASF43:
	.ascii	"LIGHTS_set_not_physically_available_based_upon_comm"
	.ascii	"unication_scan_results\000"
.LASF11:
	.ascii	"init_file_LIGHTS\000"
.LASF47:
	.ascii	"LIGHTS_calculate_chain_sync_crc\000"
.LASF17:
	.ascii	"LIGHTS_get_light_at_this_index\000"
.LASF12:
	.ascii	"save_file_LIGHTS\000"
.LASF37:
	.ascii	"LIGHTS_extract_and_store_group_name_from_GuiVars\000"
.LASF41:
	.ascii	"nm_LIGHTS_get_prev_available_light\000"
.LASF30:
	.ascii	"LIGHTS_get_stop_time_1\000"
.LASF31:
	.ascii	"LIGHTS_get_stop_time_2\000"
.LASF27:
	.ascii	"LIGHTS_get_physically_available\000"
.LASF45:
	.ascii	"LIGHTS_on_all_lights_set_or_clear_commserver_change"
	.ascii	"_bits\000"
.LASF13:
	.ascii	"LIGHTS_copy_guivars_to_daily_schedule\000"
.LASF39:
	.ascii	"nm_LIGHTS_get_first_available_light_and_init_lights"
	.ascii	"_output_GuiVars\000"
.LASF19:
	.ascii	"LIGHTS_copy_file_schedule_into_global_daily_schedul"
	.ascii	"e\000"
.LASF16:
	.ascii	"nm_LIGHTS_get_pointer_to_light_struct_with_this_box"
	.ascii	"_and_output_index\000"
.LASF38:
	.ascii	"LIGHTS_build_data_to_send\000"
.LASF26:
	.ascii	"nm_LIGHTS_create_new_group\000"
.LASF4:
	.ascii	"LIGHTS_get_day_index\000"
.LASF6:
	.ascii	"nm_LIGHTS_set_physically_available\000"
.LASF20:
	.ascii	"LIGHTS_copy_light_struct_into_guivars\000"
.LASF10:
	.ascii	"nm_LIGHTS_set_default_values\000"
.LASF1:
	.ascii	"nm_LIGHTS_set_stop_time_1\000"
.LASF0:
	.ascii	"nm_LIGHTS_set_stop_time_2\000"
.LASF7:
	.ascii	"nm_LIGHTS_set_name\000"
.LASF34:
	.ascii	"nm_LIGHTS_store_changes\000"
.LASF28:
	.ascii	"LIGHTS_get_start_time_1\000"
.LASF3:
	.ascii	"nm_LIGHTS_set_start_time_1\000"
.LASF2:
	.ascii	"nm_LIGHTS_set_start_time_2\000"
.LASF29:
	.ascii	"LIGHTS_get_start_time_2\000"
.LASF25:
	.ascii	"LIGHTS_get_output_index\000"
.LASF23:
	.ascii	"LIGHTS_get_lights_array_index\000"
.LASF33:
	.ascii	"LIGHTS_get_change_bits_ptr\000"
.LASF15:
	.ascii	"LIGHTS_get_index_using_ptr_to_light_struct\000"
.LASF35:
	.ascii	"LIGHTS_extract_and_store_changes_from_GuiVars\000"
.LASF46:
	.ascii	"nm_LIGHTS_update_pending_change_bits\000"
.LASF40:
	.ascii	"nm_LIGHTS_get_next_available_light\000"
.LASF5:
	.ascii	"nm_LIGHTS_set_box_index\000"
.LASF8:
	.ascii	"nm_LIGHTS_set_output_index\000"
.LASF18:
	.ascii	"LIGHTS_load_group_name_into_guivar\000"
.LASF48:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF9:
	.ascii	"nm_light_structure_updater\000"
.LASF21:
	.ascii	"LIGHTS_populate_group_name\000"
.LASF14:
	.ascii	"LIGHTS_copy_daily_schedule_to_guivars\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
