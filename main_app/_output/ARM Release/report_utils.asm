	.file	"report_utils.c"
	.text
.Ltext0:
	.section	.text.FDTO_REPORTS_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_REPORTS_draw_report
	.type	FDTO_REPORTS_draw_report, %function
FDTO_REPORTS_draw_report:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI0:
	mov	r4, r0
	mov	r0, #0
	mov	r5, r1
	mov	r6, r2
	bl	GuiLib_ScrollBox_Close
	cmp	r4, #1
	ldr	r3, .L6
	ldr	r1, .L6+4
	bne	.L2
	ldr	r2, .L6+8
	mov	r0, #0
	cmp	r5, r0
	str	r0, [r2, #0]
	str	r0, [r3, #0]
	str	r0, [r1, #0]
	movne	r2, r5, asl #16
	moveq	r0, r5
	moveq	r1, r6
	moveq	r2, r0
	mvneq	r3, #0
	movne	r1, r6
	movne	r2, r2, asr #16
	movne	r3, r0
	bl	GuiLib_ScrollBox_Init
	b	.L4
.L2:
	ldrsh	r1, [r1, #0]
	mov	r2, r5, asl #16
	ldrsh	r3, [r3, #0]
	mov	r0, #0
	str	r1, [sp, #0]
	mov	r2, r2, asr #16
	mov	r1, r6
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L4:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_Refresh
.L7:
	.align	2
.L6:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	GuiVar_ScrollBoxHorizScrollPos
.LFE0:
	.size	FDTO_REPORTS_draw_report, .-FDTO_REPORTS_draw_report
	.section	.text.REPORTS_process_report,"ax",%progbits
	.align	2
	.global	REPORTS_process_report
	.type	REPORTS_process_report, %function
REPORTS_process_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #3
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	mov	r4, r0
	sub	sp, sp, #36
.LCFI2:
	mov	r5, r2
	mov	r6, r3
	beq	.L11
	bhi	.L13
	cmp	r0, #0
	beq	.L10
	cmp	r0, #1
	bne	.L9
	b	.L11
.L13:
	cmp	r0, #4
	beq	.L10
	cmp	r0, #67
	bne	.L9
	b	.L17
.L10:
	mov	r0, #0
	mov	r1, r4
	bl	SCROLL_BOX_up_or_down
	b	.L8
.L11:
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L18
	str	r0, [r3, #0]
	mov	r0, #0
	bl	GuiLib_ScrollBox_GetTopLine
	ldr	r3, .L18+4
	ldr	r2, .L18+8
	cmp	r4, #3
	movne	r4, r2
	str	r6, [sp, #24]
	str	r5, [sp, #28]
	str	r0, [r3, #0]
	mov	r3, #3
	str	r3, [sp, #0]
	ldr	r3, .L18+12
	mov	r0, sp
	moveq	r4, r3
	str	r4, [sp, #20]
	bl	Display_Post_Command
	b	.L8
.L17:
	ldr	r3, .L18+16
	ldr	r2, .L18+20
	ldr	r3, [r3, #0]
	mov	r0, #36
	mla	r3, r0, r3, r2
	ldr	r2, [r3, #4]
	ldr	r3, .L18+24
	str	r2, [r3, #0]
.L9:
	mov	r0, r4
	bl	KEY_process_global_keys
.L8:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, pc}
.L19:
	.align	2
.L18:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	FDTO_scroll_left
	.word	FDTO_scroll_right
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE1:
	.size	REPORTS_process_report, .-REPORTS_process_report
	.section	.bss.g_REPORT_top_line,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_REPORT_top_line, %object
	.size	g_REPORT_top_line, 4
g_REPORT_top_line:
	.space	4
	.section	.bss.g_REPORT_active_line,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_REPORT_active_line, %object
	.size	g_REPORT_active_line, 4
g_REPORT_active_line:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/report_utils.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x46
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x20
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x3e
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"FDTO_REPORTS_draw_report\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/r"
	.ascii	"eport_utils.c\000"
.LASF1:
	.ascii	"REPORTS_process_report\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
