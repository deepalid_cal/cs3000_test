	.file	"flowsense.c"
	.text
.Ltext0:
	.section	.text.flowsense_updater,"ax",%progbits
	.align	2
	.type	flowsense_updater, %function
flowsense_updater:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	subs	r1, r0, #0
	bne	.L2
	ldr	r0, .L3
	b	Alert_Message_va
.L2:
	ldr	r0, .L3+4
	b	Alert_Message
.L4:
	.align	2
.L3:
	.word	.LC0
	.word	.LC1
.LFE4:
	.size	flowsense_updater, .-flowsense_updater
	.section	.text.init_file_flowsense,"ax",%progbits
	.align	2
	.global	init_file_flowsense
	.type	init_file_flowsense, %function
init_file_flowsense:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r4, .L6
	sub	sp, sp, #20
.LCFI1:
	mov	r3, #12
	str	r3, [sp, #0]
	ldr	r3, .L6+4
	mov	r2, #0
	str	r3, [sp, #8]
	ldr	r3, .L6+8
	mov	r0, #1
	str	r3, [sp, #12]
	mov	r3, #14
	str	r3, [sp, #16]
	ldr	r1, .L6+12
	mov	r3, r4
	str	r2, [sp, #4]
	bl	FLASH_FILE_find_or_create_variable_file
	ldr	r2, [r4, #0]
	ldr	r3, .L6+16
	sub	r2, r2, #65
	str	r2, [r3, #64]
	add	sp, sp, #20
	ldmfd	sp!, {r4, pc}
.L7:
	.align	2
.L6:
	.word	.LANCHOR1
	.word	flowsense_updater
	.word	FLOWSENSE_set_default_values
	.word	.LANCHOR0
	.word	restart_info
.LFE5:
	.size	init_file_flowsense, .-init_file_flowsense
	.section	.text.save_file_flowsense,"ax",%progbits
	.align	2
	.global	save_file_flowsense
	.type	save_file_flowsense, %function
save_file_flowsense:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI2:
	mov	r3, #14
	mov	r1, #12
	mov	r2, #0
	stmia	sp, {r1, r2, r3}
	ldr	r1, .L9
	mov	r0, #1
	ldr	r3, .L9+4
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L10:
	.align	2
.L9:
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE6:
	.size	save_file_flowsense, .-save_file_flowsense
	.section	.text.FLOWSENSE_copy_settings_into_guivars,"ax",%progbits
	.align	2
	.global	FLOWSENSE_copy_settings_into_guivars
	.type	FLOWSENSE_copy_settings_into_guivars, %function
FLOWSENSE_copy_settings_into_guivars:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L12
	ldr	r2, .L12+4
	ldr	r1, [r3, #0]
	sub	r1, r1, #65
	str	r1, [r2, #0]
	ldr	r1, [r3, #4]
	ldr	r2, .L12+8
	str	r1, [r2, #0]
	ldr	r2, [r3, #8]
	ldr	r3, .L12+12
	str	r2, [r3, #0]
	bx	lr
.L13:
	.align	2
.L12:
	.word	.LANCHOR1
	.word	GuiVar_FLControllerIndex_0
	.word	GuiVar_FLPartOfChain
	.word	GuiVar_FLNumControllersInChain
.LFE7:
	.size	FLOWSENSE_copy_settings_into_guivars, .-FLOWSENSE_copy_settings_into_guivars
	.section	.text.FLOWSENSE_get_controller_letter,"ax",%progbits
	.align	2
	.global	FLOWSENSE_get_controller_letter
	.type	FLOWSENSE_get_controller_letter, %function
FLOWSENSE_get_controller_letter:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI3:
	ldr	r0, .L16
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L16+4
	mov	r1, #65
	str	r3, [sp, #4]
	ldr	r3, .L16+8
	mov	r2, #76
	str	r3, [sp, #8]
	ldr	r3, .L16+12
	str	r3, [sp, #12]
	mov	r3, r1
	bl	RC_uint32_with_filename
	subs	r1, r0, #0
	bne	.L15
	mov	r0, #14
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L15:
	ldr	r3, .L16
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L17:
	.align	2
.L16:
	.word	.LANCHOR1
	.word	.LC2
	.word	.LC3
	.word	477
.LFE9:
	.size	FLOWSENSE_get_controller_letter, .-FLOWSENSE_get_controller_letter
	.section	.text.FLOWSENSE_get_controller_index,"ax",%progbits
	.align	2
	.global	FLOWSENSE_get_controller_index
	.type	FLOWSENSE_get_controller_index, %function
FLOWSENSE_get_controller_index:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI4:
	bl	FLOWSENSE_get_controller_letter
	sub	r0, r0, #65
	ldr	pc, [sp], #4
.LFE10:
	.size	FLOWSENSE_get_controller_index, .-FLOWSENSE_get_controller_index
	.section	.text.set_controller_letter,"ax",%progbits
	.align	2
	.type	set_controller_letter, %function
set_controller_letter:
.LFB0:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI5:
	ldr	r4, .L23
	mov	sl, r3
	ldr	r3, [r4, #0]
	sub	sp, sp, #20
.LCFI6:
	str	r3, [sp, #16]
	ldr	r3, .L23+4
	mov	r6, r1
	str	r3, [sp, #4]
	ldr	r3, .L23+8
	mov	r1, #65
	str	r3, [sp, #8]
	mov	r3, #113
	str	r0, [r4, #0]
	mov	r5, #0
	str	r3, [sp, #12]
	mov	r7, r0
	mov	r3, r1
	mov	r8, r2
	mov	r0, r4
	mov	r2, #76
	str	r5, [sp, #0]
	bl	RC_uint32_with_filename
	ldr	r2, [r4, #0]
	ldr	r3, [sp, #16]
	cmp	r2, r3
	moveq	r0, r5
	beq	.L20
	ldr	r3, .L23+12
	sub	r7, r7, #65
	cmp	r6, #1
	str	r7, [r3, #64]
	movne	r0, #1
	bne	.L20
	bl	FLOWSENSE_get_controller_index
	mov	r3, #4
	stmia	sp, {r3, r8, sl}
	add	r2, sp, #16
	mov	r3, r4
	mov	r1, r0
	ldr	r0, .L23+16
	bl	Alert_ChangeLine_Controller
	mov	r0, r6
.L20:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L24:
	.align	2
.L23:
	.word	.LANCHOR1
	.word	.LC2
	.word	.LC3
	.word	restart_info
	.word	57349
.LFE0:
	.size	set_controller_letter, .-set_controller_letter
	.section	.text.set_num_controllers_in_chain,"ax",%progbits
	.align	2
	.type	set_num_controllers_in_chain, %function
set_num_controllers_in_chain:
.LFB2:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI7:
	mov	r8, r3
	ldr	r3, .L30
	sub	sp, sp, #20
.LCFI8:
	add	r4, sp, #20
	str	r0, [r4, #-4]!
	str	r3, [sp, #4]
	ldr	r3, .L30+4
	mov	r7, r1
	str	r3, [sp, #8]
	mov	r1, #1
	mov	r3, #211
	str	r3, [sp, #12]
	mov	r6, r2
	mov	r5, #0
	mov	r0, r4
	mov	r2, #12
	mov	r3, r1
	str	r5, [sp, #0]
	bl	RC_uint32_with_filename
	cmp	r0, #1
	bne	.L29
	ldr	sl, .L30+8
	ldr	r3, [sp, #16]
	ldr	r2, [sl, #8]
	cmp	r2, r3
	beq	.L29
	cmp	r7, #1
	bne	.L27
	bl	FLOWSENSE_get_controller_index
	mov	r3, #4
	stmia	sp, {r3, r6, r8}
	add	r2, sl, #8
	mov	r3, r4
	mov	r1, r0
	ldr	r0, .L30+12
	bl	Alert_ChangeLine_Controller
.L27:
	ldr	r2, [sp, #16]
	ldr	r3, .L30+8
	mov	r0, #1
	str	r2, [r3, #8]
	b	.L26
.L29:
	mov	r0, r5
.L26:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L31:
	.align	2
.L30:
	.word	.LC4
	.word	.LC3
	.word	.LANCHOR1
	.word	57351
.LFE2:
	.size	set_num_controllers_in_chain, .-set_num_controllers_in_chain
	.section	.text.FLOWSENSE_set_default_values,"ax",%progbits
	.align	2
	.type	FLOWSENSE_set_default_values, %function
FLOWSENSE_set_default_values:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r1, #0
	stmfd	sp!, {r4, r5, lr}
.LCFI9:
	mov	r3, r1
	sub	sp, sp, #32
.LCFI10:
	mov	r2, #11
	mov	r0, #65
	bl	set_controller_letter
.LBB6:
	ldr	r3, .L33
	mov	r5, #11
	stmia	sp, {r3, r5}
	ldr	r3, .L33+4
	mov	r4, #0
	str	r3, [sp, #28]
	mov	r1, r4
	mov	r2, r4
	mov	r3, r4
	ldr	r0, .L33+8
	str	r4, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	str	r4, [sp, #20]
	str	r4, [sp, #24]
	bl	SHARED_set_bool_controller
.LBE6:
	mov	r0, #2
	mov	r1, r4
	mov	r2, r5
	mov	r3, r4
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, lr}
	b	set_num_controllers_in_chain
.L34:
	.align	2
.L33:
	.word	57350
	.word	.LC5
	.word	.LANCHOR1+4
.LFE3:
	.size	FLOWSENSE_set_default_values, .-FLOWSENSE_set_default_values
	.section	.text.FLOWSENSE_we_are_poafs,"ax",%progbits
	.align	2
	.global	FLOWSENSE_we_are_poafs
	.type	FLOWSENSE_we_are_poafs, %function
FLOWSENSE_we_are_poafs:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L39
	stmfd	sp!, {r0, r1, lr}
.LCFI11:
	ldr	r0, .L39+4
	mov	r1, #0
	str	r3, [sp, #0]
	mov	r3, #512
	str	r3, [sp, #4]
	mov	r2, r1
	ldr	r3, .L39+8
	bl	RC_bool_with_filename
	subs	r1, r0, #0
	bne	.L36
	mov	r0, #14
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L36:
	ldr	r3, .L39+12
	ldrb	r0, [r3, #52]	@ zero_extendqisi2
	ands	r0, r0, #1
	beq	.L37
	ldr	r3, .L39+16
	ldr	r0, [r3, #4]
	adds	r0, r0, #0
	movne	r0, #1
.L37:
	ldmfd	sp!, {r2, r3, pc}
.L40:
	.align	2
.L39:
	.word	.LC3
	.word	.LANCHOR1+4
	.word	.LC6
	.word	config_c
	.word	.LANCHOR1
.LFE11:
	.size	FLOWSENSE_we_are_poafs, .-FLOWSENSE_we_are_poafs
	.section	.text.FLOWSENSE_get_num_controllers_in_chain,"ax",%progbits
	.align	2
	.global	FLOWSENSE_get_num_controllers_in_chain
	.type	FLOWSENSE_get_num_controllers_in_chain, %function
FLOWSENSE_get_num_controllers_in_chain:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI12:
	ldr	r0, .L43
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L43+4
	mov	r1, #2
	str	r3, [sp, #4]
	ldr	r3, .L43+8
	mov	r2, #12
	str	r3, [sp, #8]
	ldr	r3, .L43+12
	str	r3, [sp, #12]
	mov	r3, r1
	bl	RC_uint32_with_filename
	subs	r1, r0, #0
	bne	.L42
	mov	r0, #14
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L42:
	ldr	r3, .L43+16
	ldr	r0, [r3, #8]
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L44:
	.align	2
.L43:
	.word	.LANCHOR1+8
	.word	.LC7
	.word	.LC3
	.word	533
	.word	.LANCHOR1
.LFE12:
	.size	FLOWSENSE_get_num_controllers_in_chain, .-FLOWSENSE_get_num_controllers_in_chain
	.section	.text.FLOWSENSE_we_are_a_master_one_way_or_another,"ax",%progbits
	.align	2
	.global	FLOWSENSE_we_are_a_master_one_way_or_another
	.type	FLOWSENSE_we_are_a_master_one_way_or_another, %function
FLOWSENSE_we_are_a_master_one_way_or_another:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI13:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L47
	bl	FLOWSENSE_get_controller_letter
	sub	ip, r0, #65
	rsbs	r0, ip, #0
	adc	r0, r0, ip
	ldr	pc, [sp], #4
.L47:
	mov	r0, #1
	ldr	pc, [sp], #4
.LFE13:
	.size	FLOWSENSE_we_are_a_master_one_way_or_another, .-FLOWSENSE_we_are_a_master_one_way_or_another
	.section	.text.FLOWSENSE_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	FLOWSENSE_extract_and_store_changes_from_GuiVars
	.type	FLOWSENSE_extract_and_store_changes_from_GuiVars, %function
FLOWSENSE_extract_and_store_changes_from_GuiVars:
.LFB8:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI14:
	ldr	r6, .L57
	sub	sp, sp, #72
.LCFI15:
	ldr	r3, [r6, #0]
	mov	r5, r0
	mov	r1, #1
	add	r0, r3, #65
	mov	r2, #2
	bl	set_controller_letter
.LBB7:
	ldr	r3, .L57+4
	mov	r7, #2
	stmia	sp, {r3, r7}
	ldr	r3, [r6, #0]
	mov	r4, #0
	str	r3, [sp, #8]
	ldr	r3, .L57+8
	str	r4, [sp, #12]
	str	r3, [sp, #28]
	ldr	r3, .L57+12
	str	r4, [sp, #16]
	str	r4, [sp, #20]
	str	r4, [sp, #24]
	mov	r2, r4
	ldr	r1, [r3, #0]
	mov	r3, #1
.LBE7:
	mov	r8, r0
.LBB8:
	ldr	r0, .L57+16
	bl	SHARED_set_bool_controller
.LBE8:
	ldr	r3, .L57+20
	mov	r1, #1
	mov	r2, r7
.LBB9:
	mov	sl, r0
.LBE9:
	ldr	r0, [r3, #0]
	ldr	r3, [r6, #0]
	bl	set_num_controllers_in_chain
	cmp	r5, #1
	moveq	r3, #1536
	streq	r5, [sp, #68]
	moveq	r4, r5
	add	r8, r0, r8
	add	sl, r8, sl
	streq	r3, [sp, #32]
	cmp	sl, #0
	beq	.L50
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	movne	r3, #1536
	strne	r3, [sp, #32]
	moveq	r3, #5632
	movne	r3, #1
	strne	r3, [sp, #68]
	streq	r3, [sp, #32]
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	bne	.L53
	bl	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation
	bl	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights
.L53:
	mov	r0, #14
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L54
.L50:
	cmp	r4, #0
	beq	.L48
.L54:
	add	r0, sp, #32
	bl	COMM_MNGR_post_event_with_details
.L48:
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L58:
	.align	2
.L57:
	.word	GuiVar_FLControllerIndex_0
	.word	57350
	.word	.LC5
	.word	GuiVar_FLPartOfChain
	.word	.LANCHOR1+4
	.word	GuiVar_FLNumControllersInChain
.LFE8:
	.size	FLOWSENSE_extract_and_store_changes_from_GuiVars, .-FLOWSENSE_extract_and_store_changes_from_GuiVars
	.section	.text.FLOWSENSE_we_are_the_master_of_a_multiple_controller_network,"ax",%progbits
	.align	2
	.global	FLOWSENSE_we_are_the_master_of_a_multiple_controller_network
	.type	FLOWSENSE_we_are_the_master_of_a_multiple_controller_network, %function
FLOWSENSE_we_are_the_master_of_a_multiple_controller_network:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI16:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #1
	bne	.L61
	bl	FLOWSENSE_get_controller_letter
	sub	lr, r0, #65
	rsbs	r0, lr, #0
	adc	r0, r0, lr
	ldr	pc, [sp], #4
.L61:
	mov	r0, #0
	ldr	pc, [sp], #4
.LFE14:
	.size	FLOWSENSE_we_are_the_master_of_a_multiple_controller_network, .-FLOWSENSE_we_are_the_master_of_a_multiple_controller_network
	.section	.text.FLOWSENSE_we_are_a_slave_in_a_chain,"ax",%progbits
	.align	2
	.global	FLOWSENSE_we_are_a_slave_in_a_chain
	.type	FLOWSENSE_we_are_a_slave_in_a_chain, %function
FLOWSENSE_we_are_a_slave_in_a_chain:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI17:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	ldreq	pc, [sp], #4
	bl	FLOWSENSE_get_controller_letter
	subs	r0, r0, #65
	movne	r0, #1
	ldr	pc, [sp], #4
.LFE15:
	.size	FLOWSENSE_we_are_a_slave_in_a_chain, .-FLOWSENSE_we_are_a_slave_in_a_chain
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"FLOWSENSE file unexpd update %u\000"
.LC1:
	.ascii	"FLOWSENSE updater error\000"
.LC2:
	.ascii	"Controller Letter\000"
.LC3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/flowsense.c\000"
.LC4:
	.ascii	"Number of Controllers in Chain\000"
.LC5:
	.ascii	"Part of a Chain\000"
.LC6:
	.ascii	"POAFS\000"
.LC7:
	.ascii	"NOCIC\000"
	.section	.rodata.FLOWSENSE_FILENAME,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	FLOWSENSE_FILENAME, %object
	.size	FLOWSENSE_FILENAME, 10
FLOWSENSE_FILENAME:
	.ascii	"FLOWSENSE\000"
	.section	.bss.fl_struct,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	fl_struct, %object
	.size	fl_struct, 12
fl_struct:
	.space	12
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI0-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x1c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI2-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI3-.LFB9
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI4-.LFB10
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI5-.LFB0
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI7-.LFB2
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI11-.LFB11
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI12-.LFB12
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI13-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI14-.LFB8
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI16-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI17-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/flowsense.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x157
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF15
	.byte	0x1
	.4byte	.LASF16
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x1
	.byte	0x9d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.byte	0xfd
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x12d
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x145
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x15c
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x1db
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1e7
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST3
	.uleb128 0x6
	.4byte	.LASF6
	.byte	0x1
	.byte	0x5f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST4
	.uleb128 0x6
	.4byte	.LASF7
	.byte	0x1
	.byte	0xc8
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST5
	.uleb128 0x6
	.4byte	.LASF8
	.byte	0x1
	.byte	0xe9
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST6
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1fe
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST7
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x20d
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST8
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x232
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST9
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x178
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST10
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x254
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST11
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x25c
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST12
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB5
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB6
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB9
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB10
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB0
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI6
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB2
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI8
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB11
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB12
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB13
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB8
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI15
	.4byte	.LFE8
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB14
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB15
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"FLOWSENSE_get_controller_letter\000"
.LASF1:
	.ascii	"save_file_flowsense\000"
.LASF10:
	.ascii	"FLOWSENSE_get_num_controllers_in_chain\000"
.LASF8:
	.ascii	"FLOWSENSE_set_default_values\000"
.LASF6:
	.ascii	"set_controller_letter\000"
.LASF2:
	.ascii	"FLOWSENSE_copy_settings_into_guivars\000"
.LASF4:
	.ascii	"FLOWSENSE_get_controller_index\000"
.LASF15:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"flowsense_updater\000"
.LASF17:
	.ascii	"set_part_of_a_chain\000"
.LASF14:
	.ascii	"FLOWSENSE_we_are_a_slave_in_a_chain\000"
.LASF12:
	.ascii	"FLOWSENSE_extract_and_store_changes_from_GuiVars\000"
.LASF11:
	.ascii	"FLOWSENSE_we_are_a_master_one_way_or_another\000"
.LASF16:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/flowsense.c\000"
.LASF0:
	.ascii	"init_file_flowsense\000"
.LASF13:
	.ascii	"FLOWSENSE_we_are_the_master_of_a_multiple_controlle"
	.ascii	"r_network\000"
.LASF7:
	.ascii	"set_num_controllers_in_chain\000"
.LASF9:
	.ascii	"FLOWSENSE_we_are_poafs\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
