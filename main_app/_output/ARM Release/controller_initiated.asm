	.file	"controller_initiated.c"
	.text
.Ltext0:
	.global	__udivsi3
	.section	.text.restart_mobile_status_timer,"ax",%progbits
	.align	2
	.type	restart_mobile_status_timer, %function
restart_mobile_status_timer:
.LFB54:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L3
	stmfd	sp!, {r0, r4, lr}
.LCFI0:
	ldr	r4, [r3, #132]
	cmp	r4, #0
	beq	.L1
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, #0
	mov	r2, r0
	mov	r0, r4
	bl	xTimerGenericCommand
.L1:
	ldmfd	sp!, {r3, r4, pc}
.L4:
	.align	2
.L3:
	.word	.LANCHOR0
.LFE54:
	.size	restart_mobile_status_timer, .-restart_mobile_status_timer
	.section	.text.restart_hub_packet_activity_timer,"ax",%progbits
	.align	2
	.type	restart_hub_packet_activity_timer, %function
restart_hub_packet_activity_timer:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mvn	r3, #0
	stmfd	sp!, {r0, lr}
.LCFI1:
	ldr	r2, .L6
	str	r3, [sp, #0]
	ldr	r3, .L6+4
	mov	r1, #2
	ldr	r0, [r3, #184]
	mov	r3, #0
	bl	xTimerGenericCommand
	ldmfd	sp!, {r3, pc}
.L7:
	.align	2
.L6:
	.word	2880000
	.word	.LANCHOR0
.LFE0:
	.size	restart_hub_packet_activity_timer, .-restart_hub_packet_activity_timer
	.section	.text.ci_start_the_waiting_to_start_the_connection_process_timer_seconds,"ax",%progbits
	.align	2
	.type	ci_start_the_waiting_to_start_the_connection_process_timer_seconds, %function
ci_start_the_waiting_to_start_the_connection_process_timer_seconds:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #1000
	stmfd	sp!, {r0, lr}
.LCFI2:
	mul	r0, r3, r0
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L9
	mov	r1, #2
	mov	r2, r0
	ldr	r0, [r3, #36]
	mov	r3, #0
	bl	xTimerGenericCommand
	ldmfd	sp!, {r3, pc}
.L10:
	.align	2
.L9:
	.word	.LANCHOR0
.LFE5:
	.size	ci_start_the_waiting_to_start_the_connection_process_timer_seconds, .-ci_start_the_waiting_to_start_the_connection_process_timer_seconds
	.section	.text.start_the_polling_timer,"ax",%progbits
	.align	2
	.type	start_the_polling_timer, %function
start_the_polling_timer:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #1000
	stmfd	sp!, {r0, lr}
.LCFI3:
	mul	r0, r3, r0
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L12
	mov	r1, #2
	mov	r2, r0
	ldr	r0, [r3, #180]
	mov	r3, #0
	bl	xTimerGenericCommand
	ldmfd	sp!, {r3, pc}
.L13:
	.align	2
.L12:
	.word	.LANCHOR0
.LFE2:
	.size	start_the_polling_timer, .-start_the_polling_timer
	.section	.text.mobile_copy_util,"ax",%progbits
	.align	2
	.type	mobile_copy_util, %function
mobile_copy_util:
.LFB56:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI4:
	ldr	r5, .L15
	mov	r3, r0
	mov	r4, r1
	mov	r2, r4
	mov	r1, r3
	ldr	r0, [r5, #0]
	bl	memcpy
	ldr	r3, [r5, #0]
	add	r3, r3, r4
	str	r3, [r5, #0]
	ldr	r3, .L15+4
	ldr	r2, [r3, #4]
	add	r4, r4, r2
	str	r4, [r3, #4]
	ldmfd	sp!, {r4, r5, pc}
.L16:
	.align	2
.L15:
	.word	.LANCHOR1
	.word	.LANCHOR2
.LFE56:
	.size	mobile_copy_util, .-mobile_copy_util
	.section	.text.ci_start_the_connection_process,"ax",%progbits
	.align	2
	.type	ci_start_the_connection_process, %function
ci_start_the_connection_process:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI5:
	bl	CONFIG_this_controller_originates_commserver_messages
	ldr	r3, .L24
	cmp	r0, #0
	beq	.L18
	ldr	r2, [r3, #0]
	cmp	r2, #3
	bne	.L19
	ldr	r3, [r3, #4]
	cmp	r3, #0
	ldmnefd	sp!, {r4, r5, r6, r7, pc}
	b	.L20
.L19:
	cmp	r2, #1
	ldmnefd	sp!, {r4, r5, r6, r7, pc}
.L20:
	ldr	r7, .L24+4
	mov	r6, #56
	ldr	r2, [r7, #80]
	ldr	r5, .L24+8
	mul	r2, r6, r2
	ldr	r4, .L24
	add	r3, r5, r2
	ldr	r2, [r5, r2]
	cmp	r2, #0
	beq	.L22
	ldr	r2, [r3, #36]
	cmp	r2, #0
	beq	.L23
	ldr	r3, [r3, #40]
	cmp	r3, #0
	beq	.L23
	bl	Alert_ci_starting_connection_process
	ldr	r3, .L24+12
	mov	r2, #0
	str	r2, [r3, #0]
	mov	r3, #2
	str	r3, [r4, #0]
	ldr	r3, [r7, #80]
	mov	r0, #1
	mla	r5, r6, r3, r5
	ldr	r3, [r5, #36]
	blx	r3
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L23:
.LBB58:
	ldr	r0, .L24+16
	bl	Alert_Message
.L22:
.LBE58:
	mov	r3, #1
	str	r3, [r4, #0]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L18:
	mov	r2, #1
	mov	r0, #60
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	ci_start_the_waiting_to_start_the_connection_process_timer_seconds
.L25:
	.align	2
.L24:
	.word	.LANCHOR0
	.word	config_c
	.word	port_device_table
	.word	GuiVar_LiveScreensCommCentralConnected
	.word	.LC0
.LFE7:
	.size	ci_start_the_connection_process, .-ci_start_the_connection_process
	.section	.text.set_now_xmitting_and_send_the_msg,"ax",%progbits
	.align	2
	.type	set_now_xmitting_and_send_the_msg, %function
set_now_xmitting_and_send_the_msg:
.LFB18:
	@ args = 8, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI6:
	sub	sp, sp, #80
.LCFI7:
	bne	.L27
	ldr	r0, .L58+28
	bl	Alert_Message
	b	.L26
.L27:
	ldr	r4, .L58+32
	str	r3, [r4, #20]
	ldr	r3, [sp, #116]
	str	r0, [r4, #8]
	str	r3, [r4, #24]
	ldr	r3, [sp, #120]
	str	r1, [r4, #12]
	str	r3, [r4, #28]
.LBB65:
	ldr	r3, .L58+36
.LBE65:
	str	r2, [r4, #16]
.LBB72:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L58+40
	ldr	r3, .L58+44
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #0
	str	r3, [r4, #48]
	str	r3, [r4, #52]
	ldr	r0, .L58+48
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, [r4, #24]
	cmp	r3, #104
	beq	.L29
	bl	Alert_divider_small
	ldrh	r0, [r4, #24]
	bl	Alert_comm_command_sent
.L29:
	ldr	r3, [r4, #16]
	mov	r8, #1
	cmp	r3, #0
	movne	r2, #1
	strne	r2, [r3, #0]
	ldr	r3, .L58+32
.LBB66:
	mov	r0, #40
.LBE66:
	ldr	r2, [r3, #20]
	str	r8, [r3, #4]
	str	r2, [r3, #76]
	ldr	fp, [r3, #8]
	ldr	r4, [r3, #12]
	ldr	r5, [r3, #24]
.LBB67:
	add	r1, sp, #60
	ldr	r2, .L58+40
	mov	r3, #864
	bl	mem_oabia
	cmp	r0, #0
	beq	.L31
	mov	r3, #40
	str	r3, [sp, #64]
	ldr	r3, .L58+52
	ldr	r6, [sp, #60]
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	add	r7, r6, #4
	strb	r2, [r6, #0]
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	strb	r2, [r6, #1]
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	strb	r2, [r6, #2]
	strb	r3, [r6, #3]
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	beq	.L32
	mov	r1, #0
	mov	r2, #2
	add	r0, sp, #76
	bl	memset
	ldrb	r3, [sp, #76]	@ zero_extendqisi2
	mov	r0, r7
	orr	r3, r3, #30
	add	r1, sp, #76
	mov	r2, #2
	strb	r3, [sp, #76]
	strb	r8, [sp, #77]
	add	r6, r6, #6
	bl	memcpy
	mov	sl, #28
	b	.L33
.L32:
	ldr	r3, [sp, #64]
	mov	r6, r7
	sub	r3, r3, #2
	mov	sl, #26
	str	r3, [sp, #64]
.L33:
	bl	NETWORK_CONFIG_get_network_id
	ldr	r3, .L58+56
	mov	r8, r5, asl #16
	ldr	r3, [r3, #48]
	mov	r8, r8, lsr #16
	str	r3, [sp, #32]
	add	r1, sp, #32
	mov	r3, #26
	mov	r2, #14
	str	r3, [sp, #40]
	strh	r8, [sp, #44]	@ movhi
	add	r9, r6, #14
	str	r0, [sp, #36]
	mov	r0, r6
	bl	memcpy
	mov	r1, r4
	mov	r0, fp
	str	r4, [sp, #48]
	bl	CRC_calculate_32bit_big_endian
	mov	r3, r4, asl #5
	mov	r3, r3, lsr #16
	movs	r4, r4, asl #21
	strh	r3, [sp, #56]	@ movhi
	addne	r3, r3, #1
	strneh	r3, [sp, #56]	@ movhi
	cmp	r5, #104
	strh	r8, [sp, #58]	@ movhi
	str	r0, [sp, #52]
	bne	.L35
	ldr	r2, .L58+60
	ldr	r3, .L58+64
	ldr	r0, [sp, #64]
	ldr	r1, [r2, r3]
	add	r1, r0, r1
	ldr	r0, [sp, #48]
	add	r1, r1, r0
	str	r1, [r2, r3]
	b	.L36
.L35:
	mov	r0, r8
	ldr	r1, [sp, #48]
	ldrh	r2, [sp, #56]
	bl	Alert_outbound_message_size
.L36:
	add	r1, sp, #48
	mov	r2, #12
	mov	r0, r9
	bl	memcpy
	mov	r1, sl
	mov	r0, r7
	bl	CRC_calculate_32bit_big_endian
	add	r1, sp, #80
	mov	r2, #4
	str	r0, [r1, #-12]!
	add	r0, r6, #26
	bl	memcpy
	ldr	r3, .L58+68
	mov	r0, #1
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	strb	r2, [r6, #30]
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	strb	r2, [r6, #31]
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	strb	r2, [r6, #32]
	strb	r3, [r6, #33]
	add	r2, sp, #60
	ldmia	r2, {r1-r2}
	mov	r3, #0
	str	r3, [sp, #0]
	str	r0, [sp, #4]
	bl	AddCopyOfBlockToXmitList
	ldr	r0, [sp, #60]
	ldr	r1, .L58+40
	mov	r2, #996
	bl	mem_free_debug
	b	.L37
.L31:
	ldr	r0, .L58+72
	bl	Alert_Message
.L37:
.LBE67:
	ldr	r3, .L58+32
.LBB68:
.LBB69:
	mov	r0, #2080
.LBE69:
.LBE68:
	ldr	r2, [r3, #8]
	ldr	r4, [r3, #12]
	str	r2, [sp, #16]
	ldr	r6, [r3, #28]
.LBB71:
.LBB70:
	add	r1, sp, #60
	ldr	r2, .L58+40
	ldr	r3, .L58+76
	bl	mem_oabia
	cmp	r0, #0
	beq	.L38
	movs	r3, r4, asl #21
	mov	r5, r4, lsr #11
	addne	r5, r5, #1
	sub	r4, r4, r5, asl #11
	add	r3, r4, #2064
	mov	r6, r6, asl #16
	add	r2, r4, #2048
	add	r3, r3, #2
	mov	r6, r6, lsr #16
	str	r4, [sp, #12]
	str	r2, [sp, #20]
	str	r3, [sp, #28]
	mov	r4, #1
	ldr	r9, .L58+52
	str	r6, [sp, #24]
	b	.L40
.L44:
	mov	r3, #2080
	ldr	r7, [sp, #60]
	str	r3, [sp, #64]
	ldrb	r3, [r9, #3]	@ zero_extendqisi2
	add	fp, r7, #4
	strb	r3, [r7, #0]
	ldrb	r3, [r9, #2]	@ zero_extendqisi2
	strb	r3, [r7, #1]
	ldrb	r3, [r9, #1]	@ zero_extendqisi2
	strb	r3, [r7, #2]
	ldrb	r3, [r9, #0]	@ zero_extendqisi2
	strb	r3, [r7, #3]
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	beq	.L41
	mov	r1, #0
	mov	r2, #2
	add	r0, sp, #76
	bl	memset
	ldrb	r3, [sp, #76]	@ zero_extendqisi2
	mov	r0, fp
	orr	r3, r3, #30
	strb	r3, [sp, #76]
	add	r1, sp, #76
	mov	r3, #1
	mov	r2, #2
	strb	r3, [sp, #77]
	add	r7, r7, #6
	bl	memcpy
	ldr	sl, .L58+80
	b	.L42
.L41:
	ldr	r3, [sp, #64]
	ldr	sl, .L58+84
	sub	r3, r3, #2
	mov	r7, fp
	str	r3, [sp, #64]
.L42:
	bl	NETWORK_CONFIG_get_network_id
	ldr	r3, .L58+56
	add	r1, sp, #72
	ldr	r3, [r3, #48]
	mov	r2, #4
	str	r3, [sp, #32]
	ldr	r3, [sp, #24]
	strh	r4, [sp, #72]	@ movhi
	strh	r3, [sp, #44]	@ movhi
	strh	r3, [sp, #74]	@ movhi
	add	r6, r7, #18
	str	r0, [sp, #36]
	add	r0, r7, #14
	bl	memcpy
	cmp	r4, r5
	movne	r8, #2048
	ldrne	r3, .L58+84
	bne	.L43
	ldr	r2, [sp, #12]
	ldr	r3, [sp, #64]
	add	sl, sl, r2
	ldr	r2, [sp, #20]
	sub	r3, r3, #2048
	add	r3, r3, r2
	str	r3, [sp, #64]
	ldr	r3, [sp, #28]
	mov	r8, r2
.L43:
	mov	r0, r6
	ldr	r1, [sp, #16]
	mov	r2, r8
	str	r3, [sp, #8]
	bl	memcpy
	ldr	r2, [sp, #16]
	ldr	r3, [sp, #8]
	add	r2, r2, r8
	str	r2, [sp, #16]
	add	r1, sp, #32
	mov	r2, #14
	mov	r0, r7
	str	r3, [sp, #40]
	bl	memcpy
	mov	r1, sl
	mov	r0, fp
	bl	CRC_calculate_32bit_big_endian
	add	r6, r6, r8
	add	r1, sp, #80
	mov	r2, #4
	add	r4, r4, #1
	str	r0, [r1, #-12]!
	mov	r0, r6
	bl	memcpy
	ldr	r3, .L58+68
	mov	r0, #1
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	strb	r2, [r6, #4]
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	strb	r2, [r6, #5]
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	strb	r2, [r6, #6]
	strb	r3, [r6, #7]
	add	r2, sp, #60
	ldmia	r2, {r1-r2}
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	bl	AddCopyOfBlockToXmitList
.L40:
	cmp	r4, r5
	bls	.L44
	mov	r0, #1
	mov	r1, r0
	bl	postSerportDrvrEvent
	ldr	r0, [sp, #60]
	ldr	r1, .L58+40
	ldr	r2, .L58+88
	bl	mem_free_debug
	b	.L45
.L38:
	ldr	r0, .L58+92
	bl	Alert_Message
.L45:
.LBE70:
.LBE71:
	ldr	r4, .L58+32
	ldr	r1, .L58+40
	ldr	r0, [r4, #8]
	ldr	r2, .L58+96
	bl	mem_free_debug
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	beq	.L53
	ldr	r2, .L58+56
	ldr	r3, [r2, #80]
	cmp	r3, #3
	bne	.L47
	ldrb	r3, [r2, #52]	@ zero_extendqisi2
	flds	s15, .L58
	flds	s14, .L58+4
	tst	r3, #128
	b	.L57
.L47:
	cmp	r3, #2
	bne	.L49
	ldrb	r3, [r2, #52]	@ zero_extendqisi2
	flds	s15, .L58+8
	flds	s14, .L58+12
	tst	r3, #64
.L57:
	ldr	r3, [r4, #12]
	fcpysne	s14, s15
	mov	r2, #10
	mul	r3, r2, r3
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s14, s15, s14
	flds	s15, .L58+16
	fmuls	s14, s14, s15
	fcvtds	d7, s14
	b	.L56
.L49:
	cmp	r3, #1
	bne	.L51
	ldr	r3, [r4, #12]
	mov	r0, #10
	mul	r0, r3, r0
	ldr	r1, .L58+100
	bl	__udivsi3
	mov	r3, #1000
	mul	r3, r0, r3
	fmsr	s13, r3	@ int
	fuitod	d7, s13
.L56:
	fldd	d6, .L58+20
	fmuld	d7, d7, d6
	ftouizd	s13, d7
	fmrs	r0, s13	@ int
	b	.L46
.L51:
	ldr	r0, .L58+104
	bl	Alert_Message
.L53:
	ldr	r0, .L58+108
.L46:
	add	r0, r0, #299008
	mov	r1, #5
	add	r0, r0, #992
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L58+32
	mov	r1, #2
	mov	r2, r0
	ldr	r0, [r3, #32]
	mov	r3, #0
	bl	xTimerGenericCommand
	ldr	r3, .L58+36
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L26:
.LBE72:
	add	sp, sp, #80
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L59:
	.align	2
.L58:
	.word	1166639104
	.word	1175027712
	.word	1150271488
	.word	1158660096
	.word	1148846080
	.word	858993459
	.word	1072902963
	.word	.LC1
	.word	.LANCHOR0
	.word	ci_and_comm_mngr_activity_recursive_MUTEX
	.word	.LC2
	.word	1245
	.word	.LANCHOR3
	.word	preamble
	.word	config_c
	.word	SerDrvrVars_s
	.word	8552
	.word	postamble
	.word	.LC3
	.word	1042
	.word	2068
	.word	2066
	.word	1206
	.word	.LC4
	.word	1320
	.word	4400
	.word	.LC5
	.word	120000
.LFE18:
	.size	set_now_xmitting_and_send_the_msg, .-set_now_xmitting_and_send_the_msg
	.section	.text.send_ci_check_for_updates,"ax",%progbits
	.align	2
	.type	send_ci_check_for_updates, %function
send_ci_check_for_updates:
.LFB32:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI8:
	ldr	r4, .L65
	sub	sp, sp, #24
.LCFI9:
	ldr	r3, [r4, #88]
	mov	r7, r0
	cmp	r3, #0
	mov	r6, r1
	mov	r5, r2
	beq	.L61
.LBB75:
	mov	r0, #16
	add	r1, sp, #8
	ldr	r2, .L65+4
	ldr	r3, .L65+8
	bl	mem_oabia
	cmp	r0, #0
	beq	.L62
	mov	sl, #0
	ldr	r1, .L65+12
	mov	r0, #-2147483648
	ldr	r8, [sp, #8]
	str	sl, [sp, #12]
	bl	convert_code_image_date_to_version_number
	add	r1, sp, #24
	mov	r2, #2
	str	r0, [r1, #-8]!
	mov	r0, r8
	bl	memcpy
	ldr	r3, [sp, #12]
	ldr	r1, .L65+12
	add	r3, r3, #2
	mov	r0, #-2147483648
	str	r3, [sp, #12]
	bl	convert_code_image_time_to_edit_count
	add	r1, sp, #24
	mov	r2, #4
	str	r0, [r1, #-4]!
	add	r0, r8, #2
	bl	memcpy
	ldr	r3, [sp, #12]
	add	r1, r4, #80
	add	r3, r3, #4
	mov	r2, #2
	add	r0, r8, #6
	str	r3, [sp, #12]
	bl	memcpy
	ldr	r3, [sp, #12]
	add	r1, r4, #84
	add	r3, r3, #2
	mov	r2, #4
	add	r0, r8, #8
	str	r3, [sp, #12]
	bl	memcpy
	ldr	r3, [sp, #12]
	mov	r2, r5
	add	r3, r3, #4
	str	r3, [sp, #12]
	add	r1, sp, #8
	ldmia	r1, {r0-r1}
	mov	r3, sl
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	bl	set_now_xmitting_and_send_the_msg
	ldr	r0, .L65+16
	b	.L64
.L62:
	ldr	r0, .L65+20
	bl	Alert_Message
	ldr	r0, .L65+24
	b	.L64
.L61:
.LBE75:
	ldr	r0, .L65+28
	bl	Alert_Message
	ldr	r0, .L65+32
.L64:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L66:
	.align	2
.L65:
	.word	tpmicro_comm
	.word	.LC2
	.word	2607
	.word	CS3000_APP_FILENAME
	.word	1029
	.word	.LC6
	.word	1027
	.word	.LC7
	.word	1025
.LFE32:
	.size	send_ci_check_for_updates, .-send_ci_check_for_updates
	.section	.text.CONTROLLER_INITIATED_update_comm_server_registration_info,"ax",%progbits
	.align	2
	.global	CONTROLLER_INITIATED_update_comm_server_registration_info
	.type	CONTROLLER_INITIATED_update_comm_server_registration_info, %function
CONTROLLER_INITIATED_update_comm_server_registration_info:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI10:
	ldr	r4, .L68
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L68+4
	ldr	r3, .L68+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L68+12
	ldr	r0, [r4, #0]
	mov	r2, #1
	str	r2, [r3, #16]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L69:
	.align	2
.L68:
	.word	ci_and_comm_mngr_activity_recursive_MUTEX
	.word	.LC2
	.word	1752
	.word	battery_backed_general_use
.LFE21:
	.size	CONTROLLER_INITIATED_update_comm_server_registration_info, .-CONTROLLER_INITIATED_update_comm_server_registration_info
	.section	.text.CONTROLLER_INITIATED_alerts_timer_upkeep,"ax",%progbits
	.align	2
	.global	CONTROLLER_INITIATED_alerts_timer_upkeep
	.type	CONTROLLER_INITIATED_alerts_timer_upkeep, %function
CONTROLLER_INITIATED_alerts_timer_upkeep:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI11:
	ldr	r4, .L73
	mov	r5, r0
	ldr	r0, [r4, #64]
	mov	r6, r1
	cmp	r0, #0
	beq	.L70
	bl	xTimerIsTimerActive
	cmp	r0, #0
	beq	.L72
	cmp	r6, #0
	beq	.L70
.L72:
	mov	r1, #5
	mov	r0, r5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, #0
	mov	r2, r0
	ldr	r0, [r4, #64]
	bl	xTimerGenericCommand
.L70:
	ldmfd	sp!, {r3, r4, r5, r6, pc}
.L74:
	.align	2
.L73:
	.word	.LANCHOR0
.LFE25:
	.size	CONTROLLER_INITIATED_alerts_timer_upkeep, .-CONTROLLER_INITIATED_alerts_timer_upkeep
	.section	.text.CONTROLLER_INITIATED_post_to_messages_queue,"ax",%progbits
	.align	2
	.global	CONTROLLER_INITIATED_post_to_messages_queue
	.type	CONTROLLER_INITIATED_post_to_messages_queue, %function
CONTROLLER_INITIATED_post_to_messages_queue:
.LFB72:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI12:
	mov	sl, r3
	ldr	r3, .L101
	mov	fp, r1
	mov	r1, #0
	mov	r4, r0
	mov	r5, r2
	ldr	r0, [r3, #0]
	mvn	r2, #0
	mov	r3, r1
	bl	xQueueGenericReceive
	bl	CONFIG_this_controller_originates_commserver_messages
	cmp	r0, #0
	beq	.L76
	ldr	r7, .L101+4
	ldr	r0, [r7, #176]
	bl	uxQueueMessagesWaiting
	cmp	r5, #512
	mov	r8, r0
	bne	.L94
	b	.L98
.L81:
	mov	r2, #0
	ldr	r0, [r7, #176]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L79
	ldr	r3, [sp, #0]
	mov	r2, #0
	cmp	r3, r4
	ldr	r0, [r9, #176]
	mov	r1, sp
	mov	r3, r2
	moveq	r5, #0
	bl	xQueueGenericSend
.L79:
	add	r6, r6, #1
	b	.L77
.L98:
	mov	r5, #1
	mov	r6, #0
	mov	r9, r7
.L77:
	cmp	r6, r8
	bne	.L81
	b	.L99
.L94:
	cmp	r5, #256
	bne	.L96
	b	.L100
.L86:
	mov	r2, #0
	ldr	r0, [r7, #176]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L85
	ldr	r3, [sp, #0]
	cmp	r3, r4
	beq	.L85
	mov	r2, #0
	ldr	r0, [r7, #176]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericSend
.L85:
	add	r5, r5, #1
	b	.L83
.L100:
	mov	r5, #0
.L83:
	cmp	r5, r8
	bne	.L86
	b	.L90
.L96:
	ldr	r0, .L101+8
	mov	r1, r5
	bl	Alert_Message_va
	b	.L90
.L99:
	cmp	r5, #0
	beq	.L76
.L90:
	ldr	r3, .L101+4
	cmp	sl, #1
	mov	r2, #0
	ldr	r0, [r3, #176]
	mov	r1, sp
	moveq	r3, sl
	movne	r3, r2
	stmia	sp, {r4, fp}
	bl	xQueueGenericSend
	cmp	r0, #0
	bne	.L76
	ldr	r0, .L101+12
	bl	Alert_Message
.L76:
	ldr	r3, .L101
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L102:
	.align	2
.L101:
	.word	ci_message_list_MUTEX
	.word	.LANCHOR0
	.word	.LC8
	.word	.LC9
.LFE72:
	.size	CONTROLLER_INITIATED_post_to_messages_queue, .-CONTROLLER_INITIATED_post_to_messages_queue
	.section	.text.mobile_status_timer_callback,"ax",%progbits
	.align	2
	.type	mobile_status_timer_callback, %function
mobile_status_timer_callback:
.LFB55:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L108
	str	lr, [sp, #-4]!
.LCFI13:
	ldr	r3, [r3, #136]
	cmp	r3, #300
	ldrcs	pc, [sp], #4
	cmp	r3, #120
	ldrhi	r0, .L108+4
	ldrls	r0, .L108+8
	bl	restart_mobile_status_timer
.LBB78:
	ldr	r0, .L108+12
	mov	r1, #0
	mov	r2, #512
	mov	r3, r1
.LBE78:
	ldr	lr, [sp], #4
.LBB79:
	b	CONTROLLER_INITIATED_post_to_messages_queue
.L109:
	.align	2
.L108:
	.word	.LANCHOR0
	.word	60000
	.word	15000
	.word	413
.LBE79:
.LFE55:
	.size	mobile_status_timer_callback, .-mobile_status_timer_callback
	.section	.text.send_rain_indication_timer_callback,"ax",%progbits
	.align	2
	.type	send_rain_indication_timer_callback, %function
send_rain_indication_timer_callback:
.LFB50:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L112
	ldr	r3, [r3, #60]
	cmp	r3, #0
	bxeq	lr
.LBB82:
	mov	r1, #0
	mov	r0, #412
	mov	r2, #512
	mov	r3, r1
.LBE82:
.LBB83:
	b	CONTROLLER_INITIATED_post_to_messages_queue
.L113:
	.align	2
.L112:
	.word	weather_preserves
.LBE83:
.LFE50:
	.size	send_rain_indication_timer_callback, .-send_rain_indication_timer_callback
	.section	.text.send_weather_data_timer_callback,"ax",%progbits
	.align	2
	.type	send_weather_data_timer_callback, %function
send_weather_data_timer_callback:
.LFB46:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, lr}
.LCFI14:
	mov	r0, sp
	bl	EPSON_obtain_latest_time_and_date
	ldr	r2, [sp, #0]
	ldr	r3, .L116
	cmp	r2, r3
	bhi	.L114
.LBB86:
	mov	r1, #0
	ldr	r0, .L116+4
	mov	r2, #512
	mov	r3, r1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
.L114:
.LBE86:
	ldmfd	sp!, {r2, r3, pc}
.L117:
	.align	2
.L116:
	.word	72599
	.word	411
.LFE46:
	.size	send_weather_data_timer_callback, .-send_weather_data_timer_callback
	.section	.text.ci_pdata_timer_callback,"ax",%progbits
	.align	2
	.type	ci_pdata_timer_callback, %function
ci_pdata_timer_callback:
.LFB59:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L119
	mov	r1, #0
	mov	r2, #256
	mov	r3, #1
	b	CONTROLLER_INITIATED_post_to_messages_queue
.L120:
	.align	2
.L119:
	.word	415
.LFE59:
	.size	ci_pdata_timer_callback, .-ci_pdata_timer_callback
	.section	.text.ci_alerts_timer_callback,"ax",%progbits
	.align	2
	.global	ci_alerts_timer_callback
	.type	ci_alerts_timer_callback, %function
ci_alerts_timer_callback:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L122
	mov	r1, #0
	mov	r2, #512
	mov	r3, r1
	b	CONTROLLER_INITIATED_post_to_messages_queue
.L123:
	.align	2
.L122:
	.word	401
.LFE24:
	.size	ci_alerts_timer_callback, .-ci_alerts_timer_callback
	.section	.text.CONTROLLER_INITIATED_post_event_with_details,"ax",%progbits
	.align	2
	.global	CONTROLLER_INITIATED_post_event_with_details
	.type	CONTROLLER_INITIATED_post_event_with_details, %function
CONTROLLER_INITIATED_post_event_with_details:
.LFB75:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L126
	mov	r2, #0
	mov	r1, r0
	str	lr, [sp, #-4]!
.LCFI15:
	ldr	r0, [r3, #0]
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	ldreq	pc, [sp], #4
	ldr	r0, .L126+4
	ldr	lr, [sp], #4
	b	Alert_Message
.L127:
	.align	2
.L126:
	.word	CONTROLLER_INITIATED_task_queue
	.word	.LC10
.LFE75:
	.size	CONTROLLER_INITIATED_post_event_with_details, .-CONTROLLER_INITIATED_post_event_with_details
	.section	.text.CONTROLLER_INITIATED_post_event,"ax",%progbits
	.align	2
	.global	CONTROLLER_INITIATED_post_event
	.type	CONTROLLER_INITIATED_post_event, %function
CONTROLLER_INITIATED_post_event:
.LFB76:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI16:
	add	r3, sp, #20
	str	r0, [r3, #-20]!
	mov	r0, sp
	bl	CONTROLLER_INITIATED_post_event_with_details
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.LFE76:
	.size	CONTROLLER_INITIATED_post_event, .-CONTROLLER_INITIATED_post_event
	.section	.text.__clear_all_waiting_for_response_flags,"ax",%progbits
	.align	2
	.type	__clear_all_waiting_for_response_flags, %function
__clear_all_waiting_for_response_flags:
.LFB11:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L156
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI17:
	ldr	r3, [r3, #128]
	adds	r5, r1, #0
	movne	r5, #1
	cmp	r3, #0
	movne	r3, #0
	andeq	r3, r5, #1
	cmp	r3, #0
	sub	sp, sp, #32
.LCFI18:
	mov	r4, r0
	beq	.L130
.LBB87:
	add	r0, sp, #24
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, .L156+4
	ldr	r0, [sp, #24]
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #8
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #16
	orr	r3, r2, r3, asl #24
	cmp	r0, r3
	bcc	.L131
	rsb	r0, r3, r0
	bl	Alert_msg_transaction_time
	b	.L130
.L131:
	ldr	r0, .L156+8
	bl	Alert_Message
.L130:
.LBE87:
	ldr	r3, .L156
	ldr	r2, [r3, #128]
	cmp	r2, #0
	movne	r5, #0
	andeq	r5, r5, #1
	cmp	r5, #0
	mov	r5, r3
	beq	.L132
	bl	Alert_divider_small
.L132:
	ldr	r2, [r5, #56]
	ldr	r3, .L156
	cmp	r2, #0
	movne	r2, #0
	strne	r2, [r3, #60]
	strne	r2, [r3, #56]
	ldr	r3, [r5, #68]
	cmp	r3, #0
	beq	.L134
	ldr	r5, .L156+12
	mov	r1, #400
	ldr	r2, .L156+16
	ldr	r3, .L156+20
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L156+24
	mov	r6, #0
	str	r6, [r3, #44]
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L156
	str	r6, [r3, #68]
.L134:
	ldr	r6, .L156
	ldr	r3, [r6, #72]
	cmp	r3, #0
	beq	.L135
	ldr	r5, .L156+28
	mov	r3, #588
	ldr	r0, [r5, #0]
	mov	r1, #400
	ldr	r2, .L156+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r6, #76]
	cmp	r3, #0
	beq	.L136
	ldr	r2, [r3, #20]
	cmp	r2, #0
	movne	r2, #0
	strne	r2, [r3, #20]
	ldreq	r0, .L156+32
	bne	.L138
	b	.L155
.L136:
	ldr	r0, .L156+36
.L155:
	bl	Alert_Message
.L138:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L156
	mov	r2, #0
	str	r2, [r3, #72]
.L135:
	ldr	r3, .L156
	ldr	r3, [r3, #80]
	cmp	r3, #0
	beq	.L139
	ldr	r5, .L156+40
	ldr	r2, .L156+16
	ldr	r3, .L156+44
	ldr	r0, [r5, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L156+48
	ldr	r2, [r3, #20]
	cmp	r2, #0
	movne	r2, #0
	strne	r2, [r3, #20]
	bne	.L141
	ldr	r0, .L156+52
	bl	Alert_Message
.L141:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L156
	mov	r2, #0
	str	r2, [r3, #80]
.L139:
	ldr	r3, .L156
	mov	r2, #0
	ldr	r1, [r3, #88]
	str	r2, [r3, #84]
	cmp	r1, r2
	ldrne	r1, .L156+56
	strne	r2, [r3, #88]
	strne	r2, [r1, #24]
	ldr	r2, [r3, #92]
	ldr	r3, [r3, #96]
	cmp	r2, #0
	ldrne	r1, .L156+60
	movne	r2, #0
	strne	r2, [r1, #24]
	ldrne	r1, .L156
	ldr	r5, .L156
	strne	r2, [r1, #92]
	cmp	r3, #0
	ldrne	r2, .L156+64
	movne	r3, #0
	strne	r3, [r2, #24]
	ldrne	r2, .L156
	strne	r3, [r2, #96]
	ldr	r3, .L156
	ldr	r2, [r3, #104]
	cmp	r2, #0
	movne	r2, #0
	strne	r2, [r3, #104]
	ldrne	r1, .L156+68
	ldr	r3, [r3, #108]
	strne	r2, [r1, #24]
	cmp	r3, #0
	ldrne	r2, .L156+72
	movne	r3, #0
	strne	r3, [r2, #24]
	ldrne	r2, .L156
	strne	r3, [r2, #108]
	ldr	r3, .L156
	ldr	r2, [r3, #100]
	cmp	r2, #0
	movne	r2, #0
	strne	r2, [r3, #100]
	ldrne	r1, .L156+76
	ldr	r3, [r3, #164]
	strne	r2, [r1, #24]
	cmp	r3, #0
	ldrne	r2, .L156+80
	movne	r3, #0
	strne	r3, [r2, #492]
	ldrne	r2, .L156
	strne	r3, [r2, #164]
	ldr	r3, [r5, #112]
	cmp	r3, #0
	beq	.L149
	mov	r3, #0
	cmp	r4, r3
	str	r3, [r5, #112]
	beq	.L149
	mov	r0, #135
	bl	CONTROLLER_INITIATED_post_event
.L149:
	ldr	r3, [r5, #120]
	cmp	r3, #0
	beq	.L150
	ldr	r3, .L156
	mov	r2, #0
	cmp	r4, r2
	str	r2, [r3, #120]
	beq	.L150
	mov	r0, #136
	bl	CONTROLLER_INITIATED_post_event
.L150:
	ldr	r5, .L156
	ldr	r3, [r5, #128]
	cmp	r3, #0
	beq	.L151
	mov	r3, #0
	cmp	r4, r3
	str	r3, [r5, #128]
	beq	.L151
	mov	r3, #140
	str	r3, [sp, #4]
	ldr	r3, .L156+84
	add	r0, sp, #4
	str	r3, [sp, #12]
	bl	CONTROLLER_INITIATED_post_event_with_details
.L151:
	ldr	r3, [r5, #140]
	ldr	r2, .L156
	cmp	r3, #0
	beq	.L152
	mov	r3, #0
	cmp	r4, r3
	str	r3, [r2, #140]
	beq	.L152
	mvn	r1, #0
	str	r1, [sp, #0]
	mov	r1, #2
	ldr	r0, [r2, #152]
	ldr	r2, .L156+88
	bl	xTimerGenericCommand
.L152:
	ldr	r5, .L156
	ldr	r3, [r5, #148]
	cmp	r3, #0
	beq	.L153
	cmp	r4, #0
	beq	.L154
	mov	r0, #1
	bl	PDATA_update_pending_change_bits
.L154:
	mov	r3, #0
	str	r3, [r5, #144]
	str	r3, [r5, #148]
.L153:
	mov	r3, #0
	str	r3, [r5, #160]
	str	r3, [r5, #168]
	str	r3, [r5, #172]
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, pc}
.L157:
	.align	2
.L156:
	.word	.LANCHOR0
	.word	.LANCHOR3
	.word	.LC11
	.word	alerts_pile_recursive_MUTEX
	.word	.LC2
	.word	567
	.word	alerts_struct_engineering
	.word	system_preserves_recursive_MUTEX
	.word	.LC12
	.word	.LC13
	.word	moisture_sensor_recorder_recursive_MUTEX
	.word	618
	.word	msrcs
	.word	.LC14
	.word	station_history_completed
	.word	station_report_data_completed
	.word	poc_report_data_completed
	.word	budget_report_data_completed
	.word	lights_report_data_completed
	.word	system_report_data_completed
	.word	weather_tables
	.word	3000
	.word	60000
.LFE11:
	.size	__clear_all_waiting_for_response_flags, .-__clear_all_waiting_for_response_flags
	.section	.text.CONTROLLER_INITIATED_force_end_of_transaction,"ax",%progbits
	.align	2
	.global	CONTROLLER_INITIATED_force_end_of_transaction
	.type	CONTROLLER_INITIATED_force_end_of_transaction, %function
CONTROLLER_INITIATED_force_end_of_transaction:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI19:
	ldr	r4, .L160
	ldr	r3, [r4, #0]
	cmp	r3, #3
	bne	.L158
	ldr	r5, [r4, #4]
	cmp	r5, #1
	bne	.L158
	ldr	r0, .L160+4
	bl	Alert_Message
	mov	r2, #0
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, r5
	mov	r3, r2
	ldr	r0, [r4, #32]
	bl	xTimerGenericCommand
	mov	r1, #0
	mov	r2, #24
	add	r0, r4, #8
	bl	memset
	mov	r0, r5
	mov	r1, r5
	bl	__clear_all_waiting_for_response_flags
	mov	r3, #0
	str	r3, [r4, #4]
.L158:
	ldmfd	sp!, {r3, r4, r5, pc}
.L161:
	.align	2
.L160:
	.word	.LANCHOR0
	.word	.LC15
.LFE17:
	.size	CONTROLLER_INITIATED_force_end_of_transaction, .-CONTROLLER_INITIATED_force_end_of_transaction
	.section	.text.check_if_we_need_to_post_a_build_and_send_event,"ax",%progbits
	.align	2
	.type	check_if_we_need_to_post_a_build_and_send_event, %function
check_if_we_need_to_post_a_build_and_send_event:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L165
	str	lr, [sp, #-4]!
.LCFI20:
	cmp	r0, r3
	ldreq	pc, [sp], #4
.LBB90:
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	bne	.L164
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	ldreq	pc, [sp], #4
.L164:
	mov	r0, #110
.LBE90:
	ldr	lr, [sp], #4
.LBB91:
	b	CONTROLLER_INITIATED_post_event
.L166:
	.align	2
.L165:
	.word	1029
.LBE91:
.LFE19:
	.size	check_if_we_need_to_post_a_build_and_send_event, .-check_if_we_need_to_post_a_build_and_send_event
	.section	.text.sending_registration_instead,"ax",%progbits
	.align	2
	.type	sending_registration_instead, %function
sending_registration_instead:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI21:
	ldr	r5, .L173
	mov	r4, r0
	mov	r6, r1
	bl	NETWORK_CONFIG_get_network_id
	ldr	r3, [r5, #16]
	cmp	r3, #0
	mov	r7, r0
	bne	.L168
	ldr	r3, [r5, #20]
	cmp	r3, #0
	beq	.L169
.L168:
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	movne	r3, #1
	strne	r3, [r5, #20]
	bne	.L170
.L169:
	cmp	r7, #0
	bne	.L172
.L170:
.LBB94:
	mov	r0, r4
	mov	r1, r6
	mov	r2, #256
	mov	r3, #1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
	mov	r0, #400
	mov	r1, #0
	mov	r2, #256
	mov	r3, #1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
	mov	r0, #110
	bl	CONTROLLER_INITIATED_post_event
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L172:
.LBE94:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L174:
	.align	2
.L173:
	.word	battery_backed_general_use
.LFE20:
	.size	sending_registration_instead, .-sending_registration_instead
	.section	.text.CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg,"ax",%progbits
	.align	2
	.global	CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg
	.type	CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg, %function
CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg:
.LFB69:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r4, r5, lr}
.LCFI22:
	sub	sp, sp, #24
.LCFI23:
	beq	.L176
	ldr	r0, .L180
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L175
.L176:
.LBB95:
	mov	r0, #2
	add	r1, sp, #8
	ldr	r2, .L180+4
	ldr	r3, .L180+8
	bl	mem_oabia
	subs	r4, r0, #0
	beq	.L178
	ldr	r5, [sp, #8]
	mov	r4, #0
	add	r0, sp, #16
	str	r4, [sp, #12]
	bl	EPSON_obtain_latest_time_and_date
	add	r1, sp, #20
	mov	r2, #2
	mov	r0, r5
	bl	memcpy
	ldr	r3, [sp, #12]
	mov	r2, #163
	add	r3, r3, #2
	str	r3, [sp, #12]
	mov	r3, #164
	stmia	sp, {r2, r3}
	ldr	r2, .L180+12
	add	r1, sp, #8
	ldmia	r1, {r0-r1}
	mov	r3, r4
	bl	set_now_xmitting_and_send_the_msg
	ldr	r0, .L180+16
	b	.L179
.L178:
	ldr	r0, .L180+20
	bl	Alert_Message
.LBE95:
	ldr	r0, .L180
	mov	r1, r4
	mov	r2, #256
	mov	r3, r4
	bl	CONTROLLER_INITIATED_post_to_messages_queue
	ldr	r0, .L180+24
.L179:
	bl	check_if_we_need_to_post_a_build_and_send_event
.L175:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, pc}
.L181:
	.align	2
.L180:
	.word	419
	.word	.LC2
	.word	4895
	.word	.LANCHOR0+168
	.word	1029
	.word	.LC16
	.word	1027
.LFE69:
	.size	CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg, .-CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg
	.section	.text.connection_process_timer_callback,"ax",%progbits
	.align	2
	.type	connection_process_timer_callback, %function
connection_process_timer_callback:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #121
	b	CONTROLLER_INITIATED_post_event
.LFE8:
	.size	connection_process_timer_callback, .-connection_process_timer_callback
	.section	.text.hub_packet_activity_time_callback,"ax",%progbits
	.align	2
	.type	hub_packet_activity_time_callback, %function
hub_packet_activity_time_callback:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI24:
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	ldreq	pc, [sp], #4
.LBB98:
	ldr	r0, .L185
	bl	Alert_Message
	mov	r0, #120
.LBE98:
	ldr	lr, [sp], #4
.LBB99:
	b	CONTROLLER_INITIATED_post_event
.L186:
	.align	2
.L185:
	.word	.LC17
.LBE99:
.LFE1:
	.size	hub_packet_activity_time_callback, .-hub_packet_activity_time_callback
	.section	.text.waiting_to_start_the_connection_process_timer_callback,"ax",%progbits
	.align	2
	.type	waiting_to_start_the_connection_process_timer_callback, %function
waiting_to_start_the_connection_process_timer_callback:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #120
	b	CONTROLLER_INITIATED_post_event
.LFE6:
	.size	waiting_to_start_the_connection_process_timer_callback, .-waiting_to_start_the_connection_process_timer_callback
	.section	.text.ci_response_timer_callback,"ax",%progbits
	.align	2
	.type	ci_response_timer_callback, %function
ci_response_timer_callback:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #105
	b	CONTROLLER_INITIATED_post_event
.LFE14:
	.size	ci_response_timer_callback, .-ci_response_timer_callback
	.section	.text.ci_queued_msgs_polling_timer_callback,"ax",%progbits
	.align	2
	.type	ci_queued_msgs_polling_timer_callback, %function
ci_queued_msgs_polling_timer_callback:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI25:
	mov	r0, #110
	bl	CONTROLLER_INITIATED_post_event
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	ldrne	pc, [sp], #4
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	ldrne	pc, [sp], #4
.LBB102:
	ldr	r3, .L191
	ldr	r3, [r3, #0]
	cmp	r3, #3
	ldrne	pc, [sp], #4
	mov	r0, #5
.LBE102:
	ldr	lr, [sp], #4
.LBB103:
	b	start_the_polling_timer
.L192:
	.align	2
.L191:
	.word	.LANCHOR0
.LBE103:
.LFE3:
	.size	ci_queued_msgs_polling_timer_callback, .-ci_queued_msgs_polling_timer_callback
	.global	__umodsi3
	.section	.text.CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records,"ax",%progbits
	.align	2
	.global	CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records
	.type	CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records, %function
CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records:
.LFB77:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI26:
	mov	r1, #3600
	bl	__umodsi3
	mov	r3, #1000
	mul	r0, r3, r0
	add	r0, r0, #897024
	add	r0, r0, #2976
	ldr	pc, [sp], #4
.LFE77:
	.size	CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records, .-CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records
	.section	.text.CONTROLLER_INITIATED_ms_after_connection_to_send_alerts,"ax",%progbits
	.align	2
	.global	CONTROLLER_INITIATED_ms_after_connection_to_send_alerts
	.type	CONTROLLER_INITIATED_ms_after_connection_to_send_alerts, %function
CONTROLLER_INITIATED_ms_after_connection_to_send_alerts:
.LFB78:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI27:
	mov	r1, #240
	bl	__umodsi3
	mov	r3, #1000
	cmp	r0, #0
	moveq	r0, #5
	mul	r0, r3, r0
	ldr	pc, [sp], #4
.LFE78:
	.size	CONTROLLER_INITIATED_ms_after_connection_to_send_alerts, .-CONTROLLER_INITIATED_ms_after_connection_to_send_alerts
	.section	.text.CONTROLLER_INITIATED_task,"ax",%progbits
	.align	2
	.global	CONTROLLER_INITIATED_task
	.type	CONTROLLER_INITIATED_task, %function
CONTROLLER_INITIATED_task:
.LFB74:
	@ args = 0, pretend = 0, frame = 116
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L470
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI28:
	ldr	r4, .L470+4
	rsb	r3, r3, r0
	sub	sp, sp, #136
.LCFI29:
	mov	r3, r3, lsr #5
	mov	r1, #8
	mov	r2, #0
	mov	r0, #20
	str	r3, [sp, #24]
	bl	xQueueGenericCreate
	ldr	r3, .L470+8
	mov	r2, #0
	mov	r1, #2000
	mov	r5, #0
	str	r0, [r4, #176]
	str	r3, [sp, #0]
	ldr	r0, .L470+12
	mov	r3, r2
	bl	xTimerCreate
	ldr	r3, .L470+16
	mov	r2, #0
	ldr	r1, .L470+20
	str	r0, [r4, #180]
	str	r3, [sp, #0]
	ldr	r0, .L470+12
	mov	r3, r2
	bl	xTimerCreate
	ldr	r3, .L470+24
	mov	r2, #0
	ldr	r1, .L470+28
	str	r0, [r4, #152]
	str	r3, [sp, #0]
	ldr	r0, .L470+12
	mov	r3, r2
	bl	xTimerCreate
	ldr	r3, .L470+32
	mov	r2, #0
	mov	r1, #200
	str	r0, [r4, #32]
	str	r3, [sp, #0]
	ldr	r0, .L470+12
	mov	r3, r2
	bl	xTimerCreate
	ldr	r3, .L470+36
	mov	r2, #0
	mov	r1, #200
	str	r0, [r4, #36]
	str	r3, [sp, #0]
	ldr	r0, .L470+12
	mov	r3, r2
	bl	xTimerCreate
	ldr	r3, .L470+40
	mov	r2, #0
	mov	r1, #200
	str	r0, [r4, #184]
	str	r3, [sp, #0]
	ldr	r0, .L470+12
	mov	r3, r2
	bl	xTimerCreate
	ldr	r3, .L470+44
	mov	r2, #0
	ldr	r1, .L470+48
	str	r0, [r4, #40]
	str	r3, [sp, #0]
	ldr	r0, .L470+12
	mov	r3, r2
	bl	xTimerCreate
	ldr	r3, .L470+52
	mov	r2, #0
	ldr	r1, .L470+48
	str	r0, [r4, #116]
	str	r3, [sp, #0]
	ldr	r0, .L470+12
	mov	r3, r2
	bl	xTimerCreate
	ldr	r3, .L470+56
	mov	r2, #0
	mov	r1, #1000
	str	r0, [r4, #124]
	str	r3, [sp, #0]
	ldr	r0, .L470+12
	mov	r3, r2
	bl	xTimerCreate
	mov	r2, #24
	mov	r1, r5
	str	r5, [r4, #0]
	str	r5, [r4, #4]
	str	r0, [r4, #132]
	add	r0, r4, #8
	bl	memset
	ldr	r3, .L470+60
	mov	r0, r5
	str	r5, [r3, #0]
	mov	r1, r5
	str	r5, [r4, #44]
	bl	__clear_all_waiting_for_response_flags
	str	r5, [r4, #48]
	mov	r0, #1
	bl	PDATA_update_pending_change_bits
	ldr	r3, .L470+64
	ldr	r3, [r3, #112]
	cmp	r3, #1
	bne	.L415
	ldr	r0, .L470+68
	bl	Alert_Message
	ldr	r3, .L470+72
	ldr	r4, [r4, #152]
	ldr	r0, [r3, #48]
	bl	CONTROLLER_INITIATED_ms_after_connection_to_send_alerts
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, r5
	mov	r2, r0
	mov	r0, r4
	bl	xTimerGenericCommand
.L415:
.LBB166:
.LBB167:
.LBB168:
.LBB169:
	ldr	r5, .L470+76
.LBE169:
.LBE168:
.LBB171:
.LBB172:
	add	r2, sp, #88
	str	r2, [sp, #20]
.L443:
.LBE172:
.LBE171:
.LBE167:
.LBE166:
	ldr	r3, .L470+80
	add	r1, sp, #28
	ldr	r0, [r3, #0]
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L199
	ldr	r4, [sp, #28]
	cmp	r4, #122
	beq	.L208
	bhi	.L217
	cmp	r4, #110
	beq	.L203
	bhi	.L218
	cmp	r4, #20
	beq	.L201
	bcc	.L200
	sub	r3, r4, #105
	cmp	r3, #2
	bhi	.L200
	b	.L464
.L218:
	cmp	r4, #116
	beq	.L205
	bhi	.L219
	cmp	r4, #115
	bne	.L200
	b	.L465
.L219:
	cmp	r4, #120
	beq	.L450
	cmp	r4, #121
	bne	.L200
	b	.L466
.L217:
	cmp	r4, #135
	beq	.L212
	bhi	.L220
	cmp	r4, #125
	beq	.L210
	cmp	r4, #130
	beq	.L211
	cmp	r4, #123
	bne	.L200
	b	.L467
.L220:
	cmp	r4, #140
	beq	.L214
	bhi	.L221
	cmp	r4, #136
	bne	.L200
	b	.L468
.L221:
	cmp	r4, #141
	beq	.L215
	cmp	r4, #142
	bne	.L200
	b	.L469
.L201:
	bl	RAVEON_id_start_the_process
	b	.L199
.L212:
.LBB211:
	ldr	r4, .L470+4
	ldr	r0, [r4, #116]
	cmp	r0, #0
	beq	.L199
	bl	xTimerIsTimerActive
	subs	r6, r0, #0
	bne	.L199
	ldr	r4, [r4, #116]
	bl	xTaskGetTickCount
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, r6
	mov	r2, r0
	mov	r0, r4
	b	.L455
.L468:
.LBE211:
.LBB212:
	ldr	r4, .L470+4
	ldr	r0, [r4, #124]
	cmp	r0, #0
	beq	.L199
	bl	xTimerIsTimerActive
	cmp	r0, #0
	bne	.L199
	ldr	r4, [r4, #124]
	bl	xTaskGetTickCount
	mvn	r3, #0
	mov	r1, #0
	str	r3, [sp, #0]
	mov	r2, r0
	mov	r0, r4
.L455:
	mov	r3, r1
	b	.L449
.L211:
.LBE212:
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L470+4
	ldr	r2, .L470+20
	ldr	r0, [r3, #152]
	mov	r1, #2
	mov	r3, #0
.L449:
	bl	xTimerGenericCommand
	b	.L199
.L464:
	ldr	r3, .L470+4
	ldr	r2, [r3, #0]
	cmp	r2, #3
	bne	.L222
	ldr	r3, [r3, #4]
	cmp	r3, #1
	bne	.L222
.LBB213:
	cmp	r4, #107
	bne	.L433
	ldr	r3, .L470+84
	ldr	r4, .L470+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L470+740
	ldr	r3, .L470+88
	bl	xQueueTakeMutexRecursive_debug
	mov	r2, #0
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #1
	mov	r3, r2
	ldr	r0, [r4, #32]
	bl	xTimerGenericCommand
	mov	r1, #0
	mov	r2, #24
	add	r0, r4, #8
	bl	memset
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	bne	.L225
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	beq	.L226
.L225:
	ldr	r2, [r4, #52]
	ldr	r3, .L470+4
	cmp	r2, #0
	bne	.L229
.L227:
	ldr	r2, [r3, #168]
	cmp	r2, #0
	bne	.L228
	ldr	r3, [r3, #172]
	cmp	r3, #0
	bne	.L228
	b	.L229
.L226:
	ldr	r3, [r4, #168]
	cmp	r3, #0
	beq	.L229
	ldr	r0, .L470+92
	bl	Alert_Message
.L229:
	mov	r0, #110
	bl	CONTROLLER_INITIATED_post_event
.L228:
	ldr	r4, .L470+4
	ldr	r3, [r4, #48]
	cmp	r3, #0
	movne	r0, #1
	bne	.L230
	ldr	r0, [r4, #52]
	adds	r0, r0, #0
	movne	r0, #1
.L230:
	mov	r1, #1
	bl	__clear_all_waiting_for_response_flags
	mov	r3, #0
	str	r3, [r4, #4]
	ldr	r3, [r4, #48]
	cmp	r3, #0
	bne	.L231
	bl	CONFIG_is_connected
	cmp	r0, #0
	bne	.L232
.L231:
	bl	ci_start_the_connection_process
.L232:
	ldr	r3, .L470+84
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L199
.L433:
	mov	r0, r4
	bl	Alert_comm_command_failure
	ldr	r0, .L470+96
	ldr	r1, .L470+100
	mov	r2, #49
	bl	strlcpy
	ldr	r3, .L470+104
	ldrsh	r3, [r3, #0]
	cmp	r3, #13
	bne	.L233
	bl	Refresh_Screen
.L233:
	ldr	r3, .L470+4
	cmp	r4, #105
	mov	r2, #1
	streq	r2, [r3, #48]
	strne	r2, [r3, #52]
	mov	r0, #107
	bl	CONTROLLER_INITIATED_post_event
	b	.L199
.L222:
.LBE213:
	ldr	r0, .L470+108
	b	.L452
.L203:
	ldr	r3, .L470+112
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L199
	bl	CONFIG_this_controller_originates_commserver_messages
	cmp	r0, #0
	beq	.L199
	ldr	r4, .L470+4
	ldr	r3, [r4, #0]
	cmp	r3, #3
	bne	.L450
	bl	CONFIG_is_connected
	cmp	r0, #0
	beq	.L450
	ldr	r3, [r4, #4]
	cmp	r3, #0
	bne	.L199
	ldr	r3, [r4, #8]
	cmp	r3, #0
	bne	.L237
	ldr	r2, [r4, #12]
	cmp	r2, #0
	bne	.L237
	ldr	r2, [r4, #24]
	cmp	r2, #0
	bne	.L237
	ldr	r3, [r4, #16]
	cmp	r3, #0
	beq	.L238
.L237:
	ldr	r0, .L470+116
	bl	Alert_Message
	ldr	r0, .L470+120
	mov	r1, #0
	mov	r2, #24
	bl	memset
.L238:
	ldr	r6, .L470+4
	mov	r2, #0
	ldr	r0, [r6, #176]
	add	r1, sp, #68
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L199
.LBB214:
.LBB210:
	ldr	r1, [sp, #68]
	sub	r3, r1, #400
	cmp	r3, #20
	ldrls	pc, [pc, r3, asl #2]
	b	.L239
.L261:
	.word	.L240
	.word	.L241
	.word	.L242
	.word	.L243
	.word	.L244
	.word	.L245
	.word	.L246
	.word	.L247
	.word	.L248
	.word	.L249
	.word	.L250
	.word	.L251
	.word	.L252
	.word	.L253
	.word	.L254
	.word	.L255
	.word	.L256
	.word	.L257
	.word	.L258
	.word	.L259
	.word	.L260
.L240:
.LBB174:
.LBB175:
	ldr	r3, .L470+84
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L470+740
	ldr	r3, .L470+124
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L470+4
	ldr	r4, [r3, #60]
	cmp	r4, #0
	bne	.L262
	ldr	r0, .L470+128
	add	r1, sp, #92
	ldr	r2, .L470+740
	ldr	r3, .L470+132
	bl	mem_oabia
	cmp	r0, #0
	beq	.L263
	mov	r1, r4
	ldr	r2, .L470+128
	ldr	r0, [sp, #92]
	bl	memset
	ldr	r0, [sp, #92]
	mov	r3, #6
	strh	r3, [r0], #2	@ movhi
	bl	EPSON_obtain_latest_time_and_date
	ldr	r0, [sp, #92]
	ldr	r4, .L470+136
	ldr	r3, .L470+140
	add	r0, r0, #8
	mov	r1, #48
	ldr	r2, .L470+144
	bl	snprintf
	ldr	r3, [r4, #88]
	cmp	r3, #0
	beq	.L264
.LBB176:
	add	r3, sp, #108
	str	r3, [sp, #0]
	add	r1, sp, #96
	ldr	r0, [r4, #80]
	add	r2, sp, #100
	add	r3, sp, #104
	bl	DateToDMY
	add	r1, sp, #112
	add	r2, sp, #116
	add	r3, sp, #120
	ldr	r0, [r4, #84]
	bl	TimeToHMS
	ldr	r0, [sp, #100]
	ldr	r4, [sp, #92]
	sub	r0, r0, #1
	bl	GetMonthShortStr
	ldr	r2, [sp, #96]
	add	r4, r4, #56
	str	r2, [sp, #0]
	ldr	r2, [sp, #104]
	mov	r1, #48
	str	r2, [sp, #4]
	ldr	r2, [sp, #112]
	str	r2, [sp, #8]
	ldr	r2, [sp, #116]
	str	r2, [sp, #12]
	ldr	r2, [sp, #120]
	str	r2, [sp, #16]
	ldr	r2, .L470+148
	mov	r3, r0
	mov	r0, r4
	bl	snprintf
	b	.L265
.L264:
.LBE176:
	ldr	r0, .L470+152
	bl	Alert_Message
.L265:
	ldr	r0, [sp, #92]
	mov	r2, #1104
	ldr	r1, .L470+156
	add	r0, r0, #104
	bl	memcpy
	ldr	r4, [sp, #92]
	bl	FLOWSENSE_get_controller_index
	mov	r3, #92
	ldr	r6, .L470+4
	mov	r7, #1
	mla	r0, r3, r0, r4
	add	r0, r0, #108
	bl	load_this_box_configuration_with_our_own_info
	ldr	r4, [sp, #92]
	bl	CONFIG_this_controller_is_a_configured_hub
	ldrb	r3, [r4, #112]	@ zero_extendqisi2
	bic	r3, r3, #8
	and	r0, r0, #1
	orr	r3, r3, r0, asl #3
	strb	r3, [r4, #112]
	ldr	r3, .L470+64
	mov	r4, #0
	ldrb	r0, [r3, #129]	@ zero_extendqisi2
	ldr	r3, [sp, #92]
	strb	r0, [r3, #1208]
	bl	GetNoYesStr
	mov	r1, r0
	ldr	r0, .L470+160
	bl	Alert_Message_va
	mov	r3, r4
	ldr	r0, [sp, #92]
	ldr	r1, .L470+128
	add	r2, r6, #56
	stmia	sp, {r4, r7}
	bl	set_now_xmitting_and_send_the_msg
	ldr	r3, .L470+164
	str	r7, [r6, #60]
	str	r4, [r3, #16]
	ldr	r4, .L470+800
	b	.L266
.L263:
	ldr	r0, .L470+168
	bl	Alert_Message
	ldr	r4, .L470+660
	b	.L266
.L262:
	ldr	r0, .L470+172
	bl	Alert_Message
	ldr	r4, .L470+656
.L266:
	ldr	r3, .L470+84
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L401
.L241:
.LBE175:
.LBE174:
.LBB177:
	ldr	r0, .L470+176
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
.LBB170:
	ldr	r3, .L470+180
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L470+740
	ldr	r3, .L470+184
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [r5, #32]
	ldr	r2, [r5, #16]
	ldr	r3, .L470+188
	cmp	r1, #0
	ldr	r9, [r3, r2, asl #3]
	bne	.L267
	ldr	r0, .L470+192
	bl	Alert_Message
	b	.L444
.L267:
	ldr	r4, [r5, #44]
	cmp	r4, #0
	beq	.L269
	ldr	r0, .L470+196
	bl	Alert_Message
	ldr	r4, .L470+656
	b	.L268
.L269:
	add	r3, r3, r2, asl #3
	ldr	r7, [r3, #4]
	add	r1, sp, #76
	mov	r0, r7
	ldr	r2, .L470+740
	ldr	r3, .L470+200
	bl	mem_oabia
	cmp	r0, #0
	bne	.L270
	ldr	r0, .L470+204
	bl	Alert_Message
	ldr	r4, .L470+660
	b	.L268
.L270:
	ldr	r3, [r5, #36]
	ldr	r2, [r5, #24]
	ldr	r6, [sp, #76]
	cmp	r3, r2
	str	r4, [sp, #80]
	bne	.L271
	mov	r0, r6
	ldr	r1, .L470+740
	ldr	r2, .L470+208
	bl	mem_free_debug
.L444:
	ldr	r4, .L470+828
	b	.L268
.L271:
	ldr	r4, .L470+800
	str	r3, [r5, #40]
	mov	r8, r4
	b	.L434
.L277:
	str	r2, [sp, #0]
	mov	r2, #0
	mov	r1, #100
	mov	r3, r2
	bl	nm_ALERTS_parse_alert_and_return_length
	mov	r4, #0
	mov	sl, r0
	b	.L273
.L276:
	ldr	r3, [sp, #80]
	cmp	r3, r7
	bcc	.L274
	ldr	r0, [sp, #76]
	ldr	r1, .L470+740
	ldr	r2, .L470+212
	bl	mem_free_debug
	ldr	r0, .L470+216
	bl	Alert_Message
	ldr	r4, .L470+644
	b	.L434
.L274:
	ldr	r3, [r5, #40]
	ldr	r0, .L470+76
	ldrb	r3, [r9, r3]	@ zero_extendqisi2
	add	r1, r0, #40
	strb	r3, [r6], #1
	ldr	r3, [sp, #80]
	mov	r2, #1
	add	r3, r3, #1
	str	r3, [sp, #80]
	bl	ALERTS_inc_index
	add	r4, r4, #1
.L273:
	cmp	r4, sl
	bne	.L276
	ldr	r4, .L470+800
.L434:
	ldr	r2, [r5, #40]
	ldr	r1, [r5, #24]
	rsb	lr, r8, r4
	rsbs	r3, lr, #0
	adc	r3, r3, lr
	cmp	r2, r1
	moveq	r3, #0
	cmp	r3, #0
	ldr	r0, .L470+76
	bne	.L277
	ldr	r2, .L470+800
	cmp	r4, r2
	ldrne	r4, .L470+644
	bne	.L268
	mov	r2, #1
	str	r2, [r0, #44]
	mov	ip, #4
	mov	r2, #3
	stmia	sp, {r2, ip}
	ldr	r2, .L470+220
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	bl	set_now_xmitting_and_send_the_msg
.L268:
	ldr	r3, .L470+180
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE170:
	ldr	r3, .L470+656
	ldr	r2, .L470+660
	cmp	r4, r3
	cmpne	r4, r2
	bne	.L401
	ldr	r0, .L470+224
	bl	Alert_Message
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L470+4
	mov	r1, #2
	ldr	r0, [r3, #64]
	ldr	r2, .L470+228
	b	.L460
.L242:
.LBE177:
.LBB178:
	ldr	r0, .L470+232
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
.LBB179:
	ldr	r3, .L470+236
	ldr	r6, .L470+240
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L470+740
	ldr	r3, .L470+244
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, [r6, #24]
	cmp	r4, #0
	beq	.L279
	ldr	r0, .L470+248
	bl	Alert_Message
	ldr	r4, .L470+656
	b	.L280
.L279:
	ldr	r2, [r6, #16]
	ldr	r3, [r6, #4]
	cmp	r2, r3
	bne	.L281
	ldr	r0, .L470+252
	bl	Alert_Message
	ldr	r4, .L470+828
	b	.L280
.L281:
	mov	r0, #230400
	add	r1, sp, #76
	ldr	r2, .L470+740
	ldr	r3, .L470+256
	bl	mem_oabia
	cmp	r0, #0
	bne	.L282
	ldr	r0, .L470+260
	bl	Alert_Message
	ldr	r4, .L470+660
	b	.L280
.L282:
	str	r4, [sp, #80]
	ldr	r3, [r6, #16]
	ldr	r4, .L470+800
	ldr	r7, [sp, #76]
	str	r3, [r6, #20]
	mov	r9, r4
	b	.L435
.L286:
	ldr	r3, [sp, #80]
	add	r3, r3, #60
	cmp	r3, #230400
	bls	.L284
	ldr	r0, [sp, #76]
	ldr	r1, .L470+740
	ldr	r2, .L470+264
	bl	mem_free_debug
	ldr	r0, .L470+268
	bl	Alert_Message
	ldr	r4, .L470+644
	b	.L435
.L284:
	mov	r2, #60
	add	r1, r1, #1
	mov	r0, r7
	mla	r1, r2, r1, r6
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r0, r8, #20
	add	r3, r3, #60
	str	r3, [sp, #80]
	bl	nm_STATION_HISTORY_inc_index
	add	r7, r7, #60
	mov	r4, sl
.L435:
	ldr	r1, [r6, #20]
	ldr	r2, [r6, #4]
	rsb	r0, r9, r4
	rsbs	r3, r0, #0
	adc	r3, r3, r0
	cmp	r1, r2
	moveq	r3, #0
	cmp	r3, #0
	ldr	r8, .L470+240
	ldr	sl, .L470+800
	bne	.L286
	cmp	r4, sl
	ldrne	r4, .L470+644
	bne	.L280
	mov	r2, #1
	str	r2, [r8, #24]
	mov	r1, #9
	mov	r2, #10
	stmia	sp, {r1, r2}
	ldr	r2, .L470+272
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	bl	set_now_xmitting_and_send_the_msg
.L280:
	ldr	r3, .L470+236
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE179:
	ldr	r3, .L470+656
	ldr	r2, .L470+660
	cmp	r4, r3
	cmpne	r4, r2
	bne	.L401
	ldr	r0, .L470+276
	bl	Alert_Message
	mov	r0, #900
	bl	STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds
	b	.L401
.L243:
.LBE178:
.LBB180:
	ldr	r0, .L470+280
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
.LBB181:
	ldr	r3, .L470+284
	ldr	r6, .L470+288
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L470+740
	ldr	r3, .L470+292
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, [r6, #24]
	cmp	r4, #0
	beq	.L288
	ldr	r0, .L470+296
	bl	Alert_Message
	ldr	r4, .L470+656
	b	.L289
.L288:
	ldr	r2, [r6, #16]
	ldr	r3, [r6, #4]
	cmp	r2, r3
	bne	.L290
	ldr	r0, .L470+300
	bl	Alert_Message
	ldr	r4, .L470+828
	b	.L289
.L290:
	mov	r0, #516096
	add	r1, sp, #76
	ldr	r2, .L470+740
	ldr	r3, .L470+304
	bl	mem_oabia
	cmp	r0, #0
	bne	.L291
	ldr	r0, .L470+308
	bl	Alert_Message
	ldr	r4, .L470+660
	b	.L289
.L291:
	str	r4, [sp, #80]
	ldr	r3, [r6, #16]
	ldr	r4, .L470+800
	ldr	r7, [sp, #76]
	str	r3, [r6, #20]
	mov	r9, r4
	b	.L436
.L295:
	ldr	r3, [sp, #80]
	add	r3, r3, #48
	cmp	r3, #516096
	bls	.L293
	ldr	r0, [sp, #76]
	ldr	r1, .L470+740
	ldr	r2, .L470+312
	bl	mem_free_debug
	ldr	r0, .L470+316
	bl	Alert_Message
	ldr	r4, .L470+644
	b	.L436
.L293:
	mov	r2, #48
	mla	r1, r2, r1, r6
	mov	r0, r7
	add	r1, r1, #60
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r0, r8, #20
	add	r3, r3, #48
	str	r3, [sp, #80]
	bl	nm_STATION_REPORT_DATA_inc_index
	add	r7, r7, #48
	mov	r4, sl
.L436:
	ldr	r1, [r6, #20]
	ldr	r2, [r6, #4]
	rsb	fp, r9, r4
	rsbs	r3, fp, #0
	adc	r3, r3, fp
	cmp	r1, r2
	moveq	r3, #0
	cmp	r3, #0
	ldr	r8, .L470+288
	ldr	sl, .L470+800
	bne	.L295
	cmp	r4, sl
	ldrne	r4, .L470+644
	bne	.L289
	mov	r2, #1
	str	r2, [r8, #24]
	mov	r6, #13
	mov	r2, #12
	stmia	sp, {r2, r6}
	ldr	r2, .L470+320
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	bl	set_now_xmitting_and_send_the_msg
.L289:
	ldr	r3, .L470+284
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE181:
	ldr	r3, .L470+656
	ldr	r2, .L470+660
	cmp	r4, r3
	cmpne	r4, r2
	bne	.L401
	ldr	r0, .L470+324
	bl	Alert_Message
	bl	STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	b	.L401
.L244:
.LBE180:
.LBB182:
	mov	r0, #404
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
.LBB183:
	ldr	r3, .L470+328
	ldr	r6, .L470+332
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L470+740
	ldr	r3, .L470+336
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, [r6, #24]
	cmp	r4, #0
	beq	.L297
	ldr	r0, .L470+340
	bl	Alert_Message
	ldr	r4, .L470+656
	b	.L298
.L297:
	ldr	r2, [r6, #16]
	ldr	r3, [r6, #4]
	cmp	r2, r3
	bne	.L299
	ldr	r0, .L470+344
	bl	Alert_Message
	ldr	r4, .L470+828
	b	.L298
.L299:
	ldr	r0, .L470+348
	add	r1, sp, #76
	ldr	r2, .L470+740
	ldr	r3, .L470+352
	bl	mem_oabia
	cmp	r0, #0
	bne	.L300
	ldr	r0, .L470+356
	bl	Alert_Message
	ldr	r4, .L470+660
	b	.L298
.L300:
	str	r4, [sp, #80]
	ldr	r3, [r6, #16]
	ldr	r4, .L470+800
	ldr	r7, [sp, #76]
	str	r3, [r6, #20]
	mov	r9, r4
	ldr	fp, .L470+348
	b	.L437
.L304:
	ldr	r3, [sp, #80]
	add	r3, r3, #76
	cmp	r3, fp
	bls	.L302
	ldr	r0, [sp, #76]
	ldr	r1, .L470+740
	ldr	r2, .L470+360
	bl	mem_free_debug
	ldr	r0, .L470+364
	bl	Alert_Message
	ldr	r4, .L470+644
	b	.L437
.L302:
	mov	r2, #76
	mla	r1, r2, r1, r6
	mov	r0, r7
	add	r1, r1, #60
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r0, r8, #20
	add	r3, r3, #76
	str	r3, [sp, #80]
	bl	nm_SYSTEM_REPORT_DATA_inc_index
	add	r7, r7, #76
	mov	r4, sl
.L437:
	ldr	r1, [r6, #20]
	ldr	r2, [r6, #4]
	rsb	lr, r9, r4
	rsbs	r3, lr, #0
	adc	r3, r3, lr
	cmp	r1, r2
	moveq	r3, #0
	cmp	r3, #0
	ldr	r8, .L470+332
	ldr	sl, .L470+800
	bne	.L304
	cmp	r4, sl
	ldrne	r4, .L470+644
	bne	.L298
	mov	r2, #1
	str	r2, [r8, #24]
	mov	ip, #38
	mov	r2, #37
	stmia	sp, {r2, ip}
	ldr	r2, .L470+368
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	bl	set_now_xmitting_and_send_the_msg
.L298:
	ldr	r3, .L470+328
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE183:
	ldr	r3, .L470+656
	ldr	r2, .L470+660
	cmp	r4, r3
	cmpne	r4, r2
	bne	.L401
	ldr	r0, .L470+372
	bl	Alert_Message
	bl	SYSTEM_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	b	.L401
.L245:
.LBE182:
.LBB184:
	ldr	r0, .L470+376
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
.LBB185:
	ldr	r3, .L470+420
	ldr	r6, .L470+412
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L470+740
	ldr	r3, .L470+380
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, [r6, #24]
	cmp	r4, #0
	beq	.L306
	ldr	r0, .L470+384
	bl	Alert_Message
	ldr	r4, .L470+656
	b	.L307
.L306:
	ldr	r2, [r6, #16]
	ldr	r3, [r6, #4]
	cmp	r2, r3
	bne	.L308
	ldr	r0, .L470+388
	bl	Alert_Message
	ldr	r4, .L470+828
	b	.L307
.L308:
	ldr	r0, .L470+392
	add	r1, sp, #76
	ldr	r2, .L470+740
	ldr	r3, .L470+396
	bl	mem_oabia
	cmp	r0, #0
	bne	.L309
	ldr	r0, .L470+400
	bl	Alert_Message
	ldr	r4, .L470+660
	b	.L307
.L309:
	str	r4, [sp, #80]
	ldr	r3, [r6, #16]
	ldr	r4, .L470+800
	ldr	r7, [sp, #76]
	str	r3, [r6, #20]
	mov	r9, r4
	ldr	fp, .L470+392
	b	.L438
.L313:
	ldr	r3, [sp, #80]
	add	r3, r3, #60
	cmp	r3, fp
	bls	.L311
	ldr	r0, [sp, #76]
	ldr	r1, .L470+740
	ldr	r2, .L470+404
	bl	mem_free_debug
	ldr	r0, .L470+408
	bl	Alert_Message
	ldr	r4, .L470+644
	b	.L438
.L471:
	.align	2
.L470:
	.word	Task_Table
	.word	.LANCHOR0
	.word	ci_queued_msgs_polling_timer_callback
	.word	.LC18
	.word	ci_pdata_timer_callback
	.word	6000
	.word	ci_response_timer_callback
	.word	60000
	.word	waiting_to_start_the_connection_process_timer_callback
	.word	hub_packet_activity_time_callback
	.word	connection_process_timer_callback
	.word	send_weather_data_timer_callback
	.word	3000
	.word	send_rain_indication_timer_callback
	.word	mobile_status_timer_callback
	.word	GuiVar_LiveScreensCommCentralConnected
	.word	weather_preserves
	.word	.LC19
	.word	config_c
	.word	alerts_struct_engineering
	.word	CONTROLLER_INITIATED_task_queue
	.word	ci_and_comm_mngr_activity_recursive_MUTEX
	.word	1420
	.word	.LC20
	.word	GuiVar_CommTestStatus
	.word	.LC21
	.word	GuiLib_CurStructureNdx
	.word	.LC22
	.word	in_device_exchange_hammer
	.word	.LC23
	.word	.LANCHOR0+8
	.word	1769
	.word	1212
	.word	1794
	.word	tpmicro_comm
	.word	restart_info+16
	.word	.LC24
	.word	.LC25
	.word	.LC26
	.word	chain+16
	.word	.LC27
	.word	battery_backed_general_use
	.word	.LC28
	.word	.LC29
	.word	401
	.word	alerts_pile_recursive_MUTEX
	.word	2001
	.word	alert_piles
	.word	.LC30
	.word	.LC31
	.word	2039
	.word	.LC32
	.word	2061
	.word	2084
	.word	.LC33
	.word	.LANCHOR0+68
	.word	.LC34
	.word	180000
	.word	402
	.word	station_history_completed_records_recursive_MUTEX
	.word	station_history_completed
	.word	2703
	.word	.LC35
	.word	.LC36
	.word	2737
	.word	.LC37
	.word	2764
	.word	.LC38
	.word	.LANCHOR0+88
	.word	.LC39
	.word	403
	.word	station_report_data_completed_records_recursive_MUTEX
	.word	station_report_data_completed
	.word	2884
	.word	.LC40
	.word	.LC41
	.word	2918
	.word	.LC42
	.word	2945
	.word	.LC43
	.word	.LANCHOR0+92
	.word	.LC44
	.word	system_report_completed_records_recursive_MUTEX
	.word	system_report_data_completed
	.word	3420
	.word	.LC45
	.word	.LC46
	.word	36480
	.word	3454
	.word	.LC47
	.word	3481
	.word	.LC48
	.word	.LANCHOR0+100
	.word	.LC49
	.word	405
	.word	3063
	.word	.LC50
	.word	.LC51
	.word	24000
	.word	3097
	.word	.LC52
	.word	3124
	.word	.LC53
	.word	poc_report_data_completed
	.word	.LANCHOR0+96
	.word	poc_report_completed_records_recursive_MUTEX
	.word	.LC54
	.word	406
	.word	4719
	.word	.LC55
	.word	.LC56
	.word	4753
	.word	.LC57
	.word	4780
	.word	.LC58
	.word	lights_report_data_completed
	.word	.LANCHOR0+108
	.word	lights_report_completed_records_recursive_MUTEX
	.word	.LC59
	.word	407
	.word	3237
	.word	.LC60
	.word	.LC61
	.word	3271
	.word	.LC62
	.word	229632
	.word	3299
	.word	.LC63
	.word	1196
	.word	budget_report_data_completed
	.word	.LANCHOR0+104
	.word	budget_report_completed_records_recursive_MUTEX
	.word	.LC64
	.word	3599
	.word	.LC65
	.word	.LC66
	.word	3626
	.word	.LC67
	.word	weather_tables
	.word	.LANCHOR0+164
	.word	weather_tables_recursive_MUTEX
	.word	.LC68
	.word	409
	.word	2215
	.word	.LC69
	.word	.LC70
	.word	2251
	.word	.LC71
	.word	38404
	.word	.LC72
	.word	.LANCHOR0+72
	.word	.LC73
	.word	720000
	.word	410
	.word	.LC74
	.word	.LC75
	.word	2453
	.word	.LC76
	.word	8208
	.word	2481
	.word	.LC77
	.word	1028
	.word	.LANCHOR0+80
	.word	moisture_sensor_recorder_recursive_MUTEX
	.word	1025
	.word	1027
	.word	.LC78
	.word	msrcs
	.word	1440000
	.word	411
	.word	3780
	.word	.LANCHOR0+112
	.word	.LC79
	.word	3946
	.word	.LANCHOR0+120
	.word	.LC80
	.word	413
	.word	4169
	.word	.LANCHOR1
	.word	4190
	.word	14224
	.word	system_preserves
	.word	system_preserves_recursive_MUTEX
	.word	.LC81
	.word	system_preserves+320
	.word	.LC2
	.word	4274
	.word	.LC82
	.word	.LC83
	.word	.LC84
	.word	foal_irri+36
	.word	foal_irri
	.word	list_foal_irri_recursive_MUTEX
	.word	.LANCHOR2
	.word	.LANCHOR0+128
	.word	.LC85
	.word	3000
	.word	414
	.word	.LANCHOR0+84
	.word	415
	.word	1029
	.word	60000
	.word	.LANCHOR0
	.word	.LC86
	.word	weather_preserves
	.word	.LC89
	.word	.LC87
	.word	1026
.L311:
	mov	r2, #60
	add	r1, r1, #1
	mov	r0, r7
	mla	r1, r2, r1, r6
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r0, r8, #20
	add	r3, r3, #60
	str	r3, [sp, #80]
	bl	nm_POC_REPORT_DATA_inc_index
	add	r7, r7, #60
	mov	r4, sl
.L438:
	ldr	r1, [r6, #20]
	ldr	r2, [r6, #4]
	rsb	r0, r9, r4
	rsbs	r3, r0, #0
	adc	r3, r3, r0
	cmp	r1, r2
	moveq	r3, #0
	cmp	r3, #0
	ldr	r8, .L470+412
	ldr	sl, .L470+800
	bne	.L313
	cmp	r4, sl
	ldrne	r4, .L470+644
	bne	.L307
	mov	r2, #1
	str	r2, [r8, #24]
	mov	r6, #35
	mov	r2, #34
	stmia	sp, {r2, r6}
	ldr	r2, .L470+416
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	bl	set_now_xmitting_and_send_the_msg
.L307:
	ldr	r3, .L470+420
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE185:
	ldr	r3, .L470+656
	ldr	r2, .L470+660
	cmp	r4, r3
	cmpne	r4, r2
	bne	.L401
	ldr	r0, .L470+424
	bl	Alert_Message
	bl	POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	b	.L401
.L246:
.LBE184:
.LBB186:
	ldr	r0, .L470+428
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
.LBB187:
	ldr	r3, .L470+468
	ldr	r6, .L470+460
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L470+740
	ldr	r3, .L470+432
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, [r6, #24]
	cmp	r4, #0
	beq	.L315
	ldr	r0, .L470+436
	bl	Alert_Message
	ldr	r4, .L470+656
	b	.L316
.L315:
	ldr	r2, [r6, #16]
	ldr	r3, [r6, #4]
	cmp	r2, r3
	bne	.L317
	ldr	r0, .L470+440
	bl	Alert_Message
	ldr	r4, .L470+828
	b	.L316
.L317:
	mov	r0, #10752
	add	r1, sp, #76
	ldr	r2, .L470+740
	ldr	r3, .L470+444
	bl	mem_oabia
	cmp	r0, #0
	bne	.L318
	ldr	r0, .L470+448
	bl	Alert_Message
	ldr	r4, .L470+660
	b	.L316
.L318:
	str	r4, [sp, #80]
	ldr	r3, [r6, #16]
	ldr	r4, .L470+800
	ldr	r7, [sp, #76]
	str	r3, [r6, #20]
	mov	r9, r4
	b	.L439
.L322:
	ldr	r3, [sp, #80]
	add	r3, r3, #16
	cmp	r3, #10752
	bls	.L320
	ldr	r0, [sp, #76]
	ldr	r1, .L470+740
	ldr	r2, .L470+452
	bl	mem_free_debug
	ldr	r0, .L470+456
	bl	Alert_Message
	ldr	r4, .L470+644
	b	.L439
.L320:
	add	r1, r6, r1, asl #4
	mov	r0, r7
	add	r1, r1, #60
	mov	r2, #16
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r0, r8, #20
	add	r3, r3, #16
	str	r3, [sp, #80]
	bl	nm_LIGHTS_REPORT_DATA_inc_index
	add	r7, r7, #16
	mov	r4, sl
.L439:
	ldr	r1, [r6, #20]
	ldr	r2, [r6, #4]
	rsb	lr, r9, r4
	rsbs	r3, lr, #0
	adc	r3, r3, lr
	cmp	r1, r2
	moveq	r3, #0
	cmp	r3, #0
	ldr	r8, .L470+460
	ldr	sl, .L470+800
	bne	.L322
	cmp	r4, sl
	ldrne	r4, .L470+644
	bne	.L316
	mov	r2, #1
	str	r2, [r8, #24]
	mov	ip, #116
	mov	r2, #115
	stmia	sp, {r2, ip}
	ldr	r2, .L470+464
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	bl	set_now_xmitting_and_send_the_msg
.L316:
	ldr	r3, .L470+468
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE187:
	ldr	r3, .L470+656
	ldr	r2, .L470+660
	cmp	r4, r3
	cmpne	r4, r2
	bne	.L401
	ldr	r0, .L470+472
	bl	Alert_Message
	bl	LIGHTS_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	b	.L401
.L247:
.LBE186:
.LBB188:
	ldr	r0, .L470+476
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
.LBB189:
	ldr	r3, .L470+524
	ldr	r6, .L470+516
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L470+740
	ldr	r3, .L470+480
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, [r6, #24]
	cmp	r4, #0
	beq	.L324
	ldr	r0, .L470+484
	bl	Alert_Message
	ldr	r4, .L470+656
	b	.L325
.L324:
	ldr	r2, [r6, #16]
	ldr	r3, [r6, #4]
	cmp	r2, r3
	bne	.L326
	ldr	r0, .L470+488
	bl	Alert_Message
	ldr	r4, .L470+828
	b	.L325
.L326:
	ldr	r0, .L470+500
	add	r1, sp, #76
	ldr	r2, .L470+740
	ldr	r3, .L470+492
	bl	mem_oabia
	cmp	r0, #0
	bne	.L327
	ldr	r0, .L470+496
	bl	Alert_Message
	ldr	r4, .L470+660
	b	.L325
.L327:
	str	r4, [sp, #80]
	ldr	r3, [r6, #16]
	ldr	r4, .L470+800
	ldr	r7, [sp, #76]
	str	r3, [r6, #20]
	mov	r9, r4
	ldr	fp, .L470+500
	b	.L440
.L331:
	ldr	r3, [sp, #80]
	add	r3, r3, #1184
	add	r3, r3, #12
	cmp	r3, fp
	bls	.L329
	ldr	r0, [sp, #76]
	ldr	r1, .L470+740
	ldr	r2, .L470+504
	bl	mem_free_debug
	ldr	r0, .L470+508
	bl	Alert_Message
	ldr	r4, .L470+644
	b	.L440
.L329:
	ldr	r2, .L470+512
	mov	r0, r7
	mla	r1, r2, r1, r6
	add	r7, r7, #1184
	add	r1, r1, #60
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r0, r8, #20
	add	r3, r3, #1184
	add	r3, r3, #12
	str	r3, [sp, #80]
	bl	nm_BUDGET_REPORT_DATA_inc_index
	add	r7, r7, #12
	mov	r4, sl
.L440:
	ldr	r1, [r6, #20]
	ldr	r2, [r6, #4]
	rsb	r0, r9, r4
	rsbs	r3, r0, #0
	adc	r3, r3, r0
	cmp	r1, r2
	moveq	r3, #0
	cmp	r3, #0
	ldr	r8, .L470+516
	ldr	sl, .L470+800
	bne	.L331
	cmp	r4, sl
	ldrne	r4, .L470+644
	bne	.L325
	mov	r2, #1
	str	r2, [r8, #24]
	mov	r6, #148
	mov	r2, #147
	stmia	sp, {r2, r6}
	ldr	r2, .L470+520
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	bl	set_now_xmitting_and_send_the_msg
.L325:
	ldr	r3, .L470+524
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE189:
	ldr	r3, .L470+656
	ldr	r2, .L470+660
	cmp	r4, r3
	cmpne	r4, r2
	bne	.L401
	ldr	r0, .L470+528
	bl	Alert_Message
	bl	BUDGET_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	b	.L401
.L248:
.LBE188:
.LBB190:
	mov	r0, #408
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
.LBB191:
	ldr	r3, .L470+560
	ldr	r4, .L470+552
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L470+740
	ldr	r3, .L470+532
	bl	xQueueTakeMutexRecursive_debug
	ldr	r6, [r4, #492]
	cmp	r6, #0
	beq	.L333
	ldr	r0, .L470+536
	bl	Alert_Message
	ldr	r4, .L470+656
	b	.L334
.L333:
	ldr	r0, [r4, #488]
	cmp	r0, #0
	bne	.L335
	ldr	r0, .L470+540
	bl	Alert_Message
	ldr	r4, .L470+828
	b	.L334
.L335:
	add	r0, r0, #1
	mov	r0, r0, asl #3
	add	r1, sp, #76
	ldr	r2, .L470+740
	ldr	r3, .L470+544
	bl	mem_oabia
	cmp	r0, #0
	bne	.L336
	ldr	r0, .L470+548
	bl	Alert_Message
	ldr	r4, .L470+660
	b	.L334
.L336:
	ldr	r7, [sp, #76]
	add	r1, r4, #488
	mov	r0, r7
	mov	r2, #4
	str	r6, [sp, #80]
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r0, r7, #4
	add	r3, r3, #4
	mov	r1, r4
	mov	r2, #4
	str	r3, [sp, #80]
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r7, r7, #8
	add	r3, r3, #4
	str	r3, [sp, #80]
	b	.L337
.L338:
	add	r8, r6, #1
	mov	r0, r7
	add	r1, r4, r8, asl #2
	mov	r2, #4
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r1, r6, #62
	add	r3, r3, #4
	add	r0, r7, #4
	add	r1, r4, r1, asl #2
	mov	r2, #4
	str	r3, [sp, #80]
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r7, r7, #8
	add	r3, r3, #4
	mov	r6, r8
	str	r3, [sp, #80]
.L337:
	ldr	r3, [r4, #488]
	cmp	r6, r3
	bcc	.L338
	ldr	r3, .L470+552
	mov	r2, #1
	str	r2, [r3, #492]
	mov	r4, #119
	mov	r3, #118
	stmia	sp, {r3, r4}
	ldr	r2, .L470+556
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	ldr	r4, .L470+800
	mov	r3, #0
	bl	set_now_xmitting_and_send_the_msg
.L334:
	ldr	r3, .L470+560
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE191:
	ldr	r3, .L470+656
	ldr	r2, .L470+660
	cmp	r4, r3
	cmpne	r4, r2
	bne	.L401
	ldr	r0, .L470+564
	bl	Alert_Message
	b	.L401
.L249:
.LBE190:
	ldr	r4, [sp, #72]
.LBB192:
	ldr	r0, .L470+568
	mov	r1, r4
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
.LBB193:
	ldr	r3, .L470+728
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L470+740
	ldr	r3, .L470+572
	bl	xQueueTakeMutexRecursive_debug
	ldr	r6, [r4, #492]
	cmp	r6, #0
	beq	.L340
	ldr	r0, .L470+576
	bl	Alert_Message
	ldr	r6, .L470+656
	b	.L341
.L340:
	ldr	r3, [r4, #472]
	cmp	r3, #0
	beq	.L342
	ldr	r2, [r4, #484]
	ldr	r3, [r4, #476]
	cmp	r2, r3
	bne	.L343
.L342:
	ldr	r0, .L470+580
	bl	Alert_Message
	ldr	r6, .L470+828
	b	.L341
.L343:
	ldr	r0, .L470+592
	add	r1, sp, #76
	ldr	r2, .L470+740
	ldr	r3, .L470+584
	bl	mem_oabia
	cmp	r0, #0
	bne	.L344
	ldr	r0, .L470+588
	bl	Alert_Message
	ldr	r6, .L470+660
	b	.L341
.L344:
	ldr	r7, [sp, #76]
	mov	r1, r4
	mov	r0, r7
	mov	r2, #4
	str	r6, [sp, #80]
	bl	memcpy
	ldr	r3, [sp, #80]
	ldr	r6, .L470+800
	add	r3, r3, #4
	str	r3, [sp, #80]
	ldr	r3, [r4, #484]
	add	r7, r7, #4
	str	r3, [r4, #488]
	mov	sl, r6
	ldr	r9, .L470+592
	b	.L441
.L348:
	ldr	r3, [sp, #80]
	add	r3, r3, #24
	cmp	r3, r9
	bls	.L346
	ldr	r0, [sp, #76]
	ldr	r1, .L470+740
	mov	r2, #2288
	bl	mem_free_debug
	ldr	r0, .L470+596
	bl	Alert_Message
	ldr	r6, .L470+644
	b	.L441
.L346:
	mov	r0, r7
	mov	r2, #24
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r0, r4, #488
	add	r3, r3, #24
	ldr	r1, [r4, #472]
	str	r3, [sp, #80]
	bl	nm_flow_recorder_inc_pointer
	add	r7, r7, #24
	mov	r6, r8
.L441:
	ldr	r1, [r4, #488]
	ldr	r2, [r4, #476]
	rsb	lr, sl, r6
	rsbs	r3, lr, #0
	adc	r3, r3, lr
	cmp	r1, r2
	moveq	r3, #0
	cmp	r3, #0
	ldr	r8, .L470+800
	bne	.L348
	cmp	r6, r8
	ldrne	r6, .L470+644
	bne	.L341
	mov	r3, #1
	str	r3, [r4, #492]
	mov	ip, #7
	mov	r3, #6
	stmia	sp, {r3, ip}
	ldr	r2, .L470+600
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	add	r3, r4, #472
	bl	set_now_xmitting_and_send_the_msg
.L341:
	ldr	r3, .L470+728
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE193:
	ldr	r3, .L470+656
	ldr	r2, .L470+660
	cmp	r6, r3
	cmpne	r6, r2
	bne	.L349
	ldr	r0, .L470+604
	bl	Alert_Message
	ldr	r3, [r4, #472]
	cmp	r3, #0
	beq	.L349
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	ldr	r0, [r4, #496]
	ldr	r2, .L470+608
	mov	r3, #0
	bl	xTimerGenericCommand
.L349:
	mov	r0, r6
	b	.L405
.L250:
.LBE192:
.LBB194:
	ldr	r0, .L470+612
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
.LBB195:
	ldr	r3, .L470+652
	ldr	r6, .L470+668
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L470+740
	mov	r3, #2416
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, [r6, #20]
	cmp	r4, #0
	beq	.L350
	ldr	r0, .L470+616
	bl	Alert_Message
	ldr	r4, .L470+656
	b	.L351
.L350:
	ldr	r3, [r6, #0]
	cmp	r3, #0
	beq	.L352
	ldr	r2, [r6, #12]
	ldr	r3, [r6, #4]
	cmp	r2, r3
	bne	.L353
.L352:
	ldr	r0, .L470+620
	bl	Alert_Message
	ldr	r4, .L470+828
	b	.L351
.L353:
	ldr	r0, .L470+632
	add	r1, sp, #76
	ldr	r2, .L470+740
	ldr	r3, .L470+624
	bl	mem_oabia
	cmp	r0, #0
	bne	.L354
	ldr	r0, .L470+628
	bl	Alert_Message
	ldr	r4, .L470+660
	b	.L351
.L354:
	str	r4, [sp, #80]
	ldr	r3, [r6, #12]
	ldr	r4, .L470+800
	ldr	r7, [sp, #76]
	str	r3, [r6, #16]
	mov	r9, r4
	ldr	fp, .L470+632
	b	.L442
.L358:
	ldr	r3, [sp, #80]
	add	r3, r3, #24
	cmp	r3, fp
	bls	.L356
	ldr	r0, [sp, #76]
	ldr	r1, .L470+740
	ldr	r2, .L470+636
	bl	mem_free_debug
	ldr	r0, .L470+640
	bl	Alert_Message
	ldr	r4, .L470+644
	b	.L442
.L356:
	mov	r0, r7
	mov	r2, #24
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r0, r8, #16
	add	r3, r3, #24
	ldr	r1, [r6, #0]
	str	r3, [sp, #80]
	bl	nm_MOISTURE_SENSOR_RECORDER_inc_pointer
	add	r7, r7, #24
	mov	r4, sl
.L442:
	ldr	r1, [r6, #16]
	ldr	r2, [r6, #4]
	rsb	r0, r9, r4
	rsbs	r3, r0, #0
	adc	r3, r3, r0
	cmp	r1, r2
	moveq	r3, #0
	cmp	r3, #0
	ldr	r8, .L470+668
	ldr	sl, .L470+800
	bne	.L358
	cmp	r4, sl
	ldrne	r4, .L470+644
	bne	.L351
	mov	r2, #1
	str	r2, [r8, #20]
	mov	r2, #150
	mov	r8, #151
	stmia	sp, {r2, r8}
	ldr	r2, .L470+648
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	bl	set_now_xmitting_and_send_the_msg
.L351:
	ldr	r3, .L470+652
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE195:
	ldr	r3, .L470+656
	ldr	r2, .L470+660
	cmp	r4, r3
	cmpne	r4, r2
	bne	.L401
	ldr	r0, .L470+664
	bl	Alert_Message
	ldr	r3, .L470+668
	ldr	r2, [r3, #0]
	cmp	r2, #0
	beq	.L401
	mvn	r2, #0
	str	r2, [sp, #0]
	mov	r1, #2
	ldr	r0, [r3, #24]
	ldr	r2, .L470+672
	b	.L460
.L251:
.LBE194:
.LBB196:
	ldr	r0, .L470+676
	mov	r1, #0
	bl	sending_registration_instead
	subs	r4, r0, #0
	bne	.L199
.LBB197:
	mov	r0, #9
	add	r1, sp, #76
	ldr	r2, .L470+740
	ldr	r3, .L470+680
	bl	mem_oabia
	cmp	r0, #0
	beq	.L360
	ldr	r6, [sp, #76]
	str	r4, [sp, #80]
	strb	r4, [sp, #120]
	bl	WEATHER_get_et_gage_is_in_use
	add	r4, r6, #1
	cmp	r0, #0
	ldrneb	r3, [sp, #120]	@ zero_extendqisi2
	orrne	r3, r3, #1
	strneb	r3, [sp, #120]
	bl	WEATHER_get_rain_bucket_is_in_use
	add	r1, sp, #120
	mov	r2, #1
	cmp	r0, #0
	ldrneb	r3, [sp, #120]	@ zero_extendqisi2
	mov	r0, r6
	orrne	r3, r3, #2
	strneb	r3, [sp, #120]
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r3, r3, #1
	str	r3, [sp, #80]
	ldrb	r3, [sp, #120]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L363
	add	r1, sp, #124
	mov	r0, #1
	bl	WEATHER_TABLES_get_et_table_entry_for_index
	ldrh	r3, [sp, #126]
	mov	r0, r4
	cmp	r3, #1
	moveq	r3, #4
	add	r1, sp, #124
	mov	r2, #4
	streqh	r3, [sp, #126]	@ movhi
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r4, r6, #5
	add	r3, r3, #4
	str	r3, [sp, #80]
.L363:
	ldrb	r3, [sp, #120]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L365
	add	r1, sp, #128
	mov	r0, #1
	bl	WEATHER_TABLES_get_rain_table_entry_for_index
	ldrh	r3, [sp, #130]
	mov	r0, r4
	cmp	r3, #1
	moveq	r3, #4
	add	r1, sp, #128
	mov	r2, #4
	streqh	r3, [sp, #130]	@ movhi
	bl	memcpy
	ldr	r3, [sp, #80]
	add	r3, r3, #4
	str	r3, [sp, #80]
.L365:
	mov	r3, #46
	mov	r7, #47
	stmia	sp, {r3, r7}
	ldr	r2, .L470+684
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	b	.L462
.L360:
	ldr	r0, .L470+688
	bl	Alert_Message
.LBE197:
	mov	r0, #135
	b	.L463
.L252:
.LBE196:
.LBB198:
	mov	r0, #412
	mov	r1, #0
	bl	sending_registration_instead
	subs	r4, r0, #0
	bne	.L199
.LBB199:
	mov	r0, #2
	add	r1, sp, #76
	ldr	r2, .L470+740
	ldr	r3, .L470+692
	bl	mem_oabia
	cmp	r0, #0
	beq	.L368
	ldr	r6, [sp, #76]
	add	r0, sp, #84
	str	r4, [sp, #80]
	bl	EPSON_obtain_latest_time_and_date
	mov	r0, r6
	ldr	r1, [sp, #20]
	mov	r2, #2
	bl	memcpy
	ldr	r3, [sp, #80]
	mov	r6, #58
	add	r3, r3, #2
	str	r3, [sp, #80]
	mov	r3, #57
	stmia	sp, {r3, r6}
	ldr	r2, .L470+696
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	b	.L456
.L368:
	ldr	r0, .L470+700
	bl	Alert_Message
.LBE199:
	mov	r0, #136
.L463:
	bl	CONTROLLER_INITIATED_post_event
	b	.L459
.L253:
.LBE198:
.LBB200:
	ldr	r0, .L470+704
	mov	r1, #0
	bl	sending_registration_instead
	subs	sl, r0, #0
	bne	.L199
.LBB201:
	ldr	r4, .L470+772
	mov	r0, #57
	mov	r1, r4
	ldr	r2, .L470+740
	ldr	r3, .L470+708
	bl	mem_oabia
	cmp	r0, #0
	beq	.L370
	ldr	r2, [r4, #0]
	ldr	r3, .L470+712
	str	sl, [r4, #4]
	str	r2, [r3, #0]
	add	r4, sp, #136
	mov	r3, #10
	strb	r3, [r4, #-16]!
	mov	r1, #1
	mov	r0, r4
	bl	mobile_copy_util
	ldr	r3, .L470+728
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L470+740
	ldr	r3, .L470+716
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, .L470+724
	ldr	fp, .L470+720
	mov	r6, sl
	mov	r8, sl
.L372:
	ldr	r3, [r7, #16]
	cmp	r3, #0
	beq	.L371
	cmp	sl, #0
	add	r6, r6, #1
	movne	sl, #1
	bne	.L371
	ldr	r2, .L470+724
	mla	r9, fp, r8, r2
	add	r9, r9, #532
	mov	r0, r9
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	cmp	r0, #0
	beq	.L371
	mov	sl, #1
	mov	r0, r4
	mov	r1, sl
	strb	sl, [sp, #120]
	bl	mobile_copy_util
	mov	r0, r9
	mov	r1, #16
	bl	mobile_copy_util
.L371:
	add	r8, r8, #1
	add	r7, r7, #14208
	cmp	r8, #4
	add	r7, r7, #16
	bne	.L372
	ldr	r3, .L470+728
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	sl, #0
	bne	.L373
	add	r0, sp, #136
	strb	sl, [r0, #-16]!
	mov	r1, #1
	bl	mobile_copy_util
.L373:
	cmp	r6, #0
	bne	.L374
	ldr	r0, .L470+732
	bl	Alert_Message
.L374:
	bl	WEATHER_get_et_gage_is_in_use
	cmp	r0, #0
	streqb	r0, [sp, #120]
	moveq	r1, #1
	addeq	r0, sp, #120
	beq	.L445
	mov	r4, #1
	add	r0, sp, #120
	mov	r1, r4
	strb	r4, [sp, #120]
	bl	mobile_copy_util
	ldr	r3, .L470+816
	mov	r1, #100
	ldrh	r0, [r3, #36]
	bl	__udivsi3
	mov	r1, r4
	strb	r0, [sp, #120]
	add	r0, sp, #120
.L445:
	bl	mobile_copy_util
	bl	WEATHER_get_rain_bucket_is_in_use
	cmp	r0, #0
	streqb	r0, [sp, #120]
	moveq	r1, #1
	addeq	r0, sp, #120
	beq	.L446
	mov	r4, #1
	add	r0, sp, #120
	mov	r1, r4
	strb	r4, [sp, #120]
	bl	mobile_copy_util
	ldr	r3, .L470+816
	add	r0, sp, #120
	ldr	r3, [r3, #52]
	mov	r1, r4
	strb	r3, [sp, #120]
.L446:
	bl	mobile_copy_util
	add	r0, sp, #136
	strb	r6, [r0, #-16]!
	mov	r1, #1
	bl	mobile_copy_util
	ldr	r7, .L470+736
	mov	r4, #0
	b	.L379
.L380:
	flds	s15, [r7, #0]
	add	r0, sp, #134
	mov	r1, #2
	add	r7, r7, #14208
	ftouizs	s15, s15
	add	r4, r4, #1
	add	r7, r7, #16
	fmrs	r3, s15	@ int
	strh	r3, [sp, #134]	@ movhi
	bl	mobile_copy_util
.L379:
	cmp	r4, r6
	bne	.L380
	ldr	r3, .L470+768
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L470+740
	ldr	r3, .L470+744
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #0
	ldr	r0, .L470+760
	strb	r3, [sp, #116]
	strb	r3, [sp, #112]
	bl	nm_ListGetFirst
	b	.L447
.L384:
	ldrh	r3, [r1, #72]
	ldrb	r2, [sp, #112]	@ zero_extendqisi2
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	cmp	r3, r2
	strgtb	r3, [sp, #112]
	ldrh	r3, [r1, #72]
	ldr	r0, .L470+760
	and	r3, r3, #960
	cmp	r3, #448
	ldreqb	r3, [sp, #116]	@ zero_extendqisi2
	addeq	r3, r3, #1
	streqb	r3, [sp, #116]
	bl	nm_ListGetNext
.L447:
	cmp	r0, #0
	mov	r1, r0
	bne	.L384
	ldr	r3, .L470+764
	ldr	r3, [r3, #44]
	cmp	r3, #0
	beq	.L385
	ldrb	r3, [sp, #112]	@ zero_extendqisi2
	sub	r3, r3, #1
	cmp	r3, #6
	bls	.L385
	ldr	r0, .L470+748
	bl	Alert_Message
	mov	r3, #1
	strb	r3, [sp, #112]
.L385:
	ldrb	r3, [sp, #116]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L386
	ldrb	r3, [sp, #112]	@ zero_extendqisi2
	cmp	r3, #7
	beq	.L387
	ldr	r0, .L470+752
	bl	Alert_Message
	mov	r3, #7
	strb	r3, [sp, #112]
.L387:
	ldrb	r3, [sp, #116]	@ zero_extendqisi2
	cmp	r3, #6
	bls	.L388
	ldr	r0, .L470+756
	bl	Alert_Message
	mov	r3, #6
	strb	r3, [sp, #116]
.L388:
	add	r0, sp, #116
	mov	r1, #1
	bl	mobile_copy_util
	add	r0, sp, #112
	mov	r1, #1
	bl	mobile_copy_util
	ldr	r0, .L470+760
	bl	nm_ListGetFirst
	mov	r6, #0
	mov	r4, r0
	b	.L389
.L392:
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #448
	bne	.L390
	cmp	r6, #5
	bhi	.L391
	ldrb	r3, [r4, #91]	@ zero_extendqisi2
	add	r0, sp, #120
	mov	r1, #1
	strb	r3, [sp, #120]
	bl	mobile_copy_util
	ldrb	r3, [r4, #90]	@ zero_extendqisi2
	add	r0, sp, #120
	mov	r1, #1
	strb	r3, [sp, #120]
	bl	mobile_copy_util
	ldr	r3, [r4, #48]
	add	r0, sp, #134
	mov	r1, #2
	strh	r3, [sp, #134]	@ movhi
	bl	mobile_copy_util
.L391:
	add	r6, r6, #1
.L390:
	mov	r1, r4
	ldr	r0, .L470+760
	bl	nm_ListGetNext
	mov	r4, r0
.L389:
	cmp	r4, #0
	bne	.L392
	b	.L393
.L386:
	ldr	r3, .L470+764
	add	r0, sp, #136
	ldr	r3, [r3, #44]
	mov	r1, #1
	strb	r3, [r0, #-16]!
	bl	mobile_copy_util
	ldrb	r3, [sp, #120]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L393
	add	r0, sp, #112
	mov	r1, #1
	bl	mobile_copy_util
.L393:
	ldr	r3, .L470+768
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r3, #104
	mov	lr, #105
	stmia	sp, {r3, lr}
	ldr	r3, .L470+772
	ldr	r2, .L470+776
	ldmia	r3, {r0-r1}
.L462:
	mov	r3, #0
	b	.L457
.L370:
	ldr	r0, .L470+780
	bl	Alert_Message
.LBE201:
	mov	r3, #140
	str	r3, [sp, #48]
	ldr	r3, .L470+784
	add	r0, sp, #48
	str	r3, [sp, #56]
	bl	CONTROLLER_INITIATED_post_event_with_details
	b	.L459
.L254:
.LBE200:
.LBB202:
	ldr	r0, .L470+788
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
	mov	r0, #40
	mov	r1, #41
	ldr	r2, .L470+792
	b	.L458
.L255:
.LBE202:
.LBB203:
	ldr	r0, .L470+796
	mov	r1, #0
	bl	sending_registration_instead
	subs	r7, r0, #0
	bne	.L199
	ldr	r6, .L470+808
	mov	r0, #79
	mov	r1, #80
	add	r2, r6, #140
	bl	send_ci_check_for_updates
	ldr	r3, .L470+800
	cmp	r0, r3
	mov	r4, r0
	beq	.L401
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	ldr	r0, [r6, #152]
	ldr	r2, .L470+804
	mov	r3, r7
	b	.L461
.L256:
.LBE203:
.LBB204:
.LBB205:
	ldr	r3, [r6, #144]
	ldr	r4, .L470+808
	cmp	r3, #0
	ldrne	r0, .L470+812
	bne	.L448
	ldr	r3, .L470+816
	ldrb	r6, [r3, #128]	@ zero_extendqisi2
	cmp	r6, #0
	ldrne	r0, .L470+820
	bne	.L448
	add	r0, sp, #76
	mov	r1, #19
	mov	r2, r6
	bl	PDATA_build_data_to_send
	cmp	r0, #512
	bne	.L399
	ldr	r0, .L470+824
	bl	Alert_Message
	ldr	r4, .L470+828
	b	.L397
.L399:
	cmp	r0, #768
	bne	.L400
	mov	r3, #68
	mov	ip, #69
	stmia	sp, {r3, ip}
	add	r2, r4, #148
	mov	r3, r6
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	bl	set_now_xmitting_and_send_the_msg
	mov	r3, #1
	str	r3, [r4, #144]
	ldr	r4, .L472
	b	.L397
.L400:
	cmp	r0, #256
	ldreq	r4, .L472+4
	beq	.L397
	ldr	r0, .L472+8
	bl	Alert_Message
	ldr	r4, .L472+12
	b	.L397
.L448:
	bl	Alert_Message
	ldr	r4, .L472+16
.L397:
.LBE205:
	ldr	r3, .L472+4
	ldr	r2, .L472+16
	cmp	r4, r3
	cmpne	r4, r2
	bne	.L401
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L472+20
	ldr	r2, .L472+24
	ldr	r0, [r3, #152]
	mov	r1, #2
.L460:
	mov	r3, #0
.L461:
	bl	xTimerGenericCommand
.L401:
	mov	r0, r4
	b	.L405
.L257:
.LBE204:
.LBB206:
	ldr	r0, .L472+28
	mov	r1, #0
	bl	sending_registration_instead
	cmp	r0, #0
	bne	.L199
	ldr	r2, .L472+32
	mov	r0, #83
	mov	r1, #84
.L458:
	bl	send_ci_check_for_updates
	b	.L405
.L258:
.LBE206:
.LBB207:
.LBB208:
	mov	r0, #1
	add	r1, sp, #76
	ldr	r2, .L472+36
	ldr	r3, .L472+40
	bl	mem_oabia
	cmp	r0, #0
	beq	.L402
	add	r1, sp, #136
	mov	r4, #0
	strb	r4, [r1, #-16]!
	mov	r2, #1
	ldr	r0, [sp, #76]
	str	r4, [sp, #80]
	bl	memcpy
	ldr	r3, [sp, #80]
	mov	r0, #71
	add	r3, r3, #1
	str	r3, [sp, #80]
	mov	r3, #72
	stmia	sp, {r0, r3}
	ldr	r2, .L472+44
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
	b	.L456
.L402:
	ldr	r0, .L472+48
	bl	Alert_Message
	b	.L459
.L259:
.LBE208:
.LBE207:
	mov	r0, #1
	bl	CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg
	b	.L199
.L260:
.LBB209:
	mov	r0, #420
	mov	r1, #0
	bl	sending_registration_instead
	subs	r4, r0, #0
	bne	.L199
.LBB173:
	mov	r0, #2
	add	r1, sp, #76
	ldr	r2, .L472+36
	ldr	r3, .L472+52
	bl	mem_oabia
	subs	r6, r0, #0
	beq	.L404
	ldr	r6, [sp, #76]
	add	r0, sp, #84
	str	r4, [sp, #80]
	bl	EPSON_obtain_latest_time_and_date
	ldr	r1, [sp, #20]
	mov	r2, #2
	mov	r0, r6
	bl	memcpy
	ldr	r3, [sp, #80]
	mov	r1, #166
	add	r3, r3, #2
	str	r3, [sp, #80]
	mov	r3, #167
	stmia	sp, {r1, r3}
	ldr	r2, .L472+56
	add	r1, sp, #76
	ldmia	r1, {r0-r1}
.L456:
	mov	r3, r4
.L457:
	bl	set_now_xmitting_and_send_the_msg
	ldr	r0, .L472
	b	.L405
.L404:
	ldr	r0, .L472+60
	bl	Alert_Message
.LBE173:
	mov	r0, #420
	mov	r1, r6
	mov	r2, #256
	mov	r3, #1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
.L459:
	ldr	r0, .L472+4
.L405:
	bl	check_if_we_need_to_post_a_build_and_send_event
	b	.L199
.L239:
.LBE209:
	ldr	r0, .L472+64
	bl	Alert_Message_va
	b	.L199
.L215:
.LBE210:
.LBE214:
	ldr	r3, .L472+20
	ldr	r2, [r3, #136]
	cmp	r2, #300
	addcc	r2, r2, #1
	bcc	.L451
	b	.L199
.L469:
	ldr	r3, .L472+20
	mov	r2, #0
.L451:
	str	r2, [r3, #136]
	b	.L199
.L214:
	ldr	r0, [sp, #36]
	bl	restart_mobile_status_timer
	b	.L199
.L210:
	bl	restart_hub_packet_activity_timer
	b	.L199
.L466:
	ldr	r3, .L472+20
	ldr	r3, [r3, #0]
	cmp	r3, #2
	bne	.L406
.LBB215:
	ldr	r3, .L472+68
	ldr	r2, .L472+72
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #40]
	cmp	r3, #0
	movne	r0, #121
	bne	.L454
	ldr	r3, .L472+76
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L199
	ldr	r0, .L472+80
	mov	r1, #2
	bl	Alert_Message_va
	b	.L450
.L406:
.LBE215:
	cmp	r3, #0
	ldrne	r0, .L472+84
	bne	.L452
	b	.L453
.L208:
	ldr	r6, .L472+20
	ldr	r3, [r6, #0]
	cmp	r3, #2
	bne	.L409
.LBB216:
	mvn	r3, #0
	mov	r2, #0
	str	r3, [sp, #0]
	mov	r1, #1
	mov	r3, r2
	ldr	r0, [r6, #40]
	bl	xTimerGenericCommand
	ldr	r3, [r6, #0]
	cmp	r3, #2
	bne	.L410
	ldr	r3, .L472+68
	ldr	r2, .L472+72
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #40]
	cmp	r3, #0
	beq	.L410
	mov	r0, r4
.L454:
	blx	r3
	b	.L199
.L410:
	ldr	r3, .L472+76
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L199
	ldr	r0, .L472+88
	bl	Alert_Message
	b	.L450
.L409:
.LBE216:
	cmp	r3, #0
	ldrne	r0, .L472+92
	bne	.L452
.L453:
	add	r0, sp, #28
	bl	RAVEON_id_event_processing
	b	.L199
.L467:
	mov	r0, r4
	bl	Alert_comm_command_failure
	ldr	r0, .L472+96
	ldr	r1, .L472+100
	mov	r2, #49
	bl	strlcpy
	ldr	r3, .L472+104
	ldrsh	r3, [r3, #0]
	cmp	r3, #13
	bne	.L412
	bl	Refresh_Screen
.L412:
	ldr	r3, .L472+20
	mov	r2, #1
	ldr	r0, [r3, #44]
	str	r2, [r3, #0]
	add	r0, r0, r2
	cmp	r0, #3
	str	r0, [r3, #44]
	movls	r0, #120
	bls	.L413
	cmp	r0, #5
	movls	r0, #600
	bls	.L413
	cmp	r0, #7
	movhi	r0, #14400
	movls	r0, #3600
.L413:
	bl	ci_start_the_waiting_to_start_the_connection_process_timer_seconds
	b	.L199
.L465:
	bl	CONFIG_this_controller_originates_commserver_messages
	cmp	r0, #0
	beq	.L199
	ldr	r3, .L472+68
	ldr	r3, [r3, #80]
	cmp	r3, #1
	beq	.L199
	ldr	r3, .L472+20
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L450
	ldr	r0, .L472+108
	bl	Alert_Message
	ldr	r3, .L472+112
	mov	r2, #0
	str	r2, [r3, #0]
	bl	CONTROLLER_INITIATED_force_end_of_transaction
	b	.L450
.L205:
	bl	CONFIG_this_controller_originates_commserver_messages
	cmp	r0, #0
	beq	.L199
	ldr	r3, .L472+68
	ldr	r3, [r3, #80]
	cmp	r3, #1
	beq	.L199
	ldr	r3, .L472+20
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L199
	ldr	r0, .L472+116
	bl	Alert_Message
	ldr	r3, .L472+112
	mov	r2, #1
	str	r2, [r3, #0]
.L450:
	bl	ci_start_the_connection_process
	b	.L199
.L200:
	ldr	r0, .L472+120
.L452:
	bl	Alert_Message
.L199:
	bl	xTaskGetTickCount
	ldr	r3, .L472+124
	ldr	r2, [sp, #24]
	str	r0, [r3, r2, asl #2]
	b	.L443
.L473:
	.align	2
.L472:
	.word	1029
	.word	1027
	.word	.LC88
	.word	1028
	.word	1025
	.word	.LANCHOR0
	.word	60000
	.word	417
	.word	.LANCHOR0+156
	.word	.LC2
	.word	4640
	.word	.LANCHOR0+160
	.word	.LC90
	.word	4982
	.word	.LANCHOR0+172
	.word	.LC91
	.word	.LC92
	.word	config_c
	.word	port_device_table
	.word	in_device_exchange_hammer
	.word	.LC93
	.word	.LC94
	.word	.LC95
	.word	.LC96
	.word	GuiVar_CommTestStatus
	.word	.LC97
	.word	GuiLib_CurStructureNdx
	.word	.LC98
	.word	GuiVar_LiveScreensCommCentralConnected
	.word	.LC99
	.word	.LC100
	.word	task_last_execution_stamp
.LFE74:
	.size	CONTROLLER_INITIATED_task, .-CONTROLLER_INITIATED_task
	.section	.text.CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities,"ax",%progbits
	.align	2
	.global	CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities
	.type	CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities, %function
CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L477
	mov	r3, #0
	mov	r1, #3
	stmfd	sp!, {r4, r5, lr}
.LCFI30:
	str	r3, [r2, #44]
	stmia	r2, {r1, r3}
	ldr	r2, .L477+4
	mov	r4, #1
	str	r3, [r2, #264]
	ldr	r3, .L477+8
	str	r4, [r3, #0]
	bl	CONFIG_this_controller_is_behind_a_hub
	subs	r5, r0, #0
	ldmnefd	sp!, {r4, r5, pc}
	ldr	r3, .L477+12
	ldr	r0, [r3, #48]
	bl	CONTROLLER_INITIATED_ms_after_connection_to_send_alerts
	mov	r1, r4
	bl	CONTROLLER_INITIATED_alerts_timer_upkeep
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	beq	.L476
	bl	restart_hub_packet_activity_timer
	ldr	r0, .L477+16
	mov	r1, r5
	mov	r2, #256
	mov	r3, r4
	bl	CONTROLLER_INITIATED_post_to_messages_queue
.L476:
	mov	r0, #3
	ldmfd	sp!, {r4, r5, lr}
	b	start_the_polling_timer
.L478:
	.align	2
.L477:
	.word	.LANCHOR0
	.word	cdcs
	.word	GuiVar_LiveScreensCommCentralConnected
	.word	config_c
	.word	419
.LFE4:
	.size	CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities, .-CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities
	.global	last_message_sent_dt_stamp
	.global	cics
	.section	.bss.mobile_ucp,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	mobile_ucp, %object
	.size	mobile_ucp, 4
mobile_ucp:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Port A: NULL device connection function(s)\000"
.LC1:
	.ascii	"CI : trying send a 0 byte msg\000"
.LC2:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/controller_initiated.c\000"
.LC3:
	.ascii	"No Memory to send init packet\000"
.LC4:
	.ascii	"No Memory to send data packet\000"
.LC5:
	.ascii	"CI: unexpected device index\000"
.LC6:
	.ascii	"No memory to check for updates\000"
.LC7:
	.ascii	"Trying to check for updates with invalid date and t"
	.ascii	"ime\000"
.LC8:
	.ascii	"CI MSG QUEUE: unk behavior %u\000"
.LC9:
	.ascii	"CI msgs to send queue overflow!\000"
.LC10:
	.ascii	"CI queue overflow!\000"
.LC11:
	.ascii	"msg spanned midnight\000"
.LC12:
	.ascii	"flow: pending_first not in use!\000"
.LC13:
	.ascii	"CICS: current_msg ptr is NULL!\000"
.LC14:
	.ascii	"mois: pending_first not in use!\000"
.LC15:
	.ascii	"CI: forcing end of message exchange\000"
.LC16:
	.ascii	"No memory to send No More Msgs msg\000"
.LC17:
	.ascii	"Hub Packet Activity timer EXPIRED\000"
.LC18:
	.ascii	"\000"
.LC19:
	.ascii	"Pending PData changes to be sent.\000"
.LC20:
	.ascii	"CI: why did non-hub controller send this msg?\000"
.LC21:
	.ascii	"Communication failed: No response\000"
.LC22:
	.ascii	"CI: waiting events while not ready & waiting\000"
.LC23:
	.ascii	"CI: During IDLE now_xmitting is non-zero\000"
.LC24:
	.ascii	"%s\000"
.LC25:
	.ascii	"%s %2d %4d @ %02d:%02d:%02d\000"
.LC26:
	.ascii	"TP Micro date/time not valid yet\000"
.LC27:
	.ascii	"Cloud PDATA to be deleted: %s\000"
.LC28:
	.ascii	"No memory for registration msg?\000"
.LC29:
	.ascii	"Registration already being sent?\000"
.LC30:
	.ascii	"Alerts pile not ready.\000"
.LC31:
	.ascii	"Attempt to send alerts while in process.\000"
.LC32:
	.ascii	"No Memory to send alerts\000"
.LC33:
	.ascii	"CI: alerts to commserver - too many!\000"
.LC34:
	.ascii	"CI busy - alerts timer restarted\000"
.LC35:
	.ascii	"Station History: message in process.\000"
.LC36:
	.ascii	"CI: no station history to send\000"
.LC37:
	.ascii	"No Memory to send station history\000"
.LC38:
	.ascii	"CI: station history to commserver - too many!\000"
.LC39:
	.ascii	"station history timer restarted\000"
.LC40:
	.ascii	"Station Report Data: message in process.\000"
.LC41:
	.ascii	"CI: no station report data to send\000"
.LC42:
	.ascii	"No Memory to send station report data\000"
.LC43:
	.ascii	"CI: station report data to commserver - too many!\000"
.LC44:
	.ascii	"station report data timer restarted\000"
.LC45:
	.ascii	"SYSTEM Report Data: message in process.\000"
.LC46:
	.ascii	"CI: no system report data to send\000"
.LC47:
	.ascii	"No Memory to send system report data\000"
.LC48:
	.ascii	"CI: system_report_data to commserver - too many!\000"
.LC49:
	.ascii	"system report data timer restarted\000"
.LC50:
	.ascii	"POC Report Data: message in process.\000"
.LC51:
	.ascii	"CI: no poc report data to send\000"
.LC52:
	.ascii	"No Memory to send poc report data\000"
.LC53:
	.ascii	"CI: poc_report_data to commserver - too many!\000"
.LC54:
	.ascii	"poc report data timer restarted\000"
.LC55:
	.ascii	"LIGHTS Report Data: message in process.\000"
.LC56:
	.ascii	"CI: no lights report data to send\000"
.LC57:
	.ascii	"No Memory to send lights report data\000"
.LC58:
	.ascii	"CI: lights_report_data to commserver - too many!\000"
.LC59:
	.ascii	"lights report data timer restarted\000"
.LC60:
	.ascii	"Budget Report Data: message in process.\000"
.LC61:
	.ascii	"CI: no budget report data to send\000"
.LC62:
	.ascii	"No Memory to send budget report data\000"
.LC63:
	.ascii	"CI: budget_report_data to commserver - too many!\000"
.LC64:
	.ascii	"budget report data timer restarted\000"
.LC65:
	.ascii	"ET/RAIN Tables: message in process.\000"
.LC66:
	.ascii	"CI: no et/rain tables entries to send\000"
.LC67:
	.ascii	"No Memory to send et/rain table\000"
.LC68:
	.ascii	"couldn't send et/rain table entries\000"
.LC69:
	.ascii	"Flow Recording: message in process.\000"
.LC70:
	.ascii	"CI: no flow recording to send\000"
.LC71:
	.ascii	"No Memory to send flow_recording\000"
.LC72:
	.ascii	"CI: flow recording to commserver - too many!\000"
.LC73:
	.ascii	"CI busy - flow recording timer restarted\000"
.LC74:
	.ascii	"Moisture Recording: message in process.\000"
.LC75:
	.ascii	"CI: no moisture recording to send\000"
.LC76:
	.ascii	"No Memory to send moisture recording\000"
.LC77:
	.ascii	"CI: moisture recording to commserver - too many!\000"
.LC78:
	.ascii	"CI busy - moisture recorder timer restarted\000"
.LC79:
	.ascii	"No memory to send weather data\000"
.LC80:
	.ascii	"No memory to send rain indication\000"
.LC81:
	.ascii	"CI: no systems!\000"
.LC82:
	.ascii	"CI: reason out of range!\000"
.LC83:
	.ascii	"CI: not for mobile!\000"
.LC84:
	.ascii	"CI: too many mobile ON!\000"
.LC85:
	.ascii	"No memory to send mobile status\000"
.LC86:
	.ascii	"PDATA: already a msg\000"
.LC87:
	.ascii	"PDATA: no data!\000"
.LC88:
	.ascii	"PDATA unexp result\000"
.LC89:
	.ascii	"PDATA to cloud blocked - unit swap underway\000"
.LC90:
	.ascii	"No memory to send program request\000"
.LC91:
	.ascii	"No memory to send Hub Is Busy msg\000"
.LC92:
	.ascii	"CI: unhandled message to send %u\000"
.LC93:
	.ascii	"Connection Sequence ERROR mode=%u\000"
.LC94:
	.ascii	"CI: timer fired unexpectedly\000"
.LC95:
	.ascii	"Connection Sequence ERROR\000"
.LC96:
	.ascii	"CI: string found unexpectedly\000"
.LC97:
	.ascii	"Communication failed: Unable to Connect\000"
.LC98:
	.ascii	"Port A: Disconnected\000"
.LC99:
	.ascii	"Port A: Connected?\000"
.LC100:
	.ascii	"CI : unk event\000"
	.section	.bss.mobile_handle,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	mobile_handle, %object
	.size	mobile_handle, 8
mobile_handle:
	.space	8
	.section	.bss.last_message_sent_dt_stamp,"aw",%nobits
	.set	.LANCHOR3,. + 0
	.type	last_message_sent_dt_stamp, %object
	.size	last_message_sent_dt_stamp, 6
last_message_sent_dt_stamp:
	.space	6
	.section	.bss.cics,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	cics, %object
	.size	cics, 188
cics:
	.space	188
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.byte	0x4
	.4byte	.LCFI0-.LFB54
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI2-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.byte	0x4
	.4byte	.LCFI4-.LFB56
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI5-.LFB7
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI6-.LFB18
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x74
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI8-.LFB32
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI10-.LFB21
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI11-.LFB25
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.byte	0x4
	.4byte	.LCFI12-.LFB72
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.byte	0x4
	.4byte	.LCFI13-.LFB55
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.byte	0x4
	.4byte	.LCFI14-.LFB46
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.byte	0x4
	.4byte	.LCFI15-.LFB75
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.byte	0x4
	.4byte	.LCFI16-.LFB76
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI17-.LFB11
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI19-.LFB17
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI20-.LFB19
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI21-.LFB20
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.byte	0x4
	.4byte	.LCFI22-.LFB69
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI24-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI25-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB77
	.4byte	.LFE77-.LFB77
	.byte	0x4
	.4byte	.LCFI26-.LFB77
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.byte	0x4
	.4byte	.LCFI27-.LFB78
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.byte	0x4
	.4byte	.LCFI28-.LFB74
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI29-.LCFI28
	.byte	0xe
	.uleb128 0xac
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI30-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE62:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/controller_initiated.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x47c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF79
	.byte	0x1
	.4byte	.LASF80
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0xa18
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x148
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0xfb0
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0xf3e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0xe8d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x654
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x117e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1211
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x668
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1368
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x1311
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.2byte	0xf5d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x772
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x85c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.2byte	0xaff
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x1
	.2byte	0xbb4
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x1
	.2byte	0xdcc
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x1
	.2byte	0xc67
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x12e3
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x1
	.2byte	0xd19
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF20
	.byte	0x1
	.2byte	0xe6b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x922
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x9e3
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF23
	.byte	0x1
	.2byte	0xf22
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x1139
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF25
	.byte	0x1
	.2byte	0xa63
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x1164
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x11f6
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x1240
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF29
	.byte	0x1
	.byte	0xa6
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF30
	.byte	0x1
	.byte	0xbd
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF31
	.byte	0x1
	.2byte	0xea2
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF32
	.byte	0x1
	.2byte	0xf4e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x1a0
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x1c0
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x342
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x3ed
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF37
	.byte	0x1
	.2byte	0xfa4
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF38
	.byte	0x1
	.byte	0x91
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x137
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST2
	.uleb128 0x5
	.4byte	.LASF40
	.byte	0x1
	.byte	0xb5
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x102d
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LLST4
	.uleb128 0x6
	.4byte	0x2a
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST5
	.uleb128 0x2
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x4c9
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x62c
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST6
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST7
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x6ce
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST8
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x792
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST9
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x13ae
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LLST10
	.uleb128 0x6
	.4byte	0x33
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LLST11
	.uleb128 0x8
	.4byte	0x3c
	.4byte	.LFB50
	.4byte	.LFE50
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	0x45
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LLST12
	.uleb128 0x9
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x115b
	.4byte	.LFB59
	.4byte	.LFE59
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x78a
	.4byte	.LFB24
	.4byte	.LFE24
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x16b5
	.4byte	.LFB75
	.4byte	.LFE75
	.4byte	.LLST13
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x16c6
	.4byte	.LFB76
	.4byte	.LFE76
	.4byte	.LLST14
	.uleb128 0x4
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x1e5
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST15
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x615
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST16
	.uleb128 0x6
	.4byte	0x4e
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST17
	.uleb128 0x6
	.4byte	0x69
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST18
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x133e
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LLST19
	.uleb128 0x9
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x197
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	0x126
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST20
	.uleb128 0x9
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x13f
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x4bf
	.4byte	.LFB14
	.4byte	.LFE14
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	0x12e
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST21
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x16d8
	.4byte	.LFB77
	.4byte	.LFE77
	.4byte	.LLST22
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF58
	.byte	0x1
	.2byte	0x16f3
	.4byte	.LFB78
	.4byte	.LFE78
	.4byte	.LLST23
	.uleb128 0x2
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x585
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF60
	.byte	0x1
	.2byte	0x1425
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF61
	.byte	0x1
	.2byte	0x6df
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x7b3
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF63
	.byte	0x1
	.2byte	0xa7c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF64
	.byte	0x1
	.2byte	0xb31
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF65
	.byte	0x1
	.2byte	0xd49
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF66
	.byte	0x1
	.2byte	0xbe4
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF67
	.byte	0x1
	.2byte	0x125c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF68
	.byte	0x1
	.2byte	0xc97
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF69
	.byte	0x1
	.2byte	0xdfa
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF70
	.byte	0x1
	.2byte	0x891
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF71
	.byte	0x1
	.2byte	0x95a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF72
	.byte	0x1
	.2byte	0xeb1
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF73
	.byte	0x1
	.2byte	0xf89
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF74
	.byte	0x1
	.2byte	0x1036
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF75
	.byte	0x1
	.2byte	0x11db
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF76
	.byte	0x1
	.2byte	0x1395
	.byte	0x1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF77
	.byte	0x1
	.2byte	0x14c6
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LLST24
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF78
	.byte	0x1
	.byte	0xd8
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST25
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB54
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE54
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB5
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB56
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE56
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB7
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB18
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI7
	.4byte	.LFE18
	.2byte	0x3
	.byte	0x7d
	.sleb128 116
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB32
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI9
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB21
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB25
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB72
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE72
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB55
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE55
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB46
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE46
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB75
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE75
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB76
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE76
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB11
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI18
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB17
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB69
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI23
	.4byte	.LFE69
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB1
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB3
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB77
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LFE77
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB78
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LFE78
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB74
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI29
	.4byte	.LFE74
	.2byte	0x3
	.byte	0x7d
	.sleb128 172
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB4
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x114
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB77
	.4byte	.LFE77-.LFB77
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB75
	.4byte	.LFE75
	.4byte	.LFB76
	.4byte	.LFE76
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB77
	.4byte	.LFE77
	.4byte	.LFB78
	.4byte	.LFE78
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF28:
	.ascii	"send_program_data_request\000"
.LASF24:
	.ascii	"send_mobile_status\000"
.LASF25:
	.ascii	"send_check_for_updates\000"
.LASF47:
	.ascii	"ci_pdata_timer_callback\000"
.LASF31:
	.ascii	"start_send_weather_data_timer_if_it_is_not_running\000"
.LASF35:
	.ascii	"nm_build_init_packet_and_send_it\000"
.LASF5:
	.ascii	"check_if_we_need_to_post_a_build_and_send_event\000"
.LASF64:
	.ascii	"send_ci_station_report_data\000"
.LASF40:
	.ascii	"start_the_polling_timer\000"
.LASF62:
	.ascii	"send_ci_engineering_alerts\000"
.LASF34:
	.ascii	"process_during_connection_process_string_found\000"
.LASF73:
	.ascii	"send_rain_indication\000"
.LASF20:
	.ascii	"send_et_rain_tables\000"
.LASF63:
	.ascii	"send_ci_station_history\000"
.LASF43:
	.ascii	"set_now_xmitting_and_send_the_msg\000"
.LASF1:
	.ascii	"ci_start_the_connection_process\000"
.LASF21:
	.ascii	"send_flow_recording\000"
.LASF77:
	.ascii	"CONTROLLER_INITIATED_task\000"
.LASF0:
	.ascii	"send_ci_check_for_updates\000"
.LASF12:
	.ascii	"send_registration_packet\000"
.LASF54:
	.ascii	"connection_process_timer_callback\000"
.LASF13:
	.ascii	"send_engineering_alerts\000"
.LASF41:
	.ascii	"mobile_copy_util\000"
.LASF8:
	.ascii	"sending_registration_instead\000"
.LASF57:
	.ascii	"CONTROLLER_INITIATED_ms_after_midnight_to_send_this"
	.ascii	"_controllers_daily_records\000"
.LASF7:
	.ascii	"send_program_data_request_primitive\000"
.LASF38:
	.ascii	"restart_hub_packet_activity_timer\000"
.LASF52:
	.ascii	"CONTROLLER_INITIATED_force_end_of_transaction\000"
.LASF68:
	.ascii	"send_ci_budget_report_data\000"
.LASF44:
	.ascii	"CONTROLLER_INITIATED_update_comm_server_registratio"
	.ascii	"n_info\000"
.LASF9:
	.ascii	"build_and_send_the_hub_is_busy_msg\000"
.LASF32:
	.ascii	"start_send_rain_indication_timer_if_it_is_not_runni"
	.ascii	"ng\000"
.LASF65:
	.ascii	"send_ci_system_report_data\000"
.LASF50:
	.ascii	"CONTROLLER_INITIATED_post_event\000"
.LASF74:
	.ascii	"send_ci_mobile_status\000"
.LASF26:
	.ascii	"send_verify_firmware_version_before_sending_program"
	.ascii	"_data\000"
.LASF16:
	.ascii	"send_system_report_data\000"
.LASF69:
	.ascii	"send_ci_et_rain_tables\000"
.LASF76:
	.ascii	"attempt_to_build_and_send_the_hub_is_busy_msg\000"
.LASF15:
	.ascii	"send_station_report_data\000"
.LASF75:
	.ascii	"build_and_send_program_data\000"
.LASF2:
	.ascii	"mobile_status_timer_callback\000"
.LASF49:
	.ascii	"CONTROLLER_INITIATED_post_event_with_details\000"
.LASF55:
	.ascii	"waiting_to_start_the_connection_process_timer_callb"
	.ascii	"ack\000"
.LASF4:
	.ascii	"send_weather_data_timer_callback\000"
.LASF42:
	.ascii	"ci_send_the_just_built_message\000"
.LASF17:
	.ascii	"send_poc_report_data\000"
.LASF39:
	.ascii	"ci_start_the_waiting_to_start_the_connection_proces"
	.ascii	"s_timer_seconds\000"
.LASF48:
	.ascii	"ci_alerts_timer_callback\000"
.LASF58:
	.ascii	"CONTROLLER_INITIATED_ms_after_connection_to_send_al"
	.ascii	"erts\000"
.LASF80:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/controller_initiated.c\000"
.LASF18:
	.ascii	"send_lights_report_data\000"
.LASF29:
	.ascii	"hub_packet_activity_time_callback\000"
.LASF33:
	.ascii	"process_during_connection_process_timer_fired\000"
.LASF30:
	.ascii	"ci_queued_msgs_polling_timer_callback\000"
.LASF46:
	.ascii	"CONTROLLER_INITIATED_post_to_messages_queue\000"
.LASF14:
	.ascii	"send_station_history\000"
.LASF51:
	.ascii	"__clear_all_waiting_for_response_flags\000"
.LASF11:
	.ascii	"send_ci_rain_indication\000"
.LASF3:
	.ascii	"send_rain_indication_timer_callback\000"
.LASF6:
	.ascii	"build_and_send_program_data_primitive\000"
.LASF36:
	.ascii	"nm_build_data_packets_and_send_them\000"
.LASF71:
	.ascii	"send_ci_moisture_sensor_recorder_records\000"
.LASF72:
	.ascii	"send_ci_weather_data\000"
.LASF78:
	.ascii	"CONTROLLER_INITIATED_after_connecting_perform_msg_h"
	.ascii	"ousekeeping_activities\000"
.LASF67:
	.ascii	"send_ci_lights_report_data\000"
.LASF23:
	.ascii	"send_weather_data\000"
.LASF59:
	.ascii	"ci_waiting_for_response_state_processing\000"
.LASF56:
	.ascii	"ci_response_timer_callback\000"
.LASF27:
	.ascii	"send_verify_firmware_version_before_asking_for_prog"
	.ascii	"ram_data\000"
.LASF79:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF19:
	.ascii	"send_budget_report_data\000"
.LASF37:
	.ascii	"restart_mobile_status_timer\000"
.LASF66:
	.ascii	"send_ci_poc_report_data\000"
.LASF60:
	.ascii	"build_and_send_this_queued_up_msg\000"
.LASF61:
	.ascii	"send_ci_registration_data\000"
.LASF45:
	.ascii	"CONTROLLER_INITIATED_alerts_timer_upkeep\000"
.LASF70:
	.ascii	"send_ci_flow_recording\000"
.LASF22:
	.ascii	"send_moisture_sensor_recording\000"
.LASF10:
	.ascii	"build_and_send_the_no_more_messages_msg\000"
.LASF53:
	.ascii	"CONTROLLER_INITIATED_attempt_to_build_and_send_the_"
	.ascii	"no_more_messages_msg\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
