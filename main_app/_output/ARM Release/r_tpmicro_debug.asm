	.file	"r_tpmicro_debug.c"
	.text
.Ltext0:
	.section	.text.FDTO_TPMICRO_draw_debug,"ax",%progbits
	.align	2
	.global	FDTO_TPMICRO_draw_debug
	.type	FDTO_TPMICRO_draw_debug, %function
FDTO_TPMICRO_draw_debug:
.LFB0:
	@ args = 0, pretend = 0, frame = 68
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
	mov	r2, r0
	sub	sp, sp, #112
.LCFI1:
	bne	.L2
	mov	r0, #104
	mvn	r1, #0
	bl	GuiLib_ShowScreen
.L2:
	ldr	r8, .L5
	mov	r7, #27
	mov	r9, #1
	mov	r6, #0
	mov	ip, #240
.L3:
	mov	r5, #1
	mov	r1, r7
	mov	sl, #15
	mov	r0, #62
	mov	r2, r5
	mvn	r3, #0
	add	r7, r7, #12
	str	r8, [sp, #0]
	str	ip, [sp, #20]
	str	ip, [sp, #44]
	str	r9, [sp, #4]
	str	r9, [sp, #8]
	str	r6, [sp, #12]
	str	r6, [sp, #16]
	str	r6, [sp, #24]
	str	r6, [sp, #28]
	str	r6, [sp, #32]
	str	r6, [sp, #36]
	str	sl, [sp, #40]
	bl	GuiLib_DrawStr
	cmp	r7, #195
	mov	r4, #0
	mov	fp, #240
	add	r8, r8, #64
	ldr	ip, [sp, #44]
	bne	.L3
	ldr	r3, .L5+4
	mov	r2, r5
	stmia	sp, {r3, r5}
	add	r6, sp, #48
	mvn	r3, #0
	mov	r0, #62
	mov	r1, #207
	str	r5, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	str	fp, [sp, #20]
	str	r4, [sp, #24]
	str	r4, [sp, #28]
	str	r4, [sp, #32]
	str	r4, [sp, #36]
	str	sl, [sp, #40]
	bl	GuiLib_DrawStr
	mov	r0, r6
	ldr	r1, .L5+8
	bl	format_binary_32
	mov	r0, #62
	mov	r1, #219
	mov	r2, r5
	mvn	r3, #0
	str	r6, [sp, #0]
	str	r5, [sp, #4]
	str	r5, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	str	fp, [sp, #20]
	str	r4, [sp, #24]
	str	r4, [sp, #28]
	str	r4, [sp, #32]
	str	r4, [sp, #36]
	str	sl, [sp, #40]
	bl	GuiLib_DrawStr
	bl	GuiLib_Refresh
	add	sp, sp, #112
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L6:
	.align	2
.L5:
	.word	TPMICRO_debug_text
	.word	.LC0
	.word	tpmicro_data+8
.LFE0:
	.size	FDTO_TPMICRO_draw_debug, .-FDTO_TPMICRO_draw_debug
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"~~~~####~~~~####~~~~####~~~~####\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x94
	.align	2
.LEFDE0:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_tpmicro_debug.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x32
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF0
	.byte	0x1
	.4byte	.LASF1
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x28
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 148
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"FDTO_TPMICRO_draw_debug\000"
.LASF1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_tpmicro_debug.c\000"
.LASF0:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
