	.file	"e_weather_sensors.c"
	.text
.Ltext0:
	.section	.text.FDTO_WIND_GAGE_show_wind_gage_in_use_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_WIND_GAGE_show_wind_gage_in_use_dropdown, %function
FDTO_WIND_GAGE_show_wind_gage_in_use_dropdown:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	mov	r0, #107
	ldr	r2, [r3, #0]
	mov	r1, #166
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L3:
	.align	2
.L2:
	.word	GuiVar_WindGageInUse
.LFE6:
	.size	FDTO_WIND_GAGE_show_wind_gage_in_use_dropdown, .-FDTO_WIND_GAGE_show_wind_gage_in_use_dropdown
	.section	.text.FDTO_RAIN_BUCKET_show_rain_bucket_in_use_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_RAIN_BUCKET_show_rain_bucket_in_use_dropdown, %function
FDTO_RAIN_BUCKET_show_rain_bucket_in_use_dropdown:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L5
	mov	r0, #116
	ldr	r2, [r3, #0]
	mov	r1, #90
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L6:
	.align	2
.L5:
	.word	GuiVar_RainBucketInUse
.LFE5:
	.size	FDTO_RAIN_BUCKET_show_rain_bucket_in_use_dropdown, .-FDTO_RAIN_BUCKET_show_rain_bucket_in_use_dropdown
	.section	.text.FDTO_ET_GAGE_show_skip_tonight_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_ET_GAGE_show_skip_tonight_dropdown, %function
FDTO_ET_GAGE_show_skip_tonight_dropdown:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L8
	mov	r0, #202
	ldr	r2, [r3, #0]
	mov	r1, #72
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L9:
	.align	2
.L8:
	.word	GuiVar_ETGageSkipTonight
.LFE3:
	.size	FDTO_ET_GAGE_show_skip_tonight_dropdown, .-FDTO_ET_GAGE_show_skip_tonight_dropdown
	.section	.text.FDTO_ET_GAGE_show_log_pulses_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_ET_GAGE_show_log_pulses_dropdown, %function
FDTO_ET_GAGE_show_log_pulses_dropdown:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L11
	mov	r0, #202
	ldr	r2, [r3, #0]
	mov	r1, #44
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L12:
	.align	2
.L11:
	.word	GuiVar_ETGageLogPulses
.LFE2:
	.size	FDTO_ET_GAGE_show_log_pulses_dropdown, .-FDTO_ET_GAGE_show_log_pulses_dropdown
	.section	.text.FDTO_ET_GAGE_show_et_gage_in_use_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_ET_GAGE_show_et_gage_in_use_dropdown, %function
FDTO_ET_GAGE_show_et_gage_in_use_dropdown:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L14
	mov	r0, #106
	ldr	r2, [r3, #0]
	mov	r1, #14
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L15:
	.align	2
.L14:
	.word	GuiVar_ETGageInUse
.LFE1:
	.size	FDTO_ET_GAGE_show_et_gage_in_use_dropdown, .-FDTO_ET_GAGE_show_et_gage_in_use_dropdown
	.section	.text.process_weather_sensor_installed_at.constprop.0,"ax",%progbits
	.align	2
	.type	process_weather_sensor_installed_at.constprop.0, %function
process_weather_sensor_installed_at.constprop.0:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L17
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI0:
	ldr	r3, [r3, #0]
	mov	r4, r2
	mov	r2, #1
	cmp	r3, r2
	str	r2, [sp, #0]
	movls	r2, #0
	movhi	r2, #1
	mov	r5, r1
	str	r2, [sp, #4]
	sub	r3, r3, #1
	mov	r2, #0
	bl	process_uns32
	ldr	r2, [r5, #0]
	ldr	r3, .L17+4
	mov	r0, r4
	ldr	r3, [r3, r2, asl #2]
	mov	r1, #3
	ldr	r2, .L17+8
	add	r3, r3, #65
	bl	snprintf
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	Refresh_Screen
.L18:
	.align	2
.L17:
	.word	.LANCHOR0
	.word	WEATHER_GuiVar_box_indexes_with_dash_w_option
	.word	.LC0
.LFE9:
	.size	process_weather_sensor_installed_at.constprop.0, .-process_weather_sensor_installed_at.constprop.0
	.section	.text.ET_GAGE_clear_runaway_gage,"ax",%progbits
	.align	2
	.global	ET_GAGE_clear_runaway_gage
	.type	ET_GAGE_clear_runaway_gage, %function
ET_GAGE_clear_runaway_gage:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L22
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r5, [r3, #84]
	cmp	r5, #1
	bne	.L20
	ldr	r4, .L22+4
	ldr	r3, [r4, #200]
	cmp	r3, #0
	bne	.L20
	bl	good_key_beep
	ldr	r0, .L22+8
	bl	DIALOG_draw_ok_dialog
	str	r5, [r4, #200]
	ldmfd	sp!, {r4, r5, pc}
.L20:
	ldmfd	sp!, {r4, r5, lr}
	b	bad_key_beep
.L23:
	.align	2
.L22:
	.word	weather_preserves
	.word	irri_comm
	.word	619
.LFE4:
	.size	ET_GAGE_clear_runaway_gage, .-ET_GAGE_clear_runaway_gage
	.section	.text.FDTO_WEATHER_SENSORS_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_WEATHER_SENSORS_draw_screen
	.type	FDTO_WEATHER_SENSORS_draw_screen, %function
FDTO_WEATHER_SENSORS_draw_screen:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L31
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	ldrnesh	r1, [r3, #0]
	bne	.L29
	ldr	r5, .L31+4
	mov	r4, #0
	str	r4, [r5, #0]
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #1
	bne	.L26
	ldr	r3, .L31+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L31+12
	mov	r3, #203
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #0]
	ldr	r2, .L31+16
	ldr	r1, .L31+20
.L28:
	ldr	r0, [r2, #16]
	add	r2, r2, #92
	cmp	r0, #0
	strne	r4, [r1, r3, asl #2]
	add	r4, r4, #1
	addne	r3, r3, #1
	cmp	r4, #12
	bne	.L28
	ldr	r2, .L31+4
	str	r3, [r2, #0]
	ldr	r3, .L31+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L26:
	bl	WEATHER_copy_et_gage_settings_into_GuiVars
	bl	WEATHER_copy_rain_bucket_settings_into_GuiVars
	bl	WEATHER_copy_wind_gage_settings_into_GuiVars
	mov	r1, #0
.L29:
	mov	r0, #72
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L32:
	.align	2
.L31:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	chain_members_recursive_MUTEX
	.word	.LC1
	.word	chain
	.word	WEATHER_GuiVar_box_indexes_with_dash_w_option
.LFE7:
	.size	FDTO_WEATHER_SENSORS_draw_screen, .-FDTO_WEATHER_SENSORS_draw_screen
	.section	.text.WEATHER_SENSORS_process_screen,"ax",%progbits
	.align	2
	.global	WEATHER_SENSORS_process_screen
	.type	WEATHER_SENSORS_process_screen, %function
WEATHER_SENSORS_process_screen:
.LFB8:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L84
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #48
.LCFI4:
	cmp	r3, #740
	mov	r4, r1
	bne	.L73
	ldr	r3, .L84+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #10
	ldrls	pc, [pc, r3, asl #2]
	b	.L33
.L42:
	.word	.L37
	.word	.L33
	.word	.L38
	.word	.L33
	.word	.L39
	.word	.L40
	.word	.L33
	.word	.L33
	.word	.L33
	.word	.L33
	.word	.L41
.L37:
	ldr	r1, .L84+8
	b	.L76
.L38:
	ldr	r1, .L84+12
.L76:
	bl	COMBO_BOX_key_press
	b	.L33
.L39:
	ldr	r1, .L84+16
	b	.L76
.L40:
	ldr	r1, .L84+20
	b	.L76
.L41:
	ldr	r1, .L84+24
	b	.L76
.L73:
	cmp	r0, #3
	beq	.L44
	bhi	.L48
	cmp	r0, #1
	beq	.L45
	bhi	.L46
	b	.L44
.L48:
	cmp	r0, #80
	beq	.L47
	cmp	r0, #84
	beq	.L47
	cmp	r0, #4
	bne	.L74
	b	.L45
.L46:
	ldr	r4, .L84+4
	ldrh	r3, [r4, #0]
	cmp	r3, #14
	bhi	.L49
	ldrsh	r2, [r4, #0]
	mov	r5, #1
	ldr	r3, .L84+28
	mov	r2, r5, asl r2
	and	r3, r2, r3
	cmp	r3, #0
	bne	.L50
	tst	r2, #16384
	bne	.L51
	b	.L49
.L50:
	bl	good_key_beep
	ldrh	r3, [r4, #0]
	str	r5, [sp, #12]
	cmp	r3, #0
	ldreq	r3, .L84+32
	beq	.L75
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #2
	ldreq	r3, .L84+36
	beq	.L75
	cmp	r3, #4
	ldreq	r3, .L84+40
	beq	.L75
	cmp	r3, #5
	ldreq	r3, .L84+44
	beq	.L75
	cmp	r3, #10
	bne	.L53
	ldr	r3, .L84+48
.L75:
	str	r3, [sp, #32]
.L53:
	add	r0, sp, #12
	bl	Display_Post_Command
	b	.L33
.L51:
	bl	ET_GAGE_clear_runaway_gage
	b	.L33
.L49:
	bl	bad_key_beep
	b	.L33
.L47:
	ldr	r3, .L84+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #13
	ldrls	pc, [pc, r3, asl #2]
	b	.L33
.L71:
	.word	.L57
	.word	.L58
	.word	.L59
	.word	.L60
	.word	.L61
	.word	.L62
	.word	.L63
	.word	.L64
	.word	.L65
	.word	.L66
	.word	.L67
	.word	.L68
	.word	.L69
	.word	.L70
.L57:
	ldr	r0, .L84+8
	b	.L78
.L58:
	ldr	r1, .L84+52
	ldr	r2, .L84+56
	b	.L80
.L59:
	ldr	r0, .L84+12
	bl	process_bool
	b	.L79
.L60:
	mov	r4, #1
	mov	r5, #0
	mov	r3, #100
	ldr	r1, .L84+60
	mov	r2, r4
	stmia	sp, {r4, r5}
	bl	process_uns32
	ldr	r3, .L84+16
	mov	r0, r5
	str	r4, [r3, #0]
	b	.L81
.L61:
	ldr	r0, .L84+16
	b	.L78
.L62:
	ldr	r0, .L84+20
	b	.L78
.L63:
	ldr	r1, .L84+64
	ldr	r2, .L84+68
	b	.L80
.L64:
	ldr	r2, .L84+72
	mov	r3, #0
	str	r2, [sp, #0]	@ float
	str	r3, [sp, #4]
	ldr	r1, .L84+76
	b	.L83
.L65:
	ldr	r2, .L84+72
	ldr	r1, .L84+80
	mov	r3, #0
	str	r2, [sp, #0]	@ float
	str	r3, [sp, #4]
.L83:
	mov	r3, #1073741824
	b	.L82
.L66:
	mov	r3, #0
	ldr	r2, .L84+72
	str	r3, [sp, #4]
	ldr	r1, .L84+84
	ldr	r3, .L84+88
	str	r2, [sp, #0]	@ float
.L82:
	bl	process_fl
	b	.L79
.L67:
	ldr	r0, .L84+24
.L78:
	bl	process_bool
	mov	r0, #0
.L81:
	bl	Redraw_Screen
	b	.L33
.L68:
	ldr	r1, .L84+92
	ldr	r2, .L84+96
.L80:
	bl	process_weather_sensor_installed_at.constprop.0
	b	.L33
.L69:
	ldr	r3, .L84+100
	mov	r1, #1
	ldr	r2, [r3, #0]
	mov	r3, #0
	stmia	sp, {r1, r3}
	ldr	r1, .L84+104
	add	r2, r2, #1
	mov	r3, #99
	b	.L77
.L70:
	ldr	r3, .L84+104
	mov	r1, #0
	ldr	r3, [r3, #0]
	str	r1, [sp, #4]
	ldr	r1, .L84+100
	mov	r2, #1
	sub	r3, r3, #1
	str	r2, [sp, #0]
.L77:
	bl	process_uns32
.L79:
	bl	Refresh_Screen
	b	.L33
.L45:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L33
.L44:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L33
.L74:
	cmp	r0, #67
	bne	.L72
	str	r0, [sp, #8]
	bl	WEATHER_extract_and_store_et_gage_changes_from_GuiVars
	bl	WEATHER_extract_and_store_rain_bucket_changes_from_GuiVars
	bl	WEATHER_extract_and_store_wind_gage_changes_from_GuiVars
	ldr	r3, .L84+108
	ldr	r0, [sp, #8]
	mov	r2, #4
	str	r2, [r3, #0]
.L72:
	mov	r1, r4
	bl	KEY_process_global_keys
.L33:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, pc}
.L85:
	.align	2
.L84:
	.word	GuiLib_CurStructureNdx
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_ETGageInUse
	.word	GuiVar_ETGageLogPulses
	.word	GuiVar_ETGageSkipTonight
	.word	GuiVar_RainBucketInUse
	.word	GuiVar_WindGageInUse
	.word	1077
	.word	FDTO_ET_GAGE_show_et_gage_in_use_dropdown
	.word	FDTO_ET_GAGE_show_log_pulses_dropdown
	.word	FDTO_ET_GAGE_show_skip_tonight_dropdown
	.word	FDTO_RAIN_BUCKET_show_rain_bucket_in_use_dropdown
	.word	FDTO_WIND_GAGE_show_wind_gage_in_use_dropdown
	.word	WEATHER_GuiVar_ETGageInstalledAtIndex
	.word	GuiVar_ETGageInstalledAt
	.word	GuiVar_ETGagePercentFull
	.word	WEATHER_GuiVar_RainBucketInstalledAtIndex
	.word	GuiVar_RainBucketInstalledAt
	.word	1008981770
	.word	GuiVar_RainBucketMinimum
	.word	GuiVar_RainBucketMaximumHourly
	.word	GuiVar_RainBucketMaximum24Hour
	.word	1086324736
	.word	WEATHER_GuiVar_WindGageInstalledAtIndex
	.word	GuiVar_WindGageInstalledAt
	.word	GuiVar_WindGageResumeSpeed
	.word	GuiVar_WindGagePauseSpeed
	.word	GuiVar_MenuScreenToShow
.LFE8:
	.size	WEATHER_SENSORS_process_screen, .-WEATHER_SENSORS_process_screen
	.section	.bss.WEATHER_GuiVar_num_of_dash_w_controllers,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	WEATHER_GuiVar_num_of_dash_w_controllers, %object
	.size	WEATHER_GuiVar_num_of_dash_w_controllers, 4
WEATHER_GuiVar_num_of_dash_w_controllers:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%c\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_weather_sensors.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI0-.LFB9
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI2-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI3-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_weather_sensors.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xcd
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.byte	0x3c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.byte	0xb5
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0xa1
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0x7c
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0x68
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.byte	0x54
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0x82
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xbb
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.byte	0xff
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB9
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB7
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB8
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI4
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"FDTO_ET_GAGE_show_skip_tonight_dropdown\000"
.LASF10:
	.ascii	"process_weather_sensor_installed_at\000"
.LASF5:
	.ascii	"ET_GAGE_clear_runaway_gage\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_weather_sensors.c\000"
.LASF0:
	.ascii	"FDTO_WIND_GAGE_show_wind_gage_in_use_dropdown\000"
.LASF3:
	.ascii	"FDTO_ET_GAGE_show_log_pulses_dropdown\000"
.LASF4:
	.ascii	"FDTO_ET_GAGE_show_et_gage_in_use_dropdown\000"
.LASF6:
	.ascii	"FDTO_WEATHER_SENSORS_draw_screen\000"
.LASF1:
	.ascii	"FDTO_RAIN_BUCKET_show_rain_bucket_in_use_dropdown\000"
.LASF7:
	.ascii	"WEATHER_SENSORS_process_screen\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
