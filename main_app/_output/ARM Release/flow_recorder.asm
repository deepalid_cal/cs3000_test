	.file	"flow_recorder.c"
	.text
.Ltext0:
	.section	.text.ci_flow_recording_timer_callback,"ax",%progbits
	.align	2
	.global	ci_flow_recording_timer_callback
	.type	ci_flow_recording_timer_callback, %function
ci_flow_recording_timer_callback:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	bl	pvTimerGetTimerID
	mov	r2, #512
	mov	r3, #0
	mov	r1, r0
	ldr	r0, .L2
	ldr	lr, [sp], #4
	b	CONTROLLER_INITIATED_post_to_messages_queue
.L3:
	.align	2
.L2:
	.word	409
.LFE0:
	.size	ci_flow_recording_timer_callback, .-ci_flow_recording_timer_callback
	.section	.text.nm_flow_recorder_inc_pointer,"ax",%progbits
	.align	2
	.global	nm_flow_recorder_inc_pointer
	.type	nm_flow_recorder_inc_pointer, %function
nm_flow_recorder_inc_pointer:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #0]
	add	r2, r1, #38400
	add	r3, r3, #24
	cmp	r2, r3
	movls	r3, r1
	str	r3, [r0, #0]
	bx	lr
.LFE2:
	.size	nm_flow_recorder_inc_pointer, .-nm_flow_recorder_inc_pointer
	.section	.text.nm_flow_recorder_add,"ax",%progbits
	.align	2
	.global	nm_flow_recorder_add
	.type	nm_flow_recorder_add, %function
nm_flow_recorder_add:
.LFB3:
	@ args = 8, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI1:
	mov	r1, r1, asl #16
	mov	r4, r0
	sub	sp, sp, #40
.LCFI2:
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	mov	r7, r1, lsr #16
	ldr	r0, .L15
	mov	r1, r4
	mov	r6, r2, lsr #16
	mov	r8, r3, lsr #16
	ldrh	r9, [sp, #72]
	bl	nm_OnList
	cmp	r0, #0
	bne	.L8
	ldr	r0, .L15+4
	ldr	r1, .L15+8
	ldr	r2, .L15+12
	mov	r3, #172
	bl	Alert_item_not_on_list_with_filename
	b	.L7
.L8:
	ldr	r3, .L15+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L15+12
	mov	r3, #176
	bl	xQueueTakeMutexRecursive_debug
	ldr	r5, [r4, #40]
.LBB5:
	ldr	sl, [r5, #472]
	cmp	sl, #0
	bne	.L10
	ldr	r1, .L15+12
	mov	r2, #76
	mov	r0, #38400
	bl	mem_malloc_debug
	ldr	r3, .L15+20
	str	sl, [r5, #492]
	ldr	r1, .L15+24
	mov	r2, sl
	str	r0, [r5, #472]
	str	r0, [r5, #476]
	str	r0, [r5, #480]
	str	r0, [r5, #484]
	str	r0, [r5, #488]
	str	r3, [sp, #0]
	ldr	r0, .L15+28
	mov	r3, r5
	bl	xTimerCreate
	cmp	r0, #0
	str	r0, [r5, #496]
	bne	.L10
	ldr	r0, .L15+12
	bl	RemovePathFromFileName
	mov	r2, #99
	mov	r1, r0
	ldr	r0, .L15+32
	bl	Alert_Message_va
.L10:
.LBE5:
	ldr	r3, [r4, #40]
	ldr	r5, [r3, #476]
	mov	r0, r5
	bl	EPSON_obtain_latest_time_and_date
	ldrb	r3, [r4, #90]	@ zero_extendqisi2
	strh	r3, [r5, #6]	@ movhi
	ldrb	r3, [r4, #91]	@ zero_extendqisi2
	strh	r3, [r5, #8]	@ movhi
	ldrh	r3, [r4, #80]
	strh	r7, [r5, #12]	@ movhi
	strh	r3, [r5, #10]	@ movhi
	ldrh	r3, [sp, #76]
	strh	r6, [r5, #14]	@ movhi
	strh	r3, [r5, #20]	@ movhi
	ldr	r3, [r4, #40]
	strh	r8, [r5, #16]	@ movhi
	ldr	r1, [r3, #472]
	add	r0, r3, #476
	strh	r9, [r5, #18]	@ movhi
	bl	nm_flow_recorder_inc_pointer
	ldr	r3, [r4, #40]
	ldr	r1, [r3, #476]
	ldr	r2, [r3, #480]
	cmp	r1, r2
	bne	.L11
	add	r0, r3, #480
	ldr	r1, [r3, #472]
	bl	nm_flow_recorder_inc_pointer
.L11:
	ldr	r3, [r4, #40]
	ldr	r1, [r3, #476]
	ldr	r2, [r3, #484]
	cmp	r1, r2
	bne	.L12
	add	r0, r3, #484
	ldr	r1, [r3, #472]
	bl	nm_flow_recorder_inc_pointer
.L12:
	ldr	r3, [r4, #40]
	ldr	r1, [r3, #476]
	ldr	r2, [r3, #488]
	cmp	r1, r2
	bne	.L13
	add	r0, r3, #488
	ldr	r1, [r3, #472]
	bl	nm_flow_recorder_inc_pointer
.L13:
	ldr	r3, [r4, #40]
	ldr	r0, [r3, #496]
	bl	xTimerIsTimerActive
	subs	r3, r0, #0
	bne	.L14
	ldr	r2, [r4, #40]
	mvn	r1, #0
	str	r1, [sp, #0]
	mov	r1, #2
	ldr	r0, [r2, #496]
	ldr	r2, .L15+24
	bl	xTimerGenericCommand
.L14:
	ldr	r3, .L15+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L15+36
	ldrsh	r3, [r3, #0]
	cmp	r3, #82
	bne	.L7
.LBB6:
	mov	r3, #1
	str	r3, [sp, #4]
	ldr	r3, .L15+40
	add	r0, sp, #4
	str	r3, [sp, #24]
	bl	Display_Post_Command
.L7:
.LBE6:
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L16:
	.align	2
.L15:
	.word	foal_irri+16
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	system_preserves_recursive_MUTEX
	.word	ci_flow_recording_timer_callback
	.word	720000
	.word	.LC3
	.word	.LC4
	.word	GuiLib_CurStructureNdx
	.word	FDTO_FLOW_RECORDING_redraw_scrollbox
.LFE3:
	.size	nm_flow_recorder_add, .-nm_flow_recorder_add
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"pilc_ptr\000"
.LC1:
	.ascii	"foal_irri.list_of_foal_all_irrigation\000"
.LC2:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/flow_recorder.c\000"
.LC3:
	.ascii	"\000"
.LC4:
	.ascii	"Timer NOT CREATED : %s, %u\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/flow_recorder.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x61
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x42
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x7c
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x9a
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"ci_flow_recording_timer_callback\000"
.LASF1:
	.ascii	"nm_flow_recorder_inc_pointer\000"
.LASF5:
	.ascii	"_nm_flow_recorder_verify_allocation\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/flow_recorder.c\000"
.LASF2:
	.ascii	"nm_flow_recorder_add\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
