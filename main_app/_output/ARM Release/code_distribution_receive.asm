	.file	"code_distribution_receive.c"
	.text
.Ltext0:
	.section	.text.free_and_step_on,"ax",%progbits
	.align	2
	.type	free_and_step_on, %function
free_and_step_on:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r4, .L4
	ldr	r0, [r4, #176]
	cmp	r0, #0
	beq	.L2
	ldr	r1, .L4+4
	ldr	r2, .L4+8
	bl	mem_free_debug
.L2:
	ldr	r0, [r4, #172]
	mov	r3, #0
	cmp	r0, r3
	str	r3, [r4, #176]
	beq	.L3
	ldr	r1, .L4+4
	mov	r2, #652
	bl	mem_free_debug
.L3:
	ldr	r3, .L4
	mov	r2, #0
	str	r2, [r3, #172]
	str	r2, [r3, #168]
	str	r2, [r3, #180]
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, pc}
.L5:
	.align	2
.L4:
	.word	cdcs
	.word	.LC1
	.word	642
.LFE4:
	.size	free_and_step_on, .-free_and_step_on
	.section	.text.process_incoming_code_download_init_packet,"ax",%progbits
	.align	2
	.type	process_incoming_code_download_init_packet, %function
process_incoming_code_download_init_packet:
.LFB5:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L57
	ldr	r2, .L57+4
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI1:
	ldr	r1, [r2, r3]
	mov	r5, r0
	ldr	r0, [r0, #12]
	ldr	r4, [r5, #8]
	sub	sp, sp, #36
.LCFI2:
	add	r1, r0, r1
	str	r1, [r2, r3]
	add	r0, sp, #4
	mov	r1, r4
	mov	r2, #12
	bl	memcpy
	add	r1, r4, #12
	mov	r2, #12
	add	r4, r4, #24
	add	r0, sp, #16
	bl	memcpy
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #28
	bl	memcpy
	mov	r2, #4
	add	r0, sp, #32
	mov	r1, r4
	bl	memcpy
	ldrh	r2, [sp, #14]
	ldrh	r3, [sp, #26]
	cmp	r2, r3
	beq	.L7
	ldr	r0, .L57+8
	bl	Alert_Message
.L7:
	ldrb	r3, [sp, #5]	@ zero_extendqisi2
	ldr	r2, [r5, #4]
	cmp	r2, r3
	beq	.L8
	ldr	r0, .L57+12
	bl	Alert_Message
.L8:
	ldrh	r3, [sp, #26]
	cmp	r3, #30
	cmpne	r3, #20
	moveq	r8, #1
	beq	.L9
	ldr	r0, .L57+16
	bl	Alert_Message
	mov	r8, #0
.L9:
	ldr	r2, [r5, #4]
	ldr	r3, .L57+20
	cmp	r2, #13
	bne	.L10
	ldr	r2, [r3, #0]
	mov	r4, #1
	sub	r2, r2, #8
	cmp	r2, r4
	str	r4, [r3, #188]
	ldrhi	r0, .L57+24
	bhi	.L56
	ldrh	r3, [r3, #158]
	cmp	r3, #21
	bne	.L12
	ldrh	r3, [sp, #26]
	cmp	r3, #20
	ldrne	r0, .L57+28
	bne	.L53
	b	.L13
.L12:
	cmp	r3, #31
	ldrne	r0, .L57+32
	bne	.L53
	ldrh	r3, [sp, #26]
	cmp	r3, #30
	ldrne	r0, .L57+36
	beq	.L13
.L53:
	bl	Alert_Message
	mov	r4, #0
.L13:
	ldr	r6, .L57+20
	ldr	r3, [sp, #28]
	ldr	r2, [r6, #148]
	cmp	r2, r3
	bne	.L15
	ldr	r2, [r6, #152]
	ldr	r3, [sp, #32]
	cmp	r2, r3
	beq	.L16
.L15:
	ldr	r0, .L57+40
	bl	Alert_Message
	mov	r4, #0
.L16:
	ldr	r2, [r6, #140]
	ldr	r3, [sp, #16]
	cmp	r2, r3
	bne	.L17
	ldr	r3, .L57+20
	ldrh	r2, [r3, #156]
	ldrh	r3, [sp, #24]
	cmp	r2, r3
	beq	.L18
.L17:
	ldr	r0, .L57+44
	bl	Alert_Message
	mov	r4, #0
.L18:
	ldr	r6, .L57+20
	ldr	r3, [r6, #0]
	cmp	r3, #8
	ldreq	r7, [r6, #176]
	ldrne	r7, [r6, #172]
	cmp	r7, #0
	bne	.L21
	ldr	r0, .L57+48
	bl	Alert_Message
	mov	r4, r7
.L21:
	ldr	r3, [r6, #144]
	ldr	r2, [sp, #20]
	cmp	r2, r3
	ldrne	r0, .L57+52
	beq	.L22
.L56:
	bl	Alert_Message
	b	.L50
.L10:
	cmp	r2, #11
	beq	.L50
	cmp	r2, #12
	mov	r4, #1
	bne	.L23
	str	r4, [r3, #188]
.L50:
	mov	r4, #0
.L22:
	cmp	r8, #0
	beq	.L23
	cmp	r4, #0
	ldr	r6, .L57+20
	beq	.L24
	ldr	r0, .L57+56
	bl	Alert_Message
	ldrh	r3, [sp, #26]
	mov	r4, #1
	strh	r3, [r6, #158]	@ movhi
	mov	r0, r4
	mov	r6, r4
	b	.L25
.L24:
	ldr	r3, [sp, #16]
	add	r0, r6, #12
	str	r3, [r6, #140]
	ldr	r3, [sp, #20]
	mov	r1, r4
	str	r3, [r6, #144]
	ldrh	r3, [sp, #24]
	mov	r2, #128
	strh	r3, [r6, #156]	@ movhi
	ldrh	r3, [sp, #26]
	str	r4, [r6, #164]
	strh	r3, [r6, #158]	@ movhi
	mov	r3, #1
	str	r3, [r6, #160]
	ldr	r3, [sp, #28]
	str	r4, [r6, #168]
	str	r3, [r6, #148]
	ldr	r3, [sp, #32]
	str	r3, [r6, #152]
	bl	memset
	ldrh	r3, [r6, #158]
	cmp	r3, #20
	bne	.L26
	ldr	r3, [r6, #8]
	cmp	r3, #0
	bne	.L27
	ldr	r0, [r6, #140]
	add	r1, r6, #176
	ldr	r2, .L57+60
	mov	r3, #956
	bl	mem_oabia
	subs	r6, r0, #0
	bne	.L46
.L27:
	ldr	r3, .L57+20
	mov	r2, #0
	str	r2, [r3, #176]
	b	.L29
.L26:
	ldr	r3, [r6, #4]
	cmp	r3, #0
	bne	.L30
	ldr	r0, [r6, #140]
	add	r1, r6, #172
	ldr	r2, .L57+60
	ldr	r3, .L57+64
	bl	mem_oabia
	subs	r6, r0, #0
	bne	.L46
.L30:
	ldr	r3, .L57+20
	mov	r2, #0
	str	r2, [r3, #172]
.L29:
	ldr	r0, .L57+68
	bl	Alert_Message
	mov	r0, #36
	bl	SYSTEM_application_requested_restart
	mov	r6, #0
.L46:
	ldr	r3, [r5, #4]
	cmp	r3, #11
	ldreq	r3, .L57+20
	moveq	r0, #1
	streq	r0, [r3, #0]
	beq	.L25
.L31:
	cmp	r3, #12
	moveq	r2, #2
	ldreq	r3, .L57+20
	beq	.L54
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	beq	.L33
	ldr	r3, [r5, #4]
	cmp	r3, #13
	bne	.L33
	ldr	r3, .L57+20
	ldrh	r2, [r3, #158]
	cmp	r2, #20
	moveq	r2, #8
	movne	r2, #9
.L54:
	str	r2, [r3, #0]
	mov	r0, #1
	b	.L25
.L33:
	ldr	r0, .L57+72
	ldr	r1, [r5, #4]
	bl	Alert_Message_va
	mov	r0, #0
	b	.L25
.L23:
	ldr	r6, .L57+20
	ldr	r0, [r6, #176]
	cmp	r0, #0
	beq	.L35
	ldr	r1, .L57+60
	ldr	r2, .L57+76
	bl	mem_free_debug
.L35:
	ldr	r0, [r6, #172]
	cmp	r0, #0
	beq	.L52
	ldr	r1, .L57+60
	ldr	r2, .L57+80
	bl	mem_free_debug
	mov	r0, #0
.L52:
	mov	r6, r0
.L25:
	cmp	r6, #0
	moveq	r6, #0
	andne	r6, r0, #1
	cmp	r6, #0
	beq	.L36
	ldr	r6, .L57+20
	ldrh	r3, [r6, #158]
	cmp	r3, #20
	bne	.L37
	cmp	r4, #0
	beq	.L38
	ldr	r0, .L57+84
	bl	Alert_Message
	b	.L39
.L38:
	ldr	r0, .L57+88
	ldr	r1, [r6, #140]
	ldrh	r2, [r6, #156]
	bl	Alert_Message_va
.L39:
	ldr	r3, [r6, #176]
	str	r3, [r6, #168]
	str	r3, [r6, #180]
	ldr	r3, [r5, #4]
	cmp	r3, #11
	bne	.L40
	mov	r2, #0
	ldr	r0, [r5, #16]
	mov	r1, #22
	mov	r3, r2
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
.L40:
	mov	r2, #21
	b	.L55
.L37:
	cmp	r3, #30
	bne	.L41
	cmp	r4, #0
	beq	.L42
	ldr	r0, .L57+92
	bl	Alert_Message
	b	.L43
.L42:
	ldr	r0, .L57+96
	ldr	r1, [r6, #140]
	ldrh	r2, [r6, #156]
	bl	Alert_Message_va
.L43:
	ldr	r3, [r6, #172]
	str	r3, [r6, #168]
	str	r3, [r6, #180]
	ldr	r3, [r5, #4]
	cmp	r3, #11
	bne	.L44
	mov	r2, #0
	ldr	r0, [r5, #16]
	mov	r1, #32
	mov	r3, r2
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
.L44:
	mov	r2, #31
.L55:
	ldr	r3, .L57+20
	strh	r2, [r3, #158]	@ movhi
.L41:
	ldr	r3, .L57+20
	mov	r1, #2
	ldr	r2, [r3, #0]
	sub	r2, r2, #8
	cmp	r2, #1
	mvn	r2, #0
	str	r2, [sp, #0]
	ldr	r0, [r3, #184]
	bls	.L45
	ldr	r2, .L57+100
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r0, #0
	bl	CODE_DOWNLOAD_draw_dialog
	b	.L36
.L45:
	ldr	r2, .L57+104
	mov	r3, #0
	bl	xTimerGenericCommand
.L36:
	ldr	r0, [r5, #8]
	ldr	r1, .L57+60
	ldr	r2, .L57+108
	bl	mem_free_debug
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L58:
	.align	2
.L57:
	.word	8548
	.word	SerDrvrVars_s
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	cdcs
	.word	.LC12
	.word	.LC5
	.word	.LC7
	.word	.LC6
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	.LC13
	.word	.LC1
	.word	970
	.word	.LC14
	.word	.LC15
	.word	1042
	.word	1047
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	6000
	.word	60000
	.word	1169
.LFE5:
	.size	process_incoming_code_download_init_packet, .-process_incoming_code_download_init_packet
	.section	.text.process_firmware_upgrade_after_last_packet_received,"ax",%progbits
	.align	2
	.type	process_firmware_upgrade_after_last_packet_received, %function
process_firmware_upgrade_after_last_packet_received:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI3:
	ldr	r4, .L69
	mov	r5, r0
	ldrh	r3, [r4, #158]
	cmp	r3, #21
	bne	.L60
	ldr	r1, [r4, #140]
	ldr	r0, [r4, #176]
	bl	CRC_calculate_32bit_big_endian
	ldr	r2, [r4, #144]
	cmp	r0, r2
	mov	r1, r0
	bne	.L61
	mov	r3, #1
	str	r3, [r4, #8]
	ldr	r3, [r5, #4]
	cmp	r3, #11
	bne	.L62
	mov	r2, #0
	ldr	r0, [r5, #16]
	mov	r1, #23
	mov	r3, r2
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	beq	.L62
	mov	r0, #0
	bl	CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg
.L62:
	bl	CONTROLLER_INITIATED_update_comm_server_registration_info
	ldr	r0, .L69+4
	ldr	r1, .L69+8
	bl	COMM_TEST_send_all_program_data_to_the_cloud
	ldr	r3, .L69
	mov	r0, #0
	ldr	r2, [r3, #140]
	ldr	r1, .L69+12
	str	r2, [sp, #0]
	str	r0, [sp, #4]
	str	r0, [sp, #8]
	mov	r2, r0
	ldr	r3, [r3, #176]
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	mov	r0, #1
	b	.L63
.L61:
	ldr	r0, .L69+16
	b	.L68
.L60:
	cmp	r3, #31
	bne	.L64
	ldr	r1, [r4, #140]
	ldr	r0, [r4, #172]
	bl	CRC_calculate_32bit_big_endian
	ldr	r2, [r4, #144]
	cmp	r0, r2
	mov	r1, r0
	bne	.L65
	ldr	r3, [r5, #4]
	cmp	r3, #11
	moveq	r3, #1
	streq	r3, [r4, #192]
	ldreq	r3, [r5, #16]
	streq	r3, [r4, #196]
	bl	CONTROLLER_INITIATED_update_comm_server_registration_info
	ldr	r3, .L69
	mov	r0, #0
	ldr	r2, [r3, #140]
	mov	r4, #1
	str	r4, [r3, #4]
	ldr	r1, .L69+20
	str	r2, [sp, #0]
	str	r0, [sp, #4]
	str	r0, [sp, #8]
	mov	r2, r0
	ldr	r3, [r3, #172]
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	mov	r0, r4
	b	.L63
.L65:
	ldr	r0, .L69+24
.L68:
	bl	Alert_Message_va
	b	.L67
.L64:
	ldr	r0, .L69+28
	bl	Alert_Message
.L67:
	mov	r0, #0
.L63:
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L70:
	.align	2
.L69:
	.word	cdcs
	.word	921
	.word	665
	.word	CS3000_APP_FILENAME
	.word	.LC20
	.word	TPMICRO_APP_FILENAME
	.word	.LC21
	.word	.LC22
.LFE3:
	.size	process_firmware_upgrade_after_last_packet_received, .-process_firmware_upgrade_after_last_packet_received
	.section	.text.code_receipt_error_timer_callback,"ax",%progbits
	.align	2
	.global	code_receipt_error_timer_callback
	.type	code_receipt_error_timer_callback, %function
code_receipt_error_timer_callback:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #37
	b	SYSTEM_application_requested_restart
.LFE0:
	.size	code_receipt_error_timer_callback, .-code_receipt_error_timer_callback
	.section	.text.process_incoming_packet,"ax",%progbits
	.align	2
	.global	process_incoming_packet
	.type	process_incoming_packet, %function
process_incoming_packet:
.LFB9:
	@ args = 0, pretend = 0, frame = 180
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI4:
	mov	r4, r0
	sub	sp, sp, #184
.LCFI5:
	add	r0, sp, #132
	ldr	r1, [r4, #8]
	mov	r2, #12
	bl	memcpy
	ldrh	r3, [sp, #142]
	cmp	r3, #20
	cmpne	r3, #30
	bne	.L73
	bl	CONFIG_this_controller_is_behind_a_hub
	ldr	r3, .L144
	ldr	r3, [r3, #0]
	cmp	r0, #0
	beq	.L74
	cmp	r3, #0
	cmpne	r3, #8
	beq	.L136
	cmp	r3, #9
	bne	.L72
	b	.L136
.L74:
	cmp	r3, #0
	ldrne	r0, .L144+4
	bne	.L137
.L136:
	mov	r0, r4
	bl	process_incoming_code_download_init_packet
	b	.L72
.L73:
	cmp	r3, #21
	cmpne	r3, #31
	movne	r6, #0
	moveq	r6, #1
	bne	.L78
.LBB14:
	ldr	r2, .L144+8
	ldr	r3, .L144+12
	ldr	r0, [r4, #12]
	ldr	r1, [r2, r3]
	add	r1, r0, r1
	str	r1, [r2, r3]
	ldr	r3, .L144
	ldr	r3, [r3, #0]
	sub	r2, r3, #1
	cmp	r3, #8
	cmpne	r2, #1
	bls	.L79
	cmp	r3, #9
	bne	.L80
.L79:
	ldr	r7, [r4, #8]
	mov	r2, #12
	mov	r1, r7
	add	r0, sp, #4
	bl	memcpy
	mov	r2, #4
	add	r0, sp, #156
	add	r1, r7, #12
	bl	memcpy
	ldr	r3, .L144
	ldrh	r2, [r3, #158]
	cmp	r2, #21
	bne	.L81
	ldr	r2, [r3, #8]
	cmp	r2, #0
	ldreq	r5, [r3, #176]
	beq	.L139
	b	.L120
.L81:
	cmp	r2, #31
	bne	.L120
	ldr	r2, [r3, #4]
	cmp	r2, #0
	bne	.L120
	ldr	r5, [r3, #172]
.L139:
	cmp	r5, #0
	beq	.L82
	ldr	r5, [r3, #168]
	adds	r5, r5, #0
	movne	r5, #1
	b	.L82
.L120:
	mov	r5, #0
.L82:
	ldr	r3, .L144
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L83
	ldr	r3, [r4, #4]
	cmp	r3, #11
	b	.L127
.L83:
	cmp	r3, #2
	bne	.L86
	ldr	r3, [r4, #4]
	cmp	r3, #12
	b	.L127
.L86:
	sub	r3, r3, #8
	cmp	r3, #1
	bhi	.L85
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	beq	.L85
	ldr	r3, [r4, #4]
	cmp	r3, #13
.L127:
	moveq	r3, #1
	beq	.L116
.L85:
	ldr	r0, .L144+16
	bl	Alert_Message
	mov	r3, #0
.L116:
	tst	r3, r5
	beq	.L80
	ldr	r5, .L144
	ldrh	r1, [sp, #158]
	ldrh	r2, [r5, #158]
	ldr	r6, [r4, #12]
	cmp	r1, r2
	ldrne	r0, .L144+20
	bne	.L142
	ldr	r3, [r5, #0]
	add	r1, r7, #16
	sub	r3, r3, #8
	cmp	r3, #1
	sub	r6, r6, #20
	ldrh	r3, [sp, #156]
	bhi	.L88
	ldrh	r2, [r5, #156]
	cmp	r2, r3
	bcc	.L89
	ldr	r2, [r5, #180]
	cmp	r2, #0
	beq	.L90
	sub	r3, r3, #1
	add	r0, r2, r3, asl #11
	str	r0, [r5, #168]
	mov	r2, r6
	bl	memcpy
	ldrh	r2, [sp, #156]
.LBB15:
	mov	r7, #1
	sub	r2, r2, #1
	add	r3, r5, r2, lsr #3
	ldrb	r1, [r3, #12]	@ zero_extendqisi2
	and	r2, r2, #7
	orr	r2, r1, r7, asl r2
.LBE15:
	ldrh	r6, [r5, #156]
.LBB16:
	strb	r2, [r3, #12]
.LBE16:
.LBB17:
	ldr	r1, .L144+24
	mov	r2, #8
	add	r0, sp, #144
	bl	memcpy
	movs	r2, r6, lsr #3
	and	r1, r6, #7
	beq	.L91
	add	r5, r5, #11
	mov	r3, #0
.L92:
	ldrb	r7, [r5, #1]!	@ zero_extendqisi2
	add	r3, r3, #1
	sub	r0, r7, #255
	rsbs	r7, r0, #0
	adc	r7, r7, r0
	cmp	r3, r2
	movcs	r0, #0
	andcc	r0, r7, #1
	cmp	r0, #0
	bne	.L92
.L91:
	cmp	r1, #0
	beq	.L93
	ldr	r3, .L144
	sub	r6, r6, #1
	add	r2, r3, r2
	and	r6, r6, #7
	add	r3, sp, #184
	add	r6, r3, r6
	ldrb	r2, [r2, #12]	@ zero_extendqisi2
	ldrb	r3, [r6, #-40]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L94
.L93:
.LBE17:
	cmp	r7, #0
	beq	.L94
	ldr	r3, .L144
	ldrh	r3, [r3, #158]
	cmp	r3, #21
	ldreq	r0, .L144+28
	ldrne	r0, .L144+32
	bl	Alert_Message
	b	.L141
.L94:
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L144
	mov	r1, #2
	ldr	r0, [r3, #184]
	ldr	r2, .L144+36
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L80
.L90:
	ldr	r0, .L144+40
	b	.L143
.L89:
	ldr	r0, .L144+44
.L143:
	bl	Alert_Message
	b	.L130
.L88:
	ldr	r2, [r5, #160]
	cmp	r3, r2
	ldrne	r0, .L144+48
	movne	r1, r3
	bne	.L142
	mov	r2, r6
	ldr	r0, [r5, #168]
	bl	memcpy
	ldr	r3, [r5, #164]
	ldrh	r2, [r5, #156]
	add	r3, r6, r3
	str	r3, [r5, #164]
	ldrh	r1, [sp, #156]
	cmp	r1, r2
	bne	.L98
	ldr	r2, [r5, #140]
	cmp	r3, r2
	bne	.L98
	mov	r2, #0
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #1
	mov	r3, r2
	ldr	r0, [r5, #184]
	bl	xTimerGenericCommand
	ldrh	r3, [r5, #158]
	cmp	r3, #21
	ldreq	r0, .L144+52
	ldrne	r0, .L144+56
	bl	Alert_Message
	mov	r0, #1
	bl	CODE_DOWNLOAD_close_dialog
.L141:
	mov	r0, r4
	bl	process_firmware_upgrade_after_last_packet_received
	cmp	r0, #0
	bne	.L80
	b	.L130
.L98:
	ldr	r5, .L144
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, #0
	ldr	r0, [r5, #184]
	ldr	r2, .L144+60
	bl	xTimerGenericCommand
	ldr	r3, [r5, #160]
	add	r3, r3, #1
	str	r3, [r5, #160]
	ldr	r3, [r5, #168]
	add	r6, r3, r6
	str	r6, [r5, #168]
	b	.L80
.L142:
	bl	Alert_Message_va
.L130:
	bl	free_and_step_on
.L80:
	ldr	r0, [r4, #8]
	ldr	r1, .L144+64
	ldr	r2, .L144+68
	b	.L135
.L78:
.LBE14:
	cmp	r3, #24
	bne	.L101
.LBB18:
.LBB19:
	ldr	r5, [r4, #8]
	mov	r2, #12
	mov	r1, r5
	add	r0, sp, #144
	bl	memcpy
	add	r1, r5, #12
	mov	r2, #4
	add	r0, sp, #160
	bl	memcpy
	add	r1, r5, #16
	mov	r2, #4
	add	r0, sp, #164
	bl	memcpy
	add	r1, r5, #20
	mov	r2, #4
	add	r0, sp, #168
	bl	memcpy
	add	r1, r5, #24
	mov	r2, #4
	add	r0, sp, #172
	bl	memcpy
	ldr	r0, [r4, #8]
	ldr	r1, .L144+64
	mov	r2, #152
	bl	mem_free_debug
	ldr	r3, [sp, #164]
	str	r6, [sp, #176]
	cmp	r3, #8
	bne	.L102
	ldr	r1, .L144+72
	mov	r0, #-2147483648
	bl	convert_code_image_date_to_version_number
	ldr	r1, .L144+72
	mov	r5, r0
	mov	r0, #-2147483648
	bl	convert_code_image_time_to_edit_count
	ldr	r3, [sp, #168]
	cmp	r5, r3
	bne	.L103
	ldr	r3, [sp, #172]
	cmp	r0, r3
	ldreq	r0, .L144+76
	bne	.L103
	b	.L131
.L102:
	cmp	r3, #9
	bne	.L104
	ldr	r3, .L144+80
	ldr	r2, [sp, #168]
	ldr	r1, [r3, #80]
	cmp	r1, r2
	bne	.L103
	ldr	r2, [r3, #84]
	ldr	r3, [sp, #172]
	cmp	r2, r3
	bne	.L103
	ldr	r0, .L144+84
.L131:
	bl	Alert_Message
	mov	r3, #2
	str	r3, [sp, #176]
	b	.L103
.L104:
	ldr	r0, .L144+88
	bl	Alert_Message
.L103:
	ldr	r3, [sp, #176]
	cmp	r3, #0
	bne	.L105
	ldr	r3, .L144
	ldr	r2, [sp, #164]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	moveq	r3, #1
	streq	r3, [sp, #176]
.L105:
	ldr	r1, .L144+64
	mov	r2, #222
	mov	r0, #148
	bl	mem_malloc_debug
	mov	r1, #0
	mov	r2, #12
	mov	r5, r0
	add	r0, sp, #144
	bl	memset
	ldrb	r3, [sp, #144]	@ zero_extendqisi2
	ldrh	r2, [sp, #160]
	orr	r3, r3, #30
	strh	r2, [sp, #146]	@ movhi
	ldrh	r2, [sp, #162]
	strb	r3, [sp, #144]
	mov	r3, #15
	strb	r3, [sp, #145]
	mov	r3, #0
	strh	r2, [sp, #148]	@ movhi
	strh	r3, [sp, #150]	@ movhi
	strh	r3, [sp, #152]	@ movhi
	add	r1, sp, #144
	mov	r3, #25
	mov	r2, #12
	mov	r0, r5
	strh	r3, [sp, #154]	@ movhi
	bl	memcpy
	add	r0, r5, #12
	add	r1, sp, #176
	mov	r2, #4
	bl	memcpy
	ldr	r6, [sp, #176]
	add	r7, r5, #16
	cmp	r6, #0
	bne	.L106
	ldr	r0, .L144+92
	bl	Alert_Message
	mov	r1, r6
	mov	r2, #128
	ldr	r0, .L144+96
	bl	memset
	b	.L132
.L106:
	cmp	r6, #1
	bne	.L108
	ldr	r0, .L144+100
	bl	Alert_Message
.L132:
	mov	r0, r7
	ldr	r1, .L144+96
	mov	r2, #128
	bl	memcpy
	add	r7, r5, #144
	mov	r6, #148
	b	.L107
.L108:
	ldr	r0, .L144+104
	bl	Alert_Message
	ldr	r3, .L144
	ldr	r3, [r3, #0]
	sub	r3, r3, #8
	cmp	r3, #1
	bhi	.L123
	bl	CODE_DISTRIBUTION_stop_and_cleanup
.L123:
	mov	r6, #20
.L107:
	sub	r1, r6, #4
	mov	r0, r5
	bl	CRC_calculate_32bit_big_endian
	add	r1, sp, #184
	mov	r2, #4
	str	r0, [r1, #-4]!
	mov	r0, r7
	bl	memcpy
	ldr	r0, [r4, #16]
	mov	r1, r5
	mov	r2, r6
	mov	r3, #0
	bl	attempt_to_copy_packet_and_route_out_another_port
	mov	r0, r5
	ldr	r1, .L144+64
	ldr	r2, .L144+108
	b	.L135
.L101:
.LBE19:
.LBE18:
	cmp	r3, #25
	ldrne	r0, .L144+112
	bne	.L137
.LBB20:
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L144
	mov	r1, #1
	mov	r2, r6
	ldr	r0, [r3, #260]
	mov	r3, r6
	bl	xTimerGenericCommand
	ldr	r0, .L144+116
	bl	CODE_DISTRIBUTION_post_event
	ldr	r5, [r4, #8]
	mov	r2, #12
	mov	r1, r5
	add	r0, sp, #144
	bl	memcpy
	add	r0, sp, #180
	add	r1, r5, #12
	mov	r2, #4
	bl	memcpy
	ldr	r3, [sp, #180]
	cmp	r3, #1
	bhi	.L110
	cmp	r3, #0
	ldreq	r0, .L144+120
	ldrne	r0, .L144+124
	bl	Alert_Message
	mov	r2, #128
	add	r0, sp, #4
	add	r1, r5, #16
	bl	memcpy
	ldr	r2, .L144+96
	mov	r3, #0
.L113:
	add	r1, sp, #4
	ldrb	r1, [r3, r1]	@ zero_extendqisi2
	ldrb	r0, [r2, #0]	@ zero_extendqisi2
	mvn	r1, r1
	add	r3, r3, #1
	orr	r1, r1, r0
	cmp	r3, #128
	strb	r1, [r2], #1
	bne	.L113
	b	.L114
.L110:
	cmp	r3, #2
	ldreq	r0, .L144+128
	ldrne	r0, .L144+132
	bl	Alert_Message
.L114:
	ldr	r0, [r4, #8]
	ldr	r1, .L144+64
	ldr	r2, .L144+136
.L135:
	bl	mem_free_debug
	b	.L72
.L137:
.LBE20:
	bl	Alert_Message
.L72:
	add	sp, sp, #184
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L145:
	.align	2
.L144:
	.word	cdcs
	.word	.LC23
	.word	SerDrvrVars_s
	.word	8548
	.word	.LC24
	.word	.LC32
	.word	.LANCHOR0
	.word	.LC25
	.word	.LC26
	.word	360000
	.word	.LC27
	.word	.LC28
	.word	.LC31
	.word	.LC29
	.word	.LC30
	.word	12000
	.word	.LC1
	.word	1550
	.word	CS3000_APP_FILENAME
	.word	.LC33
	.word	tpmicro_comm
	.word	.LC34
	.word	.LC35
	.word	.LC36
	.word	cdcs+12
	.word	.LC37
	.word	.LC38
	.word	317
	.word	.LC43
	.word	4471
	.word	.LC39
	.word	.LC40
	.word	.LC41
	.word	.LC42
	.word	406
.LFE9:
	.size	process_incoming_packet, .-process_incoming_packet
	.section	.text.CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet,"ax",%progbits
	.align	2
	.global	CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet
	.type	CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet, %function
CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet:
.LFB10:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI6:
	mov	r4, r2
	sub	sp, sp, #24
.LCFI7:
	mov	r5, r0
	mov	r6, r1
	mov	r0, r2
	ldr	r1, .L148
	ldr	r2, .L148+4
	mov	r8, r3
	bl	mem_malloc_debug
	mov	r1, r6
	mov	r2, r4
	mov	r7, r0
	bl	memcpy
	ldr	r3, .L148+8
	mov	r2, #0
	stmia	sp, {r3, r8}
	ldr	r3, .L148+12
	mov	r1, sp
	ldr	r0, [r3, #0]
	mov	r3, r2
	str	r7, [sp, #8]
	str	r4, [sp, #12]
	str	r5, [sp, #16]
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L146
	ldr	r0, .L148
	bl	RemovePathFromFileName
	ldr	r2, .L148+16
	mov	r1, r0
	ldr	r0, .L148+20
	bl	Alert_Message_va
.L146:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L149:
	.align	2
.L148:
	.word	.LC1
	.word	1652
	.word	4369
	.word	CODE_DISTRIBUTION_task_queue
	.word	1670
	.word	.LC44
.LFE10:
	.size	CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet, .-CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_packet
	.section .rodata
	.set	.LANCHOR0,. + 0
.LC0:
	.byte	1
	.byte	3
	.byte	7
	.byte	15
	.byte	31
	.byte	63
	.byte	127
	.byte	-1
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/code_distribution_receive.c\000"
.LC2:
	.ascii	"CDCS: mids don't match?\000"
.LC3:
	.ascii	"CDCS: classes don't match?\000"
.LC4:
	.ascii	"CODE: unexpected mid\000"
.LC5:
	.ascii	"not re-transmitting: main mid wrong\000"
.LC6:
	.ascii	"not re-transmitting: tpmicro mid wrong\000"
.LC7:
	.ascii	"not re-transmitting: saved mid corrupt\000"
.LC8:
	.ascii	"not re-transmitting: date and time don't match\000"
.LC9:
	.ascii	"not re-transmitting: expected size & packets don't "
	.ascii	"match\000"
.LC10:
	.ascii	"not re-transmitting: memory ptr is NULL\000"
.LC11:
	.ascii	"not re-transmitting: crcs don't match\000"
.LC12:
	.ascii	"not a HUB re-transmission\000"
.LC13:
	.ascii	"is a HUB re-transmission\000"
.LC14:
	.ascii	"Code receipt memory block not available\000"
.LC15:
	.ascii	"Code Receipt: unexp packet class %u\000"
.LC16:
	.ascii	"MAIN CODE: receiving fill-in packets\000"
.LC17:
	.ascii	"MAIN CODE: receiving %d bytes (%d packets)\000"
.LC18:
	.ascii	"TPMICRO CODE: receiving fill-in packets\000"
.LC19:
	.ascii	"TPMICRO CODE: receiving %d bytes (%d packets)\000"
.LC20:
	.ascii	"Main App Firmware CRC failed! (saw %08x, exp %08x)\000"
.LC21:
	.ascii	"TP Micro Firmware CRC failed! (saw %08x, exp %08x)\000"
.LC22:
	.ascii	"Invalid MID in DSS\000"
.LC23:
	.ascii	"CODE RECEIPT: init packet ignored, already active\000"
.LC24:
	.ascii	"Code DISTRIB: class integrity failed\000"
.LC25:
	.ascii	"MAIN CODE: reception from hub completed\000"
.LC26:
	.ascii	"TPMICRO: reception from hub completed\000"
.LC27:
	.ascii	"HUB CODE RECEIPT: rcv start ptr NULL\000"
.LC28:
	.ascii	"HUB CODE RECEIPT: packet # rcvd > expected\000"
.LC29:
	.ascii	"MAIN CODE: reception completed\000"
.LC30:
	.ascii	"TPMICRO: reception completed\000"
.LC31:
	.ascii	"CODE RECEIPT: unexp packet number (saw %d; exp %d)\000"
.LC32:
	.ascii	"CODE RECEIPT: unexp MID (saw %d; exp %d)\000"
.LC33:
	.ascii	"HUB QUERY: already running main code\000"
.LC34:
	.ascii	"HUB QUERY: already running tpmicro code\000"
.LC35:
	.ascii	"QUERY: delivered UNEXP mode\000"
.LC36:
	.ascii	"QUERY: received none of it - including bitfield\000"
.LC37:
	.ascii	"QUERY: partial received - including bitfield\000"
.LC38:
	.ascii	"QUERY: fully received\000"
.LC39:
	.ascii	"reports none rcvd\000"
.LC40:
	.ascii	"reports partial received\000"
.LC41:
	.ascii	"reports fully rcvd\000"
.LC42:
	.ascii	"reports UNK??\000"
.LC43:
	.ascii	"UNK code distribution MID\000"
.LC44:
	.ascii	"CODE DISTRIB queue full! : %s, %u\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI0-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI1-.LFB5
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI4-.LFB9
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0xcc
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI6-.LFB10
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/code_distribution_receive.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xc3
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF11
	.byte	0x1
	.4byte	.LASF12
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x495
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x4a7
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x271
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x29e
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x19b
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.byte	0x4e
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x4f1
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x1
	.byte	0x65
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x141
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x612
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x66a
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST4
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB4
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB5
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI2
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB9
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI5
	.4byte	.LFE9
	.2byte	0x3
	.byte	0x7d
	.sleb128 204
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB10
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI7
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"all_hub_code_distribution_packets_received\000"
.LASF4:
	.ascii	"process_firmware_upgrade_after_last_packet_received"
	.ascii	"\000"
.LASF8:
	.ascii	"code_receipt_error_timer_callback\000"
.LASF10:
	.ascii	"CODE_DISTRIBUTION_make_a_copy_and_queue_incoming_pa"
	.ascii	"cket\000"
.LASF7:
	.ascii	"process_incoming_hub_distribution_query_packet_ack\000"
.LASF12:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/code_distribution_receive.c\000"
.LASF11:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"free_and_step_on\000"
.LASF9:
	.ascii	"process_incoming_packet\000"
.LASF6:
	.ascii	"process_and_respond_to_incoming_hub_distribution_qu"
	.ascii	"ery_packet\000"
.LASF3:
	.ascii	"process_incoming_code_download_init_packet\000"
.LASF5:
	.ascii	"process_incoming_code_download_data_packet\000"
.LASF0:
	.ascii	"set_bit_for_rcvd_packet\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
