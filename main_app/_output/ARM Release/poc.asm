	.file	"poc.c"
	.text
.Ltext0:
	.section	.text.nm_POC_set_budget_calculation_percent_of_et,"ax",%progbits
	.align	2
	.global	nm_POC_set_budget_calculation_percent_of_et
	.type	nm_POC_set_budget_calculation_percent_of_et, %function
nm_POC_set_budget_calculation_percent_of_et:
.LFB14:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	sub	sp, sp, #44
.LCFI1:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #300
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #232
	mov	r1, #80
	str	r3, [sp, #32]
	mov	r3, #11
	stmib	sp, {r1, r2}
	ldr	r2, .L3
	str	r3, [sp, #36]
	ldr	r3, .L3+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #244
	mov	r3, #10
	mov	r2, lr
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	cmp	r0, #1
	ldreq	r3, .L3+8
	streq	r0, [r3, #0]
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L4:
	.align	2
.L3:
	.word	49399
	.word	.LC0
	.word	g_BUDGET_using_et_calculate_budget
.LFE14:
	.size	nm_POC_set_budget_calculation_percent_of_et, .-nm_POC_set_budget_calculation_percent_of_et
	.section	.text.nm_POC_set_budget_gallons_entry_option,"ax",%progbits
	.align	2
	.global	nm_POC_set_budget_gallons_entry_option
	.type	nm_POC_set_budget_gallons_entry_option, %function
nm_POC_set_budget_gallons_entry_option:
.LFB13:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	sub	sp, sp, #44
.LCFI3:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L7
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	ip, #0
	str	r3, [sp, #28]
	add	r3, r0, #232
	str	r3, [sp, #32]
	mov	r3, #10
	str	r3, [sp, #36]
	ldr	r3, .L7+4
	mov	r4, r1
	mov	r1, #2
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #240
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L5
	cmp	r4, #2
	ldreq	r3, .L7+8
	streq	r0, [r3, #0]
.L5:
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L8:
	.align	2
.L7:
	.word	49398
	.word	.LC1
	.word	g_BUDGET_using_et_calculate_budget
.LFE13:
	.size	nm_POC_set_budget_gallons_entry_option, .-nm_POC_set_budget_gallons_entry_option
	.section	.text.nm_POC_set_bypass_number_of_levels,"ax",%progbits
	.align	2
	.global	nm_POC_set_bypass_number_of_levels
	.type	nm_POC_set_bypass_number_of_levels, %function
nm_POC_set_bypass_number_of_levels:
.LFB11:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI4:
	sub	sp, sp, #44
.LCFI5:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L10
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #232
	str	r3, [sp, #32]
	mov	r3, #9
	str	r3, [sp, #36]
	ldr	r3, .L10+4
	mov	ip, #2
	mov	r1, #3
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #164
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L11:
	.align	2
.L10:
	.word	49222
	.word	.LC2
.LFE11:
	.size	nm_POC_set_bypass_number_of_levels, .-nm_POC_set_bypass_number_of_levels
	.section	.text.nm_POC_set_poc_type,"ax",%progbits
	.align	2
	.global	nm_POC_set_poc_type
	.type	nm_POC_set_poc_type, %function
nm_POC_set_poc_type:
.LFB5:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI6:
	sub	sp, sp, #44
.LCFI7:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L14
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	ip, #10
	str	r3, [sp, #28]
	add	r3, r0, #232
	str	r3, [sp, #32]
	mov	r3, #3
	str	r3, [sp, #36]
	ldr	r3, .L14+4
	mov	r4, r1
	mov	r1, #12
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #80
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L12
	add	sp, sp, #44
	ldmfd	sp!, {r4, lr}
	b	POC_PRESERVES_request_a_resync
.L12:
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L15:
	.align	2
.L14:
	.word	49207
	.word	.LC3
.LFE5:
	.size	nm_POC_set_poc_type, .-nm_POC_set_poc_type
	.section	.text.nm_POC_set_poc_usage,"ax",%progbits
	.align	2
	.global	nm_POC_set_poc_usage
	.type	nm_POC_set_poc_usage, %function
nm_POC_set_poc_usage:
.LFB4:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI8:
	sub	sp, sp, #44
.LCFI9:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #2
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #232
	mov	r1, #1
	str	r3, [sp, #32]
	mov	r3, #6
	stmib	sp, {r1, r2}
	ldr	r2, .L18
	str	r3, [sp, #36]
	ldr	r3, .L18+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #112
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L16
	add	sp, sp, #44
	ldr	lr, [sp], #4
	b	POC_PRESERVES_request_a_resync
.L16:
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L19:
	.align	2
.L18:
	.word	49206
	.word	.LC4
.LFE4:
	.size	nm_POC_set_poc_usage, .-nm_POC_set_poc_usage
	.section	.text.nm_POC_set_box_index,"ax",%progbits
	.align	2
	.global	nm_POC_set_box_index
	.type	nm_POC_set_box_index, %function
nm_POC_set_box_index:
.LFB1:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI10:
	sub	sp, sp, #44
.LCFI11:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L22
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	ip, #0
	str	r3, [sp, #28]
	add	r3, r0, #232
	str	r3, [sp, #32]
	mov	r3, #2
	str	r3, [sp, #36]
	ldr	r3, .L22+4
	mov	r4, r1
	mov	r1, #11
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #76
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L20
	add	sp, sp, #44
	ldmfd	sp!, {r4, lr}
	b	POC_PRESERVES_request_a_resync
.L20:
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L23:
	.align	2
.L22:
	.word	49204
	.word	.LC5
.LFE1:
	.size	nm_POC_set_box_index, .-nm_POC_set_box_index
	.section	.text.nm_POC_set_has_pump_attached,"ax",%progbits
	.align	2
	.global	nm_POC_set_has_pump_attached
	.type	nm_POC_set_has_pump_attached, %function
nm_POC_set_has_pump_attached:
.LFB16:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI12:
	sub	sp, sp, #36
.LCFI13:
	str	r2, [sp, #0]
	ldr	r2, .L25
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #248
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #232
	str	r3, [sp, #24]
	mov	r3, #15
	str	r3, [sp, #28]
	ldr	r3, .L25+4
	str	r3, [sp, #32]
	mov	r3, #1
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L26:
	.align	2
.L25:
	.word	49402
	.word	.LC6
.LFE16:
	.size	nm_POC_set_has_pump_attached, .-nm_POC_set_has_pump_attached
	.section	.text.nm_POC_set_show_this_poc,"ax",%progbits
	.align	2
	.global	nm_POC_set_show_this_poc
	.type	nm_POC_set_show_this_poc, %function
nm_POC_set_show_this_poc:
.LFB3:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI14:
	sub	sp, sp, #36
.LCFI15:
	str	r2, [sp, #0]
	ldr	r2, .L29
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #96
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #232
	str	r3, [sp, #24]
	mov	r3, #5
	str	r3, [sp, #28]
	ldr	r3, .L29+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L27
	add	sp, sp, #36
	ldr	lr, [sp], #4
	b	POC_PRESERVES_request_a_resync
.L27:
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L30:
	.align	2
.L29:
	.word	49258
	.word	.LC7
.LFE3:
	.size	nm_POC_set_show_this_poc, .-nm_POC_set_show_this_poc
	.section	.text.nm_POC_set_budget_gallons,"ax",%progbits
	.align	2
	.type	nm_POC_set_budget_gallons, %function
nm_POC_set_budget_gallons:
.LFB15:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #23
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI16:
	mov	r5, r0
	sub	sp, sp, #76
.LCFI17:
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	bhi	.L32
.LBB37:
	add	r6, sp, #44
	add	r3, r1, #1
	str	r3, [sp, #0]
	mov	r1, #32
	ldr	r2, .L36
	ldr	r3, .L36+4
	mov	r0, r6
	bl	snprintf
	bl	NETWORK_CONFIG_get_water_units
	ldr	r3, .L36+8
	ldr	r2, .L36+12
	add	r1, r4, #64
	add	r1, r5, r1, asl #2
	str	r6, [sp, #40]
	cmp	r0, #1
	movne	r2, r3
	add	r4, r2, r4
	ldr	r2, [sp, #100]
	ldr	r3, .L36+16
	str	r2, [sp, #16]
	ldr	r2, [sp, #104]
	str	r3, [sp, #0]
	str	r2, [sp, #20]
	ldr	r2, [sp, #108]
	mov	r3, #0
	str	r2, [sp, #24]
	ldr	r2, [sp, #112]
	mov	r0, r5
	str	r2, [sp, #28]
	add	r2, r5, #232
	str	r2, [sp, #32]
	mov	r2, #14
	str	r2, [sp, #36]
	mov	r2, r8
	stmib	sp, {r3, r7}
	str	r4, [sp, #12]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L31
.L32:
.LBE37:
	ldr	r0, .L36+20
	ldr	r1, .L36+24
	bl	Alert_index_out_of_range_with_filename
.L31:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L37:
	.align	2
.L36:
	.word	.LC8
	.word	.LC9
	.word	49428
	.word	49552
	.word	100000000
	.word	.LC10
	.word	1441
.LFE15:
	.size	nm_POC_set_budget_gallons, .-nm_POC_set_budget_gallons
	.section	.text.nm_poc_structure_updater,"ax",%progbits
	.align	2
	.type	nm_poc_structure_updater, %function
nm_poc_structure_updater:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI18:
	mov	r9, r0
	bl	FLOWSENSE_get_controller_index
	cmp	r9, #5
	mov	r5, r0
	bne	.L39
	ldr	r0, .L62
	mov	r1, r9
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	Alert_Message_va
.L39:
	ldr	r0, .L62+4
	mov	r1, #5
	mov	r2, r9
	bl	Alert_Message_va
	cmp	r9, #0
	bne	.L40
	ldr	r0, .L62+8
	bl	nm_ListGetFirst
	b	.L61
.L42:
	add	r0, r6, #11
	add	r1, sl, r7
	add	r0, r4, r0, asl #4
	mov	r2, #12
	add	r6, r6, #1
	bl	memcpy
	cmp	r6, #3
	str	r9, [r8, #188]
	add	r7, r7, #12
	add	r8, r8, #16
	bne	.L42
	mov	r1, #0
	mov	r2, #36
	mov	r0, sl
	bl	memset
	mov	r1, #0
	add	r0, r4, #168
	mov	r2, #8
	bl	memset
	mvn	r3, #0
	str	r3, [r4, #232]
	ldr	r0, .L62+8
	mov	r1, r4
	bl	nm_ListGetNext
.L61:
	cmp	r0, #0
	mov	r4, r0
	beq	.L43
	mov	r7, #0
	mov	r8, r0
	mov	r6, r7
	add	sl, r0, #128
	b	.L42
.L40:
	cmp	r9, #1
	bne	.L44
.L43:
	ldr	r0, .L62+8
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L45
.L46:
	str	r4, [r1, #236]
	ldr	r0, .L62+8
	bl	nm_ListGetNext
	mov	r1, r0
.L45:
	cmp	r1, #0
	bne	.L46
	b	.L49
.L44:
	cmp	r9, #2
	beq	.L49
	cmp	r9, #3
	bne	.L50
.L49:
	ldr	r0, .L62+8
	bl	nm_ListGetFirst
	mov	r8, #1
	mov	sl, #11
	mov	r4, r0
	b	.L51
.L53:
	mov	r1, #0
	mov	r2, r1
	add	r7, r4, #224
	mov	r0, r4
	mov	r3, #11
	stmia	sp, {r5, r8}
	str	r7, [sp, #8]
	bl	nm_POC_set_budget_gallons_entry_option
	mov	r0, r4
	mov	r1, #80
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r8}
	str	r7, [sp, #8]
	bl	nm_POC_set_budget_calculation_percent_of_et
	mov	r6, #0
.L52:
	mov	r2, #0
	mov	r1, r6
	mov	r0, r4
	mov	r3, r2
	add	r6, r6, #1
	str	sl, [sp, #0]
	stmib	sp, {r5, r8}
	str	r7, [sp, #12]
	bl	nm_POC_set_budget_gallons
	cmp	r6, #24
	bne	.L52
	mov	r1, r4
	ldr	r0, .L62+8
	bl	nm_ListGetNext
	mov	r4, r0
.L51:
	cmp	r4, #0
	bne	.L53
	b	.L54
.L50:
	cmp	r9, #4
	bne	.L55
.L54:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	beq	.L38
	ldr	r0, .L62+8
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r4, r0
	b	.L57
.L58:
	add	r7, r4, #224
	ldr	r1, [r4, #80]
	ldr	r0, .L62+12
	mov	r2, r7
	bl	Alert_Message_va
	mov	r0, r4
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r6, r7}
	bl	nm_POC_set_has_pump_attached
	ldr	r0, .L62+8
	mov	r1, r4
	bl	nm_ListGetNext
	mov	r4, r0
.L57:
	cmp	r4, #0
	bne	.L58
	b	.L38
.L55:
	ldr	r0, .L62+16
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	Alert_Message
.L38:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L63:
	.align	2
.L62:
	.word	.LC11
	.word	.LC12
	.word	.LANCHOR0
	.word	.LC13
	.word	.LC14
.LFE19:
	.size	nm_poc_structure_updater, .-nm_poc_structure_updater
	.section	.text.nm_POC_set_fm_type.part.1,"ax",%progbits
	.align	2
	.type	nm_POC_set_fm_type.part.1, %function
nm_POC_set_fm_type.part.1:
.LFB72:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI19:
	sub	sp, sp, #76
.LCFI20:
	add	r6, sp, #44
	mov	r5, r0
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	str	r1, [sp, #0]
	mov	r0, r6
	mov	r1, #32
	ldr	r2, .L65
	ldr	r3, .L65+4
	bl	snprintf
	ldr	r2, [sp, #100]
	add	r1, r4, #10
	str	r2, [sp, #16]
	ldr	r2, [sp, #104]
	mov	r3, #17
	str	r2, [sp, #20]
	ldr	r2, [sp, #108]
	add	r4, r4, #49152
	str	r2, [sp, #24]
	ldr	r2, [sp, #112]
	str	r3, [sp, #0]
	str	r2, [sp, #28]
	add	r2, r5, #232
	str	r2, [sp, #32]
	mov	r2, #8
	mov	r3, #0
	add	r4, r4, #60
	str	r2, [sp, #36]
	mov	r0, r5
	add	r1, r5, r1, asl #4
	mov	r2, r8
	stmib	sp, {r3, r7}
	str	r4, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L66:
	.align	2
.L65:
	.word	.LC15
	.word	.LC16
.LFE72:
	.size	nm_POC_set_fm_type.part.1, .-nm_POC_set_fm_type.part.1
	.section	.text.nm_POC_set_kvalue.part.2,"ax",%progbits
	.align	2
	.type	nm_POC_set_kvalue.part.2, %function
nm_POC_set_kvalue.part.2:
.LFB73:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI21:
	sub	sp, sp, #76
.LCFI22:
	add	r6, sp, #44
	mov	r4, r0
	mov	r5, r1
	mov	r8, r2
	mov	r7, r3
	str	r1, [sp, #0]
	mov	r0, r6
	mov	r1, #32
	ldr	r2, .L68
	ldr	r3, .L68+4
	bl	snprintf
	ldr	r3, .L68+8
	ldr	lr, .L68+12
	add	r1, r4, r5, asl #4
	stmia	sp, {r3, lr}
	ldr	r3, [sp, #100]
	add	r5, r5, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	add	r5, r5, #63
	str	r3, [sp, #20]
	ldr	r3, [sp, #108]
	mov	r0, r4
	str	r3, [sp, #24]
	ldr	r3, [sp, #112]
	add	r1, r1, #164
	str	r3, [sp, #28]
	add	r3, r4, #232
	str	r3, [sp, #32]
	mov	r3, #8
	str	r3, [sp, #36]
	mov	r2, r8
	mov	r3, #0
	str	r7, [sp, #8]
	str	r5, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L69:
	.align	2
.L68:
	.word	.LC17
	.word	.LC16
	.word	80000000
	.word	100000
.LFE73:
	.size	nm_POC_set_kvalue.part.2, .-nm_POC_set_kvalue.part.2
	.section	.text.nm_POC_set_reed_switch.part.3,"ax",%progbits
	.align	2
	.type	nm_POC_set_reed_switch.part.3, %function
nm_POC_set_reed_switch.part.3:
.LFB74:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI23:
	sub	sp, sp, #76
.LCFI24:
	add	r6, sp, #44
	mov	r4, r0
	mov	r5, r1
	mov	r8, r2
	mov	r7, r3
	str	r1, [sp, #0]
	mov	r0, r6
	mov	r1, #32
	ldr	r2, .L71
	ldr	r3, .L71+4
	bl	snprintf
	ldr	r2, [sp, #100]
	add	r1, r4, r5, asl #4
	str	r2, [sp, #16]
	ldr	r2, [sp, #104]
	mov	r3, #1
	str	r2, [sp, #20]
	ldr	r2, [sp, #108]
	add	r5, r5, #49152
	str	r2, [sp, #24]
	ldr	r2, [sp, #112]
	str	r3, [sp, #0]
	str	r2, [sp, #28]
	add	r2, r4, #232
	str	r2, [sp, #32]
	mov	r2, #8
	mov	r3, #0
	add	r5, r5, #130
	str	r2, [sp, #36]
	mov	r0, r4
	add	r1, r1, #172
	mov	r2, r8
	stmib	sp, {r3, r7}
	str	r5, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L72:
	.align	2
.L71:
	.word	.LC18
	.word	.LC16
.LFE74:
	.size	nm_POC_set_reed_switch.part.3, .-nm_POC_set_reed_switch.part.3
	.section	.text.nm_get_decoder_serial_number_support.part.4,"ax",%progbits
	.align	2
	.type	nm_get_decoder_serial_number_support.part.4, %function
nm_get_decoder_serial_number_support.part.4:
.LFB75:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI25:
	ldr	r2, .L74
	sub	sp, sp, #72
.LCFI26:
	add	r6, sp, #24
	add	r3, r1, #1
	mov	r5, r0
	mov	r4, r1
	str	r3, [sp, #0]
	mov	r0, r6
	mov	r1, #48
	ldr	r3, .L74+4
	bl	snprintf
	ldr	r1, .L74+8
	mov	r3, #0
	stmib	sp, {r1, r3}
	str	r3, [sp, #0]
	ldr	r3, .L74+12
	add	r2, r4, #21
	str	r3, [sp, #12]
	add	r3, r5, #224
	str	r3, [sp, #16]
	mov	r0, r5
	mov	r1, r4
	add	r2, r5, r2, asl #2
	mov	r3, #3
	str	r6, [sp, #20]
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, pc}
.L75:
	.align	2
.L74:
	.word	.LC8
	.word	.LC19
	.word	2097151
	.word	nm_POC_set_decoder_serial_number
.LFE75:
	.size	nm_get_decoder_serial_number_support.part.4, .-nm_get_decoder_serial_number_support.part.4
	.section	.text.nm_POC_set_mv_type.part.5,"ax",%progbits
	.align	2
	.type	nm_POC_set_mv_type.part.5, %function
nm_POC_set_mv_type.part.5:
.LFB76:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI27:
	sub	sp, sp, #76
.LCFI28:
	add	r6, sp, #44
	mov	r5, r0
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	str	r1, [sp, #0]
	ldr	r2, .L78
	mov	r1, #32
	ldr	r3, .L78+4
	mov	r0, r6
	bl	snprintf
	mov	r3, #1
	str	r3, [sp, #0]
	stmib	sp, {r3, r7}
	ldr	r3, [sp, #100]
	add	r1, r4, #28
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	add	r4, r4, #49152
	str	r3, [sp, #20]
	ldr	r3, [sp, #108]
	add	r4, r4, #57
	str	r3, [sp, #24]
	ldr	r3, [sp, #112]
	mov	r0, r5
	str	r3, [sp, #28]
	add	r3, r5, #232
	str	r3, [sp, #32]
	mov	r3, #7
	str	r3, [sp, #36]
	add	r1, r5, r1, asl #2
	mov	r2, r8
	mov	r3, #0
	str	r4, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L76
	bl	POC_PRESERVES_request_a_resync
.L76:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L79:
	.align	2
.L78:
	.word	.LC8
	.word	.LC20
.LFE76:
	.size	nm_POC_set_mv_type.part.5, .-nm_POC_set_mv_type.part.5
	.section	.text.nm_POC_set_decoder_serial_number,"ax",%progbits
	.align	2
	.global	nm_POC_set_decoder_serial_number
	.type	nm_POC_set_decoder_serial_number, %function
nm_POC_set_decoder_serial_number:
.LFB2:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI29:
	mov	r7, r3
	sub	r3, r1, #1
	cmp	r3, #2
	sub	sp, sp, #76
.LCFI30:
	mov	r5, r0
	mov	r4, r1
	mov	r8, r2
	bhi	.L81
.LBB40:
	add	r6, sp, #44
	str	r1, [sp, #0]
	ldr	r2, .L83
	mov	r1, #32
	ldr	r3, .L83+4
	mov	r0, r6
	bl	snprintf
	ldr	r2, [sp, #100]
	ldr	r3, .L83+8
	str	r2, [sp, #16]
	ldr	r2, [sp, #104]
	add	r1, r4, #20
	str	r2, [sp, #20]
	ldr	r2, [sp, #108]
	add	r4, r4, #49152
	str	r2, [sp, #24]
	ldr	r2, [sp, #112]
	str	r3, [sp, #0]
	str	r2, [sp, #28]
	add	r2, r5, #232
	str	r2, [sp, #32]
	mov	r2, #4
	mov	r3, #0
	str	r2, [sp, #36]
	add	r4, r4, #126
	mov	r0, r5
	add	r1, r5, r1, asl #2
	mov	r2, r8
	stmib	sp, {r3, r7}
	str	r4, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L80
	bl	POC_PRESERVES_request_a_resync
	b	.L80
.L81:
.LBE40:
	ldr	r0, .L83+12
	ldr	r1, .L83+16
	bl	Alert_index_out_of_range_with_filename
.L80:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L84:
	.align	2
.L83:
	.word	.LC8
	.word	.LC19
	.word	2097151
	.word	.LC10
	.word	486
.LFE2:
	.size	nm_POC_set_decoder_serial_number, .-nm_POC_set_decoder_serial_number
	.section	.text.nm_POC_set_offset.part.7,"ax",%progbits
	.align	2
	.type	nm_POC_set_offset.part.7, %function
nm_POC_set_offset.part.7:
.LFB78:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI31:
	sub	sp, sp, #76
.LCFI32:
	add	r6, sp, #44
	mov	r4, r0
	mov	r5, r1
	mov	r8, r2
	mov	r7, r3
	str	r1, [sp, #0]
	mov	r0, r6
	mov	r1, #32
	ldr	r2, .L86
	ldr	r3, .L86+4
	bl	snprintf
	ldr	r3, .L86+8
	ldr	ip, .L86+12
	add	r1, r4, r5, asl #4
	stmia	sp, {r3, ip}
	ldr	r3, [sp, #100]
	add	r5, r5, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	add	r5, r5, #66
	str	r3, [sp, #20]
	ldr	r3, [sp, #108]
	mov	r0, r4
	str	r3, [sp, #24]
	ldr	r3, [sp, #112]
	add	r1, r1, #168
	str	r3, [sp, #28]
	add	r3, r4, #232
	str	r3, [sp, #32]
	mov	r3, #8
	str	r3, [sp, #36]
	mov	r2, r8
	ldr	r3, .L86+16
	str	r7, [sp, #8]
	str	r5, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_int32_with_32_bit_change_bits_group
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L87:
	.align	2
.L86:
	.word	.LC21
	.word	.LC16
	.word	1000000
	.word	20000
	.word	-1000000
.LFE78:
	.size	nm_POC_set_offset.part.7, .-nm_POC_set_offset.part.7
	.section	.text.nm_POC_set_GID_irrigation_system,"ax",%progbits
	.align	2
	.global	nm_POC_set_GID_irrigation_system
	.type	nm_POC_set_GID_irrigation_system, %function
nm_POC_set_GID_irrigation_system:
.LFB12:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI33:
	sub	sp, sp, #44
.LCFI34:
	mov	r4, r0
	add	r5, sp, #76
	mov	r0, #0
	mov	r7, r1
	mov	r6, r3
	ldmia	r5, {r5, r9, sl}
	mov	r8, r2
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_group_ID
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #8]
	ldr	r3, .L91
	ldr	r2, .L91+4
	str	r3, [sp, #12]
	add	r3, r4, #232
	str	r3, [sp, #32]
	str	r2, [sp, #40]
	mov	r3, #1
	add	r1, r4, #72
	mov	r2, r7
	str	r6, [sp, #16]
	str	r5, [sp, #20]
	str	r9, [sp, #24]
	str	sl, [sp, #28]
	str	r3, [sp, #36]
	str	r0, [sp, #4]
	mov	r0, r4
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L88
.LBB44:
	ldr	r3, .L91+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L91+12
	ldr	r3, .L91+16
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r7
	bl	SYSTEM_get_group_with_this_GID
	cmp	r8, #1
	mov	r7, r0
	bne	.L90
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r4, r0
	mov	r0, r7
	bl	nm_GROUP_get_name
	mov	r2, r6
	mov	r3, r5
	mov	r1, r0
	mov	r0, r4
	bl	Alert_poc_assigned_to_mainline
.L90:
	ldr	r3, .L91+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE44:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LBB45:
	b	POC_PRESERVES_request_a_resync
.L88:
.LBE45:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L92:
	.align	2
.L91:
	.word	49282
	.word	.LC22
	.word	list_system_recursive_MUTEX
	.word	.LC10
	.word	1246
.LFE12:
	.size	nm_POC_set_GID_irrigation_system, .-nm_POC_set_GID_irrigation_system
	.section	.text.nm_POC_set_name,"ax",%progbits
	.align	2
	.global	nm_POC_set_name
	.type	nm_POC_set_name, %function
nm_POC_set_name:
.LFB0:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI35:
	ldr	ip, [sp, #24]
	str	ip, [sp, #0]
	ldr	ip, [sp, #28]
	str	ip, [sp, #4]
	ldr	ip, [sp, #32]
	str	ip, [sp, #8]
	add	ip, r0, #232
	str	ip, [sp, #12]
	mov	ip, #0
	str	ip, [sp, #16]
	bl	SHARED_set_name_32_bit_change_bits
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.LFE0:
	.size	nm_POC_set_name, .-nm_POC_set_name
	.section	.text.nm_POC_set_default_values,"ax",%progbits
	.align	2
	.type	nm_POC_set_default_values, %function
nm_POC_set_default_values:
.LFB22:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI36:
	sub	sp, sp, #64
.LCFI37:
	mov	r4, r0
	mov	r7, r1
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #0
	mov	r5, r0
	beq	.L95
	ldr	r8, .L102
	add	r6, r4, #224
	ldr	r1, [r8, #0]
	mov	r0, r6
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r8, #0]
	add	r0, r4, #228
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r8, #0]
	add	r0, r4, #236
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r8, #0]
	add	r0, r4, #232
	bl	SHARED_set_all_32_bit_change_bits
	mov	r1, #48
	ldr	r2, .L102+4
	add	r3, r5, #65
	add	r0, sp, #16
	bl	snprintf
	mov	r0, r4
	add	r1, sp, #16
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	bl	nm_POC_set_name
	mov	r3, #11
	mov	r0, r4
	mov	r1, r5
	mov	r2, #0
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	bl	nm_POC_set_box_index
	cmp	r5, #0
	ldreq	r3, [r4, #224]
	mov	r1, #0
	mov	r2, r1
	mov	r0, r4
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	mov	r8, #0
	mov	sl, #11
	orreq	r3, r3, #4
	streq	r3, [r4, #224]
	mov	r3, #11
	bl	nm_POC_set_show_this_poc
	mov	r0, r4
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	bl	nm_POC_set_poc_usage
	mov	r0, r4
	mov	r1, #10
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	bl	nm_POC_set_poc_type
.L97:
	add	r8, r8, #1
	mov	r2, #0
	mov	r3, r2
	mov	r0, r4
	mov	r1, r8
	str	sl, [sp, #0]
	stmib	sp, {r5, r7}
	str	r6, [sp, #12]
	bl	nm_POC_set_decoder_serial_number
	mov	r0, r4
	mov	r1, r8
	mov	r2, #1
	mov	r3, #0
	str	sl, [sp, #0]
	stmib	sp, {r5, r7}
	str	r6, [sp, #12]
	bl	nm_POC_set_mv_type.part.5
	mov	r2, #0
	mov	r3, r2
	mov	r0, r4
	mov	r1, r8
	str	sl, [sp, #0]
	stmib	sp, {r5, r7}
	str	r6, [sp, #12]
	bl	nm_POC_set_fm_type.part.1
	mov	r0, r4
	mov	r1, r8
	ldr	r2, .L102+8
	mov	r3, #0
	str	sl, [sp, #0]
	stmib	sp, {r5, r7}
	str	r6, [sp, #12]
	bl	nm_POC_set_kvalue.part.2
	mov	r0, r4
	mov	r1, r8
	ldr	r2, .L102+12
	mov	r3, #0
	str	sl, [sp, #0]
	stmib	sp, {r5, r7}
	str	r6, [sp, #12]
	bl	nm_POC_set_offset.part.7
	cmp	r8, #3
	bne	.L97
	mov	r1, #2
	mov	r2, #0
	mov	r3, #11
	mov	r0, r4
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	bl	nm_POC_set_bypass_number_of_levels
	mov	r0, #0
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_group_ID
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	mov	r8, #0
	mov	sl, #11
	mov	r1, r0
	mov	r0, r4
	bl	nm_POC_set_GID_irrigation_system
	mov	r1, #0
	mov	r2, r1
	mov	r0, r4
	mov	r3, #11
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	bl	nm_POC_set_budget_gallons_entry_option
	mov	r0, r4
	mov	r1, #80
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	bl	nm_POC_set_budget_calculation_percent_of_et
.L98:
	mov	r2, #0
	mov	r1, r8
	mov	r0, r4
	mov	r3, r2
	add	r8, r8, #1
	str	sl, [sp, #0]
	stmib	sp, {r5, r7}
	str	r6, [sp, #12]
	bl	nm_POC_set_budget_gallons
	cmp	r8, #24
	bne	.L98
	mov	r0, r4
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	bl	nm_POC_set_has_pump_attached
	b	.L94
.L95:
	ldr	r0, .L102+16
	ldr	r1, .L102+20
	bl	Alert_func_call_with_null_ptr_with_filename
.L94:
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L103:
	.align	2
.L102:
	.word	list_poc_recursive_MUTEX
	.word	.LC23
	.word	100000
	.word	20000
	.word	.LC24
	.word	901
.LFE22:
	.size	nm_POC_set_default_values, .-nm_POC_set_default_values
	.section	.text.init_file_POC,"ax",%progbits
	.align	2
	.global	init_file_POC
	.type	init_file_POC, %function
init_file_POC:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI38:
	ldr	r3, .L105
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, .L105+4
	ldr	r1, .L105+8
	ldr	r3, [r3, #0]
	mov	r2, #5
	str	r3, [sp, #4]
	ldr	r3, .L105+12
	str	r3, [sp, #8]
	ldr	r3, .L105+16
	str	r3, [sp, #12]
	ldr	r3, .L105+20
	str	r3, [sp, #16]
	mov	r3, #11
	str	r3, [sp, #20]
	ldr	r3, .L105+24
	bl	FLASH_FILE_initialize_list_and_find_or_create_group_list_file
	add	sp, sp, #24
	ldmfd	sp!, {pc}
.L106:
	.align	2
.L105:
	.word	.LANCHOR2
	.word	list_poc_recursive_MUTEX
	.word	.LANCHOR1
	.word	nm_poc_structure_updater
	.word	nm_POC_set_default_values
	.word	.LANCHOR3
	.word	.LANCHOR0
.LFE20:
	.size	init_file_POC, .-init_file_POC
	.section	.text.save_file_POC,"ax",%progbits
	.align	2
	.global	save_file_POC
	.type	save_file_POC, %function
save_file_POC:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #352
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI39:
	ldr	r1, .L108
	str	r3, [sp, #0]
	ldr	r3, .L108+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, #5
	str	r3, [sp, #4]
	mov	r3, #11
	str	r3, [sp, #8]
	ldr	r3, .L108+8
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L109:
	.align	2
.L108:
	.word	.LANCHOR1
	.word	list_poc_recursive_MUTEX
	.word	.LANCHOR0
.LFE21:
	.size	save_file_POC, .-save_file_POC
	.section	.text.POC_get_index_using_ptr_to_poc_struct,"ax",%progbits
	.align	2
	.global	POC_get_index_using_ptr_to_poc_struct
	.type	POC_get_index_using_ptr_to_poc_struct, %function
POC_get_index_using_ptr_to_poc_struct:
.LFB32:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L115
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI40:
	ldr	r2, .L115+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L115+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L115+12
	bl	nm_ListGetFirst
	mov	r5, #0
	ldr	r6, .L115+12
	mov	r1, r0
	b	.L111
.L113:
	cmp	r1, r4
	beq	.L112
	ldr	r0, .L115+12
	bl	nm_ListGetNext
	add	r5, r5, #1
	mov	r1, r0
.L111:
	ldr	r3, [r6, #8]
	cmp	r5, r3
	bcc	.L113
	mov	r5, #0
.L112:
	ldr	r3, .L115
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L116:
	.align	2
.L115:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	1946
	.word	.LANCHOR0
.LFE32:
	.size	POC_get_index_using_ptr_to_poc_struct, .-POC_get_index_using_ptr_to_poc_struct
	.section	.text.nm_POC_get_pointer_to_terminal_poc_with_this_box_index_0,"ax",%progbits
	.align	2
	.global	nm_POC_get_pointer_to_terminal_poc_with_this_box_index_0
	.type	nm_POC_get_pointer_to_terminal_poc_with_this_box_index_0, %function
nm_POC_get_pointer_to_terminal_poc_with_this_box_index_0:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI41:
	mov	r4, r0
	ldr	r0, .L123
	bl	nm_ListGetFirst
	b	.L122
.L121:
	ldr	r3, [r1, #76]
	cmp	r3, r4
	bne	.L119
	ldr	r3, [r1, #80]
	cmp	r3, #10
	beq	.L120
.L119:
	ldr	r0, .L123
	bl	nm_ListGetNext
.L122:
	cmp	r0, #0
	mov	r1, r0
	bne	.L121
.L120:
	mov	r0, r1
	ldmfd	sp!, {r4, pc}
.L124:
	.align	2
.L123:
	.word	.LANCHOR0
.LFE33:
	.size	nm_POC_get_pointer_to_terminal_poc_with_this_box_index_0, .-nm_POC_get_pointer_to_terminal_poc_with_this_box_index_0
	.section	.text.nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0,"ax",%progbits
	.align	2
	.global	nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0
	.type	nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0, %function
nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0:
.LFB34:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI42:
	mov	r5, r0
	ldr	r0, .L131
	mov	r4, r1
	bl	nm_ListGetFirst
	b	.L130
.L129:
	ldr	r3, [r1, #76]
	cmp	r3, r5
	bne	.L127
	ldr	r3, [r1, #80]
	cmp	r3, #12
	bne	.L127
	cmp	r4, #0
	beq	.L128
	ldr	r3, [r1, #96]
	cmp	r3, #0
	bne	.L128
.L127:
	ldr	r0, .L131
	bl	nm_ListGetNext
.L130:
	cmp	r0, #0
	mov	r1, r0
	bne	.L129
.L128:
	mov	r0, r1
	ldmfd	sp!, {r4, r5, pc}
.L132:
	.align	2
.L131:
	.word	.LANCHOR0
.LFE34:
	.size	nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0, .-nm_POC_get_pointer_to_bypass_poc_with_this_box_index_0
	.section	.text.nm_POC_get_pointer_to_poc_with_this_gid_and_box_index,"ax",%progbits
	.align	2
	.global	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	.type	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index, %function
nm_POC_get_pointer_to_poc_with_this_gid_and_box_index:
.LFB35:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI43:
	mov	r5, r0
	ldr	r0, .L139
	mov	r4, r1
	bl	nm_ListGetFirst
	b	.L138
.L137:
	ldr	r3, [r1, #76]
	cmp	r3, r4
	bne	.L135
	ldr	r3, [r1, #16]
	cmp	r3, r5
	beq	.L136
.L135:
	ldr	r0, .L139
	bl	nm_ListGetNext
.L138:
	cmp	r0, #0
	mov	r1, r0
	bne	.L137
.L136:
	mov	r0, r1
	ldmfd	sp!, {r4, r5, pc}
.L140:
	.align	2
.L139:
	.word	.LANCHOR0
.LFE35:
	.size	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index, .-nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	.section	.text.nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn,"ax",%progbits
	.align	2
	.global	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	.type	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn, %function
nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn:
.LFB36:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI44:
	mov	r5, r0
	ldr	r0, .L154
	mov	r4, r1
	mov	r6, r2
	mov	r8, r3
	bl	nm_ListGetFirst
	mov	r7, #0
	mov	r1, r0
	b	.L142
.L146:
	ldr	r3, [r1, #76]
	cmp	r3, r5
	bne	.L149
	cmp	r8, #0
	beq	.L144
	ldr	r7, [r1, #96]
	cmp	r7, #0
	beq	.L143
.L144:
	ldr	r3, [r1, #80]
	cmp	r3, r4
	bne	.L149
	cmp	r4, #10
	moveq	r7, r1
	beq	.L143
	cmp	r4, #11
	bne	.L145
	ldr	r7, [r1, #84]
	cmp	r7, r6
	b	.L153
.L145:
	cmp	r4, #12
.L153:
	moveq	r7, r1
	movne	r7, #0
	b	.L143
.L149:
	mov	r7, #0
.L143:
	ldr	r0, .L154
	bl	nm_ListGetNext
	mov	r1, r0
.L142:
	rsbs	r3, r7, #1
	movcc	r3, #0
	cmp	r1, #0
	moveq	r3, #0
	cmp	r3, #0
	bne	.L146
	mov	r0, r7
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L155:
	.align	2
.L154:
	.word	.LANCHOR0
.LFE36:
	.size	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn, .-nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	.section	.text.POC_get_group_at_this_index,"ax",%progbits
	.align	2
	.global	POC_get_group_at_this_index
	.type	POC_get_group_at_this_index, %function
POC_get_group_at_this_index:
.LFB37:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI45:
	ldr	r4, .L157
	ldr	r2, .L157+4
	ldr	r3, .L157+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L157+12
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L158:
	.align	2
.L157:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2146
	.word	.LANCHOR0
.LFE37:
	.size	POC_get_group_at_this_index, .-POC_get_group_at_this_index
	.section	.text.nm_POC_load_group_name_into_guivar,"ax",%progbits
	.align	2
	.global	nm_POC_load_group_name_into_guivar
	.type	nm_POC_load_group_name_into_guivar, %function
nm_POC_load_group_name_into_guivar:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, lr}
.LCFI46:
	mov	r0, r0, asr #16
	bl	POC_get_group_at_this_index
	mov	r4, r0
	mov	r1, r4
	ldr	r0, .L161
	bl	nm_OnList
	cmp	r0, #1
	bne	.L160
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L161+4
	ldmfd	sp!, {r4, lr}
	b	strlcpy
.L160:
	ldr	r0, .L161+8
	ldr	r1, .L161+12
	ldmfd	sp!, {r4, lr}
	b	Alert_group_not_found_with_filename
.L162:
	.align	2
.L161:
	.word	.LANCHOR0
	.word	GuiVar_itmGroupName
	.word	.LC24
	.word	1931
.LFE31:
	.size	nm_POC_load_group_name_into_guivar, .-nm_POC_load_group_name_into_guivar
	.section	.text.BUDGETS_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	BUDGETS_copy_group_into_guivars
	.type	BUDGETS_copy_group_into_guivars, %function
BUDGETS_copy_group_into_guivars:
.LFB26:
	@ args = 0, pretend = 0, frame = 112
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L168+4
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI47:
	ldr	r2, .L168+8
	sub	sp, sp, #112
.LCFI48:
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L168+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L168+16
	ldr	r2, .L168+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L168+20
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	POC_get_group_at_this_index
	ldr	r3, .L168+24
	mov	r4, r0
	ldr	r0, [r0, #72]
	mov	r5, r4
	str	r0, [r3, #0]
	bl	SYSTEM_get_group_with_this_GID
	mov	r1, sp
	mov	r6, r0
	bl	SYSTEM_get_budget_details
	ldr	r1, .L168+28
	ldr	ip, .L168+32
	add	r0, sp, #8
	mov	r2, r4
	mov	r3, #0
.L164:
	flds	s13, [r2, #256]	@ int
	ldr	lr, [r0, #4]!
	add	r2, r2, #4
	fuitos	s15, s13
	str	lr, [r3, ip]
	add	r3, r3, #4
	cmp	r3, #96
	fmrs	lr, s15
	str	lr, [r1, #4]!	@ float
	bne	.L164
	mov	r0, r6
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L168+36
	bl	strlcpy
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L168+40
	flds	s15, .L168
	ldr	r2, [r4, #76]
	str	r0, [r3, #0]
	ldr	r3, .L168+44
	str	r2, [r3, #0]
	ldr	r2, [r4, #84]
	ldr	r3, .L168+48
	str	r2, [r3, #0]
	ldr	r3, .L168+52
	ldr	r2, [r4, #80]
	str	r2, [r3, #0]
	mov	r3, #12
.L165:
	flds	s13, [r5, #256]	@ int
	subs	r3, r3, #1
	add	r5, r5, #4
	fuitos	s14, s13
	fadds	s15, s15, s14
	bne	.L165
	ldr	r3, .L168+56
	ldr	r2, [r4, #240]
	fsts	s15, [r3, #0]
	ldr	r3, .L168+60
	mov	r0, r4
	fsts	s15, [r3, #0]
	ldr	r3, .L168+64
	str	r2, [r3, #0]
	bl	nm_GROUP_get_name
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L168+68
	bl	strlcpy
	ldr	r3, .L168+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L168+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #112
	ldmfd	sp!, {r4, r5, r6, pc}
.L169:
	.align	2
.L168:
	.word	0
	.word	list_system_recursive_MUTEX
	.word	.LC24
	.word	1463
	.word	list_poc_recursive_MUTEX
	.word	1465
	.word	.LANCHOR4
	.word	g_BUDGET_SETUP_budget-4
	.word	g_BUDGET_SETUP_meter_read_date
	.word	GuiVar_BudgetMainlineName
	.word	g_GROUP_ID
	.word	GuiVar_POCBoxIndex
	.word	GuiVar_POCDecoderSN1
	.word	GuiVar_POCType
	.word	GuiVar_BudgetAnnualValue
	.word	g_BUDGET_SETUP_annual_budget
	.word	GuiVar_BudgetEntryOptionIdx
	.word	GuiVar_GroupName
.LFE26:
	.size	BUDGETS_copy_group_into_guivars, .-BUDGETS_copy_group_into_guivars
	.section	.text.POC_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	POC_copy_group_into_guivars
	.type	POC_copy_group_into_guivars, %function
POC_copy_group_into_guivars:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI49:
	ldr	r4, .L171+4
	mov	r1, #400
	ldr	r2, .L171+8
	ldr	r3, .L171+12
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	POC_get_group_at_this_index
	mov	r5, r0
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L171+16
	str	r0, [r3, #0]
	mov	r0, r5
	bl	nm_GROUP_get_name
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L171+20
	bl	strlcpy
	ldr	r2, [r5, #76]
	flds	s13, [r5, #180]	@ int
	ldr	r3, .L171+24
	flds	s15, .L171
	ldr	r1, [r5, #176]
	fuitos	s14, s13
	str	r2, [r3, #0]
	ldr	r2, [r5, #84]
	flds	s13, [r5, #196]	@ int
	ldr	r3, .L171+28
	ldr	r0, .L171+32
	str	r2, [r3, #0]
	ldr	r2, [r5, #88]
	fdivs	s14, s14, s15
	ldr	r3, .L171+36
	cmp	r1, #8
	str	r2, [r3, #0]
	ldr	r2, [r5, #92]
	ldr	r3, .L171+40
	str	r2, [r3, #0]
	ldr	r2, [r5, #96]
	ldr	r3, .L171+44
	str	r2, [r3, #0]
	ldr	r2, [r5, #80]
	ldr	r3, .L171+48
	str	r2, [r3, #0]
	ldr	r2, [r5, #112]
	ldr	r3, .L171+52
	str	r2, [r3, #0]
	ldr	r2, [r5, #116]
	ldr	r3, .L171+56
	str	r2, [r3, #0]
	ldr	r3, .L171+60
	ldr	r2, [r5, #192]
	str	r1, [r3, #0]
	ldr	r3, .L171+64
	movls	r1, #0
	str	r2, [r3, #0]
	ldr	r3, [r5, #208]
	movhi	r1, #1
	str	r3, [r0, #0]
	ldr	r0, .L171+68
	cmp	r2, #8
	str	r1, [r0, #0]
	ldr	r1, .L171+72
	movls	r2, #0
	movhi	r2, #1
	str	r2, [r1, #0]
	ldr	r2, .L171+76
	cmp	r3, #8
	movls	r3, #0
	movhi	r3, #1
	str	r3, [r2, #0]
	ldr	r3, .L171+80
	fsts	s14, [r3, #0]
	fuitos	s14, s13
	flds	s13, [r5, #212]	@ int
	ldr	r3, .L171+84
	fdivs	s14, s14, s15
	fsts	s14, [r3, #0]
	fuitos	s14, s13
	flds	s13, [r5, #184]	@ int
	ldr	r3, .L171+88
	fdivs	s14, s14, s15
	fsts	s14, [r3, #0]
	fsitos	s14, s13
	flds	s13, [r5, #200]	@ int
	ldr	r3, .L171+92
	fdivs	s14, s14, s15
	fsts	s14, [r3, #0]
	fsitos	s14, s13
	ldr	r3, .L171+96
	fdivs	s14, s14, s15
	fsts	s14, [r3, #0]
	flds	s13, [r5, #216]	@ int
	ldr	r3, .L171+100
	ldr	r2, [r5, #188]
	fsitos	s14, s13
	ldr	r0, [r4, #0]
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	ldr	r3, .L171+104
	str	r2, [r3, #0]
	ldr	r2, [r5, #204]
	ldr	r3, .L171+108
	str	r2, [r3, #0]
	ldr	r2, [r5, #220]
	ldr	r3, .L171+112
	str	r2, [r3, #0]
	ldr	r2, [r5, #164]
	ldr	r3, .L171+116
	str	r2, [r3, #0]
	ldr	r2, [r5, #72]
	ldr	r3, .L171+120
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L172:
	.align	2
.L171:
	.word	1203982336
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	1397
	.word	g_GROUP_ID
	.word	GuiVar_GroupName
	.word	GuiVar_POCBoxIndex
	.word	GuiVar_POCDecoderSN1
	.word	GuiVar_POCFlowMeterType3
	.word	GuiVar_POCDecoderSN2
	.word	GuiVar_POCDecoderSN3
	.word	GuiVar_POCPhysicallyAvailable
	.word	GuiVar_POCType
	.word	GuiVar_POCUsage
	.word	GuiVar_POCMVType1
	.word	GuiVar_POCFlowMeterType1
	.word	GuiVar_POCFlowMeterType2
	.word	GuiVar_POCFMType1IsBermad
	.word	GuiVar_POCFMType2IsBermad
	.word	GuiVar_POCFMType3IsBermad
	.word	GuiVar_POCFlowMeterK1
	.word	GuiVar_POCFlowMeterK2
	.word	GuiVar_POCFlowMeterK3
	.word	GuiVar_POCFlowMeterOffset1
	.word	GuiVar_POCFlowMeterOffset2
	.word	GuiVar_POCFlowMeterOffset3
	.word	GuiVar_POCReedSwitchType1
	.word	GuiVar_POCReedSwitchType2
	.word	GuiVar_POCReedSwitchType3
	.word	GuiVar_POCBypassStages
	.word	.LANCHOR4
.LFE25:
	.size	POC_copy_group_into_guivars, .-POC_copy_group_into_guivars
	.section	.text.POC_get_gid_of_group_at_this_index,"ax",%progbits
	.align	2
	.global	POC_get_gid_of_group_at_this_index
	.type	POC_get_gid_of_group_at_this_index, %function
POC_get_gid_of_group_at_this_index:
.LFB38:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI50:
	ldr	r4, .L176
	ldr	r2, .L176+4
	ldr	r3, .L176+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L176+12
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	subs	r5, r0, #0
	ldrne	r5, [r5, #16]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L177:
	.align	2
.L176:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2182
	.word	.LANCHOR0
.LFE38:
	.size	POC_get_gid_of_group_at_this_index, .-POC_get_gid_of_group_at_this_index
	.section	.text.POC_get_index_for_group_with_this_GID,"ax",%progbits
	.align	2
	.global	POC_get_index_for_group_with_this_GID
	.type	POC_get_index_for_group_with_this_GID, %function
POC_get_index_for_group_with_this_GID:
.LFB39:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI51:
	ldr	r4, .L179
	ldr	r2, .L179+4
	ldr	r3, .L179+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L179+12
	bl	nm_GROUP_get_index_for_group_with_this_GID
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L180:
	.align	2
.L179:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2222
	.word	.LANCHOR0
.LFE39:
	.size	POC_get_index_for_group_with_this_GID, .-POC_get_index_for_group_with_this_GID
	.section	.text.POC_get_last_group_ID,"ax",%progbits
	.align	2
	.global	POC_get_last_group_ID
	.type	POC_get_last_group_ID, %function
POC_get_last_group_ID:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI52:
	ldr	r4, .L182
	mov	r1, #400
	ldr	r2, .L182+4
	ldr	r0, [r4, #0]
	ldr	r3, .L182+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L182+12
	ldr	r0, [r4, #0]
	ldr	r3, [r3, #0]
	ldr	r5, [r3, #12]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L183:
	.align	2
.L182:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2257
	.word	.LANCHOR0
.LFE40:
	.size	POC_get_last_group_ID, .-POC_get_last_group_ID
	.section	.text.POC_get_list_count,"ax",%progbits
	.align	2
	.global	POC_get_list_count
	.type	POC_get_list_count, %function
POC_get_list_count:
.LFB41:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI53:
	ldr	r4, .L185
	mov	r1, #400
	ldr	r2, .L185+4
	ldr	r0, [r4, #0]
	ldr	r3, .L185+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L185+12
	ldr	r0, [r4, #0]
	ldr	r5, [r3, #8]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L186:
	.align	2
.L185:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2292
	.word	.LANCHOR0
.LFE41:
	.size	POC_get_list_count, .-POC_get_list_count
	.section	.text.POC_get_GID_irrigation_system,"ax",%progbits
	.align	2
	.global	POC_get_GID_irrigation_system
	.type	POC_get_GID_irrigation_system, %function
POC_get_GID_irrigation_system:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI54:
	ldr	r4, .L188
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L188+4
	ldr	r3, .L188+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_get_last_group_ID
	ldr	r1, .L188+12
	mov	r2, #0
	str	r1, [sp, #4]
	add	r1, r5, #224
	str	r1, [sp, #8]
	ldr	r1, .L188+16
	str	r2, [sp, #0]
	str	r1, [sp, #12]
	add	r1, r5, #72
	mov	r3, r0
	mov	r0, r5
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L189:
	.align	2
.L188:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2325
	.word	nm_POC_set_GID_irrigation_system
	.word	.LC22
.LFE42:
	.size	POC_get_GID_irrigation_system, .-POC_get_GID_irrigation_system
	.section	.text.POC_get_GID_irrigation_system_using_POC_gid,"ax",%progbits
	.align	2
	.global	POC_get_GID_irrigation_system_using_POC_gid
	.type	POC_get_GID_irrigation_system_using_POC_gid, %function
POC_get_GID_irrigation_system_using_POC_gid:
.LFB43:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI55:
	ldr	r4, .L191
	ldr	r3, .L191+4
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L191+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L191+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	bl	POC_get_GID_irrigation_system
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L192:
	.align	2
.L191:
	.word	list_poc_recursive_MUTEX
	.word	2365
	.word	.LC24
	.word	.LANCHOR0
.LFE43:
	.size	POC_get_GID_irrigation_system_using_POC_gid, .-POC_get_GID_irrigation_system_using_POC_gid
	.section	.text.POC_get_fm_choice_and_rate_details,"ax",%progbits
	.align	2
	.global	POC_get_fm_choice_and_rate_details
	.type	POC_get_fm_choice_and_rate_details, %function
POC_get_fm_choice_and_rate_details:
.LFB44:
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI56:
	fstmfdd	sp!, {d8}
.LCFI57:
	ldr	r3, .L205+4
	mov	r4, r0
	sub	sp, sp, #68
.LCFI58:
	ldr	r0, [r3, #0]
	ldr	r2, .L205+8
	ldr	r3, .L205+12
	mov	r8, r1
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L205+16
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	beq	.L194
	mov	r5, r4
	mov	sl, #0
	add	r7, sp, #20
	ldr	r9, .L205+8
	flds	s16, .L205
	b	.L201
.L203:
	mov	sl, r6
.L201:
	mov	r1, #48
	ldr	r2, .L205+20
	ldr	r3, .L205+24
	add	r6, sl, #1
	mov	r0, r7
	str	r6, [sp, #0]
	bl	snprintf
	ldr	r3, .L205+28
	mov	r1, #0
	add	r0, sl, #11
	str	r3, [sp, #12]
	add	r0, r4, r0, asl #4
	mov	r3, r1
	mov	r2, #17
	stmia	sp, {r1, r7, r9}
	bl	RC_uint32_with_filename
	subs	r3, r0, #0
	bne	.L195
	ldr	fp, [r5, #176]
	str	r3, [sp, #16]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #4
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #8]
	add	r2, r4, #224
	str	r2, [sp, #12]
	mov	r1, r6
	mov	r2, fp
	ldr	r3, [sp, #16]
	str	r0, [sp, #4]
	mov	r0, r4
	bl	nm_POC_set_fm_type.part.1
.L195:
	ldr	r3, [r5, #176]
	cmp	r3, #8
	str	r3, [r8, #0]
	bne	.L196
	mov	r1, #48
	ldr	r2, .L205+32
	ldr	r3, .L205+24
	mov	r0, r7
	str	r6, [sp, #0]
	bl	snprintf
	ldr	r3, .L205+36
	add	r0, r4, sl, asl #4
	mov	r1, #0
	str	r3, [sp, #12]
	add	r0, r0, #180
	ldr	r3, .L205+40
	ldr	r2, .L205+44
	stmia	sp, {r1, r7, r9}
	bl	RC_uint32_with_filename
	subs	r3, r0, #0
	bne	.L197
	ldr	fp, [r5, #180]
	str	r3, [sp, #16]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #4
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #8]
	add	r2, r4, #224
	str	r2, [sp, #12]
	mov	r1, r6
	mov	r2, fp
	ldr	r3, [sp, #16]
	str	r0, [sp, #4]
	mov	r0, r4
	bl	nm_POC_set_kvalue.part.2
.L197:
	ldr	r3, [r5, #180]
	mov	r1, #48
	str	r3, [r8, #4]
	ldr	r2, .L205+48
	ldr	r3, .L205+24
	mov	r0, r7
	str	r6, [sp, #0]
	bl	snprintf
	mov	r3, #0
	stmia	sp, {r3, r7, r9}
	ldr	r3, .L205+52
	add	r0, r4, sl, asl #4
	str	r3, [sp, #12]
	add	r0, r0, #184
	ldr	r3, .L205+56
	ldr	r1, .L205+60
	ldr	r2, .L205+64
	bl	RC_int32_with_filename
	subs	r3, r0, #0
	bne	.L198
	ldr	fp, [r5, #184]
	str	r3, [sp, #16]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #4
	str	r2, [sp, #0]
	mov	r2, #1
	str	r2, [sp, #8]
	add	r2, r4, #224
	str	r2, [sp, #12]
	mov	r1, r6
	mov	r2, fp
	ldr	r3, [sp, #16]
	str	r0, [sp, #4]
	mov	r0, r4
	bl	nm_POC_set_offset.part.7
.L198:
	ldr	r3, [r5, #184]
	b	.L204
.L196:
	ldr	r2, .L205+68
	mov	r3, r3, asl #2
	add	r2, r2, r3
	flds	s15, [r2, #0]
	ldr	r2, .L205+72
	fmuls	s15, s15, s16
	add	r3, r2, r3
	ftouizs	s15, s15
	fsts	s15, [r8, #4]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
.L204:
	str	r3, [r8, #8]
	mov	r1, #48
	ldr	r2, .L205+76
	ldr	r3, .L205+24
	mov	r0, r7
	str	r6, [sp, #0]
	bl	snprintf
	mov	r1, #0
	add	r0, r4, sl, asl #4
	mov	r3, #2496
	str	r3, [sp, #12]
	add	r0, r0, #188
	mov	r2, #1
	mov	r3, r1
	stmia	sp, {r1, r7, r9}
	bl	RC_uint32_with_filename
	subs	sl, r0, #0
	bne	.L200
	ldr	fp, [r5, #188]
	bl	FLOWSENSE_get_controller_index
	mov	r3, #4
	str	r3, [sp, #0]
	mov	r3, #1
	str	r3, [sp, #8]
	add	r3, r4, #224
	str	r3, [sp, #12]
	mov	r1, r6
	mov	r2, fp
	mov	r3, sl
	str	r0, [sp, #4]
	mov	r0, r4
	bl	nm_POC_set_reed_switch.part.3
.L200:
	ldr	r3, [r5, #188]
	cmp	r6, #3
	str	r3, [r8, #12]
	add	r5, r5, #16
	add	r8, r8, #16
	bne	.L203
	mov	r4, #1
	b	.L202
.L194:
	ldr	r2, .L205+40
	ldr	r3, .L205+56
	str	r4, [r8, #0]
	stmib	r8, {r2, r3}
	ldr	r0, .L205+8
	str	r4, [r8, #12]
	str	r4, [r8, #16]
	str	r2, [r8, #20]
	str	r3, [r8, #24]
	str	r4, [r8, #28]
	str	r4, [r8, #32]
	str	r2, [r8, #36]
	str	r3, [r8, #40]
	str	r4, [r8, #44]
	ldr	r1, .L205+80
	bl	Alert_group_not_found_with_filename
.L202:
	ldr	r3, .L205+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #68
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L206:
	.align	2
.L205:
	.word	1203982336
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2414
	.word	.LANCHOR0
	.word	.LC15
	.word	.LC16
	.word	2428
	.word	.LC17
	.word	2451
	.word	100000
	.word	80000000
	.word	.LC21
	.word	2471
	.word	20000
	.word	-1000000
	.word	1000000
	.word	.LANCHOR5
	.word	.LANCHOR6
	.word	.LC18
	.word	2526
.LFE44:
	.size	POC_get_fm_choice_and_rate_details, .-POC_get_fm_choice_and_rate_details
	.section	.text.POC_get_usage,"ax",%progbits
	.align	2
	.global	POC_get_usage
	.type	POC_get_usage, %function
POC_get_usage:
.LFB45:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI59:
	ldr	r4, .L208
	mov	r5, r0
	ldr	r3, .L208+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L208+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L208+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L208+16
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r0, #224
	str	r3, [sp, #8]
	ldr	r3, .L208+20
	add	r1, r0, #112
	str	r3, [sp, #12]
	mov	r3, #2
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L209:
	.align	2
.L208:
	.word	list_poc_recursive_MUTEX
	.word	2542
	.word	.LC24
	.word	.LANCHOR0
	.word	nm_POC_set_poc_usage
	.word	.LC4
.LFE45:
	.size	POC_get_usage, .-POC_get_usage
	.section	.text.POC_get_master_valve_NO_or_NC,"ax",%progbits
	.align	2
	.global	POC_get_master_valve_NO_or_NC
	.type	POC_get_master_valve_NO_or_NC, %function
POC_get_master_valve_NO_or_NC:
.LFB46:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L216
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI60:
	ldr	r2, .L216+4
	mov	r4, r0
	sub	sp, sp, #64
.LCFI61:
	ldr	r0, [r3, #0]
	mov	sl, r1
	ldr	r3, .L216+8
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L216+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	bne	.L211
	mov	r3, #1
	str	r3, [sl, #0]
	str	r3, [sl, #4]
	str	r3, [sl, #8]
	ldr	r0, .L216+4
	ldr	r1, .L216+16
	bl	Alert_group_not_found_with_filename
	b	.L212
.L211:
	mov	r5, r4
	mov	r7, #0
	add	r8, sp, #16
	ldr	r9, .L216+4
	b	.L214
.L215:
	mov	r7, r6
.L214:
	add	r6, r7, #1
	mov	r1, #48
	ldr	r2, .L216+20
	ldr	r3, .L216+24
	mov	r0, r8
	str	r6, [sp, #0]
	bl	snprintf
	ldr	r3, .L216+28
	add	r0, r7, #29
	mov	r2, #1
	mov	r1, #0
	str	r3, [sp, #12]
	add	r0, r4, r0, asl #2
	mov	r3, r2
	stmia	sp, {r1, r8, r9}
	bl	RC_uint32_with_filename
	subs	r7, r0, #0
	bne	.L213
	ldr	fp, [r5, #116]
	bl	FLOWSENSE_get_controller_index
	mov	r3, #4
	str	r3, [sp, #0]
	mov	r3, #1
	str	r3, [sp, #8]
	add	r3, r4, #224
	str	r3, [sp, #12]
	mov	r1, r6
	mov	r2, fp
	mov	r3, r7
	str	r0, [sp, #4]
	mov	r0, r4
	bl	nm_POC_set_mv_type.part.5
.L213:
	ldr	r3, [r5, #116]
	cmp	r6, #3
	str	r3, [sl], #4
	add	r5, r5, #4
	bne	.L215
	mov	r4, #1
.L212:
	ldr	r3, .L216
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L217:
	.align	2
.L216:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2593
	.word	.LANCHOR0
	.word	2631
	.word	.LC8
	.word	.LC20
	.word	2607
.LFE46:
	.size	POC_get_master_valve_NO_or_NC, .-POC_get_master_valve_NO_or_NC
	.section	.text.POC_get_box_index_0,"ax",%progbits
	.align	2
	.global	POC_get_box_index_0
	.type	POC_get_box_index_0, %function
POC_get_box_index_0:
.LFB47:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI62:
	ldr	r4, .L219
	ldr	r3, .L219+4
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L219+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L219+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r5, r0
	bl	FLOWSENSE_get_controller_letter
	ldr	r3, .L219+16
	add	r1, r5, #76
	str	r3, [sp, #4]
	add	r3, r5, #224
	str	r3, [sp, #8]
	ldr	r3, .L219+20
	mov	r2, #0
	str	r3, [sp, #12]
	mov	r3, #11
	str	r0, [sp, #0]
	mov	r0, r5
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L220:
	.align	2
.L219:
	.word	list_poc_recursive_MUTEX
	.word	2659
	.word	.LC24
	.word	.LANCHOR0
	.word	nm_POC_set_box_index
	.word	.LC5
.LFE47:
	.size	POC_get_box_index_0, .-POC_get_box_index_0
	.section	.text.POC_get_type_of_poc,"ax",%progbits
	.align	2
	.global	POC_get_type_of_poc
	.type	POC_get_type_of_poc, %function
POC_get_type_of_poc:
.LFB48:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI63:
	ldr	r4, .L222
	mov	r5, r0
	ldr	r3, .L222+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L222+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L222+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	ldr	r3, .L222+16
	mov	r2, #10
	str	r3, [sp, #4]
	str	r2, [sp, #0]
	add	r3, r0, #224
	str	r3, [sp, #8]
	ldr	r3, .L222+20
	add	r1, r0, #80
	str	r3, [sp, #12]
	mov	r3, #12
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L223:
	.align	2
.L222:
	.word	list_poc_recursive_MUTEX
	.word	2697
	.word	.LC24
	.word	.LANCHOR0
	.word	nm_POC_set_poc_type
	.word	.LC3
.LFE48:
	.size	POC_get_type_of_poc, .-POC_get_type_of_poc
	.section	.text.POC_copy_preserve_info_into_guivars,"ax",%progbits
	.align	2
	.global	POC_copy_preserve_info_into_guivars
	.type	POC_copy_preserve_info_into_guivars, %function
POC_copy_preserve_info_into_guivars:
.LFB27:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L229
	stmfd	sp!, {r4, r5, lr}
.LCFI64:
	ldr	r2, .L229+4
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L229+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L229+12
	mov	r4, #472
	mla	r4, r5, r4, r3
	ldr	r3, .L229+16
	ldr	r2, [r4, #28]
	ldr	r5, [r4, #24]
	str	r2, [r3, #0]
	ldr	r3, .L229+20
	ldr	r2, [r4, #32]
	str	r5, [r3, #0]
	ldr	r3, .L229+24
	cmp	r5, #0
	str	r2, [r3, #0]
	ldr	r3, [r4, #112]
	ldr	r2, .L229+28
	str	r3, [r2, #0]
	ldr	r2, .L229+32
	str	r3, [r2, #0]
	ldr	r2, [r4, #200]
	ldr	r3, .L229+36
	str	r2, [r3, #0]
	ldr	r2, [r4, #288]
	ldr	r3, .L229+40
	str	r2, [r3, #0]
	ldr	r2, [r4, #140]
	ldr	r3, .L229+44
	str	r2, [r3, #0]
	ldr	r2, [r4, #228]
	ldr	r3, .L229+48
	str	r2, [r3, #0]
	ldr	r2, [r4, #316]
	ldr	r3, .L229+52
	str	r2, [r3, #0]
	ldr	r2, [r4, #144]
	ldr	r3, .L229+56
	str	r2, [r3, #0]
	ldr	r2, [r4, #232]
	ldr	r3, .L229+60
	str	r2, [r3, #0]
	ldr	r2, [r4, #320]
	ldr	r3, .L229+64
	str	r2, [r3, #0]
	ldr	r2, [r4, #160]	@ float
	ldr	r3, .L229+68
	str	r2, [r3, #0]	@ float
	ldr	r2, [r4, #248]	@ float
	ldr	r3, .L229+72
	str	r2, [r3, #0]	@ float
	ldr	r2, [r4, #336]	@ float
	ldr	r3, .L229+76
	str	r2, [r3, #0]	@ float
	ldr	r2, [r4, #168]	@ float
	ldr	r3, .L229+80
	str	r2, [r3, #0]	@ float
	ldr	r2, [r4, #256]	@ float
	ldr	r3, .L229+84
	str	r2, [r3, #0]	@ float
	ldr	r2, [r4, #344]	@ float
	ldr	r3, .L229+88
	str	r2, [r3, #0]	@ float
	ldr	r2, [r4, #188]
	ldr	r3, .L229+92
	str	r2, [r3, #0]
	ldr	r2, [r4, #276]
	ldr	r3, .L229+96
	str	r2, [r3, #0]
	ldr	r2, [r4, #364]
	ldr	r3, .L229+100
	str	r2, [r3, #0]
	ldr	r2, [r4, #192]
	ldr	r3, .L229+104
	str	r2, [r3, #0]
	ldr	r2, [r4, #280]
	ldr	r3, .L229+108
	str	r2, [r3, #0]
	ldr	r2, [r4, #368]
	ldr	r3, .L229+112
	str	r2, [r3, #0]
	beq	.L225
.LBB46:
	ldr	r3, .L229+116
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L229+4
	ldr	r3, .L229+120
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [r4, #24]
	ldr	r0, .L229+124
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	bne	.L226
	mov	r2, #65
	ldr	r0, .L229+128
	ldr	r1, .L229+132
	bl	strlcpy
	ldr	r3, .L229+136
	mov	r2, #12
	str	r2, [r3, #0]
	ldr	r3, .L229+140
	str	r4, [r3, #0]
	ldr	r3, .L229+144
	str	r4, [r3, #0]
	b	.L227
.L226:
	bl	nm_GROUP_get_name
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L229+128
	bl	strlcpy
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	ldr	r4, .L229+140
	str	r0, [r4, #0]
	bl	POC_get_box_index_0
	ldr	r3, .L229+136
	str	r0, [r3, #0]
	ldr	r0, [r4, #0]
	bl	POC_get_type_of_poc
	ldr	r3, .L229+144
	str	r0, [r3, #0]
.L227:
	ldr	r3, .L229+116
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L228
.L225:
.LBE46:
	mov	r2, #65
	ldr	r0, .L229+128
	ldr	r1, .L229+132
	bl	strlcpy
	ldr	r3, .L229+136
	mov	r2, #12
	str	r2, [r3, #0]
	ldr	r3, .L229+140
	str	r5, [r3, #0]
	ldr	r3, .L229+144
	str	r5, [r3, #0]
.L228:
	ldr	r3, .L229
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L230:
	.align	2
.L229:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC24
	.word	1541
	.word	poc_preserves
	.word	GuiVar_POCPreservesBoxIndex
	.word	GuiVar_POCPreservesGroupID
	.word	GuiVar_POCPreservesFileType
	.word	GuiVar_POCDecoderSN1
	.word	GuiVar_POCPreservesDecoderSN1
	.word	GuiVar_POCPreservesDecoderSN2
	.word	GuiVar_POCPreservesDecoderSN3
	.word	GuiVar_POCPreservesAccumPulses1
	.word	GuiVar_POCPreservesAccumPulses2
	.word	GuiVar_POCPreservesAccumPulses3
	.word	GuiVar_POCPreservesAccumMS1
	.word	GuiVar_POCPreservesAccumMS2
	.word	GuiVar_POCPreservesAccumMS3
	.word	GuiVar_POCPreservesAccumGal1
	.word	GuiVar_POCPreservesAccumGal2
	.word	GuiVar_POCPreservesAccumGal3
	.word	GuiVar_POCPreservesDelivered5SecAvg1
	.word	GuiVar_POCPreservesDelivered5SecAvg2
	.word	GuiVar_POCPreservesDelivered5SecAvg3
	.word	GuiVar_POCPreservesDeliveredMVCurrent1
	.word	GuiVar_POCPreservesDeliveredMVCurrent2
	.word	GuiVar_POCPreservesDeliveredMVCurrent3
	.word	GuiVar_POCPreservesDeliveredPumpCurrent1
	.word	GuiVar_POCPreservesDeliveredPumpCurrent2
	.word	GuiVar_POCPreservesDeliveredPumpCurrent3
	.word	list_poc_recursive_MUTEX
	.word	1590
	.word	.LANCHOR0
	.word	GuiVar_GroupName
	.word	.LC25
	.word	GuiVar_POCBoxIndex
	.word	GuiVar_POCGroupID
	.word	GuiVar_POCType
.LFE27:
	.size	POC_copy_preserve_info_into_guivars, .-POC_copy_preserve_info_into_guivars
	.section	.text.POC_get_decoder_serial_number_for_this_poc_and_level_0,"ax",%progbits
	.align	2
	.global	POC_get_decoder_serial_number_for_this_poc_and_level_0
	.type	POC_get_decoder_serial_number_for_this_poc_and_level_0, %function
POC_get_decoder_serial_number_for_this_poc_and_level_0:
.LFB50:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI65:
	ldr	r5, .L234
	mov	r4, r1
	mov	r6, r0
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L234+4
	ldr	r3, .L234+8
	bl	xQueueTakeMutexRecursive_debug
.LBB47:
	cmp	r4, #2
	bhi	.L232
	mov	r1, r4
	mov	r0, r6
	bl	nm_get_decoder_serial_number_support.part.4
	mov	r4, r0
	b	.L233
.L232:
	ldr	r0, .L234+4
	ldr	r1, .L234+12
	bl	Alert_index_out_of_range_with_filename
	mov	r4, #0
.L233:
.LBE47:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L235:
	.align	2
.L234:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2755
	.word	2741
.LFE50:
	.size	POC_get_decoder_serial_number_for_this_poc_and_level_0, .-POC_get_decoder_serial_number_for_this_poc_and_level_0
	.section	.text.POC_get_decoder_serial_number_for_this_poc_gid_and_level_0,"ax",%progbits
	.align	2
	.global	POC_get_decoder_serial_number_for_this_poc_gid_and_level_0
	.type	POC_get_decoder_serial_number_for_this_poc_gid_and_level_0, %function
POC_get_decoder_serial_number_for_this_poc_gid_and_level_0:
.LFB51:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI66:
	ldr	r5, .L239
	mov	r6, r0
	mov	r4, r1
	ldr	r2, .L239+4
	mov	r1, #400
	ldr	r3, .L239+8
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L239+12
	mov	r1, r6
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
.LBB48:
	cmp	r4, #2
	bhi	.L237
	mov	r1, r4
	bl	nm_get_decoder_serial_number_support.part.4
	mov	r4, r0
	b	.L238
.L237:
	ldr	r0, .L239+4
	ldr	r1, .L239+16
	bl	Alert_index_out_of_range_with_filename
	mov	r4, #0
.L238:
.LBE48:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L240:
	.align	2
.L239:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2772
	.word	.LANCHOR0
	.word	2741
.LFE51:
	.size	POC_get_decoder_serial_number_for_this_poc_gid_and_level_0, .-POC_get_decoder_serial_number_for_this_poc_gid_and_level_0
	.section	.text.nm_POC_create_new_group,"ax",%progbits
	.align	2
	.global	nm_POC_create_new_group
	.type	nm_POC_create_new_group, %function
nm_POC_create_new_group:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI67:
	mov	r5, r0
	ldr	r0, .L247
	mov	r4, r1
	mov	r7, r2
	bl	nm_ListGetFirst
	b	.L246
.L245:
	mov	r0, r6
	bl	nm_GROUP_get_group_ID
	mov	r8, r0
	bl	POC_get_box_index_0
	cmp	r5, r0
	bcc	.L243
	mov	r0, r8
	bl	POC_get_box_index_0
	cmp	r5, r0
	bne	.L244
	mov	r0, r8
	bl	POC_get_type_of_poc
	cmp	r4, r0
	bcc	.L243
	mov	r0, r8
	bl	POC_get_type_of_poc
	cmp	r4, r0
	bne	.L244
	mov	r0, r8
	mov	r1, #0
	bl	POC_get_decoder_serial_number_for_this_poc_gid_and_level_0
	cmp	r7, r0
	bcc	.L243
.L244:
	ldr	r0, .L247
	mov	r1, r6
	bl	nm_ListGetNext
.L246:
	cmp	r0, #0
	mov	r6, r0
	bne	.L245
.L243:
	mov	r3, #1
	stmia	sp, {r3, r6}
	ldr	r1, .L247+4
	ldr	r2, .L247+8
	mov	r3, #352
	ldr	r0, .L247
	bl	nm_GROUP_create_new_group
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L248:
	.align	2
.L247:
	.word	.LANCHOR0
	.word	.LANCHOR3
	.word	nm_POC_set_default_values
.LFE23:
	.size	nm_POC_create_new_group, .-nm_POC_create_new_group
	.section	.text.POC_get_decoder_serial_number_index_0,"ax",%progbits
	.align	2
	.global	POC_get_decoder_serial_number_index_0
	.type	POC_get_decoder_serial_number_index_0, %function
POC_get_decoder_serial_number_index_0:
.LFB52:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI68:
	subs	r4, r0, #0
	beq	.L250
	ldr	r3, [r4, #84]
	cmp	r3, r1
	beq	.L252
	ldr	r3, [r4, #88]
	cmp	r3, r1
	moveq	r0, #1
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, [r4, #92]
	cmp	r3, r1
	beq	.L254
	ldr	r0, .L255
	ldr	r1, .L255+4
	bl	Alert_group_not_found_with_filename
	b	.L252
.L250:
	ldr	r0, .L255
	ldr	r1, .L255+8
	bl	Alert_func_call_with_null_ptr_with_filename
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L252:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L254:
	mov	r0, #2
	ldmfd	sp!, {r4, pc}
.L256:
	.align	2
.L255:
	.word	.LC24
	.word	2810
	.word	2815
.LFE52:
	.size	POC_get_decoder_serial_number_index_0, .-POC_get_decoder_serial_number_index_0
	.section	.text.POC_get_show_for_this_poc,"ax",%progbits
	.align	2
	.global	POC_get_show_for_this_poc
	.type	POC_get_show_for_this_poc, %function
POC_get_show_for_this_poc:
.LFB53:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI69:
	ldr	r4, .L258
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L258+4
	ldr	r3, .L258+8
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r5, #224
	str	r3, [sp, #0]
	ldr	r3, .L258+12
	add	r1, r5, #96
	str	r3, [sp, #4]
	mov	r2, #0
	ldr	r3, .L258+16
	mov	r0, r5
	bl	SHARED_get_bool_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L259:
	.align	2
.L258:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2846
	.word	.LC7
	.word	nm_POC_set_show_this_poc
.LFE53:
	.size	POC_get_show_for_this_poc, .-POC_get_show_for_this_poc
	.section	.text.POC_get_bypass_number_of_levels,"ax",%progbits
	.align	2
	.global	POC_get_bypass_number_of_levels
	.type	POC_get_bypass_number_of_levels, %function
POC_get_bypass_number_of_levels:
.LFB54:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI70:
	ldr	r4, .L261
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L261+4
	ldr	r3, .L261+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L261+12
	mov	r2, #2
	str	r3, [sp, #4]
	add	r3, r5, #224
	str	r3, [sp, #8]
	ldr	r3, .L261+16
	add	r1, r5, #164
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #3
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L262:
	.align	2
.L261:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2865
	.word	nm_POC_set_bypass_number_of_levels
	.word	.LC2
.LFE54:
	.size	POC_get_bypass_number_of_levels, .-POC_get_bypass_number_of_levels
	.section	.text.POC_get_budget_gallons_entry_option,"ax",%progbits
	.align	2
	.global	POC_get_budget_gallons_entry_option
	.type	POC_get_budget_gallons_entry_option, %function
POC_get_budget_gallons_entry_option:
.LFB55:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI71:
	ldr	r4, .L264
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L264+4
	ldr	r3, .L264+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L264+12
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r5, #224
	str	r3, [sp, #8]
	ldr	r3, .L264+16
	add	r1, r5, #240
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #2
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L265:
	.align	2
.L264:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2900
	.word	nm_POC_set_budget_gallons_entry_option
	.word	.LC1
.LFE55:
	.size	POC_get_budget_gallons_entry_option, .-POC_get_budget_gallons_entry_option
	.section	.text.POC_get_budget_percent_ET,"ax",%progbits
	.align	2
	.global	POC_get_budget_percent_ET
	.type	POC_get_budget_percent_ET, %function
POC_get_budget_percent_ET:
.LFB56:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI72:
	ldr	r4, .L267
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L267+4
	ldr	r3, .L267+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #80
	str	r3, [sp, #0]
	ldr	r3, .L267+12
	add	r1, r5, #244
	str	r3, [sp, #4]
	add	r3, r5, #224
	str	r3, [sp, #8]
	ldr	r3, .L267+16
	mov	r2, #10
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #300
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L268:
	.align	2
.L267:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2935
	.word	nm_POC_set_budget_calculation_percent_of_et
	.word	.LC0
.LFE56:
	.size	POC_get_budget_percent_ET, .-POC_get_budget_percent_ET
	.section	.text.POC_show_poc_menu_items,"ax",%progbits
	.align	2
	.global	POC_show_poc_menu_items
	.type	POC_show_poc_menu_items, %function
POC_show_poc_menu_items:
.LFB57:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L275
	stmfd	sp!, {r4, lr}
.LCFI73:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L275+4
	ldr	r3, .L275+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L275+12
	bl	nm_ListGetFirst
	b	.L274
.L272:
	ldr	r3, [r1, #96]
	cmp	r3, #0
	bne	.L273
	ldr	r0, .L275+12
	bl	nm_ListGetNext
.L274:
	cmp	r0, #0
	mov	r1, r0
	bne	.L272
	mov	r4, r0
	b	.L271
.L273:
	mov	r4, #1
.L271:
	ldr	r3, .L275
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L276:
	.align	2
.L275:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	2963
	.word	.LANCHOR0
.LFE57:
	.size	POC_show_poc_menu_items, .-POC_show_poc_menu_items
	.section	.text.POC_at_least_one_POC_has_a_flow_meter,"ax",%progbits
	.align	2
	.global	POC_at_least_one_POC_has_a_flow_meter
	.type	POC_at_least_one_POC_has_a_flow_meter, %function
POC_at_least_one_POC_has_a_flow_meter:
.LFB58:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L283
	stmfd	sp!, {r4, lr}
.LCFI74:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L283+4
	ldr	r3, .L283+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L283+12
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L278
.L280:
	ldr	r3, [r1, #96]
	cmp	r3, #0
	beq	.L279
	ldr	r3, [r1, #112]
	cmp	r3, #0
	beq	.L279
	ldr	r3, [r1, #176]
	cmp	r3, #0
	bne	.L282
	ldr	r3, [r1, #192]
	cmp	r3, #0
	bne	.L282
	ldr	r3, [r1, #208]
	cmp	r3, #0
	movne	r4, #1
	b	.L279
.L282:
	mov	r4, #1
.L279:
	ldr	r0, .L283+12
	bl	nm_ListGetNext
	mov	r1, r0
.L278:
	cmp	r1, #0
	bne	.L280
	ldr	r3, .L283
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L284:
	.align	2
.L283:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	3005
	.word	.LANCHOR0
.LFE58:
	.size	POC_at_least_one_POC_has_a_flow_meter, .-POC_at_least_one_POC_has_a_flow_meter
	.section	.text.POC_at_least_one_POC_is_a_bypass_manifold,"ax",%progbits
	.align	2
	.global	POC_at_least_one_POC_is_a_bypass_manifold
	.type	POC_at_least_one_POC_is_a_bypass_manifold, %function
POC_at_least_one_POC_is_a_bypass_manifold:
.LFB59:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L292
	stmfd	sp!, {r4, lr}
.LCFI75:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L292+4
	ldr	r3, .L292+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L292+12
	bl	nm_ListGetFirst
	b	.L291
.L289:
	ldr	r3, [r1, #96]
	cmp	r3, #0
	beq	.L287
	ldr	r3, [r1, #112]
	cmp	r3, #0
	beq	.L287
	ldr	r3, [r1, #80]
	cmp	r3, #12
	beq	.L290
.L287:
	ldr	r0, .L292+12
	bl	nm_ListGetNext
.L291:
	cmp	r0, #0
	mov	r1, r0
	bne	.L289
	mov	r4, r0
	b	.L288
.L290:
	mov	r4, #1
.L288:
	ldr	r3, .L292
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L293:
	.align	2
.L292:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	3043
	.word	.LANCHOR0
.LFE59:
	.size	POC_at_least_one_POC_is_a_bypass_manifold, .-POC_at_least_one_POC_is_a_bypass_manifold
	.section	.text.POC_get_budget,"ax",%progbits
	.align	2
	.global	POC_get_budget
	.type	POC_get_budget, %function
POC_get_budget:
.LFB60:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI76:
	ldr	r4, .L295
	sub	sp, sp, #24
.LCFI77:
	mov	r6, r0
	mov	r5, r1
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L295+4
	ldr	r3, .L295+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L295+12
	mov	r3, #0
	stmib	sp, {r1, r3}
	str	r3, [sp, #0]
	ldr	r3, .L295+16
	add	r2, r5, #64
	str	r3, [sp, #12]
	add	r3, r6, #224
	str	r3, [sp, #16]
	ldr	r3, .L295+20
	mov	r1, r5
	str	r3, [sp, #20]
	add	r2, r6, r2, asl #2
	mov	r3, #24
	mov	r0, r6
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, pc}
.L296:
	.align	2
.L295:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	3076
	.word	100000000
	.word	nm_POC_set_budget_gallons
	.word	.LC2
.LFE60:
	.size	POC_get_budget, .-POC_get_budget
	.section	.text.POC_get_has_pump_attached,"ax",%progbits
	.align	2
	.global	POC_get_has_pump_attached
	.type	POC_get_has_pump_attached, %function
POC_get_has_pump_attached:
.LFB61:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI78:
	ldr	r4, .L298
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L298+4
	ldr	r3, .L298+8
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r5, #224
	str	r3, [sp, #0]
	ldr	r3, .L298+12
	add	r1, r5, #248
	str	r3, [sp, #4]
	mov	r2, #1
	ldr	r3, .L298+16
	mov	r0, r5
	bl	SHARED_get_bool_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L299:
	.align	2
.L298:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	3099
	.word	.LC6
	.word	nm_POC_set_has_pump_attached
.LFE61:
	.size	POC_get_has_pump_attached, .-POC_get_has_pump_attached
	.section	.text.POC_set_budget,"ax",%progbits
	.align	2
	.global	POC_set_budget
	.type	POC_set_budget, %function
POC_set_budget:
.LFB62:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, lr}
.LCFI79:
	ldr	r4, .L301
	mov	r5, r0
	mov	r6, r1
	mov	r8, r2
	bl	FLOWSENSE_get_controller_index
	mov	r1, #400
	ldr	r2, .L301+4
	ldr	r3, .L301+8
	mov	r7, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #11
	stmia	sp, {r3, r7}
	mov	r3, #1
	str	r3, [sp, #8]
	add	r3, r5, #224
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r1, r6
	mov	r2, r8
	mov	r3, #0
	bl	nm_POC_set_budget_gallons
	ldr	r0, [r4, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L302:
	.align	2
.L301:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	3123
.LFE62:
	.size	POC_set_budget, .-POC_set_budget
	.section	.text.POC_use_for_budget,"ax",%progbits
	.align	2
	.global	POC_use_for_budget
	.type	POC_use_for_budget, %function
POC_use_for_budget:
.LFB63:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L312
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI80:
	ldr	r2, .L312+4
	mov	r4, r0
	sub	sp, sp, #48
.LCFI81:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L312+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L312+12
	mov	r6, #472
	mla	r6, r4, r6, r3
	ldr	r0, [r6, #4]
	cmp	r0, #0
	moveq	r4, r0
	beq	.L304
	ldr	r2, [r6, #24]
	mov	r3, #468
	ldrh	r3, [r2, r3]
	tst	r3, #8064
	beq	.L311
	bl	POC_get_type_of_poc
	cmp	r0, #12
	movne	r5, #1
	bne	.L305
	ldr	r0, [r6, #4]
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	bl	POC_get_bypass_number_of_levels
	mov	r5, r0
.L305:
	ldr	r0, [r6, #4]
	mov	r1, sp
	bl	POC_get_fm_choice_and_rate_details
	mov	r3, #0
	b	.L306
.L307:
	ldr	r2, [sp, r3, asl #4]
	cmp	r2, #0
	bne	.L311
	add	r3, r3, #1
.L306:
	cmp	r3, r5
	bne	.L307
	mov	r4, #0
	b	.L304
.L311:
	mov	r4, #1
.L304:
	ldr	r3, .L312
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, pc}
.L313:
	.align	2
.L312:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC24
	.word	3137
	.word	poc_preserves+20
.LFE63:
	.size	POC_use_for_budget, .-POC_use_for_budget
	.section	.text.POC_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	POC_get_change_bits_ptr
	.type	POC_get_change_bits_ptr, %function
POC_get_change_bits_ptr:
.LFB64:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r0
	add	r2, r3, #228
	mov	r0, r1
	add	r1, r3, #224
	add	r3, r3, #232
	b	SHARED_get_32_bit_change_bits_ptr
.LFE64:
	.size	POC_get_change_bits_ptr, .-POC_get_change_bits_ptr
	.section	.text.nm_POC_store_changes,"ax",%progbits
	.align	2
	.type	nm_POC_store_changes, %function
nm_POC_store_changes:
.LFB17:
	@ args = 12, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI82:
	mov	r5, r0
	sub	sp, sp, #36
.LCFI83:
	mov	r9, r1
	mov	r4, r2
	ldr	r0, .L361
	mov	r1, r5
	add	r2, sp, #32
	mov	r6, r3
	ldr	sl, [sp, #72]
	ldr	r7, [sp, #76]
	ldr	fp, [sp, #80]
	bl	nm_GROUP_find_this_group_in_list
	ldr	r3, [sp, #32]
	cmp	r3, #0
	beq	.L316
	cmp	r9, #1
	bne	.L317
	sub	r3, r7, #15
	cmp	r3, #1
	bls	.L317
	mov	r0, r5
	bl	nm_GROUP_get_name
	mov	r3, #48
	mov	r2, #0
	stmia	sp, {r3, r4, r6}
	mov	r3, r2
	mov	r1, r0
	mov	r0, #49152
	bl	Alert_ChangeLine_Group
.L317:
	ldr	r0, [sp, #32]
	mov	r1, r7
	bl	POC_get_change_bits_ptr
	cmp	r4, #2
	mov	r8, r0
	beq	.L318
	tst	fp, #1
	beq	.L319
.L318:
	mov	r0, r5
	ldr	r7, [sp, #32]
	bl	nm_GROUP_get_name
	rsbs	r2, r9, #1
	movcc	r2, #0
	mov	r3, r4
	stmia	sp, {r6, sl}
	str	r8, [sp, #8]
	mov	r1, r0
	mov	r0, r7
	bl	nm_POC_set_name
	cmp	r4, #2
	beq	.L320
.L319:
	tst	fp, #4
	beq	.L321
.L320:
	rsbs	r2, r9, #1
	stmia	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r5, #76]
	mov	r3, r4
	bl	nm_POC_set_box_index
.L321:
	ldr	r0, [sp, #32]
	ldr	r3, [r5, #96]
	ldr	r2, [r0, #96]
	cmp	r4, #2
	str	r2, [sp, #28]
	str	r3, [sp, #24]
	beq	.L322
	tst	fp, #32
	beq	.L323
.L322:
	rsbs	r2, r9, #1
	movcc	r2, #0
	ldr	r1, [sp, #24]
	mov	r3, r4
	stmia	sp, {r6, sl}
	str	r8, [sp, #8]
	bl	nm_POC_set_show_this_poc
	cmp	r4, #2
	beq	.L324
.L323:
	tst	fp, #64
	beq	.L325
.L324:
	rsbs	r2, r9, #1
	stmia	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r5, #112]
	mov	r3, r4
	bl	nm_POC_set_poc_usage
	cmp	r4, #2
	beq	.L326
.L325:
	tst	fp, #8
	beq	.L327
.L326:
	rsbs	r2, r9, #1
	stmia	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r5, #80]
	mov	r3, r4
	bl	nm_POC_set_poc_type
	cmp	r4, #2
	bne	.L327
.L329:
	mov	ip, r5
	str	r5, [sp, #20]
	mov	r7, #0
	mov	r5, r4
	mov	r4, ip
	b	.L328
.L327:
	tst	fp, #128
	bne	.L329
	b	.L330
.L328:
	rsbs	r3, r9, #1
	add	r7, r7, #1
	stmia	sp, {r5, r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #12]
	movcc	r3, #0
	ldr	r2, [r4, #116]
	mov	r1, r7
	bl	nm_POC_set_mv_type.part.5
	cmp	r7, #3
	add	r4, r4, #4
	bne	.L328
	mov	r4, r5
	cmp	r4, #2
	ldr	r5, [sp, #20]
	bne	.L330
.L332:
	str	fp, [sp, #20]
	mov	r7, #0
	mov	fp, r8
	mov	r8, r4
	mov	r4, r5
	b	.L331
.L330:
	tst	fp, #16
	bne	.L332
	b	.L333
.L331:
	ldr	r2, [r5, #80]
	rsbs	r3, r7, #1
	movcc	r3, #0
	cmp	r2, #12
	moveq	r3, #0
	cmp	r3, #0
	movne	r3, #1
	bne	.L334
	cmp	r2, #12
	bne	.L334
	ldr	r3, [r5, #164]
	cmp	r7, r3
	movcs	r3, #0
	movcc	r3, #1
.L334:
	add	r7, r7, #1
	str	r8, [sp, #0]
	stmib	sp, {r6, sl, fp}
	ldr	r0, [sp, #32]
	ldr	r2, [r4, #84]
	cmp	r9, #0
	movne	r3, #0
	andeq	r3, r3, #1
	mov	r1, r7
	bl	nm_POC_set_decoder_serial_number
	cmp	r7, #3
	add	r4, r4, #4
	bne	.L331
	mov	r4, r8
	cmp	r4, #2
	mov	r8, fp
	ldr	fp, [sp, #20]
	bne	.L333
.L336:
	mov	ip, #0
	mov	r7, r4
	str	r5, [sp, #20]
	mov	r4, ip
	mov	ip, fp
	mov	fp, r9
	mov	r9, r5
	b	.L335
.L333:
	tst	fp, #256
	bne	.L336
	b	.L337
.L335:
	ldr	r3, [r9, #80]
	rsbs	r5, r4, #1
	movcc	r5, #0
	cmp	r3, #12
	moveq	r5, #0
	cmp	r5, #0
	movne	r5, #1
	bne	.L338
	cmp	r3, #12
	bne	.L338
	ldr	r5, [r9, #164]
	cmp	r4, r5
	movcs	r5, #0
	movcc	r5, #1
.L338:
	ldr	r3, [sp, #20]
	add	r4, r4, #1
	cmp	fp, #0
	movne	r5, #0
	andeq	r5, r5, #1
	str	r7, [sp, #0]
	stmib	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #12]
	mov	r1, r4
	ldr	r2, [r3, #176]
	mov	r3, r5
	str	ip, [sp, #16]
	bl	nm_POC_set_fm_type.part.1
	ldr	r3, [sp, #20]
	str	r7, [sp, #0]
	stmib	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #12]
	mov	r1, r4
	ldr	r2, [r3, #180]
	mov	r3, r5
	bl	nm_POC_set_kvalue.part.2
	ldr	r3, [sp, #20]
	str	r7, [sp, #0]
	stmib	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #12]
	mov	r1, r4
	ldr	r2, [r3, #184]
	mov	r3, r5
	bl	nm_POC_set_offset.part.7
	ldr	r3, [sp, #20]
	str	r7, [sp, #0]
	stmib	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #12]
	mov	r1, r4
	ldr	r2, [r3, #188]
	mov	r3, r5
	bl	nm_POC_set_reed_switch.part.3
	ldr	r2, [sp, #20]
	cmp	r4, #3
	add	r2, r2, #16
	str	r2, [sp, #20]
	ldr	ip, [sp, #16]
	bne	.L335
	cmp	r7, #2
	mov	r5, r9
	mov	r4, r7
	mov	r9, fp
	mov	fp, ip
	beq	.L339
.L337:
	tst	fp, #512
	beq	.L340
.L339:
	rsbs	r2, r9, #1
	stmia	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r5, #164]
	mov	r3, r4
	bl	nm_POC_set_bypass_number_of_levels
	cmp	r4, #2
	beq	.L341
.L340:
	tst	fp, #2
	beq	.L342
.L341:
	rsbs	r2, r9, #1
	stmia	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r5, #72]
	mov	r3, r4
	bl	nm_POC_set_GID_irrigation_system
	cmp	r4, #2
	beq	.L343
.L342:
	tst	fp, #1024
	beq	.L344
.L343:
	rsbs	r2, r9, #1
	stmia	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r5, #240]
	mov	r3, r4
	bl	nm_POC_set_budget_gallons_entry_option
	cmp	r4, #2
	beq	.L345
.L344:
	tst	fp, #2048
	beq	.L346
.L345:
	rsbs	r2, r9, #1
	stmia	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r5, #244]
	mov	r3, r4
	bl	nm_POC_set_budget_calculation_percent_of_et
	cmp	r4, #2
	bne	.L346
.L348:
	mov	ip, r5
	str	r5, [sp, #20]
	mov	r7, #0
	mov	r5, r4
	mov	r4, ip
	b	.L347
.L346:
	tst	fp, #16384
	bne	.L348
	b	.L349
.L347:
	rsbs	r3, r9, #1
	stmia	sp, {r5, r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #12]
	mov	r1, r7
	ldr	r2, [r4, #256]
	movcc	r3, #0
	add	r7, r7, #1
	bl	nm_POC_set_budget_gallons
	cmp	r7, #24
	add	r4, r4, #4
	bne	.L347
	mov	r4, r5
	cmp	r4, #2
	ldr	r5, [sp, #20]
	beq	.L350
.L349:
	tst	fp, #32768
	beq	.L351
.L350:
	rsbs	r2, r9, #1
	stmia	sp, {r6, sl}
	ldr	r0, [sp, #32]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r5, #248]
	mov	r3, r4
	bl	nm_POC_set_has_pump_attached
.L351:
	ldr	r3, [sp, #28]
	ldr	r2, [sp, #24]
	cmp	r3, r2
	beq	.L315
	bl	POC_PRESERVES_synchronize_preserves_to_file
	b	.L315
.L316:
	ldr	r0, .L361+4
	ldr	r1, .L361+8
	bl	Alert_group_not_found_with_filename
.L315:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L362:
	.align	2
.L361:
	.word	.LANCHOR0
	.word	.LC10
	.word	1804
.LFE17:
	.size	nm_POC_store_changes, .-nm_POC_store_changes
	.section	.text.BUDGETS_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	BUDGETS_extract_and_store_changes_from_GuiVars
	.type	BUDGETS_extract_and_store_changes_from_GuiVars, %function
BUDGETS_extract_and_store_changes_from_GuiVars:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI84:
	ldr	r3, .L368
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L368+4
	ldr	r3, .L368+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L368+12
	ldr	r5, .L368+16
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_group_with_this_GID
	mov	r4, #0
	mov	r7, #2
	mov	r8, #1
	mov	r6, r0
.L364:
	ldr	sl, [r5, #4]!
	bl	FLOWSENSE_get_controller_index
	mov	r1, #2
	mov	r9, r0
	mov	r0, r6
	bl	SYSTEM_get_change_bits_ptr
	mov	r1, r4
	mov	r2, sl
	mov	r3, #1
	add	r4, r4, #1
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	str	r0, [sp, #12]
	mov	r0, r6
	bl	nm_SYSTEM_set_budget_period
	cmp	r4, #24
	bne	.L364
	ldr	r3, .L368
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L368+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L368+4
	ldr	r3, .L368+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L368+4
	ldr	r2, .L368+28
	mov	r0, #352
	bl	mem_malloc_debug
	ldr	r3, .L368+32
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L368+36
	ldr	r1, [r3, #0]
	ldr	r3, .L368+40
	ldr	r2, [r3, #0]
	mov	r3, #1
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	mov	r2, #352
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, .L368+44
	mov	r2, r4
	add	r1, r3, #96
.L365:
	add	r3, r3, #4
	flds	s15, [r3, #0]
	cmp	r3, r1
	ftouizs	s15, s15
	fsts	s15, [r2, #256]	@ int
	add	r2, r2, #4
	bne	.L365
	ldr	r3, .L368+48
	ldr	r5, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	mov	r1, #0
	str	r2, [sp, #0]
	str	r1, [sp, #8]
	mov	r2, #2
	mov	r1, r5
	str	r2, [sp, #4]
	mov	r3, r0
	mov	r0, r4
	bl	nm_POC_store_changes
	ldr	r3, .L368+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r1, .L368+4
	ldr	r2, .L368+52
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	mem_free_debug
.L369:
	.align	2
.L368:
	.word	list_system_recursive_MUTEX
	.word	.LC24
	.word	1858
	.word	.LANCHOR4
	.word	g_BUDGET_SETUP_meter_read_date-4
	.word	list_poc_recursive_MUTEX
	.word	1881
	.word	1883
	.word	GuiVar_POCBoxIndex
	.word	GuiVar_POCType
	.word	GuiVar_POCDecoderSN1
	.word	g_BUDGET_SETUP_budget-4
	.word	g_GROUP_creating_new
	.word	1898
.LFE30:
	.size	BUDGETS_extract_and_store_changes_from_GuiVars, .-BUDGETS_extract_and_store_changes_from_GuiVars
	.section	.text.POC_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	POC_extract_and_store_changes_from_GuiVars
	.type	POC_extract_and_store_changes_from_GuiVars, %function
POC_extract_and_store_changes_from_GuiVars:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L376+4
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, lr}
.LCFI85:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L376+8
	ldr	r2, .L376+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L376+12
	ldr	r2, .L376+16
	mov	r0, #352
	bl	mem_malloc_debug
	ldr	r3, .L376+20
	ldr	r5, .L376+24
	ldr	r2, [r5, #0]
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L376+28
	ldr	r1, [r3, #0]
	mov	r3, #1
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	mov	r2, #352
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r1, .L376+32
	mov	r2, #48
	add	r0, r4, #20
	bl	strlcpy
	ldr	r3, .L376+36
	ldr	r2, .L376+40
	ldr	r3, [r3, #0]
	ldr	r1, [r2, #0]
	str	r3, [r4, #112]
	ldr	r3, [r5, #0]
	cmp	r3, r1
	bne	.L371
	cmp	r3, #0
	bne	.L372
.L371:
	ldr	r2, .L376+44
	ldr	r2, [r2, #0]
	cmp	r2, #3
	bne	.L373
	ldr	r2, .L376+48
	ldr	r2, [r2, #0]
	cmp	r1, r2
	bne	.L374
	cmp	r2, #0
	bne	.L372
.L374:
	cmp	r3, r2
	bne	.L373
	cmp	r3, #0
	beq	.L373
.L372:
	ldr	r3, .L376+52
	ldr	r0, [r3, #0]
	bl	POC_get_group_at_this_index
	mov	r1, #0
	mov	r5, r0
	ldr	r0, [r0, #16]
	bl	POC_get_decoder_serial_number_for_this_poc_gid_and_level_0
	ldr	r3, .L376+24
	mov	r1, #1
	str	r0, [r3, #0]
	ldr	r0, [r5, #16]
	bl	POC_get_decoder_serial_number_for_this_poc_gid_and_level_0
	ldr	r3, .L376+40
	str	r0, [r3, #0]
	ldr	r3, .L376+44
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bne	.L375
	ldr	r0, [r5, #16]
	mov	r1, #2
	bl	POC_get_decoder_serial_number_for_this_poc_gid_and_level_0
	ldr	r3, .L376+48
	str	r0, [r3, #0]
.L375:
	ldr	r0, .L376+56
	bl	DIALOG_draw_ok_dialog
.L373:
	ldr	r3, .L376+24
	flds	s15, .L376
	mov	r6, #1
	ldr	r3, [r3, #0]
	str	r6, [r4, #120]
	str	r3, [r4, #84]
	ldr	r3, .L376+40
	str	r6, [r4, #124]
	ldr	r3, [r3, #0]
	ldr	r5, .L376+60
	str	r3, [r4, #88]
	ldr	r3, .L376+48
	ldr	r3, [r3, #0]
	str	r3, [r4, #92]
	ldr	r3, .L376+64
	ldr	r3, [r3, #0]
	str	r3, [r4, #116]
	ldr	r3, .L376+68
	ldr	r3, [r3, #0]
	str	r3, [r4, #176]
	ldr	r3, .L376+72
	ldr	r3, [r3, #0]
	str	r3, [r4, #192]
	ldr	r3, .L376+76
	ldr	r3, [r3, #0]
	str	r3, [r4, #208]
	ldr	r3, .L376+80
	flds	s14, [r3, #0]
	ldr	r3, .L376+84
	fmuls	s14, s14, s15
	ftouizs	s14, s14
	fsts	s14, [r4, #180]	@ int
	flds	s14, [r3, #0]
	ldr	r3, .L376+88
	fmuls	s14, s14, s15
	ftouizs	s14, s14
	fsts	s14, [r4, #196]	@ int
	flds	s14, [r3, #0]
	ldr	r3, .L376+92
	fmuls	s14, s14, s15
	ftouizs	s14, s14
	fsts	s14, [r4, #212]	@ int
	flds	s14, [r3, #0]
	ldr	r3, .L376+96
	fmuls	s14, s14, s15
	ftosizs	s14, s14
	fsts	s14, [r4, #184]	@ int
	flds	s14, [r3, #0]
	ldr	r3, .L376+100
	fmuls	s14, s14, s15
	ftosizs	s14, s14
	fsts	s14, [r4, #200]	@ int
	flds	s14, [r3, #0]
	ldr	r3, .L376+104
	fmuls	s15, s14, s15
	ldr	r3, [r3, #0]
	str	r3, [r4, #188]
	ldr	r3, .L376+108
	ldr	r3, [r3, #0]
	ftosizs	s15, s15
	str	r3, [r4, #204]
	ldr	r3, .L376+112
	ldr	r3, [r3, #0]
	fsts	s15, [r4, #216]	@ int
	str	r3, [r4, #220]
	ldr	r3, .L376+44
	ldr	r7, [r5, #0]
	ldr	r3, [r3, #0]
	str	r3, [r4, #164]
	ldr	r3, .L376+116
	ldr	r3, [r3, #0]
	str	r3, [r4, #72]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #2
	str	r6, [sp, #0]
	mov	r1, r7
	mov	r6, #0
	stmib	sp, {r2, r6}
	mov	r3, r0
	mov	r0, r4
	bl	nm_POC_store_changes
	mov	r0, r4
	ldr	r1, .L376+12
	ldr	r2, .L376+120
	bl	mem_free_debug
	ldr	r3, .L376+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	str	r6, [r5, #0]
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, pc}
.L377:
	.align	2
.L376:
	.word	1203982336
	.word	list_poc_recursive_MUTEX
	.word	1697
	.word	.LC24
	.word	1701
	.word	GuiVar_POCBoxIndex
	.word	GuiVar_POCDecoderSN1
	.word	GuiVar_POCType
	.word	GuiVar_GroupName
	.word	GuiVar_POCUsage
	.word	GuiVar_POCDecoderSN2
	.word	GuiVar_POCBypassStages
	.word	GuiVar_POCDecoderSN3
	.word	g_GROUP_list_item_index
	.word	601
	.word	g_GROUP_creating_new
	.word	GuiVar_POCMVType1
	.word	GuiVar_POCFlowMeterType1
	.word	GuiVar_POCFlowMeterType2
	.word	GuiVar_POCFlowMeterType3
	.word	GuiVar_POCFlowMeterK1
	.word	GuiVar_POCFlowMeterK2
	.word	GuiVar_POCFlowMeterK3
	.word	GuiVar_POCFlowMeterOffset1
	.word	GuiVar_POCFlowMeterOffset2
	.word	GuiVar_POCFlowMeterOffset3
	.word	GuiVar_POCReedSwitchType1
	.word	GuiVar_POCReedSwitchType2
	.word	GuiVar_POCReedSwitchType3
	.word	.LANCHOR4
	.word	1819
.LFE29:
	.size	POC_extract_and_store_changes_from_GuiVars, .-POC_extract_and_store_changes_from_GuiVars
	.section	.text.nm_POC_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	nm_POC_extract_and_store_changes_from_comm
	.type	nm_POC_extract_and_store_changes_from_comm, %function
nm_POC_extract_and_store_changes_from_comm:
.LFB18:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI86:
	mov	r7, r0
	sub	sp, sp, #60
.LCFI87:
	str	r1, [sp, #16]
	str	r2, [sp, #20]
	mov	r1, r7
	add	r0, sp, #44
	mov	r2, #4
	mov	fp, #0
	mov	r8, r3
	add	r7, r7, #4
	bl	memcpy
	mov	sl, #4
	str	fp, [sp, #12]
	b	.L379
.L398:
	mov	r1, r7
	mov	r2, #4
	add	r0, sp, #56
	bl	memcpy
	add	r1, r7, #4
	mov	r2, #4
	add	r0, sp, #48
	bl	memcpy
	add	r1, r7, #8
	mov	r2, #4
	add	r0, sp, #52
	bl	memcpy
	add	r1, r7, #12
	mov	r2, #12
	add	r0, sp, #24
	bl	memcpy
	add	r1, r7, #24
	mov	r2, #4
	add	r0, sp, #36
	bl	memcpy
	add	r1, r7, #28
	mov	r2, #4
	add	r0, sp, #40
	bl	memcpy
	ldr	r0, [sp, #48]
	ldr	r1, [sp, #52]
	ldr	r2, [sp, #24]
	mov	r3, #0
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	add	r4, r7, #32
	add	r5, sl, #32
	subs	r9, r0, #0
	bne	.L380
	ldr	r1, [sp, #52]
	ldr	r2, [sp, #24]
	ldr	r0, [sp, #48]
	bl	nm_POC_create_new_group
	mov	r9, r0
	bl	POC_PRESERVES_request_a_resync
	cmp	r8, #16
	bne	.L381
	ldr	r3, .L402
	add	r0, r9, #224
	ldr	r1, [r3, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L381:
	cmp	r9, #0
	beq	.L382
	mov	r3, #1
	str	r3, [sp, #12]
.L380:
	ldr	r1, .L402+4
	ldr	r2, .L402+8
	mov	r0, #352
	bl	mem_malloc_debug
	mov	r1, #0
	mov	r2, #352
	mov	r6, r0
	bl	memset
	mov	r0, r6
	mov	r1, r9
	mov	r2, #352
	bl	memcpy
	ldr	r3, [sp, #40]
	tst	r3, #1
	beq	.L383
	mov	r1, r4
	add	r0, r6, #20
	mov	r2, #48
	bl	memcpy
	add	r4, r7, #80
	add	r5, sl, #80
.L383:
	ldr	r3, [sp, #40]
	tst	r3, #2
	beq	.L384
	mov	r1, r4
	add	r0, r6, #72
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L384:
	ldr	r3, [sp, #40]
	tst	r3, #4
	beq	.L385
	mov	r1, r4
	add	r0, r6, #76
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L385:
	ldr	r3, [sp, #40]
	tst	r3, #8
	beq	.L386
	mov	r1, r4
	add	r0, r6, #80
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L386:
	ldr	r3, [sp, #40]
	tst	r3, #16
	beq	.L387
	mov	r1, r4
	add	r0, r6, #84
	mov	r2, #12
	bl	memcpy
	add	r4, r4, #12
	add	r5, r5, #12
.L387:
	ldr	r3, [sp, #40]
	tst	r3, #32
	beq	.L388
	mov	r1, r4
	add	r0, r6, #96
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L388:
	ldr	r3, [sp, #40]
	tst	r3, #64
	beq	.L389
	mov	r1, r4
	add	r0, r6, #112
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L389:
	ldr	r3, [sp, #40]
	tst	r3, #128
	beq	.L390
	mov	r1, r4
	add	r0, r6, #116
	mov	r2, #12
	bl	memcpy
	add	r4, r4, #12
	add	r5, r5, #12
.L390:
	ldr	r3, [sp, #40]
	tst	r3, #256
	beq	.L391
	mov	r1, r4
	add	r0, r6, #176
	mov	r2, #48
	bl	memcpy
	add	r4, r4, #48
	add	r5, r5, #48
.L391:
	ldr	r3, [sp, #40]
	tst	r3, #512
	beq	.L392
	mov	r1, r4
	add	r0, r6, #164
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L392:
	ldr	r3, [sp, #40]
	tst	r3, #1024
	beq	.L393
	mov	r1, r4
	add	r0, r6, #240
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L393:
	ldr	r3, [sp, #40]
	tst	r3, #2048
	beq	.L394
	mov	r1, r4
	add	r0, r6, #244
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L394:
	ldr	r3, [sp, #40]
	tst	r3, #16384
	beq	.L395
	mov	r1, r4
	add	r0, r6, #256
	mov	r2, #96
	bl	memcpy
	add	r4, r4, #96
	add	r5, r5, #96
.L395:
	ldr	r3, [sp, #40]
	tst	r3, #32768
	beq	.L396
	mov	r1, r4
	add	r0, r6, #248
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L396:
	ldr	r3, [sp, #20]
	mov	r0, r6
	stmia	sp, {r3, r8}
	ldr	r3, [sp, #40]
	ldr	r1, [sp, #12]
	str	r3, [sp, #8]
	ldr	r2, [sp, #16]
	mov	r3, #0
	bl	nm_POC_store_changes
	mov	r0, r6
	ldr	r1, .L402+4
	ldr	r2, .L402+12
	bl	mem_free_debug
	b	.L397
.L382:
	ldr	r0, .L402+4
	ldr	r1, .L402+16
	bl	Alert_group_not_found_with_filename
	mov	r3, #1
	str	r3, [sp, #12]
.L397:
	add	fp, fp, #1
	mov	sl, r5
	mov	r7, r4
.L379:
	ldr	r3, [sp, #44]
	cmp	fp, r3
	bcc	.L398
	cmp	sl, #0
	beq	.L399
	cmp	r8, #1
	cmpne	r8, #15
	beq	.L400
	cmp	r8, #16
	bne	.L401
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L401
.L400:
	mov	r0, #11
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L401
.L399:
	ldr	r0, .L402+4
	ldr	r1, .L402+20
	bl	Alert_bit_set_with_no_data_with_filename
.L401:
	mov	r0, sl
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L403:
	.align	2
.L402:
	.word	list_poc_recursive_MUTEX
	.word	.LC10
	.word	2021
	.word	2175
	.word	2194
	.word	2231
.LFE18:
	.size	nm_POC_extract_and_store_changes_from_comm, .-nm_POC_extract_and_store_changes_from_comm
	.section	.text.POC_extract_and_store_group_name_from_GuiVars,"ax",%progbits
	.align	2
	.global	POC_extract_and_store_group_name_from_GuiVars
	.type	POC_extract_and_store_group_name_from_GuiVars, %function
POC_extract_and_store_group_name_from_GuiVars:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L406
	stmfd	sp!, {r4, r5, lr}
.LCFI88:
	ldr	r0, [r3, #0]
	sub	sp, sp, #20
.LCFI89:
	mov	r1, #400
	ldr	r2, .L406+4
	ldr	r3, .L406+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L406+12
	ldr	r0, [r3, #0]
	bl	POC_get_group_at_this_index
	subs	r4, r0, #0
	beq	.L405
	bl	FLOWSENSE_get_controller_index
	mov	r1, #2
	mov	r5, r0
	mov	r0, r4
	bl	POC_get_change_bits_ptr
	add	r3, r4, #232
	str	r3, [sp, #12]
	mov	r3, #0
	mov	r2, #1
	str	r3, [sp, #16]
	ldr	r1, .L406+16
	mov	r3, #2
	str	r5, [sp, #0]
	str	r2, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	SHARED_set_name_32_bit_change_bits
.L405:
	ldr	r3, .L406
	ldr	r0, [r3, #0]
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L407:
	.align	2
.L406:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	1661
	.word	g_GROUP_list_item_index
	.word	GuiVar_GroupName
.LFE28:
	.size	POC_extract_and_store_group_name_from_GuiVars, .-POC_extract_and_store_group_name_from_GuiVars
	.section	.text.POC_build_data_to_send,"ax",%progbits
	.align	2
	.global	POC_build_data_to_send
	.type	POC_build_data_to_send, %function
POC_build_data_to_send:
.LFB24:
	@ args = 4, pretend = 0, frame = 44
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI90:
	mov	r8, r3
	sub	sp, sp, #72
.LCFI91:
	mov	r3, #0
	str	r3, [sp, #68]
	ldr	r3, .L426
	str	r1, [sp, #36]
	mov	r4, r0
	mov	r6, r2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L426+4
	ldr	r3, .L426+8
	ldr	sl, [sp, #108]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L426+12
	bl	nm_ListGetFirst
	b	.L424
.L412:
	mov	r0, r5
	mov	r1, r8
	bl	POC_get_change_bits_ptr
	ldr	r3, [r0, #0]
	cmp	r3, #0
	ldrne	r3, [sp, #68]
	addne	r3, r3, #1
	strne	r3, [sp, #68]
	bne	.L411
.L410:
	ldr	r0, .L426+12
	mov	r1, r5
	bl	nm_ListGetNext
.L424:
	cmp	r0, #0
	mov	r5, r0
	bne	.L412
.L411:
	ldr	r3, [sp, #68]
	cmp	r3, #0
	beq	.L422
	mov	r3, #0
	str	r3, [sp, #68]
	mov	r0, r4
	mov	r3, r6
	add	r1, sp, #60
	ldr	r2, [sp, #36]
	str	sl, [sp, #0]
	bl	PDATA_allocate_space_for_num_changed_groups_in_pucp
	ldr	r3, [r6, #0]
	cmp	r3, #0
	str	r0, [sp, #32]
	beq	.L413
	ldr	r0, .L426+12
	bl	nm_ListGetFirst
	ldr	r2, [sp, #36]
	add	r2, r2, #32
	str	r2, [sp, #52]
	mov	r5, r0
	b	.L414
.L420:
	mov	r0, r5
	mov	r1, r8
	bl	POC_get_change_bits_ptr
	ldr	r3, [r0, #0]
	mov	r7, r0
	cmp	r3, #0
	beq	.L415
	ldr	r3, [r6, #0]
	cmp	r3, #0
	beq	.L416
	ldr	r3, [sp, #52]
	ldr	r2, [sp, #32]
	add	fp, r3, r2
	cmp	fp, sl
	bcs	.L416
	add	r1, r5, #16
	mov	r2, #4
	mov	r0, r4
	bl	PDATA_copy_var_into_pucp
	add	r3, r5, #76
	mov	r1, r3
	mov	r2, #4
	mov	r0, r4
	str	r3, [sp, #40]
	bl	PDATA_copy_var_into_pucp
	add	r2, r5, #80
	mov	r1, r2
	str	r2, [sp, #44]
	mov	r0, r4
	mov	r2, #4
	bl	PDATA_copy_var_into_pucp
	add	r3, r5, #84
	mov	r1, r3
	mov	r2, #12
	mov	r0, r4
	str	r3, [sp, #48]
	bl	PDATA_copy_var_into_pucp
	mov	r1, #4
	mov	r2, r1
	add	r3, sp, #64
	mov	r0, r4
	bl	PDATA_copy_bitfield_info_into_pucp
	add	r3, r5, #20
	str	r3, [sp, #4]
	mov	r3, #48
	add	r9, r5, #236
	mov	r2, #0
	str	r3, [sp, #8]
	add	r1, sp, #56
	mov	r3, r7
	mov	r0, r4
	str	r2, [sp, #56]
	str	fp, [sp, #12]
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r2, [sp, #32]
	ldr	r3, [sp, #36]
	add	r1, sp, #56
	add	fp, r2, r3
	add	r3, r5, #72
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	mov	r2, #1
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, r0, #32
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r3, [sp, #40]
	mov	r2, #4
	str	r3, [sp, #4]
	str	r2, [sp, #8]
	add	r1, sp, #56
	mov	r2, #2
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r3, [sp, #44]
	mov	r2, #4
	str	r3, [sp, #4]
	str	r2, [sp, #8]
	add	r1, sp, #56
	mov	r2, #3
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r3, [sp, #48]
	mov	r2, #12
	str	r3, [sp, #4]
	str	r2, [sp, #8]
	add	r1, sp, #56
	mov	r2, #4
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #96
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #56
	mov	r2, #5
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #112
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #56
	mov	r2, #6
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #116
	str	r3, [sp, #4]
	mov	r3, #12
	str	r3, [sp, #8]
	add	r1, sp, #56
	mov	r2, #7
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #176
	str	r3, [sp, #4]
	mov	r2, #48
	str	r2, [sp, #8]
	add	r1, sp, #56
	mov	r2, #8
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #164
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #56
	mov	r2, #9
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #240
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #56
	mov	r2, #10
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #244
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #56
	mov	r2, #11
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #256
	str	r3, [sp, #4]
	mov	r3, #96
	str	r3, [sp, #8]
	add	r1, sp, #56
	mov	r2, #14
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, fp
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r5, #248
	mov	r2, #4
	str	r3, [sp, #4]
	str	r2, [sp, #8]
	mov	r3, r7
	add	r1, sp, #56
	mov	r2, #15
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	fp, ip, fp
	mov	r0, r4
	str	ip, [sp, #28]
	str	fp, [sp, #12]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r7, ip, r0
	cmp	r7, #32
	bls	.L423
	b	.L425
.L416:
	mov	r3, #0
	str	r3, [r6, #0]
	b	.L419
.L425:
	ldr	r3, [sp, #68]
	ldr	r0, [sp, #64]
	add	r3, r3, #1
	add	r1, sp, #56
	mov	r2, #4
	str	r3, [sp, #68]
	bl	memcpy
	ldr	r3, [sp, #32]
	add	r3, r3, r7
	str	r3, [sp, #32]
	b	.L415
.L423:
	ldr	r3, [r4, #0]
	sub	r3, r3, #32
	str	r3, [r4, #0]
.L415:
	mov	r1, r5
	ldr	r0, .L426+12
	bl	nm_ListGetNext
	mov	r5, r0
.L414:
	cmp	r5, #0
	bne	.L420
.L419:
	ldr	r3, [sp, #68]
	cmp	r3, #0
	beq	.L421
	ldr	r0, [sp, #60]
	add	r1, sp, #68
	mov	r2, #4
	bl	memcpy
	b	.L413
.L421:
	ldr	r2, [r4, #0]
	sub	r2, r2, #4
	str	r2, [r4, #0]
.L422:
	str	r3, [sp, #32]
.L413:
	ldr	r3, .L426
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [sp, #32]
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L427:
	.align	2
.L426:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	1041
	.word	.LANCHOR0
.LFE24:
	.size	POC_build_data_to_send, .-POC_build_data_to_send
	.section	.text.POC_clean_house_processing,"ax",%progbits
	.align	2
	.global	POC_clean_house_processing
	.type	POC_clean_house_processing, %function
POC_clean_house_processing:
.LFB65:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L432
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI92:
	ldr	r2, .L432+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L432+8
	bl	xQueueTakeMutexRecursive_debug
	bl	POC_get_list_count
	mov	r1, r0
	ldr	r0, .L432+12
	bl	Alert_Message_va
	ldr	r0, .L432+16
	ldr	r1, .L432+4
	ldr	r2, .L432+20
	b	.L431
.L430:
	ldr	r1, .L432+4
	mov	r2, #3216
	bl	mem_free_debug
	ldr	r0, .L432+16
	ldr	r1, .L432+4
	ldr	r2, .L432+24
.L431:
	bl	nm_ListRemoveHead_debug
	cmp	r0, #0
	bne	.L430
	ldr	r4, .L432
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	bl	POC_PRESERVES_synchronize_preserves_to_file
	mov	r3, #352
	str	r3, [sp, #0]
	ldr	r3, [r4, #0]
	mov	r0, #1
	str	r3, [sp, #4]
	mov	r3, #11
	str	r3, [sp, #8]
	ldr	r1, .L432+28
	mov	r2, #5
	ldr	r3, .L432+16
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldmfd	sp!, {r1, r2, r3, r4, pc}
.L433:
	.align	2
.L432:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	3205
	.word	.LC26
	.word	.LANCHOR0
	.word	3212
	.word	3218
	.word	.LANCHOR1
.LFE65:
	.size	POC_clean_house_processing, .-POC_clean_house_processing
	.section	.text.POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token
	.type	POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token, %function
POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token:
.LFB66:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI93:
	ldr	r4, .L438
	ldr	r2, .L438+4
	ldr	r3, .L438+8
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	POC_get_list_count
	mov	r1, r0
	ldr	r0, .L438+12
	bl	Alert_Message_va
	ldr	r0, .L438+16
	bl	nm_ListGetFirst
	b	.L437
.L436:
	add	r0, r5, #228
	ldr	r1, [r4, #0]
	bl	SHARED_set_all_32_bit_change_bits
	ldr	r0, .L438+16
	mov	r1, r5
	bl	nm_ListGetNext
.L437:
	cmp	r0, #0
	mov	r5, r0
	bne	.L436
	ldr	r3, .L438
	ldr	r4, .L438+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L438+4
	ldr	r3, .L438+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L438+28
	ldr	r0, [r4, #0]
	mov	r2, #1
	str	r2, [r3, #444]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L439:
	.align	2
.L438:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	3399
	.word	.LC27
	.word	.LANCHOR0
	.word	comm_mngr_recursive_MUTEX
	.word	3423
	.word	comm_mngr
.LFE66:
	.size	POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token, .-POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token
	.section	.text.POC_on_all_pocs_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	POC_on_all_pocs_set_or_clear_commserver_change_bits
	.type	POC_on_all_pocs_set_or_clear_commserver_change_bits, %function
POC_on_all_pocs_set_or_clear_commserver_change_bits:
.LFB67:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI94:
	ldr	r4, .L446
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L446+4
	ldr	r3, .L446+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L446+12
	bl	nm_ListGetFirst
	b	.L445
.L444:
	cmp	r5, #51
	ldr	r1, [r4, #0]
	add	r0, r6, #232
	bne	.L442
	bl	SHARED_clear_all_32_bit_change_bits
	b	.L443
.L442:
	bl	SHARED_set_all_32_bit_change_bits
.L443:
	ldr	r0, .L446+12
	mov	r1, r6
	bl	nm_ListGetNext
.L445:
	cmp	r0, #0
	mov	r6, r0
	bne	.L444
	ldr	r3, .L446
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L447:
	.align	2
.L446:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	3435
	.word	.LANCHOR0
.LFE67:
	.size	POC_on_all_pocs_set_or_clear_commserver_change_bits, .-POC_on_all_pocs_set_or_clear_commserver_change_bits
	.section	.text.nm_POC_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_POC_update_pending_change_bits
	.type	nm_POC_update_pending_change_bits, %function
nm_POC_update_pending_change_bits:
.LFB68:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L455
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI95:
	ldr	r2, .L455+4
	mov	r1, #400
	mov	r7, r0
	ldr	r0, [r3, #0]
	mov	r3, #3472
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L455+8
	bl	nm_ListGetFirst
	mov	r8, #0
	ldr	r6, .L455+12
	mov	r5, #1
	mov	r4, r0
	b	.L449
.L452:
	ldr	r3, [r4, #236]
	cmp	r3, #0
	beq	.L450
	cmp	r7, #0
	beq	.L451
	ldr	r2, [r4, #232]
	mov	r1, #2
	orr	r3, r2, r3
	str	r3, [r4, #232]
	ldr	r3, .L455+16
	ldr	r2, .L455+20
	str	r5, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L455+24
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L454
.L451:
	add	r0, r4, #236
	ldr	r1, [r6, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L454:
	mov	r8, #1
.L450:
	mov	r1, r4
	ldr	r0, .L455+8
	bl	nm_ListGetNext
	mov	r4, r0
.L449:
	cmp	r4, #0
	bne	.L452
	ldr	r3, .L455
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r8, #0
	beq	.L448
	mov	r0, #11
	mov	r1, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L448:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L456:
	.align	2
.L455:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	weather_preserves
	.word	60000
	.word	cics
.LFE68:
	.size	nm_POC_update_pending_change_bits, .-nm_POC_update_pending_change_bits
	.section	.text.POC_load_fields_syncd_to_the_poc_preserves,"ax",%progbits
	.align	2
	.global	POC_load_fields_syncd_to_the_poc_preserves
	.type	POC_load_fields_syncd_to_the_poc_preserves, %function
POC_load_fields_syncd_to_the_poc_preserves:
.LFB69:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI96:
	mov	r2, #48
	mov	r5, r0
	mov	r4, r1
	mov	r0, r1
	mov	r1, #0
	bl	memset
	cmp	r5, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldr	r6, .L459
	mov	r1, #400
	ldr	r2, .L459+4
	ldr	r0, [r6, #0]
	ldr	r3, .L459+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #16]
	ldr	r0, [r5, #72]
	str	r3, [r4, #0]
	ldr	r3, [r5, #76]
	str	r3, [r4, #4]
	ldr	r3, [r5, #80]
	str	r3, [r4, #8]
	ldr	r3, [r5, #96]
	str	r3, [r4, #12]
	ldr	r3, [r5, #112]
	str	r3, [r4, #16]
	ldr	r3, [r5, #84]
	str	r3, [r4, #20]
	ldr	r3, [r5, #116]
	str	r3, [r4, #32]
	ldr	r3, [r5, #88]
	str	r3, [r4, #24]
	ldr	r3, [r5, #120]
	str	r3, [r4, #36]
	ldr	r3, [r5, #92]
	str	r3, [r4, #28]
	ldr	r3, [r5, #124]
	str	r3, [r4, #40]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	str	r0, [r4, #44]
	ldr	r0, [r6, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L460:
	.align	2
.L459:
	.word	list_poc_recursive_MUTEX
	.word	.LC24
	.word	3547
.LFE69:
	.size	POC_load_fields_syncd_to_the_poc_preserves, .-POC_load_fields_syncd_to_the_poc_preserves
	.section	.text.POC_calculate_chain_sync_crc,"ax",%progbits
	.align	2
	.global	POC_calculate_chain_sync_crc
	.type	POC_calculate_chain_sync_crc, %function
POC_calculate_chain_sync_crc:
.LFB70:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L467
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI97:
	ldr	r5, .L467+4
	mov	r1, #400
	ldr	r2, .L467+8
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L467+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #8]
	mov	r0, #352
	mul	r0, r3, r0
	mov	r1, sp
	ldr	r2, .L467+8
	mov	r3, #3616
	bl	mem_oabia
	subs	r6, r0, #0
	beq	.L462
	ldr	r3, [sp, #0]
	add	r6, sp, #8
	mov	r0, r5
	str	r3, [r6, #-4]!
	bl	nm_ListGetFirst
	mov	r7, #0
	mov	r5, r0
	b	.L463
.L465:
	ldr	r3, [r5, #96]
	cmp	r3, #0
	beq	.L464
	add	r8, r5, #20
	mov	r0, r8
	bl	strlen
	mov	r1, r8
	mov	r2, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #72
	mov	r2, #4
	mov	r8, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #76
	mov	r2, #4
	add	r0, r8, r0
	add	r7, r0, r7
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #80
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #84
	mov	r2, #12
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #96
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #112
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #116
	mov	r2, #12
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #164
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #176
	mov	r2, #48
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #240
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #244
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #248
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #256
	mov	r2, #96
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r7, r7, r0
.L464:
	mov	r1, r5
	ldr	r0, .L467+4
	bl	nm_ListGetNext
	mov	r5, r0
.L463:
	cmp	r5, #0
	bne	.L465
	mov	r1, r7
	ldr	r0, [sp, #0]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L467+16
	add	r4, r4, #43
	ldr	r1, .L467+8
	ldr	r2, .L467+20
	mov	r6, #1
	str	r0, [r3, r4, asl #2]
	ldr	r0, [sp, #0]
	bl	mem_free_debug
	b	.L466
.L462:
	ldr	r3, .L467+24
	ldr	r0, .L467+28
	ldr	r1, [r3, r4, asl #4]
	bl	Alert_Message_va
.L466:
	ldr	r3, .L467
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L468:
	.align	2
.L467:
	.word	list_poc_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC24
	.word	3604
	.word	cscs
	.word	3708
	.word	chain_sync_file_pertinants
	.word	.LC28
.LFE70:
	.size	POC_calculate_chain_sync_crc, .-POC_calculate_chain_sync_crc
	.global	poc_list_item_sizes
	.global	POC_DEFAULT_NAME
	.section	.rodata.POC_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	POC_FILENAME, %object
	.size	POC_FILENAME, 4
POC_FILENAME:
	.ascii	"POC\000"
	.section	.rodata.poc_list_item_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	poc_list_item_sizes, %object
	.size	poc_list_item_sizes, 24
poc_list_item_sizes:
	.word	236
	.word	236
	.word	240
	.word	304
	.word	352
	.word	352
	.section	.bss.poc_group_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	poc_group_list_hdr, %object
	.size	poc_group_list_hdr, 20
poc_group_list_hdr:
	.space	20
	.section	.rodata.FLOW_METER_OFFSETS,"a",%progbits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	FLOW_METER_OFFSETS, %object
	.size	FLOW_METER_OFFSETS, 72
FLOW_METER_OFFSETS:
	.word	0
	.word	1048970869
	.word	1042864365
	.word	1035382397
	.word	-1096693055
	.word	0
	.word	1041428906
	.word	1047032496
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"BudgetCalcPercentOfET\000"
.LC1:
	.ascii	"BudgetGallonsEntryOption\000"
.LC2:
	.ascii	"NumberOfLevels\000"
.LC3:
	.ascii	"TypeOfPOC\000"
.LC4:
	.ascii	"POC_Usage\000"
.LC5:
	.ascii	"BoxIndex\000"
.LC6:
	.ascii	"HasPumpAttached\000"
.LC7:
	.ascii	"POC_CardConnected\000"
.LC8:
	.ascii	"%s%d\000"
.LC9:
	.ascii	"BudgetGallons\000"
.LC10:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/shared_poc.c\000"
.LC11:
	.ascii	"POC file unexpd update %u\000"
.LC12:
	.ascii	"POC file update : to revision %u from %u\000"
.LC13:
	.ascii	"poc type %d bitfield %x\000"
.LC14:
	.ascii	"POC updater error\000"
.LC15:
	.ascii	"%sChoice%d\000"
.LC16:
	.ascii	"FlowMeter\000"
.LC17:
	.ascii	"%sK_Value%d\000"
.LC18:
	.ascii	"%sReedSwitch%d\000"
.LC19:
	.ascii	"DecoderSerialNumber\000"
.LC20:
	.ascii	"MasterValveType\000"
.LC21:
	.ascii	"%sOffset_Value%d\000"
.LC22:
	.ascii	"SystemGID\000"
.LC23:
	.ascii	"Terminal Strip @ %c\000"
.LC24:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/poc.c\000"
.LC25:
	.ascii	"ERROR: NO MATCHING POC FOUND\000"
.LC26:
	.ascii	"POC, clean house list count = %d\000"
.LC27:
	.ascii	" POC count to distribute = %d\000"
.LC28:
	.ascii	"SYNC: no mem to calc checksum %s\000"
	.section	.rodata.POC_DEFAULT_NAME,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	POC_DEFAULT_NAME, %object
	.size	POC_DEFAULT_NAME, 4
POC_DEFAULT_NAME:
	.ascii	"POC\000"
	.section	.rodata.FLOW_METER_KVALUES,"a",%progbits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	FLOW_METER_KVALUES, %object
	.size	FLOW_METER_KVALUES, 72
FLOW_METER_KVALUES:
	.word	0
	.word	1053520850
	.word	1061401678
	.word	1065900657
	.word	1071200076
	.word	1076874969
	.word	1077277203
	.word	1090843050
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.section	.bss.g_POC_system_gid,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_POC_system_gid, %object
	.size	g_POC_system_gid, 4
g_POC_system_gid:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI0-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI2-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI4-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI6-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI8-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI10-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI12-.LFB16
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI14-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI16-.LFB15
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI18-.LFB19
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x83
	.uleb128 0x9
	.byte	0x82
	.uleb128 0xa
	.byte	0x81
	.uleb128 0xb
	.byte	0x80
	.uleb128 0xc
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.byte	0x4
	.4byte	.LCFI19-.LFB72
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI20-.LCFI19
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.byte	0x4
	.4byte	.LCFI21-.LFB73
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.byte	0x4
	.4byte	.LCFI23-.LFB74
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.byte	0x4
	.4byte	.LCFI25-.LFB75
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI26-.LCFI25
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.byte	0x4
	.4byte	.LCFI27-.LFB76
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI29-.LFB2
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.byte	0x4
	.4byte	.LCFI31-.LFB78
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI32-.LCFI31
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI33-.LFB12
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI35-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI36-.LFB22
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI38-.LFB20
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI39-.LFB21
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI40-.LFB32
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI41-.LFB33
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI42-.LFB34
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI43-.LFB35
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI44-.LFB36
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI45-.LFB37
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI46-.LFB31
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI47-.LFB26
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI48-.LCFI47
	.byte	0xe
	.uleb128 0x80
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI49-.LFB25
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI50-.LFB38
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI51-.LFB39
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI52-.LFB40
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI53-.LFB41
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI54-.LFB42
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI55-.LFB43
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.byte	0x4
	.4byte	.LCFI56-.LFB44
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI57-.LCFI56
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI58-.LCFI57
	.byte	0xe
	.uleb128 0x70
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.byte	0x4
	.4byte	.LCFI59-.LFB45
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.byte	0x4
	.4byte	.LCFI60-.LFB46
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI61-.LCFI60
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.byte	0x4
	.4byte	.LCFI62-.LFB47
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.byte	0x4
	.4byte	.LCFI63-.LFB48
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI64-.LFB27
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.byte	0x4
	.4byte	.LCFI65-.LFB50
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE86:
.LSFDE88:
	.4byte	.LEFDE88-.LASFDE88
.LASFDE88:
	.4byte	.Lframe0
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.byte	0x4
	.4byte	.LCFI66-.LFB51
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE88:
.LSFDE90:
	.4byte	.LEFDE90-.LASFDE90
.LASFDE90:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI67-.LFB23
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE90:
.LSFDE92:
	.4byte	.LEFDE92-.LASFDE92
.LASFDE92:
	.4byte	.Lframe0
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.byte	0x4
	.4byte	.LCFI68-.LFB52
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE92:
.LSFDE94:
	.4byte	.LEFDE94-.LASFDE94
.LASFDE94:
	.4byte	.Lframe0
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.byte	0x4
	.4byte	.LCFI69-.LFB53
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE94:
.LSFDE96:
	.4byte	.LEFDE96-.LASFDE96
.LASFDE96:
	.4byte	.Lframe0
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.byte	0x4
	.4byte	.LCFI70-.LFB54
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE96:
.LSFDE98:
	.4byte	.LEFDE98-.LASFDE98
.LASFDE98:
	.4byte	.Lframe0
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.byte	0x4
	.4byte	.LCFI71-.LFB55
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE98:
.LSFDE100:
	.4byte	.LEFDE100-.LASFDE100
.LASFDE100:
	.4byte	.Lframe0
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.byte	0x4
	.4byte	.LCFI72-.LFB56
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE100:
.LSFDE102:
	.4byte	.LEFDE102-.LASFDE102
.LASFDE102:
	.4byte	.Lframe0
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.byte	0x4
	.4byte	.LCFI73-.LFB57
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE102:
.LSFDE104:
	.4byte	.LEFDE104-.LASFDE104
.LASFDE104:
	.4byte	.Lframe0
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.byte	0x4
	.4byte	.LCFI74-.LFB58
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE104:
.LSFDE106:
	.4byte	.LEFDE106-.LASFDE106
.LASFDE106:
	.4byte	.Lframe0
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.byte	0x4
	.4byte	.LCFI75-.LFB59
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE106:
.LSFDE108:
	.4byte	.LEFDE108-.LASFDE108
.LASFDE108:
	.4byte	.Lframe0
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.byte	0x4
	.4byte	.LCFI76-.LFB60
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI77-.LCFI76
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE108:
.LSFDE110:
	.4byte	.LEFDE110-.LASFDE110
.LASFDE110:
	.4byte	.Lframe0
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.byte	0x4
	.4byte	.LCFI78-.LFB61
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE110:
.LSFDE112:
	.4byte	.LEFDE112-.LASFDE112
.LASFDE112:
	.4byte	.Lframe0
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.byte	0x4
	.4byte	.LCFI79-.LFB62
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x83
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE112:
.LSFDE114:
	.4byte	.LEFDE114-.LASFDE114
.LASFDE114:
	.4byte	.Lframe0
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.byte	0x4
	.4byte	.LCFI80-.LFB63
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI81-.LCFI80
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE114:
.LSFDE116:
	.4byte	.LEFDE116-.LASFDE116
.LASFDE116:
	.4byte	.Lframe0
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.align	2
.LEFDE116:
.LSFDE118:
	.4byte	.LEFDE118-.LASFDE118
.LASFDE118:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI82-.LFB17
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI83-.LCFI82
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE118:
.LSFDE120:
	.4byte	.LEFDE120-.LASFDE120
.LASFDE120:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI84-.LFB30
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x83
	.uleb128 0x9
	.byte	0x82
	.uleb128 0xa
	.byte	0x81
	.uleb128 0xb
	.byte	0x80
	.uleb128 0xc
	.align	2
.LEFDE120:
.LSFDE122:
	.4byte	.LEFDE122-.LASFDE122
.LASFDE122:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI85-.LFB29
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE122:
.LSFDE124:
	.4byte	.LEFDE124-.LASFDE124
.LASFDE124:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI86-.LFB18
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI87-.LCFI86
	.byte	0xe
	.uleb128 0x60
	.align	2
.LEFDE124:
.LSFDE126:
	.4byte	.LEFDE126-.LASFDE126
.LASFDE126:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI88-.LFB28
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI89-.LCFI88
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE126:
.LSFDE128:
	.4byte	.LEFDE128-.LASFDE128
.LASFDE128:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI90-.LFB24
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI91-.LCFI90
	.byte	0xe
	.uleb128 0x6c
	.align	2
.LEFDE128:
.LSFDE130:
	.4byte	.LEFDE130-.LASFDE130
.LASFDE130:
	.4byte	.Lframe0
	.4byte	.LFB65
	.4byte	.LFE65-.LFB65
	.byte	0x4
	.4byte	.LCFI92-.LFB65
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE130:
.LSFDE132:
	.4byte	.LEFDE132-.LASFDE132
.LASFDE132:
	.4byte	.Lframe0
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.byte	0x4
	.4byte	.LCFI93-.LFB66
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE132:
.LSFDE134:
	.4byte	.LEFDE134-.LASFDE134
.LASFDE134:
	.4byte	.Lframe0
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.byte	0x4
	.4byte	.LCFI94-.LFB67
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE134:
.LSFDE136:
	.4byte	.LEFDE136-.LASFDE136
.LASFDE136:
	.4byte	.Lframe0
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.byte	0x4
	.4byte	.LCFI95-.LFB68
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE136:
.LSFDE138:
	.4byte	.LEFDE138-.LASFDE138
.LASFDE138:
	.4byte	.Lframe0
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.byte	0x4
	.4byte	.LCFI96-.LFB69
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE138:
.LSFDE140:
	.4byte	.LEFDE140-.LASFDE140
.LASFDE140:
	.4byte	.Lframe0
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.byte	0x4
	.4byte	.LCFI97-.LFB70
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE140:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/shared_poc.c"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/poc.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x61c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF71
	.byte	0x1
	.4byte	.LASF72
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x565
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x347
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x3a3
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x435
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x2
	.2byte	0xa9c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x2e2
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1ae
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x3ff
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x4ad
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x538
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x50b
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x46b
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x28c
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x23b
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x15d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST5
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x5c4
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST6
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x1ed
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST7
	.uleb128 0x5
	.4byte	0x21
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST8
	.uleb128 0x6
	.4byte	.LASF18
	.byte	0x2
	.2byte	0x247
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST9
	.uleb128 0x5
	.4byte	0x2a
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LLST10
	.uleb128 0x5
	.4byte	0x33
	.4byte	.LFB73
	.4byte	.LFE73
	.4byte	.LLST11
	.uleb128 0x5
	.4byte	0x3c
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LLST12
	.uleb128 0x5
	.4byte	0x45
	.4byte	.LFB75
	.4byte	.LFE75
	.4byte	.LLST13
	.uleb128 0x5
	.4byte	0x4e
	.4byte	.LFB76
	.4byte	.LFE76
	.4byte	.LLST14
	.uleb128 0x5
	.4byte	0x57
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST15
	.uleb128 0x5
	.4byte	0x61
	.4byte	.LFB78
	.4byte	.LFE78
	.4byte	.LLST16
	.uleb128 0x5
	.4byte	0x6a
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST17
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x11b
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST18
	.uleb128 0x6
	.4byte	.LASF19
	.byte	0x2
	.2byte	0x326
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST19
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF20
	.byte	0x2
	.2byte	0x2f5
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST20
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF21
	.byte	0x2
	.2byte	0x305
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST21
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF22
	.byte	0x2
	.2byte	0x790
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST22
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF23
	.byte	0x2
	.2byte	0x7c6
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST23
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF24
	.byte	0x2
	.2byte	0x7da
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST24
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF25
	.byte	0x2
	.2byte	0x7f8
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST25
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF26
	.byte	0x2
	.2byte	0x80c
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST26
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF27
	.byte	0x2
	.2byte	0x85e
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST27
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF28
	.byte	0x2
	.2byte	0x77f
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST28
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF29
	.byte	0x2
	.2byte	0x5ab
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST29
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF30
	.byte	0x2
	.2byte	0x571
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST30
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF31
	.byte	0x2
	.2byte	0x87e
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST31
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF32
	.byte	0x2
	.2byte	0x8aa
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST32
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF33
	.byte	0x2
	.2byte	0x8cd
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST33
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF34
	.byte	0x2
	.2byte	0x8f0
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST34
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF35
	.byte	0x2
	.2byte	0x911
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST35
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF36
	.byte	0x2
	.2byte	0x936
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST36
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF37
	.byte	0x2
	.2byte	0x961
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LLST37
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF38
	.byte	0x2
	.2byte	0x9e7
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LLST38
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF39
	.byte	0x2
	.2byte	0xa14
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LLST39
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF40
	.byte	0x2
	.2byte	0xa5c
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LLST40
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF41
	.byte	0x2
	.2byte	0xa82
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LLST41
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF42
	.byte	0x2
	.2byte	0x603
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST42
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF43
	.byte	0x2
	.2byte	0xabe
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LLST43
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF44
	.byte	0x2
	.2byte	0xacd
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LLST44
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF45
	.byte	0x2
	.2byte	0x39c
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST45
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF46
	.byte	0x2
	.2byte	0xae4
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LLST46
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF47
	.byte	0x2
	.2byte	0xb1a
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LLST47
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF48
	.byte	0x2
	.2byte	0xb2d
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LLST48
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF49
	.byte	0x2
	.2byte	0xb50
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LLST49
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF50
	.byte	0x2
	.2byte	0xb73
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LLST50
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF51
	.byte	0x2
	.2byte	0xb88
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LLST51
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF52
	.byte	0x2
	.2byte	0xbb3
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LLST52
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF53
	.byte	0x2
	.2byte	0xbdb
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LLST53
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF54
	.byte	0x2
	.2byte	0xbfe
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LLST54
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF55
	.byte	0x2
	.2byte	0xc17
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LLST55
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF56
	.byte	0x2
	.2byte	0xc2b
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LLST56
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF57
	.byte	0x2
	.2byte	0xc3f
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LLST57
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF58
	.byte	0x2
	.2byte	0xc71
	.4byte	.LFB64
	.4byte	.LFE64
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF59
	.byte	0x1
	.2byte	0x60f
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST58
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF60
	.byte	0x2
	.2byte	0x738
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST59
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF61
	.byte	0x2
	.2byte	0x69b
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST60
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x727
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST61
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF63
	.byte	0x2
	.2byte	0x679
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST62
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF64
	.byte	0x2
	.2byte	0x3ea
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST63
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF65
	.byte	0x2
	.2byte	0xc77
	.4byte	.LFB65
	.4byte	.LFE65
	.4byte	.LLST64
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF66
	.byte	0x2
	.2byte	0xd41
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LLST65
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF67
	.byte	0x2
	.2byte	0xd67
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	.LLST66
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF68
	.byte	0x2
	.2byte	0xd88
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LLST67
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF69
	.byte	0x2
	.2byte	0xdc6
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LLST68
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF70
	.byte	0x2
	.2byte	0xdfd
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	.LLST69
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB14
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB13
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB11
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI5
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI9
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB1
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI11
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB16
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB3
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI15
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI17
	.4byte	.LFE15
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB19
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB72
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI20
	.4byte	.LFE72
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB73
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI22
	.4byte	.LFE73
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB74
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI24
	.4byte	.LFE74
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB75
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI26
	.4byte	.LFE75
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB76
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI28
	.4byte	.LFE76
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB2
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI30
	.4byte	.LFE2
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB78
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI32
	.4byte	.LFE78
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB12
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI34
	.4byte	.LFE12
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB0
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB22
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI37
	.4byte	.LFE22
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB20
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB21
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB32
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB33
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB34
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB35
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB36
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB37
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB31
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB26
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI48
	.4byte	.LFE26
	.2byte	0x3
	.byte	0x7d
	.sleb128 128
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB25
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI49
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB38
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB39
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB40
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI52
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB41
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB42
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB43
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI55
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB44
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI57
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI58
	.4byte	.LFE44
	.2byte	0x3
	.byte	0x7d
	.sleb128 112
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB45
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI59
	.4byte	.LFE45
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB46
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI61
	.4byte	.LFE46
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB47
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI62
	.4byte	.LFE47
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB48
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LFE48
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB27
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI64
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB50
	.4byte	.LCFI65
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI65
	.4byte	.LFE50
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST44:
	.4byte	.LFB51
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI66
	.4byte	.LFE51
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST45:
	.4byte	.LFB23
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI67
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST46:
	.4byte	.LFB52
	.4byte	.LCFI68
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI68
	.4byte	.LFE52
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST47:
	.4byte	.LFB53
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI69
	.4byte	.LFE53
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST48:
	.4byte	.LFB54
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI70
	.4byte	.LFE54
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST49:
	.4byte	.LFB55
	.4byte	.LCFI71
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI71
	.4byte	.LFE55
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST50:
	.4byte	.LFB56
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI72
	.4byte	.LFE56
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST51:
	.4byte	.LFB57
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI73
	.4byte	.LFE57
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST52:
	.4byte	.LFB58
	.4byte	.LCFI74
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI74
	.4byte	.LFE58
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST53:
	.4byte	.LFB59
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI75
	.4byte	.LFE59
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST54:
	.4byte	.LFB60
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI76
	.4byte	.LCFI77
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI77
	.4byte	.LFE60
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST55:
	.4byte	.LFB61
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI78
	.4byte	.LFE61
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST56:
	.4byte	.LFB62
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI79
	.4byte	.LFE62
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST57:
	.4byte	.LFB63
	.4byte	.LCFI80
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI80
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI81
	.4byte	.LFE63
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST58:
	.4byte	.LFB17
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI82
	.4byte	.LCFI83
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI83
	.4byte	.LFE17
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST59:
	.4byte	.LFB30
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI84
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST60:
	.4byte	.LFB29
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI85
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST61:
	.4byte	.LFB18
	.4byte	.LCFI86
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI86
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI87
	.4byte	.LFE18
	.2byte	0x3
	.byte	0x7d
	.sleb128 96
	.4byte	0
	.4byte	0
.LLST62:
	.4byte	.LFB28
	.4byte	.LCFI88
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI88
	.4byte	.LCFI89
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI89
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST63:
	.4byte	.LFB24
	.4byte	.LCFI90
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI90
	.4byte	.LCFI91
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI91
	.4byte	.LFE24
	.2byte	0x3
	.byte	0x7d
	.sleb128 108
	.4byte	0
	.4byte	0
.LLST64:
	.4byte	.LFB65
	.4byte	.LCFI92
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI92
	.4byte	.LFE65
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST65:
	.4byte	.LFB66
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI93
	.4byte	.LFE66
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST66:
	.4byte	.LFB67
	.4byte	.LCFI94
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI94
	.4byte	.LFE67
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST67:
	.4byte	.LFB68
	.4byte	.LCFI95
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI95
	.4byte	.LFE68
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST68:
	.4byte	.LFB69
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI96
	.4byte	.LFE69
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST69:
	.4byte	.LFB70
	.4byte	.LCFI97
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI97
	.4byte	.LFE70
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB65
	.4byte	.LFE65-.LFB65
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LFB73
	.4byte	.LFE73
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LFB75
	.4byte	.LFE75
	.4byte	.LFB76
	.4byte	.LFE76
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB78
	.4byte	.LFE78
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB65
	.4byte	.LFE65
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF67:
	.ascii	"POC_on_all_pocs_set_or_clear_commserver_change_bits"
	.ascii	"\000"
.LASF49:
	.ascii	"POC_get_budget_gallons_entry_option\000"
.LASF7:
	.ascii	"nm_POC_set_decoder_serial_number\000"
.LASF3:
	.ascii	"nm_POC_set_reed_switch\000"
.LASF19:
	.ascii	"nm_POC_set_default_values\000"
.LASF43:
	.ascii	"POC_get_decoder_serial_number_for_this_poc_and_leve"
	.ascii	"l_0\000"
.LASF32:
	.ascii	"POC_get_index_for_group_with_this_GID\000"
.LASF37:
	.ascii	"POC_get_fm_choice_and_rate_details\000"
.LASF36:
	.ascii	"POC_get_GID_irrigation_system_using_POC_gid\000"
.LASF22:
	.ascii	"POC_get_index_using_ptr_to_poc_struct\000"
.LASF60:
	.ascii	"BUDGETS_extract_and_store_changes_from_GuiVars\000"
.LASF6:
	.ascii	"nm_POC_set_offset\000"
.LASF4:
	.ascii	"nm_get_decoder_serial_number_support\000"
.LASF31:
	.ascii	"POC_get_gid_of_group_at_this_index\000"
.LASF70:
	.ascii	"POC_calculate_chain_sync_crc\000"
.LASF50:
	.ascii	"POC_get_budget_percent_ET\000"
.LASF69:
	.ascii	"POC_load_fields_syncd_to_the_poc_preserves\000"
.LASF63:
	.ascii	"POC_extract_and_store_group_name_from_GuiVars\000"
.LASF51:
	.ascii	"POC_show_poc_menu_items\000"
.LASF61:
	.ascii	"POC_extract_and_store_changes_from_GuiVars\000"
.LASF57:
	.ascii	"POC_use_for_budget\000"
.LASF46:
	.ascii	"POC_get_decoder_serial_number_index_0\000"
.LASF65:
	.ascii	"POC_clean_house_processing\000"
.LASF53:
	.ascii	"POC_at_least_one_POC_is_a_bypass_manifold\000"
.LASF56:
	.ascii	"POC_set_budget\000"
.LASF72:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/poc.c\000"
.LASF28:
	.ascii	"nm_POC_load_group_name_into_guivar\000"
.LASF18:
	.ascii	"nm_poc_structure_updater\000"
.LASF1:
	.ascii	"nm_POC_set_fm_type\000"
.LASF58:
	.ascii	"POC_get_change_bits_ptr\000"
.LASF25:
	.ascii	"nm_POC_get_pointer_to_poc_with_this_gid_and_box_ind"
	.ascii	"ex\000"
.LASF68:
	.ascii	"nm_POC_update_pending_change_bits\000"
.LASF44:
	.ascii	"POC_get_decoder_serial_number_for_this_poc_gid_and_"
	.ascii	"level_0\000"
.LASF11:
	.ascii	"nm_POC_set_bypass_number_of_levels\000"
.LASF59:
	.ascii	"nm_POC_store_changes\000"
.LASF23:
	.ascii	"nm_POC_get_pointer_to_terminal_poc_with_this_box_in"
	.ascii	"dex_0\000"
.LASF41:
	.ascii	"POC_get_type_of_poc\000"
.LASF30:
	.ascii	"POC_copy_group_into_guivars\000"
.LASF39:
	.ascii	"POC_get_master_valve_NO_or_NC\000"
.LASF48:
	.ascii	"POC_get_bypass_number_of_levels\000"
.LASF55:
	.ascii	"POC_get_has_pump_attached\000"
.LASF45:
	.ascii	"nm_POC_create_new_group\000"
.LASF2:
	.ascii	"nm_POC_set_kvalue\000"
.LASF24:
	.ascii	"nm_POC_get_pointer_to_bypass_poc_with_this_box_inde"
	.ascii	"x_0\000"
.LASF33:
	.ascii	"POC_get_last_group_ID\000"
.LASF21:
	.ascii	"save_file_POC\000"
.LASF66:
	.ascii	"POC_set_bits_on_all_pocs_to_cause_distribution_in_t"
	.ascii	"he_next_token\000"
.LASF16:
	.ascii	"nm_POC_set_show_this_poc\000"
.LASF42:
	.ascii	"POC_copy_preserve_info_into_guivars\000"
.LASF38:
	.ascii	"POC_get_usage\000"
.LASF5:
	.ascii	"nm_POC_set_mv_type\000"
.LASF17:
	.ascii	"nm_POC_set_name\000"
.LASF8:
	.ascii	"nm_POC_set_GID_irrigation_system\000"
.LASF14:
	.ascii	"nm_POC_set_box_index\000"
.LASF52:
	.ascii	"POC_at_least_one_POC_has_a_flow_meter\000"
.LASF62:
	.ascii	"nm_POC_extract_and_store_changes_from_comm\000"
.LASF54:
	.ascii	"POC_get_budget\000"
.LASF47:
	.ascii	"POC_get_show_for_this_poc\000"
.LASF26:
	.ascii	"nm_POC_get_pointer_to_poc_for_this_box_index_0_and_"
	.ascii	"poc_type_and_decoder_sn\000"
.LASF9:
	.ascii	"nm_POC_set_budget_calculation_percent_of_et\000"
.LASF13:
	.ascii	"nm_POC_set_poc_usage\000"
.LASF10:
	.ascii	"nm_POC_set_budget_gallons_entry_option\000"
.LASF0:
	.ascii	"nm_POC_set_budget_gallons\000"
.LASF71:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF20:
	.ascii	"init_file_POC\000"
.LASF12:
	.ascii	"nm_POC_set_poc_type\000"
.LASF35:
	.ascii	"POC_get_GID_irrigation_system\000"
.LASF40:
	.ascii	"POC_get_box_index_0\000"
.LASF64:
	.ascii	"POC_build_data_to_send\000"
.LASF34:
	.ascii	"POC_get_list_count\000"
.LASF29:
	.ascii	"BUDGETS_copy_group_into_guivars\000"
.LASF27:
	.ascii	"POC_get_group_at_this_index\000"
.LASF15:
	.ascii	"nm_POC_set_has_pump_attached\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
