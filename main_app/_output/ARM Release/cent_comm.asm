	.file	"cent_comm.c"
	.text
.Ltext0:
	.section	.text.terminate_ci_transaction,"ax",%progbits
	.align	2
	.type	terminate_ci_transaction, %function
terminate_ci_transaction:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #107
	b	CONTROLLER_INITIATED_post_event
.LFE2:
	.size	terminate_ci_transaction, .-terminate_ci_transaction
	.section	.text.poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers,"ax",%progbits
	.align	2
	.type	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers, %function
poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L12
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r3, [r3, #4]
	cmp	r3, #1
	bne	.L3
	ldr	r0, .L12+4
	bl	Alert_Message
	mov	r0, #106
	bl	CONTROLLER_INITIATED_post_event
.L3:
	bl	CONFIG_this_controller_is_a_configured_hub
	subs	r4, r0, #0
	beq	.L4
	bl	CODE_DISTRIBUTION_hub_should_report_busy_to_the_commserver
	subs	r1, r0, #0
	ldreq	r0, .L12+8
	beq	.L11
	mov	r0, #420
	mov	r1, #0
	mov	r2, #256
	mov	r3, #1
	b	.L10
.L4:
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
.LBB96:
	ldr	r0, .L12+8
	mov	r1, r4
.L11:
	mov	r2, #256
	mov	r3, r1
.L10:
	bl	CONTROLLER_INITIATED_post_to_messages_queue
	mov	r0, #110
.LBE96:
	ldmfd	sp!, {r4, lr}
.LBB97:
	b	CONTROLLER_INITIATED_post_event
.L13:
	.align	2
.L12:
	.word	cics
	.word	.LC0
	.word	419
.LBE97:
.LFE3:
	.size	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers, .-poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers
	.section	.text.CENT_COMM_build_and_send_ack_with_optional_job_number,"ax",%progbits
	.align	2
	.global	CENT_COMM_build_and_send_ack_with_optional_job_number
	.type	CENT_COMM_build_and_send_ack_with_optional_job_number, %function
CENT_COMM_build_and_send_ack_with_optional_job_number:
.LFB1:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI1:
	sub	sp, sp, #40
.LCFI2:
	str	r1, [sp, #8]
	mov	r9, r2
	ldr	r1, .L20
	mov	r2, #120
	mov	fp, r0
	mov	r0, #32
	str	r3, [sp, #12]
	bl	mem_malloc_debug
	ldr	r3, .L20+4
	cmp	r9, #0
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	moveq	r7, #28
	movne	r7, #32
	moveq	r5, #14
	movne	r5, #18
	strb	r2, [r0, #0]
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	mov	r4, r0
	strb	r2, [r0, #1]
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	strb	r2, [r0, #2]
	strb	r3, [r0, #3]
	add	r8, r0, #4
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	beq	.L16
	cmp	fp, #3
	beq	.L16
	mov	r1, #0
	mov	r2, #2
	add	r0, sp, #36
	bl	memset
	ldrb	r3, [sp, #36]	@ zero_extendqisi2
	mov	r0, r8
	orr	r3, r3, #30
	strb	r3, [sp, #36]
	add	r1, sp, #36
	mov	r3, #1
	mov	r2, #2
	strb	r3, [sp, #37]
	add	r6, r4, #6
	bl	memcpy
	add	sl, r5, #2
	b	.L17
.L16:
	sub	r7, r7, #2
	mov	sl, r5
	mov	r6, r8
.L17:
	mov	r1, #0
	mov	r2, #14
	add	r0, sp, #16
	bl	memset
	ldr	r3, .L20+8
	ldr	r3, [r3, #48]
	str	r3, [sp, #16]
	bl	NETWORK_CONFIG_get_network_id
	ldr	r3, [sp, #8]
	add	r1, sp, #16
	mov	r2, #14
	str	r5, [sp, #24]
	strh	r3, [sp, #28]	@ movhi
	add	r5, r6, #14
	str	r0, [sp, #20]
	mov	r0, r6
	bl	memcpy
	cmp	r9, #0
	beq	.L18
	mov	r0, r5
	add	r1, sp, #12
	mov	r2, #4
	bl	memcpy
	add	r5, r6, #18
.L18:
	mov	r1, sl
	mov	r0, r8
	bl	CRC_calculate_32bit_big_endian
	add	r1, sp, #40
	mov	r2, #4
	str	r0, [r1, #-8]!
	mov	r0, r5
	bl	memcpy
	ldr	r3, .L20+12
	mov	r1, r4
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	mov	r0, fp
	strb	r2, [r5, #4]
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	strb	r2, [r5, #5]
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	strb	r2, [r5, #6]
	mov	r2, #1
	strb	r3, [r5, #7]
	mov	r3, #0
	str	r2, [sp, #4]
	mov	r2, r7
	str	r3, [sp, #0]
	bl	AddCopyOfBlockToXmitList
	mov	r0, r4
	ldr	r1, .L20
	mov	r2, #235
	bl	mem_free_debug
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L21:
	.align	2
.L20:
	.word	.LC1
	.word	preamble
	.word	config_c
	.word	postamble
.LFE1:
	.size	CENT_COMM_build_and_send_ack_with_optional_job_number, .-CENT_COMM_build_and_send_ack_with_optional_job_number
	.section	.text.build_response_to_mobile_cmd_and_update_session_data,"ax",%progbits
	.align	2
	.type	build_response_to_mobile_cmd_and_update_session_data, %function
build_response_to_mobile_cmd_and_update_session_data:
.LFB4:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI3:
	mov	r2, r0
	mov	r0, #1
	mov	r3, r1
	mov	r1, r2
	mov	r2, r0
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	mov	r0, #142
	bl	CONTROLLER_INITIATED_post_event
	mov	r3, #140
	str	r3, [sp, #0]
	ldr	r3, .L23
	mov	r0, sp
	str	r3, [sp, #8]
	bl	CONTROLLER_INITIATED_post_event_with_details
	bl	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.L24:
	.align	2
.L23:
	.word	15000
.LFE4:
	.size	build_response_to_mobile_cmd_and_update_session_data, .-build_response_to_mobile_cmd_and_update_session_data
	.section	.text.process_incoming_request_to_send_registration,"ax",%progbits
	.align	2
	.type	process_incoming_request_to_send_registration, %function
process_incoming_request_to_send_registration:
.LFB44:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI4:
	mov	r4, r0
	bl	CONTROLLER_INITIATED_update_comm_server_registration_info
	mov	r1, #0
	ldr	r0, .L27
	mov	r2, #512
	mov	r3, r1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
	cmp	r4, #0
	beq	.L26
	mov	r2, #0
	mov	r0, #1
	mov	r1, #134
	mov	r3, r2
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
.L26:
	ldmfd	sp!, {r4, lr}
	b	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers
.L28:
	.align	2
.L27:
	.word	401
.LFE44:
	.size	process_incoming_request_to_send_registration, .-process_incoming_request_to_send_registration
	.section	.text.commserver_msg_receipt_error_timer_callback,"ax",%progbits
	.align	2
	.global	commserver_msg_receipt_error_timer_callback
	.type	commserver_msg_receipt_error_timer_callback, %function
commserver_msg_receipt_error_timer_callback:
.LFB64:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L31
	str	lr, [sp, #-4]!
.LCFI5:
	ldr	r0, [r3, #24]
	cmp	r0, #0
	beq	.L30
	ldr	r1, .L31+4
	ldr	r2, .L31+8
	bl	mem_free_debug
.L30:
	ldr	r0, .L31
	mov	r1, #0
	mov	r2, #28
	ldr	lr, [sp], #4
	b	memset
.L32:
	.align	2
.L31:
	.word	.LANCHOR0
	.word	.LC1
	.word	3542
.LFE64:
	.size	commserver_msg_receipt_error_timer_callback, .-commserver_msg_receipt_error_timer_callback
	.section	.text.CENT_COMM_process_incoming_packet_from_comm_server,"ax",%progbits
	.align	2
	.global	CENT_COMM_process_incoming_packet_from_comm_server
	.type	CENT_COMM_process_incoming_packet_from_comm_server, %function
CENT_COMM_process_incoming_packet_from_comm_server:
.LFB67:
	@ args = 0, pretend = 0, frame = 164
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r3, #2
	cmpeq	r0, #1
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI6:
	mov	ip, r0
	sub	sp, sp, #176
.LCFI7:
	mov	r4, r1
	mov	r5, r2
	bne	.L34
	add	r0, sp, #88
	mov	r2, #12
	bl	memcpy
	ldrh	r0, [sp, #98]
	cmp	r0, #106
	cmpne	r0, #76
	beq	.L35
	bl	Alert_comm_command_received
.L35:
	ldrh	r3, [sp, #98]
	cmp	r3, #75
	bne	.L36
.LBB175:
	ldr	r7, .L313
	ldr	r3, [r7, #24]
	cmp	r3, #0
	beq	.L37
	ldr	r0, .L313+4
	bl	Alert_Message
	ldr	r0, [r7, #24]
	ldr	r1, .L313+8
	ldr	r2, .L313+12
	bl	mem_free_debug
.L37:
	ldr	r5, .L313
	add	r1, r4, #12
	mov	r2, #12
	mov	r0, r5
	bl	memcpy
	mov	r4, #0
	ldrh	r0, [r7, #10]
	ldr	r1, [r7, #0]
	ldrh	r2, [r7, #8]
	bl	Alert_inbound_message_size
	mov	r6, #1
	str	r4, [r7, #16]
	str	r6, [r7, #12]
	str	r4, [r7, #20]
	ldr	r0, [r7, #0]
	add	r1, r5, #24
	ldr	r2, .L313+8
	ldr	r3, .L313+16
	bl	mem_oabia
	subs	r7, r0, #0
	bne	.L38
	ldr	r0, .L313+20
	bl	Alert_Message
	mov	r0, r5
	mov	r1, r7
	b	.L272
.L38:
	ldr	r3, [r5, #24]
	str	r3, [r5, #20]
	ldrh	r3, [r5, #10]
	cmp	r3, #75
	bne	.L40
	mov	r3, #76
	strh	r3, [r5, #10]	@ movhi
	mov	r0, r6
	mov	r1, #77
	mov	r2, r4
	mov	r3, r4
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	b	.L41
.L40:
	ldr	r0, .L313+24
	bl	Alert_Message
.L41:
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L313+28
	mov	r1, #2
	ldr	r0, [r3, #452]
	ldr	r2, .L313+32
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L33
.L36:
.LBE175:
	cmp	r3, #76
	bne	.L42
.LBB176:
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L313+28
	mov	r2, #0
	ldr	r6, .L313
	ldr	r0, [r3, #452]
	mov	r1, #1
	mov	r3, r2
	bl	xTimerGenericCommand
	add	r0, sp, #136
	add	r1, r4, #12
	mov	r2, #4
	bl	memcpy
	ldr	r3, [r6, #24]
	cmp	r3, #0
	beq	.L43
	ldr	r0, [r6, #20]
	cmp	r0, #0
	beq	.L43
	ldrh	r2, [r6, #10]
	ldrh	r3, [sp, #138]
	cmp	r2, r3
	ldrne	r0, .L313+36
	bne	.L252
	ldrh	r2, [sp, #136]
	ldr	r3, [r6, #12]
	cmp	r3, r2
	bne	.L45
	ldrh	r2, [r6, #8]
	cmp	r2, r3
	bcc	.L45
	ldr	r2, [r6, #16]
	ldr	r3, [r6, #0]
	sub	r5, r5, #20
	add	r2, r5, r2
	cmp	r2, r3
	ldrhi	r0, .L313+40
	bhi	.L252
	add	r1, r4, #16
	mov	r2, r5
	bl	memcpy
	ldr	r1, [r6, #16]
	ldrh	r2, [r6, #8]
	add	r1, r5, r1
	str	r1, [r6, #16]
	ldrh	r3, [sp, #136]
	cmp	r2, r3
	bne	.L47
	ldr	r3, [r6, #0]
	cmp	r1, r3
	bne	.L47
	ldr	r0, [r6, #24]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, [r6, #4]
	mov	r5, r6
	cmp	r0, r3
	ldrne	r0, .L313+44
	bne	.L251
	ldrh	r3, [r6, #10]
	ldr	r4, [r6, #24]
	cmp	r3, #76
	ldrne	r0, .L313+48
	bne	.L251
	mov	r0, #78
	bl	Alert_comm_command_sent
	mov	r2, #0
	mov	r3, r2
	mov	r0, #1
	mov	r1, #78
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
.LBB177:
	mov	r1, #1
	mov	r2, r1
	mov	r3, r1
	mov	r0, r4
	bl	PDATA_extract_and_store_changes
	ldr	r3, .L313+436
	mov	r2, #0
	strb	r2, [r3, #128]
	bl	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers
	b	.L50
.L251:
.LBE177:
	bl	Alert_Message
.L50:
	ldr	r3, .L313
	ldr	r0, [r3, #24]
	cmp	r0, #0
	beq	.L51
	ldr	r1, .L313+8
	ldr	r2, .L313+52
.L284:
	bl	mem_free_debug
.L51:
	ldr	r0, .L313
	mov	r1, #0
.L272:
	mov	r2, #28
	bl	memset
	b	.L33
.L47:
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L313+28
	ldr	r2, .L313+32
	ldr	r0, [r3, #452]
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	ldr	r3, .L313
	ldr	r2, [r3, #12]
	add	r2, r2, #1
	str	r2, [r3, #12]
	ldr	r2, [r3, #20]
	add	r5, r2, r5
	str	r5, [r3, #20]
	b	.L33
.L45:
	ldr	r0, .L313+56
.L252:
	bl	Alert_Message
.L43:
	ldr	r3, .L313
	ldr	r0, [r3, #24]
	cmp	r0, #0
	beq	.L51
	ldr	r1, .L313+8
	ldr	r2, .L313+60
	b	.L284
.L42:
.LBE176:
.LBB178:
.LBB179:
	add	r0, sp, #100
	mov	r1, r4
	mov	r2, #12
	bl	memcpy
	ldrh	r6, [sp, #110]
	cmp	r6, #137
	beq	.L89
	bhi	.L110
	cmp	r6, #86
	bhi	.L111
	cmp	r6, #85
	bcs	.L69
	cmp	r6, #44
	bhi	.L112
	cmp	r6, #42
	bcs	.L61
	cmp	r6, #11
	beq	.L57
	bhi	.L113
	cmp	r6, #5
	beq	.L55
	cmp	r6, #8
	beq	.L56
	cmp	r6, #2
	b	.L254
.L113:
	cmp	r6, #36
	beq	.L59
	cmp	r6, #39
	beq	.L60
	cmp	r6, #14
	bne	.L53
	b	.L58
.L112:
	cmp	r6, #70
	beq	.L66
	bhi	.L114
	cmp	r6, #53
	beq	.L63
	bhi	.L115
	cmp	r6, #48
	b	.L262
.L115:
	cmp	r6, #59
	beq	.L64
	cmp	r6, #64
	bne	.L53
	b	.L294
.L114:
	cmp	r6, #73
	bcc	.L53
	cmp	r6, #74
	bls	.L67
	sub	r3, r6, #81
	cmp	r3, #1
	bhi	.L53
	b	.L295
.L111:
	cmp	r6, #117
	beq	.L79
	bhi	.L116
	cmp	r6, #106
	beq	.L74
	bhi	.L117
	cmp	r6, #91
	beq	.L71
	bhi	.L118
	cmp	r6, #90
	bne	.L53
	b	.L296
.L118:
	cmp	r6, #100
	beq	.L72
	cmp	r6, #102
	bne	.L53
	b	.L297
.L117:
	cmp	r6, #109
	beq	.L76
	bhi	.L119
	cmp	r6, #107
	bne	.L53
	b	.L298
.L119:
	cmp	r6, #111
	beq	.L77
	cmp	r6, #113
	bne	.L53
	b	.L299
.L116:
	cmp	r6, #127
	beq	.L84
	bhi	.L120
	cmp	r6, #121
	beq	.L81
	bhi	.L121
	cmp	r6, #120
	b	.L257
.L121:
	cmp	r6, #123
	beq	.L82
	cmp	r6, #125
	bne	.L53
	b	.L300
.L120:
	cmp	r6, #131
	beq	.L86
	bhi	.L122
	cmp	r6, #129
	bne	.L53
	b	.L301
.L122:
	cmp	r6, #133
	beq	.L87
	cmp	r6, #135
	bne	.L53
	b	.L302
.L110:
	cmp	r6, #212
	beq	.L106
	bhi	.L123
	cmp	r6, #162
	beq	.L99
	bhi	.L124
	cmp	r6, #149
	beq	.L94
	bhi	.L125
	cmp	r6, #141
	beq	.L91
	bhi	.L126
	cmp	r6, #139
	bne	.L53
	b	.L303
.L126:
	cmp	r6, #143
	beq	.L92
	cmp	r6, #145
	bne	.L53
	b	.L304
.L125:
	cmp	r6, #153
	beq	.L96
	bhi	.L127
	cmp	r6, #152
	bne	.L53
	b	.L95
.L127:
	cmp	r6, #155
	beq	.L97
	cmp	r6, #157
	bne	.L53
	b	.L305
.L124:
	cmp	r6, #176
	beq	.L104
	bhi	.L128
	cmp	r6, #168
	beq	.L101
	bhi	.L129
	cmp	r6, #165
	bne	.L53
	b	.L306
.L129:
	cmp	r6, #171
	beq	.L102
	cmp	r6, #174
	bne	.L53
	b	.L307
.L128:
	cmp	r6, #202
	beq	.L94
	bhi	.L130
	sub	r3, r6, #180
	cmp	r3, #3
	bhi	.L53
	b	.L61
.L130:
	cmp	r6, #210
	bne	.L53
	b	.L308
.L123:
	ldr	r3, .L313+64
	cmp	r6, r3
	beq	.L60
	bhi	.L131
	sub	r3, r3, #34
	cmp	r6, r3
	beq	.L55
	bhi	.L132
	cmp	r6, #216
	beq	.L108
	bhi	.L133
	cmp	r6, #214
	bne	.L53
	b	.L309
.L133:
	cmp	r6, #245
	beq	.L109
	cmp	r6, #1000
.L254:
	bne	.L53
	b	.L310
.L132:
	ldr	r3, .L313+68
	cmp	r6, r3
	beq	.L57
	bhi	.L134
	sub	r3, r3, #3
	cmp	r6, r3
	bne	.L53
	b	.L56
.L134:
	cmp	r6, #1012
	beq	.L58
	ldr	r3, .L313+72
	cmp	r6, r3
	bne	.L53
	b	.L59
.L131:
	ldr	r3, .L313+76
	cmp	r6, r3
	beq	.L79
	bhi	.L135
	sub	r3, r3, #58
	cmp	r6, r3
	beq	.L64
	bhi	.L136
	sub	r3, r3, #11
	cmp	r6, r3
.L262:
	bne	.L53
	b	.L311
.L136:
	ldr	r3, .L313+80
	cmp	r6, r3
	beq	.L66
	add	r3, r3, #3
	cmp	r6, r3
	bne	.L53
	b	.L67
.L135:
	ldr	r3, .L313+84
	cmp	r6, r3
	beq	.L94
	bhi	.L137
	sub	r3, r3, #29
	cmp	r6, r3
.L257:
	bne	.L53
	b	.L312
.L137:
	ldr	r3, .L313+88
	cmp	r6, r3
	beq	.L95
	cmp	r6, #1200
	bne	.L53
	b	.L94
.L310:
	ldrh	r3, [sp, #106]
	ldrh	r7, [sp, #108]
	orr	r7, r3, r7, asl #16
.LBB180:
	ldr	r3, .L313+532
	ldr	r3, [r3, #56]
	cmp	r3, #0
	ldreq	r0, .L313+92
	beq	.L268
	cmp	r6, #2
	bne	.L139
	ldr	r3, .L313+96
	mov	r5, #0
	str	r5, [r3, #20]
	ldr	r3, .L313+436
	strb	r5, [r3, #129]
	bl	NETWORK_CONFIG_get_network_id
	cmp	r7, r0
	beq	.L232
	mov	r0, #1
	bl	NETWORK_CONFIG_get_change_bits_ptr
	mov	r4, #1
	mov	r6, r0
	bl	FLOWSENSE_get_controller_index
	mov	r1, r4
	mov	r2, r4
	stmia	sp, {r4, r6}
	mov	r3, r0
	mov	r0, r7
	bl	NETWORK_CONFIG_set_network_id
	mov	r0, r4
	mov	r1, r5
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L232
.L55:
.LBE180:
.LBB181:
	ldr	r3, .L313+532
	ldr	r3, [r3, #68]
	cmp	r3, #0
	bne	.L141
	ldr	r0, .L313+100
.L268:
	bl	Alert_Message
	b	.L139
.L141:
	cmp	r6, #5
	bne	.L232
	ldr	r4, .L313+104
	ldr	r2, .L313+8
	mov	r3, #468
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+108
	ldr	r2, [r3, #40]
	str	r2, [r3, #36]
	b	.L208
.L56:
.LBE181:
.LBB182:
	ldr	r5, .L313+532
	ldr	r3, [r5, #72]
	cmp	r3, #0
	ldreq	r0, .L313+112
	beq	.L268
	cmp	r6, #8
	bne	.L232
	ldr	r4, .L313+116
	ldr	r3, .L313+120
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L313+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #76]
	cmp	r3, #0
	beq	.L145
	ldr	r2, [r3, #20]
	cmp	r2, #0
	bne	.L288
.L146:
	ldr	r0, .L313+124
	b	.L286
.L145:
	ldr	r0, .L313+128
	b	.L286
.L95:
.LBE182:
.LBB183:
	ldr	r3, .L313+532
	ldr	r3, [r3, #80]
	cmp	r3, #0
	ldreq	r0, .L313+132
	beq	.L268
	cmp	r6, #152
	bne	.L232
	ldr	r4, .L313+136
	ldr	r2, .L313+8
	ldr	r3, .L313+140
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+144
	ldr	r2, [r3, #20]
	cmp	r2, #0
	beq	.L150
.L288:
	ldr	r2, [r3, #16]
	str	r2, [r3, #12]
	b	.L208
.L150:
	ldr	r0, .L313+148
	b	.L286
.L57:
.LBE183:
.LBB184:
	ldr	r3, .L313+532
	ldr	r3, [r3, #88]
	cmp	r3, #0
	ldreq	r0, .L313+152
	beq	.L268
	cmp	r6, #11
	bne	.L232
	ldr	r4, .L313+156
	ldr	r2, .L313+8
	mov	r3, #864
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+160
	ldr	r2, [r3, #24]
	cmp	r2, #0
	ldreq	r0, .L313+164
	beq	.L286
	ldr	r2, [r3, #20]
	mov	r0, #5
	str	r2, [r3, #16]
	ldr	r1, .L313+168
	bl	FLASH_STORAGE_if_not_running_start_file_save_timer
	b	.L208
.L58:
.LBE184:
.LBB185:
	ldr	r3, .L313+532
	ldr	r3, [r3, #92]
	cmp	r3, #0
	ldreq	r0, .L313+172
	beq	.L268
	cmp	r6, #14
	bne	.L232
	ldr	r4, .L313+176
	ldr	r2, .L313+8
	ldr	r3, .L313+180
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+184
	ldr	r2, [r3, #24]
	cmp	r2, #0
	ldrne	r2, [r3, #20]
	movne	r0, #4
	strne	r2, [r3, #16]
	bne	.L293
	b	.L170
.L59:
.LBE185:
.LBB186:
	ldr	r3, .L313+532
	ldr	r3, [r3, #96]
	cmp	r3, #0
	ldreq	r0, .L313+188
	beq	.L268
	cmp	r6, #36
	bne	.L232
	ldr	r4, .L313+192
	ldr	r2, .L313+8
	ldr	r3, .L313+196
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+200
	ldr	r2, [r3, #24]
	cmp	r2, #0
	ldrne	r2, [r3, #20]
	movne	r0, #3
	strne	r2, [r3, #16]
	ldreq	r0, .L313+204
	beq	.L286
	b	.L293
.L60:
.LBE186:
.LBB187:
	ldr	r3, .L313+532
	ldr	r3, [r3, #100]
	cmp	r3, #0
	ldreq	r0, .L313+208
	beq	.L268
	cmp	r6, #39
	bne	.L232
	ldr	r4, .L313+212
	ldr	r2, .L313+8
	ldr	r3, .L313+216
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+220
	ldr	r2, [r3, #24]
	cmp	r2, #0
	ldreq	r0, .L313+224
	beq	.L286
	ldr	r2, [r3, #20]
	mov	r0, #2
	str	r2, [r3, #16]
	mov	r1, r0
	b	.L287
.L79:
.LBE187:
.LBB188:
	ldr	r3, .L313+532
	ldr	r3, [r3, #108]
	cmp	r3, #0
	ldreq	r0, .L313+228
	beq	.L268
	cmp	r6, #117
	bne	.L232
	ldr	r4, .L313+232
	ldr	r2, .L313+8
	ldr	r3, .L313+236
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+240
	ldr	r2, [r3, #24]
	cmp	r2, #0
	beq	.L170
	ldr	r2, [r3, #20]
	mov	r0, #17
	str	r2, [r3, #16]
.L293:
	mov	r1, #2
.L287:
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L208
.L170:
	ldr	r0, .L313+244
	b	.L286
.L94:
.LBE188:
.LBB189:
	ldr	r3, .L313+532
	ldr	r3, [r3, #104]
	cmp	r3, #0
	ldreq	r0, .L313+248
	beq	.L268
	cmp	r6, #149
	cmpne	r6, #202
	bne	.L232
	ldr	r4, .L313+252
	ldr	r2, .L313+8
	mov	r3, #1088
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+256
	ldr	r2, [r3, #24]
	cmp	r2, #0
	ldrne	r2, [r3, #20]
	movne	r0, #19
	strne	r2, [r3, #16]
	ldreq	r0, .L313+260
	beq	.L286
	b	.L293
.L312:
.LBE189:
.LBB190:
	ldr	r3, .L313+532
	ldr	r3, [r3, #164]
	cmp	r3, #0
	ldreq	r0, .L313+264
	beq	.L268
	cmp	r6, #120
	bne	.L232
	ldr	r4, .L313+268
	ldr	r5, .L313+272
	ldr	r3, .L313+276
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L313+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #492]
	cmp	r3, #0
	beq	.L178
	mov	r1, #400
	ldr	r2, .L313+8
	ldr	r0, [r4, #0]
	ldr	r3, .L313+280
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #0
	str	r3, [r5, #488]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #7
	mov	r1, #2
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L179
.L178:
	ldr	r0, .L313+284
	bl	Alert_Message
.L179:
	ldr	r3, .L313+268
	ldr	r0, [r3, #0]
	b	.L278
.L61:
.LBE190:
.LBB191:
	ldr	r3, .L313+532
	ldr	r3, [r3, #84]
	cmp	r3, #0
	ldreq	r0, .L313+288
	beq	.L268
	bl	CONFIG_this_controller_is_behind_a_hub
	subs	r4, r0, #0
	ldrne	r0, .L313+292
	bne	.L268
	bl	CONFIG_this_controller_is_a_configured_hub
	subs	r5, r0, #0
	beq	.L182
	ldr	r5, .L313+296
	cmp	r6, #181
	str	r4, [r5, #272]
	beq	.L241
	cmp	r6, #182
	beq	.L242
	cmp	r6, #183
	beq	.L243
	cmp	r6, #180
	beq	.L285
.L184:
	cmp	r6, #43
	bne	.L185
	ldr	r0, .L313+300
	bl	Alert_Message
	b	.L273
.L185:
	cmp	r6, #44
	movne	r3, r4
	bne	.L258
	ldr	r0, .L313+304
	bl	Alert_Message
.L285:
	mov	r3, #41
	str	r3, [r5, #272]
.L273:
	mov	r3, #1
	b	.L259
.L182:
	sub	r3, r6, #180
	cmp	r3, #3
	bhi	.L186
	ldr	r0, .L313+308
	mov	r1, r6
	bl	Alert_Message_va
	b	.L245
.L186:
	cmp	r6, #42
	moveq	r3, r5
	bne	.L245
	b	.L258
.L241:
	mov	r3, r4
	mov	r4, #39
	b	.L260
.L242:
	mov	r3, r4
	mov	r4, #40
	b	.L260
.L243:
	mov	r3, r4
	mov	r4, #41
.L260:
	mov	r5, #1
	b	.L183
.L258:
	mov	r4, r3
	mov	r5, r3
	b	.L183
.L245:
	mov	r3, #1
	mov	r4, r5
.L259:
	mov	r5, r4
.L183:
	ldr	r2, .L313+296
	mov	r1, #1
	cmp	r6, #44
	str	r1, [r2, #188]
	moveq	r1, #0
	streq	r1, [r2, #188]
	cmp	r3, #0
	beq	.L188
	ldr	r6, .L313+532
.L246:
	mov	r2, #0
	ldr	r0, [r6, #176]
	add	r1, sp, #112
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	bne	.L246
.L188:
	bl	terminate_ci_transaction
	cmp	r5, #0
	beq	.L139
	mov	r0, #800
	bl	vTaskDelay
	mov	r0, r4
	bl	SYSTEM_application_requested_restart
	b	.L139
.L311:
.LBE191:
.LBB192:
	ldr	r3, .L313+532
	ldr	r3, [r3, #112]
	cmp	r3, #0
	ldreq	r0, .L313+312
	beq	.L268
	b	.L232
.L64:
.LBE192:
.LBB193:
	ldr	r3, .L313+532
	ldr	r3, [r3, #120]
	cmp	r3, #0
	ldreq	r0, .L313+316
	beq	.L268
	b	.L232
.L295:
.LBE193:
.LBB194:
	ldr	r3, .L313+532
	ldr	r3, [r3, #140]
	cmp	r3, #0
	ldreq	r0, .L313+320
	beq	.L268
	cmp	r6, #82
	movne	r0, #416
	beq	.L292
.L263:
	mov	r1, #0
	mov	r2, #256
	mov	r3, #1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
	b	.L232
.L66:
.LBE194:
.LBB195:
	ldr	r3, .L313+532
	ldr	r3, [r3, #148]
	cmp	r3, #0
	ldreq	r0, .L313+324
	beq	.L268
	cmp	r6, #70
	moveq	r0, #0
	beq	.L264
	ldr	r3, .L313+80
	cmp	r6, r3
	bne	.L232
	mov	r0, #1
.L264:
	bl	PDATA_update_pending_change_bits
	b	.L232
.L296:
.LBE195:
.LBB196:
	ldr	r3, .L313+436
	ldr	r2, [r3, #112]
	cmp	r2, #0
	beq	.L199
	ldrb	r4, [r3, #128]	@ zero_extendqisi2
	cmp	r4, #0
	bne	.L199
	ldr	r0, .L313+328
	bl	Alert_Message
	ldr	r0, .L313+428
	mov	r1, r4
	mov	r2, #256
	mov	r3, #1
	b	.L281
.L199:
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	bne	.L200
	bl	CONFIG_this_controller_is_behind_a_hub
	subs	r4, r0, #0
	beq	.L201
.L200:
	ldr	r0, .L313+332
	bl	Alert_Message
	ldr	r3, .L313+532
	ldr	r3, [r3, #4]
	cmp	r3, #0
	beq	.L202
	ldr	r0, .L313+336
	bl	Alert_Message
.L202:
	ldr	r0, .L313+340
	mov	r1, #0
	mov	r2, #256
	mov	r3, #1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
	mov	r0, #110
	bl	CONTROLLER_INITIATED_post_event
	b	.L139
.L201:
	ldr	r0, .L313+344
	bl	Alert_Message
	ldr	r0, .L313+340
	mov	r1, r4
	mov	r2, #256
	mov	r3, #1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
	b	.L139
.L69:
.LBE196:
.LBB197:
	ldr	r3, .L313+532
	ldr	r3, [r3, #156]
	cmp	r3, #0
	ldreq	r0, .L313+348
	beq	.L268
	cmp	r6, #86
	bne	.L204
.L292:
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	bne	.L205
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	bne	.L232
.L205:
	ldr	r0, .L313+464
	b	.L263
.L204:
	ldr	r4, .L313+352
	mov	r1, #400
	ldr	r3, .L313+356
	ldr	r0, [r4, #0]
	ldr	r2, .L313+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+436
	ldr	r1, [r3, #112]
	cmp	r1, #0
	beq	.L207
	ldrb	r3, [r3, #128]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L207
	ldr	r0, .L313+360
	mov	r2, #0
	bl	Alert_Message_va
	b	.L208
.L207:
	ldr	r3, .L313+532
	ldr	r1, [r3, #144]
	cmp	r1, #0
	beq	.L209
	ldr	r0, .L313+364
.L286:
	bl	Alert_Message
	b	.L208
.L209:
	ldr	r0, .L313+368
	mov	r2, #256
	mov	r3, #1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
.L208:
	ldr	r0, [r4, #0]
.L278:
	bl	xQueueGiveMutexRecursive
	b	.L232
.L67:
.LBE197:
.LBB198:
	ldr	r3, .L313+532
	ldr	r3, [r3, #160]
	cmp	r3, #0
	ldreq	r0, .L313+372
	beq	.L268
	b	.L232
.L71:
.LBE198:
.LBB199:
	add	r5, sp, #128
	add	r1, r4, #12
	mov	r2, #6
	mov	r0, r5
	bl	memcpy
	ldmia	r5, {r0, r1}
	mov	r2, #1
	bl	EPSON_set_date_time
	cmp	r0, #0
	beq	.L270
	ldr	r4, .L313+376
	mov	r1, #400
	ldr	r2, .L313+8
	ldr	r3, .L313+380
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+28
	ldmia	r5, {r0, r1}
	add	r2, r3, #464
	strh	r1, [r2, #0]	@ movhi
	mov	r2, #1
	str	r0, [r3, #460]
	str	r2, [r3, #468]
	str	r2, [r3, #456]
	b	.L282
.L63:
.LBE199:
.LBB200:
	mov	r0, #56
	bl	Alert_comm_command_sent
	mov	r2, #0
	mov	r3, r2
	mov	r1, #56
	mov	r0, #1
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	bl	FLOWSENSE_get_controller_index
	add	r1, r4, #12
	mov	r2, #1
	add	r6, r4, #13
	mov	r5, r0
	add	r0, sp, #140
	bl	memcpy
	ldrb	r3, [sp, #140]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L212
	mov	r1, r6
	mov	r2, #4
	add	r0, sp, #156
	bl	memcpy
	add	r1, sp, #160
	mov	r0, #1
	bl	WEATHER_TABLES_get_et_table_entry_for_index
	mov	r1, #2
	mov	r3, #7
	stmia	sp, {r1, r3}
	ldr	r0, .L313+384
	mov	r1, r5
	add	r2, sp, #160
	add	r3, sp, #156
	str	r5, [sp, #8]
	bl	Alert_ChangeLine_Controller
	mov	r0, #1
	add	r1, sp, #156
	add	r6, r4, #17
	bl	WEATHER_TABLES_update_et_table_entry_for_index
.L212:
	ldrb	r3, [sp, #140]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L213
	mov	r2, #4
	mov	r1, r6
	add	r0, sp, #164
	bl	memcpy
	add	r1, sp, #168
	mov	r0, #1
	bl	WEATHER_TABLES_get_rain_table_entry_for_index
	mov	r2, #2
	mov	r3, #7
	stmia	sp, {r2, r3}
	ldr	r0, .L313+388
	mov	r1, r5
	add	r2, sp, #168
	add	r3, sp, #164
	str	r5, [sp, #8]
	bl	Alert_ChangeLine_Controller
	mov	r0, #1
	add	r1, sp, #164
	bl	WEATHER_TABLES_update_rain_table_entry_for_index
.L213:
	ldrb	r3, [sp, #140]	@ zero_extendqisi2
	tst	r3, #1
	bne	.L214
	tst	r3, #2
	beq	.L270
.L214:
	ldr	r4, .L313+400
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L313+8
	ldr	r3, .L313+392
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+436
	ldr	r0, [r4, #0]
	ldr	r4, .L313+268
	mov	r2, #1
	str	r2, [r3, #68]
	bl	xQueueGiveMutexRecursive
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L313+8
	ldr	r3, .L313+396
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+272
	ldr	r0, [r4, #0]
	ldr	r2, [r3, #488]
	cmp	r2, #1
	movls	r2, #2
	strls	r2, [r3, #488]
	bl	xQueueGiveMutexRecursive
	mov	r0, #7
	mov	r1, #2
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L270
.L314:
	.align	2
.L313:
	.word	.LANCHOR0
	.word	.LC2
	.word	.LC1
	.word	3562
	.word	3597
	.word	.LC3
	.word	.LC4
	.word	comm_mngr
	.word	6000
	.word	.LC9
	.word	.LC7
	.word	.LC6
	.word	.LC5
	.word	3747
	.word	.LC8
	.word	3790
	.word	1037
	.word	1009
	.word	1034
	.word	1115
	.word	1068
	.word	1147
	.word	1150
	.word	.LC10
	.word	battery_backed_general_use
	.word	.LC11
	.word	alerts_pile_recursive_MUTEX
	.word	alerts_struct_engineering
	.word	.LC12
	.word	system_preserves_recursive_MUTEX
	.word	506
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	moisture_sensor_recorder_recursive_MUTEX
	.word	563
	.word	msrcs
	.word	.LC16
	.word	.LC17
	.word	station_history_completed_records_recursive_MUTEX
	.word	station_history_completed
	.word	.LC18
	.word	7200
	.word	.LC19
	.word	station_report_data_completed_records_recursive_MUTEX
	.word	915
	.word	station_report_data_completed
	.word	.LC21
	.word	poc_report_completed_records_recursive_MUTEX
	.word	958
	.word	poc_report_data_completed
	.word	.LC22
	.word	.LC23
	.word	system_report_completed_records_recursive_MUTEX
	.word	1001
	.word	system_report_data_completed
	.word	.LC24
	.word	.LC25
	.word	lights_report_completed_records_recursive_MUTEX
	.word	1044
	.word	lights_report_data_completed
	.word	.LC20
	.word	.LC26
	.word	budget_report_completed_records_recursive_MUTEX
	.word	budget_report_data_completed
	.word	.LC27
	.word	.LC28
	.word	weather_tables_recursive_MUTEX
	.word	weather_tables
	.word	1131
	.word	1138
	.word	.LC29
	.word	.LC30
	.word	.LC31
	.word	cdcs
	.word	.LC32
	.word	.LC33
	.word	.LC34
	.word	.LC35
	.word	.LC36
	.word	.LC37
	.word	.LC38
	.word	.LC39
	.word	.LC40
	.word	.LC41
	.word	417
	.word	.LC42
	.word	.LC43
	.word	pending_changes_recursive_MUTEX
	.word	1579
	.word	.LC44
	.word	.LC45
	.word	418
	.word	.LC46
	.word	comm_mngr_recursive_MUTEX
	.word	1700
	.word	61486
	.word	61487
	.word	1287
	.word	1298
	.word	weather_preserves_recursive_MUTEX
	.word	1357
	.word	15000
	.word	413
	.word	.LC47
	.word	921
	.word	665
	.word	415
	.word	.LC48
	.word	weather_preserves
	.word	6000
	.word	.LC49
	.word	.LC50
	.word	2367
	.word	chain
	.word	.LC51
	.word	414
	.word	2556
	.word	comm_mngr
	.word	.LC52
	.word	.LC53
	.word	2657
	.word	10000
	.word	config_c
	.word	.LC54
	.word	.LC55
	.word	irri_comm_recursive_MUTEX
	.word	.LC1
	.word	2831
	.word	irri_comm
	.word	.LC56
	.word	.LC57
	.word	.LC58
	.word	cics
	.word	.LC59
	.word	.LC60
	.word	.LC61
	.word	router_hub_list_queue
	.word	.LC62
	.word	router_hub_list_MUTEX
	.word	.LC63
	.word	4522
	.word	.LC64
	.word	.LC65
	.word	.LC66
	.word	GuiVar_CommTestStatus
	.word	GuiLib_CurStructureNdx
	.word	port_names
	.word	.LC67
.L294:
.LBE200:
.LBB201:
	mov	r0, #67
	ldr	r4, .L313+400
	bl	Alert_comm_command_sent
	mov	r2, #0
	mov	r3, r2
	mov	r0, #1
	mov	r1, #67
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	ldr	r2, .L313+508
	ldr	r3, .L313+404
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+436
	mov	r2, #2
	str	r2, [r3, #56]
.L282:
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	b	.L270
.L298:
.LBE201:
.LBB202:
	mov	r0, #108
	bl	Alert_comm_command_sent
	mov	r2, #0
	mov	r3, r2
	mov	r1, #108
	mov	r0, #1
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	mov	r0, #142
	bl	CONTROLLER_INITIATED_post_event
	mov	r3, #140
	str	r3, [sp, #32]
	ldr	r3, .L313+408
	add	r0, sp, #32
	str	r3, [sp, #40]
	bl	CONTROLLER_INITIATED_post_event_with_details
	ldr	r0, .L313+412
	mov	r1, #0
	mov	r2, #256
	b	.L289
.L74:
.LBE202:
.LBB203:
	ldr	r3, .L313+532
	ldr	r3, [r3, #128]
	cmp	r3, #0
	ldreq	r0, .L313+416
	beq	.L268
	b	.L232
.L72:
.LBE203:
.LBB204:
	mov	r1, #0
	mov	r2, #20
	add	r0, sp, #12
	bl	memset
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #152
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #16
	bl	memcpy
	add	r1, r4, #20
	mov	r2, #4
	add	r0, sp, #20
	bl	memcpy
	add	r1, r4, #24
	mov	r2, #4
	add	r0, sp, #24
	bl	memcpy
	mov	r3, #0
	str	r3, [sp, #28]
.LBB205:
	bl	tpmicro_is_available_for_token_generation
	cmp	r0, #0
	beq	.L218
	ldr	r3, .L313+472
	ldr	r2, [r3, #0]
	cmp	r2, #2
	bne	.L218
	ldr	r3, [r3, #8]
	cmp	r3, #0
	bne	.L218
.LBE205:
	add	r0, sp, #12
	mov	r1, #7
	bl	FOAL_IRRI_initiate_a_station_test
.L218:
	mov	r0, #101
	b	.L275
.L297:
.LBE204:
.LBB206:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #148
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	add	r1, r4, #20
	mov	r2, #4
	add	r0, sp, #152
	bl	memcpy
	ldr	r0, [sp, #144]
	ldr	r1, [sp, #152]
	mov	r2, #7
	mov	r3, #32
	bl	FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists
	mov	r0, #103
	b	.L276
.L76:
.LBE206:
.LBB207:
	mov	r0, #110
	bl	Alert_comm_command_sent
	mov	r2, #0
	mov	r0, #1
	mov	r1, #110
	mov	r3, r2
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	mov	r0, #2000
	bl	vTaskDelay
	mov	r0, #28
	b	.L283
.L77:
.LBE207:
.LBB208:
	mov	r0, #112
	bl	Alert_comm_command_sent
	mov	r2, #0
	mov	r0, #1
	mov	r1, #112
	mov	r3, r2
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	mov	r0, #2000
	bl	vTaskDelay
	mov	r0, #29
.L283:
	bl	FACTORY_RESET_init_all_battery_backed_delete_all_files_and_restart
	b	.L270
.L299:
.LBE208:
.LBB209:
	mov	r0, #114
	bl	Alert_comm_command_sent
	mov	r2, #0
	mov	r0, #1
	mov	r1, #114
	mov	r3, r2
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	ldr	r0, .L313+420
	ldr	r1, .L313+424
	bl	COMM_TEST_send_all_program_data_to_the_cloud
	ldr	r0, .L313+428
	mov	r1, #0
	b	.L280
.L81:
.LBE209:
.LBB210:
	mov	r1, #0
	mov	r2, #20
	add	r0, sp, #52
	bl	memset
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #152
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #68
	bl	memcpy
	add	r1, r4, #20
	mov	r2, #4
	add	r0, sp, #56
	bl	memcpy
	add	r1, r4, #24
	mov	r2, #4
	add	r0, sp, #60
	bl	memcpy
	ldr	r0, [sp, #68]
	ldr	r1, [sp, #56]
	ldr	r2, [sp, #60]
	mov	r3, #1
	bl	FOAL_IRRI_initiate_or_cancel_a_master_valve_override
	mov	r0, #122
	b	.L275
.L82:
.LBE210:
.LBB211:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #152
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	add	r1, r4, #20
	mov	r2, #4
	add	r0, sp, #148
	bl	memcpy
	add	r1, r4, #24
	mov	r2, #4
	add	r0, sp, #140
	bl	memcpy
	mov	r2, #4
	add	r0, sp, #172
	add	r1, r4, #28
	bl	memcpy
	ldr	r2, [sp, #144]
	cmp	r2, #3
	bne	.L219
	mov	r1, #0
	add	r0, sp, #72
	mov	r2, #16
	bl	memset
	mov	r3, #1
	ldr	r2, [sp, #148]
	str	r3, [sp, #72]
	ldr	r3, [sp, #140]
	add	r0, sp, #128
	add	r3, r3, r2, asl #2
	str	r3, [sp, #76]
	bl	EPSON_obtain_latest_time_and_date
	add	r0, sp, #128
	ldr	r1, [sp, #172]
	bl	TDUTILS_add_seconds_to_passed_DT_ptr
	ldrh	r3, [sp, #132]
	add	r0, sp, #72
	str	r3, [sp, #80]
	ldr	r3, [sp, #128]
	str	r3, [sp, #84]
	bl	FOAL_LIGHTS_initiate_a_mobile_light_ON
	b	.L220
.L219:
	cmp	r2, #8
	bne	.L221
	add	r0, sp, #120
	mov	r1, #0
	bl	memset
	mov	r3, #1
	str	r3, [sp, #120]
	ldr	r2, [sp, #148]
	ldr	r3, [sp, #140]
	add	r0, sp, #120
	add	r3, r3, r2, asl #2
	str	r3, [sp, #124]
	bl	FOAL_LIGHTS_initiate_a_mobile_light_OFF
	b	.L220
.L221:
	ldr	r0, .L313+432
	bl	Alert_Message
.L220:
	mov	r0, #124
	b	.L275
.L300:
.LBE211:
.LBB212:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #172
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #140
	bl	memcpy
	ldr	r6, [sp, #140]
	bl	FLOWSENSE_get_controller_index
	mov	r4, #1
	mov	r5, r0
	mov	r0, #1
	bl	NETWORK_CONFIG_get_change_bits_ptr
	mov	r1, r4
	mov	r2, r4
	mov	r3, r5
	str	r4, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, r6
	bl	NETWORK_CONFIG_set_scheduled_irrigation_off
	mov	r0, #34
	bl	NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits
	ldr	r3, .L313+436
	mov	r1, #2
	str	r4, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L313+532
	ldr	r2, .L313+440
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r0, #126
	ldr	r1, [sp, #172]
	b	.L271
.L84:
.LBE212:
.LBB213:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #148
	bl	memcpy
	ldr	r0, [sp, #148]
	mov	r1, #1
	bl	STATION_set_no_water_days_for_all_stations
	mov	r0, #128
	b	.L274
.L301:
.LBE213:
.LBB214:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #152
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #148
	bl	memcpy
	add	r1, r4, #20
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	ldr	r0, [sp, #148]
	ldr	r1, [sp, #144]
	mov	r2, #1
	bl	STATION_set_no_water_days_by_group
	mov	r0, #130
.L275:
	ldr	r1, [sp, #152]
	b	.L271
.L86:
.LBE214:
.LBB215:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #140
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	add	r1, r4, #20
	mov	r2, #4
	add	r0, sp, #148
	bl	memcpy
	add	r1, r4, #24
	mov	r2, #4
	add	r0, sp, #152
	bl	memcpy
	add	r0, sp, #144
	ldmia	r0, {r0, r1, r2}
	mov	r3, #1
	bl	STATION_set_no_water_days_by_station
	mov	r0, #132
	b	.L277
.L304:
.LBE215:
.LBB216:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #148
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	add	r1, r4, #20
	mov	r2, #4
	add	r0, sp, #140
	bl	memcpy
	ldr	r0, [sp, #144]
	ldr	r1, [sp, #140]
	mov	r2, #1
	bl	STATION_set_no_water_days_by_box
	mov	r0, #146
	b	.L276
.L87:
.LBE216:
	mov	r0, #1
	bl	process_incoming_request_to_send_registration
	b	.L139
.L302:
.LBB217:
	add	r1, r4, #12
	ldr	r4, .L313+492
	mov	r2, #4
	add	r0, sp, #140
	bl	memcpy
	ldrb	r2, [r4, #52]	@ zero_extendqisi2
	ldr	r3, [sp, #140]
	and	r2, r2, #1
	cmp	r2, r3
	beq	.L222
	cmp	r3, #0
	ldrne	r0, .L313+444
	ldreq	r0, .L313+448
	bl	Alert_Message
.L222:
	ldrb	r3, [r4, #52]	@ zero_extendqisi2
	ldr	r2, [sp, #140]
	bic	r3, r3, #1
	and	r2, r2, #1
	orr	r3, r2, r3
	strb	r3, [r4, #52]
	mov	r0, #0
	ldr	r4, .L313+504
	mov	r1, r0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	bl	DIALOG_close_all_dialogs
	mov	r0, #1
	bl	Redraw_Screen
	mov	r1, #400
	ldr	r2, .L313+508
	ldr	r3, .L313+452
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+516
	mov	r5, #1
	str	r5, [r3, #204]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L313+456
	mov	r2, #92
	ldr	r1, [sp, #140]
	and	r1, r1, r5
	mla	r3, r2, r0, r3
	mov	r0, r5
	ldrb	r2, [r3, #24]	@ zero_extendqisi2
	bic	r2, r2, #1
	orr	r2, r1, r2
	strb	r2, [r3, #24]
	mov	r1, #136
	b	.L279
.L89:
.LBE217:
.LBB218:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #148
	bl	memcpy
	ldr	r0, [sp, #148]
	bl	SYSTEM_clear_mainline_break
	bl	DIALOG_close_all_dialogs
	mov	r0, #1
	bl	Redraw_Screen
	mov	r0, #138
	b	.L274
.L303:
.LBE218:
.LBB219:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	mov	r0, #1
	bl	IRRI_COMM_process_stop_command
	bl	DIALOG_close_all_dialogs
	mov	r0, #1
	bl	Redraw_Screen
	mov	r0, #140
	b	.L274
.L91:
.LBE219:
.LBB220:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #148
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	mov	r0, #0
	bl	IRRI_COMM_process_stop_command
	bl	DIALOG_close_all_dialogs
	mov	r0, #1
	bl	Redraw_Screen
	mov	r0, #142
	b	.L276
.L92:
.LBE220:
.LBB221:
	bl	CONFIG_this_controller_is_behind_a_hub
	subs	r4, r0, #0
	ldrne	r0, .L313+460
	bne	.L268
	mov	r0, #1
	mov	r1, #144
	mov	r2, r4
	mov	r3, r4
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	ldr	r0, .L313+464
	mov	r1, r4
.L280:
	mov	r2, #512
.L289:
	mov	r3, r1
.L281:
	bl	CONTROLLER_INITIATED_post_to_messages_queue
	b	.L270
.L96:
.LBE221:
.LBB222:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	add	r1, r4, #16
	ldr	r4, .L313+504
	mov	r2, #4
	add	r0, sp, #148
	bl	memcpy
	mov	r1, #400
	ldr	r2, .L313+508
	ldr	r3, .L313+468
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+472
	mov	r2, #1
	str	r2, [r3, #472]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #154
	b	.L274
.L97:
.LBE222:
.LBB223:
	mov	r1, #0
	mov	r2, #20
	add	r0, sp, #12
	bl	memset
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #16
	bl	memcpy
	add	r1, r4, #20
	mov	r2, #4
	add	r0, sp, #20
	bl	memcpy
	mov	r3, #360
	str	r3, [sp, #24]
	add	r0, sp, #12
	mov	r3, #1
	mov	r1, #7
	str	r3, [sp, #28]
	bl	FOAL_IRRI_initiate_a_station_test
	mov	r0, #156
	b	.L274
.L305:
.LBE223:
.LBB224:
	add	r1, r4, #12
	ldr	r4, .L313+492
	mov	r2, #4
	add	r0, sp, #140
	bl	memcpy
	ldrb	r2, [r4, #52]	@ zero_extendqisi2
	ldr	r3, [sp, #140]
	mov	r2, r2, lsr #3
	and	r2, r2, #1
	cmp	r2, r3
	beq	.L225
	cmp	r3, #0
	ldrne	r0, .L313+476
	ldreq	r0, .L313+480
	bl	Alert_Message
.L225:
	ldrb	r3, [r4, #52]	@ zero_extendqisi2
	ldr	r2, [sp, #140]
	bic	r3, r3, #8
	and	r2, r2, #1
	orr	r3, r3, r2, asl #3
	strb	r3, [r4, #52]
	ldr	r4, .L313+504
	mov	r0, #0
	mov	r1, r0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	mov	r1, #400
	ldr	r2, .L313+508
	ldr	r3, .L313+484
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+516
	mov	r5, #1
	str	r5, [r3, #204]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	mov	r1, #158
	b	.L279
.L308:
.LBE224:
.LBB225:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	mov	r0, #1
	bl	STATION_set_ignore_moisture_balance_at_next_irrigation_all_stations
	mov	r0, #211
.L274:
	ldr	r1, [sp, #144]
	b	.L271
.L106:
.LBE225:
.LBB226:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #148
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	ldr	r0, [sp, #144]
	mov	r1, #1
	bl	STATION_set_ignore_moisture_balance_at_next_irrigation_by_group
	mov	r0, #213
.L276:
	ldr	r1, [sp, #148]
	b	.L271
.L309:
.LBE226:
.LBB227:
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #140
	bl	memcpy
	add	r1, r4, #16
	mov	r2, #4
	add	r0, sp, #144
	bl	memcpy
	add	r1, r4, #20
	mov	r2, #4
	add	r0, sp, #148
	bl	memcpy
	ldr	r0, [sp, #144]
	ldr	r1, [sp, #148]
	mov	r2, #1
	bl	STATION_set_ignore_moisture_balance_at_next_irrigation_by_station
	mov	r0, #215
.L277:
	ldr	r1, [sp, #140]
.L271:
	bl	build_response_to_mobile_cmd_and_update_session_data
	b	.L139
.L108:
.LBE227:
.LBB228:
	ldr	r0, .L313+488
	mov	r1, #1
	bl	CONTROLLER_INITIATED_alerts_timer_upkeep
	mov	r2, #0
	mov	r0, #1
	mov	r1, #217
	mov	r3, r2
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	b	.L139
.L109:
.LBE228:
.LBB229:
	add	r1, r4, #12
	ldr	r4, .L313+492
	mov	r2, #4
	add	r0, sp, #140
	bl	memcpy
	ldrb	r2, [r4, #53]	@ zero_extendqisi2
	ldr	r3, [sp, #140]
	mov	r2, r2, lsr #4
	and	r2, r2, #1
	cmp	r2, r3
	beq	.L227
	cmp	r3, #0
	ldrne	r0, .L313+496
	ldreq	r0, .L313+500
	bl	Alert_Message
.L227:
	ldrb	r3, [r4, #53]	@ zero_extendqisi2
	ldr	r2, [sp, #140]
	bic	r3, r3, #16
	and	r2, r2, #1
	orr	r3, r3, r2, asl #4
	strb	r3, [r4, #53]
	ldr	r4, .L313+504
	mov	r0, #0
	mov	r1, r0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	mov	r1, #400
	ldr	r2, .L313+508
	ldr	r3, .L313+512
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L313+516
	mov	r5, #1
	str	r5, [r3, #204]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	mov	r1, #246
.L279:
	mov	r2, #0
	mov	r3, r2
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	mov	r0, #0
	bl	process_incoming_request_to_send_registration
	b	.L270
.L99:
.LBE229:
.LBB230:
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	bne	.L229
	bl	CONFIG_this_controller_is_behind_a_hub
	cmp	r0, #0
	ldreq	r0, .L313+520
	beq	.L268
.L229:
	ldr	r3, .L313+532
	ldr	r1, [r3, #0]
	cmp	r1, #3
	beq	.L270
	ldr	r0, .L313+524
	bl	Alert_Message_va
	b	.L139
.L306:
.LBE230:
.LBB231:
	ldr	r3, .L313+532
	ldr	r3, [r3, #168]
	cmp	r3, #0
	ldreq	r0, .L313+528
	beq	.L268
	b	.L232
.L101:
.LBE231:
.LBB232:
	ldr	r3, .L313+532
	ldr	r3, [r3, #172]
	cmp	r3, #0
	ldreq	r0, .L313+536
	beq	.L268
.L232:
	bl	terminate_ci_transaction
	b	.L139
.L307:
.LBE232:
.LBB233:
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	ldreq	r0, .L313+540
	beq	.L268
	subs	r5, r5, #16
	beq	.L234
	ands	r6, r5, #3
	bne	.L234
	ldr	r0, .L313+544
	mov	r1, r5
	bl	Alert_Message_va
	ldr	r3, .L313+556
	mov	r1, r6
	ldr	r0, [r3, #0]
	mvn	r2, #0
	mov	r3, r6
	bl	xQueueGenericReceive
	ldr	r7, .L313+548
	add	r6, sp, #140
.L235:
	mov	r2, #0
	ldr	r0, [r7, #0]
	mov	r1, r6
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	bne	.L235
	ldr	r7, .L313+548
	add	r4, r4, #12
.L237:
	mov	r1, r4
	mov	r2, #4
	mov	r0, r6
	bl	memcpy
	mov	r2, #0
	ldr	r0, [r7, #0]
	mov	r1, r6
	mov	r3, r2
	bl	xQueueGenericSend
	add	r4, r4, #4
	sub	r5, r5, #4
	cmp	r0, #1
	mov	r8, r0
	beq	.L236
	ldr	r0, .L313+552
	bl	Alert_Message
.L236:
	cmp	r8, #0
	cmpne	r5, #0
	moveq	r1, #0
	movne	r1, #1
	bne	.L237
	ldr	r3, .L313+556
	mov	r2, r1
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericSend
	b	.L238
.L234:
	ldr	r0, .L313+560
	mov	r1, r5
	bl	Alert_Message_va
.L238:
	mov	r2, #0
	mov	r0, #1
	mov	r1, #175
	mov	r3, r2
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	ldr	r0, .L313+564
	bl	CODE_DISTRIBUTION_post_event
	b	.L270
.L102:
.LBE233:
.LBB234:
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	ldreq	r0, .L313+568
	movne	r0, #1
	movne	r1, #172
	bne	.L269
	b	.L268
.L104:
.LBE234:
.LBB235:
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	ldreq	r0, .L313+572
	beq	.L268
	add	r1, r4, #12
	mov	r2, #4
	add	r0, sp, #140
	bl	memcpy
	mov	r0, #6
	bl	CODE_DISTRIBUTION_try_to_start_a_code_distribution
	mov	r0, #1
	mov	r1, #177
.L269:
	mov	r2, #0
	mov	r3, r2
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
.L270:
	bl	poll_wrap_up_by_sending_no_more_messages_msg_for_hub_based_controllers
	b	.L139
.L53:
.LBE235:
	ldr	r0, .L313+576
	mov	r1, r6
	bl	Alert_Message_va
	b	.L33
.L139:
	ldrh	r0, [sp, #110]
	ldr	r1, .L313+580
	mov	r2, #49
	bl	ALERTS_parse_comm_command_string
	ldr	r3, .L313+584
	ldrsh	r3, [r3, #0]
	cmp	r3, #13
	bne	.L33
	bl	Refresh_Screen
	b	.L33
.L34:
.LBE179:
.LBE178:
	ldr	r2, .L313+588
	ldr	r0, .L313+592
	ldr	r1, [r2, ip, asl #2]
	mov	r2, r3
	bl	Alert_Message_va
.L33:
	add	sp, sp, #176
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.LFE67:
	.size	CENT_COMM_process_incoming_packet_from_comm_server, .-CENT_COMM_process_incoming_packet_from_comm_server
	.section	.bss.msg_being_built,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	msg_being_built, %object
	.size	msg_being_built, 28
msg_being_built:
	.space	28
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"CI: rcvd new message, forcing prior message termina"
	.ascii	"tion\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/cent_comm.c\000"
.LC2:
	.ascii	"Commserver msg receipt unexpected allocation\000"
.LC3:
	.ascii	"Commserver msg receipt memory unavailable\000"
.LC4:
	.ascii	"From commserver unexpected mid\000"
.LC5:
	.ascii	"From commserver: rcvd UNKNOWN msg\000"
.LC6:
	.ascii	"Commserver inbound msg CRC failed!\000"
.LC7:
	.ascii	"Commserver inbound msg bigger than expected!\000"
.LC8:
	.ascii	"Commserver inbound msg unexp packet!\000"
.LC9:
	.ascii	"Commserver inbound msg unexp MID!\000"
.LC10:
	.ascii	"Not expecting a registration ACK\000"
.LC11:
	.ascii	"Not expecting an alerts ACK or NAK.\000"
.LC12:
	.ascii	"Not expecting a flow recording ACK.\000"
.LC13:
	.ascii	"CI Flow Recording: pending_first not in use!\000"
.LC14:
	.ascii	"CI Flow Recording: current_msg ptr is NULL!\000"
.LC15:
	.ascii	"Not expecting a moisture recording ACK.\000"
.LC16:
	.ascii	"CI Moisture Recording: pending_first not in use!\000"
.LC17:
	.ascii	"Not expecting a station history ACK.\000"
.LC18:
	.ascii	"CI Station History: pending_first not in use!\000"
.LC19:
	.ascii	"Not expecting a station report data ACK.\000"
.LC20:
	.ascii	"CI Station Report Data: pending_first not in use!\000"
.LC21:
	.ascii	"Not expecting a POC report data ACK.\000"
.LC22:
	.ascii	"CI POC Report Data: pending_first not in use!\000"
.LC23:
	.ascii	"Not expecting a SYSTEM report data ACK.\000"
.LC24:
	.ascii	"CI SYSTEM Report Data: pending_first not in use!\000"
.LC25:
	.ascii	"Not expecting a lights report data ACK.\000"
.LC26:
	.ascii	"Not expecting a budget report data ACK.\000"
.LC27:
	.ascii	"CI Budget Report Data: pending_first not in use!\000"
.LC28:
	.ascii	"Not expecting a WEATHER_TABLES ACK.\000"
.LC29:
	.ascii	"CI Weather Tables: pending_first not in use!\000"
.LC30:
	.ascii	"Not expecting a check for updates ACK.\000"
.LC31:
	.ascii	"On a HUB: should not receive a check for updates AC"
	.ascii	"K\000"
.LC32:
	.ascii	"is a HUB: one update coming\000"
.LC33:
	.ascii	"is a HUB: two updates coming\000"
.LC34:
	.ascii	"Not a HUB: %u should not receive this ACK\000"
.LC35:
	.ascii	"Not expecting a weather data ACK.\000"
.LC36:
	.ascii	"Not expecting a rain indication ACK.\000"
.LC37:
	.ascii	"Not expecting a verify firmware version ACK.\000"
.LC38:
	.ascii	"Not expecting a pdata ACK or NAK.\000"
.LC39:
	.ascii	"IHSFY ignored. CHANGES need to be sent first.\000"
.LC40:
	.ascii	"IHSFY processed (as a poll msg). Checking firmware "
	.ascii	"first.\000"
.LC41:
	.ascii	"CI: msg when not idle?\000"
.LC42:
	.ascii	"IHSFY processed. Checking firmware first.\000"
.LC43:
	.ascii	"Not expecting a check firmware version ACK.\000"
.LC44:
	.ascii	"detected pdata changes that need to send first %d %"
	.ascii	"d\000"
.LC45:
	.ascii	"pdata message already in process\000"
.LC46:
	.ascii	"Not expecting a program data request ACK.\000"
.LC47:
	.ascii	"Not expecting a mobile status screen ACK.\000"
.LC48:
	.ascii	"Invalid lights command rcvd\000"
.LC49:
	.ascii	"switching -FL option ON\000"
.LC50:
	.ascii	"switching -FL option OFF\000"
.LC51:
	.ascii	"Should not receive a check for updates request\000"
.LC52:
	.ascii	"switching -HUB option ON\000"
.LC53:
	.ascii	"switching -HUB option OFF\000"
.LC54:
	.ascii	"switching -AQUAPONICS option ON\000"
.LC55:
	.ascii	"switching -AQUAPONICS option OFF\000"
.LC56:
	.ascii	"CI: in-appropriate poll msg rcvd\000"
.LC57:
	.ascii	"CI: poll msg when not ready : mode=%u\000"
.LC58:
	.ascii	"Not expecting a no more messages ACK.\000"
.LC59:
	.ascii	"Not expecting a hub is busy ACK.\000"
.LC60:
	.ascii	"NON HUB: rcvd hub list from commserver!\000"
.LC61:
	.ascii	"HUB: rcvd hub list as %u bytes\000"
.LC62:
	.ascii	"HUB: hub list queue FULL\000"
.LC63:
	.ascii	"HUB ERROR: rcvd hub list as %u bytes\000"
.LC64:
	.ascii	"NON HUB: rcvd stop and cancel hub distribution from"
	.ascii	" commserver!\000"
.LC65:
	.ascii	"NON HUB: rcvd start hub distribution from commserve"
	.ascii	"r!\000"
.LC66:
	.ascii	"Unhandled MID %d from COMMSERVER\000"
.LC67:
	.ascii	"ROUTER: attempt to process unexp packet, port %s, c"
	.ascii	"lass %u\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.byte	0x4
	.4byte	.LCFI4-.LFB44
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.byte	0x4
	.4byte	.LCFI5-.LFB64
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.byte	0x4
	.4byte	.LCFI6-.LFB67
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0xc8
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/cent_comm.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x2d3
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF68
	.byte	0x1
	.4byte	.LASF69
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0xb50
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0xb6f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0xf8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x670
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x68e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0xddd
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x4a4
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x541
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x1
	.byte	0x4d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x6eb
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x71e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x7a1
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x7cb
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x820
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x851
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x870
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x894
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x8bd
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x965
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x98f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x9b4
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x9e0
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF22
	.byte	0x1
	.2byte	0xa0a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF23
	.byte	0x1
	.2byte	0xa7e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF24
	.byte	0x1
	.2byte	0xa98
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF25
	.byte	0x1
	.2byte	0xab7
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x744
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x761
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x907
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x24d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF30
	.byte	0x1
	.2byte	0xa37
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF31
	.byte	0x1
	.2byte	0xadb
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF32
	.byte	0x1
	.2byte	0xae5
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF33
	.byte	0x1
	.2byte	0xc05
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF34
	.byte	0x1
	.2byte	0xc22
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x175
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x1bd
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x1e3
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x21c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x34a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x385
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x3b0
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x3db
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x406
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x431
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x490
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x52e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x560
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x596
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x64f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x6b8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x6db
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x77e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF53
	.byte	0x1
	.2byte	0xb2e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x16d
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF55
	.byte	0x1
	.byte	0xef
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	0x33
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF58
	.byte	0x1
	.byte	0x5b
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x7
	.4byte	.LASF56
	.byte	0x1
	.2byte	0x14a
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.uleb128 0x7
	.4byte	.LASF57
	.byte	0x1
	.2byte	0x8e1
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LLST3
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF59
	.byte	0x1
	.2byte	0xdcf
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LLST4
	.uleb128 0x2
	.4byte	.LASF60
	.byte	0x1
	.2byte	0xe3f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF61
	.byte	0x1
	.2byte	0xc51
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF62
	.byte	0x1
	.2byte	0x45d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x26c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF64
	.byte	0x1
	.2byte	0x5ba
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x5fb
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF66
	.byte	0x1
	.2byte	0xb8b
	.byte	0x1
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF67
	.byte	0x1
	.2byte	0xed6
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB44
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE44
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB64
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE64
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB67
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI7
	.4byte	.LFE67
	.2byte	0x3
	.byte	0x7d
	.sleb128 200
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF22:
	.ascii	"process_incoming_mobile_acquire_expected_flow_by_st"
	.ascii	"ation_cmd\000"
.LASF57:
	.ascii	"process_incoming_request_to_send_registration\000"
.LASF38:
	.ascii	"process_moisture_sensor_recording_ack\000"
.LASF47:
	.ascii	"process_verify_firmware_version_ack\000"
.LASF48:
	.ascii	"process_commserver_rcvd_program_data_from_us_ack\000"
.LASF59:
	.ascii	"commserver_msg_receipt_error_timer_callback\000"
.LASF11:
	.ascii	"process_incoming_mobile_mvor_cmd\000"
.LASF41:
	.ascii	"process_poc_report_data_ack\000"
.LASF15:
	.ascii	"process_incoming_mobile_now_days_by_group_cmd\000"
.LASF35:
	.ascii	"process_registration_data_ack\000"
.LASF50:
	.ascii	"process_request_to_send_status\000"
.LASF33:
	.ascii	"process_incoming_stop_and_cancel_hub_code_distribut"
	.ascii	"ion\000"
.LASF40:
	.ascii	"process_station_report_data_ack\000"
.LASF0:
	.ascii	"process_no_more_messages_msg_ack\000"
.LASF43:
	.ascii	"process_lights_report_data_ack\000"
.LASF16:
	.ascii	"process_incoming_mobile_now_days_by_station_cmd\000"
.LASF34:
	.ascii	"process_incoming_start_the_hub_code_distribution\000"
.LASF18:
	.ascii	"process_incoming_clear_mlb_cmd\000"
.LASF25:
	.ascii	"process_incoming_clear_rain_by_station_cmd\000"
.LASF65:
	.ascii	"process_check_firmware_version_before_pdata_request"
	.ascii	"_ack\000"
.LASF7:
	.ascii	"process_incoming_rain_shutdown_message\000"
.LASF26:
	.ascii	"process_request_factory_reset_panel_swap_for_new_pa"
	.ascii	"nel\000"
.LASF42:
	.ascii	"process_system_report_data_ack\000"
.LASF10:
	.ascii	"process_incoming_mobile_station_OFF\000"
.LASF5:
	.ascii	"process_incoming_commserver_init_packet\000"
.LASF39:
	.ascii	"process_station_history_ack\000"
.LASF37:
	.ascii	"process_flow_recording_ack\000"
.LASF19:
	.ascii	"process_incoming_stop_all_irrigation_cmd\000"
.LASF53:
	.ascii	"process_commserver_sent_us_a_poll_packet\000"
.LASF1:
	.ascii	"process_hub_is_busy_msg_ack\000"
.LASF61:
	.ascii	"process_packet_from_commserver\000"
.LASF63:
	.ascii	"process_check_for_updates_ack\000"
.LASF24:
	.ascii	"process_incoming_clear_rain_by_group_cmd\000"
.LASF36:
	.ascii	"process_engineering_alerts_ack\000"
.LASF9:
	.ascii	"process_incoming_mobile_station_ON\000"
.LASF66:
	.ascii	"process_incoming_hub_list_from_commserver\000"
.LASF32:
	.ascii	"process_incoming_enable_or_disable_AQUAPONICS_cmd\000"
.LASF17:
	.ascii	"process_incoming_mobile_now_days_by_box_cmd\000"
.LASF2:
	.ascii	"poll_wrap_up_by_sending_no_more_messages_msg_for_hu"
	.ascii	"b_based_controllers\000"
.LASF44:
	.ascii	"process_budget_report_data_ack\000"
.LASF49:
	.ascii	"process_asking_commserver_if_there_is_program_data_"
	.ascii	"for_us_ack\000"
.LASF13:
	.ascii	"process_incoming_mobile_turn_controller_on_off_cmd\000"
.LASF54:
	.ascii	"update_when_last_communicated\000"
.LASF69:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/cent_comm.c\000"
.LASF27:
	.ascii	"process_request_factory_reset_panel_swap_for_old_pa"
	.ascii	"nel\000"
.LASF56:
	.ascii	"build_response_to_mobile_cmd_and_update_session_dat"
	.ascii	"a\000"
.LASF12:
	.ascii	"process_incoming_mobile_lights_cmd\000"
.LASF31:
	.ascii	"process_incoming_request_for_alerts_cmd\000"
.LASF58:
	.ascii	"CENT_COMM_build_and_send_ack_with_optional_job_numb"
	.ascii	"er\000"
.LASF52:
	.ascii	"process_send_all_program_data\000"
.LASF6:
	.ascii	"process_incoming_weather_data_message\000"
.LASF21:
	.ascii	"process_incoming_perform_two_wire_discovery_cmd\000"
.LASF3:
	.ascii	"process_incoming_program_data_message\000"
.LASF8:
	.ascii	"allowed_to_process_irrigation_commands_from_comm_se"
	.ascii	"rver\000"
.LASF14:
	.ascii	"process_incoming_mobile_now_days_all_stations_cmd\000"
.LASF46:
	.ascii	"process_rain_indication_ack\000"
.LASF62:
	.ascii	"process_et_rain_tables_ack\000"
.LASF67:
	.ascii	"CENT_COMM_process_incoming_packet_from_comm_server\000"
.LASF68:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF23:
	.ascii	"process_incoming_clear_rain_all_stations_cmd\000"
.LASF45:
	.ascii	"process_from_commserver_weather_data_receipt_ack\000"
.LASF60:
	.ascii	"process_incoming_commserver_data_packet\000"
.LASF29:
	.ascii	"process_incoming_request_to_check_for_updates\000"
.LASF30:
	.ascii	"process_incoming_enable_or_disable_HUB_cmd\000"
.LASF55:
	.ascii	"terminate_ci_transaction\000"
.LASF51:
	.ascii	"process_ack_from_commserver_for_status_screen\000"
.LASF20:
	.ascii	"process_incoming_stop_irrigation_cmd\000"
.LASF64:
	.ascii	"process_commserver_sent_us_an_ihsfy_packet\000"
.LASF4:
	.ascii	"process_set_time_and_date_from_commserver\000"
.LASF28:
	.ascii	"process_incoming_enable_or_disable_FL_cmd\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
