	.file	"d_two_wire_discovery.c"
	.text
.Ltext0:
	.section	.text.TWO_WIRE_init_discovery_dialog,"ax",%progbits
	.align	2
	.global	TWO_WIRE_init_discovery_dialog
	.type	TWO_WIRE_init_discovery_dialog, %function
TWO_WIRE_init_discovery_dialog:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L3
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L3+4
	mov	r1, #1
	str	r3, [r2, #0]
	ldr	r2, .L3+8
	cmp	r0, r1
	str	r3, [r2, #0]
	ldr	r2, .L3+12
	str	r1, [r2, #0]
	ldr	r2, .L3+16
	str	r3, [r2, #0]
	bxne	lr
	ldr	r0, .L3+20
	b	DIALOG_draw_ok_dialog
.L4:
	.align	2
.L3:
	.word	GuiVar_TwoWireNumDiscoveredStaDecoders
	.word	GuiVar_TwoWireNumDiscoveredPOCDecoders
	.word	GuiVar_TwoWireNumDiscoveredMoisDecoders
	.word	GuiVar_TwoWireDiscoveryState
	.word	GuiVar_TwoWireDiscoveryProgressBar
	.word	599
.LFE0:
	.size	TWO_WIRE_init_discovery_dialog, .-TWO_WIRE_init_discovery_dialog
	.section	.text.FDTO_TWO_WIRE_update_discovery_dialog,"ax",%progbits
	.align	2
	.global	FDTO_TWO_WIRE_update_discovery_dialog
	.type	FDTO_TWO_WIRE_update_discovery_dialog, %function
FDTO_TWO_WIRE_update_discovery_dialog:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r3, .L22
	bne	.L6
	ldr	r2, .L22+4
	ldr	r1, [r2, #0]
	add	r1, r1, #1
	and	r0, r1, #3
	str	r0, [r2, #0]
	ldr	r2, [r3, #0]
	cmp	r2, #159
	bhi	.L7
	tst	r1, #1
	addne	r2, r2, #1
	strne	r2, [r3, #0]
	ldr	r3, .L22+8
	ldr	r2, [r3, #0]
	ldr	r3, .L22
	mov	r2, r2, asl #1
	ldr	r1, [r3, #0]
	cmp	r2, r1
	bls	.L9
	b	.L20
.L7:
	mov	r2, #160
.L20:
	str	r2, [r3, #0]
	b	.L9
.L6:
	mov	r2, #160
	str	r2, [r3, #0]
	ldr	r3, .L22+12
	mov	r2, #5056
	ldr	r2, [r3, r2]
	cmp	r2, #0
	ldrne	r2, .L22+16
	bne	.L21
	ldr	r2, .L22+20
	ldr	r3, [r3, r2]
	cmp	r3, #0
	beq	.L11
	ldr	r2, .L22+24
.L21:
	ldr	r3, .L22+28
	strh	r2, [r3, #0]	@ movhi
	b	.L9
.L11:
	ldr	r3, .L22+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L12
	ldr	r3, .L22+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L12
	ldr	r3, .L22+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldreq	r2, .L22+40
	streq	r3, [r2, #0]
	beq	.L9
.L12:
	ldr	r2, .L22+12
	mov	r3, #0
	mov	r0, r3
	add	r4, r2, #4800
	mov	r1, r3
.L18:
	ldrb	ip, [r2, #224]	@ zero_extendqisi2
	cmp	ip, #3
	addeq	r1, r1, #1
	beq	.L14
	cmp	ip, #8
	addeq	r0, r0, #1
	beq	.L14
	cmp	ip, #2
	addeq	r3, r3, #1
.L14:
	add	r2, r2, #60
	cmp	r2, r4
	bne	.L18
	ldr	r2, .L22+8
	str	r3, [r2, #0]
	ldr	r3, .L22+32
	mov	r2, #2
	str	r0, [r3, #0]
	ldr	r3, .L22+36
	str	r1, [r3, #0]
	ldr	r3, .L22+40
	b	.L20
.L9:
	ldmfd	sp!, {r4, lr}
	b	FDTO_DIALOG_redraw_ok_dialog
.L23:
	.align	2
.L22:
	.word	GuiVar_TwoWireDiscoveryProgressBar
	.word	GuiVar_SpinnerPos
	.word	GuiVar_TwoWireNumDiscoveredStaDecoders
	.word	tpmicro_data
	.word	642
	.word	5060
	.word	641
	.word	GuiLib_CurStructureNdx
	.word	GuiVar_TwoWireNumDiscoveredPOCDecoders
	.word	GuiVar_TwoWireNumDiscoveredMoisDecoders
	.word	GuiVar_TwoWireDiscoveryState
.LFE1:
	.size	FDTO_TWO_WIRE_update_discovery_dialog, .-FDTO_TWO_WIRE_update_discovery_dialog
	.section	.text.FDTO_TWO_WIRE_close_discovery_dialog,"ax",%progbits
	.align	2
	.global	FDTO_TWO_WIRE_close_discovery_dialog
	.type	FDTO_TWO_WIRE_close_discovery_dialog, %function
FDTO_TWO_WIRE_close_discovery_dialog:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L28
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r3, [r3, #0]
	sub	sp, sp, #36
.LCFI2:
	cmp	r3, #2
	mov	r4, r0
	bne	.L25
	cmp	r0, #2
	bne	.L26
.LBB2:
	mov	r3, #0
	str	r3, [r2, #0]
	bl	good_key_beep
	ldr	r3, .L28+4
	mov	r0, sp
	str	r3, [sp, #20]
	mov	r3, #1
	str	r4, [sp, #0]
	str	r3, [sp, #24]
	bl	Display_Post_Command
.LBE2:
	b	.L24
.L25:
	cmp	r3, #1
	bne	.L26
	cmp	r0, #2
	bne	.L26
	bl	bad_key_beep
	b	.L24
.L26:
	bl	good_key_beep
	ldr	r3, .L28+8
	mov	r2, #1
	str	r2, [r3, #0]
	bl	DIALOG_close_ok_dialog
.L24:
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L29:
	.align	2
.L28:
	.word	GuiVar_TwoWireDiscoveryState
	.word	FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog
	.word	g_DIALOG_modal_result
.LFE2:
	.size	FDTO_TWO_WIRE_close_discovery_dialog, .-FDTO_TWO_WIRE_close_discovery_dialog
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_two_wire_discovery.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x59
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x26
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x3b
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x92
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"TWO_WIRE_init_discovery_dialog\000"
.LASF2:
	.ascii	"FDTO_TWO_WIRE_close_discovery_dialog\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_two_wire_discovery.c\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"FDTO_TWO_WIRE_update_discovery_dialog\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
