	.file	"r_serial_port_info.c"
	.text
.Ltext0:
	.section	.text.FDTO_SERIAL_PORT_INFO_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_SERIAL_PORT_INFO_draw_report
	.type	FDTO_SERIAL_PORT_INFO_draw_report, %function
FDTO_SERIAL_PORT_INFO_draw_report:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L15
	ldr	r1, .L15+4
	mov	r2, r0
	ldr	r0, [r3, #80]
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldr	ip, .L15+8
	str	r0, [r1, #0]
	ldr	r1, [r3, #84]
	ldr	r3, .L15+12
	str	r1, [r3, #0]
	ldr	r3, .L15+16
	ldr	r4, [r3, ip]
	ldr	ip, .L15+20
	str	r4, [ip, #0]
	ldr	ip, .L15+24
	ldr	r4, [r3, ip]
	ldr	ip, .L15+28
	str	r4, [ip, #0]
	ldr	ip, .L15+32
	ldr	r4, [r3, ip]
	ldr	ip, .L15+36
	str	r4, [ip, #0]
	ldr	ip, .L15+40
	ldr	r4, [r3, ip]
	ldr	ip, .L15+44
	str	r4, [ip, #0]
	ldr	ip, .L15+48
	ldr	r4, [r3, ip]
	ldr	ip, .L15+52
	str	r4, [ip, #0]
	ldr	ip, .L15+56
	ldr	r4, [r3, ip]
	ldr	ip, .L15+60
	str	r4, [ip, #0]
	ldr	ip, .L15+64
	mov	r4, #56
	mla	ip, r4, r0, ip
	ldr	ip, [ip, #8]
	cmp	ip, #0
	ldr	ip, .L15+68
	ldr	ip, [ip, #0]
	beq	.L2
	tst	ip, #65536
	moveq	ip, #0
	movne	ip, #1
	b	.L3
.L2:
	tst	ip, #65536
	movne	ip, #0
	moveq	ip, #1
.L3:
	ldr	r4, .L15+72
	strb	ip, [r3, r4]
	ldr	r3, .L15+64
	mov	ip, #56
	mla	r3, ip, r1, r3
	ldr	r3, [r3, #8]
	cmp	r3, #0
	ldr	r3, .L15+68
	ldrne	r4, [r3, #0]
	andne	r4, r4, #1
	bne	.L5
	ldr	r3, [r3, #0]
	tst	r3, #1
	movne	r4, #0
	moveq	r4, #1
.L5:
	ldr	r3, .L15+16
	ldr	ip, .L15+76
	strb	r4, [r3, ip]
	ldr	r4, .L15+72
	ldrb	r5, [r3, r4]	@ zero_extendqisi2
	ldr	r4, .L15+80
	str	r5, [r4, #0]
	ldrb	r4, [r3, ip]	@ zero_extendqisi2
	ldr	ip, .L15+84
	str	r4, [ip, #0]
	ldr	ip, .L15+64
	mov	r4, #56
	mla	r0, r4, r0, ip
	ldr	r0, [r0, #12]
	cmp	r0, #0
	ldr	r0, .L15+68
	ldr	r0, [r0, #0]
	beq	.L6
	tst	r0, #32
	moveq	r0, #0
	movne	r0, #1
	b	.L7
.L6:
	tst	r0, #32
	movne	r0, #0
	moveq	r0, #1
.L7:
	ldr	ip, .L15+88
	strb	r0, [r3, ip]
	ldr	r3, .L15+64
	mov	r0, #56
	mla	r1, r0, r1, r3
	ldr	r3, [r1, #12]
	cmp	r3, #0
	ldr	r3, .L15+68
	ldr	r3, [r3, #0]
	beq	.L8
	tst	r3, #4
	moveq	r0, #0
	movne	r0, #1
	b	.L9
.L8:
	tst	r3, #4
	movne	r0, #0
	moveq	r0, #1
.L9:
	ldr	r3, .L15+16
	ldr	r1, .L15+92
	ldr	r4, .L15+96
	strb	r0, [r3, r1]
	ldr	r0, .L15+88
	ldrb	ip, [r3, r0]	@ zero_extendqisi2
	ldrb	r1, [r3, r1]	@ zero_extendqisi2
	ldr	r3, .L15+100
	ldr	r0, .L15+104
	str	r1, [r3, #0]
	ldr	r3, .L15+108
	str	ip, [r0, #0]
	ldr	ip, [r3, #44]
	ldr	r0, .L15+112
	adds	r1, ip, #0
	movne	r1, #1
	str	r1, [r0, #0]
	ldr	r0, [r3, #68]
	adds	r3, r0, #0
	movne	r3, #1
	cmp	r1, #0
	ldrneh	r1, [ip, #20]
	ldr	ip, .L15+116
	cmp	r3, #0
	str	r3, [r4, #0]
	str	r1, [ip, #0]
	ldrneh	r3, [r0, #20]
	ldr	r1, .L15+120
	cmp	r2, #1
	str	r3, [r1, #0]
	ldr	r3, .L15+16
	ldr	r1, .L15+124
	ldr	r0, [r3, r1]
	ldr	r1, .L15+128
	str	r0, [r1, #0]
	mov	r1, #8576
	ldr	r0, [r3, r1]
	ldr	r1, .L15+132
	str	r0, [r1, #0]
	ldr	r1, [r3, #16]
	ldr	r3, .L15+136
	str	r1, [r3, #0]
	bne	.L12
	mov	r0, #99
	mov	r1, #0
	bl	GuiLib_ShowScreen
.L12:
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L16:
	.align	2
.L15:
	.word	config_c
	.word	GuiVar_SerialPortOptionA
	.word	8540
	.word	GuiVar_SerialPortOptionB
	.word	SerDrvrVars_s
	.word	GuiVar_SerialPortRcvdA
	.word	8544
	.word	GuiVar_SerialPortXmitA
	.word	12820
	.word	GuiVar_SerialPortRcvdB
	.word	12824
	.word	GuiVar_SerialPortXmitB
	.word	4260
	.word	GuiVar_SerialPortRcvdTP
	.word	4264
	.word	GuiVar_SerialPortXmitTP
	.word	port_device_table
	.word	1073905664
	.word	4300
	.word	8580
	.word	GuiVar_SerialPortCTSA
	.word	GuiVar_SerialPortCTSB
	.word	4303
	.word	8583
	.word	GuiVar_SerialPortXmittingB
	.word	GuiVar_SerialPortCDB
	.word	GuiVar_SerialPortCDA
	.word	xmit_cntrl
	.word	GuiVar_SerialPortXmittingA
	.word	GuiVar_SerialPortXmitLengthA
	.word	GuiVar_SerialPortXmitLengthB
	.word	4296
	.word	GuiVar_SerialPortStateA
	.word	GuiVar_SerialPortStateB
	.word	GuiVar_SerialPortStateTP
.LFE0:
	.size	FDTO_SERIAL_PORT_INFO_draw_report, .-FDTO_SERIAL_PORT_INFO_draw_report
	.section	.text.SERIAL_PORT_INFO_process_report,"ax",%progbits
	.align	2
	.global	SERIAL_PORT_INFO_process_report
	.type	SERIAL_PORT_INFO_process_report, %function
SERIAL_PORT_INFO_process_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	beq	.L19
	cmp	r0, #67
	bne	.L24
	b	.L26
.L19:
	bl	good_key_beep
	ldr	r6, .L27
	ldr	r5, .L27+4
	mov	r4, #0
.L21:
	bl	vPortEnterCritical
	mul	r0, r6, r4
	mov	r1, #0
	add	r0, r0, #4224
	add	r0, r0, #16
	mov	r2, #36
	add	r0, r5, r0
	add	r4, r4, #1
	bl	memset
	bl	vPortExitCritical
	bl	Refresh_Screen
	cmp	r4, #5
	bne	.L21
	ldmfd	sp!, {r4, r5, r6, pc}
.L26:
	ldr	r3, .L27+8
	ldr	r2, .L27+12
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r2, [r3, #4]
	ldr	r3, .L27+16
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, lr}
	b	COMM_OPTIONS_draw_dialog
.L24:
	ldmfd	sp!, {r4, r5, r6, lr}
	b	KEY_process_global_keys
.L28:
	.align	2
.L27:
	.word	4280
	.word	SerDrvrVars_s
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE1:
	.size	SERIAL_PORT_INFO_process_report, .-SERIAL_PORT_INFO_process_report
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_serial_port_info.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x46
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x2c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x77
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"SERIAL_PORT_INFO_process_report\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_serial_port_info.c\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"FDTO_SERIAL_PORT_INFO_draw_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
