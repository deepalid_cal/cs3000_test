	.file	"irri_comm.c"
	.text
.Ltext0:
	.section	.text.irri_add_to_main_list,"ax",%progbits
	.align	2
	.type	irri_add_to_main_list, %function
irri_add_to_main_list:
.LFB5:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L18
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, lr}
.LCFI0:
	ldr	r2, .L18+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L18+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L18+12
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r5, r0
	b	.L15
.L6:
	ldr	r2, [r5, #36]
	cmp	r2, r3
	bne	.L3
	ldrb	r2, [r5, #40]	@ zero_extendqisi2
	ldrb	r3, [r4, #20]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L3
	ldrb	r3, [r5, #52]	@ zero_extendqisi2
	ldrb	r2, [r4, #21]	@ zero_extendqisi2
	cmp	r2, r3, lsr #4
	bne	.L3
	and	r3, r3, #240
	cmp	r3, #32
	bne	.L4
	ldr	r3, [r4, #4]
	mov	r6, #0
	str	r3, [r5, #32]
.L3:
	mov	r1, r5
	ldr	r0, .L18+12
	bl	nm_ListGetNext
	mov	r5, r0
	b	.L15
.L17:
	ldr	r0, .L18+4
	bl	RemovePathFromFileName
	ldr	r2, .L18+16
	mov	r1, r0
	ldr	r0, .L18+20
	bl	Alert_Message_va
.L14:
	mov	r0, r5
	ldr	r1, .L18+4
	ldr	r2, .L18+24
	bl	mem_free_debug
	mov	r5, r7
.L15:
	cmp	r5, #0
	ldr	r3, [r4, #16]
	bne	.L6
	ldr	r2, .L18+28
	mov	r1, #92
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #16]
	cmp	r3, #0
	bne	.L7
	ldrb	r1, [r5, #40]	@ zero_extendqisi2
	ldr	r0, [r5, #36]
	mov	r2, sp
	mov	r3, #8
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r1, r0
	ldr	r0, .L18+32
	bl	Alert_Message_va
	b	.L8
.L7:
	cmp	r6, #0
	beq	.L8
	ldr	r0, .L18+12
	bl	nm_ListGetFirst
	b	.L16
.L12:
	ldrb	r2, [r4, #21]	@ zero_extendqisi2
	ldrb	r3, [r5, #52]	@ zero_extendqisi2
	cmp	r2, r3, lsr #4
	bgt	.L10
	bne	.L11
	ldr	r2, [r4, #16]
	ldr	r3, [r5, #36]
	cmp	r2, r3
	bcc	.L10
	bne	.L11
	ldrb	r2, [r4, #20]	@ zero_extendqisi2
	ldrb	r3, [r5, #40]	@ zero_extendqisi2
	cmp	r2, r3
	bcc	.L10
.L11:
	ldr	r0, .L18+12
	mov	r1, r5
	bl	nm_ListGetNext
.L16:
	cmp	r0, #0
	mov	r5, r0
	bne	.L12
.L10:
	ldr	r1, .L18+4
	ldr	r2, .L18+36
	mov	r0, #56
	bl	mem_malloc_debug
	ldr	r3, [r4, #0]
	str	r3, [r0, #28]
	ldr	r3, [r4, #4]
	mov	r1, r0
	str	r3, [r0, #32]
	ldr	r3, [r4, #16]
	str	r3, [r0, #36]
	ldrb	r3, [r4, #20]	@ zero_extendqisi2
	strb	r3, [r0, #40]
	ldrb	r3, [r0, #52]	@ zero_extendqisi2
	ldrb	r2, [r4, #21]	@ zero_extendqisi2
	bic	r3, r3, #240
	and	r2, r2, #15
	orr	r3, r3, r2, asl #4
	strb	r3, [r0, #52]
	ldr	r3, [r4, #12]
	mov	r2, r5
	str	r3, [r0, #48]
	ldrb	r3, [r0, #53]	@ zero_extendqisi2
	bic	r3, r3, #1
	strb	r3, [r0, #53]
	ldr	r3, [r4, #8]
	str	r3, [r0, #44]
	ldr	r0, .L18+12
	bl	nm_ListInsert
.L8:
	ldr	r3, .L18
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, pc}
.L4:
	mov	r1, r5
	ldr	r0, .L18+12
	bl	nm_ListGetNext
	mov	r1, r5
	ldr	r2, .L18+4
	ldr	r3, .L18+40
	mov	r7, r0
	ldr	r0, .L18+12
	bl	nm_ListRemove_debug
	cmp	r0, #0
	beq	.L14
	b	.L17
.L19:
	.align	2
.L18:
	.word	irri_irri_recursive_MUTEX
	.word	.LC0
	.word	390
	.word	irri_irri
	.word	447
	.word	.LC1
	.word	451
	.word	chain
	.word	.LC2
	.word	514
	.word	445
.LFE5:
	.size	irri_add_to_main_list, .-irri_add_to_main_list
	.section	.text.extract_a_two_wire_cable_anomaly_from_the_incoming_flowsense_token,"ax",%progbits
	.align	2
	.type	extract_a_two_wire_cable_anomaly_from_the_incoming_flowsense_token, %function
extract_a_two_wire_cable_anomaly_from_the_incoming_flowsense_token:
.LFB25:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, lr}
.LCFI1:
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	ldr	r1, [r4, #0]
	mov	r0, sp
	mov	r2, #4
	bl	memcpy
	ldr	r3, [r4, #0]
	add	r3, r3, #4
	str	r3, [r4, #0]
	ldr	r4, [sp, #0]
	cmp	r4, #11
	bhi	.L21
	mov	r7, #1
	str	r7, [r6, r4, asl #2]
	bl	FLOWSENSE_get_controller_index
	cmp	r4, r0
	movne	r0, r7
	bne	.L22
.LBB43:
	ldr	r3, [r5, #0]
	cmp	r3, #2
	beq	.L23
	ldr	r0, .L26
	bl	Alert_Message
.L23:
	ldr	r3, .L26+4
	cmp	r5, r3
	ldreq	r3, .L26+8
	ldreq	r2, .L26+12
	moveq	r1, #3
	streq	r1, [r2, r3]
	b	.L25
.L21:
.LBE43:
	ldr	r0, .L26+16
	bl	Alert_Message
	mov	r0, #0
	b	.L22
.L25:
.LBB44:
	mov	r0, #1
.L22:
.LBE44:
	ldmfd	sp!, {r3, r4, r5, r6, r7, pc}
.L27:
	.align	2
.L26:
	.word	.LC3
	.word	tpmicro_data+5060
	.word	5060
	.word	tpmicro_data
	.word	.LC4
.LFE25:
	.size	extract_a_two_wire_cable_anomaly_from_the_incoming_flowsense_token, .-extract_a_two_wire_cable_anomaly_from_the_incoming_flowsense_token
	.section	.text.IRRI_COMM_if_any_2W_cable_is_over_heated,"ax",%progbits
	.align	2
	.global	IRRI_COMM_if_any_2W_cable_is_over_heated
	.type	IRRI_COMM_if_any_2W_cable_is_over_heated, %function
IRRI_COMM_if_any_2W_cable_is_over_heated:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L33
	mov	r0, #0
	add	r2, r3, #48
.L30:
	ldr	r1, [r3, #4]!
	cmp	r1, #0
	movne	r0, #1
	cmp	r3, r2
	bne	.L30
	bx	lr
.L34:
	.align	2
.L33:
	.word	.LANCHOR0+140
.LFE0:
	.size	IRRI_COMM_if_any_2W_cable_is_over_heated, .-IRRI_COMM_if_any_2W_cable_is_over_heated
	.section	.text.IRRI_COMM_process_incoming__clean_house_request,"ax",%progbits
	.align	2
	.global	IRRI_COMM_process_incoming__clean_house_request
	.type	IRRI_COMM_process_incoming__clean_house_request, %function
IRRI_COMM_process_incoming__clean_house_request:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	mov	r1, #1
	mov	r4, r0
	add	r0, r0, #23
	bl	this_communication_address_is_our_serial_number
	cmp	r0, #0
	beq	.L36
	ldr	r0, .L37
	bl	Alert_Message
	bl	STATION_clean_house_processing
	bl	POC_clean_house_processing
	bl	MOISTURE_SENSOR_clean_house_processing
	bl	SYSTEM_clean_house_processing
	bl	MANUAL_PROGRAMS_clean_house_processing
	bl	STATION_GROUP_clean_house_processing
	bl	LIGHTS_clean_house_processing
	bl	WALK_THRU_clean_house_processing
	ldr	r3, .L37+4
	mov	r2, #1
	str	r2, [r3, #4]
	mov	r0, #61
	mov	r1, r4
	mov	r2, #6
	bl	COMM_UTIL_build_and_send_a_simple_flowsense_message
.L36:
	bl	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation
	ldmfd	sp!, {r4, lr}
	b	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights
.L38:
	.align	2
.L37:
	.word	.LC5
	.word	tpmicro_data
.LFE1:
	.size	IRRI_COMM_process_incoming__clean_house_request, .-IRRI_COMM_process_incoming__clean_house_request
	.section	.text.IRRI_COMM_process_incoming__crc_error_clean_house_request,"ax",%progbits
	.align	2
	.global	IRRI_COMM_process_incoming__crc_error_clean_house_request
	.type	IRRI_COMM_process_incoming__crc_error_clean_house_request, %function
IRRI_COMM_process_incoming__crc_error_clean_house_request:
.LFB2:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI3:
	mov	r1, #1
	mov	r4, r0
	add	r0, r0, #23
	bl	this_communication_address_is_our_serial_number
	cmp	r0, #0
	beq	.L40
	ldr	r0, .L41
	bl	Alert_Message
	ldr	r1, [r4, #12]
	mov	r2, #4
	add	r1, r1, #2
	mov	r0, sp
	bl	memcpy
	ldr	r2, [sp, #0]
	ldr	r3, .L41+4
	add	r3, r3, r2, asl #4
	ldr	r3, [r3, #12]
	blx	r3
	ldr	r3, .L41+8
	mov	r2, #1
	str	r2, [r3, #4]
	mov	r0, #61
	mov	r1, r4
	mov	r2, #6
	bl	COMM_UTIL_build_and_send_a_simple_flowsense_message
.L40:
	bl	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation
	bl	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights
	ldmfd	sp!, {r3, r4, pc}
.L42:
	.align	2
.L41:
	.word	.LC5
	.word	chain_sync_file_pertinants
	.word	tpmicro_data
.LFE2:
	.size	IRRI_COMM_process_incoming__crc_error_clean_house_request, .-IRRI_COMM_process_incoming__crc_error_clean_house_request
	.section	.text.IRRI_COMM_process_stop_command,"ax",%progbits
	.align	2
	.global	IRRI_COMM_process_stop_command
	.type	IRRI_COMM_process_stop_command, %function
IRRI_COMM_process_stop_command:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI4:
	ldr	r4, .L46
	mov	r5, #1
	mov	r6, r0
	mov	r1, #0
	add	r0, r4, #72
	mov	r2, #24
	str	r5, [r4, #68]
	bl	memset
	cmp	r6, #0
	strne	r5, [r4, #84]
	strne	r5, [r4, #88]
	streq	r5, [r4, #80]
	ldmfd	sp!, {r4, r5, r6, pc}
.L47:
	.align	2
.L46:
	.word	.LANCHOR0
.LFE3:
	.size	IRRI_COMM_process_stop_command, .-IRRI_COMM_process_stop_command
	.section	.text.irri_add_to_main_list_for_test_sequential,"ax",%progbits
	.align	2
	.global	irri_add_to_main_list_for_test_sequential
	.type	irri_add_to_main_list_for_test_sequential, %function
irri_add_to_main_list_for_test_sequential:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	b	irri_add_to_main_list
.LFE6:
	.size	irri_add_to_main_list_for_test_sequential, .-irri_add_to_main_list_for_test_sequential
	.section	.text.IRRI_COMM_process_incoming__irrigation_token,"ax",%progbits
	.align	2
	.global	IRRI_COMM_process_incoming__irrigation_token
	.type	IRRI_COMM_process_incoming__irrigation_token, %function
IRRI_COMM_process_incoming__irrigation_token:
.LFB29:
	@ args = 0, pretend = 0, frame = 384
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L236
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI5:
	ldr	r3, [r3, #0]
	sub	sp, sp, #384
.LCFI6:
	str	r3, [sp, #108]
	str	r0, [sp, #100]
	bl	COMM_MNGR_network_is_available_for_normal_use
	ldr	r2, [sp, #100]
	ldr	r4, [r2, #12]
	add	r3, r4, #6
	str	r3, [sp, #336]
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	cmp	r3, #80
	mov	r9, r0
	beq	.L50
	ldr	r0, .L236+4
	bl	RemovePathFromFileName
	ldr	r2, .L236+8
	mov	r1, r0
	ldr	r0, .L236+12
	bl	Alert_Message_va
	b	.L51
.L50:
.LBB88:
	mov	r0, #0
	mov	r1, #9
	bl	vTaskPrioritySet
	ldr	r3, .L236+16
	mov	r5, #0
	ldr	r2, [r3, #20]
	add	r0, sp, #384
	add	r2, r2, #1
	str	r2, [r3, #20]
	ldr	r3, [sp, #100]
	str	r5, [r0, #-44]!
	add	r1, r3, #20
	mov	r2, #3
	bl	memcpy
	ldr	r2, [sp, #100]
	add	r0, sp, #384
	str	r5, [r0, #-40]!
	add	r1, r2, #23
	mov	r2, #3
	bl	memcpy
	ldrb	r3, [r4, #3]	@ zero_extendqisi2
	tst	r3, #4
	moveq	r5, #1
	beq	.L52
	ldr	r6, [sp, #336]
	mov	r1, #11
	mov	r0, r6
	mov	r2, r5
	mov	r3, #16
	bl	PDATA_extract_and_store_changes
	add	r0, r6, r0
	str	r0, [sp, #336]
.L52:
	ldrb	r3, [r4, #3]	@ zero_extendqisi2
	movs	r3, r3, lsr #7
	beq	.L53
.LBB89:
	ldr	r3, .L236+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L236+4
	ldr	r3, .L236+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L236+28
	ldr	r3, [r3, #204]
	cmp	r3, #0
	bne	.L54
	ldr	r5, .L236+32
	mov	r3, #312
	mov	r1, #400
	ldr	r2, .L236+4
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r2, #1104
	ldr	r0, .L236+36
	ldr	r1, [sp, #336]
	bl	memcpy
	ldr	r3, .L236+40
	mov	r2, #1
	str	r2, [r3, #4]
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
.L54:
	ldr	r3, .L236+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, [sp, #336]
	add	r3, r3, #1104
	str	r3, [sp, #336]
	b	.L55
.L53:
.LBE89:
	cmp	r5, #0
	beq	.L55
	ldr	r3, .L236+356
	ldr	r2, [r3, #148]
	cmp	r2, #0
	bne	.L56
	ldr	r1, [r3, #152]
	cmp	r1, #0
	strne	r2, [r3, #152]
.L56:
	ldr	r2, [r3, #140]
	add	r2, r2, #1
	str	r2, [r3, #140]
	bl	CHAIN_SYNC_increment_clean_token_counts
	b	.L57
.L55:
	ldr	r3, .L236+356
	ldr	r2, [r3, #140]
	cmp	r2, #3
	movls	r2, #0
	strls	r2, [r3, #140]
	strls	r2, [r3, #144]
.L57:
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L58
.LBB90:
	add	r0, sp, #380
	add	r0, r0, #2
	ldr	r1, [sp, #336]
	mov	r2, #2
	bl	memcpy
	ldr	r3, [sp, #336]
	mov	r5, #0
	add	r3, r3, #2
	str	r3, [sp, #336]
	b	.L59
.L60:
	ldr	r1, [sp, #336]
	mov	r2, #24
	add	r0, sp, #216
	bl	memcpy
	ldr	r3, [sp, #336]
	add	r0, sp, #216
	add	r3, r3, #24
	str	r3, [sp, #336]
	bl	irri_add_to_main_list
	add	r5, r5, #1
.L59:
	add	r2, sp, #512
	ldrh	r3, [r2, #-130]
	cmp	r5, r3
	bcc	.L60
.L58:
.LBE90:
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L61
	ldr	r6, [sp, #336]
.LBB91:
	add	r0, sp, #380
	mov	r2, #2
	add	r0, r0, #2
	mov	r1, r6
	bl	memcpy
	add	r3, sp, #512
	ldrh	r2, [r3, #-130]
	ldr	r3, .L236+44
	sub	r2, r2, #1
	mov	r2, r2, asl #16
	cmp	r3, r2, lsr #16
	movcs	r5, #2
	movcs	r7, #0
	bcs	.L62
	ldr	r0, .L236+4
	bl	RemovePathFromFileName
	ldr	r2, .L236+48
	mov	r5, #0
	mov	r1, r0
	ldr	r0, .L236+52
	bl	Alert_Message_va
	b	.L63
.L64:
	mov	r2, #12
	add	r0, sp, #292
	bl	memcpy
	add	r0, sp, #292
	bl	IRRI_process_rcvd_action_needed_record
	add	r5, r5, #12
	add	r7, r7, #1
.L62:
	add	r2, sp, #512
	ldrh	r3, [r2, #-130]
	add	r1, r6, r5
	cmp	r7, r3
	blt	.L64
.L63:
.LBE91:
	add	r5, r6, r5
	str	r5, [sp, #336]
.L61:
	ldrb	r3, [r4, #3]	@ zero_extendqisi2
	tst	r3, #8
	beq	.L65
.LBB92:
	mov	r2, #24
	add	r0, sp, #240
	ldr	r1, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	mov	r2, #0
	add	r3, r3, #24
	str	r3, [sp, #336]
	ldr	r3, .L236+56
	str	r2, [r3, #0]
	ldr	r3, .L236+60
	ldrsh	r2, [r3, #0]
	ldr	r3, .L236+64
	cmp	r2, r3
	bne	.L65
	ldr	r2, [sp, #260]
	ldr	r3, .L236+68
	str	r2, [r3, #0]
	bl	Refresh_Screen
.L65:
.LBE92:
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	ldrb	r5, [r4, #3]	@ zero_extendqisi2
.LBB93:
	mov	r1, #400
.LBE93:
	orr	r5, r3, r5, asl #8
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
.LBB97:
	ldr	r2, .L236+4
.LBE97:
	orr	r5, r5, r3, asl #16
	ldrb	r3, [r4, #5]	@ zero_extendqisi2
	orr	r5, r5, r3, asl #24
.LBB98:
	ldr	r3, .L236+72
	ldr	r0, [r3, #0]
	ldr	r3, .L236+76
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L236+80
	ldr	r1, .L236+84
	mov	r2, #0
.L66:
	strb	r2, [r3, #532]
	strb	r2, [r3, #533]
	strb	r2, [r3, #534]
	add	r3, r3, #14208
	add	r3, r3, #16
	cmp	r3, r1
	bne	.L66
	tst	r5, #4
	beq	.L198
.LBB94:
	ldr	r3, [sp, #336]
	mov	r5, #0
	ldrb	r7, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #336]
	b	.L68
.L70:
.LBB95:
	ldr	r1, [sp, #336]
	mov	r2, #4
	add	r0, sp, #376
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r0, [sp, #376]
	add	r3, r3, #4
	str	r3, [sp, #336]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	r6, r0, #0
	beq	.L69
	add	r0, r6, #516
	ldr	r1, [sp, #336]
	mov	r2, #16
	bl	memcpy
	ldr	r3, [sp, #336]
.LBE95:
	add	r5, r5, #1
.LBB96:
	add	r3, r3, #16
	str	r3, [sp, #336]
	b	.L68
.L69:
	ldr	r0, .L236+4
	bl	RemovePathFromFileName
	ldr	r2, .L236+88
	mov	r1, r0
	ldr	r0, .L236+92
	bl	Alert_Message_va
	b	.L67
.L68:
.LBE96:
	cmp	r5, r7
	blt	.L70
.L198:
.LBE94:
	mov	r6, #1
.L67:
	ldr	r3, .L236+72
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE98:
	cmp	r6, #0
	beq	.L95
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	tst	r3, #8
	beq	.L72
.LBB99:
	ldr	r3, [sp, #336]
	ldrb	r6, [r3], #1	@ zero_extendqisi2
	cmp	r6, #12
	str	r3, [sp, #336]
	bhi	.L73
	mov	r5, #0
.LBB100:
.LBB101:
.LBB102:
	ldr	r8, .L236+96
	ldr	sl, .L236+40
	b	.L74
.L75:
.LBE102:
	ldr	r1, [sp, #336]
	ldrb	r7, [r1], #1	@ zero_extendqisi2
	cmp	r7, #11
	str	r1, [sp, #336]
	bhi	.L73
.LBB103:
	add	r0, sp, #376
	mov	r2, #4
	bl	memcpy
	ldr	r3, [sp, #336]
	add	r3, r3, #4
	str	r3, [sp, #336]
	ldr	r3, [sp, #376]
	cmp	r3, r8
	bhi	.L73
	add	r7, r7, #39
.LBE103:
.LBE101:
	add	r5, r5, #1
.LBB105:
.LBB104:
	str	r3, [sl, r7, asl #2]
.L74:
.LBE104:
.LBE105:
	cmp	r5, r6
	bcc	.L75
	b	.L72
.L73:
.LBE100:
	ldr	r0, .L236+4
	bl	RemovePathFromFileName
	ldr	r2, .L236+100
	mov	r1, r0
	ldr	r0, .L236+104
	b	.L228
.L72:
.LBE99:
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	tst	r3, #16
	beq	.L76
.LBB106:
	ldr	r1, [sp, #336]
	mov	r2, #16
	add	r0, sp, #264
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #268]
	add	r3, r3, #16
	str	r3, [sp, #336]
	ldr	r3, [sp, #272]
	sub	r1, r3, #2
	cmp	r3, #0
	cmpne	r1, #1
	bls	.L77
	cmp	r3, #1
	bne	.L78
.L77:
	sub	r0, r2, #1
	cmp	r2, #5
	cmpne	r0, #1
	bls	.L79
.L78:
	ldr	r0, .L236+4
	bl	RemovePathFromFileName
	ldr	r2, .L236+108
	mov	r1, r0
	ldr	r0, .L236+112
.L228:
	bl	Alert_Message_va
	b	.L95
.L79:
	cmp	r2, #5
	bne	.L81
	cmp	r1, #1
	bhi	.L82
	ldr	r5, .L236+316
	mov	r1, #400
	ldr	r2, .L236+4
	ldr	r3, .L236+116
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [sp, #264]
	mov	r1, #0
	add	r2, sp, #372
	add	r3, sp, #348
	bl	POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters
	cmp	r0, #0
	beq	.L87
	ldr	r3, [sp, #272]
	cmp	r3, #2
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	orreq	r3, r3, #64
	orrne	r3, r3, #128
	b	.L227
.L82:
	cmp	r3, #0
	bne	.L86
	ldr	r0, [sp, #264]
	ldr	r1, [sp, #276]
	add	r2, sp, #376
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	cmp	r0, #1
	bne	.L85
	ldr	r5, .L236+120
	ldr	r2, .L236+4
	ldr	r3, .L236+124
	ldr	r0, [r5, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [sp, #376]
	ldr	r3, .L236+128
	add	r3, r3, r2, asl #7
	ldrh	r2, [r3, #140]
	bic	r2, r2, #896
	orr	r2, r2, #64
	strh	r2, [r3, #140]	@ movhi
	b	.L87
.L86:
	cmp	r3, #1
	bne	.L85
	ldr	r0, [sp, #264]
	cmp	r0, #11
	bhi	.L85
	ldr	r1, [sp, #276]
	cmp	r1, #3
	bhi	.L85
	bl	LIGHTS_get_lights_array_index
	ldr	r3, .L236+132
	add	r3, r3, r0
	ldrb	r2, [r3, #976]	@ zero_extendqisi2
	orr	r2, r2, #8
	strb	r2, [r3, #976]
	b	.L85
.L81:
	cmp	r2, #1
	bne	.L85
	cmp	r1, #1
	bhi	.L85
	ldr	r5, .L236+316
	mov	r1, #400
	ldr	r2, .L236+4
	ldr	r3, .L236+136
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [sp, #264]
	mov	r1, #0
	add	r2, sp, #372
	add	r3, sp, #348
	bl	POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters
	cmp	r0, #0
	beq	.L87
	ldr	r3, [sp, #272]
	cmp	r3, #2
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	orreq	r3, r3, #16
	orrne	r3, r3, #32
.L227:
	strb	r3, [r0, #0]
.L87:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
.L85:
	ldr	r5, [sp, #264]
	bl	FLOWSENSE_get_controller_index
	cmp	r5, r0
	bne	.L76
	ldr	r0, .L236+40
	ldr	r3, [r0, #204]
	cmp	r3, #2
	bne	.L90
	add	r0, r0, #208
	add	r1, sp, #268
	mov	r2, #12
	bl	memcmp
	cmp	r0, #0
	beq	.L91
.L90:
	ldr	r0, .L236+4
	bl	RemovePathFromFileName
	ldr	r2, .L236+140
	mov	r1, r0
	ldr	r0, .L236+144
	bl	Alert_Message_va
.L91:
	ldr	r3, .L236+40
	mov	r2, #3
	str	r2, [r3, #204]
.L76:
.LBE106:
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	tst	r3, #16
	moveq	r0, #1
	beq	.L92
	add	r0, sp, #336
	ldr	r1, .L236+148
	ldr	r2, .L236+152
	bl	extract_a_two_wire_cable_anomaly_from_the_incoming_flowsense_token
	cmp	r0, #0
	beq	.L95
.L92:
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L94
	add	r0, sp, #336
	ldr	r1, .L236+156
	ldr	r2, .L236+160
	bl	extract_a_two_wire_cable_anomaly_from_the_incoming_flowsense_token
	cmp	r0, #0
	beq	.L95
.L94:
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	movs	r3, r3, lsr #7
	beq	.L96
.LBB107:
	add	r0, sp, #348
	ldr	r1, [sp, #336]
	mov	r2, #4
	bl	memcpy
	ldr	r3, [sp, #336]
	add	r3, r3, #4
	str	r3, [sp, #336]
	ldr	r3, [sp, #348]
	cmp	r3, #11
	bhi	.L95
	ldr	r2, .L236+28
	add	r3, r3, #36
	mov	r1, #0
	str	r1, [r2, r3, asl #2]
	mov	r0, #1
	b	.L96
.L95:
.LBE107:
	mov	r0, #0
	b	.L97
.L187:
.LBB108:
	ldr	r3, .L236+164
	ldr	r2, [r3, #48]
	ldr	r3, [sp, #340]
	cmp	r2, r3
	ldreq	r3, [sp, #336]
	addeq	r3, r3, #36
	beq	.L233
	ldr	r1, [sp, #336]
	mov	r2, #4
	ldr	r0, .L236+168
	bl	memcpy
	ldr	r1, [sp, #336]
	mov	r2, #4
	add	r1, r1, #4
	ldr	r0, .L236+172
	str	r1, [sp, #336]
	bl	memcpy
	ldr	r1, [sp, #336]
	mov	r2, #4
	add	r1, r1, #4
	ldr	r0, .L236+176
	str	r1, [sp, #336]
	bl	memcpy
	ldr	r1, [sp, #336]
	mov	r2, #4
	add	r1, r1, #4
	ldr	r0, .L236+180
	str	r1, [sp, #336]
	bl	memcpy
	ldr	r1, [sp, #336]
	mov	r2, #4
	add	r1, r1, #4
	ldr	r0, .L236+184
	str	r1, [sp, #336]
	bl	memcpy
	ldr	r1, [sp, #336]
	mov	r2, #4
	add	r1, r1, #4
	ldr	r0, .L236+188
	str	r1, [sp, #336]
	bl	memcpy
	ldr	r1, [sp, #336]
	mov	r2, #4
	add	r1, r1, #4
	ldr	r0, .L236+192
	str	r1, [sp, #336]
	bl	memcpy
	ldr	r1, [sp, #336]
	mov	r2, #4
	add	r1, r1, #4
	ldr	r0, .L236+196
	str	r1, [sp, #336]
	bl	memcpy
	ldr	r1, [sp, #336]
	ldr	r0, .L236+200
	add	r1, r1, #4
	mov	r2, #4
	str	r1, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	add	r3, r3, #4
.L233:
	str	r3, [sp, #336]
	mov	r0, #1
	b	.L99
.L234:
.LBE108:
	add	r0, sp, #336
	ldr	r1, [sp, #340]
	bl	WEATHER_TABLES_extract_et_rain_tables_out_of_msg_from_main
	cmp	r0, #0
	beq	.L97
.L189:
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	tst	r3, #2
	ldrne	r3, .L236+204
	movne	r2, #1
	strne	r2, [r3, #0]
	bne	.L100
.L97:
	ldr	r3, .L236+204
	mov	r2, #0
	cmp	r0, r2
	str	r2, [r3, #0]
	beq	.L101
.L100:
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	tst	r3, #4
	ldrne	r3, .L236+208
	movne	r2, #1
	strne	r2, [r3, #0]
	bne	.L102
.L101:
	ldr	r3, .L236+208
	mov	r2, #0
	cmp	r0, r2
	str	r2, [r3, #0]
	beq	.L103
.L102:
	ldrb	r2, [r4, #3]	@ zero_extendqisi2
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r4, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r4, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	tst	r3, #16384
	ldrne	r2, .L236+40
	movne	r1, #1
	strne	r1, [r2, #28]
	strne	r1, [r2, #24]
	b	.L104
.L190:
	ldr	r3, .L236+28
	mov	r2, #1
	str	r2, [r3, #208]
.L103:
	cmp	r9, #0
	beq	.L105
	cmp	r0, #0
	beq	.L113
	ldrb	r3, [r4, #3]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L107
	add	r0, sp, #336
	ldr	r1, [sp, #340]
	bl	IRRI_FLOW_extract_system_info_out_of_msg_from_main
	cmp	r0, #0
	beq	.L113
.L107:
	ldrb	r3, [r4, #3]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L108
	add	r0, sp, #336
	bl	IRRI_FLOW_extract_the_poc_info_from_the_token
.L105:
	cmp	r0, #0
	beq	.L113
.L108:
	ldrb	r3, [r4, #5]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L110
.LBB109:
	add	r0, sp, #380
	add	r0, r0, #2
	ldr	r1, [sp, #336]
	mov	r2, #2
	bl	memcpy
	ldr	r3, [sp, #336]
	mov	r5, #0
	add	r3, r3, #2
	str	r3, [sp, #336]
	b	.L111
.L112:
	ldr	r1, [sp, #336]
	mov	r2, #8
	add	r0, sp, #304
	bl	memcpy
	ldr	r3, [sp, #336]
	add	r0, sp, #304
	add	r3, r3, #8
	str	r3, [sp, #336]
	bl	IRRI_LIGHTS_process_rcvd_lights_on_list_record
	add	r5, r5, #1
.L111:
	add	r2, sp, #512
	ldrh	r3, [r2, #-130]
	cmp	r5, r3
	bcc	.L112
	b	.L110
.L191:
.LBE109:
.LBB110:
	add	r0, sp, #380
	add	r0, r0, #2
	ldr	r1, [sp, #336]
	mov	r2, #2
	bl	memcpy
	ldr	r3, [sp, #336]
	mov	r5, #0
	add	r3, r3, #2
	str	r3, [sp, #336]
	b	.L114
.L115:
	ldr	r1, [sp, #336]
	mov	r2, #8
	add	r0, sp, #312
	bl	memcpy
	ldr	r3, [sp, #336]
	add	r0, sp, #312
	add	r3, r3, #8
	str	r3, [sp, #336]
	bl	IRRI_LIGHTS_process_rcvd_lights_action_needed_record
	add	r5, r5, #1
.L114:
	add	r2, sp, #512
	ldrh	r3, [r2, #-130]
	cmp	r5, r3
	bcc	.L115
	b	.L116
.L192:
.LBE110:
.LBB111:
	add	r5, sp, #280
	mov	r2, #12
	mov	r0, r5
	ldr	r1, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #340]
	add	r3, r3, #12
	str	r3, [sp, #336]
	ldr	r3, .L236+164
	ldr	r3, [r3, #48]
	cmp	r2, r3
	beq	.L117
	ldmia	r5, {r0, r1}
	ldr	r2, [sp, #288]
	bl	EPSON_set_date_time
	b	.L117
.L193:
.LBE111:
	mov	r0, #1
	mov	r1, #0
	bl	TWO_WIRE_perform_discovery_process
	b	.L118
.L235:
	add	r0, sp, #336
	ldr	r1, .L236+336
	bl	ALERTS_extract_and_store_alerts_from_comm
.L113:
	ldr	r3, .L236+164
	ldr	r2, [sp, #344]
	ldr	r3, [r3, #48]
	cmp	r2, r3
	bne	.L119
.LBB112:
	ldr	r3, .L236+16
	ldr	r5, .L236+356
	ldr	r2, [r3, #24]
	mov	r1, #400
	add	r2, r2, #1
	str	r2, [r3, #24]
	ldr	r3, .L236+212
	ldr	r2, .L236+4
	ldr	r0, [r3, #0]
	mov	r3, #2368
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #448]
	cmp	r3, #1
	movne	r3, #0
	strne	r3, [sp, #324]
	strne	r3, [sp, #320]
	bne	.L121
	ldr	r2, [sp, #344]
	ldr	r3, [sp, #340]
	add	r0, sp, #320
	rsb	r3, r3, r2
	rsbs	r2, r3, #0
	adc	r2, r2, r3
	mov	r1, #17
	bl	PDATA_build_data_to_send
	cmp	r0, #768
	beq	.L121
	cmp	r0, #512
	moveq	r3, #0
	streq	r3, [r5, #448]
.L121:
.LBB113:
	ldr	r3, .L236+20
	ldr	r7, .L236+28
	mov	r5, #0
	ldr	r0, [r3, #0]
	mov	r1, #400
	mov	r3, #1360
	ldr	r2, .L236+4
.LBE113:
	ldr	r6, [sp, #324]
.LBB114:
	str	r5, [sp, #352]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r7, #204]
.LBE114:
	add	r8, r6, #6
.LBB115:
	cmp	r3, r5
	beq	.L200
	mov	r0, #72
	add	r1, sp, #352
	ldr	r2, .L236+4
	ldr	r3, .L236+216
	bl	mem_oabia
	cmp	r0, #0
	streq	r0, [sp, #8]
	beq	.L122
	mov	r1, r5
	mov	r2, #72
	str	r5, [r7, #204]
	add	r0, sp, #112
	bl	memset
	add	r0, sp, #112
	bl	load_this_box_configuration_with_our_own_info
	ldr	r0, [sp, #352]
	add	r1, sp, #112
	mov	r2, #72
	bl	memcpy
	mov	r3, #72
.L200:
	str	r3, [sp, #8]
.L122:
	ldr	r3, .L236+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE115:
	ldr	r2, [sp, #8]
	add	r8, r8, r2
	cmp	r2, #0
	str	r8, [sp, #104]
	movne	r6, #0
	bne	.L123
	rsbs	r6, r6, #1
	movcc	r6, #0
.L123:
	ldr	r3, .L236+28
	ldr	r3, [r3, #68]
	cmp	r3, #0
	ldrne	r3, [sp, #104]
	addne	r3, r3, #24
	strne	r3, [sp, #104]
	ldr	r3, .L236+356
	cmp	r6, #0
	ldr	r2, [r3, #144]
	addne	r2, r2, #1
	strne	r2, [r3, #144]
	bne	.L126
	cmp	r2, #3
	strls	r6, [r3, #144]
	strls	r6, [r3, #140]
.L126:
	ldr	r3, .L236+220
	mov	r7, #0
	add	r2, r3, #8
.L128:
.LBB116:
	ldrh	r1, [r3, #2]!
	cmp	r1, #0
	addne	r7, r7, #1
	andne	r7, r7, #255
	cmp	r3, r2
	bne	.L128
	cmp	r7, #0
	beq	.L203
.LBB117:
	mov	r3, r7, asl #1
	add	r3, r3, #1
	mov	r0, r3
	ldr	r1, .L236+4
	ldr	r2, .L236+224
	str	r3, [sp, #12]
	bl	mem_malloc_debug
	ldr	sl, .L236+228
	mov	r6, #0
	mov	r5, r7
	sub	fp, sl, #192
	mov	r3, r6
	mov	r8, r0
	mov	r7, r0
	strb	r5, [r8], #1
.L131:
	ldrh	r2, [sl], #2
	cmp	r2, #0
	beq	.L130
	add	r1, r6, #96
	mov	r0, r8
	add	r1, fp, r1, asl #1
	mov	r2, #2
	str	r3, [sp, #0]
	bl	memcpy
	ldr	r3, [sp, #0]
	sub	r5, r5, #1
	add	r8, r8, #2
	strh	r3, [sl, #-2]	@ movhi
.L130:
	add	r6, r6, #1
	cmp	r6, #4
	bne	.L131
	cmp	r5, #0
	beq	.L129
	ldr	r0, .L236+4
	bl	RemovePathFromFileName
	ldr	r2, .L236+232
	mov	r1, r0
	ldr	r0, .L236+236
	bl	Alert_Message_va
	mov	r0, r7
	ldr	r1, .L236+4
	ldr	r2, .L236+240
	bl	mem_free_debug
	mov	r7, #0
.L203:
.LBE117:
	str	r7, [sp, #12]
.L129:
.LBE116:
.LBB118:
	ldr	r5, .L236+40
	ldr	r3, [r5, #56]
	cmp	r3, #1
	movne	r3, #0
	strne	r3, [sp, #36]
	strne	r3, [sp, #40]
	bne	.L132
	ldr	r1, .L236+4
	ldr	r2, .L236+244
	mov	r0, #4
	bl	mem_malloc_debug
	mov	r2, #4
	add	r1, r5, #52
	str	r0, [sp, #36]
	bl	memcpy
	mov	r3, #0
	mov	r2, #4
	str	r3, [r5, #56]
	str	r2, [sp, #40]
.L132:
.LBE118:
.LBB119:
	ldr	r5, .L236+40
	mov	r3, #0
	ldr	r2, [r5, #204]
	str	r3, [sp, #356]
	cmp	r2, #1
	strne	r3, [sp, #24]
	bne	.L133
	mov	r0, #12
	add	r1, sp, #356
	ldr	r2, .L236+4
	ldr	r3, .L236+248
	bl	mem_oabia
	cmp	r0, #0
	streq	r0, [sp, #24]
	beq	.L133
	mov	r2, #12
	ldr	r0, [sp, #356]
	add	r1, r5, #208
	bl	memcpy
	mov	r3, #2
	mov	r2, #12
	str	r3, [r5, #204]
	str	r2, [sp, #24]
.L133:
.LBE119:
.LBB120:
	ldr	r3, .L236+252
	mov	r0, #4
	add	r1, sp, #360
	ldr	r2, .L236+4
	bl	mem_oabia
	subs	r3, r0, #0
	beq	.L207
	ldr	r0, [sp, #360]
	ldr	r1, .L236+256
	mov	r2, #4
	bl	memcpy
	mov	r3, #4
.L207:
.LBE120:
.LBB121:
	ldr	r5, .L236+28
.LBE121:
.LBB122:
	str	r3, [sp, #44]
.LBE122:
.LBB123:
	ldr	r3, [r5, #0]
	mov	r6, #0
	cmp	r3, r6
	str	r6, [sp, #364]
	streq	r3, [sp, #28]
	beq	.L135
	mov	r0, #20
	add	r1, sp, #364
	ldr	r2, .L236+4
	ldr	r3, .L236+260
	bl	mem_oabia
	cmp	r0, #0
	streq	r0, [sp, #28]
	beq	.L135
	mov	r2, #20
	ldr	r0, [sp, #364]
	mov	r1, r5
	bl	memcpy
	mov	r2, #20
	str	r6, [r5, #0]
	str	r2, [sp, #28]
.L135:
.LBE123:
.LBB124:
	ldr	r3, .L236+264
	ldr	r5, .L236+40
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L236+268
	ldr	r2, .L236+4
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #16]
	cmp	r3, #0
	streq	r3, [sp, #48]
	beq	.L229
	ldr	r1, .L236+4
	ldr	r2, .L236+272
	mov	r0, #4
	bl	mem_malloc_debug
	add	r1, r5, #16
	mov	r2, #4
	str	r0, [sp, #48]
	bl	memcpy
	mov	r3, #0
	str	r3, [r5, #16]
	mov	r3, #4
.L229:
	ldr	r5, .L236+264
	str	r3, [sp, #52]
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
.LBE124:
.LBB125:
	ldr	r0, [r5, #0]
	ldr	r5, .L236+40
	ldr	r3, .L236+276
	mov	r1, #400
	ldr	r2, .L236+4
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #32]
	cmp	r3, #0
	streq	r3, [sp, #56]
	streq	r3, [sp, #60]
	beq	.L137
	ldr	r1, .L236+4
	ldr	r2, .L236+280
	mov	r0, #4
	bl	mem_malloc_debug
	mov	r2, #4
	add	r1, r5, #32
	str	r0, [sp, #56]
	bl	memcpy
	mov	r3, #0
	mov	r2, #4
	str	r3, [r5, #32]
	str	r2, [sp, #60]
.L137:
	ldr	r3, .L236+264
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE125:
.LBB126:
	bl	WEATHER_get_wind_gage_in_use
	subs	fp, r0, #0
	beq	.L230
	bl	WEATHER_get_wind_gage_box_index
	mov	r5, r0
	bl	FLOWSENSE_get_controller_index
	cmp	r5, r0
	movne	fp, #0
	bne	.L230
	ldr	r1, .L236+4
	mov	r2, #1328
	mov	r0, #8
	bl	mem_malloc_debug
	ldr	r1, .L236+284
	mov	r2, #4
	mov	fp, r0
	bl	memcpy
	add	r0, fp, #4
	ldr	r1, .L236+288
	mov	r2, #4
	bl	memcpy
	mov	r3, #8
	str	r3, [sp, #32]
	b	.L138
.L230:
	str	fp, [sp, #32]
.L138:
.LBE126:
.LBB127:
	ldr	r5, .L236+28
	ldr	r3, [r5, #20]
	cmp	r3, #1
	movne	r3, #0
	strne	r3, [sp, #64]
	strne	r3, [sp, #68]
	bne	.L139
	ldr	r1, .L236+4
	ldr	r2, .L236+292
	mov	r0, #24
	bl	mem_malloc_debug
	mov	r2, #24
	add	r1, r5, #20
	str	r0, [sp, #64]
	bl	memcpy
	mov	r3, #0
	mov	r2, #24
	str	r3, [r5, #20]
	str	r2, [sp, #68]
.L139:
.LBE127:
.LBB128:
	ldr	r1, .L236+40
.LBE128:
.LBB129:
	mov	r3, #0
.L141:
.LBE129:
.LBB130:
	add	r2, r3, r1
	add	r2, r2, #5056
	ldr	r2, [r2, #36]
	cmp	r2, #0
	bne	.L140
	add	r3, r3, #16
	cmp	r3, #128
	bne	.L141
	str	r2, [sp, #72]
	b	.L231
.L237:
	.align	2
.L236:
	.word	my_tick_count
	.word	.LC0
	.word	1935
	.word	.LC6
	.word	comm_stats
	.word	irri_comm_recursive_MUTEX
	.word	289
	.word	.LANCHOR0
	.word	chain_members_recursive_MUTEX
	.word	chain+16
	.word	tpmicro_data
	.word	767
	.word	586
	.word	.LC7
	.word	GuiVar_StopKeyPending
	.word	GuiLib_CurStructureNdx
	.word	635
	.word	GuiVar_StopKeyReasonInList
	.word	system_preserves_recursive_MUTEX
	.word	690
	.word	system_preserves
	.word	system_preserves+56896
	.word	741
	.word	.LC8
	.word	10000
	.word	1118
	.word	.LC9
	.word	1526
	.word	.LC10
	.word	1541
	.word	station_preserves_recursive_MUTEX
	.word	1571
	.word	station_preserves
	.word	lights_preserves
	.word	1597
	.word	1640
	.word	.LC11
	.word	tpmicro_data+5056
	.word	.LANCHOR0+96
	.word	tpmicro_data+5060
	.word	.LANCHOR0+144
	.word	config_c
	.word	weather_preserves+36
	.word	weather_preserves+92
	.word	weather_preserves+80
	.word	weather_preserves+40
	.word	weather_preserves+52
	.word	weather_preserves+100
	.word	weather_preserves+104
	.word	GuiVar_StatusWindGageReading
	.word	GuiVar_StatusWindPaused
	.word	GuiVar_StatusRainSwitchState
	.word	GuiVar_StatusFreezeSwitchState
	.word	comm_mngr_recursive_MUTEX
	.word	1366
	.word	.LANCHOR0+190
	.word	971
	.word	.LANCHOR0+192
	.word	1002
	.word	.LC12
	.word	1006
	.word	1047
	.word	1158
	.word	1182
	.word	tpmicro_data+5052
	.word	1202
	.word	tpmicro_data_recursive_MUTEX
	.word	1246
	.word	1250
	.word	1294
	.word	1298
	.word	tpmicro_comm+96
	.word	tpmicro_comm+108
	.word	1413
	.word	tpmicro_data+5092
	.word	779
	.word	401
	.word	801
	.word	poc_preserves
	.word	poc_preserves_recursive_MUTEX
	.word	878
	.word	1800
	.word	1836
	.word	902
	.word	alerts_struct_user
	.word	.LANCHOR1
	.word	2739
	.word	2762
	.word	2775
	.word	comm_mngr
	.word	2818
	.word	2845
	.word	5060
	.word	5064
	.word	2885
	.word	2898
	.word	2911
	.word	2924
	.word	2937
	.word	tpmicro_comm
	.word	tpmicro_data
	.word	.LANCHOR0
	.word	2986
	.word	2999
	.word	3013
	.word	3027
	.word	3053
	.word	.LC0
	.word	3066
.L140:
	ldr	r1, .L236+428
	mov	r2, #1456
	mov	r0, #128
	bl	mem_malloc_debug
	ldr	r1, .L236+296
	mov	r2, #128
	str	r0, [sp, #72]
	bl	memcpy
	mov	r2, #128
	ldr	r0, .L236+296
	mov	r1, #0
	bl	memset
	mov	r2, #128
.L231:
.LBE130:
	cmp	r9, #0
.LBB131:
	str	r2, [sp, #76]
.LBE131:
	beq	.L143
.LBB132:
	ldr	r1, .L236+428
	ldr	r2, .L236+300
	ldr	r0, .L236+304
	bl	mem_malloc_debug
	ldr	sl, .L236+312
	mov	r1, #400
	ldr	r2, .L236+428
	mov	r9, #0
	mov	r5, #1
	add	r3, r0, #1
	str	r3, [sp, #4]
	ldr	r3, .L236+316
	mov	r6, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L236+308
	bl	xQueueTakeMutexRecursive_debug
	mov	r8, r6
	str	r7, [sp, #20]
	mov	r7, r9
	str	r4, [sp, #16]
.L148:
	ldr	r3, [sl, #24]
	cmp	r3, #0
	beq	.L144
	ldr	r4, [sl, #28]
	bl	FLOWSENSE_get_controller_index
	cmp	r4, r0
	bne	.L144
	mov	r3, #472
	mul	r3, r7, r3
	ldr	r2, .L236+312
	add	r4, r3, #20
	mov	ip, r7
	add	r4, r4, r2
	mov	r6, #0
	mov	r7, r3
.L147:
	add	r2, r6, #1
	mov	r1, #88
	mla	r2, r1, r2, r7
	ldr	r3, .L236+312
	add	r2, r3, r2
	ldrb	r2, [r2, #20]	@ zero_extendqisi2
	tst	r2, #8
	bne	.L145
	tst	r2, #4
	beq	.L146
.L145:
	mov	r1, #0
	mov	r2, #32
	add	r0, sp, #184
	str	ip, [sp, #0]
	bl	memset
	ldr	r2, [r4, #92]
	add	r1, sp, #184
	str	r2, [sp, #184]
	ldr	r2, [r4, #100]
	ldr	r0, [sp, #4]
	str	r2, [sp, #188]
	ldr	r2, [r4, #104]
	add	r9, r9, #1
	str	r2, [sp, #192]
	ldr	r2, [r4, #108]
	and	r9, r9, #255
	str	r2, [sp, #196]
	ldr	r2, [r4, #112]
	add	r5, r5, #32
	str	r2, [sp, #200]
	ldr	r2, [r4, #116]
	str	r2, [sp, #204]
	ldr	r2, [r4, #152]
	str	r2, [sp, #208]
	ldr	r2, [r4, #156]
	str	r2, [sp, #212]
	mov	r2, #32
	bl	memcpy
	ldr	r2, [sp, #4]
	mov	r1, #88
	add	r2, r2, #32
	str	r2, [sp, #4]
	mov	r2, #0
	str	r2, [r4, #100]
	str	r2, [r4, #104]
	add	r2, r6, #1
	mla	r2, r1, r2, r7
	ldr	r3, .L236+312
	add	r2, r3, r2
	ldrb	r1, [r2, #20]	@ zero_extendqisi2
	and	r1, r1, #243
	strb	r1, [r2, #20]
	ldr	ip, [sp, #0]
.L146:
	add	r6, r6, #1
	cmp	r6, #3
	add	r4, r4, #88
	bne	.L147
	mov	r7, ip
.L144:
	add	r7, r7, #1
	cmp	r7, #12
	add	sl, sl, #472
	bne	.L148
	ldr	r3, .L236+316
	ldr	r4, [sp, #16]
	ldr	r0, [r3, #0]
	ldr	r7, [sp, #20]
	bl	xQueueGiveMutexRecursive
	cmp	r9, #0
	mov	r6, r8
	strneb	r9, [r8, #0]
	bne	.L150
	mov	r0, r8
	ldr	r1, .L236+428
	ldr	r2, .L236+320
	bl	mem_free_debug
	mov	r6, r9
	mov	r5, r9
.L150:
.LBE132:
	add	r0, sp, #348
	bl	MOISTURE_SENSOR_find_a_sensor_reading_to_send_to_the_master
	str	r0, [sp, #88]
.L195:
.LBB133:
	ldr	r8, .L236+404
.LBE133:
	ldr	sl, [sp, #348]
.LBB134:
	ldr	r3, [r8, #44]
	cmp	r3, #1
	movne	r3, #0
	strne	r3, [sp, #4]
	strne	r3, [sp, #16]
	bne	.L151
	ldr	r1, .L236+428
	ldr	r2, .L236+324
	mov	r0, #16
	bl	mem_malloc_debug
	mov	r2, #16
	add	r1, r8, #44
	str	r0, [sp, #4]
	bl	memcpy
	mov	r2, #99
	mov	r3, #0
	str	r2, [r8, #48]
	mov	r2, #16
	str	r3, [r8, #44]
	str	r3, [r8, #52]
	str	r3, [r8, #56]
	str	r2, [sp, #16]
.L151:
.LBE134:
.LBB135:
	ldr	r8, .L236+404
	ldr	r3, [r8, #60]
	cmp	r3, #1
	movne	r2, #0
	strne	r2, [sp, #20]
	movne	r9, r2
	bne	.L152
	ldr	r1, .L236+428
	ldr	r2, .L236+328
	mov	r0, #8
	bl	mem_malloc_debug
	add	r1, r8, #60
	mov	r2, #8
	mov	r9, #8
	str	r0, [sp, #20]
	bl	memcpy
	mov	r3, #0
	str	r3, [r8, #60]
	mov	r3, #99
	str	r3, [r8, #64]
.L152:
.LBE135:
.LBB136:
	ldr	r8, .L236+404
	ldr	r3, [r8, #212]
	cmp	r3, #0
	streq	r3, [sp, #80]
	beq	.L232
	ldr	r1, .L236+428
	ldr	r2, .L236+332
	mov	r0, #20
	bl	mem_malloc_debug
	add	r1, r8, #212
	mov	r2, #20
	str	r0, [sp, #80]
	bl	memcpy
	mov	r3, #0
	str	r3, [r8, #212]
	mov	r3, #20
.L232:
.LBE136:
	add	r0, sp, #368
.LBB137:
	str	r3, [sp, #84]
.LBE137:
	bl	WALK_THRU_load_walk_thru_gid_for_token_response
	str	r0, [sp, #92]
	add	r0, sp, #372
	bl	CHAIN_SYNC_load_a_chain_sync_crc_into_the_token_response
	ldr	r2, [sp, #104]
	ldr	r3, [sp, #12]
	add	r8, r2, sl
	ldr	r2, [sp, #40]
	add	r8, r8, r3
	ldr	r3, [sp, #24]
	add	r8, r8, r2
	ldr	r2, [sp, #44]
	add	r8, r8, r3
	ldr	r3, [sp, #28]
	add	r8, r8, r2
	ldr	r2, [sp, #52]
	add	r8, r8, r3
	ldr	r3, [sp, #60]
	add	r8, r8, r2
	ldr	r2, [sp, #32]
	add	r8, r8, r3
	ldr	r3, [sp, #68]
	add	r8, r8, r2
	ldr	r2, [sp, #76]
	add	r8, r8, r3
	ldr	r3, [sp, #16]
	add	r8, r8, r2
	add	r8, r8, r5
	ldr	r2, [sp, #92]
	add	r8, r8, r3
	add	r8, r8, r9
	ldr	r3, [sp, #84]
	add	r8, r8, r2
	add	r8, r8, r0
	str	r0, [sp, #96]
	add	r8, r8, r3
	ldrb	r3, [r4, #3]	@ zero_extendqisi2
	tst	r3, #16
	beq	.L154
	ldr	r0, .L236+336
	bl	ALERTS_get_display_structure_for_pile
	add	sl, sp, #384
	mov	r2, #2
	add	r8, r8, #6
	ldrh	r3, [r0, #0]
	ldr	r0, .L236+336
	str	r3, [sl, #-8]!
	mov	r1, sl
	bl	ALERTS_inc_index
	ldr	r0, .L236+336
	add	r1, sp, #328
	mov	r2, sl
	mov	r3, #6
	bl	ALERTS_pull_object_off_pile
.L154:
	ldr	r3, .L236+340
	ldr	r2, [r3, #0]
	cmp	r8, r2
	bls	.L155
	str	r8, [r3, #0]
	mov	r0, r8
	mov	r1, #1
	bl	Alert_largest_token_size
.L155:
	ldr	r2, .L236+344
	mov	r0, r8
	ldr	r1, .L236+428
	bl	mem_malloc_debug
	mov	r3, #0
	mov	r2, #81
	strb	r2, [r0, #0]
	strb	r3, [r0, #1]
	strb	r3, [r0, #2]
	strb	r3, [r0, #3]
	strb	r3, [r0, #4]
	strb	r3, [r0, #5]
	ldr	r2, [sp, #324]
	mov	sl, r0
	cmp	r2, r3
	add	r0, r0, #6
	str	r0, [sp, #336]
	beq	.L156
	ldr	r1, [sp, #320]
	cmp	r1, r3
	beq	.L156
	strb	r3, [sl, #2]
	strb	r3, [sl, #3]
	strb	r3, [sl, #4]
	mov	r3, #1
	strb	r3, [sl, #5]
	bl	memcpy
	ldr	r2, [sp, #336]
	ldr	r3, [sp, #324]
	ldr	r0, [sp, #320]
	add	r3, r2, r3
	ldr	r1, .L236+428
	ldr	r2, .L236+348
	str	r3, [sp, #336]
	bl	mem_free_debug
.L156:
	ldr	r2, [sp, #8]
	cmp	r2, #0
	beq	.L157
	ldr	r1, [sp, #352]
	cmp	r1, #0
	beq	.L157
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #2048
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r2, [sp, #8]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #8]
	ldr	r0, [sp, #352]
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+352
	str	r3, [sp, #336]
	bl	mem_free_debug
.L157:
	ldr	r3, .L236+356
	ldr	r2, [r3, #148]
	cmp	r2, #0
	bne	.L158
	ldr	r3, [r3, #156]
	cmp	r3, #0
	beq	.L159
.L158:
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #67108864
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r2, [sl, #4]
	strb	r3, [sl, #5]
.L159:
	ldr	r3, .L236+404
	ldr	r2, [r3, #68]
	cmp	r2, #0
	beq	.L160
	ldrb	r1, [sl, #3]	@ zero_extendqisi2
	ldrb	r2, [sl, #2]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #8
	ldrb	r1, [sl, #4]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #16
	ldrb	r1, [sl, #5]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #24
	orr	r2, r2, #2
	mov	r1, r2, lsr #8
	strb	r2, [sl, #2]
	strb	r1, [sl, #3]
	mov	r1, r2, lsr #16
	mov	r2, r2, lsr #24
	strb	r1, [sl, #4]
	strb	r2, [sl, #5]
	add	r1, r3, #72
	mov	r2, #24
	ldr	r0, [sp, #336]
	str	r3, [sp, #0]
	bl	memcpy
	ldr	r2, [sp, #336]
	ldr	r3, [sp, #0]
	add	r2, r2, #24
	str	r2, [sp, #336]
	mov	r2, #0
	str	r2, [r3, #68]
.L160:
	ldr	r3, [sp, #12]
	cmp	r3, #0
	beq	.L161
	cmp	r7, #0
	beq	.L161
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	mov	r1, r7
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #4
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r2, [sp, #12]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #12]
	mov	r0, r7
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+360
	str	r3, [sp, #336]
	bl	mem_free_debug
.L161:
	ldr	r3, [sp, #40]
	cmp	r3, #0
	beq	.L162
	ldr	r2, [sp, #36]
	cmp	r2, #0
	beq	.L162
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #8
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r1, [sp, #36]
	ldr	r2, [sp, #40]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #40]
	ldr	r0, [sp, #36]
	add	r3, r3, r2
	ldr	r1, .L236+428
	mov	r2, #2832
	str	r3, [sp, #336]
	bl	mem_free_debug
.L162:
	ldr	r3, [sp, #24]
	cmp	r3, #0
	beq	.L163
	ldr	r1, [sp, #356]
	cmp	r1, #0
	beq	.L163
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #16
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r2, [sp, #24]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #24]
	ldr	r0, [sp, #356]
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+364
	str	r3, [sp, #336]
	bl	mem_free_debug
.L163:
	ldr	r1, .L236+400
	mov	r2, #5056
	ldr	r3, [r1, r2]
	cmp	r3, #1
	bne	.L164
	ldrb	r0, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r0, asl #8
	ldrb	r0, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r0, asl #16
	ldrb	r0, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r0, asl #24
	orr	r3, r3, #65536
	mov	r0, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r0, [sl, #3]
	mov	r0, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	mov	r3, #2
	strb	r0, [sl, #4]
	str	r3, [r1, r2]
.L164:
	ldr	r1, .L236+400
	ldr	r2, .L236+368
	ldr	r3, [r1, r2]
	cmp	r3, #1
	bne	.L165
	ldrb	r0, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r0, asl #8
	ldrb	r0, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r0, asl #16
	ldrb	r0, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r0, asl #24
	orr	r3, r3, #131072
	mov	r0, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r0, [sl, #3]
	mov	r0, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	mov	r3, #2
	strb	r0, [sl, #4]
	str	r3, [r1, r2]
.L165:
	ldr	r1, .L236+400
	ldr	r2, .L236+372
	ldr	r3, [r1, r2]
	cmp	r3, #1
	bne	.L166
	ldrb	r0, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r0, asl #8
	ldrb	r0, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r0, asl #16
	ldrb	r0, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r0, asl #24
	orr	r3, r3, #262144
	mov	r0, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r0, [sl, #3]
	mov	r0, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	mov	r3, #2
	strb	r0, [sl, #4]
	str	r3, [r1, r2]
.L166:
	ldr	r3, [sp, #44]
	cmp	r3, #0
	beq	.L167
	ldr	r1, [sp, #360]
	cmp	r1, #0
	beq	.L167
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #32
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r2, [sp, #44]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #44]
	ldr	r0, [sp, #360]
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+376
	str	r3, [sp, #336]
	bl	mem_free_debug
.L167:
	ldr	r3, [sp, #28]
	cmp	r3, #0
	beq	.L168
	ldr	r1, [sp, #364]
	cmp	r1, #0
	beq	.L168
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #64
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r2, [sp, #28]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #28]
	ldr	r0, [sp, #364]
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+380
	str	r3, [sp, #336]
	bl	mem_free_debug
.L168:
	ldr	r3, [sp, #52]
	cmp	r3, #0
	beq	.L169
	ldr	r2, [sp, #48]
	cmp	r2, #0
	beq	.L169
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #128
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r1, [sp, #48]
	ldr	r2, [sp, #52]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #52]
	ldr	r0, [sp, #48]
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+384
	str	r3, [sp, #336]
	bl	mem_free_debug
.L169:
	ldr	r3, [sp, #60]
	cmp	r3, #0
	beq	.L170
	ldr	r2, [sp, #56]
	cmp	r2, #0
	beq	.L170
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #256
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r1, [sp, #56]
	ldr	r2, [sp, #60]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #60]
	ldr	r0, [sp, #56]
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+388
	str	r3, [sp, #336]
	bl	mem_free_debug
.L170:
	ldr	r3, [sp, #32]
	cmp	r3, #0
	beq	.L171
	cmp	fp, #0
	beq	.L171
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	mov	r1, fp
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #8388608
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r2, [sp, #32]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #32]
	mov	r0, fp
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+392
	str	r3, [sp, #336]
	bl	mem_free_debug
.L171:
	ldr	r3, .L236+396
	ldr	r3, [r3, #100]
	cmp	r3, #1
	bne	.L172
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #16384
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r2, [sl, #4]
	strb	r3, [sl, #5]
.L172:
	ldr	r3, .L236+396
	ldr	r3, [r3, #104]
	cmp	r3, #1
	bne	.L173
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #32768
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r2, [sl, #4]
	strb	r3, [sl, #5]
.L173:
	ldr	r2, .L236+400
	ldr	r3, [r2, #20]
	cmp	r3, #1
	bne	.L174
	ldrb	r1, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #8
	ldrb	r1, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #16
	ldrb	r1, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #24
	orr	r3, r3, #512
	mov	r1, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r1, [sl, #3]
	mov	r1, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	mov	r3, #0
	strb	r1, [sl, #4]
	str	r3, [r2, #20]
.L174:
	ldr	r2, .L236+404
	ldr	r3, [r2, #200]
	cmp	r3, #1
	bne	.L175
	ldrb	r1, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #8
	ldrb	r1, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #16
	ldrb	r1, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #24
	orr	r3, r3, #1024
	mov	r1, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r1, [sl, #3]
	mov	r1, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	mov	r3, #0
	strb	r1, [sl, #4]
	str	r3, [r2, #200]
.L175:
	ldr	r3, [sp, #68]
	cmp	r3, #0
	beq	.L176
	ldr	r2, [sp, #64]
	cmp	r2, #0
	beq	.L176
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #8192
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r1, [sp, #64]
	ldr	r2, [sp, #68]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #68]
	ldr	r0, [sp, #64]
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+408
	str	r3, [sp, #336]
	bl	mem_free_debug
.L176:
	ldr	r3, [sp, #76]
	cmp	r3, #0
	beq	.L177
	ldr	r2, [sp, #72]
	cmp	r2, #0
	beq	.L177
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #524288
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r1, [sp, #72]
	ldr	r2, [sp, #76]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #76]
	ldr	r0, [sp, #72]
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+412
	str	r3, [sp, #336]
	bl	mem_free_debug
.L177:
	ldr	r3, [sp, #16]
	cmp	r3, #0
	beq	.L178
	ldr	r2, [sp, #4]
	cmp	r2, #0
	beq	.L178
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #134217728
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r1, [sp, #4]
	ldr	r2, [sp, #16]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #16]
	ldr	r0, [sp, #4]
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+416
	str	r3, [sp, #336]
	bl	mem_free_debug
.L178:
	cmp	r9, #0
	beq	.L179
	ldr	r3, [sp, #20]
	cmp	r3, #0
	beq	.L179
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #268435456
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r2, [sl, #4]
	strb	r3, [sl, #5]
	mov	r2, r9
	ldr	r1, [sp, #20]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r0, [sp, #20]
	add	r9, r3, r9
	ldr	r1, .L236+428
	ldr	r2, .L236+420
	str	r9, [sp, #336]
	bl	mem_free_debug
.L179:
	cmp	r6, #0
	cmpne	r5, #0
	beq	.L180
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	mov	r1, r6
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #1
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r2, [sl, #4]
	strb	r3, [sl, #5]
	mov	r2, r5
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	mov	r0, r6
	add	r5, r3, r5
	ldr	r1, .L236+428
	mov	r2, #3040
	str	r5, [sp, #336]
	bl	mem_free_debug
.L180:
	ldr	r2, [sp, #348]
	ldr	r3, [sp, #88]
	cmp	r3, #0
	cmpne	r2, #0
	beq	.L181
	ldrb	r1, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #8
	ldrb	r1, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #16
	ldrb	r1, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #24
	orr	r3, r3, #536870912
	mov	r1, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r1, [sl, #3]
	mov	r1, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r1, [sl, #4]
	ldr	r1, [sp, #88]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r2, [sp, #336]
	ldr	r3, [sp, #348]
	ldr	r0, [sp, #88]
	add	r3, r2, r3
	ldr	r1, .L236+428
	ldr	r2, .L236+424
	str	r3, [sp, #336]
	bl	mem_free_debug
.L181:
	ldr	r2, [sp, #84]
	cmp	r2, #0
	beq	.L182
	ldr	r3, [sp, #80]
	cmp	r3, #0
	beq	.L182
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #1048576
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r1, [sp, #80]
	ldr	r2, [sp, #84]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #84]
	ldr	r0, [sp, #80]
	add	r3, r3, r2
	ldr	r1, .L236+428
	ldr	r2, .L236+432
	str	r3, [sp, #336]
	bl	mem_free_debug
.L182:
	ldr	r3, [sp, #92]
	cmp	r3, #0
	beq	.L183
	ldr	r1, [sp, #368]
	cmp	r1, #0
	beq	.L183
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #2097152
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r2, [sp, #92]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #92]
	ldr	r0, [sp, #368]
	add	r3, r3, r2
	ldr	r1, .L238
	ldr	r2, .L238+4
	str	r3, [sp, #336]
	bl	mem_free_debug
.L183:
	ldr	r3, [sp, #96]
	cmp	r3, #0
	beq	.L184
	ldr	r1, [sp, #372]
	cmp	r1, #0
	beq	.L184
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #4096
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [sl, #5]
	strb	r2, [sl, #4]
	ldr	r2, [sp, #96]
	ldr	r0, [sp, #336]
	bl	memcpy
	ldr	r3, [sp, #336]
	ldr	r2, [sp, #96]
	ldr	r0, [sp, #372]
	add	r3, r3, r2
	ldr	r1, .L238
	ldr	r2, .L238+8
	str	r3, [sp, #336]
	bl	mem_free_debug
.L184:
	ldrb	r3, [r4, #3]	@ zero_extendqisi2
	tst	r3, #16
	beq	.L185
	ldrb	r2, [sl, #3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	add	r1, sp, #328
	orr	r3, r3, r2, asl #8
	ldrb	r2, [sl, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [sl, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	orr	r3, r3, #33554432
	mov	r2, r3, lsr #8
	strb	r3, [sl, #2]
	strb	r2, [sl, #3]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r2, [sl, #4]
	strb	r3, [sl, #5]
	ldr	r0, [sp, #336]
	mov	r2, #6
	bl	memcpy
	ldr	r3, [sp, #336]
	add	r3, r3, #6
	str	r3, [sp, #336]
.L185:
	ldr	r3, .L238+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, sl
	mov	r1, r8
	ldr	r2, [sp, #100]
	mov	r3, #6
	bl	SendCommandResponseAndFree
.L119:
.LBE112:
	mov	r0, #0
	mov	r1, #5
	bl	vTaskPrioritySet
.L51:
.LBE88:
	ldr	r3, .L238+16
	ldr	r2, [r3, #0]
	ldr	r3, [sp, #108]
	rsb	r2, r3, r2
	ldr	r3, .L238+20
	ldr	r1, [r3, #0]
	cmp	r2, r1
	strhi	r2, [r3, #0]
	add	sp, sp, #384
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L96:
.LBB139:
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	tst	r3, #32
	bne	.L187
.L99:
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L189
	b	.L234
.L104:
	tst	r3, #65536
	bne	.L190
	b	.L103
.L110:
	ldrb	r3, [r4, #5]	@ zero_extendqisi2
	tst	r3, #2
	bne	.L191
.L116:
	ldrb	r3, [r4, #5]	@ zero_extendqisi2
	tst	r3, #4
	bne	.L192
.L117:
	ldrb	r3, [r4, #5]	@ zero_extendqisi2
	tst	r3, #8
	bne	.L193
.L118:
	ldrb	r3, [r4, #3]	@ zero_extendqisi2
	tst	r3, #32
	beq	.L113
	b	.L235
.L143:
.LBB138:
	str	r9, [sp, #348]
	mov	r5, r9
	mov	r6, r9
	str	r9, [sp, #88]
	b	.L195
.L239:
	.align	2
.L238:
	.word	.LC0
	.word	3079
	.word	3092
	.word	comm_mngr_recursive_MUTEX
	.word	my_tick_count
	.word	.LANCHOR2
.LBE138:
.LBE139:
.LFE29:
	.size	IRRI_COMM_process_incoming__irrigation_token, .-IRRI_COMM_process_incoming__irrigation_token
	.global	irri_comm
	.section	.bss.__largest_token_resp_size,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	__largest_token_resp_size, %object
	.size	__largest_token_resp_size, 4
__largest_token_resp_size:
	.space	4
	.section	.bss.__largest_token_parsing_delta,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	__largest_token_parsing_delta, %object
	.size	__largest_token_parsing_delta, 4
__largest_token_parsing_delta:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/irri_comm.c\000"
.LC1:
	.ascii	"Error removing from list : %s, %u\000"
.LC2:
	.ascii	"Irri: box not active %s\000"
.LC3:
	.ascii	"irri: two wire cable unexp state\000"
.LC4:
	.ascii	"irri: cable anomoly box_index bad\000"
.LC5:
	.ascii	"Clean House request received\000"
.LC6:
	.ascii	"Unexpected command : %s, %u\000"
.LC7:
	.ascii	"Irri: range check error on number of records : %s, "
	.ascii	"%u\000"
.LC8:
	.ascii	"IRRI_COMM : mlb content error. : %s, %u\000"
.LC9:
	.ascii	"IRRI_COMM: box current problem. : %s, %u\000"
.LC10:
	.ascii	"IRRI_COMM: short struct out of range! : %s, %u\000"
.LC11:
	.ascii	"IRRI_COMM: short unexp state! : %s, %u\000"
.LC12:
	.ascii	"IRRI_COMM : unexp outcome. : %s, %u\000"
	.section	.bss.irri_comm,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	irri_comm, %object
	.size	irri_comm, 240
irri_comm:
	.space	240
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI0-.LFB5
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI1-.LFB25
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI5-.LFB29
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x1a4
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/irri_comm.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x18b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF29
	.byte	0x1
	.4byte	.LASF30
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x2a6
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x679
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x11d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x23b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x279
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x40b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x475
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x498
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x4a8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x4d3
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x503
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x522
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x57b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x594
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x6f8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x720
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x37c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x175
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	0x2a
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.byte	0x56
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.byte	0x6a
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.byte	0xc5
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x105
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x226
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x425
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x5c1
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x6b9
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x540
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x3a5
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x2f7
	.byte	0x1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x758
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB5
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB25
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB29
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI6
	.4byte	.LFE29
	.2byte	0x3
	.byte	0x7d
	.sleb128 420
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF13:
	.ascii	"load_decoder_faults_to_ship_to_master\000"
.LASF26:
	.ascii	"__load_system_gids_to_clear_mlb_for\000"
.LASF29:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF24:
	.ascii	"extract_real_time_weather_data_out_of_msg_from_main"
	.ascii	"\000"
.LASF20:
	.ascii	"IRRI_COMM_process_stop_command\000"
.LASF8:
	.ascii	"load_test_request_to_ship_to_master\000"
.LASF16:
	.ascii	"load_mvor_request_to_ship_to_master\000"
.LASF5:
	.ascii	"__load_box_current_to_ship_to_master\000"
.LASF22:
	.ascii	"__extract_the_box_currents_from_the_incoming_flowse"
	.ascii	"nse_token\000"
.LASF12:
	.ascii	"load_manual_water_request_to_ship_to_master\000"
.LASF27:
	.ascii	"build_poc_update_to_ship_to_master\000"
.LASF17:
	.ascii	"IRRI_COMM_if_any_2W_cable_is_over_heated\000"
.LASF11:
	.ascii	"load_wind_to_ship_to_master\000"
.LASF23:
	.ascii	"extract_a_terminal_short_acknowledge_from_the_incom"
	.ascii	"ing_flowsense_token\000"
.LASF6:
	.ascii	"load_terminal_short_or_no_current_to_ship_to_master"
	.ascii	"\000"
.LASF30:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/irri_comm.c\000"
.LASF0:
	.ascii	"__extract_mlb_info_from_the_incoming_flowsense_toke"
	.ascii	"n\000"
.LASF25:
	.ascii	"load_box_configuration_to_ship_to_master\000"
.LASF31:
	.ascii	"irri_add_to_main_list\000"
.LASF4:
	.ascii	"__extract_stop_key_record_from_the_incoming_flowsen"
	.ascii	"se_token\000"
.LASF14:
	.ascii	"load_light_on_request_to_ship_to_master\000"
.LASF2:
	.ascii	"extract_chain_members_array_from_the_incoming_flows"
	.ascii	"ense_token\000"
.LASF18:
	.ascii	"IRRI_COMM_process_incoming__clean_house_request\000"
.LASF3:
	.ascii	"__extract_action_needed_records_from_the_incoming_f"
	.ascii	"lowsense_token\000"
.LASF15:
	.ascii	"load_light_off_request_to_ship_to_master\000"
.LASF1:
	.ascii	"extract_a_two_wire_cable_anomaly_from_the_incoming_"
	.ascii	"flowsense_token\000"
.LASF21:
	.ascii	"irri_add_to_main_list_for_test_sequential\000"
.LASF28:
	.ascii	"IRRI_COMM_process_incoming__irrigation_token\000"
.LASF7:
	.ascii	"load_two_wire_cable_current_measurement_to_ship_to_"
	.ascii	"master\000"
.LASF10:
	.ascii	"load_rain_bucket_count_to_ship_to_master\000"
.LASF9:
	.ascii	"load_et_gage_count_to_ship_to_master\000"
.LASF19:
	.ascii	"IRRI_COMM_process_incoming__crc_error_clean_house_r"
	.ascii	"equest\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
