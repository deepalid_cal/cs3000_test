	.file	"r_rt_communications.c"
	.text
.Ltext0:
	.section	.text.FDTO_REAL_TIME_COMMUNICATIONS_update_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_COMMUNICATIONS_update_report
	.type	FDTO_REAL_TIME_COMMUNICATIONS_update_report, %function
FDTO_REAL_TIME_COMMUNICATIONS_update_report:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L6
	ldr	r2, .L6+4
	ldr	r1, [r3, #0]
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r4, .L6+8
	str	r1, [r2, #0]
	ldr	r2, [r3, #8]
	ldr	r3, .L6+12
	ldr	r6, [r4, #0]
	str	r2, [r3, #0]
	bl	FLOWSENSE_we_are_poafs
	str	r0, [r4, #0]
	bl	COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members
	ldr	r3, .L6+16
	mov	r1, #56
	ldr	r2, .L6+20
	ldr	r4, .L6+24
	ldr	r5, [r2, #0]
	str	r0, [r3, #0]
	ldr	r3, .L6+28
	ldr	r3, [r3, #80]
	mul	r3, r1, r3
	ldr	r1, .L6+32
	ldr	r3, [r1, r3]
	cmp	r3, #0
	str	r3, [r2, #0]
	streq	r3, [r4, #0]
	beq	.L3
	bl	CONFIG_is_connected
	str	r0, [r4, #0]
.L3:
	ldr	r4, .L6+36
	mov	r1, #400
	ldr	r2, .L6+40
	mov	r3, #73
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L6+44
	ldr	r0, [r4, #0]
	ldr	r2, [r3, #12]
	ldr	r3, .L6+48
	cmp	r2, #4
	movhi	r2, #0
	movls	r2, #1
	str	r2, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L6+8
	ldr	r3, [r3, #0]
	cmp	r6, r3
	bne	.L4
	ldr	r3, .L6+20
	ldr	r3, [r3, #0]
	cmp	r5, r3
	beq	.L5
.L4:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, lr}
	b	FDTO_Redraw_Screen
.L5:
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_Refresh
.L7:
	.align	2
.L6:
	.word	comm_mngr
	.word	GuiVar_LiveScreensCommFLCommMngrMode
	.word	GuiVar_LiveScreensCommFLEnabled
	.word	GuiVar_LiveScreensCommFLInForced
	.word	GuiVar_FLNumControllersInChain
	.word	GuiVar_LiveScreensCommCentralEnabled
	.word	GuiVar_LiveScreensCommCentralConnected
	.word	config_c
	.word	port_device_table
	.word	tpmicro_data_recursive_MUTEX
	.word	.LC0
	.word	tpmicro_comm
	.word	GuiVar_LiveScreensCommTPMicroConnected
.LFE0:
	.size	FDTO_REAL_TIME_COMMUNICATIONS_update_report, .-FDTO_REAL_TIME_COMMUNICATIONS_update_report
	.section	.text.FDTO_REAL_TIME_COMMUNICATIONS_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_COMMUNICATIONS_draw_report
	.type	FDTO_REAL_TIME_COMMUNICATIONS_draw_report, %function
FDTO_REAL_TIME_COMMUNICATIONS_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	str	lr, [sp, #-4]!
.LCFI1:
	mov	r2, r0
	bne	.L9
	mov	r0, #93
	mvn	r1, #0
	bl	GuiLib_ShowScreen
.L9:
	ldr	lr, [sp], #4
	b	FDTO_REAL_TIME_COMMUNICATIONS_update_report
.LFE1:
	.size	FDTO_REAL_TIME_COMMUNICATIONS_draw_report, .-FDTO_REAL_TIME_COMMUNICATIONS_draw_report
	.section	.text.REAL_TIME_COMMUNICATIONS_process_report,"ax",%progbits
	.align	2
	.global	REAL_TIME_COMMUNICATIONS_process_report
	.type	REAL_TIME_COMMUNICATIONS_process_report, %function
REAL_TIME_COMMUNICATIONS_process_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	stmfd	sp!, {r4, lr}
.LCFI2:
	bne	.L14
	ldr	r3, .L15
	ldr	r2, .L15+4
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r4, .L15+8
	ldr	r3, [r3, #4]
	str	r3, [r4, #0]
	bl	KEY_process_global_keys
	ldr	r3, [r4, #0]
	cmp	r3, #10
	ldmnefd	sp!, {r4, pc}
	mov	r0, #1
	ldmfd	sp!, {r4, lr}
	b	LIVE_SCREENS_draw_dialog
.L14:
	ldmfd	sp!, {r4, lr}
	b	KEY_process_global_keys
.L16:
	.align	2
.L15:
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	REAL_TIME_COMMUNICATIONS_process_report, .-REAL_TIME_COMMUNICATIONS_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_communications.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_rt_communications.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x22
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x61
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x6c
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"REAL_TIME_COMMUNICATIONS_process_report\000"
.LASF0:
	.ascii	"FDTO_REAL_TIME_COMMUNICATIONS_update_report\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_communications.c\000"
.LASF1:
	.ascii	"FDTO_REAL_TIME_COMMUNICATIONS_draw_report\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
