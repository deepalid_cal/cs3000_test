	.file	"screen_utils.c"
	.text
.Ltext0:
	.section	.text.FDTO_Redraw_Screen,"ax",%progbits
	.align	2
	.global	FDTO_Redraw_Screen
	.type	FDTO_Redraw_Screen, %function
FDTO_Redraw_Screen:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	mov	r4, r0
	bl	DIALOG_a_dialog_is_visible
	cmp	r0, #0
	ldmnefd	sp!, {r4, pc}
.LBB4:
	ldr	r3, .L3
	ldr	r2, .L3+4
	ldr	r3, [r3, #0]
	mov	r1, #36
	mla	r3, r1, r3, r2
	mov	r0, r4
	ldr	r3, [r3, #20]
	blx	r3
	ldmfd	sp!, {r4, pc}
.L4:
	.align	2
.L3:
	.word	.LANCHOR1
	.word	.LANCHOR0
.LBE4:
.LFE7:
	.size	FDTO_Redraw_Screen, .-FDTO_Redraw_Screen
	.section	.text.FDTO_DrawStr,"ax",%progbits
	.align	2
	.global	FDTO_DrawStr
	.type	FDTO_DrawStr, %function
FDTO_DrawStr:
.LFB0:
	@ args = 4, pretend = 4, frame = 260
	@ frame_needed = 0, uses_anonymous_args = 1
	str	r3, [sp, #-4]!
.LCFI1:
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI2:
	sub	sp, sp, #304
.LCFI3:
	add	r4, sp, #44
	mov	r6, r1
	mov	r7, r0
	ldr	r1, [sp, #324]
	mov	r5, r2
	mov	r0, r4
	add	r2, sp, #328
	str	r2, [sp, #300]
	bl	vsprintf
	mov	ip, #6
	mov	r3, #0
	mov	r7, r7, asl #16
	mov	r1, r6, asl #16
	str	r3, [sp, #12]
	str	r3, [sp, #16]
	str	r3, [sp, #24]
	str	r3, [sp, #28]
	str	r3, [sp, #32]
	str	r3, [sp, #36]
	mov	r3, #15
	mov	r2, #1
	and	r5, r5, #255
	str	r3, [sp, #40]
	mov	r1, r1, asr #16
	mvn	r3, #0
	str	r4, [sp, #0]
	str	r2, [sp, #4]
	str	r5, [sp, #8]
	mul	ip, r0, ip
	mov	r0, r7, asr #16
	mov	ip, ip, asl #16
	mov	ip, ip, asr #16
	str	ip, [sp, #20]
	bl	GuiLib_DrawStr
	add	sp, sp, #304
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	add	sp, sp, #4
	bx	lr
.LFE0:
	.size	FDTO_DrawStr, .-FDTO_DrawStr
	.section	.text.FDTO_show_time_date,"ax",%progbits
	.align	2
	.global	FDTO_show_time_date
	.type	FDTO_show_time_date, %function
FDTO_show_time_date:
.LFB1:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI4:
	ldrb	r3, [r0, #4]	@ zero_extendqisi2
	ldrb	r2, [r0, #5]	@ zero_extendqisi2
	sub	sp, sp, #72
.LCFI5:
	mov	r1, #200
	mov	r4, r0
	str	r1, [sp, #0]
	orr	r2, r3, r2, asl #8
	add	r0, sp, #40
	mov	r1, #32
	mov	r3, #100
	bl	GetDateStr
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	ldrb	ip, [r4, #3]	@ zero_extendqisi2
	orr	r2, r3, r2, asl #8
	ldrb	r3, [r4, #2]	@ zero_extendqisi2
	mov	r1, #32
	orr	r2, r2, r3, asl #16
	mov	r3, #1
	orr	r2, r2, ip, asl #24
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r5, r0
	add	r0, sp, #8
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, r0
	add	r0, sp, #8
	bl	ShaveLeftPad
	mov	r1, #65
	ldr	r2, .L7
	mov	r3, r5
	str	r0, [sp, #0]
	ldr	r0, .L7+4
	bl	snprintf
	bl	GuiLib_Refresh
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, pc}
.L8:
	.align	2
.L7:
	.word	.LC0
	.word	GuiVar_DateTimeFullStr
.LFE1:
	.size	FDTO_show_time_date, .-FDTO_show_time_date
	.section	.text.FDTO_show_screen_name_in_title_bar,"ax",%progbits
	.align	2
	.global	FDTO_show_screen_name_in_title_bar
	.type	FDTO_show_screen_name_in_title_bar, %function
FDTO_show_screen_name_in_title_bar:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI6:
	mov	sl, r1
	sub	sp, sp, #44
.LCFI7:
	mov	r7, r0, lsr #16
	mov	r1, #0
	ldr	r0, .L10
	bl	GuiLib_GetTextPtr
	add	r3, r7, #5
	mov	r6, #2
	mov	r3, r3, asl #16
	mov	r4, #0
	mov	r5, #1
	mov	r8, #15
	mov	r2, r6
	mov	r1, #8
	stmib	sp, {r5, r6}
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	str	r4, [sp, #20]
	str	r4, [sp, #24]
	str	r4, [sp, #28]
	str	r4, [sp, #32]
	str	r8, [sp, #36]
	str	r4, [sp, #40]
	add	r7, r7, r8
	mov	r7, r7, asl #16
	str	r0, [sp, #0]
	mov	r0, r3, asr #16
	mvn	r3, #0
	bl	GuiLib_DrawStr
	mov	r0, sl, asl #16
	mov	r1, r4
	mov	r0, r0, lsr #16
	bl	GuiLib_GetTextPtr
	mov	r1, #8
	mov	r2, r5
	mvn	r3, #0
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	str	r4, [sp, #20]
	str	r4, [sp, #24]
	str	r4, [sp, #28]
	str	r4, [sp, #32]
	str	r8, [sp, #36]
	str	r4, [sp, #40]
	stmia	sp, {r0, r5, r6}
	mov	r0, r7, asr #16
	bl	GuiLib_DrawStr
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L11:
	.align	2
.L10:
	.word	865
.LFE2:
	.size	FDTO_show_screen_name_in_title_bar, .-FDTO_show_screen_name_in_title_bar
	.section	.text.FDTO_scroll_left,"ax",%progbits
	.align	2
	.global	FDTO_scroll_left
	.type	FDTO_scroll_left, %function
FDTO_scroll_left:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI8:
	ldr	r5, .L16
	mov	r4, r0
	ldr	r3, [r5, #0]
	mov	r6, r1
	cmp	r3, #0
	bge	.L13
	bl	good_key_beep
	ldr	r3, [r5, #0]
	add	r6, r6, r3
	cmp	r6, #0
	movgt	r3, #0
	str	r6, [r5, #0]
	strgt	r3, [r5, #0]
	ldr	r0, [r5, #0]
	bl	abs
	ldr	r3, .L16+4
	cmp	r4, #201
	str	r0, [r3, #0]
	addhi	r0, r0, r0, lsr #31
	movhi	r0, r0, asr #1
	strhi	r0, [r3, #0]
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, lr}
	b	FDTO_Redraw_Screen
.L13:
	ldmfd	sp!, {r4, r5, r6, lr}
	b	bad_key_beep
.L17:
	.align	2
.L16:
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	GuiVar_ScrollBoxHorizScrollMarker
.LFE3:
	.size	FDTO_scroll_left, .-FDTO_scroll_left
	.section	.text.FDTO_scroll_right,"ax",%progbits
	.align	2
	.global	FDTO_scroll_right
	.type	FDTO_scroll_right, %function
FDTO_scroll_right:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI9:
	ldr	r5, .L22
	rsb	r6, r0, #0
	ldr	r3, [r5, #0]
	mov	r4, r0
	cmp	r3, r6
	mov	r7, r1
	ble	.L19
	bl	good_key_beep
	ldr	r0, [r5, #0]
	rsb	r7, r7, r0
	cmp	r6, r7
	movge	r0, r6
	movlt	r0, r7
	str	r0, [r5, #0]
	bl	abs
	ldr	r3, .L22+4
	cmp	r4, #201
	str	r0, [r3, #0]
	addhi	r0, r0, r0, lsr #31
	movhi	r0, r0, asr #1
	strhi	r0, [r3, #0]
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	FDTO_Redraw_Screen
.L19:
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	bad_key_beep
.L23:
	.align	2
.L22:
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	GuiVar_ScrollBoxHorizScrollMarker
.LFE4:
	.size	FDTO_scroll_right, .-FDTO_scroll_right
	.section	.text.KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen,"ax",%progbits
	.align	2
	.global	KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen
	.type	KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen, %function
KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI10:
	bl	SYSTEM_REPORT_free_report_support
	bl	POC_REPORT_free_report_support
	bl	STATION_REPORT_DATA_free_report_support
	bl	STATION_HISTORY_free_report_support
	ldr	lr, [sp], #4
	b	LIGHTS_REPORT_DATA_free_report_support
.LFE6:
	.size	KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen, .-KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen
	.section	.text.Change_Screen,"ax",%progbits
	.align	2
	.global	Change_Screen
	.type	Change_Screen, %function
Change_Screen:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L31
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI11:
	ldrsh	r2, [r3, #0]
	ldr	r3, [r0, #8]
	mov	r6, r0
	cmp	r2, r3
	beq	.L26
	bl	good_key_beep
	bl	KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen
	ldr	r3, .L31+4
	ldr	r2, [r3, #0]
	cmp	r2, #48
	addls	r2, r2, #1
	strls	r2, [r3, #0]
	bls	.L28
	ldr	r5, .L31+8
	add	r7, r5, #1728
.L29:
	add	r4, r5, #72
	ldmia	r4!, {r0, r1, r2, r3}
	add	ip, r5, #36
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	r4!, {r0, r1, r2, r3}
	add	r5, r5, #36
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [r4, #0]
	cmp	r5, r7
	str	r3, [ip, #0]
	bne	.L29
.L28:
	ldr	r3, .L31+4
	mov	r4, r6
	ldr	r2, [r3, #0]
	ldr	r3, .L31+8
	mov	ip, #36
	mla	ip, r2, ip, r3
	ldmia	r4!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	r4!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [r4, #0]
	mov	r0, r6
	str	r3, [ip, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	Display_Post_Command
.L26:
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	bad_key_beep
.L32:
	.align	2
.L31:
	.word	GuiLib_CurStructureNdx
	.word	.LANCHOR1
	.word	.LANCHOR0
.LFE5:
	.size	Change_Screen, .-Change_Screen
	.section	.text.Redraw_Screen,"ax",%progbits
	.align	2
	.global	Redraw_Screen
	.type	Redraw_Screen, %function
Redraw_Screen:
.LFB8:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI12:
	mov	r3, #2
	sub	sp, sp, #36
.LCFI13:
	str	r3, [sp, #0]
	ldr	r3, .L34
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L35:
	.align	2
.L34:
	.word	FDTO_Redraw_Screen
.LFE8:
	.size	Redraw_Screen, .-Redraw_Screen
	.section	.text.Refresh_Screen,"ax",%progbits
	.align	2
	.global	Refresh_Screen
	.type	Refresh_Screen, %function
Refresh_Screen:
.LFB9:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI14:
	mov	r3, #1
	sub	sp, sp, #36
.LCFI15:
	str	r3, [sp, #0]
	ldr	r3, .L37
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L38:
	.align	2
.L37:
	.word	GuiLib_Refresh
.LFE9:
	.size	Refresh_Screen, .-Refresh_Screen
	.global	screen_history_index
	.global	ScreenHistory
	.section	.bss.screen_history_index,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	screen_history_index, %object
	.size	screen_history_index, 4
screen_history_index:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%s %s\000"
	.section	.bss.ScreenHistory,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	ScreenHistory, %object
	.size	ScreenHistory, 1800
ScreenHistory:
	.space	1800
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI0-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x148
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI4-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x54
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI8-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI9-.LFB4
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI10-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI11-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI12-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI14-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE18:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/screen_utils.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xf0
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF9
	.byte	0x1
	.4byte	.LASF10
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x152
	.byte	0x1
	.uleb128 0x3
	.4byte	0x21
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x6d
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x8c
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x97
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x9f
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST4
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xbf
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x127
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xfa
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST7
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x171
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x1ad
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB7
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI3
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 328
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI5
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 84
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB3
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB5
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI15
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x64
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF11:
	.ascii	"FDTO_Redraw_Screen\000"
.LASF2:
	.ascii	"FDTO_show_screen_name_in_title_bar\000"
.LASF6:
	.ascii	"Change_Screen\000"
.LASF8:
	.ascii	"Refresh_Screen\000"
.LASF5:
	.ascii	"KEY_PROCESS_free_memory_allocated_for_screen_before"
	.ascii	"_changing_screen\000"
.LASF9:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"FDTO_DrawStr\000"
.LASF7:
	.ascii	"Redraw_Screen\000"
.LASF4:
	.ascii	"FDTO_scroll_right\000"
.LASF3:
	.ascii	"FDTO_scroll_left\000"
.LASF10:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/s"
	.ascii	"creen_utils.c\000"
.LASF1:
	.ascii	"FDTO_show_time_date\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
