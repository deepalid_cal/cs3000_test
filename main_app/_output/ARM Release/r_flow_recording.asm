	.file	"r_flow_recording.c"
	.text
.Ltext0:
	.section	.text.nm_FLOW_RECORDING_get_record_count,"ax",%progbits
	.align	2
	.type	nm_FLOW_RECORDING_get_record_count, %function
nm_FLOW_RECORDING_get_record_count:
.LFB1:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L6
	ldr	r2, .L6+4
	ldr	r3, [r3, #0]
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI0:
	mul	r2, r3, r2
	ldr	r3, .L6+8
	add	r2, r2, #488
	ldr	r4, [r2, r3]
	add	r6, r2, r3
	cmp	r4, #0
	beq	.L2
	ldr	r3, [r6, #8]
	add	r5, sp, #4
	str	r3, [r5, #-4]!
	mov	r4, #0
	b	.L3
.L4:
	mov	r0, sp
	ldr	r1, [r6, #0]
	add	r4, r4, #1
	bl	nm_flow_recorder_inc_pointer
.L3:
	ldr	r3, [r6, #4]
	ldr	r2, [sp, #0]
	cmp	r2, r3
	bne	.L4
.L2:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, r6, pc}
.L7:
	.align	2
.L6:
	.word	.LANCHOR0
	.word	14224
	.word	system_preserves
.LFE1:
	.size	nm_FLOW_RECORDING_get_record_count, .-nm_FLOW_RECORDING_get_record_count
	.section	.text.FDTO_FLOW_RECORDING_redraw_scrollbox,"ax",%progbits
	.align	2
	.global	FDTO_FLOW_RECORDING_redraw_scrollbox
	.type	FDTO_FLOW_RECORDING_redraw_scrollbox, %function
FDTO_FLOW_RECORDING_redraw_scrollbox:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L14
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	ldr	r0, [r3, #0]
	bl	FDTO_SYSTEM_load_system_name_into_guivar
	ldr	r3, .L14+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L14+8
	ldr	r3, .L14+12
	bl	xQueueTakeMutexRecursive_debug
	bl	nm_FLOW_RECORDING_get_record_count
	ldr	r5, .L14+16
	ldr	r3, [r5, #0]
	mov	r4, r0
	cmp	r4, r3
	mov	r0, #0
	beq	.L9
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	subs	r6, r0, #0
	bne	.L10
	bl	GuiLib_ScrollBox_Close
	mov	r2, r4, asl #16
	mov	r0, r6
	ldr	r1, .L14+20
	mov	r2, r2, asr #16
	mov	r3, r6
	b	.L13
.L10:
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r5, [r5, #0]
	rsb	r5, r5, r4
	add	r5, r5, r0
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	cmp	r5, r4
	movcs	r5, r4
	ldr	r1, .L14+20
	mov	r2, r4, asl #16
	mov	r3, r5, asl #16
	mov	r0, #0
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
.L13:
	bl	GuiLib_ScrollBox_Init
	bl	GuiLib_Refresh
	b	.L12
.L9:
	mov	r1, r4
	mov	r2, r0
	bl	FDTO_SCROLL_BOX_redraw_retaining_topline
.L12:
	ldr	r3, .L14+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L14+16
	str	r4, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, pc}
.L15:
	.align	2
.L14:
	.word	.LANCHOR0
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	329
	.word	.LANCHOR1
	.word	nm_FLOW_RECORDING_load_guivars_for_scroll_line
.LFE3:
	.size	FDTO_FLOW_RECORDING_redraw_scrollbox, .-FDTO_FLOW_RECORDING_redraw_scrollbox
	.section	.text.nm_FLOW_RECORDING_load_guivars_for_scroll_line,"ax",%progbits
	.align	2
	.type	nm_FLOW_RECORDING_load_guivars_for_scroll_line, %function
nm_FLOW_RECORDING_load_guivars_for_scroll_line:
.LFB2:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	ldr	r7, .L41
	ldr	r2, .L41+4
	ldr	r3, [r7, #0]
	ldr	r8, .L41+8
	sub	sp, sp, #32
.LCFI3:
	mla	r3, r2, r3, r8
	add	r5, sp, #32
	ldr	r3, [r3, #496]
	mov	r0, r0, asl #16
	str	r3, [r5, #-8]!
	mov	r6, r0, asr #16
	bl	nm_FLOW_RECORDING_get_record_count
	mov	r4, #0
	sub	r0, r0, #1
	rsb	sl, r6, r0
	b	.L17
.L18:
	ldr	r3, [r7, #0]
	ldr	r2, .L41+4
	mov	r0, r5
	mla	r3, r2, r3, r8
	add	r4, r4, #1
	ldr	r1, [r3, #488]
	bl	nm_flow_recorder_inc_pointer
.L17:
	cmp	r4, sl
	blt	.L18
	ldr	r4, .L41+12
	ldr	r3, [sp, #24]
	mov	r5, #0
	strb	r5, [r4, #0]
	ldrh	r2, [r3, #4]
	mov	r3, #250
	str	r3, [sp, #0]
	mov	r1, #16
	mov	r3, #150
	add	r0, sp, #8
	bl	GetDateStr
	mov	r2, #17
	mov	r1, r0
	mov	r0, r4
	bl	strlcat
	ldr	r1, .L41+16
	mov	r2, #17
	mov	r0, r4
	bl	strlcat
	mov	r3, #1
	stmia	sp, {r3, r5}
	ldr	r3, [sp, #24]
	mov	r1, #16
	ldr	r2, [r3, #0]
	add	r0, sp, #8
	mov	r3, r5
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r2, #17
	mov	r1, r0
	mov	r0, r4
	bl	strlcat
	ldr	r3, [sp, #24]
	ldr	r2, .L41+20
	ldrh	r0, [r3, #8]
	ldrh	r1, [r3, #6]
	str	r0, [r2, #0]
	mov	r3, #16
	add	r2, sp, #8
	bl	STATION_get_station_number_string
	ldr	r2, .L41+24
	mov	r1, #4
	mov	r3, r0
	ldr	r0, .L41+28
	bl	snprintf
	ldr	r3, [sp, #24]
	ldrh	r2, [r3, #10]
	fmsr	s14, r2	@ int
	ldr	r2, .L41+32
	fuitos	s15, s14
	fsts	s15, [r2, #0]
	ldrh	r2, [r3, #18]
	fmsr	s14, r2	@ int
	ldr	r2, .L41+36
	fuitos	s15, s14
	fsts	s15, [r2, #0]
	ldrh	r2, [r3, #20]
	strh	r2, [sp, #28]	@ movhi
.LBB4:
.LBB5:
	and	r2, r2, #255
	and	r3, r2, #7
	cmp	r3, #1
	ldreq	r0, .L41+40
	ldreq	r1, .L41+16
	beq	.L38
.L19:
	cmp	r3, #2
	ldreq	r0, .L41+40
	ldreq	r1, .L41+44
	beq	.L38
.L21:
	cmp	r3, #3
	ldreq	r0, .L41+40
	ldreq	r1, .L41+48
	beq	.L38
.L22:
	cmp	r3, #4
	bne	.L20
	tst	r2, #64
	ldrne	r0, .L41+40
	ldrne	r1, .L41+52
	bne	.L38
.L23:
	ldrb	r3, [sp, #29]	@ zero_extendqisi2
	tst	r3, #1
	ldrne	r0, .L41+40
	ldrne	r1, .L41+56
	bne	.L38
.L24:
	tst	r2, #16
	bne	.L40
.L25:
	tst	r2, #32
	beq	.L26
.L40:
	ldr	r0, .L41+40
	ldr	r1, .L41+60
	b	.L38
.L26:
	tst	r3, #128
	beq	.L27
	ldr	r1, .L41+64
	mov	r2, #17
	ldr	r0, .L41+40
	bl	strlcat
	b	.L20
.L27:
	tst	r2, #128
	ldrne	r0, .L41+40
	ldrne	r1, .L41+68
	bne	.L38
.L28:
	tst	r3, #2
	bne	.L39
.L29:
	tst	r3, #4
	ldrne	r0, .L41+40
	ldrne	r1, .L41+72
	bne	.L38
.L30:
	tst	r3, #8
	ldrne	r0, .L41+40
	ldrne	r1, .L41+76
	bne	.L38
.L31:
	tst	r3, #64
	ldrne	r0, .L41+40
	ldrne	r1, .L41+80
	bne	.L38
.L32:
	tst	r3, #16
	ldrne	r0, .L41+40
	ldrne	r1, .L41+84
	bne	.L38
.L33:
	tst	r2, #8
	ldrne	r0, .L41+40
	ldrne	r1, .L41+88
	bne	.L38
.L34:
	tst	r3, #32
	beq	.L20
.L39:
	ldr	r0, .L41+40
	ldr	r1, .L41+92
.L38:
	mov	r2, #17
	bl	strlcpy
.L20:
	ldr	r0, .L41+40
	mov	r1, #17
	bl	PadRight
.LBE5:
.LBE4:
	cmp	r6, #0
	bne	.L35
	ldr	r2, [sp, #24]
	ldr	r3, .L41+96
	ldrb	r1, [r2, #4]	@ zero_extendqisi2
	strb	r1, [r3, #4]
	ldrb	r1, [r2, #5]	@ zero_extendqisi2
	strb	r1, [r3, #5]
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	strb	r1, [r3, #1]
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r3, #2]
	ldrb	r2, [r2, #3]	@ zero_extendqisi2
	strb	r2, [r3, #3]
.L35:
	ldr	r4, .L41+96
	ldr	r0, [sp, #24]
	mov	r1, r4
	bl	DT1_IsEqualTo_DT2
	ldr	r3, .L41+100
	cmp	r0, #0
	movne	r2, #0
	strne	r2, [r3, #0]
	bne	.L16
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, [sp, #24]
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	strb	r2, [r4, #4]
	ldrb	r2, [r3, #5]	@ zero_extendqisi2
	strb	r2, [r4, #5]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	strb	r2, [r4, #0]
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	strb	r2, [r4, #1]
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	strb	r2, [r4, #2]
	ldrb	r3, [r3, #3]	@ zero_extendqisi2
	strb	r3, [r4, #3]
.L16:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L42:
	.align	2
.L41:
	.word	.LANCHOR0
	.word	14224
	.word	system_preserves
	.word	GuiVar_FlowRecTimeStamp
	.word	.LC1
	.word	GuiVar_RptController
	.word	.LC2
	.word	GuiVar_RptStation
	.word	GuiVar_FlowRecExpected
	.word	GuiVar_FlowRecActual
	.word	GuiVar_FlowRecFlag
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	.LC11
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	.LC10
	.word	.LANCHOR2
	.word	GuiVar_ShowDivider
.LFE2:
	.size	nm_FLOW_RECORDING_load_guivars_for_scroll_line, .-nm_FLOW_RECORDING_load_guivars_for_scroll_line
	.section	.text.FDTO_FLOW_RECORDING_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_FLOW_RECORDING_draw_report
	.type	FDTO_FLOW_RECORDING_draw_report, %function
FDTO_FLOW_RECORDING_draw_report:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI4:
	mvn	r1, #0
	mov	r5, r0
	mov	r2, #1
	mov	r0, #82
	bl	GuiLib_ShowScreen
	cmp	r5, #1
	bne	.L44
	ldr	r3, .L45
	mov	r0, #0
	strb	r0, [r3, #0]
	ldr	r3, .L45+4
	str	r0, [r3, #0]
	bl	FDTO_SYSTEM_load_system_name_into_guivar
.L44:
	ldr	r4, .L45+8
	mov	r1, #400
	ldr	r2, .L45+12
	ldr	r3, .L45+16
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	nm_FLOW_RECORDING_get_record_count
	ldr	r3, .L45+20
	ldr	r2, .L45+24
	mov	r1, r0
	str	r0, [r3, #0]
	mov	r0, r5
	bl	FDTO_REPORTS_draw_report
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L46:
	.align	2
.L45:
	.word	GuiVar_StationDescription
	.word	.LANCHOR0
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	401
	.word	.LANCHOR1
	.word	nm_FLOW_RECORDING_load_guivars_for_scroll_line
.LFE4:
	.size	FDTO_FLOW_RECORDING_draw_report, .-FDTO_FLOW_RECORDING_draw_report
	.section	.text.FLOW_RECORDING_process_report,"ax",%progbits
	.align	2
	.global	FLOW_RECORDING_process_report
	.type	FLOW_RECORDING_process_report, %function
FLOW_RECORDING_process_report:
.LFB5:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r4, r5, lr}
.LCFI5:
	sub	sp, sp, #44
.LCFI6:
	beq	.L49
	cmp	r0, #20
	bne	.L54
	b	.L55
.L49:
	mov	r5, #80
	b	.L50
.L55:
	mov	r5, #84
.L50:
	bl	SYSTEM_num_systems_in_use
	mov	r4, #1
	ldr	r1, .L56
	mov	r2, #0
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	sub	r3, r0, #1
	mov	r0, r5
	bl	process_uns32
	ldr	r3, .L56+4
	add	r0, sp, #8
	str	r4, [sp, #8]
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L47
.L54:
	mov	r2, #0
	mov	r3, r2
	bl	REPORTS_process_report
.L47:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L57:
	.align	2
.L56:
	.word	.LANCHOR0
	.word	FDTO_FLOW_RECORDING_redraw_scrollbox
.LFE5:
	.size	FLOW_RECORDING_process_report, .-FLOW_RECORDING_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_flow_recording.c\000"
.LC1:
	.ascii	" \000"
.LC2:
	.ascii	"%s\000"
.LC3:
	.ascii	"High Flow\000"
.LC4:
	.ascii	"Low Flow\000"
.LC5:
	.ascii	"No flow meter\000"
.LC6:
	.ascii	"Acquiring flow\000"
.LC7:
	.ascii	"Developing hist.\000"
.LC8:
	.ascii	"Flow too far off\000"
.LC9:
	.ascii	"Checking off\000"
.LC10:
	.ascii	"Not checked\000"
.LC11:
	.ascii	"Unstable flow\000"
.LC12:
	.ascii	"Cycle too short\000"
.LC13:
	.ascii	"Zero flow rate\000"
.LC14:
	.ascii	"Power fail\000"
.LC15:
	.ascii	"Flow too high\000"
	.section	.bss.g_FLOW_RECORDING_previous_time_stamp,"aw",%nobits
	.set	.LANCHOR2,. + 0
	.type	g_FLOW_RECORDING_previous_time_stamp, %object
	.size	g_FLOW_RECORDING_previous_time_stamp, 6
g_FLOW_RECORDING_previous_time_stamp:
	.space	6
	.section	.bss.g_FLOW_RECORDING_line_count,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_FLOW_RECORDING_line_count, %object
	.size	g_FLOW_RECORDING_line_count, 4
g_FLOW_RECORDING_line_count:
	.space	4
	.section	.bss.g_FLOW_RECORDING_index_of_system_to_show,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_FLOW_RECORDING_index_of_system_to_show, %object
	.size	g_FLOW_RECORDING_index_of_system_to_show, 4
g_FLOW_RECORDING_index_of_system_to_show:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_flow_recording.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x8b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.byte	0x46
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.byte	0xab
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x140
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0xf0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x184
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1a7
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI6
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"nm_FLOW_RECORDING_load_guivars_for_scroll_line\000"
.LASF7:
	.ascii	"FLOW_RECORDING_get_flag_str\000"
.LASF3:
	.ascii	"FDTO_FLOW_RECORDING_draw_report\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_flow_recording.c\000"
.LASF0:
	.ascii	"nm_FLOW_RECORDING_get_record_count\000"
.LASF4:
	.ascii	"FLOW_RECORDING_process_report\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"FDTO_FLOW_RECORDING_redraw_scrollbox\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
