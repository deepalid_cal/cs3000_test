	.file	"e_moisture_sensors.c"
	.text
.Ltext0:
	.section	.text.MOISTURE_SENSOR_get_menu_index_for_displayed_sensor,"ax",%progbits
	.align	2
	.type	MOISTURE_SENSOR_get_menu_index_for_displayed_sensor, %function
MOISTURE_SENSOR_get_menu_index_for_displayed_sensor:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L6
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r0, [r3, #0]
	ldr	r2, .L6+4
	mov	r3, #125
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
.LBB10:
	ldr	r3, .L6+8
.LBE10:
	mov	r4, #0
.LBB11:
	ldr	r2, [r3, #0]
	ldr	r3, .L6+12
	ldr	r6, [r3, r2, asl #2]
	sub	r5, r3, #4
.LBE11:
	b	.L2
.L4:
	ldr	r3, [r5, #4]!
	cmp	r3, r6
	beq	.L3
	add	r4, r4, #1
.L2:
	bl	MOISTURE_SENSOR_get_list_count
	cmp	r4, r0
	bcc	.L4
	mov	r4, #0
.L3:
	ldr	r3, .L6
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L7:
	.align	2
.L6:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR1
	.word	.LANCHOR0
.LFE0:
	.size	MOISTURE_SENSOR_get_menu_index_for_displayed_sensor, .-MOISTURE_SENSOR_get_menu_index_for_displayed_sensor
	.section	.text.FDTO_MOISTURE_SENSOR_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_MOISTURE_SENSOR_return_to_menu, %function
FDTO_MOISTURE_SENSOR_return_to_menu:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L9
	ldr	r1, .L9+4
	b	FDTO_GROUP_return_to_menu
.L10:
	.align	2
.L9:
	.word	.LANCHOR2
	.word	MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars
.LFE6:
	.size	FDTO_MOISTURE_SENSOR_return_to_menu, .-FDTO_MOISTURE_SENSOR_return_to_menu
	.section	.text.MOISTURE_SENSOR_load_sensor_name_into_guivar,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_load_sensor_name_into_guivar
	.type	MOISTURE_SENSOR_load_sensor_name_into_guivar, %function
MOISTURE_SENSOR_load_sensor_name_into_guivar:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L14
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r2, .L14+4
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L14+8
	bl	xQueueTakeMutexRecursive_debug
	bl	MOISTURE_SENSOR_get_list_count
	cmp	r4, r0
	bcs	.L12
	ldr	r3, .L14+12
	ldr	r0, [r3, r4, asl #2]
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L14+16
	bl	strlcpy
	b	.L13
.L12:
	ldr	r0, .L14+4
	ldr	r1, .L14+20
	bl	Alert_group_not_found_with_filename
.L13:
	ldr	r3, .L14
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L15:
	.align	2
.L14:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC0
	.word	591
	.word	.LANCHOR0
	.word	GuiVar_itmGroupName
	.word	599
.LFE4:
	.size	MOISTURE_SENSOR_load_sensor_name_into_guivar, .-MOISTURE_SENSOR_load_sensor_name_into_guivar
	.section	.text.MOISTURE_SENSOR_populate_pointers_of_physically_available_sensors_for_display,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_populate_pointers_of_physically_available_sensors_for_display
	.type	MOISTURE_SENSOR_populate_pointers_of_physically_available_sensors_for_display, %function
MOISTURE_SENSOR_populate_pointers_of_physically_available_sensors_for_display:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI2:
	ldr	r0, .L20
	mov	r1, #0
	mov	r2, #256
	bl	memset
	ldr	r3, .L20+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L20+8
	ldr	r3, .L20+12
	bl	xQueueTakeMutexRecursive_debug
	mov	r4, #0
	mov	r5, r4
	ldr	r6, .L20
	b	.L17
.L19:
	mov	r0, r4
	bl	MOISTURE_SENSOR_get_group_at_this_index
	subs	r7, r0, #0
	beq	.L18
	bl	MOISTURE_SENSOR_get_physically_available
	cmp	r0, #1
	streq	r7, [r6, r5, asl #2]
	addeq	r5, r5, #1
.L18:
	add	r4, r4, #1
.L17:
	bl	MOISTURE_SENSOR_get_list_count
	cmp	r4, r0
	bcc	.L19
	ldr	r3, .L20+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L21:
	.align	2
.L20:
	.word	.LANCHOR0
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC0
	.word	567
.LFE3:
	.size	MOISTURE_SENSOR_populate_pointers_of_physically_available_sensors_for_display, .-MOISTURE_SENSOR_populate_pointers_of_physically_available_sensors_for_display
	.section	.text.FDTO_MOISTURE_SENSOR_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_MOISTURE_SENSOR_draw_menu
	.type	FDTO_MOISTURE_SENSOR_draw_menu, %function
FDTO_MOISTURE_SENSOR_draw_menu:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L27
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI3:
	ldr	r2, .L27+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #668
	bl	xQueueTakeMutexRecursive_debug
	bl	MOISTURE_SENSOR_populate_pointers_of_physically_available_sensors_for_display
	cmp	r4, #1
	ldr	r6, .L27+8
	mov	r5, r0
	bne	.L23
	ldr	r2, .L27+12
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L27+16
	str	r3, [r6, #0]
	str	r3, [r2, #0]
.LBB12:
	ldr	r3, .L27+20
.LBE12:
	mvn	r4, #0
	ldr	r0, [r3, #0]
	bl	MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct
	ldr	r3, .L27+24
	str	r0, [r3, #0]
	bl	MOISTURE_SENSOR_copy_group_into_guivars
	b	.L24
.L23:
	ldr	r3, .L27+28
	mov	r0, #0
	ldrsh	r4, [r3, #0]
	bl	GuiLib_ScrollBox_GetTopLine
	str	r0, [r6, #0]
.L24:
	mov	r1, r4
	mov	r2, #1
	mov	r0, #44
	bl	GuiLib_ShowScreen
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	bl	MOISTURE_SENSOR_get_menu_index_for_displayed_sensor
	ldr	r3, .L27+12
	mov	r2, r5, asl #16
	ldr	r3, [r3, #0]
	ldr	r1, .L27+8
	cmp	r3, #1
	mov	r2, r2, asr #16
	mov	r4, r0
	bne	.L25
	ldrsh	r3, [r1, #0]
	mov	r0, #0
	str	r3, [sp, #0]
	ldr	r1, .L27+32
	mvn	r3, #0
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	mov	r1, r4, asl #16
	mov	r1, r1, asr #16
	mov	r0, #0
	bl	GuiLib_ScrollBox_SetIndicator
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
	b	.L26
.L25:
	ldrsh	r1, [r1, #0]
	mov	r3, r0, asl #16
	str	r1, [sp, #0]
	mov	r0, #0
	ldr	r1, .L27+32
	mov	r3, r3, asr #16
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L26:
	ldr	r3, .L27
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_Refresh
.L28:
	.align	2
.L27:
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR3
	.word	.LANCHOR2
	.word	.LANCHOR1
	.word	.LANCHOR0
	.word	g_GROUP_list_item_index
	.word	GuiLib_ActiveCursorFieldNo
	.word	MOISTURE_SENSOR_load_sensor_name_into_guivar
.LFE8:
	.size	FDTO_MOISTURE_SENSOR_draw_menu, .-FDTO_MOISTURE_SENSOR_draw_menu
	.section	.text.MOISTURE_SENSOR_get_ptr_to_physically_available_sensor,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_get_ptr_to_physically_available_sensor
	.type	MOISTURE_SENSOR_get_ptr_to_physically_available_sensor, %function
MOISTURE_SENSOR_get_ptr_to_physically_available_sensor:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L30
	ldr	r0, [r3, r0, asl #2]
	bx	lr
.L31:
	.align	2
.L30:
	.word	.LANCHOR0
.LFE5:
	.size	MOISTURE_SENSOR_get_ptr_to_physically_available_sensor, .-MOISTURE_SENSOR_get_ptr_to_physically_available_sensor
	.section	.text.MOISTURE_SENSOR_update_measurements,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_update_measurements
	.type	MOISTURE_SENSOR_update_measurements, %function
MOISTURE_SENSOR_update_measurements:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L33
	ldr	r0, [r3, #0]
	b	MOISTURE_SENSOR_copy_latest_readings_into_guivars
.L34:
	.align	2
.L33:
	.word	g_GROUP_list_item_index
.LFE7:
	.size	MOISTURE_SENSOR_update_measurements, .-MOISTURE_SENSOR_update_measurements
	.section	.text.MOISTURE_SENSOR_process_menu,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_process_menu
	.type	MOISTURE_SENSOR_process_menu, %function
MOISTURE_SENSOR_process_menu:
.LFB9:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI4:
	ldr	r6, .L152
	sub	sp, sp, #44
.LCFI5:
	ldr	r3, [r6, #0]
	mov	r4, r0
	cmp	r3, #1
	mov	r5, r1
	bne	.L36
.LBB19:
.LBB20:
	ldr	r2, .L152+4
	ldr	ip, .L152+8
	ldrsh	r2, [r2, #0]
	cmp	r2, ip
	beq	.L38
	cmp	r2, #616
	bne	.L121
	b	.L141
.L38:
	cmp	r0, #67
	beq	.L40
	cmp	r0, #2
	bne	.L41
	ldr	r3, .L152+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L41
.L40:
	bl	MOISTURE_SENSOR_extract_and_store_group_name_from_GuiVars
.L41:
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L152+16
	bl	KEYBOARD_process_key
	b	.L35
.L141:
	ldr	r2, .L152+16
	bl	NUMERIC_KEYPAD_process_key
	b	.L35
.L121:
	cmp	r0, #4
	beq	.L48
	bhi	.L52
	cmp	r0, #1
	beq	.L45
	bcc	.L44
	cmp	r0, #2
	beq	.L46
	cmp	r0, #3
	bne	.L112
	b	.L142
.L52:
	cmp	r0, #67
	beq	.L50
	bhi	.L53
	cmp	r0, #16
	beq	.L49
	cmp	r0, #20
	bne	.L112
	b	.L49
.L53:
	cmp	r0, #80
	beq	.L51
	cmp	r0, #84
	bne	.L112
	b	.L51
.L46:
	ldr	r3, .L152+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #32
	beq	.L58
	bgt	.L63
	cmp	r3, #1
	beq	.L56
	cmp	r3, #31
	beq	.L57
	cmp	r3, #0
	beq	.L55
	b	.L66
.L63:
	cmp	r3, #43
	beq	.L60
	bgt	.L64
	cmp	r3, #41
	bne	.L66
	b	.L143
.L64:
	cmp	r3, #51
	beq	.L61
	cmp	r3, #53
	bne	.L66
	b	.L144
.L55:
	mov	r0, #163
	mov	r1, #27
	bl	GROUP_process_show_keyboard
	b	.L79
.L56:
	str	r3, [sp, #12]
	str	r3, [sp, #32]
	mov	r3, #20
	str	r3, [sp, #40]
	ldr	r3, .L152+20
	mov	r2, #2
	str	r2, [sp, #8]
	mov	r2, #55
	str	r2, [sp, #16]
	str	r3, [sp, #24]
	ldr	r2, .L152+24
	ldr	r3, .L152+28
	add	r0, sp, #8
	str	r2, [sp, #28]
	str	r3, [sp, #20]
	bl	Change_Screen
	b	.L79
.L58:
	bl	good_key_beep
	ldr	r0, .L152+32
	mov	r1, #0
	mov	r2, #1065353216
	mov	r3, r4
	b	.L125
.L57:
	bl	good_key_beep
	ldr	r0, .L152+36
	mov	r1, #0
	mov	r2, #1065353216
.L134:
	mov	r3, #2
.L125:
	bl	NUMERIC_KEYPAD_draw_float_keypad
	b	.L79
.L143:
	bl	good_key_beep
	ldr	r0, .L152+40
	b	.L126
.L60:
	bl	good_key_beep
	ldr	r0, .L152+44
.L126:
	mov	r1, #0
	mov	r2, #72
	bl	NUMERIC_KEYPAD_draw_int32_keypad
	b	.L79
.L61:
	bl	good_key_beep
	ldr	r0, .L152+48
	b	.L139
.L144:
	bl	good_key_beep
	ldr	r0, .L152+52
.L139:
	mov	r1, #0
	ldr	r2, .L152+56
	b	.L134
.L51:
	ldr	r3, .L152+12
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #30
	cmp	r3, #23
	ldrls	pc, [pc, r3, asl #2]
	b	.L66
.L78:
	.word	.L67
	.word	.L68
	.word	.L69
	.word	.L66
	.word	.L66
	.word	.L66
	.word	.L66
	.word	.L66
	.word	.L66
	.word	.L66
	.word	.L70
	.word	.L71
	.word	.L72
	.word	.L73
	.word	.L66
	.word	.L66
	.word	.L66
	.word	.L66
	.word	.L66
	.word	.L66
	.word	.L74
	.word	.L75
	.word	.L76
	.word	.L77
.L67:
	mov	r3, #1
	mov	r2, #2
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L152+60
	mov	r3, r2
	bl	process_uns32
	mov	r0, #0
	bl	Redraw_Screen
	b	.L79
.L69:
	ldr	r3, .L152+64
	mov	r0, r4
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r1, .L152+32
	b	.L127
.L68:
	ldr	r3, .L152+64
	ldr	r1, .L152+36
	str	r3, [sp, #0]	@ float
	mov	r0, r4
	mov	r3, #0
	str	r3, [sp, #4]
.L127:
	mov	r2, #0
	mov	r3, #1065353216
.L128:
	bl	process_fl
	b	.L79
.L70:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L152+68
	b	.L129
.L71:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L152+40
	b	.L130
.L72:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L152+72
	b	.L129
.L73:
	ldr	r1, .L152+44
	mov	r3, #1
	mov	r2, #0
	mov	r0, r4
	str	r3, [sp, #0]
	str	r2, [sp, #4]
.L130:
	mov	r3, #72
	bl	process_int32
	b	.L79
.L74:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L152+76
	b	.L129
.L75:
	ldr	r3, .L152+64
	mov	r0, r4
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r1, .L152+48
	b	.L135
.L76:
	ldr	r1, .L152+80
	mov	r3, #1
	mov	r0, r4
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L129:
	mov	r2, #0
	bl	process_uns32
	b	.L79
.L77:
	ldr	r3, .L152+64
	ldr	r1, .L152+52
	str	r3, [sp, #0]	@ float
	mov	r0, r4
	mov	r3, #0
	str	r3, [sp, #4]
.L135:
	mov	r2, #0
	ldr	r3, .L152+56
	b	.L128
.L66:
	bl	bad_key_beep
.L79:
	bl	Refresh_Screen
	b	.L35
.L49:
.LBB21:
	bl	MOISTURE_SENSOR_get_menu_index_for_displayed_sensor
	cmp	r4, #20
	mov	r5, r0
	mov	r4, r5, asl #16
	mov	r0, #0
	mov	r4, r4, asr #16
	bne	.L80
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	mov	r1, r4
	bl	SCROLL_BOX_to_line
	cmp	r5, #63
	bhi	.L84
	ldr	r3, .L152+84
	add	r5, r5, #1
	ldr	r3, [r3, r5, asl #2]
	cmp	r3, #0
	movne	r0, #0
	movne	r1, r0
	bne	.L136
	b	.L85
.L80:
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	mov	r1, r4
	bl	SCROLL_BOX_to_line
	cmp	r5, #0
	beq	.L84
	ldr	r3, .L152+84
	sub	r5, r5, #1
	ldr	r3, [r3, r5, asl #2]
	cmp	r3, #0
	beq	.L85
	mov	r0, #0
	mov	r1, #4
.L136:
	bl	SCROLL_BOX_up_or_down
	b	.L85
.L84:
	bl	bad_key_beep
.L85:
	ldr	r4, .L152+84
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	ldr	r3, [r4, r5, asl #2]
	cmp	r3, #0
	beq	.L86
	bl	MOISTURE_SENSOR_extract_and_store_changes_from_GuiVars
	ldr	r0, [r4, r5, asl #2]
	bl	MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct
	ldr	r5, .L152+88
	ldr	r4, .L152+92
	mov	r1, #400
	ldr	r2, .L152+96
	mov	r3, #225
	str	r0, [r5, #0]
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, #0]
	bl	MOISTURE_SENSOR_copy_group_into_guivars
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	b	.L131
.L86:
	bl	bad_key_beep
	b	.L131
.L48:
.LBE21:
	ldr	r2, .L152+12
	ldrsh	r2, [r2, #0]
	cmp	r2, #30
	beq	.L90
	bgt	.L93
	sub	r2, r2, #3
	cmp	r2, #2
	bhi	.L102
	b	.L145
.L93:
	cmp	r2, #40
	beq	.L137
	cmp	r2, #50
	bne	.L102
	b	.L146
.L145:
	mov	r0, r3
	mov	r1, r3
	b	.L132
.L90:
	mov	r0, #3
.L137:
	mov	r1, r3
.L132:
	bl	CURSOR_Select
	b	.L35
.L146:
	mov	r0, #5
	b	.L137
.L44:
	ldr	r3, .L152+12
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L123
.L99:
	.word	.L95
	.word	.L123
	.word	.L96
	.word	.L97
	.word	.L98
.L95:
	ldr	r3, .L152+100
	ldr	r1, [r3, #0]
	cmp	r1, #0
	moveq	r0, #3
	beq	.L138
	cmp	r1, #1
	moveq	r0, #4
	beq	.L132
	cmp	r1, #2
	moveq	r0, #5
	bne	.L35
	b	.L138
.L96:
	mov	r0, #30
	b	.L138
.L97:
	mov	r0, #40
	b	.L138
.L98:
	mov	r0, #50
	b	.L138
.L45:
	ldr	r3, .L152+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #5
	beq	.L105
	bgt	.L108
	cmp	r3, #0
	beq	.L50
	cmp	r3, #4
	bne	.L102
	b	.L147
.L108:
	cmp	r3, #30
	beq	.L106
	cmp	r3, #40
	bne	.L102
	b	.L148
.L147:
	mov	r0, #3
	mov	r1, #1
	bl	CURSOR_Select
	ldr	r3, .L152+100
	mov	r0, #0
	str	r0, [r3, #0]
	b	.L133
.L105:
	mov	r0, #4
	mov	r1, r4
	bl	CURSOR_Select
	ldr	r3, .L152+100
	str	r4, [r3, #0]
	b	.L131
.L106:
	mov	r0, #3
	mov	r1, r4
	b	.L132
.L148:
	mov	r0, #4
.L138:
	mov	r1, #1
	b	.L132
.L102:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L35
.L142:
	ldr	r3, .L152+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #3
	beq	.L110
	cmp	r3, #4
	bne	.L123
	b	.L149
.L110:
	mov	r0, #4
	mov	r1, #1
	bl	CURSOR_Select
	mov	r2, #1
	b	.L140
.L149:
	mov	r0, #5
	mov	r1, #1
	bl	CURSOR_Select
	mov	r2, #2
.L140:
	ldr	r3, .L152+100
	str	r2, [r3, #0]
	b	.L131
.L123:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L35
.L50:
	ldr	r0, .L152+104
	bl	KEY_process_BACK_from_editing_screen
	b	.L35
.L36:
.LBE20:
.LBE19:
	cmp	r0, #4
	beq	.L118
	bhi	.L117
	cmp	r0, #0
	beq	.L118
	cmp	r0, #2
	bcc	.L112
	b	.L150
.L117:
	cmp	r0, #20
	beq	.L120
	cmp	r0, #67
	beq	.L116
	cmp	r0, #16
	bne	.L112
	b	.L151
.L150:
	bl	good_key_beep
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L152+108
	str	r0, [r3, #0]
.LBB22:
	ldr	r3, .L152+84
.LBE22:
	ldr	r0, [r3, r0, asl #2]
	bl	MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct
	ldr	r3, .L152+88
	str	r0, [r3, #0]
	mov	r3, #1
	str	r3, [r6, #0]
	ldr	r3, .L152+12
	mov	r0, #0
	strh	r0, [r3, #0]	@ movhi
	b	.L133
.L151:
	mov	r1, #4
	b	.L113
.L120:
	mov	r1, #0
	b	.L113
.L118:
	mov	r1, r4
.L113:
	mov	r0, #0
	bl	SCROLL_BOX_up_or_down
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L152+108
	str	r0, [r3, #0]
.LBB23:
	ldr	r3, .L152+84
.LBE23:
	ldr	r0, [r3, r0, asl #2]
	bl	MOISTURE_SENSOR_get_index_using_ptr_to_mois_struct
	ldr	r3, .L152+88
	str	r0, [r3, #0]
	bl	MOISTURE_SENSOR_copy_group_into_guivars
.L131:
	mov	r0, #0
.L133:
	bl	Redraw_Screen
	b	.L35
.L116:
	ldr	r3, .L152+112
	ldr	r2, .L152+116
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r4, .L152+120
	ldr	r3, [r3, #4]
	str	r3, [r4, #0]
	bl	KEY_process_global_keys
	ldr	r3, [r4, #0]
	cmp	r3, #11
	bne	.L35
	mov	r0, #1
	bl	TWO_WIRE_ASSIGNMENT_draw_dialog
	b	.L35
.L112:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L35:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, pc}
.L153:
	.align	2
.L152:
	.word	.LANCHOR2
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_MOISTURE_SENSOR_draw_menu
	.word	STATION_GROUP_process_menu
	.word	FDTO_STATION_GROUP_draw_menu
	.word	nm_STATION_GROUP_load_group_name_into_guivar
	.word	GuiVar_MoisSetPoint_Low
	.word	GuiVar_MoisSetPoint_High
	.word	GuiVar_MoisTempPoint_High
	.word	GuiVar_MoisTempPoint_Low
	.word	GuiVar_MoisECPoint_High
	.word	GuiVar_MoisECPoint_Low
	.word	1092616192
	.word	GuiVar_MoisControlMode
	.word	1008981770
	.word	GuiVar_MoisTempAction_High
	.word	GuiVar_MoisTempAction_Low
	.word	GuiVar_MoisECAction_High
	.word	GuiVar_MoisECAction_Low
	.word	.LANCHOR0
	.word	g_GROUP_list_item_index
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_MoistureTabToDisplay
	.word	FDTO_MOISTURE_SENSOR_return_to_menu
	.word	.LANCHOR1
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE9:
	.size	MOISTURE_SENSOR_process_menu, .-MOISTURE_SENSOR_process_menu
	.section	.bss.g_MOISTURE_SENSOR_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_MOISTURE_SENSOR_editing_group, %object
	.size	g_MOISTURE_SENSOR_editing_group, 4
g_MOISTURE_SENSOR_editing_group:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_moisture_sensors.c\000"
	.section	.bss.g_MOISTURE_SENSOR_top_line,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_MOISTURE_SENSOR_top_line, %object
	.size	g_MOISTURE_SENSOR_top_line, 4
g_MOISTURE_SENSOR_top_line:
	.space	4
	.section	.bss.MOISTURE_SENSOR_menu_items,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	MOISTURE_SENSOR_menu_items, %object
	.size	MOISTURE_SENSOR_menu_items, 256
MOISTURE_SENSOR_menu_items:
	.space	256
	.section	.bss.g_MOISTURE_SENSOR_current_list_item_index,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_MOISTURE_SENSOR_current_list_item_index, %object
	.size	g_MOISTURE_SENSOR_current_list_item_index, 4
g_MOISTURE_SENSOR_current_list_item_index:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI3-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI4-.LFB9
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_moisture_sensors.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xd7
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF9
	.byte	0x1
	.4byte	.LASF10
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x25e
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.byte	0x6f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x274
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x24d
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x227
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x292
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST3
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x27a
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x102
	.byte	0x1
	.uleb128 0x9
	.4byte	.LASF7
	.byte	0x1
	.byte	0x93
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x2e2
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST4
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB8
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB9
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI5
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"MOISTURE_SENSOR_populate_pointers_of_physically_ava"
	.ascii	"ilable_sensors_for_display\000"
.LASF5:
	.ascii	"MOISTURE_SENSOR_update_measurements\000"
.LASF4:
	.ascii	"FDTO_MOISTURE_SENSOR_draw_menu\000"
.LASF11:
	.ascii	"MOISTURE_SENSOR_get_ptr_to_physically_available_sen"
	.ascii	"sor\000"
.LASF10:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_moisture_sensors.c\000"
.LASF9:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF8:
	.ascii	"MOISTURE_SENSOR_process_menu\000"
.LASF7:
	.ascii	"MOISTURE_SENSOR_process_NEXT_and_PREV\000"
.LASF0:
	.ascii	"MOISTURE_SENSOR_get_menu_index_for_displayed_sensor"
	.ascii	"\000"
.LASF6:
	.ascii	"MOISTURE_SENSOR_process_group\000"
.LASF1:
	.ascii	"FDTO_MOISTURE_SENSOR_return_to_menu\000"
.LASF2:
	.ascii	"MOISTURE_SENSOR_load_sensor_name_into_guivar\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
