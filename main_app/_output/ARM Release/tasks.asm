	.file	"tasks.c"
	.text
.Ltext0:
	.section	.text.prvAddCurrentTaskToDelayedList,"ax",%progbits
	.align	2
	.type	prvAddCurrentTaskToDelayedList, %function
prvAddCurrentTaskToDelayedList:
.LFB34:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L4
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r2, [r3, #0]
	mov	r4, r0
	str	r0, [r2, #4]
	ldr	r2, .L4+4
	ldr	r2, [r2, #0]
	cmp	r0, r2
	bcs	.L2
.LBB22:
	ldr	r2, .L4+8
	ldr	r0, [r2, #0]
	ldr	r1, [r3, #0]
	add	r1, r1, #4
.LBE22:
	ldmfd	sp!, {r4, lr}
.LBB23:
	b	vListInsert
.L2:
.LBE23:
	ldr	r2, .L4+12
	ldr	r0, [r2, #0]
	ldr	r1, [r3, #0]
	add	r1, r1, #4
	bl	vListInsert
	ldr	r3, .L4+16
	ldr	r2, [r3, #0]
	cmp	r4, r2
	strcc	r4, [r3, #0]
	ldmfd	sp!, {r4, pc}
.L5:
	.align	2
.L4:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LANCHOR4
.LFE34:
	.size	prvAddCurrentTaskToDelayedList, .-prvAddCurrentTaskToDelayedList
	.section	.text.xTaskGenericCreate,"ax",%progbits
	.align	2
	.global	xTaskGenericCreate
	.type	xTaskGenericCreate, %function
xTaskGenericCreate:
.LFB0:
	@ args = 16, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI1:
	ldr	r6, [sp, #40]
	mov	r2, r2, asl #16
	subs	sl, r0, #0
	mov	r9, r1
	mov	r8, r3
	ldr	fp, [sp, #44]
	ldr	r5, [sp, #48]
	mov	r7, r2, lsr #16
	bne	.L7
	ldr	r0, .L23
	ldr	r1, .L23+4
	mov	r2, #404
	bl	__assert
.L7:
	cmp	r6, #15
	bls	.L8
	ldr	r0, .L23+8
	ldr	r1, .L23+4
	ldr	r2, .L23+12
	bl	__assert
.L8:
.LBB24:
	mov	r0, #96
	bl	pvPortMalloc
	subs	r4, r0, #0
	beq	.L19
	cmp	r5, #0
	bne	.L10
	mov	r0, r7, asl #2
	bl	pvPortMalloc
	mov	r5, r0
.L10:
	cmp	r5, #0
	str	r5, [r4, #48]
	bne	.L11
	mov	r0, r4
	bl	vPortFree
	b	.L19
.L11:
	mov	r2, r7, asl #2
	mov	r0, r5
	mov	r1, #165
	bl	memset
.LBE24:
.LBB25:
	ldr	r3, [r4, #48]
.LBB26:
	mov	r1, r9
	mov	r2, #32
.LBE26:
	sub	r7, r7, #1
.LBB27:
	add	r0, r4, #52
.LBE27:
	add	r7, r3, r7, asl #2
.LBB28:
	mov	r9, #0
	bl	strncpy
	add	r5, r4, #4
	cmp	r6, #15
	movcc	r3, r6
	movcs	r3, #15
	str	r3, [r4, #44]
	str	r3, [r4, #84]
	strb	r9, [r4, #83]
	mov	r0, r5
	str	r3, [sp, #0]
	bl	vListInitialiseItem
	add	r0, r4, #24
	bl	vListInitialiseItem
	ldr	r3, [sp, #0]
.LBE28:
	bic	r7, r7, #7
.LBB29:
	rsb	r3, r3, #16
	str	r4, [r4, #16]
	str	r3, [r4, #24]
	str	r4, [r4, #36]
	str	r9, [r4, #88]
	str	r9, [r4, #92]
.LBE29:
	mov	r0, r7
	mov	r1, sl
	mov	r2, r8
	bl	pxPortInitialiseStack
	tst	r0, #7
	str	r0, [r4, #0]
	beq	.L12
	ldr	r0, .L23+16
	ldr	r1, .L23+4
	ldr	r2, .L23+20
	bl	__assert
.L12:
	cmp	fp, #0
	strne	r4, [fp, #0]
	bl	vPortEnterCritical
	ldr	r2, .L23+24
	ldr	r3, [r2, #0]
	add	r3, r3, #1
	str	r3, [r2, #0]
	ldr	r3, .L23+28
	ldr	r7, [r3, #0]
	cmp	r7, #0
	bne	.L14
	str	r4, [r3, #0]
	ldr	r3, [r2, #0]
	cmp	r3, #1
	bne	.L15
.LBB30:
	ldr	sl, .L23+32
	mov	r8, #20
.L16:
	mla	r0, r8, r7, sl
	bl	vListInitialise
	add	r7, r7, #1
	cmp	r7, #16
	bne	.L16
	ldr	r8, .L23+36
	ldr	r7, .L23+40
	mov	r0, r8
	bl	vListInitialise
	mov	r0, r7
	bl	vListInitialise
	ldr	r0, .L23+44
	bl	vListInitialise
	ldr	r0, .L23+48
	bl	vListInitialise
	ldr	r0, .L23+52
	bl	vListInitialise
	ldr	r3, .L23+56
	str	r8, [r3, #0]
	ldr	r3, .L23+60
	str	r7, [r3, #0]
	b	.L15
.L14:
.LBE30:
	ldr	r2, .L23+64
	ldr	r2, [r2, #0]
	cmp	r2, #0
	bne	.L15
	ldr	r2, [r3, #0]
	ldr	r2, [r2, #44]
	cmp	r2, r6
	strls	r4, [r3, #0]
.L15:
	ldr	r2, .L23+68
	ldr	r3, [r4, #44]
	ldr	r1, [r2, #0]
	mov	r0, #20
	cmp	r3, r1
	strhi	r3, [r2, #0]
	ldr	r2, .L23+72
	ldr	r1, [r2, #0]
	add	r1, r1, #1
	str	r1, [r2, #0]
	ldr	r2, .L23+76
	ldr	r1, [r2, #0]
	cmp	r3, r1
	strhi	r3, [r2, #0]
	ldr	r2, .L23+32
	mov	r1, r5
	mla	r0, r3, r0, r2
	bl	vListInsertEnd
	bl	vPortExitCritical
.LBE25:
	ldr	r3, .L23+64
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L21
	ldr	r3, .L23+28
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #44]
	cmp	r3, r6
	bcs	.L21
@ 553 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
	b	.L21
.L19:
.LBB31:
	mvn	r0, #0
	b	.L9
.L21:
.LBE31:
	mov	r0, #1
.L9:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L24:
	.align	2
.L23:
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	405
	.word	.LC3
	.word	473
	.word	.LANCHOR5
	.word	.LANCHOR0
	.word	.LANCHOR6
	.word	.LANCHOR7
	.word	.LANCHOR8
	.word	.LANCHOR9
	.word	.LANCHOR10
	.word	.LANCHOR11
	.word	.LANCHOR3
	.word	.LANCHOR2
	.word	.LANCHOR12
	.word	.LANCHOR13
	.word	.LANCHOR14
	.word	.LANCHOR15
.LFE0:
	.size	xTaskGenericCreate, .-xTaskGenericCreate
	.section	.text.vTaskDelete,"ax",%progbits
	.align	2
	.global	vTaskDelete
	.type	vTaskDelete, %function
vTaskDelete:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI2:
	mov	r4, r0
	bl	vPortEnterCritical
	ldr	r3, .L31
	ldr	r2, [r3, #0]
	cmp	r4, r2
	beq	.L26
	cmp	r4, #0
	movne	r5, r4
	bne	.L27
.L26:
	ldr	r5, [r3, #0]
	mov	r4, #0
.L27:
	add	r6, r5, #4
	mov	r0, r6
	bl	vListRemove
	ldr	r3, [r5, #40]
	cmp	r3, #0
	beq	.L28
	add	r0, r5, #24
	bl	vListRemove
.L28:
	ldr	r0, .L31+4
	mov	r1, r6
	bl	vListInsertEnd
	ldr	r3, .L31+8
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L31+12
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	bl	vPortExitCritical
	ldr	r3, .L31+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	cmp	r4, #0
	ldmnefd	sp!, {r4, r5, r6, pc}
@ 612 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
	ldmfd	sp!, {r4, r5, r6, pc}
.L32:
	.align	2
.L31:
	.word	.LANCHOR0
	.word	.LANCHOR10
	.word	.LANCHOR16
	.word	.LANCHOR14
	.word	.LANCHOR12
.LFE1:
	.size	vTaskDelete, .-vTaskDelete
	.section	.text.uxTaskPriorityGet,"ax",%progbits
	.align	2
	.global	uxTaskPriorityGet
	.type	uxTaskPriorityGet, %function
uxTaskPriorityGet:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI3:
	mov	r4, r0
	bl	vPortEnterCritical
	cmp	r4, #0
	ldreq	r3, .L36
	ldreq	r4, [r3, #0]
	ldr	r4, [r4, #44]
	bl	vPortExitCritical
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L37:
	.align	2
.L36:
	.word	.LANCHOR0
.LFE4:
	.size	uxTaskPriorityGet, .-uxTaskPriorityGet
	.section	.text.vTaskPrioritySet,"ax",%progbits
	.align	2
	.global	vTaskPrioritySet
	.type	vTaskPrioritySet, %function
vTaskPrioritySet:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #15
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI4:
	mov	r4, r0
	mov	r6, r1
	bls	.L39
	ldr	r0, .L49
	ldr	r1, .L49+4
	ldr	r2, .L49+8
	bl	__assert
	mov	r6, #15
.L39:
	bl	vPortEnterCritical
	ldr	r3, .L49+12
	ldr	r2, [r3, #0]
	cmp	r4, r2
	beq	.L40
	cmp	r4, #0
	movne	r5, r4
	bne	.L41
.L40:
	ldr	r5, [r3, #0]
	mov	r4, #0
.L41:
	ldr	r3, [r5, #84]
	cmp	r3, r6
	beq	.L42
	cmp	r6, r3
	bls	.L43
	adds	r4, r4, #0
	movne	r4, #1
	b	.L44
.L43:
	rsbs	r4, r4, #1
	movcc	r4, #0
.L44:
	ldr	r2, [r5, #44]
	mov	r1, #20
	cmp	r3, r2
	ldr	r2, .L49+16
	streq	r6, [r5, #44]
	mla	r3, r1, r3, r2
	ldr	r2, [r5, #20]
	str	r6, [r5, #84]
	cmp	r2, r3
	rsb	r6, r6, #16
	str	r6, [r5, #24]
	bne	.L46
	add	r6, r5, #4
	mov	r0, r6
	bl	vListRemove
	ldr	r2, .L49+20
	ldr	r3, [r5, #44]
	ldr	r1, [r2, #0]
	mov	r0, #20
	cmp	r3, r1
	strhi	r3, [r2, #0]
	ldr	r2, .L49+16
	mov	r1, r6
	mla	r0, r3, r0, r2
	bl	vListInsertEnd
.L46:
	cmp	r4, #1
	bne	.L42
@ 859 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
.L42:
	ldmfd	sp!, {r4, r5, r6, lr}
	b	vPortExitCritical
.L50:
	.align	2
.L49:
	.word	.LC4
	.word	.LC1
	.word	769
	.word	.LANCHOR0
	.word	.LANCHOR6
	.word	.LANCHOR15
.LFE5:
	.size	vTaskPrioritySet, .-vTaskPrioritySet
	.section	.text.xTaskIsTaskSuspended,"ax",%progbits
	.align	2
	.global	xTaskIsTaskSuspended
	.type	xTaskIsTaskSuspended, %function
xTaskIsTaskSuspended:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI5:
	subs	r4, r0, #0
	bne	.L52
	ldr	r0, .L57
	ldr	r1, .L57+4
	ldr	r2, .L57+8
	bl	__assert
.L52:
	ldr	r2, [r4, #20]
	ldr	r3, .L57+12
	cmp	r2, r3
	bne	.L55
.LBB34:
	ldr	r0, [r4, #40]
	ldr	r3, .L57+16
	cmp	r0, r3
	moveq	r0, #0
	ldmeqfd	sp!, {r4, pc}
	rsbs	r0, r0, #1
	movcc	r0, #0
	ldmfd	sp!, {r4, pc}
.L55:
.LBE34:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L58:
	.align	2
.L57:
	.word	.LC5
	.word	.LC1
	.word	941
	.word	.LANCHOR11
	.word	.LANCHOR9
.LFE7:
	.size	xTaskIsTaskSuspended, .-xTaskIsTaskSuspended
	.section	.text.vTaskResume,"ax",%progbits
	.align	2
	.global	vTaskResume
	.type	vTaskResume, %function
vTaskResume:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI6:
	subs	r4, r0, #0
	bne	.L60
	ldr	r0, .L64
	ldr	r1, .L64+4
	ldr	r2, .L64+8
	ldmfd	sp!, {r4, r5, lr}
	b	__assert
.L60:
	ldr	r3, .L64+12
	ldr	r3, [r3, #0]
	cmp	r4, r3
	ldmeqfd	sp!, {r4, r5, pc}
	bl	vPortEnterCritical
	mov	r0, r4
	bl	xTaskIsTaskSuspended
	cmp	r0, #1
	bne	.L62
	add	r5, r4, #4
	mov	r0, r5
	bl	vListRemove
	ldr	r2, .L64+16
	ldr	r3, [r4, #44]
	ldr	r1, [r2, #0]
	mov	r0, #20
	cmp	r3, r1
	strhi	r3, [r2, #0]
	ldr	r2, .L64+20
	mov	r1, r5
	mla	r0, r3, r0, r2
	bl	vListInsertEnd
	ldr	r3, .L64+12
	ldr	r2, [r4, #44]
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #44]
	cmp	r2, r3
	bcc	.L62
@ 1000 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
.L62:
	ldmfd	sp!, {r4, r5, lr}
	b	vPortExitCritical
.L65:
	.align	2
.L64:
	.word	.LC6
	.word	.LC1
	.word	974
	.word	.LANCHOR0
	.word	.LANCHOR15
	.word	.LANCHOR6
.LFE8:
	.size	vTaskResume, .-vTaskResume
	.section	.text.xTaskResumeFromISR,"ax",%progbits
	.align	2
	.global	xTaskResumeFromISR
	.type	xTaskResumeFromISR, %function
xTaskResumeFromISR:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI7:
	subs	r4, r0, #0
	bne	.L67
	ldr	r0, .L72
	ldr	r1, .L72+4
	mov	r2, #1020
	bl	__assert
.L67:
	mov	r0, r4
	bl	xTaskIsTaskSuspended
	cmp	r0, #1
	bne	.L71
	ldr	r3, .L72+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L69
	ldr	r3, .L72+12
	ldr	r5, [r4, #44]
	ldr	r3, [r3, #0]
	add	r6, r4, #4
	ldr	r3, [r3, #44]
	mov	r0, r6
	cmp	r5, r3
	movcc	r5, #0
	movcs	r5, #1
	bl	vListRemove
	ldr	r2, .L72+16
	ldr	r3, [r4, #44]
	ldr	r1, [r2, #0]
	mov	r0, #20
	cmp	r3, r1
	strhi	r3, [r2, #0]
	ldr	r2, .L72+20
	mov	r1, r6
	mla	r0, r3, r0, r2
	bl	vListInsertEnd
	b	.L68
.L69:
	ldr	r0, .L72+24
	add	r1, r4, #24
	bl	vListInsertEnd
.L71:
	mov	r5, #0
.L68:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L73:
	.align	2
.L72:
	.word	.LC6
	.word	.LC1
	.word	.LANCHOR17
	.word	.LANCHOR0
	.word	.LANCHOR15
	.word	.LANCHOR6
	.word	.LANCHOR9
.LFE9:
	.size	xTaskResumeFromISR, .-xTaskResumeFromISR
	.section	.text.vTaskStartScheduler,"ax",%progbits
	.align	2
	.global	vTaskStartScheduler
	.type	vTaskStartScheduler, %function
vTaskStartScheduler:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI8:
	ldr	r0, .L77
	mov	r4, #0
	ldr	r1, .L77+4
	mov	r2, #256
	mov	r3, r4
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	str	r4, [sp, #8]
	str	r4, [sp, #12]
	bl	xTaskGenericCreate
	cmp	r0, #1
	bne	.L75
	bl	xTimerCreateTimerTask
	cmp	r0, #1
	bne	.L75
@ 1097 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	STMDB	SP!, {R0}		
	MRS	R0, CPSR		
	ORR	R0, R0, #0xC0	
	MSR	CPSR, R0		
	LDMIA	SP!, {R0}			
@ 0 "" 2
	ldr	r3, .L77+8
	str	r0, [r3, #0]
	ldr	r3, .L77+12
	str	r4, [r3, #0]
	bl	vConfigureTimerForRunTimeStats
	add	sp, sp, #16
	ldmfd	sp!, {r4, lr}
	b	xPortStartScheduler
.L75:
	cmp	r0, #0
	bne	.L74
	ldr	r0, .L77+16
	ldr	r1, .L77+20
	ldr	r2, .L77+24
	add	sp, sp, #16
	ldmfd	sp!, {r4, lr}
	b	__assert
.L74:
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L78:
	.align	2
.L77:
	.word	prvIdleTask
	.word	.LC7
	.word	.LANCHOR12
	.word	.LANCHOR1
	.word	.LC8
	.word	.LC1
	.word	1121
.LFE10:
	.size	vTaskStartScheduler, .-vTaskStartScheduler
	.section	.text.vTaskEndScheduler,"ax",%progbits
	.align	2
	.global	vTaskEndScheduler
	.type	vTaskEndScheduler, %function
vTaskEndScheduler:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
@ 1130 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	STMDB	SP!, {R0}		
	MRS	R0, CPSR		
	ORR	R0, R0, #0xC0	
	MSR	CPSR, R0		
	LDMIA	SP!, {R0}			
@ 0 "" 2
	ldr	r3, .L80
	mov	r2, #0
	str	r2, [r3, #0]
	b	vPortEndScheduler
.L81:
	.align	2
.L80:
	.word	.LANCHOR12
.LFE11:
	.size	vTaskEndScheduler, .-vTaskEndScheduler
	.section	.text.vTaskSuspendAll,"ax",%progbits
	.align	2
	.global	vTaskSuspendAll
	.type	vTaskSuspendAll, %function
vTaskSuspendAll:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L83
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	bx	lr
.L84:
	.align	2
.L83:
	.word	.LANCHOR17
.LFE12:
	.size	vTaskSuspendAll, .-vTaskSuspendAll
	.section	.text.xTaskGetTickCount,"ax",%progbits
	.align	2
	.global	xTaskGetTickCount
	.type	xTaskGetTickCount, %function
xTaskGetTickCount:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI9:
	bl	vPortEnterCritical
	ldr	r3, .L86
	ldr	r4, [r3, #0]
	bl	vPortExitCritical
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L87:
	.align	2
.L86:
	.word	.LANCHOR1
.LFE14:
	.size	xTaskGetTickCount, .-xTaskGetTickCount
	.section	.text.xTaskGetTickCountFromISR,"ax",%progbits
	.align	2
	.global	xTaskGetTickCountFromISR
	.type	xTaskGetTickCountFromISR, %function
xTaskGetTickCountFromISR:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L89
	ldr	r0, [r3, #0]
	bx	lr
.L90:
	.align	2
.L89:
	.word	.LANCHOR1
.LFE15:
	.size	xTaskGetTickCountFromISR, .-xTaskGetTickCountFromISR
	.section	.text.uxTaskGetNumberOfTasks,"ax",%progbits
	.align	2
	.global	uxTaskGetNumberOfTasks
	.type	uxTaskGetNumberOfTasks, %function
uxTaskGetNumberOfTasks:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L92
	ldr	r0, [r3, #0]
	bx	lr
.L93:
	.align	2
.L92:
	.word	.LANCHOR5
.LFE16:
	.size	uxTaskGetNumberOfTasks, .-uxTaskGetNumberOfTasks
	.section	.text.pcTaskGetTaskName,"ax",%progbits
	.align	2
	.global	pcTaskGetTaskName
	.type	pcTaskGetTaskName, %function
pcTaskGetTaskName:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI10:
	subs	r4, r0, #0
	bne	.L95
	ldr	r3, .L97
	ldr	r4, [r3, #0]
	cmp	r4, #0
	bne	.L95
	ldr	r0, .L97+4
	ldr	r1, .L97+8
	ldr	r2, .L97+12
	bl	__assert
.L95:
	add	r0, r4, #52
	ldmfd	sp!, {r4, pc}
.L98:
	.align	2
.L97:
	.word	.LANCHOR0
	.word	.LC9
	.word	.LC1
	.word	1275
.LFE17:
	.size	pcTaskGetTaskName, .-pcTaskGetTaskName
	.section	.text.vTaskIncrementTick,"ax",%progbits
	.align	2
	.global	vTaskIncrementTick
	.type	vTaskIncrementTick, %function
vTaskIncrementTick:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L113
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI11:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L100
	ldr	r3, .L113+4
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L101
.LBB35:
	ldr	r4, .L113+8
	ldr	r3, [r4, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L102
	ldr	r0, .L113+12
	ldr	r1, .L113+16
	ldr	r2, .L113+20
	bl	__assert
.L102:
	ldr	r3, .L113+24
	ldr	r2, [r4, #0]
	ldr	r1, [r3, #0]
	str	r1, [r4, #0]
	str	r2, [r3, #0]
	ldr	r3, .L113+28
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	ldr	r3, [r4, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldrne	r2, .L113+8
	ldr	r3, .L113+32
	ldrne	r2, [r2, #0]
	mvneq	r2, #0
	ldrne	r2, [r2, #12]
	ldrne	r2, [r2, #12]
	ldrne	r2, [r2, #4]
	str	r2, [r3, #0]
.L101:
.LBE35:
.LBB36:
	ldr	r6, .L113+4
	ldr	r3, .L113+32
	ldr	r2, [r6, #0]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	ldmccfd	sp!, {r4, r5, r6, r7, r8, pc}
	ldr	r5, .L113+8
	ldr	r4, .L113+36
.L110:
	ldr	r3, [r5, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	mvneq	r2, #0
	ldreq	r3, .L113+32
	beq	.L112
.L105:
	ldr	r3, [r5, #0]
	ldr	r3, [r3, #12]
	ldr	r7, [r3, #12]
	ldr	r2, [r6, #0]
	ldr	r3, [r7, #4]
	cmp	r2, r3
	bcs	.L106
	ldr	r2, .L113+32
	str	r3, [r2, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L106:
	add	r8, r7, #4
	mov	r0, r8
	bl	vListRemove
	ldr	r3, [r7, #40]
	cmp	r3, #0
	beq	.L107
	add	r0, r7, #24
	bl	vListRemove
.L107:
	ldr	r3, [r7, #44]
	ldr	r2, [r4, #0]
	mov	r0, #20
	cmp	r3, r2
	ldr	r2, .L113+40
	strhi	r3, [r4, #0]
	mla	r0, r3, r0, r2
	mov	r1, r8
	bl	vListInsertEnd
	b	.L110
.L100:
.LBE36:
	ldr	r3, .L113+44
	ldr	r2, [r3, #0]
	add	r2, r2, #1
.L112:
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L114:
	.align	2
.L113:
	.word	.LANCHOR17
	.word	.LANCHOR1
	.word	.LANCHOR3
	.word	.LC10
	.word	.LC1
	.word	1615
	.word	.LANCHOR2
	.word	.LANCHOR18
	.word	.LANCHOR4
	.word	.LANCHOR15
	.word	.LANCHOR6
	.word	.LANCHOR19
.LFE19:
	.size	vTaskIncrementTick, .-vTaskIncrementTick
	.section	.text.xTaskResumeAll,"ax",%progbits
	.align	2
	.global	xTaskResumeAll
	.type	xTaskResumeAll, %function
xTaskResumeAll:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI12:
	ldr	r4, .L134
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L116
	ldr	r0, .L134+4
	ldr	r1, .L134+8
	ldr	r2, .L134+12
	bl	__assert
.L116:
	bl	vPortEnterCritical
	ldr	r3, [r4, #0]
	sub	r3, r3, #1
	str	r3, [r4, #0]
	ldr	r5, [r4, #0]
	cmp	r5, #0
	movne	r4, #0
	bne	.L117
	ldr	r3, .L134+16
	ldr	r4, [r3, #0]
	cmp	r4, #0
	bne	.L127
	b	.L117
.L121:
.LBB37:
	ldr	r3, [r7, #12]
	ldr	r4, [r3, #12]
	add	r0, r4, #24
	add	sl, r4, #4
	bl	vListRemove
	mov	r0, sl
	bl	vListRemove
	ldr	r3, [r4, #44]
	ldr	r2, [r6, #0]
	mov	r0, #20
	cmp	r3, r2
	mla	r0, r3, r0, r8
	mov	r1, sl
	strhi	r3, [r6, #0]
	bl	vListInsertEnd
	ldr	r3, .L134+20
	ldr	r2, [r4, #44]
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #44]
	cmp	r2, r3
	movcs	r5, #1
	b	.L130
.L127:
	ldr	r7, .L134+24
	ldr	r6, .L134+28
	ldr	r8, .L134+32
.L130:
	ldr	r3, [r7, #0]
	cmp	r3, #0
	bne	.L121
	ldr	r4, .L134+36
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L132
	b	.L133
.L124:
	bl	vTaskIncrementTick
	ldr	r3, [r4, #0]
	sub	r3, r3, #1
	str	r3, [r4, #0]
.L132:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L124
	b	.L125
.L133:
	cmp	r5, #1
	beq	.L125
	ldr	r2, .L134+40
	ldr	r2, [r2, #0]
	cmp	r2, #1
	movne	r4, r3
	bne	.L117
.L125:
	ldr	r3, .L134+40
	mov	r2, #0
	str	r2, [r3, #0]
@ 1210 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
	mov	r4, #1
.L117:
.LBE37:
	bl	vPortExitCritical
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L135:
	.align	2
.L134:
	.word	.LANCHOR17
	.word	.LC11
	.word	.LC1
	.word	1151
	.word	.LANCHOR5
	.word	.LANCHOR0
	.word	.LANCHOR9
	.word	.LANCHOR15
	.word	.LANCHOR6
	.word	.LANCHOR19
	.word	.LANCHOR20
.LFE13:
	.size	xTaskResumeAll, .-xTaskResumeAll
	.section	.text.prvIdleTask,"ax",%progbits
	.align	2
	.type	prvIdleTask, %function
prvIdleTask:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI13:
.LBB38:
	ldr	r4, .L142
	ldr	r7, .L142+4
.LBB39:
	ldr	r6, .L142+8
.L141:
.LBE39:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L137
	bl	vTaskSuspendAll
	ldr	r5, [r7, #0]
	bl	xTaskResumeAll
	cmp	r5, #0
	beq	.L137
.LBB41:
	bl	vPortEnterCritical
	ldr	r3, [r7, #12]
	ldr	r5, [r3, #12]
	add	r0, r5, #4
	bl	vListRemove
	ldr	r3, [r6, #0]
	sub	r3, r3, #1
	str	r3, [r6, #0]
	ldr	r3, [r4, #0]
	sub	r3, r3, #1
	str	r3, [r4, #0]
	bl	vPortExitCritical
.LBB40:
	ldr	r0, [r5, #48]
	bl	vPortFree
	mov	r0, r5
	bl	vPortFree
.L137:
.LBE40:
.LBE41:
.LBE38:
	ldr	r3, .L142+12
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bls	.L141
@ 2083 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
	b	.L141
.L143:
	.align	2
.L142:
	.word	.LANCHOR16
	.word	.LANCHOR10
	.word	.LANCHOR5
	.word	.LANCHOR6
.LFE30:
	.size	prvIdleTask, .-prvIdleTask
	.section	.text.vTaskDelay,"ax",%progbits
	.align	2
	.global	vTaskDelay
	.type	vTaskDelay, %function
vTaskDelay:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI14:
	subs	r4, r0, #0
	beq	.L145
	bl	vTaskSuspendAll
	ldr	r3, .L147
	ldr	r3, [r3, #0]
	add	r4, r4, r3
	ldr	r3, .L147+4
	ldr	r0, [r3, #0]
	add	r0, r0, #4
	bl	vListRemove
	mov	r0, r4
	bl	prvAddCurrentTaskToDelayedList
	bl	xTaskResumeAll
	cmp	r0, #0
	ldmnefd	sp!, {r4, pc}
.L145:
@ 732 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
	ldmfd	sp!, {r4, pc}
.L148:
	.align	2
.L147:
	.word	.LANCHOR1
	.word	.LANCHOR0
.LFE3:
	.size	vTaskDelay, .-vTaskDelay
	.section	.text.vTaskDelayUntil,"ax",%progbits
	.align	2
	.global	vTaskDelayUntil
	.type	vTaskDelayUntil, %function
vTaskDelayUntil:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI15:
	subs	r4, r0, #0
	mov	r5, r1
	bne	.L150
	ldr	r0, .L159
	ldr	r1, .L159+4
	ldr	r2, .L159+8
	bl	__assert
.L150:
	cmp	r5, #0
	bne	.L151
	ldr	r0, .L159+12
	ldr	r1, .L159+4
	mov	r2, #636
	bl	__assert
.L151:
	bl	vTaskSuspendAll
	ldr	r2, .L159+16
	ldr	r3, [r4, #0]
	ldr	r1, [r2, #0]
	add	r5, r3, r5
	cmp	r1, r3
	bcs	.L152
	cmp	r5, r3
	movcs	r3, #0
	bcs	.L153
	b	.L158
.L152:
	cmp	r5, r3
	movcc	r3, #1
	bcc	.L153
.L158:
	ldr	r3, [r2, #0]
	cmp	r5, r3
	movls	r3, #0
	movhi	r3, #1
.L153:
	cmp	r3, #0
	str	r5, [r4, #0]
	beq	.L154
	ldr	r3, .L159+20
	ldr	r0, [r3, #0]
	add	r0, r0, #4
	bl	vListRemove
	mov	r0, r5
	bl	prvAddCurrentTaskToDelayedList
.L154:
	bl	xTaskResumeAll
	cmp	r0, #0
	ldmnefd	sp!, {r4, r5, pc}
@ 686 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
	ldmfd	sp!, {r4, r5, pc}
.L160:
	.align	2
.L159:
	.word	.LC12
	.word	.LC1
	.word	635
	.word	.LC13
	.word	.LANCHOR1
	.word	.LANCHOR0
.LFE2:
	.size	vTaskDelayUntil, .-vTaskDelayUntil
	.section	.text.vTaskSetApplicationTaskTag,"ax",%progbits
	.align	2
	.global	vTaskSetApplicationTaskTag
	.type	vTaskSetApplicationTaskTag, %function
vTaskSetApplicationTaskTag:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI16:
	subs	r5, r0, #0
	ldreq	r3, .L164
	mov	r4, r1
	ldreq	r5, [r3, #0]
	bl	vPortEnterCritical
	str	r4, [r5, #88]
	ldmfd	sp!, {r4, r5, lr}
	b	vPortExitCritical
.L165:
	.align	2
.L164:
	.word	.LANCHOR0
.LFE20:
	.size	vTaskSetApplicationTaskTag, .-vTaskSetApplicationTaskTag
	.section	.text.xTaskGetApplicationTaskTag,"ax",%progbits
	.align	2
	.global	xTaskGetApplicationTaskTag
	.type	xTaskGetApplicationTaskTag, %function
xTaskGetApplicationTaskTag:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI17:
	subs	r4, r0, #0
	ldreq	r3, .L169
	ldreq	r4, [r3, #0]
	bl	vPortEnterCritical
	ldr	r4, [r4, #88]
	bl	vPortExitCritical
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L170:
	.align	2
.L169:
	.word	.LANCHOR0
.LFE21:
	.size	xTaskGetApplicationTaskTag, .-xTaskGetApplicationTaskTag
	.section	.text.xTaskCallApplicationTaskHook,"ax",%progbits
	.align	2
	.global	xTaskCallApplicationTaskHook
	.type	xTaskCallApplicationTaskHook, %function
xTaskCallApplicationTaskHook:
.LFB22:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	ldreq	r3, .L176
	str	lr, [sp, #-4]!
.LCFI18:
	ldreq	r0, [r3, #0]
	ldr	r3, [r0, #88]
	cmp	r3, #0
	beq	.L175
	mov	r0, r1
	blx	r3
	ldr	pc, [sp], #4
.L175:
	mov	r0, r3
	ldr	pc, [sp], #4
.L177:
	.align	2
.L176:
	.word	.LANCHOR0
.LFE22:
	.size	xTaskCallApplicationTaskHook, .-xTaskCallApplicationTaskHook
	.section	.text.vTaskSwitchContext,"ax",%progbits
	.align	2
	.global	vTaskSwitchContext
	.type	vTaskSwitchContext, %function
vTaskSwitchContext:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L189
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI19:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L179
	ldr	r3, .L189+4
	mov	r2, #1
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, pc}
.L179:
	ldr	r4, .L189+8
	ldr	r3, [r4, #0]
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L181
.LBB42:
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #88]
	bl	vPortSaveVFPRegisters
.L181:
.LBE42:
.LBB43:
	bl	return_run_timer_counter_value
	ldr	r1, [r4, #0]
	ldr	r2, .L189+12
	ldr	lr, [r1, #92]
	ldr	ip, [r2, #0]
	ldr	r3, .L189+8
	rsb	ip, ip, lr
	add	ip, ip, r0
	str	ip, [r1, #92]
	str	r0, [r2, #0]
.LBE43:
	ldr	r1, [r4, #0]
	ldr	r2, [r4, #0]
	ldr	r1, [r1, #0]
	ldr	r2, [r2, #48]
	cmp	r1, r2
	bhi	.L182
	ldr	r0, [r3, #0]
	ldr	r1, [r3, #0]
	add	r1, r1, #52
	bl	vApplicationStackOverflowHook
.L182:
.LBB44:
	ldr	r4, .L189+8
	ldr	r1, .L189+16
	ldr	r3, [r4, #0]
	mov	r2, #20
	ldr	r0, [r3, #48]
	bl	memcmp
	cmp	r0, #0
	beq	.L184
	ldr	r0, [r4, #0]
	ldr	r1, [r4, #0]
	add	r1, r1, #52
	bl	vApplicationStackOverflowHook
	b	.L184
.L186:
.LBE44:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L185
	ldr	r0, .L189+20
	ldr	r1, .L189+24
	ldr	r2, .L189+28
	bl	__assert
.L185:
	ldr	r3, [r5, #0]
	sub	r3, r3, #1
	str	r3, [r5, #0]
	b	.L188
.L184:
	ldr	r4, .L189+32
	ldr	r6, .L189+36
	mov	r5, r4
.L188:
	ldr	r3, [r4, #0]
	mov	r2, #20
	mul	r3, r2, r3
	ldr	r3, [r6, r3]
	cmp	r3, #0
	beq	.L186
.LBB45:
	ldr	r3, .L189+32
	ldr	r1, [r3, #0]
	ldr	r3, .L189+36
	mla	r3, r2, r1, r3
	ldr	r2, [r3, #4]
	add	r1, r3, #8
	ldr	r2, [r2, #4]
	cmp	r2, r1
	str	r2, [r3, #4]
	ldreq	r2, [r2, #4]
	streq	r2, [r3, #4]
	ldr	r3, [r3, #4]
	ldr	r2, [r3, #12]
	ldr	r3, .L189+8
	str	r2, [r3, #0]
.LBE45:
	ldr	r2, [r3, #0]
	ldr	r2, [r2, #88]
	cmp	r2, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
.LBB46:
	ldr	r3, [r3, #0]
	ldr	r0, [r3, #88]
.LBE46:
	ldmfd	sp!, {r4, r5, r6, lr}
.LBB47:
	b	vPortRestoreVFPRegisters
.L190:
	.align	2
.L189:
	.word	.LANCHOR17
	.word	.LANCHOR20
	.word	.LANCHOR0
	.word	.LANCHOR21
	.word	.LANCHOR22
	.word	.LC14
	.word	.LC1
	.word	1798
	.word	.LANCHOR15
	.word	.LANCHOR6
.LBE47:
.LFE23:
	.size	vTaskSwitchContext, .-vTaskSwitchContext
	.section	.text.vTaskSuspend,"ax",%progbits
	.align	2
	.global	vTaskSuspend
	.type	vTaskSuspend, %function
vTaskSuspend:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI20:
	mov	r4, r0
	bl	vPortEnterCritical
	ldr	r3, .L199
	ldr	r2, [r3, #0]
	cmp	r4, r2
	beq	.L192
	cmp	r4, #0
	movne	r5, r4
	bne	.L193
.L192:
	ldr	r5, [r3, #0]
	mov	r4, #0
.L193:
	add	r6, r5, #4
	mov	r0, r6
	bl	vListRemove
	ldr	r3, [r5, #40]
	cmp	r3, #0
	beq	.L194
	add	r0, r5, #24
	bl	vListRemove
.L194:
	ldr	r5, .L199+4
	mov	r1, r6
	mov	r0, r5
	bl	vListInsertEnd
	bl	vPortExitCritical
	cmp	r4, #0
	ldmnefd	sp!, {r4, r5, r6, pc}
	ldr	r3, .L199+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L196
@ 907 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c" 1
	SWI 0
@ 0 "" 2
	ldmfd	sp!, {r4, r5, r6, pc}
.L196:
	ldr	r3, .L199+12
	ldr	r2, [r5, #0]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	ldreq	r3, .L199
	streq	r4, [r3, #0]
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldmfd	sp!, {r4, r5, r6, lr}
	b	vTaskSwitchContext
.L200:
	.align	2
.L199:
	.word	.LANCHOR0
	.word	.LANCHOR11
	.word	.LANCHOR12
	.word	.LANCHOR5
.LFE6:
	.size	vTaskSuspend, .-vTaskSuspend
	.section	.text.vTaskPlaceOnEventList,"ax",%progbits
	.align	2
	.global	vTaskPlaceOnEventList
	.type	vTaskPlaceOnEventList, %function
vTaskPlaceOnEventList:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI21:
	subs	r5, r0, #0
	mov	r4, r1
	bne	.L202
	ldr	r0, .L204
	ldr	r1, .L204+4
	ldr	r2, .L204+8
	bl	__assert
.L202:
	ldr	r6, .L204+12
	mov	r0, r5
	ldr	r1, [r6, #0]
	add	r1, r1, #24
	bl	vListInsert
	ldr	r0, [r6, #0]
	add	r0, r0, #4
	bl	vListRemove
	cmn	r4, #1
	bne	.L203
	ldr	r1, [r6, #0]
	ldr	r0, .L204+16
	add	r1, r1, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	vListInsertEnd
.L203:
	ldr	r3, .L204+20
	ldr	r0, [r3, #0]
	add	r0, r4, r0
	ldmfd	sp!, {r4, r5, r6, lr}
	b	prvAddCurrentTaskToDelayedList
.L205:
	.align	2
.L204:
	.word	.LC15
	.word	.LC1
	.word	1815
	.word	.LANCHOR0
	.word	.LANCHOR11
	.word	.LANCHOR1
.LFE24:
	.size	vTaskPlaceOnEventList, .-vTaskPlaceOnEventList
	.section	.text.vTaskPlaceOnEventListRestricted,"ax",%progbits
	.align	2
	.global	vTaskPlaceOnEventListRestricted
	.type	vTaskPlaceOnEventListRestricted, %function
vTaskPlaceOnEventListRestricted:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI22:
	subs	r5, r0, #0
	mov	r4, r1
	bne	.L207
	ldr	r0, .L208
	ldr	r1, .L208+4
	ldr	r2, .L208+8
	bl	__assert
.L207:
	ldr	r6, .L208+12
	mov	r0, r5
	ldr	r1, [r6, #0]
	add	r1, r1, #24
	bl	vListInsertEnd
	ldr	r0, [r6, #0]
	add	r0, r0, #4
	bl	vListRemove
	ldr	r3, .L208+16
	ldr	r0, [r3, #0]
	add	r0, r4, r0
	ldmfd	sp!, {r4, r5, r6, lr}
	b	prvAddCurrentTaskToDelayedList
.L209:
	.align	2
.L208:
	.word	.LC15
	.word	.LC1
	.word	1865
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE25:
	.size	vTaskPlaceOnEventListRestricted, .-vTaskPlaceOnEventListRestricted
	.section	.text.xTaskRemoveFromEventList,"ax",%progbits
	.align	2
	.global	xTaskRemoveFromEventList
	.type	xTaskRemoveFromEventList, %function
xTaskRemoveFromEventList:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #12]
	stmfd	sp!, {r4, r5, lr}
.LCFI23:
	ldr	r4, [r3, #12]
	cmp	r4, #0
	bne	.L211
	ldr	r0, .L216
	ldr	r1, .L216+4
	ldr	r2, .L216+8
	bl	__assert
.L211:
	add	r5, r4, #24
	mov	r0, r5
	bl	vListRemove
	ldr	r3, .L216+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldrne	r0, .L216+16
	bne	.L215
	add	r5, r4, #4
	mov	r0, r5
	bl	vListRemove
	ldr	r2, .L216+20
	ldr	r3, [r4, #44]
	ldr	r1, [r2, #0]
	mov	r0, #20
	cmp	r3, r1
	strhi	r3, [r2, #0]
	ldr	r2, .L216+24
	mla	r0, r3, r0, r2
.L215:
	mov	r1, r5
	bl	vListInsertEnd
	ldr	r3, .L216+28
	ldr	r0, [r4, #44]
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #44]
	cmp	r0, r3
	movcc	r0, #0
	movcs	r0, #1
	ldmfd	sp!, {r4, r5, pc}
.L217:
	.align	2
.L216:
	.word	.LC16
	.word	.LC1
	.word	1912
	.word	.LANCHOR17
	.word	.LANCHOR9
	.word	.LANCHOR15
	.word	.LANCHOR6
	.word	.LANCHOR0
.LFE26:
	.size	xTaskRemoveFromEventList, .-xTaskRemoveFromEventList
	.section	.text.vTaskSetTimeOutState,"ax",%progbits
	.align	2
	.global	vTaskSetTimeOutState
	.type	vTaskSetTimeOutState, %function
vTaskSetTimeOutState:
.LFB27:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI24:
	subs	r4, r0, #0
	bne	.L219
	ldr	r0, .L220
	ldr	r1, .L220+4
	ldr	r2, .L220+8
	bl	__assert
.L219:
	ldr	r3, .L220+12
	ldr	r3, [r3, #0]
	str	r3, [r4, #0]
	ldr	r3, .L220+16
	ldr	r3, [r3, #0]
	str	r3, [r4, #4]
	ldmfd	sp!, {r4, pc}
.L221:
	.align	2
.L220:
	.word	.LC17
	.word	.LC1
	.word	1946
	.word	.LANCHOR18
	.word	.LANCHOR1
.LFE27:
	.size	vTaskSetTimeOutState, .-vTaskSetTimeOutState
	.section	.text.xTaskCheckForTimeOut,"ax",%progbits
	.align	2
	.global	xTaskCheckForTimeOut
	.type	xTaskCheckForTimeOut, %function
xTaskCheckForTimeOut:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI25:
	subs	r4, r0, #0
	mov	r5, r1
	bne	.L223
	ldr	r0, .L230
	ldr	r1, .L230+4
	ldr	r2, .L230+8
	bl	__assert
.L223:
	cmp	r5, #0
	bne	.L224
	ldr	r0, .L230+12
	ldr	r1, .L230+4
	ldr	r2, .L230+16
	bl	__assert
.L224:
	bl	vPortEnterCritical
	ldr	r3, [r5, #0]
	cmn	r3, #1
	beq	.L227
	ldr	r2, .L230+20
	ldr	r1, [r2, #0]
	ldr	r2, [r4, #0]
	cmp	r1, r2
	ldr	r2, .L230+24
	beq	.L226
	ldr	r0, [r2, #0]
	ldr	r1, [r4, #4]
	cmp	r0, r1
	bcs	.L229
.L226:
	ldr	r1, [r2, #0]
	ldr	r2, [r4, #4]
	rsb	r1, r2, r1
	cmp	r1, r3
	bcs	.L229
	ldr	r1, .L230+24
	mov	r0, r4
	ldr	r1, [r1, #0]
	rsb	r2, r1, r2
	add	r3, r2, r3
	str	r3, [r5, #0]
	bl	vTaskSetTimeOutState
.L227:
	mov	r4, #0
	b	.L225
.L229:
	mov	r4, #1
.L225:
	bl	vPortExitCritical
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.L231:
	.align	2
.L230:
	.word	.LC17
	.word	.LC1
	.word	1956
	.word	.LC18
	.word	1957
	.word	.LANCHOR18
	.word	.LANCHOR1
.LFE28:
	.size	xTaskCheckForTimeOut, .-xTaskCheckForTimeOut
	.section	.text.vTaskMissedYield,"ax",%progbits
	.align	2
	.global	vTaskMissedYield
	.type	vTaskMissedYield, %function
vTaskMissedYield:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L233
	mov	r2, #1
	str	r2, [r3, #0]
	bx	lr
.L234:
	.align	2
.L233:
	.word	.LANCHOR20
.LFE29:
	.size	vTaskMissedYield, .-vTaskMissedYield
	.section	.text.uxTaskGetStackHighWaterMark,"ax",%progbits
	.align	2
	.global	uxTaskGetStackHighWaterMark
	.type	uxTaskGetStackHighWaterMark, %function
uxTaskGetStackHighWaterMark:
.LFB38:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldreq	r3, .L240
	ldreq	r0, [r3, #0]
	mov	r3, #0
	ldr	r2, [r0, #48]
.L237:
	mov	r0, r3, asl #16
	add	r3, r3, #1
.LBB48:
	add	r1, r2, r3
	ldrb	r1, [r1, #-1]	@ zero_extendqisi2
	cmp	r1, #165
	beq	.L237
.LBE48:
	mov	r0, r0, lsr #18
	bx	lr
.L241:
	.align	2
.L240:
	.word	.LANCHOR0
.LFE38:
	.size	uxTaskGetStackHighWaterMark, .-uxTaskGetStackHighWaterMark
	.section	.text.prvGenerateRunTimeStatsForTasksInList,"ax",%progbits
	.align	2
	.type	prvGenerateRunTimeStatsForTasksInList, %function
prvGenerateRunTimeStatsForTasksInList:
.LFB36:
	@ args = 0, pretend = 0, frame = 132
	@ frame_needed = 0, uses_anonymous_args = 0
.LBB49:
	ldr	r3, [r1, #4]
.LBE49:
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI26:
	fstmfdd	sp!, {d8}
.LCFI27:
	fldd	d8, .L249
.LBB50:
	ldr	r3, [r3, #4]
.LBE50:
	mov	r7, r2
.LBB51:
	add	r2, r1, #8
	cmp	r3, r2
	str	r3, [r1, #4]
	ldreq	r3, [r3, #4]
.LBE51:
	sub	sp, sp, #144
.LCFI28:
.LBB52:
	streq	r3, [r1, #4]
	ldr	r3, [r1, #4]
.LBE52:
	mov	r8, r0
.LBB53:
	ldr	r9, [r3, #12]
.LBE53:
	mov	r4, r1
.LBB54:
	add	r6, r1, #8
.L247:
	ldr	r3, [r4, #4]
	ldr	r3, [r3, #4]
	cmp	r3, r6
	str	r3, [r4, #4]
	ldreq	r3, [r6, #4]
	streq	r3, [r4, #4]
	ldr	r3, [r4, #4]
.LBE54:
	cmp	r7, #0
.LBB55:
	ldr	r5, [r3, #12]
	add	fp, r5, #52
.LBE55:
	beq	.L245
	ldr	r3, [r5, #92]
	mov	r0, r5
	str	r3, [sp, #12]
	ldr	sl, [r5, #92]
	bl	uxTaskGetStackHighWaterMark
	flds	s13, [sp, #12]	@ int
	ldr	r1, .L249+8
	mov	r2, fp
	fuitod	d7, s13
	fmsr	s13, r7	@ int
	mov	r3, sl
	fuitos	s12, s13
	fmuld	d7, d7, d8
	fcvtds	d6, s12
	fdivd	d7, d7, d6
	mov	r0, r0, asl #2
	str	r0, [sp, #8]
	add	r0, sp, #16
	fcvtsd	s14, d7
	fcvtds	d7, s14
	fdivd	d7, d7, d8
	fstd	d7, [sp, #0]
	bl	sprintf
	b	.L246
.L245:
	mov	r0, r5
	bl	uxTaskGetStackHighWaterMark
	ldr	r1, .L249+12
	mov	r2, fp
	ldr	r3, .L249+16
	mov	r0, r0, asl #2
	str	r0, [sp, #0]
	add	r0, sp, #16
	bl	sprintf
.L246:
	mov	r0, r8
	add	r1, sp, #16
	bl	strcat
	cmp	r5, r9
	bne	.L247
	add	sp, sp, #144
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L250:
	.align	2
.L249:
	.word	0
	.word	1079574528
	.word	.LC19
	.word	.LC20
	.word	.LC21
.LFE36:
	.size	prvGenerateRunTimeStatsForTasksInList, .-prvGenerateRunTimeStatsForTasksInList
	.global	__udivsi3
	.section	.text.vTaskGetRunTimeStats,"ax",%progbits
	.align	2
	.global	vTaskGetRunTimeStats
	.type	vTaskGetRunTimeStats, %function
vTaskGetRunTimeStats:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI29:
	ldr	r7, .L259
	mov	r4, r0
	bl	vTaskSuspendAll
	bl	return_run_timer_counter_value
	mov	r1, #100
	bl	__udivsi3
	mov	r3, #0
	strb	r3, [r4, #0]
	ldr	r3, .L259+4
	mov	r8, #20
	ldr	r6, [r3, #0]
	add	r6, r6, #1
	mov	r5, r0
.L253:
	sub	r6, r6, #1
	mul	r3, r8, r6
	add	r1, r7, r3
	ldr	r3, [r7, r3]
	cmp	r3, #0
	beq	.L252
	mov	r0, r4
	mov	r2, r5
	bl	prvGenerateRunTimeStatsForTasksInList
.L252:
	cmp	r6, #0
	bne	.L253
	ldr	r3, .L259+8
	ldr	r2, [r3, #0]
	ldr	r2, [r2, #0]
	cmp	r2, #0
	beq	.L254
	ldr	r1, [r3, #0]
	mov	r0, r4
	mov	r2, r5
	bl	prvGenerateRunTimeStatsForTasksInList
.L254:
	ldr	r3, .L259+12
	ldr	r2, [r3, #0]
	ldr	r2, [r2, #0]
	cmp	r2, #0
	beq	.L255
	ldr	r1, [r3, #0]
	mov	r0, r4
	mov	r2, r5
	bl	prvGenerateRunTimeStatsForTasksInList
.L255:
	ldr	r1, .L259+16
	ldr	r3, [r1, #0]
	cmp	r3, #0
	beq	.L256
	mov	r0, r4
	mov	r2, r5
	bl	prvGenerateRunTimeStatsForTasksInList
.L256:
	ldr	r1, .L259+20
	ldr	r3, [r1, #0]
	cmp	r3, #0
	beq	.L257
	mov	r0, r4
	mov	r2, r5
	bl	prvGenerateRunTimeStatsForTasksInList
.L257:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xTaskResumeAll
.L260:
	.align	2
.L259:
	.word	.LANCHOR6
	.word	.LANCHOR13
	.word	.LANCHOR3
	.word	.LANCHOR2
	.word	.LANCHOR10
	.word	.LANCHOR11
.LFE18:
	.size	vTaskGetRunTimeStats, .-vTaskGetRunTimeStats
	.section	.text.xTaskGetCurrentTaskHandle,"ax",%progbits
	.align	2
	.global	xTaskGetCurrentTaskHandle
	.type	xTaskGetCurrentTaskHandle, %function
xTaskGetCurrentTaskHandle:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L262
	ldr	r0, [r3, #0]
	bx	lr
.L263:
	.align	2
.L262:
	.word	.LANCHOR0
.LFE40:
	.size	xTaskGetCurrentTaskHandle, .-xTaskGetCurrentTaskHandle
	.section	.text.xTaskGetSchedulerState,"ax",%progbits
	.align	2
	.global	xTaskGetSchedulerState
	.type	xTaskGetSchedulerState, %function
xTaskGetSchedulerState:
.LFB41:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L268
	ldr	r0, [r3, #0]
	cmp	r0, #0
	bxeq	lr
	ldr	r3, .L268+4
	ldr	r0, [r3, #0]
	cmp	r0, #0
	movne	r0, #2
	moveq	r0, #1
	bx	lr
.L269:
	.align	2
.L268:
	.word	.LANCHOR12
	.word	.LANCHOR17
.LFE41:
	.size	xTaskGetSchedulerState, .-xTaskGetSchedulerState
	.section	.text.vTaskPriorityInherit,"ax",%progbits
	.align	2
	.global	vTaskPriorityInherit
	.type	vTaskPriorityInherit, %function
vTaskPriorityInherit:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI30:
	subs	r4, r0, #0
	bne	.L271
	ldr	r0, .L275
	ldr	r1, .L275+4
	ldr	r2, .L275+8
	bl	__assert
.L271:
	ldr	r5, .L275+12
	ldr	r3, [r4, #44]
	ldr	r2, [r5, #0]
	ldr	r2, [r2, #44]
	cmp	r3, r2
	ldmcsfd	sp!, {r4, r5, r6, pc}
	ldr	r2, [r5, #0]
	mov	r1, #20
	ldr	r2, [r2, #44]
	rsb	r2, r2, #16
	str	r2, [r4, #24]
	ldr	r2, .L275+16
	mla	r3, r1, r3, r2
	ldr	r2, [r4, #20]
	cmp	r2, r3
	ldrne	r3, [r5, #0]
	ldrne	r3, [r3, #44]
	strne	r3, [r4, #44]
	ldmnefd	sp!, {r4, r5, r6, pc}
	add	r6, r4, #4
	mov	r0, r6
	bl	vListRemove
	ldr	r3, [r5, #0]
	ldr	r2, .L275+20
	ldr	r3, [r3, #44]
	ldr	r1, [r2, #0]
	mov	r0, #20
	cmp	r3, r1
	strhi	r3, [r2, #0]
	ldr	r2, .L275+16
	mov	r1, r6
	mla	r0, r3, r0, r2
	str	r3, [r4, #44]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	vListInsertEnd
.L276:
	.align	2
.L275:
	.word	.LC22
	.word	.LC1
	.word	2595
	.word	.LANCHOR0
	.word	.LANCHOR6
	.word	.LANCHOR15
.LFE42:
	.size	vTaskPriorityInherit, .-vTaskPriorityInherit
	.section	.text.vTaskPriorityDisinherit,"ax",%progbits
	.align	2
	.global	vTaskPriorityDisinherit
	.type	vTaskPriorityDisinherit, %function
vTaskPriorityDisinherit:
.LFB43:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI31:
	subs	r4, r0, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r2, [r4, #44]
	ldr	r3, [r4, #84]
	cmp	r2, r3
	ldmeqfd	sp!, {r4, r5, pc}
	add	r5, r4, #4
	mov	r0, r5
	bl	vListRemove
	ldr	r3, [r4, #84]
	mov	r0, #20
	rsb	r2, r3, #16
	str	r2, [r4, #24]
	ldr	r2, .L280
	str	r3, [r4, #44]
	ldr	r1, [r2, #0]
	cmp	r3, r1
	strhi	r3, [r2, #0]
	ldr	r2, .L280+4
	mov	r1, r5
	mla	r0, r3, r0, r2
	ldmfd	sp!, {r4, r5, lr}
	b	vListInsertEnd
.L281:
	.align	2
.L280:
	.word	.LANCHOR15
	.word	.LANCHOR6
.LFE43:
	.size	vTaskPriorityDisinherit, .-vTaskPriorityDisinherit
	.global	pxCurrentTCB
	.section	.bss.xPendingReadyList,"aw",%nobits
	.align	2
	.set	.LANCHOR9,. + 0
	.type	xPendingReadyList, %object
	.size	xPendingReadyList, 20
xPendingReadyList:
	.space	20
	.section	.bss.uxTopReadyPriority,"aw",%nobits
	.align	2
	.set	.LANCHOR15,. + 0
	.type	uxTopReadyPriority, %object
	.size	uxTopReadyPriority, 4
uxTopReadyPriority:
	.space	4
	.section	.bss.uxTasksDeleted,"aw",%nobits
	.align	2
	.set	.LANCHOR16,. + 0
	.type	uxTasksDeleted, %object
	.size	uxTasksDeleted, 4
uxTasksDeleted:
	.space	4
	.section	.bss.xTickCount,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	xTickCount, %object
	.size	xTickCount, 4
xTickCount:
	.space	4
	.section	.bss.pxReadyTasksLists,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	pxReadyTasksLists, %object
	.size	pxReadyTasksLists, 320
pxReadyTasksLists:
	.space	320
	.section	.bss.pxOverflowDelayedTaskList,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	pxOverflowDelayedTaskList, %object
	.size	pxOverflowDelayedTaskList, 4
pxOverflowDelayedTaskList:
	.space	4
	.section	.bss.xTasksWaitingTermination,"aw",%nobits
	.align	2
	.set	.LANCHOR10,. + 0
	.type	xTasksWaitingTermination, %object
	.size	xTasksWaitingTermination, 20
xTasksWaitingTermination:
	.space	20
	.section	.bss.pxDelayedTaskList,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	pxDelayedTaskList, %object
	.size	pxDelayedTaskList, 4
pxDelayedTaskList:
	.space	4
	.section	.bss.uxCurrentNumberOfTasks,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	uxCurrentNumberOfTasks, %object
	.size	uxCurrentNumberOfTasks, 4
uxCurrentNumberOfTasks:
	.space	4
	.section	.bss.xSchedulerRunning,"aw",%nobits
	.align	2
	.set	.LANCHOR12,. + 0
	.type	xSchedulerRunning, %object
	.size	xSchedulerRunning, 4
xSchedulerRunning:
	.space	4
	.section	.bss.ulTaskSwitchedInTime,"aw",%nobits
	.align	2
	.set	.LANCHOR21,. + 0
	.type	ulTaskSwitchedInTime, %object
	.size	ulTaskSwitchedInTime, 4
ulTaskSwitchedInTime:
	.space	4
	.section	.bss.xMissedYield,"aw",%nobits
	.align	2
	.set	.LANCHOR20,. + 0
	.type	xMissedYield, %object
	.size	xMissedYield, 4
xMissedYield:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"pxTaskCode\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/tasks.c\000"
.LC2:
	.ascii	"( ( uxPriority & ( ~( ( unsigned long ) 0x00 ) ) ) "
	.ascii	"< 16 )\000"
.LC3:
	.ascii	"( ( ( unsigned long ) pxNewTCB->pxTopOfStack & ( un"
	.ascii	"signed long ) ( 0x0007 ) ) == 0UL )\000"
.LC4:
	.ascii	"( uxNewPriority < 16 )\000"
.LC5:
	.ascii	"xTask\000"
.LC6:
	.ascii	"pxTaskToResume\000"
.LC7:
	.ascii	"IDLE\000"
.LC8:
	.ascii	"xReturn\000"
.LC9:
	.ascii	"pxTCB\000"
.LC10:
	.ascii	"( ( ( pxDelayedTaskList )->uxNumberOfItems == ( uns"
	.ascii	"igned long ) 0 ) )\000"
.LC11:
	.ascii	"uxSchedulerSuspended\000"
.LC12:
	.ascii	"pxPreviousWakeTime\000"
.LC13:
	.ascii	"( xTimeIncrement > 0U )\000"
.LC14:
	.ascii	"uxTopReadyPriority\000"
.LC15:
	.ascii	"pxEventList\000"
.LC16:
	.ascii	"pxUnblockedTCB\000"
.LC17:
	.ascii	"pxTimeOut\000"
.LC18:
	.ascii	"pxTicksToWait\000"
.LC19:
	.ascii	"%15s  0x%08lX%10.3f%%%6u\012\000"
.LC20:
	.ascii	"%15s   %23s    12u\012\000"
.LC21:
	.ascii	"(task hasn't run @ all)\000"
.LC22:
	.ascii	"pxMutexHolder\000"
	.section	.bss.pxCurrentTCB,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	pxCurrentTCB, %object
	.size	pxCurrentTCB, 4
pxCurrentTCB:
	.space	4
	.section	.bss.xSuspendedTaskList,"aw",%nobits
	.align	2
	.set	.LANCHOR11,. + 0
	.type	xSuspendedTaskList, %object
	.size	xSuspendedTaskList, 20
xSuspendedTaskList:
	.space	20
	.section	.data.xNextTaskUnblockTime,"aw",%progbits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	xNextTaskUnblockTime, %object
	.size	xNextTaskUnblockTime, 4
xNextTaskUnblockTime:
	.word	-1
	.section	.bss.xDelayedTaskList1,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	xDelayedTaskList1, %object
	.size	xDelayedTaskList1, 20
xDelayedTaskList1:
	.space	20
	.section	.bss.xDelayedTaskList2,"aw",%nobits
	.align	2
	.set	.LANCHOR8,. + 0
	.type	xDelayedTaskList2, %object
	.size	xDelayedTaskList2, 20
xDelayedTaskList2:
	.space	20
	.section	.bss.uxTaskNumber,"aw",%nobits
	.align	2
	.set	.LANCHOR14,. + 0
	.type	uxTaskNumber, %object
	.size	uxTaskNumber, 4
uxTaskNumber:
	.space	4
	.section	.bss.uxSchedulerSuspended,"aw",%nobits
	.align	2
	.set	.LANCHOR17,. + 0
	.type	uxSchedulerSuspended, %object
	.size	uxSchedulerSuspended, 4
uxSchedulerSuspended:
	.space	4
	.section	.bss.uxMissedTicks,"aw",%nobits
	.align	2
	.set	.LANCHOR19,. + 0
	.type	uxMissedTicks, %object
	.size	uxMissedTicks, 4
uxMissedTicks:
	.space	4
	.section	.bss.xNumOfOverflows,"aw",%nobits
	.align	2
	.set	.LANCHOR18,. + 0
	.type	xNumOfOverflows, %object
	.size	xNumOfOverflows, 4
xNumOfOverflows:
	.space	4
	.section	.rodata.ucExpectedStackBytes.1602,"a",%progbits
	.set	.LANCHOR22,. + 0
	.type	ucExpectedStackBytes.1602, %object
	.size	ucExpectedStackBytes.1602, 20
ucExpectedStackBytes.1602:
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.byte	-91
	.section	.bss.uxTopUsedPriority,"aw",%nobits
	.align	2
	.set	.LANCHOR13,. + 0
	.type	uxTopUsedPriority, %object
	.size	uxTopUsedPriority, 4
uxTopUsedPriority:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI0-.LFB34
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI5-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI6-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI7-.LFB9
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI8-.LFB10
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI9-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI10-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI11-.LFB19
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI12-.LFB13
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI13-.LFB30
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI14-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI15-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI16-.LFB20
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI17-.LFB21
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI18-.LFB22
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI19-.LFB23
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI20-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI21-.LFB24
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI22-.LFB25
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI23-.LFB26
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI24-.LFB27
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI25-.LFB28
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI26-.LFB36
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xe
	.uleb128 0xbc
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI29-.LFB18
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI30-.LFB42
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI31-.LFB43
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE74:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/tasks.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x373
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF43
	.byte	0x1
	.4byte	.LASF44
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x8dc
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x843
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x8f6
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x899
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x3a7
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x9dd
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x8b9
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x9ad
	.byte	0x1
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x18f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x234
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x2e5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x2fb
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	0x45
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x3c9
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST6
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x3f6
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST7
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x424
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x465
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x470
	.4byte	.LFB12
	.4byte	.LFE12
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x4cf
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST9
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x4de
	.4byte	.LFB15
	.4byte	.LFE15
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x4eb
	.4byte	.LFB16
	.4byte	.LFE16
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x4f5
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST10
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x63e
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST11
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x478
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST12
	.uleb128 0x7
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x802
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST13
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x2b7
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST14
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x276
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST15
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x68b
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST16
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x6a5
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST17
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x6c2
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST18
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x6e0
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST19
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x367
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST20
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x713
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST21
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x745
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST22
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x765
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST23
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x798
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST24
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x7a0
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST25
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x7ce
	.4byte	.LFB29
	.4byte	.LFE29
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x9c1
	.4byte	.LFB38
	.4byte	.LFE38
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x938
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST26
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x546
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST27
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x9f1
	.4byte	.LFB40
	.4byte	.LFE40
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF40
	.byte	0x1
	.2byte	0xa03
	.4byte	.LFB41
	.4byte	.LFE41
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF41
	.byte	0x1
	.2byte	0xa1f
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST28
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF42
	.byte	0x1
	.2byte	0xa43
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST29
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB34
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB7
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB8
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB9
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB10
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB14
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB17
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB19
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB13
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB30
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB3
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB2
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB20
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB21
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB22
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB23
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB6
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB24
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB25
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB26
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB27
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB28
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB36
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI28
	.4byte	.LFE36
	.2byte	0x3
	.byte	0x7d
	.sleb128 188
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB18
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB42
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB43
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x144
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF16:
	.ascii	"xTaskGetTickCount\000"
.LASF13:
	.ascii	"vTaskStartScheduler\000"
.LASF31:
	.ascii	"xTaskRemoveFromEventList\000"
.LASF43:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF33:
	.ascii	"xTaskCheckForTimeOut\000"
.LASF11:
	.ascii	"vTaskResume\000"
.LASF29:
	.ascii	"vTaskPlaceOnEventList\000"
.LASF22:
	.ascii	"vTaskDelay\000"
.LASF4:
	.ascii	"prvDeleteTCB\000"
.LASF41:
	.ascii	"vTaskPriorityInherit\000"
.LASF40:
	.ascii	"xTaskGetSchedulerState\000"
.LASF23:
	.ascii	"vTaskDelayUntil\000"
.LASF1:
	.ascii	"prvInitialiseTCBVariables\000"
.LASF42:
	.ascii	"vTaskPriorityDisinherit\000"
.LASF19:
	.ascii	"pcTaskGetTaskName\000"
.LASF21:
	.ascii	"xTaskResumeAll\000"
.LASF12:
	.ascii	"xTaskResumeFromISR\000"
.LASF3:
	.ascii	"prvInitialiseTaskLists\000"
.LASF32:
	.ascii	"vTaskSetTimeOutState\000"
.LASF45:
	.ascii	"xTaskIsTaskSuspended\000"
.LASF10:
	.ascii	"vTaskPrioritySet\000"
.LASF15:
	.ascii	"vTaskSuspendAll\000"
.LASF7:
	.ascii	"xTaskGenericCreate\000"
.LASF14:
	.ascii	"vTaskEndScheduler\000"
.LASF24:
	.ascii	"vTaskSetApplicationTaskTag\000"
.LASF38:
	.ascii	"vTaskGetRunTimeStats\000"
.LASF28:
	.ascii	"vTaskSuspend\000"
.LASF37:
	.ascii	"prvGenerateRunTimeStatsForTasksInList\000"
.LASF8:
	.ascii	"vTaskDelete\000"
.LASF35:
	.ascii	"uxTaskGetStackHighWaterMark\000"
.LASF34:
	.ascii	"vTaskMissedYield\000"
.LASF9:
	.ascii	"uxTaskPriorityGet\000"
.LASF27:
	.ascii	"vTaskSwitchContext\000"
.LASF26:
	.ascii	"xTaskCallApplicationTaskHook\000"
.LASF39:
	.ascii	"xTaskGetCurrentTaskHandle\000"
.LASF2:
	.ascii	"prvAllocateTCBAndStack\000"
.LASF17:
	.ascii	"xTaskGetTickCountFromISR\000"
.LASF18:
	.ascii	"uxTaskGetNumberOfTasks\000"
.LASF0:
	.ascii	"prvAddCurrentTaskToDelayedList\000"
.LASF44:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/tasks.c\000"
.LASF5:
	.ascii	"prvCheckTasksWaitingTermination\000"
.LASF6:
	.ascii	"usTaskCheckFreeStackSpace\000"
.LASF36:
	.ascii	"prvIdleTask\000"
.LASF25:
	.ascii	"xTaskGetApplicationTaskTag\000"
.LASF20:
	.ascii	"vTaskIncrementTick\000"
.LASF30:
	.ascii	"vTaskPlaceOnEventListRestricted\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
