	.file	"ftimes_vars.c"
	.text
.Ltext0:
	.section	.text.unwind_list,"ax",%progbits
	.align	2
	.type	unwind_list, %function
unwind_list:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r1, .L5
	mov	r4, r0
	mov	r2, #75
	b	.L4
.L3:
	ldr	r1, .L5
	mov	r2, #79
	bl	mem_free_debug
	ldr	r1, .L5
	mov	r0, r4
	mov	r2, #81
.L4:
	bl	nm_ListRemoveHead_debug
	cmp	r0, #0
	bne	.L3
	ldmfd	sp!, {r4, pc}
.L6:
	.align	2
.L5:
	.word	.LC0
.LFE0:
	.size	unwind_list, .-unwind_list
	.section	.text.FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID,"ax",%progbits
	.align	2
	.global	FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID
	.type	FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID, %function
FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r4, r0
	ldr	r0, .L13
	bl	nm_ListGetFirst
	b	.L12
.L10:
	ldr	r3, [r1, #12]
	cmp	r3, r4
	beq	.L9
	ldr	r0, .L13
	bl	nm_ListGetNext
.L12:
	cmp	r0, #0
	mov	r1, r0
	bne	.L10
.L9:
	mov	r0, r1
	ldmfd	sp!, {r4, pc}
.L14:
	.align	2
.L13:
	.word	.LANCHOR0
.LFE2:
	.size	FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID, .-FTIMES_VARS_return_ptr_to_manual_program_group_in_ftimes_list_with_this_GID
	.section	.text.FTIMES_VARS_clean_and_reload_all_ftimes_data_structures,"ax",%progbits
	.align	2
	.global	FTIMES_VARS_clean_and_reload_all_ftimes_data_structures
	.type	FTIMES_VARS_clean_and_reload_all_ftimes_data_structures, %function
FTIMES_VARS_clean_and_reload_all_ftimes_data_structures:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
.LBB4:
	ldr	r0, .L18
	mov	r2, #86016
	mov	r1, #0
	bl	memset
	ldr	r0, .L18+4
	mov	r1, #4
	bl	nm_ListInit
	mov	r1, #16
	ldr	r0, .L18+8
	bl	nm_ListInit
	ldr	r0, .L18+12
	bl	unwind_list
	mov	r1, #0
	ldr	r0, .L18+12
	bl	nm_ListInit
	ldr	r0, .L18+16
	bl	unwind_list
	mov	r1, #0
	ldr	r0, .L18+16
	bl	nm_ListInit
	ldr	r0, .L18+20
	bl	unwind_list
	mov	r1, #0
	ldr	r0, .L18+20
	bl	nm_ListInit
.LBE4:
	ldr	r0, .L18+24
	bl	EPSON_obtain_latest_complete_time_and_date
	ldr	r3, .L18+28
	mov	r2, #1
	str	r2, [r3, #28]
	ldr	r2, [r3, #8]
	mov	r4, #0
	str	r2, [r3, #32]
	ldrh	r2, [r3, #12]
	str	r4, [r3, #44]
	add	r2, r2, #14
	strh	r2, [r3, #36]	@ movhi
	ldr	r2, .L18+32
	ldr	r0, .L18+36
	str	r2, [r3, #40]
	mov	r2, #12096
	str	r2, [r3, #48]
	mov	r2, #0
	str	r2, [r3, #52]	@ float
	str	r2, [r3, #56]	@ float
	ldr	r2, .L18+40
	mov	r1, r4
	ldr	r2, [r2, #0]
	ldr	r5, .L18+44
	str	r2, [r3, #60]
	mov	r2, #48
	bl	memset
.L16:
	mov	r0, r4
	bl	NETWORK_CONFIG_get_electrical_limit
	add	r4, r4, #1
	cmp	r4, #12
	str	r0, [r5, #4]!
	bne	.L16
	mov	r0, #20
	bl	WEATHER_TABLES_set_et_ratio
	bl	WEATHER_TABLES_get_et_ratio
	ldr	r3, .L18+48
	str	r0, [r3, #0]	@ float
	ldr	r3, .L18+28
	ldrh	r0, [r3, #12]
	bl	STATION_GROUPS_load_ftimes_list
	bl	IRRIGATION_SYSTEM_load_ftimes_list
	bl	MANUAL_PROGRAMS_load_ftimes_list
	ldmfd	sp!, {r4, r5, lr}
	b	STATIONS_load_all_the_stations_for_ftimes
.L19:
	.align	2
.L18:
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LANCHOR4
	.word	.LANCHOR5
	.word	.LANCHOR0
	.word	ftcs+8
	.word	ftcs
	.word	1209600
	.word	.LANCHOR6
	.word	my_tick_count
	.word	.LANCHOR7-4
	.word	.LANCHOR8
.LFE3:
	.size	FTIMES_VARS_clean_and_reload_all_ftimes_data_structures, .-FTIMES_VARS_clean_and_reload_all_ftimes_data_structures
	.global	ft_manual_programs_list_hdr
	.global	ft_system_groups_list_hdr
	.global	ft_station_groups_list_hdr
	.global	ft_stations_ON_list_hdr
	.global	ft_irrigating_stations_list_hdr
	.global	ft_stations
	.global	ft_et_ratio
	.global	ufim_one_or_more_in_the_list_for_manual_program
	.global	ft_electrical_limits
	.global	ft_stations_ON_by_controller
	.section	.bss.ft_et_ratio,"aw",%nobits
	.align	2
	.set	.LANCHOR8,. + 0
	.type	ft_et_ratio, %object
	.size	ft_et_ratio, 4
ft_et_ratio:
	.space	4
	.section	.bss.ft_stations,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	ft_stations, %object
	.size	ft_stations, 86016
ft_stations:
	.space	86016
	.section	.bss.ft_manual_programs_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	ft_manual_programs_list_hdr, %object
	.size	ft_manual_programs_list_hdr, 20
ft_manual_programs_list_hdr:
	.space	20
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ftim"
	.ascii	"es/ftimes_vars.c\000"
	.section	.bss.ft_stations_ON_by_controller,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	ft_stations_ON_by_controller, %object
	.size	ft_stations_ON_by_controller, 48
ft_stations_ON_by_controller:
	.space	48
	.section	.bss.ft_electrical_limits,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	ft_electrical_limits, %object
	.size	ft_electrical_limits, 48
ft_electrical_limits:
	.space	48
	.section	.bss.ft_system_groups_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	ft_system_groups_list_hdr, %object
	.size	ft_system_groups_list_hdr, 20
ft_system_groups_list_hdr:
	.space	20
	.section	.bss.ft_irrigating_stations_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	ft_irrigating_stations_list_hdr, %object
	.size	ft_irrigating_stations_list_hdr, 20
ft_irrigating_stations_list_hdr:
	.space	20
	.section	.bss.ufim_one_or_more_in_the_list_for_manual_program,"aw",%nobits
	.align	2
	.type	ufim_one_or_more_in_the_list_for_manual_program, %object
	.size	ufim_one_or_more_in_the_list_for_manual_program, 4
ufim_one_or_more_in_the_list_for_manual_program:
	.space	4
	.section	.bss.ft_station_groups_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	ft_station_groups_list_hdr, %object
	.size	ft_station_groups_list_hdr, 20
ft_station_groups_list_hdr:
	.space	20
	.section	.bss.ft_stations_ON_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	ft_stations_ON_list_hdr, %object
	.size	ft_stations_ON_list_hdr, 20
ft_stations_ON_list_hdr:
	.space	20
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ftimes/ftimes_vars.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x61
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x56
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.byte	0x47
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x77
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x96
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"free_memory_and_initialize_all_ftimes_lists\000"
.LASF0:
	.ascii	"FTIMES_VARS_return_ptr_to_manual_program_group_in_f"
	.ascii	"times_list_with_this_GID\000"
.LASF5:
	.ascii	"unwind_list\000"
.LASF1:
	.ascii	"FTIMES_VARS_clean_and_reload_all_ftimes_data_struct"
	.ascii	"ures\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ftim"
	.ascii	"es/ftimes_vars.c\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
