	.file	"r_poc_report.c"
	.text
.Ltext0:
	.section	.text.FDTO_POC_USAGE_load_poc_name_into_guivar,"ax",%progbits
	.align	2
	.type	FDTO_POC_USAGE_load_poc_name_into_guivar, %function
FDTO_POC_USAGE_load_poc_name_into_guivar:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L4
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r2, .L4+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #74
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	POC_get_ptr_to_physically_available_poc
	subs	r4, r0, #0
	beq	.L2
	bl	nm_GROUP_get_name
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L4+8
	bl	strlcpy
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	mov	r4, r0
	bl	POC_get_box_index_0
	ldr	r3, .L4+12
	str	r0, [r3, #0]
	mov	r0, r4
	bl	POC_get_type_of_poc
	ldr	r3, .L4+16
	mov	r1, #0
	str	r0, [r3, #0]
	mov	r0, r4
	bl	POC_get_decoder_serial_number_for_this_poc_gid_and_level_0
	ldr	r3, .L4+20
	str	r0, [r3, #0]
	b	.L3
.L2:
	ldr	r0, .L4+4
	bl	RemovePathFromFileName
	mov	r2, #94
	mov	r1, r0
	ldr	r0, .L4+24
	bl	Alert_Message_va
	ldr	r3, .L4+8
	strb	r4, [r3, #0]
.L3:
	ldr	r3, .L4
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L5:
	.align	2
.L4:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_GroupName
	.word	GuiVar_POCBoxIndex
	.word	GuiVar_POCType
	.word	GuiVar_POCDecoderSN1
	.word	.LC1
.LFE0:
	.size	FDTO_POC_USAGE_load_poc_name_into_guivar, .-FDTO_POC_USAGE_load_poc_name_into_guivar
	.section	.text.FDTO_POC_USAGE_redraw_scrollbox,"ax",%progbits
	.align	2
	.global	FDTO_POC_USAGE_redraw_scrollbox
	.type	FDTO_POC_USAGE_redraw_scrollbox, %function
FDTO_POC_USAGE_redraw_scrollbox:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r4, .L7
	ldr	r5, .L7+4
	ldr	r0, [r4, #0]
	bl	FDTO_POC_USAGE_load_poc_name_into_guivar
	ldr	r0, [r4, #0]
	bl	FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines
	ldr	r2, [r5, #0]
	mov	r4, r0
	subs	r2, r4, r2
	mov	r0, #0
	mov	r1, r4
	movne	r2, #1
	bl	FDTO_SCROLL_BOX_redraw_retaining_topline
	str	r4, [r5, #0]
	ldmfd	sp!, {r4, r5, pc}
.L8:
	.align	2
.L7:
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE1:
	.size	FDTO_POC_USAGE_redraw_scrollbox, .-FDTO_POC_USAGE_redraw_scrollbox
	.section	.text.FDTO_POC_USAGE_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_POC_USAGE_draw_report
	.type	FDTO_POC_USAGE_draw_report, %function
FDTO_POC_USAGE_draw_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	mvn	r1, #0
	mov	r4, r0
	mov	r2, #1
	mov	r0, #91
	bl	GuiLib_ShowScreen
	cmp	r4, #1
	bne	.L10
	mov	r0, #0
	bl	POC_populate_pointers_of_POCs_for_display
	ldr	r3, .L12
	str	r0, [r3, #0]
	ldr	r3, .L12+4
	ldr	r2, [r3, #0]
	cmp	r2, r0
	movcs	r2, #0
	strcs	r2, [r3, #0]
	ldr	r0, [r3, #0]
	bl	FDTO_POC_USAGE_load_poc_name_into_guivar
.L10:
	ldr	r3, .L12+4
	ldr	r0, [r3, #0]
	bl	FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines
	ldr	r3, .L12+8
	ldr	r2, .L12+12
	mov	r1, r0
	str	r0, [r3, #0]
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	b	FDTO_REPORTS_draw_report
.L13:
	.align	2
.L12:
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	FDTO_POC_REPORT_load_guivars_for_scroll_line
.LFE2:
	.size	FDTO_POC_USAGE_draw_report, .-FDTO_POC_USAGE_draw_report
	.section	.text.POC_USAGE_process_report,"ax",%progbits
	.align	2
	.global	POC_USAGE_process_report
	.type	POC_USAGE_process_report, %function
POC_USAGE_process_report:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI3:
	sub	sp, sp, #44
.LCFI4:
	beq	.L16
	cmp	r0, #20
	bne	.L21
	b	.L22
.L16:
	mov	r6, #80
	b	.L17
.L22:
	mov	r6, #84
.L17:
	ldr	r5, .L23
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L23+4
	mov	r3, #205
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L23+8
	mov	r2, #0
	ldr	r3, [r3, #0]
	mov	r4, #1
	mov	r0, r6
	ldr	r1, .L23+12
	sub	r3, r3, #1
	str	r2, [sp, #4]
	str	r4, [sp, #0]
	bl	process_uns32
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L23+16
	add	r0, sp, #8
	str	r4, [sp, #8]
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L14
.L21:
	mov	r2, #0
	mov	r3, r2
	bl	REPORTS_process_report
.L14:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, pc}
.L24:
	.align	2
.L23:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	FDTO_POC_USAGE_redraw_scrollbox
.LFE3:
	.size	POC_USAGE_process_report, .-POC_USAGE_process_report
	.section	.bss.g_POC_USAGE_index_of_poc_to_show,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_POC_USAGE_index_of_poc_to_show, %object
	.size	g_POC_USAGE_index_of_poc_to_show, 4
g_POC_USAGE_index_of_poc_to_show:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_poc_report.c\000"
.LC1:
	.ascii	"POC: invalid result. : %s, %u\000"
	.section	.bss.g_POC_USAGE_poc_count,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_POC_USAGE_poc_count, %object
	.size	g_POC_USAGE_poc_count, 4
g_POC_USAGE_poc_count:
	.space	4
	.section	.bss.g_POC_USAGE_line_count,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_POC_USAGE_line_count, %object
	.size	g_POC_USAGE_line_count, 4
g_POC_USAGE_line_count:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_poc_report.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x44
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x74
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x8f
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xb9
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_poc_report.c\000"
.LASF1:
	.ascii	"FDTO_POC_USAGE_draw_report\000"
.LASF2:
	.ascii	"POC_USAGE_process_report\000"
.LASF5:
	.ascii	"FDTO_POC_USAGE_load_poc_name_into_guivar\000"
.LASF0:
	.ascii	"FDTO_POC_USAGE_redraw_scrollbox\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
