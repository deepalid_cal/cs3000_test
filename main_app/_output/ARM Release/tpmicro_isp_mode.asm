	.file	"tpmicro_isp_mode.c"
	.text
.Ltext0:
	.section	.text.__setup_for_normal_string_exchange,"ax",%progbits
	.align	2
	.type	__setup_for_normal_string_exchange, %function
__setup_for_normal_string_exchange:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, lr}
.LCFI0:
	ldr	r7, .L2
	mov	r4, r0
	mov	r5, r1
	mov	r2, #100
	mov	r1, #300
	mov	r0, #0
	bl	RCVD_DATA_enable_hunting_mode
	mov	r0, r4
	bl	strlen
	ldr	r3, .L2+4
	str	r5, [r7, r3]
	mov	r6, r0
	mov	r0, r5
	bl	strlen
	ldr	r3, .L2+8
	mov	r2, #2
	mov	r1, r4
	str	r0, [r7, r3]
	mov	r0, #0
	add	r3, r3, #4
	str	r0, [r7, r3]
	mov	r3, #1
	stmia	sp, {r2, r3}
	mov	r2, r6
	mov	r3, r0
	bl	AddCopyOfBlockToXmitList
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, pc}
.L3:
	.align	2
.L2:
	.word	SerDrvrVars_s
	.word	4196
	.word	4200
.LFE1:
	.size	__setup_for_normal_string_exchange, .-__setup_for_normal_string_exchange
	.global	__umodsi3
	.global	__udivsi3
	.section	.text.TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg,"ax",%progbits
	.align	2
	.global	TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg
	.type	TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg, %function
TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg:
.LFB2:
	@ args = 0, pretend = 0, frame = 140
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI1:
	ldr	r4, .L114
	sub	sp, sp, #152
.LCFI2:
	mvn	r3, #0
	mov	r2, #0
	str	r3, [sp, #0]
	mov	r5, r0
	mov	r1, #1
	ldr	r0, [r4, #8]
	mov	r3, r2
	bl	xTimerGenericCommand
	ldr	r6, [r4, #16]
	cmp	r6, #80
	beq	.L13
	bhi	.L22
	cmp	r6, #40
	beq	.L9
	bhi	.L23
	cmp	r6, #20
	beq	.L7
	cmp	r6, #30
	beq	.L8
	cmp	r6, #10
	bne	.L101
	b	.L109
.L23:
	cmp	r6, #60
	beq	.L11
	cmp	r6, #70
	beq	.L12
	cmp	r6, #50
	bne	.L101
	b	.L110
.L22:
	cmp	r6, #120
	beq	.L17
	bhi	.L24
	cmp	r6, #100
	beq	.L15
	cmp	r6, #110
	beq	.L16
	cmp	r6, #90
	bne	.L101
	b	.L111
.L24:
	cmp	r6, #140
	beq	.L19
	bhi	.L25
	cmp	r6, #130
	bne	.L101
	b	.L112
.L25:
	cmp	r6, #150
	beq	.L20
	cmp	r6, #900
	bne	.L101
	b	.L113
.L109:
	ldr	r3, [r5, #0]
	cmp	r3, #1792
	bne	.L26
	bl	elevate_priority_of_ISP_related_code_update_tasks
	ldr	r0, .L114+4
	ldr	r1, .L114+8
	bl	__setup_for_normal_string_exchange
	mov	r6, #20
	mov	r3, #0
	str	r3, [r4, #60]
	str	r6, [r4, #16]
	b	.L5
.L26:
	ldr	r0, .L114+12
	bl	RemovePathFromFileName
	ldr	r2, .L114+16
	mov	r1, r0
	ldr	r0, .L114+20
	bl	Alert_Message_va
	b	.L101
.L7:
	ldr	r3, [r5, #0]
	cmp	r3, #2304
	bne	.L27
	ldr	r0, .L114+24
	ldr	r1, .L114+28
	bl	__setup_for_normal_string_exchange
	mov	r3, #40
	b	.L102
.L27:
	cmp	r3, #1792
	bne	.L5
	mov	r3, #1
	str	r3, [r4, #60]
	mov	r3, #30
.L102:
	str	r3, [r4, #16]
	b	.L5
.L8:
	mov	r0, #0
	mov	r2, r0
	mov	r1, #100
	bl	RCVD_DATA_enable_hunting_mode
	ldr	r0, .L114+32
	bl	strlen
	mov	r3, #2
	mov	ip, #1
	stmia	sp, {r3, ip}
	ldr	r1, .L114+32
	mov	r2, r0
	mov	r0, #0
	mov	r3, r0
	bl	AddCopyOfBlockToXmitList
	mov	r3, #40
	b	.L103
.L9:
	ldr	r3, [r5, #0]
	cmp	r3, #2304
	ldreq	r0, .L114+36
	ldreq	r1, .L114+40
	beq	.L107
.L28:
	cmp	r3, #1792
	bne	.L101
	ldr	r3, [r4, #60]
	cmp	r3, #0
	beq	.L31
	ldr	r0, .L114+36
	mov	r1, #0
.L107:
	bl	__setup_for_normal_string_exchange
	mov	r3, #50
	b	.L103
.L110:
	ldr	r3, [r5, #0]
	cmp	r3, #2304
	ldreq	r0, .L114+44
	ldreq	r1, .L114+48
	beq	.L106
.L30:
	cmp	r3, #1792
	bne	.L101
	ldr	r3, [r4, #60]
	cmp	r3, #0
	beq	.L31
	ldr	r0, .L114+44
	mov	r1, #0
.L106:
	bl	__setup_for_normal_string_exchange
	mov	r3, #60
	b	.L103
.L31:
	mov	r3, #1
	str	r3, [r4, #60]
	mov	r3, #30
	b	.L103
.L11:
	ldr	r0, .L114+52
	ldr	r1, .L114+56
	bl	__setup_for_normal_string_exchange
	mov	r3, #70
	b	.L103
.L12:
	ldr	r3, [r5, #0]
	cmp	r3, #2304
	bne	.L85
	ldr	r0, .L114+60
	bl	Alert_message_on_tpmicro_pile_T
	bl	CODE_DOWNLOAD_draw_tp_micro_update_dialog
	ldr	r0, .L114+64
	mov	r1, #66
	bl	FLASH_STORAGE_request_code_image_file_read
	mov	r3, #80
	str	r3, [r4, #16]
	b	.L4
.L13:
	ldr	r3, [r5, #0]
	cmp	r3, #2816
	ldrne	r0, .L114+68
	bne	.L108
	ldr	r3, [r5, #16]
	cmp	r3, #0
	ldreq	r0, .L114+72
	beq	.L108
	add	r3, r5, #16
	ldmia	r3, {r2-r3}
	str	r2, [r4, #20]
	str	r3, [r4, #24]
	ldr	r4, .L114+76
	ldr	r3, [r4, #108]
	cmp	r3, #0
	beq	.L36
.LBB5:
	ldr	r0, [r5, #20]
	add	r1, sp, #148
	ldr	r2, .L114+12
	ldr	r3, .L114+80
	bl	mem_oabia
	cmp	r0, #0
	beq	.L36
	ldr	r1, [r5, #16]
	ldr	r2, [r5, #20]
	ldr	r0, [sp, #148]
	bl	memcpy
	ldr	r3, .L114+84
	mov	r0, #0
	str	r0, [r3, #188]
	ldr	r3, [r5, #20]
	str	r0, [r4, #108]
	ldr	r1, .L114+64
	str	r3, [sp, #0]
	mov	r2, r0
	ldr	r3, [sp, #148]
	str	r0, [sp, #4]
	str	r0, [sp, #8]
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
.L36:
.LBE5:
	ldr	r4, .L114
	mov	r2, #0
	ldr	r3, [r4, #20]
	add	r3, r3, #1024
	str	r3, [r4, #36]
	mov	r3, #1024
	str	r3, [r4, #32]
	ldr	r3, [r5, #20]
	str	r2, [r4, #56]
	sub	r3, r3, #1024
	b	.L37
.L92:
	mov	r3, r1
.L37:
	ands	r2, r3, #3
	add	r1, r3, #1
	bne	.L92
	str	r3, [r4, #48]
	mov	r3, #100
	str	r3, [r4, #28]
	str	r2, [r4, #44]
	str	r2, [r4, #40]
	str	r2, [r4, #52]
	ldr	r0, .L114+88
	ldr	r1, .L114+92
	bl	__setup_for_normal_string_exchange
	mov	r3, #90
	b	.L103
.L108:
	bl	Alert_Message
	b	.L85
.L111:
	ldr	r3, [r5, #0]
	cmp	r3, #2304
	bne	.L85
	ldr	r0, .L114+96
	ldr	r1, .L114+92
	bl	__setup_for_normal_string_exchange
	ldr	r3, [r4, #28]
	b	.L103
.L15:
	ldr	r3, [r5, #0]
	cmp	r3, #2304
	bne	.L85
	ldr	r0, .L114+100
	ldr	r1, .L114+92
	bl	__setup_for_normal_string_exchange
	mov	r3, #110
	str	r3, [r4, #16]
	mov	r6, #250
	b	.L5
.L16:
	ldr	r3, [r5, #0]
	cmp	r3, #2304
	bne	.L85
	ldr	r3, [r4, #48]
	cmp	r3, #1024
	bls	.L41
	add	r0, sp, #124
	mov	r1, #24
	ldr	r2, .L114+104
	ldr	r3, .L114+108
	bl	snprintf
	b	.L42
.L41:
	str	r3, [sp, #0]
	add	r0, sp, #124
	mov	r1, #24
	ldr	r2, .L114+112
	ldr	r3, .L114+108
	bl	snprintf
.L42:
	add	r0, sp, #124
	ldr	r1, .L114+92
	bl	__setup_for_normal_string_exchange
	mov	r2, #120
	ldr	r3, .L114
	b	.L104
.L17:
.LBB6:
.LBB7:
	mov	r1, #0
	mov	r2, #45
	add	r0, sp, #76
	bl	memset
	ldr	r5, [r4, #52]
	ldr	r3, [r4, #48]
	rsb	r5, r5, #1024
	cmp	r5, r3
	movcs	r5, r3
	cmp	r5, #45
	movcs	r5, #45
	add	r0, sp, #76
	ldr	r1, [r4, #36]
	mov	r2, r5
	bl	memcpy
	ldr	r3, [r4, #48]
	rsb	r3, r5, r3
	str	r3, [r4, #48]
	ldr	r3, [r4, #36]
	add	r3, r3, r5
	str	r3, [r4, #36]
	ldr	r3, [r4, #52]
	add	r3, r5, r3
	str	r3, [r4, #52]
	add	r3, r5, #32
	strb	r3, [sp, #12]
	b	.L44
.L45:
	add	r5, r5, #1
.L44:
	mov	r0, r5
	mov	r1, #3
	bl	__umodsi3
	cmp	r0, #0
	bne	.L45
	mov	r0, r5
	mov	r1, #3
	bl	__udivsi3
	ldr	r3, .L114
	mov	r4, r5
	ldr	r8, [r3, #44]
	add	r7, sp, #12
	add	r6, sp, #76
	mov	r2, r0, asl #2
	add	r2, r2, #3
	b	.L46
.L75:
	ldrb	r0, [r6, #0]	@ zero_extendqisi2
	ldrb	r1, [r6, #1]	@ zero_extendqisi2
	cmp	r0, #127
	add	ip, r0, r1
	subgt	r0, r0, #128
	andgt	r0, r0, #255
	movgt	lr, #32
	movle	lr, #0
	cmp	r0, #63
	subgt	r0, r0, #64
	andgt	r0, r0, #255
	addgt	lr, lr, #16
	andgt	lr, lr, #255
	cmp	r0, #31
	subgt	r0, r0, #32
	andgt	r0, r0, #255
	addgt	lr, lr, #8
	andgt	lr, lr, #255
	cmp	r0, #15
	subgt	r0, r0, #16
	andgt	r0, r0, #255
	addgt	lr, lr, #4
	andgt	lr, lr, #255
	cmp	r0, #7
	subgt	r0, r0, #8
	andgt	r0, r0, #255
	addgt	lr, lr, #2
	andgt	lr, lr, #255
	cmp	r0, #3
	subgt	r0, r0, #4
	ldrb	r3, [r6, #2]	@ zero_extendqisi2
	andgt	r0, r0, #255
	addgt	lr, lr, #1
	andgt	lr, lr, #255
	cmp	r0, #1
	subgt	r0, r0, #2
	add	ip, ip, r3
	andgt	r0, r0, #255
	add	r8, r8, ip
	movgt	ip, #32
	movle	ip, #0
	cmp	r0, #0
	addne	ip, ip, #16
	andne	ip, ip, #255
	cmp	r1, #127
	subgt	r1, r1, #128
	andgt	r1, r1, #255
	addgt	ip, ip, #8
	andgt	ip, ip, #255
	cmp	r1, #63
	subgt	r1, r1, #64
	andgt	r1, r1, #255
	addgt	ip, ip, #4
	andgt	ip, ip, #255
	cmp	r1, #31
	subgt	r1, r1, #32
	andgt	r1, r1, #255
	addgt	ip, ip, #2
	andgt	ip, ip, #255
	cmp	r1, #15
	subgt	r1, r1, #16
	andgt	r1, r1, #255
	addgt	ip, ip, #1
	andgt	ip, ip, #255
	cmp	r1, #7
	subgt	r1, r1, #8
	andgt	r1, r1, #255
	movgt	r0, #32
	movle	r0, #0
	cmp	r1, #3
	subgt	r1, r1, #4
	andgt	r1, r1, #255
	addgt	r0, r0, #16
	andgt	r0, r0, #255
	cmp	r1, #1
	subgt	r1, r1, #2
	andgt	r1, r1, #255
	addgt	r0, r0, #8
	andgt	r0, r0, #255
	cmp	r1, #0
	addne	r0, r0, #4
	andne	r0, r0, #255
	cmp	r3, #127
	subgt	r3, r3, #128
	andgt	r3, r3, #255
	addgt	r0, r0, #2
	andgt	r0, r0, #255
	cmp	r3, #63
	subgt	r3, r3, #64
	andgt	r3, r3, #255
	addgt	r0, r0, #1
	andgt	r0, r0, #255
	cmp	r3, #31
	subgt	r3, r3, #32
	andgt	r3, r3, #255
	movgt	r1, #32
	movle	r1, #0
	cmp	r3, #15
	subgt	r3, r3, #16
	addgt	r1, r1, #16
	andgt	r3, r3, #255
	andgt	r1, r1, #255
	cmp	r3, #7
	subgt	r3, r3, #8
	addgt	r1, r1, #8
	andgt	r3, r3, #255
	andgt	r1, r1, #255
	cmp	r3, #3
	subgt	r3, r3, #4
	addgt	r1, r1, #4
	andgt	r3, r3, #255
	andgt	r1, r1, #255
	cmp	r3, #1
	subgt	r3, r3, #2
	addgt	r1, r1, #2
	andgt	r3, r3, #255
	andgt	r1, r1, #255
	cmp	r3, #0
	addne	r1, r1, #1
	andne	r1, r1, #255
	cmp	lr, #0
	addne	lr, lr, #32
	andne	lr, lr, #255
	moveq	lr, #96
	cmp	ip, #0
	addne	ip, ip, #32
	andne	ip, ip, #255
	moveq	ip, #96
	cmp	r0, #0
	addne	r0, r0, #32
	andne	r0, r0, #255
	moveq	r0, #96
	cmp	r1, #0
	addne	r1, r1, #32
	andne	r1, r1, #255
	moveq	r1, #96
	sub	r5, r5, #3
	add	r6, r6, #3
	strb	lr, [r7, #1]
	strb	ip, [r7, #2]
	strb	r0, [r7, #3]
	strb	r1, [r7, #4]!
.L46:
	cmp	r5, #0
	bne	.L75
	ldr	r3, .L114+116
	add	r0, sp, #152
	mul	r3, r4, r3
	ldr	r4, .L114
	add	r1, r0, r3
	mov	r0, #13
	add	r3, r3, #3
	strb	r0, [r1, #-139]
	cmp	r3, r2
	mov	r0, #10
	str	r8, [r4, #44]
	strb	r0, [r1, #-138]
	bne	.L76
	mov	r0, #2
	mov	r3, #1
	stmia	sp, {r0, r3}
	add	r1, sp, #12
	mov	r3, r5
	mov	r0, r5
	bl	AddCopyOfBlockToXmitList
	ldr	r3, [r4, #40]
	add	r3, r3, #1
	str	r3, [r4, #40]
	b	.L77
.L76:
	ldr	r0, .L114+120
	bl	Alert_Message
.L77:
.LBE7:
.LBE6:
	ldr	r3, .L114
	ldr	r2, [r3, #48]
	cmp	r2, #0
	beq	.L105
.L78:
	ldr	r6, [r3, #40]
	cmp	r6, #20
	moveq	r2, #130
	streq	r2, [r3, #16]
	beq	.L5
	ldr	r2, [r3, #52]
	cmp	r2, #1024
	bne	.L101
.L105:
	mov	r2, #130
.L104:
	str	r2, [r3, #16]
	b	.L101
.L112:
	ldr	r3, [r4, #44]
	mov	r1, #24
	ldr	r2, .L114+124
	add	r0, sp, #124
	bl	snprintf
	add	r0, sp, #124
	ldr	r1, .L114+128
	bl	__setup_for_normal_string_exchange
	mov	r3, #0
	str	r3, [r4, #44]
	str	r3, [r4, #40]
	ldr	r3, [r4, #48]
	cmp	r3, #0
	beq	.L80
	ldr	r3, [r4, #52]
	cmp	r3, #1024
	movne	r3, #120
	bne	.L103
.L80:
	ldr	r3, .L114
	mov	r2, #90
	str	r2, [r3, #16]
	mov	r2, #140
	str	r2, [r3, #28]
	b	.L101
.L19:
	ldr	r3, [r5, #0]
	cmp	r3, #2304
	bne	.L85
	ldr	r3, .L114+108
	mov	r1, #24
	str	r3, [sp, #0]
	ldr	r2, .L114+132
	ldr	r3, [r4, #32]
	add	r0, sp, #124
	bl	snprintf
	add	r0, sp, #124
	ldr	r1, .L114+92
	bl	__setup_for_normal_string_exchange
	ldr	r3, [r4, #32]
	add	r3, r3, #1024
	str	r3, [r4, #32]
	mov	r3, #0
	str	r3, [r4, #52]
	ldr	r3, [r4, #48]
	cmp	r3, #0
	bne	.L83
	ldr	r3, [r4, #56]
	cmp	r3, #0
	movne	r3, #150
	bne	.L103
	ldr	r2, [r4, #20]
	str	r3, [r4, #32]
	mov	r3, #1024
	str	r3, [r4, #48]
	mov	r3, #1
	str	r2, [r4, #36]
	str	r3, [r4, #56]
.L83:
	mov	r3, #110
	b	.L103
.L20:
	ldr	r3, [r5, #0]
	cmp	r3, #2304
	bne	.L85
	mov	r0, #0
	mov	r2, r0
	mov	r1, #100
	mov	r5, #0
	bl	RCVD_DATA_enable_hunting_mode
	str	r5, [r4, #4]
	bl	setup_to_check_if_tpmicro_needs_a_code_update
	ldr	r0, .L114+136
	bl	strlen
	mov	r1, #2
	mov	r3, #1
	stmia	sp, {r1, r3}
	ldr	r1, .L114+136
	mov	r3, r5
	mov	r2, r0
	mov	r0, r5
	bl	AddCopyOfBlockToXmitList
	ldr	r0, [r4, #20]
	cmp	r0, r5
	beq	.L86
	ldr	r1, .L114+12
	ldr	r2, .L114+140
	bl	mem_free_debug
	str	r5, [r4, #20]
.L86:
	bl	restore_priority_of_ISP_related_code_update_tasks
	mov	r0, #0
	bl	CODE_DOWNLOAD_close_dialog
	ldr	r0, .L114+144
	bl	Alert_Message
	ldr	r0, .L114+144
	bl	Alert_message_on_tpmicro_pile_M
	ldr	r6, .L114+148
	b	.L5
.L85:
	mov	r3, #900
.L103:
	str	r3, [r4, #16]
	b	.L101
.L113:
	ldr	r0, [r4, #20]
	cmp	r0, #0
	beq	.L87
	ldr	r1, .L114+12
	ldr	r2, .L114+152
	bl	mem_free_debug
	mov	r3, #0
	str	r3, [r4, #20]
.L87:
	bl	restore_priority_of_ISP_related_code_update_tasks
	mov	r0, #0
	mov	r2, r0
	mov	r1, #100
	bl	RCVD_DATA_enable_hunting_mode
	ldr	r3, .L114
	mov	r2, #0
	str	r2, [r3, #4]
	bl	setup_to_check_if_tpmicro_needs_a_code_update
.L101:
	mov	r6, #20
.L5:
	mov	r1, #5
	mov	r0, r6
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L114
	mov	r1, #2
	mov	r2, r0
	ldr	r0, [r3, #8]
	mov	r3, #0
	bl	xTimerGenericCommand
.L4:
	add	sp, sp, #152
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L115:
	.align	2
.L114:
	.word	tpmicro_comm
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LC0
	.word	489
	.word	.LC1
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LANCHOR4
	.word	.LANCHOR5
	.word	.LANCHOR6
	.word	.LANCHOR7
	.word	.LANCHOR8
	.word	.LANCHOR9
	.word	.LANCHOR10
	.word	.LC2
	.word	TPMICRO_APP_FILENAME
	.word	.LC4
	.word	.LC3
	.word	weather_preserves
	.word	746
	.word	cdcs
	.word	.LANCHOR11
	.word	.LANCHOR12
	.word	.LANCHOR13
	.word	.LANCHOR14
	.word	.LC5
	.word	268435968
	.word	.LC6
	.word	-1431655764
	.word	.LC7
	.word	.LC8
	.word	.LANCHOR15
	.word	.LC9
	.word	.LANCHOR16
	.word	1105
	.word	.LC10
	.word	1500
	.word	1141
.LFE2:
	.size	TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg, .-TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg
	.global	go_str
	.global	command_success_str
	.global	erase_str
	.global	prepare_str
	.global	unlock_str
	.global	esc_str
	.global	ok_str
	.global	part_id_str_response
	.global	part_id_str
	.global	echo_off_str_response
	.global	echo_off_str
	.global	frequency_str_response
	.global	frequency_str
	.global	synchronized_str_response
	.global	synchronized_str
	.global	question_mark_str_response
	.global	question_mark_str
	.section	.rodata.unlock_str,"a",%progbits
	.set	.LANCHOR11,. + 0
	.type	unlock_str, %object
	.size	unlock_str, 10
unlock_str:
	.ascii	"U 23130\015\012\000"
	.section	.rodata.erase_str,"a",%progbits
	.set	.LANCHOR14,. + 0
	.type	erase_str, %object
	.size	erase_str, 9
erase_str:
	.ascii	"E 0 21\015\012\000"
	.section	.rodata.ok_str,"a",%progbits
	.set	.LANCHOR15,. + 0
	.type	ok_str, %object
	.size	ok_str, 5
ok_str:
	.ascii	"OK\015\012\000"
	.section	.rodata.prepare_str,"a",%progbits
	.set	.LANCHOR13,. + 0
	.type	prepare_str, %object
	.size	prepare_str, 9
prepare_str:
	.ascii	"P 0 21\015\012\000"
	.section	.rodata.frequency_str_response,"a",%progbits
	.set	.LANCHOR6,. + 0
	.type	frequency_str_response, %object
	.size	frequency_str_response, 12
frequency_str_response:
	.ascii	"12000\015\012OK\015\012\000"
	.section	.rodata.esc_str,"a",%progbits
	.set	.LANCHOR4,. + 0
	.type	esc_str, %object
	.size	esc_str, 2
esc_str:
	.ascii	"\033\000"
	.section	.rodata.part_id_str_response,"a",%progbits
	.set	.LANCHOR10,. + 0
	.type	part_id_str_response, %object
	.size	part_id_str_response, 15
part_id_str_response:
	.ascii	"0\015\012637607987\015\012\000"
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/tpmicro_isp_mode.c\000"
.LC1:
	.ascii	"TPMicro comm unexpd event : %s, %u\000"
.LC2:
	.ascii	"TPMicro ISP executing ... \000"
.LC3:
	.ascii	"Unexp'd file read result.\000"
.LC4:
	.ascii	"File read problem.\000"
.LC5:
	.ascii	"W %d 1024\015\012\000"
.LC6:
	.ascii	"W %d %d\015\012\000"
.LC7:
	.ascii	"UUENCODE length mismatch\000"
.LC8:
	.ascii	"%d\015\012\000"
.LC9:
	.ascii	"C %d %d 1024\015\012\000"
.LC10:
	.ascii	"TPMicro code ISP update completed\000"
	.section	.rodata.go_str,"a",%progbits
	.set	.LANCHOR16,. + 0
	.type	go_str, %object
	.size	go_str, 8
go_str:
	.ascii	"G 0 T\015\012\000"
	.section	.rodata.command_success_str,"a",%progbits
	.set	.LANCHOR12,. + 0
	.type	command_success_str, %object
	.size	command_success_str, 4
command_success_str:
	.ascii	"0\015\012\000"
	.section	.rodata.part_id_str,"a",%progbits
	.set	.LANCHOR9,. + 0
	.type	part_id_str, %object
	.size	part_id_str, 4
part_id_str:
	.ascii	"J\015\012\000"
	.section	.rodata.synchronized_str_response,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	synchronized_str_response, %object
	.size	synchronized_str_response, 19
synchronized_str_response:
	.ascii	"Synchronized\015\012OK\015\012\000"
	.section	.rodata.echo_off_str_response,"a",%progbits
	.set	.LANCHOR8,. + 0
	.type	echo_off_str_response, %object
	.size	echo_off_str_response, 9
echo_off_str_response:
	.ascii	"A 0\015\0120\015\012\000"
	.section	.rodata.question_mark_str,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	question_mark_str, %object
	.size	question_mark_str, 2
question_mark_str:
	.ascii	"?\000"
	.section	.rodata.frequency_str,"a",%progbits
	.set	.LANCHOR5,. + 0
	.type	frequency_str, %object
	.size	frequency_str, 8
frequency_str:
	.ascii	"12000\015\012\000"
	.section	.rodata.question_mark_str_response,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	question_mark_str_response, %object
	.size	question_mark_str_response, 15
question_mark_str_response:
	.ascii	"Synchronized\015\012\000"
	.section	.rodata.synchronized_str,"a",%progbits
	.set	.LANCHOR2,. + 0
	.type	synchronized_str, %object
	.size	synchronized_str, 15
synchronized_str:
	.ascii	"Synchronized\015\012\000"
	.section	.rodata.echo_off_str,"a",%progbits
	.set	.LANCHOR7,. + 0
	.type	echo_off_str, %object
	.size	echo_off_str, 6
echo_off_str:
	.ascii	"A 0\015\012\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0xb0
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/tpmicro_isp_mode.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x4f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF0
	.byte	0x1
	.4byte	.LASF1
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x129
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0x4e
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x196
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x3
	.byte	0x7d
	.sleb128 176
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"__setup_for_normal_string_exchange\000"
.LASF4:
	.ascii	"TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg\000"
.LASF3:
	.ascii	"__uuencode_and_send_up_to_next_45_bytes\000"
.LASF1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/tpmicro_isp_mode.c\000"
.LASF0:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
