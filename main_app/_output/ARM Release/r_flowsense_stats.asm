	.file	"r_flowsense_stats.c"
	.text
.Ltext0:
	.global	__udivsi3
	.section	.text.FDTO_FLOWSENSE_STATS_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_FLOWSENSE_STATS_draw_report
	.type	FDTO_FLOWSENSE_STATS_draw_report, %function
FDTO_FLOWSENSE_STATS_draw_report:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI0:
	mov	r2, r0
	bne	.L2
	mov	r0, #78
	mvn	r1, #0
	bl	GuiLib_ShowScreen
.L2:
	ldr	r3, .L8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L8+4
	mov	r3, #38
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L8+8
	ldr	r6, .L8+12
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L8+4
	mov	r3, #40
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [r6, #0]
	ldr	r3, .L8+16
	ldr	r5, .L8+20
	str	r2, [r3, #0]
	ldr	r2, [r6, #12]
	ldr	r3, .L8+24
	ldr	r4, .L8+28
	str	r2, [r3, #0]
	ldr	r2, [r6, #4]
	ldr	r3, .L8+32
	ldr	r1, .L8+36
	str	r2, [r3, #0]
	ldr	r2, [r6, #8]
	ldr	r3, .L8+40
	str	r2, [r3, #0]
	ldr	r2, [r6, #16]
	ldr	r3, .L8+44
	str	r2, [r3, #0]
	ldr	r2, [r6, #28]
	ldr	r3, .L8+48
	str	r2, [r3, #0]
	ldr	r2, [r6, #20]
	ldr	r3, .L8+52
	str	r2, [r3, #0]
	ldr	r2, [r6, #24]
	ldr	r3, .L8+56
	str	r2, [r3, #0]
	ldr	r2, [r5, #16]
	ldr	r3, .L8+60
	str	r2, [r3, #0]
	ldr	r2, [r5, #108]
	ldr	r3, .L8+64
	str	r2, [r3, #0]
	ldr	r2, [r5, #200]
	ldr	r3, .L8+68
	str	r2, [r3, #0]
	ldr	r2, [r5, #292]
	ldr	r3, .L8+72
	str	r2, [r3, #0]
	ldr	r2, [r5, #384]
	ldr	r3, .L8+76
	str	r2, [r3, #0]
	ldr	r2, [r5, #476]
	ldr	r3, .L8+80
	str	r2, [r3, #0]
	ldr	r2, [r5, #568]
	ldr	r3, .L8+84
	str	r2, [r3, #0]
	ldr	r2, [r5, #660]
	ldr	r3, .L8+88
	str	r2, [r3, #0]
	ldr	r2, [r5, #752]
	ldr	r3, .L8+92
	str	r2, [r3, #0]
	ldr	r2, [r5, #844]
	ldr	r3, .L8+96
	ldr	r8, [r4, #0]
	str	r2, [r3, #0]
	ldr	r2, [r5, #936]
	ldr	r3, .L8+100
	ldr	r7, [r4, #4]
	str	r2, [r3, #0]
	ldr	r2, [r5, #1028]
	ldr	r3, .L8+104
	mov	r0, r7
	str	r2, [r3, #0]
	ldr	r2, [r4, #168]
	ldr	r3, .L8+108
	str	r2, [r3, #0]
	ldr	r2, [r4, #172]
	ldr	r3, .L8+112
	str	r2, [r3, #0]
	ldr	r2, [r4, #176]
	ldr	r3, .L8+116
	str	r2, [r3, #0]
	ldr	r2, [r4, #180]
	ldr	r3, .L8+120
	str	r2, [r3, #0]
	ldr	r2, [r4, #184]
	ldr	r3, .L8+124
	str	r2, [r3, #0]
	ldr	r2, [r4, #188]
	ldr	r3, .L8+128
	str	r2, [r3, #0]
	ldr	r2, [r4, #192]
	ldr	r3, .L8+132
	str	r2, [r3, #0]
	ldr	r2, [r4, #196]
	ldr	r3, .L8+136
	str	r2, [r3, #0]
	ldr	r2, [r4, #200]
	ldr	r3, .L8+140
	str	r2, [r3, #0]
	ldr	r2, [r4, #204]
	ldr	r3, .L8+144
	str	r2, [r3, #0]
	ldr	r2, [r4, #208]
	ldr	r3, .L8+148
	str	r2, [r3, #0]
	ldr	r2, [r4, #212]
	ldr	r3, .L8+152
	str	r2, [r3, #0]
	ldr	r3, .L8+156
	str	r8, [r3, #0]
	bl	__udivsi3
	ldr	r3, .L8+160
	ldr	r2, [r4, #8]
	cmp	r8, #2
	str	r0, [r3, #0]
	ldr	r3, .L8+164
	str	r2, [r3, #0]
	ldr	r3, .L8+168
	ldr	r2, .L8+172
	ldrh	r3, [r3, #0]
	str	r3, [r2, #0]
	mov	r2, #92
	mla	r5, r2, r3, r5
	ldr	r3, .L8+176
	ldr	r2, [r5, #20]
	str	r2, [r3, #0]
	ldr	r2, [r6, #32]
	ldr	r3, .L8+180
	str	r2, [r3, #0]
	bne	.L5
	ldr	r3, .L8+184
	cmp	r7, r3
	bne	.L5
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	beq	.L3
	ldr	r0, [r4, #8]
	rsbs	r0, r0, #1
	movcc	r0, #0
	b	.L3
.L5:
	mov	r0, #0
.L3:
	ldr	r3, .L8+188
	str	r0, [r3, #0]
	ldr	r3, .L8+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	GuiLib_Refresh
.L9:
	.align	2
.L8:
	.word	chain_members_recursive_MUTEX
	.word	.LC0
	.word	comm_mngr_recursive_MUTEX
	.word	comm_stats
	.word	GuiVar_ChainStatsFOALChainTGen
	.word	chain
	.word	GuiVar_ChainStatsFOALChainRRcvd
	.word	comm_mngr
	.word	GuiVar_ChainStatsIRRIChainTRcvd
	.word	1111
	.word	GuiVar_ChainStatsIRRIChainRGen
	.word	GuiVar_ChainStatsFOALIrriTGen
	.word	GuiVar_ChainStatsFOALIrriRRcvd
	.word	GuiVar_ChainStatsIRRIIrriTRcvd
	.word	GuiVar_ChainStatsIRRIIrriRGen
	.word	GuiVar_ChainStatsInUseA
	.word	GuiVar_ChainStatsInUseB
	.word	GuiVar_ChainStatsInUseC
	.word	GuiVar_ChainStatsInUseD
	.word	GuiVar_ChainStatsInUseE
	.word	GuiVar_ChainStatsInUseF
	.word	GuiVar_ChainStatsInUseG
	.word	GuiVar_ChainStatsInUseH
	.word	GuiVar_ChainStatsInUseI
	.word	GuiVar_ChainStatsInUseJ
	.word	GuiVar_ChainStatsInUseK
	.word	GuiVar_ChainStatsInUseL
	.word	GuiVar_ChainStatsErrorsA
	.word	GuiVar_ChainStatsErrorsB
	.word	GuiVar_ChainStatsErrorsC
	.word	GuiVar_ChainStatsErrorsD
	.word	GuiVar_ChainStatsErrorsE
	.word	GuiVar_ChainStatsErrorsF
	.word	GuiVar_ChainStatsErrorsG
	.word	GuiVar_ChainStatsErrorsH
	.word	GuiVar_ChainStatsErrorsI
	.word	GuiVar_ChainStatsErrorsJ
	.word	GuiVar_ChainStatsErrorsK
	.word	GuiVar_ChainStatsErrorsL
	.word	GuiVar_ChainStatsCommMngrMode
	.word	GuiVar_ChainStatsCommMngrState
	.word	GuiVar_ChainStatsInForced
	.word	next_contact
	.word	GuiVar_ChainStatsNextContact
	.word	GuiVar_ChainStatsNextContactSerNum
	.word	GuiVar_ChainStatsScans
	.word	2222
	.word	GuiVar_ChainStatsPresentlyMakingTokens
.LFE0:
	.size	FDTO_FLOWSENSE_STATS_draw_report, .-FDTO_FLOWSENSE_STATS_draw_report
	.section	.text.FLOWSENSE_STATS_process_report,"ax",%progbits
	.align	2
	.global	FLOWSENSE_STATS_process_report
	.type	FLOWSENSE_STATS_process_report, %function
FLOWSENSE_STATS_process_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #67
	ldreq	r3, .L13
	moveq	r2, #11
	streq	r2, [r3, #0]
	b	KEY_process_global_keys
.L14:
	.align	2
.L13:
	.word	GuiVar_MenuScreenToShow
.LFE1:
	.size	FLOWSENSE_STATS_process_report, .-FLOWSENSE_STATS_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_flowsense_stats.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_flowsense_stats.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x45
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x1c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x6c
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"FLOWSENSE_STATS_process_report\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_flowsense_stats.c\000"
.LASF0:
	.ascii	"FDTO_FLOWSENSE_STATS_draw_report\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
