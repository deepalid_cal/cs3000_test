	.file	"code_distribution_task.c"
	.text
.Ltext0:
	.section	.text.CODE_DOWNLOAD_draw_tp_micro_update_dialog,"ax",%progbits
	.align	2
	.global	CODE_DOWNLOAD_draw_tp_micro_update_dialog
	.type	CODE_DOWNLOAD_draw_tp_micro_update_dialog, %function
CODE_DOWNLOAD_draw_tp_micro_update_dialog:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	mov	r2, #100
	str	r2, [r3, #0]
	ldr	r3, .L2+4
	mov	r2, #200
	str	r2, [r3, #0]
	ldr	r3, .L2+8
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L2+12
	mov	r2, #5
	mov	r0, #600
	str	r2, [r3, #0]
	b	DIALOG_draw_ok_dialog
.L3:
	.align	2
.L2:
	.word	GuiVar_CodeDownloadPercentComplete
	.word	GuiVar_CodeDownloadProgressBar
	.word	GuiVar_CodeDownloadUpdatingTPMicro
	.word	GuiVar_CodeDownloadState
.LFE0:
	.size	CODE_DOWNLOAD_draw_tp_micro_update_dialog, .-CODE_DOWNLOAD_draw_tp_micro_update_dialog
	.section	.text.CODE_DOWNLOAD_draw_dialog,"ax",%progbits
	.align	2
	.global	CODE_DOWNLOAD_draw_dialog
	.type	CODE_DOWNLOAD_draw_dialog, %function
CODE_DOWNLOAD_draw_dialog:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L7
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L7+4
	cmp	r0, #1
	str	r3, [r2, #0]
	ldr	r2, .L7+8
	movne	r0, #1
	str	r3, [r2, #0]
	ldr	r3, .L7+12
	moveq	r0, #2
	str	r0, [r3, #0]
	mov	r0, #600
	b	DIALOG_draw_ok_dialog
.L8:
	.align	2
.L7:
	.word	GuiVar_CodeDownloadPercentComplete
	.word	GuiVar_CodeDownloadProgressBar
	.word	GuiVar_CodeDownloadUpdatingTPMicro
	.word	GuiVar_CodeDownloadState
.LFE1:
	.size	CODE_DOWNLOAD_draw_dialog, .-CODE_DOWNLOAD_draw_dialog
	.global	__udivsi3
	.section	.text.FDTO_CODE_DOWNLOAD_update_dialog,"ax",%progbits
	.align	2
	.global	FDTO_CODE_DOWNLOAD_update_dialog
	.type	FDTO_CODE_DOWNLOAD_update_dialog, %function
FDTO_CODE_DOWNLOAD_update_dialog:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L13
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r2, [r3, #0]
	ldr	r4, .L13+4
	add	r2, r2, #1
	and	r2, r2, #3
	str	r2, [r3, #0]
	ldr	r3, .L13+8
	ldr	r3, [r3, #0]
	cmp	r3, #2
	bne	.L10
	ldr	r3, .L13+12
	mov	r0, #100
	ldr	r2, [r3, #216]
	ldr	r1, [r3, #212]
	mul	r0, r2, r0
	b	.L12
.L10:
	cmp	r3, #1
	bne	.L11
	ldr	r3, .L13+12
	mov	r0, #100
	ldr	r2, [r3, #160]
	ldrh	r1, [r3, #156]
	mul	r0, r2, r0
.L12:
	bl	__udivsi3
	str	r0, [r4, #0]
.L11:
	ldr	r3, .L13+4
	ldr	r2, [r3, #0]
	ldr	r3, .L13+16
	mov	r2, r2, asl #1
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	FDTO_DIALOG_redraw_ok_dialog
.L14:
	.align	2
.L13:
	.word	GuiVar_SpinnerPos
	.word	GuiVar_CodeDownloadPercentComplete
	.word	GuiVar_CodeDownloadState
	.word	.LANCHOR0
	.word	GuiVar_CodeDownloadProgressBar
.LFE2:
	.size	FDTO_CODE_DOWNLOAD_update_dialog, .-FDTO_CODE_DOWNLOAD_update_dialog
	.section	.text.CODE_DOWNLOAD_close_dialog,"ax",%progbits
	.align	2
	.global	CODE_DOWNLOAD_close_dialog
	.type	CODE_DOWNLOAD_close_dialog, %function
CODE_DOWNLOAD_close_dialog:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L24
	mov	r2, #100
	str	lr, [sp, #-4]!
.LCFI1:
	str	r2, [r3, #0]
	ldr	r3, .L24+4
	mov	r2, #200
	str	r2, [r3, #0]
	ldr	r3, .L24+8
	sub	sp, sp, #36
.LCFI2:
	ldr	r2, [r3, #0]
	cmp	r2, #1
	moveq	r2, #3
	beq	.L23
	cmp	r2, #2
	movne	r2, #0
	moveq	r2, #4
.L23:
	cmp	r0, #1
	str	r2, [r3, #0]
	bne	.L20
.LBB2:
	ldr	r3, .L24+12
	str	r0, [sp, #0]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	b	.L15
.L20:
.LBE2:
	bl	DIALOG_close_ok_dialog
.L15:
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L25:
	.align	2
.L24:
	.word	GuiVar_CodeDownloadPercentComplete
	.word	GuiVar_CodeDownloadProgressBar
	.word	GuiVar_CodeDownloadState
	.word	FDTO_DIALOG_redraw_ok_dialog
.LFE3:
	.size	CODE_DOWNLOAD_close_dialog, .-CODE_DOWNLOAD_close_dialog
	.section	.text.CODE_DISTRIBUTION_there_are_3000s_on_the_hub_list,"ax",%progbits
	.align	2
	.global	CODE_DISTRIBUTION_there_are_3000s_on_the_hub_list
	.type	CODE_DISTRIBUTION_there_are_3000s_on_the_hub_list, %function
CODE_DISTRIBUTION_there_are_3000s_on_the_hub_list:
.LFB4:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L35
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, sl, lr}
.LCFI3:
	ldr	r6, .L35+4
	mov	r1, #0
	ldr	r0, [r3, #0]
	mvn	r2, #0
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, [r6, #0]
	bl	uxQueueMessagesWaiting
	mov	r4, #0
	mov	r7, r4
	mov	r5, r4
	mov	sl, r6
	mov	r8, r0
	b	.L27
.L32:
	mov	r2, #0
	ldr	r0, [r6, #0]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L28
	cmp	r5, #0
	bne	.L29
	ldr	r3, [sp, #0]
	cmn	r3, #1
	eorne	r5, r7, #1
	moveq	r7, #1
.L29:
	mov	r2, #0
	ldr	r0, [sl, #0]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericSend
	b	.L31
.L28:
	ldr	r0, .L35+8
	bl	Alert_Message
.L31:
	add	r4, r4, #1
.L27:
	cmp	r4, r8
	bne	.L32
	ldr	r3, .L35
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, pc}
.L36:
	.align	2
.L35:
	.word	router_hub_list_MUTEX
	.word	router_hub_list_queue
	.word	.LC0
.LFE4:
	.size	CODE_DISTRIBUTION_there_are_3000s_on_the_hub_list, .-CODE_DISTRIBUTION_there_are_3000s_on_the_hub_list
	.section	.text.CODE_DISTRIBUTION_try_to_start_a_code_distribution,"ax",%progbits
	.align	2
	.global	CODE_DISTRIBUTION_try_to_start_a_code_distribution
	.type	CODE_DISTRIBUTION_try_to_start_a_code_distribution, %function
CODE_DISTRIBUTION_try_to_start_a_code_distribution:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L64
	stmfd	sp!, {r4, lr}
.LCFI4:
	ldr	r2, .L64+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #292
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, .L64+8
	ldr	r3, [r2, #0]
	cmp	r3, #0
	bne	.L54
	ldr	r1, .L64+12
	ldr	r1, [r1, #0]
	cmp	r1, #0
	movne	r4, r3
	bne	.L38
	mov	r3, #32
	sub	r4, r4, #4
	str	r3, [r2, #232]
	cmp	r4, #3
	ldrls	pc, [pc, r4, asl #2]
	b	.L54
.L42:
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L41
.L39:
	mov	r2, #4
	b	.L62
.L40:
	mov	r2, #5
	b	.L63
.L41:
	bl	CONFIG_this_controller_is_a_configured_hub
	ldr	r3, .L64+8
	subs	r4, r0, #0
	streq	r4, [r3, #0]
	beq	.L59
	ldr	r4, [r3, #268]
	cmp	r4, #0
	beq	.L38
	ldr	r4, [r3, #264]
	cmp	r4, #0
	beq	.L38
	ldr	r3, .L64+16
	ldr	r1, [r3, #84]
	cmp	r1, #1
	bne	.L44
	ldrb	r2, [r3, #53]	@ zero_extendqisi2
	and	r2, r2, #3
	cmp	r2, #1
	beq	.L44
	ldr	r0, .L64+20
	bl	Alert_Message_va
	mov	r4, #0
	ldr	r3, .L64+24
	b	.L61
.L44:
	bl	CODE_DISTRIBUTION_there_are_3000s_on_the_hub_list
	subs	r4, r0, #0
	beq	.L59
	ldr	r2, .L64+16
	ldr	r3, .L64+8
	ldr	r1, [r2, #84]
	cmp	r1, #2
	bne	.L46
	ldrb	r2, [r2, #53]	@ zero_extendqisi2
	tst	r2, #4
	ldrne	r2, .L64+28
	ldreq	r2, .L64+32
	b	.L58
.L46:
	cmp	r1, #1
	ldreq	r2, .L64+36
	beq	.L58
	ldrb	r2, [r2, #53]	@ zero_extendqisi2
	tst	r2, #8
	ldrne	r2, .L64+40
	ldreq	r2, .L64+44
.L58:
	str	r2, [r3, #252]
	ldr	r3, .L64+24
	ldr	r2, [r3, #140]
	cmp	r2, #0
	beq	.L51
	mov	r2, #6
.L62:
	ldr	r3, .L64+8
	ldr	r0, .L64+48
	str	r2, [r3, #0]
	b	.L60
.L51:
	ldr	r4, [r3, #136]
	cmp	r4, #0
	beq	.L38
	mov	r2, #7
.L63:
	ldr	r3, .L64+8
	ldr	r0, .L64+52
	str	r2, [r3, #0]
.L60:
	mov	r1, #77
	bl	FLASH_STORAGE_request_code_image_file_read
	mov	r4, #1
	b	.L38
.L59:
	ldr	r3, .L64+24
.L61:
	str	r4, [r3, #140]
	str	r4, [r3, #136]
	b	.L38
.L54:
	mov	r4, #0
.L38:
	ldr	r3, .L64
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L65:
	.align	2
.L64:
	.word	code_distribution_control_structure_recursive_MUTEX
	.word	.LC1
	.word	.LANCHOR0
	.word	in_device_exchange_hammer
	.word	config_c
	.word	.LC2
	.word	weather_preserves
	.word	17600
	.word	8800
	.word	4750
	.word	4650
	.word	2350
	.word	CS3000_APP_FILENAME
	.word	TPMICRO_APP_FILENAME
.LFE5:
	.size	CODE_DISTRIBUTION_try_to_start_a_code_distribution, .-CODE_DISTRIBUTION_try_to_start_a_code_distribution
	.section	.text.CODE_DISTRIBUTION_hub_should_report_busy_to_the_commserver,"ax",%progbits
	.align	2
	.global	CODE_DISTRIBUTION_hub_should_report_busy_to_the_commserver
	.type	CODE_DISTRIBUTION_hub_should_report_busy_to_the_commserver, %function
CODE_DISTRIBUTION_hub_should_report_busy_to_the_commserver:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L67
	ldr	r0, [r3, #0]
	sub	r0, r0, #6
	cmp	r0, #1
	movhi	r0, #0
	movls	r0, #1
	bx	lr
.L68:
	.align	2
.L67:
	.word	.LANCHOR0
.LFE6:
	.size	CODE_DISTRIBUTION_hub_should_report_busy_to_the_commserver, .-CODE_DISTRIBUTION_hub_should_report_busy_to_the_commserver
	.section	.text.CODE_DISTRIBUTION_comm_mngr_should_be_idle,"ax",%progbits
	.align	2
	.global	CODE_DISTRIBUTION_comm_mngr_should_be_idle
	.type	CODE_DISTRIBUTION_comm_mngr_should_be_idle, %function
CODE_DISTRIBUTION_comm_mngr_should_be_idle:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI5:
	bl	FLOWSENSE_we_are_the_master_of_a_multiple_controller_network
	subs	r4, r0, #0
	beq	.L70
	ldr	r3, .L74
	ldr	r4, [r3, #0]
	sub	r4, r4, #4
	cmp	r4, #1
	movhi	r4, #0
	movls	r4, #1
.L70:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L71
	bl	FLOWSENSE_get_controller_letter
	cmp	r0, #65
	beq	.L71
	ldr	r3, .L74
	ldr	r3, [r3, #0]
	cmp	r3, #2
	moveq	r4, #1
.L71:
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L75:
	.align	2
.L74:
	.word	.LANCHOR0
.LFE7:
	.size	CODE_DISTRIBUTION_comm_mngr_should_be_idle, .-CODE_DISTRIBUTION_comm_mngr_should_be_idle
	.section	.text.CODE_DISTRIBUTION_task,"ax",%progbits
	.align	2
	.global	CODE_DISTRIBUTION_task
	.type	CODE_DISTRIBUTION_task, %function
CODE_DISTRIBUTION_task:
.LFB8:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI6:
	ldr	r4, .L108
	mov	r3, #1
	str	r3, [r4, #188]
	mov	r3, #1000
	str	r3, [r4, #248]
	ldr	r3, .L108+4
	ldr	r5, .L108+8
	str	r3, [r4, #252]
	ldr	r3, .L108+12
	mov	r6, #0
	sub	sp, sp, #68
.LCFI7:
	mov	r2, r6
	rsb	r5, r5, r0
	str	r3, [sp, #0]
	mov	r0, r6
	mov	r3, r6
	mov	r1, #200
	str	r6, [r4, #0]
	str	r6, [r4, #264]
	str	r6, [r4, #268]
	str	r6, [r4, #192]
	bl	xTimerCreate
	ldr	r3, .L108+16
	mov	r2, r6
	str	r3, [sp, #0]
	ldr	r1, .L108+20
	mov	r3, r6
	mov	r5, r5, lsr #5
	mov	r8, r4
	str	r0, [r4, #260]
	mov	r0, r6
	bl	xTimerCreate
	ldr	r6, .L108+24
	str	r0, [r4, #184]
.L101:
	ldr	r0, [r6, #0]
	add	r1, sp, #44
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	mov	r7, r0
	bne	.L77
	ldr	r3, .L108+28
	ldr	r2, .L108+32
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L108+36
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [sp, #44]
	ldr	r2, .L108+40
	cmp	r3, r2
	beq	.L88
	bhi	.L89
	sub	r2, r2, #68
	cmp	r3, r2
	beq	.L80
	bhi	.L90
	sub	r2, r2, #34
	cmp	r3, r2
	beq	.L79
	add	r2, r2, #17
	cmp	r3, r2
	bne	.L78
	b	.L80
.L90:
	ldr	r2, .L108+44
	cmp	r3, r2
	beq	.L82
	add	r2, r2, #17
	cmp	r3, r2
	beq	.L83
	sub	r2, r2, #34
	cmp	r3, r2
	bne	.L78
	b	.L106
.L89:
	ldr	r2, .L108+48
	cmp	r3, r2
	streq	r7, [r4, #268]
	beq	.L92
	bhi	.L91
	sub	r2, r2, #51
	cmp	r3, r2
	ldreq	r3, [sp, #64]
	streq	r3, [r4, #248]
	beq	.L92
	add	r2, r2, #34
	cmp	r3, r2
	bne	.L78
	b	.L107
.L91:
	ldr	r2, .L108+52
	cmp	r3, r2
	beq	.L88
	add	r2, r2, #17
	cmp	r3, r2
	beq	.L88
	sub	r2, r2, #34
	cmp	r3, r2
	bne	.L78
	b	.L88
.L79:
	add	r0, sp, #44
	bl	process_incoming_packet
	b	.L92
.L107:
	str	r7, [r4, #264]
	b	.L92
.L106:
	mov	r3, #2816
	str	r3, [sp, #4]
	add	r3, sp, #52
	ldmia	r3, {r2-r3}
	add	r0, sp, #4
	str	r2, [sp, #20]
	str	r3, [sp, #24]
	bl	COMM_MNGR_post_event_with_details
	b	.L92
.L80:
	ldr	r3, .L108+56
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L93
	add	r0, sp, #44
	bl	build_and_send_code_transmission_init_packet
	b	.L92
.L93:
	ldr	r0, [sp, #52]
	cmp	r0, #0
	beq	.L92
	ldr	r1, .L108+32
	ldr	r2, .L108+60
	bl	mem_free_debug
	b	.L92
.L88:
	ldr	r3, .L108+56
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L92
	add	r0, sp, #44
	bl	perform_next_code_transmission_state_activity
	b	.L92
.L82:
	mov	r0, #800
	bl	vTaskDelay
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	moveq	r0, #32
	beq	.L102
	ldr	r3, [r4, #272]
	cmp	r3, #0
	moveq	r3, #40
	streq	r3, [r4, #272]
	ldr	r0, [r8, #272]
	b	.L102
.L83:
	ldr	r3, [r4, #192]
	cmp	r3, #0
	beq	.L96
	mov	r2, #0
	ldr	r0, [r4, #196]
	mov	r1, #33
	mov	r3, r2
	str	r2, [r4, #0]
	bl	CENT_COMM_build_and_send_ack_with_optional_job_number
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	beq	.L97
	ldr	r3, [r4, #188]
	cmp	r3, #0
	beq	.L97
	mov	r0, #0
	bl	CONTROLLER_INITIATED_attempt_to_build_and_send_the_no_more_messages_msg
.L97:
	ldr	r3, [r4, #188]
	cmp	r3, #0
	beq	.L96
	mov	r0, #800
	bl	vTaskDelay
.L96:
	ldr	r3, [r4, #188]
	cmp	r3, #0
	ldreq	r0, .L108+64
	beq	.L103
	ldr	r0, .L108+68
	bl	Alert_Message
	bl	CONFIG_this_controller_is_a_configured_hub
	cmp	r0, #0
	beq	.L99
	ldr	r3, [r4, #272]
	cmp	r3, #0
	moveq	r3, #39
	streq	r3, [r4, #272]
	ldr	r0, [r4, #272]
	b	.L102
.L99:
	mov	r0, #33
.L102:
	bl	SYSTEM_application_requested_restart
	b	.L92
.L78:
	ldr	r0, .L108+72
.L103:
	bl	Alert_Message
.L92:
	ldr	r3, .L108+28
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L77:
	bl	xTaskGetTickCount
	ldr	r3, .L108+76
	str	r0, [r3, r5, asl #2]
	b	.L101
.L109:
	.align	2
.L108:
	.word	.LANCHOR0
	.word	4750
	.word	Task_Table
	.word	code_distribution_packet_rate_timer_callback
	.word	code_receipt_error_timer_callback
	.word	6000
	.word	CODE_DISTRIBUTION_task_queue
	.word	code_distribution_control_structure_recursive_MUTEX
	.word	.LC1
	.word	639
	.word	4471
	.word	4437
	.word	4539
	.word	4573
	.word	in_device_exchange_hammer
	.word	702
	.word	.LC4
	.word	.LC3
	.word	.LC5
	.word	task_last_execution_stamp
.LFE8:
	.size	CODE_DISTRIBUTION_task, .-CODE_DISTRIBUTION_task
	.section	.text.CODE_DISTRIBUTION_post_event_with_details,"ax",%progbits
	.align	2
	.global	CODE_DISTRIBUTION_post_event_with_details
	.type	CODE_DISTRIBUTION_post_event_with_details, %function
CODE_DISTRIBUTION_post_event_with_details:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L112
	mov	r2, #0
	mov	r1, r0
	str	lr, [sp, #-4]!
.LCFI8:
	ldr	r0, [r3, #0]
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	ldreq	pc, [sp], #4
	ldr	r0, .L112+4
	ldr	lr, [sp], #4
	b	Alert_Message
.L113:
	.align	2
.L112:
	.word	CODE_DISTRIBUTION_task_queue
	.word	.LC6
.LFE9:
	.size	CODE_DISTRIBUTION_post_event_with_details, .-CODE_DISTRIBUTION_post_event_with_details
	.section	.text.CODE_DISTRIBUTION_post_event,"ax",%progbits
	.align	2
	.global	CODE_DISTRIBUTION_post_event
	.type	CODE_DISTRIBUTION_post_event, %function
CODE_DISTRIBUTION_post_event:
.LFB10:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI9:
	add	r3, sp, #24
	str	r0, [r3, #-24]!
	mov	r0, sp
	bl	CODE_DISTRIBUTION_post_event_with_details
	add	sp, sp, #24
	ldmfd	sp!, {pc}
.LFE10:
	.size	CODE_DISTRIBUTION_post_event, .-CODE_DISTRIBUTION_post_event
	.section	.text.CODE_DISTRIBUTION_stop_and_cleanup,"ax",%progbits
	.align	2
	.global	CODE_DISTRIBUTION_stop_and_cleanup
	.type	CODE_DISTRIBUTION_stop_and_cleanup, %function
CODE_DISTRIBUTION_stop_and_cleanup:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI10:
	ldr	r4, .L119
	mvn	r3, #0
	mov	r2, #0
	str	r3, [sp, #0]
	ldr	r0, [r4, #184]
	mov	r1, #1
	mov	r3, r2
	bl	xTimerGenericCommand
	ldr	r0, [r4, #176]
	cmp	r0, #0
	beq	.L116
	ldr	r1, .L119+4
	ldr	r2, .L119+8
	bl	mem_free_debug
	mov	r3, #0
	str	r3, [r4, #176]
.L116:
	ldr	r0, [r4, #172]
	cmp	r0, #0
	beq	.L117
	ldr	r2, .L119+12
	ldr	r1, .L119+4
	bl	mem_free_debug
	ldr	r3, .L119
	mov	r2, #0
	str	r2, [r3, #172]
.L117:
	ldr	r4, .L119
	ldr	r0, [r4, #204]
	cmp	r0, #0
	beq	.L118
	ldr	r1, .L119+4
	ldr	r2, .L119+16
	bl	mem_free_debug
	mov	r3, #0
	str	r3, [r4, #204]
.L118:
	mov	r3, #0
	str	r3, [r4, #0]
	strh	r3, [r4, #158]	@ movhi
	str	r3, [r4, #236]
	str	r3, [r4, #232]
	ldmfd	sp!, {r3, r4, pc}
.L120:
	.align	2
.L119:
	.word	.LANCHOR0
	.word	.LC1
	.word	923
	.word	931
	.word	939
.LFE11:
	.size	CODE_DISTRIBUTION_stop_and_cleanup, .-CODE_DISTRIBUTION_stop_and_cleanup
	.global	cdcs
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"CODE DIST: hub list ERROR\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/code_distribution_task.c\000"
.LC2:
	.ascii	"HUB WARNING: WITHOUT AN M7 - WILL NOT WORK!, %u, %u"
	.ascii	"\000"
.LC3:
	.ascii	"CODE RECEIPT: after tpmicro rebooting\000"
.LC4:
	.ascii	"CODE RECEIPT: after tpmicro not rebooting\000"
.LC5:
	.ascii	"unhandled code distrib event\000"
.LC6:
	.ascii	"Code Distrib queue overflow!\000"
	.section	.bss.cdcs,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	cdcs, %object
	.size	cdcs, 276
cdcs:
	.space	276
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI5-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI6-.LFB8
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI8-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI9-.LFB10
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI10-.LFB11
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE22:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/code_distribution_task.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x112
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF12
	.byte	0x1
	.4byte	.LASF13
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x43
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x55
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x7a
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x9d
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xd0
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x118
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1d1
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1e2
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST4
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x238
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST5
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x372
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST6
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x380
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST7
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x38f
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB7
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB8
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI7
	.4byte	.LFE8
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB9
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB10
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB11
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x74
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"FDTO_CODE_DOWNLOAD_update_dialog\000"
.LASF4:
	.ascii	"CODE_DISTRIBUTION_there_are_3000s_on_the_hub_list\000"
.LASF8:
	.ascii	"CODE_DISTRIBUTION_task\000"
.LASF3:
	.ascii	"CODE_DOWNLOAD_close_dialog\000"
.LASF0:
	.ascii	"CODE_DOWNLOAD_draw_tp_micro_update_dialog\000"
.LASF12:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF7:
	.ascii	"CODE_DISTRIBUTION_comm_mngr_should_be_idle\000"
.LASF6:
	.ascii	"CODE_DISTRIBUTION_hub_should_report_busy_to_the_com"
	.ascii	"mserver\000"
.LASF5:
	.ascii	"CODE_DISTRIBUTION_try_to_start_a_code_distribution\000"
.LASF9:
	.ascii	"CODE_DISTRIBUTION_post_event_with_details\000"
.LASF1:
	.ascii	"CODE_DOWNLOAD_draw_dialog\000"
.LASF11:
	.ascii	"CODE_DISTRIBUTION_stop_and_cleanup\000"
.LASF10:
	.ascii	"CODE_DISTRIBUTION_post_event\000"
.LASF13:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/code_distribution_task.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
