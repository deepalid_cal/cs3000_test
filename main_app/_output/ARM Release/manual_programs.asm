	.file	"manual_programs.c"
	.text
.Ltext0:
	.section	.text.nm_MANUAL_PROGRAMS_set_run_time,"ax",%progbits
	.align	2
	.type	nm_MANUAL_PROGRAMS_set_run_time, %function
nm_MANUAL_PROGRAMS_set_run_time:
.LFB4:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	sub	sp, sp, #44
.LCFI1:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	ldr	r1, .L2
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #144
	mov	r1, #600
	str	r3, [sp, #32]
	mov	r3, #5
	stmib	sp, {r1, r2}
	ldr	r2, .L2+4
	str	r3, [sp, #36]
	ldr	r3, .L2+8
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #132
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L3:
	.align	2
.L2:
	.word	28800
	.word	49275
	.word	.LC0
.LFE4:
	.size	nm_MANUAL_PROGRAMS_set_run_time, .-nm_MANUAL_PROGRAMS_set_run_time
	.section	.text.nm_MANUAL_PROGRAMS_set_in_use,"ax",%progbits
	.align	2
	.type	nm_MANUAL_PROGRAMS_set_in_use, %function
nm_MANUAL_PROGRAMS_set_in_use:
.LFB5:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI2:
	sub	sp, sp, #36
.LCFI3:
	str	r2, [sp, #0]
	ldr	r2, .L5
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #152
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #144
	str	r3, [sp, #24]
	mov	r3, #6
	str	r3, [sp, #28]
	ldr	r3, .L5+4
	str	r3, [sp, #32]
	mov	r3, #1
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L6:
	.align	2
.L5:
	.word	49416
	.word	.LC1
.LFE5:
	.size	nm_MANUAL_PROGRAMS_set_in_use, .-nm_MANUAL_PROGRAMS_set_in_use
	.section	.text.nm_MANUAL_PROGRAMS_set_start_date,"ax",%progbits
	.align	2
	.type	nm_MANUAL_PROGRAMS_set_start_date, %function
nm_MANUAL_PROGRAMS_set_start_date:
.LFB2:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI4:
	ldr	r6, .L8
	mov	r4, r0
	mov	r0, #1
	sub	sp, sp, #44
.LCFI5:
	mov	r9, r1
	mov	r5, r2
	mov	r1, r0
	mov	r2, r6
	mov	r7, r3
	bl	DMYToDate
	mov	r1, #12
	ldr	r2, .L8+4
	mov	sl, r0
	mov	r0, #31
	bl	DMYToDate
	mov	r2, r6
	mov	r8, r0
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	ldr	r2, .L8+8
	mov	r3, sl, asl #16
	str	r2, [sp, #12]
	ldr	r2, [sp, #76]
	mov	r8, r8, asl #16
	str	r2, [sp, #20]
	ldr	r2, [sp, #80]
	mov	r8, r8, lsr #16
	str	r2, [sp, #24]
	ldr	r2, [sp, #84]
	add	r1, r4, #124
	str	r2, [sp, #28]
	add	r2, r4, #144
	str	r2, [sp, #32]
	mov	r2, #3
	str	r2, [sp, #36]
	ldr	r2, .L8+12
	mov	r3, r3, lsr #16
	str	r2, [sp, #40]
	mov	r2, r9
	str	r8, [sp, #0]
	str	r7, [sp, #16]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	stmib	sp, {r0, r5}
	mov	r0, r4
	bl	SHARED_set_date_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L9:
	.align	2
.L8:
	.word	2011
	.word	2042
	.word	49273
	.word	.LC2
.LFE2:
	.size	nm_MANUAL_PROGRAMS_set_start_date, .-nm_MANUAL_PROGRAMS_set_start_date
	.section	.text.nm_MANUAL_PROGRAMS_set_end_date,"ax",%progbits
	.align	2
	.type	nm_MANUAL_PROGRAMS_set_end_date, %function
nm_MANUAL_PROGRAMS_set_end_date:
.LFB3:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI6:
	ldr	r6, .L11
	mov	r4, r0
	mov	r0, #1
	sub	sp, sp, #44
.LCFI7:
	mov	r9, r1
	mov	r5, r2
	mov	r1, r0
	mov	r2, r6
	mov	r7, r3
	bl	DMYToDate
	mov	r1, #12
	ldr	r2, .L11+4
	mov	sl, r0
	mov	r0, #31
	bl	DMYToDate
	mov	r2, r6
	mov	r8, r0
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	ldr	r2, .L11+8
	mov	r3, sl, asl #16
	str	r2, [sp, #12]
	ldr	r2, [sp, #76]
	mov	r8, r8, asl #16
	str	r2, [sp, #20]
	ldr	r2, [sp, #80]
	mov	r8, r8, lsr #16
	str	r2, [sp, #24]
	ldr	r2, [sp, #84]
	add	r1, r4, #128
	str	r2, [sp, #28]
	add	r2, r4, #144
	str	r2, [sp, #32]
	mov	r2, #4
	str	r2, [sp, #36]
	ldr	r2, .L11+12
	mov	r3, r3, lsr #16
	str	r2, [sp, #40]
	mov	r2, r9
	str	r8, [sp, #0]
	str	r7, [sp, #16]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	stmib	sp, {r0, r5}
	mov	r0, r4
	bl	SHARED_set_date_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L12:
	.align	2
.L11:
	.word	2011
	.word	2042
	.word	49274
	.word	.LC3
.LFE3:
	.size	nm_MANUAL_PROGRAMS_set_end_date, .-nm_MANUAL_PROGRAMS_set_end_date
	.section	.text.nm_manual_programs_updater,"ax",%progbits
	.align	2
	.type	nm_manual_programs_updater, %function
nm_manual_programs_updater:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI8:
	mov	r4, r0
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #3
	mov	r6, r0
	bne	.L14
	ldr	r0, .L29
	mov	r1, r4
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_Message_va
.L14:
	ldr	r0, .L29+4
	mov	r1, #3
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	bne	.L15
	ldr	r0, .L29+8
	bl	nm_ListGetFirst
	mvn	r4, #0
	mov	r1, r0
	b	.L16
.L17:
	str	r4, [r1, #144]
	ldr	r0, .L29+8
	bl	nm_ListGetNext
	mov	r1, r0
.L16:
	cmp	r1, #0
	bne	.L17
	b	.L18
.L15:
	cmp	r4, #1
	bne	.L19
.L18:
	ldr	r0, .L29+8
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L20
.L21:
	str	r4, [r1, #148]
	ldr	r0, .L29+8
	bl	nm_ListGetNext
	mov	r1, r0
.L20:
	cmp	r1, #0
	bne	.L21
	b	.L22
.L19:
	cmp	r4, #2
	bne	.L23
.L22:
	ldr	r0, .L29+8
	bl	nm_ListGetFirst
	mov	r5, #1
	mov	r4, r0
	b	.L24
.L25:
	add	r3, r4, #136
	mov	r0, r4
	str	r3, [sp, #8]
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	str	r6, [sp, #0]
	str	r5, [sp, #4]
	bl	nm_MANUAL_PROGRAMS_set_in_use
	mov	r1, r4
	ldr	r0, .L29+8
	bl	nm_ListGetNext
	mov	r4, r0
.L24:
	cmp	r4, #0
	bne	.L25
	b	.L28
.L23:
	ldr	r0, .L29+12
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_Message
.L28:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, pc}
.L30:
	.align	2
.L29:
	.word	.LC4
	.word	.LC5
	.word	.LANCHOR0
	.word	.LC6
.LFE8:
	.size	nm_manual_programs_updater, .-nm_manual_programs_updater
	.section	.text.nm_MANUAL_PROGRAMS_set_start_time,"ax",%progbits
	.align	2
	.type	nm_MANUAL_PROGRAMS_set_start_time, %function
nm_MANUAL_PROGRAMS_set_start_time:
.LFB0:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #5
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI9:
	mov	r5, r0
	sub	sp, sp, #76
.LCFI10:
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	bhi	.L32
.LBB4:
	add	r6, sp, #44
	add	r3, r1, #1
	str	r3, [sp, #0]
	mov	r0, r6
	mov	r1, #32
	ldr	r2, .L34
	ldr	r3, .L34+4
	bl	snprintf
	ldr	r3, .L34+8
	add	r1, r4, #18
	str	r3, [sp, #0]
	stmib	sp, {r3, r7}
	ldr	r3, [sp, #100]
	add	r4, r4, #49152
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	add	r4, r4, #108
	str	r3, [sp, #20]
	ldr	r3, [sp, #108]
	mov	r0, r5
	str	r3, [sp, #24]
	ldr	r3, [sp, #112]
	add	r1, r5, r1, asl #2
	str	r3, [sp, #28]
	add	r3, r5, #144
	str	r3, [sp, #32]
	mov	r3, #1
	str	r3, [sp, #36]
	mov	r2, r8
	mov	r3, #0
	str	r4, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	b	.L31
.L32:
.LBE4:
	ldr	r0, .L34+12
	mov	r1, #160
	bl	Alert_index_out_of_range_with_filename
.L31:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L35:
	.align	2
.L34:
	.word	.LC7
	.word	.LC8
	.word	86400
	.word	.LC9
.LFE0:
	.size	nm_MANUAL_PROGRAMS_set_start_time, .-nm_MANUAL_PROGRAMS_set_start_time
	.section	.text.nm_MANUAL_PROGRAMS_set_water_day,"ax",%progbits
	.align	2
	.type	nm_MANUAL_PROGRAMS_set_water_day, %function
nm_MANUAL_PROGRAMS_set_water_day:
.LFB1:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #6
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI11:
	mov	r5, r0
	sub	sp, sp, #68
.LCFI12:
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	bhi	.L37
.LBB7:
	add	r6, sp, #36
	add	r3, r1, #1
	str	r3, [sp, #0]
	mov	r0, r6
	mov	r1, #32
	ldr	r2, .L39
	ldr	r3, .L39+4
	bl	snprintf
	ldr	r3, [sp, #92]
	add	r1, r4, #24
	str	r3, [sp, #8]
	ldr	r3, [sp, #96]
	add	r4, r4, #49152
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	add	r4, r4, #114
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	mov	r0, r5
	str	r3, [sp, #20]
	add	r3, r5, #144
	str	r3, [sp, #24]
	mov	r3, #2
	str	r3, [sp, #28]
	add	r1, r5, r1, asl #2
	mov	r2, r8
	mov	r3, #0
	str	r7, [sp, #0]
	str	r4, [sp, #4]
	str	r6, [sp, #32]
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	b	.L36
.L37:
.LBE7:
	ldr	r0, .L39+8
	mov	r1, #212
	bl	Alert_index_out_of_range_with_filename
.L36:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L40:
	.align	2
.L39:
	.word	.LC7
	.word	.LC10
	.word	.LC9
.LFE1:
	.size	nm_MANUAL_PROGRAMS_set_water_day, .-nm_MANUAL_PROGRAMS_set_water_day
	.section	.text.nm_MANUAL_PROGRAMS_set_default_values,"ax",%progbits
	.align	2
	.type	nm_MANUAL_PROGRAMS_set_default_values, %function
nm_MANUAL_PROGRAMS_set_default_values:
.LFB11:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI13:
	sub	sp, sp, #24
.LCFI14:
	mov	r4, r0
	mov	r6, r1
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #0
	mov	r7, r0
	beq	.L42
	ldr	r8, .L48
	add	r5, r4, #136
	ldr	r1, [r8, #0]
	mov	r0, r5
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r8, #0]
	add	r0, r4, #140
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r8, #0]
	add	r0, r4, #148
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r8, #0]
	add	r0, r4, #144
	bl	SHARED_set_all_32_bit_change_bits
	mov	r8, #0
	mov	r9, #11
.L43:
	mov	r1, r8
	mov	r0, r4
	ldr	r2, .L48+4
	mov	r3, #0
	add	r8, r8, #1
	str	r9, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	r5, [sp, #12]
	bl	nm_MANUAL_PROGRAMS_set_start_time
	cmp	r8, #6
	mov	sl, #11
	bne	.L43
	mov	r1, #0
	mov	r3, r1
	mov	r0, r4
	ldr	r2, .L48+8
	str	sl, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	r5, [sp, #12]
	bl	nm_MANUAL_PROGRAMS_set_start_time
	mov	r0, r4
	mov	r1, #1
	ldr	r2, .L48+12
	mov	r3, #0
	str	sl, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	r5, [sp, #12]
	bl	nm_MANUAL_PROGRAMS_set_start_time
	mov	r0, r4
	mov	r1, #2
	ldr	r2, .L48+16
	mov	r3, #0
	str	sl, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	r5, [sp, #12]
	bl	nm_MANUAL_PROGRAMS_set_start_time
	mov	r8, #0
.L44:
	mov	r2, #0
	mov	r1, r8
	mov	r0, r4
	mov	r3, r2
	add	r8, r8, #1
	str	sl, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	r5, [sp, #12]
	bl	nm_MANUAL_PROGRAMS_set_water_day
	cmp	r8, #7
	bne	.L44
	add	r0, sp, #16
	bl	EPSON_obtain_latest_time_and_date
	ldrh	r1, [sp, #20]
	mov	r0, r4
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_MANUAL_PROGRAMS_set_start_date
	ldrh	r1, [sp, #20]
	mov	r0, r4
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_MANUAL_PROGRAMS_set_end_date
	mov	r0, r4
	mov	r1, #600
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_MANUAL_PROGRAMS_set_run_time
	mov	r0, r4
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_MANUAL_PROGRAMS_set_in_use
	b	.L41
.L42:
	ldr	r0, .L48+20
	ldr	r1, .L48+24
	bl	Alert_func_call_with_null_ptr_with_filename
.L41:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L49:
	.align	2
.L48:
	.word	list_program_data_recursive_MUTEX
	.word	86400
	.word	25200
	.word	39600
	.word	54000
	.word	.LC11
	.word	377
.LFE11:
	.size	nm_MANUAL_PROGRAMS_set_default_values, .-nm_MANUAL_PROGRAMS_set_default_values
	.section	.text.init_file_manual_programs,"ax",%progbits
	.align	2
	.global	init_file_manual_programs
	.type	init_file_manual_programs, %function
init_file_manual_programs:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI15:
	ldr	r3, .L51
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, .L51+4
	ldr	r1, .L51+8
	ldr	r3, [r3, #0]
	mov	r2, #3
	str	r3, [sp, #4]
	ldr	r3, .L51+12
	str	r3, [sp, #8]
	ldr	r3, .L51+16
	str	r3, [sp, #12]
	ldr	r3, .L51+20
	str	r3, [sp, #16]
	mov	r3, #12
	str	r3, [sp, #20]
	ldr	r3, .L51+24
	bl	FLASH_FILE_initialize_list_and_find_or_create_group_list_file
	add	sp, sp, #24
	ldmfd	sp!, {pc}
.L52:
	.align	2
.L51:
	.word	.LANCHOR2
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR1
	.word	nm_manual_programs_updater
	.word	nm_MANUAL_PROGRAMS_set_default_values
	.word	.LANCHOR3
	.word	.LANCHOR0
.LFE9:
	.size	init_file_manual_programs, .-init_file_manual_programs
	.section	.text.save_file_manual_programs,"ax",%progbits
	.align	2
	.global	save_file_manual_programs
	.type	save_file_manual_programs, %function
save_file_manual_programs:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #156
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI16:
	ldr	r1, .L54
	str	r3, [sp, #0]
	ldr	r3, .L54+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, #3
	str	r3, [sp, #4]
	mov	r3, #12
	str	r3, [sp, #8]
	ldr	r3, .L54+8
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L55:
	.align	2
.L54:
	.word	.LANCHOR1
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR0
.LFE10:
	.size	save_file_manual_programs, .-save_file_manual_programs
	.section	.text.nm_MANUAL_PROGRAMS_create_new_group,"ax",%progbits
	.align	2
	.global	nm_MANUAL_PROGRAMS_create_new_group
	.type	nm_MANUAL_PROGRAMS_create_new_group, %function
nm_MANUAL_PROGRAMS_create_new_group:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, lr}
.LCFI17:
	ldr	r0, .L61
	bl	nm_ListGetFirst
	b	.L60
.L59:
	ldr	r3, [r1, #152]
	cmp	r3, #0
	beq	.L58
	ldr	r0, .L61
	bl	nm_ListGetNext
.L60:
	cmp	r0, #0
	mov	r1, r0
	bne	.L59
.L58:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r1, [sp, #4]
	ldr	r2, .L61+4
	ldr	r1, .L61+8
	mov	r3, #156
	ldr	r0, .L61
	bl	nm_GROUP_create_new_group
	ldmfd	sp!, {r2, r3, pc}
.L62:
	.align	2
.L61:
	.word	.LANCHOR0
	.word	nm_MANUAL_PROGRAMS_set_default_values
	.word	.LANCHOR3
.LFE12:
	.size	nm_MANUAL_PROGRAMS_create_new_group, .-nm_MANUAL_PROGRAMS_create_new_group
	.section	.text.MANUAL_PROGRAMS_get_group_with_this_GID,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_get_group_with_this_GID
	.type	MANUAL_PROGRAMS_get_group_with_this_GID, %function
MANUAL_PROGRAMS_get_group_with_this_GID:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI18:
	ldr	r4, .L64
	ldr	r3, .L64+4
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L64+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L64+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L65:
	.align	2
.L64:
	.word	list_program_data_recursive_MUTEX
	.word	909
	.word	.LC11
	.word	.LANCHOR0
.LFE18:
	.size	MANUAL_PROGRAMS_get_group_with_this_GID, .-MANUAL_PROGRAMS_get_group_with_this_GID
	.section	.text.MANUAL_PROGRAMS_get_group_at_this_index,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_get_group_at_this_index
	.type	MANUAL_PROGRAMS_get_group_at_this_index, %function
MANUAL_PROGRAMS_get_group_at_this_index:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI19:
	ldr	r4, .L67
	ldr	r2, .L67+4
	mov	r3, #944
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L67+8
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L68:
	.align	2
.L67:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	.LANCHOR0
.LFE19:
	.size	MANUAL_PROGRAMS_get_group_at_this_index, .-MANUAL_PROGRAMS_get_group_at_this_index
	.global	__udivsi3
	.section	.text.MANUAL_PROGRAMS_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_copy_group_into_guivars
	.type	MANUAL_PROGRAMS_copy_group_into_guivars, %function
MANUAL_PROGRAMS_copy_group_into_guivars:
.LFB14:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI20:
	ldr	r5, .L70+4
	sub	sp, sp, #56
.LCFI21:
	ldr	r2, .L70+8
	ldr	r3, .L70+12
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	MANUAL_PROGRAMS_get_group_at_this_index
	mov	r4, r0
	bl	nm_GROUP_load_common_guivars
	ldr	r0, [r4, #72]
	mov	r1, #60
	bl	__udivsi3
	ldr	r3, .L70+16
	mov	r1, #60
	mov	r9, r0
	str	r0, [r3, #0]
	ldr	r0, [r4, #76]
	bl	__udivsi3
	ldr	r3, .L70+20
	mov	r1, #60
	mov	sl, r0
	str	r0, [r3, #0]
	ldr	r0, [r4, #80]
	bl	__udivsi3
	ldr	r3, .L70+24
	mov	r1, #60
	mov	r8, r0
	str	r0, [r3, #0]
	ldr	r0, [r4, #84]
	bl	__udivsi3
	ldr	r3, .L70+28
	mov	r1, #60
	mov	r7, r0
	str	r0, [r3, #0]
	ldr	r0, [r4, #88]
	bl	__udivsi3
	ldr	r3, .L70+32
	mov	r1, #60
	mov	r6, r0
	str	r0, [r3, #0]
	ldr	r0, [r4, #92]
	bl	__udivsi3
	ldr	r3, .L70+36
	ldr	r2, .L70+40
	mov	r1, #48
	str	r0, [r3, #0]
	ldr	r3, .L70+44
	cmp	r9, r3
	movhi	r9, #0
	movls	r9, #1
	str	r9, [r2, #0]
	ldr	r2, .L70+48
	cmp	sl, r3
	movhi	sl, #0
	movls	sl, #1
	str	sl, [r2, #0]
	ldr	r2, .L70+52
	cmp	r8, r3
	movhi	r8, #0
	movls	r8, #1
	str	r8, [r2, #0]
	ldr	r2, .L70+56
	cmp	r7, r3
	movhi	r7, #0
	movls	r7, #1
	str	r7, [r2, #0]
	ldr	r2, .L70+60
	cmp	r6, r3
	movhi	r6, #0
	movls	r6, #1
	str	r6, [r2, #0]
	ldr	r2, .L70+64
	cmp	r0, r3
	movhi	r3, #0
	movls	r3, #1
	str	r3, [r2, #0]
	ldr	r2, [r4, #96]
	ldr	r3, .L70+68
	ldr	r6, .L70+72
	str	r2, [r3, #0]
	ldr	r2, [r4, #100]
	ldr	r3, .L70+76
	mov	r7, #225
	str	r2, [r3, #0]
	ldr	r2, [r4, #104]
	ldr	r3, .L70+80
	add	r0, sp, #8
	str	r2, [r3, #0]
	ldr	r2, [r4, #108]
	ldr	r3, .L70+84
	str	r2, [r3, #0]
	ldr	r2, [r4, #112]
	ldr	r3, .L70+88
	str	r2, [r3, #0]
	ldr	r2, [r4, #116]
	ldr	r3, .L70+92
	str	r2, [r3, #0]
	ldr	r2, [r4, #120]
	ldr	r3, .L70+96
	str	r2, [r3, #0]
	ldr	r2, [r4, #124]
	ldr	r3, .L70+100
	str	r2, [r3, #0]
	ldr	r3, [r4, #128]
	str	r3, [r6, #0]
	mov	r3, #125
	str	r7, [sp, #0]
	bl	GetDateStr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L70+104
	bl	strlcpy
	str	r7, [sp, #0]
	mov	r3, #125
	ldr	r2, [r6, #0]
	add	r0, sp, #8
	mov	r1, #48
	bl	GetDateStr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L70+108
	bl	strlcpy
	flds	s15, [r4, #132]	@ int
	ldr	r3, .L70+112
	ldr	r2, [r4, #152]
	fuitos	s14, s15
	flds	s15, .L70
	ldr	r1, .L70+116
	ldr	r0, .L70+120
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	ldr	r3, .L70+124
	str	r2, [r3, #0]
	ldr	r2, .L70+128
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	ldr	r2, [r2, #0]
	bl	STATION_SELECTION_GRID_populate_cursors
	bl	STATION_SELECTION_GRID_get_count_of_selected_stations
	ldr	r3, .L70+132
	str	r0, [r3, #0]
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #56
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L71:
	.align	2
.L70:
	.word	1114636288
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	735
	.word	GuiVar_ManualPStartTime1
	.word	GuiVar_ManualPStartTime2
	.word	GuiVar_ManualPStartTime3
	.word	GuiVar_ManualPStartTime4
	.word	GuiVar_ManualPStartTime5
	.word	GuiVar_ManualPStartTime6
	.word	GuiVar_ManualPStartTime1Enabled
	.word	1439
	.word	GuiVar_ManualPStartTime2Enabled
	.word	GuiVar_ManualPStartTime3Enabled
	.word	GuiVar_ManualPStartTime4Enabled
	.word	GuiVar_ManualPStartTime5Enabled
	.word	GuiVar_ManualPStartTime6Enabled
	.word	GuiVar_ManualPWaterDay_Sun
	.word	.LANCHOR5
	.word	GuiVar_ManualPWaterDay_Mon
	.word	GuiVar_ManualPWaterDay_Tue
	.word	GuiVar_ManualPWaterDay_Wed
	.word	GuiVar_ManualPWaterDay_Thu
	.word	GuiVar_ManualPWaterDay_Fri
	.word	GuiVar_ManualPWaterDay_Sat
	.word	.LANCHOR4
	.word	GuiVar_ManualPStartDateStr
	.word	GuiVar_ManualPEndDateStr
	.word	GuiVar_ManualPRunTime
	.word	STATION_get_GID_manual_program_B
	.word	STATION_get_GID_manual_program_A
	.word	GuiVar_ManualPInUse
	.word	g_GROUP_ID
	.word	GuiVar_StationSelectionGridStationCount
.LFE14:
	.size	MANUAL_PROGRAMS_copy_group_into_guivars, .-MANUAL_PROGRAMS_copy_group_into_guivars
	.section	.text.MANUAL_PROGRAMS_get_index_for_group_with_this_GID,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_get_index_for_group_with_this_GID
	.type	MANUAL_PROGRAMS_get_index_for_group_with_this_GID, %function
MANUAL_PROGRAMS_get_index_for_group_with_this_GID:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI22:
	ldr	r4, .L73
	ldr	r2, .L73+4
	mov	r3, #980
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L73+8
	bl	nm_GROUP_get_index_for_group_with_this_GID
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L74:
	.align	2
.L73:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	.LANCHOR0
.LFE20:
	.size	MANUAL_PROGRAMS_get_index_for_group_with_this_GID, .-MANUAL_PROGRAMS_get_index_for_group_with_this_GID
	.section	.text.MANUAL_PROGRAMS_get_last_group_ID,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_get_last_group_ID
	.type	MANUAL_PROGRAMS_get_last_group_ID, %function
MANUAL_PROGRAMS_get_last_group_ID:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI23:
	ldr	r4, .L76
	mov	r1, #400
	ldr	r2, .L76+4
	ldr	r0, [r4, #0]
	ldr	r3, .L76+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L76+12
	ldr	r0, [r4, #0]
	ldr	r3, [r3, #0]
	ldr	r5, [r3, #12]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L77:
	.align	2
.L76:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1015
	.word	.LANCHOR0
.LFE21:
	.size	MANUAL_PROGRAMS_get_last_group_ID, .-MANUAL_PROGRAMS_get_last_group_ID
	.section	.text.MANUAL_PROGRAMS_get_num_groups_in_use,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_get_num_groups_in_use
	.type	MANUAL_PROGRAMS_get_num_groups_in_use, %function
MANUAL_PROGRAMS_get_num_groups_in_use:
.LFB22:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L82
	stmfd	sp!, {r4, lr}
.LCFI24:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L82+4
	ldr	r3, .L82+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L82+12
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L79
.L81:
	ldr	r3, [r1, #152]
	ldr	r0, .L82+12
	cmp	r3, #0
	addne	r4, r4, #1
	bl	nm_ListGetNext
	mov	r1, r0
.L79:
	cmp	r1, #0
	bne	.L81
	ldr	r3, .L82
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L83:
	.align	2
.L82:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1037
	.word	.LANCHOR0
.LFE22:
	.size	MANUAL_PROGRAMS_get_num_groups_in_use, .-MANUAL_PROGRAMS_get_num_groups_in_use
	.section	.text.MANUAL_PROGRAMS_load_group_name_into_guivar,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_load_group_name_into_guivar
	.type	MANUAL_PROGRAMS_load_group_name_into_guivar, %function
MANUAL_PROGRAMS_load_group_name_into_guivar:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L89
	stmfd	sp!, {r0, r4, lr}
.LCFI25:
	ldr	r2, .L89+4
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L89+8
	bl	xQueueTakeMutexRecursive_debug
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	cmp	r4, r0
	bcs	.L85
	mov	r0, r4
	bl	MANUAL_PROGRAMS_get_group_at_this_index
	mov	r4, r0
	mov	r1, r4
	ldr	r0, .L89+12
	bl	nm_OnList
	cmp	r0, #1
	bne	.L86
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L89+16
	bl	strlcpy
	b	.L87
.L86:
	ldr	r0, .L89+4
	ldr	r1, .L89+20
	bl	Alert_group_not_found_with_filename
	b	.L87
.L85:
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	cmp	r4, r0
	bne	.L88
	mov	r1, #0
	ldr	r0, .L89+24
	bl	GuiLib_GetTextPtr
	mov	r1, #48
	ldr	r2, .L89+28
	mov	r3, r0
	ldr	r0, .L89+16
	bl	snprintf
	b	.L87
.L88:
	mov	r1, #0
	ldr	r0, .L89+32
	bl	GuiLib_GetTextPtr
	mov	r1, #0
	mov	r4, r0
	mov	r0, #1020
	bl	GuiLib_GetTextPtr
	mov	r1, #48
	ldr	r2, .L89+36
	mov	r3, r4
	str	r0, [sp, #0]
	ldr	r0, .L89+16
	bl	snprintf
.L87:
	ldr	r3, .L89
	ldr	r0, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L90:
	.align	2
.L89:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	877
	.word	.LANCHOR0
	.word	GuiVar_itmGroupName
	.word	889
	.word	943
	.word	.LC12
	.word	874
	.word	.LC13
.LFE17:
	.size	MANUAL_PROGRAMS_load_group_name_into_guivar, .-MANUAL_PROGRAMS_load_group_name_into_guivar
	.section	.text.MANUAL_PROGRAMS_get_start_date,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_get_start_date
	.type	MANUAL_PROGRAMS_get_start_date, %function
MANUAL_PROGRAMS_get_start_date:
.LFB23:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI26:
	ldr	r4, .L92
	sub	sp, sp, #24
.LCFI27:
	mov	r5, r0
	add	r0, sp, #16
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, .L92+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L92+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #1
	mov	r1, r0
	ldr	r2, .L92+12
	bl	DMYToDate
	mov	r1, #12
	ldr	r2, .L92+16
	mov	r6, r0
	mov	r0, #31
	bl	DMYToDate
	ldrh	r1, [sp, #20]
	mov	r2, r6, asl #16
	str	r1, [sp, #0]
	ldr	r1, .L92+20
	mov	r2, r2, lsr #16
	str	r1, [sp, #4]
	add	r1, r5, #136
	str	r1, [sp, #8]
	ldr	r1, .L92+24
	str	r1, [sp, #12]
	add	r1, r5, #124
	mov	r3, r0, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r5
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, pc}
.L93:
	.align	2
.L92:
	.word	list_program_data_recursive_MUTEX
	.word	1069
	.word	.LC11
	.word	2011
	.word	2042
	.word	nm_MANUAL_PROGRAMS_set_start_date
	.word	.LC2
.LFE23:
	.size	MANUAL_PROGRAMS_get_start_date, .-MANUAL_PROGRAMS_get_start_date
	.section	.text.MANUAL_PROGRAMS_get_end_date,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_get_end_date
	.type	MANUAL_PROGRAMS_get_end_date, %function
MANUAL_PROGRAMS_get_end_date:
.LFB24:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI28:
	ldr	r4, .L95
	sub	sp, sp, #24
.LCFI29:
	mov	r5, r0
	add	r0, sp, #16
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, .L95+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L95+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #1
	mov	r1, r0
	ldr	r2, .L95+12
	bl	DMYToDate
	mov	r1, #12
	ldr	r2, .L95+16
	mov	r6, r0
	mov	r0, #31
	bl	DMYToDate
	ldrh	r1, [sp, #20]
	mov	r2, r6, asl #16
	str	r1, [sp, #0]
	ldr	r1, .L95+20
	mov	r2, r2, lsr #16
	str	r1, [sp, #4]
	add	r1, r5, #136
	str	r1, [sp, #8]
	ldr	r1, .L95+24
	str	r1, [sp, #12]
	add	r1, r5, #128
	mov	r3, r0, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r5
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, pc}
.L96:
	.align	2
.L95:
	.word	list_program_data_recursive_MUTEX
	.word	1094
	.word	.LC11
	.word	2011
	.word	2042
	.word	nm_MANUAL_PROGRAMS_set_end_date
	.word	.LC3
.LFE24:
	.size	MANUAL_PROGRAMS_get_end_date, .-MANUAL_PROGRAMS_get_end_date
	.section	.text.MANUAL_PROGRAMS_today_is_a_water_day,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_today_is_a_water_day
	.type	MANUAL_PROGRAMS_today_is_a_water_day, %function
MANUAL_PROGRAMS_today_is_a_water_day:
.LFB25:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L100
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI30:
	ldr	r2, .L100+4
	mov	r4, r1
	sub	sp, sp, #64
.LCFI31:
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L100+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #6
	bhi	.L98
	add	r3, r4, #1
	add	r6, sp, #16
	str	r3, [sp, #0]
	mov	r1, #48
	ldr	r2, .L100+12
	ldr	r3, .L100+16
	mov	r0, r6
	bl	snprintf
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L100+20
	add	r1, r4, #24
	str	r3, [sp, #4]
	add	r3, r5, #136
	str	r3, [sp, #8]
	mov	r2, r4
	mov	r0, r5
	add	r1, r5, r1, asl #2
	mov	r3, #7
	str	r6, [sp, #12]
	bl	SHARED_get_bool_from_array_32_bit_change_bits_group
	mov	r4, r0
	b	.L99
.L98:
	ldr	r0, .L100+4
	ldr	r1, .L100+24
	bl	Alert_index_out_of_range_with_filename
	mov	r4, #0
.L99:
	ldr	r3, .L100
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, pc}
.L101:
	.align	2
.L100:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1117
	.word	.LC7
	.word	.LC10
	.word	nm_MANUAL_PROGRAMS_set_water_day
	.word	1134
.LFE25:
	.size	MANUAL_PROGRAMS_today_is_a_water_day, .-MANUAL_PROGRAMS_today_is_a_water_day
	.section	.text.MANUAL_PROGRAMS_get_start_time,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_get_start_time
	.type	MANUAL_PROGRAMS_get_start_time, %function
MANUAL_PROGRAMS_get_start_time:
.LFB26:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L105
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI32:
	ldr	r2, .L105+4
	mov	r4, r1
	sub	sp, sp, #72
.LCFI33:
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L105+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #5
	bhi	.L103
	add	r3, r4, #1
	add	r6, sp, #24
	str	r3, [sp, #0]
	mov	r1, #48
	ldr	r2, .L105+12
	ldr	r3, .L105+16
	mov	r0, r6
	bl	snprintf
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L105+20
	add	r2, r4, #18
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	ldr	r3, .L105+24
	mov	r1, r4
	str	r3, [sp, #12]
	add	r3, r5, #136
	str	r3, [sp, #16]
	mov	r0, r5
	add	r2, r5, r2, asl #2
	mov	r3, #6
	str	r6, [sp, #20]
	bl	SHARED_get_uint32_from_array_32_bit_change_bits_group
	mov	r4, r0
	b	.L104
.L103:
	ldr	r0, .L105+4
	ldr	r1, .L105+28
	bl	Alert_index_out_of_range_with_filename
	ldr	r4, .L105+20
.L104:
	ldr	r3, .L105
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, pc}
.L106:
	.align	2
.L105:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1151
	.word	.LC7
	.word	.LC8
	.word	86400
	.word	nm_MANUAL_PROGRAMS_set_start_time
	.word	1170
.LFE26:
	.size	MANUAL_PROGRAMS_get_start_time, .-MANUAL_PROGRAMS_get_start_time
	.section	.text.MANUAL_PROGRAMS_get_run_time,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_get_run_time
	.type	MANUAL_PROGRAMS_get_run_time, %function
MANUAL_PROGRAMS_get_run_time:
.LFB27:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI34:
	ldr	r4, .L108
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L108+4
	ldr	r3, .L108+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #600
	str	r3, [sp, #0]
	ldr	r3, .L108+12
	add	r1, r5, #132
	str	r3, [sp, #4]
	add	r3, r5, #136
	str	r3, [sp, #8]
	ldr	r3, .L108+16
	mov	r2, #0
	str	r3, [sp, #12]
	mov	r0, r5
	ldr	r3, .L108+20
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L109:
	.align	2
.L108:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1185
	.word	nm_MANUAL_PROGRAMS_set_run_time
	.word	.LC0
	.word	28800
.LFE27:
	.size	MANUAL_PROGRAMS_get_run_time, .-MANUAL_PROGRAMS_get_run_time
	.section	.text.MANUAL_PROGRAMS_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_get_change_bits_ptr
	.type	MANUAL_PROGRAMS_get_change_bits_ptr, %function
MANUAL_PROGRAMS_get_change_bits_ptr:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r0
	add	r2, r3, #140
	mov	r0, r1
	add	r1, r3, #136
	add	r3, r3, #144
	b	SHARED_get_32_bit_change_bits_ptr
.LFE28:
	.size	MANUAL_PROGRAMS_get_change_bits_ptr, .-MANUAL_PROGRAMS_get_change_bits_ptr
	.section	.text.nm_MANUAL_PROGRAMS_store_changes,"ax",%progbits
	.align	2
	.type	nm_MANUAL_PROGRAMS_store_changes, %function
nm_MANUAL_PROGRAMS_store_changes:
.LFB6:
	@ args = 12, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI35:
	mov	r5, r0
	sub	sp, sp, #28
.LCFI36:
	mov	r6, r1
	mov	r4, r2
	ldr	r0, .L133
	mov	r1, r5
	add	r2, sp, #24
	mov	r7, r3
	ldr	r9, [sp, #64]
	ldr	sl, [sp, #72]
	bl	nm_GROUP_find_this_group_in_list
	ldr	r3, [sp, #24]
	cmp	r3, #0
	beq	.L112
	cmp	r6, #1
	bne	.L113
	mov	r0, r5
	bl	nm_GROUP_get_name
	mov	r3, #48
	mov	r2, #0
	stmia	sp, {r3, r4, r7}
	mov	r3, r2
	mov	r1, r0
	mov	r0, #49152
	bl	Alert_ChangeLine_Group
.L113:
	ldr	r0, [sp, #24]
	ldr	r1, [sp, #68]
	bl	MANUAL_PROGRAMS_get_change_bits_ptr
	cmp	r4, #2
	mov	r8, r0
	beq	.L114
	tst	sl, #1
	beq	.L115
.L114:
	mov	r0, r5
	bl	nm_GROUP_get_name
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	mov	r1, r0
	ldr	r0, [sp, #24]
	add	r3, r0, #144
	str	r3, [sp, #12]
	mov	r3, #0
	rsb	ip, r3, r6
	rsbs	r2, ip, #0
	str	r3, [sp, #16]
	adc	r2, r2, ip
	mov	r3, r4
	bl	SHARED_set_name_32_bit_change_bits
	cmp	r4, #2
	bne	.L115
.L117:
	mov	ip, r5
	mov	fp, #0
	b	.L116
.L115:
	tst	sl, #2
	bne	.L117
	b	.L118
.L116:
	rsbs	r3, r6, #1
	stmia	sp, {r4, r7, r9}
	ldr	r0, [sp, #24]
	str	r8, [sp, #12]
	mov	r1, fp
	ldr	r2, [ip, #72]
	movcc	r3, #0
	str	ip, [sp, #20]
	bl	nm_MANUAL_PROGRAMS_set_start_time
	ldr	ip, [sp, #20]
	add	fp, fp, #1
	cmp	fp, #6
	add	ip, ip, #4
	bne	.L116
	cmp	r4, #2
	bne	.L118
.L120:
	mov	ip, r5
	mov	fp, #0
	b	.L119
.L118:
	tst	sl, #4
	bne	.L120
	b	.L121
.L119:
	rsbs	r3, r6, #1
	stmia	sp, {r4, r7, r9}
	ldr	r0, [sp, #24]
	str	r8, [sp, #12]
	mov	r1, fp
	ldr	r2, [ip, #96]
	movcc	r3, #0
	str	ip, [sp, #20]
	bl	nm_MANUAL_PROGRAMS_set_water_day
	ldr	ip, [sp, #20]
	add	fp, fp, #1
	cmp	fp, #7
	add	ip, ip, #4
	bne	.L119
	cmp	r4, #2
	beq	.L122
.L121:
	tst	sl, #8
	beq	.L123
.L122:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #124]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_MANUAL_PROGRAMS_set_start_date
	cmp	r4, #2
	beq	.L124
.L123:
	tst	sl, #16
	beq	.L125
.L124:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #128]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_MANUAL_PROGRAMS_set_end_date
	cmp	r4, #2
	beq	.L126
.L125:
	tst	sl, #32
	beq	.L127
.L126:
	rsbs	r2, r6, #1
	movcc	r2, #0
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #132]
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_MANUAL_PROGRAMS_set_run_time
	cmp	r4, #2
	beq	.L128
.L127:
	tst	sl, #64
	beq	.L111
.L128:
	ldr	r1, [sp, #24]
	ldr	r3, [r1, #152]
	cmp	r3, #0
	beq	.L130
	ldr	r3, [r5, #152]
	cmp	r3, #0
	bne	.L130
	ldr	r0, .L133
	ldr	r2, .L133+4
	ldr	r3, .L133+8
	bl	nm_ListRemove_debug
	ldr	r0, .L133
	ldr	r1, [sp, #24]
	bl	nm_ListInsertTail
.L130:
	rsbs	r2, r6, #1
	ldr	r0, [sp, #24]
	ldr	r1, [r5, #152]
	movcc	r2, #0
	mov	r3, r4
	stmia	sp, {r7, r9}
	str	r8, [sp, #8]
	bl	nm_MANUAL_PROGRAMS_set_in_use
	b	.L111
.L112:
	ldr	r0, .L133+4
	ldr	r1, .L133+12
	bl	Alert_group_not_found_with_filename
.L111:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L134:
	.align	2
.L133:
	.word	.LANCHOR0
	.word	.LC9
	.word	493
	.word	510
.LFE6:
	.size	nm_MANUAL_PROGRAMS_store_changes, .-nm_MANUAL_PROGRAMS_store_changes
	.section	.text.MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars
	.type	MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars, %function
MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI37:
	ldr	r5, .L136+4
	ldr	r7, .L136+8
	ldr	r3, .L136+12
	mov	r1, #400
	mov	r2, r5
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r2, .L136+16
	mov	r0, #156
	bl	mem_malloc_debug
	ldr	r3, .L136+20
	ldr	r8, .L136+24
	mov	r6, #0
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	mov	r2, #156
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r1, .L136+28
	mov	r2, #48
	add	r0, r4, #20
	bl	strlcpy
	ldr	r3, .L136+32
	flds	s15, .L136
	ldr	r2, [r3, #0]
	mov	r3, #60
	mul	r2, r3, r2
	str	r2, [r4, #72]
	ldr	r2, .L136+36
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #76]
	ldr	r2, .L136+40
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #80]
	ldr	r2, .L136+44
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #84]
	ldr	r2, .L136+48
	ldr	r2, [r2, #0]
	mul	r2, r3, r2
	str	r2, [r4, #88]
	ldr	r2, .L136+52
	ldr	r2, [r2, #0]
	mul	r3, r2, r3
	str	r3, [r4, #92]
	ldr	r3, .L136+56
	ldr	r3, [r3, #0]
	str	r3, [r4, #96]
	ldr	r3, .L136+60
	ldr	r3, [r3, #0]
	str	r3, [r4, #100]
	ldr	r3, .L136+64
	ldr	r3, [r3, #0]
	str	r3, [r4, #104]
	ldr	r3, .L136+68
	ldr	r3, [r3, #0]
	str	r3, [r4, #108]
	ldr	r3, .L136+72
	ldr	r3, [r3, #0]
	str	r3, [r4, #112]
	ldr	r3, .L136+76
	ldr	r3, [r3, #0]
	str	r3, [r4, #116]
	ldr	r3, .L136+80
	ldr	r3, [r3, #0]
	str	r3, [r4, #120]
	ldr	r3, .L136+84
	ldr	r3, [r3, #0]
	str	r3, [r4, #124]
	ldr	r3, .L136+88
	ldr	r3, [r3, #0]
	str	r3, [r4, #128]
	ldr	r3, .L136+92
	flds	s14, [r3, #0]
	ldr	r3, .L136+96
	fmuls	s15, s14, s15
	ldr	r3, [r3, #0]
	ftouizs	s15, s15
	fsts	s15, [r4, #132]	@ int
	str	r3, [r4, #152]
	ldr	sl, [r8, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r1, sl
	mov	r2, #2
	stmib	sp, {r2, r6}
	mov	r3, r0
	mov	r0, r4
	bl	nm_MANUAL_PROGRAMS_store_changes
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L136+100
	bl	mem_free_debug
	ldr	r0, [r7, #0]
	bl	xQueueGiveMutexRecursive
	str	r6, [r8, #0]
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, sl, pc}
.L137:
	.align	2
.L136:
	.word	1114636288
	.word	.LC11
	.word	list_program_data_recursive_MUTEX
	.word	819
	.word	823
	.word	g_GROUP_ID
	.word	g_GROUP_creating_new
	.word	GuiVar_GroupName
	.word	GuiVar_ManualPStartTime1
	.word	GuiVar_ManualPStartTime2
	.word	GuiVar_ManualPStartTime3
	.word	GuiVar_ManualPStartTime4
	.word	GuiVar_ManualPStartTime5
	.word	GuiVar_ManualPStartTime6
	.word	GuiVar_ManualPWaterDay_Sun
	.word	GuiVar_ManualPWaterDay_Mon
	.word	GuiVar_ManualPWaterDay_Tue
	.word	GuiVar_ManualPWaterDay_Wed
	.word	GuiVar_ManualPWaterDay_Thu
	.word	GuiVar_ManualPWaterDay_Fri
	.word	GuiVar_ManualPWaterDay_Sat
	.word	.LANCHOR4
	.word	.LANCHOR5
	.word	GuiVar_ManualPRunTime
	.word	GuiVar_ManualPInUse
	.word	861
.LFE16:
	.size	MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars, .-MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars
	.section	.text.nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm
	.type	nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm, %function
nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm:
.LFB7:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI38:
	mov	r8, r0
	sub	sp, sp, #36
.LCFI39:
	str	r1, [sp, #12]
	str	r2, [sp, #16]
	mov	r1, r8
	add	r0, sp, #24
	mov	r2, #4
	mov	r9, #0
	mov	r7, r3
	add	r8, r8, #4
	bl	memcpy
	mov	r5, #4
	mov	fp, r9
	b	.L139
.L149:
	mov	r1, r8
	mov	r2, #4
	add	r0, sp, #32
	bl	memcpy
	add	r1, r8, #4
	mov	r2, #4
	add	r0, sp, #28
	bl	memcpy
	add	r1, r8, #8
	mov	r2, #4
	add	r0, sp, #20
	bl	memcpy
	ldr	r0, .L154
	ldr	r1, [sp, #32]
	mov	r2, #1
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	add	r4, r8, #12
	subs	sl, r0, #0
	bne	.L140
	mov	r0, r7
	bl	nm_MANUAL_PROGRAMS_create_new_group
	ldr	r3, [sp, #32]
	cmp	r7, #16
	mov	sl, r0
	str	r3, [r0, #16]
	bne	.L153
	ldr	r3, .L154+4
	add	r0, r0, #136
	ldr	r1, [r3, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L153:
	mov	fp, #1
.L140:
	ldr	r1, .L154+8
	mov	r2, #648
	mov	r0, #156
	bl	mem_malloc_debug
	mov	r1, #0
	mov	r2, #156
	mov	r6, r0
	bl	memset
	mov	r0, r6
	mov	r1, sl
	mov	r2, #156
	bl	memcpy
	ldr	r3, [sp, #20]
	tst	r3, #1
	addeq	r5, r5, #12
	beq	.L142
	mov	r1, r4
	add	r0, r6, #20
	mov	r2, #48
	bl	memcpy
	add	r4, r8, #60
	add	r5, r5, #60
.L142:
	ldr	r3, [sp, #20]
	tst	r3, #2
	beq	.L143
	mov	r1, r4
	add	r0, r6, #72
	mov	r2, #24
	bl	memcpy
	add	r4, r4, #24
	add	r5, r5, #24
.L143:
	ldr	r3, [sp, #20]
	tst	r3, #4
	beq	.L144
	mov	r1, r4
	add	r0, r6, #96
	mov	r2, #28
	bl	memcpy
	add	r4, r4, #28
	add	r5, r5, #28
.L144:
	ldr	r3, [sp, #20]
	tst	r3, #8
	beq	.L145
	mov	r1, r4
	add	r0, r6, #124
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L145:
	ldr	r3, [sp, #20]
	tst	r3, #16
	beq	.L146
	mov	r1, r4
	add	r0, r6, #128
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L146:
	ldr	r3, [sp, #20]
	tst	r3, #32
	beq	.L147
	mov	r1, r4
	add	r0, r6, #132
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L147:
	ldr	r3, [sp, #20]
	tst	r3, #64
	beq	.L148
	mov	r1, r4
	add	r0, r6, #152
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L148:
	ldr	r3, [sp, #16]
	mov	r0, r6
	stmia	sp, {r3, r7}
	ldr	r3, [sp, #20]
	mov	r1, fp
	str	r3, [sp, #8]
	ldr	r2, [sp, #12]
	mov	r3, #0
	bl	nm_MANUAL_PROGRAMS_store_changes
	mov	r0, r6
	ldr	r1, .L154+8
	ldr	r2, .L154+12
	bl	mem_free_debug
	add	r9, r9, #1
	mov	r8, r4
.L139:
	ldr	r3, [sp, #24]
	cmp	r9, r3
	bcc	.L149
	cmp	r5, #0
	beq	.L150
	cmp	r7, #1
	cmpne	r7, #15
	beq	.L151
	cmp	r7, #16
	bne	.L152
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L152
.L151:
	mov	r0, #12
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L152
.L150:
	ldr	r0, .L154+8
	ldr	r1, .L154+16
	bl	Alert_bit_set_with_no_data_with_filename
.L152:
	mov	r0, r5
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L155:
	.align	2
.L154:
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	.LC9
	.word	745
	.word	799
.LFE7:
	.size	nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm, .-nm_MANUAL_PROGRAMS_extract_and_store_changes_from_comm
	.section	.text.MANUAL_PROGRAMS_extract_and_store_group_name_from_GuiVars,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_extract_and_store_group_name_from_GuiVars
	.type	MANUAL_PROGRAMS_extract_and_store_group_name_from_GuiVars, %function
MANUAL_PROGRAMS_extract_and_store_group_name_from_GuiVars:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L158
	stmfd	sp!, {r4, r5, lr}
.LCFI40:
	ldr	r0, [r3, #0]
	sub	sp, sp, #20
.LCFI41:
	mov	r1, #400
	ldr	r2, .L158+4
	ldr	r3, .L158+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L158+12
	ldr	r0, [r3, #0]
	bl	MANUAL_PROGRAMS_get_group_at_this_index
	subs	r4, r0, #0
	beq	.L157
	bl	FLOWSENSE_get_controller_index
	mov	r1, #2
	mov	r5, r0
	mov	r0, r4
	bl	MANUAL_PROGRAMS_get_change_bits_ptr
	add	r3, r4, #144
	str	r3, [sp, #12]
	mov	r3, #0
	mov	r2, #1
	str	r3, [sp, #16]
	ldr	r1, .L158+16
	mov	r3, #2
	str	r5, [sp, #0]
	str	r2, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	SHARED_set_name_32_bit_change_bits
.L157:
	ldr	r3, .L158
	ldr	r0, [r3, #0]
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L159:
	.align	2
.L158:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	802
	.word	g_GROUP_list_item_index
	.word	GuiVar_GroupName
.LFE15:
	.size	MANUAL_PROGRAMS_extract_and_store_group_name_from_GuiVars, .-MANUAL_PROGRAMS_extract_and_store_group_name_from_GuiVars
	.section	.text.MANUAL_PROGRAMS_build_data_to_send,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_build_data_to_send
	.type	MANUAL_PROGRAMS_build_data_to_send, %function
MANUAL_PROGRAMS_build_data_to_send:
.LFB13:
	@ args = 4, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI42:
	mov	r8, r3
	sub	sp, sp, #60
.LCFI43:
	mov	r3, #0
	str	r3, [sp, #56]
	ldr	r3, .L178
	str	r1, [sp, #36]
	mov	r4, r0
	mov	r6, r2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L178+4
	ldr	r3, .L178+8
	ldr	sl, [sp, #96]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L178+12
	bl	nm_ListGetFirst
	b	.L176
.L164:
	mov	r0, r5
	mov	r1, r8
	bl	MANUAL_PROGRAMS_get_change_bits_ptr
	ldr	r3, [r0, #0]
	cmp	r3, #0
	ldrne	r3, [sp, #56]
	addne	r3, r3, #1
	strne	r3, [sp, #56]
	bne	.L163
.L162:
	ldr	r0, .L178+12
	mov	r1, r5
	bl	nm_ListGetNext
.L176:
	cmp	r0, #0
	mov	r5, r0
	bne	.L164
.L163:
	ldr	fp, [sp, #56]
	cmp	fp, #0
	beq	.L165
	mov	r3, #0
	str	r3, [sp, #56]
	mov	r0, r4
	mov	r3, r6
	add	r1, sp, #48
	ldr	r2, [sp, #36]
	str	sl, [sp, #0]
	bl	PDATA_allocate_space_for_num_changed_groups_in_pucp
	ldr	r3, [r6, #0]
	cmp	r3, #0
	mov	fp, r0
	beq	.L165
	ldr	r0, .L178+12
	bl	nm_ListGetFirst
	ldr	r2, [sp, #36]
	add	r2, r2, #12
	str	r2, [sp, #40]
	mov	r5, r0
	b	.L166
.L172:
	mov	r0, r5
	mov	r1, r8
	bl	MANUAL_PROGRAMS_get_change_bits_ptr
	ldr	r3, [r0, #0]
	mov	r7, r0
	cmp	r3, #0
	beq	.L167
	ldr	r3, [r6, #0]
	cmp	r3, #0
	beq	.L168
	ldr	r3, [sp, #40]
	add	ip, r3, fp
	cmp	ip, sl
	bcs	.L168
	add	r1, r5, #16
	mov	r2, #4
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp
	mov	r1, #4
	mov	r2, r1
	add	r3, sp, #52
	mov	r0, r4
	bl	PDATA_copy_bitfield_info_into_pucp
	ldr	ip, [sp, #28]
	add	r3, r5, #20
	str	r3, [sp, #4]
	mov	r3, #48
	add	r9, r5, #148
	mov	r2, #0
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r3, r7
	mov	r0, r4
	str	r2, [sp, #44]
	str	ip, [sp, #12]
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r2, [sp, #36]
	add	r3, r5, #72
	add	r2, fp, r2
	str	r3, [sp, #4]
	mov	r3, #24
	str	r3, [sp, #8]
	str	r2, [sp, #32]
	add	r1, sp, #44
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, r0, #12
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #1
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #96
	str	r3, [sp, #4]
	mov	r3, #28
	str	r3, [sp, #8]
	add	r1, sp, #44
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #2
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #124
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #44
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #3
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #128
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #44
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #4
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #132
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #44
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #5
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #152
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #44
	str	r9, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	mov	r2, #6
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r7, ip, r0
	cmp	r7, #12
	bls	.L175
	b	.L177
.L168:
	mov	r3, #0
	str	r3, [r6, #0]
	b	.L171
.L177:
	ldr	r3, [sp, #56]
	ldr	r0, [sp, #52]
	add	r3, r3, #1
	add	r1, sp, #44
	mov	r2, #4
	str	r3, [sp, #56]
	add	fp, fp, r7
	bl	memcpy
	b	.L167
.L175:
	ldr	r3, [r4, #0]
	sub	r3, r3, #12
	str	r3, [r4, #0]
.L167:
	mov	r1, r5
	ldr	r0, .L178+12
	bl	nm_ListGetNext
	mov	r5, r0
.L166:
	cmp	r5, #0
	bne	.L172
.L171:
	ldr	r3, [sp, #56]
	cmp	r3, #0
	beq	.L173
	ldr	r0, [sp, #48]
	add	r1, sp, #56
	mov	r2, #4
	bl	memcpy
	b	.L165
.L173:
	ldr	r2, [r4, #0]
	mov	fp, r3
	sub	r2, r2, #4
	str	r2, [r4, #0]
.L165:
	ldr	r3, .L178
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, fp
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L179:
	.align	2
.L178:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	491
	.word	.LANCHOR0
.LFE13:
	.size	MANUAL_PROGRAMS_build_data_to_send, .-MANUAL_PROGRAMS_build_data_to_send
	.section	.text.MANUAL_PROGRAMS_return_the_number_of_starts_for_this_GID,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_return_the_number_of_starts_for_this_GID
	.type	MANUAL_PROGRAMS_return_the_number_of_starts_for_this_GID, %function
MANUAL_PROGRAMS_return_the_number_of_starts_for_this_GID:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI44:
	subs	r5, r0, #0
	mov	r4, r1
	beq	.L181
	ldr	r3, .L191
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L191+4
	ldr	r3, .L191+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L191+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r5, r0, #0
	beq	.L182
	ldrh	r3, [r4, #4]
	ldr	r2, [r5, #124]
	cmp	r3, r2
	bcc	.L189
	ldr	r2, [r5, #128]
	cmp	r3, r2
	bhi	.L189
	ldrb	r3, [r4, #18]	@ zero_extendqisi2
	add	r3, r3, #24
	ldr	r3, [r5, r3, asl #2]
	cmp	r3, #1
	bne	.L189
.LBB8:
	ldr	r1, [r4, #0]
	mov	r2, r5
	mov	r3, #6
	mov	r5, #0
.L184:
	ldr	r0, [r2, #72]
	add	r2, r2, #4
	cmp	r0, r1
	addeq	r5, r5, #1
	subs	r3, r3, #1
	bne	.L184
	b	.L182
.L189:
.LBE8:
	mov	r5, #0
.L182:
	ldr	r3, .L191
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L181:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L192:
	.align	2
.L191:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1222
	.word	.LANCHOR0
.LFE29:
	.size	MANUAL_PROGRAMS_return_the_number_of_starts_for_this_GID, .-MANUAL_PROGRAMS_return_the_number_of_starts_for_this_GID
	.section	.text.MANUAL_PROGRAMS_clean_house_processing,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_clean_house_processing
	.type	MANUAL_PROGRAMS_clean_house_processing, %function
MANUAL_PROGRAMS_clean_house_processing:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L197
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI45:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L197+4
	ldr	r3, .L197+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L197+12
	ldr	r1, .L197+4
	ldr	r2, .L197+16
	b	.L196
.L195:
	ldr	r1, .L197+4
	ldr	r2, .L197+20
	bl	mem_free_debug
	ldr	r0, .L197+12
	ldr	r1, .L197+4
	ldr	r2, .L197+24
.L196:
	bl	nm_ListRemoveHead_debug
	cmp	r0, #0
	bne	.L195
	ldr	r4, .L197
	str	r0, [sp, #0]
	str	r0, [sp, #4]
	ldr	r1, .L197+28
	ldr	r2, .L197+32
	mov	r3, #156
	ldr	r0, .L197+12
	bl	nm_GROUP_create_new_group
	mov	r3, #156
	str	r3, [sp, #0]
	ldr	r3, [r4, #0]
	ldr	r1, .L197+36
	str	r3, [sp, #4]
	mov	r3, #12
	str	r3, [sp, #8]
	mov	r2, #3
	ldr	r3, .L197+12
	mov	r0, #1
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	SYSTEM_PRESERVES_synchronize_preserves_to_file
.L198:
	.align	2
.L197:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1274
	.word	.LANCHOR0
	.word	1278
	.word	1282
	.word	1284
	.word	.LANCHOR3
	.word	nm_MANUAL_PROGRAMS_set_default_values
	.word	.LANCHOR1
.LFE30:
	.size	MANUAL_PROGRAMS_clean_house_processing, .-MANUAL_PROGRAMS_clean_house_processing
	.section	.text.MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.type	MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token, %function
MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI46:
	ldr	r4, .L203
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L203+4
	ldr	r3, .L203+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L203+12
	bl	nm_ListGetFirst
	b	.L202
.L201:
	add	r0, r5, #140
	ldr	r1, [r4, #0]
	bl	SHARED_set_all_32_bit_change_bits
	ldr	r0, .L203+12
	mov	r1, r5
	bl	nm_ListGetNext
.L202:
	cmp	r0, #0
	mov	r5, r0
	bne	.L201
	ldr	r4, .L203+16
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L203+4
	ldr	r3, .L203+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L203+24
	mov	r2, #1
	str	r2, [r3, #444]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L203
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L204:
	.align	2
.L203:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1313
	.word	.LANCHOR0
	.word	comm_mngr_recursive_MUTEX
	.word	1335
	.word	comm_mngr
.LFE31:
	.size	MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token, .-MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.section	.text.MANUAL_PROGRAMS_on_all_groups_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_on_all_groups_set_or_clear_commserver_change_bits
	.type	MANUAL_PROGRAMS_on_all_groups_set_or_clear_commserver_change_bits, %function
MANUAL_PROGRAMS_on_all_groups_set_or_clear_commserver_change_bits:
.LFB32:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI47:
	ldr	r4, .L211
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L211+4
	ldr	r3, .L211+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L211+12
	bl	nm_ListGetFirst
	b	.L210
.L209:
	cmp	r5, #51
	ldr	r1, [r4, #0]
	add	r0, r6, #144
	bne	.L207
	bl	SHARED_clear_all_32_bit_change_bits
	b	.L208
.L207:
	bl	SHARED_set_all_32_bit_change_bits
.L208:
	ldr	r0, .L211+12
	mov	r1, r6
	bl	nm_ListGetNext
.L210:
	cmp	r0, #0
	mov	r6, r0
	bne	.L209
	ldr	r3, .L211
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L212:
	.align	2
.L211:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1351
	.word	.LANCHOR0
.LFE32:
	.size	MANUAL_PROGRAMS_on_all_groups_set_or_clear_commserver_change_bits, .-MANUAL_PROGRAMS_on_all_groups_set_or_clear_commserver_change_bits
	.section	.text.nm_MANUAL_PROGRAMS_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_MANUAL_PROGRAMS_update_pending_change_bits
	.type	nm_MANUAL_PROGRAMS_update_pending_change_bits, %function
nm_MANUAL_PROGRAMS_update_pending_change_bits:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI48:
	ldr	r5, .L220
	mov	r1, #400
	ldr	r2, .L220+4
	ldr	r3, .L220+8
	mov	r7, r0
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L220+12
	bl	nm_ListGetFirst
	mov	r8, #0
	mov	r6, #1
	mov	r4, r0
	b	.L214
.L217:
	ldr	r3, [r4, #148]
	cmp	r3, #0
	beq	.L215
	cmp	r7, #0
	beq	.L216
	ldr	r2, [r4, #144]
	mov	r1, #2
	orr	r3, r2, r3
	str	r3, [r4, #144]
	ldr	r3, .L220+16
	ldr	r2, .L220+20
	str	r6, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L220+24
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L219
.L216:
	add	r0, r4, #148
	ldr	r1, [r5, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L219:
	mov	r8, #1
.L215:
	mov	r1, r4
	ldr	r0, .L220+12
	bl	nm_ListGetNext
	mov	r4, r0
.L214:
	cmp	r4, #0
	bne	.L217
	ldr	r3, .L220
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r8, #0
	beq	.L213
	mov	r0, #12
	mov	r1, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L213:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L221:
	.align	2
.L220:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1388
	.word	.LANCHOR0
	.word	weather_preserves
	.word	60000
	.word	cics
.LFE33:
	.size	nm_MANUAL_PROGRAMS_update_pending_change_bits, .-nm_MANUAL_PROGRAMS_update_pending_change_bits
	.section	.text.MANUAL_PROGRAMS_load_ftimes_list,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_load_ftimes_list
	.type	MANUAL_PROGRAMS_load_ftimes_list, %function
MANUAL_PROGRAMS_load_ftimes_list:
.LFB34:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L227
	stmfd	sp!, {r0, r4, lr}
.LCFI49:
	ldr	r2, .L227+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L227+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L227+12
	bl	nm_ListGetFirst
	b	.L226
.L225:
	ldr	r3, [r4, #152]
	cmp	r3, #0
	beq	.L224
	mov	r0, #80
	mov	r1, sp
	ldr	r2, .L227+4
	ldr	r3, .L227+16
	bl	mem_oabia
	cmp	r0, #0
	beq	.L224
	mov	r1, #0
	mov	r2, #80
	ldr	r0, [sp, #0]
	bl	memset
	ldr	r3, [r4, #16]
	ldr	r0, [sp, #0]
	add	r1, r4, #72
	str	r3, [r0, #12]
	mov	r2, #24
	add	r0, r0, #16
	bl	memcpy
	ldr	r0, [sp, #0]
	add	r1, r4, #96
	add	r0, r0, #40
	mov	r2, #28
	bl	memcpy
	ldr	r3, [r4, #124]
	ldr	r1, [sp, #0]
	ldr	r0, .L227+20
	str	r3, [r1, #68]
	ldr	r3, [r4, #128]
	str	r3, [r1, #72]
	ldr	r3, [r4, #132]
	str	r3, [r1, #76]
	bl	nm_ListInsertTail
.L224:
	ldr	r0, .L227+12
	mov	r1, r4
	bl	nm_ListGetNext
.L226:
	cmp	r0, #0
	mov	r4, r0
	bne	.L225
	ldr	r3, .L227
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r3, r4, pc}
.L228:
	.align	2
.L227:
	.word	list_program_data_recursive_MUTEX
	.word	.LC11
	.word	1450
	.word	.LANCHOR0
	.word	1460
	.word	ft_manual_programs_list_hdr
.LFE34:
	.size	MANUAL_PROGRAMS_load_ftimes_list, .-MANUAL_PROGRAMS_load_ftimes_list
	.section	.text.MANUAL_PROGRAMS_calculate_chain_sync_crc,"ax",%progbits
	.align	2
	.global	MANUAL_PROGRAMS_calculate_chain_sync_crc
	.type	MANUAL_PROGRAMS_calculate_chain_sync_crc, %function
MANUAL_PROGRAMS_calculate_chain_sync_crc:
.LFB35:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L234
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI50:
	ldr	r5, .L234+4
	mov	r1, #400
	ldr	r2, .L234+8
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L234+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #8]
	mov	r0, #156
	mul	r0, r3, r0
	mov	r1, sp
	ldr	r2, .L234+8
	ldr	r3, .L234+16
	bl	mem_oabia
	subs	r6, r0, #0
	beq	.L230
	ldr	r3, [sp, #0]
	add	r6, sp, #8
	mov	r0, r5
	str	r3, [r6, #-4]!
	bl	nm_ListGetFirst
	mov	r7, #0
	mov	r5, r0
	b	.L231
.L232:
	add	r8, r5, #20
	mov	r0, r8
	bl	strlen
	mov	r1, r8
	mov	r2, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #72
	mov	r2, #24
	mov	r8, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #96
	mov	r2, #28
	add	r0, r8, r0
	add	r7, r0, r7
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #124
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #128
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #132
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #152
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r1, r5
	add	r7, r7, r0
	ldr	r0, .L234+4
	bl	nm_ListGetNext
	mov	r5, r0
.L231:
	cmp	r5, #0
	bne	.L232
	mov	r1, r7
	ldr	r0, [sp, #0]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L234+20
	add	r4, r4, #43
	ldr	r1, .L234+8
	ldr	r2, .L234+24
	mov	r6, #1
	str	r0, [r3, r4, asl #2]
	ldr	r0, [sp, #0]
	bl	mem_free_debug
	b	.L233
.L230:
	ldr	r3, .L234+28
	ldr	r0, .L234+32
	ldr	r1, [r3, r4, asl #4]
	bl	Alert_Message_va
.L233:
	ldr	r3, .L234
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L235:
	.align	2
.L234:
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC11
	.word	1516
	.word	1528
	.word	cscs
	.word	1590
	.word	chain_sync_file_pertinants
	.word	.LC14
.LFE35:
	.size	MANUAL_PROGRAMS_calculate_chain_sync_crc, .-MANUAL_PROGRAMS_calculate_chain_sync_crc
	.global	g_MANUAL_PROGRAMS_stop_date
	.global	g_MANUAL_PROGRAMS_start_date
	.global	manual_programs_list_item_sizes
	.global	MANUAL_PROGRAMS_DEFAULT_NAME
	.section	.bss.g_MANUAL_PROGRAMS_stop_date,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	g_MANUAL_PROGRAMS_stop_date, %object
	.size	g_MANUAL_PROGRAMS_stop_date, 4
g_MANUAL_PROGRAMS_stop_date:
	.space	4
	.section	.bss.g_MANUAL_PROGRAMS_start_date,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_MANUAL_PROGRAMS_start_date, %object
	.size	g_MANUAL_PROGRAMS_start_date, 4
g_MANUAL_PROGRAMS_start_date:
	.space	4
	.section	.bss.manual_programs_group_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	manual_programs_group_list_hdr, %object
	.size	manual_programs_group_list_hdr, 20
manual_programs_group_list_hdr:
	.space	20
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"RunTime\000"
.LC1:
	.ascii	"InUse\000"
.LC2:
	.ascii	"StartDate\000"
.LC3:
	.ascii	"StopDate\000"
.LC4:
	.ascii	"MAN PROGS file unexpd update %u\000"
.LC5:
	.ascii	"MAN PROGS file update : to revision %u from %u\000"
.LC6:
	.ascii	"MAN PROGS updater error\000"
.LC7:
	.ascii	"%s%d\000"
.LC8:
	.ascii	"StartTime\000"
.LC9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/shared_manual_programs.c\000"
.LC10:
	.ascii	"Day\000"
.LC11:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/manual_programs.c\000"
.LC12:
	.ascii	"%s\000"
.LC13:
	.ascii	" <%s %s>\000"
.LC14:
	.ascii	"SYNC: no mem to calc checksum %s\000"
	.section	.rodata.MANUAL_PROGRAMS_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	MANUAL_PROGRAMS_FILENAME, %object
	.size	MANUAL_PROGRAMS_FILENAME, 16
MANUAL_PROGRAMS_FILENAME:
	.ascii	"MANUAL_PROGRAMS\000"
	.section	.rodata.MANUAL_PROGRAMS_DEFAULT_NAME,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	MANUAL_PROGRAMS_DEFAULT_NAME, %object
	.size	MANUAL_PROGRAMS_DEFAULT_NAME, 15
MANUAL_PROGRAMS_DEFAULT_NAME:
	.ascii	"Manual Program\000"
	.section	.rodata.manual_programs_list_item_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	manual_programs_list_item_sizes, %object
	.size	manual_programs_list_item_sizes, 16
manual_programs_list_item_sizes:
	.word	148
	.word	148
	.word	152
	.word	156
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI0-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI2-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI6-.LFB3
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI8-.LFB8
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI9-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI11-.LFB1
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI13-.LFB11
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI15-.LFB9
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI16-.LFB10
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI17-.LFB12
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI18-.LFB18
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI19-.LFB19
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI20-.LFB14
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI22-.LFB20
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI23-.LFB21
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI24-.LFB22
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI25-.LFB17
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI26-.LFB23
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI28-.LFB24
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI29-.LCFI28
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI30-.LFB25
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI32-.LFB26
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI34-.LFB27
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI35-.LFB6
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI37-.LFB16
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI38-.LFB7
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI39-.LCFI38
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI40-.LFB15
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI41-.LCFI40
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI42-.LFB13
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xe
	.uleb128 0x60
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI44-.LFB29
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI45-.LFB30
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI46-.LFB31
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI47-.LFB32
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI48-.LFB33
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI49-.LFB34
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI50-.LFB35
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE70:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/shared_manual_programs.c"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/manual_programs.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x310
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF36
	.byte	0x1
	.4byte	.LASF37
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x71
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0xa7
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x12a
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x14f
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.byte	0xdb
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x104
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x2
	.byte	0xa1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST4
	.uleb128 0x5
	.4byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST5
	.uleb128 0x5
	.4byte	0x29
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.2byte	0x12d
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x2
	.2byte	0x10f
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x2
	.2byte	0x11e
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST9
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x2
	.2byte	0x17e
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST10
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x2
	.2byte	0x389
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST11
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x2
	.2byte	0x3ac
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST12
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x2
	.2byte	0x2d9
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST13
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF14
	.byte	0x2
	.2byte	0x3d0
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST14
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF15
	.byte	0x2
	.2byte	0x3f3
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST15
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF16
	.byte	0x2
	.2byte	0x401
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST16
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF17
	.byte	0x2
	.2byte	0x369
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF18
	.byte	0x2
	.2byte	0x425
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST18
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF19
	.byte	0x2
	.2byte	0x43e
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST19
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF20
	.byte	0x2
	.2byte	0x457
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST20
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF21
	.byte	0x2
	.2byte	0x479
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST21
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF22
	.byte	0x2
	.2byte	0x49d
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST22
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF23
	.byte	0x2
	.2byte	0x4b2
	.4byte	.LFB28
	.4byte	.LFE28
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x173
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST23
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF25
	.byte	0x2
	.2byte	0x32f
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST24
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x204
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST25
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF27
	.byte	0x2
	.2byte	0x31e
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST26
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF28
	.byte	0x2
	.2byte	0x1c3
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST27
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF29
	.byte	0x2
	.2byte	0x4b8
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST28
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF30
	.byte	0x2
	.2byte	0x4ec
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST29
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF31
	.byte	0x2
	.2byte	0x51b
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST30
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF32
	.byte	0x2
	.2byte	0x543
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST31
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF33
	.byte	0x2
	.2byte	0x564
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST32
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF34
	.byte	0x2
	.2byte	0x5a2
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST33
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF35
	.byte	0x2
	.2byte	0x5d5
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST34
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB4
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB5
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI5
	.4byte	.LFE2
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI7
	.4byte	.LFE3
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB8
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB0
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI10
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB1
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI12
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB11
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI14
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB9
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB10
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB12
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB18
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB19
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB14
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI21
	.4byte	.LFE14
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB20
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB21
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB22
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB23
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI27
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB24
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI29
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB25
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI31
	.4byte	.LFE25
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB26
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI33
	.4byte	.LFE26
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB27
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB6
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI36
	.4byte	.LFE6
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB16
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB7
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI39
	.4byte	.LFE7
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB15
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI41
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB13
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI43
	.4byte	.LFE13
	.2byte	0x3
	.byte	0x7d
	.sleb128 96
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB29
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB30
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB31
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB32
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB33
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB34
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI49
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB35
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x134
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF29:
	.ascii	"MANUAL_PROGRAMS_return_the_number_of_starts_for_thi"
	.ascii	"s_GID\000"
.LASF36:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF25:
	.ascii	"MANUAL_PROGRAMS_extract_and_store_changes_from_GuiV"
	.ascii	"ars\000"
.LASF27:
	.ascii	"MANUAL_PROGRAMS_extract_and_store_group_name_from_G"
	.ascii	"uiVars\000"
.LASF33:
	.ascii	"nm_MANUAL_PROGRAMS_update_pending_change_bits\000"
.LASF5:
	.ascii	"nm_MANUAL_PROGRAMS_set_end_date\000"
.LASF8:
	.ascii	"init_file_manual_programs\000"
.LASF37:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/manual_programs.c\000"
.LASF4:
	.ascii	"nm_MANUAL_PROGRAMS_set_start_date\000"
.LASF11:
	.ascii	"MANUAL_PROGRAMS_get_group_with_this_GID\000"
.LASF23:
	.ascii	"MANUAL_PROGRAMS_get_change_bits_ptr\000"
.LASF12:
	.ascii	"MANUAL_PROGRAMS_get_group_at_this_index\000"
.LASF16:
	.ascii	"MANUAL_PROGRAMS_get_num_groups_in_use\000"
.LASF30:
	.ascii	"MANUAL_PROGRAMS_clean_house_processing\000"
.LASF34:
	.ascii	"MANUAL_PROGRAMS_load_ftimes_list\000"
.LASF7:
	.ascii	"nm_MANUAL_PROGRAMS_set_default_values\000"
.LASF35:
	.ascii	"MANUAL_PROGRAMS_calculate_chain_sync_crc\000"
.LASF22:
	.ascii	"MANUAL_PROGRAMS_get_run_time\000"
.LASF6:
	.ascii	"nm_manual_programs_updater\000"
.LASF17:
	.ascii	"MANUAL_PROGRAMS_load_group_name_into_guivar\000"
.LASF24:
	.ascii	"nm_MANUAL_PROGRAMS_store_changes\000"
.LASF21:
	.ascii	"MANUAL_PROGRAMS_get_start_time\000"
.LASF13:
	.ascii	"MANUAL_PROGRAMS_copy_group_into_guivars\000"
.LASF3:
	.ascii	"nm_MANUAL_PROGRAMS_set_in_use\000"
.LASF9:
	.ascii	"save_file_manual_programs\000"
.LASF14:
	.ascii	"MANUAL_PROGRAMS_get_index_for_group_with_this_GID\000"
.LASF32:
	.ascii	"MANUAL_PROGRAMS_on_all_groups_set_or_clear_commserv"
	.ascii	"er_change_bits\000"
.LASF10:
	.ascii	"nm_MANUAL_PROGRAMS_create_new_group\000"
.LASF28:
	.ascii	"MANUAL_PROGRAMS_build_data_to_send\000"
.LASF31:
	.ascii	"MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_dis"
	.ascii	"tribution_in_the_next_token\000"
.LASF18:
	.ascii	"MANUAL_PROGRAMS_get_start_date\000"
.LASF19:
	.ascii	"MANUAL_PROGRAMS_get_end_date\000"
.LASF2:
	.ascii	"nm_MANUAL_PROGRAMS_set_run_time\000"
.LASF26:
	.ascii	"nm_MANUAL_PROGRAMS_extract_and_store_changes_from_c"
	.ascii	"omm\000"
.LASF15:
	.ascii	"MANUAL_PROGRAMS_get_last_group_ID\000"
.LASF20:
	.ascii	"MANUAL_PROGRAMS_today_is_a_water_day\000"
.LASF1:
	.ascii	"nm_MANUAL_PROGRAMS_set_water_day\000"
.LASF0:
	.ascii	"nm_MANUAL_PROGRAMS_set_start_time\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
