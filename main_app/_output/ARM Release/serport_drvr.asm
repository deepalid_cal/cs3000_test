	.file	"serport_drvr.c"
	.text
.Ltext0:
	.section	.text.postSerportDrvrEvent,"ax",%progbits
	.align	2
	.global	postSerportDrvrEvent
	.type	postSerportDrvrEvent, %function
postSerportDrvrEvent:
.LFB0:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L3
	stmfd	sp!, {r0, lr}
.LCFI0:
	mul	r2, r0, r2
	add	r3, sp, #4
	str	r1, [r3, #-4]!
	ldr	r1, .L3+4
	ldr	r0, [r1, r2]
	mov	r2, #0
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L1
	ldr	r0, .L3+8
	bl	Alert_Message
.L1:
	ldmfd	sp!, {r3, pc}
.L4:
	.align	2
.L3:
	.word	4280
	.word	.LANCHOR0
	.word	.LC0
.LFE0:
	.size	postSerportDrvrEvent, .-postSerportDrvrEvent
	.section	.text.serial_driver_task,"ax",%progbits
	.align	2
	.global	serial_driver_task
	.type	serial_driver_task, %function
serial_driver_task:
.LFB6:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L70
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI1:
	ldr	r4, [r0, #24]
	rsb	r3, r3, r0
	sub	sp, sp, #20
.LCFI2:
	mov	r3, r3, lsr #5
	cmp	r4, #3
	str	r3, [sp, #12]
	bls	.L6
	ldr	r0, .L70+4
	bl	Alert_Message
.L7:
	b	.L7
.L6:
	ldr	r6, .L70+8
	mov	r1, #0
	mvn	r2, #0
	mov	r3, r1
	ldr	r0, [r6, r4, asl #2]
	bl	xQueueGenericReceive
	ldr	r3, .L70+12
	mov	r5, #24
	mla	r5, r4, r5, r3
	mov	r1, #0
	mov	r0, r5
	bl	nm_ListInit
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	ldr	r0, [r6, r4, asl #2]
	bl	xQueueGenericSend
	mov	r3, #0
	str	r3, [r5, #20]
	mov	r0, r4
	bl	initialize_uart_hardware
	ldr	r3, .L70+16
	ldr	sl, .L70+20
	mul	r3, r4, r3
	mov	r2, #1
	add	r5, sl, r3
	ldr	r3, [sl, r3]
	str	r2, [r5, #16]
	cmp	r3, #0
	str	r3, [sp, #8]
	beq	.L5
.LBB15:
	add	r2, r5, #4224
	add	r2, r2, #52
	mov	r7, r5
	str	r2, [sp, #4]
.L63:
.LBE15:
	ldr	r0, [sp, #8]
	add	r1, sp, #16
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L9
	ldr	r3, [r7, #16]
	ldr	r5, .L70+20
	ldr	r6, .L70+16
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L9
.L15:
	.word	.L10
	.word	.L11
	.word	.L12
	.word	.L13
	.word	.L14
.L10:
.LBB16:
	ldr	r0, .L70+24
	bl	Alert_Message
	b	.L9
.L11:
.LBE16:
.LBB17:
	ldr	r3, [sp, #16]
	sub	r3, r3, #1
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L9
.L20:
	.word	.L16
	.word	.L17
	.word	.L9
	.word	.L18
	.word	.L19
.L16:
	cmp	r4, #1
	bne	.L21
	ldr	r3, .L70+28
	ldr	r2, .L70+32
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #8]
	cmp	r3, #0
	ldr	r3, .L70+36
	ldr	r3, [r3, #0]
	beq	.L22
	tst	r3, #65536
	moveq	r3, #0
	movne	r3, #1
	b	.L23
.L22:
	tst	r3, #65536
	movne	r3, #0
	moveq	r3, #1
.L23:
	ldr	r2, .L70+40
	b	.L64
.L21:
	cmp	r4, #2
	bne	.L25
	ldr	r3, .L70+28
	ldr	r2, .L70+32
	ldr	r3, [r3, #84]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #8]
	cmp	r3, #0
	ldr	r3, .L70+36
	ldr	r3, [r3, #0]
	andne	r3, r3, #1
	bne	.L27
	tst	r3, #1
	movne	r3, #0
	moveq	r3, #1
.L27:
	ldr	r2, .L70+44
.L64:
	strb	r3, [sl, r2]
	b	.L24
.L25:
	cmp	r4, #3
	ldreq	r3, .L70+48
	moveq	r2, #1
	streqb	r2, [sl, r3]
	beq	.L24
.L28:
	cmp	r4, #0
	bne	.L24
	ldrb	r3, [sl, #20]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L24
	ldr	r0, .L70+52
	bl	Alert_Message
	mov	r3, #1
	strb	r3, [sl, #20]
.L24:
	ldr	fp, .L70+12
	mov	r8, #24
	mla	r8, r4, r8, fp
	ldr	r5, [r8, #20]
	cmp	r5, #0
	bne	.L29
	ldr	r9, .L70+8
	mov	r1, r5
	mvn	r2, #0
	mov	r3, r5
	ldr	r0, [r9, r4, asl #2]
	bl	xQueueGenericReceive
	mov	r0, r8
	bl	nm_ListGetFirst
	mov	r1, r5
	mov	r2, r5
	mov	r3, r5
	mov	r6, r0
	ldr	r0, [r9, r4, asl #2]
	bl	xQueueGenericSend
	cmp	r6, #0
	beq	.L9
	ldrb	r3, [r7, #20]	@ zero_extendqisi2
	str	r6, [r8, #20]
	ands	r3, r3, #255
	beq	.L30
.LBB18:
	bl	CONFIG_this_controller_originates_commserver_messages
	cmp	r0, #0
	beq	.L31
	cmp	r4, #1
	bne	.L31
	ldr	r3, [fp, #44]
	ldr	r3, [r3, #32]
	cmp	r3, #0
	bne	.L31
	bl	CONFIG_is_connected
	subs	r5, r0, #0
	bne	.L31
	ldr	r0, .L70+56
	bl	Alert_Message
	ldr	r0, [r9, #4]
	mov	r1, r5
	mvn	r2, #0
	mov	r3, r5
	bl	xQueueGenericReceive
	b	.L32
.L31:
	ldr	r2, .L70+16
	ldr	r3, .L70+20
	mov	r0, r4
	mla	r3, r2, r4, r3
	mov	r2, #2
	str	r2, [r3, #16]
	b	.L54
.L34:
	ldr	r0, [r5, #16]
	ldr	r1, .L70+60
	mov	r2, #201
	bl	mem_free_debug
	mov	r0, r5
	ldr	r1, .L70+60
	mov	r2, #202
	bl	mem_free_debug
.L32:
	mov	r0, r8
	ldr	r1, .L70+60
	mov	r2, #199
	bl	nm_ListRemoveHead_debug
	subs	r5, r0, #0
	bne	.L34
	ldr	r3, .L70+8
	mov	r1, r5
	ldr	r0, [r3, #4]
	mov	r2, r5
	mov	r3, r5
	bl	xQueueGenericSend
	ldr	r3, .L70+12
	str	r5, [r3, #44]
	b	.L9
.L30:
.LBE18:
	str	r3, [r8, #20]
	ldr	r0, [r7, #8]
	bl	xTimerIsTimerActive
	subs	r5, r0, #0
	bne	.L9
	ldr	r3, [r7, #12]
	ldr	r6, [r7, #8]
	bic	r3, r3, #2
	str	r3, [r7, #12]
	b	.L67
.L29:
	ldr	r0, .L70+64
	bl	Alert_Message
	mov	r3, #0
	str	r3, [r8, #20]
	b	.L18
.L17:
	ldr	r3, .L70+68
	ldr	r0, .L70+72
	ldr	r1, [r3, r4, asl #2]
	bl	Alert_Message_va
.L18:
	mov	r0, r4
	mov	r1, #1
	b	.L66
.L19:
	ldr	r3, .L70+8
	mov	r1, #0
	ldr	r0, [r3, r4, asl #2]
	mvn	r2, #0
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r3, .L70+12
	mov	r2, #24
	mla	r3, r2, r4, r3
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L36
	mov	r0, r4
	mov	r1, #0
	bl	Alert_cts_timeout
	b	.L36
.L37:
	ldr	r0, [r5, #16]
	ldr	r1, .L70+60
	ldr	r2, .L70+76
	bl	mem_free_debug
	mov	r0, r5
	ldr	r1, .L70+60
	ldr	r2, .L70+80
	bl	mem_free_debug
	b	.L62
.L36:
	ldr	r3, .L70+12
	mov	r6, #24
	mla	r6, r4, r6, r3
.L62:
	mov	r0, r6
	ldr	r1, .L70+60
	mov	r2, #316
	bl	nm_ListRemoveHead_debug
	subs	r5, r0, #0
	bne	.L37
	ldr	r3, .L70+8
	mov	r1, r5
	ldr	r0, [r3, r4, asl #2]
	mov	r2, r5
	mov	r3, r5
	bl	xQueueGenericSend
	b	.L9
.L12:
.LBE17:
.LBB19:
	ldr	r3, [sp, #16]
	cmp	r3, #3
	mlaeq	r5, r6, r4, r5
	streq	r3, [r5, #16]
	beq	.L9
	cmp	r3, #5
	beq	.L40
	cmp	r3, #2
	bne	.L9
	ldr	r3, .L70+12
	mov	r5, #24
	mla	r5, r4, r5, r3
	ldr	r3, [r5, #20]
	cmp	r3, #0
	bne	.L41
	ldr	r3, .L70+68
	ldr	r0, .L70+84
	ldr	r1, [r3, r4, asl #2]
	bl	Alert_Message_va
	b	.L69
.L41:
	ldrh	r2, [r3, #20]
	cmp	r2, #0
	beq	.L42
	ldr	r3, .L70+68
	ldr	r0, .L70+88
	ldr	r1, [r3, r4, asl #2]
	bl	Alert_Message_va
	mov	r3, #0
	str	r3, [r5, #20]
	b	.L69
.L42:
	ldr	r0, [r3, #24]
	cmp	r0, #0
	beq	.L43
	bl	TPL_OUT_queue_a_message_to_start_resp_timer
.L43:
	ldr	r3, .L70+12
	cmp	r4, #0
	ldreq	r2, [r3, #20]
	movne	r9, #0
	ldreq	r9, [r2, #28]
	mov	r2, #24
	mla	r3, r2, r4, r3
	ldr	r3, [r3, #20]
	ldr	r3, [r3, #28]
	cmp	r3, #0
	beq	.L45
	cmp	r4, #0
	beq	.L45
	ldr	r0, .L70+92
	bl	Alert_Message
.L45:
	ldr	r8, .L70+8
	mov	r1, #0
	mov	r3, r1
	mvn	r2, #0
	ldr	r0, [r8, r4, asl #2]
	bl	xQueueGenericReceive
	ldr	r3, .L70+12
	mov	r5, #24
	mla	r5, r4, r5, r3
	ldr	r1, .L70+60
	ldr	r2, .L70+96
	mov	r0, r5
	bl	nm_ListRemoveHead_debug
	mov	r1, #0
	mov	r3, r1
	mov	r2, r1
	mov	r6, r0
	ldr	r0, [r8, r4, asl #2]
	bl	xQueueGenericSend
	ldr	r3, [r5, #20]
	cmp	r6, r3
	beq	.L46
	ldr	r0, .L70+100
	bl	Alert_Message
	cmp	r6, #0
	beq	.L46
	ldr	r0, [r6, #16]
	ldr	r1, .L70+60
	ldr	r2, .L70+104
	bl	mem_free_debug
	mov	r0, r6
	ldr	r1, .L70+60
	mov	r2, #424
	bl	mem_free_debug
.L46:
	ldr	r3, .L70+12
	mov	r5, #24
	mla	r5, r4, r5, r3
	ldr	r3, [r5, #20]
	cmp	r3, #0
	beq	.L47
	ldr	r0, [r3, #16]
	ldr	r1, .L70+60
	mov	r2, #432
	bl	mem_free_debug
	ldr	r0, [r5, #20]
	ldr	r1, .L70+60
	ldr	r2, .L70+108
	bl	mem_free_debug
.L47:
	ldr	r3, .L70+12
	mov	r2, #24
	mla	r3, r2, r4, r3
	mov	r5, #0
	cmp	r9, r5
	str	r5, [r3, #20]
	beq	.L69
	mov	r3, #4
	str	r3, [r7, #16]
	ldr	r3, [sp, #4]
	ldr	r6, [r3, #0]
.L67:
	bl	xTaskGetTickCount
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, r5
	mov	r3, r5
	mov	r2, r0
	mov	r0, r6
	bl	xTimerGenericCommand
	b	.L9
.L40:
	mov	r0, r4
	mov	r1, #2
	bl	Alert_cts_timeout
	b	.L9
.L13:
.LBE19:
.LBB20:
	ldr	r3, [sp, #16]
	cmp	r3, #4
	beq	.L50
	cmp	r3, #5
	beq	.L51
	cmp	r3, #2
	bne	.L9
	ldr	r3, .L70+68
	ldr	r0, .L70+112
	ldr	r1, [r3, r4, asl #2]
	bl	Alert_Message_va
	ldr	r3, .L70+12
	mov	r2, #24
	mla	r3, r2, r4, r3
	mov	r2, #0
	str	r2, [r3, #20]
	b	.L69
.L50:
	mla	r5, r6, r4, r5
	ldrb	r3, [r5, #20]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L52
	ldr	r3, .L70+68
	ldr	r0, .L70+116
	ldr	r1, [r3, r4, asl #2]
	bl	Alert_Message_va
	b	.L9
.L52:
	ldr	r3, .L70+12
	mov	r2, #24
	mla	r3, r2, r4, r3
	ldr	r3, [r3, #20]
	cmp	r3, #0
	beq	.L53
	ldrh	r3, [r3, #20]
	mov	r1, #2
	cmp	r3, #0
	str	r1, [r5, #16]
	mov	r0, r4
	beq	.L66
.L54:
	bl	kick_off_a_block_by_feeding_the_uart
	b	.L9
.L53:
	ldr	r3, .L70+68
	ldr	r0, .L70+120
	ldr	r1, [r3, r4, asl #2]
	bl	Alert_Message_va
	b	.L68
.L51:
	mov	r0, r4
	mov	r1, #1
	bl	Alert_cts_timeout
	ldr	r3, .L70+12
	mov	r2, #24
	mla	r3, r2, r4, r3
	ldr	r3, [r3, #20]
	cmp	r3, #0
	beq	.L55
	mov	r0, #100
	bl	Alert_comm_command_failure
	ldr	r0, .L70+124
	ldr	r1, .L70+128
	mov	r2, #49
	bl	strlcpy
.L55:
	ldr	r3, .L70+8
	mov	r1, #0
	ldr	r0, [r3, r4, asl #2]
	mvn	r2, #0
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r3, .L70+12
	mov	r6, #24
	mla	r6, r4, r6, r3
	b	.L56
.L57:
	ldr	r0, [r5, #16]
	ldr	r1, .L70+60
	mov	r2, #592
	bl	mem_free_debug
	mov	r0, r5
	ldr	r1, .L70+60
	ldr	r2, .L70+132
	bl	mem_free_debug
.L56:
	mov	r0, r6
	ldr	r1, .L70+60
	ldr	r2, .L70+136
	bl	nm_ListRemoveHead_debug
	subs	r5, r0, #0
	bne	.L57
	ldr	r3, .L70+8
	mov	r1, r5
	ldr	r0, [r3, r4, asl #2]
	mov	r2, r5
	mov	r3, r5
	bl	xQueueGenericSend
	str	r5, [r6, #20]
.L69:
	mov	r1, #1
	str	r1, [r7, #16]
	b	.L65
.L14:
.LBE20:
.LBB21:
	cmp	r4, #0
.LBE21:
	ldr	r3, [sp, #16]
.LBB22:
	beq	.L58
	ldr	r0, .L70+140
	bl	Alert_Message
	mla	r5, r6, r4, r5
	b	.L68
.L58:
	cmp	r3, #8
	bne	.L59
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L70+144
	mov	r1, #1
	ldr	r0, [r5, r3]
	mov	r2, r4
	mov	r3, r4
	bl	xTimerGenericCommand
.L68:
	mov	r1, #1
	str	r1, [r5, #16]
	b	.L65
.L59:
	cmp	r3, #9
	bne	.L9
	ldr	r0, .L70+148
	bl	Alert_Message
	mov	r1, #1
	str	r1, [sl, #16]
.L65:
	mov	r0, r4
.L66:
	bl	postSerportDrvrEvent
.L9:
.LBE22:
	bl	xTaskGetTickCount
	ldr	r3, .L70+152
	ldr	r2, [sp, #12]
	str	r0, [r3, r2, asl #2]
	b	.L63
.L5:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L71:
	.align	2
.L70:
	.word	Task_Table
	.word	.LC1
	.word	xmit_list_MUTEX
	.word	xmit_cntrl
	.word	4280
	.word	.LANCHOR0
	.word	.LC2
	.word	config_c
	.word	port_device_table
	.word	1073905664
	.word	4300
	.word	8580
	.word	12860
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	port_names
	.word	.LC7
	.word	318
	.word	319
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.word	406
	.word	.LC11
	.word	422
	.word	434
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	GuiVar_CommTestStatus
	.word	.LC15
	.word	593
	.word	590
	.word	.LC16
	.word	4276
	.word	.LC17
	.word	task_last_execution_stamp
.LFE6:
	.size	serial_driver_task, .-serial_driver_task
	.global	SerDrvrVars_s
	.section	.bss.SerDrvrVars_s,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	SerDrvrVars_s, %object
	.size	SerDrvrVars_s, 21400
SerDrvrVars_s:
	.space	21400
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"SerPort queue overflow!\000"
.LC1:
	.ascii	"PORT out of range!\000"
.LC2:
	.ascii	"Serial_Drvr is DISABLED!\000"
.LC3:
	.ascii	"UPORT_TP ERROR: CTS went FALSE!\000"
.LC4:
	.ascii	"Packet(s) dropped : NOT connected\000"
.LC5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/serport_drvr.c\000"
.LC6:
	.ascii	"ERROR: State is IDLE yet TX in progress!\000"
.LC7:
	.ascii	"%s: IDLE - tx blk sent event\000"
.LC8:
	.ascii	"%s: UNDERWAY - tx blk sent now_xmitting NULL\000"
.LC9:
	.ascii	"%s: UNDERWAY - tx blk sent length != 0\000"
.LC10:
	.ascii	"Unexpectedly part of flow control.\000"
.LC11:
	.ascii	"TX BLK IN PROG : not at head of list\000"
.LC12:
	.ascii	"%s: CTS_WAIT - tx blk sent event\000"
.LC13:
	.ascii	"%s: CTS_WAIT - cts okay again but it is false\000"
.LC14:
	.ascii	"%s: CTS_WAIT - cts okay again but no block\000"
.LC15:
	.ascii	"Communication failed: CTS Timeout\000"
.LC16:
	.ascii	"Port in unexpected state!\000"
.LC17:
	.ascii	"UPORT_TP: flow control timeout\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI1-.LFB6
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/serport_drvr.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x72
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF7
	.byte	0x1
	.4byte	.LASF8
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x35
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x268
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0x29
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0x3c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x14d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1ec
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x2a0
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB6
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI2
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"serdrvr_flow_control_wait\000"
.LASF2:
	.ascii	"serdrvr_idle\000"
.LASF8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/serport_drvr.c\000"
.LASF0:
	.ascii	"serdrvr_disabled\000"
.LASF5:
	.ascii	"postSerportDrvrEvent\000"
.LASF3:
	.ascii	"serdrvr_tx_blk_in_prog\000"
.LASF7:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"serdrvr_cts_wait\000"
.LASF6:
	.ascii	"serial_driver_task\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
