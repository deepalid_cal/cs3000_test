	.file	"system_report_data.c"
	.text
.Ltext0:
	.section	.text.nm_init_system_report_records,"ax",%progbits
	.align	2
	.type	nm_init_system_report_records, %function
nm_init_system_report_records:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r0, .L4
	mov	r1, #0
	mov	r2, #60
	bl	memset
	ldr	r6, .L4
	mov	r4, #0
	mov	r5, #76
.L2:
	mla	r0, r5, r4, r6
.LBB9:
	mov	r1, #0
	add	r0, r0, #60
	mov	r2, #76
.LBE9:
	add	r4, r4, #1
.LBB10:
	bl	memset
.LBE10:
	cmp	r4, #480
	bne	.L2
	ldmfd	sp!, {r4, r5, r6, pc}
.L5:
	.align	2
.L4:
	.word	.LANCHOR0
.LFE1:
	.size	nm_init_system_report_records, .-nm_init_system_report_records
	.section	.text.system_report_data_ci_timer_callback,"ax",%progbits
	.align	2
	.type	system_report_data_ci_timer_callback, %function
system_report_data_ci_timer_callback:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, #0
	mov	r0, #404
	mov	r2, #512
	mov	r3, r1
	b	CONTROLLER_INITIATED_post_to_messages_queue
.LFE5:
	.size	system_report_data_ci_timer_callback, .-system_report_data_ci_timer_callback
	.section	.text.nm_system_report_data_updater,"ax",%progbits
	.align	2
	.type	nm_system_report_data_updater, %function
nm_system_report_data_updater:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r4, r0
	bne	.L8
	ldr	r0, .L10
	mov	r1, r4
	ldmfd	sp!, {r4, lr}
	b	Alert_Message_va
.L8:
	ldr	r0, .L10+4
	mov	r1, #1
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	bne	.L9
	ldmfd	sp!, {r4, lr}
	b	nm_init_system_report_records
.L9:
	ldr	r0, .L10+8
	ldmfd	sp!, {r4, lr}
	b	Alert_Message
.L11:
	.align	2
.L10:
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE2:
	.size	nm_system_report_data_updater, .-nm_system_report_data_updater
	.section	.text.init_file_system_report_records,"ax",%progbits
	.align	2
	.global	init_file_system_report_records
	.type	init_file_system_report_records, %function
init_file_system_report_records:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L13
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r4, .L13+4
	sub	sp, sp, #28
.LCFI3:
	str	r3, [sp, #0]
	ldr	r3, .L13+8
	mov	r0, #1
	str	r3, [sp, #4]
	ldr	r3, .L13+12
	ldr	r1, .L13+16
	str	r3, [sp, #8]
	ldr	r3, .L13+20
	mov	r2, r0
	ldr	r3, [r3, #0]
	str	r3, [sp, #12]
	ldr	r3, .L13+24
	str	r3, [sp, #16]
	ldr	r3, .L13+28
	str	r3, [sp, #20]
	mov	r3, #2
	str	r3, [sp, #24]
	mov	r3, r4
	bl	FLASH_FILE_find_or_create_reports_file
	mov	r3, #0
	str	r3, [r4, #24]
	add	sp, sp, #28
	ldmfd	sp!, {r4, pc}
.L14:
	.align	2
.L13:
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR3
	.word	36540
	.word	.LANCHOR1
	.word	system_report_completed_records_recursive_MUTEX
	.word	nm_system_report_data_updater
	.word	nm_init_system_report_records
.LFE3:
	.size	init_file_system_report_records, .-init_file_system_report_records
	.section	.text.save_file_system_report_records,"ax",%progbits
	.align	2
	.global	save_file_system_report_records
	.type	save_file_system_report_records, %function
save_file_system_report_records:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L16
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI4:
	ldr	r1, .L16+4
	str	r3, [sp, #0]
	ldr	r3, .L16+8
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, r0
	str	r3, [sp, #4]
	mov	r3, #2
	str	r3, [sp, #8]
	ldr	r3, .L16+12
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L17:
	.align	2
.L16:
	.word	36540
	.word	.LANCHOR1
	.word	system_report_completed_records_recursive_MUTEX
	.word	.LANCHOR0
.LFE4:
	.size	save_file_system_report_records, .-save_file_system_report_records
	.global	__udivsi3
	.section	.text.SYSTEM_REPORT_DATA_start_the_ci_timer_if_it_is_not_running,"ax",%progbits
	.align	2
	.global	SYSTEM_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	.type	SYSTEM_REPORT_DATA_start_the_ci_timer_if_it_is_not_running, %function
SYSTEM_REPORT_DATA_start_the_ci_timer_if_it_is_not_running:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI5:
	ldr	r4, .L21
	ldr	r2, [r4, #0]
	cmp	r2, #0
	bne	.L19
	ldr	r3, .L21+4
	ldr	r0, .L21+8
	str	r3, [sp, #0]
	ldr	r1, .L21+12
	mov	r3, r2
	bl	xTimerCreate
	cmp	r0, #0
	str	r0, [r4, #0]
	bne	.L19
	ldr	r0, .L21+16
	bl	RemovePathFromFileName
	mov	r2, #272
	mov	r1, r0
	ldr	r0, .L21+20
	bl	Alert_Message_va
.L19:
	ldr	r5, .L21
	ldr	r0, [r5, #0]
	cmp	r0, #0
	beq	.L18
	bl	xTimerIsTimerActive
	subs	r4, r0, #0
	bne	.L18
	ldr	r3, .L21+24
	ldr	r5, [r5, #0]
	ldr	r0, [r3, #48]
	bl	CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, r4
	mov	r2, r0
	mov	r0, r5
	bl	xTimerGenericCommand
.L18:
	ldmfd	sp!, {r3, r4, r5, pc}
.L22:
	.align	2
.L21:
	.word	.LANCHOR4
	.word	system_report_data_ci_timer_callback
	.word	.LC3
	.word	12000
	.word	.LC4
	.word	.LC5
	.word	config_c
.LFE6:
	.size	SYSTEM_REPORT_DATA_start_the_ci_timer_if_it_is_not_running, .-SYSTEM_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	.section	.text.nm_SYSTEM_REPORT_DATA_inc_index,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_REPORT_DATA_inc_index
	.type	nm_SYSTEM_REPORT_DATA_inc_index, %function
nm_SYSTEM_REPORT_DATA_inc_index:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #0]
	add	r3, r3, #1
	cmp	r3, #480
	movcs	r3, #0
	str	r3, [r0, #0]
	bx	lr
.LFE7:
	.size	nm_SYSTEM_REPORT_DATA_inc_index, .-nm_SYSTEM_REPORT_DATA_inc_index
	.section	.text.nm_SYSTEM_REPORT_RECORDS_get_previous_completed_record,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_REPORT_RECORDS_get_previous_completed_record
	.type	nm_SYSTEM_REPORT_RECORDS_get_previous_completed_record, %function
nm_SYSTEM_REPORT_RECORDS_get_previous_completed_record:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L34
	add	r2, r3, #60
	cmp	r0, r2
	bcc	.L32
	ldr	r1, .L34+4
	cmp	r0, r1
	bcs	.L32
	ldr	r1, [r3, #12]
	cmp	r1, #1
	beq	.L32
	cmp	r0, r2
	subne	r0, r0, #76
	bne	.L27
	ldr	r0, [r3, #8]
	ldr	r3, .L34+8
	cmp	r0, #1
	moveq	r0, r3
	movne	r0, #0
	b	.L27
.L32:
	mov	r0, #0
.L27:
	ldr	r3, .L34
	mov	r1, #76
	ldr	r2, [r3, #4]
	mla	r2, r1, r2, r3
	add	r2, r2, #60
	cmp	r0, r2
	moveq	r2, #1
	streq	r2, [r3, #12]
	bx	lr
.L35:
	.align	2
.L34:
	.word	.LANCHOR0
	.word	.LANCHOR0+36540
	.word	.LANCHOR0+36464
.LFE9:
	.size	nm_SYSTEM_REPORT_RECORDS_get_previous_completed_record, .-nm_SYSTEM_REPORT_RECORDS_get_previous_completed_record
	.section	.text.nm_SYSTEM_REPORT_RECORDS_get_most_recently_completed_record,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_REPORT_RECORDS_get_most_recently_completed_record
	.type	nm_SYSTEM_REPORT_RECORDS_get_most_recently_completed_record, %function
nm_SYSTEM_REPORT_RECORDS_get_most_recently_completed_record:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L37
	mov	r2, #0
	str	r2, [r3, #12]
	ldr	r2, [r3, #4]
	mov	r1, #76
	mla	r3, r1, r2, r3
	add	r0, r3, #60
	b	nm_SYSTEM_REPORT_RECORDS_get_previous_completed_record
.L38:
	.align	2
.L37:
	.word	.LANCHOR0
.LFE10:
	.size	nm_SYSTEM_REPORT_RECORDS_get_most_recently_completed_record, .-nm_SYSTEM_REPORT_RECORDS_get_most_recently_completed_record
	.section	.text.FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_lines,"ax",%progbits
	.align	2
	.global	FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_lines
	.type	FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_lines, %function
FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_lines:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI6:
	ldr	r5, .L47
	mov	r4, r0
	mov	r1, #400
	ldr	r2, .L47+4
	ldr	r3, .L47+8
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_group_ID
	subs	r4, r0, #0
	bne	.L40
	ldr	r0, .L47+4
	bl	RemovePathFromFileName
	ldr	r2, .L47+12
	mov	r1, r0
	ldr	r0, .L47+16
	bl	Alert_Message_va
.L40:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L47+20
	ldr	r5, .L47+24
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L47+28
	ldr	r2, .L47+4
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #0]
	cmp	r3, #0
	bne	.L41
	mov	r0, #1920
	ldr	r1, .L47+4
	mov	r2, #528
	bl	mem_malloc_debug
	str	r0, [r5, #0]
.L41:
	mov	r0, r4
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldr	r3, .L47+24
	mov	r5, #1
	ldr	r6, [r3, #0]
	ldr	r7, .L47+32
	add	r0, r0, #4
	str	r0, [r6, #0]
	bl	nm_SYSTEM_REPORT_RECORDS_get_most_recently_completed_record
	b	.L42
.L46:
	ldr	r3, [r0, #0]
	cmp	r3, r4
	streq	r0, [r6, r5, asl #2]
	addeq	r5, r5, #1
	cmp	r5, r7
	bls	.L44
	ldr	r0, .L47+4
	bl	RemovePathFromFileName
	mov	r2, #556
	mov	r1, r0
	ldr	r0, .L47+36
	bl	Alert_Message_va
	b	.L45
.L44:
	bl	nm_SYSTEM_REPORT_RECORDS_get_previous_completed_record
.L42:
	cmp	r0, #0
	bne	.L46
.L45:
	ldr	r3, .L47+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L48:
	.align	2
.L47:
	.word	list_system_recursive_MUTEX
	.word	.LC4
	.word	495
	.word	503
	.word	.LC6
	.word	system_report_completed_records_recursive_MUTEX
	.word	.LANCHOR5
	.word	515
	.word	479
	.word	.LC7
.LFE11:
	.size	FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_lines, .-FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_lines
	.section	.text.FDTO_SYSTEM_REPORT_load_guivars_for_scroll_line,"ax",%progbits
	.align	2
	.global	FDTO_SYSTEM_REPORT_load_guivars_for_scroll_line
	.type	FDTO_SYSTEM_REPORT_load_guivars_for_scroll_line, %function
FDTO_SYSTEM_REPORT_load_guivars_for_scroll_line:
.LFB12:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L50+4
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	ldr	r3, [r3, #0]
	ldr	r5, .L50+8
	mov	r0, r0, asl #16
	ldr	r4, [r3, r0, asr #14]
	sub	sp, sp, #20
.LCFI8:
	ldr	r0, [r5, #0]
	mov	r1, #400
	ldr	r2, .L50+12
	ldr	r3, .L50+16
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #250
	ldrh	r2, [r4, #8]
	add	r0, sp, #4
	str	r3, [sp, #0]
	mov	r1, #16
	mov	r3, #150
	bl	GetDateStr
	mov	r2, #6
	mov	r1, r0
	ldr	r0, .L50+20
	bl	strlcpy
	flds	s14, [r4, #60]	@ int
	flds	s13, .L50
	ldr	r3, .L50+24
	fuitos	s15, s14
	flds	s14, [r4, #64]
	ldr	r0, [r5, #0]
	fdivs	s15, s15, s13
	fsts	s15, [r3, #0]
	fcvtds	d7, s14
	ldr	r3, .L50+28
	fstd	d7, [r3, #0]
	flds	s14, [r4, #52]	@ int
	ldr	r3, .L50+32
	fuitos	s15, s14
	flds	s14, [r4, #56]
	fdivs	s15, s15, s13
	fsts	s15, [r3, #0]
	fcvtds	d7, s14
	ldr	r3, .L50+36
	fstd	d7, [r3, #0]
	flds	s14, [r4, #44]	@ int
	ldr	r3, .L50+40
	fuitos	s15, s14
	flds	s14, [r4, #48]
	fdivs	s15, s15, s13
	fsts	s15, [r3, #0]
	fcvtds	d7, s14
	ldr	r3, .L50+44
	fstd	d7, [r3, #0]
	flds	s14, [r4, #28]	@ int
	ldr	r3, .L50+48
	fuitos	s15, s14
	flds	s14, [r4, #32]
	fdivs	s15, s15, s13
	fsts	s15, [r3, #0]
	fcvtds	d7, s14
	ldr	r3, .L50+52
	fstd	d7, [r3, #0]
	flds	s14, [r4, #36]	@ int
	ldr	r3, .L50+56
	fuitos	s15, s14
	flds	s14, [r4, #40]
	fdivs	s15, s15, s13
	fsts	s15, [r3, #0]
	fcvtds	d7, s14
	ldr	r3, .L50+60
	fstd	d7, [r3, #0]
	flds	s14, [r4, #20]	@ int
	ldr	r3, .L50+64
	fuitos	s15, s14
	flds	s14, [r4, #24]
	fdivs	s15, s15, s13
	fsts	s15, [r3, #0]
	fcvtds	d7, s14
	ldr	r3, .L50+68
	fstd	d7, [r3, #0]
	flds	s14, [r4, #68]	@ int
	ldr	r3, .L50+72
	fuitos	s15, s14
	flds	s14, [r4, #72]
	fdivs	s13, s15, s13
	fcvtds	d7, s14
	fsts	s13, [r3, #0]
	ldr	r3, .L50+76
	fstd	d7, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, pc}
.L51:
	.align	2
.L50:
	.word	1114636288
	.word	.LANCHOR5
	.word	system_report_completed_records_recursive_MUTEX
	.word	.LC4
	.word	601
	.word	GuiVar_RptDate
	.word	GuiVar_RptIrrigMin
	.word	GuiVar_RptIrrigGal
	.word	GuiVar_RptManualPMin
	.word	GuiVar_RptManualPGal
	.word	GuiVar_RptManualMin
	.word	GuiVar_RptManualGal
	.word	GuiVar_RptTestMin
	.word	GuiVar_RptTestGal
	.word	GuiVar_RptWalkThruMin
	.word	GuiVar_RptWalkThruGal
	.word	GuiVar_RptRReMin
	.word	GuiVar_RptRReGal
	.word	GuiVar_RptNonCMin
	.word	GuiVar_RptNonCGal
.LFE12:
	.size	FDTO_SYSTEM_REPORT_load_guivars_for_scroll_line, .-FDTO_SYSTEM_REPORT_load_guivars_for_scroll_line
	.section	.text.nm_SYSTEM_REPORT_close_and_start_a_new_record,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_REPORT_close_and_start_a_new_record
	.type	nm_SYSTEM_REPORT_close_and_start_a_new_record, %function
nm_SYSTEM_REPORT_close_and_start_a_new_record:
.LFB13:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L57
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI9:
	ldr	r6, .L57+4
	sub	sp, sp, #36
.LCFI10:
	mov	r4, r0
	ldr	r2, .L57+8
	ldr	r0, [r3, #0]
	mov	r5, r1
	ldr	r3, .L57+12
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r6, #4]
	mov	r2, #76
	mla	r0, r2, r0, r6
	add	r1, r4, #4
	add	r0, r0, #60
	bl	memcpy
.LBB11:
	add	r0, r6, #4
	bl	nm_SYSTEM_REPORT_DATA_inc_index
	ldr	r3, [r6, #4]
	cmp	r3, #0
	moveq	r2, #1
	streq	r2, [r6, #8]
	ldr	r2, [r6, #16]
	cmp	r3, r2
	bne	.L54
	ldr	r0, .L57+16
	bl	nm_SYSTEM_REPORT_DATA_inc_index
.L54:
	ldr	r2, [r6, #4]
	ldr	r3, [r6, #20]
	cmp	r2, r3
	bne	.L55
	ldr	r0, .L57+20
	bl	nm_SYSTEM_REPORT_DATA_inc_index
.L55:
.LBE11:
	ldr	r3, .L57
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	SYSTEM_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	mov	r0, #2
	mov	r1, r0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.LBB12:
	mov	r2, #76
	add	r0, r4, #4
	mov	r1, #0
	bl	memset
.LBE12:
	ldrb	r2, [r5, #5]	@ zero_extendqisi2
	ldrb	r3, [r5, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	strh	r3, [r4, #12]	@ movhi
	ldrb	r2, [r5, #1]	@ zero_extendqisi2
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r5, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r5, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	str	r3, [r4, #8]
	ldr	r3, [r4, #0]
	str	r3, [r4, #4]
	ldr	r3, .L57+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #103
	bne	.L52
.LBB13:
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L57+28
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
.L52:
.LBE13:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, pc}
.L58:
	.align	2
.L57:
	.word	system_report_completed_records_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC4
	.word	658
	.word	.LANCHOR0+16
	.word	.LANCHOR0+20
	.word	GuiLib_CurStructureNdx
	.word	FDTO_SYSTEM_REPORT_redraw_scrollbox
.LFE13:
	.size	nm_SYSTEM_REPORT_close_and_start_a_new_record, .-nm_SYSTEM_REPORT_close_and_start_a_new_record
	.section	.text.SYSTEM_REPORT_free_report_support,"ax",%progbits
	.align	2
	.global	SYSTEM_REPORT_free_report_support
	.type	SYSTEM_REPORT_free_report_support, %function
SYSTEM_REPORT_free_report_support:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI11:
	ldr	r4, .L61
	ldr	r5, .L61+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L61+8
	ldr	r3, .L61+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, #0]
	cmp	r0, #0
	beq	.L60
	ldr	r1, .L61+8
	ldr	r2, .L61+16
	bl	mem_free_debug
	mov	r3, #0
	str	r3, [r5, #0]
.L60:
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L62:
	.align	2
.L61:
	.word	system_report_completed_records_recursive_MUTEX
	.word	.LANCHOR5
	.word	.LC4
	.word	771
	.word	775
.LFE14:
	.size	SYSTEM_REPORT_free_report_support, .-SYSTEM_REPORT_free_report_support
	.global	system_revision_record_counts
	.global	system_revision_record_sizes
	.global	SYSTEM_REPORT_RECORDS_FILENAME
	.global	system_report_data_completed
	.section	.rodata.system_revision_record_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	system_revision_record_sizes, %object
	.size	system_revision_record_sizes, 8
system_revision_record_sizes:
	.word	76
	.word	76
	.section	.rodata.system_revision_record_counts,"a",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	system_revision_record_counts, %object
	.size	system_revision_record_counts, 8
system_revision_record_counts:
	.word	480
	.word	480
	.section	.bss.system_report_ptrs,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	system_report_ptrs, %object
	.size	system_report_ptrs, 4
system_report_ptrs:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"SYS RPRT file unexpd update %u\000"
.LC1:
	.ascii	"SYS RPRT file update : to revision %u from %u\000"
.LC2:
	.ascii	"SYS RPRT updater error\000"
.LC3:
	.ascii	"\000"
.LC4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/system_report_data.c\000"
.LC5:
	.ascii	"Timer NOT CREATED : %s, %u\000"
.LC6:
	.ascii	"SYSTEM: unexpd results. : %s, %u\000"
.LC7:
	.ascii	"REPORTS: why so many records? : %s, %u\000"
	.section	.rodata.SYSTEM_REPORT_RECORDS_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	SYSTEM_REPORT_RECORDS_FILENAME, %object
	.size	SYSTEM_REPORT_RECORDS_FILENAME, 22
SYSTEM_REPORT_RECORDS_FILENAME:
	.ascii	"SYSTEM_REPORT_RECORDS\000"
	.section	.bss.system_report_data_ci_timer,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	system_report_data_ci_timer, %object
	.size	system_report_data_ci_timer, 4
system_report_data_ci_timer:
	.space	4
	.section	.bss.system_report_data_completed,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	system_report_data_completed, %object
	.size	system_report_data_completed, 36540
system_report_data_completed:
	.space	36540
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI5-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI6-.LFB11
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI7-.LFB12
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI9-.LFB13
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI11-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE24:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/system_report_data.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x134
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF15
	.byte	0x1
	.4byte	.LASF16
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x4e
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x146
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF2
	.byte	0x1
	.byte	0x59
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF3
	.byte	0x1
	.byte	0xfc
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.byte	0x92
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xcb
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xf0
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x104
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x124
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x17f
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x1c7
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1e7
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST5
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x251
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST6
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x28a
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST7
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x2fd
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB11
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB12
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI8
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB13
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI10
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB14
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x7c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"nm_init_system_report_record\000"
.LASF4:
	.ascii	"nm_system_report_data_updater\000"
.LASF12:
	.ascii	"FDTO_SYSTEM_REPORT_load_guivars_for_scroll_line\000"
.LASF8:
	.ascii	"nm_SYSTEM_REPORT_DATA_inc_index\000"
.LASF9:
	.ascii	"nm_SYSTEM_REPORT_RECORDS_get_previous_completed_rec"
	.ascii	"ord\000"
.LASF14:
	.ascii	"SYSTEM_REPORT_free_report_support\000"
.LASF2:
	.ascii	"nm_init_system_report_records\000"
.LASF15:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF13:
	.ascii	"nm_SYSTEM_REPORT_close_and_start_a_new_record\000"
.LASF11:
	.ascii	"FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_li"
	.ascii	"nes\000"
.LASF10:
	.ascii	"nm_SYSTEM_REPORT_RECORDS_get_most_recently_complete"
	.ascii	"d_record\000"
.LASF16:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/system_report_data.c\000"
.LASF1:
	.ascii	"nm_SYSTEM_increment_next_avail_ptr\000"
.LASF7:
	.ascii	"SYSTEM_REPORT_DATA_start_the_ci_timer_if_it_is_not_"
	.ascii	"running\000"
.LASF5:
	.ascii	"init_file_system_report_records\000"
.LASF6:
	.ascii	"save_file_system_report_records\000"
.LASF3:
	.ascii	"system_report_data_ci_timer_callback\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
