	.file	"watersense.c"
	.text
.Ltext0:
	.section	.text.WATERSENSE_get_Kmc_index_based_on_exposure,"ax",%progbits
	.align	2
	.global	WATERSENSE_get_Kmc_index_based_on_exposure
	.type	WATERSENSE_get_Kmc_index_based_on_exposure, %function
WATERSENSE_get_Kmc_index_based_on_exposure:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #4
	ldrls	r3, .L4
	movhi	r0, #1
	ldrlsb	r0, [r3, r0]	@ zero_extendqisi2
	bx	lr
.L5:
	.align	2
.L4:
	.word	.LANCHOR0
.LFE0:
	.size	WATERSENSE_get_Kmc_index_based_on_exposure, .-WATERSENSE_get_Kmc_index_based_on_exposure
	.section	.text.WATERSENSE_get_RZWWS,"ax",%progbits
	.align	2
	.global	WATERSENSE_get_RZWWS
	.type	WATERSENSE_get_RZWWS, %function
WATERSENSE_get_RZWWS:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	fmsr	s13, r0
	fmsr	s14, r1
	fmuls	s15, s13, s14
	fmsr	s13, r2
	fmuls	s13, s15, s13
	fmrs	r0, s13
	bx	lr
.LFE1:
	.size	WATERSENSE_get_RZWWS, .-WATERSENSE_get_RZWWS
	.section	.text.WATERSENSE_get_Kl,"ax",%progbits
	.align	2
	.global	WATERSENSE_get_Kl
	.type	WATERSENSE_get_Kl, %function
WATERSENSE_get_Kl:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	fmsr	s13, r0
	fmsr	s14, r1
	fmuls	s15, s13, s14
	fmsr	s13, r2
	fmuls	s13, s15, s13
	fmrs	r0, s13
	bx	lr
.LFE2:
	.size	WATERSENSE_get_Kl, .-WATERSENSE_get_Kl
	.section	.text.WATERSENSE_get_cycle_time,"ax",%progbits
	.align	2
	.global	WATERSENSE_get_cycle_time
	.type	WATERSENSE_get_cycle_time, %function
WATERSENSE_get_cycle_time:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	fmsr	s13, r1
	fmsr	s12, r2
	flds	s14, .L15
	fmsr	s15, r0
	fcmpes	s12, s13
	fmstat
	fcpysle	s15, s14
	ble	.L9
.LBB16:
	ldr	r3, .L15+4
	fsubs	s13, s12, s13
	flds	s11, [r3, #0]
	fmuls	s15, s11, s15
	fdivs	s15, s15, s13
.LBE16:
	fcmps	s15, s14
	fmstat
	fcpysgt	s15, s14
.L9:
	fmrs	r0, s15
	bx	lr
.L16:
	.align	2
.L15:
	.word	1133903872
	.word	.LANCHOR1
.LFE8:
	.size	WATERSENSE_get_cycle_time, .-WATERSENSE_get_cycle_time
	.section	.text.WATERSENSE_get_soak_time__cycle_end_to_cycle_start,"ax",%progbits
	.align	2
	.global	WATERSENSE_get_soak_time__cycle_end_to_cycle_start
	.type	WATERSENSE_get_soak_time__cycle_end_to_cycle_start, %function
WATERSENSE_get_soak_time__cycle_end_to_cycle_start:
.LFB9:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	fmsr	s15, r1
	fmsr	s14, r2
	sub	sp, sp, #4
.LCFI0:
	fmsr	s13, r0
	fmsr	s10, r3
	fcmpes	s14, s15
	fmstat
	ble	.L22
.LBB17:
	ldr	r3, .L23+4
.LBE17:
.LBB18:
	ldr	r2, .L23+8
.LBE18:
.LBB19:
	flds	s12, [r3, #0]
.LBE19:
.LBB20:
	flds	s9, [r3, #0]
.LBE20:
.LBB21:
	str	r2, [sp, #0]	@ float
	flds	s11, [r3, #0]
.LBE21:
.LBB22:
	fmuls	s13, s12, s13
	fsubs	s12, s14, s15
	fdivs	s13, s13, s12
.LBE22:
.LBB23:
	flds	s12, [sp, #0]
.LBE23:
.LBB24:
	fmuls	s14, s13, s14
	fdivs	s14, s14, s9
.LBE24:
.LBB25:
	fmuls	s14, s14, s10
.LBE25:
.LBB26:
	fmuls	s14, s14, s11
	fdivs	s15, s14, s15
	fdivs	s15, s15, s12
.LBE26:
	fsubs	s15, s15, s13
	b	.L18
.L22:
	flds	s15, .L23
.L18:
	fmrs	r0, s15
	add	sp, sp, #4
	bx	lr
.L24:
	.align	2
.L23:
	.word	1123024896
	.word	.LANCHOR1
	.word	1120403456
.LFE9:
	.size	WATERSENSE_get_soak_time__cycle_end_to_cycle_start, .-WATERSENSE_get_soak_time__cycle_end_to_cycle_start
	.section	.text.WATERSENSE_reduce_moisture_balance_by_ETc,"ax",%progbits
	.align	2
	.global	WATERSENSE_reduce_moisture_balance_by_ETc
	.type	WATERSENSE_reduce_moisture_balance_by_ETc, %function
WATERSENSE_reduce_moisture_balance_by_ETc:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	fmsr	s14, r0
	fmsr	s13, r2
	fmsr	s15, r1
	fnmacs	s13, s14, s15
	fmrs	r0, s13
	bx	lr
.LFE10:
	.size	WATERSENSE_reduce_moisture_balance_by_ETc, .-WATERSENSE_reduce_moisture_balance_by_ETc
	.section	.text.WATERSENSE_increase_moisture_balance_by_irrigation,"ax",%progbits
	.align	2
	.global	WATERSENSE_increase_moisture_balance_by_irrigation
	.type	WATERSENSE_increase_moisture_balance_by_irrigation, %function
WATERSENSE_increase_moisture_balance_by_irrigation:
.LFB11:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	flds	s14, [sp, #0]
	fmsr	s10, r3
.LBB27:
	fmsr	s11, r0
	ldr	r3, .L29
.LBE27:
	fsubs	s13, s10, s14
.LBB28:
	fmsr	s10, r2
	flds	s15, [r3, #0]
	fmuls	s12, s11, s10
.LBE28:
	fmsr	s11, r1
.LBB29:
	fdivs	s15, s12, s15
.LBE29:
	fcmps	s15, s13
	fmstat
	fcpysgt	s15, s13
	fmacs	s14, s15, s11
	fmrs	r0, s14
	bx	lr
.L30:
	.align	2
.L29:
	.word	.LANCHOR1
.LFE11:
	.size	WATERSENSE_increase_moisture_balance_by_irrigation, .-WATERSENSE_increase_moisture_balance_by_irrigation
	.section	.text.WATERSENSE_increase_moisture_balance_by_rain,"ax",%progbits
	.align	2
	.global	WATERSENSE_increase_moisture_balance_by_rain
	.type	WATERSENSE_increase_moisture_balance_by_rain, %function
WATERSENSE_increase_moisture_balance_by_rain:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
.LBB30:
	fmsr	s12, r2
	fmsr	s13, r0
.LBE30:
	fmsr	s14, r3
	stmfd	sp!, {r0, lr}
.LCFI1:
.LBB31:
	fmuls	s15, s12, s13
.LBE31:
	fmsr	s12, r1
	fsubs	s13, s12, s14
	fcmps	s15, s13
	fmstat
	fcpysgt	s15, s13
	fadds	s15, s14, s15
	flds	s14, .L37
	fcmpes	s15, s14
	fmstat
	ble	.L33
	fcvtds	d6, s15
	ldr	r3, .L37+8
	ldr	r0, .L37+12
	str	r3, [sp, #0]
	mov	r3, #0
	fmrrd	r1, r2, d6
	bl	Alert_Message_va
	flds	s15, .L37+4
.L33:
	fmrs	r0, s15
	ldmfd	sp!, {r3, pc}
.L38:
	.align	2
.L37:
	.word	1065353216
	.word	1056964608
	.word	1071644672
	.word	.LC0
.LFE12:
	.size	WATERSENSE_increase_moisture_balance_by_rain, .-WATERSENSE_increase_moisture_balance_by_rain
	.section	.text.WATERSENSE_verify_watersense_compliance,"ax",%progbits
	.align	2
	.global	WATERSENSE_verify_watersense_compliance
	.type	WATERSENSE_verify_watersense_compliance, %function
WATERSENSE_verify_watersense_compliance:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	mov	r4, #0
	b	.L40
.L43:
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	subs	r5, r0, #0
	beq	.L41
	bl	SCHEDULE_get_enabled
	cmp	r0, #0
	beq	.L41
	mov	r0, r5
	bl	DAILY_ET_get_et_in_use
	cmp	r0, #0
	beq	.L42
	mov	r0, r5
	bl	RAIN_get_rain_in_use
	cmp	r0, #0
	bne	.L41
.L42:
	mov	r0, r5
	bl	nm_GROUP_get_name
	bl	Alert_group_not_watersense_compliant
.L41:
	add	r4, r4, #1
.L40:
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r4, r0
	bcc	.L43
	ldmfd	sp!, {r4, r5, pc}
.LFE13:
	.size	WATERSENSE_verify_watersense_compliance, .-WATERSENSE_verify_watersense_compliance
	.global	PR
	.global	IR
	.global	AW
	.global	MAD
	.global	ROOT_ZONE_DEPTH
	.global	Kmc
	.global	Kd
	.global	Ks
	.global	ASA
	.section	.rodata.CSWTCH.5,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	CSWTCH.5, %object
	.size	CSWTCH.5, 5
CSWTCH.5:
	.byte	2
	.byte	1
	.byte	1
	.byte	1
	.byte	0
	.section	.rodata.MAD,"a",%progbits
	.align	2
	.type	MAD, %object
	.size	MAD, 28
MAD:
	.word	1051931443
	.word	1053609165
	.word	1056964608
	.word	1056964608
	.word	1057803469
	.word	1056964608
	.word	1056964608
	.section	.rodata.ASA,"a",%progbits
	.align	2
	.type	ASA, %object
	.size	ASA, 112
ASA:
	.word	1045220557
	.word	1041865114
	.word	1036831949
	.word	1036831949
	.word	1047233823
	.word	1044549468
	.word	1042536202
	.word	1040522936
	.word	1048911544
	.word	1046562734
	.word	1043878380
	.word	1041865114
	.word	1050253722
	.word	1048576000
	.word	1045891645
	.word	1043207291
	.word	1051260355
	.word	1049918177
	.word	1047904911
	.word	1045220557
	.word	1052266988
	.word	1050253722
	.word	1048911544
	.word	1046562734
	.word	1053609165
	.word	1051931443
	.word	1050253722
	.word	1048576000
	.section	.rodata.AW,"a",%progbits
	.align	2
	.type	AW, %object
	.size	AW, 28
AW:
	.word	1043207291
	.word	1043207291
	.word	1043878380
	.word	1043207291
	.word	1040522936
	.word	1035489772
	.word	1031127695
	.section	.rodata.Kd,"a",%progbits
	.align	2
	.type	Kd, %object
	.size	Kd, 168
Kd:
	.word	1058642330
	.word	1065353216
	.word	1065353216
	.word	1058642330
	.word	1065353216
	.word	1065353216
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1058642330
	.word	1066192077
	.word	1067869798
	.word	1058642330
	.word	1066192077
	.word	1067869798
	.word	1058642330
	.word	1066192077
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1066192077
	.word	1058642330
	.word	1065353216
	.word	1065353216
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"MB out of range (%0.2f); reset to %0.2f\000"
	.section	.rodata.IR,"a",%progbits
	.align	2
	.type	IR, %object
	.size	IR, 28
IR:
	.word	1036831949
	.word	1041865114
	.word	1045220557
	.word	1051931443
	.word	1053609165
	.word	1056964608
	.word	1058642330
	.section	.rodata.Kmc,"a",%progbits
	.align	2
	.type	Kmc, %object
	.size	Kmc, 168
Kmc:
	.word	1061997773
	.word	1065353216
	.word	1067030938
	.word	1061997773
	.word	1065353216
	.word	1067030938
	.word	1061997773
	.word	1065353216
	.word	1067030938
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1068708659
	.word	1056964608
	.word	1065353216
	.word	1067030938
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1056964608
	.word	1065353216
	.word	1068708659
	.word	1056964608
	.word	1065353216
	.word	1068708659
	.word	1056964608
	.word	1065353216
	.word	1068708659
	.word	1056964608
	.word	1065353216
	.word	1067869798
	.word	1061997773
	.word	1065353216
	.word	1067030938
	.section	.rodata.PR,"a",%progbits
	.align	2
	.type	PR, %object
	.size	PR, 52
PR:
	.word	1071225242
	.word	1061158912
	.word	1056964608
	.word	1056964608
	.word	1065353216
	.word	1061158912
	.word	1059481190
	.word	1056964608
	.word	1065353216
	.word	1061158912
	.word	1076048691
	.word	1045220557
	.word	1065353216
	.section	.data.MINUTES_PER_HOUR,"aw",%progbits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	MINUTES_PER_HOUR, %object
	.size	MINUTES_PER_HOUR, 4
MINUTES_PER_HOUR:
	.word	1114636288
	.section	.rodata.Ks,"a",%progbits
	.align	2
	.type	Ks, %object
	.size	Ks, 56
Ks:
	.word	1061997773
	.word	1058642330
	.word	1061158912
	.word	1056964608
	.word	1056964608
	.word	1056964608
	.word	1060320051
	.word	1056964608
	.word	1045220557
	.word	1063675494
	.word	1056964608
	.word	1045220557
	.word	1056964608
	.word	1060320051
	.section	.rodata.ROOT_ZONE_DEPTH,"a",%progbits
	.align	2
	.type	ROOT_ZONE_DEPTH, %object
	.size	ROOT_ZONE_DEPTH, 392
ROOT_ZONE_DEPTH:
	.word	1082130432
	.word	1082130432
	.word	1082130432
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1082130432
	.word	1082130432
	.word	1082130432
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1082130432
	.word	1082130432
	.word	1082130432
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1103101952
	.word	1103101952
	.word	1103101952
	.word	1103101952
	.word	1103101952
	.word	1103101952
	.word	1103101952
	.word	1082130432
	.word	1082130432
	.word	1082130432
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1094713344
	.word	1099956224
	.word	1099956224
	.word	1099956224
	.word	1099956224
	.word	1099956224
	.word	1099956224
	.word	1099956224
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.word	1086324736
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI0-.LFB9
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI1-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI2-.LFB13
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/watersense.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x102
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF14
	.byte	0x1
	.4byte	.LASF15
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1e9
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x217
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x232
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x19b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x17f
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x112
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x146
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x163
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x258
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x289
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x2c6
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x2e9
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x31d
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x36d
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB9
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB12
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB13
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF12:
	.ascii	"WATERSENSE_increase_moisture_balance_by_rain\000"
.LASF2:
	.ascii	"WATERSENSE_get_Da\000"
.LASF3:
	.ascii	"WATERSENSE_get_St\000"
.LASF10:
	.ascii	"WATERSENSE_reduce_moisture_balance_by_ETc\000"
.LASF0:
	.ascii	"WATERSENSE_get_Rt_max\000"
.LASF15:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/watersense.c\000"
.LASF6:
	.ascii	"WATERSENSE_get_RZWWS\000"
.LASF14:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF11:
	.ascii	"WATERSENSE_increase_moisture_balance_by_irrigation\000"
.LASF9:
	.ascii	"WATERSENSE_get_soak_time__cycle_end_to_cycle_start\000"
.LASF5:
	.ascii	"WATERSENSE_get_Kmc_index_based_on_exposure\000"
.LASF7:
	.ascii	"WATERSENSE_get_Kl\000"
.LASF13:
	.ascii	"WATERSENSE_verify_watersense_compliance\000"
.LASF4:
	.ascii	"WATERSENSE_get_Rn\000"
.LASF1:
	.ascii	"WATERSENSE_get_I\000"
.LASF8:
	.ascii	"WATERSENSE_get_cycle_time\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
