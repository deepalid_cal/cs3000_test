	.file	"serial.c"
	.text
.Ltext0:
	.section	.text.flow_control_timer_callback,"ax",%progbits
	.align	2
	.type	flow_control_timer_callback, %function
flow_control_timer_callback:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	bl	pvTimerGetTimerID
	subs	r4, r0, #0
	beq	.L2
	ldr	r3, .L3
	ldr	r0, .L3+4
	ldr	r1, [r3, r4, asl #2]
	bl	Alert_Message_va
.L2:
	mov	r0, r4
	mov	r1, #9
	ldmfd	sp!, {r4, lr}
	b	postSerportDrvrEvent
.L4:
	.align	2
.L3:
	.word	.LANCHOR0
	.word	.LC0
.LFE11:
	.size	flow_control_timer_callback, .-flow_control_timer_callback
	.section	.text.cts_main_timer_callback,"ax",%progbits
	.align	2
	.type	cts_main_timer_callback, %function
cts_main_timer_callback:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI1:
	bl	pvTimerGetTimerID
	ldr	r2, .L6
	ldr	r3, .L6+4
	mla	r3, r2, r0, r3
	ldr	r2, [r3, #12]
	orr	r2, r2, #4
	str	r2, [r3, #12]
	ldr	pc, [sp], #4
.L7:
	.align	2
.L6:
	.word	4280
	.word	SerDrvrVars_s
.LFE9:
	.size	cts_main_timer_callback, .-cts_main_timer_callback
	.section	.text.port_a_carrier_detect_INT_HANDLER,"ax",%progbits
	.align	2
	.type	port_a_carrier_detect_INT_HANDLER, %function
port_a_carrier_detect_INT_HANDLER:
.LFB16:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI2:
	ldr	r3, .L13
	mov	r0, #56
	ldr	r2, [r3, #12]
	ands	r2, r2, #134217728
	ldrne	r2, [r3, #12]
	ldreq	r1, [r3, #12]
	bicne	r2, r2, #134217728
	orreq	r1, r1, #134217728
	strne	r2, [r3, #12]
	streq	r1, [r3, #12]
	moveq	r3, r2
	ldr	r2, .L13+4
	ldr	r1, .L13+8
	ldr	r2, [r2, #80]
	movne	r3, #1
	mla	r2, r0, r2, r1
	mov	r1, sp
	ldr	r2, [r2, #12]
	cmp	r3, r2
	ldr	r2, .L13+12
	movne	r3, #115
	moveq	r3, #116
	str	r3, [sp, #0]
	ldr	r0, [r2, #0]
	mov	r3, #0
	add	r2, sp, #20
	str	r3, [sp, #20]
	bl	xQueueGenericSendFromISR
	add	sp, sp, #24
	ldmfd	sp!, {pc}
.L14:
	.align	2
.L13:
	.word	1073807360
	.word	config_c
	.word	port_device_table
	.word	CONTROLLER_INITIATED_task_queue
.LFE16:
	.size	port_a_carrier_detect_INT_HANDLER, .-port_a_carrier_detect_INT_HANDLER
	.section	.text.standard_uart_INT_HANDLER,"ax",%progbits
	.align	2
	.type	standard_uart_INT_HANDLER, %function
standard_uart_INT_HANDLER:
.LFB0:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI3:
	mov	r3, #0
	sub	sp, sp, #32
.LCFI4:
	str	r3, [sp, #0]
	ldr	r3, [r1, #8]
	and	r3, r3, #14
	str	r3, [sp, #8]
	ldr	r3, [sp, #8]
	cmp	r3, #12
	ldrls	pc, [pc, r3, asl #2]
	b	.L16
.L21:
	.word	.L17
	.word	.L16
	.word	.L18
	.word	.L16
	.word	.L19
	.word	.L16
	.word	.L20
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L19
.L20:
	ldr	r3, [r1, #20]
	str	r3, [sp, #12]
	ldr	r3, [sp, #12]
	tst	r3, #16
	beq	.L22
	ldr	r1, .L41
	ldr	r2, .L41+4
	ldr	r3, .L41+8
	mla	r2, r1, r0, r2
	ldr	r1, [r2, r3]
	add	r1, r1, #1
	str	r1, [r2, r3]
.L22:
	ldr	r3, [sp, #12]
	tst	r3, #8
	beq	.L23
	ldr	r1, .L41
	ldr	r2, .L41+4
	ldr	r3, .L41+12
	mla	r2, r1, r0, r2
	ldr	r1, [r2, r3]
	add	r1, r1, #1
	str	r1, [r2, r3]
.L23:
	ldr	r3, [sp, #12]
	tst	r3, #4
	beq	.L24
	ldr	r1, .L41
	ldr	r2, .L41+4
	ldr	r3, .L41+16
	mla	r2, r1, r0, r2
	ldr	r1, [r2, r3]
	add	r1, r1, #1
	str	r1, [r2, r3]
.L24:
	ldr	r3, [sp, #12]
	tst	r3, #2
	beq	.L16
	ldr	r2, .L41+4
	ldr	r1, .L41
	ldr	r3, .L41+20
	mla	r0, r1, r0, r2
	ldr	r2, [r0, r3]
	add	r2, r2, #1
	b	.L40
.L19:
	ldr	r3, [r1, #28]
	ldr	r2, .L41
	and	r3, r3, #127
	str	r3, [sp, #20]
	ldr	r3, .L41+24
	ldr	r4, .L41+28
	mla	r3, r2, r0, r3
	mov	r2, #0
	str	r2, [sp, #4]
	ldr	sl, .L41+32
	ldr	r8, .L41+36
	ldr	lr, .L41+40
	ldr	r7, .L41+44
	ldr	ip, .L41+48
	ldr	r6, .L41+52
	ldr	r5, .L41+56
	b	.L25
.L31:
	mov	r2, #4096
	ldr	r9, [r3, r2]
	ldr	fp, [r1, #0]
	strb	fp, [r3, r9]
	ldr	r9, [r3, r2]
	add	r9, r9, #1
	str	r9, [r3, r2]
	ldr	r2, [r3, r2]
	cmp	r2, #4096
	moveq	r9, #0
	streq	r9, [r3, r2]
	ldr	r2, [r3, r4]
	cmp	r2, #0
	bne	.L27
	ldr	r2, [r3, sl]
	cmp	r2, #1
	bne	.L27
	mov	r2, #4096
	ldr	r9, [r3, r2]
	ldrh	r2, [r3, r8]
	rsb	r9, r2, r9
	rsbs	r2, r9, #0
	adc	r2, r2, r9
	str	r2, [r3, r4]
.L27:
	ldr	r2, [r3, lr]
	cmp	r2, #0
	bne	.L28
	ldr	r2, [r3, r7]
	cmp	r2, #1
	bne	.L28
	ldr	fp, .L41+60
	mov	r2, #4096
	ldr	r9, [r3, r2]
	ldrh	r2, [r3, fp]
	rsb	r9, r2, r9
	rsbs	r2, r9, #0
	adc	r2, r2, r9
	str	r2, [r3, lr]
.L28:
	ldr	r2, [r3, ip]
	cmp	r2, #0
	bne	.L29
	ldr	r2, [r3, r6]
	cmp	r2, #1
	bne	.L29
	mov	r2, #4096
	ldr	r9, [r3, r2]
	ldr	r2, [r3, r5]
	rsb	r9, r2, r9
	rsbs	r2, r9, #0
	adc	r2, r2, r9
	str	r2, [r3, ip]
.L29:
	ldr	r2, .L41+64
	ldr	r9, [r3, r2]
	cmp	r9, #0
	bne	.L30
	ldr	r9, .L41+68
	ldr	r9, [r3, r9]
	cmp	r9, #1
	bne	.L30
	mov	r9, #4096
	ldr	fp, [r3, r9]
	add	r9, r9, #88
	ldr	r9, [r3, r9]
	cmp	fp, r9
	movne	r9, #0
	moveq	r9, #1
	str	r9, [r3, r2]
.L30:
	ldr	r2, [sp, #4]
	add	r2, r2, #1
	str	r2, [sp, #4]
.L25:
	ldr	r9, [sp, #4]
	ldr	r2, [sp, #20]
	cmp	r9, r2
	bcc	.L31
	ldr	r1, .L41
	ldr	r2, .L41+4
	ldr	r3, .L41+72
	mla	r2, r1, r0, r2
	ldr	ip, [sp, #20]
	ldr	r1, [r2, r3]
	add	r1, ip, r1
	str	r1, [r2, r3]
	ldr	r3, .L41+76
	mov	r1, #0
	ldr	r0, [r3, r0, asl #2]
	mov	r2, sp
	mov	r3, r1
	b	.L39
.L18:
	ldr	r3, .L41+80
	mov	r2, #24
	mla	r3, r2, r0, r3
	ldr	r2, [r3, #20]
	cmp	r2, #0
	beq	.L16
	ldr	ip, .L41
	ldr	r4, .L41+4
	mul	ip, r0, ip
	add	r3, r4, ip
	ldrb	r3, [r3, #20]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L16
	ldrh	r3, [r2, #20]
	cmp	r3, #0
	bne	.L32
	ldr	r0, [r4, ip]
	mov	r2, #2
	add	r1, sp, #32
	str	r2, [r1, #-4]!
	mov	r2, sp
.L39:
	bl	xQueueGenericSendFromISR
	b	.L16
.L32:
	cmp	r3, #15
	movls	ip, r3
	movhi	ip, #16
	str	ip, [sp, #24]
	mov	ip, #0
	b	.L38
.L35:
	ldr	ip, [r2, #12]
	ldrb	r4, [ip], #1	@ zero_extendqisi2
	str	r4, [r1, #0]
	str	ip, [r2, #12]
	ldr	ip, [sp, #4]
	add	ip, ip, #1
.L38:
	str	ip, [sp, #4]
	ldr	r4, [sp, #4]
	ldr	ip, [sp, #24]
	cmp	r4, ip
	bcc	.L35
	ldr	r1, [sp, #24]
	ldr	ip, .L41
	rsb	r3, r1, r3
	strh	r3, [r2, #20]	@ movhi
	ldr	r2, .L41+4
	ldr	r3, .L41+84
	mla	r0, ip, r0, r2
	ldr	r1, [sp, #24]
	ldr	r2, [r0, r3]
	add	r2, r1, r2
.L40:
	str	r2, [r0, r3]
	b	.L16
.L17:
	ldr	r3, [r1, #24]
	str	r3, [sp, #16]
.L16:
	ldr	r3, [sp, #0]
	cmp	r3, #0
	beq	.L15
.LBB3:
	bl	vTaskSwitchContext
.L15:
.LBE3:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L42:
	.align	2
.L41:
	.word	4280
	.word	SerDrvrVars_s
	.word	4252
	.word	4248
	.word	4244
	.word	4240
	.word	SerDrvrVars_s+28
	.word	4196
	.word	4100
	.word	4124
	.word	4200
	.word	4104
	.word	4204
	.word	4108
	.word	4164
	.word	4140
	.word	4208
	.word	4116
	.word	4260
	.word	rcvd_data_binary_semaphore
	.word	.LANCHOR1
	.word	4264
.LFE0:
	.size	standard_uart_INT_HANDLER, .-standard_uart_INT_HANDLER
	.section	.text.uart5_isr,"ax",%progbits
	.align	2
	.type	uart5_isr, %function
uart5_isr:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, .L44
	mov	r0, #3
	b	standard_uart_INT_HANDLER
.L45:
	.align	2
.L44:
	.word	1074331648
.LFE5:
	.size	uart5_isr, .-uart5_isr
	.section	.text.uart6_isr,"ax",%progbits
	.align	2
	.type	uart6_isr, %function
uart6_isr:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, .L47
	mov	r0, #2
	b	standard_uart_INT_HANDLER
.L48:
	.align	2
.L47:
	.word	1074364416
.LFE4:
	.size	uart6_isr, .-uart6_isr
	.section	.text.uart3_isr,"ax",%progbits
	.align	2
	.type	uart3_isr, %function
uart3_isr:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, .L50
	mov	r0, #1
	b	standard_uart_INT_HANDLER
.L51:
	.align	2
.L50:
	.word	1074266112
.LFE3:
	.size	uart3_isr, .-uart3_isr
	.section	.text.cts_poll_timer_callback,"ax",%progbits
	.align	2
	.type	cts_poll_timer_callback, %function
cts_poll_timer_callback:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, lr}
.LCFI5:
	bl	pvTimerGetTimerID
	cmp	r0, #1
	mov	r4, r0
	bne	.L53
	ldr	r3, .L66
	ldr	r2, .L66+4
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #8]
	cmp	r3, #0
	ldr	r3, .L66+8
	ldr	r3, [r3, #0]
	beq	.L54
	tst	r3, #65536
	moveq	r3, #0
	movne	r3, #1
	b	.L55
.L54:
	tst	r3, #65536
	movne	r3, #0
	moveq	r3, #1
.L55:
	ldr	r2, .L66+12
	b	.L64
.L53:
	cmp	r0, #2
	bne	.L57
	ldr	r3, .L66
	ldr	r2, .L66+4
	ldr	r3, [r3, #84]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #8]
	cmp	r3, #0
	ldr	r3, .L66+8
	ldr	r3, [r3, #0]
	andne	r3, r3, #1
	bne	.L59
	tst	r3, #1
	movne	r3, #0
	moveq	r3, #1
.L59:
	ldr	r2, .L66+16
.L64:
	ldr	r1, .L66+20
	strb	r3, [r1, r2]
	b	.L56
.L57:
	cmp	r0, #3
	ldreq	r3, .L66+24
	ldreq	r2, .L66+20
	moveq	r1, #1
	streqb	r1, [r2, r3]
.L56:
	ldr	r5, .L66+28
	ldr	r3, .L66+20
	mla	r5, r4, r5, r3
	ldrb	r1, [r5, #20]	@ zero_extendqisi2
	and	r1, r1, #255
	cmp	r1, #1
	bne	.L60
	mvn	r3, #0
	mov	r2, #0
	str	r3, [sp, #0]
	mov	r3, r2
	ldr	r0, [r5, #4]
	bl	xTimerGenericCommand
	mov	r0, r4
	mov	r1, #4
	b	.L65
.L60:
	ldr	r6, [r5, #12]
	ands	r6, r6, #2
	bne	.L61
	ldr	r7, [r5, #4]
	bl	xTaskGetTickCount
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, r6
	mov	r3, r6
	mov	r2, r0
	mov	r0, r7
	bl	xTimerGenericCommand
	ldr	r3, [r5, #12]
	orr	r3, r3, #2
	str	r3, [r5, #12]
	ldr	r3, [r5, #12]
	bic	r3, r3, #4
	str	r3, [r5, #12]
.L61:
	ldr	r2, .L66+28
	ldr	r3, .L66+20
	mla	r3, r2, r4, r3
	ldr	r5, [r3, #12]
	ands	r5, r5, #4
	beq	.L62
	mov	r0, r4
	mov	r1, #5
.L65:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	postSerportDrvrEvent
.L62:
	ldr	r6, [r3, #8]
	bl	xTaskGetTickCount
	mov	r1, r5
	mov	r3, r5
	str	r5, [sp, #0]
	mov	r2, r0
	mov	r0, r6
	bl	xTimerGenericCommand
	cmp	r0, #1
	beq	.L52
	ldr	r3, .L66+32
	ldr	r0, .L66+36
	ldr	r1, [r3, r4, asl #2]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	Alert_Message_va
.L52:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L67:
	.align	2
.L66:
	.word	config_c
	.word	port_device_table
	.word	1073905664
	.word	4300
	.word	8580
	.word	SerDrvrVars_s
	.word	12860
	.word	4280
	.word	.LANCHOR0
	.word	.LC1
.LFE10:
	.size	cts_poll_timer_callback, .-cts_poll_timer_callback
	.section	.text.common_cts_INT_HANDLER,"ax",%progbits
	.align	2
	.type	common_cts_INT_HANDLER, %function
common_cts_INT_HANDLER:
.LFB6:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, lr}
.LCFI6:
	ldr	r6, .L70
	ldr	r7, .L70+4
	mul	r6, r0, r6
	mov	r4, #0
	add	r3, r7, r6
	strb	r4, [r3, #20]
	ldr	r2, [r3, #12]
	add	r5, sp, #12
	bic	r2, r2, #2
	str	r2, [r3, #12]
	str	r4, [r5, #-8]!
	ldr	r8, [r3, #8]
	bl	xTaskGetTickCountFromISR
	mov	r1, r4
	mov	r3, r5
	str	r4, [sp, #0]
	mov	r2, r0
	mov	r0, r8
	bl	xTimerGenericCommand
	add	r1, sp, #12
	mov	r3, #3
	str	r3, [r1, #-4]!
	ldr	r0, [r7, r6]
	mov	r3, r4
	mov	r2, r5
	bl	xQueueGenericSendFromISR
	ldr	r3, [sp, #4]
	cmp	r3, #1
	bne	.L68
.LBB4:
	bl	vTaskSwitchContext
.L68:
.LBE4:
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, pc}
.L71:
	.align	2
.L70:
	.word	4280
	.word	SerDrvrVars_s
.LFE6:
	.size	common_cts_INT_HANDLER, .-common_cts_INT_HANDLER
	.section	.text.port_b_cts_INT_HANDLER,"ax",%progbits
	.align	2
	.type	port_b_cts_INT_HANDLER, %function
port_b_cts_INT_HANDLER:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #2
	b	common_cts_INT_HANDLER
.LFE8:
	.size	port_b_cts_INT_HANDLER, .-port_b_cts_INT_HANDLER
	.section	.text.port_a_cts_INT_HANDLER,"ax",%progbits
	.align	2
	.type	port_a_cts_INT_HANDLER, %function
port_a_cts_INT_HANDLER:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #1
	b	common_cts_INT_HANDLER
.LFE7:
	.size	port_a_cts_INT_HANDLER, .-port_a_cts_INT_HANDLER
	.section	.text.uart1_isr,"ax",%progbits
	.align	2
	.type	uart1_isr, %function
uart1_isr:
.LFB2:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
.LBB8:
	ldr	r0, .L93
.LBE8:
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI7:
.LBB10:
	ldr	r3, [r0, #8]
.LBE10:
	sub	sp, sp, #40
.LCFI8:
.LBB11:
	str	r3, [sp, #24]
	ldr	r3, [sp, #24]
	mov	r1, #0
	tst	r3, #6
	str	r1, [sp, #16]
	beq	.L75
	ldr	r3, [r0, #4]
	ldr	r2, .L93+4
	and	r3, r3, #255
	str	r3, [sp, #28]
	ldr	r3, .L93+8
	str	r1, [sp, #20]
	ldr	sl, [r3, r2]
	add	r2, r2, #24
	ldrh	r8, [r3, r2]
	sub	r2, r2, #20
	ldr	r7, [r3, r2]
	add	r2, r2, #36
	ldrh	r6, [r3, r2]
	sub	r2, r2, #32
	ldr	r5, [r3, r2]
	add	r2, r2, #56
	ldr	r2, [r3, r2]
	mov	r4, #4224
	str	r2, [sp, #4]
	ldr	r2, .L93+12
	mov	lr, r3
	ldr	r2, [r3, r2]
	ldr	ip, .L93+16
	str	r2, [sp, #8]
	ldr	r2, .L93+20
	ldr	r0, .L93+24
	ldr	r2, [r3, r2]
	ldr	r1, .L93+28
	str	r2, [sp, #12]
	str	r5, [sp, #0]
	ldr	r2, .L93+32
	b	.L76
.L82:
	ldr	r5, .L93
	ldr	r9, [r3, r2]
	ldr	fp, [r5, #0]
	add	r9, r3, r9
	strb	fp, [r9, #28]
	ldr	r9, [r3, r2]
	add	r9, r9, #1
	str	r9, [r3, r2]
	ldr	r9, [r3, r2]
	cmp	r9, #4096
	moveq	r5, #0
	streq	r5, [r3, r2]
	ldr	r9, [r3, r4]
	cmp	r9, #0
	bne	.L78
	cmp	sl, #1
	bne	.L78
	ldr	r9, [r3, r2]
	rsb	fp, r8, r9
	rsbs	r9, fp, #0
	adc	r9, r9, fp
	str	r9, [r3, r4]
.L78:
	ldr	r9, [lr, ip]
	cmp	r9, #0
	bne	.L79
	cmp	r7, #1
	bne	.L79
	ldr	r9, [r3, r2]
	rsb	fp, r6, r9
	rsbs	r9, fp, #0
	adc	r9, r9, fp
	str	r9, [r3, ip]
.L79:
	ldr	r9, [r3, r0]
	cmp	r9, #0
	bne	.L80
	ldr	r5, [sp, #0]
	cmp	r5, #1
	bne	.L80
	ldr	r9, [r3, r2]
	ldr	r5, [sp, #4]
	rsb	fp, r5, r9
	rsbs	r9, fp, #0
	adc	r9, r9, fp
	str	r9, [r3, r0]
.L80:
	ldr	r9, [lr, r1]
	cmp	r9, #0
	bne	.L81
	ldr	r5, [sp, #8]
	cmp	r5, #1
	bne	.L81
	ldr	r9, [r3, r2]
	ldr	r5, [sp, #12]
	rsb	fp, r5, r9
	rsbs	r9, fp, #0
	adc	r9, r9, fp
	str	r9, [r3, r1]
.L81:
	ldr	r9, [sp, #20]
	add	r9, r9, #1
	str	r9, [sp, #20]
.L76:
	ldr	fp, [sp, #20]
	ldr	r9, [sp, #28]
	cmp	fp, r9
	ldr	r9, .L93+8
	bcc	.L82
	ldr	r3, .L93+36
	ldr	r1, [sp, #28]
	ldr	r2, [r9, r3]
	add	r2, r1, r2
	str	r2, [r9, r3]
	ldr	r3, .L93+40
	mov	r1, #0
	ldr	r0, [r3, #0]
	add	r2, sp, #16
	mov	r3, r1
	bl	xQueueGenericSendFromISR
.L75:
	ldr	r3, [sp, #24]
	tst	r3, #1
	beq	.L83
	ldr	r3, .L93+44
	ldr	r2, [r3, #20]
	cmp	r2, #0
	beq	.L83
	ldrh	r3, [r2, #20]
	cmp	r3, #0
	bne	.L84
	add	r1, sp, #40
	mov	r2, #2
	str	r2, [r1, #-4]!
	ldr	r2, .L93+8
	ldr	r0, [r2, #0]
	add	r2, sp, #16
	bl	xQueueGenericSendFromISR
	b	.L83
.L84:
	cmp	r3, #15
	movls	r1, r3
	movhi	r1, #16
	str	r1, [sp, #32]
	mov	r1, #0
	str	r1, [sp, #20]
	ldr	r0, .L93
	b	.L86
.L87:
	ldr	r1, [r2, #12]
	ldrb	ip, [r1], #1	@ zero_extendqisi2
	str	ip, [r0, #0]
	str	r1, [r2, #12]
	ldr	r1, [sp, #20]
	add	r1, r1, #1
	str	r1, [sp, #20]
.L86:
	ldr	ip, [sp, #20]
	ldr	r1, [sp, #32]
	cmp	ip, r1
	bcc	.L87
	ldr	r1, [sp, #32]
	ldr	r0, [sp, #32]
	rsb	r3, r1, r3
	strh	r3, [r2, #20]	@ movhi
	ldr	r2, .L93+8
	ldr	r3, .L93+48
	ldr	r1, [r2, r3]
	add	r1, r0, r1
	str	r1, [r2, r3]
.L83:
	ldr	r3, [sp, #24]
	tst	r3, #56
	beq	.L88
	ldr	r3, [sp, #24]
	tst	r3, #32
	ldrne	r2, .L93+8
	ldrne	r3, .L93+52
	ldrne	r1, [r2, r3]
	addne	r1, r1, #1
	strne	r1, [r2, r3]
	ldr	r3, [sp, #24]
	tst	r3, #16
	ldrne	r2, .L93+8
	ldrne	r3, .L93+56
	ldrne	r1, [r2, r3]
	addne	r1, r1, #1
	strne	r1, [r2, r3]
	ldr	r3, [sp, #24]
	tst	r3, #8
	ldrne	r2, .L93+8
	ldrne	r3, .L93+60
	ldrne	r1, [r2, r3]
	addne	r1, r1, #1
	strne	r1, [r2, r3]
.L88:
	ldr	r3, .L93
	ldr	r2, [sp, #24]
	str	r2, [r3, #8]
	ldr	r3, [sp, #16]
	cmp	r3, #1
	bne	.L74
.LBB9:
	bl	vTaskSwitchContext
.L74:
.LBE9:
.LBE11:
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L94:
	.align	2
.L93:
	.word	1073823744
	.word	4128
	.word	SerDrvrVars_s
	.word	4144
	.word	4228
	.word	4212
	.word	4232
	.word	4236
	.word	4124
	.word	4260
	.word	rcvd_data_binary_semaphore
	.word	.LANCHOR1
	.word	4264
	.word	4240
	.word	4252
	.word	4248
.LFE2:
	.size	uart1_isr, .-uart1_isr
	.section	.text.set_reset_ACTIVE_to_serial_port_device,"ax",%progbits
	.align	2
	.global	set_reset_ACTIVE_to_serial_port_device
	.type	set_reset_ACTIVE_to_serial_port_device, %function
set_reset_ACTIVE_to_serial_port_device:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	beq	.L97
	cmp	r0, #2
	bxne	lr
	b	.L104
.L97:
	ldr	r3, .L105
	ldr	r2, .L105+4
	ldr	r1, [r3, #80]
	mov	r0, #56
	mla	r1, r0, r1, r2
	mov	r0, r3
	ldr	r1, [r1, #28]
	ldr	r3, .L105+8
	cmp	r1, #0
	mov	r1, r2
	mov	r2, #8388608
	strne	r2, [r3, #100]
	streq	r2, [r3, #104]
	ldr	r3, [r0, #80]
	mov	r2, #56
	mla	r2, r3, r2, r1
	ldr	r3, .L105+12
	ldrb	r1, [r2, #28]	@ zero_extendqisi2
	b	.L103
.L104:
	ldr	r3, .L105
	ldr	r2, .L105+4
	ldr	r1, [r3, #84]
	mov	r0, #56
	mla	r1, r0, r1, r2
	mov	r0, r3
	ldr	r1, [r1, #28]
	ldr	r3, .L105+8
	cmp	r1, #0
	mov	r1, r2
	mov	r2, #64
	strne	r2, [r3, #4]
	streq	r2, [r3, #8]
	ldr	r3, [r0, #84]
	mov	r2, #56
	mla	r2, r3, r2, r1
	ldr	r3, .L105+16
	ldrb	r1, [r2, #28]	@ zero_extendqisi2
.L103:
	ldr	r2, .L105+20
	strb	r1, [r2, r3]
	bx	lr
.L106:
	.align	2
.L105:
	.word	config_c
	.word	port_device_table
	.word	1073905664
	.word	4306
	.word	8586
	.word	SerDrvrVars_s
.LFE12:
	.size	set_reset_ACTIVE_to_serial_port_device, .-set_reset_ACTIVE_to_serial_port_device
	.section	.text.set_reset_INACTIVE_to_serial_port_device,"ax",%progbits
	.align	2
	.global	set_reset_INACTIVE_to_serial_port_device
	.type	set_reset_INACTIVE_to_serial_port_device, %function
set_reset_INACTIVE_to_serial_port_device:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	beq	.L109
	cmp	r0, #2
	bxne	lr
	b	.L116
.L109:
	ldr	r3, .L117
	ldr	r2, .L117+4
	ldr	r1, [r3, #80]
	mov	r0, #56
	mla	r1, r0, r1, r2
	mov	r0, r3
	ldr	r1, [r1, #28]
	ldr	r3, .L117+8
	cmp	r1, #0
	mov	r1, r2
	mov	r2, #8388608
	strne	r2, [r3, #104]
	streq	r2, [r3, #100]
	ldr	r3, [r0, #80]
	mov	r2, #56
	mla	r2, r3, r2, r1
	ldr	r3, .L117+12
	ldr	r1, [r2, #28]
	rsbs	r1, r1, #1
	movcc	r1, #0
	b	.L115
.L116:
	ldr	r3, .L117
	ldr	r2, .L117+4
	ldr	r1, [r3, #84]
	mov	r0, #56
	mla	r1, r0, r1, r2
	mov	r0, r3
	ldr	r1, [r1, #28]
	ldr	r3, .L117+8
	cmp	r1, #0
	mov	r1, r2
	mov	r2, #64
	strne	r2, [r3, #8]
	streq	r2, [r3, #4]
	ldr	r3, [r0, #84]
	mov	r2, #56
	mla	r2, r3, r2, r1
	ldr	r3, .L117+16
	ldr	r1, [r2, #28]
	rsbs	r1, r1, #1
	movcc	r1, #0
.L115:
	ldr	r2, .L117+20
	strb	r1, [r2, r3]
	bx	lr
.L118:
	.align	2
.L117:
	.word	config_c
	.word	port_device_table
	.word	1073905664
	.word	4306
	.word	8586
	.word	SerDrvrVars_s
.LFE13:
	.size	set_reset_INACTIVE_to_serial_port_device, .-set_reset_INACTIVE_to_serial_port_device
	.section	.text.SetRTS,"ax",%progbits
	.align	2
	.global	SetRTS
	.type	SetRTS, %function
SetRTS:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	r3, r0, #1
	cmp	r3, #1
	bxhi	lr
	ldr	ip, .L127
	ldr	r3, .L127+4
	and	r2, r1, #255
	mla	r3, ip, r0, r3
	cmp	r0, #1
	strb	r2, [r3, #24]
	ldr	r3, .L127+8
	bne	.L121
	cmp	r1, #1
	mov	r2, #8388608
	b	.L126
.L121:
	cmp	r1, #1
	mov	r2, #2
.L126:
	streq	r2, [r3, #4]
	strne	r2, [r3, #8]
	bx	lr
.L128:
	.align	2
.L127:
	.word	4280
	.word	SerDrvrVars_s
	.word	1073905664
.LFE14:
	.size	SetRTS, .-SetRTS
	.section	.text.SetDTR,"ax",%progbits
	.align	2
	.global	SetDTR
	.type	SetDTR, %function
SetDTR:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	r3, r0, #1
	cmp	r3, #1
	bxhi	lr
	ldr	ip, .L137
	ldr	r3, .L137+4
	and	r2, r1, #255
	mla	r3, ip, r0, r3
	cmp	r0, #1
	strb	r2, [r3, #25]
	ldr	r3, .L137+8
	bne	.L131
	cmp	r1, #1
	mov	r2, #1048576
	b	.L136
.L131:
	cmp	r1, #1
	mov	r2, #16
.L136:
	streq	r2, [r3, #4]
	strne	r2, [r3, #8]
	bx	lr
.L138:
	.align	2
.L137:
	.word	4280
	.word	SerDrvrVars_s
	.word	1073905664
.LFE15:
	.size	SetDTR, .-SetDTR
	.section	.text.SERIAL_set_baud_rate_on_A_or_B,"ax",%progbits
	.align	2
	.global	SERIAL_set_baud_rate_on_A_or_B
	.type	SERIAL_set_baud_rate_on_A_or_B, %function
SERIAL_set_baud_rate_on_A_or_B:
.LFB17:
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	r3, r0, #1
	cmp	r3, #1
	stmfd	sp!, {r4, lr}
.LCFI9:
	mov	r4, r1
	sub	sp, sp, #52
.LCFI10:
	bhi	.L140
	cmp	r0, #1
	ldreq	r3, .L145
	ldrne	r3, .L145+4
	streq	r3, [sp, #0]
	strne	r3, [sp, #0]
	moveq	r3, #0
	movne	r3, #3
	str	r3, [sp, #16]
	bl	vPortEnterCritical
	ldr	r3, [sp, #0]
	mov	r1, #0
	ldr	r2, [r3, #12]
	orr	r2, r2, #128
	str	r2, [r3, #12]
	mov	r2, #1
	str	r2, [r3, #0]
	str	r1, [r3, #4]
	ldr	r0, [r3, #12]
	str	r1, [sp, #40]
	bic	r0, r0, #128
	str	r0, [r3, #12]
	add	r1, sp, #36
	mov	r3, #8
	mov	r0, sp
	str	r2, [sp, #48]
	str	r3, [sp, #44]
	str	r4, [sp, #36]
	bl	uart_setup_trans_mode
	ldr	r3, [sp, #0]
	ldr	r2, [r3, #12]
	bic	r2, r2, #128
	str	r2, [r3, #12]
	bl	vPortExitCritical
	b	.L139
.L140:
	ldr	r0, .L145+8
	bl	Alert_Message
.L139:
	add	sp, sp, #52
	ldmfd	sp!, {r4, pc}
.L146:
	.align	2
.L145:
	.word	1074266112
	.word	1074364416
	.word	.LC2
.LFE17:
	.size	SERIAL_set_baud_rate_on_A_or_B, .-SERIAL_set_baud_rate_on_A_or_B
	.section	.text.initialize_uart_hardware,"ax",%progbits
	.align	2
	.global	initialize_uart_hardware
	.type	initialize_uart_hardware, %function
initialize_uart_hardware:
.LFB20:
	@ args = 0, pretend = 0, frame = 104
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI11:
	subs	r4, r0, #0
	sub	sp, sp, #108
.LCFI12:
	beq	.L149
	cmp	r4, #3
	bhi	.L148
	b	.L191
.L149:
.LBB16:
	mov	r0, #26
	bl	xDisable_ISR
	ldr	r3, .L192
	mov	r2, #2
	str	r3, [sp, #40]
	mov	r1, #6
	ldr	r3, .L192+4
	mov	r0, #26
	str	r4, [sp, #56]
	str	r4, [sp, #0]
	bl	xSetISR_Vector
	ldr	r3, .L192+8
	add	r0, sp, #40
	add	r1, sp, #72
	str	r3, [sp, #72]
	str	r4, [sp, #84]
	str	r4, [sp, #76]
	bl	hsuart_setup_trans_mode
	ldr	r3, [sp, #40]
	ldr	r2, .L192+12
	str	r2, [r3, #12]
.L151:
	ldr	r2, [r3, #0]
	tst	r2, #256
	beq	.L151
	ldr	r4, .L192+16
	mov	r3, #1
	strb	r3, [r4, #20]
	strb	r3, [r4, #23]
	strb	r3, [r4, #22]
	ldr	r3, .L192+20
	mov	r2, #0
	str	r2, [r4, #12]
	ldr	r0, .L192+24
	str	r3, [sp, #0]
	mov	r1, #400
	mov	r3, r2
	bl	xTimerCreate
	ldr	r3, .L192+28
	cmp	r0, #0
	str	r0, [r4, r3]
	bne	.L152
	ldr	r0, .L192+32
	ldr	r1, .L192+36
	bl	Alert_Message_va
.L152:
	ldr	r3, [sp, #40]
	mov	r2, #32
	str	r2, [r3, #8]
	mov	r2, #16
	str	r2, [r3, #8]
	mov	r2, #8
	str	r2, [r3, #8]
	mov	r0, #26
	b	.L190
.L191:
.LBE16:
.LBB17:
	cmp	r4, #1
	sub	r7, r4, #1
	bne	.L154
	ldr	r3, .L192+40
	mov	r5, #56
	ldr	r2, [r3, #80]
	ldr	r3, .L192+44
	mov	r6, #0
	mla	r5, r2, r5, r3
	ldr	r3, .L192+48
	mov	r1, r4
	mov	r0, #31
	str	r3, [sp, #4]
	str	r6, [sp, #20]
	bl	clkpwr_clk_en_dis
	mov	r0, #7
	bl	xDisable_ISR
	mov	r0, #7
	mov	r1, r0
	mov	r2, #2
	ldr	r3, .L192+52
	str	r6, [sp, #0]
	bl	xSetISR_Vector
	mov	r0, #71
	bl	xDisable_ISR
	ldr	r2, [r5, #8]
	mov	r1, #16
	cmp	r2, r6
	movne	r2, #3
	moveq	r2, #4
	ldr	r3, .L192+56
	mov	r0, #71
	str	r6, [sp, #0]
	bl	xSetISR_Vector
	mov	r0, #91
	bl	xDisable_ISR
	mov	r1, #19
	mov	r2, #2
	ldr	r3, .L192+60
	mov	r0, #91
	str	r6, [sp, #0]
	bl	xSetISR_Vector
	mov	r0, #91
	bl	xEnable_ISR
	b	.L156
.L154:
	cmp	r4, #2
	bne	.L157
	ldr	r3, .L192+40
	mov	r5, #56
	ldr	r2, [r3, #84]
	ldr	r3, .L192+44
	mov	r1, #1
	mla	r5, r2, r5, r3
	ldr	r3, .L192+64
	mov	r0, #28
	str	r3, [sp, #4]
	mov	r3, #3
	str	r3, [sp, #20]
	bl	clkpwr_clk_en_dis
	mov	r0, #10
	bl	xDisable_ISR
	mov	r0, #10
	mov	r6, #0
	mov	r1, r0
	mov	r2, r4
	ldr	r3, .L192+68
	str	r6, [sp, #0]
	bl	xSetISR_Vector
	mov	r0, #86
	bl	xDisable_ISR
	ldr	r2, [r5, #8]
	mov	r0, #86
	cmp	r2, r6
	mov	r1, #17
	movne	r2, #3
	moveq	r2, #4
	ldr	r3, .L192+72
	str	r6, [sp, #0]
	bl	xSetISR_Vector
	mov	r0, #88
	b	.L187
.L157:
	ldr	r3, .L192+76
	mov	r1, #1
	mov	r6, #2
	mov	r0, #29
	str	r3, [sp, #4]
	str	r6, [sp, #20]
	bl	clkpwr_clk_en_dis
	mov	r0, #9
	bl	xDisable_ISR
	mov	r0, #9
	mov	r5, #0
	mov	r1, r0
	mov	r2, r6
	ldr	r3, .L192+80
	str	r5, [sp, #0]
	bl	xSetISR_Vector
	mov	r0, #76
.L187:
	bl	xDisable_ISR
.L156:
	ldr	r3, [sp, #4]
	cmp	r7, #2
	ldr	r2, [r3, #12]
	add	r0, sp, #4
	orr	r2, r2, #128
	str	r2, [r3, #12]
	mov	r2, #1
	str	r2, [r3, #0]
	mov	r2, #0
	str	r2, [r3, #4]
	ldr	r1, [r3, #12]
	str	r2, [sp, #96]
	bic	r1, r1, #128
	str	r1, [r3, #12]
	mov	r3, #57600
	str	r3, [sp, #92]
	ldrne	r3, [r5, #4]
	ldreq	r3, .L192+8
	add	r1, sp, #92
	str	r3, [sp, #92]
	mov	r3, #1
	str	r3, [sp, #104]
	mov	r3, #8
	str	r3, [sp, #100]
	bl	uart_setup_trans_mode
	ldr	r3, [sp, #4]
	ldr	r2, [r3, #12]
	bic	r2, r2, #128
	str	r2, [r3, #12]
	mov	r2, #64
	str	r2, [r3, #8]
	mov	r2, #73
	str	r2, [r3, #8]
	mov	r2, #79
	str	r2, [r3, #8]
	b	.L161
.L162:
	ldr	r2, [r3, #0]
.L161:
	ldr	r2, [r3, #20]
	tst	r2, #1
	bne	.L162
	mov	r2, #7
	cmp	r7, #2
	str	r2, [r3, #4]
	beq	.L163
	mov	r0, r4
	ldr	r1, [r5, #20]
	bl	SetRTS
	mov	r0, r4
	ldr	r1, [r5, #24]
	bl	SetDTR
	cmp	r4, #1
	bne	.L163
	ldr	r3, .L192+40
	ldr	r2, .L192+44
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r2, r1, r3, r2
	ldr	r2, [r2, #8]
	cmp	r2, #0
	ldr	r2, .L192+84
	ldr	r2, [r2, #0]
	beq	.L164
	tst	r2, #65536
	moveq	r1, #0
	movne	r1, #1
	b	.L165
.L164:
	tst	r2, #65536
	movne	r1, #0
	moveq	r1, #1
.L165:
	ldr	r2, .L192+16
	ldr	r0, .L192+88
	strb	r1, [r2, r0]
	ldr	r1, .L192+44
	mov	r0, #56
	mla	r1, r0, r3, r1
	mov	r0, r2
	ldr	r1, [r1, #12]
	ldr	r2, .L192+84
	cmp	r1, #0
	ldr	r2, [r2, #0]
	beq	.L166
	tst	r2, #32
	moveq	r2, #0
	movne	r2, #1
	b	.L167
.L166:
	tst	r2, #32
	movne	r2, #0
	moveq	r2, #1
.L167:
	ldr	r1, .L192+92
	strb	r2, [r0, r1]
	ldr	r2, .L192+44
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #16]
	cmp	r3, #0
	ldr	r3, .L192+84
	ldr	r3, [r3, #0]
	beq	.L168
	tst	r3, #268435456
	moveq	r3, #0
	movne	r3, #1
	b	.L169
.L168:
	tst	r3, #268435456
	movne	r3, #0
	moveq	r3, #1
.L169:
	ldr	r2, .L192+96
	ldr	r1, .L192+16
	strb	r3, [r1, r2]
	b	.L170
.L163:
	cmp	r4, #2
	bne	.L171
	ldr	r3, .L192+40
	ldr	r2, .L192+44
	ldr	r3, [r3, #84]
	mov	r1, #56
	mla	r2, r1, r3, r2
	ldr	r2, [r2, #8]
	cmp	r2, #0
	ldr	r2, .L192+84
	ldrne	r1, [r2, #0]
	andne	r1, r1, #1
	bne	.L173
	ldr	r2, [r2, #0]
	tst	r2, #1
	movne	r1, #0
	moveq	r1, #1
.L173:
	ldr	r2, .L192+16
	ldr	r0, .L192+100
	strb	r1, [r2, r0]
	ldr	r1, .L192+44
	mov	r0, #56
	mla	r1, r0, r3, r1
	mov	r0, r2
	ldr	r1, [r1, #12]
	ldr	r2, .L192+84
	cmp	r1, #0
	ldr	r2, [r2, #0]
	beq	.L174
	tst	r2, #4
	moveq	r2, #0
	movne	r2, #1
	b	.L175
.L174:
	tst	r2, #4
	movne	r2, #0
	moveq	r2, #1
.L175:
	ldr	r1, .L192+104
	strb	r2, [r0, r1]
	ldr	r2, .L192+44
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #16]
	cmp	r3, #0
	ldr	r3, .L192+84
	ldr	r3, [r3, #0]
	beq	.L176
	tst	r3, #8
	moveq	r3, #0
	movne	r3, #1
	b	.L177
.L176:
	tst	r3, #8
	movne	r3, #0
	moveq	r3, #1
.L177:
	ldr	r2, .L192+108
	ldr	r1, .L192+16
	strb	r3, [r1, r2]
	b	.L178
.L171:
	ldr	r3, .L192+16
	ldr	r1, .L192+112
	mov	r2, #1
	strb	r2, [r3, r1]
	add	r1, r1, #3
	strb	r2, [r3, r1]
	sub	r1, r1, #1
	strb	r2, [r3, r1]
	ldr	r3, .L192+84
	mov	r2, #2048
	str	r2, [r3, #4]
	mov	r2, #512
	str	r2, [r3, #4]
.L178:
	cmp	r7, #2
	beq	.L179
.L170:
	ldr	r3, .L192+116
	ldr	r1, .L192+120
	str	r3, [sp, #0]
	ldr	r3, .L192+124
	mov	r2, #0
	ldr	r0, [r3, r4, asl #2]
	mov	r3, r4
	bl	xTimerCreate
	ldr	r5, .L192+128
	ldr	r3, .L192+16
	mov	r1, #10
	mla	r5, r4, r5, r3
	ldr	r3, .L192+132
	mov	r2, #0
	str	r0, [r5, #4]
	str	r3, [sp, #0]
	ldr	r3, .L192+136
	ldr	r0, [r3, r4, asl #2]
	mov	r3, r4
	bl	xTimerCreate
	ldr	r3, [r5, #4]
	cmp	r3, #0
	str	r0, [r5, #8]
	beq	.L180
	cmp	r0, #0
	bne	.L179
.L180:
	ldr	r3, .L192+140
	ldr	r0, .L192+144
	ldr	r1, [r3, r4, asl #2]
	bl	Alert_Message_va
.L179:
	ldr	r2, .L192+128
	ldr	r3, .L192+16
	cmp	r4, #1
	mla	r3, r2, r4, r3
	mov	r2, #0
	str	r2, [r3, #12]
	ldr	r3, [sp, #4]
	moveq	r0, #71
	ldr	r2, [r3, #8]
	ldr	r2, [r3, #0]
	ldr	r2, [r3, #20]
	ldr	r3, [r3, #24]
	beq	.L189
	cmp	r4, #2
	bne	.L182
	mov	r0, #86
.L189:
	bl	xEnable_ISR
.L182:
	ldr	r3, .L192+148
	mov	r2, #12
	mla	r4, r2, r4, r3
	ldr	r0, [r4, #4]
.L190:
	bl	xEnable_ISR
	b	.L147
.L148:
.LBE17:
	ldr	r0, .L192+152
	bl	Alert_Message
.L147:
	add	sp, sp, #108
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L193:
	.align	2
.L192:
	.word	1073823744
	.word	uart1_isr
	.word	115200
	.word	141548
	.word	SerDrvrVars_s
	.word	flow_control_timer_callback
	.word	.LC3
	.word	4276
	.word	.LC4
	.word	.LC5
	.word	config_c
	.word	port_device_table
	.word	1074266112
	.word	uart3_isr
	.word	port_a_cts_INT_HANDLER
	.word	port_a_carrier_detect_INT_HANDLER
	.word	1074364416
	.word	uart6_isr
	.word	port_b_cts_INT_HANDLER
	.word	1074331648
	.word	uart5_isr
	.word	1073905664
	.word	4300
	.word	4303
	.word	4302
	.word	8580
	.word	8583
	.word	8582
	.word	12860
	.word	cts_main_timer_callback
	.word	24000
	.word	.LANCHOR2
	.word	4280
	.word	cts_poll_timer_callback
	.word	.LANCHOR3
	.word	.LANCHOR0
	.word	.LC6
	.word	.LANCHOR4
	.word	.LC7
.LFE20:
	.size	initialize_uart_hardware, .-initialize_uart_hardware
	.section	.text.kick_off_a_block_by_feeding_the_uart,"ax",%progbits
	.align	2
	.global	kick_off_a_block_by_feeding_the_uart
	.type	kick_off_a_block_by_feeding_the_uart, %function
kick_off_a_block_by_feeding_the_uart:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	r3, r0, #1
	cmp	r3, #2
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI13:
	mov	r5, r0
	bhi	.L195
	mov	r2, #12
	mul	r2, r0, r2
	ldr	r3, .L207
	mov	r1, #24
	ldr	r7, [r3, r2]
	ldr	r2, .L207+4
	mla	r2, r1, r0, r2
	ldr	r6, [r2, #20]
	cmp	r6, #0
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
.L205:
	ldr	r2, [r7, #20]
	and	r2, r2, #96
	cmp	r2, #96
	bne	.L205
.L197:
	mov	r2, #12
	mla	r3, r2, r5, r3
	ldr	r4, [r3, #4]
	mov	r0, r4
	bl	xDisable_ISR
	ldrh	r2, [r6, #20]
	cmp	r2, #0
	bne	.L199
	ldr	r3, .L207+8
	ldr	r0, .L207+12
	ldr	r1, [r3, r5, asl #2]
	bl	Alert_Message_va
	b	.L200
.L199:
	ldr	r3, [r6, #12]
	sub	r2, r2, #1
	ldrb	r1, [r3], #1	@ zero_extendqisi2
	str	r1, [r7, #0]
	strh	r2, [r6, #20]	@ movhi
	ldr	r1, .L207+16
	ldr	r2, .L207+20
	str	r3, [r6, #12]
	mla	r5, r1, r5, r2
	ldr	r3, .L207+24
	ldr	r2, [r5, r3]
	add	r2, r2, #1
	str	r2, [r5, r3]
.L200:
	mov	r0, r4
	b	.L206
.L195:
	cmp	r0, #0
	ldmnefd	sp!, {r4, r5, r6, r7, pc}
	ldr	r3, .L207+4
	ldr	r4, [r3, #20]
	cmp	r4, #0
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	mov	r0, #26
	bl	xDisable_ISR
	ldrh	r2, [r4, #20]
	cmp	r2, #0
	bne	.L201
	ldr	r0, .L207+12
	ldr	r1, .L207+28
	bl	Alert_Message_va
	b	.L202
.L201:
	ldr	r3, [r4, #12]
	ldr	r1, .L207+32
	ldrb	r0, [r3], #1	@ zero_extendqisi2
	sub	r2, r2, #1
	str	r0, [r1, #0]
	str	r3, [r4, #12]
	strh	r2, [r4, #20]	@ movhi
	ldr	r3, .L207+24
	ldr	r2, .L207+20
	ldr	r1, [r2, r3]
	add	r1, r1, #1
	str	r1, [r2, r3]
.L202:
	mov	r0, #26
.L206:
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xEnable_ISR
.L208:
	.align	2
.L207:
	.word	.LANCHOR4
	.word	.LANCHOR1
	.word	.LANCHOR0
	.word	.LC8
	.word	4280
	.word	SerDrvrVars_s
	.word	4264
	.word	.LC5
	.word	1073823744
.LFE21:
	.size	kick_off_a_block_by_feeding_the_uart, .-kick_off_a_block_by_feeding_the_uart
	.section	.text.AddCopyOfBlockToXmitList,"ax",%progbits
	.align	2
	.global	AddCopyOfBlockToXmitList
	.type	AddCopyOfBlockToXmitList, %function
AddCopyOfBlockToXmitList:
.LFB22:
	@ args = 8, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI14:
	cmp	r0, #3
	sub	sp, sp, #24
.LCFI15:
	mov	r4, r0
	mov	r7, r1
	mov	r5, r2
	mov	r8, r3
	ldr	r9, [sp, #56]
	ldr	r6, [sp, #60]
	bls	.L210
	ldr	r0, .L219
	mov	r1, r4
	bl	Alert_Message_va
	mov	sl, #0
	b	.L211
.L210:
	cmp	r2, #0
	bne	.L212
	ldr	r3, .L219+4
	ldr	r0, .L219+8
	ldr	r1, [r3, r4, asl #2]
	bl	Alert_Message_va
	mov	sl, r5
	b	.L211
.L212:
	mov	r0, #36
	add	r1, sp, #20
	ldr	r2, .L219+12
	ldr	r3, .L219+16
	bl	mem_oabia
	subs	sl, r0, #0
	ldreq	r0, .L219+20
	beq	.L218
	ldr	r1, [sp, #20]
	mov	r0, r5
	add	r1, r1, #12
	ldr	r2, .L219+12
	ldr	r3, .L219+24
	bl	mem_oabia
	subs	sl, r0, #0
	beq	.L214
	ldr	r3, [sp, #20]
	mov	r2, r5
	ldr	r0, [r3, #12]
	strh	r5, [r3, #20]	@ movhi
	str	r0, [r3, #16]
	mov	r1, r7
	bl	memcpy
	ldr	r3, [sp, #20]
	mov	r2, #0
	cmp	r4, r2
	str	r8, [r3, #24]
	str	r2, [r3, #28]
	bne	.L215
	cmp	r9, r2
	bne	.L215
.LBB18:
	mov	r2, #12
	add	r1, r7, #4
	mov	r0, sp
	bl	memcpy
	mov	r0, sp
	add	r1, sp, #12
	bl	get_this_packets_message_class
	ldr	r2, [sp, #12]
	ldr	r3, .L219+28
	cmp	r2, r3
	bne	.L215
	ldr	r3, [sp, #16]
	cmp	r3, #7
	ldrne	r3, [sp, #20]
	movne	r2, #1
	strne	r2, [r3, #28]
.L215:
.LBE18:
	ldr	r3, [sp, #20]
	ldr	r5, .L219+32
	mov	r1, #0
	str	r9, [r3, #32]
	mvn	r2, #0
	mov	r3, r1
	ldr	r0, [r5, r4, asl #2]
	bl	xQueueGenericReceive
	ldr	r3, .L219+36
	mov	r0, #24
	mla	r0, r4, r0, r3
	ldr	r1, [sp, #20]
	bl	nm_ListInsertTail
	cmp	r0, #0
	beq	.L216
	ldr	r0, .L219+12
	bl	RemovePathFromFileName
	ldr	r2, .L219+40
	mov	r1, r0
	ldr	r0, .L219+44
	bl	Alert_Message_va
.L216:
	mov	r1, #0
	ldr	r0, [r5, r4, asl #2]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
	cmp	r6, #1
	movne	sl, #1
	bne	.L211
	mov	r0, r4
	mov	r1, r6
	bl	postSerportDrvrEvent
	mov	sl, r6
	b	.L211
.L214:
	ldr	r0, [sp, #20]
	ldr	r1, .L219+12
	ldr	r2, .L219+48
	bl	mem_free_debug
	ldr	r0, .L219+52
.L218:
	bl	Alert_Message
.L211:
	mov	r0, sl
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L220:
	.align	2
.L219:
	.word	.LC9
	.word	.LANCHOR0
	.word	.LC10
	.word	.LC11
	.word	1693
	.word	.LC14
	.word	1703
	.word	3000
	.word	xmit_list_MUTEX
	.word	.LANCHOR1
	.word	1771
	.word	.LC12
	.word	1800
	.word	.LC13
.LFE22:
	.size	AddCopyOfBlockToXmitList, .-AddCopyOfBlockToXmitList
	.section	.text.strout,"ax",%progbits
	.align	2
	.global	strout
	.type	strout, %function
strout:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI16:
	mov	r4, r0
	mov	r0, r1
	mov	r5, r1
	bl	strlen
	mov	r1, #2
	mov	r3, #1
	stmia	sp, {r1, r3}
	mov	r1, r5
	mov	r3, #0
	mov	r2, r0
	mov	r0, r4
	bl	AddCopyOfBlockToXmitList
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.LFE23:
	.size	strout, .-strout
	.section	.text.term_dat_out,"ax",%progbits
	.align	2
	.global	term_dat_out
	.type	term_dat_out, %function
term_dat_out:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI17:
	mov	r4, r0
	bl	strlen
	mov	r3, #1
	mov	r1, r4
	mov	r2, r0
	mov	r0, #2
	stmia	sp, {r0, r3}
	mov	r3, #0
	mov	r0, #3
	bl	AddCopyOfBlockToXmitList
	ldmfd	sp!, {r2, r3, r4, pc}
.LFE24:
	.size	term_dat_out, .-term_dat_out
	.section	.text.term_dat_out_crlf,"ax",%progbits
	.align	2
	.global	term_dat_out_crlf
	.type	term_dat_out_crlf, %function
term_dat_out_crlf:
.LFB25:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI18:
	ldr	r2, .L224
	sub	sp, sp, #64
.LCFI19:
	mov	r3, r0
	mov	r1, #64
	mov	r0, sp
	bl	snprintf
	mov	r0, sp
	bl	term_dat_out
	add	sp, sp, #64
	ldmfd	sp!, {pc}
.L225:
	.align	2
.L224:
	.word	.LC15
.LFE25:
	.size	term_dat_out_crlf, .-term_dat_out_crlf
	.section	.text.SERIAL_add_daily_stats_to_monthly_battery_backed,"ax",%progbits
	.align	2
	.global	SERIAL_add_daily_stats_to_monthly_battery_backed
	.type	SERIAL_add_daily_stats_to_monthly_battery_backed, %function
SERIAL_add_daily_stats_to_monthly_battery_backed:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, .L227
	ldr	r2, .L227+4
	ldr	r3, .L227+8
	mla	r2, r1, r0, r2
	sub	r1, r1, #16
	ldr	r0, [r2, r1]
	ldr	r1, [r3, #120]
	add	r1, r0, r1
	str	r1, [r3, #120]
	ldr	r1, .L227+12
	ldr	r0, [r2, r1]
	ldr	r1, [r3, #116]
	add	r1, r0, r1
	str	r1, [r3, #116]
	ldr	r1, .L227+16
	ldr	r0, [r2, r1]
	ldr	r1, [r3, #124]
	add	r1, r0, r1
	str	r1, [r3, #124]
	ldr	r1, .L227+20
	ldr	r1, [r2, r1]
	ldr	r2, [r3, #132]
	add	r2, r1, r2
	str	r2, [r3, #132]
	bx	lr
.L228:
	.align	2
.L227:
	.word	4280
	.word	SerDrvrVars_s
	.word	weather_preserves
	.word	4260
	.word	4268
	.word	4272
.LFE26:
	.size	SERIAL_add_daily_stats_to_monthly_battery_backed, .-SERIAL_add_daily_stats_to_monthly_battery_backed
	.section	.text.SERIAL_at_midnight_manage_xmit_and_rcvd_accumulators_and_make_alert_lines,"ax",%progbits
	.align	2
	.global	SERIAL_at_midnight_manage_xmit_and_rcvd_accumulators_and_make_alert_lines
	.type	SERIAL_at_midnight_manage_xmit_and_rcvd_accumulators_and_make_alert_lines, %function
SERIAL_at_midnight_manage_xmit_and_rcvd_accumulators_and_make_alert_lines:
.LFB27:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L235
	stmfd	sp!, {r0, r4, lr}
.LCFI20:
	ldr	r3, [r3, #80]
	mov	r4, r0
	cmp	r3, #10
	cmpne	r3, #7
	bne	.L230
	ldr	r3, .L235+4
	ldr	r2, .L235+8
	ldr	ip, .L235+12
	ldr	r1, [r3, r2]
	sub	r2, r2, #4
	ldr	r2, [r3, r2]
	ldr	r0, .L235+16
	ldr	r3, [r3, ip]
	bl	Alert_Message_va
.L230:
	mov	r0, #1
	bl	SERIAL_add_daily_stats_to_monthly_battery_backed
	ldr	r3, .L235+4
	ldr	r1, .L235+8
	mov	r2, #0
	str	r2, [r3, r1]
	sub	r1, r1, #4
	str	r2, [r3, r1]
	add	r1, r1, #8
	str	r2, [r3, r1]
	add	r1, r1, #4
	str	r2, [r3, r1]
	ldr	r3, .L235
	ldr	r3, [r3, #80]
	cmp	r3, #10
	cmpne	r3, #7
	bne	.L231
	ldrh	r3, [r4, #6]
	cmp	r3, #1
	ldr	r3, .L235+20
	ldreq	r0, .L235+24
	ldr	r2, [r3, #132]
	ldrne	r0, .L235+28
	str	r2, [sp, #0]
	ldr	r1, [r3, #120]
	ldr	r2, [r3, #116]
	ldr	r3, [r3, #124]
	bl	Alert_Message_va
.L231:
	ldrh	r3, [r4, #6]
	cmp	r3, #1
	bne	.L229
	ldr	r3, .L235+20
	mov	r2, #0
	str	r2, [r3, #120]
	str	r2, [r3, #116]
	str	r2, [r3, #124]
	str	r2, [r3, #132]
.L229:
	ldmfd	sp!, {r3, r4, pc}
.L236:
	.align	2
.L235:
	.word	config_c
	.word	SerDrvrVars_s
	.word	8544
	.word	8552
	.word	.LC16
	.word	weather_preserves
	.word	.LC17
	.word	.LC18
.LFE27:
	.size	SERIAL_at_midnight_manage_xmit_and_rcvd_accumulators_and_make_alert_lines, .-SERIAL_at_midnight_manage_xmit_and_rcvd_accumulators_and_make_alert_lines
	.global	CTS_poll_timer_names
	.global	CTS_main_timer_names
	.global	xmit_cntrl
	.global	port_names
	.global	uinfo
	.section	.rodata.CTS_main_timer_names,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	CTS_main_timer_names, %object
	.size	CTS_main_timer_names, 20
CTS_main_timer_names:
	.word	.LC24
	.word	.LC25
	.word	.LC26
	.word	.LC27
	.word	.LC28
	.section	.rodata.CTS_poll_timer_names,"a",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	CTS_poll_timer_names, %object
	.size	CTS_poll_timer_names, 20
CTS_poll_timer_names:
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	.LC23
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Only for TP port (not for %s)\000"
.LC1:
	.ascii	"%s CTS: Polling Timer Queue Full!\000"
.LC2:
	.ascii	"BAUD PROGRAMMING: port out of range.\000"
.LC3:
	.ascii	"TP_flow\000"
.LC4:
	.ascii	"%s Flow Control Timer NOT CREATED!\000"
.LC5:
	.ascii	"Port TP\000"
.LC6:
	.ascii	"%s CTS Timer NOT CREATED!\000"
.LC7:
	.ascii	"Serial Driver Task for an UNK port!\000"
.LC8:
	.ascii	"%s UART: zero length block!\000"
.LC9:
	.ascii	"Port out of range: %d\000"
.LC10:
	.ascii	"%s: trying to add a 0 byte block\000"
.LC11:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/serial.c\000"
.LC12:
	.ascii	"List Insert Failed : %s, %u\000"
.LC13:
	.ascii	"SERIAL: no memory to add data\000"
.LC14:
	.ascii	"SERIAL: no memory to add list item\000"
.LC15:
	.ascii	"%s\015\012\000"
.LC16:
	.ascii	"Daily (bytes): xmit %u, rcvd %u, mobile %u\000"
.LC17:
	.ascii	"PRIOR MONTH USE (bytes): xmit %u, rcvd %u, code %u,"
	.ascii	" mobile %u\000"
.LC18:
	.ascii	"Month to date (bytes): xmit %u, rcvd %u, code %u, m"
	.ascii	"obile %u\000"
.LC19:
	.ascii	"TP Poll\000"
.LC20:
	.ascii	"A Poll\000"
.LC21:
	.ascii	"B Poll\000"
.LC22:
	.ascii	"RRE Poll\000"
.LC23:
	.ascii	"USB Poll\000"
.LC24:
	.ascii	"TP Main\000"
.LC25:
	.ascii	"A Main\000"
.LC26:
	.ascii	"B Main\000"
.LC27:
	.ascii	"RRE Main\000"
.LC28:
	.ascii	"USB Main\000"
.LC29:
	.ascii	"Port A\000"
.LC30:
	.ascii	"Port B\000"
.LC31:
	.ascii	"Port RRE\000"
.LC32:
	.ascii	"Port USB\000"
.LC33:
	.ascii	"Port M1\000"
	.section	.rodata.port_names,"a",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	port_names, %object
	.size	port_names, 24
port_names:
	.word	.LC5
	.word	.LC29
	.word	.LC30
	.word	.LC31
	.word	.LC32
	.word	.LC33
	.section	.rodata.uinfo,"a",%progbits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	uinfo, %object
	.size	uinfo, 60
uinfo:
	.word	1073823744
	.word	26
	.word	0
	.word	1074266112
	.word	7
	.word	71
	.word	1074364416
	.word	10
	.word	86
	.word	1074331648
	.word	9
	.word	0
	.word	0
	.word	0
	.word	0
	.section	.bss.xmit_cntrl,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	xmit_cntrl, %object
	.size	xmit_cntrl, 120
xmit_cntrl:
	.space	120
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI0-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI1-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI2-.LFB16
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI3-.LFB0
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI5-.LFB10
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI6-.LFB6
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI7-.LFB2
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI9-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI11-.LFB20
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x80
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI13-.LFB21
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI14-.LFB22
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI16-.LFB23
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI17-.LFB24
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI18-.LFB25
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI20-.LFB27
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE48:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/serial.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x22f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF28
	.byte	0x1
	.4byte	.LASF29
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x117
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x263
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x213
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x315
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.byte	0x76
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST3
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1ba
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1b4
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1ae
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x223
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST4
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x1c0
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST5
	.uleb128 0x5
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x201
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x1f9
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1a8
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x275
	.4byte	.LFB12
	.4byte	.LFE12
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x28d
	.4byte	.LFB13
	.4byte	.LFE13
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x2a5
	.4byte	.LFB14
	.4byte	.LFE14
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x2dd
	.4byte	.LFB15
	.4byte	.LFE15
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x368
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST7
	.uleb128 0x2
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x57d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x3bf
	.byte	0x1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x5cc
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST8
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x5e5
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST9
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x681
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST10
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x718
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST11
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x726
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST12
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x732
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST13
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x73b
	.4byte	.LFB26
	.4byte	.LFE26
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x74a
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST14
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB11
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB9
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB16
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB0
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI4
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB10
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB6
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB2
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI8
	.4byte	.LFE2
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB17
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB20
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI12
	.4byte	.LFE20
	.2byte	0x3
	.byte	0x7d
	.sleb128 128
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB21
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB22
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI15
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB23
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB24
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB25
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE25
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB27
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xdc
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"cts_main_timer_callback\000"
.LASF28:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF13:
	.ascii	"set_reset_INACTIVE_to_serial_port_device\000"
.LASF14:
	.ascii	"SetRTS\000"
.LASF26:
	.ascii	"SERIAL_add_daily_stats_to_monthly_battery_backed\000"
.LASF6:
	.ascii	"uart3_isr\000"
.LASF2:
	.ascii	"port_a_carrier_detect_INT_HANDLER\000"
.LASF27:
	.ascii	"SERIAL_at_midnight_manage_xmit_and_rcvd_accumulator"
	.ascii	"s_and_make_alert_lines\000"
.LASF29:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/serial.c\000"
.LASF16:
	.ascii	"SERIAL_set_baud_rate_on_A_or_B\000"
.LASF22:
	.ascii	"AddCopyOfBlockToXmitList\000"
.LASF10:
	.ascii	"port_a_cts_INT_HANDLER\000"
.LASF15:
	.ascii	"SetDTR\000"
.LASF23:
	.ascii	"strout\000"
.LASF25:
	.ascii	"term_dat_out_crlf\000"
.LASF20:
	.ascii	"initialize_uart_hardware\000"
.LASF7:
	.ascii	"cts_poll_timer_callback\000"
.LASF12:
	.ascii	"set_reset_ACTIVE_to_serial_port_device\000"
.LASF21:
	.ascii	"kick_off_a_block_by_feeding_the_uart\000"
.LASF4:
	.ascii	"uart5_isr\000"
.LASF11:
	.ascii	"uart1_isr\000"
.LASF0:
	.ascii	"flow_control_timer_callback\000"
.LASF19:
	.ascii	"standard_uart_init\000"
.LASF24:
	.ascii	"term_dat_out\000"
.LASF9:
	.ascii	"port_b_cts_INT_HANDLER\000"
.LASF8:
	.ascii	"common_cts_INT_HANDLER\000"
.LASF3:
	.ascii	"standard_uart_INT_HANDLER\000"
.LASF18:
	.ascii	"highspeed_uart_init\000"
.LASF17:
	.ascii	"highspeed_uart_INT_HANDLER\000"
.LASF5:
	.ascii	"uart6_isr\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
