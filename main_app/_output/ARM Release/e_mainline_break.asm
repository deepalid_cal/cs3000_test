	.file	"e_mainline_break.c"
	.text
.Ltext0:
	.section	.text.FDTO_MAINLINE_BREAK_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_MAINLINE_BREAK_draw_screen
	.type	FDTO_MAINLINE_BREAK_draw_screen, %function
FDTO_MAINLINE_BREAK_draw_screen:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L4
	str	lr, [sp, #-4]!
.LCFI0:
	ldrnesh	r1, [r3, #0]
	bne	.L3
	bl	MAINLINE_BREAK_copy_group_into_guivars
	mov	r1, #0
.L3:
	mov	r0, #37
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L5:
	.align	2
.L4:
	.word	GuiLib_ActiveCursorFieldNo
.LFE0:
	.size	FDTO_MAINLINE_BREAK_draw_screen, .-FDTO_MAINLINE_BREAK_draw_screen
	.global	__modsi3
	.section	.text.MAINLINE_BREAK_process_screen,"ax",%progbits
	.align	2
	.global	MAINLINE_BREAK_process_screen
	.type	MAINLINE_BREAK_process_screen, %function
MAINLINE_BREAK_process_screen:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI1:
	mov	r4, r0
	mov	r5, r1
	beq	.L11
	bhi	.L14
	cmp	r0, #1
	beq	.L9
	bcc	.L8
	cmp	r0, #3
	beq	.L10
	cmp	r0, #4
	bne	.L7
	b	.L11
.L14:
	cmp	r0, #67
	beq	.L12
	bhi	.L15
	cmp	r0, #20
	bne	.L7
	b	.L8
.L15:
	cmp	r0, #80
	beq	.L13
	cmp	r0, #84
	bne	.L7
.L13:
	mov	r0, r5
	bl	SYSTEM_get_inc_by_for_MLB_and_capacity
	ldr	r3, .L46
	ldrsh	r3, [r3, #0]
	cmp	r3, #23
	ldrls	pc, [pc, r3, asl #2]
	b	.L16
.L29:
	.word	.L17
	.word	.L18
	.word	.L19
	.word	.L20
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L21
	.word	.L22
	.word	.L23
	.word	.L24
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L16
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
.L17:
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L46+4
	b	.L39
.L18:
	ldr	r1, .L46+8
	str	r0, [sp, #0]
	mov	r2, #1
	mov	r0, r4
	str	r2, [sp, #4]
.L39:
	mov	r3, #4000
	bl	process_uns32
	b	.L30
.L19:
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L46+12
	b	.L39
.L20:
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L46+16
	b	.L39
.L21:
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L46+20
	b	.L39
.L22:
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L46+24
	b	.L39
.L23:
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L46+28
	b	.L39
.L24:
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L46+32
	b	.L39
.L25:
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L46+36
	b	.L39
.L26:
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L46+40
	b	.L39
.L27:
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L46+44
	b	.L39
.L28:
	mov	r2, #1
	str	r0, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L46+48
	b	.L39
.L16:
	bl	bad_key_beep
.L30:
	mov	r0, #0
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	Redraw_Screen
.L11:
	ldr	r3, .L46
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	movs	r0, r0, asl #16
	beq	.L38
.L31:
	mov	r0, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Up
.L8:
	ldr	r3, .L46
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	bl	SYSTEM_num_systems_in_use
	sub	r0, r0, #1
	cmp	r4, r0
	beq	.L38
.L32:
	mov	r0, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Down
.L9:
	ldr	r3, .L46
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #3
	movls	r0, r0, asr #16
	addls	r0, r0, #19
	bls	.L42
	sub	r2, r3, #10
	mov	r2, r2, asl #16
	cmp	r2, #196608
	bls	.L44
.L34:
	sub	r3, r3, #20
	mov	r3, r3, asl #16
	cmp	r3, #196608
	bhi	.L38
.L44:
	mov	r0, r0, asr #16
	sub	r0, r0, #10
.L42:
	mov	r1, r4
	b	.L41
.L10:
	ldr	r3, .L46
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #3
	bls	.L45
.L36:
	sub	r2, r3, #10
	mov	r2, r2, asl #16
	cmp	r2, #196608
	bhi	.L37
.L45:
	mov	r0, r0, asr #16
	add	r0, r0, #10
	b	.L43
.L37:
	sub	r3, r3, #20
	mov	r3, r3, asl #16
	cmp	r3, #196608
	bhi	.L38
	mov	r0, r0, asr #16
	sub	r0, r0, #19
.L43:
	mov	r1, #1
.L41:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Select
.L38:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	bad_key_beep
.L12:
	ldr	r3, .L46+52
	mov	r2, #3
	str	r2, [r3, #0]
	bl	MAINLINE_BREAK_extract_and_store_changes_from_GuiVars
.L7:
	mov	r0, r4
	mov	r1, r5
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	KEY_process_global_keys
.L47:
	.align	2
.L46:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_GroupSettingC_3
	.word	GuiVar_MenuScreenToShow
.LFE1:
	.size	MAINLINE_BREAK_process_screen, .-MAINLINE_BREAK_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_mainline_break.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x46
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x2d
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x41
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"MAINLINE_BREAK_process_screen\000"
.LASF0:
	.ascii	"FDTO_MAINLINE_BREAK_draw_screen\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_mainline_break.c\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
