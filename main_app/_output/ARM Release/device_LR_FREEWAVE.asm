	.file	"device_LR_FREEWAVE.c"
	.text
.Ltext0:
	.section	.text.LR_final_radio_verification,"ax",%progbits
	.align	2
	.type	LR_final_radio_verification, %function
LR_final_radio_verification:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	ldr	r1, .L2
	mov	r2, #49
	ldr	r0, .L2+4
	bl	strlcpy
	ldr	r0, .L2+8
	ldr	lr, [sp], #4
	b	COMM_MNGR_device_exchange_results_to_key_process_task
.L3:
	.align	2
.L2:
	.word	.LC0
	.word	GuiVar_CommOptionInfoText
	.word	36867
.LFE9:
	.size	LR_final_radio_verification, .-LR_final_radio_verification
	.section	.text.LR_final_radio_analysis,"ax",%progbits
	.align	2
	.type	LR_final_radio_analysis, %function
LR_final_radio_analysis:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI1:
	ldr	r1, .L8
	mov	r4, r0
	mov	r2, #49
	ldr	r0, .L8+4
	bl	strlcpy
	ldr	r6, .L8+8
	mov	r5, #0
	mov	sl, #12
	add	r9, r4, #52
.L6:
	mul	r7, sl, r5
	ldr	r0, [r4, #20]
	add	r8, r7, #4
	add	r8, r6, r8
	add	r0, r0, #44
	add	r1, r8, #3
	mov	r2, #5
	bl	strncmp
	cmp	r0, #0
	bne	.L5
	ldr	r0, [r4, #20]
	add	r7, r6, r7
	add	r0, r0, #30
	add	r1, r7, #3
	mov	r2, #2
	bl	strncmp
	cmp	r0, #0
	bne	.L5
	ldr	r0, [r4, #20]
	add	r1, r8, #1
	add	r0, r0, #32
	mov	r2, #2
	bl	strncmp
	cmp	r0, #0
	bne	.L5
	mov	r0, r9
	mov	r1, r7
	mov	r2, #3
	bl	strlcpy
	str	r5, [r4, #48]
.L5:
	add	r5, r5, #1
	cmp	r5, #6
	bne	.L6
	ldr	r0, .L8+12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	COMM_MNGR_device_exchange_results_to_key_process_task
.L9:
	.align	2
.L8:
	.word	.LC0
	.word	GuiVar_CommOptionInfoText
	.word	.LANCHOR0
	.word	36867
.LFE8:
	.size	LR_final_radio_analysis, .-LR_final_radio_analysis
	.section	.text.LR_analyze_multipoint_parameters,"ax",%progbits
	.align	2
	.type	LR_analyze_multipoint_parameters, %function
LR_analyze_multipoint_parameters:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #20]
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI2:
	ldr	r1, .L16
	add	r3, r3, #41
	str	r3, [sp, #0]
	ldr	r3, .L16+4
	mov	r4, r0
	str	r3, [sp, #4]
	mov	r2, #21
	ldr	r0, [r0, #12]
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #20]
	ldr	r0, [r4, #12]
	add	r3, r3, #44
	str	r3, [sp, #0]
	ldr	r3, .L16+8
	ldr	r1, .L16+12
	str	r3, [sp, #4]
	mov	r2, #19
	mov	r3, #5
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #20]
	ldr	r0, [r4, #12]
	add	r3, r3, #49
	str	r3, [sp, #0]
	ldr	r3, .L16+16
	ldr	r1, .L16+20
	str	r3, [sp, #4]
	mov	r2, #19
	mov	r3, #9
	bl	dev_extract_text_from_buffer
	ldr	r0, [r4, #20]
	ldr	r1, .L16+24
	add	r0, r0, #49
	mov	r2, #7
	bl	strncmp
	cmp	r0, #0
	ldr	r0, [r4, #20]
	addeq	r0, r0, #58
	beq	.L13
	add	r0, r0, #49
	ldr	r1, .L16+28
	mov	r2, #8
	bl	strncmp
	cmp	r0, #0
	ldr	r0, [r4, #20]
	bne	.L12
	ldr	r1, .L16+32
	add	r0, r0, #58
	mov	r2, #3
	bl	strlcpy
	ldr	r0, [r4, #20]
	ldr	r1, .L16+36
	add	r0, r0, #61
	b	.L15
.L12:
	add	r0, r0, #49
	ldr	r1, .L16+40
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	ldr	r0, [r4, #20]
	add	r0, r0, #58
	bne	.L13
	ldr	r3, .L16+44
	str	r0, [sp, #0]
	str	r3, [sp, #4]
	ldr	r0, [r4, #12]
	ldr	r1, .L16+40
	mov	r2, #4
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #20]
	ldr	r0, [r4, #12]
	add	r3, r3, #61
	str	r3, [sp, #0]
	ldr	r3, .L16+48
	ldr	r1, .L16+52
	str	r3, [sp, #4]
	mov	r2, #5
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	add	sp, sp, #8
	ldmfd	sp!, {r4, pc}
.L13:
	ldr	r1, .L16+32
	mov	r2, #3
	bl	strlcpy
	ldr	r0, [r4, #20]
	ldr	r1, .L16+32
	add	r0, r0, #61
.L15:
	mov	r2, #3
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	strlcpy
.L17:
	.align	2
.L16:
	.word	.LC1
	.word	.LC2
	.word	.LC4
	.word	.LC3
	.word	.LC6
	.word	.LC5
	.word	.LC7
	.word	.LC9
	.word	.LC8
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	.LC14
	.word	.LC13
.LFE6:
	.size	LR_analyze_multipoint_parameters, .-LR_analyze_multipoint_parameters
	.section	.text.LR_analyze_radio_parameters,"ax",%progbits
	.align	2
	.type	LR_analyze_radio_parameters, %function
LR_analyze_radio_parameters:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #20]
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI3:
	ldr	r1, .L19
	add	r3, r3, #30
	str	r3, [sp, #0]
	ldr	r3, .L19+4
	mov	r4, r0
	str	r3, [sp, #4]
	mov	r2, #17
	ldr	r0, [r0, #12]
	mov	r3, #2
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #20]
	ldr	r0, [r4, #12]
	add	r3, r3, #32
	str	r3, [sp, #0]
	ldr	r3, .L19+8
	ldr	r1, .L19+12
	str	r3, [sp, #4]
	mov	r2, #17
	mov	r3, #2
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #20]
	ldr	r0, [r4, #12]
	add	r3, r3, #34
	str	r3, [sp, #0]
	ldr	r3, .L19+16
	ldr	r1, .L19+20
	str	r3, [sp, #4]
	mov	r2, #17
	mov	r3, #2
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #20]
	ldr	r0, [r4, #12]
	add	r3, r3, #36
	str	r3, [sp, #0]
	ldr	r3, .L19+24
	ldr	r1, .L19+28
	str	r3, [sp, #4]
	mov	r2, #16
	mov	r3, #5
	bl	dev_extract_text_from_buffer
	ldmfd	sp!, {r2, r3, r4, pc}
.L20:
	.align	2
.L19:
	.word	.LC15
	.word	.LC16
	.word	.LC18
	.word	.LC17
	.word	.LC20
	.word	.LC19
	.word	.LC22
	.word	.LC21
.LFE4:
	.size	LR_analyze_radio_parameters, .-LR_analyze_radio_parameters
	.section	.text.LR_analyze_set_baud_rate,"ax",%progbits
	.align	2
	.type	LR_analyze_set_baud_rate, %function
LR_analyze_set_baud_rate:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #20]
	stmfd	sp!, {r0, r1, lr}
.LCFI4:
	ldr	r1, .L22
	add	r3, r3, #3
	str	r3, [sp, #0]
	ldr	r3, .L22+4
	mov	r2, #15
	str	r3, [sp, #4]
	mov	r3, #7
	ldr	r0, [r0, #12]
	bl	dev_extract_text_from_buffer
	ldmfd	sp!, {r2, r3, pc}
.L23:
	.align	2
.L22:
	.word	.LC23
	.word	.LC24
.LFE3:
	.size	LR_analyze_set_baud_rate, .-LR_analyze_set_baud_rate
	.section	.text.LR_analyze_set_modem_mode,"ax",%progbits
	.align	2
	.type	LR_analyze_set_modem_mode, %function
LR_analyze_set_modem_mode:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #20]
	stmfd	sp!, {r0, r1, lr}
.LCFI5:
	ldr	r1, .L25
	str	r3, [sp, #0]
	ldr	r3, .L25+4
	mov	r2, #16
	str	r3, [sp, #4]
	mov	r3, #3
	ldr	r0, [r0, #12]
	bl	dev_extract_text_from_buffer
	ldmfd	sp!, {r2, r3, pc}
.L26:
	.align	2
.L25:
	.word	.LC25
	.word	.LC26
.LFE2:
	.size	LR_analyze_set_modem_mode, .-LR_analyze_set_modem_mode
	.section	.text.LR_analyze_main_menu,"ax",%progbits
	.align	2
	.type	LR_analyze_main_menu, %function
LR_analyze_main_menu:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #0]
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI6:
	cmp	r3, #82
	mov	r4, r0
	ldreq	r1, .L31
	ldrne	r1, .L31+4
	mov	r2, #49
	ldr	r0, .L31+8
	bl	strlcpy
	add	r3, r4, #24
	str	r3, [sp, #0]
	ldr	r3, .L31+12
	ldr	r0, [r4, #12]
	str	r3, [sp, #4]
	ldr	r1, .L31+16
	mov	r2, #15
	mov	r3, #7
	bl	dev_extract_text_from_buffer
	add	r3, r4, #31
	str	r3, [sp, #0]
	ldr	r3, .L31+20
	ldr	r0, [r4, #12]
	str	r3, [sp, #4]
	ldr	r1, .L31+24
	mov	r2, #20
	mov	r3, #9
	bl	dev_extract_text_from_buffer
	add	r3, r4, #40
	str	r3, [sp, #0]
	ldr	r3, .L31+28
	ldr	r0, [r4, #12]
	str	r3, [sp, #4]
	ldr	r1, .L31+32
	mov	r2, #11
	mov	r3, #6
	bl	dev_extract_text_from_buffer
	ldmfd	sp!, {r2, r3, r4, pc}
.L32:
	.align	2
.L31:
	.word	.LC27
	.word	.LC28
	.word	GuiVar_CommOptionInfoText
	.word	.LC30
	.word	.LC29
	.word	.LC32
	.word	.LC31
	.word	.LC34
	.word	.LC33
.LFE1:
	.size	LR_analyze_main_menu, .-LR_analyze_main_menu
	.section	.text.LR_analyze_frequency,"ax",%progbits
	.align	2
	.type	LR_analyze_frequency, %function
LR_analyze_frequency:
.LFB5:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI7:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI8:
	fldd	d9, .L34
	ldr	r7, .L34+16
	fldd	d8, .L34+8
	ldr	r3, .L34+20
	sub	sp, sp, #24
.LCFI9:
	add	r6, sp, #8
	mov	r4, r0
	str	r6, [sp, #0]
	str	r3, [sp, #4]
	mov	r1, r7
	ldr	r0, [r0, #12]
	mov	r2, #4
	mov	r3, #6
	bl	dev_extract_text_from_buffer
	ldr	r3, .L34+24
	add	r5, sp, #16
	str	r3, [sp, #4]
	str	r5, [sp, #0]
	mov	r1, r7
	ldr	r0, [r4, #12]
	mov	r2, #11
	mov	r3, #6
	bl	dev_extract_text_from_buffer
	mov	r0, r6
	ldr	r7, [r4, #20]
	bl	atof
	fcpyd	d6, d9
	ldr	r6, .L34+28
	add	r7, r7, #10
	fmdrr	d7, r0, r1
	mov	r0, r7
	mov	r1, #10
	fmacd	d6, d7, d8
	fmrrd	r2, r3, d6
	str	r3, [sp, #0]
	mov	r3, r2
	mov	r2, r6
	bl	snprintf
	mov	r0, r5
	ldr	r4, [r4, #20]
	bl	atof
	fcpyd	d6, d9
	add	r4, r4, #20
	fmdrr	d7, r0, r1
	mov	r0, r4
	mov	r1, #10
	fmacd	d6, d7, d8
	fmrrd	r2, r3, d6
	str	r3, [sp, #0]
	mov	r3, r2
	mov	r2, r6
	bl	snprintf
	add	sp, sp, #24
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L35:
	.align	2
.L34:
	.word	0
	.word	1081815040
	.word	-1610612736
	.word	1064933785
	.word	.LC35
	.word	.LC36
	.word	.LC37
	.word	.LC38
.LFE5:
	.size	LR_analyze_frequency, .-LR_analyze_frequency
	.section	.text.LR_get_and_process_command,"ax",%progbits
	.align	2
	.type	LR_get_and_process_command, %function
LR_get_and_process_command:
.LFB15:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI10:
	sub	sp, sp, #32
.LCFI11:
	mov	r4, r1
	mov	r6, r0
	mov	r5, r2
.LBB22:
.LBB23:
	add	r0, sp, #8
	mov	r1, #0
	mov	r2, #23
	bl	memset
	ldr	r3, .L91+12
	cmp	r4, r3
	beq	.L48
	bhi	.L64
	sub	r3, r3, #8
	cmp	r4, r3
	beq	.L40
	bhi	.L65
	cmp	r4, #57
	bhi	.L66
	cmp	r4, #48
	bcs	.L38
	cmp	r4, #27
	bne	.L37
	b	.L38
.L66:
	cmp	r4, #65
	bcc	.L37
	cmp	r4, #71
	bls	.L38
	ldr	r3, .L91+16
	cmp	r4, r3
	bne	.L37
	b	.L87
.L65:
	ldr	r3, .L91+20
	cmp	r4, r3
	beq	.L44
	bhi	.L67
	sub	r3, r3, #2
	cmp	r4, r3
	beq	.L81
	bhi	.L55
	b	.L88
.L67:
	ldr	r3, .L91+24
	ldr	r1, [r6, #20]
	cmp	r4, r3
	beq	.L46
	addhi	r0, sp, #8
	addhi	r1, r1, #32
	bhi	.L83
	b	.L89
.L64:
	cmp	r4, #6016
	beq	.L56
	bhi	.L68
	ldr	r3, .L91+28
	cmp	r4, r3
	addeq	r0, sp, #8
	ldreq	r1, .L91+32
	beq	.L83
	bhi	.L69
	sub	r3, r3, #2
	cmp	r4, r3
	ldreq	r1, [r6, #20]
	addeq	r0, sp, #8
	addeq	r1, r1, #36
	beq	.L83
	bhi	.L81
	b	.L60
.L69:
	ldr	r3, .L91+36
	cmp	r4, r3
	ldreq	r1, [r6, #20]
	addeq	r0, sp, #8
	addeq	r1, r1, #41
	beq	.L83
	bhi	.L55
	b	.L81
.L68:
	ldr	r3, .L91+40
	cmp	r4, r3
	beq	.L60
	bhi	.L70
	sub	r3, r3, #2
	cmp	r4, r3
	ldreq	r1, [r6, #20]
	addeq	r0, sp, #8
	addeq	r1, r1, #44
	beq	.L83
	bhi	.L59
	b	.L81
.L70:
	ldr	r3, .L91+44
	cmp	r4, r3
	beq	.L62
	ldrcc	r1, [r6, #20]
	addcc	r0, sp, #8
	addcc	r1, r1, #49
	bcc	.L83
	add	r3, r3, #1
	cmp	r4, r3
	bne	.L37
	b	.L90
.L38:
	add	r0, sp, #8
	mov	r1, r4
	b	.L82
.L87:
	add	r0, sp, #8
	mov	r1, #255
.L82:
	mov	r2, #1
	bl	memset
	b	.L37
.L40:
	add	r0, sp, #8
	ldr	r1, [r6, #20]
	b	.L83
.L88:
	ldr	r1, .L91+48
	mov	r2, #23
	add	r0, sp, #8
	bl	strlcpy
	ldr	r0, [r6, #20]
	ldr	r1, .L91+52
	add	r0, r0, #3
	mov	r2, #7
	b	.L84
.L44:
	ldr	r0, [r6, #20]
	add	r0, r0, #10
	b	.L85
.L89:
	add	r0, r1, #20
.L85:
	bl	atof
	fldd	d7, .L91
	fmdrr	d6, r0, r1
	mov	r1, #4
	fsubd	d7, d6, d7
	fcvtsd	s13, d7
	fmrs	r0, s13
	bl	__round_float
	flds	s15, .L91+8
	fmsr	s14, r0
	fdivs	s14, s14, s15
	fmrs	r0, s14
	bl	roundf
	mov	r1, #23
	ldr	r2, .L91+56
	fmsr	s15, r0
	add	r0, sp, #8
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	bl	snprintf
	b	.L37
.L46:
	add	r0, sp, #8
	add	r1, r1, #30
	b	.L83
.L48:
	ldr	r1, [r6, #20]
	add	r0, sp, #8
	add	r1, r1, #34
	b	.L83
.L55:
	add	r0, sp, #8
	b	.L86
.L56:
	add	r0, sp, #8
	ldr	r1, .L91+60
	b	.L83
.L81:
	add	r0, sp, #8
	b	.L71
.L59:
	ldr	r0, [r6, #20]
	bl	atoi
	cmp	r0, #7
	add	r0, sp, #8
	bne	.L71
.L86:
	ldr	r1, .L91+64
	b	.L83
.L71:
	ldr	r1, .L91+68
	b	.L83
.L60:
	add	r0, sp, #8
	ldr	r1, .L91+72
	b	.L83
.L62:
	ldr	r1, [r6, #20]
	add	r0, sp, #8
	add	r1, r1, #58
	b	.L83
.L90:
	ldr	r1, [r6, #20]
	add	r0, sp, #8
	add	r1, r1, #61
.L83:
	mov	r2, #23
.L84:
	bl	strlcpy
.L37:
	ldrb	r0, [sp, #8]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L72
	ldr	r3, .L91+16
	cmp	r4, r3
	bls	.L73
	add	r0, sp, #8
	ldr	r1, .L91+76
	mov	r2, #23
	bl	strlcat
.L73:
	add	r0, sp, #8
	bl	strlen
	ldr	r1, .L91+80
	ldr	r2, .L91+84
	mov	r7, #0
	add	r0, r0, #1
	bl	mem_malloc_debug
	mov	r4, r0
	add	r0, sp, #8
	bl	strlen
	add	r1, sp, #8
	mov	r2, r0
	mov	r0, r4
	bl	memcpy
	add	r0, sp, #8
	bl	strlen
.LBE23:
.LBE22:
	cmp	r4, r7
.LBB25:
.LBB24:
	strb	r7, [r4, r0]
.LBE24:
.LBE25:
	moveq	r0, r4
	beq	.L72
.LBB26:
.LBB27:
	ldr	r6, .L91+88
	mov	r1, #500
	mov	r2, #100
	ldr	r0, [r6, #368]
	bl	RCVD_DATA_enable_hunting_mode
	mov	r0, r4
	bl	strlen
	ldr	sl, .L91+92
	ldr	r2, [r6, #368]
	ldr	r3, .L91+96
	mla	sl, r2, sl, r3
	ldr	r3, .L91+100
	str	r5, [sl, r3]
	mov	r8, r0
	mov	r0, r5
	bl	strlen
	ldr	r3, .L91+104
	cmp	r8, r7
	str	r0, [sl, r3]
	beq	.L74
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L74
	mov	r2, #2
	mov	r3, #1
	ldr	r0, [r6, #368]
	mov	r1, r4
	stmia	sp, {r2, r3}
	mov	r2, r8
	mov	r3, r7
	bl	AddCopyOfBlockToXmitList
.L74:
.LBE27:
.LBE26:
	mov	r0, r4
	ldr	r1, .L91+80
	ldr	r2, .L91+108
	bl	mem_free_debug
	mov	r0, #1
.L72:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L92:
	.align	2
.L91:
	.word	0
	.word	1081815040
	.word	1003277517
	.word	6008
	.word	5000
	.word	6004
	.word	6006
	.word	6012
	.word	.LC44
	.word	6014
	.word	6020
	.word	6022
	.word	.LC39
	.word	.LC40
	.word	.LC42
	.word	.LC45
	.word	.LC41
	.word	.LC8
	.word	.LC43
	.word	.LC46
	.word	.LC47
	.word	525
	.word	comm_mngr
	.word	4280
	.word	SerDrvrVars_s
	.word	4216
	.word	4220
	.word	829
.LFE15:
	.size	LR_get_and_process_command, .-LR_get_and_process_command
	.section	.text.send_no_response_esc_to_radio,"ax",%progbits
	.align	2
	.type	send_no_response_esc_to_radio, %function
send_no_response_esc_to_radio:
.LFB12:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI12:
	ldr	r5, .L94
	add	r4, sp, #12
	mov	r3, #27
	ldr	r0, [r5, #368]
	mov	r1, #100
	mov	r2, #0
	strb	r3, [r4, #-1]!
	bl	RCVD_DATA_enable_hunting_mode
	mov	r3, #2
	mov	r2, #1
	str	r3, [sp, #0]
	ldr	r0, [r5, #368]
	mov	r1, r4
	mov	r3, #0
	str	r2, [sp, #4]
	bl	AddCopyOfBlockToXmitList
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L95:
	.align	2
.L94:
	.word	comm_mngr
.LFE12:
	.size	send_no_response_esc_to_radio, .-send_no_response_esc_to_radio
	.section	.text.exit_radio_programming_mode,"ax",%progbits
	.align	2
	.type	exit_radio_programming_mode, %function
exit_radio_programming_mode:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI13:
	bl	send_no_response_esc_to_radio
	ldr	r4, .L97
	ldr	r0, [r4, #368]
	bl	set_reset_INACTIVE_to_serial_port_device
	ldr	r3, .L97+4
	ldr	r0, [r4, #368]
	ldr	r1, [r3, #116]
	bl	SERIAL_set_baud_rate_on_A_or_B
	ldr	r3, .L97+8
	str	r3, [r4, #372]
	ldmfd	sp!, {r4, pc}
.L98:
	.align	2
.L97:
	.word	comm_mngr
	.word	port_device_table
	.word	6000
.LFE13:
	.size	exit_radio_programming_mode, .-exit_radio_programming_mode
	.section	.text.process_list.isra.0,"ax",%progbits
	.align	2
	.type	process_list.isra.0, %function
process_list.isra.0:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r1, #0]
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI14:
	cmp	r3, #4864
	mov	r6, r0
	mov	r4, r1
	mov	r5, r2
	bne	.L100
	ldr	r0, [r1, #16]
	cmp	r0, #0
	beq	.L101
	ldr	r2, [r1, #20]
	cmp	r2, #0
	beq	.L101
	ldr	r1, [r5, #0]
	bl	find_string_in_block
	subs	r7, r0, #0
	beq	.L102
	ldr	r3, [r4, #16]
	str	r7, [r6, #12]
	rsb	r7, r7, r3
	ldr	r3, [r4, #20]
	add	r7, r7, r3
	ldr	r3, [r5, #4]
	str	r7, [r6, #16]
	cmp	r3, #0
	beq	.L105
	mov	r0, r6
	blx	r3
.L105:
	mov	r7, #1
.L102:
	ldr	r0, [r4, #16]
	ldr	r1, .L106
	ldr	r2, .L106+4
	bl	mem_free_debug
	cmp	r7, #0
	beq	.L100
.L101:
	mov	r0, r6
	ldr	r1, [r5, #8]
	ldr	r2, [r5, #12]
	bl	LR_get_and_process_command
	adds	r0, r0, #0
	movne	r0, #1
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L100:
	bl	send_no_response_esc_to_radio
	bl	send_no_response_esc_to_radio
	bl	send_no_response_esc_to_radio
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L107:
	.align	2
.L106:
	.word	.LC47
	.word	1019
.LFE30:
	.size	process_list.isra.0, .-process_list.isra.0
	.section	.text.LR_FREEWAVE_post_device_query_to_queue,"ax",%progbits
	.align	2
	.global	LR_FREEWAVE_post_device_query_to_queue
	.type	LR_FREEWAVE_post_device_query_to_queue, %function
LR_FREEWAVE_post_device_query_to_queue:
.LFB0:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L111
	str	lr, [sp, #-4]!
.LCFI15:
	cmp	r1, #87
	sub	sp, sp, #40
.LCFI16:
	mov	r2, #1
	movne	r1, #4352
	moveq	r1, #4608
	str	r0, [sp, #32]
	str	r2, [r3, #0]
	mov	r0, sp
	str	r1, [sp, #0]
	bl	COMM_MNGR_post_event_with_details
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L112:
	.align	2
.L111:
	.word	.LANCHOR1
.LFE0:
	.size	LR_FREEWAVE_post_device_query_to_queue, .-LR_FREEWAVE_post_device_query_to_queue
	.section	.text.LR_FREEWAVE_power_control,"ax",%progbits
	.align	2
	.global	LR_FREEWAVE_power_control
	.type	LR_FREEWAVE_power_control, %function
LR_FREEWAVE_power_control:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI17:
	mov	r4, r0
	mov	r5, r1
	bl	set_reset_INACTIVE_to_serial_port_device
	mov	r0, r4
	mov	r1, r5
	ldmfd	sp!, {r4, r5, lr}
	b	GENERIC_freewave_card_power_control
.LFE23:
	.size	LR_FREEWAVE_power_control, .-LR_FREEWAVE_power_control
	.section	.text.LR_FREEWAVE_initialize_device_exchange,"ax",%progbits
	.align	2
	.global	LR_FREEWAVE_initialize_device_exchange
	.type	LR_FREEWAVE_initialize_device_exchange, %function
LR_FREEWAVE_initialize_device_exchange:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI18:
.LBB37:
	ldr	r4, .L127
	ldr	r5, [r4, #20]
	cmp	r5, #0
	beq	.L115
.L124:
.LBE37:
.LBB38:
	ldr	r5, .L127+4
	ldr	r4, .L127
	ldr	r3, [r5, #364]
	cmp	r3, #4608
	bne	.L116
	ldr	r0, .L127+8
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
.LBB39:
.LBB40:
	ldr	sl, .L127+12
.LBE40:
	mov	r3, #87
	mov	r5, #0
	str	r3, [r4, #0]
	ldr	r0, .L127+8
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
.LBB41:
	mov	r9, #12
.LBE41:
	str	r5, [r4, #8]
	str	r5, [r4, #4]
.L119:
.LBB42:
	ldr	r4, .L127
	mla	r8, r9, r5, sl
	add	r0, r4, #52
	mov	r1, r8
	mov	r2, #3
	bl	strncmp
	ldr	r6, .L127+12
	mov	r7, #12
	cmp	r0, #0
	bne	.L117
	mov	r1, r8
	str	r5, [r4, #48]
	add	r0, r4, #52
	mov	r2, #3
	bl	strlcpy
	ldr	r1, [r4, #48]
	ldr	r0, [r4, #20]
	mla	r1, r7, r1, r6
	mov	r2, #2
	add	r1, r1, #3
	add	r0, r0, #30
	bl	strlcpy
	ldr	r1, [r4, #48]
	ldr	r0, [r4, #20]
	mla	r1, r7, r1, r6
	mov	r2, #2
	add	r1, r1, #5
	add	r0, r0, #32
	bl	strlcpy
	ldr	r3, [r4, #48]
	ldr	r0, [r4, #20]
	mla	r6, r7, r3, r6
	mov	r2, #5
	add	r1, r6, #7
	add	r0, r0, #44
	bl	strlcpy
	b	.L118
.L117:
	add	r5, r5, #1
	cmp	r5, #6
	bne	.L119
.L118:
.LBE42:
	ldr	r2, .L127+16
	ldr	r3, .L127+4
	str	r2, [r3, #372]
	b	.L120
.L116:
.LBE39:
	ldr	r0, .L127+20
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
.LBB43:
	mov	r3, #82
	str	r3, [r4, #0]
	ldr	r0, .L127+20
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	mov	r3, #0
	str	r3, [r4, #4]
	str	r3, [r4, #8]
	mov	r3, #4000
	str	r3, [r5, #372]
.L120:
.LBE43:
.LBE38:
	ldr	r4, .L127
	ldr	r3, .L127+24
	ldr	r2, [r4, #4]
	mov	r0, r4
	add	r3, r3, r2, asl #4
	ldr	r1, [r3, #8]
	ldr	r2, [r3, #12]
	bl	LR_get_and_process_command
	cmp	r0, #0
	beq	.L114
.LBB44:
	mov	r2, #49
	ldr	r1, .L127+28
	ldr	r0, .L127+32
	bl	strlcpy
	ldr	r3, [r4, #0]
	ldr	r4, .L127+4
	cmp	r3, #82
	ldreq	r0, .L127+20
	ldrne	r0, .L127+8
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	mov	r1, #19200
	ldr	r0, [r4, #368]
	bl	SERIAL_set_baud_rate_on_A_or_B
	ldr	r0, [r4, #368]
	bl	set_reset_ACTIVE_to_serial_port_device
	mov	r0, #20
	bl	vTaskDelay
	ldr	r0, [r4, #368]
	bl	set_reset_INACTIVE_to_serial_port_device
.LBE44:
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	ldr	r0, [r4, #384]
	mov	r2, #2000
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L114
.L115:
.LBB45:
	ldr	r3, .L127+36
	add	r0, r4, #24
	str	r3, [r4, #20]
	ldr	r1, .L127+40
	mov	r2, #7
	bl	strlcpy
	add	r0, r4, #31
	ldr	r1, .L127+40
	mov	r2, #9
	bl	strlcpy
	add	r0, r4, #40
	ldr	r1, .L127+40
	mov	r2, #6
	bl	strlcpy
	str	r5, [r4, #48]
	add	r0, r4, #52
	ldr	r1, .L127+40
	mov	r2, #3
	bl	strlcpy
	b	.L124
.L114:
.LBE45:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, pc}
.L128:
	.align	2
.L127:
	.word	.LANCHOR2
	.word	comm_mngr
	.word	36870
	.word	.LANCHOR0
	.word	5000
	.word	36867
	.word	.LANCHOR3
	.word	.LC48
	.word	GuiVar_CommOptionInfoText
	.word	.LANCHOR4
	.word	.LC49
.LFE24:
	.size	LR_FREEWAVE_initialize_device_exchange, .-LR_FREEWAVE_initialize_device_exchange
	.section	.text.LR_FREEWAVE_exchange_processing,"ax",%progbits
	.align	2
	.global	LR_FREEWAVE_exchange_processing
	.type	LR_FREEWAVE_exchange_processing, %function
LR_FREEWAVE_exchange_processing:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, lr}
.LCFI19:
	ldr	r4, .L146
	mov	r2, #0
	mov	r3, r2
	mov	r6, r0
	mvn	r5, #0
	ldr	r0, [r4, #384]
	mov	r1, #1
	str	r5, [sp, #0]
	bl	xTimerGenericCommand
	ldr	r3, [r4, #0]
	cmp	r3, #5
	bne	.L129
.LBB48:
	ldr	r3, [r4, #372]
	ldr	r7, .L146+4
	cmp	r3, #4000
	beq	.L132
	ldr	r2, .L146+8
	cmp	r3, r2
	movne	r3, #73
	strne	r3, [r7, #0]
	bne	.L129
	b	.L145
.L132:
	ldr	r3, [r7, #4]
	ldr	r2, .L146+12
	add	r3, r3, #1
	mov	r0, r7
	mov	r1, r6
	add	r2, r2, r3, asl #4
	str	r3, [r7, #4]
	bl	process_list.isra.0
	cmp	r0, #0
	bne	.L137
	bl	exit_radio_programming_mode
	ldr	r3, [r7, #4]
	cmp	r3, #1
	ldreq	r0, .L146+16
	ldreq	r1, .L146+20
	beq	.L143
.L135:
	cmp	r3, #11
	ldrhi	r0, .L146+24
	bhi	.L139
	ldr	r0, .L146+16
	ldr	r1, .L146+28
.L143:
	mov	r2, #49
	bl	strlcpy
	ldr	r0, .L146+32
	b	.L139
.L145:
	ldr	r3, [r7, #8]
	ldr	r2, .L146+36
	add	r3, r3, #1
	mov	r0, r7
	mov	r1, r6
	add	r2, r2, r3, asl #4
	str	r3, [r7, #8]
	bl	process_list.isra.0
	cmp	r0, #0
	bne	.L137
	bl	exit_radio_programming_mode
	ldr	r3, [r7, #8]
	cmp	r3, #1
	ldreq	r0, .L146+16
	ldreq	r1, .L146+20
	beq	.L144
.L138:
	cmp	r3, #65
	ldrhi	r0, .L146+40
	bhi	.L139
	ldr	r0, .L146+16
	ldr	r1, .L146+44
.L144:
	mov	r2, #49
	bl	strlcpy
	ldr	r0, .L146+48
.L139:
.LBE48:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, lr}
.LBB49:
	b	COMM_MNGR_device_exchange_results_to_key_process_task
.L137:
	ldr	r0, [r4, #384]
	mov	r1, #2
	mov	r2, #2000
	mov	r3, #0
	str	r5, [sp, #0]
	bl	xTimerGenericCommand
.L129:
.LBE49:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L147:
	.align	2
.L146:
	.word	comm_mngr
	.word	.LANCHOR2
	.word	5000
	.word	.LANCHOR3
	.word	GuiVar_CommOptionInfoText
	.word	.LC50
	.word	36865
	.word	.LC51
	.word	36866
	.word	.LANCHOR5
	.word	36868
	.word	.LC52
	.word	36869
.LFE25:
	.size	LR_FREEWAVE_exchange_processing, .-LR_FREEWAVE_exchange_processing
	.section	.text.LR_FREEWAVE_initialize_the_connection_process,"ax",%progbits
	.align	2
	.global	LR_FREEWAVE_initialize_the_connection_process
	.type	LR_FREEWAVE_initialize_the_connection_process, %function
LR_FREEWAVE_initialize_the_connection_process:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI20:
	mov	r6, r0
	beq	.L149
	ldr	r0, .L151
	bl	Alert_Message
.L149:
	ldr	r3, .L151+4
	ldr	r2, .L151+8
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #44]
	cmp	r3, #0
	bne	.L150
	ldr	r0, .L151+12
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_Message
.L150:
	ldr	r3, .L151+16
	ldr	r4, .L151+20
	ldr	r3, [r3, #0]
	mov	r5, #0
	mov	r0, r6
	str	r3, [r4, #12]
	str	r6, [r4, #4]
	str	r5, [r4, #8]
	bl	power_down_device
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L151+24
	mov	r1, #2
	ldr	r0, [r3, #40]
	mov	r2, #1000
	mov	r3, r5
	bl	xTimerGenericCommand
	mov	r3, #1
	str	r3, [r4, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, pc}
.L152:
	.align	2
.L151:
	.word	.LC53
	.word	config_c
	.word	port_device_table
	.word	.LC54
	.word	my_tick_count
	.word	.LANCHOR6
	.word	cics
.LFE26:
	.size	LR_FREEWAVE_initialize_the_connection_process, .-LR_FREEWAVE_initialize_the_connection_process
	.global	__udivsi3
	.section	.text.LR_FREEWAVE_connection_processing,"ax",%progbits
	.align	2
	.global	LR_FREEWAVE_connection_processing
	.type	LR_FREEWAVE_connection_processing, %function
LR_FREEWAVE_connection_processing:
.LFB27:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI21:
	sub	r0, r0, #121
	cmp	r0, #1
	bls	.L154
	ldr	r0, .L161
	bl	Alert_Message
	b	.L155
.L154:
	ldr	r4, .L161+4
	ldr	r5, [r4, #0]
	cmp	r5, #1
	beq	.L157
	cmp	r5, #2
	bne	.L153
	b	.L160
.L157:
	ldr	r0, [r4, #4]
	bl	power_up_device
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L161+8
	mov	r1, #2
	ldr	r0, [r3, #40]
	mov	r2, #2000
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r3, #2
	str	r3, [r4, #0]
	b	.L153
.L160:
	ldr	r3, .L161+12
	ldr	r2, .L161+16
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #44]
	cmp	r3, #0
	beq	.L155
	ldr	r0, [r4, #4]
	blx	r3
	subs	r3, r0, #0
	beq	.L159
	mov	r2, #49
	ldr	r1, .L161+20
	ldr	r0, .L161+24
	bl	strlcpy
	ldr	r3, .L161+28
	mov	r1, #200
	ldr	r0, [r3, #0]
	ldr	r3, [r4, #12]
	rsb	r0, r3, r0
	bl	__udivsi3
	mov	r1, r0
	ldr	r0, .L161+32
	bl	Alert_Message_va
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities
.L159:
	ldr	r2, [r4, #8]
	cmp	r2, #119
	bhi	.L155
	add	r2, r2, #1
	str	r2, [r4, #8]
	mvn	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L161+8
	mov	r1, r5
	ldr	r0, [r2, #40]
	mov	r2, #200
	bl	xTimerGenericCommand
	b	.L153
.L155:
	ldr	r3, .L161+8
	mov	r2, #1
	mov	r0, #123
	str	r2, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	CONTROLLER_INITIATED_post_event
.L153:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, pc}
.L162:
	.align	2
.L161:
	.word	.LC55
	.word	.LANCHOR6
	.word	cics
	.word	config_c
	.word	port_device_table
	.word	.LC56
	.word	GuiVar_CommTestStatus
	.word	my_tick_count
	.word	.LC57
.LFE27:
	.size	LR_FREEWAVE_connection_processing, .-LR_FREEWAVE_connection_processing
	.section	.text.LR_FREEWAVE_extract_changes_from_guivars,"ax",%progbits
	.align	2
	.global	LR_FREEWAVE_extract_changes_from_guivars
	.type	LR_FREEWAVE_extract_changes_from_guivars, %function
LR_FREEWAVE_extract_changes_from_guivars:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI22:
	ldr	r4, .L170
	ldr	r5, .L170+4
	mov	r1, #3
	ldr	r2, .L170+8
	ldr	r3, [r5, #0]
	ldr	r0, [r4, #20]
	bl	snprintf
	ldr	r3, .L170+12
	mov	r1, #3
	ldr	r2, .L170+8
	ldr	r3, [r3, #0]
	add	r0, r4, #52
	bl	snprintf
	ldr	r0, [r4, #20]
	ldr	r3, .L170+16
	add	r0, r0, #34
	ldr	r3, [r3, #0]
	mov	r1, #2
	ldr	r2, .L170+8
	bl	snprintf
	ldr	r3, [r5, #0]
	ldr	r0, [r4, #20]
	cmp	r3, #2
	add	r0, r0, #58
	bne	.L164
	ldr	r1, .L170+20
	mov	r2, #3
	bl	strlcpy
	ldr	r0, [r4, #20]
	ldr	r1, .L170+20
	add	r0, r0, #61
	b	.L169
.L164:
	cmp	r3, #7
	bne	.L166
	ldr	r1, .L170+20
	mov	r2, #3
	bl	strlcpy
	ldr	r0, [r4, #20]
	ldr	r1, .L170+24
	add	r0, r0, #61
	b	.L169
.L166:
	ldr	r3, .L170+28
	mov	r1, #3
	ldr	r2, .L170+8
	ldr	r3, [r3, #0]
	bl	snprintf
	ldr	r0, [r4, #20]
	ldr	r1, .L170+32
	add	r0, r0, #61
.L169:
	mov	r2, #3
	bl	strlcpy
	ldr	r3, .L170+4
	ldr	r3, [r3, #0]
	cmp	r3, #2
	ldr	r3, .L170
	ldr	r0, [r3, #20]
	add	r0, r0, #41
	bne	.L167
	ldr	r3, .L170+36
	mov	r1, #3
	ldr	r2, .L170+8
	ldr	r3, [r3, #0]
	bl	snprintf
	b	.L168
.L167:
	ldr	r1, .L170+20
	mov	r2, #3
	bl	strlcpy
.L168:
	ldr	r4, .L170
	ldr	r6, .L170+40
	ldr	r0, [r4, #20]
	ldr	r3, [r6, #0]
	ldr	r5, .L170+44
	str	r3, [sp, #0]
	mov	r1, #10
	ldr	r3, [r5, #0]
	ldr	r2, .L170+48
	add	r0, r0, #10
	bl	snprintf
	ldr	r3, [r6, #0]
	ldr	r0, [r4, #20]
	str	r3, [sp, #0]
	mov	r1, #10
	ldr	r3, [r5, #0]
	ldr	r2, .L170+48
	add	r0, r0, #20
	bl	snprintf
	ldr	r3, .L170+52
	ldr	r0, [r4, #20]
	ldr	r2, .L170+56
	ldr	r3, [r3, #0]
	add	r0, r0, #36
	mov	r1, #5
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	snprintf
.L171:
	.align	2
.L170:
	.word	.LANCHOR2
	.word	GuiVar_LRMode
	.word	.LC42
	.word	GuiVar_LRGroup
	.word	GuiVar_LRBeaconRate
	.word	.LC8
	.word	.LC41
	.word	GuiVar_LRSlaveLinksToRepeater
	.word	.LC10
	.word	GuiVar_LRRepeaterInUse
	.word	GuiVar_LRFrequency_Decimal
	.word	GuiVar_LRFrequency_WholeNum
	.word	.LC58
	.word	GuiVar_LRTransmitPower
	.word	.LC59
.LFE28:
	.size	LR_FREEWAVE_extract_changes_from_guivars, .-LR_FREEWAVE_extract_changes_from_guivars
	.global	__umodsi3
	.section	.text.LR_FREEWAVE_copy_settings_into_guivars,"ax",%progbits
	.align	2
	.global	LR_FREEWAVE_copy_settings_into_guivars
	.type	LR_FREEWAVE_copy_settings_into_guivars, %function
LR_FREEWAVE_copy_settings_into_guivars:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI23:
	ldr	r4, .L182
	mov	r2, #9
	add	r1, r4, #31
	ldr	r0, .L182+4
	bl	strlcpy
	add	r1, r4, #24
	mov	r2, #49
	ldr	r0, .L182+8
	bl	strlcpy
	ldr	r0, [r4, #20]
	bl	atoi
	ldr	r5, .L182+12
	cmp	r0, #3
	beq	.L181
.L173:
	ldr	r0, [r4, #20]
	bl	atoi
	cmp	r0, #7
	movne	r3, #2
	strne	r3, [r5, #0]
	bne	.L174
.L181:
	str	r0, [r5, #0]
.L174:
	ldr	r0, .L182+16
	bl	atoi
	ldr	r3, .L182+20
	ldr	r4, .L182
	ldr	r5, .L182+24
	str	r0, [r3, #0]
	sub	r0, r0, #1
	cmp	r0, #5
	ldr	r0, [r4, #20]
	movhi	r2, #1
	strhi	r2, [r3, #0]
	add	r0, r0, #34
	bl	atoi
	ldr	r3, .L182+28
	str	r0, [r3, #0]
	ldr	r0, [r4, #20]
	add	r0, r0, #58
	bl	atoi
	ldr	r3, .L182+32
	str	r0, [r3, #0]
	ldr	r0, [r4, #20]
	add	r0, r0, #41
	bl	atoi
	ldr	r3, .L182+36
	ldr	r1, .L182+40
	str	r0, [r3, #0]
	ldr	r0, [r4, #20]
	add	r0, r0, #10
	bl	strtok
	bl	atoi
	ldr	r3, .L182+44
	ldr	r1, .L182+40
	str	r0, [r3, #0]
	mov	r0, #0
	bl	strtok
	bl	atoi
	mov	r1, #125
	str	r0, [r5, #0]
	mov	r6, r0
	bl	__umodsi3
	cmp	r0, #0
	addne	r6, r6, #125
	rsbne	r6, r0, r6
	ldr	r0, [r4, #20]
	strne	r6, [r5, #0]
	add	r0, r0, #36
	bl	atoi
	ldr	r3, .L182+48
	str	r0, [r3, #0]
	ldr	r3, .L182+52
	ldr	r3, [r3, #368]
	cmp	r3, #1
	bne	.L178
	ldr	r3, .L182+36
	ldr	r1, [r3, #0]
	ldr	r3, .L182+56
	ldrb	r2, [r3, #52]	@ zero_extendqisi2
	mov	r0, r2, lsr #6
	and	r0, r0, #1
	cmp	r0, r1
	ldmeqfd	sp!, {r4, r5, r6, pc}
	and	r1, r1, #1
	bic	r2, r2, #64
	orr	r2, r2, r1, asl #6
	strb	r2, [r3, #52]
	b	.L180
.L178:
	cmp	r3, #2
	ldmnefd	sp!, {r4, r5, r6, pc}
	ldr	r3, .L182+36
	ldr	r1, [r3, #0]
	ldr	r3, .L182+56
	ldrb	r2, [r3, #53]	@ zero_extendqisi2
	mov	r0, r2, lsr #2
	and	r0, r0, #1
	cmp	r0, r1
	ldmeqfd	sp!, {r4, r5, r6, pc}
	and	r1, r1, #1
	bic	r2, r2, #4
	orr	r2, r2, r1, asl #2
	strb	r2, [r3, #53]
.L180:
	bl	CONTROLLER_INITIATED_update_comm_server_registration_info
	mov	r0, #0
	mov	r1, #2
	ldmfd	sp!, {r4, r5, r6, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L183:
	.align	2
.L182:
	.word	.LANCHOR2
	.word	GuiVar_LRSerialNumber
	.word	GuiVar_LRFirmwareVer
	.word	GuiVar_LRMode
	.word	.LANCHOR2+52
	.word	GuiVar_LRGroup
	.word	GuiVar_LRFrequency_Decimal
	.word	GuiVar_LRBeaconRate
	.word	GuiVar_LRSlaveLinksToRepeater
	.word	GuiVar_LRRepeaterInUse
	.word	.LC60
	.word	GuiVar_LRFrequency_WholeNum
	.word	GuiVar_LRTransmitPower
	.word	comm_mngr
	.word	config_c
.LFE29:
	.size	LR_FREEWAVE_copy_settings_into_guivars, .-LR_FREEWAVE_copy_settings_into_guivars
	.global	LR_write_list
	.global	LR_read_list
	.global	LR_group_list
	.global	LR_PROGRAMMING_querying_device
	.section	.bss.LR_cs,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	LR_cs, %object
	.size	LR_cs, 16
LR_cs:
	.space	16
	.section	.data.LR_write_list,"aw",%progbits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	LR_write_list, %object
	.size	LR_write_list, 1072
LR_write_list:
	.word	.LC61
	.word	0
	.word	5000
	.word	.LC62
	.word	.LC63
	.word	LR_analyze_main_menu
	.word	48
	.word	.LC62
	.word	.LC64
	.word	0
	.word	6000
	.word	.LC62
	.word	.LC64
	.word	0
	.word	70
	.word	.LC65
	.word	.LC66
	.word	0
	.word	48
	.word	.LC67
	.word	.LC68
	.word	0
	.word	48
	.word	.LC65
	.word	.LC66
	.word	0
	.word	27
	.word	.LC62
	.word	.LC64
	.word	LR_analyze_set_modem_mode
	.word	27
	.word	.LC62
	.word	.LC63
	.word	0
	.word	49
	.word	.LC62
	.word	.LC69
	.word	0
	.word	6001
	.word	.LC62
	.word	.LC69
	.word	0
	.word	68
	.word	.LC70
	.word	.LC70
	.word	0
	.word	51
	.word	.LC62
	.word	.LC69
	.word	0
	.word	70
	.word	.LC71
	.word	.LC72
	.word	0
	.word	49
	.word	.LC62
	.word	.LC69
	.word	LR_analyze_set_baud_rate
	.word	27
	.word	.LC62
	.word	.LC63
	.word	0
	.word	51
	.word	.LC62
	.word	.LC73
	.word	0
	.word	49
	.word	.LC74
	.word	.LC75
	.word	0
	.word	6006
	.word	.LC62
	.word	.LC73
	.word	0
	.word	50
	.word	.LC76
	.word	.LC77
	.word	0
	.word	6007
	.word	.LC62
	.word	.LC73
	.word	0
	.word	51
	.word	.LC78
	.word	.LC79
	.word	0
	.word	6008
	.word	.LC62
	.word	.LC73
	.word	0
	.word	52
	.word	.LC80
	.word	.LC81
	.word	0
	.word	6009
	.word	.LC62
	.word	.LC73
	.word	0
	.word	53
	.word	.LC82
	.word	.LC83
	.word	0
	.word	6010
	.word	.LC62
	.word	.LC73
	.word	0
	.word	55
	.word	.LC84
	.word	.LC85
	.word	0
	.word	6011
	.word	.LC62
	.word	.LC73
	.word	0
	.word	56
	.word	.LC86
	.word	.LC87
	.word	0
	.word	6012
	.word	.LC62
	.word	.LC73
	.word	0
	.word	57
	.word	.LC88
	.word	.LC89
	.word	0
	.word	6013
	.word	.LC62
	.word	.LC73
	.word	0
	.word	48
	.word	.LC90
	.word	.LC90
	.word	0
	.word	70
	.word	.LC91
	.word	.LC92
	.word	0
	.word	49
	.word	.LC93
	.word	.LC94
	.word	0
	.word	6002
	.word	.LC91
	.word	.LC92
	.word	0
	.word	50
	.word	.LC95
	.word	.LC95
	.word	0
	.word	6003
	.word	.LC91
	.word	.LC92
	.word	0
	.word	48
	.word	.LC96
	.word	.LC97
	.word	0
	.word	6002
	.word	.LC98
	.word	.LC99
	.word	0
	.word	6004
	.word	.LC100
	.word	.LC101
	.word	0
	.word	6005
	.word	.LC91
	.word	.LC92
	.word	0
	.word	27
	.word	.LC62
	.word	.LC73
	.word	LR_analyze_radio_parameters
	.word	48
	.word	.LC90
	.word	.LC90
	.word	0
	.word	70
	.word	.LC102
	.word	.LC103
	.word	LR_analyze_frequency
	.word	27
	.word	.LC62
	.word	.LC73
	.word	0
	.word	27
	.word	.LC62
	.word	.LC63
	.word	0
	.word	53
	.word	.LC62
	.word	.LC104
	.word	0
	.word	48
	.word	.LC105
	.word	.LC106
	.word	0
	.word	6014
	.word	.LC62
	.word	.LC104
	.word	0
	.word	49
	.word	.LC107
	.word	.LC108
	.word	0
	.word	6015
	.word	.LC62
	.word	.LC104
	.word	0
	.word	50
	.word	.LC109
	.word	.LC110
	.word	0
	.word	6016
	.word	.LC62
	.word	.LC104
	.word	0
	.word	51
	.word	.LC111
	.word	.LC112
	.word	0
	.word	6017
	.word	.LC62
	.word	.LC104
	.word	0
	.word	54
	.word	.LC113
	.word	.LC114
	.word	0
	.word	6018
	.word	.LC62
	.word	.LC104
	.word	0
	.word	65
	.word	.LC115
	.word	.LC116
	.word	0
	.word	6019
	.word	.LC62
	.word	.LC104
	.word	0
	.word	66
	.word	.LC117
	.word	.LC118
	.word	0
	.word	6020
	.word	.LC62
	.word	.LC104
	.word	0
	.word	67
	.word	.LC119
	.word	.LC120
	.word	0
	.word	6022
	.word	.LC121
	.word	.LC122
	.word	0
	.word	6023
	.word	.LC62
	.word	.LC104
	.word	LR_analyze_multipoint_parameters
	.word	27
	.word	.LC62
	.word	.LC63
	.word	LR_final_radio_verification
	.word	99999
	.word	.LC49
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Verifying settings...\000"
.LC1:
	.ascii	"Number Repeaters\000"
.LC2:
	.ascii	"NREP\000"
.LC3:
	.ascii	"NetWork ID\000"
.LC4:
	.ascii	"NETID\000"
.LC5:
	.ascii	"SubNet ID\000"
.LC6:
	.ascii	"SNID\000"
.LC7:
	.ascii	"Roaming\000"
.LC8:
	.ascii	"0\000"
.LC9:
	.ascii	"Disabled\000"
.LC10:
	.ascii	"F\000"
.LC11:
	.ascii	"Rcv=\000"
.LC12:
	.ascii	"RCVID\000"
.LC13:
	.ascii	"Xmit=\000"
.LC14:
	.ascii	"XMTID\000"
.LC15:
	.ascii	"Max Packet Size\000"
.LC16:
	.ascii	"MAXP\000"
.LC17:
	.ascii	"Min Packet Size\000"
.LC18:
	.ascii	"MINP\000"
.LC19:
	.ascii	"Xmit Rate\000"
.LC20:
	.ascii	"XRATE\000"
.LC21:
	.ascii	"RF Xmit Power\000"
.LC22:
	.ascii	"XPOWER\000"
.LC23:
	.ascii	"Modem Baud is\000"
.LC24:
	.ascii	"BAUD\000"
.LC25:
	.ascii	"Modem Mode is\000"
.LC26:
	.ascii	"MODE\000"
.LC27:
	.ascii	"Reading device settings...\000"
.LC28:
	.ascii	"Saving device settings...\000"
.LC29:
	.ascii	"LRS455 Version\000"
.LC30:
	.ascii	"VERSION\000"
.LC31:
	.ascii	"Modem Serial Number\000"
.LC32:
	.ascii	"SN\000"
.LC33:
	.ascii	"Model Code\000"
.LC34:
	.ascii	"MODEL\000"
.LC35:
	.ascii	"  0 \000"
.LC36:
	.ascii	"XMTFREQ\000"
.LC37:
	.ascii	"RCVFREQ\000"
.LC38:
	.ascii	"%0.04f\000"
.LC39:
	.ascii	"3\000"
.LC40:
	.ascii	"057600\000"
.LC41:
	.ascii	"1\000"
.LC42:
	.ascii	"%d\000"
.LC43:
	.ascii	"5\000"
.LC44:
	.ascii	"32\000"
.LC45:
	.ascii	"8\000"
.LC46:
	.ascii	"\015\012\000"
.LC47:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_LR_FREEWAVE.c\000"
.LC48:
	.ascii	"Connecting to device...\000"
.LC49:
	.ascii	"\000"
.LC50:
	.ascii	"No response from device...\000"
.LC51:
	.ascii	"Unable to read data...\000"
.LC52:
	.ascii	"Unable to write data...\000"
.LC53:
	.ascii	"Why is the LR not on PORT A?\000"
.LC54:
	.ascii	"Unexpected NULL is_connected function\000"
.LC55:
	.ascii	"Connection Process : UNXEXP EVENT\000"
.LC56:
	.ascii	"LR Connection\000"
.LC57:
	.ascii	"LR Radio sync'd to master in %u seconds\000"
.LC58:
	.ascii	"%3d.%04d\000"
.LC59:
	.ascii	"%2d\000"
.LC60:
	.ascii	".\000"
.LC61:
	.ascii	"not used\000"
.LC62:
	.ascii	"Enter Choice\000"
.LC63:
	.ascii	"MAIN MENU\000"
.LC64:
	.ascii	"SET MODEM MODE\000"
.LC65:
	.ascii	"IP Address\000"
.LC66:
	.ascii	"IP Radio Setup\000"
.LC67:
	.ascii	"0 For Off\000"
.LC68:
	.ascii	"1 For Ethernet Mode\000"
.LC69:
	.ascii	"SET BAUD RATE\000"
.LC70:
	.ascii	"3 for Both\000"
.LC71:
	.ascii	"Enter 0 for None,1 For RTS,2 for DTR\000"
.LC72:
	.ascii	"Enter 0 for None\000"
.LC73:
	.ascii	"RADIO PARAMETERS\000"
.LC74:
	.ascii	"Enter Max Packet (0-9)\000"
.LC75:
	.ascii	"Enter Max Packet\000"
.LC76:
	.ascii	"Enter Min Packet (0-9)\000"
.LC77:
	.ascii	"Enter Min Packet\000"
.LC78:
	.ascii	"Enter New Xmit Rate (0-F)\000"
.LC79:
	.ascii	"Enter New Xmit Rate\000"
.LC80:
	.ascii	"Enter New RF Data Rate (0-5)\000"
.LC81:
	.ascii	"Enter New RF Data \000"
.LC82:
	.ascii	"Enter New XmitPower (0-10)\000"
.LC83:
	.ascii	"Enter New XmitPower\000"
.LC84:
	.ascii	"Enter 1 to allow Master RTS to Slave CTS connection"
	.ascii	", 0 otherwise\000"
.LC85:
	.ascii	"Slave CTS connection\000"
.LC86:
	.ascii	"Enter Number Retries before Timeout\000"
.LC87:
	.ascii	"Enter Number Retries\000"
.LC88:
	.ascii	"Enter LowPower Option or 0 to disable (0-31)\000"
.LC89:
	.ascii	"Enter LowPower Option\000"
.LC90:
	.ascii	"Enter New Freq\000"
.LC91:
	.ascii	"Number of Hopping Channels\000"
.LC92:
	.ascii	"Hop Table Size\000"
.LC93:
	.ascii	"Enter Frequency Channel to use\000"
.LC94:
	.ascii	"Enter Frequency Channel\000"
.LC95:
	.ascii	"Num Channels\000"
.LC96:
	.ascii	"Channel Number (0-15)\000"
.LC97:
	.ascii	"Channel Number\000"
.LC98:
	.ascii	"Xmit Chan (00000-05600)\000"
.LC99:
	.ascii	"Xmit Chan\000"
.LC100:
	.ascii	"Rcv Chan (00000-05600)\000"
.LC101:
	.ascii	"Rcv Chan\000"
.LC102:
	.ascii	"  1 \000"
.LC103:
	.ascii	"Ch  Xmit   Rcv\000"
.LC104:
	.ascii	"MULTIPOINT PARAMETERS\000"
.LC105:
	.ascii	"Enter Number of Parallel Repeaters in Network(0-9)\000"
.LC106:
	.ascii	"Enter Number of Para\000"
.LC107:
	.ascii	"Enter Number Times Master Repeats Packets(0-9)\000"
.LC108:
	.ascii	"Enter Number Times M\000"
.LC109:
	.ascii	"Enter Number Times Slave Tries Before Backing Off ("
	.ascii	"0-9)\000"
.LC110:
	.ascii	"Enter Number Times S\000"
.LC111:
	.ascii	"Enter Slave Backing Off Retry Odds (0-9)\000"
.LC112:
	.ascii	"Enter Slave Backing\000"
.LC113:
	.ascii	"Enter Network ID Number (0-4095)\000"
.LC114:
	.ascii	"Enter Network ID\000"
.LC115:
	.ascii	"Enter 1 to enable Slave/Repeater or 0 for Normal\000"
.LC116:
	.ascii	"Enter 1 to enable\000"
.LC117:
	.ascii	"Enter 1 to 129 Enable Diagnostics, 0 To Disable\000"
.LC118:
	.ascii	"Enter 1 to 129\000"
.LC119:
	.ascii	"Enter Rcv SubNetID (0-F)\000"
.LC120:
	.ascii	"Enter Rcv SubNetID\000"
.LC121:
	.ascii	"Enter Xmit SubNetID (0-F)\000"
.LC122:
	.ascii	"Enter Xmit SubNetID\000"
	.section	.data.LR_read_list,"aw",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	LR_read_list, %object
	.size	LR_read_list, 208
LR_read_list:
	.word	.LC61
	.word	0
	.word	5000
	.word	.LC62
	.word	.LC63
	.word	LR_analyze_main_menu
	.word	48
	.word	.LC62
	.word	.LC64
	.word	LR_analyze_set_modem_mode
	.word	27
	.word	.LC62
	.word	.LC63
	.word	0
	.word	49
	.word	.LC62
	.word	.LC69
	.word	LR_analyze_set_baud_rate
	.word	27
	.word	.LC62
	.word	.LC63
	.word	0
	.word	51
	.word	.LC62
	.word	.LC73
	.word	LR_analyze_radio_parameters
	.word	48
	.word	.LC90
	.word	.LC90
	.word	0
	.word	70
	.word	.LC102
	.word	.LC103
	.word	LR_analyze_frequency
	.word	27
	.word	.LC62
	.word	.LC73
	.word	0
	.word	27
	.word	.LC62
	.word	.LC63
	.word	0
	.word	53
	.word	.LC62
	.word	.LC104
	.word	LR_analyze_multipoint_parameters
	.word	27
	.word	.LC62
	.word	.LC63
	.word	LR_final_radio_analysis
	.word	99999
	.word	.LC49
	.section	.bss.LR_PROGRAMMING_querying_device,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	LR_PROGRAMMING_querying_device, %object
	.size	LR_PROGRAMMING_querying_device, 4
LR_PROGRAMMING_querying_device:
	.space	4
	.section	.bss.lrs_struct,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	lrs_struct, %object
	.size	lrs_struct, 64
lrs_struct:
	.space	64
	.section	.bss.state_struct,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	state_struct, %object
	.size	state_struct, 56
state_struct:
	.space	56
	.section	.data.LR_group_list,"aw",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	LR_group_list, %object
	.size	LR_group_list, 72
LR_group_list:
	.ascii	"1\000"
	.space	1
	.ascii	"8\000"
	.ascii	"4\000"
	.ascii	"0320\000"
	.ascii	"2\000"
	.space	1
	.ascii	"8\000"
	.ascii	"5\000"
	.ascii	"0340\000"
	.ascii	"3\000"
	.space	1
	.ascii	"8\000"
	.ascii	"6\000"
	.ascii	"0420\000"
	.ascii	"4\000"
	.space	1
	.ascii	"8\000"
	.ascii	"7\000"
	.ascii	"0440\000"
	.ascii	"5\000"
	.space	1
	.ascii	"8\000"
	.ascii	"8\000"
	.ascii	"0520\000"
	.ascii	"6\000"
	.space	1
	.ascii	"8\000"
	.ascii	"9\000"
	.ascii	"0540\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI0-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI1-.LFB8
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI2-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI6-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI7-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x54
	.uleb128 0x7
	.byte	0x5
	.uleb128 0x52
	.uleb128 0x9
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI10-.LFB15
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI12-.LFB12
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI13-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI14-.LFB30
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI15-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI17-.LFB23
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI18-.LFB24
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI19-.LFB25
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI20-.LFB26
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI21-.LFB27
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI22-.LFB28
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI23-.LFB29
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE38:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_LR_FREEWAVE.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x20f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF30
	.byte	0x1
	.4byte	.LASF31
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x364
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x34f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x30b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x3c7
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x3ae
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x39a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x386
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x2be
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x14a
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x126
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x1
	.byte	0xb4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF11
	.byte	0x1
	.byte	0x98
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	.LASF12
	.byte	0x1
	.byte	0x92
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x1
	.byte	0x8c
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST5
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x1
	.byte	0x79
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST6
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x1
	.byte	0xa4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST7
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x158
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x330
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST8
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x2e0
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST9
	.uleb128 0x3
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x2f3
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST10
	.uleb128 0x5
	.4byte	0x3c
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST11
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.byte	0x58
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST12
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x4a4
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST13
	.uleb128 0x2
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x104
	.byte	0x1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x4b6
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST14
	.uleb128 0x2
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x423
	.byte	0x1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x4e0
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST15
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x4f5
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST16
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x51d
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST17
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x584
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST18
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x5ac
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST19
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB9
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB8
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB6
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB1
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB5
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI9
	.4byte	.LFE5
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB15
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI11
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB12
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB13
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB30
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB0
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI16
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB23
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB24
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB25
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB26
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB27
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB28
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB29
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xb4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"setup_for_termination_string_hunt\000"
.LASF14:
	.ascii	"LR_analyze_main_menu\000"
.LASF11:
	.ascii	"LR_analyze_radio_parameters\000"
.LASF30:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF8:
	.ascii	"LR_final_radio_verification\000"
.LASF27:
	.ascii	"LR_FREEWAVE_connection_processing\000"
.LASF24:
	.ascii	"LR_state_machine\000"
.LASF17:
	.ascii	"LR_get_and_process_command\000"
.LASF13:
	.ascii	"LR_analyze_set_modem_mode\000"
.LASF9:
	.ascii	"LR_final_radio_analysis\000"
.LASF22:
	.ascii	"LR_set_network_group_values\000"
.LASF6:
	.ascii	"LR_set_state_struct_for_new_device_exchange\000"
.LASF31:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_LR_FREEWAVE.c\000"
.LASF16:
	.ascii	"get_command_text\000"
.LASF3:
	.ascii	"process_list\000"
.LASF4:
	.ascii	"LR_verify_state_struct\000"
.LASF28:
	.ascii	"LR_FREEWAVE_extract_changes_from_guivars\000"
.LASF15:
	.ascii	"LR_analyze_frequency\000"
.LASF5:
	.ascii	"LR_initialize_state_struct\000"
.LASF29:
	.ascii	"LR_FREEWAVE_copy_settings_into_guivars\000"
.LASF23:
	.ascii	"LR_FREEWAVE_initialize_device_exchange\000"
.LASF0:
	.ascii	"LR_set_write_operation\000"
.LASF21:
	.ascii	"LR_FREEWAVE_power_control\000"
.LASF25:
	.ascii	"LR_FREEWAVE_exchange_processing\000"
.LASF18:
	.ascii	"send_no_response_esc_to_radio\000"
.LASF7:
	.ascii	"LR_FREEWAVE_set_radio_to_programming_mode\000"
.LASF26:
	.ascii	"LR_FREEWAVE_initialize_the_connection_process\000"
.LASF1:
	.ascii	"LR_set_read_operation\000"
.LASF20:
	.ascii	"LR_FREEWAVE_post_device_query_to_queue\000"
.LASF10:
	.ascii	"LR_analyze_multipoint_parameters\000"
.LASF12:
	.ascii	"LR_analyze_set_baud_rate\000"
.LASF19:
	.ascii	"exit_radio_programming_mode\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
