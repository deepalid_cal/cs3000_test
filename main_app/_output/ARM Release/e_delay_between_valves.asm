	.file	"e_delay_between_valves.c"
	.text
.Ltext0:
	.section	.text.FDTO_DELAY_BETWEEN_VALVES_show_in_use_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_DELAY_BETWEEN_VALVES_show_in_use_dropdown, %function
FDTO_DELAY_BETWEEN_VALVES_show_in_use_dropdown:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	ldr	r0, .L2+4
	ldr	r2, [r3, #0]
	mov	r1, #19
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L3:
	.align	2
.L2:
	.word	GuiVar_DelayBetweenValvesInUse
	.word	274
.LFE0:
	.size	FDTO_DELAY_BETWEEN_VALVES_show_in_use_dropdown, .-FDTO_DELAY_BETWEEN_VALVES_show_in_use_dropdown
	.section	.text.FDTO_DELAY_BETWEEN_VALVES_close_in_use_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_DELAY_BETWEEN_VALVES_close_in_use_dropdown, %function
FDTO_DELAY_BETWEEN_VALVES_close_in_use_dropdown:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, #0
	str	lr, [sp, #-4]!
.LCFI0:
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L5
	str	r0, [r3, #0]
	ldr	lr, [sp], #4
	b	FDTO_COMBOBOX_hide
.L6:
	.align	2
.L5:
	.word	GuiVar_DelayBetweenValvesInUse
.LFE1:
	.size	FDTO_DELAY_BETWEEN_VALVES_close_in_use_dropdown, .-FDTO_DELAY_BETWEEN_VALVES_close_in_use_dropdown
	.section	.text.FDTO_DELAY_BETWEEN_VALVES_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_DELAY_BETWEEN_VALVES_draw_screen
	.type	FDTO_DELAY_BETWEEN_VALVES_draw_screen, %function
FDTO_DELAY_BETWEEN_VALVES_draw_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L10
	str	lr, [sp, #-4]!
.LCFI1:
	ldrnesh	r1, [r3, #0]
	bne	.L9
	bl	DELAY_BETWEEN_VALVES_copy_group_into_guivars
	mov	r1, #0
.L9:
	mov	r0, #16
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L11:
	.align	2
.L10:
	.word	GuiLib_ActiveCursorFieldNo
.LFE2:
	.size	FDTO_DELAY_BETWEEN_VALVES_draw_screen, .-FDTO_DELAY_BETWEEN_VALVES_draw_screen
	.section	.text.DELAY_BETWEEN_VALVES_process_screen,"ax",%progbits
	.align	2
	.global	DELAY_BETWEEN_VALVES_process_screen
	.type	DELAY_BETWEEN_VALVES_process_screen, %function
DELAY_BETWEEN_VALVES_process_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L49
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #44
.LCFI3:
	cmp	r3, #740
	mov	r4, r0
	mov	r5, r1
	bne	.L45
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L15
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L49+4
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	mov	r0, #0
	bl	Redraw_Screen
	b	.L12
.L15:
	bl	COMBO_BOX_key_press
	b	.L12
.L45:
	cmp	r0, #4
	beq	.L19
	bhi	.L25
	cmp	r0, #1
	beq	.L19
	bcc	.L18
	cmp	r0, #2
	beq	.L20
	cmp	r0, #3
	bne	.L17
	b	.L18
.L25:
	cmp	r0, #67
	beq	.L23
	bhi	.L26
	cmp	r0, #16
	beq	.L21
	cmp	r0, #20
	bne	.L17
	b	.L48
.L26:
	cmp	r0, #80
	beq	.L24
	cmp	r0, #84
	bne	.L17
	b	.L24
.L20:
	ldr	r3, .L49+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L44
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L49+12
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L12
.L24:
	ldr	r3, .L49+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #10
	ldrls	pc, [pc, r3, asl #2]
	b	.L29
.L41:
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
.L30:
	ldr	r0, .L49+16
	bl	process_bool
	mov	r0, #0
	bl	Redraw_Screen
	b	.L42
.L31:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L49+20
	b	.L47
.L32:
	ldr	r1, .L49+24
	mov	r3, #1
	mov	r2, #0
	mov	r0, r4
	str	r3, [sp, #0]
	str	r2, [sp, #4]
.L47:
	mov	r3, #120
	bl	process_uns32
	b	.L42
.L33:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L49+28
	b	.L47
.L34:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L49+32
	b	.L47
.L35:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L49+36
	b	.L47
.L36:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L49+40
	b	.L47
.L37:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L49+44
	b	.L47
.L38:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L49+48
	b	.L47
.L39:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L49+52
	b	.L47
.L40:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L49+56
	b	.L47
.L29:
	bl	bad_key_beep
.L42:
	bl	Refresh_Screen
	b	.L12
.L21:
	ldr	r3, .L49+8
	ldrh	r3, [r3, #0]
	sub	r3, r3, #2
	mov	r3, r3, asl #16
	cmp	r3, #524288
	bhi	.L44
.L19:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L12
.L48:
	ldr	r3, .L49+8
	ldrh	r3, [r3, #0]
	sub	r3, r3, #1
	mov	r3, r3, asl #16
	cmp	r3, #524288
	bls	.L18
.L44:
	bl	bad_key_beep
	b	.L12
.L18:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L12
.L23:
	ldr	r3, .L49+60
	mov	r2, #2
	str	r2, [r3, #0]
	bl	DELAY_BETWEEN_VALVES_extract_and_store_changes_from_GuiVars
.L17:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L12:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L50:
	.align	2
.L49:
	.word	GuiLib_CurStructureNdx
	.word	FDTO_DELAY_BETWEEN_VALVES_close_in_use_dropdown
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_DELAY_BETWEEN_VALVES_show_in_use_dropdown
	.word	GuiVar_DelayBetweenValvesInUse
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	DELAY_BETWEEN_VALVES_process_screen, .-DELAY_BETWEEN_VALVES_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_delay_between_valves.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x30
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x36
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x3e
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x55
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_delay_between_valves.c\000"
.LASF0:
	.ascii	"FDTO_DELAY_BETWEEN_VALVES_show_in_use_dropdown\000"
.LASF3:
	.ascii	"DELAY_BETWEEN_VALVES_process_screen\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"FDTO_DELAY_BETWEEN_VALVES_close_in_use_dropdown\000"
.LASF2:
	.ascii	"FDTO_DELAY_BETWEEN_VALVES_draw_screen\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
