	.file	"timers.c"
	.text
.Ltext0:
	.section	.text.prvCheckForValidListAndQueue,"ax",%progbits
	.align	2
	.type	prvCheckForValidListAndQueue, %function
prvCheckForValidListAndQueue:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI0:
	bl	vPortEnterCritical
	ldr	r4, .L3
	ldr	r5, [r4, #0]
	cmp	r5, #0
	bne	.L2
	ldr	r7, .L3+4
	ldr	r6, .L3+8
	mov	r0, r7
	bl	vListInitialise
	mov	r0, r6
	bl	vListInitialise
	ldr	r3, .L3+12
	mov	r0, #20
	str	r7, [r3, #0]
	ldr	r3, .L3+16
	mov	r1, #12
	str	r6, [r3, #0]
	mov	r2, r5
	bl	xQueueGenericCreate
	str	r0, [r4, #0]
.L2:
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	vPortExitCritical
.L4:
	.align	2
.L3:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LANCHOR4
.LFE12:
	.size	prvCheckForValidListAndQueue, .-prvCheckForValidListAndQueue
	.section	.text.prvInsertTimerInActiveList,"ax",%progbits
	.align	2
	.type	prvInsertTimerInActiveList, %function
prvInsertTimerInActiveList:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	ip, r0
	cmp	r1, r2
	stmfd	sp!, {r4, lr}
.LCFI1:
	str	r0, [ip, #16]
	str	r1, [r0, #4]
	bhi	.L6
	ldr	r1, [r0, #24]
	rsb	r2, r3, r2
	cmp	r2, r1
	movcs	r0, #1
	ldmcsfd	sp!, {r4, pc}
.LBB12:
	ldr	r3, .L10
	add	r1, ip, #4
	ldr	r0, [r3, #0]
	bl	vListInsert
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L6:
.LBE12:
	cmp	r1, r3
	movcc	r4, #0
	movcs	r4, #1
	cmp	r2, r3
	movcs	r4, #0
	cmp	r4, #0
	bne	.L9
	ldr	r3, .L10+4
	add	r1, ip, #4
	ldr	r0, [r3, #0]
	bl	vListInsert
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L9:
	mov	r0, #1
	ldmfd	sp!, {r4, pc}
.L11:
	.align	2
.L10:
	.word	.LANCHOR4
	.word	.LANCHOR3
.LFE9:
	.size	prvInsertTimerInActiveList, .-prvInsertTimerInActiveList
	.section	.text.xTimerCreateTimerTask,"ax",%progbits
	.align	2
	.global	xTimerCreateTimerTask
	.type	xTimerCreateTimerTask, %function
xTimerCreateTimerTask:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI2:
	bl	prvCheckForValidListAndQueue
	ldr	r3, .L15
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L13
	mov	r3, #13
	str	r3, [sp, #0]
	ldr	r3, .L15+4
	ldr	r0, .L15+8
	str	r3, [sp, #4]
	ldr	r1, .L15+12
	mov	r3, #0
	mov	r2, #2048
	str	r3, [sp, #8]
	str	r3, [sp, #12]
	bl	xTaskGenericCreate
	cmp	r0, #0
	bne	.L14
.L13:
	ldr	r0, .L15+16
	ldr	r1, .L15+20
	mov	r2, #213
	bl	__assert
	mov	r0, #0
.L14:
	add	sp, sp, #16
	ldmfd	sp!, {pc}
.L16:
	.align	2
.L15:
	.word	.LANCHOR0
	.word	.LANCHOR5
	.word	prvTimerTask
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE0:
	.size	xTimerCreateTimerTask, .-xTimerCreateTimerTask
	.section	.text.xTimerCreate,"ax",%progbits
	.align	2
	.global	xTimerCreate
	.type	xTimerCreate, %function
xTimerCreate:
.LFB1:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI3:
	subs	r5, r1, #0
	mov	r4, r0
	mov	r6, r2
	mov	r7, r3
	bne	.L18
	ldr	r0, .L20
	ldr	r1, .L20+4
	mov	r2, #226
	bl	__assert
	mov	r8, r5
	b	.L19
.L18:
	mov	r0, #40
	bl	pvPortMalloc
	subs	r8, r0, #0
	beq	.L19
	bl	prvCheckForValidListAndQueue
	ldr	r3, [sp, #24]
	str	r4, [r8, #0]
	str	r5, [r8, #24]
	str	r6, [r8, #28]
	str	r7, [r8, #32]
	str	r3, [r8, #36]
	add	r0, r8, #4
	bl	vListInitialiseItem
.L19:
	mov	r0, r8
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L21:
	.align	2
.L20:
	.word	.LC3
	.word	.LC2
.LFE1:
	.size	xTimerCreate, .-xTimerCreate
	.section	.text.xTimerGenericCommand,"ax",%progbits
	.align	2
	.global	xTimerGenericCommand
	.type	xTimerGenericCommand, %function
xTimerGenericCommand:
.LFB2:
	@ args = 4, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI4:
	ldr	r5, .L28
	mov	r4, r3
	ldr	r3, [r5, #0]
	cmp	r0, #0
	cmpne	r3, #0
	moveq	r6, #0
	movne	r6, #1
	beq	.L23
	cmp	r4, #0
	stmia	sp, {r1, r2}
	str	r0, [sp, #8]
	bne	.L24
	bl	xTaskGetSchedulerState
	mov	r1, sp
	cmp	r0, #1
	movne	r2, r4
	ldr	r0, [r5, #0]
	ldreq	r2, [sp, #28]
	moveq	r3, r4
	movne	r3, r2
	bl	xQueueGenericSend
	b	.L26
.L24:
	mov	r0, r3
	mov	r1, sp
	mov	r2, r4
	mov	r3, #0
	bl	xQueueGenericSendFromISR
	b	.L26
.L23:
.LBB13:
	ldr	r0, .L28+4
	bl	Alert_Message
	mov	r0, r6
.L26:
.LBE13:
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, pc}
.L29:
	.align	2
.L28:
	.word	.LANCHOR0
	.word	.LC4
.LFE2:
	.size	xTimerGenericCommand, .-xTimerGenericCommand
	.section	.text.prvSwitchTimerLists.isra.1,"ax",%progbits
	.align	2
	.type	prvSwitchTimerLists.isra.1, %function
prvSwitchTimerLists.isra.1:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI5:
	ldr	r4, .L39
	mov	r5, #0
	b	.L38
.L35:
	ldr	r2, [r3, #12]
	ldr	r6, [r2, #0]
	ldr	r3, [r3, #12]
	ldr	r7, [r3, #12]
	add	r8, r7, #4
	mov	r0, r8
	bl	vListRemove
	ldr	r3, [r7, #36]
	mov	r0, r7
	blx	r3
	ldr	r3, [r7, #28]
	cmp	r3, #1
	bne	.L38
	ldr	r3, [r7, #24]
	add	r3, r6, r3
	cmp	r3, r6
	bls	.L33
	str	r3, [r7, #4]
	str	r7, [r7, #16]
	ldr	r0, [r4, #0]
	mov	r1, r8
	bl	vListInsert
	b	.L38
.L33:
	mov	r1, #0
	mov	r0, r7
	mov	r2, r6
	mov	r3, r1
	str	r5, [sp, #0]
	bl	xTimerGenericCommand
	cmp	r0, #0
	bne	.L38
	ldr	r0, .L39+4
	ldr	r1, .L39+8
	ldr	r2, .L39+12
	bl	__assert
.L38:
	ldr	r3, [r4, #0]
	ldr	r2, [r3, #0]
	cmp	r2, #0
	bne	.L35
	ldr	r2, .L39+16
	ldr	r1, .L39
	ldr	r0, [r2, #0]
	str	r3, [r2, #0]
	str	r0, [r1, #0]
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, pc}
.L40:
	.align	2
.L39:
	.word	.LANCHOR3
	.word	.LC5
	.word	.LC2
	.word	643
	.word	.LANCHOR4
.LFE16:
	.size	prvSwitchTimerLists.isra.1, .-prvSwitchTimerLists.isra.1
	.section	.text.prvTimerTask,"ax",%progbits
	.align	2
	.type	prvTimerTask, %function
prvTimerTask:
.LFB5:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, sl, lr}
.LCFI6:
.LBB22:
.LBB23:
	ldr	r7, .L65
	mov	r8, r7
.L58:
.LBE23:
.LBE22:
.LBB27:
	ldr	r3, .L65+4
	ldr	r3, [r3, #0]
	ldr	r4, [r3, #0]
	rsbs	r4, r4, #1
	movcc	r4, #0
	cmp	r4, #0
	ldreq	r3, [r3, #12]
	movne	r5, #0
	ldreq	r5, [r3, #0]
.LBE27:
.LBB28:
	bl	vTaskSuspendAll
.LBB24:
	bl	xTaskGetTickCount
	ldr	r3, [r7, #0]
	cmp	r0, r3
	mov	sl, r0
	movcs	r6, #0
	bcs	.L43
	bl	prvSwitchTimerLists.isra.1
	mov	r6, #1
.L43:
.LBE24:
	cmp	r6, #0
.LBB25:
	str	sl, [r8, #0]
.LBE25:
	bne	.L44
	eor	r4, r4, #1
	cmp	r5, sl
	movhi	r4, #0
	andls	r4, r4, #1
	cmp	r4, #0
	beq	.L45
	bl	xTaskResumeAll
.LBB26:
	ldr	r3, .L65+4
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #12]
	ldr	r4, [r3, #12]
	add	r0, r4, #4
	bl	vListRemove
	ldr	r3, [r4, #28]
	cmp	r3, #1
	bne	.L46
	ldr	r1, [r4, #24]
	mov	r0, r4
	add	r1, r5, r1
	mov	r2, sl
	mov	r3, r5
	bl	prvInsertTimerInActiveList
	cmp	r0, #1
	bne	.L46
	mov	r0, r4
	mov	r1, r6
	mov	r2, r5
	mov	r3, r6
	str	r6, [sp, #0]
	bl	xTimerGenericCommand
	cmp	r0, #0
	bne	.L46
	ldr	r0, .L65+8
	ldr	r1, .L65+12
	ldr	r2, .L65+16
	bl	__assert
.L46:
	ldr	r3, [r4, #36]
	mov	r0, r4
	blx	r3
	b	.L62
.L45:
.LBE26:
	ldr	r3, .L65+20
	rsb	r1, sl, r5
	ldr	r0, [r3, #0]
	bl	vQueueWaitForMessageRestricted
	bl	xTaskResumeAll
	cmp	r0, #0
	bne	.L62
@ 425 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/timers.c" 1
	SWI 0
@ 0 "" 2
	b	.L62
.L44:
	bl	xTaskResumeAll
	b	.L62
.L57:
.LBE28:
.LBB29:
	ldr	r4, [sp, #12]
	ldr	r3, [r4, #20]
	cmp	r3, #0
	beq	.L49
	add	r0, r4, #4
	bl	vListRemove
.L49:
.LBB30:
	bl	xTaskGetTickCount
	ldr	r3, [r7, #0]
	cmp	r0, r3
	mov	sl, r0
	bcs	.L50
	bl	prvSwitchTimerLists.isra.1
.L50:
.LBE30:
	ldr	r5, [sp, #4]
.LBB31:
	str	sl, [r8, #0]
.LBE31:
	cmp	r5, #2
	beq	.L53
	cmp	r5, #3
	beq	.L54
	cmp	r5, #0
	bne	.L64
	ldr	r3, [sp, #8]
	ldr	r1, [r4, #24]
	mov	r0, r4
	add	r1, r3, r1
	mov	r2, sl
	bl	prvInsertTimerInActiveList
	cmp	r0, #1
	bne	.L64
	ldr	r3, [r4, #36]
	mov	r0, r4
	blx	r3
	ldr	r3, [r4, #28]
	cmp	r3, #1
	bne	.L64
	ldr	r2, [r4, #24]
	ldr	r3, [sp, #8]
	mov	r0, r4
	add	r2, r2, r3
	mov	r1, r5
	mov	r3, r5
	str	r5, [sp, #0]
	bl	xTimerGenericCommand
	cmp	r0, #0
	bne	.L64
	ldr	r0, .L65+8
	ldr	r1, .L65+12
	ldr	r2, .L65+24
	bl	__assert
	b	.L64
.L53:
	ldr	r3, [sp, #8]
	cmp	r3, #0
	str	r3, [r4, #24]
	bne	.L56
	ldr	r0, .L65+28
	ldr	r1, .L65+12
	mov	r2, #580
	bl	__assert
.L56:
	ldr	r1, [r4, #24]
	mov	r0, r4
	add	r1, sl, r1
	mov	r2, sl
	mov	r3, sl
	bl	prvInsertTimerInActiveList
	b	.L64
.L54:
	mov	r0, r4
	bl	vPortFree
	b	.L64
.L62:
	ldr	r6, .L65+20
.L64:
	mov	r2, #0
	ldr	r0, [r6, #0]
	add	r1, sp, #4
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	bne	.L57
	b	.L58
.L66:
	.align	2
.L65:
	.word	.LANCHOR6
	.word	.LANCHOR3
	.word	.LC5
	.word	.LC2
	.word	354
	.word	.LANCHOR0
	.word	567
	.word	.LC6
.LBE29:
.LFE5:
	.size	prvTimerTask, .-prvTimerTask
	.section	.text.xTimerGetTimerDaemonTaskHandle,"ax",%progbits
	.align	2
	.global	xTimerGetTimerDaemonTaskHandle
	.type	xTimerGetTimerDaemonTaskHandle, %function
xTimerGetTimerDaemonTaskHandle:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI7:
	ldr	r4, .L69
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L68
	ldr	r0, .L69+4
	ldr	r1, .L69+8
	ldr	r2, .L69+12
	bl	__assert
.L68:
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, pc}
.L70:
	.align	2
.L69:
	.word	.LANCHOR5
	.word	.LC7
	.word	.LC2
	.word	321
.LFE3:
	.size	xTimerGetTimerDaemonTaskHandle, .-xTimerGetTimerDaemonTaskHandle
	.section	.text.xTimerIsTimerActive,"ax",%progbits
	.align	2
	.global	xTimerIsTimerActive
	.type	xTimerIsTimerActive, %function
xTimerIsTimerActive:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI8:
	mov	r4, r0
	bl	vPortEnterCritical
	ldr	r4, [r4, #20]
	adds	r4, r4, #0
	movne	r4, #1
	bl	vPortExitCritical
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.LFE13:
	.size	xTimerIsTimerActive, .-xTimerIsTimerActive
	.section	.text.pvTimerGetTimerID,"ax",%progbits
	.align	2
	.global	pvTimerGetTimerID
	.type	pvTimerGetTimerID, %function
pvTimerGetTimerID:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, [r0, #32]
	bx	lr
.LFE14:
	.size	pvTimerGetTimerID, .-pvTimerGetTimerID
	.section	.bss.pxCurrentTimerList,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	pxCurrentTimerList, %object
	.size	pxCurrentTimerList, 4
pxCurrentTimerList:
	.space	4
	.section	.bss.xActiveTimerList1,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	xActiveTimerList1, %object
	.size	xActiveTimerList1, 20
xActiveTimerList1:
	.space	20
	.section	.bss.xActiveTimerList2,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	xActiveTimerList2, %object
	.size	xActiveTimerList2, 20
xActiveTimerList2:
	.space	20
	.section	.bss.xLastTime.1193,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	xLastTime.1193, %object
	.size	xLastTime.1193, 4
xLastTime.1193:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Tmr Svc\000"
.LC1:
	.ascii	"xReturn\000"
.LC2:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/timers.c\000"
.LC3:
	.ascii	"( xTimerPeriodInTicks > 0 )\000"
.LC4:
	.ascii	"--> NULL timer call <--\000"
.LC5:
	.ascii	"xResult\000"
.LC6:
	.ascii	"( pxTimer->xTimerPeriodInTicks > 0 )\000"
.LC7:
	.ascii	"( xTimerTaskHandle != 0 )\000"
	.section	.bss.xTimerQueue,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	xTimerQueue, %object
	.size	xTimerQueue, 4
xTimerQueue:
	.space	4
	.section	.bss.pxOverflowTimerList,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	pxOverflowTimerList, %object
	.size	pxOverflowTimerList, 4
pxOverflowTimerList:
	.space	4
	.section	.bss.xTimerTaskHandle,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	xTimerTaskHandle, %object
	.size	xTimerTaskHandle, 4
xTimerTaskHandle:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI0-.LFB12
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI1-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI2-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI5-.LFB16
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI6-.LFB5
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x83
	.uleb128 0x8
	.byte	0x82
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI7-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI8-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.align	2
.LEFDE18:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/timers.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x122
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF15
	.byte	0x1
	.4byte	.LASF16
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1e6
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x256
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x1cf
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x148
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1b5
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x28f
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xbb
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xda
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x101
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	0x2a
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST5
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x184
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x20f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x16c
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x13d
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x2a3
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST8
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x2b6
	.4byte	.LFB14
	.4byte	.LFE14
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB12
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB9
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB0
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB16
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB3
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB13
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x64
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"prvSampleTimeNow\000"
.LASF9:
	.ascii	"prvProcessReceivedCommands\000"
.LASF16:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/timers.c\000"
.LASF12:
	.ascii	"xTimerGetTimerDaemonTaskHandle\000"
.LASF13:
	.ascii	"xTimerIsTimerActive\000"
.LASF0:
	.ascii	"prvInsertTimerInActiveList\000"
.LASF11:
	.ascii	"prvTimerTask\000"
.LASF10:
	.ascii	"prvCheckForValidListAndQueue\000"
.LASF14:
	.ascii	"pvTimerGetTimerID\000"
.LASF15:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF8:
	.ascii	"prvProcessTimerOrBlockTask\000"
.LASF4:
	.ascii	"prvGetNextExpireTime\000"
.LASF1:
	.ascii	"prvSwitchTimerLists\000"
.LASF5:
	.ascii	"xTimerCreateTimerTask\000"
.LASF3:
	.ascii	"prvProcessExpiredTimer\000"
.LASF6:
	.ascii	"xTimerCreate\000"
.LASF7:
	.ascii	"xTimerGenericCommand\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
