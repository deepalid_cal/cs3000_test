	.file	"station_groups.c"
	.text
.Ltext0:
	.section	.text.nm_STATION_GROUP_set_plant_type,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_plant_type, %function
nm_STATION_GROUP_set_plant_type:
.LFB0:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	sub	sp, sp, #44
.LCFI1:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L2
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #1
	str	r3, [sp, #36]
	ldr	r3, .L2+4
	mov	ip, #0
	mov	r1, #13
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #72
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L3:
	.align	2
.L2:
	.word	49241
	.word	.LC0
.LFE0:
	.size	nm_STATION_GROUP_set_plant_type, .-nm_STATION_GROUP_set_plant_type
	.section	.text.nm_STATION_GROUP_set_head_type,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_head_type, %function
nm_STATION_GROUP_set_head_type:
.LFB1:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	sub	sp, sp, #44
.LCFI3:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L5
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #2
	str	r3, [sp, #36]
	ldr	r3, .L5+4
	mov	ip, #0
	mov	r1, #12
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #76
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L6:
	.align	2
.L5:
	.word	49235
	.word	.LC1
.LFE1:
	.size	nm_STATION_GROUP_set_head_type, .-nm_STATION_GROUP_set_head_type
	.section	.text.nm_STATION_GROUP_set_precip_rate,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_precip_rate, %function
nm_STATION_GROUP_set_precip_rate:
.LFB2:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI4:
	sub	sp, sp, #44
.LCFI5:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	ldr	r1, .L8
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	ldr	r1, .L8+4
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #3
	stmib	sp, {r1, r2}
	ldr	r2, .L8+8
	str	r3, [sp, #36]
	ldr	r3, .L8+12
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #80
	mov	r2, lr
	mov	r3, #1000
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L9:
	.align	2
.L8:
	.word	999999
	.word	100000
	.word	49236
	.word	.LC2
.LFE2:
	.size	nm_STATION_GROUP_set_precip_rate, .-nm_STATION_GROUP_set_precip_rate
	.section	.text.nm_STATION_GROUP_set_soil_type,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_soil_type, %function
nm_STATION_GROUP_set_soil_type:
.LFB3:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI6:
	sub	sp, sp, #44
.LCFI7:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #6
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #312
	mov	r1, #3
	str	r3, [sp, #32]
	mov	r3, #4
	stmib	sp, {r1, r2}
	ldr	r2, .L11
	str	r3, [sp, #36]
	ldr	r3, .L11+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #84
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L12:
	.align	2
.L11:
	.word	49237
	.word	.LC3
.LFE3:
	.size	nm_STATION_GROUP_set_soil_type, .-nm_STATION_GROUP_set_soil_type
	.section	.text.nm_STATION_GROUP_set_slope_percentage,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_slope_percentage, %function
nm_STATION_GROUP_set_slope_percentage:
.LFB4:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI8:
	sub	sp, sp, #44
.LCFI9:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L14
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #5
	str	r3, [sp, #36]
	ldr	r3, .L14+4
	mov	ip, #0
	mov	r1, #3
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #88
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L15:
	.align	2
.L14:
	.word	49286
	.word	.LC4
.LFE4:
	.size	nm_STATION_GROUP_set_slope_percentage, .-nm_STATION_GROUP_set_slope_percentage
	.section	.text.nm_STATION_GROUP_set_usable_rain,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_usable_rain, %function
nm_STATION_GROUP_set_usable_rain:
.LFB6:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI10:
	sub	sp, sp, #44
.LCFI11:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #100
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #312
	mov	r1, #80
	str	r3, [sp, #32]
	mov	r3, #7
	stmib	sp, {r1, r2}
	ldr	r2, .L17
	str	r3, [sp, #36]
	ldr	r3, .L17+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #96
	mov	r2, lr
	mov	r3, #50
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L18:
	.align	2
.L17:
	.word	49277
	.word	.LC5
.LFE6:
	.size	nm_STATION_GROUP_set_usable_rain, .-nm_STATION_GROUP_set_usable_rain
	.section	.text.nm_STATION_GROUP_set_soil_intake_rate,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_soil_intake_rate, %function
nm_STATION_GROUP_set_soil_intake_rate:
.LFB40:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI12:
	sub	sp, sp, #44
.LCFI13:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #100
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #312
	mov	r1, #35
	str	r3, [sp, #32]
	mov	r3, #44
	stmib	sp, {r1, r2}
	ldr	r2, .L20
	str	r3, [sp, #36]
	ldr	r3, .L20+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #344
	mov	r2, lr
	mov	r3, #1
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L21:
	.align	2
.L20:
	.word	49238
	.word	.LC6
.LFE40:
	.size	nm_STATION_GROUP_set_soil_intake_rate, .-nm_STATION_GROUP_set_soil_intake_rate
	.section	.text.nm_STATION_GROUP_set_allowable_surface_accumulation,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_allowable_surface_accumulation, %function
nm_STATION_GROUP_set_allowable_surface_accumulation:
.LFB41:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI14:
	sub	sp, sp, #44
.LCFI15:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #100
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #312
	mov	r1, #25
	str	r3, [sp, #32]
	mov	r3, #45
	stmib	sp, {r1, r2}
	ldr	r2, .L23
	str	r3, [sp, #36]
	ldr	r3, .L23+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #348
	mov	r2, lr
	mov	r3, #1
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L24:
	.align	2
.L23:
	.word	49292
	.word	.LC7
.LFE41:
	.size	nm_STATION_GROUP_set_allowable_surface_accumulation, .-nm_STATION_GROUP_set_allowable_surface_accumulation
	.section	.text.nm_PRIORITY_set_priority,"ax",%progbits
	.align	2
	.type	nm_PRIORITY_set_priority, %function
nm_PRIORITY_set_priority:
.LFB8:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI16:
	sub	sp, sp, #44
.LCFI17:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L26
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #9
	str	r3, [sp, #36]
	ldr	r3, .L26+4
	mov	ip, #1
	mov	r1, #3
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #148
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L27:
	.align	2
.L26:
	.word	49155
	.word	.LC8
.LFE8:
	.size	nm_PRIORITY_set_priority, .-nm_PRIORITY_set_priority
	.section	.text.nm_PERCENT_ADJUST_set_percent,"ax",%progbits
	.align	2
	.type	nm_PERCENT_ADJUST_set_percent, %function
nm_PERCENT_ADJUST_set_percent:
.LFB9:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI18:
	sub	sp, sp, #44
.LCFI19:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #200
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #312
	mov	r1, #100
	str	r3, [sp, #32]
	mov	r3, #10
	stmib	sp, {r1, r2}
	ldr	r2, .L29
	str	r3, [sp, #36]
	ldr	r3, .L29+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #152
	mov	r2, lr
	mov	r3, #20
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L30:
	.align	2
.L29:
	.word	49164
	.word	.LC9
.LFE9:
	.size	nm_PERCENT_ADJUST_set_percent, .-nm_PERCENT_ADJUST_set_percent
	.section	.text.nm_SCHEDULE_set_mow_day,"ax",%progbits
	.align	2
	.type	nm_SCHEDULE_set_mow_day, %function
nm_SCHEDULE_set_mow_day:
.LFB15:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI20:
	sub	sp, sp, #44
.LCFI21:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #7
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	stmib	sp, {r1, r2}
	ldr	r2, .L32
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #20
	str	r3, [sp, #36]
	ldr	r3, .L32+4
	str	r1, [sp, #0]
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #216
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L33:
	.align	2
.L32:
	.word	49174
	.word	.LC10
.LFE15:
	.size	nm_SCHEDULE_set_mow_day, .-nm_SCHEDULE_set_mow_day
	.section	.text.nm_SCHEDULE_set_schedule_type,"ax",%progbits
	.align	2
	.type	nm_SCHEDULE_set_schedule_type, %function
nm_SCHEDULE_set_schedule_type:
.LFB16:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI22:
	sub	sp, sp, #44
.LCFI23:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L35
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #14
	str	r3, [sp, #36]
	ldr	r3, .L35+4
	mov	ip, #0
	mov	r1, #18
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #168
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L36:
	.align	2
.L35:
	.word	49175
	.word	.LC11
.LFE16:
	.size	nm_SCHEDULE_set_schedule_type, .-nm_SCHEDULE_set_schedule_type
	.section	.text.nm_ALERT_ACTIONS_set_high_flow_action,"ax",%progbits
	.align	2
	.type	nm_ALERT_ACTIONS_set_high_flow_action, %function
nm_ALERT_ACTIONS_set_high_flow_action:
.LFB26:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI24:
	sub	sp, sp, #44
.LCFI25:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #2
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #312
	mov	r1, #1
	str	r3, [sp, #32]
	mov	r3, #30
	stmib	sp, {r1, r2}
	ldr	r2, .L38
	str	r3, [sp, #36]
	ldr	r3, .L38+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #260
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L39:
	.align	2
.L38:
	.word	49156
	.word	.LC12
.LFE26:
	.size	nm_ALERT_ACTIONS_set_high_flow_action, .-nm_ALERT_ACTIONS_set_high_flow_action
	.section	.text.nm_ALERT_ACTIONS_set_low_flow_action,"ax",%progbits
	.align	2
	.type	nm_ALERT_ACTIONS_set_low_flow_action, %function
nm_ALERT_ACTIONS_set_low_flow_action:
.LFB27:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI26:
	sub	sp, sp, #44
.LCFI27:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #2
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #312
	mov	r1, #1
	str	r3, [sp, #32]
	mov	r3, #31
	stmib	sp, {r1, r2}
	ldr	r2, .L41
	str	r3, [sp, #36]
	ldr	r3, .L41+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #264
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L42:
	.align	2
.L41:
	.word	49157
	.word	.LC13
.LFE27:
	.size	nm_ALERT_ACTIONS_set_low_flow_action, .-nm_ALERT_ACTIONS_set_low_flow_action
	.section	.text.nm_LINE_FILL_TIME_set_line_fill_time,"ax",%progbits
	.align	2
	.type	nm_LINE_FILL_TIME_set_line_fill_time, %function
nm_LINE_FILL_TIME_set_line_fill_time:
.LFB31:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI28:
	sub	sp, sp, #44
.LCFI29:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	ldr	r1, .L44
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #312
	mov	r1, #120
	str	r3, [sp, #32]
	mov	r3, #35
	stmib	sp, {r1, r2}
	ldr	r2, .L44+4
	str	r3, [sp, #36]
	ldr	r3, .L44+8
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #284
	mov	r2, lr
	mov	r3, #15
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L45:
	.align	2
.L44:
	.word	1800
	.word	49159
	.word	.LC14
.LFE31:
	.size	nm_LINE_FILL_TIME_set_line_fill_time, .-nm_LINE_FILL_TIME_set_line_fill_time
	.section	.text.nm_DELAY_BETWEEN_VALVES_set_delay_time,"ax",%progbits
	.align	2
	.type	nm_DELAY_BETWEEN_VALVES_set_delay_time, %function
nm_DELAY_BETWEEN_VALVES_set_delay_time:
.LFB32:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI30:
	sub	sp, sp, #44
.LCFI31:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L47
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #36
	str	r3, [sp, #36]
	ldr	r3, .L47+4
	mov	ip, #0
	mov	r1, #120
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #288
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L48:
	.align	2
.L47:
	.word	49160
	.word	.LC15
.LFE32:
	.size	nm_DELAY_BETWEEN_VALVES_set_delay_time, .-nm_DELAY_BETWEEN_VALVES_set_delay_time
	.section	.text.nm_STATION_GROUP_set_budget_reduction_percentage_cap,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_budget_reduction_percentage_cap, %function
nm_STATION_GROUP_set_budget_reduction_percentage_cap:
.LFB42:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI32:
	sub	sp, sp, #44
.LCFI33:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	mov	r1, #100
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	add	r3, r0, #312
	mov	r1, #25
	str	r3, [sp, #32]
	mov	r3, #46
	stmib	sp, {r1, r2}
	ldr	r2, .L50
	str	r3, [sp, #36]
	ldr	r3, .L50+4
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #360
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L51:
	.align	2
.L50:
	.word	49397
	.word	.LC16
.LFE42:
	.size	nm_STATION_GROUP_set_budget_reduction_percentage_cap, .-nm_STATION_GROUP_set_budget_reduction_percentage_cap
	.section	.text.nm_PERCENT_ADJUST_set_end_date,"ax",%progbits
	.align	2
	.type	nm_PERCENT_ADJUST_set_end_date, %function
nm_PERCENT_ADJUST_set_end_date:
.LFB11:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI34:
	ldr	r6, .L53
	mov	r4, r0
	mov	r0, #1
	sub	sp, sp, #44
.LCFI35:
	mov	r9, r1
	mov	r5, r2
	mov	r1, r0
	mov	r2, r6
	mov	r7, r3
	bl	DMYToDate
	mov	r1, #12
	ldr	r2, .L53+4
	mov	sl, r0
	mov	r0, #31
	bl	DMYToDate
	mov	r2, r6
	mov	r8, r0
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	ldr	r2, .L53+8
	mov	r3, sl, asl #16
	str	r2, [sp, #12]
	ldr	r2, [sp, #76]
	mov	r8, r8, asl #16
	str	r2, [sp, #20]
	ldr	r2, [sp, #80]
	mov	r8, r8, lsr #16
	str	r2, [sp, #24]
	ldr	r2, [sp, #84]
	add	r1, r4, #160
	str	r2, [sp, #28]
	add	r2, r4, #312
	str	r2, [sp, #32]
	mov	r2, #12
	str	r2, [sp, #36]
	ldr	r2, .L53+12
	mov	r3, r3, lsr #16
	str	r2, [sp, #40]
	mov	r2, r9
	str	r8, [sp, #0]
	str	r7, [sp, #16]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	stmib	sp, {r0, r5}
	mov	r0, r4
	bl	SHARED_set_date_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L54:
	.align	2
.L53:
	.word	2011
	.word	2042
	.word	49166
	.word	.LC17
.LFE11:
	.size	nm_PERCENT_ADJUST_set_end_date, .-nm_PERCENT_ADJUST_set_end_date
	.section	.text.nm_SCHEDULE_set_enabled,"ax",%progbits
	.align	2
	.type	nm_SCHEDULE_set_enabled, %function
nm_SCHEDULE_set_enabled:
.LFB12:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI36:
	sub	sp, sp, #36
.LCFI37:
	str	r2, [sp, #0]
	ldr	r2, .L56
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #164
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #312
	str	r3, [sp, #24]
	mov	r3, #13
	str	r3, [sp, #28]
	ldr	r3, .L56+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_with_64_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L57:
	.align	2
.L56:
	.word	49171
	.word	.LC18
.LFE12:
	.size	nm_SCHEDULE_set_enabled, .-nm_SCHEDULE_set_enabled
	.section	.text.nm_SCHEDULE_set_irrigate_on_29th_or_31st,"ax",%progbits
	.align	2
	.type	nm_SCHEDULE_set_irrigate_on_29th_or_31st, %function
nm_SCHEDULE_set_irrigate_on_29th_or_31st:
.LFB19:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI38:
	sub	sp, sp, #36
.LCFI39:
	str	r2, [sp, #0]
	ldr	r2, .L59
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #212
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #312
	str	r3, [sp, #24]
	mov	r3, #19
	str	r3, [sp, #28]
	ldr	r3, .L59+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_with_64_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L60:
	.align	2
.L59:
	.word	49184
	.word	.LC19
.LFE19:
	.size	nm_SCHEDULE_set_irrigate_on_29th_or_31st, .-nm_SCHEDULE_set_irrigate_on_29th_or_31st
	.section	.text.nm_DAILY_ET_set_ET_in_use,"ax",%progbits
	.align	2
	.type	nm_DAILY_ET_set_ET_in_use, %function
nm_DAILY_ET_set_ET_in_use:
.LFB21:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI40:
	sub	sp, sp, #36
.LCFI41:
	str	r2, [sp, #0]
	ldr	r2, .L62
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #240
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #312
	str	r3, [sp, #24]
	mov	r3, #25
	str	r3, [sp, #28]
	ldr	r3, .L62+4
	str	r3, [sp, #32]
	mov	r3, #1
	bl	SHARED_set_bool_with_64_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L63:
	.align	2
.L62:
	.word	49158
	.word	.LC20
.LFE21:
	.size	nm_DAILY_ET_set_ET_in_use, .-nm_DAILY_ET_set_ET_in_use
	.section	.text.nm_DAILY_ET_set_use_ET_averaging,"ax",%progbits
	.align	2
	.type	nm_DAILY_ET_set_use_ET_averaging, %function
nm_DAILY_ET_set_use_ET_averaging:
.LFB22:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI42:
	sub	sp, sp, #36
.LCFI43:
	str	r2, [sp, #0]
	ldr	r2, .L65
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #244
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #312
	str	r3, [sp, #24]
	mov	r3, #26
	str	r3, [sp, #28]
	ldr	r3, .L65+4
	str	r3, [sp, #32]
	mov	r3, #1
	bl	SHARED_set_bool_with_64_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L66:
	.align	2
.L65:
	.word	49255
	.word	.LC21
.LFE22:
	.size	nm_DAILY_ET_set_use_ET_averaging, .-nm_DAILY_ET_set_use_ET_averaging
	.section	.text.nm_RAIN_set_rain_in_use,"ax",%progbits
	.align	2
	.type	nm_RAIN_set_rain_in_use, %function
nm_RAIN_set_rain_in_use:
.LFB23:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI44:
	sub	sp, sp, #36
.LCFI45:
	str	r2, [sp, #0]
	ldr	r2, .L68
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #248
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #312
	str	r3, [sp, #24]
	mov	r3, #27
	str	r3, [sp, #28]
	ldr	r3, .L68+4
	str	r3, [sp, #32]
	mov	r3, #1
	bl	SHARED_set_bool_with_64_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L69:
	.align	2
.L68:
	.word	49162
	.word	.LC22
.LFE23:
	.size	nm_RAIN_set_rain_in_use, .-nm_RAIN_set_rain_in_use
	.section	.text.nm_WIND_set_wind_in_use,"ax",%progbits
	.align	2
	.type	nm_WIND_set_wind_in_use, %function
nm_WIND_set_wind_in_use:
.LFB25:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI46:
	sub	sp, sp, #36
.LCFI47:
	str	r2, [sp, #0]
	ldr	r2, .L71
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #256
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #312
	str	r3, [sp, #24]
	mov	r3, #29
	str	r3, [sp, #28]
	ldr	r3, .L71+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_with_64_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L72:
	.align	2
.L71:
	.word	49161
	.word	.LC23
.LFE25:
	.size	nm_WIND_set_wind_in_use, .-nm_WIND_set_wind_in_use
	.section	.text.nm_PUMP_set_pump_in_use,"ax",%progbits
	.align	2
	.type	nm_PUMP_set_pump_in_use, %function
nm_PUMP_set_pump_in_use:
.LFB30:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI48:
	sub	sp, sp, #36
.LCFI49:
	str	r2, [sp, #0]
	ldr	r2, .L74
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #280
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #312
	str	r3, [sp, #24]
	mov	r3, #34
	str	r3, [sp, #28]
	ldr	r3, .L74+4
	str	r3, [sp, #32]
	mov	r3, #1
	bl	SHARED_set_bool_with_64_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L75:
	.align	2
.L74:
	.word	49163
	.word	.LC24
.LFE30:
	.size	nm_PUMP_set_pump_in_use, .-nm_PUMP_set_pump_in_use
	.section	.text.nm_STATION_GROUP_set_in_use,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_in_use, %function
nm_STATION_GROUP_set_in_use:
.LFB44:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI50:
	sub	sp, sp, #36
.LCFI51:
	str	r2, [sp, #0]
	ldr	r2, .L77
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #368
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #312
	str	r3, [sp, #24]
	mov	r3, #48
	str	r3, [sp, #28]
	ldr	r3, .L77+4
	str	r3, [sp, #32]
	mov	r3, #1
	bl	SHARED_set_bool_with_64_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L78:
	.align	2
.L77:
	.word	49415
	.word	.LC25
.LFE44:
	.size	nm_STATION_GROUP_set_in_use, .-nm_STATION_GROUP_set_in_use
	.section	.text.nm_ACQUIRE_EXPECTEDS_set_acquire_expected,"ax",%progbits
	.align	2
	.global	nm_ACQUIRE_EXPECTEDS_set_acquire_expected
	.type	nm_ACQUIRE_EXPECTEDS_set_acquire_expected, %function
nm_ACQUIRE_EXPECTEDS_set_acquire_expected:
.LFB33:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI52:
	sub	sp, sp, #36
.LCFI53:
	str	r2, [sp, #0]
	ldr	r2, .L80
	mov	lr, r1
	stmib	sp, {r2, r3}
	ldr	r3, [sp, #40]
	add	r1, r0, #292
	str	r3, [sp, #12]
	ldr	r3, [sp, #44]
	mov	r2, lr
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	str	r3, [sp, #20]
	add	r3, r0, #312
	str	r3, [sp, #24]
	mov	r3, #37
	str	r3, [sp, #28]
	ldr	r3, .L80+4
	str	r3, [sp, #32]
	mov	r3, #0
	bl	SHARED_set_bool_with_64_bit_change_bits_group
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L81:
	.align	2
.L80:
	.word	49278
	.word	.LC26
.LFE33:
	.size	nm_ACQUIRE_EXPECTEDS_set_acquire_expected, .-nm_ACQUIRE_EXPECTEDS_set_acquire_expected
	.section	.text.nm_SCHEDULE_set_start_time,"ax",%progbits
	.align	2
	.type	nm_SCHEDULE_set_start_time, %function
nm_SCHEDULE_set_start_time:
.LFB13:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI54:
	sub	sp, sp, #44
.LCFI55:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	mov	r4, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r1, .L83
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	str	r2, [sp, #8]
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #15
	ldr	r2, .L83+4
	str	r3, [sp, #36]
	ldr	r3, .L83+8
	mov	ip, #0
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #172
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_time_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L84:
	.align	2
.L83:
	.word	86400
	.word	49172
	.word	.LC27
.LFE13:
	.size	nm_SCHEDULE_set_start_time, .-nm_SCHEDULE_set_start_time
	.section	.text.nm_SCHEDULE_set_stop_time,"ax",%progbits
	.align	2
	.type	nm_SCHEDULE_set_stop_time, %function
nm_SCHEDULE_set_stop_time:
.LFB14:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI56:
	sub	sp, sp, #44
.LCFI57:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #20]
	ldr	r3, [sp, #52]
	ldr	r1, .L86
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	stmib	sp, {r1, r2}
	ldr	r2, .L86+4
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #16
	str	r3, [sp, #36]
	ldr	r3, .L86+8
	str	r1, [sp, #0]
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #176
	mov	r2, lr
	mov	r3, #0
	bl	SHARED_set_time_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L87:
	.align	2
.L86:
	.word	86400
	.word	49173
	.word	.LC28
.LFE14:
	.size	nm_SCHEDULE_set_stop_time, .-nm_SCHEDULE_set_stop_time
	.section	.text.nm_ON_AT_A_TIME_set_on_at_a_time_in_group,"ax",%progbits
	.align	2
	.type	nm_ON_AT_A_TIME_set_on_at_a_time_in_group, %function
nm_ON_AT_A_TIME_set_on_at_a_time_in_group:
.LFB28:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI58:
	sub	sp, sp, #44
.LCFI59:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L89
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #32
	str	r3, [sp, #36]
	ldr	r3, .L89+4
	mov	ip, #1
	mov	r1, #30
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #268
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_on_at_a_time_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L90:
	.align	2
.L89:
	.word	49167
	.word	.LC29
.LFE28:
	.size	nm_ON_AT_A_TIME_set_on_at_a_time_in_group, .-nm_ON_AT_A_TIME_set_on_at_a_time_in_group
	.section	.text.nm_ON_AT_A_TIME_set_on_at_a_time_in_system,"ax",%progbits
	.align	2
	.type	nm_ON_AT_A_TIME_set_on_at_a_time_in_system, %function
nm_ON_AT_A_TIME_set_on_at_a_time_in_system:
.LFB29:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI60:
	sub	sp, sp, #44
.LCFI61:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L92
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #33
	str	r3, [sp, #36]
	ldr	r3, .L92+4
	mov	ip, #1
	mov	r1, #30
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #272
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_on_at_a_time_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L93:
	.align	2
.L92:
	.word	49168
	.word	.LC30
.LFE29:
	.size	nm_ON_AT_A_TIME_set_on_at_a_time_in_system, .-nm_ON_AT_A_TIME_set_on_at_a_time_in_system
	.section	.text.nm_STATION_GROUP_set_moisture_sensor_serial_number,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_moisture_sensor_serial_number, %function
nm_STATION_GROUP_set_moisture_sensor_serial_number:
.LFB45:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI62:
	mov	r8, r2
	ldr	r2, .L100
	sub	sp, sp, #44
.LCFI63:
	str	r2, [sp, #12]
	ldr	r2, [sp, #72]
	mov	r6, r3
	str	r2, [sp, #24]
	ldr	r2, [sp, #76]
	ldr	r3, .L100+4
	str	r2, [sp, #28]
	add	r2, r0, #312
	str	r2, [sp, #32]
	mov	r2, #49
	str	r2, [sp, #36]
	ldr	r2, .L100+8
	ldr	r5, [sp, #68]
	mov	r7, r1
	str	r3, [sp, #0]
	str	r2, [sp, #40]
	mov	r3, #0
	add	r1, r0, #372
	mov	r2, r7
	mov	r4, r0
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	str	r6, [sp, #16]
	str	r5, [sp, #20]
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	cmp	r0, #1
	bne	.L94
.LBB20:
	ldr	r3, .L100+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L100+16
	ldr	r3, .L100+20
	bl	xQueueTakeMutexRecursive_debug
	cmp	r8, #0
	beq	.L96
	cmp	r7, #0
	bne	.L97
	mov	r0, r4
	bl	nm_GROUP_get_name
	ldr	r1, .L100+24
	b	.L99
.L97:
	mov	r0, r7
	bl	MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number
	subs	r7, r0, #0
	mov	r0, r4
	beq	.L98
	bl	nm_GROUP_get_name
	mov	r4, r0
	mov	r0, r7
	bl	nm_GROUP_get_name
	mov	r1, r0
	mov	r0, r4
	b	.L99
.L98:
	bl	nm_GROUP_get_name
	ldr	r1, .L100+28
.L99:
	mov	r2, r6
	mov	r3, r5
	bl	Alert_station_group_assigned_to_a_moisture_sensor
.L96:
	ldr	r3, .L100+12
	ldr	r0, [r3, #0]
.LBE20:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LBB21:
	b	xQueueGiveMutexRecursive
.L94:
.LBE21:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L101:
	.align	2
.L100:
	.word	49427
	.word	2097151
	.word	.LC31
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC32
	.word	2202
	.word	.LC33
	.word	.LC34
.LFE45:
	.size	nm_STATION_GROUP_set_moisture_sensor_serial_number, .-nm_STATION_GROUP_set_moisture_sensor_serial_number
	.section	.text.nm_STATION_GROUP_set_crop_coefficient,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_crop_coefficient, %function
nm_STATION_GROUP_set_crop_coefficient:
.LFB7:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #11
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI64:
	mov	r5, r0
	sub	sp, sp, #76
.LCFI65:
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	bhi	.L103
.LBB24:
	add	r6, sp, #44
	add	r3, r1, #1
	str	r3, [sp, #0]
	mov	r0, r6
	mov	r1, #32
	ldr	r2, .L105
	ldr	r3, .L105+4
	bl	snprintf
	mov	r0, #300
	mov	r3, #100
	stmia	sp, {r0, r3}
	ldr	r3, [sp, #100]
	add	r1, r4, #25
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	add	r4, r4, #49152
	str	r3, [sp, #20]
	ldr	r3, [sp, #108]
	add	r4, r4, #91
	str	r3, [sp, #24]
	ldr	r3, [sp, #112]
	mov	r0, r5
	str	r3, [sp, #28]
	add	r3, r5, #312
	str	r3, [sp, #32]
	mov	r3, #8
	str	r3, [sp, #36]
	add	r1, r5, r1, asl #2
	mov	r2, r8
	mov	r3, #1
	str	r7, [sp, #8]
	str	r4, [sp, #12]
	str	r6, [sp, #40]
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	b	.L102
.L103:
.LBE24:
	ldr	r0, .L105+8
	mov	r1, #700
	bl	Alert_index_out_of_range_with_filename
.L102:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L106:
	.align	2
.L105:
	.word	.LC35
	.word	.LC36
	.word	.LC32
.LFE7:
	.size	nm_STATION_GROUP_set_crop_coefficient, .-nm_STATION_GROUP_set_crop_coefficient
	.section	.text.nm_STATION_GROUP_set_soil_storage_capacity,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_soil_storage_capacity, %function
nm_STATION_GROUP_set_soil_storage_capacity:
.LFB24:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI66:
	mov	r6, r3
	sub	sp, sp, #44
.LCFI67:
	mov	r3, #600
	mov	lr, #150
	stmia	sp, {r3, lr}
	ldr	r3, .L113
	ldr	r8, [sp, #68]
	str	r3, [sp, #12]
	ldr	r3, [sp, #76]
	ldr	r7, [sp, #72]
	str	r3, [sp, #28]
	add	r3, r0, #312
	str	r3, [sp, #32]
	mov	r3, #28
	str	r3, [sp, #36]
	ldr	r3, .L113+4
	mov	ip, r1
	str	r2, [sp, #8]
	str	r3, [sp, #40]
	add	r1, r0, #252
	mov	r2, ip
	mov	r3, #1
	mov	r5, r0
	str	r6, [sp, #16]
	str	r8, [sp, #20]
	str	r7, [sp, #24]
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	cmp	r0, #1
	bne	.L107
.LBB29:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	beq	.L107
.LBE29:
.LBB30:
	ldr	r0, .L113+8
	bl	nm_ListGetFirst
	b	.L112
.L111:
	mov	r0, r4
	bl	STATION_get_GID_station_group
	ldr	r3, [r5, #16]
	cmp	r0, r3
	bne	.L110
	mov	r1, r6
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r1, #1
	mov	r2, #0
	mov	r3, r6
	str	r8, [sp, #0]
	str	r7, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_ignore_moisture_balance_at_next_irrigation
.L110:
	ldr	r0, .L113+8
	mov	r1, r4
	bl	nm_ListGetNext
.L112:
	cmp	r0, #0
	mov	r4, r0
	bne	.L111
.L107:
.LBE30:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L114:
	.align	2
.L113:
	.word	49231
	.word	.LC37
	.word	station_info_list_hdr
.LFE24:
	.size	nm_STATION_GROUP_set_soil_storage_capacity, .-nm_STATION_GROUP_set_soil_storage_capacity
	.section	.text.nm_SCHEDULE_set_water_day,"ax",%progbits
	.align	2
	.type	nm_SCHEDULE_set_water_day, %function
nm_SCHEDULE_set_water_day:
.LFB17:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #6
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI68:
	mov	r5, r0
	sub	sp, sp, #68
.LCFI69:
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	bhi	.L116
.LBB33:
	mov	r0, r1
	bl	GetDayShortStr
	add	r6, sp, #36
	mov	r1, #32
	ldr	r2, .L118
	ldr	r3, .L118+4
	str	r0, [sp, #0]
	mov	r0, r6
	bl	snprintf
	ldr	r3, [sp, #92]
	add	r1, r4, #46
	str	r3, [sp, #8]
	ldr	r3, [sp, #96]
	add	r4, r4, #49152
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	add	r4, r4, #24
	str	r3, [sp, #16]
	ldr	r3, [sp, #104]
	mov	r0, r5
	str	r3, [sp, #20]
	add	r3, r5, #312
	str	r3, [sp, #24]
	mov	r3, #18
	str	r3, [sp, #28]
	add	r1, r5, r1, asl #2
	mov	r2, r8
	mov	r3, #0
	str	r7, [sp, #0]
	str	r4, [sp, #4]
	str	r6, [sp, #32]
	bl	SHARED_set_bool_with_64_bit_change_bits_group
	b	.L115
.L116:
.LBE33:
	ldr	r0, .L118+8
	ldr	r1, .L118+12
	bl	Alert_index_out_of_range_with_filename
.L115:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L119:
	.align	2
.L118:
	.word	.LC38
	.word	.LC39
	.word	.LC32
	.word	1092
.LFE17:
	.size	nm_SCHEDULE_set_water_day, .-nm_SCHEDULE_set_water_day
	.section	.text.nm_STATION_GROUP_set_GID_irrigation_system,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_GID_irrigation_system, %function
nm_STATION_GROUP_set_GID_irrigation_system:
.LFB43:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI70:
	sub	sp, sp, #44
.LCFI71:
	mov	r4, r0
	add	r5, sp, #76
	mov	r0, #0
	mov	r7, r1
	mov	r6, r3
	ldmia	r5, {r5, r9, sl}
	mov	r8, r2
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_group_ID
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #8]
	ldr	r3, .L123
	add	r1, r4, #364
	str	r3, [sp, #12]
	add	r3, r4, #312
	str	r3, [sp, #32]
	mov	r3, #47
	str	r3, [sp, #36]
	ldr	r3, .L123+4
	mov	r2, r7
	str	r3, [sp, #40]
	mov	r3, #1
	str	r6, [sp, #16]
	str	r5, [sp, #20]
	str	r9, [sp, #24]
	str	sl, [sp, #28]
	str	r0, [sp, #4]
	mov	r0, r4
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	cmp	r0, #1
	bne	.L120
.LBB37:
	ldr	r3, .L123+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L123+12
	ldr	r3, .L123+16
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r7
	bl	SYSTEM_get_group_with_this_GID
	cmp	r0, #0
	cmpne	r8, #0
	mov	r7, r0
	beq	.L122
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r4, r0
	mov	r0, r7
	bl	nm_GROUP_get_name
	mov	r2, r6
	mov	r3, r5
	mov	r1, r0
	mov	r0, r4
	bl	Alert_station_group_assigned_to_mainline
.L122:
	ldr	r3, .L123+8
	ldr	r0, [r3, #0]
.LBE37:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LBB38:
	b	xQueueGiveMutexRecursive
.L120:
.LBE38:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L124:
	.align	2
.L123:
	.word	49417
	.word	.LC40
	.word	list_system_recursive_MUTEX
	.word	.LC32
	.word	2109
.LFE43:
	.size	nm_STATION_GROUP_set_GID_irrigation_system, .-nm_STATION_GROUP_set_GID_irrigation_system
	.section	.text.nm_STATION_GROUP_set_microclimate_factor.constprop.6,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_microclimate_factor.constprop.6, %function
nm_STATION_GROUP_set_microclimate_factor.constprop.6:
.LFB183:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI72:
	sub	sp, sp, #44
.LCFI73:
	str	r3, [sp, #20]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #24]
	ldr	r3, [sp, #52]
	mov	r1, #200
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	mov	r1, #100
	add	r3, r0, #312
	str	r1, [sp, #4]
	str	r3, [sp, #32]
	mov	r1, #0
	mov	r3, #43
	str	r1, [sp, #8]
	str	r3, [sp, #36]
	ldr	r1, .L126
	ldr	r3, .L126+4
	str	r1, [sp, #12]
	str	r2, [sp, #16]
	str	r3, [sp, #40]
	add	r1, r0, #340
	mov	r2, lr
	mov	r3, #10
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L127:
	.align	2
.L126:
	.word	49291
	.word	.LC41
.LFE183:
	.size	nm_STATION_GROUP_set_microclimate_factor.constprop.6, .-nm_STATION_GROUP_set_microclimate_factor.constprop.6
	.section	.text.nm_PERCENT_ADJUST_set_start_date.constprop.7,"ax",%progbits
	.align	2
	.type	nm_PERCENT_ADJUST_set_start_date.constprop.7, %function
nm_PERCENT_ADJUST_set_start_date.constprop.7:
.LFB182:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI74:
	ldr	r6, .L129
	mov	r4, r0
	mov	r0, #1
	sub	sp, sp, #44
.LCFI75:
	mov	r9, r1
	mov	r5, r2
	mov	r1, r0
	mov	r2, r6
	mov	r7, r3
	bl	DMYToDate
	mov	r1, #12
	ldr	r2, .L129+4
	mov	sl, r0
	mov	r0, #31
	bl	DMYToDate
	mov	r2, r6
	mov	r8, r0
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	mov	r2, #0
	str	r2, [sp, #8]
	ldr	r2, .L129+8
	mov	r3, sl, asl #16
	str	r2, [sp, #12]
	ldr	r2, [sp, #76]
	mov	r8, r8, asl #16
	str	r2, [sp, #24]
	ldr	r2, [sp, #80]
	mov	r8, r8, lsr #16
	str	r2, [sp, #28]
	add	r2, r4, #312
	str	r2, [sp, #32]
	mov	r2, #11
	str	r2, [sp, #36]
	ldr	r2, .L129+12
	add	r1, r4, #156
	str	r2, [sp, #40]
	mov	r3, r3, lsr #16
	mov	r2, r9
	str	r8, [sp, #0]
	str	r5, [sp, #16]
	str	r7, [sp, #20]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	str	r0, [sp, #4]
	mov	r0, r4
	bl	SHARED_set_date_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L130:
	.align	2
.L129:
	.word	2011
	.word	2042
	.word	49165
	.word	.LC42
.LFE182:
	.size	nm_PERCENT_ADJUST_set_start_date.constprop.7, .-nm_PERCENT_ADJUST_set_start_date.constprop.7
	.section	.text.nm_STATION_GROUP_set_allowable_depletion.constprop.8,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_allowable_depletion.constprop.8, %function
nm_STATION_GROUP_set_allowable_depletion.constprop.8:
.LFB181:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI76:
	sub	sp, sp, #44
.LCFI77:
	str	r3, [sp, #20]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #24]
	ldr	r3, [sp, #52]
	mov	r1, #100
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	mov	r1, #50
	add	r3, r0, #312
	str	r1, [sp, #4]
	str	r3, [sp, #32]
	mov	r1, #0
	mov	r3, #38
	str	r1, [sp, #8]
	str	r3, [sp, #36]
	ldr	r1, .L132
	ldr	r3, .L132+4
	str	r1, [sp, #12]
	str	r2, [sp, #16]
	str	r3, [sp, #40]
	add	r1, r0, #320
	mov	r2, lr
	mov	r3, #10
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L133:
	.align	2
.L132:
	.word	49287
	.word	.LC43
.LFE181:
	.size	nm_STATION_GROUP_set_allowable_depletion.constprop.8, .-nm_STATION_GROUP_set_allowable_depletion.constprop.8
	.section	.text.nm_STATION_GROUP_set_available_water.constprop.9,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_available_water.constprop.9, %function
nm_STATION_GROUP_set_available_water.constprop.9:
.LFB180:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI78:
	sub	sp, sp, #44
.LCFI79:
	str	r3, [sp, #20]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #24]
	ldr	r3, [sp, #52]
	mov	r1, #100
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	mov	r1, #15
	add	r3, r0, #312
	str	r1, [sp, #4]
	str	r3, [sp, #32]
	mov	r1, #0
	mov	r3, #39
	str	r1, [sp, #8]
	str	r3, [sp, #36]
	ldr	r1, .L135
	ldr	r3, .L135+4
	str	r1, [sp, #12]
	str	r2, [sp, #16]
	str	r3, [sp, #40]
	add	r1, r0, #324
	mov	r2, lr
	mov	r3, #1
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L136:
	.align	2
.L135:
	.word	49288
	.word	.LC44
.LFE180:
	.size	nm_STATION_GROUP_set_available_water.constprop.9, .-nm_STATION_GROUP_set_available_water.constprop.9
	.section	.text.nm_STATION_GROUP_set_root_zone_depth.constprop.10,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_root_zone_depth.constprop.10, %function
nm_STATION_GROUP_set_root_zone_depth.constprop.10:
.LFB179:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI80:
	sub	sp, sp, #44
.LCFI81:
	str	r3, [sp, #20]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #24]
	ldr	r3, [sp, #52]
	mov	r1, #480
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	mov	r1, #100
	add	r3, r0, #312
	str	r1, [sp, #4]
	str	r3, [sp, #32]
	mov	r1, #0
	mov	r3, #40
	str	r1, [sp, #8]
	str	r3, [sp, #36]
	ldr	r1, .L138
	ldr	r3, .L138+4
	str	r1, [sp, #12]
	str	r2, [sp, #16]
	str	r3, [sp, #40]
	add	r1, r0, #328
	mov	r2, lr
	mov	r3, #10
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L139:
	.align	2
.L138:
	.word	49242
	.word	.LC45
.LFE179:
	.size	nm_STATION_GROUP_set_root_zone_depth.constprop.10, .-nm_STATION_GROUP_set_root_zone_depth.constprop.10
	.section	.text.nm_STATION_GROUP_set_species_factor.constprop.11,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_species_factor.constprop.11, %function
nm_STATION_GROUP_set_species_factor.constprop.11:
.LFB178:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI82:
	sub	sp, sp, #44
.LCFI83:
	str	r3, [sp, #20]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #24]
	ldr	r3, [sp, #52]
	mov	r1, #100
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	mov	r1, #50
	add	r3, r0, #312
	str	r1, [sp, #4]
	str	r3, [sp, #32]
	mov	r1, #0
	mov	r3, #41
	str	r1, [sp, #8]
	str	r3, [sp, #36]
	ldr	r1, .L141
	ldr	r3, .L141+4
	str	r1, [sp, #12]
	str	r2, [sp, #16]
	str	r3, [sp, #40]
	add	r1, r0, #332
	mov	r2, lr
	mov	r3, #10
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L142:
	.align	2
.L141:
	.word	49289
	.word	.LC46
.LFE178:
	.size	nm_STATION_GROUP_set_species_factor.constprop.11, .-nm_STATION_GROUP_set_species_factor.constprop.11
	.section	.text.nm_STATION_GROUP_set_density_factor.constprop.12,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_density_factor.constprop.12, %function
nm_STATION_GROUP_set_density_factor.constprop.12:
.LFB177:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI84:
	sub	sp, sp, #44
.LCFI85:
	str	r3, [sp, #20]
	ldr	r3, [sp, #48]
	mov	lr, r1
	str	r3, [sp, #24]
	ldr	r3, [sp, #52]
	mov	r1, #200
	str	r1, [sp, #0]
	str	r3, [sp, #28]
	mov	r1, #100
	add	r3, r0, #312
	str	r1, [sp, #4]
	str	r3, [sp, #32]
	mov	r1, #0
	mov	r3, #42
	str	r1, [sp, #8]
	str	r3, [sp, #36]
	ldr	r1, .L144
	ldr	r3, .L144+4
	str	r1, [sp, #12]
	str	r2, [sp, #16]
	str	r3, [sp, #40]
	add	r1, r0, #336
	mov	r2, lr
	mov	r3, #10
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L145:
	.align	2
.L144:
	.word	49290
	.word	.LC47
.LFE177:
	.size	nm_STATION_GROUP_set_density_factor.constprop.12, .-nm_STATION_GROUP_set_density_factor.constprop.12
	.section	.text.nm_SCHEDULE_set_begin_date,"ax",%progbits
	.align	2
	.global	nm_SCHEDULE_set_begin_date
	.type	nm_SCHEDULE_set_begin_date, %function
nm_SCHEDULE_set_begin_date:
.LFB18:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI86:
	ldr	r6, .L147
	mov	r4, r0
	mov	r0, #1
	sub	sp, sp, #44
.LCFI87:
	mov	r9, r1
	mov	r5, r2
	mov	r1, r0
	mov	r2, r6
	mov	r7, r3
	bl	DMYToDate
	mov	r1, #12
	ldr	r2, .L147+4
	mov	sl, r0
	mov	r0, #31
	bl	DMYToDate
	mov	r2, r6
	mov	r8, r0
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	ldr	r2, .L147+8
	mov	r3, sl, asl #16
	str	r2, [sp, #12]
	ldr	r2, [sp, #76]
	mov	r8, r8, asl #16
	str	r2, [sp, #20]
	ldr	r2, [sp, #80]
	mov	r8, r8, lsr #16
	str	r2, [sp, #24]
	ldr	r2, [sp, #84]
	add	r1, r4, #180
	str	r2, [sp, #28]
	add	r2, r4, #312
	str	r2, [sp, #32]
	mov	r2, #17
	str	r2, [sp, #36]
	ldr	r2, .L147+12
	mov	r3, r3, lsr #16
	str	r2, [sp, #40]
	mov	r2, r9
	str	r8, [sp, #0]
	str	r7, [sp, #16]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	stmib	sp, {r0, r5}
	mov	r0, r4
	bl	SHARED_set_date_with_64_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L148:
	.align	2
.L147:
	.word	2011
	.word	2042
	.word	49183
	.word	.LC48
.LFE18:
	.size	nm_SCHEDULE_set_begin_date, .-nm_SCHEDULE_set_begin_date
	.section	.text.ripple_begin_date_when_irrigating_every_third_day_or_greater.part.5,"ax",%progbits
	.align	2
	.type	ripple_begin_date_when_irrigating_every_third_day_or_greater.part.5, %function
ripple_begin_date_when_irrigating_every_third_day_or_greater.part.5:
.LFB176:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI88:
	ldr	r6, [r1, #0]
	mov	r5, r2
	mov	r4, r0
	bl	FLOWSENSE_get_controller_index
	mov	r3, #1
	stmib	sp, {r3, r5}
	mov	r1, r6
	mov	r2, #0
	mov	r3, #6
	str	r0, [sp, #0]
	mov	r0, r4
	bl	nm_SCHEDULE_set_begin_date
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, pc}
.LFE176:
	.size	ripple_begin_date_when_irrigating_every_third_day_or_greater.part.5, .-ripple_begin_date_when_irrigating_every_third_day_or_greater.part.5
	.section	.text.nm_SCHEDULE_set_last_ran,"ax",%progbits
	.align	2
	.global	nm_SCHEDULE_set_last_ran
	.type	nm_SCHEDULE_set_last_ran, %function
nm_SCHEDULE_set_last_ran:
.LFB20:
	@ args = 16, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI89:
	sub	sp, sp, #64
.LCFI90:
	add	ip, sp, #56
	stmia	ip, {r1, r2}
	mov	lr, r0
	ldmia	ip, {r0, r1}
	str	r3, [sp, #24]
	ldr	r3, [sp, #68]
	str	r0, [sp, #0]
	str	r3, [sp, #28]
	ldr	r3, [sp, #72]
	strh	r1, [sp, #4]	@ movhi
	str	r3, [sp, #32]
	ldr	r3, [sp, #76]
	str	r0, [sp, #8]
	str	r3, [sp, #36]
	ldr	r3, [sp, #80]
	strh	r1, [sp, #12]	@ movhi
	str	r3, [sp, #40]
	add	r3, lr, #312
	str	r3, [sp, #44]
	mov	r3, #21
	str	r3, [sp, #48]
	ldr	r3, .L151
	str	r0, [sp, #16]
	strh	r1, [sp, #20]	@ movhi
	str	r3, [sp, #52]
	mov	r0, lr
	add	r1, lr, #220
	ldmia	ip, {r2, r3}
	bl	SHARED_set_date_time_with_64_bit_change_bits_group
	add	sp, sp, #64
	ldmfd	sp!, {pc}
.L152:
	.align	2
.L151:
	.word	.LC49
.LFE20:
	.size	nm_SCHEDULE_set_last_ran, .-nm_SCHEDULE_set_last_ran
	.section	.text.nm_STATION_GROUP_set_default_values,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_set_default_values, %function
nm_STATION_GROUP_set_default_values:
.LFB53:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L160+4
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI91:
	sub	sp, sp, #64
.LCFI92:
	str	r3, [sp, #56]	@ float
	ldr	r3, .L160+8
	mov	r4, r0
	mov	r6, r1
	str	r3, [sp, #60]	@ float
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #0
	mov	r7, r0
	beq	.L154
	ldr	r8, .L160+12
	add	r5, r4, #296
	ldr	r1, [r8, #0]
	mov	r0, r5
	bl	SHARED_clear_all_64_bit_change_bits
	ldr	r1, [r8, #0]
	add	r0, r4, #304
	bl	SHARED_clear_all_64_bit_change_bits
	add	r9, r4, #312
	ldr	r1, [r8, #0]
	add	r0, r4, #352
	bl	SHARED_clear_all_64_bit_change_bits
	mov	r0, r9
	ldr	r1, [r8, #0]
	bl	SHARED_set_all_64_bit_change_bits
	mov	r8, #0
	mov	r0, r4
	mov	r1, r8
	mov	r2, r8
	mov	r3, #11
	str	r8, [r4, #228]
	str	r8, [r4, #232]
	str	r8, [r4, #236]
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_STATION_GROUP_set_plant_type
	mov	r0, r4
	mov	r1, r8
	mov	r2, r8
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_STATION_GROUP_set_head_type
	ldr	r3, .L160+16
	flds	s15, .L160
	mov	r0, r4
	flds	s14, [r3, #0]
	mov	r2, r8
	mov	r3, #11
	str	r7, [sp, #0]
	fmuls	s15, s14, s15
	str	r6, [sp, #4]
	str	r5, [sp, #8]
.LBB39:
	mov	sl, #11
.LBE39:
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_precip_rate
	mov	r0, r4
	mov	r1, #3
	mov	r2, r8
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_STATION_GROUP_set_soil_type
	mov	r0, r4
	mov	r1, r8
	mov	r2, r8
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_STATION_GROUP_set_slope_percentage
.LBB40:
	mov	r3, #4
	stmia	sp, {r3, r8}
	ldr	r3, .L160+20
	add	r1, r4, #92
	str	r3, [sp, #12]
	mov	r3, #6
	str	r3, [sp, #36]
	ldr	r3, .L160+24
	mov	r2, r8
	str	r3, [sp, #40]
	mov	r0, r4
	mov	r3, r8
	str	r9, [sp, #32]
	str	r8, [sp, #8]
	str	sl, [sp, #16]
	str	r7, [sp, #20]
	str	r6, [sp, #24]
	str	r5, [sp, #28]
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
.LBE40:
	mov	r0, r4
	mov	r1, #80
	mov	r2, r8
	mov	r3, sl
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_STATION_GROUP_set_usable_rain
	ldr	r9, [r4, #72]
	ldr	r3, .L160+28
	ldr	r2, .L160+32
	mov	r1, #12
	add	r3, r3, r9, asl #2
	mla	r2, r1, r9, r2
	ldr	r3, [r3, #0]	@ float
	ldr	r0, [r4, #92]
	ldr	fp, [r2, #4]	@ float
	str	r3, [sp, #44]
	bl	WATERSENSE_get_Kmc_index_based_on_exposure
	ldr	r2, .L160+36
	ldr	r3, [sp, #44]
	add	r9, r9, r9, asl #1
	mov	r1, fp	@ float
	add	r9, r9, r0
	add	r9, r2, r9, asl #2
	ldr	r2, [r9, #0]	@ float
	mov	r0, r3	@ float
	bl	WATERSENSE_get_Kl
	flds	s15, [sp, #56]
	fmsr	s9, r0
	fmuls	s15, s9, s15
	ftouizs	s15, s15
	fmrs	r9, s15	@ int
.L155:
	mov	r1, r8
	mov	r0, r4
	mov	r2, r9
	mov	r3, #0
	add	r8, r8, #1
	str	sl, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	r5, [sp, #12]
	bl	nm_STATION_GROUP_set_crop_coefficient
	cmp	r8, #12
	bne	.L155
	ldr	r2, [r4, #84]
	flds	s14, [sp, #56]
	ldr	r3, .L160+40
	mov	r0, r4
	add	r3, r3, r2, asl #2
	flds	s15, [r3, #0]
	mov	r2, #11
	mov	r3, r7
	str	r6, [sp, #0]
	fmuls	s15, s14, s15
	str	r5, [sp, #4]
	mov	sl, #7
	mov	r9, #11
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_allowable_depletion.constprop.8
	ldr	r2, [r4, #84]
	flds	s14, [sp, #56]
	ldr	r3, .L160+44
	mov	r0, r4
	add	r3, r3, r2, asl #2
	flds	s15, [r3, #0]
	mov	r2, #11
	mov	r3, r7
	str	r6, [sp, #0]
	fmuls	s15, s14, s15
	str	r5, [sp, #4]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_available_water.constprop.9
	ldr	r2, [r4, #72]
	flds	s14, [sp, #60]
	ldr	r3, [r4, #84]
	mov	r0, r4
	mla	r3, sl, r2, r3
	ldr	r2, .L160+48
	str	r6, [sp, #0]
	add	r3, r2, r3, asl #2
	flds	s15, [r3, #0]
	mov	r2, #11
	mov	r3, r7
	str	r5, [sp, #4]
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_root_zone_depth.constprop.10
	flds	s9, [r4, #324]	@ int
	flds	s14, [r4, #320]	@ int
	flds	s11, [sp, #56]
	flds	s13, [sp, #56]
	fuitos	s12, s9
	flds	s9, [r4, #328]	@ int
	fuitos	s10, s14
	flds	s15, [sp, #60]
	fuitos	s14, s9
	fdivs	s12, s12, s13
	fdivs	s10, s10, s11
	fmrs	r1, s12
	fdivs	s14, s14, s15
	fmrs	r0, s10
	fmrs	r2, s14
	bl	WATERSENSE_get_RZWWS
	flds	s15, [sp, #56]
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	fmsr	s14, r0
	mov	r0, r4
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_soil_storage_capacity
	ldr	r2, [r4, #72]
	flds	s14, [sp, #56]
	ldr	r3, .L160+28
	mov	r0, r4
	add	r3, r3, r2, asl #2
	flds	s15, [r3, #0]
	mov	r2, #11
	mov	r3, r7
	str	r6, [sp, #0]
	fmuls	s15, s14, s15
	str	r5, [sp, #4]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_species_factor.constprop.11
	ldr	r2, [r4, #72]
	flds	s14, [sp, #56]
	ldr	r3, .L160+32
	mov	r0, r4
	mla	r8, r2, r8, r3
	mov	r2, #11
	flds	s15, [r8, #4]
	mov	r3, r7
	str	r6, [sp, #0]
	str	r5, [sp, #4]
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_density_factor.constprop.12
	ldr	r0, [r4, #92]
	ldr	r8, [r4, #72]
	bl	WATERSENSE_get_Kmc_index_based_on_exposure
	ldr	r3, .L160+36
	flds	s14, [sp, #56]
	add	r8, r8, r8, asl #1
	mov	r2, #11
	str	r6, [sp, #0]
	str	r5, [sp, #4]
	add	r8, r8, r0
	add	r8, r3, r8, asl #2
	flds	s15, [r8, #0]
	mov	r0, r4
	mov	r3, r7
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_microclimate_factor.constprop.6
	ldr	r2, [r4, #84]
	flds	s14, [sp, #56]
	ldr	r3, .L160+52
	mov	r0, r4
	add	r3, r3, r2, asl #2
	flds	s15, [r3, #0]
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	fmuls	s15, s14, s15
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_soil_intake_rate
	ldr	r2, [r4, #84]
	flds	s14, [sp, #56]
	ldr	r3, [r4, #88]
	mov	r0, r4
	add	r3, r3, r2, asl #2
	ldr	r2, .L160+56
	str	r7, [sp, #0]
	add	r3, r2, r3, asl #2
	flds	s15, [r3, #0]
	mov	r2, #0
	mov	r3, #11
	str	r6, [sp, #4]
	fmuls	s15, s14, s15
	str	r5, [sp, #8]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_allowable_surface_accumulation
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	mov	r0, r4
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_PRIORITY_set_priority
	add	r0, sp, #48
	b	.L161
.L162:
	.align	2
.L160:
	.word	1203982336
	.word	1120403456
	.word	1092616192
	.word	list_program_data_recursive_MUTEX
	.word	PR
	.word	49276
	.word	.LC50
	.word	Ks
	.word	Kd
	.word	Kmc
	.word	MAD
	.word	AW
	.word	ROOT_ZONE_DEPTH
	.word	IR
	.word	ASA
	.word	86400
	.word	2011
	.word	.LC51
.L161:
	bl	EPSON_obtain_latest_time_and_date
	mov	r0, r4
	mov	r1, #100
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_PERCENT_ADJUST_set_percent
	ldrh	r1, [sp, #52]
	mov	r0, r4
	mov	r2, #11
	mov	r3, r7
	str	r6, [sp, #0]
	str	r5, [sp, #4]
	bl	nm_PERCENT_ADJUST_set_start_date.constprop.7
	ldrh	r1, [sp, #52]
	mov	r0, r4
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_PERCENT_ADJUST_set_end_date
	mov	r1, #0
	mov	r2, r1
	mov	r0, r4
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_SCHEDULE_set_enabled
	mov	r1, #0
	mov	r2, r1
	mov	r0, r4
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_SCHEDULE_set_start_time
	mov	r0, r4
	ldr	r1, .L160+60
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_SCHEDULE_set_stop_time
	mov	r1, sl
	mov	r0, r4
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_SCHEDULE_set_mow_day
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_SCHEDULE_set_schedule_type
	mov	sl, #0
.L156:
	mov	r2, #0
	mov	r1, sl
	mov	r0, r4
	mov	r3, r2
	add	sl, sl, #1
	str	r9, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	r5, [sp, #12]
	bl	nm_SCHEDULE_set_water_day
	cmp	sl, #7
	mov	r8, #11
	bne	.L156
	ldrh	r1, [sp, #52]
	mov	r0, r4
	mov	r3, r8
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_SCHEDULE_set_begin_date
	mov	r1, #0
	mov	r3, r8
	mov	r0, r4
	mov	r2, r1
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_SCHEDULE_set_irrigate_on_29th_or_31st
	mov	r0, #1
	mov	r1, r0
	ldr	r2, .L160+64
	bl	DMYToDate
	strh	r0, [sp, #52]	@ movhi
	mov	r0, #0
	mov	r1, r0
	mov	r2, r0
	bl	HMSToTime
	add	r3, sp, #64
	str	r0, [r3, #-16]!
	str	r8, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	r5, [sp, #12]
	mov	r0, r4
	ldmia	r3, {r1, r2}
	mov	r3, #0
	bl	nm_SCHEDULE_set_last_ran
	mov	r0, r4
	mov	r3, r8
	mov	r1, #1
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_DAILY_ET_set_ET_in_use
	mov	r0, r4
	mov	r3, r8
	mov	r1, #1
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_DAILY_ET_set_use_ET_averaging
	mov	r0, r4
	mov	r3, r8
	mov	r1, #1
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_RAIN_set_rain_in_use
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, r8
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_WIND_set_wind_in_use
	mov	r0, r4
	mov	r3, r8
	mov	r1, #1
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_ALERT_ACTIONS_set_high_flow_action
	mov	r0, r4
	mov	r3, r8
	mov	r1, #1
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_ALERT_ACTIONS_set_low_flow_action
	mov	r0, r4
	mov	r3, r8
	mov	r1, #1
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_ON_AT_A_TIME_set_on_at_a_time_in_group
	mov	r0, r4
	mov	r3, r8
	mov	r1, #1
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_ON_AT_A_TIME_set_on_at_a_time_in_system
	mov	r0, r4
	mov	r3, r8
	mov	r1, #1
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_PUMP_set_pump_in_use
	mov	r0, r4
	mov	r3, r8
	mov	r1, #120
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_LINE_FILL_TIME_set_line_fill_time
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, r8
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_DELAY_BETWEEN_VALVES_set_delay_time
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, r8
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_ACQUIRE_EXPECTEDS_set_acquire_expected
	mov	r3, r8
	mov	r1, #25
	mov	r2, #0
	mov	r0, r4
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_STATION_GROUP_set_budget_reduction_percentage_cap
	mov	r0, #0
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_group_ID
	mov	r3, r8
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	mov	r1, r0
	mov	r0, r4
	bl	nm_STATION_GROUP_set_GID_irrigation_system
	mov	r0, r4
	mov	r3, r8
	mov	r1, #1
	mov	r2, #0
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_STATION_GROUP_set_in_use
	mov	r1, #0
	mov	r0, r4
	mov	r2, r1
	mov	r3, r8
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_STATION_GROUP_set_moisture_sensor_serial_number
	b	.L153
.L154:
	ldr	r0, .L160+68
	mov	r1, #1472
	bl	Alert_func_call_with_null_ptr_with_filename
.L153:
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.LFE53:
	.size	nm_STATION_GROUP_set_default_values, .-nm_STATION_GROUP_set_default_values
	.section	.text.save_file_station_group,"ax",%progbits
	.align	2
	.global	save_file_station_group
	.type	save_file_station_group, %function
save_file_station_group:
.LFB50:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #376
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI93:
	ldr	r1, .L164
	str	r3, [sp, #0]
	ldr	r3, .L164+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, #7
	str	r3, [sp, #4]
	mov	r3, #13
	str	r3, [sp, #8]
	ldr	r3, .L164+8
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L165:
	.align	2
.L164:
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR1
.LFE50:
	.size	save_file_station_group, .-save_file_station_group
	.section	.text.STATION_GROUP_copy_details_into_group_name,"ax",%progbits
	.align	2
	.global	STATION_GROUP_copy_details_into_group_name
	.type	STATION_GROUP_copy_details_into_group_name, %function
STATION_GROUP_copy_details_into_group_name:
.LFB51:
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI94:
	cmp	r1, #2
	sub	sp, sp, #104
.LCFI95:
	mov	r5, r0
	mov	r4, r2
	mov	r6, r3
	addls	r0, sp, #8
	ldrls	r1, .L183
	bls	.L181
	cmp	r1, #3
	addeq	r0, sp, #8
	ldreq	r1, .L183+4
	beq	.L181
	cmp	r1, #4
	addeq	r0, sp, #8
	ldreq	r1, .L183+8
	beq	.L181
	cmp	r1, #5
	addeq	r0, sp, #8
	ldreq	r1, .L183+12
	beq	.L181
	sub	r3, r1, #6
	cmp	r3, #2
	addls	r0, sp, #8
	ldrls	r1, .L183+16
	bls	.L181
	sub	r3, r1, #9
	cmp	r3, #2
	addls	r0, sp, #8
	ldrls	r1, .L183+20
	bls	.L181
	sub	r1, r1, #12
	cmp	r1, #1
	ldrls	r1, .L183+24
	ldrhi	r1, .L183+28
	add	r0, sp, #8
.L181:
	mov	r2, #48
	bl	strlcpy
	cmp	r4, #2
	addls	r0, sp, #56
	ldrls	r1, .L183+32
	bls	.L182
	sub	r3, r4, #3
	cmp	r3, #3
	addls	r0, sp, #56
	ldrls	r1, .L183+36
	bls	.L182
	sub	r3, r4, #7
	cmp	r3, #2
	addls	r0, sp, #56
	ldrls	r1, .L183+40
	bls	.L182
	cmp	r4, #10
	addeq	r0, sp, #56
	ldreq	r1, .L183+44
	beq	.L182
	sub	r4, r4, #11
	cmp	r4, #1
	ldrls	r1, .L183+48
	ldrhi	r1, .L183+52
	addls	r0, sp, #56
	addhi	r0, sp, #8
.L182:
	mov	r2, #48
	bl	strlcpy
	mov	r0, r6
	bl	GetExposureStr
	add	r3, sp, #56
	str	r3, [sp, #0]
	mov	r1, #48
	ldr	r2, .L183+56
	add	r3, sp, #8
	str	r0, [sp, #4]
	mov	r0, r5
	bl	snprintf
	mov	r0, r5
	add	sp, sp, #104
	ldmfd	sp!, {r4, r5, r6, pc}
.L184:
	.align	2
.L183:
	.word	.LC52
	.word	.LC53
	.word	.LC54
	.word	.LC55
	.word	.LC56
	.word	.LC57
	.word	.LC58
	.word	.LC59
	.word	.LC60
	.word	.LC61
	.word	.LC62
	.word	.LC63
	.word	.LC64
	.word	.LC65
	.word	.LC66
.LFE51:
	.size	STATION_GROUP_copy_details_into_group_name, .-STATION_GROUP_copy_details_into_group_name
	.section	.text.init_file_station_group,"ax",%progbits
	.align	2
	.global	init_file_station_group
	.type	init_file_station_group, %function
init_file_station_group:
.LFB49:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI96:
	ldr	r3, .L187
	ldr	r7, .L187+4
	sub	sp, sp, #24
.LCFI97:
	str	r3, [sp, #0]
	ldr	r3, [r7, #0]
	ldr	r5, .L187+8
	str	r3, [sp, #4]
	ldr	r3, .L187+12
	ldr	r4, .L187+16
	str	r3, [sp, #8]
	ldr	r3, .L187+20
	mov	r0, #1
	str	r3, [sp, #12]
	ldr	r1, .L187+24
	mov	r2, #7
	mov	r3, r5
	mov	r6, #13
	str	r4, [sp, #16]
	str	r6, [sp, #20]
	bl	FLASH_FILE_initialize_list_and_find_or_create_group_list_file
.LBB43:
	ldr	r3, .L187+28
	ldr	r0, [r7, #0]
	mov	r1, #400
	ldr	r2, .L187+32
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #8]
	cmp	r3, #1
	bne	.L186
	mov	r0, r5
	bl	nm_ListGetFirst
	add	r7, r0, #20
	mov	r5, r0
	mov	r0, r4
	bl	strlen
	mov	r1, r4
	mov	r2, r0
	mov	r0, r7
	bl	strncmp
	subs	r4, r0, #0
	bne	.L186
	ldr	r1, [r5, #72]
	mov	r0, r7
	ldr	r2, [r5, #76]
	ldr	r3, [r5, #92]
	bl	STATION_GROUP_copy_details_into_group_name
	mov	r0, r6
	mov	r1, r4
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L186:
	ldr	r3, .L187+4
	ldr	r0, [r3, #0]
.LBE43:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, lr}
.LBB44:
	b	xQueueGiveMutexRecursive
.L188:
	.align	2
.L187:
	.word	.LANCHOR2
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR1
	.word	nm_station_group_updater
	.word	.LANCHOR3
	.word	nm_STATION_GROUP_set_default_values
	.word	.LANCHOR0
	.word	1262
	.word	.LC51
.LBE44:
.LFE49:
	.size	init_file_station_group, .-init_file_station_group
	.section	.text.nm_station_group_updater,"ax",%progbits
	.align	2
	.type	nm_station_group_updater, %function
nm_station_group_updater:
.LFB48:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI98:
	fstmfdd	sp!, {d8}
.LCFI99:
	ldr	r3, .L232
	sub	sp, sp, #84
.LCFI100:
	str	r3, [sp, #72]	@ float
	ldr	r3, .L232+4
	mov	r5, r0
	str	r3, [sp, #76]	@ float
	ldr	r3, .L232+8
	str	r3, [sp, #80]	@ float
	bl	FLOWSENSE_get_controller_index
	cmp	r5, #7
	mov	r4, r0
	bne	.L190
	ldr	r0, .L232+12
	mov	r1, r5
	bl	Alert_Message_va
	b	.L189
.L190:
	ldr	r0, .L232+16
	mov	r1, #7
	mov	r2, r5
	bl	Alert_Message_va
	cmp	r5, #0
	bne	.L192
	ldr	r0, .L232+20
	bl	nm_ListGetFirst
	mvn	r6, #0
	mov	r7, #0
	mov	r1, r0
	b	.L193
.L194:
	str	r6, [r1, #312]
	str	r7, [r1, #316]
	ldr	r0, .L232+20
	bl	nm_ListGetNext
	mov	r1, r0
.L193:
	cmp	r1, #0
	bne	.L194
	b	.L195
.L192:
	cmp	r5, #1
	bne	.L196
.L195:
.LBB45:
	ldr	r0, .L232+20
	bl	nm_ListGetFirst
	b	.L231
.L202:
	ldr	r3, [r5, #72]
	mov	fp, #1
	sub	r3, r3, #1
	cmp	r3, #2
	ldrls	r0, .L232+24
	movhi	r7, #0
	ldrlsb	r7, [r0, r3]	@ zero_extendqisi2
	add	r6, r5, #296
	mov	r3, #11
	mov	r0, r5
	mov	r1, r7
	mov	r2, #0
	str	fp, [sp, #4]
	str	r4, [sp, #0]
	str	r6, [sp, #8]
	bl	nm_STATION_GROUP_set_plant_type
	ldr	r3, [r5, #88]
	cmp	r3, #3
	movls	fp, #0
	bls	.L199
	sub	r2, r3, #4
	cmp	r2, #2
	bls	.L199
	sub	fp, r3, #7
	cmp	fp, #5
	movhi	fp, #3
	movls	fp, #2
.L199:
	mov	r8, #1
	mov	r0, r5
	mov	r1, fp
	mov	r2, #0
	mov	r3, #11
	str	r4, [sp, #0]
	str	r8, [sp, #4]
	str	r6, [sp, #8]
	bl	nm_STATION_GROUP_set_slope_percentage
	ldr	r3, [r5, #84]
	ldr	r0, .L232+28
	mov	r2, r3, asl #2
	add	r1, r0, r2
	ldr	r0, .L232+32
	mov	sl, #0
	add	r2, r0, r2
	mov	r0, #7
	mla	r3, r0, r7, r3
	ldr	r0, .L232+36
	add	r3, r0, r3, asl #2
	ldr	r0, [r1, #0]	@ float
	ldr	r1, [r2, #0]	@ float
	ldr	r2, [r3, #0]	@ float
	bl	WATERSENSE_get_RZWWS
	flds	s15, [sp, #76]
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r4, r8}
	str	r6, [sp, #8]
	fmsr	s14, r0
	mov	r0, r5
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_soil_storage_capacity
	ldr	r3, [r5, #76]
	mov	r0, r5
	sub	r3, r3, #1
	cmp	r3, #2
	ldrls	r2, .L232+40
	movhi	r9, #0
	stmia	sp, {r4, r8}
	str	r6, [sp, #8]
	ldrlsb	r9, [r2, r3]	@ zero_extendqisi2
	mov	r2, #0
	mov	r1, r9
	mov	r3, #11
	bl	nm_STATION_GROUP_set_head_type
	ldr	r3, .L232+44
	flds	s14, [sp, #72]
	mov	r0, r5
	add	r3, r3, r9, asl #2
	flds	s15, [r3, #0]
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r4, r8}
	fmuls	s15, s14, s15
	str	r6, [sp, #8]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_precip_rate
	ldr	r3, .L232+48
	mov	r2, #12
	add	r3, r3, r7, asl #2
	flds	s16, [r3, #0]
	ldr	r3, .L232+52
	ldr	r0, [r5, #92]
	mla	r3, r2, r7, r3
	flds	s17, [r3, #4]
	bl	WATERSENSE_get_Kmc_index_based_on_exposure
	ldr	r2, .L232+56
	add	r3, r7, r7, asl #1
	fmrs	r1, s17
	add	r3, r3, r0
	add	r3, r2, r3, asl #2
	fmrs	r0, s16
	ldr	r2, [r3, #0]	@ float
	bl	WATERSENSE_get_Kl
	flds	s15, [sp, #76]
	str	r7, [sp, #20]
	fmsr	s14, r0
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r7, s15	@ int
.L201:
	mov	r0, #11
	mov	r8, #1
	stmia	sp, {r0, r4}
	mov	r1, sl
	mov	r0, r5
	mov	r2, r7
	mov	r3, #0
	add	sl, sl, r8
	str	r8, [sp, #8]
	str	r6, [sp, #12]
	bl	nm_STATION_GROUP_set_crop_coefficient
	cmp	sl, #12
	bne	.L201
	ldr	r7, [sp, #20]
	mov	r0, r5
	mov	r1, r8
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r4, r8}
	str	r6, [sp, #8]
	bl	nm_DAILY_ET_set_use_ET_averaging
	mov	r1, r7
	mov	r2, r9
	ldr	r3, [r5, #92]
	add	r0, sp, #24
	bl	STATION_GROUP_copy_details_into_group_name
	mov	sl, #0
	add	r3, r5, #312
	str	r3, [sp, #12]
	add	r1, sp, #24
	mov	r2, sl
	mov	r3, #11
	mov	r0, r5
	stmia	sp, {r4, r8}
	str	r6, [sp, #8]
	str	sl, [sp, #16]
	bl	SHARED_set_name_64_bit_change_bits
	ldr	r3, [r5, #84]
	flds	s14, [sp, #76]
	ldr	r2, .L232+28
	mov	r0, r5
	add	r3, r2, r3, asl #2
	flds	s15, [r3, #0]
	mov	r2, #11
	mov	r3, r4
	str	r8, [sp, #0]
	fmuls	s15, s14, s15
	str	r6, [sp, #4]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_allowable_depletion.constprop.8
	ldr	r3, [r5, #84]
	flds	s14, [sp, #76]
	ldr	r0, .L232+32
	mov	r2, #11
	add	r3, r0, r3, asl #2
	flds	s15, [r3, #0]
	mov	r0, r5
	mov	r3, r4
	str	r8, [sp, #0]
	fmuls	s15, s14, s15
	str	r6, [sp, #4]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_available_water.constprop.9
	ldr	r3, [r5, #84]
	flds	s14, [sp, #80]
	mov	r2, #7
	mla	r3, r2, r7, r3
	ldr	r2, .L232+36
	mov	r0, r5
	add	r3, r2, r3, asl #2
	flds	s15, [r3, #0]
	mov	r2, #11
	mov	r3, r4
	str	r8, [sp, #0]
	fmuls	s15, s14, s15
	str	r6, [sp, #4]
	add	r7, r7, r7, asl #1
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_root_zone_depth.constprop.10
	flds	s15, [sp, #76]
	mov	r0, r5
	mov	r2, #11
	mov	r3, r4
	fmuls	s16, s16, s15
	str	r8, [sp, #0]
	str	r6, [sp, #4]
	ftouizs	s16, s16
	fmrs	r1, s16	@ int
	bl	nm_STATION_GROUP_set_species_factor.constprop.11
	flds	s15, [sp, #76]
	mov	r2, #11
	mov	r3, r4
	mov	r0, r5
	fmuls	s17, s17, s15
	str	r8, [sp, #0]
	str	r6, [sp, #4]
	ftouizs	s17, s17
	fmrs	r1, s17	@ int
	bl	nm_STATION_GROUP_set_density_factor.constprop.12
	ldr	r0, [r5, #92]
	bl	WATERSENSE_get_Kmc_index_based_on_exposure
	ldr	r3, .L232+56
	flds	s14, [sp, #76]
	mov	r2, #11
	str	r8, [sp, #0]
	str	r6, [sp, #4]
	add	r7, r7, r0
	add	r7, r3, r7, asl #2
	flds	s15, [r7, #0]
	mov	r0, r5
	mov	r3, r4
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_microclimate_factor.constprop.6
	ldr	r2, [r5, #84]
	flds	s14, [sp, #76]
	ldr	r3, .L232+60
	mov	r0, r5
	add	r3, r3, r2, asl #2
	flds	s15, [r3, #0]
	mov	r2, sl
	mov	r3, #11
	stmia	sp, {r4, r8}
	fmuls	s15, s14, s15
	str	r6, [sp, #8]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_soil_intake_rate
	ldr	r3, [r5, #84]
	flds	s14, [sp, #76]
	mov	r0, r5
	add	fp, fp, r3, asl #2
	ldr	r3, .L232+64
	mov	r2, sl
	add	fp, r3, fp, asl #2
	flds	s15, [fp, #0]
	mov	r3, #11
	stmia	sp, {r4, r8}
	fmuls	s15, s14, s15
	str	r6, [sp, #8]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_GROUP_set_allowable_surface_accumulation
	ldr	r0, .L232+20
	mov	r1, r5
	bl	nm_ListGetNext
.L231:
	cmp	r0, #0
	mov	r5, r0
	bne	.L202
	b	.L203
.L196:
.LBE45:
	cmp	r5, #2
	bne	.L204
.L203:
	ldr	r0, .L232+20
	bl	nm_ListGetFirst
	mov	r6, #0
	mov	r7, #0
	mov	r1, r0
	b	.L205
.L206:
	str	r6, [r1, #352]
	str	r7, [r1, #356]
	ldr	r0, .L232+20
	bl	nm_ListGetNext
	mov	r1, r0
.L205:
	cmp	r1, #0
	bne	.L206
	b	.L207
.L204:
	cmp	r5, #3
	bne	.L208
.L207:
	ldr	r0, .L232+20
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r5, r0
	b	.L209
.L210:
	add	r3, r5, #296
	mov	r0, r5
	str	r3, [sp, #8]
	mov	r1, #25
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r4, r6}
	bl	nm_STATION_GROUP_set_budget_reduction_percentage_cap
	mov	r1, r5
	ldr	r0, .L232+20
	bl	nm_ListGetNext
	mov	r5, r0
.L209:
	cmp	r5, #0
	bne	.L210
	b	.L211
.L208:
	cmp	r5, #4
	bne	.L212
.L211:
	ldr	r0, .L232+20
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r5, r0
	b	.L213
.L214:
	mov	r0, #0
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_group_ID
	add	r7, r5, #296
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r4, r6, r7}
	mov	r1, r0
	mov	r0, r5
	bl	nm_STATION_GROUP_set_GID_irrigation_system
	mov	r0, r5
	mov	r1, #1
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r4, r6, r7}
	bl	nm_STATION_GROUP_set_in_use
	ldr	r0, .L232+20
	mov	r1, r5
	bl	nm_ListGetNext
	mov	r5, r0
.L213:
	cmp	r5, #0
	bne	.L214
	b	.L215
.L212:
	cmp	r5, #5
	bne	.L216
.L215:
	ldr	r0, .L232+20
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r5, r0
	b	.L217
.L220:
	mov	r3, #11
	add	r7, r5, #296
	mov	r0, r5
	mov	r1, #1
	mov	r2, #0
	stmia	sp, {r4, r6, r7}
	bl	nm_DAILY_ET_set_use_ET_averaging
	ldr	r3, [r5, #268]
	cmn	r3, #1
	bne	.L218
	mov	r0, r5
	mvn	r1, #-2147483648
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r4, r6, r7}
	bl	nm_ON_AT_A_TIME_set_on_at_a_time_in_group
.L218:
	ldr	r3, [r5, #272]
	cmn	r3, #1
	bne	.L219
	mov	r0, r5
	mvn	r1, #-2147483648
	mov	r2, #0
	mov	r3, #11
	stmia	sp, {r4, r6, r7}
	bl	nm_ON_AT_A_TIME_set_on_at_a_time_in_system
.L219:
	mov	r1, r5
	ldr	r0, .L232+20
	bl	nm_ListGetNext
	mov	r5, r0
.L217:
	cmp	r5, #0
	bne	.L220
	b	.L221
.L216:
	cmp	r5, #6
	bne	.L222
.L221:
	ldr	r0, .L232+20
	bl	nm_ListGetFirst
	mov	r6, #1
	mov	r5, r0
	b	.L223
.L224:
	add	r3, r5, #296
	mov	r1, #0
	mov	r0, r5
	mov	r2, r1
	str	r3, [sp, #8]
	mov	r3, #11
	stmia	sp, {r4, r6}
	bl	nm_STATION_GROUP_set_moisture_sensor_serial_number
	mov	r1, r5
	ldr	r0, .L232+20
	bl	nm_ListGetNext
	mov	r5, r0
.L223:
	cmp	r5, #0
	bne	.L224
	b	.L189
.L222:
	ldr	r0, .L232+68
	bl	Alert_Message
.L189:
	add	sp, sp, #84
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L233:
	.align	2
.L232:
	.word	1203982336
	.word	1120403456
	.word	1092616192
	.word	.LC67
	.word	.LC68
	.word	.LANCHOR1
	.word	.LANCHOR4
	.word	MAD
	.word	AW
	.word	ROOT_ZONE_DEPTH
	.word	.LANCHOR5
	.word	PR
	.word	Ks
	.word	Kd
	.word	Kmc
	.word	IR
	.word	ASA
	.word	.LC69
.LFE48:
	.size	nm_station_group_updater, .-nm_station_group_updater
	.section	.text.nm_STATION_GROUP_create_new_group,"ax",%progbits
	.align	2
	.global	nm_STATION_GROUP_create_new_group
	.type	nm_STATION_GROUP_create_new_group, %function
nm_STATION_GROUP_create_new_group:
.LFB54:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI101:
	ldr	r0, .L239
	bl	nm_ListGetFirst
	b	.L238
.L237:
	ldr	r3, [r1, #368]
	cmp	r3, #0
	beq	.L236
	ldr	r0, .L239
	bl	nm_ListGetNext
.L238:
	cmp	r0, #0
	mov	r1, r0
	bne	.L237
.L236:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r1, [sp, #4]
	ldr	r2, .L239+4
	ldr	r1, .L239+8
	mov	r3, #376
	ldr	r0, .L239
	bl	nm_GROUP_create_new_group
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	mov	r4, r0
	add	r0, r0, #20
	bl	STATION_GROUP_copy_details_into_group_name
	mov	r0, r4
	ldmfd	sp!, {r2, r3, r4, pc}
.L240:
	.align	2
.L239:
	.word	.LANCHOR1
	.word	nm_STATION_GROUP_set_default_values
	.word	.LANCHOR3
.LFE54:
	.size	nm_STATION_GROUP_create_new_group, .-nm_STATION_GROUP_create_new_group
	.section	.text.STATION_GROUP_copy_group,"ax",%progbits
	.align	2
	.global	STATION_GROUP_copy_group
	.type	STATION_GROUP_copy_group, %function
STATION_GROUP_copy_group:
.LFB55:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L244
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI102:
	ldr	r2, .L244+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L244+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, #0
	ldr	r0, .L244+12
	bl	GuiLib_GetTextPtr
	add	r6, r5, #20
	mov	r1, r0
	mov	r0, r6
	bl	strstr
	subs	r1, r0, #0
	bne	.L242
	ldr	r0, .L244+12
	bl	GuiLib_GetTextPtr
	mov	r1, #48
	ldr	r2, .L244+16
	mov	r3, r6
	str	r0, [sp, #0]
	add	r0, r4, #20
	bl	snprintf
	b	.L243
.L242:
	add	r0, r4, #20
	mov	r1, r6
	mov	r2, #48
	bl	strlcpy
.L243:
	ldr	r3, [r5, #72]
	add	r1, r5, #100
	str	r3, [r4, #72]
	ldr	r3, [r5, #76]
	add	r0, r4, #100
	str	r3, [r4, #76]
	ldr	r3, [r5, #80]
	mov	r2, #48
	str	r3, [r4, #80]
	ldr	r3, [r5, #84]
	str	r3, [r4, #84]
	ldr	r3, [r5, #88]
	str	r3, [r4, #88]
	ldr	r3, [r5, #92]
	str	r3, [r4, #92]
	ldr	r3, [r5, #96]
	str	r3, [r4, #96]
	bl	memcpy
	ldr	r3, [r5, #148]
	add	r1, r5, #184
	str	r3, [r4, #148]
	ldr	r3, [r5, #152]
	mov	r2, #28
	str	r3, [r4, #152]
	ldr	r3, [r5, #156]
	add	r0, r4, #184
	str	r3, [r4, #156]
	ldr	r3, [r5, #160]
	str	r3, [r4, #160]
	ldr	r3, [r5, #164]
	str	r3, [r4, #164]
	ldr	r3, [r5, #168]
	str	r3, [r4, #168]
	ldr	r3, [r5, #172]
	str	r3, [r4, #172]
	ldr	r3, [r5, #176]
	str	r3, [r4, #176]
	ldr	r3, [r5, #180]
	str	r3, [r4, #180]
	bl	memcpy
	ldr	r3, [r5, #212]
	add	r0, r4, #296
	str	r3, [r4, #212]
	ldr	r3, [r5, #216]
	str	r3, [r4, #216]
	ldr	r3, [r5, #240]
	str	r3, [r4, #240]
	ldr	r3, [r5, #244]
	str	r3, [r4, #244]
	ldr	r3, [r5, #248]
	str	r3, [r4, #248]
	ldr	r3, [r5, #252]
	str	r3, [r4, #252]
	ldr	r3, [r5, #256]
	str	r3, [r4, #256]
	ldr	r3, [r5, #260]
	str	r3, [r4, #260]
	ldr	r3, [r5, #264]
	str	r3, [r4, #264]
	ldr	r3, [r5, #268]
	str	r3, [r4, #268]
	ldr	r3, [r5, #272]
	str	r3, [r4, #272]
	ldr	r3, [r5, #280]
	str	r3, [r4, #280]
	ldr	r3, [r5, #284]
	str	r3, [r4, #284]
	ldr	r3, [r5, #288]
	str	r3, [r4, #288]
	ldr	r3, [r5, #292]
	str	r3, [r4, #292]
	ldr	r3, [r5, #320]
	str	r3, [r4, #320]
	ldr	r3, [r5, #324]
	str	r3, [r4, #324]
	ldr	r3, [r5, #328]
	str	r3, [r4, #328]
	ldr	r3, [r5, #332]
	str	r3, [r4, #332]
	ldr	r3, [r5, #336]
	str	r3, [r4, #336]
	ldr	r3, [r5, #340]
	str	r3, [r4, #340]
	ldr	r3, [r5, #344]
	str	r3, [r4, #344]
	ldr	r3, [r5, #348]
	str	r3, [r4, #348]
	ldr	r3, [r5, #360]
	str	r3, [r4, #360]
	ldr	r3, [r5, #364]
	str	r3, [r4, #364]
	mov	r3, #1
	str	r3, [r4, #368]
	ldr	r3, [r5, #372]
	ldr	r5, .L244
	str	r3, [r4, #372]
	ldr	r1, [r5, #0]
	bl	SHARED_set_all_64_bit_change_bits
	ldr	r0, [r5, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L245:
	.align	2
.L244:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	1521
	.word	903
	.word	.LC70
.LFE55:
	.size	STATION_GROUP_copy_group, .-STATION_GROUP_copy_group
	.section	.text.STATION_GROUP_get_group_with_this_GID,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_group_with_this_GID
	.type	STATION_GROUP_get_group_with_this_GID, %function
STATION_GROUP_get_group_with_this_GID:
.LFB103:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI103:
	ldr	r5, .L249
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L249+4
	ldr	r3, .L249+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	beq	.L247
	mov	r1, r4
	ldr	r0, .L249+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	bne	.L247
	ldr	r0, .L249+4
	bl	RemovePathFromFileName
	ldr	r2, .L249+16
	mov	r1, r0
	ldr	r0, .L249+20
	bl	Alert_Message_va
.L247:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.L250:
	.align	2
.L249:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3999
	.word	.LANCHOR1
	.word	4010
	.word	.LC71
.LFE103:
	.size	STATION_GROUP_get_group_with_this_GID, .-STATION_GROUP_get_group_with_this_GID
	.section	.text.STATION_GROUP_get_group_at_this_index,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_group_at_this_index
	.type	STATION_GROUP_get_group_at_this_index, %function
STATION_GROUP_get_group_at_this_index:
.LFB104:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI104:
	ldr	r4, .L252
	ldr	r2, .L252+4
	ldr	r3, .L252+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L252+12
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L253:
	.align	2
.L252:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4028
	.word	.LANCHOR1
.LFE104:
	.size	STATION_GROUP_get_group_at_this_index, .-STATION_GROUP_get_group_at_this_index
	.section	.text.ACQUIRE_EXPECTEDS_fill_guivars,"ax",%progbits
	.align	2
	.type	ACQUIRE_EXPECTEDS_fill_guivars, %function
ACQUIRE_EXPECTEDS_fill_guivars:
.LFB75:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI105:
	mov	r4, r3
	ldr	r3, .L258
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	ldr	r3, .L258+4
	mov	r1, #400
	ldr	r2, .L258+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L258+12
	ldr	r3, [r3, #8]
	cmp	r3, r5
	movls	r3, #0
	bls	.L257
	mov	r0, r5
	bl	STATION_GROUP_get_group_at_this_index
	mov	r5, r0
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r5, #292]
	str	r3, [r6, #0]
	mov	r3, #1
.L257:
	str	r3, [r4, #0]
	ldr	r3, .L258
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L259:
	.align	2
.L258:
	.word	list_program_data_recursive_MUTEX
	.word	3124
	.word	.LC51
	.word	.LANCHOR1
.LFE75:
	.size	ACQUIRE_EXPECTEDS_fill_guivars, .-ACQUIRE_EXPECTEDS_fill_guivars
	.section	.text.ACQUIRE_EXPECTEDS_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	ACQUIRE_EXPECTEDS_copy_group_into_guivars
	.type	ACQUIRE_EXPECTEDS_copy_group_into_guivars, %function
ACQUIRE_EXPECTEDS_copy_group_into_guivars:
.LFB76:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI106:
	ldr	r4, .L261
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L261+4
	ldr	r3, .L261+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	ldr	r1, .L261+12
	ldr	r2, .L261+16
	ldr	r3, .L261+20
	bl	ACQUIRE_EXPECTEDS_fill_guivars
	mov	r0, #1
	ldr	r1, .L261+24
	ldr	r2, .L261+28
	ldr	r3, .L261+32
	bl	ACQUIRE_EXPECTEDS_fill_guivars
	mov	r0, #2
	ldr	r1, .L261+36
	ldr	r2, .L261+40
	ldr	r3, .L261+44
	bl	ACQUIRE_EXPECTEDS_fill_guivars
	mov	r0, #3
	ldr	r1, .L261+48
	ldr	r2, .L261+52
	ldr	r3, .L261+56
	bl	ACQUIRE_EXPECTEDS_fill_guivars
	mov	r0, #4
	ldr	r1, .L261+60
	ldr	r2, .L261+64
	ldr	r3, .L261+68
	bl	ACQUIRE_EXPECTEDS_fill_guivars
	mov	r0, #5
	ldr	r1, .L261+72
	ldr	r2, .L261+76
	ldr	r3, .L261+80
	bl	ACQUIRE_EXPECTEDS_fill_guivars
	mov	r0, #6
	ldr	r1, .L261+84
	ldr	r2, .L261+88
	ldr	r3, .L261+92
	bl	ACQUIRE_EXPECTEDS_fill_guivars
	mov	r0, #7
	ldr	r1, .L261+96
	ldr	r2, .L261+100
	ldr	r3, .L261+104
	bl	ACQUIRE_EXPECTEDS_fill_guivars
	mov	r0, #8
	ldr	r1, .L261+108
	ldr	r2, .L261+112
	ldr	r3, .L261+116
	bl	ACQUIRE_EXPECTEDS_fill_guivars
	mov	r0, #9
	ldr	r1, .L261+120
	ldr	r2, .L261+124
	ldr	r3, .L261+128
	bl	ACQUIRE_EXPECTEDS_fill_guivars
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L262:
	.align	2
.L261:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3151
	.word	GuiVar_GroupName_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupName_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupName_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupName_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupName_4
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupName_5
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupName_6
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupName_7
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupName_8
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupName_9
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE76:
	.size	ACQUIRE_EXPECTEDS_copy_group_into_guivars, .-ACQUIRE_EXPECTEDS_copy_group_into_guivars
	.section	.text.DELAY_BETWEEN_VALVES_fill_guivars,"ax",%progbits
	.align	2
	.type	DELAY_BETWEEN_VALVES_fill_guivars, %function
DELAY_BETWEEN_VALVES_fill_guivars:
.LFB73:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI107:
	mov	r4, r3
	ldr	r3, .L268
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	ldr	r3, .L268+4
	mov	r1, #400
	ldr	r2, .L268+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L268+12
	ldr	r3, [r3, #8]
	cmp	r3, r5
	movls	r3, #0
	bls	.L267
	mov	r0, r5
	bl	STATION_GROUP_get_group_at_this_index
	mov	r5, r0
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r5, #288]
	cmp	r3, #0
	ldrne	r2, .L268+16
	str	r3, [r6, #0]
	mov	r3, #1
	strne	r3, [r2, #0]
.L267:
	str	r3, [r4, #0]
	ldr	r3, .L268
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L269:
	.align	2
.L268:
	.word	list_program_data_recursive_MUTEX
	.word	3065
	.word	.LC51
	.word	.LANCHOR1
	.word	GuiVar_DelayBetweenValvesInUse
.LFE73:
	.size	DELAY_BETWEEN_VALVES_fill_guivars, .-DELAY_BETWEEN_VALVES_fill_guivars
	.section	.text.DELAY_BETWEEN_VALVES_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	DELAY_BETWEEN_VALVES_copy_group_into_guivars
	.type	DELAY_BETWEEN_VALVES_copy_group_into_guivars, %function
DELAY_BETWEEN_VALVES_copy_group_into_guivars:
.LFB74:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI108:
	ldr	r4, .L271
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L271+4
	ldr	r3, .L271+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L271+12
	mov	r0, #0
	str	r0, [r3, #0]
	ldr	r1, .L271+16
	ldr	r2, .L271+20
	ldr	r3, .L271+24
	bl	DELAY_BETWEEN_VALVES_fill_guivars
	mov	r0, #1
	ldr	r1, .L271+28
	ldr	r2, .L271+32
	ldr	r3, .L271+36
	bl	DELAY_BETWEEN_VALVES_fill_guivars
	mov	r0, #2
	ldr	r1, .L271+40
	ldr	r2, .L271+44
	ldr	r3, .L271+48
	bl	DELAY_BETWEEN_VALVES_fill_guivars
	mov	r0, #3
	ldr	r1, .L271+52
	ldr	r2, .L271+56
	ldr	r3, .L271+60
	bl	DELAY_BETWEEN_VALVES_fill_guivars
	mov	r0, #4
	ldr	r1, .L271+64
	ldr	r2, .L271+68
	ldr	r3, .L271+72
	bl	DELAY_BETWEEN_VALVES_fill_guivars
	mov	r0, #5
	ldr	r1, .L271+76
	ldr	r2, .L271+80
	ldr	r3, .L271+84
	bl	DELAY_BETWEEN_VALVES_fill_guivars
	mov	r0, #6
	ldr	r1, .L271+88
	ldr	r2, .L271+92
	ldr	r3, .L271+96
	bl	DELAY_BETWEEN_VALVES_fill_guivars
	mov	r0, #7
	ldr	r1, .L271+100
	ldr	r2, .L271+104
	ldr	r3, .L271+108
	bl	DELAY_BETWEEN_VALVES_fill_guivars
	mov	r0, #8
	ldr	r1, .L271+112
	ldr	r2, .L271+116
	ldr	r3, .L271+120
	bl	DELAY_BETWEEN_VALVES_fill_guivars
	mov	r0, #9
	ldr	r1, .L271+124
	ldr	r2, .L271+128
	ldr	r3, .L271+132
	bl	DELAY_BETWEEN_VALVES_fill_guivars
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L272:
	.align	2
.L271:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3097
	.word	GuiVar_DelayBetweenValvesInUse
	.word	GuiVar_GroupName_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupName_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupName_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupName_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupName_4
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupName_5
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupName_6
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupName_7
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupName_8
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupName_9
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE74:
	.size	DELAY_BETWEEN_VALVES_copy_group_into_guivars, .-DELAY_BETWEEN_VALVES_copy_group_into_guivars
	.section	.text.LINE_FILL_TIME_fill_guivars,"ax",%progbits
	.align	2
	.type	LINE_FILL_TIME_fill_guivars, %function
LINE_FILL_TIME_fill_guivars:
.LFB71:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI109:
	mov	r4, r3
	ldr	r3, .L278
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	ldr	r3, .L278+4
	mov	r1, #400
	ldr	r2, .L278+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L278+12
	ldr	r3, [r3, #8]
	cmp	r3, r5
	movls	r3, #0
	bls	.L277
	mov	r0, r5
	bl	STATION_GROUP_get_group_at_this_index
	ldr	r3, [r0, #368]
	mov	r5, r0
	cmp	r3, #0
	beq	.L277
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r5, #284]
	str	r3, [r6, #0]
	mov	r3, #1
.L277:
	str	r3, [r4, #0]
	ldr	r3, .L278
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L279:
	.align	2
.L278:
	.word	list_program_data_recursive_MUTEX
	.word	3009
	.word	.LC51
	.word	.LANCHOR1
.LFE71:
	.size	LINE_FILL_TIME_fill_guivars, .-LINE_FILL_TIME_fill_guivars
	.section	.text.LINE_FILL_TIME_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	LINE_FILL_TIME_copy_group_into_guivars
	.type	LINE_FILL_TIME_copy_group_into_guivars, %function
LINE_FILL_TIME_copy_group_into_guivars:
.LFB72:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI110:
	ldr	r4, .L281
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L281+4
	ldr	r3, .L281+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	ldr	r1, .L281+12
	ldr	r2, .L281+16
	ldr	r3, .L281+20
	bl	LINE_FILL_TIME_fill_guivars
	mov	r0, #1
	ldr	r1, .L281+24
	ldr	r2, .L281+28
	ldr	r3, .L281+32
	bl	LINE_FILL_TIME_fill_guivars
	mov	r0, #2
	ldr	r1, .L281+36
	ldr	r2, .L281+40
	ldr	r3, .L281+44
	bl	LINE_FILL_TIME_fill_guivars
	mov	r0, #3
	ldr	r1, .L281+48
	ldr	r2, .L281+52
	ldr	r3, .L281+56
	bl	LINE_FILL_TIME_fill_guivars
	mov	r0, #4
	ldr	r1, .L281+60
	ldr	r2, .L281+64
	ldr	r3, .L281+68
	bl	LINE_FILL_TIME_fill_guivars
	mov	r0, #5
	ldr	r1, .L281+72
	ldr	r2, .L281+76
	ldr	r3, .L281+80
	bl	LINE_FILL_TIME_fill_guivars
	mov	r0, #6
	ldr	r1, .L281+84
	ldr	r2, .L281+88
	ldr	r3, .L281+92
	bl	LINE_FILL_TIME_fill_guivars
	mov	r0, #7
	ldr	r1, .L281+96
	ldr	r2, .L281+100
	ldr	r3, .L281+104
	bl	LINE_FILL_TIME_fill_guivars
	mov	r0, #8
	ldr	r1, .L281+108
	ldr	r2, .L281+112
	ldr	r3, .L281+116
	bl	LINE_FILL_TIME_fill_guivars
	mov	r0, #9
	ldr	r1, .L281+120
	ldr	r2, .L281+124
	ldr	r3, .L281+128
	bl	LINE_FILL_TIME_fill_guivars
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L282:
	.align	2
.L281:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3044
	.word	GuiVar_GroupName_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupName_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupName_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupName_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupName_4
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupName_5
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupName_6
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupName_7
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupName_8
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupName_9
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE72:
	.size	LINE_FILL_TIME_copy_group_into_guivars, .-LINE_FILL_TIME_copy_group_into_guivars
	.section	.text.PUMP_fill_guivars,"ax",%progbits
	.align	2
	.type	PUMP_fill_guivars, %function
PUMP_fill_guivars:
.LFB69:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI111:
	mov	r4, r3
	ldr	r3, .L288
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	ldr	r3, .L288+4
	mov	r1, #400
	ldr	r2, .L288+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L288+12
	ldr	r3, [r3, #8]
	cmp	r3, r5
	movls	r3, #0
	bls	.L287
	mov	r0, r5
	bl	STATION_GROUP_get_group_at_this_index
	ldr	r3, [r0, #368]
	mov	r5, r0
	cmp	r3, #0
	beq	.L287
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r5, #280]
	str	r3, [r6, #0]
	mov	r3, #1
.L287:
	str	r3, [r4, #0]
	ldr	r3, .L288
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L289:
	.align	2
.L288:
	.word	list_program_data_recursive_MUTEX
	.word	2952
	.word	.LC51
	.word	.LANCHOR1
.LFE69:
	.size	PUMP_fill_guivars, .-PUMP_fill_guivars
	.section	.text.PUMP_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	PUMP_copy_group_into_guivars
	.type	PUMP_copy_group_into_guivars, %function
PUMP_copy_group_into_guivars:
.LFB70:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI112:
	ldr	r4, .L291
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L291+4
	ldr	r3, .L291+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	ldr	r1, .L291+12
	ldr	r2, .L291+16
	ldr	r3, .L291+20
	bl	PUMP_fill_guivars
	mov	r0, #1
	ldr	r1, .L291+24
	ldr	r2, .L291+28
	ldr	r3, .L291+32
	bl	PUMP_fill_guivars
	mov	r0, #2
	ldr	r1, .L291+36
	ldr	r2, .L291+40
	ldr	r3, .L291+44
	bl	PUMP_fill_guivars
	mov	r0, #3
	ldr	r1, .L291+48
	ldr	r2, .L291+52
	ldr	r3, .L291+56
	bl	PUMP_fill_guivars
	mov	r0, #4
	ldr	r1, .L291+60
	ldr	r2, .L291+64
	ldr	r3, .L291+68
	bl	PUMP_fill_guivars
	mov	r0, #5
	ldr	r1, .L291+72
	ldr	r2, .L291+76
	ldr	r3, .L291+80
	bl	PUMP_fill_guivars
	mov	r0, #6
	ldr	r1, .L291+84
	ldr	r2, .L291+88
	ldr	r3, .L291+92
	bl	PUMP_fill_guivars
	mov	r0, #7
	ldr	r1, .L291+96
	ldr	r2, .L291+100
	ldr	r3, .L291+104
	bl	PUMP_fill_guivars
	mov	r0, #8
	ldr	r1, .L291+108
	ldr	r2, .L291+112
	ldr	r3, .L291+116
	bl	PUMP_fill_guivars
	mov	r0, #9
	ldr	r1, .L291+120
	ldr	r2, .L291+124
	ldr	r3, .L291+128
	bl	PUMP_fill_guivars
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L292:
	.align	2
.L291:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	2988
	.word	GuiVar_GroupName_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupName_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupName_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupName_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupName_4
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupName_5
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupName_6
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupName_7
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupName_8
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupName_9
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE70:
	.size	PUMP_copy_group_into_guivars, .-PUMP_copy_group_into_guivars
	.section	.text.ON_AT_A_TIME_fill_guivars,"ax",%progbits
	.align	2
	.type	ON_AT_A_TIME_fill_guivars, %function
ON_AT_A_TIME_fill_guivars:
.LFB67:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI113:
	ldr	r4, [sp, #24]
	mov	r8, r3
	ldr	r3, .L298
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	ldr	r3, .L298+4
	mov	r1, #400
	ldr	r2, .L298+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L298+12
	ldr	r3, [r3, #8]
	cmp	r3, r5
	movls	r3, #0
	bls	.L297
	mov	r0, r5
	bl	STATION_GROUP_get_group_at_this_index
	ldr	r3, [r0, #368]
	mov	r5, r0
	cmp	r3, #0
	beq	.L297
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r5, #268]
	str	r3, [r6, #0]
	ldr	r3, [r5, #272]
	str	r3, [r8, #0]
	mov	r3, #1
.L297:
	str	r3, [r4, #0]
	ldr	r3, .L298
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L299:
	.align	2
.L298:
	.word	list_program_data_recursive_MUTEX
	.word	2894
	.word	.LC51
	.word	.LANCHOR1
.LFE67:
	.size	ON_AT_A_TIME_fill_guivars, .-ON_AT_A_TIME_fill_guivars
	.section	.text.ON_AT_A_TIME_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	ON_AT_A_TIME_copy_group_into_guivars
	.type	ON_AT_A_TIME_copy_group_into_guivars, %function
ON_AT_A_TIME_copy_group_into_guivars:
.LFB68:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI114:
	ldr	r4, .L301
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L301+4
	ldr	r3, .L301+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L301+12
	mov	r0, #0
	str	r3, [sp, #0]
	ldr	r1, .L301+16
	ldr	r2, .L301+20
	ldr	r3, .L301+24
	bl	ON_AT_A_TIME_fill_guivars
	ldr	r3, .L301+28
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r1, .L301+32
	ldr	r2, .L301+36
	ldr	r3, .L301+40
	bl	ON_AT_A_TIME_fill_guivars
	ldr	r3, .L301+44
	mov	r0, #2
	str	r3, [sp, #0]
	ldr	r1, .L301+48
	ldr	r2, .L301+52
	ldr	r3, .L301+56
	bl	ON_AT_A_TIME_fill_guivars
	ldr	r3, .L301+60
	mov	r0, #3
	str	r3, [sp, #0]
	ldr	r1, .L301+64
	ldr	r2, .L301+68
	ldr	r3, .L301+72
	bl	ON_AT_A_TIME_fill_guivars
	ldr	r3, .L301+76
	mov	r0, #4
	str	r3, [sp, #0]
	ldr	r1, .L301+80
	ldr	r2, .L301+84
	ldr	r3, .L301+88
	bl	ON_AT_A_TIME_fill_guivars
	ldr	r3, .L301+92
	mov	r0, #5
	str	r3, [sp, #0]
	ldr	r1, .L301+96
	ldr	r2, .L301+100
	ldr	r3, .L301+104
	bl	ON_AT_A_TIME_fill_guivars
	ldr	r3, .L301+108
	mov	r0, #6
	str	r3, [sp, #0]
	ldr	r1, .L301+112
	ldr	r2, .L301+116
	ldr	r3, .L301+120
	bl	ON_AT_A_TIME_fill_guivars
	ldr	r3, .L301+124
	mov	r0, #7
	str	r3, [sp, #0]
	ldr	r1, .L301+128
	ldr	r2, .L301+132
	ldr	r3, .L301+136
	bl	ON_AT_A_TIME_fill_guivars
	ldr	r3, .L301+140
	mov	r0, #8
	str	r3, [sp, #0]
	ldr	r1, .L301+144
	ldr	r2, .L301+148
	ldr	r3, .L301+152
	bl	ON_AT_A_TIME_fill_guivars
	ldr	r3, .L301+156
	mov	r0, #9
	str	r3, [sp, #0]
	ldr	r1, .L301+160
	ldr	r2, .L301+164
	ldr	r3, .L301+168
	bl	ON_AT_A_TIME_fill_guivars
	ldr	r0, [r4, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L302:
	.align	2
.L301:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	2931
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupName_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupName_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupName_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupName_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupName_4
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupName_5
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupName_6
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupName_7
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupName_8
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSetting_Visible_9
	.word	GuiVar_GroupName_9
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSettingB_9
.LFE68:
	.size	ON_AT_A_TIME_copy_group_into_guivars, .-ON_AT_A_TIME_copy_group_into_guivars
	.section	.text.ALERT_ACTIONS_fill_guivars,"ax",%progbits
	.align	2
	.type	ALERT_ACTIONS_fill_guivars, %function
ALERT_ACTIONS_fill_guivars:
.LFB65:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI115:
	ldr	r4, [sp, #24]
	mov	r8, r3
	ldr	r3, .L308
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	ldr	r3, .L308+4
	mov	r1, #400
	ldr	r2, .L308+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L308+12
	ldr	r3, [r3, #8]
	cmp	r3, r5
	movls	r3, #0
	bls	.L307
	mov	r0, r5
	bl	STATION_GROUP_get_group_at_this_index
	ldr	r3, [r0, #368]
	mov	r5, r0
	cmp	r3, #0
	beq	.L307
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r5, #260]
	str	r3, [r6, #0]
	ldr	r3, [r5, #264]
	str	r3, [r8, #0]
	mov	r3, #1
.L307:
	str	r3, [r4, #0]
	ldr	r3, .L308
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L309:
	.align	2
.L308:
	.word	list_program_data_recursive_MUTEX
	.word	2837
	.word	.LC51
	.word	.LANCHOR1
.LFE65:
	.size	ALERT_ACTIONS_fill_guivars, .-ALERT_ACTIONS_fill_guivars
	.section	.text.ALERT_ACTIONS_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	ALERT_ACTIONS_copy_group_into_guivars
	.type	ALERT_ACTIONS_copy_group_into_guivars, %function
ALERT_ACTIONS_copy_group_into_guivars:
.LFB66:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI116:
	ldr	r4, .L311
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L311+4
	ldr	r3, .L311+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L311+12
	mov	r0, #0
	str	r3, [sp, #0]
	ldr	r1, .L311+16
	ldr	r2, .L311+20
	ldr	r3, .L311+24
	bl	ALERT_ACTIONS_fill_guivars
	ldr	r3, .L311+28
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r1, .L311+32
	ldr	r2, .L311+36
	ldr	r3, .L311+40
	bl	ALERT_ACTIONS_fill_guivars
	ldr	r3, .L311+44
	mov	r0, #2
	str	r3, [sp, #0]
	ldr	r1, .L311+48
	ldr	r2, .L311+52
	ldr	r3, .L311+56
	bl	ALERT_ACTIONS_fill_guivars
	ldr	r3, .L311+60
	mov	r0, #3
	str	r3, [sp, #0]
	ldr	r1, .L311+64
	ldr	r2, .L311+68
	ldr	r3, .L311+72
	bl	ALERT_ACTIONS_fill_guivars
	ldr	r3, .L311+76
	mov	r0, #4
	str	r3, [sp, #0]
	ldr	r1, .L311+80
	ldr	r2, .L311+84
	ldr	r3, .L311+88
	bl	ALERT_ACTIONS_fill_guivars
	ldr	r3, .L311+92
	mov	r0, #5
	str	r3, [sp, #0]
	ldr	r1, .L311+96
	ldr	r2, .L311+100
	ldr	r3, .L311+104
	bl	ALERT_ACTIONS_fill_guivars
	ldr	r3, .L311+108
	mov	r0, #6
	str	r3, [sp, #0]
	ldr	r1, .L311+112
	ldr	r2, .L311+116
	ldr	r3, .L311+120
	bl	ALERT_ACTIONS_fill_guivars
	ldr	r3, .L311+124
	mov	r0, #7
	str	r3, [sp, #0]
	ldr	r1, .L311+128
	ldr	r2, .L311+132
	ldr	r3, .L311+136
	bl	ALERT_ACTIONS_fill_guivars
	ldr	r3, .L311+140
	mov	r0, #8
	str	r3, [sp, #0]
	ldr	r1, .L311+144
	ldr	r2, .L311+148
	ldr	r3, .L311+152
	bl	ALERT_ACTIONS_fill_guivars
	ldr	r3, .L311+156
	mov	r0, #9
	str	r3, [sp, #0]
	ldr	r1, .L311+160
	ldr	r2, .L311+164
	ldr	r3, .L311+168
	bl	ALERT_ACTIONS_fill_guivars
	ldr	r0, [r4, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L312:
	.align	2
.L311:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	2873
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupName_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupName_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupName_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupName_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupName_4
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupName_5
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupName_6
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupName_7
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupName_8
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSetting_Visible_9
	.word	GuiVar_GroupName_9
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSettingB_9
.LFE66:
	.size	ALERT_ACTIONS_copy_group_into_guivars, .-ALERT_ACTIONS_copy_group_into_guivars
	.section	.text.WEATHER_fill_guivars,"ax",%progbits
	.align	2
	.type	WEATHER_fill_guivars, %function
WEATHER_fill_guivars:
.LFB63:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI117:
	ldr	sl, [sp, #28]
	mov	r8, r3
	ldr	r3, .L318
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	ldr	r3, .L318+4
	mov	r1, #400
	ldr	r2, .L318+8
	ldr	r4, [sp, #32]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L318+12
	ldr	r3, [r3, #8]
	cmp	r3, r5
	movls	r3, #0
	bls	.L317
	mov	r0, r5
	bl	STATION_GROUP_get_group_at_this_index
	ldr	r3, [r0, #368]
	mov	r5, r0
	cmp	r3, #0
	beq	.L317
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r5, #240]
	str	r3, [r6, #0]
	ldr	r3, [r5, #248]
	str	r3, [r8, #0]
	ldr	r3, [r5, #256]
	str	r3, [sl, #0]
	mov	r3, #1
.L317:
	str	r3, [r4, #0]
	ldr	r3, .L318
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L319:
	.align	2
.L318:
	.word	list_program_data_recursive_MUTEX
	.word	2776
	.word	.LC51
	.word	.LANCHOR1
.LFE63:
	.size	WEATHER_fill_guivars, .-WEATHER_fill_guivars
	.section	.text.WEATHER_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	WEATHER_copy_group_into_guivars
	.type	WEATHER_copy_group_into_guivars, %function
WEATHER_copy_group_into_guivars:
.LFB64:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI118:
	ldr	r4, .L321
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L321+4
	mov	r3, #2816
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L321+8
	mov	r0, #0
	str	r3, [sp, #0]
	ldr	r3, .L321+12
	ldr	r1, .L321+16
	str	r3, [sp, #4]
	ldr	r2, .L321+20
	ldr	r3, .L321+24
	bl	WEATHER_fill_guivars
	ldr	r3, .L321+28
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, .L321+32
	ldr	r1, .L321+36
	str	r3, [sp, #4]
	ldr	r2, .L321+40
	ldr	r3, .L321+44
	bl	WEATHER_fill_guivars
	ldr	r3, .L321+48
	mov	r0, #2
	str	r3, [sp, #0]
	ldr	r3, .L321+52
	ldr	r1, .L321+56
	str	r3, [sp, #4]
	ldr	r2, .L321+60
	ldr	r3, .L321+64
	bl	WEATHER_fill_guivars
	ldr	r3, .L321+68
	mov	r0, #3
	str	r3, [sp, #0]
	ldr	r3, .L321+72
	ldr	r1, .L321+76
	str	r3, [sp, #4]
	ldr	r2, .L321+80
	ldr	r3, .L321+84
	bl	WEATHER_fill_guivars
	ldr	r3, .L321+88
	mov	r0, #4
	str	r3, [sp, #0]
	ldr	r3, .L321+92
	ldr	r1, .L321+96
	str	r3, [sp, #4]
	ldr	r2, .L321+100
	ldr	r3, .L321+104
	bl	WEATHER_fill_guivars
	ldr	r3, .L321+108
	mov	r0, #5
	str	r3, [sp, #0]
	ldr	r3, .L321+112
	ldr	r1, .L321+116
	str	r3, [sp, #4]
	ldr	r2, .L321+120
	ldr	r3, .L321+124
	bl	WEATHER_fill_guivars
	ldr	r3, .L321+128
	mov	r0, #6
	str	r3, [sp, #0]
	ldr	r3, .L321+132
	ldr	r1, .L321+136
	str	r3, [sp, #4]
	ldr	r2, .L321+140
	ldr	r3, .L321+144
	bl	WEATHER_fill_guivars
	ldr	r3, .L321+148
	mov	r0, #7
	str	r3, [sp, #0]
	ldr	r3, .L321+152
	ldr	r1, .L321+156
	str	r3, [sp, #4]
	ldr	r2, .L321+160
	ldr	r3, .L321+164
	bl	WEATHER_fill_guivars
	ldr	r3, .L321+168
	mov	r0, #8
	str	r3, [sp, #0]
	ldr	r3, .L321+172
	ldr	r1, .L321+176
	str	r3, [sp, #4]
	ldr	r2, .L321+180
	ldr	r3, .L321+184
	bl	WEATHER_fill_guivars
	ldr	r3, .L321+188
	mov	r0, #9
	str	r3, [sp, #0]
	ldr	r3, .L321+192
	ldr	r1, .L321+196
	str	r3, [sp, #4]
	ldr	r2, .L321+200
	ldr	r3, .L321+204
	bl	WEATHER_fill_guivars
	ldr	r0, [r4, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L322:
	.align	2
.L321:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupName_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupName_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupName_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingC_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupName_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingC_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupName_4
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSettingC_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupName_5
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSettingC_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupName_6
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSettingC_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupName_7
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSettingC_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupName_8
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSettingC_9
	.word	GuiVar_GroupSetting_Visible_9
	.word	GuiVar_GroupName_9
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSettingB_9
.LFE64:
	.size	WEATHER_copy_group_into_guivars, .-WEATHER_copy_group_into_guivars
	.global	__udivsi3
	.section	.text.SCHEDULE_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	SCHEDULE_copy_group_into_guivars
	.type	SCHEDULE_copy_group_into_guivars, %function
SCHEDULE_copy_group_into_guivars:
.LFB62:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L328
	mov	r2, #1
	stmfd	sp!, {r4, r5, lr}
.LCFI119:
	str	r2, [r3, #0]
	ldr	r3, .L328+4
	mov	r4, r0
	sub	sp, sp, #60
.LCFI120:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L328+8
	ldr	r3, .L328+12
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	subs	r4, r0, #0
	beq	.L324
	bl	nm_GROUP_load_common_guivars
	ldr	r0, [r4, #172]
	mov	r1, #60
	bl	__udivsi3
	ldr	r3, .L328+16
	mov	r1, #60
	str	r0, [r3, #0]
	ldr	r0, [r4, #176]
	bl	__udivsi3
	ldr	r3, .L328+20
	ldr	r2, [r4, #164]
	str	r0, [r3, #0]
	ldr	r3, .L328+24
	cmp	r0, #1440
	str	r2, [r3, #0]
	ldr	r3, .L328+28
	movcs	r0, #0
	movcc	r0, #1
	str	r0, [r3, #0]
	ldr	r2, [r4, #168]
	ldr	r3, .L328+32
	add	r0, sp, #52
	str	r2, [r3, #0]
	ldr	r2, [r4, #216]
	ldr	r3, .L328+36
	str	r2, [r3, #0]
	ldr	r2, [r4, #184]
	ldr	r3, .L328+40
	str	r2, [r3, #0]
	ldr	r2, [r4, #188]
	ldr	r3, .L328+44
	str	r2, [r3, #0]
	ldr	r2, [r4, #192]
	ldr	r3, .L328+48
	str	r2, [r3, #0]
	ldr	r2, [r4, #196]
	ldr	r3, .L328+52
	str	r2, [r3, #0]
	ldr	r2, [r4, #200]
	ldr	r3, .L328+56
	str	r2, [r3, #0]
	ldr	r2, [r4, #204]
	ldr	r3, .L328+60
	str	r2, [r3, #0]
	ldr	r2, [r4, #208]
	ldr	r3, .L328+64
	str	r2, [r3, #0]
	ldr	r2, [r4, #212]
	ldr	r3, .L328+68
	str	r2, [r3, #0]
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, [r4, #180]
	ldrh	r2, [sp, #56]
	mov	r1, #48
	cmp	r2, r3
	movcc	r2, r3
	ldr	r3, .L328+72
	add	r0, sp, #4
	str	r2, [r3, #0]
	mov	r3, #200
	str	r3, [sp, #0]
	mov	r3, #100
	bl	GetDateStr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L328+76
	bl	strlcpy
	ldr	r2, [r4, #240]
	ldr	r3, .L328+80
	ldr	r0, .L328+84
	str	r2, [r3, #0]
	ldr	r2, [r4, #244]
	ldr	r3, .L328+88
	ldr	r4, .L328+92
	str	r2, [r3, #0]
	bl	nm_ListGetFirst
	mov	r5, r0
	b	.L325
.L327:
	mov	r0, r5
	bl	STATION_station_is_available_for_use
	cmp	r0, #1
	bne	.L326
	mov	r0, r5
	bl	STATION_get_GID_station_group
	ldr	r3, [r4, #0]
	cmp	r0, r3
	ldreq	r3, .L328
	moveq	r2, #0
	streq	r2, [r3, #0]
	beq	.L324
.L326:
	mov	r1, r5
	ldr	r0, .L328+84
	bl	nm_ListGetNext
	mov	r5, r0
.L325:
	cmp	r5, #0
	bne	.L327
.L324:
	ldr	r3, .L328+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, pc}
.L329:
	.align	2
.L328:
	.word	GuiVar_ScheduleNoStationsInGroup
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	2696
	.word	GuiVar_ScheduleStartTime
	.word	GuiVar_ScheduleStopTime
	.word	GuiVar_ScheduleStartTimeEnabled
	.word	GuiVar_ScheduleStopTimeEnabled
	.word	GuiVar_ScheduleType
	.word	GuiVar_ScheduleMowDay
	.word	GuiVar_ScheduleWaterDay_Sun
	.word	GuiVar_ScheduleWaterDay_Mon
	.word	GuiVar_ScheduleWaterDay_Tue
	.word	GuiVar_ScheduleWaterDay_Wed
	.word	GuiVar_ScheduleWaterDay_Thu
	.word	GuiVar_ScheduleWaterDay_Fri
	.word	GuiVar_ScheduleWaterDay_Sat
	.word	GuiVar_ScheduleIrrigateOn29thOr31st
	.word	GuiVar_ScheduleBeginDate
	.word	GuiVar_ScheduleBeginDateStr
	.word	GuiVar_ScheduleUsesET
	.word	station_info_list_hdr
	.word	GuiVar_ScheduleUseETAveraging
	.word	g_GROUP_ID
.LFE62:
	.size	SCHEDULE_copy_group_into_guivars, .-SCHEDULE_copy_group_into_guivars
	.section	.text.PERCENT_ADJUST_fill_guivars,"ax",%progbits
	.align	2
	.type	PERCENT_ADJUST_fill_guivars, %function
PERCENT_ADJUST_fill_guivars:
.LFB60:
	@ args = 12, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI121:
	mov	r5, r3
	ldr	r3, .L338
	sub	sp, sp, #60
.LCFI122:
	mov	r6, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r8, r2
	ldr	r3, .L338+4
	mov	r1, #400
	ldr	r2, .L338+8
	ldr	r4, [sp, #92]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L338+12
	ldr	r3, [r3, #8]
	cmp	r3, r6
	movls	r3, #0
	bls	.L337
	mov	r0, r6
	bl	STATION_GROUP_get_group_at_this_index
	ldr	r3, [r0, #368]
	mov	r6, r0
	cmp	r3, #0
	beq	.L337
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r6, #152]
	ldr	r2, [sp, #84]
	sub	r3, r3, #100
	str	r3, [r8, #0]
	adds	r3, r3, #0
	movne	r3, #1
	str	r3, [r2, #0]
	add	r0, sp, #52
	bl	EPSON_obtain_latest_time_and_date
	ldrh	r3, [sp, #56]
	ldr	r2, [r6, #156]
	cmp	r3, r2
	bcc	.L333
	ldr	r2, [r6, #160]
	cmp	r3, r2
	rsbls	r2, r3, r2
	bls	.L336
.L333:
	mov	r2, #0
.L336:
	str	r2, [r5, #0]
	ldr	r2, [r5, #0]
	mov	r1, #225
	add	r2, r3, r2
	str	r1, [sp, #0]
	mov	r3, #125
	mov	r1, #48
	add	r0, sp, #4
	bl	GetDateStr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, [sp, #88]
	bl	strlcpy
	mov	r3, #1
.L337:
	str	r3, [r4, #0]
	ldr	r3, .L338
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L339:
	.align	2
.L338:
	.word	list_program_data_recursive_MUTEX
	.word	2612
	.word	.LC51
	.word	.LANCHOR1
.LFE60:
	.size	PERCENT_ADJUST_fill_guivars, .-PERCENT_ADJUST_fill_guivars
	.section	.text.PERCENT_ADJUST_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	PERCENT_ADJUST_copy_group_into_guivars
	.type	PERCENT_ADJUST_copy_group_into_guivars, %function
PERCENT_ADJUST_copy_group_into_guivars:
.LFB61:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L341
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI123:
	ldr	r1, .L341+4
	str	r3, [sp, #0]
	ldr	r3, .L341+8
	mov	r0, #0
	str	r3, [sp, #4]
	ldr	r3, .L341+12
	ldr	r2, .L341+16
	str	r3, [sp, #8]
	ldr	r3, .L341+20
	bl	PERCENT_ADJUST_fill_guivars
	ldr	r3, .L341+24
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, .L341+28
	ldr	r1, .L341+32
	str	r3, [sp, #4]
	ldr	r3, .L341+36
	ldr	r2, .L341+40
	str	r3, [sp, #8]
	ldr	r3, .L341+44
	bl	PERCENT_ADJUST_fill_guivars
	ldr	r3, .L341+48
	mov	r0, #2
	str	r3, [sp, #0]
	ldr	r3, .L341+52
	ldr	r1, .L341+56
	str	r3, [sp, #4]
	ldr	r3, .L341+60
	ldr	r2, .L341+64
	str	r3, [sp, #8]
	ldr	r3, .L341+68
	bl	PERCENT_ADJUST_fill_guivars
	ldr	r3, .L341+72
	mov	r0, #3
	str	r3, [sp, #0]
	ldr	r3, .L341+76
	ldr	r1, .L341+80
	str	r3, [sp, #4]
	ldr	r3, .L341+84
	ldr	r2, .L341+88
	str	r3, [sp, #8]
	ldr	r3, .L341+92
	bl	PERCENT_ADJUST_fill_guivars
	ldr	r3, .L341+96
	mov	r0, #4
	str	r3, [sp, #0]
	ldr	r3, .L341+100
	ldr	r1, .L341+104
	str	r3, [sp, #4]
	ldr	r3, .L341+108
	ldr	r2, .L341+112
	str	r3, [sp, #8]
	ldr	r3, .L341+116
	bl	PERCENT_ADJUST_fill_guivars
	ldr	r3, .L341+120
	mov	r0, #5
	str	r3, [sp, #0]
	ldr	r3, .L341+124
	ldr	r1, .L341+128
	str	r3, [sp, #4]
	ldr	r3, .L341+132
	ldr	r2, .L341+136
	str	r3, [sp, #8]
	ldr	r3, .L341+140
	bl	PERCENT_ADJUST_fill_guivars
	ldr	r3, .L341+144
	mov	r0, #6
	str	r3, [sp, #0]
	ldr	r3, .L341+148
	ldr	r1, .L341+152
	str	r3, [sp, #4]
	ldr	r3, .L341+156
	ldr	r2, .L341+160
	str	r3, [sp, #8]
	ldr	r3, .L341+164
	bl	PERCENT_ADJUST_fill_guivars
	ldr	r3, .L341+168
	mov	r0, #7
	str	r3, [sp, #0]
	ldr	r3, .L341+172
	ldr	r1, .L341+176
	str	r3, [sp, #4]
	ldr	r3, .L341+180
	ldr	r2, .L341+184
	str	r3, [sp, #8]
	ldr	r3, .L341+188
	bl	PERCENT_ADJUST_fill_guivars
	ldr	r3, .L341+192
	mov	r0, #8
	str	r3, [sp, #0]
	ldr	r3, .L341+196
	ldr	r1, .L341+200
	str	r3, [sp, #4]
	ldr	r3, .L341+204
	ldr	r2, .L341+208
	str	r3, [sp, #8]
	ldr	r3, .L341+212
	bl	PERCENT_ADJUST_fill_guivars
	ldr	r3, .L341+216
	mov	r0, #9
	str	r3, [sp, #0]
	ldr	r3, .L341+220
	ldr	r1, .L341+224
	str	r3, [sp, #4]
	ldr	r3, .L341+228
	ldr	r2, .L341+232
	str	r3, [sp, #8]
	ldr	r3, .L341+236
	bl	PERCENT_ADJUST_fill_guivars
	ldmfd	sp!, {r1, r2, r3, pc}
.L342:
	.align	2
.L341:
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_GroupName_0
	.word	GuiVar_PercentAdjustEndDate_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_PercentAdjustPercent_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_PercentAdjustEndDate_1
	.word	GuiVar_GroupName_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_PercentAdjustPercent_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_PercentAdjustEndDate_2
	.word	GuiVar_GroupName_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_PercentAdjustPercent_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingC_3
	.word	GuiVar_PercentAdjustEndDate_3
	.word	GuiVar_GroupName_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_PercentAdjustPercent_3
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingC_4
	.word	GuiVar_PercentAdjustEndDate_4
	.word	GuiVar_GroupName_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_PercentAdjustPercent_4
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSettingC_5
	.word	GuiVar_PercentAdjustEndDate_5
	.word	GuiVar_GroupName_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_PercentAdjustPercent_5
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSettingC_6
	.word	GuiVar_PercentAdjustEndDate_6
	.word	GuiVar_GroupName_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_PercentAdjustPercent_6
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSettingC_7
	.word	GuiVar_PercentAdjustEndDate_7
	.word	GuiVar_GroupName_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_PercentAdjustPercent_7
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSettingC_8
	.word	GuiVar_PercentAdjustEndDate_8
	.word	GuiVar_GroupName_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_PercentAdjustPercent_8
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSettingC_9
	.word	GuiVar_PercentAdjustEndDate_9
	.word	GuiVar_GroupName_9
	.word	GuiVar_GroupSetting_Visible_9
	.word	GuiVar_PercentAdjustPercent_9
	.word	GuiVar_GroupSettingB_9
.LFE61:
	.size	PERCENT_ADJUST_copy_group_into_guivars, .-PERCENT_ADJUST_copy_group_into_guivars
	.section	.text.PRIORITY_fill_guivars,"ax",%progbits
	.align	2
	.type	PRIORITY_fill_guivars, %function
PRIORITY_fill_guivars:
.LFB58:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI124:
	mov	r4, r3
	ldr	r3, .L348
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	ldr	r3, .L348+4
	mov	r1, #400
	ldr	r2, .L348+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L348+12
	ldr	r3, [r3, #8]
	cmp	r3, r5
	movls	r3, #0
	bls	.L347
	mov	r0, r5
	bl	STATION_GROUP_get_group_at_this_index
	ldr	r3, [r0, #368]
	mov	r5, r0
	cmp	r3, #0
	beq	.L347
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r5, #148]
	str	r3, [r6, #0]
	mov	r3, #1
.L347:
	str	r3, [r4, #0]
	ldr	r3, .L348
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L349:
	.align	2
.L348:
	.word	list_program_data_recursive_MUTEX
	.word	2552
	.word	.LC51
	.word	.LANCHOR1
.LFE58:
	.size	PRIORITY_fill_guivars, .-PRIORITY_fill_guivars
	.section	.text.PRIORITY_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	PRIORITY_copy_group_into_guivars
	.type	PRIORITY_copy_group_into_guivars, %function
PRIORITY_copy_group_into_guivars:
.LFB59:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI125:
	ldr	r4, .L351
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L351+4
	ldr	r3, .L351+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	ldr	r1, .L351+12
	ldr	r2, .L351+16
	ldr	r3, .L351+20
	bl	PRIORITY_fill_guivars
	mov	r0, #1
	ldr	r1, .L351+24
	ldr	r2, .L351+28
	ldr	r3, .L351+32
	bl	PRIORITY_fill_guivars
	mov	r0, #2
	ldr	r1, .L351+36
	ldr	r2, .L351+40
	ldr	r3, .L351+44
	bl	PRIORITY_fill_guivars
	mov	r0, #3
	ldr	r1, .L351+48
	ldr	r2, .L351+52
	ldr	r3, .L351+56
	bl	PRIORITY_fill_guivars
	mov	r0, #4
	ldr	r1, .L351+60
	ldr	r2, .L351+64
	ldr	r3, .L351+68
	bl	PRIORITY_fill_guivars
	mov	r0, #5
	ldr	r1, .L351+72
	ldr	r2, .L351+76
	ldr	r3, .L351+80
	bl	PRIORITY_fill_guivars
	mov	r0, #6
	ldr	r1, .L351+84
	ldr	r2, .L351+88
	ldr	r3, .L351+92
	bl	PRIORITY_fill_guivars
	mov	r0, #7
	ldr	r1, .L351+96
	ldr	r2, .L351+100
	ldr	r3, .L351+104
	bl	PRIORITY_fill_guivars
	mov	r0, #8
	ldr	r1, .L351+108
	ldr	r2, .L351+112
	ldr	r3, .L351+116
	bl	PRIORITY_fill_guivars
	mov	r0, #9
	ldr	r1, .L351+120
	ldr	r2, .L351+124
	ldr	r3, .L351+128
	bl	PRIORITY_fill_guivars
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L352:
	.align	2
.L351:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	2587
	.word	GuiVar_GroupName_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupName_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupName_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupName_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupName_4
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupName_5
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupName_6
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupName_7
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupName_8
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupName_9
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE59:
	.size	PRIORITY_copy_group_into_guivars, .-PRIORITY_copy_group_into_guivars
	.section	.text.STATION_GROUP_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	STATION_GROUP_copy_group_into_guivars
	.type	STATION_GROUP_copy_group_into_guivars, %function
STATION_GROUP_copy_group_into_guivars:
.LFB57:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L369+192
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI126:
	ldr	r2, .L369+24
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L369+28
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	subs	r4, r0, #0
	beq	.L354
	bl	nm_GROUP_load_common_guivars
	flds	s0, [r4, #80]	@ int
	fldd	d6, .L369
	ldr	r2, [r4, #72]
	flds	s1, [r4, #252]	@ int
	fuitod	d7, s0
	ldr	r3, .L369+32
	flds	s8, [r4, #104]	@ int
	flds	s4, [r4, #100]	@ int
	str	r2, [r3, #0]
	ldr	r2, [r4, #76]
	fuitod	d5, s8
	ldr	r3, .L369+36
	fdivd	d7, d7, d6
	fldd	d6, .L369+8
	str	r2, [r3, #0]
	ldr	r3, .L369+40
	flds	s9, [r4, #108]	@ int
	ldr	r2, [r4, #92]
	flds	s0, [r4, #116]	@ int
	flds	s8, [r4, #128]	@ int
	fuitod	d3, s0
	flds	s0, [r4, #132]	@ int
	fdivd	d5, d5, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	fuitod	d7, s1
	ldr	r3, .L369+44
	flds	s1, [r4, #120]	@ int
	str	r2, [r3, #0]
	ldr	r2, [r4, #96]
	ldr	r3, .L369+48
	str	r2, [r3, #0]
	ldr	r3, .L369+52
	ldr	r2, [r4, #84]
	fdivd	d7, d7, d6
	fcvtsd	s3, d5
	fuitod	d5, s9
	fdivd	d5, d5, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	fuitod	d7, s4
	flds	s4, [r4, #124]	@ int
	ldr	r3, .L369+56
	str	r2, [r3, #0]
	ldr	r2, [r4, #88]
	ldr	r3, .L369+60
	str	r2, [r3, #0]
	ldr	r3, .L369+64
	fdivd	d7, d7, d6
	fcvtsd	s2, d5
	fdivd	d3, d3, d6
	fcvtsd	s15, d7
	flds	s14, [r4, #112]	@ int
	fuitod	d5, s14
	fsts	s15, [r3, #0]
	ldr	r3, .L369+68
	fcmps	s15, s3
	fsts	s3, [r3, #0]
	ldr	r3, .L369+72
	fsts	s2, [r3, #0]
	ldr	r3, .L369+76
	fdivd	d5, d5, d6
	fcvtsd	s6, d3
	fmstat
	fcvtsd	s5, d5
	fuitod	d5, s1
	fsts	s5, [r3, #0]
	fdivd	d5, d5, d6
	ldr	r3, .L369+80
	fsts	s6, [r3, #0]
	ldr	r3, .L369+84
	fcvtsd	s7, d5
	fuitod	d5, s4
	fsts	s7, [r3, #0]
	fdivd	d5, d5, d6
	ldr	r3, .L369+88
	fcvtsd	s14, d5
	fuitod	d5, s8
	fsts	s14, [r3, #0]
	fdivd	d5, d5, d6
	ldr	r3, .L369+92
	fcvtsd	s9, d5
	fuitod	d5, s0
	fsts	s9, [r3, #0]
	fdivd	d5, d5, d6
	ldr	r3, .L369+96
	fcvtsd	s8, d5
	fsts	s8, [r3, #0]
	flds	s1, [r4, #136]	@ int
	flds	s0, [r4, #140]	@ int
	ldr	r3, .L369+100
	fuitod	d5, s1
	fdivd	d5, d5, d6
	fcvtsd	s4, d5
	fuitod	d5, s0
	flds	s0, [r4, #144]	@ int
	fsts	s4, [r3, #0]
	fdivd	d5, d5, d6
	ldr	r3, .L369+104
	fcvtsd	s1, d5
	fuitod	d5, s0
	fsts	s1, [r3, #0]
	fdivd	d5, d5, d6
	ldr	r3, .L369+108
	fcvtsd	s11, d5
	fsts	s11, [r3, #0]
	bne	.L368
	fcmps	s15, s2
	fmstat
	bne	.L368
	fcmps	s15, s5
	fmstat
	bne	.L368
	fcmps	s15, s6
	fmstat
	bne	.L368
	fcmps	s15, s7
	fmstat
	bne	.L368
	fcmps	s15, s14
	fmstat
	bne	.L368
	fcmps	s15, s9
	fmstat
	bne	.L368
	fcmps	s15, s8
	fmstat
	bne	.L368
	fcmps	s15, s4
	fmstat
	bne	.L368
	fcmps	s15, s1
	fmstat
	bne	.L368
	fcmps	s15, s11
	fmstat
	moveq	r3, #0
	movne	r3, #1
	b	.L355
.L368:
	mov	r3, #1
.L355:
	flds	s1, [r4, #324]	@ int
	ldr	r2, .L369+112
	flds	s4, [r4, #328]	@ int
	fldd	d5, .L369+16
	fuitod	d7, s1
	str	r3, [r2, #0]
	ldr	r2, [r4, #320]
	flds	s8, [r4, #332]	@ int
	ldr	r3, .L369+116
	flds	s9, [r4, #336]	@ int
	flds	s0, [r4, #344]	@ int
	str	r2, [r3, #0]
	fdivd	d7, d7, d6
	ldr	r3, .L369+120
	flds	s1, [r4, #348]	@ int
	ldr	r2, [r4, #368]
	ldr	r0, [r4, #364]
	cmp	r0, #0
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	fuitod	d7, s4
	ldr	r3, .L369+124
	fdivd	d7, d7, d5
	flds	s11, [r4, #340]	@ int
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	fuitod	d7, s8
	ldr	r3, .L369+128
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	fuitod	d7, s9
	ldr	r3, .L369+132
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	fuitod	d7, s11
	ldr	r3, .L369+136
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	fuitod	d7, s0
	ldr	r3, .L369+140
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	fuitod	d7, s1
	ldr	r3, .L369+144
	fdivd	d6, d7, d6
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	ldr	r3, .L369+148
	str	r2, [r3, #0]
	ldr	r3, .L369+152
	str	r0, [r3, #0]
	beq	.L356
	bl	SYSTEM_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L369+156
	bl	strlcpy
.L356:
	ldr	r1, [r4, #372]
	ldr	r3, .L369+160
	cmp	r1, #0
	str	r1, [r3, #0]
	beq	.L357
	ldr	r5, .L369+164
	ldr	r3, .L369+168
	mov	r1, #400
	ldr	r2, .L369+24
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r4, #372]
	bl	MOISTURE_SENSOR_get_ptr_to_sensor_with_this_serial_number
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L369+176
	bl	strlcpy
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	b	.L358
.L370:
	.align	2
.L369:
	.word	0
	.word	1090021888
	.word	0
	.word	1079574528
	.word	0
	.word	1076101120
	.word	.LC51
	.word	2435
	.word	GuiVar_StationGroupPlantType
	.word	GuiVar_StationGroupHeadType
	.word	GuiVar_StationGroupPrecipRate
	.word	GuiVar_StationGroupExposure
	.word	GuiVar_StationGroupUsableRain
	.word	GuiVar_StationGroupSoilStorageCapacity
	.word	GuiVar_StationGroupSoilType
	.word	GuiVar_StationGroupSlopePercentage
	.word	GuiVar_StationGroupKc1
	.word	GuiVar_StationGroupKc2
	.word	GuiVar_StationGroupKc3
	.word	GuiVar_StationGroupKc4
	.word	GuiVar_StationGroupKc5
	.word	GuiVar_StationGroupKc6
	.word	GuiVar_StationGroupKc7
	.word	GuiVar_StationGroupKc8
	.word	GuiVar_StationGroupKc9
	.word	GuiVar_StationGroupKc10
	.word	GuiVar_StationGroupKc11
	.word	GuiVar_StationGroupKc12
	.word	GuiVar_StationGroupKcsAreCustom
	.word	GuiVar_StationGroupAllowableDepletion
	.word	GuiVar_StationGroupAvailableWater
	.word	GuiVar_StationGroupRootZoneDepth
	.word	GuiVar_StationGroupSpeciesFactor
	.word	GuiVar_StationGroupDensityFactor
	.word	GuiVar_StationGroupMicroclimateFactor
	.word	GuiVar_StationGroupSoilIntakeRate
	.word	GuiVar_StationGroupAllowableSurfaceAccum
	.word	GuiVar_StationGroupInUse
	.word	GuiVar_StationGroupSystemGID
	.word	GuiVar_SystemName
	.word	GuiVar_StationGroupMoistureSensorDecoderSN
	.word	moisture_sensor_items_recursive_MUTEX
	.word	2521
	.word	GuiLib_LanguageIndex
	.word	GuiVar_MoisSensorName
	.word	g_GROUP_ID
	.word	STATION_get_GID_station_group
	.word	GuiVar_StationSelectionGridStationCount
	.word	list_program_data_recursive_MUTEX
.L357:
	ldr	r3, .L369+172
	mov	r0, #1000
	ldrsh	r2, [r3, #0]
	bl	GuiLib_GetTextLanguagePtr
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L369+176
	bl	strlcpy
.L358:
	ldr	r2, .L369+180
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	ldr	r0, .L369+184
	mov	r1, #0
	ldr	r2, [r2, #0]
	bl	STATION_SELECTION_GRID_populate_cursors
	bl	STATION_SELECTION_GRID_get_count_of_selected_stations
	ldr	r3, .L369+188
	str	r0, [r3, #0]
.L354:
	ldr	r3, .L369+192
	ldr	r0, [r3, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.LFE57:
	.size	STATION_GROUP_copy_group_into_guivars, .-STATION_GROUP_copy_group_into_guivars
	.section	.text.STATION_GROUP_get_index_for_group_with_this_GID,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_index_for_group_with_this_GID
	.type	STATION_GROUP_get_index_for_group_with_this_GID, %function
STATION_GROUP_get_index_for_group_with_this_GID:
.LFB105:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI127:
	ldr	r4, .L372
	ldr	r2, .L372+4
	ldr	r3, .L372+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L372+12
	bl	nm_GROUP_get_index_for_group_with_this_GID
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L373:
	.align	2
.L372:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4042
	.word	.LANCHOR1
.LFE105:
	.size	STATION_GROUP_get_index_for_group_with_this_GID, .-STATION_GROUP_get_index_for_group_with_this_GID
	.section	.text.STATION_GROUP_get_last_group_ID,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_last_group_ID
	.type	STATION_GROUP_get_last_group_ID, %function
STATION_GROUP_get_last_group_ID:
.LFB106:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI128:
	ldr	r4, .L375
	mov	r1, #400
	ldr	r2, .L375+4
	ldr	r0, [r4, #0]
	ldr	r3, .L375+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L375+12
	ldr	r0, [r4, #0]
	ldr	r3, [r3, #0]
	ldr	r5, [r3, #12]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L376:
	.align	2
.L375:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4056
	.word	.LANCHOR1
.LFE106:
	.size	STATION_GROUP_get_last_group_ID, .-STATION_GROUP_get_last_group_ID
	.section	.text.STATION_GROUP_get_num_groups_in_use,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_num_groups_in_use
	.type	STATION_GROUP_get_num_groups_in_use, %function
STATION_GROUP_get_num_groups_in_use:
.LFB107:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L381
	stmfd	sp!, {r4, lr}
.LCFI129:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L381+4
	ldr	r3, .L381+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L381+12
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L378
.L380:
	ldr	r3, [r1, #368]
	ldr	r0, .L381+12
	cmp	r3, #0
	addne	r4, r4, #1
	bl	nm_ListGetNext
	mov	r1, r0
.L378:
	cmp	r1, #0
	bne	.L380
	ldr	r3, .L381
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L382:
	.align	2
.L381:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4078
	.word	.LANCHOR1
.LFE107:
	.size	STATION_GROUP_get_num_groups_in_use, .-STATION_GROUP_get_num_groups_in_use
	.section	.text.nm_STATION_GROUP_load_group_name_into_guivar,"ax",%progbits
	.align	2
	.global	nm_STATION_GROUP_load_group_name_into_guivar
	.type	nm_STATION_GROUP_load_group_name_into_guivar, %function
nm_STATION_GROUP_load_group_name_into_guivar:
.LFB102:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI130:
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r4, r0
	bcs	.L384
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	mov	r4, r0
	mov	r1, r4
	ldr	r0, .L387
	bl	nm_OnList
	cmp	r0, #1
	bne	.L385
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L387+4
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	strlcpy
.L385:
	ldr	r0, .L387+8
	ldr	r1, .L387+12
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	Alert_group_not_found_with_filename
.L384:
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r4, r0
	bne	.L386
	mov	r1, #0
	ldr	r0, .L387+16
	bl	GuiLib_GetTextPtr
	ldr	r2, .L387+20
	mov	r1, #48
	mov	r3, r0
	ldr	r0, .L387+4
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	snprintf
.L386:
	mov	r1, #0
	ldr	r0, .L387+24
	bl	GuiLib_GetTextPtr
	mov	r1, #0
	mov	r4, r0
	ldr	r0, .L387+28
	bl	GuiLib_GetTextPtr
	mov	r1, #48
	ldr	r2, .L387+32
	mov	r3, r4
	str	r0, [sp, #0]
	ldr	r0, .L387+4
	bl	snprintf
	add	sp, sp, #4
	ldmfd	sp!, {r4, pc}
.L388:
	.align	2
.L387:
	.word	.LANCHOR1
	.word	GuiVar_itmGroupName
	.word	.LC51
	.word	3955
	.word	943
	.word	.LC72
	.word	874
	.word	942
	.word	.LC73
.LFE102:
	.size	nm_STATION_GROUP_load_group_name_into_guivar, .-nm_STATION_GROUP_load_group_name_into_guivar
	.section	.text.BUDGET_REDUCTION_LIMITS_fill_guivars,"ax",%progbits
	.align	2
	.type	BUDGET_REDUCTION_LIMITS_fill_guivars, %function
BUDGET_REDUCTION_LIMITS_fill_guivars:
.LFB77:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI131:
	mov	r4, r3
	ldr	r3, .L394
	mov	r5, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L394+4
	mov	r6, r2
	ldr	r2, .L394+8
	bl	xQueueTakeMutexRecursive_debug
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r5, r0
	movcs	r3, #0
	bcs	.L393
	mov	r0, r5
	bl	STATION_GROUP_get_group_at_this_index
	ldr	r3, [r0, #368]
	mov	r5, r0
	cmp	r3, #0
	beq	.L393
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	mov	r0, r7
	bl	strlcpy
	ldr	r3, [r5, #360]
	rsb	r3, r3, #0
	str	r3, [r6, #0]
	mov	r3, #1
.L393:
	str	r3, [r4, #0]
	ldr	r3, .L394
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L395:
	.align	2
.L394:
	.word	list_program_data_recursive_MUTEX
	.word	3172
	.word	.LC51
.LFE77:
	.size	BUDGET_REDUCTION_LIMITS_fill_guivars, .-BUDGET_REDUCTION_LIMITS_fill_guivars
	.section	.text.BUDGET_REDUCTION_LIMITS_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	BUDGET_REDUCTION_LIMITS_copy_group_into_guivars
	.type	BUDGET_REDUCTION_LIMITS_copy_group_into_guivars, %function
BUDGET_REDUCTION_LIMITS_copy_group_into_guivars:
.LFB78:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI132:
	ldr	r1, .L397
	mov	r0, #0
	ldr	r2, .L397+4
	ldr	r3, .L397+8
	bl	BUDGET_REDUCTION_LIMITS_fill_guivars
	mov	r0, #1
	ldr	r1, .L397+12
	ldr	r2, .L397+16
	ldr	r3, .L397+20
	bl	BUDGET_REDUCTION_LIMITS_fill_guivars
	mov	r0, #2
	ldr	r1, .L397+24
	ldr	r2, .L397+28
	ldr	r3, .L397+32
	bl	BUDGET_REDUCTION_LIMITS_fill_guivars
	mov	r0, #3
	ldr	r1, .L397+36
	ldr	r2, .L397+40
	ldr	r3, .L397+44
	bl	BUDGET_REDUCTION_LIMITS_fill_guivars
	mov	r0, #4
	ldr	r1, .L397+48
	ldr	r2, .L397+52
	ldr	r3, .L397+56
	bl	BUDGET_REDUCTION_LIMITS_fill_guivars
	mov	r0, #5
	ldr	r1, .L397+60
	ldr	r2, .L397+64
	ldr	r3, .L397+68
	bl	BUDGET_REDUCTION_LIMITS_fill_guivars
	mov	r0, #6
	ldr	r1, .L397+72
	ldr	r2, .L397+76
	ldr	r3, .L397+80
	bl	BUDGET_REDUCTION_LIMITS_fill_guivars
	mov	r0, #7
	ldr	r1, .L397+84
	ldr	r2, .L397+88
	ldr	r3, .L397+92
	bl	BUDGET_REDUCTION_LIMITS_fill_guivars
	mov	r0, #8
	ldr	r1, .L397+96
	ldr	r2, .L397+100
	ldr	r3, .L397+104
	bl	BUDGET_REDUCTION_LIMITS_fill_guivars
	ldr	r1, .L397+108
	ldr	r2, .L397+112
	ldr	r3, .L397+116
	mov	r0, #9
	ldr	lr, [sp], #4
	b	BUDGET_REDUCTION_LIMITS_fill_guivars
.L398:
	.align	2
.L397:
	.word	GuiVar_GroupName_0
	.word	GuiVar_BudgetReductionLimit_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupName_1
	.word	GuiVar_BudgetReductionLimit_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupName_2
	.word	GuiVar_BudgetReductionLimit_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupName_3
	.word	GuiVar_BudgetReductionLimit_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupName_4
	.word	GuiVar_BudgetReductionLimit_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupName_5
	.word	GuiVar_BudgetReductionLimit_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupName_6
	.word	GuiVar_BudgetReductionLimit_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupName_7
	.word	GuiVar_BudgetReductionLimit_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupName_8
	.word	GuiVar_BudgetReductionLimit_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupName_9
	.word	GuiVar_BudgetReductionLimit_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE78:
	.size	BUDGET_REDUCTION_LIMITS_copy_group_into_guivars, .-BUDGET_REDUCTION_LIMITS_copy_group_into_guivars
	.section	.text.STATION_GROUP_get_plant_type,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_plant_type
	.type	STATION_GROUP_get_plant_type, %function
STATION_GROUP_get_plant_type:
.LFB108:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI133:
	ldr	r4, .L400
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L400+4
	ldr	r3, .L400+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L400+12
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	ldr	r3, .L400+16
	add	r1, r5, #72
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #13
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L401:
	.align	2
.L400:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4111
	.word	nm_STATION_GROUP_set_plant_type
	.word	.LC0
.LFE108:
	.size	STATION_GROUP_get_plant_type, .-STATION_GROUP_get_plant_type
	.section	.text.STATION_GROUP_get_head_type,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_head_type
	.type	STATION_GROUP_get_head_type, %function
STATION_GROUP_get_head_type:
.LFB109:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI134:
	ldr	r4, .L403
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L403+4
	ldr	r3, .L403+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L403+12
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	ldr	r3, .L403+16
	add	r1, r5, #76
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #12
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L404:
	.align	2
.L403:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4134
	.word	nm_STATION_GROUP_set_head_type
	.word	.LC1
.LFE109:
	.size	STATION_GROUP_get_head_type, .-STATION_GROUP_get_head_type
	.section	.text.STATION_GROUP_get_soil_type,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_soil_type
	.type	STATION_GROUP_get_soil_type, %function
STATION_GROUP_get_soil_type:
.LFB110:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI135:
	ldr	r4, .L406
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L406+4
	ldr	r3, .L406+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #3
	str	r3, [sp, #0]
	ldr	r3, .L406+12
	add	r1, r5, #84
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	ldr	r3, .L406+16
	mov	r2, #0
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #6
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L407:
	.align	2
.L406:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4157
	.word	nm_STATION_GROUP_set_soil_type
	.word	.LC3
.LFE110:
	.size	STATION_GROUP_get_soil_type, .-STATION_GROUP_get_soil_type
	.section	.text.STATION_GROUP_get_slope_percentage,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_slope_percentage
	.type	STATION_GROUP_get_slope_percentage, %function
STATION_GROUP_get_slope_percentage:
.LFB111:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI136:
	ldr	r4, .L409
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L409+4
	ldr	r3, .L409+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L409+12
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	ldr	r3, .L409+16
	add	r1, r5, #88
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #3
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L410:
	.align	2
.L409:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4180
	.word	nm_STATION_GROUP_set_slope_percentage
	.word	.LC4
.LFE111:
	.size	STATION_GROUP_get_slope_percentage, .-STATION_GROUP_get_slope_percentage
	.section	.text.STATION_GROUP_get_precip_rate_in_per_hr,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_precip_rate_in_per_hr
	.type	STATION_GROUP_get_precip_rate_in_per_hr, %function
STATION_GROUP_get_precip_rate_in_per_hr:
.LFB112:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI137:
	ldr	r4, .L412+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L412+12
	ldr	r3, .L412+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L412+20
	add	r1, r5, #80
	str	r3, [sp, #0]
	ldr	r3, .L412+24
	mov	r2, #1000
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	ldr	r3, .L412+28
	mov	r0, r5
	str	r3, [sp, #12]
	ldr	r3, .L412+32
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	fmsr	s13, r5	@ int
	fuitod	d7, s13
	fldd	d6, .L412
	fdivd	d7, d7, d6
	fcvtsd	s13, d7
	fmrs	r0, s13
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L413:
	.align	2
.L412:
	.word	0
	.word	1090021888
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4205
	.word	100000
	.word	nm_STATION_GROUP_set_precip_rate
	.word	.LC2
	.word	999999
.LFE112:
	.size	STATION_GROUP_get_precip_rate_in_per_hr, .-STATION_GROUP_get_precip_rate_in_per_hr
	.section	.text.STATION_GROUP_get_allowable_surface_accumulation,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_allowable_surface_accumulation
	.type	STATION_GROUP_get_allowable_surface_accumulation, %function
STATION_GROUP_get_allowable_surface_accumulation:
.LFB113:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI138:
	ldr	r4, .L415+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L415+12
	ldr	r3, .L415+16
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #25
	str	r3, [sp, #0]
	ldr	r3, .L415+20
	add	r1, r5, #348
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	ldr	r3, .L415+24
	mov	r2, #1
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #100
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	fmsr	s13, r5	@ int
	fuitod	d7, s13
	fldd	d6, .L415
	fdivd	d7, d7, d6
	fcvtsd	s13, d7
	fmrs	r0, s13
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L416:
	.align	2
.L415:
	.word	0
	.word	1079574528
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4232
	.word	nm_STATION_GROUP_set_allowable_surface_accumulation
	.word	.LC7
.LFE113:
	.size	STATION_GROUP_get_allowable_surface_accumulation, .-STATION_GROUP_get_allowable_surface_accumulation
	.section	.text.STATION_GROUP_get_soil_intake_rate,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_soil_intake_rate
	.type	STATION_GROUP_get_soil_intake_rate, %function
STATION_GROUP_get_soil_intake_rate:
.LFB114:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI139:
	ldr	r4, .L418+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L418+12
	ldr	r3, .L418+16
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #35
	str	r3, [sp, #0]
	ldr	r3, .L418+20
	add	r1, r5, #344
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	ldr	r3, .L418+24
	mov	r2, #1
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #100
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	fmsr	s13, r5	@ int
	fuitod	d7, s13
	fldd	d6, .L418
	fdivd	d7, d7, d6
	fcvtsd	s13, d7
	fmrs	r0, s13
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L419:
	.align	2
.L418:
	.word	0
	.word	1079574528
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4259
	.word	nm_STATION_GROUP_set_soil_intake_rate
	.word	.LC6
.LFE114:
	.size	STATION_GROUP_get_soil_intake_rate, .-STATION_GROUP_get_soil_intake_rate
	.section	.text.SCHEDULE_get_enabled,"ax",%progbits
	.align	2
	.global	SCHEDULE_get_enabled
	.type	SCHEDULE_get_enabled, %function
SCHEDULE_get_enabled:
.LFB115:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI140:
	ldr	r4, .L421
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L421+4
	ldr	r3, .L421+8
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r5, #296
	str	r3, [sp, #0]
	ldr	r3, .L421+12
	add	r1, r5, #164
	str	r3, [sp, #4]
	mov	r2, #0
	ldr	r3, .L421+16
	mov	r0, r5
	bl	SHARED_get_bool_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L422:
	.align	2
.L421:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4300
	.word	.LC18
	.word	nm_SCHEDULE_set_enabled
.LFE115:
	.size	SCHEDULE_get_enabled, .-SCHEDULE_get_enabled
	.section	.text.SCHEDULE_get_start_time,"ax",%progbits
	.align	2
	.global	SCHEDULE_get_start_time
	.type	SCHEDULE_get_start_time, %function
SCHEDULE_get_start_time:
.LFB116:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI141:
	ldr	r3, .L426
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L426+4
	ldr	r3, .L426+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r4, #164]
	cmp	r3, #0
	ldr	r3, .L426+12
	moveq	r4, r3
	beq	.L424
	ldr	r2, .L426+16
	mov	r0, r4
	str	r2, [sp, #4]
	add	r2, r4, #296
	str	r2, [sp, #8]
	ldr	r2, .L426+20
	add	r1, r4, #172
	str	r2, [sp, #12]
	mov	r2, #0
	str	r3, [sp, #0]
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
.L424:
	ldr	r3, .L426
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L427:
	.align	2
.L426:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4341
	.word	86400
	.word	nm_SCHEDULE_set_start_time
	.word	.LC27
.LFE116:
	.size	SCHEDULE_get_start_time, .-SCHEDULE_get_start_time
	.section	.text.SCHEDULE_get_stop_time,"ax",%progbits
	.align	2
	.global	SCHEDULE_get_stop_time
	.type	SCHEDULE_get_stop_time, %function
SCHEDULE_get_stop_time:
.LFB117:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI142:
	ldr	r4, .L429
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L429+4
	ldr	r3, .L429+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L429+12
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	ldr	r3, .L429+16
	add	r1, r5, #176
	str	r3, [sp, #12]
	mov	r0, r5
	ldr	r3, .L429+20
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L430:
	.align	2
.L429:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4394
	.word	nm_SCHEDULE_set_stop_time
	.word	.LC28
	.word	86400
.LFE117:
	.size	SCHEDULE_get_stop_time, .-SCHEDULE_get_stop_time
	.section	.text.SCHEDULE_get_schedule_type,"ax",%progbits
	.align	2
	.global	SCHEDULE_get_schedule_type
	.type	SCHEDULE_get_schedule_type, %function
SCHEDULE_get_schedule_type:
.LFB118:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI143:
	ldr	r4, .L432
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L432+4
	ldr	r3, .L432+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L432+12
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	ldr	r3, .L432+16
	add	r1, r5, #168
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #18
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L433:
	.align	2
.L432:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4415
	.word	nm_SCHEDULE_set_schedule_type
	.word	.LC11
.LFE118:
	.size	SCHEDULE_get_schedule_type, .-SCHEDULE_get_schedule_type
	.section	.text.SCHEDULE_get_mow_day,"ax",%progbits
	.align	2
	.global	SCHEDULE_get_mow_day
	.type	SCHEDULE_get_mow_day, %function
SCHEDULE_get_mow_day:
.LFB119:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI144:
	ldr	r4, .L435
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L435+4
	ldr	r3, .L435+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, .L435+12
	mov	r3, #7
	str	r2, [sp, #4]
	add	r2, r5, #296
	str	r2, [sp, #8]
	ldr	r2, .L435+16
	add	r1, r5, #216
	str	r2, [sp, #12]
	mov	r0, r5
	mov	r2, #0
	str	r3, [sp, #0]
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L436:
	.align	2
.L435:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4458
	.word	nm_SCHEDULE_set_mow_day
	.word	.LC10
.LFE119:
	.size	SCHEDULE_get_mow_day, .-SCHEDULE_get_mow_day
	.section	.text.SCHEDULE_get_irrigate_after_29th_or_31st,"ax",%progbits
	.align	2
	.global	SCHEDULE_get_irrigate_after_29th_or_31st
	.type	SCHEDULE_get_irrigate_after_29th_or_31st, %function
SCHEDULE_get_irrigate_after_29th_or_31st:
.LFB122:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI145:
	ldr	r4, .L438
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L438+4
	ldr	r3, .L438+8
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r5, #296
	str	r3, [sp, #0]
	ldr	r3, .L438+12
	add	r1, r5, #212
	str	r3, [sp, #4]
	mov	r2, #0
	ldr	r3, .L438+16
	mov	r0, r5
	bl	SHARED_get_bool_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L439:
	.align	2
.L438:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4767
	.word	.LC19
	.word	nm_SCHEDULE_set_irrigate_on_29th_or_31st
.LFE122:
	.size	SCHEDULE_get_irrigate_after_29th_or_31st, .-SCHEDULE_get_irrigate_after_29th_or_31st
	.section	.text.DAILY_ET_get_et_in_use,"ax",%progbits
	.align	2
	.global	DAILY_ET_get_et_in_use
	.type	DAILY_ET_get_et_in_use, %function
DAILY_ET_get_et_in_use:
.LFB123:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI146:
	ldr	r4, .L441
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L441+4
	ldr	r3, .L441+8
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r5, #296
	str	r3, [sp, #0]
	ldr	r3, .L441+12
	add	r1, r5, #240
	str	r3, [sp, #4]
	mov	r2, #1
	ldr	r3, .L441+16
	mov	r0, r5
	bl	SHARED_get_bool_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L442:
	.align	2
.L441:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4786
	.word	.LC20
	.word	nm_DAILY_ET_set_ET_in_use
.LFE123:
	.size	DAILY_ET_get_et_in_use, .-DAILY_ET_get_et_in_use
	.section	.text.RAIN_get_rain_in_use,"ax",%progbits
	.align	2
	.global	RAIN_get_rain_in_use
	.type	RAIN_get_rain_in_use, %function
RAIN_get_rain_in_use:
.LFB124:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI147:
	ldr	r4, .L444
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L444+4
	ldr	r3, .L444+8
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r5, #296
	str	r3, [sp, #0]
	ldr	r3, .L444+12
	add	r1, r5, #248
	str	r3, [sp, #4]
	mov	r2, #1
	ldr	r3, .L444+16
	mov	r0, r5
	bl	SHARED_get_bool_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L445:
	.align	2
.L444:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4805
	.word	.LC22
	.word	nm_RAIN_set_rain_in_use
.LFE124:
	.size	RAIN_get_rain_in_use, .-RAIN_get_rain_in_use
	.section	.text.PERCENT_ADJUST_get_percentage_100u_for_this_gid,"ax",%progbits
	.align	2
	.global	PERCENT_ADJUST_get_percentage_100u_for_this_gid
	.type	PERCENT_ADJUST_get_percentage_100u_for_this_gid, %function
PERCENT_ADJUST_get_percentage_100u_for_this_gid:
.LFB125:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI148:
	ldr	r3, .L452
	mov	r5, r0
	ldr	r2, .L452+4
	ldr	r0, [r3, #0]
	mov	r4, r1
	ldr	r3, .L452+8
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L452+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r1, r0, #0
	beq	.L447
	ldr	r2, [r1, #156]
	cmp	r4, r2
	bcc	.L449
	ldr	r3, [r1, #160]
	cmp	r4, r3
	mov	r4, #100
	bhi	.L448
	cmp	r2, r3
	beq	.L448
	ldr	r3, .L452+16
	mov	r2, #20
	str	r3, [sp, #4]
	add	r3, r1, #296
	str	r3, [sp, #8]
	ldr	r3, .L452+20
	add	r1, r1, #152
	str	r3, [sp, #12]
	mov	r3, #200
	str	r4, [sp, #0]
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
	b	.L448
.L447:
	ldr	r0, .L452+4
	ldr	r1, .L452+24
	bl	Alert_station_not_in_group_with_filename
.L449:
	mov	r4, #100
.L448:
	ldr	r3, .L452
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L453:
	.align	2
.L452:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4833
	.word	.LANCHOR1
	.word	nm_PERCENT_ADJUST_set_percent
	.word	.LC9
	.word	4853
.LFE125:
	.size	PERCENT_ADJUST_get_percentage_100u_for_this_gid, .-PERCENT_ADJUST_get_percentage_100u_for_this_gid
	.section	.text.PERCENT_ADJUST_get_remaining_days_for_this_gid,"ax",%progbits
	.align	2
	.global	PERCENT_ADJUST_get_remaining_days_for_this_gid
	.type	PERCENT_ADJUST_get_remaining_days_for_this_gid, %function
PERCENT_ADJUST_get_remaining_days_for_this_gid:
.LFB126:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI149:
	ldr	r3, .L457
	ldr	r5, .L457+4
	sub	sp, sp, #28
.LCFI150:
	mov	r4, r0
	mov	r6, #0
	ldr	r0, [r3, #0]
	mov	r2, r5
	ldr	r3, .L457+8
	mov	r7, r1
	mov	r1, #400
	str	r6, [sp, #24]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L457+12
	mov	r2, r6
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	beq	.L455
	ldr	r3, [r4, #156]
	cmp	r7, r3
	bcc	.L456
	ldr	r3, [r4, #160]
	cmp	r7, r3
	bhi	.L456
	add	r0, sp, #16
	bl	EPSON_obtain_latest_time_and_date
	mov	r0, #1
	mov	r1, r0
	ldr	r2, .L457+16
	bl	DMYToDate
	mov	r1, #12
	ldr	r2, .L457+20
	mov	r8, r0
	mov	r0, #31
	bl	DMYToDate
	ldrh	r1, [sp, #20]
	mov	r2, r8, asl #16
	str	r1, [sp, #0]
	ldr	r1, .L457+24
	mov	r2, r2, lsr #16
	str	r1, [sp, #4]
	add	r1, r4, #296
	str	r1, [sp, #8]
	ldr	r1, .L457+28
	add	r8, sp, #28
	str	r1, [sp, #12]
	add	r1, r4, #160
	mov	r3, r0, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r4
	bl	SHARED_get_uint32_64_bit_change_bits_group
	rsb	r0, r7, r0
	str	r0, [r8, #-4]!
	mov	r0, r4
	bl	nm_GROUP_get_name
	ldr	r3, .L457+32
	mov	r1, r6
	stmib	sp, {r3, r5}
	ldr	r3, .L457+36
	ldr	r2, .L457+40
	str	r3, [sp, #12]
	mov	r3, r6
	str	r0, [sp, #0]
	mov	r0, r8
	bl	RC_uint32_with_filename
	b	.L456
.L455:
	mov	r0, r5
	ldr	r1, .L457+44
	bl	Alert_station_not_in_group_with_filename
.L456:
	ldr	r3, .L457
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [sp, #24]
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L458:
	.align	2
.L457:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4881
	.word	.LANCHOR1
	.word	2011
	.word	2042
	.word	nm_PERCENT_ADJUST_set_end_date
	.word	.LC17
	.word	.LC74
	.word	4901
	.word	366
	.word	4906
.LFE126:
	.size	PERCENT_ADJUST_get_remaining_days_for_this_gid, .-PERCENT_ADJUST_get_remaining_days_for_this_gid
	.section	.text.STATION_GROUP_get_cycle_time_10u_for_this_gid,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_cycle_time_10u_for_this_gid
	.type	STATION_GROUP_get_cycle_time_10u_for_this_gid, %function
STATION_GROUP_get_cycle_time_10u_for_this_gid:
.LFB127:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L462+4
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI151:
	ldr	r2, .L462+8
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L462+12
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L462+16
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	moveq	r4, #50
	beq	.L460
	bl	STATION_GROUP_get_allowable_surface_accumulation
	mov	r6, r0	@ float
	mov	r0, r4
	bl	STATION_GROUP_get_soil_intake_rate
	mov	r5, r0	@ float
	mov	r0, r4
	bl	STATION_GROUP_get_precip_rate_in_per_hr
	mov	r1, r5	@ float
	mov	r2, r0	@ float
	mov	r0, r6	@ float
	bl	WATERSENSE_get_cycle_time
	flds	s15, .L462
	fmsr	s14, r0
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fmrs	r4, s15	@ int
.L460:
	ldr	r3, .L462+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L463:
	.align	2
.L462:
	.word	1092616192
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4931
	.word	.LANCHOR1
.LFE127:
	.size	STATION_GROUP_get_cycle_time_10u_for_this_gid, .-STATION_GROUP_get_cycle_time_10u_for_this_gid
	.section	.text.STATION_GROUP_get_soak_time_for_this_gid,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_soak_time_for_this_gid
	.type	STATION_GROUP_get_soak_time_for_this_gid, %function
STATION_GROUP_get_soak_time_for_this_gid:
.LFB128:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L468
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI152:
	ldr	r2, .L468+4
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L468+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L468+12
	mov	r1, r4
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	cmp	r0, #0
	beq	.L467
	mov	r1, r4
	ldr	r0, .L468+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	beq	.L467
	bl	STATION_GROUP_get_allowable_surface_accumulation
	mov	r7, r0	@ float
	mov	r0, r4
	bl	STATION_GROUP_get_soil_intake_rate
	mov	r6, r0	@ float
	mov	r0, r4
	bl	STATION_GROUP_get_precip_rate_in_per_hr
	fmsr	s15, r5	@ int
	mov	r1, r6	@ float
	fuitos	s15, s15
	fmrs	r3, s15
	mov	r2, r0	@ float
	mov	r0, r7	@ float
	bl	WATERSENSE_get_soak_time__cycle_end_to_cycle_start
	fmsr	s15, r0
	ftouizs	s15, s15
	fmrs	r4, s15	@ int
	b	.L465
.L467:
	mov	r4, #20
.L465:
	ldr	r3, .L468
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L469:
	.align	2
.L468:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4964
	.word	.LANCHOR1
.LFE128:
	.size	STATION_GROUP_get_soak_time_for_this_gid, .-STATION_GROUP_get_soak_time_for_this_gid
	.section	.text.STATION_GROUP_get_station_precip_rate_in_per_hr_100000u,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_station_precip_rate_in_per_hr_100000u
	.type	STATION_GROUP_get_station_precip_rate_in_per_hr_100000u, %function
STATION_GROUP_get_station_precip_rate_in_per_hr_100000u:
.LFB129:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI153:
	ldr	r3, .L474
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L474+4
	ldr	r3, .L474+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	beq	.L473
.LBB46:
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	beq	.L473
.LBB47:
	mov	r2, #0
	ldr	r0, .L474+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	ldr	r3, .L474+16
	mov	r2, #1000
	str	r3, [sp, #0]
	ldr	r3, .L474+20
	str	r3, [sp, #4]
	add	r3, r0, #296
	str	r3, [sp, #8]
	ldr	r3, .L474+24
	add	r1, r0, #80
	str	r3, [sp, #12]
	ldr	r3, .L474+28
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
	b	.L471
.L473:
.LBE47:
	ldr	r4, .L474+16
.L471:
.LBE46:
	ldr	r3, .L474
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L475:
	.align	2
.L474:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4999
	.word	.LANCHOR1
	.word	100000
	.word	nm_STATION_GROUP_set_precip_rate
	.word	.LC2
	.word	999999
.LFE129:
	.size	STATION_GROUP_get_station_precip_rate_in_per_hr_100000u, .-STATION_GROUP_get_station_precip_rate_in_per_hr_100000u
	.section	.text.STATION_GROUP_get_station_crop_coefficient_100u,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_station_crop_coefficient_100u
	.type	STATION_GROUP_get_station_crop_coefficient_100u, %function
STATION_GROUP_get_station_crop_coefficient_100u:
.LFB130:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L481
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI154:
	ldr	r2, .L481+4
	mov	r5, r0
	sub	sp, sp, #72
.LCFI155:
	ldr	r0, [r3, #0]
	mov	r4, r1
	ldr	r3, .L481+8
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	cmp	r5, #0
	beq	.L477
	mov	r0, r5
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	beq	.L480
	ldr	r0, .L481+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	sub	r5, r4, #1
	cmp	r5, #11
	mov	r6, r0
	bhi	.L479
	add	r7, sp, #24
	mov	r1, #48
	ldr	r2, .L481+16
	ldr	r3, .L481+20
	mov	r0, r7
	str	r4, [sp, #0]
	bl	snprintf
	mov	r0, #1
	mov	r2, #300
	mov	r3, #100
	stmia	sp, {r0, r2, r3}
	ldr	r3, .L481+24
	add	r1, r4, #24
	str	r3, [sp, #12]
	add	r3, r6, #296
	str	r3, [sp, #16]
	mov	r0, r6
	add	r1, r6, r1, asl #2
	mov	r2, r5
	mov	r3, #12
	str	r7, [sp, #20]
	bl	SHARED_get_uint32_from_array_64_bit_change_bits_group
	mov	r4, r0
	b	.L478
.L479:
	ldr	r0, .L481+4
	ldr	r1, .L481+28
	bl	Alert_index_out_of_range_with_filename
	b	.L480
.L477:
	ldr	r0, .L481+4
	ldr	r1, .L481+32
	bl	Alert_func_call_with_null_ptr_with_filename
.L480:
	mov	r4, #100
.L478:
	ldr	r3, .L481
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L482:
	.align	2
.L481:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5046
	.word	.LANCHOR1
	.word	.LC35
	.word	.LC36
	.word	nm_STATION_GROUP_set_crop_coefficient
	.word	5075
	.word	5083
.LFE130:
	.size	STATION_GROUP_get_station_crop_coefficient_100u, .-STATION_GROUP_get_station_crop_coefficient_100u
	.section	.text.STATION_GROUP_get_crop_coefficient_100u,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_crop_coefficient_100u
	.type	STATION_GROUP_get_crop_coefficient_100u, %function
STATION_GROUP_get_crop_coefficient_100u:
.LFB131:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L487
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI156:
	ldr	r2, .L487+4
	mov	r5, r0
	sub	sp, sp, #72
.LCFI157:
	ldr	r0, [r3, #0]
	mov	r4, r1
	ldr	r3, .L487+8
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	cmp	r5, #0
	beq	.L486
	mov	r0, r5
	bl	STATION_GROUP_get_group_with_this_GID
	sub	r5, r4, #1
	cmp	r5, #11
	mov	r6, r0
	bhi	.L485
	add	r7, sp, #24
	mov	r1, #48
	ldr	r2, .L487+12
	ldr	r3, .L487+16
	mov	r0, r7
	str	r4, [sp, #0]
	bl	snprintf
	mov	r3, #1
	mov	ip, #300
	mov	lr, #100
	stmia	sp, {r3, ip, lr}
	ldr	r3, .L487+20
	add	r1, r4, #24
	str	r3, [sp, #12]
	add	r3, r6, #296
	str	r3, [sp, #16]
	mov	r0, r6
	add	r1, r6, r1, asl #2
	mov	r2, r5
	mov	r3, #12
	str	r7, [sp, #20]
	bl	SHARED_get_uint32_from_array_64_bit_change_bits_group
	mov	r4, r0
	b	.L484
.L485:
	ldr	r0, .L487+4
	ldr	r1, .L487+24
	bl	Alert_index_out_of_range_with_filename
.L486:
	mov	r4, #100
.L484:
	ldr	r3, .L487
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L488:
	.align	2
.L487:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5104
	.word	.LC35
	.word	.LC36
	.word	nm_STATION_GROUP_set_crop_coefficient
	.word	5129
.LFE131:
	.size	STATION_GROUP_get_crop_coefficient_100u, .-STATION_GROUP_get_crop_coefficient_100u
	.section	.text.STATION_GROUP_get_station_usable_rain_percentage_100u,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_station_usable_rain_percentage_100u
	.type	STATION_GROUP_get_station_usable_rain_percentage_100u, %function
STATION_GROUP_get_station_usable_rain_percentage_100u:
.LFB132:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI158:
	ldr	r3, .L493
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L493+4
	ldr	r3, .L493+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	beq	.L492
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	beq	.L492
	mov	r2, #0
	ldr	r0, .L493+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #80
	str	r3, [sp, #0]
	ldr	r3, .L493+16
	mov	r2, #50
	str	r3, [sp, #4]
	add	r3, r0, #296
	str	r3, [sp, #8]
	ldr	r3, .L493+20
	add	r1, r0, #96
	str	r3, [sp, #12]
	mov	r3, #100
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
	b	.L490
.L492:
	mov	r4, #80
.L490:
	ldr	r3, .L493
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L494:
	.align	2
.L493:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5155
	.word	.LANCHOR1
	.word	nm_STATION_GROUP_set_usable_rain
	.word	.LC5
.LFE132:
	.size	STATION_GROUP_get_station_usable_rain_percentage_100u, .-STATION_GROUP_get_station_usable_rain_percentage_100u
	.section	.text.PRIORITY_get_station_priority_level,"ax",%progbits
	.align	2
	.global	PRIORITY_get_station_priority_level
	.type	PRIORITY_get_station_priority_level, %function
PRIORITY_get_station_priority_level:
.LFB133:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI159:
	ldr	r3, .L498
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L498+4
	ldr	r3, .L498+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r4, #1
	beq	.L496
	mov	r2, #0
	ldr	r0, .L498+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	ldr	r3, .L498+16
	mov	r2, #1
	str	r3, [sp, #4]
	str	r2, [sp, #0]
	add	r3, r0, #296
	str	r3, [sp, #8]
	ldr	r3, .L498+20
	add	r1, r0, #148
	str	r3, [sp, #12]
	mov	r3, #3
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
.L496:
	ldr	r3, .L498
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L499:
	.align	2
.L498:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5212
	.word	.LANCHOR1
	.word	nm_PRIORITY_set_priority
	.word	.LC8
.LFE133:
	.size	PRIORITY_get_station_priority_level, .-PRIORITY_get_station_priority_level
	.section	.text.PERCENT_ADJUST_get_station_percentage_100u,"ax",%progbits
	.align	2
	.global	PERCENT_ADJUST_get_station_percentage_100u
	.type	PERCENT_ADJUST_get_station_percentage_100u, %function
PERCENT_ADJUST_get_station_percentage_100u:
.LFB134:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI160:
	ldr	r4, .L503
	mov	r6, r0
	mov	r5, r1
	ldr	r2, .L503+4
	mov	r1, #400
	ldr	r3, .L503+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r6
	bl	STATION_get_GID_station_group
	cmp	r0, #0
	moveq	r5, #100
	beq	.L501
	mov	r1, r5
	bl	PERCENT_ADJUST_get_percentage_100u_for_this_gid
	mov	r5, r0
.L501:
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L504:
	.align	2
.L503:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5268
.LFE134:
	.size	PERCENT_ADJUST_get_station_percentage_100u, .-PERCENT_ADJUST_get_station_percentage_100u
	.section	.text.PERCENT_ADJUST_get_station_remaining_days,"ax",%progbits
	.align	2
	.global	PERCENT_ADJUST_get_station_remaining_days
	.type	PERCENT_ADJUST_get_station_remaining_days, %function
PERCENT_ADJUST_get_station_remaining_days:
.LFB135:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI161:
	ldr	r4, .L508
	ldr	r3, .L508+4
	mov	r6, r0
	mov	r5, r1
	ldr	r2, .L508+8
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r6
	bl	STATION_get_GID_station_group
	subs	r3, r0, #0
	moveq	r5, r3
	beq	.L506
	mov	r1, r5
	bl	PERCENT_ADJUST_get_remaining_days_for_this_gid
	mov	r5, r0
.L506:
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L509:
	.align	2
.L508:
	.word	list_program_data_recursive_MUTEX
	.word	5315
	.word	.LC51
.LFE135:
	.size	PERCENT_ADJUST_get_station_remaining_days, .-PERCENT_ADJUST_get_station_remaining_days
	.section	.text.SCHEDULE_get_station_schedule_type,"ax",%progbits
	.align	2
	.global	SCHEDULE_get_station_schedule_type
	.type	SCHEDULE_get_station_schedule_type, %function
SCHEDULE_get_station_schedule_type:
.LFB136:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI162:
	ldr	r4, .L513
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L513+4
	ldr	r3, .L513+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r5, r1
	beq	.L511
	mov	r2, #0
	ldr	r0, .L513+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	bl	SCHEDULE_get_schedule_type
	mov	r5, r0
.L511:
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L514:
	.align	2
.L513:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5362
	.word	.LANCHOR1
.LFE136:
	.size	SCHEDULE_get_station_schedule_type, .-SCHEDULE_get_station_schedule_type
	.section	.text.SCHEDULE_get_station_waters_this_day,"ax",%progbits
	.align	2
	.global	SCHEDULE_get_station_waters_this_day
	.type	SCHEDULE_get_station_waters_this_day, %function
SCHEDULE_get_station_waters_this_day:
.LFB137:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L519
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI163:
	ldr	r2, .L519+4
	sub	sp, sp, #64
.LCFI164:
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L519+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r4, r1
	beq	.L516
	ldr	r0, .L519+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	cmp	r4, #6
	mov	r5, r0
	bhi	.L517
	mov	r0, r4
	bl	GetDayShortStr
	add	r6, sp, #16
	mov	r1, #48
	ldr	r2, .L519+16
	ldr	r3, .L519+20
	str	r0, [sp, #0]
	mov	r0, r6
	bl	snprintf
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L519+24
	add	r1, r4, #46
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	mov	r2, r4
	mov	r0, r5
	add	r1, r5, r1, asl #2
	mov	r3, #7
	str	r6, [sp, #12]
	bl	SHARED_get_bool_from_array_64_bit_change_bits_group
	mov	r4, r0
	b	.L516
.L517:
	ldr	r0, .L519+4
	mov	r1, #5440
	bl	Alert_index_out_of_range_with_filename
	mov	r4, #0
.L516:
	ldr	r3, .L519
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, pc}
.L520:
	.align	2
.L519:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5415
	.word	.LANCHOR1
	.word	.LC38
	.word	.LC39
	.word	nm_SCHEDULE_set_water_day
.LFE137:
	.size	SCHEDULE_get_station_waters_this_day, .-SCHEDULE_get_station_waters_this_day
	.section	.text.SCHEDULE_get_run_time_calculation_support,"ax",%progbits
	.align	2
	.global	SCHEDULE_get_run_time_calculation_support
	.type	SCHEDULE_get_run_time_calculation_support, %function
SCHEDULE_get_run_time_calculation_support:
.LFB138:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L524
	stmfd	sp!, {r4, r5, lr}
.LCFI165:
	ldr	r2, .L524+4
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L524+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L524+12
	bl	nm_OnList
	cmp	r0, #0
	moveq	r4, r0
	beq	.L522
	ldr	r3, [r4, #164]
	add	r1, r4, #184
	str	r3, [r5, #0]
	ldr	r3, [r4, #168]
	add	r0, r5, #8
	str	r3, [r5, #4]
	mov	r2, #28
	bl	memcpy
	ldr	r3, [r4, #212]
	str	r3, [r5, #36]
	ldr	r3, [r4, #244]
	str	r3, [r5, #40]
	ldr	r3, [r4, #172]
	mov	r4, #1
	str	r3, [r5, #44]
.L522:
	ldr	r3, .L524
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.L525:
	.align	2
.L524:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5466
	.word	.LANCHOR1
.LFE138:
	.size	SCHEDULE_get_run_time_calculation_support, .-SCHEDULE_get_run_time_calculation_support
	.section	.text.WEATHER_get_station_uses_daily_et,"ax",%progbits
	.align	2
	.global	WEATHER_get_station_uses_daily_et
	.type	WEATHER_get_station_uses_daily_et, %function
WEATHER_get_station_uses_daily_et:
.LFB139:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI166:
	ldr	r4, .L529
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L529+4
	ldr	r3, .L529+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r5, r1
	beq	.L527
	mov	r2, #0
	ldr	r0, .L529+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	bl	DAILY_ET_get_et_in_use
	mov	r5, r0
.L527:
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L530:
	.align	2
.L529:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5524
	.word	.LANCHOR1
.LFE139:
	.size	WEATHER_get_station_uses_daily_et, .-WEATHER_get_station_uses_daily_et
	.section	.text.WEATHER_get_station_uses_et_averaging,"ax",%progbits
	.align	2
	.global	WEATHER_get_station_uses_et_averaging
	.type	WEATHER_get_station_uses_et_averaging, %function
WEATHER_get_station_uses_et_averaging:
.LFB140:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L536
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI167:
	ldr	r2, .L536+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L536+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	beq	.L534
	ldr	r0, .L536+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r1, r0, #0
	beq	.L533
	ldr	r2, [r1, #240]
	cmp	r2, #1
	movne	r4, #0
	bne	.L532
	add	r3, r1, #296
	str	r3, [sp, #0]
	ldr	r3, .L536+16
	add	r1, r1, #244
	str	r3, [sp, #4]
	ldr	r3, .L536+20
	bl	SHARED_get_bool_64_bit_change_bits_group
	mov	r4, r0
	b	.L532
.L533:
	ldr	r0, .L536+4
	ldr	r1, .L536+24
	bl	Alert_station_not_in_group_with_filename
.L534:
	mov	r4, #1
.L532:
	ldr	r3, .L536
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r2, r3, r4, pc}
.L537:
	.align	2
.L536:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5573
	.word	.LANCHOR1
	.word	.LC21
	.word	nm_DAILY_ET_set_use_ET_averaging
	.word	5602
.LFE140:
	.size	WEATHER_get_station_uses_et_averaging, .-WEATHER_get_station_uses_et_averaging
	.section	.text.WEATHER_get_station_uses_rain,"ax",%progbits
	.align	2
	.global	WEATHER_get_station_uses_rain
	.type	WEATHER_get_station_uses_rain, %function
WEATHER_get_station_uses_rain:
.LFB141:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI168:
	ldr	r4, .L541
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L541+4
	ldr	r3, .L541+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r5, r1
	beq	.L539
	mov	r2, #0
	ldr	r0, .L541+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	bl	RAIN_get_rain_in_use
	mov	r5, r0
.L539:
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L542:
	.align	2
.L541:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5645
	.word	.LANCHOR1
.LFE141:
	.size	WEATHER_get_station_uses_rain, .-WEATHER_get_station_uses_rain
	.section	.text.STATION_GROUP_get_soil_storage_capacity_inches_100u,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_soil_storage_capacity_inches_100u
	.type	STATION_GROUP_get_soil_storage_capacity_inches_100u, %function
STATION_GROUP_get_soil_storage_capacity_inches_100u:
.LFB142:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI169:
	ldr	r3, .L546
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L546+4
	ldr	r3, .L546+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r4, #150
	beq	.L544
	mov	r2, #0
	ldr	r0, .L546+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #150
	str	r3, [sp, #0]
	ldr	r3, .L546+16
	mov	r2, #1
	str	r3, [sp, #4]
	add	r3, r0, #296
	str	r3, [sp, #8]
	ldr	r3, .L546+20
	add	r1, r0, #252
	str	r3, [sp, #12]
	mov	r3, #600
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
.L544:
	ldr	r3, .L546
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L547:
	.align	2
.L546:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5695
	.word	.LANCHOR1
	.word	nm_STATION_GROUP_set_soil_storage_capacity
	.word	.LC37
.LFE142:
	.size	STATION_GROUP_get_soil_storage_capacity_inches_100u, .-STATION_GROUP_get_soil_storage_capacity_inches_100u
	.section	.text.WEATHER_get_station_uses_wind,"ax",%progbits
	.align	2
	.global	WEATHER_get_station_uses_wind
	.type	WEATHER_get_station_uses_wind, %function
WEATHER_get_station_uses_wind:
.LFB143:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L551
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI170:
	ldr	r2, .L551+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L551+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r4, r1
	beq	.L549
	mov	r2, #0
	ldr	r0, .L551+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r2, #0
	add	r3, r0, #296
	str	r3, [sp, #0]
	ldr	r3, .L551+16
	add	r1, r0, #256
	str	r3, [sp, #4]
	ldr	r3, .L551+20
	bl	SHARED_get_bool_64_bit_change_bits_group
	mov	r4, r0
.L549:
	ldr	r3, .L551
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r2, r3, r4, pc}
.L552:
	.align	2
.L551:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5752
	.word	.LANCHOR1
	.word	.LC23
	.word	nm_WIND_set_wind_in_use
.LFE143:
	.size	WEATHER_get_station_uses_wind, .-WEATHER_get_station_uses_wind
	.section	.text.ALERT_ACTIONS_get_station_high_flow_action,"ax",%progbits
	.align	2
	.global	ALERT_ACTIONS_get_station_high_flow_action
	.type	ALERT_ACTIONS_get_station_high_flow_action, %function
ALERT_ACTIONS_get_station_high_flow_action:
.LFB144:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI171:
	ldr	r3, .L556
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L556+4
	ldr	r3, .L556+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r4, #1
	beq	.L554
	mov	r2, #0
	ldr	r0, .L556+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L556+16
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r0, #296
	str	r3, [sp, #8]
	ldr	r3, .L556+20
	add	r1, r0, #260
	str	r3, [sp, #12]
	mov	r3, #2
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
.L554:
	ldr	r3, .L556
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L557:
	.align	2
.L556:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5805
	.word	.LANCHOR1
	.word	nm_ALERT_ACTIONS_set_high_flow_action
	.word	.LC12
.LFE144:
	.size	ALERT_ACTIONS_get_station_high_flow_action, .-ALERT_ACTIONS_get_station_high_flow_action
	.section	.text.ALERT_ACTIONS_get_station_low_flow_action,"ax",%progbits
	.align	2
	.global	ALERT_ACTIONS_get_station_low_flow_action
	.type	ALERT_ACTIONS_get_station_low_flow_action, %function
ALERT_ACTIONS_get_station_low_flow_action:
.LFB145:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI172:
	ldr	r3, .L561
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L561+4
	ldr	r3, .L561+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r4, #1
	beq	.L559
	mov	r2, #0
	ldr	r0, .L561+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L561+16
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r0, #296
	str	r3, [sp, #8]
	ldr	r3, .L561+20
	add	r1, r0, #264
	str	r3, [sp, #12]
	mov	r3, #2
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
.L559:
	ldr	r3, .L561
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L562:
	.align	2
.L561:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5860
	.word	.LANCHOR1
	.word	nm_ALERT_ACTIONS_set_low_flow_action
	.word	.LC13
.LFE145:
	.size	ALERT_ACTIONS_get_station_low_flow_action, .-ALERT_ACTIONS_get_station_low_flow_action
	.section	.text.PUMP_get_station_uses_pump,"ax",%progbits
	.align	2
	.global	PUMP_get_station_uses_pump
	.type	PUMP_get_station_uses_pump, %function
PUMP_get_station_uses_pump:
.LFB146:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L566
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI173:
	ldr	r2, .L566+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L566+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r4, #1
	beq	.L564
	mov	r2, #0
	ldr	r0, .L566+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r2, #1
	add	r3, r0, #296
	str	r3, [sp, #0]
	ldr	r3, .L566+16
	add	r1, r0, #280
	str	r3, [sp, #4]
	ldr	r3, .L566+20
	bl	SHARED_get_bool_64_bit_change_bits_group
	mov	r4, r0
.L564:
	ldr	r3, .L566
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r2, r3, r4, pc}
.L567:
	.align	2
.L566:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5917
	.word	.LANCHOR1
	.word	.LC24
	.word	nm_PUMP_set_pump_in_use
.LFE146:
	.size	PUMP_get_station_uses_pump, .-PUMP_get_station_uses_pump
	.section	.text.LINE_FILL_TIME_get_station_line_fill_seconds,"ax",%progbits
	.align	2
	.global	LINE_FILL_TIME_get_station_line_fill_seconds
	.type	LINE_FILL_TIME_get_station_line_fill_seconds, %function
LINE_FILL_TIME_get_station_line_fill_seconds:
.LFB147:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI174:
	ldr	r3, .L571
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L571+4
	ldr	r3, .L571+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r4, #120
	beq	.L569
	mov	r2, #0
	ldr	r0, .L571+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r3, #120
	str	r3, [sp, #0]
	ldr	r3, .L571+16
	mov	r2, #15
	str	r3, [sp, #4]
	add	r3, r0, #296
	str	r3, [sp, #8]
	ldr	r3, .L571+20
	add	r1, r0, #284
	str	r3, [sp, #12]
	ldr	r3, .L571+24
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
.L569:
	ldr	r3, .L571
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L572:
	.align	2
.L571:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	5972
	.word	.LANCHOR1
	.word	nm_LINE_FILL_TIME_set_line_fill_time
	.word	.LC14
	.word	1800
.LFE147:
	.size	LINE_FILL_TIME_get_station_line_fill_seconds, .-LINE_FILL_TIME_get_station_line_fill_seconds
	.section	.text.DELAY_BETWEEN_VALVES_get_station_slow_closing_valve_seconds,"ax",%progbits
	.align	2
	.global	DELAY_BETWEEN_VALVES_get_station_slow_closing_valve_seconds
	.type	DELAY_BETWEEN_VALVES_get_station_slow_closing_valve_seconds, %function
DELAY_BETWEEN_VALVES_get_station_slow_closing_valve_seconds:
.LFB148:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI175:
	ldr	r3, .L576
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L576+4
	ldr	r3, .L576+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r4, r1
	beq	.L574
	mov	r2, #0
	ldr	r0, .L576+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	ldr	r3, .L576+16
	mov	r2, #0
	str	r3, [sp, #4]
	str	r2, [sp, #0]
	add	r3, r0, #296
	str	r3, [sp, #8]
	ldr	r3, .L576+20
	add	r1, r0, #288
	str	r3, [sp, #12]
	mov	r3, #120
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
.L574:
	ldr	r3, .L576
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L577:
	.align	2
.L576:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6029
	.word	.LANCHOR1
	.word	nm_DELAY_BETWEEN_VALVES_set_delay_time
	.word	.LC15
.LFE148:
	.size	DELAY_BETWEEN_VALVES_get_station_slow_closing_valve_seconds, .-DELAY_BETWEEN_VALVES_get_station_slow_closing_valve_seconds
	.section	.text.ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow,"ax",%progbits
	.align	2
	.global	ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow
	.type	ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow, %function
ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow:
.LFB149:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L581
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI176:
	ldr	r2, .L581+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L581+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r1, r0, #0
	moveq	r4, r1
	beq	.L579
	mov	r2, #0
	ldr	r0, .L581+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r2, #0
	add	r3, r0, #296
	str	r3, [sp, #0]
	ldr	r3, .L581+16
	add	r1, r0, #292
	str	r3, [sp, #4]
	ldr	r3, .L581+20
	bl	SHARED_get_bool_64_bit_change_bits_group
	mov	r4, r0
.L579:
	ldr	r3, .L581
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r2, r3, r4, pc}
.L582:
	.align	2
.L581:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6069
	.word	.LANCHOR1
	.word	.LC26
	.word	nm_ACQUIRE_EXPECTEDS_set_acquire_expected
.LFE149:
	.size	ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow, .-ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow
	.section	.text.STATION_GROUP_get_GID_irrigation_system_for_this_station,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_GID_irrigation_system_for_this_station
	.type	STATION_GROUP_get_GID_irrigation_system_for_this_station, %function
STATION_GROUP_get_GID_irrigation_system_for_this_station:
.LFB150:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI177:
	ldr	r3, .L585
	mov	r1, #400
	ldr	r2, .L585+4
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L585+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_get_GID_station_group
	mov	r2, #1
	mov	r1, r0
	ldr	r0, .L585+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r5, r0, #0
	bne	.L584
	ldr	r0, .L585+12
	bl	nm_ListGetFirst
	mov	r5, r0
.L584:
	bl	SYSTEM_get_last_group_ID
	mov	r4, r0
	mov	r0, #0
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L585+16
	add	r1, r5, #364
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	ldr	r3, .L585+20
	mov	r2, #1
	str	r3, [sp, #12]
	mov	r3, r4
	str	r0, [sp, #0]
	mov	r0, r5
	bl	SHARED_get_uint32_64_bit_change_bits_group
	ldr	r3, .L585
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L586:
	.align	2
.L585:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6103
	.word	.LANCHOR1
	.word	nm_STATION_GROUP_set_GID_irrigation_system
	.word	.LC40
.LFE150:
	.size	STATION_GROUP_get_GID_irrigation_system_for_this_station, .-STATION_GROUP_get_GID_irrigation_system_for_this_station
	.section	.text.STATION_GROUP_get_GID_irrigation_system,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_GID_irrigation_system
	.type	STATION_GROUP_get_GID_irrigation_system, %function
STATION_GROUP_get_GID_irrigation_system:
.LFB151:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI178:
	ldr	r4, .L588
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L588+4
	ldr	r3, .L588+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_get_last_group_ID
	mov	r6, r0
	mov	r0, #0
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L588+12
	add	r1, r5, #364
	str	r3, [sp, #4]
	add	r3, r5, #296
	str	r3, [sp, #8]
	ldr	r3, .L588+16
	mov	r2, #1
	str	r3, [sp, #12]
	mov	r3, r6
	str	r0, [sp, #0]
	mov	r0, r5
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, pc}
.L589:
	.align	2
.L588:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6137
	.word	nm_STATION_GROUP_set_GID_irrigation_system
	.word	.LC40
.LFE151:
	.size	STATION_GROUP_get_GID_irrigation_system, .-STATION_GROUP_get_GID_irrigation_system
	.section	.text.STATION_GROUP_get_budget_reduction_limit,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_budget_reduction_limit
	.type	STATION_GROUP_get_budget_reduction_limit, %function
STATION_GROUP_get_budget_reduction_limit:
.LFB152:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI179:
	ldr	r3, .L593
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L593+4
	ldr	r3, .L593+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	beq	.L591
	mov	r3, #25
	str	r3, [sp, #0]
	ldr	r3, .L593+12
	mov	r0, r4
	str	r3, [sp, #4]
	add	r3, r4, #296
	str	r3, [sp, #8]
	ldr	r3, .L593+16
	add	r1, r4, #360
	str	r3, [sp, #12]
	mov	r2, #0
	mov	r3, #100
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
.L591:
	ldr	r3, .L593
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L594:
	.align	2
.L593:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6162
	.word	nm_STATION_GROUP_set_budget_reduction_percentage_cap
	.word	.LC16
.LFE152:
	.size	STATION_GROUP_get_budget_reduction_limit, .-STATION_GROUP_get_budget_reduction_limit
	.section	.text.STATION_GROUP_get_budget_reduction_limit_for_this_station,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_budget_reduction_limit_for_this_station
	.type	STATION_GROUP_get_budget_reduction_limit_for_this_station, %function
STATION_GROUP_get_budget_reduction_limit_for_this_station:
.LFB153:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI180:
	ldr	r4, .L596
	ldr	r3, .L596+4
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L596+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	STATION_get_GID_station_group
	mov	r2, #1
	mov	r1, r0
	ldr	r0, .L596+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	bl	STATION_GROUP_get_budget_reduction_limit
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L597:
	.align	2
.L596:
	.word	list_program_data_recursive_MUTEX
	.word	6192
	.word	.LC51
	.word	.LANCHOR1
.LFE153:
	.size	STATION_GROUP_get_budget_reduction_limit_for_this_station, .-STATION_GROUP_get_budget_reduction_limit_for_this_station
	.section	.text.nm_STATION_GROUP_set_budget_start_time_flag,"ax",%progbits
	.align	2
	.global	nm_STATION_GROUP_set_budget_start_time_flag
	.type	nm_STATION_GROUP_set_budget_start_time_flag, %function
nm_STATION_GROUP_set_budget_start_time_flag:
.LFB154:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r1, [r0, #228]
	bx	lr
.LFE154:
	.size	nm_STATION_GROUP_set_budget_start_time_flag, .-nm_STATION_GROUP_set_budget_start_time_flag
	.section	.text.nm_STATION_GROUP_get_budget_start_time_flag,"ax",%progbits
	.align	2
	.global	nm_STATION_GROUP_get_budget_start_time_flag
	.type	nm_STATION_GROUP_get_budget_start_time_flag, %function
nm_STATION_GROUP_get_budget_start_time_flag:
.LFB155:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, [r0, #228]
	bx	lr
.LFE155:
	.size	nm_STATION_GROUP_get_budget_start_time_flag, .-nm_STATION_GROUP_get_budget_start_time_flag
	.section	.text.STATION_GROUP_clear_budget_start_time_flags,"ax",%progbits
	.align	2
	.global	STATION_GROUP_clear_budget_start_time_flags
	.type	STATION_GROUP_clear_budget_start_time_flags, %function
STATION_GROUP_clear_budget_start_time_flags:
.LFB156:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L603
	stmfd	sp!, {r4, lr}
.LCFI181:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L603+4
	ldr	r3, .L603+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L603+12
	bl	nm_ListGetFirst
.LBB48:
	mov	r4, #0
.LBE48:
	mov	r1, r0
	b	.L601
.L602:
.LBB49:
	str	r4, [r1, #228]
.LBE49:
	ldr	r0, .L603+12
	bl	nm_ListGetNext
	mov	r1, r0
.L601:
	cmp	r1, #0
	bne	.L602
	ldr	r3, .L603
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L604:
	.align	2
.L603:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6232
	.word	.LANCHOR1
.LFE156:
	.size	STATION_GROUP_clear_budget_start_time_flags, .-STATION_GROUP_clear_budget_start_time_flags
	.section	.text.ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid,"ax",%progbits
	.align	2
	.global	ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid
	.type	ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid, %function
ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid:
.LFB157:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI182:
	subs	r4, r0, #0
	moveq	r4, #1
	beq	.L606
	ldr	r3, .L611
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L611+4
	ldr	r3, .L611+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L611+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r1, r0, #0
	beq	.L607
	ldr	r4, [r1, #272]
	cmn	r4, #-2147483647
	beq	.L608
	ldr	r3, .L611+16
	mov	r2, #1
	str	r3, [sp, #4]
	add	r3, r1, #296
	str	r3, [sp, #8]
	ldr	r3, .L611+20
	add	r1, r1, #272
	str	r3, [sp, #12]
	mov	r3, #30
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
	b	.L608
.L607:
	ldr	r0, .L611+4
	ldr	r1, .L611+24
	bl	Alert_station_not_in_group_with_filename
	mov	r4, #1
.L608:
	ldr	r3, .L611
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L606:
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L612:
	.align	2
.L611:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6278
	.word	.LANCHOR1
	.word	nm_ON_AT_A_TIME_set_on_at_a_time_in_system
	.word	.LC30
	.word	6302
.LFE157:
	.size	ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid, .-ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid
	.section	.text.ON_AT_A_TIME_init_all_groups_ON_count,"ax",%progbits
	.align	2
	.global	ON_AT_A_TIME_init_all_groups_ON_count
	.type	ON_AT_A_TIME_init_all_groups_ON_count, %function
ON_AT_A_TIME_init_all_groups_ON_count:
.LFB158:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L616
	stmfd	sp!, {r4, lr}
.LCFI183:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L616+4
	mov	r3, #6336
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L616+8
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L614
.L615:
	str	r4, [r1, #276]
	ldr	r0, .L616+8
	bl	nm_ListGetNext
	mov	r1, r0
.L614:
	cmp	r1, #0
	bne	.L615
	ldr	r3, .L616
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L617:
	.align	2
.L616:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	.LANCHOR1
.LFE158:
	.size	ON_AT_A_TIME_init_all_groups_ON_count, .-ON_AT_A_TIME_init_all_groups_ON_count
	.section	.text.ON_AT_A_TIME_can_another_valve_come_ON_within_this_group,"ax",%progbits
	.align	2
	.global	ON_AT_A_TIME_can_another_valve_come_ON_within_this_group
	.type	ON_AT_A_TIME_can_another_valve_come_ON_within_this_group, %function
ON_AT_A_TIME_can_another_valve_come_ON_within_this_group:
.LFB159:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI184:
	subs	r4, r0, #0
	beq	.L619
	ldr	r3, .L625
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L625+4
	ldr	r3, .L625+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L625+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	beq	.L620
	ldr	r2, [r4, #276]
	ldr	r3, [r4, #268]
	cmp	r2, r3
	movcc	r4, #1
	bcc	.L621
	bls	.L624
	ldr	r0, .L625+4
	bl	RemovePathFromFileName
	ldr	r2, .L625+16
	mov	r1, r0
	ldr	r0, .L625+20
	bl	Alert_Message_va
	b	.L624
.L620:
	ldr	r0, .L625+4
	ldr	r1, .L625+24
	bl	Alert_group_not_found_with_filename
	b	.L621
.L624:
	mov	r4, #0
.L621:
	ldr	r3, .L625
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L619:
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L626:
	.align	2
.L625:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6394
	.word	.LANCHOR1
	.word	6412
	.word	.LC75
	.word	6418
.LFE159:
	.size	ON_AT_A_TIME_can_another_valve_come_ON_within_this_group, .-ON_AT_A_TIME_can_another_valve_come_ON_within_this_group
	.section	.text.ON_AT_A_TIME_bump_this_groups_ON_count,"ax",%progbits
	.align	2
	.global	ON_AT_A_TIME_bump_this_groups_ON_count
	.type	ON_AT_A_TIME_bump_this_groups_ON_count, %function
ON_AT_A_TIME_bump_this_groups_ON_count:
.LFB160:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI185:
	subs	r4, r0, #0
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L631
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L631+4
	ldr	r3, .L631+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L631+12
	mov	r1, r4
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	cmp	r0, #0
	beq	.L629
	ldr	r3, [r0, #276]
	ldr	r2, [r0, #268]
	add	r3, r3, #1
	cmp	r3, r2
	str	r3, [r0, #276]
	bls	.L630
	ldr	r0, .L631+4
	bl	RemovePathFromFileName
	ldr	r2, .L631+16
	mov	r1, r0
	ldr	r0, .L631+20
	bl	Alert_Message_va
	b	.L630
.L629:
	ldr	r0, .L631+4
	ldr	r1, .L631+24
	bl	Alert_group_not_found_with_filename
.L630:
	ldr	r3, .L631
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L632:
	.align	2
.L631:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6454
	.word	.LANCHOR1
	.word	6466
	.word	.LC76
	.word	6471
.LFE160:
	.size	ON_AT_A_TIME_bump_this_groups_ON_count, .-ON_AT_A_TIME_bump_this_groups_ON_count
	.section	.text.STATION_GROUP_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	STATION_GROUP_get_change_bits_ptr
	.type	STATION_GROUP_get_change_bits_ptr, %function
STATION_GROUP_get_change_bits_ptr:
.LFB162:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r0
	add	r2, r3, #304
	mov	r0, r1
	add	r1, r3, #296
	add	r3, r3, #312
	b	SHARED_get_64_bit_change_bits_ptr
.LFE162:
	.size	STATION_GROUP_get_change_bits_ptr, .-STATION_GROUP_get_change_bits_ptr
	.section	.text.ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_for_GID,"ax",%progbits
	.align	2
	.global	ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_for_GID
	.type	ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_for_GID, %function
ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_for_GID:
.LFB161:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI186:
	subs	r4, r0, #0
	beq	.L634
	ldr	r3, .L637
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L637+4
	ldr	r3, .L637+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r4
	ldr	r0, .L637+12
	mov	r2, #0
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	subs	r4, r0, #0
	beq	.L636
	mov	r1, #6
	bl	STATION_GROUP_get_change_bits_ptr
	mov	r5, r0
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	mov	r1, #0
	mov	r3, #6
	stmib	sp, {r2, r5}
	str	r0, [sp, #0]
	mov	r0, r4
	bl	nm_ACQUIRE_EXPECTEDS_set_acquire_expected
.L636:
	ldr	r3, .L637
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L634:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, pc}
.L638:
	.align	2
.L637:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6493
	.word	.LANCHOR1
.LFE161:
	.size	ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_for_GID, .-ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_for_GID
	.section	.text.SCHEDULE_does_it_irrigate_on_this_date,"ax",%progbits
	.align	2
	.global	SCHEDULE_does_it_irrigate_on_this_date
	.type	SCHEDULE_does_it_irrigate_on_this_date, %function
SCHEDULE_does_it_irrigate_on_this_date:
.LFB121:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #180]
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI187:
	mov	r8, r1
	sub	sp, sp, #32
.LCFI188:
	mov	r1, #6
	mov	r5, r0
	mov	r6, r2
	str	r3, [sp, #28]
	bl	STATION_GROUP_get_change_bits_ptr
	ldr	r3, .L695
	mov	r1, #400
	ldr	r2, .L695+4
	mov	r7, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L695+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L695+12
	mov	r1, r5
	bl	nm_OnList
	cmp	r0, #1
	bne	.L640
	ldr	r3, [r5, #168]
	cmp	r3, #18
	ldrls	pc, [pc, r3, asl #2]
	b	.L641
.L649:
	.word	.L642
	.word	.L643
	.word	.L644
	.word	.L645
	.word	.L646
	.word	.L646
	.word	.L646
	.word	.L646
	.word	.L646
	.word	.L646
	.word	.L646
	.word	.L646
	.word	.L646
	.word	.L646
	.word	.L646
	.word	.L646
	.word	.L646
	.word	.L647
	.word	.L648
.L642:
	ldrh	r3, [r8, #4]
	add	r3, r3, #1
	b	.L693
.L643:
	ldrh	r3, [r8, #6]
	tst	r3, #1
	bne	.L683
	cmp	r6, #2
	bne	.L680
.L684:
	ldr	r0, [sp, #28]
	add	r3, sp, #24
	add	r0, r0, #1
	str	r3, [sp, #0]
	add	r1, sp, #12
	add	r3, sp, #20
	add	r2, sp, #16
	str	r0, [sp, #28]
	bl	DateToDMY
	ldr	r3, [sp, #12]
	tst	r3, #1
	bne	.L684
	b	.L680
.L644:
	ldrh	r3, [r8, #6]
	ands	r4, r3, #1
	beq	.L652
	cmp	r3, #1
	bne	.L653
	ldrh	r3, [r8, #8]
	sub	r2, r3, #1
	mov	r2, r2, asl #16
	cmp	r3, #4
	cmpne	r2, #65536
	bls	.L654
	cmp	r3, #6
	beq	.L654
	cmp	r3, #8
	beq	.L654
	cmp	r3, #9
	beq	.L654
	cmp	r3, #11
	bne	.L653
.L654:
	ldr	r4, [r5, #212]
	cmp	r4, #0
	movne	r4, #1
	moveq	r4, #0
	b	.L652
.L653:
	ldrh	r0, [r8, #10]
	bl	IsLeapYear
	cmp	r0, #1
	movne	r4, #1
	bne	.L652
	ldrh	r3, [r8, #6]
	cmp	r3, #1
	movne	r4, r0
	bne	.L652
	ldrh	r2, [r8, #8]
	cmp	r2, #3
	movne	r4, r3
	bne	.L652
	ldr	r4, [r5, #212]
	cmp	r4, #1
	moveq	r4, #1
	movne	r4, #0
.L652:
	cmp	r6, #2
	movne	r3, #0
	andeq	r3, r4, #1
	cmp	r3, #0
	beq	.L650
.L685:
	ldr	r0, [sp, #28]
	add	r3, sp, #24
	add	r0, r0, #1
	str	r3, [sp, #0]
	add	r1, sp, #12
	add	r3, sp, #20
	add	r2, sp, #16
	str	r0, [sp, #28]
	bl	DateToDMY
	ldr	r3, [sp, #12]
	tst	r3, #1
	beq	.L685
	cmp	r3, #1
	bne	.L656
	ldr	r3, [sp, #16]
	sub	r2, r3, #1
	cmp	r3, #4
	cmpne	r2, #1
	bls	.L657
	cmp	r3, #6
	beq	.L657
	cmp	r3, #8
	beq	.L657
	cmp	r3, #9
	beq	.L657
	cmp	r3, #11
	bne	.L656
.L657:
	ldr	r3, [r5, #212]
	cmp	r3, #0
	beq	.L680
	ldr	r3, [sp, #28]
	add	r3, r3, #2
.L693:
	str	r3, [sp, #28]
	b	.L680
.L656:
	ldr	r0, [sp, #20]
	bl	IsLeapYear
	cmp	r0, #1
	bne	.L680
	ldr	r3, [sp, #12]
	cmp	r3, #1
	movne	r4, r0
	bne	.L650
	ldr	r2, [sp, #16]
	cmp	r2, #3
	bne	.L678
	ldr	r4, [r5, #212]
	cmp	r4, #1
	ldreq	r3, [sp, #28]
	addeq	r3, r3, #2
	streq	r3, [sp, #28]
	bne	.L678
	b	.L650
.L645:
	ldrb	r3, [r8, #18]	@ zero_extendqisi2
	add	r3, r3, #46
	ldr	r4, [r5, r3, asl #2]
	cmp	r4, #0
	beq	.L650
	cmp	r6, #2
.L694:
	bne	.L680
	ldr	r0, [sp, #28]
	add	r3, sp, #24
	add	r0, r0, #1
	str	r3, [sp, #0]
	add	r1, sp, #12
	add	r3, sp, #20
	add	r2, sp, #16
	str	r0, [sp, #28]
	bl	DateToDMY
	ldr	r3, [sp, #24]
	add	r3, r3, #46
	ldr	r3, [r5, r3, asl #2]
	cmp	r3, #0
	b	.L694
.L646:
	ldrh	r2, [r8, #4]
	ldr	r1, [r5, #180]
	cmp	r2, r1
	bls	.L659
	sub	r1, r3, #2
	ldr	r3, [sp, #28]
.L660:
.LBB50:
	add	r3, r3, r1
	cmp	r3, r2
	bcc	.L660
	str	r3, [sp, #28]
	sub	r3, r6, #1
	cmp	r3, #1
	bhi	.L659
	mov	r0, r5
	add	r1, sp, #28
	mov	r2, r7
	bl	ripple_begin_date_when_irrigating_every_third_day_or_greater.part.5
.L659:
.LBE50:
	ldrh	r2, [r8, #4]
	ldr	r3, [r5, #180]
	cmp	r2, r3
	bne	.L683
	ldr	r3, [r5, #168]
	ldr	r2, [sp, #28]
	add	r3, r2, r3
	sub	r3, r3, #2
	b	.L693
.L647:
	ldrh	r2, [r8, #4]
	ldr	r3, [r5, #180]
	cmp	r2, r3
	bls	.L661
	ldr	r3, [sp, #28]
.L662:
.LBB51:
	add	r3, r3, #21
	cmp	r3, r2
	bcc	.L662
	str	r3, [sp, #28]
	sub	r3, r6, #1
	cmp	r3, #1
	bhi	.L661
	mov	r0, r5
	add	r1, sp, #28
	mov	r2, r7
	bl	ripple_begin_date_when_irrigating_every_third_day_or_greater.part.5
.L661:
.LBE51:
	ldr	r3, [r5, #180]
	ldrh	r2, [r8, #4]
	cmp	r2, r3
	ldreq	r3, [sp, #28]
	addeq	r3, r3, #21
	bne	.L683
	b	.L693
.L648:
	ldrh	r2, [r8, #4]
	ldr	r3, [r5, #180]
	cmp	r2, r3
	bls	.L663
	ldr	r3, [sp, #28]
.L664:
.LBB52:
	add	r3, r3, #28
	cmp	r3, r2
	bcc	.L664
	str	r3, [sp, #28]
	sub	r3, r6, #1
	cmp	r3, #1
	bhi	.L663
	mov	r0, r5
	add	r1, sp, #28
	mov	r2, r7
	bl	ripple_begin_date_when_irrigating_every_third_day_or_greater.part.5
.L663:
.LBE52:
	ldr	r3, [r5, #180]
	ldrh	r2, [r8, #4]
	cmp	r2, r3
	ldreq	r3, [sp, #28]
	addeq	r3, r3, #28
	bne	.L683
	b	.L693
.L641:
	ldr	r0, .L695+16
	bl	Alert_Message
	b	.L683
.L678:
	mov	r4, r3
	b	.L650
.L680:
	mov	r4, #1
	b	.L650
.L683:
	mov	r4, #0
.L650:
	ldr	r8, [sp, #28]
	ldr	r3, [r5, #180]
	sub	r2, r6, #2
	rsbs	r6, r2, #0
	adc	r6, r6, r2
	cmp	r8, r3
	moveq	r6, #0
	cmp	r6, #0
	beq	.L665
	bl	FLOWSENSE_get_controller_index
	mov	r3, #1
	stmib	sp, {r3, r7}
	mov	r1, r8
	mov	r2, #0
	mov	r3, #6
	str	r0, [sp, #0]
	mov	r0, r5
	bl	nm_SCHEDULE_set_begin_date
	b	.L665
.L640:
	ldr	r0, .L695+20
	ldr	r1, .L695+24
	ldr	r2, .L695+4
	ldr	r3, .L695+28
	bl	Alert_item_not_on_list_with_filename
	mov	r4, #0
.L665:
	ldr	r3, .L695
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L696:
	.align	2
.L695:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	4537
	.word	.LANCHOR1
	.word	.LC77
	.word	.LC78
	.word	.LC79
	.word	4752
.LFE121:
	.size	SCHEDULE_does_it_irrigate_on_this_date, .-SCHEDULE_does_it_irrigate_on_this_date
	.section	.text.nm_STATION_GROUP_store_changes,"ax",%progbits
	.align	2
	.type	nm_STATION_GROUP_store_changes, %function
nm_STATION_GROUP_store_changes:
.LFB46:
	@ args = 16, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI189:
	mov	r7, r0
	sub	sp, sp, #48
.LCFI190:
	mov	fp, r1
	mov	r4, r2
	ldr	r0, .L801
	mov	r1, r7
	add	r2, sp, #44
	mov	r8, r3
	ldr	r9, [sp, #84]
	bl	nm_GROUP_find_this_group_in_list
	ldr	r3, [sp, #44]
	cmp	r3, #0
	beq	.L698
	cmp	fp, #1
	bne	.L699
	mov	r0, r7
	bl	nm_GROUP_get_name
	mov	r3, #48
	mov	r2, #0
	stmia	sp, {r3, r4, r8}
	mov	r3, r2
	mov	r1, r0
	mov	r0, #49152
	bl	Alert_ChangeLine_Group
.L699:
	ldr	r0, [sp, #44]
	ldr	r1, [sp, #88]
	bl	STATION_GROUP_get_change_bits_ptr
	cmp	r4, #2
	mov	sl, r0
	beq	.L700
	ldr	r0, [sp, #92]
	tst	r0, #1
	beq	.L701
.L700:
	mov	r0, r7
	bl	nm_GROUP_get_name
	stmia	sp, {r8, r9, sl}
	mov	r1, r0
	ldr	r0, [sp, #44]
	add	r3, r0, #312
	str	r3, [sp, #12]
	mov	r3, #0
	rsb	lr, r3, fp
	rsbs	r2, lr, #0
	str	r3, [sp, #16]
	adc	r2, r2, lr
	mov	r3, r4
	bl	SHARED_set_name_64_bit_change_bits
	cmp	r4, #2
	beq	.L702
.L701:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #2
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L703
.L702:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #72]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_plant_type
	cmp	r4, #2
	beq	.L704
.L703:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #4
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L705
.L704:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #76]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_head_type
	cmp	r4, #2
	beq	.L706
.L705:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #8
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L707
.L706:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #80]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_precip_rate
	cmp	r4, #2
	beq	.L708
.L707:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #16
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L709
.L708:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #84]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_soil_type
	cmp	r4, #2
	beq	.L710
.L709:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #32
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L711
.L710:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #88]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_slope_percentage
	cmp	r4, #2
	beq	.L712
.L711:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #64
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L713
.L712:
.LBB53:
	mov	r3, #4
	str	r3, [sp, #0]
	mov	r3, #0
.LBE53:
	rsb	ip, r3, fp
	rsbs	r2, ip, #0
	adc	r2, r2, ip
	ldr	r0, [sp, #44]
.LBB54:
	str	r2, [sp, #8]
	ldr	r2, .L801+4
	add	r1, r0, #92
	str	r2, [sp, #12]
	add	r2, r0, #312
	str	r2, [sp, #32]
	mov	r2, #6
	str	r2, [sp, #36]
	ldr	r2, .L801+8
	str	r3, [sp, #4]
	str	r2, [sp, #40]
	ldr	r2, [r7, #92]
	str	r4, [sp, #16]
	str	r8, [sp, #20]
	str	r9, [sp, #24]
	str	sl, [sp, #28]
	bl	SHARED_set_uint32_with_64_bit_change_bits_group
.LBE54:
	cmp	r4, #2
	beq	.L714
.L713:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #128
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L715
.L714:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #96]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_usable_rain
	cmp	r4, #2
	beq	.L716
.L715:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #268435456
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L717
.L716:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #252]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_soil_storage_capacity
	cmp	r4, #2
	bne	.L717
.L719:
	mov	r6, r7
	mov	r5, #0
	b	.L718
.L717:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #256
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	bne	.L719
	b	.L720
.L718:
	rsbs	r3, fp, #1
	stmia	sp, {r4, r8, r9, sl}
	ldr	r0, [sp, #44]
	mov	r1, r5
	ldr	r2, [r6, #100]
	movcc	r3, #0
	add	r5, r5, #1
	bl	nm_STATION_GROUP_set_crop_coefficient
	cmp	r5, #12
	add	r6, r6, #4
	bne	.L718
	cmp	r4, #2
	beq	.L721
.L720:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #512
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L722
.L721:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #148]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_PRIORITY_set_priority
	cmp	r4, #2
	beq	.L723
.L722:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #1024
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L724
.L723:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #152]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_PERCENT_ADJUST_set_percent
	cmp	r4, #2
	beq	.L725
.L724:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #2048
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L726
.L725:
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #156]
	mov	r2, r4
	mov	r3, r8
	stmia	sp, {r9, sl}
	bl	nm_PERCENT_ADJUST_set_start_date.constprop.7
	cmp	r4, #2
	beq	.L727
.L726:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #4096
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L728
.L727:
	cmp	fp, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #160]
	movne	r2, #0
	bne	.L729
	ldr	r2, [r7, #152]
	subs	r2, r2, #100
	movne	r2, #1
.L729:
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_PERCENT_ADJUST_set_end_date
	cmp	r4, #2
	beq	.L730
.L728:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #8192
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L731
.L730:
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #164]
	mov	r2, #0
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_SCHEDULE_set_enabled
	cmp	r4, #2
	beq	.L732
.L731:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #16384
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L733
.L732:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #168]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_SCHEDULE_set_schedule_type
	cmp	r4, #2
	beq	.L734
.L733:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #32768
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L735
.L734:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #172]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_SCHEDULE_set_start_time
	cmp	r4, #2
	beq	.L736
.L735:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #65536
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L737
.L736:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #176]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_SCHEDULE_set_stop_time
	cmp	r4, #2
	beq	.L738
.L737:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #131072
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L739
.L738:
	subs	r2, r4, #6
	movne	r2, #1
	cmp	fp, #0
	movne	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #180]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_SCHEDULE_set_begin_date
	cmp	r4, #2
	bne	.L739
.L741:
	mov	r6, r7
	mov	r5, #0
	b	.L740
.L739:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #262144
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	bne	.L741
	b	.L742
.L740:
	rsbs	r3, fp, #1
	stmia	sp, {r4, r8, r9, sl}
	ldr	r0, [sp, #44]
	mov	r1, r5
	ldr	r2, [r6, #184]
	movcc	r3, #0
	add	r5, r5, #1
	bl	nm_SCHEDULE_set_water_day
	cmp	r5, #7
	add	r6, r6, #4
	bne	.L740
	cmp	r4, #2
	beq	.L743
.L742:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #524288
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L744
.L743:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #212]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_SCHEDULE_set_irrigate_on_29th_or_31st
	cmp	r4, #2
	beq	.L745
.L744:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #1048576
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L746
.L745:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #216]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_SCHEDULE_set_mow_day
	cmp	r4, #2
	beq	.L747
.L746:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #2097152
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L748
.L747:
	add	r3, r7, #220
	stmia	sp, {r4, r8, r9, sl}
	ldr	r0, [sp, #44]
	ldmia	r3, {r1, r2}
	rsbs	r3, fp, #1
	movcc	r3, #0
	bl	nm_SCHEDULE_set_last_ran
	cmp	r4, #2
	beq	.L749
.L748:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #33554432
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L750
.L749:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #240]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_DAILY_ET_set_ET_in_use
	cmp	r4, #2
	beq	.L751
.L750:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #67108864
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L752
.L751:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #244]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_DAILY_ET_set_use_ET_averaging
	cmp	r4, #2
	beq	.L753
.L752:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #134217728
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L754
.L753:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #248]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_RAIN_set_rain_in_use
	cmp	r4, #2
	beq	.L755
.L754:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #536870912
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L756
.L755:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #256]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_WIND_set_wind_in_use
	cmp	r4, #2
	beq	.L757
.L756:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #1073741824
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L758
.L757:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #260]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_ALERT_ACTIONS_set_high_flow_action
	cmp	r4, #2
	beq	.L759
.L758:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #-2147483648
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L760
.L759:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #264]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_ALERT_ACTIONS_set_low_flow_action
	cmp	r4, #2
	beq	.L761
.L760:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #1
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L762
.L761:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #268]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_ON_AT_A_TIME_set_on_at_a_time_in_group
	cmp	r4, #2
	beq	.L763
.L762:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #2
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L764
.L763:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #272]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_ON_AT_A_TIME_set_on_at_a_time_in_system
	cmp	r4, #2
	beq	.L765
.L764:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #4
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L766
.L765:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #280]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_PUMP_set_pump_in_use
	cmp	r4, #2
	beq	.L767
.L766:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #8
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L768
.L767:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #284]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_LINE_FILL_TIME_set_line_fill_time
	cmp	r4, #2
	beq	.L769
.L768:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #16
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L770
.L769:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #288]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_DELAY_BETWEEN_VALVES_set_delay_time
	cmp	r4, #2
	beq	.L771
.L770:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #32
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L772
.L771:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #292]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_ACQUIRE_EXPECTEDS_set_acquire_expected
	cmp	r4, #2
	beq	.L773
.L772:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #64
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L774
.L773:
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #320]
	mov	r2, r4
	mov	r3, r8
	stmia	sp, {r9, sl}
	bl	nm_STATION_GROUP_set_allowable_depletion.constprop.8
	cmp	r4, #2
	beq	.L775
.L774:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #128
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L776
.L775:
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #324]
	mov	r2, r4
	mov	r3, r8
	stmia	sp, {r9, sl}
	bl	nm_STATION_GROUP_set_available_water.constprop.9
	cmp	r4, #2
	beq	.L777
.L776:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #256
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L778
.L777:
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #328]
	mov	r2, r4
	mov	r3, r8
	stmia	sp, {r9, sl}
	bl	nm_STATION_GROUP_set_root_zone_depth.constprop.10
	cmp	r4, #2
	beq	.L779
.L778:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #512
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L780
.L779:
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #332]
	mov	r2, r4
	mov	r3, r8
	stmia	sp, {r9, sl}
	bl	nm_STATION_GROUP_set_species_factor.constprop.11
	cmp	r4, #2
	beq	.L781
.L780:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #1024
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L782
.L781:
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #336]
	mov	r2, r4
	mov	r3, r8
	stmia	sp, {r9, sl}
	bl	nm_STATION_GROUP_set_density_factor.constprop.12
	cmp	r4, #2
	beq	.L783
.L782:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #2048
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L784
.L783:
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #340]
	mov	r2, r4
	mov	r3, r8
	stmia	sp, {r9, sl}
	bl	nm_STATION_GROUP_set_microclimate_factor.constprop.6
	cmp	r4, #2
	beq	.L785
.L784:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #4096
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L786
.L785:
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #344]
	mov	r2, #0
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_soil_intake_rate
	cmp	r4, #2
	beq	.L787
.L786:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #8192
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L788
.L787:
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #348]
	mov	r2, #0
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_allowable_surface_accumulation
	cmp	r4, #2
	beq	.L789
.L788:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #16384
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L790
.L789:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #360]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_budget_reduction_percentage_cap
	cmp	r4, #2
	beq	.L791
.L790:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #32768
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L792
.L791:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #364]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_GID_irrigation_system
	cmp	r4, #2
	beq	.L793
.L792:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #65536
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L794
.L793:
	ldr	r1, [sp, #44]
	ldr	r3, [r1, #368]
	cmp	r3, #0
	beq	.L795
	ldr	r3, [r7, #368]
	cmp	r3, #0
	bne	.L795
	ldr	r0, .L801
	ldr	r2, .L801+12
	ldr	r3, .L801+16
	bl	nm_ListRemove_debug
	ldr	r0, .L801
	ldr	r1, [sp, #44]
	bl	nm_ListInsertTail
.L795:
	rsbs	r2, fp, #1
	movcc	r2, #0
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #368]
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_in_use
	cmp	r4, #2
	beq	.L796
.L794:
	add	r1, sp, #92
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #131072
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L697
.L796:
	rsbs	r2, fp, #1
	ldr	r0, [sp, #44]
	ldr	r1, [r7, #372]
	movcc	r2, #0
	mov	r3, r4
	stmia	sp, {r8, r9, sl}
	bl	nm_STATION_GROUP_set_moisture_sensor_serial_number
	b	.L697
.L698:
	ldr	r0, .L801+12
	ldr	r1, .L801+20
	bl	Alert_group_not_found_with_filename
.L697:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L802:
	.align	2
.L801:
	.word	.LANCHOR1
	.word	49276
	.word	.LC50
	.word	.LC32
	.word	2739
	.word	2765
.LFE46:
	.size	nm_STATION_GROUP_store_changes, .-nm_STATION_GROUP_store_changes
	.section	.text.BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change, %function
BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change:
.LFB100:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI191:
	ldr	r3, .L806
	mov	r4, r2
	mov	r6, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L806+4
	ldr	r2, .L806+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r4, #0]
	cmp	r3, #1
	bne	.L804
	ldr	r1, .L806+8
	ldr	r2, .L806+12
	mov	r0, #376
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r6
	bl	STATION_GROUP_get_group_at_this_index
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r5, #0]
	cmp	r3, #0
	rsblt	r3, r3, #0
	strlt	r3, [r5, #0]
	ldr	r3, [r5, #0]
	str	r3, [r4, #360]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	mov	r1, #0
	str	r2, [sp, #0]
	mov	r2, #2
	str	r2, [sp, #4]
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	ldr	r1, .L806+8
	ldr	r2, .L806+16
	bl	mem_free_debug
.L804:
	ldr	r3, .L806
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L807:
	.align	2
.L806:
	.word	list_program_data_recursive_MUTEX
	.word	3891
	.word	.LC51
	.word	3895
	.word	3911
.LFE100:
	.size	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change, .-BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change
	.section	.text.BUDGET_REDUCTION_LIMITS_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	BUDGET_REDUCTION_LIMITS_extract_and_store_changes_from_GuiVars
	.type	BUDGET_REDUCTION_LIMITS_extract_and_store_changes_from_GuiVars, %function
BUDGET_REDUCTION_LIMITS_extract_and_store_changes_from_GuiVars:
.LFB101:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI192:
	ldr	r1, .L809
	mov	r0, #0
	ldr	r2, .L809+4
	bl	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change
	mov	r0, #1
	ldr	r1, .L809+8
	ldr	r2, .L809+12
	bl	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change
	mov	r0, #2
	ldr	r1, .L809+16
	ldr	r2, .L809+20
	bl	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change
	mov	r0, #3
	ldr	r1, .L809+24
	ldr	r2, .L809+28
	bl	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change
	mov	r0, #4
	ldr	r1, .L809+32
	ldr	r2, .L809+36
	bl	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change
	mov	r0, #5
	ldr	r1, .L809+40
	ldr	r2, .L809+44
	bl	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change
	mov	r0, #6
	ldr	r1, .L809+48
	ldr	r2, .L809+52
	bl	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change
	mov	r0, #7
	ldr	r1, .L809+56
	ldr	r2, .L809+60
	bl	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change
	mov	r0, #8
	ldr	r1, .L809+64
	ldr	r2, .L809+68
	bl	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change
	ldr	r1, .L809+72
	ldr	r2, .L809+76
	mov	r0, #9
	ldr	lr, [sp], #4
	b	BUDGET_REDUCTION_LIMITS_extract_and_store_individual_group_change
.L810:
	.align	2
.L809:
	.word	GuiVar_BudgetReductionLimit_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_BudgetReductionLimit_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_BudgetReductionLimit_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_BudgetReductionLimit_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_BudgetReductionLimit_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_BudgetReductionLimit_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_BudgetReductionLimit_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_BudgetReductionLimit_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_BudgetReductionLimit_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_BudgetReductionLimit_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE101:
	.size	BUDGET_REDUCTION_LIMITS_extract_and_store_changes_from_GuiVars, .-BUDGET_REDUCTION_LIMITS_extract_and_store_changes_from_GuiVars
	.section	.text.ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change, %function
ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change:
.LFB98:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI193:
	ldr	r3, .L813
	mov	r4, r2
	mov	r6, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L813+4
	ldr	r3, .L813+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, [r4, #0]
	cmp	r7, #1
	bne	.L812
	ldr	r1, .L813+4
	ldr	r2, .L813+12
	mov	r0, #376
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r6
	bl	STATION_GROUP_get_group_at_this_index
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r5, #0]
	str	r3, [r4, #292]
	bl	FLOWSENSE_get_controller_index
	mov	r1, #0
	mov	r2, #2
	str	r2, [sp, #4]
	str	r7, [sp, #0]
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	ldr	r1, .L813+4
	ldr	r2, .L813+16
	bl	mem_free_debug
.L812:
	ldr	r3, .L813
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L814:
	.align	2
.L813:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3839
	.word	3843
	.word	3857
.LFE98:
	.size	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change, .-ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change
	.section	.text.ACQUIRE_EXPECTEDS_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	ACQUIRE_EXPECTEDS_extract_and_store_changes_from_GuiVars
	.type	ACQUIRE_EXPECTEDS_extract_and_store_changes_from_GuiVars, %function
ACQUIRE_EXPECTEDS_extract_and_store_changes_from_GuiVars:
.LFB99:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI194:
	ldr	r4, .L816
	ldr	r3, .L816+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L816+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	ldr	r1, .L816+12
	ldr	r2, .L816+16
	bl	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change
	mov	r0, #1
	ldr	r1, .L816+20
	ldr	r2, .L816+24
	bl	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change
	mov	r0, #2
	ldr	r1, .L816+28
	ldr	r2, .L816+32
	bl	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change
	mov	r0, #3
	ldr	r1, .L816+36
	ldr	r2, .L816+40
	bl	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change
	mov	r0, #4
	ldr	r1, .L816+44
	ldr	r2, .L816+48
	bl	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change
	mov	r0, #5
	ldr	r1, .L816+52
	ldr	r2, .L816+56
	bl	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change
	mov	r0, #6
	ldr	r1, .L816+60
	ldr	r2, .L816+64
	bl	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change
	mov	r0, #7
	ldr	r1, .L816+68
	ldr	r2, .L816+72
	bl	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change
	mov	r0, #8
	ldr	r1, .L816+76
	ldr	r2, .L816+80
	bl	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change
	mov	r0, #9
	ldr	r1, .L816+84
	ldr	r2, .L816+88
	bl	ACQUIRE_EXPECTEDS_extract_and_store_individual_group_change
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L817:
	.align	2
.L816:
	.word	list_program_data_recursive_MUTEX
	.word	3870
	.word	.LC51
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE99:
	.size	ACQUIRE_EXPECTEDS_extract_and_store_changes_from_GuiVars, .-ACQUIRE_EXPECTEDS_extract_and_store_changes_from_GuiVars
	.section	.text.DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change, %function
DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change:
.LFB96:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI195:
	ldr	r3, .L821
	mov	r4, r2
	mov	r6, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L821+4
	ldr	r2, .L821+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r4, #0]
	cmp	r3, #1
	bne	.L819
	ldr	r1, .L821+8
	ldr	r2, .L821+12
	mov	r0, #376
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r6
	bl	STATION_GROUP_get_group_at_this_index
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, .L821+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	streq	r3, [r5, #0]
	ldr	r3, [r5, #0]
	str	r3, [r4, #288]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	mov	r1, #0
	str	r2, [sp, #0]
	mov	r2, #2
	str	r2, [sp, #4]
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	ldr	r1, .L821+8
	ldr	r2, .L821+20
	bl	mem_free_debug
.L819:
	ldr	r3, .L821
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L822:
	.align	2
.L821:
	.word	list_program_data_recursive_MUTEX
	.word	3779
	.word	.LC51
	.word	3783
	.word	GuiVar_DelayBetweenValvesInUse
	.word	3805
.LFE96:
	.size	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change, .-DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change
	.section	.text.DELAY_BETWEEN_VALVES_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	DELAY_BETWEEN_VALVES_extract_and_store_changes_from_GuiVars
	.type	DELAY_BETWEEN_VALVES_extract_and_store_changes_from_GuiVars, %function
DELAY_BETWEEN_VALVES_extract_and_store_changes_from_GuiVars:
.LFB97:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI196:
	ldr	r4, .L824
	ldr	r3, .L824+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L824+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	ldr	r1, .L824+12
	ldr	r2, .L824+16
	bl	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change
	mov	r0, #1
	ldr	r1, .L824+20
	ldr	r2, .L824+24
	bl	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change
	mov	r0, #2
	ldr	r1, .L824+28
	ldr	r2, .L824+32
	bl	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change
	mov	r0, #3
	ldr	r1, .L824+36
	ldr	r2, .L824+40
	bl	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change
	mov	r0, #4
	ldr	r1, .L824+44
	ldr	r2, .L824+48
	bl	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change
	mov	r0, #5
	ldr	r1, .L824+52
	ldr	r2, .L824+56
	bl	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change
	mov	r0, #6
	ldr	r1, .L824+60
	ldr	r2, .L824+64
	bl	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change
	mov	r0, #7
	ldr	r1, .L824+68
	ldr	r2, .L824+72
	bl	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change
	mov	r0, #8
	ldr	r1, .L824+76
	ldr	r2, .L824+80
	bl	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change
	mov	r0, #9
	ldr	r1, .L824+84
	ldr	r2, .L824+88
	bl	DELAY_BETWEEN_VALVES_extract_and_store_individual_group_change
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L825:
	.align	2
.L824:
	.word	list_program_data_recursive_MUTEX
	.word	3818
	.word	.LC51
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE97:
	.size	DELAY_BETWEEN_VALVES_extract_and_store_changes_from_GuiVars, .-DELAY_BETWEEN_VALVES_extract_and_store_changes_from_GuiVars
	.section	.text.LINE_FILL_TIME_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	LINE_FILL_TIME_extract_and_store_individual_group_change, %function
LINE_FILL_TIME_extract_and_store_individual_group_change:
.LFB94:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI197:
	ldr	r3, .L828
	mov	r4, r2
	mov	r6, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L828+4
	ldr	r3, .L828+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, [r4, #0]
	cmp	r7, #1
	bne	.L827
	ldr	r1, .L828+4
	ldr	r2, .L828+12
	mov	r0, #376
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r6
	bl	STATION_GROUP_get_group_at_this_index
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r5, #0]
	str	r3, [r4, #284]
	bl	FLOWSENSE_get_controller_index
	mov	r1, #0
	mov	r2, #2
	str	r2, [sp, #4]
	str	r7, [sp, #0]
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	ldr	r1, .L828+4
	ldr	r2, .L828+16
	bl	mem_free_debug
.L827:
	ldr	r3, .L828
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L829:
	.align	2
.L828:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3727
	.word	3731
	.word	3745
.LFE94:
	.size	LINE_FILL_TIME_extract_and_store_individual_group_change, .-LINE_FILL_TIME_extract_and_store_individual_group_change
	.section	.text.LINE_FILL_TIME_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	LINE_FILL_TIME_extract_and_store_changes_from_GuiVars
	.type	LINE_FILL_TIME_extract_and_store_changes_from_GuiVars, %function
LINE_FILL_TIME_extract_and_store_changes_from_GuiVars:
.LFB95:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI198:
	ldr	r4, .L831
	ldr	r3, .L831+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L831+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	ldr	r1, .L831+12
	ldr	r2, .L831+16
	bl	LINE_FILL_TIME_extract_and_store_individual_group_change
	mov	r0, #1
	ldr	r1, .L831+20
	ldr	r2, .L831+24
	bl	LINE_FILL_TIME_extract_and_store_individual_group_change
	mov	r0, #2
	ldr	r1, .L831+28
	ldr	r2, .L831+32
	bl	LINE_FILL_TIME_extract_and_store_individual_group_change
	mov	r0, #3
	ldr	r1, .L831+36
	ldr	r2, .L831+40
	bl	LINE_FILL_TIME_extract_and_store_individual_group_change
	mov	r0, #4
	ldr	r1, .L831+44
	ldr	r2, .L831+48
	bl	LINE_FILL_TIME_extract_and_store_individual_group_change
	mov	r0, #5
	ldr	r1, .L831+52
	ldr	r2, .L831+56
	bl	LINE_FILL_TIME_extract_and_store_individual_group_change
	mov	r0, #6
	ldr	r1, .L831+60
	ldr	r2, .L831+64
	bl	LINE_FILL_TIME_extract_and_store_individual_group_change
	mov	r0, #7
	ldr	r1, .L831+68
	ldr	r2, .L831+72
	bl	LINE_FILL_TIME_extract_and_store_individual_group_change
	mov	r0, #8
	ldr	r1, .L831+76
	ldr	r2, .L831+80
	bl	LINE_FILL_TIME_extract_and_store_individual_group_change
	mov	r0, #9
	ldr	r1, .L831+84
	ldr	r2, .L831+88
	bl	LINE_FILL_TIME_extract_and_store_individual_group_change
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L832:
	.align	2
.L831:
	.word	list_program_data_recursive_MUTEX
	.word	3758
	.word	.LC51
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE95:
	.size	LINE_FILL_TIME_extract_and_store_changes_from_GuiVars, .-LINE_FILL_TIME_extract_and_store_changes_from_GuiVars
	.section	.text.PUMP_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	PUMP_extract_and_store_individual_group_change, %function
PUMP_extract_and_store_individual_group_change:
.LFB92:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI199:
	ldr	r3, .L835
	mov	r4, r2
	mov	r6, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L835+4
	ldr	r3, .L835+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, [r4, #0]
	cmp	r7, #1
	bne	.L834
	ldr	r1, .L835+4
	ldr	r2, .L835+12
	mov	r0, #376
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r6
	bl	STATION_GROUP_get_group_at_this_index
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r5, #0]
	str	r3, [r4, #280]
	bl	FLOWSENSE_get_controller_index
	mov	r1, #0
	mov	r2, #2
	str	r2, [sp, #4]
	str	r7, [sp, #0]
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	ldr	r1, .L835+4
	ldr	r2, .L835+16
	bl	mem_free_debug
.L834:
	ldr	r3, .L835
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L836:
	.align	2
.L835:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3675
	.word	3679
	.word	3693
.LFE92:
	.size	PUMP_extract_and_store_individual_group_change, .-PUMP_extract_and_store_individual_group_change
	.section	.text.PUMP_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	PUMP_extract_and_store_changes_from_GuiVars
	.type	PUMP_extract_and_store_changes_from_GuiVars, %function
PUMP_extract_and_store_changes_from_GuiVars:
.LFB93:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI200:
	ldr	r4, .L838
	ldr	r3, .L838+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L838+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	ldr	r1, .L838+12
	ldr	r2, .L838+16
	bl	PUMP_extract_and_store_individual_group_change
	mov	r0, #1
	ldr	r1, .L838+20
	ldr	r2, .L838+24
	bl	PUMP_extract_and_store_individual_group_change
	mov	r0, #2
	ldr	r1, .L838+28
	ldr	r2, .L838+32
	bl	PUMP_extract_and_store_individual_group_change
	mov	r0, #3
	ldr	r1, .L838+36
	ldr	r2, .L838+40
	bl	PUMP_extract_and_store_individual_group_change
	mov	r0, #4
	ldr	r1, .L838+44
	ldr	r2, .L838+48
	bl	PUMP_extract_and_store_individual_group_change
	mov	r0, #5
	ldr	r1, .L838+52
	ldr	r2, .L838+56
	bl	PUMP_extract_and_store_individual_group_change
	mov	r0, #6
	ldr	r1, .L838+60
	ldr	r2, .L838+64
	bl	PUMP_extract_and_store_individual_group_change
	mov	r0, #7
	ldr	r1, .L838+68
	ldr	r2, .L838+72
	bl	PUMP_extract_and_store_individual_group_change
	mov	r0, #8
	ldr	r1, .L838+76
	ldr	r2, .L838+80
	bl	PUMP_extract_and_store_individual_group_change
	mov	r0, #9
	ldr	r1, .L838+84
	ldr	r2, .L838+88
	bl	PUMP_extract_and_store_individual_group_change
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L839:
	.align	2
.L838:
	.word	list_program_data_recursive_MUTEX
	.word	3706
	.word	.LC51
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE93:
	.size	PUMP_extract_and_store_changes_from_GuiVars, .-PUMP_extract_and_store_changes_from_GuiVars
	.section	.text.ON_AT_A_TIME_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	ON_AT_A_TIME_extract_and_store_individual_group_change, %function
ON_AT_A_TIME_extract_and_store_individual_group_change:
.LFB90:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, lr}
.LCFI201:
	mov	r4, r3
	ldr	r3, .L842
	mov	r7, r0
	mov	r6, r1
	ldr	r0, [r3, #0]
	mov	r5, r2
	mov	r1, #400
	ldr	r2, .L842+4
	ldr	r3, .L842+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r8, [r4, #0]
	cmp	r8, #1
	bne	.L841
	ldr	r1, .L842+4
	ldr	r2, .L842+12
	mov	r0, #376
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r7
	bl	STATION_GROUP_get_group_at_this_index
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r6, #0]
	str	r3, [r4, #268]
	ldr	r3, [r5, #0]
	str	r3, [r4, #272]
	bl	FLOWSENSE_get_controller_index
	mov	r1, #0
	mov	r2, #2
	str	r2, [sp, #4]
	str	r8, [sp, #0]
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	ldr	r1, .L842+4
	ldr	r2, .L842+16
	bl	mem_free_debug
.L841:
	ldr	r3, .L842
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L843:
	.align	2
.L842:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3621
	.word	3625
	.word	3641
.LFE90:
	.size	ON_AT_A_TIME_extract_and_store_individual_group_change, .-ON_AT_A_TIME_extract_and_store_individual_group_change
	.section	.text.ON_AT_A_TIME_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	ON_AT_A_TIME_extract_and_store_changes_from_GuiVars
	.type	ON_AT_A_TIME_extract_and_store_changes_from_GuiVars, %function
ON_AT_A_TIME_extract_and_store_changes_from_GuiVars:
.LFB91:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI202:
	ldr	r4, .L845
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L845+4
	ldr	r3, .L845+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	ldr	r1, .L845+12
	ldr	r2, .L845+16
	ldr	r3, .L845+20
	bl	ON_AT_A_TIME_extract_and_store_individual_group_change
	mov	r0, #1
	ldr	r1, .L845+24
	ldr	r2, .L845+28
	ldr	r3, .L845+32
	bl	ON_AT_A_TIME_extract_and_store_individual_group_change
	mov	r0, #2
	ldr	r1, .L845+36
	ldr	r2, .L845+40
	ldr	r3, .L845+44
	bl	ON_AT_A_TIME_extract_and_store_individual_group_change
	mov	r0, #3
	ldr	r1, .L845+48
	ldr	r2, .L845+52
	ldr	r3, .L845+56
	bl	ON_AT_A_TIME_extract_and_store_individual_group_change
	mov	r0, #4
	ldr	r1, .L845+60
	ldr	r2, .L845+64
	ldr	r3, .L845+68
	bl	ON_AT_A_TIME_extract_and_store_individual_group_change
	mov	r0, #5
	ldr	r1, .L845+72
	ldr	r2, .L845+76
	ldr	r3, .L845+80
	bl	ON_AT_A_TIME_extract_and_store_individual_group_change
	mov	r0, #6
	ldr	r1, .L845+84
	ldr	r2, .L845+88
	ldr	r3, .L845+92
	bl	ON_AT_A_TIME_extract_and_store_individual_group_change
	mov	r0, #7
	ldr	r1, .L845+96
	ldr	r2, .L845+100
	ldr	r3, .L845+104
	bl	ON_AT_A_TIME_extract_and_store_individual_group_change
	mov	r0, #8
	ldr	r1, .L845+108
	ldr	r2, .L845+112
	ldr	r3, .L845+116
	bl	ON_AT_A_TIME_extract_and_store_individual_group_change
	mov	r0, #9
	ldr	r1, .L845+120
	ldr	r2, .L845+124
	ldr	r3, .L845+128
	bl	ON_AT_A_TIME_extract_and_store_individual_group_change
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L846:
	.align	2
.L845:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3654
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSettingB_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE91:
	.size	ON_AT_A_TIME_extract_and_store_changes_from_GuiVars, .-ON_AT_A_TIME_extract_and_store_changes_from_GuiVars
	.section	.text.ALERT_ACTIONS_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	ALERT_ACTIONS_extract_and_store_individual_group_change, %function
ALERT_ACTIONS_extract_and_store_individual_group_change:
.LFB88:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, lr}
.LCFI203:
	mov	r4, r3
	ldr	r3, .L849
	mov	r7, r0
	mov	r6, r1
	ldr	r0, [r3, #0]
	mov	r5, r2
	mov	r1, #400
	ldr	r2, .L849+4
	ldr	r3, .L849+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r8, [r4, #0]
	cmp	r8, #1
	bne	.L848
	ldr	r1, .L849+4
	ldr	r2, .L849+12
	mov	r0, #376
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r7
	bl	STATION_GROUP_get_group_at_this_index
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r6, #0]
	str	r3, [r4, #260]
	ldr	r3, [r5, #0]
	str	r3, [r4, #264]
	bl	FLOWSENSE_get_controller_index
	mov	r1, #0
	mov	r2, #2
	str	r2, [sp, #4]
	str	r8, [sp, #0]
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	ldr	r1, .L849+4
	ldr	r2, .L849+16
	bl	mem_free_debug
.L848:
	ldr	r3, .L849
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L850:
	.align	2
.L849:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3567
	.word	3571
	.word	3587
.LFE88:
	.size	ALERT_ACTIONS_extract_and_store_individual_group_change, .-ALERT_ACTIONS_extract_and_store_individual_group_change
	.section	.text.ALERT_ACTIONS_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	ALERT_ACTIONS_extract_and_store_changes_from_GuiVars
	.type	ALERT_ACTIONS_extract_and_store_changes_from_GuiVars, %function
ALERT_ACTIONS_extract_and_store_changes_from_GuiVars:
.LFB89:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI204:
	ldr	r4, .L852
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L852+4
	mov	r3, #3600
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	ldr	r1, .L852+8
	ldr	r2, .L852+12
	ldr	r3, .L852+16
	bl	ALERT_ACTIONS_extract_and_store_individual_group_change
	mov	r0, #1
	ldr	r1, .L852+20
	ldr	r2, .L852+24
	ldr	r3, .L852+28
	bl	ALERT_ACTIONS_extract_and_store_individual_group_change
	mov	r0, #2
	ldr	r1, .L852+32
	ldr	r2, .L852+36
	ldr	r3, .L852+40
	bl	ALERT_ACTIONS_extract_and_store_individual_group_change
	mov	r0, #3
	ldr	r1, .L852+44
	ldr	r2, .L852+48
	ldr	r3, .L852+52
	bl	ALERT_ACTIONS_extract_and_store_individual_group_change
	mov	r0, #4
	ldr	r1, .L852+56
	ldr	r2, .L852+60
	ldr	r3, .L852+64
	bl	ALERT_ACTIONS_extract_and_store_individual_group_change
	mov	r0, #5
	ldr	r1, .L852+68
	ldr	r2, .L852+72
	ldr	r3, .L852+76
	bl	ALERT_ACTIONS_extract_and_store_individual_group_change
	mov	r0, #6
	ldr	r1, .L852+80
	ldr	r2, .L852+84
	ldr	r3, .L852+88
	bl	ALERT_ACTIONS_extract_and_store_individual_group_change
	mov	r0, #7
	ldr	r1, .L852+92
	ldr	r2, .L852+96
	ldr	r3, .L852+100
	bl	ALERT_ACTIONS_extract_and_store_individual_group_change
	mov	r0, #8
	ldr	r1, .L852+104
	ldr	r2, .L852+108
	ldr	r3, .L852+112
	bl	ALERT_ACTIONS_extract_and_store_individual_group_change
	mov	r0, #9
	ldr	r1, .L852+116
	ldr	r2, .L852+120
	ldr	r3, .L852+124
	bl	ALERT_ACTIONS_extract_and_store_individual_group_change
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L853:
	.align	2
.L852:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSettingB_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE89:
	.size	ALERT_ACTIONS_extract_and_store_changes_from_GuiVars, .-ALERT_ACTIONS_extract_and_store_changes_from_GuiVars
	.section	.text.WEATHER_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	WEATHER_extract_and_store_individual_group_change, %function
WEATHER_extract_and_store_individual_group_change:
.LFB86:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, sl, lr}
.LCFI205:
	ldr	r4, [sp, #44]
	mov	r5, r3
	ldr	r3, .L856
	mov	r8, r0
	mov	r7, r1
	ldr	r0, [r3, #0]
	mov	r6, r2
	mov	r1, #400
	ldr	r2, .L856+4
	ldr	r3, .L856+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	sl, [r4, #0]
	cmp	sl, #1
	bne	.L855
	ldr	r1, .L856+4
	ldr	r2, .L856+12
	mov	r0, #376
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r8
	bl	STATION_GROUP_get_group_at_this_index
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r7, #0]
	str	r3, [r4, #240]
	ldr	r3, [r6, #0]
	str	r3, [r4, #248]
	ldr	r3, [r5, #0]
	str	r3, [r4, #256]
	bl	FLOWSENSE_get_controller_index
	mov	r1, #0
	mov	r2, #2
	str	r2, [sp, #4]
	str	sl, [sp, #0]
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	ldr	r1, .L856+4
	ldr	r2, .L856+16
	bl	mem_free_debug
.L855:
	ldr	r3, .L856
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L857:
	.align	2
.L856:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3513
	.word	3517
	.word	3533
.LFE86:
	.size	WEATHER_extract_and_store_individual_group_change, .-WEATHER_extract_and_store_individual_group_change
	.section	.text.WEATHER_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_extract_and_store_changes_from_GuiVars
	.type	WEATHER_extract_and_store_changes_from_GuiVars, %function
WEATHER_extract_and_store_changes_from_GuiVars:
.LFB87:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI206:
	ldr	r4, .L859
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L859+4
	ldr	r3, .L859+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L859+12
	mov	r0, #0
	str	r3, [sp, #0]
	ldr	r1, .L859+16
	ldr	r2, .L859+20
	ldr	r3, .L859+24
	bl	WEATHER_extract_and_store_individual_group_change
	ldr	r3, .L859+28
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r1, .L859+32
	ldr	r2, .L859+36
	ldr	r3, .L859+40
	bl	WEATHER_extract_and_store_individual_group_change
	ldr	r3, .L859+44
	mov	r0, #2
	str	r3, [sp, #0]
	ldr	r1, .L859+48
	ldr	r2, .L859+52
	ldr	r3, .L859+56
	bl	WEATHER_extract_and_store_individual_group_change
	ldr	r3, .L859+60
	mov	r0, #3
	str	r3, [sp, #0]
	ldr	r1, .L859+64
	ldr	r2, .L859+68
	ldr	r3, .L859+72
	bl	WEATHER_extract_and_store_individual_group_change
	ldr	r3, .L859+76
	mov	r0, #4
	str	r3, [sp, #0]
	ldr	r1, .L859+80
	ldr	r2, .L859+84
	ldr	r3, .L859+88
	bl	WEATHER_extract_and_store_individual_group_change
	ldr	r3, .L859+92
	mov	r0, #5
	str	r3, [sp, #0]
	ldr	r1, .L859+96
	ldr	r2, .L859+100
	ldr	r3, .L859+104
	bl	WEATHER_extract_and_store_individual_group_change
	ldr	r3, .L859+108
	mov	r0, #6
	str	r3, [sp, #0]
	ldr	r1, .L859+112
	ldr	r2, .L859+116
	ldr	r3, .L859+120
	bl	WEATHER_extract_and_store_individual_group_change
	ldr	r3, .L859+124
	mov	r0, #7
	str	r3, [sp, #0]
	ldr	r1, .L859+128
	ldr	r2, .L859+132
	ldr	r3, .L859+136
	bl	WEATHER_extract_and_store_individual_group_change
	ldr	r3, .L859+140
	mov	r0, #8
	str	r3, [sp, #0]
	ldr	r1, .L859+144
	ldr	r2, .L859+148
	ldr	r3, .L859+152
	bl	WEATHER_extract_and_store_individual_group_change
	ldr	r3, .L859+156
	mov	r0, #9
	str	r3, [sp, #0]
	ldr	r1, .L859+160
	ldr	r2, .L859+164
	ldr	r3, .L859+168
	bl	WEATHER_extract_and_store_individual_group_change
	ldr	r0, [r4, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L860:
	.align	2
.L859:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3546
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingC_3
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSettingC_4
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSettingC_5
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSettingC_6
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSettingC_7
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSettingC_8
	.word	GuiVar_GroupSetting_Visible_9
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSettingB_9
	.word	GuiVar_GroupSettingC_9
.LFE87:
	.size	WEATHER_extract_and_store_changes_from_GuiVars, .-WEATHER_extract_and_store_changes_from_GuiVars
	.section	.text.SCHEDULE_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	SCHEDULE_extract_and_store_changes_from_GuiVars
	.type	SCHEDULE_extract_and_store_changes_from_GuiVars, %function
SCHEDULE_extract_and_store_changes_from_GuiVars:
.LFB85:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI207:
	ldr	r5, .L862
	ldr	r6, .L862+4
	mov	r2, r5
	ldr	r3, .L862+8
	ldr	r0, [r6, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #3472
	mov	r0, #376
	bl	mem_malloc_debug
	ldr	r3, .L862+12
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	STATION_GROUP_get_group_with_this_GID
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, .L862+16
	ldr	r3, [r3, #0]
	str	r3, [r4, #164]
	ldr	r3, .L862+20
	ldr	r3, [r3, #0]
	str	r3, [r4, #168]
	ldr	r3, .L862+24
	ldr	r2, [r3, #0]
	mov	r3, #60
	mul	r2, r3, r2
	str	r2, [r4, #172]
	ldr	r2, .L862+28
	ldr	r2, [r2, #0]
	mul	r3, r2, r3
	str	r3, [r4, #176]
	ldr	r3, .L862+32
	ldr	r3, [r3, #0]
	str	r3, [r4, #180]
	ldr	r3, .L862+36
	ldr	r3, [r3, #0]
	str	r3, [r4, #184]
	ldr	r3, .L862+40
	ldr	r3, [r3, #0]
	str	r3, [r4, #188]
	ldr	r3, .L862+44
	ldr	r3, [r3, #0]
	str	r3, [r4, #192]
	ldr	r3, .L862+48
	ldr	r3, [r3, #0]
	str	r3, [r4, #196]
	ldr	r3, .L862+52
	ldr	r3, [r3, #0]
	str	r3, [r4, #200]
	ldr	r3, .L862+56
	ldr	r3, [r3, #0]
	str	r3, [r4, #204]
	ldr	r3, .L862+60
	ldr	r3, [r3, #0]
	str	r3, [r4, #208]
	ldr	r3, .L862+64
	ldr	r3, [r3, #0]
	str	r3, [r4, #212]
	ldr	r3, .L862+68
	ldr	r3, [r3, #0]
	str	r3, [r4, #216]
	ldr	r3, .L862+72
	ldr	r3, [r3, #0]
	str	r3, [r4, #244]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	mov	r1, #0
	str	r2, [sp, #0]
	mov	r2, #2
	str	r2, [sp, #4]
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L862+76
	bl	mem_free_debug
	ldr	r0, [r6, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L863:
	.align	2
.L862:
	.word	.LC51
	.word	list_program_data_recursive_MUTEX
	.word	3468
	.word	g_GROUP_ID
	.word	GuiVar_ScheduleStartTimeEnabled
	.word	GuiVar_ScheduleType
	.word	GuiVar_ScheduleStartTime
	.word	GuiVar_ScheduleStopTime
	.word	GuiVar_ScheduleBeginDate
	.word	GuiVar_ScheduleWaterDay_Sun
	.word	GuiVar_ScheduleWaterDay_Mon
	.word	GuiVar_ScheduleWaterDay_Tue
	.word	GuiVar_ScheduleWaterDay_Wed
	.word	GuiVar_ScheduleWaterDay_Thu
	.word	GuiVar_ScheduleWaterDay_Fri
	.word	GuiVar_ScheduleWaterDay_Sat
	.word	GuiVar_ScheduleIrrigateOn29thOr31st
	.word	GuiVar_ScheduleMowDay
	.word	GuiVar_ScheduleUseETAveraging
	.word	3501
.LFE85:
	.size	SCHEDULE_extract_and_store_changes_from_GuiVars, .-SCHEDULE_extract_and_store_changes_from_GuiVars
	.section	.text.PERCENT_ADJUST_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	PERCENT_ADJUST_extract_and_store_individual_group_change, %function
PERCENT_ADJUST_extract_and_store_individual_group_change:
.LFB83:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI208:
	mov	r4, r3
	ldr	r3, .L867
	sub	sp, sp, #24
.LCFI209:
	mov	r7, r0
	mov	r6, r1
	ldr	r0, [r3, #0]
	mov	r5, r2
	ldr	r3, .L867+4
	mov	r1, #400
	ldr	r2, .L867+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r4, #0]
	cmp	r3, #1
	bne	.L865
	ldr	r1, .L867+8
	ldr	r2, .L867+12
	mov	r0, #376
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r7
	bl	STATION_GROUP_get_group_at_this_index
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r6, #0]
	add	r3, r3, #100
	cmp	r3, #100
	str	r3, [r4, #152]
	beq	.L866
	add	r0, sp, #16
	bl	EPSON_obtain_latest_time_and_date
	ldrh	r3, [sp, #20]
	str	r3, [r4, #156]
	ldr	r2, [r5, #0]
	add	r3, r2, r3
	str	r3, [r4, #160]
.L866:
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	mov	r1, #0
	str	r2, [sp, #0]
	mov	r2, #2
	str	r2, [sp, #4]
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	ldr	r1, .L867+8
	ldr	r2, .L867+16
	bl	mem_free_debug
.L865:
	ldr	r3, .L867
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L868:
	.align	2
.L867:
	.word	list_program_data_recursive_MUTEX
	.word	3411
	.word	.LC51
	.word	3415
	.word	3438
.LFE83:
	.size	PERCENT_ADJUST_extract_and_store_individual_group_change, .-PERCENT_ADJUST_extract_and_store_individual_group_change
	.section	.text.PERCENT_ADJUST_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	PERCENT_ADJUST_extract_and_store_changes_from_GuiVars
	.type	PERCENT_ADJUST_extract_and_store_changes_from_GuiVars, %function
PERCENT_ADJUST_extract_and_store_changes_from_GuiVars:
.LFB84:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI210:
	ldr	r1, .L870
	mov	r0, #0
	ldr	r2, .L870+4
	ldr	r3, .L870+8
	bl	PERCENT_ADJUST_extract_and_store_individual_group_change
	mov	r0, #1
	ldr	r1, .L870+12
	ldr	r2, .L870+16
	ldr	r3, .L870+20
	bl	PERCENT_ADJUST_extract_and_store_individual_group_change
	mov	r0, #2
	ldr	r1, .L870+24
	ldr	r2, .L870+28
	ldr	r3, .L870+32
	bl	PERCENT_ADJUST_extract_and_store_individual_group_change
	mov	r0, #3
	ldr	r1, .L870+36
	ldr	r2, .L870+40
	ldr	r3, .L870+44
	bl	PERCENT_ADJUST_extract_and_store_individual_group_change
	mov	r0, #4
	ldr	r1, .L870+48
	ldr	r2, .L870+52
	ldr	r3, .L870+56
	bl	PERCENT_ADJUST_extract_and_store_individual_group_change
	mov	r0, #5
	ldr	r1, .L870+60
	ldr	r2, .L870+64
	ldr	r3, .L870+68
	bl	PERCENT_ADJUST_extract_and_store_individual_group_change
	mov	r0, #6
	ldr	r1, .L870+72
	ldr	r2, .L870+76
	ldr	r3, .L870+80
	bl	PERCENT_ADJUST_extract_and_store_individual_group_change
	mov	r0, #7
	ldr	r1, .L870+84
	ldr	r2, .L870+88
	ldr	r3, .L870+92
	bl	PERCENT_ADJUST_extract_and_store_individual_group_change
	mov	r0, #8
	ldr	r1, .L870+96
	ldr	r2, .L870+100
	ldr	r3, .L870+104
	bl	PERCENT_ADJUST_extract_and_store_individual_group_change
	ldr	r1, .L870+108
	ldr	r2, .L870+112
	ldr	r3, .L870+116
	mov	r0, #9
	ldr	lr, [sp], #4
	b	PERCENT_ADJUST_extract_and_store_individual_group_change
.L871:
	.align	2
.L870:
	.word	GuiVar_PercentAdjustPercent_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_PercentAdjustPercent_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_PercentAdjustPercent_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_PercentAdjustPercent_3
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_PercentAdjustPercent_4
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_PercentAdjustPercent_5
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_PercentAdjustPercent_6
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_PercentAdjustPercent_7
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_PercentAdjustPercent_8
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_PercentAdjustPercent_9
	.word	GuiVar_GroupSettingB_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE84:
	.size	PERCENT_ADJUST_extract_and_store_changes_from_GuiVars, .-PERCENT_ADJUST_extract_and_store_changes_from_GuiVars
	.section	.text.PRIORITY_extract_and_store_individual_group_change,"ax",%progbits
	.align	2
	.type	PRIORITY_extract_and_store_individual_group_change, %function
PRIORITY_extract_and_store_individual_group_change:
.LFB81:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI211:
	ldr	r3, .L874
	mov	r4, r2
	mov	r6, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L874+4
	ldr	r3, .L874+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, [r4, #0]
	cmp	r7, #1
	bne	.L873
	ldr	r1, .L874+4
	ldr	r2, .L874+12
	mov	r0, #376
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r0, r6
	bl	STATION_GROUP_get_group_at_this_index
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r5, #0]
	str	r3, [r4, #148]
	bl	FLOWSENSE_get_controller_index
	mov	r1, #0
	mov	r2, #2
	str	r2, [sp, #4]
	str	r7, [sp, #0]
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	ldr	r1, .L874+4
	ldr	r2, .L874+16
	bl	mem_free_debug
.L873:
	ldr	r3, .L874
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L875:
	.align	2
.L874:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3357
	.word	3361
	.word	3375
.LFE81:
	.size	PRIORITY_extract_and_store_individual_group_change, .-PRIORITY_extract_and_store_individual_group_change
	.section	.text.PRIORITY_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	PRIORITY_extract_and_store_changes_from_GuiVars
	.type	PRIORITY_extract_and_store_changes_from_GuiVars, %function
PRIORITY_extract_and_store_changes_from_GuiVars:
.LFB82:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI212:
	ldr	r4, .L877
	ldr	r3, .L877+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L877+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	ldr	r1, .L877+12
	ldr	r2, .L877+16
	bl	PRIORITY_extract_and_store_individual_group_change
	mov	r0, #1
	ldr	r1, .L877+20
	ldr	r2, .L877+24
	bl	PRIORITY_extract_and_store_individual_group_change
	mov	r0, #2
	ldr	r1, .L877+28
	ldr	r2, .L877+32
	bl	PRIORITY_extract_and_store_individual_group_change
	mov	r0, #3
	ldr	r1, .L877+36
	ldr	r2, .L877+40
	bl	PRIORITY_extract_and_store_individual_group_change
	mov	r0, #4
	ldr	r1, .L877+44
	ldr	r2, .L877+48
	bl	PRIORITY_extract_and_store_individual_group_change
	mov	r0, #5
	ldr	r1, .L877+52
	ldr	r2, .L877+56
	bl	PRIORITY_extract_and_store_individual_group_change
	mov	r0, #6
	ldr	r1, .L877+60
	ldr	r2, .L877+64
	bl	PRIORITY_extract_and_store_individual_group_change
	mov	r0, #7
	ldr	r1, .L877+68
	ldr	r2, .L877+72
	bl	PRIORITY_extract_and_store_individual_group_change
	mov	r0, #8
	ldr	r1, .L877+76
	ldr	r2, .L877+80
	bl	PRIORITY_extract_and_store_individual_group_change
	mov	r0, #9
	ldr	r1, .L877+84
	ldr	r2, .L877+88
	bl	PRIORITY_extract_and_store_individual_group_change
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L878:
	.align	2
.L877:
	.word	list_program_data_recursive_MUTEX
	.word	3388
	.word	.LC51
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSetting_Visible_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSetting_Visible_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSetting_Visible_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSetting_Visible_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSetting_Visible_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSetting_Visible_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSetting_Visible_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSetting_Visible_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSetting_Visible_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSetting_Visible_9
.LFE82:
	.size	PRIORITY_extract_and_store_changes_from_GuiVars, .-PRIORITY_extract_and_store_changes_from_GuiVars
	.section	.text.STATION_GROUP_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	STATION_GROUP_extract_and_store_changes_from_GuiVars
	.type	STATION_GROUP_extract_and_store_changes_from_GuiVars, %function
STATION_GROUP_extract_and_store_changes_from_GuiVars:
.LFB80:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI213:
	fstmfdd	sp!, {d8}
.LCFI214:
	flds	s16, .L883
	ldr	r3, .L883+164
	sub	sp, sp, #16
.LCFI215:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L883+12
	ldr	r2, .L883+152
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L883+152
	ldr	r2, .L883+16
	mov	r0, #376
	bl	mem_malloc_debug
	ldr	r5, .L883+20
	mov	r8, #1
	ldr	r7, .L883+160
	mov	r4, r0
	ldr	r0, [r5, #0]
	bl	STATION_GROUP_get_group_with_this_GID
	mov	r2, #376
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	mov	r2, #48
	add	r0, r4, #20
	ldr	r1, .L883+24
	bl	strlcpy
	ldr	r3, .L883+28
	flds	s15, .L883+4
	mov	r1, #0
	ldr	r3, [r3, #0]
	str	r3, [r4, #72]
	ldr	r3, .L883+32
	ldr	r3, [r3, #0]
	str	r3, [r4, #76]
	ldr	r3, .L883+36
	flds	s14, [r3, #0]
	fmuls	s15, s14, s15
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+40
	mov	r1, #0
	ldr	r3, [r3, #0]
	str	r3, [r4, #92]
	ldr	r3, .L883+44
	ldr	r3, [r3, #0]
	str	r3, [r4, #96]
	ldr	r3, .L883+48
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #80]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+52
	mov	r1, #0
	ldr	r3, [r3, #0]
	str	r3, [r4, #84]
	ldr	r3, .L883+56
	ldr	r3, [r3, #0]
	str	r3, [r4, #88]
	ldr	r3, .L883+60
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #252]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+64
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #100]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+68
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #104]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+72
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #108]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+76
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #112]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+80
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #116]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+84
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #120]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+88
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #124]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+92
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #128]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+96
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #132]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+100
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #136]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+104
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #140]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+108
	mov	r1, #0
	ldr	r3, [r3, #0]
	str	r3, [r4, #320]
	ldr	r3, .L883+112
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #144]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+116
	mov	r1, #0
	flds	s14, [r3, #0]
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #324]	@ int
	flds	s15, .L883+8
	fmuls	s15, s14, s15
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+120
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #328]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+124
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #332]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+128
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #336]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+132
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #340]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+136
	mov	r1, #0
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #344]	@ int
	flds	s15, [r3, #0]
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_float
	ldr	r3, .L883+140
	ldr	r3, [r3, #0]
	str	r3, [r4, #364]
	ldr	r3, .L883+144
	ldr	r3, [r3, #0]
	str	r3, [r4, #368]
	ldr	r3, .L883+148
	ldr	r3, [r3, #0]
	str	r3, [r4, #372]
	ldr	r3, .L883+168
	ldr	r6, [r3, #0]
	fmsr	s15, r0
	ftouizs	s15, s15
	fsts	s15, [r4, #348]	@ int
	bl	FLOWSENSE_get_controller_index
	mov	r1, #0
	mov	r2, #2
	str	r2, [sp, #4]
	str	r8, [sp, #0]
	b	.L884
.L885:
	.align	2
.L883:
	.word	1120403456
	.word	1203982336
	.word	1092616192
	.word	3251
	.word	3255
	.word	g_GROUP_ID
	.word	GuiVar_GroupName
	.word	GuiVar_StationGroupPlantType
	.word	GuiVar_StationGroupHeadType
	.word	GuiVar_StationGroupPrecipRate
	.word	GuiVar_StationGroupExposure
	.word	GuiVar_StationGroupUsableRain
	.word	GuiVar_StationGroupSoilStorageCapacity
	.word	GuiVar_StationGroupSoilType
	.word	GuiVar_StationGroupSlopePercentage
	.word	GuiVar_StationGroupKc1
	.word	GuiVar_StationGroupKc2
	.word	GuiVar_StationGroupKc3
	.word	GuiVar_StationGroupKc4
	.word	GuiVar_StationGroupKc5
	.word	GuiVar_StationGroupKc6
	.word	GuiVar_StationGroupKc7
	.word	GuiVar_StationGroupKc8
	.word	GuiVar_StationGroupKc9
	.word	GuiVar_StationGroupKc10
	.word	GuiVar_StationGroupKc11
	.word	GuiVar_StationGroupKc12
	.word	GuiVar_StationGroupAllowableDepletion
	.word	GuiVar_StationGroupAvailableWater
	.word	GuiVar_StationGroupRootZoneDepth
	.word	GuiVar_StationGroupSpeciesFactor
	.word	GuiVar_StationGroupDensityFactor
	.word	GuiVar_StationGroupMicroclimateFactor
	.word	GuiVar_StationGroupSoilIntakeRate
	.word	GuiVar_StationGroupAllowableSurfaceAccum
	.word	GuiVar_StationGroupSystemGID
	.word	GuiVar_StationGroupInUse
	.word	GuiVar_StationGroupMoistureSensorDecoderSN
	.word	.LC51
	.word	3313
	.word	station_info_list_hdr
	.word	list_program_data_recursive_MUTEX
	.word	g_GROUP_creating_new
.L884:
	mov	r3, r0
	mov	r0, #0
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	mov	r1, r6
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r4
	ldr	r1, .L883+152
	ldr	r2, .L883+156
	bl	mem_free_debug
	ldr	r0, .L883+160
	bl	nm_ListGetFirst
	mov	r6, #0
	mov	r4, r0
	b	.L880
.L882:
	mov	r0, r4
	bl	STATION_get_GID_station_group
	ldr	r3, [r5, #0]
	cmp	r0, r3
	mov	sl, r0
	bne	.L881
	bl	FLOWSENSE_get_controller_index
	mov	r2, #2
	str	r2, [sp, #4]
	mov	r1, sl
	mov	r2, sl
	str	r8, [sp, #0]
	mov	r3, r0
	mov	r0, r4
	bl	STATION_adjust_group_settings_after_changing_group_assignment
.L881:
	mov	r1, r4
	ldr	r0, .L883+160
	bl	nm_ListGetNext
	add	r6, r6, #1
	mov	r4, r0
.L880:
	ldr	r3, [r7, #8]
	cmp	r6, r3
	bcc	.L882
	ldr	r3, .L883+164
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L883+168
	mov	r2, #0
	str	r2, [r3, #0]
	add	sp, sp, #16
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.LFE80:
	.size	STATION_GROUP_extract_and_store_changes_from_GuiVars, .-STATION_GROUP_extract_and_store_changes_from_GuiVars
	.section	.text.nm_STATION_GROUP_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	nm_STATION_GROUP_extract_and_store_changes_from_comm
	.type	nm_STATION_GROUP_extract_and_store_changes_from_comm, %function
nm_STATION_GROUP_extract_and_store_changes_from_comm:
.LFB47:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI216:
	mov	r4, r0
	sub	sp, sp, #44
.LCFI217:
	str	r1, [sp, #16]
	str	r2, [sp, #20]
	mov	r1, r4
	add	r0, sp, #32
	mov	r2, #4
	mov	r9, #0
	mov	sl, r3
	add	r4, r4, #4
	bl	memcpy
	mov	r7, #4
	mov	fp, r9
	b	.L887
.L937:
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #40
	bl	memcpy
	add	r1, r4, #4
	mov	r2, #4
	add	r0, sp, #36
	bl	memcpy
	add	r1, r4, #8
	mov	r2, #8
	add	r0, sp, #24
	bl	memcpy
	ldr	r0, .L942
	ldr	r1, [sp, #40]
	mov	r2, #1
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	add	r5, r4, #16
	subs	r8, r0, #0
	bne	.L888
	mov	r0, sl
	bl	nm_STATION_GROUP_create_new_group
	ldr	r3, [sp, #40]
	cmp	sl, #16
	mov	r8, r0
	str	r3, [r0, #16]
	bne	.L941
	ldr	r3, .L942+4
	add	r0, r0, #296
	ldr	r1, [r3, #0]
	bl	SHARED_clear_all_64_bit_change_bits
.L941:
	mov	fp, #1
.L888:
	ldr	r1, .L942+8
	ldr	r2, .L942+12
	mov	r0, #376
	bl	mem_malloc_debug
	mov	r1, #0
	mov	r2, #376
	mov	r6, r0
	bl	memset
	mov	r0, r6
	mov	r1, r8
	mov	r2, #376
	bl	memcpy
	ldr	r3, [sp, #24]
	tst	r3, #1
	addeq	r7, r7, #16
	beq	.L890
	mov	r1, r5
	add	r0, r6, #20
	mov	r2, #48
	bl	memcpy
	add	r5, r4, #64
	add	r7, r7, #64
.L890:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #2
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L891
	mov	r1, r5
	add	r0, r6, #72
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L891:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #4
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L892
	mov	r1, r5
	add	r0, r6, #76
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L892:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #8
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L893
	mov	r1, r5
	add	r0, r6, #80
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L893:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #16
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L894
	mov	r1, r5
	add	r0, r6, #84
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L894:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #32
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L895
	mov	r1, r5
	add	r0, r6, #88
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L895:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #64
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L896
	mov	r1, r5
	add	r0, r6, #92
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L896:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #128
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L897
	mov	r1, r5
	add	r0, r6, #96
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L897:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #256
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L898
	mov	r1, r5
	add	r0, r6, #100
	mov	r2, #48
	bl	memcpy
	add	r5, r5, #48
	add	r7, r7, #48
.L898:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #512
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L899
	mov	r1, r5
	add	r0, r6, #148
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L899:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #1024
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L900
	mov	r1, r5
	add	r0, r6, #152
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L900:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #2048
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L901
	mov	r1, r5
	add	r0, r6, #156
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L901:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #4096
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L902
	mov	r1, r5
	add	r0, r6, #160
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L902:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #8192
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L903
	mov	r1, r5
	add	r0, r6, #164
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L903:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #16384
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L904
	mov	r1, r5
	add	r0, r6, #168
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L904:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #32768
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L905
	mov	r1, r5
	add	r0, r6, #172
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L905:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #65536
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L906
	mov	r1, r5
	add	r0, r6, #176
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L906:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #131072
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L907
	mov	r1, r5
	add	r0, r6, #180
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L907:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #262144
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L908
	mov	r1, r5
	add	r0, r6, #184
	mov	r2, #28
	bl	memcpy
	add	r5, r5, #28
	add	r7, r7, #28
.L908:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #524288
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L909
	mov	r1, r5
	add	r0, r6, #212
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L909:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #1048576
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L910
	mov	r1, r5
	add	r0, r6, #216
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L910:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #2097152
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L911
	mov	r1, r5
	add	r0, r6, #220
	mov	r2, #6
	bl	memcpy
	add	r5, r5, #6
	add	r7, r7, #6
.L911:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #33554432
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L912
	mov	r1, r5
	add	r0, r6, #240
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L912:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #67108864
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L913
	mov	r1, r5
	add	r0, r6, #244
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L913:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #134217728
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L914
	mov	r1, r5
	add	r0, r6, #248
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L914:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #268435456
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L915
	mov	r1, r5
	add	r0, r6, #252
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L915:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #536870912
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L916
	mov	r1, r5
	add	r0, r6, #256
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L916:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #1073741824
	mov	r3, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L917
	mov	r1, r5
	add	r0, r6, #260
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L917:
	add	r3, sp, #24
	ldmia	r3, {r2-r3}
	mov	r1, #0
	mov	r0, #-2147483648
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L918
	mov	r1, r5
	add	r0, r6, #264
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L918:
	add	r3, sp, #24
	ldmia	r3, {r2-r3}
	mov	r1, #1
	mov	r0, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L919
	mov	r1, r5
	add	r0, r6, #268
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L919:
	add	r3, sp, #24
	ldmia	r3, {r2-r3}
	mov	r1, #2
	mov	r0, #0
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L920
	mov	r1, r5
	add	r0, r6, #272
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L920:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #4
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L921
	mov	r1, r5
	add	r0, r6, #280
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L921:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #8
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L922
	mov	r1, r5
	add	r0, r6, #284
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L922:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #16
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L923
	mov	r1, r5
	add	r0, r6, #288
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L923:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #32
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L924
	mov	r1, r5
	add	r0, r6, #292
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L924:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #64
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L925
	mov	r1, r5
	add	r0, r6, #320
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L925:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #128
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L926
	mov	r1, r5
	add	r0, r6, #324
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L926:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #256
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L927
	mov	r1, r5
	add	r0, r6, #328
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L927:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #512
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L928
	mov	r1, r5
	add	r0, r6, #332
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L928:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #1024
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L929
	mov	r1, r5
	add	r0, r6, #336
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L929:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #2048
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L930
	mov	r1, r5
	add	r0, r6, #340
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L930:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #4096
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L931
	mov	r1, r5
	add	r0, r6, #344
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L931:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #8192
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L932
	mov	r1, r5
	add	r0, r6, #348
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L932:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #16384
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L933
	mov	r1, r5
	add	r0, r6, #360
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L933:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #32768
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L934
	mov	r1, r5
	add	r0, r6, #364
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L934:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #65536
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r0, r2, r3
	beq	.L935
	mov	r1, r5
	add	r0, r6, #368
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L935:
	add	r1, sp, #24
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #131072
	and	r2, r2, r0
	and	r3, r3, r1
	orrs	r1, r2, r3
	beq	.L936
	mov	r1, r5
	add	r0, r6, #372
	mov	r2, #4
	bl	memcpy
	add	r5, r5, #4
	add	r7, r7, #4
.L936:
	ldr	r3, [sp, #20]
	mov	r0, r6
	stmia	sp, {r3, sl}
	add	r3, sp, #24
	ldmia	r3, {r2-r3}
	mov	r1, fp
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	ldr	r2, [sp, #16]
	mov	r3, #0
	bl	nm_STATION_GROUP_store_changes
	mov	r0, r6
	ldr	r1, .L942+8
	ldr	r2, .L942+16
	bl	mem_free_debug
	add	r9, r9, #1
	mov	r4, r5
.L887:
	ldr	r3, [sp, #32]
	cmp	r9, r3
	bcc	.L937
	cmp	r7, #0
	beq	.L938
	cmp	sl, #1
	cmpne	sl, #15
	beq	.L939
	cmp	sl, #16
	bne	.L940
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L940
.L939:
	mov	r0, #13
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L940
.L938:
	ldr	r0, .L942+8
	ldr	r1, .L942+20
	bl	Alert_bit_set_with_no_data_with_filename
.L940:
	mov	r0, r7
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L943:
	.align	2
.L942:
	.word	.LANCHOR1
	.word	list_program_data_recursive_MUTEX
	.word	.LC32
	.word	2904
	.word	3286
	.word	3338
.LFE47:
	.size	nm_STATION_GROUP_extract_and_store_changes_from_comm, .-nm_STATION_GROUP_extract_and_store_changes_from_comm
	.section	.text.STATION_GROUP_extract_and_store_group_name_from_GuiVars,"ax",%progbits
	.align	2
	.global	STATION_GROUP_extract_and_store_group_name_from_GuiVars
	.type	STATION_GROUP_extract_and_store_group_name_from_GuiVars, %function
STATION_GROUP_extract_and_store_group_name_from_GuiVars:
.LFB79:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L946
	stmfd	sp!, {r4, r5, lr}
.LCFI218:
	ldr	r0, [r3, #0]
	sub	sp, sp, #20
.LCFI219:
	mov	r1, #400
	ldr	r2, .L946+4
	ldr	r3, .L946+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L946+12
	ldr	r0, [r3, #0]
	bl	STATION_GROUP_get_group_at_this_index
	subs	r4, r0, #0
	beq	.L945
	bl	FLOWSENSE_get_controller_index
	mov	r1, #2
	mov	r5, r0
	mov	r0, r4
	bl	STATION_GROUP_get_change_bits_ptr
	add	r3, r4, #312
	str	r3, [sp, #12]
	mov	r3, #0
	mov	r2, #1
	str	r3, [sp, #16]
	ldr	r1, .L946+16
	mov	r3, #2
	str	r5, [sp, #0]
	str	r2, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	SHARED_set_name_64_bit_change_bits
.L945:
	ldr	r3, .L946
	ldr	r0, [r3, #0]
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L947:
	.align	2
.L946:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	3230
	.word	g_GROUP_list_item_index
	.word	GuiVar_GroupName
.LFE79:
	.size	STATION_GROUP_extract_and_store_group_name_from_GuiVars, .-STATION_GROUP_extract_and_store_group_name_from_GuiVars
	.section	.text.STATION_GROUP_build_data_to_send,"ax",%progbits
	.align	2
	.global	STATION_GROUP_build_data_to_send
	.type	STATION_GROUP_build_data_to_send, %function
STATION_GROUP_build_data_to_send:
.LFB56:
	@ args = 4, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI220:
	mov	fp, r3
	sub	sp, sp, #64
.LCFI221:
	mov	r3, #0
	str	r3, [sp, #60]
	ldr	r3, .L966
	str	r1, [sp, #36]
	mov	r8, r0
	mov	r9, r2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L966+4
	ldr	r3, .L966+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L966+12
	bl	nm_ListGetFirst
	b	.L964
.L952:
	mov	r1, fp
	mov	r0, r4
	bl	STATION_GROUP_get_change_bits_ptr
	ldmia	r0, {r2-r3}
	orrs	r1, r2, r3
	ldrne	r3, [sp, #60]
	addne	r3, r3, #1
	strne	r3, [sp, #60]
	bne	.L951
.L950:
	ldr	r0, .L966+12
	mov	r1, r4
	bl	nm_ListGetNext
.L964:
	cmp	r0, #0
	mov	r4, r0
	bne	.L952
.L951:
	ldr	r3, [sp, #60]
	cmp	r3, #0
	beq	.L962
	ldr	r2, [sp, #100]
	mov	r3, #0
	str	r3, [sp, #60]
	str	r2, [sp, #0]
	mov	r3, r9
	mov	r0, r8
	add	r1, sp, #52
	ldr	r2, [sp, #36]
	bl	PDATA_allocate_space_for_num_changed_groups_in_pucp
	ldr	r3, [r9, #0]
	cmp	r3, #0
	str	r0, [sp, #32]
	beq	.L953
	ldr	r0, .L966+12
	bl	nm_ListGetFirst
	ldr	r3, [sp, #36]
	add	r3, r3, #16
	str	r3, [sp, #40]
	mov	r4, r0
	b	.L954
.L960:
	mov	r1, fp
	mov	r0, r4
	bl	STATION_GROUP_get_change_bits_ptr
	ldmia	r0, {r2-r3}
	mov	sl, r0
	orrs	r1, r2, r3
	beq	.L955
	ldr	r3, [r9, #0]
	cmp	r3, #0
	beq	.L956
	ldr	r2, [sp, #40]
	ldr	r3, [sp, #32]
	ldr	r1, [sp, #100]
	add	r6, r2, r3
	cmp	r6, r1
	bcs	.L956
	add	r1, r4, #16
	mov	r2, #4
	mov	r0, r8
	bl	PDATA_copy_var_into_pucp
	mov	r1, #8
	mov	r2, #4
	add	r3, sp, #56
	mov	r0, r8
	bl	PDATA_copy_bitfield_info_into_pucp
	mov	r2, #0
	mov	r3, #0
	str	r2, [sp, #44]
	str	r3, [sp, #48]
	add	r3, r4, #20
	str	r3, [sp, #4]
	ldr	r3, [sp, #100]
	mov	r2, #48
	add	r5, r4, #352
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	add	r1, sp, #44
	mov	r2, #0
	mov	r3, sl
	mov	r0, r8
	str	r5, [sp, #0]
	str	r6, [sp, #12]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	r1, [sp, #32]
	ldr	r2, [sp, #36]
	add	r3, r4, #72
	add	r6, r1, r2
	str	r3, [sp, #4]
	mov	r7, #4
	add	r1, sp, #44
	mov	r2, #1
	str	r5, [sp, #0]
	str	r7, [sp, #8]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, r0, #16
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #76
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #2
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #80
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #3
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #84
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, r7
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #88
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #5
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #92
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #6
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #96
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #7
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #100
	str	r3, [sp, #4]
	mov	r1, #48
	str	r1, [sp, #8]
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #8
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #148
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #9
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #152
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #10
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #156
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #11
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #160
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #12
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #164
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #13
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #168
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #14
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #172
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #15
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #176
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #16
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #180
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #17
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #184
	str	r3, [sp, #4]
	mov	r3, #28
	str	r3, [sp, #8]
	str	r1, [sp, #20]
	mov	r2, #18
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #212
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #19
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #216
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #20
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #220
	str	r3, [sp, #4]
	mov	r3, #6
	str	r3, [sp, #8]
	str	r1, [sp, #20]
	mov	r2, #21
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #240
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #25
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #244
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #26
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #248
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #27
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #252
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #28
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #256
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #29
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #260
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #30
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #264
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #31
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #268
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #32
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #272
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #33
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #280
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #34
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #284
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #35
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #288
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #36
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #292
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #37
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #320
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #38
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #324
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #39
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #328
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #40
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #332
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #41
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #336
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #42
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #340
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #43
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #344
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #348
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #45
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #360
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #46
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r4, #364
	stmib	sp, {r3, r7}
	add	r1, sp, #44
	mov	r2, #47
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	ldr	r3, [sp, #100]
	mov	r0, r8
	str	r3, [sp, #20]
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #100]
	add	r3, r4, #368
	stmib	sp, {r3, r7}
	str	r1, [sp, #20]
	mov	r2, #48
	add	r1, sp, #44
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r8
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #100]
	add	r3, r4, #372
	stmib	sp, {r3, r7}
	str	r2, [sp, #20]
	add	r1, sp, #44
	mov	r2, #49
	mov	r3, sl
	str	r5, [sp, #0]
	str	r9, [sp, #16]
	str	fp, [sp, #24]
	add	ip, ip, r0
	add	r6, ip, r6
	mov	r0, r8
	str	ip, [sp, #28]
	str	r6, [sp, #12]
	bl	PDATA_copy_var_into_pucp_and_set_64_bit_change_bits
	ldr	ip, [sp, #28]
	add	r5, ip, r0
	cmp	r5, #16
	bls	.L963
	b	.L965
.L956:
	mov	r3, #0
	str	r3, [r9, #0]
	b	.L959
.L965:
	ldr	r3, [sp, #60]
	ldr	r0, [sp, #56]
	add	r3, r3, #1
	add	r1, sp, #44
	mov	r2, #8
	str	r3, [sp, #60]
	bl	memcpy
	ldr	r3, [sp, #32]
	add	r3, r3, r5
	str	r3, [sp, #32]
	b	.L955
.L963:
	ldr	r3, [r8, #0]
	sub	r3, r3, #16
	str	r3, [r8, #0]
.L955:
	mov	r1, r4
	ldr	r0, .L966+12
	bl	nm_ListGetNext
	mov	r4, r0
.L954:
	cmp	r4, #0
	bne	.L960
.L959:
	ldr	r3, [sp, #60]
	cmp	r3, #0
	beq	.L961
	ldr	r0, [sp, #52]
	add	r1, sp, #60
	mov	r2, #4
	bl	memcpy
	b	.L953
.L961:
	ldr	r2, [r8, #0]
	sub	r2, r2, #4
	str	r2, [r8, #0]
.L962:
	str	r3, [sp, #32]
.L953:
	ldr	r3, .L966
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [sp, #32]
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L967:
	.align	2
.L966:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	1667
	.word	.LANCHOR1
.LFE56:
	.size	STATION_GROUP_build_data_to_send, .-STATION_GROUP_build_data_to_send
	.section	.text.STATION_GROUP_clean_house_processing,"ax",%progbits
	.align	2
	.global	STATION_GROUP_clean_house_processing
	.type	STATION_GROUP_clean_house_processing, %function
STATION_GROUP_clean_house_processing:
.LFB163:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L972
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI222:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L972+4
	ldr	r3, .L972+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L972+12
	ldr	r1, .L972+4
	ldr	r2, .L972+16
	b	.L971
.L970:
	ldr	r1, .L972+4
	ldr	r2, .L972+20
	bl	mem_free_debug
	ldr	r0, .L972+12
	ldr	r1, .L972+4
	ldr	r2, .L972+24
.L971:
	bl	nm_ListRemoveHead_debug
	cmp	r0, #0
	bne	.L970
	ldr	r4, .L972
	str	r0, [sp, #0]
	str	r0, [sp, #4]
	ldr	r1, .L972+28
	ldr	r2, .L972+32
	mov	r3, #376
	ldr	r0, .L972+12
	bl	nm_GROUP_create_new_group
	mov	r3, #376
	str	r3, [sp, #0]
	ldr	r3, [r4, #0]
	mov	r0, #1
	str	r3, [sp, #4]
	mov	r3, #13
	str	r3, [sp, #8]
	ldr	r1, .L972+36
	mov	r2, #7
	ldr	r3, .L972+12
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldr	r0, [r4, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L973:
	.align	2
.L972:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6529
	.word	.LANCHOR1
	.word	6533
	.word	6537
	.word	6539
	.word	.LANCHOR3
	.word	nm_STATION_GROUP_set_default_values
	.word	.LANCHOR0
.LFE163:
	.size	STATION_GROUP_clean_house_processing, .-STATION_GROUP_clean_house_processing
	.section	.text.STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.type	STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token, %function
STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token:
.LFB164:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI223:
	ldr	r4, .L978
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L978+4
	ldr	r3, .L978+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L978+12
	bl	nm_ListGetFirst
	b	.L977
.L976:
	add	r0, r5, #304
	ldr	r1, [r4, #0]
	bl	SHARED_set_all_64_bit_change_bits
	ldr	r0, .L978+12
	mov	r1, r5
	bl	nm_ListGetNext
.L977:
	cmp	r0, #0
	mov	r5, r0
	bne	.L976
	ldr	r4, .L978+16
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L978+4
	ldr	r3, .L978+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L978+24
	mov	r2, #1
	str	r2, [r3, #444]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L978
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L979:
	.align	2
.L978:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6562
	.word	.LANCHOR1
	.word	comm_mngr_recursive_MUTEX
	.word	6584
	.word	comm_mngr
.LFE164:
	.size	STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token, .-STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.section	.text.STATION_GROUP_on_all_groups_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	STATION_GROUP_on_all_groups_set_or_clear_commserver_change_bits
	.type	STATION_GROUP_on_all_groups_set_or_clear_commserver_change_bits, %function
STATION_GROUP_on_all_groups_set_or_clear_commserver_change_bits:
.LFB165:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI224:
	ldr	r4, .L986
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L986+4
	ldr	r3, .L986+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L986+12
	bl	nm_ListGetFirst
	b	.L985
.L984:
	cmp	r5, #51
	ldr	r1, [r4, #0]
	add	r0, r6, #312
	bne	.L982
	bl	SHARED_clear_all_64_bit_change_bits
	b	.L983
.L982:
	bl	SHARED_set_all_64_bit_change_bits
.L983:
	ldr	r0, .L986+12
	mov	r1, r6
	bl	nm_ListGetNext
.L985:
	cmp	r0, #0
	mov	r6, r0
	bne	.L984
	ldr	r3, .L986
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L987:
	.align	2
.L986:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6600
	.word	.LANCHOR1
.LFE165:
	.size	STATION_GROUP_on_all_groups_set_or_clear_commserver_change_bits, .-STATION_GROUP_on_all_groups_set_or_clear_commserver_change_bits
	.section	.text.nm_STATION_GROUP_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_STATION_GROUP_update_pending_change_bits
	.type	nm_STATION_GROUP_update_pending_change_bits, %function
nm_STATION_GROUP_update_pending_change_bits:
.LFB166:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI225:
	ldr	r6, .L995
	mov	r1, #400
	ldr	r2, .L995+4
	ldr	r3, .L995+8
	mov	r5, r0
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L995+12
	bl	nm_ListGetFirst
	mov	r8, #0
	mov	r7, #1
	mov	r4, r0
	b	.L989
.L992:
	add	r1, r4, #352
	ldmia	r1, {r0-r1}
	orrs	r3, r0, r1
	beq	.L990
	cmp	r5, #0
	beq	.L991
	add	r3, r4, #312
	ldmia	r3, {r2-r3}
	orr	r2, r2, r0
	orr	r3, r3, r1
	str	r2, [r4, #312]
	str	r3, [r4, #316]
	ldr	r3, .L995+16
	mov	r1, #2
	str	r7, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L995+20
	ldr	r2, .L995+24
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L994
.L991:
	add	r0, r4, #352
	ldr	r1, [r6, #0]
	bl	SHARED_clear_all_64_bit_change_bits
.L994:
	mov	r8, #1
.L990:
	mov	r1, r4
	ldr	r0, .L995+12
	bl	nm_ListGetNext
	mov	r4, r0
.L989:
	cmp	r4, #0
	bne	.L992
	ldr	r3, .L995
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r8, #0
	beq	.L988
	mov	r0, #13
	mov	r1, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L988:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L996:
	.align	2
.L995:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6637
	.word	.LANCHOR1
	.word	weather_preserves
	.word	cics
	.word	60000
.LFE166:
	.size	nm_STATION_GROUP_update_pending_change_bits, .-nm_STATION_GROUP_update_pending_change_bits
	.section	.text.STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group,"ax",%progbits
	.align	2
	.global	STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group
	.type	STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group, %function
STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group:
.LFB167:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI226:
	ldr	r3, .L1001
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L1001+4
	ldr	r3, .L1001+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	beq	.L998
	ldr	r3, .L1001+12
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r4, #296
	str	r3, [sp, #8]
	ldr	r3, .L1001+16
	add	r1, r4, #372
	str	r3, [sp, #12]
	mov	r0, r4
	ldr	r3, .L1001+20
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_64_bit_change_bits_group
	mov	r4, r0
	bl	MOISTURE_SENSOR_this_decoder_is_in_use_and_physically_available
	cmp	r0, #0
	moveq	r4, #0
.L998:
	ldr	r3, .L1001
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, pc}
.L1002:
	.align	2
.L1001:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6708
	.word	nm_STATION_GROUP_set_moisture_sensor_serial_number
	.word	.LC31
	.word	2097151
.LFE167:
	.size	STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group, .-STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group
	.section	.text.STATION_GROUPS_skip_irrigation_due_to_a_mow_day,"ax",%progbits
	.align	2
	.global	STATION_GROUPS_skip_irrigation_due_to_a_mow_day
	.type	STATION_GROUPS_skip_irrigation_due_to_a_mow_day, %function
STATION_GROUPS_skip_irrigation_due_to_a_mow_day:
.LFB168:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r4, lr}
.LCFI227:
	mov	r4, r3
	ldrne	r3, [r2, #32]
	ldrne	r0, [r3, #136]
	bne	.L1005
	mov	r0, r1
	bl	SCHEDULE_get_mow_day
.L1005:
	cmp	r0, #7
	moveq	r0, #0
	ldmeqfd	sp!, {r4, pc}
	ldrb	r3, [r4, #18]	@ zero_extendqisi2
	cmp	r0, #0
	cmpeq	r3, #6
	movne	r2, #0
	moveq	r2, #1
	beq	.L1007
	add	r1, r3, #1
	cmp	r1, r0
	bne	.L1008
.L1007:
	ldr	r4, [r4, #0]
	bl	NETWORK_CONFIG_get_start_of_irrigation_day
	cmp	r4, r0
	movcc	r0, #0
	movcs	r0, #1
	ldmfd	sp!, {r4, pc}
.L1008:
	cmp	r3, r0
	bne	.L1010
	ldr	r4, [r4, #0]
	bl	NETWORK_CONFIG_get_start_of_irrigation_day
	cmp	r4, r0
	movcs	r0, #0
	movcc	r0, #1
	ldmfd	sp!, {r4, pc}
.L1010:
	mov	r0, r2
	ldmfd	sp!, {r4, pc}
.LFE168:
	.size	STATION_GROUPS_skip_irrigation_due_to_a_mow_day, .-STATION_GROUPS_skip_irrigation_due_to_a_mow_day
	.section	.text.STATION_GROUPS_load_ftimes_list,"ax",%progbits
	.align	2
	.global	STATION_GROUPS_load_ftimes_list
	.type	STATION_GROUPS_load_ftimes_list, %function
STATION_GROUPS_load_ftimes_list:
.LFB169:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1022
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI228:
	ldr	r2, .L1022+4
	mov	r1, #400
	mov	r6, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L1022+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L1022+12
	bl	nm_ListGetFirst
	ldr	r5, .L1022+16
	ldr	r9, .L1022+20
	mov	r4, r0
	b	.L1012
.L1017:
	ldr	r3, [r4, #368]
	cmp	r3, #0
	beq	.L1013
	mov	r0, #256
	mov	r1, sp
	ldr	r2, .L1022+4
	ldr	r3, .L1022+24
	bl	mem_oabia
	cmp	r0, #0
	beq	.L1013
	mov	r1, #0
	mov	r2, #256
	ldr	r0, [sp, #0]
	bl	memset
	ldr	r0, [sp, #0]
	ldr	r3, [r4, #16]
	mov	r2, #48
	str	r3, [r0, #12]
	ldr	r3, [r4, #80]
	add	r1, r4, #100
	str	r3, [r0, #16]
	add	r0, r0, #20
	bl	memcpy
	ldr	r3, [r4, #148]
	ldr	r0, [sp, #0]
	ldr	r2, [r4, #168]
	str	r3, [r0, #68]
	ldr	r3, [r4, #152]
	cmp	r2, #4
	str	r3, [r0, #72]
	ldr	r3, [r4, #156]
	str	r3, [r0, #76]
	ldr	r3, [r4, #160]
	str	r3, [r0, #80]
	ldr	r3, [r4, #164]
	str	r2, [r0, #88]
	str	r3, [r0, #84]
	ldr	r3, [r4, #172]
	str	r3, [r0, #92]
	ldr	r3, [r4, #176]
	str	r3, [r0, #96]
	ldr	r3, [r4, #180]
	str	r3, [r0, #100]
	bls	.L1014
	cmp	r3, r6
	bls	.L1014
	cmp	r2, #17
	moveq	r2, #21
	beq	.L1020
	cmp	r2, #18
	subne	r2, r2, #2
	moveq	r2, #28
.L1020:
	rsb	r3, r2, r3
	cmp	r3, r6
	bhi	.L1020
	str	r3, [r0, #100]
.L1014:
	add	r1, r4, #184
	mov	r2, #28
	add	r0, r0, #104
	bl	memcpy
	ldr	r8, [sp, #0]
	ldr	r3, [r4, #212]
	mov	r7, #0
	str	r3, [r8, #132]
	ldr	r3, [r4, #216]
	mov	r0, #1
	str	r3, [r8, #136]
	ldr	r3, [r4, #240]
	mov	r1, r0
	str	r3, [r8, #140]
	ldr	r3, [r4, #244]
	ldr	r2, .L1022+28
	str	r3, [r8, #144]
	ldr	r3, [r4, #268]
	str	r3, [r8, #148]
	ldr	r3, [r4, #272]
	str	r3, [r8, #152]
	ldr	r3, [r4, #280]
	str	r3, [r8, #160]
	ldr	r3, [r4, #284]
	str	r3, [r8, #164]
	ldr	r3, [r4, #288]
	str	r3, [r8, #168]
	ldr	r3, [r4, #260]
	str	r3, [r8, #172]
	ldr	r3, [r4, #264]
	str	r3, [r8, #176]
	ldr	r3, [r4, #364]
	str	r7, [r8, #220]
	str	r3, [r8, #180]
	str	r5, [r8, #224]
	bl	DMYToDate
	ldr	sl, [sp, #0]
	ldr	r2, .L1022+28
	strh	r0, [r8, #228]	@ movhi
	mov	r8, #1
	strh	r9, [sl, #230]	@ movhi
	strh	r8, [sl, #232]	@ movhi
	mov	r1, r8
	mov	r0, r8
	bl	DMYToDate
	mov	r1, r8
	ldr	r2, .L1022+28
	strh	r0, [sl, #234]	@ movhi
	ldr	sl, [sp, #0]
	mov	r0, r8
	str	r5, [sl, #236]
	bl	DMYToDate
	ldr	r1, [sp, #0]
	str	r7, [r1, #244]
	str	r7, [r1, #248]
	str	r7, [r1, #252]
	strh	r0, [sl, #240]	@ movhi
	ldr	r0, .L1022+32
	bl	nm_ListInsertTail
.L1013:
	mov	r1, r4
	ldr	r0, .L1022+12
	bl	nm_ListGetNext
	mov	r4, r0
.L1012:
	cmp	r4, #0
	bne	.L1017
	ldr	r3, .L1022
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, pc}
.L1023:
	.align	2
.L1022:
	.word	list_program_data_recursive_MUTEX
	.word	.LC51
	.word	6815
	.word	.LANCHOR1
	.word	86400
	.word	20864
	.word	6823
	.word	2011
	.word	ft_station_groups_list_hdr
.LFE169:
	.size	STATION_GROUPS_load_ftimes_list, .-STATION_GROUPS_load_ftimes_list
	.section	.text.STATION_GROUPS_calculate_chain_sync_crc,"ax",%progbits
	.align	2
	.global	STATION_GROUPS_calculate_chain_sync_crc
	.type	STATION_GROUPS_calculate_chain_sync_crc, %function
STATION_GROUPS_calculate_chain_sync_crc:
.LFB170:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1029
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI229:
	ldr	r5, .L1029+4
	mov	r1, #400
	ldr	r2, .L1029+8
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L1029+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #8]
	mov	r0, #376
	mul	r0, r3, r0
	mov	r1, sp
	ldr	r2, .L1029+8
	ldr	r3, .L1029+16
	bl	mem_oabia
	subs	r6, r0, #0
	beq	.L1025
	ldr	r3, [sp, #0]
	add	r6, sp, #8
	mov	r0, r5
	str	r3, [r6, #-4]!
	bl	nm_ListGetFirst
	mov	r7, #0
	mov	r5, r0
	b	.L1026
.L1027:
	add	r8, r5, #20
	mov	r0, r8
	bl	strlen
	mov	r1, r8
	mov	r2, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #72
	mov	r2, #4
	mov	r8, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #76
	mov	r2, #4
	add	r0, r8, r0
	add	r7, r0, r7
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #80
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #84
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #88
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #92
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #96
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #100
	mov	r2, #48
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #148
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #152
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #156
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #160
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #164
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #168
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #172
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #176
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #180
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #184
	mov	r2, #28
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #212
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #216
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #240
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #244
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #248
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #252
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #256
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #260
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #264
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #268
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #272
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #280
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #284
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #288
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #320
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #324
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #328
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #332
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #336
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #340
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #344
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #348
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #360
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #364
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #368
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #372
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r1, r5
	add	r7, r7, r0
	ldr	r0, .L1029+4
	bl	nm_ListGetNext
	mov	r5, r0
.L1026:
	cmp	r5, #0
	bne	.L1027
	mov	r1, r7
	ldr	r0, [sp, #0]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L1029+20
	add	r4, r4, #43
	ldr	r1, .L1029+8
	ldr	r2, .L1029+24
	mov	r6, #1
	str	r0, [r3, r4, asl #2]
	ldr	r0, [sp, #0]
	bl	mem_free_debug
	b	.L1028
.L1025:
	ldr	r3, .L1029+28
	ldr	r0, .L1029+32
	ldr	r1, [r3, r4, asl #4]
	bl	Alert_Message_va
.L1028:
	ldr	r3, .L1029
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L1030:
	.align	2
.L1029:
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR1
	.word	.LC51
	.word	6965
	.word	6977
	.word	cscs
	.word	7121
	.word	chain_sync_file_pertinants
	.word	.LC80
.LFE170:
	.size	STATION_GROUPS_calculate_chain_sync_crc, .-STATION_GROUPS_calculate_chain_sync_crc
	.global	station_group_list_item_sizes
	.global	STATION_GROUP_DEFAULT_NAME
	.section	.rodata.CSWTCH.491,"a",%progbits
	.set	.LANCHOR5,. + 0
	.type	CSWTCH.491, %object
	.size	CSWTCH.491, 3
CSWTCH.491:
	.byte	4
	.byte	10
	.byte	11
	.section	.rodata.STATION_GROUP_DEFAULT_NAME,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	STATION_GROUP_DEFAULT_NAME, %object
	.size	STATION_GROUP_DEFAULT_NAME, 14
STATION_GROUP_DEFAULT_NAME:
	.ascii	"Station Group\000"
	.section	.rodata.CSWTCH.489,"a",%progbits
	.set	.LANCHOR4,. + 0
	.type	CSWTCH.489, %object
	.size	CSWTCH.489, 3
CSWTCH.489:
	.byte	7
	.byte	4
	.byte	3
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"PlantType\000"
.LC1:
	.ascii	"HeadType\000"
.LC2:
	.ascii	"PrecipRate\000"
.LC3:
	.ascii	"SoilType\000"
.LC4:
	.ascii	"SlopePercentage\000"
.LC5:
	.ascii	"UsableRainPercentage\000"
.LC6:
	.ascii	"SoilIntakeRate\000"
.LC7:
	.ascii	"AllowableSurfaceAccum\000"
.LC8:
	.ascii	"PriorityLevel\000"
.LC9:
	.ascii	"PercentAdjust\000"
.LC10:
	.ascii	"MowDay\000"
.LC11:
	.ascii	"ScheduleType\000"
.LC12:
	.ascii	"HighFlowAction\000"
.LC13:
	.ascii	"LowFlowAction\000"
.LC14:
	.ascii	"LineFillTime\000"
.LC15:
	.ascii	"ValveCloseTime\000"
.LC16:
	.ascii	"BudgetReductionCap\000"
.LC17:
	.ascii	"EndDate\000"
.LC18:
	.ascii	"Enabled\000"
.LC19:
	.ascii	"IrrigateOn29Or31\000"
.LC20:
	.ascii	"ET_Mode\000"
.LC21:
	.ascii	"UseET_Averaging\000"
.LC22:
	.ascii	"RainInUse\000"
.LC23:
	.ascii	"WindInUse\000"
.LC24:
	.ascii	"PumpInUse\000"
.LC25:
	.ascii	"InUse\000"
.LC26:
	.ascii	"AcquireExpecteds\000"
.LC27:
	.ascii	"StartTime\000"
.LC28:
	.ascii	"StopTime\000"
.LC29:
	.ascii	"On_At_A_TimeInGroup\000"
.LC30:
	.ascii	"On_At_A_TimeInSystem\000"
.LC31:
	.ascii	"MoisSensorSerNum\000"
.LC32:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/shared_station_groups.c\000"
.LC33:
	.ascii	"<NONE ASSIGNED>\000"
.LC34:
	.ascii	"ERROR - SENSOR NOT FOUND\000"
.LC35:
	.ascii	"%s%d\000"
.LC36:
	.ascii	"CropCoefficient\000"
.LC37:
	.ascii	"MaximumAccumulation\000"
.LC38:
	.ascii	"%s%s\000"
.LC39:
	.ascii	"WaterDays\000"
.LC40:
	.ascii	"SystemID\000"
.LC41:
	.ascii	"MicroclimateFactor\000"
.LC42:
	.ascii	"StartDate\000"
.LC43:
	.ascii	"AllowableDepletion\000"
.LC44:
	.ascii	"AvailableWater\000"
.LC45:
	.ascii	"RootDepth\000"
.LC46:
	.ascii	"SpeciesFactor\000"
.LC47:
	.ascii	"DensityFactor\000"
.LC48:
	.ascii	"BeginDate\000"
.LC49:
	.ascii	"LastRan\000"
.LC50:
	.ascii	"Exposure\000"
.LC51:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/station_groups.c\000"
.LC52:
	.ascii	"Turf\000"
.LC53:
	.ascii	"Annuals\000"
.LC54:
	.ascii	"Trees\000"
.LC55:
	.ascii	"Ground Cover\000"
.LC56:
	.ascii	"Shrubs\000"
.LC57:
	.ascii	"Mixed\000"
.LC58:
	.ascii	"Natives\000"
.LC59:
	.ascii	"UNK PLANT\000"
.LC60:
	.ascii	"Spray\000"
.LC61:
	.ascii	"Rotor\000"
.LC62:
	.ascii	"Impact\000"
.LC63:
	.ascii	"Bubbler\000"
.LC64:
	.ascii	"Drip\000"
.LC65:
	.ascii	"UNK HEAD\000"
.LC66:
	.ascii	"%s %s %s\000"
.LC67:
	.ascii	"LNDSCAPE DTLS file unexpd update %u\000"
.LC68:
	.ascii	"LNDSCAPE DTLS file update : to revision %u from %u\000"
.LC69:
	.ascii	"LNDSCAPE DTLS updater error\000"
.LC70:
	.ascii	"%s %s\000"
.LC71:
	.ascii	"Group with this GID not in the list. : %s, %u\000"
.LC72:
	.ascii	"%s\000"
.LC73:
	.ascii	"  <%s %s>\000"
.LC74:
	.ascii	"Percent Adjust Days\000"
.LC75:
	.ascii	"too many ON in group : %s, %u\000"
.LC76:
	.ascii	"More valves ON than the limit! : %s, %u\000"
.LC77:
	.ascii	"Invalid schedule type\000"
.LC78:
	.ascii	"pgroup\000"
.LC79:
	.ascii	"station_group_group_list_hdr\000"
.LC80:
	.ascii	"SYNC: no mem to calc checksum %s\000"
	.section	.bss.station_group_group_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	station_group_group_list_hdr, %object
	.size	station_group_group_list_hdr, 20
station_group_group_list_hdr:
	.space	20
	.section	.rodata.station_group_list_item_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	station_group_list_item_sizes, %object
	.size	station_group_list_item_sizes, 32
station_group_list_item_sizes:
	.word	320
	.word	320
	.word	352
	.word	360
	.word	364
	.word	372
	.word	372
	.word	376
	.section	.rodata.LANDSCAPE_DETAILS_FILENAME,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	LANDSCAPE_DETAILS_FILENAME, %object
	.size	LANDSCAPE_DETAILS_FILENAME, 18
LANDSCAPE_DETAILS_FILENAME:
	.ascii	"LANDSCAPE_DETAILS\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI6-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI8-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI10-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI12-.LFB40
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI14-.LFB41
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI16-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI18-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI20-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI22-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI24-.LFB26
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI26-.LFB27
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI28-.LFB31
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI29-.LCFI28
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI30-.LFB32
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI32-.LFB42
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI34-.LFB11
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI35-.LCFI34
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI36-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI37-.LCFI36
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI38-.LFB19
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI39-.LCFI38
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI40-.LFB21
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI41-.LCFI40
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI42-.LFB22
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI43-.LCFI42
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI44-.LFB23
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI45-.LCFI44
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI46-.LFB25
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI47-.LCFI46
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI48-.LFB30
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.byte	0x4
	.4byte	.LCFI50-.LFB44
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI51-.LCFI50
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI52-.LFB33
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI53-.LCFI52
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI54-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI55-.LCFI54
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI56-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI57-.LCFI56
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI58-.LFB28
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI59-.LCFI58
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI60-.LFB29
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI61-.LCFI60
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.byte	0x4
	.4byte	.LCFI62-.LFB45
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI63-.LCFI62
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI64-.LFB7
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI65-.LCFI64
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI66-.LFB24
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI67-.LCFI66
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI68-.LFB17
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI69-.LCFI68
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI70-.LFB43
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI71-.LCFI70
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB183
	.4byte	.LFE183-.LFB183
	.byte	0x4
	.4byte	.LCFI72-.LFB183
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI73-.LCFI72
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB182
	.4byte	.LFE182-.LFB182
	.byte	0x4
	.4byte	.LCFI74-.LFB182
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI75-.LCFI74
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB181
	.4byte	.LFE181-.LFB181
	.byte	0x4
	.4byte	.LCFI76-.LFB181
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI77-.LCFI76
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB180
	.4byte	.LFE180-.LFB180
	.byte	0x4
	.4byte	.LCFI78-.LFB180
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI79-.LCFI78
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB179
	.4byte	.LFE179-.LFB179
	.byte	0x4
	.4byte	.LCFI80-.LFB179
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI81-.LCFI80
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB178
	.4byte	.LFE178-.LFB178
	.byte	0x4
	.4byte	.LCFI82-.LFB178
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI83-.LCFI82
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB177
	.4byte	.LFE177-.LFB177
	.byte	0x4
	.4byte	.LCFI84-.LFB177
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI85-.LCFI84
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI86-.LFB18
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI87-.LCFI86
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE86:
.LSFDE88:
	.4byte	.LEFDE88-.LASFDE88
.LASFDE88:
	.4byte	.Lframe0
	.4byte	.LFB176
	.4byte	.LFE176-.LFB176
	.byte	0x4
	.4byte	.LCFI88-.LFB176
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE88:
.LSFDE90:
	.4byte	.LEFDE90-.LASFDE90
.LASFDE90:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI89-.LFB20
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI90-.LCFI89
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE90:
.LSFDE92:
	.4byte	.LEFDE92-.LASFDE92
.LASFDE92:
	.4byte	.Lframe0
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.byte	0x4
	.4byte	.LCFI91-.LFB53
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI92-.LCFI91
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE92:
.LSFDE94:
	.4byte	.LEFDE94-.LASFDE94
.LASFDE94:
	.4byte	.Lframe0
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.byte	0x4
	.4byte	.LCFI93-.LFB50
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE94:
.LSFDE96:
	.4byte	.LEFDE96-.LASFDE96
.LASFDE96:
	.4byte	.Lframe0
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.byte	0x4
	.4byte	.LCFI94-.LFB51
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI95-.LCFI94
	.byte	0xe
	.uleb128 0x78
	.align	2
.LEFDE96:
.LSFDE98:
	.4byte	.LEFDE98-.LASFDE98
.LASFDE98:
	.4byte	.Lframe0
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.byte	0x4
	.4byte	.LCFI96-.LFB49
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI97-.LCFI96
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE98:
.LSFDE100:
	.4byte	.LEFDE100-.LASFDE100
.LASFDE100:
	.4byte	.Lframe0
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.byte	0x4
	.4byte	.LCFI98-.LFB48
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI99-.LCFI98
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI100-.LCFI99
	.byte	0xe
	.uleb128 0x80
	.align	2
.LEFDE100:
.LSFDE102:
	.4byte	.LEFDE102-.LASFDE102
.LASFDE102:
	.4byte	.Lframe0
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.byte	0x4
	.4byte	.LCFI101-.LFB54
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE102:
.LSFDE104:
	.4byte	.LEFDE104-.LASFDE104
.LASFDE104:
	.4byte	.Lframe0
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.byte	0x4
	.4byte	.LCFI102-.LFB55
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE104:
.LSFDE106:
	.4byte	.LEFDE106-.LASFDE106
.LASFDE106:
	.4byte	.Lframe0
	.4byte	.LFB103
	.4byte	.LFE103-.LFB103
	.byte	0x4
	.4byte	.LCFI103-.LFB103
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE106:
.LSFDE108:
	.4byte	.LEFDE108-.LASFDE108
.LASFDE108:
	.4byte	.Lframe0
	.4byte	.LFB104
	.4byte	.LFE104-.LFB104
	.byte	0x4
	.4byte	.LCFI104-.LFB104
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE108:
.LSFDE110:
	.4byte	.LEFDE110-.LASFDE110
.LASFDE110:
	.4byte	.Lframe0
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.byte	0x4
	.4byte	.LCFI105-.LFB75
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE110:
.LSFDE112:
	.4byte	.LEFDE112-.LASFDE112
.LASFDE112:
	.4byte	.Lframe0
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.byte	0x4
	.4byte	.LCFI106-.LFB76
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE112:
.LSFDE114:
	.4byte	.LEFDE114-.LASFDE114
.LASFDE114:
	.4byte	.Lframe0
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.byte	0x4
	.4byte	.LCFI107-.LFB73
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE114:
.LSFDE116:
	.4byte	.LEFDE116-.LASFDE116
.LASFDE116:
	.4byte	.Lframe0
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.byte	0x4
	.4byte	.LCFI108-.LFB74
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE116:
.LSFDE118:
	.4byte	.LEFDE118-.LASFDE118
.LASFDE118:
	.4byte	.Lframe0
	.4byte	.LFB71
	.4byte	.LFE71-.LFB71
	.byte	0x4
	.4byte	.LCFI109-.LFB71
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE118:
.LSFDE120:
	.4byte	.LEFDE120-.LASFDE120
.LASFDE120:
	.4byte	.Lframe0
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.byte	0x4
	.4byte	.LCFI110-.LFB72
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE120:
.LSFDE122:
	.4byte	.LEFDE122-.LASFDE122
.LASFDE122:
	.4byte	.Lframe0
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.byte	0x4
	.4byte	.LCFI111-.LFB69
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE122:
.LSFDE124:
	.4byte	.LEFDE124-.LASFDE124
.LASFDE124:
	.4byte	.Lframe0
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.byte	0x4
	.4byte	.LCFI112-.LFB70
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE124:
.LSFDE126:
	.4byte	.LEFDE126-.LASFDE126
.LASFDE126:
	.4byte	.Lframe0
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.byte	0x4
	.4byte	.LCFI113-.LFB67
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE126:
.LSFDE128:
	.4byte	.LEFDE128-.LASFDE128
.LASFDE128:
	.4byte	.Lframe0
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.byte	0x4
	.4byte	.LCFI114-.LFB68
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE128:
.LSFDE130:
	.4byte	.LEFDE130-.LASFDE130
.LASFDE130:
	.4byte	.Lframe0
	.4byte	.LFB65
	.4byte	.LFE65-.LFB65
	.byte	0x4
	.4byte	.LCFI115-.LFB65
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE130:
.LSFDE132:
	.4byte	.LEFDE132-.LASFDE132
.LASFDE132:
	.4byte	.Lframe0
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.byte	0x4
	.4byte	.LCFI116-.LFB66
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE132:
.LSFDE134:
	.4byte	.LEFDE134-.LASFDE134
.LASFDE134:
	.4byte	.Lframe0
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.byte	0x4
	.4byte	.LCFI117-.LFB63
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE134:
.LSFDE136:
	.4byte	.LEFDE136-.LASFDE136
.LASFDE136:
	.4byte	.Lframe0
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.byte	0x4
	.4byte	.LCFI118-.LFB64
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE136:
.LSFDE138:
	.4byte	.LEFDE138-.LASFDE138
.LASFDE138:
	.4byte	.Lframe0
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.byte	0x4
	.4byte	.LCFI119-.LFB62
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI120-.LCFI119
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE138:
.LSFDE140:
	.4byte	.LEFDE140-.LASFDE140
.LASFDE140:
	.4byte	.Lframe0
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.byte	0x4
	.4byte	.LCFI121-.LFB60
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI122-.LCFI121
	.byte	0xe
	.uleb128 0x54
	.align	2
.LEFDE140:
.LSFDE142:
	.4byte	.LEFDE142-.LASFDE142
.LASFDE142:
	.4byte	.Lframe0
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.byte	0x4
	.4byte	.LCFI123-.LFB61
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE142:
.LSFDE144:
	.4byte	.LEFDE144-.LASFDE144
.LASFDE144:
	.4byte	.Lframe0
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.byte	0x4
	.4byte	.LCFI124-.LFB58
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE144:
.LSFDE146:
	.4byte	.LEFDE146-.LASFDE146
.LASFDE146:
	.4byte	.Lframe0
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.byte	0x4
	.4byte	.LCFI125-.LFB59
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE146:
.LSFDE148:
	.4byte	.LEFDE148-.LASFDE148
.LASFDE148:
	.4byte	.Lframe0
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.byte	0x4
	.4byte	.LCFI126-.LFB57
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE148:
.LSFDE150:
	.4byte	.LEFDE150-.LASFDE150
.LASFDE150:
	.4byte	.Lframe0
	.4byte	.LFB105
	.4byte	.LFE105-.LFB105
	.byte	0x4
	.4byte	.LCFI127-.LFB105
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE150:
.LSFDE152:
	.4byte	.LEFDE152-.LASFDE152
.LASFDE152:
	.4byte	.Lframe0
	.4byte	.LFB106
	.4byte	.LFE106-.LFB106
	.byte	0x4
	.4byte	.LCFI128-.LFB106
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE152:
.LSFDE154:
	.4byte	.LEFDE154-.LASFDE154
.LASFDE154:
	.4byte	.Lframe0
	.4byte	.LFB107
	.4byte	.LFE107-.LFB107
	.byte	0x4
	.4byte	.LCFI129-.LFB107
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE154:
.LSFDE156:
	.4byte	.LEFDE156-.LASFDE156
.LASFDE156:
	.4byte	.Lframe0
	.4byte	.LFB102
	.4byte	.LFE102-.LFB102
	.byte	0x4
	.4byte	.LCFI130-.LFB102
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE156:
.LSFDE158:
	.4byte	.LEFDE158-.LASFDE158
.LASFDE158:
	.4byte	.Lframe0
	.4byte	.LFB77
	.4byte	.LFE77-.LFB77
	.byte	0x4
	.4byte	.LCFI131-.LFB77
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE158:
.LSFDE160:
	.4byte	.LEFDE160-.LASFDE160
.LASFDE160:
	.4byte	.Lframe0
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.byte	0x4
	.4byte	.LCFI132-.LFB78
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE160:
.LSFDE162:
	.4byte	.LEFDE162-.LASFDE162
.LASFDE162:
	.4byte	.Lframe0
	.4byte	.LFB108
	.4byte	.LFE108-.LFB108
	.byte	0x4
	.4byte	.LCFI133-.LFB108
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE162:
.LSFDE164:
	.4byte	.LEFDE164-.LASFDE164
.LASFDE164:
	.4byte	.Lframe0
	.4byte	.LFB109
	.4byte	.LFE109-.LFB109
	.byte	0x4
	.4byte	.LCFI134-.LFB109
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE164:
.LSFDE166:
	.4byte	.LEFDE166-.LASFDE166
.LASFDE166:
	.4byte	.Lframe0
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.byte	0x4
	.4byte	.LCFI135-.LFB110
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE166:
.LSFDE168:
	.4byte	.LEFDE168-.LASFDE168
.LASFDE168:
	.4byte	.Lframe0
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.byte	0x4
	.4byte	.LCFI136-.LFB111
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE168:
.LSFDE170:
	.4byte	.LEFDE170-.LASFDE170
.LASFDE170:
	.4byte	.Lframe0
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.byte	0x4
	.4byte	.LCFI137-.LFB112
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE170:
.LSFDE172:
	.4byte	.LEFDE172-.LASFDE172
.LASFDE172:
	.4byte	.Lframe0
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.byte	0x4
	.4byte	.LCFI138-.LFB113
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE172:
.LSFDE174:
	.4byte	.LEFDE174-.LASFDE174
.LASFDE174:
	.4byte	.Lframe0
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.byte	0x4
	.4byte	.LCFI139-.LFB114
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE174:
.LSFDE176:
	.4byte	.LEFDE176-.LASFDE176
.LASFDE176:
	.4byte	.Lframe0
	.4byte	.LFB115
	.4byte	.LFE115-.LFB115
	.byte	0x4
	.4byte	.LCFI140-.LFB115
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE176:
.LSFDE178:
	.4byte	.LEFDE178-.LASFDE178
.LASFDE178:
	.4byte	.Lframe0
	.4byte	.LFB116
	.4byte	.LFE116-.LFB116
	.byte	0x4
	.4byte	.LCFI141-.LFB116
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE178:
.LSFDE180:
	.4byte	.LEFDE180-.LASFDE180
.LASFDE180:
	.4byte	.Lframe0
	.4byte	.LFB117
	.4byte	.LFE117-.LFB117
	.byte	0x4
	.4byte	.LCFI142-.LFB117
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE180:
.LSFDE182:
	.4byte	.LEFDE182-.LASFDE182
.LASFDE182:
	.4byte	.Lframe0
	.4byte	.LFB118
	.4byte	.LFE118-.LFB118
	.byte	0x4
	.4byte	.LCFI143-.LFB118
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE182:
.LSFDE184:
	.4byte	.LEFDE184-.LASFDE184
.LASFDE184:
	.4byte	.Lframe0
	.4byte	.LFB119
	.4byte	.LFE119-.LFB119
	.byte	0x4
	.4byte	.LCFI144-.LFB119
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE184:
.LSFDE186:
	.4byte	.LEFDE186-.LASFDE186
.LASFDE186:
	.4byte	.Lframe0
	.4byte	.LFB122
	.4byte	.LFE122-.LFB122
	.byte	0x4
	.4byte	.LCFI145-.LFB122
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE186:
.LSFDE188:
	.4byte	.LEFDE188-.LASFDE188
.LASFDE188:
	.4byte	.Lframe0
	.4byte	.LFB123
	.4byte	.LFE123-.LFB123
	.byte	0x4
	.4byte	.LCFI146-.LFB123
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE188:
.LSFDE190:
	.4byte	.LEFDE190-.LASFDE190
.LASFDE190:
	.4byte	.Lframe0
	.4byte	.LFB124
	.4byte	.LFE124-.LFB124
	.byte	0x4
	.4byte	.LCFI147-.LFB124
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE190:
.LSFDE192:
	.4byte	.LEFDE192-.LASFDE192
.LASFDE192:
	.4byte	.Lframe0
	.4byte	.LFB125
	.4byte	.LFE125-.LFB125
	.byte	0x4
	.4byte	.LCFI148-.LFB125
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE192:
.LSFDE194:
	.4byte	.LEFDE194-.LASFDE194
.LASFDE194:
	.4byte	.Lframe0
	.4byte	.LFB126
	.4byte	.LFE126-.LFB126
	.byte	0x4
	.4byte	.LCFI149-.LFB126
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI150-.LCFI149
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE194:
.LSFDE196:
	.4byte	.LEFDE196-.LASFDE196
.LASFDE196:
	.4byte	.Lframe0
	.4byte	.LFB127
	.4byte	.LFE127-.LFB127
	.byte	0x4
	.4byte	.LCFI151-.LFB127
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE196:
.LSFDE198:
	.4byte	.LEFDE198-.LASFDE198
.LASFDE198:
	.4byte	.Lframe0
	.4byte	.LFB128
	.4byte	.LFE128-.LFB128
	.byte	0x4
	.4byte	.LCFI152-.LFB128
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE198:
.LSFDE200:
	.4byte	.LEFDE200-.LASFDE200
.LASFDE200:
	.4byte	.Lframe0
	.4byte	.LFB129
	.4byte	.LFE129-.LFB129
	.byte	0x4
	.4byte	.LCFI153-.LFB129
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE200:
.LSFDE202:
	.4byte	.LEFDE202-.LASFDE202
.LASFDE202:
	.4byte	.Lframe0
	.4byte	.LFB130
	.4byte	.LFE130-.LFB130
	.byte	0x4
	.4byte	.LCFI154-.LFB130
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI155-.LCFI154
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE202:
.LSFDE204:
	.4byte	.LEFDE204-.LASFDE204
.LASFDE204:
	.4byte	.Lframe0
	.4byte	.LFB131
	.4byte	.LFE131-.LFB131
	.byte	0x4
	.4byte	.LCFI156-.LFB131
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI157-.LCFI156
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE204:
.LSFDE206:
	.4byte	.LEFDE206-.LASFDE206
.LASFDE206:
	.4byte	.Lframe0
	.4byte	.LFB132
	.4byte	.LFE132-.LFB132
	.byte	0x4
	.4byte	.LCFI158-.LFB132
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE206:
.LSFDE208:
	.4byte	.LEFDE208-.LASFDE208
.LASFDE208:
	.4byte	.Lframe0
	.4byte	.LFB133
	.4byte	.LFE133-.LFB133
	.byte	0x4
	.4byte	.LCFI159-.LFB133
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE208:
.LSFDE210:
	.4byte	.LEFDE210-.LASFDE210
.LASFDE210:
	.4byte	.Lframe0
	.4byte	.LFB134
	.4byte	.LFE134-.LFB134
	.byte	0x4
	.4byte	.LCFI160-.LFB134
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE210:
.LSFDE212:
	.4byte	.LEFDE212-.LASFDE212
.LASFDE212:
	.4byte	.Lframe0
	.4byte	.LFB135
	.4byte	.LFE135-.LFB135
	.byte	0x4
	.4byte	.LCFI161-.LFB135
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE212:
.LSFDE214:
	.4byte	.LEFDE214-.LASFDE214
.LASFDE214:
	.4byte	.Lframe0
	.4byte	.LFB136
	.4byte	.LFE136-.LFB136
	.byte	0x4
	.4byte	.LCFI162-.LFB136
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE214:
.LSFDE216:
	.4byte	.LEFDE216-.LASFDE216
.LASFDE216:
	.4byte	.Lframe0
	.4byte	.LFB137
	.4byte	.LFE137-.LFB137
	.byte	0x4
	.4byte	.LCFI163-.LFB137
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI164-.LCFI163
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE216:
.LSFDE218:
	.4byte	.LEFDE218-.LASFDE218
.LASFDE218:
	.4byte	.Lframe0
	.4byte	.LFB138
	.4byte	.LFE138-.LFB138
	.byte	0x4
	.4byte	.LCFI165-.LFB138
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE218:
.LSFDE220:
	.4byte	.LEFDE220-.LASFDE220
.LASFDE220:
	.4byte	.Lframe0
	.4byte	.LFB139
	.4byte	.LFE139-.LFB139
	.byte	0x4
	.4byte	.LCFI166-.LFB139
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE220:
.LSFDE222:
	.4byte	.LEFDE222-.LASFDE222
.LASFDE222:
	.4byte	.Lframe0
	.4byte	.LFB140
	.4byte	.LFE140-.LFB140
	.byte	0x4
	.4byte	.LCFI167-.LFB140
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE222:
.LSFDE224:
	.4byte	.LEFDE224-.LASFDE224
.LASFDE224:
	.4byte	.Lframe0
	.4byte	.LFB141
	.4byte	.LFE141-.LFB141
	.byte	0x4
	.4byte	.LCFI168-.LFB141
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE224:
.LSFDE226:
	.4byte	.LEFDE226-.LASFDE226
.LASFDE226:
	.4byte	.Lframe0
	.4byte	.LFB142
	.4byte	.LFE142-.LFB142
	.byte	0x4
	.4byte	.LCFI169-.LFB142
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE226:
.LSFDE228:
	.4byte	.LEFDE228-.LASFDE228
.LASFDE228:
	.4byte	.Lframe0
	.4byte	.LFB143
	.4byte	.LFE143-.LFB143
	.byte	0x4
	.4byte	.LCFI170-.LFB143
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE228:
.LSFDE230:
	.4byte	.LEFDE230-.LASFDE230
.LASFDE230:
	.4byte	.Lframe0
	.4byte	.LFB144
	.4byte	.LFE144-.LFB144
	.byte	0x4
	.4byte	.LCFI171-.LFB144
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE230:
.LSFDE232:
	.4byte	.LEFDE232-.LASFDE232
.LASFDE232:
	.4byte	.Lframe0
	.4byte	.LFB145
	.4byte	.LFE145-.LFB145
	.byte	0x4
	.4byte	.LCFI172-.LFB145
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE232:
.LSFDE234:
	.4byte	.LEFDE234-.LASFDE234
.LASFDE234:
	.4byte	.Lframe0
	.4byte	.LFB146
	.4byte	.LFE146-.LFB146
	.byte	0x4
	.4byte	.LCFI173-.LFB146
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE234:
.LSFDE236:
	.4byte	.LEFDE236-.LASFDE236
.LASFDE236:
	.4byte	.Lframe0
	.4byte	.LFB147
	.4byte	.LFE147-.LFB147
	.byte	0x4
	.4byte	.LCFI174-.LFB147
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE236:
.LSFDE238:
	.4byte	.LEFDE238-.LASFDE238
.LASFDE238:
	.4byte	.Lframe0
	.4byte	.LFB148
	.4byte	.LFE148-.LFB148
	.byte	0x4
	.4byte	.LCFI175-.LFB148
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE238:
.LSFDE240:
	.4byte	.LEFDE240-.LASFDE240
.LASFDE240:
	.4byte	.Lframe0
	.4byte	.LFB149
	.4byte	.LFE149-.LFB149
	.byte	0x4
	.4byte	.LCFI176-.LFB149
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE240:
.LSFDE242:
	.4byte	.LEFDE242-.LASFDE242
.LASFDE242:
	.4byte	.Lframe0
	.4byte	.LFB150
	.4byte	.LFE150-.LFB150
	.byte	0x4
	.4byte	.LCFI177-.LFB150
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE242:
.LSFDE244:
	.4byte	.LEFDE244-.LASFDE244
.LASFDE244:
	.4byte	.Lframe0
	.4byte	.LFB151
	.4byte	.LFE151-.LFB151
	.byte	0x4
	.4byte	.LCFI178-.LFB151
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE244:
.LSFDE246:
	.4byte	.LEFDE246-.LASFDE246
.LASFDE246:
	.4byte	.Lframe0
	.4byte	.LFB152
	.4byte	.LFE152-.LFB152
	.byte	0x4
	.4byte	.LCFI179-.LFB152
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE246:
.LSFDE248:
	.4byte	.LEFDE248-.LASFDE248
.LASFDE248:
	.4byte	.Lframe0
	.4byte	.LFB153
	.4byte	.LFE153-.LFB153
	.byte	0x4
	.4byte	.LCFI180-.LFB153
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE248:
.LSFDE250:
	.4byte	.LEFDE250-.LASFDE250
.LASFDE250:
	.4byte	.Lframe0
	.4byte	.LFB154
	.4byte	.LFE154-.LFB154
	.align	2
.LEFDE250:
.LSFDE252:
	.4byte	.LEFDE252-.LASFDE252
.LASFDE252:
	.4byte	.Lframe0
	.4byte	.LFB155
	.4byte	.LFE155-.LFB155
	.align	2
.LEFDE252:
.LSFDE254:
	.4byte	.LEFDE254-.LASFDE254
.LASFDE254:
	.4byte	.Lframe0
	.4byte	.LFB156
	.4byte	.LFE156-.LFB156
	.byte	0x4
	.4byte	.LCFI181-.LFB156
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE254:
.LSFDE256:
	.4byte	.LEFDE256-.LASFDE256
.LASFDE256:
	.4byte	.Lframe0
	.4byte	.LFB157
	.4byte	.LFE157-.LFB157
	.byte	0x4
	.4byte	.LCFI182-.LFB157
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE256:
.LSFDE258:
	.4byte	.LEFDE258-.LASFDE258
.LASFDE258:
	.4byte	.Lframe0
	.4byte	.LFB158
	.4byte	.LFE158-.LFB158
	.byte	0x4
	.4byte	.LCFI183-.LFB158
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE258:
.LSFDE260:
	.4byte	.LEFDE260-.LASFDE260
.LASFDE260:
	.4byte	.Lframe0
	.4byte	.LFB159
	.4byte	.LFE159-.LFB159
	.byte	0x4
	.4byte	.LCFI184-.LFB159
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE260:
.LSFDE262:
	.4byte	.LEFDE262-.LASFDE262
.LASFDE262:
	.4byte	.Lframe0
	.4byte	.LFB160
	.4byte	.LFE160-.LFB160
	.byte	0x4
	.4byte	.LCFI185-.LFB160
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE262:
.LSFDE264:
	.4byte	.LEFDE264-.LASFDE264
.LASFDE264:
	.4byte	.Lframe0
	.4byte	.LFB162
	.4byte	.LFE162-.LFB162
	.align	2
.LEFDE264:
.LSFDE266:
	.4byte	.LEFDE266-.LASFDE266
.LASFDE266:
	.4byte	.Lframe0
	.4byte	.LFB161
	.4byte	.LFE161-.LFB161
	.byte	0x4
	.4byte	.LCFI186-.LFB161
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE266:
.LSFDE268:
	.4byte	.LEFDE268-.LASFDE268
.LASFDE268:
	.4byte	.Lframe0
	.4byte	.LFB121
	.4byte	.LFE121-.LFB121
	.byte	0x4
	.4byte	.LCFI187-.LFB121
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI188-.LCFI187
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE268:
.LSFDE270:
	.4byte	.LEFDE270-.LASFDE270
.LASFDE270:
	.4byte	.Lframe0
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.byte	0x4
	.4byte	.LCFI189-.LFB46
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI190-.LCFI189
	.byte	0xe
	.uleb128 0x54
	.align	2
.LEFDE270:
.LSFDE272:
	.4byte	.LEFDE272-.LASFDE272
.LASFDE272:
	.4byte	.Lframe0
	.4byte	.LFB100
	.4byte	.LFE100-.LFB100
	.byte	0x4
	.4byte	.LCFI191-.LFB100
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE272:
.LSFDE274:
	.4byte	.LEFDE274-.LASFDE274
.LASFDE274:
	.4byte	.Lframe0
	.4byte	.LFB101
	.4byte	.LFE101-.LFB101
	.byte	0x4
	.4byte	.LCFI192-.LFB101
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE274:
.LSFDE276:
	.4byte	.LEFDE276-.LASFDE276
.LASFDE276:
	.4byte	.Lframe0
	.4byte	.LFB98
	.4byte	.LFE98-.LFB98
	.byte	0x4
	.4byte	.LCFI193-.LFB98
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE276:
.LSFDE278:
	.4byte	.LEFDE278-.LASFDE278
.LASFDE278:
	.4byte	.Lframe0
	.4byte	.LFB99
	.4byte	.LFE99-.LFB99
	.byte	0x4
	.4byte	.LCFI194-.LFB99
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE278:
.LSFDE280:
	.4byte	.LEFDE280-.LASFDE280
.LASFDE280:
	.4byte	.Lframe0
	.4byte	.LFB96
	.4byte	.LFE96-.LFB96
	.byte	0x4
	.4byte	.LCFI195-.LFB96
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE280:
.LSFDE282:
	.4byte	.LEFDE282-.LASFDE282
.LASFDE282:
	.4byte	.Lframe0
	.4byte	.LFB97
	.4byte	.LFE97-.LFB97
	.byte	0x4
	.4byte	.LCFI196-.LFB97
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE282:
.LSFDE284:
	.4byte	.LEFDE284-.LASFDE284
.LASFDE284:
	.4byte	.Lframe0
	.4byte	.LFB94
	.4byte	.LFE94-.LFB94
	.byte	0x4
	.4byte	.LCFI197-.LFB94
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE284:
.LSFDE286:
	.4byte	.LEFDE286-.LASFDE286
.LASFDE286:
	.4byte	.Lframe0
	.4byte	.LFB95
	.4byte	.LFE95-.LFB95
	.byte	0x4
	.4byte	.LCFI198-.LFB95
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE286:
.LSFDE288:
	.4byte	.LEFDE288-.LASFDE288
.LASFDE288:
	.4byte	.Lframe0
	.4byte	.LFB92
	.4byte	.LFE92-.LFB92
	.byte	0x4
	.4byte	.LCFI199-.LFB92
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE288:
.LSFDE290:
	.4byte	.LEFDE290-.LASFDE290
.LASFDE290:
	.4byte	.Lframe0
	.4byte	.LFB93
	.4byte	.LFE93-.LFB93
	.byte	0x4
	.4byte	.LCFI200-.LFB93
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE290:
.LSFDE292:
	.4byte	.LEFDE292-.LASFDE292
.LASFDE292:
	.4byte	.Lframe0
	.4byte	.LFB90
	.4byte	.LFE90-.LFB90
	.byte	0x4
	.4byte	.LCFI201-.LFB90
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x83
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE292:
.LSFDE294:
	.4byte	.LEFDE294-.LASFDE294
.LASFDE294:
	.4byte	.Lframe0
	.4byte	.LFB91
	.4byte	.LFE91-.LFB91
	.byte	0x4
	.4byte	.LCFI202-.LFB91
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE294:
.LSFDE296:
	.4byte	.LEFDE296-.LASFDE296
.LASFDE296:
	.4byte	.Lframe0
	.4byte	.LFB88
	.4byte	.LFE88-.LFB88
	.byte	0x4
	.4byte	.LCFI203-.LFB88
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x83
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE296:
.LSFDE298:
	.4byte	.LEFDE298-.LASFDE298
.LASFDE298:
	.4byte	.Lframe0
	.4byte	.LFB89
	.4byte	.LFE89-.LFB89
	.byte	0x4
	.4byte	.LCFI204-.LFB89
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE298:
.LSFDE300:
	.4byte	.LEFDE300-.LASFDE300
.LASFDE300:
	.4byte	.Lframe0
	.4byte	.LFB86
	.4byte	.LFE86-.LFB86
	.byte	0x4
	.4byte	.LCFI205-.LFB86
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x83
	.uleb128 0x8
	.byte	0x82
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE300:
.LSFDE302:
	.4byte	.LEFDE302-.LASFDE302
.LASFDE302:
	.4byte	.Lframe0
	.4byte	.LFB87
	.4byte	.LFE87-.LFB87
	.byte	0x4
	.4byte	.LCFI206-.LFB87
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE302:
.LSFDE304:
	.4byte	.LEFDE304-.LASFDE304
.LASFDE304:
	.4byte	.Lframe0
	.4byte	.LFB85
	.4byte	.LFE85-.LFB85
	.byte	0x4
	.4byte	.LCFI207-.LFB85
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE304:
.LSFDE306:
	.4byte	.LEFDE306-.LASFDE306
.LASFDE306:
	.4byte	.Lframe0
	.4byte	.LFB83
	.4byte	.LFE83-.LFB83
	.byte	0x4
	.4byte	.LCFI208-.LFB83
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI209-.LCFI208
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE306:
.LSFDE308:
	.4byte	.LEFDE308-.LASFDE308
.LASFDE308:
	.4byte	.Lframe0
	.4byte	.LFB84
	.4byte	.LFE84-.LFB84
	.byte	0x4
	.4byte	.LCFI210-.LFB84
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE308:
.LSFDE310:
	.4byte	.LEFDE310-.LASFDE310
.LASFDE310:
	.4byte	.Lframe0
	.4byte	.LFB81
	.4byte	.LFE81-.LFB81
	.byte	0x4
	.4byte	.LCFI211-.LFB81
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE310:
.LSFDE312:
	.4byte	.LEFDE312-.LASFDE312
.LASFDE312:
	.4byte	.Lframe0
	.4byte	.LFB82
	.4byte	.LFE82-.LFB82
	.byte	0x4
	.4byte	.LCFI212-.LFB82
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE312:
.LSFDE314:
	.4byte	.LEFDE314-.LASFDE314
.LASFDE314:
	.4byte	.Lframe0
	.4byte	.LFB80
	.4byte	.LFE80-.LFB80
	.byte	0x4
	.4byte	.LCFI213-.LFB80
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI214-.LCFI213
	.byte	0xe
	.uleb128 0x24
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI215-.LCFI214
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE314:
.LSFDE316:
	.4byte	.LEFDE316-.LASFDE316
.LASFDE316:
	.4byte	.Lframe0
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.byte	0x4
	.4byte	.LCFI216-.LFB47
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI217-.LCFI216
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE316:
.LSFDE318:
	.4byte	.LEFDE318-.LASFDE318
.LASFDE318:
	.4byte	.Lframe0
	.4byte	.LFB79
	.4byte	.LFE79-.LFB79
	.byte	0x4
	.4byte	.LCFI218-.LFB79
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI219-.LCFI218
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE318:
.LSFDE320:
	.4byte	.LEFDE320-.LASFDE320
.LASFDE320:
	.4byte	.Lframe0
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.byte	0x4
	.4byte	.LCFI220-.LFB56
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI221-.LCFI220
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE320:
.LSFDE322:
	.4byte	.LEFDE322-.LASFDE322
.LASFDE322:
	.4byte	.Lframe0
	.4byte	.LFB163
	.4byte	.LFE163-.LFB163
	.byte	0x4
	.4byte	.LCFI222-.LFB163
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE322:
.LSFDE324:
	.4byte	.LEFDE324-.LASFDE324
.LASFDE324:
	.4byte	.Lframe0
	.4byte	.LFB164
	.4byte	.LFE164-.LFB164
	.byte	0x4
	.4byte	.LCFI223-.LFB164
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE324:
.LSFDE326:
	.4byte	.LEFDE326-.LASFDE326
.LASFDE326:
	.4byte	.Lframe0
	.4byte	.LFB165
	.4byte	.LFE165-.LFB165
	.byte	0x4
	.4byte	.LCFI224-.LFB165
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE326:
.LSFDE328:
	.4byte	.LEFDE328-.LASFDE328
.LASFDE328:
	.4byte	.Lframe0
	.4byte	.LFB166
	.4byte	.LFE166-.LFB166
	.byte	0x4
	.4byte	.LCFI225-.LFB166
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE328:
.LSFDE330:
	.4byte	.LEFDE330-.LASFDE330
.LASFDE330:
	.4byte	.Lframe0
	.4byte	.LFB167
	.4byte	.LFE167-.LFB167
	.byte	0x4
	.4byte	.LCFI226-.LFB167
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE330:
.LSFDE332:
	.4byte	.LEFDE332-.LASFDE332
.LASFDE332:
	.4byte	.Lframe0
	.4byte	.LFB168
	.4byte	.LFE168-.LFB168
	.byte	0x4
	.4byte	.LCFI227-.LFB168
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE332:
.LSFDE334:
	.4byte	.LEFDE334-.LASFDE334
.LASFDE334:
	.4byte	.Lframe0
	.4byte	.LFB169
	.4byte	.LFE169-.LFB169
	.byte	0x4
	.4byte	.LCFI228-.LFB169
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE334:
.LSFDE336:
	.4byte	.LEFDE336-.LASFDE336
.LASFDE336:
	.4byte	.Lframe0
	.4byte	.LFB170
	.4byte	.LFE170-.LFB170
	.byte	0x4
	.4byte	.LCFI229-.LFB170
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE336:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/shared_station_groups.c"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/station_groups.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xe1c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF170
	.byte	0x1
	.4byte	.LASF171
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x871
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x28d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x526
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x417
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x80e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x2
	.2byte	0x117b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x241
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF172
	.byte	0x2
	.2byte	0x1843
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x750
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x72a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x704
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x6de
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x6b8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x30f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x776
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x183
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x1a9
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x1cf
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x1f5
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x21b
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x267
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST5
	.uleb128 0x4
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x79c
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST6
	.uleb128 0x4
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x7c2
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST7
	.uleb128 0x4
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x2c3
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x4
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x2e9
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x4
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x3cb
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST10
	.uleb128 0x4
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x3f1
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST11
	.uleb128 0x4
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x58c
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST12
	.uleb128 0x4
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x5b2
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST13
	.uleb128 0x4
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x648
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST14
	.uleb128 0x4
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x66e
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST15
	.uleb128 0x4
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x7e8
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST16
	.uleb128 0x4
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x335
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST17
	.uleb128 0x4
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x35b
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST18
	.uleb128 0x4
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x471
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST19
	.uleb128 0x4
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x4ba
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST20
	.uleb128 0x4
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x4de
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST21
	.uleb128 0x4
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x502
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST22
	.uleb128 0x4
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x568
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST23
	.uleb128 0x4
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x624
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST24
	.uleb128 0x4
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x84d
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LLST25
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x694
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST26
	.uleb128 0x4
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x37f
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST27
	.uleb128 0x4
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x3a5
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST28
	.uleb128 0x4
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x5d8
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST29
	.uleb128 0x4
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x5fe
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST30
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LLST31
	.uleb128 0x6
	.4byte	0x2a
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST32
	.uleb128 0x6
	.4byte	0x33
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST33
	.uleb128 0x6
	.4byte	0x3c
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST34
	.uleb128 0x6
	.4byte	0x45
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST35
	.uleb128 0x6
	.4byte	0xa0
	.4byte	.LFB183
	.4byte	.LFE183
	.4byte	.LLST36
	.uleb128 0x6
	.4byte	0x97
	.4byte	.LFB182
	.4byte	.LFE182
	.4byte	.LLST37
	.uleb128 0x6
	.4byte	0x8e
	.4byte	.LFB181
	.4byte	.LFE181
	.4byte	.LLST38
	.uleb128 0x6
	.4byte	0x85
	.4byte	.LFB180
	.4byte	.LFE180
	.4byte	.LLST39
	.uleb128 0x6
	.4byte	0x7c
	.4byte	.LFB179
	.4byte	.LFE179
	.4byte	.LLST40
	.uleb128 0x6
	.4byte	0x73
	.4byte	.LFB178
	.4byte	.LFE178
	.4byte	.LLST41
	.uleb128 0x6
	.4byte	0x6a
	.4byte	.LFB177
	.4byte	.LFE177
	.4byte	.LLST42
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x44b
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST43
	.uleb128 0x6
	.4byte	0x4e
	.4byte	.LFB176
	.4byte	.LFE176
	.4byte	.LLST44
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x495
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST45
	.uleb128 0x4
	.4byte	.LASF47
	.byte	0x2
	.2byte	0x503
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LLST46
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF48
	.byte	0x2
	.2byte	0x489
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LLST47
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF49
	.byte	0x2
	.2byte	0x495
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LLST48
	.uleb128 0x2
	.4byte	.LASF50
	.byte	0x2
	.2byte	0x4ea
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF51
	.byte	0x2
	.2byte	0x479
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LLST49
	.uleb128 0x4
	.4byte	.LASF52
	.byte	0x2
	.2byte	0x2d9
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LLST50
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF53
	.byte	0x2
	.2byte	0x5c5
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LLST51
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF54
	.byte	0x2
	.2byte	0x5ef
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LLST52
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF55
	.byte	0x2
	.2byte	0xf9b
	.4byte	.LFB103
	.4byte	.LFE103
	.4byte	.LLST53
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF56
	.byte	0x2
	.2byte	0xfb8
	.4byte	.LFB104
	.4byte	.LFE104
	.4byte	.LLST54
	.uleb128 0x4
	.4byte	.LASF57
	.byte	0x2
	.2byte	0xc30
	.4byte	.LFB75
	.4byte	.LFE75
	.4byte	.LLST55
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF58
	.byte	0x2
	.2byte	0xc49
	.4byte	.LFB76
	.4byte	.LFE76
	.4byte	.LLST56
	.uleb128 0x4
	.4byte	.LASF59
	.byte	0x2
	.2byte	0xbf5
	.4byte	.LFB73
	.4byte	.LFE73
	.4byte	.LLST57
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF60
	.byte	0x2
	.2byte	0xc13
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LLST58
	.uleb128 0x4
	.4byte	.LASF61
	.byte	0x2
	.2byte	0xbbd
	.4byte	.LFB71
	.4byte	.LFE71
	.4byte	.LLST59
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF62
	.byte	0x2
	.2byte	0xbde
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LLST60
	.uleb128 0x4
	.4byte	.LASF63
	.byte	0x2
	.2byte	0xb84
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LLST61
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF64
	.byte	0x2
	.2byte	0xba6
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	.LLST62
	.uleb128 0x4
	.4byte	.LASF65
	.byte	0x2
	.2byte	0xb4a
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	.LLST63
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF66
	.byte	0x2
	.2byte	0xb6d
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LLST64
	.uleb128 0x4
	.4byte	.LASF67
	.byte	0x2
	.2byte	0xb11
	.4byte	.LFB65
	.4byte	.LFE65
	.4byte	.LLST65
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF68
	.byte	0x2
	.2byte	0xb33
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LLST66
	.uleb128 0x4
	.4byte	.LASF69
	.byte	0x2
	.2byte	0xad4
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LLST67
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF70
	.byte	0x2
	.2byte	0xafa
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LLST68
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF71
	.byte	0x2
	.2byte	0xa7c
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LLST69
	.uleb128 0x4
	.4byte	.LASF72
	.byte	0x2
	.2byte	0xa2c
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LLST70
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF73
	.byte	0x2
	.2byte	0xa69
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LLST71
	.uleb128 0x4
	.4byte	.LASF74
	.byte	0x2
	.2byte	0x9f4
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LLST72
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF75
	.byte	0x2
	.2byte	0xa15
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LLST73
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF76
	.byte	0x2
	.2byte	0x979
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LLST74
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF77
	.byte	0x2
	.2byte	0xfc6
	.4byte	.LFB105
	.4byte	.LFE105
	.4byte	.LLST75
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF78
	.byte	0x2
	.2byte	0xfd4
	.4byte	.LFB106
	.4byte	.LFE106
	.4byte	.LLST76
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF79
	.byte	0x2
	.2byte	0xfe2
	.4byte	.LFB107
	.4byte	.LFE107
	.4byte	.LLST77
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF80
	.byte	0x2
	.2byte	0xf65
	.4byte	.LFB102
	.4byte	.LFE102
	.4byte	.LLST78
	.uleb128 0x4
	.4byte	.LASF81
	.byte	0x2
	.2byte	0xc60
	.4byte	.LFB77
	.4byte	.LFE77
	.4byte	.LLST79
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF82
	.byte	0x2
	.2byte	0xc87
	.4byte	.LFB78
	.4byte	.LFE78
	.4byte	.LLST80
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF83
	.byte	0x2
	.2byte	0x1009
	.4byte	.LFB108
	.4byte	.LFE108
	.4byte	.LLST81
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF84
	.byte	0x2
	.2byte	0x1020
	.4byte	.LFB109
	.4byte	.LFE109
	.4byte	.LLST82
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF85
	.byte	0x2
	.2byte	0x1037
	.4byte	.LFB110
	.4byte	.LFE110
	.4byte	.LLST83
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF86
	.byte	0x2
	.2byte	0x104e
	.4byte	.LFB111
	.4byte	.LFE111
	.4byte	.LLST84
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF87
	.byte	0x2
	.2byte	0x1065
	.4byte	.LFB112
	.4byte	.LFE112
	.4byte	.LLST85
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF88
	.byte	0x2
	.2byte	0x1080
	.4byte	.LFB113
	.4byte	.LFE113
	.4byte	.LLST86
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF89
	.byte	0x2
	.2byte	0x109b
	.4byte	.LFB114
	.4byte	.LFE114
	.4byte	.LLST87
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF90
	.byte	0x2
	.2byte	0x10c8
	.4byte	.LFB115
	.4byte	.LFE115
	.4byte	.LLST88
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF91
	.byte	0x2
	.2byte	0x10f1
	.4byte	.LFB116
	.4byte	.LFE116
	.4byte	.LLST89
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF92
	.byte	0x2
	.2byte	0x1126
	.4byte	.LFB117
	.4byte	.LFE117
	.4byte	.LLST90
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF93
	.byte	0x2
	.2byte	0x113b
	.4byte	.LFB118
	.4byte	.LFE118
	.4byte	.LLST91
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF94
	.byte	0x2
	.2byte	0x1166
	.4byte	.LFB119
	.4byte	.LFE119
	.4byte	.LLST92
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF95
	.byte	0x2
	.2byte	0x129b
	.4byte	.LFB122
	.4byte	.LFE122
	.4byte	.LLST93
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF96
	.byte	0x2
	.2byte	0x12ae
	.4byte	.LFB123
	.4byte	.LFE123
	.4byte	.LLST94
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF97
	.byte	0x2
	.2byte	0x12c1
	.4byte	.LFB124
	.4byte	.LFE124
	.4byte	.LLST95
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF98
	.byte	0x2
	.2byte	0x12d4
	.4byte	.LFB125
	.4byte	.LFE125
	.4byte	.LLST96
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF99
	.byte	0x2
	.2byte	0x1300
	.4byte	.LFB126
	.4byte	.LFE126
	.4byte	.LLST97
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF100
	.byte	0x2
	.2byte	0x1335
	.4byte	.LFB127
	.4byte	.LFE127
	.4byte	.LLST98
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF101
	.byte	0x2
	.2byte	0x1358
	.4byte	.LFB128
	.4byte	.LFE128
	.4byte	.LLST99
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF102
	.byte	0x2
	.2byte	0x137d
	.4byte	.LFB129
	.4byte	.LFE129
	.4byte	.LLST100
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF103
	.byte	0x2
	.2byte	0x13a8
	.4byte	.LFB130
	.4byte	.LFE130
	.4byte	.LLST101
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF104
	.byte	0x2
	.2byte	0x13e4
	.4byte	.LFB131
	.4byte	.LFE131
	.4byte	.LLST102
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF105
	.byte	0x2
	.2byte	0x1415
	.4byte	.LFB132
	.4byte	.LFE132
	.4byte	.LLST103
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF106
	.byte	0x2
	.2byte	0x1452
	.4byte	.LFB133
	.4byte	.LFE133
	.4byte	.LLST104
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF107
	.byte	0x2
	.2byte	0x148c
	.4byte	.LFB134
	.4byte	.LFE134
	.4byte	.LLST105
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF108
	.byte	0x2
	.2byte	0x14bb
	.4byte	.LFB135
	.4byte	.LFE135
	.4byte	.LLST106
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF109
	.byte	0x2
	.2byte	0x14ea
	.4byte	.LFB136
	.4byte	.LFE136
	.4byte	.LLST107
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF110
	.byte	0x2
	.2byte	0x151d
	.4byte	.LFB137
	.4byte	.LFE137
	.4byte	.LLST108
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF111
	.byte	0x2
	.2byte	0x1550
	.4byte	.LFB138
	.4byte	.LFE138
	.4byte	.LLST109
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF112
	.byte	0x2
	.2byte	0x158a
	.4byte	.LFB139
	.4byte	.LFE139
	.4byte	.LLST110
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF113
	.byte	0x2
	.2byte	0x15bb
	.4byte	.LFB140
	.4byte	.LFE140
	.4byte	.LLST111
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF114
	.byte	0x2
	.2byte	0x1603
	.4byte	.LFB141
	.4byte	.LFE141
	.4byte	.LLST112
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF115
	.byte	0x2
	.2byte	0x1635
	.4byte	.LFB142
	.4byte	.LFE142
	.4byte	.LLST113
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF116
	.byte	0x2
	.2byte	0x166e
	.4byte	.LFB143
	.4byte	.LFE143
	.4byte	.LLST114
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF117
	.byte	0x2
	.2byte	0x16a3
	.4byte	.LFB144
	.4byte	.LFE144
	.4byte	.LLST115
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF118
	.byte	0x2
	.2byte	0x16da
	.4byte	.LFB145
	.4byte	.LFE145
	.4byte	.LLST116
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF119
	.byte	0x2
	.2byte	0x1713
	.4byte	.LFB146
	.4byte	.LFE146
	.4byte	.LLST117
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF120
	.byte	0x2
	.2byte	0x174a
	.4byte	.LFB147
	.4byte	.LFE147
	.4byte	.LLST118
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF121
	.byte	0x2
	.2byte	0x1783
	.4byte	.LFB148
	.4byte	.LFE148
	.4byte	.LLST119
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF122
	.byte	0x2
	.2byte	0x17ab
	.4byte	.LFB149
	.4byte	.LFE149
	.4byte	.LLST120
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF123
	.byte	0x2
	.2byte	0x17d1
	.4byte	.LFB150
	.4byte	.LFE150
	.4byte	.LLST121
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF124
	.byte	0x2
	.2byte	0x17f5
	.4byte	.LFB151
	.4byte	.LFE151
	.4byte	.LLST122
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF125
	.byte	0x2
	.2byte	0x180c
	.4byte	.LFB152
	.4byte	.LFE152
	.4byte	.LLST123
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF126
	.byte	0x2
	.2byte	0x182a
	.4byte	.LFB153
	.4byte	.LFE153
	.4byte	.LLST124
	.uleb128 0x7
	.4byte	0x60
	.4byte	.LFB154
	.4byte	.LFE154
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF127
	.byte	0x2
	.2byte	0x184c
	.4byte	.LFB155
	.4byte	.LFE155
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF128
	.byte	0x2
	.2byte	0x1854
	.4byte	.LFB156
	.4byte	.LFE156
	.4byte	.LLST125
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF129
	.byte	0x2
	.2byte	0x1879
	.4byte	.LFB157
	.4byte	.LFE157
	.4byte	.LLST126
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF130
	.byte	0x2
	.2byte	0x18ba
	.4byte	.LFB158
	.4byte	.LFE158
	.4byte	.LLST127
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF131
	.byte	0x2
	.2byte	0x18e7
	.4byte	.LFB159
	.4byte	.LFE159
	.4byte	.LLST128
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF132
	.byte	0x2
	.2byte	0x192b
	.4byte	.LFB160
	.4byte	.LFE160
	.4byte	.LLST129
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF133
	.byte	0x2
	.2byte	0x196d
	.4byte	.LFB162
	.4byte	.LFE162
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF134
	.byte	0x2
	.2byte	0x194f
	.4byte	.LFB161
	.4byte	.LFE161
	.4byte	.LLST130
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF135
	.byte	0x2
	.2byte	0x119b
	.4byte	.LFB121
	.4byte	.LFE121
	.4byte	.LLST131
	.uleb128 0x4
	.4byte	.LASF136
	.byte	0x1
	.2byte	0x8bc
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LLST132
	.uleb128 0x4
	.4byte	.LASF137
	.byte	0x2
	.2byte	0xf2f
	.4byte	.LFB100
	.4byte	.LFE100
	.4byte	.LLST133
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF138
	.byte	0x2
	.2byte	0xf4e
	.4byte	.LFB101
	.4byte	.LFE101
	.4byte	.LLST134
	.uleb128 0x4
	.4byte	.LASF139
	.byte	0x2
	.2byte	0xefb
	.4byte	.LFB98
	.4byte	.LFE98
	.4byte	.LLST135
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF140
	.byte	0x2
	.2byte	0xf18
	.4byte	.LFB99
	.4byte	.LFE99
	.4byte	.LLST136
	.uleb128 0x4
	.4byte	.LASF141
	.byte	0x2
	.2byte	0xebf
	.4byte	.LFB96
	.4byte	.LFE96
	.4byte	.LLST137
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF142
	.byte	0x2
	.2byte	0xee4
	.4byte	.LFB97
	.4byte	.LFE97
	.4byte	.LLST138
	.uleb128 0x4
	.4byte	.LASF143
	.byte	0x2
	.2byte	0xe8b
	.4byte	.LFB94
	.4byte	.LFE94
	.4byte	.LLST139
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF144
	.byte	0x2
	.2byte	0xea8
	.4byte	.LFB95
	.4byte	.LFE95
	.4byte	.LLST140
	.uleb128 0x4
	.4byte	.LASF145
	.byte	0x2
	.2byte	0xe57
	.4byte	.LFB92
	.4byte	.LFE92
	.4byte	.LLST141
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF146
	.byte	0x2
	.2byte	0xe74
	.4byte	.LFB93
	.4byte	.LFE93
	.4byte	.LLST142
	.uleb128 0x4
	.4byte	.LASF147
	.byte	0x2
	.2byte	0xe21
	.4byte	.LFB90
	.4byte	.LFE90
	.4byte	.LLST143
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF148
	.byte	0x2
	.2byte	0xe40
	.4byte	.LFB91
	.4byte	.LFE91
	.4byte	.LLST144
	.uleb128 0x4
	.4byte	.LASF149
	.byte	0x2
	.2byte	0xdeb
	.4byte	.LFB88
	.4byte	.LFE88
	.4byte	.LLST145
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF150
	.byte	0x2
	.2byte	0xe0a
	.4byte	.LFB89
	.4byte	.LFE89
	.4byte	.LLST146
	.uleb128 0x4
	.4byte	.LASF151
	.byte	0x2
	.2byte	0xdb5
	.4byte	.LFB86
	.4byte	.LFE86
	.4byte	.LLST147
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF152
	.byte	0x2
	.2byte	0xdd4
	.4byte	.LFB87
	.4byte	.LFE87
	.4byte	.LLST148
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF153
	.byte	0x2
	.2byte	0xd88
	.4byte	.LFB85
	.4byte	.LFE85
	.4byte	.LLST149
	.uleb128 0x4
	.4byte	.LASF154
	.byte	0x2
	.2byte	0xd4d
	.4byte	.LFB83
	.4byte	.LFE83
	.4byte	.LLST150
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF155
	.byte	0x2
	.2byte	0xd75
	.4byte	.LFB84
	.4byte	.LFE84
	.4byte	.LLST151
	.uleb128 0x4
	.4byte	.LASF156
	.byte	0x2
	.2byte	0xd19
	.4byte	.LFB81
	.4byte	.LFE81
	.4byte	.LLST152
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF157
	.byte	0x2
	.2byte	0xd36
	.4byte	.LFB82
	.4byte	.LFE82
	.4byte	.LLST153
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF158
	.byte	0x2
	.2byte	0xcaf
	.4byte	.LFB80
	.4byte	.LFE80
	.4byte	.LLST154
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF159
	.byte	0x1
	.2byte	0xad3
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LLST155
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF160
	.byte	0x2
	.2byte	0xc9a
	.4byte	.LFB79
	.4byte	.LFE79
	.4byte	.LLST156
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF161
	.byte	0x2
	.2byte	0x65b
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LLST157
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF162
	.byte	0x2
	.2byte	0x1973
	.4byte	.LFB163
	.4byte	.LFE163
	.4byte	.LLST158
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF163
	.byte	0x2
	.2byte	0x199c
	.4byte	.LFB164
	.4byte	.LFE164
	.4byte	.LLST159
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF164
	.byte	0x2
	.2byte	0x19c4
	.4byte	.LFB165
	.4byte	.LFE165
	.4byte	.LLST160
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF165
	.byte	0x2
	.2byte	0x19e5
	.4byte	.LFB166
	.4byte	.LFE166
	.4byte	.LLST161
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF166
	.byte	0x2
	.2byte	0x1a23
	.4byte	.LFB167
	.4byte	.LFE167
	.4byte	.LLST162
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF167
	.byte	0x2
	.2byte	0x1a55
	.4byte	.LFB168
	.4byte	.LFE168
	.4byte	.LLST163
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF168
	.byte	0x2
	.2byte	0x1a90
	.4byte	.LFB169
	.4byte	.LFE169
	.4byte	.LLST164
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF169
	.byte	0x2
	.2byte	0x1b1e
	.4byte	.LFB170
	.4byte	.LFE170
	.4byte	.LLST165
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI5
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI9
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB6
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI11
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB40
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB41
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI15
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI17
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB15
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI21
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB16
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI23
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB26
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI25
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB27
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI27
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB31
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI29
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB32
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB42
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI33
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB11
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI35
	.4byte	.LFE11
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB12
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI37
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB19
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI39
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB21
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI41
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB22
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI43
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB23
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI45
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB25
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI47
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB30
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI49
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB44
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI51
	.4byte	.LFE44
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB33
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI52
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI53
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB13
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI55
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB14
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI57
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB28
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI58
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI59
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB29
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI61
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB45
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI62
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI63
	.4byte	.LFE45
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB7
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI64
	.4byte	.LCFI65
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI65
	.4byte	.LFE7
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB24
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI66
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI67
	.4byte	.LFE24
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB17
	.4byte	.LCFI68
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI68
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI69
	.4byte	.LFE17
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB43
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI70
	.4byte	.LCFI71
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI71
	.4byte	.LFE43
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB183
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI72
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI73
	.4byte	.LFE183
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB182
	.4byte	.LCFI74
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI74
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI75
	.4byte	.LFE182
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB181
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI76
	.4byte	.LCFI77
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI77
	.4byte	.LFE181
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB180
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI78
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI79
	.4byte	.LFE180
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB179
	.4byte	.LCFI80
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI80
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI81
	.4byte	.LFE179
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB178
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI82
	.4byte	.LCFI83
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI83
	.4byte	.LFE178
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB177
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI84
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI85
	.4byte	.LFE177
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB18
	.4byte	.LCFI86
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI86
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI87
	.4byte	.LFE18
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST44:
	.4byte	.LFB176
	.4byte	.LCFI88
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI88
	.4byte	.LFE176
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST45:
	.4byte	.LFB20
	.4byte	.LCFI89
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI89
	.4byte	.LCFI90
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI90
	.4byte	.LFE20
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST46:
	.4byte	.LFB53
	.4byte	.LCFI91
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI91
	.4byte	.LCFI92
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI92
	.4byte	.LFE53
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST47:
	.4byte	.LFB50
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI93
	.4byte	.LFE50
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST48:
	.4byte	.LFB51
	.4byte	.LCFI94
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI94
	.4byte	.LCFI95
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI95
	.4byte	.LFE51
	.2byte	0x3
	.byte	0x7d
	.sleb128 120
	.4byte	0
	.4byte	0
.LLST49:
	.4byte	.LFB49
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI96
	.4byte	.LCFI97
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI97
	.4byte	.LFE49
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST50:
	.4byte	.LFB48
	.4byte	.LCFI98
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI98
	.4byte	.LCFI99
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI99
	.4byte	.LCFI100
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI100
	.4byte	.LFE48
	.2byte	0x3
	.byte	0x7d
	.sleb128 128
	.4byte	0
	.4byte	0
.LLST51:
	.4byte	.LFB54
	.4byte	.LCFI101
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI101
	.4byte	.LFE54
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST52:
	.4byte	.LFB55
	.4byte	.LCFI102
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI102
	.4byte	.LFE55
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST53:
	.4byte	.LFB103
	.4byte	.LCFI103
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI103
	.4byte	.LFE103
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST54:
	.4byte	.LFB104
	.4byte	.LCFI104
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI104
	.4byte	.LFE104
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST55:
	.4byte	.LFB75
	.4byte	.LCFI105
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI105
	.4byte	.LFE75
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST56:
	.4byte	.LFB76
	.4byte	.LCFI106
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI106
	.4byte	.LFE76
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST57:
	.4byte	.LFB73
	.4byte	.LCFI107
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI107
	.4byte	.LFE73
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST58:
	.4byte	.LFB74
	.4byte	.LCFI108
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI108
	.4byte	.LFE74
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST59:
	.4byte	.LFB71
	.4byte	.LCFI109
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI109
	.4byte	.LFE71
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST60:
	.4byte	.LFB72
	.4byte	.LCFI110
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI110
	.4byte	.LFE72
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST61:
	.4byte	.LFB69
	.4byte	.LCFI111
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI111
	.4byte	.LFE69
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST62:
	.4byte	.LFB70
	.4byte	.LCFI112
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI112
	.4byte	.LFE70
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST63:
	.4byte	.LFB67
	.4byte	.LCFI113
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI113
	.4byte	.LFE67
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST64:
	.4byte	.LFB68
	.4byte	.LCFI114
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI114
	.4byte	.LFE68
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST65:
	.4byte	.LFB65
	.4byte	.LCFI115
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI115
	.4byte	.LFE65
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST66:
	.4byte	.LFB66
	.4byte	.LCFI116
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI116
	.4byte	.LFE66
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST67:
	.4byte	.LFB63
	.4byte	.LCFI117
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI117
	.4byte	.LFE63
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST68:
	.4byte	.LFB64
	.4byte	.LCFI118
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI118
	.4byte	.LFE64
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST69:
	.4byte	.LFB62
	.4byte	.LCFI119
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI119
	.4byte	.LCFI120
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI120
	.4byte	.LFE62
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST70:
	.4byte	.LFB60
	.4byte	.LCFI121
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI121
	.4byte	.LCFI122
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI122
	.4byte	.LFE60
	.2byte	0x3
	.byte	0x7d
	.sleb128 84
	.4byte	0
	.4byte	0
.LLST71:
	.4byte	.LFB61
	.4byte	.LCFI123
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI123
	.4byte	.LFE61
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST72:
	.4byte	.LFB58
	.4byte	.LCFI124
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI124
	.4byte	.LFE58
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST73:
	.4byte	.LFB59
	.4byte	.LCFI125
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI125
	.4byte	.LFE59
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST74:
	.4byte	.LFB57
	.4byte	.LCFI126
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI126
	.4byte	.LFE57
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST75:
	.4byte	.LFB105
	.4byte	.LCFI127
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI127
	.4byte	.LFE105
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST76:
	.4byte	.LFB106
	.4byte	.LCFI128
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI128
	.4byte	.LFE106
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST77:
	.4byte	.LFB107
	.4byte	.LCFI129
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI129
	.4byte	.LFE107
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST78:
	.4byte	.LFB102
	.4byte	.LCFI130
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI130
	.4byte	.LFE102
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST79:
	.4byte	.LFB77
	.4byte	.LCFI131
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI131
	.4byte	.LFE77
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST80:
	.4byte	.LFB78
	.4byte	.LCFI132
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI132
	.4byte	.LFE78
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST81:
	.4byte	.LFB108
	.4byte	.LCFI133
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI133
	.4byte	.LFE108
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST82:
	.4byte	.LFB109
	.4byte	.LCFI134
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI134
	.4byte	.LFE109
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST83:
	.4byte	.LFB110
	.4byte	.LCFI135
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI135
	.4byte	.LFE110
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST84:
	.4byte	.LFB111
	.4byte	.LCFI136
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI136
	.4byte	.LFE111
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST85:
	.4byte	.LFB112
	.4byte	.LCFI137
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI137
	.4byte	.LFE112
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST86:
	.4byte	.LFB113
	.4byte	.LCFI138
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI138
	.4byte	.LFE113
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST87:
	.4byte	.LFB114
	.4byte	.LCFI139
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI139
	.4byte	.LFE114
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST88:
	.4byte	.LFB115
	.4byte	.LCFI140
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI140
	.4byte	.LFE115
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST89:
	.4byte	.LFB116
	.4byte	.LCFI141
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI141
	.4byte	.LFE116
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST90:
	.4byte	.LFB117
	.4byte	.LCFI142
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI142
	.4byte	.LFE117
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST91:
	.4byte	.LFB118
	.4byte	.LCFI143
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI143
	.4byte	.LFE118
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST92:
	.4byte	.LFB119
	.4byte	.LCFI144
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI144
	.4byte	.LFE119
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST93:
	.4byte	.LFB122
	.4byte	.LCFI145
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI145
	.4byte	.LFE122
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST94:
	.4byte	.LFB123
	.4byte	.LCFI146
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI146
	.4byte	.LFE123
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST95:
	.4byte	.LFB124
	.4byte	.LCFI147
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI147
	.4byte	.LFE124
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST96:
	.4byte	.LFB125
	.4byte	.LCFI148
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI148
	.4byte	.LFE125
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST97:
	.4byte	.LFB126
	.4byte	.LCFI149
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI149
	.4byte	.LCFI150
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI150
	.4byte	.LFE126
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST98:
	.4byte	.LFB127
	.4byte	.LCFI151
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI151
	.4byte	.LFE127
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST99:
	.4byte	.LFB128
	.4byte	.LCFI152
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI152
	.4byte	.LFE128
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST100:
	.4byte	.LFB129
	.4byte	.LCFI153
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI153
	.4byte	.LFE129
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST101:
	.4byte	.LFB130
	.4byte	.LCFI154
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI154
	.4byte	.LCFI155
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI155
	.4byte	.LFE130
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST102:
	.4byte	.LFB131
	.4byte	.LCFI156
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI156
	.4byte	.LCFI157
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI157
	.4byte	.LFE131
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST103:
	.4byte	.LFB132
	.4byte	.LCFI158
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI158
	.4byte	.LFE132
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST104:
	.4byte	.LFB133
	.4byte	.LCFI159
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI159
	.4byte	.LFE133
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST105:
	.4byte	.LFB134
	.4byte	.LCFI160
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI160
	.4byte	.LFE134
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST106:
	.4byte	.LFB135
	.4byte	.LCFI161
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI161
	.4byte	.LFE135
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST107:
	.4byte	.LFB136
	.4byte	.LCFI162
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI162
	.4byte	.LFE136
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST108:
	.4byte	.LFB137
	.4byte	.LCFI163
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI163
	.4byte	.LCFI164
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI164
	.4byte	.LFE137
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST109:
	.4byte	.LFB138
	.4byte	.LCFI165
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI165
	.4byte	.LFE138
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST110:
	.4byte	.LFB139
	.4byte	.LCFI166
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI166
	.4byte	.LFE139
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST111:
	.4byte	.LFB140
	.4byte	.LCFI167
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI167
	.4byte	.LFE140
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST112:
	.4byte	.LFB141
	.4byte	.LCFI168
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI168
	.4byte	.LFE141
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST113:
	.4byte	.LFB142
	.4byte	.LCFI169
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI169
	.4byte	.LFE142
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST114:
	.4byte	.LFB143
	.4byte	.LCFI170
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI170
	.4byte	.LFE143
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST115:
	.4byte	.LFB144
	.4byte	.LCFI171
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI171
	.4byte	.LFE144
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST116:
	.4byte	.LFB145
	.4byte	.LCFI172
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI172
	.4byte	.LFE145
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST117:
	.4byte	.LFB146
	.4byte	.LCFI173
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI173
	.4byte	.LFE146
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST118:
	.4byte	.LFB147
	.4byte	.LCFI174
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI174
	.4byte	.LFE147
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST119:
	.4byte	.LFB148
	.4byte	.LCFI175
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI175
	.4byte	.LFE148
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST120:
	.4byte	.LFB149
	.4byte	.LCFI176
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI176
	.4byte	.LFE149
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST121:
	.4byte	.LFB150
	.4byte	.LCFI177
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI177
	.4byte	.LFE150
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST122:
	.4byte	.LFB151
	.4byte	.LCFI178
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI178
	.4byte	.LFE151
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST123:
	.4byte	.LFB152
	.4byte	.LCFI179
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI179
	.4byte	.LFE152
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST124:
	.4byte	.LFB153
	.4byte	.LCFI180
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI180
	.4byte	.LFE153
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST125:
	.4byte	.LFB156
	.4byte	.LCFI181
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI181
	.4byte	.LFE156
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST126:
	.4byte	.LFB157
	.4byte	.LCFI182
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI182
	.4byte	.LFE157
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST127:
	.4byte	.LFB158
	.4byte	.LCFI183
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI183
	.4byte	.LFE158
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST128:
	.4byte	.LFB159
	.4byte	.LCFI184
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI184
	.4byte	.LFE159
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST129:
	.4byte	.LFB160
	.4byte	.LCFI185
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI185
	.4byte	.LFE160
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST130:
	.4byte	.LFB161
	.4byte	.LCFI186
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI186
	.4byte	.LFE161
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST131:
	.4byte	.LFB121
	.4byte	.LCFI187
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI187
	.4byte	.LCFI188
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI188
	.4byte	.LFE121
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST132:
	.4byte	.LFB46
	.4byte	.LCFI189
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI189
	.4byte	.LCFI190
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI190
	.4byte	.LFE46
	.2byte	0x3
	.byte	0x7d
	.sleb128 84
	.4byte	0
	.4byte	0
.LLST133:
	.4byte	.LFB100
	.4byte	.LCFI191
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI191
	.4byte	.LFE100
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST134:
	.4byte	.LFB101
	.4byte	.LCFI192
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI192
	.4byte	.LFE101
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST135:
	.4byte	.LFB98
	.4byte	.LCFI193
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI193
	.4byte	.LFE98
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST136:
	.4byte	.LFB99
	.4byte	.LCFI194
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI194
	.4byte	.LFE99
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST137:
	.4byte	.LFB96
	.4byte	.LCFI195
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI195
	.4byte	.LFE96
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST138:
	.4byte	.LFB97
	.4byte	.LCFI196
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI196
	.4byte	.LFE97
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST139:
	.4byte	.LFB94
	.4byte	.LCFI197
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI197
	.4byte	.LFE94
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST140:
	.4byte	.LFB95
	.4byte	.LCFI198
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI198
	.4byte	.LFE95
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST141:
	.4byte	.LFB92
	.4byte	.LCFI199
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI199
	.4byte	.LFE92
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST142:
	.4byte	.LFB93
	.4byte	.LCFI200
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI200
	.4byte	.LFE93
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST143:
	.4byte	.LFB90
	.4byte	.LCFI201
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI201
	.4byte	.LFE90
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST144:
	.4byte	.LFB91
	.4byte	.LCFI202
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI202
	.4byte	.LFE91
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST145:
	.4byte	.LFB88
	.4byte	.LCFI203
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI203
	.4byte	.LFE88
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST146:
	.4byte	.LFB89
	.4byte	.LCFI204
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI204
	.4byte	.LFE89
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST147:
	.4byte	.LFB86
	.4byte	.LCFI205
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI205
	.4byte	.LFE86
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST148:
	.4byte	.LFB87
	.4byte	.LCFI206
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI206
	.4byte	.LFE87
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST149:
	.4byte	.LFB85
	.4byte	.LCFI207
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI207
	.4byte	.LFE85
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST150:
	.4byte	.LFB83
	.4byte	.LCFI208
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI208
	.4byte	.LCFI209
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI209
	.4byte	.LFE83
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST151:
	.4byte	.LFB84
	.4byte	.LCFI210
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI210
	.4byte	.LFE84
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST152:
	.4byte	.LFB81
	.4byte	.LCFI211
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI211
	.4byte	.LFE81
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST153:
	.4byte	.LFB82
	.4byte	.LCFI212
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI212
	.4byte	.LFE82
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST154:
	.4byte	.LFB80
	.4byte	.LCFI213
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI213
	.4byte	.LCFI214
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI214
	.4byte	.LCFI215
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI215
	.4byte	.LFE80
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST155:
	.4byte	.LFB47
	.4byte	.LCFI216
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI216
	.4byte	.LCFI217
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI217
	.4byte	.LFE47
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST156:
	.4byte	.LFB79
	.4byte	.LCFI218
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI218
	.4byte	.LCFI219
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI219
	.4byte	.LFE79
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST157:
	.4byte	.LFB56
	.4byte	.LCFI220
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI220
	.4byte	.LCFI221
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI221
	.4byte	.LFE56
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST158:
	.4byte	.LFB163
	.4byte	.LCFI222
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI222
	.4byte	.LFE163
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST159:
	.4byte	.LFB164
	.4byte	.LCFI223
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI223
	.4byte	.LFE164
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST160:
	.4byte	.LFB165
	.4byte	.LCFI224
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI224
	.4byte	.LFE165
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST161:
	.4byte	.LFB166
	.4byte	.LCFI225
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI225
	.4byte	.LFE166
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST162:
	.4byte	.LFB167
	.4byte	.LCFI226
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI226
	.4byte	.LFE167
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST163:
	.4byte	.LFB168
	.4byte	.LCFI227
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI227
	.4byte	.LFE168
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST164:
	.4byte	.LFB169
	.4byte	.LCFI228
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI228
	.4byte	.LFE169
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST165:
	.4byte	.LFB170
	.4byte	.LCFI229
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI229
	.4byte	.LFE170
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x55c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB183
	.4byte	.LFE183-.LFB183
	.4byte	.LFB182
	.4byte	.LFE182-.LFB182
	.4byte	.LFB181
	.4byte	.LFE181-.LFB181
	.4byte	.LFB180
	.4byte	.LFE180-.LFB180
	.4byte	.LFB179
	.4byte	.LFE179-.LFB179
	.4byte	.LFB178
	.4byte	.LFE178-.LFB178
	.4byte	.LFB177
	.4byte	.LFE177-.LFB177
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB176
	.4byte	.LFE176-.LFB176
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.4byte	.LFB103
	.4byte	.LFE103-.LFB103
	.4byte	.LFB104
	.4byte	.LFE104-.LFB104
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.4byte	.LFB71
	.4byte	.LFE71-.LFB71
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.4byte	.LFB65
	.4byte	.LFE65-.LFB65
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.4byte	.LFB105
	.4byte	.LFE105-.LFB105
	.4byte	.LFB106
	.4byte	.LFE106-.LFB106
	.4byte	.LFB107
	.4byte	.LFE107-.LFB107
	.4byte	.LFB102
	.4byte	.LFE102-.LFB102
	.4byte	.LFB77
	.4byte	.LFE77-.LFB77
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.4byte	.LFB108
	.4byte	.LFE108-.LFB108
	.4byte	.LFB109
	.4byte	.LFE109-.LFB109
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.4byte	.LFB115
	.4byte	.LFE115-.LFB115
	.4byte	.LFB116
	.4byte	.LFE116-.LFB116
	.4byte	.LFB117
	.4byte	.LFE117-.LFB117
	.4byte	.LFB118
	.4byte	.LFE118-.LFB118
	.4byte	.LFB119
	.4byte	.LFE119-.LFB119
	.4byte	.LFB122
	.4byte	.LFE122-.LFB122
	.4byte	.LFB123
	.4byte	.LFE123-.LFB123
	.4byte	.LFB124
	.4byte	.LFE124-.LFB124
	.4byte	.LFB125
	.4byte	.LFE125-.LFB125
	.4byte	.LFB126
	.4byte	.LFE126-.LFB126
	.4byte	.LFB127
	.4byte	.LFE127-.LFB127
	.4byte	.LFB128
	.4byte	.LFE128-.LFB128
	.4byte	.LFB129
	.4byte	.LFE129-.LFB129
	.4byte	.LFB130
	.4byte	.LFE130-.LFB130
	.4byte	.LFB131
	.4byte	.LFE131-.LFB131
	.4byte	.LFB132
	.4byte	.LFE132-.LFB132
	.4byte	.LFB133
	.4byte	.LFE133-.LFB133
	.4byte	.LFB134
	.4byte	.LFE134-.LFB134
	.4byte	.LFB135
	.4byte	.LFE135-.LFB135
	.4byte	.LFB136
	.4byte	.LFE136-.LFB136
	.4byte	.LFB137
	.4byte	.LFE137-.LFB137
	.4byte	.LFB138
	.4byte	.LFE138-.LFB138
	.4byte	.LFB139
	.4byte	.LFE139-.LFB139
	.4byte	.LFB140
	.4byte	.LFE140-.LFB140
	.4byte	.LFB141
	.4byte	.LFE141-.LFB141
	.4byte	.LFB142
	.4byte	.LFE142-.LFB142
	.4byte	.LFB143
	.4byte	.LFE143-.LFB143
	.4byte	.LFB144
	.4byte	.LFE144-.LFB144
	.4byte	.LFB145
	.4byte	.LFE145-.LFB145
	.4byte	.LFB146
	.4byte	.LFE146-.LFB146
	.4byte	.LFB147
	.4byte	.LFE147-.LFB147
	.4byte	.LFB148
	.4byte	.LFE148-.LFB148
	.4byte	.LFB149
	.4byte	.LFE149-.LFB149
	.4byte	.LFB150
	.4byte	.LFE150-.LFB150
	.4byte	.LFB151
	.4byte	.LFE151-.LFB151
	.4byte	.LFB152
	.4byte	.LFE152-.LFB152
	.4byte	.LFB153
	.4byte	.LFE153-.LFB153
	.4byte	.LFB154
	.4byte	.LFE154-.LFB154
	.4byte	.LFB155
	.4byte	.LFE155-.LFB155
	.4byte	.LFB156
	.4byte	.LFE156-.LFB156
	.4byte	.LFB157
	.4byte	.LFE157-.LFB157
	.4byte	.LFB158
	.4byte	.LFE158-.LFB158
	.4byte	.LFB159
	.4byte	.LFE159-.LFB159
	.4byte	.LFB160
	.4byte	.LFE160-.LFB160
	.4byte	.LFB162
	.4byte	.LFE162-.LFB162
	.4byte	.LFB161
	.4byte	.LFE161-.LFB161
	.4byte	.LFB121
	.4byte	.LFE121-.LFB121
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.4byte	.LFB100
	.4byte	.LFE100-.LFB100
	.4byte	.LFB101
	.4byte	.LFE101-.LFB101
	.4byte	.LFB98
	.4byte	.LFE98-.LFB98
	.4byte	.LFB99
	.4byte	.LFE99-.LFB99
	.4byte	.LFB96
	.4byte	.LFE96-.LFB96
	.4byte	.LFB97
	.4byte	.LFE97-.LFB97
	.4byte	.LFB94
	.4byte	.LFE94-.LFB94
	.4byte	.LFB95
	.4byte	.LFE95-.LFB95
	.4byte	.LFB92
	.4byte	.LFE92-.LFB92
	.4byte	.LFB93
	.4byte	.LFE93-.LFB93
	.4byte	.LFB90
	.4byte	.LFE90-.LFB90
	.4byte	.LFB91
	.4byte	.LFE91-.LFB91
	.4byte	.LFB88
	.4byte	.LFE88-.LFB88
	.4byte	.LFB89
	.4byte	.LFE89-.LFB89
	.4byte	.LFB86
	.4byte	.LFE86-.LFB86
	.4byte	.LFB87
	.4byte	.LFE87-.LFB87
	.4byte	.LFB85
	.4byte	.LFE85-.LFB85
	.4byte	.LFB83
	.4byte	.LFE83-.LFB83
	.4byte	.LFB84
	.4byte	.LFE84-.LFB84
	.4byte	.LFB81
	.4byte	.LFE81-.LFB81
	.4byte	.LFB82
	.4byte	.LFE82-.LFB82
	.4byte	.LFB80
	.4byte	.LFE80-.LFB80
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.4byte	.LFB79
	.4byte	.LFE79-.LFB79
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.4byte	.LFB163
	.4byte	.LFE163-.LFB163
	.4byte	.LFB164
	.4byte	.LFE164-.LFB164
	.4byte	.LFB165
	.4byte	.LFE165-.LFB165
	.4byte	.LFB166
	.4byte	.LFE166-.LFB166
	.4byte	.LFB167
	.4byte	.LFE167-.LFB167
	.4byte	.LFB168
	.4byte	.LFE168-.LFB168
	.4byte	.LFB169
	.4byte	.LFE169-.LFB169
	.4byte	.LFB170
	.4byte	.LFE170-.LFB170
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB183
	.4byte	.LFE183
	.4byte	.LFB182
	.4byte	.LFE182
	.4byte	.LFB181
	.4byte	.LFE181
	.4byte	.LFB180
	.4byte	.LFE180
	.4byte	.LFB179
	.4byte	.LFE179
	.4byte	.LFB178
	.4byte	.LFE178
	.4byte	.LFB177
	.4byte	.LFE177
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB176
	.4byte	.LFE176
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LFB103
	.4byte	.LFE103
	.4byte	.LFB104
	.4byte	.LFE104
	.4byte	.LFB75
	.4byte	.LFE75
	.4byte	.LFB76
	.4byte	.LFE76
	.4byte	.LFB73
	.4byte	.LFE73
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LFB71
	.4byte	.LFE71
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LFB65
	.4byte	.LFE65
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LFB105
	.4byte	.LFE105
	.4byte	.LFB106
	.4byte	.LFE106
	.4byte	.LFB107
	.4byte	.LFE107
	.4byte	.LFB102
	.4byte	.LFE102
	.4byte	.LFB77
	.4byte	.LFE77
	.4byte	.LFB78
	.4byte	.LFE78
	.4byte	.LFB108
	.4byte	.LFE108
	.4byte	.LFB109
	.4byte	.LFE109
	.4byte	.LFB110
	.4byte	.LFE110
	.4byte	.LFB111
	.4byte	.LFE111
	.4byte	.LFB112
	.4byte	.LFE112
	.4byte	.LFB113
	.4byte	.LFE113
	.4byte	.LFB114
	.4byte	.LFE114
	.4byte	.LFB115
	.4byte	.LFE115
	.4byte	.LFB116
	.4byte	.LFE116
	.4byte	.LFB117
	.4byte	.LFE117
	.4byte	.LFB118
	.4byte	.LFE118
	.4byte	.LFB119
	.4byte	.LFE119
	.4byte	.LFB122
	.4byte	.LFE122
	.4byte	.LFB123
	.4byte	.LFE123
	.4byte	.LFB124
	.4byte	.LFE124
	.4byte	.LFB125
	.4byte	.LFE125
	.4byte	.LFB126
	.4byte	.LFE126
	.4byte	.LFB127
	.4byte	.LFE127
	.4byte	.LFB128
	.4byte	.LFE128
	.4byte	.LFB129
	.4byte	.LFE129
	.4byte	.LFB130
	.4byte	.LFE130
	.4byte	.LFB131
	.4byte	.LFE131
	.4byte	.LFB132
	.4byte	.LFE132
	.4byte	.LFB133
	.4byte	.LFE133
	.4byte	.LFB134
	.4byte	.LFE134
	.4byte	.LFB135
	.4byte	.LFE135
	.4byte	.LFB136
	.4byte	.LFE136
	.4byte	.LFB137
	.4byte	.LFE137
	.4byte	.LFB138
	.4byte	.LFE138
	.4byte	.LFB139
	.4byte	.LFE139
	.4byte	.LFB140
	.4byte	.LFE140
	.4byte	.LFB141
	.4byte	.LFE141
	.4byte	.LFB142
	.4byte	.LFE142
	.4byte	.LFB143
	.4byte	.LFE143
	.4byte	.LFB144
	.4byte	.LFE144
	.4byte	.LFB145
	.4byte	.LFE145
	.4byte	.LFB146
	.4byte	.LFE146
	.4byte	.LFB147
	.4byte	.LFE147
	.4byte	.LFB148
	.4byte	.LFE148
	.4byte	.LFB149
	.4byte	.LFE149
	.4byte	.LFB150
	.4byte	.LFE150
	.4byte	.LFB151
	.4byte	.LFE151
	.4byte	.LFB152
	.4byte	.LFE152
	.4byte	.LFB153
	.4byte	.LFE153
	.4byte	.LFB154
	.4byte	.LFE154
	.4byte	.LFB155
	.4byte	.LFE155
	.4byte	.LFB156
	.4byte	.LFE156
	.4byte	.LFB157
	.4byte	.LFE157
	.4byte	.LFB158
	.4byte	.LFE158
	.4byte	.LFB159
	.4byte	.LFE159
	.4byte	.LFB160
	.4byte	.LFE160
	.4byte	.LFB162
	.4byte	.LFE162
	.4byte	.LFB161
	.4byte	.LFE161
	.4byte	.LFB121
	.4byte	.LFE121
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LFB100
	.4byte	.LFE100
	.4byte	.LFB101
	.4byte	.LFE101
	.4byte	.LFB98
	.4byte	.LFE98
	.4byte	.LFB99
	.4byte	.LFE99
	.4byte	.LFB96
	.4byte	.LFE96
	.4byte	.LFB97
	.4byte	.LFE97
	.4byte	.LFB94
	.4byte	.LFE94
	.4byte	.LFB95
	.4byte	.LFE95
	.4byte	.LFB92
	.4byte	.LFE92
	.4byte	.LFB93
	.4byte	.LFE93
	.4byte	.LFB90
	.4byte	.LFE90
	.4byte	.LFB91
	.4byte	.LFE91
	.4byte	.LFB88
	.4byte	.LFE88
	.4byte	.LFB89
	.4byte	.LFE89
	.4byte	.LFB86
	.4byte	.LFE86
	.4byte	.LFB87
	.4byte	.LFE87
	.4byte	.LFB85
	.4byte	.LFE85
	.4byte	.LFB83
	.4byte	.LFE83
	.4byte	.LFB84
	.4byte	.LFE84
	.4byte	.LFB81
	.4byte	.LFE81
	.4byte	.LFB82
	.4byte	.LFE82
	.4byte	.LFB80
	.4byte	.LFE80
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LFB79
	.4byte	.LFE79
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LFB163
	.4byte	.LFE163
	.4byte	.LFB164
	.4byte	.LFE164
	.4byte	.LFB165
	.4byte	.LFE165
	.4byte	.LFB166
	.4byte	.LFE166
	.4byte	.LFB167
	.4byte	.LFE167
	.4byte	.LFB168
	.4byte	.LFE168
	.4byte	.LFB169
	.4byte	.LFE169
	.4byte	.LFB170
	.4byte	.LFE170
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF35:
	.ascii	"nm_DAILY_ET_set_use_ET_averaging\000"
.LASF49:
	.ascii	"STATION_GROUP_copy_details_into_group_name\000"
.LASF170:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF151:
	.ascii	"WEATHER_extract_and_store_individual_group_change\000"
.LASF91:
	.ascii	"SCHEDULE_get_start_time\000"
.LASF108:
	.ascii	"PERCENT_ADJUST_get_station_remaining_days\000"
.LASF64:
	.ascii	"PUMP_copy_group_into_guivars\000"
.LASF5:
	.ascii	"ripple_begin_date_when_irrigating_every_third_day_o"
	.ascii	"r_greater\000"
.LASF146:
	.ascii	"PUMP_extract_and_store_changes_from_GuiVars\000"
.LASF2:
	.ascii	"nm_STATION_GROUP_set_soil_storage_capacity\000"
.LASF154:
	.ascii	"PERCENT_ADJUST_extract_and_store_individual_group_c"
	.ascii	"hange\000"
.LASF116:
	.ascii	"WEATHER_get_station_uses_wind\000"
.LASF132:
	.ascii	"ON_AT_A_TIME_bump_this_groups_ON_count\000"
.LASF76:
	.ascii	"STATION_GROUP_copy_group_into_guivars\000"
.LASF83:
	.ascii	"STATION_GROUP_get_plant_type\000"
.LASF144:
	.ascii	"LINE_FILL_TIME_extract_and_store_changes_from_GuiVa"
	.ascii	"rs\000"
.LASF97:
	.ascii	"RAIN_get_rain_in_use\000"
.LASF47:
	.ascii	"nm_STATION_GROUP_set_default_values\000"
.LASF158:
	.ascii	"STATION_GROUP_extract_and_store_changes_from_GuiVar"
	.ascii	"s\000"
.LASF95:
	.ascii	"SCHEDULE_get_irrigate_after_29th_or_31st\000"
.LASF24:
	.ascii	"nm_SCHEDULE_set_mow_day\000"
.LASF161:
	.ascii	"STATION_GROUP_build_data_to_send\000"
.LASF103:
	.ascii	"STATION_GROUP_get_station_crop_coefficient_100u\000"
.LASF141:
	.ascii	"DELAY_BETWEEN_VALVES_extract_and_store_individual_g"
	.ascii	"roup_change\000"
.LASF26:
	.ascii	"nm_ALERT_ACTIONS_set_high_flow_action\000"
.LASF106:
	.ascii	"PRIORITY_get_station_priority_level\000"
.LASF79:
	.ascii	"STATION_GROUP_get_num_groups_in_use\000"
.LASF68:
	.ascii	"ALERT_ACTIONS_copy_group_into_guivars\000"
.LASF28:
	.ascii	"nm_LINE_FILL_TIME_set_line_fill_time\000"
.LASF137:
	.ascii	"BUDGET_REDUCTION_LIMITS_extract_and_store_individua"
	.ascii	"l_group_change\000"
.LASF85:
	.ascii	"STATION_GROUP_get_soil_type\000"
.LASF45:
	.ascii	"nm_SCHEDULE_set_begin_date\000"
.LASF51:
	.ascii	"init_file_station_group\000"
.LASF126:
	.ascii	"STATION_GROUP_get_budget_reduction_limit_for_this_s"
	.ascii	"tation\000"
.LASF109:
	.ascii	"SCHEDULE_get_station_schedule_type\000"
.LASF123:
	.ascii	"STATION_GROUP_get_GID_irrigation_system_for_this_st"
	.ascii	"ation\000"
.LASF107:
	.ascii	"PERCENT_ADJUST_get_station_percentage_100u\000"
.LASF101:
	.ascii	"STATION_GROUP_get_soak_time_for_this_gid\000"
.LASF133:
	.ascii	"STATION_GROUP_get_change_bits_ptr\000"
.LASF122:
	.ascii	"ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow"
	.ascii	"\000"
.LASF39:
	.ascii	"nm_STATION_GROUP_set_in_use\000"
.LASF33:
	.ascii	"nm_SCHEDULE_set_irrigate_on_29th_or_31st\000"
.LASF156:
	.ascii	"PRIORITY_extract_and_store_individual_group_change\000"
.LASF100:
	.ascii	"STATION_GROUP_get_cycle_time_10u_for_this_gid\000"
.LASF112:
	.ascii	"WEATHER_get_station_uses_daily_et\000"
.LASF31:
	.ascii	"nm_PERCENT_ADJUST_set_end_date\000"
.LASF77:
	.ascii	"STATION_GROUP_get_index_for_group_with_this_GID\000"
.LASF19:
	.ascii	"nm_STATION_GROUP_set_usable_rain\000"
.LASF57:
	.ascii	"ACQUIRE_EXPECTEDS_fill_guivars\000"
.LASF43:
	.ascii	"nm_ON_AT_A_TIME_set_on_at_a_time_in_system\000"
.LASF62:
	.ascii	"LINE_FILL_TIME_copy_group_into_guivars\000"
.LASF75:
	.ascii	"PRIORITY_copy_group_into_guivars\000"
.LASF140:
	.ascii	"ACQUIRE_EXPECTEDS_extract_and_store_changes_from_Gu"
	.ascii	"iVars\000"
.LASF159:
	.ascii	"nm_STATION_GROUP_extract_and_store_changes_from_com"
	.ascii	"m\000"
.LASF115:
	.ascii	"STATION_GROUP_get_soil_storage_capacity_inches_100u"
	.ascii	"\000"
.LASF86:
	.ascii	"STATION_GROUP_get_slope_percentage\000"
.LASF67:
	.ascii	"ALERT_ACTIONS_fill_guivars\000"
.LASF105:
	.ascii	"STATION_GROUP_get_station_usable_rain_percentage_10"
	.ascii	"0u\000"
.LASF55:
	.ascii	"STATION_GROUP_get_group_with_this_GID\000"
.LASF6:
	.ascii	"nm_STATION_GROUP_set_exposure\000"
.LASF15:
	.ascii	"nm_STATION_GROUP_set_head_type\000"
.LASF160:
	.ascii	"STATION_GROUP_extract_and_store_group_name_from_Gui"
	.ascii	"Vars\000"
.LASF37:
	.ascii	"nm_WIND_set_wind_in_use\000"
.LASF44:
	.ascii	"nm_ACQUIRE_EXPECTEDS_set_acquire_expected\000"
.LASF167:
	.ascii	"STATION_GROUPS_skip_irrigation_due_to_a_mow_day\000"
.LASF13:
	.ascii	"nm_STATION_GROUP_set_microclimate_factor\000"
.LASF30:
	.ascii	"nm_STATION_GROUP_set_budget_reduction_percentage_ca"
	.ascii	"p\000"
.LASF38:
	.ascii	"nm_PUMP_set_pump_in_use\000"
.LASF148:
	.ascii	"ON_AT_A_TIME_extract_and_store_changes_from_GuiVars"
	.ascii	"\000"
.LASF1:
	.ascii	"nm_STATION_GROUP_set_crop_coefficient\000"
.LASF34:
	.ascii	"nm_DAILY_ET_set_ET_in_use\000"
.LASF119:
	.ascii	"PUMP_get_station_uses_pump\000"
.LASF11:
	.ascii	"nm_STATION_GROUP_set_allowable_depletion\000"
.LASF65:
	.ascii	"ON_AT_A_TIME_fill_guivars\000"
.LASF87:
	.ascii	"STATION_GROUP_get_precip_rate_in_per_hr\000"
.LASF25:
	.ascii	"nm_SCHEDULE_set_schedule_type\000"
.LASF48:
	.ascii	"save_file_station_group\000"
.LASF118:
	.ascii	"ALERT_ACTIONS_get_station_low_flow_action\000"
.LASF104:
	.ascii	"STATION_GROUP_get_crop_coefficient_100u\000"
.LASF3:
	.ascii	"nm_SCHEDULE_set_water_day\000"
.LASF17:
	.ascii	"nm_STATION_GROUP_set_soil_type\000"
.LASF96:
	.ascii	"DAILY_ET_get_et_in_use\000"
.LASF4:
	.ascii	"nm_STATION_GROUP_set_GID_irrigation_system\000"
.LASF164:
	.ascii	"STATION_GROUP_on_all_groups_set_or_clear_commserver"
	.ascii	"_change_bits\000"
.LASF93:
	.ascii	"SCHEDULE_get_schedule_type\000"
.LASF63:
	.ascii	"PUMP_fill_guivars\000"
.LASF69:
	.ascii	"WEATHER_fill_guivars\000"
.LASF73:
	.ascii	"PERCENT_ADJUST_copy_group_into_guivars\000"
.LASF82:
	.ascii	"BUDGET_REDUCTION_LIMITS_copy_group_into_guivars\000"
.LASF90:
	.ascii	"SCHEDULE_get_enabled\000"
.LASF52:
	.ascii	"nm_station_group_updater\000"
.LASF46:
	.ascii	"nm_SCHEDULE_set_last_ran\000"
.LASF74:
	.ascii	"PRIORITY_fill_guivars\000"
.LASF80:
	.ascii	"nm_STATION_GROUP_load_group_name_into_guivar\000"
.LASF78:
	.ascii	"STATION_GROUP_get_last_group_ID\000"
.LASF131:
	.ascii	"ON_AT_A_TIME_can_another_valve_come_ON_within_this_"
	.ascii	"group\000"
.LASF98:
	.ascii	"PERCENT_ADJUST_get_percentage_100u_for_this_gid\000"
.LASF102:
	.ascii	"STATION_GROUP_get_station_precip_rate_in_per_hr_100"
	.ascii	"000u\000"
.LASF54:
	.ascii	"STATION_GROUP_copy_group\000"
.LASF152:
	.ascii	"WEATHER_extract_and_store_changes_from_GuiVars\000"
.LASF50:
	.ascii	"STATION_GROUP_prepopulate_default_groups\000"
.LASF18:
	.ascii	"nm_STATION_GROUP_set_slope_percentage\000"
.LASF162:
	.ascii	"STATION_GROUP_clean_house_processing\000"
.LASF8:
	.ascii	"nm_STATION_GROUP_set_species_factor\000"
.LASF36:
	.ascii	"nm_RAIN_set_rain_in_use\000"
.LASF121:
	.ascii	"DELAY_BETWEEN_VALVES_get_station_slow_closing_valve"
	.ascii	"_seconds\000"
.LASF110:
	.ascii	"SCHEDULE_get_station_waters_this_day\000"
.LASF171:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/station_groups.c\000"
.LASF128:
	.ascii	"STATION_GROUP_clear_budget_start_time_flags\000"
.LASF134:
	.ascii	"ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_"
	.ascii	"for_GID\000"
.LASF61:
	.ascii	"LINE_FILL_TIME_fill_guivars\000"
.LASF163:
	.ascii	"STATION_GROUP_set_bits_on_all_groups_to_cause_distr"
	.ascii	"ibution_in_the_next_token\000"
.LASF21:
	.ascii	"nm_STATION_GROUP_set_allowable_surface_accumulation"
	.ascii	"\000"
.LASF23:
	.ascii	"nm_PERCENT_ADJUST_set_percent\000"
.LASF138:
	.ascii	"BUDGET_REDUCTION_LIMITS_extract_and_store_changes_f"
	.ascii	"rom_GuiVars\000"
.LASF168:
	.ascii	"STATION_GROUPS_load_ftimes_list\000"
.LASF135:
	.ascii	"SCHEDULE_does_it_irrigate_on_this_date\000"
.LASF53:
	.ascii	"nm_STATION_GROUP_create_new_group\000"
.LASF169:
	.ascii	"STATION_GROUPS_calculate_chain_sync_crc\000"
.LASF165:
	.ascii	"nm_STATION_GROUP_update_pending_change_bits\000"
.LASF40:
	.ascii	"nm_SCHEDULE_set_start_time\000"
.LASF27:
	.ascii	"nm_ALERT_ACTIONS_set_low_flow_action\000"
.LASF88:
	.ascii	"STATION_GROUP_get_allowable_surface_accumulation\000"
.LASF16:
	.ascii	"nm_STATION_GROUP_set_precip_rate\000"
.LASF142:
	.ascii	"DELAY_BETWEEN_VALVES_extract_and_store_changes_from"
	.ascii	"_GuiVars\000"
.LASF7:
	.ascii	"nm_STATION_GROUP_set_density_factor\000"
.LASF70:
	.ascii	"WEATHER_copy_group_into_guivars\000"
.LASF58:
	.ascii	"ACQUIRE_EXPECTEDS_copy_group_into_guivars\000"
.LASF92:
	.ascii	"SCHEDULE_get_stop_time\000"
.LASF136:
	.ascii	"nm_STATION_GROUP_store_changes\000"
.LASF120:
	.ascii	"LINE_FILL_TIME_get_station_line_fill_seconds\000"
.LASF94:
	.ascii	"SCHEDULE_get_mow_day\000"
.LASF60:
	.ascii	"DELAY_BETWEEN_VALVES_copy_group_into_guivars\000"
.LASF157:
	.ascii	"PRIORITY_extract_and_store_changes_from_GuiVars\000"
.LASF10:
	.ascii	"nm_STATION_GROUP_set_available_water\000"
.LASF41:
	.ascii	"nm_SCHEDULE_set_stop_time\000"
.LASF84:
	.ascii	"STATION_GROUP_get_head_type\000"
.LASF72:
	.ascii	"PERCENT_ADJUST_fill_guivars\000"
.LASF99:
	.ascii	"PERCENT_ADJUST_get_remaining_days_for_this_gid\000"
.LASF29:
	.ascii	"nm_DELAY_BETWEEN_VALVES_set_delay_time\000"
.LASF155:
	.ascii	"PERCENT_ADJUST_extract_and_store_changes_from_GuiVa"
	.ascii	"rs\000"
.LASF89:
	.ascii	"STATION_GROUP_get_soil_intake_rate\000"
.LASF150:
	.ascii	"ALERT_ACTIONS_extract_and_store_changes_from_GuiVar"
	.ascii	"s\000"
.LASF113:
	.ascii	"WEATHER_get_station_uses_et_averaging\000"
.LASF22:
	.ascii	"nm_PRIORITY_set_priority\000"
.LASF147:
	.ascii	"ON_AT_A_TIME_extract_and_store_individual_group_cha"
	.ascii	"nge\000"
.LASF149:
	.ascii	"ALERT_ACTIONS_extract_and_store_individual_group_ch"
	.ascii	"ange\000"
.LASF129:
	.ascii	"ON_AT_A_TIME_get_on_at_a_time_within_system_for_thi"
	.ascii	"s_gid\000"
.LASF111:
	.ascii	"SCHEDULE_get_run_time_calculation_support\000"
.LASF145:
	.ascii	"PUMP_extract_and_store_individual_group_change\000"
.LASF59:
	.ascii	"DELAY_BETWEEN_VALVES_fill_guivars\000"
.LASF42:
	.ascii	"nm_ON_AT_A_TIME_set_on_at_a_time_in_group\000"
.LASF81:
	.ascii	"BUDGET_REDUCTION_LIMITS_fill_guivars\000"
.LASF143:
	.ascii	"LINE_FILL_TIME_extract_and_store_individual_group_c"
	.ascii	"hange\000"
.LASF139:
	.ascii	"ACQUIRE_EXPECTEDS_extract_and_store_individual_grou"
	.ascii	"p_change\000"
.LASF71:
	.ascii	"SCHEDULE_copy_group_into_guivars\000"
.LASF14:
	.ascii	"nm_STATION_GROUP_set_plant_type\000"
.LASF172:
	.ascii	"nm_STATION_GROUP_set_budget_start_time_flag\000"
.LASF32:
	.ascii	"nm_SCHEDULE_set_enabled\000"
.LASF12:
	.ascii	"nm_PERCENT_ADJUST_set_start_date\000"
.LASF20:
	.ascii	"nm_STATION_GROUP_set_soil_intake_rate\000"
.LASF0:
	.ascii	"nm_STATION_GROUP_set_moisture_sensor_serial_number\000"
.LASF127:
	.ascii	"nm_STATION_GROUP_get_budget_start_time_flag\000"
.LASF124:
	.ascii	"STATION_GROUP_get_GID_irrigation_system\000"
.LASF117:
	.ascii	"ALERT_ACTIONS_get_station_high_flow_action\000"
.LASF153:
	.ascii	"SCHEDULE_extract_and_store_changes_from_GuiVars\000"
.LASF114:
	.ascii	"WEATHER_get_station_uses_rain\000"
.LASF9:
	.ascii	"nm_STATION_GROUP_set_root_zone_depth\000"
.LASF66:
	.ascii	"ON_AT_A_TIME_copy_group_into_guivars\000"
.LASF166:
	.ascii	"STATION_GROUPS_if_the_moisture_decoder_is_available"
	.ascii	"_return_decoder_serial_number_for_this_station_grou"
	.ascii	"p\000"
.LASF125:
	.ascii	"STATION_GROUP_get_budget_reduction_limit\000"
.LASF130:
	.ascii	"ON_AT_A_TIME_init_all_groups_ON_count\000"
.LASF56:
	.ascii	"STATION_GROUP_get_group_at_this_index\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
