	.file	"e_flowsense.c"
	.text
.Ltext0:
	.section	.text.FDTO_FLOWSENSE_show_controller_letter_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_FLOWSENSE_show_controller_letter_dropdown, %function
FDTO_FLOWSENSE_show_controller_letter_dropdown:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	ldr	r0, .L2+4
	ldr	r1, .L2+8
	ldr	r3, [r3, #0]
	mov	r2, #12
	b	FDTO_COMBOBOX_show
.L3:
	.align	2
.L2:
	.word	GuiVar_FLControllerIndex_0
	.word	725
	.word	FDTO_COMBOBOX_add_items
.LFE2:
	.size	FDTO_FLOWSENSE_show_controller_letter_dropdown, .-FDTO_FLOWSENSE_show_controller_letter_dropdown
	.section	.text.FDTO_FLOWSENSE_show_part_of_chain_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_FLOWSENSE_show_part_of_chain_dropdown, %function
FDTO_FLOWSENSE_show_part_of_chain_dropdown:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L5
	mov	r0, #191
	ldr	r2, [r3, #0]
	mov	r1, #72
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L6:
	.align	2
.L5:
	.word	GuiVar_FLPartOfChain
.LFE0:
	.size	FDTO_FLOWSENSE_show_part_of_chain_dropdown, .-FDTO_FLOWSENSE_show_part_of_chain_dropdown
	.section	.text.FDTO_FLOWSENSE_update_screen,"ax",%progbits
	.align	2
	.global	FDTO_FLOWSENSE_update_screen
	.type	FDTO_FLOWSENSE_update_screen, %function
FDTO_FLOWSENSE_update_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L15
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r6, [r3, #0]
	cmp	r6, #1
	ldmnefd	sp!, {r4, r5, r6, pc}
	ldr	r3, .L15+4
	ldr	r0, .L15+8
	ldrh	r2, [r3, #0]
	ldr	r3, .L15+12
	ldr	r5, .L15+16
	str	r2, [r3, #0]
	ldr	r3, .L15+20
	ldr	r2, .L15+24
	ldr	r1, [r3, #0]
	mov	r4, #66
	str	r1, [r2, #0]
	ldr	r2, [r3, #8]
	ldr	r3, .L15+28
	ldr	r1, [r3, #0]
	cmp	r1, r2
	strne	r2, [r3, #0]
	ldr	r1, .L15+32
	mov	r2, #49
	moveq	r6, #0
	bl	strlcpy
.L11:
	ldr	r3, [r5, #108]
	cmp	r3, #0
	beq	.L10
	ldr	r0, .L15+8
	mov	r1, #49
	ldr	r2, .L15+36
	mov	r3, r4
	bl	sp_strlcat
.L10:
	add	r4, r4, #1
	cmp	r4, #77
	add	r5, r5, #92
	bne	.L11
	cmp	r6, #0
	beq	.L12
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, lr}
	b	FDTO_Redraw_Screen
.L12:
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_Refresh
.L16:
	.align	2
.L15:
	.word	GuiVar_FLPartOfChain
	.word	next_contact
	.word	GuiVar_ChainStatsControllersInChain
	.word	GuiVar_ChainStatsNextContact
	.word	chain
	.word	comm_mngr
	.word	GuiVar_ChainStatsCommMngrMode
	.word	GuiVar_ChainStatsInForced
	.word	.LC0
	.word	.LC1
.LFE3:
	.size	FDTO_FLOWSENSE_update_screen, .-FDTO_FLOWSENSE_update_screen
	.section	.text.FDTO_FLOWSENSE_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_FLOWSENSE_draw_screen
	.type	FDTO_FLOWSENSE_draw_screen, %function
FDTO_FLOWSENSE_draw_screen:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L21
	str	lr, [sp, #-4]!
.LCFI1:
	ldrnesh	r1, [r3, #0]
	bne	.L19
	bl	FLOWSENSE_copy_settings_into_guivars
	mov	r1, #0
.L19:
	mov	r0, #19
	mov	r2, #1
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
	ldr	r3, .L21+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	ldrne	pc, [sp], #4
	ldr	lr, [sp], #4
	b	FDTO_FLOWSENSE_update_screen
.L22:
	.align	2
.L21:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_FLPartOfChain
.LFE4:
	.size	FDTO_FLOWSENSE_draw_screen, .-FDTO_FLOWSENSE_draw_screen
	.section	.text.FLOWSENSE_process_screen,"ax",%progbits
	.align	2
	.global	FLOWSENSE_process_screen
	.type	FLOWSENSE_process_screen, %function
FLOWSENSE_process_screen:
.LFB5:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L53
	ldr	r2, .L53+4
	ldrsh	r3, [r3, #0]
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI2:
	cmp	r3, r2
	sub	sp, sp, #44
.LCFI3:
	mov	r6, r0
	mov	r4, r1
	beq	.L25
	cmp	r3, #740
	ldreq	r1, .L53+8
	bne	.L52
	b	.L50
.L25:
	ldr	r1, .L53+12
.L50:
	bl	COMBO_BOX_key_press
	b	.L23
.L52:
	cmp	r0, #3
	beq	.L29
	bhi	.L33
	cmp	r0, #1
	beq	.L30
	bhi	.L31
	b	.L29
.L33:
	cmp	r0, #80
	beq	.L32
	cmp	r0, #84
	beq	.L32
	cmp	r0, #4
	bne	.L46
	b	.L30
.L31:
	ldr	r3, .L53+16
	ldrsh	r4, [r3, #0]
	cmp	r4, #1
	beq	.L36
	cmp	r4, #3
	beq	.L37
	cmp	r4, #0
	bne	.L47
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L53+20
	b	.L51
.L36:
	bl	good_key_beep
	ldr	r3, .L53+24
	str	r4, [sp, #8]
.L51:
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L23
.L37:
	bl	good_key_beep
	mov	r0, #1
	bl	FLOWSENSE_extract_and_store_changes_from_GuiVars
	b	.L23
.L47:
	bl	bad_key_beep
	b	.L23
.L32:
	ldr	r3, .L53+16
	ldrsh	r7, [r3, #0]
	cmp	r7, #1
	beq	.L40
	cmp	r7, #2
	beq	.L41
	cmp	r7, #0
	bne	.L48
	ldr	r0, .L53+8
	bl	process_bool
	mov	r0, r7
	b	.L49
.L40:
.LBB4:
	ldr	r4, .L53+12
	mov	r2, #0
	mov	r3, #11
	mov	r0, r6
	mov	r1, r4
	ldr	r5, [r4, #0]
	str	r7, [sp, #0]
	str	r2, [sp, #4]
	bl	process_uns32
	ldr	r3, [r4, #0]
	cmp	r5, #1
	cmpeq	r3, #0
	beq	.L43
	cmp	r5, #0
	cmpeq	r3, #1
	bne	.L42
.L43:
	mov	r0, #0
.L49:
	bl	Redraw_Screen
	b	.L42
.L41:
.LBE4:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L53+28
	mov	r0, r6
	mov	r2, r7
	mov	r3, #12
	bl	process_uns32
	b	.L42
.L48:
	bl	bad_key_beep
.L42:
	bl	Refresh_Screen
	b	.L23
.L30:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L23
.L29:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L23
.L46:
	cmp	r0, #67
	bne	.L44
	mov	r0, #0
	bl	FLOWSENSE_extract_and_store_changes_from_GuiVars
	ldr	r3, .L53+32
	mov	r2, #11
	str	r2, [r3, #0]
.L44:
	mov	r0, r6
	mov	r1, r4
	bl	KEY_process_global_keys
.L23:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L54:
	.align	2
.L53:
	.word	GuiLib_CurStructureNdx
	.word	725
	.word	GuiVar_FLPartOfChain
	.word	GuiVar_FLControllerIndex_0
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_FLOWSENSE_show_part_of_chain_dropdown
	.word	FDTO_FLOWSENSE_show_controller_letter_dropdown
	.word	GuiVar_FLNumControllersInChain
	.word	GuiVar_MenuScreenToShow
.LFE5:
	.size	FLOWSENSE_process_screen, .-FLOWSENSE_process_screen
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"A\000"
.LC1:
	.ascii	" %c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI2-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_flowsense.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x86
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.byte	0x32
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.byte	0x44
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x2c
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x4a
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x7e
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xa3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB5
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"FDTO_FLOWSENSE_show_part_of_chain_dropdown\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_flowsense.c\000"
.LASF7:
	.ascii	"FLOWSENSE_process_controller_letter\000"
.LASF0:
	.ascii	"FDTO_FLOWSENSE_show_controller_letter_dropdown\000"
.LASF2:
	.ascii	"FDTO_FLOWSENSE_update_screen\000"
.LASF4:
	.ascii	"FLOWSENSE_process_screen\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"FDTO_FLOWSENSE_draw_screen\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
