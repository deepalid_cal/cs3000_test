	.file	"chain_sync_funcs.c"
	.text
.Ltext0:
	.section	.text.calculate_crc_and_set_if_crc_is_valid.part.0,"ax",%progbits
	.align	2
	.type	calculate_crc_and_set_if_crc_is_valid.part.0, %function
calculate_crc_and_set_if_crc_is_valid.part.0:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L6
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r2, .L6+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #115
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L6+8
	add	r2, r4, #22
	ldr	r3, [r3, r2, asl #2]
	ldr	r5, .L6+12
	cmp	r3, #0
	beq	.L2
	ldr	r0, .L6+16
	ldr	r1, [r5, r4, asl #4]
	bl	Alert_Message_va
.L2:
	add	r5, r5, r4, asl #4
	ldr	r3, [r5, #4]
	ldr	r6, .L6+12
	cmp	r3, #0
	beq	.L3
	mov	r0, r4
	blx	r3
	ldr	r3, .L6+8
	add	r2, r4, #22
	ldr	r1, [r6, r4, asl #4]
	cmp	r0, #0
	str	r0, [r3, r2, asl #2]
	ldrne	r0, .L6+20
	ldreq	r0, .L6+24
	bl	Alert_Message_va
.L3:
	ldr	r3, .L6
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L7:
	.align	2
.L6:
	.word	chain_sync_control_structure_recursive_MUTEX
	.word	.LC0
	.word	cscs
	.word	chain_sync_file_pertinants
	.word	.LC1
	.word	.LC2
	.word	.LC3
.LFE6:
	.size	calculate_crc_and_set_if_crc_is_valid.part.0, .-calculate_crc_and_set_if_crc_is_valid.part.0
	.section	.text.CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response,"ax",%progbits
	.align	2
	.global	CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response
	.type	CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response, %function
CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r5, .L11
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L11+4
	mov	r3, #32
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #20
	bhi	.L9
	ldr	r3, .L11+8
	add	r1, r4, #1
	mov	r2, #0
	add	r4, r4, #22
	str	r2, [r3, r1, asl #2]
	str	r2, [r3, r4, asl #2]
	b	.L10
.L9:
	ldr	r0, .L11+12
	bl	Alert_Message
.L10:
	ldr	r0, [r5, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L12:
	.align	2
.L11:
	.word	chain_sync_control_structure_recursive_MUTEX
	.word	.LC0
	.word	cscs
	.word	.LC4
.LFE0:
	.size	CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response, .-CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response
	.section	.text.CHAIN_SYNC_increment_clean_token_counts,"ax",%progbits
	.align	2
	.global	CHAIN_SYNC_increment_clean_token_counts
	.type	CHAIN_SYNC_increment_clean_token_counts, %function
CHAIN_SYNC_increment_clean_token_counts:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r4, .L17
	mov	r1, #400
	mov	r3, #66
	ldr	r0, [r4, #0]
	ldr	r2, .L17+4
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L17+8
	add	r1, r3, #84
.L15:
	ldr	r2, [r3], #4
	cmp	r2, #15
	addls	r2, r2, #1
	strls	r2, [r3, #-4]
	cmp	r3, r1
	bne	.L15
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L18:
	.align	2
.L17:
	.word	chain_sync_control_structure_recursive_MUTEX
	.word	.LC0
	.word	cscs+4
.LFE1:
	.size	CHAIN_SYNC_increment_clean_token_counts, .-CHAIN_SYNC_increment_clean_token_counts
	.section	.text.CHAIN_SYNC_push_data_onto_block,"ax",%progbits
	.align	2
	.global	CHAIN_SYNC_push_data_onto_block
	.type	CHAIN_SYNC_push_data_onto_block, %function
CHAIN_SYNC_push_data_onto_block:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	subs	r5, r2, #0
	mov	r4, r0
	beq	.L20
	ldr	r0, [r0, #0]
	bl	memcpy
	ldr	r3, [r4, #0]
	add	r3, r3, r5
	str	r3, [r4, #0]
.L20:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.LFE2:
	.size	CHAIN_SYNC_push_data_onto_block, .-CHAIN_SYNC_push_data_onto_block
	.section	.text.CHAIN_SYNC_load_a_chain_sync_crc_into_the_token_response,"ax",%progbits
	.align	2
	.global	CHAIN_SYNC_load_a_chain_sync_crc_into_the_token_response
	.type	CHAIN_SYNC_load_a_chain_sync_crc_into_the_token_response, %function
CHAIN_SYNC_load_a_chain_sync_crc_into_the_token_response:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L30
	stmfd	sp!, {r4, r5, lr}
.LCFI4:
	ldr	r2, .L30+4
	mov	r4, #0
	str	r4, [r0, #0]
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #175
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L30+8
	ldr	r2, .L30+12
	ldr	r0, [r3, #0]
	add	r2, r2, r0, asl #4
	ldr	r2, [r2, #4]
	cmp	r2, r4
	moveq	r4, r2
	beq	.L22
	add	r2, r0, #1
	ldr	r2, [r3, r2, asl #2]
	cmp	r2, #15
	bls	.L22
	add	r2, r0, #22
	ldr	r3, [r3, r2, asl #2]
	cmp	r3, r4
	bne	.L23
.LBB6:
	cmp	r0, #20
	bhi	.L24
	bl	calculate_crc_and_set_if_crc_is_valid.part.0
	b	.L23
.L24:
	ldr	r0, .L30+16
	bl	Alert_Message
.L23:
.LBE6:
	ldr	r4, .L30+8
	ldr	r3, [r4, #0]
	add	r3, r3, #22
	ldr	r3, [r4, r3, asl #2]
	cmp	r3, #0
	moveq	r4, r3
	beq	.L22
	mov	r0, #8
	mov	r1, r5
	ldr	r2, .L30+4
	mov	r3, #204
	bl	mem_oabia
	cmp	r0, #0
	moveq	r4, r0
	beq	.L22
	ldr	r5, [r5, #0]
	mov	r1, r4
	mov	r2, #4
	mov	r0, r5
	bl	memcpy
	ldr	r1, [r4, #0]
	add	r0, r5, #4
	add	r1, r1, #43
	add	r1, r4, r1, asl #2
	mov	r2, #4
	bl	memcpy
	mov	r4, #8
.L22:
	ldr	r3, .L30+8
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	cmp	r2, #20
	str	r2, [r3, #0]
	movhi	r2, #0
	strhi	r2, [r3, #0]
	ldr	r3, .L30
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.L31:
	.align	2
.L30:
	.word	chain_sync_control_structure_recursive_MUTEX
	.word	.LC0
	.word	cscs
	.word	chain_sync_file_pertinants
	.word	.LC5
.LFE4:
	.size	CHAIN_SYNC_load_a_chain_sync_crc_into_the_token_response, .-CHAIN_SYNC_load_a_chain_sync_crc_into_the_token_response
	.section	.text.CHAIN_SYNC_test_deliverd_crc,"ax",%progbits
	.align	2
	.global	CHAIN_SYNC_test_deliverd_crc
	.type	CHAIN_SYNC_test_deliverd_crc, %function
CHAIN_SYNC_test_deliverd_crc:
.LFB5:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #20
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI5:
	mov	r4, r1
	str	r0, [sp, #0]
	mov	r5, r2
	bhi	.L33
	ldr	r6, .L37
	add	r2, r0, #1
	ldr	r2, [r6, r2, asl #2]
	cmp	r2, #15
	bls	.L32
	add	r3, r0, #22
	ldr	r3, [r6, r3, asl #2]
	cmp	r3, #0
	bne	.L35
	bl	calculate_crc_and_set_if_crc_is_valid.part.0
.L35:
	ldr	r3, [sp, #0]
	add	r2, r3, #22
	ldr	r2, [r6, r2, asl #2]
	cmp	r2, #0
	beq	.L32
	ldr	r2, .L37
	add	r1, r3, #43
	ldr	r2, [r2, r1, asl #2]
	cmp	r4, r2
	beq	.L32
	ldr	r4, .L37+4
	ldr	r0, .L37+8
	ldr	r2, [r4, r3, asl #4]
	add	r1, r5, #65
	bl	Alert_Message_va
	ldr	r3, [sp, #0]
	add	r3, r4, r3, asl #4
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L36
	blx	r3
.L36:
	ldr	r3, [sp, #0]
	add	r4, r4, r3, asl #4
	ldr	r3, [r4, #12]
	cmp	r3, #0
	beq	.L32
	ldr	r3, .L37+12
	add	r5, r5, #10
	mov	r2, #1
	str	r2, [r3, r5, asl #2]
	ldr	r2, .L37+16
	ldr	r4, .L37+20
	str	r2, [r3, #4]
	mov	r3, #62
	strh	r3, [r4, #2]	@ movhi
	ldr	r1, .L37+24
	ldr	r2, .L37+28
	mov	r0, #6
	bl	mem_malloc_debug
	add	r1, r4, #2
	mov	r2, #2
	mov	r5, r0
	bl	memcpy
	add	r0, r5, #2
	mov	r1, sp
	mov	r2, #4
	bl	memcpy
	mov	r3, #6
	str	r5, [r4, #4]
	str	r3, [r4, #8]
	b	.L32
.L33:
	ldr	r0, .L37+32
	bl	Alert_Message
.L32:
	ldmfd	sp!, {r3, r4, r5, r6, pc}
.L38:
	.align	2
.L37:
	.word	cscs
	.word	chain_sync_file_pertinants
	.word	.LC6
	.word	comm_mngr
	.word	3333
	.word	next_contact
	.word	.LC0
	.word	319
	.word	.LC7
.LFE5:
	.size	CHAIN_SYNC_test_deliverd_crc, .-CHAIN_SYNC_test_deliverd_crc
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/flas"
	.ascii	"h_storage/chain_sync_funcs.c\000"
.LC1:
	.ascii	"CSCS: crc is valid? %s\000"
.LC2:
	.ascii	"CSCS: calculated crc for %s\000"
.LC3:
	.ascii	"CSCS: failed to calculate crc for %s\000"
.LC4:
	.ascii	"SYNC VARS: file # out of range\000"
.LC5:
	.ascii	"CSCS: ff_name oor\000"
.LC6:
	.ascii	"CHAIN SYNC: controller %c, %s crc error\000"
.LC7:
	.ascii	"CRC Test index error\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI0-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/flash_storage/chain_sync_funcs.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x9b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.byte	0x69
	.byte	0x1
	.uleb128 0x3
	.4byte	0x21
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x16
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x38
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x58
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x9a
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xf9
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB6
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"CHAIN_SYNC_increment_clean_token_counts\000"
.LASF2:
	.ascii	"CHAIN_SYNC_push_data_onto_block\000"
.LASF4:
	.ascii	"CHAIN_SYNC_test_deliverd_crc\000"
.LASF7:
	.ascii	"calculate_crc_and_set_if_crc_is_valid\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/flas"
	.ascii	"h_storage/chain_sync_funcs.c\000"
.LASF3:
	.ascii	"CHAIN_SYNC_load_a_chain_sync_crc_into_the_token_res"
	.ascii	"ponse\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_respo"
	.ascii	"nse\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
