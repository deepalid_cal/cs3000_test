	.file	"cal_td_utils.c"
	.text
.Ltext0:
	.global	__udivsi3
	.section	.text.DMYToDate,"ax",%progbits
	.align	2
	.global	DMYToDate
	.type	DMYToDate, %function
DMYToDate:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r4, .L4
	cmp	r1, #2
	sub	r2, r2, #1888
	subhi	r2, r2, #12
	subhi	r1, r1, #3
	addls	r1, r1, #9
	subls	r2, r2, #13
	mov	r3, #153
	mul	r1, r3, r1
	mul	r4, r2, r4
	add	r0, r0, #58
	add	r4, r0, r4, lsr #2
	add	r0, r1, #2
	mov	r1, #5
	bl	__udivsi3
	add	r0, r4, r0
	ldmfd	sp!, {r4, pc}
.L5:
	.align	2
.L4:
	.word	1461
.LFE0:
	.size	DMYToDate, .-DMYToDate
	.global	__umodsi3
	.section	.text.HMSToTime,"ax",%progbits
	.align	2
	.global	HMSToTime
	.type	HMSToTime, %function
HMSToTime:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	mov	r5, #60
	mul	r5, r1, r5
	mov	r1, #24
	mov	r4, r2
	bl	__umodsi3
	mov	r3, #3600
	ldr	r1, .L7
	mla	r0, r3, r0, r5
	add	r0, r0, r4
	bl	__umodsi3
	ldmfd	sp!, {r4, r5, pc}
.L8:
	.align	2
.L7:
	.word	86400
.LFE2:
	.size	HMSToTime, .-HMSToTime
	.section	.text.DayOfWeek,"ax",%progbits
	.align	2
	.global	DayOfWeek
	.type	DayOfWeek, %function
DayOfWeek:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI2:
	mov	r1, #7
	add	r0, r0, #1
	bl	__umodsi3
	ldr	pc, [sp], #4
.LFE3:
	.size	DayOfWeek, .-DayOfWeek
	.section	.text.DateToDMY,"ax",%progbits
	.align	2
	.global	DateToDMY
	.type	DateToDMY, %function
DateToDMY:
.LFB4:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI3:
	mov	r4, r0
	mov	r5, r1
.LBB4:
	add	r0, r0, #1
	mov	r1, #7
.LBE4:
	mov	r6, r2
	mov	r7, r3
.LBB5:
	bl	__umodsi3
.LBE5:
	ldr	r3, [sp, #20]
	mov	r4, r4, asl #2
	sub	r4, r4, #233
	ldr	r1, .L13
	str	r0, [r3, #0]
	mov	r0, r4
	bl	__udivsi3
	ldr	r1, .L13
	str	r0, [r7, #0]
	mov	r0, r4
	bl	__umodsi3
	mov	r1, #153
	mov	r0, r0, lsr #2
	add	r0, r0, r0, asl #2
	add	r4, r0, #2
	mov	r0, r4
	bl	__udivsi3
	mov	r1, #153
	str	r0, [r6, #0]
	mov	r0, r4
	bl	__umodsi3
	mov	r1, #5
	add	r0, r0, #5
	bl	__udivsi3
	str	r0, [r5, #0]
	ldr	r3, [r6, #0]
	cmp	r3, #9
	subhi	r3, r3, #9
	strhi	r3, [r6, #0]
	ldrhi	r3, [r7, #0]
	addls	r3, r3, #3
	addhi	r3, r3, #1
	strls	r3, [r6, #0]
	strhi	r3, [r7, #0]
	ldr	r3, [r7, #0]
	add	r3, r3, #1888
	add	r3, r3, #12
	str	r3, [r7, #0]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L14:
	.align	2
.L13:
	.word	1461
.LFE4:
	.size	DateToDMY, .-DateToDMY
	.section	.text.IsLeapYear,"ax",%progbits
	.align	2
	.global	IsLeapYear
	.type	IsLeapYear, %function
IsLeapYear:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	tst	r0, #3
	stmfd	sp!, {r4, lr}
.LCFI4:
	mov	r4, r0
	bne	.L16
	mov	r1, #100
	bl	__umodsi3
	cmp	r0, #0
	bne	.L18
.L16:
	mov	r0, r4
	mov	r1, #400
	bl	__umodsi3
	rsbs	r0, r0, #1
	movcc	r0, #0
	ldmfd	sp!, {r4, pc}
.L18:
	mov	r0, #1
	ldmfd	sp!, {r4, pc}
.LFE5:
	.size	IsLeapYear, .-IsLeapYear
	.section	.text.NumberOfDaysInMonth,"ax",%progbits
	.align	2
	.global	NumberOfDaysInMonth
	.type	NumberOfDaysInMonth, %function
NumberOfDaysInMonth:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #11
	str	lr, [sp, #-4]!
.LCFI5:
	bhi	.L23
	mov	r2, #1
	mov	r3, r2, asl r0
	tst	r3, #2640
	movne	r0, #30
	ldrne	pc, [sp], #4
	tst	r3, #4
	beq	.L23
.L21:
	mov	r0, r1
	bl	IsLeapYear
	cmp	r0, #1
	movne	r0, #28
	moveq	r0, #29
	ldr	pc, [sp], #4
.L23:
	mov	r0, #31
	ldr	pc, [sp], #4
.LFE6:
	.size	NumberOfDaysInMonth, .-NumberOfDaysInMonth
	.section	.text.TimeToHMS,"ax",%progbits
	.align	2
	.global	TimeToHMS
	.type	TimeToHMS, %function
TimeToHMS:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI6:
	mov	r5, r1
	mov	r1, #3600
	mov	r6, r2
	mov	r7, r3
	mov	r4, r0
	bl	__udivsi3
	mov	r3, #3600
	mov	r1, #60
	str	r0, [r5, #0]
	mul	r0, r3, r0
	rsb	r4, r0, r4
	mov	r0, r4
	bl	__udivsi3
	mov	r3, #60
	str	r0, [r6, #0]
	mul	r0, r3, r0
	rsb	r4, r0, r4
	str	r4, [r7, #0]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE7:
	.size	TimeToHMS, .-TimeToHMS
	.section	.text.DateAndTimeToDTCS,"ax",%progbits
	.align	2
	.global	DateAndTimeToDTCS
	.type	DateAndTimeToDTCS, %function
DateAndTimeToDTCS:
.LFB1:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	mov	r0, r0, asl #16
	sub	sp, sp, #32
.LCFI8:
	mov	r0, r0, lsr #16
	add	r3, sp, #16
	mov	r4, r2
	strh	r0, [r2, #4]	@ movhi
	mov	r5, r1
	str	r3, [sp, #0]
	add	r1, sp, #4
	add	r2, sp, #8
	add	r3, sp, #12
	bl	DateToDMY
	ldr	r3, [sp, #8]
	str	r5, [r4, #0]
	strh	r3, [r4, #8]	@ movhi
	ldr	r3, [sp, #4]
	mov	r0, r5
	strh	r3, [r4, #6]	@ movhi
	ldr	r3, [sp, #12]
	add	r1, sp, #20
	strh	r3, [r4, #10]	@ movhi
	ldr	r3, [sp, #16]
	add	r2, sp, #24
	strb	r3, [r4, #18]
	add	r3, sp, #28
	bl	TimeToHMS
	ldr	r3, [sp, #20]
	strh	r3, [r4, #12]	@ movhi
	ldr	r3, [sp, #24]
	strh	r3, [r4, #14]	@ movhi
	ldr	r3, [sp, #28]
	strh	r3, [r4, #16]	@ movhi
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, pc}
.LFE1:
	.size	DateAndTimeToDTCS, .-DateAndTimeToDTCS
	.section	.text.TDUTILS_time_to_time_string_with_ampm,"ax",%progbits
	.align	2
	.global	TDUTILS_time_to_time_string_with_ampm
	.type	TDUTILS_time_to_time_string_with_ampm, %function
TDUTILS_time_to_time_string_with_ampm:
.LFB8:
	@ args = 8, pretend = 0, frame = 44
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI9:
	ldr	r4, .L38
	sub	sp, sp, #48
.LCFI10:
	cmp	r3, #1
	cmpeq	r2, r4
	mov	r5, r0
	mov	r7, r1
	ldr	r6, [sp, #72]
	movne	r4, #0
	moveq	r4, #1
	bne	.L28
	mov	r0, #0
	bl	GetOffOnStr
	mov	r1, #32
	ldr	r2, .L38+4
	mov	r3, r0
	add	r0, sp, #4
	bl	snprintf
	b	.L29
.L28:
	mov	r0, r2
	add	r3, sp, #44
	add	r2, sp, #40
	add	r1, sp, #36
	bl	TimeToHMS
	ldr	r3, [sp, #36]
	cmp	r3, #0
	moveq	r2, #12
	moveq	r4, r3
	streq	r2, [sp, #36]
	beq	.L31
	cmp	r3, #12
	beq	.L35
	bls	.L31
	sub	r3, r3, #12
	str	r3, [sp, #36]
.L35:
	mov	r4, #1
.L31:
	ldr	r3, [sp, #40]
	add	r0, sp, #4
	str	r3, [sp, #0]
	mov	r1, #32
	ldr	r3, [sp, #36]
	ldr	r2, .L38+8
	bl	snprintf
	ldr	r3, [sp, #68]
	cmp	r3, #1
	bne	.L32
	add	r0, sp, #4
	mov	r1, #32
	ldr	r2, .L38+12
	ldr	r3, [sp, #44]
	bl	sp_strlcat
.L32:
	cmp	r4, #1
	add	r0, sp, #4
	ldreq	r1, .L38+16
	ldrne	r1, .L38+20
	mov	r2, #32
	bl	strlcat
.L29:
	cmp	r6, #0
	streqb	r6, [r5, #0]
	mov	r0, r5
	add	r1, sp, #4
	mov	r2, r7
	bl	strlcat
	mov	r0, r5
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L39:
	.align	2
.L38:
	.word	86400
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
.LFE8:
	.size	TDUTILS_time_to_time_string_with_ampm, .-TDUTILS_time_to_time_string_with_ampm
	.section	.text.GetDateStr,"ax",%progbits
	.align	2
	.global	GetDateStr
	.type	GetDateStr, %function
GetDateStr:
.LFB9:
	@ args = 4, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI11:
	sub	sp, sp, #20
.LCFI12:
	ldr	r7, [sp, #40]
	mov	r6, r3
	add	r3, sp, #16
	mov	r4, r0
	str	r3, [sp, #0]
	mov	r0, r2
	add	r3, sp, #12
	mov	r5, r1
	add	r2, sp, #8
	add	r1, sp, #4
	bl	DateToDMY
	mov	r3, #0
	cmp	r7, #225
	strb	r3, [r4, #0]
	bne	.L41
	ldr	r0, [sp, #16]
	bl	GetDayShortStr
	b	.L47
.L41:
	cmp	r7, #200
	bne	.L42
	ldr	r0, [sp, #16]
	bl	GetDayLongStr
.L47:
	mov	r3, r0
	mov	r1, r5
	mov	r0, r4
	ldr	r2, .L50
	bl	sp_strlcat
.L42:
	ldr	r3, [sp, #4]
	mov	r0, r4
	str	r3, [sp, #0]
	mov	r1, r5
	ldr	r2, .L50+4
	ldr	r3, [sp, #8]
	bl	sp_strlcat
	cmp	r6, #125
	bne	.L43
	ldr	r3, [sp, #12]
	mov	r0, r4
	cmp	r3, #2000
	subcc	r3, r3, #1984
	subcs	r3, r3, #2000
	subcc	r3, r3, #15
	str	r3, [sp, #12]
	mov	r1, r5
	ldr	r2, .L50+8
	b	.L49
.L43:
	cmp	r6, #100
	bne	.L46
	ldr	r2, .L50+12
	mov	r0, r4
	mov	r1, r5
.L49:
	ldr	r3, [sp, #12]
	bl	sp_strlcat
.L46:
	mov	r0, r4
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L51:
	.align	2
.L50:
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
.LFE9:
	.size	GetDateStr, .-GetDateStr
	.section	.text.DATE_TIME_to_DateTimeStr_32,"ax",%progbits
	.align	2
	.global	DATE_TIME_to_DateTimeStr_32
	.type	DATE_TIME_to_DateTimeStr_32, %function
DATE_TIME_to_DateTimeStr_32:
.LFB10:
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI13:
	sub	sp, sp, #80
.LCFI14:
	mov	r6, r1
	add	r1, sp, #8
	stmia	r1, {r2, r3}
	ldrh	r2, [sp, #12]
	mov	r3, #250
	mov	r4, r0
	str	r3, [sp, #0]
	add	r0, sp, #48
	mov	r1, #32
	mov	r3, #100
	bl	GetDateStr
	mov	r3, #0
	ldr	r2, [sp, #8]
	mov	r1, #32
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r5, r0
	add	r0, sp, #16
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, r0
	add	r0, sp, #16
	bl	ShaveLeftPad
	mov	r1, r6
	ldr	r2, .L53
	mov	r3, r5
	str	r0, [sp, #0]
	mov	r0, r4
	bl	snprintf
	mov	r0, r4
	add	sp, sp, #80
	ldmfd	sp!, {r4, r5, r6, pc}
.L54:
	.align	2
.L53:
	.word	.LC9
.LFE10:
	.size	DATE_TIME_to_DateTimeStr_32, .-DATE_TIME_to_DateTimeStr_32
	.section	.text.DT1_IsBiggerThan_DT2,"ax",%progbits
	.align	2
	.global	DT1_IsBiggerThan_DT2
	.type	DT1_IsBiggerThan_DT2, %function
DT1_IsBiggerThan_DT2:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #4]	@ zero_extendqisi2
	ldrb	r2, [r0, #5]	@ zero_extendqisi2
	ldrb	ip, [r1, #5]	@ zero_extendqisi2
	orr	r2, r3, r2, asl #8
	ldrb	r3, [r1, #4]	@ zero_extendqisi2
	orr	r3, r3, ip, asl #8
	cmp	r2, r3
	movhi	r0, #1
	bxhi	lr
	bne	.L58
	ldrb	r2, [r0, #1]	@ zero_extendqisi2
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r0, #2]	@ zero_extendqisi2
	ldrb	r0, [r0, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	orr	r0, r3, r0, asl #24
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r1, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r1, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	cmp	r0, r3
	movls	r0, #0
	movhi	r0, #1
	bx	lr
.L58:
	mov	r0, #0
	bx	lr
.LFE11:
	.size	DT1_IsBiggerThan_DT2, .-DT1_IsBiggerThan_DT2
	.section	.text.DT1_IsBiggerThanOrEqualTo_DT2,"ax",%progbits
	.align	2
	.global	DT1_IsBiggerThanOrEqualTo_DT2
	.type	DT1_IsBiggerThanOrEqualTo_DT2, %function
DT1_IsBiggerThanOrEqualTo_DT2:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #4]	@ zero_extendqisi2
	ldrb	r2, [r0, #5]	@ zero_extendqisi2
	ldrb	ip, [r1, #5]	@ zero_extendqisi2
	orr	r2, r3, r2, asl #8
	ldrb	r3, [r1, #4]	@ zero_extendqisi2
	orr	r3, r3, ip, asl #8
	cmp	r2, r3
	movhi	r0, #1
	bxhi	lr
	bne	.L62
	ldrb	r2, [r0, #1]	@ zero_extendqisi2
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r0, #2]	@ zero_extendqisi2
	ldrb	r0, [r0, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	orr	r0, r3, r0, asl #24
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r1, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r1, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	cmp	r0, r3
	movcc	r0, #0
	movcs	r0, #1
	bx	lr
.L62:
	mov	r0, #0
	bx	lr
.LFE12:
	.size	DT1_IsBiggerThanOrEqualTo_DT2, .-DT1_IsBiggerThanOrEqualTo_DT2
	.section	.text.DT1_IsEqualTo_DT2,"ax",%progbits
	.align	2
	.global	DT1_IsEqualTo_DT2
	.type	DT1_IsEqualTo_DT2, %function
DT1_IsEqualTo_DT2:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI15:
	ldrb	r3, [r1, #4]	@ zero_extendqisi2
	ldrb	r2, [r0, #4]	@ zero_extendqisi2
	ldrb	r4, [r0, #5]	@ zero_extendqisi2
	ldrb	ip, [r1, #5]	@ zero_extendqisi2
	orr	r2, r2, r4, asl #8
	orr	r3, r3, ip, asl #8
	cmp	r2, r3
	bne	.L65
	ldrb	r2, [r0, #1]	@ zero_extendqisi2
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r0, #2]	@ zero_extendqisi2
	ldrb	r0, [r0, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	orr	r0, r3, r0, asl #24
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r1, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r1, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	rsb	r3, r3, r0
	rsbs	r0, r3, #0
	adc	r0, r0, r3
	ldmfd	sp!, {r4, pc}
.L65:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.LFE13:
	.size	DT1_IsEqualTo_DT2, .-DT1_IsEqualTo_DT2
	.section	.text.TDUTILS_add_seconds_to_passed_DT_ptr,"ax",%progbits
	.align	2
	.global	TDUTILS_add_seconds_to_passed_DT_ptr
	.type	TDUTILS_add_seconds_to_passed_DT_ptr, %function
TDUTILS_add_seconds_to_passed_DT_ptr:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI16:
	mov	r4, r0
	mov	r6, r1
	mov	r0, r1
	ldr	r1, .L68
	bl	__udivsi3
	ldr	r1, .L68
	mov	r5, r0
	mov	r0, r6
	bl	__umodsi3
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r4, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r4, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	rsb	r3, r3, #86016
	add	r3, r3, #384
	cmp	r0, r3
	bcc	.L67
	rsb	r0, r3, r0
	add	r5, r5, #1
	mov	r3, #0
	strb	r3, [r4, #0]
	strb	r3, [r4, #1]
	strb	r3, [r4, #2]
	strb	r3, [r4, #3]
.L67:
	ldrb	r2, [r4, #5]	@ zero_extendqisi2
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	add	r5, r5, r3
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	strb	r5, [r4, #4]
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r4, #2]	@ zero_extendqisi2
	mov	r5, r5, lsr #8
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r4, #3]	@ zero_extendqisi2
	strb	r5, [r4, #5]
	orr	r3, r3, r2, asl #24
	add	r0, r0, r3
	mov	r3, r0, lsr #8
	strb	r0, [r4, #0]
	strb	r3, [r4, #1]
	mov	r3, r0, lsr #16
	mov	r0, r0, lsr #24
	strb	r3, [r4, #2]
	strb	r0, [r4, #3]
	ldmfd	sp!, {r4, r5, r6, pc}
.L69:
	.align	2
.L68:
	.word	86400
.LFE14:
	.size	TDUTILS_add_seconds_to_passed_DT_ptr, .-TDUTILS_add_seconds_to_passed_DT_ptr
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%s\000"
.LC1:
	.ascii	"%2u:%02u\000"
.LC2:
	.ascii	":%02u\000"
.LC3:
	.ascii	"PM\000"
.LC4:
	.ascii	"AM\000"
.LC5:
	.ascii	"%s \000"
.LC6:
	.ascii	"%02d/%02d\000"
.LC7:
	.ascii	"/%02d\000"
.LC8:
	.ascii	"/%04d\000"
.LC9:
	.ascii	"%s %s\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI5-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI6-.LFB7
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI7-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI9-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI11-.LFB9
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI13-.LFB10
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x60
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI15-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI16-.LFB14
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/cal_td_utils.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x154
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF14
	.byte	0x1
	.4byte	.LASF15
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.byte	0x7b
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x23
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x63
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x85
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xa2
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xa9
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST5
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xca
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST6
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0x3d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST7
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.byte	0xd6
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x11b
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x14a
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x159
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x16c
	.4byte	.LFB12
	.4byte	.LFE12
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x17f
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST11
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x1e0
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST12
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB6
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB7
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB1
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI8
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI10
	.4byte	.LFE8
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI12
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI14
	.4byte	.LFE10
	.2byte	0x3
	.byte	0x7d
	.sleb128 96
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB13
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB14
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF8:
	.ascii	"GetDateStr\000"
.LASF12:
	.ascii	"DT1_IsEqualTo_DT2\000"
.LASF7:
	.ascii	"TDUTILS_time_to_time_string_with_ampm\000"
.LASF1:
	.ascii	"HMSToTime\000"
.LASF9:
	.ascii	"DATE_TIME_to_DateTimeStr_32\000"
.LASF5:
	.ascii	"TimeToHMS\000"
.LASF14:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"DMYToDate\000"
.LASF6:
	.ascii	"DateAndTimeToDTCS\000"
.LASF15:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/../commo"
	.ascii	"n_includes/cal_td_utils.c\000"
.LASF16:
	.ascii	"DayOfWeek\000"
.LASF2:
	.ascii	"DateToDMY\000"
.LASF4:
	.ascii	"NumberOfDaysInMonth\000"
.LASF11:
	.ascii	"DT1_IsBiggerThanOrEqualTo_DT2\000"
.LASF10:
	.ascii	"DT1_IsBiggerThan_DT2\000"
.LASF13:
	.ascii	"TDUTILS_add_seconds_to_passed_DT_ptr\000"
.LASF3:
	.ascii	"IsLeapYear\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
