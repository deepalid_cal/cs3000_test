	.file	"lpc32xx_clcdc_driver.c"
	.text
.Ltext0:
	.global	__udivsi3
	.section	.text.lcd_update_clock,"ax",%progbits
	.align	2
	.type	lcd_update_clock, %function
lcd_update_clock:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI0:
	ldr	r5, [r0, #4]
	mov	r0, #1
	mov	r7, r1
	bl	clkpwr_get_clock_rate
	mov	r4, #1
	mov	r6, r0
	b	.L2
.L3:
	add	r4, r4, #1
.L2:
	mov	r0, r6
	mov	r1, r4
	bl	__udivsi3
	cmp	r0, r7
	movls	r0, #0
	movhi	r0, #1
	cmp	r4, #63
	movhi	r0, #0
	cmp	r0, #0
	bne	.L3
	ldr	r2, [r5, #8]
	cmp	r4, #1
	subne	r3, r4, #2
	bicne	r2, r2, #-67108864
	andne	r4, r3, #31
	bicne	r2, r2, #29
	movne	r3, r3, asl #19
	orrne	r4, r4, r2
	andne	r3, r3, #520093696
	orreq	r4, r2, #67108864
	orrne	r4, r4, r3
	str	r4, [r5, #8]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE0:
	.size	lcd_update_clock, .-lcd_update_clock
	.section	.text.lcd_initialize,"ax",%progbits
	.align	2
	.type	lcd_initialize, %function
lcd_initialize:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r5, [r0, #4]
	ldr	r4, [r0, #8]
	ldr	r3, [r5, #24]
	ldrh	r2, [r4, #4]
	bic	r3, r3, #1
	str	r3, [r5, #24]
	ldrb	r3, [r4, #1]	@ zero_extendqisi2
	ldrb	r1, [r4, #2]	@ zero_extendqisi2
	sub	r3, r3, #1
	sub	r1, r1, #1
	and	r3, r3, #255
	mov	r3, r3, asl #16
	mov	r1, r1, asl #24
	orr	r1, r3, r1, lsr #16
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	sub	r3, r3, #1
	orr	r3, r1, r3, asl #24
	mov	r1, r2, lsr #4
	sub	r1, r1, #1
	mov	r1, r1, asl #2
	and	r1, r1, #255
	orr	r3, r3, r1
	str	r3, [r5, #0]
	ldrb	ip, [r4, #7]	@ zero_extendqisi2
	ldrb	r3, [r4, #28]	@ zero_extendqisi2
	ldrb	r1, [r4, #6]	@ zero_extendqisi2
	mov	ip, ip, asl #16
	cmp	r3, #1
	orr	ip, ip, r1, asl #24
	ldrh	r3, [r4, #10]
	ldrb	r1, [r4, #8]	@ zero_extendqisi2
	moveq	r3, r3, lsr #1
	sub	r1, r1, #1
	sub	r3, r3, #1
	mov	r1, r1, asl #26
	orr	r1, ip, r1, lsr #16
	mov	r3, r3, asl #22
	orr	r3, r1, r3, lsr #22
	str	r3, [r5, #4]
	ldrb	r1, [r4, #12]	@ zero_extendqisi2
	ldrb	r3, [r4, #16]	@ zero_extendqisi2
	cmp	r1, #0
	sub	r3, r3, #1
	ldrb	r1, [r4, #13]	@ zero_extendqisi2
	and	r3, r3, #31
	mov	r3, r3, asl #6
	orrne	r3, r3, #16384
	cmp	r1, #0
	ldrb	r1, [r4, #14]	@ zero_extendqisi2
	orrne	r3, r3, #8192
	cmp	r1, #0
	ldrb	r1, [r4, #15]	@ zero_extendqisi2
	orrne	r3, r3, #4096
	cmp	r1, #0
	ldr	r1, [r4, #24]
	orrne	r3, r3, #2048
	cmp	r1, #4
	beq	.L15
	cmp	r1, #5
	beq	.L16
	cmp	r1, #3
	moveq	r2, r2, lsr #2
	b	.L21
.L15:
	mov	r2, r2, lsr #3
	b	.L21
.L16:
	add	r2, r2, r2, asl #1
	mov	r2, r2, asr #3
	sub	r2, r2, #1
	mov	r2, r2, asl #22
	orr	r3, r3, r2, lsr #6
	orr	r3, r3, #134217728
	b	.L17
.L21:
	sub	r2, r2, #1
	mov	r2, r2, asl #22
	orr	r3, r3, r2, lsr #6
.L17:
	ldr	r1, [r4, #20]
	str	r3, [r5, #8]
	bl	lcd_update_clock
	ldr	r2, [r4, #24]
	mov	r3, #0
	cmp	r2, #5
	ldrls	r1, .L22
	str	r3, [r5, #12]
	str	r3, [r5, #28]
	movls	r3, r2, asl #1
	movhi	r3, #268
	mvnhi	r0, #0
	ldrlsh	r3, [r1, r3]
	ldrls	r1, .L22+4
	ldrlssb	r0, [r1, r2]
	ldrb	r2, [r4, #28]	@ zero_extendqisi2
	cmp	r2, #1
	orreq	r3, r3, #128
	str	r3, [r5, #24]
	ldmfd	sp!, {r4, r5, pc}
.L23:
	.align	2
.L22:
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE1:
	.size	lcd_initialize, .-lcd_initialize
	.section	.text.lcd_open,"ax",%progbits
	.align	2
	.global	lcd_open
	.type	lcd_open, %function
lcd_open:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L28
	ldr	r2, .L28+4
	ldr	ip, [r3, #0]
	str	lr, [sp, #-4]!
.LCFI2:
	cmp	r0, r2
	cmpeq	ip, #0
	movne	r0, #0
	moveq	r0, #1
	ldrne	pc, [sp], #4
	mov	r0, #1
	stmia	r3, {r0, r2}
	ldr	r0, [r2, #24]
	cmp	r1, #0
	bic	r0, r0, #1
	str	r1, [r3, #8]
	str	r0, [r2, #24]
	beq	.L26
	mov	r0, r3
	bl	lcd_initialize
.L26:
	ldr	r0, .L28
	ldr	pc, [sp], #4
.L29:
	.align	2
.L28:
	.word	.LANCHOR2
	.word	822345728
.LFE2:
	.size	lcd_open, .-lcd_open
	.section	.text.lcd_close,"ax",%progbits
	.align	2
	.global	lcd_close
	.type	lcd_close, %function
lcd_close:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #0]
	cmp	r3, #1
	ldreq	r2, .L33
	moveq	r3, #0
	ldreq	r2, [r2, #4]
	streq	r3, [r0, #0]
	ldreq	r1, [r2, #24]
	moveq	r0, r3
	biceq	r1, r1, #1
	streq	r1, [r2, #24]
	mvnne	r0, #0
	bx	lr
.L34:
	.align	2
.L33:
	.word	.LANCHOR2
.LFE3:
	.size	lcd_close, .-lcd_close
	.section	.text.lcd_ioctl,"ax",%progbits
	.align	2
	.global	lcd_ioctl
	.type	lcd_ioctl, %function
lcd_ioctl:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #0]
	str	lr, [sp, #-4]!
.LCFI3:
	cmp	r3, #1
	bne	.L84
	ldr	r3, [r0, #4]
	cmp	r1, #21
	ldrls	pc, [pc, r1, asl #2]
	b	.L86
.L58:
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L86
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
	.word	.L48
	.word	.L49
	.word	.L50
	.word	.L51
	.word	.L52
	.word	.L53
	.word	.L54
	.word	.L55
	.word	.L56
	.word	.L57
.L37:
	cmp	r2, #0
	beq	.L84
	str	r2, [r0, #8]
	bl	lcd_initialize
	b	.L85
.L38:
	cmp	r2, #1
	ldr	r2, [r3, #24]
	mvnne	r1, #1
	mulne	r2, r1, r2
	bne	.L90
	b	.L92
.L39:
	cmp	r2, #1
	ldr	r2, [r3, #24]
	bicne	r2, r2, #2048
	bicne	r2, r2, #1
	bne	.L90
	orr	r2, r2, #2048
.L92:
	orr	r2, r2, #1
.L90:
	str	r2, [r3, #24]
	b	.L85
.L40:
	str	r2, [r3, #16]
	b	.L85
.L41:
	str	r2, [r3, #20]
	b	.L85
.L42:
	ldr	r1, [r3, #28]
	and	r2, r2, #30
	orr	r2, r2, r1
	b	.L91
.L43:
	ldr	r1, [r3, #28]
	and	r2, r2, #30
	bic	r2, r1, r2
.L91:
	str	r2, [r3, #28]
	b	.L85
.L44:
	cmp	r2, #3
	bhi	.L84
	ldr	r1, [r3, #28]
	ldr	r0, [r3, #28]
	bic	r0, r0, #8
	str	r0, [r3, #28]
	ldr	r0, .L94
	ldr	ip, [r3, #24]
	ldr	r0, [r0, r2, asl #2]
	bic	ip, ip, #12288
	orr	r2, ip, r0
	str	r2, [r3, #24]
	str	r1, [r3, #28]
	b	.L85
.L45:
	mov	r2, #31
	str	r2, [r3, #40]
	mov	r2, #1
	str	r2, [r3, #3108]
	b	.L85
.L46:
	cmp	r2, #0
	ldrne	r2, [r3, #24]
	bicne	r2, r2, #65536
	bne	.L90
	ldr	r1, [r3, #24]
	mov	r0, r2
	orr	r1, r1, #65536
	str	r1, [r3, #24]
	ldr	pc, [sp], #4
.L47:
	cmp	r2, #1
	ldr	r2, [r3, #24]
	biceq	r2, r2, #256
	orrne	r2, r2, #256
	b	.L90
.L48:
	ldr	r1, [r3, #24]
	sub	r2, r2, #1
	bic	r1, r1, #14
	cmp	r2, #7
	ldrls	pc, [pc, r2, asl #2]
	b	.L63
.L68:
	.word	.L64
	.word	.L65
	.word	.L63
	.word	.L66
	.word	.L63
	.word	.L63
	.word	.L63
	.word	.L67
.L65:
	orr	r1, r1, #2
	b	.L64
.L66:
	orr	r1, r1, #4
	b	.L64
.L67:
	orr	r1, r1, #6
	b	.L64
.L63:
	orr	r1, r1, #12
.L64:
	str	r1, [r3, #24]
	b	.L85
.L49:
	cmp	r2, #0
	ble	.L85
	mov	r1, r2
	bl	lcd_update_clock
	b	.L85
.L50:
	cmp	r2, #9
	ldrls	pc, [pc, r2, asl #2]
	b	.L86
.L78:
	.word	.L69
	.word	.L70
	.word	.L71
	.word	.L72
	.word	.L86
	.word	.L73
	.word	.L85
	.word	.L75
	.word	.L76
	.word	.L77
.L69:
	ldr	r0, [r3, #24]
	and	r0, r0, #1
	ldr	pc, [sp], #4
.L70:
	ldr	r3, [r3, #24]
	tst	r3, #2048
	moveq	r0, #0
	movne	r0, #1
	ldr	pc, [sp], #4
.L71:
	ldr	r0, [r3, #16]
	ldr	pc, [sp], #4
.L72:
	ldr	r0, [r3, #20]
	ldr	pc, [sp], #4
.L73:
	ldr	r3, [r0, #8]
	ldr	r0, [r3, #24]
	ldr	pc, [sp], #4
.L75:
	ldr	r3, [r0, #8]
	ldrh	r0, [r3, #4]
	ldr	pc, [sp], #4
.L76:
	ldr	r3, [r0, #8]
	ldrh	r0, [r3, #10]
	ldr	pc, [sp], #4
.L77:
	ldr	r3, [r3, #24]
	and	r3, r3, #14
	cmp	r3, #12
	ldrls	r2, .L94+4
	ldrlssb	r0, [r2, r3]
	ldrls	pc, [sp], #4
	b	.L93
.L51:
	str	r2, [r3, #3072]
	b	.L85
.L52:
	str	r2, [r3, #3088]
	b	.L85
.L53:
	str	r2, [r3, #3080]
	b	.L85
.L54:
	str	r2, [r3, #3084]
	b	.L85
.L55:
	str	r2, [r3, #3104]
	b	.L85
.L56:
	mov	r1, #0
.L79:
	ldr	ip, [r2], #4
	add	r0, r1, #512
	add	r1, r1, #1
	cmp	r1, #256
	str	ip, [r3, r0, asl #2]
	bne	.L79
	b	.L85
.L57:
	mov	r1, #0
.L80:
	ldr	ip, [r2], #4
	add	r0, r1, #128
	add	r1, r1, #1
	cmp	r1, #128
	str	ip, [r3, r0, asl #2]
	bne	.L80
	b	.L85
.L84:
	mvn	r0, #0
	ldr	pc, [sp], #4
.L85:
	mov	r0, #0
	ldr	pc, [sp], #4
.L86:
	mvn	r0, #6
	ldr	pc, [sp], #4
.L93:
	mvn	r0, #6
	ldr	pc, [sp], #4
.L95:
	.align	2
.L94:
	.word	.LANCHOR3
	.word	.LANCHOR4
.LFE4:
	.size	lcd_ioctl, .-lcd_ioctl
	.section	.text.lcd_read,"ax",%progbits
	.align	2
	.global	lcd_read
	.type	lcd_read, %function
lcd_read:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
.LFE5:
	.size	lcd_read, .-lcd_read
	.section	.text.lcd_write,"ax",%progbits
	.align	2
	.global	lcd_write
	.type	lcd_write, %function
lcd_write:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
.LFE6:
	.size	lcd_write, .-lcd_write
	.section	.text.calsense_lcd_open,"ax",%progbits
	.align	2
	.global	calsense_lcd_open
	.type	calsense_lcd_open, %function
calsense_lcd_open:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L114
	ldr	r3, .L114+4
	ldr	ip, [r2, #0]
	stmfd	sp!, {r4, lr}
.LCFI4:
	cmp	r0, r3
	cmpeq	ip, #0
	movne	r0, #0
	moveq	r0, #1
	ldmnefd	sp!, {r4, pc}
	mov	r0, #1
	stmia	r2, {r0, r3}
	str	r1, [r2, #8]
	ldr	r2, [r3, #24]
	cmp	r1, #0
	bic	r2, r2, #1
	str	r2, [r3, #24]
	beq	.L100
.LBB4:
	mov	r2, #0
	str	r2, [r3, #24]
	ldrb	r0, [r1, #1]	@ zero_extendqisi2
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	sub	r0, r0, #1
	sub	ip, ip, #1
	and	r0, r0, #255
	mov	r0, r0, asl #16
	mov	ip, ip, asl #24
	orr	ip, r0, ip, lsr #16
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldrh	r2, [r1, #4]
	sub	r0, r0, #1
	orr	r0, ip, r0, asl #24
	mov	ip, r2, lsr #4
	sub	ip, ip, #1
	mov	ip, ip, asl #2
	and	ip, ip, #255
	orr	r0, r0, ip
	str	r0, [r3, #0]
	ldrb	ip, [r1, #7]	@ zero_extendqisi2
	ldrb	r3, [r1, #28]	@ zero_extendqisi2
	ldrb	r0, [r1, #6]	@ zero_extendqisi2
	mov	ip, ip, asl #16
	cmp	r3, #1
	orr	ip, ip, r0, asl #24
	ldrh	r3, [r1, #10]
	ldrb	r0, [r1, #8]	@ zero_extendqisi2
	moveq	r3, r3, lsr #1
	sub	r0, r0, #1
	sub	r3, r3, #1
	mov	r0, r0, asl #26
	orr	r0, ip, r0, lsr #16
	mov	r3, r3, asl #22
	orr	r3, r0, r3, lsr #22
	ldr	r0, .L114+4
	str	r3, [r0, #4]
	ldrb	r0, [r1, #12]	@ zero_extendqisi2
	ldrb	r3, [r1, #16]	@ zero_extendqisi2
	cmp	r0, #0
	sub	r3, r3, #1
	ldrb	r0, [r1, #13]	@ zero_extendqisi2
	and	r3, r3, #31
	mov	r3, r3, asl #6
	orrne	r3, r3, #16384
	cmp	r0, #0
	ldrb	r0, [r1, #14]	@ zero_extendqisi2
	orrne	r3, r3, #8192
	cmp	r0, #0
	ldrb	r0, [r1, #15]	@ zero_extendqisi2
	orrne	r3, r3, #4096
	cmp	r0, #0
	ldr	r0, [r1, #24]
	orrne	r3, r3, #2048
	cmp	r0, #4
	beq	.L109
	cmp	r0, #5
	beq	.L110
	cmp	r0, #3
	moveq	r2, r2, lsr #2
	b	.L113
.L109:
	mov	r2, r2, lsr #3
	b	.L113
.L110:
	add	r2, r2, r2, asl #1
	mov	r2, r2, asr #3
	sub	r2, r2, #1
	mov	r2, r2, asl #22
	orr	r3, r3, r2, lsr #6
	orr	r3, r3, #134217728
	b	.L111
.L113:
	sub	r2, r2, #1
	mov	r2, r2, asl #22
	orr	r3, r3, r2, lsr #6
.L111:
	ldr	r4, .L114+4
	ldr	r0, .L114
	str	r3, [r4, #8]
	ldr	r1, [r1, #20]
	bl	lcd_update_clock
	mov	r3, #0
	str	r3, [r4, #12]
	str	r3, [r4, #28]
	mov	r3, #20
	str	r3, [r4, #24]
.L100:
.LBE4:
	ldr	r0, .L114
	ldmfd	sp!, {r4, pc}
.L115:
	.align	2
.L114:
	.word	.LANCHOR2
	.word	822345728
.LFE8:
	.size	calsense_lcd_open, .-calsense_lcd_open
	.section	.rodata.CSWTCH.6,"a",%progbits
	.align	1
	.set	.LANCHOR0,. + 0
	.type	CSWTCH.6, %object
	.size	CSWTCH.6, 12
CSWTCH.6:
	.short	300
	.short	268
	.short	268
	.short	284
	.short	348
	.short	268
	.section	.rodata.CSWTCH.7,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	CSWTCH.7, %object
	.size	CSWTCH.7, 6
CSWTCH.7:
	.byte	0
	.byte	-1
	.byte	-1
	.byte	0
	.byte	0
	.byte	0
	.section	.rodata.CSWTCH.9,"a",%progbits
	.set	.LANCHOR4,. + 0
	.type	CSWTCH.9, %object
	.size	CSWTCH.9, 13
CSWTCH.9:
	.byte	1
	.byte	-7
	.byte	2
	.byte	-7
	.byte	4
	.byte	-7
	.byte	8
	.byte	-7
	.byte	-7
	.byte	-7
	.byte	-7
	.byte	-7
	.byte	16
	.section	.bss.lcdcfg,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	lcdcfg, %object
	.size	lcdcfg, 12
lcdcfg:
	.space	12
	.section	.rodata.lcd_vcmp_sel,"a",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	lcd_vcmp_sel, %object
	.size	lcd_vcmp_sel, 16
lcd_vcmp_sel:
	.word	0
	.word	4096
	.word	8192
	.word	12288
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI4-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_clcdc_driver.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xc8
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x31e
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.byte	0x51
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x8c
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x12f
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x163
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x18c
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x2f8
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x315
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x37d
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST4
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB8
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF7:
	.ascii	"calsense_lcd_open\000"
.LASF10:
	.ascii	"calsense_lcd_initialize\000"
.LASF6:
	.ascii	"lcd_write\000"
.LASF0:
	.ascii	"lcd_update_clock\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"lcd_open\000"
.LASF4:
	.ascii	"lcd_ioctl\000"
.LASF1:
	.ascii	"lcd_initialize\000"
.LASF3:
	.ascii	"lcd_close\000"
.LASF5:
	.ascii	"lcd_read\000"
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_clcdc_driver.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
