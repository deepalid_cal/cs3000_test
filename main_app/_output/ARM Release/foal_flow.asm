	.file	"foal_flow.c"
	.text
.Ltext0:
	.section	.text._allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line,"ax",%progbits
	.align	2
	.type	_allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line, %function
_allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #92]
	cmp	r3, #1
	bne	.L2
.LBB13:
	ldr	r2, .L9
	ldr	r2, [r2, #36]
	ldrh	r2, [r2, #72]
	and	r2, r2, #960
	cmp	r2, #384
	beq	.L3
.L2:
	ldr	r3, [r0, #456]
	rsbs	r3, r3, #1
	movcc	r3, #0
.L3:
.LBE13:
	ldr	r2, [r0, #208]
	ldrb	r0, [r0, #466]	@ zero_extendqisi2
	cmp	r2, #0
	movne	r3, #0
	ands	r0, r0, #4
	movne	r0, r3
	bx	lr
.L10:
	.align	2
.L9:
	.word	foal_irri
.LFE15:
	.size	_allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line, .-_allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line
	.section	.text._nm_nm_for_a_flow_error_make_flow_recording_lines_and_turn_OFF_all_ON,"ax",%progbits
	.align	2
	.type	_nm_nm_for_a_flow_error_make_flow_recording_lines_and_turn_OFF_all_ON, %function
_nm_nm_for_a_flow_error_make_flow_recording_lines_and_turn_OFF_all_ON:
.LFB9:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
	fstmfdd	sp!, {d8}
.LCFI1:
	mov	r8, r2
	sub	sp, sp, #48
.LCFI2:
	strh	r0, [sp, #44]	@ movhi
	ldr	r0, [r1, #100]
	ldr	r2, [r1, #92]
	str	r0, [sp, #32]
	ldr	r0, .L46+4
	mov	r3, #0
	str	r3, [r0, #76]
	add	r0, r0, #36
	mov	r5, r1
	str	r2, [sp, #36]
	bl	nm_ListGetFirst
	b	.L38
.L29:
.LBB14:
	ldr	r3, [r4, #40]
	cmp	r3, r5
	bne	.L13
.LBB15:
	ldrb	r3, [r4, #75]	@ zero_extendqisi2
	orr	r3, r3, #16
	strb	r3, [r4, #75]
	ldrb	r3, [sp, #44]	@ zero_extendqisi2
	ldr	r0, [sp, #36]
	and	r3, r3, #7
	cmp	r3, #2
	ldreq	r2, .L46+8
	ldrne	r2, .L46+12
	ldreq	fp, [r5, r2]
	moveq	r6, #233
	ldrne	fp, [r5, r2]
	movne	r6, #243
	cmp	r0, #1
	beq	.L16
	ldrb	r2, [r4, #73]	@ zero_extendqisi2
	ands	r2, r2, #32
	beq	.L41
.L16:
	cmp	r3, #2
	bne	.L18
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	and	r3, r3, #192
	cmp	r3, #128
	beq	.L31
	cmp	r3, #64
	ldreqb	r3, [r4, #75]	@ zero_extendqisi2
	mov	r6, #232
	orreq	r3, r3, #2
	streqb	r3, [r4, #75]
	b	.L40
.L18:
	ldrb	r2, [r4, #74]	@ zero_extendqisi2
	and	r2, r2, #3
	cmp	r2, #2
	beq	.L33
	cmp	r2, #1
	movne	r6, #242
	bne	.L40
	ldrb	r3, [r4, #75]	@ zero_extendqisi2
	mov	r6, #242
	orr	r3, r3, #2
	strb	r3, [r4, #75]
	b	.L42
.L31:
	mov	r6, #232
	b	.L45
.L33:
	mov	r6, #242
.L45:
	mov	r2, #1
.L41:
	mov	r7, r2
	b	.L17
.L40:
	mov	r2, #1
.L42:
	mov	r7, #0
.L17:
	ldr	r3, [sp, #32]
	cmp	r3, #0
	ldrneh	r3, [r4, #80]
	fldseq	s16, .L46
	str	r2, [sp, #28]
	fmsrne	s14, r3	@ int
	ldr	r3, .L46+16
	fuitosne	s16, s14
	fldsne	s14, [sp, #32]	@ int
	ldr	r3, [r5, r3]
	fuitosne	s15, s14
	fmsr	s14, r3	@ int
	fdivsne	s16, s16, s15
	fuitos	s15, s14
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_UNS16
	fmsr	s14, r8	@ int
	fuitos	s15, s14
	fmuls	s15, s15, s16
	str	r0, [sp, #40]
	fmrs	r0, s15
	bl	__round_UNS16
	ldr	r3, .L46+8
	ldr	r3, [r5, r3]
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fmuls	s15, s15, s16
	mov	sl, r0
	fmrs	r0, s15
	bl	__round_UNS16
	ldr	r3, .L46+12
	ldr	r3, [r5, r3]
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fmuls	s15, s15, s16
	mov	r9, r0
	fmrs	r0, s15
	bl	__round_UNS16
	ldrh	r1, [r4, #72]
	ldr	r2, [sp, #28]
	and	r1, r1, #960
	cmp	r1, #64
	mov	r3, r0
	bne	.L20
	cmp	r2, #1
	bne	.L20
	ldrb	r2, [sp, #44]	@ zero_extendqisi2
	ldr	r1, [r4, #44]
	and	r2, r2, #7
	cmp	r2, #2
	ldr	r2, .L46+20
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #37]	@ zero_extendqisi2
	orreq	r1, r1, #8
	orrne	r1, r1, #4
	strb	r1, [r2, #37]
	ldr	r2, .L46+20
	ldr	r1, [r4, #44]
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #36]	@ zero_extendqisi2
	strh	sl, [r2, #62]	@ movhi
	orr	r1, r1, #1
	strh	r9, [r2, #64]	@ movhi
	strh	r0, [r2, #66]	@ movhi
	strb	r1, [r2, #36]
.L20:
	ldrh	r2, [sp, #44]
	mov	r0, r4
	strh	r2, [sp, #4]	@ movhi
	ldr	r1, [sp, #40]
	mov	r2, r9
	str	sl, [sp, #0]
	bl	nm_flow_recorder_add
	ldr	r0, [sp, #32]
	ldrh	r1, [r4, #72]
	ldrb	r3, [r4, #90]	@ zero_extendqisi2
	ldrb	r2, [r4, #91]	@ zero_extendqisi2
	str	r0, [sp, #0]
	ldr	r0, .L46+16
	mov	r1, r1, lsr #6
	ldr	r0, [r5, r0]
	and	r1, r1, #15
	stmib	sp, {r0, fp}
	ldr	r0, [sp, #40]
	str	r8, [sp, #12]
	str	r0, [sp, #16]
	ldr	r0, [sp, #36]
	str	sl, [sp, #20]
	str	r0, [sp, #24]
	mov	r0, r6
	bl	Alert_flow_error_idx
	ldrb	r3, [r5, #465]	@ zero_extendqisi2
	cmp	r7, #1
	orr	r3, r3, #64
	strb	r3, [r5, #465]
	bne	.L23
	ldrb	r2, [sp, #44]	@ zero_extendqisi2
	mov	r1, r4
	and	r2, r2, #7
	cmp	r2, #2
	movne	r2, #27
	moveq	r2, #26
	ldr	r0, .L46+24
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	b	.L38
.L23:
.LBB16:
	ldrb	r6, [r4, #75]	@ zero_extendqisi2
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	ands	r6, r6, #2
	beq	.L26
	bic	r3, r3, #32
	strb	r3, [r4, #73]
	ldrb	r2, [sp, #44]	@ zero_extendqisi2
	mov	r6, #1
	and	r2, r2, #7
	cmp	r2, #2
	movne	r2, #7
	moveq	r2, #6
	b	.L27
.L26:
	ldr	r2, [r5, #104]
	cmp	r2, #1
	biceq	r3, r3, #32
	orrne	r3, r3, #32
	mov	r2, #8
	strb	r3, [r4, #73]
.L27:
	ldr	r0, .L46+24
	mov	r1, r4
	bl	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	cmp	r6, #1
	ldreqb	r3, [r4, #74]	@ zero_extendqisi2
	biceq	r3, r3, #12
	streqb	r3, [r4, #74]
	b	.L38
.L13:
.LBE16:
.LBE15:
	ldr	r0, .L46+24
	mov	r1, r4
	bl	nm_ListGetNext
.L38:
.LBE14:
	cmp	r0, #0
.LBB19:
.LBB18:
.LBB17:
	mov	r4, r0
.LBE17:
.LBE18:
.LBE19:
	bne	.L29
	add	sp, sp, #48
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L47:
	.align	2
.L46:
	.word	0
	.word	foal_irri
	.word	14104
	.word	14108
	.word	14100
	.word	station_preserves
	.word	foal_irri+36
.LFE9:
	.size	_nm_nm_for_a_flow_error_make_flow_recording_lines_and_turn_OFF_all_ON, .-_nm_nm_for_a_flow_error_make_flow_recording_lines_and_turn_OFF_all_ON
	.section	.text._nm_nm_make_an_alert_line_about_the_valves_ON,"ax",%progbits
	.align	2
	.type	_nm_nm_make_an_alert_line_about_the_valves_ON, %function
_nm_nm_make_an_alert_line_about_the_valves_ON:
.LFB12:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, lr}
.LCFI3:
	mov	r2, #64
	mov	r7, r0
	mov	r4, r1
	mov	r0, r1
	mov	r1, #0
	bl	memset
	ldr	r0, .L58
	bl	nm_ListGetFirst
	mov	r6, #0
	subs	r5, r0, #0
	beq	.L52
.L55:
	ldr	r3, [r5, #40]
	cmp	r3, r7
	bne	.L50
	add	r6, r6, #1
	cmp	r6, #6
	moveq	r0, r4
	moveq	r1, #64
	ldreq	r2, .L58+4
	beq	.L57
.L51:
	ldrb	r1, [r5, #90]	@ zero_extendqisi2
	mov	r2, sp
	mov	r3, #8
	ldrb	r0, [r5, #91]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	mov	r0, r4
	mov	r1, #64
	ldr	r2, .L58+8
	mov	r3, sp
	bl	sp_strlcat
.L50:
	mov	r1, r5
	ldr	r0, .L58
	bl	nm_ListGetNext
	subs	r5, r0, #0
	bne	.L55
	ldr	r2, .L58+12
	mov	r0, r4
	mov	r1, #64
.L57:
	bl	sp_strlcat
.L52:
	mov	r0, r4
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, pc}
.L59:
	.align	2
.L58:
	.word	foal_irri+36
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE12:
	.size	_nm_nm_make_an_alert_line_about_the_valves_ON, .-_nm_nm_make_an_alert_line_about_the_valves_ON
	.section	.text._nm_take_action_for_a_detected_MLB,"ax",%progbits
	.align	2
	.type	_nm_take_action_for_a_detected_MLB, %function
_nm_take_action_for_a_detected_MLB:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r3, #10
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI4:
	mov	r5, r3
	moveq	r3, #1
	streqb	r3, [r0, #500]
	mov	r4, r0
	mov	r8, r1
	mov	r6, r2
	moveq	r3, #504
	beq	.L65
	cmp	r5, #20
	bne	.L63
	mov	r3, #1
	strb	r3, [r0, #501]
	mov	r3, #508
.L65:
	strh	r8, [r4, r3]	@ movhi
	add	r3, r3, #2
	strh	r6, [r4, r3]	@ movhi
	b	.L62
.L63:
	cmp	r5, #30
	moveq	r3, #1
	streqb	r3, [r0, #502]
	moveq	r3, #512
	beq	.L65
	ldr	r0, .L66
	bl	Alert_Message
.L62:
	ldr	r7, .L66+4
	mov	r1, #400
	ldr	r2, .L66+8
	ldr	r3, .L66+12
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r4, #0]
	bl	SYSTEM_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r2, r5
	mov	r3, r8
	str	r6, [sp, #0]
	mov	r1, r0
	ldr	r0, .L66+16
	bl	Alert_mainline_break
	ldr	r0, [r7, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	mov	r1, #0
	mov	r2, #1
	mov	r3, #101
	bl	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists
	ldr	r0, [r4, #0]
	mov	r1, #2
	mov	r2, #0
	mov	r3, #5
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	FOAL_IRRI_initiate_or_cancel_a_master_valve_override
.L67:
	.align	2
.L66:
	.word	.LC3
	.word	list_system_recursive_MUTEX
	.word	.LC4
	.word	1511
	.word	.LC5
.LFE7:
	.size	_nm_take_action_for_a_detected_MLB, .-_nm_take_action_for_a_detected_MLB
	.section	.text.FOAL_FLOW__A__build_poc_flow_rates,"ax",%progbits
	.align	2
	.global	FOAL_FLOW__A__build_poc_flow_rates
	.type	FOAL_FLOW__A__build_poc_flow_rates, %function
FOAL_FLOW__A__build_poc_flow_rates:
.LFB0:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI5:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI6:
	flds	s16, .L96
	ldr	r3, .L96+16
	flds	s17, .L96+4
	sub	sp, sp, #64
.LCFI7:
	str	r3, [sp, #48]	@ float
	ldr	r3, .L96+20
	fldd	d10, .L96+8
	ldr	r4, .L96+24
	str	r3, [sp, #52]	@ float
	ldr	r3, .L96+28
	fmsr	s18, r4
	str	r4, [sp, #56]	@ float
	str	r3, [sp, #60]	@ float
	ldr	r3, .L96+32
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L96+36
	mov	r3, #226
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L96+40
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L96+36
	mov	r3, #228
	bl	xQueueTakeMutexRecursive_debug
	ldr	r6, .L96+44
	mov	r7, #0
	mov	r8, r7
.L87:
	ldr	r0, [r6, #24]
	cmp	r0, #0
	beq	.L69
	mov	r1, sp
	bl	POC_get_fm_choice_and_rate_details
	ldr	r1, [r6, #28]
	cmp	r0, #0
	ldr	r0, [r6, #24]
	moveq	r7, #1
	bl	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	subs	r4, r0, #0
	bne	.L71
	ldr	r0, .L96+36
	bl	RemovePathFromFileName
	mov	r2, #254
	mov	r7, #1
	mov	r1, r0
	ldr	r0, .L96+48
	bl	Alert_Message_va
	b	.L72
.L71:
	ldr	r0, [r6, #24]
	bl	POC_get_type_of_poc
	cmp	r0, #12
	movne	r9, #1
	bne	.L72
	mov	r0, r4
	bl	POC_get_bypass_number_of_levels
	mov	r9, r0
.L72:
	ldr	r3, .L96+52
	mov	r4, #472
	mla	r4, r8, r4, r3
	mov	r5, sp
	mov	sl, #0
.L86:
	cmp	r7, #0
	bne	.L73
	cmp	sl, r9
	bcs	.L73
	ldr	r3, [r5, #0]
	cmp	r3, #0
	beq	.L73
	sub	r3, r3, #9
	cmp	r3, #8
	bhi	.L74
	ldr	r3, [r5, #12]
	cmp	r3, #1
	bhi	.L75
	cmp	r3, #0
	ldr	r3, [r4, #-12]
	fcpyseq	s14, s18
	fcpysne	s14, s17
	movne	r2, #10
	moveq	r2, #1
	movne	r1, #60
	moveq	r1, #20
	cmp	r3, #0
	beq	.L77
	ldr	r0, [r4, #-8]
	cmp	r0, r1
	bcs	.L77
	fmsr	s7, r3	@ int
	fcvtds	d7, s14
	fuitod	d6, s7
	fdivd	d6, d10, d6
	fmuld	d7, d6, d7
	fcvtsd	s14, d7
	fsts	s14, [r4, #0]
	b	.L78
.L77:
	fsts	s16, [r4, #0]
.L78:
	ldr	r3, [r4, #-24]
	flds	s14, [r4, #-4]
	mul	r3, r2, r3
	fmsr	s8, r3	@ int
	mov	r3, #0
	str	r3, [r4, #-24]
	fuitos	s15, s8
	fadds	s15, s14, s15
	fsts	s15, [r4, #-4]
	b	.L88
.L75:
	ldr	r0, .L96+56
	bl	Alert_Message
	b	.L73
.L74:
	ldr	r3, [r4, #-16]
	cmp	r3, #0
	fstseq	s16, [r4, #0]
	beq	.L81
	flds	s9, [r5, #4]	@ int
	flds	s11, [sp, #48]
	flds	s15, [sp, #60]
	flds	s8, [r5, #8]	@ int
	fuitos	s14, s9
	flds	s9, [sp, #48]
	flds	s10, [sp, #48]
	fmsr	s7, r3	@ int
	flds	s13, [sp, #60]
	fuitos	s12, s7
	fdivs	s11, s14, s11
	fdivs	s14, s14, s9
	fmuls	s14, s15, s14
	fsitos	s15, s8
	fdivs	s15, s15, s10
	fmuls	s15, s14, s15
	fmacs	s15, s11, s12
	fdivs	s15, s15, s13
	fcmpzs	s15
	fmstat
	fcpysmi	s15, s16
	fsts	s15, [r4, #0]
.L81:
	flds	s15, [r4, #-4]
	fcmpzs	s15
	fmstat
	beq	.L83
	ldr	r0, .L96+36
	bl	RemovePathFromFileName
	ldr	r2, .L96+60
	mov	r1, r0
	ldr	r0, .L96+64
	bl	Alert_Message_va
.L83:
	ldr	r3, [r4, #-24]
	cmp	r3, #0
	beq	.L88
	fmsr	s7, r3	@ int
	flds	s9, [r5, #4]	@ int
	flds	s11, [sp, #48]
	flds	s15, [sp, #52]
	fuitos	s12, s7
	flds	s7, [r4, #-20]	@ int
	fuitos	s14, s9
	flds	s9, [sp, #48]
	flds	s10, [sp, #48]
	fuitos	s8, s7
	flds	s13, [sp, #56]
	mov	r3, #0
	str	r3, [r4, #-24]
	str	r3, [r4, #-20]
	fdivs	s11, s14, s11
	fdivs	s15, s8, s15
	flds	s8, [r5, #8]	@ int
	fdivs	s14, s14, s9
	fmuls	s14, s15, s14
	fsitos	s15, s8
	fdivs	s15, s15, s10
	fmuls	s15, s14, s15
	flds	s14, [r4, #-4]
	fmacs	s15, s11, s12
	fdivs	s15, s15, s13
	fadds	s15, s15, s14
	fcmpzs	s15
	fmstat
	fcpysmi	s15, s16
	fsts	s15, [r4, #-4]
	b	.L88
.L73:
	fsts	s16, [r4, #0]
	fsts	s16, [r4, #-4]
	mov	r3, #0
	str	r3, [r4, #-24]
	str	r3, [r4, #-20]
	str	r3, [r4, #-12]
	str	r3, [r4, #-8]
.L88:
	add	sl, sl, #1
	cmp	sl, #3
	add	r4, r4, #88
	add	r5, r5, #16
	bne	.L86
.L69:
	add	r8, r8, #1
	cmp	r8, #12
	add	r6, r6, #472
	bne	.L87
	ldr	r3, .L96+40
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L96+32
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #64
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L97:
	.align	2
.L96:
	.word	0
	.word	1142292480
	.word	0
	.word	1085227008
	.word	1203982336
	.word	1148846080
	.word	1114636288
	.word	1084227584
	.word	poc_preserves_recursive_MUTEX
	.word	.LC4
	.word	list_poc_recursive_MUTEX
	.word	poc_preserves
	.word	.LC6
	.word	poc_preserves+164
	.word	.LC7
	.word	414
	.word	.LC8
.LFE0:
	.size	FOAL_FLOW__A__build_poc_flow_rates, .-FOAL_FLOW__A__build_poc_flow_rates
	.section	.text.FOAL_FLOW__B__build_system_flow_rates,"ax",%progbits
	.align	2
	.global	FOAL_FLOW__B__build_system_flow_rates
	.type	FOAL_FLOW__B__build_system_flow_rates, %function
FOAL_FLOW__B__build_system_flow_rates:
.LFB1:
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI8:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI9:
	ldr	r3, .L143+16
	sub	sp, sp, #88
.LCFI10:
	str	r3, [sp, #72]	@ float
	ldr	r3, .L143+20
	mov	r1, #400
	str	r3, [sp, #76]	@ float
	ldr	r3, .L143+24
	ldr	r2, .L143+28
	str	r3, [sp, #80]	@ float
	mov	r3, #1065353216
	str	r3, [sp, #84]	@ float
	ldr	r3, .L143+32
	ldr	r4, .L143+36
	ldr	r0, [r3, #0]
	ldr	r3, .L143+40
	bl	xQueueTakeMutexRecursive_debug
	mov	r6, #0
	mov	r5, r6
.L115:
	ldr	r3, [r4, #-308]
	cmp	r3, #0
	beq	.L99
	flds	s15, [r4, #0]
	fcmpzs	s15
	fmstat
	beq	.L100
	ldr	r0, .L143+28
	bl	RemovePathFromFileName
	ldr	r2, .L143+44
	mov	r1, r0
	ldr	r0, .L143+48
	bl	Alert_Message_va
.L100:
	ldr	r2, .L143+52
	ldr	r7, .L143+56
	mul	r8, r2, r5
	mov	r1, #0
	add	r3, r7, r8
	add	r2, r3, #484
	str	r1, [r4, #0]	@ float
	fmsr	s15, r1
	ldrh	r1, [r2, #0]
	ldr	sl, .L143+60
	bic	r1, r1, #8064
	strh	r1, [r2, #0]	@ movhi
	ldr	r2, [r3, #484]
	mov	r1, #400
	bic	r2, r2, #122880
	str	r2, [r3, #484]
	ldrb	r2, [r3, #482]	@ zero_extendqisi2
	mov	r9, #0
	bic	r2, r2, #2
	strb	r2, [r3, #482]
	ldrb	r2, [r3, #480]	@ zero_extendqisi2
	bic	r2, r2, #16
	strb	r2, [r3, #480]
	ldr	r2, .L143+64
	ldr	r3, [r4, #-8]
	mla	r3, r2, r5, r3
	ldr	r2, .L143+28
	add	r3, r7, r3, asl #2
	fsts	s15, [r3, #232]
	ldr	r3, .L143+68
	ldr	r0, [r3, #0]
	ldr	r3, .L143+72
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L143+76
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L143+28
	mov	r3, #616
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r8, #16
	add	r3, r7, r3
	str	r8, [sp, #0]
	str	r3, [sp, #8]
.L112:
	ldr	r0, [sl, #24]
	cmp	r0, #0
	beq	.L101
	ldr	r3, [sl, #44]
	ldr	r2, [sp, #8]
	cmp	r3, r2
	bne	.L101
	add	r1, sp, #12
	bl	POC_get_fm_choice_and_rate_details
	add	r1, sp, #60
	cmp	r0, #0
	ldr	r0, [sl, #24]
	moveq	r6, #1
	bl	POC_get_master_valve_NO_or_NC
	ldr	r1, [sl, #28]
	cmp	r0, #0
	ldr	r0, [sl, #24]
	moveq	r6, #1
	bl	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	subs	r8, r0, #0
	bne	.L104
	ldr	r0, .L143+28
	bl	RemovePathFromFileName
	ldr	r2, .L143+80
	mov	r6, #1
	mov	r1, r0
	ldr	r0, .L143+84
	bl	Alert_Message_va
	b	.L105
.L104:
	ldr	r0, [sl, #24]
	bl	POC_get_type_of_poc
	cmp	r0, #12
	movne	r0, #1
	bne	.L106
	mov	r0, r8
	bl	POC_get_bypass_number_of_levels
.L106:
	cmp	r6, #0
	bne	.L105
	ldr	r8, [sp, #0]
	ldr	lr, .L143+64
	add	r3, r7, r8
	ldr	r2, [r3, #484]
	mul	r8, lr, r5
	mov	r1, r2, lsr #13
	add	r1, r1, #1
	and	r1, r1, #15
	bic	r2, r2, #122880
	orr	r2, r2, r1, asl #13
	str	r2, [r3, #484]
	ldr	r2, .L143+88
	mov	r1, #472
	mla	r1, r9, r1, r2
	add	ip, sp, #56
	mov	r2, r6
	str	r8, [sp, #4]
	b	.L107
.L111:
	add	r8, sp, #12
	ldr	lr, [r8, r2, asl #4]
	cmp	lr, #0
	ldreqb	lr, [r3, #482]	@ zero_extendqisi2
	orreq	lr, lr, #2
	streqb	lr, [r3, #482]
	beq	.L109
	add	lr, r3, #484
	ldrh	r8, [lr, #0]
	mov	fp, r8, lsr #7
	add	fp, fp, #1
	and	fp, fp, #63
	bic	r8, r8, #8064
	orr	r8, r8, fp, asl #7
	strh	r8, [lr, #0]	@ movhi
	ldr	r8, [sp, #4]
	flds	s15, [r1, #0]
	ldr	lr, [r4, #-8]
	add	lr, r8, lr
	add	lr, lr, #58
	add	lr, r7, lr, asl #2
	flds	s14, [lr, #0]
	fadds	s15, s14, s15
	fsts	s15, [lr, #0]
	flds	s15, [r1, #-4]
	flds	s14, [r4, #0]
	fadds	s15, s14, s15
	fsts	s15, [r4, #0]
.L109:
	ldr	lr, [ip, #4]!
	add	r2, r2, #1
	cmp	lr, #1
	ldreqb	lr, [r3, #480]	@ zero_extendqisi2
	add	r1, r1, #88
	orreq	lr, lr, #16
	streqb	lr, [r3, #480]
.L107:
	cmp	r2, r0
	bne	.L111
.L101:
	add	r9, r9, #1
	cmp	r9, #12
	add	sl, sl, #472
	bne	.L112
.L105:
	ldr	r3, .L143+76
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L143+68
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r1, .L143+52
	ldr	r3, .L143+56
	mla	r3, r1, r5, r3
	add	r2, r3, #484
	ldrh	r2, [r2, #0]
	tst	r2, #8064
	ldrneb	r2, [r3, #481]	@ zero_extendqisi2
	ldrneb	r1, [r3, #482]	@ zero_extendqisi2
	ldreqb	r2, [r3, #481]	@ zero_extendqisi2
	bicne	r2, r2, #128
	andne	r1, r1, #1
	orrne	r2, r2, r1, asl #7
	biceq	r2, r2, #128
	strb	r2, [r3, #481]
	ldr	r2, .L143+64
	ldr	r3, [r4, #-8]
	ldr	r1, .L143+56
	mla	r2, r5, r2, r3
	add	r3, r3, #1
	add	r2, r1, r2, asl #2
	ldr	r2, [r2, #232]	@ float
	cmp	r3, #19
	str	r3, [r4, #-8]
	movhi	r3, #0
	str	r2, [r4, #-12]	@ float
	strhi	r3, [r4, #-8]
.L99:
	add	r5, r5, #1
	add	r4, r4, #14208
	cmp	r5, #4
	add	r4, r4, #16
	bne	.L115
	ldr	r5, .L143+56
	flds	s19, .L143
	mov	r4, #0
	mov	r6, r5
	mov	r7, r5
.L126:
	ldr	r3, [r5, #16]
	cmp	r3, #0
	beq	.L116
	ldr	r2, .L143+52
	flds	s15, .L143
	mov	r3, #20
	mla	r2, r4, r2, r6
	add	r2, r2, #228
.L117:
.LBB20:
	add	r2, r2, #4
	flds	s14, [r2, #0]
	subs	r3, r3, #1
	mov	r8, r2
	fadds	s15, s15, s14
	bne	.L117
	flds	s12, .L143+4
	flds	s13, [sp, #72]
	ldr	r2, .L143+64
	flds	s16, [sp, #76]
	fdivs	s15, s15, s12
	ldr	r3, [r5, #448]
	mul	r2, r4, r2
	add	r1, r2, r3
	add	r1, r6, r1, asl #2
	flds	s14, [r1, #328]
	add	r3, r3, #1
	cmp	r3, #29
	movhi	r3, #0
	add	r1, r2, r3
	str	r3, [r5, #448]
	add	r1, r7, r1, asl #2
	add	r3, r3, #1
	fmuls	s16, s16, s15
	fmacs	s16, s13, s14
	fldd	d7, .L143+8
	fcvtds	d6, s16
	fcmpd	d6, d7
	fmstat
	fcpysmi	s16, s19
	cmp	r3, #29
	movhi	r3, #0
	add	r2, r2, r3
	fsts	s16, [r1, #328]
	add	r2, r6, r2, asl #2
	flds	s17, [r2, #328]
	mov	r2, #14
.L122:
	cmp	r3, #29
	addne	r3, r3, #1
	moveq	r3, #0
	subs	r2, r2, #1
	bne	.L122
	ldr	r2, .L143+64
	mla	r3, r2, r4, r3
	add	r3, r7, r3, asl #2
	flds	s15, [r3, #328]
	fsubs	s15, s16, s15
	fmrs	r0, s15
	bl	fabsf
	fsubs	s17, s16, s17
	fmsr	s18, r0
	fmrs	r0, s17
	bl	fabsf
	flds	s13, [sp, #80]
	flds	s14, [sp, #84]
	ldr	r3, .L143+52
	fdivs	s16, s16, s13
	mla	r3, r4, r3, r6
	fadds	s16, s16, s14
	fcmpes	s18, s16
	fmsr	s15, r0
	fmstat
	bhi	.L123
	fcmpes	s15, s16
	fmstat
	ldrlsb	r2, [r3, #482]	@ zero_extendqisi2
	orrls	r2, r2, #4
	bls	.L142
.L123:
	ldrb	r2, [r3, #482]	@ zero_extendqisi2
	bic	r2, r2, #4
.L142:
	strb	r2, [r3, #482]
.L116:
.LBE20:
	add	r4, r4, #1
	add	r5, r5, #14208
	cmp	r4, #4
	add	r5, r5, #16
	bne	.L126
	ldr	r3, .L143+32
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #88
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L144:
	.align	2
.L143:
	.word	0
	.word	1101004800
	.word	-1717986918
	.word	1069128089
	.word	1060320051
	.word	1050253722
	.word	1120403456
	.word	.LC4
	.word	system_preserves_recursive_MUTEX
	.word	system_preserves+324
	.word	571
	.word	587
	.word	.LC8
	.word	14224
	.word	system_preserves
	.word	poc_preserves
	.word	3556
	.word	poc_preserves_recursive_MUTEX
	.word	614
	.word	list_poc_recursive_MUTEX
	.word	663
	.word	.LC6
	.word	poc_preserves+164
.LFE1:
	.size	FOAL_FLOW__B__build_system_flow_rates, .-FOAL_FLOW__B__build_system_flow_rates
	.section	.text.FOAL_FLOW_extract_poc_update_from_token_response,"ax",%progbits
	.align	2
	.global	FOAL_FLOW_extract_poc_update_from_token_response
	.type	FOAL_FLOW_extract_poc_update_from_token_response, %function
FOAL_FLOW_extract_poc_update_from_token_response:
.LFB2:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI11:
	sub	sp, sp, #40
.LCFI12:
	mov	r4, r0
	mov	r6, r1
	bl	COMM_MNGR_network_is_available_for_normal_use
	subs	r8, r0, #0
	bne	.L146
	ldr	r0, .L153
	bl	Alert_Message
.L146:
	ldr	r3, [r4, #0]
	ldrb	r7, [r3], #1	@ zero_extendqisi2
	cmp	r7, #12
	str	r3, [r4, #0]
	bls	.L147
	ldr	r0, .L153+4
	bl	RemovePathFromFileName
	ldr	r2, .L153+8
	mov	sl, #0
	mov	r1, r0
	ldr	r0, .L153+12
	bl	Alert_Message_va
	b	.L148
.L147:
	ldr	r3, .L153+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L153+4
	ldr	r3, .L153+20
	bl	xQueueTakeMutexRecursive_debug
	mov	r5, #0
	b	.L149
.L152:
	mov	r0, sp
	ldr	r1, [r4, #0]
	mov	r2, #32
	bl	memcpy
	ldr	r3, [r4, #0]
	cmp	r8, #0
	add	r3, r3, #32
	str	r3, [r4, #0]
	beq	.L150
	mov	r0, r6
	ldr	r1, [sp, #0]
	add	r2, sp, #36
	add	r3, sp, #32
	bl	POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters
	subs	sl, r0, #0
	bne	.L151
	ldr	r0, [sp, #0]
	mov	r1, r6
	bl	Alert_poc_not_found_extracting_token_resp
	b	.L148
.L151:
	ldr	r2, [sl, #32]
	ldr	r3, [sp, #4]
	add	r3, r2, r3
	str	r3, [sl, #32]
	ldr	r2, [sl, #36]
	ldr	r3, [sp, #8]
	add	r3, r2, r3
	str	r3, [sl, #36]
	ldr	r3, [sp, #12]
	str	r3, [sl, #40]
	ldr	r3, [sp, #16]
	str	r3, [sl, #44]
	ldr	r3, [sp, #20]
	str	r3, [sl, #48]
	ldr	r3, [sp, #24]
	str	r3, [sl, #72]
	ldr	r3, [sp, #28]
	str	r3, [sl, #76]
.L150:
	add	r5, r5, #1
.L149:
	cmp	r5, r7
	bcc	.L152
	mov	sl, #1
.L148:
	ldr	r3, .L153+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, sl
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L154:
	.align	2
.L153:
	.word	.LC9
	.word	.LC4
	.word	974
	.word	.LC10
	.word	poc_preserves_recursive_MUTEX
	.word	983
.LFE2:
	.size	FOAL_FLOW_extract_poc_update_from_token_response, .-FOAL_FLOW_extract_poc_update_from_token_response
	.section	.text.FOAL_FLOW_load_system_info_for_distribution_to_slaves,"ax",%progbits
	.align	2
	.global	FOAL_FLOW_load_system_info_for_distribution_to_slaves
	.type	FOAL_FLOW_load_system_info_for_distribution_to_slaves, %function
FOAL_FLOW_load_system_info_for_distribution_to_slaves:
.LFB3:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L165
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI13:
	ldr	r2, .L165+4
	mov	r7, #0
	str	r7, [r0, #0]
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	sl, .L165+8
	ldr	r3, .L165+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, .L165+16
	mov	r3, sl
	mov	r9, r7
.L157:
	ldr	r1, [r3, #16]
	add	r3, r3, #14208
	cmp	r1, #0
	add	r3, r3, #16
	addne	r9, r9, #1
	cmp	r3, r2
	bne	.L157
	cmp	r9, #0
	str	r9, [sp, #4]
	beq	.L158
.LBB21:
	mov	r7, #30
	mul	r7, r9, r7
	ldr	r1, .L165+4
	add	r9, r7, #1
	ldr	r2, .L165+20
	mov	r0, r9
	bl	mem_malloc_debug
	add	r1, sp, #4
	mov	r2, #1
	mov	r8, #0
	str	r0, [r5, #0]
	mov	r4, r0
.LBB22:
	ldr	r5, .L165+8
.LBE22:
	bl	memcpy
.LBB23:
	ldr	r3, .L165+24
.LBE23:
	add	r4, r4, #1
.L160:
	ldr	r2, [sl, #16]
	cmp	r2, #0
	beq	.L159
.LBB24:
	mul	r6, r3, r8
	mov	r0, r4
	add	r1, r6, #16
	add	r1, r5, r1
	mov	r2, #2
	str	r3, [sp, #0]
	bl	memcpy
	add	r1, r6, #480
	add	r0, r4, #2
	add	r1, r5, r1
	mov	r2, #8
	bl	memcpy
	add	r1, r6, #108
	add	r0, r4, #10
	add	r1, r5, r1
	mov	r2, #2
	bl	memcpy
	add	r1, r6, #116
	add	r0, r4, #12
	add	r1, r5, r1
	mov	r2, #2
	bl	memcpy
	add	r1, r6, #312
	add	r0, r4, #14
	add	r1, r5, r1
	mov	r2, #4
	add	fp, r5, r6
	bl	memcpy
	ldrb	r2, [fp, #482]	@ zero_extendqisi2
	add	r0, r4, #18
	mov	r2, r2, lsr #3
	and	r2, r2, #1
	str	r2, [sp, #8]
	add	r1, sp, #8
	mov	r2, #4
	bl	memcpy
	ldrb	r2, [fp, #482]	@ zero_extendqisi2
	add	r0, r4, #22
	mov	r2, r2, lsr #4
	and	r2, r2, #1
	str	r2, [sp, #8]
	add	r1, sp, #8
	mov	r2, #4
	bl	memcpy
	add	r1, r6, #460
	add	r0, r4, #26
	mov	r2, #4
	add	r1, r5, r1
	bl	memcpy
	ldr	r2, [sp, #4]
	ldr	r3, [sp, #0]
	sub	r2, r2, #1
	add	r4, r4, #30
	sub	r7, r7, #30
	str	r2, [sp, #4]
.L159:
.LBE24:
	add	r8, r8, #1
	add	sl, sl, #14208
	cmp	r8, #4
	add	sl, sl, #16
	bne	.L160
	ldr	r3, [sp, #4]
	orrs	r3, r7, r3
	beq	.L158
	ldr	r0, .L165+4
	bl	RemovePathFromFileName
	ldr	r2, .L165+28
	mov	r1, r0
	ldr	r0, .L165+32
	bl	Alert_Message_va
.L158:
.LBE21:
	ldr	r3, .L165
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r9
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L166:
	.align	2
.L165:
	.word	system_preserves_recursive_MUTEX
	.word	.LC4
	.word	system_preserves
	.word	1116
	.word	system_preserves+56896
	.word	1144
	.word	14224
	.word	1207
	.word	.LC11
.LFE3:
	.size	FOAL_FLOW_load_system_info_for_distribution_to_slaves, .-FOAL_FLOW_load_system_info_for_distribution_to_slaves
	.section	.text.FOAL_FLOW_load_poc_info_for_distribution_to_slaves,"ax",%progbits
	.align	2
	.global	FOAL_FLOW_load_poc_info_for_distribution_to_slaves
	.type	FOAL_FLOW_load_poc_info_for_distribution_to_slaves, %function
FOAL_FLOW_load_poc_info_for_distribution_to_slaves:
.LFB4:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI14:
	ldr	r1, .L175
	sub	sp, sp, #48
.LCFI15:
	mov	r9, r0
	ldr	r2, .L175+4
	mov	r0, #592
	bl	mem_malloc_debug
	ldr	r3, .L175+8
	mov	r1, #400
	ldr	r2, .L175
	ldr	r6, .L175+12
	mov	r8, #0
	mov	r7, r8
	mov	sl, #1
	mov	fp, #472
	str	r0, [r9, #0]
	mov	r4, r0
	add	r5, r0, #1
	ldr	r0, [r3, #0]
	ldr	r3, .L175+16
	bl	xQueueTakeMutexRecursive_debug
.L170:
	ldr	r3, [r6, #24]
	cmp	r3, #0
	beq	.L168
	mov	r1, #0
	mov	r2, #48
	mov	r0, sp
	bl	memset
	ldr	r3, [r6, #28]
	ldr	r2, .L175+12
	str	r3, [sp, #0]
	ldr	r3, [r6, #32]
	add	r7, r7, #1
	str	r3, [sp, #4]
	ldr	r3, [r6, #112]
	and	r7, r7, #255
	str	r3, [sp, #8]
	mla	r3, fp, r8, r2
	add	r0, sp, #8
	add	r3, r3, #20
	mov	r2, sp
	mov	r1, #3
.L169:
	ldr	ip, [r3, #144]	@ float
	subs	r1, r1, #1
	str	ip, [r0, #4]!	@ float
	ldr	ip, [r3, #160]
	str	ip, [r2, #24]
	ldr	ip, [r3, #164]
	add	r3, r3, #88
	str	ip, [r2, #36]
	add	r2, r2, #4
	bne	.L169
	mov	r0, r5
	mov	r1, sp
	mov	r2, #48
	bl	memcpy
	add	r5, r5, #48
	add	sl, sl, #48
.L168:
	add	r8, r8, #1
	cmp	r8, #12
	add	r6, r6, #472
	bne	.L170
	ldr	r3, .L175+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r7, #0
	strneb	r7, [r4, #0]
	bne	.L172
	ldr	r0, [r9, #0]
	ldr	r1, .L175
	ldr	r2, .L175+20
	bl	mem_free_debug
	mov	sl, r7
	str	r7, [r9, #0]
.L172:
	mov	r0, sl
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L176:
	.align	2
.L175:
	.word	.LC4
	.word	1243
	.word	poc_preserves_recursive_MUTEX
	.word	poc_preserves
	.word	1261
	.word	1319
.LFE4:
	.size	FOAL_FLOW_load_poc_info_for_distribution_to_slaves, .-FOAL_FLOW_load_poc_info_for_distribution_to_slaves
	.section	.text.FOAL_FLOW_check_for_MLB,"ax",%progbits
	.align	2
	.global	FOAL_FLOW_check_for_MLB
	.type	FOAL_FLOW_check_for_MLB, %function
FOAL_FLOW_check_for_MLB:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L191
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI16:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L191+4
	ldr	r3, .L191+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, .L191+12
	ldr	r8, .L191+16
	ldr	r7, .L191+20
	mov	r5, #0
	sub	r6, r4, #448
.L185:
	ldr	r0, [r4, #-432]
	cmp	r0, #0
	beq	.L178
	ldr	r3, [r4, #0]
	mla	fp, r7, r5, r6
	mla	r3, r8, r5, r3
	add	fp, fp, #16
	add	r3, r6, r3, asl #2
	flds	s15, [r3, #328]
	ldr	r3, [r4, #-340]
	ftouizs	s15, s15
	cmp	r3, #0
	fmrs	r9, s15	@ int
	flds	s15, [r4, #-136]
	ftouizs	s15, s15
	fmrs	sl, s15	@ int
	beq	.L179
.LBB25:
	ldr	r3, [fp, #92]
	cmp	r3, #1
	bne	.L180
.LBB26:
	ldr	r2, .L191+24
	ldr	r2, [r2, #36]
	ldrh	r2, [r2, #72]
	and	r2, r2, #960
	cmp	r2, #384
	beq	.L181
.L180:
	ldr	r3, [fp, #456]
	rsbs	r3, r3, #1
	movcc	r3, #0
.L181:
.LBE26:
	mov	r2, #468
	ldrh	r2, [fp, r2]
	ands	r2, r2, #8064
	moveq	r3, r2
	ldr	r2, [fp, #208]
	cmp	r2, #0
	bne	.L178
.LBE25:
	cmp	r3, #1
	bne	.L178
	bl	SYSTEM_get_mlb_during_irri_gpm
	cmp	sl, r0
	cmphi	r9, r0
	mov	r2, r0
	bls	.L178
	ldrb	r3, [r4, #68]	@ zero_extendqisi2
	cmp	r3, #0
	moveq	r0, fp
	moveq	r1, sl
	moveq	r3, #10
	bne	.L178
	b	.L190
.L179:
.LBB27:
	ldr	r1, [fp, #460]
	mov	r2, #468
	cmp	r1, #0
	ldrh	r2, [fp, r2]
	bne	.L183
	tst	r2, #8064
	moveq	r3, #0
	movne	r3, #1
.L183:
	ldr	r2, [fp, #456]
	cmp	r2, #0
	bne	.L178
.LBE27:
	cmp	r3, #1
	bne	.L178
	mla	r3, r7, r5, r6
	ldrb	r3, [r3, #482]	@ zero_extendqisi2
	tst	r3, #8
	beq	.L184
	bl	SYSTEM_get_mlb_during_mvor_opened_gpm
	cmp	sl, r0
	cmphi	r9, r0
	mov	r2, r0
	bls	.L178
	ldrb	r3, [r4, #69]	@ zero_extendqisi2
	cmp	r3, #0
	moveq	r0, fp
	moveq	r1, sl
	moveq	r3, #20
	bne	.L178
	b	.L190
.L184:
	bl	SYSTEM_get_mlb_during_all_other_times_gpm
	cmp	sl, r0
	cmphi	r9, r0
	mov	r2, r0
	bls	.L178
	ldrb	r3, [r4, #70]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L178
	mov	r0, fp
	mov	r1, sl
	mov	r3, #30
.L190:
	bl	_nm_take_action_for_a_detected_MLB
.L178:
	add	r5, r5, #1
	add	r4, r4, #14208
	cmp	r5, #4
	add	r4, r4, #16
	bne	.L185
	ldr	r3, .L191
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	xQueueGiveMutexRecursive
.L192:
	.align	2
.L191:
	.word	system_preserves_recursive_MUTEX
	.word	.LC4
	.word	1556
	.word	system_preserves+448
	.word	3556
	.word	14224
	.word	foal_irri
.LFE8:
	.size	FOAL_FLOW_check_for_MLB, .-FOAL_FLOW_check_for_MLB
	.section	.text.FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines,"ax",%progbits
	.align	2
	.global	FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines
	.type	FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines, %function
FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines:
.LFB10:
	@ args = 8, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI17:
	fstmfdd	sp!, {d8}
.LCFI18:
.LBB28:
	ldr	r7, .L201+4
.LBE28:
	sub	sp, sp, #20
.LCFI19:
	str	r3, [sp, #12]
	mov	sl, r2
	ldr	r3, .L201+8
	ldr	r2, [sp, #68]
	mov	r5, r0
	str	r2, [sp, #16]
	ldr	r0, [r3, #0]
	ldr	r2, .L201+12
	ldr	r3, .L201+16
	mov	r8, r1
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L201+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L201+12
	mov	r3, #2128
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L201+24
	bl	nm_ListGetFirst
.LBB29:
	ldr	r6, .L201+28
.LBE29:
	mov	r4, r0
	b	.L194
.L199:
	ldr	r3, [r4, #40]
	cmp	r3, r5
	bne	.L195
.LBB30:
	cmp	r8, #1
	ldreqb	r3, [r4, #75]	@ zero_extendqisi2
	orreq	r3, r3, #16
	streqb	r3, [r4, #75]
	ldr	r3, [r5, #100]
	cmp	r3, #0
	ldrneh	r2, [r4, #80]
	fldseq	s16, .L201
	fmsrne	s14, r2	@ int
	ldr	r2, [r5, r7]
	fuitosne	s16, s14
	fmsrne	s14, r3	@ int
	fuitosne	s15, s14
	fmsr	s14, r2	@ int
	fdivsne	s16, s16, s15
	fuitos	s15, s14
	fmuls	s15, s15, s16
	fmrs	r0, s15
	bl	__round_UNS16
	flds	s14, [sp, #16]	@ int
	fuitos	s15, s14
	fmuls	s15, s15, s16
	mov	r1, r0
	fmrs	r0, s15
	str	r1, [sp, #8]
	bl	__round_UNS16
	ldr	r2, [r5, r6]
	fmsr	s14, r2	@ int
	fuitos	s15, s14
	fmuls	s15, s15, s16
	mov	fp, r0
	fmrs	r0, s15
	bl	__round_UNS16
	ldr	r3, .L201+32
	ldr	r3, [r5, r3]
	fmsr	s14, r3	@ int
	fuitos	s15, s14
	fmuls	s15, s15, s16
	mov	r9, r0
	fmrs	r0, s15
	bl	__round_UNS16
	cmp	sl, #1
	ldr	r1, [sp, #8]
	mov	r3, r0
	bne	.L198
	ldrh	r2, [r4, #72]
	and	r2, r2, #960
	cmp	r2, #64
	bne	.L198
	ldr	r0, [r4, #44]
	ldr	r2, .L201+36
	add	r2, r2, r0, asl #7
	ldrb	r0, [r2, #36]	@ zero_extendqisi2
	tst	r0, #1
	orreq	r0, r0, #1
	streqh	fp, [r2, #62]	@ movhi
	streqh	r9, [r2, #64]	@ movhi
	streqh	r3, [r2, #66]	@ movhi
	streqb	r0, [r2, #36]
.L198:
	ldr	r2, [sp, #12]
	cmp	r2, #1
	bne	.L195
	ldrh	r2, [sp, #64]
	mov	r0, r4
	strh	r2, [sp, #4]	@ movhi
	mov	r2, r9
	str	fp, [sp, #0]
	bl	nm_flow_recorder_add
.L195:
.LBE30:
	mov	r1, r4
	ldr	r0, .L201+24
	bl	nm_ListGetNext
	mov	r4, r0
.L194:
	cmp	r4, #0
	bne	.L199
	ldr	r3, .L201+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L201+8
	ldr	r0, [r3, #0]
	add	sp, sp, #20
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	xQueueGiveMutexRecursive
.L202:
	.align	2
.L201:
	.word	0
	.word	14100
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC4
	.word	2126
	.word	system_preserves_recursive_MUTEX
	.word	foal_irri+36
	.word	14104
	.word	14108
	.word	station_preserves
.LFE10:
	.size	FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines, .-FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines
	.global	__udivsi3
	.global	__divsi3
	.section	.text.FOAL_FLOW_check_valve_flow,"ax",%progbits
	.align	2
	.global	FOAL_FLOW_check_valve_flow
	.type	FOAL_FLOW_check_valve_flow, %function
FOAL_FLOW_check_valve_flow:
.LFB17:
	@ args = 0, pretend = 0, frame = 100
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI20:
	fstmfdd	sp!, {d8}
.LCFI21:
	ldr	r3, .L312
	sub	sp, sp, #112
.LCFI22:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L312+4
	ldr	r3, .L312+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L312+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L312+4
	ldr	r3, .L312+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r5, .L312+20
	ldr	r7, .L312+24
	mov	r8, #0
	mov	r6, r5
.L270:
	ldr	r3, [r5, #16]
	cmp	r3, #0
	beq	.L204
	ldr	r4, [r5, #148]
	cmp	r4, #1
	beq	.L205
	ldr	r4, [r5, #156]
	sub	r3, r4, #1
	rsbs	r4, r3, #0
	adc	r4, r4, r3
.L205:
	ldr	r3, .L312+28
	ldr	r2, [r5, #108]
	mla	r3, r8, r3, r6
	mov	sl, #0
	cmp	r2, sl
	strh	sl, [sp, #108]	@ movhi
	ldreqb	r2, [r3, #480]	@ zero_extendqisi2
	orreq	r2, r2, #128
	streqb	r2, [r3, #480]
	beq	.L207
	add	r1, r3, #484
	ldrh	r1, [r1, #0]
	tst	r1, #8064
	bne	.L208
	ldrb	r2, [sp, #108]	@ zero_extendqisi2
	bic	r2, r2, #3
	orr	r2, r2, #68
	strb	r2, [sp, #108]
	ldrb	r2, [r3, #481]	@ zero_extendqisi2
	orr	r2, r2, #1
	strb	r2, [r3, #481]
	b	.L207
.L208:
	cmp	r4, #1
	bne	.L209
	cmp	r2, #1
	bne	.L210
	ldrb	r2, [sp, #108]	@ zero_extendqisi2
	bic	r2, r2, #3
	orr	r2, r2, #4
	strb	r2, [sp, #108]
	ldrb	r2, [sp, #109]	@ zero_extendqisi2
	orr	r2, r2, #1
	strb	r2, [sp, #109]
	ldrb	r2, [r3, #481]	@ zero_extendqisi2
	orr	r2, r2, #32
	b	.L300
.L210:
	ldr	r0, .L312+32
	bl	Alert_Message
	b	.L299
.L275:
	ldrb	r2, [sp, #108]	@ zero_extendqisi2
	bic	r2, r2, #3
	orr	r2, r2, #132
	strb	r2, [sp, #108]
	ldrb	r2, [r3, #481]	@ zero_extendqisi2
	orr	r2, r2, #1
.L300:
	strb	r2, [r3, #481]
	b	.L212
.L272:
	ldrb	r2, [sp, #108]	@ zero_extendqisi2
	bic	r2, r2, #3
	orr	r2, r2, #4
	strb	r2, [sp, #108]
	ldrb	r2, [sp, #109]	@ zero_extendqisi2
	orr	r2, r2, #32
	strb	r2, [sp, #109]
	ldrb	r2, [r3, #481]	@ zero_extendqisi2
	orr	r2, r2, #1
	strb	r2, [r3, #481]
.L299:
	mov	r4, sl
.L212:
	mov	r9, #0
	b	.L213
.L310:
	ldrb	r2, [sp, #108]	@ zero_extendqisi2
	mov	r9, r4
	bic	r2, r2, #3
	orr	r2, r2, #4
	strb	r2, [sp, #108]
	ldrb	r2, [sp, #109]	@ zero_extendqisi2
	orr	r2, r2, #2
	strb	r2, [sp, #109]
	ldrb	r2, [r3, #481]	@ zero_extendqisi2
	orr	r2, r2, #1
	strb	r2, [r3, #481]
.L213:
	ldr	r2, .L312+36
	ldr	r3, [r5, #448]
	cmp	r4, #1
	mla	r3, r2, r8, r3
	ldr	r2, [r5, #116]
	add	r3, r6, r3, asl #2
	flds	s16, [r3, #328]
	str	r2, [sp, #24]
	bne	.L214
	ldr	fp, .L312+28
	mul	fp, r8, fp
	add	sl, fp, #16
	add	sl, sl, r6
	mov	r0, sl
	bl	_allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line
	cmp	r0, #1
	bne	.L204
	add	fp, r6, fp
	ldrb	r1, [fp, #481]	@ zero_extendqisi2
	mov	r1, r1, lsr #6
	ands	r1, r1, #1
	bne	.L204
	ftouizs	s16, s16
	ldrh	r3, [sp, #108]
	mov	r0, sl
	strh	r3, [sp, #0]	@ movhi
	mov	r2, r1
	mov	r3, r4
	fsts	s16, [sp, #4]	@ int
	bl	FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines
	ldrb	r3, [fp, #481]	@ zero_extendqisi2
	fmrs	r9, s16	@ int
	orr	r3, r3, #64
	strb	r3, [fp, #481]
.LBB38:
	ldr	r3, [sl, #92]
	cmp	r3, #1
	beq	.L215
	ldr	r0, .L312+4
	bl	RemovePathFromFileName
	ldr	r2, .L312+40
	mov	r1, r0
	ldr	r0, .L312+44
	bl	Alert_Message_va
.L215:
	ldr	r0, .L312+48
	ldr	ip, [sl, #92]
	mov	r3, #0
	str	r3, [r0, #76]
	add	r0, r0, #36
	str	ip, [sp, #28]
	bl	nm_ListGetFirst
	b	.L301
.L224:
	ldr	r3, [r4, #40]
	cmp	r3, sl
	bne	.L217
	ldr	ip, [sp, #28]
	cmp	ip, #1
	bne	.L218
	cmp	r9, #0
	bne	.L219
	ldrb	r1, [r4, #90]	@ zero_extendqisi2
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	add	r1, r1, #1
	add	r2, sp, #32
	mov	r3, #16
	bl	STATION_get_station_number_string
	mov	r1, r0
	ldr	r0, .L312+52
	bl	Alert_Message_va
	b	.L218
.L219:
	ldr	lr, .L312+56
	ldr	r2, .L312+4
	ldr	r0, [lr, #0]
	ldr	r3, .L312+60
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldrb	r1, [r4, #90]	@ zero_extendqisi2
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	bl	nm_STATION_get_pointer_to_station
	mov	fp, r0
	mov	r1, fp
	ldr	r0, .L312+64
	bl	nm_OnList
	cmp	r0, #1
	bne	.L220
	bl	FLOWSENSE_get_controller_index
	mov	r1, #5
	mov	r3, r0
	mov	r0, fp
	str	r3, [sp, #12]
	bl	STATION_get_change_bits_ptr
	ldr	r3, [sp, #12]
	ldr	ip, [sp, #28]
	str	r3, [sp, #0]
	mov	r1, r9
	mov	r2, ip
	mov	r3, #5
	str	ip, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, fp
	bl	nm_STATION_set_expected_flow_rate
	b	.L221
.L220:
	ldr	r0, .L312+4
	bl	RemovePathFromFileName
	ldr	r2, .L312+68
	mov	r1, r0
	ldr	r0, .L312+72
	bl	Alert_Message_va
.L221:
	ldr	lr, .L312+56
	ldr	r0, [lr, #0]
	bl	xQueueGiveMutexRecursive
	strh	r9, [r4, #80]	@ movhi
	str	r9, [sl, #100]
.L218:
	ldrh	r3, [r4, #72]
	ldr	r0, .L312+76
	and	r3, r3, #960
	cmp	r3, #64
	mov	r1, r4
	bne	.L222
	mov	r2, #18
	bl	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	b	.L301
.L222:
	mov	r2, #25
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	b	.L301
.L217:
	ldr	r0, .L312+76
	mov	r1, r4
	bl	nm_ListGetNext
.L301:
	cmp	r0, #0
	mov	r4, r0
	bne	.L224
	b	.L204
.L214:
.LBE38:
	cmp	r9, #1
	bne	.L225
	ldr	r4, [r5, #108]
	cmp	r4, #1
	bls	.L278
	ldr	r0, [sp, #24]
	ldr	r1, [r7, #-64]
	bl	__udivsi3
	ldr	r2, [r7, #-60]
	ldr	r3, [r7, #-56]
	cmp	r4, r2
	str	r0, [sp, #28]
	bhi	.L280
	cmp	r0, r3
	bcs	.L280
	ldr	r3, .L312+28
.LBB39:
	ldr	r2, .L312+80
.LBE39:
	mul	r3, r8, r3
.LBB40:
	ldr	ip, [sp, #28]
.LBE40:
	add	r1, r3, #16
	add	r1, r1, r6
.LBB41:
	ldr	r0, [r1, #92]
	ldr	r2, [r1, r2]
	sub	r0, r0, #2
	sub	r2, r2, #1
	mla	r2, ip, r2, r0
.LBE41:
	add	r3, r6, r3
	add	r3, r3, r2
	add	r3, r3, #9536
	ldrb	r1, [r3, #12]	@ zero_extendqisi2
	ldr	r3, [r7, #-72]
	mov	sl, #0
	cmp	r1, r3
	bcc	.L281
	b	.L227
.L280:
	mov	sl, r9
	mov	r2, #0
.L281:
	mov	r9, sl
.L227:
	cmp	sl, #1
	ldr	ip, [r5, #180]
	bne	.L226
	b	.L228
.L278:
	mov	r2, #0
	mov	ip, r9
	str	r2, [sp, #28]
.L226:
	ldr	r3, [r5, #120]
	cmp	r3, #0
	ldr	r3, .L312+28
	mla	r3, r8, r3, r6
	ldrb	r1, [r3, #481]	@ zero_extendqisi2
	bne	.L229
	cmp	ip, #0
	moveq	sl, #0
	andne	sl, r9, #1
	cmp	sl, #0
	orreq	r1, r1, #8
	streqb	r1, [r3, #481]
	beq	.L228
.L229:
	orr	r1, r1, #2
	mov	sl, #0
	strb	r1, [r3, #481]
.L228:
	ldr	fp, .L312+28
	str	r2, [sp, #20]
	mul	r3, fp, r8
	str	ip, [sp, #16]
	add	r4, r3, #16
	add	r4, r4, r6
	mov	r0, r4
	str	r3, [sp, #12]
	bl	_allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line
	ldr	r2, [sp, #20]
	ldr	r3, [sp, #12]
	ldr	ip, [sp, #16]
	cmp	r0, #1
	bne	.L204
	cmp	sl, #1
	beq	.L231
	ldr	r1, [r5, #120]
	cmp	r1, #1
	beq	.L232
	cmp	ip, #1
	movne	r1, #0
	andeq	r1, r9, #1
	cmp	r1, #0
	beq	.L233
.L232:
	ldr	lr, .L312+20
	mla	fp, r8, fp, lr
	ldrb	r3, [fp, #481]	@ zero_extendqisi2
	orr	r3, r3, #4
	strb	r3, [fp, #481]
	b	.L231
.L233:
	add	r3, r6, r3
	ldrb	r1, [r3, #481]	@ zero_extendqisi2
	orr	r1, r1, #16
	strb	r1, [r3, #481]
.L231:
	ldr	fp, [r5, #220]
	cmp	fp, #0
	movne	fp, #1
	bne	.L284
	ldr	r3, [r5, #216]
	cmp	r3, #0
	movne	r3, #1
	bne	.L234
	ldr	fp, [r5, #212]
	cmp	fp, #0
	movne	fp, #1
	bne	.L234
.L284:
	mov	r3, fp
.L234:
	cmp	sl, #0
	bne	.L286
	ldr	r1, [r5, #108]
	cmp	r1, #1
	beq	.L286
	ldr	r1, .L312+84
	mla	r2, r1, r8, r2
	mov	r1, #10
	mov	r2, r2, asl #1
	add	r2, r2, #548
	ldrsh	r0, [r6, r2]
	str	r3, [sp, #12]
	str	ip, [sp, #16]
	bl	__divsi3
	ldr	r3, [sp, #12]
	ldr	ip, [sp, #16]
	mov	r2, r0, asl #16
	ldr	r0, [sp, #24]
	mov	r2, r2, asr #16
	cmp	r2, r0
	rsblt	r2, r2, r0
	blt	.L235
	b	.L309
.L286:
	ldr	r2, [sp, #24]
	b	.L235
.L309:
	mov	r2, sl
.L235:
	str	r2, [r7, #-8]
.LBB42:
	ldr	r1, .L312+88
	ldr	r2, .L312+92
	ldr	r1, [r4, r1]
	ldr	r2, [r4, r2]
	cmp	r2, r1
	movcc	r1, #0
	bcc	.L236
	ldr	r1, .L312+96
	ldr	r1, [r4, r1]
	cmp	r2, r1
	movls	r1, #1
	bls	.L236
	ldr	r1, .L312+100
	ldr	r1, [r4, r1]
	cmp	r2, r1
	movls	r1, #2
	movhi	r1, #3
.L236:
	add	r0, r1, #3504
	add	r0, r0, #13
	ldr	lr, [r4, r0, asl #2]
	ldr	r0, .L312+104
	add	r1, r1, #3520
	add	lr, lr, r2
	add	r1, r1, #1
	str	lr, [r4, r0]
	ldr	r0, [r4, r1, asl #2]
	ldr	r1, .L312+108
	cmp	r0, r2
	movcs	r2, #0
	rsbcc	r2, r0, r2
	str	r2, [r4, r1]
.LBE42:
	ldr	r2, .L312+28
	mla	r2, r8, r2, r6
	ldrb	r2, [r2, #481]	@ zero_extendqisi2
	mov	r2, r2, lsr #2
	and	r2, r2, #1
	tst	r2, r3
	beq	.L239
	flds	s9, [r7, #-4]	@ int
	fuitos	s15, s9
	fcmpes	s16, s15
	fmstat
	ldrgtb	r3, [sp, #108]	@ zero_extendqisi2
	bicgt	r3, r3, #5
	orrgt	r3, r3, #2
	bgt	.L306
.L239:
	and	r1, fp, #255
	tst	r1, r2
	beq	.L241
	flds	s10, [r7, #0]	@ int
	fuitos	s15, s10
	fcmpes	s16, s15
	fmstat
	bpl	.L241
	ldrb	r3, [sp, #108]	@ zero_extendqisi2
	bic	r3, r3, #4
	orr	r3, r3, #3
.L306:
	ftouizs	s16, s16
	strb	r3, [sp, #108]
	ldr	r0, [sp, #108]
	mov	r1, r4
	fmrs	r2, s16	@ int
	bl	_nm_nm_for_a_flow_error_make_flow_recording_lines_and_turn_OFF_all_ON
	b	.L204
.L241:
	ldr	r0, .L312+28
	mla	r0, r8, r0, r6
	ldrb	r0, [r0, #481]	@ zero_extendqisi2
	tst	r0, #64
	bne	.L204
	cmp	sl, #0
	movne	r2, #0
	andeq	r2, r2, #1
	cmp	r2, #0
	ldrb	r2, [sp, #108]	@ zero_extendqisi2
	bicne	r2, r2, #6
	orrne	r2, r2, #1
	strneb	r2, [sp, #108]
	bne	.L244
	bic	r2, r2, #3
	orr	r2, r2, #4
	cmp	sl, #1
	strb	r2, [sp, #108]
	bne	.L245
	and	r3, r2, #255
	orr	r3, r3, #8
	strb	r3, [sp, #108]
	ldr	ip, [sp, #28]
	ldr	r3, [r7, #-56]
	cmp	ip, r3
	bcc	.L246
	bl	Alert_derate_table_flow_too_high
.L246:
	ldr	r2, [r5, #108]
	ldr	r3, [r7, #-60]
	cmp	r2, r3
	bls	.L244
	bl	Alert_derate_table_too_many_stations
	b	.L244
.L245:
	cmp	ip, #0
	ldreqb	r2, [sp, #108]	@ zero_extendqisi2
.LBB43:
	ldr	r0, .L312+112
.LBE43:
	orreq	r2, r2, #16
	streqb	r2, [sp, #108]
	cmp	r9, #0
	ldreqb	r2, [sp, #108]	@ zero_extendqisi2
.LBB45:
	str	r0, [sp, #96]	@ float
.LBE45:
	orreq	r2, r2, #32
	streqb	r2, [sp, #108]
.LBB46:
	mov	r2, #1056964608
	str	r2, [sp, #100]	@ float
	ldr	r2, .L312+116
	ldr	ip, [sp, #28]
	str	r2, [sp, #104]	@ float
	ldr	r2, .L312+120
	ldr	r2, [r4, r2]
	cmp	ip, r2
	bcc	.L249
	ldr	r0, .L312+4
	bl	RemovePathFromFileName
	ldr	r2, .L312+124
	mov	r1, r0
	ldr	r0, .L312+128
	b	.L305
.L249:
	ldr	r0, .L312+80
	ldr	r2, [r4, #92]
	ldr	r0, [r4, r0]
	cmp	r2, r0
	bls	.L250
	ldr	r0, .L312+4
	bl	RemovePathFromFileName
	ldr	r2, .L312+132
	mov	r1, r0
	ldr	r0, .L312+128
.L305:
	bl	Alert_Message_va
	b	.L244
.L250:
	cmp	r2, #1
	bne	.L251
	ldr	r0, .L312+4
	bl	RemovePathFromFileName
	ldr	r2, .L312+136
	mov	r1, r0
	ldr	r0, .L312+140
	b	.L305
.L251:
.LBE46:
	ftouizs	s9, s16
	fmrs	r9, s9	@ int
.LBB47:
	cmp	r9, #0
	movne	r1, #0
	andeq	r1, r1, #1
	cmp	r1, #0
	beq	.L252
	ldrb	r3, [sp, #109]	@ zero_extendqisi2
	add	r1, sp, #32
	orr	r3, r3, #64
	mov	r0, r4
	strb	r3, [sp, #109]
	bl	_nm_nm_make_an_alert_line_about_the_valves_ON
	mov	r2, #64
	mov	r1, r0
	add	r0, sp, #32
	bl	strlcpy
	mov	r0, #185
	mov	r1, #0
	b	.L304
.L252:
	cmp	r9, #9
	bls	.L254
	ldr	r0, [r4, #100]
	str	r3, [sp, #12]
	rsb	r0, r9, r0
	bl	abs
	ldr	r2, [r4, #100]
	flds	s13, [sp, #96]
	ldr	r3, [sp, #12]
	fmsr	s12, r2	@ int
	fuitos	s15, s12
	fmuls	s15, s13, s15
	fmsr	s10, r0	@ int
	fsitos	s14, s10
	fcmpes	s14, s15
	fmstat
	ble	.L255
	cmp	fp, #1
	bne	.L257
	cmp	r9, r2
	bcc	.L258
.L257:
	cmp	r3, #1
	bne	.L255
	cmp	r9, r2
	bls	.L255
.L258:
	ldrb	r3, [sp, #109]	@ zero_extendqisi2
	add	r1, sp, #32
	orr	r3, r3, #128
	mov	r0, r4
	strb	r3, [sp, #109]
	bl	_nm_nm_make_an_alert_line_about_the_valves_ON
	mov	r2, #64
	mov	r1, r0
	add	r0, sp, #32
	bl	strlcpy
	mov	r0, #186
	b	.L308
.L254:
	cmp	r9, #4
	bls	.L255
	ldr	r0, [r4, #100]
	str	r3, [sp, #12]
	rsb	r0, r9, r0
	bl	abs
	ldr	r2, [r4, #100]
	flds	s13, [sp, #100]
	ldr	r3, [sp, #12]
	fmsr	s9, r2	@ int
	fmsr	s15, r0	@ int
	fsitos	s14, s15
	fuitos	s15, s9
	fmuls	s15, s13, s15
	fcmpes	s14, s15
	fmstat
	ble	.L255
	cmp	fp, #1
	bne	.L260
	cmp	r9, r2
	bcc	.L261
.L260:
	cmp	r3, #1
	bne	.L255
	cmp	r9, r2
	bls	.L255
.L261:
	ldrb	r3, [sp, #109]	@ zero_extendqisi2
	add	r1, sp, #32
	orr	r3, r3, #128
	mov	r0, r4
	strb	r3, [sp, #109]
	bl	_nm_nm_make_an_alert_line_about_the_valves_ON
	mov	r2, #64
	mov	r1, r0
	add	r0, sp, #32
	bl	strlcpy
	mov	r0, #187
.L308:
	mov	r1, r9
.L304:
	ldr	r2, [r4, #100]
	add	r3, sp, #32
	bl	Alert_derate_table_update_failed
	b	.L244
.L264:
	ldr	r3, [sl, #40]
	cmp	r3, r4
	bne	.L262
	ldr	r0, .L312+144
	ldr	r2, [sl, #44]
	ldr	ip, .L312+148
	add	r2, r0, r2, asl #7
	ldrb	r1, [r2, #140]	@ zero_extendqisi2
	ldr	r0, [r4, ip]
	and	r3, r1, #15
	cmp	r3, r0
	bcs	.L262
	cmp	r3, #15
	addne	r3, r3, #1
	ldr	lr, .L312+152
	bicne	r1, r1, #15
	andne	r3, r3, #15
	orrne	r1, r3, r1
	strneb	r1, [r2, #140]
	ldrb	r9, [lr, #116]	@ zero_extendqisi2
	ands	r9, r9, #8
	beq	.L262
	ldr	r3, [sl, #44]
	ldr	r0, .L312+144
	ldrb	r1, [sl, #90]	@ zero_extendqisi2
	add	r3, r0, r3, asl #7
	ldrb	r2, [r3, #140]	@ zero_extendqisi2
	ldrb	r0, [sl, #91]	@ zero_extendqisi2
	and	r2, r2, #15
	bl	Alert_derate_table_update_station_count_idx
	mov	r9, #0
.L262:
	mov	r1, sl
	ldr	r0, .L312+76
	bl	nm_ListGetNext
	mov	sl, r0
.L274:
	cmp	sl, #0
	bne	.L264
	ldr	r2, .L312+156
	mov	r3, r9
	ldr	r0, [r2, #0]
	ldr	r9, [sp, #24]
	str	r3, [sp, #12]
	bl	xQueueGiveMutexRecursive
	ldr	r3, [sp, #12]
	cmp	r3, #0
	beq	.L265
	ldr	r2, .L312+160
	ldr	r2, [r4, r2]
	cmp	r2, #0
	beq	.L265
	add	r2, r4, fp
	add	r2, r2, #9472
	ldrb	r1, [r2, #60]	@ zero_extendqisi2
	ldr	r2, .L312+164
	ldr	r2, [r4, r2]
	cmp	r1, r2
	bcs	.L266
.L265:
	mov	r1, fp, asl #1
	add	r1, r1, #532
	ldrsh	lr, [r4, r1]
	flds	s15, [sp, #104]
	flds	s12, [sp, #100]
	fmsr	s9, lr	@ int
	flds	s11, [sp, #100]
	flds	s14, [sp, #104]
	add	r2, r4, fp
	fsitos	s13, s9
	fmsr	s9, r9	@ int
	add	r2, r2, #9472
	add	r2, r2, #60
	cmp	r3, #1
	ldrh	r0, [r4, r1]
	ldrb	ip, [r2, #0]	@ zero_extendqisi2
	fdivs	s13, s13, s15
	flds	s15, [r4, #100]	@ int
	movne	r3, #0
	fuitos	s10, s15
	fuitos	s15, s9
	fsubs	s15, s10, s15
	fmuls	s15, s11, s15
	fmacs	s15, s12, s13
	fmuls	s15, s15, s14
	ftosizs	s15, s15
	fmrs	lr, s15	@ int
	mov	lr, lr, asl #16
	mov	lr, lr, lsr #16
	strh	lr, [r4, r1]	@ movhi
	bne	.L303
	ldr	r3, .L312+164
	ldr	r3, [r4, r3]
	cmp	ip, r3
	addcc	r3, ip, #1
	bcs	.L268
.L303:
	strb	r3, [r2, #0]
.L268:
	ldr	r2, .L312+152
	add	r3, r4, fp
	ldrb	r2, [r2, #116]	@ zero_extendqisi2
	add	r3, r3, #9472
	add	r3, r3, #60
	tst	r2, #8
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	beq	.L244
	ldr	r2, .L312+168
	mov	r0, r0, asl #16
	ldr	r1, [r4, r2]
	ldr	r2, [sp, #28]
	mov	lr, lr, asl #16
	mov	r0, r0, asr #16
	mov	lr, lr, asr #16
	str	r3, [sp, #0]
	str	r0, [sp, #4]
	str	lr, [sp, #8]
	mul	r1, r2, r1
	ldr	r0, [r4, #92]
	mov	r2, fp
	mov	r3, ip
	bl	Alert_derate_table_update_successful
.L244:
.LBE47:
	ftouizs	s16, s16
	ldrh	r3, [sp, #108]
	mov	r1, #1
	strh	r3, [sp, #0]	@ movhi
	mov	r2, r1
	mov	r3, r1
	mov	r0, r4
	fsts	s16, [sp, #4]	@ int
	bl	FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines
	ldr	r3, .L312+28
	mla	r3, r8, r3, r6
	ldrb	r2, [r3, #481]	@ zero_extendqisi2
	orr	r2, r2, #64
	strb	r2, [r3, #481]
	b	.L204
.L225:
	ldr	r3, [r5, #108]
	cmp	r3, #0
	beq	.L204
	ldr	sl, .L312+28
	mul	sl, r8, sl
	add	r4, r6, sl
	add	r3, r4, #484
	ldrh	r3, [r3, #0]
	tst	r3, #8064
	beq	.L269
	add	sl, sl, #16
	add	sl, sl, r6
	mov	r0, sl
	bl	_allowed_to_acquire_expected_or_check_flow_or_make_flow_recording_line
	cmp	r0, #1
	mov	r2, r0
	bne	.L204
	ldrb	r1, [r4, #481]	@ zero_extendqisi2
	mov	r1, r1, lsr #6
	ands	r1, r1, #1
	bne	.L204
	ftouizs	s16, s16
	ldrh	r3, [sp, #108]
	mov	r0, sl
	strh	r3, [sp, #0]	@ movhi
	mov	r3, r2
	fsts	s16, [sp, #4]	@ int
	b	.L307
.L269:
	ldrb	r1, [r4, #481]	@ zero_extendqisi2
	mov	r1, r1, lsr #6
	ands	r1, r1, #1
	bne	.L204
	ldrh	r3, [sp, #108]
	add	sl, sl, #16
	strh	r3, [sp, #0]	@ movhi
	add	r0, r6, sl
	mov	r2, r1
	mov	r3, #1
	str	r1, [sp, #4]
.L307:
	bl	FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines
	ldrb	r3, [r4, #481]	@ zero_extendqisi2
	orr	r3, r3, #64
	strb	r3, [r4, #481]
.L204:
	add	r8, r8, #1
	add	r5, r5, #14208
	add	r7, r7, #14208
	cmp	r8, #4
	add	r5, r5, #16
	add	r7, r7, #16
	bne	.L270
	ldr	r3, .L312+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L312
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #112
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L207:
	mov	r4, #0
	b	.L212
.L311:
	ldr	r4, [r5, #208]
	cmp	r4, #0
	bne	.L272
	ldr	r2, [r5, #172]
	cmp	r2, #1
	movne	r9, #1
	bne	.L213
	b	.L310
.L255:
.LBB48:
.LBB44:
	ldr	r2, .L312+80
	ldr	r3, [r4, #92]
	ldr	fp, [r4, r2]
	ldr	ip, [sp, #28]
	sub	r3, r3, #2
	sub	fp, fp, #1
	mla	fp, ip, fp, r3
.LBE44:
	ldr	r3, .L312+156
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L312+4
	ldr	r3, .L312+172
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L312+76
	bl	nm_ListGetFirst
	mov	r3, #1
	str	r9, [sp, #24]
	mov	r9, r3
	mov	sl, r0
	b	.L274
.L266:
	ldr	r0, .L312+176
	bl	Alert_Message
	b	.L244
.L209:
.LBE48:
	ldrb	r4, [r3, #482]	@ zero_extendqisi2
	ands	r4, r4, #1
	beq	.L275
	b	.L311
.L313:
	.align	2
.L312:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC4
	.word	2981
	.word	system_preserves_recursive_MUTEX
	.word	2985
	.word	system_preserves
	.word	system_preserves+14124
	.word	14224
	.word	.LC12
	.word	3556
	.word	2840
	.word	.LC13
	.word	foal_irri
	.word	.LC14
	.word	list_program_data_recursive_MUTEX
	.word	2883
	.word	station_info_list_hdr
	.word	2893
	.word	.LC15
	.word	foal_irri+36
	.word	14048
	.word	7112
	.word	14056
	.word	14100
	.word	14060
	.word	14064
	.word	14104
	.word	14108
	.word	1050253722
	.word	1092616192
	.word	14052
	.word	2344
	.word	.LC16
	.word	2350
	.word	2356
	.word	.LC17
	.word	station_preserves
	.word	14032
	.word	config_c
	.word	station_preserves_recursive_MUTEX
	.word	14040
	.word	14036
	.word	14044
	.word	2519
	.word	.LC18
.LFE17:
	.size	FOAL_FLOW_check_valve_flow, .-FOAL_FLOW_check_valve_flow
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	" (MORE)\000"
.LC1:
	.ascii	" %s\000"
.LC2:
	.ascii	" (finished)\000"
.LC3:
	.ascii	"MLB : during what UNK.\000"
.LC4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/foal_flow.c\000"
.LC5:
	.ascii	"\000"
.LC6:
	.ascii	"file POC not found : %s, %u\000"
.LC7:
	.ascii	"POC : Unexpected Bermad Reed choice\000"
.LC8:
	.ascii	"Unexp flow accum gal value : %s, %u\000"
.LC9:
	.ascii	"POC content rcvd network not ready\000"
.LC10:
	.ascii	"FOAL: error in token_resp poc data : %s, %u\000"
.LC11:
	.ascii	"SYSTEM: token info error : %s, %u\000"
.LC12:
	.ascii	"FOAL_FLOW: expected only 1 valve ON during acquire\000"
.LC13:
	.ascii	"FOAL_IRRI: unable to acquire - more than 1 ON. : %s"
	.ascii	", %u\000"
.LC14:
	.ascii	"Unable to acquire expected for Sta %s - measured 0 "
	.ascii	"gpm\000"
.LC15:
	.ascii	"FLOW: not on list! : %s, %u\000"
.LC16:
	.ascii	"FOAL_FLOW: unexp out of bounds index : %s, %u\000"
.LC17:
	.ascii	"FOAL_FLOW: unexp here with one ON : %s, %u\000"
.LC18:
	.ascii	"FOAL_FLOW: not logically expected.\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI0-.LFB9
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI3-.LFB12
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI4-.LFB7
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI5-.LFB0
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x38
	.byte	0x5
	.uleb128 0x54
	.uleb128 0xa
	.byte	0x5
	.uleb128 0x52
	.uleb128 0xc
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xe
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x78
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI8-.LFB1
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x3c
	.byte	0x5
	.uleb128 0x54
	.uleb128 0xb
	.byte	0x5
	.uleb128 0x52
	.uleb128 0xd
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xf
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x94
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI11-.LFB2
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI13-.LFB3
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x82
	.uleb128 0xa
	.byte	0x81
	.uleb128 0xb
	.byte	0x80
	.uleb128 0xc
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI14-.LFB4
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x54
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI16-.LFB8
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI17-.LFB10
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI20-.LFB17
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xe
	.uleb128 0x9c
	.align	2
.LEFDE22:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_flow.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x14a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF18
	.byte	0x1
	.4byte	.LASF19
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x8ae
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x544
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x58b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0xa71
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.2byte	0xaaa
	.4byte	.LFB15
	.4byte	.LFE15
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x6df
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x8bf
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x5bb
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.byte	0xb8
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x210
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x3a3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x43b
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x4c2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x60c
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x843
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST9
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x1
	.2byte	0xb07
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x8ff
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0xb8a
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST10
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB9
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI2
	.4byte	.LFE9
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB12
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB7
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB0
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	.LCFI7
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 120
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB1
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	.LCFI10
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 148
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB2
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI12
	.4byte	.LFE2
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB3
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB4
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI15
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 84
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB10
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI19
	.4byte	.LFE10
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB17
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI22
	.4byte	.LFE17
	.2byte	0x3
	.byte	0x7d
	.sleb128 156
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x74
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"_derate_table_index\000"
.LASF9:
	.ascii	"FOAL_FLOW__B__build_system_flow_rates\000"
.LASF15:
	.ascii	"_nm_assign_new_expected_and_turn_all_stations_OFF\000"
.LASF8:
	.ascii	"FOAL_FLOW__A__build_poc_flow_rates\000"
.LASF19:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/foal_flow.c\000"
.LASF17:
	.ascii	"FOAL_FLOW_check_valve_flow\000"
.LASF4:
	.ascii	"_allowed_to_acquire_expected_or_check_flow_or_make_"
	.ascii	"flow_recording_line\000"
.LASF6:
	.ascii	"_nm_nm_make_an_alert_line_about_the_valves_ON\000"
.LASF16:
	.ascii	"_nm_nm_update_the_derate_table_and_valve_cycle_coun"
	.ascii	"t_as_needed\000"
.LASF18:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"_nm_set_the_hi_lo_limits\000"
.LASF2:
	.ascii	"_nm_okay_to_test_for_MLB_with_all_valves_OFF\000"
.LASF14:
	.ascii	"FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowr"
	.ascii	"ecordinglines\000"
.LASF11:
	.ascii	"FOAL_FLOW_load_system_info_for_distribution_to_slav"
	.ascii	"es\000"
.LASF7:
	.ascii	"_nm_take_action_for_a_detected_MLB\000"
.LASF1:
	.ascii	"_nm_okay_to_test_for_MLB_with_valves_ON\000"
.LASF13:
	.ascii	"FOAL_FLOW_check_for_MLB\000"
.LASF12:
	.ascii	"FOAL_FLOW_load_poc_info_for_distribution_to_slaves\000"
.LASF10:
	.ascii	"FOAL_FLOW_extract_poc_update_from_token_response\000"
.LASF5:
	.ascii	"_nm_nm_for_a_flow_error_make_flow_recording_lines_a"
	.ascii	"nd_turn_OFF_all_ON\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
