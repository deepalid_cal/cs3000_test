	.file	"switch_input.c"
	.text
.Ltext0:
	.section	.text.nm_SWITCH_set_name,"ax",%progbits
	.align	2
	.global	nm_SWITCH_set_name
	.type	nm_SWITCH_set_name, %function
nm_SWITCH_set_name:
.LFB0:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI0:
	str	r3, [sp, #0]
	ldr	r3, [sp, #36]
	mov	lr, r1
	str	r3, [sp, #4]
	ldr	r3, [sp, #40]
	mov	ip, r2
	str	r3, [sp, #8]
	ldr	r3, [sp, #44]
	mov	r1, #24
	str	r3, [sp, #12]
	mov	r3, #0
	str	r3, [sp, #16]
	str	r3, [sp, #20]
	str	r3, [sp, #24]
	ldr	r3, .L2
	mov	r2, lr
	str	r3, [sp, #28]
	mov	r3, ip
	bl	SHARED_set_string_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L3:
	.align	2
.L2:
	.word	.LC0
.LFE0:
	.size	nm_SWITCH_set_name, .-nm_SWITCH_set_name
	.section	.text.nm_SWITCH_set_serial_number,"ax",%progbits
	.align	2
	.global	nm_SWITCH_set_serial_number
	.type	nm_SWITCH_set_serial_number, %function
nm_SWITCH_set_serial_number:
.LFB1:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI1:
	sub	sp, sp, #40
.LCFI2:
	stmia	sp, {r2, r3}
	ldr	r3, [sp, #44]
	ldr	r2, .L5
	str	r3, [sp, #8]
	ldr	r3, [sp, #48]
	add	r0, r0, #48
	str	r3, [sp, #12]
	ldr	r3, [sp, #52]
	str	r3, [sp, #16]
	ldr	r3, [sp, #56]
	str	r3, [sp, #20]
	mov	r3, #0
	str	r3, [sp, #24]
	str	r3, [sp, #28]
	str	r3, [sp, #32]
	ldr	r3, .L5+4
	str	r3, [sp, #36]
	ldr	r3, .L5+8
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L6:
	.align	2
.L5:
	.word	50000
	.word	.LC1
	.word	99999
.LFE1:
	.size	nm_SWITCH_set_serial_number, .-nm_SWITCH_set_serial_number
	.section	.text.nm_SWITCH_set_connected,"ax",%progbits
	.align	2
	.global	nm_SWITCH_set_connected
	.type	nm_SWITCH_set_connected, %function
nm_SWITCH_set_connected:
.LFB2:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI3:
	ldr	ip, [sp, #36]
	add	r0, r0, #52
	str	ip, [sp, #0]
	ldr	ip, [sp, #40]
	str	ip, [sp, #4]
	ldr	ip, [sp, #44]
	str	ip, [sp, #8]
	ldr	ip, [sp, #48]
	str	ip, [sp, #12]
	mov	ip, #0
	str	ip, [sp, #16]
	str	ip, [sp, #20]
	str	ip, [sp, #24]
	ldr	ip, .L8
	str	ip, [sp, #28]
	bl	SHARED_set_bool_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L9:
	.align	2
.L8:
	.word	.LC2
.LFE2:
	.size	nm_SWITCH_set_connected, .-nm_SWITCH_set_connected
	.section	.text.nm_SWITCH_set_normally_closed,"ax",%progbits
	.align	2
	.global	nm_SWITCH_set_normally_closed
	.type	nm_SWITCH_set_normally_closed, %function
nm_SWITCH_set_normally_closed:
.LFB3:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI4:
	ldr	ip, [sp, #36]
	add	r0, r0, #56
	str	ip, [sp, #0]
	ldr	ip, [sp, #40]
	str	ip, [sp, #4]
	ldr	ip, [sp, #44]
	str	ip, [sp, #8]
	ldr	ip, [sp, #48]
	str	ip, [sp, #12]
	mov	ip, #0
	str	ip, [sp, #16]
	str	ip, [sp, #20]
	str	ip, [sp, #24]
	ldr	ip, .L11
	str	ip, [sp, #28]
	bl	SHARED_set_bool_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L12:
	.align	2
.L11:
	.word	.LC3
.LFE3:
	.size	nm_SWITCH_set_normally_closed, .-nm_SWITCH_set_normally_closed
	.section	.text.nm_SWITCH_set_stops_irrigation,"ax",%progbits
	.align	2
	.global	nm_SWITCH_set_stops_irrigation
	.type	nm_SWITCH_set_stops_irrigation, %function
nm_SWITCH_set_stops_irrigation:
.LFB4:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI5:
	ldr	ip, [sp, #36]
	add	r0, r0, #60
	str	ip, [sp, #0]
	ldr	ip, [sp, #40]
	str	ip, [sp, #4]
	ldr	ip, [sp, #44]
	str	ip, [sp, #8]
	ldr	ip, [sp, #48]
	str	ip, [sp, #12]
	mov	ip, #0
	str	ip, [sp, #16]
	str	ip, [sp, #20]
	str	ip, [sp, #24]
	ldr	ip, .L14
	str	ip, [sp, #28]
	bl	SHARED_set_bool_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L15:
	.align	2
.L14:
	.word	.LC4
.LFE4:
	.size	nm_SWITCH_set_stops_irrigation, .-nm_SWITCH_set_stops_irrigation
	.section	.text.nm_SWITCH_set_currently_active,"ax",%progbits
	.align	2
	.global	nm_SWITCH_set_currently_active
	.type	nm_SWITCH_set_currently_active, %function
nm_SWITCH_set_currently_active:
.LFB5:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI6:
	str	r3, [sp, #4]
	ldr	r3, [sp, #36]
	mov	ip, #0
	str	r3, [sp, #8]
	ldr	r3, [sp, #40]
	add	r0, r0, #64
	str	r3, [sp, #12]
	ldr	r3, .L17
	str	ip, [sp, #0]
	str	r3, [sp, #28]
	mov	r3, ip
	str	ip, [sp, #16]
	str	ip, [sp, #20]
	str	ip, [sp, #24]
	bl	SHARED_set_bool_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L18:
	.align	2
.L17:
	.word	.LC5
.LFE5:
	.size	nm_SWITCH_set_currently_active, .-nm_SWITCH_set_currently_active
	.section	.text.nm_SWITCH_set_irrigation_to_affect,"ax",%progbits
	.align	2
	.global	nm_SWITCH_set_irrigation_to_affect
	.type	nm_SWITCH_set_irrigation_to_affect, %function
nm_SWITCH_set_irrigation_to_affect:
.LFB6:
	@ args = 24, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI7:
	sub	sp, sp, #40
.LCFI8:
	ldr	ip, [sp, #44]
	add	r0, r0, #68
	str	ip, [sp, #0]
	ldr	ip, [sp, #48]
	str	ip, [sp, #4]
	ldr	ip, [sp, #52]
	str	ip, [sp, #8]
	ldr	ip, [sp, #56]
	str	ip, [sp, #12]
	ldr	ip, [sp, #60]
	str	ip, [sp, #16]
	ldr	ip, [sp, #64]
	str	ip, [sp, #20]
	mov	ip, #0
	str	ip, [sp, #24]
	str	ip, [sp, #28]
	str	ip, [sp, #32]
	ldr	ip, .L20
	str	ip, [sp, #36]
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L21:
	.align	2
.L20:
	.word	.LC6
.LFE6:
	.size	nm_SWITCH_set_irrigation_to_affect, .-nm_SWITCH_set_irrigation_to_affect
	.section	.text.SWITCH_get_name,"ax",%progbits
	.align	2
	.global	SWITCH_get_name
	.type	SWITCH_get_name, %function
SWITCH_get_name:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI9:
	ldr	r5, .L23
	mov	r4, r0
	ldr	r3, .L23+4
	mov	r6, r1
	ldr	r0, [r5, #0]
	mov	r1, #400
	ldr	r2, .L23+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r6
	ldr	r2, .L23+12
	mov	r0, r4
	bl	snprintf
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L24:
	.align	2
.L23:
	.word	weather_control_recursive_MUTEX
	.word	393
	.word	.LC7
	.word	.LC8
.LFE7:
	.size	SWITCH_get_name, .-SWITCH_get_name
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Switch Name\000"
.LC1:
	.ascii	"Switch Controller S/N\000"
.LC2:
	.ascii	"Switch Connected\000"
.LC3:
	.ascii	"Switch Normally Closed\000"
.LC4:
	.ascii	"Switch Stops Irrigation\000"
.LC5:
	.ascii	"Switch Currently Active\000"
.LC6:
	.ascii	"Switch Irrigation to Affect\000"
.LC7:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/switch_input.c\000"
.LC8:
	.ascii	"<name not available>\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI5-.LFB4
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI6-.LFB5
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI7-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI9-.LFB7
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/switch_input.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xc2
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x33
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x66
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x9c
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xd1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x106
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x137
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x16b
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x187
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI8
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"nm_SWITCH_set_connected\000"
.LASF6:
	.ascii	"nm_SWITCH_set_irrigation_to_affect\000"
.LASF5:
	.ascii	"nm_SWITCH_set_currently_active\000"
.LASF4:
	.ascii	"nm_SWITCH_set_stops_irrigation\000"
.LASF0:
	.ascii	"nm_SWITCH_set_name\000"
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/switch_input.c\000"
.LASF1:
	.ascii	"nm_SWITCH_set_serial_number\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"nm_SWITCH_set_normally_closed\000"
.LASF7:
	.ascii	"SWITCH_get_name\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
