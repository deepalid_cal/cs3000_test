	.file	"e_lights.c"
	.text
.Ltext0:
	.section	.text.LIGHTS_start_time_exists_in_GUI_struct,"ax",%progbits
	.align	2
	.type	LIGHTS_start_time_exists_in_GUI_struct, %function
LIGHTS_start_time_exists_in_GUI_struct:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L7
	ldr	r2, .L7+4
	add	r1, r3, #224
.L3:
	ldr	r0, [r3, #0]
	cmp	r0, r2
	bne	.L4
	ldr	r0, [r3, #4]
	cmp	r0, r2
	bne	.L5
	add	r3, r3, #16
	cmp	r3, r1
	bne	.L3
	mov	r0, #0
	bx	lr
.L4:
	mov	r0, #1
	bx	lr
.L5:
	mov	r0, #1
	bx	lr
.L8:
	.align	2
.L7:
	.word	.LANCHOR0
	.word	86400
.LFE1:
	.size	LIGHTS_start_time_exists_in_GUI_struct, .-LIGHTS_start_time_exists_in_GUI_struct
	.section	.text.LIGHTS_stop_time_exists_in_GUI_struct,"ax",%progbits
	.align	2
	.type	LIGHTS_stop_time_exists_in_GUI_struct, %function
LIGHTS_stop_time_exists_in_GUI_struct:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L15
	ldr	r2, .L15+4
	add	r1, r3, #224
.L11:
	ldr	r0, [r3, #8]
	cmp	r0, r2
	bne	.L12
	ldr	r0, [r3, #12]
	cmp	r0, r2
	bne	.L13
	add	r3, r3, #16
	cmp	r3, r1
	bne	.L11
	mov	r0, #0
	bx	lr
.L12:
	mov	r0, #1
	bx	lr
.L13:
	mov	r0, #1
	bx	lr
.L16:
	.align	2
.L15:
	.word	.LANCHOR0
	.word	86400
.LFE2:
	.size	LIGHTS_stop_time_exists_in_GUI_struct, .-LIGHTS_stop_time_exists_in_GUI_struct
	.section	.text.LIGHTS_get_menu_index_for_displayed_light,"ax",%progbits
	.align	2
	.type	LIGHTS_get_menu_index_for_displayed_light, %function
LIGHTS_get_menu_index_for_displayed_light:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L22
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L22+4
	mov	r3, #143
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L22+8
	ldr	r5, .L22+12
	ldr	r0, [r3, #0]
	ldr	r3, .L22+16
	mov	r4, #0
	ldr	r1, [r3, #0]
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	mov	r6, r0
	b	.L18
.L20:
	ldr	r3, [r5, #4]!
	cmp	r3, r6
	beq	.L19
	add	r4, r4, #1
.L18:
	bl	LIGHTS_get_list_count
	cmp	r4, r0
	bcc	.L20
	mov	r4, #0
.L19:
	ldr	r3, .L22
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L23:
	.align	2
.L22:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_LightsBoxIndex_0
	.word	.LANCHOR1-4
	.word	GuiVar_LightsOutputIndex_0
.LFE0:
	.size	LIGHTS_get_menu_index_for_displayed_light, .-LIGHTS_get_menu_index_for_displayed_light
	.section	.text.FDTO_LIGHTS_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_LIGHTS_return_to_menu, %function
FDTO_LIGHTS_return_to_menu:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L25
	ldr	r1, .L25+4
	b	FDTO_GROUP_return_to_menu
.L26:
	.align	2
.L25:
	.word	.LANCHOR2
	.word	LIGHTS_extract_and_store_changes_from_GuiVars
.LFE14:
	.size	FDTO_LIGHTS_return_to_menu, .-FDTO_LIGHTS_return_to_menu
	.section	.text.LIGHTS_copy_group_into_guivars,"ax",%progbits
	.align	2
	.type	LIGHTS_copy_group_into_guivars, %function
LIGHTS_copy_group_into_guivars:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r4, r0
	bl	LIGHTS_copy_light_struct_into_guivars
	mov	r0, r4
	bl	LIGHTS_copy_file_schedule_into_global_daily_schedule
	ldr	r3, .L28
	ldr	r0, [r3, #0]
	bl	LIGHTS_get_day_index
	bl	LIGHTS_copy_daily_schedule_to_guivars
	ldr	r3, .L28+4
	ldr	r0, [r3, #0]
	ldr	r3, .L28+8
	ldr	r1, [r3, #0]
	bl	LIGHTS_get_lights_array_index
	mov	r1, #0
	ldmfd	sp!, {r4, lr}
	b	LIGHTS_populate_group_name
.L29:
	.align	2
.L28:
	.word	.LANCHOR3
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
.LFE5:
	.size	LIGHTS_copy_group_into_guivars, .-LIGHTS_copy_group_into_guivars
	.global	__umodsi3
	.section	.text.LIGHTS_get_next_scheduled_time,"ax",%progbits
	.align	2
	.type	LIGHTS_get_next_scheduled_time, %function
LIGHTS_get_next_scheduled_time:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI2:
	mov	r5, r3
	ldr	r3, .L75
	mov	r4, r0
	mov	r6, r1
	mov	r7, r2
	mov	r1, #400
	ldr	r2, .L75+4
	ldr	r0, [r3, #0]
	ldr	r3, .L75+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r7
	bl	LIGHTS_get_light_at_this_index
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	ldrb	r0, [r4, #5]	@ zero_extendqisi2
	orr	r0, r3, r0, asl #8
	bl	LIGHTS_get_day_index
	cmp	r6, #0
	mov	r9, r0
	beq	.L31
	bl	LIGHTS_start_time_exists_in_GUI_struct
	cmp	r0, #0
	ldrne	r3, .L75+12
	ldrne	r7, [r3, r9, asl #4]
	bne	.L35
	b	.L33
.L31:
	bl	LIGHTS_stop_time_exists_in_GUI_struct
	cmp	r0, #0
	beq	.L33
.L34:
	ldr	r3, .L75+12
	add	r3, r3, r9, asl #4
	ldr	r7, [r3, #8]
.L35:
	ldr	r3, .L75+16
	cmp	r7, r3
	bhi	.L65
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r4, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r4, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	cmp	r7, r3
	ldrcsb	r3, [r4, #4]	@ zero_extendqisi2
	ldrcsb	r8, [r4, #5]	@ zero_extendqisi2
	movcs	r2, #1
	orrcs	r8, r3, r8, asl #8
	bcs	.L36
.L65:
	ldr	r8, .L75+20
	mvn	r7, #0
	mov	r2, #0
.L36:
	ldr	r1, .L75+12
	mov	r3, r9, asl #4
	add	r3, r1, r3
	cmp	r6, #0
	ldrne	r3, [r3, #4]
	ldreq	r3, [r3, #12]
	ldr	r1, .L75+16
	cmp	r3, r1
	bhi	.L39
	ldrb	r0, [r4, #1]	@ zero_extendqisi2
	ldrb	r1, [r4, #0]	@ zero_extendqisi2
	orr	r1, r1, r0, asl #8
	ldrb	r0, [r4, #2]	@ zero_extendqisi2
	orr	r1, r1, r0, asl #16
	ldrb	r0, [r4, #3]	@ zero_extendqisi2
	orr	r1, r1, r0, asl #24
	cmp	r3, r1
	bcc	.L39
	cmp	r7, r3
	ldrhib	r2, [r4, #4]	@ zero_extendqisi2
	ldrhib	r8, [r4, #5]	@ zero_extendqisi2
	orrhi	r8, r2, r8, asl #8
	bhi	.L74
	b	.L68
.L39:
	cmp	r2, #0
	bne	.L68
	add	r9, r9, #1
	mov	fp, #1
.L48:
	cmp	r6, #0
	mov	r0, r9
	mov	r1, #14
	beq	.L42
	bl	__umodsi3
	ldr	r2, .L75+12
	ldr	r3, [r2, r0, asl #4]
	b	.L43
.L42:
	bl	__umodsi3
	ldr	r3, .L75+12
	add	r0, r3, r0, asl #4
	ldr	r3, [r0, #8]
.L43:
	ldr	r2, .L75+16
	mov	r0, r9
	cmp	r3, r2
	ldrlsb	r8, [r4, #5]	@ zero_extendqisi2
	ldrlsb	r2, [r4, #4]	@ zero_extendqisi2
	movls	r7, r3
	orrls	r8, r2, r8, asl #8
	addls	r8, fp, r8
	movls	r8, r8, asl #16
	movls	r8, r8, lsr #16
	movls	sl, #1
	movhi	sl, #0
	cmp	r6, #0
	mov	r1, #14
	beq	.L45
	bl	__umodsi3
	ldr	r3, .L75+12
	add	r0, r3, r0, asl #4
	ldr	r3, [r0, #4]
	b	.L46
.L45:
	bl	__umodsi3
	ldr	r2, .L75+12
	add	r0, r2, r0, asl #4
	ldr	r3, [r0, #12]
.L46:
	ldr	r2, .L75+16
	cmp	r3, r2
	bhi	.L47
	cmp	r7, r3
	bls	.L47
	ldrb	r2, [r4, #4]	@ zero_extendqisi2
	ldrb	r8, [r4, #5]	@ zero_extendqisi2
	orr	r8, r2, r8, asl #8
	add	r8, fp, r8
	mov	r8, r8, asl #16
	mov	r8, r8, lsr #16
.L74:
	mov	r7, r3
	b	.L68
.L47:
	cmp	sl, #0
	bne	.L68
	add	fp, fp, #1
	cmp	fp, #15
	add	r9, r9, #1
	bne	.L48
	b	.L41
.L68:
	mov	sl, #1
.L41:
	ldr	r3, .L75+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #31
	bne	.L49
	ldr	r3, .L75+28
	ldr	r9, [r3, #0]
	ldr	r3, .L75+32
	ldr	r0, [r3, #0]
	bl	LIGHTS_get_day_index
	cmp	r6, #0
	ldrne	r2, .L75+36
	ldreq	r2, .L75+40
	mov	r3, #60
	ldr	r2, [r2, #0]
	add	r9, r0, r9
	mul	r3, r2, r3
	ldr	r2, .L75+16
	cmp	r3, r2
	bhi	.L52
	ldrb	r2, [r4, #4]	@ zero_extendqisi2
	ldrb	r1, [r4, #5]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #8
	cmp	r9, r2
	bne	.L53
	ldrb	r1, [r4, #1]	@ zero_extendqisi2
	ldrb	r2, [r4, #0]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #8
	ldrb	r1, [r4, #2]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #16
	ldrb	r1, [r4, #3]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #24
	cmp	r3, r2
.L53:
	bls	.L52
	cmp	r8, r9
	bcc	.L70
	bhi	.L55
	cmp	r7, r3
	bls	.L70
.L55:
	mov	r8, r9, asl #16
	mov	r8, r8, lsr #16
	mov	r7, r3
.L70:
	mov	sl, #1
.L52:
	cmp	r6, #0
	ldrne	r2, .L75+44
	ldreq	r2, .L75+48
	mov	r3, #60
	ldr	r2, [r2, #0]
	mul	r3, r2, r3
	ldr	r2, .L75+16
	cmp	r3, r2
	bhi	.L49
	ldrb	r2, [r4, #4]	@ zero_extendqisi2
	ldrb	r1, [r4, #5]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #8
	cmp	r9, r2
	bne	.L58
	ldrb	r1, [r4, #1]	@ zero_extendqisi2
	ldrb	r2, [r4, #0]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #8
	ldrb	r1, [r4, #2]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #16
	ldrb	r1, [r4, #3]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #24
	cmp	r3, r2
.L58:
	bls	.L49
	cmp	r8, r9
	bcc	.L63
	bhi	.L61
	cmp	r7, r3
	bls	.L63
.L61:
	mov	r9, r9, asl #16
	mov	r8, r9, lsr #16
	mov	r7, r3
	b	.L63
.L49:
	cmp	sl, #0
	beq	.L33
.L63:
	mov	r3, r7, lsr #8
	strb	r8, [r5, #4]
	strb	r7, [r5, #0]
	mov	r8, r8, lsr #8
	strb	r3, [r5, #1]
	mov	r3, r7, lsr #16
	mov	r7, r7, lsr #24
	strb	r8, [r5, #5]
	strb	r3, [r5, #2]
	strb	r7, [r5, #3]
	mov	r4, #1
	b	.L62
.L33:
	ldrb	r2, [r4, #5]	@ zero_extendqisi2
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	strb	r3, [r5, #4]
	mov	r3, r3, lsr #8
	strb	r3, [r5, #5]
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r4, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r4, #3]	@ zero_extendqisi2
	mov	r4, #0
	orr	r3, r3, r2, asl #24
	mov	r2, r3, lsr #8
	strb	r3, [r5, #0]
	strb	r2, [r5, #1]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r2, [r5, #2]
	strb	r3, [r5, #3]
.L62:
	ldr	r3, .L75
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L76:
	.align	2
.L75:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	299
	.word	.LANCHOR0
	.word	86399
	.word	65535
	.word	GuiLib_CurStructureNdx
	.word	.LANCHOR4
	.word	.LANCHOR3
	.word	GuiVar_LightsStartTime1InMinutes
	.word	GuiVar_LightsStopTime1InMinutes
	.word	GuiVar_LightsStartTime2InMinutes
	.word	GuiVar_LightsStopTime2InMinutes
.LFE3:
	.size	LIGHTS_get_next_scheduled_time, .-LIGHTS_get_next_scheduled_time
	.section	.text.LIGHTS_load_light_name_into_guivar,"ax",%progbits
	.align	2
	.type	LIGHTS_load_light_name_into_guivar, %function
LIGHTS_load_light_name_into_guivar:
.LFB12:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L81
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	ldr	r2, .L81+4
	sub	sp, sp, #48
.LCFI4:
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L81+8
	bl	xQueueTakeMutexRecursive_debug
	bl	LIGHTS_get_list_count
	cmp	r4, r0
	bcs	.L78
	ldr	r5, .L81+12
	ldr	r0, [r5, r4, asl #2]
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	mov	r0, sp
	bl	strlcpy
	ldr	r0, .L81+16
	bl	strlen
	ldr	r1, .L81+16
	mov	r2, r0
	mov	r0, sp
	bl	strncmp
	cmp	r0, #0
	bne	.L79
	ldr	r0, [r5, r4, asl #2]
	bl	LIGHTS_get_output_index
	mov	r1, #49
	ldr	r2, .L81+20
	add	r3, r0, #1
	ldr	r0, .L81+24
	bl	snprintf
	b	.L80
.L79:
	mov	r1, sp
	mov	r2, #49
	ldr	r0, .L81+24
	bl	strlcpy
	b	.L80
.L78:
	ldr	r0, .L81+4
	ldr	r1, .L81+28
	bl	Alert_group_not_found_with_filename
.L80:
	ldr	r3, .L81
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, pc}
.L82:
	.align	2
.L81:
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	1250
	.word	.LANCHOR1
	.word	LIGHTS_DEFAULT_NAME
	.word	.LC1
	.word	GuiVar_itmGroupName
	.word	1267
.LFE12:
	.size	LIGHTS_load_light_name_into_guivar, .-LIGHTS_load_light_name_into_guivar
	.section	.text.LIGHTS_process_this_time.isra.0,"ax",%progbits
	.align	2
	.type	LIGHTS_process_this_time.isra.0, %function
LIGHTS_process_this_time.isra.0:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI5:
	mov	r5, r2
	ldr	r2, [r1, #0]
	mov	r4, r1
	cmp	r2, #0
	cmpeq	r0, #80
	movne	r6, #0
	moveq	r6, #1
	bne	.L84
	bl	good_key_beep
	mov	r3, #1440
	str	r3, [r4, #0]
	b	.L85
.L84:
	cmp	r2, #1440
	cmpeq	r0, #84
	movne	r2, #0
	moveq	r2, #1
	bne	.L86
	bl	good_key_beep
	str	r6, [r4, #0]
	b	.L85
.L86:
	mov	r3, #10
	str	r3, [sp, #0]
	mov	r3, #1440
	str	r2, [sp, #4]
	bl	process_uns32
.L85:
	ldr	r3, [r4, #0]
	subs	r3, r3, #1440
	movne	r3, #1
	str	r3, [r5, #0]
	ldr	r3, .L87
	ldr	r0, [r3, #0]
	bl	LIGHTS_get_day_index
	bl	LIGHTS_copy_guivars_to_daily_schedule
	mov	r0, #0
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Redraw_Screen
.L88:
	.align	2
.L87:
	.word	.LANCHOR3
.LFE17:
	.size	LIGHTS_process_this_time.isra.0, .-LIGHTS_process_this_time.isra.0
	.section	.text.LIGHTS_update_next_scheduled_date_and_time_panel,"ax",%progbits
	.align	2
	.global	LIGHTS_update_next_scheduled_date_and_time_panel
	.type	LIGHTS_update_next_scheduled_date_and_time_panel, %function
LIGHTS_update_next_scheduled_date_and_time_panel:
.LFB4:
	@ args = 0, pretend = 0, frame = 80
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI6:
	sub	sp, sp, #88
.LCFI7:
	bl	LIGHTS_start_time_exists_in_GUI_struct
	mov	r4, r0
	bl	LIGHTS_stop_time_exists_in_GUI_struct
	adds	r2, r4, #0
	movne	r2, #1
	ldr	r3, .L94
	cmp	r0, #0
	cmpne	r4, #0
	beq	.L90
	ldr	r7, .L94+4
	ldr	r6, .L94+8
	mov	r4, #0
	str	r4, [r3, #0]
	add	r0, sp, #80
	bl	EPSON_obtain_latest_time_and_date
	ldr	r1, [r6, #0]
	ldr	r0, [r7, #0]
	bl	LIGHTS_get_lights_array_index
	add	r3, sp, #64
	mov	r1, #1
	mov	r5, #200
	mov	r2, r0
	add	r0, sp, #80
	bl	LIGHTS_get_next_scheduled_time
	ldr	r2, [sp, #64]
	mov	r3, r4
	add	r0, sp, #56
	mov	r1, #8
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldrh	r2, [sp, #68]
	mov	r1, #48
	mov	r3, #100
	str	r5, [sp, #0]
	mov	r8, r0
	add	r0, sp, #8
	bl	GetDateStr
	mov	r3, r8
	ldr	r2, .L94+12
	mov	r1, #33
	str	r0, [sp, #0]
	ldr	r0, .L94+16
	bl	snprintf
	ldr	r1, [r6, #0]
	ldr	r0, [r7, #0]
	bl	LIGHTS_get_lights_array_index
	mov	r1, r4
	add	r3, sp, #72
	mov	r2, r0
	add	r0, sp, #80
	bl	LIGHTS_get_next_scheduled_time
	ldr	r2, [sp, #72]
	mov	r3, r4
	add	r0, sp, #56
	mov	r1, #8
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldrh	r2, [sp, #76]
	mov	r1, #48
	mov	r3, #100
	str	r5, [sp, #0]
	mov	r4, r0
	add	r0, sp, #8
	bl	GetDateStr
	mov	r1, #33
	ldr	r2, .L94+12
	mov	r3, r4
	str	r0, [sp, #0]
	ldr	r0, .L94+20
	bl	snprintf
	b	.L89
.L90:
	cmp	r0, #0
	movne	r0, #0
	andeq	r0, r2, #1
	cmp	r0, #0
	movne	r2, #2
	moveq	r2, #1
	str	r2, [r3, #0]
.L89:
	add	sp, sp, #88
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L95:
	.align	2
.L94:
	.word	GuiVar_LightScheduleState
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	.LC2
	.word	GuiVar_LightScheduledStart
	.word	GuiVar_LightScheduledStop
.LFE4:
	.size	LIGHTS_update_next_scheduled_date_and_time_panel, .-LIGHTS_update_next_scheduled_date_and_time_panel
	.section	.text.FDTO_LIGHTS_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_LIGHTS_draw_menu
	.type	FDTO_LIGHTS_draw_menu, %function
FDTO_LIGHTS_draw_menu:
.LFB15:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI8:
	ldr	r4, .L105
	sub	sp, sp, #60
.LCFI9:
	ldr	r3, .L105+4
	mov	r1, #400
	ldr	r2, .L105+8
	mov	r8, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
.LBB12:
	mov	r1, #0
	mov	r2, #192
	ldr	r0, .L105+12
	bl	memset
	mov	r5, #0
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L105+8
	ldr	r3, .L105+16
	bl	xQueueTakeMutexRecursive_debug
	mov	r4, r5
	ldr	r6, .L105+12
	b	.L97
.L99:
	mov	r0, r5
	bl	LIGHTS_get_light_at_this_index
	subs	r7, r0, #0
	beq	.L98
	bl	LIGHTS_get_physically_available
	cmp	r0, #0
	strne	r7, [r6, r4, asl #2]
	addne	r4, r4, #1
.L98:
	add	r5, r5, #1
.L97:
	bl	LIGHTS_get_list_count
	cmp	r5, r0
	bcc	.L99
	ldr	r3, .L105
	ldr	r6, .L105+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE12:
	cmp	r8, #1
	bne	.L100
	ldr	r3, .L105+24
	mov	r5, #0
	str	r5, [r3, #0]
	ldr	r3, .L105+28
	str	r5, [r6, #0]
	str	r5, [r3, #0]
.LBB13:
	ldr	r3, .L105+12
.LBE13:
	ldr	r6, .L105+32
	ldr	r0, [r3, #0]
	bl	LIGHTS_get_index_using_ptr_to_light_struct
.LBB14:
	ldr	r7, .L105+36
.LBE14:
	str	r0, [r6, #0]
.LBB15:
	add	r0, sp, #52
	bl	EPSON_obtain_latest_time_and_date
	ldrh	r2, [sp, #56]
	mov	r3, #200
	str	r2, [r7, #0]
	mov	r1, #48
	str	r3, [sp, #0]
	add	r0, sp, #4
	mov	r3, #100
	bl	GetDateStr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L105+40
	bl	strlcpy
	ldr	r0, [r7, #0]
	ldrh	sl, [sp, #56]
	bl	LIGHTS_get_day_index
	ldr	r3, .L105+44
	rsb	r0, r0, sl
	str	r0, [r3, #0]
.LBE15:
	ldr	r0, [r6, #0]
	bl	LIGHTS_copy_group_into_guivars
	ldr	r3, .L105+48
	str	r5, [r3, #0]
	mvn	r5, #0
	b	.L101
.L100:
	ldr	r3, .L105+52
	mov	r0, #0
	ldrsh	r5, [r3, #0]
	bl	GuiLib_ScrollBox_GetTopLine
	ldr	r3, .L105+56
	str	r0, [r6, #0]
	ldr	r0, [r3, #0]
	ldr	r3, .L105+60
	ldr	r1, [r3, #0]
	bl	LIGHTS_get_lights_array_index
	bl	LIGHTS_copy_light_struct_into_guivars
.L101:
	bl	LIGHTS_update_next_scheduled_date_and_time_panel
	mov	r1, r5
	mov	r2, #1
	mov	r0, #31
	bl	GuiLib_ShowScreen
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	bl	LIGHTS_get_menu_index_for_displayed_light
	ldr	r3, .L105+24
	mov	r2, r4, asl #16
	ldr	r3, [r3, #0]
	mov	r2, r2, asr #16
	cmp	r3, #1
	ldr	r3, .L105+20
	mov	r5, r0
	bne	.L102
	ldrsh	r3, [r3, #0]
	ldr	r1, .L105+64
	str	r3, [sp, #0]
	mov	r0, #0
	mvn	r3, #0
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	mov	r1, r5, asl #16
	mov	r1, r1, asr #16
	mov	r0, #0
	bl	GuiLib_ScrollBox_SetIndicator
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
	b	.L103
.L102:
	cmp	r8, #1
	bne	.L104
	ldr	r1, .L105+32
	ldr	r1, [r1, #0]
	cmp	r1, #12
	subhi	r1, r1, #11
	strhi	r1, [r3, #0]
.L104:
	ldr	r1, .L105+20
	mov	r3, r5, asl #16
	ldrsh	r1, [r1, #0]
	mov	r0, #0
	str	r1, [sp, #0]
	mov	r3, r3, asr #16
	ldr	r1, .L105+64
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L103:
	ldr	r3, .L105
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	GuiLib_Refresh
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L106:
	.align	2
.L105:
	.word	list_lights_recursive_MUTEX
	.word	1351
	.word	.LC0
	.word	.LANCHOR1
	.word	1208
	.word	.LANCHOR5
	.word	.LANCHOR2
	.word	.LANCHOR6
	.word	g_GROUP_list_item_index
	.word	.LANCHOR3
	.word	GuiVar_LightsDate
	.word	.LANCHOR4
	.word	.LANCHOR7
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_LightsBoxIndex_0
	.word	GuiVar_LightsOutputIndex_0
	.word	LIGHTS_load_light_name_into_guivar
.LFE15:
	.size	FDTO_LIGHTS_draw_menu, .-FDTO_LIGHTS_draw_menu
	.section	.text.LIGHTS_process_menu,"ax",%progbits
	.align	2
	.global	LIGHTS_process_menu
	.type	LIGHTS_process_menu, %function
LIGHTS_process_menu:
.LFB16:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI10:
	ldr	r5, .L176
	mov	r4, r1
	ldr	r1, [r5, #0]
	sub	sp, sp, #64
.LCFI11:
	cmp	r1, #1
	mov	r6, r0
	bne	.L108
.LBB24:
.LBB25:
	ldr	r3, .L176+4
	ldrsh	r2, [r3, #0]
	ldr	r3, .L176+8
	cmp	r2, r3
	bne	.L160
	cmp	r0, #67
	beq	.L111
	cmp	r0, #2
	bne	.L112
	ldr	r3, .L176+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L112
.L111:
	bl	LIGHTS_extract_and_store_group_name_from_GuiVars
.L112:
	mov	r0, r6
	mov	r1, r4
	ldr	r2, .L176+16
	bl	KEYBOARD_process_key
	b	.L107
.L160:
	cmp	r0, #4
	beq	.L119
	bhi	.L123
	cmp	r0, #1
	beq	.L116
	bcc	.L115
	cmp	r0, #2
	beq	.L117
	cmp	r0, #3
	bne	.L151
	b	.L173
.L123:
	cmp	r0, #67
	beq	.L121
	bhi	.L124
	cmp	r0, #16
	beq	.L120
	cmp	r0, #20
	bne	.L151
	b	.L120
.L124:
	cmp	r0, #80
	beq	.L122
	cmp	r0, #84
	bne	.L151
	b	.L122
.L117:
	ldr	r3, .L176+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L147
	mov	r0, #158
	mov	r1, #27
	bl	GROUP_process_show_keyboard
	b	.L107
.L120:
.LBB26:
	bl	LIGHTS_get_menu_index_for_displayed_light
	cmp	r6, #20
	mov	r4, r0
	mov	r5, r4, asl #16
	mov	r0, #0
	mov	r5, r5, asr #16
	bne	.L127
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	mov	r1, r5
	bl	SCROLL_BOX_to_line
	ldr	r3, .L176+20
	add	r4, r4, #1
	ldr	r3, [r3, r4, asl #2]
	cmp	r3, #0
	movne	r0, #0
	movne	r1, r0
	bne	.L171
	b	.L130
.L127:
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	mov	r1, r5
	bl	SCROLL_BOX_to_line
	ldr	r3, .L176+20
	sub	r4, r4, #1
	ldr	r3, [r3, r4, asl #2]
	cmp	r3, #0
	beq	.L130
	mov	r0, #0
	mov	r1, #4
.L171:
	bl	SCROLL_BOX_up_or_down
.L130:
	ldr	r5, .L176+20
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	ldr	r3, [r5, r4, asl #2]
	cmp	r3, #0
	beq	.L131
	bl	LIGHTS_extract_and_store_changes_from_GuiVars
	ldr	r0, [r5, r4, asl #2]
	bl	LIGHTS_get_index_using_ptr_to_light_struct
	ldr	r3, .L176+24
	str	r0, [r3, #0]
	bl	LIGHTS_copy_group_into_guivars
	b	.L132
.L131:
	bl	bad_key_beep
.L132:
	mov	r0, #0
	b	.L166
.L122:
.LBE26:
	ldr	r3, .L176+12
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L147
.L139:
	.word	.L134
	.word	.L135
	.word	.L136
	.word	.L137
	.word	.L138
.L134:
.LBB27:
	ldr	r4, .L176+28
	add	r0, sp, #56
	bl	EPSON_obtain_latest_time_and_date
	ldr	r0, [r4, #0]
	bl	LIGHTS_get_day_index
	bl	LIGHTS_copy_guivars_to_daily_schedule
	ldrh	r2, [sp, #60]
	mov	r3, #1
	mov	r5, #0
	stmia	sp, {r3, r5}
	mov	r0, r6
	mov	r1, r4
	add	r3, r2, #13
	bl	process_uns32
	mov	r3, #200
	str	r3, [sp, #0]
	add	r0, sp, #8
	mov	r3, #100
	ldr	r2, [r4, #0]
	mov	r1, #48
	bl	GetDateStr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L176+32
	bl	strlcpy
	ldr	r0, [r4, #0]
	bl	LIGHTS_get_day_index
	bl	LIGHTS_copy_daily_schedule_to_guivars
	b	.L172
.L135:
.LBE27:
	mov	r0, r6
	ldr	r1, .L176+36
	ldr	r2, .L176+40
	b	.L167
.L136:
	ldr	r1, .L176+44
	ldr	r2, .L176+48
	mov	r0, r6
.L167:
	bl	LIGHTS_process_this_time.isra.0
	b	.L107
.L137:
	mov	r0, r6
	ldr	r1, .L176+52
	ldr	r2, .L176+56
	b	.L167
.L138:
	mov	r0, r6
	ldr	r1, .L176+60
	ldr	r2, .L176+64
	b	.L167
.L119:
	ldr	r3, .L176+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	beq	.L142
	cmp	r3, #5
	beq	.L143
	cmp	r3, #3
	mov	r0, r1
	moveq	r1, r1
	bne	.L163
	b	.L170
.L142:
	mov	r0, #2
	b	.L170
.L143:
	mov	r0, #3
	b	.L170
.L115:
	ldr	r3, .L176+12
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #2
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L144
.L148:
	.word	.L145
	.word	.L146
	.word	.L147
	.word	.L147
.L145:
	mov	r0, #4
	b	.L169
.L146:
	mov	r0, #5
.L169:
	mov	r1, #1
.L170:
	bl	CURSOR_Select
	b	.L107
.L147:
	bl	bad_key_beep
	b	.L107
.L144:
	mov	r0, #1
	b	.L168
.L116:
	ldr	r3, .L176+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L121
.L163:
	bl	CURSOR_Up
	b	.L107
.L173:
	mov	r0, r1
.L168:
	bl	CURSOR_Down
	b	.L107
.L121:
	ldr	r0, .L176+68
	bl	KEY_process_BACK_from_editing_screen
	b	.L107
.L108:
.LBE25:
.LBE24:
	cmp	r0, #4
	beq	.L157
	bhi	.L156
	cmp	r0, #0
	beq	.L157
	cmp	r0, #2
	bcc	.L151
	b	.L174
.L156:
	cmp	r0, #20
	beq	.L159
	cmp	r0, #67
	beq	.L155
	cmp	r0, #16
	bne	.L151
	b	.L175
.L174:
	bl	good_key_beep
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r6, .L176+72
	ldr	r4, .L176+76
	mov	r1, #400
	ldr	r2, .L176+80
	ldr	r3, .L176+84
	str	r0, [r6, #0]
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L176+20
.LBB28:
	ldr	r2, [r6, #0]
.LBE28:
	ldr	r0, [r3, r2, asl #2]
	bl	LIGHTS_get_index_using_ptr_to_light_struct
	ldr	r3, .L176+24
	str	r0, [r3, #0]
	mov	r3, #1
	str	r3, [r5, #0]
	ldr	r3, .L176+12
	mov	r5, #0
	strh	r5, [r3, #0]	@ movhi
	b	.L165
.L175:
	mov	r1, #4
	b	.L152
.L159:
	mov	r1, #0
	b	.L152
.L157:
	mov	r1, r6
.L152:
	mov	r0, #0
	bl	SCROLL_BOX_up_or_down
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r5, .L176+72
	ldr	r4, .L176+76
	mov	r1, #400
	ldr	r2, .L176+80
	ldr	r3, .L176+88
	str	r0, [r5, #0]
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
.LBB29:
	ldr	r2, [r5, #0]
.LBE29:
	ldr	r3, .L176+20
	mov	r5, #0
	ldr	r0, [r3, r2, asl #2]
	bl	LIGHTS_get_index_using_ptr_to_light_struct
	ldr	r3, .L176+24
	str	r0, [r3, #0]
	bl	LIGHTS_copy_group_into_guivars
	ldr	r3, .L176+92
	str	r5, [r3, #0]
.L165:
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
.L172:
	mov	r0, r5
.L166:
	bl	Redraw_Screen
	b	.L107
.L155:
	ldr	r3, .L176+96
	ldr	r2, .L176+100
	ldr	r3, [r3, #0]
	mov	r1, #36
	mla	r3, r1, r3, r2
	ldr	r2, [r3, #4]
	ldr	r3, .L176+104
	str	r2, [r3, #0]
.L151:
	mov	r0, r6
	mov	r1, r4
	bl	KEY_process_global_keys
.L107:
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, pc}
.L177:
	.align	2
.L176:
	.word	.LANCHOR2
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_LIGHTS_draw_menu
	.word	.LANCHOR1
	.word	g_GROUP_list_item_index
	.word	.LANCHOR3
	.word	GuiVar_LightsDate
	.word	GuiVar_LightsStartTime1InMinutes
	.word	GuiVar_LightsStartTimeEnabled1
	.word	GuiVar_LightsStartTime2InMinutes
	.word	GuiVar_LightsStartTimeEnabled2
	.word	GuiVar_LightsStopTime1InMinutes
	.word	GuiVar_LightsStopTimeEnabled1
	.word	GuiVar_LightsStopTime2InMinutes
	.word	GuiVar_LightsStopTimeEnabled2
	.word	FDTO_LIGHTS_return_to_menu
	.word	.LANCHOR6
	.word	list_lights_recursive_MUTEX
	.word	.LC0
	.word	1466
	.word	1500
	.word	.LANCHOR7
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE16:
	.size	LIGHTS_process_menu, .-LIGHTS_process_menu
	.global	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY
	.global	g_LIGHTS_numeric_date
	.section	.bss.g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY, %object
	.size	g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY, 224
g_LIGHTS_daily_schedule_in_seconds__for_UI_ONLY:
	.space	224
	.section	.bss.LIGHTS_MENU_items,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	LIGHTS_MENU_items, %object
	.size	LIGHTS_MENU_items, 192
LIGHTS_MENU_items:
	.space	192
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_lights.c\000"
.LC1:
	.ascii	"Lights L%d\000"
.LC2:
	.ascii	"%s, %s\000"
	.section	.bss.g_LIGHTS_top_line,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	g_LIGHTS_top_line, %object
	.size	g_LIGHTS_top_line, 4
g_LIGHTS_top_line:
	.space	4
	.section	.bss.g_LIGHTS_prev_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	g_LIGHTS_prev_cursor_pos, %object
	.size	g_LIGHTS_prev_cursor_pos, 4
g_LIGHTS_prev_cursor_pos:
	.space	4
	.section	.bss.g_LIGHTS_numeric_date,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_LIGHTS_numeric_date, %object
	.size	g_LIGHTS_numeric_date, 4
g_LIGHTS_numeric_date:
	.space	4
	.section	.bss.g_LIGHTS_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_LIGHTS_editing_group, %object
	.size	g_LIGHTS_editing_group, 4
g_LIGHTS_editing_group:
	.space	4
	.section	.bss.g_LIGHTS_first_sundays_date,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_LIGHTS_first_sundays_date, %object
	.size	g_LIGHTS_first_sundays_date, 4
g_LIGHTS_first_sundays_date:
	.space	4
	.section	.bss.g_LIGHTS_current_list_item_index,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	g_LIGHTS_current_list_item_index, %object
	.size	g_LIGHTS_current_list_item_index, 4
g_LIGHTS_current_list_item_index:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI1-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI3-.LFB12
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI5-.LFB17
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI6-.LFB4
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x70
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI8-.LFB15
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI10-.LFB16
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE20:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_lights.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x133
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF17
	.byte	0x1
	.4byte	.LASF18
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x3dc
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x4a8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x508
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x385
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.byte	0xb2
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.byte	0xdc
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x1
	.byte	0x81
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x524
	.4byte	.LFB14
	.4byte	.LFE14
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x274
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST1
	.uleb128 0x6
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x111
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x6
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x4dc
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST3
	.uleb128 0x7
	.4byte	0x21
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x223
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x53f
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST6
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x415
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x32e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x3a9
	.byte	0x1
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x5aa
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST7
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB5
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB12
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI4
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB17
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI7
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 112
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB15
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI9
	.4byte	.LFE15
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB16
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI11
	.4byte	.LFE16
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF8:
	.ascii	"LIGHTS_copy_group_into_guivars\000"
.LASF16:
	.ascii	"LIGHTS_process_menu\000"
.LASF15:
	.ascii	"LIGHTS_process_daily_schedule_change\000"
.LASF3:
	.ascii	"LIGHTS_set_start_of_14_day_schedule\000"
.LASF9:
	.ascii	"LIGHTS_get_next_scheduled_time\000"
.LASF1:
	.ascii	"LIGHTS_populate_pointers_of_physically_available_li"
	.ascii	"ghts_for_display\000"
.LASF18:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_lights.c\000"
.LASF13:
	.ascii	"LIGHTS_process_group\000"
.LASF17:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF11:
	.ascii	"LIGHTS_update_next_scheduled_date_and_time_panel\000"
.LASF5:
	.ascii	"LIGHTS_stop_time_exists_in_GUI_struct\000"
.LASF6:
	.ascii	"LIGHTS_get_menu_index_for_displayed_light\000"
.LASF0:
	.ascii	"LIGHTS_process_this_time\000"
.LASF14:
	.ascii	"LIGHTS_process_NEXT_and_PREV\000"
.LASF12:
	.ascii	"FDTO_LIGHTS_draw_menu\000"
.LASF4:
	.ascii	"LIGHTS_start_time_exists_in_GUI_struct\000"
.LASF10:
	.ascii	"LIGHTS_load_light_name_into_guivar\000"
.LASF7:
	.ascii	"FDTO_LIGHTS_return_to_menu\000"
.LASF2:
	.ascii	"LIGHTS_get_ptr_to_physically_available_light\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
