	.file	"GuiLib.c"
	.text
.Ltext0:
	.section	.text.OrderCoord,"ax",%progbits
	.align	2
	.type	OrderCoord, %function
OrderCoord:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldrsh	ip, [r1, #0]
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldrsh	r4, [r0, #0]
	ldrh	r3, [r0, #0]
	ldrh	r2, [r1, #0]
	cmp	r4, ip
.LBB118:
	strgth	r2, [r0, #0]	@ movhi
.LBE118:
	movle	r0, #0
.LBB119:
	strgth	r3, [r1, #0]	@ movhi
.LBE119:
	movgt	r0, #1
	ldmfd	sp!, {r4, pc}
.LFE1:
	.size	OrderCoord, .-OrderCoord
	.section	.text.MarkDisplayBoxRepaint,"ax",%progbits
	.align	2
	.type	MarkDisplayBoxRepaint, %function
MarkDisplayBoxRepaint:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	add	ip, r0, #3
	cmp	r0, #0
	movlt	r0, ip
	add	ip, r2, #3
	cmp	r2, #0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	ldr	r4, .L10
	movlt	r2, ip
	mov	r2, r2, asr #2
	mov	r2, r2, asl #17
	mov	r0, r0, asr #2
	add	r2, r2, #65536
	add	r4, r4, r1, asl #2
	mov	r0, r0, asl #1
	mov	r2, r2, asr #16
	mov	ip, #0
	mov	r5, r4
	b	.L5
.L9:
	add	r6, r4, ip
	ldrsh	r6, [r6, #2]
	cmn	r6, #1
	beq	.L6
	ldrsh	r6, [r4, ip]
	cmp	r6, r0
	ble	.L7
.L6:
	strh	r0, [r5, ip]	@ movhi
.L7:
	add	r6, r5, ip
	ldrsh	r7, [r6, #2]
	add	r1, r1, #1
	mov	r1, r1, asl #16
	cmp	r7, r2
	mov	r1, r1, asr #16
	add	ip, ip, #4
	strlth	r2, [r6, #2]	@ movhi
.L5:
	cmp	r1, r3
	ble	.L9
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L11:
	.align	2
.L10:
	.word	.LANCHOR0
.LFE4:
	.size	MarkDisplayBoxRepaint, .-MarkDisplayBoxRepaint
	.section	.text.HorzLine,"ax",%progbits
	.align	2
	.type	HorzLine, %function
HorzLine:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	add	r4, r0, r0, lsr #31
	mov	r4, r4, asl #15
	mov	r4, r4, lsr #16
	add	r7, r4, #1
	mov	r4, r4, asl #16
	mov	r4, r4, asr #16
	add	r6, r1, r1, lsr #31
	ands	r4, r4, #-2147483647
	mov	r6, r6, asl #15
	submi	r4, r4, #1
	mov	r6, r6, lsr #16
	mvnmi	r4, r4, asl #31
	mov	ip, r6, asl #16
	mvnmi	r4, r4, lsr #31
	mov	ip, ip, asr #16
	addmi	r4, r4, #1
	ands	ip, ip, #-2147483647
	submi	ip, ip, #1
	mvnmi	ip, ip, asl #31
	mov	r7, r7, asl #16
	mvn	r5, #1
	mvnmi	ip, ip, lsr #31
	mov	r7, r7, lsr #16
	mla	r4, r5, r4, r7
	add	r8, r6, #1
	addmi	ip, ip, #1
	mla	ip, r5, ip, r8
	mov	r4, r4, asl #16
	mov	ip, ip, asl #16
	mov	r4, r4, lsr #16
	mov	r8, ip, lsr #16
	cmp	ip, r4, asl #16
	ldr	ip, .L26
	mov	r5, r4, asl #16
	mov	r5, r5, asr #16
	ldr	ip, [ip, #0]
	bne	.L15
	ands	r1, r1, #-2147483647
	submi	r1, r1, #1
	mvnmi	r1, r1, asl #31
	mvnmi	r1, r1, lsr #31
	addmi	r1, r1, #1
	ldr	r4, .L26+4
	ands	r0, r0, #-2147483647
	submi	r0, r0, #1
	mov	r1, r1, asl #16
	mvnmi	r0, r0, asl #31
	mov	r1, r1, asr #16
	ldrb	r4, [r4, r1]	@ zero_extendqisi2
	mvnmi	r0, r0, lsr #31
	ldr	r1, .L26+8
	addmi	r0, r0, #1
	mov	r0, r0, asl #16
	ldrb	r1, [r1, r0, asr #16]	@ zero_extendqisi2
	mov	r0, #160
	and	r1, r4, r1
	mla	r2, r0, r2, r5
	ldr	r4, .L26+12
	ldrb	r0, [ip, r2]	@ zero_extendqisi2
	ldrb	r3, [r4, r3]	@ zero_extendqisi2
	bic	r0, r0, r1
	and	r1, r1, r3
	orr	r1, r0, r1
	strb	r1, [ip, r2]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L15:
	ands	r0, r0, #-2147483647
	submi	r0, r0, #1
	mvnmi	r0, r0, asl #31
	mvnmi	r0, r0, lsr #31
	ldr	r4, .L26+8
	addmi	r0, r0, #1
	mov	r0, r0, asl #16
	ldrb	r0, [r4, r0, asr #16]	@ zero_extendqisi2
	ldr	r4, .L26+12
	mov	r6, r6, asl #16
	ldrb	r3, [r4, r3]	@ zero_extendqisi2
	mov	r4, #160
	mul	r4, r2, r4
	mov	r6, r6, asr #16
	add	r5, r4, r5
	ldrb	sl, [ip, r5]	@ zero_extendqisi2
	bic	sl, sl, r0
	and	r0, r3, r0
	orr	r0, sl, r0
	strb	r0, [ip, r5]
	mov	r0, r7
	mov	r7, r4
	b	.L20
.L23:
	mov	r5, r4, lsr #16
	tst	r4, #65536
	addeq	r5, r5, #1
	subne	r5, r5, #1
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
	add	r0, r0, #1
	mov	r5, r5, asl #16
	mov	r0, r0, asl #16
	add	r5, r7, r5, asr #16
	mov	r0, r0, lsr #16
	strb	r3, [ip, r5]
.L20:
	mov	r4, r0, asl #16
	cmp	r6, r4, asr #16
	bgt	.L23
	ands	r1, r1, #-2147483647
	submi	r1, r1, #1
	mvnmi	r1, r1, asl #31
	mvnmi	r1, r1, lsr #31
	ldr	r0, .L26+4
	addmi	r1, r1, #1
	mov	r8, r8, asl #16
	mov	r1, r1, asl #16
	ldrb	r1, [r0, r1, asr #16]	@ zero_extendqisi2
	mov	r8, r8, asr #16
	mov	r0, #160
	mla	r2, r0, r2, r8
	ldrb	r0, [ip, r2]	@ zero_extendqisi2
	bic	r0, r0, r1
	and	r1, r3, r1
	orr	r3, r0, r1
	strb	r3, [ip, r2]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L27:
	.align	2
.L26:
	.word	guilib_display_ptr
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
.LFE8:
	.size	HorzLine, .-HorzLine
	.section	.text.VertLine,"ax",%progbits
	.align	2
	.type	VertLine, %function
VertLine:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	add	ip, r0, r0, lsr #31
	mov	ip, ip, asl #15
	mov	ip, ip, lsr #16
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI3:
	add	r4, ip, #1
	mov	ip, ip, asl #16
	mov	ip, ip, asr #16
	ands	ip, ip, #-2147483647
	submi	ip, ip, #1
	mvnmi	ip, ip, asl #31
	mvnmi	ip, ip, lsr #31
	addmi	ip, ip, #1
	tst	r0, #1
	sub	ip, r4, ip, asl #1
	moveq	r3, r3, asl #4
	ldr	r4, .L35
	mov	ip, ip, asl #16
	andeq	r3, r3, #255
	ands	r0, r0, #-2147483647
	submi	r0, r0, #1
	mov	ip, ip, lsr #16
	mvnmi	r0, r0, asl #31
	mov	ip, ip, asl #16
	ldr	r5, [r4, #0]
	mvnmi	r0, r0, lsr #31
	mov	ip, ip, asr #16
	mov	r6, #160	@ movhi
	addmi	r0, r0, #1
	smlabb	ip, r1, r6, ip
	mov	r0, r0, asl #16
	add	ip, r5, ip
	mov	r0, r0, asr #16
	mov	r4, #0
	ldr	r5, .L35+4
	b	.L32
.L33:
	ldrb	r6, [ip, r4]	@ zero_extendqisi2
	ldrb	r7, [r5, r0]	@ zero_extendqisi2
	add	r1, r1, #1
	bic	r6, r6, r7
	orr	r6, r6, r3
	mov	r1, r1, asl #16
	strb	r6, [ip, r4]
	mov	r1, r1, asr #16
	add	r4, r4, #160
.L32:
	cmp	r1, r2
	ble	.L33
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L36:
	.align	2
.L35:
	.word	guilib_display_ptr
	.word	.LANCHOR4
.LFE9:
	.size	VertLine, .-VertLine
	.global	__umodsi3
	.global	__udivsi3
	.section	.text.ConvertIntToStr,"ax",%progbits
	.align	2
	.type	ConvertIntToStr, %function
ConvertIntToStr:
.LFB60:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI4:
	sub	sp, sp, #28
.LCFI5:
	mov	r5, r0
	mov	r4, r1
	mov	r6, r2
	mov	r0, sp
	ldr	r1, .L43
	mov	r2, #16
	bl	memcpy
	mov	r7, #0
.L38:
	add	r0, sp, #28
	add	r8, r0, r7
	mov	r1, r6
	mov	r0, r5
	bl	__umodsi3
	add	r3, sp, #28
	mov	r1, r6
	add	r7, r7, #1
	and	r7, r7, #255
	and	r0, r0, #255
	add	r0, r3, r0
	ldrb	r3, [r0, #-28]	@ zero_extendqisi2
	mov	r0, r5
	strb	r3, [r8, #-12]
	bl	__udivsi3
	subs	r5, r0, #0
	bne	.L38
	cmp	r7, #16
	mov	r2, r7
	movls	r3, r4
	bls	.L40
	b	.L39
.L41:
	sub	r7, r7, #1
	and	r7, r7, #255
	add	r0, sp, #28
	add	r1, r0, r7
	ldrb	r1, [r1, #-12]	@ zero_extendqisi2
	strb	r1, [r3], #1
.L40:
	cmp	r7, #0
	bne	.L41
	add	r4, r4, r2
.L39:
	mov	r3, #0
	strb	r3, [r4, #0]
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L44:
	.align	2
.L43:
	.word	.LANCHOR5
.LFE60:
	.size	ConvertIntToStr, .-ConvertIntToStr
	.section	.text.CalcCharsWidth,"ax",%progbits
	.align	2
	.type	CalcCharsWidth, %function
CalcCharsWidth:
.LFB63:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI6:
	ldr	r5, .L55
	mov	r0, r0, asl #16
	mov	ip, r0, lsr #16
	ldrb	r0, [r5, r0, lsr #16]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	cmp	r0, #0
	moveq	ip, ip, asl #1
	mov	r4, r1, lsr #16
	ldreqh	r0, [r2, ip]
	beq	.L48
	ldr	r0, .L55+4
	ldr	r0, [r0, ip, asl #2]
	mov	ip, ip, asl #1
	ldrb	r5, [r0, #14]	@ zero_extendqisi2
	tst	r5, #1
	ldrneb	r5, [r0, #0]	@ zero_extendqisi2
	ldreqb	r5, [r0, #10]	@ zero_extendqisi2
	ldrh	r0, [r2, ip]
	add	r0, r5, r0
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
.L48:
	ldr	ip, .L55
	ldrb	r1, [ip, r1, lsr #16]	@ zero_extendqisi2
	cmp	r1, #0
	beq	.L49
	ldr	r1, .L55+4
	ldr	r1, [r1, r4, asl #2]
	mov	r4, r4, asl #1
	ldrb	ip, [r1, #14]	@ zero_extendqisi2
	tst	ip, #1
	ldreqh	r2, [r2, r4]
	ldreqb	ip, [r1, #10]	@ zero_extendqisi2
	ldrneb	r1, [r1, #5]	@ zero_extendqisi2
	ldrneh	r2, [r2, r4]
	ldreqb	r1, [r1, #11]	@ zero_extendqisi2
	subeq	r2, r2, #1
	addeq	r2, r2, ip
	add	r2, r2, r1
	b	.L53
.L49:
	ldr	r1, .L55+8
	mov	r4, r4, asl #1
	ldr	r1, [r1, #0]
	ldrb	ip, [r1, #10]	@ zero_extendqisi2
	ldrh	r1, [r2, r4]
	sub	ip, ip, #1
	add	r2, ip, r1
.L53:
	mov	r2, r2, asl #16
	strh	r0, [r3, #0]	@ movhi
	mov	r2, r2, lsr #16
	ldr	r3, [sp, #12]
	rsb	r0, r0, #1
	add	r0, r0, r2
	mov	r0, r0, asl #16
	strh	r2, [r3, #0]	@ movhi
	mov	r0, r0, lsr #16
	ldmfd	sp!, {r4, r5, pc}
.L56:
	.align	2
.L55:
	.word	.LANCHOR6
	.word	.LANCHOR7
	.word	.LANCHOR8
.LFE63:
	.size	CalcCharsWidth, .-CalcCharsWidth
	.section	.text.PrepareText,"ax",%progbits
	.align	2
	.type	PrepareText, %function
PrepareText:
.LFB64:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L74
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI7:
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L74+4
	cmp	r1, #2000
	movcs	r1, #2000
	str	r1, [sp, #0]
	ldr	r1, [r3, #0]
	ldr	r3, .L74+8
	str	r0, [sp, #4]
	ldrb	r8, [r3, #70]	@ zero_extendqisi2
	mov	r0, #0
	add	r3, r1, r5
	mov	r2, r0
	mov	fp, r0
	str	r3, [sp, #8]
	ldr	r7, .L74+12
	ldr	r4, .L74+16
	mov	r6, #1
	b	.L58
.L69:
	ldr	r9, [sp, #4]
	cmp	r5, #0
	ldrb	r3, [r9, r2]	@ zero_extendqisi2
	moveq	ip, r5
	beq	.L59
	ldrb	ip, [r1, r5]	@ zero_extendqisi2
	cmp	ip, r3
	movhi	ip, #0
	bhi	.L59
	ldr	sl, [sp, #8]
	ldrb	ip, [sl, #2]	@ zero_extendqisi2
	cmp	ip, r3
	movcs	ip, r5
	movcc	ip, #0
.L59:
	ldrb	r9, [r1, ip]	@ zero_extendqisi2
	cmp	r3, r9
	bcc	.L60
	add	sl, r1, ip
	ldrb	sl, [sl, #2]	@ zero_extendqisi2
	cmp	sl, r3
	bcs	.L61
.L60:
	ldrh	sl, [r1, #8]
	b	.L73
.L61:
	add	sl, r1, ip, asl #1
	ldrh	sl, [sl, #4]
	add	sl, sl, r3
	rsb	sl, r9, sl
.L73:
	ldr	sl, [r7, sl, asl #2]
	ldr	r9, .L74+20
	cmp	r8, #2
	str	sl, [r9, r0]
	beq	.L68
.L63:
	cmp	r8, #3
	bne	.L65
	cmp	ip, #0
	bne	.L68
.L66:
	sub	ip, r3, #48
	cmp	r3, #32
	cmpne	ip, #9
	bls	.L65
	cmp	r3, #43
	cmpne	r3, #45
	beq	.L65
	cmp	r3, #42
	cmpne	r3, #47
	beq	.L65
	cmp	r3, #61
	beq	.L65
.L68:
	strb	r6, [r2, r4]
	b	.L64
.L65:
	strb	fp, [r4, r2]
.L64:
	add	r2, r2, #1
	add	r0, r0, #4
.L58:
	ldr	sl, [sp, #0]
	cmp	r2, sl
	blt	.L69
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L75:
	.align	2
.L74:
	.word	.LANCHOR9
	.word	.LANCHOR8
	.word	.LANCHOR10
	.word	GuiFont_ChPtrList
	.word	.LANCHOR6
	.word	.LANCHOR7
.LFE64:
	.size	PrepareText, .-PrepareText
	.section	.text.UpdateDrawLimits,"ax",%progbits
	.align	2
	.type	UpdateDrawLimits, %function
UpdateDrawLimits:
.LFB66:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L77
	stmfd	sp!, {r4, lr}
.LCFI8:
	mov	r4, #1
	strb	r4, [ip, #34]
	strh	r0, [ip, #36]	@ movhi
	strh	r1, [ip, #38]	@ movhi
	strh	r2, [ip, #40]	@ movhi
	strh	r3, [ip, #42]	@ movhi
	ldr	ip, .L77+4
	strb	r4, [ip, #0]
	ldr	ip, .L77+8
	ldrsh	r4, [ip, #0]
	cmp	r0, r4
	movge	r0, r4
	strh	r0, [ip, #0]	@ movhi
	ldr	r0, .L77+12
	ldrsh	ip, [r0, #0]
	cmp	r1, ip
	movge	r1, ip
	strh	r1, [r0, #0]	@ movhi
	ldr	r1, .L77+16
	ldrsh	r0, [r1, #0]
	cmp	r2, r0
	movlt	r2, r0
	strh	r2, [r1, #0]	@ movhi
	ldr	r2, .L77+20
	ldrsh	r1, [r2, #0]
	cmp	r3, r1
	movlt	r3, r1
	strh	r3, [r2, #0]	@ movhi
	ldmfd	sp!, {r4, pc}
.L78:
	.align	2
.L77:
	.word	.LANCHOR10
	.word	.LANCHOR11
	.word	.LANCHOR12
	.word	.LANCHOR13
	.word	.LANCHOR14
	.word	.LANCHOR15
.LFE66:
	.size	UpdateDrawLimits, .-UpdateDrawLimits
	.section	.text.SetBackBox,"ax",%progbits
	.align	2
	.type	SetBackBox, %function
SetBackBox:
.LFB69:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI9:
	ldr	r4, .L93
	ldr	r0, .L93+4
	ldrh	r5, [r4, #18]
	ldrh	ip, [r4, #10]
	mov	r5, r5, asl #16
	mov	r1, r5, lsr #16
	add	r2, ip, r1
	ldrb	r4, [r4, #64]	@ zero_extendqisi2
	sub	r2, r2, #1
	ldr	r3, .L93+8
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	cmp	r4, #2
	strh	ip, [r0, #0]	@ movhi
	strh	r2, [r3, #0]	@ movhi
	bne	.L80
	mov	r1, r5, asr #16
	add	r1, r1, r1, lsr #31
	mov	r1, r1, asr #1
	rsb	r1, r1, #0
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	add	ip, r1, ip
	strh	ip, [r0, #0]	@ movhi
	b	.L90
.L80:
	cmp	r4, #3
	bne	.L81
	add	ip, ip, #1
	rsb	ip, r1, ip
	rsb	r1, r1, #1
	strh	ip, [r0, #0]	@ movhi
.L90:
	add	r2, r2, r1
	strh	r2, [r3, #0]	@ movhi
.L81:
	ldr	r3, .L93
	ldr	r2, .L93+12
	ldrb	r1, [r3, #20]	@ zero_extendqisi2
	ldrh	r0, [r3, #12]
	cmp	r1, #0
	ldreq	r1, .L93+16
	ldreq	r1, [r1, #0]
	ldreqb	r1, [r1, #14]	@ zero_extendqisi2
	rsb	r1, r1, r0
	strh	r1, [r2, #0]	@ movhi
	ldrb	r2, [r3, #21]	@ zero_extendqisi2
	ldr	r1, .L93
	cmp	r2, #0
	ldreq	r2, .L93+16
	ldreqh	r1, [r1, #12]
	ldreq	r2, [r2, #0]
	ldrneh	r1, [r1, #12]
	ldreqb	r0, [r2, #11]	@ zero_extendqisi2
	ldr	r3, .L93+20
	addeq	r0, r1, r0
	ldreqb	r1, [r2, #14]	@ zero_extendqisi2
	subeq	r0, r0, #1
	addne	r2, r2, r1
	rsbeq	r2, r1, r0
	strh	r2, [r3, #0]	@ movhi
	ldr	r3, .L93
	ldrb	r3, [r3, #69]	@ zero_extendqisi2
	tst	r3, #1
	ldrne	r2, .L93+4
	ldrneh	r1, [r2, #0]
	subne	r1, r1, #1
	strneh	r1, [r2, #0]	@ movhi
	tst	r3, #2
	ldrne	r2, .L93+8
	ldrneh	r1, [r2, #0]
	addne	r1, r1, #1
	strneh	r1, [r2, #0]	@ movhi
	tst	r3, #4
	ldrne	r2, .L93+12
	ldrneh	r1, [r2, #0]
	subne	r1, r1, #1
	strneh	r1, [r2, #0]	@ movhi
	tst	r3, #8
	ldrne	r3, .L93+20
	ldrneh	r2, [r3, #0]
	addne	r2, r2, #1
	strneh	r2, [r3, #0]	@ movhi
	ldmfd	sp!, {r4, r5, pc}
.L94:
	.align	2
.L93:
	.word	.LANCHOR10
	.word	.LANCHOR16
	.word	.LANCHOR17
	.word	.LANCHOR18
	.word	.LANCHOR8
	.word	.LANCHOR19
.LFE69:
	.size	SetBackBox, .-SetBackBox
	.section	.text.ReadVar,"ax",%progbits
	.align	2
	.type	ReadVar, %function
ReadVar:
.LFB73:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	subs	r3, r0, #0
	moveq	r0, r3
	bxeq	lr
	ldr	r2, .L141+24
	ldrb	r0, [r2, #75]	@ zero_extendqisi2
	cmp	r0, #1
	bne	.L97
	sub	r0, r1, #7
	cmp	r0, #1
	ldrls	r2, .L141+28
	mov	r0, #0
	strlsh	r0, [r2, #0]	@ movhi
	strhib	r0, [r2, #75]
.L97:
	cmp	r1, #8
	ldrls	pc, [pc, r1, asl #2]
	b	.L136
.L108:
	.word	.L99
	.word	.L100
	.word	.L101
	.word	.L102
	.word	.L103
	.word	.L105
	.word	.L105
	.word	.L106
	.word	.L107
.L99:
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	adds	r0, r0, #0
	movne	r0, #1
	bx	lr
.L100:
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	bx	lr
.L101:
	ldrsb	r0, [r3, #0]
	bx	lr
.L102:
	ldrh	r0, [r3, #0]
	bx	lr
.L103:
	ldrsh	r0, [r3, #0]
	bx	lr
.L105:
	ldr	r0, [r3, #0]
	bx	lr
.L106:
	flds	s14, [r3, #0]
	ldr	r3, .L141+24
	fcmpezs	s14
	ldrb	r3, [r3, #75]	@ zero_extendqisi2
	fmstat
	movpl	r1, #0
	movmi	r1, #1
	fnegsmi	s15, s14
	fcpyspl	s15, s14
	cmp	r3, #1
	bne	.L110
	fcmpzs	s15
	fmstat
	beq	.L133
	ldr	r3, .L141+28
	flds	s14, .L141
	ldrh	r3, [r3, #0]
	b	.L112
.L113:
	fdivs	s15, s15, s14
.L112:
	fcmpes	s15, s14
	mov	r2, r3
	add	r3, r3, #1
	mov	r3, r3, asl #16
	fmstat
	mov	r3, r3, lsr #16
	flds	s13, .L141
	bgt	.L113
	flds	s14, .L141+4
	b	.L114
.L115:
	fmuls	s15, s15, s13
.L114:
	fcmpes	s15, s14
	mov	r3, r2
	sub	r2, r2, #1
	mov	r2, r2, asl #16
	fmstat
	mov	r2, r2, lsr #16
	bmi	.L115
	ldr	r2, .L141+28
	strh	r3, [r2, #0]	@ movhi
	b	.L133
.L110:
	ftosizshi	s14, s14
	fmrshi	r0, s14	@ int
	bxhi	lr
.L133:
	ldr	r3, .L141+24
	flds	s14, .L141+4
	flds	s13, .L141
	ldrb	r2, [r3, #73]	@ zero_extendqisi2
	mov	r3, #0
	b	.L117
.L118:
	fmuls	s14, s14, s13
	add	r3, r3, #1
	and	r3, r3, #255
.L117:
	cmp	r3, r2
	bne	.L118
	fmuls	s13, s15, s14
	mvn	r2, #9
	ftosizs	s13, s13
	fmrs	r0, s13	@ int
	flds	s13, .L141
	fmuls	s14, s14, s13
	fmuls	s15, s15, s14
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
	mla	r3, r2, r0, r3
	cmp	r3, #4
	addgt	r0, r0, #1
	cmp	r1, #0
	bne	.L140
	bx	lr
.L107:
	fldd	d7, [r3, #0]
	ldr	r2, .L141+24
	fcmpezd	d7
	ldrb	r2, [r2, #75]	@ zero_extendqisi2
	fmstat
	fnegdmi	d7, d7
	cmp	r2, #1
	bne	.L122
	fcmpzd	d7
	fmstat
	beq	.L134
	ldr	r2, .L141+28
	fldd	d5, .L141+8
	ldrh	r2, [r2, #0]
	b	.L124
.L125:
	fdivd	d7, d7, d5
.L124:
	fcmped	d7, d5
	mov	r1, r2
	add	r2, r2, #1
	mov	r2, r2, asl #16
	fmstat
	mov	r2, r2, lsr #16
	fldd	d6, .L141+8
	bgt	.L125
	fldd	d5, .L141+16
	b	.L126
.L127:
	fmuld	d7, d7, d6
.L126:
	fcmped	d7, d5
	mov	r2, r1
	sub	r1, r1, #1
	mov	r1, r1, asl #16
	fmstat
	mov	r1, r1, lsr #16
	bmi	.L127
	ldr	r1, .L141+28
	strh	r2, [r1, #0]	@ movhi
	b	.L134
.L122:
	bhi	.L128
.L134:
	ldr	r2, .L141+24
	fldd	d6, .L141+16
	fldd	d5, .L141+8
	ldrb	r1, [r2, #73]	@ zero_extendqisi2
	mov	r2, #0
	b	.L129
.L130:
	fmuld	d6, d6, d5
	add	r2, r2, #1
	and	r2, r2, #255
.L129:
	cmp	r2, r1
	bne	.L130
	fmuld	d5, d7, d6
	mvn	r1, #9
	ftosizd	s9, d5
	fldd	d5, .L141+8
	fmuld	d6, d6, d5
	fmrs	r0, s9	@ int
	fmuld	d7, d7, d6
	ftosizd	s13, d7
	fldd	d7, [r3, #0]
	fcmpezd	d7
	fmrs	r2, s13	@ int
	mla	r2, r1, r0, r2
	cmp	r2, #4
	addgt	r0, r0, #1
	fmstat
	bxpl	lr
.L140:
	rsb	r0, r0, #0
	bx	lr
.L128:
	ftosizd	s9, d7
	fmrs	r0, s9	@ int
	bx	lr
.L136:
	mov	r0, #0
	bx	lr
.L142:
	.align	2
.L141:
	.word	1092616192
	.word	1065353216
	.word	0
	.word	1076101120
	.word	0
	.word	1072693248
	.word	.LANCHOR10
	.word	.LANCHOR20
.LFE73:
	.size	ReadVar, .-ReadVar
	.section	.text.GetItemByte,"ax",%progbits
	.align	2
	.type	GetItemByte, %function
GetItemByte:
.LFB75:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #0]
	mov	r2, r0
	ldrb	r0, [r3], #1	@ zero_extendqisi2
	str	r3, [r2, #0]
	ldr	r3, .L144
	ldrh	r2, [r3, #0]
	add	r2, r2, #1
	strh	r2, [r3, #0]	@ movhi
	bx	lr
.L145:
	.align	2
.L144:
	.word	.LANCHOR21
.LFE75:
	.size	GetItemByte, .-GetItemByte
	.section	.text.GetItemWord,"ax",%progbits
	.align	2
	.type	GetItemWord, %function
GetItemWord:
.LFB76:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #0]
	mov	r2, r3
	ldrb	r1, [r2], #1	@ zero_extendqisi2
	str	r2, [r0, #0]
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	add	r3, r3, #2
	str	r3, [r0, #0]
	ldr	r3, .L147
	add	r2, r1, r2, asl #8
	ldrh	r1, [r3, #0]
	mov	r0, r2, asl #16
	add	r1, r1, #2
	strh	r1, [r3, #0]	@ movhi
	mov	r0, r0, asr #16
	bx	lr
.L148:
	.align	2
.L147:
	.word	.LANCHOR21
.LFE76:
	.size	GetItemWord, .-GetItemWord
	.section	.text.SetCurFont,"ax",%progbits
	.align	2
	.type	SetCurFont, %function
SetCurFont:
.LFB77:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L152
	cmp	r0, #6
	movhi	r0, #0
	ldr	r2, [r3, r0, asl #2]
	ldr	r3, .L152+4
	str	r2, [r3, #0]
	bx	lr
.L153:
	.align	2
.L152:
	.word	GuiFont_FontList
	.word	.LANCHOR8
.LFE77:
	.size	SetCurFont, .-SetCurFont
	.section	.text.ReadItem,"ax",%progbits
	.align	2
	.type	ReadItem, %function
ReadItem:
.LFB80:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI10:
	ldr	r6, .L389
	mov	r8, r0
	ldr	r3, [r6, #60]
	ldr	r0, .L389+4
	orr	r3, r3, #1
	str	r3, [r6, #60]
	bl	GetItemByte
	ldr	r3, .L389+8
	mov	r4, r0
	strb	r0, [r3, #0]
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389+12
	and	r4, r4, #31
	strb	r0, [r3, #0]
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389+16
	strb	r0, [r3, #0]
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389+20
	strb	r0, [r3, #0]
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389+24
	mov	r7, r0
	strb	r0, [r3, #0]
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389+28
	mov	r5, r0
	strb	r0, [r3, #0]
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389+32
	strb	r4, [r6, #56]
	strb	r0, [r3, #0]
	mov	r3, #1
	mov	r4, r3, asl r4
	ldr	r3, .L389+36
	str	r4, [r3, #0]
	ldr	r3, .L389+40
	and	r3, r4, r3
	cmp	r3, #0
	beq	.L155
	ldr	r3, [r6, #60]
	tst	r7, #128
	orrne	r3, r3, #128
	biceq	r3, r3, #128
	str	r3, [r6, #60]
	ldr	r0, [r6, #60]
	ldr	r3, .L389+44
	ldr	r2, .L389+4
	ands	r0, r0, #128
	ldr	r7, .L389
	movne	r0, r8, asl #16
	ldrh	r6, [r3, #0]
	movne	r0, r0, lsr #16
	movne	fp, #2
	moveq	fp, #1
	mov	r3, #0
	mov	r8, r2
.L163:
	cmp	r3, r0
	ldreq	r1, [r2, #0]
	streq	r1, [r7, #4]
	mov	r1, #0
	b	.L160
.L161:
	add	r1, r1, #1
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	str	ip, [r2, #0]
.L160:
	ldr	ip, [r2, #0]
	add	sl, r1, r6
	ldrb	r9, [ip], #1	@ zero_extendqisi2
	mov	sl, sl, asl #16
	cmp	r9, #0
	mov	sl, sl, lsr #16
	bne	.L161
	cmp	r3, r0
	add	r3, r3, #1
	streqh	r1, [r7, #54]	@ movhi
	add	r6, sl, #1
	mov	r1, r3, asl #16
	mov	r6, r6, asl #16
	cmp	fp, r1, asr #16
	str	ip, [r8, #0]
	mov	r6, r6, lsr #16
	bgt	.L163
	ldr	r3, .L389+44
	strh	r6, [r3, #0]	@ movhi
.L155:
	ldr	r6, .L389+48
	and	r6, r4, r6
	cmp	r6, #0
	beq	.L164
	ldr	r3, .L389
	tst	r5, #1
	ldr	r2, [r3, #60]
	orrne	r2, r2, #256
	biceq	r2, r2, #256
	str	r2, [r3, #60]
.L164:
	ldr	r3, .L389+52
	and	r3, r4, r3
	cmp	r3, #0
	beq	.L166
	ldr	r0, .L389+4
	bl	GetItemByte
	cmp	r0, #255
	ldrne	r3, .L389
	addne	r0, r0, #1
	strneb	r0, [r3, #52]
.L166:
	cmp	r6, #0
	beq	.L167
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389
	strb	r0, [r3, #80]
.L167:
	ands	r6, r4, #10240
	beq	.L168
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389
	strb	r0, [r3, #88]
.L168:
	tst	r4, #8512
	beq	.L169
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389
	strb	r0, [r3, #81]
.L169:
	tst	r4, #8
	beq	.L170
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389
	cmp	r0, #0
	moveq	r2, #1
	strb	r0, [r3, #71]
	streqb	r2, [r3, #71]
.L170:
	tst	r4, #64
	beq	.L171
	ldr	r0, .L389+4
	bl	GetItemWord
	ldr	r3, .L389
	strh	r0, [r3, #82]	@ movhi
.L171:
	ldr	r3, .L389+56
	and	r3, r4, r3
	cmp	r3, #0
	beq	.L172
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389
	ldr	r2, [r3, #60]
	tst	r0, #1
	orrne	r2, r2, #512
	biceq	r2, r2, #512
	str	r2, [r3, #60]
	ldr	r2, [r3, #60]
	tst	r0, #2
	orrne	r2, r2, #1024
	biceq	r2, r2, #1024
	str	r2, [r3, #60]
.L172:
	tst	r4, #512
	beq	.L176
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r5, .L389
	strb	r0, [r5, #75]
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, [r5, #60]
	tst	r0, #1
	orrne	r3, r3, #8
	biceq	r3, r3, #8
	str	r3, [r5, #60]
	ldr	r3, [r5, #60]
	tst	r0, #2
	orrne	r3, r3, #16
	biceq	r3, r3, #16
	str	r3, [r5, #60]
	ldr	r3, .L389
	mov	r2, r0, lsr #2
	and	r2, r2, #3
	strb	r2, [r3, #74]
	ldr	r2, [r3, #60]
	tst	r0, #16
	orrne	r2, r2, #4096
	biceq	r2, r2, #4096
	str	r2, [r3, #60]
	ldr	r2, [r3, #60]
	tst	r0, #32
	orrne	r2, r2, #8192
	biceq	r2, r2, #8192
	str	r2, [r3, #60]
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r5, .L389
	strb	r0, [r5, #72]
	ldr	r0, .L389+4
	bl	GetItemByte
	strb	r0, [r5, #73]
.L176:
	ldr	r3, .L389+60
	and	r3, r4, r3
	cmp	r3, #0
	beq	.L185
	ldr	r0, .L389+4
	bl	GetItemWord
	ldr	r3, .L389
	strh	r0, [r3, #8]	@ movhi
.L185:
	cmp	r6, #0
	beq	.L186
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r5, .L389
	strb	r0, [r5, #84]
	ldr	r0, .L389+4
	bl	GetItemByte
	strb	r0, [r5, #85]
	ldr	r0, .L389+4
	bl	GetItemByte
	strb	r0, [r5, #86]
	ldr	r0, .L389+4
	bl	GetItemByte
	strb	r0, [r5, #87]
.L186:
	ldr	r3, .L389+64
	and	r3, r4, r3
	cmp	r3, #0
	beq	.L187
	ldr	r3, .L389+20
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	and	r3, r3, #7
	cmp	r3, #3
	bne	.L188
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389
	strb	r0, [r3, #65]
.L188:
	ldr	r3, .L389+28
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r3, r3, lsr #1
	and	r3, r3, #7
	cmp	r3, #3
	bne	.L187
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389
	strb	r0, [r3, #67]
.L187:
	ldr	r3, .L389+68
	and	r3, r4, r3
	cmp	r3, #0
	beq	.L189
	ldr	r3, .L389+20
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r3, r3, lsr #3
	and	r3, r3, #7
	cmp	r3, #3
	bne	.L190
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389
	strb	r0, [r3, #66]
.L190:
	ldr	r3, .L389+28
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r3, r3, lsr #4
	and	r3, r3, #7
	cmp	r3, #3
	bne	.L189
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389
	strb	r0, [r3, #68]
.L189:
	ldr	r3, .L389+72
	and	r3, r4, r3
	cmp	r3, #0
	beq	.L191
	ldr	r3, .L389+32
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L191
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389
	strb	r0, [r3, #94]
.L191:
	ldr	r3, .L389+8
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	ands	r0, r5, #32
	beq	.L192
	ldr	r0, .L389+4
	bl	GetItemByte
.L192:
	ldr	r3, .L389+76
	and	r2, r0, #3
	strb	r2, [r3, #0]
	ldr	r3, .L389+80
	mov	r2, r0, lsr #2
	and	r2, r2, #3
	strb	r2, [r3, #0]
	ldr	r3, .L389+84
	mov	r2, r0, lsr #4
	and	r2, r2, #3
	strb	r2, [r3, #0]
	ldr	r3, .L389+88
	mov	r0, r0, lsr #6
	strb	r0, [r3, #0]
	ands	r0, r5, #64
	beq	.L193
	ldr	r0, .L389+4
	bl	GetItemByte
.L193:
	ldr	r3, .L389+92
	and	r2, r0, #3
	strb	r2, [r3, #0]
	ldr	r3, .L389+96
	mov	r2, r0, lsr #2
	and	r2, r2, #3
	strb	r2, [r3, #0]
	ldr	r3, .L389+100
	mov	r2, r0, lsr #4
	and	r2, r2, #3
	strb	r2, [r3, #0]
	ldr	r3, .L389+104
	mov	r0, r0, lsr #6
	strb	r0, [r3, #0]
	ldr	r3, .L389+32
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	ands	r0, r0, #1
	beq	.L194
	ldr	r0, .L389+4
	bl	GetItemByte
.L194:
	ldr	r3, .L389+108
	ldr	r6, .L389
	and	r3, r4, r3
	cmp	r3, #0
	and	r7, r5, #255
	moveq	r0, #0
	tst	r7, #128
	strb	r0, [r6, #69]
	moveq	r5, #0
	beq	.L196
	ldr	r0, .L389+4
	bl	GetItemWord
	mov	r5, r0
	ldr	r0, .L389+4
	bl	GetItemByte
	strb	r0, [r6, #20]
	ldr	r0, .L389+4
	bl	GetItemByte
	strb	r0, [r6, #21]
.L196:
	ldr	r3, .L389+112
	ldr	r2, .L389
	and	r3, r4, r3
	cmp	r3, #0
	moveq	r5, #0
	tst	r4, #32768
	strh	r5, [r2, #18]	@ movhi
	beq	.L198
	ldr	r3, .L389+116
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L198
	tst	r7, #128
	beq	.L198
	ldr	r3, .L389+120
	ldr	r1, .L389+204
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r0, #268
	mla	r3, r0, r3, r1
	ldrb	r1, [r2, #20]	@ zero_extendqisi2
	ldrb	r2, [r2, #21]	@ zero_extendqisi2
	strh	r5, [r3, #136]	@ movhi
	strh	r1, [r3, #138]	@ movhi
	strh	r2, [r3, #140]	@ movhi
.L198:
	ldr	r3, .L389+12
	ldr	r2, .L389+124
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldr	r5, .L389+128
	and	r1, r3, #3
	strb	r1, [r2, #0]
	ldr	r2, .L389+132
	mov	r1, r3, lsr #2
	and	r1, r1, #3
	strb	r1, [r2, #0]
	ldr	r2, .L389+136
	mov	r1, r3, lsr #4
	and	r1, r1, #3
	strb	r1, [r2, #0]
	ldr	r2, .L389+140
	mov	r3, r3, lsr #6
	strb	r3, [r2, #0]
	ldr	r3, .L389+16
	ldrb	r4, [r3, #0]	@ zero_extendqisi2
	ands	r3, r4, #1
	streqh	r3, [r5, #0]	@ movhi
	beq	.L200
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #0]	@ movhi
.L200:
	ldr	r5, .L389+144
	ands	r3, r4, #2
	streqh	r3, [r5, #0]	@ movhi
	beq	.L202
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #0]	@ movhi
.L202:
	ldr	r5, .L389+148
	ands	r3, r4, #4
	streqh	r3, [r5, #0]	@ movhi
	beq	.L204
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #0]	@ movhi
.L204:
	ldr	r5, .L389+152
	ands	r3, r4, #8
	streqh	r3, [r5, #0]	@ movhi
	beq	.L206
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #0]	@ movhi
.L206:
	tst	r4, #16
	beq	.L207
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389+156
	strb	r0, [r3, #0]
.L207:
	tst	r4, #32
	beq	.L208
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389+160
	strb	r0, [r3, #0]
.L208:
	tst	r4, #64
	beq	.L209
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389+164
	strb	r0, [r3, #0]
.L209:
	tst	r4, #128
	beq	.L210
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389+168
	strb	r0, [r3, #0]
.L210:
	ldr	r2, .L389+36
	ldr	r3, .L389
	ldr	r1, [r2, #0]
	ldr	r2, .L389+64
	ldrb	r7, [r3, #65]	@ zero_extendqisi2
	and	r2, r1, r2
	cmp	r2, #0
	ldrb	r8, [r3, #66]	@ zero_extendqisi2
	beq	.L211
	ldr	r2, .L389+20
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	and	r2, r2, #7
	cmp	r2, #2
	beq	.L214
	cmp	r2, #4
	streqb	r8, [r3, #65]
	beq	.L212
	cmp	r2, #1
	moveq	r2, #0
	bne	.L212
	b	.L358
.L214:
	mov	r2, #15
.L358:
	strb	r2, [r3, #65]
.L212:
	ldr	r3, .L389+28
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L389
	mov	r2, r2, lsr #1
	and	r2, r2, #7
	cmp	r2, #2
	beq	.L217
	cmp	r2, #4
	beq	.L218
	cmp	r2, #1
	moveq	r2, #0
	bne	.L211
	b	.L359
.L217:
	mov	r2, #15
	b	.L359
.L218:
	ldrb	r2, [r3, #66]	@ zero_extendqisi2
.L359:
	strb	r2, [r3, #67]
.L211:
	ldr	r3, .L389+36
	ldr	r2, [r3, #0]
	ldr	r3, .L389+68
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L219
	ldr	r3, .L389+20
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L389
	mov	r2, r2, lsr #3
	and	r2, r2, #7
	sub	r2, r2, #1
	cmp	r2, #4
	ldrls	pc, [pc, r2, asl #2]
	b	.L220
.L226:
	.word	.L221
	.word	.L222
	.word	.L375
	.word	.L224
	.word	.L225
.L221:
	mov	r2, #0
	b	.L383
.L222:
	mov	r2, #15
.L383:
	strb	r2, [r3, #66]
	b	.L375
.L224:
	strb	r7, [r3, #66]
.L375:
	ldr	r2, [r3, #60]
	bic	r2, r2, #2
	b	.L360
.L225:
	ldr	r2, [r3, #60]
	orr	r2, r2, #2
.L360:
	str	r2, [r3, #60]
.L220:
	ldr	r3, .L389+28
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L389
	mov	r2, r2, lsr #4
	and	r2, r2, #7
	sub	r2, r2, #1
	cmp	r2, #4
	ldrls	pc, [pc, r2, asl #2]
	b	.L219
.L232:
	.word	.L227
	.word	.L228
	.word	.L377
	.word	.L230
	.word	.L231
.L227:
	mov	r2, #0
	b	.L376
.L228:
	mov	r2, #15
.L376:
	strb	r2, [r3, #68]
.L377:
	ldr	r2, [r3, #60]
	bic	r2, r2, #32768
	b	.L361
.L230:
	ldr	r2, [r3, #60]
	bic	r2, r2, #32768
	str	r2, [r3, #60]
	ldrb	r2, [r3, #65]	@ zero_extendqisi2
	strb	r2, [r3, #68]
	b	.L219
.L231:
	ldr	r2, [r3, #60]
	orr	r2, r2, #32768
.L361:
	str	r2, [r3, #60]
.L219:
	ldr	r3, .L389+36
	ldr	r4, [r3, #0]
	ldr	r3, .L389+172
	and	r3, r4, r3
	cmp	r3, #0
	beq	.L233
	ldr	r3, .L389+20
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	movs	r3, r3, lsr #6
	ldrne	r2, .L389
	strneb	r3, [r2, #64]
.L233:
	ldr	r3, .L389+24
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L389+108
	and	r3, r4, r3
	cmp	r3, #0
	beq	.L234
	ands	r0, r2, #3
	ldrne	r1, .L389
	strneb	r0, [r1, #70]
.L234:
	ldr	r1, .L389+176
	mov	r0, r2, lsr #2
	and	r0, r0, #3
	cmp	r3, #0
	strb	r0, [r1, #0]
	ldr	r3, .L389
	bne	.L385
.L235:
	tst	r4, #4
	beq	.L237
.L385:
	ldr	r1, [r3, #60]
	tst	r2, #16
	orrne	r1, r1, #4
	biceq	r1, r1, #4
	str	r1, [r3, #60]
.L237:
	ldr	r3, .L389+180
	and	r3, r4, r3
	cmp	r3, #0
	ldr	r3, .L389
	ldreq	r2, [r3, #60]
	beq	.L379
	tst	r2, #32
	ldr	r2, [r3, #60]
	orrne	r2, r2, #32
	bne	.L363
.L379:
	bic	r2, r2, #32
.L363:
	str	r2, [r3, #60]
	ldr	r3, .L389+24
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	tst	r3, #64
	moveq	r0, #255
	beq	.L242
	ldr	r0, .L389+4
	bl	GetItemByte
	and	r0, r0, #255
.L242:
	ldr	r3, .L389+184
	ldr	sl, .L389
	and	r3, r4, r3
	cmp	r3, #0
	ldr	r3, .L389+28
	moveq	r0, #255
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	strb	r0, [sl, #76]
	tst	r6, #1
	moveq	r0, #255
	beq	.L244
	ldr	r0, .L389+4
	bl	GetItemByte
	and	r0, r0, #255
.L244:
	ldr	r3, .L389+176
	ldr	r5, .L389+48
	ldrsb	r3, [r3, #0]
	and	r5, r4, r5
	cmp	r5, #0
	moveq	r0, #255
	cmp	r3, #3
	strb	r0, [sl, #79]
	bne	.L246
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, .L389+188
	strb	r0, [r3, #0]
.L246:
	cmp	r5, #0
	ldr	r5, .L389
	beq	.L247
	mov	r6, r6, asl #24
	tst	r4, #8448
	mov	r6, r6, asr #24
	bne	.L248
	ldr	r3, [r5, #60]
	tst	r3, #128
	beq	.L249
.L248:
	cmp	r6, #0
	ldr	r5, .L389
	bge	.L247
	b	.L387
.L249:
	cmp	r6, #0
	bge	.L380
.L387:
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	r3, [r5, #60]
	cmp	r0, #0
	orrne	r3, r3, #2048
	bne	.L364
	b	.L380
.L247:
	ldr	r3, [r5, #60]
.L380:
	bic	r3, r3, #2048
.L364:
	tst	r4, #4
	str	r3, [r5, #60]
	beq	.L255
	ldr	r5, .L389
	ldr	r3, [r5, #60]
	tst	r3, #4
	beq	.L255
	ldr	r0, .L389+4
	bl	GetItemByte
	strb	r0, [r5, #95]
.L255:
	tst	r4, #32768
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	sl, .L389+204
	ldr	r3, .L389+120
	mov	r6, #268
	mla	r5, r6, r0, sl
	strb	r0, [r3, #0]
	mov	r3, #1
	strb	r3, [r5, #4]
	mov	r4, r0
	ldr	r0, .L389+4
	bl	GetItemByte
	add	r9, r5, #144
	strb	r0, [r5, #124]
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #126]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #128]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #130]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #132]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #134]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #136]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #138]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #142]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #144]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r9, #2]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemByte
	add	r3, r5, #148
	mov	r2, #0
	strb	r2, [r3, #3]
	strb	r0, [r5, #148]
	mov	r5, sl
	cmp	r0, #5
	ldrls	pc, [pc, r0, asl #2]
	b	.L257
.L264:
	.word	.L258
	.word	.L259
	.word	.L260
	.word	.L261
	.word	.L262
	.word	.L263
.L258:
	ldr	r3, .L389
	mla	r6, r4, r6, sl
	ldrb	r3, [r3, #65]	@ zero_extendqisi2
	b	.L365
.L259:
	mla	r6, r4, r6, sl
	mov	r3, #0
	b	.L365
.L260:
	mla	r6, r4, r6, sl
	mov	r3, #15
.L365:
	strb	r3, [r6, #149]
	b	.L257
.L261:
	ldr	r0, .L389+4
	bl	GetItemByte
	mov	r3, #268
	mla	r5, r3, r4, r5
	strb	r0, [r5, #149]
	b	.L257
.L262:
	mla	r6, r4, r6, sl
	strb	r7, [r6, #149]
	b	.L257
.L263:
	mla	r6, r4, r6, sl
	mov	r3, #1
	strb	r3, [r6, #151]
.L257:
	ldr	r0, .L389+4
	bl	GetItemByte
	ldr	sl, .L389+204
	mov	r5, #268
	mla	r5, r4, r5, sl
	mov	r6, #0
	ldr	r9, .L389
	strb	r0, [r5, #152]
	ldr	r0, .L389+4
	bl	GetItemByte
	strb	r0, [r5, #153]
	ldr	r0, .L389+4
	bl	GetItemByte
	strb	r0, [r5, #154]
	ldr	r0, .L389+4
	bl	GetItemByte
	strb	r0, [r5, #155]
	b	.L265
.L275:
	bl	GetItemByte
	add	r3, r5, r6
	strb	r0, [r3, #156]
	cmp	r0, #5
	ldrls	pc, [pc, r0, asl #2]
	b	.L266
.L273:
	.word	.L267
	.word	.L268
	.word	.L269
	.word	.L270
	.word	.L271
	.word	.L272
.L267:
	ldrb	r2, [r9, #66]	@ zero_extendqisi2
	add	r3, r5, r6
	strb	r2, [r3, #157]
	b	.L266
.L268:
	add	r3, r5, r6
	mov	r2, #0
	strb	r2, [r3, #157]
	b	.L388
.L269:
	add	r3, r5, r6
	mov	r2, #15
	strb	r2, [r3, #157]
.L382:
	mov	r2, #0
	b	.L388
.L270:
	ldr	r0, .L389+4
	bl	GetItemByte
	add	r3, r5, r6
	strb	r0, [r3, #157]
	b	.L382
.L271:
	add	r3, r5, r6
	strb	r7, [r3, #157]
	b	.L382
.L272:
	add	r3, r5, r6
	mov	r2, #1
.L388:
	strb	r2, [r3, #158]
.L266:
	ldr	r0, .L389+4
	bl	GetItemWord
	mov	r3, #134
	mla	r3, r4, r3, r6
	add	r3, sl, r3, asl #1
	strh	r0, [r3, #160]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemByte
	add	r3, r5, r6
	cmp	r6, #0
	ldr	r2, .L389+204
	movne	r1, #0
	strb	r0, [r3, #162]
	bne	.L274
	ldrb	r1, [r5, #124]	@ zero_extendqisi2
	rsbs	r1, r1, #1
	movcc	r1, #0
.L274:
	mov	r3, #134
	mla	r3, r4, r3, r6
	add	r6, r6, #1
	add	r3, r3, #82
	mov	r3, r3, asl #1
	add	r0, r2, r3
	strh	r1, [r0, #2]	@ movhi
	and	r6, r6, #255
	mov	r1, #0
	strh	r1, [r2, r3]	@ movhi
.L265:
	ldrb	r3, [r5, #155]	@ zero_extendqisi2
	ldr	r0, .L389+4
	cmp	r3, r6
	bhi	.L275
	bl	GetItemByte
	cmp	r0, #0
	strb	r0, [r5, #168]
	beq	.L276
	ldr	r0, .L389+4
	bl	GetItemByte
	add	r6, r5, #176
	strb	r0, [r5, #169]
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #170]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #172]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #174]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r5, #176]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemWord
	strh	r0, [r6, #2]	@ movhi
	ldr	r0, .L389+4
	bl	GetItemByte
	mov	r3, #268
	strb	r0, [r5, #180]
	and	r0, r0, #7
	ldr	r5, .L389+204
	cmp	r0, #4
	ldrls	pc, [pc, r0, asl #2]
	b	.L277
.L283:
	.word	.L278
	.word	.L279
	.word	.L280
	.word	.L281
	.word	.L282
.L278:
	mla	r5, r3, r4, r5
	ldr	r3, .L389
	ldrb	r3, [r3, #65]	@ zero_extendqisi2
	b	.L367
.L279:
	mla	r3, r4, r3, r5
	mov	r2, #0
	strb	r2, [r3, #182]
	b	.L277
.L280:
	mla	r5, r3, r4, r5
	mov	r3, #15
.L367:
	strb	r3, [r5, #182]
	b	.L277
.L281:
	ldr	r0, .L389+4
	bl	GetItemByte
	mov	r3, #268
	mla	r5, r3, r4, r5
	strb	r0, [r5, #182]
	b	.L277
.L282:
	mla	r5, r3, r4, r5
	strb	r8, [r5, #182]
.L277:
	ldr	r5, .L389+204
	mov	r3, #268
	mla	r2, r3, r4, r5
	mov	r0, #0
	add	r1, r2, #180
	ldrb	r2, [r2, #180]	@ zero_extendqisi2
	strb	r0, [r1, #1]
	and	r2, r2, #56
	mov	r2, r2, asr #3
	cmp	r2, #5
	ldrls	pc, [pc, r2, asl #2]
	b	.L284
.L291:
	.word	.L285
	.word	.L286
	.word	.L287
	.word	.L288
	.word	.L289
	.word	.L290
.L285:
	mla	r5, r3, r4, r5
	ldr	r3, .L389
	ldrb	r3, [r3, #65]	@ zero_extendqisi2
	b	.L368
.L286:
	mla	r3, r4, r3, r5
	mov	r2, #0
	strb	r2, [r3, #183]
	b	.L284
.L287:
	mla	r5, r3, r4, r5
	mov	r3, #15
.L368:
	strb	r3, [r5, #183]
	b	.L284
.L288:
	ldr	r0, .L389+4
	bl	GetItemByte
	mov	r3, #268
	mla	r5, r3, r4, r5
	strb	r0, [r5, #183]
	b	.L284
.L289:
	mla	r5, r3, r4, r5
	strb	r7, [r5, #183]
	b	.L284
.L290:
	mla	r5, r3, r4, r5
	mov	r3, #1
	strb	r3, [r5, #181]
.L284:
	ldr	r5, .L389+4
	ldr	r9, .L389+204
	mov	r0, r5
	bl	GetItemByte
	mov	sl, #268
	mla	r6, sl, r4, r9
	add	fp, r6, #200
	strb	r0, [r6, #184]
	mov	r0, r5
	bl	GetItemWord
	strh	r0, [r6, #186]	@ movhi
	mov	r0, r5
	bl	GetItemWord
	strh	r0, [r6, #188]	@ movhi
	mov	r0, r5
	bl	GetItemWord
	strh	r0, [r6, #190]	@ movhi
	mov	r0, r5
	bl	GetItemWord
	ldr	r3, [r5, #0]
	str	r3, [r6, #212]
	strh	r0, [r6, #192]	@ movhi
	mov	r0, r5
	bl	GetItemByte
	strh	r0, [r6, #194]	@ movhi
	mov	r0, r5
	bl	GetItemByte
	add	r0, r0, #1
	strb	r0, [r6, #196]
	mov	r0, r5
	bl	GetItemWord
	strh	r0, [r6, #198]	@ movhi
	mov	r0, r5
	bl	GetItemWord
	strh	r0, [r6, #200]	@ movhi
	mov	r0, r5
	bl	GetItemByte
	mov	r5, r9
	strb	r0, [fp, #2]
	and	r0, r0, #7
	cmp	r0, #4
	ldrls	pc, [pc, r0, asl #2]
	b	.L292
.L298:
	.word	.L293
	.word	.L294
	.word	.L295
	.word	.L296
	.word	.L297
.L293:
	ldr	r3, .L389
	mla	sl, r4, sl, r9
	ldrb	r3, [r3, #65]	@ zero_extendqisi2
	b	.L369
.L294:
	mla	sl, r4, sl, r9
	mov	r3, #0
.L369:
	strb	r3, [sl, #204]
	b	.L292
.L295:
	mla	sl, r4, sl, r9
	mov	r3, #15
	b	.L369
.L296:
	ldr	r0, .L389+4
	bl	GetItemByte
	mov	r3, #268
	mla	r5, r3, r4, r5
	strb	r0, [r5, #204]
	b	.L292
.L297:
	mla	sl, r4, sl, r9
	strb	r8, [sl, #204]
.L292:
	ldr	r5, .L389+204
	mov	r3, #268
	mla	r2, r3, r4, r5
	mov	r1, #0
	strb	r1, [r2, #203]
	add	r2, r2, #200
	ldrb	r2, [r2, #2]	@ zero_extendqisi2
	and	r2, r2, #56
	mov	r2, r2, asr #3
	cmp	r2, #5
	ldrls	pc, [pc, r2, asl #2]
	b	.L299
.L306:
	.word	.L300
	.word	.L301
	.word	.L302
	.word	.L303
	.word	.L304
	.word	.L305
.L300:
	mla	r5, r3, r4, r5
	ldr	r3, .L389
	ldrb	r3, [r3, #66]	@ zero_extendqisi2
	b	.L370
.L301:
	mla	r3, r4, r3, r5
	mov	r2, #0
	strb	r2, [r3, #205]
	b	.L299
.L390:
	.align	2
.L389:
	.word	.LANCHOR10
	.word	.LANCHOR22
	.word	.LANCHOR23
	.word	.LANCHOR24
	.word	.LANCHOR25
	.word	.LANCHOR26
	.word	.LANCHOR27
	.word	.LANCHOR28
	.word	.LANCHOR29
	.word	.LANCHOR30
	.word	2049
	.word	.LANCHOR21
	.word	10497
	.word	536914273
	.word	16512
	.word	263264
	.word	537110911
	.word	537635177
	.word	263168
	.word	.LANCHOR31
	.word	.LANCHOR32
	.word	.LANCHOR33
	.word	.LANCHOR34
	.word	.LANCHOR35
	.word	.LANCHOR36
	.word	.LANCHOR37
	.word	.LANCHOR38
	.word	10593
	.word	353
	.word	.LANCHOR39
	.word	.LANCHOR40
	.word	.LANCHOR42
	.word	.LANCHOR46
	.word	.LANCHOR43
	.word	.LANCHOR44
	.word	.LANCHOR45
	.word	.LANCHOR47
	.word	.LANCHOR48
	.word	.LANCHOR49
	.word	.LANCHOR50
	.word	.LANCHOR51
	.word	.LANCHOR52
	.word	.LANCHOR53
	.word	537918973
	.word	.LANCHOR54
	.word	470399
	.word	207231
	.word	.LANCHOR55
	.word	GuiStruct_BitmapPtrList
	.word	.LANCHOR10
	.word	.LANCHOR22
	.word	.LANCHOR41
.L302:
	mla	r5, r3, r4, r5
	mov	r3, #15
.L370:
	strb	r3, [r5, #205]
	b	.L299
.L303:
	ldr	r0, .L389+200
	bl	GetItemByte
	mov	r3, #268
	mla	r5, r3, r4, r5
	strb	r0, [r5, #205]
	b	.L299
.L304:
	mla	r5, r3, r4, r5
	strb	r7, [r5, #205]
	b	.L299
.L305:
	mla	r5, r3, r4, r5
	mov	r3, #1
	strb	r3, [r5, #203]
.L299:
	ldr	r3, .L389+204
	mov	r5, #268
	mla	r5, r4, r5, r3
	ldrb	r3, [r5, #168]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L276
	ldr	r0, .L389+200
	bl	GetItemWord
	ldr	r3, .L389+192
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	ldr	r2, [r3, r0, asl #2]
	strh	r0, [r5, #206]	@ movhi
	ldrb	r3, [r2, #2]	@ zero_extendqisi2
	ldr	r0, .L389+200
	strh	r3, [r5, #208]	@ movhi
	ldrb	r2, [r2, #3]	@ zero_extendqisi2
	add	r3, r3, r2, asl #8
	strh	r3, [r5, #208]	@ movhi
	bl	GetItemByte
	and	r0, r0, #1
	cmp	r0, #0
	strb	r0, [r5, #210]
	beq	.L276
	ldr	r0, .L389+200
	bl	GetItemByte
	strb	r0, [r5, #211]
.L276:
	ldr	r0, .L389+200
	bl	GetItemByte
	ldr	sl, .L389+204
	mov	r6, #268
	mla	r5, r6, r4, sl
	add	r9, r5, #216
	cmp	r0, #0
	strb	r0, [r5, #216]
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
	ldr	r0, .L389+200
	bl	GetItemByte
	add	r9, r5, #224
	strb	r0, [r5, #217]
	ldr	r0, .L389+200
	bl	GetItemWord
	strh	r0, [r5, #218]	@ movhi
	ldr	r0, .L389+200
	bl	GetItemWord
	strh	r0, [r5, #220]	@ movhi
	ldr	r0, .L389+200
	bl	GetItemWord
	strh	r0, [r5, #222]	@ movhi
	ldr	r0, .L389+200
	bl	GetItemWord
	strh	r0, [r5, #224]	@ movhi
	ldr	r0, .L389+200
	bl	GetItemWord
	strh	r0, [r9, #2]	@ movhi
	ldr	r0, .L389+200
	bl	GetItemByte
	strb	r0, [r5, #228]
	and	r0, r0, #7
	mov	r5, sl
	cmp	r0, #4
	ldrls	pc, [pc, r0, asl #2]
	b	.L307
.L313:
	.word	.L308
	.word	.L309
	.word	.L310
	.word	.L311
	.word	.L312
.L308:
	ldr	r3, .L389+196
	mla	r6, r4, r6, sl
	ldrb	r3, [r3, #65]	@ zero_extendqisi2
	b	.L371
.L309:
	mla	r6, r4, r6, sl
	mov	r3, #0
	b	.L371
.L310:
	mla	r6, r4, r6, sl
	mov	r3, #15
.L371:
	strb	r3, [r6, #230]
	b	.L307
.L311:
	ldr	r0, .L389+200
	bl	GetItemByte
	mov	r3, #268
	mla	r5, r3, r4, r5
	strb	r0, [r5, #230]
	b	.L307
.L312:
	mla	r6, r4, r6, sl
	strb	r8, [r6, #230]
.L307:
	ldr	r5, .L389+204
	mov	r3, #268
	mla	r2, r3, r4, r5
	mov	r0, #0
	add	r1, r2, #228
	ldrb	r2, [r2, #228]	@ zero_extendqisi2
	strb	r0, [r1, #1]
	and	r2, r2, #56
	mov	r2, r2, asr #3
	cmp	r2, #5
	ldrls	pc, [pc, r2, asl #2]
	b	.L314
.L321:
	.word	.L315
	.word	.L316
	.word	.L317
	.word	.L318
	.word	.L319
	.word	.L320
.L315:
	mla	r5, r3, r4, r5
	ldr	r3, .L389+196
	ldrb	r3, [r3, #66]	@ zero_extendqisi2
	b	.L372
.L316:
	mla	r3, r4, r3, r5
	mov	r2, #0
	strb	r2, [r3, #231]
	b	.L314
.L317:
	mla	r5, r3, r4, r5
	mov	r3, #15
.L372:
	strb	r3, [r5, #231]
	b	.L314
.L318:
	ldr	r0, .L389+200
	bl	GetItemByte
	mov	r3, #268
	mla	r5, r3, r4, r5
	strb	r0, [r5, #231]
	b	.L314
.L319:
	mla	r5, r3, r4, r5
	strb	r7, [r5, #231]
	b	.L314
.L320:
	mla	r5, r3, r4, r5
	mov	r3, #1
	strb	r3, [r5, #229]
.L314:
	ldr	r5, .L389+200
	ldr	r9, .L389+204
	mov	r0, r5
	bl	GetItemByte
	mov	sl, #268
	mla	r6, sl, r4, r9
	add	fp, r6, #248
	strb	r0, [r6, #232]
	mov	r0, r5
	bl	GetItemWord
	strh	r0, [r6, #234]	@ movhi
	mov	r0, r5
	bl	GetItemWord
	strh	r0, [r6, #236]	@ movhi
	mov	r0, r5
	bl	GetItemWord
	strh	r0, [r6, #238]	@ movhi
	mov	r0, r5
	bl	GetItemWord
	ldr	r3, [r5, #0]
	str	r3, [r6, #260]
	strh	r0, [r6, #240]	@ movhi
	mov	r0, r5
	bl	GetItemByte
	strh	r0, [r6, #242]	@ movhi
	mov	r0, r5
	bl	GetItemByte
	add	r0, r0, #1
	strh	r0, [r6, #244]	@ movhi
	mov	r0, r5
	bl	GetItemWord
	strh	r0, [r6, #246]	@ movhi
	mov	r0, r5
	bl	GetItemWord
	strh	r0, [r6, #248]	@ movhi
	mov	r0, r5
	bl	GetItemByte
	mov	r5, r9
	strb	r0, [fp, #2]
	and	r0, r0, #7
	cmp	r0, #4
	ldrls	pc, [pc, r0, asl #2]
	b	.L322
.L328:
	.word	.L323
	.word	.L324
	.word	.L325
	.word	.L326
	.word	.L327
.L323:
	ldr	r3, .L389+196
	mla	sl, r4, sl, r9
	ldrb	r3, [r3, #65]	@ zero_extendqisi2
	b	.L373
.L324:
	mla	sl, r4, sl, r9
	mov	r3, #0
.L373:
	strb	r3, [sl, #252]
	b	.L322
.L325:
	mla	sl, r4, sl, r9
	mov	r3, #15
	b	.L373
.L326:
	ldr	r0, .L389+200
	bl	GetItemByte
	mov	r3, #268
	mla	r5, r3, r4, r5
	strb	r0, [r5, #252]
	b	.L322
.L327:
	mla	sl, r4, sl, r9
	strb	r8, [sl, #252]
.L322:
	ldr	r5, .L389+204
	mov	r3, #268
	mla	r2, r3, r4, r5
	mov	r1, #0
	strb	r1, [r2, #251]
	add	r2, r2, #248
	ldrb	r2, [r2, #2]	@ zero_extendqisi2
	and	r2, r2, #56
	mov	r2, r2, asr #3
	cmp	r2, #5
	ldrls	pc, [pc, r2, asl #2]
	b	.L329
.L336:
	.word	.L330
	.word	.L331
	.word	.L332
	.word	.L333
	.word	.L334
	.word	.L335
.L330:
	mla	r5, r3, r4, r5
	ldr	r3, .L389+196
	ldrb	r3, [r3, #66]	@ zero_extendqisi2
	b	.L374
.L331:
	mla	r3, r4, r3, r5
	mov	r2, #0
	strb	r2, [r3, #253]
	b	.L329
.L332:
	mla	r5, r3, r4, r5
	mov	r3, #15
.L374:
	strb	r3, [r5, #253]
	b	.L329
.L333:
	ldr	r0, .L389+200
	bl	GetItemByte
	mov	r3, #268
	mla	r5, r3, r4, r5
	strb	r0, [r5, #253]
	b	.L329
.L334:
	mla	r5, r3, r4, r5
	strb	r7, [r5, #253]
	b	.L329
.L335:
	mla	r5, r3, r4, r5
	mov	r3, #1
	strb	r3, [r5, #251]
.L329:
	ldr	r3, .L389+204
	mov	r5, #268
	mla	r5, r4, r5, r3
	ldrb	r3, [r5, #216]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L337
	ldr	r0, .L389+200
	bl	GetItemWord
	strh	r0, [r5, #254]	@ movhi
	ldr	r0, .L389+200
	bl	GetItemByte
	and	r0, r0, #1
	cmp	r0, #0
	strb	r0, [r5, #256]
	beq	.L337
	ldr	r0, .L389+200
	bl	GetItemByte
	strb	r0, [r5, #257]
.L337:
	ldr	r3, .L389+204
	mov	r2, #268
	mla	r4, r2, r4, r3
	mvn	r3, #0
	add	r4, r4, #256
	strh	r3, [r4, #2]	@ movhi
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.LFE80:
	.size	ReadItem, .-ReadItem
	.global	__divsi3
	.section	.text.TextBox_Scroll_CalcEndPos,"ax",%progbits
	.align	2
	.type	TextBox_Scroll_CalcEndPos, %function
TextBox_Scroll_CalcEndPos:
.LFB146:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L395
	mov	r2, #100
	mla	r3, r2, r0, r3
	cmp	r1, #0
	ldrh	r0, [r3, #16]
	ldrh	r2, [r3, #12]
	add	r0, r0, #1
	rsb	r0, r2, r0
	add	r2, r3, #84
	stmfd	sp!, {r4, r5, lr}
.LCFI11:
	ldrh	r4, [r3, #92]
	ldreqsb	r3, [r2, #2]
	mov	r0, r0, asl #16
	muleq	r4, r3, r4
	mov	r0, r0, lsr #16
	rsbeq	r0, r0, r4
	beq	.L394
	ldrsb	r5, [r2, #2]
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	mov	r1, r5
	bl	__divsi3
	rsb	r0, r0, r4
	mul	r0, r5, r0
.L394:
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	ldmfd	sp!, {r4, r5, pc}
.L396:
	.align	2
.L395:
	.word	.LANCHOR56
.LFE146:
	.size	TextBox_Scroll_CalcEndPos, .-TextBox_Scroll_CalcEndPos
	.global	__modsi3
	.section	.text.DataNumStr,"ax",%progbits
	.align	2
	.type	DataNumStr, %function
DataNumStr:
.LFB74:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L547
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI12:
	mov	r2, #0
	sub	sp, sp, #36
.LCFI13:
	mov	r8, r0
	str	r1, [sp, #12]
	strb	r2, [r3, #0]
	cmp	r1, #8
	ldrls	pc, [pc, r1, asl #2]
	b	.L493
.L404:
	.word	.L399
	.word	.L399
	.word	.L400
	.word	.L401
	.word	.L402
	.word	.L493
	.word	.L403
	.word	.L403
	.word	.L403
.L399:
	cmp	r0, #255
	movgt	r4, #0
	movgt	r8, #255
	bgt	.L398
	b	.L540
.L400:
	cmp	r0, #127
	movgt	r8, #127
	bgt	.L493
	cmn	r8, #128
	movlt	r4, #1
	mvnlt	r8, #127
	blt	.L398
	b	.L542
.L401:
	ldr	r3, .L547+4
	cmp	r0, r3
	movgt	r4, #0
	movgt	r8, r3
	bgt	.L398
.L540:
	cmp	r8, #0
	movlt	r8, #0
	movlt	r4, r8
	bge	.L493
	b	.L398
.L402:
	ldr	r3, .L547+8
	cmp	r0, r3
	movgt	r8, r3
	bgt	.L493
	cmn	r8, #32768
	movlt	r4, #1
	ldrlt	r8, .L547+12
	blt	.L398
.L542:
	cmp	r8, #0
	bge	.L493
	b	.L543
.L403:
	mov	r4, r0, lsr #31
	b	.L398
.L493:
	mov	r4, #0
	b	.L398
.L543:
	mov	r4, #1
.L398:
	ldr	r3, .L547+16
	ldrb	r7, [r3, #75]	@ zero_extendqisi2
	cmp	r7, #1
	sub	r6, r7, #3
	ldrlsb	r5, [r3, #73]	@ zero_extendqisi2
	movhi	r5, #0
	cmp	r6, #6
	movhi	r6, #0
	movls	r6, #1
	cmp	r7, #1
	movhi	r4, #0
	ldr	r3, .L547+16
	strhi	r4, [sp, #16]
	bhi	.L408
	ldr	r2, [r3, #60]
	tst	r2, #8
	moveq	r1, #0
	movne	r1, #1
	str	r1, [sp, #16]
.L408:
	ldrb	r3, [r3, #74]	@ zero_extendqisi2
	cmp	r3, #2
	movne	r3, #0
	bne	.L533
	ldr	r3, .L547+16
	ldr	r3, [r3, #60]
	tst	r3, #16
	moveq	r3, #0
	movne	r3, #1
.L533:
	cmp	r7, #1
	str	r3, [sp, #8]
	movhi	r3, #0
	bhi	.L534
	ldr	r3, .L547+16
	ldr	r3, [r3, #60]
	tst	r3, #4096
	moveq	r3, #0
	movne	r3, #1
.L534:
	sub	r2, r7, #2
	str	r3, [sp, #20]
	rsbs	r3, r2, #0
	adc	r3, r3, r2
	cmp	r7, #0
	movne	r2, r3
	orreq	r2, r3, #1
	cmp	r2, #0
	streq	r2, [sp, #24]
	beq	.L411
	ldr	r2, .L547+16
	ldr	r2, [r2, #60]
	tst	r2, #8192
	moveq	r1, #0
	movne	r1, #1
	str	r1, [sp, #24]
.L411:
	cmp	r7, #1
	bhi	.L412
	cmp	r4, #0
	rsbne	r0, r8, #0
	moveq	r0, r8
	ldr	r1, .L547
	mov	r2, #10
	bl	ConvertIntToStr
	b	.L414
.L412:
	cmp	r3, #0
	beq	.L415
	mov	r2, #16
	mov	r0, r8
	ldr	r1, .L547
	bl	ConvertIntToStr
	ldr	r2, .L547
	mov	r3, #0
	b	.L416
.L418:
	sub	r1, ip, #97
	cmp	r1, #5
	add	r3, r3, #1
	subls	ip, ip, #32
	and	r3, r3, #255
	strlsb	ip, [r2, #-1]
.L416:
	ldrb	ip, [r2], #1	@ zero_extendqisi2
	cmp	ip, #0
	bne	.L418
	cmp	r8, #0
	bge	.L419
	ldr	r2, .L547+16
	ldr	r0, .L547
	ldrb	r8, [r2, #72]	@ zero_extendqisi2
	mov	r7, r0
	b	.L420
.L424:
	ldrb	r2, [r0, #0]	@ zero_extendqisi2
	cmp	r2, #70
	beq	.L510
	b	.L419
.L422:
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	add	r2, r2, #1
	strb	sl, [r1], #1
	b	.L421
.L510:
	ldr	r1, .L547
	mov	r2, #0
.L421:
	cmp	r2, r3
	blt	.L422
	strb	ip, [r7, r3]
	sub	r3, r3, #1
	and	r3, r3, #255
.L420:
	cmp	r3, #1
	bls	.L423
	cmp	r3, r8
	bcs	.L424
.L419:
	cmp	r3, #15
	bhi	.L524
.L423:
	ldr	r0, .L547
	ldr	r1, .L547+20
	b	.L536
.L415:
	cmp	r6, #0
	moveq	r7, r6
	beq	.L425
	cmp	r7, #5
	cmpne	r7, #7
	mov	r0, r8
	beq	.L426
	cmp	r7, #9
	bne	.L427
.L426:
	ldr	r1, .L547+24
	bl	__modsi3
	mov	r1, #3600
	mov	r8, r0
	bl	__divsi3
	mov	r1, #3600
	mov	fp, r0, asl #16
	mov	r0, r8
	bl	__modsi3
	mov	r1, #60
	bl	__divsi3
	mov	r1, #60
	mov	fp, fp, lsr #16
	mov	r0, r0, asl #16
	mov	r9, r0, lsr #16
	mov	r0, r8
	bl	__modsi3
	mov	r0, r0, asl #16
	mov	sl, r0, lsr #16
	b	.L428
.L427:
	ldr	r1, .L547+28
	bl	__modsi3
	mov	r1, #60
	ldr	sl, .L547+4
	mov	r8, r0
	bl	__divsi3
	mov	r1, #60
	mov	fp, r0, asl #16
	mov	r0, r8
	bl	__modsi3
	mov	fp, fp, lsr #16
	mov	r0, r0, asl #16
	mov	r9, r0, lsr #16
.L428:
	sub	r7, r7, #6
	cmp	r7, #3
	movhi	r8, #0
	bhi	.L429
	mov	fp, fp, asl #16
	mov	r0, fp, asr #16
	cmp	r0, #11
	mov	r1, #12
	movgt	r8, #0
	movle	r8, #1
	bl	__modsi3
	mov	fp, r0, asl #16
	cmp	fp, #0
	movne	fp, fp, lsr #16
	moveq	fp, #12
.L429:
	ldr	r3, [sp, #8]
	mov	fp, fp, asl #16
	mov	fp, fp, asr #16
	cmp	fp, #9
	movgt	r3, #0
	andle	r3, r3, #1
	cmp	r3, #0
	beq	.L430
	ldr	r0, .L547
	ldr	r1, .L547+32
	bl	strcat
.L430:
	ldr	r7, .L547
	mov	r9, r9, asl #16
	mov	r0, r7
	bl	strlen
	mov	r2, #10
	mov	r9, r9, asr #16
	add	r1, r7, r0
	mov	r0, fp
	bl	ConvertIntToStr
	mov	r0, r7
	ldr	r1, .L547+36
	bl	strcat
	cmp	r9, #9
	bgt	.L431
	mov	r0, r7
	ldr	r1, .L547+32
	bl	strcat
.L431:
	ldr	r0, .L547
	bl	strlen
	mov	r2, #10
	add	r1, r7, r0
	mov	r0, r9
	mov	r7, sl, asl #16
	bl	ConvertIntToStr
	movs	r7, r7, asr #16
	bmi	.L432
	ldr	r0, .L547
	ldr	r1, .L547+36
	bl	strcat
	cmp	r7, #9
	bgt	.L433
	ldr	r0, .L547
	ldr	r1, .L547+32
	bl	strcat
.L433:
	ldr	r7, .L547
	mov	sl, sl, asl #16
	mov	r0, r7
	bl	strlen
	mov	r2, #10
	add	r1, r7, r0
	mov	r0, sl, asr #16
	bl	ConvertIntToStr
.L432:
	ldr	r3, .L547+16
	ldrb	r3, [r3, #75]	@ zero_extendqisi2
	sub	r2, r3, #6
	cmp	r2, #1
	bhi	.L434
	cmp	r8, #0
	ldr	r0, .L547
	ldrne	r1, .L547+40
	ldreq	r1, .L547+44
	b	.L536
.L434:
	sub	r3, r3, #8
	cmp	r3, #1
	bhi	.L414
	cmp	r8, #0
	ldr	r0, .L547
	ldrne	r1, .L547+48
	ldreq	r1, .L547+52
.L536:
	bl	strcat
.L414:
	ldr	r1, .L547
	mov	r3, #0
.L437:
	and	r7, r3, #255
	add	r3, r3, #1
	add	r2, r1, r3
	ldrb	r2, [r2, #-1]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L437
	cmp	r5, #0
	moveq	r8, r7
	beq	.L449
	add	r3, r5, #1
	and	r3, r3, #255
	cmp	r7, r3
	bcs	.L439
	cmp	r3, #16
	bhi	.L522
	ldr	r1, .L547
	add	r0, r1, r7
	add	r1, r1, r3
	b	.L440
.L441:
	ldrb	ip, [r0, r2]	@ zero_extendqisi2
	strb	ip, [r1, r2]
.L440:
	sub	r2, r2, #1
	rsb	ip, r2, #0
	cmp	ip, r7
	ble	.L441
	mov	r2, #0
	rsb	r7, r7, r3
	ldr	r0, .L547
	mov	r1, #48
	b	.L442
.L443:
	strb	r1, [r2, r0]
	add	r2, r2, #1
.L442:
	cmp	r2, r7
	blt	.L443
	mov	r7, r3
.L439:
	ldr	r3, [sp, #20]
	ldr	r1, [sp, #12]
	orrs	r3, r3, r6
	movne	r3, #0
	moveq	r3, #1
	cmp	r1, #0
	moveq	r3, #0
	andne	r3, r3, #1
	cmp	r3, #0
	beq	.L444
	ldr	r3, .L547+16
	ldr	r1, .L547
	ldrb	r0, [r3, #73]	@ zero_extendqisi2
	sub	r2, r7, #1
	rsb	r0, r0, r7
	mov	r3, #0
	add	r1, r1, r2
	b	.L445
.L446:
	sub	r3, r3, #1
	add	ip, r1, r3
	ldrb	ip, [ip, #1]	@ zero_extendqisi2
	cmp	ip, #48
	bne	.L444
	sub	r5, r5, #1
	sub	r7, r7, #1
	and	r5, r5, #255
	and	r7, r7, #255
.L445:
	add	ip, r3, r2
	cmp	ip, r0
	bgt	.L446
.L444:
	cmp	r7, #15
	bhi	.L524
	ldr	r2, .L547
	rsb	r5, r5, r7
	mov	r5, r5, asl #16
	mov	r8, r5, lsr #16
	add	r2, r2, r7
	mov	r3, #0
	mov	r5, r5, asr #16
	b	.L447
.L448:
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	sub	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strb	r1, [r2], #-1
.L447:
	add	r1, r3, r7
	mov	r1, r1, asl #16
	cmp	r5, r1, asr #16
	blt	.L448
	ldr	r3, .L547
	mov	r2, #46
	add	r7, r7, #1
	strb	r2, [r3, r5]
	and	r7, r7, #255
	mov	r2, #0
	strb	r2, [r3, r7]
.L449:
	ldr	r3, [sp, #24]
	cmp	r3, #0
	bne	.L517
	b	.L451
.L455:
	cmp	sl, #0
	beq	.L452
	mov	r0, sl, asl #16
	mov	r1, #3
	mov	r0, r0, asr #16
	str	r3, [sp, #4]
	str	ip, [sp, #0]
	bl	__modsi3
	ldr	r3, [sp, #4]
	ldr	ip, [sp, #0]
	mov	r2, r0, asl #16
	movs	r2, r2, asr #16
	addeq	r1, r9, r7
	moveq	r0, r3
	beq	.L453
	b	.L452
.L454:
	ldrb	r3, [r1, #-1]	@ zero_extendqisi2
	sub	r2, r2, #1
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	strb	r3, [r1], #-1
.L453:
	add	r3, r2, r7
	mov	r3, r3, asl #16
	cmp	r0, r3, asr #16
	blt	.L454
	add	r7, r7, #1
	and	r7, r7, #255
	cmp	r7, #15
	strb	ip, [r4, r5]
	bhi	.L524
	mov	r1, #0
	strb	r1, [r9, r7]
.L452:
	add	sl, sl, #1
	sub	r8, r8, #1
	mov	sl, sl, asl #16
	mov	r8, r8, asl #16
	mov	sl, sl, lsr #16
	mov	r8, r8, lsr #16
	sub	r5, r5, #1
	b	.L450
.L517:
	ldr	r9, .L547
	mov	r3, r8, asl #16
	mov	r5, #0
	add	r3, r9, r3, asr #16
	mov	fp, r4
	mov	sl, r5
	mov	ip, #44
	mov	r4, r3
.L450:
	mov	r3, r8, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #0
	bgt	.L455
	mov	r4, fp
.L451:
	ldr	r3, [sp, #16]
	orrs	r3, r3, r4
	beq	.L456
	cmp	r7, #16
	ldrls	r2, .L547
	addls	r3, r2, r7
	bls	.L457
	b	.L524
.L458:
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	strb	r1, [r3], #-1
.L457:
	cmp	r3, r2
	bne	.L458
	ldr	r3, .L547
	cmp	r4, #0
	add	r7, r7, #1
	moveq	r4, #43
	movne	r4, #45
	and	r7, r7, #255
	mov	r2, #0
	strb	r4, [r3, #0]
	strb	r2, [r3, r7]
.L456:
	ldr	r3, .L547+16
	ldrb	r3, [r3, #75]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L460
	ldr	r3, .L547+56
	add	r4, sp, #28
	ldrh	r0, [r3, #0]
	ldrsh	r3, [r3, #0]
	mov	r1, r4
	cmp	r3, #0
	rsblt	r0, r0, #0
	movlt	r0, r0, asl #16
	movlt	r0, r0, lsr #16
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	mov	r2, #10
	bl	ConvertIntToStr
	mov	r3, #0
.L462:
	mov	r5, r3, asl #16
	add	r3, r3, #1
	add	r2, r4, r3
	ldrb	r2, [r2, #-1]	@ zero_extendqisi2
	mov	r5, r5, lsr #16
	cmp	r2, #0
	bne	.L462
	cmp	r5, #1
	movne	r3, r5
	moveq	r3, #2
	add	r1, r7, #2
	mov	r3, r3, asl #16
	add	r3, r1, r3, asr #16
	cmp	r3, #15
	bgt	.L522
	ldr	r1, .L547+60
	ldr	r0, .L547
	bl	strcat
	ldr	r3, .L547+56
	ldr	r0, .L547
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	ldrge	r1, .L547+64
	ldrlt	r1, .L547+68
	bl	strcat
	cmp	r5, #1
	addne	r7, r7, #2
	bne	.L538
	ldr	r0, .L547
	ldr	r1, .L547+32
	bl	strcat
	add	r7, r7, #3
.L538:
	ldr	r0, .L547
	add	r1, sp, #28
	bl	strcat
	and	r7, r7, #255
	add	r7, r7, r5
	and	r7, r7, #255
.L460:
	ldr	r2, .L547+16
	ldrb	r3, [r2, #72]	@ zero_extendqisi2
	cmp	r3, #0
	ldreq	r2, .L547
	streqb	r3, [r2, r7]
	beq	.L425
	cmp	r7, r3
	bhi	.L523
	b	.L544
.L471:
	strb	r1, [r2, r0]
	add	r2, r2, #1
	b	.L469
.L523:
	ldr	r0, .L547
	mov	r2, #0
	mov	r1, #45
.L469:
	cmp	r2, r3
	blt	.L471
	b	.L472
.L544:
	cmp	r3, #16
	bhi	.L524
	ldr	r1, [sp, #8]
	eor	r6, r6, #1
	ands	r6, r1, r6
	beq	.L473
	ldr	r1, .L547
	mov	r2, #0
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	add	ip, r1, r7
	cmp	r0, #45
	cmpne	r0, #43
	movne	r0, #0
	moveq	r0, #1
	rsb	r4, r0, r7
	add	r1, r1, r3
	b	.L474
.L475:
	ldrb	r5, [ip, r2]	@ zero_extendqisi2
	strb	r5, [r1, r2]
.L474:
	sub	r2, r2, #1
	rsb	r5, r2, #0
	cmp	r5, r4
	ble	.L475
	ldr	r1, .L547
	mov	r2, #0
	add	r0, r1, r0
	rsb	r7, r7, r3
	mov	r1, #48
	b	.L476
.L477:
	strb	r1, [r0, r2]
	add	r2, r2, #1
.L476:
	cmp	r2, r7
	blt	.L477
	b	.L472
.L473:
	ldrb	r2, [r2, #74]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L525
	b	.L545
.L480:
	strb	r1, [r0, r2]
	add	r2, r2, #1
	b	.L478
.L525:
	ldr	r0, .L547
	mov	r1, #32
	add	r0, r0, r7
.L478:
	add	ip, r2, r7
	cmp	ip, r3
	blt	.L480
	b	.L472
.L545:
	cmp	r2, #1
	bne	.L481
	rsb	r1, r7, r3
	add	r1, r1, r1, lsr #31
	mov	r1, r1, asr #1
	ands	r1, r1, #255
	beq	.L482
	ldr	r0, .L547
	sub	ip, r7, #1
	add	r5, r0, ip
	add	ip, r1, ip
	mov	r2, r6
	add	r0, r0, ip
	b	.L483
.L484:
	ldrb	ip, [r5, r6]	@ zero_extendqisi2
	strb	ip, [r0, r6]
	sub	r6, r6, #1
.L483:
	sub	r2, r2, #1
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	add	ip, r2, r7
	tst	ip, #32768
	ldr	r4, .L547
	beq	.L484
	mov	r2, #0
	mov	r0, #32
	b	.L485
.L486:
	strb	r0, [r4, r2]
	add	r2, r2, #1
.L485:
	cmp	r2, r1
	blt	.L486
.L482:
	rsb	r7, r7, r3
	rsb	r1, r1, r7
	ands	r1, r1, #255
	beq	.L472
	ldr	ip, .L547
	rsb	r1, r1, r3
	mov	r2, #0
	add	ip, ip, r1
	mov	r0, #32
	b	.L487
.L488:
	strb	r0, [ip, r2]
	add	r2, r2, #1
.L487:
	add	r4, r2, r1
	cmp	r4, r3
	blt	.L488
	b	.L472
.L481:
	cmp	r2, #2
	bne	.L472
	b	.L546
.L490:
	ldrb	r0, [r1, r6]	@ zero_extendqisi2
	strb	r0, [r2, r6]
	b	.L489
.L546:
	ldr	r2, .L547
	add	r1, r2, r7
	add	r2, r2, r3
.L489:
	sub	r6, r6, #1
	rsb	r0, r6, #0
	cmp	r0, r7
	ldr	r0, .L547
	ble	.L490
	mov	r2, #0
	rsb	r7, r7, r3
	mov	r1, #32
	b	.L491
.L492:
	strb	r1, [r0, r2]
	add	r2, r2, #1
.L491:
	cmp	r2, r7
	blt	.L492
.L472:
	ldr	r2, .L547
	mov	r1, #0
	strb	r1, [r2, r3]
	mov	r7, r3
	b	.L425
.L522:
	mov	r7, r2
	b	.L425
.L524:
	mov	r7, #0
.L425:
	mov	r0, r7
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L548:
	.align	2
.L547:
	.word	.LANCHOR57
	.word	65535
	.word	32767
	.word	-32768
	.word	.LANCHOR10
	.word	.LC1
	.word	360000
	.word	6000
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LANCHOR20
	.word	.LC8
	.word	.LC9
	.word	.LC10
.LFE74:
	.size	DataNumStr, .-DataNumStr
	.section	.text.CheckRect,"ax",%progbits
	.align	2
	.type	CheckRect, %function
CheckRect:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L560
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI14:
	ldrb	ip, [ip, #0]	@ zero_extendqisi2
	cmp	ip, #0
	movne	ip, #0
	bne	.L550
	ldr	r4, .L560+4
	ldrsh	r8, [r0, #0]
	ldrh	r6, [r4, #0]
	ldrsh	r4, [r4, #0]
	cmp	r8, r4
	bgt	.L550
	ldr	r4, .L560+8
	ldrsh	r5, [r2, #0]
	ldrsh	r7, [r4, #0]
	ldrh	fp, [r4, #0]
	cmp	r5, r7
	blt	.L550
	ldr	r5, .L560+12
	ldrsh	sl, [r1, #0]
	ldrh	r4, [r5, #0]
	ldrsh	r5, [r5, #0]
	cmp	sl, r5
	bgt	.L550
	ldr	sl, .L560+16
	ldrsh	r9, [r3, #0]
	ldrh	r5, [sl, #0]
	ldrsh	sl, [sl, #0]
	cmp	r9, sl
	blt	.L550
.LBB122:
	cmp	r8, r7
	strlth	fp, [r0, #0]	@ movhi
	ldrsh	r0, [r2, #0]
	mov	ip, r6, asl #16
	cmp	r0, ip, asr #16
	strgth	r6, [r2, #0]	@ movhi
	ldrsh	r2, [r1, #0]
	mov	r0, r5, asl #16
	cmp	r2, r0, asr #16
	strlth	r5, [r1, #0]	@ movhi
	ldrsh	r2, [r3, #0]
	mov	r1, r4, asl #16
	cmp	r2, r1, asr #16
	mov	ip, #1
	strgth	r4, [r3, #0]	@ movhi
.L550:
.LBE122:
	mov	r0, ip
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L561:
	.align	2
.L560:
	.word	.LANCHOR58
	.word	.LANCHOR59
	.word	.LANCHOR60
	.word	.LANCHOR61
	.word	.LANCHOR62
.LFE2:
	.size	CheckRect, .-CheckRect
	.section	.text.MakeDot,"ax",%progbits
	.align	2
	.type	MakeDot, %function
MakeDot:
.LFB6:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI15:
	strh	r0, [sp, #4]	@ movhi
	add	r0, sp, #4
	strh	r1, [sp, #0]	@ movhi
	mov	r4, r2
	mov	r1, sp
	mov	r2, r0
	mov	r3, sp
	bl	CheckRect
	cmp	r0, #0
	beq	.L562
	ldrsh	r3, [sp, #4]
	ldrh	r0, [sp, #4]
	add	r3, r3, r3, lsr #31
	mov	r3, r3, asl #15
	mov	r3, r3, lsr #16
	add	lr, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	ands	r3, r3, #-2147483647
	submi	r3, r3, #1
	mvnmi	r3, r3, asl #31
	mvnmi	r3, r3, lsr #31
	mov	ip, r0, asl #16
	addmi	r3, r3, #1
	mov	ip, ip, asr #16
	sub	lr, lr, r3, asl #1
	ands	r3, ip, #-2147483647
	submi	r3, r3, #1
	mvnmi	r3, r3, asl #31
	mvnmi	r3, r3, lsr #31
	ldr	ip, .L566
	ldrsh	r1, [sp, #0]
	ldr	r2, .L566+4
	addmi	r3, r3, #1
	mov	r3, r3, asl #16
	mov	lr, lr, asl #16
	ldrb	ip, [ip, r3, asr #16]	@ zero_extendqisi2
	mov	lr, lr, asr #16
	mov	r3, #160
	ldr	r2, [r2, #0]
	mla	r3, r1, r3, lr
	ldr	r5, .L566+8
	ldrb	lr, [r2, r3]	@ zero_extendqisi2
	ldrb	r4, [r5, r4]	@ zero_extendqisi2
	bic	lr, lr, ip
	mov	r0, r0, asl #16
	and	ip, ip, r4
	orr	ip, lr, ip
	mov	r0, r0, asr #16
	strb	ip, [r2, r3]
	mov	r2, r0
	mov	r3, r1
	bl	MarkDisplayBoxRepaint
.L562:
	ldmfd	sp!, {r2, r3, r4, r5, pc}
.L567:
	.align	2
.L566:
	.word	.LANCHOR4
	.word	guilib_display_ptr
	.word	.LANCHOR3
.LFE6:
	.size	MakeDot, .-MakeDot
	.section	.text.DrawChar.isra.1,"ax",%progbits
	.align	2
	.type	DrawChar.isra.1, %function
DrawChar.isra.1:
.LFB165:
	@ args = 0, pretend = 0, frame = 68
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L591
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI16:
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	sub	sp, sp, #68
.LCFI17:
	cmp	r5, #0
	bne	.L568
	ldrb	r4, [r2, #11]	@ zero_extendqisi2
	cmp	r4, #0
	beq	.L568
	ldrb	ip, [r2, #13]	@ zero_extendqisi2
	cmp	ip, #0
	beq	.L568
.LBB125:
	and	r3, r3, #15
	str	r3, [sp, #4]
	adds	r3, ip, #7
	ldr	r6, .L591+4
	addmi	r3, ip, #14
	movs	r3, r3, lsr #3
	add	fp, r2, #14
	moveq	r3, #1
	ldrh	r6, [r6, #0]
	add	fp, fp, r3
	adds	r3, r4, #7
	addmi	r4, r4, #14
	movpl	r4, r3
	mov	r4, r4, asr #3
	and	r3, r4, #255
	add	r0, r0, r6
	ldr	r6, .L591+8
	str	r3, [sp, #24]
	ldrb	r3, [r2, #10]	@ zero_extendqisi2
	ldrh	r6, [r6, #0]
	add	r0, r0, r3
	ldrb	r3, [r2, #12]	@ zero_extendqisi2
	add	r1, r1, r6
	add	r1, r1, r3
	ldr	r3, .L591+12
	ldr	r6, [sp, #4]
	ldrsh	r3, [r3, #0]
	str	r4, [sp, #8]
	str	r3, [sp, #32]
	ldr	r3, .L591+16
	ldrb	r4, [r2, #14]	@ zero_extendqisi2
	ldrsh	r3, [r3, #0]
	mov	r0, r0, asl #16
	str	r3, [sp, #36]
	ldr	r3, .L591+20
	mov	r0, r0, lsr #16
	ldr	r3, [r3, #0]
	mov	r1, r1, asl #16
	str	r3, [sp, #44]
	ldr	r3, .L591+24
	add	r2, r2, #15
	ldrsh	r3, [r3, #0]
	mov	r6, r6, asl #4
	str	r3, [sp, #60]
	ldr	r3, .L591+28
	str	r0, [sp, #28]
	ldrsh	r3, [r3, #0]
	mov	r1, r1, lsr #16
	mov	r4, r4, lsr #1
	str	r2, [sp, #12]
	str	r6, [sp, #40]
	mov	r0, r5
	str	r3, [sp, #64]
	b	.L571
.L587:
	ldr	r8, [sp, #12]
	mov	r3, #0
	mov	r2, r3
	sub	r6, ip, #1
.L575:
	add	r2, r2, #1
	mov	r2, r2, asl #16
	tst	r4, #1
	mov	r2, r2, lsr #16
	moveq	sl, #1
	beq	.L572
	add	sl, r3, r7
	cmp	sl, r6
	movlt	sl, #0
	movge	sl, #1
.L572:
	add	r5, r2, r0
	mov	r5, r5, asl #16
	mov	r9, r5, lsr #16
	and	r5, r5, #458752
	cmp	r5, #458752
	and	sl, sl, #255
	ldreqb	r4, [r8], #1	@ zero_extendqisi2
	movne	r4, r4, lsr #1
	cmp	sl, #0
	str	r9, [sp, #52]
	add	r3, r3, #1
	beq	.L575
	mov	r3, r2, asl #16
	mov	r3, r3, asr #16
	str	r8, [sp, #12]
	ldr	r9, [sp, #28]
	mov	r5, #0
	str	r3, [sp, #56]
	mov	sl, ip
	b	.L576
.L584:
	ldrb	ip, [fp, r5]	@ zero_extendqisi2
	cmp	ip, #0
	str	ip, [sp, #16]
	beq	.L577
	mov	r0, #0
.L583:
	ldr	r6, [sp, #32]
	mov	r3, r9, asl #16
	add	r3, r0, r3, asr #16
	cmp	r3, r6
	blt	.L578
	ldr	r7, [sp, #36]
	cmp	r3, r7
	bgt	.L578
	ldr	r8, [sp, #16]
	rsb	ip, r0, #7
	mov	ip, r8, asr ip
	tst	ip, #1
	beq	.L578
	add	ip, r3, r3, lsr #31
	mov	ip, ip, asr #1
	add	r6, ip, #1
	and	ip, ip, #1
	sub	r7, r6, ip, asl #1
	mov	r7, r7, asl #16
	ldr	r6, [sp, #4]
	ldr	ip, [sp, #40]
	mov	r7, r7, lsr #16
	tst	r3, #1
	mov	r7, r7, asl #16
	movne	ip, r6
	mov	r8, #160	@ movhi
	ands	r3, r3, #-2147483647
	mov	r7, r7, asr #16
	submi	r3, r3, #1
	smlabb	r7, r1, r8, r7
	ldr	r8, [sp, #44]
	mvnmi	r3, r3, asl #31
	mvnmi	r3, r3, lsr #31
	str	ip, [sp, #48]
	addmi	r3, r3, #1
	add	r7, r8, r7
	mov	r6, r1
	mov	ip, #0
	str	r7, [sp, #20]
	str	r3, [sp, #0]
.L582:
	ldr	r3, [sp, #60]
	mov	r7, r6, asl #16
	mov	r7, r7, asr #16
	cmp	r7, r3
	blt	.L581
	ldr	r8, [sp, #64]
	cmp	r7, r8
	bgt	.L581
	ldr	r7, .L591+32
	ldr	r3, [sp, #0]
	ldrb	r8, [r7, r3]	@ zero_extendqisi2
	ldr	r3, [sp, #20]
	ldrb	r7, [r3, ip]	@ zero_extendqisi2
	bic	r7, r7, r8
	ldr	r8, [sp, #48]
	orr	r7, r7, r8
	strb	r7, [r3, ip]
.L581:
	add	r6, r6, #1
	mov	r6, r6, asl #16
	mov	r6, r6, lsr #16
	ldr	r3, [sp, #56]
	rsb	r7, r1, r6
	mov	r7, r7, asl #16
	cmp	r3, r7, asr #16
	add	ip, ip, #160
	bgt	.L582
.L578:
	add	r0, r0, #1
	cmp	r0, #8
	bne	.L583
.L577:
	add	r9, r9, #8
	mov	r9, r9, asl #16
	mov	r9, r9, lsr #16
	add	r5, r5, #1
.L576:
	ldr	r6, [sp, #24]
	cmp	r5, r6
	blt	.L584
	ldr	r7, [sp, #8]
	add	r1, r2, r1
	ldr	r0, [sp, #52]
	mov	r1, r1, asl #16
	mov	ip, sl
	add	fp, fp, r7
	mov	r1, r1, lsr #16
.L571:
	mov	r7, r0, asl #16
	mov	r7, r7, asr #16
	cmp	r7, ip
	blt	.L587
.L568:
.LBE125:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L592:
	.align	2
.L591:
	.word	.LANCHOR58
	.word	.LANCHOR63
	.word	.LANCHOR64
	.word	.LANCHOR60
	.word	.LANCHOR59
	.word	guilib_display_ptr
	.word	.LANCHOR62
	.word	.LANCHOR61
	.word	.LANCHOR4
.LFE165:
	.size	DrawChar.isra.1, .-DrawChar.isra.1
	.section	.text.ShowBitmapArea.isra.3,"ax",%progbits
	.align	2
	.type	ShowBitmapArea.isra.3, %function
ShowBitmapArea.isra.3:
.LFB167:
	@ args = 16, pretend = 0, frame = 76
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI18:
	ldrb	r5, [r0, #1]	@ zero_extendqisi2
	mov	r4, r0
	ldrb	r0, [r0, #0]	@ zero_extendqisi2
	ldrb	r6, [r4, #3]	@ zero_extendqisi2
	add	r5, r0, r5, asl #8
	ldrb	r0, [r4, #2]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	mov	r5, r5, asl #16
	add	r6, r0, r6, asl #8
	mov	r1, r1, lsr #16
	ldr	r0, .L627
	mov	r7, r5, lsr #16
	sub	ip, r1, #1
	add	ip, r7, ip
	mov	r2, r2, asl #16
	sub	sp, sp, #76
.LCFI19:
	strh	ip, [r0, #0]	@ movhi
	mov	r2, r2, lsr #16
	ldr	r0, .L627+4
	mov	r6, r6, asl #16
	sub	ip, r2, #1
	strh	r3, [sp, #64]	@ movhi
	mov	r6, r6, lsr #16
	ldr	r3, [sp, #112]
	add	ip, r6, ip
	strh	ip, [r0, #0]	@ movhi
	ldr	r0, .L627+8
	strh	r3, [sp, #60]	@ movhi
	ldr	r3, [sp, #116]
	ldrh	ip, [r0, #0]
	ldr	r0, .L627+12
	strh	r3, [sp, #56]	@ movhi
	ldr	r3, [sp, #120]
	ldrh	r0, [r0, #0]
	strh	r3, [sp, #52]	@ movhi
	ldr	r3, [sp, #124]
	add	r1, ip, r1
	add	r2, r0, r2
	and	r3, r3, #255
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r1, r1, lsr #16
	mov	r2, r2, lsr #16
	cmp	r3, #1
	mov	r5, r5, asr #16
	strh	r1, [sp, #72]	@ movhi
	strh	r2, [sp, #68]	@ movhi
	bne	.L594
	ldrh	r3, [sp, #64]
	add	r1, sp, #56
	add	r3, ip, r3
	strh	r3, [sp, #64]	@ movhi
	ldrh	r3, [sp, #60]
	add	r3, r0, r3
	strh	r3, [sp, #60]	@ movhi
	ldrh	r3, [sp, #56]
	add	ip, ip, r3
	ldrh	r3, [sp, #52]
	strh	ip, [sp, #56]	@ movhi
	add	r0, r0, r3
	strh	r0, [sp, #52]	@ movhi
	add	r0, sp, #64
	bl	OrderCoord
	add	r0, sp, #60
	add	r1, sp, #52
	bl	OrderCoord
	ldrsh	r2, [sp, #72]
	ldrsh	r8, [sp, #64]
	add	ip, r2, r5
	cmp	ip, r8
	ldrh	r0, [sp, #72]
	bgt	.L623
	b	.L593
.L594:
	sub	r2, r2, #1
	sub	r1, r1, #1
	add	r6, r6, r2
	add	r7, r7, r1
	add	r0, sp, #72
	add	r1, sp, #56
	strh	r7, [sp, #56]	@ movhi
	strh	r6, [sp, #52]	@ movhi
	bl	OrderCoord
	add	r0, sp, #68
	add	r1, sp, #52
	bl	OrderCoord
	ldrh	r3, [sp, #72]
	strh	r3, [sp, #64]	@ movhi
	ldrh	r3, [sp, #68]
	strh	r3, [sp, #60]	@ movhi
	b	.L597
.L623:
	ldrh	r3, [sp, #56]
	str	r3, [sp, #0]
	ldrsh	r3, [sp, #56]
	cmp	r3, r2
	blt	.L593
	ldrh	r9, [sp, #60]
	ldrsh	sl, [sp, #68]
	str	r9, [sp, #4]
	ldrsh	r9, [sp, #60]
	mov	r1, r6, asl #16
	add	r1, sl, r1, asr #16
	cmp	r1, r9
	ldrh	r3, [sp, #68]
	ble	.L593
	ldrsh	r9, [sp, #52]
	ldrh	fp, [sp, #52]
	cmp	r9, sl
	blt	.L593
	ldr	sl, [sp, #0]
	cmp	r8, r2
	mov	r2, sl, asl #16
	strlth	r0, [sp, #64]	@ movhi
	cmp	ip, r2, asr #16
	ldr	ip, [sp, #4]
	suble	r0, r0, #1
	addle	r7, r7, r0
	mov	r2, ip, asl #16
	strleh	r7, [sp, #56]	@ movhi
	mov	fp, fp, asl #16
	cmp	r2, r3, asl #16
	strlth	r3, [sp, #60]	@ movhi
	cmp	r1, fp, asr #16
	suble	r3, r3, #1
	addle	r6, r6, r3
	strleh	r6, [sp, #52]	@ movhi
.L597:
	add	r0, sp, #64
	add	r1, sp, #60
	add	r2, sp, #56
	add	r3, sp, #52
	bl	CheckRect
	cmp	r0, #0
	beq	.L593
	ldrsh	r0, [sp, #64]
	ldrsh	r1, [sp, #60]
	ldrsh	r2, [sp, #56]
	ldrsh	r3, [sp, #52]
	bl	MarkDisplayBoxRepaint
	ldrh	r1, [sp, #72]
	add	r5, r5, #1
	mov	r8, r1, asl #16
	movs	r3, r8, asr #16
	add	r5, r5, r5, lsr #31
	rsbmi	r8, r3, #1
	mov	r5, r5, asl #15
	ldrsh	r3, [sp, #64]
	mov	r0, r5, lsr #16
	addmi	r8, r8, r8, lsr #31
	ldrsh	r7, [sp, #60]
	str	r0, [sp, #36]
	ldrsh	r0, [sp, #68]
	movmi	r8, r8, asr #1
	add	r3, r3, r3, lsr #31
	rsbmi	r8, r8, #0
	movpl	r8, r8, asr #17
	mov	r3, r3, asl #15
	mov	r5, r5, asr #16
	rsb	r0, r0, r7
	mov	r8, r8, asl #16
	mov	r7, r3, lsr #16
	mov	r3, r3, asr #16
	mla	r0, r5, r0, r3
	mov	r8, r8, lsr #16
	mov	r3, r8, asl #16
	mov	r3, r3, asr #16
	rsb	r0, r3, r0
	ands	r3, r3, #-2147483647
	submi	r3, r3, #1
	mvnmi	r3, r3, asl #31
	mvnmi	r3, r3, lsr #31
	mvn	r5, #1
	addmi	r3, r3, #1
	add	r8, r8, #1
	add	r4, r4, #4
	mla	r3, r5, r3, r8
	add	r0, r4, r0
	mov	r4, r7, asl #16
	mov	r4, r4, asr #16
	ands	r4, r4, #-2147483647
	mov	r3, r3, asl #16
	submi	r4, r4, #1
	mov	r3, r3, lsr #16
	mvnmi	r4, r4, asl #31
	str	r3, [sp, #40]
	add	r3, r7, #1
	mvnmi	r4, r4, lsr #31
	ldrsh	r2, [sp, #56]
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	addmi	r4, r4, #1
	mla	r4, r5, r4, r3
	add	r2, r2, r2, lsr #31
	mov	r2, r2, asl #15
	mov	r2, r2, lsr #16
	mov	r4, r4, asl #16
	str	r2, [sp, #0]
	mov	r4, r4, lsr #16
	str	r4, [sp, #20]
	ldr	r4, [sp, #0]
	ldrh	r6, [sp, #64]
	add	r5, r4, #1
	mov	r4, r4, asl #16
	mov	r4, r4, asr #16
	ands	r4, r4, #-2147483647
	submi	r4, r4, #1
	mvnmi	r4, r4, asl #31
	mvnmi	r4, r4, lsr #31
	addmi	r4, r4, #1
	sub	r5, r5, r4, asl #1
	mov	r4, r6, asl #16
	mov	r4, r4, asr #16
	ands	r4, r4, #-2147483647
	submi	r4, r4, #1
	mvnmi	r4, r4, asl #31
	ldrh	ip, [sp, #56]
	mvnmi	r4, r4, lsr #31
	and	r1, r1, #1
	ldr	r7, [sp, #20]
	addmi	r4, r4, #1
	ldrh	r6, [sp, #52]
	str	r1, [sp, #44]
	ldr	r1, .L627+16
	ldrh	r2, [sp, #60]
	and	r4, r4, #255
	mov	ip, ip, asl #16
	str	r4, [sp, #4]
	mov	ip, ip, asr #16
	mov	r4, r7, asl #16
	ands	ip, ip, #-2147483647
	ldr	r1, [r1, #0]
	mov	r4, r4, asr #16
	mov	r5, r5, asl #16
	str	r6, [sp, #48]
	mov	r6, #160	@ movhi
	submi	ip, ip, #1
	mov	r5, r5, lsr #16
	str	r4, [sp, #28]
	smlabb	r4, r2, r6, r4
	mvnmi	ip, ip, asl #31
	mov	r5, r5, asl #16
	mvnmi	ip, ip, lsr #31
	add	r4, r1, r4
	mov	r5, r5, asr #16
	addmi	ip, ip, #1
	str	r4, [sp, #32]
	smlabb	r4, r2, r6, r5
	and	ip, ip, #255
	str	r3, [sp, #24]
	mov	r3, #0
	str	ip, [sp, #8]
	add	r4, r1, r4
	mov	ip, r3
	b	.L608
.L622:
	ldr	r8, [sp, #44]
	ldr	r9, [sp, #20]
	cmp	r8, #0
	mov	r7, r9, asl #16
	mov	r7, r7, asr #16
	bne	.L609
	cmp	r7, r5
	ldr	r8, .L627+20
	bne	.L610
	ldr	r9, [sp, #4]
	ldr	sl, .L627+24
	ldr	fp, [sp, #8]
	ldrb	r8, [r8, r9]	@ zero_extendqisi2
	ldrb	sl, [sl, fp]	@ zero_extendqisi2
	smlabb	r7, r2, r6, r5
	and	r8, sl, r8
	add	r7, r1, r7
	ldrb	sl, [r0, #0]	@ zero_extendqisi2
	ldrb	r9, [r7, r3]	@ zero_extendqisi2
	and	sl, r8, sl
	bic	r8, r9, r8
	orr	r8, sl, r8
	strb	r8, [r7, r3]
	b	.L611
.L610:
	ldr	sl, [sp, #4]
	smlabb	r7, r2, r6, r7
	ldrb	r8, [r8, sl]	@ zero_extendqisi2
	add	r7, r1, r7
	ldrb	r9, [r7, r3]	@ zero_extendqisi2
	ldrb	sl, [r0, #0]	@ zero_extendqisi2
	mov	fp, r2, asl #16
	and	sl, r8, sl
	bic	r8, r9, r8
	ldr	r9, [sp, #0]
	orr	r8, sl, r8
	mov	sl, r9, asl #16
	mov	sl, sl, asr #16
	add	fp, ip, fp, asr #16
	str	sl, [sp, #12]
	mov	sl, #160
	mul	r9, sl, fp
	strb	r8, [r7, r3]
	str	r9, [sp, #16]
	mov	r8, r0
	ldr	r7, [sp, #24]
	mov	r9, r3
	b	.L612
.L615:
	mov	sl, r3, lsr #16
	tst	r3, #65536
	addeq	sl, sl, #1
	subne	sl, sl, #1
	ldrb	r3, [r8, #1]	@ zero_extendqisi2
	mov	sl, sl, asl #16
	ldr	r8, [sp, #16]
	mov	sl, sl, lsr #16
	add	r7, r7, #1
	mov	sl, sl, asl #16
	mov	r7, r7, asl #16
	add	sl, r8, sl, asr #16
	mov	r7, r7, lsr #16
	mov	r8, fp
	strb	r3, [r1, sl]
.L612:
	ldr	sl, [sp, #12]
	mov	r3, r7, asl #16
	cmp	sl, r3, asr #16
	add	fp, r8, #1
	bgt	.L615
	ldr	r8, [sp, #8]
	ldr	r7, .L627+24
	ldrb	sl, [r4, r9]	@ zero_extendqisi2
	ldrb	r7, [r7, r8]	@ zero_extendqisi2
	ldrb	r8, [fp, #0]	@ zero_extendqisi2
	mov	r3, r9
	and	r8, r7, r8
	bic	r7, sl, r7
	orr	r7, r8, r7
	strb	r7, [r4, r9]
	b	.L611
.L609:
	ldr	r9, [sp, #40]
	ldr	fp, [sp, #4]
	mov	r8, r9, asl #16
	cmp	r7, r8, asr #16
	ldrneb	r8, [r0, #0]	@ zero_extendqisi2
	ldrneb	r7, [r0, #-1]	@ zero_extendqisi2
	movne	r8, r8, lsr #4
	addne	r8, r8, r7, asl #4
	ldr	r7, .L627+20
	ldreqb	r8, [r0, #0]	@ zero_extendqisi2
	ldrb	r7, [r7, fp]	@ zero_extendqisi2
	ldr	fp, [sp, #32]
	moveq	r8, r8, lsr #4
	ldrb	r9, [fp, r3]	@ zero_extendqisi2
	andne	r8, r8, #255
	and	r8, r8, r7
	bic	r7, r9, r7
	orr	r7, r8, r7
	strb	r7, [fp, r3]
	ldr	r7, [sp, #28]
	mov	sl, r2, asl #16
	cmp	r5, r7
	add	sl, ip, sl, asr #16
	ble	.L611
	ldr	fp, [sp, #0]
	add	r7, r0, #1
	mov	r9, fp, asl #16
	mov	r9, r9, asr #16
	str	r9, [sp, #12]
	mov	r9, #160
	mul	fp, r9, sl
	ldr	r8, [sp, #24]
	str	fp, [sp, #16]
	b	.L618
.L621:
	mov	r9, sl, lsr #16
	tst	sl, #65536
	addeq	fp, r9, #1
	subne	fp, r9, #1
	mov	fp, fp, asl #16
	ldr	r9, [sp, #16]
	mov	fp, fp, lsr #16
	mov	fp, fp, asl #16
	add	fp, r9, fp, asr #16
	ldrb	r9, [r7, #-1]	@ zero_extendqisi2
	ldrb	sl, [r7], #1	@ zero_extendqisi2
	add	r8, r8, #1
	mov	sl, sl, lsr #4
	mov	r8, r8, asl #16
	add	sl, sl, r9, asl #4
	mov	r8, r8, lsr #16
	strb	sl, [r1, fp]
.L618:
	ldr	fp, [sp, #12]
	mov	sl, r8, asl #16
	cmp	fp, sl, asr #16
	bgt	.L621
	ldrb	sl, [r7, #-1]	@ zero_extendqisi2
	ldrb	r7, [r7, #0]	@ zero_extendqisi2
	ldr	r8, .L627+24
	ldr	r9, [sp, #8]
	mov	r7, r7, lsr #4
	ldrb	r8, [r8, r9]	@ zero_extendqisi2
	add	r7, r7, sl, asl #4
	ldrb	sl, [r4, r3]	@ zero_extendqisi2
	and	r7, r8, r7
	bic	r8, sl, r8
	orr	r8, r7, r8
	strb	r8, [r4, r3]
.L611:
	ldr	sl, [sp, #36]
	add	ip, ip, #1
	mov	r7, sl, asl #16
	add	r0, r0, r7, asr #16
	add	r3, r3, #160
.L608:
	ldr	fp, [sp, #48]
	add	r7, ip, r2
	mov	r7, r7, asl #16
	cmp	r7, fp, asl #16
	ble	.L622
.L593:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L628:
	.align	2
.L627:
	.word	.LANCHOR65
	.word	.LANCHOR66
	.word	.LANCHOR63
	.word	.LANCHOR64
	.word	guilib_display_ptr
	.word	.LANCHOR2
	.word	.LANCHOR1
.LFE167:
	.size	ShowBitmapArea.isra.3, .-ShowBitmapArea.isra.3
	.section	.text.TextPixelLength,"ax",%progbits
	.align	2
	.type	TextPixelLength, %function
TextPixelLength:
.LFB62:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI20:
	moveq	r4, r1
	sub	sp, sp, #20
.LCFI21:
	beq	.L630
.LBB130:
	ldr	r3, .L659
	ldrb	r4, [r3, #0]	@ zero_extendqisi2
	cmp	r4, #0
	beq	.L631
	ldr	r3, .L659+4
	ldr	r3, [r3, #0]
	ldrb	ip, [r3, #14]	@ zero_extendqisi2
	tst	ip, #1
	ldrneb	r4, [r3, #0]	@ zero_extendqisi2
	ldreqb	r4, [r3, #10]	@ zero_extendqisi2
	rsb	r4, r4, #0
	mov	r4, r4, asl #16
	mov	r4, r4, lsr #16
.L631:
.LBB131:
	ldr	r3, .L659+8
	sub	r5, r0, #2
.LBE131:
	cmp	r2, #0
.LBB132:
	and	r5, r5, #255
	ldr	ip, [r3, #0]
.LBE132:
	strneh	r4, [r2, #0]	@ movhi
.LBB133:
	mov	r3, #0
.LBE133:
	sub	r6, r1, #1
.LBB134:
	str	r5, [sp, #16]
	ldr	r7, .L659+4
	str	r1, [sp, #0]
	b	.L653
.L642:
.LBE134:
	add	r3, r3, #1
	str	r4, [sp, #8]
.LBB135:
	cmp	r0, #1
.LBE135:
	mov	r4, r3, asl #16
	mov	r3, r4, lsr #16
.LBB136:
	ldreqb	r5, [ip, #10]	@ zero_extendqisi2
	beq	.L636
	ldr	r1, [sp, #16]
	cmp	r1, #1
	movhi	r5, #0
	bhi	.L636
	cmp	r0, #2
	beq	.L637
	ldr	sl, .L659
	ldrb	r5, [sl, r8]	@ zero_extendqisi2
	ldrb	r4, [sl, r4, lsr #16]	@ zero_extendqisi2
	cmp	r5, #0
	beq	.L638
	cmp	r4, #0
	beq	.L639
.L637:
	ldr	r4, [r7, r3, asl #2]
	ldr	r5, [r7, r8, asl #2]
	ldrb	sl, [r4, #1]	@ zero_extendqisi2
	ldrb	r9, [r5, #6]	@ zero_extendqisi2
	ldrb	r1, [r4, #0]	@ zero_extendqisi2
	ldrb	fp, [r5, #5]	@ zero_extendqisi2
	rsb	sl, sl, r9
	str	sl, [sp, #12]
	rsb	fp, r1, fp
	ldrb	r9, [r4, #2]	@ zero_extendqisi2
	ldrb	sl, [r5, #7]	@ zero_extendqisi2
	ldr	r1, [sp, #12]
	rsb	sl, r9, sl
	cmp	r1, fp
	movlt	r1, fp
	ldrb	r9, [r4, #3]	@ zero_extendqisi2
	ldrb	fp, [r5, #8]	@ zero_extendqisi2
	cmp	r1, sl
	movlt	r1, sl
	ldrb	r5, [r5, #9]	@ zero_extendqisi2
	ldrb	sl, [r4, #4]	@ zero_extendqisi2
	ldrb	r4, [ip, #21]	@ zero_extendqisi2
	rsb	fp, r9, fp
	add	r4, r4, #1
	cmp	fp, r1
	movlt	fp, r1
	rsb	r5, sl, r5
	cmp	fp, r5
	addge	r5, r4, fp
	addlt	r5, r4, r5
	b	.L655
.L639:
	ldr	r4, [r7, r8, asl #2]
	ldrb	r5, [r4, #10]	@ zero_extendqisi2
	ldrb	r4, [r4, #11]	@ zero_extendqisi2
	add	r5, r5, r4
	ldrb	r4, [ip, #21]	@ zero_extendqisi2
	add	r5, r5, r4
	b	.L636
.L638:
	cmp	r4, #0
	ldrb	r5, [ip, #21]	@ zero_extendqisi2
	ldrb	r4, [ip, #20]	@ zero_extendqisi2
	add	r5, r5, r4
	beq	.L636
	ldr	r4, [r7, r3, asl #2]
	ldrb	sl, [r4, #10]	@ zero_extendqisi2
	rsb	r5, sl, r5
.L655:
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
.L636:
.LBE136:
	ldr	sl, [sp, #8]
	cmp	r2, #0
	add	r4, sl, r5
	mov	r4, r4, asl #16
	mov	r4, r4, lsr #16
	addne	r8, r2, r8, asl #1
	strneh	r4, [r8, #2]	@ movhi
.L653:
	cmp	r3, r6
	mov	r8, r3
	blt	.L642
	ldr	r3, .L659
	ldr	r1, [sp, #0]
	ldrb	r3, [r3, r6]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L643
	ldr	r3, .L659+4
	ldr	r3, [r3, r6, asl #2]
	ldrb	r0, [r3, #14]	@ zero_extendqisi2
	tst	r0, #1
	ldreqb	r0, [r3, #10]	@ zero_extendqisi2
	ldreqb	r3, [r3, #11]	@ zero_extendqisi2
	beq	.L658
	b	.L657
.L643:
	cmp	r0, #3
	bne	.L646
	ldr	r3, .L659+4
	ldr	r3, [r3, r6, asl #2]
	ldrb	r0, [r3, #14]	@ zero_extendqisi2
	tst	r0, #1
	beq	.L647
.L657:
	add	r4, r4, #1
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	b	.L656
.L647:
	ldr	r3, .L659+8
	ldr	r3, [r3, #0]
	ldrb	r0, [r3, #21]	@ zero_extendqisi2
	ldrb	r3, [r3, #20]	@ zero_extendqisi2
.L658:
	add	r3, r0, r3
	b	.L656
.L646:
	ldr	r3, .L659+8
	ldr	r3, [r3, #0]
	ldrb	r3, [r3, #10]	@ zero_extendqisi2
.L656:
	add	r4, r4, r3
	cmp	r2, #0
	mov	r4, r4, asl #16
	mov	r4, r4, lsr #16
	movne	r1, r1, asl #1
	strneh	r4, [r2, r1]	@ movhi
.L630:
	mov	r4, r4, asl #16
.LBE130:
	mov	r0, r4, asr #16
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L660:
	.align	2
.L659:
	.word	.LANCHOR6
	.word	.LANCHOR7
	.word	.LANCHOR8
.LFE62:
	.size	TextPixelLength, .-TextPixelLength
	.section	.text.SetBackColorBox,"ax",%progbits
	.align	2
	.type	SetBackColorBox, %function
SetBackColorBox:
.LFB68:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L666
	stmfd	sp!, {r4, r5, lr}
.LCFI22:
	ldr	r4, .L666+4
	strh	r1, [ip, #0]	@ movhi
	ldr	ip, .L666+8
	ldr	r5, .L666+12
	strh	r3, [ip, #0]	@ movhi
	ldr	ip, .L666+16
	strh	r0, [r4, #0]	@ movhi
	ldrb	ip, [ip, #69]	@ zero_extendqisi2
	strh	r2, [r5, #0]	@ movhi
	tst	ip, #1
	subne	r0, r0, #1
	strneh	r0, [r4, #0]	@ movhi
	tst	ip, #2
	addne	r2, r2, #1
	strneh	r2, [r5, #0]	@ movhi
	tst	ip, #4
	ldrne	r2, .L666
	subne	r1, r1, #1
	strneh	r1, [r2, #0]	@ movhi
	tst	ip, #8
.LBB139:
	ldrne	r2, .L666+8
	addne	r3, r3, #1
	strneh	r3, [r2, #0]	@ movhi
	ldmfd	sp!, {r4, r5, pc}
.L667:
	.align	2
.L666:
	.word	.LANCHOR18
	.word	.LANCHOR16
	.word	.LANCHOR19
	.word	.LANCHOR17
	.word	.LANCHOR10
.LBE139:
.LFE68:
	.size	SetBackColorBox, .-SetBackColorBox
	.section	.text.GetCharCode.part.6,"ax",%progbits
	.align	2
	.type	GetCharCode.part.6, %function
GetCharCode.part.6:
.LFB170:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r2, #0
	mov	r1, r1, asl #16
	movne	r2, #0
	mov	r3, r0
	stmfd	sp!, {r4, lr}
.LCFI23:
	mov	ip, r1, lsr #16
	movne	r0, r2
	bne	.L670
	b	.L675
.L673:
	cmp	r4, #0
	ldrb	r1, [r3, r2]	@ zero_extendqisi2
	beq	.L671
	cmp	r1, #10
	beq	.L672
	cmp	r0, #32
	cmpne	r0, #45
	movne	r0, #0
	moveq	r0, #1
	cmp	r1, #32
	movne	r0, #0
	andeq	r0, r0, #1
	cmp	r0, #0
	beq	.L671
.L672:
	add	ip, ip, #1
	mov	ip, ip, asl #16
	mov	ip, ip, lsr #16
.L671:
	add	r2, r2, #1
	mov	r0, r1
.L670:
	mov	r4, r2, asl #16
	mov	r4, r4, lsr #16
	cmp	ip, r4
	bge	.L673
	ldmfd	sp!, {r4, pc}
.L675:
	ldrb	r0, [r3, r1, lsr #16]	@ zero_extendqisi2
	ldmfd	sp!, {r4, pc}
.LFE170:
	.size	GetCharCode.part.6, .-GetCharCode.part.6
	.section	.text.ScrollBox_SetTopLine,"ax",%progbits
	.align	2
	.type	ScrollBox_SetTopLine, %function
ScrollBox_SetTopLine:
.LFB129:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L682
	mov	r2, #268
	mla	r3, r2, r0, r3
	stmfd	sp!, {r4, lr}
.LCFI24:
	ldrh	r0, [r3, #164]
	ldrb	r2, [r3, #154]	@ zero_extendqisi2
	ldrb	r1, [r3, #153]	@ zero_extendqisi2
	cmp	r2, #0
	rsb	r1, r1, r0
	strneh	r1, [r3, #12]	@ movhi
	ldmnefd	sp!, {r4, pc}
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	mov	r0, r1, asl #16
	movs	r4, r0, asr #16
	strh	r1, [r3, #12]	@ movhi
	bmi	.L681
.L678:
	ldrh	ip, [r3, #20]
	ldrh	r0, [r3, #128]
	subs	r0, ip, r0
	bpl	.L680
	cmp	r1, #0
	ldmeqfd	sp!, {r4, pc}
.L681:
	strh	r2, [r3, #12]	@ movhi
	ldmfd	sp!, {r4, pc}
.L680:
	cmp	r4, r0
.LBB142:
	strgth	r0, [r3, #12]	@ movhi
	ldmfd	sp!, {r4, pc}
.L683:
	.align	2
.L682:
	.word	.LANCHOR41
.LBE142:
.LFE129:
	.size	ScrollBox_SetTopLine, .-ScrollBox_SetTopLine
	.section	.text.TextBox_Scroll_Get_Pos.part.8,"ax",%progbits
	.align	2
	.type	TextBox_Scroll_Get_Pos.part.8, %function
TextBox_Scroll_Get_Pos.part.8:
.LFB172:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI25:
	mov	r4, r0
	bl	TextBox_Scroll_CalcEndPos
	ldr	r3, .L690
	mov	r2, #100
	mla	r4, r2, r4, r3
	ldrh	r3, [r4, #90]
	cmp	r3, #0
	moveq	r0, #2
	ldmeqfd	sp!, {r4, pc}
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r3, r0
	moveq	r0, #3
	ldmeqfd	sp!, {r4, pc}
	cmp	r3, #0
	blt	.L688
	cmp	r3, r0
	movle	r0, #1
	movgt	r0, #5
	ldmfd	sp!, {r4, pc}
.L688:
	mov	r0, #4
	ldmfd	sp!, {r4, pc}
.L691:
	.align	2
.L690:
	.word	.LANCHOR56
.LFE172:
	.size	TextBox_Scroll_Get_Pos.part.8, .-TextBox_Scroll_Get_Pos.part.8
	.section	.text.ScrollLineAdjustY.constprop.22,"ax",%progbits
	.align	2
	.type	ScrollLineAdjustY.constprop.22, %function
ScrollLineAdjustY.constprop.22:
.LFB186:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L693
	ldr	r2, .L693+4
	ldrh	r3, [r3, #0]
	ldrh	r1, [r2, #12]
	mul	r3, r0, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r1, r3, r1
	strh	r1, [r2, #12]	@ movhi
	ldrh	r1, [r2, #16]
	add	r1, r3, r1
	strh	r1, [r2, #16]	@ movhi
	ldrh	r1, [r2, #24]
	add	r3, r3, r1
	strh	r3, [r2, #24]	@ movhi
	bx	lr
.L694:
	.align	2
.L693:
	.word	.LANCHOR67
	.word	.LANCHOR10
.LFE186:
	.size	ScrollLineAdjustY.constprop.22, .-ScrollLineAdjustY.constprop.22
	.section	.text.GuiLib_InvertBox,"ax",%progbits
	.align	2
	.global	GuiLib_InvertBox
	.type	GuiLib_InvertBox, %function
GuiLib_InvertBox:
.LFB12:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L710
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI26:
	ldrh	ip, [ip, #0]
	sub	sp, sp, #32
.LCFI27:
	add	r0, r0, ip
	strh	r0, [sp, #28]	@ movhi
	ldr	r0, .L710+4
	add	r2, r2, ip
	ldrh	r0, [r0, #0]
	strh	r2, [sp, #20]	@ movhi
	add	r1, r1, r0
	add	r3, r3, r0
	strh	r1, [sp, #24]	@ movhi
	add	r0, sp, #28
	add	r1, sp, #20
	strh	r3, [sp, #16]	@ movhi
	bl	OrderCoord
	add	r1, sp, #16
	add	r0, sp, #24
	bl	OrderCoord
	add	r0, sp, #28
	add	r1, sp, #24
	add	r2, sp, #20
	add	r3, sp, #16
	bl	CheckRect
	cmp	r0, #0
	beq	.L695
	ldrsh	r0, [sp, #28]
	ldrsh	r1, [sp, #24]
	ldrsh	r2, [sp, #20]
	ldrsh	r3, [sp, #16]
	bl	MarkDisplayBoxRepaint
	ldrsh	r8, [sp, #28]
	ldrsh	r5, [sp, #20]
	add	r8, r8, r8, lsr #31
	mov	r8, r8, asl #15
	mov	r8, r8, lsr #16
	add	r1, r8, #1
	mov	r8, r8, asl #16
	mov	r8, r8, asr #16
	add	r5, r5, r5, lsr #31
	ands	r8, r8, #-2147483647
	mov	r5, r5, asl #15
	submi	r8, r8, #1
	mov	r5, r5, lsr #16
	mvnmi	r8, r8, asl #31
	mov	r3, r5, asl #16
	mvnmi	r8, r8, lsr #31
	ldrh	sl, [sp, #28]
	mov	r3, r3, asr #16
	addmi	r8, r8, #1
	ands	r3, r3, #-2147483647
	submi	r3, r3, #1
	mvnmi	r3, r3, asl #31
	mov	sl, sl, asl #16
	mvnmi	r3, r3, lsr #31
	mov	r1, r1, asl #16
	ldrh	r7, [sp, #20]
	mov	sl, sl, asr #16
	mov	r1, r1, lsr #16
	add	r2, r5, #1
	addmi	r3, r3, #1
	mvn	r6, #1
	ands	sl, sl, #-2147483647
	submi	sl, sl, #1
	mla	r8, r6, r8, r1
	mla	r6, r3, r6, r2
	mvnmi	sl, sl, asl #31
	ldrh	r4, [sp, #16]
	mov	r7, r7, asl #16
	mvnmi	sl, sl, lsr #31
	mov	r7, r7, asr #16
	ldr	r3, .L710+8
	addmi	sl, sl, #1
	mov	r8, r8, asl #16
	ands	r7, r7, #-2147483647
	mov	r6, r6, asl #16
	submi	r7, r7, #1
	mov	r8, r8, lsr #16
	mov	r6, r6, lsr #16
	str	r1, [sp, #4]
	mov	r4, r4, asl #16
	ldrh	r1, [sp, #24]
	mvnmi	r7, r7, asl #31
	mov	r4, r4, asr #16
	mov	r0, r8, asl #16
	mov	fp, r6, asl #16
	ldr	r2, [r3, #0]
	mvnmi	r7, r7, lsr #31
	mov	r0, r0, asr #16
	mov	fp, fp, asr #16
	str	r4, [sp, #8]
	mov	r4, #160	@ movhi
	addmi	r7, r7, #1
	smlabb	r0, r1, r4, r0
	mov	r5, r5, asl #16
	smlabb	fp, r1, r4, fp
	mov	sl, sl, asl #16
	mov	r7, r7, asl #16
	mov	r3, #0
	mov	r5, r5, asr #16
	mov	sl, sl, asr #16
	mov	r7, r7, asr #16
	mov	ip, r3
	add	r0, r2, r0
	str	r5, [sp, #12]
	add	fp, r2, fp
	b	.L701
.L708:
	cmp	r6, r8
	ldr	r4, .L710+12
	bne	.L702
	ldr	r5, .L710+16
	ldrb	r4, [r4, sl]	@ zero_extendqisi2
	ldrb	r5, [r5, r7]	@ zero_extendqisi2
	and	r4, r5, r4
	ldrb	r5, [r0, r3]	@ zero_extendqisi2
	eor	r4, r5, r4
	strb	r4, [r0, r3]
	b	.L703
.L702:
	mov	r5, r1, asl #16
	add	r5, ip, r5, asr #16
	str	r5, [sp, #0]
	ldrb	r4, [r4, sl]	@ zero_extendqisi2
	ldrb	r5, [r0, r3]	@ zero_extendqisi2
	ldr	r9, [sp, #4]
	eor	r5, r5, r4
	strb	r5, [r0, r3]
	ldr	r5, [sp, #0]
	mov	r4, #160
	mul	r5, r4, r5
	str	r5, [sp, #0]
	b	.L704
.L707:
	mov	r5, r4, lsr #16
	tst	r4, #65536
	addeq	r5, r5, #1
	subne	r5, r5, #1
	ldr	r4, [sp, #0]
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
	mov	r5, r5, asl #16
	add	r5, r4, r5, asr #16
	ldrb	r4, [r2, r5]	@ zero_extendqisi2
	add	r9, r9, #1
	mov	r9, r9, asl #16
	mvn	r4, r4
	mov	r9, r9, lsr #16
	strb	r4, [r2, r5]
.L704:
	ldr	r5, [sp, #12]
	mov	r4, r9, asl #16
	cmp	r5, r4, asr #16
	bgt	.L707
	ldr	r4, .L710+16
	ldrb	r5, [fp, r3]	@ zero_extendqisi2
	ldrb	r4, [r4, r7]	@ zero_extendqisi2
	eor	r4, r5, r4
	strb	r4, [fp, r3]
.L703:
	add	ip, ip, #1
	add	r3, r3, #160
.L701:
	ldr	r5, [sp, #8]
	add	r4, ip, r1
	mov	r4, r4, asl #16
	cmp	r5, r4, asr #16
	bge	.L708
.L695:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L711:
	.align	2
.L710:
	.word	.LANCHOR63
	.word	.LANCHOR64
	.word	guilib_display_ptr
	.word	.LANCHOR2
	.word	.LANCHOR1
.LFE12:
	.size	GuiLib_InvertBox, .-GuiLib_InvertBox
	.section	.text.BlinkBox,"ax",%progbits
	.align	2
	.type	BlinkBox, %function
BlinkBox:
.LFB112:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L714
	str	lr, [sp, #-4]!
.LCFI28:
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L713
	ldr	r3, .L714+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L713
	ldr	r3, .L714+8
	ldrsh	r0, [r3, #0]
	ldr	r3, .L714+12
	ldrsh	r1, [r3, #0]
	ldr	r3, .L714+16
	ldrsh	r2, [r3, #0]
	ldr	r3, .L714+20
	ldrsh	r3, [r3, #0]
	bl	GuiLib_InvertBox
.L713:
	ldr	r3, .L714+24
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	rsbs	r2, r2, #1
	movcc	r2, #0
	strb	r2, [r3, #0]
	ldr	pc, [sp], #4
.L715:
	.align	2
.L714:
	.word	.LANCHOR68
	.word	.LANCHOR69
	.word	.LANCHOR70
	.word	.LANCHOR71
	.word	.LANCHOR72
	.word	.LANCHOR73
	.word	.LANCHOR74
.LFE112:
	.size	BlinkBox, .-BlinkBox
	.section	.text.InvertBox,"ax",%progbits
	.align	2
	.type	InvertBox, %function
InvertBox:
.LFB118:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L718
	str	lr, [sp, #-4]!
.LCFI29:
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L717
	ldr	r3, .L718+4
	ldrsh	r0, [r3, #0]
	ldr	r3, .L718+8
	ldrsh	r1, [r3, #0]
	ldr	r3, .L718+12
	ldrsh	r2, [r3, #0]
	ldr	r3, .L718+16
	ldrsh	r3, [r3, #0]
	bl	GuiLib_InvertBox
.L717:
	ldr	r3, .L718+20
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	rsbs	r2, r2, #1
	movcc	r2, #0
	strb	r2, [r3, #0]
	ldr	pc, [sp], #4
.L719:
	.align	2
.L718:
	.word	.LANCHOR69
	.word	.LANCHOR75
	.word	.LANCHOR76
	.word	.LANCHOR77
	.word	.LANCHOR78
	.word	.LANCHOR79
.LFE118:
	.size	InvertBox, .-InvertBox
	.section	.text.GuiLib_DegToRad,"ax",%progbits
	.align	2
	.global	GuiLib_DegToRad
	.type	GuiLib_DegToRad, %function
GuiLib_DegToRad:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI30:
	ldr	r1, .L721
	mov	r0, r0, asl #12
	bl	__divsi3
	ldr	pc, [sp], #4
.L722:
	.align	2
.L721:
	.word	573
.LFE13:
	.size	GuiLib_DegToRad, .-GuiLib_DegToRad
	.section	.text.GuiLib_RadToDeg,"ax",%progbits
	.align	2
	.global	GuiLib_RadToDeg
	.type	GuiLib_RadToDeg, %function
GuiLib_RadToDeg:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L724
	mul	r3, r0, r3
	add	r2, r3, #4080
	cmp	r3, #0
	addlt	r3, r2, #15
	mov	r0, r3, asr #12
	bx	lr
.L725:
	.align	2
.L724:
	.word	573
.LFE14:
	.size	GuiLib_RadToDeg, .-GuiLib_RadToDeg
	.section	.text.GuiLib_SinRad,"ax",%progbits
	.align	2
	.global	GuiLib_SinRad
	.type	GuiLib_SinRad, %function
GuiLib_SinRad:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI31:
	ldr	r1, .L731
	bl	__modsi3
	ldr	r3, .L731+4
	mov	r1, #6
	cmp	r0, r3
	ldr	r3, .L731+8
	rsbgt	r4, r0, #25600
	addgt	r4, r4, #136
	movle	r4, r0
	cmp	r4, r3
	rsbgt	r4, r4, #12864
	addgt	r4, r4, #4
	mov	r5, r0
	mul	r0, r4, r4
	mov	r0, r0, asr #12
	mul	r0, r4, r0
	bl	__divsi3
	mov	r1, #20
	mov	r6, r0, asr #12
	mul	r0, r4, r6
	rsb	r6, r6, r4
	mov	r0, r0, asr #12
	mul	r0, r4, r0
	bl	__divsi3
	mov	r1, #42
	mov	r0, r0, asr #12
	add	r6, r6, r0
	mul	r0, r4, r0
	bl	__divsi3
	ldr	r3, .L731+4
	cmp	r5, r3
	mul	r0, r4, r0
	sub	r0, r6, r0, asr #24
	rsbgt	r0, r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L732:
	.align	2
.L731:
	.word	25736
	.word	12868
	.word	6434
.LFE15:
	.size	GuiLib_SinRad, .-GuiLib_SinRad
	.section	.text.GuiLib_SinDeg,"ax",%progbits
	.align	2
	.global	GuiLib_SinDeg
	.type	GuiLib_SinDeg, %function
GuiLib_SinDeg:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI32:
.LBB143:
	ldr	r1, .L734
	mov	r0, r0, asl #12
	bl	__divsi3
.LBE143:
	ldr	lr, [sp], #4
	b	GuiLib_SinRad
.L735:
	.align	2
.L734:
	.word	573
.LFE16:
	.size	GuiLib_SinDeg, .-GuiLib_SinDeg
	.section	.text.GuiLib_CosRad,"ax",%progbits
	.align	2
	.global	GuiLib_CosRad
	.type	GuiLib_CosRad, %function
GuiLib_CosRad:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI33:
	ldr	r1, .L741
	bl	__modsi3
	ldr	r3, .L741+4
	mov	r1, #12
	cmp	r0, r3
	ldr	r3, .L741+8
	mov	r5, r0
	rsbgt	r5, r0, #25600
	addgt	r5, r5, #136
	cmp	r5, r3
	rsbgt	r4, r5, #12864
	addgt	r4, r4, #4
	movle	r4, r5
	mul	r7, r4, r4
	mov	r7, r7, asr #13
	mul	r0, r4, r7
	rsb	r7, r7, #4096
	mov	r0, r0, asr #12
	mul	r0, r4, r0
	bl	__divsi3
	mov	r1, #30
	mov	r6, r0, asr #12
	mul	r0, r4, r6
	bl	__divsi3
	add	r6, r7, r6
	mov	r1, #7
	mul	r3, r4, r0
	mov	r3, r3, asr #24
	rsb	r6, r3, r6
	mul	r3, r4, r3
	add	r0, r3, #7
	cmp	r3, #0
	movlt	r3, r0
	mov	r0, r3, asr #3
	mul	r0, r4, r0
	bl	__divsi3
	ldr	r3, .L741+8
	cmp	r5, r3
	add	r0, r6, r0, asr #24
	rsbgt	r0, r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L742:
	.align	2
.L741:
	.word	25736
	.word	12868
	.word	6434
.LFE17:
	.size	GuiLib_CosRad, .-GuiLib_CosRad
	.section	.text.GuiLib_CosDeg,"ax",%progbits
	.align	2
	.global	GuiLib_CosDeg
	.type	GuiLib_CosDeg, %function
GuiLib_CosDeg:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI34:
.LBB144:
	ldr	r1, .L744
	mov	r0, r0, asl #12
	bl	__divsi3
.LBE144:
	ldr	lr, [sp], #4
	b	GuiLib_CosRad
.L745:
	.align	2
.L744:
	.word	573
.LFE18:
	.size	GuiLib_CosDeg, .-GuiLib_CosDeg
	.section	.text.GuiLib_Sqrt,"ax",%progbits
	.align	2
	.global	GuiLib_Sqrt
	.type	GuiLib_Sqrt, %function
GuiLib_Sqrt:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI35:
	subs	r6, r0, #0
	moveq	r4, r6
	beq	.L747
.LBB147:
	mov	r4, r6, lsr #1
	add	r4, r4, #1
	mov	r1, r4
	bl	__udivsi3
	add	r0, r0, r4
	b	.L751
.L749:
	mov	r0, r6
	mov	r1, r5
	bl	__udivsi3
	mov	r4, r5
	add	r0, r0, r5
.L751:
	mov	r5, r0, lsr #1
	cmp	r5, r4
	bcc	.L749
.L747:
.LBE147:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.LFE19:
	.size	GuiLib_Sqrt, .-GuiLib_Sqrt
	.section	.text.GuiLib_GetRedRgbColor,"ax",%progbits
	.align	2
	.global	GuiLib_GetRedRgbColor
	.type	GuiLib_GetRedRgbColor, %function
GuiLib_GetRedRgbColor:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r0, r0, #255
	bx	lr
.LFE20:
	.size	GuiLib_GetRedRgbColor, .-GuiLib_GetRedRgbColor
	.section	.text.GuiLib_SetRedRgbColor,"ax",%progbits
	.align	2
	.global	GuiLib_SetRedRgbColor
	.type	GuiLib_SetRedRgbColor, %function
GuiLib_SetRedRgbColor:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bic	r0, r0, #-16777216
	and	r1, r1, #255
	bic	r0, r0, #255
	orr	r0, r1, r0
	bx	lr
.LFE21:
	.size	GuiLib_SetRedRgbColor, .-GuiLib_SetRedRgbColor
	.section	.text.GuiLib_GetGreenRgbColor,"ax",%progbits
	.align	2
	.global	GuiLib_GetGreenRgbColor
	.type	GuiLib_GetGreenRgbColor, %function
GuiLib_GetGreenRgbColor:
.LFB22:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r0, r0, #65280
	mov	r0, r0, lsr #8
	bx	lr
.LFE22:
	.size	GuiLib_GetGreenRgbColor, .-GuiLib_GetGreenRgbColor
	.section	.text.GuiLib_SetGreenRgbColor,"ax",%progbits
	.align	2
	.global	GuiLib_SetGreenRgbColor
	.type	GuiLib_SetGreenRgbColor, %function
GuiLib_SetGreenRgbColor:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, r1, asl #8
	bic	r0, r0, #-16777216
	and	r1, r1, #65280
	bic	r0, r0, #65280
	orr	r0, r1, r0
	bx	lr
.LFE23:
	.size	GuiLib_SetGreenRgbColor, .-GuiLib_SetGreenRgbColor
	.section	.text.GuiLib_GetBlueRgbColor,"ax",%progbits
	.align	2
	.global	GuiLib_GetBlueRgbColor
	.type	GuiLib_GetBlueRgbColor, %function
GuiLib_GetBlueRgbColor:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r0, r0, #16711680
	mov	r0, r0, lsr #16
	bx	lr
.LFE24:
	.size	GuiLib_GetBlueRgbColor, .-GuiLib_GetBlueRgbColor
	.section	.text.GuiLib_SetBlueRgbColor,"ax",%progbits
	.align	2
	.global	GuiLib_SetBlueRgbColor
	.type	GuiLib_SetBlueRgbColor, %function
GuiLib_SetBlueRgbColor:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, r1, asl #16
	and	r1, r1, #16711680
	bic	r0, r0, #16711680
	orr	r0, r1, r0
	bx	lr
.LFE25:
	.size	GuiLib_SetBlueRgbColor, .-GuiLib_SetBlueRgbColor
	.section	.text.GuiLib_RgbColorToGrayScale,"ax",%progbits
	.align	2
	.global	GuiLib_RgbColorToGrayScale
	.type	GuiLib_RgbColorToGrayScale, %function
GuiLib_RgbColorToGrayScale:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	and	r3, r0, #16711680
	and	r2, r0, #65280
	mov	r3, r3, lsr #16
	add	r3, r3, r2, lsr #8
	and	r0, r0, #255
	str	lr, [sp, #-4]!
.LCFI36:
	add	r0, r3, r0
	mov	r1, #3
	bl	__udivsi3
	and	r0, r0, #255
	ldr	pc, [sp], #4
.LFE28:
	.size	GuiLib_RgbColorToGrayScale, .-GuiLib_RgbColorToGrayScale
	.section	.text.GuiLib_RgbToPixelColor,"ax",%progbits
	.align	2
	.global	GuiLib_RgbToPixelColor
	.type	GuiLib_RgbToPixelColor, %function
GuiLib_RgbToPixelColor:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI37:
	bl	GuiLib_RgbColorToGrayScale
	mov	r1, #17
	bl	__divsi3
	and	r0, r0, #255
	ldr	pc, [sp], #4
.LFE26:
	.size	GuiLib_RgbToPixelColor, .-GuiLib_RgbToPixelColor
	.section	.text.GuiLib_GrayScaleToRgbColor,"ax",%progbits
	.align	2
	.global	GuiLib_GrayScaleToRgbColor
	.type	GuiLib_GrayScaleToRgbColor, %function
GuiLib_GrayScaleToRgbColor:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r0, r0, #255
	mov	r3, r0, asl #16
	orr	r3, r3, r0, asl #8
	orr	r0, r3, r0
	bx	lr
.LFE29:
	.size	GuiLib_GrayScaleToRgbColor, .-GuiLib_GrayScaleToRgbColor
	.section	.text.GuiLib_PixelToRgbColor,"ax",%progbits
	.align	2
	.global	GuiLib_PixelToRgbColor
	.type	GuiLib_PixelToRgbColor, %function
GuiLib_PixelToRgbColor:
.LFB27:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r0, r0, r0, asl #4
	and	r0, r0, #255
	b	GuiLib_GrayScaleToRgbColor
.LFE27:
	.size	GuiLib_PixelToRgbColor, .-GuiLib_PixelToRgbColor
	.section	.text.GuiLib_PixelColorToGrayScale,"ax",%progbits
	.align	2
	.global	GuiLib_PixelColorToGrayScale
	.type	GuiLib_PixelColorToGrayScale, %function
GuiLib_PixelColorToGrayScale:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI38:
	and	r0, r0, #255
	bl	GuiLib_PixelToRgbColor
	ldr	lr, [sp], #4
	b	GuiLib_RgbColorToGrayScale
.LFE30:
	.size	GuiLib_PixelColorToGrayScale, .-GuiLib_PixelColorToGrayScale
	.section	.text.GuiLib_GrayScaleToPixelColor,"ax",%progbits
	.align	2
	.global	GuiLib_GrayScaleToPixelColor
	.type	GuiLib_GrayScaleToPixelColor, %function
GuiLib_GrayScaleToPixelColor:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI39:
	and	r0, r0, #255
	bl	GuiLib_GrayScaleToRgbColor
	ldr	lr, [sp], #4
	b	GuiLib_RgbToPixelColor
.LFE31:
	.size	GuiLib_GrayScaleToPixelColor, .-GuiLib_GrayScaleToPixelColor
	.section	.text.GuiLib_BrightenRgbColor,"ax",%progbits
	.align	2
	.global	GuiLib_BrightenRgbColor
	.type	GuiLib_BrightenRgbColor, %function
GuiLib_BrightenRgbColor:
.LFB32:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r1, r1, asl #16
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI40:
	movs	r5, r1, lsr #16
	mov	r4, r0
	beq	.L765
.LBB153:
	and	r0, r0, #255
	rsb	r5, r5, #1000
	rsb	r0, r0, #255
	mov	r1, #1000
	mul	r0, r5, r0
	bl	__udivsi3
	mov	r1, #1000
	rsb	r6, r0, #255
.LBB154:
	and	r0, r4, #65280
.LBE154:
	mov	r0, r0, lsr #8
	rsb	r0, r0, #255
	mul	r0, r5, r0
	bl	__udivsi3
	mov	r1, #1000
	rsb	r0, r0, #255
	mov	r7, r0, asl #8
.LBB155:
	and	r0, r4, #16711680
.LBE155:
	mov	r0, r0, lsr #16
	rsb	r0, r0, #255
	mul	r0, r5, r0
	bl	__udivsi3
	rsb	r4, r0, #255
	orr	r4, r7, r4, asl #16
	orr	r4, r4, r6
.L765:
.LBE153:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE32:
	.size	GuiLib_BrightenRgbColor, .-GuiLib_BrightenRgbColor
	.section	.text.GuiLib_BrightenPixelColor,"ax",%progbits
	.align	2
	.global	GuiLib_BrightenPixelColor
	.type	GuiLib_BrightenPixelColor, %function
GuiLib_BrightenPixelColor:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r1, r1, asl #16
	stmfd	sp!, {r4, lr}
.LCFI41:
	and	r0, r0, #255
	mov	r4, r1, lsr #16
	bl	GuiLib_PixelToRgbColor
	mov	r1, r4
	bl	GuiLib_BrightenRgbColor
	ldmfd	sp!, {r4, lr}
	b	GuiLib_RgbToPixelColor
.LFE33:
	.size	GuiLib_BrightenPixelColor, .-GuiLib_BrightenPixelColor
	.section	.text.GuiLib_DarkenRgbColor,"ax",%progbits
	.align	2
	.global	GuiLib_DarkenRgbColor
	.type	GuiLib_DarkenRgbColor, %function
GuiLib_DarkenRgbColor:
.LFB34:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r1, r1, asl #16
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI42:
	movs	r5, r1, lsr #16
	mov	r4, r0
	beq	.L768
.LBB161:
	rsb	r5, r5, #1000
	and	r0, r0, #255
	mov	r1, #1000
	mul	r0, r5, r0
	bl	__udivsi3
	mov	r1, #1000
	mov	r6, r0
.LBB162:
	and	r0, r4, #65280
.LBE162:
	mov	r0, r0, lsr #8
	mul	r0, r5, r0
	bl	__udivsi3
.LBB163:
	and	r4, r4, #16711680
.LBE163:
	mov	r1, #1000
	mov	r7, r0, asl #8
	mov	r0, r4, lsr #16
	mul	r0, r5, r0
	bl	__udivsi3
	orr	r4, r7, r0, asl #16
	orr	r4, r4, r6
.L768:
.LBE161:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE34:
	.size	GuiLib_DarkenRgbColor, .-GuiLib_DarkenRgbColor
	.section	.text.GuiLib_DarkenPixelColor,"ax",%progbits
	.align	2
	.global	GuiLib_DarkenPixelColor
	.type	GuiLib_DarkenPixelColor, %function
GuiLib_DarkenPixelColor:
.LFB35:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r1, r1, asl #16
	stmfd	sp!, {r4, lr}
.LCFI43:
	and	r0, r0, #255
	mov	r4, r1, lsr #16
	bl	GuiLib_PixelToRgbColor
	mov	r1, r4
	bl	GuiLib_DarkenRgbColor
	ldmfd	sp!, {r4, lr}
	b	GuiLib_RgbToPixelColor
.LFE35:
	.size	GuiLib_DarkenPixelColor, .-GuiLib_DarkenPixelColor
	.section	.text.GuiLib_AccentuateRgbColor,"ax",%progbits
	.align	2
	.global	GuiLib_AccentuateRgbColor
	.type	GuiLib_AccentuateRgbColor, %function
GuiLib_AccentuateRgbColor:
.LFB36:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r1, r1, asl #16
	stmfd	sp!, {r4, r5, lr}
.LCFI44:
	movs	r5, r1, lsr #16
	mov	r4, r0
	ldmeqfd	sp!, {r4, r5, pc}
	bl	GuiLib_RgbColorToGrayScale
	mov	r1, r5
	tst	r0, #128
	mov	r0, r4
	bne	.L772
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_BrightenRgbColor
.L772:
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_DarkenRgbColor
.LFE36:
	.size	GuiLib_AccentuateRgbColor, .-GuiLib_AccentuateRgbColor
	.section	.text.GuiLib_AccentuatePixelColor,"ax",%progbits
	.align	2
	.global	GuiLib_AccentuatePixelColor
	.type	GuiLib_AccentuatePixelColor, %function
GuiLib_AccentuatePixelColor:
.LFB37:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r1, r1, asl #16
	stmfd	sp!, {r4, lr}
.LCFI45:
	and	r0, r0, #255
	mov	r4, r1, lsr #16
	bl	GuiLib_PixelToRgbColor
	mov	r1, r4
	bl	GuiLib_AccentuateRgbColor
	ldmfd	sp!, {r4, lr}
	b	GuiLib_RgbToPixelColor
.LFE37:
	.size	GuiLib_AccentuatePixelColor, .-GuiLib_AccentuatePixelColor
	.section	.text.GuiLib_ResetClipping,"ax",%progbits
	.align	2
	.global	GuiLib_ResetClipping
	.type	GuiLib_ResetClipping, %function
GuiLib_ResetClipping:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L775
	mov	r3, #0
	strb	r3, [r2, #0]
.LBB164:
	ldr	r2, .L775+4
	strh	r3, [r2, #0]	@ movhi
	ldr	r2, .L775+8
	strh	r3, [r2, #0]	@ movhi
	ldr	r2, .L775+12
	ldr	r3, .L775+16
	strh	r2, [r3, #0]	@ movhi
	ldr	r3, .L775+20
	mov	r2, #239
	strh	r2, [r3, #0]	@ movhi
.LBE164:
	bx	lr
.L776:
	.align	2
.L775:
	.word	.LANCHOR58
	.word	.LANCHOR60
	.word	.LANCHOR62
	.word	319
	.word	.LANCHOR59
	.word	.LANCHOR61
.LFE40:
	.size	GuiLib_ResetClipping, .-GuiLib_ResetClipping
	.section	.text.GuiLib_SetClipping,"ax",%progbits
	.align	2
	.global	GuiLib_SetClipping
	.type	GuiLib_SetClipping, %function
GuiLib_SetClipping:
.LFB41:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI46:
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	mov	r0, r0, asl #16
	mov	r3, r3, asl #16
	mov	ip, r1, asl #16
	mov	r0, r0, lsr #16
	mov	r3, r3, lsr #16
	mov	r2, r2, asl #16
	cmp	ip, r3, asl #16
	mov	r2, r2, lsr #16
	mov	ip, r0, asl #16
	movle	lr, #0
	movgt	lr, #1
	cmp	ip, r2, asl #16
	ldr	ip, .L778
	orrgt	lr, lr, #1
	strb	lr, [ip, #0]
	ldr	ip, .L778+4
	ldrh	ip, [ip, #0]
	add	r0, ip, r0
	strh	r0, [sp, #12]	@ movhi
	ldr	r0, .L778+8
	add	r2, ip, r2
	ldrh	r0, [r0, #0]
	strh	r2, [sp, #4]	@ movhi
	add	r1, r0, r1
	add	r3, r0, r3
	strh	r1, [sp, #8]	@ movhi
	add	r0, sp, #12
	add	r1, sp, #4
	strh	r3, [sp, #0]	@ movhi
	bl	OrderCoord
	mov	r1, sp
	add	r0, sp, #8
	bl	OrderCoord
	ldr	r3, .L778+12
	ldrsh	r1, [sp, #4]
.LBB165:
.LBB166:
	ldrsh	ip, [sp, #12]
.LBE166:
.LBE165:
	cmp	r1, r3
	movge	r1, r3
.LBB172:
.LBB167:
	ldr	r3, .L778+16
.LBE167:
.LBE172:
	ldrsh	r0, [sp, #8]
.LBB173:
.LBB168:
	bic	ip, ip, ip, asr #31
	strh	ip, [r3, #0]	@ movhi
	ldr	r3, .L778+20
.LBE168:
.LBE173:
	bic	r0, r0, r0, asr #31
.LBB174:
.LBB169:
	strh	r0, [r3, #0]	@ movhi
	ldr	r3, .L778+24
.LBE169:
.LBE174:
	ldrsh	r2, [sp, #0]
.LBB175:
.LBB170:
	strh	r1, [r3, #0]	@ movhi
	ldr	r3, .L778+28
.LBE170:
.LBE175:
	cmp	r2, #239
	movge	r2, #239
.LBB176:
.LBB171:
	strh	r2, [r3, #0]	@ movhi
.LBE171:
.LBE176:
	ldmfd	sp!, {r0, r1, r2, r3, pc}
.L779:
	.align	2
.L778:
	.word	.LANCHOR58
	.word	.LANCHOR63
	.word	.LANCHOR64
	.word	319
	.word	.LANCHOR60
	.word	.LANCHOR62
	.word	.LANCHOR59
	.word	.LANCHOR61
.LFE41:
	.size	GuiLib_SetClipping, .-GuiLib_SetClipping
	.section	.text.GuiLib_ResetDisplayRepaint,"ax",%progbits
	.align	2
	.global	GuiLib_ResetDisplayRepaint
	.type	GuiLib_ResetDisplayRepaint, %function
GuiLib_ResetDisplayRepaint:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, .L783
	mov	r3, #0
	mvn	r2, #0
.L781:
	add	r0, r1, r3
	add	r3, r3, #4
	cmp	r3, #960
	strh	r2, [r0, #2]	@ movhi
	bne	.L781
	bx	lr
.L784:
	.align	2
.L783:
	.word	.LANCHOR0
.LFE42:
	.size	GuiLib_ResetDisplayRepaint, .-GuiLib_ResetDisplayRepaint
	.section	.text.GuiLib_MarkDisplayBoxRepaint,"ax",%progbits
	.align	2
	.global	GuiLib_MarkDisplayBoxRepaint
	.type	GuiLib_MarkDisplayBoxRepaint, %function
GuiLib_MarkDisplayBoxRepaint:
.LFB43:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L787
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI47:
	ldrh	ip, [ip, #0]
	add	r0, r0, ip
	strh	r0, [sp, #12]	@ movhi
	ldr	r0, .L787+4
	add	r2, r2, ip
	ldrh	r0, [r0, #0]
	strh	r2, [sp, #4]	@ movhi
	add	r1, r1, r0
	add	r3, r3, r0
	strh	r1, [sp, #8]	@ movhi
	add	r0, sp, #12
	add	r1, sp, #4
	strh	r3, [sp, #0]	@ movhi
	bl	OrderCoord
	mov	r1, sp
	add	r0, sp, #8
	bl	OrderCoord
	add	r0, sp, #12
	add	r1, sp, #8
	add	r2, sp, #4
	mov	r3, sp
	bl	CheckRect
	cmp	r0, #0
	beq	.L785
	ldrsh	r0, [sp, #12]
	ldrsh	r1, [sp, #8]
	ldrsh	r2, [sp, #4]
	ldrsh	r3, [sp, #0]
	bl	MarkDisplayBoxRepaint
.L785:
	ldmfd	sp!, {r0, r1, r2, r3, pc}
.L788:
	.align	2
.L787:
	.word	.LANCHOR63
	.word	.LANCHOR64
.LFE43:
	.size	GuiLib_MarkDisplayBoxRepaint, .-GuiLib_MarkDisplayBoxRepaint
	.section	.text.GuiLib_ClearDisplay,"ax",%progbits
	.align	2
	.global	GuiLib_ClearDisplay
	.type	GuiLib_ClearDisplay, %function
GuiLib_ClearDisplay:
.LFB44:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
.LBB177:
	ldr	r3, .L790
.LBE177:
	str	lr, [sp, #-4]!
.LCFI48:
.LBB178:
	ldr	r0, [r3, #0]
	mov	r1, #255
	mov	r2, #38400
	bl	memset
.LBE178:
	ldr	r2, .L790+4
	mov	r0, #0
	mov	r1, r0
	mov	r3, #239
	ldr	lr, [sp], #4
	b	MarkDisplayBoxRepaint
.L791:
	.align	2
.L790:
	.word	guilib_display_ptr
	.word	319
.LFE44:
	.size	GuiLib_ClearDisplay, .-GuiLib_ClearDisplay
	.section	.text.GuiLib_Dot,"ax",%progbits
	.align	2
	.global	GuiLib_Dot
	.type	GuiLib_Dot, %function
GuiLib_Dot:
.LFB45:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L793
	and	r2, r2, #15
	ldrh	r3, [r3, #0]
	add	r3, r0, r3
	ldr	r0, .L793+4
	mov	r3, r3, asl #16
	ldrh	r0, [r0, #0]
	add	r1, r1, r0
	mov	r1, r1, asl #16
	mov	r0, r3, asr #16
	mov	r1, r1, asr #16
	b	MakeDot
.L794:
	.align	2
.L793:
	.word	.LANCHOR63
	.word	.LANCHOR64
.LFE45:
	.size	GuiLib_Dot, .-GuiLib_Dot
	.section	.text.GuiLib_GetDot,"ax",%progbits
	.align	2
	.global	GuiLib_GetDot
	.type	GuiLib_GetDot, %function
GuiLib_GetDot:
.LFB46:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L799
	ldr	r2, .L799+4
	ldrh	r3, [r3, #0]
	ldrh	r2, [r2, #0]
	add	r3, r0, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	add	r2, r1, r2
.LBB179:
	cmp	r3, #320
.LBE179:
	mov	r2, r2, asl #16
	mov	r2, r2, asr #16
.LBB180:
	movcc	r0, #0
	movcs	r0, #1
	orrs	r0, r0, r2, lsr #31
	bne	.L797
	cmp	r2, #239
	bxgt	lr
	mov	ip, #160
	ldr	r1, .L799+8
	mul	r2, ip, r2
.LBE180:
	mov	r0, r3, asl #16
.LBB181:
	add	r2, r2, r0, asr #17
	ldr	r1, [r1, #0]
	ldr	r0, .L799+12
	and	r3, r3, #1
	mov	r3, r3, asl #16
	ldrb	r0, [r0, r3, asr #16]	@ zero_extendqisi2
	cmp	r3, #0
	ldrb	r2, [r1, r2]	@ zero_extendqisi2
	movne	r3, #4
	moveq	r3, #0
	cmp	r3, #0
	and	r0, r0, r2
	moveq	r3, #4
	movne	r3, #0
	mov	r0, r0, asr r3
	and	r0, r0, #255
	bx	lr
.L797:
	mov	r0, #0
.LBE181:
	bx	lr
.L800:
	.align	2
.L799:
	.word	.LANCHOR63
	.word	.LANCHOR64
	.word	guilib_display_ptr
	.word	.LANCHOR4
.LFE46:
	.size	GuiLib_GetDot, .-GuiLib_GetDot
	.section	.text.GuiLib_Line,"ax",%progbits
	.align	2
	.global	GuiLib_Line
	.type	GuiLib_Line, %function
GuiLib_Line:
.LFB47:
	@ args = 4, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L819
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, lr}
.LCFI49:
	ldr	lr, .L819+4
	ldrh	ip, [ip, #0]
	ldrh	lr, [lr, #0]
	add	r0, r0, ip
	add	ip, r2, ip
	add	r1, r1, lr
	mov	r0, r0, asl #16
	mov	ip, ip, asl #16
	add	lr, r3, lr
	ldr	r4, [sp, #40]
	mov	r0, r0, lsr #16
	mov	ip, ip, lsr #16
	mov	r1, r1, asl #16
	mov	lr, lr, asl #16
	mov	r1, r1, lsr #16
	mov	lr, lr, lsr #16
	cmp	ip, r0
	strh	r0, [sp, #12]	@ movhi
	strh	r1, [sp, #8]	@ movhi
	strh	ip, [sp, #4]	@ movhi
	strh	lr, [sp, #0]	@ movhi
	and	r4, r4, #15
	bne	.L802
	mov	r1, sp
	add	r0, sp, #8
	bl	OrderCoord
	add	r0, sp, #12
	add	r1, sp, #8
	add	r2, sp, #4
	mov	r3, sp
	bl	CheckRect
	cmp	r0, #0
	beq	.L801
	ldrsh	r0, [sp, #12]
	ldrsh	r1, [sp, #8]
	ldrsh	r2, [sp, #0]
	mov	r3, r4
	bl	VertLine
	ldrsh	r0, [sp, #12]
	ldrsh	r1, [sp, #8]
	mov	r2, r0
	ldrsh	r3, [sp, #0]
	b	.L818
.L802:
	mov	r1, r1, asl #16
	mov	r0, lr, asl #16
	mov	r1, r1, asr #16
	mov	r0, r0, asr #16
	cmp	r1, r0
	bne	.L804
	add	r1, sp, #4
	add	r0, sp, #12
	bl	OrderCoord
	add	r0, sp, #12
	add	r1, sp, #8
	add	r2, sp, #4
	mov	r3, sp
	bl	CheckRect
	cmp	r0, #0
	beq	.L801
	ldrsh	r0, [sp, #12]
	ldrsh	r1, [sp, #4]
	ldrsh	r2, [sp, #8]
	mov	r3, r4
	bl	HorzLine
	ldrsh	r1, [sp, #8]
	ldrsh	r0, [sp, #12]
	ldrsh	r2, [sp, #4]
	mov	r3, r1
.L818:
	bl	MarkDisplayBoxRepaint
	b	.L801
.L804:
	ldr	r5, .L819+8
	rsb	r0, r1, r0
	mul	r0, r5, r0
	bl	labs
	ldrsh	r3, [sp, #12]
	mov	r6, r0
	ldrsh	r0, [sp, #4]
	rsb	r0, r3, r0
	bl	labs
	mov	r1, r0
	mov	r0, r6
	bl	__divsi3
	cmp	r0, r5
	mov	r6, r0
	bgt	.L805
	add	r0, sp, #12
	add	r1, sp, #4
	bl	OrderCoord
	ldr	r7, .L819+12
	mov	r5, #0
	cmp	r0, #0
.LBB182:
	ldrneh	r3, [sp, #8]
	ldrneh	r2, [sp, #0]
	strneh	r3, [sp, #0]	@ movhi
	strneh	r2, [sp, #8]	@ movhi
	b	.L807
.L810:
.LBE182:
	mov	r0, r7
	ldr	r1, .L819+8
	bl	__divsi3
	ldrsh	r2, [sp, #8]
	ldrh	r3, [sp, #8]
	add	r8, r5, r8
	mov	r8, r8, asl #16
	add	r5, r5, #1
	add	r7, r7, r6
	mov	r1, r0, asl #16
	ldrsh	r0, [sp, #0]
	mov	r1, r1, lsr #16
	cmp	r0, r2
	addgt	r1, r1, r3
	rsble	r1, r1, r3
	mov	r1, r1, asl #16
	mov	r0, r8, asr #16
	mov	r1, r1, asr #16
	mov	r2, r4
	bl	MakeDot
.L807:
	ldrsh	r2, [sp, #4]
	ldrsh	r3, [sp, #12]
	ldrh	r8, [sp, #12]
	rsb	r3, r3, r2
	cmp	r5, r3
	ble	.L810
	b	.L801
.L805:
	add	r0, sp, #8
	mov	r1, sp
	bl	OrderCoord
	mov	r7, r6, asr #1
	mov	r5, #0
	cmp	r0, #0
.LBB183:
	ldrneh	r3, [sp, #12]
	ldrneh	r2, [sp, #4]
	strneh	r3, [sp, #4]	@ movhi
	strneh	r2, [sp, #12]	@ movhi
	b	.L812
.L815:
.LBE183:
	mov	r0, r7
	mov	r1, r6
	bl	__divsi3
	ldrsh	r1, [sp, #4]
	ldrsh	r2, [sp, #12]
	ldrh	r3, [sp, #12]
	cmp	r1, r2
	add	r1, r5, r8
	mov	r1, r1, asl #16
	mov	r1, r1, asr #16
	mov	r2, r4
	add	r7, r7, #9984
	add	r5, r5, #1
	add	r7, r7, #16
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	addgt	r0, r0, r3
	rsble	r0, r0, r3
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bl	MakeDot
.L812:
	ldrsh	r2, [sp, #0]
	ldrsh	r3, [sp, #8]
	ldrh	r8, [sp, #8]
	rsb	r3, r3, r2
	cmp	r5, r3
	ble	.L815
.L801:
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, pc}
.L820:
	.align	2
.L819:
	.word	.LANCHOR63
	.word	.LANCHOR64
	.word	10000
	.word	5000
.LFE47:
	.size	GuiLib_Line, .-GuiLib_Line
	.section	.text.GuiLib_LinePattern,"ax",%progbits
	.align	2
	.global	GuiLib_LinePattern
	.type	GuiLib_LinePattern, %function
GuiLib_LinePattern:
.LFB48:
	@ args = 8, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI50:
	ldr	lr, .L843
	sub	sp, sp, #24
.LCFI51:
	ldrb	ip, [sp, #60]	@ zero_extendqisi2
	ldrh	lr, [lr, #0]
	str	ip, [sp, #4]
	ldr	ip, .L843+4
	add	r1, r1, lr
	ldrh	ip, [ip, #0]
	add	lr, r3, lr
	add	r0, r0, ip
	add	sl, r2, ip
	mov	r0, r0, asl #16
	mov	sl, sl, asl #16
	mov	sl, sl, lsr #16
	mov	r0, r0, lsr #16
	strh	r0, [sp, #20]	@ movhi
	rsb	r0, r0, sl
	strh	sl, [sp, #12]	@ movhi
	ldr	r7, [sp, #64]
	rsbs	sl, r0, #0
	mov	r1, r1, asl #16
	mov	lr, lr, asl #16
	adc	sl, sl, r0
	mov	r1, r1, lsr #16
	mov	lr, lr, lsr #16
	cmp	sl, #0
	strh	r1, [sp, #16]	@ movhi
	strh	lr, [sp, #8]	@ movhi
	and	r7, r7, #15
	movne	r5, #0
	bne	.L822
	mov	r1, r1, asl #16
	ldr	r0, .L843+8
	mov	r1, r1, asr #16
	mov	lr, lr, asl #16
	rsb	lr, r1, lr, asr #16
	mul	r0, lr, r0
	bl	labs
	ldrsh	r3, [sp, #20]
	mov	r5, r0
	ldrsh	r0, [sp, #12]
	rsb	r0, r3, r0
	bl	labs
	mov	r1, r0
	mov	r0, r5
	bl	__divsi3
	mov	r5, r0
.L822:
	ldr	r3, .L843+8
	cmp	r5, r3
	movle	r3, sl
	orrgt	r3, sl, #1
	cmp	r3, #0
	beq	.L823
	add	r0, sp, #16
	add	r1, sp, #8
	bl	OrderCoord
	mov	r6, #0
	add	r9, r5, r5, lsr #31
	mov	r9, r9, asr #1
	mov	r8, r6
	mov	fp, r6
	cmp	r0, #0
.LBB184:
	ldrneh	r3, [sp, #20]
	ldrneh	r2, [sp, #12]
	strneh	r3, [sp, #12]	@ movhi
	strneh	r2, [sp, #20]	@ movhi
	b	.L825
.L830:
.LBE184:
	cmp	r8, #0
	ldreq	r4, [sp, #4]
	moveq	r8, #8
	tst	r4, #1
	beq	.L827
	cmp	sl, #0
	bne	.L828
	mov	r0, r9
	mov	r1, r5
	str	r3, [sp, #0]
	bl	__divsi3
	ldr	r3, [sp, #0]
	mov	r0, r0, asl #16
	mov	fp, r0, lsr #16
.L828:
	ldrsh	r0, [sp, #12]
	ldrsh	r1, [sp, #20]
	ldrh	r2, [sp, #20]
	cmp	r0, r1
	addgt	r2, fp, r2
	rsble	r2, fp, r2
	add	r1, r6, r3
	mov	r0, r2, asl #16
	mov	r1, r1, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r7
	bl	MakeDot
.L827:
	mov	r4, r4, lsr #1
	sub	r8, r8, #1
	add	r9, r9, #9984
	and	r4, r4, #255
	and	r8, r8, #255
	add	r6, r6, #1
	add	r9, r9, #16
.L825:
	ldrsh	r1, [sp, #8]
	ldrsh	r2, [sp, #16]
	ldrh	r3, [sp, #16]
	rsb	r2, r2, r1
	cmp	r6, r2
	ble	.L830
	b	.L821
.L823:
	add	r0, sp, #20
	add	r1, sp, #12
	bl	OrderCoord
	mov	r6, #0
	ldr	sl, .L843+12
	mov	r8, r6
	cmp	r0, #0
.LBB185:
	ldrneh	r3, [sp, #16]
	ldrneh	r2, [sp, #8]
	strneh	r3, [sp, #8]	@ movhi
	strneh	r2, [sp, #16]	@ movhi
	b	.L833
.L837:
.LBE185:
	cmp	r8, #0
	ldreq	r4, [sp, #4]
	moveq	r8, #8
	tst	r4, #1
	beq	.L835
	ldr	r1, .L843+8
	mov	r0, sl
	bl	__divsi3
	ldrsh	r2, [sp, #16]
	ldrh	r3, [sp, #16]
	add	r9, r6, r9
	mov	r9, r9, asl #16
	mov	r1, r0, asl #16
	ldrsh	r0, [sp, #8]
	mov	r1, r1, lsr #16
	cmp	r0, r2
	addgt	r1, r1, r3
	rsble	r1, r1, r3
	mov	r1, r1, asl #16
	mov	r0, r9, asr #16
	mov	r1, r1, asr #16
	mov	r2, r7
	bl	MakeDot
.L835:
	mov	r4, r4, lsr #1
	sub	r8, r8, #1
	and	r4, r4, #255
	and	r8, r8, #255
	add	r6, r6, #1
	add	sl, sl, r5
.L833:
	ldrsh	r2, [sp, #12]
	ldrsh	r3, [sp, #20]
	ldrh	r9, [sp, #20]
	rsb	r3, r3, r2
	cmp	r6, r3
	ble	.L837
.L821:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L844:
	.align	2
.L843:
	.word	.LANCHOR64
	.word	.LANCHOR63
	.word	10000
	.word	5000
.LFE48:
	.size	GuiLib_LinePattern, .-GuiLib_LinePattern
	.section	.text.GuiLib_HLine,"ax",%progbits
	.align	2
	.global	GuiLib_HLine
	.type	GuiLib_HLine, %function
GuiLib_HLine:
.LFB49:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r2, r2, asl #16
	stmfd	sp!, {r0, lr}
.LCFI52:
	mov	ip, r1, asl #16
	and	r3, r3, #255
	mov	r1, r2, asr #16
	mov	r0, r0, asl #16
	str	r3, [sp, #0]
	mov	r0, r0, asr #16
	mov	r2, ip, asr #16
	mov	r3, r1
	bl	GuiLib_Line
	ldmfd	sp!, {r3, pc}
.LFE49:
	.size	GuiLib_HLine, .-GuiLib_HLine
	.section	.text.GuiLib_VLine,"ax",%progbits
	.align	2
	.global	GuiLib_VLine
	.type	GuiLib_VLine, %function
GuiLib_VLine:
.LFB50:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, lr}
.LCFI53:
	mov	r0, r0, asl #16
	mov	ip, r2, asl #16
	mov	r0, r0, asr #16
	and	r3, r3, #255
	mov	r1, r1, asl #16
	str	r3, [sp, #0]
	mov	r1, r1, asr #16
	mov	r2, r0
	mov	r3, ip, asr #16
	bl	GuiLib_Line
	ldmfd	sp!, {r3, pc}
.LFE50:
	.size	GuiLib_VLine, .-GuiLib_VLine
	.section	.text.GuiLib_Ellipse,"ax",%progbits
	.align	2
	.global	GuiLib_Ellipse
	.type	GuiLib_Ellipse, %function
GuiLib_Ellipse:
.LFB38:
	@ args = 8, pretend = 0, frame = 52
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI54:
	sub	sp, sp, #52
.LCFI55:
	ldr	ip, [sp, #92]
	ldr	r4, [sp, #88]
	str	ip, [sp, #8]
	adds	ip, ip, #-268435456
	movne	ip, #1
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	cmp	r4, #268435456
	mov	r5, r0, asr #16
	mov	r6, r1, asr #16
	mov	r7, r2, lsr #16
	mov	r8, r3, lsr #16
	str	ip, [sp, #12]
	andne	r4, r4, #255
	bne	.L850
	cmp	ip, #0
	ldrne	ip, [sp, #8]
	andne	r4, ip, #255
	beq	.L884
.L850:
	cmp	r7, #0
	bne	.L851
	mov	r2, r6, asl #16
	mov	r2, r2, lsr #16
	rsb	r1, r8, r2
	add	r2, r8, r2
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	b	.L880
.L851:
	cmp	r8, #0
	bne	.L852
	mov	r1, r5, asl #16
	mov	r1, r1, lsr #16
	rsb	r0, r7, r1
	add	r1, r7, r1
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	b	.L883
.L852:
	cmp	r7, #1
	bne	.L853
	mov	r6, r6, asl #16
	mov	r6, r6, lsr #16
	mov	sl, r8, lsr #1
	rsb	r7, sl, r6
	add	sl, sl, r6
	mov	r0, r5, asl #16
	mov	r7, r7, asl #16
	mov	sl, sl, asl #16
	mov	ip, r0, lsr #16
	mov	r9, r7, lsr #16
	mov	fp, sl, lsr #16
	mov	r7, r7, asr #16
	mov	sl, sl, asr #16
	sub	r0, r0, #65536
	mov	r0, r0, asr #16
	mov	r1, r7
	mov	r2, sl
	mov	r3, r4
	str	ip, [sp, #0]
	sub	r9, r9, #1
	bl	GuiLib_VLine
	rsb	r1, r8, r6
	mov	r1, r1, asl #16
	mov	r2, r9, asl #16
	mov	r0, r5
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r4
	bl	GuiLib_VLine
	ldr	ip, [sp, #12]
	cmp	ip, #0
	beq	.L854
	ldr	ip, [sp, #8]
	mov	r0, r5
	and	r3, ip, #255
	mov	r1, r7
	mov	r2, sl
	bl	GuiLib_VLine
.L854:
	add	fp, fp, #1
	add	r6, r8, r6
	mov	r1, fp, asl #16
	mov	r2, r6, asl #16
	mov	r0, r5
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r4
	bl	GuiLib_VLine
	ldr	ip, [sp, #0]
	mov	r1, r7
	add	r0, ip, #1
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	mov	r2, sl
	b	.L882
.L853:
	cmp	r8, #1
	bne	.L855
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
	mov	sl, r7, lsr #1
	rsb	r8, sl, r5
	add	sl, sl, r5
	mov	r8, r8, asl #16
	mov	sl, sl, asl #16
	mov	r2, r6, asl #16
	mov	ip, r2, lsr #16
	mov	r9, r8, lsr #16
	mov	fp, sl, lsr #16
	mov	r8, r8, asr #16
	mov	sl, sl, asr #16
	sub	r2, r2, #65536
	mov	r0, r8
	mov	r1, sl
	mov	r2, r2, asr #16
	mov	r3, r4
	str	ip, [sp, #0]
	sub	r9, r9, #1
	bl	GuiLib_HLine
	rsb	r0, r7, r5
	mov	r0, r0, asl #16
	mov	r1, r9, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r6
	mov	r3, r4
	bl	GuiLib_HLine
	ldr	ip, [sp, #12]
	cmp	ip, #0
	beq	.L856
	ldr	ip, [sp, #8]
	mov	r0, r8
	and	r3, ip, #255
	mov	r1, sl
	mov	r2, r6
	bl	GuiLib_HLine
.L856:
	add	fp, fp, #1
	add	r5, r7, r5
	mov	r0, fp, asl #16
	mov	r1, r5, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r6
	mov	r3, r4
	bl	GuiLib_HLine
	ldr	ip, [sp, #0]
	mov	r0, r8
	add	r2, ip, #1
	mov	r2, r2, asl #16
	mov	r1, sl
	mov	r2, r2, asr #16
.L881:
	mov	r3, r4
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	GuiLib_HLine
.L855:
	mov	r9, r7, asl #19
	mov	sl, r8, asl #19
	cmp	r7, r8
	mov	r9, r9, lsr #16
	mov	sl, sl, lsr #16
	bcc	.L857
	smulbb	ip, sl, sl
	mov	r9, r9, asl #16
	sub	sl, sl, #4
	mov	r8, r5, asl #16
	mov	sl, sl, asl #16
	mov	r3, #1
	mov	r9, r9, asr #16
	str	r5, [sp, #44]
	str	ip, [sp, #36]
	mov	sl, sl, lsr #16
	mov	fp, #0
	str	r9, [sp, #40]
	mov	r8, r8, lsr #16
	mov	r5, r3
	str	r7, [sp, #48]
	b	.L858
.L864:
	smulbb	r0, sl, sl
	ldr	r1, [sp, #36]
	mov	r0, r0, asl #10
	bl	__divsi3
	add	r9, r9, #4
	mov	r9, r9, asl #13
	mov	r9, r9, lsr #16
	rsb	r0, r0, #1024
	mov	r0, r0, asl #8
	bl	GuiLib_Sqrt
	ldr	ip, [sp, #40]
	cmp	r5, #0
	mul	r3, ip, r0
	mov	r3, r3, lsr #9
	add	r3, r3, #4
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	str	r3, [sp, #0]
	beq	.L859
	mov	fp, r3, asl #16
	mov	fp, fp, asr #19
	mov	fp, fp, asl #16
	mov	fp, fp, lsr #16
	mov	r5, r6, asl #16
	rsb	r7, fp, r8
	mov	r5, r5, lsr #16
	add	fp, fp, r8
	rsb	r2, r9, r5
	mov	r7, r7, asl #16
	mov	fp, fp, asl #16
	mov	r7, r7, asr #16
	mov	fp, fp, asr #16
	mov	r2, r2, asl #16
	mov	r0, r7
	mov	r1, fp
	mov	r2, r2, asr #16
	mov	r3, r4
	add	r9, r9, r5
	bl	GuiLib_HLine
	mov	r2, r9, asl #16
	mov	r0, r7
	mov	r1, fp
	mov	r2, r2, asr #16
	b	.L878
.L859:
	ldr	ip, [sp, #0]
	add	r7, fp, #8
	mov	r3, ip, asl #16
	mov	r7, r7, asl #16
	mov	r3, r3, asr #19
	mov	r7, r7, asr #19
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r7, r7, asl #16
	mov	r7, r7, lsr #16
	rsb	r2, r3, r8
	add	r3, r3, r8
	rsb	r1, r7, r8
	mov	r3, r3, asl #16
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, lsr #16
	cmp	r1, r2
	str	r3, [sp, #4]
	mov	r3, r2, lsr #16
	mov	r5, r1, lsr #16
	movlt	r5, r3
	mov	r3, r3, asl #16
	addge	r7, r7, r8
	ldrlt	r7, [sp, #4]
	mov	r3, r3, asr #16
	movge	r7, r7, asl #16
	str	r3, [sp, #24]
	mov	r3, r6, asl #16
	movge	r7, r7, lsr #16
	mov	r3, r3, lsr #16
	ldr	ip, [sp, #12]
	str	r9, [sp, #32]
	mov	r2, r7, asl #16
	rsb	r9, r9, r3
	mov	fp, r5, asl #16
	mov	r2, r2, asr #16
	mov	fp, fp, asr #16
	mov	r9, r9, asl #16
	str	r2, [sp, #16]
	mov	r9, r9, asr #16
	rsb	r2, fp, r2
	cmp	r2, #1
	movle	ip, #0
	andgt	ip, ip, #1
	str	r3, [sp, #28]
	ldr	r0, [sp, #24]
	mov	r1, fp
	mov	r2, r9
	mov	r3, r4
	str	ip, [sp, #20]
	bl	GuiLib_HLine
	ldr	ip, [sp, #20]
	cmp	ip, #0
	beq	.L862
	ldr	ip, [sp, #8]
	add	r0, r5, #1
	sub	r1, r7, #1
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	and	r3, ip, #255
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r9
	bl	GuiLib_HLine
.L862:
	ldr	ip, [sp, #4]
	mov	r2, r9
	mov	r3, ip, asl #16
	mov	r3, r3, asr #16
	mov	r1, r3
	str	r3, [sp, #4]
	ldr	r0, [sp, #16]
	mov	r3, r4
	bl	GuiLib_HLine
	ldr	ip, [sp, #32]
	ldr	r0, [sp, #28]
	mov	r1, fp
	add	r9, ip, r0
	mov	r9, r9, asl #16
	mov	r9, r9, asr #16
	ldr	r0, [sp, #24]
	mov	r2, r9
	mov	r3, r4
	bl	GuiLib_HLine
	ldr	ip, [sp, #20]
	cmp	ip, #0
	beq	.L863
	ldr	ip, [sp, #8]
	add	r5, r5, #1
	sub	r7, r7, #1
	mov	r0, r5, asl #16
	mov	r1, r7, asl #16
	and	r3, ip, #255
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r9
	bl	GuiLib_HLine
.L863:
	ldr	r0, [sp, #16]
	ldr	r1, [sp, #4]
	mov	r2, r9
.L878:
	mov	r3, r4
	bl	GuiLib_HLine
	sub	sl, sl, #8
	ldr	fp, [sp, #0]
	mov	sl, sl, asl #16
	mov	sl, sl, lsr #16
	mov	r5, #0
.L858:
	mov	r9, sl, asl #16
	mov	r9, r9, asr #16
	cmp	r9, #3
	bgt	.L864
	ldr	r7, [sp, #48]
	mov	fp, fp, asl #16
	mov	fp, fp, asr #3
	ldr	r5, [sp, #44]
	add	fp, fp, #65536
	cmp	r7, fp, asr #16
	mov	sl, fp, lsr #16
	movlt	sl, r7
	mov	sl, sl, asl #16
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
	mov	r8, sl, lsr #16
	rsb	r1, r8, r5
	rsb	r0, r7, r5
	mov	r1, r1, asl #16
	mov	r0, r0, asl #16
	mov	r9, r1, lsr #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r6
	mov	r3, r4
	bl	GuiLib_HLine
	ldr	ip, [sp, #12]
	cmp	sl, #0
	movle	ip, #0
	andgt	ip, ip, #1
	cmp	ip, #0
	mov	sl, ip
	beq	.L866
	sub	r1, r5, #1
	ldr	ip, [sp, #8]
	add	r9, r9, #1
	add	r1, r8, r1
	mov	r0, r9, asl #16
	mov	r1, r1, asl #16
	and	r3, ip, #255
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r6
	bl	GuiLib_HLine
.L866:
	add	r8, r8, r5
	add	r5, r7, r5
	mov	r0, r8, asl #16
	mov	r1, r5, asl #16
.L883:
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r6
	b	.L881
.L857:
	smulbb	ip, r9, r9
	mov	sl, sl, asl #16
	sub	r9, r9, #4
	mov	r7, r6, asl #16
	mov	r9, r9, asl #16
	mov	r3, #1
	mov	sl, sl, asr #16
	str	r6, [sp, #44]
	str	ip, [sp, #36]
	mov	r9, r9, lsr #16
	mov	fp, #0
	str	sl, [sp, #40]
	mov	r7, r7, lsr #16
	mov	r6, r3
	str	r8, [sp, #48]
	b	.L867
.L873:
	smulbb	r0, r9, r9
	ldr	r1, [sp, #36]
	mov	r0, r0, asl #10
	bl	__divsi3
	add	sl, sl, #4
	mov	sl, sl, asl #13
	mov	sl, sl, lsr #16
	rsb	r0, r0, #1024
	mov	r0, r0, asl #8
	bl	GuiLib_Sqrt
	ldr	ip, [sp, #40]
	cmp	r6, #0
	mul	r3, ip, r0
	mov	r3, r3, lsr #9
	add	r3, r3, #4
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	str	r3, [sp, #0]
	beq	.L868
	mov	fp, r3, asl #16
	mov	fp, fp, asr #19
	mov	fp, fp, asl #16
	mov	fp, fp, lsr #16
	mov	r6, r5, asl #16
	mov	r6, r6, lsr #16
	rsb	r8, fp, r7
	add	fp, fp, r7
	rsb	r0, sl, r6
	mov	r8, r8, asl #16
	mov	fp, fp, asl #16
	mov	r8, r8, asr #16
	mov	fp, fp, asr #16
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	mov	r1, r8
	mov	r2, fp
	mov	r3, r4
	add	sl, sl, r6
	bl	GuiLib_VLine
	mov	r0, sl, asl #16
	mov	r0, r0, asr #16
	mov	r1, r8
	mov	r2, fp
	b	.L879
.L868:
	ldr	ip, [sp, #0]
	add	r8, fp, #8
	mov	r3, ip, asl #16
	mov	r8, r8, asl #16
	mov	r8, r8, asr #19
	mov	r3, r3, asr #19
	mov	r8, r8, asl #16
	mov	r3, r3, asl #16
	mov	r8, r8, lsr #16
	mov	r3, r3, lsr #16
	rsb	r1, r8, r7
	rsb	r2, r3, r7
	add	r3, r3, r7
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	cmp	r1, r2
	mov	r3, r3, lsr #16
	addge	r8, r8, r7
	str	r3, [sp, #4]
	ldrlt	r8, [sp, #4]
	movge	r8, r8, asl #16
	mov	r3, r2, lsr #16
	movge	r8, r8, lsr #16
	mov	r6, r1, lsr #16
	movlt	r6, r3
	ldr	ip, [sp, #12]
	mov	r2, r8, asl #16
	mov	fp, r6, asl #16
	mov	r2, r2, asr #16
	mov	fp, fp, asr #16
	str	r2, [sp, #16]
	rsb	r2, fp, r2
	cmp	r2, #1
	movle	ip, #0
	andgt	ip, ip, #1
	mov	r2, r5, asl #16
	mov	r2, r2, lsr #16
	str	sl, [sp, #28]
	rsb	sl, sl, r2
	mov	sl, sl, asl #16
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	sl, sl, asr #16
	str	r2, [sp, #24]
	str	r3, [sp, #32]
	mov	r1, r3
	mov	r0, sl
	mov	r2, fp
	mov	r3, r4
	str	ip, [sp, #20]
	bl	GuiLib_VLine
	ldr	ip, [sp, #20]
	cmp	ip, #0
	beq	.L871
	ldr	ip, [sp, #8]
	add	r3, r6, #1
	sub	r2, r8, #1
	mov	r1, r3, asl #16
	mov	r2, r2, asl #16
	and	r3, ip, #255
	mov	r0, sl
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	bl	GuiLib_VLine
.L871:
	ldr	ip, [sp, #4]
	mov	r0, sl
	mov	r3, ip, asl #16
	mov	r3, r3, asr #16
	mov	r2, r3
	str	r3, [sp, #4]
	ldr	r1, [sp, #16]
	mov	r3, r4
	bl	GuiLib_VLine
	ldr	ip, [sp, #28]
	ldr	r0, [sp, #24]
	ldr	r1, [sp, #32]
	add	sl, ip, r0
	mov	sl, sl, asl #16
	mov	sl, sl, asr #16
	mov	r0, sl
	mov	r2, fp
	mov	r3, r4
	bl	GuiLib_VLine
	ldr	ip, [sp, #20]
	cmp	ip, #0
	beq	.L872
	ldr	ip, [sp, #8]
	add	r6, r6, #1
	sub	r8, r8, #1
	mov	r1, r6, asl #16
	mov	r2, r8, asl #16
	and	r3, ip, #255
	mov	r0, sl
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	bl	GuiLib_VLine
.L872:
	ldr	r1, [sp, #16]
	ldr	r2, [sp, #4]
	mov	r0, sl
.L879:
	mov	r3, r4
	bl	GuiLib_VLine
	sub	r9, r9, #8
	ldr	fp, [sp, #0]
	mov	r9, r9, asl #16
	mov	r9, r9, lsr #16
	mov	r6, #0
.L867:
	mov	sl, r9, asl #16
	mov	sl, sl, asr #16
	cmp	sl, #3
	bgt	.L873
	ldr	r8, [sp, #48]
	mov	fp, fp, asl #16
	mov	fp, fp, asr #3
	ldr	r6, [sp, #44]
	add	fp, fp, #65536
	cmp	r8, fp, asr #16
	mov	sl, fp, lsr #16
	movlt	sl, r8
	mov	sl, sl, asl #16
	mov	r6, r6, asl #16
	mov	r6, r6, lsr #16
	mov	r7, sl, lsr #16
	rsb	r2, r7, r6
	rsb	r1, r8, r6
	mov	r2, r2, asl #16
	mov	r1, r1, asl #16
	mov	r9, r2, lsr #16
	mov	r0, r5
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r4
	bl	GuiLib_VLine
	ldr	ip, [sp, #12]
	cmp	sl, #0
	movle	ip, #0
	andgt	ip, ip, #1
	cmp	ip, #0
	mov	sl, ip
	beq	.L875
	sub	r2, r6, #1
	ldr	ip, [sp, #8]
	add	r9, r9, #1
	add	r2, r7, r2
	mov	r1, r9, asl #16
	mov	r2, r2, asl #16
	and	r3, ip, #255
	mov	r0, r5
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	bl	GuiLib_VLine
.L875:
	add	r7, r7, r6
	add	r6, r8, r6
	mov	r1, r7, asl #16
	mov	r2, r6, asl #16
.L880:
	mov	r0, r5
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
.L882:
	mov	r3, r4
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	GuiLib_VLine
.L884:
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.LFE38:
	.size	GuiLib_Ellipse, .-GuiLib_Ellipse
	.section	.text.GuiLib_Circle,"ax",%progbits
	.align	2
	.global	GuiLib_Circle
	.type	GuiLib_Circle, %function
GuiLib_Circle:
.LFB39:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, lr}
.LCFI56:
	str	r3, [sp, #0]
	ldr	r3, [sp, #12]
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	str	r3, [sp, #4]
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r3, r2
	bl	GuiLib_Ellipse
	ldmfd	sp!, {r2, r3, pc}
.LFE39:
	.size	GuiLib_Circle, .-GuiLib_Circle
	.section	.text.GuiLib_Box,"ax",%progbits
	.align	2
	.global	GuiLib_Box
	.type	GuiLib_Box, %function
GuiLib_Box:
.LFB51:
	@ args = 4, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L892
	stmfd	sp!, {r4, lr}
.LCFI57:
	ldrh	ip, [ip, #0]
	sub	sp, sp, #24
.LCFI58:
	add	r0, r0, ip
	strh	r0, [sp, #12]	@ movhi
	ldr	r0, .L892+4
	add	r2, r2, ip
	ldrh	r0, [r0, #0]
	strh	r2, [sp, #4]	@ movhi
	add	r1, r1, r0
	add	r3, r3, r0
	strh	r1, [sp, #8]	@ movhi
	add	r0, sp, #12
	add	r1, sp, #4
	strh	r3, [sp, #0]	@ movhi
	ldr	r4, [sp, #32]
	bl	OrderCoord
	mov	r1, sp
	add	r0, sp, #8
	bl	OrderCoord
	ldrh	r3, [sp, #12]
	add	r0, sp, #16
	strh	r3, [sp, #16]	@ movhi
	ldrh	r3, [sp, #8]
	add	r1, sp, #18
	strh	r3, [sp, #18]	@ movhi
	ldrh	r3, [sp, #4]
	add	r2, sp, #20
	strh	r3, [sp, #20]	@ movhi
	ldrh	r3, [sp, #0]
	strh	r3, [sp, #22]	@ movhi
	add	r3, sp, #22
	bl	CheckRect
	cmp	r0, #0
	beq	.L886
	ldrh	r3, [sp, #12]
	ldrh	r2, [sp, #8]
	strh	r3, [sp, #16]	@ movhi
	strh	r3, [sp, #20]	@ movhi
	ldrh	r3, [sp, #0]
	add	r0, sp, #16
	strh	r2, [sp, #18]	@ movhi
	strh	r3, [sp, #22]	@ movhi
	add	r1, sp, #18
	mov	r2, r0
	add	r3, sp, #22
	bl	CheckRect
	and	r4, r4, #15
	cmp	r0, #0
	beq	.L888
	ldrsh	r0, [sp, #16]
	ldrsh	r1, [sp, #18]
	ldrsh	r2, [sp, #22]
	mov	r3, r4
	bl	VertLine
.L888:
	ldrh	r3, [sp, #4]
	ldrh	r2, [sp, #8]
	strh	r3, [sp, #16]	@ movhi
	strh	r3, [sp, #20]	@ movhi
	ldrh	r3, [sp, #0]
	add	r0, sp, #20
	strh	r2, [sp, #18]	@ movhi
	strh	r3, [sp, #22]	@ movhi
	add	r1, sp, #18
	mov	r2, r0
	add	r3, sp, #22
	bl	CheckRect
	cmp	r0, #0
	beq	.L889
	ldrsh	r0, [sp, #20]
	ldrsh	r1, [sp, #18]
	ldrsh	r2, [sp, #22]
	mov	r3, r4
	bl	VertLine
.L889:
	ldrsh	r0, [sp, #4]
	ldrsh	r1, [sp, #12]
	ldrh	r3, [sp, #4]
	rsb	r1, r1, r0
	cmp	r1, #1
	ldrh	r2, [sp, #12]
	ble	.L890
	add	r2, r2, #1
	strh	r2, [sp, #16]	@ movhi
	ldrh	r2, [sp, #8]
	sub	r3, r3, #1
	add	r1, sp, #18
	strh	r2, [sp, #18]	@ movhi
	strh	r3, [sp, #20]	@ movhi
	add	r0, sp, #16
	add	r2, sp, #20
	mov	r3, r1
	bl	CheckRect
	cmp	r0, #0
	beq	.L891
	ldrsh	r0, [sp, #16]
	ldrsh	r1, [sp, #20]
	ldrsh	r2, [sp, #18]
	mov	r3, r4
	bl	HorzLine
.L891:
	ldrh	r3, [sp, #12]
	ldrh	r2, [sp, #4]
	add	r3, r3, #1
	strh	r3, [sp, #16]	@ movhi
	ldrh	r3, [sp, #0]
	sub	r2, r2, #1
	add	r1, sp, #22
	strh	r3, [sp, #18]	@ movhi
	strh	r2, [sp, #20]	@ movhi
	strh	r3, [sp, #22]	@ movhi
	add	r0, sp, #16
	add	r2, sp, #20
	mov	r3, r1
	bl	CheckRect
	cmp	r0, #0
	beq	.L890
	ldrsh	r0, [sp, #16]
	ldrsh	r1, [sp, #20]
	ldrsh	r2, [sp, #22]
	mov	r3, r4
	bl	HorzLine
.L890:
	add	r1, sp, #8
	add	r2, sp, #4
	mov	r3, sp
	add	r0, sp, #12
	bl	CheckRect
	ldrsh	r0, [sp, #12]
	ldrsh	r1, [sp, #8]
	ldrsh	r2, [sp, #4]
	ldrsh	r3, [sp, #0]
	bl	MarkDisplayBoxRepaint
.L886:
	add	sp, sp, #24
	ldmfd	sp!, {r4, pc}
.L893:
	.align	2
.L892:
	.word	.LANCHOR63
	.word	.LANCHOR64
.LFE51:
	.size	GuiLib_Box, .-GuiLib_Box
	.section	.text.GuiLib_FillBox,"ax",%progbits
	.align	2
	.global	GuiLib_FillBox
	.type	GuiLib_FillBox, %function
GuiLib_FillBox:
.LFB52:
	@ args = 4, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L898
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI59:
	ldrh	ip, [ip, #0]
	ldr	r4, [sp, #24]
	add	r0, r0, ip
	strh	r0, [sp, #12]	@ movhi
	ldr	r0, .L898+4
	add	r2, r2, ip
	ldrh	r0, [r0, #0]
	strh	r2, [sp, #4]	@ movhi
	add	r1, r1, r0
	add	r3, r3, r0
	strh	r1, [sp, #8]	@ movhi
	add	r0, sp, #12
	add	r1, sp, #4
	strh	r3, [sp, #0]	@ movhi
	bl	OrderCoord
	mov	r1, sp
	add	r0, sp, #8
	bl	OrderCoord
	add	r0, sp, #12
	add	r1, sp, #8
	add	r2, sp, #4
	mov	r3, sp
	bl	CheckRect
	and	r4, r4, #15
	cmp	r0, #0
	beq	.L894
	ldrsh	r0, [sp, #12]
	ldrsh	r1, [sp, #8]
	ldrsh	r2, [sp, #4]
	ldrsh	r3, [sp, #0]
	bl	MarkDisplayBoxRepaint
	b	.L896
.L897:
	mov	r3, r4
	ldrsh	r0, [sp, #12]
	ldrsh	r1, [sp, #4]
	bl	HorzLine
	ldrh	r3, [sp, #8]
	add	r3, r3, #1
	strh	r3, [sp, #8]	@ movhi
.L896:
	ldrsh	r3, [sp, #0]
	ldrsh	r2, [sp, #8]
	cmp	r3, r2
	bge	.L897
.L894:
	ldmfd	sp!, {r0, r1, r2, r3, r4, pc}
.L899:
	.align	2
.L898:
	.word	.LANCHOR63
	.word	.LANCHOR64
.LFE52:
	.size	GuiLib_FillBox, .-GuiLib_FillBox
	.section	.text.DrawText,"ax",%progbits
	.align	2
	.type	DrawText, %function
DrawText:
.LFB71:
	@ args = 4, pretend = 0, frame = 4016
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI60:
	mov	r5, r1, asl #16
	sub	sp, sp, #4016
.LCFI61:
	mov	r4, r5, lsr #16
	sub	sp, sp, #4
.LCFI62:
	mov	r1, r4
	str	r3, [sp, #8]
	str	r2, [sp, #12]
	ldrb	r7, [sp, #4056]	@ zero_extendqisi2
	bl	PrepareText
	ldr	r3, .L920
	ldr	r3, [r3, #60]
	tst	r3, #2048
	beq	.L901
	ldr	r1, .L920+4
	ldr	r2, .L920+8
	mov	r3, #0
	mov	r0, r3
	mov	ip, r3
	mov	r5, r5, lsr #17
	add	r6, r1, r4, asl #2
	add	lr, r2, r4
	b	.L902
.L903:
	ldr	r8, [r1, #-4]
	ldr	sl, [r6, r0]
	add	ip, ip, #1
	str	sl, [r1, #-4]
	ldrb	sl, [lr, r3]	@ zero_extendqisi2
	str	r8, [r6, r0]
	ldrb	r8, [r2, #-1]	@ zero_extendqisi2
	mov	ip, ip, asl #16
	mov	ip, ip, lsr #16
	strb	sl, [r2, #-1]
	strb	r8, [lr, r3]
.L902:
	mov	r8, ip, asl #16
	cmp	r5, r8, asr #16
	add	r1, r1, #4
	sub	r0, r0, #4
	add	r2, r2, #1
	sub	r3, r3, #1
	bne	.L903
.L901:
	ldr	r6, .L920
	add	r9, sp, #16
	ldrb	r0, [r6, #70]	@ zero_extendqisi2
	mov	r1, r4
	mov	r2, r9
	bl	TextPixelLength
	ldrb	r3, [r6, #64]	@ zero_extendqisi2
	ldrh	r5, [r6, #10]
	cmp	r3, #2
	beq	.L905
	cmp	r3, #3
	bne	.L904
	b	.L919
.L905:
	cmp	r4, #0
	addne	r3, r0, r0, lsr #31
	subne	r5, r5, r3, asr #1
	bne	.L917
	b	.L904
.L919:
	add	r5, r5, #1
	rsb	r5, r0, r5
.L917:
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
.L904:
	ldr	r3, .L920+12
	mov	ip, r5, asl #16
	ldr	r2, [r3, #0]
	ldr	r3, .L920
	ldrb	r1, [r2, #14]	@ zero_extendqisi2
	ldrh	r8, [r3, #12]
	ldrb	r2, [r2, #11]	@ zero_extendqisi2
	rsb	r8, r1, r8
	mov	r8, r8, asl #16
	mov	r8, r8, lsr #16
	sub	r2, r2, #1
	add	r2, r8, r2
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	str	r2, [sp, #4]
	ldr	r2, .L920+16
	sub	sl, r0, #1
	strh	r5, [r2, #0]	@ movhi
	ldr	r2, .L920+20
	mov	fp, ip, lsr #16
	strh	r8, [r2, #0]	@ movhi
	add	sl, fp, sl
	ldr	r2, .L920+24
	mov	sl, sl, asl #16
	mov	sl, sl, lsr #16
	strh	sl, [r2, #0]	@ movhi
	ldr	r0, [sp, #4]
	ldr	r2, .L920+28
	mov	r6, r8
	strh	r0, [r2, #0]	@ movhi
	ldr	r2, .L920+32
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L900
	cmp	r4, #0
	beq	.L908
	cmp	r7, #0
	bne	.L909
	ldrsh	r3, [r3, #18]
	cmp	r3, #0
	bne	.L909
	mov	r3, r0, asl #16
	mov	r1, r8, asl #16
	mov	r2, sl, asl #16
	mov	r0, ip, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	bl	SetBackColorBox
	ldr	r3, .L920+36
	ldr	ip, [sp, #8]
	ldrsh	r0, [r3, #0]
	ldr	r3, .L920+40
	ldrsh	r1, [r3, #0]
	ldr	r3, .L920+44
	ldrsh	r2, [r3, #0]
	ldr	r3, .L920+48
	ldrsh	r3, [r3, #0]
	str	ip, [sp, #0]
	bl	GuiLib_FillBox
.L909:
	mov	ip, r6, asl #16
	mov	ip, ip, asr #16
	str	r5, [sp, #8]
	mov	r7, #0
	mov	r5, ip
	b	.L910
.L911:
	mov	r3, r7, asl #1
	ldrh	r0, [r9, r3]
	ldr	r3, .L920+4
	add	r0, fp, r0
	mov	r0, r0, asl #16
	ldr	r2, [r3, r7, asl #2]
	mov	r0, r0, asr #16
	mov	r1, r5
	ldr	r3, [sp, #12]
	bl	DrawChar.isra.1
	add	r7, r7, #1
.L910:
	cmp	r7, r4
	blt	.L911
	ldr	r5, [sp, #8]
.L908:
	ldr	r3, .L920
	ldr	r3, [r3, #60]
	tst	r3, #4
	moveq	r3, #0
	movne	r3, #1
	cmp	r4, #0
	moveq	r3, #0
	andne	r3, r3, #1
	cmp	r3, #0
	beq	.L912
	ldr	r3, .L920+12
	ldr	ip, [sp, #12]
	ldr	r3, [r3, #0]
	mov	r0, r5, asl #16
	ldrb	r1, [r3, #17]	@ zero_extendqisi2
	ldrb	r3, [r3, #18]	@ zero_extendqisi2
	add	r1, r8, r1
	add	r8, r8, r3
	mov	r1, r1, asl #16
	mov	r2, sl, asl #16
	mov	r3, r8, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	str	ip, [sp, #0]
	bl	GuiLib_FillBox
.L912:
	ldr	ip, [sp, #4]
	mov	r0, r5, asl #16
	mov	r3, ip, asl #16
	mov	r1, r6, asl #16
	mov	r2, sl, asl #16
	mov	r3, r3, asr #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	bl	GuiLib_MarkDisplayBoxRepaint
	ldr	r3, .L920
	ldr	fp, [r3, #60]
	tst	fp, #256
	beq	.L900
	ldrsb	r1, [r3, #79]
	cmp	r1, #0
	bgt	.L900
	mov	r5, #60
	mul	r0, r5, r1
	ldr	r7, .L920+52
	ldrb	r9, [r3, #72]	@ zero_extendqisi2
	add	r2, r7, r0
	strh	r4, [r2, #28]	@ movhi
	ldrb	r4, [r3, #70]	@ zero_extendqisi2
	strb	r9, [r2, #12]
	strb	r4, [r2, #7]
	ldrb	r4, [r3, #64]	@ zero_extendqisi2
	ldrb	r9, [r3, #73]	@ zero_extendqisi2
	strb	r4, [r2, #11]
	ldrb	r4, [r3, #81]	@ zero_extendqisi2
	strb	r9, [r2, #13]
	strb	r4, [r2, #9]
	ldrb	r9, [r3, #74]	@ zero_extendqisi2
	ldrb	r4, [r3, #80]	@ zero_extendqisi2
	ldrb	r8, [r3, #56]	@ zero_extendqisi2
	strb	r4, [r2, #10]
	strb	r9, [r2, #14]
	ldrb	r4, [r3, #52]	@ zero_extendqisi2
	ldrb	r9, [r3, #75]	@ zero_extendqisi2
	mov	ip, #1
	strb	ip, [r2, #4]
	strb	r4, [r2, #16]
	add	ip, r2, #16
	add	sl, r2, #4
	strb	r8, [r2, #8]
	strb	r9, [r2, #15]
	ldr	r2, .L920+12
	cmp	r8, #0
	ldr	r2, [r2, #0]
	ldreq	r3, [r3, #4]
	ldrne	r3, [r3, #0]
	ldrb	r4, [r2, #10]	@ zero_extendqisi2
	str	r3, [r7, r0]
	strb	r4, [sl, #2]
	ldr	r3, .L920
	ldrb	r4, [r2, #20]	@ zero_extendqisi2
	mla	r1, r5, r1, r7
	strb	r4, [ip, #1]
	ldrh	r0, [r3, #10]
	ldrb	r4, [r2, #21]	@ zero_extendqisi2
	str	fp, [r1, #24]
	strb	r4, [ip, #2]
	strh	r0, [r1, #30]	@ movhi
	ldrh	r0, [r3, #14]
	add	ip, r1, #32
	strh	r0, [r1, #32]	@ movhi
	ldrsh	r0, [r3, #18]
	cmp	r0, #0
	strleh	r6, [ip, #2]	@ movhi
	ldrle	ip, [sp, #4]
	strleh	ip, [r1, #36]	@ movhi
	ble	.L900
	ldrb	r4, [r3, #20]	@ zero_extendqisi2
	ldrh	r0, [r3, #12]
	cmp	r4, #0
	beq	.L916
	ldrb	r3, [r3, #21]	@ zero_extendqisi2
	rsb	r4, r4, r0
	add	r0, r0, r3
	strh	r4, [ip, #2]	@ movhi
	strh	r0, [r1, #36]	@ movhi
	b	.L900
.L916:
	ldrb	r3, [r2, #14]	@ zero_extendqisi2
	ldrb	r2, [r2, #11]	@ zero_extendqisi2
	rsb	r4, r3, r0
	add	r0, r0, r2
	sub	r0, r0, #1
	rsb	r3, r3, r0
	strh	r4, [ip, #2]	@ movhi
	strh	r3, [r1, #36]	@ movhi
.L900:
	add	sp, sp, #948
	add	sp, sp, #3072
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L921:
	.align	2
.L920:
	.word	.LANCHOR10
	.word	.LANCHOR7
	.word	.LANCHOR6
	.word	.LANCHOR8
	.word	.LANCHOR80
	.word	.LANCHOR81
	.word	.LANCHOR82
	.word	.LANCHOR83
	.word	.LANCHOR69
	.word	.LANCHOR16
	.word	.LANCHOR18
	.word	.LANCHOR17
	.word	.LANCHOR19
	.word	.LANCHOR84
.LFE71:
	.size	DrawText, .-DrawText
	.section	.text.DrawTextBlock,"ax",%progbits
	.align	2
	.type	DrawTextBlock, %function
DrawTextBlock:
.LFB72:
	@ args = 4, pretend = 0, frame = 4588
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI63:
	mov	r4, r3
	ldr	r3, .L979
	mov	r7, r1
	ldrh	r6, [r3, #10]
	ldr	r1, .L979+4
	ldrh	r9, [r3, #12]
	strh	r6, [r1, #0]	@ movhi
	ldr	r1, .L979+8
	ldrh	fp, [r3, #14]
	strh	r9, [r1, #0]	@ movhi
	ldr	r1, .L979+12
	ldrh	r8, [r3, #16]
	strh	fp, [r1, #0]	@ movhi
	ldr	r1, .L979+16
	sub	sp, sp, #4544
.LCFI64:
	strh	r8, [r1, #0]	@ movhi
	ldr	r1, .L979+20
	sub	sp, sp, #48
.LCFI65:
	ldrb	r1, [r1, #0]	@ zero_extendqisi2
	mov	sl, r0
	cmp	r1, #0
	add	r0, sp, #8192
	str	r2, [sp, #52]
	str	r9, [sp, #20]
	ldrb	r2, [r0, #-3564]	@ zero_extendqisi2
	str	fp, [sp, #24]
	beq	.L922
	cmp	r2, #0
	bne	.L924
	ldrsh	r3, [r3, #18]
	cmp	r3, #0
	bne	.L924
	mov	r0, r6, asl #16
	mov	r1, r9, asl #16
	mov	r2, fp, asl #16
	mov	r3, r8, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	bl	SetBackColorBox
	ldr	r3, .L979+24
	ldrsh	r0, [r3, #0]
	ldr	r3, .L979+28
	ldrsh	r1, [r3, #0]
	ldr	r3, .L979+32
	ldrsh	r2, [r3, #0]
	ldr	r3, .L979+36
	ldrsh	r3, [r3, #0]
	str	r4, [sp, #0]
	bl	GuiLib_FillBox
.L924:
	ldr	r3, .L979
	ldrb	r2, [r3, #87]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L925
	ldr	r2, .L979+40
	ldrb	r1, [r3, #86]	@ zero_extendqisi2
	ldr	r2, [r2, #0]
	ldrb	r2, [r2, #11]	@ zero_extendqisi2
	add	r2, r1, r2
	strb	r2, [r3, #86]
	mov	r2, #0
	strb	r2, [r3, #87]
.L925:
	ldr	r4, .L979
	mov	r3, #1
	cmp	r7, #0
	strh	r3, [r4, #92]	@ movhi
	beq	.L926
	ldr	ip, [sp, #24]
	mov	r0, sl
	add	r3, ip, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	str	r3, [sp, #44]
	rsb	r3, r6, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r1, r7
	str	r3, [sp, #48]
	str	r6, [sp, #40]
	bl	PrepareText
	mov	r1, r7
	add	r2, sp, #72
	ldrb	r0, [r4, #70]	@ zero_extendqisi2
	bl	TextPixelLength
	ldr	r2, .L979+44
	add	lr, sp, #4544
	mov	r3, #0
	add	lr, lr, #48
	strh	r3, [lr, r2]	@ movhi
	mvn	r1, #0
	add	r2, r2, #256
	strh	r1, [lr, r2]	@ movhi
	ldrb	r2, [r4, #88]	@ zero_extendqisi2
	cmp	r2, #255
	streq	r3, [sp, #28]
	beq	.L927
	ldrsh	r0, [r4, #90]
	ldrsb	r1, [r4, #86]
	bl	__divsi3
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	str	r0, [sp, #28]
.L927:
	ldr	r9, [sp, #28]
	sub	ip, r7, #1
	rsb	r5, r9, #1
	mov	r5, r5, asl #16
	cmp	r5, #0
	str	ip, [sp, #32]
	movle	r5, r5, lsr #16
	movgt	r5, #1
	mov	fp, #1
	mov	r4, #0
	mov	ip, r6
	mov	r9, r8
	b	.L929
.L931:
	add	r4, r4, #1
	mov	r4, r4, asl #16
	mov	r4, r4, lsr #16
.L944:
	ldr	lr, [sp, #32]
	rsb	r2, sl, r3
	cmp	r2, lr
	bge	.L930
	ldrb	r1, [r3], #1	@ zero_extendqisi2
	cmp	r1, #10
	beq	.L930
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	cmp	r2, #32
	beq	.L931
	cmp	r1, #32
	beq	.L930
	cmp	r2, #45
	bne	.L931
.L930:
	mov	r8, fp, asl #16
	mov	r8, r8, asr #16
	add	r2, sp, #4544
	sub	r0, r8, #1
	add	r2, r2, #48
	add	r2, r2, r0, asl #1
	sub	r3, r2, #516
	str	r0, [sp, #12]
	ldrh	r0, [r3, #0]
	add	r3, sp, #4544
	mov	r1, r4, asl #16
	add	r3, r3, #46
	str	r3, [sp, #0]
	mov	r6, r1, lsr #16
	add	r3, sp, #4544
	str	r1, [sp, #8]
	str	r2, [sp, #16]
	add	r3, r3, #44
	mov	r1, r6
	add	r2, sp, #72
	str	ip, [sp, #4]
	bl	CalcCharsWidth
	ldr	lr, [sp, #48]
	ldr	ip, [sp, #4]
	mov	r3, lr, asl #16
	cmp	r0, r3, asr #16
	ldr	r0, [sp, #16]
	sub	r3, r0, #260
	strleh	r4, [r3, #0]	@ movhi
	ble	.L940
	ldrsh	r0, [r3, #0]
	ldrh	r1, [r3, #0]
	cmn	r0, #1
	mov	r2, r8, asl #1
	bne	.L933
	add	r1, sp, #4544
	add	r1, r1, #48
	add	r2, r1, r2
	strh	r4, [r3, #0]	@ movhi
	add	r1, r6, #1
	sub	r3, r2, #516
	sub	r2, r2, #260
	strh	r1, [r3, #0]	@ movhi
	strh	r0, [r2, #0]	@ movhi
	b	.L934
.L933:
	add	r3, sp, #4544
	add	r3, r3, #48
	add	r0, r3, r2
	add	r1, r1, #1
	sub	r3, r0, #516
	strh	r1, [r3, #0]	@ movhi
	ldr	r1, [sp, #8]
	str	r2, [sp, #16]
	mov	lr, r1, asr #16
	b	.L935
.L937:
	add	r2, r2, #1
	strh	r2, [r1, #0]	@ movhi
.L935:
	sub	r1, r0, #516
	ldrh	r2, [r1, #0]
	mov	r3, r2, asl #16
	cmp	lr, r3, asr #16
	ble	.L936
	ldrb	r3, [sl, r3, asr #16]	@ zero_extendqisi2
	cmp	r3, #32
	beq	.L937
.L936:
	ldr	r2, [sp, #16]
	add	lr, sp, #4544
	add	lr, lr, #48
	add	r3, lr, r2
	sub	r3, r3, #260
	strh	r4, [r3, #0]	@ movhi
.L934:
	mov	r3, r5, asl #16
	cmp	r3, #8323072
	bgt	.L974
	add	r5, r5, #1
	mov	r3, r5, asl #16
	mov	r5, r3, lsr #16
	cmp	r3, #65536
	movgt	fp, r5
	bgt	.L939
	add	r0, sp, #4544
	add	r0, r0, #48
	add	r8, r0, r8, asl #1
	ldr	r1, [sp, #12]
	sub	r3, r8, #516
	ldrh	r2, [r3, #0]
	add	r3, r0, r1, asl #1
	sub	r1, r3, #516
	sub	r8, r8, #260
	strh	r2, [r1, #0]	@ movhi
	ldrh	r2, [r8, #0]
	sub	r3, r3, #260
	strh	r2, [r3, #0]	@ movhi
.L939:
	ldr	r3, .L979
	ldrh	r2, [r3, #92]
	add	r2, r2, #1
	strh	r2, [r3, #92]	@ movhi
.L940:
	mov	r3, r4, asl #16
	ldrb	r3, [sl, r3, asr #16]	@ zero_extendqisi2
	cmp	r3, #10
	bne	.L941
	mov	r2, fp, asl #16
	mov	r2, r2, asr #16
	add	r3, sp, #4544
	sub	r1, r2, #1
	add	r3, r3, #48
	add	lr, sp, #4544
	sub	r0, r6, #1
	add	r1, r3, r1, asl #1
	add	r6, r6, #1
	add	lr, lr, #48
	add	r2, lr, r2, asl #1
	sub	r3, r1, #260
	mov	r6, r6, asl #16
	strh	r0, [r3, #0]	@ movhi
	mov	r6, r6, lsr #16
	sub	r0, r2, #516
	strh	r6, [r0, #0]	@ movhi
	sub	r0, r2, #260
	mvn	r2, #0
	strh	r2, [r0, #0]	@ movhi
	mov	r0, r5, asl #16
	cmp	r0, #8323072
	bgt	.L974
	add	r5, r5, #1
	mov	r0, r5, asl #16
	cmp	r0, #65536
	strleh	r2, [r3, #0]	@ movhi
	ldr	r3, .L979
	mov	r5, r0, lsr #16
	ldrh	r2, [r3, #92]
	suble	r1, r1, #516
	add	r2, r2, #1
	movgt	fp, r5
	strleh	r6, [r1, #0]	@ movhi
	strh	r2, [r3, #92]	@ movhi
.L941:
	add	r4, r4, #1
	mov	r4, r4, asl #16
	mov	r4, r4, lsr #16
.L929:
	mov	r3, r4, asl #16
	mov	r3, r3, asr #16
	cmp	r3, r7
	addlt	r3, sl, r3
	blt	.L944
.L943:
	mov	r3, r5, asl #16
	cmp	r3, #0
	mov	r6, ip
	mov	r8, r9
	bgt	.L963
	b	.L926
.L974:
	mov	r6, ip
	mov	r8, r9
.L963:
	ldr	r3, .L979
	ldr	r3, [r3, #60]
	tst	r3, #2048
	beq	.L945
	mov	r3, r5, asl #16
	mov	r2, #0
	mov	r3, r3, asr #16
	str	r2, [sp, #12]
	str	r3, [sp, #36]
.L948:
	add	r3, sp, #4064
	add	r3, r3, #12
	ldrsh	lr, [r3, r2]
	add	r3, sp, #4288
	add	r3, r3, #44
	ldrsh	sl, [r3, r2]
	ldr	r9, .L979+48
	rsb	r3, lr, sl
	add	r3, r3, #1
	add	r4, r9, lr, asl #2
	ldr	r9, .L979+52
	add	r3, r3, r3, lsr #31
	add	r3, lr, r3, asr #1
	mov	fp, sl, asl #2
	str	r3, [sp, #16]
	add	sl, r9, sl
	mov	r3, #0
	mov	ip, r3
	mov	r0, r3
	mov	r1, r3
	str	fp, [sp, #32]
	str	sl, [sp, #8]
	str	r2, [sp, #56]
	b	.L946
.L947:
	ldr	r2, .L979+48
	ldr	fp, [sp, #32]
	ldr	r9, [r4, r0]
	add	sl, r2, fp
	ldr	fp, [sl, ip]
	ldr	r2, [sp, #8]
	str	fp, [r4, r0]
	str	r9, [sl, ip]
	ldr	r9, .L979+52
	ldrb	fp, [r2, r3]	@ zero_extendqisi2
	add	sl, r9, lr
	ldrb	r9, [sl, r1]	@ zero_extendqisi2
	add	r0, r0, #4
	strb	fp, [sl, r1]
	sub	ip, ip, #4
	strb	r9, [r2, r3]
	add	r1, r1, #1
	sub	r3, r3, #1
.L946:
	ldr	r9, [sp, #16]
	add	sl, r1, lr
	cmp	r9, sl
	bgt	.L947
	ldr	fp, [sp, #12]
	ldr	lr, [sp, #36]
	ldr	r2, [sp, #56]
	add	r3, fp, #1
	mov	r3, r3, asl #16
	mov	ip, r3, lsr #16
	cmp	lr, r3, asr #16
	add	r2, r2, #2
	str	ip, [sp, #12]
	bgt	.L948
	ldr	r3, .L979
	mov	r1, r7
	ldrb	r0, [r3, #70]	@ zero_extendqisi2
	add	r2, sp, #72
	bl	TextPixelLength
.L945:
	ldr	r4, .L979
	ldr	ip, [sp, #20]
	ldrh	r9, [r4, #50]
	ldrsh	r2, [r4, #50]
	str	r9, [sp, #56]
	ldrh	r9, [r4, #44]
	mov	r3, r8, asl #16
	ldrh	fp, [r4, #46]
	ldrsh	r1, [r4, #46]
	mov	r3, r3, asr #16
	cmp	r2, r3
	movlt	r3, r2
	movge	r3, r3
	str	r9, [sp, #16]
	mov	r2, ip, asl #16
	ldr	r9, [sp, #24]
	mov	r2, r2, asr #16
	cmp	r1, r2
	movlt	r1, r2
	str	fp, [sp, #32]
	ldrsh	r0, [r4, #44]
	ldrh	fp, [r4, #48]
	ldrsh	ip, [r4, #48]
	str	r1, [sp, #12]
	mov	r2, r9, asl #16
	mov	r1, r6, asl #16
	mov	r3, r3, asl #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	cmp	r0, r1
	movlt	r0, r1
	str	fp, [sp, #36]
	cmp	ip, r2
	movlt	r2, ip
	movge	r2, r2
	mov	fp, r3, lsr #16
	ldr	r1, [sp, #12]
	mov	r3, r3, asr #16
	str	fp, [sp, #8]
	bl	GuiLib_SetClipping
	ldr	r0, [r4, #60]
	tst	r0, #256
	beq	.L949
	ldrsb	ip, [r4, #79]
	cmp	ip, #0
	bgt	.L949
	mov	lr, #60
	mul	r1, lr, ip
	ldr	sl, .L979+56
	mov	r2, #1
	add	r3, sl, r1
	strb	r2, [r3, #4]
	ldrb	r2, [r4, #52]	@ zero_extendqisi2
	ldrb	fp, [r4, #81]	@ zero_extendqisi2
	strb	r2, [r3, #16]
	ldr	r2, .L979+40
	strb	fp, [r3, #9]
	ldr	r2, [r2, #0]
	ldrb	r9, [r4, #56]	@ zero_extendqisi2
	ldrb	fp, [r2, #10]	@ zero_extendqisi2
	strh	r7, [r3, #28]	@ movhi
	strb	fp, [r3, #6]
	ldrb	r7, [r4, #80]	@ zero_extendqisi2
	ldrb	fp, [r2, #11]	@ zero_extendqisi2
	strb	r9, [r3, #8]
	strb	r7, [r3, #10]
	strb	fp, [r3, #46]
	add	r7, r3, #16
	ldrb	r3, [r2, #20]	@ zero_extendqisi2
	cmp	r9, #11
	strb	r3, [r7, #1]
	ldrb	r3, [r2, #21]	@ zero_extendqisi2
	mla	ip, lr, ip, sl
	strb	r3, [r7, #2]
	ldreq	r3, [r4, #4]
	ldrne	r3, [r4, #0]
	ldr	r9, [sp, #24]
	str	r3, [sl, r1]
	ldr	r3, .L979
	strh	r9, [ip, #32]	@ movhi
	ldrb	r2, [r3, #86]	@ zero_extendqisi2
	ldr	fp, [sp, #20]
	strb	r2, [ip, #52]
	ldr	r9, [sp, #28]
	ldrb	r2, [r3, #70]	@ zero_extendqisi2
	strh	r6, [ip, #30]	@ movhi
	strh	fp, [ip, #34]	@ movhi
	strh	r8, [ip, #36]	@ movhi
	strh	r5, [ip, #48]	@ movhi
	strh	r9, [ip, #50]	@ movhi
	strb	r2, [ip, #7]
	ldrb	lr, [r3, #72]	@ zero_extendqisi2
	ldrb	r2, [r3, #64]	@ zero_extendqisi2
	strb	lr, [ip, #12]
	ldrb	lr, [r3, #73]	@ zero_extendqisi2
	strb	r2, [ip, #11]
	strb	lr, [ip, #13]
	ldrb	r2, [r3, #84]	@ zero_extendqisi2
	ldrb	lr, [r3, #74]	@ zero_extendqisi2
	strb	r2, [ip, #53]
	strb	lr, [ip, #14]
	ldrb	r2, [r3, #85]	@ zero_extendqisi2
	ldrb	lr, [r3, #75]	@ zero_extendqisi2
	ldrh	r3, [r3, #90]
	strb	lr, [ip, #15]
	str	r0, [ip, #24]
	strb	r2, [ip, #54]
	strh	r3, [ip, #56]	@ movhi
.L949:
	ldr	r3, .L979
	ldr	fp, [sp, #20]
	ldr	r9, [sp, #28]
	ldrsb	r0, [r3, #86]
	ldrh	ip, [r3, #90]
	mov	r1, fp, asl #16
	mov	r4, r9, asl #16
	mov	r2, r1, lsr #16
	mov	r0, r0, asl #16
	mov	r4, r4, asr #16
	mov	r0, r0, lsr #16
	rsb	ip, ip, r2
	bic	r4, r4, r4, asr #31
	mla	r4, r0, r4, ip
	ldr	ip, .L979+40
	mov	lr, r5, asl #16
	ldr	ip, [ip, #0]
	mov	lr, lr, asr #16
	ldrb	ip, [ip, #11]	@ zero_extendqisi2
	sub	lr, lr, #1
	mla	r0, lr, r0, ip
	ldrb	r3, [r3, #85]	@ zero_extendqisi2
	mov	r4, r4, asl #16
	mov	r0, r0, asl #16
	cmp	r3, #2
	mov	r4, r4, lsr #16
	mov	r0, r0, lsr #16
	beq	.L953
	cmp	r3, #3
	bne	.L955
	b	.L954
.L953:
	mov	r3, r8, asl #16
	mov	r1, r1, asr #16
	rsb	r1, r1, r3, asr #16
	add	r1, r1, #1
	mov	r0, r0, asl #16
	sub	r0, r1, r0, asr #16
	add	r0, r0, r0, lsr #31
	add	r4, r4, r0, asr #1
.L976:
	mov	r4, r4, asl #16
	mov	r4, r4, lsr #16
.L955:
	ldr	fp, [sp, #8]
	mov	r5, r5, asl #16
	mov	r3, fp, asl #16
	mov	r3, r3, asr #16
	mov	r5, r5, asr #16
	mov	r7, #0
	str	r3, [sp, #60]
	str	r5, [sp, #64]
	mov	r9, r8
	b	.L956
.L954:
	add	r3, r8, #1
	rsb	r2, r2, r3
	add	r4, r4, r2
	rsb	r4, r0, r4
	b	.L976
.L956:
	add	r2, sp, #4288
	mov	r3, r7, asl #1
	add	r2, r2, #44
	ldrh	fp, [r3, r2]
	add	r2, sp, #4064
	add	r2, r2, #12
	ldrh	sl, [r3, r2]
	mov	r1, fp, asl #16
	mov	r0, sl, asl #16
	cmp	r1, r0
	blt	.L957
	add	r3, sp, #4544
	add	r3, r3, #46
	str	r3, [sp, #0]
	add	r3, sp, #4544
	add	r3, r3, #44
	mov	r0, r0, lsr #16
	mov	r1, r1, lsr #16
	add	r2, sp, #72
	bl	CalcCharsWidth
	ldr	r3, .L979
	ldrb	r3, [r3, #84]	@ zero_extendqisi2
	cmp	r3, #2
	beq	.L959
	cmp	r3, #3
	movne	r5, r6
	bne	.L958
	b	.L978
.L959:
	ldr	ip, [sp, #48]
	mov	r3, r0, asl #16
	mov	r2, ip, asl #16
	mov	r3, r3, asr #16
	ldr	ip, [sp, #40]
	rsb	r3, r3, r2, asr #16
	add	r3, r3, r3, lsr #31
	add	r5, ip, r3, asr #1
	b	.L977
.L978:
	ldr	ip, [sp, #44]
	rsb	r5, r0, ip
.L977:
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
.L958:
	ldr	r3, .L979
	ldr	r3, [r3, #60]
	tst	r3, #4
	beq	.L961
	ldr	r2, .L979+40
	mov	ip, r5, asl #16
	ldr	lr, [r2, #0]
	sub	r2, r0, #1
	ldrb	r1, [lr, #17]	@ zero_extendqisi2
	ldrb	r0, [lr, #18]	@ zero_extendqisi2
	add	r1, r4, r1
	add	r3, r4, r0
	add	r2, r2, r5
	ldr	lr, [sp, #52]
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	mov	r0, ip, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	str	lr, [sp, #0]
	bl	GuiLib_FillBox
.L961:
	add	r0, sp, #4608
	ldrh	r3, [r0, #-20]
	mov	r8, r4, asl #16
	rsb	r5, r3, r5
	ldr	r3, .L979+40
	ldr	ip, [sp, #12]
	ldr	r3, [r3, #0]
	mov	r8, r8, asr #16
	ldrb	r3, [r3, #11]	@ zero_extendqisi2
	ldr	lr, [sp, #60]
	add	r3, r8, r3
	cmp	r3, ip
	movlt	r3, #0
	movge	r3, #1
	cmp	r8, lr
	mov	r5, r5, asl #16
	movgt	r3, #0
	mov	r5, r5, lsr #16
	cmp	r3, #0
	str	r5, [sp, #28]
	beq	.L957
	mov	r3, sl, asl #16
	ldr	r2, .L979+48
	mov	r3, r3, asr #16
	add	r0, sp, #72
	add	ip, r0, r3, asl #1
	mov	fp, fp, asl #16
	add	r2, r2, r3, asl #2
	str	r6, [sp, #68]
	mov	r5, #0
	mov	r6, r4
	mov	fp, fp, asr #16
	mov	r4, ip
	str	r2, [sp, #8]
.L962:
	ldrh	r0, [r4, r5]
	ldr	ip, [sp, #28]
	ldr	r3, [sp, #52]
	add	r0, ip, r0
	ldr	ip, [sp, #8]
	mov	r0, r0, asl #16
	ldr	r2, [ip, r5, asl #1]
	add	sl, sl, #1
	mov	r0, r0, asr #16
	mov	r1, r8
	bl	DrawChar.isra.1
	mov	r3, sl, asl #16
	cmp	fp, r3, asr #16
	add	r5, r5, #2
	mov	sl, r3, lsr #16
	bge	.L962
	mov	r4, r6
	ldr	r6, [sp, #68]
.L957:
	ldr	r3, .L979
	ldr	fp, [sp, #64]
	ldrsb	r3, [r3, #86]
	add	r7, r7, #1
	add	r4, r4, r3
	mov	r3, r7, asl #16
	mov	r4, r4, asl #16
	cmp	fp, r3, asr #16
	mov	r4, r4, lsr #16
	bgt	.L956
	ldr	ip, [sp, #16]
	mov	r8, r9
	mov	r0, ip, asl #16
	ldr	r9, [sp, #32]
	ldr	fp, [sp, #36]
	ldr	ip, [sp, #56]
	mov	r1, r9, asl #16
	mov	r2, fp, asl #16
	mov	r3, ip, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	bl	GuiLib_SetClipping
.L926:
	ldr	r9, [sp, #20]
	ldr	fp, [sp, #24]
	mov	r0, r6, asl #16
	mov	r1, r9, asl #16
	mov	r2, fp, asl #16
	mov	r3, r8, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	bl	GuiLib_MarkDisplayBoxRepaint
.L922:
	add	sp, sp, #496
	add	sp, sp, #4096
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L980:
	.align	2
.L979:
	.word	.LANCHOR10
	.word	.LANCHOR80
	.word	.LANCHOR81
	.word	.LANCHOR82
	.word	.LANCHOR83
	.word	.LANCHOR69
	.word	.LANCHOR16
	.word	.LANCHOR18
	.word	.LANCHOR17
	.word	.LANCHOR19
	.word	.LANCHOR8
	.word	-516
	.word	.LANCHOR7
	.word	.LANCHOR6
	.word	.LANCHOR84
.LFE72:
	.size	DrawTextBlock, .-DrawTextBlock
	.section	.text.DrawBackBox,"ax",%progbits
	.align	2
	.type	DrawBackBox, %function
DrawBackBox:
.LFB70:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI66:
	mov	r5, r1
	mov	r4, r0
	bl	SetBackBox
	cmp	r5, #0
	bne	.L981
.LBB188:
	ldr	r3, .L983
	ldrsh	r0, [r3, #0]
	ldr	r3, .L983+4
	ldrsh	r1, [r3, #0]
	ldr	r3, .L983+8
	ldrsh	r2, [r3, #0]
	ldr	r3, .L983+12
	ldrsh	r3, [r3, #0]
	str	r4, [sp, #0]
	bl	GuiLib_FillBox
.L981:
.LBE188:
	ldmfd	sp!, {r3, r4, r5, pc}
.L984:
	.align	2
.L983:
	.word	.LANCHOR16
	.word	.LANCHOR18
	.word	.LANCHOR17
	.word	.LANCHOR19
.LFE70:
	.size	DrawBackBox, .-DrawBackBox
	.section	.text.ScrollBox_ShowBarBlock,"ax",%progbits
	.align	2
	.type	ScrollBox_ShowBarBlock, %function
ScrollBox_ShowBarBlock:
.LFB128:
	@ args = 12, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI67:
	sub	sp, sp, #24
.LCFI68:
	mov	r9, r3
	ldrsh	r3, [sp, #68]
	subs	r4, r2, #0
	str	r0, [sp, #12]
	str	r1, [sp, #16]
	ldrsh	fp, [sp, #60]
	ldrsh	ip, [sp, #64]
	str	r3, [sp, #8]
	beq	.L986
	mov	r7, r9, asl #16
	mov	r8, ip, asl #16
	mov	r7, r7, lsr #16
	mov	r8, r8, lsr #16
	mov	sl, fp, asl #16
	add	r6, r8, r7
	mov	sl, sl, lsr #16
	mov	r6, r6, asl #16
	rsb	r3, r7, sl
	mov	r6, r6, lsr #16
	mov	r5, #0
	str	r3, [sp, #20]
	b	.L987
.L988:
	ldr	lr, [sp, #20]
	add	r0, r5, r7
	rsb	r3, r8, lr
	ldr	lr, [sp, #8]
	add	r1, r5, sl
	add	r3, r3, lr
	add	r3, r6, r3
	ldr	lr, [sp, #12]
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r2, r6, asl #16
	mov	r3, r3, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	str	ip, [sp, #4]
	str	lr, [sp, #0]
	add	r5, r5, #1
	bl	GuiLib_Box
	ldr	ip, [sp, #4]
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
.L987:
	sub	r6, r6, #1
	and	r3, r5, #255
	mov	r6, r6, asl #16
	cmp	r3, r4
	mov	r6, r6, lsr #16
	bcc	.L988
.L986:
	mov	r9, r9, asl #16
	mov	r9, r9, lsr #16
	add	r0, r9, r4
	rsb	r9, r4, r9
	sub	r9, r9, #1
	add	ip, r9, ip
	mov	r2, ip, asl #16
	mov	fp, fp, asl #16
	ldr	ip, [sp, #8]
	mov	fp, fp, lsr #16
	mvn	r3, r4
	add	r3, fp, r3
	add	r1, fp, r4
	add	r3, r3, ip
	ldr	ip, [sp, #16]
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r3, r3, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	str	ip, [sp, #60]
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	GuiLib_FillBox
.LFE128:
	.size	ScrollBox_ShowBarBlock, .-ScrollBox_ShowBarBlock
	.section	.text.DrawScrollIndicator,"ax",%progbits
	.align	2
	.type	DrawScrollIndicator, %function
DrawScrollIndicator:
.LFB93:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L996
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI69:
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L989
	ldr	r3, .L996+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L989
	ldr	r7, .L996+8
	ldr	r2, .L996+12
	ldrsh	r3, [r7, #0]
	ldrsh	r4, [r2, #0]
	cmp	r3, r4
	bge	.L989
	ldr	r3, .L996+16
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L989
.LBB191:
	ldr	r3, .L996+20
	ldrsh	r6, [r3, #0]
	ldrh	r2, [r3, #0]
	ldr	r3, .L996+24
	str	r2, [sp, #4]
	ldrh	r2, [r3, #0]
	ldr	r3, .L996+28
	mov	r0, r6
	ldrh	r1, [r3, #0]
	ldr	r3, .L996+32
	add	r1, r2, r1
	ldrsh	r5, [r3, #0]
	ldr	r3, .L996+36
	mov	r1, r1, asl #16
	ldrh	r3, [r3, #0]
	mov	r8, r1, lsr #16
	sub	r3, r3, #1
	rsb	r3, r2, r3
	ldr	r2, .L996+40
	add	r1, r1, #65536
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	mov	r3, r3, asl #16
	str	r2, [sp, #0]
	mov	r1, r1, asr #16
	mov	r2, r5
	mov	r3, r3, asr #16
	bl	GuiLib_FillBox
	ldr	r3, .L996+44
	ldrsh	r1, [r7, #0]
	ldrh	r0, [r3, #0]
	ldr	r3, .L996+48
	rsb	r1, r1, r4
	ldrh	r3, [r3, #0]
	smulbb	r0, r0, r3
	bl	__divsi3
	ldr	r3, .L996+52
	mov	r2, r5
	ldrh	r4, [r3, #0]
	ldr	r3, .L996+56
	mov	r5, #1
	ldrb	r9, [r3, #0]	@ zero_extendqisi2
	str	r9, [sp, #0]
	add	r8, r8, r0
	mov	r1, r8, asl #16
	mov	r7, r1, lsr #16
	add	r3, r7, r4
	mov	r3, r3, asl #16
	mov	r0, r6
	mov	r1, r1, asr #16
	mov	r3, r3, asr #16
	mov	r4, r4, asl #16
	bl	GuiLib_Box
	mov	r6, #2
	mov	r4, r4, asr #16
	b	.L991
.L994:
	ldr	r3, [sp, #4]
	mov	sl, r6
	add	fp, r5, r3
	mov	fp, fp, asl #16
	mov	r8, #1
	mov	fp, fp, asr #16
.L993:
	tst	sl, #1
	beq	.L992
	add	r1, r7, r8
	mov	r1, r1, asl #16
	mov	r0, fp
	mov	r1, r1, asr #16
	mov	r2, r9
	bl	GuiLib_Dot
.L992:
	add	r8, r8, #1
	mov	r3, r8, asl #16
	cmp	r4, r3, asr #16
	add	sl, sl, #1
	mov	r8, r3, lsr #16
	bgt	.L993
	add	r5, r5, #1
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
	add	r6, r6, #1
.L991:
	mov	r3, r5, asl #16
	cmp	r4, r3, asr #16
	bgt	.L994
	ldr	r3, .L996+60
	ldrh	r2, [r3, #0]
	ldr	r3, .L996+64
	strh	r2, [r3, #0]	@ movhi
	ldr	r3, .L996+68
	ldrh	r2, [r3, #0]
	ldr	r3, .L996+72
	strh	r2, [r3, #0]	@ movhi
.L989:
.LBE191:
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L997:
	.align	2
.L996:
	.word	.LANCHOR85
	.word	.LANCHOR86
	.word	.LANCHOR88
	.word	.LANCHOR87
	.word	.LANCHOR69
	.word	.LANCHOR89
	.word	.LANCHOR90
	.word	.LANCHOR91
	.word	.LANCHOR92
	.word	.LANCHOR93
	.word	.LANCHOR94
	.word	.LANCHOR95
	.word	.LANCHOR96
	.word	.LANCHOR97
	.word	.LANCHOR98
	.word	.LANCHOR99
	.word	.LANCHOR63
	.word	.LANCHOR100
	.word	.LANCHOR64
.LFE93:
	.size	DrawScrollIndicator, .-DrawScrollIndicator
	.section	.text.GuiLib_BorderBox,"ax",%progbits
	.align	2
	.global	GuiLib_BorderBox
	.type	GuiLib_BorderBox, %function
GuiLib_BorderBox:
.LFB53:
	@ args = 8, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI70:
	sub	sp, sp, #20
.LCFI71:
	ldr	ip, [sp, #32]
	strh	r0, [sp, #16]	@ movhi
	strh	r1, [sp, #12]	@ movhi
	strh	r2, [sp, #8]	@ movhi
	strh	r3, [sp, #4]	@ movhi
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	and	ip, ip, #255
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	str	ip, [sp, #0]
	ldr	r4, [sp, #36]
	bl	GuiLib_Box
	add	r1, sp, #8
	add	r0, sp, #16
	bl	OrderCoord
	add	r1, sp, #4
	add	r0, sp, #12
	bl	OrderCoord
	ldrsh	r1, [sp, #8]
	ldrsh	r3, [sp, #16]
	and	r4, r4, #255
	rsb	r3, r3, r1
	cmp	r3, #1
	ldrh	r2, [sp, #8]
	ldrh	r0, [sp, #16]
	ble	.L998
	ldrsh	r5, [sp, #4]
	ldrsh	ip, [sp, #12]
	ldrh	r3, [sp, #4]
	rsb	ip, ip, r5
	cmp	ip, #1
	ldrh	r1, [sp, #12]
	ble	.L998
	add	r0, r0, #1
	add	r1, r1, #1
	sub	r2, r2, #1
	sub	r3, r3, #1
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	str	r4, [sp, #0]
	bl	GuiLib_FillBox
.L998:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, pc}
.LFE53:
	.size	GuiLib_BorderBox, .-GuiLib_BorderBox
	.section	.text.GuiLib_ShowBitmap,"ax",%progbits
	.align	2
	.global	GuiLib_ShowBitmap
	.type	GuiLib_ShowBitmap, %function
GuiLib_ShowBitmap:
.LFB54:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L1001
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI72:
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, #0
	ldr	r0, [ip, r0, lsr #14]
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	str	r3, [sp, #12]
	bl	ShowBitmapArea.isra.3
	ldmfd	sp!, {r0, r1, r2, r3, pc}
.L1002:
	.align	2
.L1001:
	.word	GuiStruct_BitmapPtrList
.LFE54:
	.size	GuiLib_ShowBitmap, .-GuiLib_ShowBitmap
	.section	.text.DrawItem,"ax",%progbits
	.align	2
	.type	DrawItem, %function
DrawItem:
.LFB79:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1107
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI73:
	ldrb	r4, [r3, #0]	@ zero_extendqisi2
	sub	sp, sp, #72
.LCFI74:
	cmp	r4, #0
	str	r0, [sp, #24]
	bne	.L1004
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1004
	ldr	r3, .L1107+8
	ldrsh	r0, [r3, #44]
	ldrsh	r1, [r3, #46]
	ldrsh	r2, [r3, #48]
	ldrsh	r3, [r3, #50]
	bl	GuiLib_SetClipping
.L1004:
	ldr	r5, .L1107+8
	ldrb	r0, [r5, #52]	@ zero_extendqisi2
	bl	SetCurFont
	ldr	r3, .L1107+12
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1005
	ldrsb	r3, [r5, #76]
	cmp	r3, #0
	movlt	r3, #0
	blt	.L1005
	cmp	r3, #99
	movgt	r3, #0
	movle	r3, #1
.L1005:
	ands	r3, r3, #255
	str	r3, [sp, #40]
	ldreq	r3, [sp, #40]
	beq	.L1006
	ldr	r3, .L1107+16
	ldrsh	r2, [r3, #0]
	ldr	r3, .L1107+8
	ldrsb	r3, [r3, #76]
	rsb	ip, r3, r2
	rsbs	r3, ip, #0
	adc	r3, r3, ip
.L1006:
	ands	r3, r3, #255
	str	r3, [sp, #44]
	ldrne	r3, .L1107+20
	movne	r2, #1
	strneb	r2, [r3, #0]
	cmp	r4, #0
	beq	.L1008
	ldr	r3, .L1107+8
	ldrsb	r1, [r3, #76]
	cmp	r1, #0
	blt	.L1009
	ldr	r2, .L1107+24
	ldrsh	r0, [r2, #0]
	cmn	r0, #1
	streqh	r1, [r2, #0]	@ movhi
.L1009:
	ldr	r0, [sp, #40]
	cmp	r0, #0
	beq	.L1010
	ldrb	r1, [r3, #77]	@ zero_extendqisi2
	ldr	r2, .L1107+8
	add	r1, r1, #1
	strb	r1, [r3, #77]
	ldrb	r1, [r3, #76]	@ zero_extendqisi2
	strb	r1, [r3, #78]
	ldr	r3, .L1107+28
	ldr	r1, .L1107+8
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldrsb	r0, [r1, #76]
	cmp	r3, #0
	ldr	r3, [r2, #60]
	orrne	r3, r3, #16384
	strne	r3, [r2, #60]
	ldrne	r3, .L1107+32
	biceq	r3, r3, #16384
	ldrneb	r3, [r3, #0]	@ zero_extendqisi2
	streq	r3, [r2, #60]
	strneb	r3, [r2, #96]
	ldrne	r3, .L1107+36
	movne	r2, #1
	strneb	r2, [r3, #0]
	ldr	r3, .L1107+40
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	ldr	r3, .L1107+8
	ldr	r2, [r3, #60]
	orrne	r2, r2, #64
	strne	r2, [r3, #60]
	ldrne	r3, .L1107+36
	movne	r2, #1
	biceq	r2, r2, #64
	strneb	r2, [r3, #0]
	streq	r2, [r3, #60]
	ldr	r3, .L1107+44
	mov	r2, #100
	mla	r0, r2, r0, r3
	bl	memcpy
.L1010:
	ldr	r2, .L1107+8
	ldr	r3, [r2, #60]
	tst	r3, #32
	beq	.L1008
	ldr	r1, .L1107+48
	ldrh	r0, [r1, #0]
	ldrsh	r1, [r1, #0]
	cmp	r1, #127
	bgt	.L1008
	ldrb	r1, [r2, #77]	@ zero_extendqisi2
	mov	r0, r0, asl #16
	cmp	r1, #0
	mvneq	r1, #0
	streqb	r1, [r2, #78]
	ldr	r2, .L1107+40
	mov	r0, r0, asr #16
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	ldr	r1, .L1107+8
	cmp	r2, #0
	ldr	r2, .L1107+8
	orrne	r3, r3, #64
	biceq	r3, r3, #64
	str	r3, [r2, #60]
	ldr	r3, .L1107+52
	mov	r2, #100
	mla	r0, r2, r0, r3
	bl	memcpy
	ldr	r2, .L1107+48
	ldr	r1, .L1107+56
	ldrh	r3, [r2, #0]
	strh	r3, [r1, #0]	@ movhi
	ldr	r1, .L1107+60
	mov	r0, r3, asl #16
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldr	r1, .L1107+64
	add	r3, r3, #1
	strb	ip, [r1, r0, asr #16]
	strh	r3, [r2, #0]	@ movhi
.L1008:
	ldr	r1, [sp, #24]
	ldr	r3, .L1107+68
	cmp	r1, #1
	beq	.L1020
	movcc	r2, #0
	bcc	.L1094
	cmp	r1, #2
	bne	.L1018
	b	.L1106
.L1020:
	ldr	r2, [sp, #24]
	b	.L1094
.L1106:
	ldr	r0, [sp, #44]
	cmp	r0, #0
	beq	.L1018
	mov	r2, #1
.L1094:
	strb	r2, [r3, #0]
.L1018:
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	ldr	r3, .L1107+8
	beq	.L1022
	ldrb	r4, [r3, #67]	@ zero_extendqisi2
	ldrb	r5, [r3, #68]	@ zero_extendqisi2
	ldr	r3, [r3, #60]
	tst	r3, #32768
	b	.L1095
.L1022:
	ldrb	r4, [r3, #65]	@ zero_extendqisi2
	ldrb	r5, [r3, #66]	@ zero_extendqisi2
	ldr	r3, [r3, #60]
	tst	r3, #2
.L1095:
	ldr	r7, .L1107+8
	mov	r3, #0
	strb	r3, [r7, #34]
	ldrb	r3, [r7, #56]	@ zero_extendqisi2
	moveq	r6, #0
	movne	r6, #1
	cmp	r3, #19
	ldrls	pc, [pc, r3, asl #2]
	b	.L1024
.L1040:
	.word	.L1025
	.word	.L1026
	.word	.L1027
	.word	.L1028
	.word	.L1029
	.word	.L1030
	.word	.L1030
	.word	.L1031
	.word	.L1032
	.word	.L1024
	.word	.L1033
	.word	.L1034
	.word	.L1024
	.word	.L1035
	.word	.L1036
	.word	.L1024
	.word	.L1037
	.word	.L1038
	.word	.L1033
	.word	.L1039
.L1039:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldrh	r3, [r7, #10]
	add	r1, sp, #66
	strh	r3, [sp, #64]	@ movhi
	ldrh	r3, [r7, #14]
	add	r0, sp, #64
	strh	r3, [sp, #66]	@ movhi
	ldrh	r3, [r7, #12]
	strh	r3, [sp, #68]	@ movhi
	ldrh	r3, [r7, #16]
	strh	r3, [sp, #70]	@ movhi
	bl	OrderCoord
	add	r1, sp, #70
	add	r0, sp, #68
	bl	OrderCoord
	ldrsh	r0, [sp, #64]
	ldrsh	r1, [sp, #68]
	ldrsh	r2, [sp, #66]
	ldrsh	r3, [sp, #70]
	str	r5, [sp, #0]
	b	.L1104
.L1025:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldrsh	r3, [r7, #18]
	cmp	r3, #0
	ble	.L1041
	mov	r0, r5
	mov	r1, r6
	bl	DrawBackBox
.L1041:
	ldr	r3, .L1107+8
	ldrsb	r2, [r3, #80]
	ldrb	r1, [r3, #80]	@ zero_extendqisi2
	cmp	r2, #0
	ldr	r2, .L1107+72
.LBB207:
	movlt	r1, #0
	strb	r1, [r2, #0]
.LBE207:
	ldrh	r1, [r3, #54]
	str	r6, [sp, #0]
	ldr	r0, [r3, #4]
.L1105:
	mov	r2, r4
	mov	r3, r5
	bl	DrawText
.L1102:
	ldr	r3, .L1107+76
	ldrsh	r0, [r3, #0]
	ldr	r3, .L1107+80
	ldrsh	r1, [r3, #0]
	ldr	r3, .L1107+84
	ldrsh	r2, [r3, #0]
	ldr	r3, .L1107+88
	ldrsh	r3, [r3, #0]
.L1100:
	bl	UpdateDrawLimits
	b	.L1024
.L1034:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldrsb	r3, [r7, #80]
	ldrb	r2, [r7, #80]	@ zero_extendqisi2
	cmp	r3, #0
	ldr	r7, .L1107+8
	ldr	r3, .L1107+72
.LBB208:
	movlt	r2, #0
	strb	r2, [r3, #0]
.LBE208:
	ldrh	r1, [r7, #54]
	mov	r2, r4
	str	r6, [sp, #0]
	mov	r3, r5
	ldr	r0, [r7, #4]
	bl	DrawTextBlock
	ldrsh	r0, [r7, #10]
	ldrsh	r1, [r7, #12]
	ldrsh	r2, [r7, #14]
	ldrsh	r3, [r7, #16]
	b	.L1100
.L1032:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldrsh	r3, [r7, #18]
	cmp	r3, #0
	ble	.L1046
	mov	r0, r5
	mov	r1, r6
	bl	DrawBackBox
.L1046:
	ldr	r7, .L1107+8
	ldr	r8, [r7, #0]
	cmp	r8, #0
	beq	.L1102
	ldrb	r1, [r7, #81]	@ zero_extendqisi2
	mov	r0, r8
	cmp	r1, #9
	bne	.L1048
	bl	strlen
	mov	r0, r0, asl #16
	mov	r7, r0, lsr #16
	b	.L1049
.L1048:
	bl	ReadVar
	ldrb	r1, [r7, #81]	@ zero_extendqisi2
	bl	DataNumStr
	ldr	r8, .L1107+92
	mov	r7, r0
.L1049:
	ldr	r3, .L1107+8
	mov	r1, r8
	ldrb	r2, [r3, #80]	@ zero_extendqisi2
	ldrsb	r3, [r3, #80]
	ldr	r0, .L1107+96
	cmp	r3, #0
	ldr	r3, .L1107+72
.LBB209:
	movlt	r2, #0
	strb	r2, [r3, #0]
.LBE209:
	bl	strcpy
	str	r6, [sp, #0]
	ldr	r0, .L1107+96
	mov	r1, r7
	b	.L1105
.L1035:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldr	sl, [r7, #0]
	ldr	r8, .L1107+8
	cmp	sl, #0
	beq	.L1052
	ldrb	r1, [r8, #81]	@ zero_extendqisi2
	mov	r0, sl
	cmp	r1, #9
	bne	.L1053
	bl	strlen
	mov	r0, r0, asl #16
	mov	r7, r0, lsr #16
	b	.L1054
.L1053:
	bl	ReadVar
	ldrb	r1, [r8, #81]	@ zero_extendqisi2
	bl	DataNumStr
	ldr	sl, .L1107+92
	mov	r7, r0
.L1054:
	ldr	r3, .L1107+8
	mov	r1, sl
	ldrb	r2, [r3, #80]	@ zero_extendqisi2
	ldrsb	r3, [r3, #80]
	ldr	r0, .L1107+96
	cmp	r3, #0
	ldr	r3, .L1107+72
.LBB210:
	movlt	r2, #0
	strb	r2, [r3, #0]
.LBE210:
	bl	strcpy
	ldr	r0, .L1107+96
	mov	r1, r7
	mov	r2, r4
	mov	r3, r5
	str	r6, [sp, #0]
	bl	DrawTextBlock
.L1052:
	ldr	r3, .L1107+8
	ldrsh	r0, [r3, #10]
	ldrsh	r1, [r3, #12]
	ldrsh	r2, [r3, #14]
	ldrsh	r3, [r3, #16]
	b	.L1100
.L1026:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldrsh	r0, [r7, #10]
	ldrsh	r1, [r7, #12]
	mov	r2, r4
	bl	GuiLib_Dot
	ldrsh	r0, [r7, #10]
	ldrsh	r1, [r7, #12]
	mov	r2, r0
	mov	r3, r1
	b	.L1100
.L1027:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldrh	r0, [r7, #10]
	ldrh	r2, [r7, #14]
	ldrh	r1, [r7, #12]
	ldrh	r3, [r7, #16]
	ldr	ip, [r7, #60]
	strh	r0, [sp, #64]	@ movhi
	strh	r2, [sp, #66]	@ movhi
	strh	r1, [sp, #68]	@ movhi
	strh	r3, [sp, #70]	@ movhi
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	tst	ip, #4
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	beq	.L1057
	ldr	ip, .L1107+8
	ldrb	ip, [ip, #95]	@ zero_extendqisi2
	str	r4, [sp, #4]
	str	ip, [sp, #0]
	bl	GuiLib_LinePattern
	b	.L1103
.L1057:
	str	r4, [sp, #0]
	bl	GuiLib_Line
	b	.L1103
.L1028:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldrh	r3, [r7, #10]
	add	r1, sp, #66
	strh	r3, [sp, #64]	@ movhi
	ldrh	r3, [r7, #14]
	add	r0, sp, #64
	strh	r3, [sp, #66]	@ movhi
	ldrh	r3, [r7, #12]
	strh	r3, [sp, #68]	@ movhi
	ldrh	r3, [r7, #16]
	strh	r3, [sp, #70]	@ movhi
	bl	OrderCoord
	add	r1, sp, #70
	add	r0, sp, #68
	bl	OrderCoord
	ldrb	r7, [r7, #71]	@ zero_extendqisi2
	ldrh	r1, [sp, #64]
	ldrh	r2, [sp, #68]
	ldrh	r3, [sp, #66]
	ldrh	r0, [sp, #70]
.LBB211:
.LBB212:
	cmp	r7, #1
.LBE212:
.LBE211:
	str	r1, [sp, #20]
	ldrsh	sl, [sp, #64]
	str	r2, [sp, #28]
	ldrsh	fp, [sp, #68]
	str	r3, [sp, #32]
	ldrsh	r8, [sp, #66]
	str	r0, [sp, #36]
	ldrsh	r9, [sp, #70]
.LBB215:
.LBB213:
	bne	.L1059
	mov	r0, sl
	mov	r1, fp
	mov	r2, r8
	mov	r3, r9
	str	r4, [sp, #0]
	bl	GuiLib_Box
	b	.L1060
.L1059:
	ldr	r1, [sp, #20]
	mov	r0, sl
	sub	r2, r1, #1
	add	r2, r7, r2
	mov	r2, r2, asl #16
	mov	r1, fp
	mov	r2, r2, asr #16
	mov	r3, r9
	str	r4, [sp, #0]
	bl	GuiLib_FillBox
	ldr	r2, [sp, #32]
	mov	r1, fp
	add	r0, r2, #1
	rsb	r0, r7, r0
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	mov	r2, r8
	mov	r3, r9
	str	r4, [sp, #0]
	bl	GuiLib_FillBox
	ldr	r0, [sp, #28]
	mov	r1, fp
	sub	r3, r0, #1
	add	r3, r7, r3
	mov	r3, r3, asl #16
	mov	r0, sl
	mov	r2, r8
	mov	r3, r3, asr #16
	str	r4, [sp, #0]
	bl	GuiLib_FillBox
	ldr	r2, [sp, #36]
	mov	r0, sl
	add	r1, r2, #1
	rsb	r1, r7, r1
	mov	r1, r1, asl #16
	mov	r1, r1, asr #16
	mov	r2, r8
	mov	r3, r9
	str	r4, [sp, #0]
	bl	GuiLib_FillBox
.L1060:
	cmp	r6, #0
	bne	.L1103
	mov	r3, r7, asl #1
	rsb	r8, sl, r8
	cmp	r8, r3
	blt	.L1103
	rsb	r9, fp, r9
	cmp	r9, r3
	blt	.L1103
	ldr	r3, [sp, #20]
	ldr	r2, [sp, #28]
	add	r0, r7, r3
	ldr	r3, [sp, #32]
	add	r1, r7, r2
	rsb	r2, r7, r3
	ldr	r3, [sp, #36]
	mov	r0, r0, asl #16
	rsb	r7, r7, r3
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r7, asl #16
.LBE213:
.LBE215:
	str	r5, [sp, #0]
.LBB216:
.LBB214:
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	b	.L1104
.L1029:
.LBE214:
.LBE216:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldrh	r3, [r7, #10]
	add	r1, sp, #66
	strh	r3, [sp, #64]	@ movhi
	ldrh	r3, [r7, #14]
	add	r0, sp, #64
	strh	r3, [sp, #66]	@ movhi
	ldrh	r3, [r7, #12]
	strh	r3, [sp, #68]	@ movhi
	ldrh	r3, [r7, #16]
	strh	r3, [sp, #70]	@ movhi
	bl	OrderCoord
	add	r1, sp, #70
	add	r0, sp, #68
	bl	OrderCoord
	ldrsh	r0, [sp, #64]
	ldrsh	r1, [sp, #68]
	ldrsh	r2, [sp, #66]
	ldrsh	r3, [sp, #70]
	str	r4, [sp, #0]
.L1104:
	bl	GuiLib_FillBox
.L1103:
	ldrsh	r0, [sp, #64]
	ldrsh	r1, [sp, #68]
	ldrsh	r2, [sp, #66]
	ldrsh	r3, [sp, #70]
	b	.L1100
.L1037:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldrh	r2, [r7, #14]
	movs	r2, r2, asl #16
	bmi	.L1062
	ldr	r3, .L1107+8
	cmp	r6, #0
	ldrsh	r0, [r3, #10]
	ldrsh	r1, [r3, #12]
	movne	r5, #268435456
	mov	r2, r2, lsr #16
	mov	r3, r4
	str	r5, [sp, #0]
	bl	GuiLib_Circle
.L1062:
	ldr	r1, .L1107+8
	ldrh	r2, [r1, #10]
	ldrh	r3, [r1, #14]
	ldrh	ip, [r1, #12]
	rsb	r0, r3, r2
	rsb	r1, r3, ip
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	add	r2, r3, r2
	b	.L1101
.L1038:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldrh	r2, [r7, #14]
	ldr	r1, .L1107+8
	movs	r2, r2, asl #16
	bmi	.L1064
	ldrh	r3, [r1, #16]
	movs	r3, r3, asl #16
	bmi	.L1064
	cmp	r6, #0
	ldrsh	r0, [r1, #10]
	movne	r5, #268435456
	ldrsh	r1, [r1, #12]
	mov	r2, r2, lsr #16
	mov	r3, r3, lsr #16
	stmia	sp, {r4, r5}
	bl	GuiLib_Ellipse
.L1064:
	ldr	r1, .L1107+8
	ldrh	r2, [r1, #10]
	ldrh	lr, [r1, #14]
	ldrh	r3, [r1, #12]
	ldrh	ip, [r1, #16]
	rsb	r0, lr, r2
	rsb	r1, ip, r3
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	add	r2, lr, r2
.L1101:
	add	r3, ip, r3
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	b	.L1100
.L1033:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1066
	ldr	r3, .L1107+100
	ldr	r4, .L1107+8
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldrh	r0, [r4, #8]
	tst	r3, #2
	ldrneb	r3, [r4, #94]	@ zero_extendqisi2
	mvneq	r3, #0
	ldrsh	r1, [r4, #10]
	ldrsh	r2, [r4, #12]
	bl	GuiLib_ShowBitmap
	ldr	r3, .L1107+104
	ldrsh	r0, [r4, #10]
	ldrsh	r2, [r3, #0]
	ldr	r3, .L1107+108
	ldrsh	r1, [r4, #12]
	ldrsh	r3, [r3, #0]
	bl	UpdateDrawLimits
.L1066:
	ldr	r2, .L1107+8
	ldrb	r3, [r2, #56]	@ zero_extendqisi2
	cmp	r3, #18
	bne	.L1024
	ldr	r0, .L1107+112
	ldr	r1, .L1107+116
	ldrh	r3, [r0, #0]
	mov	ip, #1
	strb	ip, [r1, r3, asl #3]
	ldrh	ip, [r2, #8]
	add	r1, r1, r3, asl #3
	strh	ip, [r1, #2]	@ movhi
	ldrh	ip, [r2, #10]
	ldrh	r2, [r2, #12]
	cmp	r3, #8
	addls	r3, r3, #1
	strh	ip, [r1, #4]	@ movhi
	strh	r2, [r1, #6]	@ movhi
	strlsh	r3, [r0, #0]	@ movhi
	b	.L1024
.L1036:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldr	r2, [r7, #60]
	ldr	r3, .L1107+8
	tst	r2, #512
	beq	.L1068
	ldrh	r2, [r3, #10]
	ldr	r7, .L1107+120
	ldr	r6, .L1107+124
	strh	r2, [r7, #0]	@ movhi
	ldrh	r2, [r3, #12]
	ldr	r5, .L1107+128
	ldr	r4, .L1107+132
	strh	r2, [r6, #0]	@ movhi
	ldrh	r2, [r3, #14]
	ldrh	r3, [r3, #16]
	strh	r2, [r5, #0]	@ movhi
	strh	r3, [r4, #0]	@ movhi
	mov	r1, r5
	mov	r0, r7
	bl	OrderCoord
	mov	r1, r4
	mov	r0, r6
	bl	OrderCoord
	ldrsh	r0, [r7, #0]
	ldrsh	r1, [r6, #0]
	ldrsh	r2, [r5, #0]
	ldrsh	r3, [r4, #0]
	bl	GuiLib_SetClipping
.L1068:
	ldr	r3, .L1107+136
	ldr	r0, .L1107+140
	ldrh	ip, [r3, #0]
	ldr	r3, .L1107+144
	ldr	r2, .L1107+148
	ldrh	r1, [r3, #0]
	ldr	r3, .L1107+8
	strh	ip, [r0, #0]	@ movhi
	ldr	r4, [r3, #60]
	strh	r1, [r2, #0]	@ movhi
	tst	r4, #1024
	beq	.L1024
	ldrh	r4, [r3, #10]
	ldrh	r3, [r3, #12]
	add	ip, r4, ip
	add	r1, r3, r1
	strh	ip, [r0, #0]	@ movhi
	strh	r1, [r2, #0]	@ movhi
	b	.L1024
.L1031:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1024
	ldr	r3, [r7, #60]
	ldr	r4, .L1107+8
	tst	r3, #512
	beq	.L1069
	add	r1, r4, #48
	add	r0, r4, #44
	bl	OrderCoord
	add	r1, r4, #50
	add	r0, r4, #46
	bl	OrderCoord
	ldr	r3, .L1107+120
	ldrsh	r1, [r4, #44]
	ldrh	r2, [r3, #0]
	ldrsh	r3, [r3, #0]
	cmp	r1, r3
	strlth	r2, [r4, #44]	@ movhi
	ldr	r3, .L1107+124
	ldr	r2, .L1107+8
	ldrh	r1, [r3, #0]
	ldrsh	r0, [r2, #46]
	ldrsh	r3, [r3, #0]
	cmp	r0, r3
	ldr	r3, .L1107+128
	strlth	r1, [r2, #46]	@ movhi
	ldrsh	r1, [r2, #48]
	ldrh	r2, [r3, #0]
	ldrsh	r3, [r3, #0]
	cmp	r1, r3
	ldrgt	r3, .L1107+8
	strgth	r2, [r3, #48]	@ movhi
	ldr	r2, .L1107+132
	ldr	r3, .L1107+8
	ldrh	r1, [r2, #0]
	ldrsh	r0, [r3, #50]
	ldrsh	r2, [r2, #0]
	cmp	r0, r2
	strgth	r1, [r3, #50]	@ movhi
	b	.L1073
.L1069:
	ldr	r3, .L1107+120
	ldrh	r3, [r3, #0]
	strh	r3, [r4, #44]	@ movhi
	ldr	r3, .L1107+124
	ldrh	r3, [r3, #0]
	strh	r3, [r4, #46]	@ movhi
	ldr	r3, .L1107+128
	ldrh	r3, [r3, #0]
	strh	r3, [r4, #48]	@ movhi
	ldr	r3, .L1107+132
	ldrh	r3, [r3, #0]
	strh	r3, [r4, #50]	@ movhi
.L1073:
	ldr	r3, .L1107+8
	ldrsh	r0, [r3, #44]
	ldrsh	r1, [r3, #46]
	ldrsh	r2, [r3, #48]
	ldrsh	r3, [r3, #50]
	bl	GuiLib_SetClipping
	b	.L1024
.L1030:
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1074
	ldrsh	r3, [r7, #18]
	cmp	r3, #0
	ble	.L1074
	mov	r0, r5
	mov	r1, r6
	bl	DrawBackBox
.L1074:
	ldrb	r3, [r7, #56]	@ zero_extendqisi2
	ldr	r4, .L1107+8
	cmp	r3, #6
	ldrh	sl, [r7, #8]
	bne	.L1075
	ldr	r3, .L1107+152
	cmp	sl, r3
	beq	.L1024
	ldr	r0, [r4, #0]
	ldrb	r1, [r4, #81]	@ zero_extendqisi2
	bl	ReadVar
	ldrh	r3, [r4, #82]
	ldr	r2, .L1107+156
	b	.L1077
.L1078:
	mov	r1, sl, asl #1
	ldrh	r1, [r2, r1]
	cmp	r1, r0
	beq	.L1075
	add	sl, sl, #1
	sub	r3, r3, #1
	mov	sl, sl, asl #16
	mov	r3, r3, asl #16
	mov	sl, sl, lsr #16
	mov	r3, r3, lsr #16
.L1077:
	cmp	r3, #0
	bne	.L1078
	b	.L1024
.L1075:
	ldr	r3, .L1107+152
	cmp	sl, r3
	beq	.L1024
	ldr	r4, .L1107+8
	ldr	r3, .L1107+160
	ldrb	r2, [r4, #18]	@ zero_extendqisi2
	ldrh	r3, [r3, #0]
	str	r2, [sp, #28]
	ldr	r2, .L1107+164
	str	r3, [sp, #32]
	ldrh	r2, [r2, #0]
	ldr	r3, .L1107+168
	str	r2, [sp, #36]
	ldrh	r3, [r3, #0]
	ldr	r9, .L1107+172
	ldr	r2, .L1107+176
	str	r3, [sp, #48]
	ldrh	r2, [r2, #0]
	ldrb	r3, [r9, #0]	@ zero_extendqisi2
	ldr	r8, .L1107+180
	ldr	r7, .L1107+184
	ldr	r6, .L1107+188
	ldr	r5, .L1107+192
.LBB217:
	mov	r1, #0
	strb	r1, [r9, #0]
	mvn	r1, #32768
.LBE217:
	str	r2, [sp, #52]
	str	r3, [sp, #56]
	ldrh	r2, [r7, #0]
	ldrh	r3, [r8, #0]
.LBB218:
	strh	r1, [r7, #0]	@ movhi
	strh	r1, [r8, #0]	@ movhi
	mov	r1, #32768
.LBE218:
	ldrh	r0, [r5, #0]
	ldrh	ip, [r6, #0]
.LBB219:
	strh	r1, [r5, #0]	@ movhi
	strh	r1, [r6, #0]	@ movhi
.LBE219:
	ldr	r1, .L1107+56
	ldr	fp, .L1107+196
	ldrh	r1, [r1, #0]
	str	r0, [sp, #20]
	str	r1, [sp, #60]
	ldrh	r1, [fp, #0]
	add	r1, r1, #1
	strh	r1, [fp, #0]	@ movhi
	ldr	r1, .L1107+200
	str	r2, [sp, #12]
	ldr	r0, [r1, sl, asl #2]
	ldr	r1, [sp, #24]
	str	r3, [sp, #16]
	str	ip, [sp, #8]
	bl	DrawStructure
	ldrh	r1, [fp, #0]
	ldr	r2, [sp, #12]
	sub	r1, r1, #1
	strh	r1, [fp, #0]	@ movhi
	ldr	r1, [sp, #28]
	ldr	r3, [sp, #16]
	cmp	r1, #0
	ldr	ip, [sp, #8]
	beq	.L1079
	ldr	r0, [sp, #32]
	mov	r1, #1
	strb	r1, [r4, #34]
	strh	r0, [r4, #36]	@ movhi
	ldr	r1, [sp, #48]
	ldr	r0, [sp, #36]
	strh	r1, [r4, #38]	@ movhi
	strh	r0, [r4, #40]	@ movhi
	ldr	r1, [sp, #52]
	ldr	r0, .L1107+160
	strh	r1, [r4, #42]	@ movhi
	ldrsh	r1, [r0, #0]
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	cmp	r1, r3
	movlt	r3, r1
	movge	r3, r3
	ldr	r1, .L1107+168
	strh	r3, [r8, #0]	@ movhi
	ldrsh	r3, [r1, #0]
	mov	r2, r2, asl #16
	mov	r2, r2, asr #16
	cmp	r3, r2
	movge	r3, r2
	ldr	r2, .L1107+164
	strh	r3, [r7, #0]	@ movhi
	ldrsh	r3, [r2, #0]
	mov	ip, ip, asl #16
	mov	ip, ip, asr #16
	cmp	r3, ip
	movge	ip, r3
	movlt	ip, ip
	ldr	r3, .L1107+176
	ldr	r0, [sp, #20]
	ldrsh	r2, [r3, #0]
	mov	r3, r0, asl #16
	mov	r3, r3, asr #16
	cmp	r2, r3
	movge	r3, r2
	movlt	r3, r3
	strh	ip, [r6, #0]	@ movhi
	strh	r3, [r5, #0]	@ movhi
	b	.L1080
.L1079:
	ldrb	r1, [r9, #0]	@ zero_extendqisi2
	cmp	r1, #0
	beq	.L1081
	ldrh	sl, [r7, #0]
	ldrh	r9, [r8, #0]
	strh	sl, [r4, #38]	@ movhi
	mov	r2, r2, asl #16
	mov	sl, sl, asl #16
	mov	r1, #1
	mov	r2, r2, asr #16
	mov	sl, sl, asr #16
	ldrh	r0, [r6, #0]
	cmp	sl, r2
	movge	sl, r2
	strb	r1, [r4, #34]
	ldr	r2, [sp, #20]
	ldrh	r1, [r5, #0]
	strh	r9, [r4, #36]	@ movhi
	mov	r3, r3, asl #16
	mov	r9, r9, asl #16
	mov	r3, r3, asr #16
	mov	r9, r9, asr #16
	cmp	r9, r3
	movge	r9, r3
	strh	r0, [r4, #40]	@ movhi
	strh	r1, [r4, #42]	@ movhi
	mov	r0, r0, asl #16
	mov	ip, ip, asl #16
	mov	r1, r1, asl #16
	mov	r3, r2, asl #16
	mov	r0, r0, asr #16
	mov	ip, ip, asr #16
	mov	r1, r1, asr #16
	mov	r3, r3, asr #16
	cmp	r0, ip
	movlt	r0, ip
	cmp	r1, r3
	movlt	r1, r3
	strh	r9, [r8, #0]	@ movhi
	strh	sl, [r7, #0]	@ movhi
	strh	r0, [r6, #0]	@ movhi
	strh	r1, [r5, #0]	@ movhi
	b	.L1080
.L1081:
	ldr	r0, [sp, #56]
	strh	r2, [r7, #0]	@ movhi
	ldr	r2, [sp, #20]
	strb	r0, [r9, #0]
	strh	r3, [r8, #0]	@ movhi
	strh	ip, [r6, #0]	@ movhi
	strh	r2, [r5, #0]	@ movhi
.L1080:
	ldr	r3, .L1107+56
	ldr	r2, [sp, #60]
	strh	r2, [r3, #0]	@ movhi
.L1024:
	ldr	r3, .L1107
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1082
	ldr	r1, .L1107+8
	ldrb	r3, [r1, #56]	@ zero_extendqisi2
	cmp	r3, #11
	cmpne	r3, #13
	bne	.L1082
	ldrb	r3, [r1, #88]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L1082
	ldr	r0, .L1107+204
	mov	r2, #100
	mla	r0, r2, r3, r0
	bl	memcpy
.L1082:
	ldr	r3, .L1107
	ldr	r2, [sp, #40]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldr	r0, [sp, #24]
	cmp	r3, #0
	moveq	r2, #0
	andne	r2, r2, #1
	cmp	r2, #0
	ldrne	r2, .L1107+8
	ldrneb	r1, [r2, #77]	@ zero_extendqisi2
	subne	r1, r1, #1
	strneb	r1, [r2, #77]
	ldr	r2, [sp, #44]
	cmp	r0, #2
	movne	r2, #0
	andeq	r2, r2, #1
	cmp	r2, #0
	ldrne	r2, .L1107+68
	movne	r1, #0
	strneb	r1, [r2, #0]
	cmp	r3, #0
	bne	.L1003
	ldr	r3, .L1107+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1003
	bl	GuiLib_ResetClipping
.L1003:
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L1108:
	.align	2
.L1107:
	.word	.LANCHOR101
	.word	.LANCHOR69
	.word	.LANCHOR10
	.word	.LANCHOR102
	.word	.LANCHOR103
	.word	.LANCHOR104
	.word	.LANCHOR105
	.word	.LANCHOR39
	.word	.LANCHOR40
	.word	.LANCHOR106
	.word	.LANCHOR107
	.word	.LANCHOR108
	.word	.LANCHOR109
	.word	.LANCHOR110
	.word	.LANCHOR111
	.word	.LANCHOR113
	.word	.LANCHOR112
	.word	.LANCHOR114
	.word	.LANCHOR9
	.word	.LANCHOR80
	.word	.LANCHOR81
	.word	.LANCHOR82
	.word	.LANCHOR83
	.word	.LANCHOR57
	.word	.LANCHOR115
	.word	.LANCHOR29
	.word	.LANCHOR65
	.word	.LANCHOR66
	.word	.LANCHOR116
	.word	.LANCHOR117
	.word	.LANCHOR118
	.word	.LANCHOR119
	.word	.LANCHOR120
	.word	.LANCHOR121
	.word	.LANCHOR99
	.word	.LANCHOR63
	.word	.LANCHOR100
	.word	.LANCHOR64
	.word	65535
	.word	GuiStruct_StructNdxList
	.word	.LANCHOR16
	.word	.LANCHOR17
	.word	.LANCHOR18
	.word	.LANCHOR11
	.word	.LANCHOR19
	.word	.LANCHOR12
	.word	.LANCHOR13
	.word	.LANCHOR14
	.word	.LANCHOR15
	.word	.LANCHOR122
	.word	GuiStruct_StructPtrList
	.word	.LANCHOR56
.LFE79:
	.size	DrawItem, .-DrawItem
	.section	.text.DrawStructure,"ax",%progbits
	.align	2
	.type	DrawStructure, %function
DrawStructure:
.LFB81:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI75:
	ldr	r3, .L1182
	add	r5, sp, #16
	mov	sl, #0
	str	r0, [r5, #-4]!
	strh	sl, [r3, #0]	@ movhi
	mov	r0, r5
	str	r1, [sp, #8]
	bl	GetItemWord
	mov	r0, r0, asl #16
	mov	r4, r0, lsr #16
	mov	r0, r5
	bl	GetItemByte
	ldr	r5, .L1182+4
	str	r0, [sp, #4]
	b	.L1110
.L1168:
	ldr	r3, [sp, #12]
	ldr	r0, .L1182+8
	str	r3, [r0, #0]
	ldr	r3, .L1182+12
	ldrsh	r0, [r3, #0]
	bl	ReadItem
	ldr	r2, .L1182+8
	ldr	r3, [r2, #0]
	mvn	r2, #0
	str	r3, [sp, #12]
	ldr	r3, .L1182+16
	strh	r2, [r3, #0]	@ movhi
	ldr	r3, .L1182+20
	ldr	r2, [r3, #0]
	ldr	r3, .L1182+24
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L1111
	ldr	r3, .L1182+28
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	tst	r3, #16
	beq	.L1112
	ldr	r3, .L1182+32
	ldr	r0, [r3, r4, asl #2]
	add	r4, r4, #1
	mov	r4, r4, asl #16
	cmp	r0, #0
	mov	r4, r4, lsr #16
	beq	.L1112
	ldr	r3, .L1182+36
	ldr	r6, .L1182+40
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrh	r7, [r6, #0]
	bl	ReadVar
	add	r0, r7, r0
	strh	r0, [r6, #0]	@ movhi
.L1112:
	ldr	r3, .L1182+44
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1113
	ldr	r1, .L1182+48
	ldr	r3, .L1182+40
	add	r2, r1, r2, asl #1
	ldrh	r1, [r2, #-2]
	ldrh	r2, [r3, #0]
	add	r2, r1, r2
	strh	r2, [r3, #0]	@ movhi
.L1113:
	ldr	r3, .L1182+52
	ldr	r2, .L1182+56
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L1182+40
	cmp	r1, #1
	ldreqh	r1, [r2, #22]
	beq	.L1174
	cmp	r1, #2
	ldreqh	r1, [r2, #26]
	beq	.L1174
	cmp	r1, #3
	bne	.L1115
	ldrh	r1, [r2, #30]
.L1174:
	ldrh	r2, [r3, #0]
	add	r2, r1, r2
	strh	r2, [r3, #0]	@ movhi
.L1115:
	ldr	r3, .L1182+60
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	ldrne	r2, .L1182+48
	addne	r3, r2, r3, asl #1
	ldrne	r2, .L1182+40
	ldrneh	r2, [r2, #0]
	strneh	r2, [r3, #-2]	@ movhi
	ldr	r3, .L1182+40
	ldrh	r2, [r3, #0]
	ldr	r3, .L1182+56
	strh	r2, [r3, #10]	@ movhi
	ldr	r3, .L1182+28
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	tst	r3, #32
	beq	.L1118
	ldr	r3, .L1182+32
	ldr	r0, [r3, r4, asl #2]
	add	r4, r4, #1
	mov	r4, r4, asl #16
	cmp	r0, #0
	mov	r4, r4, lsr #16
	beq	.L1118
	ldr	r3, .L1182+64
	ldr	r6, .L1182+68
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrh	r7, [r6, #0]
	bl	ReadVar
	add	r0, r7, r0
	strh	r0, [r6, #0]	@ movhi
.L1118:
	ldr	r3, .L1182+72
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1119
	ldr	r1, .L1182+76
	ldr	r3, .L1182+68
	add	r2, r1, r2, asl #1
	ldrh	r1, [r2, #-2]
	ldrh	r2, [r3, #0]
	add	r2, r1, r2
	strh	r2, [r3, #0]	@ movhi
.L1119:
	ldr	r3, .L1182+80
	ldr	r2, .L1182+56
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L1182+68
	cmp	r1, #1
	ldreqh	r1, [r2, #24]
	beq	.L1175
	cmp	r1, #2
	ldreqh	r1, [r2, #28]
	beq	.L1175
	cmp	r1, #3
	bne	.L1121
	ldrh	r1, [r2, #32]
.L1175:
	ldrh	r2, [r3, #0]
	add	r2, r1, r2
	strh	r2, [r3, #0]	@ movhi
.L1121:
	ldr	r3, .L1182+84
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L1182+68
	cmp	r2, #0
	ldrne	r1, .L1182+76
	addne	r2, r1, r2, asl #1
	ldrneh	r1, [r3, #0]
	strneh	r1, [r2, #-2]	@ movhi
	ldrh	r2, [r3, #0]
	ldr	r3, .L1182+56
	ldrh	r1, [r3, #10]
	strh	r2, [r3, #12]	@ movhi
	strh	r1, [r3, #22]	@ movhi
	strh	r2, [r3, #24]	@ movhi
.L1111:
	ldr	r3, .L1182+20
	ldr	ip, [r3, #0]
	tst	ip, #32768
	beq	.L1124
	ldr	r3, .L1182+88
	ldr	lr, .L1182+56
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r2, #268
	ldrh	r0, [lr, #10]
	mul	r2, r3, r2
	add	r1, r5, r2
	strh	r0, [r5, r2]	@ movhi
	ldrh	r2, [lr, #12]
	ldrb	lr, [lr, #66]	@ zero_extendqisi2
	strh	r2, [r1, #2]	@ movhi
	strb	lr, [r1, #150]
	ldrb	lr, [r1, #169]	@ zero_extendqisi2
	add	r1, r1, #168
	and	lr, lr, #3
	cmp	lr, #1
	ldreqh	lr, [r1, #2]
	addeq	lr, r0, lr
	streqh	lr, [r1, #2]	@ movhi
	ldr	lr, .L1182+4
	mov	r1, #268
	mla	r1, r3, r1, lr
	ldrb	lr, [r1, #169]	@ zero_extendqisi2
	mov	lr, lr, lsr #2
	and	lr, lr, #3
	cmp	lr, #1
	ldreqh	lr, [r1, #172]
	addeq	lr, r2, lr
	streqh	lr, [r1, #172]	@ movhi
	mov	r1, #268
	mla	r1, r3, r1, r5
	ldrb	lr, [r1, #217]	@ zero_extendqisi2
	add	r1, r1, #216
	and	lr, lr, #3
	cmp	lr, #1
	ldreqh	lr, [r1, #2]
	addeq	r0, r0, lr
	streqh	r0, [r1, #2]	@ movhi
	ldr	r0, .L1182+4
	mov	r1, #268
	mla	r3, r1, r3, r0
	ldrb	r1, [r3, #217]	@ zero_extendqisi2
	mov	r1, r1, lsr #2
	and	r1, r1, #3
	cmp	r1, #1
	ldreqh	r1, [r3, #220]
	addeq	r2, r2, r1
	streqh	r2, [r3, #220]	@ movhi
.L1124:
	ldr	r3, .L1182+92
	and	r3, ip, r3
	cmp	r3, #0
	beq	.L1128
	ldr	r3, .L1182+28
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L1129
	ldr	r3, .L1182+32
	ldr	r0, [r3, r4, asl #2]
	add	r4, r4, #1
	mov	r4, r4, asl #16
	cmp	r0, #0
	mov	r4, r4, lsr #16
	beq	.L1129
	ldr	r3, .L1182+96
	ldr	r6, .L1182+100
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrh	r7, [r6, #0]
	bl	ReadVar
	add	r0, r7, r0
	strh	r0, [r6, #0]	@ movhi
.L1129:
	ldr	r3, .L1182+104
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1130
	ldr	r1, .L1182+48
	ldr	r3, .L1182+100
	add	r2, r1, r2, asl #1
	ldrh	r1, [r2, #-2]
	ldrh	r2, [r3, #0]
	add	r2, r1, r2
	strh	r2, [r3, #0]	@ movhi
.L1130:
	ldr	r3, .L1182+108
	ldr	r2, .L1182+56
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L1182+100
	cmp	r1, #1
	ldreqh	r1, [r2, #10]
	beq	.L1176
	cmp	r1, #2
	ldreqh	r1, [r2, #26]
	beq	.L1176
	cmp	r1, #3
	bne	.L1132
	ldrh	r1, [r2, #30]
.L1176:
	ldrh	r2, [r3, #0]
	add	r2, r1, r2
	strh	r2, [r3, #0]	@ movhi
.L1132:
	ldr	r3, .L1182+112
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	ldrne	r2, .L1182+48
	addne	r3, r2, r3, asl #1
	ldrne	r2, .L1182+100
	ldrneh	r2, [r2, #0]
	strneh	r2, [r3, #-2]	@ movhi
	ldr	r3, .L1182+100
	ldrh	r2, [r3, #0]
	ldr	r3, .L1182+56
	strh	r2, [r3, #14]	@ movhi
	ldr	r3, .L1182+20
	ldr	r3, [r3, #0]
	tst	r3, #65536
	bne	.L1128
	ldr	r3, .L1182+28
	ldrsb	r3, [r3, #0]
	cmp	r3, #0
	bge	.L1135
	ldr	r3, .L1182+32
	ldr	r0, [r3, r4, asl #2]
	add	r4, r4, #1
	mov	r4, r4, asl #16
	cmp	r0, #0
	mov	r4, r4, lsr #16
	beq	.L1135
	ldr	r3, .L1182+116
	ldr	r6, .L1182+120
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrh	r7, [r6, #0]
	bl	ReadVar
	add	r0, r7, r0
	strh	r0, [r6, #0]	@ movhi
.L1135:
	ldr	r3, .L1182+124
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1136
	ldr	r1, .L1182+76
	ldr	r3, .L1182+120
	add	r2, r1, r2, asl #1
	ldrh	r1, [r2, #-2]
	ldrh	r2, [r3, #0]
	add	r2, r1, r2
	strh	r2, [r3, #0]	@ movhi
.L1136:
	ldr	r3, .L1182+128
	ldr	r2, .L1182+56
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L1182+120
	cmp	r1, #1
	ldreqh	r1, [r2, #12]
	beq	.L1177
	cmp	r1, #2
	ldreqh	r1, [r2, #28]
	beq	.L1177
	cmp	r1, #3
	bne	.L1138
	ldrh	r1, [r2, #32]
.L1177:
	ldrh	r2, [r3, #0]
	add	r2, r1, r2
	strh	r2, [r3, #0]	@ movhi
.L1138:
	ldr	r3, .L1182+132
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	ldrne	r2, .L1182+76
	addne	r3, r2, r3, asl #1
	ldrne	r2, .L1182+120
	ldrneh	r2, [r2, #0]
	strneh	r2, [r3, #-2]	@ movhi
	ldr	r3, .L1182+120
	ldrh	r2, [r3, #0]
	ldr	r3, .L1182+56
	strh	r2, [r3, #16]	@ movhi
.L1128:
	ldr	r3, .L1182+20
	ldr	r2, [r3, #0]
	ldr	r3, .L1182+136
	and	r3, r2, r3
	cmp	r3, #0
	ldr	r3, .L1182+56
	beq	.L1141
	ldrh	r6, [r3, #14]
	ldrh	r1, [r3, #10]
	ldrb	lr, [r3, #64]	@ zero_extendqisi2
	add	ip, r6, #1
	rsb	ip, r1, ip
	mov	ip, ip, asl #16
	cmp	lr, #2
	mov	ip, ip, lsr #16
	bne	.L1142
	mov	r0, ip, asl #16
	mov	r0, r0, asr #16
	sub	r0, r0, #1
	add	r0, r0, r0, lsr #31
	mov	r0, r0, asr #1
	rsb	r0, r0, #0
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	add	r1, r0, r1
	add	r0, r0, r6
	strh	r1, [r3, #10]	@ movhi
	strh	r0, [r3, #14]	@ movhi
	b	.L1143
.L1142:
	cmp	lr, #3
	addeq	r0, r1, #1
	rsbeq	ip, ip, r0
	streqh	ip, [r3, #10]	@ movhi
	streqh	r1, [r3, #14]	@ movhi
	b	.L1143
.L1141:
	tst	r2, #196608
	beq	.L1143
	ldrb	r1, [r3, #64]	@ zero_extendqisi2
	cmp	r1, #1
	ldreqh	r0, [r3, #14]
	ldreqh	r1, [r3, #10]
	addeq	r1, r0, r1
	beq	.L1178
	cmp	r1, #3
	bne	.L1143
	ldrh	r0, [r3, #10]
	ldrh	r1, [r3, #14]
	rsb	r1, r1, r0
.L1178:
	strh	r1, [r3, #10]	@ movhi
.L1143:
	tst	r2, #8512
	beq	.L1145
	ldr	r3, .L1182+32
	ldr	r1, [r3, r4, asl #2]
	ldr	r3, .L1182+56
	add	r4, r4, #1
	mov	r4, r4, asl #16
	mov	r4, r4, lsr #16
	str	r1, [r3, #0]
.L1145:
	ldr	r3, .L1182+140
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L1146
	ldr	r3, .L1182+56
	ldr	r2, [r3, #60]
	tst	r2, #512
	beq	.L1146
	ldrh	r2, [r3, #10]
	strh	r2, [r3, #44]	@ movhi
	ldrh	r2, [r3, #12]
	strh	r2, [r3, #46]	@ movhi
	ldrh	r2, [r3, #14]
	strh	r2, [r3, #48]	@ movhi
	ldrh	r2, [r3, #16]
	strh	r2, [r3, #50]	@ movhi
.L1146:
	ldr	r0, .L1182+56
	ldr	lr, .L1182+144
	ldrb	r3, [r0, #71]	@ zero_extendqisi2
	ldrb	r1, [lr, #0]	@ zero_extendqisi2
	cmp	r3, #0
	moveq	r3, #1
	cmp	r1, #0
	bne	.L1148
	ldr	r2, .L1182+148
	ldrsb	r6, [r2, #0]
	cmp	r6, #1
	bne	.L1148
	ldr	r2, .L1182+20
	ldr	ip, .L1182+152
	ldr	r2, [r2, #0]
	and	ip, r2, ip
	cmp	ip, #0
	beq	.L1148
	strb	r6, [lr, #0]
	ldrh	fp, [r0, #10]
	ldrh	r6, [r0, #14]
	ldrh	r8, [r0, #12]
	ldrh	ip, [r0, #16]
	ldr	r9, .L1182+156
	ldr	r7, .L1182+160
	ldr	lr, .L1182+164
	ldr	r0, .L1182+168
	ands	r2, r2, #8
	strh	r6, [lr, #0]	@ movhi
	strh	fp, [r9, #0]	@ movhi
	strh	r8, [r7, #0]	@ movhi
	strh	ip, [r0, #0]	@ movhi
	moveq	r6, r2
	beq	.L1179
	rsb	r6, r3, r6
	add	fp, r3, fp
	add	r8, r3, r8
	rsb	r3, r3, ip
	strh	r6, [lr, #0]	@ movhi
	strh	fp, [r9, #0]	@ movhi
	strh	r8, [r7, #0]	@ movhi
	strh	r3, [r0, #0]	@ movhi
	mov	r6, r1
	b	.L1179
.L1148:
	ldr	r1, .L1182+172
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L1150
	ldr	r2, .L1182+148
	ldrsb	r2, [r2, #0]
	cmp	r2, #2
	bne	.L1150
	ldr	r2, .L1182+20
	ldr	r9, [r2, #0]
	ldr	r2, .L1182+152
	and	r2, r9, r2
	cmp	r2, #0
	beq	.L1150
	mov	r2, #1
	strb	r2, [r1, #0]
	ldr	r2, .L1182+56
	ldr	r7, .L1182+176
	ldrh	r8, [r2, #10]
	ldrh	r6, [r2, #12]
	ldrh	ip, [r2, #14]
	ldrh	r1, [r2, #16]
	ldr	lr, .L1182+180
	ldr	r0, .L1182+184
	ldr	r2, .L1182+188
	tst	r9, #8
	strh	r8, [r7, #0]	@ movhi
	strh	r6, [lr, #0]	@ movhi
	strh	ip, [r0, #0]	@ movhi
	strh	r1, [r2, #0]	@ movhi
	beq	.L1151
	add	r8, r3, r8
	add	r6, r3, r6
	rsb	ip, r3, ip
	rsb	r3, r3, r1
	strh	r8, [r7, #0]	@ movhi
	strh	r6, [lr, #0]	@ movhi
	strh	ip, [r0, #0]	@ movhi
	strh	r3, [r2, #0]	@ movhi
.L1151:
	ldr	r3, .L1182+184
	ldr	r1, .L1182+192
	ldrh	r2, [r3, #0]
	ldr	r3, .L1182+176
	mov	r6, #0
	ldrh	r3, [r3, #0]
	mov	r7, #1
	rsb	r3, r3, r2
	ldr	r2, .L1182+196
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [r2, #0]	@ movhi
	mov	r2, r3, asl #16
	mov	r2, r2, asr #16
	add	r2, r2, r2, lsr #31
	mov	r2, r2, asl #15
	mov	r2, r2, lsr #16
	add	r0, r2, #2
	strh	r0, [r1, #0]	@ movhi
	ldr	r1, .L1182+188
	ldr	r0, .L1182+180
	ldrh	r1, [r1, #0]
	ldrh	r0, [r0, #0]
	sub	r1, r1, #4
	rsb	r1, r0, r1
	rsb	r3, r3, r1
	sub	r2, r3, r2, asl #1
	ldr	r1, .L1182+200
	ldr	r3, .L1182+56
	strh	r2, [r1, #0]	@ movhi
	ldrb	r1, [r3, #65]	@ zero_extendqisi2
	ldr	r2, .L1182+204
	strb	r1, [r2, #0]
	ldrb	r2, [r3, #66]	@ zero_extendqisi2
	ldr	r3, .L1182+208
	strb	r2, [r3, #0]
	b	.L1149
.L1150:
	ldr	r3, .L1182+212
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	cmp	r7, #0
	movne	r6, #0
	bne	.L1179
	ldr	r2, .L1182+148
	ldrsb	r2, [r2, #0]
	cmp	r2, #3
	movne	r6, r7
	bne	.L1179
	mov	r6, #1
	strb	r6, [r3, #0]
	ldr	r3, .L1182+216
	ldr	r2, .L1182+220
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L1182+224
	strb	r6, [r2, #0]
	strh	r1, [r3, #0]	@ movhi
	ldr	r3, .L1182+168
	ldrsh	r0, [r3, #0]
	ldr	r3, .L1182+160
	ldrsh	r3, [r3, #0]
	rsb	r0, r3, r0
	add	r0, r0, r6
	bl	__divsi3
	ldr	r3, .L1182+228
	ldr	r1, .L1182+56
	mov	r2, #100
	strh	r0, [r3, #0]	@ movhi
	ldr	r0, .L1182+232
	bl	memcpy
	b	.L1149
.L1179:
	mov	r7, r6
.L1149:
	ldr	r3, .L1182+20
	ldr	r3, [r3, #0]
	tst	r3, #32768
	beq	.L1152
	ldr	r3, .L1182+88
	mov	r0, #268
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldr	r1, .L1182+56
	mla	r0, r3, r0, r5
	mov	r2, #100
	add	r0, r0, #24
	bl	memcpy
.L1152:
	ldr	r3, .L1182+220
	ldr	r8, .L1182+236
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldr	r0, [sp, #8]
	rsbs	r3, r3, #1
	movcc	r3, #0
	strb	r3, [r8, #0]
	ldr	r3, .L1182+240
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	movne	r3, #0
	strneb	r3, [r8, #0]
	bl	DrawItem
	cmp	r6, #0
	mov	r3, #1
	strb	r3, [r8, #0]
	ldrne	r3, .L1182+220
	movne	r2, #0
	strneb	r2, [r3, #0]
	cmp	r7, #0
	bne	.L1173
	b	.L1156
.L1157:
	ldrh	r1, [r9, #0]
	mov	r3, r3, lsr #16
	add	r0, r1, #1
	add	r1, r1, r2
	ldrh	r2, [ip, #0]
	sub	r1, r1, #1
	add	r2, lr, r2
	add	r0, r3, r0
	rsb	r1, r3, r1
	rsb	r2, r3, r2
	ldr	r3, .L1182+56
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	add	r6, r6, #1
	ldrb	r3, [r3, #65]	@ zero_extendqisi2
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r6, r6, asl #16
	str	ip, [sp, #0]
	mov	r6, r6, lsr #16
	bl	GuiLib_HLine
	ldr	ip, [sp, #0]
	b	.L1155
.L1173:
	ldr	r7, .L1182+196
	ldr	r9, .L1182+176
	ldr	ip, .L1182+180
	mov	r6, #0
.L1155:
	ldrsh	r1, [r7, #0]
	mov	r3, r6, asl #16
	add	r1, r1, r1, lsr #31
	mov	r1, r1, asl #15
	mov	lr, r1, lsr #16
	mov	r1, r1, asr #16
	cmp	r1, r3, asr #16
	ldr	fp, .L1182+196
	ldrh	r2, [r7, #0]
	ldr	r8, .L1182+176
	bgt	.L1157
	ldr	r3, .L1182+180
	ldr	r6, .L1182+184
	ldrh	r2, [r3, #0]
	ldr	r7, .L1182+56
	add	r2, r2, #2
	add	lr, r2, lr
	ldr	r9, .L1182+188
	mov	r2, lr, asl #16
	ldrsh	r1, [r6, #0]
	ldrb	r3, [r7, #65]	@ zero_extendqisi2
	ldrsh	r0, [r8, #0]
	mov	r2, r2, asr #16
	bl	GuiLib_HLine
	ldrh	r2, [r9, #0]
	ldrsh	r3, [fp, #0]
	sub	r2, r2, #2
	add	r3, r3, r3, lsr #31
	sub	r2, r2, r3, asr #1
	mov	r2, r2, asl #16
	ldrsh	r1, [r6, #0]
	ldrb	r3, [r7, #65]	@ zero_extendqisi2
	ldrsh	r0, [r8, #0]
	mov	r2, r2, asr #16
	bl	GuiLib_HLine
	mov	r6, #0
	mov	r7, fp
	b	.L1158
.L1159:
	ldrh	ip, [r8, #0]
	mov	r2, r2, lsr #16
	add	r0, ip, #1
	add	r1, ip, r1
	ldrh	ip, [r9, #0]
	sub	r1, r1, #1
	rsb	r3, r3, ip
	add	r0, r2, r0
	rsb	r1, r2, r1
	add	r2, r2, r3
	ldr	r3, .L1182+56
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	ldrb	r3, [r3, #65]	@ zero_extendqisi2
	bl	GuiLib_HLine
	add	r6, r6, #1
	mov	r6, r6, asl #16
	mov	r6, r6, lsr #16
.L1158:
	ldrsh	r3, [r7, #0]
	mov	r2, r6, asl #16
	add	r3, r3, r3, lsr #31
	mov	r3, r3, asr #1
	cmp	r3, r2, asr #16
	ldrh	r1, [r7, #0]
	bgt	.L1159
.L1156:
	ldr	r3, .L1182+56
	ldrb	r2, [r3, #56]	@ zero_extendqisi2
	cmp	r2, #19
	ldrls	pc, [pc, r2, asl #2]
	b	.L1160
.L1166:
	.word	.L1161
	.word	.L1162
	.word	.L1163
	.word	.L1163
	.word	.L1163
	.word	.L1160
	.word	.L1160
	.word	.L1163
	.word	.L1161
	.word	.L1160
	.word	.L1163
	.word	.L1163
	.word	.L1163
	.word	.L1163
	.word	.L1163
	.word	.L1160
	.word	.L1164
	.word	.L1165
	.word	.L1163
	.word	.L1163
.L1161:
	ldr	r2, .L1182+244
	ldrh	r2, [r2, #0]
	strh	r2, [r3, #26]	@ movhi
	ldr	r2, .L1182+248
	ldrh	r2, [r2, #0]
	strh	r2, [r3, #28]	@ movhi
	ldr	r2, .L1182+252
	ldrh	r2, [r2, #0]
	add	r2, r2, #1
	strh	r2, [r3, #30]	@ movhi
	ldr	r2, .L1182+256
	ldrh	r2, [r2, #0]
	b	.L1180
.L1162:
	ldrh	r1, [r3, #10]
	ldrh	r2, [r3, #12]
	strh	r1, [r3, #26]	@ movhi
	add	r1, r1, #1
	strh	r2, [r3, #28]	@ movhi
	strh	r1, [r3, #30]	@ movhi
	b	.L1180
.L1164:
	ldrh	r2, [r3, #14]
	ldrh	r0, [r3, #10]
	rsb	r1, r2, r0
	strh	r1, [r3, #26]	@ movhi
	ldrh	r1, [r3, #12]
	add	r0, r0, r2
	rsb	ip, r2, r1
	strh	ip, [r3, #28]	@ movhi
	b	.L1181
.L1165:
	ldrh	ip, [r3, #10]
	ldrh	r0, [r3, #14]
	ldrh	r1, [r3, #16]
	rsb	r2, r0, ip
	strh	r2, [r3, #26]	@ movhi
	ldrh	r2, [r3, #12]
	add	r0, ip, r0
	rsb	r6, r1, r2
	strh	r6, [r3, #28]	@ movhi
.L1181:
	add	r0, r0, #1
	strh	r0, [r3, #30]	@ movhi
	add	r2, r1, r2
	b	.L1180
.L1163:
	ldrh	r2, [r3, #10]
	strh	r2, [r3, #26]	@ movhi
	ldrh	r2, [r3, #12]
	strh	r2, [r3, #28]	@ movhi
	ldrh	r2, [r3, #14]
	add	r2, r2, #1
	strh	r2, [r3, #30]	@ movhi
	ldrh	r2, [r3, #16]
.L1180:
	strh	r2, [r3, #32]	@ movhi
.L1160:
	ldr	r3, .L1182+16
	ldrsh	r2, [r3, #0]
	cmp	r2, #0
	blt	.L1167
	ldr	r3, .L1182+260
	mov	r1, #100
	mla	r3, r1, r2, r3
	ldr	r2, .L1182+56
	ldrb	r1, [r2, #34]	@ zero_extendqisi2
	strb	r1, [r3, #34]
	ldrh	r1, [r2, #36]
	strh	r1, [r3, #36]	@ movhi
	ldrh	r1, [r2, #38]
	strh	r1, [r3, #38]	@ movhi
	ldrh	r1, [r2, #40]
	ldrh	r2, [r2, #42]
	strh	r1, [r3, #40]	@ movhi
	strh	r2, [r3, #42]	@ movhi
.L1167:
	add	sl, sl, #1
.L1110:
	ldr	r2, [sp, #4]
	cmp	sl, r2
	blt	.L1168
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L1183:
	.align	2
.L1182:
	.word	.LANCHOR21
	.word	.LANCHOR41
	.word	.LANCHOR22
	.word	.LANCHOR123
	.word	.LANCHOR111
	.word	.LANCHOR30
	.word	537918975
	.word	.LANCHOR25
	.word	GuiStruct_ItemPtrList
	.word	.LANCHOR50
	.word	.LANCHOR46
	.word	.LANCHOR31
	.word	.LANCHOR124
	.word	.LANCHOR42
	.word	.LANCHOR10
	.word	.LANCHOR32
	.word	.LANCHOR51
	.word	.LANCHOR47
	.word	.LANCHOR33
	.word	.LANCHOR125
	.word	.LANCHOR43
	.word	.LANCHOR34
	.word	.LANCHOR40
	.word	537885852
	.word	.LANCHOR52
	.word	.LANCHOR48
	.word	.LANCHOR35
	.word	.LANCHOR44
	.word	.LANCHOR36
	.word	.LANCHOR53
	.word	.LANCHOR49
	.word	.LANCHOR37
	.word	.LANCHOR45
	.word	.LANCHOR38
	.word	537689244
	.word	16512
	.word	.LANCHOR85
	.word	.LANCHOR54
	.word	460024
	.word	.LANCHOR126
	.word	.LANCHOR127
	.word	.LANCHOR128
	.word	.LANCHOR129
	.word	.LANCHOR86
	.word	.LANCHOR89
	.word	.LANCHOR91
	.word	.LANCHOR92
	.word	.LANCHOR93
	.word	.LANCHOR90
	.word	.LANCHOR97
	.word	.LANCHOR96
	.word	.LANCHOR98
	.word	.LANCHOR94
	.word	.LANCHOR130
	.word	.LANCHOR55
	.word	.LANCHOR107
	.word	.LANCHOR67
	.word	.LANCHOR88
	.word	.LANCHOR131
	.word	.LANCHOR69
	.word	.LANCHOR39
	.word	.LANCHOR80
	.word	.LANCHOR81
	.word	.LANCHOR82
	.word	.LANCHOR83
	.word	.LANCHOR110
.LFE81:
	.size	DrawStructure, .-DrawStructure
	.section	.text.ScrollBox_ShowLineStruct,"ax",%progbits
	.align	2
	.type	ScrollBox_ShowLineStruct, %function
ScrollBox_ShowLineStruct:
.LFB126:
	@ args = 4, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI76:
	ldrb	r6, [sp, #48]	@ zero_extendqisi2
	mov	sl, r3
	ldr	r3, .L1188
	mov	r4, r1
	cmp	r1, r3
	str	r2, [sp, #8]
	beq	.L1184
.LBB222:
	ldr	r3, .L1188+4
	ldr	r5, .L1188+8
	ldrh	r2, [r3, #0]
	mov	r7, #268
	mul	r7, r0, r7
	add	r2, r2, #1
	ldr	r8, .L1188+12
	strh	r2, [r3, #0]	@ movhi
	ldrh	ip, [r5, #50]
	ldrh	r3, [r5, #48]
	add	r1, r7, #24
	mov	r2, #100
	mov	r0, r5
	add	r1, r8, r1
	ldrh	r9, [r5, #44]
	ldrh	fp, [r5, #46]
	str	r3, [sp, #4]
	str	ip, [sp, #0]
	bl	memcpy
	ldr	r3, [sp, #4]
	ldr	r2, [sp, #8]
	strh	r3, [r5, #48]	@ movhi
	ldr	ip, [sp, #0]
	ldr	r3, .L1188+16
	cmp	r6, #1
	strh	r2, [r5, #22]	@ movhi
	strh	sl, [r5, #24]	@ movhi
	strh	r9, [r5, #44]	@ movhi
	strh	fp, [r5, #46]	@ movhi
	strh	ip, [r5, #50]	@ movhi
	ldr	r0, [r3, r4, asl #2]
	bne	.L1186
	add	r7, r8, r7
	ldrb	r6, [r7, #264]	@ zero_extendqisi2
	cmp	r6, #0
	moveq	r6, #1
	movne	r6, #2
.L1186:
	mov	r1, r6
	bl	DrawStructure
	ldr	r3, .L1188+4
	ldrh	r2, [r3, #0]
	sub	r2, r2, #1
	strh	r2, [r3, #0]	@ movhi
.L1184:
.LBE222:
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L1189:
	.align	2
.L1188:
	.word	65535
	.word	.LANCHOR122
	.word	.LANCHOR10
	.word	.LANCHOR41
	.word	GuiStruct_StructPtrList
.LFE126:
	.size	ScrollBox_ShowLineStruct, .-ScrollBox_ShowLineStruct
	.section	.text.ScrollBox_DrawScrollLine,"ax",%progbits
	.align	2
	.type	ScrollBox_DrawScrollLine, %function
ScrollBox_DrawScrollLine:
.LFB130:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI77:
	mov	r4, r0
	mov	sl, r1
	mov	r1, #268
	ldr	r0, .L1229
	mul	r1, r4, r1
	sub	sp, sp, #36
.LCFI78:
	add	r3, r0, r1
	ldrsh	r2, [r3, #12]
	cmp	sl, r2
	blt	.L1190
	ldrh	r5, [r3, #128]
	add	r5, r2, r5
	cmp	sl, r5
	bge	.L1190
.LBB235:
.LBB236:
	ldrh	r9, [r3, #132]
	ldrh	r1, [r0, r1]
	ldrh	fp, [r3, #134]
	add	r9, r9, r1
	ldrh	r1, [r3, #2]
	ldrh	r7, [r3, #136]
	add	r1, fp, r1
	ldrh	r8, [r3, #138]
	ldrh	fp, [r3, #130]
	ldr	r3, .L1229+4
	rsb	r2, r2, sl
	ldrh	ip, [r3, #46]
	mla	fp, r2, fp, r1
	str	ip, [sp, #12]
	ldrh	ip, [r3, #48]
	mov	r9, r9, asl #16
	str	ip, [sp, #20]
	ldrh	ip, [r3, #50]
	mov	r9, r9, lsr #16
	mov	fp, fp, asl #16
	ldrsh	r2, [r3, #44]
	mov	fp, fp, lsr #16
	sub	r7, r7, #1
	sub	r8, r8, #1
	str	ip, [sp, #24]
	mov	r1, r9, asl #16
	ldrh	ip, [r3, #44]
	add	r7, r9, r7
	add	r8, fp, r8
	mov	r1, r1, asr #16
	mov	r7, r7, asl #16
	mov	r8, r8, asl #16
	cmp	r2, r1
	mov	r6, r9
	mov	r5, fp
	mov	r7, r7, lsr #16
	mov	r8, r8, lsr #16
	str	ip, [sp, #16]
	blt	.L1220
.L1192:
	mov	r0, r7, asl #16
	mov	r0, r0, asr #16
	cmp	r0, r1
	bge	.L1194
	cmp	r2, r1
	ble	.L1193
.L1220:
	strh	r6, [r3, #44]	@ movhi
	b	.L1193
.L1194:
	cmp	r2, r0
	strgth	r7, [r3, #44]	@ movhi
.L1193:
	ldr	ip, [sp, #12]
	mov	r1, r5, asl #16
	mov	r2, ip, asl #16
	mov	r2, r2, asr #16
	mov	r1, r1, asr #16
	cmp	r2, r1
	ldr	r3, .L1229+4
	blt	.L1221
.L1195:
	mov	r0, r8, asl #16
	mov	r0, r0, asr #16
	cmp	r0, r1
	bge	.L1197
	cmp	r2, r1
	ble	.L1196
.L1221:
	strh	r5, [r3, #46]	@ movhi
	b	.L1196
.L1197:
	cmp	r2, r0
	strgth	r8, [r3, #46]	@ movhi
.L1196:
	ldr	r3, .L1229+4
	mov	r1, r6, asl #16
	ldrsh	r2, [r3, #48]
	mov	r1, r1, asr #16
	cmp	r2, r1
	blt	.L1222
.L1198:
	mov	r0, r7, asl #16
	mov	r0, r0, asr #16
	cmp	r0, r1
	bge	.L1200
	cmp	r2, r1
	ble	.L1199
.L1222:
	strh	r6, [r3, #48]	@ movhi
	b	.L1199
.L1200:
	cmp	r2, r0
	strgth	r7, [r3, #48]	@ movhi
.L1199:
	ldrsh	r3, [r3, #50]
	mov	r1, r5, asl #16
	mov	r1, r1, asr #16
	cmp	r3, r1
	ldr	r2, .L1229+4
	blt	.L1223
.L1201:
	mov	r0, r8, asl #16
	mov	r0, r0, asr #16
	cmp	r0, r1
	bge	.L1203
	cmp	r3, r1
	ble	.L1202
.L1223:
	strh	r5, [r2, #50]	@ movhi
	b	.L1202
.L1203:
	cmp	r3, r0
	strgth	r8, [r2, #50]	@ movhi
.L1202:
	ldr	r3, .L1229+4
	ldrsh	r2, [r3, #48]
	ldrsh	r0, [r3, #44]
	ldrsh	r1, [r3, #46]
	ldrsh	r3, [r3, #50]
	bl	GuiLib_SetClipping
	cmp	sl, #0
	ldr	r2, .L1229
	blt	.L1204
	mov	r3, #268
	mla	r3, r4, r3, r2
	ldrh	r1, [r3, #20]
	cmp	sl, r1
	bge	.L1204
	ldr	r2, [r3, #8]
	mov	r0, sl
	str	r3, [sp, #4]
	blx	r2
	ldr	r3, [sp, #4]
	mov	ip, #0
	ldrh	r2, [r3, #144]
	str	r6, [sp, #28]
	add	r2, r9, r2
	ldrh	r9, [r3, #146]
	mov	r2, r2, asl #16
	add	r9, fp, r9
	mov	r9, r9, asl #16
	mov	fp, r3
	mov	r6, r5
	mov	r3, r2, lsr #16
	mov	r9, r9, lsr #16
	mov	r2, r2, asr #16
	mov	r5, ip
	str	r3, [sp, #8]
	str	r2, [sp, #32]
.L1218:
	cmp	r5, #0
	bne	.L1205
	ldrsh	r3, [fp, #164]
	cmp	r3, #0
	blt	.L1206
	ldrsh	r2, [fp, #166]
	cmp	r2, #0
	ble	.L1206
	cmp	sl, r3
	blt	.L1206
	add	r3, r3, r2
	cmp	r3, sl
	ble	.L1206
	ldrb	r3, [fp, #162]	@ zero_extendqisi2
	mov	r5, r6
	ldr	r6, [sp, #28]
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L1207
.L1212:
	.word	.L1208
	.word	.L1209
	.word	.L1210
	.word	.L1211
.L1208:
	ldr	r3, .L1229
	ldr	r2, [sp, #8]
	mov	sl, #268
	mla	sl, r4, sl, r3
	mov	ip, r2, asl #16
	mov	r9, r9, asl #16
	mov	ip, ip, asr #16
	mov	fp, r9, asr #16
	ldrh	r1, [sl, #142]
	mov	r2, ip
	mov	r3, fp
	mov	r9, #1
	mov	r0, r4
	str	ip, [sp, #4]
	str	r9, [sp, #0]
	bl	ScrollBox_ShowLineStruct
	ldrh	r1, [sl, #160]
	ldr	r3, .L1229+8
	ldr	ip, [sp, #4]
	cmp	r1, r3
	ldreqb	ip, [sl, #157]	@ zero_extendqisi2
.LBB237:
	ldreqb	r3, [sl, #158]	@ zero_extendqisi2
.LBE237:
	addeq	sl, sl, #156
	beq	.L1227
	str	r9, [sp, #0]
	mov	r0, r4
	mov	r2, ip
	mov	r3, fp
	b	.L1225
.L1209:
	ldr	r3, .L1229
	mov	r2, #268
	mla	r3, r2, r4, r3
	ldr	r2, .L1229+8
	ldrh	r1, [r3, #160]
	cmp	r1, r2
	bne	.L1214
	ldrb	ip, [r3, #157]	@ zero_extendqisi2
	add	r3, r3, #156
.LBB238:
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L1210
.LBE238:
	mov	r0, r6, asl #16
	mov	r1, r5, asl #16
	mov	r2, r7, asl #16
	mov	r3, r8, asl #16
.LBB239:
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
.LBE239:
	str	ip, [sp, #0]
.LBB240:
	bl	GuiLib_FillBox
	b	.L1210
.L1214:
.LBE240:
	ldr	ip, [sp, #8]
	mov	r0, #1
	mov	r2, ip, asl #16
	mov	r3, r9, asl #16
	str	r0, [sp, #0]
	mov	r2, r2, asr #16
	mov	r0, r4
	mov	r3, r3, asr #16
	bl	ScrollBox_ShowLineStruct
.L1210:
	ldr	r3, .L1229
	mov	r2, #268
	mla	r3, r2, r4, r3
	ldrh	r1, [r3, #142]
	b	.L1216
.L1211:
	ldr	r3, .L1229
	mov	r2, #268
	mla	r3, r2, r4, r3
	ldr	r2, .L1229+8
	ldrh	r1, [r3, #160]
	cmp	r1, r2
	ldreqb	ip, [r3, #157]	@ zero_extendqisi2
	addeq	r3, r3, #156
.LBB241:
	ldreqb	r3, [r3, #2]	@ zero_extendqisi2
.LBE241:
	beq	.L1227
.L1216:
	ldr	ip, [sp, #8]
	mov	r0, #1
	mov	r2, ip, asl #16
	mov	r3, r9, asl #16
	str	r0, [sp, #0]
	mov	r2, r2, asr #16
	mov	r0, r4
	mov	r3, r3, asr #16
.L1225:
	bl	ScrollBox_ShowLineStruct
	b	.L1207
.L1205:
.LBB242:
	ldrb	r3, [fp, #151]	@ zero_extendqisi2
.LBE242:
	ldrb	ip, [fp, #149]	@ zero_extendqisi2
.LBB243:
	cmp	r3, #0
	bne	.L1217
.LBE243:
	ldr	r2, [sp, #28]
	mov	r1, r6, asl #16
	mov	r0, r2, asl #16
	mov	r3, r8, asl #16
	mov	r2, r7, asl #16
.LBB244:
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
.LBE244:
	str	ip, [sp, #0]
.LBB245:
	bl	GuiLib_FillBox
.L1217:
.LBE245:
	mov	r2, #0
	mov	r3, r9, asl #16
	ldrh	r1, [fp, #142]
	mov	r0, r4
	str	r2, [sp, #0]
	mov	r3, r3, asr #16
	ldr	r2, [sp, #32]
	bl	ScrollBox_ShowLineStruct
.L1206:
	add	r5, r5, #1
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
	cmp	r5, #2
	beq	.L1207
	b	.L1218
.L1204:
	mov	r3, #268
	mla	r2, r3, r4, r2
	ldrb	ip, [r2, #149]	@ zero_extendqisi2
.LBB246:
	ldrb	r3, [r2, #151]	@ zero_extendqisi2
.L1227:
	cmp	r3, #0
	bne	.L1207
.LBE246:
	mov	r0, r6, asl #16
	mov	r1, r5, asl #16
	mov	r2, r7, asl #16
	mov	r3, r8, asl #16
.LBB247:
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
.LBE247:
	str	ip, [sp, #0]
.LBB248:
	bl	GuiLib_FillBox
.L1207:
.LBE248:
	ldr	r3, .L1229+4
	ldr	ip, [sp, #16]
	strh	ip, [r3, #44]	@ movhi
	ldr	ip, [sp, #12]
	strh	ip, [r3, #46]	@ movhi
	ldr	ip, [sp, #20]
	strh	ip, [r3, #48]	@ movhi
	ldr	ip, [sp, #24]
	strh	ip, [r3, #50]	@ movhi
	ldr	ip, [sp, #16]
	mov	r0, ip, asl #16
	ldr	ip, [sp, #12]
	mov	r0, r0, asr #16
	mov	r1, ip, asl #16
	ldr	ip, [sp, #20]
	mov	r1, r1, asr #16
	mov	r2, ip, asl #16
	ldr	ip, [sp, #24]
	mov	r2, r2, asr #16
	mov	r3, ip, asl #16
	mov	r3, r3, asr #16
.LBE236:
.LBE235:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LBB250:
.LBB249:
	b	GuiLib_SetClipping
.L1190:
.LBE249:
.LBE250:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L1230:
	.align	2
.L1229:
	.word	.LANCHOR41
	.word	.LANCHOR10
	.word	65535
.LFE130:
	.size	ScrollBox_DrawScrollLine, .-ScrollBox_DrawScrollLine
	.section	.text.TextBox_Scroll_To,"ax",%progbits
	.align	2
	.type	TextBox_Scroll_To, %function
TextBox_Scroll_To:
.LFB147:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r4, lr}
.LCFI79:
	bne	.L1238
	ldr	ip, .L1242
	ldrb	r4, [ip, #88]	@ zero_extendqisi2
	cmp	r4, #255
	ldmeqfd	sp!, {r4, pc}
.LBB253:
	cmp	r2, #0
	ldrnesb	r0, [ip, #86]
	mulne	r1, r0, r1
	movne	r1, r1, asl #16
	movne	r1, r1, asr #16
	cmp	r3, #1
	beq	.L1236
	bcc	.L1235
	cmp	r3, #2
	bne	.L1234
	b	.L1241
.L1235:
	ldr	r3, .L1242
	ldrh	r2, [r3, #90]
	add	r1, r1, r2
	b	.L1240
.L1236:
	ldr	r3, .L1242
.L1240:
	strh	r1, [r3, #90]	@ movhi
	b	.L1234
.L1241:
	mov	r0, #0
	mov	r1, r2
	bl	TextBox_Scroll_CalcEndPos
	ldr	r3, .L1242
	strh	r0, [r3, #90]	@ movhi
.L1234:
	ldr	r1, .L1242
	mov	r2, #100
	ldr	r0, .L1242+4
	bl	memcpy
	ldr	r3, .L1242+8
	mov	r0, #0
	strh	r0, [r3, #0]	@ movhi
	ldr	r3, .L1242+12
	strb	r0, [r3, #0]
	bl	DrawItem
	mov	r0, #1
	ldmfd	sp!, {r4, pc}
.L1238:
.LBE253:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L1243:
	.align	2
.L1242:
	.word	.LANCHOR56
	.word	.LANCHOR10
	.word	.LANCHOR122
	.word	.LANCHOR114
.LFE147:
	.size	TextBox_Scroll_To, .-TextBox_Scroll_To
	.section	.text.DrawScrollLine,"ax",%progbits
	.align	2
	.type	DrawScrollLine, %function
DrawScrollLine:
.LFB96:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1249
	stmfd	sp!, {r4, r5, lr}
.LCFI80:
	ldr	r3, [r3, #0]
	mov	r5, r0
	cmp	r3, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r2, .L1249+4
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	cmp	r2, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r2, .L1249+8
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	cmp	r2, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r2, .L1249+12
	ldrsh	r2, [r2, #0]
	cmp	r2, #0
	ldmlefd	sp!, {r4, r5, pc}
.LBB256:
	ldr	r2, .L1249+16
	ldr	r4, .L1249+20
	ldrh	r0, [r2, #0]
	add	r0, r5, r0
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	blx	r3
	ldr	r1, .L1249+24
	mov	r2, #100
	mov	r0, r4
	bl	memcpy
	mov	r0, r5
	bl	ScrollLineAdjustY.constprop.22
	ldr	r3, .L1249+28
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1246
	ldr	r3, .L1249+32
	ldrh	r3, [r3, #0]
	strh	r3, [r4, #44]	@ movhi
	ldr	r3, .L1249+36
	ldrh	r3, [r3, #0]
	strh	r3, [r4, #46]	@ movhi
	ldr	r3, .L1249+40
	ldrh	r3, [r3, #0]
	strh	r3, [r4, #48]	@ movhi
	ldr	r3, .L1249+44
	ldrh	r3, [r3, #0]
	strh	r3, [r4, #50]	@ movhi
.L1246:
	ldr	r3, .L1249+48
	mov	r0, #0
	strb	r0, [r3, #0]
	ldr	r3, .L1249+16
	ldrsh	r3, [r3, #0]
	add	r5, r3, r5
	ldr	r3, .L1249+52
	ldrsh	r3, [r3, #0]
	cmp	r5, r3
	bne	.L1247
	ldr	r3, .L1249+56
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, r0
	movne	r0, #2
	moveq	r0, #1
.L1247:
.LBE256:
	ldmfd	sp!, {r4, r5, lr}
.LBB257:
	b	DrawItem
.L1250:
	.align	2
.L1249:
	.word	.LANCHOR132
	.word	.LANCHOR85
	.word	.LANCHOR130
	.word	.LANCHOR87
	.word	.LANCHOR95
	.word	.LANCHOR10
	.word	.LANCHOR131
	.word	.LANCHOR69
	.word	.LANCHOR126
	.word	.LANCHOR127
	.word	.LANCHOR128
	.word	.LANCHOR129
	.word	.LANCHOR114
	.word	.LANCHOR133
	.word	.LANCHOR106
.LBE257:
.LFE96:
	.size	DrawScrollLine, .-DrawScrollLine
	.section	.text.DrawScrollList,"ax",%progbits
	.align	2
	.type	DrawScrollList, %function
DrawScrollList:
.LFB97:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1255
	stmfd	sp!, {r4, r5, lr}
.LCFI81:
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r3, .L1255+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r3, .L1255+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	ldmlefd	sp!, {r4, r5, pc}
	ldr	r2, .L1255+12
.LBB260:
	mov	r4, #0
.LBE260:
	ldrsh	r5, [r2, #0]
.LBB261:
	cmp	r5, r3
	movge	r5, r3
	b	.L1253
.L1254:
	mov	r0, r4
	bl	DrawScrollLine
	add	r4, r4, #1
.L1253:
	mov	r3, r4, asl #16
	cmp	r5, r3, asr #16
	bgt	.L1254
	ldmfd	sp!, {r4, r5, pc}
.L1256:
	.align	2
.L1255:
	.word	.LANCHOR85
	.word	.LANCHOR130
	.word	.LANCHOR87
	.word	.LANCHOR88
.LBE261:
.LFE97:
	.size	DrawScrollList, .-DrawScrollList
	.section	.text.GuiLib_ShowBitmapAt,"ax",%progbits
	.align	2
	.global	GuiLib_ShowBitmapAt
	.type	GuiLib_ShowBitmapAt, %function
GuiLib_ShowBitmapAt:
.LFB55:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI82:
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, #0
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	str	r3, [sp, #12]
	bl	ShowBitmapArea.isra.3
	ldmfd	sp!, {r0, r1, r2, r3, pc}
.LFE55:
	.size	GuiLib_ShowBitmapAt, .-GuiLib_ShowBitmapAt
	.section	.text.GuiLib_ShowBitmapArea,"ax",%progbits
	.align	2
	.global	GuiLib_ShowBitmapArea
	.type	GuiLib_ShowBitmapArea, %function
GuiLib_ShowBitmapArea:
.LFB56:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI83:
	ldrsh	ip, [sp, #20]
	mov	r0, r0, asl #16
	str	ip, [sp, #20]
	mov	ip, #1
	str	ip, [sp, #24]
	ldr	ip, .L1259
	ldrsh	r5, [sp, #12]
	ldrsh	r4, [sp, #16]
	ldr	r0, [ip, r0, lsr #14]
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	str	r5, [sp, #12]
	str	r4, [sp, #16]
	ldmfd	sp!, {r4, r5, lr}
	b	ShowBitmapArea.isra.3
.L1260:
	.align	2
.L1259:
	.word	GuiStruct_BitmapPtrList
.LFE56:
	.size	GuiLib_ShowBitmapArea, .-GuiLib_ShowBitmapArea
	.section	.text.UpdateBackgroundBitmap,"ax",%progbits
	.align	2
	.type	UpdateBackgroundBitmap, %function
UpdateBackgroundBitmap:
.LFB78:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, sl, lr}
.LCFI84:
	ldr	r5, .L1274
	ldr	r4, .L1274+4
	ldr	r7, .L1274+8
	ldr	r6, .L1274+12
	add	r8, r5, #80
.L1268:
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1262
	ldrb	r3, [r4, #56]	@ zero_extendqisi2
	cmp	r3, #13
	bhi	.L1262
	ldrsb	r3, [r4, #56]
	mov	r2, #1
	mov	r3, r2, asl r3
	tst	r3, #10240
	bne	.L1265
	add	r2, r2, #256
	and	r2, r3, r2
	cmp	r2, #0
	bne	.L1263
	tst	r3, #96
	bne	.L1264
	b	.L1262
.L1263:
	ldrsh	r3, [r4, #18]
	cmp	r3, #0
	beq	.L1271
.L1266:
	ldrb	r0, [r4, #52]	@ zero_extendqisi2
	bl	SetCurFont
	bl	SetBackBox
	b	.L1272
.L1264:
	ldrb	r3, [r4, #34]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1262
.L1271:
	ldrh	r3, [r4, #36]
	ldrh	sl, [r4, #38]
	ldrh	lr, [r4, #40]
	ldrh	ip, [r4, #42]
	b	.L1267
.L1265:
	ldr	r3, [r4, #60]
	tst	r3, #2
	beq	.L1262
	ldrsh	r0, [r4, #10]
	ldrsh	r1, [r4, #12]
	ldrsh	r2, [r4, #14]
	ldrsh	r3, [r4, #16]
	bl	SetBackColorBox
.L1272:
	ldr	r2, .L1274+16
	ldrh	r3, [r7, #0]
	ldrh	lr, [r2, #0]
	ldr	r2, .L1274+20
	ldrh	sl, [r6, #0]
	ldrh	ip, [r2, #0]
	b	.L1267
.L1262:
	add	r5, r5, #8
	cmp	r5, r8
	bne	.L1268
	b	.L1273
.L1267:
	mov	ip, ip, asl #16
	mov	ip, ip, asr #16
	mov	r3, r3, asl #16
	mov	sl, sl, asl #16
	mov	lr, lr, asl #16
	mov	sl, sl, asr #16
	mov	lr, lr, asr #16
	str	ip, [sp, #8]
	ldrh	r0, [r5, #2]
	mvn	ip, #0
	ldrsh	r1, [r5, #4]
	ldrsh	r2, [r5, #6]
	mov	r3, r3, asr #16
	str	sl, [sp, #0]
	str	lr, [sp, #4]
	str	ip, [sp, #12]
	bl	GuiLib_ShowBitmapArea
	b	.L1262
.L1273:
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, sl, pc}
.L1275:
	.align	2
.L1274:
	.word	.LANCHOR117
	.word	.LANCHOR10
	.word	.LANCHOR16
	.word	.LANCHOR18
	.word	.LANCHOR17
	.word	.LANCHOR19
.LFE78:
	.size	UpdateBackgroundBitmap, .-UpdateBackgroundBitmap
	.section	.text.DrawCursorItem,"ax",%progbits
	.align	2
	.type	DrawCursorItem, %function
DrawCursorItem:
.LFB104:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1285
	stmfd	sp!, {r4, r5, lr}
.LCFI85:
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mov	r5, r0
	cmp	r3, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r3, .L1285+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	ldmltfd	sp!, {r4, r5, pc}
	cmp	r3, #99
	ldmgtfd	sp!, {r4, r5, pc}
	ldr	r1, .L1285+8
	mov	r2, #100
	mla	r1, r2, r3, r1
	ldr	r3, [r1, #60]
	tst	r3, #1
	ldmeqfd	sp!, {r4, r5, pc}
.LBB264:
	ldr	r4, .L1285+12
	mov	r0, r4
	bl	memcpy
	ldr	r3, [r4, #60]
	tst	r3, #16384
	beq	.L1278
	ldr	r3, .L1285+16
	ldrb	r2, [r4, #96]	@ zero_extendqisi2
	mov	r1, #268
	mla	r2, r1, r2, r3
	ldr	r3, [r2, #8]
	ldrsh	r0, [r2, #164]
	blx	r3
.L1278:
	ldr	r3, .L1285+12
	ldr	r3, [r3, #60]
	tst	r3, #64
	beq	.L1279
	ldr	r3, .L1285+20
	ldr	r4, .L1285+24
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L1280
	ldrsh	r0, [r4, #0]
	blx	r3
.L1280:
	ldr	r3, .L1285+28
	ldrh	r0, [r4, #0]
	ldrh	r3, [r3, #0]
	rsb	r0, r3, r0
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bl	ScrollLineAdjustY.constprop.22
.L1279:
	cmp	r5, #0
	ldreq	r3, .L1285+4
	mvneq	r2, #0
	ldreqh	r4, [r3, #0]
	streqh	r2, [r3, #0]	@ movhi
	ldr	r3, .L1285+32
	mov	r2, #0
	strb	r2, [r3, #0]
	ldr	r3, .L1285+12
	movne	r4, #0
	ldr	r2, [r3, #60]
	tst	r2, #16384
	beq	.L1282
	ldrb	r0, [r3, #96]	@ zero_extendqisi2
	ldr	r3, .L1285+16
	mov	r2, #268
	mla	r3, r2, r0, r3
	ldrsh	r1, [r3, #164]
	bl	ScrollBox_DrawScrollLine
	b	.L1283
.L1282:
	bl	UpdateBackgroundBitmap
	mov	r0, #2
	bl	DrawItem
.L1283:
	cmp	r5, #0
	ldreq	r3, .L1285+4
	streqh	r4, [r3, #0]	@ movhi
	ldmfd	sp!, {r4, r5, pc}
.L1286:
	.align	2
.L1285:
	.word	.LANCHOR102
	.word	.LANCHOR103
	.word	.LANCHOR108
	.word	.LANCHOR10
	.word	.LANCHOR41
	.word	.LANCHOR132
	.word	.LANCHOR133
	.word	.LANCHOR95
	.word	.LANCHOR114
.LBE264:
.LFE104:
	.size	DrawCursorItem, .-DrawCursorItem
	.section	.text.GuiLib_ShowBitmapAreaAt,"ax",%progbits
	.align	2
	.global	GuiLib_ShowBitmapAreaAt
	.type	GuiLib_ShowBitmapAreaAt, %function
GuiLib_ShowBitmapAreaAt:
.LFB57:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI86:
	ldrsh	ip, [sp, #20]
	ldrsh	r5, [sp, #12]
	ldrsh	r4, [sp, #16]
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	str	ip, [sp, #20]
	mov	r1, r1, asr #16
	mov	ip, #1
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	str	r5, [sp, #12]
	str	r4, [sp, #16]
	str	ip, [sp, #24]
	ldmfd	sp!, {r4, r5, lr}
	b	ShowBitmapArea.isra.3
.LFE57:
	.size	GuiLib_ShowBitmapAreaAt, .-GuiLib_ShowBitmapAreaAt
	.section	.text.GuiLib_Clear,"ax",%progbits
	.align	2
	.global	GuiLib_Clear
	.type	GuiLib_Clear, %function
GuiLib_Clear:
.LFB59:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI87:
	bl	GuiLib_ClearDisplay
	ldr	r3, .L1297
	mvn	r2, #0
	strh	r2, [r3, #0]	@ movhi
	ldr	r3, .L1297+4
	mov	r1, #0
	add	r0, r3, #12800
.L1289:
	str	r1, [r3, #60]
	add	r3, r3, #100
	cmp	r3, r0
	mov	r2, #0
	bne	.L1289
	ldr	r3, .L1297+8
	ldr	r0, .L1297+12
	strb	r2, [r3, #0]
	ldr	r3, .L1297+16
	mov	r1, r2
	strh	r2, [r3, #0]	@ movhi
	ldr	r3, .L1297+20
.L1290:
	str	r1, [r3, #60]
	add	r3, r3, #100
	cmp	r3, r0
	mov	r2, #0
	bne	.L1290
	ldr	r3, .L1297+24
	mvn	r1, #0
	strb	r2, [r3, #4]
	strb	r2, [r3, #5]
	ldr	r3, .L1297+28
	mov	r0, r2	@ movhi
	strb	r1, [r3, #88]
	ldr	r3, .L1297+32
	ldrh	r1, [r3, #0]
	ldr	r3, .L1297+36
	strh	r1, [r3, #0]	@ movhi
	ldr	r3, .L1297+40
	ldrh	r1, [r3, #0]
	ldr	r3, .L1297+44
	strh	r1, [r3, #0]	@ movhi
	ldr	r3, .L1297+48
	strb	r2, [r3, #0]
	ldr	r3, .L1297+52
	strb	r2, [r3, #0]
	ldr	r3, .L1297+56
	strb	r2, [r3, #0]
	ldr	r3, .L1297+60
	strb	r2, [r3, #0]
	ldr	r3, .L1297+64
	strb	r2, [r3, #0]
	ldr	r3, .L1297+68
	add	ip, r3, #804
.L1291:
	strh	r2, [r3, #-4]	@ movhi
	strh	r2, [r3, #-2]	@ movhi
	strb	r0, [r3, #0]
	strh	r2, [r3, #8]	@ movhi
	strh	r2, [r3, #10]	@ movhi
	strh	r2, [r3, #12]	@ movhi
	strh	r2, [r3, #14]	@ movhi
	strh	r2, [r3, #16]	@ movhi
	add	r3, r3, #268
	cmp	r3, ip
	mov	r1, #0
	bne	.L1291
	ldr	r3, .L1297+72
	mov	r2, r1
	strh	r1, [r3, #0]	@ movhi
	ldr	r3, .L1297+76
	add	r0, r3, #80
.L1292:
	strb	r2, [r3, #0]
	strh	r2, [r3, #2]	@ movhi
	strh	r2, [r3, #4]	@ movhi
	strh	r2, [r3, #6]	@ movhi
	add	r3, r3, #8
	cmp	r3, r0
	bne	.L1292
	ldr	pc, [sp], #4
.L1298:
	.align	2
.L1297:
	.word	.LANCHOR134
	.word	.LANCHOR110
	.word	.LANCHOR102
	.word	.LANCHOR108+10000
	.word	.LANCHOR103
	.word	.LANCHOR108
	.word	.LANCHOR84
	.word	.LANCHOR56
	.word	.LANCHOR99
	.word	.LANCHOR63
	.word	.LANCHOR100
	.word	.LANCHOR64
	.word	.LANCHOR85
	.word	.LANCHOR86
	.word	.LANCHOR130
	.word	.LANCHOR39
	.word	.LANCHOR40
	.word	.LANCHOR41+4
	.word	.LANCHOR116
	.word	.LANCHOR117
.LFE59:
	.size	GuiLib_Clear, .-GuiLib_Clear
	.section	.text.GuiLib_ShowScreen,"ax",%progbits
	.align	2
	.global	GuiLib_ShowScreen
	.type	GuiLib_ShowScreen, %function
GuiLib_ShowScreen:
.LFB82:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1321
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI88:
	strh	r0, [r3, #0]	@ movhi
	ldr	r3, .L1321+4
	mov	r1, r1, asl #16
	ldr	r4, [r3, r0, asl #2]
	ldr	r3, .L1321+8
	mvn	r0, #0
	strh	r0, [r3, #0]	@ movhi
	ldr	r0, .L1321+12
	mov	r3, #0
	strb	r3, [r0, #0]
	ldr	r0, .L1321+16
	mov	r1, r1, asr #16
	strh	r1, [r0, #0]	@ movhi
	ldr	r0, .L1321+20
	cmp	r1, r3
	movlt	r1, #0
	movge	r1, #1
	cmp	r4, r3
	and	r2, r2, #255
	strb	r1, [r0, #0]
	beq	.L1300
	ldr	ip, .L1321+24
	ldr	r1, .L1321+28
	ldr	lr, .L1321+32
	strh	r3, [ip, #0]	@ movhi
	strh	r3, [ip, #2]	@ movhi
	strh	r3, [ip, #4]	@ movhi
	mov	ip, #15
	strb	ip, [r1, #66]
	strb	ip, [r1, #67]
	mov	ip, #2
	str	ip, [r1, #60]
	strb	ip, [r1, #70]
	strb	ip, [r1, #74]
	ldr	r0, .L1321+36
	ldr	ip, .L1321+40
	strh	r3, [lr, #0]	@ movhi
	strh	r3, [lr, #2]	@ movhi
	strh	r3, [lr, #4]	@ movhi
	mov	lr, #1
	strb	lr, [r1, #64]
	mov	lr, #10
	strh	r3, [r1, #10]	@ movhi
	strh	r3, [r1, #12]	@ movhi
	strh	r3, [r1, #14]	@ movhi
	strh	r3, [r1, #16]	@ movhi
	strh	r3, [r1, #22]	@ movhi
	strh	r3, [r1, #24]	@ movhi
	strh	r3, [r1, #26]	@ movhi
	strh	r3, [r1, #28]	@ movhi
	strh	r3, [r1, #30]	@ movhi
	strh	r3, [r1, #32]	@ movhi
	strb	r3, [r1, #65]
	strb	r3, [r1, #68]
	strh	r3, [r1, #18]	@ movhi
	strb	r3, [r1, #52]
	strb	lr, [r1, #72]
	strb	r3, [r1, #73]
	strb	r3, [r1, #75]
.L1301:
	str	r3, [r0, #60]
	add	r0, r0, #100
	cmp	r0, ip
	mov	r1, #0
	bne	.L1301
	ldr	r3, .L1321+44
	mov	r0, #1
	strb	r1, [r3, #4]
	strb	r1, [r3, #5]
	ldr	r3, .L1321+48
	strb	r0, [r3, #158]
	strb	r0, [r3, #426]
	strb	r0, [r3, #694]
	strb	r1, [r3, #157]
	strb	r1, [r3, #425]
	strb	r1, [r3, #693]
	ldr	r3, .L1321+52
	ldr	r0, .L1321+56
	strh	r1, [r3, #0]	@ movhi
	mov	r3, r1
.L1302:
	strb	r3, [r1, r0]
	add	r1, r1, #8
	cmp	r1, #80
	bne	.L1302
	ldr	r3, .L1321+60
	cmp	r2, #0
	add	r1, r3, #12800
	mvneq	r2, #0
	beq	.L1307
	mov	r2, #0
.L1304:
	str	r2, [r3, #60]
	add	r3, r3, #100
	cmp	r3, r1
	bne	.L1304
	ldr	r3, .L1321+64
	mov	r2, #0
	strh	r2, [r3, #0]	@ movhi
	b	.L1305
.L1307:
	ldr	r0, [r3, #60]
	tst	r0, #1
	strneb	r2, [r3, #76]
	add	r3, r3, #100
	cmp	r3, r1
	bne	.L1307
.L1305:
	ldr	r2, .L1321+68
	mov	r3, #0
	ldr	ip, .L1321+28
	strb	r3, [r2, #0]
	ldr	r2, .L1321+72
	strb	r3, [ip, #88]
	str	r3, [r2, #0]
	strh	r3, [ip, #90]	@ movhi
	ldr	r3, .L1321+76
	ldrh	r2, [r3, #0]
	ldr	r3, .L1321+80
	strh	r2, [r3, #0]	@ movhi
	ldr	r3, .L1321+84
	ldrh	r2, [r3, #0]
	ldr	r3, .L1321+88
	strh	r2, [r3, #0]	@ movhi
	ldr	r3, .L1321+92
	ldrh	r0, [r3, #0]
	ldr	r3, .L1321+96
	strh	r0, [ip, #44]	@ movhi
	ldrh	r1, [r3, #0]
	ldr	r3, .L1321+100
	strh	r1, [ip, #46]	@ movhi
	ldrh	r2, [r3, #0]
	ldr	r3, .L1321+104
	strh	r2, [ip, #48]	@ movhi
	ldrh	r3, [r3, #0]
	strh	r3, [ip, #50]	@ movhi
	ldr	ip, .L1321+108
	strh	r0, [ip, #0]	@ movhi
	ldr	ip, .L1321+112
	strh	r1, [ip, #0]	@ movhi
	ldr	ip, .L1321+116
	strh	r2, [ip, #0]	@ movhi
	ldr	ip, .L1321+120
	strh	r3, [ip, #0]	@ movhi
	ldr	ip, .L1321+124
	ldrb	ip, [ip, #0]	@ zero_extendqisi2
	cmp	ip, #0
	beq	.L1308
	cmp	r0, #0
	bne	.L1309
	cmp	r1, #0
	bne	.L1309
	ldr	ip, .L1321+128
	cmp	r2, ip
	bne	.L1309
	cmp	r3, #239
	beq	.L1310
.L1309:
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	bl	GuiLib_SetClipping
	b	.L1308
.L1310:
	bl	GuiLib_ResetClipping
.L1308:
	ldr	r3, .L1321+28
	mov	r5, #0
	strb	r5, [r3, #77]
	ldr	r3, .L1321+132
	ldr	r8, .L1321+136
	strb	r5, [r3, #0]
	ldr	r3, .L1321+140
	ldr	r7, .L1321+144
	strb	r5, [r3, #0]
	ldr	r3, .L1321+148
	ldr	r6, .L1321+152
	strb	r5, [r3, #0]
	ldr	r3, .L1321+156
	mov	r0, r4
	strh	r5, [r3, #0]	@ movhi
	mov	r3, #1
	strb	r3, [r8, #0]
	ldr	r3, .L1321+160
	mov	r1, #2
	strb	r5, [r3, #0]
	strb	r5, [r7, #0]
	strb	r5, [r6, #0]
	bl	DrawStructure
	ldr	r3, .L1321+76
	strb	r5, [r8, #0]
	ldrh	r2, [r3, #0]
	ldr	r3, .L1321+80
	strh	r2, [r3, #0]	@ movhi
	ldr	r3, .L1321+84
	ldrh	r2, [r3, #0]
	ldr	r3, .L1321+88
	strh	r2, [r3, #0]	@ movhi
	ldrb	r3, [r7, #0]	@ zero_extendqisi2
	cmp	r3, r5
	beq	.L1311
	ldrb	r3, [r6, #0]	@ zero_extendqisi2
	cmp	r3, r5
	beq	.L1311
	ldr	r3, .L1321+164
	ldrh	r2, [r3, #0]
	ldrsh	r3, [r3, #0]
	cmp	r3, r5
	ble	.L1311
	ldr	r1, .L1321+168
	ldrh	lr, [r1, #0]
	ldrsh	r1, [r1, #0]
	cmp	r3, r1
	ldrle	r3, .L1321+172
	strleh	r5, [r3, #0]	@ movhi
	ble	.L1314
	ldr	ip, .L1321+176
	add	r0, r1, r1, lsr #31
	ldrh	ip, [ip, #0]
	sub	ip, ip, r0, asr #1
	ldr	r0, .L1321+172
	mov	ip, ip, asl #16
	mov	ip, ip, lsr #16
	strh	ip, [r0, #0]	@ movhi
	mov	ip, ip, asl #16
	movs	ip, ip, asr #16
	strmih	r5, [r0, #0]	@ movhi
	bmi	.L1314
	rsb	r3, r1, r3
	cmp	ip, r3
	rsbgt	r2, lr, r2
	strgth	r2, [r0, #0]	@ movhi
.L1314:
	bl	DrawScrollList
	bl	DrawScrollIndicator
.L1311:
	ldr	r3, .L1321+124
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1300
	bl	GuiLib_ResetClipping
.L1300:
	ldr	r3, .L1321+8
	ldrh	r2, [r3, #0]
	ldrsh	r3, [r3, #0]
	cmn	r3, #1
	ldr	r3, .L1321+20
	moveq	r2, #0
	streqb	r2, [r3, #0]
	beq	.L1316
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1316
	ldr	r3, .L1321+12
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L1316
	ldr	r3, .L1321+16
	mov	r0, #1
	strh	r2, [r3, #0]	@ movhi
	bl	DrawCursorItem
.L1316:
	ldr	r3, .L1321+180
	mov	r2, #0
	strb	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L1322:
	.align	2
.L1321:
	.word	.LANCHOR134
	.word	GuiStruct_StructPtrList
	.word	.LANCHOR105
	.word	.LANCHOR104
	.word	.LANCHOR103
	.word	.LANCHOR102
	.word	.LANCHOR125
	.word	.LANCHOR10
	.word	.LANCHOR124
	.word	.LANCHOR108
	.word	.LANCHOR108+10000
	.word	.LANCHOR84
	.word	.LANCHOR41
	.word	.LANCHOR116
	.word	.LANCHOR117
	.word	.LANCHOR110
	.word	.LANCHOR109
	.word	.LANCHOR113
	.word	.LANCHOR135
	.word	.LANCHOR99
	.word	.LANCHOR63
	.word	.LANCHOR100
	.word	.LANCHOR64
	.word	.LANCHOR136
	.word	.LANCHOR137
	.word	.LANCHOR138
	.word	.LANCHOR139
	.word	.LANCHOR118
	.word	.LANCHOR119
	.word	.LANCHOR120
	.word	.LANCHOR121
	.word	.LANCHOR69
	.word	319
	.word	.LANCHOR106
	.word	.LANCHOR101
	.word	.LANCHOR86
	.word	.LANCHOR85
	.word	.LANCHOR107
	.word	.LANCHOR130
	.word	.LANCHOR122
	.word	.LANCHOR114
	.word	.LANCHOR87
	.word	.LANCHOR88
	.word	.LANCHOR95
	.word	.LANCHOR133
	.word	.LANCHOR74
.LFE82:
	.size	GuiLib_ShowScreen, .-GuiLib_ShowScreen
	.section	.text.CheckLanguageIndex,"ax",%progbits
	.align	2
	.global	CheckLanguageIndex
	.type	CheckLanguageIndex, %function
CheckLanguageIndex:
.LFB83:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, r0, asl #16
	cmp	r0, #65536
	movls	r0, r0, asr #16
	movhi	r0, #0
	bx	lr
.LFE83:
	.size	CheckLanguageIndex, .-CheckLanguageIndex
	.section	.text.GuiLib_GetTextLanguagePtr,"ax",%progbits
	.align	2
	.global	GuiLib_GetTextLanguagePtr
	.type	GuiLib_GetTextLanguagePtr, %function
GuiLib_GetTextLanguagePtr:
.LFB84:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r2, r2, asl #16
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI89:
	mov	r6, r2, asr #16
	ldr	r2, .L1335
	mov	r3, r0, asl #16
	ldr	r3, [r2, r3, lsr #14]
	mov	r1, r1, asl #16
	cmp	r3, #0
	mov	r4, r1, lsr #16
	beq	.L1333
	ldr	r0, .L1335+4
	add	r3, r3, #2
	str	r3, [r0, #0]
	ldr	r3, .L1335+8
.LBB265:
	mov	r2, r6, asl #16
.LBE265:
	mov	r5, #0
.LBB266:
	cmp	r2, #65536
.LBE266:
	strh	r5, [r3, #0]	@ movhi
.LBB267:
	movhi	r6, #0
.LBE267:
	bl	GetItemByte
	mov	r7, r5
	ldr	r8, .L1335+12
	mov	sl, r0
	b	.L1329
.L1332:
	mov	r0, r6
	bl	ReadItem
	ldrb	r3, [r8, #56]	@ zero_extendqisi2
	cmp	r3, #0
	cmpne	r3, #11
	bne	.L1330
	cmp	r7, r4
	bne	.L1331
	ldr	r3, .L1335+12
	ldr	r0, [r3, #4]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L1331:
	add	r7, r7, #1
	mov	r7, r7, asl #16
	mov	r7, r7, lsr #16
.L1330:
	add	r5, r5, #1
.L1329:
	cmp	r5, sl
	blt	.L1332
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L1333:
	mov	r0, r3
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L1336:
	.align	2
.L1335:
	.word	GuiStruct_StructPtrList
	.word	.LANCHOR22
	.word	.LANCHOR21
	.word	.LANCHOR10
.LFE84:
	.size	GuiLib_GetTextLanguagePtr, .-GuiLib_GetTextLanguagePtr
	.section	.text.GuiLib_GetTextPtr,"ax",%progbits
	.align	2
	.global	GuiLib_GetTextPtr
	.type	GuiLib_GetTextPtr, %function
GuiLib_GetTextPtr:
.LFB85:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L1338
	mov	r0, r0, asl #16
	ldrsh	r2, [r3, #0]
	mov	r1, r1, asl #16
	mov	r0, r0, lsr #16
	mov	r1, r1, lsr #16
	b	GuiLib_GetTextLanguagePtr
.L1339:
	.align	2
.L1338:
	.word	.LANCHOR123
.LFE85:
	.size	GuiLib_GetTextPtr, .-GuiLib_GetTextPtr
	.section	.text.GuiLib_GetTextWidth,"ax",%progbits
	.align	2
	.global	GuiLib_GetTextWidth
	.type	GuiLib_GetTextWidth, %function
GuiLib_GetTextWidth:
.LFB86:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI90:
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	mov	r4, r0
	cmp	r1, #0
	cmpne	r3, #0
	and	r6, r2, #255
	mov	r5, r1
	moveq	r3, #0
	movne	r3, #1
	beq	.L1342
.LBB268:
	ldr	r3, .L1343
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldr	r3, .L1343+4
	strb	r2, [r3, #0]
.LBE268:
	ldr	r3, .L1343+8
	strb	r6, [r3, #70]
	bl	strlen
	ldr	r3, .L1343+12
	str	r5, [r3, #0]
	mov	r0, r0, asl #16
	mov	r5, r0, lsr #16
	mov	r1, r5
	mov	r0, r4
	bl	PrepareText
	mov	r0, r6
	mov	r1, r5
	mov	r2, #0
	bl	TextPixelLength
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	ldmfd	sp!, {r4, r5, r6, pc}
.L1342:
	mov	r0, r3
	ldmfd	sp!, {r4, r5, r6, pc}
.L1344:
	.align	2
.L1343:
	.word	.LANCHOR140
	.word	.LANCHOR9
	.word	.LANCHOR10
	.word	.LANCHOR8
.LFE86:
	.size	GuiLib_GetTextWidth, .-GuiLib_GetTextWidth
	.section	.text.GuiLib_GetCharCode,"ax",%progbits
	.align	2
	.global	GuiLib_GetCharCode
	.type	GuiLib_GetCharCode, %function
GuiLib_GetCharCode:
.LFB88:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI91:
	mov	r3, r3, asl #16
	mov	r2, r2, asl #16
	mov	r1, r1, lsr #16
	mov	r0, r0, lsr #16
	mov	r4, r2, lsr #16
	mov	r5, r3, lsr #16
	bl	GuiLib_GetTextPtr
	mov	r6, r0
	bl	strlen
.LBB269:
	mov	r0, r0, asl #16
	cmp	r4, r0, lsr #16
	movls	r3, #0
	movhi	r3, #1
	cmp	r4, #2000
	orrhi	r3, r3, #1
	cmp	r3, #0
	bne	.L1346
	mov	r0, r6
	mov	r1, r4
	mov	r2, r5
.LBE269:
	ldmfd	sp!, {r4, r5, r6, lr}
.LBB270:
	b	GetCharCode.part.6
.L1346:
.LBE270:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.LFE88:
	.size	GuiLib_GetCharCode, .-GuiLib_GetCharCode
	.section	.text.GuiLib_GetBlinkingCharCode,"ax",%progbits
	.align	2
	.global	GuiLib_GetBlinkingCharCode
	.type	GuiLib_GetBlinkingCharCode, %function
GuiLib_GetBlinkingCharCode:
.LFB89:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI92:
	ldr	r7, .L1351
	mov	r0, r0, lsr #16
	mov	r6, #60
	mul	r6, r0, r6
	mov	r1, r1, asl #16
	add	r3, r7, r6
	ldrb	r0, [r3, #4]	@ zero_extendqisi2
	mov	r2, r2, asl #16
	cmp	r0, #0
	mov	r4, r1, lsr #16
	mov	r5, r2, lsr #16
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	ldr	r0, [r7, r6]
	bl	strlen
	ldr	r3, [r7, r6]
.LBB271:
	mov	r0, r0, asl #16
	cmp	r4, r0, lsr #16
	movls	r2, #0
	movhi	r2, #1
	cmp	r4, #2000
	orrhi	r2, r2, #1
	cmp	r2, #0
	bne	.L1350
	mov	r0, r3
	mov	r1, r4
	mov	r2, r5
.LBE271:
	ldmfd	sp!, {r4, r5, r6, r7, lr}
.LBB272:
	b	GetCharCode.part.6
.L1350:
	mov	r0, #0
.LBE272:
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L1352:
	.align	2
.L1351:
	.word	.LANCHOR84
.LFE89:
	.size	GuiLib_GetBlinkingCharCode, .-GuiLib_GetBlinkingCharCode
	.section	.text.GuiLib_SetLanguage,"ax",%progbits
	.align	2
	.global	GuiLib_SetLanguage
	.type	GuiLib_SetLanguage, %function
GuiLib_SetLanguage:
.LFB90:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, r0, asl #16
	ldr	r3, .L1356
.LBB273:
	cmp	r0, #65536
.LBE273:
	ldr	r2, .L1356+4
.LBB274:
	movls	r0, r0, asr #16
	movhi	r0, #0
.LBE274:
	strh	r0, [r3, #0]	@ movhi
	mov	r3, #0
	strb	r3, [r2, #0]
.LBB275:
	ldr	r2, .L1356+8
	strb	r3, [r2, #0]
.LBE275:
	bx	lr
.L1357:
	.align	2
.L1356:
	.word	.LANCHOR123
	.word	.LANCHOR140
	.word	.LANCHOR9
.LFE90:
	.size	GuiLib_SetLanguage, .-GuiLib_SetLanguage
	.section	.text.GuiLib_Init,"ax",%progbits
	.align	2
	.global	GuiLib_Init
	.type	GuiLib_Init, %function
GuiLib_Init:
.LFB58:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1359
	stmfd	sp!, {r4, r5, lr}
.LCFI93:
	ldr	r2, .L1359+4
	mov	r4, #0
	str	r4, [r3, #0]
	ldr	r3, .L1359+8
	mov	r5, r4	@ movhi
	strh	r4, [r3, #0]	@ movhi
	ldr	r3, .L1359+12
	strh	r4, [r3, #0]	@ movhi
	ldr	r3, .L1359+16
	strh	r4, [r3, #0]	@ movhi
	ldr	r3, .L1359+20
	strh	r4, [r3, #0]	@ movhi
	ldr	r3, .L1359+24
	strh	r2, [r3, #0]	@ movhi
	ldr	r3, .L1359+28
	mov	r2, #239
	strh	r2, [r3, #0]	@ movhi
	ldr	r3, .L1359+32
	strh	r4, [r3, #0]	@ movhi
	ldr	r3, .L1359+36
	strh	r4, [r3, #0]	@ movhi
	bl	GuiLib_ResetClipping
	bl	GuiLib_ResetDisplayRepaint
	bl	GuiLib_Clear
	ldr	r3, .L1359+40
	mov	r2, #1
	strb	r2, [r3, #0]
	ldr	r3, .L1359+44
	mov	r0, r4
	strb	r4, [r3, #0]
	ldr	r3, .L1359+48
	str	r4, [r3, #0]
	ldr	r3, .L1359+52
	strb	r4, [r3, #0]
	bl	GuiLib_SetLanguage
	ldr	r3, .L1359+56
	mvn	r2, #0
	str	r4, [r3, #0]
	ldr	r3, .L1359+60
	strh	r4, [r3, #0]	@ movhi
	ldr	r3, .L1359+64
	strb	r4, [r3, #0]
	ldr	r3, .L1359+68
	strb	r4, [r3, #0]
	ldr	r3, .L1359+72
	strh	r2, [r3, #0]	@ movhi
	ldmfd	sp!, {r4, r5, pc}
.L1360:
	.align	2
.L1359:
	.word	.LANCHOR141
	.word	319
	.word	.LANCHOR99
	.word	.LANCHOR100
	.word	.LANCHOR136
	.word	.LANCHOR137
	.word	.LANCHOR138
	.word	.LANCHOR139
	.word	.LANCHOR63
	.word	.LANCHOR64
	.word	.LANCHOR69
	.word	.LANCHOR101
	.word	.LANCHOR135
	.word	.LANCHOR114
	.word	.LANCHOR132
	.word	.LANCHOR68
	.word	.LANCHOR79
	.word	.LANCHOR113
	.word	.LANCHOR134
.LFE58:
	.size	GuiLib_Init, .-GuiLib_Init
	.section	.text.GuiLib_SetScrollPars,"ax",%progbits
	.align	2
	.global	GuiLib_SetScrollPars
	.type	GuiLib_SetScrollPars, %function
GuiLib_SetScrollPars:
.LFB92:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L1364
	mov	r1, r1, asl #16
	str	r0, [r3, #0]
	mov	r2, r2, asl #16
	ldr	r3, .L1364+4
	mov	r2, r2, asr #16
	mov	r1, r1, asr #16
	bic	r1, r1, r1, asr #31
	cmn	r2, #1
	strh	r1, [r3, #0]	@ movhi
	mvnlt	r2, #0
	blt	.L1362
	cmp	r1, r2
	suble	r1, r1, #1
	movle	r2, r1, asl #16
	movle	r2, r2, asr #16
.L1362:
	ldr	r3, .L1364+8
	strh	r2, [r3, #0]	@ movhi
	bx	lr
.L1365:
	.align	2
.L1364:
	.word	.LANCHOR132
	.word	.LANCHOR87
	.word	.LANCHOR133
.LFE92:
	.size	GuiLib_SetScrollPars, .-GuiLib_SetScrollPars
	.section	.text.GuiLib_ScrollLineOffsetY,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollLineOffsetY
	.type	GuiLib_ScrollLineOffsetY, %function
GuiLib_ScrollLineOffsetY:
.LFB94:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L1369
	ldrsh	r3, [r3, #0]
	cmn	r3, #1
	ldrne	r2, .L1369+4
	moveq	r0, #0
	ldrnesh	r0, [r2, #0]
	ldrne	r2, .L1369+8
	rsbne	r3, r0, r3
	ldrneh	r0, [r2, #0]
	mulne	r0, r3, r0
	movne	r0, r0, asl #16
	movne	r0, r0, lsr #16
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bx	lr
.L1370:
	.align	2
.L1369:
	.word	.LANCHOR133
	.word	.LANCHOR95
	.word	.LANCHOR67
.LFE94:
	.size	GuiLib_ScrollLineOffsetY, .-GuiLib_ScrollLineOffsetY
	.section	.text.GuiLib_RedrawScrollList,"ax",%progbits
	.align	2
	.global	GuiLib_RedrawScrollList
	.type	GuiLib_RedrawScrollList, %function
GuiLib_RedrawScrollList:
.LFB98:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI94:
	bl	DrawScrollList
	ldr	lr, [sp], #4
	b	DrawScrollIndicator
.LFE98:
	.size	GuiLib_RedrawScrollList, .-GuiLib_RedrawScrollList
	.section	.text.GuiLib_Scroll_Down,"ax",%progbits
	.align	2
	.global	GuiLib_Scroll_Down
	.type	GuiLib_Scroll_Down, %function
GuiLib_Scroll_Down:
.LFB99:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1385
	stmfd	sp!, {r4, lr}
.LCFI95:
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L1385+4
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L1385+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	ble	.L1382
	ldr	r1, .L1385+12
	ldrsh	r0, [r1, #0]
	ldrh	ip, [r1, #0]
	cmn	r0, #1
	bne	.L1374
	ldr	r2, .L1385+16
	ldrsh	r1, [r2, #0]
	cmp	r3, r1
	ble	.L1382
	ldr	r2, .L1385+20
	rsb	r3, r1, r3
	ldrsh	ip, [r2, #0]
	ldrh	r0, [r2, #0]
	cmp	ip, r3
	addlt	r0, r0, #1
	strlth	r0, [r2, #0]	@ movhi
	bge	.L1382
	b	.L1384
.L1374:
	sub	r2, r3, #1
	cmp	r0, r2
	bge	.L1383
	add	ip, ip, #1
	mov	ip, ip, asl #16
	mov	ip, ip, lsr #16
	strh	ip, [r1, #0]	@ movhi
	ldr	r1, .L1385+16
	ldrh	lr, [r1, #0]
	ldrsh	r1, [r1, #0]
	cmp	r3, r1
	ble	.L1375
	mov	r3, ip, asl #16
	mov	r3, r3, asr #16
	cmp	r3, r2
	blt	.L1376
	cmp	r1, #2
	bgt	.L1375
	cmp	r3, r2
	bne	.L1375
.L1376:
	ldr	r2, .L1385+20
	mov	lr, lr, asl #16
	ldrsh	r1, [r2, #0]
	mov	lr, lr, asr #16
	rsb	r4, r1, r3
	sub	lr, lr, #1
	cmp	r4, lr
	ldrh	r0, [r2, #0]
	blt	.L1375
	add	r1, r1, #1
	cmp	r3, r1
	ble	.L1375
	add	r3, r0, #1
	strh	r3, [r2, #0]	@ movhi
.L1384:
	bl	DrawScrollList
	b	.L1377
.L1375:
	ldr	r4, .L1385+20
	ldrh	r0, [r4, #0]
	rsb	r0, r0, ip
	sub	r0, r0, #1
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bl	DrawScrollLine
	ldr	r3, .L1385+12
	ldrh	r0, [r3, #0]
	ldrh	r3, [r4, #0]
	rsb	r0, r3, r0
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bl	DrawScrollLine
.L1377:
	bl	DrawScrollIndicator
	mov	r0, #1
	ldmfd	sp!, {r4, pc}
.L1382:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L1383:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L1386:
	.align	2
.L1385:
	.word	.LANCHOR85
	.word	.LANCHOR130
	.word	.LANCHOR87
	.word	.LANCHOR133
	.word	.LANCHOR88
	.word	.LANCHOR95
.LFE99:
	.size	GuiLib_Scroll_Down, .-GuiLib_Scroll_Down
	.section	.text.GuiLib_Scroll_Up,"ax",%progbits
	.align	2
	.global	GuiLib_Scroll_Up
	.type	GuiLib_Scroll_Up, %function
GuiLib_Scroll_Up:
.LFB100:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1400
	stmfd	sp!, {r4, lr}
.LCFI96:
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L1400+4
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L1400+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	ble	.L1397
	ldr	r2, .L1400+12
	ldrsh	r1, [r2, #0]
	ldrh	r0, [r2, #0]
	cmn	r1, #1
	bne	.L1389
	ldr	r2, .L1400+16
	ldrsh	r2, [r2, #0]
	cmp	r2, r3
	bge	.L1397
	ldr	r3, .L1400+20
	ldrsh	r1, [r3, #0]
	ldrh	r2, [r3, #0]
	cmp	r1, #0
	subgt	r2, r2, #1
	bgt	.L1399
	b	.L1397
.L1389:
	cmp	r1, #0
	ble	.L1398
	sub	r0, r0, #1
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	strh	r0, [r2, #0]	@ movhi
	ldr	r2, .L1400+16
	ldrh	lr, [r2, #0]
	ldrsh	r2, [r2, #0]
	cmp	r3, r2
	ble	.L1390
	cmp	r0, #0
	bne	.L1391
	cmp	r2, #2
	bgt	.L1390
.L1391:
	ldr	r3, .L1400+20
	mov	r2, r0, asl #16
	ldrsh	r1, [r3, #0]
	mov	r2, r2, asr #16
	rsb	r4, r1, r2
	cmp	r4, #0
	ldrh	ip, [r3, #0]
	bgt	.L1390
	mov	lr, lr, asl #16
	add	r1, r1, lr, asr #16
	sub	r1, r1, #2
	cmp	r2, r1
	bge	.L1390
	sub	r2, ip, #1
.L1399:
	strh	r2, [r3, #0]	@ movhi
	bl	DrawScrollList
	b	.L1392
.L1390:
	ldr	r4, .L1400+20
	ldrh	r3, [r4, #0]
	rsb	r0, r3, r0
	add	r0, r0, #1
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bl	DrawScrollLine
	ldr	r3, .L1400+12
	ldrh	r0, [r3, #0]
	ldrh	r3, [r4, #0]
	rsb	r0, r3, r0
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bl	DrawScrollLine
.L1392:
	bl	DrawScrollIndicator
	mov	r0, #1
	ldmfd	sp!, {r4, pc}
.L1397:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L1398:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L1401:
	.align	2
.L1400:
	.word	.LANCHOR85
	.word	.LANCHOR130
	.word	.LANCHOR87
	.word	.LANCHOR133
	.word	.LANCHOR88
	.word	.LANCHOR95
.LFE100:
	.size	GuiLib_Scroll_Up, .-GuiLib_Scroll_Up
	.section	.text.GuiLib_Scroll_Home,"ax",%progbits
	.align	2
	.global	GuiLib_Scroll_Home
	.type	GuiLib_Scroll_Home, %function
GuiLib_Scroll_Home:
.LFB101:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1415
	stmfd	sp!, {r4, lr}
.LCFI97:
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L1415+4
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L1415+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	movle	r0, #0
	ldmlefd	sp!, {r4, pc}
	ldr	r2, .L1415+12
	ldrsh	r1, [r2, #0]
	ldrh	ip, [r2, #0]
	cmn	r1, #1
	bne	.L1404
	ldr	r2, .L1415+16
	ldrsh	r2, [r2, #0]
	cmp	r2, r3
	bge	.L1411
	ldr	r3, .L1415+20
	mov	r0, #0
	ldrsh	r2, [r3, #0]
	cmp	r2, #0
	strgth	r0, [r3, #0]	@ movhi
	bgt	.L1414
	ldmfd	sp!, {r4, pc}
.L1404:
	cmp	r1, #0
	mov	r0, #0
	ldmlefd	sp!, {r4, pc}
	strh	r0, [r2, #0]	@ movhi
	ldr	r2, .L1415+16
	ldr	r4, .L1415+20
	ldrsh	r2, [r2, #0]
	cmp	r2, r3
	bge	.L1405
	ldrsh	r3, [r4, #0]
	cmp	r3, r0
	bne	.L1406
.L1405:
	ldrh	r0, [r4, #0]
	rsb	ip, r0, ip
	mov	r0, ip, asl #16
	mov	r0, r0, asr #16
	bl	DrawScrollLine
	ldr	r3, .L1415+12
	ldrh	r0, [r3, #0]
	ldrh	r3, [r4, #0]
	rsb	r0, r3, r0
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bl	DrawScrollLine
	b	.L1407
.L1406:
	strh	r0, [r4, #0]	@ movhi
.L1414:
	bl	DrawScrollList
.L1407:
	bl	DrawScrollIndicator
	mov	r0, #1
	ldmfd	sp!, {r4, pc}
.L1411:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L1416:
	.align	2
.L1415:
	.word	.LANCHOR85
	.word	.LANCHOR130
	.word	.LANCHOR87
	.word	.LANCHOR133
	.word	.LANCHOR88
	.word	.LANCHOR95
.LFE101:
	.size	GuiLib_Scroll_Home, .-GuiLib_Scroll_Home
	.section	.text.GuiLib_Scroll_End,"ax",%progbits
	.align	2
	.global	GuiLib_Scroll_End
	.type	GuiLib_Scroll_End, %function
GuiLib_Scroll_End:
.LFB102:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1430
	stmfd	sp!, {r4, r5, lr}
.LCFI98:
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	cmp	r0, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r3, .L1430+4
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	cmp	r0, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r3, .L1430+8
	ldrh	ip, [r3, #0]
	mov	r1, ip, asl #16
	mov	r3, r1, asr #16
	cmp	r3, #0
	ble	.L1427
	ldr	r2, .L1430+12
	ldrsh	r0, [r2, #0]
	ldrh	r5, [r2, #0]
	cmn	r0, #1
	bne	.L1419
	ldr	r2, .L1430+16
	ldrsh	r1, [r2, #0]
	ldrh	r0, [r2, #0]
	cmp	r3, r1
	ble	.L1427
	ldr	r2, .L1430+20
	rsb	r3, r1, r3
	ldrsh	r4, [r2, #0]
	cmp	r4, r3
	rsblt	ip, r0, ip
	strlth	ip, [r2, #0]	@ movhi
	bge	.L1427
	b	.L1429
.L1419:
	sub	ip, r3, #1
	cmp	r0, ip
	bge	.L1428
	mov	r1, r1, lsr #16
	sub	r0, r1, #1
	strh	r0, [r2, #0]	@ movhi
	ldr	r2, .L1430+16
	ldr	r4, .L1430+20
	ldrh	r0, [r2, #0]
	ldrsh	r2, [r2, #0]
	cmp	r3, r2
	ble	.L1420
	ldrsh	ip, [r4, #0]
	rsb	r3, r2, r3
	cmp	ip, r3
	rsbne	r1, r0, r1
	strneh	r1, [r4, #0]	@ movhi
	bne	.L1429
.L1420:
	ldrh	r0, [r4, #0]
	rsb	r5, r0, r5
	mov	r0, r5, asl #16
	mov	r0, r0, asr #16
	bl	DrawScrollLine
	ldr	r3, .L1430+12
	ldrh	r0, [r3, #0]
	ldrh	r3, [r4, #0]
	rsb	r0, r3, r0
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bl	DrawScrollLine
	b	.L1422
.L1429:
	bl	DrawScrollList
.L1422:
	bl	DrawScrollIndicator
	mov	r0, #1
	ldmfd	sp!, {r4, r5, pc}
.L1427:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, pc}
.L1428:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, pc}
.L1431:
	.align	2
.L1430:
	.word	.LANCHOR85
	.word	.LANCHOR130
	.word	.LANCHOR87
	.word	.LANCHOR133
	.word	.LANCHOR88
	.word	.LANCHOR95
.LFE102:
	.size	GuiLib_Scroll_End, .-GuiLib_Scroll_End
	.section	.text.GuiLib_Scroll_To_Line,"ax",%progbits
	.align	2
	.global	GuiLib_Scroll_To_Line
	.type	GuiLib_Scroll_To_Line, %function
GuiLib_Scroll_To_Line:
.LFB103:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r1, .L1449
	mov	r3, r0, asl #16
	ldrh	r2, [r1, #0]
	ldrsh	r1, [r1, #0]
	mov	r3, r3, asr #16
	cmp	r1, r3
	ldr	r1, .L1449+4
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI99:
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	suble	r3, r2, #1
	movle	r3, r3, asl #16
	movle	r3, r3, asr #16
	bicgt	r3, r3, r3, asr #31
	cmp	r0, #0
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
	ldr	r1, .L1449+8
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	cmp	r0, #0
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
	mov	r1, r2, asl #16
	mov	r1, r1, asr #16
	cmp	r1, #0
	ble	.L1447
	ldr	r5, .L1449+12
	ldrsh	r0, [r5, #0]
	ldrh	ip, [r5, #0]
	cmn	r0, #1
	bne	.L1436
	ldr	r0, .L1449+16
	ldrh	ip, [r0, #0]
	ldrsh	r0, [r0, #0]
	cmp	r1, r0
	ble	.L1447
	rsb	r1, r0, r1
	cmp	r3, r1
	rsbgt	r3, ip, r2
	ldr	r2, .L1449+20
	movgt	r3, r3, asl #16
	ldrsh	r1, [r2, #0]
	movgt	r3, r3, asr #16
	cmp	r1, r3
	strneh	r3, [r2, #0]	@ movhi
	bne	.L1442
	b	.L1447
.L1436:
	cmp	r0, r3
	beq	.L1448
	ldr	r0, .L1449+16
	ldr	r4, .L1449+20
	ldrh	r6, [r0, #0]
	ldrsh	r8, [r4, #0]
	mov	r6, r6, asl #16
	mov	r0, r6, asr #16
	cmp	r0, #2
	movle	lr, #0
	movgt	lr, #1
	add	r9, r8, lr
	cmp	r3, r9
	strh	r3, [r5, #0]	@ movhi
	ldrh	sl, [r4, #0]
	mov	r7, lr
	bge	.L1438
	rsb	r3, lr, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	tst	r3, #32768
	strh	r3, [r4, #0]	@ movhi
	movne	r3, #0
	strneh	r3, [r4, #0]	@ movhi
	b	.L1442
.L1438:
	add	r8, r8, r0
	rsb	r7, lr, r8
	cmp	r7, r3
	bgt	.L1441
	mov	ip, r6, lsr #16
	rsb	lr, ip, lr
	add	lr, lr, #1
	add	r3, lr, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [r4, #0]	@ movhi
	rsb	r1, r0, r1
	mov	r3, r3, asl #16
	cmp	r1, r3, asr #16
	rsblt	r2, ip, r2
	strlth	r2, [r4, #0]	@ movhi
.L1442:
	bl	DrawScrollList
	b	.L1440
.L1441:
	rsb	ip, sl, ip
	mov	r0, ip, asl #16
	mov	r0, r0, asr #16
	bl	DrawScrollLine
	ldrh	r0, [r5, #0]
	ldrh	r3, [r4, #0]
	rsb	r0, r3, r0
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bl	DrawScrollLine
.L1440:
	bl	DrawScrollIndicator
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L1447:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L1448:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L1450:
	.align	2
.L1449:
	.word	.LANCHOR87
	.word	.LANCHOR85
	.word	.LANCHOR130
	.word	.LANCHOR133
	.word	.LANCHOR88
	.word	.LANCHOR95
.LFE103:
	.size	GuiLib_Scroll_To_Line, .-GuiLib_Scroll_To_Line
	.section	.text.GuiLib_Cursor_Hide,"ax",%progbits
	.align	2
	.global	GuiLib_Cursor_Hide
	.type	GuiLib_Cursor_Hide, %function
GuiLib_Cursor_Hide:
.LFB105:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI100:
	mov	r0, #0
	bl	DrawCursorItem
	ldr	r3, .L1452
	mvn	r2, #0
	strh	r2, [r3, #0]	@ movhi
	ldr	pc, [sp], #4
.L1453:
	.align	2
.L1452:
	.word	.LANCHOR103
.LFE105:
	.size	GuiLib_Cursor_Hide, .-GuiLib_Cursor_Hide
	.section	.text.GuiLib_IsCursorFieldInUse,"ax",%progbits
	.align	2
	.global	GuiLib_IsCursorFieldInUse
	.type	GuiLib_IsCursorFieldInUse, %function
GuiLib_IsCursorFieldInUse:
.LFB106:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, r0, asl #16
	cmp	r0, #6488064
	ldrls	r2, .L1457
	mov	r3, r0, asr #16
	movls	r1, #100
	mlals	r3, r1, r3, r2
	movhi	r0, #0
	ldrls	r0, [r3, #60]
	andls	r0, r0, #1
	bx	lr
.L1458:
	.align	2
.L1457:
	.word	.LANCHOR108
.LFE106:
	.size	GuiLib_IsCursorFieldInUse, .-GuiLib_IsCursorFieldInUse
	.section	.text.GuiLib_Cursor_Select,"ax",%progbits
	.align	2
	.global	GuiLib_Cursor_Select
	.type	GuiLib_Cursor_Select, %function
GuiLib_Cursor_Select:
.LFB107:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, lr}
.LCFI101:
	mov	r4, r0, asr #16
	cmn	r4, #1
	bne	.L1460
	bl	GuiLib_Cursor_Hide
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L1460:
	mov	r3, r4, asl #16
	cmp	r3, #6488064
	bhi	.L1462
	ldr	r3, .L1464
	mov	r2, #100
	mla	r3, r2, r4, r3
	ldr	r0, [r3, #60]
	ands	r0, r0, #1
	ldmeqfd	sp!, {r4, pc}
.LBB278:
	mov	r0, #0
	bl	DrawCursorItem
	ldr	r3, .L1464+4
	mov	r0, #1
	strh	r4, [r3, #0]	@ movhi
	bl	DrawCursorItem
	mov	r0, #1
	ldmfd	sp!, {r4, pc}
.L1462:
.LBE278:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L1465:
	.align	2
.L1464:
	.word	.LANCHOR108
	.word	.LANCHOR103
.LFE107:
	.size	GuiLib_Cursor_Select, .-GuiLib_Cursor_Select
	.section	.text.GuiLib_Cursor_Down,"ax",%progbits
	.align	2
	.global	GuiLib_Cursor_Down
	.type	GuiLib_Cursor_Down, %function
GuiLib_Cursor_Down:
.LFB108:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L1471
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI102:
	ldrsh	r3, [r2, #0]
	ldr	r1, .L1471+4
	mvn	r5, #99
	add	r0, r3, r3, asl #2
	mul	r5, r3, r5
	mov	ip, #100
	add	r0, r0, r0, asl #2
	mla	ip, r3, ip, r1
	mov	r4, r0, asl #2
	ldrh	r1, [r2, #0]
	mov	r2, #0
.L1469:
	mov	r0, r1, asl #16
	cmp	r0, #6422528
	bgt	.L1470
	add	r6, ip, r5
	add	r6, r6, r4
	ldr	r6, [r6, #160]
	add	r1, r1, #1
	mov	r1, r1, asl #16
	add	r0, r3, #1
	tst	r6, #1
	mov	r1, r1, lsr #16
	add	r0, r0, r2
	beq	.L1468
	bl	GuiLib_Cursor_Select
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, pc}
.L1468:
	add	r2, r2, #1
	add	ip, ip, #100
	b	.L1469
.L1470:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L1472:
	.align	2
.L1471:
	.word	.LANCHOR103
	.word	.LANCHOR108
.LFE108:
	.size	GuiLib_Cursor_Down, .-GuiLib_Cursor_Down
	.section	.text.GuiLib_Cursor_Up,"ax",%progbits
	.align	2
	.global	GuiLib_Cursor_Up
	.type	GuiLib_Cursor_Up, %function
GuiLib_Cursor_Up:
.LFB109:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L1478
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI103:
	ldrsh	r3, [r2, #0]
	ldr	r1, .L1478+4
	mvn	r5, #99
	add	r0, r3, r3, asl #2
	mul	r5, r3, r5
	mov	ip, #100
	add	r0, r0, r0, asl #2
	mla	ip, r3, ip, r1
	mov	r4, r0, asl #2
	ldrh	r1, [r2, #0]
	mov	r2, #0
.L1476:
	mov	r0, r1, asl #16
	cmp	r0, #0
	ble	.L1477
	add	r6, ip, r5
	add	r6, r6, r4
	ldr	r6, [r6, #-40]
	sub	r1, r1, #1
	mov	r1, r1, asl #16
	sub	r0, r3, #1
	tst	r6, #1
	mov	r1, r1, lsr #16
	rsb	r0, r2, r0
	beq	.L1475
	bl	GuiLib_Cursor_Select
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, pc}
.L1475:
	add	r2, r2, #1
	sub	ip, ip, #100
	b	.L1476
.L1477:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L1479:
	.align	2
.L1478:
	.word	.LANCHOR103
	.word	.LANCHOR108
.LFE109:
	.size	GuiLib_Cursor_Up, .-GuiLib_Cursor_Up
	.section	.text.GuiLib_Cursor_Home,"ax",%progbits
	.align	2
	.global	GuiLib_Cursor_Home
	.type	GuiLib_Cursor_Home, %function
GuiLib_Cursor_Home:
.LFB110:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI104:
	ldr	r3, .L1486
	mov	r0, #0
	b	.L1481
.L1483:
	add	r0, r0, #1
	mov	r2, r0, asl #16
	cmp	r2, #6488064
	add	r3, r3, #100
	mov	r0, r2, lsr #16
	beq	.L1482
.L1481:
	ldr	r2, [r3, #60]
	tst	r2, #1
	beq	.L1483
.L1482:
	ldr	r3, .L1486+4
	mov	r0, r0, asl #16
	ldrsh	r3, [r3, #0]
	mov	r0, r0, asr #16
	cmp	r3, r0
	beq	.L1485
	bl	GuiLib_Cursor_Select
	mov	r0, #1
	ldr	pc, [sp], #4
.L1485:
	mov	r0, #0
	ldr	pc, [sp], #4
.L1487:
	.align	2
.L1486:
	.word	.LANCHOR108
	.word	.LANCHOR103
.LFE110:
	.size	GuiLib_Cursor_Home, .-GuiLib_Cursor_Home
	.section	.text.GuiLib_Cursor_End,"ax",%progbits
	.align	2
	.global	GuiLib_Cursor_End
	.type	GuiLib_Cursor_End, %function
GuiLib_Cursor_End:
.LFB111:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI105:
	ldr	ip, .L1500
	mov	r3, #0
	mov	r0, #99
	b	.L1489
.L1491:
	sub	r0, r0, #1
	cmn	r0, #1
	sub	r3, r3, #100
	beq	.L1494
.L1489:
	add	r2, ip, r3
	add	r2, r2, #9920
	ldr	r2, [r2, #40]
	mov	r1, r0, asl #16
	ands	r2, r2, #1
	mov	r1, r1, lsr #16
	beq	.L1491
	b	.L1498
.L1497:
	bl	GuiLib_Cursor_Select
	mov	r0, #1
	ldr	pc, [sp], #4
.L1494:
	mov	r0, r2
	ldr	pc, [sp], #4
.L1499:
	mov	r0, #0
	ldr	pc, [sp], #4
.L1498:
	ldr	r3, .L1500+4
	ldrh	r3, [r3, #0]
	cmp	r1, r3
	bne	.L1497
	b	.L1499
.L1501:
	.align	2
.L1500:
	.word	.LANCHOR108
	.word	.LANCHOR103
.LFE111:
	.size	GuiLib_Cursor_End, .-GuiLib_Cursor_End
	.section	.text.GuiLib_BlinkBoxMarkedItem,"ax",%progbits
	.align	2
	.global	GuiLib_BlinkBoxMarkedItem
	.type	GuiLib_BlinkBoxMarkedItem, %function
GuiLib_BlinkBoxMarkedItem:
.LFB115:
	@ args = 0, pretend = 0, frame = 4564
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI106:
	mov	r0, r0, asl #16
	sub	sp, sp, #4544
.LCFI107:
	mov	r2, r2, asl #16
	sub	sp, sp, #24
.LCFI108:
	mov	r1, r1, asl #16
	mov	r2, r2, asr #16
	movs	r6, r0, lsr #16
	mov	r4, r1, lsr #16
	str	r2, [sp, #40]
	bne	.L1502
	ldr	r5, .L1569
	ldrb	r3, [r5, #4]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1502
	ldrb	r3, [r5, #5]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1504
	ldrb	r3, [r5, #19]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1504
	ldrb	r3, [r5, #21]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1504
	ldr	r3, .L1569+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1504
	ldrsh	r0, [r5, #38]
	ldrsh	r1, [r5, #42]
	ldrsh	r2, [r5, #40]
	ldrsh	r3, [r5, #44]
	bl	GuiLib_InvertBox
	strb	r6, [r5, #21]
.L1504:
	ldr	r5, .L1569
	mov	r7, #0
	ldrh	r3, [r5, #30]
	strb	r7, [r5, #5]
	strh	r3, [r5, #38]	@ movhi
	ldrh	r3, [r5, #32]
	strb	r7, [r5, #19]
	strh	r3, [r5, #40]	@ movhi
	ldrh	r3, [r5, #34]
	strh	r3, [r5, #42]	@ movhi
	ldrh	r3, [r5, #36]
	strh	r3, [r5, #44]	@ movhi
	ldr	r3, .L1569+8
	ldrb	r2, [r3, #72]	@ zero_extendqisi2
	ldrb	r0, [r3, #70]	@ zero_extendqisi2
	str	r2, [sp, #16]
	ldrb	r2, [r3, #75]	@ zero_extendqisi2
	ldrh	r1, [r3, #54]
	str	r2, [sp, #28]
	ldrb	r2, [r5, #12]	@ zero_extendqisi2
	str	r0, [sp, #8]
	strb	r2, [r3, #72]
	ldrb	r2, [r5, #13]	@ zero_extendqisi2
	ldrb	r0, [r3, #73]	@ zero_extendqisi2
	strb	r2, [r3, #73]
	ldrb	r2, [r5, #14]	@ zero_extendqisi2
	str	r1, [sp, #12]
	ldrb	r1, [r3, #74]	@ zero_extendqisi2
	strb	r2, [r3, #74]
	ldrb	r2, [r5, #15]	@ zero_extendqisi2
	str	r0, [sp, #20]
	strb	r2, [r3, #75]
	ldr	r0, [r3, #60]
	ldr	r2, [r5, #24]
	str	r1, [sp, #24]
	str	r0, [sp, #32]
	str	r2, [r3, #60]
	ldrh	r6, [r5, #28]
	ldrb	r2, [r5, #7]	@ zero_extendqisi2
	strh	r6, [r3, #54]	@ movhi
	strb	r2, [r3, #70]
	ldrb	r0, [r5, #16]	@ zero_extendqisi2
	bl	SetCurFont
	ldrb	r3, [r5, #8]	@ zero_extendqisi2
	cmp	r3, r7
	cmpne	r3, #11
	bne	.L1505
	ldrsb	r2, [r5, #10]
	ldrb	r1, [r5, #10]	@ zero_extendqisi2
	cmp	r2, r7
	ldr	r2, .L1569+12
.LBB279:
	strltb	r7, [r2, #0]
.LBE279:
	ldr	r7, [r5, #0]
.LBB280:
	strgeb	r1, [r2, #0]
.LBE280:
	mov	r0, r7
	mov	r1, r6
	bl	PrepareText
	b	.L1508
.L1505:
	ldrb	r6, [r5, #9]	@ zero_extendqisi2
	cmp	r6, #9
	bne	.L1509
	ldr	r6, [r5, #0]
	mov	r0, r6
	bl	strlen
	strh	r0, [r5, #28]	@ movhi
	b	.L1510
.L1509:
	mov	r1, r6
	ldr	r0, [r5, #0]
	bl	ReadVar
	mov	r1, r6
	bl	DataNumStr
	ldr	r6, .L1569+16
	strh	r0, [r5, #28]	@ movhi
.L1510:
	ldr	r5, .L1569
	mov	r1, r6
	ldrsb	r3, [r5, #10]
	ldrb	r2, [r5, #10]	@ zero_extendqisi2
	cmp	r3, #0
	ldr	r3, .L1569+12
.LBB281:
	movlt	r2, #0
	strb	r2, [r3, #0]
.LBE281:
	ldr	r0, .L1569+20
	bl	strcpy
	ldr	r0, .L1569+20
	ldrh	r1, [r5, #28]
	bl	PrepareText
	ldr	r7, .L1569+20
.L1508:
	ldr	r3, .L1569
	ldrh	r2, [r3, #28]
	cmp	r2, r4
	bcs	.L1513
	ldr	r4, .L1569+8
	ldrb	r0, [r4, #52]	@ zero_extendqisi2
	bl	SetCurFont
	ldr	r1, [sp, #8]
	ldr	r2, [sp, #12]
	ldr	r3, [sp, #16]
	strb	r1, [r4, #70]
	strh	r2, [r4, #54]	@ movhi
	strb	r3, [r4, #72]
	ldr	r0, [sp, #20]
	ldr	r1, [sp, #24]
	ldr	r2, [sp, #28]
	ldr	r3, [sp, #32]
	strb	r0, [r4, #73]
	strb	r1, [r4, #74]
	strb	r2, [r4, #75]
	str	r3, [r4, #60]
	b	.L1502
.L1513:
	cmp	r4, #0
	beq	.L1514
	add	r2, r7, r4
	ldrb	r2, [r2, #-1]	@ zero_extendqisi2
	cmp	r2, #10
	movne	r2, #1
	strneb	r2, [r3, #5]
	strneh	r4, [r3, #22]	@ movhi
.L1514:
	ldrb	r2, [r3, #8]	@ zero_extendqisi2
	ldr	fp, .L1569
	cmp	r2, #11
	cmpne	r2, #13
	movne	r2, #0
	moveq	r2, #1
	bne	.L1515
	cmp	r4, #0
	moveq	r3, #1
	streqb	r3, [fp, #5]
	streqh	r4, [fp, #22]	@ movhi
	beq	.L1545
	add	r2, sp, #48
	ldrb	r0, [fp, #7]	@ zero_extendqisi2
	ldrh	r1, [fp, #28]
	bl	TextPixelLength
	ldrh	r6, [fp, #50]
	ldr	r3, .L1569+24
	add	r0, sp, #4544
	rsb	r6, r6, #1
	mov	r6, r6, asl #16
	mov	r5, #0
	add	r0, r0, #24
	strh	r5, [r0, r3]	@ movhi
	cmp	r6, r5
	mvn	r2, #0
	add	r3, r3, #256
	strh	r2, [r0, r3]	@ movhi
	movle	r6, r6, lsr #16
	movgt	r6, #1
	mov	sl, #1
	mov	r9, r4
	b	.L1518
.L1520:
	add	r5, r5, #1
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
.L1530:
	rsb	r2, r7, r3
	cmp	r2, r0
	bge	.L1519
	ldrb	r1, [r3], #1	@ zero_extendqisi2
	cmp	r1, #10
	beq	.L1519
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	cmp	r2, #32
	beq	.L1520
	cmp	r1, #32
	beq	.L1519
	cmp	r2, #45
	bne	.L1520
.L1519:
	mov	r8, sl, asl #16
	mov	r8, r8, asr #16
	add	r3, sp, #4544
	sub	r1, r8, #1
	add	r3, r3, #24
	add	ip, r3, r1, asl #1
	sub	r3, ip, #516
	mov	r2, r5, asl #16
	str	r1, [sp, #36]
	add	r1, sp, #4544
	ldrh	r0, [r3, #0]
	mov	r4, r2, lsr #16
	add	r1, r1, #22
	add	r3, sp, #4544
	str	r2, [sp, #44]
	str	r1, [sp, #0]
	add	r2, sp, #48
	add	r3, r3, #20
	mov	r1, r4
	str	ip, [sp, #4]
	bl	CalcCharsWidth
	ldrsh	r2, [fp, #32]
	ldrsh	r3, [fp, #30]
	ldr	ip, [sp, #4]
	rsb	r3, r3, r2
	add	r3, r3, #1
	sub	ip, ip, #260
	cmp	r0, r3
	strleh	r5, [ip, #0]	@ movhi
	ble	.L1528
	ldrsh	r2, [ip, #0]
	ldrh	r3, [ip, #0]
	cmn	r2, #1
	mov	lr, r8, asl #1
	bne	.L1522
	add	r3, sp, #4544
	add	r3, r3, #24
	add	lr, r3, lr
	sub	r3, lr, #516
	add	r1, r4, #1
	sub	lr, lr, #260
	strh	r5, [ip, #0]	@ movhi
	strh	r1, [r3, #0]	@ movhi
	strh	r2, [lr, #0]	@ movhi
	b	.L1523
.L1522:
	add	r1, sp, #4544
	add	r1, r1, #24
	add	r0, r1, lr
	sub	r2, r0, #516
	add	r3, r3, #1
	strh	r3, [r2, #0]	@ movhi
	ldr	r2, [sp, #44]
	mov	ip, r2, asr #16
	b	.L1524
.L1526:
	add	r2, r2, #1
	strh	r2, [r1, #0]	@ movhi
.L1524:
	sub	r1, r0, #516
	ldrh	r2, [r1, #0]
	mov	r3, r2, asl #16
	cmp	ip, r3, asr #16
	ble	.L1525
	ldrb	r3, [r7, r3, asr #16]	@ zero_extendqisi2
	cmp	r3, #32
	beq	.L1526
.L1525:
	add	r3, sp, #4544
	add	r3, r3, #24
	add	lr, r3, lr
	sub	lr, lr, #260
	strh	r5, [lr, #0]	@ movhi
.L1523:
	mov	r3, r6, asl #16
	cmp	r3, #8323072
	bgt	.L1527
	add	r6, r6, #1
	mov	r3, r6, asl #16
	cmp	r3, #65536
	addle	r0, sp, #4544
	addle	r0, r0, #24
	addle	r8, r0, r8, asl #1
	ldrle	r1, [sp, #36]
	suble	r8, r8, #516
	mov	r6, r3, lsr #16
	ldrleh	r3, [r8, #0]
	addle	r2, r0, r1, asl #1
	suble	r2, r2, #516
	movgt	sl, r6
	strleh	r3, [r2, #0]	@ movhi
.L1528:
	mov	r3, r5, asl #16
	ldrb	r3, [r7, r3, asr #16]	@ zero_extendqisi2
	cmp	r3, #10
	bne	.L1529
	mov	r2, sl, asl #16
	mov	r2, r2, asr #16
	add	r0, sp, #4544
	sub	r3, r2, #1
	add	r0, r0, #24
	add	r3, r0, r3, asl #1
	sub	r1, r3, #260
	sub	r0, r4, #1
	strh	r0, [r1, #0]	@ movhi
	add	r1, sp, #4544
	add	r1, r1, #24
	add	r4, r4, #1
	add	r2, r1, r2, asl #1
	mov	r4, r4, asl #16
	sub	r1, r2, #516
	mov	r4, r4, lsr #16
	sub	r2, r2, #260
	strh	r4, [r1, #0]	@ movhi
	mvn	r1, #0
	strh	r1, [r2, #0]	@ movhi
	mov	r2, r6, asl #16
	cmp	r2, #8323072
	bgt	.L1527
	add	r6, r6, #1
	mov	r2, r6, asl #16
	cmp	r2, #65536
	mov	r6, r2, lsr #16
	suble	r3, r3, #516
	movgt	sl, r6
	strleh	r4, [r3, #0]	@ movhi
.L1529:
	add	r5, r5, #1
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
.L1518:
	ldrh	r0, [fp, #28]
	mov	r3, r5, asl #16
	mov	r3, r3, asr #16
	cmp	r3, r0
	addlt	r3, r7, r3
	sublt	r0, r0, #1
	blt	.L1530
.L1527:
	ldr	r3, .L1569
	mov	r4, r9
	ldr	r2, [r3, #24]
	tst	r2, #2048
	beq	.L1531
	mov	r2, #0
	mov	r0, r2
	ldrsh	r8, [r3, #48]
	ldr	lr, .L1569+28
	b	.L1532
.L1535:
	add	r3, sp, #4048
	add	r3, r3, #4
	ldrsh	r7, [r3, r2]
	add	r3, sp, #4288
	add	r3, r3, #20
	ldrsh	r5, [r3, r2]
	mov	r3, #0
	rsb	sl, r7, r5
	add	sl, sl, #1
	add	sl, sl, sl, lsr #31
	add	sl, r7, sl, asr #1
	mov	r1, r3
	mov	ip, r3
	add	r6, lr, r7, asl #2
	add	r5, lr, r5, asl #2
	b	.L1533
.L1534:
	ldr	r9, [r6, r1]
	ldr	fp, [r5, r3]
	add	ip, ip, #1
	str	fp, [r6, r1]
	str	r9, [r5, r3]
	add	r1, r1, #4
	sub	r3, r3, #4
.L1533:
	add	r9, ip, r7
	cmp	sl, r9
	bgt	.L1534
	add	r0, r0, #1
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	add	r2, r2, #2
.L1532:
	mov	r3, r0, asl #16
	cmp	r8, r3, asr #16
	bgt	.L1535
	ldr	r3, .L1569
	add	r2, sp, #48
	ldrb	r0, [r3, #7]	@ zero_extendqisi2
	ldrh	r1, [r3, #28]
	bl	TextPixelLength
.L1531:
	ldr	r2, .L1569
	ldrsh	r3, [r2, #50]
	ldrb	ip, [r2, #52]	@ zero_extendqisi2
	cmp	r3, #0
	movlt	r3, #0
	strlth	r3, [r2, #50]	@ movhi
	mov	ip, ip, asl #24
	ldrh	lr, [r2, #50]
	mov	r0, ip, asr #8
	mov	r0, r0, lsr #16
	ldrh	r1, [r2, #56]
	mul	lr, r0, lr
	ldr	r3, .L1569
	rsb	lr, r1, lr
	ldrh	r1, [r2, #42]
	add	r1, lr, r1
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	strh	r1, [r2, #42]	@ movhi
	ldrb	r2, [r2, #54]	@ zero_extendqisi2
	cmp	r2, #2
	beq	.L1538
	cmp	r2, #3
	bne	.L1537
	b	.L1566
.L1538:
	ldrsh	r2, [r3, #34]
	ldrsh	r0, [r3, #36]
	ldrsh	lr, [r3, #48]
	rsb	r0, r2, r0
	add	r0, r0, #1
	rsb	lr, lr, #1
	mov	ip, ip, asr #24
	ldrb	r2, [r3, #46]	@ zero_extendqisi2
	mla	ip, lr, ip, r0
	rsb	r2, r2, ip
	add	r2, r2, r2, lsr #31
	add	r1, r1, r2, asr #1
	strh	r1, [r3, #42]	@ movhi
.L1537:
	ldr	r3, .L1569
	add	lr, sp, #4288
	ldrsb	r7, [r3, #52]
	ldrh	r0, [r3, #42]
	mov	r7, r7, asl #16
	mov	r7, r7, lsr #16
	add	lr, lr, #18
	mov	r2, #0
	ldrsh	r6, [r3, #48]
	sub	r5, r4, #1
	b	.L1540
.L1566:
	ldrh	r2, [r3, #36]
	ldrh	ip, [r3, #34]
	add	r2, r2, #1
	rsb	r2, ip, r2
	ldrb	ip, [r3, #46]	@ zero_extendqisi2
	rsb	r2, ip, r2
	add	r1, r1, r2
	ldrsh	r2, [r3, #48]
	rsb	r2, r2, #1
	mla	r0, r2, r0, r1
	strh	r0, [r3, #42]	@ movhi
	b	.L1537
.L1546:
	ldrh	r1, [lr, #2]!
	add	r0, r0, r7
	mov	r8, r1, asl #16
	mov	r0, r0, asl #16
	cmp	r5, r8, asr #16
	mov	r3, r2
	mov	r0, r0, lsr #16
	add	r2, r2, #1
	bgt	.L1540
	ldr	r2, .L1569
	strh	ip, [r2, #42]	@ movhi
	ldr	r2, [r2, #24]
	tst	r2, #2048
	beq	.L1541
	add	r0, sp, #4544
	add	r0, r0, #24
	add	r2, r0, r3, asl #1
	sub	r2, r2, #516
	ldrh	r2, [r2, #0]
	rsb	r4, r4, #2
	add	r4, r4, r1
	add	r4, r4, r2
	mov	r4, r4, asl #16
	mov	r4, r4, lsr #16
.L1541:
	ldr	r5, .L1569
	add	r2, sp, #4544
	add	r2, r2, #24
	add	r3, r2, r3, asl #1
	ldrh	r6, [r5, #30]
	sub	r3, r3, #516
	ldrh	r0, [r3, #0]
	add	r3, sp, #4544
	add	r3, r3, #22
	strh	r6, [r5, #38]	@ movhi
	str	r3, [sp, #0]
	add	r3, sp, #4544
	add	r3, r3, #20
	add	r2, sp, #48
	bl	CalcCharsWidth
	ldrb	r3, [r5, #53]	@ zero_extendqisi2
	cmp	r3, #2
	beq	.L1543
	cmp	r3, #3
	bne	.L1542
	b	.L1567
.L1543:
	ldrsh	r2, [r5, #32]
	mov	r3, r6, asl #16
	sub	r3, r2, r3, asr #16
	mov	r0, r0, asl #16
	add	r0, r3, r0, asr #16
	sub	r0, r0, #1
	add	r0, r0, r0, lsr #31
	add	r6, r6, r0, asr #1
	strh	r6, [r5, #38]	@ movhi
	b	.L1542
.L1567:
	ldrh	r3, [r5, #32]
	sub	r3, r3, #1
	add	r0, r0, r3
	strh	r0, [r5, #38]	@ movhi
.L1542:
	ldr	r5, .L1569
	add	r3, sp, #4544
	add	r3, r3, #24
	ldrh	r2, [r3, #-4]!
	ldrh	r6, [r5, #38]
	sub	r0, r4, #1
	rsb	r6, r2, r6
	mov	r0, r0, asl #16
	add	r2, sp, #4544
	mov	r0, r0, lsr #16
	add	r2, r2, #22
	str	r2, [sp, #0]
	mov	r1, r0
	add	r2, sp, #48
	bl	CalcCharsWidth
	add	r0, sp, #4608
	ldrh	r3, [r0, #-42]
	mov	r6, r6, asl #16
	mov	r6, r6, lsr #16
	add	r3, r6, r3
	strh	r3, [r5, #40]	@ movhi
	ldrh	r3, [r0, #-44]
	ldrh	r2, [r5, #42]
	add	r6, r6, r3
	ldrb	r3, [r5, #46]	@ zero_extendqisi2
	strh	r6, [r5, #38]	@ movhi
	add	r3, r2, r3
	sub	r3, r3, #1
	strh	r3, [r5, #44]	@ movhi
	b	.L1545
.L1540:
	mov	r3, r2, asl #16
	cmp	r6, r3, asr #16
	mov	ip, r0
	bgt	.L1546
	ldr	r3, .L1569
	strh	r0, [r3, #42]	@ movhi
	b	.L1545
.L1515:
	ldr	r3, [fp, #24]
	tst	r3, #2048
	beq	.L1547
	ldrh	r1, [fp, #28]
	ldr	r3, .L1569+28
	mov	r0, r2
	mov	lr, r1, lsr #1
	add	ip, r3, r1, asl #2
	b	.L1548
.L1549:
	ldr	r5, [r3, #-4]
	ldr	r6, [ip, r2]
	add	r0, r0, #1
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	str	r6, [r3, #-4]
	str	r5, [ip, r2]
.L1548:
	mov	r5, r0, asl #16
	cmp	lr, r5, asr #16
	add	r3, r3, #4
	sub	r2, r2, #4
	bne	.L1549
	add	r1, r1, #1
	rsb	r4, r4, r1
	mov	r4, r4, asl #16
	mov	r4, r4, lsr #16
.L1547:
	ldr	r5, .L1569
	add	r2, sp, #48
	ldrh	r6, [r5, #28]
	ldrb	r0, [r5, #7]	@ zero_extendqisi2
	mov	r1, r6
	bl	TextPixelLength
	ldrb	r3, [r5, #11]	@ zero_extendqisi2
	cmp	r3, #2
	beq	.L1551
	cmp	r3, #3
	bne	.L1550
	b	.L1568
.L1551:
	cmp	r6, #0
	ldrneh	r2, [r5, #38]
	addne	r3, r0, r0, lsr #31
	subne	r3, r2, r3, asr #1
	bne	.L1564
	b	.L1550
.L1568:
	ldrh	r3, [r5, #38]
	add	r3, r3, #1
	rsb	r3, r0, r3
.L1564:
	strh	r3, [r5, #38]	@ movhi
.L1550:
	ldr	r3, .L1569
	cmp	r4, #0
	ldrh	r2, [r3, #38]
	streqh	r4, [r3, #22]	@ movhi
	sub	r2, r2, #1
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	add	r0, r2, r0
	moveq	r2, #1
	strh	r0, [r3, #40]	@ movhi
	streqb	r2, [r3, #5]
	beq	.L1545
	ldrb	r0, [r3, #7]	@ zero_extendqisi2
	sub	r4, r4, #1
	cmp	r0, #1
	ldr	r1, .L1569+32
	bne	.L1554
	add	r0, sp, #4544
	add	r0, r0, #24
	add	r4, r0, r4, asl #1
	ldrh	r1, [r4, r1]
	add	r2, r2, r1
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	strh	r2, [r3, #38]	@ movhi
	ldrb	r1, [r3, #6]	@ zero_extendqisi2
	b	.L1565
.L1554:
	cmp	r0, #3
	bne	.L1555
	ldr	r0, .L1569+36
	ldrb	r0, [r0, r4]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L1555
	add	r0, sp, #4544
	add	r0, r0, #24
	add	r4, r0, r4, asl #1
	ldrh	r1, [r4, r1]
	ldrb	r0, [r3, #18]	@ zero_extendqisi2
	add	r2, r2, r1
	ldrb	r1, [r3, #17]	@ zero_extendqisi2
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	add	r1, r0, r1
	strh	r2, [r3, #38]	@ movhi
.L1565:
	add	r2, r2, r1
	strh	r2, [r3, #40]	@ movhi
	b	.L1545
.L1555:
	ldr	r3, .L1569+28
	ldr	r1, [r3, r4, asl #2]
	add	r3, sp, #4544
	add	r3, r3, #24
	add	r4, r3, r4, asl #1
	ldr	r3, .L1569+32
	ldrh	r3, [r4, r3]
	add	r2, r2, r3
	ldrb	r3, [r1, #10]	@ zero_extendqisi2
	ldrb	r1, [r1, #11]	@ zero_extendqisi2
	add	r2, r2, r3
	ldr	r3, .L1569
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	strh	r2, [r3, #38]	@ movhi
	add	r1, r1, #1
	b	.L1565
.L1545:
	ldr	r4, .L1569+8
	ldrb	r0, [r4, #52]	@ zero_extendqisi2
	bl	SetCurFont
	ldr	r2, [sp, #16]
	ldr	r3, [sp, #20]
	strb	r2, [r4, #72]
	strb	r3, [r4, #73]
	ldr	r2, [sp, #32]
	ldr	r3, .L1569
	ldr	r0, [sp, #8]
	ldr	r1, [sp, #12]
	str	r2, [r4, #60]
	ldrb	r2, [r3, #5]	@ zero_extendqisi2
	strb	r0, [r4, #70]
	strh	r1, [r4, #54]	@ movhi
	ldr	r0, [sp, #24]
	ldr	r1, [sp, #28]
	cmp	r2, #0
	strb	r0, [r4, #74]
	strb	r1, [r4, #75]
	beq	.L1502
	ldr	r0, [sp, #40]
	ldr	r4, .L1569
	cmp	r0, #0
	ldrgt	r1, [sp, #40]
	movle	r2, #1
	strleb	r2, [r3, #19]
	strgtb	r1, [r3, #19]
	ldrb	r5, [r3, #19]	@ zero_extendqisi2
	cmp	r5, #255
	beq	.L1558
	ldr	r3, .L1569+40
	mov	r1, r5
	ldr	r6, [r3, #0]
	mov	r0, r6
	bl	__umodsi3
	mov	r1, r5
	rsb	r0, r0, r5
	strb	r0, [r4, #20]
	mov	r0, r6
	bl	__udivsi3
	tst	r0, #1
	movne	r3, #0
	moveq	r3, #1
	cmp	r3, #0
	strb	r3, [r4, #21]
	beq	.L1502
	ldr	r3, .L1569+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1502
	ldrsh	r0, [r4, #38]
	ldrsh	r1, [r4, #42]
	ldrsh	r2, [r4, #40]
	ldrsh	r3, [r4, #44]
	bl	GuiLib_InvertBox
	b	.L1502
.L1558:
	mvn	r3, #0
	strb	r3, [r4, #20]
	mov	r3, #0
	strb	r3, [r4, #21]
.L1502:
	add	sp, sp, #472
	add	sp, sp, #4096
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L1570:
	.align	2
.L1569:
	.word	.LANCHOR84
	.word	.LANCHOR69
	.word	.LANCHOR10
	.word	.LANCHOR9
	.word	.LANCHOR57
	.word	.LANCHOR115
	.word	-516
	.word	.LANCHOR7
	.word	-4520
	.word	.LANCHOR6
	.word	.LANCHOR141
.LFE115:
	.size	GuiLib_BlinkBoxMarkedItem, .-GuiLib_BlinkBoxMarkedItem
	.section	.text.GuiLib_BlinkBoxMarkedItemStop,"ax",%progbits
	.align	2
	.global	GuiLib_BlinkBoxMarkedItemStop
	.type	GuiLib_BlinkBoxMarkedItemStop, %function
GuiLib_BlinkBoxMarkedItemStop:
.LFB116:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, lr}
.LCFI109:
	movs	r5, r0, lsr #16
	ldmnefd	sp!, {r4, r5, pc}
	ldr	r4, .L1574
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	cmp	r3, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldrb	r3, [r4, #5]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1573
	ldrb	r3, [r4, #19]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1573
	ldrb	r3, [r4, #21]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1573
	ldr	r3, .L1574+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1573
	ldrsh	r0, [r4, #38]
	ldrsh	r1, [r4, #42]
	ldrsh	r2, [r4, #40]
	ldrsh	r3, [r4, #44]
	bl	GuiLib_InvertBox
	strb	r5, [r4, #21]
.L1573:
	ldr	r3, .L1574
	mov	r2, #0
	strb	r2, [r3, #19]
	strb	r2, [r3, #5]
	ldmfd	sp!, {r4, r5, pc}
.L1575:
	.align	2
.L1574:
	.word	.LANCHOR84
	.word	.LANCHOR69
.LFE116:
	.size	GuiLib_BlinkBoxMarkedItemStop, .-GuiLib_BlinkBoxMarkedItemStop
	.section	.text.GuiLib_BlinkBoxStop,"ax",%progbits
	.align	2
	.global	GuiLib_BlinkBoxStop
	.type	GuiLib_BlinkBoxStop, %function
GuiLib_BlinkBoxStop:
.LFB114:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1579
	stmfd	sp!, {r4, lr}
.LCFI110:
	ldrsh	r2, [r3, #0]
	mov	r4, r3
	cmp	r2, #0
	beq	.L1577
	ldr	r3, .L1579+4
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1577
	bl	BlinkBox
.L1577:
	ldr	r3, .L1579+8
	mov	r0, #0
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	strh	r0, [r4, #0]	@ movhi
	cmp	r2, r0
	ldmeqfd	sp!, {r4, pc}
	ldrb	r3, [r3, #5]	@ zero_extendqisi2
	cmp	r3, r0
	ldmeqfd	sp!, {r4, pc}
	ldmfd	sp!, {r4, lr}
	b	GuiLib_BlinkBoxMarkedItemStop
.L1580:
	.align	2
.L1579:
	.word	.LANCHOR68
	.word	.LANCHOR74
	.word	.LANCHOR84
.LFE114:
	.size	GuiLib_BlinkBoxStop, .-GuiLib_BlinkBoxStop
	.section	.text.GuiLib_BlinkBoxStart,"ax",%progbits
	.align	2
	.global	GuiLib_BlinkBoxStart
	.type	GuiLib_BlinkBoxStart, %function
GuiLib_BlinkBoxStart:
.LFB113:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI111:
	ldrsh	r7, [sp, #24]
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	mov	r6, r2, asr #16
	mov	r8, r3, asr #16
	mov	r4, r0, asr #16
	mov	r5, r1, asr #16
	bl	GuiLib_BlinkBoxStop
	ldr	r3, .L1584
	cmp	r7, #1
	movlt	r7, #1
	strh	r4, [r3, #0]	@ movhi
	ldr	r3, .L1584+4
	ldr	r2, .L1584+8
	strh	r5, [r3, #0]	@ movhi
	ldr	r3, .L1584+12
	strh	r8, [r2, #0]	@ movhi
	strh	r6, [r3, #0]	@ movhi
	ldr	r3, .L1584+16
	mov	r2, #0
	strh	r7, [r3, #0]	@ movhi
	ldr	r3, .L1584+20
	strh	r7, [r3, #0]	@ movhi
	ldr	r3, .L1584+24
	strb	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	BlinkBox
.L1585:
	.align	2
.L1584:
	.word	.LANCHOR70
	.word	.LANCHOR71
	.word	.LANCHOR73
	.word	.LANCHOR72
	.word	.LANCHOR68
	.word	.LANCHOR142
	.word	.LANCHOR74
.LFE113:
	.size	GuiLib_BlinkBoxStart, .-GuiLib_BlinkBoxStart
	.section	.text.GuiLib_BlinkBoxMarkedItemUpdate,"ax",%progbits
	.align	2
	.global	GuiLib_BlinkBoxMarkedItemUpdate
	.type	GuiLib_BlinkBoxMarkedItemUpdate, %function
GuiLib_BlinkBoxMarkedItemUpdate:
.LFB117:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, r0, asl #16
	movs	r0, r0, lsr #16
	bxne	lr
	ldr	r3, .L1588
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	cmp	r2, #0
	bxeq	lr
	ldrb	r2, [r3, #5]	@ zero_extendqisi2
	cmp	r2, #0
	bxeq	lr
	ldrb	r2, [r3, #8]	@ zero_extendqisi2
	cmp	r2, #8
	cmpne	r2, #13
	bxne	lr
	ldrh	r1, [r3, #22]
	ldrb	r2, [r3, #19]	@ zero_extendqisi2
	b	GuiLib_BlinkBoxMarkedItem
.L1589:
	.align	2
.L1588:
	.word	.LANCHOR84
.LFE117:
	.size	GuiLib_BlinkBoxMarkedItemUpdate, .-GuiLib_BlinkBoxMarkedItemUpdate
	.section	.text.GuiLib_InvertBoxStop,"ax",%progbits
	.align	2
	.global	GuiLib_InvertBoxStop
	.type	GuiLib_InvertBoxStop, %function
GuiLib_InvertBoxStop:
.LFB120:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L1592
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	bxeq	lr
	b	InvertBox
.L1593:
	.align	2
.L1592:
	.word	.LANCHOR79
.LFE120:
	.size	GuiLib_InvertBoxStop, .-GuiLib_InvertBoxStop
	.section	.text.GuiLib_InvertBoxStart,"ax",%progbits
	.align	2
	.global	GuiLib_InvertBoxStart
	.type	GuiLib_InvertBoxStart, %function
GuiLib_InvertBoxStart:
.LFB119:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI112:
	mov	r0, r0, asl #16
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	mov	r6, r2, asr #16
	mov	r7, r3, asr #16
	mov	r4, r0, asr #16
	mov	r5, r1, asr #16
	bl	GuiLib_InvertBoxStop
	ldr	r3, .L1595
	ldr	r2, .L1595+4
	strh	r4, [r3, #0]	@ movhi
	ldr	r3, .L1595+8
	strh	r7, [r2, #0]	@ movhi
	strh	r5, [r3, #0]	@ movhi
	ldr	r3, .L1595+12
	strh	r6, [r3, #0]	@ movhi
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	InvertBox
.L1596:
	.align	2
.L1595:
	.word	.LANCHOR75
	.word	.LANCHOR78
	.word	.LANCHOR76
	.word	.LANCHOR77
.LFE119:
	.size	GuiLib_InvertBoxStart, .-GuiLib_InvertBoxStart
	.section	.text.GuiLib_Refresh,"ax",%progbits
	.align	2
	.global	GuiLib_Refresh
	.type	GuiLib_Refresh, %function
GuiLib_Refresh:
.LFB121:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1613
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI113:
	ldr	r2, [r3, #0]
	ldr	r6, .L1613+4
	ldr	r5, .L1613+8
	mov	r4, #0
	add	r2, r2, #1
	mov	r7, r4
	mov	r8, r4
	mov	sl, r4	@ movhi
	str	r2, [r3, #0]
.L1605:
	ldr	r3, [r6, #0]
	tst	r3, #1
	beq	.L1598
	ldr	r3, [r6, #-60]
	cmp	r3, #0
	beq	.L1598
	ldr	r1, .L1613+12
	mov	r2, #100
	ldr	r0, .L1613+8
	mla	r1, r2, r4, r1
	bl	memcpy
	ldr	r3, [r5, #60]
	tst	r3, #64
	beq	.L1599
	ldr	r3, .L1613+16
	ldrh	r0, [r3, #0]
	ldr	r3, .L1613+20
	ldrh	r3, [r3, #0]
	rsb	r0, r3, r0
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bl	ScrollLineAdjustY.constprop.22
.L1599:
	bl	UpdateBackgroundBitmap
	ldr	r3, .L1613+24
	strh	r8, [r3, #0]	@ movhi
	ldr	r3, .L1613+28
	strb	sl, [r3, #0]
	ldr	r3, [r5, #60]
	tst	r3, #256
	beq	.L1600
	ldrsb	r2, [r5, #79]
	cmp	r2, #0
	bgt	.L1600
	ldr	r3, .L1613+32
	mov	r1, #60
	mla	r3, r1, r2, r3
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	cmp	r1, #0
	beq	.L1600
	ldrb	r2, [r3, #5]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1600
	ldrb	r2, [r3, #21]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1600
	ldrsh	r0, [r3, #38]
	ldrsh	r1, [r3, #42]
	ldrsh	r2, [r3, #40]
	ldrsh	r3, [r3, #44]
	bl	GuiLib_InvertBox
.L1600:
	ldr	r3, [r5, #60]
	tst	r3, #64
	bne	.L1601
	ldr	r3, .L1613+36
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1602
	ldr	r3, .L1613+40
	ldrsh	r3, [r3, #0]
	cmn	r3, #1
	beq	.L1602
	ldrsb	r2, [r5, #78]
	cmp	r2, r3
	bne	.L1602
.L1601:
	mov	r0, #1
	b	.L1612
.L1602:
	mov	r0, #0
.L1612:
	bl	DrawItem
	ldr	r3, [r5, #60]
	tst	r3, #256
	beq	.L1604
	ldrsb	r2, [r5, #79]
	cmp	r2, #0
	bgt	.L1604
	ldr	r3, .L1613+32
	mov	r1, #60
	mla	r3, r1, r2, r3
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	cmp	r1, #0
	beq	.L1604
	ldrb	r2, [r3, #5]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1604
	ldrb	r2, [r3, #21]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1604
	ldrsh	r0, [r3, #38]
	ldrsh	r1, [r3, #42]
	ldrsh	r2, [r3, #40]
	ldrsh	r3, [r3, #44]
	bl	GuiLib_InvertBox
.L1604:
	ldr	r3, .L1613+44
	ldrb	r3, [r4, r3]	@ zero_extendqisi2
	cmp	r3, #0
	moveq	r7, #1
.L1598:
	add	r4, r4, #1
	cmp	r4, #128
	add	r6, r6, #100
	bne	.L1605
	ldr	r3, .L1613+48
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r7, #0
	cmpne	r3, #0
	beq	.L1606
	ldr	r3, .L1613+52
	ldr	r0, [r3, #0]
	cmp	r0, #0
	beq	.L1606
	ldr	r1, .L1613+24
	mov	r3, #0
	mov	r2, r3	@ movhi
	strh	r3, [r1, #0]	@ movhi
	ldr	r3, .L1613+28
	mov	r1, #2
	strb	r2, [r3, #0]
	bl	DrawStructure
.L1606:
	ldr	r4, .L1613+32
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1607
	ldrb	r3, [r4, #5]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1607
	ldrb	r3, [r4, #20]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L1607
	sub	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	strb	r3, [r4, #20]
	bne	.L1607
	ldrb	r3, [r4, #19]	@ zero_extendqisi2
	strb	r3, [r4, #20]
	ldr	r3, .L1613+56
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1607
	ldrsh	r3, [r4, #44]
	ldrsh	r0, [r4, #38]
	ldrsh	r1, [r4, #42]
	ldrsh	r2, [r4, #40]
	bl	GuiLib_InvertBox
	ldrb	r3, [r4, #21]	@ zero_extendqisi2
	rsbs	r3, r3, #1
	movcc	r3, #0
	strb	r3, [r4, #21]
.L1607:
	ldr	r3, .L1613+60
	ldrh	r2, [r3, #0]
	cmp	r2, #0
	beq	.L1608
	ldr	r3, .L1613+64
	ldrsh	r0, [r3, #0]
	ldrh	r1, [r3, #0]
	cmp	r0, #254
	suble	r1, r1, #1
	strleh	r1, [r3, #0]	@ movhi
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L1608
	ldr	r3, .L1613+64
	strh	r2, [r3, #0]	@ movhi
	bl	BlinkBox
.L1608:
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	GuiDisplay_Refresh
.L1614:
	.align	2
.L1613:
	.word	.LANCHOR141
	.word	.LANCHOR110+60
	.word	.LANCHOR10
	.word	.LANCHOR110
	.word	.LANCHOR133
	.word	.LANCHOR95
	.word	.LANCHOR122
	.word	.LANCHOR114
	.word	.LANCHOR84
	.word	.LANCHOR102
	.word	.LANCHOR103
	.word	.LANCHOR112
	.word	.LANCHOR113
	.word	.LANCHOR135
	.word	.LANCHOR69
	.word	.LANCHOR68
	.word	.LANCHOR142
.LFE121:
	.size	GuiLib_Refresh, .-GuiLib_Refresh
	.section	.text.GuiLib_DrawChar,"ax",%progbits
	.align	2
	.global	GuiLib_DrawChar
	.type	GuiLib_DrawChar, %function
GuiLib_DrawChar:
.LFB122:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L1619
	mov	r2, r2, asl #16
	ldr	r2, [ip, r2, lsr #14]
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI114:
	ldrb	ip, [r2, #14]	@ zero_extendqisi2
	ldrb	lr, [r2, #0]	@ zero_extendqisi2
	mov	r0, r0, asl #16
	rsb	r1, ip, r1
	mov	r5, r0, asr #16
	and	r0, r3, #255
	mov	r1, r1, asl #16
	cmp	r0, lr
	mov	r6, r1, lsr #16
	ldrb	r3, [sp, #16]	@ zero_extendqisi2
	mov	r1, r1, asr #16
	ldr	ip, .L1619+4
	bcc	.L1616
	ldrb	r4, [r2, #2]	@ zero_extendqisi2
	cmp	r4, r0
	bcs	.L1617
.L1616:
	ldrh	r2, [r2, #8]
	ldr	r4, [ip, r2, asl #2]
	b	.L1618
.L1617:
	ldrh	r2, [r2, #4]
	add	r0, r2, r0
	rsb	lr, lr, r0
	ldr	r4, [ip, lr, asl #2]
.L1618:
	mov	r0, r5
	mov	r2, r4
	bl	DrawChar.isra.1
	ldrb	r0, [r4, #10]	@ zero_extendqisi2
	ldrb	r1, [r4, #12]	@ zero_extendqisi2
	ldrb	r2, [r4, #11]	@ zero_extendqisi2
	ldrb	r3, [r4, #13]	@ zero_extendqisi2
	add	r5, r0, r5
	add	r6, r6, r1
	mov	r0, r5, asl #16
	mov	r1, r6, asl #16
	sub	r2, r2, #1
	sub	r3, r3, #1
	add	r2, r2, r0, lsr #16
	add	r3, r3, r1, lsr #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_MarkDisplayBoxRepaint
.L1620:
	.align	2
.L1619:
	.word	GuiFont_FontList
	.word	GuiFont_ChPtrList
.LFE122:
	.size	GuiLib_DrawChar, .-GuiLib_DrawChar
	.section	.text.GuiLib_DrawStr,"ax",%progbits
	.align	2
	.global	GuiLib_DrawStr
	.type	GuiLib_DrawStr, %function
GuiLib_DrawStr:
.LFB123:
	@ args = 44, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI115:
	sub	sp, sp, #28
.LCFI116:
	mov	sl, r2, asl #16
	ldrsh	r2, [sp, #92]
	mov	r0, r0, asl #16
	str	r2, [sp, #16]
	ldrb	r2, [sp, #96]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	str	r2, [sp, #20]
	ldrb	r2, [sp, #100]	@ zero_extendqisi2
	mov	sl, sl, lsr #16
	mov	r3, r3, asl #24
	mov	r0, r0, asr #16
	mov	r1, r1, asr #16
	cmp	sl, #6
	mov	r7, r3, asr #24
	ldr	r4, [sp, #64]
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	ldrb	r3, [sp, #68]	@ zero_extendqisi2
	ldrb	fp, [sp, #72]	@ zero_extendqisi2
	ldrb	r6, [sp, #76]	@ zero_extendqisi2
	ldrb	r9, [sp, #80]	@ zero_extendqisi2
	ldrsh	r8, [sp, #84]
	ldrsh	ip, [sp, #88]
	str	r2, [sp, #24]
	ldrb	r5, [sp, #104]	@ zero_extendqisi2
	bhi	.L1621
	ldrb	r2, [r4, #0]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1621
	and	sl, sl, #255
	mov	r0, sl
	str	r3, [sp, #4]
	str	ip, [sp, #0]
	bl	SetCurFont
	ldr	r0, .L1629
	mov	r2, #0
	str	r4, [r0, #4]
	strh	r2, [r0, #54]	@ movhi
	ldr	r3, [sp, #4]
	ldr	ip, [sp, #0]
	b	.L1623
.L1624:
	ldrh	r2, [r0, #54]
	add	r2, r2, #1
	strh	r2, [r0, #54]	@ movhi
.L1623:
	ldrb	r1, [r4], #1	@ zero_extendqisi2
	ldr	r2, .L1629
	cmp	r1, #0
	bne	.L1624
	ldr	r0, [sp, #8]
	strb	sl, [r2, #52]
	strh	r0, [r2, #10]	@ movhi
	ldr	r0, [sp, #12]
	strb	r7, [r2, #80]
	strh	r0, [r2, #12]	@ movhi
	ldr	r2, .L1629+4
	cmp	r7, #0
.LBB282:
	strgeb	r7, [r2, #0]
.LBE282:
.LBB283:
	strltb	r1, [r2, #0]
.LBE283:
	ldr	r2, .L1629
	strb	r3, [r2, #64]
	mov	r3, #0
	cmp	r9, r3
	str	r3, [r2, #60]
	strb	fp, [r2, #70]
	mov	r3, r2
	movne	r2, #4
	strne	r2, [r3, #60]
	ldr	r2, [sp, #16]
	cmp	r8, #0
	strb	r2, [r3, #21]
	ldr	r2, [sp, #20]
	strh	r8, [r3, #18]	@ movhi
	strb	ip, [r3, #20]
	strb	r2, [r3, #69]
	ble	.L1628
	mov	r0, r5
	mov	r1, r6
	bl	DrawBackBox
.L1628:
	ldr	r3, .L1629
	ldr	r2, [sp, #24]
	ldrh	r1, [r3, #54]
	str	r6, [sp, #64]
	ldr	r0, [r3, #4]
	mov	r3, r5
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	DrawText
.L1621:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L1630:
	.align	2
.L1629:
	.word	.LANCHOR10
	.word	.LANCHOR9
.LFE123:
	.size	GuiLib_DrawStr, .-GuiLib_DrawStr
	.section	.text.GuiLib_DrawVar,"ax",%progbits
	.align	2
	.global	GuiLib_DrawVar
	.type	GuiLib_DrawVar, %function
GuiLib_DrawVar:
.LFB124:
	@ args = 80, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI117:
	mov	r0, r0, asr #16
	sub	sp, sp, #64
.LCFI118:
	str	r0, [sp, #16]
	ldrb	r0, [sp, #132]	@ zero_extendqisi2
	ldr	ip, [sp, #100]
	str	r0, [sp, #28]
	ldrb	r0, [sp, #136]	@ zero_extendqisi2
	mov	r8, r2, asl #16
	str	r0, [sp, #32]
	ldrb	r0, [sp, #140]	@ zero_extendqisi2
	mov	r1, r1, asl #16
	str	r0, [sp, #36]
	ldrb	r0, [sp, #144]	@ zero_extendqisi2
	mov	r8, r8, lsr #16
	str	r0, [sp, #40]
	ldrb	r0, [sp, #152]	@ zero_extendqisi2
	mov	r1, r1, asr #16
	str	r0, [sp, #44]
	ldrsh	r0, [sp, #160]
	mov	r3, r3, asl #24
	str	r0, [sp, #48]
	ldrsh	r0, [sp, #164]
	cmp	r8, #6
	str	r0, [sp, #52]
	ldrb	r0, [sp, #168]	@ zero_extendqisi2
	str	ip, [sp, #24]
	str	r0, [sp, #56]
	ldrb	r0, [sp, #172]	@ zero_extendqisi2
	str	r1, [sp, #20]
	mov	r6, r3, asr #24
	ldrb	r1, [sp, #104]	@ zero_extendqisi2
	ldrb	r3, [sp, #108]	@ zero_extendqisi2
	ldrb	fp, [sp, #112]	@ zero_extendqisi2
	ldrb	r9, [sp, #116]	@ zero_extendqisi2
	ldrb	sl, [sp, #120]	@ zero_extendqisi2
	ldrb	r2, [sp, #124]	@ zero_extendqisi2
	ldrb	ip, [sp, #128]	@ zero_extendqisi2
	ldrb	r5, [sp, #148]	@ zero_extendqisi2
	ldrsh	r7, [sp, #156]
	str	r0, [sp, #60]
	ldrb	r4, [sp, #176]	@ zero_extendqisi2
	bhi	.L1631
	and	r8, r8, #255
	mov	r0, r8
	stmib	sp, {r2, r3}
	str	r1, [sp, #12]
	str	ip, [sp, #0]
	bl	SetCurFont
	ldr	r1, [sp, #12]
	ldr	r0, .L1646
	ldr	lr, [sp, #24]
	strb	r1, [r0, #81]
	ldr	r3, [sp, #8]
	strb	fp, [r0, #72]
	strb	r3, [r0, #75]
	strb	r9, [r0, #74]
	strb	sl, [r0, #73]
	ldr	r2, [sp, #4]
	ldr	ip, [sp, #0]
	mov	r3, #0
	cmp	r2, r3
	str	r3, [r0, #60]
	movne	r3, #8
	strne	r3, [r0, #60]
	cmp	ip, #0
	ldrne	r3, [r0, #60]
	ldr	r2, [sp, #28]
	orrne	r3, r3, #16
	strne	r3, [r0, #60]
	cmp	r2, #0
	ldrne	r3, [r0, #60]
	ldr	ip, [sp, #16]
	orrne	r3, r3, #4096
	strne	r3, [r0, #60]
	ldr	r3, [sp, #32]
	str	lr, [r0, #0]
	cmp	r3, #0
	ldr	r3, .L1646
	ldrne	r2, [r3, #60]
	strh	ip, [r3, #10]	@ movhi
	ldr	ip, [sp, #20]
	orrne	r2, r2, #8192
	strne	r2, [r3, #60]
	strh	ip, [r3, #12]	@ movhi
	strb	r8, [r3, #52]
	strb	r6, [r3, #80]
	ldr	r3, .L1646+4
	cmp	r6, #0
.LBB284:
	movlt	r2, #0
	strltb	r2, [r3, #0]
.LBE284:
.LBB285:
	strgeb	r6, [r3, #0]
.LBE285:
	ldr	r2, [sp, #36]
	ldr	r3, .L1646
	ldr	ip, [sp, #40]
	strb	r2, [r3, #64]
	ldr	r2, [sp, #44]
	strb	ip, [r3, #70]
	cmp	r2, #0
	ldrne	r2, [r3, #60]
	orrne	r2, r2, #4
	strne	r2, [r3, #60]
	ldr	ip, [sp, #48]
	ldr	r2, [sp, #52]
	strb	ip, [r3, #20]
	ldr	ip, [sp, #56]
	cmp	r7, #0
	strh	r7, [r3, #18]	@ movhi
	strb	r2, [r3, #21]
	strb	ip, [r3, #69]
	ble	.L1640
	mov	r0, r4
	mov	r1, r5
	bl	DrawBackBox
.L1640:
	ldr	r6, .L1646
	ldrb	r1, [r6, #81]	@ zero_extendqisi2
	cmp	r1, #9
	bne	.L1641
	ldr	r6, [r6, #0]
	mov	r0, r6
	bl	strlen
	mov	r1, r0, asl #16
	mov	r1, r1, lsr #16
	b	.L1642
.L1641:
	ldr	r0, [r6, #0]
	bl	ReadVar
	ldrb	r1, [r6, #81]	@ zero_extendqisi2
	bl	DataNumStr
	ldr	r6, .L1646+8
	mov	r1, r0
.L1642:
	ldr	r3, .L1646
	mov	r0, r6
	ldrb	r2, [r3, #80]	@ zero_extendqisi2
	ldrsb	r3, [r3, #80]
	cmp	r3, #0
	ldr	r3, .L1646+4
.LBB286:
	movlt	r2, #0
	strb	r2, [r3, #0]
.LBE286:
	ldr	r2, [sp, #60]
	mov	r3, r4
	str	r5, [sp, #100]
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	DrawText
.L1631:
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L1647:
	.align	2
.L1646:
	.word	.LANCHOR10
	.word	.LANCHOR9
	.word	.LANCHOR57
.LFE124:
	.size	GuiLib_DrawVar, .-GuiLib_DrawVar
	.section	.text.GuiLib_TestPattern,"ax",%progbits
	.align	2
	.global	GuiLib_TestPattern
	.type	GuiLib_TestPattern, %function
GuiLib_TestPattern:
.LFB125:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, #0
	str	lr, [sp, #-4]!
.LCFI119:
	mov	r2, r0
	mov	r3, r0
	mov	r1, #31
	bl	GuiLib_HLine
	mov	r0, #0
	mov	r3, r0
	mov	r1, #1
	mov	r2, #31
	bl	GuiLib_VLine
	mov	r0, #2
	mov	r2, r0
	mov	r1, #8
	mov	r3, #0
	bl	GuiLib_HLine
	mov	r0, #11
	mov	r1, #16
	mov	r2, #2
	mov	r3, #0
	bl	GuiLib_HLine
	mov	r0, #4
	mov	r1, r0
	mov	r2, #10
	mov	r3, #0
	bl	GuiLib_VLine
	mov	r0, #4
	mov	r1, #13
	mov	r2, #18
	mov	r3, #0
	ldr	lr, [sp], #4
	b	GuiLib_VLine
.LFE125:
	.size	GuiLib_TestPattern, .-GuiLib_TestPattern
	.section	.text.GuiLib_ScrollBox_Redraw,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_Redraw
	.type	GuiLib_ScrollBox_Redraw, %function
GuiLib_ScrollBox_Redraw:
.LFB132:
	@ args = 0, pretend = 0, frame = 148
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1759
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI120:
	and	r4, r0, #255
	mov	r5, #268
	mla	r5, r4, r5, r3
	sub	sp, sp, #160
.LCFI121:
	ldrb	r3, [r5, #4]	@ zero_extendqisi2
	cmp	r3, #2
	movne	r0, #0
	bne	.L1650
.LBB289:
	mov	r2, #100
	add	r0, sp, #60
	ldr	r1, .L1759+4
	bl	memcpy
	ldr	r3, .L1759+8
	ldrh	r6, [r5, #128]
	strb	r4, [r3, #0]
	ldrh	r3, [r5, #12]
	ldrh	r0, [r5, #14]
	sub	r8, r6, #1
	add	r8, r8, r3
	mov	r2, r3, asl #16
	mov	r8, r8, asl #16
	cmp	r0, r2, asr #16
	mov	r8, r8, lsr #16
	bne	.L1651
	ldrsh	ip, [r5, #16]
	ldrh	r2, [r5, #16]
	cmn	ip, #1
	beq	.L1652
	ldrsh	r1, [r5, #164]
	ldrh	lr, [r5, #164]
	cmn	r1, #1
	bne	.L1653
.L1652:
	ldr	r1, .L1759
	mov	r0, #268
	mla	r0, r4, r0, r1
	mov	r1, r2, asl #16
	ldrh	ip, [r0, #164]
	ldrsh	r0, [r0, #164]
	mov	r1, r1, asr #16
	cmp	r1, r0
	beq	.L1651
	cmn	r1, #1
	movne	r8, r2
	bne	.L1739
	b	.L1756
.L1653:
	cmp	ip, r1
	bge	.L1654
	cmp	ip, r0
	movge	r3, ip
	movlt	r3, r0
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	mov	r8, lr
	b	.L1651
.L1654:
	ble	.L1651
	add	r0, r0, r6
	cmp	r0, r1
	movgt	r8, r2
	movgt	r3, lr
	bgt	.L1651
	b	.L1757
.L1756:
	mov	r8, ip
.L1739:
	mov	r3, r8
	b	.L1651
.L1757:
	mov	r3, r8
	mov	r8, r2
.L1651:
	ldr	r2, .L1759
	mov	r1, #268
	mla	r2, r1, r4, r2
	ldr	r2, [r2, #8]
	cmp	r2, #0
	bne	.L1732
	b	.L1656
.L1657:
	add	r5, r5, #1
	add	r1, r6, r7
	mov	r0, r4
	mov	r5, r5, asl #16
	bl	ScrollBox_DrawScrollLine
	mov	r5, r5, lsr #16
	add	r6, r6, #1
	b	.L1655
.L1732:
	mov	r8, r8, asl #16
	mov	r7, r3, asl #16
	mov	r5, r3
	mov	r6, #0
	mov	r8, r8, asr #16
	mov	r7, r7, asr #16
.L1655:
	mov	r3, r5, asl #16
	cmp	r8, r3, asr #16
	bge	.L1657
.L1656:
	ldr	r3, .L1759
	mov	r2, #268
	mla	r3, r2, r4, r3
	ldrh	r2, [r3, #12]
	ldrb	r1, [r3, #168]	@ zero_extendqisi2
	strh	r2, [r3, #14]	@ movhi
	ldrh	r2, [r3, #164]
	cmp	r1, #0
	strh	r2, [r3, #16]	@ movhi
	beq	.L1658
	ldrh	r6, [r3, #170]
	ldrh	r1, [r3, #186]
	mov	ip, r6, asl #16
	mov	r0, ip, lsr #16
	str	r0, [sp, #16]
	str	r1, [sp, #44]
	ldrh	r0, [r3, #174]
	ldr	r1, [sp, #16]
	ldrh	r2, [r3, #190]
	add	r0, r1, r0
	sub	r0, r0, #1
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	ldrh	r5, [r3, #172]
	str	r2, [sp, #48]
	str	r0, [sp, #20]
	ldrh	r2, [r3, #176]
	mov	r1, r0
	ldrh	r0, [r3, #188]
	ldrh	r3, [r3, #192]
	str	r0, [sp, #52]
	str	r3, [sp, #56]
	ldr	r3, .L1759+4
	add	r2, r5, r2
	ldrh	r0, [r3, #48]
	sub	r2, r2, #1
	str	r0, [sp, #36]
	ldrh	r0, [r3, #50]
	mov	r2, r2, asl #16
	str	r0, [sp, #40]
	ldrh	r0, [r3, #44]
	mov	ip, ip, asr #16
	str	r0, [sp, #24]
	ldrsh	r0, [r3, #44]
	mov	r2, r2, lsr #16
	cmp	r0, ip
	str	r5, [sp, #28]
	str	r2, [sp, #32]
	ldrh	r8, [r3, #46]
	blt	.L1740
.L1659:
	mov	lr, r1, asl #16
	mov	lr, lr, asr #16
	cmp	lr, ip
	bge	.L1661
	cmp	r0, ip
	ble	.L1660
.L1740:
	strh	r6, [r3, #44]	@ movhi
	b	.L1660
.L1661:
	cmp	r0, lr
	strgth	r1, [r3, #44]	@ movhi
.L1660:
	mov	r0, r8, asl #16
	mov	ip, r5, asl #16
	mov	r0, r0, asr #16
	mov	ip, ip, asr #16
	cmp	r0, ip
	ldr	r3, .L1759+4
	blt	.L1741
.L1662:
	mov	lr, r2, asl #16
	mov	lr, lr, asr #16
	cmp	lr, ip
	bge	.L1664
	cmp	r0, ip
	ble	.L1663
.L1741:
	strh	r5, [r3, #46]	@ movhi
	b	.L1663
.L1664:
	cmp	r0, lr
	strgth	r2, [r3, #46]	@ movhi
.L1663:
	ldr	r3, .L1759+4
	mov	ip, r6, asl #16
	ldrsh	r0, [r3, #48]
	mov	ip, ip, asr #16
	cmp	r0, ip
	blt	.L1742
.L1665:
	mov	lr, r1, asl #16
	mov	lr, lr, asr #16
	cmp	lr, ip
	bge	.L1667
	cmp	r0, ip
	ble	.L1666
.L1742:
	strh	r6, [r3, #48]	@ movhi
	b	.L1666
.L1667:
	cmp	r0, lr
	strgth	r1, [r3, #48]	@ movhi
.L1666:
	ldrsh	r3, [r3, #50]
	mov	r0, r5, asl #16
	mov	r0, r0, asr #16
	cmp	r3, r0
	ldr	r1, .L1759+4
	blt	.L1743
.L1668:
	mov	ip, r2, asl #16
	mov	ip, ip, asr #16
	cmp	ip, r0
	bge	.L1670
	cmp	r3, r0
	ble	.L1669
.L1743:
	strh	r5, [r1, #50]	@ movhi
	b	.L1669
.L1670:
	cmp	r3, ip
	strgth	r2, [r1, #50]	@ movhi
.L1669:
	ldr	r7, .L1759+4
	mov	r9, #268
	ldr	fp, .L1759
	mul	r9, r4, r9
	ldrsh	r0, [r7, #44]
	ldrsh	r1, [r7, #46]
	ldrsh	r2, [r7, #48]
	ldrsh	r3, [r7, #50]
	bl	GuiLib_SetClipping
	add	lr, fp, r9
	ldr	r3, .L1759+12
	ldrh	ip, [lr, #178]
	add	r2, lr, #180
	cmp	ip, r3
	mov	sl, r5, asl #16
	mov	r3, r6, asl #16
	mov	r3, r3, asr #16
	mov	sl, sl, asr #16
	ldrb	r0, [r2, #2]	@ zero_extendqisi2
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	beq	.L1671
	str	sl, [sp, #0]
	ldrsh	r2, [lr, #174]
	ldr	sl, .L1759+16
	str	r2, [sp, #4]
	ldrsh	r2, [lr, #176]
	str	ip, [sp, #12]
	str	r2, [sp, #8]
	mov	r2, #0
	bl	ScrollBox_ShowBarBlock
	ldrh	r3, [sl, #0]
	add	r1, r9, #24
	add	r3, r3, #1
	strh	r3, [sl, #0]	@ movhi
	add	r1, fp, r1
	mov	r2, #100
	mov	r0, r7
	bl	memcpy
	ldr	r3, .L1759+20
	ldr	ip, [sp, #12]
	strh	r6, [r7, #22]	@ movhi
	ldr	r0, [r3, ip, asl #2]
	strh	r5, [r7, #24]	@ movhi
	mov	r1, #0
	bl	DrawStructure
	ldrh	r3, [sl, #0]
	sub	r3, r3, #1
	strh	r3, [sl, #0]	@ movhi
	b	.L1672
.L1671:
	ldrb	r2, [lr, #184]	@ zero_extendqisi2
	str	sl, [sp, #0]
	ldrsh	ip, [lr, #174]
	str	ip, [sp, #4]
	ldrsh	ip, [lr, #176]
	str	ip, [sp, #8]
	bl	ScrollBox_ShowBarBlock
.L1672:
	ldr	r5, .L1759+4
	ldr	ip, [sp, #24]
	mov	r1, r8, asl #16
	strh	ip, [r5, #44]	@ movhi
	ldr	ip, [sp, #36]
	strh	r8, [r5, #46]	@ movhi
	strh	ip, [r5, #48]	@ movhi
	ldr	ip, [sp, #40]
	mov	r1, r1, asr #16
	strh	ip, [r5, #50]	@ movhi
	ldr	ip, [sp, #24]
	mov	r0, ip, asl #16
	ldr	ip, [sp, #36]
	mov	r0, r0, asr #16
	mov	r2, ip, asl #16
	ldr	ip, [sp, #40]
	mov	r2, r2, asr #16
	mov	r3, ip, asl #16
	mov	r3, r3, asr #16
	bl	GuiLib_SetClipping
	ldr	r3, .L1759
	mov	r2, #268
	mla	r3, r2, r4, r3
	ldrh	r2, [r3, #20]
	ldrh	r3, [r3, #128]
	cmp	r2, r3
	bls	.L1658
	ldr	ip, [sp, #16]
	ldr	r0, [sp, #44]
	ldr	r1, [sp, #52]
	add	r7, ip, r0
	ldr	ip, [sp, #20]
	mov	r7, r7, asl #16
	rsb	r0, r1, ip
	ldrh	ip, [r5, #48]
	mov	r7, r7, lsr #16
	str	ip, [sp, #20]
	ldrh	ip, [r5, #50]
	ldrsh	r3, [r5, #44]
	str	ip, [sp, #24]
	mov	r2, r7, asl #16
	ldrh	ip, [r5, #44]
	mov	r2, r2, asr #16
	mov	r0, r0, asl #16
	cmp	r3, r2
	mov	r6, r7
	mov	r0, r0, lsr #16
	ldrh	r8, [r5, #46]
	str	ip, [sp, #16]
	blt	.L1744
.L1673:
	mov	r1, r0, asl #16
	mov	r1, r1, asr #16
	cmp	r1, r2
	bge	.L1675
	cmp	r3, r2
	ble	.L1674
.L1744:
	strh	r6, [r5, #44]	@ movhi
	b	.L1674
.L1675:
	cmp	r3, r1
	strgth	r0, [r5, #44]	@ movhi
.L1674:
	ldr	ip, [sp, #28]
	ldr	r1, [sp, #48]
	add	r5, ip, r1
	ldr	ip, [sp, #32]
	ldr	r1, [sp, #56]
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
	rsb	r2, r1, ip
	mov	lr, r5, asl #16
	mov	ip, r8, asl #16
	mov	ip, ip, asr #16
	mov	lr, lr, asr #16
	mov	r2, r2, asl #16
	cmp	ip, lr
	mov	r3, r5
	mov	r2, r2, lsr #16
	ldr	r1, .L1759+4
	blt	.L1745
.L1676:
	mov	sl, r2, asl #16
	mov	sl, sl, asr #16
	cmp	sl, lr
	bge	.L1678
	cmp	ip, lr
	ble	.L1677
.L1745:
	strh	r3, [r1, #46]	@ movhi
	b	.L1677
.L1678:
	cmp	ip, sl
	strgth	r2, [r1, #46]	@ movhi
.L1677:
	ldr	r1, .L1759+4
	mov	lr, r6, asl #16
	ldrsh	ip, [r1, #48]
	mov	lr, lr, asr #16
	cmp	ip, lr
	blt	.L1746
.L1679:
	mov	sl, r0, asl #16
	mov	sl, sl, asr #16
	cmp	sl, lr
	bge	.L1681
	cmp	ip, lr
	ble	.L1680
.L1746:
	strh	r6, [r1, #48]	@ movhi
	b	.L1680
.L1681:
	cmp	ip, sl
	strgth	r0, [r1, #48]	@ movhi
.L1680:
	ldrsh	r1, [r1, #50]
	mov	ip, r3, asl #16
	mov	ip, ip, asr #16
	cmp	r1, ip
	ldr	r0, .L1759+4
	blt	.L1747
.L1682:
	mov	lr, r2, asl #16
	mov	lr, lr, asr #16
	cmp	lr, ip
	bge	.L1684
	cmp	r1, ip
	ble	.L1683
.L1747:
	strh	r3, [r0, #50]	@ movhi
	b	.L1683
.L1684:
	cmp	r1, lr
	strgth	r2, [r0, #50]	@ movhi
.L1683:
	ldr	r3, .L1759+4
	ldrsh	r0, [r3, #44]
	ldrsh	r1, [r3, #46]
	ldrsh	r2, [r3, #48]
	ldrsh	r3, [r3, #50]
	bl	GuiLib_SetClipping
	ldr	r3, .L1759
	mov	r0, #268
	mla	r2, r0, r4, r3
	ldrh	r1, [r2, #192]
	ldrh	fp, [r2, #176]
	ldrb	r9, [r2, #168]	@ zero_extendqisi2
	rsb	fp, r1, fp
	ldrh	r1, [r2, #190]
	rsb	fp, r1, fp
	mov	fp, fp, asl #16
	sub	r1, r9, #1
	mov	fp, fp, lsr #16
	cmp	r1, #3
	ldrls	pc, [pc, r1, asl #2]
	b	.L1733
.L1690:
	.word	.L1686
	.word	.L1687
	.word	.L1688
	.word	.L1689
.L1686:
	mla	r3, r0, r4, r3
	ldrb	r2, [r3, #196]	@ zero_extendqisi2
	ldr	r3, .L1759+24
	ldr	r3, [r3, r2, asl #2]
	ldr	r2, .L1759+28
	ldrb	sl, [r3, #11]	@ zero_extendqisi2
	str	r3, [r2, #0]
	b	.L1685
.L1687:
	mla	r3, r0, r4, r3
	ldrh	sl, [r3, #208]
	b	.L1685
.L1688:
	mla	r3, r0, r4, r3
	ldrh	sl, [r3, #174]
	ldrh	r2, [r3, #186]
	ldrh	r3, [r3, #188]
	rsb	sl, r2, sl
	rsb	sl, r3, sl
	mov	sl, sl, asl #16
	mov	sl, sl, lsr #16
	b	.L1685
.L1689:
	mov	r1, #268
	mla	r3, r1, r4, r3
	mov	r2, fp, asl #16
	ldrh	r0, [r3, #128]
	mov	r2, r2, asr #16
	add	r2, r2, r2, asl #2
	mov	r2, r2, asl #1
	mul	r0, r2, r0
	ldrh	r1, [r3, #20]
	add	r0, r0, #5
	bl	__divsi3
	mov	r1, #10
	bl	__divsi3
	mov	sl, r0, asl #16
	cmp	sl, #196608
	movgt	sl, sl, lsr #16
	movle	sl, #4
	b	.L1685
.L1733:
	mov	sl, #0
.L1685:
	ldr	r3, .L1759
	mov	r2, #268
	mla	r3, r2, r4, r3
	ldrh	r1, [r3, #20]
	ldrh	r2, [r3, #128]
	ldrsh	r3, [r3, #12]
	rsb	r1, r2, r1
	cmp	r3, #0
	blt	.L1736
	movle	r2, #0
	movgt	r2, #1
	ands	r2, r2, r1, lsr #31
	bne	.L1736
	cmp	r1, #0
	cmpge	r3, r1
	movgt	r3, r1
	b	.L1691
.L1736:
	mov	r3, #0
.L1691:
	mov	r2, sl, asl #16
	mov	fp, fp, asl #16
	mov	r2, r2, asr #16
	rsb	r2, r2, fp, asr #16
	mov	fp, #10
	mul	r2, fp, r2
	sub	r9, r9, #1
	mul	r3, r2, r3
	add	r0, r3, #5
	bl	__divsi3
	mov	r1, fp
	bl	__divsi3
	ldr	r1, .L1759
	mov	ip, #268
	mov	r2, r0, asl #16
	mov	r2, r2, lsr #16
	cmp	r9, #3
	ldrls	pc, [pc, r9, asl #2]
	b	.L1692
.L1696:
	.word	.L1693
	.word	.L1694
	.word	.L1695
	.word	.L1695
.L1693:
	mla	ip, r4, ip, r1
	ldr	r3, .L1759+4
	ldrh	r1, [ip, #198]
	add	r7, r7, r1
	ldr	r1, .L1759+28
	strh	r7, [r3, #10]	@ movhi
	ldr	r1, [r1, #0]
	ldrb	r0, [r1, #14]	@ zero_extendqisi2
	ldrh	r1, [ip, #200]
	add	r1, r0, r1
	add	r5, r5, r1
	add	r2, r5, r2
	strh	r2, [r3, #12]	@ movhi
	ldrb	r2, [ip, #196]	@ zero_extendqisi2
	ldrb	r0, [ip, #203]	@ zero_extendqisi2
	strb	r2, [r3, #52]
	mov	r1, #1
	mov	r2, #0
	strb	r1, [r3, #64]
	strb	r1, [r3, #70]
	str	r2, [r3, #60]
	strh	r2, [r3, #18]	@ movhi
	strb	r2, [r3, #69]
	ldrb	r2, [ip, #204]	@ zero_extendqisi2
	ldrb	r3, [ip, #205]	@ zero_extendqisi2
	str	r0, [sp, #0]
	ldr	r0, [ip, #212]
	bl	DrawText
	b	.L1692
.L1694:
	mla	ip, r4, ip, r1
	ldr	r1, .L1759
	ldrb	r3, [ip, #210]	@ zero_extendqisi2
	mov	r0, #268
	mla	r0, r4, r0, r1
	add	r2, r5, r2
	cmp	r3, #0
	add	ip, ip, #208
	mov	r6, r6, asl #16
	mov	r2, r2, asl #16
	ldrneb	r3, [ip, #3]	@ zero_extendqisi2
	mvneq	r3, #0
	ldrh	r0, [r0, #206]
	mov	r1, r6, asr #16
	mov	r2, r2, asr #16
	bl	GuiLib_ShowBitmap
	b	.L1692
.L1695:
	mla	ip, r4, ip, r1
	add	r1, r5, r2
	ldrb	r3, [ip, #203]	@ zero_extendqisi2
	ldrh	r2, [ip, #174]
	mov	r0, r6, asl #16
	cmp	r3, #0
	mov	r0, r0, asr #16
	mov	r1, r1, asl #16
	sub	r2, r2, #1
	beq	.L1698
	ldrh	r3, [ip, #188]
	rsb	r2, r3, r2
	ldrh	r3, [ip, #186]
	add	r2, r7, r2
	rsb	r2, r3, r2
	sub	r3, sl, #1
	add	r3, r3, r1, lsr #16
	ldrb	ip, [ip, #204]	@ zero_extendqisi2
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	str	ip, [sp, #0]
	bl	GuiLib_Box
	b	.L1692
.L1698:
	ldrh	r3, [ip, #186]
	ldrb	lr, [ip, #204]	@ zero_extendqisi2
	rsb	r2, r3, r2
	ldrh	r3, [ip, #188]
	add	r2, r7, r2
	rsb	r2, r3, r2
	sub	r3, sl, #1
	add	r3, r3, r1, lsr #16
	str	lr, [sp, #0]
	ldrb	ip, [ip, #205]	@ zero_extendqisi2
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	mov	r1, r1, asr #16
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	str	ip, [sp, #4]
	bl	GuiLib_BorderBox
.L1692:
	ldr	r3, .L1759+4
	ldr	ip, [sp, #16]
	strh	r8, [r3, #46]	@ movhi
	strh	ip, [r3, #44]	@ movhi
	ldr	ip, [sp, #20]
	mov	r1, r8, asl #16
	strh	ip, [r3, #48]	@ movhi
	ldr	ip, [sp, #24]
	mov	r1, r1, asr #16
	strh	ip, [r3, #50]	@ movhi
	ldr	ip, [sp, #16]
	mov	r0, ip, asl #16
	ldr	ip, [sp, #20]
	mov	r0, r0, asr #16
	mov	r2, ip, asl #16
	ldr	ip, [sp, #24]
	mov	r2, r2, asr #16
	mov	r3, ip, asl #16
	mov	r3, r3, asr #16
	bl	GuiLib_SetClipping
.L1658:
	ldr	r3, .L1759
	mov	r2, #268
	mla	r3, r2, r4, r3
	ldrb	r1, [r3, #216]	@ zero_extendqisi2
	cmp	r1, #0
	beq	.L1699
	ldrh	r2, [r3, #238]
	ldrh	r6, [r3, #218]
	str	r2, [sp, #48]
	ldrh	r2, [r3, #222]
	mov	ip, r6, asl #16
	mov	r0, ip, lsr #16
	add	r2, r0, r2
	ldrh	r1, [r3, #234]
	sub	r2, r2, #1
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	ldrh	r5, [r3, #220]
	str	r0, [sp, #16]
	str	r1, [sp, #44]
	ldrh	r0, [r3, #236]
	str	r2, [sp, #20]
	mov	r1, r2
	ldrh	r2, [r3, #224]
	ldrh	r3, [r3, #240]
	str	r0, [sp, #52]
	str	r3, [sp, #56]
	ldr	r3, .L1759+4
	add	r2, r5, r2
	ldrh	r0, [r3, #48]
	sub	r2, r2, #1
	str	r0, [sp, #36]
	ldrh	r0, [r3, #50]
	mov	r2, r2, asl #16
	str	r0, [sp, #40]
	ldrh	r0, [r3, #44]
	mov	ip, ip, asr #16
	str	r0, [sp, #24]
	ldrsh	r0, [r3, #44]
	mov	r2, r2, lsr #16
	cmp	r0, ip
	str	r5, [sp, #28]
	str	r2, [sp, #32]
	ldrh	r8, [r3, #46]
	blt	.L1748
.L1700:
	mov	lr, r1, asl #16
	mov	lr, lr, asr #16
	cmp	lr, ip
	bge	.L1702
	cmp	r0, ip
	ble	.L1701
.L1748:
	strh	r6, [r3, #44]	@ movhi
	b	.L1701
.L1702:
	cmp	r0, lr
	strgth	r1, [r3, #44]	@ movhi
.L1701:
	mov	r0, r8, asl #16
	mov	ip, r5, asl #16
	mov	r0, r0, asr #16
	mov	ip, ip, asr #16
	cmp	r0, ip
	ldr	r3, .L1759+4
	blt	.L1749
.L1703:
	mov	lr, r2, asl #16
	mov	lr, lr, asr #16
	cmp	lr, ip
	bge	.L1705
	cmp	r0, ip
	ble	.L1704
.L1749:
	strh	r5, [r3, #46]	@ movhi
	b	.L1704
.L1705:
	cmp	r0, lr
	strgth	r2, [r3, #46]	@ movhi
.L1704:
	ldr	r3, .L1759+4
	mov	ip, r6, asl #16
	ldrsh	r0, [r3, #48]
	mov	ip, ip, asr #16
	cmp	r0, ip
	blt	.L1750
.L1706:
	mov	lr, r1, asl #16
	mov	lr, lr, asr #16
	cmp	lr, ip
	bge	.L1708
	cmp	r0, ip
	ble	.L1707
.L1750:
	strh	r6, [r3, #48]	@ movhi
	b	.L1707
.L1708:
	cmp	r0, lr
	strgth	r1, [r3, #48]	@ movhi
.L1707:
	ldrsh	r3, [r3, #50]
	mov	r0, r5, asl #16
	mov	r0, r0, asr #16
	cmp	r3, r0
	ldr	r1, .L1759+4
	blt	.L1751
.L1709:
	mov	ip, r2, asl #16
	mov	ip, ip, asr #16
	cmp	ip, r0
	bge	.L1711
	cmp	r3, r0
	ble	.L1710
.L1751:
	strh	r5, [r1, #50]	@ movhi
	b	.L1710
.L1711:
	cmp	r3, ip
	strgth	r2, [r1, #50]	@ movhi
.L1710:
	ldr	r7, .L1759+4
	mov	r9, #268
	ldr	fp, .L1759
	mul	r9, r4, r9
	ldrsh	r0, [r7, #44]
	ldrsh	r1, [r7, #46]
	ldrsh	r2, [r7, #48]
	ldrsh	r3, [r7, #50]
	bl	GuiLib_SetClipping
	add	lr, fp, r9
	ldr	r3, .L1759+12
	ldrh	ip, [lr, #226]
	add	r2, lr, #228
	cmp	ip, r3
	mov	sl, r5, asl #16
	mov	r3, r6, asl #16
	mov	r3, r3, asr #16
	mov	sl, sl, asr #16
	ldrb	r0, [r2, #2]	@ zero_extendqisi2
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	beq	.L1712
	str	sl, [sp, #0]
	ldrsh	r2, [lr, #222]
	ldr	sl, .L1759+16
	str	r2, [sp, #4]
	ldrsh	r2, [lr, #224]
	str	ip, [sp, #12]
	str	r2, [sp, #8]
	mov	r2, #0
	bl	ScrollBox_ShowBarBlock
	ldrh	r3, [sl, #0]
	add	r1, r9, #24
	add	r3, r3, #1
	strh	r3, [sl, #0]	@ movhi
	add	r1, fp, r1
	mov	r2, #100
	mov	r0, r7
	bl	memcpy
	ldr	r3, .L1759+20
	ldr	ip, [sp, #12]
	strh	r6, [r7, #22]	@ movhi
	ldr	r0, [r3, ip, asl #2]
	strh	r5, [r7, #24]	@ movhi
	mov	r1, #0
	bl	DrawStructure
	ldrh	r3, [sl, #0]
	sub	r3, r3, #1
	strh	r3, [sl, #0]	@ movhi
	b	.L1713
.L1712:
	ldrb	r2, [lr, #232]	@ zero_extendqisi2
	str	sl, [sp, #0]
	ldrsh	ip, [lr, #222]
	str	ip, [sp, #4]
	ldrsh	ip, [lr, #224]
	str	ip, [sp, #8]
	bl	ScrollBox_ShowBarBlock
.L1713:
	ldr	r5, .L1759+4
	ldr	ip, [sp, #24]
	mov	r1, r8, asl #16
	strh	ip, [r5, #44]	@ movhi
	ldr	ip, [sp, #36]
	strh	r8, [r5, #46]	@ movhi
	strh	ip, [r5, #48]	@ movhi
	ldr	ip, [sp, #40]
	mov	r1, r1, asr #16
	strh	ip, [r5, #50]	@ movhi
	ldr	ip, [sp, #24]
	mov	r0, ip, asl #16
	ldr	ip, [sp, #36]
	mov	r0, r0, asr #16
	mov	r2, ip, asl #16
	ldr	ip, [sp, #40]
	mov	r2, r2, asr #16
	mov	r3, ip, asl #16
	mov	r3, r3, asr #16
	bl	GuiLib_SetClipping
	ldr	r3, .L1759
	mov	r2, #268
	mla	r2, r4, r2, r3
	add	r3, r2, #256
	ldrsh	r3, [r3, #2]
	cmp	r3, #0
	blt	.L1699
	ldrsh	r1, [r2, #12]
	cmp	r3, r1
	blt	.L1699
	ldrh	r2, [r2, #128]
	add	r1, r1, r2
	cmp	r3, r1
	bge	.L1699
	ldr	ip, [sp, #16]
	ldr	r0, [sp, #44]
	ldrsh	r2, [r5, #44]
	add	r8, ip, r0
	mov	r8, r8, asl #16
	ldr	ip, [sp, #20]
	ldr	r0, [sp, #52]
	mov	r8, r8, lsr #16
	mov	r1, r8, asl #16
	rsb	r3, r0, ip
	mov	r1, r1, asr #16
	mov	r3, r3, asl #16
	cmp	r2, r1
	mov	r6, r8
	mov	r3, r3, lsr #16
	blt	.L1752
.L1714:
	mov	r0, r3, asl #16
	mov	r0, r0, asr #16
	cmp	r0, r1
	bge	.L1716
	cmp	r2, r1
	ble	.L1715
.L1752:
	strh	r6, [r5, #44]	@ movhi
	b	.L1715
.L1716:
	cmp	r2, r0
	strgth	r3, [r5, #44]	@ movhi
.L1715:
	ldr	ip, [sp, #28]
	ldr	r0, [sp, #48]
	add	r7, ip, r0
	ldr	ip, [sp, #32]
	ldr	r0, [sp, #56]
	mov	r7, r7, asl #16
	rsb	r1, r0, ip
	ldr	r0, .L1759+4
	mov	r7, r7, lsr #16
	ldrsh	ip, [r0, #46]
	mov	lr, r7, asl #16
	mov	lr, lr, asr #16
	mov	r1, r1, asl #16
	cmp	ip, lr
	mov	r2, r7
	mov	r1, r1, lsr #16
	blt	.L1753
.L1717:
	mov	r5, r1, asl #16
	mov	r5, r5, asr #16
	cmp	r5, lr
	bge	.L1719
	cmp	ip, lr
	ble	.L1718
.L1753:
	strh	r2, [r0, #46]	@ movhi
	b	.L1718
.L1719:
	cmp	ip, r5
	strgth	r1, [r0, #46]	@ movhi
.L1718:
	ldrsh	r0, [r0, #48]
	mov	lr, r6, asl #16
	mov	lr, lr, asr #16
	cmp	r0, lr
	ldr	ip, .L1759+4
	blt	.L1754
.L1720:
	mov	r5, r3, asl #16
	mov	r5, r5, asr #16
	cmp	r5, lr
	bge	.L1722
	cmp	r0, lr
	ble	.L1721
.L1754:
	strh	r6, [ip, #48]	@ movhi
	b	.L1721
.L1722:
	cmp	r0, r5
	strgth	r3, [ip, #48]	@ movhi
.L1721:
	ldr	r3, .L1759+4
	mov	ip, r2, asl #16
	ldrsh	r0, [r3, #50]
	mov	ip, ip, asr #16
	cmp	r0, ip
	blt	.L1755
.L1723:
	mov	lr, r1, asl #16
	mov	lr, lr, asr #16
	cmp	lr, ip
	bge	.L1725
	cmp	r0, ip
	ble	.L1724
.L1755:
	strh	r2, [r3, #50]	@ movhi
	b	.L1724
.L1725:
	cmp	r0, lr
	strgth	r1, [r3, #50]	@ movhi
.L1724:
	ldrsh	r0, [r3, #44]
	ldrsh	r1, [r3, #46]
	ldrsh	r2, [r3, #48]
	ldrsh	r3, [r3, #50]
	bl	GuiLib_SetClipping
	ldr	r3, .L1759
	mov	r0, #268
	mla	r0, r4, r0, r3
	ldr	r5, .L1759+4
	ldrb	r1, [r0, #216]	@ zero_extendqisi2
	mov	r2, r3
	cmp	r1, #1
	beq	.L1726
	cmp	r1, #2
	bne	.L1699
	b	.L1758
.L1726:
	ldrh	r3, [r0, #244]
	ldr	r2, .L1759+24
	ldr	ip, .L1759+28
	ldr	r2, [r2, r3, asl #2]
	str	r2, [ip, #0]
	ldrh	ip, [r0, #246]
	add	r8, r8, ip
	ldrb	ip, [r2, #14]	@ zero_extendqisi2
	ldrh	r2, [r0, #248]
	strb	r3, [r5, #52]
	add	r2, ip, r2
	add	ip, r0, #256
	add	r2, r7, r2
	ldrsh	r7, [ip, #2]
	ldrsh	ip, [r0, #12]
	mov	r3, #0
	rsb	r7, ip, r7
	ldrh	ip, [r0, #130]
	str	r3, [r5, #60]
	mla	r7, ip, r7, r2
	ldrb	ip, [r0, #251]	@ zero_extendqisi2
	strh	r3, [r5, #18]	@ movhi
	strb	r3, [r5, #69]
	ldrb	r2, [r0, #252]	@ zero_extendqisi2
	ldrb	r3, [r0, #253]	@ zero_extendqisi2
	strh	r8, [r5, #10]	@ movhi
	strh	r7, [r5, #12]	@ movhi
	strb	r1, [r5, #64]
	strb	r1, [r5, #70]
	str	ip, [sp, #0]
	ldr	r0, [r0, #260]
	bl	DrawText
	b	.L1699
.L1758:
	mov	r1, #268
	mla	r4, r1, r4, r2
	ldrb	r3, [r0, #256]	@ zero_extendqisi2
	add	r2, r4, #256
	cmp	r3, #0
	ldrneb	r3, [r0, #257]	@ zero_extendqisi2
	ldrsh	r0, [r2, #2]
	ldrsh	r2, [r4, #12]
	mov	r1, r6, asl #16
	rsb	r2, r2, r0
	ldrh	r0, [r4, #130]
	mvneq	r3, #0
	mla	r7, r0, r2, r7
	mov	r1, r1, asr #16
	mov	r2, r7, asl #16
	ldrh	r0, [r4, #254]
	mov	r2, r2, asr #16
	bl	GuiLib_ShowBitmap
.L1699:
	ldr	r0, .L1759+4
	add	r1, sp, #60
	mov	r2, #100
	bl	memcpy
	mov	r0, #1
.L1650:
.LBE289:
	add	sp, sp, #160
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L1760:
	.align	2
.L1759:
	.word	.LANCHOR41
	.word	.LANCHOR10
	.word	.LANCHOR40
	.word	65535
	.word	.LANCHOR122
	.word	GuiStruct_StructPtrList
	.word	GuiFont_FontList
	.word	.LANCHOR8
.LFE132:
	.size	GuiLib_ScrollBox_Redraw, .-GuiLib_ScrollBox_Redraw
	.section	.text.GuiLib_ScrollBox_Init,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_Init
	.type	GuiLib_ScrollBox_Init, %function
GuiLib_ScrollBox_Init:
.LFB131:
	@ args = 0, pretend = 0, frame = 124
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, r3, asl #16
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI122:
	mov	r5, r3, asr #16
	sub	sp, sp, #124
.LCFI123:
	mov	r2, r2, asl #16
	cmn	r5, #1
	str	r1, [sp, #20]
	and	r4, r0, #255
	mov	r6, r2, asr #16
	blt	.L1771
	mov	r3, r6, lsr #31
	cmp	r5, #0
	movlt	r3, #0
	andge	r3, r3, #1
	cmp	r3, #0
	bne	.L1771
	cmp	r6, r5
	movgt	r3, #0
	movle	r3, #1
	cmp	r6, #0
	movlt	r3, #0
	cmp	r3, #0
	subne	r5, r6, #1
	movne	r5, r5, asl #16
	movne	r5, r5, asr #16
	b	.L1762
.L1771:
	mvn	r5, #0
.L1762:
	mov	sl, #268
	ldr	r9, .L1773
	mul	sl, r4, sl
	add	r8, r9, sl
	ldrb	r7, [r8, #4]	@ zero_extendqisi2
	cmp	r7, #1
	movne	r0, #0
	bne	.L1763
	ldr	r1, .L1773+4
	mov	r2, #100
	add	r0, sp, #24
	bl	memcpy
	ldr	r3, .L1773+8
	add	sl, sl, #24
	add	sl, r9, sl
	strb	r4, [r3, #0]
	mov	r2, #100
	ldr	r0, .L1773+4
	mov	r1, sl
	str	sl, [sp, #12]
	bl	memcpy
	ldrh	r3, [r8, #142]
	ldr	r2, .L1773+12
	cmp	r3, r2
	beq	.L1764
	ldr	r2, .L1773+16
	ldr	r9, .L1773+20
	ldrh	r1, [r2, #0]
	ldr	ip, .L1773+24
	add	r1, r1, #1
	strh	r1, [r2, #0]	@ movhi
	ldr	r2, .L1773+28
	ldr	fp, .L1773+32
	ldr	r0, [r2, r3, asl #2]
	ldrb	r2, [r9, #0]	@ zero_extendqisi2
	ldr	r3, .L1773+36
	str	r2, [sp, #16]
	ldrh	r2, [ip, #0]
	strb	r7, [r3, #0]
	eor	r2, r2, #32768
	mov	r2, r2, lsr #15
	strb	r2, [r9, #0]
	mvn	r2, #0
	strh	r2, [fp, #0]	@ movhi
	ldr	r3, .L1773+40
	ldr	r2, .L1773+44
	mov	sl, #0
	strb	r7, [r3, #0]
	strb	sl, [r2, #0]
	strb	sl, [r8, #264]
	mov	r1, sl
	str	r2, [sp, #0]
	str	r3, [sp, #8]
	str	ip, [sp, #4]
	bl	DrawStructure
	ldrsh	r1, [fp, #0]
	ldr	r3, [sp, #8]
	cmn	r1, #1
	strb	sl, [r3, #0]
	ldrh	r3, [fp, #0]
	ldmia	sp, {r2, ip}
	streqb	sl, [r9, #0]
	beq	.L1766
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	strb	r7, [r8, #264]
	cmp	r2, #0
	bne	.L1766
	strh	r3, [ip, #0]	@ movhi
	mov	r0, r7
	bl	DrawCursorItem
.L1766:
	ldrb	r3, [r9, #0]	@ zero_extendqisi2
	ldr	r2, [sp, #16]
	orr	r3, r2, r3
	strb	r3, [r9, #0]
	ldr	r3, .L1773+36
	mov	r2, #0
	strb	r2, [r3, #0]
	ldr	r3, .L1773+16
	ldrh	r2, [r3, #0]
	sub	r2, r2, #1
	strh	r2, [r3, #0]	@ movhi
.L1764:
	ldr	r3, .L1773+4
	ldrb	r0, [r3, #52]	@ zero_extendqisi2
	bl	SetCurFont
	ldr	r3, .L1773
	mov	r2, #268
	mla	r3, r2, r4, r3
	ldrsh	r2, [r3, #138]
	cmp	r2, #0
	bne	.L1767
	ldrsh	r2, [r3, #140]
	cmp	r2, #0
	bne	.L1767
	ldr	r2, .L1773+48
	ldr	r1, [r2, #0]
	ldrb	r2, [r1, #14]	@ zero_extendqisi2
	strh	r2, [r3, #138]	@ movhi
	ldrb	r1, [r1, #11]	@ zero_extendqisi2
	sub	r1, r1, #1
	rsb	r2, r2, r1
	strh	r2, [r3, #140]	@ movhi
.L1767:
	ldr	r7, .L1773+4
	ldr	sl, .L1773
	ldrsh	r3, [r7, #50]
	ldrsh	r0, [r7, #44]
	ldrsh	r1, [r7, #46]
	ldrsh	r2, [r7, #48]
	bl	GuiLib_SetClipping
	mov	r9, #268
	mla	r3, r9, r4, sl
	ldrh	fp, [r3, #126]
	ldr	r3, .L1773+12
	cmp	fp, r3
	beq	.L1768
	ldr	r8, .L1773+16
	ldr	r1, [sp, #12]
	ldrh	r3, [r8, #0]
	mov	r2, #100
	add	r3, r3, #1
	strh	r3, [r8, #0]	@ movhi
	mov	r0, r7
	bl	memcpy
	ldr	r3, .L1773+8
	mov	r1, #0
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mul	r9, r3, r9
	add	r3, sl, r9
	ldrh	r3, [r3, #2]
	ldrh	r2, [sl, r9]
	strh	r3, [r7, #24]	@ movhi
	ldr	r3, .L1773+28
	strh	r2, [r7, #22]	@ movhi
	ldr	r0, [r3, fp, asl #2]
	bl	DrawStructure
	ldrh	r3, [r8, #0]
	sub	r3, r3, #1
	strh	r3, [r8, #0]	@ movhi
.L1768:
	ldr	r1, .L1773
	mov	r3, #268
	mla	r3, r4, r3, r1
	ldr	r2, [sp, #20]
	ldrb	r0, [r3, #154]	@ zero_extendqisi2
	str	r2, [r3, #8]
	mov	r2, r6, asl #16
	mov	r2, r2, lsr #16
	cmp	r0, #0
	strh	r5, [r3, #164]	@ movhi
	strh	r5, [r3, #16]	@ movhi
	strh	r2, [r3, #20]	@ movhi
	bne	.L1769
	ldrh	r0, [r3, #128]
	cmp	r6, r0
	strlth	r2, [r3, #128]	@ movhi
.L1769:
	mov	r5, #268
	mla	r5, r4, r5, r1
	mov	r3, #2
	strb	r3, [r5, #4]
	mov	r0, r4
	bl	ScrollBox_SetTopLine
	mov	r0, r4
	bl	GuiLib_ScrollBox_Redraw
	mvn	r3, #0
	strh	r3, [r5, #14]	@ movhi
	ldr	r0, .L1773+4
	add	r1, sp, #24
	mov	r2, #100
	bl	memcpy
	mov	r0, #1
.L1763:
	add	sp, sp, #124
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L1774:
	.align	2
.L1773:
	.word	.LANCHOR41
	.word	.LANCHOR10
	.word	.LANCHOR40
	.word	65535
	.word	.LANCHOR122
	.word	.LANCHOR102
	.word	.LANCHOR103
	.word	GuiStruct_StructPtrList
	.word	.LANCHOR105
	.word	.LANCHOR39
	.word	.LANCHOR101
	.word	.LANCHOR104
	.word	.LANCHOR8
.LFE131:
	.size	GuiLib_ScrollBox_Init, .-GuiLib_ScrollBox_Init
	.section	.text.GuiLib_ScrollBox_RedrawLine,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_RedrawLine
	.type	GuiLib_ScrollBox_RedrawLine, %function
GuiLib_ScrollBox_RedrawLine:
.LFB133:
	@ args = 0, pretend = 0, frame = 100
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1779
	stmfd	sp!, {r4, r5, lr}
.LCFI124:
	mov	r2, #268
	and	r4, r0, #255
	mla	r3, r2, r4, r3
	mov	r1, r1, asl #16
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	sub	sp, sp, #100
.LCFI125:
	cmp	r2, #2
	mov	r5, r1, lsr #16
	movne	r0, #0
	bne	.L1776
	ldr	r0, [r3, #8]
	cmp	r0, #0
	beq	.L1776
	mov	r2, #100
	ldr	r1, .L1779+4
	mov	r0, sp
	bl	memcpy
	ldr	r3, .L1779+8
	mov	r1, r5, asl #16
	strb	r4, [r3, #0]
	mov	r0, r4
	mov	r1, r1, asr #16
	bl	ScrollBox_DrawScrollLine
	ldr	r0, .L1779+4
	mov	r1, sp
	mov	r2, #100
	bl	memcpy
	mov	r0, #1
.L1776:
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, pc}
.L1780:
	.align	2
.L1779:
	.word	.LANCHOR41
	.word	.LANCHOR10
	.word	.LANCHOR40
.LFE133:
	.size	GuiLib_ScrollBox_RedrawLine, .-GuiLib_ScrollBox_RedrawLine
	.section	.text.GuiLib_ScrollBox_Close,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_Close
	.type	GuiLib_ScrollBox_Close, %function
GuiLib_ScrollBox_Close:
.LFB134:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L1784
	and	r0, r0, #255
	mov	r3, #268
	mla	r3, r0, r3, r2
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	cmp	r2, #2
	moveq	r0, #1
	streqb	r0, [r3, #4]
	movne	r0, #0
	bx	lr
.L1785:
	.align	2
.L1784:
	.word	.LANCHOR41
.LFE134:
	.size	GuiLib_ScrollBox_Close, .-GuiLib_ScrollBox_Close
	.section	.text.GuiLib_ScrollBox_Down,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_Down
	.type	GuiLib_ScrollBox_Down, %function
GuiLib_ScrollBox_Down:
.LFB135:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1801
	mov	r2, #268
	and	r0, r0, #255
	mla	r3, r2, r0, r3
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI126:
	ldrb	ip, [r3, #4]	@ zero_extendqisi2
	ldrh	r2, [r3, #12]
	cmp	ip, #2
	ldrh	r1, [r3, #164]
	bne	.L1787
	ldrh	r8, [r3, #128]
	ldrb	r7, [r3, #124]	@ zero_extendqisi2
	sub	sl, r8, #1
	add	sl, sl, r2
	mov	sl, sl, asl #16
	cmp	r7, #0
	mov	sl, sl, lsr #16
	beq	.L1788
	ldrh	ip, [r3, #20]
	sub	ip, ip, #1
	cmp	sl, ip
	addlt	ip, r2, #1
	blt	.L1798
	ldrb	ip, [r3, #152]	@ zero_extendqisi2
	cmp	ip, #1
	moveq	ip, #0
	bne	.L1787
.L1798:
	strh	ip, [r3, #12]	@ movhi
	b	.L1787
.L1788:
	mov	r4, r1, asl #16
	movs	r4, r4, asr #16
	bmi	.L1787
	add	ip, r3, #152
	ldrb	r6, [ip, #2]	@ zero_extendqisi2
	ldrh	r5, [r3, #20]
	cmp	r6, #0
	bne	.L1790
	sub	r7, r5, #1
	cmp	r4, r7
	bne	.L1791
	ldrb	ip, [r3, #152]	@ zero_extendqisi2
	cmp	ip, #1
	streqh	r6, [r3, #12]	@ movhi
	streqh	r6, [r3, #164]	@ movhi
	b	.L1787
.L1791:
	ldrb	ip, [ip, #1]	@ zero_extendqisi2
	rsb	sl, ip, sl
	cmp	r4, sl
	blt	.L1800
.L1792:
	add	ip, r2, #1
	mov	ip, ip, asl #16
	mov	ip, ip, lsr #16
	strh	ip, [r3, #12]	@ movhi
	rsb	r5, r8, r5
	mov	ip, ip, asl #16
	cmp	r5, ip, asr #16
	strlth	r5, [r3, #12]	@ movhi
	ldr	r3, .L1801
	mov	ip, #268
	mla	r3, ip, r0, r3
	ldrh	ip, [r3, #164]
	add	ip, ip, #1
	b	.L1799
.L1790:
	sub	r5, r5, #1
	cmp	r4, r5
	addne	ip, r2, #1
	strneh	ip, [r3, #12]	@ movhi
	bne	.L1800
	ldrb	r4, [r3, #152]	@ zero_extendqisi2
	cmp	r4, #1
	ldreqb	ip, [ip, #1]	@ zero_extendqisi2
	streqh	r7, [r3, #164]	@ movhi
	rsbeq	ip, ip, #0
	streqh	ip, [r3, #12]	@ movhi
	b	.L1787
.L1800:
	add	ip, r1, #1
.L1799:
	strh	ip, [r3, #164]	@ movhi
.L1787:
	ldr	r3, .L1801
	mov	ip, #268
	mla	r3, ip, r0, r3
	ldrh	ip, [r3, #12]
	cmp	r2, ip
	bne	.L1795
	ldrh	r3, [r3, #164]
	cmp	r1, r3
	beq	.L1797
.L1795:
	bl	GuiLib_ScrollBox_Redraw
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L1797:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L1802:
	.align	2
.L1801:
	.word	.LANCHOR41
.LFE135:
	.size	GuiLib_ScrollBox_Down, .-GuiLib_ScrollBox_Down
	.section	.text.GuiLib_ScrollBox_Up,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_Up
	.type	GuiLib_ScrollBox_Up, %function
GuiLib_ScrollBox_Up:
.LFB136:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1821
	mov	r2, #268
	and	r0, r0, #255
	mla	r3, r2, r0, r3
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI127:
	ldrb	ip, [r3, #4]	@ zero_extendqisi2
	ldrh	r1, [r3, #12]
	cmp	ip, #2
	ldrh	r2, [r3, #164]
	bne	.L1804
	ldrb	ip, [r3, #124]	@ zero_extendqisi2
	cmp	ip, #0
	beq	.L1805
	cmp	r1, #0
	subne	ip, r1, #1
	bne	.L1817
	ldrb	ip, [r3, #152]	@ zero_extendqisi2
	cmp	ip, #1
	ldreqh	ip, [r3, #128]
	ldreqh	r4, [r3, #20]
	rsbeq	ip, ip, r4
	streqh	ip, [r3, #12]	@ movhi
	ldr	r3, .L1821
	mov	ip, #268
	mla	r3, ip, r0, r3
	ldrsh	ip, [r3, #12]
	cmp	ip, #0
	movlt	ip, #0
	bge	.L1804
.L1817:
	strh	ip, [r3, #12]	@ movhi
	b	.L1804
.L1805:
	mov	ip, r2, asl #16
	movs	r6, ip, asr #16
	bmi	.L1804
	add	ip, r3, #152
	ldrb	r4, [ip, #2]	@ zero_extendqisi2
	cmp	r4, #0
	bne	.L1808
	cmp	r2, #0
	bne	.L1809
	ldrb	ip, [r3, #152]	@ zero_extendqisi2
	cmp	ip, #1
	bne	.L1804
	ldrh	r4, [r3, #20]
	ldrh	ip, [r3, #128]
	rsb	ip, ip, r4
	mov	ip, ip, asl #16
	mov	ip, ip, lsr #16
	tst	ip, #32768
	strh	ip, [r3, #12]	@ movhi
	strneh	r2, [r3, #12]	@ movhi
	ldr	r3, .L1821
	mov	ip, #268
	mla	r3, ip, r0, r3
	ldrh	ip, [r3, #20]
	b	.L1819
.L1809:
	ldrb	ip, [ip, #1]	@ zero_extendqisi2
	mov	r5, r1, asl #16
	add	r5, ip, r5, asr #16
	cmp	r6, r5
	bgt	.L1820
.L1811:
	sub	ip, r1, #1
	mov	ip, ip, asl #16
	mov	ip, ip, lsr #16
	tst	ip, #32768
	strh	ip, [r3, #12]	@ movhi
	strneh	r4, [r3, #12]	@ movhi
	ldr	r3, .L1821
	mov	ip, #268
	mla	r3, ip, r0, r3
	ldrh	ip, [r3, #164]
.L1819:
	sub	ip, ip, #1
	b	.L1818
.L1808:
	cmp	r2, #0
	subne	ip, r1, #1
	strneh	ip, [r3, #12]	@ movhi
	bne	.L1820
	ldrb	r4, [r3, #152]	@ zero_extendqisi2
	cmp	r4, #1
	bne	.L1804
	ldrh	r4, [r3, #20]
	ldrb	ip, [ip, #1]	@ zero_extendqisi2
	sub	r4, r4, #1
	rsb	ip, ip, r4
	strh	ip, [r3, #12]	@ movhi
	strh	r4, [r3, #164]	@ movhi
	b	.L1804
.L1820:
	sub	ip, r2, #1
.L1818:
	strh	ip, [r3, #164]	@ movhi
.L1804:
	ldr	r3, .L1821
	mov	ip, #268
	mla	r3, ip, r0, r3
	ldrh	ip, [r3, #12]
	cmp	r1, ip
	bne	.L1814
	ldrh	r3, [r3, #164]
	cmp	r2, r3
	beq	.L1816
.L1814:
	bl	GuiLib_ScrollBox_Redraw
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, pc}
.L1816:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L1822:
	.align	2
.L1821:
	.word	.LANCHOR41
.LFE136:
	.size	GuiLib_ScrollBox_Up, .-GuiLib_ScrollBox_Up
	.section	.text.GuiLib_ScrollBox_Home,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_Home
	.type	GuiLib_ScrollBox_Home, %function
GuiLib_ScrollBox_Home:
.LFB137:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1831
	mov	r2, #268
	and	r0, r0, #255
	mla	r3, r2, r0, r3
	stmfd	sp!, {r4, lr}
.LCFI128:
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrh	r4, [r3, #12]
	cmp	r2, #2
	ldrh	ip, [r3, #164]
	bne	.L1824
	ldrb	r2, [r3, #124]	@ zero_extendqisi2
	cmp	r2, #0
	movne	r2, #0
	strneh	r2, [r3, #12]	@ movhi
	bne	.L1824
	add	r2, r3, #152
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	cmp	r1, #0
	ldrneb	r2, [r2, #1]	@ zero_extendqisi2
	streqh	r1, [r3, #12]	@ movhi
	rsbne	r2, r2, #0
	strneh	r2, [r3, #12]	@ movhi
	ldr	r3, .L1831
	mov	r2, #268
	mla	r3, r2, r0, r3
	ldrsh	r2, [r3, #164]
	cmp	r2, #0
	movge	r2, #0
	strgeh	r2, [r3, #164]	@ movhi
.L1824:
	ldr	r3, .L1831
	mov	r2, #268
	mla	r3, r2, r0, r3
	ldrh	r2, [r3, #12]
	cmp	r4, r2
	bne	.L1828
	ldrh	r3, [r3, #164]
	cmp	ip, r3
	beq	.L1830
.L1828:
	bl	GuiLib_ScrollBox_Redraw
	mov	r0, #1
	ldmfd	sp!, {r4, pc}
.L1830:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L1832:
	.align	2
.L1831:
	.word	.LANCHOR41
.LFE137:
	.size	GuiLib_ScrollBox_Home, .-GuiLib_ScrollBox_Home
	.section	.text.GuiLib_ScrollBox_End,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_End
	.type	GuiLib_ScrollBox_End, %function
GuiLib_ScrollBox_End:
.LFB138:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1842
	mov	r2, #268
	and	r0, r0, #255
	mla	r3, r2, r0, r3
	stmfd	sp!, {r4, lr}
.LCFI129:
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrh	r4, [r3, #12]
	cmp	r2, #2
	ldrh	ip, [r3, #164]
	bne	.L1834
	ldrb	r2, [r3, #124]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1835
	ldrh	r1, [r3, #20]
	ldrh	r2, [r3, #128]
	rsb	r2, r2, r1
	bic	r2, r2, r2, asr #31
	strh	r2, [r3, #12]	@ movhi
	b	.L1834
.L1835:
	add	r1, r3, #152
	ldrb	r2, [r1, #2]	@ zero_extendqisi2
	cmp	r2, #0
	ldreqh	r1, [r3, #128]
	ldrh	r2, [r3, #20]
	ldrneb	r1, [r1, #1]	@ zero_extendqisi2
	rsbeq	r2, r1, r2
	subne	r2, r2, #1
	biceq	r2, r2, r2, asr #31
	rsbne	r2, r1, r2
	strh	r2, [r3, #12]	@ movhi
	ldr	r3, .L1842
	mov	r2, #268
	mla	r3, r2, r0, r3
	ldrsh	r2, [r3, #164]
	cmp	r2, #0
	ldrgeh	r2, [r3, #20]
	subge	r2, r2, #1
	strgeh	r2, [r3, #164]	@ movhi
.L1834:
	ldr	r3, .L1842
	mov	r2, #268
	mla	r3, r2, r0, r3
	ldrh	r2, [r3, #12]
	cmp	r4, r2
	bne	.L1838
	ldrh	r3, [r3, #164]
	cmp	ip, r3
	beq	.L1840
.L1838:
	bl	GuiLib_ScrollBox_Redraw
	mov	r0, #1
	ldmfd	sp!, {r4, pc}
.L1840:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L1843:
	.align	2
.L1842:
	.word	.LANCHOR41
.LFE138:
	.size	GuiLib_ScrollBox_End, .-GuiLib_ScrollBox_End
	.section	.text.GuiLib_ScrollBox_To_Line,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_To_Line
	.type	GuiLib_ScrollBox_To_Line, %function
GuiLib_ScrollBox_To_Line:
.LFB139:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1866
	and	r0, r0, #255
	mov	r2, #268
	mla	r2, r0, r2, r3
	mov	r1, r1, asl #16
	ldrh	ip, [r2, #20]
	mov	r2, #268
	mla	r3, r2, r0, r3
	mov	r1, r1, lsr #16
	cmp	ip, r1
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI130:
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	suble	r1, ip, #1
	movle	r1, r1, asl #16
	movle	r1, r1, lsr #16
	cmp	r2, #2
	ldrh	r4, [r3, #12]
	ldrh	r5, [r3, #164]
	bne	.L1846
	ldrb	r2, [r3, #124]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1847
	ldrh	r2, [r3, #128]
	rsb	r2, r2, ip
	mov	r2, r2, asl #16
	mov	r2, r2, asr #16
	bic	r2, r2, r2, asr #31
	mov	r2, r2, asl #16
	cmp	r1, r2, asr #16
	mov	ip, r2, lsr #16
	strgth	ip, [r3, #12]	@ movhi
	strleh	r1, [r3, #12]	@ movhi
	b	.L1849
.L1847:
	add	r2, r3, #152
	ldrb	r6, [r2, #2]	@ zero_extendqisi2
	cmp	r6, #0
	ldrneb	r2, [r2, #1]	@ zero_extendqisi2
	rsbne	r2, r2, r1
	bne	.L1863
	mov	r2, r4, asl #16
	mov	r2, r2, asr #16
	cmp	r1, r2
	blt	.L1851
	ldrh	r3, [r3, #128]
	add	r2, r2, r3
	cmp	r1, r2
	blt	.L1849
.L1851:
	ldr	r3, .L1866
	mov	r7, #268
	mla	r2, r7, r0, r3
	mla	r3, r7, r0, r3
	ldrh	r8, [r2, #128]
	ldrb	r6, [r2, #153]	@ zero_extendqisi2
	rsb	ip, r8, ip
	add	r8, ip, r6
	cmp	r1, r8
	rsble	r6, r6, r1
	strgth	ip, [r2, #12]	@ movhi
	strleh	r6, [r2, #12]	@ movhi
	ldrh	r2, [r3, #128]
	ldrsh	ip, [r3, #12]
	rsb	r7, r2, r1
	cmp	r7, ip
	bge	.L1864
.L1854:
	add	r6, r1, r2
	add	r7, r7, #1
	cmp	r6, r7
	bgt	.L1856
	cmp	ip, r7
	ble	.L1855
.L1864:
	rsb	r2, r2, #1
	b	.L1862
.L1856:
	cmp	r6, ip
	bgt	.L1855
	sub	r2, r2, #1
.L1862:
	add	r2, r1, r2
	strh	r2, [r3, #12]	@ movhi
.L1855:
	ldr	r3, .L1866
	mov	r2, #268
	mla	r3, r2, r0, r3
	ldrsh	ip, [r3, #12]
	ldrh	r7, [r3, #12]
	cmp	ip, #0
	blt	.L1865
.L1857:
	ldrh	r6, [r3, #20]
	ldrh	r2, [r3, #128]
	subs	r2, r6, r2
	bpl	.L1858
	cmp	r7, #0
	beq	.L1849
.L1865:
	mov	r2, #0
	b	.L1863
.L1858:
	cmp	ip, r2
	ble	.L1849
.L1863:
	strh	r2, [r3, #12]	@ movhi
.L1849:
	ldr	r3, .L1866
	mov	r2, #268
	mla	r3, r2, r0, r3
	strh	r1, [r3, #164]	@ movhi
.L1846:
	ldr	r3, .L1866
	mov	r2, #268
	mla	r3, r2, r0, r3
	ldrh	r2, [r3, #12]
	cmp	r4, r2
	bne	.L1859
	ldrh	r3, [r3, #164]
	cmp	r5, r3
	beq	.L1861
.L1859:
	bl	GuiLib_ScrollBox_Redraw
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L1861:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L1867:
	.align	2
.L1866:
	.word	.LANCHOR41
.LFE139:
	.size	GuiLib_ScrollBox_To_Line, .-GuiLib_ScrollBox_To_Line
	.section	.text.GuiLib_ScrollBox_SetLineMarker,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_SetLineMarker
	.type	GuiLib_ScrollBox_SetLineMarker, %function
GuiLib_ScrollBox_SetLineMarker:
.LFB140:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	and	r0, r0, #255
	mov	r1, r1, asl #16
	mov	r2, r2, asl #16
	mov	r3, r3, asl #16
	cmp	r0, #2
	stmfd	sp!, {r4, r5, lr}
.LCFI131:
	mov	r1, r1, lsr #16
	mov	r2, r2, asr #16
	mov	r3, r3, lsr #16
	movhi	r0, #0
	ldmhifd	sp!, {r4, r5, pc}
	ldr	r4, .L1874
	mov	ip, #268
	mla	ip, r0, ip, r4
	ldrb	r5, [ip, #4]	@ zero_extendqisi2
	cmp	r1, #0
	cmpeq	r5, #2
	movne	r1, #0
	moveq	r1, #1
	bne	.L1872
	cmp	r2, #0
	mvnlt	r2, #0
	blt	.L1870
	ldrh	r1, [ip, #20]
	ldrsh	ip, [ip, #20]
	cmp	ip, r2
	suble	r2, r1, #1
	movle	r2, r2, asl #16
	movle	r2, r2, asr #16
.L1870:
	mov	r1, #268
	mla	r4, r1, r0, r4
	cmp	r3, #1
	movcs	r3, #1
	strh	r2, [r4, #164]	@ movhi
	strh	r3, [r4, #166]	@ movhi
	bl	ScrollBox_SetTopLine
	mov	r0, #1
	ldmfd	sp!, {r4, r5, pc}
.L1872:
	mov	r0, r1
	ldmfd	sp!, {r4, r5, pc}
.L1875:
	.align	2
.L1874:
	.word	.LANCHOR41
.LFE140:
	.size	GuiLib_ScrollBox_SetLineMarker, .-GuiLib_ScrollBox_SetLineMarker
	.section	.text.GuiLib_ScrollBox_GetActiveLine,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_GetActiveLine
	.type	GuiLib_ScrollBox_GetActiveLine, %function
GuiLib_ScrollBox_GetActiveLine:
.LFB141:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L1879
	movs	r1, r1, asl #16
	and	r0, r0, #255
	mov	r1, #268
	mla	r0, r1, r0, r2
	movne	r3, #0
	moveq	r3, #1
	ldrb	r2, [r0, #4]	@ zero_extendqisi2
	cmp	r2, #2
	movne	r3, #0
	andeq	r3, r3, #1
	cmp	r3, #0
	ldrneh	r0, [r0, #164]
	ldreq	r0, .L1879+4
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bx	lr
.L1880:
	.align	2
.L1879:
	.word	.LANCHOR41
	.word	65535
.LFE141:
	.size	GuiLib_ScrollBox_GetActiveLine, .-GuiLib_ScrollBox_GetActiveLine
	.section	.text.GuiLib_ScrollBox_GetActiveLineCount,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_GetActiveLineCount
	.type	GuiLib_ScrollBox_GetActiveLineCount, %function
GuiLib_ScrollBox_GetActiveLineCount:
.LFB142:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L1884
	movs	r1, r1, asl #16
	and	r0, r0, #255
	mov	r1, #268
	mla	r0, r1, r0, r2
	movne	r3, #0
	moveq	r3, #1
	ldrb	r2, [r0, #4]	@ zero_extendqisi2
	cmp	r2, #2
	movne	r3, #0
	andeq	r3, r3, #1
	cmp	r3, #0
	ldrneh	r0, [r0, #166]
	ldreq	r0, .L1884+4
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bx	lr
.L1885:
	.align	2
.L1884:
	.word	.LANCHOR41
	.word	65535
.LFE142:
	.size	GuiLib_ScrollBox_GetActiveLineCount, .-GuiLib_ScrollBox_GetActiveLineCount
	.section	.text.GuiLib_ScrollBox_SetIndicator,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_SetIndicator
	.type	GuiLib_ScrollBox_SetIndicator, %function
GuiLib_ScrollBox_SetIndicator:
.LFB143:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r0, r0, #255
	mov	r1, r1, asl #16
	cmp	r0, #2
	mov	r1, r1, asr #16
	bhi	.L1889
	cmp	r1, #0
	mvnlt	r1, #0
	blt	.L1888
	ldr	r3, .L1891
	mov	r2, #268
	mla	r3, r2, r0, r3
	ldrh	r2, [r3, #20]
	ldrsh	r3, [r3, #20]
	cmp	r3, r1
	suble	r1, r2, #1
	movle	r1, r1, asl #16
	movle	r1, r1, asr #16
.L1888:
	ldr	r3, .L1891
	mov	r2, #268
	mla	r0, r2, r0, r3
	add	r0, r0, #256
	strh	r1, [r0, #2]	@ movhi
	mov	r0, #1
	bx	lr
.L1889:
	mov	r0, #0
	bx	lr
.L1892:
	.align	2
.L1891:
	.word	.LANCHOR41
.LFE143:
	.size	GuiLib_ScrollBox_SetIndicator, .-GuiLib_ScrollBox_SetIndicator
	.section	.text.GuiLib_ScrollBox_SetTopLine,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_SetTopLine
	.type	GuiLib_ScrollBox_SetTopLine, %function
GuiLib_ScrollBox_SetTopLine:
.LFB144:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r0, r0, #255
	mov	r1, r1, asl #16
	cmp	r0, #2
	mov	r1, r1, asr #16
	bhi	.L1897
	ldr	r2, .L1900
	mov	r3, #268
	mla	r3, r0, r3, r2
	ldrb	ip, [r3, #4]	@ zero_extendqisi2
	cmp	ip, #2
	mov	ip, r2
	bne	.L1897
	ldrb	r2, [r3, #154]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L1898
	cmp	r1, #0
	movle	r3, r2
	ble	.L1895
	ldrh	r2, [r3, #20]
	ldrh	r3, [r3, #128]
	rsb	r3, r3, r2
	cmp	r1, r3
	movlt	r3, r1
	movge	r3, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
.L1895:
	mov	r2, #268
	mla	r0, r2, r0, ip
	strh	r3, [r0, #12]	@ movhi
	mov	r0, #1
	bx	lr
.L1897:
	mov	r0, #0
	bx	lr
.L1898:
	mov	r0, #1
	bx	lr
.L1901:
	.align	2
.L1900:
	.word	.LANCHOR41
.LFE144:
	.size	GuiLib_ScrollBox_SetTopLine, .-GuiLib_ScrollBox_SetTopLine
	.section	.text.GuiLib_ScrollBox_GetTopLine,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_GetTopLine
	.type	GuiLib_ScrollBox_GetTopLine, %function
GuiLib_ScrollBox_GetTopLine:
.LFB145:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L1905
	mov	r2, #268
	and	r0, r0, #255
	mla	r0, r2, r0, r3
	ldrb	r3, [r0, #4]	@ zero_extendqisi2
	cmp	r3, #2
	ldreqh	r0, [r0, #12]
	ldrne	r0, .L1905+4
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bx	lr
.L1906:
	.align	2
.L1905:
	.word	.LANCHOR41
	.word	65535
.LFE145:
	.size	GuiLib_ScrollBox_GetTopLine, .-GuiLib_ScrollBox_GetTopLine
	.section	.text.GuiLib_TextBox_Scroll_Up,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_Up
	.type	GuiLib_TextBox_Scroll_Up, %function
GuiLib_TextBox_Scroll_Up:
.LFB148:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r0, r0, #255
	mvn	r1, #0
	mov	r2, #1
	mov	r3, #0
	b	TextBox_Scroll_To
.LFE148:
	.size	GuiLib_TextBox_Scroll_Up, .-GuiLib_TextBox_Scroll_Up
	.section	.text.GuiLib_TextBox_Scroll_Down,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_Down
	.type	GuiLib_TextBox_Scroll_Down, %function
GuiLib_TextBox_Scroll_Down:
.LFB149:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, #1
	and	r0, r0, #255
	mov	r2, r1
	mov	r3, #0
	b	TextBox_Scroll_To
.LFE149:
	.size	GuiLib_TextBox_Scroll_Down, .-GuiLib_TextBox_Scroll_Down
	.section	.text.GuiLib_TextBox_Scroll_Home,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_Home
	.type	GuiLib_TextBox_Scroll_Home, %function
GuiLib_TextBox_Scroll_Home:
.LFB150:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r2, #1
	and	r0, r0, #255
	mov	r1, #0
	mov	r3, r2
	b	TextBox_Scroll_To
.LFE150:
	.size	GuiLib_TextBox_Scroll_Home, .-GuiLib_TextBox_Scroll_Home
	.section	.text.GuiLib_TextBox_Scroll_End,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_End
	.type	GuiLib_TextBox_Scroll_End, %function
GuiLib_TextBox_Scroll_End:
.LFB151:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r0, r0, #255
	mov	r1, #0
	mov	r2, #1
	mov	r3, #2
	b	TextBox_Scroll_To
.LFE151:
	.size	GuiLib_TextBox_Scroll_End, .-GuiLib_TextBox_Scroll_End
	.section	.text.GuiLib_TextBox_Scroll_To_Line,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_To_Line
	.type	GuiLib_TextBox_Scroll_To_Line, %function
GuiLib_TextBox_Scroll_To_Line:
.LFB152:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r2, #1
	mov	r1, r1, asl #16
	and	r0, r0, #255
	mov	r1, r1, asr #16
	mov	r3, r2
	b	TextBox_Scroll_To
.LFE152:
	.size	GuiLib_TextBox_Scroll_To_Line, .-GuiLib_TextBox_Scroll_To_Line
	.section	.text.GuiLib_TextBox_Scroll_Up_Pixel,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_Up_Pixel
	.type	GuiLib_TextBox_Scroll_Up_Pixel, %function
GuiLib_TextBox_Scroll_Up_Pixel:
.LFB153:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r2, #0
	and	r0, r0, #255
	mvn	r1, #0
	mov	r3, r2
	b	TextBox_Scroll_To
.LFE153:
	.size	GuiLib_TextBox_Scroll_Up_Pixel, .-GuiLib_TextBox_Scroll_Up_Pixel
	.section	.text.GuiLib_TextBox_Scroll_Down_Pixel,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_Down_Pixel
	.type	GuiLib_TextBox_Scroll_Down_Pixel, %function
GuiLib_TextBox_Scroll_Down_Pixel:
.LFB154:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r2, #0
	and	r0, r0, #255
	mov	r1, #1
	mov	r3, r2
	b	TextBox_Scroll_To
.LFE154:
	.size	GuiLib_TextBox_Scroll_Down_Pixel, .-GuiLib_TextBox_Scroll_Down_Pixel
	.section	.text.GuiLib_TextBox_Scroll_Home_Pixel,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_Home_Pixel
	.type	GuiLib_TextBox_Scroll_Home_Pixel, %function
GuiLib_TextBox_Scroll_Home_Pixel:
.LFB155:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, #0
	and	r0, r0, #255
	mov	r2, r1
	mov	r3, #1
	b	TextBox_Scroll_To
.LFE155:
	.size	GuiLib_TextBox_Scroll_Home_Pixel, .-GuiLib_TextBox_Scroll_Home_Pixel
	.section	.text.GuiLib_TextBox_Scroll_End_Pixel,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_End_Pixel
	.type	GuiLib_TextBox_Scroll_End_Pixel, %function
GuiLib_TextBox_Scroll_End_Pixel:
.LFB156:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, #0
	and	r0, r0, #255
	mov	r2, r1
	mov	r3, #2
	b	TextBox_Scroll_To
.LFE156:
	.size	GuiLib_TextBox_Scroll_End_Pixel, .-GuiLib_TextBox_Scroll_End_Pixel
	.section	.text.GuiLib_TextBox_Scroll_To_PixelLine,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_To_PixelLine
	.type	GuiLib_TextBox_Scroll_To_PixelLine, %function
GuiLib_TextBox_Scroll_To_PixelLine:
.LFB157:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, r1, asl #16
	and	r0, r0, #255
	mov	r1, r1, asr #16
	mov	r2, #0
	mov	r3, #1
	b	TextBox_Scroll_To
.LFE157:
	.size	GuiLib_TextBox_Scroll_To_PixelLine, .-GuiLib_TextBox_Scroll_To_PixelLine
	.section	.text.GuiLib_TextBox_Scroll_Get_Pos,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_Get_Pos
	.type	GuiLib_TextBox_Scroll_Get_Pos, %function
GuiLib_TextBox_Scroll_Get_Pos:
.LFB159:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LBB290:
	ands	r0, r0, #255
	bne	.L1918
	ldr	r3, .L1921
	ldrb	r3, [r3, #88]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L1918
	mov	r1, #1
.LBE290:
.LBB291:
	b	TextBox_Scroll_Get_Pos.part.8
.L1918:
.LBE291:
	mov	r0, #0
	bx	lr
.L1922:
	.align	2
.L1921:
	.word	.LANCHOR56
.LFE159:
	.size	GuiLib_TextBox_Scroll_Get_Pos, .-GuiLib_TextBox_Scroll_Get_Pos
	.section	.text.GuiLib_TextBox_Scroll_Get_Pos_Pixel,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_Get_Pos_Pixel
	.type	GuiLib_TextBox_Scroll_Get_Pos_Pixel, %function
GuiLib_TextBox_Scroll_Get_Pos_Pixel:
.LFB160:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LBB292:
	ands	r0, r0, #255
	bne	.L1924
	ldr	r3, .L1927
	ldrb	r3, [r3, #88]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L1924
	mov	r1, r0
.LBE292:
.LBB293:
	b	TextBox_Scroll_Get_Pos.part.8
.L1924:
.LBE293:
	mov	r0, #0
	bx	lr
.L1928:
	.align	2
.L1927:
	.word	.LANCHOR56
.LFE160:
	.size	GuiLib_TextBox_Scroll_Get_Pos_Pixel, .-GuiLib_TextBox_Scroll_Get_Pos_Pixel
	.section	.text.GuiLib_TextBox_Scroll_FitsInside,"ax",%progbits
	.align	2
	.global	GuiLib_TextBox_Scroll_FitsInside
	.type	GuiLib_TextBox_Scroll_FitsInside, %function
GuiLib_TextBox_Scroll_FitsInside:
.LFB161:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ands	r0, r0, #255
	str	lr, [sp, #-4]!
.LCFI132:
	bne	.L1931
	ldr	r3, .L1933
	ldrb	r3, [r3, #88]	@ zero_extendqisi2
	cmp	r3, #255
	ldreq	pc, [sp], #4
	mov	r1, r0
	bl	TextBox_Scroll_CalcEndPos
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #31
	ldr	pc, [sp], #4
.L1931:
	mov	r0, #0
	ldr	pc, [sp], #4
.L1934:
	.align	2
.L1933:
	.word	.LANCHOR56
.LFE161:
	.size	GuiLib_TextBox_Scroll_FitsInside, .-GuiLib_TextBox_Scroll_FitsInside
	.section	.text.GuiLib_ScrollBox_Init_Custom_SetTopLine,"ax",%progbits
	.align	2
	.global	GuiLib_ScrollBox_Init_Custom_SetTopLine
	.type	GuiLib_ScrollBox_Init_Custom_SetTopLine, %function
GuiLib_ScrollBox_Init_Custom_SetTopLine:
.LFB163:
	@ args = 4, pretend = 0, frame = 128
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI133:
	mov	r2, r2, asl #16
	sub	sp, sp, #128
.LCFI134:
	mov	r6, r2, asr #16
	mov	r3, r3, asl #16
	ldrsh	r2, [sp, #164]
	mov	r5, r3, asr #16
	cmn	r5, #1
	str	r1, [sp, #24]
	and	r4, r0, #255
	str	r2, [sp, #16]
	blt	.L1948
	mov	r3, r6, lsr #31
	cmp	r5, #0
	movlt	r3, #0
	andge	r3, r3, #1
	cmp	r3, #0
	bne	.L1948
	cmp	r6, r5
	movgt	r3, #0
	movle	r3, #1
	cmp	r6, #0
	movlt	r3, #0
	cmp	r3, #0
	subne	r5, r6, #1
	movne	r5, r5, asl #16
	movne	r5, r5, asr #16
	b	.L1936
.L1948:
	mvn	r5, #0
.L1936:
	mov	sl, #268
	ldr	r9, .L1951
	mul	sl, r4, sl
	add	r8, r9, sl
	ldrb	r7, [r8, #4]	@ zero_extendqisi2
	cmp	r7, #1
	movne	r0, #0
	bne	.L1937
	ldr	r1, .L1951+4
	mov	r2, #100
	add	r0, sp, #28
	bl	memcpy
	ldr	r3, .L1951+8
	add	sl, sl, #24
	add	sl, r9, sl
	strb	r4, [r3, #0]
	mov	r2, #100
	ldr	r0, .L1951+4
	mov	r1, sl
	str	sl, [sp, #12]
	bl	memcpy
	ldrh	r3, [r8, #142]
	ldr	r2, .L1951+12
	cmp	r3, r2
	beq	.L1938
	ldr	r2, .L1951+16
	ldr	r9, .L1951+20
	ldrh	r1, [r2, #0]
	ldr	ip, .L1951+24
	add	r1, r1, #1
	strh	r1, [r2, #0]	@ movhi
	ldr	r2, .L1951+28
	ldr	fp, .L1951+32
	ldr	r0, [r2, r3, asl #2]
	ldrb	r2, [r9, #0]	@ zero_extendqisi2
	ldr	r3, .L1951+36
	str	r2, [sp, #20]
	ldrh	r2, [ip, #0]
	strb	r7, [r3, #0]
	eor	r2, r2, #32768
	mov	r2, r2, lsr #15
	strb	r2, [r9, #0]
	mvn	r2, #0
	strh	r2, [fp, #0]	@ movhi
	ldr	r3, .L1951+40
	ldr	r2, .L1951+44
	mov	sl, #0
	strb	r7, [r3, #0]
	strb	sl, [r2, #0]
	strb	sl, [r8, #264]
	mov	r1, sl
	str	r2, [sp, #0]
	str	r3, [sp, #8]
	str	ip, [sp, #4]
	bl	DrawStructure
	ldrsh	r1, [fp, #0]
	ldr	r3, [sp, #8]
	cmn	r1, #1
	strb	sl, [r3, #0]
	ldrh	r3, [fp, #0]
	ldmia	sp, {r2, ip}
	streqb	sl, [r9, #0]
	beq	.L1940
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	strb	r7, [r8, #264]
	cmp	r2, #0
	bne	.L1940
	strh	r3, [ip, #0]	@ movhi
	mov	r0, r7
	bl	DrawCursorItem
.L1940:
	ldrb	r3, [r9, #0]	@ zero_extendqisi2
	ldr	r2, [sp, #20]
	orr	r3, r2, r3
	strb	r3, [r9, #0]
	ldr	r3, .L1951+36
	mov	r2, #0
	strb	r2, [r3, #0]
	ldr	r3, .L1951+16
	ldrh	r2, [r3, #0]
	sub	r2, r2, #1
	strh	r2, [r3, #0]	@ movhi
.L1938:
	ldr	r3, .L1951+4
	ldrb	r0, [r3, #52]	@ zero_extendqisi2
	bl	SetCurFont
	ldr	r3, .L1951
	mov	r2, #268
	mla	r3, r2, r4, r3
	ldrsh	r2, [r3, #138]
	cmp	r2, #0
	bne	.L1941
	ldrsh	r2, [r3, #140]
	cmp	r2, #0
	bne	.L1941
	ldr	r2, .L1951+48
	ldr	r1, [r2, #0]
	ldrb	r2, [r1, #14]	@ zero_extendqisi2
	strh	r2, [r3, #138]	@ movhi
	ldrb	r1, [r1, #11]	@ zero_extendqisi2
	sub	r1, r1, #1
	rsb	r2, r2, r1
	strh	r2, [r3, #140]	@ movhi
.L1941:
	ldr	r7, .L1951+4
	ldr	sl, .L1951
	ldrsh	r3, [r7, #50]
	ldrsh	r0, [r7, #44]
	ldrsh	r1, [r7, #46]
	ldrsh	r2, [r7, #48]
	bl	GuiLib_SetClipping
	mov	r9, #268
	mla	r3, r9, r4, sl
	ldrh	fp, [r3, #126]
	ldr	r3, .L1951+12
	cmp	fp, r3
	beq	.L1942
	ldr	r8, .L1951+16
	ldr	r1, [sp, #12]
	ldrh	r3, [r8, #0]
	mov	r2, #100
	add	r3, r3, #1
	strh	r3, [r8, #0]	@ movhi
	mov	r0, r7
	bl	memcpy
	ldr	r3, .L1951+8
	mov	r1, #0
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	mul	r9, r3, r9
	add	r3, sl, r9
	ldrh	r3, [r3, #2]
	ldrh	r2, [sl, r9]
	strh	r3, [r7, #24]	@ movhi
	ldr	r3, .L1951+28
	strh	r2, [r7, #22]	@ movhi
	ldr	r0, [r3, fp, asl #2]
	bl	DrawStructure
	ldrh	r3, [r8, #0]
	sub	r3, r3, #1
	strh	r3, [r8, #0]	@ movhi
.L1942:
	ldr	r1, .L1951
	mov	r3, #268
	mla	r3, r4, r3, r1
	ldr	r2, [sp, #24]
	ldrb	r0, [r3, #154]	@ zero_extendqisi2
	str	r2, [r3, #8]
	mov	r2, r6, asl #16
	mov	r2, r2, lsr #16
	cmp	r0, #0
	strh	r5, [r3, #164]	@ movhi
	strh	r5, [r3, #16]	@ movhi
	strh	r2, [r3, #20]	@ movhi
	bne	.L1943
	ldrh	r0, [r3, #128]
	cmp	r6, r0
	strlth	r2, [r3, #128]	@ movhi
.L1943:
	mov	r3, #268
	mla	r3, r4, r3, r1
	mov	r2, #2
	strb	r2, [r3, #4]
.LBB294:
	add	r2, r3, #152
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L1944
	ldr	r2, [sp, #16]
	cmp	r2, #0
	movle	r3, r1
	ble	.L1945
	ldrh	r2, [r3, #20]
	ldrh	r3, [r3, #128]
	rsb	r3, r3, r2
	ldr	r2, [sp, #16]
	cmp	r3, r2
	movge	r3, r2
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
.L1945:
	ldr	r2, .L1951
	mov	r1, #268
	mla	r2, r1, r4, r2
	strh	r3, [r2, #12]	@ movhi
	b	.L1946
.L1944:
	ldrb	r2, [r2, #1]	@ zero_extendqisi2
	ldrh	r1, [r3, #164]
	rsb	r2, r2, r1
	strh	r2, [r3, #12]	@ movhi
.L1946:
.LBE294:
	mov	r0, r4
	bl	GuiLib_ScrollBox_Redraw
	ldr	r0, .L1951+4
	add	r1, sp, #28
	mov	r2, #100
	bl	memcpy
	mov	r0, #1
.L1937:
	add	sp, sp, #128
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L1952:
	.align	2
.L1951:
	.word	.LANCHOR41
	.word	.LANCHOR10
	.word	.LANCHOR40
	.word	65535
	.word	.LANCHOR122
	.word	.LANCHOR102
	.word	.LANCHOR103
	.word	GuiStruct_StructPtrList
	.word	.LANCHOR105
	.word	.LANCHOR39
	.word	.LANCHOR101
	.word	.LANCHOR104
	.word	.LANCHOR8
.LFE163:
	.size	GuiLib_ScrollBox_Init_Custom_SetTopLine, .-GuiLib_ScrollBox_Init_Custom_SetTopLine
	.global	Color4BitPattern
	.global	Color4BitPixelPattern
	.global	LinePattern2Pixel
	.global	LinePattern2Right
	.global	LinePattern2Left
	.global	GuiLib_CurStructureNdx
	.global	GuiLib_CurCharSet
	.global	GuiLib_LanguageCharSet
	.global	GuiLib_LanguageIndex
	.global	GuiLib_ScrollVisibleLines
	.global	GuiLib_ScrollTopLine
	.global	GuiLib_ScrollActiveLine
	.global	GuiLib_AutoRedrawFieldNo
	.global	GuiLib_ActiveCursorFieldNo
	.global	GuiLib_DisplayRepaint
	.global	Dummy1_16S
	.global	Dummy3_8U
	.global	Dummy2_8U
	.global	Dummy1_8U
	.global	DrawnY2
	.global	DrawnX2
	.global	DrawnY1
	.global	DrawnX1
	.global	Drawn
	.section .rodata
	.set	.LANCHOR5,. + 0
.LC0:
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	65
	.byte	66
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.section	.bss.ScrollBoxDetected,"aw",%nobits
	.set	.LANCHOR85,. + 0
	.type	ScrollBoxDetected, %object
	.size	ScrollBoxDetected, 1
ScrollBoxDetected:
	.space	1
	.section	.bss.GuiLib_DisplayRepaint,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	GuiLib_DisplayRepaint, %object
	.size	GuiLib_DisplayRepaint, 960
GuiLib_DisplayRepaint:
	.space	960
	.section	.bss.DisplayWriting,"aw",%nobits
	.set	.LANCHOR69,. + 0
	.type	DisplayWriting, %object
	.size	DisplayWriting, 1
DisplayWriting:
	.space	1
	.section	.rodata.LinePattern2Left,"a",%progbits
	.set	.LANCHOR2,. + 0
	.type	LinePattern2Left, %object
	.size	LinePattern2Left, 2
LinePattern2Left:
	.byte	-1
	.byte	15
	.section	.bss.DrawingLevel,"aw",%nobits
	.set	.LANCHOR113,. + 0
	.type	DrawingLevel, %object
	.size	DrawingLevel, 1
DrawingLevel:
	.space	1
	.section	.bss.ActiveAreaY2,"aw",%nobits
	.align	1
	.set	.LANCHOR121,. + 0
	.type	ActiveAreaY2, %object
	.size	ActiveAreaY2, 2
ActiveAreaY2:
	.space	2
	.section	.bss.ScrollBarMarkerForeColor,"aw",%nobits
	.set	.LANCHOR98,. + 0
	.type	ScrollBarMarkerForeColor, %object
	.size	ScrollBarMarkerForeColor, 1
ScrollBarMarkerForeColor:
	.space	1
	.section	.bss.Dummy1_16S,"aw",%nobits
	.align	1
	.type	Dummy1_16S, %object
	.size	Dummy1_16S, 2
Dummy1_16S:
	.space	2
	.section	.bss.ScrollLineDetected,"aw",%nobits
	.set	.LANCHOR130,. + 0
	.type	ScrollLineDetected, %object
	.size	ScrollLineDetected, 1
ScrollLineDetected:
	.space	1
	.section	.bss.DrawnY1,"aw",%nobits
	.align	1
	.set	.LANCHOR13,. + 0
	.type	DrawnY1, %object
	.size	DrawnY1, 2
DrawnY1:
	.space	2
	.section	.bss.CursorFieldFound,"aw",%nobits
	.align	1
	.set	.LANCHOR105,. + 0
	.type	CursorFieldFound, %object
	.size	CursorFieldFound, 2
CursorFieldFound:
	.space	2
	.section	.bss.InitialDrawing,"aw",%nobits
	.set	.LANCHOR101,. + 0
	.type	InitialDrawing, %object
	.size	InitialDrawing, 1
InitialDrawing:
	.space	1
	.section	.bss.Dummy2_8U,"aw",%nobits
	.type	Dummy2_8U, %object
	.size	Dummy2_8U, 1
Dummy2_8U:
	.space	1
	.section	.bss.ItemDataPtr,"aw",%nobits
	.align	2
	.set	.LANCHOR22,. + 0
	.type	ItemDataPtr, %object
	.size	ItemDataPtr, 4
ItemDataPtr:
	.space	4
	.section	.bss.Y2MemoryRead,"aw",%nobits
	.set	.LANCHOR37,. + 0
	.type	Y2MemoryRead, %object
	.size	Y2MemoryRead, 1
Y2MemoryRead:
	.space	1
	.section	.bss.BlinkBoxRate,"aw",%nobits
	.align	1
	.set	.LANCHOR68,. + 0
	.type	BlinkBoxRate, %object
	.size	BlinkBoxRate, 2
BlinkBoxRate:
	.space	2
	.section	.bss.ClippingY1,"aw",%nobits
	.align	1
	.set	.LANCHOR62,. + 0
	.type	ClippingY1, %object
	.size	ClippingY1, 2
ClippingY1:
	.space	2
	.section	.bss.ClippingY2,"aw",%nobits
	.align	1
	.set	.LANCHOR61,. + 0
	.type	ClippingY2, %object
	.size	ClippingY2, 2
ClippingY2:
	.space	2
	.section	.bss.ScrollBarX1,"aw",%nobits
	.align	1
	.set	.LANCHOR89,. + 0
	.type	ScrollBarX1, %object
	.size	ScrollBarX1, 2
ScrollBarX1:
	.space	2
	.section	.bss.GuiLib_AutoRedrawFieldNo,"aw",%nobits
	.align	1
	.set	.LANCHOR109,. + 0
	.type	GuiLib_AutoRedrawFieldNo, %object
	.size	GuiLib_AutoRedrawFieldNo, 2
GuiLib_AutoRedrawFieldNo:
	.space	2
	.section	.bss.ScrollBarX2,"aw",%nobits
	.align	1
	.set	.LANCHOR92,. + 0
	.type	ScrollBarX2, %object
	.size	ScrollBarX2, 2
ScrollBarX2:
	.space	2
	.section	.bss.GuiLib_CurCharSet,"aw",%nobits
	.set	.LANCHOR9,. + 0
	.type	GuiLib_CurCharSet, %object
	.size	GuiLib_CurCharSet, 1
GuiLib_CurCharSet:
	.space	1
	.section	.bss.GuiLib_CurStructureNdx,"aw",%nobits
	.align	1
	.set	.LANCHOR134,. + 0
	.type	GuiLib_CurStructureNdx, %object
	.size	GuiLib_CurStructureNdx, 2
GuiLib_CurStructureNdx:
	.space	2
	.section	.bss.GuiLib_ScrollActiveLine,"aw",%nobits
	.align	1
	.set	.LANCHOR133,. + 0
	.type	GuiLib_ScrollActiveLine, %object
	.size	GuiLib_ScrollActiveLine, 2
GuiLib_ScrollActiveLine:
	.space	2
	.section	.bss.GlobalScrollBoxIndex,"aw",%nobits
	.set	.LANCHOR40,. + 0
	.type	GlobalScrollBoxIndex, %object
	.size	GlobalScrollBoxIndex, 1
GlobalScrollBoxIndex:
	.space	1
	.section	.bss.FontWriteX1,"aw",%nobits
	.align	1
	.set	.LANCHOR80,. + 0
	.type	FontWriteX1, %object
	.size	FontWriteX1, 2
FontWriteX1:
	.space	2
	.section	.bss.Drawn,"aw",%nobits
	.set	.LANCHOR11,. + 0
	.type	Drawn, %object
	.size	Drawn, 1
Drawn:
	.space	1
	.section	.rodata.Color4BitPattern,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	Color4BitPattern, %object
	.size	Color4BitPattern, 16
Color4BitPattern:
	.byte	0
	.byte	17
	.byte	34
	.byte	51
	.byte	68
	.byte	85
	.byte	102
	.byte	119
	.byte	-120
	.byte	-103
	.byte	-86
	.byte	-69
	.byte	-52
	.byte	-35
	.byte	-18
	.byte	-1
	.section	.bss.GlobalBackgrBitmapIndex,"aw",%nobits
	.align	1
	.set	.LANCHOR116,. + 0
	.type	GlobalBackgrBitmapIndex, %object
	.size	GlobalBackgrBitmapIndex, 2
GlobalBackgrBitmapIndex:
	.space	2
	.section	.bss.DisplayLevel,"aw",%nobits
	.align	1
	.set	.LANCHOR122,. + 0
	.type	DisplayLevel, %object
	.size	DisplayLevel, 2
DisplayLevel:
	.space	2
	.section	.bss.ScrollLineDY,"aw",%nobits
	.align	1
	.set	.LANCHOR67,. + 0
	.type	ScrollLineDY, %object
	.size	ScrollLineDY, 2
ScrollLineDY:
	.space	2
	.section	.bss.YMemory,"aw",%nobits
	.align	1
	.set	.LANCHOR125,. + 0
	.type	YMemory, %object
	.size	YMemory, 6
YMemory:
	.space	6
	.section	.bss.X2MemoryRead,"aw",%nobits
	.set	.LANCHOR35,. + 0
	.type	X2MemoryRead, %object
	.size	X2MemoryRead, 1
X2MemoryRead:
	.space	1
	.section	.bss.ScrollBarArrowHeight,"aw",%nobits
	.align	1
	.set	.LANCHOR90,. + 0
	.type	ScrollBarArrowHeight, %object
	.size	ScrollBarArrowHeight, 2
ScrollBarArrowHeight:
	.space	2
	.section	.bss.TextboxScrollItems,"aw",%nobits
	.align	2
	.set	.LANCHOR56,. + 0
	.type	TextboxScrollItems, %object
	.size	TextboxScrollItems, 100
TextboxScrollItems:
	.space	100
	.section	.bss.ScrollBoxX1,"aw",%nobits
	.align	1
	.set	.LANCHOR126,. + 0
	.type	ScrollBoxX1, %object
	.size	ScrollBoxX1, 2
ScrollBoxX1:
	.space	2
	.section	.bss.AutoRedrawItems,"aw",%nobits
	.align	2
	.set	.LANCHOR110,. + 0
	.type	AutoRedrawItems, %object
	.size	AutoRedrawItems, 12800
AutoRedrawItems:
	.space	12800
	.section	.bss.Y2Mode,"aw",%nobits
	.set	.LANCHOR45,. + 0
	.type	Y2Mode, %object
	.size	Y2Mode, 1
Y2Mode:
	.space	1
	.section	.bss.InvertBoxOn,"aw",%nobits
	.set	.LANCHOR79,. + 0
	.type	InvertBoxOn, %object
	.size	InvertBoxOn, 1
InvertBoxOn:
	.space	1
	.section	.bss.FontWriteY1,"aw",%nobits
	.align	1
	.set	.LANCHOR81,. + 0
	.type	FontWriteY1, %object
	.size	FontWriteY1, 2
FontWriteY1:
	.space	2
	.section	.bss.GuiLib_ScrollTopLine,"aw",%nobits
	.align	1
	.set	.LANCHOR95,. + 0
	.type	GuiLib_ScrollTopLine, %object
	.size	GuiLib_ScrollTopLine, 2
GuiLib_ScrollTopLine:
	.space	2
	.section	.bss.FontWriteY2,"aw",%nobits
	.align	1
	.set	.LANCHOR83,. + 0
	.type	FontWriteY2, %object
	.size	FontWriteY2, 2
FontWriteY2:
	.space	2
	.section	.bss.X1MemoryRead,"aw",%nobits
	.set	.LANCHOR31,. + 0
	.type	X1MemoryRead, %object
	.size	X1MemoryRead, 1
X1MemoryRead:
	.space	1
	.section	.bss.GuiLib_ScrollVisibleLines,"aw",%nobits
	.align	1
	.set	.LANCHOR88,. + 0
	.type	GuiLib_ScrollVisibleLines, %object
	.size	GuiLib_ScrollVisibleLines, 2
GuiLib_ScrollVisibleLines:
	.space	2
	.section	.bss.BitmapWriteX2,"aw",%nobits
	.align	1
	.set	.LANCHOR65,. + 0
	.type	BitmapWriteX2, %object
	.size	BitmapWriteX2, 2
BitmapWriteX2:
	.space	2
	.section	.bss.X1VarType,"aw",%nobits
	.set	.LANCHOR50,. + 0
	.type	X1VarType, %object
	.size	X1VarType, 1
X1VarType:
	.space	1
	.section	.rodata.LinePattern2Pixel,"a",%progbits
	.set	.LANCHOR4,. + 0
	.type	LinePattern2Pixel, %object
	.size	LinePattern2Pixel, 2
LinePattern2Pixel:
	.byte	-16
	.byte	15
	.section	.bss.DisplayActiveAreaY1,"aw",%nobits
	.align	1
	.set	.LANCHOR137,. + 0
	.type	DisplayActiveAreaY1, %object
	.size	DisplayActiveAreaY1, 2
DisplayActiveAreaY1:
	.space	2
	.section	.bss.AnsiTextBuf,"aw",%nobits
	.set	.LANCHOR115,. + 0
	.type	AnsiTextBuf, %object
	.size	AnsiTextBuf, 2001
AnsiTextBuf:
	.space	2001
	.section	.bss.VarNumTextStr,"aw",%nobits
	.set	.LANCHOR57,. + 0
	.type	VarNumTextStr, %object
	.size	VarNumTextStr, 17
VarNumTextStr:
	.space	17
	.section	.bss.ActiveAreaX1,"aw",%nobits
	.align	1
	.set	.LANCHOR118,. + 0
	.type	ActiveAreaX1, %object
	.size	ActiveAreaX1, 2
ActiveAreaX1:
	.space	2
	.section	.bss.Y1VarType,"aw",%nobits
	.set	.LANCHOR51,. + 0
	.type	Y1VarType, %object
	.size	Y1VarType, 1
Y1VarType:
	.space	1
	.section	.bss.Y1MemoryRead,"aw",%nobits
	.set	.LANCHOR33,. + 0
	.type	Y1MemoryRead, %object
	.size	Y1MemoryRead, 1
Y1MemoryRead:
	.space	1
	.section	.bss.ScrollLineDataFunc,"aw",%nobits
	.align	2
	.set	.LANCHOR132,. + 0
	.type	ScrollLineDataFunc, %object
	.size	ScrollLineDataFunc, 4
ScrollLineDataFunc:
	.space	4
	.section	.bss.DisplayOrigoY,"aw",%nobits
	.align	1
	.set	.LANCHOR100,. + 0
	.type	DisplayOrigoY, %object
	.size	DisplayOrigoY, 2
DisplayOrigoY:
	.space	2
	.section	.bss.CommonByte0,"aw",%nobits
	.set	.LANCHOR23,. + 0
	.type	CommonByte0, %object
	.size	CommonByte0, 1
CommonByte0:
	.space	1
	.section	.bss.CommonByte1,"aw",%nobits
	.set	.LANCHOR24,. + 0
	.type	CommonByte1, %object
	.size	CommonByte1, 1
CommonByte1:
	.space	1
	.section	.bss.CommonByte2,"aw",%nobits
	.set	.LANCHOR25,. + 0
	.type	CommonByte2, %object
	.size	CommonByte2, 1
CommonByte2:
	.space	1
	.section	.bss.CommonByte3,"aw",%nobits
	.set	.LANCHOR26,. + 0
	.type	CommonByte3, %object
	.size	CommonByte3, 1
CommonByte3:
	.space	1
	.section	.bss.CommonByte4,"aw",%nobits
	.set	.LANCHOR27,. + 0
	.type	CommonByte4, %object
	.size	CommonByte4, 1
CommonByte4:
	.space	1
	.section	.bss.CommonByte5,"aw",%nobits
	.set	.LANCHOR28,. + 0
	.type	CommonByte5, %object
	.size	CommonByte5, 1
CommonByte5:
	.space	1
	.section	.bss.CommonByte6,"aw",%nobits
	.set	.LANCHOR29,. + 0
	.type	CommonByte6, %object
	.size	CommonByte6, 1
CommonByte6:
	.space	1
	.section	.bss.DrawnX2,"aw",%nobits
	.align	1
	.set	.LANCHOR14,. + 0
	.type	DrawnX2, %object
	.size	DrawnX2, 2
DrawnX2:
	.space	2
	.section	.bss.X1Mode,"aw",%nobits
	.set	.LANCHOR42,. + 0
	.type	X1Mode, %object
	.size	X1Mode, 1
X1Mode:
	.space	1
	.section	.bss.XMemory,"aw",%nobits
	.align	1
	.set	.LANCHOR124,. + 0
	.type	XMemory, %object
	.size	XMemory, 6
XMemory:
	.space	6
	.section	.bss.RefreshClock,"aw",%nobits
	.align	2
	.set	.LANCHOR141,. + 0
	.type	RefreshClock, %object
	.size	RefreshClock, 4
RefreshClock:
	.space	4
	.section	.bss.X2MemoryWrite,"aw",%nobits
	.set	.LANCHOR36,. + 0
	.type	X2MemoryWrite, %object
	.size	X2MemoryWrite, 1
X2MemoryWrite:
	.space	1
	.section	.bss.ScrollLineItem,"aw",%nobits
	.align	2
	.set	.LANCHOR131,. + 0
	.type	ScrollLineItem, %object
	.size	ScrollLineItem, 100
ScrollLineItem:
	.space	100
	.section	.bss.CursorFieldsInScrollLine,"aw",%nobits
	.set	.LANCHOR106,. + 0
	.type	CursorFieldsInScrollLine, %object
	.size	CursorFieldsInScrollLine, 1
CursorFieldsInScrollLine:
	.space	1
	.section	.bss.BlinkBoxX1,"aw",%nobits
	.align	1
	.set	.LANCHOR70,. + 0
	.type	BlinkBoxX1, %object
	.size	BlinkBoxX1, 2
BlinkBoxX1:
	.space	2
	.section	.bss.BlinkBoxX2,"aw",%nobits
	.align	1
	.set	.LANCHOR72,. + 0
	.type	BlinkBoxX2, %object
	.size	BlinkBoxX2, 2
BlinkBoxX2:
	.space	2
	.section	.bss.ScrollBoxesAry,"aw",%nobits
	.align	2
	.set	.LANCHOR41,. + 0
	.type	ScrollBoxesAry, %object
	.size	ScrollBoxesAry, 804
ScrollBoxesAry:
	.space	804
	.section	.bss.CurFont,"aw",%nobits
	.align	2
	.set	.LANCHOR8,. + 0
	.type	CurFont, %object
	.size	CurFont, 4
CurFont:
	.space	4
	.section	.bss.ItemTypeBit,"aw",%nobits
	.align	2
	.set	.LANCHOR30,. + 0
	.type	ItemTypeBit, %object
	.size	ItemTypeBit, 4
ItemTypeBit:
	.space	4
	.section	.bss.ScrollBarIndicatorRange,"aw",%nobits
	.align	1
	.set	.LANCHOR96,. + 0
	.type	ScrollBarIndicatorRange, %object
	.size	ScrollBarIndicatorRange, 2
ScrollBarIndicatorRange:
	.space	2
	.section	.bss.ScrollBoxY1,"aw",%nobits
	.align	1
	.set	.LANCHOR127,. + 0
	.type	ScrollBoxY1, %object
	.size	ScrollBoxY1, 2
ScrollBoxY1:
	.space	2
	.section	.bss.ScrollBoxY2,"aw",%nobits
	.align	1
	.set	.LANCHOR129,. + 0
	.type	ScrollBoxY2, %object
	.size	ScrollBoxY2, 2
ScrollBoxY2:
	.space	2
	.section	.bss.BbX1,"aw",%nobits
	.align	1
	.set	.LANCHOR16,. + 0
	.type	BbX1, %object
	.size	BbX1, 2
BbX1:
	.space	2
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC1:
	.ascii	"h\000"
.LC2:
	.ascii	"0\000"
.LC3:
	.ascii	":\000"
.LC4:
	.ascii	"am\000"
.LC5:
	.ascii	"pm\000"
.LC6:
	.ascii	"AM\000"
.LC7:
	.ascii	"PM\000"
.LC8:
	.ascii	"E\000"
.LC9:
	.ascii	"+\000"
.LC10:
	.ascii	"-\000"
	.section	.bss.DrawnX1,"aw",%nobits
	.align	1
	.set	.LANCHOR12,. + 0
	.type	DrawnX1, %object
	.size	DrawnX1, 2
DrawnX1:
	.space	2
	.section	.rodata.LinePattern2Right,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	LinePattern2Right, %object
	.size	LinePattern2Right, 2
LinePattern2Right:
	.byte	-16
	.byte	-1
	.section	.bss.InvertBoxX1,"aw",%nobits
	.align	1
	.set	.LANCHOR75,. + 0
	.type	InvertBoxX1, %object
	.size	InvertBoxX1, 2
InvertBoxX1:
	.space	2
	.section	.bss.InvertBoxX2,"aw",%nobits
	.align	1
	.set	.LANCHOR77,. + 0
	.type	InvertBoxX2, %object
	.size	InvertBoxX2, 2
InvertBoxX2:
	.space	2
	.section	.bss.SwapColors,"aw",%nobits
	.set	.LANCHOR114,. + 0
	.type	SwapColors, %object
	.size	SwapColors, 1
SwapColors:
	.space	1
	.section	.bss.ItemX1,"aw",%nobits
	.align	1
	.set	.LANCHOR46,. + 0
	.type	ItemX1, %object
	.size	ItemX1, 2
ItemX1:
	.space	2
	.section	.bss.ItemX2,"aw",%nobits
	.align	1
	.set	.LANCHOR48,. + 0
	.type	ItemX2, %object
	.size	ItemX2, 2
ItemX2:
	.space	2
	.section	.bss.VarExponent,"aw",%nobits
	.align	1
	.set	.LANCHOR20,. + 0
	.type	VarExponent, %object
	.size	VarExponent, 2
VarExponent:
	.space	2
	.section	.bss.AutoRedrawSaveIndex,"aw",%nobits
	.align	1
	.set	.LANCHOR111,. + 0
	.type	AutoRedrawSaveIndex, %object
	.size	AutoRedrawSaveIndex, 2
AutoRedrawSaveIndex:
	.space	2
	.section	.bss.BitmapWriteY2,"aw",%nobits
	.align	1
	.set	.LANCHOR66,. + 0
	.type	BitmapWriteY2, %object
	.size	BitmapWriteY2, 2
BitmapWriteY2:
	.space	2
	.section	.bss.ScrollBarY1,"aw",%nobits
	.align	1
	.set	.LANCHOR91,. + 0
	.type	ScrollBarY1, %object
	.size	ScrollBarY1, 2
ScrollBarY1:
	.space	2
	.section	.bss.ScrollBarY2,"aw",%nobits
	.align	1
	.set	.LANCHOR93,. + 0
	.type	ScrollBarY2, %object
	.size	ScrollBarY2, 2
ScrollBarY2:
	.space	2
	.section	.bss.BbX2,"aw",%nobits
	.align	1
	.set	.LANCHOR17,. + 0
	.type	BbX2, %object
	.size	BbX2, 2
BbX2:
	.space	2
	.section	.bss.NextScrollLineReading,"aw",%nobits
	.set	.LANCHOR39,. + 0
	.type	NextScrollLineReading, %object
	.size	NextScrollLineReading, 1
NextScrollLineReading:
	.space	1
	.section	.bss.CurItem,"aw",%nobits
	.align	2
	.set	.LANCHOR10,. + 0
	.type	CurItem, %object
	.size	CurItem, 100
CurItem:
	.space	100
	.section	.bss.ScrollBarDetected,"aw",%nobits
	.set	.LANCHOR86,. + 0
	.type	ScrollBarDetected, %object
	.size	ScrollBarDetected, 1
ScrollBarDetected:
	.space	1
	.section	.bss.ScrollBarSize,"aw",%nobits
	.align	1
	.set	.LANCHOR97,. + 0
	.type	ScrollBarSize, %object
	.size	ScrollBarSize, 2
ScrollBarSize:
	.space	2
	.section	.bss.ActiveAreaY1,"aw",%nobits
	.align	1
	.set	.LANCHOR119,. + 0
	.type	ActiveAreaY1, %object
	.size	ActiveAreaY1, 2
ActiveAreaY1:
	.space	2
	.section	.bss.AutoRedrawLevel,"aw",%nobits
	.set	.LANCHOR112,. + 0
	.type	AutoRedrawLevel, %object
	.size	AutoRedrawLevel, 128
AutoRedrawLevel:
	.space	128
	.section	.bss.CursorActiveFieldFound,"aw",%nobits
	.set	.LANCHOR104,. + 0
	.type	CursorActiveFieldFound, %object
	.size	CursorActiveFieldFound, 1
CursorActiveFieldFound:
	.space	1
	.section	.bss.CursorInUse,"aw",%nobits
	.set	.LANCHOR102,. + 0
	.type	CursorInUse, %object
	.size	CursorInUse, 1
CursorInUse:
	.space	1
	.section	.bss.GuiLib_LanguageIndex,"aw",%nobits
	.align	1
	.set	.LANCHOR123,. + 0
	.type	GuiLib_LanguageIndex, %object
	.size	GuiLib_LanguageIndex, 2
GuiLib_LanguageIndex:
	.space	2
	.section	.bss.ItemDataBufCnt,"aw",%nobits
	.align	1
	.set	.LANCHOR21,. + 0
	.type	ItemDataBufCnt, %object
	.size	ItemDataBufCnt, 2
ItemDataBufCnt:
	.space	2
	.section	.bss.X2VarType,"aw",%nobits
	.set	.LANCHOR52,. + 0
	.type	X2VarType, %object
	.size	X2VarType, 1
X2VarType:
	.space	1
	.section	.bss.Y1MemoryWrite,"aw",%nobits
	.set	.LANCHOR34,. + 0
	.type	Y1MemoryWrite, %object
	.size	Y1MemoryWrite, 1
Y1MemoryWrite:
	.space	1
	.section	.bss.DrawnY2,"aw",%nobits
	.align	1
	.set	.LANCHOR15,. + 0
	.type	DrawnY2, %object
	.size	DrawnY2, 2
DrawnY2:
	.space	2
	.section	.bss.Y2VarType,"aw",%nobits
	.set	.LANCHOR53,. + 0
	.type	Y2VarType, %object
	.size	Y2VarType, 1
Y2VarType:
	.space	1
	.section	.bss.MarkedAs,"aw",%nobits
	.set	.LANCHOR54,. + 0
	.type	MarkedAs, %object
	.size	MarkedAs, 1
MarkedAs:
	.space	1
	.section	.bss.Y2MemoryWrite,"aw",%nobits
	.set	.LANCHOR38,. + 0
	.type	Y2MemoryWrite, %object
	.size	Y2MemoryWrite, 1
Y2MemoryWrite:
	.space	1
	.section	.bss.X1MemoryWrite,"aw",%nobits
	.set	.LANCHOR32,. + 0
	.type	X1MemoryWrite, %object
	.size	X1MemoryWrite, 1
X1MemoryWrite:
	.space	1
	.section	.bss.GuiLib_LanguageCharSet,"aw",%nobits
	.set	.LANCHOR140,. + 0
	.type	GuiLib_LanguageCharSet, %object
	.size	GuiLib_LanguageCharSet, 1
GuiLib_LanguageCharSet:
	.space	1
	.section	.bss.BackgrBitmapAry,"aw",%nobits
	.align	2
	.set	.LANCHOR117,. + 0
	.type	BackgrBitmapAry, %object
	.size	BackgrBitmapAry, 80
BackgrBitmapAry:
	.space	80
	.section	.bss.ActiveAreaX2,"aw",%nobits
	.align	1
	.set	.LANCHOR120,. + 0
	.type	ActiveAreaX2, %object
	.size	ActiveAreaX2, 2
ActiveAreaX2:
	.space	2
	.section	.bss.BlinkBoxY1,"aw",%nobits
	.align	1
	.set	.LANCHOR71,. + 0
	.type	BlinkBoxY1, %object
	.size	BlinkBoxY1, 2
BlinkBoxY1:
	.space	2
	.section	.bss.BlinkBoxY2,"aw",%nobits
	.align	1
	.set	.LANCHOR73,. + 0
	.type	BlinkBoxY2, %object
	.size	BlinkBoxY2, 2
BlinkBoxY2:
	.space	2
	.section	.bss.BlinkTextItems,"aw",%nobits
	.align	2
	.set	.LANCHOR84,. + 0
	.type	BlinkTextItems, %object
	.size	BlinkTextItems, 60
BlinkTextItems:
	.space	60
	.section	.bss.FontWriteX2,"aw",%nobits
	.align	1
	.set	.LANCHOR82,. + 0
	.type	FontWriteX2, %object
	.size	FontWriteX2, 2
FontWriteX2:
	.space	2
	.section	.bss.DisplayActiveAreaX1,"aw",%nobits
	.align	1
	.set	.LANCHOR136,. + 0
	.type	DisplayActiveAreaX1, %object
	.size	DisplayActiveAreaX1, 2
DisplayActiveAreaX1:
	.space	2
	.section	.bss.X2Mode,"aw",%nobits
	.set	.LANCHOR44,. + 0
	.type	X2Mode, %object
	.size	X2Mode, 1
X2Mode:
	.space	1
	.section	.bss.InsideScrollLine,"aw",%nobits
	.set	.LANCHOR107,. + 0
	.type	InsideScrollLine, %object
	.size	InsideScrollLine, 1
InsideScrollLine:
	.space	1
	.section	.bss.ScrollNoOfLines,"aw",%nobits
	.align	1
	.set	.LANCHOR87,. + 0
	.type	ScrollNoOfLines, %object
	.size	ScrollNoOfLines, 2
ScrollNoOfLines:
	.space	2
	.section	.bss.Dummy1_8U,"aw",%nobits
	.type	Dummy1_8U, %object
	.size	Dummy1_8U, 1
Dummy1_8U:
	.space	1
	.section	.bss.GuiLib_ActiveCursorFieldNo,"aw",%nobits
	.align	1
	.set	.LANCHOR103,. + 0
	.type	GuiLib_ActiveCursorFieldNo, %object
	.size	GuiLib_ActiveCursorFieldNo, 2
GuiLib_ActiveCursorFieldNo:
	.space	2
	.section	.bss.TextPsMode,"aw",%nobits
	.set	.LANCHOR6,. + 0
	.type	TextPsMode, %object
	.size	TextPsMode, 2001
TextPsMode:
	.space	2001
	.section	.bss.DisplayActiveAreaX2,"aw",%nobits
	.align	1
	.set	.LANCHOR138,. + 0
	.type	DisplayActiveAreaX2, %object
	.size	DisplayActiveAreaX2, 2
DisplayActiveAreaX2:
	.space	2
	.section	.bss.CursorItems,"aw",%nobits
	.align	2
	.set	.LANCHOR108,. + 0
	.type	CursorItems, %object
	.size	CursorItems, 10000
CursorItems:
	.space	10000
	.section	.bss.BlinkBoxInverted,"aw",%nobits
	.set	.LANCHOR74,. + 0
	.type	BlinkBoxInverted, %object
	.size	BlinkBoxInverted, 1
BlinkBoxInverted:
	.space	1
	.section	.bss.ClippingX1,"aw",%nobits
	.align	1
	.set	.LANCHOR60,. + 0
	.type	ClippingX1, %object
	.size	ClippingX1, 2
ClippingX1:
	.space	2
	.section	.bss.InvertBoxY1,"aw",%nobits
	.align	1
	.set	.LANCHOR76,. + 0
	.type	InvertBoxY1, %object
	.size	InvertBoxY1, 2
InvertBoxY1:
	.space	2
	.section	.bss.InvertBoxY2,"aw",%nobits
	.align	1
	.set	.LANCHOR78,. + 0
	.type	InvertBoxY2, %object
	.size	InvertBoxY2, 2
InvertBoxY2:
	.space	2
	.section	.bss.ItemY1,"aw",%nobits
	.align	1
	.set	.LANCHOR47,. + 0
	.type	ItemY1, %object
	.size	ItemY1, 2
ItemY1:
	.space	2
	.section	.bss.ItemY2,"aw",%nobits
	.align	1
	.set	.LANCHOR49,. + 0
	.type	ItemY2, %object
	.size	ItemY2, 2
ItemY2:
	.space	2
	.section	.bss.TopLevelStructure,"aw",%nobits
	.align	2
	.set	.LANCHOR135,. + 0
	.type	TopLevelStructure, %object
	.size	TopLevelStructure, 4
TopLevelStructure:
	.space	4
	.section	.bss.ClippingX2,"aw",%nobits
	.align	1
	.set	.LANCHOR59,. + 0
	.type	ClippingX2, %object
	.size	ClippingX2, 2
ClippingX2:
	.space	2
	.section	.bss.DisplayActiveAreaY2,"aw",%nobits
	.align	1
	.set	.LANCHOR139,. + 0
	.type	DisplayActiveAreaY2, %object
	.size	DisplayActiveAreaY2, 2
DisplayActiveAreaY2:
	.space	2
	.section	.bss.BbY1,"aw",%nobits
	.align	1
	.set	.LANCHOR18,. + 0
	.type	BbY1, %object
	.size	BbY1, 2
BbY1:
	.space	2
	.section	.bss.BbY2,"aw",%nobits
	.align	1
	.set	.LANCHOR19,. + 0
	.type	BbY2, %object
	.size	BbY2, 2
BbY2:
	.space	2
	.section	.bss.ScrollBarMarkerBackColor,"aw",%nobits
	.set	.LANCHOR94,. + 0
	.type	ScrollBarMarkerBackColor, %object
	.size	ScrollBarMarkerBackColor, 1
ScrollBarMarkerBackColor:
	.space	1
	.section	.bss.ScrollBoxX2,"aw",%nobits
	.align	1
	.set	.LANCHOR128,. + 0
	.type	ScrollBoxX2, %object
	.size	ScrollBoxX2, 2
ScrollBoxX2:
	.space	2
	.section	.bss.ClippingTotal,"aw",%nobits
	.set	.LANCHOR58,. + 0
	.type	ClippingTotal, %object
	.size	ClippingTotal, 1
ClippingTotal:
	.space	1
	.section	.bss.TextCharPtrAry,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	TextCharPtrAry, %object
	.size	TextCharPtrAry, 8004
TextCharPtrAry:
	.space	8004
	.section	.bss.ScrLineDy,"aw",%nobits
	.set	.LANCHOR55,. + 0
	.type	ScrLineDy, %object
	.size	ScrLineDy, 1
ScrLineDy:
	.space	1
	.section	.bss.BlinkBoxState,"aw",%nobits
	.align	1
	.set	.LANCHOR142,. + 0
	.type	BlinkBoxState, %object
	.size	BlinkBoxState, 2
BlinkBoxState:
	.space	2
	.section	.bss.Y1Mode,"aw",%nobits
	.set	.LANCHOR43,. + 0
	.type	Y1Mode, %object
	.size	Y1Mode, 1
Y1Mode:
	.space	1
	.section	.rodata.Color4BitPixelPattern,"a",%progbits
	.type	Color4BitPixelPattern, %object
	.size	Color4BitPixelPattern, 64
Color4BitPixelPattern:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	0
	.byte	16
	.byte	32
	.byte	48
	.byte	64
	.byte	80
	.byte	96
	.byte	112
	.byte	-128
	.byte	-112
	.byte	-96
	.byte	-80
	.byte	-64
	.byte	-48
	.byte	-32
	.byte	-16
	.byte	0
	.byte	17
	.byte	34
	.byte	51
	.byte	68
	.byte	85
	.byte	102
	.byte	119
	.byte	-120
	.byte	-103
	.byte	-86
	.byte	-69
	.byte	-52
	.byte	-35
	.byte	-18
	.byte	-1
	.section	.bss.Dummy3_8U,"aw",%nobits
	.type	Dummy3_8U, %object
	.size	Dummy3_8U, 1
Dummy3_8U:
	.space	1
	.section	.bss.CoordOrigoX,"aw",%nobits
	.align	1
	.set	.LANCHOR63,. + 0
	.type	CoordOrigoX, %object
	.size	CoordOrigoX, 2
CoordOrigoX:
	.space	2
	.section	.bss.CoordOrigoY,"aw",%nobits
	.align	1
	.set	.LANCHOR64,. + 0
	.type	CoordOrigoY, %object
	.size	CoordOrigoY, 2
CoordOrigoY:
	.space	2
	.section	.bss.DisplayOrigoX,"aw",%nobits
	.align	1
	.set	.LANCHOR99,. + 0
	.type	DisplayOrigoX, %object
	.size	DisplayOrigoX, 2
DisplayOrigoX:
	.space	2
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI2-.LFB8
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI3-.LFB9
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.byte	0x4
	.4byte	.LCFI4-.LFB60
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.byte	0x4
	.4byte	.LCFI6-.LFB63
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.byte	0x4
	.4byte	.LCFI7-.LFB64
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x82
	.uleb128 0xa
	.byte	0x81
	.uleb128 0xb
	.byte	0x80
	.uleb128 0xc
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.byte	0x4
	.4byte	.LCFI8-.LFB66
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.byte	0x4
	.4byte	.LCFI9-.LFB69
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB77
	.4byte	.LFE77-.LFB77
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB80
	.4byte	.LFE80-.LFB80
	.byte	0x4
	.4byte	.LCFI10-.LFB80
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB146
	.4byte	.LFE146-.LFB146
	.byte	0x4
	.4byte	.LCFI11-.LFB146
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.byte	0x4
	.4byte	.LCFI12-.LFB74
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI14-.LFB2
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI15-.LFB6
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB165
	.4byte	.LFE165-.LFB165
	.byte	0x4
	.4byte	.LCFI16-.LFB165
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xe
	.uleb128 0x68
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB167
	.4byte	.LFE167-.LFB167
	.byte	0x4
	.4byte	.LCFI18-.LFB167
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xe
	.uleb128 0x70
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.byte	0x4
	.4byte	.LCFI20-.LFB62
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.byte	0x4
	.4byte	.LCFI22-.LFB68
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB170
	.4byte	.LFE170-.LFB170
	.byte	0x4
	.4byte	.LCFI23-.LFB170
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB129
	.4byte	.LFE129-.LFB129
	.byte	0x4
	.4byte	.LCFI24-.LFB129
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB172
	.4byte	.LFE172-.LFB172
	.byte	0x4
	.4byte	.LCFI25-.LFB172
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB186
	.4byte	.LFE186-.LFB186
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI26-.LFB12
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.byte	0x4
	.4byte	.LCFI28-.LFB112
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB118
	.4byte	.LFE118-.LFB118
	.byte	0x4
	.4byte	.LCFI29-.LFB118
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI30-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI31-.LFB15
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI32-.LFB16
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI33-.LFB17
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI34-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI35-.LFB19
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI36-.LFB28
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI37-.LFB26
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE86:
.LSFDE88:
	.4byte	.LEFDE88-.LASFDE88
.LASFDE88:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.align	2
.LEFDE88:
.LSFDE90:
	.4byte	.LEFDE90-.LASFDE90
.LASFDE90:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.align	2
.LEFDE90:
.LSFDE92:
	.4byte	.LEFDE92-.LASFDE92
.LASFDE92:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI38-.LFB30
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE92:
.LSFDE94:
	.4byte	.LEFDE94-.LASFDE94
.LASFDE94:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI39-.LFB31
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE94:
.LSFDE96:
	.4byte	.LEFDE96-.LASFDE96
.LASFDE96:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI40-.LFB32
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE96:
.LSFDE98:
	.4byte	.LEFDE98-.LASFDE98
.LASFDE98:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI41-.LFB33
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE98:
.LSFDE100:
	.4byte	.LEFDE100-.LASFDE100
.LASFDE100:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI42-.LFB34
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE100:
.LSFDE102:
	.4byte	.LEFDE102-.LASFDE102
.LASFDE102:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI43-.LFB35
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE102:
.LSFDE104:
	.4byte	.LEFDE104-.LASFDE104
.LASFDE104:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI44-.LFB36
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE104:
.LSFDE106:
	.4byte	.LEFDE106-.LASFDE106
.LASFDE106:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI45-.LFB37
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE106:
.LSFDE108:
	.4byte	.LEFDE108-.LASFDE108
.LASFDE108:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.align	2
.LEFDE108:
.LSFDE110:
	.4byte	.LEFDE110-.LASFDE110
.LASFDE110:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI46-.LFB41
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE110:
.LSFDE112:
	.4byte	.LEFDE112-.LASFDE112
.LASFDE112:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.align	2
.LEFDE112:
.LSFDE114:
	.4byte	.LEFDE114-.LASFDE114
.LASFDE114:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI47-.LFB43
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE114:
.LSFDE116:
	.4byte	.LEFDE116-.LASFDE116
.LASFDE116:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.byte	0x4
	.4byte	.LCFI48-.LFB44
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE116:
.LSFDE118:
	.4byte	.LEFDE118-.LASFDE118
.LASFDE118:
	.4byte	.Lframe0
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.align	2
.LEFDE118:
.LSFDE120:
	.4byte	.LEFDE120-.LASFDE120
.LASFDE120:
	.4byte	.Lframe0
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.align	2
.LEFDE120:
.LSFDE122:
	.4byte	.LEFDE122-.LASFDE122
.LASFDE122:
	.4byte	.Lframe0
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.byte	0x4
	.4byte	.LCFI49-.LFB47
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x83
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE122:
.LSFDE124:
	.4byte	.LEFDE124-.LASFDE124
.LASFDE124:
	.4byte	.Lframe0
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.byte	0x4
	.4byte	.LCFI50-.LFB48
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI51-.LCFI50
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE124:
.LSFDE126:
	.4byte	.LEFDE126-.LASFDE126
.LASFDE126:
	.4byte	.Lframe0
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.byte	0x4
	.4byte	.LCFI52-.LFB49
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE126:
.LSFDE128:
	.4byte	.LEFDE128-.LASFDE128
.LASFDE128:
	.4byte	.Lframe0
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.byte	0x4
	.4byte	.LCFI53-.LFB50
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE128:
.LSFDE130:
	.4byte	.LEFDE130-.LASFDE130
.LASFDE130:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI54-.LFB38
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI55-.LCFI54
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE130:
.LSFDE132:
	.4byte	.LEFDE132-.LASFDE132
.LASFDE132:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI56-.LFB39
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE132:
.LSFDE134:
	.4byte	.LEFDE134-.LASFDE134
.LASFDE134:
	.4byte	.Lframe0
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.byte	0x4
	.4byte	.LCFI57-.LFB51
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI58-.LCFI57
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE134:
.LSFDE136:
	.4byte	.LEFDE136-.LASFDE136
.LASFDE136:
	.4byte	.Lframe0
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.byte	0x4
	.4byte	.LCFI59-.LFB52
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE136:
.LSFDE138:
	.4byte	.LEFDE138-.LASFDE138
.LASFDE138:
	.4byte	.Lframe0
	.4byte	.LFB71
	.4byte	.LFE71-.LFB71
	.byte	0x4
	.4byte	.LCFI60-.LFB71
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI61-.LCFI60
	.byte	0xe
	.uleb128 0xfd4
	.byte	0x4
	.4byte	.LCFI62-.LCFI61
	.byte	0xe
	.uleb128 0xfd8
	.align	2
.LEFDE138:
.LSFDE140:
	.4byte	.LEFDE140-.LASFDE140
.LASFDE140:
	.4byte	.Lframe0
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.byte	0x4
	.4byte	.LCFI63-.LFB72
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI64-.LCFI63
	.byte	0xe
	.uleb128 0x11e4
	.byte	0x4
	.4byte	.LCFI65-.LCFI64
	.byte	0xe
	.uleb128 0x1214
	.align	2
.LEFDE140:
.LSFDE142:
	.4byte	.LEFDE142-.LASFDE142
.LASFDE142:
	.4byte	.Lframe0
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.byte	0x4
	.4byte	.LCFI66-.LFB70
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE142:
.LSFDE144:
	.4byte	.LEFDE144-.LASFDE144
.LASFDE144:
	.4byte	.Lframe0
	.4byte	.LFB128
	.4byte	.LFE128-.LFB128
	.byte	0x4
	.4byte	.LCFI67-.LFB128
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI68-.LCFI67
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE144:
.LSFDE146:
	.4byte	.LEFDE146-.LASFDE146
.LASFDE146:
	.4byte	.Lframe0
	.4byte	.LFB93
	.4byte	.LFE93-.LFB93
	.byte	0x4
	.4byte	.LCFI69-.LFB93
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE146:
.LSFDE148:
	.4byte	.LEFDE148-.LASFDE148
.LASFDE148:
	.4byte	.Lframe0
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.byte	0x4
	.4byte	.LCFI70-.LFB53
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI71-.LCFI70
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE148:
.LSFDE150:
	.4byte	.LEFDE150-.LASFDE150
.LASFDE150:
	.4byte	.Lframe0
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.byte	0x4
	.4byte	.LCFI72-.LFB54
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE150:
.LSFDE152:
	.4byte	.LEFDE152-.LASFDE152
.LASFDE152:
	.4byte	.Lframe0
	.4byte	.LFB79
	.4byte	.LFE79-.LFB79
	.byte	0x4
	.4byte	.LCFI73-.LFB79
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI74-.LCFI73
	.byte	0xe
	.uleb128 0x6c
	.align	2
.LEFDE152:
.LSFDE154:
	.4byte	.LEFDE154-.LASFDE154
.LASFDE154:
	.4byte	.Lframe0
	.4byte	.LFB81
	.4byte	.LFE81-.LFB81
	.byte	0x4
	.4byte	.LCFI75-.LFB81
	.byte	0xe
	.uleb128 0x34
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x83
	.uleb128 0xa
	.byte	0x82
	.uleb128 0xb
	.byte	0x81
	.uleb128 0xc
	.byte	0x80
	.uleb128 0xd
	.align	2
.LEFDE154:
.LSFDE156:
	.4byte	.LEFDE156-.LASFDE156
.LASFDE156:
	.4byte	.Lframe0
	.4byte	.LFB126
	.4byte	.LFE126-.LFB126
	.byte	0x4
	.4byte	.LCFI76-.LFB126
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x82
	.uleb128 0xa
	.byte	0x81
	.uleb128 0xb
	.byte	0x80
	.uleb128 0xc
	.align	2
.LEFDE156:
.LSFDE158:
	.4byte	.LEFDE158-.LASFDE158
.LASFDE158:
	.4byte	.Lframe0
	.4byte	.LFB130
	.4byte	.LFE130-.LFB130
	.byte	0x4
	.4byte	.LCFI77-.LFB130
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI78-.LCFI77
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE158:
.LSFDE160:
	.4byte	.LEFDE160-.LASFDE160
.LASFDE160:
	.4byte	.Lframe0
	.4byte	.LFB147
	.4byte	.LFE147-.LFB147
	.byte	0x4
	.4byte	.LCFI79-.LFB147
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE160:
.LSFDE162:
	.4byte	.LEFDE162-.LASFDE162
.LASFDE162:
	.4byte	.Lframe0
	.4byte	.LFB96
	.4byte	.LFE96-.LFB96
	.byte	0x4
	.4byte	.LCFI80-.LFB96
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE162:
.LSFDE164:
	.4byte	.LEFDE164-.LASFDE164
.LASFDE164:
	.4byte	.Lframe0
	.4byte	.LFB97
	.4byte	.LFE97-.LFB97
	.byte	0x4
	.4byte	.LCFI81-.LFB97
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE164:
.LSFDE166:
	.4byte	.LEFDE166-.LASFDE166
.LASFDE166:
	.4byte	.Lframe0
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.byte	0x4
	.4byte	.LCFI82-.LFB55
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE166:
.LSFDE168:
	.4byte	.LEFDE168-.LASFDE168
.LASFDE168:
	.4byte	.Lframe0
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.byte	0x4
	.4byte	.LCFI83-.LFB56
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE168:
.LSFDE170:
	.4byte	.LEFDE170-.LASFDE170
.LASFDE170:
	.4byte	.Lframe0
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.byte	0x4
	.4byte	.LCFI84-.LFB78
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x83
	.uleb128 0x8
	.byte	0x82
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE170:
.LSFDE172:
	.4byte	.LEFDE172-.LASFDE172
.LASFDE172:
	.4byte	.Lframe0
	.4byte	.LFB104
	.4byte	.LFE104-.LFB104
	.byte	0x4
	.4byte	.LCFI85-.LFB104
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE172:
.LSFDE174:
	.4byte	.LEFDE174-.LASFDE174
.LASFDE174:
	.4byte	.Lframe0
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.byte	0x4
	.4byte	.LCFI86-.LFB57
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE174:
.LSFDE176:
	.4byte	.LEFDE176-.LASFDE176
.LASFDE176:
	.4byte	.Lframe0
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.byte	0x4
	.4byte	.LCFI87-.LFB59
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE176:
.LSFDE178:
	.4byte	.LEFDE178-.LASFDE178
.LASFDE178:
	.4byte	.Lframe0
	.4byte	.LFB82
	.4byte	.LFE82-.LFB82
	.byte	0x4
	.4byte	.LCFI88-.LFB82
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE178:
.LSFDE180:
	.4byte	.LEFDE180-.LASFDE180
.LASFDE180:
	.4byte	.Lframe0
	.4byte	.LFB83
	.4byte	.LFE83-.LFB83
	.align	2
.LEFDE180:
.LSFDE182:
	.4byte	.LEFDE182-.LASFDE182
.LASFDE182:
	.4byte	.Lframe0
	.4byte	.LFB84
	.4byte	.LFE84-.LFB84
	.byte	0x4
	.4byte	.LCFI89-.LFB84
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE182:
.LSFDE184:
	.4byte	.LEFDE184-.LASFDE184
.LASFDE184:
	.4byte	.Lframe0
	.4byte	.LFB85
	.4byte	.LFE85-.LFB85
	.align	2
.LEFDE184:
.LSFDE186:
	.4byte	.LEFDE186-.LASFDE186
.LASFDE186:
	.4byte	.Lframe0
	.4byte	.LFB86
	.4byte	.LFE86-.LFB86
	.byte	0x4
	.4byte	.LCFI90-.LFB86
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE186:
.LSFDE188:
	.4byte	.LEFDE188-.LASFDE188
.LASFDE188:
	.4byte	.Lframe0
	.4byte	.LFB88
	.4byte	.LFE88-.LFB88
	.byte	0x4
	.4byte	.LCFI91-.LFB88
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE188:
.LSFDE190:
	.4byte	.LEFDE190-.LASFDE190
.LASFDE190:
	.4byte	.Lframe0
	.4byte	.LFB89
	.4byte	.LFE89-.LFB89
	.byte	0x4
	.4byte	.LCFI92-.LFB89
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE190:
.LSFDE192:
	.4byte	.LEFDE192-.LASFDE192
.LASFDE192:
	.4byte	.Lframe0
	.4byte	.LFB90
	.4byte	.LFE90-.LFB90
	.align	2
.LEFDE192:
.LSFDE194:
	.4byte	.LEFDE194-.LASFDE194
.LASFDE194:
	.4byte	.Lframe0
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.byte	0x4
	.4byte	.LCFI93-.LFB58
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE194:
.LSFDE196:
	.4byte	.LEFDE196-.LASFDE196
.LASFDE196:
	.4byte	.Lframe0
	.4byte	.LFB92
	.4byte	.LFE92-.LFB92
	.align	2
.LEFDE196:
.LSFDE198:
	.4byte	.LEFDE198-.LASFDE198
.LASFDE198:
	.4byte	.Lframe0
	.4byte	.LFB94
	.4byte	.LFE94-.LFB94
	.align	2
.LEFDE198:
.LSFDE200:
	.4byte	.LEFDE200-.LASFDE200
.LASFDE200:
	.4byte	.Lframe0
	.4byte	.LFB98
	.4byte	.LFE98-.LFB98
	.byte	0x4
	.4byte	.LCFI94-.LFB98
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE200:
.LSFDE202:
	.4byte	.LEFDE202-.LASFDE202
.LASFDE202:
	.4byte	.Lframe0
	.4byte	.LFB99
	.4byte	.LFE99-.LFB99
	.byte	0x4
	.4byte	.LCFI95-.LFB99
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE202:
.LSFDE204:
	.4byte	.LEFDE204-.LASFDE204
.LASFDE204:
	.4byte	.Lframe0
	.4byte	.LFB100
	.4byte	.LFE100-.LFB100
	.byte	0x4
	.4byte	.LCFI96-.LFB100
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE204:
.LSFDE206:
	.4byte	.LEFDE206-.LASFDE206
.LASFDE206:
	.4byte	.Lframe0
	.4byte	.LFB101
	.4byte	.LFE101-.LFB101
	.byte	0x4
	.4byte	.LCFI97-.LFB101
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE206:
.LSFDE208:
	.4byte	.LEFDE208-.LASFDE208
.LASFDE208:
	.4byte	.Lframe0
	.4byte	.LFB102
	.4byte	.LFE102-.LFB102
	.byte	0x4
	.4byte	.LCFI98-.LFB102
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE208:
.LSFDE210:
	.4byte	.LEFDE210-.LASFDE210
.LASFDE210:
	.4byte	.Lframe0
	.4byte	.LFB103
	.4byte	.LFE103-.LFB103
	.byte	0x4
	.4byte	.LCFI99-.LFB103
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.align	2
.LEFDE210:
.LSFDE212:
	.4byte	.LEFDE212-.LASFDE212
.LASFDE212:
	.4byte	.Lframe0
	.4byte	.LFB105
	.4byte	.LFE105-.LFB105
	.byte	0x4
	.4byte	.LCFI100-.LFB105
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE212:
.LSFDE214:
	.4byte	.LEFDE214-.LASFDE214
.LASFDE214:
	.4byte	.Lframe0
	.4byte	.LFB106
	.4byte	.LFE106-.LFB106
	.align	2
.LEFDE214:
.LSFDE216:
	.4byte	.LEFDE216-.LASFDE216
.LASFDE216:
	.4byte	.Lframe0
	.4byte	.LFB107
	.4byte	.LFE107-.LFB107
	.byte	0x4
	.4byte	.LCFI101-.LFB107
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE216:
.LSFDE218:
	.4byte	.LEFDE218-.LASFDE218
.LASFDE218:
	.4byte	.Lframe0
	.4byte	.LFB108
	.4byte	.LFE108-.LFB108
	.byte	0x4
	.4byte	.LCFI102-.LFB108
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE218:
.LSFDE220:
	.4byte	.LEFDE220-.LASFDE220
.LASFDE220:
	.4byte	.Lframe0
	.4byte	.LFB109
	.4byte	.LFE109-.LFB109
	.byte	0x4
	.4byte	.LCFI103-.LFB109
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE220:
.LSFDE222:
	.4byte	.LEFDE222-.LASFDE222
.LASFDE222:
	.4byte	.Lframe0
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.byte	0x4
	.4byte	.LCFI104-.LFB110
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE222:
.LSFDE224:
	.4byte	.LEFDE224-.LASFDE224
.LASFDE224:
	.4byte	.Lframe0
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.byte	0x4
	.4byte	.LCFI105-.LFB111
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE224:
.LSFDE226:
	.4byte	.LEFDE226-.LASFDE226
.LASFDE226:
	.4byte	.Lframe0
	.4byte	.LFB115
	.4byte	.LFE115-.LFB115
	.byte	0x4
	.4byte	.LCFI106-.LFB115
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI107-.LCFI106
	.byte	0xe
	.uleb128 0x11e4
	.byte	0x4
	.4byte	.LCFI108-.LCFI107
	.byte	0xe
	.uleb128 0x11fc
	.align	2
.LEFDE226:
.LSFDE228:
	.4byte	.LEFDE228-.LASFDE228
.LASFDE228:
	.4byte	.Lframe0
	.4byte	.LFB116
	.4byte	.LFE116-.LFB116
	.byte	0x4
	.4byte	.LCFI109-.LFB116
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE228:
.LSFDE230:
	.4byte	.LEFDE230-.LASFDE230
.LASFDE230:
	.4byte	.Lframe0
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.byte	0x4
	.4byte	.LCFI110-.LFB114
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE230:
.LSFDE232:
	.4byte	.LEFDE232-.LASFDE232
.LASFDE232:
	.4byte	.Lframe0
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.byte	0x4
	.4byte	.LCFI111-.LFB113
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE232:
.LSFDE234:
	.4byte	.LEFDE234-.LASFDE234
.LASFDE234:
	.4byte	.Lframe0
	.4byte	.LFB117
	.4byte	.LFE117-.LFB117
	.align	2
.LEFDE234:
.LSFDE236:
	.4byte	.LEFDE236-.LASFDE236
.LASFDE236:
	.4byte	.Lframe0
	.4byte	.LFB120
	.4byte	.LFE120-.LFB120
	.align	2
.LEFDE236:
.LSFDE238:
	.4byte	.LEFDE238-.LASFDE238
.LASFDE238:
	.4byte	.Lframe0
	.4byte	.LFB119
	.4byte	.LFE119-.LFB119
	.byte	0x4
	.4byte	.LCFI112-.LFB119
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE238:
.LSFDE240:
	.4byte	.LEFDE240-.LASFDE240
.LASFDE240:
	.4byte	.Lframe0
	.4byte	.LFB121
	.4byte	.LFE121-.LFB121
	.byte	0x4
	.4byte	.LCFI113-.LFB121
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE240:
.LSFDE242:
	.4byte	.LEFDE242-.LASFDE242
.LASFDE242:
	.4byte	.Lframe0
	.4byte	.LFB122
	.4byte	.LFE122-.LFB122
	.byte	0x4
	.4byte	.LCFI114-.LFB122
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE242:
.LSFDE244:
	.4byte	.LEFDE244-.LASFDE244
.LASFDE244:
	.4byte	.Lframe0
	.4byte	.LFB123
	.4byte	.LFE123-.LFB123
	.byte	0x4
	.4byte	.LCFI115-.LFB123
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI116-.LCFI115
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE244:
.LSFDE246:
	.4byte	.LEFDE246-.LASFDE246
.LASFDE246:
	.4byte	.Lframe0
	.4byte	.LFB124
	.4byte	.LFE124-.LFB124
	.byte	0x4
	.4byte	.LCFI117-.LFB124
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI118-.LCFI117
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE246:
.LSFDE248:
	.4byte	.LEFDE248-.LASFDE248
.LASFDE248:
	.4byte	.Lframe0
	.4byte	.LFB125
	.4byte	.LFE125-.LFB125
	.byte	0x4
	.4byte	.LCFI119-.LFB125
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE248:
.LSFDE250:
	.4byte	.LEFDE250-.LASFDE250
.LASFDE250:
	.4byte	.Lframe0
	.4byte	.LFB132
	.4byte	.LFE132-.LFB132
	.byte	0x4
	.4byte	.LCFI120-.LFB132
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI121-.LCFI120
	.byte	0xe
	.uleb128 0xc4
	.align	2
.LEFDE250:
.LSFDE252:
	.4byte	.LEFDE252-.LASFDE252
.LASFDE252:
	.4byte	.Lframe0
	.4byte	.LFB131
	.4byte	.LFE131-.LFB131
	.byte	0x4
	.4byte	.LCFI122-.LFB131
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI123-.LCFI122
	.byte	0xe
	.uleb128 0xa0
	.align	2
.LEFDE252:
.LSFDE254:
	.4byte	.LEFDE254-.LASFDE254
.LASFDE254:
	.4byte	.Lframe0
	.4byte	.LFB133
	.4byte	.LFE133-.LFB133
	.byte	0x4
	.4byte	.LCFI124-.LFB133
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI125-.LCFI124
	.byte	0xe
	.uleb128 0x70
	.align	2
.LEFDE254:
.LSFDE256:
	.4byte	.LEFDE256-.LASFDE256
.LASFDE256:
	.4byte	.Lframe0
	.4byte	.LFB134
	.4byte	.LFE134-.LFB134
	.align	2
.LEFDE256:
.LSFDE258:
	.4byte	.LEFDE258-.LASFDE258
.LASFDE258:
	.4byte	.Lframe0
	.4byte	.LFB135
	.4byte	.LFE135-.LFB135
	.byte	0x4
	.4byte	.LCFI126-.LFB135
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE258:
.LSFDE260:
	.4byte	.LEFDE260-.LASFDE260
.LASFDE260:
	.4byte	.Lframe0
	.4byte	.LFB136
	.4byte	.LFE136-.LFB136
	.byte	0x4
	.4byte	.LCFI127-.LFB136
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE260:
.LSFDE262:
	.4byte	.LEFDE262-.LASFDE262
.LASFDE262:
	.4byte	.Lframe0
	.4byte	.LFB137
	.4byte	.LFE137-.LFB137
	.byte	0x4
	.4byte	.LCFI128-.LFB137
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE262:
.LSFDE264:
	.4byte	.LEFDE264-.LASFDE264
.LASFDE264:
	.4byte	.Lframe0
	.4byte	.LFB138
	.4byte	.LFE138-.LFB138
	.byte	0x4
	.4byte	.LCFI129-.LFB138
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE264:
.LSFDE266:
	.4byte	.LEFDE266-.LASFDE266
.LASFDE266:
	.4byte	.Lframe0
	.4byte	.LFB139
	.4byte	.LFE139-.LFB139
	.byte	0x4
	.4byte	.LCFI130-.LFB139
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE266:
.LSFDE268:
	.4byte	.LEFDE268-.LASFDE268
.LASFDE268:
	.4byte	.Lframe0
	.4byte	.LFB140
	.4byte	.LFE140-.LFB140
	.byte	0x4
	.4byte	.LCFI131-.LFB140
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE268:
.LSFDE270:
	.4byte	.LEFDE270-.LASFDE270
.LASFDE270:
	.4byte	.Lframe0
	.4byte	.LFB141
	.4byte	.LFE141-.LFB141
	.align	2
.LEFDE270:
.LSFDE272:
	.4byte	.LEFDE272-.LASFDE272
.LASFDE272:
	.4byte	.Lframe0
	.4byte	.LFB142
	.4byte	.LFE142-.LFB142
	.align	2
.LEFDE272:
.LSFDE274:
	.4byte	.LEFDE274-.LASFDE274
.LASFDE274:
	.4byte	.Lframe0
	.4byte	.LFB143
	.4byte	.LFE143-.LFB143
	.align	2
.LEFDE274:
.LSFDE276:
	.4byte	.LEFDE276-.LASFDE276
.LASFDE276:
	.4byte	.Lframe0
	.4byte	.LFB144
	.4byte	.LFE144-.LFB144
	.align	2
.LEFDE276:
.LSFDE278:
	.4byte	.LEFDE278-.LASFDE278
.LASFDE278:
	.4byte	.Lframe0
	.4byte	.LFB145
	.4byte	.LFE145-.LFB145
	.align	2
.LEFDE278:
.LSFDE280:
	.4byte	.LEFDE280-.LASFDE280
.LASFDE280:
	.4byte	.Lframe0
	.4byte	.LFB148
	.4byte	.LFE148-.LFB148
	.align	2
.LEFDE280:
.LSFDE282:
	.4byte	.LEFDE282-.LASFDE282
.LASFDE282:
	.4byte	.Lframe0
	.4byte	.LFB149
	.4byte	.LFE149-.LFB149
	.align	2
.LEFDE282:
.LSFDE284:
	.4byte	.LEFDE284-.LASFDE284
.LASFDE284:
	.4byte	.Lframe0
	.4byte	.LFB150
	.4byte	.LFE150-.LFB150
	.align	2
.LEFDE284:
.LSFDE286:
	.4byte	.LEFDE286-.LASFDE286
.LASFDE286:
	.4byte	.Lframe0
	.4byte	.LFB151
	.4byte	.LFE151-.LFB151
	.align	2
.LEFDE286:
.LSFDE288:
	.4byte	.LEFDE288-.LASFDE288
.LASFDE288:
	.4byte	.Lframe0
	.4byte	.LFB152
	.4byte	.LFE152-.LFB152
	.align	2
.LEFDE288:
.LSFDE290:
	.4byte	.LEFDE290-.LASFDE290
.LASFDE290:
	.4byte	.Lframe0
	.4byte	.LFB153
	.4byte	.LFE153-.LFB153
	.align	2
.LEFDE290:
.LSFDE292:
	.4byte	.LEFDE292-.LASFDE292
.LASFDE292:
	.4byte	.Lframe0
	.4byte	.LFB154
	.4byte	.LFE154-.LFB154
	.align	2
.LEFDE292:
.LSFDE294:
	.4byte	.LEFDE294-.LASFDE294
.LASFDE294:
	.4byte	.Lframe0
	.4byte	.LFB155
	.4byte	.LFE155-.LFB155
	.align	2
.LEFDE294:
.LSFDE296:
	.4byte	.LEFDE296-.LASFDE296
.LASFDE296:
	.4byte	.Lframe0
	.4byte	.LFB156
	.4byte	.LFE156-.LFB156
	.align	2
.LEFDE296:
.LSFDE298:
	.4byte	.LEFDE298-.LASFDE298
.LASFDE298:
	.4byte	.Lframe0
	.4byte	.LFB157
	.4byte	.LFE157-.LFB157
	.align	2
.LEFDE298:
.LSFDE300:
	.4byte	.LEFDE300-.LASFDE300
.LASFDE300:
	.4byte	.Lframe0
	.4byte	.LFB159
	.4byte	.LFE159-.LFB159
	.align	2
.LEFDE300:
.LSFDE302:
	.4byte	.LEFDE302-.LASFDE302
.LASFDE302:
	.4byte	.Lframe0
	.4byte	.LFB160
	.4byte	.LFE160-.LFB160
	.align	2
.LEFDE302:
.LSFDE304:
	.4byte	.LEFDE304-.LASFDE304
.LASFDE304:
	.4byte	.Lframe0
	.4byte	.LFB161
	.4byte	.LFE161-.LFB161
	.byte	0x4
	.4byte	.LCFI132-.LFB161
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE304:
.LSFDE306:
	.4byte	.LEFDE306-.LASFDE306
.LASFDE306:
	.4byte	.Lframe0
	.4byte	.LFB163
	.4byte	.LFE163-.LFB163
	.byte	0x4
	.4byte	.LCFI133-.LFB163
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI134-.LCFI133
	.byte	0xe
	.uleb128 0xa4
	.align	2
.LEFDE306:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiLib.c"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiGraph4H.c"
	.file 3 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiGraph.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xd4f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF164
	.byte	0x1
	.4byte	.LASF165
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x413
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x42e
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x2
	.byte	0xf7
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x2
	.2byte	0x2a3
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x678
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x922
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1c46
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x25b8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x2f1f
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF9
	.byte	0x3
	.byte	0x1c
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF10
	.byte	0x3
	.byte	0x6a
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF11
	.byte	0x3
	.byte	0x7d
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF12
	.byte	0x3
	.byte	0x8c
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF13
	.byte	0x3
	.byte	0x9b
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF14
	.byte	0x3
	.2byte	0x12b
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF15
	.byte	0x3
	.2byte	0x149
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x2
	.byte	0x3f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF17
	.byte	0x2
	.byte	0x71
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF18
	.byte	0x2
	.byte	0x91
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x968
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x1cfc
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x1ce0
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x8e6
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x2552
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x258a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x25cb
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x2ea4
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x1d33
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x1d59
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x1e86
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x1bb6
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x1edf
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x26d5
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x2f6c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x1d29
	.byte	0x1
	.uleb128 0x6
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x41f
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x7
	.4byte	.LASF36
	.byte	0x2
	.byte	0x56
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x7
	.4byte	.LASF37
	.byte	0x2
	.byte	0xa3
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST2
	.uleb128 0x7
	.4byte	.LASF38
	.byte	0x2
	.byte	0xd4
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST3
	.uleb128 0x6
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x5c7
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LLST4
	.uleb128 0x6
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x6fc
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LLST5
	.uleb128 0x6
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x79f
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LLST6
	.uleb128 0x6
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x8f0
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LLST7
	.uleb128 0x6
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x938
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LLST8
	.uleb128 0x8
	.4byte	.LASF44
	.byte	0x1
	.2byte	0xbbb
	.4byte	.LFB73
	.4byte	.LFE73
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.4byte	.LASF45
	.byte	0x1
	.2byte	0xe19
	.4byte	.LFB75
	.4byte	.LFE75
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.4byte	.LASF46
	.byte	0x1
	.2byte	0xe3a
	.4byte	.LFB76
	.4byte	.LFE76
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.4byte	.LASF47
	.byte	0x1
	.2byte	0xf2c
	.4byte	.LFB77
	.4byte	.LFE77
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x1318
	.4byte	.LFB80
	.4byte	.LFE80
	.4byte	.LLST9
	.uleb128 0x6
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x2e91
	.4byte	.LFB146
	.4byte	.LFE146
	.4byte	.LLST10
	.uleb128 0x6
	.4byte	.LASF50
	.byte	0x1
	.2byte	0xc42
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LLST11
	.uleb128 0x9
	.4byte	0x2a
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST12
	.uleb128 0x7
	.4byte	.LASF51
	.byte	0x2
	.byte	0x78
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST13
	.uleb128 0x9
	.4byte	0x33
	.4byte	.LFB165
	.4byte	.LFE165
	.4byte	.LLST14
	.uleb128 0x9
	.4byte	0x3b
	.4byte	.LFB167
	.4byte	.LFE167
	.4byte	.LLST15
	.uleb128 0x2
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x5e3
	.byte	0x1
	.uleb128 0x9
	.4byte	0x44
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LLST16
	.uleb128 0x9
	.4byte	0x4d
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LLST17
	.uleb128 0x9
	.4byte	0x56
	.4byte	.LFB170
	.4byte	.LFE170
	.4byte	.LLST18
	.uleb128 0x9
	.4byte	0x5f
	.4byte	.LFB129
	.4byte	.LFE129
	.4byte	.LLST19
	.uleb128 0x9
	.4byte	0x68
	.4byte	.LFB172
	.4byte	.LFE172
	.4byte	.LLST20
	.uleb128 0xa
	.4byte	0x154
	.4byte	.LFB186
	.4byte	.LFE186
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF55
	.byte	0x2
	.2byte	0x3b3
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST21
	.uleb128 0x6
	.4byte	.LASF53
	.byte	0x1
	.2byte	0x1f65
	.4byte	.LFB112
	.4byte	.LFE112
	.4byte	.LLST22
	.uleb128 0x6
	.4byte	.LASF54
	.byte	0x1
	.2byte	0x223a
	.4byte	.LFB118
	.4byte	.LFE118
	.4byte	.LLST23
	.uleb128 0x9
	.4byte	0x71
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST24
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF56
	.byte	0x3
	.byte	0x23
	.4byte	.LFB14
	.4byte	.LFE14
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF57
	.byte	0x3
	.byte	0x2a
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST25
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF58
	.byte	0x3
	.byte	0x43
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST26
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF59
	.byte	0x3
	.byte	0x4a
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST27
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF60
	.byte	0x3
	.byte	0x63
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST28
	.uleb128 0x9
	.4byte	0x7a
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST29
	.uleb128 0xa
	.4byte	0x83
	.4byte	.LFB20
	.4byte	.LFE20
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF61
	.byte	0x3
	.byte	0x84
	.4byte	.LFB21
	.4byte	.LFE21
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xa
	.4byte	0x8c
	.4byte	.LFB22
	.4byte	.LFE22
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF62
	.byte	0x3
	.byte	0x93
	.4byte	.LFB23
	.4byte	.LFE23
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xa
	.4byte	0x95
	.4byte	.LFB24
	.4byte	.LFE24
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF63
	.byte	0x3
	.byte	0xa2
	.4byte	.LFB25
	.4byte	.LFE25
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF64
	.byte	0x3
	.2byte	0x10b
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST30
	.uleb128 0xd
	.byte	0x1
	.4byte	.LASF65
	.byte	0x3
	.byte	0xaa
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST31
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF66
	.byte	0x3
	.2byte	0x114
	.4byte	.LFB29
	.4byte	.LFE29
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xc
	.byte	0x1
	.4byte	.LASF67
	.byte	0x3
	.byte	0xe3
	.4byte	.LFB27
	.4byte	.LFE27
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF68
	.byte	0x3
	.2byte	0x11d
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST32
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF69
	.byte	0x3
	.2byte	0x124
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST33
	.uleb128 0x9
	.4byte	0x9e
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST34
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF70
	.byte	0x3
	.2byte	0x140
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST35
	.uleb128 0x9
	.4byte	0xa8
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST36
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF71
	.byte	0x3
	.2byte	0x15e
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST37
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF72
	.byte	0x3
	.2byte	0x167
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST38
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF73
	.byte	0x3
	.2byte	0x174
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST39
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF74
	.byte	0x3
	.2byte	0x230
	.4byte	.LFB40
	.4byte	.LFE40
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF75
	.byte	0x3
	.2byte	0x238
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST40
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF76
	.byte	0x3
	.2byte	0x24a
	.4byte	.LFB42
	.4byte	.LFE42
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF77
	.byte	0x3
	.2byte	0x253
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST41
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF78
	.byte	0x3
	.2byte	0x264
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LLST42
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF79
	.byte	0x3
	.2byte	0x26c
	.4byte	.LFB45
	.4byte	.LFE45
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF80
	.byte	0x3
	.2byte	0x278
	.4byte	.LFB46
	.4byte	.LFE46
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF81
	.byte	0x3
	.2byte	0x282
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LLST43
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF82
	.byte	0x3
	.2byte	0x2cc
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LLST44
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF83
	.byte	0x3
	.2byte	0x31e
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LLST45
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF84
	.byte	0x3
	.2byte	0x328
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LLST46
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF85
	.byte	0x3
	.2byte	0x17d
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST47
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF86
	.byte	0x3
	.2byte	0x222
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST48
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF87
	.byte	0x3
	.2byte	0x332
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LLST49
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF88
	.byte	0x3
	.2byte	0x377
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LLST50
	.uleb128 0x6
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x972
	.4byte	.LFB71
	.4byte	.LFE71
	.4byte	.LLST51
	.uleb128 0x6
	.4byte	.LASF90
	.byte	0x1
	.2byte	0xa30
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LLST52
	.uleb128 0x9
	.4byte	0xca
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	.LLST53
	.uleb128 0x6
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x259a
	.4byte	.LFB128
	.4byte	.LFE128
	.4byte	.LLST54
	.uleb128 0x9
	.4byte	0xd3
	.4byte	.LFB93
	.4byte	.LFE93
	.4byte	.LLST55
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF92
	.byte	0x3
	.2byte	0x39b
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LLST56
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF93
	.byte	0x3
	.2byte	0x3ac
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LLST57
	.uleb128 0x2
	.4byte	.LASF94
	.byte	0x1
	.2byte	0x904
	.byte	0x1
	.uleb128 0x6
	.4byte	.LASF95
	.byte	0x1
	.2byte	0xf7e
	.4byte	.LFB79
	.4byte	.LFE79
	.4byte	.LLST58
	.uleb128 0x6
	.4byte	.LASF96
	.byte	0x1
	.2byte	0x18ee
	.4byte	.LFB81
	.4byte	.LFE81
	.4byte	.LLST59
	.uleb128 0x9
	.4byte	0xee
	.4byte	.LFB126
	.4byte	.LFE126
	.4byte	.LLST60
	.uleb128 0x9
	.4byte	0x100
	.4byte	.LFB130
	.4byte	.LFE130
	.4byte	.LLST61
	.uleb128 0x9
	.4byte	0x109
	.4byte	.LFB147
	.4byte	.LFE147
	.4byte	.LLST62
	.uleb128 0x9
	.4byte	0x112
	.4byte	.LFB96
	.4byte	.LFE96
	.4byte	.LLST63
	.uleb128 0x9
	.4byte	0x11b
	.4byte	.LFB97
	.4byte	.LFE97
	.4byte	.LLST64
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF97
	.byte	0x3
	.2byte	0x3cc
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LLST65
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF98
	.byte	0x3
	.2byte	0x3d7
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LLST66
	.uleb128 0x6
	.4byte	.LASF99
	.byte	0x1
	.2byte	0xf3b
	.4byte	.LFB78
	.4byte	.LFE78
	.4byte	.LLST67
	.uleb128 0x9
	.4byte	0x124
	.4byte	.LFB104
	.4byte	.LFE104
	.4byte	.LLST68
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF100
	.byte	0x3
	.2byte	0x3fb
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LLST69
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF101
	.byte	0x1
	.2byte	0x4ff
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LLST70
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF102
	.byte	0x1
	.2byte	0x1ad3
	.4byte	.LFB82
	.4byte	.LFE82
	.4byte	.LLST71
	.uleb128 0xa
	.4byte	0x12d
	.4byte	.LFB83
	.4byte	.LFE83
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF103
	.byte	0x1
	.2byte	0x1bc9
	.4byte	.LFB84
	.4byte	.LFE84
	.4byte	.LLST72
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF104
	.byte	0x1
	.2byte	0x1c09
	.4byte	.LFB85
	.4byte	.LFE85
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF105
	.byte	0x1
	.2byte	0x1c11
	.4byte	.LFB86
	.4byte	.LFE86
	.4byte	.LLST73
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF106
	.byte	0x1
	.2byte	0x1c94
	.4byte	.LFB88
	.4byte	.LFE88
	.4byte	.LLST74
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF107
	.byte	0x1
	.2byte	0x1cad
	.4byte	.LFB89
	.4byte	.LFE89
	.4byte	.LLST75
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x1cce
	.4byte	.LFB90
	.4byte	.LFE90
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x49a
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LLST76
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x1ce9
	.4byte	.LFB92
	.4byte	.LFE92
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x1d20
	.4byte	.LFB94
	.4byte	.LFE94
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x1d68
	.4byte	.LFB98
	.4byte	.LFE98
	.4byte	.LLST77
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x1d6f
	.4byte	.LFB99
	.4byte	.LFE99
	.4byte	.LLST78
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x1da5
	.4byte	.LFB100
	.4byte	.LFE100
	.4byte	.LLST79
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x1ddb
	.4byte	.LFB101
	.4byte	.LFE101
	.4byte	.LLST80
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x1e0a
	.4byte	.LFB102
	.4byte	.LFE102
	.4byte	.LLST81
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x1e39
	.4byte	.LFB103
	.4byte	.LFE103
	.4byte	.LLST82
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x1ec4
	.4byte	.LFB105
	.4byte	.LFE105
	.4byte	.LLST83
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x1ecf
	.4byte	.LFB106
	.4byte	.LFE106
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.4byte	0x137
	.4byte	.LFB107
	.4byte	.LFE107
	.4byte	.LLST84
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF120
	.byte	0x1
	.2byte	0x1eff
	.4byte	.LFB108
	.4byte	.LFE108
	.4byte	.LLST85
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF121
	.byte	0x1
	.2byte	0x1f1a
	.4byte	.LFB109
	.4byte	.LFE109
	.4byte	.LLST86
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF122
	.byte	0x1
	.2byte	0x1f35
	.4byte	.LFB110
	.4byte	.LFE110
	.4byte	.LLST87
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF123
	.byte	0x1
	.2byte	0x1f4c
	.4byte	.LFB111
	.4byte	.LFE111
	.4byte	.LLST88
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF124
	.byte	0x1
	.2byte	0x1f95
	.4byte	.LFB115
	.4byte	.LFE115
	.4byte	.LLST89
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF125
	.byte	0x1
	.2byte	0x220a
	.4byte	.LFB116
	.4byte	.LFE116
	.4byte	.LLST90
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF126
	.byte	0x1
	.2byte	0x1f83
	.4byte	.LFB114
	.4byte	.LFE114
	.4byte	.LLST91
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF127
	.byte	0x1
	.2byte	0x1f6d
	.4byte	.LFB113
	.4byte	.LFE113
	.4byte	.LLST92
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF128
	.byte	0x1
	.2byte	0x2226
	.4byte	.LFB117
	.4byte	.LFE117
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF129
	.byte	0x1
	.2byte	0x2251
	.4byte	.LFB120
	.4byte	.LFE120
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF130
	.byte	0x1
	.2byte	0x2242
	.4byte	.LFB119
	.4byte	.LFE119
	.4byte	.LLST93
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF131
	.byte	0x1
	.2byte	0x234a
	.4byte	.LFB121
	.4byte	.LFE121
	.4byte	.LLST94
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF132
	.byte	0x1
	.2byte	0x23e4
	.4byte	.LFB122
	.4byte	.LFE122
	.4byte	.LLST95
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF133
	.byte	0x1
	.2byte	0x2447
	.4byte	.LFB123
	.4byte	.LFE123
	.4byte	.LLST96
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF134
	.byte	0x1
	.2byte	0x24b8
	.4byte	.LFB124
	.4byte	.LFE124
	.4byte	.LLST97
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF135
	.byte	0x1
	.2byte	0x2546
	.4byte	.LFB125
	.4byte	.LFE125
	.4byte	.LLST98
	.uleb128 0x9
	.4byte	0x141
	.4byte	.LFB132
	.4byte	.LFE132
	.4byte	.LLST99
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF136
	.byte	0x1
	.2byte	0x2659
	.4byte	.LFB131
	.4byte	.LFE131
	.4byte	.LLST100
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF137
	.byte	0x1
	.2byte	0x288a
	.4byte	.LFB133
	.4byte	.LFE133
	.4byte	.LLST101
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF138
	.byte	0x1
	.2byte	0x28a0
	.4byte	.LFB134
	.4byte	.LFE134
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF139
	.byte	0x1
	.2byte	0x28ad
	.4byte	.LFB135
	.4byte	.LFE135
	.4byte	.LLST102
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF140
	.byte	0x1
	.2byte	0x290e
	.4byte	.LFB136
	.4byte	.LFE136
	.4byte	.LLST103
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF141
	.byte	0x1
	.2byte	0x2971
	.4byte	.LFB137
	.4byte	.LFE137
	.4byte	.LLST104
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF142
	.byte	0x1
	.2byte	0x2995
	.4byte	.LFB138
	.4byte	.LFE138
	.4byte	.LLST105
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF143
	.byte	0x1
	.2byte	0x29bf
	.4byte	.LFB139
	.4byte	.LFE139
	.4byte	.LLST106
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF144
	.byte	0x1
	.2byte	0x2a01
	.4byte	.LFB140
	.4byte	.LFE140
	.4byte	.LLST107
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF145
	.byte	0x1
	.2byte	0x2a1a
	.4byte	.LFB141
	.4byte	.LFE141
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF146
	.byte	0x1
	.2byte	0x2a28
	.4byte	.LFB142
	.4byte	.LFE142
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF147
	.byte	0x1
	.2byte	0x2a35
	.4byte	.LFB143
	.4byte	.LFE143
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF148
	.byte	0x1
	.2byte	0x2a48
	.4byte	.LFB144
	.4byte	.LFE144
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF149
	.byte	0x1
	.2byte	0x2a5c
	.4byte	.LFB145
	.4byte	.LFE145
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF150
	.byte	0x1
	.2byte	0x2ed7
	.4byte	.LFB148
	.4byte	.LFE148
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF151
	.byte	0x1
	.2byte	0x2ede
	.4byte	.LFB149
	.4byte	.LFE149
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF152
	.byte	0x1
	.2byte	0x2ee5
	.4byte	.LFB150
	.4byte	.LFE150
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF153
	.byte	0x1
	.2byte	0x2eec
	.4byte	.LFB151
	.4byte	.LFE151
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF154
	.byte	0x1
	.2byte	0x2ef3
	.4byte	.LFB152
	.4byte	.LFE152
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF155
	.byte	0x1
	.2byte	0x2efb
	.4byte	.LFB153
	.4byte	.LFE153
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF156
	.byte	0x1
	.2byte	0x2f02
	.4byte	.LFB154
	.4byte	.LFE154
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF157
	.byte	0x1
	.2byte	0x2f09
	.4byte	.LFB155
	.4byte	.LFE155
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF158
	.byte	0x1
	.2byte	0x2f10
	.4byte	.LFB156
	.4byte	.LFE156
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF159
	.byte	0x1
	.2byte	0x2f17
	.4byte	.LFB157
	.4byte	.LFE157
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF160
	.byte	0x1
	.2byte	0x2f3f
	.4byte	.LFB159
	.4byte	.LFE159
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xe
	.byte	0x1
	.4byte	.LASF161
	.byte	0x1
	.2byte	0x2f46
	.4byte	.LFB160
	.4byte	.LFE160
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF162
	.byte	0x1
	.2byte	0x2f4d
	.4byte	.LFB161
	.4byte	.LFE161
	.4byte	.LLST108
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF163
	.byte	0x1
	.2byte	0x2f7e
	.4byte	.LFB163
	.4byte	.LFE163
	.4byte	.LLST109
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB8
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB9
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB60
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI5
	.4byte	.LFE60
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB63
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE63
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB64
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE64
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB66
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE66
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB69
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE69
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB80
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE80
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB146
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE146
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB74
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI13
	.4byte	.LFE74
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB2
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB6
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB165
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI17
	.4byte	.LFE165
	.2byte	0x3
	.byte	0x7d
	.sleb128 104
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB167
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI19
	.4byte	.LFE167
	.2byte	0x3
	.byte	0x7d
	.sleb128 112
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB62
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI21
	.4byte	.LFE62
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB68
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LFE68
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB170
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE170
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB129
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE129
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB172
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LFE172
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB12
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI27
	.4byte	.LFE12
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB112
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LFE112
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB118
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LFE118
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB13
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB15
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB16
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB17
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB18
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB19
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB28
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB26
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB30
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB31
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB32
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB33
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB34
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB35
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB36
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB37
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB41
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB43
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB44
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LFE44
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB47
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI49
	.4byte	.LFE47
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST44:
	.4byte	.LFB48
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI51
	.4byte	.LFE48
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST45:
	.4byte	.LFB49
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI52
	.4byte	.LFE49
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST46:
	.4byte	.LFB50
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LFE50
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST47:
	.4byte	.LFB38
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI55
	.4byte	.LFE38
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST48:
	.4byte	.LFB39
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST49:
	.4byte	.LFB51
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI57
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI58
	.4byte	.LFE51
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST50:
	.4byte	.LFB52
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI59
	.4byte	.LFE52
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST51:
	.4byte	.LFB71
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI61
	.4byte	.LCFI62
	.2byte	0x3
	.byte	0x7d
	.sleb128 4052
	.4byte	.LCFI62
	.4byte	.LFE71
	.2byte	0x3
	.byte	0x7d
	.sleb128 4056
	.4byte	0
	.4byte	0
.LLST52:
	.4byte	.LFB72
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI64
	.4byte	.LCFI65
	.2byte	0x3
	.byte	0x7d
	.sleb128 4580
	.4byte	.LCFI65
	.4byte	.LFE72
	.2byte	0x3
	.byte	0x7d
	.sleb128 4628
	.4byte	0
	.4byte	0
.LLST53:
	.4byte	.LFB70
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI66
	.4byte	.LFE70
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST54:
	.4byte	.LFB128
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI67
	.4byte	.LCFI68
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI68
	.4byte	.LFE128
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST55:
	.4byte	.LFB93
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI69
	.4byte	.LFE93
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST56:
	.4byte	.LFB53
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI70
	.4byte	.LCFI71
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI71
	.4byte	.LFE53
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST57:
	.4byte	.LFB54
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI72
	.4byte	.LFE54
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST58:
	.4byte	.LFB79
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI73
	.4byte	.LCFI74
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI74
	.4byte	.LFE79
	.2byte	0x3
	.byte	0x7d
	.sleb128 108
	.4byte	0
	.4byte	0
.LLST59:
	.4byte	.LFB81
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI75
	.4byte	.LFE81
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST60:
	.4byte	.LFB126
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI76
	.4byte	.LFE126
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST61:
	.4byte	.LFB130
	.4byte	.LCFI77
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI77
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI78
	.4byte	.LFE130
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST62:
	.4byte	.LFB147
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI79
	.4byte	.LFE147
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST63:
	.4byte	.LFB96
	.4byte	.LCFI80
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI80
	.4byte	.LFE96
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST64:
	.4byte	.LFB97
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI81
	.4byte	.LFE97
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST65:
	.4byte	.LFB55
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI82
	.4byte	.LFE55
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST66:
	.4byte	.LFB56
	.4byte	.LCFI83
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI83
	.4byte	.LFE56
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST67:
	.4byte	.LFB78
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI84
	.4byte	.LFE78
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST68:
	.4byte	.LFB104
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI85
	.4byte	.LFE104
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST69:
	.4byte	.LFB57
	.4byte	.LCFI86
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI86
	.4byte	.LFE57
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST70:
	.4byte	.LFB59
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI87
	.4byte	.LFE59
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST71:
	.4byte	.LFB82
	.4byte	.LCFI88
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI88
	.4byte	.LFE82
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST72:
	.4byte	.LFB84
	.4byte	.LCFI89
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI89
	.4byte	.LFE84
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST73:
	.4byte	.LFB86
	.4byte	.LCFI90
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI90
	.4byte	.LFE86
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST74:
	.4byte	.LFB88
	.4byte	.LCFI91
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI91
	.4byte	.LFE88
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST75:
	.4byte	.LFB89
	.4byte	.LCFI92
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI92
	.4byte	.LFE89
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST76:
	.4byte	.LFB58
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI93
	.4byte	.LFE58
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST77:
	.4byte	.LFB98
	.4byte	.LCFI94
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI94
	.4byte	.LFE98
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST78:
	.4byte	.LFB99
	.4byte	.LCFI95
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI95
	.4byte	.LFE99
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST79:
	.4byte	.LFB100
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI96
	.4byte	.LFE100
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST80:
	.4byte	.LFB101
	.4byte	.LCFI97
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI97
	.4byte	.LFE101
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST81:
	.4byte	.LFB102
	.4byte	.LCFI98
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI98
	.4byte	.LFE102
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST82:
	.4byte	.LFB103
	.4byte	.LCFI99
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI99
	.4byte	.LFE103
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST83:
	.4byte	.LFB105
	.4byte	.LCFI100
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI100
	.4byte	.LFE105
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST84:
	.4byte	.LFB107
	.4byte	.LCFI101
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI101
	.4byte	.LFE107
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST85:
	.4byte	.LFB108
	.4byte	.LCFI102
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI102
	.4byte	.LFE108
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST86:
	.4byte	.LFB109
	.4byte	.LCFI103
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI103
	.4byte	.LFE109
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST87:
	.4byte	.LFB110
	.4byte	.LCFI104
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI104
	.4byte	.LFE110
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST88:
	.4byte	.LFB111
	.4byte	.LCFI105
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI105
	.4byte	.LFE111
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST89:
	.4byte	.LFB115
	.4byte	.LCFI106
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI106
	.4byte	.LCFI107
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI107
	.4byte	.LCFI108
	.2byte	0x3
	.byte	0x7d
	.sleb128 4580
	.4byte	.LCFI108
	.4byte	.LFE115
	.2byte	0x3
	.byte	0x7d
	.sleb128 4604
	.4byte	0
	.4byte	0
.LLST90:
	.4byte	.LFB116
	.4byte	.LCFI109
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI109
	.4byte	.LFE116
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST91:
	.4byte	.LFB114
	.4byte	.LCFI110
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI110
	.4byte	.LFE114
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST92:
	.4byte	.LFB113
	.4byte	.LCFI111
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI111
	.4byte	.LFE113
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST93:
	.4byte	.LFB119
	.4byte	.LCFI112
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI112
	.4byte	.LFE119
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST94:
	.4byte	.LFB121
	.4byte	.LCFI113
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI113
	.4byte	.LFE121
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST95:
	.4byte	.LFB122
	.4byte	.LCFI114
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI114
	.4byte	.LFE122
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST96:
	.4byte	.LFB123
	.4byte	.LCFI115
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI115
	.4byte	.LCFI116
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI116
	.4byte	.LFE123
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST97:
	.4byte	.LFB124
	.4byte	.LCFI117
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI117
	.4byte	.LCFI118
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI118
	.4byte	.LFE124
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST98:
	.4byte	.LFB125
	.4byte	.LCFI119
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI119
	.4byte	.LFE125
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST99:
	.4byte	.LFB132
	.4byte	.LCFI120
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI120
	.4byte	.LCFI121
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI121
	.4byte	.LFE132
	.2byte	0x3
	.byte	0x7d
	.sleb128 196
	.4byte	0
	.4byte	0
.LLST100:
	.4byte	.LFB131
	.4byte	.LCFI122
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI122
	.4byte	.LCFI123
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI123
	.4byte	.LFE131
	.2byte	0x3
	.byte	0x7d
	.sleb128 160
	.4byte	0
	.4byte	0
.LLST101:
	.4byte	.LFB133
	.4byte	.LCFI124
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI124
	.4byte	.LCFI125
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI125
	.4byte	.LFE133
	.2byte	0x3
	.byte	0x7d
	.sleb128 112
	.4byte	0
	.4byte	0
.LLST102:
	.4byte	.LFB135
	.4byte	.LCFI126
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI126
	.4byte	.LFE135
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST103:
	.4byte	.LFB136
	.4byte	.LCFI127
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI127
	.4byte	.LFE136
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST104:
	.4byte	.LFB137
	.4byte	.LCFI128
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI128
	.4byte	.LFE137
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST105:
	.4byte	.LFB138
	.4byte	.LCFI129
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI129
	.4byte	.LFE138
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST106:
	.4byte	.LFB139
	.4byte	.LCFI130
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI130
	.4byte	.LFE139
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST107:
	.4byte	.LFB140
	.4byte	.LCFI131
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI131
	.4byte	.LFE140
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST108:
	.4byte	.LFB161
	.4byte	.LCFI132
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI132
	.4byte	.LFE161
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST109:
	.4byte	.LFB163
	.4byte	.LCFI133
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI133
	.4byte	.LCFI134
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI134
	.4byte	.LFE163
	.2byte	0x3
	.byte	0x7d
	.sleb128 164
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4e4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.4byte	.LFB75
	.4byte	.LFE75-.LFB75
	.4byte	.LFB76
	.4byte	.LFE76-.LFB76
	.4byte	.LFB77
	.4byte	.LFE77-.LFB77
	.4byte	.LFB80
	.4byte	.LFE80-.LFB80
	.4byte	.LFB146
	.4byte	.LFE146-.LFB146
	.4byte	.LFB74
	.4byte	.LFE74-.LFB74
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB165
	.4byte	.LFE165-.LFB165
	.4byte	.LFB167
	.4byte	.LFE167-.LFB167
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.4byte	.LFB170
	.4byte	.LFE170-.LFB170
	.4byte	.LFB129
	.4byte	.LFE129-.LFB129
	.4byte	.LFB172
	.4byte	.LFE172-.LFB172
	.4byte	.LFB186
	.4byte	.LFE186-.LFB186
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.4byte	.LFB118
	.4byte	.LFE118-.LFB118
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.4byte	.LFB71
	.4byte	.LFE71-.LFB71
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.4byte	.LFB128
	.4byte	.LFE128-.LFB128
	.4byte	.LFB93
	.4byte	.LFE93-.LFB93
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.4byte	.LFB79
	.4byte	.LFE79-.LFB79
	.4byte	.LFB81
	.4byte	.LFE81-.LFB81
	.4byte	.LFB126
	.4byte	.LFE126-.LFB126
	.4byte	.LFB130
	.4byte	.LFE130-.LFB130
	.4byte	.LFB147
	.4byte	.LFE147-.LFB147
	.4byte	.LFB96
	.4byte	.LFE96-.LFB96
	.4byte	.LFB97
	.4byte	.LFE97-.LFB97
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.4byte	.LFB78
	.4byte	.LFE78-.LFB78
	.4byte	.LFB104
	.4byte	.LFE104-.LFB104
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.4byte	.LFB82
	.4byte	.LFE82-.LFB82
	.4byte	.LFB83
	.4byte	.LFE83-.LFB83
	.4byte	.LFB84
	.4byte	.LFE84-.LFB84
	.4byte	.LFB85
	.4byte	.LFE85-.LFB85
	.4byte	.LFB86
	.4byte	.LFE86-.LFB86
	.4byte	.LFB88
	.4byte	.LFE88-.LFB88
	.4byte	.LFB89
	.4byte	.LFE89-.LFB89
	.4byte	.LFB90
	.4byte	.LFE90-.LFB90
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.4byte	.LFB92
	.4byte	.LFE92-.LFB92
	.4byte	.LFB94
	.4byte	.LFE94-.LFB94
	.4byte	.LFB98
	.4byte	.LFE98-.LFB98
	.4byte	.LFB99
	.4byte	.LFE99-.LFB99
	.4byte	.LFB100
	.4byte	.LFE100-.LFB100
	.4byte	.LFB101
	.4byte	.LFE101-.LFB101
	.4byte	.LFB102
	.4byte	.LFE102-.LFB102
	.4byte	.LFB103
	.4byte	.LFE103-.LFB103
	.4byte	.LFB105
	.4byte	.LFE105-.LFB105
	.4byte	.LFB106
	.4byte	.LFE106-.LFB106
	.4byte	.LFB107
	.4byte	.LFE107-.LFB107
	.4byte	.LFB108
	.4byte	.LFE108-.LFB108
	.4byte	.LFB109
	.4byte	.LFE109-.LFB109
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.4byte	.LFB115
	.4byte	.LFE115-.LFB115
	.4byte	.LFB116
	.4byte	.LFE116-.LFB116
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.4byte	.LFB117
	.4byte	.LFE117-.LFB117
	.4byte	.LFB120
	.4byte	.LFE120-.LFB120
	.4byte	.LFB119
	.4byte	.LFE119-.LFB119
	.4byte	.LFB121
	.4byte	.LFE121-.LFB121
	.4byte	.LFB122
	.4byte	.LFE122-.LFB122
	.4byte	.LFB123
	.4byte	.LFE123-.LFB123
	.4byte	.LFB124
	.4byte	.LFE124-.LFB124
	.4byte	.LFB125
	.4byte	.LFE125-.LFB125
	.4byte	.LFB132
	.4byte	.LFE132-.LFB132
	.4byte	.LFB131
	.4byte	.LFE131-.LFB131
	.4byte	.LFB133
	.4byte	.LFE133-.LFB133
	.4byte	.LFB134
	.4byte	.LFE134-.LFB134
	.4byte	.LFB135
	.4byte	.LFE135-.LFB135
	.4byte	.LFB136
	.4byte	.LFE136-.LFB136
	.4byte	.LFB137
	.4byte	.LFE137-.LFB137
	.4byte	.LFB138
	.4byte	.LFE138-.LFB138
	.4byte	.LFB139
	.4byte	.LFE139-.LFB139
	.4byte	.LFB140
	.4byte	.LFE140-.LFB140
	.4byte	.LFB141
	.4byte	.LFE141-.LFB141
	.4byte	.LFB142
	.4byte	.LFE142-.LFB142
	.4byte	.LFB143
	.4byte	.LFE143-.LFB143
	.4byte	.LFB144
	.4byte	.LFE144-.LFB144
	.4byte	.LFB145
	.4byte	.LFE145-.LFB145
	.4byte	.LFB148
	.4byte	.LFE148-.LFB148
	.4byte	.LFB149
	.4byte	.LFE149-.LFB149
	.4byte	.LFB150
	.4byte	.LFE150-.LFB150
	.4byte	.LFB151
	.4byte	.LFE151-.LFB151
	.4byte	.LFB152
	.4byte	.LFE152-.LFB152
	.4byte	.LFB153
	.4byte	.LFE153-.LFB153
	.4byte	.LFB154
	.4byte	.LFE154-.LFB154
	.4byte	.LFB155
	.4byte	.LFE155-.LFB155
	.4byte	.LFB156
	.4byte	.LFE156-.LFB156
	.4byte	.LFB157
	.4byte	.LFE157-.LFB157
	.4byte	.LFB159
	.4byte	.LFE159-.LFB159
	.4byte	.LFB160
	.4byte	.LFE160-.LFB160
	.4byte	.LFB161
	.4byte	.LFE161-.LFB161
	.4byte	.LFB163
	.4byte	.LFE163-.LFB163
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LFB73
	.4byte	.LFE73
	.4byte	.LFB75
	.4byte	.LFE75
	.4byte	.LFB76
	.4byte	.LFE76
	.4byte	.LFB77
	.4byte	.LFE77
	.4byte	.LFB80
	.4byte	.LFE80
	.4byte	.LFB146
	.4byte	.LFE146
	.4byte	.LFB74
	.4byte	.LFE74
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB165
	.4byte	.LFE165
	.4byte	.LFB167
	.4byte	.LFE167
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LFB170
	.4byte	.LFE170
	.4byte	.LFB129
	.4byte	.LFE129
	.4byte	.LFB172
	.4byte	.LFE172
	.4byte	.LFB186
	.4byte	.LFE186
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB112
	.4byte	.LFE112
	.4byte	.LFB118
	.4byte	.LFE118
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LFB71
	.4byte	.LFE71
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	.LFB128
	.4byte	.LFE128
	.4byte	.LFB93
	.4byte	.LFE93
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LFB79
	.4byte	.LFE79
	.4byte	.LFB81
	.4byte	.LFE81
	.4byte	.LFB126
	.4byte	.LFE126
	.4byte	.LFB130
	.4byte	.LFE130
	.4byte	.LFB147
	.4byte	.LFE147
	.4byte	.LFB96
	.4byte	.LFE96
	.4byte	.LFB97
	.4byte	.LFE97
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LFB78
	.4byte	.LFE78
	.4byte	.LFB104
	.4byte	.LFE104
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LFB82
	.4byte	.LFE82
	.4byte	.LFB83
	.4byte	.LFE83
	.4byte	.LFB84
	.4byte	.LFE84
	.4byte	.LFB85
	.4byte	.LFE85
	.4byte	.LFB86
	.4byte	.LFE86
	.4byte	.LFB88
	.4byte	.LFE88
	.4byte	.LFB89
	.4byte	.LFE89
	.4byte	.LFB90
	.4byte	.LFE90
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LFB92
	.4byte	.LFE92
	.4byte	.LFB94
	.4byte	.LFE94
	.4byte	.LFB98
	.4byte	.LFE98
	.4byte	.LFB99
	.4byte	.LFE99
	.4byte	.LFB100
	.4byte	.LFE100
	.4byte	.LFB101
	.4byte	.LFE101
	.4byte	.LFB102
	.4byte	.LFE102
	.4byte	.LFB103
	.4byte	.LFE103
	.4byte	.LFB105
	.4byte	.LFE105
	.4byte	.LFB106
	.4byte	.LFE106
	.4byte	.LFB107
	.4byte	.LFE107
	.4byte	.LFB108
	.4byte	.LFE108
	.4byte	.LFB109
	.4byte	.LFE109
	.4byte	.LFB110
	.4byte	.LFE110
	.4byte	.LFB111
	.4byte	.LFE111
	.4byte	.LFB115
	.4byte	.LFE115
	.4byte	.LFB116
	.4byte	.LFE116
	.4byte	.LFB114
	.4byte	.LFE114
	.4byte	.LFB113
	.4byte	.LFE113
	.4byte	.LFB117
	.4byte	.LFE117
	.4byte	.LFB120
	.4byte	.LFE120
	.4byte	.LFB119
	.4byte	.LFE119
	.4byte	.LFB121
	.4byte	.LFE121
	.4byte	.LFB122
	.4byte	.LFE122
	.4byte	.LFB123
	.4byte	.LFE123
	.4byte	.LFB124
	.4byte	.LFE124
	.4byte	.LFB125
	.4byte	.LFE125
	.4byte	.LFB132
	.4byte	.LFE132
	.4byte	.LFB131
	.4byte	.LFE131
	.4byte	.LFB133
	.4byte	.LFE133
	.4byte	.LFB134
	.4byte	.LFE134
	.4byte	.LFB135
	.4byte	.LFE135
	.4byte	.LFB136
	.4byte	.LFE136
	.4byte	.LFB137
	.4byte	.LFE137
	.4byte	.LFB138
	.4byte	.LFE138
	.4byte	.LFB139
	.4byte	.LFE139
	.4byte	.LFB140
	.4byte	.LFE140
	.4byte	.LFB141
	.4byte	.LFE141
	.4byte	.LFB142
	.4byte	.LFE142
	.4byte	.LFB143
	.4byte	.LFE143
	.4byte	.LFB144
	.4byte	.LFE144
	.4byte	.LFB145
	.4byte	.LFE145
	.4byte	.LFB148
	.4byte	.LFE148
	.4byte	.LFB149
	.4byte	.LFE149
	.4byte	.LFB150
	.4byte	.LFE150
	.4byte	.LFB151
	.4byte	.LFE151
	.4byte	.LFB152
	.4byte	.LFE152
	.4byte	.LFB153
	.4byte	.LFE153
	.4byte	.LFB154
	.4byte	.LFE154
	.4byte	.LFB155
	.4byte	.LFE155
	.4byte	.LFB156
	.4byte	.LFE156
	.4byte	.LFB157
	.4byte	.LFE157
	.4byte	.LFB159
	.4byte	.LFE159
	.4byte	.LFB160
	.4byte	.LFE160
	.4byte	.LFB161
	.4byte	.LFE161
	.4byte	.LFB163
	.4byte	.LFE163
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF83:
	.ascii	"GuiLib_HLine\000"
.LASF164:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF136:
	.ascii	"GuiLib_ScrollBox_Init\000"
.LASF134:
	.ascii	"GuiLib_DrawVar\000"
.LASF22:
	.ascii	"ResetDrawLimits\000"
.LASF14:
	.ascii	"GuiLib_BrightenRgbColor\000"
.LASF45:
	.ascii	"GetItemByte\000"
.LASF144:
	.ascii	"GuiLib_ScrollBox_SetLineMarker\000"
.LASF165:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/"
	.ascii	"library_src/GuiLib.c\000"
.LASF161:
	.ascii	"GuiLib_TextBox_Scroll_Get_Pos_Pixel\000"
.LASF111:
	.ascii	"GuiLib_ScrollLineOffsetY\000"
.LASF155:
	.ascii	"GuiLib_TextBox_Scroll_Up_Pixel\000"
.LASF131:
	.ascii	"GuiLib_Refresh\000"
.LASF9:
	.ascii	"GuiLib_DegToRad\000"
.LASF15:
	.ascii	"GuiLib_DarkenRgbColor\000"
.LASF61:
	.ascii	"GuiLib_SetRedRgbColor\000"
.LASF64:
	.ascii	"GuiLib_RgbColorToGrayScale\000"
.LASF7:
	.ascii	"ScrollBox_SetTopLine\000"
.LASF10:
	.ascii	"GuiLib_Sqrt\000"
.LASF0:
	.ascii	"SwapCoord\000"
.LASF141:
	.ascii	"GuiLib_ScrollBox_Home\000"
.LASF24:
	.ascii	"ScrollBox_ShowLineBlock\000"
.LASF71:
	.ascii	"GuiLib_DarkenPixelColor\000"
.LASF146:
	.ascii	"GuiLib_ScrollBox_GetActiveLineCount\000"
.LASF44:
	.ascii	"ReadVar\000"
.LASF130:
	.ascii	"GuiLib_InvertBoxStart\000"
.LASF86:
	.ascii	"GuiLib_Circle\000"
.LASF76:
	.ascii	"GuiLib_ResetDisplayRepaint\000"
.LASF108:
	.ascii	"GuiLib_SetLanguage\000"
.LASF41:
	.ascii	"PrepareText\000"
.LASF27:
	.ascii	"DrawScrollLine\000"
.LASF80:
	.ascii	"GuiLib_GetDot\000"
.LASF66:
	.ascii	"GuiLib_GrayScaleToRgbColor\000"
.LASF133:
	.ascii	"GuiLib_DrawStr\000"
.LASF157:
	.ascii	"GuiLib_TextBox_Scroll_Home_Pixel\000"
.LASF114:
	.ascii	"GuiLib_Scroll_Up\000"
.LASF138:
	.ascii	"GuiLib_ScrollBox_Close\000"
.LASF58:
	.ascii	"GuiLib_SinDeg\000"
.LASF120:
	.ascii	"GuiLib_Cursor_Down\000"
.LASF29:
	.ascii	"DrawCursorItem\000"
.LASF38:
	.ascii	"VertLine\000"
.LASF159:
	.ascii	"GuiLib_TextBox_Scroll_To_PixelLine\000"
.LASF77:
	.ascii	"GuiLib_MarkDisplayBoxRepaint\000"
.LASF65:
	.ascii	"GuiLib_RgbToPixelColor\000"
.LASF129:
	.ascii	"GuiLib_InvertBoxStop\000"
.LASF145:
	.ascii	"GuiLib_ScrollBox_GetActiveLine\000"
.LASF149:
	.ascii	"GuiLib_ScrollBox_GetTopLine\000"
.LASF19:
	.ascii	"DrawBackBox\000"
.LASF33:
	.ascii	"ScrollBox_SetTopLine_Custom\000"
.LASF150:
	.ascii	"GuiLib_TextBox_Scroll_Up\000"
.LASF128:
	.ascii	"GuiLib_BlinkBoxMarkedItemUpdate\000"
.LASF12:
	.ascii	"GuiLib_GetGreenRgbColor\000"
.LASF60:
	.ascii	"GuiLib_CosDeg\000"
.LASF147:
	.ascii	"GuiLib_ScrollBox_SetIndicator\000"
.LASF94:
	.ascii	"DrawBorderBox\000"
.LASF55:
	.ascii	"GuiLib_InvertBox\000"
.LASF118:
	.ascii	"GuiLib_Cursor_Hide\000"
.LASF102:
	.ascii	"GuiLib_ShowScreen\000"
.LASF63:
	.ascii	"GuiLib_SetBlueRgbColor\000"
.LASF54:
	.ascii	"InvertBox\000"
.LASF75:
	.ascii	"GuiLib_SetClipping\000"
.LASF104:
	.ascii	"GuiLib_GetTextPtr\000"
.LASF69:
	.ascii	"GuiLib_GrayScaleToPixelColor\000"
.LASF70:
	.ascii	"GuiLib_BrightenPixelColor\000"
.LASF72:
	.ascii	"GuiLib_AccentuateRgbColor\000"
.LASF162:
	.ascii	"GuiLib_TextBox_Scroll_FitsInside\000"
.LASF62:
	.ascii	"GuiLib_SetGreenRgbColor\000"
.LASF109:
	.ascii	"GuiLib_Init\000"
.LASF116:
	.ascii	"GuiLib_Scroll_End\000"
.LASF26:
	.ascii	"TextBox_Scroll_To\000"
.LASF119:
	.ascii	"GuiLib_IsCursorFieldInUse\000"
.LASF151:
	.ascii	"GuiLib_TextBox_Scroll_Down\000"
.LASF160:
	.ascii	"GuiLib_TextBox_Scroll_Get_Pos\000"
.LASF89:
	.ascii	"DrawText\000"
.LASF48:
	.ascii	"ReadItem\000"
.LASF37:
	.ascii	"HorzLine\000"
.LASF110:
	.ascii	"GuiLib_SetScrollPars\000"
.LASF42:
	.ascii	"UpdateDrawLimits\000"
.LASF97:
	.ascii	"GuiLib_ShowBitmapAt\000"
.LASF112:
	.ascii	"GuiLib_RedrawScrollList\000"
.LASF127:
	.ascii	"GuiLib_BlinkBoxStart\000"
.LASF113:
	.ascii	"GuiLib_Scroll_Down\000"
.LASF82:
	.ascii	"GuiLib_LinePattern\000"
.LASF87:
	.ascii	"GuiLib_Box\000"
.LASF143:
	.ascii	"GuiLib_ScrollBox_To_Line\000"
.LASF99:
	.ascii	"UpdateBackgroundBitmap\000"
.LASF163:
	.ascii	"GuiLib_ScrollBox_Init_Custom_SetTopLine\000"
.LASF57:
	.ascii	"GuiLib_SinRad\000"
.LASF8:
	.ascii	"TextBox_Scroll_Get_Pos\000"
.LASF49:
	.ascii	"TextBox_Scroll_CalcEndPos\000"
.LASF107:
	.ascii	"GuiLib_GetBlinkingCharCode\000"
.LASF154:
	.ascii	"GuiLib_TextBox_Scroll_To_Line\000"
.LASF28:
	.ascii	"DrawScrollList\000"
.LASF52:
	.ascii	"CharDist\000"
.LASF152:
	.ascii	"GuiLib_TextBox_Scroll_Home\000"
.LASF101:
	.ascii	"GuiLib_Clear\000"
.LASF96:
	.ascii	"DrawStructure\000"
.LASF78:
	.ascii	"GuiLib_ClearDisplay\000"
.LASF81:
	.ascii	"GuiLib_Line\000"
.LASF5:
	.ascii	"SetBackColorBox\000"
.LASF43:
	.ascii	"SetBackBox\000"
.LASF126:
	.ascii	"GuiLib_BlinkBoxStop\000"
.LASF103:
	.ascii	"GuiLib_GetTextLanguagePtr\000"
.LASF68:
	.ascii	"GuiLib_PixelColorToGrayScale\000"
.LASF2:
	.ascii	"DrawChar\000"
.LASF23:
	.ascii	"ScrollBox_ShowLineStruct\000"
.LASF117:
	.ascii	"GuiLib_Scroll_To_Line\000"
.LASF98:
	.ascii	"GuiLib_ShowBitmapArea\000"
.LASF88:
	.ascii	"GuiLib_FillBox\000"
.LASF16:
	.ascii	"SetClipping\000"
.LASF34:
	.ascii	"ScrollLineAdjustY\000"
.LASF35:
	.ascii	"OrderCoord\000"
.LASF93:
	.ascii	"GuiLib_ShowBitmap\000"
.LASF91:
	.ascii	"ScrollBox_ShowBarBlock\000"
.LASF90:
	.ascii	"DrawTextBlock\000"
.LASF123:
	.ascii	"GuiLib_Cursor_End\000"
.LASF92:
	.ascii	"GuiLib_BorderBox\000"
.LASF142:
	.ascii	"GuiLib_ScrollBox_End\000"
.LASF47:
	.ascii	"SetCurFont\000"
.LASF73:
	.ascii	"GuiLib_AccentuatePixelColor\000"
.LASF21:
	.ascii	"SetLanguageCharSet\000"
.LASF46:
	.ascii	"GetItemWord\000"
.LASF67:
	.ascii	"GuiLib_PixelToRgbColor\000"
.LASF79:
	.ascii	"GuiLib_Dot\000"
.LASF39:
	.ascii	"ConvertIntToStr\000"
.LASF158:
	.ascii	"GuiLib_TextBox_Scroll_End_Pixel\000"
.LASF18:
	.ascii	"ReadDot\000"
.LASF17:
	.ascii	"ClearDisplay\000"
.LASF50:
	.ascii	"DataNumStr\000"
.LASF137:
	.ascii	"GuiLib_ScrollBox_RedrawLine\000"
.LASF13:
	.ascii	"GuiLib_GetBlueRgbColor\000"
.LASF20:
	.ascii	"DrawScrollIndicator\000"
.LASF115:
	.ascii	"GuiLib_Scroll_Home\000"
.LASF100:
	.ascii	"GuiLib_ShowBitmapAreaAt\000"
.LASF11:
	.ascii	"GuiLib_GetRedRgbColor\000"
.LASF30:
	.ascii	"CheckLanguageIndex\000"
.LASF51:
	.ascii	"MakeDot\000"
.LASF36:
	.ascii	"MarkDisplayBoxRepaint\000"
.LASF25:
	.ascii	"ScrollBox_DrawScrollLine\000"
.LASF95:
	.ascii	"DrawItem\000"
.LASF1:
	.ascii	"CheckRect\000"
.LASF148:
	.ascii	"GuiLib_ScrollBox_SetTopLine\000"
.LASF74:
	.ascii	"GuiLib_ResetClipping\000"
.LASF122:
	.ascii	"GuiLib_Cursor_Home\000"
.LASF4:
	.ascii	"TextPixelLength\000"
.LASF140:
	.ascii	"GuiLib_ScrollBox_Up\000"
.LASF121:
	.ascii	"GuiLib_Cursor_Up\000"
.LASF6:
	.ascii	"GetCharCode\000"
.LASF124:
	.ascii	"GuiLib_BlinkBoxMarkedItem\000"
.LASF31:
	.ascii	"GuiLib_Cursor_Select\000"
.LASF84:
	.ascii	"GuiLib_VLine\000"
.LASF40:
	.ascii	"CalcCharsWidth\000"
.LASF105:
	.ascii	"GuiLib_GetTextWidth\000"
.LASF132:
	.ascii	"GuiLib_DrawChar\000"
.LASF59:
	.ascii	"GuiLib_CosRad\000"
.LASF56:
	.ascii	"GuiLib_RadToDeg\000"
.LASF156:
	.ascii	"GuiLib_TextBox_Scroll_Down_Pixel\000"
.LASF125:
	.ascii	"GuiLib_BlinkBoxMarkedItemStop\000"
.LASF32:
	.ascii	"GuiLib_ScrollBox_Redraw\000"
.LASF135:
	.ascii	"GuiLib_TestPattern\000"
.LASF3:
	.ascii	"ShowBitmapArea\000"
.LASF153:
	.ascii	"GuiLib_TextBox_Scroll_End\000"
.LASF85:
	.ascii	"GuiLib_Ellipse\000"
.LASF106:
	.ascii	"GuiLib_GetCharCode\000"
.LASF139:
	.ascii	"GuiLib_ScrollBox_Down\000"
.LASF53:
	.ascii	"BlinkBox\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
