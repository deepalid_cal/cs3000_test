	.file	"r_flow_table.c"
	.text
.Ltext0:
	.section	.text.FDTO_FLOW_TABLE_redraw_scrollbox,"ax",%progbits
	.align	2
	.global	FDTO_FLOW_TABLE_redraw_scrollbox
	.type	FDTO_FLOW_TABLE_redraw_scrollbox, %function
FDTO_FLOW_TABLE_redraw_scrollbox:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldr	r5, .L2
	ldr	r4, .L2+4
	ldr	r0, [r5, #0]
	bl	FDTO_SYSTEM_load_system_name_into_guivar
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L2+8
	mov	r3, #145
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [r5, #0]
	ldr	r1, .L2+12
	ldr	r3, .L2+16
	mov	r0, #0
	mla	r2, r1, r2, r3
	ldr	r3, .L2+20
	ldr	r1, [r2, r3]
	mov	r2, r0
	bl	FDTO_SCROLL_BOX_redraw_retaining_topline
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	14224
	.word	system_preserves
	.word	14068
.LFE2:
	.size	FDTO_FLOW_TABLE_redraw_scrollbox, .-FDTO_FLOW_TABLE_redraw_scrollbox
	.section	.text.nm_FLOW_TABLE_get_cell_value,"ax",%progbits
	.align	2
	.type	nm_FLOW_TABLE_get_cell_value, %function
nm_FLOW_TABLE_get_cell_value:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L7+8
	ldr	r3, [r0, r3]
	sub	r3, r3, #1
	mla	r3, r1, r3, r2
	ldr	r2, .L7+12
	cmp	r3, r2
.LBB4:
	movls	r3, r3, asl #1
	addls	r3, r3, #532
	ldrlssh	r3, [r0, r3]
.LBE4:
	fldshi	s15, .L7
.LBB5:
	fmsrls	s15, r3	@ int
	fsitosls	s14, s15
	fldsls	s15, .L7+4
	fdivsls	s15, s14, s15
.LBE5:
	fmrs	r0, s15
	bx	lr
.L8:
	.align	2
.L7:
	.word	0
	.word	1092616192
	.word	14048
	.word	4499
.LFE0:
	.size	nm_FLOW_TABLE_get_cell_value, .-nm_FLOW_TABLE_get_cell_value
	.section	.text.nm_FLOW_TABLE_load_guivars_for_scroll_line,"ax",%progbits
	.align	2
	.type	nm_FLOW_TABLE_load_guivars_for_scroll_line, %function
nm_FLOW_TABLE_load_guivars_for_scroll_line:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L10
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r2, [r3, #0]
	ldr	r5, .L10+4
	ldr	r3, .L10+8
	mov	r0, r0, asl #16
	mla	r5, r2, r5, r3
	ldr	r3, .L10+12
	mov	r4, r0, asr #16
	ldr	r2, [r5, r3]
	ldr	r3, .L10+16
	mul	r2, r4, r2
	mov	r1, r4
	str	r2, [r3, #0]
	mov	r0, r5
	mov	r2, #0
	bl	nm_FLOW_TABLE_get_cell_value
	ldr	r3, .L10+20
	mov	r1, r4
	mov	r2, #1
	str	r0, [r3, #0]	@ float
	mov	r0, r5
	bl	nm_FLOW_TABLE_get_cell_value
	ldr	r3, .L10+24
	mov	r1, r4
	mov	r2, #2
	str	r0, [r3, #0]	@ float
	mov	r0, r5
	bl	nm_FLOW_TABLE_get_cell_value
	ldr	r3, .L10+28
	mov	r1, r4
	mov	r2, #3
	str	r0, [r3, #0]	@ float
	mov	r0, r5
	bl	nm_FLOW_TABLE_get_cell_value
	ldr	r3, .L10+32
	mov	r1, r4
	mov	r2, #4
	str	r0, [r3, #0]	@ float
	mov	r0, r5
	bl	nm_FLOW_TABLE_get_cell_value
	ldr	r3, .L10+36
	mov	r1, r4
	mov	r2, #5
	str	r0, [r3, #0]	@ float
	mov	r0, r5
	bl	nm_FLOW_TABLE_get_cell_value
	ldr	r3, .L10+40
	mov	r1, r4
	mov	r2, #6
	str	r0, [r3, #0]	@ float
	mov	r0, r5
	bl	nm_FLOW_TABLE_get_cell_value
	ldr	r3, .L10+44
	mov	r1, r4
	mov	r2, #7
	str	r0, [r3, #0]	@ float
	mov	r0, r5
	bl	nm_FLOW_TABLE_get_cell_value
	ldr	r3, .L10+48
	mov	r1, r4
	mov	r2, #8
	str	r0, [r3, #0]	@ float
	mov	r0, r5
	bl	nm_FLOW_TABLE_get_cell_value
	ldr	r3, .L10+52
	str	r0, [r3, #0]	@ float
	ldmfd	sp!, {r4, r5, pc}
.L11:
	.align	2
.L10:
	.word	.LANCHOR0
	.word	14224
	.word	system_preserves+16
	.word	14044
	.word	GuiVar_FlowTableGPM
	.word	GuiVar_FlowTable2Sta
	.word	GuiVar_FlowTable3Sta
	.word	GuiVar_FlowTable4Sta
	.word	GuiVar_FlowTable5Sta
	.word	GuiVar_FlowTable6Sta
	.word	GuiVar_FlowTable7Sta
	.word	GuiVar_FlowTable8Sta
	.word	GuiVar_FlowTable9Sta
	.word	GuiVar_FlowTable10Sta
.LFE1:
	.size	nm_FLOW_TABLE_load_guivars_for_scroll_line, .-nm_FLOW_TABLE_load_guivars_for_scroll_line
	.section	.text.FDTO_FLOW_TABLE_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_FLOW_TABLE_draw_report
	.type	FDTO_FLOW_TABLE_draw_report, %function
FDTO_FLOW_TABLE_draw_report:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	mvn	r1, #0
	mov	r5, r0
	mov	r2, #1
	mov	r0, #83
	bl	GuiLib_ShowScreen
	cmp	r5, #1
	bne	.L13
	ldr	r3, .L14
	mov	r0, #0
	str	r0, [r3, #0]
	bl	FDTO_SYSTEM_load_system_name_into_guivar
.L13:
	ldr	r4, .L14+4
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L14+8
	mov	r3, #177
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L14
	ldr	r1, .L14+12
	ldr	r2, [r3, #0]
	ldr	r3, .L14+16
	mov	r0, r5
	mla	r2, r1, r2, r3
	ldr	r3, .L14+20
	ldr	r1, [r2, r3]
	ldr	r2, .L14+24
	bl	FDTO_REPORTS_draw_report
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L15:
	.align	2
.L14:
	.word	.LANCHOR0
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	14224
	.word	system_preserves
	.word	14068
	.word	nm_FLOW_TABLE_load_guivars_for_scroll_line
.LFE3:
	.size	FDTO_FLOW_TABLE_draw_report, .-FDTO_FLOW_TABLE_draw_report
	.section	.text.FLOW_TABLE_process_report,"ax",%progbits
	.align	2
	.global	FLOW_TABLE_process_report
	.type	FLOW_TABLE_process_report, %function
FLOW_TABLE_process_report:
.LFB4:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI3:
	sub	sp, sp, #44
.LCFI4:
	beq	.L18
	cmp	r0, #20
	bne	.L23
	b	.L24
.L18:
	mov	r6, #80
	b	.L19
.L24:
	mov	r6, #84
.L19:
	ldr	r5, .L25
	mov	r1, #400
	ldr	r2, .L25+4
	mov	r3, #217
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	mov	r4, #1
	ldr	r1, .L25+8
	mov	r2, #0
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	sub	r3, r0, #1
	mov	r0, r6
	bl	process_uns32
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L25+12
	add	r0, sp, #8
	str	r4, [sp, #8]
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L16
.L23:
	mov	r2, #0
	mov	r3, r2
	bl	REPORTS_process_report
.L16:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, pc}
.L26:
	.align	2
.L25:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
	.word	FDTO_FLOW_TABLE_redraw_scrollbox
.LFE4:
	.size	FLOW_TABLE_process_report, .-FLOW_TABLE_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_flow_table.c\000"
	.section	.bss.g_FLOW_TABLE_index_of_system_to_show,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_FLOW_TABLE_index_of_system_to_show, %object
	.size	g_FLOW_TABLE_index_of_system_to_show, 4
g_FLOW_TABLE_index_of_system_to_show:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_flow_table.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x85
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x39
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x8d
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x1
	.byte	0x69
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xa6
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xc5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"FLOW_TABLE_process_report\000"
.LASF1:
	.ascii	"FDTO_FLOW_TABLE_draw_report\000"
.LASF6:
	.ascii	"nm_FLOW_TABLE_load_guivars_for_scroll_line\000"
.LASF5:
	.ascii	"nm_FLOW_TABLE_get_cell_value\000"
.LASF0:
	.ascii	"FDTO_FLOW_TABLE_redraw_scrollbox\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_flow_table.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
