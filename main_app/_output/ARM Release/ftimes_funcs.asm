	.file	"ftimes_funcs.c"
	.text
.Ltext0:
	.section	.text.find_a_duplicate_ahead_of_this_station,"ax",%progbits
	.align	2
	.type	find_a_duplicate_ahead_of_this_station, %function
find_a_duplicate_ahead_of_this_station:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L6
	mov	r1, #112
	mla	r3, r1, r0, ip
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI0:
	ldr	r6, .L6+4
	mov	r5, r1
	mov	r2, r0
	mov	r1, r3
	b	.L2
.L5:
	add	r2, r2, #1
	mul	r4, r5, r2
	add	r0, ip, r4
	ldrb	r4, [ip, r4]	@ zero_extendqisi2
	tst	r4, #2
	beq	.L3
	ldr	r7, [r3, #160]
	ldr	r4, [r1, #48]
	cmp	r7, r4
	bne	.L3
	ldr	r7, [r3, #156]
	ldr	r4, [r1, #44]
	cmp	r7, r4
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
.L3:
	add	r3, r3, #112
.L2:
	cmp	r2, r6
	bls	.L5
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L7:
	.align	2
.L6:
	.word	ft_stations
	.word	766
.LFE1:
	.size	find_a_duplicate_ahead_of_this_station, .-find_a_duplicate_ahead_of_this_station
	.global	__umodsi3
	.section	.text.start_time_check_does_it_irrigate_on_this_date,"ax",%progbits
	.align	2
	.type	start_time_check_does_it_irrigate_on_this_date, %function
start_time_check_does_it_irrigate_on_this_date:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r1, [r0, #88]
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	mov	r4, r0
	cmp	r1, #18
	ldrls	pc, [pc, r1, asl #2]
	b	.L9
.L15:
	.word	.L21
	.word	.L11
	.word	.L12
	.word	.L13
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
	.word	.L14
.L11:
	ldr	r3, .L28
	ldrh	r3, [r3, #14]
	tst	r3, #1
	movne	r0, #0
	moveq	r0, #1
	ldmfd	sp!, {r4, r5, pc}
.L12:
	ldr	r2, .L28
	ldrh	r3, [r2, #14]
	ands	r0, r3, #1
	ldmeqfd	sp!, {r4, r5, pc}
	cmp	r3, #1
	bne	.L16
	ldrh	r3, [r2, #16]
	sub	r2, r3, #1
	mov	r2, r2, asl #16
	cmp	r3, #4
	cmpne	r2, #65536
	bls	.L27
	cmp	r3, #6
	beq	.L27
	cmp	r3, #8
	beq	.L27
	cmp	r3, #9
	beq	.L27
	cmp	r3, #11
	beq	.L27
.L16:
	ldr	r5, .L28
	ldrh	r0, [r5, #18]
	bl	IsLeapYear
	cmp	r0, #1
	bne	.L21
	ldrh	r3, [r5, #14]
	cmp	r3, #1
	ldmnefd	sp!, {r4, r5, pc}
	ldrh	r2, [r5, #16]
	cmp	r2, #3
	bne	.L23
.L27:
	ldr	r0, [r4, #132]
	b	.L26
.L13:
	ldr	r3, .L28
	ldrb	r3, [r3, #26]	@ zero_extendqisi2
	add	r3, r3, #26
	ldr	r0, [r0, r3, asl #2]
.L26:
	adds	r0, r0, #0
	movne	r0, #1
	ldmfd	sp!, {r4, r5, pc}
.L14:
	cmp	r1, #17
	moveq	r1, #21
	beq	.L18
	cmp	r1, #18
	subne	r1, r1, #2
	moveq	r1, #28
.L18:
	ldr	r3, .L28
	ldrh	r0, [r3, #12]
	ldr	r3, [r4, #100]
	rsb	r0, r3, r0
	bl	__umodsi3
	rsbs	r0, r0, #1
	movcc	r0, #0
	ldmfd	sp!, {r4, r5, pc}
.L9:
	ldr	r0, .L28+4
	bl	Alert_Message
	mov	r0, #0
	ldmfd	sp!, {r4, r5, pc}
.L21:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, pc}
.L23:
	mov	r0, r3
	ldmfd	sp!, {r4, r5, pc}
.L29:
	.align	2
.L28:
	.word	ftcs
	.word	.LC0
.LFE11:
	.size	start_time_check_does_it_irrigate_on_this_date, .-start_time_check_does_it_irrigate_on_this_date
	.section	.text.add_duplicate_ft_station,"ax",%progbits
	.align	2
	.type	add_duplicate_ft_station, %function
add_duplicate_ft_station:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	ldr	sl, .L35
	mov	r3, r0
	mov	r8, r1
	mov	ip, #112
	mov	r1, r0
	ldr	r0, .L35+4
	b	.L31
.L33:
	add	r1, r1, #1
	mul	r6, ip, r1
	ldr	r7, .L35+4
	ldrb	r5, [r0, r6]	@ zero_extendqisi2
	mov	r2, #112
	ands	r5, r5, #1
	add	r4, r0, r6
	bne	.L31
	mla	r1, r2, r3, r7
	mov	r0, r4
	bl	memcpy
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	add	r0, r6, #4
	orr	r3, r3, #2
	strb	r3, [r4, #0]
	mov	r1, r5
	mov	r2, #12
	add	r0, r7, r0
	bl	memset
	add	r0, r6, #16
	mov	r2, #12
	add	r0, r7, r0
	mov	r1, r5
	bl	memset
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	ldrb	r2, [r4, #106]	@ zero_extendqisi2
	bic	r3, r3, #4
	strb	r3, [r4, #0]
	ldrh	r3, [r4, #104]
	and	r8, r8, #15
	bic	r3, r3, #960
	bic	r2, r2, #64
	orr	r8, r3, r8, asl #6
	str	r5, [r4, #56]
	str	r5, [r4, #84]
	strb	r2, [r4, #106]
	strh	r8, [r4, #104]	@ movhi
	b	.L32
.L31:
	cmp	r1, sl
	bls	.L33
	mov	r4, #0
.L32:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L36:
	.align	2
.L35:
	.word	766
	.word	ft_stations
.LFE0:
	.size	add_duplicate_ft_station, .-add_duplicate_ft_station
	.section	.text.FTIMES_FUNCS_load_run_time_calculation_support,"ax",%progbits
	.align	2
	.global	FTIMES_FUNCS_load_run_time_calculation_support
	.type	FTIMES_FUNCS_load_run_time_calculation_support, %function
FTIMES_FUNCS_load_run_time_calculation_support:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	subs	r4, r0, #0
	mov	r5, r1
	beq	.L39
	ldr	r3, [r4, #84]
	add	r0, r1, #8
	str	r3, [r1, #0]
	ldr	r3, [r4, #88]
	mov	r2, #28
	str	r3, [r1, #4]
	add	r1, r4, #104
	bl	memcpy
	ldr	r3, [r4, #132]
	mov	r0, #1
	str	r3, [r5, #36]
	ldr	r3, [r4, #144]
	str	r3, [r5, #40]
	ldr	r3, [r4, #92]
	str	r3, [r5, #44]
	ldmfd	sp!, {r4, r5, pc}
.L39:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.LFE2:
	.size	FTIMES_FUNCS_load_run_time_calculation_support, .-FTIMES_FUNCS_load_run_time_calculation_support
	.section	.text.FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID,"ax",%progbits
	.align	2
	.global	FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID
	.type	FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID, %function
FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI4:
	mov	r4, r0
	ldr	r0, .L46
	bl	nm_ListGetFirst
	b	.L45
.L43:
	ldr	r3, [r1, #12]
	cmp	r3, r4
	beq	.L42
	ldr	r0, .L46
	bl	nm_ListGetNext
.L45:
	cmp	r0, #0
	mov	r1, r0
	bne	.L43
.L42:
	mov	r0, r1
	ldmfd	sp!, {r4, pc}
.L47:
	.align	2
.L46:
	.word	ft_station_groups_list_hdr
.LFE3:
	.size	FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID, .-FTIMES_FUNCS_find_station_group_in_ftimes_list_with_this_GID
	.section	.text.FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID,"ax",%progbits
	.align	2
	.global	FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID
	.type	FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID, %function
FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI5:
	mov	r4, r0
	ldr	r0, .L54
	bl	nm_ListGetFirst
	b	.L53
.L51:
	ldr	r3, [r1, #12]
	cmp	r3, r4
	beq	.L50
	ldr	r0, .L54
	bl	nm_ListGetNext
.L53:
	cmp	r0, #0
	mov	r1, r0
	bne	.L51
.L50:
	mov	r0, r1
	ldmfd	sp!, {r4, pc}
.L55:
	.align	2
.L54:
	.word	ft_system_groups_list_hdr
.LFE4:
	.size	FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID, .-FTIMES_FUNCS_find_system_group_in_ftimes_list_with_this_GID
	.section	.text.ftimes_if_station_is_ON_turn_it_OFF,"ax",%progbits
	.align	2
	.global	ftimes_if_station_is_ON_turn_it_OFF
	.type	ftimes_if_station_is_ON_turn_it_OFF, %function
ftimes_if_station_is_ON_turn_it_OFF:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI6:
	mov	r6, r0
	ldr	r0, .L79
	mov	r4, r1
	bl	nm_OnList
	subs	r5, r0, #0
	bne	.L57
	ldr	r0, .L79+4
	bl	RemovePathFromFileName
	mov	r2, #236
	mov	r1, r0
	ldr	r0, .L79+8
	bl	Alert_Message_va
	b	.L58
.L57:
	cmp	r6, #0
	moveq	r5, r6
	beq	.L59
	mov	r0, r6
	mov	r1, r4
	bl	nm_ListGetNext
	mov	r5, r0
.L59:
	ldrb	r3, [r4, #106]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L60
	ldr	r2, [r4, #48]
	ldr	r3, .L79+12
	ldr	r1, [r3, r2, asl #2]
	cmp	r1, #0
	subne	r1, r1, #1
	strne	r1, [r3, r2, asl #2]
	bne	.L62
	ldr	r0, .L79+4
	bl	RemovePathFromFileName
	ldr	r2, .L79+16
	mov	r1, r0
	ldr	r0, .L79+20
	bl	Alert_Message_va
.L62:
	ldr	r0, .L79+24
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	beq	.L63
	mov	r1, r4
	ldr	r2, .L79+4
	ldr	r3, .L79+28
	ldr	r0, .L79+24
	bl	nm_ListRemove_debug
	b	.L64
.L63:
	ldr	r0, .L79+4
	bl	RemovePathFromFileName
	ldr	r2, .L79+32
	mov	r1, r0
	ldr	r0, .L79+36
	bl	Alert_Message_va
.L64:
	ldr	r3, [r4, #28]
	ldr	r2, [r3, #36]
	cmp	r2, #0
	subne	r2, r2, #1
	strne	r2, [r3, #36]
	bne	.L66
	ldr	r0, .L79+40
	bl	Alert_Message
.L66:
	ldr	r3, [r4, #28]
	ldrh	r2, [r4, #102]
	ldr	r1, [r3, #44]
	cmp	r1, r2
	rsbcs	r2, r2, r1
	bcs	.L77
	ldr	r0, .L79+44
	bl	Alert_Message
	ldr	r3, [r4, #28]
	mov	r2, #0
.L77:
	str	r2, [r3, #44]
	ldrb	r2, [r4, #106]	@ zero_extendqisi2
	ldr	r3, [r4, #28]
	mov	r2, r2, lsr #2
	and	r2, r2, #3
	add	r2, r2, #29
	ldr	r1, [r3, r2, asl #2]
	cmp	r1, #0
	subne	r1, r1, #1
	strne	r1, [r3, r2, asl #2]
	bne	.L70
	ldr	r0, .L79+48
	bl	Alert_Message
.L70:
	ldrb	r3, [r4, #105]	@ zero_extendqisi2
	tst	r3, #8
	beq	.L71
	ldr	r3, [r4, #28]
	ldr	r2, [r3, #36]
	cmp	r2, #0
	beq	.L78
.L71:
	ldr	r3, [r4, #28]
	ldr	r2, [r4, #32]
	ldr	r1, [r3, #132]
	ldr	r2, [r2, #168]
	cmp	r1, r2
	bcs	.L73
.L78:
	str	r2, [r3, #132]
.L73:
	ldrb	r3, [r4, #106]	@ zero_extendqisi2
	mov	r0, #1
	bic	r3, r3, #64
	strb	r3, [r4, #106]
	mov	r3, #0
	str	r3, [r4, #64]
	ldr	r3, [r4, #60]
	mov	r1, r4
	cmp	r3, #0
	ldrne	r3, [r4, #80]
	str	r3, [r4, #84]
	bl	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
	b	.L58
.L60:
	ldr	r0, .L79+24
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	beq	.L58
	ldr	r0, .L79+4
	bl	RemovePathFromFileName
	ldr	r2, .L79+52
	mov	r1, r0
	ldr	r0, .L79+56
	bl	Alert_Message_va
	ldr	r0, .L79+24
	mov	r1, r4
	ldr	r2, .L79+4
	ldr	r3, .L79+60
	bl	nm_ListRemove_debug
.L58:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L80:
	.align	2
.L79:
	.word	ft_irrigating_stations_list_hdr
	.word	.LC1
	.word	.LC2
	.word	ft_stations_ON_by_controller
	.word	265
	.word	.LC3
	.word	ft_stations_ON_list_hdr
	.word	279
	.word	283
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	427
	.word	.LC8
	.word	429
.LFE5:
	.size	ftimes_if_station_is_ON_turn_it_OFF, .-ftimes_if_station_is_ON_turn_it_OFF
	.section	.text.ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists,"ax",%progbits
	.align	2
	.global	ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists
	.type	ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists, %function
ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	mov	r4, r1
	bl	ftimes_if_station_is_ON_turn_it_OFF
	ldr	r3, .L83
	mov	r1, r4
	ldr	r2, .L83+4
	mov	r5, r0
	ldr	r0, .L83+8
	bl	nm_ListRemove_debug
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L82
.LBB19:
	mov	r0, r4
	mov	r1, #0
	mov	r2, #112
	bl	memset
.L82:
.LBE19:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L84:
	.align	2
.L83:
	.word	466
	.word	.LC1
	.word	ft_irrigating_stations_list_hdr
.LFE6:
	.size	ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists, .-ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists
	.section	.text.ftimes_complete_the_turn_ON,"ax",%progbits
	.align	2
	.global	ftimes_complete_the_turn_ON
	.type	ftimes_complete_the_turn_ON, %function
ftimes_complete_the_turn_ON:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI8:
	ldrb	r3, [r0, #106]	@ zero_extendqisi2
	ldr	r2, [r0, #48]
	orr	r3, r3, #64
	strb	r3, [r0, #106]
	ldr	r3, .L87
	mov	r4, r0
	ldr	r1, [r3, r2, asl #2]
	ldr	r0, .L87+4
	add	r1, r1, #1
	str	r1, [r3, r2, asl #2]
	mov	r1, r4
	bl	nm_ListInsertTail
	ldr	r3, [r4, #28]
	ldrb	r2, [r3, #137]	@ zero_extendqisi2
	ldrb	r1, [r3, #136]	@ zero_extendqisi2
	and	r2, r2, #128
	bic	r1, r1, #128
	strb	r1, [r3, #136]
	strb	r2, [r3, #137]
	ldrh	r3, [r4, #104]
	and	r3, r3, #960
	cmp	r3, #64
	ldreqb	r3, [r4, #0]	@ zero_extendqisi2
	biceq	r3, r3, #4
	streqb	r3, [r4, #0]
	ldmfd	sp!, {r4, pc}
.L88:
	.align	2
.L87:
	.word	ft_stations_ON_by_controller
	.word	ft_stations_ON_list_hdr
.LFE7:
	.size	ftimes_complete_the_turn_ON, .-ftimes_complete_the_turn_ON
	.section	.text.ftimes_get_percent_adjust_100u,"ax",%progbits
	.align	2
	.global	ftimes_get_percent_adjust_100u
	.type	ftimes_get_percent_adjust_100u, %function
ftimes_get_percent_adjust_100u:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	beq	.L93
	ldr	r2, [r0, #76]
	cmp	r1, r2
	bcc	.L93
	ldr	r3, [r0, #80]
	cmp	r1, r3
	bhi	.L93
	cmp	r2, r3
	ldrne	r0, [r0, #72]
	bxne	lr
	b	.L95
.L93:
	mov	r0, #100
	bx	lr
.L95:
	mov	r0, #100
	bx	lr
.LFE8:
	.size	ftimes_get_percent_adjust_100u, .-ftimes_get_percent_adjust_100u
	.section	.text.ftimes_number_of_starts_for_this_manual_program,"ax",%progbits
	.align	2
	.global	ftimes_number_of_starts_for_this_manual_program
	.type	ftimes_number_of_starts_for_this_manual_program, %function
ftimes_number_of_starts_for_this_manual_program:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI9:
	subs	r5, r0, #0
	moveq	r4, r5
	beq	.L97
	ldr	r6, .L105
	ldr	r2, [r5, #68]
	ldrh	r3, [r6, #12]
	cmp	r3, r2
	bcc	.L103
	ldr	r2, [r5, #72]
	cmp	r3, r2
	bhi	.L103
	mov	r7, r5
	add	sl, r5, #24
	mov	r4, #0
.LBB22:
	mov	r8, #1
.L100:
	ldr	r1, [r7, #16]
	ldr	r3, [r6, #8]
	cmp	r1, r3
	bne	.L98
	ldr	r3, [r6, #28]
	cmp	r3, #0
	bne	.L99
	ldrh	r0, [r6, #12]
	ldr	r2, .L105+4
	bl	DateAndTimeToDTCS
	str	r8, [r6, #28]
.L99:
	ldrb	r3, [r6, #26]	@ zero_extendqisi2
	add	r3, r3, #10
	ldr	r3, [r5, r3, asl #2]
	cmp	r3, #0
	addne	r4, r4, #1
.L98:
	add	r7, r7, #4
	cmp	r7, sl
	bne	.L100
	b	.L97
.L103:
.LBE22:
	mov	r4, #0
.L97:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L106:
	.align	2
.L105:
	.word	ftcs
	.word	ftcs+8
.LFE9:
	.size	ftimes_number_of_starts_for_this_manual_program, .-ftimes_number_of_starts_for_this_manual_program
	.section	.text.ftimes_add_to_the_irrigation_list,"ax",%progbits
	.align	2
	.global	ftimes_add_to_the_irrigation_list
	.type	ftimes_add_to_the_irrigation_list, %function
ftimes_add_to_the_irrigation_list:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI10:
	ldrb	r3, [r0, #106]	@ zero_extendqisi2
	mov	r4, r0
	bic	r3, r3, #64
	strb	r3, [r0, #106]
	ldrh	r3, [r0, #104]
	and	r3, r3, #960
	cmp	r3, #64
	ldreq	r2, [r0, #76]
	mov	r3, #0
	strne	r3, [r0, #80]
	streq	r2, [r0, #80]
	str	r3, [r0, #84]
	str	r3, [r0, #64]
	ldrh	r0, [r0, #104]
	mov	r0, r0, lsr #6
	and	r0, r0, #15
	bl	FOAL_IRRI_we_test_flow_when_ON_for_this_reason
	ldrb	r3, [r4, #107]	@ zero_extendqisi2
	mov	r1, r4
	bic	r3, r3, #1
	and	r0, r0, #1
	orr	r3, r0, r3
	strb	r3, [r4, #107]
	ldr	r0, .L110
	bl	nm_ListInsertTail
	mov	r0, #1
	mov	r1, r4
	ldmfd	sp!, {r4, lr}
	b	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
.L111:
	.align	2
.L110:
	.word	ft_irrigating_stations_list_hdr
.LFE10:
	.size	ftimes_add_to_the_irrigation_list, .-ftimes_add_to_the_irrigation_list
	.section	.text.FTIMES_FUNCS_check_for_a_start,"ax",%progbits
	.align	2
	.global	FTIMES_FUNCS_check_for_a_start
	.type	FTIMES_FUNCS_check_for_a_start, %function
FTIMES_FUNCS_check_for_a_start:
.LFB14:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI11:
	fstmfdd	sp!, {d8}
.LCFI12:
	ldr	r3, .L155
	sub	sp, sp, #12
.LCFI13:
.LBB23:
	ldr	r0, .L155+4
.LBE23:
	str	r3, [sp, #8]	@ float
.LBB24:
	bl	nm_ListGetFirst
	ldr	r5, .L155+8
	mov	r6, #1
	mov	r4, r0
	b	.L113
.L117:
	ldr	r3, [r4, #84]
	cmp	r3, #0
	beq	.L114
	ldr	r1, [r4, #92]
	ldr	r3, [r5, #8]
	cmp	r1, r3
	bne	.L114
	ldr	r3, [r5, #28]
	cmp	r3, #0
	bne	.L115
	ldrh	r0, [r5, #12]
	ldr	r2, .L155+12
	bl	DateAndTimeToDTCS
	str	r6, [r5, #28]
.L115:
	mov	r0, r4
	bl	start_time_check_does_it_irrigate_on_this_date
	cmp	r0, #0
	bne	.L146
.L114:
	mov	r1, r4
	ldr	r0, .L155+4
	bl	nm_ListGetNext
	mov	r4, r0
.L113:
	cmp	r4, #0
	bne	.L117
	b	.L116
.L146:
	mov	r4, #1
.L116:
.LBE24:
.LBB25:
	ldr	r0, .L155+16
	bl	nm_ListGetFirst
	b	.L151
.L120:
	mov	r0, r5
	bl	ftimes_number_of_starts_for_this_manual_program
	cmp	r0, #0
	bne	.L119
	ldr	r0, .L155+16
	mov	r1, r5
	bl	nm_ListGetNext
.L151:
	cmp	r0, #0
	mov	r5, r0
	bne	.L120
.LBE25:
	cmp	r4, #0
	beq	.L112
.L119:
	ldr	r5, .L155+20
.LBB26:
	mov	r6, #0
.LBE26:
	mov	r8, #1
.L145:
	mov	r7, #112
	mul	r7, r6, r7
	ldrb	r3, [r7, r5]	@ zero_extendqisi2
	add	r4, r7, r5
	tst	r3, #1
	beq	.L122
	tst	r3, #2
	beq	.L123
	ldrh	r3, [r4, #104]
	and	r3, r3, #960
	cmp	r3, #128
	beq	.L124
.L123:
	ldr	r3, [r4, #32]
	cmp	r3, #0
	beq	.L124
	ldr	r2, [r3, #84]
	cmp	r2, #0
	beq	.L124
	ldr	sl, .L155+8
	ldr	r1, [r3, #92]
	ldr	r2, [sl, #8]
	cmp	r1, r2
	bne	.L124
	ldr	r3, [r3, #96]
	cmp	r3, r1
	beq	.L124
	ldr	r3, [sl, #28]
	cmp	r3, #0
	bne	.L125
	ldrh	r0, [sl, #12]
	add	r2, sl, #8
	bl	DateAndTimeToDTCS
	str	r8, [sl, #28]
.L125:
	ldr	r0, [r4, #32]
	bl	start_time_check_does_it_irrigate_on_this_date
	cmp	r0, #0
	beq	.L124
	mov	r0, #1
	mov	r1, #0
	mov	r2, r4
	ldr	r3, .L155+12
	bl	STATION_GROUPS_skip_irrigation_due_to_a_mow_day
	subs	sl, r0, #0
	bne	.L124
	ldr	r0, .L155+24
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	beq	.L126
	ldrh	r3, [r4, #104]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L127
	mov	r0, sl
	mov	r1, r4
	bl	ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists
	b	.L126
.L127:
	cmp	r3, #128
	bne	.L128
	ldrb	r3, [r7, r5]	@ zero_extendqisi2
	tst	r3, #2
	bne	.L124
	mov	r0, r6
	bl	find_a_duplicate_ahead_of_this_station
	subs	sl, r0, #0
	beq	.L130
	ldr	r0, .L155+24
	mov	r1, sl
	bl	nm_OnList
	cmp	r0, #0
	beq	.L131
	ldrh	r3, [sl, #104]
	and	r3, r3, #960
	cmp	r3, #64
	beq	.L124
.L131:
	ldr	r0, .L155+28
	bl	RemovePathFromFileName
	ldr	r2, .L155+32
	mov	r1, r0
	ldr	r0, .L155+36
	b	.L154
.L130:
	mov	r0, r6
	mov	r1, #1
	bl	add_duplicate_ft_station
	cmp	r0, #0
	bne	.L124
	ldr	r0, .L155+28
	bl	RemovePathFromFileName
	ldr	r2, .L155+40
	mov	r1, r0
	ldr	r0, .L155+44
	bl	Alert_Message_va
	b	.L124
.L128:
	ldr	r0, .L155+28
	bl	RemovePathFromFileName
	ldr	r2, .L155+48
	mov	r1, r0
	ldr	r0, .L155+52
	b	.L154
.L126:
	ldr	r3, .L155+12
	flds	s16, [sp, #8]
	mov	r1, #0
	str	r3, [sp, #0]
	flds	s14, [r4, #92]	@ int
	mov	r2, r1
	mov	r3, r4
	mov	r0, #1
	fuitos	s15, s14
	fsts	s15, [sp, #4]
	bl	nm_WATERSENSE_calculate_run_minutes_for_this_station
	ldr	r2, [r4, #32]
	ldr	r2, [r2, #140]
	cmp	r2, #0
	fmsr	s15, r0
	fmuls	s16, s16, s15
	ftouizs	s16, s16
	fmrs	r3, s16	@ int
	fsts	s16, [r4, #60]	@ int
	beq	.L133
	ldr	r2, [r4, #56]
	add	r2, r3, r2
	mov	r3, #0
	cmp	r2, r3
	str	r2, [r4, #60]
	str	r3, [r4, #56]
	beq	.L133
	mov	r0, #1
	add	r1, r4, #60
	ldr	r2, [r4, #68]
	bl	WATERSENSE_make_min_cycle_adjustment
	str	r0, [r4, #56]
.L133:
	flds	s14, [r4, #60]	@ int
	flds	s17, [sp, #8]
	ldr	r3, .L155+8
	flds	s15, [sp, #8]
	fuitos	s16, s14
	ldrh	r1, [r3, #12]
	ldr	r0, [r4, #32]
	fdivs	s16, s16, s15
	bl	ftimes_get_percent_adjust_100u
	ldr	r3, [r4, #88]
	mov	r2, r4
	str	r3, [sp, #0]
	mov	r1, #0
	fmrs	r3, s16
	str	r0, [sp, #4]
	mov	r0, #1
	bl	nm_WATERSENSE_calculate_adjusted_run_time
	ldr	r2, [r4, #32]
	ldr	r2, [r2, #140]
	cmp	r2, #0
	fmsr	s15, r0
	fmuls	s17, s17, s15
	ftouizs	s17, s17
	fmrs	r3, s17	@ int
	fsts	s17, [r4, #60]	@ int
	beq	.L134
	cmp	r3, #0
	beq	.L134
	mov	r0, #1
	add	r1, r4, #60
	ldr	r2, [r4, #68]
	mov	r3, #0
	bl	WATERSENSE_make_min_cycle_adjustment
.L134:
	ldr	r3, [r4, #32]
	ldr	sl, .L155+12
	ldr	r1, [r3, #96]
	mov	r0, sl
	str	r1, [r4, #96]
	bl	FOAL_IRRI_return_stop_date_according_to_stop_time
	ldr	r3, [r4, #60]
	cmp	r3, #0
	strh	r0, [r4, #100]	@ movhi
	beq	.L124
	ldrh	r3, [r4, #104]
	mov	r0, r4
	bic	r3, r3, #896
	orr	r3, r3, #64
	strh	r3, [r4, #104]	@ movhi
	mov	r2, r3, lsr #8
	ldrb	r3, [r7, r5]	@ zero_extendqisi2
	and	r2, r2, #232
	mov	r3, r3, asl #2
	and	r3, r3, #16
	orr	r3, r3, r2
	strb	r3, [r4, #105]
	ldr	r3, [r4, #68]
	str	r3, [r4, #72]
	bl	ftimes_add_to_the_irrigation_list
	ldrb	r3, [r7, r5]	@ zero_extendqisi2
	mov	r2, #0
	orr	r3, r3, #4
	strb	r3, [r7, r5]
	ldr	r3, [r4, #32]
	ldmia	sl, {r0, r1}
	str	r2, [r3, #216]
	str	r0, [r3, #192]
	strh	r1, [r3, #196]	@ movhi
	str	r2, [r3, #200]
	str	r2, [r3, #204]
	str	r2, [r3, #212]
.L124:
	ldrb	r3, [r7, r5]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L135
	ldrh	r3, [r4, #104]
	and	r3, r3, #960
	cmp	r3, #64
	beq	.L122
.L135:
	ldr	r0, [r4, #36]
	bl	ftimes_number_of_starts_for_this_manual_program
	mov	sl, r0
	ldr	r0, [r4, #40]
	bl	ftimes_number_of_starts_for_this_manual_program
	add	r0, r0, sl
	cmp	r0, #0
	beq	.L122
	ldr	r0, .L155+24
	mov	r1, r4
	bl	nm_OnList
	subs	sl, r0, #0
	beq	.L136
	ldrh	r3, [r4, #104]
	and	r3, r3, #960
	cmp	r3, #128
	beq	.L148
	cmp	r3, #64
	bne	.L137
	ldrb	r3, [r7, r5]	@ zero_extendqisi2
	tst	r3, #2
	bne	.L122
	mov	r0, r6
	bl	find_a_duplicate_ahead_of_this_station
	subs	r4, r0, #0
	beq	.L139
	ldr	r0, .L155+24
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	beq	.L140
	ldrh	r3, [r4, #104]
	and	r3, r3, #960
	cmp	r3, #128
	beq	.L122
.L140:
	ldr	r0, .L155+28
	bl	RemovePathFromFileName
	mov	r2, #1296
	mov	r1, r0
	ldr	r0, .L155+36
	b	.L154
.L139:
	mov	r0, r6
	mov	r1, #2
	bl	add_duplicate_ft_station
	cmp	r0, #0
	bne	.L122
	ldr	r0, .L155+28
	bl	RemovePathFromFileName
	ldr	r2, .L155+56
	mov	r1, r0
	ldr	r0, .L155+44
	bl	Alert_Message_va
	b	.L122
.L137:
	ldr	r0, .L155+28
	bl	RemovePathFromFileName
	ldr	r2, .L155+60
	mov	r1, r0
	ldr	r0, .L155+52
.L154:
	bl	Alert_Message_va
	add	sp, sp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	FTIMES_TASK_restart_calculation
.L148:
	mov	sl, #1
.L136:
	mov	r7, r4
	add	r9, r4, #8
.L144:
	ldr	r0, [r7, #36]
	bl	ftimes_number_of_starts_for_this_manual_program
	cmp	r0, #0
	beq	.L142
	cmp	sl, #0
	ldr	r3, [r7, #36]
	ldrne	r2, [r3, #76]
	ldrne	r3, [r4, #60]
	mlane	r0, r2, r0, r3
	strne	r0, [r4, #60]
	bne	.L152
.L143:
	ldr	r3, [r3, #76]
	mul	r0, r3, r0
	str	r3, [r4, #72]
	ldrh	r3, [r4, #104]
	str	r0, [r4, #60]
	bic	r3, r3, #832
	orr	r3, r3, #128
	strh	r3, [r4, #104]	@ movhi
	mov	r0, r4
	bl	ftimes_add_to_the_irrigation_list
.L152:
	mov	sl, #1
.L142:
	add	r7, r7, #4
	cmp	r7, r9
	bne	.L144
.L122:
	add	r6, r6, #1
	cmp	r6, #768
	bne	.L145
.L112:
	add	sp, sp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L156:
	.align	2
.L155:
	.word	1114636288
	.word	ft_station_groups_list_hdr
	.word	ftcs
	.word	ftcs+8
	.word	ft_manual_programs_list_hdr
	.word	ft_stations
	.word	ft_irrigating_stations_list_hdr
	.word	.LC1
	.word	1041
	.word	.LC9
	.word	1055
	.word	.LC10
	.word	1066
	.word	.LC11
	.word	1310
	.word	1325
.LFE14:
	.size	FTIMES_FUNCS_check_for_a_start, .-FTIMES_FUNCS_check_for_a_start
	.section	.text.ftimes_will_exceed_system_capacity,"ax",%progbits
	.align	2
	.global	ftimes_will_exceed_system_capacity
	.type	ftimes_will_exceed_system_capacity, %function
ftimes_will_exceed_system_capacity:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #28]
	ldr	r2, [r3, #16]
	cmp	r2, #0
	beq	.L163
	ldr	r2, [r3, #96]
	cmp	r2, #1
	beq	.L159
	ldrb	r2, [r0, #105]	@ zero_extendqisi2
	tst	r2, #8
	ldreq	r2, [r3, #24]
	beq	.L161
.L159:
	ldr	r2, [r3, #20]
.L161:
	ldr	r1, [r3, #36]
	cmp	r1, #0
	ldrh	r1, [r0, #102]
	bne	.L162
	cmp	r2, r1
	movcc	r2, r1
.L162:
	ldr	r0, [r3, #44]
	add	r0, r1, r0
	cmp	r0, r2
	movls	r0, #0
	movhi	r0, #1
	bx	lr
.L163:
	mov	r0, r2
	bx	lr
.LFE15:
	.size	ftimes_will_exceed_system_capacity, .-ftimes_will_exceed_system_capacity
	.section	.text.ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON,"ax",%progbits
	.align	2
	.global	ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON
	.type	ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON, %function
ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, [r0, #28]
	ldr	r3, [r2, #36]
	ldr	r2, [r2, #32]
	cmp	r3, r2
	bcs	.L166
	ldr	r2, [r0, #32]
	ldr	r0, [r2, #152]
	cmp	r0, r3
	movls	r0, #0
	movhi	r0, #1
	bx	lr
.L166:
	mov	r0, #0
	bx	lr
.LFE16:
	.size	ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON, .-ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON
	.section	.text.ftimes_can_another_valve_come_ON_within_this_station_group,"ax",%progbits
	.align	2
	.global	ftimes_can_another_valve_come_ON_within_this_station_group
	.type	ftimes_can_another_valve_come_ON_within_this_station_group, %function
ftimes_can_another_valve_come_ON_within_this_station_group:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #32]
	ldr	r0, [r3, #156]
	ldr	r3, [r3, #148]
	cmp	r0, r3
	movcs	r0, #0
	movcc	r0, #1
	bx	lr
.LFE17:
	.size	ftimes_can_another_valve_come_ON_within_this_station_group, .-ftimes_can_another_valve_come_ON_within_this_station_group
	.section	.text.ftimes_merge_new_mainline_allowed_ON_into_the_limit,"ax",%progbits
	.align	2
	.global	ftimes_merge_new_mainline_allowed_ON_into_the_limit
	.type	ftimes_merge_new_mainline_allowed_ON_into_the_limit, %function
ftimes_merge_new_mainline_allowed_ON_into_the_limit:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #32]
	ldr	r2, [r3, #152]
	ldr	r3, [r0, #28]
	ldr	r1, [r3, #32]
	cmp	r2, r1
	strcc	r2, [r3, #32]
	ldr	r2, [r3, #36]
	ldr	r3, [r3, #32]
	cmp	r2, r3
	bxls	lr
	ldr	r0, .L171
	b	Alert_Message
.L172:
	.align	2
.L171:
	.word	.LC12
.LFE18:
	.size	ftimes_merge_new_mainline_allowed_ON_into_the_limit, .-ftimes_merge_new_mainline_allowed_ON_into_the_limit
	.section	.text.ftimes_bump_this_station_groups_on_at_a_time_ON_count,"ax",%progbits
	.align	2
	.global	ftimes_bump_this_station_groups_on_at_a_time_ON_count
	.type	ftimes_bump_this_station_groups_on_at_a_time_ON_count, %function
ftimes_bump_this_station_groups_on_at_a_time_ON_count:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #32]
	ldr	r2, [r3, #156]
	add	r2, r2, #1
	str	r2, [r3, #156]
	ldr	r3, [r3, #148]
	cmp	r2, r3
	bxls	lr
	ldr	r0, .L175
	b	Alert_Message
.L176:
	.align	2
.L175:
	.word	.LC13
.LFE19:
	.size	ftimes_bump_this_station_groups_on_at_a_time_ON_count, .-ftimes_bump_this_station_groups_on_at_a_time_ON_count
	.section	.text.ftimes_set_remaining_seconds_on,"ax",%progbits
	.align	2
	.global	ftimes_set_remaining_seconds_on
	.type	ftimes_set_remaining_seconds_on, %function
ftimes_set_remaining_seconds_on:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrh	r3, [r0, #104]
	mov	r1, #0
	and	r3, r3, #960
	cmp	r3, #128
	cmpne	r3, #64
	str	r1, [r0, #64]
	bxne	lr
	ldr	r3, [r0, #60]
	ldr	r2, [r0, #72]
	cmp	r3, r2
	rsbhi	r3, r2, r3
	strhi	r2, [r0, #64]
	strhi	r3, [r0, #60]
	strls	r3, [r0, #64]
	strls	r1, [r0, #60]
	bx	lr
.LFE20:
	.size	ftimes_set_remaining_seconds_on, .-ftimes_set_remaining_seconds_on
	.section	.text.ftimes_init_ufim_variables_for_all_systems,"ax",%progbits
	.align	2
	.global	ftimes_init_ufim_variables_for_all_systems
	.type	ftimes_init_ufim_variables_for_all_systems, %function
ftimes_init_ufim_variables_for_all_systems:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI14:
	ldr	r0, .L183
	bl	nm_ListGetFirst
	mov	r4, #0
	mvn	r6, #0
	mov	r5, #1
	mov	r1, r0
	b	.L181
.L182:
	str	r4, [r1, #28]
	str	r6, [r1, #32]
	str	r4, [r1, #48]
	str	r5, [r1, #52]
	str	r5, [r1, #56]
	str	r4, [r1, #60]
	str	r4, [r1, #64]
	str	r4, [r1, #68]
	str	r4, [r1, #72]
	str	r4, [r1, #76]
	str	r4, [r1, #80]
	str	r4, [r1, #84]
	str	r4, [r1, #88]
	str	r4, [r1, #96]
	str	r4, [r1, #100]
	str	r4, [r1, #104]
	str	r4, [r1, #108]
	str	r4, [r1, #112]
	ldr	r0, .L183
	bl	nm_ListGetNext
	mov	r1, r0
.L181:
	cmp	r1, #0
	bne	.L182
	ldr	r3, .L183+4
	str	r1, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, pc}
.L184:
	.align	2
.L183:
	.word	ft_system_groups_list_hdr
	.word	ufim_one_or_more_in_the_list_for_manual_program
.LFE21:
	.size	ftimes_init_ufim_variables_for_all_systems, .-ftimes_init_ufim_variables_for_all_systems
	.section	.text.ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups,"ax",%progbits
	.align	2
	.global	ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups
	.type	ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups, %function
ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups:
.LFB22:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI15:
	ldr	r0, .L188
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r5, #1
	mov	r1, r0
	b	.L186
.L187:
	str	r4, [r1, #156]
	str	r4, [r1, #184]
	str	r4, [r1, #188]
	str	r5, [r1, #208]
	ldr	r0, .L188
	bl	nm_ListGetNext
	mov	r1, r0
.L186:
	cmp	r1, #0
	bne	.L187
	ldmfd	sp!, {r4, r5, pc}
.L189:
	.align	2
.L188:
	.word	ft_station_groups_list_hdr
.LFE22:
	.size	ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups, .-ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups
	.section	.text.ftimes_for_each_station_group_check_if_irrigation_just_completed,"ax",%progbits
	.align	2
	.global	ftimes_for_each_station_group_check_if_irrigation_just_completed
	.type	ftimes_for_each_station_group_check_if_irrigation_just_completed, %function
ftimes_for_each_station_group_check_if_irrigation_just_completed:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI16:
	ldr	r0, .L197
	bl	nm_ListGetFirst
	ldr	r5, .L197+4
	mov	r7, r5
	add	r6, r5, #8
	mov	r4, r0
	b	.L191
.L196:
	ldr	r3, [r4, #184]
	cmp	r3, #0
	beq	.L192
	ldr	r3, [r4, #188]
	cmp	r3, #0
	beq	.L193
	ldrh	r2, [r5, #12]
	ldrh	r3, [r4, #196]
	cmp	r2, r3
	bne	.L194
	ldr	r2, [r5, #8]
	ldr	r3, [r4, #192]
	cmp	r2, r3
	beq	.L195
.L194:
	ldr	r2, [r4, #216]
	ldr	r3, [r7, #64]
	add	r3, r2, r3
	str	r3, [r4, #216]
.L195:
	ldmia	r6, {r0, r1}
	str	r0, [r4, #236]
	strh	r1, [r4, #240]	@ movhi
	b	.L192
.L193:
	ldr	r3, [r4, #216]
	ldr	r2, [r4, #220]
	cmp	r3, r2
	bls	.L192
	str	r3, [r4, #220]
	add	r1, r4, #236
	mov	r2, #6
	add	r0, r4, #230
	bl	memcpy
	ldrh	r3, [r4, #230]
	ldrh	r8, [r4, #232]
	mov	r1, #60
	orr	r8, r3, r8, asl #16
	add	r8, r8, #30
	mov	r0, r8
	bl	__umodsi3
	add	r3, r4, #192
	rsb	r8, r0, r8
	strh	r8, [r4, #230]	@ movhi
	mov	r8, r8, lsr #16
	strh	r8, [r4, #232]	@ movhi
	ldmia	r3, {r0, r1}
	ldr	r3, [r4, #200]
	str	r0, [r4, #224]
	str	r3, [r4, #244]
	ldr	r3, [r4, #204]
	strh	r1, [r4, #228]	@ movhi
	str	r3, [r4, #248]
	ldr	r3, [r4, #212]
	str	r3, [r4, #252]
.L192:
	mov	r1, r4
	ldr	r0, .L197
	bl	nm_ListGetNext
	mov	r4, r0
.L191:
	cmp	r4, #0
	bne	.L196
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L198:
	.align	2
.L197:
	.word	ft_station_groups_list_hdr
	.word	ftcs
.LFE23:
	.size	ftimes_for_each_station_group_check_if_irrigation_just_completed, .-ftimes_for_each_station_group_check_if_irrigation_just_completed
	.section	.text.FTIMES_FUNCS_maintain_irrigation_list,"ax",%progbits
	.align	2
	.global	FTIMES_FUNCS_maintain_irrigation_list
	.type	FTIMES_FUNCS_maintain_irrigation_list, %function
FTIMES_FUNCS_maintain_irrigation_list:
.LFB30:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI17:
.LBB42:
	ldr	r0, .L288
	bl	nm_ListGetFirst
	b	.L278
.L202:
	ldr	r3, [r1, #132]
	ldr	r0, .L288
	cmp	r3, #0
	subne	r3, r3, #1
	strne	r3, [r1, #132]
	bl	nm_ListGetNext
.L278:
	cmp	r0, #0
	mov	r1, r0
	bne	.L202
.LBE42:
	bl	ftimes_init_ufim_variables_for_all_systems
	bl	ftimes_init_on_at_a_time_and_completion_variables_in_all_station_groups
.LBB43:
	ldr	r0, .L288+4
	bl	nm_ListGetFirst
	ldr	r6, .L288+8
	mov	r5, #1
	mov	r4, r0
	b	.L275
.L231:
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	ands	r7, r3, #8
	bne	.L223
.L204:
	orr	r3, r3, #8
	strb	r3, [r4, #0]
	ldrh	r3, [r4, #104]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L206
	ldrh	r3, [r4, #100]
	add	r1, sp, #8
	strh	r3, [sp, #4]	@ movhi
	ldr	r3, [r4, #96]
	ldr	r0, .L288+12
	str	r3, [r1, #-8]!
	mov	r1, sp
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #1
	bne	.L207
	ldr	r3, [r4, #32]
	mov	r7, #1
	ldr	r2, [r3, #200]
	str	r5, [r3, #200]
	cmp	r2, #0
	streq	r2, [r3, #208]
.L207:
	ldr	r3, [r4, #32]
	str	r5, [r3, #184]
.L206:
	ldr	r3, [r4, #28]
	ldr	r2, [r3, #28]
	add	r2, r2, #1
	str	r2, [r3, #28]
	ldrh	r2, [r4, #104]
	and	r2, r2, #960
	cmp	r2, #128
	ldreq	r2, .L288+16
	streq	r5, [r2, #0]
	ldr	r2, [r4, #84]
	cmp	r2, #0
	bne	.L210
	ldrh	r2, [r4, #104]
	ldr	r1, [r3, #104]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	cmp	r2, r1
	strhi	r2, [r3, #104]
	ldrb	r1, [r4, #105]	@ zero_extendqisi2
	tst	r1, #8
	beq	.L212
	ldr	r1, [r3, #108]
	cmp	r2, r1
	strhi	r2, [r3, #108]
	b	.L210
.L212:
	ldr	r3, [r4, #28]
	ldrh	r2, [r4, #104]
	ldr	r1, [r3, #112]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	cmp	r2, r1
	strhi	r2, [r3, #112]
.L210:
.LBB44:
	ldrb	r3, [r4, #106]	@ zero_extendqisi2
	ands	r2, r3, #64
	bne	.L214
	ldr	r3, [r4, #60]
	cmp	r3, #0
	beq	.L215
	b	.L214
.L269:
.LBE44:
	cmp	r7, #0
	beq	.L216
	ldr	r0, [r3, #204]
	ldr	r1, [r6, #64]
	add	r1, r0, r1
	str	r1, [r3, #204]
	mov	r1, #0
	str	r1, [r3, #208]
.L216:
	cmp	r2, #0
	beq	.L215
	ldr	r0, .L288+20
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	bne	.L217
	ldr	r0, .L288+24
	bl	Alert_Message
.L217:
	ldr	r2, [r4, #64]
	ldr	r3, [r6, #64]
	rsb	r3, r3, r2
	cmp	r3, #0
	str	r3, [r4, #64]
	bge	.L218
	ldr	r0, .L288+28
	bl	Alert_Message
.L218:
	ldr	r3, [r4, #64]
	cmp	r3, #0
	bgt	.L219
	mov	r1, r4
	ldr	r0, .L288+4
	bl	ftimes_if_station_is_ON_turn_it_OFF
	b	.L280
.L219:
	ldrb	r3, [r4, #107]	@ zero_extendqisi2
	mov	r0, r4
	tst	r3, #1
	ldrne	r3, [r4, #28]
	strne	r5, [r3, #88]
	bl	ftimes_merge_new_mainline_allowed_ON_into_the_limit
	mov	r0, r4
	bl	ftimes_bump_this_station_groups_on_at_a_time_ON_count
	ldrb	r3, [r4, #105]	@ zero_extendqisi2
	tst	r3, #8
	ldr	r3, [r4, #28]
	strne	r5, [r3, #96]
	streq	r5, [r3, #100]
	b	.L223
.L215:
	ldr	r0, .L288+20
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	beq	.L224
	ldr	r0, .L288+32
	bl	Alert_Message
.L224:
	ldr	r3, [r4, #84]
	cmp	r3, #0
	beq	.L225
	ldr	r2, [r6, #64]
	cmp	r3, r2
	rsbhi	r3, r2, r3
	movls	r3, #0
	str	r3, [r4, #84]
.L225:
.LBB45:
	ldrb	r3, [r4, #106]	@ zero_extendqisi2
	tst	r3, #64
	bne	.L227
	ldr	r3, [r4, #60]
	cmp	r3, #0
	bne	.L227
.LBE45:
	mov	r1, r4
	ldr	r0, .L288+4
	bl	ftimes_turn_OFF_this_station_if_ON_and_remove_from_all_irrigation_lists
	b	.L280
.L287:
	ldr	r3, [r4, #84]
	cmp	r3, #0
	bne	.L228
	ldr	r3, [r4, #60]
	cmp	r3, #0
	beq	.L228
	ldr	r3, [r4, #28]
	ldrb	r2, [r4, #104]	@ zero_extendqisi2
	ldr	r1, [r3, #52]
	mov	r2, r2, lsr #4
	and	r2, r2, #3
	cmp	r2, r1
	bls	.L229
	ldrb	r1, [r4, #105]	@ zero_extendqisi2
	tst	r1, #8
	strne	r2, [r3, #52]
.L229:
	ldr	r1, [r3, #56]
	cmp	r2, r1
	bls	.L228
	ldrb	r1, [r4, #105]	@ zero_extendqisi2
	tst	r1, #8
	streq	r2, [r3, #56]
.L228:
	ldr	r3, [r4, #84]
	cmp	r3, #0
	bne	.L223
	ldr	r3, [r4, #60]
	cmp	r3, #0
	beq	.L223
	ldrb	r3, [r4, #105]	@ zero_extendqisi2
	tst	r3, #8
	ldr	r3, [r4, #28]
	strne	r5, [r3, #64]
	streq	r5, [r3, #68]
	b	.L223
.L275:
	cmp	r4, #0
	bne	.L231
.LBE43:
.LBB46:
	ldr	r0, .L288
	bl	nm_ListGetFirst
	mov	r5, #1
	mov	r6, r0
	b	.L232
.L251:
	ldr	r7, [r6, #36]
	cmp	r7, #0
	bne	.L233
	ldr	r3, [r6, #44]
	cmp	r3, #0
	beq	.L233
	ldr	r0, .L288+36
	bl	Alert_Message
	str	r7, [r6, #44]
.L233:
	ldr	r3, [r6, #28]
	cmp	r3, #0
	beq	.L248
.L234:
	ldr	r3, [r6, #52]
	ldr	r2, [r6, #56]
	cmp	r3, r2
	bls	.L236
	ldr	r1, [r6, #104]
	cmp	r1, #1
	streq	r1, [r6, #40]
	beq	.L235
.L236:
	cmp	r2, r3
	bls	.L237
	ldr	r3, [r6, #104]
	cmp	r3, #1
	beq	.L277
.L237:
	ldr	r3, [r6, #96]
	cmp	r3, #1
	streq	r3, [r6, #40]
	beq	.L235
	ldr	r3, [r6, #100]
	cmp	r3, #1
	beq	.L277
.L239:
	ldr	r3, [r6, #104]
	cmp	r3, #2
	bls	.L240
	ldr	r2, [r6, #108]
	ldr	r3, [r6, #112]
	cmp	r2, r3
	bhi	.L248
.L241:
	bcs	.L235
	b	.L277
.L240:
	ldr	r3, [r6, #72]
	cmp	r3, #1
	ldr	r3, [r6, #40]
	bne	.L242
	cmp	r3, #1
	ldreq	r3, [r6, #76]
	beq	.L286
	cmp	r3, #0
	ldreq	r3, [r6, #80]
	beq	.L284
	b	.L248
.L242:
	cmp	r3, #1
	bne	.L246
	ldr	r3, [r6, #64]
.L286:
	cmp	r3, #1
	bne	.L277
	b	.L235
.L246:
	cmp	r3, #0
	bne	.L248
	ldr	r3, [r6, #68]
.L284:
	cmp	r3, #1
	bne	.L248
	b	.L235
.L277:
	str	r4, [r6, #40]
	b	.L235
.L248:
	str	r5, [r6, #40]
.L235:
	ldr	r3, [r6, #88]
	cmp	r3, #0
	beq	.L249
	ldr	r3, [r6, #92]
	cmp	r3, #1
	bhi	.L249
	add	r0, r6, #116
	bl	FOAL_IRRI_there_is_more_than_one_flow_group_ON
	cmp	r0, #0
	beq	.L249
	ldr	r0, .L288+40
	bl	Alert_Message
.L249:
	ldr	r3, [r6, #100]
	cmp	r3, #1
	movne	r3, #0
	bne	.L250
	ldr	r3, [r6, #96]
	sub	r2, r3, #1
	rsbs	r3, r2, #0
	adc	r3, r3, r2
.L250:
	str	r3, [r6, #84]
	mov	r1, r6
	ldr	r0, .L288
	bl	nm_ListGetNext
	mov	r6, r0
.L232:
	cmp	r6, #0
	bne	.L251
.LBE46:
.LBB47:
	ldr	r0, .L288+4
	bl	nm_ListGetFirst
	mov	r4, #1
	mov	r1, r0
	b	.L252
.L254:
	ldrh	r3, [r1, #104]
	ldr	r0, .L288+4
	and	r3, r3, #960
	cmp	r3, #64
	ldreq	r3, [r1, #32]
	streq	r4, [r3, #188]
	bl	nm_ListGetNext
	mov	r1, r0
.L252:
	cmp	r1, #0
	bne	.L254
	bl	ftimes_for_each_station_group_check_if_irrigation_just_completed
.LBE47:
.LBB48:
	ldr	r0, .L288+4
	bl	nm_ListGetFirst
.LBB49:
	ldr	r8, .L288+16
	ldr	r7, .L288+44
	ldr	r6, .L288+48
.LBE49:
	mov	r4, r0
	b	.L255
.L258:
.LBB51:
	mov	r1, r4
	ldr	r0, .L288+4
	bl	nm_ListGetNext
	mov	r4, r0
.L255:
	cmp	r4, #0
	beq	.L199
	ldrh	r3, [r4, #104]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L257
	ldr	r3, [r8, #0]
	cmp	r3, #0
	ldrne	r3, [r4, #32]
	movne	r2, #1
	strne	r2, [r3, #212]
.L257:
.LBB50:
	ldrb	r3, [r4, #106]	@ zero_extendqisi2
	tst	r3, #64
	bne	.L258
	ldr	r3, [r4, #60]
	cmp	r3, #0
	beq	.L258
.LBE50:
	ldr	r3, [r4, #84]
	cmp	r3, #0
	bne	.L258
	ldr	r1, [r4, #28]
	ldr	r3, [r1, #132]
	cmp	r3, #0
	bne	.L258
	ldrb	r3, [r4, #107]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L259
	ldrh	r3, [r4, #104]
	and	r3, r3, #960
	cmp	r3, #384
	beq	.L259
	add	r0, r4, #104
	add	r1, r1, #116
	bl	FOAL_IRRI_there_are_other_flow_check_groups_already_ON
	cmp	r0, #0
	bne	.L258
.L259:
	mov	r0, r4
	bl	ftimes_will_exceed_system_capacity
	cmp	r0, #0
	bne	.L258
	ldr	r3, [r4, #28]
	ldr	r2, [r3, #84]
	cmp	r2, #1
	beq	.L258
	ldrb	r2, [r4, #105]	@ zero_extendqisi2
	tst	r2, #8
	ldr	r2, [r3, #40]
	beq	.L260
	cmp	r2, #0
	beq	.L258
	ldr	r2, [r3, #36]
	cmp	r2, #0
	beq	.L261
	ldr	r3, [r3, #96]
	cmp	r3, #0
	b	.L281
.L260:
	cmp	r2, #1
	beq	.L258
	ldr	r3, [r3, #96]
	cmp	r3, #1
.L281:
	beq	.L258
.L261:
	ldr	r3, [r4, #48]
	ldr	r2, [r7, r3, asl #2]
	ldr	r3, [r6, r3, asl #2]
	cmp	r2, r3
	bcs	.L258
	mov	r0, r4
	bl	ftimes_the_on_at_a_time_rules_in_the_mainline_allow_this_valve_to_come_ON
	cmp	r0, #0
	beq	.L258
	mov	r0, r4
	bl	ftimes_can_another_valve_come_ON_within_this_station_group
	cmp	r0, #0
	beq	.L258
	ldr	r0, .L288+20
	bl	nm_ListGetFirst
	mov	r5, #0
	mov	r1, r0
	b	.L262
.L264:
	ldr	r2, [r1, #48]
	ldr	r3, [r4, #48]
	cmp	r2, r3
	bne	.L263
	ldr	r2, [r1, #44]
	ldr	r3, [r4, #44]
	cmp	r2, r3
	moveq	r5, #1
.L263:
	ldr	r0, .L288+20
	bl	nm_ListGetNext
	mov	r1, r0
.L262:
	cmp	r1, #0
	bne	.L264
	cmp	r5, #1
	beq	.L258
	mov	r0, r4
	bl	ftimes_set_remaining_seconds_on
	ldr	r3, [r4, #64]
	cmp	r3, #0
	ble	.L265
	ldr	r3, [r4, #28]
	ldr	r2, [r3, #36]
	cmp	r2, #0
	beq	.L266
	ldr	r2, [r3, #96]
	cmp	r2, #0
	ldrb	r2, [r4, #105]	@ zero_extendqisi2
	bne	.L267
	tst	r2, #8
	beq	.L266
	b	.L282
.L267:
	tst	r2, #8
	bne	.L266
.L282:
	mov	r2, #1
	str	r2, [r3, #84]
.L266:
	mov	r0, r4
	bl	ftimes_complete_the_turn_ON
	ldr	r3, [r4, #28]
	ldrh	r2, [r4, #102]
	ldr	r1, [r3, #44]
	mov	r0, r4
	add	r2, r1, r2
	str	r2, [r3, #44]
	ldrb	r2, [r4, #106]	@ zero_extendqisi2
	mov	r2, r2, lsr #2
	and	r2, r2, #3
	add	r2, r2, #29
	ldr	r1, [r3, r2, asl #2]
	add	r1, r1, #1
	str	r1, [r3, r2, asl #2]
	ldr	r2, [r3, #36]
	add	r2, r2, #1
	str	r2, [r3, #36]
	bl	ftimes_merge_new_mainline_allowed_ON_into_the_limit
	mov	r0, r4
	bl	ftimes_bump_this_station_groups_on_at_a_time_ON_count
	ldrb	r3, [r4, #105]	@ zero_extendqisi2
	mov	r2, #1
	tst	r3, #8
	ldr	r3, [r4, #28]
	strne	r2, [r3, #96]
	streq	r2, [r3, #100]
	b	.L258
.L265:
	ldr	r0, .L288+52
	bl	Alert_Message
	b	.L258
.L214:
.LBE51:
.LBE48:
.LBB52:
	ldr	r3, [r4, #32]
	ldr	r1, [r3, #208]
	cmp	r1, #0
	bne	.L269
	b	.L216
.L227:
	ldrh	r3, [r4, #104]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L228
	b	.L287
.L223:
	ldr	r0, .L288+4
	mov	r1, r4
	bl	nm_ListGetNext
.L280:
	mov	r4, r0
	b	.L275
.L199:
.LBE52:
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L289:
	.align	2
.L288:
	.word	ft_system_groups_list_hdr
	.word	ft_irrigating_stations_list_hdr
	.word	ftcs
	.word	ftcs+8
	.word	ufim_one_or_more_in_the_list_for_manual_program
	.word	ft_stations_ON_list_hdr
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	ft_stations_ON_by_controller
	.word	ft_electrical_limits
	.word	.LC19
.LFE30:
	.size	FTIMES_FUNCS_maintain_irrigation_list, .-FTIMES_FUNCS_maintain_irrigation_list
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Invalid schedule type\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ftim"
	.ascii	"es/ftimes_funcs.c\000"
.LC2:
	.ascii	"FTIMES: pftstation not on irrigation list! : %s, %u"
	.ascii	"\000"
.LC3:
	.ascii	"FTIMES: unexp ON count : %s, %u\000"
.LC4:
	.ascii	"FTIMES: should been on the ON list : %s, %u\000"
.LC5:
	.ascii	"FTIMES: system number ON 0\000"
.LC6:
	.ascii	"FTIMES: unexp system expected flow rate\000"
.LC7:
	.ascii	"FTIMES: unexp flow group count\000"
.LC8:
	.ascii	"FTIMES: on ON list! : %s, %u\000"
.LC9:
	.ascii	"FTIMES ERROR: duplicate problem : %s, %u\000"
.LC10:
	.ascii	"FTIMES ERROR: no room for duplicate : %s, %u\000"
.LC11:
	.ascii	"FTIMES ERROR: station in irri list for UNK reason :"
	.ascii	" %s, %u\000"
.LC12:
	.ascii	"FTIMES: too many valves ON in mainline\000"
.LC13:
	.ascii	"FTIMES: too many valves ON in station group\000"
.LC14:
	.ascii	"FTIMES: not on ON list!\000"
.LC15:
	.ascii	"FTIMES: -ve remaining seconds!!\000"
.LC16:
	.ascii	"FTIMES: is OFF but on the ON list\000"
.LC17:
	.ascii	"FOAL_IRRI: expected flow rate should be zero.\000"
.LC18:
	.ascii	"FTIMES: mix of flow groups ON.\000"
.LC19:
	.ascii	"FTIMES: run time 0\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI1-.LFB11
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI2-.LFB0
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI5-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI6-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI7-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI8-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI9-.LFB9
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI10-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI11-.LFB14
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x28
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xa
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI14-.LFB21
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI15-.LFB22
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI16-.LFB23
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI17-.LFB30
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE44:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ftimes/ftimes_funcs.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x245
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF31
	.byte	0x1
	.4byte	.LASF32
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1b9
	.byte	0x1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x221
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x31c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x35a
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x6ff
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x6e6
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x935
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x1
	.byte	0x54
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x2a1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x1
	.byte	0x18
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.byte	0x80
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.byte	0x9c
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.byte	0xbe
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.byte	0xdd
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST6
	.uleb128 0x7
	.4byte	0x21
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST7
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x1e2
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST8
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x20a
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.4byte	0x2b
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x250
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x37d
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST11
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x573
	.4byte	.LFB15
	.4byte	.LFE15
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x5c2
	.4byte	.LFB16
	.4byte	.LFE16
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x5df
	.4byte	.LFB17
	.4byte	.LFE17
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x5fa
	.4byte	.LFB18
	.4byte	.LFE18
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x611
	.4byte	.LFB19
	.4byte	.LFE19
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x61e
	.4byte	.LFB20
	.4byte	.LFE20
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x63c
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST12
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x68b
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST13
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x6a6
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST14
	.uleb128 0x3
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x71b
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x86f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x954
	.byte	0x1
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.2byte	0xa67
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST15
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB11
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB0
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB14
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	.LCFI13
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB21
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB22
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB23
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB30
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xcc
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF14:
	.ascii	"ftimes_complete_the_turn_ON\000"
.LASF31:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF11:
	.ascii	"FTIMES_FUNCS_find_station_group_in_ftimes_list_with"
	.ascii	"_this_GID\000"
.LASF29:
	.ascii	"ftimes_irrigation_maintenance_FOURTH__full_station_"
	.ascii	"list_pass_to_hunt_for_stations_to_turn_ON\000"
.LASF18:
	.ascii	"ftimes_will_exceed_system_capacity\000"
.LASF9:
	.ascii	"add_duplicate_ft_station\000"
.LASF2:
	.ascii	"we_are_at_a_start_time_and_a_water_day\000"
.LASF1:
	.ascii	"ftimes_number_of_starts_for_this_manual_program\000"
.LASF13:
	.ascii	"ftimes_if_station_is_ON_turn_it_OFF\000"
.LASF25:
	.ascii	"ftimes_init_on_at_a_time_and_completion_variables_i"
	.ascii	"n_all_station_groups\000"
.LASF21:
	.ascii	"ftimes_merge_new_mainline_allowed_ON_into_the_limit"
	.ascii	"\000"
.LASF23:
	.ascii	"ftimes_set_remaining_seconds_on\000"
.LASF27:
	.ascii	"ftimes_irrigation_maintenance_FIRST__full_list_pass"
	.ascii	"_to_develop_ufim_vars_and_remove_completed_stations"
	.ascii	"\000"
.LASF8:
	.ascii	"start_time_check_does_it_irrigate_on_this_date\000"
.LASF7:
	.ascii	"find_a_duplicate_ahead_of_this_station\000"
.LASF24:
	.ascii	"ftimes_init_ufim_variables_for_all_systems\000"
.LASF3:
	.ascii	"a_manual_program_wants_to_start\000"
.LASF32:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ftim"
	.ascii	"es/ftimes_funcs.c\000"
.LASF0:
	.ascii	"ftimes_turn_OFF_this_station_if_ON_and_remove_from_"
	.ascii	"all_irrigation_lists\000"
.LASF10:
	.ascii	"FTIMES_FUNCS_load_run_time_calculation_support\000"
.LASF4:
	.ascii	"ftimes_station_is_eligible_to_be_removed_from_the_i"
	.ascii	"rrigating_list\000"
.LASF22:
	.ascii	"ftimes_bump_this_station_groups_on_at_a_time_ON_cou"
	.ascii	"nt\000"
.LASF26:
	.ascii	"ftimes_for_each_station_group_check_if_irrigation_j"
	.ascii	"ust_completed\000"
.LASF15:
	.ascii	"ftimes_get_percent_adjust_100u\000"
.LASF30:
	.ascii	"FTIMES_FUNCS_maintain_irrigation_list\000"
.LASF16:
	.ascii	"ftimes_add_to_the_irrigation_list\000"
.LASF17:
	.ascii	"FTIMES_FUNCS_check_for_a_start\000"
.LASF5:
	.ascii	"ftimes_decrement_system_timers\000"
.LASF19:
	.ascii	"ftimes_the_on_at_a_time_rules_in_the_mainline_allow"
	.ascii	"_this_valve_to_come_ON\000"
.LASF6:
	.ascii	"ftimes_irrigation_maintenance_THIRD__full_station_l"
	.ascii	"ist_pass_to_turn_off_stations_that_should_not_be_ON"
	.ascii	"_based_upon_those_trying_to_come_ON\000"
.LASF20:
	.ascii	"ftimes_can_another_valve_come_ON_within_this_statio"
	.ascii	"n_group\000"
.LASF28:
	.ascii	"ftimes_irrigation_maintenance_SECOND__pass_through_"
	.ascii	"all_systems_to_develop_more_ufim_vars\000"
.LASF12:
	.ascii	"FTIMES_FUNCS_find_system_group_in_ftimes_list_with_"
	.ascii	"this_GID\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
