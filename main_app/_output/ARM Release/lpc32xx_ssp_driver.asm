	.file	"lpc32xx_ssp_driver.c"
	.text
.Ltext0:
	.section	.text.ssp_standard_interrupt,"ax",%progbits
	.align	2
	.type	ssp_standard_interrupt, %function
ssp_standard_interrupt:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldr	r4, [r0, #4]
	mov	r5, r0
	ldr	r3, [r4, #28]
	tst	r3, #7
	beq	.L2
	ldr	r3, [r0, #12]
	cmp	r3, #0
	ldreq	r3, [r4, #20]
	biceq	r3, r3, #6
	streq	r3, [r4, #20]
	beq	.L2
	blx	r3
	mov	r3, #3
	str	r3, [r4, #32]
.L2:
	ldr	r3, [r4, #28]
	tst	r3, #8
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r3, [r5, #8]
	cmp	r3, #0
	ldreq	r3, [r4, #20]
	biceq	r3, r3, #8
	streq	r3, [r4, #20]
	ldmeqfd	sp!, {r4, r5, pc}
	blx	r3
	ldmfd	sp!, {r4, r5, pc}
.LFE2:
	.size	ssp_standard_interrupt, .-ssp_standard_interrupt
	.global	__udivsi3
	.section	.text.ssp_configure,"ax",%progbits
	.align	2
	.type	ssp_configure, %function
ssp_configure:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, [r0, #0]
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI1:
	sub	r3, r2, #4
	cmp	r3, #12
	ldr	r3, [r0, #4]
	subls	ip, r2, #1
	movls	r4, #0
	mvnhi	r4, #0
	movhi	ip, #0
	cmp	r2, #8
	mov	r7, r1
	movhi	r2, #2
	movls	r2, #1
	str	r2, [r7, #20]
	bics	r2, r3, #48
	ldr	r1, [r1, #4]
	mvnne	r4, #0
	bne	.L9
	cmp	r3, #48
	mvneq	r4, #0
.L9:
	ldr	r2, [r0, #8]
	orr	r3, ip, r3
	cmp	r2, #1
	ldr	r2, [r0, #12]
	orreq	r3, r3, #64
	cmp	r2, #1
	ldr	r2, [r0, #20]
	orreq	r3, r3, #128
	cmp	r2, #0
	moveq	r2, #4
	movne	r2, #0
	cmp	r4, #0
	mvnne	r4, #0
	bne	.L13
	str	r3, [r1, #0]
	str	r2, [r1, #4]
.LBB4:
	ldr	r3, .L26
	ldr	r2, [r7, #16]
.LBE4:
	ldr	sl, [r0, #16]
.LBB5:
	ldr	r0, [r3, r2, asl #2]
	bl	clkpwr_get_clock_rate
	mov	r6, r4
	mov	r5, #2
	mov	r9, r0
	mvn	r0, #0
	b	.L25
.L16:
	add	r8, r6, #1
	mov	r0, r9
	mul	r1, r5, r8
	bl	__udivsi3
	cmp	r0, sl
	bls	.L24
	cmp	r8, #255
	addhi	r5, r5, #2
	movhi	r6, #0
	movls	r6, r8
.L25:
	cmp	r0, sl
	bhi	.L16
.L24:
	ldr	r3, [r7, #4]
	sub	r6, r6, #1
	ldr	r2, [r3, #0]
	mov	r6, r6, asl #24
	bic	r2, r2, #65280
	str	r2, [r3, #0]
	orr	r2, r2, r6, lsr #16
	str	r2, [r3, #0]
	str	r5, [r3, #16]
.L13:
.LBE5:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L27:
	.align	2
.L26:
	.word	.LANCHOR0
.LFE1:
	.size	ssp_configure, .-ssp_configure
	.section	.text.ssp_open,"ax",%progbits
	.align	2
	.global	ssp_open
	.type	ssp_open, %function
ssp_open:
.LFB3:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L40
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI2:
	cmp	r0, r3
	sub	sp, sp, #28
.LCFI3:
	mov	r5, r1
	ldreq	r4, .L40+4
	moveq	r3, #0
	streq	r3, [r4, #16]
	beq	.L30
.L29:
	ldr	r3, .L40+8
	cmp	r0, r3
	bne	.L39
	ldr	r4, .L40+4
	mov	r3, #1
	str	r3, [r4, #40]
	add	r4, r4, #24
.L30:
	ldr	r6, [r4, #0]
	cmp	r6, #0
	bne	.L39
	ldr	r3, .L40+12
	ldr	r2, [r4, #16]
	str	r0, [r4, #4]
	mov	r1, #1
	ldr	r0, [r3, r2, asl #2]
	bl	clkpwr_clk_en_dis
	cmp	r5, #0
	ldreq	r2, .L40+16
	moveq	r3, #8
	moveq	r0, sp
	movne	r0, r5
	str	r6, [r4, #8]
	str	r6, [r4, #12]
	mov	r1, r4
	streq	r5, [sp, #12]
	stmeqia	sp, {r3, r5}
	moveq	r3, #1
	streq	r3, [sp, #8]
	streq	r2, [sp, #16]
	streq	r3, [sp, #20]
	bl	ssp_configure
	cmn	r0, #1
	beq	.L34
	mov	r3, #1
	mov	r0, r4
	str	r3, [r4, #0]
.L36:
	ldr	r3, [r4, #4]
	b	.L35
.L34:
	ldr	r2, [r4, #16]
	ldr	r3, .L40+12
	mov	r1, #0
	ldr	r0, [r3, r2, asl #2]
	bl	clkpwr_clk_en_dis
	mov	r0, #0
	b	.L36
.L37:
	ldr	r2, [r3, #8]
	str	r2, [sp, #24]
.L35:
	ldr	r2, [r3, #12]
	tst	r2, #4
	bne	.L37
	mov	r2, #3
	str	r2, [r3, #32]
	mov	r2, #7
	str	r2, [r3, #20]
	b	.L31
.L39:
	mov	r0, #0
.L31:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, pc}
.L41:
	.align	2
.L40:
	.word	537411584
	.word	.LANCHOR1
	.word	537444352
	.word	.LANCHOR0
	.word	1000000
.LFE3:
	.size	ssp_open, .-ssp_open
	.section	.text.ssp_close,"ax",%progbits
	.align	2
	.global	ssp_close
	.type	ssp_close, %function
ssp_close:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #0]
	stmfd	sp!, {r4, lr}
.LCFI4:
	cmp	r3, #1
	bne	.L44
	ldr	r3, [r0, #4]
	mov	r4, #0
	ldr	r2, [r3, #4]
	str	r4, [r0, #0]
	bic	r2, r2, #2
	str	r2, [r3, #4]
	ldr	r2, [r0, #16]
	ldr	r3, .L45
	mov	r1, r4
	ldr	r0, [r3, r2, asl #2]
	bl	clkpwr_clk_en_dis
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L44:
	mvn	r0, #0
	ldmfd	sp!, {r4, pc}
.L46:
	.align	2
.L45:
	.word	.LANCHOR0
.LFE4:
	.size	ssp_close, .-ssp_close
	.section	.text.ssp_ioctl,"ax",%progbits
	.align	2
	.global	ssp_ioctl
	.type	ssp_ioctl, %function
ssp_ioctl:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, r0
	mov	r0, r2
	ldr	r2, [r3, #0]
	stmfd	sp!, {r4, r5, lr}
.LCFI5:
	cmp	r2, #1
	mvnne	r0, #0
	ldmnefd	sp!, {r4, r5, pc}
	ldr	ip, [r3, #4]
	cmp	r1, #6
	ldrls	pc, [pc, r1, asl #2]
	b	.L65
.L56:
	.word	.L49
	.word	.L50
	.word	.L51
	.word	.L52
	.word	.L53
	.word	.L54
	.word	.L55
.L49:
	ldr	r3, [ip, #4]
	cmp	r0, #1
	orreq	r3, r3, #2
	bicne	r3, r3, #2
	b	.L71
.L50:
	mov	r1, r3
	ldmfd	sp!, {r4, r5, lr}
	b	ssp_configure
.L51:
	ldr	r3, [ip, #4]
	cmp	r0, #1
	bicne	r3, r3, #1
	orreq	r3, r3, #1
.L71:
	str	r3, [ip, #4]
	b	.L69
.L52:
	ldr	r3, [ip, #4]
	cmp	r0, #0
	biceq	r3, r3, #8
	orrne	r3, r3, #8
	streq	r3, [ip, #4]
	ldmeqfd	sp!, {r4, r5, pc}
	b	.L71
.L53:
	ldr	r2, [r0, #0]
	str	r2, [r3, #8]
	ldr	r2, [r0, #4]
	str	r2, [r3, #12]
	b	.L69
.L54:
	and	r0, r0, #3
	str	r0, [ip, #32]
.L69:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, pc}
.L55:
	cmp	r0, #1
	ldreq	r0, [ip, #28]
	ldmeqfd	sp!, {r4, r5, pc}
	cmp	r0, #2
	ldreq	r0, [ip, #24]
	ldmeqfd	sp!, {r4, r5, pc}
	cmp	r0, #0
	bne	.L68
	ldr	r5, [ip, #0]
	ldr	r2, [r3, #16]
	ldr	r4, [ip, #16]
	ldr	r3, .L72
	and	r5, r5, #65280
	cmp	r4, #0
	mov	r5, r5, lsr #8
	ldr	r0, [r3, r2, asl #2]
	moveq	r4, #1
	bl	clkpwr_get_clock_rate
	add	r1, r5, #1
	mul	r1, r4, r1
	bl	__udivsi3
	ldmfd	sp!, {r4, r5, pc}
.L65:
	mvn	r0, #6
	ldmfd	sp!, {r4, r5, pc}
.L68:
	mvn	r0, #6
	ldmfd	sp!, {r4, r5, pc}
.L73:
	.align	2
.L72:
	.word	.LANCHOR0
.LFE5:
	.size	ssp_ioctl, .-ssp_ioctl
	.section	.text.ssp_read,"ax",%progbits
	.align	2
	.global	ssp_read
	.type	ssp_read, %function
ssp_read:
.LFB6:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI6:
	mov	r3, r0
	ldr	r0, [r0, #0]
	cmp	r0, #1
	movne	r0, #0
	bne	.L76
	b	.L81
.L79:
	ldr	r4, [r4, #8]
	add	r0, r0, #1
	str	r4, [sp, #0]
	ldr	r4, [r3, #20]
	cmp	r4, #1
	ldr	r4, [sp, #0]
	streqb	r4, [r1], #1
	strneh	r4, [ip], #2	@ movhi
	b	.L75
.L81:
	mov	ip, r1
	mov	r0, #0
.L75:
	rsb	r4, r0, r2
	cmp	r4, #0
	ble	.L76
	ldr	r4, [r3, #4]
	ldr	r5, [r4, #12]
	tst	r5, #4
	bne	.L79
.L76:
	ldmfd	sp!, {r3, r4, r5, pc}
.LFE6:
	.size	ssp_read, .-ssp_read
	.section	.text.ssp_write,"ax",%progbits
	.align	2
	.global	ssp_write
	.type	ssp_write, %function
ssp_write:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, r0
	ldr	r0, [r0, #0]
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	cmp	r0, #1
	movne	r0, #0
	ldmnefd	sp!, {r4, r5, pc}
	b	.L92
.L88:
	ldr	r5, [r3, #20]
	add	ip, ip, #1
	cmp	r5, #1
	ldreqb	r5, [r1], #1	@ zero_extendqisi2
	ldrneh	r5, [r4], #2
	str	r5, [r0, #8]
	b	.L83
.L92:
	mov	r4, r1
	mov	ip, #0
.L83:
	rsb	r0, ip, r2
	cmp	r0, #0
	ble	.L90
	ldr	r0, [r3, #4]
	ldr	r5, [r0, #12]
	tst	r5, #2
	bne	.L88
.L90:
	subs	r0, ip, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r3, [r3, #4]
	mov	r0, ip
	ldr	r2, [r3, #20]
	orr	r2, r2, #8
	str	r2, [r3, #20]
	ldmfd	sp!, {r4, r5, pc}
.LFE7:
	.size	ssp_write, .-ssp_write
	.section	.text.ssp0_int,"ax",%progbits
	.align	2
	.global	ssp0_int
	.type	ssp0_int, %function
ssp0_int:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L94
	b	ssp_standard_interrupt
.L95:
	.align	2
.L94:
	.word	.LANCHOR1
.LFE8:
	.size	ssp0_int, .-ssp0_int
	.section	.text.ssp1_int,"ax",%progbits
	.align	2
	.global	ssp1_int
	.type	ssp1_int, %function
ssp1_int:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L97
	b	ssp_standard_interrupt
.L98:
	.align	2
.L97:
	.word	.LANCHOR1+24
.LFE9:
	.size	ssp1_int, .-ssp1_int
	.section	.bss.sspdrv,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	sspdrv, %object
	.size	sspdrv, 48
sspdrv:
	.space	48
	.section	.rodata.sspclks,"a",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	sspclks, %object
	.size	sspclks, 8
sspclks:
	.word	3
	.word	2
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI6-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI7-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_ssp_driver.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xdd
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF9
	.byte	0x1
	.4byte	.LASF10
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.byte	0x4c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.byte	0xd9
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x86
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x116
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x179
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1a4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x22d
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST5
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x266
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST6
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x2a2
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x2b9
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB6
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB7
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF7:
	.ascii	"ssp0_int\000"
.LASF2:
	.ascii	"ssp_open\000"
.LASF9:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF8:
	.ascii	"ssp1_int\000"
.LASF6:
	.ascii	"ssp_write\000"
.LASF0:
	.ascii	"ssp_standard_interrupt\000"
.LASF10:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_ssp_driver.c\000"
.LASF1:
	.ascii	"ssp_configure\000"
.LASF4:
	.ascii	"ssp_ioctl\000"
.LASF11:
	.ascii	"ssp_set_clock\000"
.LASF5:
	.ascii	"ssp_read\000"
.LASF3:
	.ascii	"ssp_close\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
