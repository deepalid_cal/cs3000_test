	.file	"d_tech_support.c"
	.text
.Ltext0:
	.section	.text.FDTO_TECH_SUPPORT_draw_dialog,"ax",%progbits
	.align	2
	.type	FDTO_TECH_SUPPORT_draw_dialog, %function
FDTO_TECH_SUPPORT_draw_dialog:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L4
	cmp	r0, #1
	ldrsh	r1, [r3, #0]
	ldr	r3, .L4+4
	str	lr, [sp, #-4]!
.LCFI0:
	str	r1, [r3, #0]
	bne	.L2
	ldr	r3, .L4+8
	ldr	r2, [r3, #0]
	cmp	r2, #0
	moveq	r2, #50
	streq	r2, [r3, #0]
	ldr	r1, [r3, #0]
.L2:
	ldr	r3, .L4+12
	mov	r2, #1
	mov	r1, r1, asl #16
	str	r2, [r3, #0]
	mov	r0, #636
	mov	r1, r1, asr #16
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L5:
	.align	2
.L4:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
.LFE0:
	.size	FDTO_TECH_SUPPORT_draw_dialog, .-FDTO_TECH_SUPPORT_draw_dialog
	.section	.text.TECH_SUPPORT_draw_dialog,"ax",%progbits
	.align	2
	.global	TECH_SUPPORT_draw_dialog
	.type	TECH_SUPPORT_draw_dialog, %function
TECH_SUPPORT_draw_dialog:
.LFB1:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI1:
	mov	r3, #2
	sub	sp, sp, #36
.LCFI2:
	str	r3, [sp, #0]
	ldr	r3, .L7
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L8:
	.align	2
.L7:
	.word	FDTO_TECH_SUPPORT_draw_dialog
.LFE1:
	.size	TECH_SUPPORT_draw_dialog, .-TECH_SUPPORT_draw_dialog
	.section	.text.TECH_SUPPORT_process_dialog,"ax",%progbits
	.align	2
	.global	TECH_SUPPORT_process_dialog
	.type	TECH_SUPPORT_process_dialog, %function
TECH_SUPPORT_process_dialog:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	str	lr, [sp, #-4]!
.LCFI3:
	sub	sp, sp, #36
.LCFI4:
	beq	.L12
	bhi	.L15
	cmp	r0, #0
	beq	.L11
	b	.L10
.L15:
	cmp	r0, #4
	beq	.L13
	cmp	r0, #67
	bne	.L10
	b	.L23
.L12:
	ldr	r3, .L24
	ldr	r2, .L24+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #51
	str	r3, [r2, #0]
	beq	.L18
	cmp	r3, #52
	beq	.L19
	cmp	r3, #50
	bne	.L21
	ldr	r3, .L24+8
	mov	r2, #0
	str	r2, [r3, #0]
	mov	ip, #87
	mov	r3, #11
	stmib	sp, {r3, ip}
	ldr	r3, .L24+12
	str	r0, [sp, #0]
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	ldr	r3, .L24+16
	ldr	r3, [r3, #0]
	str	r3, [sp, #32]
	ldr	r3, .L24+20
	b	.L22
.L18:
	ldr	r3, .L24+8
	mov	r2, #0
	str	r2, [r3, #0]
	str	r0, [sp, #0]
	mov	r3, #105
	mov	r0, #11
	stmib	sp, {r0, r3}
	ldr	r3, .L24+24
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	ldr	r3, .L24+16
	ldr	r3, [r3, #0]
	str	r3, [sp, #32]
	ldr	r3, .L24+28
.L22:
	mov	r0, sp
	str	r3, [sp, #16]
	bl	Change_Screen
	b	.L9
.L19:
	ldr	r3, .L24+8
	mov	r2, #0
	str	r2, [r3, #0]
	mov	r1, #1
	mov	r3, #20
	mov	r2, #11
	stmia	sp, {r1, r2, r3}
	ldr	r3, .L24+32
	str	r3, [sp, #20]
	ldr	r3, .L24+16
	ldr	r3, [r3, #0]
	str	r3, [sp, #32]
	ldr	r3, .L24+36
	b	.L22
.L21:
	bl	bad_key_beep
	b	.L9
.L13:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L9
.L11:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L9
.L23:
	bl	good_key_beep
	ldr	r3, .L24
	ldr	r2, .L24+4
	ldrsh	r1, [r3, #0]
	mov	r0, #0
	str	r1, [r2, #0]
	ldr	r2, .L24+8
	str	r0, [r2, #0]
	ldr	r2, .L24+40
	ldr	r2, [r2, #0]
	strh	r2, [r3, #0]	@ movhi
	bl	Redraw_Screen
	b	.L9
.L10:
	bl	KEY_process_global_keys
.L9:
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L25:
	.align	2
.L24:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	FDTO_MEM_draw_report
	.word	GuiVar_MenuScreenToShow
	.word	MEM_process_report
	.word	FDTO_TASK_LIST_draw_report
	.word	TASK_LIST_process_report
	.word	FDTO_FACTORY_RESET_draw_screen
	.word	FACTORY_RESET_process_screen
	.word	.LANCHOR0
.LFE2:
	.size	TECH_SUPPORT_process_dialog, .-TECH_SUPPORT_process_dialog
	.global	g_TECH_SUPPORT_dialog_visible
	.global	g_TECH_SUPPORT_cursor_position_when_dialog_displayed
	.section	.bss.g_TECH_SUPPORT_cursor_position_when_dialog_displayed,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_TECH_SUPPORT_cursor_position_when_dialog_displayed, %object
	.size	g_TECH_SUPPORT_cursor_position_when_dialog_displayed, 4
g_TECH_SUPPORT_cursor_position_when_dialog_displayed:
	.space	4
	.section	.bss.g_TECH_SUPPORT_last_cursor_position,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_TECH_SUPPORT_last_cursor_position, %object
	.size	g_TECH_SUPPORT_last_cursor_position, 4
g_TECH_SUPPORT_last_cursor_position:
	.space	4
	.section	.bss.g_TECH_SUPPORT_dialog_visible,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_TECH_SUPPORT_dialog_visible, %object
	.size	g_TECH_SUPPORT_dialog_visible, 4
g_TECH_SUPPORT_dialog_visible:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_tech_support.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x59
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x31
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x4e
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x59
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_tech_support.c\000"
.LASF1:
	.ascii	"TECH_SUPPORT_process_dialog\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"FDTO_TECH_SUPPORT_draw_dialog\000"
.LASF0:
	.ascii	"TECH_SUPPORT_draw_dialog\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
