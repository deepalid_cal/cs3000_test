	.file	"queue.c"
	.text
.Ltext0:
	.section	.text.prvUnlockQueue,"ax",%progbits
	.align	2
	.type	prvUnlockQueue, %function
prvUnlockQueue:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	mov	r4, r0
	add	r5, r4, #36
	bl	vPortEnterCritical
	b	.L2
.L5:
	ldr	r3, [r4, #36]
	cmp	r3, #0
	beq	.L3
	mov	r0, r5
	bl	xTaskRemoveFromEventList
	cmp	r0, #0
	beq	.L4
	bl	vTaskMissedYield
.L4:
	ldr	r3, [r4, #72]
	sub	r3, r3, #1
	str	r3, [r4, #72]
.L2:
	ldr	r3, [r4, #72]
	cmp	r3, #0
	bgt	.L5
.L3:
	mvn	r3, #0
	str	r3, [r4, #72]
	bl	vPortExitCritical
	add	r5, r4, #16
	bl	vPortEnterCritical
	b	.L6
.L9:
	ldr	r3, [r4, #16]
	cmp	r3, #0
	beq	.L7
	mov	r0, r5
	bl	xTaskRemoveFromEventList
	cmp	r0, #0
	beq	.L8
	bl	vTaskMissedYield
.L8:
	ldr	r3, [r4, #68]
	sub	r3, r3, #1
	str	r3, [r4, #68]
.L6:
	ldr	r3, [r4, #68]
	cmp	r3, #0
	bgt	.L9
.L7:
	mvn	r3, #0
	str	r3, [r4, #68]
	ldmfd	sp!, {r4, r5, lr}
	b	vPortExitCritical
.LFE16:
	.size	prvUnlockQueue, .-prvUnlockQueue
	.section	.text.prvCopyDataFromQueue,"ax",%progbits
	.align	2
	.type	prvCopyDataFromQueue, %function
prvCopyDataFromQueue:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, [r0, #0]
	stmfd	sp!, {r4, lr}
.LCFI1:
	cmp	ip, #0
	mov	r3, r0
	ldmeqfd	sp!, {r4, pc}
	ldr	r4, [r0, #12]
	ldr	r2, [r0, #64]
	add	r4, r4, r2
	str	r4, [r0, #12]
	ldr	r0, [r0, #4]
	cmp	r4, r0
	strcs	ip, [r3, #12]
	mov	r0, r1
	ldr	r1, [r3, #12]
	ldmfd	sp!, {r4, lr}
	b	memcpy
.LFE15:
	.size	prvCopyDataFromQueue, .-prvCopyDataFromQueue
	.section	.text.prvCopyDataToQueue,"ax",%progbits
	.align	2
	.type	prvCopyDataToQueue, %function
prvCopyDataToQueue:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, r2
	ldr	r2, [r0, #64]
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	cmp	r2, #0
	mov	r4, r0
	bne	.L14
	ldr	r5, [r0, #0]
	cmp	r5, #0
	bne	.L15
	ldr	r0, [r0, #4]
	bl	vTaskPriorityDisinherit
	str	r5, [r4, #4]
	b	.L15
.L14:
	cmp	r3, #0
	bne	.L16
	ldr	r0, [r0, #8]
	bl	memcpy
	ldr	r2, [r4, #8]
	ldr	r3, [r4, #64]
	add	r3, r2, r3
	ldr	r2, [r4, #4]
	str	r3, [r4, #8]
	cmp	r3, r2
	ldrcs	r3, [r4, #0]
	strcs	r3, [r4, #8]
	b	.L15
.L16:
	ldr	r0, [r0, #12]
	bl	memcpy
	ldr	r3, [r4, #64]
	ldr	r2, [r4, #12]
	ldr	r1, [r4, #0]
	rsb	r3, r3, #0
	add	r2, r2, r3
	cmp	r2, r1
	str	r2, [r4, #12]
	ldrcc	r2, [r4, #4]
	addcc	r3, r2, r3
	strcc	r3, [r4, #12]
.L15:
	ldr	r3, [r4, #56]
	add	r3, r3, #1
	str	r3, [r4, #56]
	ldmfd	sp!, {r4, r5, pc}
.LFE14:
	.size	prvCopyDataToQueue, .-prvCopyDataToQueue
	.section	.text.xQueueGenericReset,"ax",%progbits
	.align	2
	.global	xQueueGenericReset
	.type	xQueueGenericReset, %function
xQueueGenericReset:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	subs	r4, r0, #0
	mov	r5, r1
	bne	.L18
	ldr	r0, .L21
	ldr	r1, .L21+4
	ldr	r2, .L21+8
	bl	__assert
.L18:
	bl	vPortEnterCritical
	ldr	r3, [r4, #0]
	ldr	r1, [r4, #60]
	ldr	r2, [r4, #64]
	str	r3, [r4, #8]
	mla	r0, r1, r2, r3
	sub	r1, r1, #1
	mla	r3, r2, r1, r3
	str	r0, [r4, #4]
	mov	r0, #0
	str	r3, [r4, #12]
	cmp	r5, r0
	mvn	r3, #0
	str	r0, [r4, #56]
	str	r3, [r4, #68]
	str	r3, [r4, #72]
	bne	.L19
	ldr	r3, [r4, #16]
	cmp	r3, r0
	beq	.L20
	add	r0, r4, #16
	bl	xTaskRemoveFromEventList
	cmp	r0, #1
	bne	.L20
@ 303 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c" 1
	SWI 0
@ 0 "" 2
	b	.L20
.L19:
	add	r0, r4, #16
	bl	vListInitialise
	add	r0, r4, #36
	bl	vListInitialise
.L20:
	bl	vPortExitCritical
	mov	r0, #1
	ldmfd	sp!, {r4, r5, pc}
.L22:
	.align	2
.L21:
	.word	.LC0
	.word	.LC1
	.word	281
.LFE0:
	.size	xQueueGenericReset, .-xQueueGenericReset
	.section	.text.xQueueGenericCreate,"ax",%progbits
	.align	2
	.global	xQueueGenericCreate
	.type	xQueueGenericCreate, %function
xQueueGenericCreate:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI4:
	subs	r6, r0, #0
	mov	r5, r1
	beq	.L24
	mov	r0, #76
	bl	pvPortMalloc
	subs	r4, r0, #0
	beq	.L24
	mul	r0, r6, r5
	add	r0, r0, #1
	bl	pvPortMalloc
	cmp	r0, #0
	str	r0, [r4, #0]
	beq	.L25
	str	r6, [r4, #60]
	str	r5, [r4, #64]
	mov	r0, r4
	mov	r1, #1
	bl	xQueueGenericReset
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L25:
	mov	r0, r4
	bl	vPortFree
.L24:
	ldr	r0, .L27
	ldr	r1, .L27+4
	ldr	r2, .L27+8
	bl	__assert
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L28:
	.align	2
.L27:
	.word	.LC2
	.word	.LC1
	.word	367
.LFE1:
	.size	xQueueGenericCreate, .-xQueueGenericCreate
	.section	.text.xQueueGetMutexHolder,"ax",%progbits
	.align	2
	.global	xQueueGetMutexHolder
	.type	xQueueGetMutexHolder, %function
xQueueGetMutexHolder:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI5:
	mov	r4, r0
	bl	vPortEnterCritical
	ldr	r3, [r4, #0]
	cmp	r3, #0
	ldreq	r4, [r4, #4]
	movne	r4, #0
	bl	vPortExitCritical
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.LFE3:
	.size	xQueueGetMutexHolder, .-xQueueGetMutexHolder
	.section	.text.my_take_recursive_failed,"ax",%progbits
	.align	2
	.global	my_take_recursive_failed
	.type	my_take_recursive_failed, %function
my_take_recursive_failed:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r3, r0, #52
	ldr	r0, .L33
	mov	r2, r2, asl #16
	mov	r2, r2, lsr #16
	b	Alert_Message_va
.L34:
	.align	2
.L33:
	.word	.LC3
.LFE5:
	.size	my_take_recursive_failed, .-my_take_recursive_failed
	.section	.text.xQueueGenericSend,"ax",%progbits
	.align	2
	.global	xQueueGenericSend
	.type	xQueueGenericSend, %function
xQueueGenericSend:
.LFB7:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI6:
	subs	r5, r0, #0
	mov	r4, r1
	str	r2, [sp, #0]
	mov	r6, r3
	bne	.L36
	ldr	r0, .L49
	ldr	r1, .L49+4
	mov	r2, #644
	bl	__assert
.L36:
	cmp	r4, #0
	bne	.L37
	ldr	r3, [r5, #64]
	cmp	r3, #0
	beq	.L37
	ldr	r0, .L49+8
	ldr	r1, .L49+4
	ldr	r2, .L49+12
	bl	__assert
.L37:
	mov	r8, #0
	mov	r7, r8
	b	.L38
.L48:
	mov	r8, #1
.L38:
	bl	vPortEnterCritical
	ldr	r2, [r5, #56]
	ldr	r3, [r5, #60]
	cmp	r2, r3
	bcs	.L39
	mov	r0, r5
	mov	r1, r4
	mov	r2, r6
	bl	prvCopyDataToQueue
	ldr	r3, [r5, #36]
	cmp	r3, #0
	beq	.L40
	add	r0, r5, #36
	bl	xTaskRemoveFromEventList
	cmp	r0, #1
	bne	.L40
@ 671 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c" 1
	SWI 0
@ 0 "" 2
.L40:
	bl	vPortExitCritical
	mov	r0, #1
	b	.L41
.L39:
	ldr	sl, [sp, #0]
	cmp	sl, #0
	bne	.L42
	bl	vPortExitCritical
	mov	r0, sl
	b	.L41
.L42:
	cmp	r8, #0
	bne	.L43
	add	r0, sp, #4
	bl	vTaskSetTimeOutState
.L43:
	bl	vPortExitCritical
	bl	vTaskSuspendAll
	bl	vPortEnterCritical
	ldr	r3, [r5, #68]
	cmn	r3, #1
	streq	r7, [r5, #68]
	ldr	r3, [r5, #72]
	cmn	r3, #1
	streq	r7, [r5, #72]
	bl	vPortExitCritical
	add	r0, sp, #4
	mov	r1, sp
	bl	xTaskCheckForTimeOut
	cmp	r0, #0
	bne	.L46
.LBB8:
	bl	vPortEnterCritical
	ldr	sl, [r5, #56]
	ldr	r8, [r5, #60]
	bl	vPortExitCritical
.LBE8:
	cmp	sl, r8
	bne	.L47
	ldr	r1, [sp, #0]
	add	r0, r5, #16
	bl	vTaskPlaceOnEventList
	mov	r0, r5
	bl	prvUnlockQueue
	bl	xTaskResumeAll
	cmp	r0, #0
	bne	.L48
@ 733 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c" 1
	SWI 0
@ 0 "" 2
	b	.L48
.L47:
	mov	r0, r5
	bl	prvUnlockQueue
	bl	xTaskResumeAll
	b	.L48
.L46:
	mov	r0, r5
	bl	prvUnlockQueue
	bl	xTaskResumeAll
	mov	r0, #0
.L41:
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, sl, pc}
.L50:
	.align	2
.L49:
	.word	.LC0
	.word	.LC1
	.word	.LC4
	.word	645
.LFE7:
	.size	xQueueGenericSend, .-xQueueGenericSend
	.section	.text.xQueueGiveMutexRecursive,"ax",%progbits
	.align	2
	.global	xQueueGiveMutexRecursive
	.type	xQueueGiveMutexRecursive, %function
xQueueGiveMutexRecursive:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	subs	r4, r0, #0
	bne	.L52
	ldr	r0, .L56
	ldr	r1, .L56+4
	mov	r2, #468
	bl	__assert
.L52:
	ldr	r5, [r4, #4]
	bl	xTaskGetCurrentTaskHandle
	cmp	r5, r0
	movne	r0, #0
	ldmnefd	sp!, {r4, r5, pc}
	ldr	r1, [r4, #12]
	sub	r1, r1, #1
	cmp	r1, #0
	str	r1, [r4, #12]
	bne	.L55
	mov	r0, r4
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
	mov	r0, #1
	ldmfd	sp!, {r4, r5, pc}
.L55:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, pc}
.L57:
	.align	2
.L56:
	.word	.LC5
	.word	.LC1
.LFE4:
	.size	xQueueGiveMutexRecursive, .-xQueueGiveMutexRecursive
	.section	.text.xQueueCreateMutex,"ax",%progbits
	.align	2
	.global	xQueueCreateMutex
	.type	xQueueCreateMutex, %function
xQueueCreateMutex:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI8:
	mov	r0, #76
	bl	pvPortMalloc
	subs	r4, r0, #0
	beq	.L59
	mov	r3, #1
	mov	r5, #0
	str	r3, [r4, #60]
	mvn	r3, #0
	str	r5, [r4, #56]
	add	r0, r4, #16
	str	r3, [r4, #68]
	str	r5, [r4, #4]
	str	r3, [r4, #72]
	str	r5, [r4, #0]
	str	r5, [r4, #8]
	str	r5, [r4, #12]
	str	r5, [r4, #64]
	bl	vListInitialise
	add	r0, r4, #36
	bl	vListInitialise
	mov	r0, r4
	mov	r1, r5
	mov	r2, r5
	mov	r3, r5
	bl	xQueueGenericSend
	b	.L60
.L59:
	ldr	r0, .L61
	ldr	r1, .L61+4
	ldr	r2, .L61+8
	bl	__assert
.L60:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.L62:
	.align	2
.L61:
	.word	.LC6
	.word	.LC1
	.word	425
.LFE2:
	.size	xQueueCreateMutex, .-xQueueCreateMutex
	.section	.text.xQueueGenericSendFromISR,"ax",%progbits
	.align	2
	.global	xQueueGenericSendFromISR
	.type	xQueueGenericSendFromISR, %function
xQueueGenericSendFromISR:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI9:
	subs	r6, r0, #0
	mov	r5, r1
	mov	r4, r2
	mov	r7, r3
	bne	.L64
	ldr	r0, .L72
	ldr	r1, .L72+4
	ldr	r2, .L72+8
	bl	__assert
.L64:
	cmp	r5, #0
	bne	.L65
	ldr	r3, [r6, #64]
	cmp	r3, #0
	beq	.L65
	ldr	r0, .L72+12
	ldr	r1, .L72+4
	ldr	r2, .L72+16
	bl	__assert
.L65:
	ldr	r2, [r6, #56]
	ldr	r3, [r6, #60]
	cmp	r2, r3
	movcs	r0, #0
	ldmcsfd	sp!, {r4, r5, r6, r7, pc}
	mov	r0, r6
	mov	r1, r5
	mov	r2, r7
	bl	prvCopyDataToQueue
	ldr	r3, [r6, #72]
	cmn	r3, #1
	bne	.L67
	ldr	r3, [r6, #36]
	cmp	r3, #0
	beq	.L69
	add	r0, r6, #36
	bl	xTaskRemoveFromEventList
	cmp	r0, #0
	mov	r0, #1
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	cmp	r4, #0
	strne	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L67:
	ldr	r3, [r6, #72]
	mov	r0, #1
	add	r3, r3, #1
	str	r3, [r6, #72]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L69:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L73:
	.align	2
.L72:
	.word	.LC0
	.word	.LC1
	.word	969
	.word	.LC4
	.word	970
.LFE8:
	.size	xQueueGenericSendFromISR, .-xQueueGenericSendFromISR
	.section	.text.xQueueGenericReceive,"ax",%progbits
	.align	2
	.global	xQueueGenericReceive
	.type	xQueueGenericReceive, %function
xQueueGenericReceive:
.LFB9:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI10:
	subs	r5, r0, #0
	mov	r4, r1
	str	r2, [sp, #0]
	mov	r6, r3
	bne	.L75
	ldr	r0, .L92
	ldr	r1, .L92+4
	ldr	r2, .L92+8
	bl	__assert
.L75:
	cmp	r4, #0
	bne	.L76
	ldr	r3, [r5, #64]
	cmp	r3, #0
	beq	.L76
	ldr	r0, .L92+12
	ldr	r1, .L92+4
	ldr	r2, .L92+16
	bl	__assert
.L76:
	mov	r8, #0
	mov	r7, r8
	b	.L77
.L90:
	mov	r8, #1
.L77:
	bl	vPortEnterCritical
	ldr	r3, [r5, #56]
	cmp	r3, #0
	beq	.L78
	mov	r0, r5
	mov	r1, r4
	ldr	r7, [r5, #12]
	bl	prvCopyDataFromQueue
	cmp	r6, #0
	bne	.L79
	ldr	r3, [r5, #56]
	sub	r3, r3, #1
	str	r3, [r5, #56]
	ldr	r3, [r5, #0]
	cmp	r3, #0
	bne	.L80
	bl	xTaskGetCurrentTaskHandle
	str	r0, [r5, #4]
.L80:
	ldr	r3, [r5, #16]
	cmp	r3, #0
	beq	.L81
	add	r0, r5, #16
	bl	xTaskRemoveFromEventList
	cmp	r0, #1
	bne	.L81
	b	.L91
.L79:
	ldr	r3, [r5, #36]
	str	r7, [r5, #12]
	cmp	r3, #0
	beq	.L81
	add	r0, r5, #36
	bl	xTaskRemoveFromEventList
	cmp	r0, #0
	beq	.L81
.L91:
@ 1092 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c" 1
	SWI 0
@ 0 "" 2
.L81:
	bl	vPortExitCritical
	mov	r0, #1
	b	.L82
.L78:
	ldr	sl, [sp, #0]
	cmp	sl, #0
	bne	.L83
	bl	vPortExitCritical
	mov	r0, sl
	b	.L82
.L83:
	cmp	r8, #0
	bne	.L84
	add	r0, sp, #4
	bl	vTaskSetTimeOutState
.L84:
	bl	vPortExitCritical
	bl	vTaskSuspendAll
	bl	vPortEnterCritical
	ldr	r3, [r5, #68]
	cmn	r3, #1
	streq	r7, [r5, #68]
	ldr	r3, [r5, #72]
	cmn	r3, #1
	streq	r7, [r5, #72]
	bl	vPortExitCritical
	add	r0, sp, #4
	mov	r1, sp
	bl	xTaskCheckForTimeOut
	cmp	r0, #0
	bne	.L87
.LBB9:
	bl	vPortEnterCritical
	ldr	r8, [r5, #56]
	bl	vPortExitCritical
.LBE9:
	cmp	r8, #0
	bne	.L88
	ldr	r3, [r5, #0]
	cmp	r3, #0
	bne	.L89
	bl	vPortEnterCritical
	ldr	r0, [r5, #4]
	bl	vTaskPriorityInherit
	bl	vPortExitCritical
.L89:
	ldr	r1, [sp, #0]
	add	r0, r5, #36
	bl	vTaskPlaceOnEventList
	mov	r0, r5
	bl	prvUnlockQueue
	bl	xTaskResumeAll
	cmp	r0, #0
	bne	.L90
@ 1151 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c" 1
	SWI 0
@ 0 "" 2
	b	.L90
.L88:
	mov	r0, r5
	bl	prvUnlockQueue
	bl	xTaskResumeAll
	b	.L90
.L87:
	mov	r0, r5
	bl	prvUnlockQueue
	bl	xTaskResumeAll
	mov	r0, #0
.L82:
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, sl, pc}
.L93:
	.align	2
.L92:
	.word	.LC0
	.word	.LC1
	.word	1029
	.word	.LC7
	.word	1030
.LFE9:
	.size	xQueueGenericReceive, .-xQueueGenericReceive
	.section	.text.xQueueTakeMutexRecursive_debug,"ax",%progbits
	.align	2
	.global	xQueueTakeMutexRecursive_debug
	.type	xQueueTakeMutexRecursive_debug, %function
xQueueTakeMutexRecursive_debug:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI11:
	mov	r3, r3, asl #16
	subs	r4, r0, #0
	mov	r7, r1
	mov	r6, r2
	mov	r5, r3, lsr #16
	bne	.L95
	ldr	r0, .L99
	ldr	r1, .L99+4
	ldr	r2, .L99+8
	bl	__assert
.L95:
	ldr	r8, [r4, #4]
	bl	xTaskGetCurrentTaskHandle
	cmp	r8, r0
	ldreq	r3, [r4, #12]
	moveq	r7, #1
	addeq	r3, r3, #1
	streq	r3, [r4, #12]
	beq	.L97
.L96:
	mov	r1, #0
	mov	r3, r1
	mov	r2, r7
	mov	r0, r4
	bl	xQueueGenericReceive
	cmp	r0, #1
	ldreq	r3, [r4, #12]
	mov	r7, r0
	addeq	r3, r3, #1
	streq	r3, [r4, #12]
	beq	.L97
	mov	r0, r6
	ldr	r4, [r4, #4]
	bl	RemovePathFromFileName
	mov	r2, r5
	mov	r1, r0
	mov	r0, r4
	bl	my_take_recursive_failed
.L97:
	mov	r0, r7
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L100:
	.align	2
.L99:
	.word	.LC5
	.word	.LC1
	.word	577
.LFE6:
	.size	xQueueTakeMutexRecursive_debug, .-xQueueTakeMutexRecursive_debug
	.section	.text.xQueueReceiveFromISR,"ax",%progbits
	.align	2
	.global	xQueueReceiveFromISR
	.type	xQueueReceiveFromISR, %function
xQueueReceiveFromISR:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI12:
	subs	r4, r0, #0
	mov	r6, r1
	mov	r5, r2
	bne	.L102
	ldr	r0, .L110
	ldr	r1, .L110+4
	ldr	r2, .L110+8
	bl	__assert
.L102:
	cmp	r6, #0
	bne	.L103
	ldr	r3, [r4, #64]
	cmp	r3, #0
	beq	.L103
	ldr	r0, .L110+12
	ldr	r1, .L110+4
	ldr	r2, .L110+16
	bl	__assert
.L103:
	ldr	r0, [r4, #56]
	cmp	r0, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	mov	r0, r4
	mov	r1, r6
	bl	prvCopyDataFromQueue
	ldr	r3, [r4, #56]
	sub	r3, r3, #1
	str	r3, [r4, #56]
	ldr	r3, [r4, #68]
	cmn	r3, #1
	bne	.L105
	ldr	r3, [r4, #16]
	cmp	r3, #0
	beq	.L107
	add	r0, r4, #16
	bl	xTaskRemoveFromEventList
	cmp	r0, #0
	mov	r0, #1
	ldmeqfd	sp!, {r4, r5, r6, pc}
	cmp	r5, #0
	strne	r0, [r5, #0]
	ldmfd	sp!, {r4, r5, r6, pc}
.L105:
	ldr	r3, [r4, #68]
	mov	r0, #1
	add	r3, r3, #1
	str	r3, [r4, #68]
	ldmfd	sp!, {r4, r5, r6, pc}
.L107:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, r6, pc}
.L111:
	.align	2
.L110:
	.word	.LC0
	.word	.LC1
	.word	1177
	.word	.LC7
	.word	1178
.LFE10:
	.size	xQueueReceiveFromISR, .-xQueueReceiveFromISR
	.section	.text.uxQueueMessagesWaiting,"ax",%progbits
	.align	2
	.global	uxQueueMessagesWaiting
	.type	uxQueueMessagesWaiting, %function
uxQueueMessagesWaiting:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI13:
	subs	r4, r0, #0
	bne	.L113
	ldr	r0, .L114
	ldr	r1, .L114+4
	ldr	r2, .L114+8
	bl	__assert
.L113:
	bl	vPortEnterCritical
	ldr	r4, [r4, #56]
	bl	vPortExitCritical
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L115:
	.align	2
.L114:
	.word	.LC0
	.word	.LC1
	.word	1233
.LFE11:
	.size	uxQueueMessagesWaiting, .-uxQueueMessagesWaiting
	.section	.text.uxQueueMessagesWaitingFromISR,"ax",%progbits
	.align	2
	.global	uxQueueMessagesWaitingFromISR
	.type	uxQueueMessagesWaitingFromISR, %function
uxQueueMessagesWaitingFromISR:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI14:
	subs	r4, r0, #0
	bne	.L117
	ldr	r0, .L118
	ldr	r1, .L118+4
	ldr	r2, .L118+8
	bl	__assert
.L117:
	ldr	r0, [r4, #56]
	ldmfd	sp!, {r4, pc}
.L119:
	.align	2
.L118:
	.word	.LC0
	.word	.LC1
	.word	1247
.LFE12:
	.size	uxQueueMessagesWaitingFromISR, .-uxQueueMessagesWaitingFromISR
	.section	.text.vQueueDelete,"ax",%progbits
	.align	2
	.global	vQueueDelete
	.type	vQueueDelete, %function
vQueueDelete:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI15:
	subs	r4, r0, #0
	bne	.L121
	ldr	r0, .L126
	ldr	r1, .L126+4
	ldr	r2, .L126+8
	bl	__assert
.L121:
	ldr	r2, .L126+12
	mov	r3, #0
	mov	r1, r2
.L124:
.LBB10:
	ldr	r0, [r2, #4]
	cmp	r0, r4
	moveq	r2, #0
	streq	r2, [r1, r3, asl #3]
	beq	.L123
.L122:
	add	r3, r3, #1
	cmp	r3, #10
	add	r2, r2, #8
	bne	.L124
.L123:
.LBE10:
	ldr	r0, [r4, #0]
	bl	vPortFree
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	b	vPortFree
.L127:
	.align	2
.L126:
	.word	.LC0
	.word	.LC1
	.word	1257
	.word	.LANCHOR0
.LFE13:
	.size	vQueueDelete, .-vQueueDelete
	.section	.text.xQueueIsQueueEmptyFromISR,"ax",%progbits
	.align	2
	.global	xQueueIsQueueEmptyFromISR
	.type	xQueueIsQueueEmptyFromISR, %function
xQueueIsQueueEmptyFromISR:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI16:
	subs	r4, r0, #0
	bne	.L129
	ldr	r0, .L130
	ldr	r1, .L130+4
	ldr	r2, .L130+8
	bl	__assert
.L129:
	ldr	r0, [r4, #56]
	rsbs	r0, r0, #1
	movcc	r0, #0
	ldmfd	sp!, {r4, pc}
.L131:
	.align	2
.L130:
	.word	.LC0
	.word	.LC1
	.word	1428
.LFE18:
	.size	xQueueIsQueueEmptyFromISR, .-xQueueIsQueueEmptyFromISR
	.section	.text.xQueueIsQueueFullFromISR,"ax",%progbits
	.align	2
	.global	xQueueIsQueueFullFromISR
	.type	xQueueIsQueueFullFromISR, %function
xQueueIsQueueFullFromISR:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI17:
	subs	r4, r0, #0
	bne	.L133
	ldr	r0, .L134
	ldr	r1, .L134+4
	ldr	r2, .L134+8
	bl	__assert
.L133:
	ldr	r0, [r4, #56]
	ldr	r3, [r4, #60]
	rsb	r3, r3, r0
	rsbs	r0, r3, #0
	adc	r0, r0, r3
	ldmfd	sp!, {r4, pc}
.L135:
	.align	2
.L134:
	.word	.LC0
	.word	.LC1
	.word	1451
.LFE20:
	.size	xQueueIsQueueFullFromISR, .-xQueueIsQueueFullFromISR
	.section	.text.vQueueAddToRegistry,"ax",%progbits
	.align	2
	.global	vQueueAddToRegistry
	.type	vQueueAddToRegistry, %function
vQueueAddToRegistry:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI18:
	ldr	r4, .L141
	mov	r3, #0
.L139:
	ldr	r5, [r4, r3, asl #3]
	ldr	r2, .L141
	cmp	r5, #0
	mov	ip, r3, asl #3
	bne	.L137
	str	r1, [r2, r3, asl #3]
	add	r2, r2, ip
	str	r0, [r2, #4]
	ldmfd	sp!, {r4, r5, pc}
.L137:
	add	r3, r3, #1
	cmp	r3, #10
	bne	.L139
	ldmfd	sp!, {r4, r5, pc}
.L142:
	.align	2
.L141:
	.word	.LANCHOR0
.LFE21:
	.size	vQueueAddToRegistry, .-vQueueAddToRegistry
	.section	.text.vQueueWaitForMessageRestricted,"ax",%progbits
	.align	2
	.global	vQueueWaitForMessageRestricted
	.type	vQueueWaitForMessageRestricted, %function
vQueueWaitForMessageRestricted:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI19:
	mov	r4, r0
	mov	r5, r1
	bl	vPortEnterCritical
	ldr	r3, [r4, #68]
	cmn	r3, #1
	moveq	r3, #0
	streq	r3, [r4, #68]
	ldr	r3, [r4, #72]
	cmn	r3, #1
	moveq	r3, #0
	streq	r3, [r4, #72]
	bl	vPortExitCritical
	ldr	r3, [r4, #56]
	cmp	r3, #0
	bne	.L146
	add	r0, r4, #36
	mov	r1, r5
	bl	vTaskPlaceOnEventListRestricted
.L146:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, lr}
	b	prvUnlockQueue
.LFE23:
	.size	vQueueWaitForMessageRestricted, .-vQueueWaitForMessageRestricted
	.global	xQueueRegistry
	.section	.bss.xQueueRegistry,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	xQueueRegistry, %object
	.size	xQueueRegistry, 80
xQueueRegistry:
	.space	80
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"pxQueue\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/queue.c\000"
.LC2:
	.ascii	"xReturn\000"
.LC3:
	.ascii	"MUTEX TAKE FAILED: %s, %u, held by %s\000"
.LC4:
	.ascii	"!( ( pvItemToQueue == 0 ) && ( pxQueue->uxItemSize "
	.ascii	"!= ( unsigned long ) 0U ) )\000"
.LC5:
	.ascii	"pxMutex\000"
.LC6:
	.ascii	"pxNewQueue\000"
.LC7:
	.ascii	"!( ( pvBuffer == 0 ) && ( pxQueue->uxItemSize != ( "
	.ascii	"unsigned long ) 0U ) )\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI0-.LFB16
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI1-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI2-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI3-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI4-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI5-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI6-.LFB7
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI7-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI8-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI9-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI10-.LFB9
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI11-.LFB6
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI12-.LFB10
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI13-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI14-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI15-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI16-.LFB18
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI17-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI18-.LFB21
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI19-.LFB23
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE40:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/queue.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1ee
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF24
	.byte	0x1
	.4byte	.LASF25
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x59b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x584
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x69f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x544
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x536
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x510
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x117
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST3
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x142
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST4
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x1b2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x230
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x27f
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST6
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1d0
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST7
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x177
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST8
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x3c4
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST9
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x3ff
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST10
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x23c
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST11
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x494
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST12
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x4cd
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST13
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x4db
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST14
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x4e7
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST15
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x590
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST16
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x5a7
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST17
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x688
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST18
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x6b6
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST19
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB16
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB15
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB14
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB0
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB1
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB3
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB7
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB4
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB2
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB6
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB10
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB11
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB12
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB13
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB18
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB20
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB21
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB23
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xbc
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF7:
	.ascii	"xQueueGenericCreate\000"
.LASF24:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF19:
	.ascii	"vQueueDelete\000"
.LASF15:
	.ascii	"xQueueTakeMutexRecursive_debug\000"
.LASF4:
	.ascii	"prvCopyDataFromQueue\000"
.LASF10:
	.ascii	"xQueueGenericSend\000"
.LASF22:
	.ascii	"vQueueAddToRegistry\000"
.LASF2:
	.ascii	"vQueueUnregisterQueue\000"
.LASF17:
	.ascii	"uxQueueMessagesWaiting\000"
.LASF3:
	.ascii	"prvUnlockQueue\000"
.LASF18:
	.ascii	"uxQueueMessagesWaitingFromISR\000"
.LASF14:
	.ascii	"xQueueGenericReceive\000"
.LASF16:
	.ascii	"xQueueReceiveFromISR\000"
.LASF21:
	.ascii	"xQueueIsQueueFullFromISR\000"
.LASF23:
	.ascii	"vQueueWaitForMessageRestricted\000"
.LASF0:
	.ascii	"prvIsQueueFull\000"
.LASF12:
	.ascii	"xQueueCreateMutex\000"
.LASF13:
	.ascii	"xQueueGenericSendFromISR\000"
.LASF20:
	.ascii	"xQueueIsQueueEmptyFromISR\000"
.LASF25:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/queue.c\000"
.LASF6:
	.ascii	"xQueueGenericReset\000"
.LASF1:
	.ascii	"prvIsQueueEmpty\000"
.LASF5:
	.ascii	"prvCopyDataToQueue\000"
.LASF9:
	.ascii	"my_take_recursive_failed\000"
.LASF8:
	.ascii	"xQueueGetMutexHolder\000"
.LASF11:
	.ascii	"xQueueGiveMutexRecursive\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
