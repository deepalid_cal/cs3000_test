	.file	"lpc32xx_gpio_driver.c"
	.text
.Ltext0:
	.section	.text.gpio_set_gpo_state,"ax",%progbits
	.align	2
	.global	gpio_set_gpo_state
	.type	gpio_set_gpo_state, %function
gpio_set_gpo_state:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L4
	cmp	r0, #0
	strne	r0, [r3, #4]
	cmp	r1, #0
	strne	r1, [r3, #8]
	ldr	r0, [r3, #12]
	bx	lr
.L5:
	.align	2
.L4:
	.word	1073905664
.LFE0:
	.size	gpio_set_gpo_state, .-gpio_set_gpo_state
	.section	.text.gpio_set_dir,"ax",%progbits
	.align	2
	.global	gpio_set_dir
	.type	gpio_set_dir, %function
gpio_set_dir:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L9
	cmp	r0, #0
	strne	r0, [r3, #16]
	cmp	r1, #0
	strne	r1, [r3, #20]
	ldr	r0, [r3, #24]
	bx	lr
.L10:
	.align	2
.L9:
	.word	1073905664
.LFE1:
	.size	gpio_set_dir, .-gpio_set_dir
	.section	.text.gpio_set_sdr_state,"ax",%progbits
	.align	2
	.global	gpio_set_sdr_state
	.type	gpio_set_sdr_state, %function
gpio_set_sdr_state:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L14
	cmp	r0, #0
	strne	r0, [r3, #32]
	cmp	r1, #0
	strne	r1, [r3, #36]
	ldr	r0, [r3, #28]
	bx	lr
.L15:
	.align	2
.L14:
	.word	1073905664
.LFE2:
	.size	gpio_set_sdr_state, .-gpio_set_sdr_state
	.section	.text.gpio_set_p0_state,"ax",%progbits
	.align	2
	.global	gpio_set_p0_state
	.type	gpio_set_p0_state, %function
gpio_set_p0_state:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L19
	cmp	r0, #0
	strne	r0, [r3, #68]
	cmp	r1, #0
	strne	r1, [r3, #72]
	ldr	r0, [r3, #76]
	bx	lr
.L20:
	.align	2
.L19:
	.word	1073905664
.LFE3:
	.size	gpio_set_p0_state, .-gpio_set_p0_state
	.section	.text.gpio_set_p0_dir,"ax",%progbits
	.align	2
	.global	gpio_set_p0_dir
	.type	gpio_set_p0_dir, %function
gpio_set_p0_dir:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L24
	cmp	r0, #0
	strne	r0, [r3, #80]
	cmp	r1, #0
	strne	r1, [r3, #84]
	ldr	r0, [r3, #88]
	bx	lr
.L25:
	.align	2
.L24:
	.word	1073905664
.LFE4:
	.size	gpio_set_p0_dir, .-gpio_set_p0_dir
	.section	.text.gpio_set_p1_state,"ax",%progbits
	.align	2
	.global	gpio_set_p1_state
	.type	gpio_set_p1_state, %function
gpio_set_p1_state:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L29
	cmp	r0, #0
	strne	r0, [r3, #100]
	cmp	r1, #0
	strne	r1, [r3, #104]
	ldr	r0, [r3, #108]
	bx	lr
.L30:
	.align	2
.L29:
	.word	1073905664
.LFE5:
	.size	gpio_set_p1_state, .-gpio_set_p1_state
	.section	.text.gpio_set_p1_dir,"ax",%progbits
	.align	2
	.global	gpio_set_p1_dir
	.type	gpio_set_p1_dir, %function
gpio_set_p1_dir:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L34
	cmp	r0, #0
	strne	r0, [r3, #112]
	cmp	r1, #0
	strne	r1, [r3, #116]
	ldr	r0, [r3, #120]
	bx	lr
.L35:
	.align	2
.L34:
	.word	1073905664
.LFE6:
	.size	gpio_set_p1_dir, .-gpio_set_p1_dir
	.section	.text.i2s_pin_mux,"ax",%progbits
	.align	2
	.global	i2s_pin_mux
	.type	i2s_pin_mux, %function
i2s_pin_mux:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bne	.L37
	cmp	r1, #1
	ldr	r3, .L43
	mov	r2, #252
	bne	.L41
	b	.L42
.L37:
	cmp	r0, #1
	bxne	lr
	ldr	r3, .L43
	cmp	r1, #1
	mov	r1, #28
	mov	r2, #3
	strne	r1, [r3, #256]
	bne	.L41
	str	r1, [r3, #260]
.L42:
	str	r2, [r3, #288]
	bx	lr
.L41:
	str	r2, [r3, #292]
	bx	lr
.L44:
	.align	2
.L43:
	.word	1073905664
.LFE7:
	.size	i2s_pin_mux, .-i2s_pin_mux
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_gpio_driver.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xb8
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x35
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x5b
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x81
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xa7
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xcb
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xf1
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x115
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x137
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF6:
	.ascii	"gpio_set_p1_dir\000"
.LASF1:
	.ascii	"gpio_set_dir\000"
.LASF3:
	.ascii	"gpio_set_p0_state\000"
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_gpio_driver.c\000"
.LASF0:
	.ascii	"gpio_set_gpo_state\000"
.LASF2:
	.ascii	"gpio_set_sdr_state\000"
.LASF4:
	.ascii	"gpio_set_p0_dir\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF7:
	.ascii	"i2s_pin_mux\000"
.LASF5:
	.ascii	"gpio_set_p1_state\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
