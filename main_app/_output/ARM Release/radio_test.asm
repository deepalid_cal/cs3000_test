	.file	"radio_test.c"
	.text
.Ltext0:
	.section	.text.update_radio_test_gui_vars,"ax",%progbits
	.align	2
	.type	update_radio_test_gui_vars, %function
update_radio_test_gui_vars:
.LFB9:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L5
	sub	sp, sp, #8
.LCFI0:
	str	r3, [sp, #0]	@ float
	ldr	r3, .L5+4
	ldr	r2, .L5+8
	str	r3, [sp, #4]	@ float
	ldr	r3, .L5+12
	flds	s15, [sp, #4]
	ldr	r1, [r3, #0]
	subs	r1, r1, #256
	movne	r1, #1
	str	r1, [r2, #0]
	ldr	r2, .L5+16
	cmp	r0, #0
	ldr	r1, [r2, #0]
	ldr	r2, [r3, #4]
	rsb	r2, r2, r1
	add	r2, r2, r2, asl #2
	fmsr	s12, r2	@ int
	ldr	r2, .L5+20
	fuitos	s14, s12
	fdivs	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r2, #0]	@ int
	beq	.L2
	flds	s15, [r3, #564]	@ int
	ldr	r3, [r3, #552]
	flds	s14, [sp, #0]
	fmsr	s12, r3	@ int
	fuitos	s13, s15
	ldr	r3, .L5+24
	fuitos	s15, s12
	fdivs	s15, s13, s15
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r3, #0]	@ int
.L2:
	ldr	r3, .L5+12
	flds	s15, [sp, #4]
	ldr	r2, .L5+28
	flds	s13, [r3, #548]	@ int
	ldr	r1, [r3, #552]
	fuitos	s14, s13
	str	r1, [r2, #0]
	ldr	r1, [r3, #556]
	ldr	r2, .L5+32
	str	r1, [r2, #0]
	fdivs	s15, s14, s15
	ldr	r1, [r3, #560]
	ldr	r2, .L5+36
	str	r1, [r2, #0]
	ldr	r1, [r3, #536]
	ldr	r2, .L5+40
	cmn	r1, #1
	fmsrne	s12, r1	@ int
	ldr	r3, [r3, #540]
	moveq	r1, #0
	fuitosne	s14, s12
	fmsr	s13, r3	@ int
	ldr	r3, .L5+44
	fsts	s15, [r2, #0]
	fldsne	s15, [sp, #4]
	ldr	r2, .L5+48
	fdivsne	s15, s14, s15
	fuitos	s14, s13
	streq	r1, [r2, #0]	@ float
	fstsne	s15, [r2, #0]
	flds	s15, [sp, #4]
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	add	sp, sp, #8
	bx	lr
.L6:
	.align	2
.L5:
	.word	1120403456
	.word	1148846080
	.word	GuiVar_RadioTestInProgress
	.word	.LANCHOR0
	.word	my_tick_count
	.word	GuiVar_RadioTestStartTime
	.word	GuiVar_RadioTestSuccessfulPercent
	.word	GuiVar_RadioTestPacketsSent
	.word	GuiVar_RadioTestBadCRC
	.word	GuiVar_RadioTestNoResp
	.word	GuiVar_RadioTestRoundTripAvgMS
	.word	GuiVar_RadioTestRoundTripMaxMS
	.word	GuiVar_RadioTestRoundTripMinMS
.LFE9:
	.size	update_radio_test_gui_vars, .-update_radio_test_gui_vars
	.section	.text.FDTO_RADIO_TEST_draw_radio_comm_test_screen,"ax",%progbits
	.align	2
	.global	FDTO_RADIO_TEST_draw_radio_comm_test_screen
	.type	FDTO_RADIO_TEST_draw_radio_comm_test_screen, %function
FDTO_RADIO_TEST_draw_radio_comm_test_screen:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L11
	str	lr, [sp, #-4]!
.LCFI1:
	ldrnesh	r1, [r3, #0]
	bne	.L9
	ldr	r3, .L11+4
	ldr	r2, [r3, #0]
	ldr	r3, .L11+8
	subs	r2, r2, #256
	movne	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L11+12
	ldr	r1, [r3, #0]
	rsbs	r1, r1, #1
	movcc	r1, #0
.L9:
	mov	r0, #51
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L12:
	.align	2
.L11:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	GuiVar_RadioTestInProgress
	.word	GuiVar_RadioTestCommWithET2000e
.LFE20:
	.size	FDTO_RADIO_TEST_draw_radio_comm_test_screen, .-FDTO_RADIO_TEST_draw_radio_comm_test_screen
	.section	.text.gui_this_port_has_a_radio_test_device,"ax",%progbits
	.align	2
	.type	gui_this_port_has_a_radio_test_device, %function
gui_this_port_has_a_radio_test_device:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	ldr	r3, .L16
	bne	.L14
.LBB14:
	ldr	r0, [r3, #80]
	sub	r0, r0, #1
	cmp	r0, #2
	movhi	r0, #0
	movls	r0, #1
	bx	lr
.L14:
.LBE14:
	ldr	r0, [r3, #84]
	sub	r0, r0, #1
	cmp	r0, #2
	movhi	r0, #0
	movls	r0, #1
	bx	lr
.L17:
	.align	2
.L16:
	.word	config_c
.LFE12:
	.size	gui_this_port_has_a_radio_test_device, .-gui_this_port_has_a_radio_test_device
	.section	.text.gui_set_the_port_and_device_index_gui_vars,"ax",%progbits
	.align	2
	.type	gui_set_the_port_and_device_index_gui_vars, %function
gui_set_the_port_and_device_index_gui_vars:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L22
	cmp	r0, #1
	ldr	r1, .L22+4
	ldr	r3, .L22+8
.LBB17:
	ldreq	r2, [r2, #80]
.LBE17:
	ldrne	r2, [r2, #84]
	movne	r0, #2
.LBB18:
	streq	r0, [r1, #0]
.LBE18:
	strne	r0, [r1, #0]
	str	r2, [r3, #0]
	bx	lr
.L23:
	.align	2
.L22:
	.word	config_c
	.word	GuiVar_RadioTestPort
	.word	GuiVar_RadioTestDeviceIndex
.LFE14:
	.size	gui_set_the_port_and_device_index_gui_vars, .-gui_set_the_port_and_device_index_gui_vars
	.section	.text.RADIO_TEST_if_addressed_to_the_hub_pass_packet_on_to_the_inbound_transport,"ax",%progbits
	.align	2
	.global	RADIO_TEST_if_addressed_to_the_hub_pass_packet_on_to_the_inbound_transport
	.type	RADIO_TEST_if_addressed_to_the_hub_pass_packet_on_to_the_inbound_transport, %function
RADIO_TEST_if_addressed_to_the_hub_pass_packet_on_to_the_inbound_transport:
.LFB0:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI2:
	mov	r6, r0
	mov	r4, r2
	mov	r0, sp
	mov	r2, #12
	mov	r5, r1
	bl	memcpy
	ldrb	r3, [sp, #5]	@ zero_extendqisi2
	cmp	r3, #72
	bne	.L24
	ldrb	r3, [sp, #6]	@ zero_extendqisi2
	cmp	r3, #85
	bne	.L24
	ldrb	r3, [sp, #7]	@ zero_extendqisi2
	cmp	r3, #66
	bne	.L24
	mov	r0, r6
	mov	r1, r5
	mov	r2, r4
	bl	TPL_IN_make_a_copy_and_direct_incoming_packet
.L24:
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, pc}
.LFE0:
	.size	RADIO_TEST_if_addressed_to_the_hub_pass_packet_on_to_the_inbound_transport, .-RADIO_TEST_if_addressed_to_the_hub_pass_packet_on_to_the_inbound_transport
	.section	.text.RADIO_TEST_echo_inbound_2000e_test_message_back_to_where_it_came_from,"ax",%progbits
	.align	2
	.global	RADIO_TEST_echo_inbound_2000e_test_message_back_to_where_it_came_from
	.type	RADIO_TEST_echo_inbound_2000e_test_message_back_to_where_it_came_from, %function
RADIO_TEST_echo_inbound_2000e_test_message_back_to_where_it_came_from:
.LFB1:
	@ args = 8, pretend = 4, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	sp, sp, #4
.LCFI3:
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI4:
	sub	sp, sp, #48
.LCFI5:
	ldr	r6, [sp, #76]
	stmia	sp, {r0, r1}
	ldr	r1, .L28
	mov	r5, r2
	mov	r0, r6
	mov	r2, #182
	mov	r8, r3
	str	r3, [sp, #72]
	bl	mem_malloc_debug
	mov	r1, r8
	mov	r2, r6
	mov	r4, sp
	mov	r7, r0
	bl	memcpy
	ldr	r3, .L28+4
	add	r1, sp, #48
	strh	r3, [r1, #-2]!	@ movhi
	mov	r2, #2
	mov	r0, r7
	bl	memcpy
	mov	r3, #256
	strh	r3, [sp, #8]	@ movhi
	mov	r3, #2000
	str	r3, [sp, #36]
	mov	r3, #3
	ldmia	sp, {r0, r1}
	str	r3, [sp, #40]
	ldr	r3, .L28+8
	mov	r2, #0
	str	r0, [sp, #28]
	strh	r1, [sp, #32]	@ movhi
	ldr	r0, [r3, #0]
	add	r1, sp, #8
	mov	r3, r2
	str	r7, [sp, #16]
	str	r6, [sp, #20]
	str	r5, [sp, #24]
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L26
	ldr	r0, .L28
	bl	RemovePathFromFileName
	mov	r2, #216
	mov	r1, r0
	ldr	r0, .L28+12
	bl	Alert_Message_va
.L26:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	add	sp, sp, #4
	bx	lr
.L29:
	.align	2
.L28:
	.word	.LC0
	.word	-21755
	.word	TPL_OUT_event_queue
	.word	.LC1
.LFE1:
	.size	RADIO_TEST_echo_inbound_2000e_test_message_back_to_where_it_came_from, .-RADIO_TEST_echo_inbound_2000e_test_message_back_to_where_it_came_from
	.section	.text.RADIO_TEST_there_is_a_port_device_to_test,"ax",%progbits
	.align	2
	.global	RADIO_TEST_there_is_a_port_device_to_test
	.type	RADIO_TEST_there_is_a_port_device_to_test, %function
RADIO_TEST_there_is_a_port_device_to_test:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI6:
	mov	r0, #1
	bl	gui_this_port_has_a_radio_test_device
	cmp	r0, #0
	bne	.L32
	mov	r0, #2
	bl	gui_this_port_has_a_radio_test_device
	adds	r0, r0, #0
	movne	r0, #1
	ldr	pc, [sp], #4
.L32:
	mov	r0, #1
	ldr	pc, [sp], #4
.LFE13:
	.size	RADIO_TEST_there_is_a_port_device_to_test, .-RADIO_TEST_there_is_a_port_device_to_test
	.section	.text.init_radio_test_gui_vars_and_state_machine,"ax",%progbits
	.align	2
	.global	init_radio_test_gui_vars_and_state_machine
	.type	init_radio_test_gui_vars_and_state_machine, %function
init_radio_test_gui_vars_and_state_machine:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI7:
	ldr	r4, .L37
	mov	r1, #0
	mov	r2, #572
	mov	r0, r4
	bl	memset
	mov	r3, #256
	str	r3, [r4, #0]
	ldr	r3, .L37+4
	mov	r0, #0
	str	r3, [sp, #0]
	ldr	r1, .L37+8
	mov	r2, r0
	mov	r3, r0
	bl	xTimerCreate
	ldr	r3, .L37+12
	ldr	r1, .L37+8
	str	r3, [sp, #0]
	str	r0, [r4, #20]
	mov	r0, #0
	mov	r2, r0
	mov	r3, r0
	bl	xTimerCreate
	ldr	r3, .L37+16
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r3, .L37+20
	ldr	r2, .L37+24
	ldr	r1, .L37+28
	str	r2, [r3, #0]
	mov	r2, #2
	str	r0, [r4, #16]
	ldr	r0, .L37+32
	bl	strlcpy
	ldr	r1, .L37+28
	mov	r2, #2
	ldr	r0, .L37+36
	bl	strlcpy
	mov	r2, #2
	ldr	r1, .L37+40
	ldr	r0, .L37+44
	bl	strlcpy
.LBB19:
	mov	r0, #1
	bl	gui_this_port_has_a_radio_test_device
	cmp	r0, #0
	movne	r0, #1
	moveq	r0, #2
	bl	gui_set_the_port_and_device_index_gui_vars
.LBE19:
	mov	r0, #0
	bl	update_radio_test_gui_vars
	ldr	r3, .L37+48
	mov	r2, #0
	str	r2, [r3, #0]
	ldmfd	sp!, {r3, r4, pc}
.L38:
	.align	2
.L37:
	.word	.LANCHOR0
	.word	radio_test_waiting_for_response_timer_callback
	.word	6000
	.word	radio_test_message_rate_timer_callback
	.word	GuiVar_RadioTestCommWithET2000e
	.word	GuiVar_RadioTestCS3000SN
	.word	52000
	.word	.LC2
	.word	GuiVar_RadioTestET2000eAddr0
	.word	GuiVar_RadioTestET2000eAddr1
	.word	.LC3
	.word	GuiVar_RadioTestET2000eAddr2
	.word	GuiVar_RadioTestStartTime
.LFE16:
	.size	init_radio_test_gui_vars_and_state_machine, .-init_radio_test_gui_vars_and_state_machine
	.section	.text.RADIO_TEST_post_event_with_details,"ax",%progbits
	.align	2
	.global	RADIO_TEST_post_event_with_details
	.type	RADIO_TEST_post_event_with_details, %function
RADIO_TEST_post_event_with_details:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L41
	mov	r2, #0
	mov	r1, r0
	str	lr, [sp, #-4]!
.LCFI8:
	ldr	r0, [r3, #0]
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	ldreq	pc, [sp], #4
	ldr	r0, .L41+4
	ldr	lr, [sp], #4
	b	Alert_Message
.L42:
	.align	2
.L41:
	.word	RADIO_TEST_task_queue
	.word	.LC4
.LFE18:
	.size	RADIO_TEST_post_event_with_details, .-RADIO_TEST_post_event_with_details
	.section	.text.RADIO_TEST_post_event,"ax",%progbits
	.align	2
	.global	RADIO_TEST_post_event
	.type	RADIO_TEST_post_event, %function
RADIO_TEST_post_event:
.LFB19:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI9:
	add	r3, sp, #32
	str	r0, [r3, #-32]!
	mov	r0, sp
	bl	RADIO_TEST_post_event_with_details
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.LFE19:
	.size	RADIO_TEST_post_event, .-RADIO_TEST_post_event
	.global	__udivsi3
	.section	.text.process_radio_test_event,"ax",%progbits
	.align	2
	.type	process_radio_test_event, %function
process_radio_test_event:
.LFB10:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI10:
	ldr	r4, .L74
	mov	r5, r0
	ldr	r0, [r4, #0]
	sub	sp, sp, #60
.LCFI11:
	cmp	r0, #512
	beq	.L46
	cmp	r0, #768
	bne	.L68
	b	.L73
.L46:
	ldr	r1, [r5, #0]
	cmp	r1, #8192
	bne	.L48
	ldr	r6, [r4, #36]
	cmp	r6, #0
	beq	.L49
.LBB28:
.LBB29:
	ldr	r1, .L74+4
	ldr	r2, .L74+8
	mov	r0, #496
	bl	mem_malloc_debug
	ldr	r3, .L74+12
	mov	r5, #496
	mov	r6, r0
	ldr	r0, [r3, #0]
	bl	srand
.L50:
	bl	rand
	subs	r5, r5, #1
	strb	r0, [r6, #0]
	bne	.L50
	ldr	r4, .L74
	mov	r1, r6
	mov	r2, #496
	add	r0, r4, #40
	bl	memcpy
	ldr	r3, .L74+16
	add	r1, sp, #60
	strh	r3, [r1, #-2]!	@ movhi
	mov	r2, #2
	mov	r0, r6
	bl	memcpy
	ldrb	r3, [r4, #28]	@ zero_extendqisi2
	mov	r0, #256
	strh	r0, [sp, #4]	@ movhi
.LBE29:
	mov	r0, #496
	str	r0, [sp, #16]
.LBB30:
	ldr	r0, [r4, #32]
.LBE30:
	strb	r3, [sp, #29]
.LBB31:
	mov	r3, #2000
	str	r3, [sp, #32]
	mov	r3, #2
	ldrb	r1, [r4, #30]	@ zero_extendqisi2
	ldrb	r2, [r4, #29]	@ zero_extendqisi2
	str	r0, [sp, #20]
	str	r3, [sp, #36]
.LBE31:
	mov	r0, #66
.LBB32:
	ldr	r3, .L74+20
.LBE32:
	strb	r0, [sp, #24]
	mov	r0, #85
	strb	r0, [sp, #25]
	mov	r0, #72
	strb	r0, [sp, #26]
	strb	r1, [sp, #27]
	strb	r2, [sp, #28]
.LBB33:
	ldr	r0, [r3, #0]
	add	r1, sp, #4
	mov	r2, r5
	mov	r3, r5
.LBE33:
	str	r6, [sp, #12]
.LBB34:
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L51
	ldr	r0, .L74+4
	bl	RemovePathFromFileName
	ldr	r2, .L74+24
	mov	r1, r0
	ldr	r0, .L74+28
	bl	Alert_Message_va
	b	.L51
.L49:
.LBE34:
.LBE28:
.LBB35:
.LBB36:
	ldr	r1, .L74+4
	mov	r2, #376
	bl	mem_malloc_debug
	mov	r1, r6
	mov	r2, #12
	mov	r5, r0
	add	r0, sp, #40
	bl	memset
	ldrb	r3, [sp, #40]	@ zero_extendqisi2
	add	r1, sp, #40
	orr	r3, r3, #30
	strb	r3, [sp, #40]
	mov	r3, #16
	strb	r3, [sp, #41]
	ldr	r3, .L74+32
	mov	r0, r5
	ldrh	r2, [r3, #48]
	add	r6, r5, #12
	strh	r2, [sp, #46]	@ movhi
	ldrh	r3, [r3, #50]
	mov	r2, #12
	strh	r3, [sp, #48]	@ movhi
	ldrh	r3, [r4, #24]
	add	r7, r5, #496
	strh	r3, [sp, #42]	@ movhi
	ldrh	r3, [r4, #26]
	mov	r4, r5
	strh	r3, [sp, #44]	@ movhi
	bl	memcpy
	ldr	r3, .L74+12
	ldr	r0, [r3, #0]
	bl	srand
.L52:
	bl	rand
	strb	r0, [r4, #12]
	add	r4, r4, #1
	cmp	r4, r7
	bne	.L52
	ldr	r4, .L74
	mov	r2, #496
	mov	r1, r6
	add	r0, r4, #40
	bl	memcpy
	mov	r1, #508
	mov	r0, r5
	bl	CRC_calculate_32bit_big_endian
	add	r1, sp, #60
	mov	r2, #4
	str	r0, [r1, #-8]!
	add	r0, r5, #508
	bl	memcpy
	ldr	r0, [r4, #32]
	mov	r1, r5
	mov	r2, #512
	mov	r3, #0
	bl	attempt_to_copy_packet_and_route_out_another_port
	mov	r0, r5
	ldr	r1, .L74+4
	ldr	r2, .L74+36
	bl	mem_free_debug
.L51:
.LBE36:
.LBE35:
	ldr	r4, .L74
	mov	r1, #5
	ldr	r3, [r4, #552]
	ldr	r0, [r4, #12]
	add	r3, r3, #1
	str	r3, [r4, #552]
	ldr	r3, .L74+12
	ldr	r3, [r3, #0]
	str	r3, [r4, #8]
	mov	r3, #768
	str	r3, [r4, #0]
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, #0
	mov	r2, r0
	ldr	r0, [r4, #20]
	bl	xTimerGenericCommand
	mov	r0, #0
	b	.L53
.L48:
	ldr	r0, .L74+40
	bl	Alert_Message_va
	b	.L70
.L73:
	mvn	r3, #0
	mov	r2, #0
	str	r3, [sp, #0]
	mov	r1, #1
	ldr	r0, [r4, #20]
	mov	r3, r2
	bl	xTimerGenericCommand
	ldr	r1, [r5, #0]
	cmp	r1, #16384
	bne	.L54
	ldr	r3, [r4, #568]
	ldr	r1, [r5, #12]
	add	r3, r3, #1
	str	r3, [r4, #568]
	ldr	r3, [r4, #36]
	cmp	r3, #0
.LBB37:
	addne	r0, r4, #42
	addne	r1, r1, #2
	ldrne	r2, .L74+44
.LBE37:
.LBB38:
	addeq	r0, r4, #40
	addeq	r1, r1, #12
	moveq	r2, #496
	bl	memcmp
	cmp	r0, #0
.LBE38:
	ldrne	r3, [r4, #556]
	ldreq	r3, [r4, #564]
	addne	r3, r3, #1
	addeq	r3, r3, #1
	strne	r3, [r4, #556]
	streq	r3, [r4, #564]
	ldr	r4, .L74
	ldr	r3, .L74+12
	ldr	r0, [r4, #544]
	ldr	r2, [r3, #0]
	ldr	r3, [r4, #8]
	ldr	r1, [r4, #568]
	rsb	r3, r3, r2
	ldr	r2, [r4, #540]
	add	r3, r3, r3, asl #2
	cmp	r3, r2
	ldr	r2, [r4, #536]
	strhi	r3, [r4, #540]
	cmp	r3, r2
	ldrcc	r2, .L74
	add	r0, r3, r0
	strcc	r3, [r2, #536]
	str	r0, [r4, #544]
	bl	__udivsi3
	str	r0, [r4, #548]
	b	.L61
.L54:
	cmp	r1, #12288
	bne	.L62
	ldr	r0, .L74+48
	bl	Alert_Message
	ldr	r3, [r4, #560]
	add	r3, r3, #1
	str	r3, [r4, #560]
	b	.L61
.L62:
	cmp	r1, #13568
	bne	.L63
	ldr	r0, .L74+52
	bl	Alert_Message
	ldr	r3, [r4, #556]
	add	r3, r3, #1
	str	r3, [r4, #556]
	b	.L61
.L63:
	ldr	r0, .L74+56
	bl	Alert_Message_va
.L61:
	ldr	r4, .L74
	mov	r3, #512
	str	r3, [r4, #0]
	ldr	r3, [r5, #0]
	cmp	r3, #12288
	cmpne	r3, #13568
	bne	.L64
	mov	r0, #8192
	bl	RADIO_TEST_post_event
	b	.L70
.L64:
	ldr	r3, [r4, #548]
	mov	r1, #5
	cmp	r3, #4
	movls	r3, #5
	strls	r3, [r4, #548]
	ldr	r0, [r4, #548]
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, #0
	mov	r2, r0
	ldr	r0, [r4, #16]
	bl	xTimerGenericCommand
	b	.L70
.L68:
	ldr	r0, .L74+60
	bl	Alert_Message
.L70:
	mov	r0, #1
.L53:
	bl	update_radio_test_gui_vars
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L75:
	.align	2
.L74:
	.word	.LANCHOR0
	.word	.LC0
	.word	295
	.word	my_tick_count
	.word	-22011
	.word	TPL_OUT_event_queue
	.word	349
	.word	.LC1
	.word	config_c
	.word	427
	.word	.LC5
	.word	494
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
.LFE10:
	.size	process_radio_test_event, .-process_radio_test_event
	.section	.text.RADIO_TEST_task,"ax",%progbits
	.align	2
	.global	RADIO_TEST_task
	.type	RADIO_TEST_task, %function
RADIO_TEST_task:
.LFB17:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI12:
	ldr	r8, .L111
	ldr	sl, .L111+4
	ldr	r4, .L111+8
	rsb	r8, r8, r0
	sub	sp, sp, #40
.LCFI13:
	mov	r8, r8, lsr #5
.L100:
	ldr	r0, [sl, #0]
	add	r1, sp, #4
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	mov	r5, r0
	bne	.L77
	ldr	r3, [sp, #4]
	cmp	r3, #13568
	beq	.L81
	bhi	.L86
	cmp	r3, #8192
	beq	.L80
	cmp	r3, #12288
	beq	.L81
	cmp	r3, #4096
	bne	.L78
	b	.L108
.L86:
	cmp	r3, #20480
	beq	.L83
	bhi	.L87
	cmp	r3, #16384
	bne	.L78
	b	.L109
.L87:
	cmp	r3, #24576
	beq	.L84
	cmp	r3, #28672
	bne	.L78
	b	.L110
.L108:
	ldr	r6, [r4, #0]
	cmp	r6, #0
	bne	.L97
	ldr	r3, .L111+12
	ldr	r2, [r3, #0]
	cmp	r2, #256
	ldrne	r0, .L111+16
	bne	.L102
	ldr	r2, .L111+20
	ldr	r1, .L111+24
	ldr	r2, [r2, #0]
	ldr	r1, [r1, #0]
	str	r2, [r3, #4]
	ldr	r2, .L111+28
	str	r1, [r3, #32]
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	ldr	r1, .L111+32
	strb	r2, [r3, #28]
	ldr	r2, .L111+36
	ldr	r0, .L111+40
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	str	r6, [r3, #540]
	strb	r2, [r3, #29]
	ldr	r2, .L111+44
	str	r6, [r3, #544]
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	str	r6, [r3, #548]
	strb	r2, [r3, #30]
	ldr	r2, .L111+48
	str	r6, [r3, #552]
	ldr	r2, [r2, #0]
	str	r6, [r3, #556]
	str	r2, [r3, #24]
	ldr	r2, .L111+52
	str	r6, [r3, #560]
	ldr	r2, [r2, #0]
	str	r6, [r3, #564]
	cmp	r2, #0
	str	r2, [r3, #36]
	moveq	r2, r0
	movne	r2, r1
	str	r2, [r3, #12]
	mvn	r2, #0
	str	r2, [r3, #536]
	mov	r2, #512
	str	r6, [r3, #568]
	str	r2, [r3, #0]
	mov	r0, #8192
	bl	RADIO_TEST_post_event
	mov	r0, r5
	bl	update_radio_test_gui_vars
	mov	r0, r6
	b	.L106
.L80:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L97
	ldr	r3, .L111+12
	ldr	r3, [r3, #0]
	cmp	r3, #256
	bne	.L104
.L92:
	ldr	r0, .L111+56
	b	.L107
.L109:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L93
	ldr	r3, .L111+12
	ldr	r3, [r3, #0]
	cmp	r3, #256
	beq	.L94
	add	r0, sp, #4
	bl	process_radio_test_event
	b	.L95
.L94:
	ldr	r0, .L111+60
	bl	Alert_Message
.L93:
	mov	r0, #20480
	bl	RADIO_TEST_post_event
.L95:
	ldr	r0, [sp, #16]
	ldr	r1, .L111+64
	ldr	r2, .L111+68
	b	.L103
.L110:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L96
	ldr	r5, [sp, #16]
	ldr	r6, [sp, #24]
.LBB39:
.LBB40:
	ldrb	r1, [r5, #3]	@ zero_extendqisi2
	ldrb	r3, [r5, #2]	@ zero_extendqisi2
.LBE40:
.LBE39:
	ldr	r2, [sp, #20]
.LBB42:
.LBB41:
	orr	r3, r3, r1, asl #8
	ldrb	r1, [r5, #4]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #16
	ldrb	r1, [r5, #5]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #24
	ldr	r1, .L111+72
	ldr	r1, [r1, #48]
	cmp	r3, r1
	bne	.L96
	cmp	r2, #512
	bne	.L96
	mov	r2, #17
	ldrb	r1, [r5, #7]	@ zero_extendqisi2
	strb	r2, [r5, #1]
	ldrb	r2, [r5, #6]	@ zero_extendqisi2
	strb	r3, [r5, #6]
	orr	r2, r2, r1, asl #8
	ldrb	r1, [r5, #8]	@ zero_extendqisi2
	mov	r0, r5
	orr	r2, r2, r1, asl #16
	ldrb	r1, [r5, #9]	@ zero_extendqisi2
	orr	r2, r2, r1, asl #24
	mov	r1, r2, lsr #8
	strb	r2, [r5, #2]
	strb	r1, [r5, #3]
	mov	r1, r2, lsr #16
	mov	r2, r2, lsr #24
	strb	r2, [r5, #5]
	mov	r2, r3, lsr #8
	strb	r2, [r5, #7]
	mov	r2, r3, lsr #16
	mov	r3, r3, lsr #24
	strb	r3, [r5, #9]
	strb	r1, [r5, #4]
	strb	r2, [r5, #8]
	mov	r1, #508
	bl	CRC_calculate_32bit_big_endian
	add	r1, sp, #40
	mov	r2, #4
	str	r0, [r1, #-4]!
	add	r0, r5, #508
	bl	memcpy
	mov	r0, r6
	mov	r1, r5
	mov	r2, #512
	mov	r3, #0
	bl	attempt_to_copy_packet_and_route_out_another_port
.L96:
.LBE41:
.LBE42:
	ldr	r0, [sp, #16]
	ldr	r1, .L111+64
	ldr	r2, .L111+76
.L103:
	bl	mem_free_debug
	b	.L77
.L81:
	ldr	r2, [r4, #0]
	cmp	r2, #0
	bne	.L97
	ldr	r2, .L111+12
	ldr	r2, [r2, #0]
	cmp	r2, #256
	beq	.L98
.L104:
	add	r0, sp, #4
	bl	process_radio_test_event
	b	.L77
.L98:
	cmp	r3, #13568
	beq	.L97
	ldr	r0, .L111+80
.L107:
	bl	Alert_Message
.L97:
	mov	r0, #20480
	bl	RADIO_TEST_post_event
	b	.L77
.L83:
.LBB43:
	ldr	r6, .L111+12
	mov	r2, #0
	mvn	r7, #0
	mov	r3, r2
	str	r7, [sp, #0]
	mov	r1, r5
	ldr	r0, [r6, #20]
	bl	xTimerGenericCommand
	mov	r2, #0
	str	r7, [sp, #0]
	mov	r3, r2
	ldr	r0, [r6, #16]
	mov	r1, r5
	bl	xTimerGenericCommand
	mov	r3, #256
	str	r3, [r6, #0]
.LBE43:
	ldr	r3, .L111+84
	ldrsh	r3, [r3, #0]
	cmp	r3, #51
	bne	.L77
	b	.L105
.L84:
	ldr	r3, .L111+12
	ldr	r3, [r3, #0]
	cmp	r3, #256
	beq	.L77
.L105:
	mov	r0, #0
	bl	update_radio_test_gui_vars
	mov	r0, #0
.L106:
	bl	Redraw_Screen
	b	.L77
.L78:
	ldr	r0, .L111+88
.L102:
	bl	Alert_Message
.L77:
	bl	xTaskGetTickCount
	ldr	r3, .L111+92
	str	r0, [r3, r8, asl #2]
	b	.L100
.L112:
	.align	2
.L111:
	.word	Task_Table
	.word	RADIO_TEST_task_queue
	.word	in_device_exchange_hammer
	.word	.LANCHOR0
	.word	.LC10
	.word	my_tick_count
	.word	GuiVar_RadioTestPort
	.word	GuiVar_RadioTestET2000eAddr0
	.word	20000
	.word	GuiVar_RadioTestET2000eAddr1
	.word	10000
	.word	GuiVar_RadioTestET2000eAddr2
	.word	GuiVar_RadioTestCS3000SN
	.word	GuiVar_RadioTestCommWithET2000e
	.word	.LC11
	.word	.LC12
	.word	.LC0
	.word	1032
	.word	config_c
	.word	1050
	.word	.LC13
	.word	GuiLib_CurStructureNdx
	.word	.LC14
	.word	task_last_execution_stamp
.LFE17:
	.size	RADIO_TEST_task, .-RADIO_TEST_task
	.section	.text.radio_test_message_rate_timer_callback,"ax",%progbits
	.align	2
	.type	radio_test_message_rate_timer_callback, %function
radio_test_message_rate_timer_callback:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #8192
	b	RADIO_TEST_post_event
.LFE2:
	.size	radio_test_message_rate_timer_callback, .-radio_test_message_rate_timer_callback
	.section	.text.radio_test_waiting_for_response_timer_callback,"ax",%progbits
	.align	2
	.type	radio_test_waiting_for_response_timer_callback, %function
radio_test_waiting_for_response_timer_callback:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #12288
	b	RADIO_TEST_post_event
.LFE3:
	.size	radio_test_waiting_for_response_timer_callback, .-radio_test_waiting_for_response_timer_callback
	.section	.text.RADIO_TEST_process_radio_comm_test_screen,"ax",%progbits
	.align	2
	.global	RADIO_TEST_process_radio_comm_test_screen
	.type	RADIO_TEST_process_radio_comm_test_screen, %function
RADIO_TEST_process_radio_comm_test_screen:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L171
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI14:
	ldrsh	r2, [r2, #0]
	mov	r4, r0
	cmp	r2, #616
	mov	r3, r1
	bne	.L158
	ldr	r2, .L171+4
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	NUMERIC_KEYPAD_process_key
.L158:
	cmp	r0, #3
	beq	.L122
	bhi	.L125
	cmp	r0, #1
	ldr	r3, .L171+8
	beq	.L120
	bhi	.L121
	b	.L169
.L125:
	cmp	r0, #80
	beq	.L124
	cmp	r0, #84
	beq	.L124
	cmp	r0, #4
	bne	.L160
	b	.L170
.L121:
	ldrsh	r3, [r3, #0]
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L140
.L131:
	.word	.L127
	.word	.L128
	.word	.L129
	.word	.L140
	.word	.L140
	.word	.L140
	.word	.L140
	.word	.L130
.L127:
	ldr	r4, .L171+12
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L134
	bl	good_key_beep
	mov	r3, #1
	b	.L167
.L128:
	ldr	r4, .L171+12
	ldr	r3, [r4, #0]
	cmp	r3, #1
	bne	.L134
	bl	good_key_beep
	mov	r3, #0
.L167:
	str	r3, [r4, #0]
	b	.L135
.L134:
	bl	bad_key_beep
.L135:
	mov	r0, #0
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	Redraw_Screen
.L129:
	bl	good_key_beep
	ldr	r0, .L171+16
	ldr	r1, .L171+20
	ldr	r2, .L171+24
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	NUMERIC_KEYPAD_draw_uint32_keypad
.L130:
	ldr	r3, .L171+28
	ldr	r3, [r3, #0]
	cmp	r3, #256
	beq	.L136
	bl	good_key_beep
	mov	r0, #20480
	b	.L163
.L136:
	ldr	r3, .L171+32
	ldr	r3, [r3, #0]
	cmp	r3, #1
	ldreq	r3, .L171+36
	ldreq	r3, [r3, #80]
	beq	.L162
	cmp	r3, #2
	bne	.L139
	ldr	r3, .L171+36
	ldr	r3, [r3, #84]
.L162:
	cmp	r3, #0
	beq	.L140
.L139:
	bl	good_key_beep
	mov	r0, #4096
.L163:
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	RADIO_TEST_post_event
.L124:
	ldr	r3, .L171+8
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #3
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L140
.L145:
	.word	.L141
	.word	.L142
	.word	.L143
	.word	.L144
.L141:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L171+40
	b	.L164
.L142:
	ldr	r1, .L171+44
	mov	r3, #1
	mov	r0, r4
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L164:
	mov	r2, #33
	mov	r3, #126
	bl	process_char
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	Refresh_Screen
.L143:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L171+48
	b	.L164
.L144:
	ldr	r3, .L171+32
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L146
	mov	r0, #2
	bl	gui_this_port_has_a_radio_test_device
	cmp	r0, #0
	beq	.L140
	bl	good_key_beep
	mov	r0, #2
	b	.L166
.L146:
	mov	r0, #1
	bl	gui_this_port_has_a_radio_test_device
	cmp	r0, #0
	beq	.L140
	bl	good_key_beep
	mov	r0, #1
.L166:
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	gui_set_the_port_and_device_index_gui_vars
.L140:
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	bad_key_beep
.L170:
	ldr	r3, .L171+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #3
	blt	.L154
	cmp	r3, #5
	movle	r0, #1
	movle	r1, r0
	ble	.L165
	cmp	r3, #6
	bne	.L154
	ldr	r3, .L171+12
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r0, #3
	bne	.L154
	b	.L168
.L169:
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #3
	cmp	r3, #2
	bhi	.L122
	mov	r0, #6
.L168:
	mov	r1, #1
	b	.L165
.L120:
	ldrsh	r3, [r3, #0]
	cmp	r3, #6
	bne	.L154
	ldr	r3, .L171+12
	ldr	r1, [r3, #0]
	cmp	r1, #1
	bne	.L154
	mov	r0, #3
.L165:
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Select
.L154:
	mov	r0, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Up
.L122:
	mov	r0, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Down
.L160:
	cmp	r0, #67
	ldreq	r2, .L171+52
	moveq	r1, #11
	streq	r1, [r2, #0]
	mov	r1, r3
	bl	KEY_process_global_keys
	cmp	r4, #67
	bne	.L115
	mov	r0, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	COMM_OPTIONS_draw_dialog
.L115:
	add	sp, sp, #8
	ldmfd	sp!, {r4, pc}
.L172:
	.align	2
.L171:
	.word	GuiLib_CurStructureNdx
	.word	FDTO_RADIO_TEST_draw_radio_comm_test_screen
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_RadioTestCommWithET2000e
	.word	GuiVar_RadioTestCS3000SN
	.word	50000
	.word	99999
	.word	.LANCHOR0
	.word	GuiVar_RadioTestPort
	.word	config_c
	.word	GuiVar_RadioTestET2000eAddr0
	.word	GuiVar_RadioTestET2000eAddr1
	.word	GuiVar_RadioTestET2000eAddr2
	.word	GuiVar_MenuScreenToShow
.LFE21:
	.size	RADIO_TEST_process_radio_comm_test_screen, .-RADIO_TEST_process_radio_comm_test_screen
	.global	rtcs
	.section	.bss.rtcs,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	rtcs, %object
	.size	rtcs, 572
rtcs:
	.space	572
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/radio_test.c\000"
.LC1:
	.ascii	"TPL_OUT queue full : %s, %u\000"
.LC2:
	.ascii	"!\000"
.LC3:
	.ascii	"A\000"
.LC4:
	.ascii	"Radio Test queue overflow!\000"
.LC5:
	.ascii	"LINK TEST: waiting to build - unexp event %u\000"
.LC6:
	.ascii	"RADIO TEST: waiting for response - timeout\000"
.LC7:
	.ascii	"RADIO TEST: waiting for response - bad crc\000"
.LC8:
	.ascii	"RADIO TEST: waiting for response - unexp event %u\000"
.LC9:
	.ascii	"RADIO TEST: unhandled STATE\000"
.LC10:
	.ascii	"RADIO TEST: request to start when not idle\000"
.LC11:
	.ascii	"RADIO TEST unexp build message event\000"
.LC12:
	.ascii	"RADIO TEST unexp inbound message event\000"
.LC13:
	.ascii	"RADIO TEST unexp response timeout event\000"
.LC14:
	.ascii	"unhandled link test event\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI0-.LFB9
	.byte	0xe
	.uleb128 0x8
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI1-.LFB20
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI2-.LFB0
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI6-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI7-.LFB16
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI8-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI9-.LFB19
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI10-.LFB10
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI12-.LFB17
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI14-.LFB21
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/radio_test.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x195
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF22
	.byte	0x1
	.4byte	.LASF23
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x2f9
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x313
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x324
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0xf2
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1de
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1af
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x2e7
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x1f7
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x4cb
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST1
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB12
	.4byte	.LFE12
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	0x2a
	.4byte	.LFB14
	.4byte	.LFE14
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.byte	0x8e
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.byte	0xa6
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x30a
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST4
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x334
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x479
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST6
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x487
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST7
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x10f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x162
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x22c
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST8
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x365
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST9
	.uleb128 0x8
	.4byte	.LASF19
	.byte	0x1
	.byte	0xde
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.4byte	.LASF20
	.byte	0x1
	.byte	0xe8
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x4f1
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST10
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB9
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB20
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB0
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI5
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB13
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB16
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB18
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB19
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB10
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI11
	.4byte	.LFE10
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB17
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI13
	.4byte	.LFE17
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB21
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF20:
	.ascii	"radio_test_waiting_for_response_timer_callback\000"
.LASF12:
	.ascii	"RADIO_TEST_post_event_with_details\000"
.LASF11:
	.ascii	"init_radio_test_gui_vars_and_state_machine\000"
.LASF18:
	.ascii	"RADIO_TEST_task\000"
.LASF5:
	.ascii	"if_addressed_to_us_echo_this_3000_packet\000"
.LASF3:
	.ascii	"the_message_echoed_from_the_2000e_matched\000"
.LASF2:
	.ascii	"gui_find_the_first_port_to_test_and_set_the_port_an"
	.ascii	"d_device_index_gui_vars\000"
.LASF19:
	.ascii	"radio_test_message_rate_timer_callback\000"
.LASF1:
	.ascii	"gui_set_the_port_and_device_index_gui_vars\000"
.LASF22:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF16:
	.ascii	"update_radio_test_gui_vars\000"
.LASF17:
	.ascii	"process_radio_test_event\000"
.LASF15:
	.ascii	"build_and_send_the_next_3000_test_message\000"
.LASF8:
	.ascii	"RADIO_TEST_if_addressed_to_the_hub_pass_packet_on_t"
	.ascii	"o_the_inbound_transport\000"
.LASF13:
	.ascii	"RADIO_TEST_post_event\000"
.LASF9:
	.ascii	"RADIO_TEST_echo_inbound_2000e_test_message_back_to_"
	.ascii	"where_it_came_from\000"
.LASF6:
	.ascii	"radio_test_stop_and_cleanup\000"
.LASF23:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/radio_test.c\000"
.LASF21:
	.ascii	"RADIO_TEST_process_radio_comm_test_screen\000"
.LASF0:
	.ascii	"gui_this_port_has_a_radio_test_device\000"
.LASF10:
	.ascii	"RADIO_TEST_there_is_a_port_device_to_test\000"
.LASF14:
	.ascii	"build_and_send_the_next_2000e_test_message\000"
.LASF7:
	.ascii	"FDTO_RADIO_TEST_draw_radio_comm_test_screen\000"
.LASF4:
	.ascii	"the_message_echoed_from_the_3000_matched\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
