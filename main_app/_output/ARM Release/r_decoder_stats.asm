	.file	"r_decoder_stats.c"
	.text
.Ltext0:
	.section	.text.TWO_WIRE_copy_decoder_stats_into_guivars,"ax",%progbits
	.align	2
	.type	TWO_WIRE_copy_decoder_stats_into_guivars, %function
TWO_WIRE_copy_decoder_stats_into_guivars:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L2
	mov	r2, #60
	mla	r3, r2, r0, r3
	ldr	r2, .L2+4
	ldr	r1, [r3, #220]
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldrb	r0, [r3, #234]	@ zero_extendqisi2
	str	r1, [r2, #0]
	ldr	r1, .L2+8
	ldrb	ip, [r3, #237]	@ zero_extendqisi2
	str	r0, [r1, #0]
	ldr	r0, .L2+12
	ldrb	r4, [r3, #240]	@ zero_extendqisi2
	str	ip, [r0, #0]
	ldr	ip, .L2+16
	ldr	r1, .L2+20
	str	r4, [ip, #0]
	ldrb	r4, [r3, #238]	@ zero_extendqisi2
	ldr	ip, .L2+24
	ldrb	r5, [r3, #246]	@ zero_extendqisi2
	str	r4, [ip, #0]
	ldrb	ip, [r3, #239]	@ zero_extendqisi2
	ldrb	r4, [r3, #249]	@ zero_extendqisi2
	str	ip, [r1, #0]
	ldrb	ip, [r3, #243]	@ zero_extendqisi2
	ldr	r1, .L2+28
	ldr	r0, .L2+32
	str	ip, [r1, #0]
	ldr	ip, .L2+36
	ldr	r1, .L2+40
	str	r4, [ip, #0]
	ldrb	r4, [r3, #248]	@ zero_extendqisi2
	ldr	ip, .L2+44
	ldr	r2, .L2+48
	str	r4, [ip, #0]
	ldr	r4, .L2+52
	ldr	ip, .L2+56
	str	r5, [r4, #0]
	ldrb	r5, [r3, #244]	@ zero_extendqisi2
	ldr	r4, .L2+60
	str	r5, [r4, #0]
	ldrb	r5, [r3, #245]	@ zero_extendqisi2
	ldr	r4, .L2+64
	str	r5, [r4, #0]
	ldrb	r5, [r3, #251]	@ zero_extendqisi2
	ldr	r4, .L2+68
	str	r5, [r4, #0]
	ldrb	r4, [r3, #247]	@ zero_extendqisi2
	str	r4, [ip, #0]
	ldrb	r4, [r3, #242]	@ zero_extendqisi2
	ldr	ip, .L2+72
	str	r4, [ip, #0]
	ldrb	ip, [r3, #241]	@ zero_extendqisi2
	str	ip, [r0, #0]
	ldrb	r0, [r3, #250]	@ zero_extendqisi2
	str	r0, [r1, #0]
	ldrb	ip, [r3, #255]	@ zero_extendqisi2
	ldr	r0, .L2+76
	ldr	r1, .L2+80
	str	ip, [r0, #0]
	ldrb	ip, [r3, #252]	@ zero_extendqisi2
	ldr	r0, .L2+84
	str	ip, [r0, #0]
	ldrb	ip, [r3, #253]	@ zero_extendqisi2
	ldr	r0, .L2+88
	str	ip, [r0, #0]
	ldrb	r0, [r3, #254]	@ zero_extendqisi2
	str	r0, [r1, #0]
	ldrb	r0, [r3, #235]	@ zero_extendqisi2
	ldr	r1, .L2+92
	str	r0, [r1, #0]
	ldrb	r0, [r3, #236]	@ zero_extendqisi2
	ldr	r1, .L2+96
	str	r0, [r1, #0]
	ldrb	r0, [r3, #256]	@ zero_extendqisi2
	ldr	r1, .L2+100
	str	r0, [r1, #0]
	ldrb	r1, [r3, #233]	@ zero_extendqisi2
	str	r1, [r2, #0]
	ldrb	r1, [r3, #232]	@ zero_extendqisi2
	ldr	r2, .L2+104
	str	r1, [r2, #0]
	ldrh	r1, [r3, #228]
	ldr	r2, .L2+108
	str	r1, [r2, #0]
	ldrh	r2, [r3, #230]
	ldr	r3, .L2+112
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, pc}
.L3:
	.align	2
.L2:
	.word	tpmicro_data
	.word	GuiVar_TwoWireDecoderSN
	.word	GuiVar_TwoWireStatsBODs
	.word	GuiVar_TwoWireStatsEEPCRCComParams
	.word	GuiVar_TwoWireStatsEEPCRCErrStats
	.word	GuiVar_TwoWireStatsEEPCRCS2Params
	.word	GuiVar_TwoWireStatsEEPCRCS1Params
	.word	GuiVar_TwoWireStatsRxCRCErrs
	.word	GuiVar_TwoWireStatsRxMsgs
	.word	GuiVar_TwoWireStatsRxDataLpbkMsgs
	.word	GuiVar_TwoWireStatsRxPutParamsMsgs
	.word	GuiVar_TwoWireStatsRxDecRstMsgs
	.word	GuiVar_TwoWireStatsWDTs
	.word	GuiVar_TwoWireStatsRxDiscConfMsgs
	.word	GuiVar_TwoWireStatsRxIDReqMsgs
	.word	GuiVar_TwoWireStatsRxDiscRstMsgs
	.word	GuiVar_TwoWireStatsRxEnqMsgs
	.word	GuiVar_TwoWireStatsRxGetParamsMsgs
	.word	GuiVar_TwoWireStatsRxLongMsgs
	.word	GuiVar_TwoWireStatsRxSolCtrlMsgs
	.word	GuiVar_TwoWireStatsRxStatsReqMsgs
	.word	GuiVar_TwoWireStatsRxSolCurMeasReqMsgs
	.word	GuiVar_TwoWireStatsRxStatReqMsgs
	.word	GuiVar_TwoWireStatsS1UCos
	.word	GuiVar_TwoWireStatsS2UCos
	.word	GuiVar_TwoWireStatsTxAcksSent
	.word	GuiVar_TwoWireStatsPORs
	.word	GuiVar_TwoWireStatsTempMax
	.word	GuiVar_TwoWireStatsTempCur
.LFE0:
	.size	TWO_WIRE_copy_decoder_stats_into_guivars, .-TWO_WIRE_copy_decoder_stats_into_guivars
	.section	.text.FDTO_TWO_WIRE_update_decoder_stats,"ax",%progbits
	.align	2
	.global	FDTO_TWO_WIRE_update_decoder_stats
	.type	FDTO_TWO_WIRE_update_decoder_stats, %function
FDTO_TWO_WIRE_update_decoder_stats:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L5
	str	lr, [sp, #-4]!
.LCFI1:
	ldr	r0, [r3, #0]
	bl	TWO_WIRE_copy_decoder_stats_into_guivars
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L6:
	.align	2
.L5:
	.word	.LANCHOR0
.LFE1:
	.size	FDTO_TWO_WIRE_update_decoder_stats, .-FDTO_TWO_WIRE_update_decoder_stats
	.section	.text.FDTO_TWO_WIRE_draw_decoder_stats_report,"ax",%progbits
	.align	2
	.global	FDTO_TWO_WIRE_draw_decoder_stats_report
	.type	FDTO_TWO_WIRE_draw_decoder_stats_report, %function
FDTO_TWO_WIRE_draw_decoder_stats_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L13
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldrnesh	r1, [r3, #0]
	bne	.L10
	ldr	r3, .L13+4
	mov	r4, #0
	str	r4, [r3, #0]
	mov	r0, r4
	bl	TWO_WIRE_copy_decoder_stats_into_guivars
	ldr	r3, .L13+8
	ldr	r2, .L13+12
	add	r0, r3, #4800
.L11:
	ldr	r1, [r3, #220]
	cmp	r1, #0
	streq	r4, [r2, #0]
	beq	.L10
.L9:
	add	r3, r3, #60
	cmp	r3, r0
	add	r4, r4, #1
	bne	.L11
	mov	r1, #0
	str	r4, [r2, #0]
.L10:
	mov	r1, r1, asl #16
	mov	r0, #76
	mov	r1, r1, asr #16
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, lr}
	b	GuiLib_Refresh
.L14:
	.align	2
.L13:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	tpmicro_data
	.word	GuiVar_TwoWireNumDiscoveredStaDecoders
.LFE2:
	.size	FDTO_TWO_WIRE_draw_decoder_stats_report, .-FDTO_TWO_WIRE_draw_decoder_stats_report
	.section	.text.TWO_WIRE_process_decoder_stats_report,"ax",%progbits
	.align	2
	.global	TWO_WIRE_process_decoder_stats_report
	.type	TWO_WIRE_process_decoder_stats_report, %function
TWO_WIRE_process_decoder_stats_report:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #3
	str	lr, [sp, #-4]!
.LCFI3:
	mov	r3, r0
	mov	r2, r1
	beq	.L19
	bhi	.L22
	cmp	r0, #1
	beq	.L17
	cmp	r0, #2
	bne	.L16
	b	.L31
.L22:
	cmp	r0, #20
	beq	.L20
	cmp	r0, #67
	beq	.L21
	cmp	r0, #16
	bne	.L16
	b	.L20
.L31:
	ldr	r3, .L33
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L24
	cmp	r3, #1
	bne	.L28
	b	.L32
.L24:
	ldr	lr, [sp], #4
	b	TWO_WIRE_request_statistics_from_all_decoders_from_ui
.L32:
	ldr	lr, [sp], #4
	b	TWO_WIRE_clear_statistics_at_all_decoders_from_ui
.L20:
	ldr	r1, .L33+4
	cmp	r3, #20
	ldr	r2, [r1, #0]
	mov	r3, r1
	bne	.L26
	ldr	r1, .L33+8
	ldr	r1, [r1, #0]
	sub	r1, r1, #1
	cmp	r2, r1
	addcc	r1, r2, #1
	bcc	.L30
	b	.L27
.L26:
	cmp	r2, #0
	beq	.L27
	sub	r1, r2, #1
.L30:
	str	r1, [r3, #0]
.L27:
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L28
	bl	good_key_beep
	ldr	r3, .L33+4
	ldr	r0, [r3, #0]
	bl	TWO_WIRE_copy_decoder_stats_into_guivars
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L28:
	ldr	lr, [sp], #4
	b	bad_key_beep
.L17:
	ldr	lr, [sp], #4
	b	CURSOR_Up
.L19:
	mov	r0, #1
	ldr	lr, [sp], #4
	b	CURSOR_Down
.L21:
	ldr	r3, .L33+12
	mov	r2, #11
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	ldr	lr, [sp], #4
	b	TWO_WIRE_DEBUG_draw_dialog
.L16:
	mov	r0, r3
	mov	r1, r2
	ldr	lr, [sp], #4
	b	KEY_process_global_keys
.L34:
	.align	2
.L33:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	GuiVar_TwoWireNumDiscoveredStaDecoders
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	TWO_WIRE_process_decoder_stats_report, .-TWO_WIRE_process_decoder_stats_report
	.section	.bss.g_TWO_WIRE_list_item_index,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_TWO_WIRE_list_item_index, %object
	.size	g_TWO_WIRE_list_item_index, 4
g_TWO_WIRE_list_item_index:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_decoder_stats.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x35
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x65
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x7c
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xb2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"FDTO_TWO_WIRE_update_decoder_stats\000"
.LASF5:
	.ascii	"TWO_WIRE_copy_decoder_stats_into_guivars\000"
.LASF1:
	.ascii	"FDTO_TWO_WIRE_draw_decoder_stats_report\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_decoder_stats.c\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"TWO_WIRE_process_decoder_stats_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
