	.file	"d_two_wire_assignment.c"
	.text
.Ltext0:
	.section	.text.FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog,"ax",%progbits
	.align	2
	.global	FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog
	.type	FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog, %function
FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L9
	mov	r3, #0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI0:
	ldr	r6, .L9+4
	str	r3, [r2, #0]
	ldr	r5, .L9+8
	ldr	r4, .L9+12
	ldr	r2, .L9+16
	str	r3, [r6, #0]
	str	r3, [r2, #0]
	str	r3, [r5, #0]
	str	r3, [r4, #0]
	ldr	r3, .L9+20
	mov	r1, #400
	ldr	r2, .L9+24
	mov	r7, r0
	ldr	r0, [r3, #0]
	mov	r3, #77
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L9+28
	ldr	r1, [r6, #0]
	ldr	ip, [r5, #0]
	ldr	r2, [r4, #0]
	add	lr, r3, #4800
.L3:
	ldr	r0, [r3, #220]
	cmp	r0, #0
	beq	.L2
	ldrb	r0, [r3, #224]	@ zero_extendqisi2
	cmp	r0, #2
	moveq	r1, #1
	beq	.L2
	cmp	r0, #8
	moveq	ip, #1
	beq	.L2
	cmp	r0, #3
	moveq	r2, #1
	moveq	r1, r2
.L2:
	add	r3, r3, #60
	cmp	r3, lr
	bne	.L3
	ldr	r3, .L9+4
	str	r1, [r3, #0]
	ldr	r3, .L9+8
	str	ip, [r3, #0]
	ldr	r3, .L9+12
	str	r2, [r3, #0]
	ldr	r3, .L9+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	ORPHAN_TWO_WIRE_STATIONS_populate_list
	ldr	r3, .L9
	adds	r0, r0, #0
	movne	r0, #1
	str	r0, [r3, #0]
	bl	ORPHAN_TWO_WIRE_POCS_populate_list
	ldr	r3, .L9+16
	adds	r0, r0, #0
	movne	r0, #1
	str	r0, [r3, #0]
	ldr	r3, .L9+32
	cmp	r7, #1
	ldrsh	r1, [r3, #0]
	ldr	r3, .L9+36
	str	r1, [r3, #0]
	bne	.L4
	ldr	r3, .L9+40
	ldr	r2, [r3, #0]
	cmp	r2, #0
	moveq	r2, #50
	streq	r2, [r3, #0]
	ldr	r1, [r3, #0]
.L4:
	ldr	r3, .L9+44
	mov	r2, #1
	mov	r1, r1, asl #16
	str	r2, [r3, #0]
	ldr	r0, .L9+48
	mov	r1, r1, asr #16
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	GuiLib_Refresh
.L10:
	.align	2
.L9:
	.word	GuiVar_TwoWireOrphanedStationsExist
	.word	GuiVar_TwoWireDiscoveredStaDecoders
	.word	GuiVar_TwoWireDiscoveredPOCDecoders
	.word	GuiVar_TwoWireDiscoveredMoisDecoders
	.word	GuiVar_TwoWireOrphanedPOCsExist
	.word	tpmicro_data_recursive_MUTEX
	.word	.LC0
	.word	tpmicro_data
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	639
.LFE0:
	.size	FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog, .-FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog
	.section	.text.TWO_WIRE_ASSIGNMENT_draw_dialog,"ax",%progbits
	.align	2
	.global	TWO_WIRE_ASSIGNMENT_draw_dialog
	.type	TWO_WIRE_ASSIGNMENT_draw_dialog, %function
TWO_WIRE_ASSIGNMENT_draw_dialog:
.LFB1:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI1:
	mov	r3, #2
	sub	sp, sp, #36
.LCFI2:
	str	r3, [sp, #0]
	ldr	r3, .L12
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L13:
	.align	2
.L12:
	.word	FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog
.LFE1:
	.size	TWO_WIRE_ASSIGNMENT_draw_dialog, .-TWO_WIRE_ASSIGNMENT_draw_dialog
	.section	.text.TWO_WIRE_ASSIGNMENT_process_dialog,"ax",%progbits
	.align	2
	.global	TWO_WIRE_ASSIGNMENT_process_dialog
	.type	TWO_WIRE_ASSIGNMENT_process_dialog, %function
TWO_WIRE_ASSIGNMENT_process_dialog:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	sub	sp, sp, #36
.LCFI4:
	beq	.L17
	bhi	.L20
	cmp	r0, #0
	beq	.L16
	b	.L15
.L20:
	cmp	r0, #4
	beq	.L18
	cmp	r0, #67
	bne	.L15
	b	.L33
.L17:
	ldr	r4, .L34
	ldrsh	r3, [r4, #0]
	sub	r3, r3, #50
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L21
.L28:
	.word	.L22
	.word	.L23
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
.L22:
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	beq	.L29
	ldrsh	r2, [r4, #0]
	ldr	r3, .L34+4
	str	r2, [r3, #0]
	ldr	r3, .L34+8
	mov	r2, #0
	str	r2, [r3, #0]
	bl	DIALOG_close_ok_dialog
	mov	r0, #1
	mov	r1, r0
	bl	TWO_WIRE_perform_discovery_process
	b	.L14
.L29:
	bl	bad_key_beep
	ldr	r0, .L34+12
	bl	DIALOG_draw_ok_dialog
	b	.L14
.L23:
	ldr	r3, .L34+4
	mov	r2, #51
	str	r2, [r3, #0]
	ldr	r3, .L34+8
	mov	r2, #0
	str	r2, [r3, #0]
	mov	r0, #2
	b	.L31
.L24:
	ldr	r3, .L34+4
	mov	r2, #52
	str	r2, [r3, #0]
	ldr	r3, .L34+8
	mov	r2, #0
	mov	r0, #8
	str	r2, [r3, #0]
.L31:
	bl	TWO_WIRE_jump_to_decoder_list
	b	.L14
.L25:
	ldr	r3, .L34+4
	mov	r2, #53
	str	r2, [r3, #0]
	ldr	r3, .L34+8
	mov	r2, #0
	str	r2, [r3, #0]
	mov	lr, #44
	mov	r3, #11
	mov	r2, #2
	stmia	sp, {r2, r3, lr}
	ldr	r3, .L34+16
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	ldr	r3, .L34+20
	b	.L32
.L26:
	ldr	r3, .L34+4
	mov	r2, #54
	str	r2, [r3, #0]
	ldr	r3, .L34+8
	mov	r2, #0
	str	r2, [r3, #0]
	mov	r0, #2
	mov	r3, #11
	mov	ip, #89
	stmia	sp, {r0, r3, ip}
	ldr	r3, .L34+24
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	ldr	r3, .L34+28
.L32:
	mov	r0, sp
	str	r3, [sp, #16]
	bl	Change_Screen
	b	.L14
.L27:
	ldr	r3, .L34+4
	mov	r2, #55
	str	r2, [r3, #0]
	ldr	r3, .L34+8
	mov	r2, #0
	str	r2, [r3, #0]
	mov	r1, #2
	mov	r3, #88
	mov	r2, #11
	stmia	sp, {r1, r2, r3}
	ldr	r3, .L34+32
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	ldr	r3, .L34+36
	b	.L32
.L21:
	bl	bad_key_beep
	b	.L14
.L18:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L14
.L16:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L14
.L33:
	ldr	r5, .L34
	ldr	r3, .L34+4
	ldrsh	r2, [r5, #0]
	mov	r4, #0
	str	r2, [r3, #0]
	ldr	r3, .L34+8
	str	r4, [r3, #0]
	bl	good_key_beep
	ldr	r3, .L34+40
	mov	r0, r4
	ldr	r3, [r3, #0]
	strh	r3, [r5, #0]	@ movhi
	bl	Redraw_Screen
	b	.L14
.L15:
	bl	KEY_process_global_keys
.L14:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, pc}
.L35:
	.align	2
.L34:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	593
	.word	FDTO_MOISTURE_SENSOR_draw_menu
	.word	MOISTURE_SENSOR_process_menu
	.word	FDTO_ORPHAN_TWO_WIRE_STATIONS_draw_report
	.word	ORPHAN_TWO_WIRE_STATIONS_process_report
	.word	FDTO_ORPHAN_TWO_WIRE_POCS_draw_report
	.word	ORPHAN_TWO_WIRE_POCS_process_report
	.word	.LANCHOR0
.LFE2:
	.size	TWO_WIRE_ASSIGNMENT_process_dialog, .-TWO_WIRE_ASSIGNMENT_process_dialog
	.global	g_TWO_WIRE_dialog_visible
	.global	g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_displayed
	.section	.bss.g_TWO_WIRE_last_cursor_position,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_TWO_WIRE_last_cursor_position, %object
	.size	g_TWO_WIRE_last_cursor_position, 4
g_TWO_WIRE_last_cursor_position:
	.space	4
	.section	.bss.g_TWO_WIRE_dialog_visible,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_TWO_WIRE_dialog_visible, %object
	.size	g_TWO_WIRE_dialog_visible, 4
g_TWO_WIRE_dialog_visible:
	.space	4
	.section	.bss.g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_displayed,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_displayed, %object
	.size	g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_displayed, 4
g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_displayed:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_two_wire_assignment.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_two_wire_assignment.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x3c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x89
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x94
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"TWO_WIRE_ASSIGNMENT_process_dialog\000"
.LASF0:
	.ascii	"FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog\000"
.LASF1:
	.ascii	"TWO_WIRE_ASSIGNMENT_draw_dialog\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_two_wire_assignment.c\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
