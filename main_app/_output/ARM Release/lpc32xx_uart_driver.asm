	.file	"lpc32xx_uart_driver.c"
	.text
.Ltext0:
	.section	.text.uart_abs,"ax",%progbits
	.align	2
	.global	uart_abs
	.type	uart_abs, %function
uart_abs:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, r1
	rsbgt	r0, r1, r0
	rsble	r0, r0, r1
	bx	lr
.LFE0:
	.size	uart_abs, .-uart_abs
	.section	.text.uart_ptr_to_uart_num,"ax",%progbits
	.align	2
	.global	uart_ptr_to_uart_num
	.type	uart_ptr_to_uart_num, %function
uart_ptr_to_uart_num:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L10
	cmp	r0, r3
	moveq	r0, #0
	bxeq	lr
	add	r3, r3, #32768
	cmp	r0, r3
	moveq	r0, #1
	bxeq	lr
	add	r3, r3, #32768
	cmp	r0, r3
	beq	.L8
	add	r3, r3, #32768
	cmp	r0, r3
	moveq	r0, #3
	mvnne	r0, #0
	bx	lr
.L8:
	mov	r0, #2
	bx	lr
.L11:
	.align	2
.L10:
	.word	1074266112
.LFE1:
	.size	uart_ptr_to_uart_num, .-uart_ptr_to_uart_num
	.section	.text.uart_flush_fifos,"ax",%progbits
	.align	2
	.global	uart_flush_fifos
	.type	uart_flush_fifos, %function
uart_flush_fifos:
.LFB2:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	orr	r1, r1, #6
	str	r1, [r0, #8]
	ldr	r3, [r0, #0]
	sub	sp, sp, #4
.LCFI0:
	str	r3, [sp, #0]
	add	sp, sp, #4
	bx	lr
.LFE2:
	.size	uart_flush_fifos, .-uart_flush_fifos
	.global	__udivsi3
	.section	.text.uart_find_clk,"ax",%progbits
	.align	2
	.global	uart_find_clk
	.type	uart_find_clk, %function
uart_find_clk:
.LFB3:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI1:
	mov	r4, r0
	mov	r0, #5
	str	r1, [sp, #4]
	bl	clkpwr_get_base_clock_rate
	mov	r6, #0
	mov	sl, r6
	mov	r7, #1
	mvn	r9, #0
	mov	r3, r6
	mov	fp, r0, lsr #4
	mov	r8, fp
	b	.L14
.L20:
	mov	r0, r8
	mov	r1, r5
	str	r3, [sp, #0]
	bl	__udivsi3
.LBB6:
	ldr	r3, [sp, #0]
	cmp	r0, r4
	rsbgt	r2, r4, r0
	rsble	r2, r0, r4
.LBE6:
	cmp	r2, r9
	bcs	.L17
.LBB7:
	cmp	r0, r4
	rsbgt	r9, r4, r0
	rsble	r9, r0, r4
.LBE7:
	mov	r6, r7
	mov	sl, r5
	mov	r3, r0
.L17:
	add	r5, r5, #1
	cmp	r5, #254
	ble	.L20
	add	r7, r7, #1
	cmp	r7, #255
	add	r8, r8, fp
	beq	.L21
.L14:
	mov	r5, r7
	b	.L20
.L21:
	ldr	r2, [sp, #4]
	mov	r0, r3
	stmia	r2, {r6, sl}
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.LFE3:
	.size	uart_find_clk, .-uart_find_clk
	.section	.text.uart_setup_trans_mode,"ax",%progbits
	.align	2
	.global	uart_setup_trans_mode
	.type	uart_setup_trans_mode, %function
uart_setup_trans_mode:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r1, #12]
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI2:
	sub	r3, r3, #1
	cmp	r3, #1
	ldrls	r2, .L47
	movhi	r4, #0
	ldrlsb	r4, [r2, r3]	@ zero_extendqisi2
	ldr	r3, [r1, #4]
	movls	r6, #0
	mvnhi	r6, #0
	cmp	r3, #1
	mov	r5, r0
	orreq	r4, r4, #24
	beq	.L25
	bcc	.L25
	cmp	r3, #2
	mvnne	r6, #0
	orreq	r4, r4, #8
.L25:
	ldr	r3, [r1, #8]
	sub	r3, r3, #5
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L43
.L34:
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
.L31:
	orr	r4, r4, #1
	b	.L30
.L32:
	orr	r4, r4, #2
	b	.L30
.L33:
	orr	r4, r4, #3
.L30:
	cmp	r6, #0
	bne	.L43
	ldr	r0, [r1, #0]
	add	r1, r5, #24
	bl	uart_find_clk
	ldr	r3, [r5, #16]
	ldr	r2, [r5, #24]
	cmp	r3, #1
	str	r0, [r5, #20]
	beq	.L37
	cmp	r3, #2
	beq	.L38
	ldr	r0, [r5, #28]
	ldr	r1, .L47+4
	mov	r2, r2, asl #24
	and	r0, r0, #255
	cmp	r3, #0
	orr	r2, r0, r2, lsr #16
	streq	r2, [r1, #208]
	beq	.L39
	b	.L46
.L37:
	ldrb	r1, [r5, #28]	@ zero_extendqisi2
	mov	r2, r2, asl #24
	orr	r2, r1, r2, lsr #16
	ldr	r1, .L47+4
	str	r2, [r1, #212]
	b	.L39
.L38:
	ldrb	r1, [r5, #28]	@ zero_extendqisi2
	mov	r2, r2, asl #24
	orr	r2, r1, r2, lsr #16
	ldr	r1, .L47+4
	str	r2, [r1, #216]
	b	.L39
.L46:
	str	r2, [r1, #220]
.L39:
	ldr	r2, .L47+8
	add	r3, r3, #2
	ldr	r1, [r2, #4]
	mov	r3, r3, asl #1
	mov	r0, #3
	bic	r1, r1, r0, asl r3
	mov	r0, #2
	orr	r3, r1, r0, asl r3
	str	r3, [r2, #4]
	ldr	r3, [r5, #0]
	str	r4, [r3, #12]
	b	.L29
.L43:
	mvn	r6, #0
.L29:
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, pc}
.L48:
	.align	2
.L47:
	.word	.LANCHOR0
	.word	1073758208
	.word	1074085888
.LFE4:
	.size	uart_setup_trans_mode, .-uart_setup_trans_mode
	.section	.rodata.CSWTCH.8,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	CSWTCH.8, %object
	.size	CSWTCH.8, 2
CSWTCH.8:
	.byte	0
	.byte	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_uart_driver.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x8a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xf1
	.byte	0x1
	.uleb128 0x3
	.4byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x10e
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x13a
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x15c
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x194
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"uart_find_clk\000"
.LASF0:
	.ascii	"uart_ptr_to_uart_num\000"
.LASF1:
	.ascii	"uart_flush_fifos\000"
.LASF3:
	.ascii	"uart_setup_trans_mode\000"
.LASF6:
	.ascii	"uart_abs\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_uart_driver.c\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
