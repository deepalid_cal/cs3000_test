	.file	"e_acquire_expecteds.c"
	.text
.Ltext0:
	.section	.text.FDTO_ACQUIRE_EXPECTEDS_show_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_ACQUIRE_EXPECTEDS_show_dropdown, %function
FDTO_ACQUIRE_EXPECTEDS_show_dropdown:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
.LFE0:
	.size	FDTO_ACQUIRE_EXPECTEDS_show_dropdown, .-FDTO_ACQUIRE_EXPECTEDS_show_dropdown
	.section	.text.FDTO_ACQUIRE_EXPECTEDS_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_ACQUIRE_EXPECTEDS_draw_screen
	.type	FDTO_ACQUIRE_EXPECTEDS_draw_screen, %function
FDTO_ACQUIRE_EXPECTEDS_draw_screen:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L7
	str	lr, [sp, #-4]!
.LCFI0:
	ldrnesh	r1, [r3, #0]
	bne	.L6
	bl	ACQUIRE_EXPECTEDS_copy_group_into_guivars
	mov	r1, #0
.L6:
	mov	r0, #2
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L8:
	.align	2
.L7:
	.word	GuiLib_ActiveCursorFieldNo
.LFE1:
	.size	FDTO_ACQUIRE_EXPECTEDS_draw_screen, .-FDTO_ACQUIRE_EXPECTEDS_draw_screen
	.section	.text.ACQUIRE_EXPECTEDS_process_screen,"ax",%progbits
	.align	2
	.global	ACQUIRE_EXPECTEDS_process_screen
	.type	ACQUIRE_EXPECTEDS_process_screen, %function
ACQUIRE_EXPECTEDS_process_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L54
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #44
.LCFI2:
	cmp	r3, #740
	mov	r4, r0
	mov	r5, r1
	bne	.L49
	ldr	r3, .L54+4
	ldr	r1, [r3, #0]
	bl	COMBO_BOX_key_press
	b	.L9
.L49:
	cmp	r0, #4
	beq	.L15
	bhi	.L19
	cmp	r0, #1
	beq	.L15
	bcc	.L14
	cmp	r0, #2
	beq	.L16
	cmp	r0, #3
	b	.L53
.L19:
	cmp	r0, #67
	beq	.L17
	bhi	.L20
	cmp	r0, #16
	beq	.L15
	cmp	r0, #20
.L53:
	bne	.L13
	b	.L14
.L20:
	cmp	r0, #80
	beq	.L18
	cmp	r0, #84
	bne	.L13
	b	.L18
.L16:
	ldr	r3, .L54+8
	ldrsh	r2, [r3, #0]
	ldr	r3, .L54+4
	cmp	r2, #9
	ldrls	pc, [pc, r2, asl #2]
	b	.L21
.L32:
	.word	.L22
	.word	.L23
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
.L22:
	ldr	r2, .L54+12
	b	.L50
.L23:
	ldr	r2, .L54+16
	b	.L50
.L24:
	ldr	r2, .L54+20
	b	.L50
.L25:
	ldr	r2, .L54+24
	b	.L50
.L26:
	ldr	r2, .L54+28
	b	.L50
.L27:
	ldr	r2, .L54+32
	b	.L50
.L28:
	ldr	r2, .L54+36
	b	.L50
.L29:
	ldr	r2, .L54+40
	b	.L50
.L30:
	ldr	r2, .L54+44
	b	.L50
.L31:
	ldr	r2, .L54+48
.L50:
	str	r2, [r3, #0]
.L21:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L33
	bl	good_key_beep
	mov	r3, #3
	str	r3, [sp, #8]
	ldr	r3, .L54+52
	add	r0, sp, #8
	str	r3, [sp, #28]
	mov	r3, #251
	str	r3, [sp, #32]
	ldr	r3, .L54+8
	ldrh	r3, [r3, #0]
	cmp	r3, #0
	movne	r3, r3, asl #16
	movne	r3, r3, asr #16
	addne	r3, r3, r3, asl #3
	movne	r3, r3, asl #1
	addne	r3, r3, #46
	moveq	r3, #46
	str	r3, [sp, #36]
	bl	Display_Post_Command
	b	.L9
.L33:
	bl	bad_key_beep
	b	.L9
.L18:
	ldr	r3, .L54+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L35
.L46:
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
.L36:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L54+12
	b	.L51
.L37:
	ldr	r1, .L54+16
	mov	r3, #1
	mov	r0, r4
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L51:
	mov	r2, #0
	bl	process_uns32
	b	.L47
.L38:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L54+20
	b	.L51
.L39:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L54+24
	b	.L51
.L40:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L54+28
	b	.L51
.L41:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L54+32
	b	.L51
.L42:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L54+36
	b	.L51
.L43:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L54+40
	b	.L51
.L44:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L54+44
	b	.L51
.L45:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L54+48
	b	.L51
.L35:
	bl	bad_key_beep
.L47:
	mov	r0, #0
	bl	Redraw_Screen
	b	.L9
.L15:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L9
.L14:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L9
.L17:
	ldr	r3, .L54+56
	mov	r2, #2
	str	r2, [r3, #0]
	bl	ACQUIRE_EXPECTEDS_extract_and_store_changes_from_GuiVars
.L13:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L9:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L55:
	.align	2
.L54:
	.word	GuiLib_CurStructureNdx
	.word	.LANCHOR0
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingA_9
	.word	FDTO_ACQUIRE_EXPECTEDS_show_dropdown
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	ACQUIRE_EXPECTEDS_process_screen, .-ACQUIRE_EXPECTEDS_process_screen
	.section	.bss.g_ACQUIRE_EXPECTEDS_combo_box_guivar,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_ACQUIRE_EXPECTEDS_combo_box_guivar, %object
	.size	g_ACQUIRE_EXPECTEDS_combo_box_guivar, 4
g_ACQUIRE_EXPECTEDS_combo_box_guivar:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_acquire_expecteds.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x58
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x30
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x36
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x4a
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_acquire_expecteds.c\000"
.LASF0:
	.ascii	"FDTO_ACQUIRE_EXPECTEDS_draw_screen\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"FDTO_ACQUIRE_EXPECTEDS_show_dropdown\000"
.LASF1:
	.ascii	"ACQUIRE_EXPECTEDS_process_screen\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
