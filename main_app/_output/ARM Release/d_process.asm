	.file	"d_process.c"
	.text
.Ltext0:
	.section	.text.Display_Post_Command,"ax",%progbits
	.align	2
	.global	Display_Post_Command
	.type	Display_Post_Command, %function
Display_Post_Command:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L3
	mov	r2, #0
	mov	r1, r0
	str	lr, [sp, #-4]!
.LCFI0:
	ldr	r0, [r3, #0]
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	ldreq	pc, [sp], #4
	ldr	r3, .L3+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #74
	ldreq	pc, [sp], #4
	ldr	r0, .L3+8
	ldr	lr, [sp], #4
	b	Alert_Message
.L4:
	.align	2
.L3:
	.word	Display_Command_Queue
	.word	GuiLib_CurStructureNdx
	.word	.LC0
.LFE0:
	.size	Display_Post_Command, .-Display_Post_Command
	.section	.text.Display_Processing_Task,"ax",%progbits
	.align	2
	.global	Display_Processing_Task
	.type	Display_Processing_Task, %function
Display_Processing_Task:
.LFB1:
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	ldr	r5, .L18
	sub	sp, sp, #44
.LCFI2:
	rsb	r5, r5, r0
	bl	FDTO_set_guilib_to_write_to_the_primary_display_buffer
	bl	GuiLib_Init
	bl	CONTRAST_AND_SPEAKER_VOL_get_screen_language
	ldr	r4, .L18+4
	ldr	r7, .L18+8
	ldr	r6, .L18+12
	mov	r5, r5, lsr #5
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	bl	GuiLib_SetLanguage
	ldr	r3, .L18+16
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r0, .L18+20
	bl	postBackground_Calculation_Event
	mov	r0, #0
	mov	r1, #47
	mov	r2, #57
	mvn	r3, #0
	bl	GuiLib_ShowBitmap
	bl	GuiLib_Refresh
.L17:
	ldr	r0, [r7, #0]
	add	r1, sp, #36
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	beq	.L7
	bl	xTaskGetTickCount
	ldr	r3, .L18+24
	cmp	r4, r3
	str	r0, [r6, r5, asl #2]
	bls	.L8
	sub	r4, r4, #500
	cmp	r4, r3
	bhi	.L17
	b	.L7
.L8:
	cmp	r4, #0
	bne	.L17
.L7:
	bl	FDTO_STATUS_draw_screen
	mov	r0, #1
	bl	FDTO_MAIN_MENU_draw_menu
	ldr	r6, .L18+28
	ldr	r4, .L18+12
.L14:
	ldr	r0, [r6, #0]
	mov	r1, sp
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	bne	.L10
	ldr	r3, [sp, #0]
	cmp	r3, #2
	beq	.L12
	cmp	r3, #3
	beq	.L13
	cmp	r3, #1
	bne	.L10
	ldr	r3, [sp, #20]
	blx	r3
	b	.L10
.L13:
	ldr	r0, [sp, #24]
	ldr	r1, [sp, #28]
	ldr	r3, [sp, #20]
	blx	r3
	b	.L10
.L12:
	ldr	r0, [sp, #24]
	ldr	r3, [sp, #20]
	blx	r3
.L10:
	bl	xTaskGetTickCount
	str	r0, [r4, r5, asl #2]
	b	.L14
.L19:
	.align	2
.L18:
	.word	Task_Table
	.word	5000
	.word	Key_To_Process_Queue
	.word	task_last_execution_stamp
	.word	.LANCHOR0
	.word	4369
	.word	499
	.word	Display_Command_Queue
.LFE1:
	.size	Display_Processing_Task, .-Display_Processing_Task
	.global	GuiLib_okay_to_use
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"D_PROCESS: queue FULL\000"
	.section	.bss.GuiLib_okay_to_use,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	GuiLib_okay_to_use, %object
	.size	GuiLib_okay_to_use, 4
GuiLib_okay_to_use:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/d_process.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x46
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x33
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x78
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"Display_Post_Command\000"
.LASF1:
	.ascii	"Display_Processing_Task\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/d"
	.ascii	"_process.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
