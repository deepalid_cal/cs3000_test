	.file	"group_base_file.c"
	.text
.Ltext0:
	.section	.text.FDTO_GROUP_draw_add_new_window,"ax",%progbits
	.align	2
	.type	FDTO_GROUP_draw_add_new_window, %function
FDTO_GROUP_draw_add_new_window:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L7
	ldr	r2, .L7+4
	ldr	r3, [r3, #0]
	mov	r1, #36
	mla	r3, r1, r3, r2
	str	lr, [sp, #-4]!
.LCFI0:
	ldr	r2, [r3, #8]
	ldr	r3, .L7+8
	cmp	r2, #35
	moveq	r0, #5
	beq	.L6
	cmp	r2, #39
	moveq	r0, #6
	beq	.L6
	cmp	r2, #71
	moveq	r0, #7
	movne	r0, #4
.L6:
	ldrsh	r1, [r3, #0]
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L8:
	.align	2
.L7:
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiLib_ActiveCursorFieldNo
.LFE10:
	.size	FDTO_GROUP_draw_add_new_window, .-FDTO_GROUP_draw_add_new_window
	.section	.text.nm_GROUP_create_new_group,"ax",%progbits
	.align	2
	.global	nm_GROUP_create_new_group
	.type	nm_GROUP_create_new_group, %function
nm_GROUP_create_new_group:
.LFB1:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI1:
	ldr	r8, [sp, #36]
	mov	r5, r0
	mov	r6, r1
	mov	r7, r2
	mov	r0, #20
	ldr	r1, .L19
	ldr	r2, .L19+4
	mov	sl, r3
	ldr	r9, [sp, #40]
	bl	mem_malloc_debug
	cmp	r8, #1
	mov	r4, r0
	stmia	r0, {r5, r7, sl}
	str	r6, [r0, #16]
	bne	.L10
	ldr	r3, [r5, #8]
	cmp	r3, #0
	beq	.L10
	mov	r0, r5
	bl	nm_ListGetFirst
	ldr	r3, [r0, #12]
	b	.L17
.L10:
	mov	r3, #0
.L17:
	str	r3, [r4, #12]
.LBB4:
	ldr	r1, .L19
	mov	r2, #148
	ldr	r0, [r4, #8]
	bl	mem_malloc_debug
	mov	r1, #0
	ldr	r2, [r4, #8]
	mov	r5, r0
	bl	memset
	ldr	r3, [r4, #12]
	add	r0, r5, #20
	add	r3, r3, #1
	str	r3, [r4, #12]
	mov	r1, #48
	str	r3, [r5, #16]
	ldr	r2, .L19+8
	str	r3, [sp, #0]
	ldr	r3, [r4, #16]
	bl	snprintf
	ldr	r3, [r4, #4]
	cmp	r3, #0
	beq	.L12
	mov	r0, r5
	mov	r1, r8
	blx	r3
.L12:
	mov	r3, #0
	cmp	r9, r3
	str	r3, [r5, #68]
	ldr	r0, [r4, #0]
	mov	r1, r5
	bne	.L13
	bl	nm_ListInsertTail
	b	.L14
.L13:
	mov	r2, r9
	bl	nm_ListInsert
.L14:
	ldr	r0, [r4, #0]
	bl	nm_ListGetFirst
	b	.L18
.L16:
	ldr	r3, [r4, #12]
	ldr	r0, [r4, #0]
	str	r3, [r1, #12]
	bl	nm_ListGetNext
.L18:
	cmp	r0, #0
	mov	r1, r0
	bne	.L16
.LBE4:
	mov	r0, r4
	ldr	r1, .L19
	ldr	r2, .L19+12
	bl	mem_free_debug
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, pc}
.L20:
	.align	2
.L19:
	.word	.LC0
	.word	267
	.word	.LC1
	.word	286
.LFE1:
	.size	nm_GROUP_create_new_group, .-nm_GROUP_create_new_group
	.section	.text.nm_GROUP_get_group_ID,"ax",%progbits
	.align	2
	.global	nm_GROUP_get_group_ID
	.type	nm_GROUP_get_group_ID, %function
nm_GROUP_get_group_ID:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	subs	r4, r0, #0
	ldrne	r0, [r4, #16]
	ldmnefd	sp!, {r4, pc}
.LBB7:
	ldr	r0, .L24
	ldr	r1, .L24+4
	bl	Alert_func_call_with_null_ptr_with_filename
	mov	r0, r4
.LBE7:
	ldmfd	sp!, {r4, pc}
.L25:
	.align	2
.L24:
	.word	.LC0
	.word	343
.LFE2:
	.size	nm_GROUP_get_group_ID, .-nm_GROUP_get_group_ID
	.section	.text.nm_GROUP_get_name,"ax",%progbits
	.align	2
	.global	nm_GROUP_get_name
	.type	nm_GROUP_get_name, %function
nm_GROUP_get_name:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI3:
	subs	r4, r0, #0
	addne	r0, r4, #20
	ldmnefd	sp!, {r4, pc}
	ldr	r0, .L29
	ldr	r1, .L29+4
	bl	Alert_func_call_with_null_ptr_with_filename
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L30:
	.align	2
.L29:
	.word	.LC0
	.word	398
.LFE3:
	.size	nm_GROUP_get_name, .-nm_GROUP_get_name
	.section	.text.nm_GROUP_find_this_group_in_list,"ax",%progbits
	.align	2
	.global	nm_GROUP_find_this_group_in_list
	.type	nm_GROUP_find_this_group_in_list, %function
nm_GROUP_find_this_group_in_list:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI4:
	subs	r6, r1, #0
	mov	r4, r0
	mov	r5, r2
	beq	.L32
	bl	nm_ListGetFirst
	b	.L37
.L35:
	mov	r0, r4
	bl	nm_ListGetNext
.L37:
	str	r0, [r5, #0]
	ldr	r0, [r5, #0]
	bl	nm_GROUP_get_group_ID
	mov	r7, r0
	mov	r0, r6
	bl	nm_GROUP_get_group_ID
	ldr	r1, [r5, #0]
	cmp	r7, r0
	beq	.L34
	cmp	r1, #0
	bne	.L35
.L34:
	adds	r0, r1, #0
	movne	r0, #1
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L32:
	ldr	r0, .L38
	mov	r1, #472
	bl	Alert_func_call_with_null_ptr_with_filename
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L39:
	.align	2
.L38:
	.word	.LC0
.LFE4:
	.size	nm_GROUP_find_this_group_in_list, .-nm_GROUP_find_this_group_in_list
	.section	.text.nm_GROUP_get_ptr_to_group_at_this_location_in_list,"ax",%progbits
	.align	2
	.global	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	.type	nm_GROUP_get_ptr_to_group_at_this_location_in_list, %function
nm_GROUP_get_ptr_to_group_at_this_location_in_list:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #8]
	stmfd	sp!, {r4, r5, lr}
.LCFI5:
	sub	r3, r3, #1
	cmp	r1, r3
	mov	r4, r1
	mov	r5, r0
	movhi	r1, #0
	bhi	.L41
	bl	nm_ListGetFirst
	mov	r1, r0
	b	.L42
.L43:
	mov	r0, r5
	bl	nm_ListGetNext
	sub	r4, r4, #1
	mov	r1, r0
.L42:
	cmp	r4, #0
	bne	.L43
.L41:
	mov	r0, r1
	ldmfd	sp!, {r4, r5, pc}
.LFE5:
	.size	nm_GROUP_get_ptr_to_group_at_this_location_in_list, .-nm_GROUP_get_ptr_to_group_at_this_location_in_list
	.section	.text.nm_GROUP_get_ptr_to_group_with_this_GID,"ax",%progbits
	.align	2
	.global	nm_GROUP_get_ptr_to_group_with_this_GID
	.type	nm_GROUP_get_ptr_to_group_with_this_GID, %function
nm_GROUP_get_ptr_to_group_with_this_GID:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI6:
	subs	r5, r0, #0
	mov	r4, r1
	mov	r6, r2
	beq	.L46
	bl	nm_ListGetFirst
	b	.L51
.L49:
	ldr	r3, [r1, #16]
	cmp	r3, r4
	beq	.L48
	mov	r0, r5
	bl	nm_ListGetNext
.L51:
	cmp	r0, #0
	mov	r1, r0
	bne	.L49
.L48:
	cmp	r1, #0
	cmpeq	r6, #0
	bne	.L50
	ldr	r1, .L52
	ldr	r0, .L52+4
	bl	Alert_group_not_found_with_filename
	mov	r1, #0
	b	.L50
.L46:
	ldr	r1, .L52+8
	ldr	r0, .L52+4
	bl	Alert_func_call_with_null_ptr_with_filename
	mov	r1, r5
.L50:
	mov	r0, r1
	ldmfd	sp!, {r4, r5, r6, pc}
.L53:
	.align	2
.L52:
	.word	586
	.word	.LC0
	.word	591
.LFE6:
	.size	nm_GROUP_get_ptr_to_group_with_this_GID, .-nm_GROUP_get_ptr_to_group_with_this_GID
	.section	.text.nm_GROUP_get_index_for_group_with_this_GID,"ax",%progbits
	.align	2
	.global	nm_GROUP_get_index_for_group_with_this_GID
	.type	nm_GROUP_get_index_for_group_with_this_GID, %function
nm_GROUP_get_index_for_group_with_this_GID:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI7:
	subs	r7, r1, #0
	mov	r6, r0
	moveq	r4, r7
	beq	.L55
	cmp	r0, #0
	beq	.L56
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r5, r0
	b	.L57
.L58:
	mov	r0, r5
	bl	nm_GROUP_get_group_ID
	cmp	r0, r7
	beq	.L55
	mov	r1, r5
	mov	r0, r6
	bl	nm_ListGetNext
	add	r4, r4, #1
	mov	r5, r0
.L57:
	cmp	r5, #0
	bne	.L58
	b	.L62
.L56:
	ldr	r0, .L63
	ldr	r1, .L63+4
	bl	Alert_func_call_with_null_ptr_with_filename
	mov	r4, r6
.L55:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L62:
	ldr	r0, .L63
	ldr	r1, .L63+8
	bl	Alert_group_not_found_with_filename
	mov	r4, r5
	b	.L55
.L64:
	.align	2
.L63:
	.word	.LC0
	.word	662
	.word	655
.LFE7:
	.size	nm_GROUP_get_index_for_group_with_this_GID, .-nm_GROUP_get_index_for_group_with_this_GID
	.section	.text.GROUP_process_show_keyboard,"ax",%progbits
	.align	2
	.global	GROUP_process_show_keyboard
	.type	GROUP_process_show_keyboard, %function
GROUP_process_show_keyboard:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI8:
	mov	r5, r0
	mov	r4, r1
	bl	good_key_beep
	ldr	r3, .L68
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	moveq	r0, r5
	moveq	r1, r4
	moveq	r2, #48
	addne	r0, r5, #10
	movne	r1, r4
	movne	r2, #48
	movne	r3, #0
	ldmfd	sp!, {r4, r5, lr}
	b	KEYBOARD_draw_keyboard
.L69:
	.align	2
.L68:
	.word	GuiLib_LanguageIndex
.LFE8:
	.size	GROUP_process_show_keyboard, .-GROUP_process_show_keyboard
	.section	.text.nm_GROUP_create_new_group_from_UI,"ax",%progbits
	.align	2
	.global	nm_GROUP_create_new_group_from_UI
	.type	nm_GROUP_create_new_group_from_UI, %function
nm_GROUP_create_new_group_from_UI:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L72
	mov	r2, #1
	cmp	r0, #0
	stmfd	sp!, {r4, lr}
.LCFI9:
	mov	r4, r1
	str	r2, [r3, #0]
	ldmeqfd	sp!, {r4, pc}
	blx	r0
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L72+4
	cmp	r4, #0
	str	r0, [r3, #0]
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L72+8
	ldr	r0, [r3, #0]
	blx	r4
	ldmfd	sp!, {r4, pc}
.L73:
	.align	2
.L72:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
.LFE9:
	.size	nm_GROUP_create_new_group_from_UI, .-nm_GROUP_create_new_group_from_UI
	.section	.text.nm_GROUP_load_common_guivars,"ax",%progbits
	.align	2
	.global	nm_GROUP_load_common_guivars
	.type	nm_GROUP_load_common_guivars, %function
nm_GROUP_load_common_guivars:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI10:
	mov	r4, r0
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L75
	str	r0, [r3, #0]
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L75+4
	ldmfd	sp!, {r4, lr}
	b	strlcpy
.L76:
	.align	2
.L75:
	.word	.LANCHOR1
	.word	GuiVar_GroupName
.LFE11:
	.size	nm_GROUP_load_common_guivars, .-nm_GROUP_load_common_guivars
	.section	.text.FDTO_GROUP_return_to_menu,"ax",%progbits
	.align	2
	.global	FDTO_GROUP_return_to_menu
	.type	FDTO_GROUP_return_to_menu, %function
FDTO_GROUP_return_to_menu:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI11:
	mov	r5, r0
	blx	r1
	ldr	r3, .L78
	mov	r4, #0
	str	r4, [r5, #0]
	mov	r0, r4
	ldrh	r1, [r3, #0]
	bl	GuiLib_ScrollBox_To_Line
	ldr	r3, .L78+4
	mvn	r2, #0
	mov	r0, r4
	strh	r2, [r3, #0]	@ movhi
	ldmfd	sp!, {r4, r5, lr}
	b	FDTO_Redraw_Screen
.L79:
	.align	2
.L78:
	.word	.LANCHOR2
	.word	GuiLib_ActiveCursorFieldNo
.LFE12:
	.size	FDTO_GROUP_return_to_menu, .-FDTO_GROUP_return_to_menu
	.section	.text.FDTO_GROUP_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_GROUP_draw_menu
	.type	FDTO_GROUP_draw_menu, %function
FDTO_GROUP_draw_menu:
.LFB13:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI12:
	ldr	r8, [sp, #36]
	cmp	r0, #1
	mov	r7, r1
	mov	r4, r2
	mov	sl, r3
	ldr	r9, [sp, #40]
	ldr	r6, [sp, #44]
	bne	.L81
	mov	r3, #0
	str	r3, [r1, #0]
	ldr	r3, .L92
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L82
	cmp	sl, #39
	ldr	r5, .L92+4
	bne	.L83
	ldr	r3, .L92+8
	ldr	r0, [r3, #0]
	bl	MANUAL_PROGRAMS_get_index_for_group_with_this_GID
	b	.L91
.L83:
	cmp	sl, #54
	bne	.L84
	ldr	r3, .L92+8
	ldr	r0, [r3, #0]
	bl	STATION_GROUP_get_index_for_group_with_this_GID
	b	.L91
.L84:
	cmp	sl, #71
	bne	.L82
	ldr	r3, .L92+8
	ldr	r0, [r3, #0]
	bl	WALK_THRU_get_index_for_group_with_this_GID
.L91:
	str	r0, [r5, #0]
.L82:
	ldr	r3, .L92+4
	mov	r5, #0
	ldr	r0, [r3, #0]
	blx	r9
	mvn	r9, #0
	b	.L85
.L81:
	ldr	r3, .L92+12
	mov	r0, #0
	ldrsh	r9, [r3, #0]
	bl	GuiLib_ScrollBox_GetTopLine
	mov	r5, r0
.L85:
	mov	r0, sl, asl #16
	mov	r0, r0, lsr #16
	mov	r1, r9
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	r3, .L92+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L92+20
	ldr	r3, .L92+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r7, #0]
	cmp	r3, #1
	bne	.L86
	add	r6, r4, r6, asl #1
	mov	r2, r6, asl #16
	mov	r5, r5, asl #16
	mov	r1, r8
	mov	r2, r2, asr #16
	mov	r5, r5, asr #16
	mvn	r3, #0
	mov	r0, #0
	str	r5, [sp, #0]
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	ldr	r3, .L92+4
	mov	r0, #0
	ldrsh	r1, [r3, #0]
	bl	GuiLib_ScrollBox_SetIndicator
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
	b	.L87
.L86:
	ldr	r3, .L92+4
	mov	r5, r5, asl #16
	ldr	r2, [r3, #0]
	mov	r5, r5, asr #16
	cmp	r2, r4
	cmpcs	r6, #1
	addeq	r2, r4, #1
	add	r6, r4, r6, asl #1
	streq	r2, [r3, #0]
	mov	r2, r6, asl #16
	ldrsh	r3, [r3, #0]
	mov	r0, #0
	mov	r1, r8
	mov	r2, r2, asr #16
	str	r5, [sp, #0]
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L87:
	ldr	r3, .L92+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L92+4
	ldr	r3, [r3, #0]
	cmp	r3, r4
	bcc	.L89
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	FDTO_GROUP_draw_add_new_window
.L89:
	ldr	r3, .L92+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L80
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	GuiLib_Refresh
.L80:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L93:
	.align	2
.L92:
	.word	GuiVar_StatusShowLiveScreens
	.word	.LANCHOR2
	.word	GuiVar_StatusNextScheduledIrriGID
	.word	GuiLib_ActiveCursorFieldNo
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	837
	.word	g_STATION_SELECTION_GRID_user_pressed_back
.LFE13:
	.size	FDTO_GROUP_draw_menu, .-FDTO_GROUP_draw_menu
	.section	.text.GROUP_process_NEXT_and_PREV,"ax",%progbits
	.align	2
	.global	GROUP_process_NEXT_and_PREV
	.type	GROUP_process_NEXT_and_PREV, %function
GROUP_process_NEXT_and_PREV:
.LFB14:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI13:
	ldr	r8, .L103
	cmp	r0, #20
	mov	r6, r2
	mov	r4, r3
	ldr	r5, [sp, #24]
	ldr	r7, [r8, #0]
	bne	.L95
	sub	r1, r1, #1
	cmp	r7, r1
	bcs	.L96
	ldr	r3, .L103+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #67
	beq	.L97
	mov	r0, #0
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	ldr	r1, [r8, #0]
	bl	SCROLL_BOX_to_line
	mov	r0, #0
	mov	r1, r0
	bl	SCROLL_BOX_up_or_down
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
.L97:
	ldr	r3, .L103
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	b	.L102
.L95:
	cmp	r0, #16
	bne	.L96
	cmp	r7, #0
	beq	.L96
	ldr	r3, .L103+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #67
	beq	.L98
	mov	r0, #0
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	ldr	r1, [r8, #0]
	bl	SCROLL_BOX_to_line
	mov	r0, #0
	mov	r1, #4
	bl	SCROLL_BOX_up_or_down
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
.L98:
	ldr	r3, .L103
	ldr	r2, [r3, #0]
	sub	r2, r2, #1
.L102:
	str	r2, [r3, #0]
.L96:
	ldr	r3, .L103
	ldr	r3, [r3, #0]
	cmp	r7, r3
	beq	.L99
	ldr	r3, .L103+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #67
	bne	.L100
	bl	good_key_beep
.L100:
	ldr	r3, .L103+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L103+12
	mov	r3, #960
	bl	xQueueTakeMutexRecursive_debug
	blx	r6
	cmp	r4, #0
	ldr	r6, .L103
	beq	.L101
	ldr	r0, [r6, #0]
	blx	r4
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L103+16
	str	r0, [r3, #0]
.L101:
	ldr	r0, [r6, #0]
	blx	r5
	ldr	r3, .L103+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	Redraw_Screen
.L99:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	bad_key_beep
.L104:
	.align	2
.L103:
	.word	.LANCHOR2
	.word	GuiLib_CurStructureNdx
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR1
.LFE14:
	.size	GROUP_process_NEXT_and_PREV, .-GROUP_process_NEXT_and_PREV
	.section	.text.GROUP_process_menu,"ax",%progbits
	.align	2
	.global	GROUP_process_menu
	.type	GROUP_process_menu, %function
GROUP_process_menu:
.LFB15:
	@ args = 16, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI14:
	mov	r4, r3
	ldr	r3, [r2, #0]
	sub	sp, sp, #36
.LCFI15:
	cmp	r3, #1
	mov	ip, r0
	mov	lr, r1
	mov	r5, r2
	ldr	r8, [sp, #72]
	ldr	r9, [sp, #76]
	mov	r7, r0
	bne	.L106
	ldr	r3, [sp, #68]
	blx	r3
	b	.L105
.L106:
	cmp	r0, #20
	bhi	.L108
	mov	sl, #1
	mov	r3, sl, asl r0
	tst	r3, #1114112
	bne	.L111
	ands	r6, r3, #17
	bne	.L109
	tst	r3, #12
	beq	.L108
	bl	good_key_beep
	mov	r0, r6
	mov	r1, r6
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L123
	cmp	r0, r4
	str	r0, [r3, #0]
	bcc	.L112
	str	r4, [r3, #0]
	ldr	r3, .L123+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	bne	.L113
	cmp	r4, sl
	mov	r0, r5
	mov	r1, r8
	bls	.L114
	bl	COPY_DIALOG_draw_dialog
	b	.L105
.L114:
	mov	r2, r6
	mov	r3, r4
	bl	COPY_DIALOG_peform_copy_and_redraw_screen
	ldr	r0, .L123+8
	bl	DIALOG_draw_ok_dialog
	b	.L105
.L113:
	cmp	r8, #0
	beq	.L105
	blx	r8
.L112:
	ldr	r3, .L123+12
	str	sl, [r5, #0]
	mov	r0, r6
	strh	r6, [r3, #0]	@ movhi
	bl	Redraw_Screen
	b	.L105
.L111:
	cmp	r0, #20
	movne	r7, #4
	moveq	r7, #0
.L109:
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r5, r0
	mov	r0, #0
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r1, r7
	mov	r0, #0
	bl	SCROLL_BOX_up_or_down
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L123
	cmp	r5, r0
	str	r0, [r3, #0]
	beq	.L105
	cmp	r0, r4
	bne	.L115
	mov	r2, #2
	cmp	r7, #4
	str	r2, [sp, #0]
	ldreq	r2, .L123+16
	ldrne	r2, .L123+20
	streq	r2, [sp, #20]
	strne	r2, [sp, #20]
	subeq	r2, r4, #1
	addne	r2, r4, #1
	mov	r5, #0
	mov	r0, sp
	str	r2, [r3, #0]
	str	r5, [sp, #24]
	bl	Display_Post_Command
	mov	r0, r5
	bl	SCROLL_BOX_redraw
.L115:
	ldr	r5, .L123
	ldr	r3, [r5, #0]
	cmp	r3, r4
	bcs	.L118
	ldr	r3, .L123+24
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L123+28
	ldr	r3, .L123+32
	bl	xQueueTakeMutexRecursive_debug
	cmp	r9, #0
	beq	.L119
	ldr	r0, [r5, #0]
	blx	r9
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L123+36
	str	r0, [r3, #0]
.L119:
	ldr	r3, .L123
	ldr	r0, [r3, #0]
	ldr	r3, [sp, #80]
	blx	r3
	mov	r0, #0
	bl	Redraw_Screen
	ldr	r3, .L123+24
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L105
.L118:
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L123+40
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	b	.L105
.L108:
	cmp	ip, #67
	bne	.L120
	ldr	r3, .L123+44
	ldr	r2, .L123+48
	ldr	r3, [r3, #0]
	mov	r1, #36
	mla	r3, r1, r3, r2
	ldr	r2, [r3, #4]
	ldr	r3, .L123+52
	str	r2, [r3, #0]
.L120:
	mov	r0, ip
	mov	r1, lr
	bl	KEY_process_global_keys
.L105:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L124:
	.align	2
.L123:
	.word	.LANCHOR2
	.word	GuiLib_CurStructureNdx
	.word	606
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiLib_ScrollBox_Up
	.word	GuiLib_ScrollBox_Down
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	1133
	.word	.LANCHOR1
	.word	FDTO_GROUP_draw_add_new_window
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE15:
	.size	GROUP_process_menu, .-GROUP_process_menu
	.global	g_GROUP_deleted__pending_transmission
	.global	g_GROUP_list_item_index
	.global	g_GROUP_ID
	.global	g_GROUP_creating_new
	.section	.bss.g_GROUP_ID,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_GROUP_ID, %object
	.size	g_GROUP_ID, 4
g_GROUP_ID:
	.space	4
	.section	.bss.g_GROUP_deleted__pending_transmission,"aw",%nobits
	.align	2
	.type	g_GROUP_deleted__pending_transmission, %object
	.size	g_GROUP_deleted__pending_transmission, 4
g_GROUP_deleted__pending_transmission:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/util"
	.ascii	"s/group_base_file.c\000"
.LC1:
	.ascii	"%s %u\000"
	.section	.bss.g_GROUP_list_item_index,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_GROUP_list_item_index, %object
	.size	g_GROUP_list_item_index, 4
g_GROUP_list_item_index:
	.space	4
	.section	.bss.g_GROUP_creating_new,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_GROUP_creating_new, %object
	.size	g_GROUP_creating_new, 4
g_GROUP_creating_new:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI0-.LFB10
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI6-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI7-.LFB7
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI8-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI9-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI10-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI11-.LFB12
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI12-.LFB13
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI13-.LFB14
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI14-.LFB15
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/group_base_file.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x166
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF13
	.byte	0x1
	.4byte	.LASF14
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x141
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x2ed
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF17
	.byte	0x1
	.byte	0x8b
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x100
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x178
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x1bf
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x1f9
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x236
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x270
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x2b6
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x2cd
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x302
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST10
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x309
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST11
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x316
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST12
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x384
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST13
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x3d8
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST14
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB10
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB11
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB12
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB13
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB14
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB15
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI15
	.4byte	.LFE15
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF17:
	.ascii	"nm_GROUP_add_new_group_to_the_list\000"
.LASF1:
	.ascii	"nm_GROUP_get_name\000"
.LASF5:
	.ascii	"nm_GROUP_get_index_for_group_with_this_GID\000"
.LASF4:
	.ascii	"nm_GROUP_get_ptr_to_group_with_this_GID\000"
.LASF8:
	.ascii	"nm_GROUP_load_common_guivars\000"
.LASF13:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF14:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/util"
	.ascii	"s/group_base_file.c\000"
.LASF2:
	.ascii	"nm_GROUP_find_this_group_in_list\000"
.LASF6:
	.ascii	"GROUP_process_show_keyboard\000"
.LASF0:
	.ascii	"nm_GROUP_create_new_group\000"
.LASF16:
	.ascii	"FDTO_GROUP_draw_add_new_window\000"
.LASF12:
	.ascii	"GROUP_process_menu\000"
.LASF15:
	.ascii	"nm_GROUP_get_group_ID\000"
.LASF9:
	.ascii	"FDTO_GROUP_return_to_menu\000"
.LASF10:
	.ascii	"FDTO_GROUP_draw_menu\000"
.LASF3:
	.ascii	"nm_GROUP_get_ptr_to_group_at_this_location_in_list\000"
.LASF7:
	.ascii	"nm_GROUP_create_new_group_from_UI\000"
.LASF11:
	.ascii	"GROUP_process_NEXT_and_PREV\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
