	.file	"cs_mem.c"
	.text
.Ltext0:
	.section	.text.init_mem_debug,"ax",%progbits
	.align	2
	.global	init_mem_debug
	.type	init_mem_debug, %function
init_mem_debug:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L6
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L5
	ldr	r2, .L6+4
	ldr	r0, .L6+8
	str	r3, [r2, #0]
	ldr	r2, .L6+12
	ldr	r1, .L6+16
	str	r3, [r2, #0]
	ldr	r2, .L6+20
	str	r3, [r2, #0]
	ldr	r2, .L6+24
	str	r3, [r2, #0]
	mov	r2, r3
.L3:
	str	r2, [r3, r0]
	str	r2, [r3, r1]
	add	r3, r3, #4
	cmp	r3, #44
	bne	.L3
	ldr	r3, .L6
	mov	r2, #1
	str	r2, [r3, #0]
	bx	lr
.L5:
	b	.L5
.L7:
	.align	2
.L6:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR5
	.word	.LANCHOR2
	.word	.LANCHOR6
	.word	.LANCHOR3
	.word	.LANCHOR4
.LFE0:
	.size	init_mem_debug, .-init_mem_debug
	.section	.text.mem_malloc_debug,"ax",%progbits
	.align	2
	.global	mem_malloc_debug
	.type	mem_malloc_debug, %function
mem_malloc_debug:
.LFB3:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
	mov	r9, r0
	mov	r7, r1
	mov	r8, r2
	mov	r6, r0
	b	.L9
.L10:
.LBB9:
.LBB10:
.LBB11:
	add	r6, r6, #1
.L9:
	ands	r4, r6, #3
	bne	.L10
.LBE11:
	ldr	fp, .L25
.LBB12:
	add	r6, r6, #28
	mov	r5, r4
.L18:
.LBE12:
	ldr	sl, [fp, r5, asl #3]
	cmp	sl, r6
	bcc	.L11
	bl	vTaskSuspendAll
	ldr	r3, .L25+4
	mov	r0, #20
	mla	r0, r5, r0, r3
	add	r1, sp, #4
	bl	OSMemGet
	ldr	r2, [sp, #4]
	adds	r3, r0, #0
	movne	r3, #1
	cmp	r2, #0
	movne	r3, #0
	cmp	r3, #0
	mov	r4, r0
	beq	.L12
	ldr	r2, .L25+8
	mov	r6, sl
	ldr	r3, [r2, r5, asl #2]
	add	r3, r3, #1
	str	r3, [r2, r5, asl #2]
	ldr	r2, .L25+12
	ldr	r1, [r2, r5, asl #2]
	cmp	r3, r1
	strhi	r3, [r2, r5, asl #2]
	ldr	r3, .L25+16
	add	r1, sp, #12
	str	r3, [r0, #20]
	ldr	r3, .L25+20
	str	r7, [r0, #8]
	str	r8, [r0, #12]
	str	sl, [r0, #16]
	sub	r0, sl, #4
	str	r3, [r1, #-4]!
	mov	r2, #4
	add	r0, r4, r0
	bl	memcpy
	ldr	r3, .L25+24
	ldr	r1, [r4, #16]
	ldr	r2, [r3, #0]
	add	r5, r4, #24
	stmia	r4, {r2, r3}
	str	r4, [r3, #0]
	ldr	r3, [r4, #0]
	cmp	r3, #0
	strne	r4, [r3, #4]
	ldr	r3, .L25+28
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	ldr	r2, .L25+32
	ldr	r3, [r2, #0]
	add	r3, r1, r3
	str	r3, [r2, #0]
	ldr	r2, .L25+36
	ldr	r1, [r2, #0]
	cmp	r3, r1
	strhi	r3, [r2, #0]
	bl	xTaskResumeAll
	b	.L16
.L12:
	bl	xTaskResumeAll
	ldr	r3, [sp, #4]
	cmp	r3, #72
	bne	.L17
	mov	r0, r7
	bl	RemovePathFromFileName
	add	r1, r5, #1
	mov	r2, r6
	str	r8, [sp, #0]
	mov	r3, r0
	ldr	r0, .L25+40
	bl	Alert_Message_va
	b	.L11
.L17:
	cmp	r4, #0
	bne	.L11
	mov	r0, r7
	bl	RemovePathFromFileName
	mov	r2, r8
	mov	r1, r0
	ldr	r0, .L25+44
	bl	Alert_Message_va
	bl	freeze_and_restart_app
.L11:
	add	r5, r5, #1
	cmp	r5, #11
	bne	.L18
	mov	r5, #0
.L16:
	cmp	r4, #0
	bne	.L19
	ldr	r3, .L25
	mov	r0, r7
	ldr	r3, [r3, #80]
	cmp	r6, r3
	bls	.L20
	bl	RemovePathFromFileName
	mov	r2, r0
	ldr	r0, .L25+48
	b	.L24
.L20:
	bl	RemovePathFromFileName
	mov	r2, r0
	ldr	r0, .L25+52
.L24:
	mov	r1, r6
	mov	r3, r8
	bl	Alert_Message_va
	bl	freeze_and_restart_app
.L19:
.LBE10:
.LBE9:
	cmp	r5, #0
	beq	.L22
	mov	r0, r5
	mov	r1, #221
	mov	r2, r9
	bl	memset
.L22:
	mov	r0, r5
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L26:
	.align	2
.L25:
	.word	partitions
	.word	OSMemTbl
	.word	.LANCHOR5
	.word	.LANCHOR6
	.word	305419896
	.word	-2023406815
	.word	.LANCHOR4
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	.LC3
.LFE3:
	.size	mem_malloc_debug, .-mem_malloc_debug
	.section	.text.mem_oabia,"ax",%progbits
	.align	2
	.global	mem_oabia
	.type	mem_oabia, %function
mem_oabia:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI1:
	mov	r6, r3
	mov	r3, #0
	mov	r5, r0
	mov	r4, r1
	mov	r7, r2
	str	r3, [r1, #0]
	mov	sl, r0
	b	.L28
.L29:
.LBB13:
	add	sl, sl, #1
.L28:
	ands	r8, sl, #3
	bne	.L29
.LBE13:
	bl	vTaskSuspendAll
	ldr	r3, .L34
	ldr	r2, .L34+4
.LBB14:
	add	sl, sl, #28
.L32:
.LBE14:
	ldr	r1, [r8, r2]
	cmp	r1, sl
	bcc	.L30
	ldr	r1, [r3, #16]
	cmp	r1, #0
	beq	.L30
	mov	r0, r5
	mov	r1, r7
	mov	r2, r6
	bl	mem_malloc_debug
	str	r0, [r4, #0]
	mov	r4, #1
	b	.L31
.L30:
	add	r8, r8, #8
	cmp	r8, #88
	add	r3, r3, #20
	bne	.L32
	mov	r4, #0
.L31:
	bl	xTaskResumeAll
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L35:
	.align	2
.L34:
	.word	OSMemTbl
	.word	partitions
.LFE4:
	.size	mem_oabia, .-mem_oabia
	.section	.text.mem_free_debug,"ax",%progbits
	.align	2
	.global	mem_free_debug
	.type	mem_free_debug, %function
mem_free_debug:
.LFB5:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	subs	r4, r0, #0
	mov	r6, r1
	mov	r7, r2
	bne	.L37
	mov	r0, r1
	bl	RemovePathFromFileName
	mov	r2, r7
	mov	r1, r0
	ldr	r0, .L49
	bl	Alert_Message_va
	bl	freeze_and_restart_app
.L37:
	ldr	r3, .L49+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L38
	mov	r0, r6
	bl	RemovePathFromFileName
	mov	r2, r7
	mov	r1, r0
	ldr	r0, .L49+8
	bl	Alert_Message_va
	bl	freeze_and_restart_app
.L38:
	ldr	r2, [r4, #-4]
	ldr	r3, .L49+12
	sub	sl, r4, #24
	cmp	r2, r3
	beq	.L39
	mov	r0, r6
	bl	RemovePathFromFileName
	mov	r2, r7
	mov	r1, r0
	ldr	r0, .L49+16
	bl	Alert_Message_va
	bl	freeze_and_restart_app
.L39:
	ldr	r1, [r4, #-8]
	mov	r2, #4
	add	r1, r4, r1
	mov	r0, sp
	sub	r1, r1, #28
	bl	memcpy
	ldr	r2, [sp, #0]
	ldr	r3, .L49+20
	cmp	r2, r3
	beq	.L40
	mov	r0, r6
	bl	RemovePathFromFileName
	mov	r2, r7
	mov	r1, r0
	ldr	r0, .L49+24
	bl	Alert_Message_va
	bl	freeze_and_restart_app
.L40:
	ldr	r3, .L49+28
	ldr	r2, [r4, #-8]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bls	.L41
	mov	r0, r6
	bl	RemovePathFromFileName
	mov	r2, r7
	mov	r1, r0
	ldr	r0, .L49+32
	bl	Alert_Message_va
	bl	freeze_and_restart_app
.L41:
	bl	vTaskSuspendAll
	ldr	r3, [r4, #-20]
	mov	r0, r4
	cmp	r3, #0
	ldrne	r2, [r4, #-24]
	ldr	r8, .L49+36
	mov	r5, #0
	strne	r2, [r3, #0]
	ldr	r2, [r4, #-24]
	cmp	r2, #0
	strne	r3, [r2, #4]
	ldr	r3, .L49+28
	ldr	r2, [r4, #-8]
	ldr	r1, [r3, #0]
	rsb	r1, r2, r1
	str	r1, [r3, #0]
	ldr	r3, .L49+4
	sub	r2, r2, #24
	ldr	r1, [r3, #0]
	sub	r1, r1, #1
	str	r1, [r3, #0]
	mov	r1, #238
	bl	memset
.L47:
	ldr	r2, [r4, #-8]
	ldr	r3, [r8, r5, asl #3]
	cmp	r2, r3
	bne	.L44
	mov	r0, r5
	mov	r1, sl
	bl	OSMemPut
	cmp	r0, #0
	ldreq	r3, .L49+40
	ldreq	r2, [r3, r5, asl #2]
	subeq	r2, r2, #1
	streq	r2, [r3, r5, asl #2]
	beq	.L46
.L45:
	mov	r0, r6
	bl	RemovePathFromFileName
	mov	r2, r7
	mov	r1, r0
	ldr	r0, .L49+44
	bl	Alert_Message_va
	bl	freeze_and_restart_app
.L44:
	add	r5, r5, #1
	cmp	r5, #11
	bne	.L47
.L46:
	bl	xTaskResumeAll
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, pc}
.L50:
	.align	2
.L49:
	.word	.LC4
	.word	.LANCHOR1
	.word	.LC5
	.word	305419896
	.word	.LC6
	.word	-2023406815
	.word	.LC7
	.word	.LANCHOR2
	.word	.LC8
	.word	partitions
	.word	.LANCHOR5
	.word	.LC9
.LFE5:
	.size	mem_free_debug, .-mem_free_debug
	.section	.text.mem_show_allocations,"ax",%progbits
	.align	2
	.global	mem_show_allocations
	.type	mem_show_allocations, %function
mem_show_allocations:
.LFB6:
	@ args = 0, pretend = 0, frame = 1280
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L56
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI3:
	ldr	r4, [r3, #0]
	sub	sp, sp, #1280
.LCFI4:
	sub	sp, sp, #8
.LCFI5:
	add	r5, sp, #8
	mov	r6, #0
	b	.L52
.L53:
	ldr	r0, [r4, #8]
	ldr	r7, [r4, #16]
	bl	RemovePathFromFileName
	mov	r1, #64
	ldr	r2, .L56+4
	add	r6, r6, #1
	str	r0, [sp, #0]
	ldr	r3, [r4, #12]
	mov	r0, r5
	str	r3, [sp, #4]
	mov	r3, r7
	bl	snprintf
	mov	r0, r5
	ldr	r1, .L56+8
	mov	r2, #64
	bl	strlcat
	ldr	r4, [r4, #0]
	add	r5, r5, #64
.L52:
	cmp	r6, #19
	movhi	r3, #0
	movls	r3, #1
	cmp	r4, #0
	moveq	r3, #0
	cmp	r3, #0
	bne	.L53
	mov	r4, r3
	mov	r6, #2
	mov	r5, #1
.L54:
	add	r3, sp, #8
	add	r7, r4, r3
	mov	r0, r7
	bl	strlen
	add	r4, r4, #64
	mov	r1, r7
	mov	r3, #0
	str	r6, [sp, #0]
	str	r5, [sp, #4]
	mov	r2, r0
	mov	r0, #3
	bl	AddCopyOfBlockToXmitList
	cmp	r4, #1280
	bne	.L54
	add	sp, sp, #264
	add	sp, sp, #1024
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L57:
	.align	2
.L56:
	.word	.LANCHOR4
	.word	.LC10
	.word	.LC11
.LFE6:
	.size	mem_show_allocations, .-mem_show_allocations
	.global	mem_alloclist
	.global	mem_max_allocations_of
	.global	mem_current_allocations_of
	.global	mem_numalloc
	.global	mem_maxalloc
	.global	mem_count
	.global	mem_inited
	.section	.bss.mem_max_allocations_of,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	mem_max_allocations_of, %object
	.size	mem_max_allocations_of, 44
mem_max_allocations_of:
	.space	44
	.section	.bss.mem_numalloc,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	mem_numalloc, %object
	.size	mem_numalloc, 4
mem_numalloc:
	.space	4
	.section	.bss.mem_maxalloc,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	mem_maxalloc, %object
	.size	mem_maxalloc, 4
mem_maxalloc:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Partition %u Out of memory (rqstd %u bytes) : %s %u"
	.ascii	"\000"
.LC1:
	.ascii	"OSMemGet returned success with NULL ptr? : %s %u\000"
.LC2:
	.ascii	"MEM REQUEST TOO BIG : %u bytes : %s, %u\000"
.LC3:
	.ascii	"NO MEMORY : rqstd %u bytes : %s, %u\000"
.LC4:
	.ascii	"MEM_FREE: freeing null ptr : %s, %d\000"
.LC5:
	.ascii	"MEM_FREE : freeing with no count : %s, %d\000"
.LC6:
	.ascii	"MEM_FREE : pointer under run : %s, %d\000"
.LC7:
	.ascii	"MEM_FREE : pointer over run : %s, %d\000"
.LC8:
	.ascii	"MEM_FREE : no more to release : %s, %d\000"
.LC9:
	.ascii	"MEM_FREE : OSMemPut error : %s, %d\000"
.LC10:
	.ascii	"size %lu : %s , %lu\000"
.LC11:
	.ascii	"\012\015\000"
	.section	.bss.mem_current_allocations_of,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	mem_current_allocations_of, %object
	.size	mem_current_allocations_of, 44
mem_current_allocations_of:
	.space	44
	.section	.bss.mem_count,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	mem_count, %object
	.size	mem_count, 4
mem_count:
	.space	4
	.section	.bss.mem_inited,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	mem_inited, %object
	.size	mem_inited, 4
mem_inited:
	.space	4
	.section	.bss.mem_alloclist,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	mem_alloclist, %object
	.size	mem_alloclist, 28
mem_alloclist:
	.space	28
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x82
	.uleb128 0xa
	.byte	0x81
	.uleb128 0xb
	.byte	0x80
	.uleb128 0xc
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI2-.LFB5
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI3-.LFB6
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x514
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x51c
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/utils/cs_mem.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x95
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF7
	.byte	0x1
	.4byte	.LASF8
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x85
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x5f
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x92
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x11f
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x13f
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x178
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1ea
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB5
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB6
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x3
	.byte	0x7d
	.sleb128 1300
	.4byte	.LCFI5
	.4byte	.LFE6
	.2byte	0x3
	.byte	0x7d
	.sleb128 1308
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"mem_calloc_debug\000"
.LASF2:
	.ascii	"init_mem_debug\000"
.LASF3:
	.ascii	"mem_malloc_debug\000"
.LASF4:
	.ascii	"mem_oabia\000"
.LASF0:
	.ascii	"__adjust_block_size\000"
.LASF6:
	.ascii	"mem_show_allocations\000"
.LASF5:
	.ascii	"mem_free_debug\000"
.LASF7:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/util"
	.ascii	"s/cs_mem.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
