	.file	"device_LR_RAVEON.c"
	.text
.Ltext0:
	.section	.text.Extract_val_from_msg,"ax",%progbits
	.align	2
	.type	Extract_val_from_msg, %function
Extract_val_from_msg:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	mov	r3, #0
	b	.L2
.L3:
	add	r3, r3, #1
.L2:
	ldrb	r2, [r0, r3]	@ zero_extendqisi2
	subs	r2, r2, #13
	movne	r2, #1
	cmp	r3, #29
	movgt	r2, #0
	cmp	r2, #0
	bne	.L3
	cmp	r3, #30
	addle	r4, r3, #2
	addle	r3, r0, r3
	ble	.L5
	b	.L4
.L7:
	strb	ip, [r1, r2]
	add	r2, r2, #1
.L5:
	ldrb	r5, [r0, r4]	@ zero_extendqisi2
	cmp	r2, #9
	movgt	ip, #0
	movle	ip, #1
	cmp	r5, #0
	moveq	ip, #0
	cmp	ip, #0
	beq	.L6
	add	r3, r3, #1
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	cmp	ip, #13
	bne	.L7
	b	.L8
.L6:
	cmp	r2, #9
	bgt	.L4
.L8:
	mov	r3, #0
	strb	r3, [r1, r2]
	ldmfd	sp!, {r4, r5, pc}
.L4:
	ldr	r0, .L11
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_Message
.L12:
	.align	2
.L11:
	.word	.LC0
.LFE2:
	.size	Extract_val_from_msg, .-Extract_val_from_msg
	.section	.text.RAVEON_string_exchange_with_specific_termination_string_hunt,"ax",%progbits
	.align	2
	.type	RAVEON_string_exchange_with_specific_termination_string_hunt, %function
RAVEON_string_exchange_with_specific_termination_string_hunt:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, lr}
.LCFI1:
	ldr	r5, .L15
	mov	r6, r1
	mov	r4, r0
	mov	r1, #500
	ldr	r0, [r5, #368]
	mov	r2, #100
	bl	RCVD_DATA_enable_hunting_mode
	ldr	r7, .L15+4
	ldr	r2, [r5, #368]
	ldr	r3, .L15+8
	mov	r0, r6
	mla	r7, r2, r7, r3
	ldr	r3, .L15+12
	str	r6, [r7, r3]
	bl	strlen
	ldr	r3, .L15+16
	str	r0, [r7, r3]
	mov	r0, r4
	bl	strlen
	subs	r2, r0, #0
	beq	.L13
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L13
	mov	r1, #2
	mov	r3, #1
	stmia	sp, {r1, r3}
	ldr	r0, [r5, #368]
	mov	r1, r4
	mov	r3, #0
	bl	AddCopyOfBlockToXmitList
.L13:
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, pc}
.L16:
	.align	2
.L15:
	.word	comm_mngr
	.word	4280
	.word	SerDrvrVars_s
	.word	4216
	.word	4220
.LFE1:
	.size	RAVEON_string_exchange_with_specific_termination_string_hunt, .-RAVEON_string_exchange_with_specific_termination_string_hunt
	.section	.text.raveon_id_record_change_in_config_file,"ax",%progbits
	.align	2
	.type	raveon_id_record_change_in_config_file, %function
raveon_id_record_change_in_config_file:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L21
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r2, [r3, #0]
	cmp	r2, #1
	bne	.L18
	ldr	r1, .L21+4
	ldrb	ip, [r1, #52]	@ zero_extendqisi2
	mov	r4, ip, lsr #4
	and	r4, r4, #3
	cmp	r4, r0
	ldmeqfd	sp!, {r4, pc}
	and	r0, r0, #3
	bic	ip, ip, #48
	orr	r0, ip, r0, asl #4
	strb	r0, [r1, #52]
	b	.L20
.L18:
	cmp	r2, #2
	ldmnefd	sp!, {r4, pc}
.LBB6:
	ldr	r2, .L21+4
	ldrb	r1, [r2, #53]	@ zero_extendqisi2
	and	ip, r1, #3
	cmp	ip, r0
	ldmeqfd	sp!, {r4, pc}
	and	r0, r0, #3
	bic	r1, r1, #3
	orr	r1, r0, r1
	strb	r1, [r2, #53]
	mov	r2, #1
.L20:
	str	r2, [r3, #8]
	ldmfd	sp!, {r4, pc}
.L22:
	.align	2
.L21:
	.word	.LANCHOR0
	.word	config_c
.LBE6:
.LFE8:
	.size	raveon_id_record_change_in_config_file, .-raveon_id_record_change_in_config_file
	.section	.text.LR_RAVEON_initialize_state_struct,"ax",%progbits
	.align	2
	.global	LR_RAVEON_initialize_state_struct
	.type	LR_RAVEON_initialize_state_struct, %function
LR_RAVEON_initialize_state_struct:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	ldr	r4, .L24
	ldr	r5, .L24+4
	ldr	r3, .L24+8
	mov	r1, r5
	add	r0, r4, #16
	mov	r2, #7
	str	r3, [r4, #12]
	bl	strlcpy
	mov	r1, r5
	add	r0, r4, #23
	mov	r2, #9
	bl	strlcpy
	add	r0, r4, #32
	mov	r1, r5
	mov	r2, #6
	ldmfd	sp!, {r4, r5, lr}
	b	strlcpy
.L25:
	.align	2
.L24:
	.word	.LANCHOR1
	.word	.LC1
	.word	.LANCHOR2
.LFE0:
	.size	LR_RAVEON_initialize_state_struct, .-LR_RAVEON_initialize_state_struct
	.section	.text.LR_RAVEON_initialize_device_exchange,"ax",%progbits
	.align	2
	.global	LR_RAVEON_initialize_device_exchange
	.type	LR_RAVEON_initialize_device_exchange, %function
LR_RAVEON_initialize_device_exchange:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L33
	stmfd	sp!, {r0, r4, lr}
.LCFI4:
	ldr	r2, [r3, #12]
	ldr	r3, .L33+4
	cmp	r2, r3
	beq	.L27
	bl	LR_RAVEON_initialize_state_struct
.L27:
	ldr	r3, .L33+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L28
	ldr	r4, .L33+12
	mov	r1, #0
	ldr	r0, [r4, #0]
	bl	__GENERIC_gr_card_power_control
	mov	r0, #400
	bl	vTaskDelay
	ldr	r0, [r4, #0]
	mov	r1, #1
	bl	__GENERIC_gr_card_power_control
	mov	r0, #600
	bl	vTaskDelay
.L28:
	ldr	r3, .L33+16
	ldr	r4, .L33+20
	ldr	r3, [r3, #372]
	cmp	r3, #87
	bne	.L29
	mov	r3, #11
	str	r3, [r4, #0]
	ldr	r3, .L33+8
	ldr	r3, [r3, #0]
	cmp	r3, #3
	ldreq	r0, .L33+24
	ldreq	r1, .L33+28
	ldrne	r0, .L33+32
	ldrne	r1, .L33+36
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	b	.L31
.L29:
	ldr	r0, .L33+32
	ldr	r1, .L33+36
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	mov	r3, #1
	str	r3, [r4, #0]
.L31:
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L33+16
	mov	r1, #2
	ldr	r0, [r3, #384]
	mov	r2, #600
	mov	r3, #0
	bl	xTimerGenericCommand
	ldmfd	sp!, {r3, r4, pc}
.L34:
	.align	2
.L33:
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	GuiVar_LRRadioType
	.word	GuiVar_LRPort
	.word	comm_mngr
	.word	.LANCHOR3
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
.LFE3:
	.size	LR_RAVEON_initialize_device_exchange, .-LR_RAVEON_initialize_device_exchange
	.section	.text.LR_RAVEON_exchange_processing,"ax",%progbits
	.align	2
	.global	LR_RAVEON_exchange_processing
	.type	LR_RAVEON_exchange_processing, %function
LR_RAVEON_exchange_processing:
.LFB4:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI5:
	ldr	r5, .L90
	sub	sp, sp, #48
.LCFI6:
	ldr	r3, [r5, #372]
	mov	r4, r0
	cmp	r3, #82
	ldr	r3, .L90+4
	bne	.L36
	ldr	r1, [r3, #0]
	mov	r2, r3
	sub	r1, r1, #1
	cmp	r1, #9
	ldrls	pc, [pc, r1, asl #2]
	b	.L37
.L45:
	.word	.L38
	.word	.L37
	.word	.L39
	.word	.L37
	.word	.L37
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
.L38:
	ldr	r2, [r0, #0]
	cmp	r2, #5120
	beq	.L85
.L46:
	mov	r2, #456
	ldr	r0, [r0, #16]
	ldr	r1, .L90+8
	bl	mem_free_debug
	ldr	r0, .L90+12
	ldr	r1, .L90+16
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L90
	mov	r2, #600
	ldr	r0, [r3, #384]
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r2, #3
	b	.L81
.L39:
	ldr	r2, [r0, #0]
	cmp	r2, #5120
	beq	.L85
.L48:
	mov	r2, #0
	mvn	r6, #0
	mov	r3, r2
	str	r6, [sp, #0]
	mov	r1, #1
	ldr	r0, [r5, #384]
	bl	xTimerGenericCommand
	ldr	r0, [r4, #16]
	ldr	r1, .L90+20
	bl	Extract_val_from_msg
	ldr	r2, .L90+24
	ldr	r0, [r4, #16]
	ldr	r1, .L90+8
	bl	mem_free_debug
	ldr	r0, .L90+28
	ldr	r1, .L90+16
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	str	r6, [sp, #0]
	mov	r2, #600
	ldr	r0, [r5, #384]
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r2, #6
	b	.L81
.L40:
	ldr	r2, [r0, #0]
	cmp	r2, #5120
	beq	.L85
.L49:
	mov	r2, #0
	mvn	r6, #0
	mov	r3, r2
	str	r6, [sp, #0]
	mov	r1, #1
	ldr	r0, [r5, #384]
	bl	xTimerGenericCommand
	ldr	r3, .L90+32
	ldr	r0, [r4, #16]
	ldr	r1, [r3, #12]
	bl	Extract_val_from_msg
	ldr	r0, [r4, #16]
	ldr	r1, .L90+8
	mov	r2, #508
	bl	mem_free_debug
	ldr	r3, .L90+36
	ldr	r7, .L90
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L50
	ldr	r0, .L90+40
	ldr	r1, .L90+16
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	str	r6, [sp, #0]
	mov	r2, #600
	ldr	r0, [r7, #384]
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r2, #7
	b	.L81
.L50:
	ldr	r0, .L90+44
	ldr	r1, .L90+16
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	str	r6, [sp, #0]
	ldr	r0, [r7, #384]
	b	.L88
.L41:
	ldr	r1, [r0, #0]
	cmp	r1, #5120
	beq	.L85
.L51:
	ldr	r3, .L90+36
	ldr	r1, [r3, #0]
	cmp	r1, #1
	bne	.L56
	ldr	r5, .L90
	mov	r2, #0
	mvn	r6, #0
	mov	r3, r2
	str	r6, [sp, #0]
	ldr	r0, [r5, #384]
	bl	xTimerGenericCommand
	ldr	r3, .L90+32
	ldr	r0, [r4, #16]
	ldr	r1, [r3, #12]
	add	r1, r1, #20
	bl	Extract_val_from_msg
	ldr	r2, .L90+48
	ldr	r0, [r4, #16]
	ldr	r1, .L90+8
	bl	mem_free_debug
	ldr	r0, .L90+52
	ldr	r1, .L90+16
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	str	r6, [sp, #0]
	mov	r2, #600
	ldr	r0, [r5, #384]
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r2, #8
	b	.L81
.L42:
	ldr	r1, [r0, #0]
	cmp	r1, #5120
	beq	.L85
.L53:
	ldr	r3, .L90+36
	ldr	r1, [r3, #0]
	cmp	r1, #1
	bne	.L56
	ldr	r5, .L90
	mov	r2, #0
	mvn	r6, #0
	mov	r3, r2
	str	r6, [sp, #0]
	ldr	r0, [r5, #384]
	bl	xTimerGenericCommand
	ldr	r0, [r4, #16]
	ldr	r1, .L90+56
	bl	Extract_val_from_msg
	ldr	r2, .L90+60
	ldr	r0, [r4, #16]
	ldr	r1, .L90+8
	bl	mem_free_debug
	ldr	r0, .L90+64
	ldr	r1, .L90+16
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	str	r6, [sp, #0]
	mov	r2, #600
	ldr	r0, [r5, #384]
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r2, #9
	b	.L81
.L43:
	ldr	r1, [r0, #0]
	cmp	r1, #5120
	beq	.L85
.L55:
	ldr	r3, .L90+36
	ldr	r1, [r3, #0]
	cmp	r1, #1
	bne	.L56
	ldr	r5, .L90
	mov	r2, #0
	mvn	r6, #0
	mov	r3, r2
	str	r6, [sp, #0]
	ldr	r0, [r5, #384]
	bl	xTimerGenericCommand
	ldr	r1, .L90+68
	ldr	r0, [r4, #16]
	bl	Extract_val_from_msg
	ldr	r0, .L90+68
	bl	atoi
	ldr	r1, .L90+72
	add	r2, r0, #32
	ldr	r0, .L90+68
	bl	sprintf
	ldr	r0, [r4, #16]
	ldr	r1, .L90+8
	ldr	r2, .L90+76
	bl	mem_free_debug
	ldr	r0, .L90+44
	ldr	r1, .L90+16
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	str	r6, [sp, #0]
	ldr	r0, [r5, #384]
.L88:
	mov	r2, #600
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r2, #10
	b	.L81
.L56:
	mov	r3, #5
	str	r3, [r2, #0]
	b	.L47
.L44:
	ldr	r2, [r0, #0]
	cmp	r2, #5120
	bne	.L57
.L85:
	mov	r2, #5
	str	r2, [r3, #0]
	b	.L47
.L57:
	mov	r2, #0
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #1
	mov	r3, r2
	ldr	r0, [r5, #384]
	bl	xTimerGenericCommand
	ldr	r2, .L90+80
	ldr	r0, [r4, #16]
	ldr	r1, .L90+8
	bl	mem_free_debug
	mov	r2, #5
.L81:
	ldr	r3, .L90+4
	str	r2, [r3, #0]
.L37:
	ldr	r3, .L90+4
	ldr	r3, [r3, #0]
	cmp	r3, #5
	ldreq	r0, .L90+84
	bne	.L35
	b	.L87
.L36:
	ldr	r2, [r3, #0]
	sub	r2, r2, #11
	cmp	r2, #5
	ldrls	pc, [pc, r2, asl #2]
	b	.L59
.L65:
	.word	.L60
	.word	.L61
	.word	.L62
	.word	.L59
	.word	.L63
	.word	.L64
.L60:
	ldr	r2, [r0, #0]
	cmp	r2, #5120
	beq	.L86
.L66:
	mov	r2, #0
	mvn	r6, #0
	mov	r3, r2
	str	r6, [sp, #0]
	mov	r1, #1
	ldr	r0, [r5, #384]
	bl	xTimerGenericCommand
	ldr	r2, .L90+88
	ldr	r0, [r4, #16]
	ldr	r1, .L90+8
	bl	mem_free_debug
	ldr	r3, .L90+36
	ldr	r3, [r3, #0]
	add	r2, r3, r6
	cmp	r2, #1
	bhi	.L68
	ldr	r3, .L90+32
	ldr	r2, .L90+92
	ldr	r3, [r3, #12]
	ldr	r1, .L90+96
	add	r0, sp, #8
	bl	sprintf
	add	r0, sp, #8
	ldr	r1, .L90+100
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	ldr	r3, .L90+4
	mov	r2, #12
	str	r2, [r3, #0]
	str	r6, [sp, #0]
	b	.L84
.L68:
	cmp	r3, #3
	bne	.L69
	ldr	r3, .L90+104
	ldrb	r4, [r3, #0]	@ zero_extendqisi2
	cmp	r4, #0
	bne	.L70
	ldr	r3, .L90+32
	ldr	r0, .L90+108
	ldr	r1, [r3, #12]
	bl	strcpy
	ldr	r0, .L90+108
	bl	strlen
	ldr	r2, .L90+108
	mov	r3, #48
	and	r0, r0, #255
	rsb	r1, r0, #8
	add	r2, r2, r0
	b	.L71
.L72:
	strb	r3, [r2, r4]
	add	r4, r4, #1
	and	r4, r4, #255
.L71:
	cmp	r4, r1
	blt	.L72
	ldr	r3, .L90+108
	add	r0, r3, r0
	mov	r3, #0
	strb	r3, [r0, r4]
.L70:
	ldr	r5, .L90+104
	ldrb	r2, [r5, #0]	@ zero_extendqisi2
	cmp	r2, #63
	bhi	.L59
	ldr	r4, .L90+112
	ldr	r1, .L90+72
	mov	r0, r4
	bl	sprintf
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	cmp	r3, #9
	bhi	.L73
	mov	r3, #0
	strb	r3, [r4, #2]
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	strb	r3, [r4, #1]
	mov	r3, #48
	strb	r3, [r4, #0]
.L73:
	ldr	r3, .L90+108
	ldr	r2, .L90+116
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	ldr	r1, .L90+120
	ldr	r3, .L90+112
	add	r0, sp, #8
	bl	sprintf
	add	r0, sp, #8
	ldr	r1, .L90+124
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	ldr	r2, .L90+104
	ldrb	r3, [r2, #0]	@ zero_extendqisi2
	cmp	r3, #62
	ldrhi	r1, .L90+4
	add	r3, r3, #1
	strb	r3, [r2, #0]
	movhi	r0, #12
	mvn	r3, #0
	strhi	r0, [r1, #0]
	str	r3, [sp, #0]
.L84:
	ldr	r3, .L90
	mov	r1, #2
	ldr	r0, [r3, #384]
	mov	r2, #600
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L59
.L69:
	ldr	r0, .L90+128
	bl	Alert_Message
	b	.L59
.L61:
	ldr	r2, [r0, #0]
	cmp	r2, #5120
	beq	.L86
.L75:
	mov	r2, #0
	mvn	r7, #0
	mov	r3, r2
	str	r7, [sp, #0]
	mov	r1, #1
	ldr	r0, [r5, #384]
	bl	xTimerGenericCommand
	ldr	r0, [r4, #16]
	ldr	r1, .L90+8
	ldr	r2, .L90+132
	bl	mem_free_debug
	ldr	r3, .L90+36
	ldr	r6, .L90
	ldr	r3, [r3, #0]
	cmp	r3, #3
	bne	.L76
	ldr	r0, .L90+136
	bl	strlen
	mov	r3, #1
	ldr	r1, .L90+136
	mov	r2, r0
	mov	r0, #2
	stmia	sp, {r0, r3}
	ldr	r0, [r6, #368]
	mov	r3, #0
	bl	AddCopyOfBlockToXmitList
	b	.L82
.L76:
	ldr	r3, .L90+32
	ldr	r2, .L90+140
	ldr	r3, [r3, #12]
	ldr	r1, .L90+96
	add	r3, r3, #10
	add	r0, sp, #8
	bl	sprintf
	add	r0, sp, #8
	ldr	r1, .L90+16
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	str	r7, [sp, #0]
	mov	r2, #600
	ldr	r0, [r6, #384]
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r2, #13
	b	.L83
.L62:
	ldr	r2, [r0, #0]
	cmp	r2, #5120
	beq	.L86
.L77:
	mov	r2, #0
	mvn	r6, #0
	mov	r3, r2
	str	r6, [sp, #0]
	mov	r1, #1
	ldr	r0, [r5, #384]
	bl	xTimerGenericCommand
	ldr	r0, [r4, #16]
	ldr	r1, .L90+8
	ldr	r2, .L90+144
	bl	mem_free_debug
	ldr	r3, .L90+36
	ldr	r7, .L90
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L78
	ldr	r3, .L90+32
	ldr	r2, .L90+148
	ldr	r3, [r3, #12]
	ldr	r1, .L90+96
	add	r3, r3, #20
	add	r0, sp, #8
	bl	sprintf
	add	r0, sp, #8
	ldr	r1, .L90+16
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	str	r6, [sp, #0]
	mov	r2, #600
	ldr	r0, [r7, #384]
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r2, #15
	b	.L83
.L78:
	ldr	r0, .L90+152
	ldr	r1, .L90+16
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	str	r6, [sp, #0]
	ldr	r0, [r7, #384]
	b	.L89
.L63:
	ldr	r2, [r0, #0]
	cmp	r2, #5120
	beq	.L86
.L79:
	ldr	r3, .L90+36
	ldr	r1, [r3, #0]
	cmp	r1, #1
	bne	.L59
	ldr	r5, .L90
	mov	r2, #0
	mvn	r6, #0
	mov	r3, r2
	str	r6, [sp, #0]
	ldr	r0, [r5, #384]
	bl	xTimerGenericCommand
	ldr	r0, [r4, #16]
	ldr	r1, .L90+8
	ldr	r2, .L90+156
	bl	mem_free_debug
	ldr	r0, .L90+152
	ldr	r1, .L90+16
	bl	RAVEON_string_exchange_with_specific_termination_string_hunt
	str	r6, [sp, #0]
	ldr	r0, [r5, #384]
.L89:
	mov	r2, #600
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r2, #16
	b	.L83
.L64:
	ldr	r2, [r0, #0]
	cmp	r2, #5120
	bne	.L80
.L86:
	mov	r2, #14
	str	r2, [r3, #0]
	ldr	r3, .L90+104
	mov	r2, #0
	strb	r2, [r3, #0]
	ldr	r0, .L90+160
	b	.L87
.L80:
	mov	r2, #0
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #1
	mov	r3, r2
	ldr	r0, [r5, #384]
	bl	xTimerGenericCommand
	ldr	r0, [r4, #16]
	ldr	r1, .L90+8
	ldr	r2, .L90+164
	bl	mem_free_debug
.L82:
	mov	r2, #14
.L83:
	ldr	r3, .L90+4
	str	r2, [r3, #0]
.L59:
	ldr	r3, .L90+4
	ldr	r3, [r3, #0]
	cmp	r3, #14
	bne	.L35
	ldr	r3, .L90+104
	mov	r2, #0
	strb	r2, [r3, #0]
	ldr	r0, .L90+168
	b	.L87
.L47:
	ldr	r0, .L90+172
.L87:
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
.L35:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L91:
	.align	2
.L90:
	.word	comm_mngr
	.word	.LANCHOR3
	.word	.LC6
	.word	.LC7
	.word	.LC5
	.word	.LANCHOR1+23
	.word	482
	.word	.LC8
	.word	.LANCHOR1
	.word	GuiVar_LRRadioType
	.word	.LC9
	.word	.LC10
	.word	547
	.word	.LC11
	.word	.LANCHOR1+16
	.word	582
	.word	.LC12
	.word	.LANCHOR1+38
	.word	.LC13
	.word	622
	.word	653
	.word	36865
	.word	701
	.word	.LC15
	.word	.LC14
	.word	.LC16
	.word	.LANCHOR4
	.word	.LANCHOR5
	.word	.LANCHOR6
	.word	.LC18
	.word	.LC17
	.word	.LC3
	.word	.LC19
	.word	765
	.word	.LC20
	.word	.LC21
	.word	807
	.word	.LC22
	.word	.LC23
	.word	845
	.word	36869
	.word	869
	.word	36868
	.word	36866
.LFE4:
	.size	LR_RAVEON_exchange_processing, .-LR_RAVEON_exchange_processing
	.section	.text.LR_RAVEON_PROGRAMMING_extract_changes_from_guivars,"ax",%progbits
	.align	2
	.global	LR_RAVEON_PROGRAMMING_extract_changes_from_guivars
	.type	LR_RAVEON_PROGRAMMING_extract_changes_from_guivars, %function
LR_RAVEON_PROGRAMMING_extract_changes_from_guivars:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L97
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI7:
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L93
	ldr	r3, .L97+4
	mov	ip, #10
	ldr	r0, [r3, #12]
	ldr	r3, .L97+8
	add	r0, r0, #20
	ldr	r3, [r3, #0]
	mov	r1, #5
	ldr	r2, .L97+12
	mul	r3, ip, r3
	bl	snprintf
.L93:
	ldr	r6, .L97+16
	ldr	r5, .L97+4
	ldr	r3, [r6, #0]
	ldr	r4, .L97+20
	cmp	r3, #1000
	str	r3, [sp, #0]
	ldr	r0, [r5, #12]
	mov	r1, #9
	bcs	.L94
	ldr	r2, .L97+24
	ldr	r3, [r4, #0]
	bl	snprintf
	ldr	r0, [r5, #12]
	ldr	r3, [r6, #0]
	add	r0, r0, #10
	str	r3, [sp, #0]
	mov	r1, #9
	ldr	r2, .L97+24
	b	.L96
.L94:
	ldr	r2, .L97+28
	ldr	r3, [r4, #0]
	bl	snprintf
	ldr	r0, [r5, #12]
	ldr	r3, [r6, #0]
	ldr	r2, .L97+28
	add	r0, r0, #10
	mov	r1, #9
	str	r3, [sp, #0]
.L96:
	ldr	r3, [r4, #0]
	bl	snprintf
	ldmfd	sp!, {r3, r4, r5, r6, pc}
.L98:
	.align	2
.L97:
	.word	GuiVar_LRRadioType
	.word	.LANCHOR1
	.word	GuiVar_LRTransmitPower
	.word	.LC24
	.word	GuiVar_LRFrequency_Decimal
	.word	GuiVar_LRFrequency_WholeNum
	.word	.LC25
	.word	.LC26
.LFE5:
	.size	LR_RAVEON_PROGRAMMING_extract_changes_from_guivars, .-LR_RAVEON_PROGRAMMING_extract_changes_from_guivars
	.global	__umodsi3
	.global	__udivsi3
	.section	.text.LR_RAVEON_PROGRAMMING_copy_settings_into_guivars,"ax",%progbits
	.align	2
	.global	LR_RAVEON_PROGRAMMING_copy_settings_into_guivars
	.type	LR_RAVEON_PROGRAMMING_copy_settings_into_guivars, %function
LR_RAVEON_PROGRAMMING_copy_settings_into_guivars:
.LFB6:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI8:
	ldr	r4, .L107
	sub	sp, sp, #24
.LCFI9:
	add	r1, r4, #23
	mov	r2, #9
	ldr	r0, .L107+4
	bl	strlcpy
	ldr	r3, [r4, #12]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	strb	r2, [sp, #0]
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	strb	r2, [sp, #1]
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	strb	r2, [sp, #2]
	mov	r2, #0
	strb	r2, [sp, #3]
	ldrb	r1, [r3, #3]	@ zero_extendqisi2
	cmp	r1, #46
	bne	.L100
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	strb	r1, [sp, #12]
	ldrb	r1, [r3, #5]	@ zero_extendqisi2
	strb	r1, [sp, #13]
	ldrb	r1, [r3, #6]	@ zero_extendqisi2
	strb	r1, [sp, #14]
	ldrb	r3, [r3, #7]	@ zero_extendqisi2
	b	.L105
.L100:
	strb	r1, [sp, #12]
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	strb	r1, [sp, #13]
	ldrb	r1, [r3, #5]	@ zero_extendqisi2
	strb	r1, [sp, #14]
	ldrb	r3, [r3, #6]	@ zero_extendqisi2
.L105:
	mov	r0, sp
	strb	r2, [sp, #16]
	strb	r3, [sp, #15]
	bl	atoi
	ldr	r3, .L107+8
	ldr	r4, .L107+12
	str	r0, [r3, #0]
	add	r0, sp, #12
	bl	atoi
	mov	r1, #125
	str	r0, [r4, #0]
	mov	r5, r0
	bl	__umodsi3
	ldr	r3, .L107+16
	ldr	r3, [r3, #0]
	cmp	r0, #0
	addne	r5, r5, #125
	rsbne	r5, r0, r5
	strne	r5, [r4, #0]
	cmp	r3, #1
	bne	.L103
	ldr	r4, .L107
	ldr	r0, [r4, #12]
	add	r0, r0, #20
	bl	atoi
	mov	r1, #10
	bl	__udivsi3
	ldr	r3, .L107+20
	mov	r2, #3
	add	r1, r4, #16
	str	r0, [r3, #0]
	ldr	r0, .L107+24
	bl	strlcpy
	add	r0, r4, #38
	bl	atoi
	ldr	r3, .L107+28
	mov	r2, #0
	str	r0, [r3, #0]
	b	.L106
.L103:
	ldr	r3, .L107+32
	ldrb	r3, [r3, #52]	@ zero_extendqisi2
	tst	r3, #8
	beq	.L99
	mov	r2, #1
.L106:
	ldr	r3, .L107+36
	strb	r2, [r3, #0]
.L99:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, pc}
.L108:
	.align	2
.L107:
	.word	.LANCHOR1
	.word	GuiVar_LRSerialNumber
	.word	GuiVar_LRFrequency_WholeNum
	.word	GuiVar_LRFrequency_Decimal
	.word	GuiVar_LRRadioType
	.word	GuiVar_LRTransmitPower
	.word	GuiVar_LRFirmwareVer
	.word	GuiVar_LRTemperature
	.word	config_c
	.word	GuiVar_LRHubFeatureNotAvailable
.LFE6:
	.size	LR_RAVEON_PROGRAMMING_copy_settings_into_guivars, .-LR_RAVEON_PROGRAMMING_copy_settings_into_guivars
	.section	.text.RAVEON_id_start_the_process,"ax",%progbits
	.align	2
	.global	RAVEON_id_start_the_process
	.type	RAVEON_id_start_the_process, %function
RAVEON_id_start_the_process:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L113
	mov	r2, #1
	stmfd	sp!, {r0, r4, lr}
.LCFI10:
	str	r2, [r3, #0]
	ldr	r3, .L113+4
	mov	r2, #0
	str	r2, [r3, #8]
	ldr	r2, .L113+8
	mov	r4, r3
	ldr	r1, [r2, #80]
	cmp	r1, #1
	streq	r1, [r3, #0]
	beq	.L111
	ldr	r2, [r2, #84]
	cmp	r2, #1
	moveq	r3, #2
	streq	r3, [r4, #0]
	beq	.L111
	mov	r2, #5
	mov	r0, #121
	str	r2, [r3, #4]
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	CONTROLLER_INITIATED_post_event
.L111:
	ldr	r0, [r4, #0]
	bl	power_down_device
	mov	r3, #1
	str	r3, [r4, #4]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L113+12
	mov	r1, #2
	ldr	r0, [r3, #40]
	mov	r2, #600
	mov	r3, #0
	bl	xTimerGenericCommand
	add	sp, sp, #4
	ldmfd	sp!, {r4, pc}
.L114:
	.align	2
.L113:
	.word	GuiVar_LRRadioType
	.word	.LANCHOR0
	.word	config_c
	.word	cics
.LFE9:
	.size	RAVEON_id_start_the_process, .-RAVEON_id_start_the_process
	.section	.text.RAVEON_id_event_processing,"ax",%progbits
	.align	2
	.global	RAVEON_id_event_processing
	.type	RAVEON_id_event_processing, %function
RAVEON_id_event_processing:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #0]
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI11:
	ldr	r6, .L139
	sub	r3, r3, #121
	cmp	r3, #1
	mov	r4, r0
	bls	.L116
	ldr	r0, .L139+4
	bl	Alert_Message
	mov	r3, #5
	str	r3, [r6, #4]
	b	.L138
.L116:
	ldr	r3, [r6, #4]
	sub	r3, r3, #1
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L115
.L123:
	.word	.L118
	.word	.L119
	.word	.L120
	.word	.L121
	.word	.L122
.L118:
	ldr	r0, [r6, #0]
	bl	power_up_device
	mvn	r3, #0
	mov	r1, #2
	str	r1, [r6, #4]
	str	r3, [sp, #0]
	ldr	r3, .L139+8
	ldr	r0, [r3, #40]
	b	.L136
.L119:
.LBB7:
.LBB8:
	ldr	r0, [r6, #0]
	mov	r1, #400
	mov	r2, #200
	bl	RCVD_DATA_enable_hunting_mode
	ldr	r2, [r6, #0]
	ldr	r1, .L139+12
	ldr	r3, .L139+16
	mov	r4, #0
	mla	r2, r1, r2, r3
	ldr	r3, .L139+20
	ldr	r0, .L139+24
	str	r4, [r2, r3]
	bl	strlen
	subs	r2, r0, #0
	beq	.L124
	ldr	r3, .L139
	mov	lr, #1
	ldr	r0, [r3, #0]
	mov	r3, #2
	stmia	sp, {r3, lr}
	ldr	r1, .L139+24
	mov	r3, r4
	bl	AddCopyOfBlockToXmitList
.L124:
.LBE8:
.LBE7:
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L139+8
	mov	r2, #400
	ldr	r0, [r3, #40]
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	ldr	r3, .L139
	mov	r2, #3
	str	r2, [r3, #4]
	b	.L115
.L120:
	ldr	r0, [r6, #0]
	mov	r1, #100
	mov	r2, #0
	bl	RCVD_DATA_enable_hunting_mode
	ldr	r3, [r4, #0]
	ldr	r5, .L139
	cmp	r3, #121
	bne	.L125
	ldr	r2, [r5, #0]
	ldr	r3, .L139+28
	ldr	r0, .L139+32
	ldr	r1, [r3, r2, asl #2]
	bl	Alert_Message_va
	ldr	r3, .L139+36
	mov	r0, #3
	str	r0, [r3, #0]
	bl	raveon_id_record_change_in_config_file
	b	.L126
.L125:
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L139+8
	mov	r2, #0
	ldr	r0, [r3, #40]
	mov	r1, #1
	mov	r3, r2
	bl	xTimerGenericCommand
	ldr	r0, [r4, #12]
	ldr	r1, .L139+40
	bl	strstr
	cmp	r0, #0
	beq	.L127
	ldr	r2, [r5, #0]
	ldr	r3, .L139+28
	ldr	r0, .L139+44
	ldr	r1, [r3, r2, asl #2]
	bl	Alert_Message_va
	mov	r0, #1
	b	.L135
.L127:
	ldr	r0, [r4, #12]
	ldr	r1, .L139+48
	bl	strstr
	cmp	r0, #0
	beq	.L129
	ldr	r2, [r5, #0]
	ldr	r3, .L139+28
	ldr	r0, .L139+52
	ldr	r1, [r3, r2, asl #2]
	bl	Alert_Message_va
	mov	r0, #2
.L135:
	ldr	r3, .L139+36
	str	r0, [r3, #0]
	bl	raveon_id_record_change_in_config_file
	b	.L128
.L129:
	ldr	r0, .L139+56
	bl	Alert_Message
.L128:
	ldr	r0, [r4, #12]
	ldr	r1, .L139+60
	ldr	r2, .L139+64
	bl	mem_free_debug
.L126:
	ldr	r3, .L139+36
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r3, #1
	bhi	.L130
	ldr	r0, .L139+68
	bl	strlen
	mov	r3, #2
	mov	ip, #1
	stmia	sp, {r3, ip}
	ldr	r3, .L139
	ldr	r1, .L139+68
	mov	r2, r0
	ldr	r0, [r3, #0]
	mov	r3, #0
	bl	AddCopyOfBlockToXmitList
	mov	r0, #50
	bl	vTaskDelay
.L130:
	ldr	r3, .L139
	ldr	r2, [r3, #0]
	cmp	r2, #1
	movne	r2, #5
	moveq	r2, #4
	str	r2, [r3, #4]
.L138:
	mov	r0, #121
	b	.L137
.L121:
	ldr	r3, .L139+72
	ldr	r4, .L139
	ldr	r6, [r3, #84]
	cmp	r6, #1
	movne	r3, #5
	strne	r3, [r4, #4]
	bne	.L138
	mov	r5, #2
	ldr	r0, .L139+76
	bl	Alert_Message
	mov	r0, r5
	str	r5, [r4, #0]
	bl	power_down_device
	mvn	r3, #0
	str	r6, [r4, #4]
	str	r3, [sp, #0]
	ldr	r3, .L139+8
	mov	r1, r5
	ldr	r0, [r3, #40]
.L136:
	mov	r2, #600
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L115
.L122:
	ldr	r3, [r6, #8]
	cmp	r3, #0
	beq	.L133
	bl	CONTROLLER_INITIATED_update_comm_server_registration_info
	mov	r0, #0
	mov	r1, #2
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L133:
	mov	r0, #5376
	bl	COMM_MNGR_post_event
	ldr	r3, .L139+8
	mov	r2, #1
	mov	r0, #120
	str	r2, [r3, #0]
.L137:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	CONTROLLER_INITIATED_post_event
.L115:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, pc}
.L140:
	.align	2
.L139:
	.word	.LANCHOR0
	.word	.LC27
	.word	cics
	.word	4280
	.word	SerDrvrVars_s
	.word	4208
	.word	.LC4
	.word	port_names
	.word	.LC28
	.word	GuiVar_LRRadioType
	.word	.LC29
	.word	.LC30
	.word	.LC31
	.word	.LC32
	.word	.LC33
	.word	.LC6
	.word	1266
	.word	.LC10
	.word	config_c
	.word	.LC34
.LFE10:
	.size	RAVEON_id_event_processing, .-RAVEON_id_event_processing
	.section	.text.RAVEON_is_connected,"ax",%progbits
	.align	2
	.global	RAVEON_is_connected
	.type	RAVEON_is_connected, %function
RAVEON_is_connected:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #1
	bx	lr
.LFE11:
	.size	RAVEON_is_connected, .-RAVEON_is_connected
	.section	.text.RAVEON_initialize_the_connection_process,"ax",%progbits
	.align	2
	.global	RAVEON_initialize_the_connection_process
	.type	RAVEON_initialize_the_connection_process, %function
RAVEON_initialize_the_connection_process:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI12:
	mov	r4, r0
	beq	.L143
	ldr	r0, .L146
	bl	Alert_Message
.L143:
	ldr	r3, .L146+4
	ldr	r2, .L146+8
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #44]
	cmp	r3, #0
	bne	.L144
	ldr	r0, .L146+12
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_Message
.L144:
	ldr	r5, .L146+16
	mov	r0, r4
	str	r4, [r5, #4]
	bl	GENERIC_GR_card_power_is_on
	cmp	r0, #0
	bne	.L145
	ldr	r0, .L146+20
	bl	Alert_Message
	ldr	r0, [r5, #4]
	bl	power_up_device
.L145:
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L146+24
	mov	r2, #600
	ldr	r0, [r3, #40]
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
	ldr	r3, .L146+16
	mov	r2, #6
	str	r2, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, pc}
.L147:
	.align	2
.L146:
	.word	.LC35
	.word	config_c
	.word	port_device_table
	.word	.LC36
	.word	.LANCHOR7
	.word	.LC37
	.word	cics
.LFE12:
	.size	RAVEON_initialize_the_connection_process, .-RAVEON_initialize_the_connection_process
	.section	.text.RAVEON_connection_processing,"ax",%progbits
	.align	2
	.global	RAVEON_connection_processing
	.type	RAVEON_connection_processing, %function
RAVEON_connection_processing:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	r0, r0, #121
	cmp	r0, #1
	str	lr, [sp, #-4]!
.LCFI13:
	bls	.L149
	ldr	r0, .L153
	bl	Alert_Message
	b	.L150
.L149:
	ldr	r2, .L153+4
	ldr	r3, [r2, #0]
	cmp	r3, #6
	ldrne	pc, [sp], #4
	ldr	r3, .L153+8
	ldr	r1, .L153+12
	ldr	r3, [r3, #80]
	mov	r0, #56
	mla	r3, r0, r3, r1
	ldr	r3, [r3, #44]
	cmp	r3, #0
	beq	.L150
	ldr	r0, [r2, #4]
	blx	r3
	cmp	r0, #0
	ldreq	pc, [sp], #4
	ldr	r1, .L153+16
	mov	r2, #49
	ldr	r0, .L153+20
	bl	strlcpy
	ldr	r0, .L153+24
	bl	Alert_Message
	ldr	lr, [sp], #4
	b	CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities
.L150:
	ldr	r3, .L153+28
	mov	r2, #1
	mov	r0, #123
	str	r2, [r3, #0]
	ldr	lr, [sp], #4
	b	CONTROLLER_INITIATED_post_event
.L154:
	.align	2
.L153:
	.word	.LC38
	.word	.LANCHOR7
	.word	config_c
	.word	port_device_table
	.word	.LC39
	.word	GuiVar_CommTestStatus
	.word	.LC40
	.word	cics
.LFE13:
	.size	RAVEON_connection_processing, .-RAVEON_connection_processing
	.global	LR_RAVEON_PROGRAMMING_querying_device
	.section	.bss.v_lrs_raveon_struct,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	v_lrs_raveon_struct, %object
	.size	v_lrs_raveon_struct, 28
v_lrs_raveon_struct:
	.space	28
	.section	.bss.chnlIndx.8081,"aw",%nobits
	.set	.LANCHOR4,. + 0
	.type	chnlIndx.8081, %object
	.size	chnlIndx.8081, 1
chnlIndx.8081:
	.space	1
	.section	.bss.LR_RAVEON_cs,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	LR_RAVEON_cs, %object
	.size	LR_RAVEON_cs, 12
LR_RAVEON_cs:
	.space	12
	.section	.bss.LR_RAVEON_PROGRAMMING_querying_device,"aw",%nobits
	.align	2
	.type	LR_RAVEON_PROGRAMMING_querying_device, %object
	.size	LR_RAVEON_PROGRAMMING_querying_device, 4
LR_RAVEON_PROGRAMMING_querying_device:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Radio response parsing error in function LR_RAVEON_"
	.ascii	"exchange_processing\000"
.LC1:
	.ascii	"\000"
.LC2:
	.ascii	"...//\000"
.LC3:
	.ascii	"OK>\000"
.LC4:
	.ascii	"+++\000"
.LC5:
	.ascii	"OK\015\012\000"
.LC6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_LR_RAVEON.c\000"
.LC7:
	.ascii	"ATSL\015\000"
.LC8:
	.ascii	"ATFT\015\000"
.LC9:
	.ascii	"ATPO\015\000"
.LC10:
	.ascii	"EXIT\015\000"
.LC11:
	.ascii	"ATVR\015\000"
.LC12:
	.ascii	"ATTE\015\000"
.LC13:
	.ascii	"%d\000"
.LC14:
	.ascii	"%s%s\015\000"
.LC15:
	.ascii	"ATFT \000"
.LC16:
	.ascii	"\015\012OK\015\012\000"
.LC17:
	.ascii	"%s%sR%sT%s\015\000"
.LC18:
	.ascii	".FRQ\000"
.LC19:
	.ascii	"Unknown Response from the Radio\000"
.LC20:
	.ascii	"EXIT\000"
.LC21:
	.ascii	"ATFR \000"
.LC22:
	.ascii	"ATPO \000"
.LC23:
	.ascii	"ATSV\015\000"
.LC24:
	.ascii	"%2d\000"
.LC25:
	.ascii	"%d.0%d\000"
.LC26:
	.ascii	"%d.%d\000"
.LC27:
	.ascii	"Raveon ID Process : UNXEXP EVENT\000"
.LC28:
	.ascii	"SONIK or unknown radio found on %s\000"
.LC29:
	.ascii	"RV-M7\000"
.LC30:
	.ascii	"M7 radio found on %s\000"
.LC31:
	.ascii	"FireLine\000"
.LC32:
	.ascii	"M5 radio found on %s\000"
.LC33:
	.ascii	"Raveon response neither M7 or M5\000"
.LC34:
	.ascii	"Unexpectedly TWO LR radios detected\000"
.LC35:
	.ascii	"Connecting LR device on PORT_B?\000"
.LC36:
	.ascii	"Unexpected NULL is_connected function\000"
.LC37:
	.ascii	"LR: expected to be powered?\000"
.LC38:
	.ascii	"Connection Process : UNXEXP EVENT\000"
.LC39:
	.ascii	"LR Radio Connection\000"
.LC40:
	.ascii	"Raveon LR Connected\000"
	.section	.bss.rccs,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	rccs, %object
	.size	rccs, 8
rccs:
	.space	8
	.section	.bss.chnlString.8082,"aw",%nobits
	.set	.LANCHOR6,. + 0
	.type	chnlString.8082, %object
	.size	chnlString.8082, 3
chnlString.8082:
	.space	3
	.section	.bss.rid_cs,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	rid_cs, %object
	.size	rid_cs, 12
rid_cs:
	.space	12
	.section	.bss.asciFreq.8083,"aw",%nobits
	.set	.LANCHOR5,. + 0
	.type	asciFreq.8083, %object
	.size	asciFreq.8083, 10
asciFreq.8083:
	.space	10
	.section	.bss.state_raveon_struct,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	state_raveon_struct, %object
	.size	state_raveon_struct, 48
state_raveon_struct:
	.space	48
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI2-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI3-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI5-.LFB4
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI7-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI8-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI10-.LFB9
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI11-.LFB10
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI12-.LFB12
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI13-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE24:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_LR_RAVEON.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x139
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF14
	.byte	0x1
	.4byte	.LASF15
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x410
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x3f3
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x13a
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x10d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xf6
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x160
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1a7
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x387
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x3a7
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x42f
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x480
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST9
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x575
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x591
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST10
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x5d4
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST11
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB8
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB0
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI6
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB5
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB6
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI9
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB12
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB13
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x7c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"Extract_val_from_msg\000"
.LASF13:
	.ascii	"RAVEON_connection_processing\000"
.LASF0:
	.ascii	"raveon_id_record_change_in_config_file\000"
.LASF15:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_LR_RAVEON.c\000"
.LASF7:
	.ascii	"LR_RAVEON_PROGRAMMING_extract_changes_from_guivars\000"
.LASF14:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"RAVEON_string_exchange_with_specific_termination_st"
	.ascii	"ring_hunt\000"
.LASF9:
	.ascii	"RAVEON_id_start_the_process\000"
.LASF11:
	.ascii	"RAVEON_is_connected\000"
.LASF6:
	.ascii	"LR_RAVEON_exchange_processing\000"
.LASF12:
	.ascii	"RAVEON_initialize_the_connection_process\000"
.LASF4:
	.ascii	"LR_RAVEON_initialize_state_struct\000"
.LASF8:
	.ascii	"LR_RAVEON_PROGRAMMING_copy_settings_into_guivars\000"
.LASF10:
	.ascii	"RAVEON_id_event_processing\000"
.LASF5:
	.ascii	"LR_RAVEON_initialize_device_exchange\000"
.LASF1:
	.ascii	"raveon_id_setup_string_hunt\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
