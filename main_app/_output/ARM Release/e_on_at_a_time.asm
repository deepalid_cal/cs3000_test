	.file	"e_on_at_a_time.c"
	.text
.Ltext0:
	.section	.text.ON_AT_A_TIME_get_max_allowed_on,"ax",%progbits
	.align	2
	.type	ON_AT_A_TIME_get_max_allowed_on, %function
ON_AT_A_TIME_get_max_allowed_on:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldr	r4, .L5
	ldr	r0, .L5+4
	mov	r1, #80
	bl	COMM_MNGR_alert_if_chain_members_should_not_be_referenced
	ldr	r2, .L5+4
	mov	r3, #82
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L5+8
	mov	r5, #0
	add	r2, r3, #1104
.L3:
	ldr	r1, [r3, #16]
	add	r3, r3, #92
	cmp	r1, #0
	addne	r5, r5, #6
	cmp	r3, r2
	bne	.L3
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r5, #30
	movcc	r0, r5
	movcs	r0, #30
	ldmfd	sp!, {r4, r5, pc}
.L6:
	.align	2
.L5:
	.word	chain_members_recursive_MUTEX
	.word	.LC0
	.word	chain
.LFE0:
	.size	ON_AT_A_TIME_get_max_allowed_on, .-ON_AT_A_TIME_get_max_allowed_on
	.section	.text.ON_AT_A_TIME_process_in_group,"ax",%progbits
	.align	2
	.type	ON_AT_A_TIME_process_in_group, %function
ON_AT_A_TIME_process_in_group:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	bl	ON_AT_A_TIME_get_max_allowed_on
	cmp	r4, #84
	ldr	r3, [r5, #0]
	mov	r7, r0
	bne	.L8
	cmn	r3, #-2147483647
	beq	.L13
.L9:
	cmp	r3, r0
	bne	.L11
	bl	good_key_beep
	mvn	r3, #-2147483648
	b	.L15
.L11:
	bcs	.L10
	bl	good_key_beep
	ldr	r3, [r5, #0]
	add	r3, r3, #1
	b	.L15
.L8:
	cmn	r3, #-2147483647
	bne	.L12
	bl	good_key_beep
	str	r7, [r5, #0]
	b	.L10
.L12:
	cmp	r3, #1
	bls	.L13
	bl	good_key_beep
	ldr	r3, [r5, #0]
	sub	r3, r3, #1
.L15:
	str	r3, [r5, #0]
	b	.L10
.L13:
	bl	bad_key_beep
.L10:
	ldr	r3, [r5, #0]
	cmn	r3, #-2147483647
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	ldr	r2, [r6, #0]
	cmp	r3, r2
	strhi	r3, [r6, #0]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE1:
	.size	ON_AT_A_TIME_process_in_group, .-ON_AT_A_TIME_process_in_group
	.section	.text.ON_AT_A_TIME_process_in_system,"ax",%progbits
	.align	2
	.type	ON_AT_A_TIME_process_in_system, %function
ON_AT_A_TIME_process_in_system:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI2:
	mov	r4, r0
	mov	r6, r2
	mov	r5, r1
	bl	ON_AT_A_TIME_get_max_allowed_on
	cmp	r4, #84
	ldr	r3, [r6, #0]
	mov	r7, r0
	bne	.L17
	cmn	r3, #-2147483647
	beq	.L24
.L18:
	cmp	r3, r0
	beq	.L27
.L20:
	bcs	.L24
	bl	good_key_beep
	ldr	r3, [r6, #0]
	add	r3, r3, #1
	b	.L26
.L17:
	cmn	r3, #-2147483647
	bne	.L22
	bl	good_key_beep
	str	r7, [r6, #0]
	b	.L19
.L22:
	cmp	r3, r0
	bls	.L23
.L27:
	bl	good_key_beep
	mvn	r3, #-2147483648
	b	.L26
.L23:
	cmp	r3, #1
	bls	.L24
	bl	good_key_beep
	ldr	r3, [r6, #0]
	sub	r3, r3, #1
.L26:
	str	r3, [r6, #0]
	b	.L19
.L24:
	bl	bad_key_beep
.L19:
	ldr	r3, [r6, #0]
	cmn	r3, #-2147483647
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	ldr	r2, [r5, #0]
	cmn	r2, #-2147483647
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	cmp	r2, r3
	strhi	r3, [r5, #0]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE2:
	.size	ON_AT_A_TIME_process_in_system, .-ON_AT_A_TIME_process_in_system
	.section	.text.FDTO_ON_AT_A_TIME_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_ON_AT_A_TIME_draw_screen
	.type	FDTO_ON_AT_A_TIME_draw_screen, %function
FDTO_ON_AT_A_TIME_draw_screen:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L51
	str	lr, [sp, #-4]!
.LCFI3:
	ldrnesh	r1, [r3, #0]
	bne	.L30
	bl	ON_AT_A_TIME_copy_group_into_guivars
	mov	r1, #0
.L30:
.LBB4:
	ldr	r3, .L51+4
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+8
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+12
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+16
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+20
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+24
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+28
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+32
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+36
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+40
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+44
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+48
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+52
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+56
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+60
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+64
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+68
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+72
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+76
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	beq	.L50
	ldr	r3, .L51+80
	ldr	r3, [r3, #0]
	cmn	r3, #-2147483647
	movne	r3, #0
	moveq	r3, #1
	b	.L31
.L50:
	mov	r3, #1
.L31:
	ldr	r2, .L51+84
.LBE4:
	mov	r0, #46
.LBB5:
	strb	r3, [r2, #0]
.LBE5:
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L52:
	.align	2
.L51:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSettingB_9
	.word	GuiVar_OnAtATimeInfoDisplay
.LFE4:
	.size	FDTO_ON_AT_A_TIME_draw_screen, .-FDTO_ON_AT_A_TIME_draw_screen
	.global	__modsi3
	.section	.text.ON_AT_A_TIME_process_screen,"ax",%progbits
	.align	2
	.global	ON_AT_A_TIME_process_screen
	.type	ON_AT_A_TIME_process_screen, %function
ON_AT_A_TIME_process_screen:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r4, r5, lr}
.LCFI4:
	mov	r4, r0
	mov	r5, r1
	beq	.L58
	bhi	.L61
	cmp	r0, #1
	beq	.L56
	bcc	.L55
	cmp	r0, #3
	beq	.L57
	cmp	r0, #4
	bne	.L54
	b	.L58
.L61:
	cmp	r0, #67
	beq	.L59
	bhi	.L62
	cmp	r0, #20
	bne	.L54
	b	.L55
.L62:
	cmp	r0, #80
	beq	.L60
	cmp	r0, #84
	bne	.L54
.L60:
	ldr	r3, .L98
	ldrsh	r3, [r3, #0]
	cmp	r3, #19
	ldrls	pc, [pc, r3, asl #2]
	b	.L63
.L84:
	.word	.L64
	.word	.L65
	.word	.L66
	.word	.L67
	.word	.L68
	.word	.L69
	.word	.L70
	.word	.L71
	.word	.L72
	.word	.L73
	.word	.L74
	.word	.L75
	.word	.L76
	.word	.L77
	.word	.L78
	.word	.L79
	.word	.L80
	.word	.L81
	.word	.L82
	.word	.L83
.L64:
	mov	r0, r4
	ldr	r1, .L98+4
	ldr	r2, .L98+8
	b	.L92
.L65:
	ldr	r1, .L98+12
	ldr	r2, .L98+16
	mov	r0, r4
.L92:
	bl	ON_AT_A_TIME_process_in_group
	b	.L85
.L66:
	mov	r0, r4
	ldr	r1, .L98+20
	ldr	r2, .L98+24
	b	.L92
.L67:
	mov	r0, r4
	ldr	r1, .L98+28
	ldr	r2, .L98+32
	b	.L92
.L68:
	mov	r0, r4
	ldr	r1, .L98+36
	ldr	r2, .L98+40
	b	.L92
.L69:
	mov	r0, r4
	ldr	r1, .L98+44
	ldr	r2, .L98+48
	b	.L92
.L70:
	mov	r0, r4
	ldr	r1, .L98+52
	ldr	r2, .L98+56
	b	.L92
.L71:
	mov	r0, r4
	ldr	r1, .L98+60
	ldr	r2, .L98+64
	b	.L92
.L72:
	mov	r0, r4
	ldr	r1, .L98+68
	ldr	r2, .L98+72
	b	.L92
.L73:
	mov	r0, r4
	ldr	r1, .L98+76
	ldr	r2, .L98+80
	b	.L92
.L74:
	mov	r0, r4
	ldr	r1, .L98+4
	ldr	r2, .L98+8
	b	.L93
.L75:
	mov	r0, r4
	ldr	r1, .L98+12
	ldr	r2, .L98+16
	b	.L93
.L76:
	mov	r0, r4
	ldr	r1, .L98+20
	ldr	r2, .L98+24
	b	.L93
.L77:
	mov	r0, r4
	ldr	r1, .L98+28
	ldr	r2, .L98+32
	b	.L93
.L78:
	mov	r0, r4
	ldr	r1, .L98+36
	ldr	r2, .L98+40
	b	.L93
.L79:
	mov	r0, r4
	ldr	r1, .L98+44
	ldr	r2, .L98+48
	b	.L93
.L80:
	mov	r0, r4
	ldr	r1, .L98+52
	ldr	r2, .L98+56
	b	.L93
.L81:
	mov	r0, r4
	ldr	r1, .L98+60
	ldr	r2, .L98+64
	b	.L93
.L82:
	mov	r0, r4
	ldr	r1, .L98+68
	ldr	r2, .L98+72
	b	.L93
.L83:
	ldr	r1, .L98+76
	ldr	r2, .L98+80
	mov	r0, r4
.L93:
	bl	ON_AT_A_TIME_process_in_system
	b	.L85
.L63:
	bl	bad_key_beep
.L85:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, lr}
	b	Redraw_Screen
.L58:
	ldr	r3, .L98
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	movs	r0, r0, asl #16
	beq	.L91
.L86:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Up
.L55:
	ldr	r3, .L98
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	bl	STATION_GROUP_get_num_groups_in_use
	sub	r0, r0, #1
	cmp	r4, r0
	beq	.L91
.L87:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Down
.L56:
	ldr	r3, .L98
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #9
	movls	r0, r0, asr #16
	addls	r0, r0, #9
	bls	.L96
	sub	r3, r3, #10
	mov	r3, r3, asl #16
	cmp	r3, #589824
	bhi	.L91
	mov	r0, r0, asr #16
	sub	r0, r0, #10
.L96:
	mov	r1, r4
	b	.L95
.L57:
	ldr	r3, .L98
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #9
	movls	r0, r0, asr #16
	addls	r0, r0, #10
	bls	.L97
	sub	r3, r3, #10
	mov	r3, r3, asl #16
	cmp	r3, #589824
	bhi	.L91
	mov	r0, r0, asr #16
	sub	r0, r0, #9
.L97:
	mov	r1, #1
.L95:
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Select
.L91:
	ldmfd	sp!, {r4, r5, lr}
	b	bad_key_beep
.L59:
	ldr	r3, .L98+84
	mov	r2, #3
	str	r2, [r3, #0]
	bl	ON_AT_A_TIME_extract_and_store_changes_from_GuiVars
.L54:
	mov	r0, r4
	mov	r1, r5
	ldmfd	sp!, {r4, r5, lr}
	b	KEY_process_global_keys
.L99:
	.align	2
.L98:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSettingB_9
	.word	GuiVar_MenuScreenToShow
.LFE5:
	.size	ON_AT_A_TIME_process_screen, .-ON_AT_A_TIME_process_screen
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_on_at_a_time.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_on_at_a_time.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x8a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x3b
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x7c
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0xbe
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x109
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x12c
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x153
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"FDTO_ON_AT_A_TIME_draw_screen\000"
.LASF7:
	.ascii	"ON_AT_A_TIME_handle_info_panel\000"
.LASF0:
	.ascii	"ON_AT_A_TIME_get_max_allowed_on\000"
.LASF4:
	.ascii	"ON_AT_A_TIME_process_screen\000"
.LASF2:
	.ascii	"ON_AT_A_TIME_process_in_system\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"ON_AT_A_TIME_process_in_group\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_on_at_a_time.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
