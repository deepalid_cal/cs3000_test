	.file	"foal_lights.c"
	.text
.Ltext0:
	.section	.text.nm_FOAL_LIGHTS_add_to_action_needed_list,"ax",%progbits
	.align	2
	.type	nm_FOAL_LIGHTS_add_to_action_needed_list, %function
nm_FOAL_LIGHTS_add_to_action_needed_list:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldrb	r3, [r0, #24]	@ zero_extendqisi2
	mov	r4, r0
	cmp	r3, #4
	bls	.L2
	ldr	r0, .L4
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	ldmnefd	sp!, {r4, pc}
.LBB12:
	ldr	r0, .L4
	mov	r1, r4
.LBE12:
	ldmfd	sp!, {r4, lr}
.LBB13:
	b	nm_ListInsertTail
.L2:
.LBE13:
	ldr	r0, .L4+4
	ldmfd	sp!, {r4, lr}
	b	Alert_Message
.L5:
	.align	2
.L4:
	.word	foal_lights+36
	.word	.LC0
.LFE8:
	.size	nm_FOAL_LIGHTS_add_to_action_needed_list, .-nm_FOAL_LIGHTS_add_to_action_needed_list
	.section	.text.nm_turn_OFF_this_light,"ax",%progbits
	.align	2
	.type	nm_turn_OFF_this_light, %function
nm_turn_OFF_this_light:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	mov	r4, r0
	mov	r5, #0
	strb	r5, [r0, #27]
.LBB14:
	mov	r1, r4
	ldr	r0, .L10
	bl	nm_OnList
	cmp	r0, #1
	bne	.L7
	mov	r1, r4
	ldr	r0, .L10
	bl	nm_ListGetNext
	mov	r1, r4
	ldr	r2, .L10+4
	ldr	r3, .L10+8
	mov	r5, r0
	ldr	r0, .L10
	bl	nm_ListRemove_debug
	cmp	r0, #0
	beq	.L8
	ldr	r0, .L10+4
	bl	RemovePathFromFileName
	ldr	r2, .L10+12
	mov	r1, r0
	ldr	r0, .L10+16
	bl	Alert_Message_va
.L8:
	mov	r0, r4
	bl	nm_FOAL_LIGHTS_add_to_action_needed_list
.L7:
.LBE14:
	mov	r0, r4
	bl	nm_FOAL_LIGHTS_add_to_action_needed_list
	mov	r1, r4
	ldr	r2, .L10+4
	mov	r3, #856
	ldr	r0, .L10
	bl	nm_ListRemove_debug
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L11:
	.align	2
.L10:
	.word	foal_lights+16
	.word	.LC1
	.word	819
	.word	821
	.word	.LC2
.LFE10:
	.size	nm_turn_OFF_this_light, .-nm_turn_OFF_this_light
	.section	.text.nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light.constprop.2,"ax",%progbits
	.align	2
	.type	nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light.constprop.2, %function
nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light.constprop.2:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #3
	cmpls	r0, #11
	stmfd	sp!, {r4, lr}
.LCFI2:
	movhi	r4, #0
	movls	r4, #1
	bhi	.L13
	bl	LIGHTS_get_lights_array_index
	ldr	r3, .L15
	mov	r4, #36
	mla	r0, r4, r0, r3
	ldmfd	sp!, {r4, pc}
.L13:
	ldr	r0, .L15+4
	bl	Alert_Message
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L16:
	.align	2
.L15:
	.word	foal_lights+56
	.word	.LC3
.LFE26:
	.size	nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light.constprop.2, .-nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light.constprop.2
	.section	.text.nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3,"ax",%progbits
	.align	2
	.type	nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3, %function
nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #3
	cmpls	r0, #11
	stmfd	sp!, {r4, lr}
.LCFI3:
	movhi	r4, #0
	movls	r4, #1
	bhi	.L18
	bl	LIGHTS_get_lights_array_index
	ldr	r3, .L21
	mov	r4, #36
	mla	r0, r4, r0, r3
	ldr	r3, [r0, #8]
	cmp	r3, #0
	moveq	r0, #0
	ldmfd	sp!, {r4, pc}
.L18:
	ldr	r0, .L21+4
	bl	Alert_Message
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L22:
	.align	2
.L21:
	.word	foal_lights+56
	.word	.LC4
.LFE25:
	.size	nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3, .-nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3
	.section	.text.FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down,"ax",%progbits
	.align	2
	.type	FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down, %function
FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L31
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI4:
	ldr	r2, .L31+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L31+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L31+12
	ldr	r5, .L31+16
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L31+4
	mov	r3, #584
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	streq	r4, [r5, #32]
	streq	r4, [r5, #52]
	beq	.L25
	add	r0, r5, #16
	mov	r1, #0
	bl	nm_ListInit
	add	r0, r5, #36
	mov	r1, #12
	bl	nm_ListInit
	mov	r1, #0
	str	r1, [r5, #32]
	str	r1, [r5, #52]
	add	r0, r5, #56
	mov	r2, #1728
	bl	memset
.L25:
	mov	r4, #0
	ldr	r7, .L31+16
	b	.L26
.L28:
	mov	r1, r5
	mov	r0, r4
	bl	LIGHTS_get_lights_array_index
	mov	r1, r5
	mla	r0, sl, r0, r7
	strb	r5, [r0, #91]
	add	r6, r0, #56
	strb	r4, [r0, #90]
	mov	r0, r4
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3
	add	r5, r5, #1
	cmp	r0, #0
	strneb	r8, [r6, #26]
	cmp	r5, #4
	bne	.L28
	add	r4, r4, #1
	cmp	r4, #12
	beq	.L29
.L26:
	mov	r5, #0
	mov	sl, #36
	mov	r8, #1
	b	.L28
.L29:
	ldr	r3, .L31+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L31
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L32:
	.align	2
.L31:
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC1
	.word	582
	.word	system_preserves_recursive_MUTEX
	.word	foal_lights
.LFE5:
	.size	FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down, .-FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down
	.section	.text._nm_nm_nm_add_to_the_lights_ON_list.isra.1,"ax",%progbits
	.align	2
	.type	_nm_nm_nm_add_to_the_lights_ON_list.isra.1, %function
_nm_nm_nm_add_to_the_lights_ON_list.isra.1:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI5:
	ldr	r5, .L37
	mov	r4, r0
	mov	r1, #400
	ldr	r2, .L37+4
	ldr	r3, .L37+8
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldrb	r0, [r4, #34]	@ zero_extendqisi2
	ldrb	r1, [r4, #35]	@ zero_extendqisi2
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3
	subs	r6, r0, #0
	bne	.L34
	ldr	r0, .L37+12
	mov	r1, r4
	bl	nm_ListInsertTail
	cmp	r0, #0
	bne	.L35
	strb	r0, [r4, #27]
.L34:
	mov	r6, #1
	strb	r6, [r4, #26]
.L35:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, pc}
.L38:
	.align	2
.L37:
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC1
	.word	915
	.word	foal_lights+16
.LFE24:
	.size	_nm_nm_nm_add_to_the_lights_ON_list.isra.1, .-_nm_nm_nm_add_to_the_lights_ON_list.isra.1
	.section	.text.FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights
	.type	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights, %function
FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI6:
	mov	r0, #1
	bl	FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down
	ldr	lr, [sp], #4
	b	IRRI_LIGHTS_restart_all_lights
.LFE6:
	.size	FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights, .-FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri_side_lights
	.section	.text.init_battery_backed_foal_lights,"ax",%progbits
	.align	2
	.global	init_battery_backed_foal_lights
	.type	init_battery_backed_foal_lights, %function
init_battery_backed_foal_lights:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L44
	stmfd	sp!, {r4, lr}
.LCFI7:
	ldr	r2, .L44+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L44+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	bne	.L41
	ldr	r0, .L44+12
	ldr	r1, .L44+16
	mov	r2, #16
	bl	strncmp
	cmp	r0, #0
	beq	.L42
.L41:
	mov	r1, r4
	ldr	r0, .L44+20
	bl	Alert_battery_backed_var_initialized
	mov	r1, #0
	ldr	r2, .L44+24
	ldr	r0, .L44+12
	bl	memset
	mov	r0, #1
	bl	FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down
	ldr	r1, .L44+16
	mov	r2, #16
	ldr	r0, .L44+12
	bl	strlcpy
	b	.L43
.L42:
	bl	FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain_goes_down
.L43:
	ldr	r3, .L44
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L45:
	.align	2
.L44:
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC1
	.word	701
	.word	foal_lights
	.word	.LANCHOR0
	.word	.LC5
	.word	1784
.LFE7:
	.size	init_battery_backed_foal_lights, .-init_battery_backed_foal_lights
	.section	.text.FOAL_LIGHTS_initiate_a_mobile_light_ON,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_initiate_a_mobile_light_ON
	.type	FOAL_LIGHTS_initiate_a_mobile_light_ON, %function
FOAL_LIGHTS_initiate_a_mobile_light_ON:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI8:
	ldr	r5, [r0, #4]
	mov	r4, r0
	cmp	r5, #47
	bhi	.L47
	ldr	r3, .L54
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L54+4
	ldr	r3, .L54+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L54+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L54+4
	ldr	r3, .L54+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L54+20
	mov	r6, r5, lsr #2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L54+4
	ldr	r3, .L54+24
	bl	xQueueTakeMutexRecursive_debug
	and	r5, r5, #3
	mov	r0, r6
	mov	r1, r5
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	cmp	r0, #0
	bne	.L48
	ldr	r0, .L54+4
	bl	RemovePathFromFileName
	ldr	r2, .L54+28
	mov	r1, r0
	ldr	r0, .L54+32
	b	.L53
.L48:
.LBB15:
	mov	r0, r6
	mov	r1, r5
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3
	cmp	r0, #0
	beq	.L50
	mov	r3, #7
	strb	r3, [r0, #25]
	mov	r3, #3
	strb	r3, [r0, #24]
	ldr	r3, [r4, #8]
	strh	r3, [r0, #32]	@ movhi
	ldr	r3, [r4, #12]
	str	r3, [r0, #28]
	mov	r3, #1
	strb	r3, [r0, #26]
	b	.L49
.L50:
	mov	r0, r6
	mov	r1, r5
	bl	nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light.constprop.2
	subs	r7, r0, #0
	bne	.L51
	ldr	r0, .L54+4
	bl	RemovePathFromFileName
	ldr	r2, .L54+36
	mov	r1, r0
	ldr	r0, .L54+40
.L53:
	bl	Alert_Message_va
	b	.L49
.L51:
	mov	r3, #7
	strb	r3, [r7, #25]
	mov	r3, #3
	strb	r3, [r7, #24]
	ldr	r3, [r4, #8]
	strh	r3, [r7, #32]	@ movhi
	ldr	r3, [r4, #12]
	str	r3, [r7, #28]
	bl	_nm_nm_nm_add_to_the_lights_ON_list.isra.1
	cmp	r0, #0
	beq	.L52
	mov	r0, r6
	mov	r1, r5
	mov	r2, #4
	bl	Alert_light_ID_with_text
.L52:
	mov	r3, #0
	strb	r3, [r7, #27]
	mov	r3, #1
	strb	r3, [r7, #26]
.L49:
.LBE15:
	ldr	r3, .L54+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L54+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L54
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L47:
	ldr	r0, .L54+44
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	Alert_Message
.L55:
	.align	2
.L54:
	.word	list_lights_recursive_MUTEX
	.word	.LC1
	.word	1049
	.word	lights_preserves_recursive_MUTEX
	.word	1051
	.word	list_foal_lights_recursive_MUTEX
	.word	1053
	.word	1065
	.word	.LC6
	.word	1095
	.word	.LC7
	.word	.LC8
.LFE13:
	.size	FOAL_LIGHTS_initiate_a_mobile_light_ON, .-FOAL_LIGHTS_initiate_a_mobile_light_ON
	.section	.text.FOAL_LIGHTS_initiate_a_mobile_light_OFF,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_initiate_a_mobile_light_OFF
	.type	FOAL_LIGHTS_initiate_a_mobile_light_OFF, %function
FOAL_LIGHTS_initiate_a_mobile_light_OFF:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI9:
	ldr	r4, [r0, #4]
	cmp	r4, #47
	bhi	.L57
	ldr	r3, .L59
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L59+4
	ldr	r3, .L59+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L59+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L59+4
	ldr	r3, .L59+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L59+20
	mov	r5, r4, lsr #2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L59+24
	ldr	r2, .L59+4
	bl	xQueueTakeMutexRecursive_debug
	and	r4, r4, #3
	mov	r0, r5
	mov	r1, r4
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3
	subs	r3, r0, #0
	beq	.L58
	mov	r2, #0
	strb	r2, [r3, #25]
	mov	r2, #8
	strb	r2, [r3, #24]
	bl	nm_turn_OFF_this_light
	mov	r0, r5
	mov	r1, r4
	mov	r2, #5
	bl	Alert_light_ID_with_text
.L58:
	ldr	r3, .L59+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L59+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L59
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L57:
	ldr	r0, .L59+28
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_Message
.L60:
	.align	2
.L59:
	.word	list_lights_recursive_MUTEX
	.word	.LC1
	.word	1167
	.word	lights_preserves_recursive_MUTEX
	.word	1169
	.word	list_foal_lights_recursive_MUTEX
	.word	1171
	.word	.LC9
.LFE14:
	.size	FOAL_LIGHTS_initiate_a_mobile_light_OFF, .-FOAL_LIGHTS_initiate_a_mobile_light_OFF
	.section	.text.FOAL_LIGHTS_load_lights_on_list_into_outgoing_token,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_load_lights_on_list_into_outgoing_token
	.type	FOAL_LIGHTS_load_lights_on_list_into_outgoing_token, %function
FOAL_LIGHTS_load_lights_on_list_into_outgoing_token:
.LFB15:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L75
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, lr}
.LCFI10:
	ldr	r2, .L75+4
	mov	r5, #0
	str	r5, [r0, #0]
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L75+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L75+12
	strh	r5, [sp, #10]	@ movhi
	bl	nm_ListGetFirst
	b	.L73
.L64:
	ldrb	r3, [r5, #26]	@ zero_extendqisi2
	ldr	r0, .L75+12
	cmp	r3, #1
	ldreqh	r3, [sp, #10]
	mov	r1, r5
	addeq	r3, r3, #1
	streqh	r3, [sp, #10]	@ movhi
	bl	nm_ListGetNext
.L73:
	cmp	r0, #0
	mov	r5, r0
	bne	.L64
	ldrh	r6, [sp, #10]
	cmp	r6, #0
	moveq	r7, r0
	beq	.L65
.LBB16:
	mov	r6, r6, asl #3
	add	r7, r6, #2
	mov	r0, r7
	ldr	r1, .L75+4
	ldr	r2, .L75+16
	bl	mem_malloc_debug
	add	r1, sp, #10
	mov	r2, #2
	str	r0, [r4, #0]
	mov	r8, r0
	bl	memcpy
	ldr	r0, .L75+12
	add	r8, r8, #2
	bl	nm_ListGetFirst
	b	.L74
.L70:
	ldrb	r3, [r4, #26]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L67
.LBB17:
	ldrh	r3, [sp, #10]
	cmp	r3, #0
	bne	.L68
	ldr	r0, .L75+4
	bl	RemovePathFromFileName
	ldr	r2, .L75+20
	mov	r1, r0
	ldr	r0, .L75+24
	bl	Alert_Message_va
	b	.L69
.L68:
	sub	r3, r3, #1
	strh	r3, [sp, #10]	@ movhi
	ldrh	r3, [r4, #32]
	ldrb	r1, [r4, #35]	@ zero_extendqisi2
	strh	r3, [sp, #4]	@ movhi
	ldr	r3, [r4, #28]
	ldrb	r0, [r4, #34]	@ zero_extendqisi2
	str	r3, [sp, #0]
	bl	LIGHTS_get_lights_array_index
	ldrb	r3, [r4, #24]	@ zero_extendqisi2
	mov	r1, sp
	mov	r2, #8
	sub	r6, r6, #8
	strb	r3, [sp, #7]
	strb	r0, [sp, #6]
	mov	r0, r8
	add	r8, r8, #8
	bl	memcpy
	strb	r5, [r4, #26]
.L67:
.LBE17:
	ldr	r0, .L75+12
	mov	r1, r4
	bl	nm_ListGetNext
.L74:
	cmp	r0, #0
	mov	r4, r0
	bne	.L70
.L69:
	ldrh	r3, [sp, #10]
	cmp	r3, #0
	cmpeq	r6, #0
	beq	.L65
	ldr	r0, .L75+4
	bl	RemovePathFromFileName
	ldr	r2, .L75+28
	mov	r1, r0
	ldr	r0, .L75+32
	bl	Alert_Message_va
.L65:
.LBE16:
	ldr	r3, .L75
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r7
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, pc}
.L76:
	.align	2
.L75:
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC1
	.word	1253
	.word	foal_lights+16
	.word	1293
	.word	1314
	.word	.LC10
	.word	1345
	.word	.LC11
.LFE15:
	.size	FOAL_LIGHTS_load_lights_on_list_into_outgoing_token, .-FOAL_LIGHTS_load_lights_on_list_into_outgoing_token
	.section	.text.FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token
	.type	FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token, %function
FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token:
.LFB16:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, lr}
.LCFI11:
	ldr	r8, .L85
	str	r3, [r0, #0]
	ldr	r3, .L85+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L85+8
	ldr	r3, .L85+12
	bl	xQueueTakeMutexRecursive_debug
	ldrh	r6, [r8, #44]
	cmp	r6, #0
	strh	r6, [sp, #10]	@ movhi
	beq	.L78
.LBB18:
	mov	r5, r6, asl #3
	add	r6, r5, #2
	ldr	r1, .L85+8
	ldr	r2, .L85+16
	mov	r0, r6
	bl	mem_malloc_debug
	add	r1, sp, #10
	mov	r2, #2
	mov	r7, r0
	str	r0, [r4, #0]
	bl	memcpy
	add	r7, r7, #2
	add	r0, r8, #36
	ldr	r1, .L85+8
	ldr	r2, .L85+20
	b	.L84
.L80:
.LBB19:
	mov	r2, #8
	mov	r1, #0
	mov	r0, sp
	bl	memset
	ldrb	r1, [r4, #35]	@ zero_extendqisi2
	ldrb	r0, [r4, #34]	@ zero_extendqisi2
	bl	LIGHTS_get_lights_array_index
	ldrb	r3, [r4, #24]	@ zero_extendqisi2
	mov	r1, sp
	mov	r2, #8
	str	r3, [sp, #4]
	sub	r5, r5, #8
	str	r0, [sp, #0]
	mov	r0, r7
	bl	memcpy
	ldrh	r3, [sp, #10]
	ldr	r0, .L85+24
	ldr	r1, .L85+8
	ldr	r2, .L85+28
	sub	r3, r3, #1
	add	r7, r7, #8
	strh	r3, [sp, #10]	@ movhi
.L84:
	bl	nm_ListRemoveHead_debug
.LBE19:
	cmp	r0, #0
.LBB20:
	mov	r4, r0
.LBE20:
	bne	.L80
	cmp	r5, #0
	bne	.L81
	ldrh	r3, [sp, #10]
	cmp	r3, #0
	beq	.L78
.L81:
	ldr	r0, .L85+8
	bl	RemovePathFromFileName
	ldr	r2, .L85+32
	mov	r1, r0
	ldr	r0, .L85+36
	bl	Alert_Message_va
.L78:
.LBE18:
	ldr	r3, .L85+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, pc}
.L86:
	.align	2
.L85:
	.word	foal_lights
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC1
	.word	1367
	.word	1388
	.word	1402
	.word	foal_lights+36
	.word	1425
	.word	1432
	.word	.LC12
.LFE16:
	.size	FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token, .-FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token
	.section	.text.FOAL_LIGHTS_process_light_on_from_token_response,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_process_light_on_from_token_response
	.type	FOAL_LIGHTS_process_light_on_from_token_response, %function
FOAL_LIGHTS_process_light_on_from_token_response:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L94
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI12:
	ldr	r2, .L94+4
	ldr	r5, [r0, #4]
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L94+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L94+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L94+4
	ldr	r3, .L94+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L94+20
	mov	r6, r5, lsr #2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L94+4
	ldr	r3, .L94+24
	bl	xQueueTakeMutexRecursive_debug
	and	r5, r5, #3
	mov	r0, r6
	mov	r1, r5
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	cmp	r0, #0
	bne	.L88
	ldr	r0, .L94+4
	bl	RemovePathFromFileName
	ldr	r2, .L94+28
	mov	r1, r0
	ldr	r0, .L94+32
	b	.L93
.L88:
.LBB21:
	mov	r0, r6
	mov	r1, r5
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3
	cmp	r0, #0
	beq	.L90
	mov	r3, #4
	strb	r3, [r0, #25]
	mov	r3, #2
	strb	r3, [r0, #24]
	ldr	r3, [r4, #8]
	strh	r3, [r0, #32]	@ movhi
	ldr	r3, [r4, #12]
	str	r3, [r0, #28]
	mov	r3, #1
	strb	r3, [r0, #26]
	b	.L89
.L90:
	mov	r0, r6
	mov	r1, r5
	bl	nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light.constprop.2
	subs	r7, r0, #0
	bne	.L91
	ldr	r0, .L94+4
	bl	RemovePathFromFileName
	ldr	r2, .L94+36
	mov	r1, r0
	ldr	r0, .L94+40
.L93:
	bl	Alert_Message_va
	b	.L89
.L91:
	mov	r3, #4
	mov	r8, #2
	strb	r3, [r7, #25]
	strb	r8, [r7, #24]
	ldr	r3, [r4, #8]
	strh	r3, [r7, #32]	@ movhi
	ldr	r3, [r4, #12]
	str	r3, [r7, #28]
	bl	_nm_nm_nm_add_to_the_lights_ON_list.isra.1
	cmp	r0, #0
	beq	.L92
	mov	r0, r6
	mov	r1, r5
	mov	r2, r8
	bl	Alert_light_ID_with_text
.L92:
	mov	r3, #0
	strb	r3, [r7, #27]
	mov	r3, #1
	strb	r3, [r7, #26]
.L89:
.LBE21:
	ldr	r3, .L94+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L94+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L94
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L95:
	.align	2
.L94:
	.word	list_lights_recursive_MUTEX
	.word	.LC1
	.word	1459
	.word	lights_preserves_recursive_MUTEX
	.word	1461
	.word	list_foal_lights_recursive_MUTEX
	.word	1463
	.word	1471
	.word	.LC13
	.word	1502
	.word	.LC7
.LFE17:
	.size	FOAL_LIGHTS_process_light_on_from_token_response, .-FOAL_LIGHTS_process_light_on_from_token_response
	.section	.text.FOAL_LIGHTS_turn_off_this_lights_output,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_turn_off_this_lights_output
	.type	FOAL_LIGHTS_turn_off_this_lights_output, %function
FOAL_LIGHTS_turn_off_this_lights_output:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI13:
	mov	r6, r3
	ldr	r3, .L98
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L98+4
	mov	r7, r2
	ldr	r2, .L98+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L98+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L98+8
	ldr	r3, .L98+16
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	mov	r1, r4
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3
	subs	r3, r0, #0
	beq	.L97
	mov	r2, #0
	strb	r2, [r3, #25]
	strb	r7, [r3, #24]
	bl	nm_turn_OFF_this_light
	mov	r0, r5
	mov	r1, r4
	mov	r2, r6
	bl	Alert_light_ID_with_text
.L97:
	ldr	r3, .L98+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L98
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L99:
	.align	2
.L98:
	.word	lights_preserves_recursive_MUTEX
	.word	1554
	.word	.LC1
	.word	list_foal_lights_recursive_MUTEX
	.word	1556
.LFE18:
	.size	FOAL_LIGHTS_turn_off_this_lights_output, .-FOAL_LIGHTS_turn_off_this_lights_output
	.section	.text.FOAL_LIGHTS_turn_off_all_lights_at_this_box,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_turn_off_all_lights_at_this_box
	.type	FOAL_LIGHTS_turn_off_all_lights_at_this_box, %function
FOAL_LIGHTS_turn_off_all_lights_at_this_box:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI14:
	mov	r7, r0
	mov	r6, r1
	mov	r5, r2
	mov	r4, #0
.L101:
	mov	r1, r4
	mov	r0, r7
	mov	r2, r6
	mov	r3, r5
	add	r4, r4, #1
	bl	FOAL_LIGHTS_turn_off_this_lights_output
	cmp	r4, #4
	bne	.L101
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE19:
	.size	FOAL_LIGHTS_turn_off_all_lights_at_this_box, .-FOAL_LIGHTS_turn_off_all_lights_at_this_box
	.section	.text.FOAL_LIGHTS_process_light_off_from_token_response,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_process_light_off_from_token_response
	.type	FOAL_LIGHTS_process_light_off_from_token_response, %function
FOAL_LIGHTS_process_light_off_from_token_response:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #4]
	mov	r2, #7
	mov	r0, r1, lsr #2
	mov	r3, #3
	and	r1, r1, #3
	b	FOAL_LIGHTS_turn_off_this_lights_output
.LFE20:
	.size	FOAL_LIGHTS_process_light_off_from_token_response, .-FOAL_LIGHTS_process_light_off_from_token_response
	.global	__umodsi3
	.section	.text.TDCHECK_at_1Hz_rate_check_for_lights_schedule_start,"ax",%progbits
	.align	2
	.global	TDCHECK_at_1Hz_rate_check_for_lights_schedule_start
	.type	TDCHECK_at_1Hz_rate_check_for_lights_schedule_start, %function
TDCHECK_at_1Hz_rate_check_for_lights_schedule_start:
.LFB21:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L138
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI15:
	ldr	r2, .L138+4
	sub	sp, sp, #32
.LCFI16:
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L138+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L138+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L138+4
	ldr	r3, .L138+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L138+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L138+4
	ldr	r3, .L138+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L138+28
	ldr	r0, .L138+32
	ldr	r3, [r3, #0]
.LBB26:
	ldr	r6, .L138+36
.LBE26:
	str	r3, [sp, #20]
	bl	nm_ListGetFirst
	mov	r5, r0
	b	.L105
.L124:
	mov	r0, r5
	bl	LIGHTS_get_box_index
	str	r0, [sp, #4]
	mov	r0, r5
	bl	LIGHTS_get_output_index
	str	r0, [sp, #8]
	ldmib	sp, {r0, r1}
	bl	LIGHTS_get_lights_array_index
	str	r0, [sp, #16]
	ldrh	r0, [r4, #4]
	bl	LIGHTS_get_day_index
	mov	r7, r0
	mov	r1, r7
	mov	r0, r5
	bl	LIGHTS_get_start_time_1
	ldr	r3, [r4, #0]
	cmp	r0, r3
	beq	.L106
	mov	r0, r5
	mov	r1, r7
	bl	LIGHTS_get_start_time_2
	ldr	r3, [r4, #0]
	cmp	r0, r3
	bne	.L107
.L106:
	mov	r0, r5
	bl	LIGHTS_get_physically_available
	cmp	r0, #0
	beq	.L107
	ldrb	r3, [r4, #19]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L107
	mov	r0, r5
	bl	LIGHTS_has_a_stop_time
	cmp	r0, #0
	beq	.L108
	mov	r0, r5
	bl	LIGHTS_get_box_index
	mov	r7, r0
	mov	r0, r5
	bl	LIGHTS_get_output_index
	mov	r1, r0
	mov	r0, r7
	bl	nm_FOAL_LIGHTS_find_this_light_on_the_list.constprop.3
	subs	r7, r0, #0
	bne	.L109
	mov	r0, r5
	bl	LIGHTS_get_box_index
	mov	r7, r0
	mov	r0, r5
	bl	LIGHTS_get_output_index
	mov	r1, r0
	mov	r0, r7
	bl	nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light.constprop.2
	subs	r7, r0, #0
	beq	.L110
	mov	r3, #1
	strb	r3, [r7, #25]
	strb	r3, [r7, #24]
	ldrh	r3, [r4, #4]
	strh	r3, [r7, #32]	@ movhi
	ldr	r3, [r4, #0]
	str	r3, [r7, #28]
	bl	_nm_nm_nm_add_to_the_lights_ON_list.isra.1
	cmp	r0, #0
	bne	.L109
	ldr	r0, .L138+40
	bl	Alert_Message
	b	.L109
.L110:
	ldr	r0, .L138+44
	bl	Alert_Message
	ldr	r0, .L138+48
	bl	Alert_Message
	b	.L107
.L109:
.LBB27:
	ldrh	r0, [r4, #4]
	bl	LIGHTS_get_day_index
	str	r0, [sp, #12]
	ldr	r1, [sp, #12]
	mov	r0, r5
	bl	LIGHTS_get_stop_time_1
	cmp	r0, r6
	mov	r8, r0
	bhi	.L127
	ldr	r3, [r4, #0]
	cmp	r0, r3
	ldrcsh	r9, [r4, #4]
	movcs	sl, #1
	bcs	.L111
.L127:
	mvn	r8, #-268435456
	mov	sl, #0
	mov	r9, r8
.L111:
	mov	r0, r5
	ldr	r1, [sp, #12]
	bl	LIGHTS_get_stop_time_2
	cmp	r0, r6
	bhi	.L129
	ldr	r3, [r4, #0]
	cmp	r0, r3
	bcc	.L129
	cmp	r8, r0
	ldrhih	r9, [r4, #4]
	movhi	r8, r0
	mov	r3, #1
	b	.L112
.L129:
	mov	r3, #0
.L112:
	orrs	r1, r3, sl
	bne	.L113
	mov	fp, #1
.L116:
	ldr	r2, [sp, #12]
	mov	r1, #14
	add	r0, fp, r2
	bl	__umodsi3
	mov	r3, r0
	mov	r1, r3
	mov	r0, r5
	str	r3, [sp, #0]
	bl	LIGHTS_get_stop_time_1
	ldr	r3, [sp, #0]
	mov	r1, r3
	cmp	r0, r6
	ldrlsh	r9, [r4, #4]
	movls	r8, r0
	mov	r0, r5
	addls	r9, fp, r9
	movls	sl, #1
	movhi	sl, #0
	bl	LIGHTS_get_stop_time_2
	cmp	r0, r6
	bhi	.L133
	cmp	r8, r0
	ldrhih	r9, [r4, #4]
	movhi	r8, r0
	addhi	r9, fp, r9
	movhi	r3, #1
	bhi	.L115
.L133:
	mov	r3, #0
.L115:
	orrs	r2, r3, sl
	bne	.L113
	add	fp, fp, #1
	cmp	fp, #15
	bne	.L116
	mov	r3, r2
	mov	sl, r2
.L113:
	orrs	r3, r3, sl
	bne	.L117
	ldr	r0, .L138+52
	bl	Alert_Message
.LBE27:
	ldr	r0, .L138+56
	bl	Alert_Message
	b	.L118
.L117:
.LBB28:
	ldrh	r3, [r7, #32]
	cmp	r3, r9
	bne	.L119
	ldr	r3, [r7, #28]
	cmp	r8, r6
	cmpls	r3, r8
	bcs	.L118
	b	.L137
.L119:
	bcs	.L118
	cmp	r8, r6
	bhi	.L118
.L137:
.LBE28:
	mov	r3, #1
.LBB29:
	strh	r9, [r7, #32]	@ movhi
	str	r8, [r7, #28]
.LBE29:
	strb	r3, [r7, #25]
.L118:
	ldrh	r3, [r4, #4]
	ldrh	r2, [r7, #32]
	add	r3, r3, #14
	cmp	r2, r3
	bgt	.L121
	ldr	r3, [r7, #28]
	cmp	r3, r6
	bls	.L122
.L121:
	ldr	r0, .L138+60
	bl	Alert_Message
	ldrh	r3, [r4, #4]
	strh	r3, [r7, #32]	@ movhi
	ldr	r3, [r4, #0]
	str	r3, [r7, #28]
.L122:
	ldrh	r3, [r7, #32]
	mov	r1, r4
	strh	r3, [sp, #28]	@ movhi
	ldr	r3, [r7, #28]
	add	r0, sp, #24
	str	r3, [sp, #24]
	bl	DT1_IsBiggerThan_DT2
	cmp	r0, #1
	movne	r2, #7
	ldmib	sp, {r0, r1}
	bne	.L136
	mov	r2, #0
	bl	Alert_light_ID_with_text
	ldr	r1, [sp, #16]
	ldr	r3, .L138+64
	mov	r2, #20
	mla	r2, r1, r2, r3
	mov	r1, #0
	strh	r1, [r2, #32]	@ movhi
	ldr	r2, [sp, #16]
	add	r3, r3, r2
	ldrb	r2, [r3, #976]	@ zero_extendqisi2
	orr	r2, r2, #3
	strb	r2, [r3, #976]
	b	.L107
.L108:
	ldmib	sp, {r0, r1}
	mov	r2, #6
.L136:
	bl	Alert_light_ID_with_text
.L107:
	mov	r1, r5
	ldr	r0, .L138+32
	bl	nm_ListGetNext
	mov	r5, r0
.L105:
	cmp	r5, #0
	bne	.L124
	ldr	r3, .L138+28
	ldr	r2, [r3, #0]
	ldr	r3, [sp, #20]
	rsb	r2, r3, r2
	ldr	r3, .L138+68
	ldr	r1, [r3, #0]
	cmp	r2, r1
	strhi	r2, [r3, #0]
	ldr	r3, .L138+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L138+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L138
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L139:
	.align	2
.L138:
	.word	list_lights_recursive_MUTEX
	.word	.LC1
	.word	1660
	.word	lights_preserves_recursive_MUTEX
	.word	1663
	.word	list_foal_lights_recursive_MUTEX
	.word	1665
	.word	my_tick_count
	.word	light_list_hdr
	.word	86399
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	lights_preserves
	.word	.LANCHOR1
.LFE21:
	.size	TDCHECK_at_1Hz_rate_check_for_lights_schedule_start, .-TDCHECK_at_1Hz_rate_check_for_lights_schedule_start
	.section	.text.FOAL_LIGHTS_maintain_lights_list,"ax",%progbits
	.align	2
	.global	FOAL_LIGHTS_maintain_lights_list
	.type	FOAL_LIGHTS_maintain_lights_list, %function
FOAL_LIGHTS_maintain_lights_list:
.LFB22:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L153
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI17:
	ldr	r2, .L153+4
	sub	sp, sp, #24
.LCFI18:
	mov	r7, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L153+8
	bl	xQueueTakeMutexRecursive_debug
.LBB30:
	ldr	r0, .L153+12
	ldr	r3, [r0, #44]
	cmp	r3, #0
	bne	.L141
.LBE30:
	add	r0, r0, #16
	bl	nm_ListGetFirst
	ldr	r8, .L153+16
	ldr	r6, .L153+4
	ldr	sl, .L153+20
	mov	r4, r0
	b	.L150
.L148:
	mov	r5, #0
	mov	r1, r5
	mov	r2, #11
	mov	r3, r5
	add	r0, r4, #34
	stmia	sp, {r5, r8}
	str	r6, [sp, #8]
	str	sl, [sp, #12]
	bl	RC_uns8_with_filename
	ldr	r3, .L153+24
	mov	r1, r5
	stmib	sp, {r3, r6}
	ldr	r3, .L153+28
	add	r0, r4, #35
	str	r3, [sp, #12]
	mov	r2, #3
	mov	r3, r5
	str	r5, [sp, #0]
	bl	RC_uns8_with_filename
	ldrh	r3, [r4, #32]
	add	r1, sp, #24
	strh	r3, [sp, #20]	@ movhi
	ldr	r3, [r4, #28]
	str	r3, [r1, #-8]!
	ldrb	r3, [r4, #27]	@ zero_extendqisi2
	cmp	r3, r5
	beq	.L143
	mov	r0, r7
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #1
	mov	r9, r0
	bne	.L144
	mov	r3, #6
	strb	r3, [r4, #24]
	mov	r0, r4
	bl	nm_turn_OFF_this_light
	ldrb	r1, [r4, #35]	@ zero_extendqisi2
	mov	r2, r9
	mov	r5, r0
	ldrb	r0, [r4, #34]	@ zero_extendqisi2
	b	.L152
.L143:
	mov	r0, r1
	mov	r1, r7
	bl	DT1_IsBiggerThan_DT2
	cmp	r0, #1
	mov	r5, r0
	bne	.L146
.LBB31:
	ldrb	r0, [r4, #34]	@ zero_extendqisi2
	ldrb	r1, [r4, #35]	@ zero_extendqisi2
	bl	LIGHTS_get_lights_array_index
	ldrb	r3, [r4, #24]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L144
	ldr	r3, .L153+32
	add	r2, r3, r0
	ldrb	r1, [r2, #976]	@ zero_extendqisi2
	and	r1, r1, #243
	strb	r1, [r2, #976]
	mov	r2, #20
	mla	r0, r2, r0, r3
	mov	r1, r4
	ldrh	r3, [r0, #26]
	add	r3, r3, #1
	strh	r3, [r0, #26]	@ movhi
	ldr	r0, .L153+36
	bl	nm_OnList
	cmp	r0, #0
	strneb	r5, [r4, #27]
	bne	.L144
	ldr	r0, .L153+40
	ldr	r1, .L153+44
	ldr	r2, .L153+4
	ldr	r3, .L153+48
	bl	Alert_item_not_on_list_with_filename
	b	.L144
.L146:
.LBE31:
	mov	r3, #11
	strb	r3, [r4, #24]
	mov	r0, r4
	bl	nm_turn_OFF_this_light
	ldrb	r1, [r4, #35]	@ zero_extendqisi2
	mov	r2, #7
	mov	r5, r0
	ldrb	r0, [r4, #34]	@ zero_extendqisi2
.L152:
	bl	Alert_light_ID_with_text
	subs	r4, r5, #0
	bne	.L150
.L144:
	mov	r1, r4
	ldr	r0, .L153+36
	bl	nm_ListGetNext
	mov	r4, r0
.L150:
	cmp	r4, #0
	bne	.L148
.L141:
	ldr	r3, .L153
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L154:
	.align	2
.L153:
	.word	list_foal_lights_recursive_MUTEX
	.word	.LC1
	.word	1926
	.word	foal_lights
	.word	.LC20
	.word	1955
	.word	.LC21
	.word	1956
	.word	lights_preserves
	.word	foal_lights+16
	.word	.LC22
	.word	.LC23
	.word	1003
.LFE22:
	.size	FOAL_LIGHTS_maintain_lights_list, .-FOAL_LIGHTS_maintain_lights_list
	.global	__largest_lights_start_time_delta
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"F_L: Non-off sent to add to A N list\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/foal_lights.c\000"
.LC2:
	.ascii	"F_L: Error removing from ON list : %s, %u\000"
.LC3:
	.ascii	"F_L: Bad inputs to return llc pointer\000"
.LC4:
	.ascii	"F_L: Bad inputs to find_on_a_list\000"
.LC5:
	.ascii	"Foal Lights\000"
.LC6:
	.ascii	"F_L: lls not found : %s, %u\000"
.LC7:
	.ascii	"F_L: llc not available : %s, %u\000"
.LC8:
	.ascii	"F_L: Bad index in mobile on\000"
.LC9:
	.ascii	"F_L: Bad index in mobile off\000"
.LC10:
	.ascii	"FOAL light list must have changed : %s, %u\000"
.LC11:
	.ascii	"F_L: ON list token error : %s, %u\000"
.LC12:
	.ascii	"F_L: A_N token error : %s, %u\000"
.LC13:
	.ascii	"F_L: light not found : %s, %u\000"
.LC14:
	.ascii	"F_L: ON add failed\000"
.LC15:
	.ascii	"F_L: Too many lights\000"
.LC16:
	.ascii	"F_L: no ON list pointer\000"
.LC17:
	.ascii	"F_L: No Stop Time\000"
.LC18:
	.ascii	"F_L: no stop time error\000"
.LC19:
	.ascii	"F_L: unexpected date/time\000"
.LC20:
	.ascii	"Box Index\000"
.LC21:
	.ascii	"Output Index\000"
.LC22:
	.ascii	"pllc_ptr\000"
.LC23:
	.ascii	"foal_lights.header_for_foal_lights_ON_list\000"
	.section	.bss.__largest_lights_start_time_delta,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	__largest_lights_start_time_delta, %object
	.size	__largest_lights_start_time_delta, 4
__largest_lights_start_time_delta:
	.space	4
	.section	.rodata.FOAL_LIGHTS_PRE_TEST_STRING,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	FOAL_LIGHTS_PRE_TEST_STRING, %object
	.size	FOAL_LIGHTS_PRE_TEST_STRING, 16
FOAL_LIGHTS_PRE_TEST_STRING:
	.ascii	"f lights v03\000"
	.space	3
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI0-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI1-.LFB10
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI2-.LFB26
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI3-.LFB25
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI5-.LFB24
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI6-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI7-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI8-.LFB13
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI9-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI10-.LFB15
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI11-.LFB16
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI12-.LFB17
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI13-.LFB18
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI14-.LFB19
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI15-.LFB21
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI17-.LFB22
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE34:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_lights.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1d5
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF23
	.byte	0x1
	.4byte	.LASF24
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x2fd
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x323
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x387
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x164
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x14c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x3d4
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x1
	.byte	0xd0
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x115
	.byte	0x1
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x343
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	0x5f
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	0x57
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST3
	.uleb128 0x5
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x234
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	0x33
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x2a6
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x2bb
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x40e
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x484
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST9
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x4d7
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST10
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x54d
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST11
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x5a8
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST12
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x604
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST13
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x634
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST14
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x63f
	.4byte	.LFB20
	.4byte	.LFE20
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x1a4
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x665
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST15
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x77b
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST16
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB8
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB10
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB26
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB25
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB24
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB13
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB14
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB15
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB16
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB17
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB18
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB19
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB21
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI16
	.4byte	.LFE21
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB22
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI18
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xa4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"nm_FOAL_LIGHTS_add_to_action_needed_list\000"
.LASF24:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/foal_lights.c\000"
.LASF23:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF11:
	.ascii	"init_battery_backed_foal_lights\000"
.LASF19:
	.ascii	"FOAL_LIGHTS_process_light_off_from_token_response\000"
.LASF7:
	.ascii	"nm_FOAL_LIGHTS_return_pointer_to_llc_for_this_light"
	.ascii	"\000"
.LASF21:
	.ascii	"TDCHECK_at_1Hz_rate_check_for_lights_schedule_start"
	.ascii	"\000"
.LASF3:
	.ascii	"FOAL_LIGHTS_set_stop_date_and_time_to_furthest\000"
.LASF22:
	.ascii	"FOAL_LIGHTS_maintain_lights_list\000"
.LASF5:
	.ascii	"_nm_nm_FOAL_LIGHTS_complete_the_action_needed_turn_"
	.ascii	"ON\000"
.LASF12:
	.ascii	"FOAL_LIGHTS_initiate_a_mobile_light_ON\000"
.LASF9:
	.ascii	"FOAL_LIGHTS_restart_lights_on_reboot_and_when_chain"
	.ascii	"_goes_down\000"
.LASF2:
	.ascii	"_nm_nm_nm_add_to_the_lights_ON_list\000"
.LASF20:
	.ascii	"nm_FOAL_LIGHTS_get_soonest_stop_time\000"
.LASF17:
	.ascii	"FOAL_LIGHTS_turn_off_this_lights_output\000"
.LASF15:
	.ascii	"FOAL_LIGHTS_load_lights_action_needed_list_into_out"
	.ascii	"going_token\000"
.LASF1:
	.ascii	"nm_remove_from_the_lights_ON_list\000"
.LASF8:
	.ascii	"nm_turn_OFF_this_light\000"
.LASF4:
	.ascii	"__foal_lights_maintenance_may_run\000"
.LASF16:
	.ascii	"FOAL_LIGHTS_process_light_on_from_token_response\000"
.LASF6:
	.ascii	"nm_FOAL_LIGHTS_find_this_light_on_the_list\000"
.LASF13:
	.ascii	"FOAL_LIGHTS_initiate_a_mobile_light_OFF\000"
.LASF10:
	.ascii	"FOAL_LIGHTS_completely_wipe_both_foal_side_and_irri"
	.ascii	"_side_lights\000"
.LASF14:
	.ascii	"FOAL_LIGHTS_load_lights_on_list_into_outgoing_token"
	.ascii	"\000"
.LASF18:
	.ascii	"FOAL_LIGHTS_turn_off_all_lights_at_this_box\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
