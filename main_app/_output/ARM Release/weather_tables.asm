	.file	"weather_tables.c"
	.text
.Ltext0:
	.section	.text.nm_weather_tables_updater,"ax",%progbits
	.align	2
	.type	nm_weather_tables_updater, %function
nm_weather_tables_updater:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	stmfd	sp!, {r4, lr}
.LCFI0:
	mov	r4, r0
	bne	.L2
	ldr	r0, .L6
	mov	r1, r4
	ldmfd	sp!, {r4, lr}
	b	Alert_Message_va
.L2:
	ldr	r0, .L6+4
	mov	r1, #2
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	beq	.L3
	cmp	r4, #1
	bne	.L4
.L3:
	ldr	r3, .L6+8
	mov	r2, #60
	str	r2, [r3, #488]
	mov	r2, #0
	str	r2, [r3, #492]
	ldmfd	sp!, {r4, pc}
.L4:
	ldr	r0, .L6+12
	ldmfd	sp!, {r4, lr}
	b	Alert_Message
.L7:
	.align	2
.L6:
	.word	.LC0
	.word	.LC1
	.word	.LANCHOR0
	.word	.LC2
.LFE1:
	.size	nm_weather_tables_updater, .-nm_weather_tables_updater
	.section	.text.nm_WEATHER_TABLES_set_default_values,"ax",%progbits
	.align	2
	.type	nm_WEATHER_TABLES_set_default_values, %function
nm_WEATHER_TABLES_set_default_values:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L11
	mov	r1, #2000
	add	r0, r3, #240
	mov	r2, #0
	str	lr, [sp, #-4]!
.LCFI1:
.L9:
	strh	r1, [r3, #4]	@ movhi
	strh	r2, [r3, #6]	@ movhi
	strh	r2, [r3, #248]	@ movhi
	strh	r2, [r3, #250]	@ movhi
	add	r3, r3, #4
	cmp	r3, r0
	bne	.L9
	mov	r0, #1
	ldr	r2, .L11+4
	mov	r1, r0
	bl	DMYToDate
	ldr	r3, .L11
	mov	r2, #60
	str	r2, [r3, #488]
	mov	r2, #0
	str	r2, [r3, #492]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	str	r0, [r3, #0]
	ldr	pc, [sp], #4
.L12:
	.align	2
.L11:
	.word	.LANCHOR0
	.word	2011
.LFE0:
	.size	nm_WEATHER_TABLES_set_default_values, .-nm_WEATHER_TABLES_set_default_values
	.section	.text.init_file_weather_tables,"ax",%progbits
	.align	2
	.global	init_file_weather_tables
	.type	init_file_weather_tables, %function
init_file_weather_tables:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r4, .L14
	sub	sp, sp, #20
.LCFI3:
	mov	r3, #500
	str	r3, [sp, #0]
	ldr	r3, .L14+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	ldr	r1, .L14+8
	str	r3, [sp, #4]
	ldr	r3, .L14+12
	mov	r2, #2
	str	r3, [sp, #8]
	ldr	r3, .L14+16
	str	r3, [sp, #12]
	mov	r3, #7
	str	r3, [sp, #16]
	mov	r3, r4
	bl	FLASH_FILE_find_or_create_variable_file
	mov	r3, #0
	str	r3, [r4, #492]
	add	sp, sp, #20
	ldmfd	sp!, {r4, pc}
.L15:
	.align	2
.L14:
	.word	.LANCHOR0
	.word	weather_tables_recursive_MUTEX
	.word	.LANCHOR1
	.word	nm_weather_tables_updater
	.word	nm_WEATHER_TABLES_set_default_values
.LFE2:
	.size	init_file_weather_tables, .-init_file_weather_tables
	.section	.text.save_file_weather_tables,"ax",%progbits
	.align	2
	.global	save_file_weather_tables
	.type	save_file_weather_tables, %function
save_file_weather_tables:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #500
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI4:
	ldr	r1, .L17
	str	r3, [sp, #0]
	ldr	r3, .L17+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, #2
	str	r3, [sp, #4]
	mov	r3, #7
	str	r3, [sp, #8]
	ldr	r3, .L17+8
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L18:
	.align	2
.L17:
	.word	.LANCHOR1
	.word	weather_tables_recursive_MUTEX
	.word	.LANCHOR0
.LFE3:
	.size	save_file_weather_tables, .-save_file_weather_tables
	.section	.text.WEATHER_TABLES_get_et_table_date,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_get_et_table_date
	.type	WEATHER_TABLES_get_et_table_date, %function
WEATHER_TABLES_get_et_table_date:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L20
	ldr	r0, [r3, #0]
	bx	lr
.L21:
	.align	2
.L20:
	.word	.LANCHOR0
.LFE4:
	.size	WEATHER_TABLES_get_et_table_date, .-WEATHER_TABLES_get_et_table_date
	.section	.text.WEATHER_TABLES_get_et_table_entry_for_index,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_get_et_table_entry_for_index
	.type	WEATHER_TABLES_get_et_table_entry_for_index, %function
WEATHER_TABLES_get_et_table_entry_for_index:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L25
	stmfd	sp!, {r4, r5, lr}
.LCFI5:
	ldr	r2, .L25+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L25+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r5, #59
	bls	.L23
	ldr	r0, .L25+4
	bl	RemovePathFromFileName
	ldr	r2, .L25+12
	mov	r1, r0
	ldr	r0, .L25+16
	bl	Alert_Message_va
	mov	r3, #2000
	strh	r3, [r4, #0]	@ movhi
	mov	r3, #0
	strh	r3, [r4, #2]	@ movhi
	mov	r4, r3
	b	.L24
.L23:
	ldr	r3, .L25+20
	add	r5, r5, #1
	ldr	r3, [r3, r5, asl #2]
	str	r3, [r4, #0]
	mov	r4, #1
.L24:
	ldr	r3, .L25
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.L26:
	.align	2
.L25:
	.word	weather_tables_recursive_MUTEX
	.word	.LC3
	.word	294
	.word	298
	.word	.LC4
	.word	.LANCHOR0
.LFE5:
	.size	WEATHER_TABLES_get_et_table_entry_for_index, .-WEATHER_TABLES_get_et_table_entry_for_index
	.section	.text.WEATHER_TABLES_update_et_table_entry_for_index,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_update_et_table_entry_for_index
	.type	WEATHER_TABLES_update_et_table_entry_for_index, %function
WEATHER_TABLES_update_et_table_entry_for_index:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L30
	stmfd	sp!, {r4, r5, lr}
.LCFI6:
	ldr	r2, .L30+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L30+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r5, #59
	bls	.L28
	ldr	r0, .L30+4
	bl	RemovePathFromFileName
	ldr	r2, .L30+12
	mov	r1, r0
	ldr	r0, .L30+16
	bl	Alert_Message_va
	b	.L29
.L28:
	ldr	r3, .L30+20
	ldrh	r1, [r4, #0]
	add	r5, r5, #1
	mov	r5, r5, asl #2
	add	r2, r3, r5
	strh	r1, [r3, r5]	@ movhi
	ldrh	r3, [r4, #2]
	strh	r3, [r2, #2]	@ movhi
.L29:
	ldr	r3, .L30
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L31:
	.align	2
.L30:
	.word	weather_tables_recursive_MUTEX
	.word	.LC3
	.word	325
	.word	329
	.word	.LC4
	.word	.LANCHOR0
.LFE6:
	.size	WEATHER_TABLES_update_et_table_entry_for_index, .-WEATHER_TABLES_update_et_table_entry_for_index
	.section	.text.WEATHER_TABLES_get_rain_table_entry_for_index,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_get_rain_table_entry_for_index
	.type	WEATHER_TABLES_get_rain_table_entry_for_index, %function
WEATHER_TABLES_get_rain_table_entry_for_index:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L35
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	ldr	r2, .L35+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L35+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r5, #59
	bls	.L33
	ldr	r0, .L35+4
	bl	RemovePathFromFileName
	ldr	r2, .L35+12
	mov	r1, r0
	ldr	r0, .L35+16
	bl	Alert_Message_va
	mov	r3, #0
	strh	r3, [r4, #0]	@ movhi
	strh	r3, [r4, #2]	@ movhi
	mov	r4, r3
	b	.L34
.L33:
	ldr	r3, .L35+20
	add	r5, r5, #62
	ldr	r3, [r3, r5, asl #2]
	str	r3, [r4, #0]
	mov	r4, #1
.L34:
	ldr	r3, .L35
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.L36:
	.align	2
.L35:
	.word	weather_tables_recursive_MUTEX
	.word	.LC3
	.word	366
	.word	370
	.word	.LC4
	.word	.LANCHOR0
.LFE7:
	.size	WEATHER_TABLES_get_rain_table_entry_for_index, .-WEATHER_TABLES_get_rain_table_entry_for_index
	.section	.text.WEATHER_TABLES_update_rain_table_entry_for_index,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_update_rain_table_entry_for_index
	.type	WEATHER_TABLES_update_rain_table_entry_for_index, %function
WEATHER_TABLES_update_rain_table_entry_for_index:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L40
	stmfd	sp!, {r4, r5, lr}
.LCFI8:
	ldr	r2, .L40+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L40+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r5, #59
	bls	.L38
	ldr	r0, .L40+4
	bl	RemovePathFromFileName
	ldr	r2, .L40+12
	mov	r1, r0
	ldr	r0, .L40+16
	bl	Alert_Message_va
	b	.L39
.L38:
	ldr	r3, .L40+20
	ldrh	r1, [r4, #0]
	add	r5, r5, #62
	mov	r5, r5, asl #2
	add	r2, r3, r5
	strh	r1, [r3, r5]	@ movhi
	ldrh	r3, [r4, #2]
	strh	r3, [r2, #2]	@ movhi
.L39:
	ldr	r3, .L40
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L41:
	.align	2
.L40:
	.word	weather_tables_recursive_MUTEX
	.word	.LC3
	.word	403
	.word	407
	.word	.LC4
	.word	.LANCHOR0
.LFE8:
	.size	WEATHER_TABLES_update_rain_table_entry_for_index, .-WEATHER_TABLES_update_rain_table_entry_for_index
	.section	.text.WEATHER_TABLES_cause_et_table_historical_values_to_be_updated,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_cause_et_table_historical_values_to_be_updated
	.type	WEATHER_TABLES_cause_et_table_historical_values_to_be_updated, %function
WEATHER_TABLES_cause_et_table_historical_values_to_be_updated:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI9:
	ldr	r4, .L43
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L43+4
	ldr	r3, .L43+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L43+12
	ldr	r0, [r4, #0]
	mov	r2, #1
	str	r2, [r3, #76]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L44:
	.align	2
.L43:
	.word	weather_preserves_recursive_MUTEX
	.word	.LC3
	.word	441
	.word	weather_preserves
.LFE9:
	.size	WEATHER_TABLES_cause_et_table_historical_values_to_be_updated, .-WEATHER_TABLES_cause_et_table_historical_values_to_be_updated
	.section	.text.nm_RAIN_for_this_station_generate_rain_time_for_raintable_slot1,"ax",%progbits
	.align	2
	.global	nm_RAIN_for_this_station_generate_rain_time_for_raintable_slot1
	.type	nm_RAIN_for_this_station_generate_rain_time_for_raintable_slot1, %function
nm_RAIN_for_this_station_generate_rain_time_for_raintable_slot1:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #2112
	stmfd	sp!, {r4, r5, lr}
.LCFI10:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI11:
	mov	r4, r0
	mov	r5, r1
	bcc	.L46
	ldr	r0, .L49+32
	ldr	r1, .L49+36
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_index_out_of_range_with_filename
.L46:
	ldr	r3, .L49+40
	ldr	r2, .L49+32
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L49+44
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L49+48
	ldrh	r2, [r3, #252]
	cmp	r2, #0
	beq	.L47
	ldrh	r3, [r3, #254]
	cmp	r3, #1
	cmpne	r3, #4
	beq	.L48
	cmp	r3, #5
	bne	.L47
.L48:
	mov	r0, r4
	bl	STATION_station_is_available_for_use
	cmp	r0, #1
	bne	.L47
	mov	r0, r4
	bl	WEATHER_get_station_uses_rain
	cmp	r0, #1
	bne	.L47
	mov	r0, r4
	bl	STATION_GROUP_get_station_precip_rate_in_per_hr_100000u
	fldd	d7, .L49
	ldr	r3, .L49+52
	fldd	d8, .L49+8
	add	r5, r3, r5, asl #7
	ldr	r3, .L49+48
	add	r5, r5, #136
	ldrh	r3, [r3, #252]
	fmsr	s13, r0	@ int
	mov	r0, r4
	fuitod	d9, s13
	fdivd	d9, d9, d7
	fldd	d7, .L49+16
	fdivd	d9, d9, d7
	fmsr	s15, r3	@ int
	ldrh	r3, [r5, #2]
	fuitos	s12, s15
	fldd	d7, .L49+24
	fcvtds	d6, s12
	fdivd	d6, d6, d8
	fcvtsd	s18, d9
	fcvtds	d9, s18
	fdivd	d6, d6, d9
	fmuld	d7, d6, d7
	ftouizd	s13, d7
	fmrs	r2, s13	@ int
	add	r3, r2, r3
	strh	r3, [r5, #2]	@ movhi
	bl	STATION_GROUP_get_soil_storage_capacity_inches_100u
	mov	r2, #10
	fmsr	s15, r0	@ int
	fuitos	s14, s15
	fcvtds	d7, s14
	fdivd	d8, d7, d8
	fdivd	d9, d8, d9
	ftouizd	s13, d9
	fmrs	r3, s13	@ int
	mul	r3, r2, r3
	ldrh	r2, [r5, #2]
	cmp	r2, r3
	strhih	r3, [r5, #2]	@ movhi
.L47:
	ldr	r3, .L49+40
	ldr	r0, [r3, #0]
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L50:
	.align	2
.L49:
	.word	0
	.word	1090021888
	.word	0
	.word	1079574528
	.word	0
	.word	1078853632
	.word	0
	.word	1076101120
	.word	.LC3
	.word	478
	.word	weather_tables_recursive_MUTEX
	.word	493
	.word	.LANCHOR0
	.word	station_preserves
.LFE10:
	.size	nm_RAIN_for_this_station_generate_rain_time_for_raintable_slot1, .-nm_RAIN_for_this_station_generate_rain_time_for_raintable_slot1
	.section	.text.WEATHER_TABLES_load_record_with_historical_et,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_load_record_with_historical_et
	.type	WEATHER_TABLES_load_record_with_historical_et, %function
WEATHER_TABLES_load_record_with_historical_et:
.LFB13:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI12:
	fstmfdd	sp!, {d8}
.LCFI13:
	ldr	r3, .L54+8
	mov	r5, r0
	sub	sp, sp, #20
.LCFI14:
	ldr	r0, [r3, #0]
	mov	r4, r1
	ldr	r2, .L54+12
	mov	r1, #400
	mov	r3, #700
	bl	xQueueTakeMutexRecursive_debug
	cmp	r5, #59
	bhi	.L52
	ldr	r3, .L54+16
	add	r2, sp, #8
	ldr	r0, [r3, #0]
	add	r3, sp, #16
	add	r1, sp, #4
	str	r3, [sp, #0]
	rsb	r0, r5, r0
	add	r3, sp, #12
	bl	DateToDMY
	ldr	r0, [sp, #8]
	bl	ET_DATA_et_number_for_the_month
	ldr	r1, [sp, #12]
	fmsr	s16, r0
	ldr	r0, [sp, #8]
	bl	NumberOfDaysInMonth
	fldd	d7, .L54
	fcvtds	d8, s16
	fmuld	d8, d8, d7
	fmsr	s15, r0	@ int
	fuitos	s14, s15
	fcvtds	d7, s14
	fdivd	d8, d8, d7
	fcvtsd	s15, d8
	fmrs	r0, s15
	bl	__round_UNS16
	strh	r0, [r4, #0]	@ movhi
	b	.L53
.L52:
	ldr	r0, .L54+20
	bl	Alert_Message
	mov	r3, #2000
	strh	r3, [r4, #0]	@ movhi
.L53:
	mov	r3, #0
	strh	r3, [r4, #2]	@ movhi
	ldr	r3, .L54+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #20
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, pc}
.L55:
	.align	2
.L54:
	.word	0
	.word	1086556160
	.word	weather_tables_recursive_MUTEX
	.word	.LC3
	.word	.LANCHOR0
	.word	.LC5
.LFE13:
	.size	WEATHER_TABLES_load_record_with_historical_et, .-WEATHER_TABLES_load_record_with_historical_et
	.global	__umodsi3
	.global	__udivsi3
	.section	.text.WEATHER_TABLES_roll_et_rain_tables,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_roll_et_rain_tables
	.type	WEATHER_TABLES_roll_et_rain_tables, %function
WEATHER_TABLES_roll_et_rain_tables:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L106+12
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI15:
	ldr	r2, .L106+16
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L106+20
	bl	xQueueTakeMutexRecursive_debug
	bl	WEATHER_get_et_and_rain_table_roll_time
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	ldrb	r5, [r4, #5]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r4, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r4, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	cmp	r3, r0
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	mov	r6, r0
	orr	r5, r3, r5, asl #8
	ldr	r3, .L106+24
	addcs	r5, r5, #1
	ldr	r2, [r3, #0]
	cmp	r2, r5
	moveq	r5, #0
	beq	.L59
	bcs	.L60
	rsb	r2, r2, r5
	cmp	r2, #1
	bls	.L61
	mov	r1, #60
	cmp	r2, r1
	subhi	r2, r5, #61
	str	r1, [r3, #488]
	strhi	r2, [r3, #0]
	b	.L66
.L61:
	ldr	r2, [r3, #488]
	cmp	r2, #0
	moveq	r2, #2
	movne	r2, #60
	str	r2, [r3, #488]
	b	.L66
.L68:
	add	r3, r3, #1
	str	r3, [r8, #0]
	ldr	r3, .L106+24
	mov	r7, #59
.L67:
	ldr	r2, [r3, #236]
	subs	r7, r7, #1
	str	r2, [r3, #240]
	ldr	r2, [r3, #480]
	str	r2, [r3, #484]
	sub	r3, r3, #4
	bne	.L67
	mov	r0, r7
	ldr	r1, .L106+28
	bl	WEATHER_TABLES_load_record_with_historical_et
	strh	r7, [r8, #248]	@ movhi
	strh	r7, [r8, #250]	@ movhi
	b	.L102
.L66:
	ldr	r8, .L106+24
.L102:
	ldr	r3, [r8, #0]
	cmp	r3, r5
	bcc	.L68
	b	.L105
.L60:
	rsb	r2, r5, r2
	mov	r1, #60
	cmp	r2, r1
	addhi	r2, r5, #61
	str	r1, [r3, #488]
	strhi	r2, [r3, #0]
	ldr	r7, .L106+24
	ldr	r8, .L106+32
	b	.L103
.L72:
	sub	r3, r3, #1
	str	r3, [r7, #0]
	ldr	r3, .L106+24
	mov	r0, #0
.L71:
	ldr	r2, [r3, #8]
	add	r0, r0, #1
	str	r2, [r3, #4]
	ldr	r2, [r3, #252]
	cmp	r0, #59
	str	r2, [r3, #248]
	add	r3, r3, #4
	bne	.L71
	ldr	r1, .L106+36
	bl	WEATHER_TABLES_load_record_with_historical_et
	mov	r3, #0
	mov	r2, #484
	strh	r3, [r7, r2]	@ movhi
	strh	r3, [r7, r8]	@ movhi
.L103:
	ldr	r3, [r7, #0]
	cmp	r3, r5
	bhi	.L72
.L105:
	mov	r5, #1
.L59:
	ldr	r3, .L106+40
	ldr	r3, [r3, #76]
	cmp	r3, #0
	beq	.L73
	ldr	r7, .L106+24
	mov	r3, #60
	mov	r5, #1
	str	r3, [r7, #488]
.L75:
	add	r1, r7, r5, asl #2
	ldrh	r3, [r1, #2]
	sub	r0, r5, #1
	cmp	r3, #0
	bne	.L74
	bl	WEATHER_TABLES_load_record_with_historical_et
.L74:
	add	r5, r5, #1
	cmp	r5, #61
	bne	.L75
	ldr	r3, .L106+40
	mov	r2, #0
	str	r2, [r3, #76]
	bl	Alert_ET_table_loaded
	mov	r5, #1
.L73:
.LBB15:
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	ldrb	r0, [r4, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r4, #2]	@ zero_extendqisi2
	mov	r1, #3600
	orr	r3, r3, r2, asl #16
	orr	r0, r3, r0, asl #24
	bl	__umodsi3
	mov	r1, #3600
	mov	r7, r0
	mov	r0, r6
	bl	__umodsi3
	cmp	r7, r0
	bne	.L76
	ldr	r3, .L106+44
	ldr	r7, .L106+40
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L106+48
	ldr	r2, .L106+16
	bl	xQueueTakeMutexRecursive_debug
	ldrh	r3, [r7, #42]
	cmp	r3, #1
	bhi	.L77
.LBB16:
	bl	WEATHER_get_rain_bucket_minimum_inches_100u
	mov	sl, r0
	bl	WEATHER_get_rain_bucket_max_24_hour_inches_100u
	ldrh	r3, [r7, #42]
	ldr	r2, [r7, #44]
	cmp	r3, #1
	ldrh	r3, [r7, #40]
	add	r3, r2, r3
	mov	r8, r0
	beq	.L80
.L78:
	cmp	r3, sl
	bcc	.L80
	rsb	r3, sl, r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [r7, #40]	@ movhi
	cmp	r3, #0
	mov	r3, #1
	streqh	r3, [r7, #40]	@ movhi
	ldr	r7, .L106+40
	strh	r3, [r7, #42]	@ movhi
	ldr	r3, [r7, #60]
	cmp	r3, #0
	bne	.L79
	ldr	r0, .L106+16
	bl	RemovePathFromFileName
	ldr	r2, .L106+52
	mov	r1, r0
	ldr	r0, .L106+56
	bl	Alert_Message_va
	mov	r3, #1
	str	r3, [r7, #60]
	b	.L79
.L80:
	strh	r3, [r7, #40]	@ movhi
.L79:
	ldr	r3, .L106+40
	ldrh	r2, [r3, #40]
	cmp	r2, r8
	strhih	r8, [r3, #40]	@ movhi
.L77:
.LBE16:
	ldr	r3, .L106+40
	mov	r2, #0
	str	r2, [r3, #44]
	ldr	r3, .L106+44
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L76:
.LBE15:
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r4, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r4, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	cmp	r3, r6
	bne	.L82
	ldr	r3, .L106+24
	ldr	r4, .L106+44
	ldr	r2, [r3, #488]
	mov	r1, #400
	cmp	r2, #1
	movls	r2, #2
	strls	r2, [r3, #488]
	ldr	r0, [r4, #0]
	ldr	r2, .L106+16
	ldr	r3, .L106+60
	bl	xQueueTakeMutexRecursive_debug
.LBB17:
	mov	r1, #400
	ldr	r2, .L106+16
	ldr	r3, .L106+64
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	WEATHER_get_et_gage_is_in_use
	cmp	r0, #1
	bne	.L84
.LBB18:
	ldr	r4, .L106+40
	ldr	r3, [r4, #84]
	cmp	r3, #1
	bne	.L85
	bl	Alert_ETGage_RunAway
	b	.L86
.L85:
	ldr	r3, [r4, #80]
	cmp	r3, #1
	moveq	r3, #0
	streq	r3, [r4, #80]
	beq	.L86
	ldrh	r0, [r4, #36]
	cmp	r0, #0
	bne	.L88
	ldr	r3, [r4, #88]
	cmp	r3, #1
	bne	.L89
	bl	Alert_ETGage_Second_or_More_Zeros
	b	.L90
.L89:
	bl	Alert_ETGage_First_Zero
	ldr	r2, [r4, #36]
	ldr	r3, .L106+24
	str	r2, [r3, #8]
.L90:
	ldr	r3, .L106+40
	mov	r2, #1
	str	r2, [r3, #88]
	b	.L86
.L88:
	ldr	r3, .L106+24
	ldr	r2, [r4, #36]
	mov	r5, #0
	str	r2, [r3, #8]
	ldrh	r3, [r4, #38]
	str	r5, [r4, #88]
	cmp	r3, #1
	bne	.L86
	mov	r1, #100
	bl	__udivsi3
	ldr	r6, [r4, #92]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	cmp	r6, r0
	rsbcs	r6, r0, r6
	strcs	r6, [r4, #92]
	strcc	r5, [r4, #92]
.L86:
	ldr	r3, .L106+40
.LBB19:
	flds	s15, .L106
	ldr	r3, [r3, #92]
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	fldd	d6, .L106+4
	fdivs	s14, s14, s15
	fcvtds	d7, s14
	fmuld	d7, d7, d6
	ftouizd	s13, d7
	fmrs	r0, s13	@ int
.LBE19:
	cmp	r0, #14
	bhi	.L84
	bl	Alert_ETGage_percent_full_warning
.L84:
.LBE18:
	ldr	r4, .L106+40
	mov	r3, #0
	strh	r3, [r4, #36]	@ movhi
	mov	r3, #1
	strh	r3, [r4, #38]	@ movhi
	ldr	r3, .L106+44
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE17:
.LBB20:
	bl	WEATHER_get_rain_bucket_is_in_use
	cmp	r0, #1
	ldreq	r2, [r4, #40]
	ldreq	r3, .L106+24
	streq	r2, [r3, #252]
	mov	r3, #0
	strh	r3, [r4, #40]	@ movhi
	strh	r3, [r4, #42]	@ movhi
.LBE20:
	ldr	r3, [r4, #60]
	ldr	r4, .L106+40
	cmp	r3, #1
	bne	.L93
	ldr	r3, .L106+24
	ldrh	r3, [r3, #254]
	cmp	r3, #1
	beq	.L94
	ldr	r0, .L106+68
	bl	Alert_Message
.L94:
	mov	r3, #0
	str	r3, [r4, #60]
.L93:
	ldr	r3, [r4, #56]
	cmp	r3, #0
	ldrne	r2, .L106+40
	subne	r3, r3, #1
	strne	r3, [r2, #56]
	ldr	r3, .L106+40
	mov	r2, #0
	str	r2, [r3, #48]
	ldr	r3, .L106+44
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L96
.L82:
	cmp	r5, #0
	beq	.L97
.L96:
	ldr	r4, .L106+44
	mov	r0, #7
	mov	r1, #2
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	ldr	r2, .L106+16
	ldr	r3, .L106+72
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L106+40
	mov	r2, #1
	str	r2, [r3, #68]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
.L97:
	ldr	r3, .L106+12
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	xQueueGiveMutexRecursive
.L107:
	.align	2
.L106:
	.word	1149861888
	.word	0
	.word	1079574528
	.word	weather_tables_recursive_MUTEX
	.word	.LC3
	.word	898
	.word	.LANCHOR0
	.word	.LANCHOR0+4
	.word	486
	.word	.LANCHOR0+240
	.word	weather_preserves
	.word	weather_preserves_recursive_MUTEX
	.word	747
	.word	799
	.word	.LC6
	.word	1102
	.word	598
	.word	.LC7
	.word	1167
.LFE16:
	.size	WEATHER_TABLES_roll_et_rain_tables, .-WEATHER_TABLES_roll_et_rain_tables
	.section	.text.WEATHER_TABLES_load_et_rain_tables_into_outgoing_token,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_load_et_rain_tables_into_outgoing_token
	.type	WEATHER_TABLES_load_et_rain_tables_into_outgoing_token, %function
WEATHER_TABLES_load_et_rain_tables_into_outgoing_token:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI16:
	ldr	r8, .L111
	mov	r6, #0
	mov	r7, r0
	str	r6, [r0, #0]
	ldr	r0, [r8, #68]
	cmp	r0, r6
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
	ldr	r4, .L111+4
	ldr	r5, .L111+8
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L111+12
	ldr	r3, .L111+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L111+20
	ldr	r0, [r5, #0]
	mov	r1, #400
	ldr	r2, .L111+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L111+12
	ldr	r2, .L111+24
	mov	r0, #484
	bl	mem_malloc_debug
	ldr	r1, .L111+28
	mov	r2, #4
	mov	sl, r0
	str	r0, [r7, #0]
	bl	memcpy
	add	r0, sl, #4
	ldr	r1, .L111+32
	mov	r2, #240
	bl	memcpy
	ldr	r1, .L111+36
	mov	r2, #240
	add	r0, sl, #244
	bl	memcpy
	ldr	r0, [r5, #0]
	str	r6, [r8, #68]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #484
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L112:
	.align	2
.L111:
	.word	weather_preserves
	.word	weather_tables_recursive_MUTEX
	.word	weather_preserves_recursive_MUTEX
	.word	.LC3
	.word	1213
	.word	1217
	.word	1223
	.word	.LANCHOR0
	.word	.LANCHOR0+4
	.word	.LANCHOR0+248
.LFE17:
	.size	WEATHER_TABLES_load_et_rain_tables_into_outgoing_token, .-WEATHER_TABLES_load_et_rain_tables_into_outgoing_token
	.section	.text.WEATHER_TABLES_extract_et_rain_tables_out_of_msg_from_main,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_extract_et_rain_tables_out_of_msg_from_main
	.type	WEATHER_TABLES_extract_et_rain_tables_out_of_msg_from_main, %function
WEATHER_TABLES_extract_et_rain_tables_out_of_msg_from_main:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L116
	stmfd	sp!, {r4, r5, lr}
.LCFI17:
	ldr	r3, [r3, #48]
	mov	r4, r0
	cmp	r3, r1
	ldreq	r3, [r0, #0]
	addeq	r3, r3, #484
	streq	r3, [r0, #0]
	beq	.L115
	ldr	r5, .L116+4
	ldr	r3, .L116+8
	mov	r1, #400
	ldr	r2, .L116+12
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [r4, #0]
	mov	r2, #4
	ldr	r0, .L116+16
	bl	memcpy
	ldr	r1, [r4, #0]
	mov	r2, #240
	add	r1, r1, #4
	str	r1, [r4, #0]
	ldr	r0, .L116+20
	bl	memcpy
	ldr	r1, [r4, #0]
	mov	r2, #240
	add	r1, r1, #240
	str	r1, [r4, #0]
	ldr	r0, .L116+24
	bl	memcpy
	ldr	r3, [r4, #0]
	add	r3, r3, #240
	str	r3, [r4, #0]
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #7
	mov	r1, #2
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L115:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, pc}
.L117:
	.align	2
.L116:
	.word	config_c
	.word	weather_tables_recursive_MUTEX
	.word	1292
	.word	.LC3
	.word	.LANCHOR0
	.word	.LANCHOR0+4
	.word	.LANCHOR0+248
.LFE18:
	.size	WEATHER_TABLES_extract_et_rain_tables_out_of_msg_from_main, .-WEATHER_TABLES_extract_et_rain_tables_out_of_msg_from_main
	.section	.text.WEATHER_TABLES_set_et_ratio,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_set_et_ratio
	.type	WEATHER_TABLES_set_et_ratio, %function
WEATHER_TABLES_set_et_ratio:
.LFB19:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI18:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI19:
	mov	r5, r0
	sub	sp, sp, #24
.LCFI20:
	bl	WEATHER_get_percent_of_historical_cap_100u
	cmp	r5, #59
	movcs	r5, #59
	mov	r4, sp
	mov	r6, r0
	mov	r0, sp
	bl	EPSON_obtain_latest_complete_time_and_date
	ldrh	r3, [sp, #4]
	ldr	r2, .L125+12
	sub	r3, r3, #1
	strh	r3, [sp, #4]	@ movhi
	ldr	r3, .L125+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L125+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, .L125+24
	mov	r3, #1065353216
	cmp	r5, #0
	str	r3, [r2, #0]	@ float
	fmsr	s18, r3
	bne	.L123
	b	.L120
.L121:
	mov	r2, sp
	ldrh	r0, [sp, #4]
	ldr	r1, [sp, #0]
	bl	DateAndTimeToDTCS
	ldrh	r1, [sp, #10]
	ldrh	r0, [sp, #8]
	bl	NumberOfDaysInMonth
	add	r7, r7, #1
	mov	r8, r0
	ldrh	r0, [sp, #8]
	bl	ET_DATA_et_number_for_the_month
	fmsr	s14, r8	@ int
	add	r1, sp, #20
	fuitos	s15, s14
	fmsr	s13, r0
	fmuls	s19, s13, s20
	fdivs	s19, s19, s15
	ftouizs	s15, s16
	fmrs	r0, s15	@ int
	bl	WEATHER_TABLES_get_et_table_entry_for_index
	ldrh	r3, [sp, #20]
	fadds	s16, s16, s18
	fmsr	s13, r3	@ int
	ldrh	r3, [sp, #4]
	fuitos	s15, s13
	sub	r3, r3, #1
	strh	r3, [sp, #4]	@ movhi
	fdivs	s19, s15, s19
	fadds	s17, s17, s19
	b	.L119
.L123:
	fmsr	s16, r3
	flds	s17, .L125
	flds	s20, .L125+4
	mov	r7, #0
	mov	r4, sp
.L119:
	fmsr	s14, r5	@ int
	fuitos	s15, s14
	fcmpes	s16, s15
	fmstat
	bls	.L121
	fmsr	s15, r6	@ int
	fmsr	s13, r7	@ int
	ldr	r3, .L125+24
	fuitos	s14, s15
	flds	s15, .L125+8
	fmuls	s15, s14, s15
	fuitos	s14, s13
	fdivs	s17, s17, s14
	fcmps	s17, s15
	fmstat
	fcpysgt	s17, s15
	fsts	s17, [r3, #0]
.L120:
	ldr	r3, .L125+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #24
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L126:
	.align	2
.L125:
	.word	0
	.word	1176256512
	.word	1008981770
	.word	.LC3
	.word	weather_tables_recursive_MUTEX
	.word	1365
	.word	.LANCHOR2
.LFE19:
	.size	WEATHER_TABLES_set_et_ratio, .-WEATHER_TABLES_set_et_ratio
	.section	.text.WEATHER_TABLES_get_et_ratio,"ax",%progbits
	.align	2
	.global	WEATHER_TABLES_get_et_ratio
	.type	WEATHER_TABLES_get_et_ratio, %function
WEATHER_TABLES_get_et_ratio:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L128
	ldr	r0, [r3, #0]	@ float
	bx	lr
.L129:
	.align	2
.L128:
	.word	.LANCHOR2
.LFE20:
	.size	WEATHER_TABLES_get_et_ratio, .-WEATHER_TABLES_get_et_ratio
	.global	weather_tables
	.global	WEATHER_TABLES_FILENAME
	.section	.bss.weather_tables,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	weather_tables, %object
	.size	weather_tables, 500
weather_tables:
	.space	500
	.section	.rodata.WEATHER_TABLES_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	WEATHER_TABLES_FILENAME, %object
	.size	WEATHER_TABLES_FILENAME, 15
WEATHER_TABLES_FILENAME:
	.ascii	"WEATHER_TABLES\000"
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"W TABLES file unexpd update %u\000"
.LC1:
	.ascii	"WEATHER TABLES file update : to revision %u from %u"
	.ascii	"\000"
.LC2:
	.ascii	"W TABLES updater error\000"
.LC3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/weather_tables.c\000"
.LC4:
	.ascii	"WEATHER: index out of range : %s, %u\000"
.LC5:
	.ascii	"Table Index OOR\000"
.LC6:
	.ascii	"WEATHER: flag not set : %s, %u\000"
.LC7:
	.ascii	"WEATHER: unexp rain table status\000"
	.section	.data.et_ratio,"aw",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	et_ratio, %object
	.size	et_ratio, 4
et_ratio:
	.word	1065353216
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x1c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI6-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI7-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI8-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI9-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI10-.LFB10
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x24
	.byte	0x5
	.uleb128 0x54
	.uleb128 0x5
	.byte	0x5
	.uleb128 0x52
	.uleb128 0x7
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x9
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI12-.LFB13
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI15-.LFB16
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI16-.LFB17
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI17-.LFB18
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI18-.LFB19
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xe
	.uleb128 0x30
	.byte	0x5
	.uleb128 0x54
	.uleb128 0x8
	.byte	0x5
	.uleb128 0x52
	.uleb128 0xa
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xc
	.byte	0x4
	.4byte	.LCFI20-.LCFI19
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.align	2
.LEFDE32:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/weather_tables.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x19f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF21
	.byte	0x1
	.4byte	.LASF22
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x235
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x353
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0x87
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0x53
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xcf
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xf9
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x105
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x120
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x141
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x168
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x18f
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1b6
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x1d6
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST9
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x2b4
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST10
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x2e3
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x252
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x378
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST11
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x4ad
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST12
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x4fc
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST13
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x52f
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST14
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x58e
	.4byte	.LFB20
	.4byte	.LFE20
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB6
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB7
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB8
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB9
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB10
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI11
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB13
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI14
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB16
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB17
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB18
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB19
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	.LCFI20
	.4byte	.LFE19
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x9c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF22:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/weather_tables.c\000"
.LASF5:
	.ascii	"save_file_weather_tables\000"
.LASF1:
	.ascii	"nm_insert_rain_number\000"
.LASF10:
	.ascii	"WEATHER_TABLES_update_rain_table_entry_for_index\000"
.LASF0:
	.ascii	"ET_GAGE_percent_full\000"
.LASF4:
	.ascii	"init_file_weather_tables\000"
.LASF13:
	.ascii	"WEATHER_TABLES_load_record_with_historical_et\000"
.LASF12:
	.ascii	"nm_RAIN_for_this_station_generate_rain_time_for_rai"
	.ascii	"ntable_slot1\000"
.LASF15:
	.ascii	"insert_et_gage_number_into_table\000"
.LASF21:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF6:
	.ascii	"WEATHER_TABLES_get_et_table_date\000"
.LASF11:
	.ascii	"WEATHER_TABLES_cause_et_table_historical_values_to_"
	.ascii	"be_updated\000"
.LASF18:
	.ascii	"WEATHER_TABLES_extract_et_rain_tables_out_of_msg_fr"
	.ascii	"om_main\000"
.LASF19:
	.ascii	"WEATHER_TABLES_set_et_ratio\000"
.LASF14:
	.ascii	"__check_if_time_to_add_the_hourly_rain_to_the_rip\000"
.LASF16:
	.ascii	"WEATHER_TABLES_roll_et_rain_tables\000"
.LASF17:
	.ascii	"WEATHER_TABLES_load_et_rain_tables_into_outgoing_to"
	.ascii	"ken\000"
.LASF9:
	.ascii	"WEATHER_TABLES_get_rain_table_entry_for_index\000"
.LASF2:
	.ascii	"nm_weather_tables_updater\000"
.LASF8:
	.ascii	"WEATHER_TABLES_update_et_table_entry_for_index\000"
.LASF20:
	.ascii	"WEATHER_TABLES_get_et_ratio\000"
.LASF3:
	.ascii	"nm_WEATHER_TABLES_set_default_values\000"
.LASF7:
	.ascii	"WEATHER_TABLES_get_et_table_entry_for_index\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
