	.file	"poc_report_data.c"
	.text
.Ltext0:
	.section	.text.nm_init_poc_report_records,"ax",%progbits
	.align	2
	.type	nm_init_poc_report_records, %function
nm_init_poc_report_records:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r0, .L4
	mov	r1, #0
	mov	r2, #60
	bl	memset
	ldr	r6, .L4
	mov	r4, #0
	mov	r5, #60
.L2:
	add	r4, r4, #1
.LBB9:
	mla	r0, r5, r4, r6
	mov	r1, #0
	mov	r2, #60
	bl	memset
.LBE9:
	cmp	r4, #400
	bne	.L2
	ldmfd	sp!, {r4, r5, r6, pc}
.L5:
	.align	2
.L4:
	.word	.LANCHOR0
.LFE1:
	.size	nm_init_poc_report_records, .-nm_init_poc_report_records
	.section	.text.poc_report_data_ci_timer_callback,"ax",%progbits
	.align	2
	.type	poc_report_data_ci_timer_callback, %function
poc_report_data_ci_timer_callback:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L7
	mov	r1, #0
	mov	r2, #512
	mov	r3, r1
	b	CONTROLLER_INITIATED_post_to_messages_queue
.L8:
	.align	2
.L7:
	.word	405
.LFE5:
	.size	poc_report_data_ci_timer_callback, .-poc_report_data_ci_timer_callback
	.section	.text.nm_poc_report_data_updater,"ax",%progbits
	.align	2
	.type	nm_poc_report_data_updater, %function
nm_poc_report_data_updater:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r4, r0
	bne	.L10
	ldr	r0, .L12
	mov	r1, r4
	ldmfd	sp!, {r4, lr}
	b	Alert_Message_va
.L10:
	ldr	r0, .L12+4
	mov	r1, #1
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	bne	.L11
	ldmfd	sp!, {r4, lr}
	b	nm_init_poc_report_records
.L11:
	ldr	r0, .L12+8
	ldmfd	sp!, {r4, lr}
	b	Alert_Message
.L13:
	.align	2
.L12:
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE2:
	.size	nm_poc_report_data_updater, .-nm_poc_report_data_updater
	.section	.text.init_file_poc_report_records,"ax",%progbits
	.align	2
	.global	init_file_poc_report_records
	.type	init_file_poc_report_records, %function
init_file_poc_report_records:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L15
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r4, .L15+4
	sub	sp, sp, #28
.LCFI3:
	str	r3, [sp, #0]
	ldr	r3, .L15+8
	mov	r0, #1
	str	r3, [sp, #4]
	ldr	r3, .L15+12
	ldr	r1, .L15+16
	str	r3, [sp, #8]
	ldr	r3, .L15+20
	mov	r2, r0
	ldr	r3, [r3, #0]
	str	r3, [sp, #12]
	ldr	r3, .L15+24
	str	r3, [sp, #16]
	ldr	r3, .L15+28
	str	r3, [sp, #20]
	mov	r3, #3
	str	r3, [sp, #24]
	mov	r3, r4
	bl	FLASH_FILE_find_or_create_reports_file
	mov	r3, #0
	str	r3, [r4, #24]
	add	sp, sp, #28
	ldmfd	sp!, {r4, pc}
.L16:
	.align	2
.L15:
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR3
	.word	24060
	.word	.LANCHOR1
	.word	poc_report_completed_records_recursive_MUTEX
	.word	nm_poc_report_data_updater
	.word	nm_init_poc_report_records
.LFE3:
	.size	init_file_poc_report_records, .-init_file_poc_report_records
	.section	.text.save_file_poc_report_records,"ax",%progbits
	.align	2
	.global	save_file_poc_report_records
	.type	save_file_poc_report_records, %function
save_file_poc_report_records:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L18
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI4:
	ldr	r1, .L18+4
	str	r3, [sp, #0]
	ldr	r3, .L18+8
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, r0
	str	r3, [sp, #4]
	mov	r3, #3
	str	r3, [sp, #8]
	ldr	r3, .L18+12
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L19:
	.align	2
.L18:
	.word	24060
	.word	.LANCHOR1
	.word	poc_report_completed_records_recursive_MUTEX
	.word	.LANCHOR0
.LFE4:
	.size	save_file_poc_report_records, .-save_file_poc_report_records
	.global	__udivsi3
	.section	.text.POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running,"ax",%progbits
	.align	2
	.global	POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	.type	POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running, %function
POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI5:
	ldr	r4, .L23
	ldr	r2, [r4, #0]
	cmp	r2, #0
	bne	.L21
	ldr	r3, .L23+4
	ldr	r0, .L23+8
	str	r3, [sp, #0]
	ldr	r1, .L23+12
	mov	r3, r2
	bl	xTimerCreate
	cmp	r0, #0
	str	r0, [r4, #0]
	bne	.L21
	ldr	r0, .L23+16
	bl	RemovePathFromFileName
	mov	r2, #284
	mov	r1, r0
	ldr	r0, .L23+20
	bl	Alert_Message_va
.L21:
	ldr	r5, .L23
	ldr	r0, [r5, #0]
	cmp	r0, #0
	beq	.L20
	bl	xTimerIsTimerActive
	subs	r4, r0, #0
	bne	.L20
	ldr	r3, .L23+24
	ldr	r5, [r5, #0]
	ldr	r0, [r3, #48]
	bl	CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, r4
	mov	r2, r0
	mov	r0, r5
	bl	xTimerGenericCommand
.L20:
	ldmfd	sp!, {r3, r4, r5, pc}
.L24:
	.align	2
.L23:
	.word	.LANCHOR4
	.word	poc_report_data_ci_timer_callback
	.word	.LC3
	.word	12000
	.word	.LC4
	.word	.LC5
	.word	config_c
.LFE6:
	.size	POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running, .-POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	.section	.text.nm_POC_REPORT_DATA_inc_index,"ax",%progbits
	.align	2
	.global	nm_POC_REPORT_DATA_inc_index
	.type	nm_POC_REPORT_DATA_inc_index, %function
nm_POC_REPORT_DATA_inc_index:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #0]
	add	r3, r3, #1
	cmp	r3, #400
	movcs	r3, #0
	str	r3, [r0, #0]
	bx	lr
.LFE7:
	.size	nm_POC_REPORT_DATA_inc_index, .-nm_POC_REPORT_DATA_inc_index
	.section	.text.nm_POC_REPORT_RECORDS_get_previous_completed_record,"ax",%progbits
	.align	2
	.global	nm_POC_REPORT_RECORDS_get_previous_completed_record
	.type	nm_POC_REPORT_RECORDS_get_previous_completed_record, %function
nm_POC_REPORT_RECORDS_get_previous_completed_record:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L36
	add	r2, r3, #60
	cmp	r0, r2
	bcc	.L34
	ldr	r1, .L36+4
	cmp	r0, r1
	bcs	.L34
	ldr	r1, [r3, #12]
	cmp	r1, #1
	beq	.L34
	cmp	r0, r2
	subne	r0, r0, #60
	bne	.L29
	ldr	r0, [r3, #8]
	ldr	r3, .L36+8
	cmp	r0, #1
	moveq	r0, r3
	movne	r0, #0
	b	.L29
.L34:
	mov	r0, #0
.L29:
	ldr	r3, .L36
	mov	r1, #60
	ldr	r2, [r3, #4]
	add	r2, r2, #1
	mla	r2, r1, r2, r3
	cmp	r0, r2
	moveq	r2, #1
	streq	r2, [r3, #12]
	bx	lr
.L37:
	.align	2
.L36:
	.word	.LANCHOR0
	.word	.LANCHOR0+24060
	.word	.LANCHOR0+24000
.LFE9:
	.size	nm_POC_REPORT_RECORDS_get_previous_completed_record, .-nm_POC_REPORT_RECORDS_get_previous_completed_record
	.section	.text.nm_POC_REPORT_RECORDS_get_most_recently_completed_record,"ax",%progbits
	.align	2
	.global	nm_POC_REPORT_RECORDS_get_most_recently_completed_record
	.type	nm_POC_REPORT_RECORDS_get_most_recently_completed_record, %function
nm_POC_REPORT_RECORDS_get_most_recently_completed_record:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L39
	mov	r2, #0
	str	r2, [r3, #12]
	ldr	r2, [r3, #4]
	mov	r0, #60
	add	r2, r2, #1
	mla	r0, r2, r0, r3
	b	nm_POC_REPORT_RECORDS_get_previous_completed_record
.L40:
	.align	2
.L39:
	.word	.LANCHOR0
.LFE10:
	.size	nm_POC_REPORT_RECORDS_get_most_recently_completed_record, .-nm_POC_REPORT_RECORDS_get_most_recently_completed_record
	.section	.text.FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines,"ax",%progbits
	.align	2
	.global	FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines
	.type	FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines, %function
FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines:
.LFB11:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, lr}
.LCFI6:
	ldr	r5, .L49
	mov	r1, #400
	ldr	r2, .L49+4
	ldr	r3, .L49+8
	mov	r4, r0
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	POC_get_ptr_to_physically_available_poc
	bl	POC_get_index_using_ptr_to_poc_struct
	bl	POC_get_gid_of_group_at_this_index
	subs	r4, r0, #0
	bne	.L42
	ldr	r0, .L49+4
	bl	RemovePathFromFileName
	ldr	r2, .L49+12
	mov	r1, r0
	ldr	r0, .L49+16
	bl	Alert_Message_va
.L42:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L49+20
	ldr	r5, .L49+24
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L49+28
	ldr	r2, .L49+4
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #0]
	cmp	r3, #0
	bne	.L43
	mov	r0, #1600
	ldr	r1, .L49+4
	mov	r2, #540
	bl	mem_malloc_debug
	str	r0, [r5, #0]
.L43:
	mov	r1, sp
	mov	r0, r4
	bl	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	ldr	r3, .L49+24
	ldr	r2, .L49+32
	ldr	r6, [r3, #0]
	ldr	r3, [sp, #0]
	mov	r1, #472
	mla	r3, r1, r3, r2
	mov	r5, #1
	str	r3, [r6, #0]
	bl	nm_POC_REPORT_RECORDS_get_most_recently_completed_record
	ldr	r7, .L49+36
	b	.L44
.L48:
	ldr	r3, [r0, #0]
	cmp	r3, r4
	streq	r0, [r6, r5, asl #2]
	addeq	r5, r5, #1
	cmp	r5, r7
	bls	.L46
	ldr	r0, .L49+4
	bl	RemovePathFromFileName
	mov	r2, #568
	mov	r1, r0
	ldr	r0, .L49+40
	bl	Alert_Message_va
	b	.L47
.L46:
	bl	nm_POC_REPORT_RECORDS_get_previous_completed_record
.L44:
	cmp	r0, #0
	bne	.L48
.L47:
	ldr	r3, .L49+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, r6, r7, pc}
.L50:
	.align	2
.L49:
	.word	list_poc_recursive_MUTEX
	.word	.LC4
	.word	507
	.word	515
	.word	.LC6
	.word	poc_report_completed_records_recursive_MUTEX
	.word	.LANCHOR5
	.word	527
	.word	poc_preserves+48
	.word	399
	.word	.LC7
.LFE11:
	.size	FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines, .-FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines
	.section	.text.FDTO_POC_REPORT_load_guivars_for_scroll_line,"ax",%progbits
	.align	2
	.global	FDTO_POC_REPORT_load_guivars_for_scroll_line
	.type	FDTO_POC_REPORT_load_guivars_for_scroll_line, %function
FDTO_POC_REPORT_load_guivars_for_scroll_line:
.LFB12:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L52+4
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	ldr	r3, [r3, #0]
	ldr	r5, .L52+8
	mov	r0, r0, asl #16
	ldr	r4, [r3, r0, asr #14]
	sub	sp, sp, #20
.LCFI8:
	ldr	r0, [r5, #0]
	mov	r1, #400
	ldr	r2, .L52+12
	ldr	r3, .L52+16
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #250
	ldrh	r2, [r4, #8]
	add	r0, sp, #4
	str	r3, [sp, #0]
	mov	r1, #16
	mov	r3, #150
	bl	GetDateStr
	mov	r2, #6
	mov	r1, r0
	ldr	r0, .L52+20
	bl	strlcpy
	flds	s13, [r4, #56]	@ int
	flds	s15, .L52
	ldr	r3, .L52+24
	fuitos	s14, s13
	flds	s13, [r4, #44]	@ int
	add	r1, r4, #48
	ldmia	r1, {r0-r1}
	fdivs	s14, s14, s15
	fsts	s14, [r3, #0]
	fuitos	s14, s13
	ldr	r3, .L52+28
	flds	s13, [r4, #32]	@ int
	stmia	r3, {r0-r1}
	ldr	r3, .L52+32
	add	r1, r4, #36
	ldmia	r1, {r0-r1}
	fdivs	s14, s14, s15
	fsts	s14, [r3, #0]
	fuitos	s14, s13
	ldr	r3, .L52+36
	stmia	r3, {r0-r1}
	ldr	r3, .L52+40
	add	r1, r4, #24
	ldmia	r1, {r0-r1}
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	ldr	r3, .L52+44
	stmia	r3, {r0-r1}
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, pc}
.L53:
	.align	2
.L52:
	.word	1114636288
	.word	.LANCHOR5
	.word	poc_report_completed_records_recursive_MUTEX
	.word	.LC4
	.word	613
	.word	GuiVar_RptDate
	.word	GuiVar_RptIrrigMin
	.word	GuiVar_RptIrrigGal
	.word	GuiVar_RptManualMin
	.word	GuiVar_RptManualGal
	.word	GuiVar_RptNonCMin
	.word	GuiVar_RptNonCGal
.LFE12:
	.size	FDTO_POC_REPORT_load_guivars_for_scroll_line, .-FDTO_POC_REPORT_load_guivars_for_scroll_line
	.section	.text.nm_POC_REPORT_DATA_close_and_start_a_new_record,"ax",%progbits
	.align	2
	.global	nm_POC_REPORT_DATA_close_and_start_a_new_record
	.type	nm_POC_REPORT_DATA_close_and_start_a_new_record, %function
nm_POC_REPORT_DATA_close_and_start_a_new_record:
.LFB13:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L59
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI9:
	ldr	r6, .L59+4
	sub	sp, sp, #36
.LCFI10:
	mov	r4, r0
	ldr	r2, .L59+8
	ldr	r0, [r3, #0]
	mov	r5, r1
	ldr	r3, .L59+12
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	ip, [r6, #4]
	mov	r3, #60
	add	ip, ip, #1
	add	lr, r4, #28
	mla	ip, r3, ip, r6
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr, {r0, r1, r2}
	stmia	ip, {r0, r1, r2}
.LBB10:
	add	r0, r6, #4
	bl	nm_POC_REPORT_DATA_inc_index
	ldr	r3, [r6, #4]
	cmp	r3, #0
	moveq	r2, #1
	streq	r2, [r6, #8]
	ldr	r2, [r6, #16]
	cmp	r3, r2
	bne	.L56
	ldr	r0, .L59+16
	bl	nm_POC_REPORT_DATA_inc_index
.L56:
	ldr	r2, [r6, #4]
	ldr	r3, [r6, #20]
	cmp	r2, r3
	bne	.L57
	ldr	r0, .L59+20
	bl	nm_POC_REPORT_DATA_inc_index
.L57:
.LBE10:
	ldr	r3, .L59
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	mov	r0, #3
	mov	r1, #2
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.LBB11:
	mov	r2, #60
	add	r0, r4, #28
	mov	r1, #0
	bl	memset
.LBE11:
	ldrb	r2, [r5, #5]	@ zero_extendqisi2
	ldrb	r3, [r5, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	strh	r3, [r4, #36]	@ movhi
	ldrb	r2, [r5, #1]	@ zero_extendqisi2
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r5, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r5, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	str	r3, [r4, #32]
	ldr	r3, [r4, #4]
	str	r3, [r4, #28]
	ldr	r3, .L59+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #91
	bne	.L54
.LBB12:
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L59+28
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
.L54:
.LBE12:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, pc}
.L60:
	.align	2
.L59:
	.word	poc_report_completed_records_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC4
	.word	657
	.word	.LANCHOR0+16
	.word	.LANCHOR0+20
	.word	GuiLib_CurStructureNdx
	.word	FDTO_POC_USAGE_redraw_scrollbox
.LFE13:
	.size	nm_POC_REPORT_DATA_close_and_start_a_new_record, .-nm_POC_REPORT_DATA_close_and_start_a_new_record
	.section	.text.POC_REPORT_free_report_support,"ax",%progbits
	.align	2
	.global	POC_REPORT_free_report_support
	.type	POC_REPORT_free_report_support, %function
POC_REPORT_free_report_support:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI11:
	ldr	r4, .L63
	ldr	r5, .L63+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L63+8
	mov	r3, #780
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, #0]
	cmp	r0, #0
	beq	.L62
	ldr	r1, .L63+8
	mov	r2, #784
	bl	mem_free_debug
	mov	r3, #0
	str	r3, [r5, #0]
.L62:
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L64:
	.align	2
.L63:
	.word	poc_report_completed_records_recursive_MUTEX
	.word	.LANCHOR5
	.word	.LC4
.LFE14:
	.size	POC_REPORT_free_report_support, .-POC_REPORT_free_report_support
	.global	poc_revision_record_counts
	.global	poc_revision_record_sizes
	.global	POC_REPORT_RECORDS_FILENAME
	.global	poc_report_data_completed
	.section	.bss.poc_report_data_completed,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	poc_report_data_completed, %object
	.size	poc_report_data_completed, 24060
poc_report_data_completed:
	.space	24060
	.section	.rodata.poc_revision_record_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	poc_revision_record_sizes, %object
	.size	poc_revision_record_sizes, 8
poc_revision_record_sizes:
	.word	60
	.word	60
	.section	.bss.poc_report_ptrs,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	poc_report_ptrs, %object
	.size	poc_report_ptrs, 4
poc_report_ptrs:
	.space	4
	.section	.bss.poc_report_data_ci_timer,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	poc_report_data_ci_timer, %object
	.size	poc_report_data_ci_timer, 4
poc_report_data_ci_timer:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"POC RPRT file unexpd update %u\000"
.LC1:
	.ascii	"POC RPRT file update : to revision %u from %u\000"
.LC2:
	.ascii	"POC RPRT updater error\000"
.LC3:
	.ascii	"\000"
.LC4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/poc_report_data.c\000"
.LC5:
	.ascii	"Timer NOT CREATED : %s, %u\000"
.LC6:
	.ascii	"POC: unexpd results. : %s, %u\000"
.LC7:
	.ascii	"REPORTS: why so many records? : %s, %u\000"
	.section	.rodata.POC_REPORT_RECORDS_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	POC_REPORT_RECORDS_FILENAME, %object
	.size	POC_REPORT_RECORDS_FILENAME, 19
POC_REPORT_RECORDS_FILENAME:
	.ascii	"POC_REPORT_RECORDS\000"
	.section	.rodata.poc_revision_record_counts,"a",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	poc_revision_record_counts, %object
	.size	poc_revision_record_counts, 8
poc_revision_record_counts:
	.word	400
	.word	400
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI5-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI6-.LFB11
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI7-.LFB12
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI9-.LFB13
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI11-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE24:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/poc_report_data.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x135
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF15
	.byte	0x1
	.4byte	.LASF16
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x52
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x152
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF2
	.byte	0x1
	.byte	0x5d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x108
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.byte	0x9e
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xd7
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xfc
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x110
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x130
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x18b
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x1d3
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1f3
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST5
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x25d
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST6
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x289
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST7
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x306
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB11
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB12
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI8
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB13
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI10
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB14
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x7c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"nm_init_poc_report_records\000"
.LASF11:
	.ascii	"FDTO_POC_REPORT_fill_ptrs_and_return_how_many_lines"
	.ascii	"\000"
.LASF3:
	.ascii	"poc_report_data_ci_timer_callback\000"
.LASF12:
	.ascii	"FDTO_POC_REPORT_load_guivars_for_scroll_line\000"
.LASF5:
	.ascii	"init_file_poc_report_records\000"
.LASF4:
	.ascii	"nm_poc_report_data_updater\000"
.LASF15:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"nm_init_poc_report_record\000"
.LASF7:
	.ascii	"POC_REPORT_DATA_start_the_ci_timer_if_it_is_not_run"
	.ascii	"ning\000"
.LASF13:
	.ascii	"nm_POC_REPORT_DATA_close_and_start_a_new_record\000"
.LASF1:
	.ascii	"nm_POC_increment_next_avail_ptr\000"
.LASF16:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/poc_report_data.c\000"
.LASF6:
	.ascii	"save_file_poc_report_records\000"
.LASF9:
	.ascii	"nm_POC_REPORT_RECORDS_get_previous_completed_record"
	.ascii	"\000"
.LASF10:
	.ascii	"nm_POC_REPORT_RECORDS_get_most_recently_completed_r"
	.ascii	"ecord\000"
.LASF8:
	.ascii	"nm_POC_REPORT_DATA_inc_index\000"
.LASF14:
	.ascii	"POC_REPORT_free_report_support\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
