	.file	"battery_backed_funcs.c"
	.text
.Ltext0:
	.section	.text.nm_poc_preserves_complete_record_initialization,"ax",%progbits
	.align	2
	.type	nm_poc_preserves_complete_record_initialization, %function
nm_poc_preserves_complete_record_initialization:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, #0
	mov	r2, #472
	b	memset
.LFE20:
	.size	nm_poc_preserves_complete_record_initialization, .-nm_poc_preserves_complete_record_initialization
	.section	.text._nm_sync_users_settings_to_this_system_preserve,"ax",%progbits
	.align	2
	.type	_nm_sync_users_settings_to_this_system_preserve, %function
_nm_sync_users_settings_to_this_system_preserve:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	mov	r4, r0
	ldr	r0, [r0, #0]
	bl	SYSTEM_get_required_cell_iterations_before_flow_checking
	ldr	r3, .L3
	add	r5, r4, #14016
	str	r0, [r4, r3]
	ldr	r0, [r4, #0]
	bl	SYSTEM_get_required_station_cycles_before_flow_checking
	ldr	r3, .L3+4
	str	r0, [r4, r3]
	ldr	r0, [r4, #0]
	bl	SYSTEM_get_allow_table_to_lock
	ldr	r3, .L3+8
	str	r0, [r4, r3]
	ldr	r0, [r4, #0]
	bl	SYSTEM_get_derate_table_gpm_slot_size
	ldr	r3, .L3+12
	str	r0, [r4, r3]
	ldr	r0, [r4, #0]
	bl	SYSTEM_get_derate_table_max_stations_ON
	ldr	r3, .L3+16
	str	r0, [r4, r3]
	ldr	r0, [r4, #0]
	bl	SYSTEM_get_derate_table_number_of_gpm_slots
	ldr	r3, .L3+20
	str	r0, [r4, r3]
	ldr	r0, [r4, #0]
	bl	SYSTEM_get_flow_checking_in_use
	ldrb	r3, [r4, #466]	@ zero_extendqisi2
	add	r1, r5, #40
	bic	r3, r3, #1
	and	r0, r0, #1
	orr	r3, r0, r3
	strb	r3, [r4, #466]
	ldr	r0, [r4, #0]
	bl	SYSTEM_get_flow_check_ranges_gpm
	ldr	r0, [r4, #0]
	add	r1, r5, #52
	bl	SYSTEM_get_flow_check_tolerances_plus_gpm
	ldr	r0, [r4, #0]
	add	r1, r4, #14080
	add	r1, r1, #4
	ldmfd	sp!, {r4, r5, lr}
	b	SYSTEM_get_flow_check_tolerances_minus_gpm
.L4:
	.align	2
.L3:
	.word	14036
	.word	14032
	.word	14040
	.word	14044
	.word	14048
	.word	14052
.LFE7:
	.size	_nm_sync_users_settings_to_this_system_preserve, .-_nm_sync_users_settings_to_this_system_preserve
	.section	.text.init_battery_backed_weather_preserves,"ax",%progbits
	.align	2
	.global	init_battery_backed_weather_preserves
	.type	init_battery_backed_weather_preserves, %function
init_battery_backed_weather_preserves:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L8
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r2, .L8+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #82
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	bne	.L6
	ldr	r0, .L8+8
	ldr	r1, .L8+12
	mov	r2, #12
	bl	strncmp
	cmp	r0, #0
	beq	.L7
.L6:
	mov	r1, r4
	ldr	r4, .L8+8
	ldr	r0, .L8+16
	bl	Alert_battery_backed_var_initialized
	mov	r1, #0
	mov	r2, #236
	mov	r0, r4
	bl	memset
	mov	r0, #1
	mov	r1, r0
	ldr	r2, .L8+20
	bl	DMYToDate
	mov	r5, #0
	ldr	r2, .L8+20
	str	r5, [r4, #20]
	str	r5, [r4, #24]
	strh	r0, [r4, #16]	@ movhi
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	mov	r1, r5
	mov	r2, r5
	strh	r0, [r4, #32]	@ movhi
	mov	r0, r5
	bl	HMSToTime
	ldr	r2, .L8+24
	mov	r3, #1
	str	r2, [r4, #92]
	str	r5, [r4, #76]
	str	r3, [r4, #80]
	strh	r3, [r4, #38]	@ movhi
	strh	r5, [r4, #42]	@ movhi
	str	r3, [r4, #112]
	ldr	r1, .L8+12
	mov	r2, #16
	str	r0, [r4, #28]
	mov	r0, r4
	bl	strlcpy
.L7:
	ldr	r0, .L8+8
	mov	r1, #0
	strb	r1, [r0, #131]
	mov	r2, #92
	add	r0, r0, #144
	bl	memset
	ldr	r3, .L8
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L9:
	.align	2
.L8:
	.word	weather_preserves_recursive_MUTEX
	.word	.LC0
	.word	weather_preserves
	.word	.LANCHOR0
	.word	.LC1
	.word	2011
	.word	1100
.LFE0:
	.size	init_battery_backed_weather_preserves, .-init_battery_backed_weather_preserves
	.section	.text.init_battery_backed_station_preserves,"ax",%progbits
	.align	2
	.global	init_battery_backed_station_preserves
	.type	init_battery_backed_station_preserves, %function
init_battery_backed_station_preserves:
.LFB1:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L18
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	ldr	r2, .L18+4
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #218
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L18+8
	ldr	r1, .L18+12
	mov	r2, #12
	bl	strncmp
	cmp	r5, #0
	cmpeq	r0, #0
	moveq	r4, #0
	movne	r4, #1
	beq	.L11
.LBB19:
	mov	r1, r5
	ldr	r0, .L18+16
	bl	Alert_battery_backed_var_initialized
	mov	r1, #0
	ldr	r2, .L18+20
	ldr	r0, .L18+8
	bl	memset
	mov	r0, sp
	bl	EPSON_obtain_latest_time_and_date
	ldr	r4, .L18+8
	mov	r5, #0
	mov	r6, r4
.L12:
	mov	r0, #0
	mov	r1, r0
	mov	r2, r0
	bl	HMSToTime
	mov	r7, r5, asl #7
	add	r5, r5, #1
	str	r0, [r4, #124]
	mov	r0, #0
	mov	r1, r0
	mov	r2, r0
	bl	HMSToTime
	ldr	r2, .L18+24
	str	r0, [r4, #128]
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	ldr	r2, .L18+24
	strh	r0, [r4, #132]	@ movhi
	mov	r0, #1
	mov	r1, r0
	bl	DMYToDate
	add	r3, r6, r7
	add	r2, r3, #140
	strh	r0, [r4, #134]	@ movhi
	ldrb	r1, [r3, #140]	@ zero_extendqisi2
	add	r0, r7, #76
	bic	r1, r1, #48
	strb	r1, [r3, #140]
	ldrh	r1, [r3, #140]
	add	r0, r6, r0
	bic	r1, r1, #960
	strh	r1, [r3, #140]	@ movhi
	ldrb	r3, [r3, #141]	@ zero_extendqisi2
	bic	r3, r3, #16
	strb	r3, [r2, #1]
	bl	nm_init_station_report_data_record
	ldrh	r3, [sp, #4]
	add	r0, r7, #16
	strh	r3, [r4, #110]	@ movhi
	ldr	r3, [sp, #0]
	add	r0, r6, r0
	str	r3, [r4, #76]
	bl	nm_init_station_history_record
	cmp	r5, #2112
	add	r4, r4, #128
	bne	.L12
	ldr	r1, .L18+12
	mov	r2, #16
	ldr	r0, .L18+8
	bl	strlcpy
	b	.L13
.L11:
.LBE19:
	ldr	r3, .L18+28
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L18+4
	ldr	r3, .L18+32
	bl	xQueueTakeMutexRecursive_debug
	ldr	r5, .L18+8
	ldr	r6, .L18+36
	mov	sl, r4
	mov	r7, r5
	mov	r8, #60
.L15:
	add	r3, r7, r4, asl #7
	ldrb	r3, [r3, #141]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L14
	ldr	ip, [r6, #4]
	add	lr, r5, #16
	ldmia	lr!, {r0, r1, r2, r3}
	add	ip, ip, #1
	mla	ip, r8, ip, r6
	mov	sl, #1
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr, {r0, r1, r2}
	stmia	ip, {r0, r1, r2}
	bl	nm_STATION_HISTORY_increment_next_avail_ptr
.L14:
	add	r4, r4, #1
	cmp	r4, #2112
	add	r5, r5, #128
	bne	.L15
	ldr	r3, .L18+28
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	sl, #0
	beq	.L13
	mov	r0, #5
	mov	r1, r0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	mov	r1, #0
	ldr	r0, .L18+40
	mov	r2, #512
	mov	r3, r1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
.L13:
	ldr	r3, .L18
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, sl, pc}
.L19:
	.align	2
.L18:
	.word	station_preserves_recursive_MUTEX
	.word	.LC0
	.word	station_preserves
	.word	.LANCHOR1
	.word	.LC2
	.word	270352
	.word	2011
	.word	station_history_completed_records_recursive_MUTEX
	.word	307
	.word	station_history_completed
	.word	402
.LFE1:
	.size	init_battery_backed_station_preserves, .-init_battery_backed_station_preserves
	.section	.text.STATION_PRESERVES_get_index_using_box_index_and_station_number,"ax",%progbits
	.align	2
	.global	STATION_PRESERVES_get_index_using_box_index_and_station_number
	.type	STATION_PRESERVES_get_index_using_box_index_and_station_number, %function
STATION_PRESERVES_get_index_using_box_index_and_station_number:
.LFB2:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L23
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI3:
	ldr	r5, .L23+4
	sub	sp, sp, #24
.LCFI4:
	mov	r4, #0
	str	r3, [sp, #4]
	mov	r3, #380
	str	r0, [sp, #20]
	str	r1, [sp, #16]
	str	r3, [sp, #12]
	mov	r1, r4
	mov	r3, r4
	mov	r6, r2
	add	r0, sp, #20
	mov	r2, #11
	str	r4, [sp, #0]
	str	r5, [sp, #8]
	bl	RC_uint32_with_filename
	ldr	r3, .L23+8
	mov	r1, r4
	stmib	sp, {r3, r5}
	ldr	r3, .L23+12
	mov	r2, #175
	str	r3, [sp, #12]
	mov	r3, r4
	str	r4, [sp, #0]
	mov	r7, r0
	add	r0, sp, #16
	bl	RC_uint32_with_filename
	cmp	r0, #0
	beq	.L21
	subs	r0, r7, r4
	movne	r0, #1
.L21:
	ldr	r2, [sp, #16]
	ldr	r3, [sp, #20]
	mov	r1, #176
	mla	r3, r1, r3, r2
	str	r3, [r6, #0]
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L24:
	.align	2
.L23:
	.word	.LC3
	.word	.LC0
	.word	.LC4
	.word	385
.LFE2:
	.size	STATION_PRESERVES_get_index_using_box_index_and_station_number, .-STATION_PRESERVES_get_index_using_box_index_and_station_number
	.section	.text.STATION_PRESERVES_get_index_using_ptr_to_station_struct,"ax",%progbits
	.align	2
	.global	STATION_PRESERVES_get_index_using_ptr_to_station_struct
	.type	STATION_PRESERVES_get_index_using_ptr_to_station_struct, %function
STATION_PRESERVES_get_index_using_ptr_to_station_struct:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI5:
	ldr	r4, .L26
	ldr	r2, .L26+4
	ldr	r3, .L26+8
	mov	r5, r0
	mov	r6, r1
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	nm_STATION_get_box_index_0
	mov	r7, r0
	mov	r0, r5
	bl	nm_STATION_get_station_number_0
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r7
	mov	r1, r5
	mov	r2, r6
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	STATION_PRESERVES_get_index_using_box_index_and_station_number
.L27:
	.align	2
.L26:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	430
.LFE3:
	.size	STATION_PRESERVES_get_index_using_ptr_to_station_struct, .-STATION_PRESERVES_get_index_using_ptr_to_station_struct
	.section	.text.init_rip_portion_of_system_record,"ax",%progbits
	.align	2
	.global	init_rip_portion_of_system_record
	.type	init_rip_portion_of_system_record, %function
init_rip_portion_of_system_record:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI6:
	ldr	r4, .L29
	mov	r5, r0
	ldr	r3, .L29+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L29+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	mov	r1, #0
	mov	r2, #76
	bl	memset
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L30:
	.align	2
.L29:
	.word	system_preserves_recursive_MUTEX
	.word	473
	.word	.LC0
.LFE4:
	.size	init_rip_portion_of_system_record, .-init_rip_portion_of_system_record
	.section	.text.nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds
	.type	nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds, %function
nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI7:
.LBB20:
	ldrb	r3, [r0, #467]	@ zero_extendqisi2
	mov	r5, #0
	and	r3, r3, #79
	strb	r3, [r0, #467]
	ldrb	r3, [r0, #468]	@ zero_extendqisi2
.LBE20:
	mov	r4, r0
.LBB21:
	and	r3, r3, #248
	strb	r3, [r0, #468]
	mvn	r3, #0
	str	r3, [r0, #88]
	mov	r3, #1
	str	r3, [r0, #108]
	str	r3, [r0, #112]
	str	r3, [r0, #164]
	str	r5, [r0, #80]
	str	r5, [r0, #84]
	str	r5, [r0, #104]
	str	r5, [r0, #116]
	str	r5, [r0, #120]
	str	r5, [r0, #124]
	str	r5, [r0, #128]
	str	r5, [r0, #132]
	str	r5, [r0, #136]
	str	r5, [r0, #140]
	str	r5, [r0, #144]
	str	r5, [r0, #148]
	str	r5, [r0, #152]
	str	r5, [r0, #156]
	str	r5, [r0, #160]
	str	r5, [r0, #172]
	str	r5, [r0, #176]
	str	r5, [r0, #180]
	str	r5, [r0, #184]
	str	r5, [r0, #188]
.LBE21:
	str	r5, [r0, #100]
	str	r5, [r0, #92]
	mov	r1, r5
	add	r0, r0, #192
	mov	r2, #16
	bl	memset
	mov	r3, #0
	str	r5, [r4, #168]
	str	r5, [r4, #212]
.L32:
	add	r2, r4, r5
	add	r5, r5, #4
	cmp	r5, #80
	str	r3, [r2, #216]	@ float
	mov	r2, #0
	bne	.L32
	mov	r3, #0
	str	r3, [r4, #300]
	str	r2, [r4, #296]	@ float
	str	r2, [r4, #304]	@ float
	str	r2, [r4, #308]	@ float
.L33:
	add	r1, r4, r3
	add	r3, r3, #4
	cmp	r3, #120
	str	r2, [r1, #312]	@ float
	bne	.L33
	mov	r3, #0
	mov	r2, #468
	str	r3, [r4, #432]
	ldrh	r0, [r4, r2]
	ldrb	r1, [r4, #466]	@ zero_extendqisi2
	bic	r0, r0, #8064
	strh	r0, [r4, r2]	@ movhi
	ldr	r2, [r4, #468]
	mov	r0, #7
	bic	r2, r2, #122880
	str	r2, [r4, #468]
	and	r2, r2, #159
	strb	r2, [r4, #468]
	str	r0, [r4, #440]
	ldr	r2, .L38
	ldrb	r0, [r4, #464]	@ zero_extendqisi2
	str	r3, [r4, #436]
	str	r3, [r4, r2]
	str	r3, [r4, #208]
	str	r3, [r4, #460]
	str	r3, [r4, #448]
	str	r3, [r4, #452]
	and	r3, r0, #15
	strb	r3, [r4, #464]
	mov	r3, #0
	strb	r3, [r4, #465]
	and	r3, r1, #152
	orr	r3, r3, #2
	strb	r3, [r4, #466]
	ldr	r5, [r4, #0]
	ldrb	r3, [r4, #467]	@ zero_extendqisi2
	mov	r2, #150
	and	r3, r3, #240
	cmp	r5, #0
	str	r2, [r4, #456]
	strb	r3, [r4, #467]
	bne	.L34
	ldr	r3, .L38+4
	add	r6, r4, #14016
	str	r5, [r4, r3]
	sub	r3, r3, #4
	str	r5, [r4, r3]
	add	r3, r3, #8
	str	r5, [r4, r3]
	add	r3, r3, #4
	str	r5, [r4, r3]
	add	r3, r3, #4
	str	r5, [r4, r3]
	add	r3, r3, #4
	str	r5, [r4, r3]
	mov	r1, r5
	add	r0, r6, #40
	mov	r2, #12
	bl	memset
	mov	r1, r5
	add	r0, r6, #52
	mov	r2, #16
	bl	memset
	add	r0, r4, #14080
	mov	r1, r5
	mov	r2, #16
	add	r0, r0, #4
	bl	memset
	b	.L35
.L34:
	mov	r0, r4
	bl	_nm_sync_users_settings_to_this_system_preserve
.L35:
	ldrb	r3, [r4, #468]	@ zero_extendqisi2
	ldr	r2, .L38+8
	bic	r3, r3, #8
	strb	r3, [r4, #468]
	mov	r3, #0
	str	r3, [r4, r2]
	add	r2, r2, #4
	str	r3, [r4, r2]
	add	r2, r2, #4
	str	r3, [r4, r2]
	ldmfd	sp!, {r4, r5, r6, pc}
.L39:
	.align	2
.L38:
	.word	14124
	.word	14036
	.word	14100
.LFE8:
	.size	nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds, .-nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds
	.section	.text.nm_SYSTEM_PRESERVES_complete_record_initialization,"ax",%progbits
	.align	2
	.global	nm_SYSTEM_PRESERVES_complete_record_initialization
	.type	nm_SYSTEM_PRESERVES_complete_record_initialization, %function
nm_SYSTEM_PRESERVES_complete_record_initialization:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #1
	stmfd	sp!, {r4, r5, lr}
.LCFI8:
	str	r3, [r0, #96]
	ldrb	r3, [r0, #466]	@ zero_extendqisi2
	mov	r5, #0
	and	r3, r3, #231
	strb	r3, [r0, #466]
	ldrb	r3, [r0, #468]	@ zero_extendqisi2
	mov	r2, #504
	and	r3, r3, #159
	strb	r3, [r0, #468]
	ldr	r3, .L41
	strh	r5, [r0, r2]	@ movhi
	str	r5, [r0, r3]
	mov	r3, #520
	strh	r5, [r0, r3]	@ movhi
	add	r2, r2, #2
	add	r3, r3, #2
	strh	r5, [r0, r2]	@ movhi
	strh	r5, [r0, r3]	@ movhi
	mov	r2, #508
	mov	r3, #524
	strh	r5, [r0, r2]	@ movhi
	strh	r5, [r0, r3]	@ movhi
	add	r2, r2, #2
	add	r3, r3, #2
	strh	r5, [r0, r2]	@ movhi
	strh	r5, [r0, r3]	@ movhi
	mov	r2, #512
	mov	r3, #528
	strh	r5, [r0, r2]	@ movhi
	strh	r5, [r0, r3]	@ movhi
	add	r2, r2, #2
	add	r3, r3, #2
	mov	r4, r0
	strh	r5, [r0, r3]	@ movhi
	strh	r5, [r0, r2]	@ movhi
	str	r5, [r0, #0]
	str	r5, [r0, #444]
	strb	r5, [r0, #500]
	strb	r5, [r0, #501]
	strb	r5, [r0, #502]
.LBB22:
	str	r5, [r0, #472]
	str	r5, [r0, #492]
	add	r0, r0, #14144
	mov	r1, r5
	mov	r2, #44
.LBE22:
	strb	r5, [r4, #516]
	strb	r5, [r4, #517]
	strb	r5, [r4, #518]
.LBB23:
	add	r0, r0, #36
	bl	memset
.LBE23:
	mov	r0, r4
	bl	nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds
	add	r0, r4, #4
	bl	init_rip_portion_of_system_record
	add	r0, r4, #14080
	mov	r1, r5
	mov	r2, #48
	add	r0, r0, #48
	bl	memset
	mov	r1, r5
	add	r0, r4, #532
	ldr	r2, .L41+4
	bl	memset
	ldr	r2, .L41+8
	add	r0, r4, #9472
	add	r0, r0, #60
	mov	r1, r5
	ldmfd	sp!, {r4, r5, lr}
	b	memset
.L42:
	.align	2
.L41:
	.word	14124
	.word	9000
	.word	4500
.LFE10:
	.size	nm_SYSTEM_PRESERVES_complete_record_initialization, .-nm_SYSTEM_PRESERVES_complete_record_initialization
	.section	.text.init_battery_backed_system_preserves,"ax",%progbits
	.align	2
	.global	init_battery_backed_system_preserves
	.type	init_battery_backed_system_preserves, %function
init_battery_backed_system_preserves:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L50
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI9:
	ldr	r2, .L50+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L50+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L50+12
	ldr	r1, .L50+16
	mov	r2, #11
	bl	strncmp
	cmp	r4, #0
	cmpeq	r0, #0
	moveq	r0, #0
	movne	r0, #1
	beq	.L47
	mov	r1, r4
	ldr	r0, .L50+20
	bl	Alert_battery_backed_var_initialized
	ldr	r6, .L50+24
	ldr	r5, .L50+12
	mov	r4, #0
.L45:
	mla	r0, r6, r4, r5
	add	r4, r4, #1
	add	r0, r0, #16
	bl	nm_SYSTEM_PRESERVES_complete_record_initialization
	cmp	r4, #4
	bne	.L45
	ldr	r1, .L50+16
	mov	r2, #16
	ldr	r0, .L50+12
	bl	strlcpy
	b	.L46
.L47:
	ldr	r7, .L50+24
	ldr	r6, .L50+12
	mov	r4, r0
.LBB24:
	mov	r5, r0
.L44:
.LBE24:
	mla	r0, r7, r4, r6
.LBB25:
	mov	r1, #0
	str	r5, [r0, #488]
	str	r5, [r0, #508]
.LBE25:
	add	r0, r0, #16
.LBB26:
	add	r0, r0, #14144
	add	r0, r0, #36
	mov	r2, #44
.LBE26:
	add	r4, r4, #1
.LBB27:
	bl	memset
.LBE27:
	cmp	r4, #4
	bne	.L44
.L46:
	ldr	r3, .L50
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L51:
	.align	2
.L50:
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	1077
	.word	system_preserves
	.word	.LANCHOR2
	.word	.LC5
	.word	14224
.LFE11:
	.size	init_battery_backed_system_preserves, .-init_battery_backed_system_preserves
	.section	.text.SYSTEM_PRESERVES_init_system_preserves_ufim_variables_for_all_systems,"ax",%progbits
	.align	2
	.global	SYSTEM_PRESERVES_init_system_preserves_ufim_variables_for_all_systems
	.type	SYSTEM_PRESERVES_init_system_preserves_ufim_variables_for_all_systems, %function
SYSTEM_PRESERVES_init_system_preserves_ufim_variables_for_all_systems:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L55
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI10:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L55+4
	ldr	r3, .L55+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r5, .L55+12
	ldr	r4, .L55+16
	mov	r1, #0
.LBB28:
	mov	r2, r1
	mvn	ip, #0
	mov	r0, #1
.L53:
.LBE28:
	mla	r3, r5, r1, r4
	add	r1, r1, #1
	add	r3, r3, #16
.LBB29:
	ldrb	r6, [r3, #467]	@ zero_extendqisi2
.LBE29:
	cmp	r1, #4
.LBB30:
	and	r6, r6, #79
	strb	r6, [r3, #467]
	ldrb	r6, [r3, #468]	@ zero_extendqisi2
	str	r2, [r3, #80]
	and	r6, r6, #248
	strb	r6, [r3, #468]
	str	r2, [r3, #84]
	str	ip, [r3, #88]
	str	r2, [r3, #104]
	str	r0, [r3, #108]
	str	r0, [r3, #112]
	str	r2, [r3, #116]
	str	r2, [r3, #120]
	str	r2, [r3, #124]
	str	r2, [r3, #128]
	str	r2, [r3, #132]
	str	r2, [r3, #136]
	str	r2, [r3, #140]
	str	r2, [r3, #144]
	str	r2, [r3, #148]
	str	r2, [r3, #152]
	str	r2, [r3, #156]
	str	r2, [r3, #160]
	str	r0, [r3, #164]
	str	r2, [r3, #172]
	str	r2, [r3, #176]
	str	r2, [r3, #180]
	str	r2, [r3, #184]
	str	r2, [r3, #188]
.LBE30:
	bne	.L53
	ldr	r3, .L55
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L56:
	.align	2
.L55:
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	1144
	.word	14224
	.word	system_preserves
.LFE12:
	.size	SYSTEM_PRESERVES_init_system_preserves_ufim_variables_for_all_systems, .-SYSTEM_PRESERVES_init_system_preserves_ufim_variables_for_all_systems
	.section	.text.SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid,"ax",%progbits
	.align	2
	.global	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	.type	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid, %function
SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI11:
	subs	r4, r0, #0
	bne	.L58
	ldr	r0, .L64
	bl	Alert_Message
	b	.L59
.L58:
.LBB31:
	ldr	r3, .L64+4
	ldr	r2, .L64+8
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L64+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, .L64+16
	mov	r3, #0
.L62:
	ldr	r1, [r2, #16]
	cmp	r1, r4
	ldreq	r2, .L64+20
	ldreq	r4, .L64+24
	mlaeq	r4, r3, r4, r2
	beq	.L61
.L60:
	add	r3, r3, #1
	add	r2, r2, #14208
	cmp	r3, #4
	add	r2, r2, #16
	bne	.L62
	mov	r4, #0
.L61:
	ldr	r3, .L64+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L59:
.LBE31:
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L65:
	.align	2
.L64:
	.word	.LC6
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	1189
	.word	system_preserves
	.word	system_preserves+16
	.word	14224
.LFE13:
	.size	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid, .-SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	.section	.text.SYSTEM_PRESERVES_request_a_resync,"ax",%progbits
	.align	2
	.global	SYSTEM_PRESERVES_request_a_resync
	.type	SYSTEM_PRESERVES_request_a_resync, %function
SYSTEM_PRESERVES_request_a_resync:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI12:
	ldr	r4, .L67
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L67+4
	ldr	r3, .L67+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldrb	r3, [r0, #468]	@ zero_extendqisi2
	orr	r3, r3, #8
	strb	r3, [r0, #468]
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L68:
	.align	2
.L67:
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	614
.LFE6:
	.size	SYSTEM_PRESERVES_request_a_resync, .-SYSTEM_PRESERVES_request_a_resync
	.section	.text.SYSTEM_PRESERVES_synchronize_preserves_to_file,"ax",%progbits
	.align	2
	.global	SYSTEM_PRESERVES_synchronize_preserves_to_file
	.type	SYSTEM_PRESERVES_synchronize_preserves_to_file, %function
SYSTEM_PRESERVES_synchronize_preserves_to_file:
.LFB15:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L102
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI13:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L102+4
	ldr	r3, .L102+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L102+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L102+4
	ldr	r3, .L102+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L102+20
	mov	r4, #0
	ldrb	r2, [r3, #484]	@ zero_extendqisi2
	bic	r2, r2, #16
	strb	r2, [r3, #484]
	ldr	r2, .L102+24
	ldrb	r1, [r3, r2]	@ zero_extendqisi2
	bic	r1, r1, #16
	strb	r1, [r3, r2]
	ldr	r2, .L102+28
	ldrb	r1, [r3, r2]	@ zero_extendqisi2
	bic	r1, r1, #16
	strb	r1, [r3, r2]
	ldr	r2, .L102+32
	ldrb	r1, [r3, r2]	@ zero_extendqisi2
	bic	r1, r1, #16
	strb	r1, [r3, r2]
	bl	SYSTEM_get_list_count
	mov	r5, r0
	b	.L70
.L75:
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	subs	r7, r0, #0
	bne	.L71
	ldr	r0, .L102+4
	bl	RemovePathFromFileName
	ldr	r2, .L102+36
	mov	r1, r0
	ldr	r0, .L102+40
	b	.L100
.L71:
	bl	nm_GROUP_get_group_ID
	subs	r6, r0, #0
	bne	.L73
	ldr	r0, .L102+4
	bl	RemovePathFromFileName
	ldr	r2, .L102+44
	mov	r1, r0
	ldr	r0, .L102+48
.L100:
	bl	Alert_Message_va
	mov	r4, #0
	ldr	r6, .L102+52
	ldr	r5, .L102+20
	b	.L93
.L73:
	mov	r0, r7
	bl	SYSTEM_this_system_is_in_use
	cmp	r0, #0
	beq	.L74
.LBB36:
	mov	r0, r6
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	cmp	r0, #0
	ldrneb	r3, [r0, #468]	@ zero_extendqisi2
	orrne	r3, r3, #16
	strneb	r3, [r0, #468]
.L74:
.LBE36:
	add	r4, r4, #1
.L70:
	cmp	r4, r5
	bne	.L75
	ldr	r4, .L102+20
	ldr	r6, .L102+52
	mov	r5, #0
	mov	r7, r4
	mvn	r8, #0
.L80:
	ldr	r3, [r4, #16]
	cmp	r3, #0
	beq	.L78
	mla	r3, r6, r5, r7
	ldrb	sl, [r3, #484]	@ zero_extendqisi2
	mov	sl, sl, lsr #4
	ands	sl, sl, #1
	bne	.L78
	mov	r0, sl
	bl	Alert_system_preserves_activity
	ldr	r0, [r4, #488]
	cmp	r0, #0
	beq	.L79
	ldr	r1, .L102+4
	ldr	r2, .L102+56
	bl	mem_free_debug
	str	r8, [sp, #0]
	mov	r1, #3
	ldr	r0, [r4, #512]
	mov	r2, sl
	mov	r3, sl
	bl	xTimerGenericCommand
.L79:
	mla	r0, r6, r5, r7
	add	r0, r0, #16
	bl	nm_SYSTEM_PRESERVES_complete_record_initialization
.L78:
	add	r5, r5, #1
	add	r4, r4, #14208
	cmp	r5, #4
	add	r4, r4, #16
	bne	.L80
	b	.L101
.L91:
.LBB37:
	mov	r0, r5
	bl	SYSTEM_get_group_at_this_index
	cmp	r0, #0
	bne	.L82
	ldr	r0, .L102+4
	bl	RemovePathFromFileName
	ldr	r2, .L102+60
	mov	r1, r0
	ldr	r0, .L102+40
	b	.L100
.L82:
	bl	nm_GROUP_get_group_ID
	subs	fp, r0, #0
	bne	.L83
	ldr	r0, .L102+4
	bl	RemovePathFromFileName
	ldr	r2, .L102+64
	mov	r1, r0
	ldr	r0, .L102+48
	b	.L100
.L83:
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	r4, r0, #0
	bne	.L84
	mov	r0, #1
	bl	Alert_system_preserves_activity
	ldr	r3, .L102+20
	mov	r9, r4
.L87:
.LBB38:
	ldr	r2, [r3, #16]
	cmp	r2, fp
	bne	.L85
	mla	r4, r8, r9, r7
	ldr	r0, .L102+68
	bl	Alert_Message
	add	r4, r4, #16
	b	.L90
.L85:
	add	r9, r9, #1
	add	r3, r3, #14208
	cmp	r9, #4
	add	r3, r3, #16
	bne	.L87
	ldr	r3, .L102+20
	mov	r9, #0
.L89:
	ldr	r2, [r3, #16]
	cmp	r2, #0
	bne	.L88
	mul	r9, r8, r9
	add	r4, r9, #16
	add	r4, r4, r7
	mov	r0, r4
	bl	nm_SYSTEM_PRESERVES_complete_record_initialization
	mov	r3, fp, asl #16
	add	r9, r7, r9
	mov	r3, r3, lsr #16
	str	r3, [r9, #16]
	mov	r0, r4
	bl	nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds
	mov	r0, r6
	str	fp, [r9, #20]
	bl	EPSON_obtain_latest_time_and_date
	ldmia	r6, {r0, r1}
	str	r0, [r9, #24]
	strh	r1, [r9, #28]	@ movhi
	b	.L90
.L88:
	add	r9, r9, #1
	add	r3, r3, #14208
	cmp	r9, #4
	add	r3, r3, #16
	bne	.L89
	ldr	r0, .L102+4
	bl	RemovePathFromFileName
	ldr	r2, .L102+72
	mov	r1, r0
	ldr	r0, .L102+76
	bl	Alert_Message_va
	b	.L90
.L84:
.LBE38:
	ldrb	r3, [r4, #468]	@ zero_extendqisi2
	tst	r3, #8
	beq	.L90
	mov	r0, #2
	bl	Alert_system_preserves_activity
	mov	r0, r4
	bl	_nm_sync_users_settings_to_this_system_preserve
.L90:
	ldrb	r3, [r4, #468]	@ zero_extendqisi2
.LBE37:
	add	r5, r5, #1
.LBB40:
	bic	r3, r3, #8
	strb	r3, [r4, #468]
.L94:
.LBE40:
	cmp	r5, sl
	bne	.L91
	b	.L92
.L93:
	mla	r0, r6, r4, r5
	add	r4, r4, #1
	add	r0, r0, #16
	bl	nm_SYSTEM_PRESERVES_complete_record_initialization
	cmp	r4, #4
	bne	.L93
.L92:
	ldr	r3, .L102+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L102
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L101:
	bl	SYSTEM_num_systems_in_use
	mov	r5, #0
.LBB41:
	ldr	r7, .L102+20
.LBB39:
	ldr	r8, .L102+52
	add	r6, sp, #4
.LBE39:
.LBE41:
	mov	sl, r0
	b	.L94
.L103:
	.align	2
.L102:
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	1351
	.word	list_system_recursive_MUTEX
	.word	1357
	.word	system_preserves
	.word	14708
	.word	28932
	.word	43156
	.word	1377
	.word	.LC7
	.word	1389
	.word	.LC8
	.word	14224
	.word	1443
	.word	1478
	.word	1490
	.word	.LC9
	.word	1306
	.word	.LC10
.LFE15:
	.size	SYSTEM_PRESERVES_synchronize_preserves_to_file, .-SYSTEM_PRESERVES_synchronize_preserves_to_file
	.section	.text.SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit,"ax",%progbits
	.align	2
	.global	SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit
	.type	SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit, %function
SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L109
	mov	r1, r1, asl #16
	stmfd	sp!, {r4, r5, lr}
.LCFI14:
	ldr	r2, .L109+4
	mov	r4, r0
	mov	r5, r1, lsr #16
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L109+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	beq	.L105
.LBB42:
	mov	r0, r5
	bl	ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid
	ldr	r3, [r4, #88]
	ldr	r2, [r4, #92]
	cmp	r0, r3
	strcc	r0, [r4, #88]
	ldr	r3, [r4, #88]
	cmp	r2, r3
	bls	.L107
	ldr	r0, .L109+4
	bl	RemovePathFromFileName
	ldr	r2, .L109+12
	mov	r1, r0
	ldr	r0, .L109+16
	b	.L108
.L105:
.LBE42:
	ldr	r0, .L109+4
	bl	RemovePathFromFileName
	ldr	r2, .L109+20
	mov	r1, r0
	ldr	r0, .L109+24
.L108:
	bl	Alert_Message_va
.L107:
	ldr	r3, .L109
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L110:
	.align	2
.L109:
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	1564
	.word	1587
	.word	.LC11
	.word	1594
	.word	.LC12
.LFE16:
	.size	SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit, .-SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit
	.section	.text.SYSTEM_PRESERVES_is_this_valve_allowed_ON,"ax",%progbits
	.align	2
	.global	SYSTEM_PRESERVES_is_this_valve_allowed_ON
	.type	SYSTEM_PRESERVES_is_this_valve_allowed_ON, %function
SYSTEM_PRESERVES_is_this_valve_allowed_ON:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L117
	mov	r1, r1, asl #16
	stmfd	sp!, {r4, r5, lr}
.LCFI15:
	ldr	r2, .L117+4
	mov	r4, r0
	mov	r5, r1, lsr #16
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L117+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	beq	.L112
	ldr	r2, [r4, #92]
	ldr	r3, [r4, #88]
	cmp	r2, r3
	bcs	.L113
.LBB43:
	mov	r0, r5
	bl	ON_AT_A_TIME_get_on_at_a_time_within_system_for_this_gid
	ldr	r4, [r4, #92]
	cmp	r0, r4
	movls	r4, #0
	movhi	r4, #1
	b	.L114
.L113:
.LBE43:
	bls	.L116
	ldr	r0, .L117+4
	bl	RemovePathFromFileName
	ldr	r2, .L117+12
	mov	r1, r0
	ldr	r0, .L117+16
	bl	Alert_Message_va
	b	.L116
.L112:
	ldr	r0, .L117+4
	ldr	r1, .L117+20
	bl	Alert_func_call_with_null_ptr_with_filename
	b	.L114
.L116:
	mov	r4, #0
.L114:
	ldr	r3, .L117
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, pc}
.L118:
	.align	2
.L117:
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	1625
	.word	1649
	.word	.LC11
	.word	1656
.LFE17:
	.size	SYSTEM_PRESERVES_is_this_valve_allowed_ON, .-SYSTEM_PRESERVES_is_this_valve_allowed_ON
	.section	.text.SYSTEM_PRESERVES_there_is_a_mlb,"ax",%progbits
	.align	2
	.global	SYSTEM_PRESERVES_there_is_a_mlb
	.type	SYSTEM_PRESERVES_there_is_a_mlb, %function
SYSTEM_PRESERVES_there_is_a_mlb:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, [r0, #0]
	movs	r0, r0, asl #8
	movne	r0, #1
	bx	lr
.LFE18:
	.size	SYSTEM_PRESERVES_there_is_a_mlb, .-SYSTEM_PRESERVES_there_is_a_mlb
	.section	.text.init_battery_backed_poc_preserves,"ax",%progbits
	.align	2
	.global	init_battery_backed_poc_preserves
	.type	init_battery_backed_poc_preserves, %function
init_battery_backed_poc_preserves:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L128
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI16:
	ldr	r2, .L128+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L128+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L128+12
	ldr	r1, .L128+16
	mov	r2, #8
	bl	strncmp
	cmp	r4, #0
	cmpeq	r0, #0
	beq	.L121
	mov	r1, r4
	ldr	r0, .L128+20
	bl	Alert_battery_backed_var_initialized
	ldr	r5, .L128+12
	mov	r4, #0
	mov	r6, #472
.L122:
	mla	r0, r6, r4, r5
	add	r4, r4, #1
	add	r0, r0, #20
	bl	nm_poc_preserves_complete_record_initialization
	cmp	r4, #12
	bne	.L122
	ldr	r0, .L128+12
	ldr	r1, .L128+16
	mov	r2, #16
	bl	strlcpy
.L121:
	ldr	r6, .L128+12
	mov	r4, #0
.LBB44:
	mov	r5, #0
.L124:
.LBE44:
	mov	r0, #472
	mla	r0, r4, r0, r6
	mov	ip, #0
	add	r0, r0, #20
	add	r3, r0, #148
.LBB45:
	mov	r2, ip
	mov	r7, #88
.L123:
	add	ip, ip, #1
	mul	lr, r7, ip
	str	r2, [r3, #-48]
	str	r2, [r3, #-44]
	str	r2, [r3, #-40]
	str	r2, [r3, #-36]
	str	r2, [r3, #-32]
	str	r2, [r3, #-28]
	str	r2, [r3, #-24]
	str	r2, [r3, #-20]
	str	r2, [r3, #-16]
	str	r2, [r3, #-12]
	str	r5, [r3, #-8]	@ float
	str	r5, [r3, #-4]	@ float
	str	r5, [r3], #88	@ float
	ldrb	r8, [r0, lr]	@ zero_extendqisi2
	cmp	ip, #3
	and	r8, r8, #240
	mov	r1, #0
	strb	r8, [r0, lr]
	bne	.L123
	str	r1, [r0, #352]
	str	r1, [r0, #356]
	mov	r2, #16
	add	r0, r0, #456
.LBE45:
	add	r4, r4, #1
.LBB46:
	bl	memset
.LBE46:
	cmp	r4, #12
	bne	.L124
	ldr	r3, .L128+12
	mov	r2, #1
	str	r2, [r3, #16]
	ldr	r3, .L128
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L129:
	.align	2
.L128:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC0
	.word	1812
	.word	poc_preserves
	.word	.LANCHOR3
	.word	.LC13
.LFE21:
	.size	init_battery_backed_poc_preserves, .-init_battery_backed_poc_preserves
	.section	.text.POC_PRESERVES_get_poc_preserve_for_this_poc_gid,"ax",%progbits
	.align	2
	.global	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	.type	POC_PRESERVES_get_poc_preserve_for_this_poc_gid, %function
POC_PRESERVES_get_poc_preserve_for_this_poc_gid:
.LFB22:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L135
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI17:
	ldr	r2, .L135+4
	mov	r5, #0
	str	r5, [r1, #0]
	mov	r6, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L135+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L135+12
.L133:
	ldr	r2, [r3, #24]
	cmp	r2, r6
	ldreq	r3, .L135+16
	moveq	r6, #472
	mlaeq	r6, r5, r6, r3
	streq	r5, [r4, #0]
	beq	.L132
.L131:
	add	r5, r5, #1
	cmp	r5, #12
	add	r3, r3, #472
	bne	.L133
	mov	r6, #0
.L132:
	ldr	r3, .L135
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, pc}
.L136:
	.align	2
.L135:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC0
	.word	1887
	.word	poc_preserves
	.word	poc_preserves+20
.LFE22:
	.size	POC_PRESERVES_get_poc_preserve_for_this_poc_gid, .-POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	.section	.text.POC_PRESERVES_get_poc_preserve_ptr_for_these_details,"ax",%progbits
	.align	2
	.global	POC_PRESERVES_get_poc_preserve_ptr_for_these_details
	.type	POC_PRESERVES_get_poc_preserve_ptr_for_these_details, %function
POC_PRESERVES_get_poc_preserve_ptr_for_these_details:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L145
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI18:
	mov	r6, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	mov	r3, #1920
	mov	r5, r2
	ldr	r2, .L145+4
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L145+8
	mov	r2, #0
	mov	r0, #472
	mov	r1, r3
.L142:
	ldr	ip, [r3, #28]
	cmp	ip, r6
	bne	.L138
	ldr	ip, [r3, #32]
	cmp	ip, r4
	bne	.L138
	cmp	r4, #10
	cmpne	r4, #12
	beq	.L144
.L139:
	ldr	ip, [r3, #112]
	cmp	ip, r5
	bne	.L138
.L144:
	mla	r7, r0, r2, r1
	add	r7, r7, #20
	cmp	r7, #0
	bne	.L141
.L138:
	add	r2, r2, #1
	cmp	r2, #12
	add	r3, r3, #472
	bne	.L142
	mov	r7, #0
.L141:
	ldr	r3, .L145
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r7
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L146:
	.align	2
.L145:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC0
	.word	poc_preserves
.LFE23:
	.size	POC_PRESERVES_get_poc_preserve_ptr_for_these_details, .-POC_PRESERVES_get_poc_preserve_ptr_for_these_details
	.section	.text.POC_PRESERVES_get_poc_preserve_for_the_sync_items,"ax",%progbits
	.align	2
	.global	POC_PRESERVES_get_poc_preserve_for_the_sync_items
	.type	POC_PRESERVES_get_poc_preserve_for_the_sync_items, %function
POC_PRESERVES_get_poc_preserve_for_the_sync_items:
.LFB24:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L152
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI19:
	ldr	r2, .L152+4
	sub	sp, sp, #48
.LCFI20:
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L152+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r5, .L152+12
	mov	r6, #0
	mov	r7, #1
.L150:
	mov	r1, #0
	mov	r2, #48
	mov	r0, sp
	bl	memset
	ldr	r3, [r5, #24]
	mov	r0, sp
	str	r3, [sp, #0]
	ldr	r3, [r5, #28]
	mov	r1, r4
	str	r3, [sp, #4]
	ldr	r3, [r5, #32]
	mov	r2, #48
	str	r3, [sp, #8]
	ldr	r3, [r5, #40]
	str	r7, [sp, #12]
	str	r3, [sp, #16]
	ldr	r3, [r5, #112]
	str	r3, [sp, #20]
	ldr	r3, [r5, #116]
	str	r3, [sp, #32]
	ldr	r3, [r5, #200]
	str	r3, [sp, #24]
	ldr	r3, [r5, #204]
	str	r3, [sp, #36]
	ldr	r3, [r5, #288]
	str	r3, [sp, #28]
	ldr	r3, [r5, #292]
	str	r3, [sp, #40]
	ldr	r3, [r5, #44]
	str	r3, [sp, #44]
	bl	memcmp
	cmp	r0, #0
	ldreq	r3, .L152+16
	moveq	r2, #472
	mlaeq	r6, r2, r6, r3
	beq	.L149
.L148:
	add	r6, r6, #1
	cmp	r6, #12
	add	r5, r5, #472
	bne	.L150
	mov	r6, #0
.L149:
	ldr	r3, .L152
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L153:
	.align	2
.L152:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC0
	.word	1981
	.word	poc_preserves
	.word	poc_preserves+20
.LFE24:
	.size	POC_PRESERVES_get_poc_preserve_for_the_sync_items, .-POC_PRESERVES_get_poc_preserve_for_the_sync_items
	.section	.text.POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters,"ax",%progbits
	.align	2
	.global	POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters
	.type	POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters, %function
POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI21:
	mov	r7, r3
	ldr	r3, .L164
	mov	r6, #0
	str	r6, [r2, #0]
	mov	r8, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	mov	r5, r2
	mov	r3, #2048
	ldr	r2, .L164+4
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L164+8
	mov	ip, #472
	mov	r1, #88
.L160:
	mla	r3, ip, r6, r0
	add	r3, r3, #20
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L155
	ldr	r2, [r3, #8]
	cmp	r2, r8
	bne	.L155
	cmp	r4, #0
	beq	.L156
	ldr	r2, [r3, #92]
	cmp	r2, r4
	beq	.L161
	ldr	r2, [r3, #180]
	cmp	r2, r4
	beq	.L162
	ldr	r2, [r3, #268]
	cmp	r2, r4
	moveq	r2, #2
	bne	.L155
	b	.L157
.L161:
	mov	r2, #0
	b	.L157
.L162:
	mov	r2, #1
.L157:
	add	sl, r2, #1
	str	r3, [r5, #0]
	mla	sl, r1, sl, r3
	str	r2, [r7, #0]
	b	.L158
.L156:
	ldr	r2, [r3, #12]
	cmp	r2, #10
	bne	.L155
	ldr	r2, [r3, #92]
	cmp	r2, #0
	bne	.L155
	add	sl, r3, #88
	str	r3, [r5, #0]
	str	r4, [r7, #0]
.L158:
	cmp	sl, #0
	bne	.L159
.L155:
	add	r6, r6, #1
	cmp	r6, #12
	bne	.L160
	mov	sl, #0
.L159:
	ldr	r3, .L164
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, sl
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L165:
	.align	2
.L164:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC0
	.word	poc_preserves
.LFE25:
	.size	POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters, .-POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters
	.section	.text.POC_PRESERVES_synchronize_preserves_to_file,"ax",%progbits
	.align	2
	.global	POC_PRESERVES_synchronize_preserves_to_file
	.type	POC_PRESERVES_synchronize_preserves_to_file, %function
POC_PRESERVES_synchronize_preserves_to_file:
.LFB28:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L192
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI22:
	ldr	r0, [r3, #0]
	sub	sp, sp, #56
.LCFI23:
	mov	r1, #400
	ldr	r2, .L192+4
	ldr	r3, .L192+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L192+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L192+4
	ldr	r3, .L192+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L192+20
	ldr	r3, .L192+24
	mov	r2, #0
.L167:
	str	r2, [r3, #20]
	add	r3, r3, #472
	cmp	r3, r1
	bne	.L167
	bl	POC_get_list_count
	mov	r4, #0
	mov	r5, #1
	mov	r6, r0
	b	.L168
.L172:
	mov	r0, r4
	bl	POC_get_group_at_this_index
	cmp	r0, #0
	bne	.L169
	ldr	r0, .L192+4
	bl	RemovePathFromFileName
	ldr	r2, .L192+28
	mov	r1, r0
	ldr	r0, .L192+32
	b	.L190
.L169:
	mov	r1, sp
	bl	POC_load_fields_syncd_to_the_poc_preserves
	ldr	r3, [sp, #12]
	cmp	r3, #0
	beq	.L171
	ldr	r3, [sp, #16]
	cmp	r3, #0
	beq	.L171
	mov	r0, sp
	bl	POC_PRESERVES_get_poc_preserve_for_the_sync_items
	cmp	r0, #0
	strne	r5, [r0, #0]
.L171:
	add	r4, r4, #1
.L168:
	cmp	r4, r6
	bne	.L172
	ldr	r4, .L192+24
	mov	r5, #0
	mov	r7, #472
	mov	r6, r4
.L174:
	ldr	r3, [r4, #24]
	cmp	r3, #0
	beq	.L173
	ldr	r0, [r4, #20]
	cmp	r0, #0
	bne	.L173
	bl	Alert_poc_preserves_activity
	mla	r0, r7, r5, r6
	add	r0, r0, #20
	bl	nm_poc_preserves_complete_record_initialization
.L173:
	add	r5, r5, #1
	cmp	r5, #12
	add	r4, r4, #472
	bne	.L174
	b	.L191
.L181:
	mov	r0, r4
	bl	POC_get_group_at_this_index
	cmp	r0, #0
	bne	.L176
	ldr	r0, .L192+4
	bl	RemovePathFromFileName
	ldr	r2, .L192+36
	mov	r1, r0
	ldr	r0, .L192+32
.L190:
	bl	Alert_Message_va
	mov	r3, #1
	b	.L170
.L176:
	mov	r1, sp
	bl	POC_load_fields_syncd_to_the_poc_preserves
	ldr	r3, [sp, #12]
	cmp	r3, #0
	beq	.L177
	ldr	r3, [sp, #16]
	cmp	r3, #0
	beq	.L177
	mov	r0, sp
	bl	POC_PRESERVES_get_poc_preserve_for_the_sync_items
	subs	r7, r0, #0
	bne	.L178
	mov	r0, #1
	bl	Alert_poc_preserves_activity
	ldr	r3, .L192+24
.L180:
.LBB52:
	ldr	r2, [r3, #24]
	cmp	r2, #0
	bne	.L179
.LBB53:
	mov	r3, #472
	mul	r7, r3, r7
	add	r8, r7, #20
	add	r8, r8, r5
	mov	r0, r8
	bl	nm_poc_preserves_complete_record_initialization
.LBB54:
	ldr	r3, [sp, #4]
	ldr	r0, [sp, #0]
	str	r3, [r8, #8]
	ldr	r3, [sp, #8]
	str	r0, [r8, #4]
	str	r3, [r8, #12]
	ldr	r3, [sp, #16]
.LBE54:
	add	r7, r5, r7
.LBB55:
	str	r3, [r8, #20]
	ldr	r3, [sp, #20]
	str	r3, [r8, #92]
	ldr	r3, [sp, #32]
	str	r3, [r8, #96]
	ldr	r3, [sp, #24]
	str	r3, [r8, #180]
	ldr	r3, [sp, #36]
	str	r3, [r8, #184]
	ldr	r3, [sp, #28]
	str	r3, [r8, #268]
	ldr	r3, [sp, #40]
	str	r3, [r8, #272]
	bl	POC_get_GID_irrigation_system_using_POC_gid
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	str	r0, [r8, #24]
.LBE55:
	add	r0, sp, #48
	bl	EPSON_obtain_latest_time_and_date
	ldrh	r3, [sp, #52]
	strh	r3, [r7, #56]	@ movhi
	ldr	r3, [sp, #48]
	str	r3, [r7, #52]
	ldr	r3, [sp, #0]
	str	r3, [r7, #48]
	b	.L177
.L179:
.LBE53:
	add	r7, r7, #1
	cmp	r7, #12
	add	r3, r3, #472
	bne	.L180
	ldr	r0, .L192+4
	bl	RemovePathFromFileName
	ldr	r2, .L192+40
	mov	r1, r0
	ldr	r0, .L192+44
	bl	Alert_Message_va
	b	.L177
.L178:
.LBE52:
	ldr	r3, [r5, #16]
	cmp	r3, #0
	beq	.L177
.LBB56:
	ldr	r3, [sp, #4]
	ldr	r0, [sp, #0]
	str	r3, [r7, #8]
	ldr	r3, [sp, #8]
	str	r0, [r7, #4]
	str	r3, [r7, #12]
	ldr	r3, [sp, #16]
	str	r3, [r7, #20]
	ldr	r3, [sp, #20]
	str	r3, [r7, #92]
	ldr	r3, [sp, #32]
	str	r3, [r7, #96]
	ldr	r3, [sp, #24]
	str	r3, [r7, #180]
	ldr	r3, [sp, #36]
	str	r3, [r7, #184]
	ldr	r3, [sp, #28]
	str	r3, [r7, #268]
	ldr	r3, [sp, #40]
	str	r3, [r7, #272]
	bl	POC_get_GID_irrigation_system_using_POC_gid
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	str	r0, [r7, #24]
.L177:
.LBE56:
	add	r4, r4, #1
.L184:
	cmp	r4, r6
	bne	.L181
	mov	r3, #0
.L170:
	ldr	r5, .L192+24
	mov	r4, #0
	cmp	r3, r4
	str	r4, [r5, #16]
	beq	.L182
	mov	r6, #472
.L183:
	mla	r0, r6, r4, r5
	add	r4, r4, #1
	add	r0, r0, #20
	bl	nm_poc_preserves_complete_record_initialization
	cmp	r4, #12
	bne	.L183
.L182:
	ldr	r3, .L192+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L192
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #56
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L191:
	bl	POC_get_list_count
	mov	r4, #0
	ldr	r5, .L192+24
	mov	r6, r0
	b	.L184
.L193:
	.align	2
.L192:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC0
	.word	2285
	.word	list_poc_recursive_MUTEX
	.word	2290
	.word	poc_preserves+5664
	.word	poc_preserves
	.word	2310
	.word	.LC14
	.word	2386
	.word	2225
	.word	.LC15
.LFE28:
	.size	POC_PRESERVES_synchronize_preserves_to_file, .-POC_PRESERVES_synchronize_preserves_to_file
	.section	.text.POC_PRESERVES_request_a_resync,"ax",%progbits
	.align	2
	.global	POC_PRESERVES_request_a_resync
	.type	POC_PRESERVES_request_a_resync, %function
POC_PRESERVES_request_a_resync:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L195
	mov	r2, #1
	str	r2, [r3, #16]
	bx	lr
.L196:
	.align	2
.L195:
	.word	poc_preserves
.LFE29:
	.size	POC_PRESERVES_request_a_resync, .-POC_PRESERVES_request_a_resync
	.section	.text.POC_PRESERVES_set_master_valve_energized_bit,"ax",%progbits
	.align	2
	.global	POC_PRESERVES_set_master_valve_energized_bit
	.type	POC_PRESERVES_set_master_valve_energized_bit, %function
POC_PRESERVES_set_master_valve_energized_bit:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L205
	stmfd	sp!, {r4, r5, lr}
.LCFI24:
	ldr	r2, .L205+4
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L205+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r5, #444
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	bne	.L198
	ldr	r2, [r4, #8]
	cmp	r2, #1
	bne	.L204
	b	.L201
.L198:
	ldr	r2, .L205+12
	cmp	r5, r2
	bne	.L201
	ldr	r2, [r4, #8]
	cmp	r2, #1
	bne	.L201
.L204:
	orr	r3, r3, #1
	b	.L203
.L201:
	bic	r3, r3, #1
.L203:
	strb	r3, [r4, #0]
	ldr	r3, .L205
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L206:
	.align	2
.L205:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC0
	.word	2497
	.word	333
.LFE30:
	.size	POC_PRESERVES_set_master_valve_energized_bit, .-POC_PRESERVES_set_master_valve_energized_bit
	.section	.text.load_this_box_configuration_with_our_own_info,"ax",%progbits
	.align	2
	.global	load_this_box_configuration_with_our_own_info
	.type	load_this_box_configuration_with_our_own_info, %function
load_this_box_configuration_with_our_own_info:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI25:
	ldr	r5, .L208
	ldr	r6, .L208+4
	ldr	r3, [r5, #48]
	mov	r4, r0
	add	r1, r5, #52
	str	r3, [r0], #4
	mov	r2, #4
	bl	memcpy
	ldr	r3, .L208+8
	mov	r1, #400
	ldr	r2, .L208+12
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L208+16
	mov	r2, #56
	add	r0, r4, #8
	bl	memcpy
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, [r5, #80]
	str	r3, [r4, #64]
	ldr	r3, [r5, #84]
	str	r3, [r4, #68]
	ldmfd	sp!, {r4, r5, r6, pc}
.L209:
	.align	2
.L208:
	.word	config_c
	.word	tpmicro_data_recursive_MUTEX
	.word	2564
	.word	.LC0
	.word	tpmicro_comm+116
.LFE31:
	.size	load_this_box_configuration_with_our_own_info, .-load_this_box_configuration_with_our_own_info
	.section	.text.init_battery_backed_chain_members,"ax",%progbits
	.align	2
	.global	init_battery_backed_chain_members
	.type	init_battery_backed_chain_members, %function
init_battery_backed_chain_members:
.LFB32:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L215
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI26:
	ldr	r2, .L215+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L215+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L215+12
	ldr	r1, .L215+16
	mov	r2, #10
	bl	strncmp
	cmp	r4, #0
	cmpeq	r0, #0
	moveq	r0, #0
	movne	r0, #1
	beq	.L213
	mov	r1, r4
	ldr	r0, .L215+20
	bl	Alert_battery_backed_var_initialized
	mov	r1, #0
	mov	r2, #1120
	ldr	r0, .L215+12
	bl	memset
	ldr	r1, .L215+16
	mov	r2, #16
	ldr	r0, .L215+12
	bl	strlcpy
	b	.L212
.L213:
	ldr	r5, .L215+12
	mov	r4, r0
	mov	r6, #92
.L211:
	add	r4, r4, #1
	mla	r0, r6, r4, r5
	mov	r1, #0
	mov	r2, #16
	bl	memset
	cmp	r4, #12
	bne	.L211
.L212:
	ldr	r3, .L215
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L216:
	.align	2
.L215:
	.word	chain_members_recursive_MUTEX
	.word	.LC0
	.word	2587
	.word	chain
	.word	.LANCHOR4
	.word	.LC16
.LFE32:
	.size	init_battery_backed_chain_members, .-init_battery_backed_chain_members
	.section	.text.controller_is_2_wire_only,"ax",%progbits
	.align	2
	.global	controller_is_2_wire_only
	.type	controller_is_2_wire_only, %function
controller_is_2_wire_only:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #11
	bhi	.L225
	ldr	r1, .L234
	mov	r2, #92
	mla	r2, r0, r2, r1
	ldr	r3, [r2, #80]
	cmp	r3, #0
	moveq	r0, r3
	bxeq	lr
	ldrb	r3, [r2, #28]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L220
	tst	r3, #2
	bne	.L231
.L220:
	mov	r3, #92
	mla	r1, r3, r0, r1
	ldrb	r3, [r1, #32]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L221
	tst	r3, #2
	bne	.L231
.L221:
	ldr	r2, .L234
	mov	r3, #92
	mla	r3, r0, r3, r2
	ldrb	r3, [r3, #36]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L222
	tst	r3, #2
	bne	.L231
.L222:
	mov	r3, #92
	mla	r2, r3, r0, r2
	ldrb	r3, [r2, #40]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L223
	tst	r3, #2
	bne	.L231
.L223:
	ldr	r2, .L234
	mov	r3, #92
	mla	r3, r0, r3, r2
	ldrb	r3, [r3, #44]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L224
	tst	r3, #2
	bne	.L231
.L224:
	mov	r3, #92
	mla	r0, r3, r0, r2
	ldrb	r3, [r0, #48]	@ zero_extendqisi2
	tst	r3, #2
	movne	r0, #0
	moveq	r0, #1
	tst	r3, #1
	bxne	lr
	b	.L233
.L231:
	mov	r0, #0
	bx	lr
.L233:
	mov	r0, #1
	bx	lr
.L225:
	mov	r0, #0
	bx	lr
.L235:
	.align	2
.L234:
	.word	chain
.LFE33:
	.size	controller_is_2_wire_only, .-controller_is_2_wire_only
	.section	.text.init_battery_backed_general_use,"ax",%progbits
	.align	2
	.global	init_battery_backed_general_use
	.type	init_battery_backed_general_use, %function
init_battery_backed_general_use:
.LFB34:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI27:
	subs	r4, r0, #0
	bne	.L237
	ldr	r0, .L239
	ldr	r1, .L239+4
	mov	r2, #12
	bl	strncmp
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
.L237:
	mov	r1, r4
	ldr	r0, .L239+8
	bl	Alert_battery_backed_var_initialized
	mov	r1, #0
	mov	r2, #120
	ldr	r0, .L239
	bl	memset
	ldr	r0, .L239
	ldr	r1, .L239+4
	mov	r2, #16
	ldmfd	sp!, {r4, lr}
	b	strlcpy
.L240:
	.align	2
.L239:
	.word	battery_backed_general_use
	.word	.LANCHOR5
	.word	.LC17
.LFE34:
	.size	init_battery_backed_general_use, .-init_battery_backed_general_use
	.section	.text.nm_LIGHTS_PRESERVES_restart_lights,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_PRESERVES_restart_lights
	.type	nm_LIGHTS_PRESERVES_restart_lights, %function
nm_LIGHTS_PRESERVES_restart_lights:
.LFB36:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	strh	r3, [r0, #16]	@ movhi
	bx	lr
.LFE36:
	.size	nm_LIGHTS_PRESERVES_restart_lights, .-nm_LIGHTS_PRESERVES_restart_lights
	.section	.text.nm_LIGHTS_PRESERVES_complete_record_initialization,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_PRESERVES_complete_record_initialization
	.type	nm_LIGHTS_PRESERVES_complete_record_initialization, %function
nm_LIGHTS_PRESERVES_complete_record_initialization:
.LFB37:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, lr}
.LCFI28:
	ldr	r6, .L247
	mov	r4, r0
	mov	r0, sp
	bl	EPSON_obtain_latest_time_and_date
	mov	r0, r4
	mov	r1, #0
	mov	r2, #20
	bl	memset
	mov	r4, #0
	b	.L243
.L244:
	mov	r1, r5
	mov	r0, r4
	bl	LIGHTS_get_lights_array_index
	ldrh	r3, [sp, #4]
	mla	r0, r7, r0, r6
	strb	r5, [r0, #29]
	add	r5, r5, #1
	cmp	r5, #4
	strh	r3, [r0, #24]	@ movhi
	strb	r4, [r0, #28]
	bne	.L244
	add	r4, r4, #1
	cmp	r4, #12
	beq	.L242
.L243:
	mov	r5, #0
	mov	r7, #20
	b	.L244
.L242:
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, pc}
.L248:
	.align	2
.L247:
	.word	lights_preserves
.LFE37:
	.size	nm_LIGHTS_PRESERVES_complete_record_initialization, .-nm_LIGHTS_PRESERVES_complete_record_initialization
	.section	.text.init_battery_backed_lights_preserves,"ax",%progbits
	.align	2
	.global	init_battery_backed_lights_preserves
	.type	init_battery_backed_lights_preserves, %function
init_battery_backed_lights_preserves:
.LFB38:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L257
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI29:
	ldr	r2, .L257+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L257+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L257+12
	ldr	r1, .L257+16
	mov	r2, #11
	bl	strncmp
	cmp	r4, #0
	cmpeq	r0, #0
	beq	.L250
	mov	r1, r4
	ldr	r0, .L257+20
	bl	Alert_battery_backed_var_initialized
	ldr	r5, .L257+12
	mov	r4, #0
	mov	r6, #20
.L251:
	mla	r0, r6, r4, r5
	add	r4, r4, #1
	add	r0, r0, #16
	bl	nm_LIGHTS_PRESERVES_complete_record_initialization
	cmp	r4, #48
	bne	.L251
	ldr	r0, .L257+12
	ldr	r1, .L257+16
	mov	r2, #16
	bl	strlcpy
.L250:
	mov	r4, #0
	mov	r7, r4
.LBB57:
	ldr	r6, .L257+12
	b	.L252
.L253:
	mov	r1, r5
	mov	r0, r4
	bl	LIGHTS_get_lights_array_index
.LBE57:
	add	r5, r5, #1
	cmp	r5, #4
.LBB58:
	add	r3, r6, r0
	ldrb	r2, [r3, #976]	@ zero_extendqisi2
	mla	r0, r8, r0, r6
	and	r2, r2, #12
	strb	r2, [r3, #976]
	strh	r7, [r0, #32]	@ movhi
	ldrb	r1, [r3, #976]	@ zero_extendqisi2
	and	r1, r1, #243
	strb	r1, [r3, #976]
	strh	r7, [r0, #34]	@ movhi
.LBE58:
	bne	.L253
	add	r4, r4, #1
	cmp	r4, #12
	beq	.L254
.L252:
	mov	r5, #0
.LBB59:
	mov	r8, #20
	b	.L253
.L254:
.LBE59:
	ldr	r3, .L257
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L258:
	.align	2
.L257:
	.word	lights_preserves_recursive_MUTEX
	.word	.LC0
	.word	2849
	.word	lights_preserves
	.word	.LANCHOR6
	.word	.LC18
.LFE38:
	.size	init_battery_backed_lights_preserves, .-init_battery_backed_lights_preserves
	.section	.text.obsolete_LIGHTS_PRESERVES_get_preserves_index,"ax",%progbits
	.align	2
	.global	obsolete_LIGHTS_PRESERVES_get_preserves_index
	.type	obsolete_LIGHTS_PRESERVES_get_preserves_index, %function
obsolete_LIGHTS_PRESERVES_get_preserves_index:
.LFB39:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r0, r1, r0, asl #2
	bx	lr
.LFE39:
	.size	obsolete_LIGHTS_PRESERVES_get_preserves_index, .-obsolete_LIGHTS_PRESERVES_get_preserves_index
	.section	.text.LIGHTS_PRESERVES_set_bit_field_bit,"ax",%progbits
	.align	2
	.global	LIGHTS_PRESERVES_set_bit_field_bit
	.type	LIGHTS_PRESERVES_set_bit_field_bit, %function
LIGHTS_PRESERVES_set_bit_field_bit:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r2, #3
	cmpls	r0, #11
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI30:
	mov	r6, r2
	mov	r5, r3
	ldmhifd	sp!, {r4, r5, r6, pc}
	cmp	r1, #3
	ldmhifd	sp!, {r4, r5, r6, pc}
	bl	LIGHTS_get_lights_array_index
	ldr	r3, .L272
	mov	r1, #400
	ldr	r2, .L272+4
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L272+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L272+12
	cmp	r6, #2
	add	r4, r3, r4
	ldrb	r3, [r4, #976]	@ zero_extendqisi2
	beq	.L264
	cmp	r6, #3
	beq	.L265
	cmp	r6, #1
	beq	.L263
	cmp	r5, #0
	orrne	r3, r3, #1
	biceq	r3, r3, #1
	b	.L271
.L263:
	cmp	r5, #0
	orrne	r3, r3, #2
	biceq	r3, r3, #2
	b	.L271
.L264:
	cmp	r5, #0
	orrne	r3, r3, #4
	biceq	r3, r3, #4
	b	.L271
.L265:
	cmp	r5, #0
	orrne	r3, r3, #8
	biceq	r3, r3, #8
.L271:
	strb	r3, [r4, #976]
	ldr	r3, .L272
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L273:
	.align	2
.L272:
	.word	lights_preserves_recursive_MUTEX
	.word	.LC0
	.word	2952
	.word	lights_preserves
.LFE40:
	.size	LIGHTS_PRESERVES_set_bit_field_bit, .-LIGHTS_PRESERVES_set_bit_field_bit
	.section	.rodata.STATION_PRESERVES_VERIFY_STRING_PRE,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	STATION_PRESERVES_VERIFY_STRING_PRE, %object
	.size	STATION_PRESERVES_VERIFY_STRING_PRE, 12
STATION_PRESERVES_VERIFY_STRING_PRE:
	.ascii	"STATION v05\000"
	.section	.rodata.SYSTEM_PRESERVES_VERIFY_STRING_PRE,"a",%progbits
	.set	.LANCHOR2,. + 0
	.type	SYSTEM_PRESERVES_VERIFY_STRING_PRE, %object
	.size	SYSTEM_PRESERVES_VERIFY_STRING_PRE, 11
SYSTEM_PRESERVES_VERIFY_STRING_PRE:
	.ascii	"SYSTEM v04\000"
	.section	.rodata.POC_PRESERVES_VERIFY_STRING_PRE,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	POC_PRESERVES_VERIFY_STRING_PRE, %object
	.size	POC_PRESERVES_VERIFY_STRING_PRE, 8
POC_PRESERVES_VERIFY_STRING_PRE:
	.ascii	"POC v02\000"
	.section	.rodata.CM_PRE_TEST_STRING,"a",%progbits
	.set	.LANCHOR4,. + 0
	.type	CM_PRE_TEST_STRING, %object
	.size	CM_PRE_TEST_STRING, 10
CM_PRE_TEST_STRING:
	.ascii	"Chain v01\000"
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/battery_backed_funcs.c\000"
.LC1:
	.ascii	"Weather Preserves\000"
.LC2:
	.ascii	"Station Preserves\000"
.LC3:
	.ascii	"Box Index\000"
.LC4:
	.ascii	"Sta_Num\000"
.LC5:
	.ascii	"System Preserves\000"
.LC6:
	.ascii	"System GID should not be 0\000"
.LC7:
	.ascii	"SYSTEM_PRESERVES : unexp NULL ptr : %s, %u\000"
.LC8:
	.ascii	"SYSTEM_PRESERVES : unexp NULL gid : %s, %u\000"
.LC9:
	.ascii	"SYSTEM: unexpected assignment request\000"
.LC10:
	.ascii	"System Preserves problem : %s, %u\000"
.LC11:
	.ascii	"FOAL_IRRI: too many valves ON. : %s, %u\000"
.LC12:
	.ascii	"system_preserve problem : %s, %u\000"
.LC13:
	.ascii	"POC Preserves\000"
.LC14:
	.ascii	"POC_PRESERVES : unexp NULL ptr : %s, %u\000"
.LC15:
	.ascii	"POC Preserves : no open records : %s, %u\000"
.LC16:
	.ascii	"Chain Members\000"
.LC17:
	.ascii	"General Use\000"
.LC18:
	.ascii	"LIGHTS Preserves\000"
	.section	.rodata.GU_PRE_TEST_STRING,"a",%progbits
	.set	.LANCHOR5,. + 0
	.type	GU_PRE_TEST_STRING, %object
	.size	GU_PRE_TEST_STRING, 12
GU_PRE_TEST_STRING:
	.ascii	"general v01\000"
	.section	.rodata.LIGHTS_PRE_TEST_STRING,"a",%progbits
	.set	.LANCHOR6,. + 0
	.type	LIGHTS_PRE_TEST_STRING, %object
	.size	LIGHTS_PRE_TEST_STRING, 11
LIGHTS_PRE_TEST_STRING:
	.ascii	"lights v03\000"
	.section	.rodata.WEATHER_PRESERVES_VERIFY_STRING_PRE,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	WEATHER_PRESERVES_VERIFY_STRING_PRE, %object
	.size	WEATHER_PRESERVES_VERIFY_STRING_PRE, 12
WEATHER_PRESERVES_VERIFY_STRING_PRE:
	.ascii	"WEATHER v01\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI0-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI5-.LFB3
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI6-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI7-.LFB8
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI8-.LFB10
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI9-.LFB11
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI10-.LFB12
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI11-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI12-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI13-.LFB15
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x82
	.uleb128 0xa
	.byte	0x81
	.uleb128 0xb
	.byte	0x80
	.uleb128 0xc
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI14-.LFB16
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI15-.LFB17
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI16-.LFB21
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI17-.LFB22
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI18-.LFB23
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI19-.LFB24
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI20-.LCFI19
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI21-.LFB25
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI22-.LFB28
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI24-.LFB30
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI25-.LFB31
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI26-.LFB32
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI27-.LFB34
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI28-.LFB37
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI29-.LFB38
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI30-.LFB40
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE66:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/battery_backed_funcs.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x31d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF41
	.byte	0x1
	.4byte	.LASF42
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1f8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x3ab
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x857
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x6a8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0xaac
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x6fb
	.4byte	.LFB20
	.4byte	.LFE20
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x287
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.byte	0x50
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.byte	0xcf
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x172
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x1a7
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1d7
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x2bd
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x3da
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x431
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x473
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST9
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x497
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST10
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x261
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST11
	.uleb128 0x2
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x4cb
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x52f
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST12
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x61a
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST13
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x653
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST14
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x681
	.4byte	.LFB18
	.4byte	.LFE18
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x710
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST15
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x74d
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST16
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x773
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST17
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x7ac
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST18
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x7ea
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST19
	.uleb128 0x2
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x885
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x8d1
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST20
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x9b1
	.4byte	.LFB29
	.4byte	.LFE29
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x9b8
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST21
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x9f4
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST22
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF33
	.byte	0x1
	.2byte	0xa12
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST23
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.2byte	0xa52
	.4byte	.LFB33
	.4byte	.LFE33
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.2byte	0xa74
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST24
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF36
	.byte	0x1
	.2byte	0xad7
	.4byte	.LFB36
	.4byte	.LFE36
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF37
	.byte	0x1
	.2byte	0xaf1
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST25
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.2byte	0xb1d
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST26
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF39
	.byte	0x1
	.2byte	0xb66
	.4byte	.LFB39
	.4byte	.LFE39
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF40
	.byte	0x1
	.2byte	0xb7f
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST27
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB7
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB3
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB8
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB10
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB11
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB12
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB13
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB6
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB15
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB16
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB17
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB21
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB22
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB23
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB24
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI20
	.4byte	.LFE24
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB25
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB28
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI23
	.4byte	.LFE28
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB30
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB31
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB32
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB34
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB37
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB38
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB40
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x124
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF18:
	.ascii	"_nm_SYSTEM_register_system_preserve_for_this_system"
	.ascii	"_gid\000"
.LASF10:
	.ascii	"STATION_PRESERVES_get_index_using_ptr_to_station_st"
	.ascii	"ruct\000"
.LASF40:
	.ascii	"LIGHTS_PRESERVES_set_bit_field_bit\000"
.LASF41:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"_nm_system_preserves_init_portion_initialized_on_ea"
	.ascii	"ch_program_start\000"
.LASF6:
	.ascii	"_nm_sync_users_settings_to_this_system_preserve\000"
.LASF39:
	.ascii	"obsolete_LIGHTS_PRESERVES_get_preserves_index\000"
.LASF13:
	.ascii	"nm_SYSTEM_PRESERVES_complete_record_initialization\000"
.LASF19:
	.ascii	"SYSTEM_PRESERVES_synchronize_preserves_to_file\000"
.LASF9:
	.ascii	"STATION_PRESERVES_get_index_using_box_index_and_sta"
	.ascii	"tion_number\000"
.LASF2:
	.ascii	"_nm_sync_users_settings_to_this_poc_preserve\000"
.LASF4:
	.ascii	"nm_lights_preserves_init_portion_initialized_on_eac"
	.ascii	"h_program_start\000"
.LASF30:
	.ascii	"POC_PRESERVES_request_a_resync\000"
.LASF0:
	.ascii	"_nm_system_preserves_init_ufim_variables\000"
.LASF5:
	.ascii	"nm_poc_preserves_complete_record_initialization\000"
.LASF16:
	.ascii	"SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_s"
	.ascii	"ystem_gid\000"
.LASF37:
	.ascii	"nm_LIGHTS_PRESERVES_complete_record_initialization\000"
.LASF36:
	.ascii	"nm_LIGHTS_PRESERVES_restart_lights\000"
.LASF38:
	.ascii	"init_battery_backed_lights_preserves\000"
.LASF29:
	.ascii	"POC_PRESERVES_synchronize_preserves_to_file\000"
.LASF34:
	.ascii	"controller_is_2_wire_only\000"
.LASF31:
	.ascii	"POC_PRESERVES_set_master_valve_energized_bit\000"
.LASF12:
	.ascii	"nm_SYSTEM_PRESERVES_restart_irrigation_and_block_fl"
	.ascii	"ow_checking_for_150_seconds\000"
.LASF11:
	.ascii	"init_rip_portion_of_system_record\000"
.LASF35:
	.ascii	"init_battery_backed_general_use\000"
.LASF32:
	.ascii	"load_this_box_configuration_with_our_own_info\000"
.LASF3:
	.ascii	"nm_poc_preserves_init_portion_initialized_on_each_p"
	.ascii	"rogram_start\000"
.LASF26:
	.ascii	"POC_PRESERVES_get_poc_preserve_for_the_sync_items\000"
.LASF14:
	.ascii	"init_battery_backed_system_preserves\000"
.LASF7:
	.ascii	"init_battery_backed_weather_preserves\000"
.LASF25:
	.ascii	"POC_PRESERVES_get_poc_preserve_ptr_for_these_detail"
	.ascii	"s\000"
.LASF23:
	.ascii	"init_battery_backed_poc_preserves\000"
.LASF17:
	.ascii	"SYSTEM_PRESERVES_request_a_resync\000"
.LASF20:
	.ascii	"SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limi"
	.ascii	"t\000"
.LASF28:
	.ascii	"nm_POC_register_poc_preserve_for_these_parameters\000"
.LASF8:
	.ascii	"init_battery_backed_station_preserves\000"
.LASF24:
	.ascii	"POC_PRESERVES_get_poc_preserve_for_this_poc_gid\000"
.LASF42:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/battery_backed_funcs.c\000"
.LASF22:
	.ascii	"SYSTEM_PRESERVES_there_is_a_mlb\000"
.LASF15:
	.ascii	"SYSTEM_PRESERVES_init_system_preserves_ufim_variabl"
	.ascii	"es_for_all_systems\000"
.LASF21:
	.ascii	"SYSTEM_PRESERVES_is_this_valve_allowed_ON\000"
.LASF27:
	.ascii	"POC_PRESERVES_get_poc_working_details_ptr_for_these"
	.ascii	"_parameters\000"
.LASF33:
	.ascii	"init_battery_backed_chain_members\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
