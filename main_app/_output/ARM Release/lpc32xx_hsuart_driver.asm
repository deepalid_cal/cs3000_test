	.file	"lpc32xx_hsuart_driver.c"
	.text
.Ltext0:
	.section	.text.hsuart_gen_int_handler,"ax",%progbits
	.align	2
	.global	hsuart_gen_int_handler
	.type	hsuart_gen_int_handler, %function
hsuart_gen_int_handler:
.LFB0:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, [r0, #0]
	stmfd	sp!, {r0, r1, lr}
.LCFI0:
	ldr	r3, [r2, #8]
	str	r0, [sp, #0]
	str	r3, [sp, #4]
	ldr	r3, [sp, #4]
	tst	r3, #6
	beq	.L2
	ldr	r3, [r0, #4]
	cmp	r3, #0
	ldreq	r3, [r2, #12]
	biceq	r3, r3, #64
	streq	r3, [r2, #12]
	beq	.L2
	mov	r0, sp
	blx	r3
.L2:
	ldr	r3, [sp, #4]
	tst	r3, #1
	beq	.L4
	ldr	r2, [sp, #0]
	ldr	r3, [r2, #8]
	cmp	r3, #0
	beq	.L5
	mov	r0, sp
	blx	r3
	b	.L4
.L5:
	ldr	r3, [r2, #0]
	ldr	r2, [r3, #12]
	bic	r2, r2, #32
	str	r2, [r3, #12]
.L4:
	ldr	r3, [sp, #4]
	tst	r3, #56
	beq	.L6
	ldr	r2, [sp, #0]
	ldr	r3, [r2, #12]
	cmp	r3, #0
	beq	.L7
	mov	r0, sp
	blx	r3
	b	.L6
.L7:
	ldr	r3, [r2, #0]
	ldr	r2, [r3, #12]
	bic	r2, r2, #128
	str	r2, [r3, #12]
.L6:
	ldr	r3, [sp, #0]
	ldr	r2, [sp, #4]
	ldr	r3, [r3, #0]
	str	r2, [r3, #8]
	ldmfd	sp!, {r2, r3, pc}
.LFE0:
	.size	hsuart_gen_int_handler, .-hsuart_gen_int_handler
	.section	.text.uart1_int_handler,"ax",%progbits
	.align	2
	.global	uart1_int_handler
	.type	uart1_int_handler, %function
uart1_int_handler:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L9
	b	hsuart_gen_int_handler
.L10:
	.align	2
.L9:
	.word	.LANCHOR0
.LFE1:
	.size	uart1_int_handler, .-uart1_int_handler
	.section	.text.uart2_int_handler,"ax",%progbits
	.align	2
	.global	uart2_int_handler
	.type	uart2_int_handler, %function
uart2_int_handler:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L12
	b	hsuart_gen_int_handler
.L13:
	.align	2
.L12:
	.word	.LANCHOR0+32
.LFE2:
	.size	uart2_int_handler, .-uart2_int_handler
	.section	.text.uart7_int_handler,"ax",%progbits
	.align	2
	.global	uart7_int_handler
	.type	uart7_int_handler, %function
uart7_int_handler:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L15
	b	hsuart_gen_int_handler
.L16:
	.align	2
.L15:
	.word	.LANCHOR0+64
.LFE3:
	.size	uart7_int_handler, .-uart7_int_handler
	.section	.text.hsuart_abs,"ax",%progbits
	.align	2
	.global	hsuart_abs
	.type	hsuart_abs, %function
hsuart_abs:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, r1
	rsbgt	r0, r1, r0
	rsble	r0, r0, r1
	bx	lr
.LFE4:
	.size	hsuart_abs, .-hsuart_abs
	.section	.text.hsuart_ptr_to_hsuart_num,"ax",%progbits
	.align	2
	.global	hsuart_ptr_to_hsuart_num
	.type	hsuart_ptr_to_hsuart_num, %function
hsuart_ptr_to_hsuart_num:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L25
	cmp	r0, r3
	moveq	r0, #0
	bxeq	lr
	add	r3, r3, #16384
	cmp	r0, r3
	beq	.L23
	add	r3, r3, #16384
	cmp	r0, r3
	moveq	r0, #2
	mvnne	r0, #0
	bx	lr
.L23:
	mov	r0, #1
	bx	lr
.L26:
	.align	2
.L25:
	.word	1073823744
.LFE5:
	.size	hsuart_ptr_to_hsuart_num, .-hsuart_ptr_to_hsuart_num
	.section	.text.hsuart_flush_fifos,"ax",%progbits
	.align	2
	.global	hsuart_flush_fifos
	.type	hsuart_flush_fifos, %function
hsuart_flush_fifos:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.L28:
	ldr	r3, [r0, #4]
	tst	r3, #65280
	bne	.L28
	b	.L32
.L30:
	ldr	r3, [r0, #0]
.L32:
	ldr	r3, [r0, #4]
	tst	r3, #255
	bne	.L30
	bx	lr
.LFE6:
	.size	hsuart_flush_fifos, .-hsuart_flush_fifos
	.global	__udivsi3
	.section	.text.hsuart_find_clk,"ax",%progbits
	.align	2
	.global	hsuart_find_clk
	.type	hsuart_find_clk, %function
hsuart_find_clk:
.LFB7:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI1:
	mov	r4, r0
	mov	r0, #5
	str	r1, [sp, #0]
	bl	clkpwr_get_base_clock_rate
	mov	r5, #0
	mov	r7, #14
	mov	r6, r5
	mvn	sl, #0
	mov	r8, r5
	mov	fp, r0
	b	.L39
.L40:
	mov	r6, r9
.L39:
	mov	r0, fp
	mov	r1, r7
	bl	__udivsi3
	add	r9, r6, #1
.LBB6:
	cmp	r0, r4
	rsbgt	r3, r4, r0
	rsble	r3, r0, r4
.LBE6:
	cmp	r3, sl
	bcs	.L36
.LBB7:
	cmp	r0, r4
	rsbgt	sl, r4, r0
	rsble	sl, r0, r4
.LBE7:
	mov	r5, r6
	mov	r8, r0
.L36:
	cmp	r9, #256
	add	r7, r7, #14
	bne	.L40
	ldr	r3, [sp, #0]
	mov	r0, r8
	str	r5, [r3, #24]
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.LFE7:
	.size	hsuart_find_clk, .-hsuart_find_clk
	.section	.text.hsuart_setup_trans_mode,"ax",%progbits
	.align	2
	.global	hsuart_setup_trans_mode
	.type	hsuart_setup_trans_mode, %function
hsuart_setup_trans_mode:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r1, #0]
	mov	r1, r4
	bl	hsuart_find_clk
	ldr	r3, [r4, #16]
	cmp	r3, #1
	str	r0, [r4, #20]
	beq	.L44
	cmp	r3, #2
	beq	.L45
	cmp	r3, #0
	ldreq	r2, [r4, #24]
	ldreq	r3, .L58
	beq	.L54
	b	.L57
.L44:
	ldr	r2, [r4, #24]
	ldr	r3, .L58+4
	b	.L54
.L45:
	ldr	r2, [r4, #24]
	ldr	r3, .L58+8
.L54:
	str	r2, [r3, #16]
	ldr	r3, [r5, #4]
	cmp	r3, #0
	ldr	r3, [r4, #0]
	ldr	r2, [r3, #12]
	biceq	r2, r2, #16384
	beq	.L55
	orr	r2, r2, #16384
	str	r2, [r3, #12]
	ldr	r2, [r5, #8]
	cmp	r2, #0
	ldr	r2, [r3, #12]
	orrne	r2, r2, #32768
	biceq	r2, r2, #32768
.L55:
	str	r2, [r3, #12]
	ldr	r3, [r4, #0]
	ldr	r0, [r5, #12]
	ldr	r2, [r3, #12]
	cmp	r0, #0
	biceq	r2, r2, #262144
	beq	.L56
	ldr	r0, [r5, #16]
	orr	r2, r2, #262144
	str	r2, [r3, #12]
	ldr	r2, [r3, #12]
	cmp	r0, #0
	orrne	r2, r2, #2097152
	strne	r2, [r3, #12]
	movne	r0, #0
	biceq	r2, r2, #2097152
	ldmnefd	sp!, {r4, r5, pc}
.L56:
	str	r2, [r3, #12]
	ldmfd	sp!, {r4, r5, pc}
.L57:
	mvn	r0, #0
	ldmfd	sp!, {r4, r5, pc}
.L59:
	.align	2
.L58:
	.word	1073823744
	.word	1073840128
	.word	1073856512
.LFE8:
	.size	hsuart_setup_trans_mode, .-hsuart_setup_trans_mode
	.section	.bss.hsuartdat,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	hsuartdat, %object
	.size	hsuartdat, 96
hsuartdat:
	.space	96
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI1-.LFB7
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI2-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_hsuart_driver.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xd5
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.byte	0xcf
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x3e
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x86
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x9e
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xb6
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	0x21
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xea
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x112
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x135
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x167
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB7
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB8
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"hsuart_gen_int_handler\000"
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_hsuart_driver.c\000"
.LASF10:
	.ascii	"hsuart_abs\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"hsuart_flush_fifos\000"
.LASF2:
	.ascii	"uart2_int_handler\000"
.LASF7:
	.ascii	"hsuart_setup_trans_mode\000"
.LASF3:
	.ascii	"uart7_int_handler\000"
.LASF1:
	.ascii	"uart1_int_handler\000"
.LASF4:
	.ascii	"hsuart_ptr_to_hsuart_num\000"
.LASF6:
	.ascii	"hsuart_find_clk\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
