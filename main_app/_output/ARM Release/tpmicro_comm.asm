	.file	"tpmicro_comm.c"
	.text
.Ltext0:
	.section	.text.msg_rate_timer_callback,"ax",%progbits
	.align	2
	.type	msg_rate_timer_callback, %function
msg_rate_timer_callback:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #1792
	b	COMM_MNGR_post_event
.LFE10:
	.size	msg_rate_timer_callback, .-msg_rate_timer_callback
	.section	.text.extract_decoder_fault_content_out_of_msg_from_tpmicro,"ax",%progbits
	.align	2
	.type	extract_decoder_fault_content_out_of_msg_from_tpmicro, %function
extract_decoder_fault_content_out_of_msg_from_tpmicro:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L7
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r2, .L7+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L7+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L7+12
	mov	r3, #0
.L5:
	mov	r2, r3, asl #4
	add	r0, r2, r1
	add	r0, r0, #5056
	ldr	r0, [r0, #36]
	cmp	r0, #0
	bne	.L3
	ldr	r0, .L7+12
	add	r2, r2, #5056
	add	r2, r2, #36
	add	r0, r0, r2
	ldr	r1, [r4, #0]
	mov	r2, #16
	bl	memcpy
	ldr	r3, [r4, #0]
	add	r3, r3, #16
	str	r3, [r4, #0]
	mov	r4, #1
	b	.L4
.L3:
	add	r3, r3, #1
	cmp	r3, #8
	bne	.L5
	mov	r4, #0
.L4:
	ldr	r3, .L7
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L8:
	.align	2
.L7:
	.word	tpmicro_data_recursive_MUTEX
	.word	.LC0
	.word	823
	.word	tpmicro_data
.LFE6:
	.size	extract_decoder_fault_content_out_of_msg_from_tpmicro, .-extract_decoder_fault_content_out_of_msg_from_tpmicro
	.section	.text.TP_MICRO_COMM_resync_wind_settings,"ax",%progbits
	.align	2
	.global	TP_MICRO_COMM_resync_wind_settings
	.type	TP_MICRO_COMM_resync_wind_settings, %function
TP_MICRO_COMM_resync_wind_settings:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L10
	mov	r2, #1
	str	r2, [r3, #0]
	bx	lr
.L11:
	.align	2
.L10:
	.word	tpmicro_data
.LFE8:
	.size	TP_MICRO_COMM_resync_wind_settings, .-TP_MICRO_COMM_resync_wind_settings
	.section	.text.TPMICRO_COMM_parse_incoming_message_from_tp_micro,"ax",%progbits
	.align	2
	.global	TPMICRO_COMM_parse_incoming_message_from_tp_micro
	.type	TPMICRO_COMM_parse_incoming_message_from_tp_micro, %function
TPMICRO_COMM_parse_incoming_message_from_tp_micro:
.LFB9:
	@ args = 0, pretend = 0, frame = 384
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI1:
	sub	sp, sp, #388
.LCFI2:
	mov	r6, r0
	mov	r4, r1
	mov	r2, #6
	add	r1, r0, #12
	add	r0, sp, #344
	str	r1, [sp, #352]
	bl	memcpy
	ldr	r1, [sp, #352]
	add	r0, sp, #512
	add	r1, r1, #6
	str	r1, [sp, #352]
	ldrh	r3, [r0, #-168]
	cmp	r3, #91
	bne	.L13
	ldr	r4, .L148
	mov	r7, #0
	str	r7, [r4, #12]
	ldrh	r3, [r0, #-166]
	ldrh	r2, [r0, #-164]
	orr	r3, r3, r2, asl #16
	tst	r3, #33554432
	beq	.L14
.LBB35:
	mov	r2, #4
	add	r0, r4, #64
	bl	memcpy
	ldr	r1, [sp, #352]
	mov	r2, #4
	add	r1, r1, #4
	add	r0, r4, #68
	str	r1, [sp, #352]
	bl	memcpy
	ldr	r3, [sp, #352]
.LBE35:
	add	r1, sp, #512
.LBB36:
	add	r3, r3, #4
	str	r3, [sp, #352]
.LBE36:
	ldr	r3, [r4, #64]
	mov	r5, #1
	strh	r3, [r1, #-172]	@ movhi
	ldr	r3, [r4, #68]
	mov	r1, #32
	str	r3, [sp, #336]
	add	r3, sp, #336
	ldmia	r3, {r2, r3}
	str	r5, [r4, #72]
	add	r0, sp, #224
	bl	DATE_TIME_to_DateTimeStr_32
	mov	r1, #128
	ldr	r2, .L148+4
	mov	r3, r0
	add	r0, sp, #4
	bl	snprintf
	add	r0, sp, #4
	bl	Alert_Message
	ldr	r3, .L148+8
	str	r7, [r3, #8]
.LBB37:
	str	r5, [r3, #0]
	b	.L15
.L14:
.LBE37:
	tst	r3, #8
	beq	.L16
.LBB38:
	mov	r2, #56
	add	r0, sp, #132
	bl	memcpy
	ldr	r3, [sp, #352]
	ldr	r5, .L148+12
	ldr	r7, .L148+16
	add	r3, r3, #56
	str	r3, [sp, #352]
	mov	r1, #400
	ldr	r2, .L148+20
	ldr	r3, .L148+24
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, #400
	ldr	r2, .L148+20
	ldr	r3, .L148+28
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_get_controller_index
	add	ip, sp, #132
	ldmia	ip!, {r0, r1, r2, r3}
	add	r4, r4, #116
	stmia	r4!, {r0, r1, r2, r3}
	ldmia	ip!, {r0, r1, r2, r3}
	stmia	r4!, {r0, r1, r2, r3}
	ldmia	ip!, {r0, r1, r2, r3}
	stmia	r4!, {r0, r1, r2, r3}
	ldr	r2, .L148+32
	mov	r3, #1
	ldmia	ip, {r0, r1}
	str	r3, [r2, #204]
	ldr	r2, .L148+8
	stmia	r4, {r0, r1}
	ldr	r0, .L148+36
	str	r3, [r2, #12]
	bl	Alert_Message
	ldr	r0, [r7, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
.L16:
.LBE38:
	add	r2, sp, #512
	ldrh	r3, [r2, #-166]
	ldrh	r2, [r2, #-164]
	orr	r3, r3, r2, asl #16
	tst	r3, #16384
.LBB39:
	ldrne	r2, .L148+8
	movne	r1, #1
	strne	r1, [r2, #0]
.LBE39:
	tst	r3, #1048576
	beq	.L18
	ldr	r5, .L148+8
	mov	r4, #5056
	ldr	r3, [r5, r4]
	cmp	r3, #0
	beq	.L19
	ldr	r0, .L148+40
	bl	Alert_Message
.L19:
	ldr	r3, .L148+44
	mov	r2, #1
	ldrsh	r1, [r3, #0]
	ldr	r3, .L148+48
	str	r2, [r5, r4]
	cmp	r1, r3
	ldrne	r2, .L148+52
	mov	r3, #2
	strne	r3, [r2, #0]
	bne	.L18
	str	r3, [sp, #188]
	ldr	r3, .L148+56
	add	r0, sp, #188
	str	r3, [sp, #208]
	str	r2, [sp, #212]
	bl	Display_Post_Command
.L18:
	add	r0, sp, #512
	ldrh	r3, [r0, #-164]
	tst	r3, #32
	beq	.L21
	ldr	r5, .L148+8
	ldr	r4, .L148+60
	ldr	r3, [r5, r4]
	cmp	r3, #0
	beq	.L22
	ldr	r0, .L148+64
	bl	Alert_Message
.L22:
	ldr	r3, .L148+44
	mov	r2, #1
	ldrsh	r1, [r3, #0]
	ldr	r3, .L148+48
	str	r2, [r5, r4]
	cmp	r1, r3
	ldrne	r2, .L148+52
	mov	r3, #2
	strne	r3, [r2, #0]
	bne	.L21
	str	r3, [sp, #188]
	ldr	r3, .L148+56
	add	r0, sp, #188
	str	r3, [sp, #208]
	str	r2, [sp, #212]
	bl	Display_Post_Command
.L21:
	add	r1, sp, #512
	ldrh	r3, [r1, #-164]
	tst	r3, #64
	beq	.L24
	ldr	r5, .L148+8
	ldr	r4, .L148+68
	ldr	r3, [r5, r4]
	cmp	r3, #0
	beq	.L25
	ldr	r0, .L148+72
	bl	Alert_Message
.L25:
	mov	r3, #1
	str	r3, [r5, r4]
.L24:
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	beq	.L15
	add	r2, sp, #512
	ldrh	r3, [r2, #-166]
	ands	r3, r3, #128
	streq	r3, [sp, #364]
	beq	.L27
	add	r0, sp, #364
	ldr	r1, [sp, #352]
	mov	r2, #4
	bl	memcpy
	ldr	r3, [sp, #352]
	add	r3, r3, #4
	str	r3, [sp, #352]
.L27:
	ldr	r4, .L148
	ldr	r3, [sp, #364]
	mov	r1, #400
	str	r3, [r4, #96]
	ldr	r3, .L148+12
	ldr	r2, .L148+20
	ldr	r0, [r3, #0]
	ldr	r3, .L148+76
	bl	xQueueTakeMutexRecursive_debug
	add	r0, sp, #512
	ldrh	r3, [r0, #-166]
	mov	r5, r4
	ands	r3, r3, #16
	streq	r3, [r4, #100]
	beq	.L30
	bl	WEATHER_get_rain_switch_in_use
	cmp	r0, #1
	bne	.L29
	bl	FLOWSENSE_get_controller_index
	bl	WEATHER_get_rain_switch_connected_to_this_controller
	cmp	r0, #1
	streq	r0, [r4, #100]
	beq	.L30
.L29:
	mov	r3, #0
	str	r3, [r5, #100]
.L30:
	ldr	r4, .L148+12
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r3, .L148+80
	ldr	r2, .L148+20
	bl	xQueueTakeMutexRecursive_debug
	add	r1, sp, #512
	ldrh	r3, [r1, #-166]
	ldr	r4, .L148
	ands	r3, r3, #32
	beq	.L31
	bl	WEATHER_get_freeze_switch_in_use
	cmp	r0, #1
	bne	.L32
	bl	FLOWSENSE_get_controller_index
	bl	WEATHER_get_freeze_switch_connected_to_this_controller
	cmp	r0, #1
	streq	r0, [r4, #104]
	beq	.L33
.L32:
	mov	r3, #0
.L31:
	str	r3, [r4, #104]
.L33:
	ldr	r4, .L148+12
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	add	r2, sp, #512
	ldrh	r3, [r2, #-166]
	tst	r3, #256
	beq	.L34
	ldr	r2, .L148+20
	ldr	r3, .L148+84
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L148+8
	ldr	r0, [r4, #0]
	ldr	r2, [r3, #16]
	add	r2, r2, #1
	str	r2, [r3, #16]
	bl	xQueueGiveMutexRecursive
.L34:
	add	r0, sp, #512
	ldrh	r3, [r0, #-166]
	tst	r3, #512
	beq	.L35
	ldr	r4, .L148+12
	ldr	r0, .L148+88
	bl	Alert_Message
	ldr	r2, .L148+20
	ldr	r3, .L148+92
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L148+8
	ldr	r0, [r4, #0]
	ldr	r2, [r3, #32]
	add	r2, r2, #1
	str	r2, [r3, #32]
	bl	xQueueGiveMutexRecursive
.L35:
	add	r1, sp, #512
	ldrh	r3, [r1, #-166]
	tst	r3, #1
	beq	.L36
.LBB40:
	ldr	r3, [sp, #352]
	ldrb	r5, [r3], #1	@ zero_extendqisi2
	cmp	r5, #12
	str	r3, [sp, #352]
	bls	.L37
	ldr	r0, .L148+96
	bl	Alert_Message
	b	.L15
.L37:
	ldr	r3, .L148+100
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L148+20
	mov	r3, #135
	bl	xQueueTakeMutexRecursive_debug
	mov	r4, #0
	b	.L39
.L44:
	ldr	r1, [sp, #352]
	mov	r2, #24
	add	r0, sp, #256
	bl	memcpy
	ldr	r3, [sp, #352]
	add	r3, r3, #24
	str	r3, [sp, #352]
	mov	r3, #0
	str	r3, [sp, #376]
	bl	FLOWSENSE_get_controller_index
	add	r3, sp, #372
	ldr	r1, [sp, #256]
	add	r2, sp, #376
	bl	POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters
	ldr	r3, [sp, #260]
	cmp	r0, #0
	beq	.L40
	ldr	ip, [sp, #376]
	cmp	ip, #0
	bne	.L41
.L40:
	cmp	r3, #0
	beq	.L42
	ldr	r0, .L148+20
	bl	RemovePathFromFileName
	mov	r2, #159
	mov	r1, r0
	ldr	r0, .L148+104
	bl	Alert_Message_va
	ldr	r3, [sp, #268]
	add	r1, sp, #256
	str	r3, [sp, #0]
	ldr	r0, .L148+108
	ldmia	r1, {r1, r2, r3}
	bl	Alert_Message_va
	b	.L42
.L41:
	cmp	r3, #0
	beq	.L43
	ldr	r2, [r0, #12]
	add	r3, r2, r3
	str	r3, [r0, #12]
	ldr	r2, [r0, #16]
	ldr	r3, [sp, #264]
	add	r3, r2, r3
	str	r3, [r0, #16]
.L43:
	ldrb	lr, [r0, #0]	@ zero_extendqisi2
	ldr	r1, [sp, #268]
	ldr	r2, [sp, #272]
	ldr	r3, [sp, #276]
	orr	lr, lr, #8
	str	r1, [r0, #20]
	str	r2, [r0, #24]
	str	r3, [r0, #28]
	strb	lr, [r0, #0]
	ldr	r0, [ip, #12]
	cmp	r0, #12
	bne	.L42
	mov	r0, #2000
	str	r0, [sp, #304]
	str	r3, [sp, #320]
	ldr	r0, [sp, #256]
	ldr	r3, .L148+112
	str	r2, [sp, #316]
	mov	r2, #0
	str	r0, [sp, #308]
	str	r1, [sp, #312]
	ldr	r0, [r3, #0]
	add	r1, sp, #304
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L42
	ldr	r0, .L148+20
	bl	RemovePathFromFileName
	mov	r2, #226
	mov	r1, r0
	ldr	r0, .L148+116
	bl	Alert_Message_va
.L42:
	add	r4, r4, #1
.L39:
	cmp	r4, r5
	bcc	.L44
	ldr	r3, .L148+100
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L36:
.LBE40:
	add	r2, sp, #512
	ldrh	r3, [r2, #-166]
	tst	r3, #2048
	beq	.L45
.LBB41:
	ldr	r1, [sp, #352]
	mov	r2, #4
	add	r0, sp, #372
	bl	memcpy
	ldr	r1, [sp, #352]
	mov	r2, #21
	add	r1, r1, #4
	add	r0, sp, #280
	str	r1, [sp, #352]
	bl	memcpy
	ldr	r3, [sp, #352]
	ldr	r4, .L148+120
	add	r3, r3, #21
	mov	r1, #400
	ldr	r2, .L148+20
	str	r3, [sp, #352]
	ldr	r0, [r4, #0]
	ldr	r3, .L148+124
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_get_controller_index
	ldr	r1, [sp, #372]
	bl	nm_MOISTURE_SENSOR_get_pointer_to_file_moisture_sensor_with_this_box_index_and_decoder_serial_number
	subs	r1, r0, #0
	beq	.L46
	add	r0, sp, #280
	bl	nm_MOISTURE_SENSOR_set_new_reading_slave_side_variables
	b	.L47
.L46:
	ldr	r0, .L148+128
	bl	Alert_Message
.L47:
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	b	.L45
.L143:
.LBE41:
.LBB42:
	ldr	r3, [sp, #352]
.LBB43:
.LBB44:
	ldr	r7, .L148+132
.LBE44:
.LBE43:
	ldrb	r5, [r3], #1	@ zero_extendqisi2
	mov	r8, r7
	str	r3, [sp, #352]
	mov	r3, #1
	b	.L48
.L63:
	ldr	r1, [sp, #352]
	mov	r2, r1
	ldrb	r3, [r2], #1	@ zero_extendqisi2
	str	r2, [sp, #352]
	cmp	r3, #0
	ldrb	r4, [r1, #1]	@ zero_extendqisi2
	add	r1, r1, #2
	str	r1, [sp, #352]
	moveq	r3, #1
	beq	.L49
	b	.L141
.L53:
.LBB46:
	ldr	r1, [sp, #352]
	add	r0, sp, #368
	ldrb	sl, [r1], #1	@ zero_extendqisi2
	mov	r2, #4
	str	r1, [sp, #352]
	bl	memcpy
	ldr	r3, [sp, #352]
	cmp	sl, #48
	add	r3, r3, #4
	str	r3, [sp, #352]
	bhi	.L124
.LBB45:
	bl	FLOWSENSE_get_controller_index
	mov	r1, sl
	add	r2, sp, #380
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	cmp	r0, #1
	bne	.L124
	mov	r1, #400
	ldr	r2, .L148+20
	ldr	r3, .L148+136
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	add	r3, sp, #512
	ldrh	r2, [r3, #-144]
	ldr	r1, [sp, #380]
	ldr	r3, .L148+140
	add	r3, r3, r1, asl #7
	ldrh	r1, [r3, #142]
	add	r3, r3, #140
	cmp	r1, r2
	strneh	r2, [r3, #2]	@ movhi
	ldrneb	r2, [r3, #1]	@ zero_extendqisi2
	orrne	r2, r2, #128
	strneb	r2, [r3, #1]
	ldr	r0, [r8, #0]
	bl	xQueueGiveMutexRecursive
	mov	r3, #1
	b	.L51
.L124:
	mov	r3, #0
.L51:
.LBE45:
	sub	r4, r4, #1
	and	r4, r4, #255
.L49:
.LBE46:
	cmp	r4, #0
	moveq	r2, #0
	andne	r2, r3, #1
	cmp	r2, #0
	bne	.L53
	b	.L54
.L141:
	cmp	r3, #1
	bne	.L55
	b	.L132
.L57:
.LBB47:
	ldr	r1, [sp, #352]
	add	r0, sp, #380
	add	r1, r1, #1
	mov	r2, #4
	str	r1, [sp, #352]
	bl	memcpy
	ldr	r3, [sp, #352]
	sub	r4, r4, #1
	add	r3, r3, #4
	and	r4, r4, #255
	str	r3, [sp, #352]
.L132:
.LBE47:
	cmp	r4, #0
	bne	.L57
	b	.L135
.L55:
	cmp	r3, #2
	bne	.L58
	cmp	r4, #1
	bne	.L127
.LBB48:
	mov	r2, #4
	add	r0, sp, #380
	bl	memcpy
	ldr	r3, [sp, #352]
	ldr	r4, .L148+100
	add	r3, r3, #4
	mov	r1, #400
	ldr	r2, .L148+20
	str	r3, [sp, #352]
	ldr	r0, [r4, #0]
	ldr	r3, .L148+144
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_get_controller_index
	mov	r1, #0
	add	r2, sp, #376
	add	r3, sp, #372
	bl	POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters
	cmp	r0, #0
	beq	.L59
	ldr	r3, [sp, #380]
	ldr	r2, [r0, #64]
	cmp	r3, r2
	strne	r3, [r0, #64]
	bne	.L139
	b	.L62
.L59:
	ldr	r0, .L148+20
	bl	RemovePathFromFileName
	ldr	r2, .L148+148
	mov	r1, r0
	ldr	r0, .L148+152
	b	.L138
.L58:
.LBE48:
	cmp	r3, #3
	bne	.L127
	cmp	r4, #1
	bne	.L127
.LBB49:
	mov	r2, #4
	add	r0, sp, #380
	bl	memcpy
	ldr	r3, [sp, #352]
	ldr	r4, .L148+100
	add	r3, r3, #4
	mov	r1, #400
	ldr	r2, .L148+20
	str	r3, [sp, #352]
	ldr	r0, [r4, #0]
	ldr	r3, .L148+156
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_get_controller_index
	mov	r1, #0
	add	r2, sp, #376
	add	r3, sp, #372
	bl	POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters
	cmp	r0, #0
	beq	.L61
	ldr	r3, [sp, #380]
	ldr	r2, [r0, #68]
	cmp	r3, r2
	beq	.L62
	str	r3, [r0, #68]
.L139:
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	orr	r3, r3, #4
	strb	r3, [r0, #0]
	b	.L62
.L61:
	ldr	r0, .L148+20
	bl	RemovePathFromFileName
	ldr	r2, .L148+160
	mov	r1, r0
	ldr	r0, .L148+152
.L138:
	bl	Alert_Message_va
.L62:
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
.L135:
	mov	r3, #1
	b	.L54
.L127:
.LBE49:
	mov	r3, #0
.L54:
	sub	r5, r5, #1
	and	r5, r5, #255
.L48:
	cmp	r5, #0
	moveq	r2, #0
	andne	r2, r3, #1
	cmp	r2, #0
	bne	.L63
	cmp	r3, #0
	ldreq	r0, .L148+164
	beq	.L136
.L64:
.LBE42:
	add	r0, sp, #512
	ldrh	r3, [r0, #-164]
	tst	r3, #8
	beq	.L65
.LBB50:
	mov	r2, #12
	add	r0, sp, #324
	ldr	r1, [sp, #352]
	bl	memcpy
	ldr	r3, [sp, #352]
	add	r3, r3, #12
	str	r3, [sp, #352]
	ldr	r3, [sp, #324]
	sub	r2, r3, #1
	cmp	r3, #5
	cmpne	r2, #1
	movls	r2, #1
	bls	.L66
	ldr	r0, .L148+168
	bl	Alert_message_on_tpmicro_pile_M
	mov	r2, #0
.L66:
	ldr	r3, [sp, #328]
	sub	r1, r3, #2
	cmp	r3, #0
	cmpne	r1, #1
	bls	.L67
	cmp	r3, #1
	ldrne	r0, .L148+172
	bne	.L136
.L67:
	cmp	r2, #0
	beq	.L15
	ldr	r4, .L148+8
	ldr	r3, [r4, #204]
	cmp	r3, #0
	beq	.L69
	ldr	r0, .L148+20
	bl	RemovePathFromFileName
	ldr	r2, .L148+176
	mov	r1, r0
	ldr	r0, .L148+180
	bl	Alert_Message_va
.L69:
	add	r2, sp, #324
	ldmia	r2, {r0, r1, r2}
	ldr	r3, .L148+184
	stmia	r3, {r0, r1, r2}
	mov	r3, #1
	str	r3, [r4, #204]
.L65:
.LBE50:
	add	r1, sp, #512
	ldrh	r5, [r1, #-166]
	ldr	r4, .L148
	ands	r5, r5, #1024
	ldr	r3, [r4, #112]
	beq	.L71
	cmp	r3, #0
	bne	.L72
	bl	FLOWSENSE_get_controller_index
	bl	Alert_fuse_blown_idx
	mov	r3, #1
	str	r3, [r4, #112]
	b	.L72
.L71:
	cmp	r3, #0
	beq	.L72
	bl	FLOWSENSE_get_controller_index
	bl	Alert_fuse_replaced_idx
	str	r5, [r4, #112]
	b	.L72
.L112:
.LBB51:
	mov	r2, #4
	add	r0, sp, #372
	ldr	r1, [sp, #352]
	bl	memcpy
	ldr	r3, [sp, #352]
	ldr	r2, [sp, #372]
	add	r3, r3, #4
	str	r3, [sp, #352]
	ldr	r3, .L148+188
	cmp	r2, r3
	bls	.L74
	ldr	r0, .L148+20
	bl	RemovePathFromFileName
	ldr	r2, .L148+192
	mov	r1, r0
	ldr	r0, .L148+196
	bl	Alert_Message_va
	b	.L15
.L74:
	ldr	r3, .L148+8
	ldr	r1, [r3, #52]
	cmp	r2, r1
	movne	r1, #1
	strne	r1, [r3, #56]
	strne	r2, [r3, #52]
	b	.L75
.L113:
.LBE51:
	ldr	r3, [r4, #108]
	cmp	r3, #0
	bne	.L77
	ldr	r0, .L148+200
	ldr	r1, [r4, #96]
	bl	Alert_Message_va
	ldr	r0, [r4, #96]
	bl	Alert_wind_paused
.L77:
	mov	r3, #1
	b	.L137
.L144:
	ldr	r3, [r4, #108]
	cmp	r3, #0
	beq	.L79
	ldr	r0, .L148+200
	ldr	r1, [r4, #96]
	bl	Alert_Message_va
	ldr	r0, [r4, #96]
	bl	Alert_wind_resumed
.L79:
	mov	r3, #0
.L137:
	add	r0, sp, #512
	str	r3, [r4, #108]
	ldrh	r3, [r0, #-164]
	tst	r3, #1
	beq	.L80
.L115:
	ldr	r1, [sp, #352]
	mov	r2, #4
	add	r0, sp, #360
	bl	memcpy
	ldr	r3, [sp, #352]
	ldr	r2, .L148+204
	add	r3, r3, #4
	str	r3, [sp, #352]
	ldr	r1, .L148+8
	ldr	r3, [sp, #360]
	str	r3, [r1, r2]
	ldr	r2, .L148+208
	str	r3, [r2, #0]
	b	.L80
.L116:
.LBB52:
	ldr	r1, [sp, #352]
	mov	r2, #4
	add	r0, sp, #368
	bl	memcpy
	ldr	r1, [sp, #352]
	add	r0, sp, #356
	add	r1, r1, #4
	mov	r2, #4
	str	r1, [sp, #352]
	bl	memcpy
	ldr	r3, [sp, #352]
	add	r3, r3, #4
	str	r3, [sp, #352]
	ldr	r3, [sp, #356]
	cmp	r3, #80
	bhi	.L81
	ldr	r3, .L148+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L148+20
	ldr	r3, .L148+212
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [sp, #368]
	cmp	r3, #0
	beq	.L82
	ldr	r0, .L148+216
	mov	r1, #0
	mov	r2, #4800
	bl	memset
.L82:
	mov	r3, #0
	str	r3, [sp, #360]
	mov	r5, #60
	ldr	r4, .L148+8
	b	.L83
.L84:
	mla	r0, r5, r0, r4
	ldr	r1, [sp, #352]
	mov	r2, #4
	add	r0, r0, #220
	bl	memcpy
	ldr	r0, [sp, #360]
	ldr	r1, [sp, #352]
	mla	r0, r5, r0, r4
	add	r1, r1, #4
	add	r0, r0, #224
	mov	r2, #4
	str	r1, [sp, #352]
	bl	memcpy
	ldr	r3, [sp, #352]
	add	r3, r3, #4
	str	r3, [sp, #352]
	ldr	r3, [sp, #360]
	add	r3, r3, #1
	str	r3, [sp, #360]
.L83:
	ldr	r0, [sp, #360]
	ldr	r3, [sp, #356]
	cmp	r0, r3
	bcc	.L84
	bl	STATION_for_stations_on_this_box_set_physically_available_based_upon_discovery_results
	bl	MOISTURE_SENSOR_set_physically_available_or_create_new_moisture_sensors_as_needed_based_upon_the_discovery_results
	ldr	r3, .L148+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #8
	mov	r1, #1
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	ldr	r3, .L148+44
	ldrsh	r2, [r3, #0]
	ldr	r3, .L148+48
	cmp	r2, r3
	ldrne	r2, .L148+52
	mov	r3, #2
	strne	r3, [r2, #0]
	bne	.L86
	str	r3, [sp, #188]
	ldr	r3, .L148+56
	add	r0, sp, #188
	str	r3, [sp, #208]
	mov	r3, #1
	str	r3, [sp, #212]
	bl	Display_Post_Command
	b	.L86
.L81:
	add	r0, sp, #4
	mov	r1, #128
	ldr	r2, .L148+220
	b	.L140
.L145:
.LBE52:
.LBB53:
	ldr	r1, [sp, #352]
	mov	r2, #4
	add	r0, sp, #368
	bl	memcpy
	ldr	r3, [sp, #352]
	mov	r1, #128
	add	r3, r3, #4
	str	r3, [sp, #352]
	ldr	r2, .L148+224
	ldr	r3, [sp, #368]
	add	r0, sp, #4
	bl	snprintf
	add	r0, sp, #4
	bl	Alert_message_on_tpmicro_pile_M
	ldr	r3, [sp, #368]
	cmp	r3, #80
	movls	r5, #0
	bls	.L88
	b	.L142
.L92:
.LBB54:
	ldr	r1, [sp, #352]
	mov	r2, #4
	add	r0, sp, #372
	bl	memcpy
	ldr	r3, [sp, #352]
	ldr	r0, [sp, #372]
	add	r3, r3, #4
	str	r3, [sp, #352]
	bl	find_two_wire_decoder
	subs	r4, r0, #0
	beq	.L90
	add	r0, r4, #8
	ldr	r1, [sp, #352]
	mov	r2, #29
	bl	memcpy
	ldr	r3, [sp, #352]
.LBE54:
	add	r5, r5, #1
.LBB55:
	add	r3, r3, #29
	str	r3, [sp, #352]
	b	.L88
.L90:
	add	r0, sp, #4
	mov	r1, #128
	ldr	r2, .L148+228
	ldr	r3, [sp, #372]
	bl	snprintf
	add	r0, sp, #4
	bl	Alert_message_on_tpmicro_pile_M
	b	.L91
.L88:
.LBE55:
	ldr	r3, [sp, #368]
	cmp	r5, r3
	bcc	.L92
	mov	r4, #1
.L91:
	mov	r0, #8
	mov	r1, #1
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.LBE53:
	cmp	r4, #0
	bne	.L93
	b	.L15
.L142:
.LBB56:
	add	r0, sp, #4
	mov	r1, #128
	ldr	r2, .L148+232
	b	.L140
.L93:
.LBE56:
	add	r2, sp, #512
	ldrh	r3, [r2, #-166]
	ldrh	r2, [r2, #-164]
	orr	r2, r3, r2, asl #16
	ands	r1, r2, #4
	ldr	r3, .L148+8
	beq	.L95
	ldr	r1, [r3, #28]
	cmp	r1, #0
	moveq	r1, #1
	streq	r1, [r3, #20]
	b	.L96
.L95:
	ldr	r0, [r3, #28]
	cmp	r0, #0
	ldrne	r0, .L148+236
	strne	r1, [r3, #28]
	strne	r1, [r0, #84]
	strneh	r1, [r0, #36]	@ movhi
	b	.L96
.L118:
	ldr	r0, .L148+240
	ldr	r1, [sp, #352]
	mov	r2, #4
	bl	memcpy
	ldr	r3, [sp, #352]
	add	r3, r3, #4
	str	r3, [sp, #352]
	b	.L98
.L146:
	add	r0, sp, #352
	bl	extract_decoder_fault_content_out_of_msg_from_tpmicro
	cmp	r0, #0
	beq	.L15
.L120:
	add	r0, sp, #512
	ldrh	r3, [r0, #-164]
	tst	r3, #256
	beq	.L99
	add	r0, sp, #352
	bl	extract_decoder_fault_content_out_of_msg_from_tpmicro
	cmp	r0, #0
	beq	.L15
.L99:
	add	r1, sp, #512
	ldrh	r3, [r1, #-164]
	tst	r3, #1024
	beq	.L101
	add	r0, sp, #352
	bl	extract_decoder_fault_content_out_of_msg_from_tpmicro
	cmp	r0, #0
	beq	.L15
.L101:
	add	r2, sp, #512
	ldrh	r3, [r2, #-164]
	tst	r3, #2048
	beq	.L103
	add	r0, sp, #384
	ldr	r1, [sp, #352]
	mov	r2, #1
	add	r0, r0, #3
	bl	memcpy
	ldr	r3, [sp, #352]
	add	r3, r3, #1
	str	r3, [sp, #352]
	bl	FLOWSENSE_get_controller_index
	bl	WEATHER_sw1_is_available_and_enabled_as_a_rain_switch_at_this_controller
	ldr	r3, .L148
	cmp	r0, #0
	streq	r0, [r3, #100]
	beq	.L103
	ldrb	r2, [sp, #387]	@ zero_extendqisi2
	cmp	r2, #0
	movne	r2, #1
	str	r2, [r3, #100]
	b	.L103
.L147:
	add	r0, sp, #388
	ldr	r4, [r0, #-36]!
	bl	TPMICRO_DATA_extract_reported_errors_out_of_msg_from_tpmicro
	add	r0, r4, r0
	str	r0, [sp, #352]
	b	.L15
.L13:
	cmp	r3, #93
	bne	.L106
	add	r0, sp, #512
	ldrh	r3, [r0, #-166]
	tst	r3, #1
	beq	.L15
	add	r0, sp, #384
	mov	r2, #1
	add	r0, r0, #2
	bl	memcpy
	ldr	r3, [sp, #352]
	add	r0, sp, #4
	add	r3, r3, #1
	str	r3, [sp, #352]
	ldrb	r3, [sp, #386]	@ zero_extendqisi2
	cmp	r3, #128
	movhi	r1, #128
	ldrhi	r2, .L148+244
	bhi	.L140
	mov	r1, #0
	mov	r2, #128
	bl	memset
	ldrb	r2, [sp, #386]	@ zero_extendqisi2
	add	r0, sp, #4
	ldr	r1, [sp, #352]
	bl	memcpy
	ldr	r2, [sp, #352]
	ldrb	r3, [sp, #386]	@ zero_extendqisi2
	add	r0, sp, #4
	add	r3, r2, r3
	str	r3, [sp, #352]
	bl	Alert_message_on_tpmicro_pile_T
	b	.L15
.L140:
	bl	snprintf
	add	r0, sp, #4
	b	.L136
.L106:
	cmp	r3, #92
	bne	.L108
	add	r1, sp, #512
	ldrh	r3, [r1, #-166]
	ldrh	r0, [r1, #-164]
	sub	r4, r4, #18
	orr	r0, r3, r0, asl #16
	cmp	r4, #64
	cmpls	r0, #13
	bhi	.L109
	ldr	r5, .L148+248
	mov	r1, #0
	add	r0, r5, r0, asl #6
	mov	r2, #64
	bl	memset
	add	r2, sp, #512
	ldrh	r3, [r2, #-166]
	ldrh	r0, [r2, #-164]
	ldr	r1, [sp, #352]
	orr	r0, r3, r0, asl #16
	add	r0, r5, r0, asl #6
	mov	r2, r4
	bl	memcpy
	b	.L15
.L109:
	mov	r1, #64
	ldr	r2, .L148+252
	ldr	r0, .L148+248
	bl	snprintf
	b	.L15
.L108:
	cmp	r3, #95
	ldrne	r0, .L148+256
	bne	.L136
	mov	r0, #0
	mov	r1, #8
	bl	postSerportDrvrEvent
	b	.L15
.L136:
	bl	Alert_message_on_tpmicro_pile_M
.L15:
	mov	r0, r6
	ldr	r1, .L148+20
	ldr	r2, .L148+260
	bl	mem_free_debug
	add	sp, sp, #388
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L45:
	add	r0, sp, #512
	ldrh	r3, [r0, #-166]
	tst	r3, #2
	beq	.L64
	b	.L143
.L72:
	add	r1, sp, #512
	ldrh	r3, [r1, #-166]
	tst	r3, #4096
	bne	.L112
.L75:
	add	r2, sp, #512
	ldrh	r3, [r2, #-166]
	ldr	r4, .L148
	tst	r3, #32768
	bne	.L113
	b	.L144
.L149:
	.align	2
.L148:
	.word	.LANCHOR0
	.word	.LC1
	.word	tpmicro_data
	.word	tpmicro_data_recursive_MUTEX
	.word	irri_comm_recursive_MUTEX
	.word	.LC0
	.word	609
	.word	614
	.word	irri_comm
	.word	.LC2
	.word	.LC3
	.word	GuiLib_CurStructureNdx
	.word	599
	.word	GuiVar_TwoWireDiscoveryState
	.word	FDTO_TWO_WIRE_update_discovery_dialog
	.word	5060
	.word	.LC4
	.word	5064
	.word	.LC5
	.word	1172
	.word	1198
	.word	1235
	.word	.LC6
	.word	1255
	.word	.LC7
	.word	poc_preserves_recursive_MUTEX
	.word	.LC8
	.word	.LC9
	.word	BYPASS_event_queue
	.word	.LC10
	.word	moisture_sensor_items_recursive_MUTEX
	.word	278
	.word	.LC11
	.word	station_preserves_recursive_MUTEX
	.word	406
	.word	station_preserves
	.word	481
	.word	497
	.word	.LC12
	.word	526
	.word	542
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	789
	.word	.LC16
	.word	tpmicro_data+208
	.word	10000
	.word	701
	.word	.LC17
	.word	.LC18
	.word	5048
	.word	GuiVar_TwoWireNumDiscoveredStaDecoders
	.word	1409
	.word	tpmicro_data+220
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	weather_preserves
	.word	tpmicro_data+5052
	.word	.LC23
	.word	.LANCHOR1
	.word	.LC24
	.word	.LC25
	.word	1751
.L80:
	add	r1, sp, #512
	ldrh	r3, [r1, #-164]
	tst	r3, #2
	bne	.L116
.L86:
	add	r2, sp, #512
	ldrh	r3, [r2, #-164]
	tst	r3, #4
	beq	.L93
	b	.L145
.L96:
	tst	r2, #64
	bne	.L118
.L98:
	add	r0, sp, #512
	ldrh	r3, [r0, #-164]
	tst	r3, #128
	beq	.L120
	b	.L146
.L103:
	add	r1, sp, #512
	ldrh	r3, [r1, #-164]
	tst	r3, #32768
	beq	.L15
	b	.L147
.LFE9:
	.size	TPMICRO_COMM_parse_incoming_message_from_tp_micro, .-TPMICRO_COMM_parse_incoming_message_from_tp_micro
	.global	__udivsi3
	.section	.text.TPMICRO_COMM_kick_out_the_next_tpmicro_msg,"ax",%progbits
	.align	2
	.global	TPMICRO_COMM_kick_out_the_next_tpmicro_msg
	.type	TPMICRO_COMM_kick_out_the_next_tpmicro_msg, %function
TPMICRO_COMM_kick_out_the_next_tpmicro_msg:
.LFB15:
	@ args = 0, pretend = 0, frame = 252
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI3:
	ldr	r4, .L268
	sub	sp, sp, #256
.LCFI4:
	ldr	r5, [r4, #4]
	cmp	r5, #0
	beq	.L151
	bl	TPMICRO_COMM_kick_out_the_next_isp_tpmicro_msg
	b	.L150
.L151:
.LBB68:
.LBB69:
	ldr	r3, [r0, #0]
	cmp	r3, #1792
	beq	.L153
	ldr	r0, .L268+4
	bl	RemovePathFromFileName
	mov	r2, #2608
	mov	r1, r0
	ldr	r0, .L268+8
	bl	Alert_Message_va
	b	.L150
.L153:
.LBB70:
	ldr	r3, [r4, #12]
	cmp	r3, #9
	bhi	.L154
	add	r3, r3, #1
	cmp	r3, #5
	str	r3, [r4, #12]
	bls	.L154
	cmp	r3, #10
	bne	.L155
	ldr	r0, .L268+12
	bl	Alert_message_on_tpmicro_pile_M
	str	r5, [r4, #0]
	b	.L154
.L155:
	add	r0, sp, #52
	mov	r1, #64
	ldr	r2, .L268+16
	bl	snprintf
	add	r0, sp, #52
	bl	Alert_message_on_tpmicro_pile_M
.L154:
	ldr	r3, .L268
	ldr	r4, [r3, #92]
	cmp	r4, #0
	beq	.L156
	ldr	r4, [r3, #88]
	cmp	r4, #0
	beq	.L156
	ldr	r4, [r3, #72]
	cmp	r4, #0
	beq	.L156
	ldr	r2, [r3, #80]
	ldr	r1, [r3, #64]
	cmp	r2, r1
	bne	.L157
	ldr	r0, [r3, #84]
	ldr	r3, [r3, #68]
	cmp	r0, r3
	bne	.L157
	ldr	r3, .L268+20
	mov	r2, #1
	mov	r4, #0
	str	r2, [r3, #32]
	mov	r2, #2
	str	r4, [r3, #36]
	str	r2, [r3, #0]
.L243:
	ldr	r3, .L268
	mov	r2, #0
	str	r2, [r3, #92]
.L156:
	ldr	r1, .L268+4
	ldr	r2, .L268+24
	ldr	r0, .L268+28
	bl	mem_malloc_debug
	mov	r2, #12
	mov	r1, #0
	str	r0, [sp, #12]
	add	r0, sp, #196
	bl	memset
	add	r0, sp, #196
	mov	r1, #7
	bl	set_this_packets_CS3000_message_class
	ldr	r1, .L268+32
	mov	r2, #3
	add	r0, sp, #198
	bl	memcpy
	add	r1, sp, #196
	mov	r2, #12
	ldr	r0, [sp, #12]
	bl	memcpy
	mov	r3, #90
	strh	r3, [sp, #244]	@ movhi
	ldr	r3, [sp, #12]
	ldr	r2, [sp, #12]
	add	r5, r3, #18
	ldr	r3, .L268
	add	r2, r2, #12
	str	r2, [sp, #32]
	mov	r2, #0
	strh	r2, [sp, #246]	@ movhi
	strh	r2, [sp, #248]	@ movhi
	ldr	r0, [r3, #76]
	mov	r1, r2	@ movhi
	cmp	r0, r2
	movne	r0, #64
	strneh	r0, [sp, #246]	@ movhi
	movne	sl, #18
	strne	r2, [r3, #76]
	bne	.L254
.L158:
	cmp	r4, #0
	beq	.L160
	mov	r2, #16
	strh	r2, [sp, #246]	@ movhi
	mov	r2, #1
	strh	r1, [sp, #248]	@ movhi
	str	r2, [r3, #4]
	mov	r2, #10
	str	r2, [r3, #16]
	mov	sl, #18
	mov	r4, #40
	b	.L159
.L160:
.LBB71:
	ldr	r3, .L268+36
	ldr	r2, [r3, #4]
	cmp	r2, #0
	ldrne	r2, .L268+40
	strneh	r1, [sp, #248]	@ movhi
	strneh	r2, [sp, #246]	@ movhi
	strne	r4, [r3, #4]
	ldr	r3, [r3, #204]
	cmp	r3, #3
	bne	.L162
	ldrh	r2, [sp, #248]
	ldrh	r3, [sp, #246]
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #8
	strh	r3, [sp, #246]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #248]	@ movhi
	ldr	r3, .L268+36
	mov	r2, #0
	str	r2, [r3, #204]
.L162:
	ldr	r2, .L268+36
	mov	r3, #5056
	ldr	r1, [r2, r3]
	cmp	r1, #3
	bne	.L163
	ldrh	r1, [sp, #246]
	ldrh	r0, [sp, #248]
	orr	r1, r1, r0, asl #16
	orr	r1, r1, #262144
	strh	r1, [sp, #246]	@ movhi
	mov	r1, r1, lsr #16
	strh	r1, [sp, #248]	@ movhi
	mov	r1, #0
	str	r1, [r2, r3]
.L163:
	ldr	r2, .L268+36
	ldr	r3, .L268+44
	ldr	r1, [r2, r3]
	cmp	r1, #3
	bne	.L164
	ldrh	r1, [sp, #246]
	ldrh	r0, [sp, #248]
	orr	r1, r1, r0, asl #16
	orr	r1, r1, #524288
	strh	r1, [sp, #246]	@ movhi
	mov	r1, r1, lsr #16
	strh	r1, [sp, #248]	@ movhi
	mov	r1, #0
	str	r1, [r2, r3]
.L164:
.LBB72:
	ldr	r3, .L268+48
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L268+4
	ldr	r3, .L268+52
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L268+4
	ldr	r2, .L268+56
	mov	r0, #109
	bl	mem_malloc_debug
	mov	r7, #0
	mov	r4, #1
	ldr	fp, .L268+60
	mov	r9, r0
	add	r8, r0, #1
	bl	FLOWSENSE_get_controller_index
	mov	sl, r0
	ldr	r0, .L268+64
	bl	nm_ListGetFirst
	mov	r6, r0
	b	.L165
.L170:
	ldr	r3, [r6, #36]
	cmp	r3, sl
	bne	.L166
	ldrb	r3, [r6, #53]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L166
	mov	r0, sl
	bl	NETWORK_CONFIG_get_electrical_limit
	cmp	r7, r0
	ldrcs	r0, .L268+68
	bcs	.L258
.L167:
	mov	r1, #400
	ldr	r2, .L268+4
	ldr	r3, .L268+72
	ldr	r0, [fp, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldrb	r1, [r6, #40]	@ zero_extendqisi2
	mov	r0, sl
	bl	nm_STATION_get_pointer_to_station
	subs	r1, r0, #0
	str	r1, [sp, #8]
	beq	.L169
	bl	nm_STATION_get_station_number_0
	add	r7, r7, #1
	add	r4, r4, #12
	and	r7, r7, #255
	str	r0, [sp, #208]
	ldr	r0, [sp, #8]
	bl	nm_STATION_get_decoder_serial_number
	str	r0, [sp, #212]
	ldr	r0, [sp, #8]
	bl	nm_STATION_get_decoder_output
	add	r1, sp, #208
	mov	r2, #12
	str	r0, [sp, #216]
	mov	r0, r8
	bl	memcpy
	ldr	r0, [fp, #0]
	add	r8, r8, #12
	bl	xQueueGiveMutexRecursive
	b	.L166
.L169:
	ldr	r0, .L268+76
.L258:
	bl	Alert_message_on_tpmicro_pile_M
	mov	r6, #1
	b	.L168
.L166:
	mov	r1, r6
	ldr	r0, .L268+64
	bl	nm_ListGetNext
	mov	r6, r0
.L165:
	cmp	r6, #0
	bne	.L170
.L168:
	ldr	r3, .L268+48
	cmp	r7, #0
	orreq	r6, r6, #1
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	tst	r6, #255
	strb	r7, [r9, #0]
	ldr	r8, .L268+80
	beq	.L171
	mov	r0, r9
	ldr	r1, .L268+4
	ldr	r2, .L268+84
	bl	mem_free_debug
	mov	sl, #18
	b	.L172
.L171:
.LBE72:
	cmp	r4, r8
	movhi	sl, #18
	bhi	.L173
	ldrh	r2, [sp, #248]
	ldrh	r3, [sp, #246]
	mov	r0, r5
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #1
	strh	r3, [sp, #246]	@ movhi
	mov	r1, r9
	mov	r3, r3, lsr #16
	mov	r2, r4
	strh	r3, [sp, #248]	@ movhi
	bl	memcpy
	rsb	r8, r4, #2032
	add	r5, r5, r4
	add	sl, r4, #18
	add	r8, r8, #14
.L173:
	mov	r0, r9
	ldr	r1, .L268+4
	ldr	r2, .L268+88
	bl	mem_free_debug
.L172:
.LBB73:
	ldr	r1, .L268+4
	ldr	r2, .L268+92
	mov	r0, #49
	bl	mem_malloc_debug
	ldr	r3, .L268+96
	mov	r1, #400
	ldr	r4, .L268+100
	mov	fp, #1
.LBB74:
	mov	r6, r4
.LBE74:
	add	r2, r0, #1
	str	r0, [sp, #8]
	str	r2, [sp, #24]
	ldr	r0, [r3, #0]
	ldr	r2, .L268+4
	ldr	r3, .L268+104
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L268+108
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L268+4
	ldr	r3, .L268+112
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #0
	mov	r7, r3
	str	r3, [sp, #20]
	str	r3, [sp, #16]
.LBB75:
	str	r8, [sp, #40]
	str	r5, [sp, #44]
	str	sl, [sp, #48]
.L220:
.LBE75:
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	.L174
	ldr	r1, [r4, #28]
	bl	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	subs	sl, r0, #0
	bne	.L175
	ldr	r0, .L268+4
	bl	RemovePathFromFileName
	mov	r2, #2032
	mov	r1, r0
	ldr	r0, .L268+116
	bl	Alert_Message_va
	mov	r1, #1
	str	r1, [sp, #20]
	b	.L174
.L175:
.LBB76:
	mov	r3, #472
	mla	r3, r7, r3, r6
	mov	r1, #400
	ldrb	r3, [r3, #108]	@ zero_extendqisi2
	and	r2, r3, #1
	mov	r3, r3, lsr #1
	and	r3, r3, #1
	str	r3, [sp, #36]
	ldr	r3, .L268+48
	str	r2, [sp, #28]
	ldr	r0, [r3, #0]
	ldr	r2, .L268+4
	ldr	r3, .L268+120
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L268+64
	bl	nm_ListGetFirst
	b	.L259
.L179:
	ldrb	r3, [r1, #53]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L177
	ldr	r3, [r4, #44]
	ldr	r2, [r1, #28]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L250
.L177:
	ldr	r0, .L268+64
	bl	nm_ListGetNext
.L259:
	cmp	r0, #0
	mov	r1, r0
	bne	.L179
	mov	r5, r0
	b	.L178
.L250:
	mov	r5, #1
.L178:
	ldr	r1, .L268+48
	ldr	r0, [r1, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r5, #0
	ldreq	r3, [r4, #376]
	ldr	r0, [r4, #44]
	movne	r3, #0
	add	r0, r0, #516
	addeq	r3, r3, #1
	str	r3, [r4, #376]
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	mov	r5, r0
	ldr	r0, [r4, #24]
	bl	POC_get_type_of_poc
	cmp	r0, #12
	movne	r9, #0
	movne	r8, #1
	bne	.L182
	mov	r0, sl
	bl	POC_get_bypass_number_of_levels
	mov	r9, #1
	mov	r8, r0
.L182:
	ldr	r3, [r4, #44]
	ldrb	r3, [r3, #464]	@ zero_extendqisi2
	tst	r3, #32
	beq	.L183
	ldr	r3, [r4, #32]
	cmp	r3, #10
	bne	.L183
	mov	r0, sl
	bl	POC_get_has_pump_attached
	cmp	r0, #1
	bne	.L183
	ldr	r3, [r4, #44]
	ldrb	r3, [r3, #464]	@ zero_extendqisi2
	tst	r3, #64
	mov	r3, #472
	mla	r3, r7, r3, r6
	ldrb	r2, [r3, #108]	@ zero_extendqisi2
	orrne	r2, r2, #2
	bne	.L261
	b	.L265
.L183:
	mov	r3, #472
	mla	r3, r7, r3, r6
	ldrb	r2, [r3, #108]	@ zero_extendqisi2
.L265:
	bic	r2, r2, #2
.L261:
	cmp	r5, #0
	strb	r2, [r3, #108]
	bne	.L188
	ldr	r3, [r4, #44]
	ldrb	r2, [r3, #468]	@ zero_extendqisi2
	tst	r2, #64
	beq	.L189
.L188:
	mov	r3, #472
	mul	r3, r7, r3
	mov	sl, #0
	str	sl, [r4, #372]
	mov	r9, r3
	b	.L190
.L191:
	add	r3, sl, #1
	mov	sl, #88
	mla	sl, r3, sl, r9
	mov	r1, #444
	add	r0, sl, #20
	add	r0, r6, r0
	add	sl, r6, sl
	str	r3, [sp, #4]
	bl	POC_PRESERVES_set_master_valve_energized_bit
	ldrb	r2, [sl, #20]	@ zero_extendqisi2
	bic	r2, r2, #2
	strb	r2, [sl, #20]
	ldr	r3, [sp, #4]
	mov	sl, r3
.L190:
	cmp	sl, r8
	bne	.L191
	b	.L266
.L189:
	tst	r2, #32
	bne	.L193
	ldrb	r3, [r3, #464]	@ zero_extendqisi2
	tst	r3, #32
	beq	.L194
.L193:
	cmp	r9, #0
	movne	r3, #1
	strne	r3, [r4, #372]
	bne	.L244
	mov	r0, #472
	mla	r0, r7, r0, r6
	ldr	r1, .L268+124
	add	r0, r0, #108
	bl	POC_PRESERVES_set_master_valve_energized_bit
	b	.L244
.L194:
	cmp	r9, #0
	moveq	r3, #472
	mlaeq	r3, r7, r3, r6
	strne	r5, [r4, #372]
	ldreqb	r2, [r3, #108]	@ zero_extendqisi2
	biceq	r2, r2, #1
	streqb	r2, [r3, #108]
	mov	r3, #472
	mla	r3, r7, r3, r6
	ldrb	r2, [r3, #108]	@ zero_extendqisi2
	bic	r2, r2, #2
	strb	r2, [r3, #108]
	ldr	r3, [r4, #44]
	ldrb	r3, [r3, #464]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L244
	ldr	r0, .L268+4
	bl	RemovePathFromFileName
	ldr	r2, .L268+128
	mov	r1, r0
	ldr	r0, .L268+132
	bl	Alert_Message_va
	b	.L244
.L266:
	cmp	r5, #0
	bne	.L199
.L244:
	ldr	r3, [r4, #44]
	ldrb	sl, [r3, #468]	@ zero_extendqisi2
	ands	sl, sl, #96
	bne	.L200
	ldr	r3, [r4, #376]
	cmp	r3, #129
	movhi	r9, #472
	mulhi	r9, r7, r9
	bhi	.L201
	b	.L200
.L203:
	add	r3, sl, #1
	mov	r2, #88
	mla	r3, r2, r3, r9
	add	r3, r6, r3
	ldrb	r3, [r3, #20]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L202
	ldr	r3, [r4, #376]
	cmp	r3, #134
	bhi	.L202
	ldr	r0, .L268+136
	bl	Alert_Message
.L202:
	add	sl, sl, #1
	mov	r3, #88
	ldr	r2, .L268+100
	mla	r3, sl, r3, r9
	add	r3, r2, r3
	ldrb	r2, [r3, #20]	@ zero_extendqisi2
	bic	r2, r2, #1
	strb	r2, [r3, #20]
.L201:
	cmp	sl, r8
	bne	.L203
	b	.L200
.L199:
	mov	r9, #472
	mov	sl, #0
	mul	r9, r7, r9
	b	.L204
.L267:
	ldr	r3, [r4, #44]
	ldrb	r3, [r3, #468]	@ zero_extendqisi2
	tst	r3, #64
	bne	.L199
	b	.L205
.L209:
	add	r3, sl, #1
	mov	r2, #88
	mla	r3, r2, r3, r9
	add	r3, r6, r3
	ldrb	r3, [r3, #20]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L206
	cmp	r5, #0
	ldrne	r0, .L268+140
	bne	.L262
	ldr	r3, [r4, #376]
	cmp	r3, #34
	ldrls	r0, .L268+144
	bls	.L262
	ldr	r3, [r4, #44]
	ldrb	r3, [r3, #468]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L206
	ldr	r0, .L268+148
.L262:
	bl	Alert_Message
.L206:
	add	sl, sl, #1
	mov	r3, #88
	mla	r3, sl, r3, r9
	ldr	r1, .L268+100
	add	r3, r1, r3
	ldrb	r2, [r3, #20]	@ zero_extendqisi2
	bic	r2, r2, #2
	strb	r2, [r3, #20]
.L204:
	cmp	sl, r8
	bne	.L209
.L205:
	ldr	r2, [sp, #28]
	cmp	r2, #0
	bne	.L210
	mov	r3, #472
	mla	r3, r7, r3, r6
	ldrb	r2, [r3, #108]	@ zero_extendqisi2
	tst	r2, #1
	andne	r2, r2, #175
	strneb	r2, [r3, #108]
.L210:
	ldr	r3, [sp, #36]
	cmp	r3, #0
	bne	.L211
	mov	r3, #472
	mla	r3, r7, r3, r6
	ldrb	r2, [r3, #108]	@ zero_extendqisi2
	tst	r2, #2
	andne	r2, r2, #95
	strneb	r2, [r3, #108]
.L211:
	ldr	r5, [r4, #28]
	bl	FLOWSENSE_get_controller_index
	cmp	r5, r0
	bne	.L174
	mov	r9, #472
	mul	r9, r7, r9
	mov	r5, #0
	add	sl, r9, #20
	add	r3, r6, r9
	add	sl, sl, r6
	str	r3, [sp, #28]
	b	.L212
.L219:
	add	r3, r5, #1
	mov	r1, #88
	mla	r3, r1, r3, r9
	cmp	r5, #0
	add	r3, r6, r3
	ldrb	r3, [r3, #20]	@ zero_extendqisi2
	and	r3, r3, #1
	bne	.L213
	ldr	r1, [sp, #28]
	ldrb	r2, [r1, #108]	@ zero_extendqisi2
	tst	r2, #2
	bne	.L214
.L213:
	cmp	r3, #0
	beq	.L215
.L214:
	ldr	r2, [sp, #16]
	cmp	r2, #5
	bhi	.L216
	add	r3, r2, #1
	and	r3, r3, #255
	mov	r1, #0
	mov	r2, #8
	add	r0, sp, #220
	str	r3, [sp, #16]
	bl	memset
	ldr	r3, [sl, #92]
	mov	r1, #88
	str	r3, [sp, #220]
	add	r3, r5, #1
	mla	r3, r1, r3, r9
	mov	r2, #88
	add	r3, r6, r3
	ldrb	r3, [r3, #20]	@ zero_extendqisi2
	ldr	r1, .L268+100
	tst	r3, #1
	ldrneb	r3, [sp, #224]	@ zero_extendqisi2
	ldr	r0, [sp, #24]
	orrne	r3, r3, #1
	strneb	r3, [sp, #224]
	add	r3, r5, #1
	mla	r3, r2, r3, r9
	mov	r2, #8
	add	r3, r1, r3
	ldrb	r3, [r3, #20]	@ zero_extendqisi2
	add	r1, sp, #220
	tst	r3, #2
	ldrneb	r3, [sp, #224]	@ zero_extendqisi2
	add	fp, fp, #8
	orrne	r3, r3, #2
	strneb	r3, [sp, #224]
	bl	memcpy
	ldr	r2, [sp, #24]
	add	r2, r2, #8
	str	r2, [sp, #24]
	b	.L215
.L216:
	ldr	r0, .L268+152
	bl	Alert_Message
.L215:
	add	r5, r5, #1
	add	sl, sl, #88
.L212:
	cmp	r5, r8
	bne	.L219
.L174:
.LBE76:
	add	r7, r7, #1
	cmp	r7, #12
	add	r4, r4, #472
	bne	.L220
	ldr	r3, .L268+108
	ldr	r8, [sp, #40]
	ldr	r0, [r3, #0]
	ldr	r5, [sp, #44]
	ldr	sl, [sp, #48]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L268+96
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, [sp, #16]
	ldr	r1, [sp, #8]
	ldr	r2, [sp, #16]
	strb	r3, [r1, #0]
	ldr	r3, [sp, #20]
	cmp	r2, #0
	orreq	r3, r3, #1
	cmp	r3, #0
	movne	r0, r1
	ldrne	r2, .L268+156
	ldrne	r1, .L268+4
	bne	.L263
.L221:
.LBE73:
	cmp	r8, fp
	bcc	.L223
	ldrh	r2, [sp, #248]
	ldrh	r3, [sp, #246]
	mov	r0, r5
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #4
	strh	r3, [sp, #246]	@ movhi
	ldr	r1, [sp, #8]
	mov	r3, r3, lsr #16
	mov	r2, fp
	strh	r3, [sp, #248]	@ movhi
	bl	memcpy
	add	r5, r5, fp
	add	sl, sl, fp
	rsb	r8, fp, r8
.L223:
	ldr	r0, [sp, #8]
	ldr	r1, .L268+4
	ldr	r2, .L268+160
.L263:
	bl	mem_free_debug
.LBB78:
	ldr	r3, .L268+164
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L268+4
	ldr	r3, .L268+168
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L268+4
	ldr	r2, .L268+172
	mov	r0, #49
	bl	mem_malloc_debug
	mov	r7, #0
	mov	r4, #1
	mov	r9, r0
	add	fp, r0, #1
	bl	FLOWSENSE_get_controller_index
	str	r0, [sp, #8]
	ldr	r0, .L268+176
	bl	nm_ListGetFirst
	mov	r6, r0
	b	.L224
.L228:
	ldr	r3, [r6, #32]
	ldr	r1, [sp, #8]
	cmp	r3, r1
	bne	.L225
	ldr	r3, [r6, #20]
	cmp	r3, #1
	bne	.L225
	cmp	r7, #3
	bls	.L226
	ldr	r0, .L268+180
	str	r3, [sp, #4]
	bl	Alert_message_on_tpmicro_pile_M
	ldr	r3, [sp, #4]
	mov	r6, r3
	b	.L227
.L226:
	ldr	r3, [r6, #36]
	mov	r2, #0
	str	r2, [sp, #212]
	str	r2, [sp, #216]
	mov	r0, fp
	add	r1, sp, #208
	mov	r2, #12
	str	r3, [sp, #208]
	bl	memcpy
	add	r7, r7, #1
	add	fp, fp, #12
	add	r4, r4, #12
	and	r7, r7, #255
.L225:
	mov	r1, r6
	ldr	r0, .L268+176
	bl	nm_ListGetNext
	mov	r6, r0
.L224:
	cmp	r6, #0
	bne	.L228
.L227:
	ldr	r3, .L268+164
	cmp	r7, #0
	orreq	r6, r6, #1
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	tst	r6, #255
	strb	r7, [r9, #0]
	movne	r0, r9
	ldrne	r1, .L268+4
	ldrne	r2, .L268+184
	bne	.L264
.L229:
.LBE78:
	cmp	r8, r4
	bcc	.L231
	ldrh	r2, [sp, #248]
	ldrh	r3, [sp, #246]
	mov	r0, r5
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #2
	strh	r3, [sp, #246]	@ movhi
	mov	r1, r9
	mov	r3, r3, lsr #16
	mov	r2, r4
	strh	r3, [sp, #248]	@ movhi
	bl	memcpy
	add	r5, r5, r4
	add	sl, sl, r4
	rsb	r8, r4, r8
.L231:
	ldr	r1, .L268+4
	ldr	r2, .L268+188
	mov	r0, r9
.L264:
	ldr	r4, .L268+36
	bl	mem_free_debug
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L232
	cmp	r8, #15
	bls	.L232
	ldrh	r2, [sp, #248]
	ldrh	r3, [sp, #246]
	add	r0, sp, #180
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #65536
	strh	r3, [sp, #246]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #248]	@ movhi
	bl	WEATHER_get_a_copy_of_the_wind_settings
	mov	r0, r5
	add	r1, sp, #180
	mov	r2, #16
	bl	memcpy
	add	r5, r5, #16
	mov	r3, #0
	add	sl, sl, #16
	sub	r8, r8, #16
	str	r3, [r4, #0]
.L232:
	ldr	r3, .L268+36
	ldr	r2, [r3, #24]
	cmp	r2, #0
	beq	.L233
	ldrh	r2, [sp, #246]
	ldrh	r1, [sp, #248]
	orr	r2, r2, r1, asl #16
	orr	r2, r2, #16384
	strh	r2, [sp, #246]	@ movhi
	mov	r2, r2, lsr #16
	strh	r2, [sp, #248]	@ movhi
	mov	r2, #0
	str	r2, [r3, #24]
.L233:
	ldr	r6, .L268+36
	ldr	r4, .L268+192
	ldr	r3, [r6, r4]
	cmp	r3, #0
	beq	.L234
	cmp	r8, #3
	bls	.L234
	ldrh	r2, [sp, #248]
	ldrh	r3, [sp, #246]
	mov	r0, r5
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #128
	strh	r3, [sp, #246]	@ movhi
	ldr	r1, .L268+196
	mov	r3, r3, lsr #16
	mov	r2, #4
	strh	r3, [sp, #248]	@ movhi
	add	r5, r5, #4
	bl	memcpy
	add	sl, sl, #4
	mov	r3, #0
	sub	r8, r8, #4
	str	r3, [r6, r4]
.L234:
	ldr	r3, .L268+200
	ldr	r2, .L268+36
	ldr	r3, [r2, r3]
	cmp	r3, #0
	beq	.L235
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L268+204
	mov	r2, #92
	mla	r3, r2, r0, r3
	ldr	r3, [r3, #80]
	cmp	r3, #0
	beq	.L236
	ldrh	r3, [sp, #246]
	ldrh	r2, [sp, #248]
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #256
	strh	r3, [sp, #246]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #248]	@ movhi
.L236:
	ldr	r3, .L268+200
	ldr	r2, .L268+36
	mov	r1, #0
	str	r1, [r2, r3]
.L235:
	ldr	r4, .L268+208
	ldrb	r3, [r4, #130]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L237
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L268+204
	mov	r2, #92
	mla	r3, r2, r0, r3
	ldr	r3, [r3, #80]
	cmp	r3, #0
	beq	.L237
	ldrh	r3, [sp, #246]
	ldrh	r2, [sp, #248]
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #256
	strh	r3, [sp, #246]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #248]	@ movhi
	mov	r3, #0	@ movhi
	strb	r3, [r4, #130]
.L237:
	ldr	r2, .L268+36
	ldr	r3, .L268+212
	ldr	r1, [r2, r3]
	cmp	r1, #0
	beq	.L238
	ldrh	r1, [sp, #246]
	ldrh	r0, [sp, #248]
	orr	r1, r1, r0, asl #16
	orr	r1, r1, #512
	strh	r1, [sp, #246]	@ movhi
	mov	r1, r1, lsr #16
	strh	r1, [sp, #248]	@ movhi
	mov	r1, #0
	str	r1, [r2, r3]
.L238:
	ldr	r2, .L268+36
	ldr	r3, .L268+216
	ldr	r1, [r2, r3]
	cmp	r1, #0
	beq	.L239
	ldrh	r1, [sp, #246]
	ldrh	r0, [sp, #248]
	orr	r1, r1, r0, asl #16
	orr	r1, r1, #1024
	strh	r1, [sp, #246]	@ movhi
	mov	r1, r1, lsr #16
	strh	r1, [sp, #248]	@ movhi
	mov	r1, #0
	str	r1, [r2, r3]
.L239:
	ldr	r2, .L268+36
	ldr	r3, .L268+220
	ldr	r1, [r2, r3]
	cmp	r1, #0
	beq	.L240
	ldrh	r1, [sp, #246]
	ldrh	r0, [sp, #248]
	orr	r1, r1, r0, asl #16
	orr	r1, r1, #2048
	strh	r1, [sp, #246]	@ movhi
	mov	r1, r1, lsr #16
	strh	r1, [sp, #248]	@ movhi
	mov	r1, #0
	str	r1, [r2, r3]
.L240:
	ldr	r2, .L268+36
	ldr	r3, .L268+224
	ldr	r1, [r2, r3]
	cmp	r1, #0
	beq	.L241
	ldrh	r1, [sp, #246]
	ldrh	r0, [sp, #248]
	orr	r1, r1, r0, asl #16
	orr	r1, r1, #4096
	strh	r1, [sp, #246]	@ movhi
	mov	r1, r1, lsr #16
	strh	r1, [sp, #248]	@ movhi
	mov	r1, #0
	str	r1, [r2, r3]
.L241:
	ldr	r6, .L268+36
	ldr	r4, .L268+228
	ldr	r3, [r6, r4]
	cmp	r3, #0
	beq	.L242
	cmp	r8, #11
	bls	.L242
	ldrh	r2, [sp, #248]
	ldrh	r3, [sp, #246]
	mov	r0, r5
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #8192
	strh	r3, [sp, #246]	@ movhi
	ldr	r1, .L268+232
	mov	r3, r3, lsr #16
	mov	r2, #12
	strh	r3, [sp, #248]	@ movhi
	add	r5, r5, #12
	bl	memcpy
	add	sl, sl, #12
	mov	r3, #0
	sub	r8, r8, #12
	str	r3, [r6, r4]
.L242:
	ldr	r6, .L268+36
	ldr	r4, .L268+236
	ldr	r3, [r6, r4]
	cmp	r3, #0
	beq	.L254
	cmp	r8, #3
	bls	.L254
	ldrh	r2, [sp, #248]
	ldrh	r3, [sp, #246]
	mov	r0, r5
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #131072
	strh	r3, [sp, #246]	@ movhi
	ldr	r1, .L268+240
	mov	r3, r3, lsr #16
	mov	r2, #4
	strh	r3, [sp, #248]	@ movhi
	add	r5, r5, #4
	bl	memcpy
	add	sl, sl, #4
	mov	r3, #0
	str	r3, [r6, r4]
.L254:
	mov	r4, #1000
.L159:
.LBE71:
	add	r1, sp, #244
	mov	r2, #6
	ldr	r0, [sp, #32]
	bl	memcpy
	mov	r1, sl
	ldr	r0, [sp, #12]
	bl	CRC_calculate_32bit_big_endian
	add	r1, sp, #256
	mov	r2, #4
	str	r0, [r1, #-4]!
	mov	r0, r5
	bl	memcpy
	mov	r0, #0
	mov	r3, r0
	ldr	r1, [sp, #12]
	add	r2, sl, #4
	bl	Get_New_Memory_Block_then_Add_Ambles_then_Xmit_A_Copy
	ldr	r0, [sp, #12]
	ldr	r2, .L268+244
	ldr	r1, .L268+4
	bl	mem_free_debug
	mov	r0, r4
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L268
	mov	r1, #2
	mov	r2, r0
	ldr	r0, [r3, #8]
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L150
.L269:
	.align	2
.L268:
	.word	.LANCHOR0
	.word	.LC0
	.word	.LC26
	.word	.LC27
	.word	.LC28
	.word	comm_mngr
	.word	2735
	.word	2068
	.word	config_c+48
	.word	tpmicro_data
	.word	-32768
	.word	5060
	.word	irri_irri_recursive_MUTEX
	.word	1810
	.word	1813
	.word	list_program_data_recursive_MUTEX
	.word	irri_irri
	.word	.LC29
	.word	1861
	.word	.LC30
	.word	2046
	.word	1915
	.word	2954
	.word	1995
	.word	poc_preserves_recursive_MUTEX
	.word	poc_preserves
	.word	2012
	.word	list_poc_recursive_MUTEX
	.word	2014
	.word	.LC31
	.word	2061
	.word	333
	.word	2234
	.word	.LC32
	.word	.LC33
	.word	.LC34
	.word	.LC35
	.word	.LC36
	.word	.LC37
	.word	2419
	.word	2996
	.word	irri_lights_recursive_MUTEX
	.word	2460
	.word	2465
	.word	irri_lights
	.word	.LC38
	.word	2543
	.word	3029
	.word	5020
	.word	tpmicro_data+5024
	.word	5028
	.word	chain
	.word	weather_preserves
	.word	5032
	.word	5036
	.word	5040
	.word	5044
	.word	5076
	.word	tpmicro_data+5080
	.word	5068
	.word	tpmicro_data+5072
	.word	3245
	.word	.LANCHOR0
	.word	.LC39
	.word	.LC40
.L157:
	ldr	r3, .L268+248
	strh	r1, [sp, #232]	@ movhi
	ldr	r1, [r3, #68]
	ldr	r3, [r3, #84]
	ldr	r0, .L268+252
	str	r1, [sp, #228]
	strh	r2, [sp, #240]	@ movhi
	str	r3, [sp, #236]
	bl	Alert_Message
	ldr	r0, .L268+252
	bl	Alert_message_on_tpmicro_pile_M
	add	r3, sp, #228
	ldmia	r3, {r2, r3}
	mov	r1, #32
	add	r0, sp, #116
	bl	DATE_TIME_to_DateTimeStr_32
	add	r3, sp, #236
	ldmia	r3, {r2, r3}
	mov	r1, #32
	mov	r4, r0
	add	r0, sp, #148
	bl	DATE_TIME_to_DateTimeStr_32
	mov	r3, r4
	mov	r1, #64
	ldr	r2, .L268+256
	mov	r4, #1
	str	r0, [sp, #0]
	add	r0, sp, #52
	bl	snprintf
	add	r0, sp, #52
	bl	Alert_Message
	add	r0, sp, #52
	bl	Alert_message_on_tpmicro_pile_M
	b	.L243
.L200:
.LBB80:
.LBB79:
.LBB77:
	ldr	r3, [r4, #376]
	cmp	r3, #29
	bhi	.L199
	b	.L267
.L150:
.LBE77:
.LBE79:
.LBE80:
.LBE70:
.LBE69:
.LBE68:
	add	sp, sp, #256
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.LFE15:
	.size	TPMICRO_COMM_kick_out_the_next_tpmicro_msg, .-TPMICRO_COMM_kick_out_the_next_tpmicro_msg
	.section	.text.TPMICRO_COMM_create_timer_and_start_messaging,"ax",%progbits
	.align	2
	.global	TPMICRO_COMM_create_timer_and_start_messaging
	.type	TPMICRO_COMM_create_timer_and_start_messaging, %function
TPMICRO_COMM_create_timer_and_start_messaging:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L271
	mov	r2, #0
	stmfd	sp!, {r0, r4, lr}
.LCFI5:
	ldr	r0, .L271+4
	str	r3, [sp, #0]
	mov	r1, #200
	mov	r3, r2
	bl	xTimerCreate
	ldr	r3, .L271+8
	str	r0, [r3, #8]
	mov	r4, r0
	bl	xTaskGetTickCount
	mvn	r3, #0
	mov	r1, #0
	str	r3, [sp, #0]
	mov	r3, r1
	mov	r2, r0
	mov	r0, r4
	bl	xTimerGenericCommand
	ldmfd	sp!, {r3, r4, pc}
.L272:
	.align	2
.L271:
	.word	msg_rate_timer_callback
	.word	.LC41
	.word	.LANCHOR0
.LFE16:
	.size	TPMICRO_COMM_create_timer_and_start_messaging, .-TPMICRO_COMM_create_timer_and_start_messaging
	.section	.text.TPMICRO_make_a_copy_and_queue_incoming_packet,"ax",%progbits
	.align	2
	.global	TPMICRO_make_a_copy_and_queue_incoming_packet
	.type	TPMICRO_make_a_copy_and_queue_incoming_packet, %function
TPMICRO_make_a_copy_and_queue_incoming_packet:
.LFB17:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI6:
	ldr	r2, .L274
	sub	sp, sp, #40
.LCFI7:
	mov	r4, r1
	mov	r6, r0
	mov	r0, r1
	ldr	r1, .L274+4
	bl	mem_malloc_debug
	mov	r1, r6
	mov	r2, r4
	mov	r5, r0
	bl	memcpy
	mov	r3, #2048
	mov	r0, sp
	str	r3, [sp, #0]
	str	r5, [sp, #16]
	str	r4, [sp, #20]
	bl	COMM_MNGR_post_event_with_details
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, pc}
.L275:
	.align	2
.L274:
	.word	3327
	.word	.LC0
.LFE17:
	.size	TPMICRO_make_a_copy_and_queue_incoming_packet, .-TPMICRO_make_a_copy_and_queue_incoming_packet
	.section	.text.TPMICRO_COMM_kick_out_test_message,"ax",%progbits
	.align	2
	.global	TPMICRO_COMM_kick_out_test_message
	.type	TPMICRO_COMM_kick_out_test_message, %function
TPMICRO_COMM_kick_out_test_message:
.LFB18:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI8:
	ldr	r1, .L277
	sub	sp, sp, #40
.LCFI9:
	ldr	r2, .L277+4
	mov	r0, #1000
	bl	mem_malloc_debug
	mov	r3, #0
	str	r3, [sp, #28]
	strb	r3, [sp, #20]
	mov	r3, #17
	strb	r3, [sp, #21]
	mov	r3, #34
	strb	r3, [sp, #22]
	mov	r1, #1000
	mov	r2, sp
	mov	r3, #6
	bl	SendCommandResponseAndFree
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L278:
	.align	2
.L277:
	.word	.LC0
	.word	3356
.LFE18:
	.size	TPMICRO_COMM_kick_out_test_message, .-TPMICRO_COMM_kick_out_test_message
	.global	tpmicro_comm
	.global	TPMICRO_debug_text
	.section	.bss.tpmicro_comm,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	tpmicro_comm, %object
	.size	tpmicro_comm, 172
tpmicro_comm:
	.space	172
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/tpmicro_comm.c\000"
.LC1:
	.ascii	"TPMicro is running %s\000"
.LC2:
	.ascii	"Whats Installed rcvd from TPMicro\000"
.LC3:
	.ascii	"Unexpected 2W cable I state\000"
.LC4:
	.ascii	"Unexpected 2W cable H state\000"
.LC5:
	.ascii	"Unexpected 2W cable C-O state\000"
.LC6:
	.ascii	"tpmicro_comm: rain bucket pulse\000"
.LC7:
	.ascii	"TP COMM: msg contains too many pocs\000"
.LC8:
	.ascii	"TP_COMM: poc not found in preserves : %s, %u\000"
.LC9:
	.ascii	"sn: %u , count/ms: %u / %u , five: %u\000"
.LC10:
	.ascii	"BYPASS queue full : %s, %u\000"
.LC11:
	.ascii	"MOISTURE group not found!\000"
.LC12:
	.ascii	"poc preserves not found : %s, %u\000"
.LC13:
	.ascii	"rcvd current draw content out of range.\000"
.LC14:
	.ascii	"rcvd short result - out_of_range.\000"
.LC15:
	.ascii	"rcvd short terminal - out_of_range.\000"
.LC16:
	.ascii	"TP MICRO : unexp state. : %s, %u\000"
.LC17:
	.ascii	"TPMICRO: box current error. : %s, %u\000"
.LC18:
	.ascii	"wind speed is %u mph\000"
.LC19:
	.ascii	"too many discovered decoders - %d\000"
.LC20:
	.ascii	"stats for %d decoders\000"
.LC21:
	.ascii	"statistics decoder not found - %07d\000"
.LC22:
	.ascii	"stats for too many decoders - %d\000"
.LC23:
	.ascii	"alert message length out of range (%d)\000"
.LC24:
	.ascii	"PROBLEM WITH DATA\000"
.LC25:
	.ascii	"unexp command from tpmicro.\000"
.LC26:
	.ascii	"TPMicro comm unexpd event : %s, %u\000"
.LC27:
	.ascii	"last warning, no msgs from TPMicro\000"
.LC28:
	.ascii	"%u seconds since last msg from TPmicro\000"
.LC29:
	.ascii	"too many ON for msg to TPMicro.\000"
.LC30:
	.ascii	"Msg to TPMicro: station ON not found.\000"
.LC31:
	.ascii	"file POC not found : %s, %u\000"
.LC32:
	.ascii	"IRRI_FLOW: unexp pump rqst! : %s, %u\000"
.LC33:
	.ascii	"TPMICRO COMM: MV energized w/ no valves ON!\000"
.LC34:
	.ascii	"TPMICRO COMM: PUMP energized w/ MLB!\000"
.LC35:
	.ascii	"TPMICRO COMM: PUMP energized w/ no valves ON!\000"
.LC36:
	.ascii	"TPMICRO COMM: PUMP energized w/ MVOR closed\000"
.LC37:
	.ascii	"POC ON hit the limit! Cannot energize them all!!\000"
.LC38:
	.ascii	"too many LIGHTS ON\000"
.LC39:
	.ascii	"TPMicro code ISP update starting\000"
.LC40:
	.ascii	"... from %s to %s\000"
.LC41:
	.ascii	"TP msg timer\000"
	.section	.bss.TPMICRO_debug_text,"aw",%nobits
	.set	.LANCHOR1,. + 0
	.type	TPMICRO_debug_text, %object
	.size	TPMICRO_debug_text, 896
TPMICRO_debug_text:
	.space	896
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI0-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI1-.LFB9
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x1a0
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI3-.LFB15
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x124
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI5-.LFB16
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI6-.LFB17
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI8-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/tpmicro_comm.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x129
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF18
	.byte	0x1
	.4byte	.LASF19
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x352
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x36b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x2a9
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x6db
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x329
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST0
	.uleb128 0x6
	.4byte	0x2a
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x248
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF5
	.byte	0x1
	.byte	0x5b
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF6
	.byte	0x1
	.byte	0xf4
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x135
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x2e5
	.byte	0x1
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x383
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST1
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0xa12
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x6f8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x7a3
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x980
	.byte	0x1
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0xcbf
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST2
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0xcce
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST3
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0xcf7
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0xd14
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB6
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB9
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI2
	.4byte	.LFE9
	.2byte	0x3
	.byte	0x7d
	.sleb128 416
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB15
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI4
	.4byte	.LFE15
	.2byte	0x3
	.byte	0x7d
	.sleb128 292
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB16
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB17
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI7
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB18
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI9
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF11:
	.ascii	"build_poc_output_activation_for_tpmicro_msg\000"
.LASF20:
	.ascii	"TP_MICRO_COMM_resync_wind_settings\000"
.LASF10:
	.ascii	"build_stations_ON_for_tpmicro_msg\000"
.LASF0:
	.ascii	"extract_executing_code_date_and_time_out_of_msg_fro"
	.ascii	"m_tpmicro\000"
.LASF5:
	.ascii	"extract_flow_meter_readings_out_of_msg_from_tpmicro"
	.ascii	"\000"
.LASF1:
	.ascii	"extract_box_level_measured_current_out_of_msg_from_"
	.ascii	"tpmicro\000"
.LASF13:
	.ascii	"TPMICRO_COMM_parse_incoming_message_from_tp_micro\000"
.LASF19:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/tpmicro_comm.c\000"
.LASF15:
	.ascii	"TPMICRO_COMM_create_timer_and_start_messaging\000"
.LASF18:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF12:
	.ascii	"build_lights_ON_for_tpmicro_msg\000"
.LASF7:
	.ascii	"extract_ma_readings_out_of_msg_from_tpmicro\000"
.LASF4:
	.ascii	"extract_whats_installed_out_of_msg_from_tpmicro\000"
.LASF17:
	.ascii	"TPMICRO_COMM_kick_out_test_message\000"
.LASF16:
	.ascii	"TPMICRO_make_a_copy_and_queue_incoming_packet\000"
.LASF3:
	.ascii	"extract_decoder_fault_content_out_of_msg_from_tpmic"
	.ascii	"ro\000"
.LASF9:
	.ascii	"kick_out_the_next_normal_tpmicro_msg\000"
.LASF2:
	.ascii	"msg_rate_timer_callback\000"
.LASF14:
	.ascii	"TPMICRO_COMM_kick_out_the_next_tpmicro_msg\000"
.LASF6:
	.ascii	"extract_moisture_reading_out_of_msg_from_tpmicro\000"
.LASF8:
	.ascii	"extract_terminal_short_report_out_of_msg_from_tpmic"
	.ascii	"ro\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
