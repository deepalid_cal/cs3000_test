	.file	"sdram_common.c"
	.text
.Ltext0:
	.section	.text.walking0bitsetup,"ax",%progbits
	.align	2
	.type	walking0bitsetup, %function
walking0bitsetup:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	mov	r2, #1
.L2:
	mvn	r1, r2, asl r3
	add	r3, r3, #1
	cmp	r3, #32
	str	r1, [r0], #4
	bne	.L2
	bx	lr
.LFE0:
	.size	walking0bitsetup, .-walking0bitsetup
	.section	.text.walking0bitcheck,"ax",%progbits
	.align	2
	.type	walking0bitcheck, %function
walking0bitcheck:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	mov	r2, #1
.L6:
	ldr	ip, [r0], #4
	mvn	r1, r2, asl r3
	cmp	ip, r1
	bne	.L7
	add	r3, r3, #1
	cmp	r3, #32
	bne	.L6
	mov	r0, #1
	bx	lr
.L7:
	mov	r0, #0
	bx	lr
.LFE1:
	.size	walking0bitcheck, .-walking0bitcheck
	.section	.text.walking1bitsetup,"ax",%progbits
	.align	2
	.type	walking1bitsetup, %function
walking1bitsetup:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	mov	r2, #1
.L10:
	mov	r1, r2, asl r3
	add	r3, r3, #1
	cmp	r3, #32
	str	r1, [r0], #4
	bne	.L10
	bx	lr
.LFE2:
	.size	walking1bitsetup, .-walking1bitsetup
	.section	.text.walking1bitcheck,"ax",%progbits
	.align	2
	.type	walking1bitcheck, %function
walking1bitcheck:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	mov	r2, #1
.L14:
	ldr	r1, [r0], #4
	cmp	r1, r2, asl r3
	bne	.L15
	add	r3, r3, #1
	cmp	r3, #32
	bne	.L14
	mov	r0, #1
	bx	lr
.L15:
	mov	r0, #0
	bx	lr
.LFE3:
	.size	walking1bitcheck, .-walking1bitcheck
	.section	.text.invaddrsetup,"ax",%progbits
	.align	2
	.type	invaddrsetup, %function
invaddrsetup:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	mvn	r2, r0
.L18:
	rsb	r1, r3, r2
	str	r1, [r0, r3]
	add	r3, r3, #4
	cmp	r3, #128
	bne	.L18
	bx	lr
.LFE4:
	.size	invaddrsetup, .-invaddrsetup
	.section	.text.invaddrcheck,"ax",%progbits
	.align	2
	.type	invaddrcheck, %function
invaddrcheck:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	mvn	r2, r0
.L22:
	ldr	ip, [r0, r3]
	rsb	r1, r3, r2
	cmp	ip, r1
	bne	.L23
	add	r3, r3, #4
	cmp	r3, #128
	bne	.L22
	mov	r0, #1
	bx	lr
.L23:
	mov	r0, #0
	bx	lr
.LFE5:
	.size	invaddrcheck, .-invaddrcheck
	.section	.text.noninvaddrsetup,"ax",%progbits
	.align	2
	.type	noninvaddrsetup, %function
noninvaddrsetup:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r3, r0, #128
.L26:
	str	r0, [r0, #0]
	add	r0, r0, #4
	cmp	r0, r3
	bne	.L26
	bx	lr
.LFE6:
	.size	noninvaddrsetup, .-noninvaddrsetup
	.section	.text.noninvaddrcheck,"ax",%progbits
	.align	2
	.type	noninvaddrcheck, %function
noninvaddrcheck:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r3, r0, #128
.L30:
	ldr	r2, [r0, #0]
	cmp	r0, r2
	bne	.L31
	add	r0, r0, #4
	cmp	r0, r3
	bne	.L30
	mov	r0, #1
	bx	lr
.L31:
	mov	r0, #0
	bx	lr
.LFE7:
	.size	noninvaddrcheck, .-noninvaddrcheck
	.section	.text.aa55setup,"ax",%progbits
	.align	2
	.type	aa55setup, %function
aa55setup:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L36
	mov	r3, #0
.L34:
	str	r2, [r0, r3]
	add	r3, r3, #4
	cmp	r3, #128
	bne	.L34
	bx	lr
.L37:
	.align	2
.L36:
	.word	1437226410
.LFE8:
	.size	aa55setup, .-aa55setup
	.section	.text.aa55check,"ax",%progbits
	.align	2
	.type	aa55check, %function
aa55check:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L43
	mov	r3, #0
.L40:
	ldr	r1, [r0, r3]
	cmp	r1, r2
	bne	.L41
	add	r3, r3, #4
	cmp	r3, #128
	bne	.L40
	mov	r0, #1
	bx	lr
.L41:
	mov	r0, #0
	bx	lr
.L44:
	.align	2
.L43:
	.word	1437226410
.LFE9:
	.size	aa55check, .-aa55check
	.section	.text._55aasetup,"ax",%progbits
	.align	2
	.type	_55aasetup, %function
_55aasetup:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L48
	mov	r3, #0
.L46:
	str	r2, [r0, r3]
	add	r3, r3, #4
	cmp	r3, #128
	bne	.L46
	bx	lr
.L49:
	.align	2
.L48:
	.word	1437226410
.LFE10:
	.size	_55aasetup, .-_55aasetup
	.section	.text._55aacheck,"ax",%progbits
	.align	2
	.type	_55aacheck, %function
_55aacheck:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L55
	mov	r3, #0
.L52:
	ldr	r1, [r0, r3]
	cmp	r1, r2
	bne	.L53
	add	r3, r3, #4
	cmp	r3, #128
	bne	.L52
	mov	r0, #1
	bx	lr
.L53:
	mov	r0, #0
	bx	lr
.L56:
	.align	2
.L55:
	.word	1437226410
.LFE11:
	.size	_55aacheck, .-_55aacheck
	.section	.text.dqsin_ddr_mod,"ax",%progbits
	.align	2
	.type	dqsin_ddr_mod, %function
dqsin_ddr_mod:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L58
	and	r2, r0, #31
	ldr	r1, [r3, #104]
	bic	r1, r1, #7232
	bic	r1, r1, #60
	orr	r1, r1, r2, asl #2
	ldr	r2, .L58+4
	ldrb	r2, [r2, r0]	@ zero_extendqisi2
	and	r2, r2, #7
	orr	r2, r1, r2, asl #10
	str	r2, [r3, #104]
	bx	lr
.L59:
	.align	2
.L58:
	.word	1073758208
	.word	.LANCHOR0
.LFE12:
	.size	dqsin_ddr_mod, .-dqsin_ddr_mod
	.section	.text.ddr_find_dqsin_delay,"ax",%progbits
	.align	2
	.global	ddr_find_dqsin_delay
	.type	ddr_find_dqsin_delay, %function
ddr_find_dqsin_delay:
.LFB14:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
	ldr	r3, .L75
	ldr	r9, .L75+4
	mov	r2, #255
	str	r2, [r3, #0]
	str	r2, [r9, #0]
.LBB4:
	add	sl, r0, #65536
	mov	r2, r1, lsr #10
	mov	r7, #0
	sub	fp, r0, #128
	mov	r2, r2, asl #2
	add	sl, sl, #16
	mov	r5, r7
	mov	r4, #1
	add	fp, fp, r1
	str	r2, [sp, #0]
.L68:
.LBE4:
	mov	r0, r4
	bl	dqsin_ddr_mod
.LBB5:
	mov	r6, sl
	b	.L61
.L63:
	mov	r0, r6
	blx	r3
	ldr	r3, [r8, #4]
	mov	r0, r6
	blx	r3
	add	r8, r8, #8
	cmp	r0, #0
	beq	.L62
.L65:
	ldr	r3, [r8, #0]
	cmp	r3, #0
	bne	.L63
	ldr	r2, [sp, #0]
	add	r6, r6, r2
.L61:
	cmp	r6, fp
	bcs	.L64
	ldr	r8, .L75+8
	b	.L65
.L62:
.LBE5:
	cmp	r5, #1
	ldreq	r2, .L75
	subeq	r3, r4, #1
	moveq	r7, r5
	mov	r5, r0
	streq	r3, [r2, #0]
.L67:
	add	r4, r4, #1
	add	sl, sl, #65536
	cmp	r4, #31
	add	sl, sl, #16
	bne	.L68
	cmp	r7, #1
	ldreq	r3, .L75+4
	movne	r0, #15
	ldreq	r0, [r3, #0]
	ldreq	r3, .L75
	ldreq	r3, [r3, #0]
	addeq	r0, r0, r3
	addeq	r0, r0, r0, lsr #31
	moveq	r0, r0, asr #1
	bl	dqsin_ddr_mod
	mov	r0, r7
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L64:
	ldr	r3, [r9, #0]
	mov	r5, #1
	cmp	r3, #255
	streq	r4, [r9, #0]
	b	.L67
.L76:
	.align	2
.L75:
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
.LFE14:
	.size	ddr_find_dqsin_delay, .-ddr_find_dqsin_delay
	.section	.text.ddr_clock_resync,"ax",%progbits
	.align	2
	.global	ddr_clock_resync
	.type	ddr_clock_resync, %function
ddr_clock_resync:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L78
	mov	r0, r0, asl #7
	ldr	r2, [r3, #104]
	orr	r0, r0, #6
	orr	r2, r2, #524288
	str	r2, [r3, #104]
	ldr	r2, [r3, #104]
	bic	r2, r2, #524288
	str	r2, [r3, #104]
	ldr	r3, .L78+4
	str	r0, [r3, #256]
	bx	lr
.L79:
	.align	2
.L78:
	.word	1073758208
	.word	822607872
.LFE15:
	.size	ddr_clock_resync, .-ddr_clock_resync
	.global	__udivsi3
	.section	.text.sdram_adjust_timing,"ax",%progbits
	.align	2
	.global	sdram_adjust_timing
	.type	sdram_adjust_timing, %function
sdram_adjust_timing:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	ldr	r1, .L81
	mov	r5, r0
	bl	__udivsi3
	ldr	r4, .L81+4
	ldr	r1, .L81+8
	and	r0, r0, #15
	str	r0, [r4, #48]
	mov	r0, r5
	bl	__udivsi3
	ldr	r1, .L81+12
	and	r0, r0, #15
	str	r0, [r4, #52]
	mov	r0, r5
	bl	__udivsi3
	ldr	r1, .L81+16
	and	r3, r0, #127
	mov	r7, r0
	str	r3, [r4, #56]
	mov	r0, r5
	bl	__udivsi3
	mov	r3, #7
	ldr	r1, .L81+20
	and	r7, r7, #255
	and	r6, r0, #15
	str	r6, [r4, #68]
	mov	r0, r5
	str	r3, [r4, #72]
	bl	__udivsi3
	mov	r5, r5, lsr #1
	and	r5, r5, #15
	mov	r3, #1
	and	r0, r0, #31
	str	r0, [r4, #76]
	str	r7, [r4, #80]
	str	r6, [r4, #84]
	str	r5, [r4, #88]
	str	r3, [r4, #92]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L82:
	.align	2
.L81:
	.word	55555555
	.word	822607872
	.word	23809523
	.word	14925373
	.word	83333333
	.word	16666666
.LFE16:
	.size	sdram_adjust_timing, .-sdram_adjust_timing
	.section	.text.sdram_find_config,"ax",%progbits
	.align	2
	.global	sdram_find_config
	.type	sdram_find_config, %function
sdram_find_config:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	mov	r0, r3
	mov	r2, r3
	ldr	ip, .L90
	b	.L84
.L86:
	add	r1, ip, r3
	ldrb	r0, [r1, #2]	@ zero_extendqisi2
	cmp	r0, #8
	bne	.L89
	ldrb	r0, [r1, #1]	@ zero_extendqisi2
	cmp	r0, #11
	bne	.L89
	ldrb	r0, [r3, ip]	@ zero_extendqisi2
	cmp	r0, #2
	ldreqb	r0, [r1, #3]	@ zero_extendqisi2
	beq	.L85
.L89:
	mov	r0, #0
.L85:
	add	r2, r2, #1
	add	r3, r3, #4
.L84:
	cmp	r2, #12
	cmple	r0, #0
	beq	.L86
	ldr	r3, .L90+4
	mov	r2, #12
	str	r2, [r3, #0]
	ldr	r3, .L90+8
	mov	r2, #10
	str	r2, [r3, #0]
	bx	lr
.L91:
	.align	2
.L90:
	.word	.LANCHOR4
	.word	.LANCHOR5
	.word	.LANCHOR6
.LFE17:
	.size	sdram_find_config, .-sdram_find_config
	.section	.text.sdram_get_bankmask,"ax",%progbits
	.align	2
	.global	sdram_get_bankmask
	.type	sdram_get_bankmask, %function
sdram_get_bankmask:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L95
	ldr	r3, [r3, #0]
	tst	r3, #1
	orrne	r0, r0, r1, asl #1
	orreq	r0, r1, r0, asl #1
	mov	r0, r0, asl r3
	bx	lr
.L96:
	.align	2
.L95:
	.word	.LANCHOR6
.LFE18:
	.size	sdram_get_bankmask, .-sdram_get_bankmask
	.global	dqsend
	.global	dqsstart
	.global	bankshift
	.global	modeshift
	.global	sdram_map
	.global	bus32
	.section	.rodata.sdram_map,"a",%progbits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	sdram_map, %object
	.size	sdram_map, 52
sdram_map:
	.byte	2
	.byte	13
	.byte	10
	.byte	-111
	.byte	2
	.byte	13
	.byte	11
	.byte	-112
	.byte	2
	.byte	13
	.byte	8
	.byte	-114
	.byte	2
	.byte	13
	.byte	9
	.byte	-115
	.byte	2
	.byte	12
	.byte	8
	.byte	-118
	.byte	2
	.byte	12
	.byte	9
	.byte	-119
	.byte	2
	.byte	12
	.byte	10
	.byte	-120
	.byte	2
	.byte	11
	.byte	8
	.byte	-122
	.byte	1
	.byte	11
	.byte	8
	.byte	-127
	.byte	1
	.byte	11
	.byte	9
	.byte	-128
	.space	12
	.section	.bss.bankshift,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	bankshift, %object
	.size	bankshift, 4
bankshift:
	.space	4
	.section	.rodata.testvecs,"a",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	testvecs, %object
	.size	testvecs, 56
testvecs:
	.word	walking0bitsetup
	.word	walking0bitcheck
	.word	walking1bitsetup
	.word	walking1bitcheck
	.word	invaddrsetup
	.word	invaddrcheck
	.word	noninvaddrsetup
	.word	noninvaddrcheck
	.word	aa55setup
	.word	aa55check
	.word	_55aasetup
	.word	_55aacheck
	.word	0
	.word	0
	.section	.rodata.dqs2calsen,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	dqs2calsen, %object
	.size	dqs2calsen, 32
dqs2calsen:
	.byte	7
	.byte	5
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.bss.modeshift,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	modeshift, %object
	.size	modeshift, 4
modeshift:
	.space	4
	.section	.bss.dqsstart,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	dqsstart, %object
	.size	dqsstart, 4
dqsstart:
	.space	4
	.section	.rodata.bus32,"a",%progbits
	.align	2
	.type	bus32, %object
	.size	bus32, 4
bus32:
	.word	1
	.section	.bss.dqsend,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	dqsend, %object
	.size	dqsend, 4
dqsend:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI0-.LFB14
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI1-.LFB16
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.align	2
.LEFDE34:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/sdram_common.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x17b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF18
	.byte	0x1
	.4byte	.LASF19
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x16e
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.byte	0x9d
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0xa6
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0xb5
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0xbe
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.byte	0xcd
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.byte	0xd6
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x1
	.byte	0xe5
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x1
	.byte	0xee
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x1
	.byte	0xfd
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x106
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x115
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x11e
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x14a
	.4byte	.LFB12
	.4byte	.LFE12
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x1b1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x20b
	.4byte	.LFB15
	.4byte	.LFE15
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x23b
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x2c2
	.4byte	.LFB17
	.4byte	.LFE17
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x2fb
	.4byte	.LFB18
	.4byte	.LFE18
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB14
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB16
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xa4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF12:
	.ascii	"dqsin_ddr_mod\000"
.LASF9:
	.ascii	"aa55check\000"
.LASF5:
	.ascii	"invaddrcheck\000"
.LASF10:
	.ascii	"_55aasetup\000"
.LASF6:
	.ascii	"noninvaddrsetup\000"
.LASF1:
	.ascii	"walking0bitcheck\000"
.LASF15:
	.ascii	"sdram_adjust_timing\000"
.LASF2:
	.ascii	"walking1bitsetup\000"
.LASF18:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF11:
	.ascii	"_55aacheck\000"
.LASF16:
	.ascii	"sdram_find_config\000"
.LASF19:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/board_in"
	.ascii	"it/sdram_common.c\000"
.LASF8:
	.ascii	"aa55setup\000"
.LASF3:
	.ascii	"walking1bitcheck\000"
.LASF7:
	.ascii	"noninvaddrcheck\000"
.LASF13:
	.ascii	"ddr_find_dqsin_delay\000"
.LASF4:
	.ascii	"invaddrsetup\000"
.LASF17:
	.ascii	"sdram_get_bankmask\000"
.LASF20:
	.ascii	"ddr_memtst\000"
.LASF0:
	.ascii	"walking0bitsetup\000"
.LASF14:
	.ascii	"ddr_clock_resync\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
