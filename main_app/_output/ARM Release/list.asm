	.file	"list.c"
	.text
.Ltext0:
	.section	.text.vListInitialise,"ax",%progbits
	.align	2
	.global	vListInitialise
	.type	vListInitialise, %function
vListInitialise:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r3, r0, #8
	mvn	r2, #0
	str	r2, [r0, #8]
	str	r3, [r0, #4]
	str	r3, [r0, #12]
	str	r3, [r0, #16]
	mov	r3, #0
	str	r3, [r0, #0]
	bx	lr
.LFE0:
	.size	vListInitialise, .-vListInitialise
	.section	.text.vListInitialiseItem,"ax",%progbits
	.align	2
	.global	vListInitialiseItem
	.type	vListInitialiseItem, %function
vListInitialiseItem:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	str	r3, [r0, #16]
	bx	lr
.LFE1:
	.size	vListInitialiseItem, .-vListInitialiseItem
	.section	.text.vListInsertEnd,"ax",%progbits
	.align	2
	.global	vListInsertEnd
	.type	vListInsertEnd, %function
vListInsertEnd:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #4]
	ldr	r2, [r3, #4]
	stmib	r1, {r2, r3}
	ldr	r2, [r3, #4]
	str	r1, [r2, #8]
	str	r1, [r3, #4]
	ldr	r3, [r0, #0]
	str	r1, [r0, #4]
	add	r3, r3, #1
	str	r0, [r1, #16]
	str	r3, [r0, #0]
	bx	lr
.LFE2:
	.size	vListInsertEnd, .-vListInsertEnd
	.section	.text.vListInsert,"ax",%progbits
	.align	2
	.global	vListInsert
	.type	vListInsert, %function
vListInsert:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, [r1, #0]
	cmn	r2, #1
	ldreq	r3, [r0, #16]
	addne	r3, r0, #8
	bne	.L7
	b	.L6
.L8:
	ldr	r3, [r3, #4]
.L7:
	ldr	ip, [r3, #4]
	ldr	ip, [ip, #0]
	cmp	ip, r2
	bls	.L8
.L6:
	ldr	r2, [r3, #4]
	str	r0, [r1, #16]
	str	r1, [r2, #8]
	str	r2, [r1, #4]
	str	r3, [r1, #8]
	str	r1, [r3, #4]
	ldr	r3, [r0, #0]
	add	r3, r3, #1
	str	r3, [r0, #0]
	bx	lr
.LFE3:
	.size	vListInsert, .-vListInsert
	.section	.text.vListRemove,"ax",%progbits
	.align	2
	.global	vListRemove
	.type	vListRemove, %function
vListRemove:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #4]
	ldr	r2, [r0, #8]
	str	r2, [r3, #8]
	ldr	r2, [r0, #8]
	str	r3, [r2, #4]
	ldr	r3, [r0, #16]
	ldr	r1, [r3, #4]
	cmp	r1, r0
	streq	r2, [r3, #4]
	mov	r2, #0
	str	r2, [r0, #16]
	ldr	r2, [r3, #0]
	sub	r2, r2, #1
	str	r2, [r3, #0]
	bx	lr
.LFE4:
	.size	vListRemove, .-vListRemove
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/list.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x7d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x4c
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x60
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x67
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x7e
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xb7
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"vListInsertEnd\000"
.LASF1:
	.ascii	"vListInitialiseItem\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/list.c\000"
.LASF4:
	.ascii	"vListRemove\000"
.LASF3:
	.ascii	"vListInsert\000"
.LASF0:
	.ascii	"vListInitialise\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
