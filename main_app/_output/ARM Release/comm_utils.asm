	.file	"comm_utils.c"
	.text
.Ltext0:
	.section	.text.this_special_scan_addr_is_for_us,"ax",%progbits
	.align	2
	.global	this_special_scan_addr_is_for_us
	.type	this_special_scan_addr_is_for_us, %function
this_special_scan_addr_is_for_us:
.LFB0:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI0:
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	ldrb	r2, [r0, #0]	@ zero_extendqisi2
	strb	r3, [sp, #5]
	ldrb	r3, [r0, #2]	@ zero_extendqisi2
	cmp	r1, #0
	streqb	r3, [sp, #4]
	strneb	r3, [sp, #6]
	add	r0, sp, #8
	mov	r3, #0
	streqb	r2, [sp, #6]
	strneb	r2, [sp, #4]
	str	r3, [r0, #-8]!
	add	r1, sp, #4
	mov	r2, #3
	mov	r0, sp
	bl	memcpy
	ldr	r4, [sp, #0]
	bl	FLOWSENSE_get_controller_letter
	rsb	r3, r0, r4
	rsbs	r0, r3, #0
	adc	r0, r0, r3
	ldmfd	sp!, {r2, r3, r4, pc}
.LFE0:
	.size	this_special_scan_addr_is_for_us, .-this_special_scan_addr_is_for_us
	.section	.text.this_communication_address_is_our_serial_number,"ax",%progbits
	.align	2
	.global	this_communication_address_is_our_serial_number
	.type	this_communication_address_is_our_serial_number, %function
this_communication_address_is_our_serial_number:
.LFB1:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, lr}
.LCFI1:
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	ldrb	r2, [r0, #0]	@ zero_extendqisi2
	strb	r3, [sp, #5]
	ldrb	r3, [r0, #2]	@ zero_extendqisi2
	cmp	r1, #0
	streqb	r3, [sp, #4]
	strneb	r3, [sp, #6]
	add	r0, sp, #8
	mov	r3, #0
	streqb	r2, [sp, #6]
	strneb	r2, [sp, #4]
	str	r3, [r0, #-8]!
	add	r1, sp, #4
	mov	r0, sp
	mov	r2, #3
	bl	memcpy
	ldr	r3, .L7
	ldr	r0, [r3, #48]
	ldr	r3, [sp, #0]
	rsb	r2, r3, r0
	rsbs	r0, r2, #0
	adc	r0, r0, r2
	ldmfd	sp!, {r2, r3, pc}
.L8:
	.align	2
.L7:
	.word	config_c
.LFE1:
	.size	this_communication_address_is_our_serial_number, .-this_communication_address_is_our_serial_number
	.section	.text.CommAddressesAreEqual,"ax",%progbits
	.align	2
	.global	CommAddressesAreEqual
	.type	CommAddressesAreEqual, %function
CommAddressesAreEqual:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, r0
	mov	r2, #0
	mov	r0, #1
	stmfd	sp!, {r4, lr}
.LCFI2:
.L11:
	ldrb	r4, [r3, r2]	@ zero_extendqisi2
	ldrb	ip, [r1, r2]	@ zero_extendqisi2
	add	r2, r2, #1
	cmp	r4, ip
	movne	r0, #0
	cmp	r2, #3
	bne	.L11
	ldmfd	sp!, {r4, pc}
.LFE2:
	.size	CommAddressesAreEqual, .-CommAddressesAreEqual
	.section	.text.COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity,"ax",%progbits
	.align	2
	.global	COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity
	.type	COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity, %function
COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity:
.LFB3:
	@ args = 12, pretend = 4, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	sp, sp, #4
.LCFI3:
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI4:
	cmp	r0, #5
	sub	sp, sp, #48
.LCFI5:
	mov	r6, r0
	mov	r5, r1
	mov	r8, r2
	str	r3, [sp, #76]
	ldr	r4, [sp, #84]
	bls	.L15
	ldr	r0, .L28
	bl	RemovePathFromFileName
	mov	r2, #221
	mov	r1, r0
	ldr	r0, .L28+4
	bl	Alert_Message_va
	mov	r0, r5
	ldr	r1, .L28
	mov	r2, #224
	b	.L27
.L15:
.LBB2:
	sub	r1, r4, #3
	rsbs	sl, r1, #0
	sub	r0, r4, #5
	adc	sl, sl, r1
	rsbs	r7, r0, #0
	adc	r7, r7, r0
	orrs	r3, r7, sl
	beq	.L17
	bl	TPL_OUT_destroy_all_outgoing_3000_scan_and_token_messages
	bl	TPL_IN_destroy_all_incoming_3000_scan_response_and_token_response_messages
	cmp	sl, #0
	beq	.L18
	add	r0, sp, #79
	mov	r1, #1
	bl	this_special_scan_addr_is_for_us
	cmp	r0, #1
	bne	.L21
	b	.L25
.L18:
	cmp	r7, #0
	beq	.L17
	bl	FLOWSENSE_we_are_the_master_of_a_multiple_controller_network
	adds	r7, r0, #0
	movne	r7, #1
	b	.L19
.L17:
	add	r0, sp, #79
	mov	r1, #1
	bl	this_communication_address_is_our_serial_number
	cmp	r0, #0
	bne	.L25
.L21:
.LBB3:
	mov	r3, #256
	strh	r3, [sp, #12]	@ movhi
	add	r3, sp, #72
	str	r8, [sp, #24]
	str	r5, [sp, #20]
	str	r6, [sp, #28]
	ldmib	r3, {r0, r1}
	ldr	r3, .L28+8
	mov	r2, #0
	str	r3, [sp, #40]
	ldr	r3, .L28+12
	str	r0, [sp, #32]
	strh	r1, [sp, #36]	@ movhi
	ldr	r0, [r3, #0]
	add	r1, sp, #12
	mov	r3, r2
	str	r4, [sp, #44]
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L14
	ldr	r0, .L28
	bl	RemovePathFromFileName
	ldr	r2, .L28+16
	mov	r1, r0
	ldr	r0, .L28+20
	bl	Alert_Message_va
	b	.L14
.L26:
.LBE3:
	ldr	r1, .L28
	ldr	r2, .L28+24
	mov	r0, r5
.L27:
	bl	mem_free_debug
	b	.L14
.L25:
	mov	r7, #0
.L19:
.LBB4:
	add	r3, sp, #72
	ldmib	r3, {r0, r1}
	ldr	r3, .L28+8
	str	r0, [sp, #4]
	strh	r1, [sp, #8]	@ movhi
	mov	r0, r6
	mov	r1, r5
	mov	r2, r8
	str	r4, [sp, #0]
	bl	CENT_COMM_make_copy_of_and_add_message_or_packet_to_list_of_incoming_messages
.LBE4:
	cmp	r7, #1
	bne	.L26
	b	.L21
.L14:
.LBE2:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	add	sp, sp, #4
	bx	lr
.L29:
	.align	2
.L28:
	.word	.LC0
	.word	.LC1
	.word	3000
	.word	TPL_OUT_event_queue
	.word	338
	.word	.LC2
	.word	349
.LFE3:
	.size	COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity, .-COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity
	.section	.text.SendCommandResponseAndFree,"ax",%progbits
	.align	2
	.global	SendCommandResponseAndFree
	.type	SendCommandResponseAndFree, %function
SendCommandResponseAndFree:
.LFB4:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI6:
	mov	r4, r2
	mov	r6, r0
	mov	r7, r1
	add	r0, sp, #8
	ldr	r1, .L31
	mov	r2, #3
	mov	r5, r3
	bl	memcpy
	add	r1, r4, #20
	add	r0, sp, #11
	mov	r2, #3
	bl	memcpy
	ldrh	r3, [sp, #12]
	ldr	r0, [r4, #28]
	strh	r3, [sp, #0]	@ movhi
	mov	r1, r6
	ldr	r3, [sp, #8]
	mov	r2, r7
	str	r5, [sp, #4]
	bl	COMM_MNGR_send_outgoing_3000_message_and_take_on_memory_release_responsiblity
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, pc}
.L32:
	.align	2
.L31:
	.word	config_c+48
.LFE4:
	.size	SendCommandResponseAndFree, .-SendCommandResponseAndFree
	.section	.text.COMM_UTIL_build_and_send_a_simple_flowsense_message,"ax",%progbits
	.align	2
	.global	COMM_UTIL_build_and_send_a_simple_flowsense_message
	.type	COMM_UTIL_build_and_send_a_simple_flowsense_message, %function
COMM_UTIL_build_and_send_a_simple_flowsense_message:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI7:
	mov	r5, r1
	mov	r4, r2
	mov	r6, r0, lsr #16
	ldr	r1, .L34
	ldr	r2, .L34+4
	mov	r0, #2
	bl	mem_malloc_debug
	mov	r1, #2
	mov	r2, r5
	mov	r3, r4
	strh	r6, [r0, #0]	@ movhi
	bl	SendCommandResponseAndFree
	ldmfd	sp!, {r4, r5, r6, pc}
.L35:
	.align	2
.L34:
	.word	.LC0
	.word	381
.LFE5:
	.size	COMM_UTIL_build_and_send_a_simple_flowsense_message, .-COMM_UTIL_build_and_send_a_simple_flowsense_message
	.section	.text.Roll_FIFO,"ax",%progbits
	.align	2
	.global	Roll_FIFO
	.type	Roll_FIFO, %function
Roll_FIFO:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r2, #1
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI8:
	mov	r4, r1
	bgt	.L37
	ldr	r0, .L40
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	Alert_Message
.L37:
	sub	r8, r2, #1
	sub	r6, r2, #2
	mla	r7, r1, r8, r0
	mla	r6, r1, r6, r0
	mov	r5, #0
	b	.L38
.L39:
	mov	r0, r7
	mov	r1, r6
	mov	r2, r4
	bl	memcpy
	add	r5, r5, #1
	mov	r5, r5, asl #16
	rsb	r7, r4, r7
	rsb	r6, r4, r6
	mov	r5, r5, lsr #16
.L38:
	cmp	r5, r8
	blt	.L39
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L41:
	.align	2
.L40:
	.word	.LC3
.LFE6:
	.size	Roll_FIFO, .-Roll_FIFO
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/comm_utils.c\000"
.LC1:
	.ascii	"Port out of range. : %s, %u\000"
.LC2:
	.ascii	"TPL_OUT queue full : %s, %u\000"
.LC3:
	.ascii	"FIFO too small\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI6-.LFB4
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI7-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI8-.LFB6
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/comm_utils.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xad
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF7
	.byte	0x1
	.4byte	.LASF8
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x40
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x78
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xa9
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xd8
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x164
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x177
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x19b
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI5
	.4byte	.LFE3
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"CommAddressesAreEqual\000"
.LASF0:
	.ascii	"this_special_scan_addr_is_for_us\000"
.LASF3:
	.ascii	"COMM_MNGR_send_outgoing_3000_message_and_take_on_me"
	.ascii	"mory_release_responsiblity\000"
.LASF4:
	.ascii	"SendCommandResponseAndFree\000"
.LASF8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/comm_utils.c\000"
.LASF6:
	.ascii	"Roll_FIFO\000"
.LASF1:
	.ascii	"this_communication_address_is_our_serial_number\000"
.LASF7:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"COMM_UTIL_build_and_send_a_simple_flowsense_message"
	.ascii	"\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
