	.file	"e_stations.c"
	.text
.Ltext0:
	.section	.text.FDTO_STATION_draw_select_a_station_window,"ax",%progbits
	.align	2
	.type	FDTO_STATION_draw_select_a_station_window, %function
FDTO_STATION_draw_select_a_station_window:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L2
	str	lr, [sp, #-4]!
.LCFI0:
	ldr	r0, .L2+4
	ldrsh	r1, [r3, #0]
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L3:
	.align	2
.L2:
	.word	GuiLib_ActiveCursorFieldNo
	.word	433
.LFE18:
	.size	FDTO_STATION_draw_select_a_station_window, .-FDTO_STATION_draw_select_a_station_window
	.section	.text.STATION_get_menu_index_for_displayed_station,"ax",%progbits
	.align	2
	.type	STATION_get_menu_index_for_displayed_station, %function
STATION_get_menu_index_for_displayed_station:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L12
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L5
	ldr	r3, .L12+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	moveq	r4, #1
	beq	.L6
.L5:
	ldr	r3, .L12+8
	ldr	r2, .L12+12
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L12+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L12+4
	mov	r4, #0
	ldr	r1, [r3, #0]
	ldr	r3, .L12
	sub	r1, r1, #1
	ldr	r0, [r3, #0]
	bl	nm_STATION_get_pointer_to_station
	ldr	r3, .L12+20
	ldr	r2, [r3, #0]
	ldr	r3, .L12+24
	ldr	r3, [r3, #8]
	add	r2, r2, r3
	ldr	r3, .L12+28
	b	.L7
.L9:
	ldr	r1, [r3, #4]!
	cmp	r1, r0
	beq	.L8
	add	r4, r4, #1
.L7:
	cmp	r4, r2
	bne	.L9
	mov	r4, #0
.L8:
	ldr	r3, .L12+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L6:
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L13:
	.align	2
.L12:
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_StationInfoNumber
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	1149
	.word	.LANCHOR0
	.word	station_info_list_hdr
	.word	.LANCHOR1-4
.LFE19:
	.size	STATION_get_menu_index_for_displayed_station, .-STATION_get_menu_index_for_displayed_station
	.section	.text.FDTO_STATION_return_to_menu,"ax",%progbits
	.align	2
	.global	FDTO_STATION_return_to_menu
	.type	FDTO_STATION_return_to_menu, %function
FDTO_STATION_return_to_menu:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	bl	STATION_extract_and_store_changes_from_GuiVars
	ldr	r3, .L15
	mov	r4, #0
	str	r4, [r3, #0]
	bl	STATION_get_menu_index_for_displayed_station
	mov	r1, r0, asl #16
	mov	r1, r1, lsr #16
	mov	r0, r4
	bl	GuiLib_ScrollBox_To_Line
	ldr	r3, .L15+4
	mvn	r2, #0
	mov	r0, r4
	strh	r2, [r3, #0]	@ movhi
	ldmfd	sp!, {r4, lr}
	b	FDTO_Redraw_Screen
.L16:
	.align	2
.L15:
	.word	.LANCHOR2
	.word	GuiLib_ActiveCursorFieldNo
.LFE20:
	.size	FDTO_STATION_return_to_menu, .-FDTO_STATION_return_to_menu
	.section	.text.STATION_process_NEXT_and_PREV,"ax",%progbits
	.align	2
	.type	STATION_process_NEXT_and_PREV, %function
STATION_process_NEXT_and_PREV:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI3:
	mov	r5, r0
	bl	STATION_get_menu_index_for_displayed_station
	cmp	r5, #20
	cmpne	r5, #84
	movne	r5, #0
	moveq	r5, #1
	mov	r6, r0, asl #16
	mov	r4, r0
	mov	r6, r6, asr #16
	bne	.L18
	mov	r0, #0
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	mov	r1, r6
	bl	SCROLL_BOX_to_line
	cmp	r4, #780
	bcs	.L19
	ldr	r3, .L29
	add	r5, r4, #1
	ldr	r6, [r3, r5, asl #2]
	cmp	r6, #0
	beq	.L20
	mov	r0, #0
	mov	r1, r0
	bl	SCROLL_BOX_up_or_down
	b	.L21
.L20:
	add	r4, r4, #2
	ldr	r3, [r3, r4, asl #2]
	cmp	r3, #0
	beq	.L21
	bl	good_key_beep
	mov	r1, r4, asl #16
	mov	r0, r6
	mov	r1, r1, asr #16
	bl	SCROLL_BOX_to_line
	b	.L28
.L19:
	bl	bad_key_beep
.L28:
	mov	r5, r4
.L21:
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r4, r5
	b	.L22
.L18:
	mov	r0, r5
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, r5
	mov	r1, r6
	bl	SCROLL_BOX_to_line
	cmp	r4, #1
	bls	.L23
	ldr	r2, .L29
	sub	r4, r4, #1
	ldr	r6, [r2, r4, asl #2]
	cmp	r6, #0
	beq	.L24
	mov	r0, r5
	mov	r1, #4
	bl	SCROLL_BOX_up_or_down
	b	.L25
.L24:
	sub	r3, r4, #1
	ldr	r2, [r2, r3, asl #2]
	cmp	r2, #0
	beq	.L25
	mov	r4, r3
	bl	good_key_beep
	mov	r1, r4, asl #16
	mov	r0, r6
	mov	r1, r1, asr #16
	bl	SCROLL_BOX_to_line
	b	.L25
.L23:
	bl	bad_key_beep
.L25:
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
.L22:
	ldr	r5, .L29
	ldr	r3, [r5, r4, asl #2]
	cmp	r3, #0
	beq	.L26
	bl	STATION_extract_and_store_changes_from_GuiVars
	ldr	r0, [r5, r4, asl #2]
	bl	STATION_get_index_using_ptr_to_station_struct
	ldr	r6, .L29+4
	ldr	r3, .L29+8
	ldr	r7, .L29+12
	mov	r1, #400
	ldr	r2, .L29+16
	str	r0, [r3, #0]
	ldr	r0, [r6, #0]
	ldr	r3, .L29+20
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, #400
	ldr	r2, .L29+16
	mov	r3, #720
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, r4, asl #2]
	bl	nm_STATION_copy_station_into_guivars
	ldr	r0, [r7, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	b	.L27
.L26:
	bl	bad_key_beep
.L27:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	Redraw_Screen
.L30:
	.align	2
.L29:
	.word	.LANCHOR1
	.word	station_preserves_recursive_MUTEX
	.word	g_GROUP_list_item_index
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	718
.LFE14:
	.size	STATION_process_NEXT_and_PREV, .-STATION_process_NEXT_and_PREV
	.section	.text.FDTO_STATIONS_show_station_group_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATIONS_show_station_group_dropdown, %function
FDTO_STATIONS_show_station_group_dropdown:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L32
	stmfd	sp!, {r4, lr}
.LCFI4:
	ldr	r0, [r3, #0]
	bl	STATION_GROUP_get_index_for_group_with_this_GID
	mov	r4, r0
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r1, .L32+4
	mov	r3, r4
	mov	r2, r0
	ldr	r0, .L32+8
	ldmfd	sp!, {r4, lr}
	b	FDTO_COMBOBOX_show
.L33:
	.align	2
.L32:
	.word	.LANCHOR3
	.word	nm_STATION_GROUP_load_group_name_into_guivar
	.word	749
.LFE10:
	.size	FDTO_STATIONS_show_station_group_dropdown, .-FDTO_STATIONS_show_station_group_dropdown
	.section	.text.FDTO_STATION_draw_station.part.0,"ax",%progbits
	.align	2
	.type	FDTO_STATION_draw_station.part.0, %function
FDTO_STATION_draw_station.part.0:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L39
	stmfd	sp!, {r4, r5, lr}
.LCFI5:
	ldr	r2, .L39+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	mov	r3, #516
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L39+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L39+4
	ldr	r3, .L39+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L39+16
	ldr	r1, [r3, #0]
	ldr	r3, .L39+20
	sub	r1, r1, #1
	ldr	r0, [r3, #0]
	bl	nm_STATION_get_pointer_to_station
	subs	r5, r0, #0
	beq	.L35
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	movne	r3, #1
	bne	.L36
.L35:
	bl	STATION_get_first_available_station
	mov	r5, r0
	bl	STATION_station_is_available_for_use
	mov	r3, #0
.L36:
	eor	r3, r3, #1
	cmp	r4, #1
	movne	r4, r3
	orreq	r4, r3, #1
	cmp	r4, #0
	beq	.L37
	mov	r0, r5
	bl	nm_STATION_copy_station_into_guivars
.L37:
	ldr	r3, .L39+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L39
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L40:
	.align	2
.L39:
	.word	station_preserves_recursive_MUTEX
	.word	.LC0
	.word	list_program_data_recursive_MUTEX
	.word	518
	.word	GuiVar_StationInfoNumber
	.word	GuiVar_StationInfoBoxIndex
.LFE23:
	.size	FDTO_STATION_draw_station.part.0, .-FDTO_STATION_draw_station.part.0
	.section	.text.FDTO_STATION_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_STATION_draw_menu
	.type	FDTO_STATION_draw_menu, %function
FDTO_STATION_draw_menu:
.LFB21:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI6:
	ldr	r6, .L53
	mov	r5, r0
	bne	.L42
	ldr	r2, .L53+4
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L53+8
	str	r3, [r6, #0]
	str	r3, [r2, #0]
.LBB32:
	ldr	r3, .L53+12
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L52
	bl	FDTO_STATION_draw_station.part.0
	b	.L52
.L42:
.LBE32:
	ldr	r3, .L53+16
	mov	r0, #0
	ldrsh	r4, [r3, #0]
	bl	GuiLib_ScrollBox_GetTopLine
	str	r0, [r6, #0]
	b	.L43
.L52:
.LBB33:
	mvn	r4, #0
.L43:
.LBE33:
	mov	r1, r4
	mov	r2, #1
	mov	r0, #56
	bl	GuiLib_ShowScreen
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	bl	STATION_get_unique_box_index_count
	ldr	r4, .L53+20
	ldr	r3, .L53+24
	mov	r1, #400
	ldr	r2, .L53+28
.LBB34:
	mov	r7, #0
	mov	r8, r7
	mov	r6, r7
	mov	r9, r7
	ldr	fp, .L53+12
	ldr	sl, .L53+32
.LBE34:
	str	r0, [r4, #0]
	ldr	r0, [r3, #0]
	ldr	r3, .L53+36
	bl	xQueueTakeMutexRecursive_debug
.LBB35:
	mov	r1, #0
	mov	r2, #3120
	ldr	r0, .L53+32
	bl	memset
	ldr	r0, .L53+40
	mov	r1, #0
	mov	r2, #96
	bl	memset
	b	.L44
.L48:
	ldr	r0, .L53+12
	rsb	r1, r6, r7
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	subs	r3, r0, #0
	beq	.L45
	str	r3, [sp, #4]
	bl	STATION_station_is_available_for_use
	ldr	r3, [sp, #4]
	cmp	r0, #1
	bne	.L45
	cmp	r6, #0
	beq	.L46
	mov	r0, r3
	bl	nm_STATION_get_box_index_0
	ldr	r3, [sp, #4]
	cmp	r9, r0
	addeq	r2, r8, r6
	streq	r3, [sl, r2, asl #2]
	addeq	r8, r8, #1
	beq	.L45
.L46:
	add	r9, r8, r6
	mov	r2, #0
	mov	r0, r3
	str	r2, [sl, r9, asl #2]
	bl	nm_STATION_get_box_index_0
	ldr	r3, .L53+40
	add	r2, r3, r6, asl #3
	str	r9, [r3, r6, asl #3]
	add	r6, r6, #1
	mov	r9, r0
	str	r0, [r2, #4]
.L45:
	add	r7, r7, #1
.L44:
	ldr	r2, [r4, #0]
	ldr	r3, [fp, #8]
	add	r3, r2, r3
	cmp	r7, r3
	bcc	.L48
.LBE35:
	bl	STATION_get_menu_index_for_displayed_station
	ldr	r3, .L53+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	mov	r4, r0
	bne	.L49
	ldr	r3, .L53+20
	ldr	r1, .L53+44
	ldr	r2, [r3, #0]
	ldr	r3, .L53
	add	r8, r8, r2
	ldrsh	r3, [r3, #0]
	mov	r2, r8, asl #16
	str	r3, [sp, #0]
	mov	r2, r2, asr #16
	mvn	r3, #0
	mov	r0, #0
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	mov	r1, r4, asl #16
	mov	r1, r1, asr #16
	mov	r0, #0
	bl	GuiLib_ScrollBox_SetIndicator
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
	b	.L50
.L49:
	cmp	r0, #12
	movls	r3, #0
	movhi	r3, #1
	cmp	r5, #1
	movne	r3, #0
	cmp	r3, #0
	ldrne	r3, .L53
	subne	r2, r0, #11
	strne	r2, [r3, #0]
	ldr	r3, .L53+20
	ldr	r1, .L53
	ldr	r2, [r3, #0]
	ldrsh	r1, [r1, #0]
	add	r8, r8, r2
	mov	r3, r0, asl #16
	mov	r2, r8, asl #16
	str	r1, [sp, #0]
	mov	r0, #0
	ldr	r1, .L53+44
	mov	r2, r2, asr #16
	mov	r3, r3, asr #16
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L50:
	ldr	r3, .L53+24
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	GuiLib_Refresh
.L54:
	.align	2
.L53:
	.word	.LANCHOR4
	.word	.LANCHOR2
	.word	.LANCHOR5
	.word	station_info_list_hdr
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR1
	.word	1220
	.word	.LANCHOR6
	.word	nm_STATION_load_station_number_into_guivar
.LFE21:
	.size	FDTO_STATION_draw_menu, .-FDTO_STATION_draw_menu
	.section	.text.STATION_update_station_group,"ax",%progbits
	.align	2
	.type	STATION_update_station_group, %function
STATION_update_station_group:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L58
	stmfd	sp!, {r4, lr}
.LCFI7:
	ldr	r2, .L58+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #372
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	subs	r4, r0, #0
	bne	.L56
	ldr	r0, .L58+4
	ldr	r1, .L58+8
	bl	Alert_group_not_found_with_filename
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	mov	r4, r0
.L56:
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L58+12
	bl	strlcpy
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L58+16
	str	r0, [r3, #0]
	ldr	r3, .L58
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	STATION_extract_and_store_changes_from_GuiVars
.LBB36:
	ldr	r3, .L58+20
	ldr	r3, [r3, #8]
	cmp	r3, #0
	ldmeqfd	sp!, {r4, pc}
	mov	r0, #1
.LBE36:
	ldmfd	sp!, {r4, lr}
.LBB37:
	b	FDTO_STATION_draw_station.part.0
.L59:
	.align	2
.L58:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	378
	.word	GuiVar_StationInfoStationGroup
	.word	.LANCHOR3
	.word	station_info_list_hdr
.LBE37:
.LFE8:
	.size	STATION_update_station_group, .-STATION_update_station_group
	.section	.text.FDTO_STATIONS_close_station_group_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATIONS_close_station_group_dropdown, %function
FDTO_STATIONS_close_station_group_dropdown:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, #0
	str	lr, [sp, #-4]!
.LCFI8:
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	bl	STATION_update_station_group
	ldr	lr, [sp], #4
	b	FDTO_COMBOBOX_hide
.LFE11:
	.size	FDTO_STATIONS_close_station_group_dropdown, .-FDTO_STATIONS_close_station_group_dropdown
	.section	.text.nm_STATION_load_station_number_into_guivar,"ax",%progbits
	.align	2
	.global	nm_STATION_load_station_number_into_guivar
	.type	nm_STATION_load_station_number_into_guivar, %function
nm_STATION_load_station_number_into_guivar:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L67
	mov	r0, r0, asl #16
	ldr	r2, [r3, #0]
	ldr	r3, .L67+4
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI9:
	ldr	r3, [r3, #8]
	mov	r5, r0, asr #16
	add	r3, r2, r3
	cmp	r5, r3
	ldmcsfd	sp!, {r4, r5, r6, pc}
	ldr	r6, .L67+8
	ldr	r3, .L67+12
	ldr	r0, [r6, r5, asl #2]
	cmp	r0, #0
	bne	.L63
.LBB40:
	str	r0, [r3, #0]
	ldr	r3, .L67+16
.L65:
	ldr	r2, [r3, r0, asl #3]
	cmp	r2, r5
	beq	.L64
	add	r0, r0, #1
	cmp	r0, #12
	bne	.L65
.L64:
	ldr	r3, .L67+16
	ldr	r1, .L67+20
	add	r0, r3, r0, asl #3
	ldr	r0, [r0, #4]
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	ldr	r0, .L67+24
	ldr	r1, .L67+28
	mov	r2, #4
.LBE40:
	ldmfd	sp!, {r4, r5, r6, lr}
.LBB41:
	b	strlcpy
.L63:
.LBE41:
	mov	r2, #10
	str	r2, [r3, #0]
	bl	nm_STATION_get_station_number_0
	mov	r4, r0
	ldr	r0, [r6, r5, asl #2]
	bl	nm_STATION_get_box_index_0
	mov	r1, #0
	mov	r5, r0
	ldr	r0, .L67+32
	bl	GuiLib_GetTextPtr
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L67+20
	bl	strlcpy
	ldr	r2, .L67+24
	mov	r0, r5
	mov	r1, r4
	mov	r3, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	STATION_get_station_number_string
.L68:
	.align	2
.L67:
	.word	.LANCHOR0
	.word	station_info_list_hdr
	.word	.LANCHOR1
	.word	GuiVar_itmSubNode
	.word	.LANCHOR6
	.word	GuiVar_itmStationName
	.word	GuiVar_itmStationNumber
	.word	.LC1
	.word	1061
.LFE17:
	.size	nm_STATION_load_station_number_into_guivar, .-nm_STATION_load_station_number_into_guivar
	.section	.text.STATION_update_cycle_too_short_warning,"ax",%progbits
	.align	2
	.global	STATION_update_cycle_too_short_warning
	.type	STATION_update_cycle_too_short_warning, %function
STATION_update_cycle_too_short_warning:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L75+4
	ldr	r3, .L75+8
	ldr	r2, [r2, #0]
	ldr	r1, [r3, #0]
	cmp	r2, #0
	mov	r0, r3
	streq	r2, [r3, #0]
	beq	.L72
	ldr	r3, .L75+12
	flds	s15, .L75
	flds	s14, [r3, #0]
	fcmpes	s14, s15
	fmstat
	movmi	r3, #1
	bmi	.L71
	ldr	r3, .L75+16
	flds	s14, [r3, #0]
	fcmpes	s14, s15
	fmstat
	movpl	r3, #0
	movmi	r3, #1
.L71:
	str	r3, [r0, #0]
.L72:
	ldr	r3, .L75+8
	ldr	r3, [r3, #0]
	cmp	r1, r3
	bxeq	lr
	mov	r0, #0
	b	Redraw_Screen
.L76:
	.align	2
.L75:
	.word	1078355558
	.word	GuiVar_StationInfoShowEstMin
	.word	GuiVar_StationInfoCycleTooShort
	.word	GuiVar_StationInfoEstMin
	.word	GuiVar_StationInfoCycleTime
.LFE1:
	.size	STATION_update_cycle_too_short_warning, .-STATION_update_cycle_too_short_warning
	.section	.text.STATION_update_estimated_minutes,"ax",%progbits
	.align	2
	.type	STATION_update_estimated_minutes, %function
STATION_update_estimated_minutes:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI10:
	ldr	r4, .L78
	ldr	r2, .L78+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	mov	r3, #173
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L78+8
	ldr	r1, [r3, #0]
	ldr	r3, .L78+12
	sub	r1, r1, #1
	ldr	r0, [r3, #0]
	bl	nm_STATION_get_pointer_to_station
	bl	nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar
	ldr	r3, .L78+16
	str	r0, [r3, #0]	@ float
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r4, lr}
	b	STATION_update_cycle_too_short_warning
.L79:
	.align	2
.L78:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_StationInfoNumber
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_StationInfoEstMin
.LFE2:
	.size	STATION_update_estimated_minutes, .-STATION_update_estimated_minutes
	.section	.text.FDTO_STATION_update_info_i_and_flow_status,"ax",%progbits
	.align	2
	.global	FDTO_STATION_update_info_i_and_flow_status
	.type	FDTO_STATION_update_info_i_and_flow_status, %function
FDTO_STATION_update_info_i_and_flow_status:
.LFB13:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L81
	stmfd	sp!, {r0, lr}
.LCFI11:
	ldr	r1, [r3, #0]
	ldr	r3, .L81+4
	mov	r2, sp
	ldr	r0, [r3, #0]
	sub	r1, r1, #1
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	ldr	r2, [sp, #0]
	ldr	r3, .L81+8
	add	r3, r3, r2, asl #7
	ldrh	r1, [r3, #140]
	ldr	r2, .L81+12
	mov	r1, r1, lsr #6
	and	r1, r1, #15
	str	r1, [r2, #0]
	ldrb	r2, [r3, #140]	@ zero_extendqisi2
	ldr	r3, .L81+16
	mov	r2, r2, lsr #4
	and	r2, r2, #3
	str	r2, [r3, #0]
	bl	GuiLib_Refresh
	ldmfd	sp!, {r3, pc}
.L82:
	.align	2
.L81:
	.word	GuiVar_StationInfoNumber
	.word	GuiVar_StationInfoBoxIndex
	.word	station_preserves
	.word	GuiVar_StationInfoIStatus
	.word	GuiVar_StationInfoFlowStatus
.LFE13:
	.size	FDTO_STATION_update_info_i_and_flow_status, .-FDTO_STATION_update_info_i_and_flow_status
	.section	.text.STATION_process_station,"ax",%progbits
	.align	2
	.global	STATION_process_station
	.type	STATION_process_station, %function
STATION_process_station:
.LFB15:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L182+4
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI12:
	fstmfdd	sp!, {d8}
.LCFI13:
	ldrsh	r3, [r3, #0]
	ldr	r2, .L182+8
	sub	sp, sp, #52
.LCFI14:
	cmp	r3, r2
	mov	r4, r0
	mov	r5, r1
	beq	.L85
	add	r2, r2, #140
	cmp	r3, r2
	bne	.L173
	b	.L181
.L85:
	ldr	r2, .L182+12
	bl	KEYBOARD_process_key
	b	.L83
.L181:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L88
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #12]
	ldr	r3, .L182+16
	b	.L177
.L88:
	bl	COMBO_BOX_key_press
	b	.L83
.L173:
	cmp	r0, #4
	beq	.L94
	bhi	.L98
	cmp	r0, #1
	beq	.L91
	bcc	.L90
	cmp	r0, #2
	beq	.L92
	cmp	r0, #3
	bne	.L89
	b	.L93
.L98:
	cmp	r0, #67
	beq	.L96
	bhi	.L99
	cmp	r0, #16
	beq	.L95
	cmp	r0, #20
	bne	.L89
	b	.L95
.L99:
	cmp	r0, #80
	beq	.L97
	cmp	r0, #84
	bne	.L89
	b	.L97
.L95:
	mov	r0, r4
	bl	STATION_process_NEXT_and_PREV
	b	.L83
.L92:
	ldr	r3, .L182+136
	ldrsh	r3, [r3, #0]
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L151
.L105:
	.word	.L101
	.word	.L102
	.word	.L151
	.word	.L151
	.word	.L151
	.word	.L151
	.word	.L151
	.word	.L151
	.word	.L151
	.word	.L103
	.word	.L151
	.word	.L104
.L101:
	bl	good_key_beep
	mov	r0, #161
	mov	r1, #27
	mov	r2, #48
	mov	r3, #0
	bl	KEYBOARD_draw_keyboard
	b	.L83
.L102:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #12]
	ldr	r3, .L182+20
.L177:
	add	r0, sp, #12
	str	r3, [sp, #32]
	bl	Display_Post_Command
	b	.L83
.L103:
	bl	good_key_beep
	ldr	r2, .L182+24
	ldr	r3, .L182+28
	str	r2, [r3, #0]	@ float
	ldr	r3, .L182+32
.LBB60:
	mov	r2, #0
.LBE60:
	add	r1, r3, #270336
.L106:
.LBB61:
	strh	r2, [r3, #136]	@ movhi
	add	r3, r3, #128
	cmp	r3, r1
	bne	.L106
	b	.L123
.L104:
.LBE61:
	bl	good_key_beep
	ldr	r3, .L182+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L107
	ldr	r8, .L182+116
	flds	s16, .L182
	ldr	r3, .L182+120
	ldr	r1, [r8, #0]
	ldr	r0, [r3, #0]
	sub	r1, r1, #1
	ldr	r2, .L182+108
	mov	r3, #14
	bl	Alert_station_copied_idx
	bl	FLOWSENSE_get_controller_index
	ldr	sl, .L182+100
	mov	r5, r0
	ldr	r0, .L182+40
	bl	nm_ListGetFirst
	mov	r4, r0
	b	.L108
.L110:
	mov	r0, r4
	bl	nm_STATION_get_station_number_0
	ldr	r3, [r8, #0]
	sub	r3, r3, #1
	cmp	r0, r3
	beq	.L109
	mov	r0, r4
	bl	STATION_get_GID_station_group
	ldr	r3, [sl, #0]
	cmp	r0, r3
	bne	.L109
	mov	r1, #2
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	ldr	r3, .L182+44
	mov	r7, #1
	flds	s15, [r3, #0]
	mov	r2, #0
	mov	r3, #14
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	str	r7, [sp, #4]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	mov	r6, r0
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_total_run_minutes
	ldr	r3, .L182+48
	mov	r0, r4
	flds	s15, [r3, #0]
	mov	r2, #0
	mov	r3, #14
	stmia	sp, {r5, r7}
	fmuls	s15, s15, s16
	str	r6, [sp, #8]
	ftouizs	s15, s15
	fmrs	r1, s15	@ int
	bl	nm_STATION_set_cycle_minutes
	ldr	r3, .L182+80
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	mov	r0, r4
	ldr	r1, [r3, #0]
	mov	r2, #0
	mov	r3, #14
	bl	nm_STATION_set_soak_minutes
.L109:
	mov	r1, r4
	ldr	r0, .L182+40
	bl	nm_ListGetNext
	mov	r4, r0
.L108:
	cmp	r4, #0
	bne	.L110
	b	.L178
.L107:
	bl	bad_key_beep
.L178:
	mov	r0, #624
	bl	DIALOG_draw_ok_dialog
	b	.L83
.L97:
	ldr	r3, .L182+136
	ldrsh	r3, [r3, #0]
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L111
.L122:
	.word	.L112
	.word	.L113
	.word	.L114
	.word	.L115
	.word	.L116
	.word	.L117
	.word	.L118
	.word	.L119
	.word	.L120
	.word	.L121
.L112:
	mov	r0, r4
	bl	STATION_process_NEXT_and_PREV
	b	.L123
.L114:
.LBB62:
.LBB63:
	cmp	r5, #64
	movhi	r5, #4
	bhi	.L124
	cmp	r5, #32
	movls	r5, #1
	movhi	r5, #2
.L124:
.LBE63:
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L182+52
	mvn	r2, #99
	mov	r3, #300
	str	r5, [sp, #0]
	bl	process_int32
	bl	STATION_update_estimated_minutes
	b	.L123
.L115:
.LBE62:
.LBB64:
	ldr	r3, .L182+44
	flds	s16, .L182
.LBB65:
	cmp	r5, #64
.LBE65:
	flds	s15, [r3, #0]
.LBB66:
	movhi	r5, #4
.LBE66:
	fmuls	s15, s15, s16
	ftouizs	s15, s15
	fsts	s15, [sp, #48]	@ int
.LBB67:
	bhi	.L125
	cmp	r5, #32
	movls	r5, #1
	movhi	r5, #2
.L125:
.LBE67:
	mov	r2, #0
	mov	r0, r4
	add	r1, sp, #48
	ldr	r3, .L182+56
	str	r2, [sp, #4]
	str	r5, [sp, #0]
	bl	process_uns32
	flds	s14, [sp, #48]	@ int
	ldr	r3, .L182+44
	fuitos	s15, s14
	fdivs	s16, s15, s16
	fsts	s16, [r3, #0]
	ldr	r3, .L182+116
	ldr	r1, [r3, #0]
	ldr	r3, .L182+120
	sub	r1, r1, #1
	ldr	r0, [r3, #0]
	bl	nm_STATION_get_pointer_to_station
	bl	nm_STATION_calculate_and_copy_estimated_minutes_into_GuiVar
	ldr	r3, .L182+60
	str	r0, [r3, #0]	@ float
	bl	Refresh_Screen
	b	.L123
.L116:
.LBE64:
.LBB68:
	ldr	r3, .L182+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldreq	r7, .L182+64
	beq	.L126
	ldr	r6, .L182+128
	ldr	r2, .L182+96
	mov	r1, #400
	mov	r3, #284
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L182+116
	ldr	r1, [r3, #0]
	ldr	r3, .L182+120
	sub	r1, r1, #1
	ldr	r0, [r3, #0]
	bl	nm_STATION_get_pointer_to_station
	bl	nm_STATION_get_watersense_cycle_max_10u
	mov	r7, r0
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
.L126:
	ldr	r3, .L182+48
	flds	s16, .L182
.LBB69:
	cmp	r5, #64
.LBE69:
	flds	s15, [r3, #0]
.LBB70:
	movhi	r5, #4
.LBE70:
	fmuls	s15, s15, s16
	ftouizs	s15, s15
	fsts	s15, [sp, #48]	@ int
.LBB71:
	bhi	.L127
	cmp	r5, #32
	movls	r5, #1
	movhi	r5, #2
.L127:
.LBE71:
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r4
	mov	r3, r7
	add	r1, sp, #48
	mov	r2, #1
	str	r5, [sp, #0]
	bl	process_uns32
	flds	s14, [sp, #48]	@ int
	ldr	r3, .L182+48
	fuitos	s15, s14
	fdivs	s16, s15, s16
	fsts	s16, [r3, #0]
	bl	STATION_update_cycle_too_short_warning
	b	.L123
.L117:
.LBE68:
.LBB72:
	ldr	r3, .L182+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	moveq	r7, #5
	beq	.L128
	ldr	r6, .L182+128
	ldr	r2, .L182+96
	mov	r1, #400
	ldr	r3, .L182+68
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L182+116
	ldr	r1, [r3, #0]
	ldr	r3, .L182+120
	sub	r1, r1, #1
	ldr	r0, [r3, #0]
	bl	nm_STATION_get_pointer_to_station
	bl	nm_STATION_get_watersense_soak_min
	mov	r7, r0
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
.L128:
.LBB73:
	cmp	r5, #64
	movhi	r5, #4
	bhi	.L129
	cmp	r5, #32
	movls	r5, #1
	movhi	r5, #2
.L129:
.LBE73:
	mov	r3, #0
	str	r3, [sp, #4]
	str	r5, [sp, #0]
	mov	r0, r4
	ldr	r1, .L182+80
	mov	r2, r7
	mov	r3, #720
	b	.L176
.L119:
.LBE72:
.LBB74:
	cmp	r5, #64
	movhi	r5, #4
	bhi	.L130
	cmp	r5, #32
	movls	r5, #1
	movhi	r5, #2
.L130:
.LBE74:
	mov	r3, #0
	str	r3, [sp, #4]
	str	r5, [sp, #0]
	mov	r0, r4
	ldr	r1, .L182+72
	mov	r2, #1
	mov	r3, #4000
	b	.L176
.L183:
	.align	2
.L182:
	.word	1092616192
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	FDTO_STATION_draw_menu
	.word	FDTO_STATIONS_close_station_group_dropdown
	.word	FDTO_STATIONS_show_station_group_dropdown
	.word	1112014848
	.word	GuiVar_StationInfoMoistureBalancePercent
	.word	station_preserves
	.word	GuiVar_StationInfoShowPercentOfET
	.word	station_info_list_hdr
	.word	GuiVar_StationInfoTotalMinutes
	.word	GuiVar_StationInfoCycleTime
	.word	GuiVar_StationInfoETFactor
	.word	9999
	.word	GuiVar_StationInfoEstMin
	.word	24000
	.word	327
	.word	GuiVar_StationInfoExpectedFlow
	.word	GuiVar_StationInfoDU
	.word	GuiVar_StationInfoSoakInTime
	.word	GuiVar_StationInfoSquareFootage
	.word	99999
	.word	GuiVar_StationInfoNoWaterDays
	.word	.LC0
	.word	.LANCHOR3
	.word	1001
	.word	GuiVar_StationInfoStationGroup
	.word	GuiVar_StationInfoGroupHasStartTime
	.word	GuiVar_StationInfoNumber
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_StationInfoShowCopyStation
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR5
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_STATION_return_to_menu
.L121:
	ldr	r3, .L182+76
.LBB75:
	cmp	r5, #64
.LBE75:
	ldr	r6, [r3, #0]
.LBB76:
	movhi	r5, #4
	bhi	.L131
	cmp	r5, #32
	movls	r5, #1
	movhi	r5, #2
.L131:
.LBE76:
	ldr	r7, .L182+76
	mov	r3, #0
	mov	r0, r4
	mov	r1, r7
	mov	r2, #40
	str	r3, [sp, #4]
	mov	r3, #100
	str	r5, [sp, #0]
	bl	process_uns32
	bl	STATION_update_estimated_minutes
.LBB77:
	ldr	r3, .L182+128
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L182+96
	mov	r3, #193
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L182+116
	ldr	r4, .L182+80
	ldr	r1, [r3, #0]
	ldr	r3, .L182+120
	sub	r1, r1, #1
	ldr	r0, [r3, #0]
	bl	nm_STATION_get_pointer_to_station
	subs	r3, r0, #0
	streq	r3, [r4, #0]
	beq	.L134
	bl	STATION_get_GID_station_group
	mov	sl, r0
	bl	STATION_GROUP_get_group_with_this_GID
	subs	r5, r0, #0
	streq	r5, [r4, #0]
	beq	.L134
	mov	r0, sl
	mov	r1, r6
	ldr	r8, [r4, #0]
	bl	STATION_GROUP_get_soak_time_for_this_gid
	cmp	r8, r0
	bne	.L134
	mov	r0, r5
	bl	STATION_GROUP_get_allowable_surface_accumulation
	mov	r8, r0	@ float
	mov	r0, r5
	bl	STATION_GROUP_get_soil_intake_rate
	mov	r6, r0	@ float
	mov	r0, r5
	bl	STATION_GROUP_get_precip_rate_in_per_hr
	ldr	r3, [r7, #0]
	mov	r1, r6	@ float
	fmsr	s15, r3	@ int
	fuitos	s15, s15
	fmrs	r3, s15
	mov	r2, r0	@ float
	mov	r0, r8	@ float
	bl	WATERSENSE_get_soak_time__cycle_end_to_cycle_start
	fmsr	s14, r0
	ftouizs	s14, s14
	fsts	s14, [r4, #0]	@ int
.L134:
	ldr	r3, .L182+128
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L123
.L120:
.LBE77:
.LBB78:
	cmp	r5, #64
	movhi	r5, #4
	bhi	.L135
	cmp	r5, #32
	movls	r5, #1
	movhi	r5, #2
.L135:
.LBE78:
	mov	r3, #0
	str	r3, [sp, #4]
	str	r5, [sp, #0]
	mov	r0, r4
	ldr	r1, .L182+84
	mov	r2, #1
	ldr	r3, .L182+88
	b	.L176
.L118:
	ldr	r1, .L182+92
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r2, #0
	mov	r0, r4
	mov	r3, #31
	str	r2, [sp, #4]
.L176:
	bl	process_uns32
	b	.L123
.L113:
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r0, #1
	bhi	.L136
	ldr	r3, .L182+100
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L111
.L136:
.LBB79:
	ldr	r3, .L182+128
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L182+96
	mov	r3, #428
	bl	xQueueTakeMutexRecursive_debug
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r3, .L182+100
	sub	r5, r0, #1
	ldr	r0, [r3, #0]
	cmp	r0, #0
	bne	.L138
	bl	good_key_beep
	cmp	r4, #84
	moveq	r5, #0
	str	r5, [sp, #48]
	mov	r0, r5
	b	.L175
.L138:
	bl	STATION_GROUP_get_index_for_group_with_this_GID
	cmp	r0, r5
	cmpeq	r4, #84
	str	r0, [sp, #48]
	beq	.L141
	cmp	r0, #0
	cmpeq	r4, #80
	movne	r2, #0
	moveq	r2, #1
	bne	.L142
.L141:
	bl	good_key_beep
	ldr	r3, .L182+100
	mov	r4, #0
	str	r4, [r3, #0]
	mov	r1, r4
	ldr	r0, .L182+104
	bl	GuiLib_GetTextPtr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L182+108
	bl	strlcpy
	ldr	r3, .L182+112
	str	r4, [r3, #0]
	b	.L140
.L142:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	add	r1, sp, #48
	mov	r3, r5
	bl	process_uns32
	ldr	r0, [sp, #48]
.L175:
	bl	STATION_update_station_group
.L140:
	ldr	r3, .L182+116
	ldr	r1, [r3, #0]
	ldr	r3, .L182+120
	sub	r1, r1, #1
	ldr	r0, [r3, #0]
	bl	nm_STATION_get_pointer_to_station
	bl	WEATHER_get_station_uses_daily_et
	ldr	r3, .L182+124
	rsbs	r0, r0, #1
	movcc	r0, #0
	str	r0, [r3, #0]
	ldr	r3, .L182+128
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #0
	bl	Redraw_Screen
	b	.L123
.L111:
.LBE79:
	bl	bad_key_beep
.L123:
	bl	Refresh_Screen
	b	.L83
.L94:
	ldr	r3, .L182+136
	ldrsh	r3, [r3, #0]
	sub	r2, r3, #2
	cmp	r2, #5
	ldrls	pc, [pc, r2, asl #2]
	b	.L143
.L146:
	.word	.L144
	.word	.L144
	.word	.L143
	.word	.L143
	.word	.L144
	.word	.L145
.L144:
	ldr	r2, .L182+132
	mov	r0, #1
	str	r3, [r2, #0]
	mov	r1, r0
	b	.L179
.L145:
	mov	r0, #5
	b	.L180
.L143:
	mov	r0, #1
	b	.L174
.L90:
	ldr	r3, .L182+136
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L93
.L152:
	.word	.L148
	.word	.L149
	.word	.L93
	.word	.L93
	.word	.L150
	.word	.L151
.L148:
	ldr	r3, .L182+132
	ldr	r0, [r3, #0]
	cmp	r0, #6
	bne	.L93
	b	.L180
.L149:
	mov	r0, #4
	b	.L180
.L150:
	mov	r0, #7
.L180:
	mov	r1, #1
.L179:
	bl	CURSOR_Select
	b	.L83
.L151:
	bl	bad_key_beep
	b	.L83
.L91:
	ldr	r3, .L182+136
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L96
.L174:
	bl	CURSOR_Up
	b	.L83
.L93:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L83
.L96:
	ldr	r0, .L182+140
	bl	KEY_process_BACK_from_editing_screen
	b	.L83
.L89:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L83:
	add	sp, sp, #52
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.LFE15:
	.size	STATION_process_station, .-STATION_process_station
	.section	.text.STATION_process_menu,"ax",%progbits
	.align	2
	.global	STATION_process_menu
	.type	STATION_process_menu, %function
STATION_process_menu:
.LFB22:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI15:
	ldr	r5, .L196
	sub	sp, sp, #36
.LCFI16:
	ldr	lr, [r5, #0]
	mov	r3, r0
	cmp	lr, #1
	mov	ip, r1
	mov	r2, r0
	bne	.L185
	bl	STATION_process_station
	b	.L184
.L185:
	cmp	r0, #20
	bhi	.L187
	mov	r6, #1
	mov	r1, r6, asl r0
	tst	r1, #1114112
	bne	.L190
	ands	r4, r1, #17
	bne	.L188
	tst	r1, #12
	beq	.L187
	mov	r0, r4
	mov	r1, r4
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r7, .L196+4
	ldr	r3, [r7, r0, asl #2]
	cmp	r3, #0
	beq	.L191
	bl	good_key_beep
	mov	r1, r4
	mov	r0, r4
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r0, [r7, r0, asl #2]
	bl	STATION_get_index_using_ptr_to_station_struct
	ldr	r3, .L196+8
	str	r6, [r5, #0]
	str	r0, [r3, #0]
	ldr	r3, .L196+12
	mov	r0, r4
	strh	r4, [r3, #0]	@ movhi
	b	.L195
.L191:
	bl	bad_key_beep
	b	.L184
.L190:
	cmp	r0, #20
	movne	r2, #4
	moveq	r2, #0
.L188:
	mov	r1, r2
	mov	r0, #0
	bl	SCROLL_BOX_up_or_down
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r4, .L196+4
	ldr	r5, [r4, r0, asl #2]
	cmp	r5, #0
	bne	.L192
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L196+16
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	mov	r0, r5
	bl	SCROLL_BOX_redraw
	b	.L184
.L192:
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r5, .L196+20
	ldr	r6, .L196+24
	ldr	r0, [r4, r0, asl #2]
	bl	STATION_get_index_using_ptr_to_station_struct
	ldr	r3, .L196+8
	mov	r1, #400
	ldr	r2, .L196+28
	str	r0, [r3, #0]
	ldr	r3, .L196+32
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, .L196+28
	ldr	r3, .L196+36
	mov	r1, #400
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r0, [r4, r0, asl #2]
	bl	nm_STATION_copy_station_into_guivars
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #0
.L195:
	bl	Redraw_Screen
	b	.L184
.L187:
	cmp	r3, #67
	ldreq	r2, .L196+40
	moveq	r1, #1
	streq	r1, [r2, #0]
	mov	r0, r3
	mov	r1, ip
	bl	KEY_process_global_keys
.L184:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L197:
	.align	2
.L196:
	.word	.LANCHOR2
	.word	.LANCHOR1
	.word	g_GROUP_list_item_index
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_STATION_draw_select_a_station_window
	.word	station_preserves_recursive_MUTEX
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	1330
	.word	1332
	.word	GuiVar_MenuScreenToShow
.LFE22:
	.size	STATION_process_menu, .-STATION_process_menu
	.global	g_STATION_group_ID
	.section	.bss.g_STATION_previous_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	g_STATION_previous_cursor_pos, %object
	.size	g_STATION_previous_cursor_pos, 4
g_STATION_previous_cursor_pos:
	.space	4
	.section	.bss.g_STATION_group_ID,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_STATION_group_ID, %object
	.size	g_STATION_group_ID, 4
g_STATION_group_ID:
	.space	4
	.section	.bss.STATION_MENU_items,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	STATION_MENU_items, %object
	.size	STATION_MENU_items, 3120
STATION_MENU_items:
	.space	3120
	.section	.bss.STATION_MENU_box_indexes,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	STATION_MENU_box_indexes, %object
	.size	STATION_MENU_box_indexes, 96
STATION_MENU_box_indexes:
	.space	96
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_stations.c\000"
.LC1:
	.ascii	"\000"
	.section	.bss.g_STATION_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_STATION_editing_group, %object
	.size	g_STATION_editing_group, 4
g_STATION_editing_group:
	.space	4
	.section	.bss.g_STATION_top_line,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_STATION_top_line, %object
	.size	g_STATION_top_line, 4
g_STATION_top_line:
	.space	4
	.section	.bss.g_STATION_controllers_to_show_in_menu,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_STATION_controllers_to_show_in_menu, %object
	.size	g_STATION_controllers_to_show_in_menu, 4
g_STATION_controllers_to_show_in_menu:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI0-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI1-.LFB19
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI2-.LFB20
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI3-.LFB14
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI4-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI5-.LFB23
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI6-.LFB21
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI7-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI8-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI9-.LFB17
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI10-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI11-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI12-.LFB15
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x24
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI15-.LFB22
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_stations.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x19f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF22
	.byte	0x1
	.4byte	.LASF23
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1f3
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x43a
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF1
	.byte	0x1
	.byte	0x7e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x166
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.byte	0xf0
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x466
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x46d
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x492
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST2
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x26b
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST3
	.uleb128 0x5
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1e1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST4
	.uleb128 0x7
	.4byte	0x21
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST5
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x407
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x4a0
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST6
	.uleb128 0x5
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x170
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST7
	.uleb128 0x5
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x1eb
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST8
	.uleb128 0x7
	.4byte	0x2a
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST9
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.byte	0x93
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.4byte	.LASF14
	.byte	0x1
	.byte	0xab
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST10
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x24a
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST11
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x10e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x13b
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF18
	.byte	0x1
	.byte	0xb7
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x1a6
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x2f3
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST12
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x4ea
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST13
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB18
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB19
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB20
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB14
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB10
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB23
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB21
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB8
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB11
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB17
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB2
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB13
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB15
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI14
	.4byte	.LFE15
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB22
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI16
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF18:
	.ascii	"STATION_update_soak_time\000"
.LASF22:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF20:
	.ascii	"STATION_process_station\000"
.LASF1:
	.ascii	"STATION_get_inc_by\000"
.LASF19:
	.ascii	"STATION_process_station_group\000"
.LASF5:
	.ascii	"STATION_get_menu_index_for_displayed_station\000"
.LASF15:
	.ascii	"FDTO_STATION_update_info_i_and_flow_status\000"
.LASF4:
	.ascii	"FDTO_STATION_draw_select_a_station_window\000"
.LASF9:
	.ascii	"FDTO_STATION_return_to_menu\000"
.LASF3:
	.ascii	"STATION_process_total_min_10u\000"
.LASF23:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_stations.c\000"
.LASF12:
	.ascii	"FDTO_STATIONS_close_station_group_dropdown\000"
.LASF11:
	.ascii	"STATION_update_station_group\000"
.LASF6:
	.ascii	"STATION_process_NEXT_and_PREV\000"
.LASF2:
	.ascii	"STATION_process_ET_factor\000"
.LASF21:
	.ascii	"STATION_process_menu\000"
.LASF17:
	.ascii	"STATION_process_soak_min\000"
.LASF8:
	.ascii	"nm_STATION_populate_pointers_for_menu\000"
.LASF0:
	.ascii	"FDTO_STATION_draw_station\000"
.LASF7:
	.ascii	"FDTO_STATIONS_show_station_group_dropdown\000"
.LASF13:
	.ascii	"STATION_update_cycle_too_short_warning\000"
.LASF10:
	.ascii	"FDTO_STATION_draw_menu\000"
.LASF24:
	.ascii	"nm_STATION_load_station_number_into_guivar\000"
.LASF16:
	.ascii	"STATION_process_cycle_min_10u\000"
.LASF14:
	.ascii	"STATION_update_estimated_minutes\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
