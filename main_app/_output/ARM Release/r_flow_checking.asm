	.file	"r_flow_checking.c"
	.text
.Ltext0:
	.section	.text.FDTO_FLOW_CHECKING_STATS_redraw_report,"ax",%progbits
	.align	2
	.type	FDTO_FLOW_CHECKING_STATS_redraw_report, %function
FDTO_FLOW_CHECKING_STATS_redraw_report:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldr	r5, .L2
	ldr	r4, .L2+4
	ldr	r0, [r5, #0]
	bl	FDTO_SYSTEM_load_system_name_into_guivar
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L2+8
	mov	r3, #49
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #0]
	ldr	r1, .L2+12
	ldr	r2, .L2+16
	mla	r3, r1, r3, r2
	ldr	r2, .L2+20
	ldrb	r1, [r3, #482]	@ zero_extendqisi2
	mov	r1, r1, lsr #2
	and	r1, r1, #1
	str	r1, [r2, #0]
	ldrb	r2, [r3, #481]	@ zero_extendqisi2
	ldr	r1, .L2+24
	mov	r0, r2, lsr #7
	str	r0, [r1, #0]
	add	r1, r3, #484
	ldrh	r0, [r1, #0]
	ldr	r1, .L2+28
	mov	r0, r0, lsr #7
	and	r0, r0, #63
	str	r0, [r1, #0]
	ldrb	r1, [r3, #480]	@ zero_extendqisi2
	ldr	r0, .L2+32
	mov	ip, r1, lsr #4
	and	ip, ip, #1
	str	ip, [r0, #0]
	ldr	r0, .L2+36
	mov	ip, r1, lsr #7
	str	ip, [r0, #0]
	ldr	r0, .L2+40
	and	ip, r2, #1
	str	ip, [r0, #0]
	ldr	r0, .L2+44
	mov	ip, r2, lsr #1
	and	ip, ip, #1
	str	ip, [r0, #0]
	ldr	r0, .L2+48
	mov	ip, r2, lsr #2
	and	ip, ip, #1
	str	ip, [r0, #0]
	ldr	r0, .L2+52
	mov	ip, r2, lsr #3
	and	ip, ip, #1
	str	ip, [r0, #0]
	ldr	r0, .L2+56
	mov	ip, r2, lsr #4
	and	ip, ip, #1
	str	ip, [r0, #0]
	ldr	r0, .L2+60
	mov	ip, r2, lsr #5
	and	ip, ip, #1
	str	ip, [r0, #0]
	ldr	r0, .L2+64
	mov	r2, r2, lsr #6
	and	r2, r2, #1
	str	r2, [r0, #0]
	ldr	r0, [r3, #224]
	ldr	r2, .L2+68
	tst	r1, #32
	str	r0, [r2, #0]
	ldr	r0, [r3, #228]
	ldr	r2, .L2+72
	mov	r1, r1, lsr #6
	str	r0, [r2, #0]
	ldr	r0, [r3, #472]
	ldr	r2, .L2+76
	and	r1, r1, #1
	str	r0, [r2, #0]
	ldr	r0, [r3, #476]
	ldr	r2, .L2+80
	str	r0, [r2, #0]
	ldr	r0, [r3, #464]
	ldr	r2, .L2+84
	str	r0, [r2, #0]
	ldr	r2, .L2+88
	movne	r0, #0
	moveq	r0, #1
	str	r0, [r2, #0]
	ldr	r0, [r3, #468]
	ldr	r2, .L2+92
	str	r0, [r2, #0]
	ldr	r2, .L2+96
	str	r1, [r2, #0]
	ldr	r1, [r3, #108]
	ldr	r2, .L2+100
	str	r1, [r2, #0]
	ldr	r1, [r3, #116]
	ldr	r2, .L2+104
	str	r1, [r2, #0]
	ldr	r1, [r3, #120]
	ldr	r2, .L2+108
	ldr	r0, [r4, #0]
	str	r1, [r2, #0]
	ldr	r2, .L2+112
	ldr	r1, [r3, r2]
	ldr	r2, .L2+116
	str	r1, [r2, #0]
	ldr	r2, .L2+120
	ldr	r1, [r3, r2]
	ldr	r2, .L2+124
	str	r1, [r2, #0]
	ldr	r2, .L2+128
	ldr	r1, [r3, r2]
	ldr	r2, .L2+132
	str	r1, [r2, #0]
	ldr	r2, .L2+136
	ldr	r1, [r3, r2]
	ldr	r2, .L2+140
	str	r1, [r2, #0]
	ldr	r2, .L2+144
	ldr	r1, [r3, r2]
	ldr	r2, .L2+148
	str	r1, [r2, #0]
	ldr	r2, .L2+152
	ldr	r1, [r3, r2]
	ldr	r2, .L2+156
	str	r1, [r2, #0]
	ldr	r2, .L2+160
	ldr	r1, [r3, r2]
	ldr	r2, .L2+164
	str	r1, [r2, #0]
	ldr	r2, .L2+168
	ldr	r1, [r3, r2]
	ldr	r2, .L2+172
	str	r1, [r2, #0]
	ldr	r2, .L2+176
	ldr	r1, [r3, r2]
	ldr	r2, .L2+180
	str	r1, [r2, #0]
	ldr	r1, [r3, #208]
	ldr	r2, .L2+184
	str	r1, [r2, #0]
	ldr	r1, [r3, #212]
	ldr	r2, .L2+188
	str	r1, [r2, #0]
	ldr	r1, [r3, #216]
	ldr	r2, .L2+192
	str	r1, [r2, #0]
	ldr	r1, [r3, #220]
	ldr	r2, .L2+196
	str	r1, [r2, #0]
	ldr	r2, [r3, #172]
	ldr	r3, .L2+200
	str	r2, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	14224
	.word	system_preserves
	.word	GuiVar_FlowCheckingStable
	.word	GuiVar_FlowCheckingAllowed
	.word	GuiVar_FlowCheckingFMs
	.word	GuiVar_FlowCheckingHasNCMV
	.word	GuiVar_FlowCheckingFlag0
	.word	GuiVar_FlowCheckingFlag1
	.word	GuiVar_FlowCheckingFlag2
	.word	GuiVar_FlowCheckingFlag3
	.word	GuiVar_FlowCheckingFlag4
	.word	GuiVar_FlowCheckingFlag5
	.word	GuiVar_FlowCheckingFlag6
	.word	GuiVar_FlowCheckingChecked
	.word	GuiVar_FlowCheckingLFTime
	.word	GuiVar_FlowCheckingINTime
	.word	GuiVar_FlowCheckingMVJOTime
	.word	GuiVar_FlowCheckingStopIrriTime
	.word	GuiVar_FlowCheckingMVTime
	.word	GuiVar_FlowCheckingMVState
	.word	GuiVar_FlowCheckingPumpTime
	.word	GuiVar_FlowCheckingPumpState
	.word	GuiVar_FlowCheckingNumOn
	.word	GuiVar_FlowCheckingExpOn
	.word	GuiVar_FlowCheckingOnForProbList
	.word	14116
	.word	GuiVar_FlowCheckingExp
	.word	14120
	.word	GuiVar_FlowCheckingHi
	.word	14124
	.word	GuiVar_FlowCheckingLo
	.word	14048
	.word	GuiVar_FlowCheckingCycles
	.word	14052
	.word	GuiVar_FlowCheckingIterations
	.word	14056
	.word	GuiVar_FlowCheckingAllowedToLock
	.word	14060
	.word	GuiVar_FlowCheckingSlotSize
	.word	14064
	.word	GuiVar_FlowCheckingMaxOn
	.word	14068
	.word	GuiVar_FlowCheckingSlots
	.word	GuiVar_FlowCheckingGroupCnt0
	.word	GuiVar_FlowCheckingGroupCnt1
	.word	GuiVar_FlowCheckingGroupCnt2
	.word	GuiVar_FlowCheckingGroupCnt3
	.word	GuiVar_FlowCheckingPumpMix
.LFE0:
	.size	FDTO_FLOW_CHECKING_STATS_redraw_report, .-FDTO_FLOW_CHECKING_STATS_redraw_report
	.section	.text.FDTO_FLOW_CHECKING_STATS_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_FLOW_CHECKING_STATS_draw_report
	.type	FDTO_FLOW_CHECKING_STATS_draw_report, %function
FDTO_FLOW_CHECKING_STATS_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	str	lr, [sp, #-4]!
.LCFI1:
	mov	r2, r0
	bne	.L5
	mov	r0, #81
	mvn	r1, #0
	bl	GuiLib_ShowScreen
	ldr	r3, .L6
	mov	r2, #0
	str	r2, [r3, #0]
.L5:
	ldr	lr, [sp], #4
	b	FDTO_FLOW_CHECKING_STATS_redraw_report
.L7:
	.align	2
.L6:
	.word	.LANCHOR0
.LFE1:
	.size	FDTO_FLOW_CHECKING_STATS_draw_report, .-FDTO_FLOW_CHECKING_STATS_draw_report
	.section	.text.FLOW_CHECKING_STATS_process_report,"ax",%progbits
	.align	2
	.global	FLOW_CHECKING_STATS_process_report
	.type	FLOW_CHECKING_STATS_process_report, %function
FLOW_CHECKING_STATS_process_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	stmfd	sp!, {r4, lr}
.LCFI2:
	mov	r3, r0
	sub	sp, sp, #44
.LCFI3:
	mov	r2, r1
	beq	.L11
	bhi	.L12
	cmp	r0, #16
	beq	.L10
	cmp	r0, #20
	bne	.L9
	b	.L15
.L12:
	cmp	r0, #80
	beq	.L10
	cmp	r0, #84
	bne	.L9
.L10:
	cmp	r3, #20
	beq	.L15
	cmp	r3, #16
	movne	r0, r3
	moveq	r0, #80
	b	.L13
.L15:
	mov	r0, #84
.L13:
	mov	r4, #1
	ldr	r1, .L18
	mov	r2, #0
	mov	r3, #3
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	bl	process_uns32
	ldr	r3, .L18+4
	add	r0, sp, #8
	str	r4, [sp, #8]
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L8
.L11:
	ldr	r3, .L18+8
	mov	r2, #10
	str	r2, [r3, #0]
	b	.L17
.L9:
	mov	r0, r3
	mov	r1, r2
.L17:
	bl	KEY_process_global_keys
.L8:
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L19:
	.align	2
.L18:
	.word	.LANCHOR0
	.word	FDTO_FLOW_CHECKING_STATS_redraw_report
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	FLOW_CHECKING_STATS_process_report, .-FLOW_CHECKING_STATS_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_flow_checking.c\000"
	.section	.bss.g_FLOW_CHECKING_index_of_system_to_show,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_FLOW_CHECKING_index_of_system_to_show, %object
	.size	g_FLOW_CHECKING_index_of_system_to_show, 4
g_FLOW_CHECKING_index_of_system_to_show:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_flow_checking.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x59
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x2d
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x7e
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x97
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"FDTO_FLOW_CHECKING_STATS_draw_report\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_flow_checking.c\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"FLOW_CHECKING_STATS_process_report\000"
.LASF4:
	.ascii	"FDTO_FLOW_CHECKING_STATS_redraw_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
