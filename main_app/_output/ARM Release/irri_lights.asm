	.file	"irri_lights.c"
	.text
.Ltext0:
	.section	.text.init_irri_lights,"ax",%progbits
	.align	2
	.global	init_irri_lights
	.type	init_irri_lights, %function
init_irri_lights:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L6
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI0:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L6+4
	mov	r3, #69
	bl	xQueueTakeMutexRecursive_debug
	mov	r5, #0
	ldr	r0, .L6+8
	mov	r1, #0
	bl	nm_ListInit
	mov	r4, r5
	ldr	r7, .L6+8
	b	.L2
.L3:
	mov	r1, r6
	mov	r0, r5
	bl	LIGHTS_get_lights_array_index
	mla	r3, r8, r0, r7
	add	r0, r0, #1
	mul	r0, r8, r0
	str	r6, [r3, #56]
	add	r6, r6, #1
	cmp	r6, #4
	str	r4, [r3, #28]
	str	r4, [r3, #24]
	str	r4, [r3, #20]
	str	r5, [r3, #52]
	str	r4, [r7, r0]
	str	r4, [r3, #32]
	str	r4, [r3, #48]
	str	r4, [r3, #44]
	bne	.L3
	add	r5, r5, #1
	cmp	r5, #12
	beq	.L4
.L2:
	mov	r6, #0
	mov	r8, #40
	b	.L3
.L4:
	ldr	r3, .L6
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L7:
	.align	2
.L6:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
.LFE0:
	.size	init_irri_lights, .-init_irri_lights
	.section	.text.IRRI_LIGHTS_restart_all_lights,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_restart_all_lights
	.type	IRRI_LIGHTS_restart_all_lights, %function
IRRI_LIGHTS_restart_all_lights:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L13
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L13+4
	mov	r3, #124
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L13+8
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L9
.L10:
	str	r4, [r1, #20]
	str	r4, [r1, #28]
	str	r4, [r1, #24]
	ldr	r0, .L13+8
	bl	nm_ListGetNext
	mov	r1, r0
.L9:
	cmp	r1, #0
	bne	.L10
	ldr	r0, .L13+8
	bl	nm_ListGetFirst
	b	.L11
.L12:
	ldr	r0, .L13+8
	ldr	r1, .L13+4
	mov	r2, #152
	bl	nm_ListRemoveHead_debug
.L11:
	cmp	r0, #0
	bne	.L12
	ldr	r3, .L13
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L14:
	.align	2
.L13:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
.LFE1:
	.size	IRRI_LIGHTS_restart_all_lights, .-IRRI_LIGHTS_restart_all_lights
	.section	.text.IRRI_LIGHTS_light_is_energized,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_light_is_energized
	.type	IRRI_LIGHTS_light_is_energized, %function
IRRI_LIGHTS_light_is_energized:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	ldr	r4, .L16
	mov	r1, #400
	ldr	r2, .L16+4
	mov	r5, r0
	mov	r3, #165
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	add	r5, r5, #1
	mov	r3, #40
	mul	r5, r3, r5
	ldr	r3, .L16+8
	ldr	r0, [r4, #0]
	ldr	r5, [r3, r5]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L17:
	.align	2
.L16:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
.LFE2:
	.size	IRRI_LIGHTS_light_is_energized, .-IRRI_LIGHTS_light_is_energized
	.section	.text.IRRI_LIGHTS_get_number_of_energized_lights,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_number_of_energized_lights
	.type	IRRI_LIGHTS_get_number_of_energized_lights, %function
IRRI_LIGHTS_get_number_of_energized_lights:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	ldr	r4, .L19
	mov	r1, #400
	ldr	r2, .L19+4
	ldr	r0, [r4, #0]
	mov	r3, #226
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L19+8
	ldr	r0, [r4, #0]
	ldr	r5, [r3, #8]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L20:
	.align	2
.L19:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
.LFE4:
	.size	IRRI_LIGHTS_get_number_of_energized_lights, .-IRRI_LIGHTS_get_number_of_energized_lights
	.section	.text.IRRI_LIGHTS_populate_pointers_of_energized_lights,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_populate_pointers_of_energized_lights
	.type	IRRI_LIGHTS_populate_pointers_of_energized_lights, %function
IRRI_LIGHTS_populate_pointers_of_energized_lights:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI4:
	ldr	r6, .L26
	mov	r1, #0
	mov	r2, #192
	ldr	r0, .L26+4
	bl	memset
	ldr	r0, [r6, #0]
	mov	r1, #400
	ldr	r2, .L26+8
	mov	r3, #264
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, .L26+4
	ldr	r8, .L26+12
	mov	r4, #0
	mov	r5, r4
.L23:
.LBB14:
	mov	r3, #202
	mov	r1, #400
	ldr	r2, .L26+8
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
.LBE14:
	mov	r0, r4
	bl	IRRI_LIGHTS_light_is_energized
	cmp	r0, #0
	movne	r3, #40
	mlane	r3, r4, r3, r8
	add	r4, r4, #1
	addne	r3, r3, #20
	strne	r3, [r7, r5, asl #2]
	addne	r5, r5, #1
	cmp	r4, #48
	bne	.L23
	bl	IRRI_LIGHTS_get_number_of_energized_lights
	cmp	r0, r5
	beq	.L24
	ldr	r0, .L26+16
	bl	Alert_Message
.L24:
	ldr	r3, .L26
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L27:
	.align	2
.L26:
	.word	irri_lights_recursive_MUTEX
	.word	.LANCHOR1
	.word	.LC0
	.word	.LANCHOR0
	.word	.LC1
.LFE5:
	.size	IRRI_LIGHTS_populate_pointers_of_energized_lights, .-IRRI_LIGHTS_populate_pointers_of_energized_lights
	.section	.text.IRRI_LIGHTS_get_ptr_to_energized_light,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_ptr_to_energized_light
	.type	IRRI_LIGHTS_get_ptr_to_energized_light, %function
IRRI_LIGHTS_get_ptr_to_energized_light:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L29
	ldr	r0, [r3, r0, asl #2]
	bx	lr
.L30:
	.align	2
.L29:
	.word	.LANCHOR1
.LFE6:
	.size	IRRI_LIGHTS_get_ptr_to_energized_light, .-IRRI_LIGHTS_get_ptr_to_energized_light
	.section	.text.IRRI_LIGHTS_get_box_index,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_box_index
	.type	IRRI_LIGHTS_get_box_index, %function
IRRI_LIGHTS_get_box_index:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI5:
	ldr	r4, .L32
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L32+4
	ldr	r0, [r4, #0]
	ldr	r3, .L32+8
	bl	xQueueTakeMutexRecursive_debug
.LBB15:
	ldr	r3, .L32+12
.LBE15:
	ldr	r0, [r4, #0]
	ldr	r3, [r3, r5, asl #2]
	ldr	r5, [r3, #32]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L33:
	.align	2
.L32:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	327
	.word	.LANCHOR1
.LFE7:
	.size	IRRI_LIGHTS_get_box_index, .-IRRI_LIGHTS_get_box_index
	.section	.text.IRRI_LIGHTS_get_output_index,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_output_index
	.type	IRRI_LIGHTS_get_output_index, %function
IRRI_LIGHTS_get_output_index:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI6:
	ldr	r4, .L35
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L35+4
	ldr	r0, [r4, #0]
	mov	r3, #356
	bl	xQueueTakeMutexRecursive_debug
.LBB16:
	ldr	r3, .L35+8
.LBE16:
	ldr	r0, [r4, #0]
	ldr	r3, [r3, r5, asl #2]
	ldr	r5, [r3, #36]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L36:
	.align	2
.L35:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR1
.LFE8:
	.size	IRRI_LIGHTS_get_output_index, .-IRRI_LIGHTS_get_output_index
	.section	.text.IRRI_LIGHTS_get_reason_on_list,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_reason_on_list
	.type	IRRI_LIGHTS_get_reason_on_list, %function
IRRI_LIGHTS_get_reason_on_list:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	ldr	r4, .L38
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L38+4
	ldr	r0, [r4, #0]
	ldr	r3, .L38+8
	bl	xQueueTakeMutexRecursive_debug
.LBB17:
	ldr	r3, .L38+12
.LBE17:
	ldr	r0, [r4, #0]
	ldr	r3, [r3, r5, asl #2]
	ldr	r5, [r3, #16]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L39:
	.align	2
.L38:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	385
	.word	.LANCHOR1
.LFE9:
	.size	IRRI_LIGHTS_get_reason_on_list, .-IRRI_LIGHTS_get_reason_on_list
	.section	.text.IRRI_LIGHTS_get_stop_date,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_stop_date
	.type	IRRI_LIGHTS_get_stop_date, %function
IRRI_LIGHTS_get_stop_date:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI8:
	ldr	r4, .L41
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L41+4
	ldr	r0, [r4, #0]
	ldr	r3, .L41+8
	bl	xQueueTakeMutexRecursive_debug
.LBB18:
	ldr	r3, .L41+12
.LBE18:
	ldr	r0, [r4, #0]
	ldr	r3, [r3, r5, asl #2]
	ldr	r5, [r3, #28]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L42:
	.align	2
.L41:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	414
	.word	.LANCHOR1
.LFE10:
	.size	IRRI_LIGHTS_get_stop_date, .-IRRI_LIGHTS_get_stop_date
	.section	.text.IRRI_LIGHTS_get_stop_time,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_stop_time
	.type	IRRI_LIGHTS_get_stop_time, %function
IRRI_LIGHTS_get_stop_time:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI9:
	ldr	r4, .L44
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L44+4
	ldr	r0, [r4, #0]
	ldr	r3, .L44+8
	bl	xQueueTakeMutexRecursive_debug
.LBB19:
	ldr	r3, .L44+12
.LBE19:
	ldr	r0, [r4, #0]
	ldr	r3, [r3, r5, asl #2]
	ldr	r5, [r3, #24]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L45:
	.align	2
.L44:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	443
	.word	.LANCHOR1
.LFE11:
	.size	IRRI_LIGHTS_get_stop_time, .-IRRI_LIGHTS_get_stop_time
	.section	.text.IRRI_LIGHTS_get_light_stop_date_and_time,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_get_light_stop_date_and_time
	.type	IRRI_LIGHTS_get_light_stop_date_and_time, %function
IRRI_LIGHTS_get_light_stop_date_and_time:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI10:
	ldr	r6, .L49
	mov	r5, r1
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r6, #0]
	ldr	r2, .L49+4
	mov	r3, #472
	bl	xQueueTakeMutexRecursive_debug
	add	r3, r5, #1
	mov	r1, #40
	ldr	r2, .L49+8
	mul	r3, r1, r3
	ldr	r0, [r6, #0]
	ldr	r3, [r2, r3]
	cmp	r3, #0
	mlane	r5, r1, r5, r2
	moveq	r5, r3
	ldrneh	r7, [r5, #48]
	ldrne	r5, [r5, #44]
	moveq	r7, r5
	bl	xQueueGiveMutexRecursive
	mov	r3, r5, lsr #8
	strb	r5, [r4, #0]
	strb	r3, [r4, #1]
	strb	r7, [r4, #4]
	mov	r3, r5, lsr #16
	mov	r7, r7, lsr #8
	mov	r5, r5, lsr #24
	strb	r3, [r4, #2]
	strb	r5, [r4, #3]
	strb	r7, [r4, #5]
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L50:
	.align	2
.L49:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
.LFE12:
	.size	IRRI_LIGHTS_get_light_stop_date_and_time, .-IRRI_LIGHTS_get_light_stop_date_and_time
	.section	.text.IRRI_LIGHTS_maintain_lights_list,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_maintain_lights_list
	.type	IRRI_LIGHTS_maintain_lights_list, %function
IRRI_LIGHTS_maintain_lights_list:
.LFB13:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L67
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, sl, lr}
.LCFI11:
	ldr	r2, .L67+4
	mov	r1, #400
	mov	sl, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L67+8
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_we_are_poafs
	ldr	r3, .L67+12
	cmp	r0, #0
	beq	.L52
	ldr	r2, .L67+16
	ldr	r2, [r2, #8]
	cmp	r2, #0
	beq	.L52
	ldr	r2, [r3, #0]
	ldr	r1, .L67+20
	cmp	r2, r1
	addls	r2, r2, #1
	bls	.L66
	b	.L53
.L52:
	mov	r2, #0
.L66:
	str	r2, [r3, #0]
.L53:
	ldr	r0, .L67+24
	bl	nm_ListGetFirst
.LBB20:
	ldr	r6, .L67+28
	ldr	r5, .L67+32
	ldr	r8, .L67+12
.LBE20:
	mov	r4, r0
	b	.L65
.L62:
.LBB22:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L55
	ldr	r3, .L67+16
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L56
.L55:
	ldr	r7, [r4, #32]
	bl	FLOWSENSE_get_controller_index
	cmp	r7, r0
	bne	.L57
.L56:
	ldr	r7, [r4, #32]
	bl	FLOWSENSE_get_controller_index
	cmp	r7, r0
	bne	.L58
	ldr	r7, [r4, #20]
	cmp	r7, #1
	bne	.L58
	ldr	r3, [r8, #0]
	ldr	r0, .L67+20
	cmp	r3, #120
	mov	r3, #0
	strhi	r3, [r4, #20]
	movls	r7, r3
	ldr	r3, [r4, #24]
	ldrh	r2, [r4, #28]
	add	r1, r3, #120
	cmp	r1, r0
	subhi	r3, r3, #86016
	strh	r2, [sp, #4]	@ movhi
	str	r1, [sp, #0]
	addhi	r2, r2, #1
	subhi	r3, r3, #264
	mov	r0, sl
	mov	r1, sp
	strhih	r2, [sp, #4]	@ movhi
	strhi	r3, [sp, #0]
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #1
	beq	.L57
	cmp	r7, #0
	bne	.L57
.L58:
	ldr	r7, [r4, #20]
	cmp	r7, #0
	bne	.L61
	ldr	r3, [r4, #12]
	sub	r3, r3, #1
	cmp	r3, #3
	bhi	.L61
	mov	r3, #1
	ldr	r2, .L67+4
	str	r3, [r4, #20]
	mov	r1, #400
	add	r3, r3, #616
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [r4, #36]
	ldr	r0, [r4, #32]
	bl	LIGHTS_get_lights_array_index
	add	r0, r5, r0
	ldrb	r3, [r0, #976]	@ zero_extendqisi2
	bic	r3, r3, #4
	strb	r3, [r0, #976]
	ldr	r1, [r4, #36]
	ldr	r0, [r4, #32]
	bl	LIGHTS_get_lights_array_index
	add	r0, r5, r0
	ldrb	r3, [r0, #976]	@ zero_extendqisi2
	bic	r3, r3, #8
	strb	r3, [r0, #976]
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	str	r7, [r4, #12]
	b	.L61
.L57:
.LBB21:
	mov	r3, #0
	str	r3, [r4, #20]
	str	r3, [r4, #28]
	str	r3, [r4, #24]
	mov	r1, r4
	ldr	r0, .L67+24
	bl	nm_ListGetNext
	mov	r1, r4
	ldr	r2, .L67+4
	ldr	r3, .L67+36
	mov	r7, r0
	ldr	r0, .L67+24
	bl	nm_ListRemove_debug
	mov	r4, r7
.L65:
.LBE21:
.LBE22:
	cmp	r4, #0
	bne	.L62
	ldr	r3, .L67
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, sl, pc}
.L61:
.LBB23:
	mov	r1, r4
	ldr	r0, .L67+24
	bl	nm_ListGetNext
	mov	r4, r0
	b	.L65
.L68:
	.align	2
.L67:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	513
	.word	.LANCHOR2
	.word	comm_mngr
	.word	86399
	.word	.LANCHOR0
	.word	lights_preserves_recursive_MUTEX
	.word	lights_preserves
	.word	650
.LBE23:
.LFE13:
	.size	IRRI_LIGHTS_maintain_lights_list, .-IRRI_LIGHTS_maintain_lights_list
	.section	.text.IRRI_LIGHTS_process_rcvd_lights_on_list_record,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_process_rcvd_lights_on_list_record
	.type	IRRI_LIGHTS_process_rcvd_lights_on_list_record, %function
IRRI_LIGHTS_process_rcvd_lights_on_list_record:
.LFB14:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI12:
	ldrh	r3, [r0, #4]
	mov	r5, r0
	strh	r3, [sp, #12]	@ movhi
	ldr	r3, [r0, #0]
	ldrb	r6, [r0, #6]	@ zero_extendqisi2
	mov	r0, sp
	str	r3, [sp, #8]
	bl	EPSON_obtain_latest_time_and_date
	ldrb	r3, [r5, #7]	@ zero_extendqisi2
	sub	r3, r3, #1
	cmp	r3, #3
	ldrhi	r0, .L89
	bhi	.L85
	ldrb	r4, [r5, #6]	@ zero_extendqisi2
	cmp	r4, #47
	ldrhi	r0, .L89+4
	bls	.L87
	b	.L85
.L83:
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	beq	.L74
	bl	FLOWSENSE_get_controller_index
	cmp	r0, r6, lsr #2
	bne	.L73
.L74:
	ldr	r0, .L89+8
.L85:
	bl	Alert_Message
.L73:
	ldr	r0, .L89+12
	bl	Alert_Message
	b	.L69
.L88:
	mov	r0, r6
	mov	r1, r4
	bl	nm_ListInsertTail
	cmp	r0, #0
	beq	.L76
	ldr	r0, .L89+16
	bl	Alert_Message
.L76:
	ldrb	r3, [r5, #7]	@ zero_extendqisi2
	str	r3, [r4, #12]
	ldrb	r3, [r5, #7]	@ zero_extendqisi2
	sub	r3, r3, #1
	cmp	r3, #3
	ldrls	pc, [pc, r3, asl #2]
	b	.L77
.L82:
	.word	.L78
	.word	.L79
	.word	.L80
	.word	.L81
.L78:
	mov	r3, #1
	b	.L86
.L79:
	mov	r3, #4
	b	.L86
.L80:
	mov	r3, #7
	b	.L86
.L81:
	mov	r3, #6
.L86:
	str	r3, [r4, #16]
.L77:
	ldrh	r3, [r5, #4]
	str	r3, [r4, #28]
	ldr	r3, [r5, #0]
	str	r3, [r4, #24]
	ldr	r3, .L89+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L69
.L87:
	mov	r0, sp
	add	r1, sp, #8
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #1
	beq	.L83
	ldr	r6, .L89+24
	mov	r3, #40
	mla	r4, r3, r4, r6
	ldr	r3, .L89+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L89+28
	ldr	r3, .L89+32
	bl	xQueueTakeMutexRecursive_debug
	add	r4, r4, #20
	mov	r0, r6
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	bne	.L76
	b	.L88
.L69:
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, pc}
.L90:
	.align	2
.L89:
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	irri_lights_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC0
	.word	747
.LFE14:
	.size	IRRI_LIGHTS_process_rcvd_lights_on_list_record, .-IRRI_LIGHTS_process_rcvd_lights_on_list_record
	.section	.text.IRRI_LIGHTS_process_rcvd_lights_action_needed_record,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_process_rcvd_lights_action_needed_record
	.type	IRRI_LIGHTS_process_rcvd_lights_action_needed_record, %function
IRRI_LIGHTS_process_rcvd_lights_action_needed_record:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #0]
	stmfd	sp!, {r4, r5, lr}
.LCFI13:
	cmp	r3, #47
	mov	r5, r0
	bls	.L92
	ldr	r0, .L99
	bl	Alert_Message
	ldr	r0, .L99+4
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_Message
.L98:
	cmp	r3, #5
	bne	.L94
	ldr	r3, [r5, #0]
	ldr	r2, .L99+8
	add	r3, r2, r3
	ldrb	r2, [r3, #976]	@ zero_extendqisi2
	orr	r2, r2, #4
	strb	r2, [r3, #976]
.L94:
	mov	r3, #0
	str	r3, [r4, #20]
	str	r3, [r4, #28]
	str	r3, [r4, #24]
	mov	r1, r4
	ldr	r2, .L99+12
	ldr	r3, .L99+16
	ldr	r0, .L99+20
	bl	nm_ListRemove_debug
	b	.L95
.L96:
	ldr	r0, .L99+24
	bl	Alert_Message
.L95:
	mov	r3, #0
	str	r3, [r4, #12]
	ldr	r3, .L99+28
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L92:
	ldr	r2, .L99+20
	mov	r4, #40
	mla	r4, r3, r4, r2
	ldr	r3, .L99+28
	ldr	r2, .L99+12
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L99+32
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #4]
	add	r4, r4, #20
	sub	r2, r3, #5
	cmp	r2, #6
	bhi	.L96
	b	.L98
.L100:
	.align	2
.L99:
	.word	.LC7
	.word	.LC8
	.word	lights_preserves
	.word	.LC0
	.word	877
	.word	.LANCHOR0
	.word	.LC9
	.word	irri_lights_recursive_MUTEX
	.word	847
.LFE15:
	.size	IRRI_LIGHTS_process_rcvd_lights_action_needed_record, .-IRRI_LIGHTS_process_rcvd_lights_action_needed_record
	.section	.text.IRRI_LIGHTS_request_light_on,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_request_light_on
	.type	IRRI_LIGHTS_request_light_on, %function
IRRI_LIGHTS_request_light_on:
.LFB16:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, lr}
.LCFI14:
	ldr	r6, .L107
	mov	r5, r0
	ldr	r7, [r6, #44]
	mov	r4, r1
	cmp	r7, #0
	mov	r8, r2
	movne	r7, #0
	bne	.L102
	mov	r0, sp
	strh	r1, [sp, #12]	@ movhi
	str	r2, [sp, #8]
	bl	EPSON_obtain_latest_time_and_date
	cmp	r5, #47
	ldrhi	r0, .L107+4
	bhi	.L106
	mov	r0, sp
	add	r1, sp, #8
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #1
	bne	.L104
	ldr	r0, .L107+8
.L106:
	bl	Alert_Message
.L102:
	mov	r0, r7
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L104:
	mov	r7, #1
	str	r7, [r6, #44]
	str	r5, [r6, #48]
	str	r4, [r6, #52]
	str	r8, [r6, #56]
	b	.L102
.L108:
	.align	2
.L107:
	.word	irri_comm
	.word	.LC10
	.word	.LC11
.LFE16:
	.size	IRRI_LIGHTS_request_light_on, .-IRRI_LIGHTS_request_light_on
	.section	.text.IRRI_LIGHTS_request_light_off,"ax",%progbits
	.align	2
	.global	IRRI_LIGHTS_request_light_off
	.type	IRRI_LIGHTS_request_light_off, %function
IRRI_LIGHTS_request_light_off:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L114
	stmfd	sp!, {r4, lr}
.LCFI15:
	ldr	r4, [r3, #44]
	cmp	r4, #0
	bne	.L112
	cmp	r0, #47
	movls	r4, #1
	strls	r4, [r3, #60]
	strls	r0, [r3, #64]
	bls	.L113
	ldr	r0, .L114+4
	ldr	r1, .L114+8
	bl	Alert_index_out_of_range_with_filename
.L113:
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L112:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L115:
	.align	2
.L114:
	.word	irri_comm
	.word	.LC0
	.word	973
.LFE17:
	.size	IRRI_LIGHTS_request_light_off, .-IRRI_LIGHTS_request_light_off
	.global	IRRI_LIGHTS_seconds_since_chain_went_down
	.global	irri_lights
	.section	.bss.IRRI_LIGHTS_MENU_items,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	IRRI_LIGHTS_MENU_items, %object
	.size	IRRI_LIGHTS_MENU_items, 192
IRRI_LIGHTS_MENU_items:
	.space	192
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/irri_lights.c\000"
.LC1:
	.ascii	"Num of Energized Lights Mismatch\000"
.LC2:
	.ascii	"I_L: Bad action (1)\000"
.LC3:
	.ascii	"I_L: Bad light index (1)\000"
.LC4:
	.ascii	"I_L: ON received wih no time left\000"
.LC5:
	.ascii	"I_L: light index fault (1)\000"
.LC6:
	.ascii	"I_L: list insert fault\000"
.LC7:
	.ascii	"I_L: Bad light index (2)\000"
.LC8:
	.ascii	"I_L: light index fault (2)\000"
.LC9:
	.ascii	"I_L: Bad action (2)\000"
.LC10:
	.ascii	"I_L: Bad light index (3)\000"
.LC11:
	.ascii	"I_L: ON received wih no time left (2)\000"
	.section	.bss.IRRI_LIGHTS_seconds_since_chain_went_down,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	IRRI_LIGHTS_seconds_since_chain_went_down, %object
	.size	IRRI_LIGHTS_seconds_since_chain_went_down, 4
IRRI_LIGHTS_seconds_since_chain_went_down:
	.space	4
	.section	.bss.irri_lights,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	irri_lights, %object
	.size	irri_lights, 1940
irri_lights:
	.space	1940
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI5-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI6-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI7-.LFB9
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI8-.LFB10
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI9-.LFB11
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI10-.LFB12
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI11-.LFB13
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI12-.LFB14
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI13-.LFB15
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI14-.LFB16
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x83
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI15-.LFB17
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE32:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/irri_lights.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x18b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF16
	.byte	0x1
	.4byte	.LASF17
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF18
	.byte	0x1
	.byte	0xc6
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x131
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x3f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x77
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xa1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xde
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xfc
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x5
	.4byte	0x29
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x141
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x15e
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x17b
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x198
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1b5
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST9
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x1d1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST10
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1f9
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST11
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x2ab
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST12
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x338
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST13
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x385
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST14
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x3bd
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST15
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB7
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB8
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB9
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB10
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB11
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB12
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB13
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB14
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB15
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB16
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB17
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x9c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"IRRI_LIGHTS_light_is_energized\000"
.LASF9:
	.ascii	"IRRI_LIGHTS_get_stop_time\000"
.LASF12:
	.ascii	"IRRI_LIGHTS_process_rcvd_lights_on_list_record\000"
.LASF0:
	.ascii	"init_irri_lights\000"
.LASF13:
	.ascii	"IRRI_LIGHTS_process_rcvd_lights_action_needed_recor"
	.ascii	"d\000"
.LASF19:
	.ascii	"IRRI_LIGHTS_get_ptr_to_energized_light\000"
.LASF8:
	.ascii	"IRRI_LIGHTS_get_stop_date\000"
.LASF3:
	.ascii	"IRRI_LIGHTS_get_number_of_energized_lights\000"
.LASF16:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF15:
	.ascii	"IRRI_LIGHTS_request_light_off\000"
.LASF10:
	.ascii	"IRRI_LIGHTS_get_light_stop_date_and_time\000"
.LASF5:
	.ascii	"IRRI_LIGHTS_get_box_index\000"
.LASF6:
	.ascii	"IRRI_LIGHTS_get_output_index\000"
.LASF1:
	.ascii	"IRRI_LIGHTS_restart_all_lights\000"
.LASF7:
	.ascii	"IRRI_LIGHTS_get_reason_on_list\000"
.LASF17:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/irri_lights.c\000"
.LASF4:
	.ascii	"IRRI_LIGHTS_populate_pointers_of_energized_lights\000"
.LASF11:
	.ascii	"IRRI_LIGHTS_maintain_lights_list\000"
.LASF18:
	.ascii	"IRRI_LIGHTS_get_light_at_this_index\000"
.LASF14:
	.ascii	"IRRI_LIGHTS_request_light_on\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
