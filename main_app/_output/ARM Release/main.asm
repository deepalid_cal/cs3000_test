	.file	"main.c"
	.text
.Ltext0:
	.section	.text.toggle_LED,"ax",%progbits
	.align	2
	.type	toggle_LED, %function
toggle_LED:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	bne	.L2
	ldr	r3, .L7
	ldr	r2, [r3, #76]
	tst	r2, #4
	mov	r2, #4
	strne	r2, [r3, #72]
	streq	r2, [r3, #68]
	ldr	r3, .L7+4
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	bx	lr
.L2:
	cmp	r0, #2
	bxne	lr
.LBB4:
	ldr	r3, .L7
	ldr	r2, [r3, #76]
	tst	r2, #2
	strne	r0, [r3, #72]
	streq	r0, [r3, #68]
	bx	lr
.L8:
	.align	2
.L7:
	.word	1073905664
	.word	.LANCHOR0
.LBE4:
.LFE0:
	.size	toggle_LED, .-toggle_LED
	.section	.text.vled_TASK,"ax",%progbits
	.align	2
	.global	vled_TASK
	.type	vled_TASK, %function
vled_TASK:
.LFB1:
	@ args = 0, pretend = 0, frame = 624
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
	mov	r4, r0
	sub	sp, sp, #624
.LCFI1:
.L57:
	mov	r0, #2
	bl	toggle_LED
	cmp	r4, #2
	beq	.L39
	cmp	r4, #3
	beq	.L40
	cmp	r4, #4
	beq	.L41
	mov	r5, #256
	mov	r7, #24
	mov	r6, r5
	add	r8, sp, #8
	b	.L11
.L39:
	mov	r5, #320
	mov	r7, #16
	mov	r6, r5
	add	r8, sp, #136
	b	.L11
.L40:
	mov	r5, #896
	mov	r7, #32
	mov	r6, #384
	add	r8, sp, #264
	b	.L11
.L41:
	mov	r5, #1104
	mov	r7, #64
	mov	r6, #512
	add	r8, sp, #392
.L11:
	sub	r3, r5, #12
	mov	r3, r3, lsr #2
	mov	sl, #0
	mov	r9, r8
	str	r3, [sp, #4]
.L14:
	add	r0, sp, #520
	mov	fp, #20
	mla	fp, sl, fp, r0
	mov	r1, #0
	mov	r0, fp
	bl	nm_ListInit
	mov	r8, #0
.L13:
	ldr	r1, .L65
	ldr	r2, .L65+4
	mov	r0, r5
	bl	mem_malloc_debug
	mov	r3, #0
	mov	r1, r0
	add	r2, r0, #12
.L12:
	rsb	r0, r4, r2
	str	r0, [r2], #4
	ldr	r0, [sp, #4]
	add	r3, r3, #1
	cmp	r3, r0
	bne	.L12
	mov	r0, fp
	add	r8, r8, #1
	bl	nm_ListInsertTail
	cmp	r8, r7
	bcc	.L13
	add	sl, sl, #1
	cmp	sl, #4
	bne	.L14
	mov	fp, r9
	mov	sl, #0
	mov	r9, r6
.L22:
	add	r1, sp, #520
	mov	r6, #20
	mla	r6, sl, r6, r1
	mov	r8, #0
	mov	r0, r6
	bl	nm_ListGetFirst
	mov	r5, r0
	b	.L15
.L19:
	add	r3, r5, #8
	mov	r2, #0
.L18:
	add	r3, r3, #4
	ldr	r0, [r3, #0]
	rsb	r1, r4, r3
	cmp	r0, r1
	beq	.L16
.L58:
	b	.L58
.L16:
	ldr	r0, [sp, #4]
	add	r2, r2, #1
	cmp	r2, r0
	bne	.L18
	mov	r1, r5
	mov	r0, r6
	bl	nm_ListGetNext
	mov	r1, r5
	ldr	r2, .L65
	ldr	r3, .L65+8
	add	r8, r8, #1
	mov	ip, r0
	mov	r0, r6
	str	ip, [sp, #0]
	bl	nm_ListRemove_debug
	mov	r0, r5
	ldr	r1, .L65
	mov	r2, #640
	bl	mem_free_debug
	ldr	ip, [sp, #0]
	mov	r5, ip
.L15:
	cmp	r5, #0
	bne	.L19
	cmp	r8, r7
	beq	.L20
.L59:
	b	.L59
.L20:
	add	sl, sl, #1
	cmp	sl, #4
	bne	.L22
	mov	r8, fp
	mov	r6, r9
	mov	r7, r9, lsr #2
.L24:
	mov	r0, r6
	ldr	r1, .L65
	mov	r2, #656
	bl	mem_malloc_debug
	mov	r3, #0
	str	r0, [r8, r5]
.L23:
	add	r3, r3, #1
	add	r2, r0, r4
	cmp	r3, r7
	str	r2, [r0], #4
	bne	.L23
	add	r5, r5, #8
	cmp	r5, #128
	bne	.L24
	cmp	r4, #1
	add	r8, r8, #128
	addeq	r8, sp, #8
	beq	.L25
	cmp	r4, #2
	addeq	r8, sp, #136
	beq	.L25
	cmp	r4, #3
	addeq	r8, sp, #264
	beq	.L25
	cmp	r4, #4
	addeq	r8, sp, #392
.L25:
	mov	r5, #0
.L29:
	ldr	r0, [r8, r5]
	mov	r2, #0
	mov	r3, r0
.L28:
	ldr	ip, [r3, #0]
	add	r1, r3, r4
	cmp	ip, r1
	beq	.L26
.L60:
	b	.L60
.L26:
	add	r2, r2, #1
	cmp	r2, r7
	add	r3, r3, #4
	bne	.L28
	ldr	r1, .L65
	ldr	r2, .L65+12
	bl	mem_free_debug
	add	r5, r5, #8
	cmp	r5, #128
	bne	.L29
	cmp	r4, #1
	bne	.L30
	mov	r0, r4
	bl	toggle_LED
	ldr	r3, .L65+16
	ldr	r2, .L65+20
	str	r3, [sp, #616]	@ float
	ldr	r3, .L65+24
	str	r3, [sp, #620]	@ float
	flds	s13, [sp, #616]
	flds	s15, [sp, #620]
	flds	s14, [sp, #620]
	ldr	r3, .L65+28
	fmuls	s15, s13, s15
	fdivs	s15, s15, s14
	fsts	s15, [sp, #620]
	str	r2, [sp, #600]
	str	r3, [sp, #604]
	ldr	r3, .L65+32
	mov	r2, #0
	str	r2, [sp, #608]
	str	r3, [sp, #612]
	fldd	d7, [sp, #600]
	fldd	d5, [sp, #608]
	fldd	d6, [sp, #608]
	fmuld	d7, d7, d5
	fdivd	d7, d7, d6
	fstd	d7, [sp, #608]
	flds	s14, [sp, #620]
	flds	s15, [sp, #616]
	fcmps	s14, s15
	fmstat
	beq	.L31
.L61:
	b	.L61
.L31:
	fldd	d6, [sp, #608]
	fldd	d7, [sp, #600]
	fcmpd	d6, d7
	fmstat
	beq	.L33
.L62:
	b	.L62
.L33:
	mov	r0, r4
	bl	toggle_LED
	b	.L57
.L30:
	cmp	r4, #2
	bne	.L57
	ldr	r3, .L65+36
	ldr	r0, .L65+40
	ldr	r2, .L65+20
	str	r0, [sp, #616]	@ float
	str	r3, [sp, #620]	@ float
	flds	s13, [sp, #616]
	flds	s15, [sp, #620]
	flds	s14, [sp, #620]
	ldr	r3, .L65+44
	fmuls	s15, s13, s15
	fdivs	s15, s15, s14
	fsts	s15, [sp, #620]
	str	r2, [sp, #600]
	str	r3, [sp, #604]
	ldr	r3, .L65+48
	mov	r2, #0
	str	r2, [sp, #608]
	str	r3, [sp, #612]
	fldd	d7, [sp, #600]
	fldd	d5, [sp, #608]
	fldd	d6, [sp, #608]
	fmuld	d7, d7, d5
	fdivd	d7, d7, d6
	fstd	d7, [sp, #608]
	flds	s14, [sp, #620]
	flds	s15, [sp, #616]
	fcmps	s14, s15
	fmstat
	beq	.L36
.L63:
	b	.L63
.L36:
	fldd	d6, [sp, #608]
	fldd	d7, [sp, #600]
	fcmpd	d6, d7
	fmstat
	beq	.L57
.L64:
	b	.L64
.L66:
	.align	2
.L65:
	.word	.LC0
	.word	595
	.word	638
	.word	707
	.word	1090868566
	.word	-1431655766
	.word	1071644672
	.word	1075882666
	.word	1073479680
	.word	1076887552
	.word	1091917142
	.word	1076013738
	.word	1074135040
.LFE1:
	.size	vled_TASK, .-vled_TASK
	.section	.text.startup.main,"ax",%progbits
	.align	2
	.global	main
	.type	main, %function
main:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI2:
	ldr	r3, .L69
	mov	r5, #0
	str	r5, [r3, #32]
	ldr	r3, .L69+4
	ldr	r4, .L69+8
	str	r5, [r3, #32]
	ldr	r3, .L69+12
	str	r5, [r3, #32]
	ldr	r3, .L69+16
	str	r5, [r3, #32]
	bl	init_mem_partitioning
	bl	init_mem_debug
	mov	r3, #1
	ldr	r1, .L69+20
	mov	r2, #1024
	str	r3, [sp, #0]
	ldr	r0, .L69+24
	mov	r3, r5
	stmib	sp, {r4, r5}
	str	r5, [sp, #12]
	bl	xTaskGenericCreate
	ldr	r0, .L69+28
	bl	vPortSaveVFPRegisters
	ldr	r0, [r4, #0]
	ldr	r1, .L69+28
	bl	vTaskSetApplicationTaskTag
	bl	vTaskStartScheduler
.L68:
	b	.L68
.L70:
	.align	2
.L69:
	.word	alerts_struct_user
	.word	alerts_struct_changes
	.word	startup_task_handle
	.word	alerts_struct_tp_micro
	.word	alerts_struct_engineering
	.word	.LC1
	.word	system_startup_task
	.word	startup_task_FLOP_registers
.LFE2:
	.size	main, .-main
	.section	.text.vApplicationStackOverflowHook,"ax",%progbits
	.align	2
	.global	vApplicationStackOverflowHook
	.type	vApplicationStackOverflowHook, %function
vApplicationStackOverflowHook:
.LFB3:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI3:
	mov	r4, r1
	sub	sp, sp, #64
.LCFI4:
	bl	vTaskSuspendAll
	mov	r0, sp
	mov	r1, #64
	ldr	r2, .L73
	mov	r3, r4
	bl	snprintf
	mov	r0, sp
	bl	Alert_Message
.L72:
	mov	r0, #100
	bl	vTaskDelay
	b	.L72
.L74:
	.align	2
.L73:
	.word	.LC2
.LFE3:
	.size	vApplicationStackOverflowHook, .-vApplicationStackOverflowHook
	.section	.text.vApplicationMallocFailedHook,"ax",%progbits
	.align	2
	.global	vApplicationMallocFailedHook
	.type	vApplicationMallocFailedHook, %function
vApplicationMallocFailedHook:
.LFB4:
	@ Volatile: function does not return.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.L76:
	b	.L76
.LFE4:
	.size	vApplicationMallocFailedHook, .-vApplicationMallocFailedHook
	.section	.text.vConfigureTimerForRunTimeStats,"ax",%progbits
	.align	2
	.global	vConfigureTimerForRunTimeStats
	.type	vConfigureTimerForRunTimeStats, %function
vConfigureTimerForRunTimeStats:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r1, #1
	str	lr, [sp, #-4]!
.LCFI5:
	mov	r0, #18
	bl	clkpwr_clk_en_dis
	ldr	r3, .L78
	ldr	r1, .L78+4
	mov	r2, #0
	str	r2, [r3, #4]
	str	r2, [r3, #112]
	str	r2, [r3, #40]
	str	r2, [r3, #60]
	str	r2, [r3, #8]
	str	r2, [r3, #16]
	str	r2, [r3, #24]
	str	r2, [r3, #28]
	str	r2, [r3, #32]
	str	r2, [r3, #36]
	str	r1, [r3, #12]
	str	r2, [r3, #20]
	mov	r2, #255
	str	r2, [r3, #0]
	mov	r2, #1
	str	r2, [r3, #4]
	ldr	pc, [sp], #4
.L79:
	.align	2
.L78:
	.word	1074135040
	.word	650
.LFE5:
	.size	vConfigureTimerForRunTimeStats, .-vConfigureTimerForRunTimeStats
	.section	.text.return_run_timer_counter_value,"ax",%progbits
	.align	2
	.global	return_run_timer_counter_value
	.type	return_run_timer_counter_value, %function
return_run_timer_counter_value:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L81
	ldr	r0, [r3, #8]
	bx	lr
.L82:
	.align	2
.L81:
	.word	1074135040
.LFE6:
	.size	return_run_timer_counter_value, .-return_run_timer_counter_value
	.section	.bss.lcounter,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	lcounter, %object
	.size	lcounter, 4
lcounter:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/main"
	.ascii	".c\000"
.LC1:
	.ascii	"Startup\000"
.LC2:
	.ascii	"Stack Overflow: %s\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x294
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/main.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xc2
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x174
	.byte	0x1
	.uleb128 0x3
	.4byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1b8
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x42e
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.4byte	0x73
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x4eb
	.byte	0x1
	.byte	0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x4fd
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x532
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x539
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x559
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 660
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF7:
	.ascii	"toggle_LED\000"
.LASF2:
	.ascii	"vApplicationMallocFailedHook\000"
.LASF1:
	.ascii	"vApplicationStackOverflowHook\000"
.LASF4:
	.ascii	"return_run_timer_counter_value\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/main"
	.ascii	".c\000"
.LASF3:
	.ascii	"vConfigureTimerForRunTimeStats\000"
.LASF0:
	.ascii	"vled_TASK\000"
.LASF9:
	.ascii	"vPortSaveVFPRegisters\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF8:
	.ascii	"main\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
