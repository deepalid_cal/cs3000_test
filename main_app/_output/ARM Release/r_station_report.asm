	.file	"r_station_report.c"
	.text
.Ltext0:
	.section	.text.FDTO_STATION_REPORT_redraw_scrollbox,"ax",%progbits
	.align	2
	.global	FDTO_STATION_REPORT_redraw_scrollbox
	.type	FDTO_STATION_REPORT_redraw_scrollbox, %function
FDTO_STATION_REPORT_redraw_scrollbox:
.LFB1:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
.LBB4:
	ldr	r3, .L4
.LBE4:
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI0:
.LBB5:
	ldr	r7, .L4+4
.LBE5:
	sub	sp, sp, #28
.LCFI1:
.LBB6:
	ldr	r2, .L4+8
	ldr	r0, [r3, #0]
	mov	r1, #400
	mov	r3, #57
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, .L4+12
	ldr	r1, [r7, #0]
	ldr	r0, [r4, #0]
	sub	r1, r1, #1
	bl	nm_STATION_get_pointer_to_station
	mov	r6, r0
	bl	nm_GROUP_get_name
	cmp	r0, #0
	ldreq	r3, .L4+16
	streqb	r0, [r3, #0]
	beq	.L3
	mov	r1, #0
	ldr	r0, .L4+20
	bl	GuiLib_GetTextPtr
	ldr	r4, [r4, #0]
	ldr	r1, [r7, #0]
	add	r2, sp, #12
	sub	r1, r1, #1
	mov	r3, #16
	mov	r5, r0
	mov	r0, r4
	bl	STATION_get_station_number_string
	add	r4, r4, #65
	mov	r7, r0
	mov	r0, r6
	bl	nm_GROUP_get_name
	mov	r1, #49
	ldr	r2, .L4+24
	mov	r3, r5
	stmia	sp, {r4, r7}
	str	r0, [sp, #8]
	ldr	r0, .L4+16
	bl	snprintf
.L3:
	ldr	r3, .L4
.LBE6:
	ldr	r5, .L4+28
.LBB7:
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE7:
	ldr	r3, .L4+4
	ldr	r1, [r3, #0]
	ldr	r3, .L4+12
	sub	r1, r1, #1
	ldr	r0, [r3, #0]
	bl	STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines
	ldr	r2, [r5, #0]
	mov	r4, r0
	subs	r2, r4, r2
	mov	r0, #0
	mov	r1, r4
	movne	r2, #1
	bl	FDTO_SCROLL_BOX_redraw_retaining_topline
	str	r4, [r5, #0]
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L5:
	.align	2
.L4:
	.word	list_program_data_recursive_MUTEX
	.word	GuiVar_StationInfoNumber
	.word	.LC0
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_StationDescription
	.word	1058
	.word	.LC1
	.word	.LANCHOR0
.LFE1:
	.size	FDTO_STATION_REPORT_redraw_scrollbox, .-FDTO_STATION_REPORT_redraw_scrollbox
	.section	.text.FDTO_STATION_REPORT_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_STATION_REPORT_draw_report
	.type	FDTO_STATION_REPORT_draw_report, %function
FDTO_STATION_REPORT_draw_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI2:
	ldr	r6, .L8
	mvn	r1, #0
	mov	r2, #1
	mov	r4, r0
	mov	r0, #101
	bl	GuiLib_ShowScreen
	ldr	r3, .L8+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L8+8
	mov	r3, #120
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r6, #0]
	cmp	r3, #0
	bne	.L7
	ldr	r5, .L8+12
	ldr	r3, [r5, #0]
	cmp	r3, #0
	bne	.L7
.LBB8:
	bl	STATION_get_first_available_station
	mov	r7, r0
	bl	nm_STATION_get_box_index_0
	str	r0, [r6, #0]
	mov	r0, r7
	bl	nm_STATION_get_station_number_0
	add	r0, r0, #1
	str	r0, [r5, #0]
.L7:
.LBE8:
	ldr	r3, .L8+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L8+12
	ldr	r1, [r3, #0]
	ldr	r3, .L8
	sub	r1, r1, #1
	ldr	r0, [r3, #0]
	bl	STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines
	ldr	r3, .L8+16
	ldr	r2, .L8+20
	mov	r1, r0
	str	r0, [r3, #0]
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	FDTO_REPORTS_draw_report
.L9:
	.align	2
.L8:
	.word	GuiVar_StationInfoBoxIndex
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_StationInfoNumber
	.word	.LANCHOR0
	.word	STATION_REPORT_draw_scroll_line
.LFE2:
	.size	FDTO_STATION_REPORT_draw_report, .-FDTO_STATION_REPORT_draw_report
	.section	.text.STATION_REPORT_process_report,"ax",%progbits
	.align	2
	.global	STATION_REPORT_process_report
	.type	STATION_REPORT_process_report, %function
STATION_REPORT_process_report:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r4, lr}
.LCFI3:
	mov	r4, r0
	sub	sp, sp, #36
.LCFI4:
	beq	.L12
	cmp	r0, #20
	bne	.L16
.L12:
	bl	good_key_beep
	cmp	r4, #20
	ldr	r0, .L17
	ldr	r1, .L17+4
	bne	.L13
	bl	STATION_get_next_available_station
	b	.L14
.L13:
	bl	STATION_get_prev_available_station
.L14:
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L17+8
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	b	.L10
.L16:
	mov	r2, #20
	mov	r3, #300
	bl	REPORTS_process_report
.L10:
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L18:
	.align	2
.L17:
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_StationInfoNumber
	.word	FDTO_STATION_REPORT_redraw_scrollbox
.LFE3:
	.size	STATION_REPORT_process_report, .-STATION_REPORT_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_station_report.c\000"
.LC1:
	.ascii	"%s %c %s: %s\000"
	.section	.bss.g_STATION_REPORT_line_count,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_STATION_REPORT_line_count, %object
	.size	g_STATION_REPORT_line_count, 4
g_STATION_REPORT_line_count:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_station_report.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x62
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x31
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x58
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x73
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x9f
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"STATION_REPORT_process_report\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_station_report.c\000"
.LASF0:
	.ascii	"FDTO_STATION_REPORT_redraw_scrollbox\000"
.LASF1:
	.ascii	"FDTO_STATION_REPORT_draw_report\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"FDTO_STATION_REPORT_load_station_name_to_guivar\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
