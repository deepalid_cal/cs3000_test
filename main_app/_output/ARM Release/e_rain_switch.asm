	.file	"e_rain_switch.c"
	.text
.Ltext0:
	.section	.text.FDTO_RAIN_SWITCH_enter_rain_switch_scrollbox,"ax",%progbits
	.align	2
	.type	FDTO_RAIN_SWITCH_enter_rain_switch_scrollbox, %function
FDTO_RAIN_SWITCH_enter_rain_switch_scrollbox:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	bl	good_key_beep
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	ldr	r3, .L2
	ldr	r1, .L2+4
	ldrsh	r2, [r3, #0]
	mov	r0, #1
	mov	r3, #0
	bl	GuiLib_ScrollBox_Init
	mov	r0, #1
	bl	GuiLib_Cursor_Select
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	RAIN_SWITCH_populate_rain_switch_scrollbox
.LFE2:
	.size	FDTO_RAIN_SWITCH_enter_rain_switch_scrollbox, .-FDTO_RAIN_SWITCH_enter_rain_switch_scrollbox
	.section	.text.RAIN_SWITCH_populate_rain_switch_scrollbox,"ax",%progbits
	.align	2
	.type	RAIN_SWITCH_populate_rain_switch_scrollbox, %function
RAIN_SWITCH_populate_rain_switch_scrollbox:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L8
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	ldr	r6, .L8+4
	mov	r5, r0, asr #16
	add	r3, r3, r5, asl #3
	ldr	r0, [r3, #4]
	ldr	r3, .L8+8
	mov	r4, #92
	mla	r4, r0, r4, r3
	ldr	r1, .L8+12
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	mov	r3, #79
	ldr	r0, [r6, #0]
	mov	r1, #400
	ldr	r2, .L8+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r4, #36]
	cmp	r3, #0
	beq	.L5
	ldr	r3, [r4, #32]
	cmp	r3, #0
	ldrne	r0, .L8+12
	ldrne	r1, .L8+20
	bne	.L7
.L5:
	ldrb	r3, [r4, #24]	@ zero_extendqisi2
	and	r3, r3, #3
	cmp	r3, #3
	bne	.L6
	ldr	r0, .L8+12
	ldr	r1, .L8+24
.L7:
	mov	r2, #49
	bl	strlcat
.L6:
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L8
	ldr	r2, [r3, r5, asl #3]
	ldr	r3, .L8+28
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, pc}
.L9:
	.align	2
.L8:
	.word	.LANCHOR1
	.word	chain_members_recursive_MUTEX
	.word	chain+28
	.word	GuiVar_RainSwitchControllerName
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	GuiVar_RainSwitchSelected
.LFE0:
	.size	RAIN_SWITCH_populate_rain_switch_scrollbox, .-RAIN_SWITCH_populate_rain_switch_scrollbox
	.section	.text.FDTO_RAIN_SWITCH_leave_rain_switch_scrollbox,"ax",%progbits
	.align	2
	.type	FDTO_RAIN_SWITCH_leave_rain_switch_scrollbox, %function
FDTO_RAIN_SWITCH_leave_rain_switch_scrollbox:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI2:
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	ldr	r3, .L11
	ldr	r1, .L11+4
	ldrsh	r2, [r3, #0]
	mov	r0, #1
	mvn	r3, #0
	bl	GuiLib_ScrollBox_Init
	mov	r0, #1
	ldr	lr, [sp], #4
	b	FDTO_Cursor_Up
.L12:
	.align	2
.L11:
	.word	.LANCHOR0
	.word	RAIN_SWITCH_populate_rain_switch_scrollbox
.LFE3:
	.size	FDTO_RAIN_SWITCH_leave_rain_switch_scrollbox, .-FDTO_RAIN_SWITCH_leave_rain_switch_scrollbox
	.section	.text.FDTO_RAIN_SWITCH_show_switch_connected_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_RAIN_SWITCH_show_switch_connected_dropdown, %function
FDTO_RAIN_SWITCH_show_switch_connected_dropdown:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	mov	r5, r0
	mov	r4, r1
	mov	r0, #1
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L14
	mov	r1, r4
	str	r0, [r3, #0]
	ldr	r3, .L14+4
	mov	r0, r5
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L15:
	.align	2
.L14:
	.word	.LANCHOR2
	.word	.LANCHOR3
.LFE5:
	.size	FDTO_RAIN_SWITCH_show_switch_connected_dropdown, .-FDTO_RAIN_SWITCH_show_switch_connected_dropdown
	.section	.text.FDTO_RAIN_SWITCH_show_rain_switch_in_use_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_RAIN_SWITCH_show_rain_switch_in_use_dropdown, %function
FDTO_RAIN_SWITCH_show_rain_switch_in_use_dropdown:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L17
	mov	r0, #184
	ldr	r3, [r3, #0]
	mov	r1, #18
	ldr	r2, [r3, #0]
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L18:
	.align	2
.L17:
	.word	.LANCHOR3
.LFE4:
	.size	FDTO_RAIN_SWITCH_show_rain_switch_in_use_dropdown, .-FDTO_RAIN_SWITCH_show_rain_switch_in_use_dropdown
	.section	.text.FDTO_RAIN_SWITCH_close_rain_switch_connected_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_RAIN_SWITCH_close_rain_switch_connected_dropdown, %function
FDTO_RAIN_SWITCH_close_rain_switch_connected_dropdown:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI4:
	ldr	r4, .L20
	mov	r1, #0
	mov	r0, #1
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r6, [r4, #0]
	mov	r5, r0
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, [r4, #0]
	str	r0, [r6, #0]
	ldr	r6, [r3, #0]
	ldr	r3, .L20+4
	add	r5, r3, r5, asl #3
	ldr	r4, [r5, #4]
	bl	FLOWSENSE_get_controller_index
	mov	r5, r0
	mov	r0, #2
	bl	WEATHER_get_change_bits_ptr
	mov	r2, #1
	mov	r1, r4
	mov	r3, #2
	str	r5, [sp, #0]
	str	r2, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r6
	bl	nm_WEATHER_set_rain_switch_connected
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	FDTO_COMBOBOX_hide
.L21:
	.align	2
.L20:
	.word	.LANCHOR3
	.word	.LANCHOR1
.LFE6:
	.size	FDTO_RAIN_SWITCH_close_rain_switch_connected_dropdown, .-FDTO_RAIN_SWITCH_close_rain_switch_connected_dropdown
	.section	.text.FDTO_RAIN_SWITCH_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_RAIN_SWITCH_draw_screen
	.type	FDTO_RAIN_SWITCH_draw_screen, %function
FDTO_RAIN_SWITCH_draw_screen:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L33
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI5:
	ldrnesh	r1, [r3, #0]
	mov	r4, r0
	bne	.L24
	bl	WEATHER_copy_rain_switch_settings_into_GuiVars
	mov	r1, #0
.L24:
	mov	r0, #52
	mov	r2, #1
	bl	GuiLib_ShowScreen
.LBB4:
	ldr	r3, .L33+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L25
	cmp	r4, #1
	beq	.L26
	ldr	r3, .L33
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	ldrne	r3, .L33+8
	ldrne	r4, [r3, #0]
	bne	.L31
.L26:
	ldr	r3, .L33+12
	ldr	r4, .L33+16
	mov	r5, #0
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L33+20
	mov	r3, #118
	str	r5, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L33+24
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L33+20
	mov	r3, #120
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L33+20
	mov	r1, #124
	bl	COMM_MNGR_alert_if_chain_members_should_not_be_referenced
	ldr	r6, .L33+28
	ldr	r7, .L33+32
	mov	r8, r6
.L30:
	ldr	r3, [r6, #16]
	cmp	r3, #0
	beq	.L28
	mov	r3, #92
	mla	r3, r5, r3, r8
	ldrb	r2, [r3, #52]	@ zero_extendqisi2
	add	r3, r3, #28
	and	r2, r2, #3
	cmp	r2, #3
	beq	.L29
	ldr	r2, [r3, #32]
	cmp	r2, #0
	beq	.L28
	ldr	r3, [r3, #36]
	cmp	r3, #0
	beq	.L28
.L29:
	mov	r0, r5
	ldr	sl, [r4, #0]
	bl	WEATHER_get_rain_switch_connected_to_this_controller
	ldr	r3, [r4, #0]
	add	r2, r7, r3, asl #3
	add	r3, r3, #1
	str	r5, [r2, #4]
	str	r3, [r4, #0]
	str	r0, [r7, sl, asl #3]
.L28:
	add	r5, r5, #1
	cmp	r5, #12
	add	r6, r6, #92
	bne	.L30
	ldr	r3, .L33+24
	mvn	r4, #0
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L33+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L31:
	mov	r0, #1
	bl	GuiLib_ScrollBox_Close
	ldr	r2, .L33+16
	mov	r3, r4, asl #16
	mov	r0, #1
	ldr	r1, .L33+36
	ldrsh	r2, [r2, #0]
	mov	r3, r3, asr #16
	bl	GuiLib_ScrollBox_Init
.L25:
.LBE4:
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LBB5:
	b	GuiLib_Refresh
.L34:
	.align	2
.L33:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_RainSwitchInUse
	.word	.LANCHOR2
	.word	chain_members_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC0
	.word	weather_control_recursive_MUTEX
	.word	chain
	.word	.LANCHOR1
	.word	RAIN_SWITCH_populate_rain_switch_scrollbox
.LBE5:
.LFE7:
	.size	FDTO_RAIN_SWITCH_draw_screen, .-FDTO_RAIN_SWITCH_draw_screen
	.section	.text.RAIN_SWITCH_process_screen,"ax",%progbits
	.align	2
	.global	RAIN_SWITCH_process_screen
	.type	RAIN_SWITCH_process_screen, %function
RAIN_SWITCH_process_screen:
.LFB8:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L76
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI6:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #48
.LCFI7:
	cmp	r3, #740
	mov	r4, r0
	mov	r5, r1
	bne	.L64
	ldr	r3, .L76+4
	ldr	r1, [r3, #0]
	ldr	r3, .L76+8
	cmp	r1, r3
	beq	.L40
.L38:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L40
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #12]
	ldr	r3, .L76+12
	add	r0, sp, #12
	str	r3, [sp, #32]
	bl	Display_Post_Command
	mov	r0, #0
	b	.L69
.L40:
	bl	COMBO_BOX_key_press
	b	.L35
.L64:
	cmp	r0, #4
	beq	.L43
	bhi	.L46
	cmp	r0, #1
	beq	.L43
	bcc	.L42
	cmp	r0, #2
	beq	.L44
	cmp	r0, #3
	bne	.L41
	b	.L42
.L46:
	cmp	r0, #20
	beq	.L42
	bhi	.L47
	cmp	r0, #16
	bne	.L41
	b	.L43
.L47:
	cmp	r0, #80
	beq	.L45
	cmp	r0, #84
	bne	.L41
	b	.L45
.L44:
	ldr	r3, .L76+16
	ldrsh	r4, [r3, #0]
	cmp	r4, #0
	beq	.L49
	cmp	r4, #1
	bne	.L68
	b	.L73
.L49:
	bl	good_key_beep
	ldr	r3, .L76+4
	ldr	r2, .L76+8
	str	r2, [r3, #0]
	mov	r3, #1
	str	r3, [sp, #12]
	ldr	r3, .L76+20
	b	.L70
.L73:
	bl	good_key_beep
	mov	r0, r4
	mov	r1, #0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r2, .L76+24
	ldr	r3, .L76+4
	add	r2, r2, r0, asl #3
	str	r2, [r3, #0]
	mov	r3, #3
	str	r3, [sp, #12]
	ldr	r3, .L76+28
	cmp	r0, #0
	str	r3, [sp, #32]
	ldr	r3, .L76+32
	moveq	r0, #47
	str	r3, [sp, #36]
	movne	r3, r0, asl #1
	movne	r0, r0, asl #4
	rsbne	r0, r3, r0
	addne	r0, r0, #47
	str	r0, [sp, #40]
	b	.L72
.L45:
	ldr	r3, .L76+16
	ldrsh	r4, [r3, #0]
	cmp	r4, #0
	beq	.L53
	cmp	r4, #1
	bne	.L68
	b	.L74
.L53:
	ldr	r0, .L76+8
	bl	process_bool
	mov	r0, r4
.L69:
	bl	Redraw_Screen
	b	.L35
.L74:
	bl	good_key_beep
	mov	r1, #0
	mov	r0, r4
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L76+24
	ldr	r5, [r3, r0, asl #3]
	rsbs	r5, r5, #1
	movcc	r5, #0
	str	r5, [r3, r0, asl #3]
	add	r3, r3, r0, asl #3
	ldr	r6, [r3, #4]
	bl	FLOWSENSE_get_controller_index
	mov	r7, r0
	mov	r0, #2
	bl	WEATHER_get_change_bits_ptr
	mov	r1, r6
	mov	r2, r4
	mov	r3, #2
	str	r7, [sp, #0]
	str	r4, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r5
	bl	nm_WEATHER_set_rain_switch_connected
	mov	r0, r4
	bl	SCROLL_BOX_redraw
	b	.L35
.L43:
	ldr	r3, .L76+16
	ldrsh	r4, [r3, #0]
	cmp	r4, #1
	bne	.L68
	mov	r1, #0
	mov	r0, r4
	bl	GuiLib_ScrollBox_GetActiveLine
	cmp	r0, #0
	streq	r4, [sp, #12]
	ldreq	r3, .L76+36
	movne	r0, r4
	movne	r1, #4
	bne	.L71
	b	.L70
.L42:
	ldr	r3, .L76+16
	ldrsh	r0, [r3, #0]
	cmp	r0, #0
	beq	.L59
	cmp	r0, #1
	bne	.L68
	b	.L75
.L59:
	ldr	r3, .L76+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L68
	str	r3, [sp, #12]
	ldr	r3, .L76+40
.L70:
	str	r3, [sp, #32]
.L72:
	add	r0, sp, #12
	bl	Display_Post_Command
	b	.L35
.L75:
	mov	r1, #0
.L71:
	bl	SCROLL_BOX_up_or_down
	b	.L35
.L68:
	bl	bad_key_beep
	b	.L35
.L41:
	cmp	r4, #67
	bne	.L62
	bl	WEATHER_extract_and_store_rain_switch_changes_from_GuiVars
	ldr	r3, .L76+44
	mov	r2, #4
	str	r2, [r3, #0]
.L62:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L35:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L77:
	.align	2
.L76:
	.word	GuiLib_CurStructureNdx
	.word	.LANCHOR3
	.word	GuiVar_RainSwitchInUse
	.word	FDTO_RAIN_SWITCH_close_rain_switch_connected_dropdown
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_RAIN_SWITCH_show_rain_switch_in_use_dropdown
	.word	.LANCHOR1
	.word	FDTO_RAIN_SWITCH_show_switch_connected_dropdown
	.word	266
	.word	FDTO_RAIN_SWITCH_leave_rain_switch_scrollbox
	.word	FDTO_RAIN_SWITCH_enter_rain_switch_scrollbox
	.word	GuiVar_MenuScreenToShow
.LFE8:
	.size	RAIN_SWITCH_process_screen, .-RAIN_SWITCH_process_screen
	.section	.bss.g_RAIN_SWITCH_combo_box_guivar,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_RAIN_SWITCH_combo_box_guivar, %object
	.size	g_RAIN_SWITCH_combo_box_guivar, 4
g_RAIN_SWITCH_combo_box_guivar:
	.space	4
	.section	.bss.g_RAIN_SWITCH,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_RAIN_SWITCH, %object
	.size	g_RAIN_SWITCH, 96
g_RAIN_SWITCH:
	.space	96
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_rain_switch.c\000"
.LC1:
	.ascii	" (Weather Terminal - RainClick)\000"
.LC2:
	.ascii	" (POC Terminal - SW1)\000"
	.section	.bss.g_RAIN_SWITCH_count,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_RAIN_SWITCH_count, %object
	.size	g_RAIN_SWITCH_count, 4
g_RAIN_SWITCH_count:
	.space	4
	.section	.bss.g_RAIN_SWITCH_active_line,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_RAIN_SWITCH_active_line, %object
	.size	g_RAIN_SWITCH_active_line, 4
g_RAIN_SWITCH_active_line:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI3-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI4-.LFB6
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI5-.LFB7
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI6-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_rain_switch.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xc0
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0xa8
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x3f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0xb4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.byte	0xd1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.byte	0xcb
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0xd9
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xe7
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x10c
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST6
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB7
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB8
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI7
	.4byte	.LFE8
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF7:
	.ascii	"RAIN_SWITCH_process_screen\000"
.LASF2:
	.ascii	"FDTO_RAIN_SWITCH_leave_rain_switch_scrollbox\000"
.LASF4:
	.ascii	"FDTO_RAIN_SWITCH_show_rain_switch_in_use_dropdown\000"
.LASF5:
	.ascii	"FDTO_RAIN_SWITCH_close_rain_switch_connected_dropdo"
	.ascii	"wn\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF10:
	.ascii	"FDTO_RAIN_SWITCH_draw_rain_switch_scrollbox\000"
.LASF3:
	.ascii	"FDTO_RAIN_SWITCH_show_switch_connected_dropdown\000"
.LASF1:
	.ascii	"RAIN_SWITCH_populate_rain_switch_scrollbox\000"
.LASF6:
	.ascii	"FDTO_RAIN_SWITCH_draw_screen\000"
.LASF0:
	.ascii	"FDTO_RAIN_SWITCH_enter_rain_switch_scrollbox\000"
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_rain_switch.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
