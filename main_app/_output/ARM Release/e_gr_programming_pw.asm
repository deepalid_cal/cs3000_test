	.file	"e_gr_programming_pw.c"
	.text
.Ltext0:
	.section	.text.get_guivars_from_gr_programming_struct,"ax",%progbits
	.align	2
	.type	get_guivars_from_gr_programming_struct, %function
get_guivars_from_gr_programming_struct:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldr	r5, .L3
	ldr	r1, [r5, #0]
	cmp	r1, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r4, [r1, #164]
	cmp	r4, #0
	ldmeqfd	sp!, {r4, r5, pc}
	add	r1, r1, #200
	mov	r2, #49
	ldr	r0, .L3+4
	bl	strlcpy
	ldr	r1, [r5, #0]
	mov	r2, #49
	add	r1, r1, #232
	ldr	r0, .L3+8
	bl	strlcpy
	ldr	r1, [r5, #0]
	mov	r2, #49
	add	r1, r1, #247
	ldr	r0, .L3+12
	bl	strlcpy
	mov	r1, r4
	mov	r2, #49
	ldr	r0, .L3+16
	bl	strlcpy
	add	r1, r4, #16
	mov	r2, #49
	ldr	r0, .L3+20
	bl	strlcpy
	add	r1, r4, #47
	mov	r2, #49
	ldr	r0, .L3+24
	bl	strlcpy
	add	r1, r4, #78
	mov	r2, #49
	ldr	r0, .L3+28
	bl	strlcpy
	mov	r2, #49
	add	r1, r4, #109
	ldr	r0, .L3+32
	bl	strlcpy
	ldr	r0, .L3+4
	mov	r1, #49
	bl	e_SHARED_string_validation
	ldr	r0, .L3+8
	mov	r1, #49
	bl	e_SHARED_string_validation
	ldr	r0, .L3+12
	mov	r1, #49
	bl	e_SHARED_string_validation
	ldr	r0, .L3+16
	mov	r1, #49
	bl	e_SHARED_string_validation
	ldr	r0, .L3+20
	mov	r1, #49
	bl	e_SHARED_string_validation
	ldr	r0, .L3+24
	mov	r1, #49
	bl	e_SHARED_string_validation
	ldr	r0, .L3+28
	mov	r1, #49
	bl	e_SHARED_string_validation
	ldr	r0, .L3+32
	mov	r1, #49
	ldmfd	sp!, {r4, r5, lr}
	b	e_SHARED_string_validation
.L4:
	.align	2
.L3:
	.word	dev_state
	.word	GuiVar_GRModel
	.word	GuiVar_GRFirmwareVersion
	.word	GuiVar_GRRadioSerialNum
	.word	GuiVar_GRIPAddress
	.word	GuiVar_GRSIMState
	.word	GuiVar_GRNetworkState
	.word	GuiVar_GRGPRSStatus
	.word	GuiVar_GRRSSI
.LFE0:
	.size	get_guivars_from_gr_programming_struct, .-get_guivars_from_gr_programming_struct
	.section	.text.FDTO_GR_PROGRAMMING_PW_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_GR_PROGRAMMING_PW_draw_screen
	.type	FDTO_GR_PROGRAMMING_PW_draw_screen, %function
FDTO_GR_PROGRAMMING_PW_draw_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L9
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	mov	r6, r1
	mov	r4, r0
	ldrnesh	r1, [r3, #0]
	bne	.L7
.LBB4:
	ldr	r3, .L9+4
	ldr	r5, .L9+8
	ldr	r1, .L9+12
	mov	r2, #49
	ldr	r0, .L9+16
	str	r3, [r5, #0]
	bl	strlcpy
	ldr	r1, .L9+12
	mov	r2, #49
	ldr	r0, .L9+20
	bl	strlcpy
	ldr	r1, .L9+12
	mov	r2, #49
	ldr	r0, .L9+24
	bl	strlcpy
	ldr	r1, .L9+12
	mov	r2, #49
	ldr	r0, .L9+28
	bl	strlcpy
	ldr	r1, .L9+12
	mov	r2, #49
	ldr	r0, .L9+32
	bl	strlcpy
	ldr	r1, .L9+12
	mov	r2, #49
	ldr	r0, .L9+36
	bl	strlcpy
	ldr	r1, .L9+12
	mov	r2, #49
	ldr	r0, .L9+40
	bl	strlcpy
	ldr	r1, .L9+12
	mov	r2, #49
	ldr	r0, .L9+44
	bl	strlcpy
	ldr	r1, .L9+12
	mov	r2, #49
	ldr	r0, .L9+48
	bl	strlcpy
.LBE4:
	ldr	r3, .L9+52
	ldr	r0, .L9+56
	str	r6, [r3, #0]
	bl	e_SHARED_index_keycode_for_gui
	mov	r1, #0
	str	r0, [r5, #0]
.L7:
	mov	r0, #25
	mov	r2, #1
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
	cmp	r4, #1
	ldmnefd	sp!, {r4, r5, r6, pc}
	bl	DEVICE_EXCHANGE_draw_dialog
	ldr	r3, .L9+52
	ldr	r2, .L9+60
	ldr	r0, [r3, #0]
	mov	r1, #82
	ldmfd	sp!, {r4, r5, r6, lr}
	b	e_SHARED_start_device_communication
.L10:
	.align	2
.L9:
	.word	GuiLib_ActiveCursorFieldNo
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	.LC0
	.word	GuiVar_GRModel
	.word	GuiVar_GRFirmwareVersion
	.word	GuiVar_GRRadioSerialNum
	.word	GuiVar_GRIPAddress
	.word	GuiVar_GRSIMState
	.word	GuiVar_GRNetworkState
	.word	GuiVar_GRGPRSStatus
	.word	GuiVar_GRRSSI
	.word	GuiVar_CommOptionInfoText
	.word	.LANCHOR0
	.word	36867
	.word	.LANCHOR1
.LFE3:
	.size	FDTO_GR_PROGRAMMING_PW_draw_screen, .-FDTO_GR_PROGRAMMING_PW_draw_screen
	.section	.text.FDTO_GR_PROGRAMMING_PW_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_GR_PROGRAMMING_PW_process_device_exchange_key, %function
FDTO_GR_PROGRAMMING_PW_process_device_exchange_key:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	mov	r5, r0
	bl	e_SHARED_get_info_text_from_programming_struct
	sub	r3, r5, #36864
	cmp	r3, #6
	ldmhifd	sp!, {r4, r5, pc}
	mov	r2, #1
	mov	r3, r2, asl r3
	tst	r3, #73
	bne	.L13
	ands	r4, r3, #36
	bne	.L15
	tst	r3, #18
	ldmeqfd	sp!, {r4, r5, pc}
.LBB7:
	bl	get_guivars_from_gr_programming_struct
	ldr	r0, .L16
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L16+4
	str	r0, [r3, #0]
	bl	DIALOG_close_ok_dialog
	ldr	r3, .L16+8
	mov	r0, r4
	ldr	r1, [r3, #0]
.LBE7:
	ldmfd	sp!, {r4, r5, lr}
.LBB8:
	b	FDTO_GR_PROGRAMMING_PW_draw_screen
.L15:
.LBE8:
	bl	get_guivars_from_gr_programming_struct
.L13:
	mov	r0, r5
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L16+4
	str	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	DEVICE_EXCHANGE_draw_dialog
.L17:
	.align	2
.L16:
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	.LANCHOR0
.LFE2:
	.size	FDTO_GR_PROGRAMMING_PW_process_device_exchange_key, .-FDTO_GR_PROGRAMMING_PW_process_device_exchange_key
	.section	.text.GR_PROGRAMMING_PW_process_screen,"ax",%progbits
	.align	2
	.global	GR_PROGRAMMING_PW_process_screen
	.type	GR_PROGRAMMING_PW_process_screen, %function
GR_PROGRAMMING_PW_process_screen:
.LFB4:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	mov	r3, r0
	sub	sp, sp, #36
.LCFI4:
	mov	r2, r1
	beq	.L20
	bhi	.L26
	cmp	r0, #1
	beq	.L21
	bcc	.L20
	cmp	r0, #2
	beq	.L22
	cmp	r0, #3
	bne	.L19
	b	.L34
.L26:
	cmp	r0, #84
	beq	.L20
	bhi	.L27
	cmp	r0, #67
	beq	.L24
	cmp	r0, #80
	bne	.L19
	b	.L20
.L27:
	sub	r1, r0, #36864
	cmp	r1, #6
	bhi	.L19
	ldr	r2, .L35
	ldr	r1, .L35+4
	str	r0, [sp, #24]
	cmp	r0, r1
	cmpne	r0, r2
	ldrne	r2, .L35+8
	movne	r1, #0
	strne	r1, [r2, #0]
	mov	r2, #2
	str	r2, [sp, #0]
	ldr	r2, .L35+12
	mov	r0, sp
	str	r2, [sp, #20]
	bl	Display_Post_Command
	b	.L18
.L22:
	ldr	r4, .L35+16
	ldr	r0, .L35
	ldr	r5, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r5, r0
	beq	.L18
	ldr	r0, .L35+4
	ldr	r4, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r4, r0
	beq	.L18
	ldr	r3, .L35+20
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L20
	ldr	r4, .L35+8
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L20
	bl	good_key_beep
	ldr	r3, .L35+24
	mov	r1, #82
	ldr	r0, [r3, #0]
	mov	r2, r4
	bl	e_SHARED_start_device_communication
	b	.L18
.L21:
	bl	CURSOR_Up
	b	.L18
.L34:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L18
.L20:
	bl	bad_key_beep
	b	.L18
.L24:
	ldr	r3, .L35+28
	mov	r2, #11
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	b	.L18
.L19:
	mov	r0, r3
	mov	r1, r2
	bl	KEY_process_global_keys
.L18:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, pc}
.L36:
	.align	2
.L35:
	.word	36867
	.word	36870
	.word	.LANCHOR1
	.word	FDTO_GR_PROGRAMMING_PW_process_device_exchange_key
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	GuiVar_MenuScreenToShow
.LFE4:
	.size	GR_PROGRAMMING_PW_process_screen, .-GR_PROGRAMMING_PW_process_screen
	.section	.bss.g_GR_PROGRAMMING_PW_port,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_GR_PROGRAMMING_PW_port, %object
	.size	g_GR_PROGRAMMING_PW_port, 4
g_GR_PROGRAMMING_PW_port:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"\000"
	.section	.bss.GR_PROGRAMMING_PW_querying_device,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	GR_PROGRAMMING_PW_querying_device, %object
	.size	GR_PROGRAMMING_PW_querying_device, 4
GR_PROGRAMMING_PW_querying_device:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_gr_programming_pw.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x7a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x73
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x1
	.byte	0x3f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x61
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xaa
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	0x21
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xd2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"GR_PROGRAMMING_PW_process_screen\000"
.LASF1:
	.ascii	"GR_PROGRAMMING_initialize_guivars\000"
.LASF0:
	.ascii	"FDTO_GR_PROGRAMMING_PW_process_device_exchange_key\000"
.LASF6:
	.ascii	"get_guivars_from_gr_programming_struct\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_gr_programming_pw.c\000"
.LASF2:
	.ascii	"FDTO_GR_PROGRAMMING_PW_draw_screen\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
