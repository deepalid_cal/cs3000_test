	.file	"e_line_fill_times.c"
	.text
.Ltext0:
	.section	.text.FDTO_LINE_FILL_TIME_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_LINE_FILL_TIME_draw_screen
	.type	FDTO_LINE_FILL_TIME_draw_screen, %function
FDTO_LINE_FILL_TIME_draw_screen:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L4
	str	lr, [sp, #-4]!
.LCFI0:
	ldrnesh	r1, [r3, #0]
	bne	.L3
	bl	LINE_FILL_TIME_copy_group_into_guivars
	mov	r1, #0
.L3:
	mov	r0, #33
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L5:
	.align	2
.L4:
	.word	GuiLib_ActiveCursorFieldNo
.LFE0:
	.size	FDTO_LINE_FILL_TIME_draw_screen, .-FDTO_LINE_FILL_TIME_draw_screen
	.section	.text.LINE_FILL_TIME_process_screen,"ax",%progbits
	.align	2
	.global	LINE_FILL_TIME_process_screen
	.type	LINE_FILL_TIME_process_screen, %function
LINE_FILL_TIME_process_screen:
.LFB1:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI1:
	mov	r4, r1
	beq	.L9
	bhi	.L12
	cmp	r0, #1
	beq	.L9
	bcc	.L8
	cmp	r0, #3
	beq	.L8
	cmp	r0, #4
	bne	.L7
	b	.L9
.L12:
	cmp	r0, #67
	beq	.L10
	bhi	.L13
	cmp	r0, #20
	bne	.L7
	b	.L8
.L13:
	cmp	r0, #80
	beq	.L11
	cmp	r0, #84
	bne	.L7
.L11:
	ldr	r3, .L28
	ldrsh	r3, [r3, #0]
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L14
.L25:
	.word	.L15
	.word	.L16
	.word	.L17
	.word	.L18
	.word	.L19
	.word	.L20
	.word	.L21
	.word	.L22
	.word	.L23
	.word	.L24
.L15:
	mov	r1, #1
	mov	r3, #0
	stmia	sp, {r1, r3}
	ldr	r1, .L28+4
	b	.L27
.L16:
	ldr	r1, .L28+8
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
.L27:
	mov	r2, #15
	ldr	r3, .L28+12
	bl	process_uns32
	b	.L26
.L17:
	mov	r3, #1
	mov	lr, #0
	stmia	sp, {r3, lr}
	ldr	r1, .L28+16
	b	.L27
.L18:
	mov	r3, #1
	mov	ip, #0
	stmia	sp, {r3, ip}
	ldr	r1, .L28+20
	b	.L27
.L19:
	mov	r1, #1
	mov	r3, #0
	stmia	sp, {r1, r3}
	ldr	r1, .L28+24
	b	.L27
.L20:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L28+28
	b	.L27
.L21:
	mov	r3, #1
	mov	lr, #0
	stmia	sp, {r3, lr}
	ldr	r1, .L28+32
	b	.L27
.L22:
	mov	r3, #1
	mov	ip, #0
	stmia	sp, {r3, ip}
	ldr	r1, .L28+36
	b	.L27
.L23:
	mov	r1, #1
	mov	r3, #0
	stmia	sp, {r1, r3}
	ldr	r1, .L28+40
	b	.L27
.L24:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L28+44
	b	.L27
.L14:
	bl	bad_key_beep
.L26:
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	Refresh_Screen
.L9:
	mov	r0, #1
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Up
.L8:
	mov	r0, #1
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Down
.L10:
	ldr	r3, .L28+48
	mov	r2, #2
	str	r2, [r3, #0]
	str	r0, [sp, #8]
	bl	LINE_FILL_TIME_extract_and_store_changes_from_GuiVars
	ldr	r0, [sp, #8]
.L7:
	mov	r1, r4
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	KEY_process_global_keys
.L29:
	.align	2
.L28:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	1800
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_MenuScreenToShow
.LFE1:
	.size	LINE_FILL_TIME_process_screen, .-LINE_FILL_TIME_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_line_fill_times.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x46
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x29
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x40
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"FDTO_LINE_FILL_TIME_draw_screen\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_line_fill_times.c\000"
.LASF1:
	.ascii	"LINE_FILL_TIME_process_screen\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
