	.file	"app_startup.c"
	.text
.Ltext0:
	.section	.text.elevate_priority_of_ISP_related_code_update_tasks,"ax",%progbits
	.align	2
	.global	elevate_priority_of_ISP_related_code_update_tasks
	.type	elevate_priority_of_ISP_related_code_update_tasks, %function
elevate_priority_of_ISP_related_code_update_tasks:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L3
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldr	r0, [r3, #0]
	cmp	r0, #0
	beq	.L2
	ldr	r5, .L3+4
	ldr	r3, [r5, #0]
	cmp	r3, #0
	beq	.L2
	ldr	r4, .L3+8
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L2
	mov	r1, #12
	bl	vTaskPrioritySet
	ldr	r0, [r5, #0]
	mov	r1, #12
	bl	vTaskPrioritySet
	ldr	r0, [r4, #0]
	mov	r1, #12
	ldmfd	sp!, {r4, r5, lr}
	b	vTaskPrioritySet
.L2:
	ldr	r0, .L3+12
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_Message
.L4:
	.align	2
.L3:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LC0
.LFE0:
	.size	elevate_priority_of_ISP_related_code_update_tasks, .-elevate_priority_of_ISP_related_code_update_tasks
	.section	.text.restore_priority_of_ISP_related_code_update_tasks,"ax",%progbits
	.align	2
	.global	restore_priority_of_ISP_related_code_update_tasks
	.type	restore_priority_of_ISP_related_code_update_tasks, %function
restore_priority_of_ISP_related_code_update_tasks:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L7
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r0, [r3, #0]
	cmp	r0, #0
	beq	.L6
	ldr	r5, .L7+4
	ldr	r3, [r5, #0]
	cmp	r3, #0
	beq	.L6
	ldr	r4, .L7+8
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L6
	mov	r1, #5
	bl	vTaskPrioritySet
	ldr	r0, [r5, #0]
	mov	r1, #5
	bl	vTaskPrioritySet
	ldr	r0, [r4, #0]
	mov	r1, #3
	ldmfd	sp!, {r4, r5, lr}
	b	vTaskPrioritySet
.L6:
	ldr	r0, .L7+12
	ldmfd	sp!, {r4, r5, lr}
	b	Alert_Message
.L8:
	.align	2
.L7:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LC0
.LFE1:
	.size	restore_priority_of_ISP_related_code_update_tasks, .-restore_priority_of_ISP_related_code_update_tasks
	.section	.text.system_startup_task,"ax",%progbits
	.align	2
	.global	system_startup_task
	.type	system_startup_task, %function
system_startup_task:
.LFB4:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	ldr	r5, .L72
	sub	sp, sp, #20
.LCFI3:
	mov	r4, #0
	mov	r3, #15
	mov	r2, #2048
	ldr	r1, .L72+4
	str	r3, [sp, #0]
	ldr	r0, .L72+8
	mov	r3, r4
	str	r5, [sp, #4]
	str	r4, [sp, #8]
	str	r4, [sp, #12]
	bl	xTaskGenericCreate
	ldr	r0, .L72+12
	bl	vPortSaveVFPRegisters
	ldr	r0, [r5, #0]
	ldr	r1, .L72+12
	bl	vTaskSetApplicationTaskTag
	ldr	r0, [r5, #0]
	mov	r1, #1
	bl	vTaskPrioritySet
	mov	r0, #1
	mov	r1, r4
	mov	r2, #3
	bl	xQueueGenericCreate
	ldr	r5, .L72+16
	cmp	r0, r4
	str	r0, [r5, #0]
	beq	.L10
	mov	r1, r4
	mov	r2, r4
	mov	r3, r4
	bl	xQueueGenericSend
.L10:
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	ldr	r0, [r5, #0]
	bl	xQueueGenericReceive
	ldr	r6, .L72+20
	mov	r4, #0
	mov	r3, #14
	mov	r2, #2048
	ldr	r1, .L72+24
	str	r3, [sp, #0]
	ldr	r0, .L72+28
	mov	r3, r4
	str	r6, [sp, #4]
	str	r4, [sp, #8]
	str	r4, [sp, #12]
	bl	xTaskGenericCreate
	ldr	r0, .L72+32
	bl	vPortSaveVFPRegisters
	ldr	r0, [r6, #0]
	ldr	r1, .L72+32
	bl	vTaskSetApplicationTaskTag
	mvn	r2, #0
	mov	r3, r4
	mov	r1, r4
	ldr	r0, [r5, #0]
	bl	xQueueGenericReceive
	bl	xTimerGetTimerDaemonTaskHandle
	ldr	r5, .L72+36
	str	r0, [r5, #0]
	ldr	r0, .L72+40
	bl	vPortSaveVFPRegisters
	ldr	r0, [r5, #0]
	ldr	r1, .L72+40
	bl	vTaskSetApplicationTaskTag
	ldr	r5, .L72+44
	mov	r3, #15
	mov	r2, #2048
	ldr	r1, .L72+48
	str	r3, [sp, #0]
	ldr	r0, .L72+52
	mov	r3, r4
	str	r5, [sp, #4]
	str	r4, [sp, #8]
	str	r4, [sp, #12]
	bl	xTaskGenericCreate
	ldr	r0, .L72+56
	bl	vPortSaveVFPRegisters
	ldr	r0, [r5, #0]
	ldr	r1, .L72+56
	bl	vTaskSetApplicationTaskTag
.LBB7:
	mov	r0, #1
	bl	xQueueCreateMutex
	ldr	r3, .L72+60
	cmp	r0, r4
	str	r0, [r3, #0]
	bne	.L11
	ldr	r0, .L72+64
	ldr	r1, .L72+68
	ldr	r2, .L72+72
	bl	__assert
.L11:
	ldr	r6, .L72+76
	ldr	r5, .L72+80
.LBE7:
	mov	r4, #0
.L13:
.LBB8:
	mov	r0, #1
	mov	r1, #0
	mov	r2, #3
	bl	xQueueGenericCreate
	cmp	r0, #0
	str	r0, [r4, r6]
	beq	.L12
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
.L12:
	mov	r0, #1
	bl	xQueueCreateMutex
	str	r0, [r5, r4]
	add	r4, r4, #4
	cmp	r4, #20
	bne	.L13
	mov	r0, #1
	bl	xQueueCreateMutex
	ldr	r3, .L72+84
.LBE8:
.LBB9:
	ldr	r6, .L72+88
	ldr	r5, .L72+92
	mov	r4, #0
.LBE9:
.LBB10:
	str	r0, [r3, #0]
	mov	r0, #1
	bl	xQueueCreateMutex
	ldr	r3, .L72+96
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+100
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+104
	str	r0, [r3, #0]
	mov	r0, #1
	bl	xQueueCreateMutex
	ldr	r3, .L72+108
	str	r0, [r3, #0]
	mov	r0, #1
	bl	xQueueCreateMutex
	ldr	r3, .L72+112
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+116
	str	r0, [r3, #0]
	mov	r0, #1
	bl	xQueueCreateMutex
	ldr	r3, .L72+120
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+124
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+128
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+132
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+136
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+140
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+144
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+148
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+152
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+156
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+160
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+164
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+168
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+172
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+176
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+180
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+184
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+188
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+192
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+196
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+200
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+204
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+208
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+212
	str	r0, [r3, #0]
	mov	r0, #1
	bl	xQueueCreateMutex
	ldr	r3, .L72+216
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+220
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+224
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+228
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+232
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+236
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+240
	str	r0, [r3, #0]
	mov	r0, #1
	bl	xQueueCreateMutex
	ldr	r3, .L72+244
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+248
	str	r0, [r3, #0]
	mov	r0, #4
	bl	xQueueCreateMutex
	ldr	r3, .L72+252
	str	r0, [r3, #0]
	mov	r0, #1
	bl	xQueueCreateMutex
	ldr	r3, .L72+256
.LBE10:
.LBB11:
	mov	r1, #8
	mov	r2, #0
.LBE11:
.LBB12:
	str	r0, [r3, #0]
.LBE12:
.LBB13:
	mov	r0, #10
	bl	xQueueGenericCreate
	ldr	r3, .L72+260
	str	r0, [r3, #0]
.L14:
	mov	r0, #170
	mov	r1, #4
	mov	r2, #0
	bl	xQueueGenericCreate
	str	r0, [r6, r4]
	add	r4, r4, #4224
	add	r4, r4, #56
	cmp	r4, r5
	bne	.L14
	mov	r1, #40
	mov	r2, #0
	mov	r0, #10
	bl	xQueueGenericCreate
	ldr	r3, .L72+264
	mov	r1, #8
	mov	r2, #0
.LBE13:
	ldr	r4, .L72+268
.LBB14:
	str	r0, [r3, #0]
	mov	r0, #25
	bl	xQueueGenericCreate
	ldr	r3, .L72+272
	mov	r1, #28
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #50
	bl	xQueueGenericCreate
	ldr	r3, .L72+276
	mov	r1, #28
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #50
	bl	xQueueGenericCreate
	ldr	r3, .L72+280
	mov	r1, #36
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #25
	bl	xQueueGenericCreate
	ldr	r3, .L72+284
	mov	r1, #20
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #30
	bl	xQueueGenericCreate
	ldr	r3, .L72+288
	mov	r1, #36
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #30
	bl	xQueueGenericCreate
	ldr	r3, .L72+292
	mov	r1, #40
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #30
	bl	xQueueGenericCreate
	ldr	r3, .L72+296
	mov	r1, #24
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #30
	bl	xQueueGenericCreate
	ldr	r3, .L72+300
	mov	r1, #20
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #15
	bl	xQueueGenericCreate
	ldr	r3, .L72+304
	mov	r1, #20
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #30
	bl	xQueueGenericCreate
	ldr	r3, .L72+308
	mov	r1, #32
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #10
	bl	xQueueGenericCreate
	ldr	r3, .L72+312
	mov	r1, #4
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #2
	bl	xQueueGenericCreate
	ldr	r3, .L72+316
	mov	r1, #4
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #15
	bl	xQueueGenericCreate
	ldr	r3, .L72+320
	mov	r1, #4
	mov	r2, #0
	str	r0, [r3, #0]
	mov	r0, #512
	bl	xQueueGenericCreate
	ldr	r3, .L72+324
.LBE14:
	mov	r2, #0
	ldr	r1, .L72+328
.LBB15:
	str	r0, [r3, #0]
.LBE15:
	ldr	r3, .L72+332
	ldr	r0, .L72+336
	str	r3, [sp, #0]
	mov	r3, r2
	bl	xTimerCreate
	ldr	r3, .L72+340
	str	r0, [r3, #64]
	mov	r0, #0
	bl	ALERTS_test_all_piles
	mov	r0, #0
	bl	init_restart_info
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L15
	mov	r0, #0
	bl	vTaskSuspend
.L15:
	bl	GPIO_SETUP_learn_and_set_board_hardware_id
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L16
	mov	r0, #0
	bl	vTaskSuspend
.L16:
	bl	FLASH_STORAGE_create_delayed_file_save_timers
	ldr	r4, .L72+268
	ldr	r3, .L72+344
	mov	r2, #32
	str	r2, [r3, #0]
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L17
	mov	r0, #0
	bl	vTaskSuspend
.L17:
	mov	r0, #0
	bl	init_FLASH_DRIVE_SSP_channel
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L18
	mov	r0, #0
	bl	vTaskSuspend
.L18:
	ldr	r4, .L72+268
	mov	r0, #1
	bl	init_FLASH_DRIVE_SSP_channel
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L19
	mov	r0, #0
	bl	vTaskSuspend
.L19:
	mov	r0, #0
	mov	r1, r0
	bl	init_FLASH_DRIVE_file_system
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L20
	mov	r0, #0
	bl	vTaskSuspend
.L20:
	ldr	r4, .L72+268
	mov	r0, #1
	mov	r1, #0
	bl	init_FLASH_DRIVE_file_system
	bl	init_chain_sync_vars
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L21
	mov	r0, #0
	bl	vTaskSuspend
.L21:
	bl	init_file_configuration_controller
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L22
	mov	r0, #0
	bl	vTaskSuspend
.L22:
	ldr	r4, .L72+268
	bl	init_file_flowsense
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L23
	mov	r0, #0
	bl	vTaskSuspend
.L23:
	bl	init_file_configuration_network
	bl	FLOWSENSE_get_controller_index
	bl	Alert_program_restart_idx
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L24
	mov	r0, #0
	bl	vTaskSuspend
.L24:
	ldr	r4, .L72+268
	bl	init_file_contrast_and_speaker_vol
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L25
	mov	r0, #0
	bl	vTaskSuspend
.L25:
	bl	init_lcd_2
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L26
	mov	r0, #0
	bl	vTaskSuspend
.L26:
	ldr	r4, .L72+268
	bl	init_speaker
	bl	init_a_to_d
	mov	r0, #1
	bl	CONFIG_device_discovery
	mov	r0, #2
	bl	CONFIG_device_discovery
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L27
	mov	r0, #0
	bl	vTaskSuspend
.L27:
	bl	init_irri_irri
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L28
	mov	r0, #0
	bl	vTaskSuspend
.L28:
	ldr	r4, .L72+268
	bl	init_irri_lights
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L29
	mov	r0, #0
	bl	vTaskSuspend
.L29:
	bl	init_file_system_report_records
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L30
	mov	r0, #0
	bl	vTaskSuspend
.L30:
	ldr	r4, .L72+268
	bl	init_file_poc_report_records
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L31
	mov	r0, #0
	bl	vTaskSuspend
.L31:
	bl	init_file_station_report_data
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L32
	mov	r0, #0
	bl	vTaskSuspend
.L32:
	ldr	r4, .L72+268
	bl	init_file_station_history
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L33
	mov	r0, #0
	bl	vTaskSuspend
.L33:
	bl	init_file_lights_report_data
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L34
	mov	r0, #0
	bl	vTaskSuspend
.L34:
	ldr	r4, .L72+268
	bl	init_file_budget_report_records
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L35
	mov	r0, #0
	bl	vTaskSuspend
.L35:
	bl	init_file_tpmicro_data
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L36
	mov	r0, #0
	bl	vTaskSuspend
.L36:
	ldr	r4, .L72+268
	bl	init_file_weather_control
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L37
	mov	r0, #0
	bl	vTaskSuspend
.L37:
	bl	init_file_weather_tables
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L38
	mov	r0, #0
	bl	vTaskSuspend
.L38:
	ldr	r4, .L72+268
	bl	init_file_manual_programs
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L39
	mov	r0, #0
	bl	vTaskSuspend
.L39:
	bl	init_file_irrigation_system
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L40
	mov	r0, #0
	bl	vTaskSuspend
.L40:
	ldr	r4, .L72+268
	bl	init_file_POC
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L41
	mov	r0, #0
	bl	vTaskSuspend
.L41:
	bl	init_file_station_group
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L42
	mov	r0, #0
	bl	vTaskSuspend
.L42:
	ldr	r4, .L72+268
	bl	init_file_station_info
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L43
	mov	r0, #0
	bl	vTaskSuspend
.L43:
	bl	init_file_LIGHTS
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L44
	mov	r0, #0
	bl	vTaskSuspend
.L44:
	ldr	r4, .L72+268
	bl	init_file_moisture_sensor
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L45
	mov	r0, #0
	bl	vTaskSuspend
.L45:
	bl	init_file_walk_thru
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L46
	mov	r0, #0
	bl	vTaskSuspend
.L46:
	ldr	r4, .L72+268
	bl	init_tpmicro_data_on_boot
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L47
	mov	r0, #0
	bl	vTaskSuspend
.L47:
	bl	init_radio_test_gui_vars_and_state_machine
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L48
	mov	r0, #0
	bl	vTaskSuspend
.L48:
	ldr	r4, .L72+268
	mov	r0, #0
	bl	init_battery_backed_weather_preserves
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L49
	mov	r0, #0
	bl	vTaskSuspend
.L49:
	mov	r0, #0
	bl	init_battery_backed_station_preserves
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L50
	mov	r0, #0
	bl	vTaskSuspend
.L50:
	ldr	r4, .L72+268
	mov	r0, #0
	bl	init_battery_backed_system_preserves
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L51
	mov	r0, #0
	bl	vTaskSuspend
.L51:
	bl	SYSTEM_PRESERVES_synchronize_preserves_to_file
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L52
	mov	r0, #0
	bl	vTaskSuspend
.L52:
	ldr	r4, .L72+268
	mov	r0, #0
	bl	init_battery_backed_poc_preserves
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L53
	mov	r0, #0
	bl	vTaskSuspend
.L53:
	bl	POC_PRESERVES_synchronize_preserves_to_file
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L54
	mov	r0, #0
	bl	vTaskSuspend
.L54:
	ldr	r4, .L72+268
	mov	r0, #0
	bl	init_battery_backed_lights_preserves
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L55
	mov	r0, #0
	bl	vTaskSuspend
.L55:
	mov	r0, #0
	bl	init_battery_backed_foal_irri
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L56
	mov	r0, #0
	bl	vTaskSuspend
.L56:
	ldr	r4, .L72+268
	mov	r0, #0
	bl	init_battery_backed_foal_lights
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L57
	mov	r0, #0
	bl	vTaskSuspend
.L57:
	mov	r0, #0
	bl	init_battery_backed_general_use
	ldr	r3, [r4, #68]
	cmp	r3, #1
	bne	.L58
	mov	r0, #0
	bl	vTaskSuspend
.L58:
	mov	r0, #0
	bl	init_battery_backed_chain_members
	mov	r1, #0
	mov	r2, #48
	ldr	r0, .L72+348
	bl	memset
	ldr	r3, .L72+352
	ldr	r0, .L72+348
	mov	r1, #48
	ldr	r2, .L72+356
	bl	snprintf
	ldr	r3, .L72+268
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L59
	mov	r0, #0
	bl	vTaskSuspend
.L59:
	mov	r1, #0
	mov	r2, #3168
	ldr	r0, .L72+360
	bl	memset
	ldr	r0, .L72+364
	mov	r1, #0
	mov	r2, #96
	bl	memset
	ldr	r6, .L72+360
	ldr	r4, .L72+368
.LBB16:
	ldr	r7, .L72+268
	ldr	sl, .L72+364
.LBE16:
	mov	r5, #0
.LBB17:
	add	r8, sp, #16
.L66:
.LBE17:
	ldr	r3, [r4, #0]
	cmp	r3, #1
	bne	.L60
.LBB18:
	ldr	r3, [r7, #68]
	cmp	r3, #1
	bne	.L61
	mov	r0, #0
	bl	vTaskSuspend
.L61:
	ldr	r3, [r4, #28]
	ldrh	r2, [r4, #20]
	stmia	sp, {r3, r8}
	mov	r3, #0
	str	r3, [sp, #8]
	str	r3, [sp, #12]
	mov	r3, r4
	ldr	r1, [r4, #16]
	ldr	r0, [r4, #12]
	bl	xTaskGenericCreate
	mov	r0, r6
	bl	vPortSaveVFPRegisters
	mov	r1, r6
	ldr	r0, [sp, #16]
	bl	vTaskSetApplicationTaskTag
	ldr	r3, [r4, #12]
	ldr	r1, .L72+372
	ldr	r2, [sp, #16]
	cmp	r3, r1
	str	r2, [r5, sl]
	bne	.L62
	ldr	r1, [r4, #24]
	cmp	r1, #0
	ldreq	r1, .L72+376
	streq	r2, [r1, #0]
	ldr	r2, [r4, #24]
	cmp	r2, #1
	ldreq	r1, [sp, #16]
	ldreq	r2, .L72+380
	streq	r1, [r2, #0]
.L62:
	ldr	r2, .L72+384
	cmp	r3, r2
	ldreq	r2, .L72+388
	ldreq	r1, [sp, #16]
	streq	r1, [r2, #0]
	ldr	r2, .L72+392
	cmp	r3, r2
	bne	.L65
	ldr	r2, [r4, #24]
	cmp	r2, #0
	ldreq	r1, [sp, #16]
	ldreq	r2, .L72+396
	streq	r1, [r2, #0]
.L65:
	ldr	r2, .L72+400
	cmp	r3, r2
	bne	.L60
	ldr	r3, [r4, #24]
	cmp	r3, #0
	ldreq	r2, [sp, #16]
	ldreq	r3, .L72+404
	streq	r2, [r3, #0]
.L60:
.LBE18:
	add	r5, r5, #4
	cmp	r5, #96
	add	r4, r4, #32
	add	r6, r6, #132
	bne	.L66
	mov	r0, #20
	bl	CONTROLLER_INITIATED_post_event
	mov	r0, #0
	bl	vTaskDelete
.L67:
	b	.L67
.L73:
	.align	2
.L72:
	.word	.LANCHOR4
	.word	.LC1
	.word	wdt_monitoring_task
	.word	.LANCHOR5
	.word	.LANCHOR6
	.word	.LANCHOR7
	.word	.LC2
	.word	EPSON_rtc_control_task
	.word	.LANCHOR5+132
	.word	.LANCHOR8
	.word	.LANCHOR5+264
	.word	.LANCHOR9
	.word	.LC3
	.word	system_shutdown_task
	.word	.LANCHOR5+396
	.word	.LANCHOR10
	.word	.LC4
	.word	.LC5
	.word	893
	.word	.LANCHOR11
	.word	.LANCHOR12
	.word	.LANCHOR13
	.word	SerDrvrVars_s
	.word	21400
	.word	.LANCHOR14
	.word	.LANCHOR15
	.word	.LANCHOR16
	.word	.LANCHOR17
	.word	.LANCHOR18
	.word	.LANCHOR19
	.word	.LANCHOR20
	.word	.LANCHOR21
	.word	.LANCHOR22
	.word	.LANCHOR23
	.word	.LANCHOR24
	.word	.LANCHOR25
	.word	.LANCHOR26
	.word	.LANCHOR27
	.word	.LANCHOR28
	.word	.LANCHOR29
	.word	.LANCHOR30
	.word	.LANCHOR31
	.word	.LANCHOR32
	.word	.LANCHOR33
	.word	.LANCHOR34
	.word	.LANCHOR35
	.word	.LANCHOR36
	.word	.LANCHOR37
	.word	.LANCHOR38
	.word	.LANCHOR39
	.word	.LANCHOR40
	.word	.LANCHOR41
	.word	.LANCHOR42
	.word	.LANCHOR43
	.word	.LANCHOR44
	.word	.LANCHOR45
	.word	.LANCHOR46
	.word	.LANCHOR47
	.word	.LANCHOR48
	.word	.LANCHOR49
	.word	.LANCHOR50
	.word	.LANCHOR51
	.word	.LANCHOR52
	.word	.LANCHOR53
	.word	.LANCHOR54
	.word	.LANCHOR55
	.word	.LANCHOR56
	.word	restart_info
	.word	.LANCHOR57
	.word	.LANCHOR58
	.word	.LANCHOR59
	.word	.LANCHOR60
	.word	.LANCHOR61
	.word	.LANCHOR62
	.word	.LANCHOR63
	.word	.LANCHOR64
	.word	.LANCHOR65
	.word	.LANCHOR66
	.word	.LANCHOR67
	.word	.LANCHOR68
	.word	.LANCHOR69
	.word	.LANCHOR70
	.word	180000
	.word	ci_alerts_timer_callback
	.word	.LC6
	.word	cics
	.word	display_model_is
	.word	restart_info+16
	.word	-2147483584
	.word	.LC7
	.word	.LANCHOR5+528
	.word	.LANCHOR71
	.word	.LANCHOR3
	.word	FLASH_STORAGE_flash_storage_task
	.word	.LANCHOR72
	.word	.LANCHOR73
	.word	COMM_MNGR_task
	.word	.LANCHOR0
	.word	ring_buffer_analysis_task
	.word	.LANCHOR1
	.word	serial_driver_task
	.word	.LANCHOR2
.LFE4:
	.size	system_startup_task, .-system_startup_task
	.global	Task_Table
	.global	task_last_execution_stamp
	.global	pwrfail_task_last_x_stamp
	.global	rtc_task_last_x_stamp
	.global	created_task_handles
	.global	flash_storage_1_task_handle
	.global	flash_storage_0_task_handle
	.global	timer_task_handle
	.global	wdt_task_handle
	.global	powerfail_task_handle
	.global	epson_rtc_task_handle
	.global	startup_task_handle
	.global	timer_task_FLOP_registers
	.global	startup_task_FLOP_registers
	.global	FTIMES_task_queue
	.global	Background_Calculation_Queue
	.global	RADIO_TEST_task_queue
	.global	BYPASS_event_queue
	.global	CONTROLLER_INITIATED_task_queue
	.global	CODE_DISTRIBUTION_task_queue
	.global	COMM_MNGR_task_queue
	.global	TPL_OUT_event_queue
	.global	TPL_IN_event_queue
	.global	Display_Command_Queue
	.global	FLASH_STORAGE_task_queue_1
	.global	FLASH_STORAGE_task_queue_0
	.global	Key_To_Process_Queue
	.global	Key_Scanner_Queue
	.global	Contrast_and_Volume_Queue
	.global	router_hub_list_queue
	.global	router_hub_list_MUTEX
	.global	chain_sync_control_structure_recursive_MUTEX
	.global	code_distribution_control_structure_recursive_MUTEX
	.global	ci_message_list_MUTEX
	.global	budget_report_completed_records_recursive_MUTEX
	.global	walk_thru_recursive_MUTEX
	.global	moisture_sensor_recorder_recursive_MUTEX
	.global	moisture_sensor_items_recursive_MUTEX
	.global	lights_report_completed_records_recursive_MUTEX
	.global	lights_preserves_recursive_MUTEX
	.global	irri_lights_recursive_MUTEX
	.global	list_foal_lights_recursive_MUTEX
	.global	list_lights_recursive_MUTEX
	.global	pending_changes_recursive_MUTEX
	.global	tpmicro_data_recursive_MUTEX
	.global	speaker_hardware_MUTEX
	.global	weather_tables_recursive_MUTEX
	.global	weather_control_recursive_MUTEX
	.global	list_poc_recursive_MUTEX
	.global	list_system_recursive_MUTEX
	.global	poc_preserves_recursive_MUTEX
	.global	system_preserves_recursive_MUTEX
	.global	station_preserves_recursive_MUTEX
	.global	weather_preserves_recursive_MUTEX
	.global	station_history_completed_records_recursive_MUTEX
	.global	station_report_data_completed_records_recursive_MUTEX
	.global	poc_report_completed_records_recursive_MUTEX
	.global	system_report_completed_records_recursive_MUTEX
	.global	irri_comm_recursive_MUTEX
	.global	chain_members_recursive_MUTEX
	.global	comm_mngr_recursive_MUTEX
	.global	list_foal_irri_recursive_MUTEX
	.global	irri_irri_recursive_MUTEX
	.global	ci_and_comm_mngr_activity_recursive_MUTEX
	.global	xmit_list_MUTEX
	.global	list_packets_waiting_for_token_MUTEX
	.global	list_incoming_messages_recursive_MUTEX
	.global	list_tpl_out_messages_MUTEX
	.global	list_tpl_in_messages_MUTEX
	.global	list_program_data_recursive_MUTEX
	.global	rcvd_data_binary_semaphore
	.global	Flash_1_MUTEX
	.global	Flash_0_MUTEX
	.global	alerts_pile_recursive_MUTEX
	.global	startup_rtc_task_sync_semaphore
	.global	debugio_MUTEX
	.section	.bss.CONTROLLER_INITIATED_task_queue,"aw",%nobits
	.align	2
	.set	.LANCHOR65,. + 0
	.type	CONTROLLER_INITIATED_task_queue, %object
	.size	CONTROLLER_INITIATED_task_queue, 4
CONTROLLER_INITIATED_task_queue:
	.space	4
	.section	.bss.TPL_IN_event_queue,"aw",%nobits
	.align	2
	.set	.LANCHOR61,. + 0
	.type	TPL_IN_event_queue, %object
	.size	TPL_IN_event_queue, 4
TPL_IN_event_queue:
	.space	4
	.section	.bss.startup_rtc_task_sync_semaphore,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	startup_rtc_task_sync_semaphore, %object
	.size	startup_rtc_task_sync_semaphore, 4
startup_rtc_task_sync_semaphore:
	.space	4
	.section	.bss.system_report_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR27,. + 0
	.type	system_report_completed_records_recursive_MUTEX, %object
	.size	system_report_completed_records_recursive_MUTEX, 4
system_report_completed_records_recursive_MUTEX:
	.space	4
	.section	.bss.code_distribution_control_structure_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR52,. + 0
	.type	code_distribution_control_structure_recursive_MUTEX, %object
	.size	code_distribution_control_structure_recursive_MUTEX, 4
code_distribution_control_structure_recursive_MUTEX:
	.space	4
	.section	.bss.FLASH_STORAGE_task_queue_0,"aw",%nobits
	.align	2
	.set	.LANCHOR58,. + 0
	.type	FLASH_STORAGE_task_queue_0, %object
	.size	FLASH_STORAGE_task_queue_0, 4
FLASH_STORAGE_task_queue_0:
	.space	4
	.section	.bss.FLASH_STORAGE_task_queue_1,"aw",%nobits
	.align	2
	.set	.LANCHOR59,. + 0
	.type	FLASH_STORAGE_task_queue_1, %object
	.size	FLASH_STORAGE_task_queue_1, 4
FLASH_STORAGE_task_queue_1:
	.space	4
	.section	.bss.ci_and_comm_mngr_activity_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR26,. + 0
	.type	ci_and_comm_mngr_activity_recursive_MUTEX, %object
	.size	ci_and_comm_mngr_activity_recursive_MUTEX, 4
ci_and_comm_mngr_activity_recursive_MUTEX:
	.space	4
	.section	.bss.list_packets_waiting_for_token_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR20,. + 0
	.type	list_packets_waiting_for_token_MUTEX, %object
	.size	list_packets_waiting_for_token_MUTEX, 4
list_packets_waiting_for_token_MUTEX:
	.space	4
	.section	.bss.CODE_DISTRIBUTION_task_queue,"aw",%nobits
	.align	2
	.set	.LANCHOR64,. + 0
	.type	CODE_DISTRIBUTION_task_queue, %object
	.size	CODE_DISTRIBUTION_task_queue, 4
CODE_DISTRIBUTION_task_queue:
	.space	4
	.section	.bss.Display_Command_Queue,"aw",%nobits
	.align	2
	.set	.LANCHOR60,. + 0
	.type	Display_Command_Queue, %object
	.size	Display_Command_Queue, 4
Display_Command_Queue:
	.space	4
	.section	.iram_vars,"aw",%progbits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	wdt_task_FLOP_registers, %object
	.size	wdt_task_FLOP_registers, 132
wdt_task_FLOP_registers:
	.space	132
	.type	epson_RTC_task_FLOP_registers, %object
	.size	epson_RTC_task_FLOP_registers, 132
epson_RTC_task_FLOP_registers:
	.space	132
	.type	timer_task_FLOP_registers, %object
	.size	timer_task_FLOP_registers, 132
timer_task_FLOP_registers:
	.space	132
	.type	powerfail_task_FLOP_registers, %object
	.size	powerfail_task_FLOP_registers, 132
powerfail_task_FLOP_registers:
	.space	132
	.type	task_table_FLOP_registers, %object
	.size	task_table_FLOP_registers, 3168
task_table_FLOP_registers:
	.space	3168
	.type	task_last_execution_stamp, %object
	.size	task_last_execution_stamp, 96
task_last_execution_stamp:
	.space	96
	.type	pwrfail_task_last_x_stamp, %object
	.size	pwrfail_task_last_x_stamp, 4
pwrfail_task_last_x_stamp:
	.space	4
	.type	startup_task_FLOP_registers, %object
	.size	startup_task_FLOP_registers, 132
startup_task_FLOP_registers:
	.space	132
	.section	.bss.BYPASS_event_queue,"aw",%nobits
	.align	2
	.set	.LANCHOR66,. + 0
	.type	BYPASS_event_queue, %object
	.size	BYPASS_event_queue, 4
BYPASS_event_queue:
	.space	4
	.section	.bss.created_task_handles,"aw",%nobits
	.align	2
	.set	.LANCHOR71,. + 0
	.type	created_task_handles, %object
	.size	created_task_handles, 96
created_task_handles:
	.space	96
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"NULL Task Handle!\000"
.LC1:
	.ascii	"WDT\000"
.LC2:
	.ascii	"RTC\000"
.LC3:
	.ascii	"PWRFAIL\000"
.LC4:
	.ascii	"( debugio_MUTEX = xSemaphoreCreateMutex() )\000"
.LC5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/app_"
	.ascii	"startup.c\000"
.LC6:
	.ascii	"ci alerts tmr\000"
.LC7:
	.ascii	"%s\000"
.LC8:
	.ascii	"Dsply_Proc\000"
.LC9:
	.ascii	"Key_Proc\000"
.LC10:
	.ascii	"Key_Scan\000"
.LC11:
	.ascii	"TD_Check\000"
.LC12:
	.ascii	"Bypass\000"
.LC13:
	.ascii	"Comm_Mngr\000"
.LC14:
	.ascii	"Code Distrib\000"
.LC15:
	.ascii	"Ring_TP\000"
.LC16:
	.ascii	"Ring_A\000"
.LC17:
	.ascii	"Ring_B\000"
.LC18:
	.ascii	"Ring_RRE\000"
.LC19:
	.ascii	"CI task\000"
.LC20:
	.ascii	"LINK task\000"
.LC21:
	.ascii	"TPL_IN\000"
.LC22:
	.ascii	"TPL_OUT\000"
.LC23:
	.ascii	"SerDrvr_TP\000"
.LC24:
	.ascii	"SerDrvr_A\000"
.LC25:
	.ascii	"SerDrvr_B\000"
.LC26:
	.ascii	"SerDrvr_RRE\000"
.LC27:
	.ascii	"Contrast\000"
.LC28:
	.ascii	"Flash_0\000"
.LC29:
	.ascii	"Flash_1\000"
.LC30:
	.ascii	"BCalc\000"
.LC31:
	.ascii	"FTimes\000"
	.section	.bss.Flash_0_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR13,. + 0
	.type	Flash_0_MUTEX, %object
	.size	Flash_0_MUTEX, 4
Flash_0_MUTEX:
	.space	4
	.section	.bss.startup_task_handle,"aw",%nobits
	.align	2
	.type	startup_task_handle, %object
	.size	startup_task_handle, 4
startup_task_handle:
	.space	4
	.section	.bss.list_lights_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR39,. + 0
	.type	list_lights_recursive_MUTEX, %object
	.size	list_lights_recursive_MUTEX, 4
list_lights_recursive_MUTEX:
	.space	4
	.section	.bss.system_preserves_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR33,. + 0
	.type	system_preserves_recursive_MUTEX, %object
	.size	system_preserves_recursive_MUTEX, 4
system_preserves_recursive_MUTEX:
	.space	4
	.section	.bss.RADIO_TEST_task_queue,"aw",%nobits
	.align	2
	.set	.LANCHOR67,. + 0
	.type	RADIO_TEST_task_queue, %object
	.size	RADIO_TEST_task_queue, 4
RADIO_TEST_task_queue:
	.space	4
	.section	.bss.Key_Scanner_Queue,"aw",%nobits
	.align	2
	.set	.LANCHOR56,. + 0
	.type	Key_Scanner_Queue, %object
	.size	Key_Scanner_Queue, 4
Key_Scanner_Queue:
	.space	4
	.section	.bss.weather_preserves_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR31,. + 0
	.type	weather_preserves_recursive_MUTEX, %object
	.size	weather_preserves_recursive_MUTEX, 4
weather_preserves_recursive_MUTEX:
	.space	4
	.section	.bss.pending_changes_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR46,. + 0
	.type	pending_changes_recursive_MUTEX, %object
	.size	pending_changes_recursive_MUTEX, 4
pending_changes_recursive_MUTEX:
	.space	4
	.section	.bss.rcvd_data_binary_semaphore,"aw",%nobits
	.align	2
	.set	.LANCHOR11,. + 0
	.type	rcvd_data_binary_semaphore, %object
	.size	rcvd_data_binary_semaphore, 20
rcvd_data_binary_semaphore:
	.space	20
	.section	.bss.Flash_1_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR14,. + 0
	.type	Flash_1_MUTEX, %object
	.size	Flash_1_MUTEX, 4
Flash_1_MUTEX:
	.space	4
	.section	.bss.tpmicro_data_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR45,. + 0
	.type	tpmicro_data_recursive_MUTEX, %object
	.size	tpmicro_data_recursive_MUTEX, 4
tpmicro_data_recursive_MUTEX:
	.space	4
	.section	.bss.list_tpl_in_messages_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR17,. + 0
	.type	list_tpl_in_messages_MUTEX, %object
	.size	list_tpl_in_messages_MUTEX, 4
list_tpl_in_messages_MUTEX:
	.space	4
	.section	.bss.list_system_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR35,. + 0
	.type	list_system_recursive_MUTEX, %object
	.size	list_system_recursive_MUTEX, 4
list_system_recursive_MUTEX:
	.space	4
	.section	.bss.list_tpl_out_messages_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR18,. + 0
	.type	list_tpl_out_messages_MUTEX, %object
	.size	list_tpl_out_messages_MUTEX, 4
list_tpl_out_messages_MUTEX:
	.space	4
	.section	.bss.ci_message_list_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR51,. + 0
	.type	ci_message_list_MUTEX, %object
	.size	ci_message_list_MUTEX, 4
ci_message_list_MUTEX:
	.space	4
	.section	.bss.TPL_OUT_event_queue,"aw",%nobits
	.align	2
	.set	.LANCHOR62,. + 0
	.type	TPL_OUT_event_queue, %object
	.size	TPL_OUT_event_queue, 4
TPL_OUT_event_queue:
	.space	4
	.section	.bss.station_preserves_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR32,. + 0
	.type	station_preserves_recursive_MUTEX, %object
	.size	station_preserves_recursive_MUTEX, 4
station_preserves_recursive_MUTEX:
	.space	4
	.section	.bss.irri_irri_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR21,. + 0
	.type	irri_irri_recursive_MUTEX, %object
	.size	irri_irri_recursive_MUTEX, 4
irri_irri_recursive_MUTEX:
	.space	4
	.section	.bss.station_report_data_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR29,. + 0
	.type	station_report_data_completed_records_recursive_MUTEX, %object
	.size	station_report_data_completed_records_recursive_MUTEX, 4
station_report_data_completed_records_recursive_MUTEX:
	.space	4
	.section	.bss.COMM_MNGR_task_queue,"aw",%nobits
	.align	2
	.set	.LANCHOR63,. + 0
	.type	COMM_MNGR_task_queue, %object
	.size	COMM_MNGR_task_queue, 4
COMM_MNGR_task_queue:
	.space	4
	.section	.bss.irri_comm_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR25,. + 0
	.type	irri_comm_recursive_MUTEX, %object
	.size	irri_comm_recursive_MUTEX, 4
irri_comm_recursive_MUTEX:
	.space	4
	.section	.bss.Key_To_Process_Queue,"aw",%nobits
	.align	2
	.set	.LANCHOR57,. + 0
	.type	Key_To_Process_Queue, %object
	.size	Key_To_Process_Queue, 4
Key_To_Process_Queue:
	.space	4
	.section	.bss.flash_storage_1_task_handle,"aw",%nobits
	.align	2
	.set	.LANCHOR73,. + 0
	.type	flash_storage_1_task_handle, %object
	.size	flash_storage_1_task_handle, 4
flash_storage_1_task_handle:
	.space	4
	.section	.bss.alerts_pile_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR15,. + 0
	.type	alerts_pile_recursive_MUTEX, %object
	.size	alerts_pile_recursive_MUTEX, 4
alerts_pile_recursive_MUTEX:
	.space	4
	.section	.bss.powerfail_task_handle,"aw",%nobits
	.align	2
	.set	.LANCHOR9,. + 0
	.type	powerfail_task_handle, %object
	.size	powerfail_task_handle, 4
powerfail_task_handle:
	.space	4
	.section	.bss.comm_mngr_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR23,. + 0
	.type	comm_mngr_recursive_MUTEX, %object
	.size	comm_mngr_recursive_MUTEX, 4
comm_mngr_recursive_MUTEX:
	.space	4
	.section	.bss.chain_members_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR24,. + 0
	.type	chain_members_recursive_MUTEX, %object
	.size	chain_members_recursive_MUTEX, 4
chain_members_recursive_MUTEX:
	.space	4
	.section	.bss.list_incoming_messages_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR19,. + 0
	.type	list_incoming_messages_recursive_MUTEX, %object
	.size	list_incoming_messages_recursive_MUTEX, 4
list_incoming_messages_recursive_MUTEX:
	.space	4
	.section	.bss.list_foal_irri_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR22,. + 0
	.type	list_foal_irri_recursive_MUTEX, %object
	.size	list_foal_irri_recursive_MUTEX, 4
list_foal_irri_recursive_MUTEX:
	.space	4
	.section	.bss.poc_report_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR28,. + 0
	.type	poc_report_completed_records_recursive_MUTEX, %object
	.size	poc_report_completed_records_recursive_MUTEX, 4
poc_report_completed_records_recursive_MUTEX:
	.space	4
	.section	.bss.port_tp_ring_buffer_analysis_task_handle,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	port_tp_ring_buffer_analysis_task_handle, %object
	.size	port_tp_ring_buffer_analysis_task_handle, 4
port_tp_ring_buffer_analysis_task_handle:
	.space	4
	.section	.bss.moisture_sensor_items_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR47,. + 0
	.type	moisture_sensor_items_recursive_MUTEX, %object
	.size	moisture_sensor_items_recursive_MUTEX, 4
moisture_sensor_items_recursive_MUTEX:
	.space	4
	.section	.bss.comm_mngr_task_handle,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	comm_mngr_task_handle, %object
	.size	comm_mngr_task_handle, 4
comm_mngr_task_handle:
	.space	4
	.section	.bss.weather_tables_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR38,. + 0
	.type	weather_tables_recursive_MUTEX, %object
	.size	weather_tables_recursive_MUTEX, 4
weather_tables_recursive_MUTEX:
	.space	4
	.section	.bss.rtc_task_last_x_stamp,"aw",%nobits
	.align	2
	.type	rtc_task_last_x_stamp, %object
	.size	rtc_task_last_x_stamp, 4
rtc_task_last_x_stamp:
	.space	4
	.section	.bss.list_program_data_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR16,. + 0
	.type	list_program_data_recursive_MUTEX, %object
	.size	list_program_data_recursive_MUTEX, 4
list_program_data_recursive_MUTEX:
	.space	4
	.section	.bss.poc_preserves_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR34,. + 0
	.type	poc_preserves_recursive_MUTEX, %object
	.size	poc_preserves_recursive_MUTEX, 4
poc_preserves_recursive_MUTEX:
	.space	4
	.section	.bss.walk_thru_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR49,. + 0
	.type	walk_thru_recursive_MUTEX, %object
	.size	walk_thru_recursive_MUTEX, 4
walk_thru_recursive_MUTEX:
	.space	4
	.section	.bss.timer_task_handle,"aw",%nobits
	.align	2
	.set	.LANCHOR8,. + 0
	.type	timer_task_handle, %object
	.size	timer_task_handle, 4
timer_task_handle:
	.space	4
	.section	.bss.chain_sync_control_structure_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR53,. + 0
	.type	chain_sync_control_structure_recursive_MUTEX, %object
	.size	chain_sync_control_structure_recursive_MUTEX, 4
chain_sync_control_structure_recursive_MUTEX:
	.space	4
	.section	.bss.list_foal_lights_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR40,. + 0
	.type	list_foal_lights_recursive_MUTEX, %object
	.size	list_foal_lights_recursive_MUTEX, 4
list_foal_lights_recursive_MUTEX:
	.space	4
	.section	.bss.xmit_list_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR12,. + 0
	.type	xmit_list_MUTEX, %object
	.size	xmit_list_MUTEX, 20
xmit_list_MUTEX:
	.space	20
	.section	.bss.lights_preserves_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR43,. + 0
	.type	lights_preserves_recursive_MUTEX, %object
	.size	lights_preserves_recursive_MUTEX, 4
lights_preserves_recursive_MUTEX:
	.space	4
	.section	.bss.router_hub_list_queue,"aw",%nobits
	.align	2
	.set	.LANCHOR70,. + 0
	.type	router_hub_list_queue, %object
	.size	router_hub_list_queue, 4
router_hub_list_queue:
	.space	4
	.section	.bss.weather_control_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR37,. + 0
	.type	weather_control_recursive_MUTEX, %object
	.size	weather_control_recursive_MUTEX, 4
weather_control_recursive_MUTEX:
	.space	4
	.section	.bss.epson_rtc_task_handle,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	epson_rtc_task_handle, %object
	.size	epson_rtc_task_handle, 4
epson_rtc_task_handle:
	.space	4
	.section	.bss.port_tp_serial_driver_task_handle,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	port_tp_serial_driver_task_handle, %object
	.size	port_tp_serial_driver_task_handle, 4
port_tp_serial_driver_task_handle:
	.space	4
	.section	.bss.station_history_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR30,. + 0
	.type	station_history_completed_records_recursive_MUTEX, %object
	.size	station_history_completed_records_recursive_MUTEX, 4
station_history_completed_records_recursive_MUTEX:
	.space	4
	.section	.bss.FTIMES_task_queue,"aw",%nobits
	.align	2
	.set	.LANCHOR69,. + 0
	.type	FTIMES_task_queue, %object
	.size	FTIMES_task_queue, 4
FTIMES_task_queue:
	.space	4
	.section	.bss.list_poc_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR36,. + 0
	.type	list_poc_recursive_MUTEX, %object
	.size	list_poc_recursive_MUTEX, 4
list_poc_recursive_MUTEX:
	.space	4
	.section	.bss.router_hub_list_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR54,. + 0
	.type	router_hub_list_MUTEX, %object
	.size	router_hub_list_MUTEX, 4
router_hub_list_MUTEX:
	.space	4
	.section	.bss.Contrast_and_Volume_Queue,"aw",%nobits
	.align	2
	.set	.LANCHOR55,. + 0
	.type	Contrast_and_Volume_Queue, %object
	.size	Contrast_and_Volume_Queue, 4
Contrast_and_Volume_Queue:
	.space	4
	.section	.bss.flash_storage_0_task_handle,"aw",%nobits
	.align	2
	.set	.LANCHOR72,. + 0
	.type	flash_storage_0_task_handle, %object
	.size	flash_storage_0_task_handle, 4
flash_storage_0_task_handle:
	.space	4
	.section	.bss.moisture_sensor_recorder_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR48,. + 0
	.type	moisture_sensor_recorder_recursive_MUTEX, %object
	.size	moisture_sensor_recorder_recursive_MUTEX, 4
moisture_sensor_recorder_recursive_MUTEX:
	.space	4
	.section	.bss.speaker_hardware_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR44,. + 0
	.type	speaker_hardware_MUTEX, %object
	.size	speaker_hardware_MUTEX, 4
speaker_hardware_MUTEX:
	.space	4
	.section	.bss.budget_report_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR50,. + 0
	.type	budget_report_completed_records_recursive_MUTEX, %object
	.size	budget_report_completed_records_recursive_MUTEX, 4
budget_report_completed_records_recursive_MUTEX:
	.space	4
	.section	.bss.wdt_task_handle,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	wdt_task_handle, %object
	.size	wdt_task_handle, 4
wdt_task_handle:
	.space	4
	.section	.bss.lights_report_completed_records_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR42,. + 0
	.type	lights_report_completed_records_recursive_MUTEX, %object
	.size	lights_report_completed_records_recursive_MUTEX, 4
lights_report_completed_records_recursive_MUTEX:
	.space	4
	.section	.bss.Background_Calculation_Queue,"aw",%nobits
	.align	2
	.set	.LANCHOR68,. + 0
	.type	Background_Calculation_Queue, %object
	.size	Background_Calculation_Queue, 4
Background_Calculation_Queue:
	.space	4
	.section	.rodata.Task_Table,"a",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	Task_Table, %object
	.size	Task_Table, 768
Task_Table:
	.word	1
	.word	1
	.word	1000
	.word	Display_Processing_Task
	.word	.LC8
	.word	2048
	.word	0
	.word	8
	.word	1
	.word	1
	.word	1000
	.word	Key_Processing_Task
	.word	.LC9
	.word	1024
	.word	0
	.word	7
	.word	1
	.word	1
	.word	1000
	.word	key_scanner_task
	.word	.LC10
	.word	1024
	.word	0
	.word	6
	.word	1
	.word	1
	.word	1000
	.word	TD_CHECK_task
	.word	.LC11
	.word	1024
	.word	0
	.word	6
	.word	1
	.word	1
	.word	1000
	.word	BYPASS_task
	.word	.LC12
	.word	1024
	.word	0
	.word	6
	.word	1
	.word	1
	.word	5000
	.word	COMM_MNGR_task
	.word	.LC13
	.word	1024
	.word	0
	.word	5
	.word	1
	.word	1
	.word	5000
	.word	CODE_DISTRIBUTION_task
	.word	.LC14
	.word	1024
	.word	0
	.word	5
	.word	1
	.word	1
	.word	1000
	.word	ring_buffer_analysis_task
	.word	.LC15
	.word	1024
	.word	0
	.word	5
	.word	1
	.word	1
	.word	10500
	.word	ring_buffer_analysis_task
	.word	.LC16
	.word	1024
	.word	1
	.word	5
	.word	1
	.word	1
	.word	1000
	.word	ring_buffer_analysis_task
	.word	.LC17
	.word	1024
	.word	2
	.word	5
	.word	1
	.word	1
	.word	1000
	.word	ring_buffer_analysis_task
	.word	.LC18
	.word	1024
	.word	3
	.word	5
	.word	1
	.word	1
	.word	2000
	.word	CONTROLLER_INITIATED_task
	.word	.LC19
	.word	1024
	.word	0
	.word	5
	.word	1
	.word	1
	.word	2000
	.word	RADIO_TEST_task
	.word	.LC20
	.word	1024
	.word	0
	.word	5
	.word	1
	.word	1
	.word	1000
	.word	TPL_IN_task
	.word	.LC21
	.word	1024
	.word	0
	.word	4
	.word	1
	.word	1
	.word	1000
	.word	TPL_OUT_task
	.word	.LC22
	.word	1024
	.word	0
	.word	4
	.word	1
	.word	1
	.word	1000
	.word	serial_driver_task
	.word	.LC23
	.word	1024
	.word	0
	.word	3
	.word	1
	.word	1
	.word	1000
	.word	serial_driver_task
	.word	.LC24
	.word	1024
	.word	1
	.word	3
	.word	1
	.word	1
	.word	1000
	.word	serial_driver_task
	.word	.LC25
	.word	1024
	.word	2
	.word	3
	.word	1
	.word	1
	.word	1000
	.word	serial_driver_task
	.word	.LC26
	.word	1024
	.word	3
	.word	3
	.word	1
	.word	1
	.word	1000
	.word	contrast_and_speaker_volume_control_task
	.word	.LC27
	.word	1024
	.word	0
	.word	2
	.word	1
	.word	1
	.word	30000
	.word	FLASH_STORAGE_flash_storage_task
	.word	.LC28
	.word	1024
	.word	0
	.word	2
	.word	1
	.word	1
	.word	20000
	.word	FLASH_STORAGE_flash_storage_task
	.word	.LC29
	.word	1024
	.word	1
	.word	2
	.word	1
	.word	1
	.word	20000
	.word	background_calculation_task
	.word	.LC30
	.word	1024
	.word	0
	.word	1
	.word	1
	.word	1
	.word	1000
	.word	FTIMES_TASK_task
	.word	.LC31
	.word	1024
	.word	0
	.word	1
	.section	.bss.debugio_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR10,. + 0
	.type	debugio_MUTEX, %object
	.size	debugio_MUTEX, 4
debugio_MUTEX:
	.space	4
	.section	.bss.irri_lights_recursive_MUTEX,"aw",%nobits
	.align	2
	.set	.LANCHOR41,. + 0
	.type	irri_lights_recursive_MUTEX, %object
	.size	irri_lights_recursive_MUTEX, 4
irri_lights_recursive_MUTEX:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/app_startup.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x7a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x34e
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x364
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x377
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x445
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x4ae
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x4ca
	.byte	0x1
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"__create_system_semaphores\000"
.LASF1:
	.ascii	"restore_priority_of_ISP_related_code_update_tasks\000"
.LASF0:
	.ascii	"elevate_priority_of_ISP_related_code_update_tasks\000"
.LASF6:
	.ascii	"system_startup_task\000"
.LASF7:
	.ascii	"vPortSaveVFPRegisters\000"
.LASF3:
	.ascii	"__create_system_queues\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/app_"
	.ascii	"startup.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
