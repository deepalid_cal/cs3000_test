	.file	"device_SR_FREEWAVE.c"
	.text
.Ltext0:
	.section	.text.sr_final_radio_analysis,"ax",%progbits
	.align	2
	.type	sr_final_radio_analysis, %function
sr_final_radio_analysis:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI0:
	ldr	r1, .L19
	mov	r4, r0
	mov	r2, #40
	add	r0, r0, #14
	bl	strlcpy
	ldr	r0, .L19+4
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	mov	r3, #1
	str	r3, [r4, #116]
	ldr	r3, [r4, #104]
	cmp	r3, #0
	beq	.L2
.LBB18:
	ldr	r6, .L19+8
	mov	r5, #0
	mov	sl, #20
	add	r9, r4, #176
	str	r5, [r4, #108]
.L4:
	mul	r7, sl, r5
	ldr	r0, [r4, #140]
	add	r1, r7, #12
	add	r0, r0, #46
	add	r1, r6, r1
	mov	r2, #5
	bl	strncmp
	cmp	r0, #0
	bne	.L3
	ldr	r0, [r4, #140]
	add	r8, r7, #8
	add	r8, r6, r8
	add	r0, r0, #17
	mov	r1, r8
	mov	r2, #2
	bl	strncmp
	cmp	r0, #0
	bne	.L3
	ldr	r0, [r4, #140]
	add	r1, r8, #2
	add	r0, r0, #19
	mov	r2, #2
	bl	strncmp
	cmp	r0, #0
	bne	.L3
	mov	r0, r9
	add	r1, r6, r7
	mov	r2, #5
	bl	strlcpy
	mov	r3, #1
	str	r3, [r4, #108]
	str	r5, [r4, #172]
.L3:
	add	r5, r5, #1
	cmp	r5, #10
	bne	.L4
.LBE18:
	ldr	r5, [r4, #108]
	cmp	r5, #0
	bne	.L5
	ldr	r1, .L19+12
	mov	r2, #5
	add	r0, r4, #176
	bl	strlcpy
	ldr	r0, .L19+16
	bl	Alert_Message
	str	r5, [r4, #116]
.L5:
	ldr	r0, [r4, #140]
	ldr	r1, .L19+20
	add	r0, r0, #4
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	ldmnefd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.LBB19:
	str	r0, [r4, #112]
	ldr	r0, [r4, #140]
	ldr	r1, .L19+24
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L7
	ldr	r0, [r4, #140]
	ldr	r1, .L19+28
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L8
.L7:
	add	r0, r4, #181
	ldr	r1, .L19+24
	b	.L18
.L8:
	ldr	r0, [r4, #140]
	ldr	r1, .L19+32
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L10
	ldr	r0, [r4, #140]
	ldr	r1, .L19+36
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L11
.L10:
	add	r0, r4, #181
	ldr	r1, .L19+32
	b	.L18
.L11:
	ldr	r0, [r4, #140]
	ldr	r1, .L19+40
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L12
	ldr	r0, [r4, #140]
	ldr	r1, .L19+44
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L13
.L12:
	add	r0, r4, #181
	ldr	r1, .L19+40
	b	.L18
.L13:
	ldr	r0, [r4, #140]
	ldr	r1, .L19+48
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L14
	ldr	r0, [r4, #140]
	ldr	r1, .L19+52
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L15
.L14:
	add	r0, r4, #181
	ldr	r1, .L19+48
	b	.L18
.L15:
	ldr	r0, [r4, #140]
	ldr	r1, .L19+56
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L16
	ldr	r0, [r4, #140]
	ldr	r1, .L19+60
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L9
.L16:
	ldr	r1, .L19+56
	add	r0, r4, #181
.L18:
	mov	r2, #3
	bl	strlcpy
	mov	r3, #1
	str	r3, [r4, #112]
.L9:
.LBE19:
	ldr	r5, [r4, #112]
	cmp	r5, #0
	ldmnefd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
	ldr	r1, .L19+64
	mov	r2, #3
	add	r0, r4, #181
	bl	strlcpy
	ldr	r0, .L19+68
	bl	Alert_Message
	str	r5, [r4, #116]
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L2:
	ldr	r0, [r4, #140]
	str	r3, [r4, #116]
	ldr	r1, .L19+64
	mov	r2, #3
	add	r0, r0, #4
	bl	strlcpy
	add	r0, r4, #176
	ldr	r1, .L19+12
	mov	r2, #5
	bl	strlcpy
	add	r0, r4, #181
	ldr	r1, .L19+64
	mov	r2, #3
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L19+64
	mov	r2, #3
	add	r0, r0, #65
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L19+64
	mov	r2, #3
	add	r0, r0, #68
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L19+12
	add	r0, r0, #24
	mov	r2, #5
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	strlcpy
.L20:
	.align	2
.L19:
	.word	.LC0
	.word	36867
	.word	.LANCHOR0
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
.LFE13:
	.size	sr_final_radio_analysis, .-sr_final_radio_analysis
	.section	.text.sr_final_radio_verification,"ax",%progbits
	.align	2
	.type	sr_final_radio_verification, %function
sr_final_radio_verification:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r4, r0
	bl	sr_final_radio_analysis
.LBB24:
	ldr	r1, .L50
	mov	r2, #40
	add	r0, r4, #14
	bl	strlcpy
	ldr	r0, .L50+4
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	mov	r3, #1
	str	r3, [r4, #120]
	add	r0, r0, #4
	add	r1, r1, #4
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L22
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+8
	bl	Alert_Message
.L22:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #7
	add	r1, r1, #7
	mov	r2, #7
	bl	strncmp
	cmp	r0, #0
	beq	.L23
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+12
	bl	Alert_Message
.L23:
.LBB25:
	ldr	r0, [r4, #140]
	ldr	r1, .L50+16
	add	r0, r0, #4
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	ldr	r0, [r4, #140]
	bne	.L24
	add	r0, r0, #14
	ldr	r1, .L50+20
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L25
	ldr	r0, [r4, #132]
	ldr	r1, .L50+24
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L47
.L25:
	ldr	r0, [r4, #140]
	ldr	r1, .L50+28
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L27
	ldr	r0, [r4, #132]
	ldr	r1, .L50+32
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L47
.L27:
	ldr	r0, [r4, #140]
	ldr	r1, .L50+36
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L28
	ldr	r0, [r4, #132]
	ldr	r1, .L50+40
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L47
.L28:
	ldr	r0, [r4, #140]
	ldr	r1, .L50+44
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L29
	ldr	r0, [r4, #132]
	ldr	r1, .L50+48
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L47
.L29:
	ldr	r0, [r4, #140]
	ldr	r1, .L50+52
	add	r0, r0, #14
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L30
	ldr	r0, [r4, #132]
	ldr	r1, .L50+56
	add	r0, r0, #14
	b	.L49
.L24:
	ldrb	r3, [r0, #14]	@ zero_extendqisi2
	ldr	r1, [r4, #132]
	cmp	r3, #32
	addeq	r0, r0, #15
	addne	r0, r0, #14
	add	r1, r1, #14
.L49:
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L30
.L47:
.LBE25:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #17
	add	r1, r1, #17
	mov	r2, #2
	bl	strncmp
	cmp	r0, #0
	beq	.L32
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+60
	bl	Alert_Message
.L32:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #19
	add	r1, r1, #19
	mov	r2, #2
	bl	strncmp
	cmp	r0, #0
	beq	.L33
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+64
	bl	Alert_Message
.L33:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #24
	add	r1, r1, #24
	mov	r2, #2
	bl	strncmp
	cmp	r0, #0
	beq	.L34
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+68
	bl	Alert_Message
.L34:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #21
	add	r1, r1, #21
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L35
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+72
	bl	Alert_Message
.L35:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #29
	add	r1, r1, #29
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L36
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+76
	bl	Alert_Message
.L36:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #32
	add	r1, r1, #32
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L37
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+80
	bl	Alert_Message
.L37:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #35
	add	r1, r1, #35
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L38
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+84
	bl	Alert_Message
.L38:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #38
	add	r1, r1, #38
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L39
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+88
	bl	Alert_Message
.L39:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #41
	add	r1, r1, #41
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L40
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+92
	bl	Alert_Message
.L40:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #44
	add	r1, r1, #44
	mov	r2, #2
	bl	strncmp
	cmp	r0, #0
	beq	.L41
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+96
	bl	Alert_Message
.L41:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #46
	add	r1, r1, #46
	mov	r2, #5
	bl	strncmp
	cmp	r0, #0
	beq	.L42
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+100
	bl	Alert_Message
.L42:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #51
	add	r1, r1, #51
	mov	r2, #2
	bl	strncmp
	cmp	r0, #0
	beq	.L43
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+104
	bl	Alert_Message
.L43:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #53
	add	r1, r1, #53
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L44
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+108
	bl	Alert_Message
.L44:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #65
	add	r1, r1, #65
	mov	r2, #1
	bl	strncmp
	cmp	r0, #0
	beq	.L45
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+112
	bl	Alert_Message
.L45:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #132]
	add	r0, r0, #68
	add	r1, r1, #68
	mov	r2, #1
	bl	strncmp
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
	ldr	r0, .L50+116
	mov	r3, #0
	str	r3, [r4, #120]
.LBE24:
	ldmfd	sp!, {r4, lr}
.LBB26:
	b	Alert_Message
.L30:
	mov	r3, #0
	str	r3, [r4, #120]
	ldr	r0, .L50+120
	bl	Alert_Message
	b	.L47
.L51:
	.align	2
.L50:
	.word	.LC16
	.word	36870
	.word	.LC17
	.word	.LC18
	.word	.LC3
	.word	.LC5
	.word	.LC4
	.word	.LC7
	.word	.LC6
	.word	.LC9
	.word	.LC8
	.word	.LC11
	.word	.LC10
	.word	.LC13
	.word	.LC12
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	.LC23
	.word	.LC24
	.word	.LC25
	.word	.LC26
	.word	.LC27
	.word	.LC28
	.word	.LC29
	.word	.LC30
	.word	.LC31
	.word	.LC32
	.word	.LC33
	.word	.LC34
.LBE26:
.LFE16:
	.size	sr_final_radio_verification, .-sr_final_radio_verification
	.section	.text.sr_analyze_multipoint_parameters,"ax",%progbits
	.align	2
	.type	sr_analyze_multipoint_parameters, %function
sr_analyze_multipoint_parameters:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #140]
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI2:
	ldr	r1, .L59
	add	r3, r3, #32
	str	r3, [sp, #0]
	ldr	r3, .L59+4
	mov	r4, r0
	str	r3, [sp, #4]
	mov	r2, #21
	ldr	r0, [r0, #124]
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #35
	str	r3, [sp, #0]
	ldr	r3, .L59+8
	ldr	r1, .L59+12
	str	r3, [sp, #4]
	mov	r2, #21
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #38
	str	r3, [sp, #0]
	ldr	r3, .L59+16
	ldr	r1, .L59+20
	str	r3, [sp, #4]
	mov	r2, #21
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #41
	str	r3, [sp, #0]
	ldr	r3, .L59+24
	ldr	r1, .L59+28
	str	r3, [sp, #4]
	mov	r2, #21
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #44
	str	r3, [sp, #0]
	ldr	r3, .L59+32
	ldr	r1, .L59+36
	str	r3, [sp, #4]
	mov	r2, #22
	mov	r3, #2
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #46
	str	r3, [sp, #0]
	ldr	r3, .L59+40
	ldr	r1, .L59+44
	str	r3, [sp, #4]
	mov	r2, #19
	mov	r3, #5
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #51
	str	r3, [sp, #0]
	ldr	r3, .L59+48
	ldr	r1, .L59+52
	str	r3, [sp, #4]
	mov	r2, #22
	mov	r3, #2
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #53
	str	r3, [sp, #0]
	ldr	r3, .L59+56
	ldr	r1, .L59+60
	str	r3, [sp, #4]
	mov	r2, #21
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #56
	str	r3, [sp, #0]
	ldr	r3, .L59+64
	ldr	r1, .L59+68
	str	r3, [sp, #4]
	mov	r2, #19
	mov	r3, #9
	bl	dev_extract_text_from_buffer
	ldr	r0, [r4, #140]
	ldr	r1, .L59+72
	add	r0, r0, #56
	mov	r2, #7
	bl	strncmp
	cmp	r0, #0
	ldr	r0, [r4, #140]
	beq	.L57
.L53:
	add	r0, r0, #56
	ldr	r1, .L59+76
	mov	r2, #8
	bl	strncmp
	cmp	r0, #0
	ldr	r0, [r4, #140]
	bne	.L54
	ldr	r1, .L59+80
	add	r0, r0, #65
	mov	r2, #3
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L59+80
	add	r0, r0, #68
	b	.L58
.L54:
	add	r0, r0, #56
	ldr	r1, .L59+84
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	bne	.L55
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #65
	str	r3, [sp, #0]
	ldr	r3, .L59+88
	ldr	r1, .L59+84
	str	r3, [sp, #4]
	mov	r2, #4
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #68
	str	r3, [sp, #0]
	ldr	r3, .L59+92
	ldr	r1, .L59+96
	str	r3, [sp, #4]
	mov	r2, #5
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	add	sp, sp, #8
	ldmfd	sp!, {r4, pc}
.L55:
	ldr	r0, .L59+100
	bl	Alert_Message
	ldr	r0, [r4, #140]
.L57:
	ldr	r1, .L59+104
	add	r0, r0, #65
	mov	r2, #3
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L59+104
	add	r0, r0, #68
.L58:
	mov	r2, #3
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	strlcpy
.L60:
	.align	2
.L59:
	.word	.LC35
	.word	.LC36
	.word	.LC38
	.word	.LC37
	.word	.LC40
	.word	.LC39
	.word	.LC42
	.word	.LC41
	.word	.LC44
	.word	.LC43
	.word	.LC46
	.word	.LC45
	.word	.LC48
	.word	.LC47
	.word	.LC50
	.word	.LC49
	.word	.LC52
	.word	.LC51
	.word	.LC53
	.word	.LC55
	.word	.LC56
	.word	.LC57
	.word	.LC58
	.word	.LC60
	.word	.LC59
	.word	.LC61
	.word	.LC54
.LFE4:
	.size	sr_analyze_multipoint_parameters, .-sr_analyze_multipoint_parameters
	.section	.text.sr_analyze_radio_parameters,"ax",%progbits
	.align	2
	.type	sr_analyze_radio_parameters, %function
sr_analyze_radio_parameters:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #140]
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI3:
	ldr	r1, .L62
	add	r3, r3, #14
	str	r3, [sp, #0]
	ldr	r3, .L62+4
	mov	r4, r0
	str	r3, [sp, #4]
	mov	r2, #16
	ldr	r0, [r0, #124]
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #17
	str	r3, [sp, #0]
	ldr	r3, .L62+8
	ldr	r1, .L62+12
	str	r3, [sp, #4]
	mov	r2, #17
	mov	r3, #2
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #19
	str	r3, [sp, #0]
	ldr	r3, .L62+16
	ldr	r1, .L62+20
	str	r3, [sp, #4]
	mov	r2, #17
	mov	r3, #2
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #21
	str	r3, [sp, #0]
	ldr	r3, .L62+24
	ldr	r1, .L62+28
	str	r3, [sp, #4]
	mov	r2, #16
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #24
	str	r3, [sp, #0]
	ldr	r3, .L62+32
	ldr	r1, .L62+36
	str	r3, [sp, #4]
	mov	r2, #16
	mov	r3, #5
	bl	dev_extract_text_from_buffer
	ldr	r3, [r4, #140]
	ldr	r0, [r4, #124]
	add	r3, r3, #29
	str	r3, [sp, #0]
	ldr	r3, .L62+40
	ldr	r1, .L62+44
	str	r3, [sp, #4]
	mov	r2, #16
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldmfd	sp!, {r2, r3, r4, pc}
.L63:
	.align	2
.L62:
	.word	.LC62
	.word	.LC63
	.word	.LC65
	.word	.LC64
	.word	.LC67
	.word	.LC66
	.word	.LC69
	.word	.LC68
	.word	.LC71
	.word	.LC70
	.word	.LC73
	.word	.LC72
.LFE3:
	.size	sr_analyze_radio_parameters, .-sr_analyze_radio_parameters
	.section	.text.sr_analyze_set_baud_rate,"ax",%progbits
	.align	2
	.type	sr_analyze_set_baud_rate, %function
sr_analyze_set_baud_rate:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #140]
	stmfd	sp!, {r0, r1, lr}
.LCFI4:
	ldr	r1, .L65
	add	r3, r3, #7
	str	r3, [sp, #0]
	ldr	r3, .L65+4
	mov	r2, #15
	str	r3, [sp, #4]
	mov	r3, #7
	ldr	r0, [r0, #124]
	bl	dev_extract_text_from_buffer
	ldmfd	sp!, {r2, r3, pc}
.L66:
	.align	2
.L65:
	.word	.LC74
	.word	.LC75
.LFE2:
	.size	sr_analyze_set_baud_rate, .-sr_analyze_set_baud_rate
	.section	.text.sr_analyze_set_modem_mode,"ax",%progbits
	.align	2
	.type	sr_analyze_set_modem_mode, %function
sr_analyze_set_modem_mode:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #140]
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI5:
	ldr	r1, .L71
	add	r3, r3, #4
	str	r3, [sp, #0]
	ldr	r3, .L71+4
	mov	r4, r0
	str	r3, [sp, #4]
	mov	r2, #16
	ldr	r0, [r0, #124]
	mov	r3, #3
	bl	dev_extract_text_from_buffer
	ldr	r0, [r4, #140]
	ldr	r1, .L71+8
	add	r0, r0, #4
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L68
	ldr	r0, [r4, #140]
	ldr	r1, .L71+12
	add	r0, r0, #4
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	beq	.L68
	ldr	r0, [r4, #140]
	ldr	r1, .L71+16
	add	r0, r0, #4
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	movne	r3, #0
	bne	.L69
.L68:
	mov	r3, #1
.L69:
	str	r3, [r4, #104]
	ldmfd	sp!, {r2, r3, r4, pc}
.L72:
	.align	2
.L71:
	.word	.LC76
	.word	.LC77
	.word	.LC78
	.word	.LC79
	.word	.LC3
.LFE1:
	.size	sr_analyze_set_modem_mode, .-sr_analyze_set_modem_mode
	.section	.text.sr_analyze_main_menu,"ax",%progbits
	.align	2
	.type	sr_analyze_main_menu, %function
sr_analyze_main_menu:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #4]
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI6:
	cmp	r3, #82
	mov	r4, r0
	add	r0, r0, #14
	bne	.L74
	ldr	r1, .L77
	mov	r2, #40
	bl	strlcpy
	ldr	r0, .L77+4
	b	.L76
.L74:
	ldr	r1, .L77+8
	mov	r2, #40
	bl	strlcpy
	ldr	r0, .L77+12
.L76:
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	add	r3, r4, #144
	str	r3, [sp, #0]
	ldr	r3, .L77+16
	ldr	r0, [r4, #124]
	str	r3, [sp, #4]
	ldr	r1, .L77+20
	mov	r2, #15
	mov	r3, #13
	bl	dev_extract_text_from_buffer
	add	r3, r4, #157
	str	r3, [sp, #0]
	ldr	r3, .L77+24
	ldr	r0, [r4, #124]
	str	r3, [sp, #4]
	ldr	r1, .L77+28
	mov	r2, #20
	mov	r3, #9
	bl	dev_extract_text_from_buffer
	add	r3, r4, #166
	str	r3, [sp, #0]
	ldr	r3, .L77+32
	ldr	r0, [r4, #124]
	str	r3, [sp, #4]
	ldr	r1, .L77+36
	mov	r2, #11
	mov	r3, #5
	bl	dev_extract_text_from_buffer
	ldmfd	sp!, {r2, r3, r4, pc}
.L78:
	.align	2
.L77:
	.word	.LC80
	.word	36867
	.word	.LC81
	.word	36870
	.word	.LC83
	.word	.LC82
	.word	.LC85
	.word	.LC84
	.word	.LC87
	.word	.LC86
.LFE0:
	.size	sr_analyze_main_menu, .-sr_analyze_main_menu
	.section	.text.sr_get_and_process_command,"ax",%progbits
	.align	2
	.type	sr_get_and_process_command, %function
sr_get_and_process_command:
.LFB23:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI7:
	sub	sp, sp, #32
.LCFI8:
.LBB33:
.LBB34:
	add	r4, sp, #8
.LBE34:
.LBE33:
	mov	r5, r1
	mov	r6, r0
	mov	r7, r2
.LBB38:
.LBB36:
	mov	r0, r4
	mov	r1, #0
	mov	r2, #23
	bl	memset
	ldr	r3, .L141
	cmp	r5, r3
	beq	.L91
	bhi	.L105
	ldr	r3, .L141+4
	cmp	r5, r3
	beq	.L83
	bhi	.L106
	cmp	r5, #57
	bhi	.L107
	cmp	r5, #48
	bcs	.L81
	cmp	r5, #27
	bne	.L80
	b	.L81
.L107:
	cmp	r5, #65
	bcc	.L80
	cmp	r5, #71
	bls	.L81
	cmp	r5, #90
	b	.L130
.L106:
	ldr	r3, .L141+8
	cmp	r5, r3
	beq	.L87
	bhi	.L108
	ldr	r3, .L141+12
	cmp	r5, r3
	beq	.L85
	ldr	r3, .L141+16
	cmp	r5, r3
	beq	.L86
	ldr	r3, .L141+20
	cmp	r5, r3
	bne	.L80
	b	.L135
.L108:
	ldr	r3, .L141+24
	cmp	r5, r3
	moveq	r0, r4
	ldreq	r1, .L141+28
	beq	.L133
	ldr	r1, [r6, #140]
	movhi	r0, r4
	addhi	r1, r1, #21
	bhi	.L133
	b	.L136
.L105:
	ldr	r3, .L141+32
	cmp	r5, r3
	beq	.L98
	bhi	.L109
	sub	r3, r3, #4
	cmp	r5, r3
	beq	.L94
	bhi	.L110
	ldr	r3, .L141+36
	cmp	r5, r3
	beq	.L92
	ldr	r3, .L141+40
	cmp	r5, r3
	bne	.L80
	b	.L137
.L110:
	ldr	r3, .L141+44
	cmp	r5, r3
	ldreq	r1, [r6, #140]
	moveq	r0, r4
	addeq	r1, r1, #41
	beq	.L133
	movhi	r0, r4
	ldrhi	r1, .L141+48
	bhi	.L133
	b	.L138
.L109:
	ldr	r3, .L141+52
	cmp	r5, r3
	beq	.L102
	bhi	.L111
	sub	r3, r3, #2
	cmp	r5, r3
	beq	.L100
	ldrhi	r1, [r6, #140]
	movhi	r0, r4
	addhi	r1, r1, #53
	bhi	.L133
	sub	r3, r3, #4
	cmp	r5, r3
	bne	.L80
	b	.L139
.L111:
	ldr	r3, .L141+56
	cmp	r5, r3
	beq	.L104
	ldrcc	r1, [r6, #140]
	movcc	r0, r4
	addcc	r1, r1, #65
	bcc	.L133
	ldr	r3, .L141+60
	cmp	r5, r3
.L130:
	bne	.L80
	b	.L82
.L81:
	add	r0, sp, #8
	mov	r1, r5
	b	.L131
.L83:
	mov	r0, r4
	mov	r1, #255
.L131:
	mov	r2, #1
	bl	memset
	b	.L82
.L135:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #4
	b	.L133
.L85:
.LBB35:
	ldr	r3, .L141+64
	mov	r0, r4
	ldr	r3, [r3, #172]
	cmp	r3, #38400
	beq	.L115
	bhi	.L119
	cmp	r3, #9600
	beq	.L113
	cmp	r3, #19200
	bne	.L112
	b	.L140
.L119:
	ldr	r2, .L141+68
	cmp	r3, r2
	beq	.L117
	cmp	r3, #230400
	bne	.L112
	b	.L118
.L113:
	ldr	r1, .L141+72
	mov	r2, #4
	bl	strlcpy
	ldr	r0, [r6, #132]
	ldr	r1, .L141+76
	add	r0, r0, #7
.L134:
	mov	r2, #7
.L132:
	bl	strlcpy
	b	.L82
.L140:
	ldr	r1, .L141+80
	mov	r2, #4
	bl	strlcpy
	ldr	r0, [r6, #132]
	ldr	r1, .L141+84
	add	r0, r0, #7
	b	.L134
.L115:
	ldr	r1, .L141+88
	mov	r2, #4
	bl	strlcpy
	ldr	r0, [r6, #132]
	ldr	r1, .L141+92
	add	r0, r0, #7
	b	.L134
.L117:
	ldr	r1, .L141+28
	mov	r2, #4
	bl	strlcpy
	ldr	r0, [r6, #132]
	ldr	r1, .L141+96
	add	r0, r0, #7
	b	.L134
.L118:
	ldr	r1, .L141+48
	mov	r2, #4
	bl	strlcpy
	ldr	r0, [r6, #132]
	ldr	r1, .L141+100
	add	r0, r0, #7
	b	.L134
.L112:
	ldr	r1, .L141+104
	mov	r2, #4
	bl	strlcpy
	ldr	r0, [r6, #132]
	ldr	r1, .L141+108
	add	r0, r0, #7
	b	.L134
.L86:
.LBE35:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #14
	b	.L133
.L87:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #17
	b	.L133
.L136:
	mov	r0, r4
	add	r1, r1, #19
	b	.L133
.L91:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #24
	b	.L133
.L92:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #29
	b	.L133
.L137:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #32
	b	.L133
.L94:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #35
	b	.L133
.L138:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #38
	b	.L133
.L98:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #44
	b	.L133
.L139:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #46
	b	.L133
.L100:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #51
	b	.L133
.L102:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #56
.L133:
	mov	r2, #23
	b	.L132
.L104:
	ldr	r1, [r6, #140]
	mov	r0, r4
	add	r1, r1, #68
	b	.L133
.L80:
	ldr	r0, .L141+112
	bl	Alert_Message
.L82:
	ldrb	r0, [sp, #8]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L120
	ldr	r3, .L141+4
	cmp	r5, r3
	bls	.L121
	add	r0, sp, #8
	ldr	r1, .L141+116
	mov	r2, #23
	bl	strlcat
.L121:
	add	r0, sp, #8
	bl	strlen
	ldr	r1, .L141+120
	ldr	r2, .L141+124
	mov	r6, #0
	add	r0, r0, #1
	bl	mem_malloc_debug
	mov	r4, r0
	add	r0, sp, #8
	bl	strlen
	add	r1, sp, #8
	mov	r2, r0
	mov	r0, r4
	bl	memcpy
	add	r0, sp, #8
	bl	strlen
.LBE36:
.LBE38:
	cmp	r4, r6
.LBB39:
.LBB37:
	strb	r6, [r4, r0]
.LBE37:
.LBE39:
	moveq	r0, r4
	beq	.L120
.LBB40:
.LBB41:
	ldr	r5, .L141+128
	mov	r1, #500
	mov	r2, #100
	ldr	r0, [r5, #368]
	bl	RCVD_DATA_enable_hunting_mode
	mov	r0, r4
	bl	strlen
	ldr	sl, .L141+132
	ldr	r2, [r5, #368]
	ldr	r3, .L141+136
	mla	sl, r2, sl, r3
	ldr	r3, .L141+140
	str	r7, [sl, r3]
	mov	r8, r0
	mov	r0, r7
	bl	strlen
	ldr	r3, .L141+144
	cmp	r8, r6
	str	r0, [sl, r3]
	beq	.L122
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L122
	mov	r2, #2
	mov	r3, #1
	ldr	r0, [r5, #368]
	mov	r1, r4
	stmia	sp, {r2, r3}
	mov	r2, r8
	mov	r3, r6
	bl	AddCopyOfBlockToXmitList
.L122:
.LBE41:
.LBE40:
	mov	r0, r4
	ldr	r1, .L141+120
	ldr	r2, .L141+148
	bl	mem_free_debug
	mov	r0, #1
.L120:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L142:
	.align	2
.L141:
	.word	13006
	.word	5000
	.word	13002
	.word	11000
	.word	13001
	.word	10000
	.word	13004
	.word	.LC95
	.word	15006
	.word	13010
	.word	15001
	.word	15004
	.word	.LC54
	.word	15013
	.word	15015
	.word	99999
	.word	port_device_table
	.word	115200
	.word	.LC88
	.word	.LC89
	.word	.LC90
	.word	.LC91
	.word	.LC92
	.word	.LC93
	.word	.LC96
	.word	.LC97
	.word	.LC79
	.word	.LC94
	.word	.LC98
	.word	.LC99
	.word	.LC100
	.word	1402
	.word	comm_mngr
	.word	4280
	.word	SerDrvrVars_s
	.word	4216
	.word	4220
	.word	1718
.LFE23:
	.size	sr_get_and_process_command, .-sr_get_and_process_command
	.section	.text.send_no_response_esc_to_radio,"ax",%progbits
	.align	2
	.type	send_no_response_esc_to_radio, %function
send_no_response_esc_to_radio:
.LFB20:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI9:
	ldr	r5, .L144
	add	r4, sp, #12
	mov	r3, #27
	ldr	r0, [r5, #368]
	mov	r1, #100
	mov	r2, #0
	strb	r3, [r4, #-1]!
	bl	RCVD_DATA_enable_hunting_mode
	mov	r3, #2
	mov	r2, #1
	str	r3, [sp, #0]
	ldr	r0, [r5, #368]
	mov	r1, r4
	mov	r3, #0
	str	r2, [sp, #4]
	bl	AddCopyOfBlockToXmitList
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L145:
	.align	2
.L144:
	.word	comm_mngr
.LFE20:
	.size	send_no_response_esc_to_radio, .-send_no_response_esc_to_radio
	.section	.text.exit_radio_programming_mode,"ax",%progbits
	.align	2
	.type	exit_radio_programming_mode, %function
exit_radio_programming_mode:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI10:
	bl	send_no_response_esc_to_radio
.LBB42:
	ldr	r4, .L147
	ldr	r0, [r4, #368]
	bl	set_reset_INACTIVE_to_serial_port_device
.LBE42:
	ldr	r3, .L147+4
	ldr	r0, [r4, #368]
	ldr	r1, [r3, #172]
	bl	SERIAL_set_baud_rate_on_A_or_B
	ldr	r3, .L147+8
	str	r3, [r4, #372]
	ldmfd	sp!, {r4, pc}
.L148:
	.align	2
.L147:
	.word	comm_mngr
	.word	port_device_table
	.word	6000
.LFE21:
	.size	exit_radio_programming_mode, .-exit_radio_programming_mode
	.section	.text.process_list.isra.0,"ax",%progbits
	.align	2
	.type	process_list.isra.0, %function
process_list.isra.0:
.LFB36:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r1, #0]
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI11:
	cmp	r3, #4864
	mov	r6, r0
	mov	r4, r1
	mov	r5, r2
	bne	.L150
	ldr	r0, [r1, #16]
	cmp	r0, #0
	beq	.L151
	ldr	r2, [r1, #20]
	cmp	r2, #0
	beq	.L151
	ldr	r1, [r5, #0]
	bl	find_string_in_block
	subs	r7, r0, #0
	beq	.L152
	ldr	r3, [r4, #16]
	str	r7, [r6, #124]
	rsb	r7, r7, r3
	ldr	r3, [r4, #20]
	add	r7, r7, r3
	ldr	r3, [r5, #4]
	str	r7, [r6, #128]
	cmp	r3, #0
	beq	.L158
	mov	r0, r6
	blx	r3
	b	.L158
.L152:
	ldr	r0, .L160
	bl	Alert_Message
	b	.L153
.L158:
	mov	r7, #1
.L153:
	ldr	r0, [r4, #16]
	ldr	r1, .L160+4
	mov	r2, #2016
	bl	mem_free_debug
	cmp	r7, #0
	bne	.L151
	b	.L154
.L150:
	cmp	r3, #5120
	ldreq	r0, .L160+8
	ldrne	r0, .L160+12
	bl	Alert_Message
	b	.L154
.L151:
	mov	r0, r6
	ldr	r1, [r5, #8]
	ldr	r2, [r5, #12]
	bl	sr_get_and_process_command
	adds	r0, r0, #0
	movne	r0, #1
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L154:
	bl	send_no_response_esc_to_radio
	bl	send_no_response_esc_to_radio
	bl	send_no_response_esc_to_radio
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L161:
	.align	2
.L160:
	.word	.LC101
	.word	.LC100
	.word	.LC102
	.word	.LC103
.LFE36:
	.size	process_list.isra.0, .-process_list.isra.0
	.section	.text.SR_FREEWAVE_power_control,"ax",%progbits
	.align	2
	.global	SR_FREEWAVE_power_control
	.type	SR_FREEWAVE_power_control, %function
SR_FREEWAVE_power_control:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI12:
	mov	r4, r0
	mov	r5, r1
	bl	set_reset_INACTIVE_to_serial_port_device
	mov	r0, r4
	mov	r1, r5
	ldmfd	sp!, {r4, r5, lr}
	b	GENERIC_freewave_card_power_control
.LFE31:
	.size	SR_FREEWAVE_power_control, .-SR_FREEWAVE_power_control
	.section	.text.SR_FREEWAVE_initialize_device_exchange,"ax",%progbits
	.align	2
	.global	SR_FREEWAVE_initialize_device_exchange
	.type	SR_FREEWAVE_initialize_device_exchange, %function
SR_FREEWAVE_initialize_device_exchange:
.LFB32:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L179
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, sl, lr}
.LCFI13:
	ldr	r4, [r3, #0]
.LBB62:
	ldr	r0, [r4, #132]
	cmp	r0, #0
	beq	.L164
	ldr	r1, .L179+4
	mov	r2, #4
	bl	strncmp
	cmp	r0, #0
	bne	.L164
	ldr	r0, [r4, #136]
	cmp	r0, #0
	beq	.L164
	ldr	r1, .L179+4
	mov	r2, #4
	bl	strncmp
	cmp	r0, #0
	beq	.L165
.L164:
.LBE62:
	ldr	r3, .L179
.LBB63:
	ldr	r0, .L179+8
.LBE63:
	ldr	r4, [r3, #0]
.LBB64:
	ldr	r3, .L179+12
	ldr	r1, .L179+4
	str	r3, [r4, #136]
	mov	r2, #4
	str	r0, [r4, #132]
	bl	strlcpy
	ldr	r1, .L179+4
	mov	r2, #4
	ldr	r0, [r4, #136]
	bl	strlcpy
	ldr	r1, .L179+16
	mov	r2, #13
	add	r0, r4, #144
	bl	strlcpy
	ldr	r1, .L179+16
	mov	r2, #9
	add	r0, r4, #157
	bl	strlcpy
	ldr	r1, .L179+16
	mov	r2, #5
	add	r0, r4, #166
	bl	strlcpy
	mov	r3, #0
	ldr	r1, .L179+16
	mov	r2, #5
	str	r3, [r4, #172]
	add	r0, r4, #176
	bl	strlcpy
	add	r0, r4, #181
	ldr	r1, .L179+16
	mov	r2, #3
	bl	strlcpy
.L165:
.LBE64:
	ldr	r3, .L179
.LBB65:
	mov	r5, #0
.LBE65:
	ldr	r4, [r3, #0]
.LBB79:
	ldr	r1, .L179+16
	mov	r6, r4
	strh	r5, [r6], #8	@ movhi
	mov	r2, #6
	mov	r0, r6
	add	r7, r4, #14
	ldr	r8, .L179+20
	bl	strlcpy
	ldr	r1, .L179+16
	mov	r2, #40
	mov	r0, r7
	bl	strlcpy
	add	r0, r4, #54
	ldr	r1, .L179+16
	mov	r2, #40
	bl	strlcpy
	ldr	r3, [r8, #364]
	cmp	r3, #4608
	bne	.L166
	ldr	r0, .L179+24
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
.LBB66:
	mov	r3, #87
	str	r3, [r4, #4]
	mov	r0, r6
	ldr	r1, .L179+28
	mov	r2, #6
	bl	strlcpy
	mov	r0, r7
	ldr	r1, .L179+32
	mov	r2, #40
	bl	strlcpy
	ldr	r0, .L179+24
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
.LBB67:
.LBB68:
	ldr	r7, .L179+36
.LBE68:
.LBE67:
	ldr	r3, [r4, #132]
.LBB76:
.LBB69:
	add	r8, r4, #176
	mov	r6, #20
.LBE69:
.LBE76:
	str	r5, [r4, #100]
	str	r5, [r4, #96]
	str	r3, [r4, #140]
.LBB77:
.LBB70:
	str	r5, [r4, #108]
.L168:
	mla	sl, r6, r5, r7
	mov	r0, r8
	mov	r1, sl
	mov	r2, #5
	bl	strncmp
	cmp	r0, #0
	bne	.L167
	mov	r1, sl
	mov	r2, #5
	str	r5, [r4, #172]
	mov	r0, r8
	bl	strlcpy
	ldr	r1, [r4, #172]
	ldr	r0, [r4, #140]
	mla	r1, r6, r1, r7
	mov	r2, #3
	add	r1, r1, #5
	add	r0, r0, #14
	bl	strlcpy
	ldr	r1, [r4, #172]
	ldr	r0, [r4, #140]
	mla	r1, r6, r1, r7
	mov	r2, #2
	add	r1, r1, #8
	add	r0, r0, #17
	bl	strlcpy
	ldr	r1, [r4, #172]
	ldr	r0, [r4, #140]
	mla	r1, r6, r1, r7
	mov	r2, #2
	add	r1, r1, #10
	add	r0, r0, #19
	bl	strlcpy
	ldr	r1, [r4, #172]
	ldr	r0, [r4, #140]
	mla	r1, r6, r1, r7
	add	r0, r0, #46
	add	r1, r1, #12
	mov	r2, #5
	bl	strlcpy
	mov	r3, #1
	str	r3, [r4, #108]
.L167:
	add	r5, r5, #1
	cmp	r5, #10
	bne	.L168
.LBE70:
	ldr	r0, [r4, #140]
	ldr	r1, .L179+40
	add	r0, r0, #4
	mov	r2, #3
	bl	strncmp
	cmp	r0, #0
	ldr	r0, [r4, #140]
	bne	.L169
.LBB71:
	ldr	r1, .L179+44
	mov	r2, #3
	add	r0, r0, #14
	bl	strncmp
	mov	r2, #3
	cmp	r0, #0
	ldr	r0, [r4, #140]
	ldreq	r1, .L179+48
	addne	r1, r4, #181
	add	r0, r0, #14
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+52
	mov	r2, #3
	add	r0, r0, #21
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+56
	mov	r2, #3
	add	r0, r0, #29
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+60
	mov	r2, #2
	add	r0, r0, #44
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+60
	mov	r2, #2
	add	r0, r0, #51
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+56
	mov	r2, #3
	add	r0, r0, #53
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+64
	mov	r2, #3
	add	r0, r0, #32
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+68
	mov	r2, #3
	add	r0, r0, #35
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+68
	mov	r2, #3
	add	r0, r0, #38
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+56
	add	r0, r0, #41
	mov	r2, #3
	bl	strlcpy
	mov	r3, #1
	str	r3, [r4, #112]
	b	.L172
.L169:
.LBE71:
	ldr	r1, .L179+72
	mov	r2, #3
	add	r0, r0, #4
	bl	strncmp
.LBB72:
	ldr	r1, .L179+52
	mov	r2, #3
.LBE72:
	cmp	r0, #0
	ldr	r0, [r4, #140]
.LBB73:
	add	r0, r0, #21
.LBE73:
	bne	.L173
.LBB74:
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+56
	mov	r2, #3
	add	r0, r0, #29
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+76
	mov	r2, #2
	add	r0, r0, #44
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+76
	mov	r2, #2
	add	r0, r0, #51
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+80
	mov	r2, #3
	add	r0, r0, #53
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+76
	add	r0, r0, #65
	mov	r2, #3
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+76
	add	r0, r0, #68
	b	.L178
.L173:
.LBE74:
.LBB75:
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+64
	mov	r2, #3
	add	r0, r0, #29
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+76
	mov	r2, #2
	add	r0, r0, #44
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+76
	mov	r2, #2
	add	r0, r0, #51
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+56
	add	r0, r0, #53
	mov	r2, #3
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+84
	add	r0, r0, #68
.L178:
	mov	r2, #3
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+64
	mov	r2, #3
	add	r0, r0, #32
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+68
	mov	r2, #3
	add	r0, r0, #35
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+68
	mov	r2, #3
	add	r0, r0, #38
	bl	strlcpy
	ldr	r0, [r4, #140]
	ldr	r1, .L179+56
	add	r0, r0, #41
	mov	r2, #3
	bl	strlcpy
.L172:
.LBE75:
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #3
	add	r1, r1, #4
	add	r0, r0, #4
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #7
	add	r1, r1, #7
	add	r0, r0, #7
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #3
	add	r1, r1, #14
	add	r0, r0, #14
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #2
	add	r1, r1, #17
	add	r0, r0, #17
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #2
	add	r1, r1, #19
	add	r0, r0, #19
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #5
	add	r1, r1, #24
	add	r0, r0, #24
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #3
	add	r1, r1, #21
	add	r0, r0, #21
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #3
	add	r1, r1, #29
	add	r0, r0, #29
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #3
	add	r1, r1, #32
	add	r0, r0, #32
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #3
	add	r1, r1, #35
	add	r0, r0, #35
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #3
	add	r1, r1, #38
	add	r0, r0, #38
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #3
	add	r1, r1, #41
	add	r0, r0, #41
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #2
	add	r1, r1, #44
	add	r0, r0, #44
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #5
	add	r1, r1, #46
	add	r0, r0, #46
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #2
	add	r1, r1, #51
	add	r0, r0, #51
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #3
	add	r1, r1, #53
	add	r0, r0, #53
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #9
	add	r1, r1, #56
	add	r0, r0, #56
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #3
	add	r1, r1, #65
	add	r0, r0, #65
	bl	strlcpy
	ldr	r0, [r4, #136]
	ldr	r1, [r4, #140]
	mov	r2, #3
	add	r0, r0, #68
	add	r1, r1, #68
	bl	strlcpy
.LBE77:
	ldr	r3, [r4, #136]
	ldr	r2, .L179+88
	str	r3, [r4, #140]
	ldr	r3, .L179+20
	str	r2, [r3, #372]
	b	.L174
.L166:
.LBE66:
	ldr	r0, .L179+92
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
.LBB78:
	mov	r3, #82
	str	r3, [r4, #4]
	ldr	r1, .L179+96
	mov	r2, #6
	mov	r0, r6
	bl	strlcpy
	ldr	r1, .L179+100
	mov	r2, #40
	mov	r0, r7
	bl	strlcpy
	ldr	r0, .L179+92
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	ldr	r3, [r4, #132]
	str	r5, [r4, #96]
	str	r3, [r4, #140]
	mov	r3, #4000
	str	r5, [r4, #100]
	str	r3, [r8, #372]
.L174:
.LBE78:
	mov	r5, #0
	str	r5, [r4, #104]
	str	r5, [r4, #108]
	str	r5, [r4, #112]
	str	r5, [r4, #116]
.LBE79:
	ldr	r4, .L179
	ldr	r3, .L179+104
	ldr	r0, [r4, #0]
	ldr	r2, [r0, #96]
	add	r3, r3, r2, asl #4
	ldr	r1, [r3, #8]
	ldr	r2, [r3, #12]
	bl	sr_get_and_process_command
	cmp	r0, r5
	beq	.L163
.LBB80:
	ldr	r0, [r4, #0]
	ldr	r4, .L179+20
	mov	r2, #40
	ldr	r1, .L179+108
	add	r0, r0, #14
	bl	strlcpy
	ldr	r0, .L179+92
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	mov	r1, #19200
	ldr	r0, [r4, #368]
	bl	SERIAL_set_baud_rate_on_A_or_B
	ldr	r0, [r4, #368]
	bl	set_reset_ACTIVE_to_serial_port_device
	mov	r0, #20
	bl	vTaskDelay
	ldr	r0, [r4, #368]
	bl	set_reset_INACTIVE_to_serial_port_device
.LBE80:
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	ldr	r0, [r4, #384]
	mov	r2, #2000
	mov	r3, r5
	bl	xTimerGenericCommand
.L163:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, pc}
.L180:
	.align	2
.L179:
	.word	.LANCHOR1
	.word	.LC104
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LC105
	.word	comm_mngr
	.word	36870
	.word	.LC106
	.word	.LC107
	.word	.LANCHOR0
	.word	.LC3
	.word	.LC14
	.word	.LC4
	.word	.LC108
	.word	.LC109
	.word	.LC95
	.word	.LC110
	.word	.LC111
	.word	.LC78
	.word	.LC54
	.word	.LC112
	.word	.LC56
	.word	5000
	.word	36867
	.word	.LC113
	.word	.LC114
	.word	.LANCHOR4
	.word	.LC115
.LFE32:
	.size	SR_FREEWAVE_initialize_device_exchange, .-SR_FREEWAVE_initialize_device_exchange
	.section	.text.SR_FREEWAVE_exchange_processing,"ax",%progbits
	.align	2
	.global	SR_FREEWAVE_exchange_processing
	.type	SR_FREEWAVE_exchange_processing, %function
SR_FREEWAVE_exchange_processing:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, lr}
.LCFI14:
	ldr	r4, .L203
	mov	r2, #0
	mvn	r5, #0
	str	r5, [sp, #0]
	mov	r3, r2
	mov	r6, r0
	mov	r1, #1
	ldr	r0, [r4, #384]
	bl	xTimerGenericCommand
	ldr	r3, [r4, #0]
	cmp	r3, #5
	bne	.L181
	ldr	r3, .L203+4
	ldr	r7, [r3, #0]
.LBB83:
	ldr	r3, [r4, #372]
	cmp	r3, #4000
	beq	.L184
	ldr	r2, .L203+8
	cmp	r3, r2
	bne	.L196
	b	.L202
.L184:
	ldr	r3, [r7, #96]
	ldr	r2, .L203+12
	add	r3, r3, #1
	str	r3, [r7, #96]
	mov	r0, r7
	mov	r1, r6
	add	r2, r2, r3, asl #4
	bl	process_list.isra.0
	cmp	r0, #0
	bne	.L191
	bl	exit_radio_programming_mode
	ldr	r1, [r7, #96]
	cmp	r1, #1
	bne	.L187
	ldr	r0, .L203+16
	bl	Alert_Message
	add	r0, r7, #14
	ldr	r1, .L203+20
	b	.L197
.L187:
	cmp	r1, #8
	add	r4, r7, #14
	bhi	.L189
	ldr	r0, .L203+24
	bl	Alert_Message_va
	ldr	r1, .L203+28
	mov	r0, r4
.L197:
	mov	r2, #40
	bl	strlcpy
	ldr	r4, .L203+32
	b	.L188
.L189:
	ldr	r3, [r7, #116]
	mov	r0, r4
	cmp	r3, #0
	ldrne	r1, .L203+36
	ldreq	r1, .L203+40
	mov	r2, #40
	bl	strlcpy
	ldr	r4, .L203+44
.L188:
	add	r7, r7, #54
	ldr	r1, .L203+48
	mov	r2, #40
	ldr	r5, .L203+52
	mov	r0, r7
	bl	strlcpy
	ldr	r0, .L203+56
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	mov	r3, #1
	str	r3, [r5, #0]
	bl	DEVICE_EXCHANGE_draw_dialog
	mov	r3, #0
	str	r3, [r5, #0]
	mov	r0, r7
	ldr	r1, .L203+60
	mov	r2, #40
	bl	strlcpy
	ldr	r0, .L203+56
	b	.L201
.L202:
	ldr	r3, [r7, #100]
	ldr	r2, .L203+64
	add	r3, r3, #1
	str	r3, [r7, #100]
	mov	r0, r7
	mov	r1, r6
	add	r2, r2, r3, asl #4
	bl	process_list.isra.0
	cmp	r0, #0
	bne	.L191
	bl	exit_radio_programming_mode
	ldr	r1, [r7, #100]
	cmp	r1, #1
	bne	.L192
	ldr	r0, .L203+68
	bl	Alert_Message
	add	r0, r7, #14
	ldr	r1, .L203+20
	b	.L199
.L192:
	cmp	r1, #49
	add	r4, r7, #14
	bhi	.L194
	ldr	r0, .L203+72
	bl	Alert_Message_va
	ldr	r1, .L203+76
	mov	r0, r4
.L199:
	mov	r2, #40
	bl	strlcpy
	ldr	r4, .L203+80
	b	.L193
.L194:
	ldr	r3, [r7, #120]
	mov	r0, r4
	cmp	r3, #0
	beq	.L195
	ldr	r1, .L203+84
	mov	r2, #40
	bl	strlcpy
	b	.L200
.L195:
	ldr	r1, .L203+88
	mov	r2, #40
	bl	strlcpy
	ldr	r0, .L203+92
	bl	Alert_Message
.L200:
	ldr	r4, .L203+96
.L193:
	add	r7, r7, #54
	ldr	r1, .L203+48
	mov	r2, #40
	ldr	r5, .L203+52
	mov	r0, r7
	bl	strlcpy
	ldr	r0, .L203+100
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	mov	r3, #1
	str	r3, [r5, #0]
	bl	DEVICE_EXCHANGE_draw_dialog
	mov	r3, #0
	str	r3, [r5, #0]
	mov	r0, r7
	ldr	r1, .L203+60
	mov	r2, #40
	bl	strlcpy
	ldr	r0, .L203+100
.L201:
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	mov	r0, r4
.LBE83:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, lr}
.LBB84:
	b	COMM_MNGR_device_exchange_results_to_key_process_task
.L191:
	str	r5, [sp, #0]
	mov	r1, #2
	ldr	r0, [r4, #384]
	mov	r2, #2000
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L181
.L196:
	ldr	r1, .L203+104
	mov	r3, #73
	add	r0, r7, #8
	mov	r2, #6
	str	r3, [r7, #4]
.LBE84:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, lr}
.LBB85:
	b	strlcpy
.L181:
.LBE85:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L204:
	.align	2
.L203:
	.word	comm_mngr
	.word	.LANCHOR1
	.word	5000
	.word	.LANCHOR4
	.word	.LC116
	.word	.LC117
	.word	.LC118
	.word	.LC119
	.word	36866
	.word	.LC120
	.word	.LC121
	.word	36865
	.word	.LC122
	.word	GuiVar_DeviceExchangeSyncingRadios
	.word	36867
	.word	.LC105
	.word	.LANCHOR5
	.word	.LC123
	.word	.LC124
	.word	.LC125
	.word	36869
	.word	.LC126
	.word	.LC127
	.word	.LC128
	.word	36868
	.word	36870
	.word	.LC129
.LFE33:
	.size	SR_FREEWAVE_exchange_processing, .-SR_FREEWAVE_exchange_processing
	.section	.text.SR_FREEWAVE_initialize_the_connection_process,"ax",%progbits
	.align	2
	.global	SR_FREEWAVE_initialize_the_connection_process
	.type	SR_FREEWAVE_initialize_the_connection_process, %function
SR_FREEWAVE_initialize_the_connection_process:
.LFB34:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI15:
	mov	r6, r0
	beq	.L206
	ldr	r0, .L208
	bl	Alert_Message
.L206:
	ldr	r3, .L208+4
	ldr	r2, .L208+8
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #44]
	cmp	r3, #0
	bne	.L207
	ldr	r0, .L208+12
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_Message
.L207:
	ldr	r3, .L208+16
	ldr	r4, .L208+20
	ldr	r3, [r3, #0]
	mov	r5, #0
	mov	r0, r6
	str	r3, [r4, #12]
	str	r6, [r4, #4]
	str	r5, [r4, #8]
	bl	power_down_device
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L208+24
	mov	r1, #2
	ldr	r0, [r3, #40]
	mov	r2, #1000
	mov	r3, r5
	bl	xTimerGenericCommand
	mov	r3, #1
	str	r3, [r4, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, pc}
.L209:
	.align	2
.L208:
	.word	.LC130
	.word	config_c
	.word	port_device_table
	.word	.LC131
	.word	my_tick_count
	.word	.LANCHOR6
	.word	cics
.LFE34:
	.size	SR_FREEWAVE_initialize_the_connection_process, .-SR_FREEWAVE_initialize_the_connection_process
	.global	__udivsi3
	.section	.text.SR_FREEWAVE_connection_processing,"ax",%progbits
	.align	2
	.global	SR_FREEWAVE_connection_processing
	.type	SR_FREEWAVE_connection_processing, %function
SR_FREEWAVE_connection_processing:
.LFB35:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI16:
	sub	r0, r0, #121
	cmp	r0, #1
	bls	.L211
	ldr	r0, .L218
	bl	Alert_Message
	b	.L212
.L211:
	ldr	r4, .L218+4
	ldr	r5, [r4, #0]
	cmp	r5, #1
	beq	.L214
	cmp	r5, #2
	bne	.L210
	b	.L217
.L214:
	ldr	r0, [r4, #4]
	bl	power_up_device
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L218+8
	mov	r1, #2
	ldr	r0, [r3, #40]
	mov	r2, #2000
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r3, #2
	str	r3, [r4, #0]
	b	.L210
.L217:
	ldr	r3, .L218+12
	ldr	r2, .L218+16
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #44]
	cmp	r3, #0
	beq	.L212
	ldr	r0, [r4, #4]
	blx	r3
	subs	r3, r0, #0
	beq	.L216
	mov	r2, #49
	ldr	r1, .L218+20
	ldr	r0, .L218+24
	bl	strlcpy
	ldr	r3, .L218+28
	mov	r1, #200
	ldr	r0, [r3, #0]
	ldr	r3, [r4, #12]
	rsb	r0, r3, r0
	bl	__udivsi3
	mov	r1, r0
	ldr	r0, .L218+32
	bl	Alert_Message_va
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities
.L216:
	ldr	r2, [r4, #8]
	cmp	r2, #119
	bhi	.L212
	add	r2, r2, #1
	str	r2, [r4, #8]
	mvn	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L218+8
	mov	r1, r5
	ldr	r0, [r2, #40]
	mov	r2, #200
	bl	xTimerGenericCommand
	b	.L210
.L212:
	ldr	r3, .L218+8
	mov	r2, #1
	mov	r0, #123
	str	r2, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	CONTROLLER_INITIATED_post_event
.L210:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, pc}
.L219:
	.align	2
.L218:
	.word	.LC132
	.word	.LANCHOR6
	.word	cics
	.word	config_c
	.word	port_device_table
	.word	.LC133
	.word	GuiVar_CommTestStatus
	.word	my_tick_count
	.word	.LC134
.LFE35:
	.size	SR_FREEWAVE_connection_processing, .-SR_FREEWAVE_connection_processing
	.global	sr_write_list
	.global	sr_read_list
	.global	sr_group_list
	.global	sr_state
	.section	.data.sr_read_list,"aw",%progbits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	sr_read_list, %object
	.size	sr_read_list, 160
sr_read_list:
	.word	.LC135
	.word	0
	.word	5000
	.word	.LC136
	.word	.LC137
	.word	sr_analyze_main_menu
	.word	48
	.word	.LC136
	.word	.LC138
	.word	sr_analyze_set_modem_mode
	.word	27
	.word	.LC136
	.word	.LC137
	.word	0
	.word	49
	.word	.LC136
	.word	.LC139
	.word	sr_analyze_set_baud_rate
	.word	27
	.word	.LC136
	.word	.LC137
	.word	0
	.word	51
	.word	.LC136
	.word	.LC143
	.word	sr_analyze_radio_parameters
	.word	27
	.word	.LC136
	.word	.LC137
	.word	0
	.word	53
	.word	.LC136
	.word	.LC158
	.word	sr_analyze_multipoint_parameters
	.word	27
	.word	.LC136
	.word	.LC137
	.word	sr_final_radio_analysis
	.word	99999
	.word	.LC105
	.section	.bss.sr_cs,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	sr_cs, %object
	.size	sr_cs, 16
sr_cs:
	.space	16
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Performing final analysis...\000"
.LC1:
	.ascii	"9999\000"
.LC2:
	.ascii	"SR2\000"
.LC3:
	.ascii	"7\000"
.LC4:
	.ascii	"A\000"
.LC5:
	.ascii	"10\000"
.LC6:
	.ascii	"B\000"
.LC7:
	.ascii	"11\000"
.LC8:
	.ascii	"C\000"
.LC9:
	.ascii	"12\000"
.LC10:
	.ascii	"D\000"
.LC11:
	.ascii	"13\000"
.LC12:
	.ascii	"E\000"
.LC13:
	.ascii	"14\000"
.LC14:
	.ascii	"99\000"
.LC15:
	.ascii	"SR3\000"
.LC16:
	.ascii	"Performing final verification...\000"
.LC17:
	.ascii	"SR4\000"
.LC18:
	.ascii	"SR5\000"
.LC19:
	.ascii	"SR9\000"
.LC20:
	.ascii	"SR10\000"
.LC21:
	.ascii	"SR11\000"
.LC22:
	.ascii	"SR12\000"
.LC23:
	.ascii	"SR13\000"
.LC24:
	.ascii	"SR14\000"
.LC25:
	.ascii	"SR15\000"
.LC26:
	.ascii	"SR16\000"
.LC27:
	.ascii	"SR17\000"
.LC28:
	.ascii	"SR18\000"
.LC29:
	.ascii	"SR19\000"
.LC30:
	.ascii	"SR20\000"
.LC31:
	.ascii	"SR21\000"
.LC32:
	.ascii	"SR22\000"
.LC33:
	.ascii	"SR23\000"
.LC34:
	.ascii	"SR8\000"
.LC35:
	.ascii	"Number Repeaters\000"
.LC36:
	.ascii	"NREP\000"
.LC37:
	.ascii	"Master Packet Repeat\000"
.LC38:
	.ascii	"MPREP\000"
.LC39:
	.ascii	"Max Slave Retry\000"
.LC40:
	.ascii	"SRETRY\000"
.LC41:
	.ascii	"Retry Odds\000"
.LC42:
	.ascii	"RODDS\000"
.LC43:
	.ascii	"Repeater Frequency\000"
.LC44:
	.ascii	"RFREQ\000"
.LC45:
	.ascii	"NetWork ID\000"
.LC46:
	.ascii	"NETID\000"
.LC47:
	.ascii	"Slave/Repeater\000"
.LC48:
	.ascii	"S_R\000"
.LC49:
	.ascii	"Diagnostics\000"
.LC50:
	.ascii	"DIAG\000"
.LC51:
	.ascii	"SubNet ID\000"
.LC52:
	.ascii	"SNID\000"
.LC53:
	.ascii	"Roaming\000"
.LC54:
	.ascii	"0\000"
.LC55:
	.ascii	"Disabled\000"
.LC56:
	.ascii	"F\000"
.LC57:
	.ascii	"Rcv=\000"
.LC58:
	.ascii	"RCVID\000"
.LC59:
	.ascii	"Xmit=\000"
.LC60:
	.ascii	"XMTID\000"
.LC61:
	.ascii	"SR1\000"
.LC62:
	.ascii	"FreqKey\000"
.LC63:
	.ascii	"FKEY\000"
.LC64:
	.ascii	"Max Packet Size\000"
.LC65:
	.ascii	"MAXP\000"
.LC66:
	.ascii	"Min Packet Size\000"
.LC67:
	.ascii	"MINP\000"
.LC68:
	.ascii	"RF Data Rate\000"
.LC69:
	.ascii	"RFRATE\000"
.LC70:
	.ascii	"RF Xmit Power\000"
.LC71:
	.ascii	"XPOWER\000"
.LC72:
	.ascii	"Lowpower Mode\000"
.LC73:
	.ascii	"LOWPOW\000"
.LC74:
	.ascii	"Modem Baud is\000"
.LC75:
	.ascii	"BAUD\000"
.LC76:
	.ascii	"Modem Mode is\000"
.LC77:
	.ascii	"MODE\000"
.LC78:
	.ascii	"2\000"
.LC79:
	.ascii	"3\000"
.LC80:
	.ascii	"Reading device settings...\000"
.LC81:
	.ascii	"Saving device settings...\000"
.LC82:
	.ascii	"D2 AES Version\000"
.LC83:
	.ascii	"VERSION\000"
.LC84:
	.ascii	"Modem Serial Number\000"
.LC85:
	.ascii	"SN\000"
.LC86:
	.ascii	"Model Code\000"
.LC87:
	.ascii	"MODEL\000"
.LC88:
	.ascii	"6\000"
.LC89:
	.ascii	"009600\000"
.LC90:
	.ascii	"5\000"
.LC91:
	.ascii	"019200\000"
.LC92:
	.ascii	"4\000"
.LC93:
	.ascii	"038400\000"
.LC94:
	.ascii	"057600\000"
.LC95:
	.ascii	"1\000"
.LC96:
	.ascii	"115200\000"
.LC97:
	.ascii	"230400\000"
.LC98:
	.ascii	"SR26\000"
.LC99:
	.ascii	"\015\012\000"
.LC100:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_SR_FREEWAVE.c\000"
.LC101:
	.ascii	"SR27\000"
.LC102:
	.ascii	"SR28\000"
.LC103:
	.ascii	"SR29\000"
.LC104:
	.ascii	"PVS\000"
.LC105:
	.ascii	"\000"
.LC106:
	.ascii	"WRITE\000"
.LC107:
	.ascii	"Beginning write operation...\000"
.LC108:
	.ascii	" 3\000"
.LC109:
	.ascii	" 0\000"
.LC110:
	.ascii	" 1\000"
.LC111:
	.ascii	" 6\000"
.LC112:
	.ascii	"32\000"
.LC113:
	.ascii	"READ\000"
.LC114:
	.ascii	"Beginning read operation...\000"
.LC115:
	.ascii	"Entering radio programming mode...\000"
.LC116:
	.ascii	"SR30\000"
.LC117:
	.ascii	"COMM_MNGR Timeout\000"
.LC118:
	.ascii	"SR31 %d\000"
.LC119:
	.ascii	"Read ended early\000"
.LC120:
	.ascii	"Read completed\000"
.LC121:
	.ascii	"Read completed. Settings Invalid\000"
.LC122:
	.ascii	"Syncing Radios \000"
.LC123:
	.ascii	"SR32\000"
.LC124:
	.ascii	"SR33 %d\000"
.LC125:
	.ascii	"Write ended early\000"
.LC126:
	.ascii	"Write completed\000"
.LC127:
	.ascii	"Write completed with mismatches\000"
.LC128:
	.ascii	"SR34\000"
.LC129:
	.ascii	"IDLE\000"
.LC130:
	.ascii	"Why is the SR not on PORT A?\000"
.LC131:
	.ascii	"Unexpected NULL is_connected function\000"
.LC132:
	.ascii	"Connection Process : UNXEXP EVENT\000"
.LC133:
	.ascii	"SR Connection\000"
.LC134:
	.ascii	"SR Radio sync'd to master in %u seconds\000"
.LC135:
	.ascii	"not used\000"
.LC136:
	.ascii	"Enter Choice\000"
.LC137:
	.ascii	"MAIN MENU\000"
.LC138:
	.ascii	"SET MODEM MODE\000"
.LC139:
	.ascii	"SET BAUD RATE\000"
.LC140:
	.ascii	"3 for Both\000"
.LC141:
	.ascii	"Enter 0 for None,1 For RTS,2 for DTR\000"
.LC142:
	.ascii	"Enter 0 for None\000"
.LC143:
	.ascii	"RADIO PARAMETERS\000"
.LC144:
	.ascii	"Enter New Frequency Key (0-E) (F for more)\000"
.LC145:
	.ascii	"Enter New Frequency\000"
.LC146:
	.ascii	"Enter Max Packet (0-9)\000"
.LC147:
	.ascii	"Enter Max Packet\000"
.LC148:
	.ascii	"Enter Min Packet (0-9)\000"
.LC149:
	.ascii	"Enter Min Packet\000"
.LC150:
	.ascii	"Enter New Xmit Rate (0-1)\000"
.LC151:
	.ascii	"Enter New Xmit Rate\000"
.LC152:
	.ascii	"Enter New RF Data Rate (2-3)\000"
.LC153:
	.ascii	"Enter New RF Data \000"
.LC154:
	.ascii	"Enter New XmitPower (0-10)\000"
.LC155:
	.ascii	"Enter New XmitPower\000"
.LC156:
	.ascii	"Enter LowPower Option or 0 to disable (0-31)\000"
.LC157:
	.ascii	"Enter LowPower Option\000"
.LC158:
	.ascii	"MULTIPOINT PARAMETERS\000"
.LC159:
	.ascii	"Enter Number of Parallel Repeaters in Network(0-9)\000"
.LC160:
	.ascii	"Enter Number of Para\000"
.LC161:
	.ascii	"Enter Number Times Master Repeats Packets(0-9)\000"
.LC162:
	.ascii	"Enter Number Times M\000"
.LC163:
	.ascii	"Enter Number Times Slave Tries Before Backing Off ("
	.ascii	"0-9)\000"
.LC164:
	.ascii	"Enter Number Times S\000"
.LC165:
	.ascii	"Enter Slave Backing Off Retry Odds (0-9)\000"
.LC166:
	.ascii	"Enter Slave Backing\000"
.LC167:
	.ascii	"Enter 1 For DTR Sensing, 2 For Burst Mode, Otherwis"
	.ascii	"e 0\000"
.LC168:
	.ascii	"Enter 1 For DTR\000"
.LC169:
	.ascii	"Enter 0 To Use Master Hop Table, 1 To Use Repeater\000"
.LC170:
	.ascii	"Enter 0 To Use Master\000"
.LC171:
	.ascii	"Enter Network ID Number (0-4095)\000"
.LC172:
	.ascii	"Enter Network ID\000"
.LC173:
	.ascii	"Enter 1 to enable Slave/Repeater or 0 for Normal\000"
.LC174:
	.ascii	"Enter 1 to enable S\000"
.LC175:
	.ascii	"Enter 1 to 129 Enable Diagnostics, 0 To Disable\000"
.LC176:
	.ascii	"Enter 1 to 129\000"
.LC177:
	.ascii	"Enter Rcv SubNetID (0-F)\000"
.LC178:
	.ascii	"Enter Rcv SubNetID\000"
.LC179:
	.ascii	"Enter Xmit SubNetID (0-F)\000"
.LC180:
	.ascii	"Enter Xmit SubNetID\000"
	.section	.data.sr_group_list,"aw",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	sr_group_list, %object
	.size	sr_group_list, 200
sr_group_list:
	.ascii	" 1\000"
	.space	2
	.ascii	"5\000"
	.space	1
	.ascii	"8\000"
	.ascii	"9\000"
	.ascii	"0100\000"
	.space	3
	.ascii	" 2\000"
	.space	2
	.ascii	"4\000"
	.space	1
	.ascii	"7\000"
	.ascii	"8\000"
	.ascii	"0110\000"
	.space	3
	.ascii	" 3\000"
	.space	2
	.ascii	"3\000"
	.space	1
	.ascii	"6\000"
	.ascii	"7\000"
	.ascii	"0120\000"
	.space	3
	.ascii	" 4\000"
	.space	2
	.ascii	"2\000"
	.space	1
	.ascii	"5\000"
	.ascii	"6\000"
	.ascii	"0130\000"
	.space	3
	.ascii	" 5\000"
	.space	2
	.ascii	"1\000"
	.space	1
	.ascii	"4\000"
	.ascii	"5\000"
	.ascii	"0140\000"
	.space	3
	.ascii	" 6\000"
	.space	2
	.ascii	"6\000"
	.space	1
	.ascii	"7\000"
	.ascii	"9\000"
	.ascii	"0150\000"
	.space	3
	.ascii	" 7\000"
	.space	2
	.ascii	"7\000"
	.space	1
	.ascii	"6\000"
	.ascii	"8\000"
	.ascii	"0160\000"
	.space	3
	.ascii	" 8\000"
	.space	2
	.ascii	"8\000"
	.space	1
	.ascii	"5\000"
	.ascii	"7\000"
	.ascii	"0170\000"
	.space	3
	.ascii	" 9\000"
	.space	2
	.ascii	"9\000"
	.space	1
	.ascii	"4\000"
	.ascii	"6\000"
	.ascii	"0180\000"
	.space	3
	.ascii	"10\000"
	.space	2
	.ascii	"0\000"
	.space	1
	.ascii	"3\000"
	.ascii	"5\000"
	.ascii	"0190\000"
	.space	3
	.section	.bss.sr_write_pvs,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	sr_write_pvs, %object
	.size	sr_write_pvs, 72
sr_write_pvs:
	.space	72
	.section	.data.sr_write_list,"aw",%progbits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	sr_write_list, %object
	.size	sr_write_list, 816
sr_write_list:
	.word	.LC135
	.word	0
	.word	5000
	.word	.LC136
	.word	.LC137
	.word	sr_analyze_main_menu
	.word	48
	.word	.LC136
	.word	.LC138
	.word	0
	.word	10000
	.word	.LC136
	.word	.LC138
	.word	sr_analyze_set_modem_mode
	.word	27
	.word	.LC136
	.word	.LC137
	.word	0
	.word	49
	.word	.LC136
	.word	.LC139
	.word	0
	.word	11000
	.word	.LC136
	.word	.LC139
	.word	0
	.word	68
	.word	.LC140
	.word	.LC140
	.word	0
	.word	51
	.word	.LC136
	.word	.LC139
	.word	0
	.word	70
	.word	.LC141
	.word	.LC142
	.word	0
	.word	49
	.word	.LC136
	.word	.LC139
	.word	sr_analyze_set_baud_rate
	.word	27
	.word	.LC136
	.word	.LC137
	.word	0
	.word	51
	.word	.LC136
	.word	.LC143
	.word	0
	.word	48
	.word	.LC144
	.word	.LC145
	.word	0
	.word	13001
	.word	.LC136
	.word	.LC143
	.word	0
	.word	49
	.word	.LC146
	.word	.LC147
	.word	0
	.word	13002
	.word	.LC136
	.word	.LC143
	.word	0
	.word	50
	.word	.LC148
	.word	.LC149
	.word	0
	.word	13003
	.word	.LC136
	.word	.LC143
	.word	0
	.word	51
	.word	.LC150
	.word	.LC151
	.word	0
	.word	13004
	.word	.LC136
	.word	.LC143
	.word	0
	.word	52
	.word	.LC152
	.word	.LC153
	.word	0
	.word	13005
	.word	.LC136
	.word	.LC143
	.word	0
	.word	53
	.word	.LC154
	.word	.LC155
	.word	0
	.word	13006
	.word	.LC136
	.word	.LC143
	.word	0
	.word	57
	.word	.LC156
	.word	.LC157
	.word	0
	.word	13010
	.word	.LC136
	.word	.LC143
	.word	sr_analyze_radio_parameters
	.word	27
	.word	.LC136
	.word	.LC137
	.word	0
	.word	53
	.word	.LC136
	.word	.LC158
	.word	0
	.word	48
	.word	.LC159
	.word	.LC160
	.word	0
	.word	15001
	.word	.LC136
	.word	.LC158
	.word	0
	.word	49
	.word	.LC161
	.word	.LC162
	.word	0
	.word	15002
	.word	.LC136
	.word	.LC158
	.word	0
	.word	50
	.word	.LC163
	.word	.LC164
	.word	0
	.word	15003
	.word	.LC136
	.word	.LC158
	.word	0
	.word	51
	.word	.LC165
	.word	.LC166
	.word	0
	.word	15004
	.word	.LC136
	.word	.LC158
	.word	0
	.word	52
	.word	.LC167
	.word	.LC168
	.word	0
	.word	15005
	.word	.LC136
	.word	.LC158
	.word	0
	.word	53
	.word	.LC169
	.word	.LC170
	.word	0
	.word	15006
	.word	.LC136
	.word	.LC158
	.word	0
	.word	54
	.word	.LC171
	.word	.LC172
	.word	0
	.word	15007
	.word	.LC136
	.word	.LC158
	.word	0
	.word	65
	.word	.LC173
	.word	.LC174
	.word	0
	.word	15011
	.word	.LC136
	.word	.LC158
	.word	0
	.word	66
	.word	.LC175
	.word	.LC176
	.word	0
	.word	15012
	.word	.LC136
	.word	.LC158
	.word	0
	.word	67
	.word	.LC177
	.word	.LC178
	.word	0
	.word	15014
	.word	.LC179
	.word	.LC180
	.word	0
	.word	15015
	.word	.LC136
	.word	.LC158
	.word	sr_analyze_multipoint_parameters
	.word	27
	.word	.LC136
	.word	.LC137
	.word	sr_final_radio_verification
	.word	99999
	.word	.LC105
	.section	.bss.sr_dynamic_pvs,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	sr_dynamic_pvs, %object
	.size	sr_dynamic_pvs, 72
sr_dynamic_pvs:
	.space	72
	.section	.data.sr_state,"aw",%progbits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	sr_state, %object
	.size	sr_state, 4
sr_state:
	.word	state_struct
	.section	.bss.state_struct,"aw",%nobits
	.align	2
	.type	state_struct, %object
	.size	state_struct, 184
state_struct:
	.space	184
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI0-.LFB13
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI1-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI5-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI6-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI7-.LFB23
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI9-.LFB20
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI10-.LFB21
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI11-.LFB36
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI12-.LFB31
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI13-.LFB32
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI14-.LFB33
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI15-.LFB34
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI16-.LFB35
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE30:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_SR_FREEWAVE.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x218
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF36
	.byte	0x1
	.4byte	.LASF37
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x6e2
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x6c8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x67a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x785
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x63f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x760
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x62b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x196
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x1e2
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x318
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x39a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x213
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x49c
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x1
	.byte	0xff
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x1
	.byte	0xe3
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x1
	.byte	0xc8
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x1
	.byte	0xa2
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST5
	.uleb128 0x4
	.4byte	.LASF17
	.byte	0x1
	.byte	0x86
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST6
	.uleb128 0x2
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x4af
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x251
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x6a4
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST7
	.uleb128 0x3
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x649
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST8
	.uleb128 0x3
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x65e
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST9
	.uleb128 0x5
	.4byte	0x3c
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST10
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x94b
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST11
	.uleb128 0x2
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x733
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x70a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x35c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x177
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x29a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x2ca
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x2ed
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x95f
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST12
	.uleb128 0x2
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x84f
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x990
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST13
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x9bd
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST14
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x9e5
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST15
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB13
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB16
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB1
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB0
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB23
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI8
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB20
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB21
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB36
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB31
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB32
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE32
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB33
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB34
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB35
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x94
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"setup_for_termination_string_hunt\000"
.LASF35:
	.ascii	"SR_FREEWAVE_connection_processing\000"
.LASF22:
	.ascii	"exit_radio_programming_mode\000"
.LASF36:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF10:
	.ascii	"sr_verify_freq_key\000"
.LASF28:
	.ascii	"sr_set_master_values\000"
.LASF11:
	.ascii	"sr_final_radio_analysis\000"
.LASF14:
	.ascii	"sr_analyze_radio_parameters\000"
.LASF29:
	.ascii	"sr_set_slave_values\000"
.LASF17:
	.ascii	"sr_analyze_main_menu\000"
.LASF12:
	.ascii	"sr_final_radio_verification\000"
.LASF6:
	.ascii	"SR_FREEWAVE_set_radio_to_programming_mode\000"
.LASF1:
	.ascii	"sr_set_read_operation\000"
.LASF3:
	.ascii	"process_list\000"
.LASF25:
	.ascii	"sr_copy_active_values_to_write\000"
.LASF15:
	.ascii	"sr_analyze_set_baud_rate\000"
.LASF7:
	.ascii	"sr_analyze_network_group\000"
.LASF0:
	.ascii	"sr_set_write_operation\000"
.LASF26:
	.ascii	"sr_set_network_group_values\000"
.LASF5:
	.ascii	"sr_verify_state_struct\000"
.LASF30:
	.ascii	"SR_FREEWAVE_power_control\000"
.LASF32:
	.ascii	"sr_state_machine\000"
.LASF23:
	.ascii	"sr_initialize_state_struct\000"
.LASF19:
	.ascii	"sr_get_baud_rate_command\000"
.LASF37:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_SR_FREEWAVE.c\000"
.LASF34:
	.ascii	"SR_FREEWAVE_initialize_the_connection_process\000"
.LASF13:
	.ascii	"sr_analyze_multipoint_parameters\000"
.LASF9:
	.ascii	"sr_verify_dynamic_values_match_write_values\000"
.LASF16:
	.ascii	"sr_analyze_set_modem_mode\000"
.LASF21:
	.ascii	"send_no_response_esc_to_radio\000"
.LASF18:
	.ascii	"get_command_text\000"
.LASF31:
	.ascii	"SR_FREEWAVE_initialize_device_exchange\000"
.LASF8:
	.ascii	"sr_analyze_repeater_settings\000"
.LASF27:
	.ascii	"sr_set_repeater_values\000"
.LASF33:
	.ascii	"SR_FREEWAVE_exchange_processing\000"
.LASF24:
	.ascii	"sr_set_state_struct_for_new_device_exchange\000"
.LASF4:
	.ascii	"SR_FREEWAVE_set_radio_inactive\000"
.LASF20:
	.ascii	"sr_get_and_process_command\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
