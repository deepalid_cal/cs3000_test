	.file	"device_WEN_WiBox.c"
	.text
.Ltext0:
	.section	.text.WIBOX_device_write_progress,"ax",%progbits
	.align	2
	.type	WIBOX_device_write_progress, %function
WIBOX_device_write_progress:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	ldr	r1, .L2
	mov	r2, #40
	add	r0, r0, #24
	bl	strlcpy
	ldr	r0, .L2+4
	ldr	lr, [sp], #4
	b	COMM_MNGR_device_exchange_results_to_key_process_task
.L3:
	.align	2
.L2:
	.word	.LC0
	.word	36870
.LFE5:
	.size	WIBOX_device_write_progress, .-WIBOX_device_write_progress
	.section	.text.WIBOX_final_device_analysis,"ax",%progbits
	.align	2
	.type	WIBOX_final_device_analysis, %function
WIBOX_final_device_analysis:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #140]
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	ldr	r5, [r0, #192]
	cmp	r3, #0
	addne	r4, r0, #128
	addeq	r4, r0, #124
	mov	r3, #1
	ldr	r6, [r0, #180]
	str	r3, [r4, #0]
	ldr	r3, [r0, #140]
	cmp	r3, #0
	beq	.L7
	ldr	r1, .L23
	mov	r2, #6
	add	r0, r5, #63
	bl	strncmp
	ldr	r1, .L23+4
	mov	r2, #2
	cmp	r0, #0
	ldreq	r3, [r4, #0]
	movne	r3, #0
	str	r3, [r4, #0]
	add	r0, r5, #71
	bl	strncmp
	ldr	r1, .L23+8
	mov	r2, #2
	cmp	r0, #0
	ldreq	r3, [r4, #0]
	movne	r3, #0
	str	r3, [r4, #0]
	add	r0, r5, #75
	bl	strncmp
	ldr	r1, .L23+12
	mov	r2, #5
	cmp	r0, #0
	ldreq	r3, [r4, #0]
	movne	r3, #0
	str	r3, [r4, #0]
	add	r0, r5, #79
	bl	strncmp
	ldr	r1, .L23+16
	mov	r2, #2
	cmp	r0, #0
	ldreq	r3, [r4, #0]
	movne	r3, #0
	str	r3, [r4, #0]
	add	r0, r5, #86
	bl	strncmp
	ldr	r1, .L23+20
	mov	r2, #2
	cmp	r0, #0
	ldreq	r3, [r4, #0]
	movne	r3, #0
	str	r3, [r4, #0]
	add	r0, r5, #144
	bl	strncmp
	ldr	r1, .L23+24
	mov	r2, #2
	cmp	r0, #0
	ldreq	r3, [r4, #0]
	movne	r3, #0
	str	r3, [r4, #0]
	add	r0, r5, #148
	bl	strncmp
	cmp	r0, #0
	ldreq	r3, [r4, #0]
	movne	r3, #0
	str	r3, [r4, #0]
	add	r0, r5, #45
	bl	strlen
	cmp	r0, #0
	movne	r3, #0
	strne	r3, [r6, #4]
.L7:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	ldmnefd	sp!, {r4, r5, r6, pc}
	ldr	r0, .L23+28
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_Message
.L24:
	.align	2
.L23:
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
.LFE6:
	.size	WIBOX_final_device_analysis, .-WIBOX_final_device_analysis
	.section	.text.WIBOX_analyze_server_info,"ax",%progbits
	.align	2
	.type	WIBOX_analyze_server_info, %function
WIBOX_analyze_server_info:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI2:
	ldr	r6, [r0, #180]
	mov	r4, r0
	ldr	r1, .L26
	mov	r2, #40
	add	r0, r0, #24
	bl	strlcpy
	ldr	r0, .L26+4
	bl	COMM_MNGR_device_exchange_results_to_key_process_task
	mov	r3, #24
	str	r3, [sp, #0]
	add	r3, r6, #8
	ldr	r5, .L26+8
	str	r3, [sp, #4]
	ldr	r3, .L26+12
	ldr	r0, [r4, #148]
	str	r3, [sp, #8]
	mov	r1, r5
	ldr	r2, .L26+16
	mov	r3, #14
	bl	dev_extract_delimited_text_from_buffer
	ldr	r3, .L26+20
	ldr	r0, [r4, #148]
	str	r3, [sp, #4]
	add	r6, r6, #32
	ldr	r1, .L26+24
	mov	r2, #12
	mov	r3, #13
	str	r6, [sp, #0]
	bl	dev_extract_text_from_buffer
	mov	r3, #15
	str	r3, [sp, #0]
	add	r3, r4, #232
	str	r3, [sp, #4]
	ldr	r3, .L26+28
	ldr	r0, [r4, #148]
	str	r3, [sp, #8]
	mov	r1, r5
	ldr	r2, .L26+32
	mov	r3, #17
	bl	dev_extract_delimited_text_from_buffer
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, pc}
.L27:
	.align	2
.L26:
	.word	.LC9
	.word	36867
	.word	.LC10
	.word	.LC12
	.word	.LC11
	.word	.LC14
	.word	.LC13
	.word	.LC16
	.word	.LC15
.LFE0:
	.size	WIBOX_analyze_server_info, .-WIBOX_analyze_server_info
	.section	.text.WIBOX_analyze_setup_mode,"ax",%progbits
	.align	2
	.type	WIBOX_analyze_setup_mode, %function
WIBOX_analyze_setup_mode:
.LFB4:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI3:
.LBB8:
	ldr	r7, .L68
.LBE8:
	sub	sp, sp, #44
.LCFI4:
.LBB9:
	add	r6, sp, #12
	ldr	r5, [r0, #192]
	ldr	r8, [r0, #180]
	mov	r3, #16
	str	r6, [sp, #0]
	str	r7, [sp, #4]
.LBE9:
	mov	r4, r0
.LBB10:
	ldr	r1, .L68+4
	ldr	r0, [r0, #148]
	mov	r2, #8
	bl	dev_extract_text_from_buffer
	ldr	r1, .L68+8
	mov	r0, r6
	mov	r2, #1
	bl	strncmp
	mov	r2, #17
	add	r3, r5, #4
	stmia	sp, {r2, r3, r7}
	add	r6, r5, #27
	add	r7, r5, #21
	cmp	r0, #0
	ldreq	r1, .L68+12
	ldreq	r2, .L68+4
	moveq	r3, #10
	ldrne	r1, .L68+16
	ldrne	r2, .L68+4
	movne	r3, #8
	ldr	r0, [r4, #148]
	bl	dev_extract_delimited_text_from_buffer
	add	r3, r5, #39
	str	r3, [sp, #0]
	add	r0, r5, #4
	mov	r1, r7
	mov	r2, r6
	add	r3, r5, #33
	bl	dev_extract_ip_octets
	mov	r0, r7
	ldr	r1, .L68+20
	mov	r2, #6
	bl	strncmp
	cmp	r0, #0
	bne	.L31
	mov	r0, r6
	ldr	r1, .L68+20
	mov	r2, #6
	bl	strncmp
	cmp	r0, #0
	moveq	r3, #1
	beq	.L32
.L31:
	mov	r3, #0
.L32:
	str	r3, [r4, #268]
	ldr	r0, [r4, #148]
	ldr	r1, .L68+24
	bl	strstr
	cmp	r0, #0
	addne	r0, r5, #152
	bne	.L35
	ldr	r0, [r4, #148]
	ldr	r1, .L68+28
	bl	strstr
	cmp	r0, #0
	add	r0, r5, #152
	beq	.L35
	mov	r3, #17
	str	r3, [sp, #0]
	ldr	r3, .L68+32
	str	r0, [sp, #4]
	str	r3, [sp, #8]
	ldr	r1, .L68+16
	ldr	r0, [r4, #148]
	ldr	r2, .L68+28
	mov	r3, #10
	bl	dev_extract_delimited_text_from_buffer
	b	.L34
.L35:
	ldr	r1, .L68+36
	mov	r2, #17
	bl	strlcpy
.L34:
	add	r3, r5, #187
	str	r3, [sp, #0]
	add	r0, r5, #152
	add	r1, r5, #169
	add	r2, r5, #175
	add	r3, r5, #181
	bl	dev_extract_ip_octets
	ldr	r0, [r4, #148]
	ldr	r1, .L68+40
	bl	strstr
	cmp	r0, #0
	add	r0, r5, #193
	beq	.L36
	mov	r3, #17
	str	r3, [sp, #0]
	ldr	r3, .L68+44
	str	r0, [sp, #4]
	str	r3, [sp, #8]
	ldr	r1, .L68+48
	ldr	r0, [r4, #148]
	ldr	r2, .L68+40
	mov	r3, #9
	bl	dev_extract_delimited_text_from_buffer
	b	.L37
.L36:
	ldr	r1, .L68+52
	mov	r2, #17
	bl	strlcpy
.L37:
	add	r0, r5, #193
	bl	dev_count_true_mask_bits
	ldr	r1, .L68+56
	ldr	r2, [r4, #152]
	rsb	r0, r0, #32
	cmp	r0, #24
	movhi	r3, #8
	str	r0, [r8, #0]
	strhi	r3, [r8, #0]
	ldr	r0, [r4, #148]
	bl	find_string_in_block
	subs	r7, r0, #0
	beq	.L39
	add	r5, r5, #45
	mov	r1, #0
	mov	r2, #17
	mov	r0, r5
	bl	memset
	mov	r0, r7
	ldr	r1, .L68+60
	bl	strstr
	subs	r6, r0, #0
	beq	.L40
	ldr	r3, [r8, #4]
	cmp	r3, #0
	beq	.L39
	mov	r0, r5
	add	r1, r8, #32
	mov	r2, #18
	bl	strlcpy
	mov	r3, #0
	str	r3, [r8, #4]
	b	.L39
.L40:
	mov	r3, #18
	stmia	sp, {r3, r5}
	ldr	r3, .L68+64
	mov	r0, r7
	str	r3, [sp, #8]
	ldr	r1, .L68+48
	ldr	r2, .L68+56
	mov	r3, #19
	bl	dev_extract_delimited_text_from_buffer
	str	r6, [r8, #4]
.L39:
.LBE10:
.LBB11:
	ldr	r0, [r4, #148]
	ldr	r1, .L68+68
	ldr	r5, [r4, #192]
	bl	strstr
	subs	r6, r0, #0
	beq	.L41
	add	r3, r5, #63
	str	r3, [sp, #4]
	ldr	r3, .L68+72
	mov	r8, #8
	str	r3, [sp, #8]
	ldr	r1, .L68+16
	ldr	r2, .L68+76
	mov	r3, #9
	str	r8, [sp, #0]
	bl	dev_extract_delimited_text_from_buffer
	add	r3, r5, #71
	str	r3, [sp, #0]
	ldr	r3, .L68+80
	mov	r0, r6
	str	r3, [sp, #4]
	ldr	r1, .L68+84
	mov	r2, #9
	mov	r3, #4
	bl	dev_extract_text_from_buffer
	add	r3, r5, #75
	str	r3, [sp, #0]
	ldr	r3, .L68+88
	mov	r0, r6
	str	r3, [sp, #4]
	ldr	r1, .L68+92
	mov	r2, #5
	mov	r3, #4
	bl	dev_extract_text_from_buffer
	mov	r3, #7
	str	r3, [sp, #0]
	add	r3, r5, #79
	str	r3, [sp, #4]
	ldr	r3, .L68+96
	mov	r0, r6
	str	r3, [sp, #8]
	ldr	r1, .L68+48
	ldr	r2, .L68+100
	mov	r3, #5
	bl	dev_extract_delimited_text_from_buffer
	add	r3, r5, #86
	str	r3, [sp, #0]
	ldr	r3, .L68+104
	mov	r0, r6
	str	r3, [sp, #4]
	ldr	r1, .L68+108
	mov	r2, #15
	mov	r3, #4
	bl	dev_extract_text_from_buffer
	ldr	r3, .L68+112
	add	r7, sp, #12
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r3, #9
	ldr	r1, .L68+116
	mov	r2, #27
	str	r7, [sp, #0]
	bl	dev_extract_text_from_buffer
	mov	r0, r7
	ldr	r1, .L68+120
	mov	r2, r8
	bl	strncmp
	cmp	r0, #0
	moveq	r3, #1
	streq	r3, [r5, #92]
	moveq	r7, r0
	beq	.L43
	mov	r0, r7
	ldr	r1, .L68+124
	mov	r2, #9
	bl	strncmp
	subs	r7, r0, #0
	movne	r7, #1
	str	r7, [r5, #92]
.L43:
	mov	r0, r6
	ldr	r1, .L68+128
	bl	strstr
	cmp	r0, #0
	add	r0, r5, #96
	beq	.L45
	ldr	r1, .L68+36
	mov	r2, #17
	bl	strlcpy
	b	.L46
.L45:
	mov	r3, #17
	str	r3, [sp, #0]
	ldr	r3, .L68+132
	str	r0, [sp, #4]
	str	r3, [sp, #8]
	mov	r0, r6
	ldr	r1, .L68+16
	ldr	r2, .L68+136
	mov	r3, #15
	bl	dev_extract_delimited_text_from_buffer
.L46:
	add	r3, r5, #131
	add	r0, r5, #96
	add	r1, r5, #113
	add	r2, r5, #119
	str	r3, [sp, #0]
	add	r3, r5, #125
	bl	dev_extract_ip_octets
	add	r2, r5, #137
	str	r2, [sp, #4]
	ldr	r2, .L68+140
	mov	r3, #7
	str	r2, [sp, #8]
	mov	r0, r6
	ldr	r1, .L68+48
	ldr	r2, .L68+144
	str	r3, [sp, #0]
	bl	dev_extract_delimited_text_from_buffer
	add	r3, r5, #144
	str	r3, [sp, #0]
	ldr	r3, .L68+148
	mov	r0, r6
	str	r3, [sp, #4]
	ldr	r1, .L68+152
	mov	r2, #15
	mov	r3, #4
	bl	dev_extract_text_from_buffer
	ldr	r3, .L68+156
	add	r5, r5, #148
	str	r3, [sp, #4]
	mov	r0, r6
	ldr	r1, .L68+160
	mov	r2, #15
	mov	r3, #4
	str	r5, [sp, #0]
	bl	dev_extract_text_from_buffer
	cmp	r7, #0
	beq	.L47
.L41:
	ldr	r0, .L68+164
	bl	Alert_Message
.L47:
.LBE11:
.LBB12:
	ldr	r0, [r4, #148]
	ldr	r1, .L68+168
	ldr	r5, [r4, #192]
	bl	strstr
	subs	r4, r0, #0
	beq	.L48
	ldr	r0, .L68+172
	bl	strlen
	mov	r2, #33
	str	r2, [sp, #0]
	add	r2, r5, #210
	str	r2, [sp, #4]
	ldr	r2, .L68+176
	ldr	r1, .L68+180
	str	r2, [sp, #8]
	ldr	r2, .L68+172
	add	r6, sp, #12
	mov	r3, r0
	mov	r0, r4
	bl	dev_extract_delimited_text_from_buffer
	ldr	r0, .L68+184
	bl	strlen
	mov	r2, #32
	str	r2, [sp, #0]
	ldr	r2, .L68+188
	ldr	r1, .L68+48
	str	r2, [sp, #8]
	ldr	r2, .L68+184
	str	r6, [sp, #4]
	mov	r3, r0
	mov	r0, r4
	bl	dev_extract_delimited_text_from_buffer
	ldr	r0, .L68+192
	bl	strlen
	ldr	r1, .L68+192
	mov	r2, r0
	mov	r0, r6
	bl	strncmp
	cmp	r0, #0
	streq	r0, [r5, #376]
	beq	.L50
	ldr	r0, .L68+196
	bl	strlen
	ldr	r1, .L68+196
	mov	r2, r0
	mov	r0, r6
	bl	strncmp
	cmp	r0, #0
	moveq	r3, #1
	beq	.L65
	ldr	r0, .L68+200
	bl	strlen
	ldr	r1, .L68+200
	mov	r2, r0
	mov	r0, r6
	bl	strncmp
	cmp	r0, #0
	moveq	r3, #3
	movne	r3, #2
.L65:
	str	r3, [r5, #376]
.L50:
	ldr	r7, [r5, #376]
	cmp	r7, #1
	bne	.L53
	ldr	r0, .L68+204
	bl	strlen
	ldr	r2, .L68+208
	add	r6, sp, #12
	ldr	r1, .L68+48
	mov	r7, #32
	str	r2, [sp, #8]
	ldr	r2, .L68+204
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	mov	r3, r0
	mov	r0, r4
	bl	dev_extract_delimited_text_from_buffer
	ldr	r0, .L68+212
	bl	strlen
	ldr	r1, .L68+212
	mov	r2, r0
	mov	r0, r6
	bl	strncmp
	adds	r0, r0, #0
	movne	r0, #1
	str	r0, [r5, #380]
	ldr	r0, .L68+216
	bl	strlen
	ldr	r2, .L68+220
	ldr	r1, .L68+48
	str	r2, [sp, #8]
	ldr	r2, .L68+216
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	mov	r3, r0
	mov	r0, r4
	bl	dev_extract_delimited_text_from_buffer
	ldr	r0, .L68+224
	bl	strlen
	ldr	r1, .L68+224
	mov	r2, r0
	mov	r0, r6
	bl	strncmp
	adds	r0, r0, #0
	movne	r0, #1
	str	r0, [r5, #384]
	b	.L28
.L53:
	cmp	r7, #2
	bne	.L56
	ldr	r0, .L68+216
	bl	strlen
	mov	r2, #32
	str	r2, [sp, #0]
	ldr	r2, .L68+220
	ldr	r1, .L68+48
	add	r6, sp, #12
	str	r2, [sp, #8]
	ldr	r2, .L68+216
	str	r6, [sp, #4]
	mov	r3, r0
	mov	r0, r4
	bl	dev_extract_delimited_text_from_buffer
	ldr	r0, .L68+228
	bl	strlen
	ldr	r1, .L68+228
	mov	r2, r0
	mov	r0, r6
	bl	strncmp
	cmp	r0, #0
	moveq	r3, #1
	movne	r3, #0
	str	r3, [r5, #388]
	b	.L28
.L56:
	cmp	r7, #3
	bne	.L28
	ldr	r0, .L68+216
	bl	strlen
	mov	r2, #32
	str	r2, [sp, #0]
	ldr	r2, .L68+220
	ldr	r1, .L68+48
	add	r6, sp, #12
	str	r2, [sp, #8]
	ldr	r2, .L68+216
	str	r6, [sp, #4]
	mov	r3, r0
	mov	r0, r4
	bl	dev_extract_delimited_text_from_buffer
	ldr	r0, .L68+232
	bl	strlen
	ldr	r1, .L68+232
	mov	r2, r0
	mov	r0, r6
	bl	strncmp
	cmp	r0, #0
	moveq	r3, #1
	beq	.L67
	ldr	r0, .L68+236
	bl	strlen
	ldr	r1, .L68+236
	mov	r2, r0
	mov	r0, r6
	bl	strncmp
	cmp	r0, #0
	moveq	r3, #2
	beq	.L67
	ldr	r0, .L68+240
	bl	strlen
	ldr	r1, .L68+240
	mov	r2, r0
	mov	r0, r6
	bl	strncmp
	cmp	r0, #0
	streq	r0, [r5, #392]
	beq	.L28
	ldr	r0, .L68+228
	bl	strlen
	ldr	r1, .L68+228
	mov	r2, r0
	mov	r0, r6
	bl	strncmp
	cmp	r0, #0
	strne	r7, [r5, #392]
	bne	.L28
	mov	r3, #4
.L67:
	str	r3, [r5, #392]
	b	.L28
.L48:
	ldr	r0, .L68+244
	bl	Alert_Message
.L28:
.LBE12:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L69:
	.align	2
.L68:
	.word	.LC18
	.word	.LC17
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	.LC23
	.word	.LC25
	.word	.LC26
	.word	.LC24
	.word	.LC27
	.word	.LC29
	.word	.LC28
	.word	.LC30
	.word	.LC31
	.word	.LC32
	.word	.LC33
	.word	.LC34
	.word	.LC36
	.word	.LC35
	.word	.LC38
	.word	.LC37
	.word	.LC40
	.word	.LC39
	.word	.LC42
	.word	.LC41
	.word	.LC44
	.word	.LC43
	.word	.LC46
	.word	.LC45
	.word	.LC47
	.word	.LC48
	.word	.LC49
	.word	.LC51
	.word	.LC50
	.word	.LC53
	.word	.LC52
	.word	.LC55
	.word	.LC54
	.word	.LC57
	.word	.LC56
	.word	.LC58
	.word	.LC59
	.word	.LC60
	.word	.LC62
	.word	.LC61
	.word	.LC63
	.word	.LC64
	.word	.LC65
	.word	.LC66
	.word	.LC67
	.word	.LC68
	.word	.LC69
	.word	.LC70
	.word	.LC71
	.word	.LC72
	.word	.LC73
	.word	.LC74
	.word	.LC75
	.word	.LC76
	.word	.LC77
	.word	.LC78
.LFE4:
	.size	WIBOX_analyze_setup_mode, .-WIBOX_analyze_setup_mode
	.section	.text.WIBOX_copy_active_values_to_static_values,"ax",%progbits
	.align	2
	.global	WIBOX_copy_active_values_to_static_values
	.type	WIBOX_copy_active_values_to_static_values, %function
WIBOX_copy_active_values_to_static_values:
.LFB7:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI5:
	ldr	r4, [r0, #192]
	sub	sp, sp, #28
.LCFI6:
	add	sl, r4, #63
	ldr	r5, [r0, #196]
	ldr	r1, .L71
	mov	r2, #8
	add	r8, r4, #71
	mov	r0, sl
	bl	strlcpy
	add	r7, r4, #75
	ldr	r1, .L71+4
	mov	r2, #4
	mov	r0, r8
	bl	strlcpy
	add	r6, r4, #79
	ldr	r1, .L71+8
	mov	r2, #4
	mov	r0, r7
	bl	strlcpy
	add	r9, r4, #86
	ldr	r1, .L71+12
	mov	r2, #7
	mov	r0, r6
	bl	strlcpy
	ldr	r1, .L71+16
	mov	r2, #4
	mov	r0, r9
	bl	strlcpy
	add	fp, r4, #96
	mov	r3, #1
	str	r3, [r4, #92]
	ldr	r1, .L71+20
	mov	r2, #17
	mov	r0, fp
	bl	strlcpy
	add	r3, r4, #113
	mov	r0, r3
	ldr	r1, .L71+24
	mov	r2, #6
	str	r3, [sp, #4]
	bl	strlcpy
	add	ip, r4, #119
	mov	r0, ip
	ldr	r1, .L71+28
	mov	r2, #6
	str	ip, [sp, #0]
	bl	strlcpy
	add	r2, r4, #125
	str	r2, [sp, #8]
	mov	r0, r2
	ldr	r1, .L71+32
	mov	r2, #6
	bl	strlcpy
	add	r2, r4, #131
	str	r2, [sp, #12]
	mov	r0, r2
	ldr	r1, .L71+36
	mov	r2, #6
	bl	strlcpy
	add	r2, r4, #137
	str	r2, [sp, #16]
	mov	r0, r2
	ldr	r1, .L71+40
	mov	r2, #7
	bl	strlcpy
	add	r2, r4, #144
	str	r2, [sp, #20]
	mov	r0, r2
	ldr	r1, .L71+44
	mov	r2, #4
	bl	strlcpy
	add	r2, r4, #148
	str	r2, [sp, #24]
	mov	r0, r2
	ldr	r1, .L71+48
	mov	r2, #4
	bl	strlcpy
	add	r1, r4, #4
	mov	r2, #17
	add	r0, r5, #4
	bl	strlcpy
	add	r1, r4, #21
	mov	r2, #6
	add	r0, r5, #21
	bl	strlcpy
	add	r1, r4, #27
	mov	r2, #6
	add	r0, r5, #27
	bl	strlcpy
	add	r1, r4, #33
	mov	r2, #6
	add	r0, r5, #33
	bl	strlcpy
	add	r1, r4, #39
	mov	r2, #6
	add	r0, r5, #39
	bl	strlcpy
	add	r1, r4, #45
	mov	r2, #18
	add	r0, r5, #45
	bl	strlcpy
	add	r1, r4, #152
	mov	r2, #17
	add	r0, r5, #152
	bl	strlcpy
	add	r1, r4, #169
	mov	r2, #6
	add	r0, r5, #169
	bl	strlcpy
	add	r1, r4, #175
	mov	r2, #6
	add	r0, r5, #175
	bl	strlcpy
	add	r1, r4, #181
	mov	r2, #6
	add	r0, r5, #181
	bl	strlcpy
	add	r1, r4, #187
	mov	r2, #6
	add	r0, r5, #187
	bl	strlcpy
	mov	r1, sl
	mov	r2, #8
	add	r0, r5, #63
	bl	strlcpy
	mov	r1, r8
	mov	r2, #4
	add	r0, r5, #71
	bl	strlcpy
	mov	r1, r7
	mov	r2, #4
	add	r0, r5, #75
	bl	strlcpy
	mov	r1, r6
	mov	r2, #7
	add	r0, r5, #79
	bl	strlcpy
	mov	r1, r9
	mov	r2, #4
	add	r0, r5, #86
	bl	strlcpy
	ldr	r2, [r4, #92]
	mov	r1, fp
	str	r2, [r5, #92]
	add	r0, r5, #96
	mov	r2, #17
	bl	strlcpy
	ldr	r3, [sp, #4]
	mov	r2, #6
	mov	r1, r3
	add	r0, r5, #113
	bl	strlcpy
	ldr	ip, [sp, #0]
	mov	r2, #6
	mov	r1, ip
	add	r0, r5, #119
	bl	strlcpy
	ldr	r1, [sp, #8]
	mov	r2, #6
	add	r0, r5, #125
	bl	strlcpy
	ldr	r1, [sp, #12]
	mov	r2, #6
	add	r0, r5, #131
	bl	strlcpy
	ldr	r1, [sp, #16]
	mov	r2, #7
	add	r0, r5, #137
	bl	strlcpy
	ldr	r1, [sp, #20]
	mov	r2, #4
	add	r0, r5, #144
	bl	strlcpy
	ldr	r1, [sp, #24]
	mov	r2, #4
	add	r0, r5, #148
	bl	strlcpy
	add	r1, r4, #210
	mov	r2, #33
	add	r0, r5, #210
	bl	strlcpy
	add	r1, r4, #243
	mov	r2, #65
	add	r0, r5, #243
	bl	strlcpy
	add	r0, r5, #308
	add	r1, r4, #308
	mov	r2, #64
	bl	strlcpy
	ldr	r3, [r4, #372]
	str	r3, [r5, #372]
	ldr	r3, [r4, #376]
	str	r3, [r5, #376]
	ldr	r3, [r4, #380]
	str	r3, [r5, #380]
	ldr	r3, [r4, #384]
	str	r3, [r5, #384]
	ldr	r3, [r4, #388]
	str	r3, [r5, #388]
	ldr	r3, [r4, #392]
	str	r3, [r5, #392]
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L72:
	.align	2
.L71:
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC79
	.word	.LC80
	.word	.LC81
	.word	.LC82
	.word	.LC83
	.word	.LC84
	.word	.LC6
	.word	.LC7
.LFE7:
	.size	WIBOX_copy_active_values_to_static_values, .-WIBOX_copy_active_values_to_static_values
	.section	.text.WIBOX_sizeof_setup_read_list,"ax",%progbits
	.align	2
	.global	WIBOX_sizeof_setup_read_list
	.type	WIBOX_sizeof_setup_read_list, %function
WIBOX_sizeof_setup_read_list:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #5
	bx	lr
.LFE8:
	.size	WIBOX_sizeof_setup_read_list, .-WIBOX_sizeof_setup_read_list
	.section	.text.WIBOX_sizeof_dhcp_write_list,"ax",%progbits
	.align	2
	.global	WIBOX_sizeof_dhcp_write_list
	.type	WIBOX_sizeof_dhcp_write_list, %function
WIBOX_sizeof_dhcp_write_list:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #63
	bx	lr
.LFE9:
	.size	WIBOX_sizeof_dhcp_write_list, .-WIBOX_sizeof_dhcp_write_list
	.section	.text.WIBOX_sizeof_static_write_list,"ax",%progbits
	.align	2
	.global	WIBOX_sizeof_static_write_list
	.type	WIBOX_sizeof_static_write_list, %function
WIBOX_sizeof_static_write_list:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #64
	bx	lr
.LFE10:
	.size	WIBOX_sizeof_static_write_list, .-WIBOX_sizeof_static_write_list
	.section	.text.WIBOX_initialize_detail_struct,"ax",%progbits
	.align	2
	.global	WIBOX_initialize_detail_struct
	.type	WIBOX_initialize_detail_struct, %function
WIBOX_initialize_detail_struct:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI7:
	ldr	r4, .L77
	mov	r5, r0
	ldr	r3, .L77+4
	str	r4, [r0, #180]
	ldr	r6, .L77+8
	ldr	r0, .L77+12
	str	r3, [r5, #196]
	mov	r1, r6
	str	r0, [r5, #192]
	mov	r2, #4
	bl	strlcpy
	ldr	r0, [r5, #196]
	ldr	r5, .L77+16
	mov	r1, r6
	mov	r2, #4
	bl	strlcpy
	mov	r3, #1
	mov	r2, #8
	stmia	r4, {r2, r3}
	mov	r1, r5
	add	r0, r4, #8
	mov	r2, #24
	bl	strlcpy
	add	r0, r4, #32
	mov	r1, r5
	mov	r2, #13
	ldmfd	sp!, {r4, r5, r6, lr}
	b	strlcpy
.L78:
	.align	2
.L77:
	.word	.LANCHOR0
	.word	.LANCHOR2
	.word	.LC85
	.word	.LANCHOR1
	.word	.LC86
.LFE11:
	.size	WIBOX_initialize_detail_struct, .-WIBOX_initialize_detail_struct
	.section	.text.WIBOX_initialize_the_connection_process,"ax",%progbits
	.align	2
	.global	WIBOX_initialize_the_connection_process
	.type	WIBOX_initialize_the_connection_process, %function
WIBOX_initialize_the_connection_process:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI8:
	mov	r6, r0
	beq	.L80
	ldr	r0, .L82
	bl	Alert_Message
.L80:
	ldr	r3, .L82+4
	ldr	r2, .L82+8
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #44]
	cmp	r3, #0
	bne	.L81
	ldr	r0, .L82+12
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_Message
.L81:
	ldr	r4, .L82+16
	mov	r5, #0
	mov	r0, r6
	str	r6, [r4, #4]
	str	r5, [r4, #8]
	bl	power_down_device
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L82+20
	mov	r1, #2
	ldr	r0, [r3, #40]
	mov	r2, #1000
	mov	r3, r5
	bl	xTimerGenericCommand
	mov	r3, #1
	str	r3, [r4, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, pc}
.L83:
	.align	2
.L82:
	.word	.LC87
	.word	config_c
	.word	port_device_table
	.word	.LC88
	.word	.LANCHOR3
	.word	cics
.LFE12:
	.size	WIBOX_initialize_the_connection_process, .-WIBOX_initialize_the_connection_process
	.section	.text.WIBOX_connection_processing,"ax",%progbits
	.align	2
	.global	WIBOX_connection_processing
	.type	WIBOX_connection_processing, %function
WIBOX_connection_processing:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI9:
	sub	r0, r0, #121
	cmp	r0, #1
	bls	.L85
	ldr	r0, .L92
	bl	Alert_Message
	b	.L86
.L85:
	ldr	r4, .L92+4
	ldr	r5, [r4, #0]
	cmp	r5, #1
	beq	.L88
	cmp	r5, #2
	bne	.L84
	b	.L91
.L88:
	ldr	r0, [r4, #4]
	bl	power_up_device
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L92+8
	mov	r1, #2
	ldr	r0, [r3, #40]
	ldr	r2, .L92+12
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r3, #2
	str	r3, [r4, #0]
	b	.L84
.L91:
	ldr	r3, .L92+16
	ldr	r2, .L92+20
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #44]
	cmp	r3, #0
	beq	.L86
	ldr	r0, [r4, #4]
	blx	r3
	subs	r3, r0, #0
	beq	.L90
	ldr	r1, .L92+24
	mov	r2, #49
	ldr	r0, .L92+28
	bl	strlcpy
	ldr	r0, .L92+32
	bl	Alert_Message
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	CONTROLLER_INITIATED_after_connecting_perform_msg_housekeeping_activities
.L90:
	ldr	r2, [r4, #8]
	cmp	r2, #300
	bcs	.L86
	add	r2, r2, #1
	str	r2, [r4, #8]
	mvn	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L92+8
	mov	r1, r5
	ldr	r0, [r2, #40]
	mov	r2, #200
	bl	xTimerGenericCommand
	b	.L84
.L86:
	ldr	r3, .L92+8
	mov	r2, #1
	mov	r0, #123
	str	r2, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	CONTROLLER_INITIATED_post_event
.L84:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, pc}
.L93:
	.align	2
.L92:
	.word	.LC89
	.word	.LANCHOR3
	.word	cics
	.word	3000
	.word	config_c
	.word	port_device_table
	.word	.LC90
	.word	GuiVar_CommTestStatus
	.word	.LC91
.LFE13:
	.size	WIBOX_connection_processing, .-WIBOX_connection_processing
	.global	WIBOX_static_write_list
	.global	WIBOX_dhcp_write_list
	.global	WIBOX_setup_read_list
	.global	WIBOX_static_pvs
	.global	WIBOX_active_pvs
	.global	WIBOX_details
	.section	.rodata.WIBOX_static_write_list,"a",%progbits
	.align	2
	.type	WIBOX_static_write_list, %object
	.size	WIBOX_static_write_list, 1024
WIBOX_static_write_list:
	.word	.LC86
	.word	0
	.word	113
	.word	.LC92
	.word	.LC92
	.word	0
	.word	16
	.word	.LC93
	.word	.LC94
	.word	0
	.word	73
	.word	.LC95
	.word	.LC95
	.word	WIBOX_device_write_progress
	.word	49
	.word	.LC96
	.word	.LC96
	.word	0
	.word	50
	.word	.LC96
	.word	.LC96
	.word	0
	.word	51
	.word	.LC96
	.word	.LC96
	.word	0
	.word	52
	.word	.LC97
	.word	.LC98
	.word	0
	.word	57
	.word	.LC99
	.word	.LC99
	.word	0
	.word	53
	.word	.LC96
	.word	.LC96
	.word	0
	.word	54
	.word	.LC96
	.word	.LC96
	.word	0
	.word	55
	.word	.LC96
	.word	.LC96
	.word	0
	.word	56
	.word	.LC100
	.word	.LC100
	.word	0
	.word	58
	.word	.LC97
	.word	.LC101
	.word	0
	.word	68
	.word	.LC97
	.word	.LC93
	.word	0
	.word	69
	.word	.LC97
	.word	.LC35
	.word	0
	.word	40
	.word	.LC97
	.word	.LC37
	.word	0
	.word	48
	.word	.LC97
	.word	.LC39
	.word	0
	.word	46
	.word	.LC97
	.word	.LC102
	.word	0
	.word	59
	.word	.LC97
	.word	.LC103
	.word	0
	.word	41
	.word	.LC97
	.word	.LC104
	.word	0
	.word	65
	.word	.LC97
	.word	.LC45
	.word	0
	.word	39
	.word	.LC95
	.word	.LC105
	.word	0
	.word	60
	.word	.LC96
	.word	.LC96
	.word	0
	.word	61
	.word	.LC96
	.word	.LC96
	.word	0
	.word	62
	.word	.LC96
	.word	.LC96
	.word	0
	.word	63
	.word	.LC97
	.word	.LC106
	.word	0
	.word	64
	.word	.LC97
	.word	.LC107
	.word	0
	.word	44
	.word	.LC97
	.word	.LC108
	.word	0
	.word	47
	.word	.LC97
	.word	.LC109
	.word	0
	.word	45
	.word	.LC110
	.word	.LC110
	.word	0
	.word	45
	.word	.LC97
	.word	.LC111
	.word	0
	.word	66
	.word	.LC97
	.word	.LC112
	.word	0
	.word	67
	.word	.LC93
	.word	.LC93
	.word	0
	.word	143
	.word	.LC97
	.word	.LC113
	.word	0
	.word	144
	.word	.LC97
	.word	.LC114
	.word	0
	.word	145
	.word	.LC97
	.word	.LC115
	.word	0
	.word	146
	.word	.LC97
	.word	.LC69
	.word	0
	.word	147
	.word	.LC97
	.word	.LC72
	.word	0
	.word	148
	.word	.LC97
	.word	.LC116
	.word	0
	.word	149
	.word	.LC97
	.word	.LC117
	.word	0
	.word	150
	.word	.LC97
	.word	.LC118
	.word	0
	.word	151
	.word	.LC110
	.word	.LC119
	.word	0
	.word	152
	.word	.LC97
	.word	.LC120
	.word	0
	.word	153
	.word	.LC97
	.word	.LC116
	.word	0
	.word	154
	.word	.LC97
	.word	.LC117
	.word	0
	.word	155
	.word	.LC97
	.word	.LC118
	.word	0
	.word	156
	.word	.LC110
	.word	.LC119
	.word	0
	.word	157
	.word	.LC97
	.word	.LC72
	.word	0
	.word	158
	.word	.LC97
	.word	.LC116
	.word	0
	.word	159
	.word	.LC97
	.word	.LC117
	.word	0
	.word	160
	.word	.LC97
	.word	.LC118
	.word	0
	.word	161
	.word	.LC110
	.word	.LC119
	.word	0
	.word	162
	.word	.LC97
	.word	.LC72
	.word	0
	.word	163
	.word	.LC97
	.word	.LC121
	.word	0
	.word	164
	.word	.LC97
	.word	.LC122
	.word	0
	.word	165
	.word	.LC97
	.word	.LC123
	.word	0
	.word	166
	.word	.LC97
	.word	.LC124
	.word	0
	.word	167
	.word	.LC97
	.word	.LC125
	.word	0
	.word	168
	.word	.LC97
	.word	.LC126
	.word	0
	.word	169
	.word	.LC97
	.word	.LC93
	.word	0
	.word	16
	.word	.LC93
	.word	.LC93
	.word	0
	.word	108
	.word	.LC127
	.word	.LC127
	.word	0
	.word	15
	.word	.LC86
	.word	.LC86
	.word	dev_cli_disconnect
	.word	29
	.word	.LC86
	.section	.bss.wibox_cs,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	wibox_cs, %object
	.size	wibox_cs, 12
wibox_cs:
	.space	12
	.section	.bss.WIBOX_details,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	WIBOX_details, %object
	.size	WIBOX_details, 48
WIBOX_details:
	.space	48
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Writing device settings...\000"
.LC1:
	.ascii	"115200\015\000"
.LC2:
	.ascii	"4C\015\000"
.LC3:
	.ascii	"02\015\000"
.LC4:
	.ascii	"10001\015\000"
.LC5:
	.ascii	"C5\015\000"
.LC6:
	.ascii	"80\015\000"
.LC7:
	.ascii	"00\015\000"
.LC8:
	.ascii	"Invalid Channel 1 Setting\000"
.LC9:
	.ascii	"Reading device settings...\000"
.LC10:
	.ascii	" \000"
.LC11:
	.ascii	"*** Lantronix\000"
.LC12:
	.ascii	"DEVICE\000"
.LC13:
	.ascii	"MAC address\000"
.LC14:
	.ascii	"MAC ADDRESS\000"
.LC15:
	.ascii	"Software version\000"
.LC16:
	.ascii	"SW VERSION\000"
.LC17:
	.ascii	"IP addr \000"
.LC18:
	.ascii	"IP ADDRESS\000"
.LC19:
	.ascii	"-\000"
.LC20:
	.ascii	"/\000"
.LC21:
	.ascii	",\000"
.LC22:
	.ascii	"0\000"
.LC23:
	.ascii	"no gateway set\000"
.LC24:
	.ascii	"0.0.0.0\000"
.LC25:
	.ascii	", gateway\000"
.LC26:
	.ascii	"GW ADDRESS\000"
.LC27:
	.ascii	",netmask\000"
.LC28:
	.ascii	"\015\000"
.LC29:
	.ascii	"MASK\000"
.LC30:
	.ascii	"255.255.255.0\000"
.LC31:
	.ascii	"DHCP device name :\000"
.LC32:
	.ascii	"not set\000"
.LC33:
	.ascii	"DHCP NAME\000"
.LC34:
	.ascii	"*** Channel 1\000"
.LC35:
	.ascii	"Baudrate\000"
.LC36:
	.ascii	"BAUD RATE\000"
.LC37:
	.ascii	"I/F Mode\000"
.LC38:
	.ascii	"IF MODE\000"
.LC39:
	.ascii	"Flow\000"
.LC40:
	.ascii	"FLOW\000"
.LC41:
	.ascii	"Port\000"
.LC42:
	.ascii	"PORT\000"
.LC43:
	.ascii	"Connect\000"
.LC44:
	.ascii	"CONN MODE\000"
.LC45:
	.ascii	"Auto increment source port\000"
.LC46:
	.ascii	"AUTO INCR\000"
.LC47:
	.ascii	"enabled\000"
.LC48:
	.ascii	"disabled\000"
.LC49:
	.ascii	"--- none ---\000"
.LC50:
	.ascii	"Remote IP Adr:\000"
.LC51:
	.ascii	"REMOTE IP ADD\000"
.LC52:
	.ascii	", Port\000"
.LC53:
	.ascii	"REMOTE PORT\000"
.LC54:
	.ascii	"Disconn Mode :\000"
.LC55:
	.ascii	"DISCONN MODE\000"
.LC56:
	.ascii	"Flush   Mode :\000"
.LC57:
	.ascii	"FLUSH MODE\000"
.LC58:
	.ascii	"Error reading Channel 1 settings\000"
.LC59:
	.ascii	"*** WLAN\000"
.LC60:
	.ascii	"Network name: \000"
.LC61:
	.ascii	"\012\000"
.LC62:
	.ascii	"SSID\000"
.LC63:
	.ascii	"Security suite: \000"
.LC64:
	.ascii	"Security\000"
.LC65:
	.ascii	"none\000"
.LC66:
	.ascii	"WEP\000"
.LC67:
	.ascii	"WPA2/802.11i\000"
.LC68:
	.ascii	"Authentication: \000"
.LC69:
	.ascii	"Authentication\000"
.LC70:
	.ascii	"open/none\000"
.LC71:
	.ascii	"Encryption: \000"
.LC72:
	.ascii	"Encryption\000"
.LC73:
	.ascii	"WEP64\000"
.LC74:
	.ascii	"TKIP+WEP\000"
.LC75:
	.ascii	"CCMP+TKIP\000"
.LC76:
	.ascii	"CCMP+WEP\000"
.LC77:
	.ascii	"CCMP\000"
.LC78:
	.ascii	"Error reading WLAN settings\000"
.LC79:
	.ascii	"64.73.242.99\000"
.LC80:
	.ascii	"64\015\000"
.LC81:
	.ascii	"73\015\000"
.LC82:
	.ascii	"242\015\000"
.LC83:
	.ascii	"99\015\000"
.LC84:
	.ascii	"16001\015\000"
.LC85:
	.ascii	"PVS\000"
.LC86:
	.ascii	"\000"
.LC87:
	.ascii	"Why is the WiBox not on PORT A?\000"
.LC88:
	.ascii	"Unexpected NULL is_connected function\000"
.LC89:
	.ascii	"Connection Process : UNXEXP EVENT\000"
.LC90:
	.ascii	"Wi-Fi Connection\000"
.LC91:
	.ascii	"WiBox Connected\000"
.LC92:
	.ascii	"Mode\000"
.LC93:
	.ascii	"Your choice ?\000"
.LC94:
	.ascii	"*** basic parameters\000"
.LC95:
	.ascii	"IP Address :\000"
.LC96:
	.ascii	")\000"
.LC97:
	.ascii	"?\000"
.LC98:
	.ascii	"Set Gateway IP Address\000"
.LC99:
	.ascii	"Gateway IP addr\000"
.LC100:
	.ascii	"Netmask: Number of Bits for Host Part (0=default)\000"
.LC101:
	.ascii	"password (N)\000"
.LC102:
	.ascii	"Port No\000"
.LC103:
	.ascii	"ConnectMode\000"
.LC104:
	.ascii	"Send '+++' in Modem Mode\000"
.LC105:
	.ascii	"Remote IP Address\000"
.LC106:
	.ascii	"Remote Port\000"
.LC107:
	.ascii	"DisConnMode\000"
.LC108:
	.ascii	"FlushMode\000"
.LC109:
	.ascii	"DisConnTime\000"
.LC110:
	.ascii	":\000"
.LC111:
	.ascii	"SendChar 1\000"
.LC112:
	.ascii	"SendChar 2\000"
.LC113:
	.ascii	"Topology\000"
.LC114:
	.ascii	"Network name\000"
.LC115:
	.ascii	"Security suite\000"
.LC116:
	.ascii	"Change Key\000"
.LC117:
	.ascii	"Display key\000"
.LC118:
	.ascii	"Key type\000"
.LC119:
	.ascii	"Enter Key\000"
.LC120:
	.ascii	"TX Key index\000"
.LC121:
	.ascii	"TX Data rate: 0=fixed\000"
.LC122:
	.ascii	"TX Data rate: 0=1\000"
.LC123:
	.ascii	"Minimum TX Data rate\000"
.LC124:
	.ascii	"Enable power\000"
.LC125:
	.ascii	"Enable Soft\000"
.LC126:
	.ascii	"Max failed\000"
.LC127:
	.ascii	"Parameters stored ...\000"
.LC128:
	.ascii	"Network mode:\000"
.LC129:
	.ascii	"Change DHCP device name\000"
.LC130:
	.ascii	"Enter new DHCP device Name\000"
.LC131:
	.ascii	"exiting without save !\000"
	.section	.bss.WIBOX_active_pvs,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	WIBOX_active_pvs, %object
	.size	WIBOX_active_pvs, 404
WIBOX_active_pvs:
	.space	404
	.section	.bss.WIBOX_static_pvs,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	WIBOX_static_pvs, %object
	.size	WIBOX_static_pvs, 404
WIBOX_static_pvs:
	.space	404
	.section	.rodata.WIBOX_setup_read_list,"a",%progbits
	.align	2
	.type	WIBOX_setup_read_list, %object
	.size	WIBOX_setup_read_list, 80
WIBOX_setup_read_list:
	.word	.LC86
	.word	0
	.word	113
	.word	.LC92
	.word	.LC92
	.word	WIBOX_analyze_server_info
	.word	16
	.word	.LC93
	.word	.LC94
	.word	WIBOX_analyze_setup_mode
	.word	36
	.word	.LC131
	.word	.LC131
	.word	WIBOX_final_device_analysis
	.word	15
	.word	.LC86
	.word	.LC86
	.word	dev_cli_disconnect
	.word	29
	.word	.LC86
	.section	.rodata.WIBOX_dhcp_write_list,"a",%progbits
	.align	2
	.type	WIBOX_dhcp_write_list, %object
	.size	WIBOX_dhcp_write_list, 1008
WIBOX_dhcp_write_list:
	.word	.LC86
	.word	0
	.word	113
	.word	.LC92
	.word	.LC92
	.word	0
	.word	16
	.word	.LC93
	.word	.LC94
	.word	0
	.word	73
	.word	.LC128
	.word	.LC128
	.word	WIBOX_device_write_progress
	.word	170
	.word	.LC95
	.word	.LC95
	.word	0
	.word	49
	.word	.LC96
	.word	.LC96
	.word	0
	.word	50
	.word	.LC96
	.word	.LC96
	.word	0
	.word	51
	.word	.LC96
	.word	.LC96
	.word	0
	.word	52
	.word	.LC97
	.word	.LC98
	.word	0
	.word	57
	.word	.LC100
	.word	.LC100
	.word	0
	.word	58
	.word	.LC97
	.word	.LC101
	.word	0
	.word	68
	.word	.LC97
	.word	.LC129
	.word	0
	.word	43
	.word	.LC110
	.word	.LC130
	.word	0
	.word	42
	.word	.LC97
	.word	.LC93
	.word	0
	.word	69
	.word	.LC97
	.word	.LC35
	.word	0
	.word	40
	.word	.LC97
	.word	.LC37
	.word	0
	.word	48
	.word	.LC97
	.word	.LC39
	.word	0
	.word	46
	.word	.LC97
	.word	.LC102
	.word	0
	.word	59
	.word	.LC97
	.word	.LC103
	.word	0
	.word	41
	.word	.LC97
	.word	.LC104
	.word	0
	.word	65
	.word	.LC97
	.word	.LC45
	.word	0
	.word	39
	.word	.LC95
	.word	.LC105
	.word	0
	.word	60
	.word	.LC96
	.word	.LC96
	.word	0
	.word	61
	.word	.LC96
	.word	.LC96
	.word	0
	.word	62
	.word	.LC96
	.word	.LC96
	.word	0
	.word	63
	.word	.LC97
	.word	.LC106
	.word	0
	.word	64
	.word	.LC97
	.word	.LC107
	.word	0
	.word	44
	.word	.LC97
	.word	.LC108
	.word	0
	.word	47
	.word	.LC97
	.word	.LC109
	.word	0
	.word	45
	.word	.LC110
	.word	.LC110
	.word	0
	.word	45
	.word	.LC97
	.word	.LC111
	.word	0
	.word	66
	.word	.LC97
	.word	.LC112
	.word	0
	.word	67
	.word	.LC93
	.word	.LC93
	.word	0
	.word	143
	.word	.LC97
	.word	.LC113
	.word	0
	.word	144
	.word	.LC97
	.word	.LC114
	.word	0
	.word	145
	.word	.LC97
	.word	.LC115
	.word	0
	.word	146
	.word	.LC97
	.word	.LC69
	.word	0
	.word	147
	.word	.LC97
	.word	.LC72
	.word	0
	.word	148
	.word	.LC97
	.word	.LC116
	.word	0
	.word	149
	.word	.LC97
	.word	.LC117
	.word	0
	.word	150
	.word	.LC97
	.word	.LC118
	.word	0
	.word	151
	.word	.LC110
	.word	.LC119
	.word	0
	.word	152
	.word	.LC97
	.word	.LC120
	.word	0
	.word	153
	.word	.LC97
	.word	.LC116
	.word	0
	.word	154
	.word	.LC97
	.word	.LC117
	.word	0
	.word	155
	.word	.LC97
	.word	.LC118
	.word	0
	.word	156
	.word	.LC110
	.word	.LC119
	.word	0
	.word	157
	.word	.LC97
	.word	.LC72
	.word	0
	.word	158
	.word	.LC97
	.word	.LC116
	.word	0
	.word	159
	.word	.LC97
	.word	.LC117
	.word	0
	.word	160
	.word	.LC97
	.word	.LC118
	.word	0
	.word	161
	.word	.LC110
	.word	.LC119
	.word	0
	.word	162
	.word	.LC97
	.word	.LC72
	.word	0
	.word	163
	.word	.LC97
	.word	.LC121
	.word	0
	.word	164
	.word	.LC97
	.word	.LC122
	.word	0
	.word	165
	.word	.LC97
	.word	.LC123
	.word	0
	.word	166
	.word	.LC97
	.word	.LC124
	.word	0
	.word	167
	.word	.LC97
	.word	.LC125
	.word	0
	.word	168
	.word	.LC97
	.word	.LC126
	.word	0
	.word	169
	.word	.LC97
	.word	.LC93
	.word	0
	.word	16
	.word	.LC93
	.word	.LC93
	.word	0
	.word	108
	.word	.LC127
	.word	.LC127
	.word	0
	.word	15
	.word	.LC86
	.word	.LC86
	.word	dev_cli_disconnect
	.word	29
	.word	.LC86
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI0-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI1-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI2-.LFB0
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI5-.LFB7
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI7-.LFB11
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI8-.LFB12
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI9-.LFB13
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE20:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_WEN_WiBox.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x116
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF14
	.byte	0x1
	.4byte	.LASF15
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1f7
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x211
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0x4e
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.byte	0x6e
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.byte	0xfe
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x15d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1e4
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x277
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST4
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x3d3
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x3d9
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x3df
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x3f4
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST5
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x433
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x459
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST7
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB5
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB6
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB0
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB7
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI6
	.4byte	.LFE7
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB11
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB12
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB13
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF13:
	.ascii	"WIBOX_connection_processing\000"
.LASF12:
	.ascii	"WIBOX_initialize_the_connection_process\000"
.LASF2:
	.ascii	"WIBOX_analyze_server_info\000"
.LASF4:
	.ascii	"WIBOX_analyze_channel_1_info\000"
.LASF11:
	.ascii	"WIBOX_initialize_detail_struct\000"
.LASF6:
	.ascii	"WIBOX_analyze_setup_mode\000"
.LASF8:
	.ascii	"WIBOX_sizeof_setup_read_list\000"
.LASF15:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_WEN_WiBox.c\000"
.LASF14:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"WIBOX_final_device_analysis\000"
.LASF9:
	.ascii	"WIBOX_sizeof_dhcp_write_list\000"
.LASF5:
	.ascii	"WIBOX_analyze_wlan_info\000"
.LASF0:
	.ascii	"WIBOX_device_write_progress\000"
.LASF10:
	.ascii	"WIBOX_sizeof_static_write_list\000"
.LASF7:
	.ascii	"WIBOX_copy_active_values_to_static_values\000"
.LASF3:
	.ascii	"WIBOX_analyze_basic_info\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
