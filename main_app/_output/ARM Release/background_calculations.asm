	.file	"background_calculations.c"
	.text
.Ltext0:
	.global	__umodsi3
	.section	.text.background_calculation_task,"ax",%progbits
	.align	2
	.global	background_calculation_task
	.type	background_calculation_task, %function
background_calculation_task:
.LFB1:
	@ args = 0, pretend = 0, frame = 176
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L56
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
.LBB4:
	ldr	r6, .L56+4
.LBE4:
	rsb	r3, r3, r0
	mov	r3, r3, lsr #5
	sub	sp, sp, #188
.LCFI1:
	str	r3, [sp, #28]
	ldr	r3, .L56+8
	mov	r2, #0
	str	r2, [r3, #0]
.L38:
	ldr	r3, .L56+12
	add	r1, sp, #168
	ldr	r0, [r3, #0]
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	bne	.L2
	mov	r0, #240
	bl	vTaskDelay
	ldr	r3, .L56+16
	ldr	r2, [sp, #168]
	cmp	r2, r3
	bne	.L2
	ldr	r3, .L56+8
	mov	r2, #0
	str	r2, [r3, #0]
.LBB5:
	ldr	r3, .L56+20
	add	r0, sp, #148
	ldr	r3, [r3, #0]
	str	r3, [sp, #32]
	bl	EPSON_obtain_latest_complete_time_and_date
	ldr	r4, [sp, #148]
	ldrh	r2, [sp, #152]
	mov	r0, r4
	mov	r1, #600
	str	r2, [sp, #16]
	bl	__umodsi3
	ldr	r3, .L56+24
	cmp	r0, #0
	addne	r4, r4, #600
	rsbne	r4, r0, r4
	strne	r4, [sp, #148]
	ldr	r2, [sp, #148]
	mov	r4, #0
	cmp	r2, r3
	ldrhi	r2, [sp, #16]
	movhi	r3, #0
	strhi	r3, [sp, #148]
	addhi	r3, r2, #1
	strhih	r3, [sp, #152]	@ movhi
	ldr	r3, .L56+28
	str	r4, [r3, #0]
	ldr	r3, .L56+32
	str	r4, [r3, #0]
	bl	NETWORK_CONFIG_get_controller_off
	cmp	r0, #0
	bne	.L6
	mov	r4, r0
	mov	r8, r0
.L32:
	add	r3, sp, #184
	str	r3, [sp, #0]
	ldrh	r0, [sp, #152]
	add	r3, sp, #180
	add	r1, sp, #172
	add	r2, sp, #176
	bl	DateToDMY
	ldr	r3, [sp, #176]
	strh	r3, [sp, #156]	@ movhi
	ldr	r3, [sp, #172]
	strh	r3, [sp, #154]	@ movhi
	ldr	r3, [sp, #180]
	strh	r3, [sp, #158]	@ movhi
	ldr	r3, [sp, #184]
	strb	r3, [sp, #166]
	b	.L7
.L18:
	mov	r0, r9
	bl	MANUAL_PROGRAMS_get_group_at_this_index
	subs	r5, r0, #0
	beq	.L8
	ldrh	r7, [sp, #152]
	bl	MANUAL_PROGRAMS_get_start_date
	cmp	r7, r0
	bcc	.L9
	mov	r0, r5
	ldrh	r7, [sp, #152]
	bl	MANUAL_PROGRAMS_get_end_date
	cmp	r7, r0
	bhi	.L9
	mov	r0, r5
	ldr	r1, [sp, #184]
	bl	MANUAL_PROGRAMS_today_is_a_water_day
	cmp	r0, #1
	bne	.L9
	ldr	fp, .L56+8
	mov	sl, #0
.L16:
	mov	r0, r5
	mov	r1, sl
	bl	MANUAL_PROGRAMS_get_start_time
	ldr	r3, [sp, #148]
	cmp	r0, r3
	bne	.L10
	mov	r1, #400
	ldr	r2, .L56+36
	mov	r3, #164
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L56+40
	bl	nm_ListGetFirst
	mov	r7, r0
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	b	.L11
.L15:
	mov	r0, r5
	bl	nm_GROUP_get_group_ID
	mov	r1, r0
	mov	r0, r7
	bl	STATION_does_this_station_belong_to_this_manual_program
	cmp	r0, #0
	beq	.L12
	mov	r0, r7
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L12
	mov	r0, r5
	bl	MANUAL_PROGRAMS_get_run_time
	cmp	r0, #0
	beq	.L12
	mov	r0, r7
	bl	STATION_get_no_water_days
	rsb	r3, r8, r0
	cmp	r3, #0
	blt	.L13
	cmp	r0, r8
	bne	.L41
	ldr	r3, [sp, #148]
	cmp	r3, #14400
	bls	.L41
.L13:
	ldr	r3, .L56+28
	mov	r2, #2
	str	r2, [r3, #0]
	mov	r0, r5
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L56+32
	str	r0, [r3, #0]
	mov	r0, r5
	bl	nm_GROUP_get_name
	mov	r1, #0
	mov	r4, r0
	ldr	r0, .L56+44
	bl	GuiLib_GetTextPtr
	mov	r3, r4
	mov	r1, #48
	mov	r4, #0
	ldr	r2, .L56+48
	str	r0, [sp, #0]
	add	r0, sp, #100
	bl	snprintf
	ldr	r2, [sp, #148]
	mov	r3, r4
	add	r0, sp, #36
	mov	r1, #64
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldrh	r3, [sp, #152]
	ldr	r2, [sp, #16]
	cmp	r3, r2
	bne	.L14
	mov	r1, r4
	ldr	r0, .L56+52
	bl	GuiLib_GetTextPtr
	mov	r2, #64
	mov	r1, r0
	add	r0, sp, #36
	bl	strlcat
	b	.L52
.L14:
	ldr	r0, [sp, #184]
	bl	GetDayLongStr
	mov	r4, r0
	ldr	r0, [sp, #176]
	sub	r0, r0, #1
	bl	GetMonthShortStr
	ldr	r3, [sp, #172]
	mov	r1, #64
	str	r3, [sp, #4]
	ldr	r3, [sp, #180]
	ldr	r2, .L56+56
	str	r3, [sp, #8]
	mov	r3, r4
	str	r0, [sp, #0]
	add	r0, sp, #36
	bl	sp_strlcat
.L52:
	mov	r4, #1
	b	.L10
.L41:
	mov	r4, #1
.L12:
	mov	r3, #208
	ldr	r2, .L56+36
	mov	r1, #400
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r7
	ldr	r0, .L56+40
	bl	nm_ListGetNext
	mov	r7, r0
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, [fp, #0]
	cmp	r3, #1
	beq	.L10
.L11:
	cmp	r7, #0
	bne	.L15
.L10:
	ldr	r3, .L56+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L9
	add	sl, sl, #1
	cmp	sl, #6
	bne	.L16
	b	.L9
.L8:
	ldr	r0, .L56+36
	mov	r1, #233
	bl	Alert_group_not_found_with_filename
.L9:
	ldr	r3, .L56+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L17
	add	r9, r9, #1
.L31:
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	cmp	r9, r0
	bcc	.L18
.L17:
	ldr	r3, .L56+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L19
	ldr	r3, .L56+28
	ldr	sl, [r3, #0]
	cmp	sl, #0
	beq	.L20
	b	.L19
.L29:
	mov	r0, sl
	bl	STATION_GROUP_get_group_at_this_index
	subs	r7, r0, #0
	beq	.L21
	ldr	r5, [sp, #148]
	bl	SCHEDULE_get_start_time
	cmp	r5, r0
	bne	.L22
	mov	r0, r7
	bl	SCHEDULE_get_start_time
	mov	r5, r0
	mov	r0, r7
	bl	SCHEDULE_get_stop_time
	cmp	r5, r0
	beq	.L22
	mov	r0, r7
	add	r1, sp, #148
	mov	r2, #0
	bl	SCHEDULE_does_it_irrigate_on_this_date
	cmp	r0, #0
	beq	.L22
	mov	r1, #400
	ldr	r2, .L56+36
	ldr	r3, .L56+60
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L56+40
	bl	nm_ListGetFirst
	ldr	r9, .L56+8
	mov	r5, r0
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	b	.L23
.L27:
	mov	r0, r5
	bl	STATION_station_is_available_for_use
	cmp	r0, #1
	str	r0, [sp, #20]
	bne	.L24
	mov	r0, r5
	bl	STATION_get_GID_station_group
	mov	fp, r0
	mov	r0, r7
	bl	nm_GROUP_get_group_ID
	cmp	fp, r0
	bne	.L24
	mov	r0, r5
	bl	STATION_get_no_water_days
	str	r0, [sp, #24]
	mov	r0, r5
	bl	WEATHER_get_station_uses_daily_et
	mov	fp, r0
	cmp	fp, #1
	mov	r0, r5
	bne	.L25
	bl	STATION_get_ET_factor_100u
	cmp	r0, #0
	beq	.L24
	ldr	r2, [sp, #24]
	rsb	r3, r8, r2
	cmp	r3, #0
	bgt	.L43
	mov	r0, r7
	str	fp, [sp, #12]
	bl	nm_GROUP_get_group_ID
	ldr	r2, .L56+32
	ldr	r3, [sp, #12]
	mov	r4, r3
	str	r0, [r2, #0]
	ldr	r2, .L56+28
	str	r3, [r2, #0]
	b	.L26
.L25:
	bl	STATION_get_total_run_minutes_10u
	cmp	r0, #0
	beq	.L24
	ldr	r2, [sp, #24]
	rsb	r3, r8, r2
	cmp	r3, #0
	bgt	.L44
	ldr	fp, [sp, #20]
	ldr	r3, .L56+28
	mov	r4, fp
	str	fp, [r3, #0]
	b	.L26
.L43:
	mov	r4, fp
	b	.L24
.L44:
	ldr	r4, [sp, #20]
.L24:
	ldr	r3, .L56+64
	ldr	r2, .L56+36
	mov	r1, #400
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L56+40
	bl	nm_ListGetNext
	mov	r5, r0
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, [r9, #0]
	cmp	r3, #1
	beq	.L26
.L23:
	cmp	r5, #0
	bne	.L27
.L26:
	ldr	r3, .L56+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L22
	mov	r0, r7
	bl	nm_GROUP_get_name
	mov	r1, #0
	mov	r5, r0
	ldr	r0, .L56+44
	bl	GuiLib_GetTextPtr
	mov	r3, r5
	mov	r1, #48
	mov	r5, #0
	ldr	r2, .L56+48
	str	r0, [sp, #0]
	add	r0, sp, #100
	bl	snprintf
	ldr	r2, [sp, #148]
	mov	r3, r5
	add	r0, sp, #36
	mov	r1, #64
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	ldrh	r3, [sp, #152]
	ldr	r2, [sp, #16]
	cmp	r3, r2
	bne	.L28
	mov	r1, r5
	ldr	r0, .L56+52
	bl	GuiLib_GetTextPtr
	mov	r2, #64
	mov	r1, r0
	add	r0, sp, #36
	bl	strlcat
	b	.L19
.L28:
	ldr	r0, [sp, #184]
	bl	GetDayLongStr
	mov	r5, r0
	ldr	r0, [sp, #176]
	sub	r0, r0, #1
	bl	GetMonthShortStr
	ldr	r3, [sp, #172]
	mov	r1, #64
	str	r3, [sp, #4]
	ldr	r3, [sp, #180]
	ldr	r2, .L56+56
	str	r3, [sp, #8]
	mov	r3, r5
	str	r0, [sp, #0]
	add	r0, sp, #36
	bl	sp_strlcat
	b	.L19
.L21:
	ldr	r0, .L56+36
	ldr	r1, .L56+68
	bl	Alert_group_not_found_with_filename
.L22:
	add	sl, sl, #1
.L20:
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	sl, r0
	bcc	.L29
.L19:
	ldr	r3, [sp, #148]
	add	r3, r3, #600
	str	r3, [sp, #148]
.L7:
	ldr	r3, .L56+24
	ldr	r2, [sp, #148]
	cmp	r2, r3
	ldr	r3, .L56+28
	bhi	.L30
	ldr	r9, [r3, #0]
	cmp	r9, #0
	beq	.L31
.L30:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L6
	str	r3, [sp, #148]
	ldrh	r3, [sp, #152]
	add	r8, r8, #1
	add	r3, r3, #1
	cmp	r8, #38
	strh	r3, [sp, #152]	@ movhi
	bne	.L32
.L6:
	ldr	r7, .L56+28
	ldr	r3, [r7, #0]
	sub	r3, r3, #1
	cmp	r3, #1
	bhi	.L33
	add	r1, sp, #100
	ldr	r0, .L56+72
	mov	r2, #49
	bl	strlcpy
	ldr	r0, .L56+76
	add	r1, sp, #36
	b	.L53
.L33:
	bl	NETWORK_CONFIG_get_controller_off
	cmp	r0, #0
	beq	.L35
	mov	r1, #0
	ldr	r0, .L56+80
	bl	GuiLib_GetTextPtr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L56+72
	bl	strlcpy
	ldr	r0, .L56+84
	mov	r1, #0
	b	.L55
.L35:
	bl	STATION_get_num_stations_assigned_to_groups
	subs	r5, r0, #0
	bne	.L36
	mov	r3, #3
	str	r3, [r7, #0]
	mov	r1, r5
	ldr	r0, .L56+84
	bl	GuiLib_GetTextPtr
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L56+72
	bl	strlcpy
	ldr	r0, .L56+88
	mov	r1, r5
.L55:
	bl	GuiLib_GetTextPtr
	mov	r1, r0
	ldr	r0, .L56+76
	b	.L53
.L36:
	cmp	r4, #0
	bne	.L37
	mov	r1, r4
	ldr	r0, .L56+92
	bl	GuiLib_GetTextPtr
	mov	r1, r0
	ldr	r0, .L56+72
	b	.L54
.L37:
	ldr	r0, .L56+72
	ldr	r1, .L56+96
.L54:
	mov	r2, #49
	bl	strlcpy
	ldr	r0, .L56+76
	ldr	r1, .L56+96
.L53:
	mov	r2, #49
	bl	strlcpy
	ldr	r3, .L56+20
	ldr	r2, [r3, #0]
	ldr	r3, [sp, #32]
	rsb	r2, r3, r2
	ldr	r3, .L56+100
	ldr	r1, [r3, #0]
	cmp	r2, r1
	strhi	r2, [r3, #0]
.L2:
.LBE5:
	bl	xTaskGetTickCount
	ldr	r3, .L56+104
	ldr	r2, [sp, #28]
	str	r0, [r3, r2, asl #2]
	b	.L38
.L57:
	.align	2
.L56:
	.word	Task_Table
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR0
	.word	Background_Calculation_Queue
	.word	4369
	.word	my_tick_count
	.word	86399
	.word	GuiVar_StatusTypeOfSchedule
	.word	GuiVar_StatusNextScheduledIrriGID
	.word	.LC0
	.word	station_info_list_hdr
	.word	1112
	.word	.LC1
	.word	1089
	.word	.LC2
	.word	257
	.word	298
	.word	335
	.word	GuiVar_StatusNextScheduledIrriType
	.word	GuiVar_StatusNextScheduledIrriDate
	.word	902
	.word	1085
	.word	885
	.word	1084
	.word	.LC3
	.word	.LANCHOR1
	.word	task_last_execution_stamp
.LFE1:
	.size	background_calculation_task, .-background_calculation_task
	.section	.text.postBackground_Calculation_Event,"ax",%progbits
	.align	2
	.global	postBackground_Calculation_Event
	.type	postBackground_Calculation_Event, %function
postBackground_Calculation_Event:
.LFB2:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L62
	stmfd	sp!, {r0, r4, lr}
.LCFI2:
	cmp	r0, r3
	str	r0, [sp, #0]
	bne	.L58
	ldr	r4, .L62+4
	ldr	r2, [r4, #0]
	cmp	r2, #0
	bne	.L58
	ldr	r3, .L62+8
	mov	r1, sp
	ldr	r0, [r3, #0]
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #0
	movne	r3, #1
	strne	r3, [r4, #0]
	bne	.L58
	ldr	r0, .L62+12
	bl	Alert_Message
.L58:
	ldmfd	sp!, {r3, r4, pc}
.L63:
	.align	2
.L62:
	.word	4369
	.word	.LANCHOR0
	.word	Background_Calculation_Queue
	.word	.LC4
.LFE2:
	.size	postBackground_Calculation_Event, .-postBackground_Calculation_Event
	.section	.bss.__largest_next_scheduled_delta,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	__largest_next_scheduled_delta, %object
	.size	__largest_next_scheduled_delta, 4
__largest_next_scheduled_delta:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/b"
	.ascii	"ackground_calculations.c\000"
.LC1:
	.ascii	"%s %s\000"
.LC2:
	.ascii	" on %s, %s %d, %d\000"
.LC3:
	.ascii	"\000"
.LC4:
	.ascii	"Background Calculation QUEUE OVERFLOW!\000"
	.section	.bss.STATUS_next_scheduled_calc_queued,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	STATUS_next_scheduled_calc_queued, %object
	.size	STATUS_next_scheduled_calc_queued, 4
STATUS_next_scheduled_calc_queued:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0xe0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/background_calculations.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x50
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x34
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x193
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x1d0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 224
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"postBackground_Calculation_Event\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/b"
	.ascii	"ackground_calculations.c\000"
.LASF0:
	.ascii	"background_calculation_task\000"
.LASF4:
	.ascii	"STATUS_load_next_scheduled_into_guivars\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
