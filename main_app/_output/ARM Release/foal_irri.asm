	.file	"foal_irri.c"
	.text
.Ltext0:
	.section	.text._nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting,"ax",%progbits
	.align	2
	.type	_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting, %function
_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrh	r2, [r0, #0]
	mov	r3, r0
	and	r2, r2, #960
	cmp	r2, #64
	movne	r0, #0
	bne	.L2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ands	r0, r0, #16
	and	r2, r2, #48
	movne	r0, #65536
	cmp	r2, #48
	addeq	r0, r0, #28672
	beq	.L2
	cmp	r2, #32
	addeq	r0, r0, #24576
	beq	.L2
	cmp	r2, #16
	addeq	r0, r0, #20480
.L2:
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	tst	r2, #8
	addne	r0, r0, #256
	tst	r2, #4
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	addne	r0, r0, #16
	tst	r2, #1
	ldrneb	r3, [r3, #2]	@ zero_extendqisi2
	movne	r3, r3, asl #28
	addne	r0, r0, r3, lsr #30
	bx	lr
.LFE7:
	.size	_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting, .-_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting
	.global	__umodsi3
	.global	__udivsi3
	.section	.text.foal_irri_cycle_and_soak_weighting,"ax",%progbits
	.align	2
	.type	foal_irri_cycle_and_soak_weighting, %function
foal_irri_cycle_and_soak_weighting:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldrne	r5, [r1, #72]
	ldreq	r5, [r1, #56]
	ldrne	r4, [r1, #60]
	ldrne	r6, [r1, #80]
	ldreq	r4, [r1, #52]
	ldreq	r6, [r1, #64]
	cmp	r5, #0
	bne	.L14
	ldr	r0, .L20
	bl	Alert_Message
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, pc}
.L14:
	cmp	r4, #0
	moveq	r0, r4
	ldmeqfd	sp!, {r4, r5, r6, pc}
	cmp	r4, r5
	bls	.L18
	mov	r1, r5
	mov	r0, r4
	bl	__umodsi3
	mov	r1, r5
	cmp	r0, #0
	mov	r0, r4
	bne	.L16
	bl	__udivsi3
	sub	r0, r0, #1
	b	.L19
.L16:
	bl	__udivsi3
.L19:
	mul	r0, r6, r0
	ldmfd	sp!, {r4, r5, r6, pc}
.L18:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L21:
	.align	2
.L20:
	.word	.LC0
.LFE6:
	.size	foal_irri_cycle_and_soak_weighting, .-foal_irri_cycle_and_soak_weighting
	.section	.text.nm_return_station_this_station_belongs_before_in_the_irrigation_list,"ax",%progbits
	.align	2
	.type	nm_return_station_this_station_belongs_before_in_the_irrigation_list, %function
nm_return_station_this_station_belongs_before_in_the_irrigation_list:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI1:
	mov	r4, r0
	mov	r0, r1
	mov	r5, r1
	mov	r7, r2
	bl	nm_ListGetFirst
	b	.L31
.L30:
.LBB15:
	cmp	r4, #0
	beq	.L24
	mov	r8, r7
	mov	sl, r6
	ldrh	r0, [r8, #104]!
	ldrh	r1, [sl, #104]!
	mov	r0, r0, lsr #6
	mov	r1, r1, lsr #6
	mov	r3, #0
	and	r0, r0, #15
	and	r1, r1, #15
	mov	r2, r3
	b	.L25
.L24:
	ldrh	r0, [r7, #72]
	ldrh	r1, [r6, #72]
	ldrb	r2, [r7, #73]	@ zero_extendqisi2
	ldrb	r3, [r6, #73]	@ zero_extendqisi2
	mov	r0, r0, lsr #6
	mov	r1, r1, lsr #6
	mov	r2, r2, lsr #5
	mov	r3, r3, lsr #5
	and	r0, r0, #15
	and	r1, r1, #15
	and	r2, r2, #1
	and	r3, r3, #1
	add	r8, r7, #72
	add	sl, r6, #72
.L25:
	cmp	r0, r1
	bhi	.L26
	bcc	.L27
	cmp	r4, #0
	bne	.L28
	ldrh	r1, [r7, #72]
	and	r1, r1, #960
	cmp	r1, #320
	beq	.L27
.L28:
	cmp	r2, #0
	beq	.L29
	cmp	r3, #0
	bne	.L27
	b	.L26
.L29:
	cmp	r3, #0
	bne	.L27
	mov	r0, r8
	bl	_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting
	mov	r9, r0
	mov	r0, sl
	bl	_nm_pumpuse_priority_setexpected_flowchecking_irrigatedlasttime_weighting
	cmp	r9, r0
	bhi	.L26
	bcc	.L27
	ldrh	r3, [r8, #0]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L27
	mov	r1, r7
	mov	r0, r4
	bl	foal_irri_cycle_and_soak_weighting
	mov	r1, r6
	mov	r8, r0
	mov	r0, r4
	bl	foal_irri_cycle_and_soak_weighting
	cmp	r8, r0
	bls	.L27
.L26:
.LBE15:
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L27:
	mov	r0, r5
	mov	r1, r6
	bl	nm_ListGetNext
.L31:
	mov	r6, r0
	cmp	r6, #0
	bne	.L30
	b	.L26
.LFE9:
	.size	nm_return_station_this_station_belongs_before_in_the_irrigation_list, .-nm_return_station_this_station_belongs_before_in_the_irrigation_list
	.section	.text.nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc,"ax",%progbits
	.align	2
	.type	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc, %function
nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r3, .L38
	mov	r4, #0
	b	.L33
.L37:
	ldr	r2, [r3, #148]
	cmp	r2, #0
	bne	.L34
	ldr	r2, [r3, #172]
	cmp	r2, #0
	bne	.L34
	ldr	r3, .L38
	mov	r2, #96
	mla	r3, r2, r4, r3
	ldr	r3, [r3, #160]
	cmp	r3, #0
	beq	.L35
	ldr	r0, .L38+4
	bl	RemovePathFromFileName
	ldr	r2, .L38+8
	mov	r1, r0
	ldr	r0, .L38+12
	bl	Alert_Message_va
.L35:
	ldr	r3, .L38+16
	mov	r2, #96
	mla	r4, r2, r4, r3
	mov	r1, #0
	mov	r0, r4
	bl	memset
	b	.L36
.L34:
	add	r4, r4, #1
	add	r3, r3, #96
.L33:
	cmp	r4, #768
	bne	.L37
	ldr	r0, .L38+20
	bl	Alert_Message
	mov	r4, #0
.L36:
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L39:
	.align	2
.L38:
	.word	foal_irri
	.word	.LC1
	.word	2994
	.word	.LC2
	.word	foal_irri+140
	.word	.LC3
.LFE26:
	.size	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc, .-nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	.section	.text.freeze_switch_timer_callback,"ax",%progbits
	.align	2
	.type	freeze_switch_timer_callback, %function
freeze_switch_timer_callback:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L41
	mov	r2, #0
	str	r2, [r3, #104]
	b	Alert_freeze_switch_inactive
.L42:
	.align	2
.L41:
	.word	weather_preserves
.LFE1:
	.size	freeze_switch_timer_callback, .-freeze_switch_timer_callback
	.section	.text.rain_switch_timer_callback,"ax",%progbits
	.align	2
	.type	rain_switch_timer_callback, %function
rain_switch_timer_callback:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L44
	mov	r2, #0
	str	r2, [r3, #100]
	b	Alert_rain_switch_inactive
.L45:
	.align	2
.L44:
	.word	weather_preserves
.LFE0:
	.size	rain_switch_timer_callback, .-rain_switch_timer_callback
	.section	.text.nm_at_starttime_decrement_NOW_days_count,"ax",%progbits
	.align	2
	.type	nm_at_starttime_decrement_NOW_days_count, %function
nm_at_starttime_decrement_NOW_days_count:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #2112
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI3:
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	bcc	.L47
	ldr	r0, .L53
	ldr	r1, .L53+4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_index_out_of_range_with_filename
.L47:
	bl	STATION_get_no_water_days
	cmp	r0, #0
	beq	.L48
	mov	r0, r4
	bl	STATION_decrement_no_water_days_and_return_value_after_decrement
	cmp	r0, #0
	ldmnefd	sp!, {r4, r5, r6, pc}
.LBB18:
	ldrb	r3, [r6, #0]	@ zero_extendqisi2
	ldrb	r2, [r6, #1]	@ zero_extendqisi2
	ldr	r1, .L53+8
	orr	r2, r3, r2, asl #8
	ldrb	r3, [r6, #2]	@ zero_extendqisi2
	orr	r2, r2, r3, asl #16
	ldrb	r3, [r6, #3]	@ zero_extendqisi2
	orr	r2, r2, r3, asl #24
	ldr	r3, .L53+12
	cmp	r2, r3
	mov	r2, r5, asl #7
	ldrb	r3, [r6, #4]	@ zero_extendqisi2
	add	r2, r1, r2
	ldrb	r1, [r6, #5]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #8
	addhi	r3, r3, #1
	strh	r3, [r2, #132]	@ movhi
	ldr	r3, .L53+8
	add	r5, r3, r5, asl #7
	ldr	r3, .L53+12
	str	r3, [r5, #124]
	ldmfd	sp!, {r4, r5, r6, pc}
.L48:
.LBE18:
	ldr	r3, .L53+8
	add	r5, r3, r5, asl #7
	ldrb	r3, [r5, #141]	@ zero_extendqisi2
	bic	r3, r3, #4
	strb	r3, [r5, #141]
	add	r5, r5, #140
	ldmfd	sp!, {r4, r5, r6, pc}
.L54:
	.align	2
.L53:
	.word	.LC1
	.word	3829
	.word	station_preserves
	.word	18000
.LFE29:
	.size	nm_at_starttime_decrement_NOW_days_count, .-nm_at_starttime_decrement_NOW_days_count
	.section	.text.FOAL_IRRI_load_sfml_for_distribution_to_slaves,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_load_sfml_for_distribution_to_slaves
	.type	FOAL_IRRI_load_sfml_for_distribution_to_slaves, %function
FOAL_IRRI_load_sfml_for_distribution_to_slaves:
.LFB5:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L69
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI4:
	ldr	r2, .L69+4
	mov	r5, #0
	str	r5, [r1, #0]
	sub	sp, sp, #28
.LCFI5:
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L69+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L69+12
	strh	r5, [sp, #26]	@ movhi
	bl	nm_ListGetFirst
	b	.L67
.L58:
	ldrb	r3, [r1, #76]	@ zero_extendqisi2
	ldr	r0, .L69+12
	tst	r3, #2
	ldrneh	r3, [sp, #26]
	addne	r3, r3, #1
	strneh	r3, [sp, #26]	@ movhi
	bl	nm_ListGetNext
.L67:
	cmp	r0, #0
	mov	r1, r0
	bne	.L58
	ldrh	r3, [sp, #26]
	cmp	r3, #0
	moveq	r6, r0
	beq	.L59
.LBB19:
	mov	r5, #24
	mul	r5, r3, r5
	ldr	r1, .L69+4
	add	r6, r5, #2
	mov	r0, r6
	ldr	r2, .L69+16
	bl	mem_malloc_debug
	add	r1, sp, #26
	mov	r2, #2
	str	r0, [r4, #0]
	mov	r7, r0
	bl	memcpy
	ldr	r0, .L69+12
	add	r7, r7, #2
	bl	nm_ListGetFirst
	b	.L68
.L64:
	ldrb	r3, [r4, #76]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L61
.LBB20:
	ldrh	r3, [sp, #26]
	cmp	r3, #0
	bne	.L62
	ldr	r0, .L69+4
	bl	RemovePathFromFileName
	mov	r2, #516
	mov	r1, r0
	ldr	r0, .L69+20
	bl	Alert_Message_va
	b	.L63
.L62:
	sub	r3, r3, #1
	strh	r3, [sp, #26]	@ movhi
	ldr	r3, [r4, #40]
	mov	r0, r7
	ldr	r3, [r3, #0]
	mov	r1, sp
	str	r3, [sp, #0]
	ldrb	r3, [r4, #91]	@ zero_extendqisi2
	mov	r2, #24
	str	r3, [sp, #16]
	ldrb	r3, [r4, #90]	@ zero_extendqisi2
	add	r7, r7, #24
	strb	r3, [sp, #20]
	ldrh	r3, [r4, #72]
	sub	r5, r5, #24
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	strb	r3, [sp, #21]
	ldr	r3, [r4, #52]
	str	r3, [sp, #4]
	ldr	r3, [r4, #56]
	str	r3, [sp, #12]
	ldr	r3, [r4, #60]
	str	r3, [sp, #8]
	bl	memcpy
	ldrb	r3, [r4, #76]	@ zero_extendqisi2
	bic	r3, r3, #2
	strb	r3, [r4, #76]
.L61:
.LBE20:
	ldr	r0, .L69+12
	mov	r1, r4
	bl	nm_ListGetNext
.L68:
	cmp	r0, #0
	mov	r4, r0
	bne	.L64
.L63:
	ldrh	r3, [sp, #26]
	cmp	r3, #0
	cmpeq	r5, #0
	beq	.L59
	ldr	r0, .L69+4
	bl	RemovePathFromFileName
	ldr	r2, .L69+24
	mov	r1, r0
	ldr	r0, .L69+28
	bl	Alert_Message_va
.L59:
.LBE19:
	ldr	r3, .L69
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L70:
	.align	2
.L69:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	451
	.word	foal_irri+16
	.word	493
	.word	.LC4
	.word	551
	.word	.LC5
.LFE5:
	.size	FOAL_IRRI_load_sfml_for_distribution_to_slaves, .-FOAL_IRRI_load_sfml_for_distribution_to_slaves
	.section	.text.FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
	.type	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list, %function
FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, lr}
.LCFI6:
	mov	r5, r0
	mov	r4, r1
	bne	.L72
.LBB23:
	ldr	r3, .L75
	ldr	r2, .L75+4
	ldr	r0, .L75+8
	bl	nm_ListRemove_debug
	mov	r2, r4
	mov	r0, r5
	ldr	r1, .L75+8
	bl	nm_return_station_this_station_belongs_before_in_the_irrigation_list
	mov	r1, r4
	mov	r2, r0
	ldr	r0, .L75+8
.LBE23:
	ldmfd	sp!, {r4, r5, lr}
.LBB24:
	b	nm_ListInsert
.L72:
.LBE24:
	ldr	r3, .L75+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L75+4
	ldr	r3, .L75+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L75+20
	mov	r1, r4
	ldr	r2, .L75+4
	ldr	r3, .L75+24
	bl	nm_ListRemove_debug
	cmp	r0, #0
	beq	.L73
	ldr	r0, .L75+28
	bl	Alert_Message
	b	.L74
.L73:
	ldr	r1, .L75+20
	mov	r2, r4
	bl	nm_return_station_this_station_belongs_before_in_the_irrigation_list
	mov	r1, r4
	mov	r2, r0
	ldr	r0, .L75+20
	bl	nm_ListInsert
.L74:
	ldr	r3, .L75+12
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L76:
	.align	2
.L75:
	.word	1066
	.word	.LC1
	.word	ft_irrigating_stations_list_hdr
	.word	list_foal_irri_recursive_MUTEX
	.word	1078
	.word	foal_irri+16
	.word	1083
	.word	.LC6
.LFE10:
	.size	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list, .-FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
	.section	.text.nm_shut_down_mvs_and_pmps_throughout_this_system,"ax",%progbits
	.align	2
	.global	nm_shut_down_mvs_and_pmps_throughout_this_system
	.type	nm_shut_down_mvs_and_pmps_throughout_this_system, %function
nm_shut_down_mvs_and_pmps_throughout_this_system:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L81
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI7:
	ldr	r4, .L81+4
	mov	r2, #0
	mov	r5, r3
	mov	r1, r2
.L79:
	ldr	ip, [r3, #16]
	cmp	ip, r0
	bne	.L78
	mla	ip, r4, r2, r5
	ldrb	r6, [ip, #480]	@ zero_extendqisi2
	and	r6, r6, #159
	strb	r6, [ip, #480]
	str	r1, [r3, #464]
	str	r1, [r3, #468]
	str	r1, [r3, #228]
.L78:
	add	r2, r2, #1
	add	r3, r3, #14208
	cmp	r2, #4
	add	r3, r3, #16
	bne	.L79
	ldmfd	sp!, {r4, r5, r6, pc}
.L82:
	.align	2
.L81:
	.word	system_preserves
	.word	14224
.LFE12:
	.size	nm_shut_down_mvs_and_pmps_throughout_this_system, .-nm_shut_down_mvs_and_pmps_throughout_this_system
	.section	.text.FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate
	.type	FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate, %function
FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L93
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI8:
	ldr	r2, .L93+4
	mov	r6, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L93+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L93+12
	bl	nm_ListGetFirst
	b	.L91
.L87:
	ldrb	r3, [r4, #91]	@ zero_extendqisi2
	cmp	r3, r6
	bne	.L85
	ldrb	r3, [r4, #90]	@ zero_extendqisi2
	cmp	r3, r5
	bne	.L85
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L89
	ldr	r5, .L93+16
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L93+4
	ldr	r3, .L93+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [r4, #44]
	ldr	r3, .L93+24
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #36]	@ zero_extendqisi2
	orr	r2, r2, #32
	strb	r2, [r3, #36]
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	b	.L89
.L85:
	ldr	r0, .L93+12
	mov	r1, r4
	bl	nm_ListGetNext
.L91:
	cmp	r0, #0
	mov	r4, r0
	bne	.L87
	b	.L92
.L89:
	ldr	r3, .L93
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L92:
	ldr	r0, .L93+28
	bl	Alert_Message
	b	.L89
.L94:
	.align	2
.L93:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	2581
	.word	foal_irri+36
	.word	station_preserves_recursive_MUTEX
	.word	2599
	.word	station_preserves
	.word	.LC7
.LFE21:
	.size	FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate, .-FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate
	.section	.text.FOAL_extract_clear_mlb_gids,"ax",%progbits
	.align	2
	.global	FOAL_extract_clear_mlb_gids
	.type	FOAL_extract_clear_mlb_gids, %function
FOAL_extract_clear_mlb_gids:
.LFB22:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L106
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, sl, lr}
.LCFI9:
	ldr	r2, .L106+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L106+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r4, #0]
	ldrb	r7, [r3], #1	@ zero_extendqisi2
	str	r3, [r4, #0]
	sub	r3, r7, #1
	cmp	r3, #3
	movhi	sl, #0
	bls	.L105
	b	.L97
.L102:
.LBB25:
.LBB26:
	ldr	r1, [r4, #0]
	mov	r2, #2
	add	r0, sp, #2
	bl	memcpy
	ldr	r3, [r4, #0]
	ldrh	r0, [sp, #2]
	add	r3, r3, #2
	str	r3, [r4, #0]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	sl, r0, #0
	beq	.L97
	ldrb	r3, [sl, #500]	@ zero_extendqisi2
	cmp	r3, #1
	beq	.L98
	ldrb	r2, [sl, #501]	@ zero_extendqisi2
	cmp	r2, #1
	beq	.L98
	ldrb	r2, [sl, #502]	@ zero_extendqisi2
	cmp	r2, #1
	bne	.L99
.L98:
	cmp	r3, #1
	str	r8, [sl, #456]
	bne	.L99
	ldrh	r0, [sp, #2]
	bl	Alert_mainline_break_cleared
	strb	r6, [sl, #500]
.L99:
	ldrb	r3, [sl, #501]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L100
	ldrh	r0, [sp, #2]
	bl	Alert_mainline_break_cleared
	strb	r6, [sl, #501]
.L100:
	ldrb	r3, [sl, #502]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L101
	ldrh	r0, [sp, #2]
	bl	Alert_mainline_break_cleared
	strb	r6, [sl, #502]
.L101:
.LBE26:
	add	r5, r5, #1
	b	.L96
.L105:
.LBE25:
	mov	r5, #0
.LBB28:
.LBB27:
	mov	r8, #150
	mov	r6, r5
.L96:
.LBE27:
	cmp	r5, r7
	blt	.L102
	mov	sl, #1
.L97:
.LBE28:
	ldr	r3, .L106
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, sl
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, pc}
.L107:
	.align	2
.L106:
	.word	system_preserves_recursive_MUTEX
	.word	.LC1
	.word	2654
.LFE22:
	.size	FOAL_extract_clear_mlb_gids, .-FOAL_extract_clear_mlb_gids
	.section	.text.FOAL_IRRI_load_mlb_info_into_outgoing_token,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_load_mlb_info_into_outgoing_token
	.type	FOAL_IRRI_load_mlb_info_into_outgoing_token, %function
FOAL_IRRI_load_mlb_info_into_outgoing_token:
.LFB23:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI10:
	ldr	r1, .L114
	mov	r4, r0
	ldr	r2, .L114+4
	ldr	r0, .L114+8
	bl	mem_malloc_debug
	ldr	r3, .L114+12
	mov	r1, #400
	ldr	r2, .L114
	ldr	r8, .L114+16
	mov	r7, #0
	mov	sl, r7
	mov	r6, r7
	mov	fp, r8
	str	r0, [r4, #0]
	add	r5, r0, #1
	ldr	r0, [r3, #0]
	ldr	r3, .L114+20
	bl	xQueueTakeMutexRecursive_debug
.L110:
	ldr	r3, [r8, #16]
	cmp	r3, #0
	beq	.L109
	ldr	r3, .L114+24
	mul	r1, r3, r7
	add	r9, r1, #516
	add	r9, r9, fp
	mov	r0, r9
	str	r1, [sp, #0]
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	ldr	r1, [sp, #0]
	cmp	r0, #0
	beq	.L109
	add	r1, r1, #16
	mov	r0, r5
	add	r1, fp, r1
	mov	r2, #4
	bl	memcpy
	add	r0, r5, #4
	mov	r1, r9
	mov	r2, #16
	bl	memcpy
	add	sl, sl, #1
	add	r5, r5, #20
	add	r6, r6, #20
	and	sl, sl, #255
.L109:
	add	r7, r7, #1
	add	r8, r8, #14208
	cmp	r7, #4
	add	r8, r8, #16
	bne	.L110
	cmp	r6, #0
	ldrne	r3, [r4, #0]
	addne	r6, r6, #1
	strneb	sl, [r3, #0]
	bne	.L112
	ldr	r0, [r4, #0]
	ldr	r1, .L114
	ldr	r2, .L114+28
	bl	mem_free_debug
	str	r6, [r4, #0]
.L112:
	ldr	r3, .L114+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L115:
	.align	2
.L114:
	.word	.LC1
	.word	2786
	.word	257
	.word	system_preserves_recursive_MUTEX
	.word	system_preserves
	.word	2797
	.word	14224
	.word	2830
.LFE23:
	.size	FOAL_IRRI_load_mlb_info_into_outgoing_token, .-FOAL_IRRI_load_mlb_info_into_outgoing_token
	.section	.text.FOAL_IRRI_translate_alert_actions_to_flow_checking_group,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_translate_alert_actions_to_flow_checking_group
	.type	FOAL_IRRI_translate_alert_actions_to_flow_checking_group, %function
FOAL_IRRI_translate_alert_actions_to_flow_checking_group:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	tst	r3, #192
	ldrb	r3, [r0, #2]	@ zero_extendqisi2
	moveq	r1, #0
	movne	r1, #1
	ands	r3, r3, #3
	movne	r3, #1
	and	r0, r1, #255
	and	r2, r3, #255
	tst	r2, r0
	movne	r0, #3
	bxne	lr
	eor	r3, r3, #1
	tst	r0, r3
	eoreq	r0, r1, #1
	andeq	r0, r2, r0
	movne	r0, #2
	bx	lr
.LFE24:
	.size	FOAL_IRRI_translate_alert_actions_to_flow_checking_group, .-FOAL_IRRI_translate_alert_actions_to_flow_checking_group
	.section	.text.FOAL_IRRI_we_test_flow_when_ON_for_this_reason,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_we_test_flow_when_ON_for_this_reason
	.type	FOAL_IRRI_we_test_flow_when_ON_for_this_reason, %function
FOAL_IRRI_we_test_flow_when_ON_for_this_reason:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #7
	moveq	r0, #0
	bxeq	lr
	cmp	r0, #5
	beq	.L123
	subs	r0, r0, #4
	movne	r0, #1
	bx	lr
.L123:
	mov	r0, #0
	bx	lr
.LFE25:
	.size	FOAL_IRRI_we_test_flow_when_ON_for_this_reason, .-FOAL_IRRI_we_test_flow_when_ON_for_this_reason
	.section	.text.FOAL_IRRI_return_stop_date_according_to_stop_time,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_return_stop_date_according_to_stop_time
	.type	FOAL_IRRI_return_stop_date_according_to_stop_time, %function
FOAL_IRRI_return_stop_date_according_to_stop_time:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L128
	cmp	r1, r3
	beq	.L127
	ldrb	r2, [r0, #1]	@ zero_extendqisi2
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r0, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r0, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	cmp	r1, r3
	ldrb	r3, [r0, #4]	@ zero_extendqisi2
	ldrb	r0, [r0, #5]	@ zero_extendqisi2
	orr	r0, r3, r0, asl #8
	addcc	r0, r0, #1
	bx	lr
.L127:
	ldr	r0, .L128+4
	bx	lr
.L129:
	.align	2
.L128:
	.word	86400
	.word	65535
.LFE28:
	.size	FOAL_IRRI_return_stop_date_according_to_stop_time, .-FOAL_IRRI_return_stop_date_according_to_stop_time
	.section	.text.FOAL_IRRI_initiate_or_cancel_a_master_valve_override,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_initiate_or_cancel_a_master_valve_override
	.type	FOAL_IRRI_initiate_or_cancel_a_master_valve_override, %function
FOAL_IRRI_initiate_or_cancel_a_master_valve_override:
.LFB35:
	@ args = 0, pretend = 0, frame = 68
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI11:
	and	r8, r3, #255
	ldr	r3, .L146
	sub	sp, sp, #68
.LCFI12:
	mov	r7, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L146+4
	mov	r6, r2
	ldr	r2, .L146+8
	bl	xQueueTakeMutexRecursive_debug
	add	r0, sp, #48
	bl	EPSON_obtain_latest_complete_time_and_date
	cmp	r7, #0
	beq	.L131
	mov	r0, r7
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	r4, r0, #0
	beq	.L132
	ldr	sl, .L146+12
	ldr	r3, .L146+16
	mov	r1, #400
	ldr	r2, .L146+8
	ldr	r0, [sl, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r7
	bl	SYSTEM_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	mov	r0, sp
	bl	strlcpy
	ldr	r0, [sl, #0]
	bl	xQueueGiveMutexRecursive
	add	r0, r4, #516
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	cmp	r0, #0
	beq	.L133
	cmp	r5, #2
	movne	r0, sp
	movne	r1, #1
	bne	.L145
.L133:
	ldrb	r3, [r4, #466]	@ zero_extendqisi2
	rsbs	r7, r5, #1
	movcc	r7, #0
	ands	r2, r3, #16
	moveq	r1, #0
	andne	r1, r7, #1
	cmp	r1, #0
	beq	.L135
	mov	r0, sp
	mov	r1, #4
.L145:
	bl	Alert_MVOR_skipped
	b	.L134
.L135:
	cmp	r7, #0
	beq	.L136
	tst	r3, #8
	moveq	r2, #150
	streq	r2, [r4, #456]
	b	.L138
.L136:
	cmp	r5, #1
	bne	.L138
	cmp	r2, #0
	moveq	r3, #150
	streq	r3, [r4, #456]
	b	.L142
.L138:
	cmp	r5, #2
	bne	.L140
	tst	r3, #24
	movne	r2, #150
	strne	r2, [r4, #456]
	b	.L141
.L140:
	cmp	r5, #1
	bhi	.L141
.L142:
	mov	r1, r5
	mov	r2, r6
	mov	r0, sp
	mov	r3, r8
	bl	Alert_MVOR_started
	ldrb	r3, [r4, #466]	@ zero_extendqisi2
	and	r5, r5, #1
	bic	r3, r3, #8
	orr	r7, r3, r7, asl #3
	bic	r7, r7, #16
	orr	r5, r7, r5, asl #4
	strb	r5, [r4, #466]
	add	r0, sp, #48
	mov	r1, r6
	bl	TDUTILS_add_seconds_to_passed_DT_ptr
	ldrh	r2, [sp, #52]
	ldr	r3, .L146+20
	str	r6, [r4, #444]
	str	r2, [r4, r3]
	ldr	r2, [sp, #48]
	add	r3, r3, #4
	str	r2, [r4, r3]
	b	.L134
.L141:
	tst	r3, #24
	beq	.L143
	mov	r0, sp
	mov	r1, #2
	mov	r2, r6
	mov	r3, r8
	bl	Alert_MVOR_started
.L143:
	ldrb	r3, [r4, #466]	@ zero_extendqisi2
	mov	r0, #1
	and	r3, r3, #231
	strb	r3, [r4, #466]
	ldr	r2, .L146+24
	mov	r1, r0
	bl	DMYToDate
	ldr	r3, .L146+20
	ldr	r2, .L146+28
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	str	r0, [r4, r3]
	mov	r3, #0
	str	r3, [r4, r2]
	str	r3, [r4, #444]
	b	.L134
.L132:
	ldr	r0, .L146+8
	ldr	r1, .L146+32
	bl	Alert_func_call_with_null_ptr_with_filename
	b	.L134
.L131:
	ldr	r0, .L146+36
	mov	r1, r8
	bl	Alert_Message_va
.L134:
	ldr	r3, .L146
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L147:
	.align	2
.L146:
	.word	system_preserves_recursive_MUTEX
	.word	5962
	.word	.LC1
	.word	list_system_recursive_MUTEX
	.word	5978
	.word	14116
	.word	2011
	.word	14120
	.word	6077
	.word	.LC8
.LFE35:
	.size	FOAL_IRRI_initiate_or_cancel_a_master_valve_override, .-FOAL_IRRI_initiate_or_cancel_a_master_valve_override
	.section	.text.FOAL_IRRI_buildup_action_needed_records_for_token,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_buildup_action_needed_records_for_token
	.type	FOAL_IRRI_buildup_action_needed_records_for_token, %function
FOAL_IRRI_buildup_action_needed_records_for_token:
.LFB37:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, lr}
.LCFI13:
	ldr	r8, .L159
	mov	r3, #0
	str	r3, [r0, #0]
	ldr	r3, .L159+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L159+8
	ldr	r3, .L159+12
	bl	xQueueTakeMutexRecursive_debug
	ldrh	r6, [r8, #64]
	cmp	r6, #0
	strh	r6, [sp, #14]	@ movhi
	beq	.L149
.LBB29:
	mov	r5, #12
	mul	r5, r6, r5
	ldr	r1, .L159+8
	add	r6, r5, #2
	ldr	r2, .L159+16
	mov	r0, r6
	bl	mem_malloc_debug
	add	r1, sp, #14
	mov	r2, #2
	mov	r7, r0
	str	r0, [r4, #0]
	bl	memcpy
	add	r7, r7, #2
	add	r0, r8, #56
	ldr	r1, .L159+8
	ldr	r2, .L159+20
	b	.L158
.L153:
.LBB30:
	mov	r1, #0
	mov	r2, #12
	mov	r0, sp
	bl	memset
	ldrb	r2, [r4, #91]	@ zero_extendqisi2
	ldrh	r3, [r4, #36]
	strb	r2, [sp, #0]
	ldrb	r2, [r4, #90]	@ zero_extendqisi2
	strb	r3, [sp, #3]
	strb	r2, [sp, #1]
	ldrh	r2, [r4, #72]
	sub	r3, r3, #1
	mov	r2, r2, lsr #6
	mov	r3, r3, asl #16
	and	r2, r2, #15
	cmp	r3, #65536
	strb	r2, [sp, #2]
	ldr	r2, [r4, #52]
	ldrls	r3, [r4, #48]
	ldrhi	r3, [r4, #60]
	str	r2, [sp, #8]
	mov	r0, r7
	mov	r1, sp
	mov	r2, #12
	str	r3, [sp, #4]
	bl	memcpy
	ldrh	r3, [sp, #14]
	ldr	r0, .L159+24
	ldr	r1, .L159+8
	ldr	r2, .L159+28
	sub	r3, r3, #1
	add	r7, r7, #12
	sub	r5, r5, #12
	strh	r3, [sp, #14]	@ movhi
.L158:
	bl	nm_ListRemoveHead_debug
.LBE30:
	cmp	r0, #0
.LBB31:
	mov	r4, r0
.LBE31:
	bne	.L153
	cmp	r5, #0
	bne	.L154
	ldrh	r3, [sp, #14]
	cmp	r3, #0
	beq	.L149
.L154:
	ldr	r0, .L159+8
	bl	RemovePathFromFileName
	ldr	r2, .L159+32
	mov	r1, r0
	ldr	r0, .L159+36
	bl	Alert_Message_va
.L149:
.LBE29:
	ldr	r3, .L159+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L160:
	.align	2
.L159:
	.word	foal_irri
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	6268
	.word	6289
	.word	6303
	.word	foal_irri+56
	.word	6351
	.word	6358
	.word	.LC9
.LFE37:
	.size	FOAL_IRRI_buildup_action_needed_records_for_token, .-FOAL_IRRI_buildup_action_needed_records_for_token
	.section	.text.FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list
	.type	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list, %function
FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	r3, r0, #1
	cmp	r3, #1
	bhi	.L162
	cmp	r1, #2
	movls	r0, #0
	movhi	r0, #1
	bx	lr
.L162:
	cmp	r1, r0
	movls	r0, #0
	movhi	r0, #1
	bx	lr
.LFE40:
	.size	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list, .-FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list
	.section	.text.FOAL_IRRI_there_are_other_flow_check_groups_already_ON,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_there_are_other_flow_check_groups_already_ON
	.type	FOAL_IRRI_there_are_other_flow_check_groups_already_ON, %function
FOAL_IRRI_there_are_other_flow_check_groups_already_ON:
.LFB41:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r2, [r0, #2]	@ zero_extendqisi2
	mov	r3, #0
	mov	r2, r2, lsr #2
	and	r2, r2, #3
.L167:
	cmp	r3, r2
	beq	.L165
	ldr	r0, [r1, r3, asl #2]
	cmp	r0, #0
	bne	.L168
.L165:
	add	r3, r3, #1
	cmp	r3, #4
	bne	.L167
	mov	r0, #0
	bx	lr
.L168:
	mov	r0, #1
	bx	lr
.LFE41:
	.size	FOAL_IRRI_there_are_other_flow_check_groups_already_ON, .-FOAL_IRRI_there_are_other_flow_check_groups_already_ON
	.section	.text.FOAL_IRRI_there_is_more_than_one_flow_group_ON,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_there_is_more_than_one_flow_group_ON
	.type	FOAL_IRRI_there_is_more_than_one_flow_group_ON, %function
FOAL_IRRI_there_is_more_than_one_flow_group_ON:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	mov	r2, r3
.L172:
	ldr	r1, [r0, r3]
	add	r3, r3, #4
	cmp	r1, #0
	addne	r2, r2, #1
	cmp	r3, #16
	bne	.L172
	cmp	r2, #1
	movls	r0, #0
	movhi	r0, #1
	bx	lr
.LFE42:
	.size	FOAL_IRRI_there_is_more_than_one_flow_group_ON, .-FOAL_IRRI_there_is_more_than_one_flow_group_ON
	.section	.text.nm_FOAL_add_to_action_needed_list,"ax",%progbits
	.align	2
	.global	nm_FOAL_add_to_action_needed_list
	.type	nm_FOAL_add_to_action_needed_list, %function
nm_FOAL_add_to_action_needed_list:
.LFB43:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI14:
	mov	r4, r0
	mov	r5, r1
	ldr	r0, .L177
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	bne	.L175
	mov	r1, r4
	ldr	r0, .L177
	bl	nm_ListInsertTail
	b	.L176
.L175:
	ldrh	r1, [r4, #36]
	cmp	r1, #5
	beq	.L176
	cmp	r1, r5
	beq	.L176
	ldr	r3, .L177+4
	ldr	r0, .L177+8
	mov	r2, r5
	ldr	r3, [r3, #64]
	bl	Alert_Message_va
.L176:
	strh	r5, [r4, #36]	@ movhi
	ldmfd	sp!, {r4, r5, pc}
.L178:
	.align	2
.L177:
	.word	foal_irri+56
	.word	foal_irri
	.word	.LC10
.LFE43:
	.size	nm_FOAL_add_to_action_needed_list, .-nm_FOAL_add_to_action_needed_list
	.section	.text.nm_nm_FOAL_if_station_is_ON_turn_it_OFF,"ax",%progbits
	.align	2
	.global	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	.type	nm_nm_FOAL_if_station_is_ON_turn_it_OFF, %function
nm_nm_FOAL_if_station_is_ON_turn_it_OFF:
.LFB11:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L218
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI15:
	mov	r4, r1
	sub	sp, sp, #20
.LCFI16:
	mov	r1, #400
	mov	r7, r0
	mov	r5, r2
	ldr	r0, [r3, #0]
	ldr	r2, .L218+4
	ldr	r3, .L218+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L218+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L218+4
	ldr	r3, .L218+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L218+20
	mov	r1, r4
	bl	nm_OnList
	subs	r6, r0, #0
	bne	.L180
	ldr	r0, .L218+24
	ldr	r1, .L218+28
	ldr	r2, .L218+4
	ldr	r3, .L218+32
	bl	Alert_item_not_on_list_with_filename
	b	.L181
.L180:
	mov	r0, r7
	mov	r1, r4
	bl	nm_ListGetNext
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	tst	r3, #64
	mov	r6, r0
	beq	.L182
	ldr	r3, .L218+36
	ldr	r3, [r3, #76]
	cmp	r3, #0
	bne	.L183
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	ands	r2, r2, #64
	bne	.L184
.LBB32:
	cmp	r5, #103
	strh	r2, [sp, #16]	@ movhi
	ldreqb	r2, [sp, #17]	@ zero_extendqisi2
	orreq	r2, r2, #16
	streqb	r2, [sp, #17]
	ldrb	r2, [r4, #75]	@ zero_extendqisi2
	and	r2, r2, #10
	cmp	r2, #8
	ldrb	r2, [sp, #16]	@ zero_extendqisi2
	bic	r2, r2, #3
	orr	r2, r2, #4
	strb	r2, [sp, #16]
	bne	.L186
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	ldrb	r1, [r4, #91]	@ zero_extendqisi2
	tst	r3, #4
	ldrb	r3, [sp, #17]	@ zero_extendqisi2
	ldreq	r0, .L218+40
	orreq	r3, r3, #4
	orrne	r3, r3, #8
	movne	r0, #256
	ldrb	r2, [r4, #90]	@ zero_extendqisi2
	streqb	r3, [sp, #17]
	strneb	r3, [sp, #17]
	bl	Alert_flow_not_checked_with_reason_idx
	b	.L188
.L186:
	ldrb	r2, [r3, #466]	@ zero_extendqisi2
	tst	r2, #1
	ldreqb	r3, [sp, #16]	@ zero_extendqisi2
	orreq	r3, r3, #128
	beq	.L214
	mov	r2, #468
	ldrh	r3, [r3, r2]
	tst	r3, #8064
	ldrneb	r3, [sp, #17]	@ zero_extendqisi2
	orrne	r3, r3, #32
	strneb	r3, [sp, #17]
	bne	.L188
	ldrb	r3, [sp, #16]	@ zero_extendqisi2
	orr	r3, r3, #64
.L214:
	strb	r3, [sp, #16]
.L188:
	ldrh	r3, [sp, #16]
	ldr	r0, [r4, #40]
	strh	r3, [sp, #0]	@ movhi
	flds	s15, [r0, #296]
	mov	r1, #0
	mov	r2, r1
	mov	r3, #1
	ftouizs	s15, s15
	fsts	s15, [sp, #4]	@ int
	bl	FOAL_FLOW_zeroproblemcount_stampreportrip_makeflowrecordinglines
.L184:
.LBE32:
	ldr	r3, .L218+36
	mov	r2, #1
	str	r2, [r3, #76]
.L183:
	ldr	r3, [r4, #40]
	ldr	r7, .L218+4
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	mov	r1, #0
	bic	r2, r2, #64
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	add	r0, r4, #91
	ldrb	r2, [r3, #464]	@ zero_extendqisi2
	bic	r2, r2, #128
	strb	r2, [r3, #464]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #1
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #2
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #4
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #8
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #16
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #32
	strb	r2, [r3, #465]
	ldr	r3, .L218+44
	mov	r2, #11
	str	r3, [sp, #4]
	ldr	r3, .L218+48
	str	r1, [sp, #0]
	str	r3, [sp, #12]
	mov	r3, r1
	str	r7, [sp, #8]
	bl	RC_uns8_with_filename
	cmp	r0, #0
	beq	.L191
	ldrb	r2, [r4, #91]	@ zero_extendqisi2
	ldr	r3, .L218+36
	add	r2, r2, #20
	ldr	r1, [r3, r2, asl #2]
	cmp	r1, #0
	subne	r1, r1, #1
	strne	r1, [r3, r2, asl #2]
	bne	.L191
	mov	r0, r7
	bl	RemovePathFromFileName
	ldr	r2, .L218+52
	mov	r1, r0
	ldr	r0, .L218+56
	bl	Alert_Message_va
.L191:
	ldr	r0, .L218+60
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	beq	.L193
	ldr	r0, .L218+60
	mov	r1, r4
	ldr	r2, .L218+4
	ldr	r3, .L218+64
	bl	nm_ListRemove_debug
	cmp	r0, #0
	beq	.L194
	ldr	r0, .L218+4
	bl	RemovePathFromFileName
	ldr	r2, .L218+68
	mov	r1, r0
	ldr	r0, .L218+72
	b	.L215
.L193:
	ldr	r0, .L218+4
	bl	RemovePathFromFileName
	ldr	r2, .L218+76
	mov	r1, r0
	ldr	r0, .L218+80
.L215:
	bl	Alert_Message_va
.L194:
	mov	r0, r4
	mov	r1, r5
	bl	nm_FOAL_add_to_action_needed_list
	ldr	r3, [r4, #40]
	ldr	r2, [r3, #92]
	cmp	r2, #0
	subne	r2, r2, #1
	strne	r2, [r3, #92]
	bne	.L196
	ldr	r0, .L218+84
	bl	Alert_Message
.L196:
	ldr	r3, [r4, #40]
	ldrh	r2, [r4, #80]
	ldr	r1, [r3, #100]
	cmp	r1, r2
	rsbcs	r2, r2, r1
	bcs	.L216
	ldr	r0, .L218+88
	bl	Alert_Message
	ldr	r3, [r4, #40]
	mov	r2, #0
.L216:
	str	r2, [r3, #100]
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #384
	bne	.L199
	ldr	r3, [r4, #40]
	ldr	r2, [r3, #168]
	cmp	r2, #0
	subne	r2, r2, #1
	strne	r2, [r3, #168]
	bne	.L199
	ldr	r0, .L218+92
	bl	Alert_Message
.L199:
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	ldrb	r2, [r4, #74]	@ zero_extendqisi2
	bic	r3, r3, #4
	strb	r3, [r4, #73]
	mov	r2, r2, lsr #2
	ldr	r3, [r4, #40]
	and	r2, r2, #3
	add	r2, r2, #48
	ldr	r1, [r3, r2, asl #2]
	cmp	r1, #0
	subne	r1, r1, #1
	strne	r1, [r3, r2, asl #2]
	bne	.L202
	ldr	r0, .L218+96
	bl	Alert_Message
.L202:
	ldr	r3, [r4, #40]
	ldrh	r2, [r4, #82]
	ldr	r1, [r3, #208]
	cmp	r1, r2
	strcc	r2, [r3, #208]
	ldrb	r1, [r4, #73]	@ zero_extendqisi2
	ldrh	r2, [r4, #72]
	tst	r1, #8
	beq	.L204
	ldr	r1, [r3, #92]
	cmp	r1, #0
	bne	.L204
	ldrh	r3, [r4, #84]
	cmp	r3, #0
	beq	.L206
	ldr	r0, .L218+100
	bl	Alert_Message
	b	.L206
.L204:
	and	r2, r2, #960
	cmp	r2, #448
	cmpne	r2, #384
	beq	.L206
	ldrh	r2, [r4, #84]
	ldr	r1, [r3, #212]
	cmp	r1, r2
	bcs	.L207
	b	.L217
.L206:
	ldr	r3, [r4, #40]
	mov	r2, #0
.L217:
	str	r2, [r3, #212]
.L207:
	cmp	r5, #18
	beq	.L208
	ldr	r3, [r4, #48]
	cmp	r3, #0
	ldrgt	r2, [r4, #52]
	addgt	r3, r2, r3
	strgt	r3, [r4, #52]
.L208:
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	ldrb	r2, [r4, #90]	@ zero_extendqisi2
	bic	r3, r3, #64
	strb	r3, [r4, #74]
	mov	r3, #0
	str	r3, [r4, #48]
	ldr	r3, [r4, #40]
	cmp	r5, #13
	str	r2, [r3, #436]
	ldrh	r2, [r4, #72]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	str	r2, [r3, #440]
	ldreqb	r3, [r4, #75]	@ zero_extendqisi2
	orreq	r3, r3, #4
	streqb	r3, [r4, #75]
	cmp	r5, #5
	cmpne	r5, #103
	ldreq	r3, [r4, #64]
	movne	r5, #0
	moveq	r5, #1
	streq	r3, [r4, #60]
	ldrh	r3, [r4, #72]
	strne	r5, [r4, #60]
	and	r3, r3, #960
	cmp	r3, #448
	bne	.L212
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	ldrb	r1, [r4, #90]	@ zero_extendqisi2
	bl	Alert_mobile_station_off
	mov	r1, #0
	ldr	r0, .L218+104
	mov	r2, #512
	mov	r3, r1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
.L212:
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #320
	beq	.L181
	mov	r0, #0
	mov	r1, r4
	bl	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
	b	.L181
.L182:
	ldr	r0, .L218+60
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	beq	.L181
	ldr	r0, .L218+4
	bl	RemovePathFromFileName
	ldr	r2, .L218+108
	mov	r1, r0
	ldr	r0, .L218+112
	bl	Alert_Message_va
	ldr	r0, .L218+60
	mov	r1, r4
	ldr	r2, .L218+4
	ldr	r3, .L218+116
	bl	nm_ListRemove_debug
	cmp	r0, #0
	beq	.L181
	ldr	r0, .L218+4
	bl	RemovePathFromFileName
	ldr	r2, .L218+120
	mov	r1, r0
	ldr	r0, .L218+124
	bl	Alert_Message_va
.L181:
	ldr	r3, .L218+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L218
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L219:
	.align	2
.L218:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	1177
	.word	system_preserves_recursive_MUTEX
	.word	1179
	.word	foal_irri+16
	.word	.LC11
	.word	.LC12
	.word	1187
	.word	foal_irri
	.word	257
	.word	.LC13
	.word	1338
	.word	1342
	.word	.LC14
	.word	foal_irri+36
	.word	1357
	.word	1359
	.word	.LC15
	.word	1365
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	413
	.word	1647
	.word	.LC22
	.word	1649
	.word	1651
	.word	.LC23
.LFE11:
	.size	nm_nm_FOAL_if_station_is_ON_turn_it_OFF, .-nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	.section	.text.FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down
	.type	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down, %function
FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L230
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI17:
	ldr	r2, .L230+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #134
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L230+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L230+4
	mov	r3, #136
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	beq	.L221
	ldr	r0, .L230+12
	mov	r1, #0
	bl	nm_ListInit
	ldr	r0, .L230+16
	mov	r1, #12
	bl	nm_ListInit
	ldr	r0, .L230+20
	mov	r1, #24
	bl	nm_ListInit
	mov	r2, #73728
	ldr	r0, .L230+24
	mov	r1, #0
	bl	memset
	ldr	r2, .L230+28
	ldr	ip, .L230+32
	mov	r3, #0
	mov	lr, r2
	mov	r0, r3
.L222:
	mla	r1, ip, r3, lr
	add	r3, r3, #1
	ldrb	r4, [r1, #482]	@ zero_extendqisi2
	cmp	r3, #4
	and	r4, r4, #231
	strb	r4, [r1, #482]
	str	r0, [r2, #460]
	ldrb	r4, [r1, #484]	@ zero_extendqisi2
	and	r4, r4, #159
	strb	r4, [r1, #484]
	add	r1, r2, #14080
	add	r2, r2, #14208
	str	r0, [r1, #60]
	add	r2, r2, #16
	bne	.L222
	b	.L223
.L221:
.LBB33:
	ldr	r0, .L230+36
	str	r4, [r0, #32]
	str	r4, [r0, #52]
	str	r4, [r0, #72]
	str	r4, [r0, #76]
	add	r0, r0, #16
	bl	nm_ListGetFirst
	mov	r4, r0
	b	.L224
.L226:
.LBB34:
	mov	r1, r4
	mov	r2, #103
	ldr	r0, .L230+12
	bl	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	mov	r1, r4
	mov	r5, r0
	ldr	r0, .L230+20
	bl	nm_OnList
	cmp	r0, #0
	beq	.L225
	ldr	r0, .L230+20
	mov	r1, r4
	ldr	r2, .L230+4
	mov	r3, #239
	bl	nm_ListRemove_debug
	cmp	r0, #0
	beq	.L225
	ldr	r0, .L230+4
	bl	RemovePathFromFileName
	mov	r2, #241
	mov	r1, r0
	ldr	r0, .L230+40
	bl	Alert_Message_va
.L225:
	ldrb	r3, [r4, #76]	@ zero_extendqisi2
	orr	r3, r3, #2
	strb	r3, [r4, #76]
	mov	r4, r5
.L224:
.LBE34:
	cmp	r4, #0
	bne	.L226
.L223:
.LBE33:
	ldr	r0, .L230+44
	mov	r1, #0
	mov	r2, #48
	bl	memset
	ldr	r6, .L230+32
	ldr	r5, .L230+28
	mov	r4, #0
.L227:
	mla	r0, r6, r4, r5
	add	r4, r4, #1
	add	r0, r0, #16
	bl	nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds
	cmp	r4, #4
	bne	.L227
	ldr	r3, .L230+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L230
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L230+36
	mov	r2, #0
	str	r2, [r3, #136]
	ldmfd	sp!, {r4, r5, r6, pc}
.L231:
	.align	2
.L230:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	system_preserves_recursive_MUTEX
	.word	foal_irri+16
	.word	foal_irri+36
	.word	foal_irri+56
	.word	foal_irri+140
	.word	system_preserves
	.word	14224
	.word	foal_irri
	.word	.LC23
	.word	foal_irri+80
.LFE2:
	.size	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down, .-FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down
	.section	.text.init_battery_backed_foal_irri,"ax",%progbits
	.align	2
	.global	init_battery_backed_foal_irri
	.type	init_battery_backed_foal_irri, %function
init_battery_backed_foal_irri:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L238
	stmfd	sp!, {r0, r4, lr}
.LCFI18:
	ldr	r2, .L238+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L238+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #0
	bne	.L233
	ldr	r0, .L238+12
	ldr	r1, .L238+16
	mov	r2, #14
	bl	strncmp
	cmp	r0, #0
	beq	.L234
.L233:
	mov	r1, r4
	ldr	r0, .L238+20
	bl	Alert_battery_backed_var_initialized
	mov	r1, #0
	ldr	r2, .L238+24
	ldr	r0, .L238+12
	bl	memset
	mov	r0, #1
	bl	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down
	ldr	r1, .L238+16
	mov	r2, #16
	ldr	r0, .L238+12
	bl	strlcpy
	b	.L235
.L234:
	bl	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down
.L235:
	ldr	r3, .L238+28
	mov	r2, #0
	str	r3, [sp, #0]
	ldr	r0, .L238+32
	mov	r3, r2
	mov	r1, #4000
	bl	xTimerCreate
	ldr	r3, .L238+12
	cmp	r0, #0
	str	r0, [r3, #128]
	bne	.L236
	ldr	r0, .L238+4
	bl	RemovePathFromFileName
	ldr	r2, .L238+36
	mov	r1, r0
	ldr	r0, .L238+40
	bl	Alert_Message_va
.L236:
	ldr	r3, .L238+44
	mov	r2, #0
	str	r3, [sp, #0]
	ldr	r0, .L238+48
	mov	r3, r2
	mov	r1, #4000
	bl	xTimerCreate
	ldr	r3, .L238+12
	cmp	r0, #0
	str	r0, [r3, #132]
	bne	.L237
	ldr	r0, .L238+4
	bl	RemovePathFromFileName
	ldr	r2, .L238+52
	mov	r1, r0
	ldr	r0, .L238+40
	bl	Alert_Message_va
.L237:
	mov	r1, #0
	mov	r2, #128
	ldr	r0, .L238+56
	bl	memset
	ldr	r3, .L238
	ldr	r0, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L239:
	.align	2
.L238:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	339
	.word	foal_irri
	.word	.LANCHOR0
	.word	.LC24
	.word	74048
	.word	rain_switch_timer_callback
	.word	.LC25
	.word	390
	.word	.LC26
	.word	freeze_switch_timer_callback
	.word	.LC27
	.word	399
	.word	foal_irri+73920
.LFE4:
	.size	init_battery_backed_foal_irri, .-init_battery_backed_foal_irri
	.section	.text.FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation
	.type	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation, %function
FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI19:
	mov	r0, #1
	bl	FOAL_IRRI_restart_irrigation_on_reboot_and_when_chain_goes_down
	ldr	lr, [sp], #4
	b	IRRI_restart_all_of_irrigation
.LFE3:
	.size	FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation, .-FOAL_IRRI_completely_wipe_both_foal_side_and_irri_side_irrigation
	.global	__divsi3
	.section	.text.nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists,"ax",%progbits
	.align	2
	.global	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	.type	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists, %function
nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists:
.LFB14:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, sl, lr}
.LCFI20:
	ldr	r3, .L273
	mov	r4, r1
	str	r3, [sp, #12]	@ float
	ldr	r3, .L273+4
	mov	r1, #400
	mov	r6, r0
	mov	r5, r2
	ldr	r0, [r3, #0]
	ldr	r2, .L273+8
	ldr	r3, .L273+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L273+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L273+8
	mov	r3, #1904
	bl	xQueueTakeMutexRecursive_debug
.LBB38:
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L242
	ldr	r3, .L273+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L273+8
	ldr	r3, .L273+24
	bl	xQueueTakeMutexRecursive_debug
	cmp	r5, #104
	cmpne	r5, #108
	bne	.L243
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #36]	@ zero_extendqisi2
	orr	r2, r2, #16
	b	.L266
.L243:
	cmp	r5, #107
	beq	.L271
.L245:
	cmp	r5, #109
	bne	.L246
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #39]	@ zero_extendqisi2
	orr	r2, r2, #32
	b	.L269
.L246:
	cmp	r5, #114
	beq	.L271
.L247:
	cmp	r5, #113
	bne	.L248
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #36]	@ zero_extendqisi2
	orr	r2, r2, #2
.L266:
	strb	r2, [r3, #36]
	b	.L244
.L248:
	cmp	r5, #101
	bne	.L249
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #37]	@ zero_extendqisi2
	orr	r2, r2, #128
	strb	r2, [r3, #37]
	b	.L244
.L249:
	cmp	r5, #105
	beq	.L272
.L250:
	cmp	r5, #106
	beq	.L272
.L251:
	cmp	r5, #111
	bne	.L252
.L272:
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #39]	@ zero_extendqisi2
	orr	r2, r2, #2
	b	.L269
.L252:
	cmp	r5, #110
	bne	.L253
.L271:
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #39]	@ zero_extendqisi2
	orr	r2, r2, #8
	b	.L269
.L253:
	cmp	r5, #112
	bne	.L254
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #39]	@ zero_extendqisi2
	orr	r2, r2, #64
.L269:
	strb	r2, [r3, #39]
	b	.L244
.L254:
	cmp	r5, #100
	bne	.L255
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #36]	@ zero_extendqisi2
	orr	r2, r2, #8
	b	.L266
.L255:
	cmp	r5, #28
	bne	.L256
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #36]	@ zero_extendqisi2
	orr	r2, r2, #4
	b	.L266
.L256:
	cmp	r5, #30
	bne	.L257
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #38]	@ zero_extendqisi2
	orr	r2, r2, #16
	b	.L267
.L257:
	cmp	r5, #31
	bne	.L258
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #38]	@ zero_extendqisi2
	orr	r2, r2, #32
	b	.L267
.L258:
	cmp	r5, #29
	bne	.L259
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #38]	@ zero_extendqisi2
	orr	r2, r2, #8
	b	.L267
.L259:
	cmp	r5, #33
	bne	.L244
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #38]	@ zero_extendqisi2
	orr	r2, r2, #128
.L267:
	strb	r2, [r3, #38]
.L244:
	cmp	r5, #28
	cmpne	r5, #20
	beq	.L260
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #141]	@ zero_extendqisi2
	bic	r2, r2, #16
	strb	r2, [r3, #141]
.L260:
	ldr	r3, .L273+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L242:
.LBE38:
	mov	r0, r6
	mov	r1, r4
	mov	r2, r5
	bl	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	mov	r1, r5
	mov	r6, r0
	mov	r0, r4
	bl	nm_FOAL_add_to_action_needed_list
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L261
.LBB39:
	ldr	r3, .L273+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L273+8
	ldr	r3, .L273+32
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L273+36
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L273+8
	ldr	r3, .L273+40
	bl	xQueueTakeMutexRecursive_debug
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	ldrb	r1, [r4, #90]	@ zero_extendqisi2
	bl	nm_STATION_get_pointer_to_station
	subs	r5, r0, #0
	beq	.L262
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #0
	beq	.L263
	mov	r0, r5
	bl	nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag
	subs	r7, r0, #0
	beq	.L264
	bl	FLOWSENSE_get_controller_index
	mov	r1, #11
	mov	r8, #0
	mov	r7, r0
	mov	r0, r5
	bl	STATION_get_change_bits_ptr
	str	r7, [sp, #0]
	mov	r1, #1056964608
	mov	r7, #1
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r5
	bl	nm_STATION_set_moisture_balance_percent
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	strh	r8, [r3, #136]	@ movhi
	bl	FLOWSENSE_get_controller_index
	mov	r1, #11
	mov	sl, r0
	mov	r0, r5
	bl	STATION_get_change_bits_ptr
	mov	r1, r8
	mov	r2, r8
	mov	r3, #11
	str	sl, [sp, #0]
	str	r7, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r5
	bl	nm_STATION_set_ignore_moisture_balance_at_next_irrigation
	b	.L263
.L264:
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	ldr	r3, [r3, #52]
	cmp	r3, #0
	beq	.L263
	bl	FLOWSENSE_get_controller_index
	mov	r1, #11
	mov	r8, r0
	mov	r0, r5
	bl	STATION_get_change_bits_ptr
	mov	r3, #1
	str	r3, [sp, #4]
	mov	r1, #1056964608
	mov	r2, r7
	mov	r3, #11
	str	r8, [sp, #0]
	str	r0, [sp, #8]
	mov	r0, r5
	bl	nm_STATION_set_moisture_balance_percent
.L263:
	mov	r0, r5
	ldr	r7, [r4, #44]
	bl	STATION_get_moisture_balance_percent
	flds	s15, [sp, #12]
	flds	s14, [sp, #12]
	ldr	r3, .L273+28
	mov	r1, #6
	add	r7, r3, r7, asl #7
	fmsr	s13, r0
	fmuls	s15, s13, s15
	fmuls	s15, s15, s14
	ftosizs	s15, s15
	fmrs	r2, s15	@ int
	strh	r2, [r7, #72]	@ movhi
	ldr	r2, [r4, #44]
	add	r3, r3, r2, asl #7
	ldrh	r0, [r3, #50]
	add	r5, r3, #48
	ldrh	r3, [r3, #136]
	mla	r0, r1, r0, r3
	bl	__divsi3
	strh	r0, [r5, #2]	@ movhi
	b	.L265
.L262:
	ldr	r0, .L273+8
	mov	r1, #2048
	bl	Alert_station_not_found_with_filename
	ldr	r2, [r4, #44]
	ldr	r3, .L273+28
	add	r3, r3, r2, asl #7
	strh	r5, [r3, #72]	@ movhi
.L265:
	ldr	r3, .L273+36
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r4, #44]
	bl	nm_STATION_HISTORY_close_and_start_a_new_record
	ldr	r3, .L273+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L261:
.LBE39:
	mov	r1, r4
	ldr	r2, .L273+8
	ldr	r3, .L273+44
	ldr	r0, .L273+48
	bl	nm_ListRemove_debug
	ldr	r3, .L273+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L273+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L274:
	.align	2
.L273:
	.word	1120403456
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	1902
	.word	system_preserves_recursive_MUTEX
	.word	station_preserves_recursive_MUTEX
	.word	1745
	.word	station_preserves
	.word	1934
	.word	list_program_data_recursive_MUTEX
	.word	1945
	.word	2069
	.word	foal_irri+16
.LFE14:
	.size	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists, .-nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	.section	.text._nm_nm_nm_add_to_the_irrigation_list,"ax",%progbits
	.align	2
	.type	_nm_nm_nm_add_to_the_irrigation_list, %function
_nm_nm_nm_add_to_the_irrigation_list:
.LFB27:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI21:
	fstmfdd	sp!, {d8}
.LCFI22:
	ldr	r7, .L318+4
	sub	sp, sp, #20
.LCFI23:
	mov	r4, r0
	mov	r0, r1
	mov	r5, r1
	mov	fp, r2
	mov	r6, r3
	str	r7, [sp, #8]	@ float
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	fmsr	s16, r7
	cmp	r0, #0
	str	r0, [r4, #40]
	beq	.L276
	mov	r0, r5
	add	r1, sp, #12
	bl	STATION_PRESERVES_get_index_using_ptr_to_station_struct
	cmp	r0, #0
	beq	.L276
	ldr	r7, [r6, #64]
	cmp	r7, #0
	beq	.L277
	ldr	r3, .L318+8
	ldr	r8, [sp, #12]
	add	r8, r3, r8, asl #7
	add	r8, r8, #136
	ldrh	r7, [r8, #2]
	cmp	r7, #0
	beq	.L277
	mov	r1, #6
	ldr	r0, [r4, #52]
	mul	r3, r1, r7
	cmp	r3, r0
	bcc	.L278
	bl	__udivsi3
	rsb	r7, r0, r7
	strh	r7, [r8, #2]	@ movhi
	mov	r7, #1
	str	r7, [r6, #68]
	b	.L277
.L278:
	mvn	r3, #5
	mla	r7, r3, r7, r0
	mov	r3, #1
	str	r7, [r4, #52]
	mov	r7, #0
	strh	r7, [r8, #2]	@ movhi
	str	r3, [r6, #72]
.L277:
	ldr	r3, [r6, #76]
	cmp	r3, #0
	beq	.L279
.LBB40:
	ldrb	r3, [r4, #76]	@ zero_extendqisi2
	mov	r0, #1
	orr	r3, r3, #4
	strb	r3, [r4, #76]
	add	r1, sp, #16
	bl	WEATHER_TABLES_get_rain_table_entry_for_index
	cmp	r0, #0
	beq	.L280
	ldrh	r3, [sp, #18]
	cmp	r3, #1
	cmpne	r3, #4
	movne	r0, #0
	moveq	r0, #1
	moveq	r0, #1
	beq	.L280
	cmp	r3, #5
	bne	.L280
	ldrh	r0, [sp, #16]
	adds	r0, r0, #0
	movne	r0, #1
.L280:
	ldr	r3, .L318+12
	ldr	r2, [r3, #60]
	cmp	r2, #0
	bne	.L281
	ldr	r3, [r3, #56]
	cmp	r3, #0
	bne	.L281
	cmp	r0, #0
	beq	.L279
.L281:
	mov	r7, #1
	str	r7, [r6, #80]
.L279:
.LBE40:
	ldr	r3, [r6, #40]
	cmp	r3, #0
	beq	.L282
	ldrb	r3, [r4, #76]	@ zero_extendqisi2
	orr	r3, r3, #64
	strb	r3, [r4, #76]
	ldr	r3, .L318+12
	ldr	r3, [r3, #100]
	cmp	r3, #0
	movne	r7, #1
	strne	r7, [r6, #44]
.L282:
	ldr	r3, [r6, #48]
	cmp	r3, #0
	beq	.L283
	ldrb	r3, [r4, #76]	@ zero_extendqisi2
	orr	r3, r3, #128
	strb	r3, [r4, #76]
	ldr	r3, .L318+12
	ldr	r3, [r3, #104]
	cmp	r3, #0
	movne	r7, #1
	strne	r7, [r6, #52]
.L283:
	ldr	r3, [r6, #56]
	ldrb	r2, [r4, #91]	@ zero_extendqisi2
	cmp	r3, #0
	ldrneb	r3, [r4, #77]	@ zero_extendqisi2
	add	r2, r2, #36
	orrne	r3, r3, #1
	strneb	r3, [r4, #77]
	ldr	r3, .L318+16
	ldr	r0, [r4, #40]
	ldr	r3, [r3, r2, asl #2]
	add	r0, r0, #500
	cmp	r3, #0
	movne	r7, #1
	strne	r7, [r6, #84]
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	ldr	r3, [r4, #40]
	cmp	r0, #0
	movne	r7, #1
	strne	r7, [r6, #8]
	ldrb	r3, [r3, #466]	@ zero_extendqisi2
	tst	r3, #16
	ldr	r3, [r6, #16]
	movne	r7, #1
	strne	r7, [r6, #12]
	cmp	r3, #1
	bne	.L288
	ldrb	r3, [r4, #76]	@ zero_extendqisi2
	orr	r3, r3, #8
	strb	r3, [r4, #76]
	bl	NETWORK_CONFIG_get_controller_off
	cmp	r0, #1
	moveq	r7, r0
	streq	r0, [r6, #20]
.L288:
	ldr	r3, [r6, #24]
	cmp	r3, #1
	bne	.L289
	ldrb	r3, [r4, #76]	@ zero_extendqisi2
	ldr	r2, [sp, #12]
	orr	r3, r3, #16
	strb	r3, [r4, #76]
	ldr	r3, .L318+8
	add	r3, r3, r2, asl #7
	ldrb	r3, [r3, #141]	@ zero_extendqisi2
	mov	r3, r3, lsr #2
	and	r3, r3, #1
	cmp	r3, #1
	moveq	r7, r3
	streq	r3, [r6, #28]
.L289:
	ldr	r3, [r6, #32]
	cmp	r3, #1
	bne	.L290
	ldrb	r3, [r4, #76]	@ zero_extendqisi2
	ldr	r2, [sp, #12]
	orr	r3, r3, #32
	strb	r3, [r4, #76]
	ldr	r3, .L318+8
	add	r3, r3, r2, asl #7
	ldrb	r3, [r3, #141]	@ zero_extendqisi2
	mov	r3, r3, lsr #3
	and	r3, r3, #1
	cmp	r3, #1
	moveq	r7, r3
	streq	r3, [r6, #36]
.L290:
	ldr	r3, [r6, #92]
	cmp	r3, #0
	beq	.L291
	mov	r0, r5
	bl	nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag
	subs	r8, r0, #0
	bne	.L291
	mov	r0, r5
	bl	STATION_get_moisture_balance_percent
	flds	s15, .L318
	fmsr	s13, r0
	fcmpes	s13, s15
	fmstat
	movgt	r3, #1
	strgt	r3, [r6, #96]
	movgt	r7, r8
	bgt	.L293
.L291:
	cmp	r7, #0
	bne	.L312
.LBB41:
	ldr	r3, .L318+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L318+52
	ldr	r3, .L318+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L318+28
	mov	sl, #1
	str	r7, [r0, #76]
	add	r0, r0, #16
	bl	nm_ListGetFirst
	ldr	r8, .L318+32
	mov	r6, r0
	b	.L316
.L297:
	ldrh	r2, [r6, #90]
	ldrh	r3, [r4, #90]
	cmp	r2, r3
	bne	.L295
	ldrh	r2, [r6, #72]
	ldrh	r3, [r4, #72]
	eor	r3, r2, r3
	ands	r3, r3, #960
	bne	.L295
	and	r2, r2, #960
	cmp	r2, #128
	bne	.L296
	ldr	r2, [r4, #52]
	ldr	r1, [r6, #52]
	mov	sl, r3
	add	r2, r1, r2
	str	r2, [r6, #52]
	ldrb	r2, [r6, #76]	@ zero_extendqisi2
	mov	r7, #1
	orr	r2, r2, #2
	strb	r2, [r6, #76]
	str	r3, [r4, #8]
	str	r3, [r4, #32]
	str	r3, [r4, #20]
.L295:
	mov	r1, r6
	ldr	r0, .L318+48
	bl	nm_ListGetNext
	mov	r6, r0
.L316:
	cmp	r6, #0
	bne	.L297
	cmp	sl, #0
	beq	.L298
	ldr	r3, [sp, #12]
	str	r3, [r4, #44]
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	bic	r3, r3, #64
	strb	r3, [r4, #74]
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	cmpne	r3, #256
	strne	r6, [r4, #64]
	bne	.L300
	mov	r0, r5
	bl	nm_STATION_get_soak_minutes
	mov	r3, #60
	mul	r3, r0, r3
	str	r3, [r4, #64]
.L300:
	mov	r3, #0
	str	r3, [r4, #60]
	str	r3, [r4, #48]
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L301
.LBB42:
	mov	r0, sp
	bl	EPSON_obtain_latest_time_and_date
	flds	s13, [r4, #52]	@ int
	flds	s15, [sp, #8]
	mov	r1, r5
	ldmia	sp, {r2, r3}
	fuitos	s14, s13
	mov	r6, sp
	fdivs	s14, s14, s15
	fmrs	r0, s14
	bl	nm_BUDGET_calc_time_adjustment
	fmsr	s14, r0
	fmuls	s16, s14, s16
	ftouizs	s16, s16
	fsts	s16, [r4, #52]	@ int
.L301:
.LBE42:
	mov	r0, r5
	bl	PUMP_get_station_uses_pump
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	bic	r3, r3, #8
	and	r0, r0, #1
	orr	r3, r3, r0, asl #3
	strb	r3, [r4, #73]
	mov	r0, r5
	bl	STATION_get_GID_station_group
	strh	r0, [r4, #86]	@ movhi
	mov	r0, r5
	bl	ALERT_ACTIONS_get_station_high_flow_action
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	bic	r3, r3, #192
	and	r0, r0, #3
	orr	r3, r3, r0, asl #6
	strb	r3, [r4, #73]
	mov	r0, r5
	bl	ALERT_ACTIONS_get_station_low_flow_action
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	bic	r3, r3, #3
	and	r0, r0, #3
	orr	r3, r0, r3
	strb	r3, [r4, #74]
	ldrh	r0, [r4, #72]
	ldrb	r3, [r4, #75]	@ zero_extendqisi2
	mov	r0, r0, lsr #6
	bic	r3, r3, #2
	strb	r3, [r4, #75]
	and	r0, r0, #15
	bl	FOAL_IRRI_we_test_flow_when_ON_for_this_reason
	cmp	r0, #0
	beq	.L302
	add	r0, r4, #72
	bl	FOAL_IRRI_translate_alert_actions_to_flow_checking_group
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	bic	r3, r3, #12
	and	r0, r0, #3
	orr	r3, r3, r0, asl #2
	strb	r3, [r4, #74]
	ldrb	r3, [r4, #75]	@ zero_extendqisi2
	orr	r3, r3, #1
	b	.L317
.L302:
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	bic	r3, r3, #12
	strb	r3, [r4, #74]
	ldrb	r3, [r4, #75]	@ zero_extendqisi2
	bic	r3, r3, #1
.L317:
	strb	r3, [r4, #75]
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	bic	r3, r3, #32
	strb	r3, [r4, #73]
	ldrb	r3, [r4, #75]	@ zero_extendqisi2
	bic	r3, r3, #16
	strb	r3, [r4, #75]
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	ands	r3, r3, #12
	beq	.L304
	ldr	r3, [r4, #40]
	ldrb	r3, [r3, #465]	@ zero_extendqisi2
	ands	r3, r3, #128
	ldrneb	r3, [r4, #75]	@ zero_extendqisi2
	andne	r3, r3, #1
.L304:
	ldrb	r2, [r4, #75]	@ zero_extendqisi2
	and	r3, r3, #1
	bic	r2, r2, #8
	orr	r3, r2, r3, asl #3
	strb	r3, [r4, #75]
	mov	r0, r5
	bl	nm_STATION_get_expected_flow_rate_gpm
	strh	r0, [r4, #80]	@ movhi
	mov	r0, r5
	bl	WEATHER_get_station_uses_wind
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	bic	r3, r3, #16
	and	r0, r0, #1
	orr	r3, r3, r0, asl #4
	strb	r3, [r4, #74]
	mov	r0, r5
	bl	WEATHER_get_station_uses_rain
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	bic	r3, r3, #32
	and	r0, r0, #1
	orr	r3, r3, r0, asl #5
	strb	r3, [r4, #74]
	mov	r0, fp
	bl	STATION_GROUPS_if_the_moisture_decoder_is_available_return_decoder_serial_number_for_this_station_group
	str	r0, [r4, #92]
	mov	r0, r5
	bl	PRIORITY_get_station_priority_level
	ldrb	r3, [r4, #72]	@ zero_extendqisi2
	bic	r3, r3, #48
	and	r0, r0, #3
	orr	r3, r3, r0, asl #4
	strb	r3, [r4, #72]
	mov	r0, r5
	bl	LINE_FILL_TIME_get_station_line_fill_seconds
	strh	r0, [r4, #82]	@ movhi
	mov	r0, r5
	bl	DELAY_BETWEEN_VALVES_get_station_slow_closing_valve_seconds
	ldrb	r3, [r4, #75]	@ zero_extendqisi2
	mov	r1, r4
	and	r3, r3, #27
	strb	r3, [r4, #75]
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #448
	ldreqb	r3, [r4, #75]	@ zero_extendqisi2
	orreq	r3, r3, #128
	streqb	r3, [r4, #75]
	strh	r0, [r4, #84]	@ movhi
	ldr	r0, .L318+48
	bl	nm_ListInsertTail
	cmp	r0, #0
	bne	.L298
	ldrb	r3, [r4, #76]	@ zero_extendqisi2
	mov	r1, r4
	orr	r3, r3, #2
	strb	r3, [r4, #76]
	bl	FOAL_IRRI_remove_and_reinsert_into_foal_irri_irrigation_list
	mov	r7, #1
.L298:
	ldr	r3, .L318+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L293
.L276:
.LBE41:
	ldr	r0, .L318+52
	bl	RemovePathFromFileName
	ldr	r2, .L318+36
	mov	r1, r0
	ldr	r0, .L318+40
	bl	Alert_Message_va
.L312:
	mov	r7, #0
.L293:
	mov	r0, r7
	add	sp, sp, #20
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L319:
	.align	2
.L318:
	.word	1056964608
	.word	1114636288
	.word	station_preserves
	.word	weather_preserves
	.word	irri_comm
	.word	list_foal_irri_recursive_MUTEX
	.word	3480
	.word	foal_irri
	.word	system_preserves_recursive_MUTEX
	.word	3750
	.word	.LC28
	.word	3555
	.word	foal_irri+16
	.word	.LC1
	.word	3572
	.word	foal_irri+56
.L296:
.LBB43:
	ldr	r3, .L318+44
	mov	r1, #400
	ldr	r2, .L318+52
	ldr	r0, [r8, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r6
	mov	r2, #102
	ldr	r0, .L318+48
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	mov	r1, r6
	ldr	r2, .L318+52
	ldr	r3, .L318+56
	mov	r9, r0
	ldr	r0, .L318+60
	bl	nm_ListRemove_debug
	ldr	r0, [r8, #0]
	bl	xQueueGiveMutexRecursive
	mov	r6, r9
	b	.L316
.LBE43:
.LFE27:
	.size	_nm_nm_nm_add_to_the_irrigation_list, .-_nm_nm_nm_add_to_the_irrigation_list
	.section	.text.FOAL_IRRI_initiate_manual_watering,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_initiate_manual_watering
	.type	FOAL_IRRI_initiate_manual_watering, %function
FOAL_IRRI_initiate_manual_watering:
.LFB34:
	@ args = 0, pretend = 0, frame = 124
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI24:
	fstmfdd	sp!, {d8}
.LCFI25:
	ldr	r3, .L344
	sub	sp, sp, #132
.LCFI26:
	add	r6, sp, #108
	mov	r5, r0
	mov	r0, r6
	str	r3, [sp, #128]	@ float
	bl	EPSON_obtain_latest_complete_time_and_date
	ldr	r3, .L344+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L344+8
	ldr	r3, .L344+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L344+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L344+8
	ldr	r3, .L344+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L344+24
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L344+8
	ldr	r3, .L344+28
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L344+32
	bl	nm_ListGetFirst
	mov	r8, #0
	mov	r7, r8
	mov	r9, #6
	mov	sl, r6
	mov	r4, r0
	b	.L321
.L332:
	mov	r0, r4
	bl	STATION_get_GID_station_group
	ldr	r3, [r5, #4]
	cmp	r3, #0
	mov	fp, r0
	bne	.L322
	mov	r0, r4
	bl	nm_STATION_get_box_index_0
	ldr	r3, [r5, #8]
	cmp	r0, r3
	bne	.L325
	mov	r0, r4
	bl	nm_STATION_get_station_number_0
	ldr	r3, [r5, #12]
	cmp	r0, r3
	b	.L342
.L322:
	cmp	r3, #1
	bne	.L324
	ldr	r3, [r5, #20]
	cmp	r0, r3
.L342:
	bne	.L325
	b	.L324
.L337:
	bl	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	subs	r6, r0, #0
	bne	.L326
	ldr	r0, .L344+8
	bl	RemovePathFromFileName
	ldr	r2, .L344+36
	mov	r1, r0
	ldr	r0, .L344+40
	bl	Alert_Message_va
	b	.L327
.L326:
	cmp	fp, #0
	moveq	r7, fp
	beq	.L328
	mov	r0, fp
	bl	STATION_GROUP_get_group_with_this_GID
	mov	r7, r0
.L328:
	mov	r0, r4
	bl	nm_STATION_get_box_index_0
	strb	r0, [r6, #91]
	mov	r0, r4
	bl	nm_STATION_get_station_number_0
	ldrh	r3, [r6, #72]
	bic	r3, r3, #704
	orr	r3, r3, #256
	strh	r3, [r6, #72]	@ movhi
	strb	r0, [r6, #90]
	ldr	r3, [r5, #4]
	cmp	r3, #0
	ldreq	r3, [r5, #16]
	beq	.L340
	mov	r0, r4
	flds	s16, [sp, #128]
	bl	nm_STATION_get_distribution_uniformity_100u
	mov	r2, r7
	mov	r1, r4
	str	sl, [sp, #0]
	fmsr	s14, r0	@ int
	mov	r0, #0
	mov	r3, r0
	fuitos	s15, s14
	fsts	s15, [sp, #4]
	bl	nm_WATERSENSE_calculate_run_minutes_for_this_station
	flds	s17, [sp, #128]
	flds	s14, [sp, #128]
	fmsr	s15, r0
	mov	r0, r4
	fmuls	s16, s16, s15
	ftouizs	s15, s16
	fuitos	s16, s15
	fsts	s15, [r6, #52]	@ int
	fdivs	s16, s16, s14
	bl	STATION_get_ET_factor_100u
	ldrh	r1, [sp, #112]
	mov	fp, r0
	mov	r0, r4
	bl	PERCENT_ADJUST_get_station_percentage_100u
	fmrs	r3, s16
	mov	r1, r4
	str	fp, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, #0
	mov	r2, r0
	bl	nm_WATERSENSE_calculate_adjusted_run_time
	fmsr	s14, r0
	fmuls	s17, s17, s14
	ftouizs	s17, s17
	fmrs	r3, s17	@ int
.L340:
	str	r3, [r6, #52]
	mov	r0, r4
	bl	nm_STATION_get_cycle_minutes_10u
	mov	r1, #0
	mov	r2, #100
	mul	r0, r9, r0
	str	r0, [r6, #56]
	add	r0, sp, #8
	bl	memset
	mov	r0, r6
	mov	r1, r4
	mov	r2, #0
	add	r3, sp, #8
	bl	_nm_nm_nm_add_to_the_irrigation_list
	cmp	r0, #1
	moveq	r8, #1
.L327:
	ldr	r3, [r5, #4]
	cmp	r3, #0
	beq	.L331
.L325:
	mov	r1, r4
	ldr	r0, .L344+32
	bl	nm_ListGetNext
	mov	r4, r0
.L321:
	cmp	r4, #0
	bne	.L332
.L331:
	ldr	r3, .L344+24
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L344+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L344+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r8, #1
	bne	.L320
	ldr	r3, [r5, #4]
	cmp	r3, #1
	beq	.L335
	bcc	.L334
	cmp	r3, #2
	bne	.L320
	b	.L343
.L334:
	ldr	r0, [r5, #8]
	ldr	r1, [r5, #12]
	mov	r2, #0
	bl	Alert_manual_water_station_started_idx
	b	.L320
.L335:
	cmp	r7, #0
	beq	.L320
	mov	r0, r7
	bl	nm_GROUP_get_name
	mov	r1, #0
	bl	Alert_manual_water_program_started
	b	.L320
.L343:
	mov	r0, #0
	bl	Alert_manual_water_all_started
	b	.L320
.L324:
	mov	r0, r4
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	bne	.L337
	b	.L325
.L320:
	add	sp, sp, #132
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L345:
	.align	2
.L344:
	.word	1114636288
	.word	list_program_data_recursive_MUTEX
	.word	.LC1
	.word	5786
	.word	station_preserves_recursive_MUTEX
	.word	5788
	.word	list_foal_irri_recursive_MUTEX
	.word	5790
	.word	station_info_list_hdr
	.word	5839
	.word	.LC29
.LFE34:
	.size	FOAL_IRRI_initiate_manual_watering, .-FOAL_IRRI_initiate_manual_watering
	.section	.text.FOAL_IRRI_start_a_walk_thru,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_start_a_walk_thru
	.type	FOAL_IRRI_start_a_walk_thru, %function
FOAL_IRRI_start_a_walk_thru:
.LFB33:
	@ args = 0, pretend = 0, frame = 104
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI27:
	ldr	r2, .L354
	sub	sp, sp, #104
.LCFI28:
	mov	r4, r0
	add	r1, sp, #100
	mov	r0, #568
	ldr	r3, .L354+4
	bl	mem_oabia
	cmp	r0, #0
	beq	.L346
	mov	r0, r4
	ldr	r1, [sp, #100]
	bl	WALK_THRU_load_kick_off_struct
	cmp	r0, #0
	beq	.L346
	ldr	r3, .L354+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L354
	ldr	r3, .L354+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L354+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L354
	ldr	r3, .L354+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L354+24
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L354
	ldr	r3, .L354+28
	bl	xQueueTakeMutexRecursive_debug
	mov	r4, #0
	mov	r5, r4
.L351:
	ldr	r3, [sp, #100]
	add	r8, r5, #1
	ldr	r1, [r3, r8, asl #2]
	cmn	r1, #1
	beq	.L348
	ldr	r0, [r3, #0]
	bl	nm_STATION_get_pointer_to_station
	subs	r7, r0, #0
	bne	.L349
	ldr	r0, .L354
	bl	RemovePathFromFileName
	ldr	r2, .L354+32
	mov	r1, r0
	ldr	r0, .L354+36
	b	.L353
.L349:
	bl	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	subs	r6, r0, #0
	bne	.L350
	ldr	r0, .L354
	bl	RemovePathFromFileName
	ldr	r2, .L354+40
	mov	r1, r0
	ldr	r0, .L354+44
.L353:
	bl	Alert_Message_va
	b	.L348
.L350:
	ldr	r3, [sp, #100]
	mov	r1, #0
	ldr	r2, [r3, #0]
	mov	r0, sp
	strb	r2, [r6, #91]
	ldr	r2, [r3, r8, asl #2]
	strb	r2, [r6, #90]
	ldrh	r2, [r6, #72]
	bic	r2, r2, #640
	orr	r2, r2, #320
	strh	r2, [r6, #72]	@ movhi
	ldr	r3, [r3, #516]
	mov	r2, #100
	str	r3, [r6, #52]
	str	r3, [r6, #56]
	bl	memset
	mov	r0, r6
	mov	r1, r7
	mov	r2, #0
	mov	r3, sp
	bl	_nm_nm_nm_add_to_the_irrigation_list
	cmp	r0, #0
	addne	r4, r4, #1
.L348:
	add	r5, r5, #1
	cmp	r5, #128
	bne	.L351
	ldr	r3, .L354+24
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L354+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L354+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r4, #0
	beq	.L346
	ldr	r0, [sp, #100]
	add	r0, r0, #520
	bl	Alert_walk_thru_started
.L346:
	add	sp, sp, #104
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L355:
	.align	2
.L354:
	.word	.LC1
	.word	5647
	.word	list_program_data_recursive_MUTEX
	.word	5663
	.word	station_preserves_recursive_MUTEX
	.word	5665
	.word	list_foal_irri_recursive_MUTEX
	.word	5667
	.word	5679
	.word	.LC30
	.word	5688
	.word	.LC31
.LFE33:
	.size	FOAL_IRRI_start_a_walk_thru, .-FOAL_IRRI_start_a_walk_thru
	.section	.text.FOAL_IRRI_initiate_a_station_test,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_initiate_a_station_test
	.type	FOAL_IRRI_initiate_a_station_test, %function
FOAL_IRRI_initiate_a_station_test:
.LFB32:
	@ args = 0, pretend = 0, frame = 100
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L361
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI29:
	ldr	r2, .L361+4
	sub	sp, sp, #100
.LCFI30:
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L361+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L361+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L361+4
	ldr	r3, .L361+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L361+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L361+4
	ldr	r3, .L361+24
	bl	xQueueTakeMutexRecursive_debug
	ldmib	r4, {r0, r1}
	bl	nm_STATION_get_pointer_to_station
	subs	r7, r0, #0
	bne	.L357
	ldr	r0, .L361+4
	bl	RemovePathFromFileName
	ldr	r2, .L361+28
	mov	r1, r0
	ldr	r0, .L361+32
	b	.L360
.L357:
	bl	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	subs	r6, r0, #0
	bne	.L359
	ldr	r0, .L361+4
	bl	RemovePathFromFileName
	ldr	r2, .L361+36
	mov	r1, r0
	ldr	r0, .L361+40
.L360:
	bl	Alert_Message_va
	b	.L358
.L359:
	ldr	r3, [r4, #4]
	and	r2, r5, #15
	strb	r3, [r6, #91]
	ldr	r3, [r4, #8]
	mov	r1, #0
	strb	r3, [r6, #90]
	ldrh	r3, [r6, #72]
	mov	r0, sp
	bic	r3, r3, #960
	orr	r3, r3, r2, asl #6
	strh	r3, [r6, #72]	@ movhi
	ldr	r2, [r4, #16]
	ldrb	r3, [r6, #73]	@ zero_extendqisi2
	and	r2, r2, #1
	bic	r3, r3, #4
	orr	r3, r3, r2, asl #2
	strb	r3, [r6, #73]
	ldr	r3, [r4, #12]
	mov	r2, #100
	str	r3, [r6, #52]
	str	r3, [r6, #56]
	bl	memset
	mov	r0, r6
	mov	r1, r7
	mov	r2, #0
	mov	r3, sp
	bl	_nm_nm_nm_add_to_the_irrigation_list
	cmp	r0, #1
	bne	.L358
	cmp	r5, #6
	bne	.L358
	ldmib	r4, {r0, r1}
	mov	r2, #0
	bl	Alert_test_station_started_idx
.L358:
	ldr	r3, .L361+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L361+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L361
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L362:
	.align	2
.L361:
	.word	list_program_data_recursive_MUTEX
	.word	.LC1
	.word	5546
	.word	station_preserves_recursive_MUTEX
	.word	5548
	.word	list_foal_irri_recursive_MUTEX
	.word	5550
	.word	5560
	.word	.LC32
	.word	5569
	.word	.LC29
.LFE32:
	.size	FOAL_IRRI_initiate_a_station_test, .-FOAL_IRRI_initiate_a_station_test
	.section	.text.TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start,"ax",%progbits
	.align	2
	.global	TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start
	.type	TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start, %function
TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start:
.LFB31:
	@ args = 0, pretend = 0, frame = 368
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI31:
	fstmfdd	sp!, {d8, d9, d10, d11}
.LCFI32:
.LBB49:
	flds	s20, .L488
.LBE49:
	ldr	r3, .L488+100
.LBB53:
	fldd	d11, .L488+4
.LBE53:
	ldr	r6, .L488+16
	fcpys	s18, s20
	sub	sp, sp, #384
.LCFI33:
	mov	r1, #400
	ldr	r2, .L488+68
	fmsr	s21, r6
	mov	r5, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L488+20
	str	r6, [sp, #368]	@ float
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L488+96
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L488+68
	ldr	r3, .L488+24
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, #0
	mov	r2, #100
	add	r0, sp, #160
	bl	memset
	mov	r1, #0
	mov	r2, #100
	add	r0, sp, #260
	bl	memset
	bl	STATION_GROUP_clear_budget_start_time_flags
	ldr	r0, .L488+80
	bl	nm_ListGetFirst
	mov	r8, #0
	str	r8, [sp, #36]
	str	r8, [sp, #32]
	str	r8, [sp, #28]
	str	r8, [sp, #56]
	str	r8, [sp, #52]
	str	r8, [sp, #48]
	mov	r9, r8
	mov	sl, r8
	mov	r4, r0
	b	.L364
.L432:
	mov	r0, r4
	add	r1, sp, #376
	bl	STATION_PRESERVES_get_index_using_ptr_to_station_struct
	cmp	r0, #0
	beq	.L365
.LBB54:
	mov	r0, r4
	bl	nm_STATION_get_box_index_0
	mov	fp, r0
	mov	r0, r4
	bl	nm_STATION_get_station_number_0
	ldr	r3, .L488+104
	ldr	r6, [sp, #376]
	add	r6, r3, r6, asl #7
	str	r0, [sp, #24]
	ldrb	r2, [r6, #141]	@ zero_extendqisi2
	tst	r2, #32
	bne	.L366
	orr	r2, r2, #32
	strb	r2, [r6, #141]
	ldr	r1, [sp, #24]
	strb	fp, [r6, #123]
	strb	r1, [r6, #122]
	mov	r0, r4
	bl	STATION_get_GID_station_group
	strh	r0, [r6, #108]	@ movhi
.L366:
	ldr	r7, [sp, #376]
.LBB50:
	cmp	r7, #2112
	bcc	.L367
	ldr	r0, .L488+68
	ldr	r1, .L488+28
	bl	Alert_index_out_of_range_with_filename
	mov	r6, #0
	b	.L368
.L367:
	mov	r0, r4
	bl	STATION_get_no_water_days
	ldr	r2, .L488+104
	mov	r3, r7, asl #7
	add	r3, r2, r3
	cmp	r0, #0
	ldrneb	r2, [r3, #141]	@ zero_extendqisi2
	orrne	r2, r2, #4
	strneb	r2, [r3, #141]
	bne	.L370
	ldrb	r2, [r3, #141]	@ zero_extendqisi2
	add	r6, r3, #140
	tst	r2, #4
	beq	.L370
	ldrh	r2, [r3, #132]
	ldr	r3, [r3, #124]
	add	r1, sp, #512
	strh	r2, [r1, #-148]	@ movhi
	add	r1, sp, #384
	str	r3, [r1, #-24]!
	mov	r0, r5
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #0
	ldrneb	r3, [r6, #1]	@ zero_extendqisi2
	bicne	r3, r3, #4
	strneb	r3, [r6, #1]
.L370:
	mov	r0, r4
	bl	STATION_get_GID_station_group
	subs	r6, r0, #0
	beq	.L371
	bl	STATION_GROUP_get_group_with_this_GID
	subs	r6, r0, #0
	beq	.L371
	bl	SCHEDULE_get_start_time
	ldr	r3, .L488+32
	cmp	r0, r3
	bne	.L372
.L371:
	ldrb	r2, [r5, #1]	@ zero_extendqisi2
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r5, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r5, #3]	@ zero_extendqisi2
	orrs	r2, r3, r2, asl #24
	bne	.L373
	mov	r0, r4
	mov	r1, r7
	mov	r2, r5
	bl	nm_at_starttime_decrement_NOW_days_count
.L373:
.LBE50:
	cmp	r6, #0
	beq	.L368
.L372:
	mov	r0, r6
	bl	SCHEDULE_get_start_time
	ldr	r3, [r5, #0]
	cmp	r0, r3
	bne	.L368
	ldrb	r3, [r5, #19]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L368
	mov	r0, r4
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L374
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #0
	mov	r0, r4
	beq	.L375
	bl	STATION_get_moisture_balance
	fmsr	s17, r0
	mov	r0, r4
	bl	STATION_GROUP_get_soil_storage_capacity_inches_100u
	fmsr	s13, r0	@ int
	mov	r0, r4
	fuitos	s19, s13
	bl	WEATHER_get_station_uses_rain
	fdivs	s19, s19, s20
	cmp	r0, #0
	beq	.L376
	add	r1, sp, #380
	mov	r0, #1
	bl	WEATHER_TABLES_get_rain_table_entry_for_index
	add	r1, sp, #512
	ldrh	r3, [r1, #-132]
	cmp	r3, #100
	fldshi	s17, .L488+12
	fmulshi	s17, s19, s17
.L376:
	ldrh	r1, [r5, #8]
	mov	r0, r4
	bl	STATION_GROUP_get_station_crop_coefficient_100u
	mov	r3, #1
	mov	r2, r5
	mov	r7, r0
	mov	r0, #0
	mov	r1, r0
	str	r0, [sp, #0]
	bl	WATERSENSE_get_et_value_after_substituting_or_limiting
	fmsr	s13, r7	@ int
	fmrs	r2, s17
	fuitos	s15, s13
	fdivs	s15, s15, s18
	mov	r1, r0	@ float
	fmrs	r0, s15
	bl	WATERSENSE_reduce_moisture_balance_by_ETc
	fmsr	s16, r0
	mov	r0, r4
	bl	WEATHER_get_station_uses_rain
	cmp	r0, #0
	beq	.L377
	add	r1, sp, #512
	ldrh	r3, [r1, #-130]
	cmp	r3, #5
	movne	r7, #100
	bne	.L378
	mov	r0, r4
	bl	STATION_GROUP_get_station_usable_rain_percentage_100u
	mov	r7, r0
.L378:
	add	r2, sp, #512
	ldrh	r3, [r2, #-132]
	mov	r0, r4
	fmsr	s13, r3	@ int
	fsitos	s17, s13
	bl	STATION_GROUP_get_soil_storage_capacity_inches_100u
	fmsr	s13, r7	@ int
	fmrs	r3, s16
	fdivs	s17, s17, s18
	fmsr	s15, r0	@ int
	fmrs	r0, s17
	fuitos	s14, s15
	fuitos	s15, s13
	fdivs	s14, s14, s18
	fdivs	s15, s15, s18
	fmrs	r1, s14
	fmrs	r2, s15
	bl	WATERSENSE_increase_moisture_balance_by_rain
	fmsr	s16, r0
.L377:
	mov	r1, #6
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	fdivs	s16, s16, s19
	mov	r3, #1
	str	r3, [sp, #4]
	mov	r2, #0
	mov	r3, #6
	str	fp, [sp, #0]
	fmrs	r1, s16
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_moisture_balance_percent
	fmuls	s16, s16, s18
	ldr	r2, [sp, #376]
	ldr	r3, .L488+104
	add	r3, r3, r2, asl #7
	fmuls	s16, s16, s18
	ftosizs	s16, s16
	fmrs	r2, s16	@ int
	strh	r2, [r3, #72]	@ movhi
	mov	r2, #0
	strh	r2, [r3, #138]	@ movhi
	b	.L374
.L489:
	.align	2
.L488:
	.word	1120403456
	.word	0
	.word	1076101120
	.word	1056964608
	.word	1114636288
	.word	4138
	.word	4141
	.word	3927
	.word	86400
	.word	4374
	.word	foal_irri
	.word	system_preserves_recursive_MUTEX
	.word	4412
	.word	foal_irri+16
	.word	foal_irri+56
	.word	4786
	.word	.LC33
	.word	.LC1
	.word	5034
	.word	list_foal_irri_recursive_MUTEX
	.word	station_info_list_hdr
	.word	.LC34
	.word	.LC35
	.word	4369
	.word	station_preserves_recursive_MUTEX
	.word	list_program_data_recursive_MUTEX
	.word	station_preserves
	.word	0
	.word	1078853632
.L375:
	ldr	r1, [sp, #376]
	bl	nm_RAIN_for_this_station_generate_rain_time_for_raintable_slot1
.L374:
	mov	r0, r4
	ldr	r1, [sp, #376]
	mov	r2, r5
	bl	nm_at_starttime_decrement_NOW_days_count
	mov	r0, r6
	mov	r1, r5
	mov	r2, #1
	bl	SCHEDULE_does_it_irrigate_on_this_date
	cmp	r0, #1
	bne	.L368
	ldr	r3, .L488+76
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L488+68
	ldr	r3, .L488+36
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L488+40
	mov	r3, #0
	str	r3, [r0, #76]
	add	r0, r0, #16
	bl	nm_ListGetFirst
	mov	r1, r0
	b	.L379
.L382:
	ldrb	r3, [r1, #91]	@ zero_extendqisi2
	cmp	r3, fp
	bne	.L380
	ldrb	r3, [r1, #90]	@ zero_extendqisi2
	ldr	r2, [sp, #24]
	cmp	r3, r2
	bne	.L380
	ldrh	r3, [r1, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L380
	ldr	r7, .L488+44
	mov	r8, r1
	mov	r0, fp
	mov	r1, r2
	bl	Alert_programmed_irrigation_still_running_idx
	ldr	r3, .L488+48
	ldr	r0, [r7, #0]
	mov	r1, #400
	ldr	r2, .L488+68
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r8
	mov	r2, #102
	ldr	r0, .L488+52
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	ldr	r0, [r7, #0]
	bl	xQueueGiveMutexRecursive
	b	.L381
.L380:
	ldr	r0, .L488+52
	bl	nm_ListGetNext
	mov	r1, r0
.L379:
	cmp	r1, #0
	bne	.L382
	mov	r8, r1
.L381:
	mov	r0, r4
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L383
	mov	r1, #0
	mov	r2, #100
	add	r0, sp, #60
	bl	memset
	ldr	r0, [sp, #376]
	ldr	sl, .L488+104
	mov	r0, r0, asl #7
	add	r0, r0, #16
	add	r0, sl, r0
	bl	nm_init_station_history_record
	ldr	r7, [sp, #376]
	mov	r0, r4
	add	r7, sl, r7, asl #7
	ldrb	r2, [r7, #39]	@ zero_extendqisi2
	orr	r2, r2, #16
	strb	r2, [r7, #39]
	ldr	r1, [sp, #24]
	ldrh	r3, [r5, #4]
	ldr	r2, [r5, #0]
	and	r1, r1, #255
	strh	r3, [r7, #44]	@ movhi
	strh	r3, [r7, #46]	@ movhi
	strh	r3, [r7, #48]	@ movhi
	and	r3, fp, #255
	str	r2, [r7, #16]
	str	r2, [r7, #20]
	str	r2, [r7, #24]
	strb	r3, [r7, #70]
	strb	r1, [r7, #68]
	str	r3, [sp, #20]
	str	r1, [sp, #40]
	bl	STATION_get_GID_station_group
	strh	r0, [r7, #42]	@ movhi
	mov	r0, r4
	ldr	r7, [sp, #376]
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	mov	r1, #6
	add	sl, sl, r7, asl #7
	strh	r0, [sl, #40]	@ movhi
	mov	r0, r6
	bl	STATION_GROUP_get_change_bits_ptr
	mov	r3, #6
	mov	r2, #1
	str	r3, [sp, #0]
	str	r2, [sp, #8]
	mov	r3, #0
	ldmia	r5, {r1, r2}
	str	r3, [sp, #4]
	str	r0, [sp, #12]
	mov	r0, r6
	bl	nm_SCHEDULE_set_last_ran
	ldr	r2, [sp, #32]
	cmp	r2, #0
	bne	.L384
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	ldr	r3, [sp, #36]
	cmp	r0, #0
	movne	r3, r6
	moveq	r1, #0
	movne	r1, r4
	str	r3, [sp, #36]
	str	r1, [sp, #32]
.L384:
	mov	r0, r4
	ldr	r7, [sp, #376]
	flds	s16, [sp, #368]
	bl	nm_STATION_get_distribution_uniformity_100u
	mov	r1, r4
	mov	r2, r6
	str	r5, [sp, #0]
	ldr	sl, .L488+104
	add	r7, sl, r7, asl #7
	fmsr	s13, r0	@ int
	mov	r0, #0
	mov	r3, r0
	fuitos	s15, s13
	fsts	s15, [sp, #4]
	bl	nm_WATERSENSE_calculate_run_minutes_for_this_station
	fmsr	s14, r0
	mov	r0, r4
	fmuls	s16, s16, s14
	ftouizs	s16, s16
	fsts	s16, [r7, #52]	@ int
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #0
	beq	.L385
	ldr	r7, [sp, #376]
	mov	r7, r7, asl #7
	add	r3, sl, r7
	ldr	r2, [r3, #52]
	ldrh	r1, [r3, #136]
	add	r1, r1, r2
	mov	r2, #0
	cmp	r1, r2
	str	r1, [r3, #52]
	strh	r2, [r3, #136]	@ movhi
	beq	.L385
	mov	r0, r4
	str	r2, [sp, #16]
	bl	nm_STATION_get_cycle_minutes_10u
	add	r1, r7, #52
	ldr	r7, [sp, #376]
	ldr	r2, [sp, #16]
	mov	r7, r7, asl #7
	add	r3, r7, #16
	add	r3, sl, r3
	add	r1, sl, r1
	add	r7, sl, r7
	fmsr	s15, r0	@ int
	mov	r0, r2
	fuitod	d6, s15
	fldd	d7, .L488+108
	fdivd	d6, d6, d11
	fmuld	d7, d6, d7
	ftouizd	s13, d7
	fmrs	r2, s13	@ int
	bl	WATERSENSE_make_min_cycle_adjustment
	ldr	r3, [sp, #376]
	add	sl, sl, r3, asl #7
	ldr	r3, [sl, #52]
	cmp	r3, #0
	moveq	r3, #1
	streq	r3, [sp, #148]
	strh	r0, [r7, #136]	@ movhi
.L385:
	ldr	r3, [sp, #376]
	ldr	r7, .L488+104
	mov	r0, r4
	add	r3, r7, r3, asl #7
	ldr	r3, [r3, #52]
	fmsr	s14, r3	@ int
	fuitos	s16, s14
	bl	STATION_get_ET_factor_100u
	ldrh	r1, [r5, #4]
	fdivs	s16, s16, s21
	mov	sl, r0
	mov	r0, r4
	bl	PERCENT_ADJUST_get_station_percentage_100u
	mov	r1, r4
	fmrs	r3, s16
	str	sl, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, #0
	mov	r2, r0
	bl	nm_WATERSENSE_calculate_adjusted_run_time
	fmsr	s13, r0
	mov	r0, r4
	fmuls	s15, s13, s21
	ftouizs	s15, s15
	fsts	s15, [sp, #372]	@ int
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #0
	beq	.L386
	ldr	r3, [sp, #372]
	cmp	r3, #0
	beq	.L386
	mov	r0, r4
	bl	nm_STATION_get_cycle_minutes_10u
	ldr	r3, [sp, #376]
	add	r1, sp, #372
	add	r7, r7, r3, asl #7
	add	r3, r7, #16
	fmsr	s14, r0	@ int
	mov	r0, #0
	fuitod	d6, s14
	fldd	d7, .L488+108
	fdivd	d6, d6, d11
	fmuld	d7, d6, d7
	ftouizd	s13, d7
	fmrs	r2, s13	@ int
	bl	WATERSENSE_make_min_cycle_adjustment
	ldr	r3, [sp, #372]
	cmp	r3, #0
	moveq	r3, #1
	streq	r3, [sp, #148]
.L386:
	ldr	r3, .L488+104
	ldr	r7, [sp, #376]
	mov	r1, #6
	ldr	r0, [sp, #372]
	add	r7, r3, r7, asl #7
	str	r3, [sp, #16]
	bl	__udivsi3
	ldrh	r2, [r7, #138]
	strh	r2, [r7, #56]	@ movhi
	ldrh	r2, [r7, #142]
	strh	r2, [r7, #60]	@ movhi
	strh	r0, [r7, #50]	@ movhi
	mov	r0, r6
	bl	SCHEDULE_get_stop_time
	ldr	r2, [r5, #0]
	ldrh	r1, [r5, #4]
	ldr	r3, [sp, #16]
	str	r1, [sp, #44]
	cmp	r0, r2
	mov	sl, r0
	bne	.L387
	mov	r2, #0
	str	r2, [sp, #372]
	ldr	r2, [sp, #376]
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #36]	@ zero_extendqisi2
	orr	r2, r2, #4
	strb	r2, [r3, #36]
	mov	r2, #1
	str	r2, [sp, #48]
	b	.L388
.L387:
	mov	r0, r5
	mov	r1, sl
	bl	FOAL_IRRI_return_stop_date_according_to_stop_time
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	str	r0, [sp, #44]
.L388:
	mov	r0, #0
	mov	r1, r6
	mov	r2, r0
	mov	r3, r5
	bl	STATION_GROUPS_skip_irrigation_due_to_a_mow_day
	cmp	r0, #0
	beq	.L389
	mov	r3, #0
	str	r3, [sp, #372]
	ldr	r2, [sp, #376]
	ldr	r3, .L488+104
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #39]	@ zero_extendqisi2
	orr	r2, r2, #4
	strb	r2, [r3, #39]
	mov	r3, #1
	str	r3, [sp, #52]
.L389:
	ldr	r3, [sp, #372]
	cmp	r3, #0
	beq	.L390
	ldr	r3, [sp, #160]
	cmp	r3, #0
	bne	.L480
	bl	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	subs	r7, r0, #0
	moveq	r3, #1
	streq	r3, [sp, #160]
	bne	.L484
	b	.L480
.L470:
	ldr	r2, [sp, #376]
	str	r0, [sp, #124]
	add	r3, r3, r2, asl #7
	strh	r0, [r3, #138]	@ movhi
	str	r0, [sp, #136]
	b	.L393
.L486:
	str	sl, [sp, #124]
	str	sl, [sp, #136]
.L393:
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	cmp	r9, #0
	str	r0, [sp, #152]
	bne	.L394
	mov	r0, r5
	mov	r1, #1
	bl	BUDGET_calculate_ratios
	mov	r9, #1
.L394:
	mov	r0, r6
	bl	nm_STATION_GROUP_get_budget_start_time_flag
	cmp	r0, #0
	bne	.L395
.LBB51:
	mov	r0, r6
	bl	STATION_GROUP_get_GID_irrigation_system
	mov	r1, r6
	bl	BUDGET_handle_alerts_at_start_time
	mov	r0, r6
	mov	r1, #1
	bl	nm_STATION_GROUP_set_budget_start_time_flag
.L395:
.LBE51:
	add	r3, sp, #60
	mov	r0, r7
	mov	r1, r4
	mov	r2, r6
	bl	_nm_nm_nm_add_to_the_irrigation_list
	ldr	sl, .L488+104
	cmp	r0, #1
	mov	r3, r0
	bne	.L396
	ldr	r0, [r7, #92]
	str	r3, [sp, #16]
	bl	MOISTURE_SENSORS_initialize__saved__irrigation_control_variables_for_this_moisture_sensor
	ldr	r2, [sp, #376]
	add	sl, sl, r2, asl #7
	ldrb	r1, [sl, #141]	@ zero_extendqisi2
	orr	r1, r1, #16
	strb	r1, [sl, #141]
	ldr	r3, [sp, #16]
	str	r3, [sp, #164]
	ldr	r3, [sp, #132]
	cmp	r3, #1
	ldreqb	r2, [sl, #38]	@ zero_extendqisi2
	orreq	r2, r2, #4
	streqb	r2, [sl, #38]
	streq	r3, [sp, #232]
	cmp	r8, #0
	beq	.L479
	ldr	r0, .L488+56
	mov	r1, r8
	ldr	r2, .L488+68
	ldr	r3, .L488+60
	bl	nm_ListRemove_debug
	cmp	r0, #0
	beq	.L479
	ldr	r0, .L488+64
	bl	Alert_Message
	b	.L479
.L396:
	ldr	r3, [sp, #144]
	cmp	r3, #1
	bne	.L398
	ldr	r2, [sp, #376]
	add	sl, sl, r2, asl #7
	ldrb	r2, [sl, #39]	@ zero_extendqisi2
	orr	r2, r2, #8
	strb	r2, [sl, #39]
	str	r3, [sp, #244]
.L398:
	ldr	r3, [sp, #68]
	cmp	r3, #1
	bne	.L399
	ldr	r1, [sp, #376]
	ldr	r2, .L488+104
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #37]	@ zero_extendqisi2
	orr	r1, r1, #128
	strb	r1, [r2, #37]
	str	r3, [sp, #168]
.L399:
	ldr	r3, [sp, #72]
	cmp	r3, #1
	bne	.L400
	ldr	r1, [sp, #376]
	ldr	r2, .L488+104
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #38]	@ zero_extendqisi2
	orr	r1, r1, #1
	strb	r1, [r2, #38]
	str	r3, [sp, #172]
.L400:
	ldr	r3, [sp, #80]
	cmp	r3, #1
	bne	.L401
	ldr	r1, [sp, #376]
	ldr	r2, .L488+104
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #36]	@ zero_extendqisi2
	orr	r1, r1, #2
	strb	r1, [r2, #36]
	str	r3, [sp, #180]
.L401:
	ldr	r3, [sp, #88]
	cmp	r3, #1
	bne	.L402
	ldr	r1, [sp, #376]
	ldr	r2, .L488+104
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #37]	@ zero_extendqisi2
	orr	r1, r1, #32
	strb	r1, [r2, #37]
	str	r3, [sp, #188]
.L402:
	ldr	r3, [sp, #96]
	cmp	r3, #1
	bne	.L403
	ldr	r1, [sp, #376]
	ldr	r2, .L488+104
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #37]	@ zero_extendqisi2
	orr	r1, r1, #64
	strb	r1, [r2, #37]
	str	r3, [sp, #196]
.L403:
	ldr	r3, [sp, #104]
	cmp	r3, #1
	bne	.L404
	ldr	r1, [sp, #376]
	ldr	r2, .L488+104
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #38]	@ zero_extendqisi2
	orr	r1, r1, #16
	strb	r1, [r2, #38]
	str	r3, [sp, #204]
.L404:
	ldr	r3, [sp, #112]
	cmp	r3, #1
	bne	.L405
	ldr	r1, [sp, #376]
	ldr	r2, .L488+104
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #38]	@ zero_extendqisi2
	orr	r1, r1, #32
	strb	r1, [r2, #38]
	str	r3, [sp, #212]
.L405:
	ldr	r3, [sp, #120]
	cmp	r3, #1
	bne	.L406
	ldr	r1, [sp, #376]
	ldr	r2, .L488+104
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #38]	@ zero_extendqisi2
	orr	r1, r1, #64
	strb	r1, [r2, #38]
	str	r3, [sp, #220]
.L406:
	ldr	r3, [sp, #128]
	cmp	r3, #1
	bne	.L407
	ldr	r1, [sp, #376]
	ldr	r2, .L488+104
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #38]	@ zero_extendqisi2
	orr	r1, r1, #2
	strb	r1, [r2, #38]
	str	r3, [sp, #228]
.L407:
	ldr	r3, [sp, #140]
	cmp	r3, #1
	bne	.L408
	ldr	r1, [sp, #376]
	ldr	r2, .L488+104
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #38]	@ zero_extendqisi2
	orr	r1, r1, #8
	strb	r1, [r2, #38]
	str	r3, [sp, #240]
.L408:
	ldr	r3, [sp, #156]
	cmp	r3, #1
	bne	.L480
	ldr	r1, [sp, #376]
	ldr	r2, .L488+104
	add	r2, r2, r1, asl #7
	ldrb	r1, [r2, #39]	@ zero_extendqisi2
	orr	r1, r1, #128
	strb	r1, [r2, #39]
	str	r3, [sp, #256]
	b	.L480
.L479:
	mov	r3, #1
	b	.L390
.L480:
	mov	r3, #0
.L390:
	ldr	r2, [sp, #148]
	cmp	r2, #1
	bne	.L409
	ldr	r0, [sp, #376]
	ldr	r1, .L488+104
	add	r1, r1, r0, asl #7
	ldrb	r0, [r1, #37]	@ zero_extendqisi2
	orr	r0, r0, #2
	strb	r0, [r1, #37]
	str	r2, [sp, #248]
.L409:
	ldr	r7, .L488+104
	ldr	r2, [sp, #376]
	cmp	r3, #0
	add	r2, r7, r2, asl #7
	ldrh	r1, [r2, #138]
	strh	r1, [r2, #58]	@ movhi
	bne	.L410
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #0
	beq	.L411
	mov	r0, r4
	ldr	r8, [sp, #376]
	bl	STATION_get_moisture_balance
	add	r7, r7, r8, asl #7
	fmsr	s13, r0
	fmuls	s15, s13, s20
	fmuls	s15, s15, s20
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
	strh	r3, [r7, #72]	@ movhi
.L411:
	ldr	r0, [sp, #376]
	bl	nm_STATION_HISTORY_close_and_start_a_new_record
	b	.L410
.L383:
	ldr	r3, .L488+104
	ldr	r2, [sp, #376]
	mov	r1, #1
	add	r3, r3, r2, asl #7
	strh	r0, [r3, #136]	@ movhi
	strh	r0, [r3, #138]	@ movhi
	str	r1, [sp, #56]
.L410:
	ldr	r3, .L488+76
	mov	sl, #1
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L368:
	mov	r0, r4
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L365
	mov	r0, r4
	bl	STATION_get_GID_manual_program_A
	mov	r7, #1
	mov	r2, #2
	str	r2, [sp, #20]
	str	sl, [sp, #44]
	mov	r8, r0
.L431:
	cmp	r8, #0
	beq	.L412
	mov	r0, r8
	mov	r1, r5
	bl	MANUAL_PROGRAMS_return_the_number_of_starts_for_this_GID
	cmp	r0, #0
	str	r0, [sp, #40]
	beq	.L412
	mov	r0, r8
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	bl	MANUAL_PROGRAMS_get_run_time
	ldr	r3, .L488+76
	mov	r1, #400
	ldr	r2, .L488+68
	mov	sl, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L488+72
	bl	xQueueTakeMutexRecursive_debug
	cmp	sl, #0
	beq	.L415
	ldr	r1, [sp, #260]
	cmp	r1, #0
	bne	.L415
	str	r1, [sp, #16]
	bl	nm_FOAL_return_pointer_to_next_available_and_fully_cleaned_ilc
	ldr	r1, [sp, #16]
	subs	r8, r0, #0
	streq	r7, [sp, #260]
	bne	.L485
	b	.L415
.L472:
	ldr	r3, [sp, #376]
	ldr	r1, .L488+104
	str	r0, [sp, #124]
	add	r3, r1, r3, asl #7
	strh	r0, [r3, #138]	@ movhi
	str	r0, [sp, #136]
	b	.L416
.L487:
	str	r7, [sp, #124]
	str	r7, [sp, #136]
.L416:
	cmp	r9, #0
	bne	.L417
	mov	r0, r5
	mov	r1, #1
	bl	BUDGET_calculate_ratios
	mov	r9, #1
.L417:
	mov	r0, r6
	bl	nm_STATION_GROUP_get_budget_start_time_flag
	cmp	r0, #0
	bne	.L418
.LBB52:
	mov	r0, r6
	bl	STATION_GROUP_get_GID_irrigation_system
	mov	r1, r6
	bl	BUDGET_handle_alerts_at_start_time
	mov	r0, r6
	mov	r1, #1
	bl	nm_STATION_GROUP_set_budget_start_time_flag
.L418:
.LBE52:
	mov	r0, r8
	mov	r1, r4
	mov	r2, #0
	add	r3, sp, #60
	bl	_nm_nm_nm_add_to_the_irrigation_list
	cmp	r0, #1
	bne	.L419
	ldr	r3, [sp, #132]
	str	r0, [sp, #264]
	cmp	r3, #1
	streq	r3, [sp, #332]
	b	.L415
.L419:
	ldr	r3, [sp, #144]
	cmp	r3, #1
	streq	r3, [sp, #344]
	ldr	r3, [sp, #68]
	cmp	r3, #1
	streq	r3, [sp, #268]
	ldr	r3, [sp, #72]
	cmp	r3, #1
	streq	r3, [sp, #272]
	ldr	r3, [sp, #80]
	cmp	r3, #1
	streq	r3, [sp, #280]
	ldr	r3, [sp, #88]
	cmp	r3, #1
	streq	r3, [sp, #288]
	ldr	r3, [sp, #96]
	cmp	r3, #1
	streq	r3, [sp, #296]
	ldr	r3, [sp, #104]
	cmp	r3, #1
	streq	r3, [sp, #304]
	ldr	r3, [sp, #112]
	cmp	r3, #1
	streq	r3, [sp, #312]
	ldr	r3, [sp, #120]
	cmp	r3, #1
	streq	r3, [sp, #320]
	ldr	r3, [sp, #128]
	cmp	r3, #1
	streq	r3, [sp, #328]
	ldr	r3, [sp, #140]
	cmp	r3, #1
	streq	r3, [sp, #340]
	mov	r3, #0
	str	r3, [sp, #348]
.L415:
	ldr	r2, .L488+76
	ldr	r0, [r2, #0]
	bl	xQueueGiveMutexRecursive
	mov	r3, #1
	str	r3, [sp, #28]
.L412:
	mov	r0, r4
	bl	STATION_get_GID_manual_program_B
	ldr	r1, [sp, #20]
	subs	r1, r1, #1
	str	r1, [sp, #20]
	mov	r8, r0
	bne	.L431
	ldr	sl, [sp, #44]
.L365:
.LBE54:
	mov	r1, r4
	ldr	r0, .L488+80
	bl	nm_ListGetNext
	mov	r4, r0
.L364:
	cmp	r4, #0
	bne	.L432
	cmp	sl, #0
	mov	r9, sl
	beq	.L433
	mov	r0, #1
	bl	Alert_scheduled_irrigation_started
	ldr	r2, [sp, #36]
	ldr	r3, [sp, #32]
	cmp	r2, #0
	cmpne	r3, #0
	beq	.L433
	mov	r0, r2
	mov	r1, r3
	mov	r2, r5
	bl	IRRITIME_make_ETTable_substitute_and_limit_alerts_after_we_know_program_ran
.L433:
	ldr	r3, [sp, #160]
	cmp	r3, #0
	beq	.L434
	mov	r0, #1
	mov	r1, #12
	bl	Alert_some_or_all_skipping_their_start
.L434:
	ldr	r1, [sp, #56]
	cmp	r1, #0
	beq	.L435
	mov	r0, #1
	mov	r1, #15
	bl	Alert_some_or_all_skipping_their_start
.L435:
	ldr	r3, [sp, #256]
	cmp	r3, #0
	beq	.L436
	mov	r0, #1
	mov	r1, #16
	bl	Alert_some_or_all_skipping_their_start
.L436:
	ldr	r2, [sp, #48]
	cmp	r2, #0
	beq	.L437
	mov	r0, #1
	mov	r1, #11
	bl	Alert_some_or_all_skipping_their_start
.L437:
	ldr	r3, [sp, #52]
	cmp	r3, #0
	beq	.L438
	mov	r0, #1
	mov	r1, #13
	bl	Alert_some_or_all_skipping_their_start
.L438:
	ldr	r3, [sp, #180]
	cmp	r3, #0
	beq	.L439
	mov	r0, #1
	mov	r1, #0
	bl	Alert_some_or_all_skipping_their_start
.L439:
	ldr	r3, [sp, #168]
	cmp	r3, #0
	beq	.L440
	mov	r0, #1
	mov	r1, r0
	bl	Alert_some_or_all_skipping_their_start
.L440:
	ldr	r3, [sp, #244]
	cmp	r3, #0
	beq	.L441
	mov	r0, #1
	mov	r1, #14
	bl	Alert_some_or_all_skipping_their_start
.L441:
	ldr	r3, [sp, #188]
	cmp	r3, #0
	beq	.L442
	mov	r0, #1
	mov	r1, #2
	bl	Alert_some_or_all_skipping_their_start
.L442:
	ldr	r3, [sp, #196]
	cmp	r3, #0
	beq	.L443
	mov	r0, #1
	mov	r1, #3
	bl	Alert_some_or_all_skipping_their_start
.L443:
	ldr	r3, [sp, #172]
	cmp	r3, #0
	beq	.L444
	mov	r0, #1
	mov	r1, #4
	bl	Alert_some_or_all_skipping_their_start
.L444:
	ldr	r3, [sp, #240]
	cmp	r3, #0
	beq	.L445
	mov	r0, #1
	mov	r1, #5
	bl	Alert_some_or_all_skipping_their_start
.L445:
	ldr	r3, [sp, #228]
	cmp	r3, #0
	beq	.L446
	mov	r0, #1
	mov	r1, #6
	bl	Alert_some_or_all_skipping_their_start
.L446:
	ldr	r3, [sp, #204]
	cmp	r3, #0
	beq	.L447
	mov	r0, #1
	mov	r1, #7
	bl	Alert_some_or_all_skipping_their_start
.L447:
	ldr	r3, [sp, #212]
	cmp	r3, #0
	beq	.L448
	mov	r0, #1
	mov	r1, #8
	bl	Alert_some_or_all_skipping_their_start
.L448:
	ldr	r3, [sp, #220]
	cmp	r3, #0
	beq	.L449
	mov	r0, #1
	mov	r1, #9
	bl	Alert_some_or_all_skipping_their_start
.L449:
	ldr	r3, [sp, #248]
	cmp	r3, #0
	beq	.L450
	mov	r0, #1
	mov	r1, #10
	bl	Alert_some_or_all_skipping_their_start
.L450:
	cmp	r9, #0
	beq	.L451
	ldr	r3, [sp, #164]
	cmp	r3, #0
	bne	.L451
	ldr	r0, .L488+84
	bl	Alert_Message
.L451:
	ldr	r1, [sp, #28]
	cmp	r1, #0
	beq	.L452
	mov	r0, #2
	bl	Alert_scheduled_irrigation_started
.L452:
	ldr	r3, [sp, #260]
	cmp	r3, #0
	beq	.L453
	mov	r0, #2
	mov	r1, #12
	bl	Alert_some_or_all_skipping_their_start
.L453:
	ldr	r3, [sp, #280]
	cmp	r3, #0
	beq	.L454
	mov	r0, #2
	mov	r1, #0
	bl	Alert_some_or_all_skipping_their_start
.L454:
	ldr	r3, [sp, #268]
	cmp	r3, #0
	beq	.L455
	mov	r0, #2
	mov	r1, #1
	bl	Alert_some_or_all_skipping_their_start
.L455:
	ldr	r3, [sp, #344]
	cmp	r3, #0
	beq	.L456
	mov	r0, #2
	mov	r1, #14
	bl	Alert_some_or_all_skipping_their_start
.L456:
	ldr	r3, [sp, #288]
	cmp	r3, #0
	beq	.L457
	mov	r0, #2
	mov	r1, r0
	bl	Alert_some_or_all_skipping_their_start
.L457:
	ldr	r3, [sp, #296]
	cmp	r3, #0
	beq	.L458
	mov	r0, #2
	mov	r1, #3
	bl	Alert_some_or_all_skipping_their_start
.L458:
	ldr	r3, [sp, #272]
	cmp	r3, #0
	beq	.L459
	mov	r0, #2
	mov	r1, #4
	bl	Alert_some_or_all_skipping_their_start
.L459:
	ldr	r3, [sp, #340]
	cmp	r3, #0
	beq	.L460
	mov	r0, #2
	mov	r1, #5
	bl	Alert_some_or_all_skipping_their_start
.L460:
	ldr	r3, [sp, #328]
	cmp	r3, #0
	beq	.L461
	mov	r0, #2
	mov	r1, #6
	bl	Alert_some_or_all_skipping_their_start
.L461:
	ldr	r3, [sp, #304]
	cmp	r3, #0
	beq	.L462
	mov	r0, #2
	mov	r1, #7
	bl	Alert_some_or_all_skipping_their_start
.L462:
	ldr	r3, [sp, #312]
	cmp	r3, #0
	beq	.L463
	mov	r0, #2
	mov	r1, #8
	bl	Alert_some_or_all_skipping_their_start
.L463:
	ldr	r3, [sp, #320]
	cmp	r3, #0
	beq	.L464
	mov	r0, #2
	mov	r1, #9
	bl	Alert_some_or_all_skipping_their_start
.L464:
	ldr	r2, [sp, #28]
	cmp	r2, #0
	beq	.L465
	ldr	r3, [sp, #264]
	cmp	r3, #0
	bne	.L465
	ldr	r0, .L488+88
	bl	Alert_Message
.L465:
	mov	r4, #0
	b	.L466
.L468:
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	mov	r6, r0
	bl	SCHEDULE_get_start_time
	ldr	r3, [r5, #0]
	cmp	r0, r3
	bne	.L467
	ldrb	r3, [r5, #19]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L467
	mov	r0, r6
	mov	r1, r5
	mov	r2, #2
	bl	SCHEDULE_does_it_irrigate_on_this_date
.L467:
	add	r4, r4, #1
.L466:
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r4, r0
	bcc	.L468
	ldr	r3, [sp, #28]
	orrs	r3, r9, r3
	beq	.L469
	ldr	r0, .L488+92
	bl	postBackground_Calculation_Event
.L469:
	ldr	r3, .L488+96
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L488+100
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #384
	fldmfdd	sp!, {d8, d9, d10, d11}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L484:
.LBB55:
	ldrh	r3, [r7, #72]
	ldr	r1, [sp, #20]
	ldr	r2, [sp, #40]
	bic	r3, r3, #896
	orr	r3, r3, #64
	strb	r1, [r7, #91]
	strb	r2, [r7, #90]
	strh	r3, [r7, #72]	@ movhi
	mov	r0, r4
	bl	ACQUIRE_EXPECTEDS_get_station_acquire_expected_flow
	ldrb	r3, [r7, #73]	@ zero_extendqisi2
	bic	r3, r3, #4
	and	r0, r0, #1
	orr	r3, r3, r0, asl #2
	strb	r3, [r7, #73]
	ldr	r2, [sp, #376]
	ldr	r3, .L488+104
	mov	r0, r4
	add	r2, r3, r2, asl #7
	ldrb	r1, [r2, #141]	@ zero_extendqisi2
	ldrb	r2, [r7, #73]	@ zero_extendqisi2
	and	r1, r1, #16
	bic	r2, r2, #16
	orr	r2, r1, r2
	strb	r2, [r7, #73]
	ldr	r2, [sp, #372]
	str	r2, [r7, #52]
	str	r3, [sp, #16]
	bl	nm_STATION_get_cycle_minutes_10u
	ldr	r1, [sp, #44]
	str	sl, [r7, #68]
	strh	r1, [r7, #88]	@ movhi
	mov	sl, #1
	str	sl, [sp, #76]
	str	sl, [sp, #84]
	str	sl, [sp, #92]
	str	sl, [sp, #100]
	str	sl, [sp, #108]
	fmsr	s14, r0	@ int
	mov	r0, r4
	fuitod	d6, s14
	fldd	d7, .L490
	fdivd	d6, d6, d11
	fmuld	d7, d6, d7
	ftouizd	s13, d7
	fsts	s13, [r7, #56]	@ int
	bl	WEATHER_get_station_uses_wind
	str	r0, [sp, #116]
	mov	r0, r4
	bl	WEATHER_get_station_uses_rain
	ldr	r3, [sp, #16]
	cmp	r0, #0
	beq	.L470
	b	.L486
.L485:
	ldrh	r3, [r8, #72]
	ldr	r2, [sp, #24]
	bic	r3, r3, #832
	orr	r3, r3, #128
	strh	r3, [r8, #72]	@ movhi
	strb	r2, [r8, #90]
	ldr	r2, [sp, #40]
	strb	fp, [r8, #91]
	mul	r3, r2, sl
	str	sl, [r8, #56]
	str	r3, [r8, #52]
	mov	r2, #100
	add	r0, sp, #60
	bl	memset
	mov	r0, r4
	str	r7, [sp, #76]
	str	r7, [sp, #84]
	str	r7, [sp, #92]
	str	r7, [sp, #100]
	str	r7, [sp, #108]
	bl	WEATHER_get_station_uses_wind
	str	r0, [sp, #116]
	mov	r0, r4
	bl	WEATHER_get_station_uses_rain
	cmp	r0, #0
	beq	.L472
	b	.L487
.L491:
	.align	2
.L490:
	.word	0
	.word	1078853632
.LBE55:
.LFE31:
	.size	TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start, .-TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start
	.section	.text.FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists, %function
FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI34:
	mov	r5, r3
	ldr	r3, .L507
	mov	r4, r0
	mov	r7, r1
	mov	r6, r2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L507+4
	ldr	r3, .L507+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L507+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L507+4
	ldr	r3, .L507+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L507+20
	mov	r8, #0
	str	r8, [r0, #76]
	add	r0, r0, #16
	bl	nm_ListGetFirst
	b	.L504
.L497:
	ldr	r3, [r1, #40]
	cmp	r3, r4
	bne	.L494
	cmp	r6, #0
	bne	.L495
	ldrh	r3, [r1, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	cmp	r3, r7
	bne	.L494
.L495:
	ldr	r0, .L507+24
	mov	r2, r5
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	mov	r8, #1
	mov	r1, r0
	b	.L502
.L494:
	ldr	r0, .L507+24
	bl	nm_ListGetNext
.L504:
	mov	r1, r0
.L502:
	cmp	r1, #0
	bne	.L497
	ldr	r0, .L507+24
	bl	nm_ListGetFirst
	b	.L505
.L500:
	ldr	r3, [r1, #40]
	cmp	r3, r4
	beq	.L499
	ldr	r0, .L507+24
	bl	nm_ListGetNext
.L505:
	cmp	r0, #0
	mov	r1, r0
	bne	.L500
	b	.L506
.L499:
	ldr	r3, .L507+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L507
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r8
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L506:
	ldr	r0, [r4, #0]
	bl	nm_shut_down_mvs_and_pmps_throughout_this_system
	b	.L499
.L508:
	.align	2
.L507:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	2499
	.word	system_preserves_recursive_MUTEX
	.word	2501
	.word	foal_irri
	.word	foal_irri+16
.LFE20:
	.size	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists, .-FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists
	.section	.text.FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists, %function
FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L516
	stmfd	sp!, {r4, r5, lr}
.LCFI35:
	ldr	r2, .L516+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L516+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L516+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L516+4
	ldr	r3, .L516+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L516+20
	mov	r3, #0
	str	r3, [r0, #76]
	add	r0, r0, #16
	bl	nm_ListGetFirst
	b	.L515
.L513:
	ldrb	r3, [r1, #91]	@ zero_extendqisi2
	ldr	r0, .L516+24
	cmp	r3, r5
	bne	.L511
	mov	r2, r4
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	b	.L515
.L511:
	bl	nm_ListGetNext
.L515:
	cmp	r0, #0
	mov	r1, r0
	bne	.L513
	ldr	r3, .L516+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L516
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L517:
	.align	2
.L516:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	2405
	.word	system_preserves_recursive_MUTEX
	.word	2407
	.word	foal_irri
	.word	foal_irri+16
.LFE18:
	.size	FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists, .-FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists
	.section	.text.FOAL_IRRI_remove_all_stations_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_all_stations_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_all_stations_from_the_irrigation_lists, %function
FOAL_IRRI_remove_all_stations_from_the_irrigation_lists:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI36:
	ldr	r4, .L522
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L522+4
	ldr	r3, .L522+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, .L522+12
	mov	r6, #0
.L520:
	ldr	r3, [r7, #16]
	cmp	r3, #0
	beq	.L519
	mov	r0, r6
	mov	r1, r5
	bl	FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists
.L519:
	add	r6, r6, #1
	cmp	r6, #12
	add	r7, r7, #92
	bne	.L520
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L523:
	.align	2
.L522:
	.word	chain_members_recursive_MUTEX
	.word	.LC1
	.word	2449
	.word	chain
.LFE19:
	.size	FOAL_IRRI_remove_all_stations_from_the_irrigation_lists, .-FOAL_IRRI_remove_all_stations_from_the_irrigation_lists
	.section	.text.FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists, %function
FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI37:
	mov	r4, r3
	ldr	r3, .L532
	mov	r7, r0
	mov	r6, r1
	mov	r5, r2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L532+4
	ldr	r3, .L532+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L532+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L532+4
	ldr	r3, .L532+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L532+20
	mov	r8, #0
	str	r8, [r0, #76]
	add	r0, r0, #16
	bl	nm_ListGetFirst
	b	.L531
.L528:
	ldrb	r3, [r1, #91]	@ zero_extendqisi2
	cmp	r3, r7
	bne	.L526
	ldrb	r3, [r1, #90]	@ zero_extendqisi2
	cmp	r3, r6
	bne	.L526
	ldrh	r3, [r1, #72]
	mov	r3, r3, lsr #6
	and	r3, r3, #15
	cmp	r3, r5
	bne	.L526
	ldr	r0, .L532+24
	mov	r2, r4
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	mov	r8, #1
	mov	r1, r0
	b	.L530
.L526:
	ldr	r0, .L532+24
	bl	nm_ListGetNext
.L531:
	mov	r1, r0
.L530:
	cmp	r1, #0
	bne	.L528
	cmp	r8, #0
	bne	.L529
	ldr	r0, .L532+28
	bl	Alert_Message
.L529:
	ldr	r3, .L532+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L532
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L533:
	.align	2
.L532:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	2341
	.word	system_preserves_recursive_MUTEX
	.word	2343
	.word	foal_irri
	.word	foal_irri+16
	.word	.LC36
.LFE17:
	.size	FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists, .-FOAL_IRRI_remove_this_station_for_this_reason_from_the_irrigation_lists
	.section	.text.FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists, %function
FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L542
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI38:
	ldr	r2, .L542+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L542+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L542+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L542+4
	ldr	r3, .L542+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L542+20
	mov	r3, #0
	str	r3, [r0, #76]
	add	r0, r0, #36
	bl	nm_ListGetFirst
	b	.L541
.L539:
	ldrb	r3, [r6, #91]	@ zero_extendqisi2
	cmp	r3, r5
	bne	.L536
	cmp	r4, #104
	bne	.L537
	mov	r0, r5
	ldrb	r1, [r6, #90]	@ zero_extendqisi2
	bl	Alert_conventional_station_short_idx
.L537:
	mov	r1, r6
	mov	r2, r4
	ldr	r0, .L542+24
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	b	.L541
.L536:
	ldr	r0, .L542+24
	mov	r1, r6
	bl	nm_ListGetNext
.L541:
	cmp	r0, #0
	mov	r6, r0
	bne	.L539
	ldr	r3, .L542+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L542
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L543:
	.align	2
.L542:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	2247
	.word	system_preserves_recursive_MUTEX
	.word	2249
	.word	foal_irri
	.word	foal_irri+36
.LFE16:
	.size	FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists, .-FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists
	.section	.text.FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists
	.type	FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists, %function
FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L559
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI39:
	mov	r6, r0
	mov	r5, r1
	mov	r4, r2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L559+4
	ldr	r3, .L559+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L559+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L559+4
	ldr	r3, .L559+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L559+20
	mov	r7, #0
	str	r7, [r0, #76]
	add	r0, r0, #16
	bl	nm_ListGetFirst
	b	.L556
.L548:
	ldrb	r3, [r1, #91]	@ zero_extendqisi2
	cmp	r3, r6
	bne	.L546
	ldrb	r3, [r1, #90]	@ zero_extendqisi2
	cmp	r3, r5
	bne	.L546
	mov	r2, r4
	ldr	r0, .L559+24
	ldr	r7, [r1, #40]
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	b	.L556
.L546:
	ldr	r0, .L559+24
	bl	nm_ListGetNext
.L556:
	cmp	r0, #0
	mov	r1, r0
	bne	.L548
	cmp	r7, #0
	bne	.L549
	cmp	r4, #112
	cmpne	r4, #109
	beq	.L550
	ldr	r0, .L559+28
	bl	Alert_Message
	b	.L550
.L549:
	cmp	r4, #104
	bne	.L550
.LBB56:
	ldr	r0, .L559+24
	bl	nm_ListGetFirst
	b	.L557
.L552:
	ldr	r3, [r1, #40]
	cmp	r3, r7
	beq	.L550
	ldr	r0, .L559+24
	bl	nm_ListGetNext
.L557:
	cmp	r0, #0
	mov	r1, r0
	bne	.L552
	b	.L558
.L550:
.LBE56:
	ldr	r3, .L559+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L559
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L558:
.LBB57:
	ldr	r0, [r7, #0]
	bl	nm_shut_down_mvs_and_pmps_throughout_this_system
	b	.L550
.L560:
	.align	2
.L559:
	.word	list_foal_irri_recursive_MUTEX
	.word	.LC1
	.word	2116
	.word	system_preserves_recursive_MUTEX
	.word	2118
	.word	foal_irri
	.word	foal_irri+16
	.word	.LC37
.LBE57:
.LFE15:
	.size	FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists, .-FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists
	.section	.text.irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations,"ax",%progbits
	.align	2
	.global	irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations
	.type	irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations, %function
irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations:
.LFB49:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI40:
.LBB58:
	ldr	r9, .L622
.LBE58:
	sub	sp, sp, #24
.LCFI41:
	mov	sl, r0
	ldr	r0, .L622+4
	bl	nm_ListGetFirst
	mov	r5, #0
	mov	r7, r5
	mov	r6, r5
	mov	r8, r5
	mov	r4, r0
	b	.L616
.L608:
.LBB60:
	ldr	r3, .L622+8
	mov	r1, #0
	str	r3, [sp, #8]
	ldr	r3, .L622+12
	add	r0, r4, #91
	str	r3, [sp, #12]
	mov	r2, #11
	mov	r3, r1
	stmia	sp, {r1, r9}
	bl	RC_uns8_with_filename
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L612
	ldrh	r3, [r4, #88]
	add	r1, sp, #24
	strh	r3, [sp, #20]	@ movhi
	ldr	r3, [r4, #68]
	mov	r0, sl
	str	r3, [r1, #-8]!
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #1
	bne	.L612
	cmp	r8, #0
	bne	.L564
	bl	Alert_hit_stop_time
.L564:
	mov	r1, r4
	ldr	r0, .L622+4
	mov	r2, #28
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	mov	r8, #1
	mov	r3, #0
	mov	r4, r0
	b	.L563
.L612:
	mov	r3, #1
.L563:
	ldrb	r2, [r4, #76]	@ zero_extendqisi2
	tst	r2, #4
	beq	.L565
	ldr	r2, .L622+16
	ldr	r1, [r2, #60]
	cmp	r1, #0
	bne	.L566
	ldr	r2, [r2, #56]
	cmp	r2, #0
	beq	.L565
.L566:
	cmp	r6, #0
	bne	.L567
	bl	Alert_rain_affecting_irrigation
.L567:
	mov	r1, r4
	ldr	r0, .L622+4
	mov	r2, #29
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	mov	r6, #1
	mov	r3, #0
	mov	r4, r0
.L565:
	ldrb	r2, [r4, #76]	@ zero_extendqisi2
	tst	r2, #64
	beq	.L568
	ldr	r2, .L622+16
	ldr	r2, [r2, #100]
	cmp	r2, #1
	bne	.L568
	cmp	r7, #0
	bne	.L569
	bl	Alert_rain_switch_affecting_irrigation
.L569:
	mov	r1, r4
	ldr	r0, .L622+4
	mov	r2, #30
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	mov	r7, #1
	mov	r3, #0
	mov	r4, r0
.L568:
	ldrb	r2, [r4, #76]	@ zero_extendqisi2
	tst	r2, #128
	beq	.L570
	ldr	r2, .L622+16
	ldr	r2, [r2, #104]
	cmp	r2, #1
	bne	.L570
	cmp	r5, #0
	bne	.L571
	bl	Alert_freeze_switch_affecting_irrigation
.L571:
	mov	r1, r4
	ldr	r0, .L622+4
	mov	r2, #31
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	mov	r5, #1
	mov	r4, r0
	b	.L616
.L570:
	cmp	r3, #1
	bne	.L616
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L573
	mov	r0, r4
	bl	MOISTURE_SENSORS_update__transient__control_variables_for_this_station
.L573:
	ldr	r3, [r4, #40]
	ldr	r2, [r3, #84]
	ldr	r1, [r3, #80]
	add	r2, r2, #1
	str	r2, [r3, #84]
	ldrh	r2, [r4, #72]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	cmp	r2, r1
	strhi	r2, [r3, #80]
	ldrh	r2, [r4, #72]
	and	r2, r2, #960
	cmp	r2, #64
	ldreqb	r2, [r3, #467]	@ zero_extendqisi2
	orreq	r2, r2, #16
	beq	.L618
	cmp	r2, #128
	bne	.L577
	ldrb	r2, [r3, #467]	@ zero_extendqisi2
	orr	r2, r2, #32
.L618:
	strb	r2, [r3, #467]
	b	.L576
.L577:
	cmp	r2, #256
	ldreqb	r2, [r3, #467]	@ zero_extendqisi2
	orreq	r2, r2, #128
	beq	.L618
	cmp	r2, #320
	ldreqb	r2, [r3, #468]	@ zero_extendqisi2
	orreq	r2, r2, #1
	beq	.L619
	cmp	r2, #384
	ldreqb	r2, [r3, #468]	@ zero_extendqisi2
	orreq	r2, r2, #2
	beq	.L619
	cmp	r2, #448
	bne	.L576
	ldrb	r2, [r3, #468]	@ zero_extendqisi2
	orr	r2, r2, #4
.L619:
	strb	r2, [r3, #468]
.L576:
	ldr	r3, [r4, #60]
	cmp	r3, #0
	bne	.L581
	ldr	r3, [r4, #40]
	ldrh	r2, [r4, #72]
	ldr	r1, [r3, #180]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	cmp	r2, r1
	strhi	r2, [r3, #180]
	ldrb	r2, [r4, #73]	@ zero_extendqisi2
	tst	r2, #8
	beq	.L583
	ldrh	r2, [r4, #72]
	ldr	r1, [r3, #184]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	cmp	r2, r1
	strhi	r2, [r3, #184]
.L583:
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	tst	r3, #8
	bne	.L581
	ldr	r3, [r4, #40]
	ldrh	r2, [r4, #72]
	ldr	r1, [r3, #188]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	cmp	r2, r1
	strhi	r2, [r3, #188]
.L581:
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	ldr	r0, .L622+20
	tst	r3, #64
	mov	r1, r4
	beq	.L584
	bl	nm_OnList
	cmp	r0, #0
	bne	.L585
	ldr	r0, .L622+24
	ldr	r1, .L622+28
	ldr	r2, .L622+8
	ldr	r3, .L622+32
	bl	Alert_item_not_on_list_with_filename
.L585:
	ldr	r3, [r4, #48]
	sub	r3, r3, #1
	cmp	r3, #0
	str	r3, [r4, #48]
	bgt	.L586
	mov	r1, r4
	mov	r2, #5
	ldr	r0, .L622+4
	bl	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	b	.L620
.L586:
	ldrb	r3, [r4, #75]	@ zero_extendqisi2
	ldr	r0, [r4, #40]
	tst	r3, #1
	movne	r3, #1
	strne	r3, [r0, #160]
	ldr	r2, [r4, #44]
	ldr	r3, .L622+36
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #140]	@ zero_extendqisi2
	ldr	r3, .L622+40
	and	r2, r2, #15
	ldr	r3, [r0, r3]
	cmp	r2, r3
	movcc	r3, #0
	strcc	r3, [r0, #164]
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #448
	cmpne	r3, #384
	beq	.L589
	ldrh	r1, [r4, #86]
	bl	SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit
	ldrh	r0, [r4, #86]
	bl	ON_AT_A_TIME_bump_this_groups_ON_count
.L589:
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #320
	ldreq	r3, .L622+44
	moveq	r2, #1
	streq	r2, [r3, #0]
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	tst	r3, #32
	ldrne	r3, [r4, #40]
	movne	r2, #1
	strne	r2, [r3, #104]
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	mov	r2, #1
	tst	r3, #8
	ldr	r3, [r4, #40]
	strne	r2, [r3, #172]
	streq	r2, [r3, #176]
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	tst	r3, #4
	beq	.L594
	ldrh	r3, [r4, #72]
	mov	r2, #1
	and	r3, r3, #960
	cmp	r3, #448
	ldr	r3, [r4, #40]
	streq	r2, [r3, #140]
	beq	.L594
	str	r2, [r3, #132]
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L594
	ldrh	r0, [r4, #86]
	bl	ACQUIRE_EXPECTEDS_clear_acquire_expected_flow_flag_for_GID
	b	.L594
.L584:
	bl	nm_OnList
	cmp	r0, #1
	bne	.L596
	ldr	r0, .L622+8
	bl	RemovePathFromFileName
	ldr	r2, .L622+48
	mov	r1, r0
	ldr	r0, .L622+52
	bl	Alert_Message_va
.L596:
	ldr	r3, [r4, #60]
	cmp	r3, #0
	subne	r3, r3, #1
	strne	r3, [r4, #60]
	ldr	r3, [r4, #52]
	cmp	r3, #0
	moveq	fp, #20
	beq	.L598
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L599
	mov	r0, r4
	bl	MOISTURE_SENSORS_ilc_is_allowed_to_be_removed_from_the_list
	cmp	r0, #0
	movne	fp, #33
	beq	.L599
.L598:
.LBB59:
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L600
	ldrb	r3, [r4, #75]	@ zero_extendqisi2
	tst	r3, #8
	beq	.L600
	tst	r3, #16
	bne	.L600
	ldr	r2, [r4, #44]
	ldr	r3, .L622+36
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #37]	@ zero_extendqisi2
	orr	r2, r2, #16
	strb	r2, [r3, #37]
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	ldrb	r1, [r4, #90]	@ zero_extendqisi2
	bl	Alert_flow_not_checked_with_no_reasons_idx
.L600:
.LBE59:
	mov	r1, r4
	mov	r2, fp
	ldr	r0, .L622+4
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	b	.L620
.L609:
	ldrh	r3, [r4, #72]
	mov	r2, #1
	and	r3, r3, #960
	cmp	r3, #448
	ldr	r3, [r4, #40]
	streq	r2, [r3, #144]
	beq	.L602
	str	r2, [r3, #136]
	ldrb	r1, [r4, #73]	@ zero_extendqisi2
	tst	r1, #8
	strne	r2, [r3, #148]
	streq	r2, [r3, #152]
.L602:
	ldr	r3, [r4, #40]
	ldrh	r2, [r4, #72]
	ldr	r1, [r3, #128]
	mov	r2, r2, lsr #6
	and	r2, r2, #15
	cmp	r2, r1
	strhi	r2, [r3, #128]
.L604:
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L605
	ldr	r3, [r4, #60]
	cmp	r3, #0
	bne	.L605
	ldr	r3, [r4, #52]
	cmp	r3, #0
	beq	.L605
	ldr	r3, [r4, #40]
	ldrb	r2, [r4, #72]	@ zero_extendqisi2
	ldr	r1, [r3, #108]
	mov	r2, r2, lsr #4
	and	r2, r2, #3
	cmp	r2, r1
	bls	.L606
	ldrb	r1, [r4, #73]	@ zero_extendqisi2
	tst	r1, #8
	strne	r2, [r3, #108]
.L606:
	ldrb	r2, [r4, #72]	@ zero_extendqisi2
	ldr	r1, [r3, #112]
	mov	r2, r2, lsr #4
	and	r2, r2, #3
	cmp	r2, r1
	bls	.L605
	ldrb	r1, [r4, #73]	@ zero_extendqisi2
	tst	r1, #8
	streq	r2, [r3, #112]
.L605:
	ldr	r3, [r4, #60]
	cmp	r3, #0
	bne	.L594
	ldr	r3, [r4, #52]
	cmp	r3, #0
	beq	.L594
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	mov	r2, #1
	tst	r3, #8
	ldr	r3, [r4, #40]
	strne	r2, [r3, #120]
	streq	r2, [r3, #124]
	b	.L594
.L616:
.LBE60:
	cmp	r4, #0
	bne	.L608
	b	.L621
.L599:
.LBB61:
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	tst	r3, #4
	bne	.L609
	b	.L604
.L594:
	ldr	r0, .L622+4
	mov	r1, r4
	bl	nm_ListGetNext
.L620:
	mov	r4, r0
	b	.L616
.L621:
.LBE61:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L623:
	.align	2
.L622:
	.word	.LC13
	.word	foal_irri+16
	.word	.LC1
	.word	7140
	.word	weather_preserves
	.word	foal_irri+36
	.word	.LC38
	.word	.LC39
	.word	7366
	.word	station_preserves
	.word	14032
	.word	.LANCHOR1
	.word	7486
	.word	.LC40
.LFE49:
	.size	irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations, .-irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations
	.section	.text.irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars,"ax",%progbits
	.align	2
	.global	irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars
	.type	irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars, %function
irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars:
.LFB50:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI42:
	ldr	r4, .L661
	ldr	r6, .L661+4
	mov	r5, #0
	mov	r7, r4
	mov	r8, r4
.L651:
	ldr	r3, [r4, #16]
	cmp	r3, #0
	beq	.L625
	ldr	sl, [r4, #108]
	cmp	sl, #0
	bne	.L626
	ldr	r3, [r4, #116]
	cmp	r3, #0
	beq	.L627
	ldr	r0, .L661+8
	bl	Alert_Message
	str	sl, [r4, #116]
.L627:
	ldr	r3, [r4, #184]
	cmp	r3, #0
	beq	.L626
	ldr	r0, .L661+12
	bl	Alert_Message
	mov	r3, #0
	str	r3, [r4, #184]
.L626:
	ldr	r3, [r4, #152]
	cmp	r3, #1
	streq	r3, [r4, #128]
	streq	r3, [r4, #124]
	ldr	r3, [r4, #100]
	cmp	r3, #0
	beq	.L643
.L629:
	ldr	r3, [r4, #124]
	ldr	r2, [r4, #128]
	cmp	r3, r2
	bls	.L631
	ldr	r1, [r4, #196]
	cmp	r1, #1
	streq	r1, [r4, #112]
	beq	.L630
.L631:
	cmp	r2, r3
	bls	.L632
	ldr	r3, [r4, #196]
	cmp	r3, #1
	beq	.L655
.L632:
	ldr	r3, [r4, #188]
	cmp	r3, #1
	beq	.L656
.L633:
	ldr	r3, [r4, #192]
	cmp	r3, #1
	beq	.L655
.L634:
	ldr	r3, [r4, #196]
	cmp	r3, #2
	bls	.L635
	ldr	r2, [r4, #200]
	ldr	r3, [r4, #204]
	cmp	r2, r3
	bhi	.L643
.L636:
	bcs	.L630
	b	.L655
.L635:
	ldr	r3, [r4, #152]
	cmp	r3, #1
	ldr	r3, [r4, #112]
	bne	.L637
	cmp	r3, #1
	ldreq	r3, [r4, #164]
	beq	.L660
	cmp	r3, #0
	ldreq	r3, [r4, #168]
	beq	.L658
	b	.L643
.L637:
	cmp	r3, #1
	bne	.L641
	ldr	r3, [r4, #136]
.L660:
	cmp	r3, #1
	bne	.L655
	b	.L630
.L641:
	cmp	r3, #0
	bne	.L643
	ldr	r3, [r4, #140]
.L658:
	cmp	r3, #1
	bne	.L643
	b	.L630
.L655:
	mov	r3, #0
	b	.L656
.L643:
	mov	r3, #1
.L656:
	str	r3, [r4, #112]
.L630:
	ldr	r3, [r4, #176]
	cmp	r3, #0
	beq	.L644
	ldr	r3, [r4, #184]
	cmp	r3, #1
	bhi	.L644
	mla	r0, r6, r5, r7
	add	r0, r0, #208
	bl	FOAL_IRRI_there_is_more_than_one_flow_group_ON
	cmp	r0, #0
	beq	.L644
	ldr	r0, .L661+16
	bl	Alert_Message
.L644:
	ldr	r3, [r4, #192]
	cmp	r3, #1
	movne	r3, #0
	bne	.L645
	ldr	r3, [r4, #188]
	sub	r2, r3, #1
	rsbs	r3, r2, #0
	adc	r3, r3, r2
.L645:
	str	r3, [r4, #172]
	mla	r3, r6, r5, r7
	ldrb	r2, [r3, #482]	@ zero_extendqisi2
	tst	r2, #32
	beq	.L646
	ldrb	r3, [r3, #483]	@ zero_extendqisi2
	tst	r3, #16
	bne	.L646
	mov	r0, #1
	bl	Alert_irrigation_ended
.L646:
	ldr	r3, .L661+4
	mla	r3, r5, r3, r8
	ldrb	r2, [r3, #482]	@ zero_extendqisi2
	tst	r2, #64
	beq	.L647
	ldrb	r3, [r3, #483]	@ zero_extendqisi2
	tst	r3, #32
	bne	.L647
	mov	r0, #2
	bl	Alert_irrigation_ended
.L647:
	mla	r3, r6, r5, r7
	ldrb	r3, [r3, #483]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L648
	movs	r3, r3, lsr #7
	bne	.L648
	mov	r0, #4
	bl	Alert_irrigation_ended
.L648:
	ldr	r3, .L661+4
	mla	r3, r5, r3, r8
	ldrb	r2, [r3, #483]	@ zero_extendqisi2
	tst	r2, #2
	beq	.L649
	ldrb	r3, [r3, #484]	@ zero_extendqisi2
	tst	r3, #1
	bne	.L649
	mov	r0, #5
	bl	Alert_irrigation_ended
.L649:
	mla	r3, r6, r5, r7
	ldrb	r2, [r3, #483]	@ zero_extendqisi2
	tst	r2, #4
	beq	.L650
	ldrb	r3, [r3, #484]	@ zero_extendqisi2
	tst	r3, #2
	bne	.L650
	mov	r0, #6
	bl	Alert_irrigation_ended
.L650:
	mla	r3, r6, r5, r8
	ldrb	r2, [r3, #483]	@ zero_extendqisi2
	ldrb	r0, [r3, #482]	@ zero_extendqisi2
	mov	ip, r2, asl #1
	mov	r1, r2, asl #1
	and	ip, ip, #32
	and	r0, r0, #159
	orr	r0, r0, ip
	and	r1, r1, #64
	orr	r1, r1, r0
	strb	r1, [r3, #482]
	bic	r1, r2, #1
	orr	r2, r1, r2, lsr #7
	ldrb	r1, [r3, #484]	@ zero_extendqisi2
	and	r2, r2, #255
	and	r1, r1, #1
	bic	r2, r2, #2
	orr	r2, r2, r1, asl #1
	strb	r2, [r3, #483]
	ldrb	r2, [r3, #484]	@ zero_extendqisi2
	ldrb	r1, [r3, #483]	@ zero_extendqisi2
	mov	r2, r2, asl #1
	bic	r1, r1, #4
	and	r2, r2, #4
	orr	r2, r2, r1
	strb	r2, [r3, #483]
	ldrb	r2, [r3, #484]	@ zero_extendqisi2
	ldrb	r1, [r3, #483]	@ zero_extendqisi2
	mov	r2, r2, asl #1
	bic	r1, r1, #8
	and	r2, r2, #8
	orr	r2, r2, r1
	strb	r2, [r3, #483]
.L625:
	add	r5, r5, #1
	add	r4, r4, #14208
	cmp	r5, #4
	add	r4, r4, #16
	bne	.L651
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L662:
	.align	2
.L661:
	.word	system_preserves
	.word	14224
	.word	.LC41
	.word	.LC42
	.word	.LC43
.LFE50:
	.size	irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars, .-irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars
	.section	.text.irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON,"ax",%progbits
	.align	2
	.global	irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON
	.type	irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON, %function
irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON:
.LFB51:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI43:
	ldr	r0, .L674
	bl	nm_ListGetFirst
.LBB62:
.LBB63:
	ldr	r4, .L674+4
.LBE63:
.LBE62:
	mov	r5, r0
	b	.L672
.L669:
.LBB65:
	ldrb	r3, [r5, #74]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L665
.LBB64:
	ldrh	r7, [r5, #72]
	ldr	r6, [r5, #40]
	mov	r0, r7, lsr #6
	and	r0, r0, #15
	ldr	r1, [r6, #180]
	bl	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list
	cmp	r0, #1
	beq	.L671
	and	r7, r7, #960
	cmp	r7, #448
	bne	.L667
	ldr	r3, [r6, #144]
	cmp	r3, #1
	bne	.L665
	ldrb	r3, [r5, #73]	@ zero_extendqisi2
	tst	r3, #4
	beq	.L671
	b	.L665
.L667:
	ldrb	r3, [r5, #77]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L665
	ldr	r3, [r4, #136]
	cmp	r3, #0
	movne	r2, #14
	bne	.L666
	b	.L665
.L671:
	mov	r2, #12
.L666:
	mov	r1, r5
	ldr	r0, .L674
	bl	nm_nm_FOAL_if_station_is_ON_turn_it_OFF
	b	.L673
.L665:
.LBE64:
	ldr	r0, .L674
	mov	r1, r5
	bl	nm_ListGetNext
.L673:
	mov	r5, r0
.L672:
.LBE65:
	cmp	r5, #0
	bne	.L669
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L675:
	.align	2
.L674:
	.word	foal_irri+16
	.word	foal_irri
.LFE51:
	.size	irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON, .-irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON
	.section	.text.irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON,"ax",%progbits
	.align	2
	.global	irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON
	.type	irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON, %function
irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON:
.LFB52:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI44:
.LBB76:
	ldr	r5, .L756
.LBE76:
	sub	sp, sp, #32
.LCFI45:
	bl	MOISTURE_SENSORS_check_each_sensor_to_possibly_update__saved__control_variables
	bl	MOISTURE_SENSORS_check_each_sensor_for_a_crossed_threshold
	ldr	r0, .L756+4
	bl	nm_ListGetFirst
	mov	r4, r0
	b	.L747
.L679:
.LBB83:
	mov	r1, r4
	ldr	r0, .L756+4
	bl	nm_ListGetNext
	mov	r4, r0
.L747:
	cmp	r4, #0
	beq	.L676
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	tst	r3, #64
	bne	.L679
	ldr	r3, [r4, #60]
	cmp	r3, #0
	bne	.L679
	ldrh	r6, [r4, #72]
	and	r6, r6, #960
	subs	sl, r6, #384
	movne	sl, #1
	cmp	r6, #448
	moveq	r8, #0
	andne	r8, sl, #1
	cmp	r8, #0
	beq	.L680
	ldr	r3, [r4, #40]
	ldr	r3, [r3, #212]
	cmp	r3, #0
	bne	.L679
.L680:
	cmp	r6, #448
	bne	.L681
	ldrb	r3, [r4, #75]	@ zero_extendqisi2
	tst	r3, #4
	bne	.L679
.L681:
	ldrh	r0, [r4, #72]
	ldr	r7, [r4, #40]
	mov	r0, r0, lsr #6
	and	r0, r0, #15
	ldr	r1, [r7, #180]
	bl	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list
	cmp	r0, #1
	bne	.L682
	cmp	r6, #448
	cmpne	r6, #384
	bne	.L679
	ldr	r0, .L756+4
	mov	r1, r4
	mov	r2, #21
	b	.L752
.L682:
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	tst	r3, #32
	beq	.L684
	ldr	r3, [r7, #104]
	cmp	r3, #1
	beq	.L679
.L684:
	ldrb	r3, [r4, #75]	@ zero_extendqisi2
	tst	r3, #1
	beq	.L685
	cmp	sl, #0
	beq	.L685
	add	r0, r4, #72
	add	r1, r7, #192
	bl	FOAL_IRRI_there_are_other_flow_check_groups_already_ON
	cmp	r0, #0
	bne	.L679
.L685:
	cmp	r8, #0
	beq	.L686
.LBB77:
	ldr	r0, [r7, #0]
	bl	SYSTEM_get_capacity_in_use_bool
	cmp	r0, #1
	bne	.L686
.LBB78:
	ldr	r3, [r4, #40]
	ldr	r2, [r3, #172]
	cmp	r2, #1
	beq	.L688
	ldrb	r6, [r4, #73]	@ zero_extendqisi2
	ands	r6, r6, #8
	beq	.L689
.L688:
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_capacity_with_pump_gpm
	mov	r6, #1
	b	.L690
.L689:
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_capacity_without_pump_gpm
.L690:
	ldr	r3, [r4, #40]
	ldr	r3, [r3, #92]
	cmp	r3, #0
	bne	.L691
	ldrh	r3, [r4, #80]
	cmp	r3, r0
	bls	.L691
	ldrb	r1, [r4, #90]	@ zero_extendqisi2
	add	r2, sp, #24
	mov	r3, #8
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	cmp	r6, #1
	ldreq	r0, .L756+8
	ldrne	r0, .L756+12
	add	r1, sp, #24
	bl	Alert_Message_va
	ldrh	r0, [r4, #80]
.L691:
	ldr	r3, [r4, #40]
	ldrh	r2, [r4, #80]
	ldr	r3, [r3, #100]
	add	r3, r2, r3
	cmp	r3, r0
	bhi	.L679
.L686:
.LBE78:
.LBE77:
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #384
	cmpne	r3, #448
	beq	.L694
	cmp	r3, #320
	beq	.L694
	ldr	r3, [r4, #40]
	ldr	r2, [r3, #156]
	cmp	r2, #1
	beq	.L679
	ldrb	r2, [r4, #73]	@ zero_extendqisi2
	tst	r2, #8
	ldr	r2, [r3, #96]
	beq	.L695
	cmp	r2, #0
	beq	.L679
	ldr	r2, [r3, #92]
	cmp	r2, #0
	beq	.L694
	ldr	r3, [r3, #172]
	cmp	r3, #0
	b	.L748
.L695:
	cmp	r2, #1
	beq	.L679
	ldr	r3, [r3, #172]
	cmp	r3, #1
.L748:
	beq	.L679
.L694:
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	add	r3, r0, #20
	ldr	r6, [r5, r3, asl #2]
	bl	NETWORK_CONFIG_get_electrical_limit
	cmp	r6, r0
	bcc	.L696
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #448
	cmpne	r3, #384
	bne	.L679
	ldr	r0, .L756+4
	mov	r1, r4
	mov	r2, #22
	b	.L752
.L696:
	ldr	r3, [r5, #136]
	cmp	r3, #0
	beq	.L697
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	tst	r3, #16
	beq	.L697
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L679
	ldr	r2, [r4, #44]
	ldr	r3, .L756+16
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #38]	@ zero_extendqisi2
	orr	r2, r2, #64
	strb	r2, [r3, #38]
	b	.L679
.L697:
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #384
	cmpne	r3, #448
	beq	.L698
	cmp	r3, #320
	beq	.L699
	ldr	r0, [r4, #40]
	ldrh	r1, [r4, #86]
	bl	SYSTEM_PRESERVES_is_this_valve_allowed_ON
	cmp	r0, #0
	beq	.L679
	ldrh	r0, [r4, #86]
	cmp	r0, #0
	beq	.L700
	bl	ON_AT_A_TIME_can_another_valve_come_ON_within_this_group
	cmp	r0, #0
	beq	.L679
	b	.L700
.L698:
	cmp	r3, #320
	bne	.L700
.L699:
	ldr	r3, .L756+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L679
.L700:
	ldr	r6, [r4, #40]
	ldr	r3, [r6, #132]
	cmp	r3, #1
	beq	.L701
	ldr	r3, [r6, #140]
	cmp	r3, #1
	bne	.L702
.L701:
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #384
	bne	.L679
	ldr	r0, .L756+4
	mov	r1, r4
	mov	r2, #23
.L752:
	bl	nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_all_irrigation_lists
	mov	r6, r0
	b	.L683
.L702:
	ldr	r3, [r6, #136]
	cmp	r3, #1
	beq	.L703
	ldr	r3, [r6, #144]
	cmp	r3, #1
	bne	.L704
.L703:
	ldrh	r7, [r4, #72]
	ldr	r1, [r6, #180]
	mov	r7, r7, lsr #6
	and	r7, r7, #15
	mov	r0, r7
	bl	FOAL_IRRI_for_turn_ON_rules__there_are_higher_reasons_in_the_list
	cmp	r0, #1
	beq	.L679
	ldr	r3, [r6, #128]
	cmp	r3, r7
	bne	.L705
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	tst	r3, #4
	beq	.L679
.L705:
	ldr	r3, [r6, #92]
	cmp	r3, #0
	bne	.L679
.L704:
	ldr	r0, .L756+24
	bl	nm_ListGetFirst
	mov	r6, #0
	mov	r1, r0
	b	.L706
.L708:
	ldrh	r2, [r1, #90]
	ldrh	r3, [r4, #90]
	ldr	r0, .L756+24
	cmp	r2, r3
	moveq	r6, #1
	bl	nm_ListGetNext
	mov	r1, r0
.L706:
	cmp	r1, #0
	bne	.L708
	cmp	r6, #1
	beq	.L679
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L709
	mov	r0, r4
	bl	MOISTURE_SENSORS_this_ilc_has_irrigated_the_required_cycles
	cmp	r0, #0
	bne	.L679
.L709:
.LBB79:
	ldrb	r2, [r4, #73]	@ zero_extendqisi2
	mov	r3, #0
	tst	r2, #4
	str	r3, [r4, #48]
	ldrh	r2, [r4, #72]
	beq	.L710
	and	r2, r2, #960
	cmp	r2, #448
	ldreq	r2, [r4, #52]
	streq	r2, [r4, #48]
	beq	.L750
	cmp	r2, #384
	mov	r2, #360
	str	r2, [r4, #48]
	bne	.L712
	b	.L750
.L710:
	mov	r3, r2, lsr #6
	and	r3, r3, #15
	sub	r3, r3, #1
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L712
.L720:
	.word	.L714
	.word	.L714
	.word	.L712
	.word	.L714
	.word	.L717
	.word	.L717
	.word	.L717
.L717:
	ldr	r3, [r4, #52]
	b	.L723
.L714:
	ldr	r2, [r4, #56]
	ldr	r3, [r4, #52]
	cmp	r3, r2
	strhi	r2, [r4, #48]
	rsbhi	r3, r2, r3
	bhi	.L750
.L723:
	str	r3, [r4, #48]
	mov	r3, #0
.L750:
	str	r3, [r4, #52]
.L712:
.LBE79:
	ldr	r3, [r4, #48]
	cmp	r3, #0
	ble	.L724
.LBB80:
	ldr	r3, [r4, #40]
	ldr	r2, [r3, #92]
	cmp	r2, #0
	beq	.L725
	ldr	r2, [r3, #172]
	cmp	r2, #0
	ldrb	r2, [r4, #73]	@ zero_extendqisi2
	bne	.L726
	tst	r2, #8
	beq	.L725
	b	.L751
.L726:
	tst	r2, #8
	bne	.L725
.L751:
	mov	r2, #1
	str	r2, [r3, #156]
.L725:
	ldrh	r6, [r4, #72]
	and	r6, r6, #960
	cmp	r6, #64
	bne	.L727
	ldr	r2, [r4, #44]
	ldr	r3, .L756+16
	add	r3, r3, r2, asl #7
	ldrb	r6, [r3, #69]	@ zero_extendqisi2
	cmp	r6, #0
	movne	r6, #2
	moveq	r6, #1
	b	.L728
.L727:
	cmp	r6, #256
	movne	r6, #1
	moveq	r6, #2
.L728:
.LBB81:
	ldr	r0, .L756+4
	mov	r1, r4
	bl	nm_OnList
	cmp	r0, #0
	bne	.L729
	ldr	r0, .L756+28
	ldr	r1, .L756+32
	ldr	r2, .L756+36
	ldr	r3, .L756+40
	bl	Alert_item_not_on_list_with_filename
	b	.L730
.L729:
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	mov	r1, #0
	orr	r3, r3, #64
	strb	r3, [r4, #74]
	ldr	r3, .L756+44
	mov	r2, #11
	str	r3, [sp, #4]
	ldr	r3, .L756+36
	add	r0, r4, #91
	str	r3, [sp, #8]
	ldr	r3, .L756+48
	str	r1, [sp, #0]
	str	r3, [sp, #12]
	mov	r3, r1
	bl	RC_uns8_with_filename
	ldrb	r3, [r4, #91]	@ zero_extendqisi2
	ldr	r0, .L756+24
	add	r3, r3, #20
	ldr	r2, [r5, r3, asl #2]
	mov	r1, r4
	add	r2, r2, #1
	str	r2, [r5, r3, asl #2]
	bl	nm_ListInsertTail
	cmp	r0, #0
	beq	.L731
	ldrb	r1, [r4, #91]	@ zero_extendqisi2
	ldrb	r2, [r4, #90]	@ zero_extendqisi2
	ldr	r0, .L756+52
	add	r1, r1, #65
	add	r2, r2, #1
	bl	Alert_Message_va
.L731:
	ldr	r3, [r4, #40]
	ldrh	r2, [r4, #82]
	ldr	r1, [r3, #208]
	cmp	r1, r2
	strcc	r2, [r3, #208]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #64
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #464]	@ zero_extendqisi2
	bic	r2, r2, #128
	strb	r2, [r3, #464]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #1
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #2
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #4
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #8
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #16
	strb	r2, [r3, #465]
	ldr	r3, [r4, #40]
	ldrb	r2, [r3, #465]	@ zero_extendqisi2
	bic	r2, r2, #32
	strb	r2, [r3, #465]
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #64
	bne	.L730
	ldr	r7, .L756+16
	ldr	r3, [r4, #44]
	add	r3, r7, r3, asl #7
	ldrb	r3, [r3, #69]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L733
.LBB82:
	add	r0, sp, #24
	bl	EPSON_obtain_latest_time_and_date
	ldr	r3, [r4, #44]
	add	r7, r7, r3, asl #7
	ldrh	r3, [sp, #28]
	strh	r3, [r7, #46]	@ movhi
	ldr	r3, [sp, #24]
	str	r3, [r7, #20]
.L733:
.LBE82:
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	tst	r3, #32
	bne	.L734
	ldr	r2, [r4, #44]
	ldr	r3, .L756+16
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #69]	@ zero_extendqisi2
	add	r2, r2, #1
	strb	r2, [r3, #69]
.L734:
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	tst	r3, #4
	bne	.L730
	ldr	r2, [r4, #44]
	ldr	r3, .L756+16
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #141]	@ zero_extendqisi2
	bic	r2, r2, #16
	strb	r2, [r3, #141]
.L730:
.LBE81:
	mov	r0, r4
	mov	r1, r6
	bl	nm_FOAL_add_to_action_needed_list
	ldr	r0, [r4, #40]
	ldrh	r3, [r4, #80]
	ldr	r2, [r0, #100]
	add	r3, r2, r3
	str	r3, [r0, #100]
	ldrb	r3, [r4, #74]	@ zero_extendqisi2
	mov	r3, r3, lsr #2
	and	r3, r3, #3
	add	r3, r3, #48
	ldr	r2, [r0, r3, asl #2]
	add	r2, r2, #1
	str	r2, [r0, r3, asl #2]
	ldr	r3, [r0, #92]
	add	r3, r3, #1
	str	r3, [r0, #92]
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #448
	cmpne	r3, #384
	beq	.L735
	ldrh	r1, [r4, #86]
	bl	SYSTEM_PRESERVES_merge_new_allowed_ON_into_the_limit
	ldrh	r0, [r4, #86]
	bl	ON_AT_A_TIME_bump_this_groups_ON_count
.L735:
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #320
	ldreq	r3, .L756+20
	moveq	r2, #1
	streq	r2, [r3, #0]
	beq	.L737
.L736:
	cmp	r3, #384
	ldreq	r3, [r4, #40]
	ldreq	r2, [r3, #168]
	addeq	r2, r2, #1
	streq	r2, [r3, #168]
.L737:
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	tst	r3, #32
	ldrne	r3, [r4, #40]
	movne	r2, #1
	strne	r2, [r3, #104]
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	mov	r2, #1
	tst	r3, #8
	ldr	r3, [r4, #40]
	strne	r2, [r3, #172]
	streq	r2, [r3, #176]
	ldrb	r3, [r4, #73]	@ zero_extendqisi2
	tst	r3, #4
	beq	.L741
	ldrh	r3, [r4, #72]
	mov	r2, #1
	and	r3, r3, #960
	cmp	r3, #448
	ldr	r3, [r4, #40]
	streq	r2, [r3, #140]
	strne	r2, [r3, #132]
.L741:
	ldrh	r3, [r4, #72]
	and	r3, r3, #960
	cmp	r3, #448
	bne	.L679
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	ldrb	r1, [r4, #90]	@ zero_extendqisi2
	bl	Alert_mobile_station_on
	mov	r1, #0
	ldr	r0, .L756+56
	mov	r2, #512
	mov	r3, r1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
	b	.L679
.L724:
.LBE80:
	add	r2, sp, #16
	mov	r3, #8
	ldrb	r1, [r4, #90]	@ zero_extendqisi2
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	bl	ALERTS_get_station_number_with_or_without_two_wire_indicator
	ldr	r0, .L756+60
	add	r1, sp, #16
	bl	Alert_Message_va
	mov	r0, r4
	mov	r1, #24
	bl	nm_FOAL_add_to_action_needed_list
	mov	r1, r4
	ldr	r0, .L756+4
	bl	nm_ListGetNext
	mov	r1, r4
	ldr	r2, .L756+36
	ldr	r3, .L756+64
	mov	r6, r0
	ldr	r0, .L756+4
	bl	nm_ListRemove_debug
.L683:
.LBE83:
	mov	r4, r6
	b	.L747
.L676:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L757:
	.align	2
.L756:
	.word	foal_irri
	.word	foal_irri+16
	.word	.LC44
	.word	.LC45
	.word	station_preserves
	.word	.LANCHOR1
	.word	foal_irri+36
	.word	.LC11
	.word	.LC12
	.word	.LC1
	.word	6114
	.word	.LC13
	.word	6130
	.word	.LC46
	.word	413
	.word	.LC47
	.word	8675
.LFE52:
	.size	irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON, .-irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON
	.section	.text.FOAL_IRRI_maintain_irrigation_list,"ax",%progbits
	.align	2
	.global	FOAL_IRRI_maintain_irrigation_list
	.type	FOAL_IRRI_maintain_irrigation_list, %function
FOAL_IRRI_maintain_irrigation_list:
.LFB53:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
.LBB95:
	ldr	r3, .L786
.LBE95:
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI46:
.LBB96:
	ldr	r6, .L786+4
	ldr	r8, .L786+8
.LBE96:
	mov	r7, r0
.LBB97:
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L786+12
	ldr	r3, .L786+16
	bl	xQueueTakeMutexRecursive_debug
.LBE97:
	add	r3, r6, #16
.L764:
.LBB98:
	ldr	r2, [r3, #0]
	cmp	r2, #0
	beq	.L759
	ldr	r2, [r3, #456]
	cmp	r2, #0
	subne	r2, r2, #1
	strne	r2, [r3, #456]
	ldr	r2, [r3, #460]
	cmp	r2, #0
	subne	r2, r2, #1
	strne	r2, [r3, #460]
	ldr	r2, [r3, #208]
	cmp	r2, #0
	subne	r2, r2, #1
	strne	r2, [r3, #208]
	ldr	r2, [r3, #212]
	cmp	r2, #0
	subne	r2, r2, #1
	strne	r2, [r3, #212]
	ldr	r2, [r3, #444]
	cmp	r2, #0
	subne	r2, r2, #1
	strne	r2, [r3, #444]
.L759:
	add	r3, r3, #14208
	add	r3, r3, #16
	cmp	r3, r8
	bne	.L764
	ldr	r4, .L786
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
.LBE98:
	ldr	r3, .L786+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L786+12
	ldr	r3, .L786+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r4, #0]
	ldr	r4, .L786+28
	ldr	r3, .L786+32
	mov	r1, #400
	ldr	r2, .L786+12
	bl	xQueueTakeMutexRecursive_debug
.LBB99:
	ldr	r5, [r4, #64]
.LBE99:
	mov	r3, #0
.LBB100:
	cmp	r5, r3
.LBE100:
	str	r3, [r4, #76]
.LBB101:
	bne	.L765
.LBE101:
	bl	SYSTEM_PRESERVES_init_system_preserves_ufim_variables_for_all_systems
	bl	ON_AT_A_TIME_init_all_groups_ON_count
	bl	MOISTURE_SENSORS_initialize__transient__control_variables_for_all_sensors
	ldr	r3, .L786+36
	mov	r0, r7
	str	r5, [r3, #0]
	bl	irrigation_maintenance_FIRST__full_list_pass_to_develop_ufim_vars_and_remove_completed_stations
.LBB102:
	ldr	r3, [r4, #64]
	cmp	r3, #0
	bne	.L766
.LBE102:
	bl	irrigation_maintenance_SECOND__pass_through_all_systems_to_develop_more_ufim_vars
	bl	irrigation_maintenance_THIRD__full_station_list_pass_to_turn_off_stations_that_should_not_be_ON_based_upon_those_trying_to_come_ON
.L766:
.LBB103:
	ldr	r3, .L786+28
	ldr	r4, [r3, #64]
	cmp	r4, #0
	bne	.L765
.LBE103:
	bl	irrigation_maintenance_FOURTH__full_station_list_pass_to_hunt_for_stations_to_turn_ON
	ldr	r5, .L786+40
	ldr	sl, .L786+4
	ldr	r9, .L786+44
.L769:
	mla	r3, r9, r4, sl
	ldrb	r3, [r3, #482]	@ zero_extendqisi2
	tst	r3, #8
	bne	.L767
	tst	r3, #16
	beq	.L768
.L767:
	ldr	r3, [r5, #0]
	mov	r0, r7
	strh	r3, [sp, #4]	@ movhi
	ldr	r3, [r5, #4]
	mov	r1, sp
	str	r3, [sp, #0]
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #0
	beq	.L768
	ldr	r0, [r6, #16]
	mov	r1, #2
	mov	r2, #0
	mov	r3, #4
	bl	FOAL_IRRI_initiate_or_cancel_a_master_valve_override
.L768:
	add	r4, r4, #1
	add	r5, r5, #14208
	add	r6, r6, #14208
	cmp	r4, #4
	add	r5, r5, #16
	add	r6, r6, #16
	bne	.L769
.LBB104:
	ldr	r3, .L786
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L786+12
	ldr	r3, .L786+48
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, .L786+52
.LBB105:
	mov	r5, #150
	mov	r6, #3
.L781:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L770
	ldr	r3, [r4, #92]
	cmp	r3, #0
	beq	.L771
	ldrb	r3, [r4, #464]	@ zero_extendqisi2
	str	r6, [r4, #448]
	tst	r3, #32
	str	r5, [r4, #460]
	bne	.L772
	tst	r3, #16
	ldrb	r3, [r4, #464]	@ zero_extendqisi2
	strne	r5, [r4, #456]
	orr	r3, r3, #32
	strb	r3, [r4, #464]
.L772:
	ldr	r3, [r4, #172]
	cmp	r3, #1
	ldreqb	r3, [r4, #464]	@ zero_extendqisi2
	streq	r6, [r4, #452]
	orreq	r3, r3, #64
	streqb	r3, [r4, #464]
	beq	.L775
.L774:
	ldr	r3, [r4, #452]
	cmp	r3, #0
	subne	r3, r3, #1
	strne	r3, [r4, #452]
	ldr	r3, [r4, #452]
	cmp	r3, #0
	bne	.L775
	b	.L785
.L771:
	ldr	r3, [r4, #212]
	cmp	r3, #0
	strne	r5, [r4, #460]
	bne	.L779
	ldr	r3, [r4, #448]
	cmp	r3, #0
	beq	.L778
	sub	r3, r3, #1
	cmp	r3, #0
	str	r3, [r4, #448]
	ldreqb	r3, [r4, #464]	@ zero_extendqisi2
	str	r5, [r4, #460]
	biceq	r3, r3, #32
	streqb	r3, [r4, #464]
.L778:
	ldr	r3, [r4, #452]
	cmp	r3, #0
	beq	.L779
	sub	r3, r3, #1
	cmp	r3, #0
	str	r3, [r4, #452]
	ldreqb	r3, [r4, #464]	@ zero_extendqisi2
	biceq	r3, r3, #64
	streqb	r3, [r4, #464]
.L779:
	ldr	r3, [r4, #448]
	cmp	r3, #0
	bne	.L780
	ldrb	r3, [r4, #464]	@ zero_extendqisi2
	tst	r3, #32
	beq	.L780
	ldr	r0, .L786+12
	bl	RemovePathFromFileName
	ldr	r2, .L786+56
	mov	r1, r0
	ldr	r0, .L786+60
	bl	Alert_Message_va
	ldrb	r3, [r4, #464]	@ zero_extendqisi2
	bic	r3, r3, #32
	strb	r3, [r4, #464]
.L780:
	ldr	r3, [r4, #452]
	cmp	r3, #0
	bne	.L775
	ldrb	r3, [r4, #464]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L775
	ldr	r0, .L786+12
	bl	RemovePathFromFileName
	ldr	r2, .L786+64
	mov	r1, r0
	ldr	r0, .L786+60
	bl	Alert_Message_va
.L785:
	ldrb	r3, [r4, #464]	@ zero_extendqisi2
	bic	r3, r3, #64
	strb	r3, [r4, #464]
.L775:
	ldrb	r3, [r4, #464]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L770
	tst	r3, #32
	bne	.L770
	ldr	r0, .L786+12
	bl	RemovePathFromFileName
	ldr	r2, .L786+68
	mov	r1, r0
	ldr	r0, .L786+72
	bl	Alert_Message_va
	ldrb	r3, [r4, #464]	@ zero_extendqisi2
	bic	r3, r3, #64
	strb	r3, [r4, #464]
.L770:
.LBE105:
	add	r4, r4, #14208
	add	r4, r4, #16
	cmp	r4, r8
	bne	.L781
	ldr	r3, .L786
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L765:
.LBE104:
	ldr	r3, .L786
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L786+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, pc}
.L787:
	.align	2
.L786:
	.word	system_preserves_recursive_MUTEX
	.word	system_preserves
	.word	system_preserves+56912
	.word	.LC1
	.word	6830
	.word	list_foal_irri_recursive_MUTEX
	.word	8715
	.word	foal_irri
	.word	8720
	.word	.LANCHOR1
	.word	system_preserves+14132
	.word	14224
	.word	7058
	.word	system_preserves+16
	.word	7018
	.word	.LC48
	.word	7027
	.word	7041
	.word	.LC49
.LFE53:
	.size	FOAL_IRRI_maintain_irrigation_list, .-FOAL_IRRI_maintain_irrigation_list
	.global	one_ON_for_walk_thru
	.global	__largest_start_time_delta
	.global	last_time_a_valve_was_turned_off
	.section	.bss.last_time_a_valve_was_turned_off,"aw",%nobits
	.type	last_time_a_valve_was_turned_off, %object
	.size	last_time_a_valve_was_turned_off, 6
last_time_a_valve_was_turned_off:
	.space	6
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Cycle=0 (out of range)\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/foal_irri.c\000"
.LC2:
	.ascii	"FOAL: dirty list item. : %s, %u\000"
.LC3:
	.ascii	"FOAL: Station Skipped (list full)\000"
.LC4:
	.ascii	"FOAL main list must have changed : %s, %u\000"
.LC5:
	.ascii	"FOAL_IRRI: token info error : %s, %u\000"
.LC6:
	.ascii	"Couldn't remove from irri list to reinsert\000"
.LC7:
	.ascii	"FOAL_IRRI: couldn't find no current station?\000"
.LC8:
	.ascii	"MVOR: System GID should not be 0 (%d)\000"
.LC9:
	.ascii	"FOAL: action_needed token error. : %s, %u\000"
.LC10:
	.ascii	"Already on action needed list: for %u, changing to "
	.ascii	"%u, count=%u\000"
.LC11:
	.ascii	"pilc_ptr\000"
.LC12:
	.ascii	"foal_irri.list_of_foal_all_irrigation\000"
.LC13:
	.ascii	"Box Index\000"
.LC14:
	.ascii	"FOAL_IRRI: expected count is 0. : %s, %u\000"
.LC15:
	.ascii	"FOAL: error removing from ON list : %s, %u\000"
.LC16:
	.ascii	"FOAL: should've been on the ON list. : %s, %u\000"
.LC17:
	.ascii	"FOAL_IRRI: number_of_valves_ON unexpectedly zero.\000"
.LC18:
	.ascii	"FOAL_IRRI: system expected flow rate out of range.\000"
.LC19:
	.ascii	"FOAL_IRRI: number_ON_for_test unexpectedly zero.\000"
.LC20:
	.ascii	"FOAL_IRRI: flow group count unexpectedly zero.\000"
.LC21:
	.ascii	"Slow closing delay ignored: pump running with no va"
	.ascii	"lves ON.\000"
.LC22:
	.ascii	"FOAL: unexpectedly on ON list. : %s, %u\000"
.LC23:
	.ascii	"Error removing from list : %s, %u\000"
.LC24:
	.ascii	"Foal Irri\000"
.LC25:
	.ascii	"rain switch timer\000"
.LC26:
	.ascii	"Timer NOT CREATED : %s, %u\000"
.LC27:
	.ascii	"freeze switch timer\000"
.LC28:
	.ascii	"FOAL: problem...can't add to list. : %s, %u\000"
.LC29:
	.ascii	"FOAL_IRRI: ilc not available : %s, %u\000"
.LC30:
	.ascii	"WALK THRU: station not found : %s, %u\000"
.LC31:
	.ascii	"WALK THRU: ilc not available : %s, %u\000"
.LC32:
	.ascii	"FOAL_IRRI: sta not found : %s, %u\000"
.LC33:
	.ascii	"Foal Irri SX: error removing\000"
.LC34:
	.ascii	"Programmed Irrigation: NO irrigation due to specifi"
	.ascii	"ed reason(s)\000"
.LC35:
	.ascii	"Manual Program: no irrigation due to specified reas"
	.ascii	"on(s)\000"
.LC36:
	.ascii	"FOAL_IRRI: couldn't find one in the list\000"
.LC37:
	.ascii	"FOAL_IRRI: couldn't find failed station?\000"
.LC38:
	.ascii	"ilc\000"
.LC39:
	.ascii	"foal_irri.list_of_foal_stations_ON\000"
.LC40:
	.ascii	"Is OFF but on the ON list! : %s, %u\000"
.LC41:
	.ascii	"FOAL_IRRI: expected flow rate should be zero.\000"
.LC42:
	.ascii	"FOAL_IRRI: number ON during TEST should be zero.\000"
.LC43:
	.ascii	"FOAL_IRRI: mix of flow groups ON.\000"
.LC44:
	.ascii	"Pump Capacity Override: Station %s expected is high"
	.ascii	"er.\000"
.LC45:
	.ascii	"Non-Pump Capacity Override: Station %s expected is "
	.ascii	"higher.\000"
.LC46:
	.ascii	"Controller: %u, sta %u : not added to ON list\000"
.LC47:
	.ascii	"Station %s Skipped: (0 time)\000"
.LC48:
	.ascii	"FOAL_IRRI: unexp condition : %s, %u\000"
.LC49:
	.ascii	"FOAL_IRRI: unexp pump setting! : %s, %u\000"
	.section	.bss.__largest_start_time_delta,"aw",%nobits
	.align	2
	.type	__largest_start_time_delta, %object
	.size	__largest_start_time_delta, 4
__largest_start_time_delta:
	.space	4
	.section	.bss.one_ON_for_walk_thru,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	one_ON_for_walk_thru, %object
	.size	one_ON_for_walk_thru, 4
one_ON_for_walk_thru:
	.space	4
	.section	.rodata.FI_PRE_TEST_STRING,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	FI_PRE_TEST_STRING, %object
	.size	FI_PRE_TEST_STRING, 14
FI_PRE_TEST_STRING:
	.ascii	"foal_irri v04\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI0-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI1-.LFB9
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI2-.LFB26
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI3-.LFB29
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI6-.LFB10
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI7-.LFB12
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI8-.LFB21
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI9-.LFB22
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI10-.LFB23
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI11-.LFB35
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x60
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI13-.LFB37
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x83
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI14-.LFB43
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI15-.LFB11
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI17-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI18-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI19-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI20-.LFB14
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x83
	.uleb128 0x8
	.byte	0x82
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI21-.LFB27
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI24-.LFB34
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI26-.LCFI25
	.byte	0xe
	.uleb128 0xb0
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI27-.LFB33
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI28-.LCFI27
	.byte	0xe
	.uleb128 0x80
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI29-.LFB32
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xe
	.uleb128 0x78
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI31-.LFB31
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI32-.LCFI31
	.byte	0xe
	.uleb128 0x44
	.byte	0x5
	.uleb128 0x56
	.uleb128 0xb
	.byte	0x5
	.uleb128 0x54
	.uleb128 0xd
	.byte	0x5
	.uleb128 0x52
	.uleb128 0xf
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x11
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xe
	.uleb128 0x1c4
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI34-.LFB20
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI35-.LFB18
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI36-.LFB19
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI37-.LFB17
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI38-.LFB16
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI39-.LFB15
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.byte	0x4
	.4byte	.LCFI40-.LFB49
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI41-.LCFI40
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.byte	0x4
	.4byte	.LCFI42-.LFB50
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.byte	0x4
	.4byte	.LCFI43-.LFB51
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.byte	0x4
	.4byte	.LCFI44-.LFB52
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI45-.LCFI44
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.byte	0x4
	.4byte	.LCFI46-.LFB53
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE84:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/foal_irri.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x400
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF53
	.byte	0x1
	.4byte	.LASF54
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0xef1
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF55
	.byte	0x1
	.2byte	0x421
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x1a6a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x1a7c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x1b8a
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x297
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x23f
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x303
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x3ea
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	.LASF8
	.byte	0x1
	.2byte	0xb9a
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST2
	.uleb128 0x6
	.4byte	.LASF9
	.byte	0x1
	.byte	0x55
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF10
	.byte	0x1
	.byte	0x4c
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.4byte	0x21
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST3
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1b4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x7
	.4byte	0x2a
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST5
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x69b
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST6
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0xa10
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST7
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0xa57
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST8
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0xad7
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST9
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0xb2a
	.4byte	.LFB24
	.4byte	.LFE24
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0xb62
	.4byte	.LFB25
	.4byte	.LFE25
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0xec0
	.4byte	.LFB28
	.4byte	.LFE28
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x1740
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST10
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x1872
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST11
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x19d9
	.4byte	.LFB40
	.4byte	.LFE40
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x19ed
	.4byte	.LFB41
	.4byte	.LFE41
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x1a09
	.4byte	.LFB42
	.4byte	.LFE42
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x1a3a
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST12
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x48a
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST13
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF26
	.byte	0x1
	.byte	0x75
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST14
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x151
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST15
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x126
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST16
	.uleb128 0x2
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x6c6
	.byte	0x1
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x758
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST17
	.uleb128 0x5
	.4byte	.LASF31
	.byte	0x1
	.2byte	0xc5c
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST18
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x167a
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST19
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x15fd
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST20
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x15a0
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST21
	.uleb128 0x2
	.4byte	.LASF35
	.byte	0x1
	.2byte	0xf41
	.byte	0x1
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF36
	.byte	0x1
	.2byte	0xfca
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST22
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x9b3
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST23
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x95c
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST24
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x98d
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST25
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x914
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST26
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x8b8
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST27
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x835
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST28
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x1baa
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LLST29
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x1de7
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LLST30
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF45
	.byte	0x1
	.2byte	0x1ee6
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LLST31
	.uleb128 0x2
	.4byte	.LASF46
	.byte	0x1
	.2byte	0x1994
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF47
	.byte	0x1
	.2byte	0x18ee
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF48
	.byte	0x1
	.2byte	0x17dd
	.byte	0x1
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF49
	.byte	0x1
	.2byte	0x1f5e
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LLST32
	.uleb128 0x2
	.4byte	.LASF50
	.byte	0x1
	.2byte	0x1aa6
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF51
	.byte	0x1
	.2byte	0x1af7
	.byte	0x1
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF52
	.byte	0x1
	.2byte	0x21fa
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LLST33
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB6
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB9
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB26
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB29
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB10
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB12
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB21
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB22
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB23
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB35
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI12
	.4byte	.LFE35
	.2byte	0x3
	.byte	0x7d
	.sleb128 96
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB37
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB43
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB11
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI16
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB2
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB4
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB3
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB14
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB27
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI23
	.4byte	.LFE27
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB34
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI25
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI26
	.4byte	.LFE34
	.2byte	0x3
	.byte	0x7d
	.sleb128 176
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB33
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI28
	.4byte	.LFE33
	.2byte	0x3
	.byte	0x7d
	.sleb128 128
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB32
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI30
	.4byte	.LFE32
	.2byte	0x3
	.byte	0x7d
	.sleb128 120
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB31
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	.LCFI33
	.4byte	.LFE31
	.2byte	0x3
	.byte	0x7d
	.sleb128 452
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB20
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB18
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB19
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB17
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB16
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB15
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB49
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI41
	.4byte	.LFE49
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB50
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI42
	.4byte	.LFE50
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB51
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LFE51
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB52
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI45
	.4byte	.LFE52
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB53
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LFE53
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x16c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF50:
	.ascii	"decrement_system_preserves_timers\000"
.LASF49:
	.ascii	"irrigation_maintenance_FOURTH__full_station_list_pa"
	.ascii	"ss_to_hunt_for_stations_to_turn_ON\000"
.LASF52:
	.ascii	"FOAL_IRRI_maintain_irrigation_list\000"
.LASF3:
	.ascii	"_load_and_maintain_system_anti_chatter_timers\000"
.LASF20:
	.ascii	"FOAL_IRRI_buildup_action_needed_records_for_token\000"
.LASF26:
	.ascii	"FOAL_IRRI_restart_irrigation_on_reboot_and_when_cha"
	.ascii	"in_goes_down\000"
.LASF33:
	.ascii	"FOAL_IRRI_start_a_walk_thru\000"
.LASF46:
	.ascii	"will_exceed_system_capacity\000"
.LASF27:
	.ascii	"init_battery_backed_foal_irri\000"
.LASF24:
	.ascii	"nm_FOAL_add_to_action_needed_list\000"
.LASF22:
	.ascii	"FOAL_IRRI_there_are_other_flow_check_groups_already"
	.ascii	"_ON\000"
.LASF7:
	.ascii	"nm_return_station_this_station_belongs_before_in_th"
	.ascii	"e_irrigation_list\000"
.LASF13:
	.ascii	"FOAL_IRRI_set_no_current_flag_in_station_history_li"
	.ascii	"ne_if_appropriate\000"
.LASF38:
	.ascii	"FOAL_IRRI_remove_all_stations_at_this_box_from_the_"
	.ascii	"irrigation_lists\000"
.LASF40:
	.ascii	"FOAL_IRRI_remove_this_station_for_this_reason_from_"
	.ascii	"the_irrigation_lists\000"
.LASF39:
	.ascii	"FOAL_IRRI_remove_all_stations_from_the_irrigation_l"
	.ascii	"ists\000"
.LASF4:
	.ascii	"_nm_pumpuse_priority_setexpected_flowchecking_irrig"
	.ascii	"atedlasttime_weighting\000"
.LASF14:
	.ascii	"FOAL_extract_clear_mlb_gids\000"
.LASF16:
	.ascii	"FOAL_IRRI_translate_alert_actions_to_flow_checking_"
	.ascii	"group\000"
.LASF9:
	.ascii	"freeze_switch_timer_callback\000"
.LASF31:
	.ascii	"_nm_nm_nm_add_to_the_irrigation_list\000"
.LASF6:
	.ascii	"nm_this_station_weighs_MORE_than_the_list_station\000"
.LASF44:
	.ascii	"irrigation_maintenance_SECOND__pass_through_all_sys"
	.ascii	"tems_to_develop_more_ufim_vars\000"
.LASF10:
	.ascii	"rain_switch_timer_callback\000"
.LASF55:
	.ascii	"FOAL_IRRI_remove_and_reinsert_into_foal_irri_irriga"
	.ascii	"tion_list\000"
.LASF37:
	.ascii	"FOAL_IRRI_remove_stations_in_this_system_from_the_i"
	.ascii	"rrigation_lists\000"
.LASF28:
	.ascii	"FOAL_IRRI_completely_wipe_both_foal_side_and_irri_s"
	.ascii	"ide_irrigation\000"
.LASF12:
	.ascii	"nm_shut_down_mvs_and_pmps_throughout_this_system\000"
.LASF34:
	.ascii	"FOAL_IRRI_initiate_a_station_test\000"
.LASF21:
	.ascii	"FOAL_IRRI_for_turn_ON_rules__there_are_higher_reaso"
	.ascii	"ns_in_the_list\000"
.LASF41:
	.ascii	"FOAL_IRRI_remove_all_stations_ON_at_this_box_from_t"
	.ascii	"he_irrigation_lists\000"
.LASF1:
	.ascii	"_if_supposed_to_but_did_not_ever_check_flow_make_an"
	.ascii	"_alert\000"
.LASF54:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/foal_irri.c\000"
.LASF32:
	.ascii	"FOAL_IRRI_initiate_manual_watering\000"
.LASF15:
	.ascii	"FOAL_IRRI_load_mlb_info_into_outgoing_token\000"
.LASF48:
	.ascii	"_nm_nm_foal_irri_complete_the_action_needed_turn_ON"
	.ascii	"\000"
.LASF5:
	.ascii	"foal_irri_cycle_and_soak_weighting\000"
.LASF42:
	.ascii	"FOAL_IRRI_remove_all_instances_of_this_station_from"
	.ascii	"_the_irrigation_lists\000"
.LASF18:
	.ascii	"FOAL_IRRI_return_stop_date_according_to_stop_time\000"
.LASF43:
	.ascii	"irrigation_maintenance_FIRST__full_list_pass_to_dev"
	.ascii	"elop_ufim_vars_and_remove_completed_stations\000"
.LASF29:
	.ascii	"set_station_history_flags_if_in_the_list_for_progra"
	.ascii	"mmed_irrigation\000"
.LASF19:
	.ascii	"FOAL_IRRI_initiate_or_cancel_a_master_valve_overrid"
	.ascii	"e\000"
.LASF45:
	.ascii	"irrigation_maintenance_THIRD__full_station_list_pas"
	.ascii	"s_to_turn_off_stations_that_should_not_be_ON_based_"
	.ascii	"upon_those_trying_to_come_ON\000"
.LASF35:
	.ascii	"nm_at_1Hz_rate_set_skip_irrigation_due_to_NOW_days_"
	.ascii	"and_check_for_isolated_stations\000"
.LASF51:
	.ascii	"_nm_anti_chatter_maintenance_sub_function\000"
.LASF25:
	.ascii	"nm_nm_FOAL_if_station_is_ON_turn_it_OFF\000"
.LASF8:
	.ascii	"nm_FOAL_return_pointer_to_next_available_and_fully_"
	.ascii	"cleaned_ilc\000"
.LASF53:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF36:
	.ascii	"TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_s"
	.ascii	"tart\000"
.LASF17:
	.ascii	"FOAL_IRRI_we_test_flow_when_ON_for_this_reason\000"
.LASF23:
	.ascii	"FOAL_IRRI_there_is_more_than_one_flow_group_ON\000"
.LASF2:
	.ascii	"foal_irri_maintenance_may_run\000"
.LASF30:
	.ascii	"nm_nm_FOAL_turn_OFF_this_ilc_if_ON_and_remove_from_"
	.ascii	"all_irrigation_lists\000"
.LASF0:
	.ascii	"nm_at_starttime_decrement_NOW_days_count\000"
.LASF11:
	.ascii	"FOAL_IRRI_load_sfml_for_distribution_to_slaves\000"
.LASF47:
	.ascii	"set_remaining_seconds_on\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
