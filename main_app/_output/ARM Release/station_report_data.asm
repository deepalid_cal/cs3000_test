	.file	"station_report_data.c"
	.text
.Ltext0:
	.section	.text.station_report_data_ci_timer_callback,"ax",%progbits
	.align	2
	.type	station_report_data_ci_timer_callback, %function
station_report_data_ci_timer_callback:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L2
	mov	r1, #0
	mov	r2, #512
	mov	r3, r1
	b	CONTROLLER_INITIATED_post_to_messages_queue
.L3:
	.align	2
.L2:
	.word	403
.LFE5:
	.size	station_report_data_ci_timer_callback, .-station_report_data_ci_timer_callback
	.section	.text.nm_init_station_report_data_record,"ax",%progbits
	.align	2
	.global	nm_init_station_report_data_record
	.type	nm_init_station_report_data_record, %function
nm_init_station_report_data_record:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, #0
	mov	r2, #48
	b	memset
.LFE0:
	.size	nm_init_station_report_data_record, .-nm_init_station_report_data_record
	.section	.text.nm_init_station_report_data_records,"ax",%progbits
	.align	2
	.type	nm_init_station_report_data_records, %function
nm_init_station_report_data_records:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r0, .L8
	mov	r1, #0
	mov	r2, #60
	bl	memset
	ldr	r6, .L8
	mov	r4, #0
	mov	r5, #48
.L6:
	mla	r0, r5, r4, r6
	add	r4, r4, #1
	add	r0, r0, #60
	bl	nm_init_station_report_data_record
	cmp	r4, #10752
	bne	.L6
	ldmfd	sp!, {r4, r5, r6, pc}
.L9:
	.align	2
.L8:
	.word	.LANCHOR0
.LFE1:
	.size	nm_init_station_report_data_records, .-nm_init_station_report_data_records
	.section	.text.nm_station_report_data_updater,"ax",%progbits
	.align	2
	.type	nm_station_report_data_updater, %function
nm_station_report_data_updater:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r4, r0
	bne	.L11
	ldr	r0, .L13
	mov	r1, r4
	ldmfd	sp!, {r4, lr}
	b	Alert_Message_va
.L11:
	ldr	r0, .L13+4
	mov	r1, #1
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	bne	.L12
	ldmfd	sp!, {r4, lr}
	b	nm_init_station_report_data_records
.L12:
	ldr	r0, .L13+8
	ldmfd	sp!, {r4, lr}
	b	Alert_Message
.L14:
	.align	2
.L13:
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE2:
	.size	nm_station_report_data_updater, .-nm_station_report_data_updater
	.section	.text.init_file_station_report_data,"ax",%progbits
	.align	2
	.global	init_file_station_report_data
	.type	init_file_station_report_data, %function
init_file_station_report_data:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L16
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r4, .L16+4
	sub	sp, sp, #28
.LCFI3:
	str	r3, [sp, #0]
	ldr	r3, .L16+8
	mov	r0, #1
	str	r3, [sp, #4]
	ldr	r3, .L16+12
	ldr	r1, .L16+16
	str	r3, [sp, #8]
	ldr	r3, .L16+20
	mov	r2, r0
	ldr	r3, [r3, #0]
	str	r3, [sp, #12]
	ldr	r3, .L16+24
	str	r3, [sp, #16]
	ldr	r3, .L16+28
	str	r3, [sp, #20]
	mov	r3, #4
	str	r3, [sp, #24]
	mov	r3, r4
	bl	FLASH_FILE_find_or_create_reports_file
	mov	r3, #0
	str	r3, [r4, #24]
	add	sp, sp, #28
	ldmfd	sp!, {r4, pc}
.L17:
	.align	2
.L16:
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR3
	.word	516156
	.word	.LANCHOR1
	.word	station_report_data_completed_records_recursive_MUTEX
	.word	nm_station_report_data_updater
	.word	nm_init_station_report_data_records
.LFE3:
	.size	init_file_station_report_data, .-init_file_station_report_data
	.section	.text.save_file_station_report_data,"ax",%progbits
	.align	2
	.global	save_file_station_report_data
	.type	save_file_station_report_data, %function
save_file_station_report_data:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L19
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI4:
	ldr	r1, .L19+4
	str	r3, [sp, #0]
	ldr	r3, .L19+8
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, r0
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	ldr	r3, .L19+12
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L20:
	.align	2
.L19:
	.word	516156
	.word	.LANCHOR1
	.word	station_report_data_completed_records_recursive_MUTEX
	.word	.LANCHOR0
.LFE4:
	.size	save_file_station_report_data, .-save_file_station_report_data
	.global	__udivsi3
	.section	.text.STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running,"ax",%progbits
	.align	2
	.global	STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	.type	STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running, %function
STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI5:
	ldr	r4, .L24
	ldr	r2, [r4, #0]
	cmp	r2, #0
	bne	.L22
	ldr	r3, .L24+4
	ldr	r0, .L24+8
	str	r3, [sp, #0]
	ldr	r1, .L24+12
	mov	r3, r2
	bl	xTimerCreate
	cmp	r0, #0
	str	r0, [r4, #0]
	bne	.L22
	ldr	r0, .L24+16
	bl	RemovePathFromFileName
	ldr	r2, .L24+20
	mov	r1, r0
	ldr	r0, .L24+24
	bl	Alert_Message_va
.L22:
	ldr	r5, .L24
	ldr	r0, [r5, #0]
	cmp	r0, #0
	beq	.L21
	bl	xTimerIsTimerActive
	subs	r4, r0, #0
	bne	.L21
	ldr	r3, .L24+28
	ldr	r5, [r5, #0]
	ldr	r0, [r3, #48]
	bl	CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, r4
	mov	r2, r0
	mov	r0, r5
	bl	xTimerGenericCommand
.L21:
	ldmfd	sp!, {r3, r4, r5, pc}
.L25:
	.align	2
.L24:
	.word	.LANCHOR4
	.word	station_report_data_ci_timer_callback
	.word	.LC3
	.word	12000
	.word	.LC4
	.word	290
	.word	.LC5
	.word	config_c
.LFE6:
	.size	STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running, .-STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	.section	.text.nm_STATION_REPORT_DATA_inc_index,"ax",%progbits
	.align	2
	.global	nm_STATION_REPORT_DATA_inc_index
	.type	nm_STATION_REPORT_DATA_inc_index, %function
nm_STATION_REPORT_DATA_inc_index:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #0]
	add	r3, r3, #1
	cmp	r3, #10752
	movcs	r3, #0
	str	r3, [r0, #0]
	bx	lr
.LFE7:
	.size	nm_STATION_REPORT_DATA_inc_index, .-nm_STATION_REPORT_DATA_inc_index
	.section	.text.nm_STATION_REPORT_DATA_get_previous_completed_record,"ax",%progbits
	.align	2
	.global	nm_STATION_REPORT_DATA_get_previous_completed_record
	.type	nm_STATION_REPORT_DATA_get_previous_completed_record, %function
nm_STATION_REPORT_DATA_get_previous_completed_record:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L37
	add	r2, r3, #60
	cmp	r0, r2
	bcc	.L35
	add	r1, r2, #516096
	cmp	r0, r1
	bcs	.L35
	ldr	r1, [r3, #12]
	cmp	r1, #1
	beq	.L35
	cmp	r0, r2
	subne	r0, r0, #48
	bne	.L30
	ldr	r0, [r3, #8]
	ldr	r3, .L37+4
	cmp	r0, #1
	moveq	r0, r3
	movne	r0, #0
	b	.L30
.L35:
	mov	r0, #0
.L30:
	ldr	r3, .L37
	mov	r1, #48
	ldr	r2, [r3, #4]
	mla	r2, r1, r2, r3
	add	r2, r2, #60
	cmp	r0, r2
	moveq	r2, #1
	streq	r2, [r3, #12]
	bx	lr
.L38:
	.align	2
.L37:
	.word	.LANCHOR0
	.word	.LANCHOR0+516108
.LFE9:
	.size	nm_STATION_REPORT_DATA_get_previous_completed_record, .-nm_STATION_REPORT_DATA_get_previous_completed_record
	.section	.text.nm_STATION_REPORT_DATA_get_most_recently_completed_record,"ax",%progbits
	.align	2
	.global	nm_STATION_REPORT_DATA_get_most_recently_completed_record
	.type	nm_STATION_REPORT_DATA_get_most_recently_completed_record, %function
nm_STATION_REPORT_DATA_get_most_recently_completed_record:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L40
	mov	r2, #0
	str	r2, [r3, #12]
	ldr	r2, [r3, #4]
	mov	r1, #48
	mla	r3, r1, r2, r3
	add	r0, r3, #60
	b	nm_STATION_REPORT_DATA_get_previous_completed_record
.L41:
	.align	2
.L40:
	.word	.LANCHOR0
.LFE10:
	.size	nm_STATION_REPORT_DATA_get_most_recently_completed_record, .-nm_STATION_REPORT_DATA_get_most_recently_completed_record
	.section	.text.STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines,"ax",%progbits
	.align	2
	.global	STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines
	.type	STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines, %function
STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines:
.LFB11:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L49
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI6:
	ldr	r6, .L49+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	mov	r3, #496
	ldr	r2, .L49+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r6, #0]
	cmp	r3, #0
	bne	.L43
	mov	r0, #43008
	ldr	r1, .L49+8
	ldr	r2, .L49+12
	bl	mem_malloc_debug
	str	r0, [r6, #0]
.L43:
	ldr	r7, .L49+4
	mov	r1, r4
	mov	r2, sp
	mov	r0, r5
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	ldr	r1, [sp, #0]
	ldr	r2, .L49+16
	ldr	r3, [r7, #0]
	add	r2, r2, r1, asl #7
	str	r2, [r3, #0]
	bl	nm_STATION_REPORT_DATA_get_most_recently_completed_record
	mov	r6, #1
	ldr	r8, .L49+20
	b	.L44
.L48:
	ldrb	r3, [r0, #47]	@ zero_extendqisi2
	cmp	r3, r5
	bne	.L45
	ldrb	r3, [r0, #46]	@ zero_extendqisi2
	cmp	r3, r4
	ldreq	r3, [r7, #0]
	streq	r0, [r3, r6, asl #2]
	addeq	r6, r6, #1
.L45:
	cmp	r6, r8
	bls	.L46
	ldr	r0, .L49+8
	bl	RemovePathFromFileName
	ldr	r2, .L49+24
	mov	r1, r0
	ldr	r0, .L49+28
	bl	Alert_Message_va
	b	.L47
.L46:
	bl	nm_STATION_REPORT_DATA_get_previous_completed_record
.L44:
	cmp	r0, #0
	bne	.L48
.L47:
	ldr	r3, .L49
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, pc}
.L50:
	.align	2
.L49:
	.word	station_report_data_completed_records_recursive_MUTEX
	.word	.LANCHOR5
	.word	.LC4
	.word	509
	.word	station_preserves+76
	.word	10751
	.word	537
	.word	.LC6
.LFE11:
	.size	STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines, .-STATION_REPORT_DATA_fill_ptrs_and_return_how_many_lines
	.section	.text.STATION_REPORT_draw_scroll_line,"ax",%progbits
	.align	2
	.global	STATION_REPORT_draw_scroll_line
	.type	STATION_REPORT_draw_scroll_line, %function
STATION_REPORT_draw_scroll_line:
.LFB12:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L53+4
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, lr}
.LCFI7:
	ldr	r2, .L53+8
	sub	sp, sp, #20
.LCFI8:
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L53+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L53+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L52
.LBB5:
	ldr	r4, [r3, r4, asl #2]
	mov	r3, #250
	ldrh	r2, [r4, #34]
	mov	r1, #16
	str	r3, [sp, #0]
	add	r0, sp, #4
	mov	r3, #150
	bl	GetDateStr
	mov	r2, #6
	mov	r1, r0
	ldr	r0, .L53+20
	bl	strlcpy
	flds	s14, [r4, #28]	@ int
	flds	s13, .L53
	ldr	r3, .L53+24
	fuitos	s15, s14
	flds	s14, [r4, #4]
	fdivs	s15, s15, s13
	fsts	s15, [r3, #0]
	fcvtds	d7, s14
	ldr	r3, .L53+28
	fstd	d7, [r3, #0]
	ldrh	r3, [r4, #44]
	fmsr	s14, r3	@ int
	ldr	r3, .L53+32
	fuitos	s15, s14
	flds	s14, [r4, #8]
	fdivs	s15, s15, s13
	fsts	s15, [r3, #0]
	fcvtds	d7, s14
	ldr	r3, .L53+36
	fstd	d7, [r3, #0]
	ldrh	r3, [r4, #42]
	fmsr	s14, r3	@ int
	ldr	r3, .L53+40
	fuitos	s15, s14
	flds	s14, [r4, #12]
	fdivs	s15, s15, s13
	fsts	s15, [r3, #0]
	fcvtds	d7, s14
	ldr	r3, .L53+44
	fstd	d7, [r3, #0]
	ldrh	r3, [r4, #38]
	fmsr	s14, r3	@ int
	ldr	r3, .L53+48
	fuitos	s15, s14
	flds	s14, [r4, #20]
	fdivs	s15, s15, s13
	fsts	s15, [r3, #0]
	fcvtds	d7, s14
	ldr	r3, .L53+52
	fstd	d7, [r3, #0]
	ldrh	r3, [r4, #40]
	fmsr	s14, r3	@ int
	ldr	r3, .L53+56
	fuitos	s15, s14
	flds	s14, [r4, #16]
	fdivs	s15, s15, s13
	fsts	s15, [r3, #0]
	fcvtds	d7, s14
	ldr	r3, .L53+60
	fstd	d7, [r3, #0]
	ldrh	r3, [r4, #36]
	fmsr	s14, r3	@ int
	ldr	r3, .L53+64
	fuitos	s15, s14
	flds	s14, [r4, #24]
	fdivs	s13, s15, s13
	fcvtds	d7, s14
	fsts	s13, [r3, #0]
	ldr	r3, .L53+68
	fstd	d7, [r3, #0]
.L52:
.LBE5:
	ldr	r3, .L53+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #20
	ldmfd	sp!, {r4, pc}
.L54:
	.align	2
.L53:
	.word	1114636288
	.word	station_report_data_completed_records_recursive_MUTEX
	.word	.LC4
	.word	581
	.word	.LANCHOR5
	.word	GuiVar_RptDate
	.word	GuiVar_RptIrrigMin
	.word	GuiVar_RptIrrigGal
	.word	GuiVar_RptManualPMin
	.word	GuiVar_RptManualPGal
	.word	GuiVar_RptManualMin
	.word	GuiVar_RptManualGal
	.word	GuiVar_RptTestMin
	.word	GuiVar_RptTestGal
	.word	GuiVar_RptWalkThruMin
	.word	GuiVar_RptWalkThruGal
	.word	GuiVar_RptRReMin
	.word	GuiVar_RptRReGal
.LFE12:
	.size	STATION_REPORT_draw_scroll_line, .-STATION_REPORT_draw_scroll_line
	.section	.text.nm_STATION_REPORT_DATA_close_and_start_a_new_record,"ax",%progbits
	.align	2
	.global	nm_STATION_REPORT_DATA_close_and_start_a_new_record
	.type	nm_STATION_REPORT_DATA_close_and_start_a_new_record, %function
nm_STATION_REPORT_DATA_close_and_start_a_new_record:
.LFB13:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L60
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI9:
	ldr	r5, .L60+4
	sub	sp, sp, #36
.LCFI10:
	mov	r6, r0
	ldr	r2, .L60+8
	ldr	r0, [r3, #0]
	mov	r4, r1
	ldr	r3, .L60+12
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #4]
	mov	ip, #48
	mov	lr, r6
	mla	ip, r3, ip, r5
	ldmia	lr!, {r0, r1, r2, r3}
	add	ip, ip, #60
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr, {r0, r1, r2, r3}
	stmia	ip, {r0, r1, r2, r3}
.LBB6:
	add	r0, r5, #4
	bl	nm_STATION_REPORT_DATA_inc_index
	ldr	r3, [r5, #4]
	cmp	r3, #0
	moveq	r2, #1
	streq	r2, [r5, #8]
	ldr	r2, [r5, #16]
	cmp	r3, r2
	bne	.L57
	ldr	r0, .L60+16
	bl	nm_STATION_REPORT_DATA_inc_index
.L57:
	ldr	r2, [r5, #4]
	ldr	r3, [r5, #20]
	cmp	r2, r3
	bne	.L58
	ldr	r0, .L60+20
	bl	nm_STATION_REPORT_DATA_inc_index
.L58:
.LBE6:
	ldr	r3, .L60
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	mov	r0, #4
	mov	r1, #2
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	mov	r0, r6
	bl	nm_init_station_report_data_record
	ldrb	r2, [r4, #5]	@ zero_extendqisi2
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	strh	r3, [r6, #34]	@ movhi
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	ldrb	r2, [r4, #2]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #16
	ldrb	r2, [r4, #3]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #24
	str	r3, [r6, #0]
	ldr	r3, .L60+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #101
	bne	.L55
.LBB7:
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L60+28
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
.L55:
.LBE7:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, pc}
.L61:
	.align	2
.L60:
	.word	station_report_data_completed_records_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC4
	.word	646
	.word	.LANCHOR0+16
	.word	.LANCHOR0+20
	.word	GuiLib_CurStructureNdx
	.word	FDTO_STATION_REPORT_redraw_scrollbox
.LFE13:
	.size	nm_STATION_REPORT_DATA_close_and_start_a_new_record, .-nm_STATION_REPORT_DATA_close_and_start_a_new_record
	.section	.text.STATION_REPORT_DATA_free_report_support,"ax",%progbits
	.align	2
	.global	STATION_REPORT_DATA_free_report_support
	.type	STATION_REPORT_DATA_free_report_support, %function
STATION_REPORT_DATA_free_report_support:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI11:
	ldr	r4, .L64
	ldr	r5, .L64+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L64+8
	ldr	r3, .L64+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, #0]
	cmp	r0, #0
	beq	.L63
	ldr	r1, .L64+8
	ldr	r2, .L64+16
	bl	mem_free_debug
	mov	r3, #0
	str	r3, [r5, #0]
.L63:
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L65:
	.align	2
.L64:
	.word	station_report_data_completed_records_recursive_MUTEX
	.word	.LANCHOR5
	.word	.LC4
	.word	770
	.word	774
.LFE14:
	.size	STATION_REPORT_DATA_free_report_support, .-STATION_REPORT_DATA_free_report_support
	.global	station_report_data_revision_record_counts
	.global	station_report_data_revision_record_sizes
	.global	STATION_REPORT_DATA_FILENAME
	.global	station_report_data_completed
	.section	.rodata.station_report_data_revision_record_counts,"a",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	station_report_data_revision_record_counts, %object
	.size	station_report_data_revision_record_counts, 8
station_report_data_revision_record_counts:
	.word	10752
	.word	10752
	.section	.bss.station_report_data_ci_timer,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	station_report_data_ci_timer, %object
	.size	station_report_data_ci_timer, 4
station_report_data_ci_timer:
	.space	4
	.section	.rodata.STATION_REPORT_DATA_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	STATION_REPORT_DATA_FILENAME, %object
	.size	STATION_REPORT_DATA_FILENAME, 23
STATION_REPORT_DATA_FILENAME:
	.ascii	"STATION_REPORT_RECORDS\000"
	.section	.bss.station_report_data_completed,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	station_report_data_completed, %object
	.size	station_report_data_completed, 516156
station_report_data_completed:
	.space	516156
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"STA_REPRT_DATA file unexpd update %u\000"
.LC1:
	.ascii	"STA_REPRT_DATA file update : to revision %u from %u"
	.ascii	"\000"
.LC2:
	.ascii	"STA_REPRT_DATA updater error\000"
.LC3:
	.ascii	"\000"
.LC4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/station_report_data.c\000"
.LC5:
	.ascii	"Timer NOT CREATED : %s, %u\000"
.LC6:
	.ascii	"REPORTS: why so many records? : %s, %u\000"
	.section	.bss.station_report_data_ptrs,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	station_report_data_ptrs, %object
	.size	station_report_data_ptrs, 4
station_report_data_ptrs:
	.space	4
	.section	.rodata.station_report_data_revision_record_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	station_report_data_revision_record_sizes, %object
	.size	station_report_data_revision_record_sizes, 8
station_report_data_revision_record_sizes:
	.word	48
	.word	48
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI5-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI6-.LFB11
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI7-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x1c
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI9-.LFB13
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI11-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE26:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/station_report_data.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x141
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF14
	.byte	0x1
	.4byte	.LASF15
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x145
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x10d
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x4a
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF1
	.byte	0x1
	.byte	0x50
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF2
	.byte	0x1
	.byte	0xa3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xdc
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x101
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x116
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x136
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x17d
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1c5
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x1e5
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST5
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x240
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST6
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x27e
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST7
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x2fc
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB11
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB12
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI8
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB13
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI10
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB14
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x84
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF6:
	.ascii	"STATION_REPORT_DATA_start_the_ci_timer_if_it_is_not"
	.ascii	"_running\000"
.LASF7:
	.ascii	"nm_STATION_REPORT_DATA_inc_index\000"
.LASF10:
	.ascii	"STATION_REPORT_DATA_fill_ptrs_and_return_how_many_l"
	.ascii	"ines\000"
.LASF5:
	.ascii	"save_file_station_report_data\000"
.LASF4:
	.ascii	"init_file_station_report_data\000"
.LASF8:
	.ascii	"nm_STATION_REPORT_DATA_get_previous_completed_recor"
	.ascii	"d\000"
.LASF15:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/station_report_data.c\000"
.LASF14:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"nm_init_station_report_data_record\000"
.LASF11:
	.ascii	"STATION_REPORT_draw_scroll_line\000"
.LASF12:
	.ascii	"nm_STATION_REPORT_DATA_close_and_start_a_new_record"
	.ascii	"\000"
.LASF16:
	.ascii	"nm_STATION_REPORT_DATA_increment_next_avail_ptr\000"
.LASF13:
	.ascii	"STATION_REPORT_DATA_free_report_support\000"
.LASF1:
	.ascii	"nm_init_station_report_data_records\000"
.LASF9:
	.ascii	"nm_STATION_REPORT_DATA_get_most_recently_completed_"
	.ascii	"record\000"
.LASF2:
	.ascii	"nm_station_report_data_updater\000"
.LASF0:
	.ascii	"station_report_data_ci_timer_callback\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
