	.file	"foal_comm.c"
	.text
.Ltext0:
	.section	.text._nm_respond_to_two_wire_cable_fault,"ax",%progbits
	.align	2
	.type	_nm_respond_to_two_wire_cable_fault, %function
_nm_respond_to_two_wire_cable_fault:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L5
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI0:
	ldr	r4, .L5+4
	mov	r5, r0
	mov	r6, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L5+8
	mov	r8, r2
	ldr	r2, .L5+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L5+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L5+12
	mov	r3, #640
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L5+20
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L5+12
	ldr	r3, .L5+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, .L5+28
.L3:
	ldr	r3, [r4, #24]
	cmp	r3, #0
	beq	.L2
	ldr	r3, [r4, #28]
	cmp	r3, r5
	bne	.L2
	ldr	r3, [r4, #32]
	sub	r3, r3, #11
	cmp	r3, #1
	bhi	.L2
	ldr	r0, [r4, #44]
	mov	r1, #0
	mov	r2, #1
	mov	r3, #114
	bl	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists
.L2:
	add	r4, r4, #472
	cmp	r4, r7
	bne	.L3
	mov	r1, #114
	mov	r0, r5
	bl	FOAL_IRRI_remove_all_stations_at_this_box_from_the_irrigation_lists
	mov	r3, #1
	str	r5, [r6, #0]
	str	r3, [r8, #0]
	ldr	r3, .L5+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L5+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L5
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L6:
	.align	2
.L5:
	.word	list_foal_irri_recursive_MUTEX
	.word	poc_preserves
	.word	638
	.word	.LC0
	.word	system_preserves_recursive_MUTEX
	.word	poc_preserves_recursive_MUTEX
	.word	642
	.word	poc_preserves+5664
.LFE5:
	.size	_nm_respond_to_two_wire_cable_fault, .-_nm_respond_to_two_wire_cable_fault
	.section	.text.__station_decoder_fault_support,"ax",%progbits
	.align	2
	.type	__station_decoder_fault_support, %function
__station_decoder_fault_support:
.LFB22:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L15
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	mov	r4, r1
	mov	r6, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r5, r2
	mov	r3, #1776
	ldr	r2, .L15+4
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r6
	ldr	r1, [r4, #8]
	ldr	r2, [r4, #12]
	bl	nm_STATION_get_pointer_to_decoder_based_station
	subs	r7, r0, #0
	beq	.L8
	bl	nm_STATION_get_station_number_0
	mov	r2, r5
	mov	r7, r0
	mov	r1, r7
	mov	r0, r6
	bl	FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists
	cmp	r5, #108
	moveq	r0, #2
	beq	.L14
	cmp	r5, #107
	moveq	r0, #1
	beq	.L14
	cmp	r5, #109
	bne	.L12
	mov	r0, #3
.L14:
	mov	r1, r6
	ldr	r2, [r4, #8]
	mov	r3, r7
	bl	Alert_two_wire_station_decoder_fault_idx
	b	.L12
.L8:
	cmp	r5, #109
	beq	.L10
	ldr	r0, .L15+8
	ldr	r1, [r4, #8]
	ldr	r2, [r4, #12]
	bl	Alert_Message_va
	b	.L10
.L12:
	mov	r7, #1
.L10:
	ldr	r3, .L15
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r7
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L16:
	.align	2
.L15:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	.LC1
.LFE22:
	.size	__station_decoder_fault_support, .-__station_decoder_fault_support
	.section	.text.__poc_decoder_fault_support,"ax",%progbits
	.align	2
	.type	__poc_decoder_fault_support, %function
__poc_decoder_fault_support:
.LFB23:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L25
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI2:
	mov	r5, r0
	sub	sp, sp, #48
.LCFI3:
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L25+4
	mov	r6, r2
	ldr	r2, .L25+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	mov	r1, #11
	ldr	r2, [r4, #8]
	mov	r3, #1
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	subs	r7, r0, #0
	bne	.L18
	mov	r2, r7
	mov	r0, r5
	mov	r1, #12
	mov	r3, #1
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	subs	r7, r0, #0
	beq	.L19
.L18:
	mov	r0, r7
	bl	POC_get_GID_irrigation_system
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	mov	r1, #0
	mov	r2, #1
	mov	r3, r6
	bl	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists
	mov	r0, r7
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	mov	r0, sp
	bl	strlcpy
	cmp	r6, #110
	moveq	r0, #1
	beq	.L24
	cmp	r6, #111
	bne	.L22
	ldr	r3, [r4, #12]
	cmp	r3, #0
	beq	.L23
	ldr	r0, .L25+12
	bl	Alert_Message
.L23:
	mov	r0, #2
.L24:
	mov	r1, r5
	ldr	r2, [r4, #8]
	mov	r3, sp
	bl	Alert_two_wire_poc_decoder_fault_idx
	b	.L21
.L22:
	cmp	r6, #112
	moveq	r0, #3
	bne	.L21
	b	.L24
.L19:
	ldr	r0, .L25+16
	ldr	r1, [r4, #8]
	bl	Alert_Message_va
.L21:
	ldr	r3, .L25
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L26:
	.align	2
.L25:
	.word	list_poc_recursive_MUTEX
	.word	1845
	.word	.LC0
	.word	.LC2
	.word	.LC3
.LFE23:
	.size	__poc_decoder_fault_support, .-__poc_decoder_fault_support
	.section	.text.FOAL_COMM_process_incoming__clean_house_request__response,"ax",%progbits
	.align	2
	.global	FOAL_COMM_process_incoming__clean_house_request__response
	.type	FOAL_COMM_process_incoming__clean_house_request__response, %function
FOAL_COMM_process_incoming__clean_house_request__response:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L28
	mov	r1, #0
	ldrh	r2, [r3, #0]
	ldr	r3, .L28+4
	add	r2, r2, #10
	str	r1, [r3, r2, asl #2]
	bx	lr
.L29:
	.align	2
.L28:
	.word	last_contact
	.word	comm_mngr
.LFE1:
	.size	FOAL_COMM_process_incoming__clean_house_request__response, .-FOAL_COMM_process_incoming__clean_house_request__response
	.section	.text.extract_stop_key_record_from_token_response,"ax",%progbits
	.align	2
	.global	extract_stop_key_record_from_token_response
	.type	extract_stop_key_record_from_token_response, %function
extract_stop_key_record_from_token_response:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI4:
	ldr	r4, .L40
	mov	r5, r0
	mov	r2, #24
	mov	r6, r1
	mov	r0, r4
	ldr	r1, [r5, #0]
	bl	memcpy
	ldr	r3, [r5, #0]
	mov	r1, #400
	add	r3, r3, #24
	str	r3, [r5, #0]
	ldr	r3, .L40+4
	ldr	r2, .L40+8
	ldr	r0, [r3, #0]
	ldr	r3, .L40+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r4, #12]
	mov	r7, #0
	cmp	r3, r7
	str	r7, [r4, #20]
	mov	r3, r4
	beq	.L31
	ldr	r2, [r4, #16]
	cmp	r2, r7
	beq	.L31
	ldr	r5, .L40+16
	ldr	r8, .L40+20
	mov	sl, r5
.L33:
	ldr	r0, [r5, #16]
	cmp	r0, #0
	beq	.L32
	bl	SYSTEM_get_used_for_irri_bool
	cmp	r0, #0
	beq	.L32
	mla	r0, r8, r7, sl
	mov	r1, #0
	add	r0, r0, #16
	mov	r2, #1
	mov	r3, #100
	bl	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists
	cmp	r0, #0
	beq	.L32
	ldr	r1, [r5, #96]
	ldr	r0, [r5, #16]
	str	r1, [r4, #20]
	mov	r2, r6
	bl	Alert_stop_key_pressed_idx
.L32:
	add	r7, r7, #1
	add	r5, r5, #14208
	cmp	r7, #4
	add	r5, r5, #16
	bne	.L33
	b	.L34
.L31:
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L35
	ldr	r4, .L40+16
	ldr	r7, .L40+20
	ldr	sl, .L40
	mov	r5, #0
	mov	r8, r4
.L37:
	ldr	r0, [r4, #16]
	cmp	r0, #0
	beq	.L36
	bl	SYSTEM_get_used_for_irri_bool
	cmp	r0, #0
	beq	.L36
	mla	r0, r7, r5, r8
	ldr	r1, [r4, #96]
	add	r0, r0, #16
	mov	r2, #0
	mov	r3, #100
	bl	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists
	cmp	r0, #0
	beq	.L36
	ldr	r1, [r4, #96]
	ldr	r0, [r4, #16]
	str	r1, [sl, #20]
	mov	r2, r6
	bl	Alert_stop_key_pressed_idx
.L36:
	add	r5, r5, #1
	add	r4, r4, #14208
	cmp	r5, #4
	add	r4, r4, #16
	bne	.L37
	b	.L34
.L35:
	ldr	r0, .L40+24
	bl	Alert_Message
.L34:
	ldr	r3, .L40+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L40+28
	mov	r0, #1
	str	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L41:
	.align	2
.L40:
	.word	.LANCHOR0
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	282
	.word	system_preserves
	.word	14224
	.word	.LC4
	.word	.LANCHOR1
.LFE3:
	.size	extract_stop_key_record_from_token_response, .-extract_stop_key_record_from_token_response
	.global	__udivsi3
	.section	.text.FOAL_COMM_process_incoming__irrigation_token__response,"ax",%progbits
	.align	2
	.global	FOAL_COMM_process_incoming__irrigation_token__response
	.type	FOAL_COMM_process_incoming__irrigation_token__response, %function
FOAL_COMM_process_incoming__irrigation_token__response:
.LFB27:
	@ args = 0, pretend = 0, frame = 224
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L201
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI5:
	ldr	r2, .L201+4
	sub	sp, sp, #236
.LCFI6:
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #2080
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [r5, #12]
	mov	r2, #6
	add	r0, sp, #216
	str	r1, [sp, #224]
	bl	memcpy
	ldr	r3, [sp, #224]
.LBB78:
	add	r0, sp, #236
.LBE78:
	add	r3, r3, #6
	str	r3, [sp, #224]
	ldr	r3, .L201+8
.LBB79:
	mov	r4, #0
.LBE79:
	ldr	r2, [r3, #28]
.LBB80:
	str	r4, [r0, #-8]!
.LBE80:
	add	r2, r2, #1
	str	r2, [r3, #28]
.LBB81:
	add	r1, r5, #20
	mov	r2, #3
	bl	memcpy
	ldr	r2, [sp, #228]
	ldr	r3, .L201+12
.L45:
	ldr	r1, [r3, #16]
	cmp	r1, #0
	beq	.L43
	ldr	r1, [r3, #20]
	cmp	r1, r2
	beq	.L44
.L43:
	add	r4, r4, #1
	cmp	r4, #12
	add	r3, r3, #92
	bne	.L45
	b	.L196
.L44:
.LBE81:
.LBB82:
	ldr	r3, .L201+16
	ldrh	r3, [r3, #0]
	cmp	r3, r4
	beq	.L47
	ldr	r0, .L201+4
	bl	RemovePathFromFileName
	ldr	r2, .L201+20
	mov	r1, r0
	ldr	r0, .L201+24
	bl	Alert_Message_va
.L47:
	ldrh	r3, [sp, #220]
	tst	r3, #256
	moveq	r3, #1
	beq	.L48
	ldr	r5, [sp, #224]
	mov	r1, #15
	mov	r3, r1
	mov	r0, r5
	mov	r2, #1
	bl	PDATA_extract_and_store_changes
	mov	r3, #0
	add	r0, r5, r0
	str	r0, [sp, #224]
.L48:
	ldrh	r2, [sp, #218]
	tst	r2, #2048
	beq	.L49
.LBB83:
	ldr	r3, .L201+28
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L201+4
	ldr	r3, .L201+32
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [sp, #224]
	mov	r2, #72
	add	r0, sp, #20
	bl	memcpy
	ldr	r3, [sp, #224]
	mov	r2, #92
	add	r3, r3, #72
	str	r3, [sp, #224]
	ldr	r3, .L201+36
.LBB84:
	mov	fp, #23
.LBE84:
	mla	r1, r2, r4, r3
	mov	r2, #72
	str	r1, [sp, #12]
	mov	r0, r1
	add	r1, sp, #20
	bl	memcmp
.LBB85:
	mul	fp, r4, fp
.LBE85:
.LBE83:
	mov	r8, #0
	mov	r6, r8
.LBB97:
	adds	r5, r0, #0
	movne	r5, #1
.LBB86:
	mov	sl, r5
.L57:
	add	r2, sp, #236
	add	r3, r2, r6, asl #2
	ldrb	r2, [r3, #-208]	@ zero_extendqisi2
	ldr	r3, .L201+12
	add	r1, fp, r6
	add	r1, r1, #7
	ldrb	r3, [r3, r1, asl #2]	@ zero_extendqisi2
	and	r2, r2, #1
	and	r3, r3, #1
	cmp	r3, r2
	beq	.L50
	mov	r0, r4
	mov	r1, r6
	bl	Alert_station_card_added_or_removed_idx
	mov	sl, #1
.L50:
	add	r1, sp, #236
	add	r3, r1, r6, asl #2
	ldrb	r2, [r3, #-208]	@ zero_extendqisi2
	ldr	r3, .L201+12
	add	r1, fp, r6
	add	r1, r1, #7
	ldrb	r3, [r3, r1, asl #2]	@ zero_extendqisi2
	mov	r2, r2, lsr #1
	mov	r3, r3, lsr #1
	and	r2, r2, #1
	and	r3, r3, #1
	cmp	r3, r2
	beq	.L51
	mov	r0, r4
	mov	r1, r6
	bl	Alert_station_terminal_added_or_removed_idx
	mov	sl, #1
.L51:
	ldr	r3, .L201+40
	ldr	r2, .L201+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L201+44
	bl	xQueueTakeMutexRecursive_debug
	add	r2, sp, #236
	add	r3, r2, r6, asl #2
	ldrb	r3, [r3, #-208]	@ zero_extendqisi2
	ands	r5, r3, #1
	beq	.L181
	mov	r3, r3, lsr #1
	ands	r5, r3, #1
	beq	.L181
	add	r3, r4, #65
	mov	r5, r8
	mov	r9, #0
	mov	r7, r6
	str	r3, [sp, #16]
.L54:
	mov	r0, r4
	mov	r1, r5
	bl	nm_STATION_get_pointer_to_station
	subs	r6, r0, #0
	bne	.L53
	mov	r1, r5
	mov	r2, #12
	mov	r0, r4
	bl	nm_STATION_create_new_station
	ldr	r1, [sp, #16]
	add	r2, r5, #1
	mov	r6, r0
	ldr	r0, .L201+48
	bl	Alert_Message_va
.L53:
	mov	r1, #12
	mov	r0, r6
	bl	STATION_get_change_bits_ptr
	add	r9, r9, #1
	mov	r1, #1
	mov	r2, #0
	mov	r3, #12
	str	r4, [sp, #0]
	str	r1, [sp, #4]
	add	r5, r5, #1
	str	r0, [sp, #8]
	mov	r0, r6
	bl	nm_STATION_set_physically_available
	cmp	r9, #8
	bne	.L54
	mov	r6, r7
	b	.L55
.L181:
	mov	r0, r4
	add	r1, r5, r8
	bl	nm_STATION_get_pointer_to_station
	subs	r7, r0, #0
	beq	.L56
	mov	r1, #13
	bl	STATION_get_change_bits_ptr
	mov	r2, #1
	mov	r1, #0
	str	r2, [sp, #4]
	mov	r3, #13
	mov	r2, r1
	str	r4, [sp, #0]
	str	r0, [sp, #8]
	mov	r0, r7
	bl	nm_STATION_set_physically_available
.L56:
	add	r5, r5, #1
	cmp	r5, #8
	bne	.L181
.L55:
	ldr	r3, .L201+40
.LBE86:
	add	r6, r6, #1
.LBB87:
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE87:
	cmp	r6, #6
	add	r8, r8, #8
	bne	.L57
.LBB88:
	ldr	r6, .L201+12
	mov	r3, #92
	mla	r3, r4, r3, r6
	ldrb	r1, [sp, #56]	@ zero_extendqisi2
	ldrb	r3, [r3, #56]	@ zero_extendqisi2
	and	r1, r1, #1
	and	r3, r3, #1
	cmp	r3, r1
	mov	r5, sl
	beq	.L58
	mov	r0, r4
	bl	Alert_lights_card_added_or_removed_idx
	mov	r5, #1
.L58:
	mov	r3, #92
	mla	r6, r3, r4, r6
	ldrb	r1, [sp, #56]	@ zero_extendqisi2
	ldrb	r3, [r6, #56]	@ zero_extendqisi2
	mov	r1, r1, lsr #1
	mov	r3, r3, lsr #1
	and	r1, r1, #1
	and	r3, r3, #1
	cmp	r3, r1
	beq	.L59
	mov	r0, r4
	bl	Alert_lights_terminal_added_or_removed_idx
	mov	r5, #1
.L59:
	ldr	r3, .L201+52
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L201+4
	ldr	r3, .L201+56
	bl	xQueueTakeMutexRecursive_debug
	ldrb	r3, [sp, #56]	@ zero_extendqisi2
	mov	r6, #0
	and	r3, r3, #3
	cmp	r3, #3
	movne	r7, #1
	bne	.L60
	mov	r8, #1
	mov	r7, r5
.L62:
	mov	r0, r4
	mov	r1, r6
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	subs	r5, r0, #0
	bne	.L61
	mov	r1, r6
	mov	r0, r4
	bl	nm_LIGHTS_create_new_group
	mov	r1, #12
	mov	r5, r0
	bl	LIGHTS_get_change_bits_ptr
	mov	r1, r4
	mov	r2, #0
	mov	r3, #12
	mov	sl, r0
	mov	r0, r5
	stmia	sp, {r4, r8, sl}
	bl	nm_LIGHTS_set_box_index
	mov	r0, r5
	mov	r1, r6
	mov	r2, #0
	mov	r3, #12
	stmia	sp, {r4, r8, sl}
	bl	nm_LIGHTS_set_output_index
.L61:
	mov	r1, #12
	mov	r0, r5
	bl	LIGHTS_get_change_bits_ptr
	add	r6, r6, #1
	mov	r1, #1
	mov	r2, #0
	mov	r3, #12
	stmia	sp, {r4, r8}
	str	r0, [sp, #8]
	mov	r0, r5
	bl	nm_LIGHTS_set_physically_available
	cmp	r6, #4
	bne	.L62
	mov	r5, r7
	b	.L63
.L60:
	mov	r0, r4
	mov	r1, r6
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	subs	r8, r0, #0
	beq	.L64
	mov	r1, #13
	bl	LIGHTS_get_change_bits_ptr
	mov	r1, #0
	mov	r2, r1
	mov	r3, #13
	stmia	sp, {r4, r7}
	str	r0, [sp, #8]
	mov	r0, r8
	bl	nm_LIGHTS_set_physically_available
.L64:
	add	r6, r6, #1
	cmp	r6, #4
	bne	.L60
.L63:
	ldr	r3, .L201+52
.LBE88:
.LBB89:
	ldr	r6, .L201+12
.LBE89:
.LBB90:
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE90:
.LBB91:
	mov	r3, #92
	mla	r3, r4, r3, r6
	ldrb	r1, [sp, #52]	@ zero_extendqisi2
	ldrb	r3, [r3, #52]	@ zero_extendqisi2
	and	r1, r1, #1
	and	r3, r3, #1
	cmp	r3, r1
	beq	.L65
	mov	r0, r4
	bl	Alert_poc_card_added_or_removed_idx
	mov	r5, #1
.L65:
	mov	r3, #92
	mla	r6, r3, r4, r6
	ldrb	r1, [sp, #52]	@ zero_extendqisi2
	ldrb	r3, [r6, #52]	@ zero_extendqisi2
	mov	r1, r1, lsr #1
	mov	r3, r3, lsr #1
	and	r1, r1, #1
	and	r3, r3, #1
	cmp	r3, r1
	beq	.L66
	mov	r0, r4
	bl	Alert_poc_terminal_added_or_removed_idx
	mov	r5, #1
.L66:
	ldr	r3, .L201+60
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L201+4
	ldr	r3, .L201+64
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	nm_POC_get_pointer_to_terminal_poc_with_this_box_index_0
	ldrb	r3, [sp, #52]	@ zero_extendqisi2
	and	r3, r3, #3
	cmp	r3, #3
	mov	r6, r0
	bne	.L67
	cmp	r0, #0
	bne	.L68
	mov	r2, r6
	mov	r1, #10
	mov	r0, r4
	bl	nm_POC_create_new_group
	mov	r1, #12
	mov	r7, #1
	mov	r6, r0
	bl	POC_get_change_bits_ptr
	mov	r1, r4
	mov	r2, #0
	mov	r3, #12
	str	r4, [sp, #0]
	str	r7, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r6
	bl	nm_POC_set_box_index
	ldr	r2, .L201+68
	add	r3, r4, #65
	mov	r1, #48
	add	r0, sp, #92
	bl	snprintf
	mov	r1, #12
	mov	r0, r6
	bl	POC_get_change_bits_ptr
	add	r1, sp, #92
	mov	r2, #0
	mov	r3, #12
	stmia	sp, {r4, r7}
	str	r0, [sp, #8]
	mov	r0, r6
	bl	nm_POC_set_name
.L68:
	mov	r1, #12
	mov	r0, r6
	bl	POC_get_change_bits_ptr
	mov	r1, #1
	mov	r2, #0
	mov	r3, #12
	str	r1, [sp, #4]
	str	r4, [sp, #0]
	str	r0, [sp, #8]
	mov	r0, r6
	bl	nm_POC_set_show_this_poc
	bl	POC_PRESERVES_synchronize_preserves_to_file
.L67:
	ldr	r3, .L201+60
.LBE91:
.LBB92:
	ldr	r6, .L201+12
.LBE92:
.LBB93:
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE93:
.LBB94:
	mov	r3, #92
	mla	r3, r4, r3, r6
	ldr	r1, [sp, #60]
	ldr	r3, [r3, #60]
	cmp	r3, r1
	beq	.L69
	mov	r0, r4
	bl	Alert_weather_card_added_or_removed_idx
	mov	r5, #1
.L69:
	mov	r3, #92
	mla	r6, r3, r4, r6
	ldr	r1, [sp, #64]
	ldr	r3, [r6, #64]
	cmp	r3, r1
	beq	.L70
	mov	r0, r4
	bl	Alert_weather_terminal_added_or_removed_idx
	mov	r5, #1
.L70:
.LBE94:
.LBB95:
	ldr	r6, .L201+12
	mov	r3, #92
	mla	r3, r4, r3, r6
	ldr	r1, [sp, #68]
	ldr	r3, [r3, #68]
	cmp	r3, r1
	beq	.L71
	mov	r0, r4
	bl	Alert_communication_card_added_or_removed_idx
	mov	r5, #1
.L71:
	mov	r3, #92
	mla	r6, r3, r4, r6
	ldr	r1, [sp, #72]
	ldr	r3, [r6, #72]
	cmp	r3, r1
	beq	.L72
	mov	r0, r4
	bl	Alert_communication_terminal_added_or_removed_idx
	mov	r5, #1
.L72:
.LBE95:
.LBB96:
	ldr	r3, .L201+12
	mov	r2, #92
	mla	r3, r2, r4, r3
	ldr	r1, [sp, #80]
	ldr	r3, [r3, #80]
	cmp	r3, r1
	beq	.L73
	mov	r0, r4
	bl	Alert_two_wire_terminal_added_or_removed_idx
	mov	r5, #1
.L73:
.LBE96:
	add	r1, sp, #20
	mov	r2, #72
	ldr	r0, [sp, #12]
	bl	memcpy
	ldr	r3, .L201+28
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r5, #0
	beq	.L75
	ldr	r0, .L201+72
	bl	Alert_Message
	ldr	r3, .L201+76
	mov	r2, #1
	str	r2, [r3, #360]
	bl	CONTROLLER_INITIATED_update_comm_server_registration_info
	ldr	r3, .L201+80
	ldrsh	r3, [r3, #0]
	cmp	r3, #36
	bne	.L75
	mov	r0, #0
	bl	Redraw_Screen
	b	.L75
.L49:
.LBE97:
	cmp	r3, #0
	beq	.L75
	ldr	r3, .L201+76
	add	r2, r4, #23
	ldr	r1, [r3, r2, asl #2]
	add	r1, r1, #1
	str	r1, [r3, r2, asl #2]
	b	.L76
.L75:
	ldr	r3, .L201+76
	add	r1, r4, #23
	ldr	r2, [r3, r1, asl #2]
	cmp	r2, #3
	movls	r2, #0
	strls	r2, [r3, r1, asl #2]
	strls	r2, [r3, #88]
.L76:
	ldrh	r2, [sp, #220]
	ldrh	r3, [sp, #218]
	orr	r3, r3, r2, asl #16
	tst	r3, #67108864
	ldrne	r2, .L201+76
	movne	r1, #3
	strne	r1, [r2, #36]
	movne	r1, #1
	strne	r1, [r2, #32]
	tst	r3, #2
	beq	.L78
	add	r0, sp, #224
	mov	r1, r4
	bl	extract_stop_key_record_from_token_response
	cmp	r0, #0
	beq	.L146
.L78:
	ldrh	r3, [sp, #218]
	tst	r3, #4
	beq	.L80
	add	r0, sp, #224
	bl	FOAL_extract_clear_mlb_gids
	cmp	r0, #0
	beq	.L146
.L80:
	ldrh	r3, [sp, #218]
	tst	r3, #8
	beq	.L81
.LBB98:
	mov	r2, #4
	add	r0, sp, #228
	ldr	r1, [sp, #224]
	bl	memcpy
	ldr	r3, [sp, #224]
	ldr	r2, .L201+84
	add	r3, r3, #4
	str	r3, [sp, #224]
	ldr	r3, [sp, #228]
	cmp	r3, r2
	bls	.L82
	ldr	r0, .L201+88
	bl	Alert_Message
	b	.L146
.L82:
	ldr	r1, .L201+92
	add	r2, r1, r4, asl #3
	ldr	r0, [r2, #60]
	cmp	r3, r0
	addne	r0, r4, #8
	movne	ip, #1
	strne	ip, [r1, r0, asl #3]
	strne	r3, [r2, #60]
	b	.L81
.L197:
.LBE98:
.LBB99:
	ldr	r1, [sp, #224]
	mov	r2, #12
	add	r0, sp, #196
	bl	memcpy
	ldr	r3, [sp, #224]
	ldr	r2, [sp, #196]
	add	r3, r3, #12
	str	r3, [sp, #224]
	ldr	r3, [sp, #200]
	sub	r1, r3, #2
	cmp	r3, #0
	cmpne	r1, #1
	bls	.L84
	cmp	r3, #1
	bne	.L146
.L84:
	sub	r6, r2, #1
	cmp	r2, #5
	cmpne	r6, #1
	movls	r6, #0
	movhi	r6, #1
	bhi	.L146
	cmp	r2, #1
	bne	.L85
	cmp	r1, #1
	bhi	.L86
	ldr	r3, .L201+96
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L201+4
	ldr	r3, .L201+100
	bl	xQueueTakeMutexRecursive_debug
	ldr	r5, .L201+104
	ldr	r7, .L201+108
.L90:
	ldr	r3, [r5, #24]
	cmp	r3, #0
	beq	.L87
	ldr	r3, [r5, #28]
	cmp	r3, r4
	bne	.L87
	ldr	r3, [r5, #32]
	cmp	r3, #10
	bne	.L87
	ldr	r3, [sp, #200]
	mov	r0, r4
	cmp	r3, #2
	bne	.L88
	bl	Alert_conventional_mv_short_idx
	mov	r3, #105
	b	.L89
.L88:
	bl	Alert_conventional_pump_short_idx
	mov	r3, #106
.L89:
	mov	r1, #0
	ldr	r0, [r5, #44]
	mov	r2, #1
	bl	FOAL_IRRI_remove_stations_in_this_system_from_the_irrigation_lists
	ldr	r1, [sp, #200]
	cmp	r1, #2
	bne	.L178
	ldr	r3, [r5, #44]
	mov	r2, #0
	ldr	r0, [r3, #0]
	mov	r3, #3
	bl	FOAL_IRRI_initiate_or_cancel_a_master_valve_override
.L178:
	mov	r6, #1
.L87:
	add	r5, r5, #472
	cmp	r5, r7
	bne	.L90
	ldr	r3, .L201+96
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r6, #0
	bne	.L91
	ldr	r0, .L201+112
	bl	Alert_Message
	b	.L91
.L86:
	cmp	r3, #0
	bne	.L92
	mov	r0, r4
	ldr	r1, [sp, #204]
	mov	r2, #104
	bl	FOAL_IRRI_remove_all_instances_of_this_station_from_the_irrigation_lists
	mov	r0, r4
	ldr	r1, [sp, #204]
	bl	Alert_conventional_station_short_idx
	b	.L91
.L92:
	cmp	r3, #1
	bne	.L91
	mov	r0, r4
	ldr	r1, [sp, #204]
	mov	r2, #5
	mov	r3, #16
	bl	FOAL_LIGHTS_turn_off_this_lights_output
	b	.L91
.L85:
	cmp	r2, #2
	bne	.L93
	mov	r0, r4
	bl	Alert_conventional_output_unknown_short_idx
	mov	r0, r4
	mov	r1, #104
	bl	FOAL_IRRI_remove_all_stations_ON_at_this_box_from_the_irrigation_lists
	ldr	r3, .L201+96
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L201+4
	mov	r3, #540
	bl	xQueueTakeMutexRecursive_debug
	ldr	r5, .L201+104
	ldr	r6, .L201+108
.L95:
	ldr	r3, [r5, #24]
	cmp	r3, #0
	beq	.L94
	ldr	r3, [r5, #28]
	cmp	r3, r4
	bne	.L94
	ldr	r3, [r5, #32]
	cmp	r3, #10
	bne	.L94
	ldr	r3, [r5, #44]
	mov	r1, #2
	ldr	r0, [r3, #0]
	mov	r2, #0
	mov	r3, #3
	bl	FOAL_IRRI_initiate_or_cancel_a_master_valve_override
.L94:
	add	r5, r5, #472
	cmp	r5, r6
	bne	.L95
	ldr	r3, .L201+96
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	mov	r1, #5
	mov	r2, #16
	bl	FOAL_LIGHTS_turn_off_all_lights_at_this_box
	b	.L91
.L93:
	cmp	r2, #5
	bne	.L91
	cmp	r3, #0
	bne	.L96
	mov	r0, r4
	ldr	r1, [sp, #204]
	bl	Alert_conventional_station_no_current_idx
	mov	r0, r4
	ldr	r1, [sp, #204]
	bl	FOAL_IRRI_set_no_current_flag_in_station_history_line_if_appropriate
	b	.L91
.L96:
	cmp	r3, #2
	bne	.L97
	mov	r0, r4
	bl	Alert_conventional_mv_no_current_idx
	b	.L91
.L97:
	cmp	r3, #3
	bne	.L98
	mov	r0, r4
	bl	Alert_conventional_pump_no_current_idx
	b	.L91
.L98:
	cmp	r3, #1
	bne	.L91
	mov	r0, r4
	ldr	r1, [sp, #204]
	mov	r2, #17
	bl	Alert_light_ID_with_text
.L91:
	add	r2, sp, #196
	ldmia	r2, {r0, r1, r2}
	ldr	r3, .L201+116
	stmib	r3, {r0, r1, r2}
	str	r4, [r3, #0]
	ldr	r3, .L201+120
	mov	r2, #1
	str	r2, [r3, #0]
.L151:
.LBE99:
	ldrh	r3, [sp, #220]
	tst	r3, #1
	beq	.L99
	ldr	r5, .L201
	mov	r1, #400
	ldr	r2, .L201+4
	ldr	r3, .L201+124
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	Alert_two_wire_cable_excessive_current_idx
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldr	r1, .L201+128
	ldr	r2, .L201+132
	bl	_nm_respond_to_two_wire_cable_fault
	b	.L99
.L152:
	ldr	r5, .L201
	mov	r1, #400
	ldr	r2, .L201+4
	ldr	r3, .L201+136
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	Alert_two_wire_cable_over_heated_idx
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldr	r1, .L201+140
	ldr	r2, .L201+144
	bl	_nm_respond_to_two_wire_cable_fault
	b	.L101
.L153:
	ldr	r5, .L201
	ldr	r2, .L201+4
	ldr	r3, .L201+148
	mov	r1, #400
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	Alert_two_wire_cable_cooled_off_idx
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L201+152
	mov	r2, #1
	str	r4, [r3, #0]
	ldr	r3, .L201+156
	str	r2, [r3, #0]
	b	.L102
.L154:
.LBB100:
	ldr	r3, .L201+160
	add	r0, r4, #18432
	add	r0, r0, #36
	add	r0, r3, r0, asl #2
	ldr	r1, [sp, #224]
	mov	r2, #4
	bl	memcpy
	ldr	r3, [sp, #224]
	add	r3, r3, #4
	str	r3, [sp, #224]
	b	.L103
.L155:
.LBE100:
.LBB101:
	ldr	r1, [sp, #224]
	add	r0, sp, #140
	mov	r2, #20
	bl	memcpy
	ldr	r3, [sp, #224]
	add	r0, sp, #140
	add	r3, r3, #20
	mov	r1, #6
	str	r3, [sp, #224]
	bl	FOAL_IRRI_initiate_a_station_test
	b	.L104
.L156:
.LBE101:
.LBB102:
	ldr	r1, [sp, #224]
	mov	r2, #4
	add	r0, sp, #232
	bl	memcpy
	ldr	r3, [sp, #224]
	add	r3, r3, #4
	str	r3, [sp, #224]
	bl	WEATHER_get_et_gage_is_in_use
	cmp	r0, #1
	bne	.L105
	bl	WEATHER_get_et_gage_box_index
	cmp	r0, r4
	bne	.L106
	ldr	r3, [sp, #232]
	sub	r3, r3, #1
	cmp	r3, #1
	bhi	.L107
	ldr	r3, .L201+224
	ldr	r5, .L201+232
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L201+164
	ldr	r2, .L201+4
	bl	xQueueTakeMutexRecursive_debug
	ldrh	r3, [r5, #38]
	cmp	r3, #1
	bne	.L108
	ldrh	r3, [r5, #36]
	ldr	r2, [sp, #232]
	mov	r6, #100
	mla	r3, r6, r2, r3
	strh	r3, [r5, #36]	@ movhi
	bl	WEATHER_get_et_gage_log_pulses
	cmp	r0, #1
	bne	.L108
	ldrh	r0, [r5, #36]
	mov	r1, r6
	bl	__udivsi3
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	bl	Alert_ETGage_Pulse
.L108:
	ldr	r3, .L201+224
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L109
.L107:
	ldr	r0, .L201+168
	bl	Alert_Message
	b	.L109
.L106:
	bl	WEATHER_get_et_gage_box_index
	add	r1, r4, #65
	add	r2, r0, #65
	ldr	r0, .L201+172
	bl	Alert_Message_va
	b	.L109
.L105:
	bl	Alert_et_gage_pulse_but_no_gage_in_use
	b	.L109
.L157:
.LBE102:
.LBB103:
	ldr	r1, [sp, #224]
	mov	r2, #4
	add	r0, sp, #232
	bl	memcpy
	ldr	r3, [sp, #224]
	add	r3, r3, #4
	str	r3, [sp, #224]
	bl	WEATHER_get_rain_bucket_is_in_use
	cmp	r0, #1
	bne	.L110
	bl	WEATHER_get_rain_bucket_box_index
	cmp	r0, r4
	bne	.L111
	ldr	r3, [sp, #232]
	sub	r3, r3, #1
	cmp	r3, #1
	bhi	.L112
.LBB104:
	ldr	r5, .L201+176
	mov	r1, #400
	ldr	r2, .L201+4
	ldr	r3, .L201+180
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [sp, #232]
	ldr	r3, .L201+184
	ldr	r2, .L201+188
.L114:
	ldr	r0, [r3, #16]
	cmp	r0, #0
	ldrne	r0, [r3, #36]
	addne	r0, r0, r1
	strne	r0, [r3, #36]
	add	r3, r3, #14208
	add	r3, r3, #16
	cmp	r3, r2
	bne	.L114
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r5, .L201+224
	ldr	r6, .L201+232
	mov	r1, #400
	ldr	r2, .L201+4
	ldr	r3, .L201+192
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [sp, #232]
	ldr	r2, [r6, #48]
	add	r2, r3, r2
	str	r2, [r6, #48]
	ldr	r2, [r6, #52]
	add	r2, r2, r3
	str	r2, [r6, #52]
	ldr	r2, [r6, #44]
	add	r3, r2, r3
	str	r3, [r6, #44]
	bl	WEATHER_get_rain_bucket_max_hourly_inches_100u
	mov	r7, r0
	bl	WEATHER_get_rain_bucket_minimum_inches_100u
	ldr	r3, [r6, #44]
	ldrh	r2, [r6, #42]
	cmp	r3, r7
	strhi	r7, [r6, #44]
	cmp	r2, #0
	ldr	r3, .L201+232
	bne	.L116
	ldrh	r1, [r3, #40]
	ldr	r2, [r3, #44]
	add	r2, r1, r2
	cmp	r2, r0
	bcc	.L116
	ldr	r3, [r3, #60]
	cmp	r3, #0
	bne	.L117
	mov	r0, #136
	bl	CONTROLLER_INITIATED_post_event
.L117:
	ldr	r3, .L201+232
	mov	r2, #1
	str	r2, [r3, #60]
.L116:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	b	.L118
.L112:
.LBE104:
	ldr	r0, .L201+196
	bl	Alert_Message
	b	.L118
.L111:
	bl	WEATHER_get_rain_bucket_box_index
	add	r1, r4, #65
	add	r2, r0, #65
	ldr	r0, .L201+200
	bl	Alert_Message_va
	b	.L118
.L110:
	bl	Alert_rain_pulse_but_no_bucket_in_use
	b	.L118
.L158:
.LBE103:
.LBB105:
	ldr	r1, [sp, #224]
	mov	r2, #4
	add	r0, sp, #232
	bl	memcpy
	ldr	r1, [sp, #224]
	mov	r2, #4
	add	r1, r1, #4
	add	r0, sp, #228
	str	r1, [sp, #224]
	bl	memcpy
	ldr	r3, [sp, #224]
	add	r3, r3, #4
	str	r3, [sp, #224]
	bl	WEATHER_get_wind_gage_in_use
	subs	r5, r0, #0
	beq	.L119
	bl	WEATHER_get_wind_gage_box_index
	cmp	r0, r4
	beq	.L160
	bl	WEATHER_get_wind_gage_box_index
	add	r1, r4, #65
	add	r2, r0, #65
	ldr	r0, .L201+204
	bl	Alert_Message_va
	mov	r3, #0
	str	r3, [sp, #232]
	str	r3, [sp, #228]
	b	.L160
.L119:
	bl	Alert_wind_detected_but_no_gage_in_use
	str	r5, [sp, #232]
	str	r5, [sp, #228]
	b	.L160
.L198:
	bl	WEATHER_get_wind_gage_in_use
	cmp	r0, #0
	moveq	r5, r0
	beq	.L121
	bl	WEATHER_get_wind_gage_box_index
	cmp	r0, r4
	streq	r5, [sp, #232]
	streq	r5, [sp, #228]
	moveq	r5, #1
.L121:
	bl	WEATHER_get_wind_gage_in_use
	cmp	r0, #0
	streq	r0, [sp, #232]
	streq	r0, [sp, #228]
	beq	.L160
	cmp	r5, #0
	beq	.L123
.L160:
	ldr	r2, [sp, #232]
	ldr	r3, .L201+208
	str	r2, [r3, #0]
	ldr	r3, [sp, #228]
	ldr	r2, .L201+212
	str	r3, [r2, #0]
	ldr	r2, .L201+160
	str	r3, [r2, #136]
	b	.L123
.L161:
.LBE105:
	ldr	r3, .L201+224
	ldr	r2, .L201+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L201+216
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L201+232
	ldr	r2, [r3, #100]
	cmp	r2, #0
	bne	.L124
	mov	r2, #1
	str	r2, [r3, #100]
	bl	Alert_rain_switch_active
.L124:
	ldr	r3, .L201+160
	ldr	r5, [r3, #128]
	bl	xTaskGetTickCount
	mov	r1, #0
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r3, r1
	mov	r2, r0
	mov	r0, r5
	bl	xTimerGenericCommand
	ldr	r3, .L201+224
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L125
.L162:
	ldr	r3, .L201+224
	ldr	r2, .L201+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L201+220
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L201+232
	ldr	r2, [r3, #104]
	cmp	r2, #0
	bne	.L126
	mov	r2, #1
	str	r2, [r3, #104]
	bl	Alert_freeze_switch_active
.L126:
	ldr	r3, .L201+160
	ldr	r5, [r3, #132]
	bl	xTaskGetTickCount
	mov	r1, #0
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r3, r1
	mov	r2, r0
	mov	r0, r5
	bl	xTimerGenericCommand
	ldr	r3, .L201+224
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L127
.L202:
	.align	2
.L201:
	.word	comm_mngr_recursive_MUTEX
	.word	.LC0
	.word	comm_stats
	.word	chain
	.word	last_contact
	.word	2115
	.word	.LC5
	.word	chain_members_recursive_MUTEX
	.word	1555
	.word	chain+20
	.word	list_program_data_recursive_MUTEX
	.word	1096
	.word	.LC6
	.word	list_lights_recursive_MUTEX
	.word	1211
	.word	list_poc_recursive_MUTEX
	.word	1326
	.word	.LC7
	.word	.LC8
	.word	comm_mngr
	.word	GuiLib_CurStructureNdx
	.word	10000
	.word	.LC9
	.word	tpmicro_data
	.word	poc_preserves_recursive_MUTEX
	.word	438
	.word	poc_preserves
	.word	poc_preserves+5664
	.word	.LC10
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	2238
	.word	.LANCHOR4
	.word	.LANCHOR5
	.word	2253
	.word	.LANCHOR6
	.word	.LANCHOR7
	.word	2268
	.word	.LANCHOR8
	.word	.LANCHOR9
	.word	foal_irri
	.word	747
	.word	.LC11
	.word	.LC12
	.word	system_preserves_recursive_MUTEX
	.word	814
	.word	system_preserves
	.word	system_preserves+56896
	.word	830
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	GuiVar_StatusWindGageReading
	.word	GuiVar_StatusWindPaused
	.word	2337
	.word	2361
	.word	weather_preserves_recursive_MUTEX
	.word	2383
	.word	weather_preserves
	.word	.LANCHOR10
	.word	.LC16
	.word	comm_mngr_recursive_MUTEX
	.word	.LC0
	.word	2109
	.word	.LC17
.L163:
	ldr	r5, .L201+224
	ldr	r2, .L201+248
	ldr	r3, .L201+228
	ldr	r0, [r5, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L201+232
	ldr	r2, [r3, #84]
	cmp	r2, #0
	bne	.L128
	mov	r2, #1
	str	r2, [r3, #84]
	bl	Alert_ETGage_RunAway
.L128:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	b	.L129
.L164:
	ldr	r3, .L201+232
	mov	r2, #1
	str	r2, [r3, #96]
	bl	Alert_ETGage_RunAway_Cleared
	b	.L130
.L165:
.LBB106:
	add	r0, sp, #92
	ldr	r1, [sp, #224]
	mov	r2, #24
	bl	memcpy
	ldr	r3, [sp, #224]
	add	r0, sp, #92
	add	r3, r3, #24
	str	r3, [sp, #224]
	bl	FOAL_IRRI_initiate_manual_watering
	b	.L131
.L166:
.LBE106:
.LBB107:
	ldr	r0, .L201+236
	ldr	r1, [sp, #224]
	mov	r2, #128
	bl	memcpy
	ldr	r6, .L201+236
	ldr	r3, [sp, #224]
	mov	r5, #0
	add	r3, r3, #128
	mov	r7, r6
	mov	r9, #1
	mov	sl, r6
	str	r3, [sp, #224]
.L139:
	ldr	r2, [sl, #0]
	cmp	r2, #0
	beq	.L132
	ldrb	r3, [sl, #4]	@ zero_extendqisi2
	cmp	r3, #8
	cmpne	r3, #2
	moveq	r1, #0
	movne	r1, #1
	beq	.L133
	ldr	r0, .L201+240
	bl	Alert_Message
	b	.L132
.L133:
	cmp	r2, #1
	bne	.L134
	cmp	r3, #2
	moveq	r0, r4
	addeq	r1, r7, r5, asl #4
	moveq	r2, #108
	beq	.L194
	cmp	r3, #8
	moveq	r0, r4
	addeq	r1, r7, r5, asl #4
	moveq	r2, #111
	bne	.L132
	b	.L195
.L134:
	cmp	r2, #3
	bne	.L136
	cmp	r3, #2
	bne	.L137
	mov	r0, r4
	add	r1, r7, r5, asl #4
	mov	r2, #107
.L194:
	bl	__station_decoder_fault_support
	b	.L132
.L137:
	cmp	r3, #8
	moveq	r0, r4
	addeq	r1, r7, r5, asl #4
	moveq	r2, #110
	bne	.L132
	b	.L195
.L136:
	cmp	r2, #2
	beq	.L132
	cmp	r2, #4
	bne	.L132
	cmp	r3, #2
	bne	.L138
	add	r6, r7, r5, asl #4
	str	r1, [sl, #12]
	mov	r2, #109
	mov	r1, r6
	mov	r0, r4
	bl	__station_decoder_fault_support
	str	r9, [sl, #12]
	mov	r1, r6
	mov	r2, #109
	mov	r8, r0
	mov	r0, r4
	bl	__station_decoder_fault_support
	cmp	r0, #0
	bne	.L132
	cmp	r8, #0
	bne	.L132
	mvn	r1, #0
	mov	r0, #3
	ldr	r2, [sl, #8]
	mov	r3, r1
	bl	Alert_two_wire_station_decoder_fault_idx
	b	.L132
.L138:
	cmp	r3, #8
	bne	.L132
	mov	r0, r4
	add	r1, r7, r5, asl #4
	mov	r2, #112
.L195:
	bl	__poc_decoder_fault_support
.L132:
	add	r5, r5, #1
	cmp	r5, #8
	add	sl, sl, #16
	bne	.L139
	b	.L140
.L167:
.LBE107:
.LBB108:
	add	r0, sp, #180
	ldr	r1, [sp, #224]
	mov	r2, #16
	bl	memcpy
	ldr	r3, [sp, #224]
	add	r0, sp, #180
	add	r3, r3, #16
	str	r3, [sp, #224]
	bl	FOAL_LIGHTS_process_light_on_from_token_response
	b	.L141
.L168:
.LBE108:
.LBB109:
	add	r0, sp, #208
	ldr	r1, [sp, #224]
	mov	r2, #8
	bl	memcpy
	ldr	r3, [sp, #224]
	add	r0, sp, #208
	add	r3, r3, #8
	str	r3, [sp, #224]
	bl	FOAL_LIGHTS_process_light_off_from_token_response
	b	.L142
.L199:
.LBE109:
	add	r0, sp, #224
	mov	r1, r4
	bl	FOAL_FLOW_extract_poc_update_from_token_response
	cmp	r0, #0
	beq	.L146
.L170:
	ldrh	r3, [sp, #220]
	tst	r3, #8192
	beq	.L143
	add	r0, sp, #224
	mov	r1, r4
	bl	MOISTURE_SENSOR_extract_moisture_reading_from_token_response
	cmp	r0, #0
	beq	.L146
.L143:
	ldrh	r3, [sp, #220]
	tst	r3, #16
	beq	.L145
.LBB110:
	ldr	r1, [sp, #224]
	add	r0, sp, #160
	mov	r2, #20
	bl	memcpy
	ldr	r3, [sp, #224]
	ldr	r0, [sp, #176]
	add	r3, r3, #20
	str	r3, [sp, #224]
	ldr	r1, [sp, #164]
	ldr	r2, [sp, #168]
	ldrb	r3, [sp, #172]	@ zero_extendqisi2
	bl	FOAL_IRRI_initiate_or_cancel_a_master_valve_override
	b	.L145
.L171:
.LBE110:
.LBB111:
	add	r0, sp, #228
	ldr	r1, [sp, #224]
	mov	r2, #4
	bl	memcpy
	ldr	r3, [sp, #224]
	ldr	r0, [sp, #228]
	add	r3, r3, #4
	str	r3, [sp, #224]
	bl	FOAL_IRRI_start_a_walk_thru
	b	.L147
.L172:
.LBE111:
.LBB112:
	ldr	r1, [sp, #224]
	add	r0, sp, #228
	mov	r2, #4
	bl	memcpy
	ldr	r1, [sp, #224]
	add	r0, sp, #232
	add	r1, r1, #4
	mov	r2, #4
	str	r1, [sp, #224]
	bl	memcpy
	ldr	r3, [sp, #224]
	ldr	r0, [sp, #228]
	add	r3, r3, #4
	ldr	r1, [sp, #232]
	mov	r2, r4
	str	r3, [sp, #224]
	bl	CHAIN_SYNC_test_deliverd_crc
	b	.L148
.L200:
.LBE112:
	add	r0, sp, #224
	mov	r1, r4
	bl	ALERTS_store_latest_timestamp_for_this_controller
	cmp	r0, #0
	bne	.L149
.L146:
	bl	Alert_token_resp_extraction_error
.L149:
.LBE82:
	ldr	r3, .L201+244
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #236
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L196:
	ldr	r0, .L201+248
	bl	RemovePathFromFileName
	ldr	r2, .L201+252
	mov	r1, r0
	ldr	r0, .L201+256
	bl	Alert_Message_va
	b	.L149
.L81:
.LBB114:
	ldrh	r3, [sp, #218]
	tst	r3, #16
	beq	.L151
	b	.L197
.L99:
	ldrh	r3, [sp, #220]
	tst	r3, #2
	bne	.L152
.L101:
	ldrh	r3, [sp, #220]
	tst	r3, #4
	bne	.L153
.L102:
	ldrh	r3, [sp, #218]
	tst	r3, #32
	bne	.L154
.L103:
	ldrh	r3, [sp, #218]
	tst	r3, #64
	bne	.L155
.L104:
	ldrh	r3, [sp, #218]
	tst	r3, #128
	bne	.L156
.L109:
	ldrh	r3, [sp, #218]
	tst	r3, #256
	bne	.L157
.L118:
	ldrh	r5, [sp, #220]
	mov	r5, r5, asl #16
.LBB113:
	ands	r5, r5, #8388608
	bne	.L158
	b	.L198
.L123:
.LBE113:
	ldrh	r3, [sp, #218]
	tst	r3, #16384
	bne	.L161
.L125:
	ldrh	r3, [sp, #218]
	tst	r3, #32768
	bne	.L162
.L127:
	ldrh	r3, [sp, #218]
	tst	r3, #512
	bne	.L163
.L129:
	ldrh	r3, [sp, #218]
	tst	r3, #1024
	bne	.L164
.L130:
	ldrh	r3, [sp, #218]
	tst	r3, #8192
	bne	.L165
.L131:
	ldrh	r3, [sp, #220]
	tst	r3, #8
	bne	.L166
.L140:
	ldrh	r3, [sp, #220]
	tst	r3, #2048
	bne	.L167
.L141:
	ldrh	r3, [sp, #220]
	tst	r3, #4096
	bne	.L168
.L142:
	ldrh	r3, [sp, #218]
	tst	r3, #1
	beq	.L170
	b	.L199
.L145:
	ldrh	r3, [sp, #220]
	tst	r3, #32
	bne	.L171
.L147:
	ldrh	r3, [sp, #218]
	tst	r3, #4096
	bne	.L172
.L148:
	ldrh	r3, [sp, #220]
	tst	r3, #512
	beq	.L149
	b	.L200
.LBE114:
.LFE27:
	.size	FOAL_COMM_process_incoming__irrigation_token__response, .-FOAL_COMM_process_incoming__irrigation_token__response
	.section	.text.FOAL_COMM_build_token__clean_house_request,"ax",%progbits
	.align	2
	.global	FOAL_COMM_build_token__clean_house_request
	.type	FOAL_COMM_build_token__clean_house_request, %function
FOAL_COMM_build_token__clean_house_request:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L204
	str	lr, [sp, #-4]!
.LCFI7:
	ldr	r1, .L204+4
	mov	r0, #2
	bl	mem_malloc_debug
	ldr	r3, .L204+8
	mov	r2, #2
	str	r2, [r3, #8]
	str	r0, [r3, #4]
	mov	r3, #60
	strh	r3, [r0, #0]	@ movhi
	ldr	pc, [sp], #4
.L205:
	.align	2
.L204:
	.word	2519
	.word	.LC0
	.word	next_contact
.LFE28:
	.size	FOAL_COMM_build_token__clean_house_request, .-FOAL_COMM_build_token__clean_house_request
	.section	.text.FOAL_COMM_build_token__irrigation_token,"ax",%progbits
	.align	2
	.global	FOAL_COMM_build_token__irrigation_token
	.type	FOAL_COMM_build_token__irrigation_token, %function
FOAL_COMM_build_token__irrigation_token:
.LFB38:
	@ args = 0, pretend = 0, frame = 156
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI8:
	ldr	r4, .L272
	sub	sp, sp, #156
.LCFI9:
	bl	COMM_MNGR_network_is_available_for_normal_use
	ldr	r3, .L272+4
	mov	r1, #400
	ldr	r3, [r3, #0]
	ldr	r2, .L272+8
	str	r3, [sp, #64]
	ldr	r3, .L272+12
	mov	r7, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L272+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r4, #444]
	cmp	r3, #1
	movne	r3, #0
	strne	r3, [sp, #72]
	strne	r3, [sp, #68]
	bne	.L208
	add	r0, sp, #68
	mov	r1, #18
	mov	r2, #0
	bl	PDATA_build_data_to_send
	cmp	r0, #768
	beq	.L208
	cmp	r0, #512
	moveq	r3, #0
	streq	r3, [r4, #444]
.L208:
.LBB125:
	ldr	r4, .L272
.LBE125:
	ldr	ip, [sp, #72]
.LBB126:
	ldr	r3, [r4, #360]
.LBE126:
	str	ip, [sp, #4]
.LBB127:
	cmp	r3, #0
	streq	r3, [sp, #12]
	streq	r3, [sp, #16]
	beq	.L209
	ldr	r1, .L272+8
	ldr	r2, .L272+20
	mov	r0, #1104
	bl	mem_malloc_debug
	mov	r3, #0
	str	r3, [r4, #360]
	ldr	r4, .L272+24
	mov	r3, #2912
	mov	r1, #400
	ldr	r2, .L272+8
	str	r0, [sp, #12]
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L272+28
	mov	r2, #1104
	ldr	r0, [sp, #12]
	bl	memcpy
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	ip, #1104
	str	ip, [sp, #16]
.L209:
.LBE127:
	ldr	r3, [sp, #72]
	ldr	r0, .L272
	ldr	ip, [sp, #16]
	orrs	ip, ip, r3
	ldr	r3, [r0, #88]
	addeq	r3, r3, #1
	streq	r3, [r0, #88]
	beq	.L211
	cmp	r3, #3
	bhi	.L211
	mov	r1, #0
	str	r1, [r0, #88]
	mov	r2, #48
	add	r0, r0, #92
	bl	memset
.L211:
	ldr	r3, .L272+32
	add	r1, sp, #92
	ldrh	r0, [r3, #0]
	bl	FOAL_IRRI_load_sfml_for_distribution_to_slaves
.LBB128:
	ldr	r4, .L272+36
.LBE128:
	str	r0, [sp, #40]
	add	r0, sp, #96
	bl	FOAL_IRRI_buildup_action_needed_records_for_token
.LBB129:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	streq	r3, [sp, #28]
	streq	r3, [sp, #32]
.LBE129:
	str	r0, [sp, #44]
.LBB130:
	beq	.L212
	ldr	r1, .L272+8
	ldr	r2, .L272+40
	mov	r0, #24
	bl	mem_malloc_debug
	ldr	r1, .L272+44
	mov	r2, #24
	str	r0, [sp, #28]
	bl	memcpy
	mov	r3, #0
	mov	ip, #24
	str	r3, [r4, #0]
	str	ip, [sp, #32]
.L212:
.LBE130:
	add	r0, sp, #100
	bl	FOAL_IRRI_load_mlb_info_into_outgoing_token
	ldr	r5, .L272+48
	mov	r4, #0
	mov	sl, r4
	mov	r8, r4
	mov	r6, r4
.LBB131:
	mov	r3, r4
.LBE131:
	str	r4, [sp, #8]
	str	r0, [sp, #48]
.L215:
.LBB132:
	ldr	fp, [r5, #64]
	cmp	fp, #1
	bne	.L213
	cmp	r3, #0
	bne	.L214
	mov	r0, #61
	ldr	r1, .L272+8
	ldr	r2, .L272+52
	bl	mem_malloc_debug
	mov	r3, fp
	mov	r9, r0
	add	sl, r0, #1
	mov	r4, r0
.L214:
	ldr	r2, .L272+48
	mov	r0, sl
	add	r1, r2, r6, asl #3
	strb	r6, [r0], #1
	add	r1, r1, #60
	mov	r2, #4
	str	r3, [sp, #0]
	bl	memcpy
	ldr	r3, [sp, #0]
	add	r8, r8, #1
	mov	ip, #0
	and	r8, r8, #255
	add	sl, sl, #5
	add	r3, r3, #5
	str	ip, [r5, #64]
.L213:
	add	r6, r6, #1
	cmp	r6, #12
	add	r5, r5, #8
	bne	.L215
.LBE132:
.LBB133:
	ldr	r5, .L272+56
.LBE133:
.LBB134:
	cmp	r8, #0
	strneb	r8, [r9, #0]
	str	r3, [sp, #8]
.LBE134:
.LBB135:
	ldr	r3, [r5, #0]
	mov	r6, #0
	cmp	r3, r6
	str	r6, [sp, #104]
	streq	r3, [sp, #20]
	beq	.L217
	mov	r0, #16
	add	r1, sp, #104
	ldr	r2, .L272+8
	ldr	r3, .L272+60
	bl	mem_oabia
	cmp	r0, #0
	streq	r0, [sp, #20]
	beq	.L217
	ldr	r0, [sp, #104]
	ldr	r1, .L272+64
	mov	r2, #16
	bl	memcpy
	mov	ip, #16
	str	r6, [r5, #0]
	str	ip, [sp, #20]
.L217:
.LBE135:
.LBB136:
	ldr	r5, .L272+68
	mov	r8, #0
	ldr	r6, [r5, #0]
	str	r8, [sp, #108]
	cmp	r6, r8
	beq	.L218
	mov	r0, #4
	add	r1, sp, #108
	ldr	r2, .L272+8
	ldr	r3, .L272+72
	bl	mem_oabia
	subs	r6, r0, #0
	beq	.L218
	ldr	r0, [sp, #108]
	ldr	r1, .L272+76
	mov	r2, #4
	mov	r6, #4
	bl	memcpy
	str	r8, [r5, #0]
.L218:
.LBE136:
.LBB137:
	ldr	sl, .L272+80
	mov	r8, #0
	ldr	r5, [sl, #0]
	str	r8, [sp, #112]
	cmp	r5, r8
	beq	.L219
	mov	r0, #4
	add	r1, sp, #112
	ldr	r2, .L272+8
	ldr	r3, .L272+84
	bl	mem_oabia
	subs	r5, r0, #0
	beq	.L219
	ldr	r0, [sp, #112]
	ldr	r1, .L272+88
	mov	r2, #4
	mov	r5, #4
	bl	memcpy
	str	r8, [sl, #0]
.L219:
.LBE137:
.LBB138:
	ldr	r9, .L272+92
	mov	r8, #0
	ldr	sl, [r9, #0]
	str	r8, [sp, #116]
	cmp	sl, r8
	beq	.L220
	mov	r0, #4
	add	r1, sp, #116
	ldr	r2, .L272+8
	ldr	r3, .L272+96
	bl	mem_oabia
	subs	sl, r0, #0
	beq	.L220
	ldr	r0, [sp, #116]
	ldr	r1, .L272+100
	mov	r2, #4
	mov	sl, #4
	bl	memcpy
	str	r8, [r9, #0]
.L220:
.LBE138:
.LBB139:
	mov	r3, #0
	str	r3, [sp, #120]
	bl	WEATHER_get_et_gage_is_in_use
	cmp	r0, #0
	bne	.L221
	bl	WEATHER_get_rain_bucket_is_in_use
	cmp	r0, #0
	bne	.L221
	bl	WEATHER_get_wind_gage_in_use
	subs	fp, r0, #0
	beq	.L222
.L221:
	mov	r0, #36
	add	r1, sp, #120
	ldr	r2, .L272+8
	ldr	r3, .L272+104
	bl	mem_oabia
	subs	fp, r0, #0
	beq	.L222
	ldr	r8, [sp, #120]
	ldr	r1, .L272+108
	mov	r2, #4
	mov	r0, r8
	bl	memcpy
	ldr	r1, .L272+112
	mov	r2, #4
	add	r0, r8, #4
	bl	memcpy
	ldr	r1, .L272+116
	mov	r2, #4
	add	r0, r8, #8
	bl	memcpy
	ldr	r1, .L272+120
	mov	r2, #4
	add	r0, r8, #12
	bl	memcpy
	ldr	r1, .L272+124
	mov	r2, #4
	add	r0, r8, #16
	bl	memcpy
	ldr	r1, .L272+128
	mov	r2, #4
	add	r0, r8, #20
	bl	memcpy
	ldr	r1, .L272+132
	mov	r2, #4
	add	r0, r8, #24
	bl	memcpy
	ldr	r1, .L272+136
	mov	r2, #4
	add	r0, r8, #28
	bl	memcpy
	add	r0, r8, #32
	ldr	r1, .L272+140
	mov	r2, #4
	bl	memcpy
	mov	fp, #36
.L222:
.LBE139:
	add	r0, sp, #124
	bl	WEATHER_TABLES_load_et_rain_tables_into_outgoing_token
	ldr	ip, [sp, #16]
	ldr	r2, [sp, #4]
	cmp	r7, #0
	add	r9, ip, r2
	ldr	ip, [sp, #40]
	streq	r7, [sp, #128]
	add	r9, r9, ip
	ldr	ip, [sp, #44]
	streq	r7, [sp, #4]
	add	r9, r9, ip
	ldr	ip, [sp, #48]
	add	r9, r9, ip
	ldr	ip, [sp, #32]
	add	r9, r9, ip
	ldr	ip, [sp, #8]
	add	r9, r9, ip
	ldr	ip, [sp, #20]
	add	r9, r9, ip
	add	r9, r9, r6
	add	r9, r9, r5
	add	r9, r9, sl
	add	r9, r9, r0
	str	r0, [sp, #52]
	add	r9, r9, fp
	beq	.L224
	add	r0, sp, #128
	bl	FOAL_FLOW_load_system_info_for_distribution_to_slaves
	str	r0, [sp, #4]
.L224:
	ldr	ip, [sp, #4]
	cmp	r7, #0
	add	r9, r9, ip
	streq	r7, [sp, #132]
	streq	r7, [sp, #24]
	beq	.L226
	add	r0, sp, #132
	bl	FOAL_FLOW_load_poc_info_for_distribution_to_slaves
	str	r0, [sp, #24]
.L226:
	ldr	ip, [sp, #24]
	add	r0, sp, #136
	add	r9, r9, ip
	bl	FOAL_LIGHTS_load_lights_on_list_into_outgoing_token
.LBB140:
	mov	r8, #0
.LBE140:
	str	r0, [sp, #56]
	add	r9, r9, r0
	add	r0, sp, #140
	bl	FOAL_LIGHTS_load_lights_action_needed_list_into_outgoing_token
.LBB141:
	str	r8, [sp, #144]
.LBE141:
	add	r9, r9, r0
	str	r9, [sp, #36]
.LBB142:
	ldr	r9, .L272
.LBE142:
	str	r0, [sp, #60]
.LBB143:
	ldr	r3, [r9, #456]
	cmp	r3, r8
	moveq	r8, r3
	beq	.L227
	mov	r0, #12
	add	r1, sp, #144
	ldr	r2, .L272+8
	mov	r3, #2800
	bl	mem_oabia
	cmp	r0, #0
	moveq	r8, r0
	beq	.L227
	ldr	r7, .L272+12
	ldr	r3, .L272+144
	mov	r1, #400
	ldr	r2, .L272+8
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	add	r1, r9, #460
	mov	r2, #12
	ldr	r0, [sp, #144]
	bl	memcpy
	str	r8, [r9, #456]
	ldr	r0, [r7, #0]
	bl	xQueueGiveMutexRecursive
	mov	r8, #12
.L227:
.LBE143:
	ldr	ip, [sp, #36]
	mov	r2, #0
	add	r9, ip, r8
	ldr	ip, .L272+148
	str	r2, [sp, #148]
	ldr	r3, [ip, #0]
	str	r2, [sp, #152]
	cmp	r3, #1
	bne	.L228
	bl	ALERTS_timestamps_are_synced
	cmp	r0, #1
	mov	r7, r0
	bne	.L228
	add	ip, sp, #76
	mov	r0, ip
	str	ip, [sp, #36]
	bl	ALERTS_get_oldest_timestamp
	ldrh	r3, [sp, #80]
	mov	r0, r7
	mov	r1, r7
	ldr	r2, .L272+152
	str	r3, [sp, #0]
	bl	DMYToDate
	ldr	r3, [sp, #0]
	mov	r0, r0, asl #16
	cmp	r3, r0, lsr #16
	bls	.L228
	mov	r0, #4096
	add	r1, sp, #148
	ldr	r2, .L272+8
	ldr	r3, .L272+156
	bl	mem_oabia
	cmp	r0, #1
	bne	.L229
	ldr	r7, .L272+160
	mov	r1, #400
	ldr	r0, [r7, #0]
	ldr	r2, .L272+8
	ldr	r3, .L272+164
	bl	xQueueTakeMutexRecursive_debug
	ldr	ip, [sp, #36]
	ldr	r1, .L272+168
	ldmia	ip, {r2, r3}
	ldr	r0, [sp, #148]
	bl	ALERTS_for_controller_to_controller_sync_return_size_of_and_build_alerts_to_send
	ldr	r2, .L272+148
	mov	r3, #0
	str	r3, [r2, #0]
	str	r0, [sp, #152]
	ldr	r0, [r7, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r7, [sp, #152]
	add	r9, r9, r7
	cmp	r7, #0
	addne	r9, r9, #4
	bne	.L228
	ldr	r0, [sp, #148]
	ldr	r1, .L272+8
	ldr	r2, .L272+172
	bl	mem_free_debug
	str	r7, [sp, #148]
	b	.L228
.L229:
	ldr	r0, .L272+176
	bl	Alert_Message
.L228:
	ldr	r3, .L272+180
	add	r9, r9, #6
	ldr	r2, [r3, #16]
	mov	r0, r9
	add	r2, r2, #1
	str	r2, [r3, #16]
	ldr	r1, .L272+8
	ldr	r2, .L272+184
	bl	mem_malloc_debug
	ldr	r3, .L272+188
	ldr	r2, [r3, #0]
	cmp	r9, r2
	str	r0, [sp, #36]
	bls	.L231
	str	r9, [r3, #0]
	mov	r0, r9
	mov	r1, #0
	bl	Alert_largest_token_size
.L231:
	ldr	r3, .L272+32
	ldr	ip, [sp, #36]
	str	r9, [r3, #8]
	str	ip, [r3, #4]
	ldrh	r3, [r3, #2]
	ldr	r2, [sp, #72]
	strh	r3, [sp, #84]	@ movhi
	mov	r3, #0
	cmp	r2, r3
	strh	r3, [sp, #86]	@ movhi
	strh	r3, [sp, #88]	@ movhi
	add	r7, ip, #6
	beq	.L232
	ldr	r1, [sp, #68]
	cmp	r1, r3
	beq	.L232
	mov	r3, #1024
	mov	r0, r7
	strh	r3, [sp, #86]	@ movhi
	bl	memcpy
	ldr	r3, [sp, #72]
	ldr	r0, [sp, #68]
	ldr	r1, .L272+8
	ldr	r2, .L272+192
	add	r7, r7, r3
	bl	mem_free_debug
.L232:
	ldr	ip, [sp, #16]
	cmp	ip, #0
	beq	.L233
	ldr	ip, [sp, #12]
	cmp	ip, #0
	beq	.L233
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r1, ip
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #32768
	strh	r3, [sp, #86]	@ movhi
	mov	r0, r7
	mov	r3, r3, lsr #16
	ldr	r2, [sp, #16]
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #16]
	ldr	r0, [sp, #12]
	ldr	r1, .L272+8
	ldr	r2, .L272+196
	add	r7, r7, ip
	bl	mem_free_debug
.L233:
	ldr	ip, [sp, #40]
	cmp	ip, #0
	beq	.L234
	ldr	r1, [sp, #92]
	cmp	r1, #0
	beq	.L234
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #2
	mov	r2, ip
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #40]
	ldr	r0, [sp, #92]
	ldr	r1, .L272+8
	ldr	r2, .L272+200
	add	r7, r7, ip
	bl	mem_free_debug
.L234:
	ldr	ip, [sp, #44]
	cmp	ip, #0
	beq	.L235
	ldr	r1, [sp, #96]
	cmp	r1, #0
	beq	.L235
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #1
	mov	r2, ip
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #44]
	ldr	r0, [sp, #96]
	ldr	r1, .L272+8
	mov	r2, #3472
	add	r7, r7, ip
	bl	mem_free_debug
.L235:
	ldr	ip, [sp, #32]
	cmp	ip, #0
	beq	.L236
	ldr	ip, [sp, #28]
	cmp	ip, #0
	beq	.L236
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r1, ip
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #2048
	strh	r3, [sp, #86]	@ movhi
	mov	r0, r7
	mov	r3, r3, lsr #16
	ldr	r2, [sp, #32]
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #32]
	ldr	r0, [sp, #28]
	ldr	r1, .L272+8
	ldr	r2, .L272+204
	add	r7, r7, ip
	bl	mem_free_debug
.L236:
	ldr	ip, [sp, #48]
	cmp	ip, #0
	beq	.L237
	ldr	r1, [sp, #100]
	cmp	r1, #0
	beq	.L237
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #4
	mov	r2, ip
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #48]
	ldr	r0, [sp, #100]
	ldr	r1, .L272+8
	ldr	r2, .L272+208
	add	r7, r7, ip
	bl	mem_free_debug
.L237:
	ldr	ip, [sp, #8]
	cmp	ip, #0
	beq	.L238
	cmp	r4, #0
	beq	.L238
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #8
	mov	r2, ip
	strh	r3, [sp, #86]	@ movhi
	mov	r1, r4
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #8]
	mov	r0, r4
	ldr	r1, .L272+8
	ldr	r2, .L272+212
	add	r7, r7, ip
	bl	mem_free_debug
.L238:
	ldr	ip, [sp, #20]
	cmp	ip, #0
	beq	.L239
	ldr	r1, [sp, #104]
	cmp	r1, #0
	beq	.L239
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #16
	mov	r2, ip
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #20]
	ldr	r0, [sp, #104]
	ldr	r1, .L272+8
	ldr	r2, .L272+216
	add	r7, r7, ip
	bl	mem_free_debug
.L239:
	cmp	r6, #0
	beq	.L240
	ldr	r1, [sp, #108]
	cmp	r1, #0
	beq	.L240
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #1048576
	strh	r3, [sp, #86]	@ movhi
	mov	r2, r6
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	r0, [sp, #108]
	ldr	r1, .L272+8
	ldr	r2, .L272+220
	add	r7, r7, r6
	bl	mem_free_debug
.L240:
	cmp	r5, #0
	beq	.L241
	ldr	r1, [sp, #112]
	cmp	r1, #0
	beq	.L241
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #4194304
	strh	r3, [sp, #86]	@ movhi
	mov	r2, r5
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	r0, [sp, #112]
	ldr	r1, .L272+8
	ldr	r2, .L272+224
	add	r7, r7, r5
	bl	mem_free_debug
.L241:
	cmp	sl, #0
	beq	.L242
	ldr	r1, [sp, #116]
	cmp	r1, #0
	beq	.L242
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #8388608
	strh	r3, [sp, #86]	@ movhi
	mov	r2, sl
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	r0, [sp, #116]
	ldr	r1, .L272+8
	ldr	r2, .L272+228
	add	r7, r7, sl
	bl	mem_free_debug
.L242:
	cmp	fp, #0
	beq	.L243
	ldr	r1, [sp, #120]
	cmp	r1, #0
	beq	.L243
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #32
	strh	r3, [sp, #86]	@ movhi
	mov	r2, fp
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	r0, [sp, #120]
	ldr	r1, .L272+8
	ldr	r2, .L272+232
	add	r7, r7, fp
	bl	mem_free_debug
.L243:
	ldr	ip, [sp, #52]
	cmp	ip, #0
	beq	.L244
	ldr	r1, [sp, #124]
	cmp	r1, #0
	beq	.L244
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #64
	mov	r2, ip
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #52]
	ldr	r0, [sp, #124]
	ldr	r1, .L272+8
	ldr	r2, .L272+236
	add	r7, r7, ip
	bl	mem_free_debug
.L244:
	ldr	r3, .L272+240
	ldr	r3, [r3, #100]
	cmp	r3, #1
	bne	.L245
	ldrh	r3, [sp, #86]
	ldrh	r2, [sp, #88]
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #131072
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
.L245:
	ldr	r3, .L272+240
	ldr	r3, [r3, #104]
	cmp	r3, #1
	bne	.L246
	ldrh	r3, [sp, #86]
	ldrh	r2, [sp, #88]
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #262144
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
.L246:
	ldr	r3, .L272+240
	ldr	r2, [r3, #96]
	cmp	r2, #1
	bne	.L247
	ldrh	r2, [sp, #86]
	ldrh	r1, [sp, #88]
	orr	r2, r2, r1, asl #16
	orr	r2, r2, #16384
	strh	r2, [sp, #86]	@ movhi
	mov	r2, r2, lsr #16
	strh	r2, [sp, #88]	@ movhi
	mov	r2, #0
	str	r2, [r3, #96]
.L247:
	ldr	ip, [sp, #4]
	cmp	ip, #0
	beq	.L248
	ldr	r1, [sp, #128]
	cmp	r1, #0
	beq	.L248
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #256
	mov	r2, ip
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #4]
	ldr	r0, [sp, #128]
	ldr	r1, .L272+8
	ldr	r2, .L272+244
	add	r7, r7, ip
	bl	mem_free_debug
.L248:
	ldr	ip, [sp, #24]
	cmp	ip, #0
	beq	.L249
	ldr	r1, [sp, #132]
	cmp	r1, #0
	beq	.L249
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #512
	mov	r2, ip
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #24]
	ldr	r0, [sp, #132]
	ldr	r1, .L272+8
	ldr	r2, .L272+248
	add	r7, r7, ip
	bl	mem_free_debug
.L249:
	ldr	ip, [sp, #56]
	cmp	ip, #0
	beq	.L250
	ldr	r1, [sp, #136]
	cmp	r1, #0
	beq	.L250
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #16777216
	mov	r2, ip
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #56]
	ldr	r0, [sp, #136]
	ldr	r1, .L272+8
	ldr	r2, .L272+252
	add	r7, r7, ip
	bl	mem_free_debug
.L250:
	ldr	ip, [sp, #60]
	cmp	ip, #0
	beq	.L251
	ldr	r1, [sp, #140]
	cmp	r1, #0
	beq	.L251
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #33554432
	mov	r2, ip
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	ip, [sp, #60]
	ldr	r0, [sp, #140]
	ldr	r1, .L272+8
	ldr	r2, .L272+256
	add	r7, r7, ip
	bl	mem_free_debug
.L251:
	cmp	r8, #0
	beq	.L252
	ldr	r1, [sp, #144]
	cmp	r1, #0
	beq	.L252
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	mov	r0, r7
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #67108864
	strh	r3, [sp, #86]	@ movhi
	mov	r2, r8
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	r0, [sp, #144]
	ldr	r1, .L272+8
	mov	r2, #3696
	add	r7, r7, r8
	bl	mem_free_debug
.L252:
	ldr	r3, .L272
	ldr	r2, [r3, #472]
	cmp	r2, #0
	beq	.L253
	ldrh	r2, [sp, #86]
	ldrh	r1, [sp, #88]
	orr	r2, r2, r1, asl #16
	orr	r2, r2, #134217728
	strh	r2, [sp, #86]	@ movhi
	mov	r2, r2, lsr #16
	strh	r2, [sp, #88]	@ movhi
	mov	r2, #0
	str	r2, [r3, #472]
.L253:
	ldr	r3, [sp, #152]
	cmp	r3, #0
	beq	.L254
	ldr	r3, [sp, #148]
	cmp	r3, #0
	beq	.L254
	ldrh	r2, [sp, #88]
	ldrh	r3, [sp, #86]
	add	r1, sp, #152
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #8192
	strh	r3, [sp, #86]	@ movhi
	mov	r0, r7
	mov	r3, r3, lsr #16
	mov	r2, #4
	strh	r3, [sp, #88]	@ movhi
	bl	memcpy
	ldr	r1, [sp, #148]
	ldr	r2, [sp, #152]
	add	r0, r7, #4
	bl	memcpy
	ldr	r0, [sp, #148]
	ldr	r1, .L272+8
	ldr	r2, .L272+260
	bl	mem_free_debug
	b	.L255
.L254:
	ldr	r3, .L272+148
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L255
	ldr	r3, .L272+32
	ldrh	r0, [r3, #0]
	bl	ALERTS_need_latest_timestamp_for_this_controller
	cmp	r0, #1
	bne	.L255
	ldrh	r3, [sp, #86]
	ldrh	r2, [sp, #88]
	orr	r3, r3, r2, asl #16
	orr	r3, r3, #4096
	strh	r3, [sp, #86]	@ movhi
	mov	r3, r3, lsr #16
	strh	r3, [sp, #88]	@ movhi
.L255:
	add	r1, sp, #84
	mov	r2, #6
	ldr	r0, [sp, #36]
	bl	memcpy
	ldr	r3, .L272+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L272+4
	ldr	ip, [sp, #64]
	ldr	r2, [r3, #0]
	ldr	r3, .L272+264
	rsb	r2, ip, r2
	ldr	r1, [r3, #0]
	cmp	r2, r1
	strhi	r2, [r3, #0]
	add	sp, sp, #156
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L273:
	.align	2
.L272:
	.word	comm_mngr
	.word	my_tick_count
	.word	.LC0
	.word	comm_mngr_recursive_MUTEX
	.word	2973
	.word	2904
	.word	chain_members_recursive_MUTEX
	.word	chain+16
	.word	next_contact
	.word	.LANCHOR1
	.word	2559
	.word	.LANCHOR0
	.word	tpmicro_data
	.word	2623
	.word	.LANCHOR3
	.word	2679
	.word	.LANCHOR2
	.word	.LANCHOR5
	.word	2710
	.word	.LANCHOR4
	.word	.LANCHOR7
	.word	2741
	.word	.LANCHOR6
	.word	.LANCHOR9
	.word	2772
	.word	.LANCHOR8
	.word	2834
	.word	weather_preserves+36
	.word	weather_preserves+92
	.word	weather_preserves+80
	.word	weather_preserves+40
	.word	weather_preserves+52
	.word	weather_preserves+100
	.word	weather_preserves+104
	.word	GuiVar_StatusWindGageReading
	.word	GuiVar_StatusWindPaused
	.word	2803
	.word	ALERTS_need_to_sync
	.word	2011
	.word	3278
	.word	alerts_pile_recursive_MUTEX
	.word	3283
	.word	alerts_struct_user
	.word	3302
	.word	.LC18
	.word	comm_stats
	.word	3389
	.word	.LANCHOR11
	.word	3432
	.word	3445
	.word	3459
	.word	3485
	.word	3498
	.word	3511
	.word	3524
	.word	3537
	.word	3550
	.word	3563
	.word	3576
	.word	3589
	.word	weather_preserves
	.word	3641
	.word	3655
	.word	3669
	.word	3683
	.word	3725
	.word	.LANCHOR12
.LFE38:
	.size	FOAL_COMM_build_token__irrigation_token, .-FOAL_COMM_build_token__irrigation_token
	.global	terminal_short_needs_distribution_to_irri_machines
	.global	terminal_short_record
	.section	.bss.stop_key_record,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	stop_key_record, %object
	.size	stop_key_record, 24
stop_key_record:
	.space	24
	.section	.bss.send_stop_key_record_in_next_token,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	send_stop_key_record_in_next_token, %object
	.size	send_stop_key_record_in_next_token, 4
send_stop_key_record_in_next_token:
	.space	4
	.section	.bss.two_wire_cable_overheated_index,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	two_wire_cable_overheated_index, %object
	.size	two_wire_cable_overheated_index, 4
two_wire_cable_overheated_index:
	.space	4
	.section	.bss.two_wire_cable_overheated_needs_distribution,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	two_wire_cable_overheated_needs_distribution, %object
	.size	two_wire_cable_overheated_needs_distribution, 4
two_wire_cable_overheated_needs_distribution:
	.space	4
	.section	.bss.terminal_short_needs_distribution_to_irri_machines,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	terminal_short_needs_distribution_to_irri_machines, %object
	.size	terminal_short_needs_distribution_to_irri_machines, 4
terminal_short_needs_distribution_to_irri_machines:
	.space	4
	.section	.bss.decoder_faults,"aw",%nobits
	.align	2
	.set	.LANCHOR10,. + 0
	.type	decoder_faults, %object
	.size	decoder_faults, 128
decoder_faults:
	.space	128
	.section	.bss.two_wire_cable_excessive_current_index,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	two_wire_cable_excessive_current_index, %object
	.size	two_wire_cable_excessive_current_index, 4
two_wire_cable_excessive_current_index:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/foal_comm.c\000"
.LC1:
	.ascii	"Decoder SN %d, output %d not found in station list\000"
.LC2:
	.ascii	"POC decoder short unexp output\000"
.LC3:
	.ascii	"POC Decoder Fault: s/n %u not found in file\000"
.LC4:
	.ascii	"Unhandled STOP KEY request\000"
.LC5:
	.ascii	"Response from unexp index : %s, %u\000"
.LC6:
	.ascii	"TPmicro: station %c%u created\000"
.LC7:
	.ascii	"Terminal Strip @ %c\000"
.LC8:
	.ascii	"DISTRIB & RE-REG: wi change detected\000"
.LC9:
	.ascii	"FOAL_COMM: current out of range.\000"
.LC10:
	.ascii	"FOAL_COMM: shorted poc not found.\000"
.LC11:
	.ascii	"Gage pulse count o.o.r.\000"
.LC12:
	.ascii	"ET rcvd from %c, gage at %c\000"
.LC13:
	.ascii	"RB pulse count o.o.r.\000"
.LC14:
	.ascii	"Rain rcvd from %c, bucket at %c\000"
.LC15:
	.ascii	"Wind rcvd from %c, gage at %c\000"
.LC16:
	.ascii	"Unexpected decoder type!\000"
.LC17:
	.ascii	"Token Resp : can't find index : %s, %u\000"
.LC18:
	.ascii	"Unable to allocate memory for alerts\000"
	.section	.bss.two_wire_cable_excessive_current_needs_distribution,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	two_wire_cable_excessive_current_needs_distribution, %object
	.size	two_wire_cable_excessive_current_needs_distribution, 4
two_wire_cable_excessive_current_needs_distribution:
	.space	4
	.section	.bss.terminal_short_record,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	terminal_short_record, %object
	.size	terminal_short_record, 16
terminal_short_record:
	.space	16
	.section	.bss.two_wire_cable_cooled_off_index,"aw",%nobits
	.align	2
	.set	.LANCHOR8,. + 0
	.type	two_wire_cable_cooled_off_index, %object
	.size	two_wire_cable_cooled_off_index, 4
two_wire_cable_cooled_off_index:
	.space	4
	.section	.bss.__largest_token_size,"aw",%nobits
	.align	2
	.set	.LANCHOR11,. + 0
	.type	__largest_token_size, %object
	.size	__largest_token_size, 4
__largest_token_size:
	.space	4
	.section	.bss.two_wire_cable_cooled_off_needs_distribution,"aw",%nobits
	.align	2
	.set	.LANCHOR9,. + 0
	.type	two_wire_cable_cooled_off_needs_distribution, %object
	.size	two_wire_cable_cooled_off_needs_distribution, 4
two_wire_cable_cooled_off_needs_distribution:
	.space	4
	.section	.bss.__largest_token_generation_delta,"aw",%nobits
	.align	2
	.set	.LANCHOR12,. + 0
	.type	__largest_token_generation_delta, %object
	.size	__largest_token_generation_delta, 4
__largest_token_generation_delta:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI0-.LFB5
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI1-.LFB22
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI2-.LFB23
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI5-.LFB27
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x110
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI7-.LFB28
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI8-.LFB38
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0xc0
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/foal_comm.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1d6
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF39
	.byte	0x1
	.4byte	.LASF40
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x57b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x5af
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x5ee
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x7ec
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x7ff
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.byte	0x7d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x1
	.byte	0xc9
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x2ad
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x2bf
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x67f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x690
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x6a5
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x6bb
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x1
	.2byte	0xb4a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x9f9
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x1
	.2byte	0xa24
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x1
	.2byte	0xa6a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x1
	.2byte	0xa89
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF18
	.byte	0x1
	.2byte	0xaa8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x1
	.2byte	0xac7
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF20
	.byte	0x1
	.2byte	0xae8
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x275
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x6db
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x726
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.byte	0xa9
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x106
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x2
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x605
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x41b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x49b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x50d
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x182
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x2d0
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x313
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x390
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x777
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x813
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x9cf
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST5
	.uleb128 0x2
	.4byte	.LASF37
	.byte	0x1
	.2byte	0xb04
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF38
	.byte	0x1
	.2byte	0xb87
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST6
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB5
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB22
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB23
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI3
	.4byte	.LFE23
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB27
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI6
	.4byte	.LFE27
	.2byte	0x3
	.byte	0x7d
	.sleb128 272
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB28
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB38
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI9
	.4byte	.LFE38
	.2byte	0x3
	.byte	0x7d
	.sleb128 192
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF8:
	.ascii	"__extract_test_request_from_token_response\000"
.LASF6:
	.ascii	"__extract_box_current_from_token_response\000"
.LASF12:
	.ascii	"extract_chain_sync_crc_from_token_response\000"
.LASF40:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/foal_comm.c\000"
.LASF39:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF22:
	.ascii	"__station_decoder_fault_support\000"
.LASF33:
	.ascii	"extract_wind_from_token_response\000"
.LASF17:
	.ascii	"_nm_load_two_wire_cable_excessive_current_into_outg"
	.ascii	"oing_token\000"
.LASF32:
	.ascii	"extract_rain_bucket_pulse_count_from_token_response"
	.ascii	"\000"
.LASF26:
	.ascii	"extract_box_configuration_from_irrigation_token_res"
	.ascii	"p\000"
.LASF20:
	.ascii	"FOAL_date_time_into_outgoing_token\000"
.LASF27:
	.ascii	"process_what_is_installed_for_stations\000"
.LASF2:
	.ascii	"process_what_is_installed_for_two_wire\000"
.LASF16:
	.ascii	"_nm_load_terminal_short_or_no_current_record_into_o"
	.ascii	"utgoing_token\000"
.LASF34:
	.ascii	"__extract_decoder_faults_from_token_response_and_st"
	.ascii	"op_affected_stations\000"
.LASF4:
	.ascii	"extract_lights_off_from_token_response\000"
.LASF30:
	.ascii	"nm_extract_terminal_short_or_no_current_from_token_"
	.ascii	"response\000"
.LASF23:
	.ascii	"__poc_decoder_fault_support\000"
.LASF13:
	.ascii	"__load_chain_members_array_into_outgoing_token\000"
.LASF3:
	.ascii	"extract_lights_on_from_token_response\000"
.LASF25:
	.ascii	"extract_stop_key_record_from_token_response\000"
.LASF10:
	.ascii	"extract_mvor_request_from_token_response\000"
.LASF24:
	.ascii	"FOAL_COMM_process_incoming__clean_house_request__re"
	.ascii	"sponse\000"
.LASF29:
	.ascii	"process_what_is_installed_for_POCs\000"
.LASF37:
	.ascii	"load_real_time_weather_data_into_outgoing_token\000"
.LASF1:
	.ascii	"process_what_is_installed_for_comm\000"
.LASF18:
	.ascii	"_nm_load_two_wire_cable_overheated_into_outgoing_to"
	.ascii	"ken\000"
.LASF0:
	.ascii	"process_what_is_installed_for_weather\000"
.LASF7:
	.ascii	"__extract_two_wire_cable_current_measurement_from_t"
	.ascii	"oken_response\000"
.LASF5:
	.ascii	"nm_using_FROM_ADDRESS_to_find_comm_mngr_members_ind"
	.ascii	"ex\000"
.LASF31:
	.ascii	"__extract_et_gage_pulse_count_from_token_response\000"
.LASF19:
	.ascii	"_nm_load_two_wire_cable_cooled_down_into_outgoing_t"
	.ascii	"oken\000"
.LASF28:
	.ascii	"process_what_is_installed_for_lights\000"
.LASF14:
	.ascii	"load_stop_key_record_into_outgoing_token\000"
.LASF21:
	.ascii	"_nm_respond_to_two_wire_cable_fault\000"
.LASF15:
	.ascii	"load_box_current_into_outgoing_token\000"
.LASF11:
	.ascii	"extract_walk_thru_start_request_from_token_response"
	.ascii	"\000"
.LASF36:
	.ascii	"FOAL_COMM_build_token__clean_house_request\000"
.LASF38:
	.ascii	"FOAL_COMM_build_token__irrigation_token\000"
.LASF9:
	.ascii	"__extract_manual_water_request_from_token_response\000"
.LASF35:
	.ascii	"FOAL_COMM_process_incoming__irrigation_token__respo"
	.ascii	"nse\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
