	.file	"chain_sync_vars.c"
	.text
.Ltext0:
	.section	.text.init_chain_sync_vars,"ax",%progbits
	.align	2
	.global	init_chain_sync_vars
	.type	init_chain_sync_vars, %function
init_chain_sync_vars:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r4, .L2
	ldr	r6, .L2+4
	ldr	r3, .L2+8
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L2+12
	bl	xQueueTakeMutexRecursive_debug
	mov	r5, #0
	mov	r0, r6
	mov	r1, r5
	str	r5, [r0], #4
	mov	r2, #84
	bl	memset
	mov	r1, r5
	add	r0, r6, #88
	mov	r2, #84
	bl	memset
	add	r0, r6, #172
	mov	r1, r5
	mov	r2, #84
	bl	memset
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L3:
	.align	2
.L2:
	.word	chain_sync_control_structure_recursive_MUTEX
	.word	.LANCHOR0
	.word	322
	.word	.LC0
.LFE0:
	.size	init_chain_sync_vars, .-init_chain_sync_vars
	.global	chain_sync_file_pertinants
	.global	cscs
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/flas"
	.ascii	"h_storage/chain_sync_vars.c\000"
.LC1:
	.ascii	"\000"
.LC2:
	.ascii	"Network Config\000"
.LC3:
	.ascii	"Weather Control\000"
.LC4:
	.ascii	"Stations\000"
.LC5:
	.ascii	"Mainline\000"
.LC6:
	.ascii	"POC\000"
.LC7:
	.ascii	"Manual Programs\000"
.LC8:
	.ascii	"Station Group\000"
.LC9:
	.ascii	"Lights\000"
.LC10:
	.ascii	"Moisture Sensor\000"
.LC11:
	.ascii	"Walk Thru\000"
	.section	.rodata.chain_sync_file_pertinants,"a",%progbits
	.align	2
	.type	chain_sync_file_pertinants, %object
	.size	chain_sync_file_pertinants, 336
chain_sync_file_pertinants:
	.word	.LC1
	.word	0
	.word	0
	.word	0
	.word	.LC2
	.word	NETWORK_CONFIG_calculate_chain_sync_crc
	.word	NETWORK_CONFIG_set_bits_on_all_settings_to_cause_distribution_in_the_next_token
	.word	0
	.word	.LC1
	.word	0
	.word	0
	.word	0
	.word	.LC1
	.word	0
	.word	0
	.word	0
	.word	.LC1
	.word	0
	.word	0
	.word	0
	.word	.LC1
	.word	0
	.word	0
	.word	0
	.word	.LC3
	.word	WEATHER_CONTROL_calculate_chain_sync_crc
	.word	WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token
	.word	0
	.word	.LC1
	.word	0
	.word	0
	.word	0
	.word	.LC1
	.word	0
	.word	0
	.word	0
	.word	.LC4
	.word	STATION_calculate_chain_sync_crc
	.word	STATION_set_bits_on_all_stations_to_cause_distribution_in_the_next_token
	.word	STATION_clean_house_processing
	.word	.LC5
	.word	IRRIGATION_SYSTEM_calculate_chain_sync_crc
	.word	IRRIGATION_SYSTEM_set_bits_on_all_systems_to_cause_distribution_in_the_next_token
	.word	SYSTEM_clean_house_processing
	.word	.LC6
	.word	POC_calculate_chain_sync_crc
	.word	POC_set_bits_on_all_pocs_to_cause_distribution_in_the_next_token
	.word	POC_clean_house_processing
	.word	.LC7
	.word	MANUAL_PROGRAMS_calculate_chain_sync_crc
	.word	MANUAL_PROGRAMS_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.word	MANUAL_PROGRAMS_clean_house_processing
	.word	.LC8
	.word	STATION_GROUPS_calculate_chain_sync_crc
	.word	STATION_GROUP_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.word	STATION_GROUP_clean_house_processing
	.word	.LC1
	.word	0
	.word	0
	.word	0
	.word	.LC1
	.word	0
	.word	0
	.word	0
	.word	.LC9
	.word	LIGHTS_calculate_chain_sync_crc
	.word	LIGHTS_set_bits_on_all_lights_to_cause_distribution_in_the_next_token
	.word	LIGHTS_clean_house_processing
	.word	.LC1
	.word	0
	.word	0
	.word	0
	.word	.LC10
	.word	MOISTURE_SENSOR_calculate_chain_sync_crc
	.word	MOISTURE_SENSOR_set_bits_on_all_moisture_sensors_to_cause_distribution_in_the_next_token
	.word	MOISTURE_SENSOR_clean_house_processing
	.word	.LC1
	.word	0
	.word	0
	.word	0
	.word	.LC11
	.word	WALK_THRU_calculate_chain_sync_crc
	.word	WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.word	WALK_THRU_clean_house_processing
	.section	.bss.cscs,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	cscs, %object
	.size	cscs, 256
cscs:
	.space	256
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/flash_storage/chain_sync_vars.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x33
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF0
	.byte	0x1
	.4byte	.LASF1
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x139
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/flas"
	.ascii	"h_storage/chain_sync_vars.c\000"
.LASF0:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"init_chain_sync_vars\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
