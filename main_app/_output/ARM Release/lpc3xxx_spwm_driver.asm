	.file	"lpc3xxx_spwm_driver.c"
	.text
.Ltext0:
	.global	__udivsi3
	.section	.text.spwm_percent_to_ctrl,"ax",%progbits
	.align	2
	.global	spwm_percent_to_ctrl
	.type	spwm_percent_to_ctrl, %function
spwm_percent_to_ctrl:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #0
	stmfd	sp!, {r4, lr}
.LCFI0:
	mov	r4, r0
	moveq	r4, r0, asl #16
	moveq	r0, r4, lsr #16
	ldmeqfd	sp!, {r4, pc}
	cmp	r1, #99
	movhi	r0, r4, asl #16
	movhi	r0, r0, lsr #16
	orrhi	r0, r0, #1073741824
	ldmhifd	sp!, {r4, pc}
.LBB4:
	rsb	r0, r1, #100
	mov	r0, r0, asl #8
	rsb	r0, r1, r0
	mov	r1, #100
	bl	__udivsi3
	ldr	r3, .L5
	and	r3, r4, r3
	and	r0, r0, #255
	orr	r0, r0, r3
.LBE4:
	ldmfd	sp!, {r4, pc}
.L6:
	.align	2
.L5:
	.word	-1073676544
.LFE0:
	.size	spwm_percent_to_ctrl, .-spwm_percent_to_ctrl
	.section	.text.spwm_open,"ax",%progbits
	.align	2
	.global	spwm_open
	.type	spwm_open, %function
spwm_open:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L19
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	cmp	r0, r3
	beq	.L14
	add	r3, r3, #4
	cmp	r0, r3
	moveq	r3, #1
	bne	.L18
	b	.L8
.L14:
	mov	r3, #0
.L8:
	mov	r0, #12
	ldr	r2, .L19+4
	mul	r0, r3, r0
	ldr	ip, [r2, r0]
	add	r4, r2, r0
	cmp	ip, #0
	bne	.L16
	cmp	r3, #0
	mov	r5, #1
	str	r5, [r2, r0]
	ldreq	r0, .L19
	str	ip, [r4, #4]
	streq	r0, [r2, #8]
	beq	.L11
	cmp	r3, #1
	ldreq	r0, .L19+8
	streq	r0, [r2, #20]
.L11:
	mov	r0, #12
	mla	r2, r0, r3, r2
	cmp	r1, #0
	ldr	r2, [r2, #8]
	ldr	r0, [r2, #0]
	bic	r0, r0, #-2147483648
	str	r0, [r2, #0]
	ldr	r1, [r2, #0]
	mov	r0, #12
	biceq	r1, r1, #1073741824
	orrne	r1, r1, #1073741824
	str	r1, [r2, #0]
	ldr	r2, .L19+4
	mla	r0, r3, r0, r2
	ldmfd	sp!, {r4, r5, pc}
.L18:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, pc}
.L16:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, pc}
.L20:
	.align	2
.L19:
	.word	1074118656
	.word	.LANCHOR0
	.word	1074118660
.LFE1:
	.size	spwm_open, .-spwm_open
	.section	.text.spwm_close,"ax",%progbits
	.align	2
	.global	spwm_close
	.type	spwm_close, %function
spwm_close:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L27
	cmp	r0, r2
	moveq	r3, #0
	beq	.L22
	add	r1, r2, #12
	cmp	r1, r0
	ldr	r3, .L27+4
	moveq	r3, #1
.L22:
	mov	r1, #12
	mul	r3, r1, r3
	add	r1, r2, r3
	ldr	r2, [r2, r3]
	cmp	r2, #1
	ldreq	r2, [r1, #8]
	mvnne	r0, #0
	ldreq	r0, [r2, #0]
	biceq	r0, r0, #-2147483648
	streq	r0, [r2, #0]
	ldreq	r2, .L27
	moveq	r0, #0
	streq	r0, [r2, r3]
	streq	r0, [r1, #4]
	bx	lr
.L28:
	.align	2
.L27:
	.word	.LANCHOR0
	.word	9999
.LFE2:
	.size	spwm_close, .-spwm_close
	.section	.text.spwm_ioctl,"ax",%progbits
	.align	2
	.global	spwm_ioctl
	.type	spwm_ioctl, %function
spwm_ioctl:
.LFB3:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI2:
	mov	r4, r2
	ldr	r2, .L50
	cmp	r0, r2
	beq	.L41
	add	r3, r2, #12
	cmp	r0, r3
	moveq	r5, #1
	bne	.L43
	b	.L30
.L41:
	mov	r5, #0
.L30:
	mov	r3, #12
	mul	r0, r3, r5
	ldr	r2, [r2, r0]
	cmp	r2, #1
	bne	.L43
	cmp	r1, #5
	ldrls	pc, [pc, r1, asl #2]
	b	.L44
.L38:
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
.L32:
	ldr	r2, .L50
	mla	r5, r3, r5, r2
	ldr	r3, [r5, #8]
	b	.L49
.L33:
	ldr	r2, .L50
	mov	r1, #0
	mla	r3, r5, r3, r2
	cmp	r4, r1
	str	r1, [r3, #4]
	movne	r3, #100
	strne	r3, [sp, #0]
	mov	r3, #12
	mla	r5, r3, r5, r2
	streq	r4, [sp, #0]
	ldr	r4, [r5, #8]
	ldr	r0, [r4, #0]
	ldr	r1, [sp, #0]
	bl	spwm_percent_to_ctrl
	str	r0, [r4, #0]
	b	.L48
.L34:
	ldr	r2, .L50
	mov	r4, r4, asl #24
	mla	r5, r3, r5, r2
	ldr	r3, [r5, #8]
	ldr	r2, [r3, #0]
	bic	r2, r2, #65280
	orr	r4, r2, r4, lsr #16
	str	r4, [r3, #0]
	b	.L48
.L35:
	cmp	r4, #0
	moveq	r0, r4
	beq	.L31
	ldr	r3, .L50
	mov	r2, #12
	mla	r5, r2, r5, r3
	and	r4, r4, #255
	ldr	r3, [r5, #8]
	ldr	r2, [r3, #0]
	bic	r2, r2, #255
	orr	r4, r4, r2
	ldr	r2, [r5, #4]
	str	r4, [r3, #0]
	cmp	r2, #1
	ldreq	r2, [r3, #0]
	orreq	r2, r2, #-2147483648
	streq	r2, [r3, #0]
	b	.L48
.L36:
	ldr	r2, .L50
	mov	r1, r4
	mla	r5, r3, r5, r2
	sub	r4, r4, #1
	ldr	r6, [r5, #8]
	ldr	r0, [r6, #0]
	bl	spwm_percent_to_ctrl
	cmp	r4, #98
	str	r0, [r6, #0]
	bhi	.L48
	ldr	r3, [r5, #4]
	cmp	r3, #1
	ldreq	r3, [r6, #0]
	orreq	r3, r3, #-2147483648
	streq	r3, [r6, #0]
	b	.L48
.L37:
	ldr	r3, [r4, #0]
	ldr	r1, [r4, #4]
	mov	r3, r3, asl #24
	mov	r3, r3, lsr #16
	orr	r3, r3, #-2147483648
	str	r3, [sp, #0]
	ldr	r0, [sp, #0]
	bl	spwm_percent_to_ctrl
	ldr	r3, .L50
	mov	r2, #12
	mla	r5, r2, r5, r3
	ldr	r3, [r5, #8]
	str	r0, [sp, #0]
	ldr	r2, [sp, #0]
	str	r2, [r3, #0]
.L49:
	ldr	r2, [r3, #0]
	orr	r2, r2, #-2147483648
	str	r2, [r3, #0]
	mov	r3, #1
	str	r3, [r5, #4]
	b	.L48
.L43:
	mvn	r0, #0
	b	.L31
.L44:
	mvn	r0, #6
	b	.L31
.L48:
	mov	r0, #0
.L31:
	ldmfd	sp!, {r3, r4, r5, r6, pc}
.L51:
	.align	2
.L50:
	.word	.LANCHOR0
.LFE3:
	.size	spwm_ioctl, .-spwm_ioctl
	.section	.text.spwm_read,"ax",%progbits
	.align	2
	.global	spwm_read
	.type	spwm_read, %function
spwm_read:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
.LFE4:
	.size	spwm_read, .-spwm_read
	.section	.text.spwm_write,"ax",%progbits
	.align	2
	.global	spwm_write
	.type	spwm_write, %function
spwm_write:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
.LFE5:
	.size	spwm_write, .-spwm_write
	.section	.bss.spwmdat,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	spwmdat, %object
	.size	spwmdat, 24
spwmdat:
	.space	24
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc3xxx_spwm_driver.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x9b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.byte	0x42
	.byte	0x1
	.uleb128 0x3
	.4byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x75
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xaf
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xde
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x147
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x163
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"spwm_open\000"
.LASF3:
	.ascii	"spwm_read\000"
.LASF1:
	.ascii	"spwm_close\000"
.LASF2:
	.ascii	"spwm_ioctl\000"
.LASF7:
	.ascii	"spwm_percent_to_ctrl\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc3xxx_spwm_driver.c\000"
.LASF4:
	.ascii	"spwm_write\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
