	.file	"r_system_report.c"
	.text
.Ltext0:
	.section	.text.FDTO_SYSTEM_REPORT_redraw_scrollbox,"ax",%progbits
	.align	2
	.global	FDTO_SYSTEM_REPORT_redraw_scrollbox
	.type	FDTO_SYSTEM_REPORT_redraw_scrollbox, %function
FDTO_SYSTEM_REPORT_redraw_scrollbox:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldr	r4, .L2
	ldr	r5, .L2+4
	ldr	r0, [r4, #0]
	bl	FDTO_SYSTEM_load_system_name_into_guivar
	ldr	r0, [r4, #0]
	bl	FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_lines
	ldr	r2, [r5, #0]
	mov	r4, r0
	subs	r2, r4, r2
	mov	r0, #0
	mov	r1, r4
	movne	r2, #1
	bl	FDTO_SCROLL_BOX_redraw_retaining_topline
	str	r4, [r5, #0]
	ldmfd	sp!, {r4, r5, pc}
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE0:
	.size	FDTO_SYSTEM_REPORT_redraw_scrollbox, .-FDTO_SYSTEM_REPORT_redraw_scrollbox
	.section	.text.FDTO_SYSTEM_REPORT_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_SYSTEM_REPORT_draw_report
	.type	FDTO_SYSTEM_REPORT_draw_report, %function
FDTO_SYSTEM_REPORT_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r5, .L6
	mov	r4, r0
	mvn	r1, #0
	mov	r0, #103
	mov	r2, #1
	bl	GuiLib_ShowScreen
	cmp	r4, #1
	bne	.L5
	ldr	r0, [r5, #0]
	bl	FDTO_SYSTEM_load_system_name_into_guivar
.L5:
	ldr	r0, [r5, #0]
	bl	FDTO_SYSTEM_REPORT_fill_ptrs_and_return_how_many_lines
	ldr	r3, .L6+4
	ldr	r2, .L6+8
	mov	r1, r0
	str	r0, [r3, #0]
	mov	r0, r4
	ldmfd	sp!, {r4, r5, lr}
	b	FDTO_REPORTS_draw_report
.L7:
	.align	2
.L6:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	FDTO_SYSTEM_REPORT_load_guivars_for_scroll_line
.LFE1:
	.size	FDTO_SYSTEM_REPORT_draw_report, .-FDTO_SYSTEM_REPORT_draw_report
	.section	.text.SYSTEM_REPORT_process_report,"ax",%progbits
	.align	2
	.global	SYSTEM_REPORT_process_report
	.type	SYSTEM_REPORT_process_report, %function
SYSTEM_REPORT_process_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI2:
	sub	sp, sp, #44
.LCFI3:
	beq	.L10
	cmp	r0, #20
	bne	.L15
	b	.L16
.L10:
	mov	r6, #80
	b	.L11
.L16:
	mov	r6, #84
.L11:
	ldr	r5, .L17
	mov	r1, #400
	ldr	r2, .L17+4
	mov	r3, #133
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	mov	r4, #1
	ldr	r1, .L17+8
	mov	r2, #0
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	sub	r3, r0, #1
	mov	r0, r6
	bl	process_uns32
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L17+12
	add	r0, sp, #8
	str	r4, [sp, #8]
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L8
.L15:
	mov	r2, #20
	mov	r3, #396
	bl	REPORTS_process_report
.L8:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, pc}
.L18:
	.align	2
.L17:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
	.word	FDTO_SYSTEM_REPORT_redraw_scrollbox
.LFE2:
	.size	SYSTEM_REPORT_process_report, .-SYSTEM_REPORT_process_report
	.section	.bss.g_SYSTEM_REPORT_index_of_system_to_show,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_SYSTEM_REPORT_index_of_system_to_show, %object
	.size	g_SYSTEM_REPORT_index_of_system_to_show, 4
g_SYSTEM_REPORT_index_of_system_to_show:
	.space	4
	.section	.bss.g_SYSTEM_REPORT_line_count,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_SYSTEM_REPORT_line_count, %object
	.size	g_SYSTEM_REPORT_line_count, 4
g_SYSTEM_REPORT_line_count:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_system_report.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_system_report.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x38
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x54
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x71
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"FDTO_SYSTEM_REPORT_redraw_scrollbox\000"
.LASF1:
	.ascii	"FDTO_SYSTEM_REPORT_draw_report\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_system_report.c\000"
.LASF2:
	.ascii	"SYSTEM_REPORT_process_report\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
