	.file	"e_lr_programming_raveon.c"
	.text
.Ltext0:
	.section	.text.LR_RAVEON_PROGRAMMING_post_device_query_to_queue,"ax",%progbits
	.align	2
	.type	LR_RAVEON_PROGRAMMING_post_device_query_to_queue, %function
LR_RAVEON_PROGRAMMING_post_device_query_to_queue:
.LFB2:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L4
	mov	r2, #1
	str	lr, [sp, #-4]!
.LCFI0:
	cmp	r1, #87
	str	r2, [r3, #0]
	ldr	r3, .L4+4
	sub	sp, sp, #40
.LCFI1:
	movne	r2, #4352
	strne	r2, [sp, #0]
	movne	r2, #82
	moveq	r2, #4608
	str	r0, [sp, #32]
	streq	r1, [r3, #372]
	strne	r2, [r3, #372]
	mov	r0, sp
	streq	r2, [sp, #0]
	bl	COMM_MNGR_post_event_with_details
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L5:
	.align	2
.L4:
	.word	LR_RAVEON_PROGRAMMING_querying_device
	.word	comm_mngr
.LFE2:
	.size	LR_RAVEON_PROGRAMMING_post_device_query_to_queue, .-LR_RAVEON_PROGRAMMING_post_device_query_to_queue
	.section	.text.FDTO_LR_PROGRAMMING_raveon_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_LR_PROGRAMMING_raveon_draw_screen
	.type	FDTO_LR_PROGRAMMING_raveon_draw_screen, %function
FDTO_LR_PROGRAMMING_raveon_draw_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI2:
	mov	r4, r0
	mov	r5, r1
	bne	.L7
.LBB4:
	ldr	r3, .L13
	ldr	r6, .L13+4
	mov	r2, #9
	ldr	r0, .L13+8
	ldr	r1, .L13+12
	str	r3, [r6, #0]
	bl	strlcpy
	ldr	r3, .L13+16
	ldr	r2, .L13+20
	str	r5, [r3, #0]
	ldr	r3, .L13+24
	mov	r7, #0
	str	r2, [r3, #0]
	ldr	r3, .L13+28
	str	r7, [r3, #0]
	ldr	r3, .L13+32
	str	r4, [r3, #0]
	ldr	r3, .L13+36
	strb	r7, [r3, #0]
.LBE4:
	ldr	r3, .L13+40
	ldr	r3, [r3, #0]
	cmp	r3, #3
	beq	.L8
	ldr	r0, .L13+44
	bl	e_SHARED_index_keycode_for_gui
	str	r0, [r6, #0]
	b	.L8
.L7:
	ldr	r3, .L13+48
	ldrsh	r7, [r3, #0]
	cmn	r7, #1
	ldreq	r3, .L13+52
	ldreq	r7, [r3, #0]
.L8:
	mov	r1, r7, asl #16
	mov	r0, #30
	mov	r1, r1, asr #16
	mov	r2, #1
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
	cmp	r4, #1
	ldmnefd	sp!, {r4, r5, r6, r7, pc}
	ldr	r3, .L13+40
	ldr	r3, [r3, #0]
	cmp	r3, #3
	beq	.L11
	bl	DEVICE_EXCHANGE_draw_dialog
	mov	r0, r5
	mov	r1, #82
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	LR_RAVEON_PROGRAMMING_post_device_query_to_queue
.L11:
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	LR_RAVEON_initialize_state_struct
.L14:
	.align	2
.L13:
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	GuiVar_LRSerialNumber
	.word	.LC0
	.word	GuiVar_LRPort
	.word	450
	.word	GuiVar_LRFrequency_WholeNum
	.word	GuiVar_LRFrequency_Decimal
	.word	GuiVar_LRTransmitPower
	.word	GuiVar_LRHubFeatureNotAvailable
	.word	GuiVar_LRRadioType
	.word	36867
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
.LFE3:
	.size	FDTO_LR_PROGRAMMING_raveon_draw_screen, .-FDTO_LR_PROGRAMMING_raveon_draw_screen
	.section	.text.FDTO_LR_RAVEON_PROGRAMMING_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_LR_RAVEON_PROGRAMMING_process_device_exchange_key, %function
FDTO_LR_RAVEON_PROGRAMMING_process_device_exchange_key:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	r3, r0, #36864
	cmp	r3, #6
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	mov	r4, r1
	ldmhifd	sp!, {r4, r5, pc}
	mov	r2, #1
	mov	r3, r2, asl r3
	ands	r5, r3, #109
	bne	.L17
	tst	r3, #18
	ldmeqfd	sp!, {r4, r5, pc}
.LBB7:
	bl	LR_RAVEON_PROGRAMMING_copy_settings_into_guivars
	ldr	r0, .L19
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L19+4
	str	r0, [r3, #0]
	bl	DIALOG_close_ok_dialog
	mov	r0, r5
	mov	r1, r4
.LBE7:
	ldmfd	sp!, {r4, r5, lr}
.LBB8:
	b	FDTO_LR_PROGRAMMING_raveon_draw_screen
.L17:
.LBE8:
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L19+4
	str	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	DEVICE_EXCHANGE_draw_dialog
.L20:
	.align	2
.L19:
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
.LFE1:
	.size	FDTO_LR_RAVEON_PROGRAMMING_process_device_exchange_key, .-FDTO_LR_RAVEON_PROGRAMMING_process_device_exchange_key
	.section	.text.LR_PROGRAMMING_raveon_process_screen,"ax",%progbits
	.align	2
	.global	LR_PROGRAMMING_raveon_process_screen
	.type	LR_PROGRAMMING_raveon_process_screen, %function
LR_PROGRAMMING_raveon_process_screen:
.LFB4:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r4, r5, lr}
.LCFI4:
	mov	r4, r0
	sub	sp, sp, #44
.LCFI5:
	mov	r5, r1
	beq	.L27
	bhi	.L31
	cmp	r0, #1
	beq	.L24
	bcc	.L23
	cmp	r0, #2
	beq	.L25
	cmp	r0, #3
	bne	.L22
	b	.L26
.L31:
	cmp	r0, #84
	beq	.L29
	bhi	.L32
	cmp	r0, #67
	beq	.L28
	cmp	r0, #80
	bne	.L22
	b	.L29
.L32:
	sub	r3, r0, #36864
	cmp	r3, #6
	bhi	.L22
	ldr	r3, .L61
	mov	r2, #0
	str	r2, [r3, #0]
	mov	r3, #3
	str	r3, [sp, #8]
	ldr	r3, .L61+4
	str	r0, [sp, #32]
	str	r3, [sp, #28]
	ldr	r3, .L61+8
	add	r0, sp, #8
	ldr	r3, [r3, #0]
	str	r3, [sp, #36]
	bl	Display_Post_Command
	b	.L21
.L25:
	ldr	r4, .L61+12
	ldr	r0, .L61+16
	ldr	r5, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r5, r0
	beq	.L46
	ldr	r0, .L61+20
	ldr	r5, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r5, r0
	beq	.L46
	ldr	r5, .L61+24
	ldrsh	r3, [r5, #0]
	cmp	r3, #4
	beq	.L36
	cmp	r3, #5
	bne	.L46
	b	.L60
.L36:
	ldr	r3, .L61+28
	ldr	r3, [r3, #0]
	cmp	r3, #3
	beq	.L46
.L38:
	bl	good_key_beep
	ldr	r3, .L61+32
	ldrsh	r2, [r5, #0]
	ldr	r0, .L61+16
	str	r2, [r3, #0]
	bl	e_SHARED_index_keycode_for_gui
	str	r0, [r4, #0]
	bl	DEVICE_EXCHANGE_draw_dialog
	ldr	r3, .L61+8
	mov	r1, #82
	ldr	r0, [r3, #0]
	b	.L57
.L60:
	bl	good_key_beep
	ldr	r3, .L61+32
	ldrsh	r2, [r5, #0]
	str	r2, [r3, #0]
	bl	LR_RAVEON_PROGRAMMING_extract_changes_from_guivars
	ldr	r0, .L61+20
	bl	e_SHARED_index_keycode_for_gui
	str	r0, [r4, #0]
	bl	DEVICE_EXCHANGE_draw_dialog
	ldr	r3, .L61+8
	mov	r1, #87
	ldr	r0, [r3, #0]
.L57:
	bl	LR_RAVEON_PROGRAMMING_post_device_query_to_queue
	b	.L21
.L27:
	ldr	r3, .L61+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L39
.L42:
	.word	.L46
	.word	.L46
	.word	.L39
	.word	.L39
	.word	.L41
	.word	.L41
.L41:
	ldr	r3, .L61+28
	ldr	r1, [r3, #0]
	cmp	r1, #1
	movne	r0, #1
	movne	r1, r0
	bne	.L58
	b	.L59
.L39:
	mov	r0, #1
.L24:
	bl	CURSOR_Up
	b	.L21
.L23:
	ldr	r3, .L61+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L26
.L47:
	.word	.L45
	.word	.L45
	.word	.L26
	.word	.L26
	.word	.L46
	.word	.L46
.L45:
	ldr	r3, .L61+28
	ldr	r1, [r3, #0]
	cmp	r1, #1
	movne	r0, #0
	movne	r1, #1
	bne	.L58
.L59:
	mov	r0, #2
.L58:
	bl	CURSOR_Select
	b	.L21
.L46:
	bl	bad_key_beep
	b	.L21
.L26:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L21
.L29:
	ldr	r3, .L61+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #1
	beq	.L51
	cmp	r3, #2
	beq	.L52
	cmp	r3, #0
	bne	.L55
	mov	r2, #1
	stmia	sp, {r2, r3}
	ldr	r1, .L61+36
	mov	r0, r4
	ldr	r2, .L61+40
	ldr	r3, .L61+44
	b	.L56
.L51:
	mov	r3, #125
	str	r3, [sp, #0]
	ldr	r1, .L61+48
	ldr	r3, .L61+52
	mov	r2, #0
	mov	r0, r4
	str	r2, [sp, #4]
.L56:
	bl	process_uns32
	b	.L53
.L52:
	mov	r3, #0
	mov	r2, #1
	stmia	sp, {r2, r3}
	ldr	r1, .L61+56
	mov	r0, r4
	mov	r3, #10
	b	.L56
.L55:
	bl	bad_key_beep
.L53:
	bl	Refresh_Screen
	b	.L21
.L28:
	bl	task_control_EXIT_device_exchange_hammer
	ldr	r3, .L61+60
	mov	r2, #11
	mov	r0, r4
	str	r2, [r3, #0]
	mov	r1, r5
	bl	KEY_process_global_keys
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	b	.L21
.L22:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L21:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L62:
	.align	2
.L61:
	.word	LR_RAVEON_PROGRAMMING_querying_device
	.word	FDTO_LR_RAVEON_PROGRAMMING_process_device_exchange_key
	.word	GuiVar_LRPort
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36867
	.word	36870
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_LRRadioType
	.word	.LANCHOR0
	.word	GuiVar_LRFrequency_WholeNum
	.word	450
	.word	470
	.word	GuiVar_LRFrequency_Decimal
	.word	9875
	.word	GuiVar_LRTransmitPower
	.word	GuiVar_MenuScreenToShow
.LFE4:
	.size	LR_PROGRAMMING_raveon_process_screen, .-LR_PROGRAMMING_raveon_process_screen
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"--------\000"
	.section	.bss.g_LR_PROGRAMMING_previous_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_LR_PROGRAMMING_previous_cursor_pos, %object
	.size	g_LR_PROGRAMMING_previous_cursor_pos, 4
g_LR_PROGRAMMING_previous_cursor_pos:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_lr_programming_raveon.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x7a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x44
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x1
	.byte	0x96
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xbd
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	0x29
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xfd
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI5
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"LR_PROGRAMMING_raveon_process_screen\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_lr_programming_raveon.c\000"
.LASF6:
	.ascii	"LR_RAVEON_PROGRAMMING_post_device_query_to_queue\000"
.LASF1:
	.ascii	"FDTO_LR_RAVEON_PROGRAMMING_process_device_exchange_"
	.ascii	"key\000"
.LASF2:
	.ascii	"FDTO_LR_PROGRAMMING_raveon_draw_screen\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"LR_RAVEON_PROGRAMMING_initialize_guivars\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
