	.file	"e_poc.c"
	.text
.Ltext0:
	.section	.text.POC_process_fm_type,"ax",%progbits
	.align	2
	.type	POC_process_fm_type, %function
POC_process_fm_type:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI0:
	ldr	r8, [r1, #0]
	mov	r4, r1
	mov	r5, r2
	mov	r6, r3
	mov	r2, #0
	mov	r3, #17
	mov	r7, #1
	str	r7, [sp, #0]
	str	r2, [sp, #4]
	bl	process_uns32
	ldr	r3, [r4, #0]
	cmp	r8, #14
	cmpeq	r3, #15
	bne	.L2
	ldr	r3, [r6, #0]
	cmp	r3, #0
	streq	r7, [r6, #0]
.L2:
	ldr	r3, [r5, #0]
	ldr	r2, [r4, #0]
	cmp	r3, #0
	bne	.L3
	cmp	r2, #8
	bls	.L5
	b	.L4
.L3:
	cmp	r3, #1
	bne	.L5
	cmp	r2, #8
	bhi	.L5
.L4:
	rsbs	r3, r3, #1
	movcc	r3, #0
	str	r3, [r5, #0]
	b	.L7
.L5:
	ldr	r3, [r4, #0]
	cmp	r3, #8
	cmpne	r8, #8
	bne	.L1
.L7:
	mov	r0, #0
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	Redraw_Screen
.L1:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.LFE7:
	.size	POC_process_fm_type, .-POC_process_fm_type
	.section	.text.POC_process_decoder_serial_number,"ax",%progbits
	.align	2
	.type	POC_process_decoder_serial_number, %function
POC_process_decoder_serial_number:
.LFB2:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L15
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, lr}
.LCFI1:
	mov	r7, r0
	ldr	r0, [r3, #0]
	mov	r5, r1
	bl	POC_get_group_at_this_index
	mov	r6, #0
	str	r6, [sp, #8]
	mov	r4, r0
.L11:
	mov	r0, r4
	mov	r1, r6
	ldr	r8, [r5, #0]
	bl	POC_get_decoder_serial_number_for_this_poc_and_level_0
	cmp	r8, r0
	streq	r6, [sp, #8]
	beq	.L10
.L9:
	add	r6, r6, #1
	cmp	r6, #3
	bne	.L11
.L10:
	ldr	r3, .L15+4
	mov	r2, #1
	ldr	r3, [r3, #0]
	mov	r0, r7
	str	r2, [sp, #0]
	add	r1, sp, #8
	mov	r2, #0
	sub	r3, r3, #1
	str	r2, [sp, #4]
	bl	process_uns32
	mov	r0, r4
	ldr	r1, [sp, #8]
	bl	POC_get_decoder_serial_number_for_this_poc_and_level_0
	cmp	r0, #0
	beq	.L12
	mov	r0, r4
	ldr	r1, [sp, #8]
	bl	POC_get_decoder_serial_number_for_this_poc_and_level_0
	str	r0, [r5, #0]
	b	.L8
.L12:
	bl	bad_key_beep
.L8:
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, pc}
.L16:
	.align	2
.L15:
	.word	g_GROUP_list_item_index
	.word	GuiVar_POCBypassStages
.LFE2:
	.size	POC_process_decoder_serial_number, .-POC_process_decoder_serial_number
	.section	.text.FDTO_POC_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_POC_return_to_menu, %function
FDTO_POC_return_to_menu:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L18
	ldr	r1, .L18+4
	b	FDTO_GROUP_return_to_menu
.L19:
	.align	2
.L18:
	.word	.LANCHOR0
	.word	POC_extract_and_store_changes_from_GuiVars
.LFE15:
	.size	FDTO_POC_return_to_menu, .-FDTO_POC_return_to_menu
	.section	.text.FDTO_POC_show_master_valve_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_master_valve_dropdown, %function
FDTO_POC_show_master_valve_dropdown:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L25
	ldr	r3, [r3, #0]
	cmp	r3, #12
	moveq	r2, #42
	ldr	r3, .L25+4
	beq	.L24
	ldr	r2, .L25+8
	ldr	r2, [r2, #0]
	cmp	r2, #0
	movne	r2, #12
	bne	.L24
	ldr	r1, .L25+12
	ldr	r1, [r1, #0]
	cmp	r1, #8
	moveq	r2, #24
.L24:
	str	r2, [r3, #0]
	ldr	r3, .L25+16
	ldr	r1, .L25+20
	ldr	r3, [r3, #0]
	mov	r0, #736
	mov	r2, #2
	b	FDTO_COMBOBOX_show
.L26:
	.align	2
.L25:
	.word	GuiVar_POCType
	.word	GuiVar_ComboBox_Y1
	.word	GuiVar_POCFMType1IsBermad
	.word	GuiVar_POCFlowMeterType1
	.word	GuiVar_POCMVType1
	.word	FDTO_COMBOBOX_add_items
.LFE4:
	.size	FDTO_POC_show_master_valve_dropdown, .-FDTO_POC_show_master_valve_dropdown
	.section	.text.FDTO_POC_show_flow_meter_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_flow_meter_dropdown, %function
FDTO_POC_show_flow_meter_dropdown:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L33
	ldr	r3, [r3, #0]
	cmp	r3, #12
	bne	.L28
	ldr	r3, .L33+4
	ldr	r2, .L33+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	ldreq	r3, .L33+12
	moveq	r1, #44
	ldreq	r3, [r3, #0]
	beq	.L32
.L29:
	cmp	r3, #10
	ldreq	r3, .L33+16
	ldrne	r3, .L33+20
	ldreq	r3, [r3, #0]
	moveq	r1, #102
	ldrne	r3, [r3, #0]
	movne	r1, #107
	b	.L32
.L28:
	ldr	r3, .L33+12
	ldr	r2, .L33+8
	ldr	r3, [r3, #0]
	mov	r1, #0
.L32:
	str	r1, [r2, #0]
	ldr	r1, .L33+24
	mov	r0, #728
	mov	r2, #18
	b	FDTO_COMBOBOX_show
.L34:
	.align	2
.L33:
	.word	GuiVar_POCType
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_ComboBox_Y1
	.word	GuiVar_POCFlowMeterType1
	.word	GuiVar_POCFlowMeterType2
	.word	GuiVar_POCFlowMeterType3
	.word	FDTO_COMBOBOX_add_items
.LFE5:
	.size	FDTO_POC_show_flow_meter_dropdown, .-FDTO_POC_show_flow_meter_dropdown
	.section	.text.FDTO_POC_show_poc_usage_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_poc_usage_dropdown, %function
FDTO_POC_show_poc_usage_dropdown:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L36
	ldr	r0, .L36+4
	ldr	r1, .L36+8
	ldr	r3, [r3, #0]
	mov	r2, #3
	b	FDTO_COMBOBOX_show
.L37:
	.align	2
.L36:
	.word	GuiVar_POCUsage
	.word	741
	.word	FDTO_COMBOBOX_add_items
.LFE3:
	.size	FDTO_POC_show_poc_usage_dropdown, .-FDTO_POC_show_poc_usage_dropdown
	.section	.text.FDTO_POC_close_flow_meter_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_close_flow_meter_dropdown, %function
FDTO_POC_close_flow_meter_dropdown:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L43
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI2:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	cmpne	r3, #44
	ldreq	r7, .L43+4
	ldreq	r6, .L43+8
	ldreq	r5, [r7, #0]
	ldreq	r4, .L43+12
	beq	.L40
.L39:
	cmp	r3, #102
	ldreq	r7, .L43+16
	ldrne	r7, .L43+20
	ldreq	r5, [r7, #0]
	ldreq	r6, .L43+24
	ldreq	r4, .L43+28
	ldrne	r5, [r7, #0]
	ldrne	r6, .L43+32
	ldrne	r4, .L43+36
.L40:
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	cmp	r0, #8
	movls	r3, #0
	movhi	r3, #1
	cmp	r0, #15
	cmpeq	r5, #14
	str	r0, [r7, #0]
	str	r3, [r6, #0]
	bne	.L42
	ldr	r3, [r4, #0]
	cmp	r3, #0
	moveq	r3, #1
	streq	r3, [r4, #0]
.L42:
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	FDTO_COMBOBOX_hide
.L44:
	.align	2
.L43:
	.word	GuiVar_ComboBox_Y1
	.word	GuiVar_POCFlowMeterType1
	.word	GuiVar_POCFMType1IsBermad
	.word	GuiVar_POCReedSwitchType1
	.word	GuiVar_POCFlowMeterType2
	.word	GuiVar_POCFlowMeterType3
	.word	GuiVar_POCFMType2IsBermad
	.word	GuiVar_POCReedSwitchType2
	.word	GuiVar_POCFMType3IsBermad
	.word	GuiVar_POCReedSwitchType3
.LFE6:
	.size	FDTO_POC_close_flow_meter_dropdown, .-FDTO_POC_close_flow_meter_dropdown
	.section	.text.POC_load_poc_name_into_guivar,"ax",%progbits
	.align	2
	.global	POC_load_poc_name_into_guivar
	.type	POC_load_poc_name_into_guivar, %function
POC_load_poc_name_into_guivar:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L48
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, lr}
.LCFI3:
	ldr	r2, .L48+4
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L48+8
	bl	xQueueTakeMutexRecursive_debug
	bl	POC_get_list_count
	cmp	r4, r0
	bcs	.L46
	ldr	r3, .L48+12
	ldr	r0, [r3, r4, asl #2]
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L48+16
	bl	strlcpy
	b	.L47
.L46:
	ldr	r0, .L48+4
	ldr	r1, .L48+20
	bl	Alert_group_not_found_with_filename
.L47:
	ldr	r3, .L48
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L49:
	.align	2
.L48:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	945
	.word	.LANCHOR1
	.word	GuiVar_itmGroupName
	.word	953
.LFE13:
	.size	POC_load_poc_name_into_guivar, .-POC_load_poc_name_into_guivar
	.section	.text.POC_process_reed_switch_type,"ax",%progbits
	.align	2
	.type	POC_process_reed_switch_type, %function
POC_process_reed_switch_type:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, r1
	stmfd	sp!, {r0, r1, lr}
.LCFI4:
	mov	r1, r2
	sub	r2, r3, #9
	cmp	r2, #5
.LBB40:
	movls	r3, #1
	movls	r2, #0
	strls	r3, [sp, #0]
	strls	r2, [sp, #4]
	bls	.L54
.L51:
.LBE40:
	sub	r3, r3, #15
	cmp	r3, #2
	bhi	.L53
	mov	r3, #1
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r2, r3
	str	r3, [sp, #0]
.L54:
	bl	process_uns32
	add	sp, sp, #8
	ldmfd	sp!, {pc}
.L53:
	add	sp, sp, #8
	ldr	lr, [sp], #4
	b	bad_key_beep
.LFE8:
	.size	POC_process_reed_switch_type, .-POC_process_reed_switch_type
	.section	.text.POC_get_menu_index_for_displayed_poc,"ax",%progbits
	.align	2
	.global	POC_get_menu_index_for_displayed_poc
	.type	POC_get_menu_index_for_displayed_poc, %function
POC_get_menu_index_for_displayed_poc:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L60
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI5:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L60+4
	ldr	r3, .L60+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L60+12
	ldr	r5, .L60+16
	ldr	r0, [r3, #0]
	ldr	r3, .L60+20
	mov	r4, #0
	ldr	r1, [r3, #0]
	bl	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	mov	r6, r0
	b	.L56
.L58:
	ldr	r3, [r5, #4]!
	cmp	r3, r6
	beq	.L57
	add	r4, r4, #1
.L56:
	bl	POC_get_list_count
	cmp	r4, r0
	bcc	.L58
	mov	r4, #0
.L57:
	ldr	r3, .L60
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.L61:
	.align	2
.L60:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	437
	.word	g_GROUP_ID
	.word	.LANCHOR1-4
	.word	GuiVar_POCBoxIndex
.LFE9:
	.size	POC_get_menu_index_for_displayed_poc, .-POC_get_menu_index_for_displayed_poc
	.section	.text.POC_populate_pointers_of_POCs_for_display,"ax",%progbits
	.align	2
	.global	POC_populate_pointers_of_POCs_for_display
	.type	POC_populate_pointers_of_POCs_for_display, %function
POC_populate_pointers_of_POCs_for_display:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI6:
	ldr	r8, .L67
	mov	r1, #0
	mov	r2, #48
	mov	r4, r0
	ldr	r0, .L67
	bl	memset
	ldr	r3, .L67+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L67+8
	ldr	r3, .L67+12
	bl	xQueueTakeMutexRecursive_debug
	mov	r6, #0
	mov	r5, r6
	b	.L63
.L66:
	mov	r0, r6
	bl	POC_get_group_at_this_index
	subs	r7, r0, #0
	beq	.L64
	bl	POC_get_show_for_this_poc
	cmp	r0, #0
	beq	.L64
	cmp	r4, #0
	beq	.L65
	mov	r0, r7
	bl	nm_GROUP_get_group_ID
	bl	POC_get_usage
	cmp	r0, #0
	beq	.L64
	mov	r0, r6
	bl	POC_use_for_budget
	cmp	r0, #0
	beq	.L64
.L65:
	str	r7, [r8, r5, asl #2]
	add	r5, r5, #1
.L64:
	add	r6, r6, #1
.L63:
	bl	POC_get_list_count
	cmp	r6, r0
	bcc	.L66
	ldr	r3, .L67+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L68:
	.align	2
.L67:
	.word	.LANCHOR1
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	907
.LFE12:
	.size	POC_populate_pointers_of_POCs_for_display, .-POC_populate_pointers_of_POCs_for_display
	.section	.text.FDTO_POC_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_POC_draw_menu
	.type	FDTO_POC_draw_menu, %function
FDTO_POC_draw_menu:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L74
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI7:
	ldr	r2, .L74+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L74+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	bl	POC_populate_pointers_of_POCs_for_display
	cmp	r4, #1
	ldr	r6, .L74+12
	mov	r5, r0
	bne	.L70
	ldr	r2, .L74+16
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L74+20
	str	r3, [r6, #0]
	str	r3, [r2, #0]
.LBB41:
	ldr	r3, .L74+24
.LBE41:
	mvn	r4, #0
	ldr	r0, [r3, #0]
	bl	POC_get_index_using_ptr_to_poc_struct
	ldr	r3, .L74+28
	str	r0, [r3, #0]
	bl	POC_copy_group_into_guivars
	ldr	r3, .L74+32
	mov	r2, #9
	str	r2, [r3, #0]
	b	.L71
.L70:
	ldr	r3, .L74+36
	mov	r0, #0
	ldrsh	r4, [r3, #0]
	bl	GuiLib_ScrollBox_GetTopLine
	str	r0, [r6, #0]
.L71:
	mov	r1, r4
	mov	r2, #1
	mov	r0, #47
	bl	GuiLib_ShowScreen
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	bl	POC_get_menu_index_for_displayed_poc
	ldr	r3, .L74+16
	mov	r2, r5, asl #16
	ldr	r3, [r3, #0]
	ldr	r1, .L74+12
	cmp	r3, #1
	mov	r2, r2, asr #16
	mov	r4, r0
	bne	.L72
	ldrsh	r3, [r1, #0]
	mov	r0, #0
	str	r3, [sp, #0]
	ldr	r1, .L74+40
	mvn	r3, #0
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	mov	r1, r4, asl #16
	mov	r1, r1, asr #16
	mov	r0, #0
	bl	GuiLib_ScrollBox_SetIndicator
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
	b	.L73
.L72:
	ldrsh	r1, [r1, #0]
	mov	r3, r0, asl #16
	str	r1, [sp, #0]
	mov	r0, #0
	ldr	r1, .L74+40
	mov	r3, r3, asr #16
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L73:
	ldr	r3, .L74
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_Refresh
.L75:
	.align	2
.L74:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	1017
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR3
	.word	.LANCHOR1
	.word	g_GROUP_list_item_index
	.word	.LANCHOR4
	.word	GuiLib_ActiveCursorFieldNo
	.word	POC_load_poc_name_into_guivar
.LFE16:
	.size	FDTO_POC_draw_menu, .-FDTO_POC_draw_menu
	.section	.text.POC_get_ptr_to_physically_available_poc,"ax",%progbits
	.align	2
	.global	POC_get_ptr_to_physically_available_poc
	.type	POC_get_ptr_to_physically_available_poc, %function
POC_get_ptr_to_physically_available_poc:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L77
	ldr	r0, [r3, r0, asl #2]
	bx	lr
.L78:
	.align	2
.L77:
	.word	.LANCHOR1
.LFE14:
	.size	POC_get_ptr_to_physically_available_poc, .-POC_get_ptr_to_physically_available_poc
	.section	.text.POC_process_menu,"ax",%progbits
	.align	2
	.global	POC_process_menu
	.type	POC_process_menu, %function
POC_process_menu:
.LFB17:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI8:
	ldr	r7, .L214+20
	sub	sp, sp, #44
.LCFI9:
	ldr	r6, [r7, #0]
	mov	r5, r0
	cmp	r6, #1
	mov	r4, r1
	bne	.L80
.LBB57:
.LBB58:
	ldr	r3, .L214+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #728
	beq	.L84
	bgt	.L87
	ldr	r2, .L214+28
	cmp	r3, r2
	beq	.L82
	cmp	r3, #616
	bne	.L81
	b	.L212
.L87:
	cmp	r3, #736
	beq	.L85
	ldr	r2, .L214+32
	cmp	r3, r2
	bne	.L81
	b	.L213
.L82:
	cmp	r0, #67
	beq	.L88
	cmp	r0, #2
	bne	.L89
	ldr	r3, .L214+172
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L89
.L88:
	bl	POC_extract_and_store_group_name_from_GuiVars
.L89:
	mov	r0, r5
	mov	r1, r4
	ldr	r2, .L214+36
	bl	KEYBOARD_process_key
	b	.L79
.L212:
	ldr	r2, .L214+36
	bl	NUMERIC_KEYPAD_process_key
	b	.L79
.L213:
	ldr	r1, .L214+40
	b	.L204
.L85:
	ldr	r1, .L214+44
.L204:
	bl	COMBO_BOX_key_press
	b	.L79
.L84:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L204
	bl	good_key_beep
	ldr	r3, .L214+48
	add	r0, sp, #8
	str	r6, [sp, #8]
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L79
.L81:
	cmp	r5, #4
	beq	.L97
	bhi	.L101
	cmp	r5, #1
	beq	.L94
	bcc	.L93
	cmp	r5, #2
	beq	.L95
	cmp	r5, #3
	bne	.L176
	b	.L96
.L101:
	cmp	r5, #67
	beq	.L99
	bhi	.L102
	cmp	r5, #16
	beq	.L98
	cmp	r5, #20
	bne	.L176
	b	.L98
.L102:
	cmp	r5, #80
	beq	.L100
	cmp	r5, #84
	bne	.L176
	b	.L100
.L95:
	ldr	r3, .L214+172
	ldrsh	r3, [r3, #0]
	cmp	r3, #17
	ldrls	pc, [pc, r3, asl #2]
	b	.L116
.L114:
	.word	.L104
	.word	.L105
	.word	.L116
	.word	.L116
	.word	.L106
	.word	.L107
	.word	.L108
	.word	.L116
	.word	.L109
	.word	.L116
	.word	.L106
	.word	.L110
	.word	.L111
	.word	.L116
	.word	.L116
	.word	.L106
	.word	.L112
	.word	.L113
.L104:
	mov	r0, #150
	mov	r1, #27
	bl	GROUP_process_show_keyboard
	b	.L136
.L105:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L214+52
	b	.L195
.L106:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L214+56
.L195:
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L136
.L109:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L214+60
	b	.L195
.L107:
	bl	good_key_beep
	ldr	r0, .L214+64
	b	.L207
.L110:
	bl	good_key_beep
	ldr	r0, .L214+68
	b	.L207
.L112:
	bl	good_key_beep
	ldr	r0, .L214+72
.L207:
	mov	r1, #0
	ldr	r2, .L214+76
	b	.L197
.L108:
	bl	good_key_beep
	ldr	r0, .L214+80
	b	.L196
.L111:
	bl	good_key_beep
	ldr	r0, .L214+84
	b	.L196
.L113:
	bl	good_key_beep
	ldr	r0, .L214+88
.L196:
	ldr	r1, .L214+92
	ldr	r2, .L214+96
.L197:
	mov	r3, #3
	bl	NUMERIC_KEYPAD_draw_float_keypad
	b	.L136
.L100:
	ldr	r3, .L214+172
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r3, #17
	ldrls	pc, [pc, r3, asl #2]
	b	.L116
.L135:
	.word	.L117
	.word	.L118
	.word	.L119
	.word	.L120
	.word	.L121
	.word	.L122
	.word	.L123
	.word	.L124
	.word	.L125
	.word	.L126
	.word	.L127
	.word	.L128
	.word	.L129
	.word	.L130
	.word	.L131
	.word	.L132
	.word	.L133
	.word	.L134
.L117:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r5
	ldr	r1, .L214+40
	mov	r3, #2
	b	.L198
.L118:
	mov	r4, #0
	mov	r3, #1
	stmia	sp, {r3, r4}
	ldr	r1, .L214+100
	mov	r0, r5
	mov	r2, #2
	mov	r3, #3
	bl	process_uns32
	mov	r0, r4
	bl	Redraw_Screen
	b	.L136
.L119:
	mov	r0, r5
	ldr	r1, .L214+104
	b	.L199
.L120:
	mov	r0, r5
	ldr	r1, .L214+108
	ldr	r2, .L214+112
	ldr	r3, .L214+116
	b	.L200
.L121:
.LBB59:
	cmp	r4, #96
	bhi	.L177
.LBB60:
	cmp	r4, #64
	bhi	.L178
	flds	s15, .L214
	flds	s14, .L214+4
	cmp	r4, #33
	fcpyscc	s15, s14
	b	.L137
.L177:
.LBE60:
	flds	s15, .L214+8
	b	.L137
.L178:
.LBB61:
	flds	s15, .L214+12
.L137:
.LBE61:
.LBE59:
	mov	r3, #0
	fsts	s15, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r5
	ldr	r1, .L214+64
	b	.L209
.L122:
.LBB62:
	cmp	r4, #64
	bhi	.L180
	flds	s15, .L214+16
	flds	s14, .L214+4
	cmp	r4, #33
	fcpyscc	s15, s14
	b	.L138
.L180:
	flds	s15, .L214
.L138:
.LBE62:
	mov	r3, #0
	fsts	s15, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r5
	ldr	r1, .L214+80
	b	.L208
.L123:
	ldr	r3, .L214+108
	mov	r0, r5
	ldr	r1, [r3, #0]
	ldr	r2, .L214+116
	b	.L202
.L124:
	ldr	r1, .L214+44
	mov	r3, #1
	mov	r0, r5
	mov	r2, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L198:
	bl	process_uns32
	b	.L136
.L125:
	ldr	r1, .L214+120
	mov	r0, r5
.L199:
	bl	POC_process_decoder_serial_number
	b	.L136
.L126:
	ldr	r1, .L214+124
	ldr	r2, .L214+128
	ldr	r3, .L214+132
	mov	r0, r5
.L200:
	bl	POC_process_fm_type
	b	.L136
.L127:
.LBB63:
	cmp	r4, #96
	bhi	.L182
.LBB64:
	cmp	r4, #64
	bhi	.L183
	flds	s15, .L214
	flds	s14, .L214+4
	cmp	r4, #33
	fcpyscc	s15, s14
	b	.L139
.L182:
.LBE64:
	flds	s15, .L214+8
	b	.L139
.L183:
.LBB65:
	flds	s15, .L214+12
.L139:
.LBE65:
.LBE63:
	mov	r3, #0
	fsts	s15, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r5
	ldr	r1, .L214+68
	b	.L209
.L128:
.LBB66:
	cmp	r4, #64
	bhi	.L185
	flds	s15, .L214+16
	flds	s14, .L214+4
	cmp	r4, #33
	fcpyscc	s15, s14
	b	.L140
.L185:
	flds	s15, .L214
.L140:
.LBE66:
	mov	r3, #0
	fsts	s15, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r5
	ldr	r1, .L214+84
	b	.L208
.L129:
	ldr	r3, .L214+124
	mov	r0, r5
	ldr	r1, [r3, #0]
	ldr	r2, .L214+132
	b	.L202
.L130:
	mov	r0, r5
	ldr	r1, .L214+136
	b	.L199
.L131:
	mov	r0, r5
	ldr	r1, .L214+140
	ldr	r2, .L214+144
	ldr	r3, .L214+148
	b	.L200
.L132:
.LBB67:
	cmp	r4, #96
	bhi	.L187
.LBB68:
	cmp	r4, #64
	bhi	.L188
	flds	s15, .L214
	flds	s14, .L214+4
	cmp	r4, #33
	fcpyscc	s15, s14
	b	.L141
.L187:
.LBE68:
	flds	s15, .L214+8
	b	.L141
.L188:
.LBB69:
	flds	s15, .L214+12
.L141:
.LBE69:
.LBE67:
	fsts	s15, [sp, #0]
	ldr	r1, .L214+72
	mov	r3, #0
	mov	r0, r5
	str	r3, [sp, #4]
.L209:
	mov	r2, #0
	ldr	r3, .L214+76
	b	.L201
.L133:
.LBB70:
	cmp	r4, #64
	bhi	.L190
	flds	s15, .L214+16
	flds	s14, .L214+4
	cmp	r4, #33
	fcpyscc	s15, s14
	b	.L142
.L190:
	flds	s15, .L214
.L142:
.LBE70:
	fsts	s15, [sp, #0]
	ldr	r1, .L214+88
	mov	r3, #0
	mov	r0, r5
	str	r3, [sp, #4]
.L208:
	ldr	r2, .L214+92
	ldr	r3, .L214+96
.L201:
	bl	process_fl
	b	.L136
.L134:
	ldr	r3, .L214+140
	ldr	r2, .L214+148
	ldr	r1, [r3, #0]
	mov	r0, r5
.L202:
	bl	POC_process_reed_switch_type
	b	.L136
.L116:
	bl	bad_key_beep
.L136:
	bl	Refresh_Screen
	b	.L79
.L98:
.LBB71:
	bl	POC_get_menu_index_for_displayed_poc
	cmp	r5, #20
	mov	r4, r0
	mov	r5, r4, asl #16
	mov	r0, #0
	mov	r5, r5, asr #16
	bne	.L143
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	mov	r1, r5
	bl	SCROLL_BOX_to_line
	cmp	r4, #11
	bhi	.L147
	ldr	r3, .L214+180
	add	r4, r4, #1
	ldr	r3, [r3, r4, asl #2]
	cmp	r3, #0
	movne	r0, #0
	movne	r1, r0
	bne	.L210
	b	.L148
.L143:
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	mov	r0, #0
	mov	r1, r5
	bl	SCROLL_BOX_to_line
	cmp	r4, #0
	beq	.L147
	ldr	r3, .L214+180
	sub	r4, r4, #1
	ldr	r3, [r3, r4, asl #2]
	cmp	r3, #0
	beq	.L148
	mov	r0, #0
	mov	r1, #4
.L210:
	bl	SCROLL_BOX_up_or_down
	b	.L148
.L147:
	bl	bad_key_beep
.L148:
	ldr	r5, .L214+180
	mov	r0, #1
	bl	SCROLL_BOX_toggle_scroll_box_automatic_redraw
	ldr	r3, [r5, r4, asl #2]
	cmp	r3, #0
	beq	.L149
	bl	POC_extract_and_store_changes_from_GuiVars
	ldr	r0, [r5, r4, asl #2]
	bl	POC_get_index_using_ptr_to_poc_struct
	ldr	r5, .L214+184
	ldr	r4, .L214+152
	mov	r1, #400
	ldr	r2, .L214+156
	ldr	r3, .L214+160
	str	r0, [r5, #0]
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, #0]
	bl	POC_copy_group_into_guivars
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	b	.L211
.L149:
	bl	bad_key_beep
	b	.L211
.L97:
.LBE71:
	ldr	r3, .L214+164
	ldr	r3, [r3, #0]
	cmp	r3, #12
	bne	.L152
.L151:
	ldr	r3, .L214+172
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #7
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L152
.L161:
	.word	.L153
	.word	.L154
	.word	.L155
	.word	.L152
	.word	.L156
	.word	.L152
	.word	.L157
	.word	.L158
	.word	.L152
	.word	.L159
	.word	.L152
	.word	.L166
.L153:
	mov	r0, #3
	b	.L206
.L154:
	mov	r0, #4
	b	.L206
.L155:
	ldr	r3, .L214+188
	mov	r2, #9
	str	r2, [r3, #0]
	b	.L152
.L157:
	mov	r0, #9
	b	.L206
.L158:
	mov	r0, #10
	b	.L206
.L156:
	ldr	r3, .L214+188
	mov	r2, #11
	str	r2, [r3, #0]
	b	.L164
.L159:
	mov	r0, #12
	b	.L206
.L152:
	mov	r0, #1
	b	.L205
.L215:
	.align	2
.L214:
	.word	1006834287
	.word	981668463
	.word	1041865114
	.word	1015222895
	.word	998445679
	.word	.LANCHOR0
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	741
	.word	FDTO_POC_draw_menu
	.word	GuiVar_POCUsage
	.word	GuiVar_POCMVType1
	.word	FDTO_POC_close_flow_meter_dropdown
	.word	FDTO_POC_show_poc_usage_dropdown
	.word	FDTO_POC_show_flow_meter_dropdown
	.word	FDTO_POC_show_master_valve_dropdown
	.word	GuiVar_POCFlowMeterK1
	.word	GuiVar_POCFlowMeterK2
	.word	GuiVar_POCFlowMeterK3
	.word	1145569280
	.word	GuiVar_POCFlowMeterOffset1
	.word	GuiVar_POCFlowMeterOffset2
	.word	GuiVar_POCFlowMeterOffset3
	.word	-1054867456
	.word	1092616192
	.word	GuiVar_POCBypassStages
	.word	GuiVar_POCDecoderSN1
	.word	GuiVar_POCFlowMeterType1
	.word	GuiVar_POCFMType1IsBermad
	.word	GuiVar_POCReedSwitchType1
	.word	GuiVar_POCDecoderSN2
	.word	GuiVar_POCFlowMeterType2
	.word	GuiVar_POCFMType2IsBermad
	.word	GuiVar_POCReedSwitchType2
	.word	GuiVar_POCDecoderSN3
	.word	GuiVar_POCFlowMeterType3
	.word	GuiVar_POCFMType3IsBermad
	.word	GuiVar_POCReedSwitchType3
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	531
	.word	GuiVar_POCType
	.word	FDTO_POC_return_to_menu
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR3
	.word	.LANCHOR1
	.word	g_GROUP_list_item_index
	.word	.LANCHOR4
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.L93:
	ldr	r3, .L214+164
	ldr	r3, [r3, #0]
	cmp	r3, #12
	bne	.L96
.L162:
	ldr	r3, .L214+172
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #4
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L96
.L169:
	.word	.L164
	.word	.L96
	.word	.L96
	.word	.L96
	.word	.L165
	.word	.L96
	.word	.L166
	.word	.L96
	.word	.L167
	.word	.L96
	.word	.L96
	.word	.L168
.L164:
	mov	r0, #8
	b	.L206
.L165:
	ldr	r3, .L214+188
	ldr	r0, [r3, #0]
	b	.L206
.L166:
	mov	r0, #14
	b	.L206
.L167:
	mov	r0, #16
.L206:
	mov	r1, #1
	bl	CURSOR_Select
	b	.L79
.L168:
	bl	bad_key_beep
	b	.L79
.L94:
	ldr	r3, .L214+172
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, r5
	beq	.L99
.L205:
	bl	CURSOR_Up
	b	.L79
.L96:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L79
.L99:
	ldr	r0, .L214+168
	bl	KEY_process_BACK_from_editing_screen
	b	.L79
.L80:
.LBE58:
.LBE57:
	cmp	r0, #20
	bhi	.L172
	mov	r8, #1
	mov	r3, r8, asl r0
	tst	r3, #1114112
	bne	.L175
	ands	r6, r3, #17
	movne	r1, r0
	bne	.L173
	tst	r3, #12
	beq	.L172
	bl	good_key_beep
	mov	r1, r6
	mov	r0, r6
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L214+176
	str	r0, [r3, #0]
.LBB72:
	ldr	r3, .L214+180
.LBE72:
	ldr	r0, [r3, r0, asl #2]
	bl	POC_get_index_using_ptr_to_poc_struct
	ldr	r3, .L214+184
	str	r8, [r7, #0]
	str	r0, [r3, #0]
	ldr	r3, .L214+172
	mov	r0, r6
	strh	r6, [r3, #0]	@ movhi
	b	.L203
.L175:
	cmp	r0, #20
	movne	r1, #4
	moveq	r1, #0
.L173:
	mov	r0, #0
	bl	SCROLL_BOX_up_or_down
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L214+176
	str	r0, [r3, #0]
.LBB73:
	ldr	r3, .L214+180
.LBE73:
	ldr	r0, [r3, r0, asl #2]
	bl	POC_get_index_using_ptr_to_poc_struct
	ldr	r3, .L214+184
	str	r0, [r3, #0]
	bl	POC_copy_group_into_guivars
	ldr	r3, .L214+188
	mov	r2, #9
	str	r2, [r3, #0]
.L211:
	mov	r0, #0
.L203:
	bl	Redraw_Screen
	b	.L79
.L172:
	cmp	r5, #67
	bne	.L176
	ldr	r3, .L214+192
	ldr	r2, .L214+196
	ldr	r3, [r3, #0]
	mov	r1, #36
	mla	r3, r1, r3, r2
	ldr	r2, [r3, #4]
	ldr	r3, .L214+200
	str	r2, [r3, #0]
.L176:
	mov	r0, r5
	mov	r1, r4
	bl	KEY_process_global_keys
.L79:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.LFE17:
	.size	POC_process_menu, .-POC_process_menu
	.global	POC_MENU_items
	.global	g_POC_current_list_item_index
	.global	g_POC_editing_group
	.global	g_POC_top_line
	.section	.bss.g_POC_prev_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_POC_prev_cursor_pos, %object
	.size	g_POC_prev_cursor_pos, 4
g_POC_prev_cursor_pos:
	.space	4
	.section	.bss.POC_MENU_items,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	POC_MENU_items, %object
	.size	POC_MENU_items, 48
POC_MENU_items:
	.space	48
	.section	.bss.g_POC_top_line,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_POC_top_line, %object
	.size	g_POC_top_line, 4
g_POC_top_line:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_poc.c\000"
	.section	.bss.g_POC_current_list_item_index,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_POC_current_list_item_index, %object
	.size	g_POC_current_list_item_index, 4
g_POC_current_list_item_index:
	.space	4
	.section	.bss.g_POC_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_POC_editing_group, %object
	.size	g_POC_editing_group, 4
g_POC_editing_group:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI0-.LFB7
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI2-.LFB6
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI3-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI4-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI5-.LFB9
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI6-.LFB12
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI7-.LFB16
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI8-.LFB17
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE26:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_poc.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x161
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF17
	.byte	0x1
	.4byte	.LASF18
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x6d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x198
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0x86
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x3c0
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x167
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST0
	.uleb128 0x6
	.4byte	.LASF4
	.byte	0x1
	.byte	0x9b
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x7
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x3d7
	.4byte	.LFB15
	.4byte	.LFE15
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.4byte	.LASF6
	.byte	0x1
	.byte	0xcc
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.4byte	.LASF7
	.byte	0x1
	.byte	0xe9
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.4byte	.LASF8
	.byte	0x1
	.byte	0xc6
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x10d
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST2
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x3af
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST3
	.uleb128 0xa
	.4byte	0x29
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST4
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1a9
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST5
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x37b
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST6
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x3ef
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST7
	.uleb128 0xb
	.4byte	0x3a
	.4byte	.LFB14
	.4byte	.LFE14
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x236
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x1c9
	.byte	0x1
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x444
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB7
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB6
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB13
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB8
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB9
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB12
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB16
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB17
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI9
	.4byte	.LFE17
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x84
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"POC_process_decoder_serial_number\000"
.LASF8:
	.ascii	"FDTO_POC_show_poc_usage_dropdown\000"
.LASF10:
	.ascii	"POC_load_poc_name_into_guivar\000"
.LASF2:
	.ascii	"POC_get_inc_by_for_offset\000"
.LASF3:
	.ascii	"POC_process_fm_type\000"
.LASF18:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_poc.c\000"
.LASF11:
	.ascii	"POC_get_menu_index_for_displayed_poc\000"
.LASF9:
	.ascii	"FDTO_POC_close_flow_meter_dropdown\000"
.LASF1:
	.ascii	"POC_process_reed_switch_type\000"
.LASF15:
	.ascii	"POC_process_NEXT_and_PREV\000"
.LASF17:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF16:
	.ascii	"POC_process_menu\000"
.LASF7:
	.ascii	"FDTO_POC_show_flow_meter_dropdown\000"
.LASF12:
	.ascii	"POC_populate_pointers_of_POCs_for_display\000"
.LASF6:
	.ascii	"FDTO_POC_show_master_valve_dropdown\000"
.LASF19:
	.ascii	"POC_get_ptr_to_physically_available_poc\000"
.LASF5:
	.ascii	"FDTO_POC_return_to_menu\000"
.LASF13:
	.ascii	"FDTO_POC_draw_menu\000"
.LASF0:
	.ascii	"POC_get_inc_by_for_K_value\000"
.LASF14:
	.ascii	"POC_process_group\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
