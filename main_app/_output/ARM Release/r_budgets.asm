	.file	"r_budgets.c"
	.text
.Ltext0:
	.section	.text.BUDGET_REPORT_copy_group_into_guivars,"ax",%progbits
	.align	2
	.type	BUDGET_REPORT_copy_group_into_guivars, %function
BUDGET_REPORT_copy_group_into_guivars:
.LFB0:
	@ args = 0, pretend = 0, frame = 412
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI0:
	fstmfdd	sp!, {d8, d9, d10, d11, d12}
.LCFI1:
	flds	s21, .L37
	ldr	r3, .L37+8
	flds	s17, .L37+4
	sub	sp, sp, #420
.LCFI2:
	mov	r4, r0
	add	r0, sp, #392
	str	r3, [sp, #412]	@ float
	bl	EPSON_obtain_latest_complete_time_and_date
	ldr	r3, .L37+12
	mov	r1, #400
	ldr	r2, .L37+16
	ldr	r0, [r3, #0]
	mov	r3, #78
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	POC_get_group_at_this_index
	mov	r6, r0
	bl	POC_get_GID_irrigation_system
	mov	r5, r0
	bl	NETWORK_CONFIG_get_water_units
	ldr	r3, .L37+20
	cmp	r0, #0
	fldsne	s20, [sp, #412]
	str	r0, [r3, #0]
	mov	r0, r6
	fcpyseq	s20, s21
	fdivsne	s20, s21, s20
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L37+24
	str	r0, [r3, #0]
	mov	r0, r6
	bl	nm_GROUP_get_name
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L37+28
	bl	strlcpy
	mov	r0, r4
	bl	POC_get_gid_of_group_at_this_index
	mov	r7, r0
	bl	POC_get_box_index_0
	ldr	r3, .L37+32
	mov	r1, #0
	str	r0, [r3, #0]
	mov	r0, r6
	bl	POC_get_budget
	ldr	r3, .L37+36
	ldr	r6, .L37+40
	fmsr	s14, r0	@ int
	mov	r8, r0
	fuitos	s16, s14
	fmuls	s15, s20, s16
	fsts	s15, [r3, #0]
	ldr	r3, .L37+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r3, #112
	mov	r1, #400
	ldr	r2, .L37+16
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	SYSTEM_get_group_with_this_GID
	mov	sl, r0
	bl	nm_SYSTEM_get_used
	mov	r9, r0
	mov	r0, sl
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L37+44
	bl	strlcpy
	mov	r0, sl
	add	r1, sp, #232
	bl	SYSTEM_get_budget_details
	mov	r0, sl
	mov	r1, #0
	bl	nm_SYSTEM_get_budget
	ldr	r3, .L37+48
	ldr	sl, [sp, #340]
	mov	r1, #0
	adds	sl, sl, #0
	movne	sl, #1
	fmsr	s14, r0	@ int
	add	r0, sp, #392
	fuitos	s15, s14
	fmuls	s15, s20, s15
	fsts	s15, [r3, #0]
	bl	BUDGET_calculate_ratios
	add	r1, sp, #512
	str	sl, [sp, #4]
	ldrh	r3, [r1, #-116]
	add	r2, sp, #392
	add	r1, sp, #232
	strh	r3, [sp, #0]	@ movhi
	mov	r0, r5
	ldr	r3, [sp, #392]
	bl	nm_BUDGET_predicted_volume
	fmsr	s19, r0
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r6, .L37+52
	ldr	r2, .L37+16
	mov	r3, #138
	ldr	r0, [r6, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r7
	add	r1, sp, #416
	bl	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	ldr	r0, [sp, #416]
	bl	nm_BUDGET_get_used
	ldr	r3, .L37+56
	mov	r2, #48
	fmdrr	d7, r0, r1
	mov	r1, #0
	add	r0, sp, #344
	fcvtsd	s18, d7
	fmuls	s15, s20, s18
	fsts	s15, [r3, #0]
	fmsr	s15, r9	@ int
	ldr	r3, .L37+60
	fuitos	s24, s15
	fmuls	s15, s20, s24
	fsts	s15, [r3, #0]
	bl	memset
	add	r1, sp, #344
	mov	r0, r5
	bl	nm_BUDGET_calc_rpoc
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	add	r1, sp, #420
	add	r3, r1, r4, asl #2
	flds	s22, [r3, #-76]
	fsubs	s15, s16, s18
	mov	r6, #0
	fmuls	s22, s19, s22
	fdivs	s22, s15, s22
	fsubs	s23, s21, s22
	fmuls	s23, s23, s17
	fcmps	s23, s17
	fmstat
	fcpysgt	s23, s17
	bl	STATION_GROUP_get_num_groups_in_use
	mov	r7, r0
	b	.L4
.L7:
	mov	r0, r6
	bl	STATION_GROUP_get_group_at_this_index
	mov	sl, r0
	bl	STATION_GROUP_get_GID_irrigation_system
	cmp	r0, r5
	bne	.L5
.LBB2:
	mov	r0, sl
	bl	STATION_GROUP_get_budget_reduction_limit
	fmsr	s14, r0	@ int
	fuitos	s15, s14
	fcmpes	s23, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	cmp	r0, #99
	movhi	r0, #0
	andls	r0, r3, #1
	cmp	r0, #0
	bne	.L23
.L5:
.LBE2:
	add	r6, r6, #1
.L4:
	cmp	r6, r7
	bne	.L7
	mov	r5, #1
	b	.L6
.L23:
.LBB3:
	mov	r5, #0
.L6:
.LBE3:
	fadds	s24, s19, s24
	ldr	r3, .L37+64
	mov	r6, #0
	mov	r1, #224
	ldr	r2, [sp, #244]
	add	r0, sp, #8
	fmuls	s20, s20, s24
	fsts	s20, [r3, #0]
	mov	r3, #125
	str	r6, [sp, #0]
	bl	GetDateStr
	mov	r2, #16
	mov	r1, r0
	ldr	r0, .L37+68
	bl	strlcpy
	mov	r3, #125
	mov	r1, #224
	ldr	r2, [sp, #248]
	add	r0, sp, #8
	str	r6, [sp, #0]
	bl	GetDateStr
	mov	r2, #16
	mov	r1, r0
	ldr	r0, .L37+72
	bl	strlcpy
	ldr	r3, [sp, #248]
	ldr	r1, [sp, #392]
	ldr	r2, [sp, #236]
	cmp	r1, r2
	movhi	r2, r3
	addls	r2, r3, #1
	add	r1, sp, #512
	ldrh	r3, [r1, #-116]
	cmp	r8, r6
	rsb	r2, r3, r2
	ldr	r3, .L37+76
	str	r2, [r3, #0]
	addeq	r0, sp, #8
	moveq	r1, #224
	ldreq	r2, .L37+80
	beq	.L33
.L8:
	ldr	r3, [sp, #340]
	cmp	r3, #0
	bne	.L10
	fcmpes	s22, s21
	mov	r4, r4, asl #2
	fmstat
	blt	.L28
	add	r2, sp, #420
	add	r4, r2, r4
	flds	s15, [r4, #-76]
	add	r0, sp, #8
	mov	r1, #224
	ldr	r2, .L37+84
	fmacs	s18, s19, s15
	fmuls	s18, s18, s17
	fdivs	s16, s18, s16
	fsubs	s17, s17, s16
	b	.L35
.L28:
	fcmpezs	s22
	fmstat
	blt	.L29
	add	r3, sp, #420
	add	r4, r3, r4
	flds	s15, [r4, #-76]
	add	r0, sp, #8
	mov	r1, #224
	ldr	r2, .L37+88
	fmacs	s18, s19, s15
	fsubs	s18, s18, s16
	fmuls	s17, s18, s17
	fdivs	s16, s17, s16
	b	.L36
.L38:
	.align	2
.L37:
	.word	1065353216
	.word	1120403456
	.word	1144718131
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_BudgetUnitHCF
	.word	g_GROUP_ID
	.word	GuiVar_GroupName
	.word	GuiVar_POCBoxIndex
	.word	GuiVar_BudgetAllocation
	.word	list_system_recursive_MUTEX
	.word	GuiVar_BudgetMainlineName
	.word	GuiVar_BudgetAllocationSYS
	.word	poc_preserves_recursive_MUTEX
	.word	GuiVar_BudgetUsed
	.word	GuiVar_BudgetUsedSYS
	.word	GuiVar_BudgetEst
	.word	GuiVar_BudgetPeriod_0
	.word	GuiVar_BudgetPeriod_1
	.word	GuiVar_BudgetDaysLeftInPeriod
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	GuiVar_BudgetReportText
.L29:
	add	r1, sp, #420
	add	r4, r1, r4
	flds	s14, [r4, #-76]
	fsubs	s15, s18, s16
	add	r0, sp, #8
	mov	r1, #224
	fmacs	s18, s19, s14
	ldr	r2, .L37+92
	fmuls	s15, s15, s17
	fsubs	s18, s18, s16
	fdivs	s15, s15, s16
	fmuls	s17, s18, s17
	fdivs	s16, s17, s16
	ftouizs	s16, s16
	fsts	s16, [sp, #0]	@ int
	b	.L34
.L10:
	cmp	r3, #1
	bne	.L15
	fcmpes	s22, s21
	fmstat
	blt	.L30
	add	r2, sp, #420
	add	r4, r2, r4, asl #2
	flds	s15, [r4, #-76]
	ldr	r2, .L37+96
	add	r0, sp, #8
	fmacs	s18, s19, s15
	mov	r1, #224
	fmuls	s18, s18, s17
	fdivs	s16, s18, s16
	fsubs	s17, s17, s16
.L35:
	ftouizs	s17, s17
	fmrs	r3, s17	@ int
	b	.L32
.L30:
	fcmpezs	s22
	fmstat
	blt	.L31
	cmp	r5, #1
	bne	.L20
	ldr	r2, .L37+100
	add	r0, sp, #8
	mov	r1, #224
.L33:
	bl	snprintf
	b	.L9
.L20:
	add	r3, sp, #420
	add	r4, r3, r4, asl #2
	flds	s15, [r4, #-76]
	ldr	r2, .L37+104
	add	r0, sp, #8
	fmacs	s18, s19, s15
	mov	r1, #224
	fsubs	s18, s18, s16
	fmuls	s17, s18, s17
	fdivs	s16, s17, s16
.L36:
	ftouizs	s16, s16
	fmrs	r3, s16	@ int
.L32:
	bl	snprintf
	b	.L9
.L31:
	add	r1, sp, #420
	add	r4, r1, r4, asl #2
	flds	s14, [r4, #-76]
	fsubs	s15, s18, s16
	ldr	r2, .L37+108
	fmacs	s18, s19, s14
	add	r0, sp, #8
	mov	r1, #224
	fmuls	s15, s15, s17
	fsubs	s18, s18, s16
	fdivs	s15, s15, s16
	fmuls	s17, s18, s17
	fdivs	s16, s17, s16
	ftouizs	s16, s16
	fsts	s16, [sp, #0]	@ int
.L34:
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	bl	snprintf
	b	.L9
.L15:
	ldr	r0, .L37+112
	bl	Alert_Message
.L9:
	add	r1, sp, #8
	mov	r2, #225
	ldr	r0, .L37+116
	bl	strlcpy
	add	sp, sp, #420
	fldmfdd	sp!, {d8, d9, d10, d11, d12}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.LFE0:
	.size	BUDGET_REPORT_copy_group_into_guivars, .-BUDGET_REPORT_copy_group_into_guivars
	.section	.text.FDTO_BUDGET_REPORT_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_BUDGET_REPORT_draw_report
	.type	FDTO_BUDGET_REPORT_draw_report, %function
FDTO_BUDGET_REPORT_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L42
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI3:
	ldr	r2, .L42+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L42+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #1
	bl	POC_populate_pointers_of_POCs_for_display
	cmp	r4, #1
	ldr	r6, .L42+12
	mov	r5, r0
	bne	.L40
	ldr	r3, .L42+16
	mov	r0, #0
	str	r0, [r3, #0]
	str	r0, [r6, #0]
	bl	POC_get_ptr_to_physically_available_poc
	bl	POC_get_index_using_ptr_to_poc_struct
	ldr	r3, .L42+20
	ldr	r4, .L42+24
	str	r0, [r3, #0]
	bl	BUDGET_REPORT_copy_group_into_guivars
	b	.L41
.L40:
	ldr	r3, .L42+28
	mov	r0, #0
	ldrh	r4, [r3, #0]
	bl	GuiLib_ScrollBox_GetTopLine
	str	r0, [r6, #0]
.L41:
	mov	r1, r4, asl #16
	mov	r1, r1, asr #16
	mov	r2, #1
	mov	r0, #75
	bl	GuiLib_ShowScreen
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	bl	POC_get_menu_index_for_displayed_poc
	ldr	r1, .L42+12
	mov	r2, r5, asl #16
	ldrsh	r1, [r1, #0]
	mov	r2, r2, asr #16
	str	r1, [sp, #0]
	ldr	r1, .L42+32
	mov	r3, r0, asl #16
	mov	r3, r3, asr #16
	mov	r0, #0
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	ldr	r3, .L42
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_Refresh
.L43:
	.align	2
.L42:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	274
	.word	g_POC_top_line
	.word	g_POC_current_list_item_index
	.word	g_GROUP_list_item_index
	.word	65535
	.word	GuiLib_ActiveCursorFieldNo
	.word	POC_load_poc_name_into_guivar
.LFE1:
	.size	FDTO_BUDGET_REPORT_draw_report, .-FDTO_BUDGET_REPORT_draw_report
	.section	.text.BUDGET_REPORT_process_menu,"ax",%progbits
	.align	2
	.global	BUDGET_REPORT_process_menu
	.type	BUDGET_REPORT_process_menu, %function
BUDGET_REPORT_process_menu:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #20
	stmfd	sp!, {r4, lr}
.LCFI4:
	bhi	.L45
	mov	r2, #1
	ldr	r4, .L50
	mov	r2, r2, asl r0
	and	r4, r2, r4
	cmp	r4, #0
	bne	.L47
	ldr	r3, .L50+4
	and	r3, r2, r3
	cmp	r3, #0
	beq	.L45
	mov	r0, r4
	mov	r1, r4
	bl	SCROLL_BOX_up_or_down
	mov	r1, r4
	mov	r0, r4
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L50+8
	str	r0, [r3, #0]
	bl	POC_get_ptr_to_physically_available_poc
	bl	POC_get_index_using_ptr_to_poc_struct
	ldr	r3, .L50+12
	str	r0, [r3, #0]
	bl	BUDGET_REPORT_copy_group_into_guivars
	mov	r0, r4
	b	.L49
.L47:
	mov	r0, #0
	mov	r1, #4
	bl	SCROLL_BOX_up_or_down
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L50+8
	str	r0, [r3, #0]
	bl	POC_get_ptr_to_physically_available_poc
	bl	POC_get_index_using_ptr_to_poc_struct
	ldr	r3, .L50+12
	str	r0, [r3, #0]
	bl	BUDGET_REPORT_copy_group_into_guivars
	mov	r0, #0
.L49:
	ldmfd	sp!, {r4, lr}
	b	Redraw_Screen
.L45:
	cmp	r0, #67
	bne	.L48
	ldr	r3, .L50+16
	ldr	r2, .L50+20
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r2, [r3, #4]
	ldr	r3, .L50+24
	str	r2, [r3, #0]
.L48:
	ldmfd	sp!, {r4, lr}
	b	KEY_process_global_keys
.L51:
	.align	2
.L50:
	.word	65554
	.word	1048589
	.word	g_POC_current_list_item_index
	.word	g_GROUP_list_item_index
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	BUDGET_REPORT_process_menu, .-BUDGET_REPORT_process_menu
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_budgets.c\000"
.LC1:
	.ascii	"\000"
.LC2:
	.ascii	"Alert only\012POC expected to be %d%% UNDER BUDGET "
	.ascii	"at end of period\000"
.LC3:
	.ascii	"Alert only\012POC expected to be %d%% OVER BUDGET a"
	.ascii	"t end of period\000"
.LC4:
	.ascii	"Alert only\012POC NOW %d%% OVER BUDGET. Expected to"
	.ascii	" be %d%% over budget at end of period\000"
.LC5:
	.ascii	"POC expected to be %d%% UNDER BUDGET at end of peri"
	.ascii	"od\000"
.LC6:
	.ascii	"POC expected to be AT BUDGET at end of period\000"
.LC7:
	.ascii	"POC expected to be %d%% OVER BUDGET at end of perio"
	.ascii	"d\000"
.LC8:
	.ascii	"POC NOW %d%% OVER BUDGET. Expected to be %d%% over "
	.ascii	"budget at end of period\000"
.LC9:
	.ascii	"unknown budget mode\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x48
	.byte	0x5
	.uleb128 0x58
	.uleb128 0xa
	.byte	0x5
	.uleb128 0x56
	.uleb128 0xc
	.byte	0x5
	.uleb128 0x54
	.uleb128 0xe
	.byte	0x5
	.uleb128 0x52
	.uleb128 0x10
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x12
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x1ec
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_budgets.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x2f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x10c
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x131
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	.LCFI2
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 492
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"BUDGET_REPORT_copy_group_into_guivars\000"
.LASF0:
	.ascii	"FDTO_BUDGET_REPORT_draw_report\000"
.LASF1:
	.ascii	"BUDGET_REPORT_process_menu\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_budgets.c\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
