	.file	"d_copy_dialog.c"
	.text
.Ltext0:
	.section	.text.FDTO_COPY_DIALOG_draw_dialog,"ax",%progbits
	.align	2
	.type	FDTO_COPY_DIALOG_draw_dialog, %function
FDTO_COPY_DIALOG_draw_dialog:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI0:
	bne	.L2
	ldr	r3, .L3
	ldr	r4, .L3+4
	mov	r5, #0
	mov	r1, #400
	ldr	r2, .L3+8
	str	r5, [r3, #0]
	ldr	r0, [r4, #0]
	mov	r3, #90
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	STATION_GROUP_get_group_at_this_index
	bl	nm_GROUP_get_name
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L3+12
	bl	strlcpy
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
.L2:
	ldr	r3, .L3+16
	mov	r2, #1
	str	r2, [r3, #0]
	mvn	r1, #0
	mov	r0, #596
	bl	GuiLib_ShowScreen
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r3, .L3
	ldr	r1, .L3+20
	ldrsh	r3, [r3, #0]
	str	r3, [sp, #0]
	mov	r2, r0, asl #16
	mov	r2, r2, asr #16
	mov	r0, #2
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L4:
	.align	2
.L3:
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_GroupName
	.word	.LANCHOR1
	.word	nm_STATION_GROUP_load_group_name_into_guivar
.LFE1:
	.size	FDTO_COPY_DIALOG_draw_dialog, .-FDTO_COPY_DIALOG_draw_dialog
	.section	.text.COPY_DIALOG_peform_copy_and_redraw_screen,"ax",%progbits
	.align	2
	.global	COPY_DIALOG_peform_copy_and_redraw_screen
	.type	COPY_DIALOG_peform_copy_and_redraw_screen, %function
COPY_DIALOG_peform_copy_and_redraw_screen:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI1:
	subs	r7, r1, #0
	mov	r8, r0
	mov	r6, r2
	mov	r5, r3
	beq	.L6
	ldr	r4, .L7
	ldr	r2, .L7+4
	mov	r3, #52
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	blx	r7
	mov	r0, r6
	bl	STATION_GROUP_get_group_at_this_index
	mov	r6, r0
	mov	r0, r5
	bl	STATION_GROUP_get_group_at_this_index
	mov	r1, r0
	mov	r0, r6
	bl	STATION_GROUP_copy_group
	mov	r0, r5
	bl	STATION_GROUP_copy_group_into_guivars
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
.L6:
	mov	r3, #1
	str	r3, [r8, #0]
	ldr	r3, .L7+8
	mov	r0, #0
	strh	r0, [r3, #0]	@ movhi
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	Redraw_Screen
.L8:
	.align	2
.L7:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	GuiLib_ActiveCursorFieldNo
.LFE0:
	.size	COPY_DIALOG_peform_copy_and_redraw_screen, .-COPY_DIALOG_peform_copy_and_redraw_screen
	.section	.text.COPY_DIALOG_draw_dialog,"ax",%progbits
	.align	2
	.global	COPY_DIALOG_draw_dialog
	.type	COPY_DIALOG_draw_dialog, %function
COPY_DIALOG_draw_dialog:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L10
	str	lr, [sp, #-4]!
.LCFI2:
	str	r0, [r3, #0]
	ldr	r3, .L10+4
	sub	sp, sp, #36
.LCFI3:
	str	r1, [r3, #0]
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L10+8
	mov	r0, sp
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L11:
	.align	2
.L10:
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	FDTO_COPY_DIALOG_draw_dialog
.LFE2:
	.size	COPY_DIALOG_draw_dialog, .-COPY_DIALOG_draw_dialog
	.section	.text.COPY_DIALOG_process_dialog,"ax",%progbits
	.align	2
	.global	COPY_DIALOG_process_dialog
	.type	COPY_DIALOG_process_dialog, %function
COPY_DIALOG_process_dialog:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI4:
	mov	r6, r0
	sub	sp, sp, #36
.LCFI5:
	beq	.L16
	bhi	.L18
	cmp	r0, #2
	beq	.L15
	cmp	r0, #4
	beq	.L14
	cmp	r0, #0
	b	.L20
.L18:
	cmp	r0, #83
	beq	.L17
	cmp	r0, #84
	beq	.L14
	cmp	r0, #80
.L20:
	bne	.L13
	b	.L14
.L17:
	bl	KEY_process_ENG_SPA
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L21
	mov	r0, sp
	str	r3, [sp, #20]
	mov	r3, #0
	str	r3, [sp, #24]
	bl	Display_Post_Command
	b	.L12
.L15:
	bl	good_key_beep
	ldr	r3, .L21+4
	mov	r1, #0
	str	r1, [r3, #0]
	ldr	r3, .L21+8
	mov	r0, r6
	ldr	r5, [r3, #0]
	ldr	r3, .L21+12
	ldr	r4, [r3, #0]
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L21+16
	mov	r1, r4
	ldr	r3, [r3, #0]
	mov	r2, r0
	mov	r0, r5
	bl	COPY_DIALOG_peform_copy_and_redraw_screen
	b	.L12
.L14:
	mov	r0, #2
	mov	r1, r6
	bl	SCROLL_BOX_up_or_down
	b	.L12
.L16:
	bl	good_key_beep
	ldr	r3, .L21+4
	mov	r0, #0
	str	r0, [r3, #0]
	bl	Redraw_Screen
	b	.L12
.L13:
	bl	bad_key_beep
.L12:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, pc}
.L22:
	.align	2
.L21:
	.word	FDTO_COPY_DIALOG_draw_dialog
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	g_GROUP_list_item_index
.LFE3:
	.size	COPY_DIALOG_process_dialog, .-COPY_DIALOG_process_dialog
	.global	g_COPY_DIALOG_visible
	.section	.bss.g_COPY_DIALOG_visible,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_COPY_DIALOG_visible, %object
	.size	g_COPY_DIALOG_visible, 4
g_COPY_DIALOG_visible:
	.space	4
	.section	.bss.g_COPY_DIALOG_add_group_func_ptr,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_COPY_DIALOG_add_group_func_ptr, %object
	.size	g_COPY_DIALOG_add_group_func_ptr, 4
g_COPY_DIALOG_add_group_func_ptr:
	.space	4
	.section	.bss.g_COPY_DIALOG_selected_group_index,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_COPY_DIALOG_selected_group_index, %object
	.size	g_COPY_DIALOG_selected_group_index, 4
g_COPY_DIALOG_selected_group_index:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_copy_dialog.c\000"
	.section	.bss.g_COPY_DIALOG_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_COPY_DIALOG_editing_group, %object
	.size	g_COPY_DIALOG_editing_group, 4
g_COPY_DIALOG_editing_group:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_copy_dialog.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x4f
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x2e
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x6d
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x7e
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI5
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"FDTO_COPY_DIALOG_draw_dialog\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_copy_dialog.c\000"
.LASF0:
	.ascii	"COPY_DIALOG_peform_copy_and_redraw_screen\000"
.LASF2:
	.ascii	"COPY_DIALOG_process_dialog\000"
.LASF1:
	.ascii	"COPY_DIALOG_draw_dialog\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
