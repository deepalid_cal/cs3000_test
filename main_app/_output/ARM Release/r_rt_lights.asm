	.file	"r_rt_lights.c"
	.text
.Ltext0:
	.section	.text.REAL_TIME_LIGHTS_draw_scroll_line,"ax",%progbits
	.align	2
	.type	REAL_TIME_LIGHTS_draw_scroll_line, %function
REAL_TIME_LIGHTS_draw_scroll_line:
.LFB0:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L7
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI0:
	ldr	r2, .L7+4
	sub	sp, sp, #20
.LCFI1:
	str	r3, [sp, #16]	@ float
	ldr	r3, .L7+8
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #69
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L7+12
	ldr	r2, .L7+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #71
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	IRRI_LIGHTS_get_ptr_to_energized_light
	mov	r0, r4
	bl	IRRI_LIGHTS_get_box_index
	mov	r7, r0
	mov	r0, r4
	bl	IRRI_LIGHTS_get_output_index
	mov	r8, r0
	mov	r0, r4
	bl	IRRI_LIGHTS_get_stop_date
	mov	r6, r0
	mov	r0, r4
	bl	IRRI_LIGHTS_get_stop_time
	mov	r1, r8
	add	r8, r8, #1
	mov	r5, r0
	mov	r0, r7
	bl	nm_LIGHTS_get_pointer_to_light_struct_with_this_box_and_output_index
	bl	nm_GROUP_get_name
	mov	r1, #65
	add	r3, r7, r1
	ldr	r2, .L7+16
	str	r8, [sp, #0]
	str	r0, [sp, #4]
	ldr	r0, .L7+20
	bl	snprintf
	mov	r0, r4
	bl	IRRI_LIGHTS_get_reason_on_list
	ldr	r3, .L7+24
	str	r0, [r3, #0]
	add	r0, sp, #8
	bl	EPSON_obtain_latest_time_and_date
	ldrh	r1, [sp, #12]
	ldr	r3, [sp, #8]
	cmp	r6, r1
	ldr	r2, .L7+28
	bne	.L2
	cmp	r5, r3
	rsbcs	r5, r3, r5
	movcc	r5, #0
	flds	s15, [sp, #16]
	fmsr	s13, r5	@ int
	b	.L6
.L2:
	rsb	r5, r3, r5
	ldr	r3, .L7+32
	flds	s15, [sp, #16]
	rsb	r6, r1, r6
	mla	r3, r6, r3, r5
	fmsr	s13, r3	@ int
.L6:
	fuitos	s14, s13
	ldr	r3, .L7+12
	ldr	r0, [r3, #0]
	fdivs	s15, s14, s15
	fsts	s15, [r2, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L7+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L8:
	.align	2
.L7:
	.word	1114636288
	.word	.LC0
	.word	list_lights_recursive_MUTEX
	.word	irri_lights_recursive_MUTEX
	.word	.LC1
	.word	GuiVar_GroupName
	.word	GuiVar_LightStartReason
	.word	GuiVar_LightRemainingMinutes
	.word	86400
.LFE0:
	.size	REAL_TIME_LIGHTS_draw_scroll_line, .-REAL_TIME_LIGHTS_draw_scroll_line
	.section	.text.FDTO_REAL_TIME_LIGHTS_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_LIGHTS_draw_report
	.type	FDTO_REAL_TIME_LIGHTS_draw_report, %function
FDTO_REAL_TIME_LIGHTS_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	ldr	r4, .L10
	mov	r5, r0
	mvn	r1, #0
	mov	r0, #95
	mov	r2, #1
	bl	GuiLib_ShowScreen
	mov	r1, #400
	ldr	r2, .L10+4
	mov	r3, #136
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	IRRI_LIGHTS_populate_pointers_of_energized_lights
	ldr	r3, .L10+8
	ldr	r2, .L10+12
	mov	r1, r0
	str	r0, [r3, #0]
	mov	r0, r5
	bl	FDTO_REPORTS_draw_report
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L11:
	.align	2
.L10:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
	.word	REAL_TIME_LIGHTS_draw_scroll_line
.LFE1:
	.size	FDTO_REAL_TIME_LIGHTS_draw_report, .-FDTO_REAL_TIME_LIGHTS_draw_report
	.section	.text.FDTO_REAL_TIME_LIGHTS_redraw_scrollbox,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_LIGHTS_redraw_scrollbox
	.type	FDTO_REAL_TIME_LIGHTS_redraw_scrollbox, %function
FDTO_REAL_TIME_LIGHTS_redraw_scrollbox:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI3:
	ldr	r4, .L13
	mov	r3, #157
	mov	r1, #400
	ldr	r2, .L13+4
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	IRRI_LIGHTS_populate_pointers_of_energized_lights
	ldr	r6, .L13+8
	ldr	r2, [r6, #0]
	mov	r5, r0
	subs	r2, r5, r2
	mov	r0, #0
	mov	r1, r5
	movne	r2, #1
	bl	FDTO_SCROLL_BOX_redraw_retaining_topline
	ldr	r0, [r4, #0]
	str	r5, [r6, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L14:
	.align	2
.L13:
	.word	irri_lights_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
.LFE2:
	.size	FDTO_REAL_TIME_LIGHTS_redraw_scrollbox, .-FDTO_REAL_TIME_LIGHTS_redraw_scrollbox
	.section	.text.REAL_TIME_LIGHTS_process_report,"ax",%progbits
	.align	2
	.global	REAL_TIME_LIGHTS_process_report
	.type	REAL_TIME_LIGHTS_process_report, %function
REAL_TIME_LIGHTS_process_report:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	stmfd	sp!, {r4, lr}
.LCFI4:
	bne	.L19
	ldr	r3, .L20
	ldr	r2, .L20+4
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r4, .L20+8
	ldr	r3, [r3, #4]
	str	r3, [r4, #0]
	bl	KEY_process_global_keys
	ldr	r3, [r4, #0]
	cmp	r3, #10
	ldmnefd	sp!, {r4, pc}
	mov	r0, #1
	ldmfd	sp!, {r4, lr}
	b	LIVE_SCREENS_draw_dialog
.L19:
	mov	r2, #0
	mov	r3, r2
	ldmfd	sp!, {r4, lr}
	b	REPORTS_process_report
.L21:
	.align	2
.L20:
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	REAL_TIME_LIGHTS_process_report, .-REAL_TIME_LIGHTS_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_lights.c\000"
.LC1:
	.ascii	"Light %c%d: %s\000"
	.section	.bss.g_REAL_TIME_LIGHTS_PreviousEnergizedCount,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_REAL_TIME_LIGHTS_PreviousEnergizedCount, %object
	.size	g_REAL_TIME_LIGHTS_PreviousEnergizedCount, 4
g_REAL_TIME_LIGHTS_PreviousEnergizedCount:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_rt_lights.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6d
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x28
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x80
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x98
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xb2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_lights.c\000"
.LASF5:
	.ascii	"REAL_TIME_LIGHTS_draw_scroll_line\000"
.LASF0:
	.ascii	"FDTO_REAL_TIME_LIGHTS_draw_report\000"
.LASF1:
	.ascii	"FDTO_REAL_TIME_LIGHTS_redraw_scrollbox\000"
.LASF2:
	.ascii	"REAL_TIME_LIGHTS_process_report\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
