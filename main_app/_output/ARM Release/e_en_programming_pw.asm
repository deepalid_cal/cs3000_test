	.file	"e_en_programming_pw.c"
	.text
.Ltext0:
	.section	.text.FDTO_PW_XE_PROGRAMMING_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_PW_XE_PROGRAMMING_draw_screen
	.type	FDTO_PW_XE_PROGRAMMING_draw_screen, %function
FDTO_PW_XE_PROGRAMMING_draw_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	subs	r4, r0, #0
	ldreq	r3, .L5
	ldreqsh	r1, [r3, #0]
	beq	.L3
	ldr	r3, .L5+4
	mov	r5, #0
	str	r1, [r3, #0]
	ldr	r3, .L5+8
	ldr	r6, .L5+12
	str	r5, [r3, #0]
	ldr	r3, .L5+16
	ldr	r0, .L5+20
	str	r5, [r3, #0]
	ldr	r3, .L5+24
	str	r3, [r6, #0]
	bl	e_SHARED_index_keycode_for_gui
	mov	r1, r5
	str	r0, [r6, #0]
.L3:
	mov	r0, #17
	mov	r2, #1
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
	cmp	r4, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
.LBB4:
	bl	DEVICE_EXCHANGE_draw_dialog
	ldr	r3, .L5+4
	ldr	r2, .L5+28
	ldr	r0, [r3, #0]
	mov	r1, #82
.LBE4:
	ldmfd	sp!, {r4, r5, r6, lr}
.LBB5:
	b	e_SHARED_start_device_communication
.L6:
	.align	2
.L5:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	.LANCHOR2
	.word	36867
	.word	36865
	.word	.LANCHOR3
.LBE5:
.LFE2:
	.size	FDTO_PW_XE_PROGRAMMING_draw_screen, .-FDTO_PW_XE_PROGRAMMING_draw_screen
	.section	.text.FDTO_PW_XE_PROGRAMMING_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_PW_XE_PROGRAMMING_process_device_exchange_key, %function
FDTO_PW_XE_PROGRAMMING_process_device_exchange_key:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r4, r0
	bl	e_SHARED_get_info_text_from_programming_struct
	sub	r3, r4, #36864
	cmp	r3, #6
	ldmhifd	sp!, {r4, pc}
	mov	r2, #1
	mov	r3, r2, asl r3
	tst	r3, #73
	bne	.L9
	tst	r3, #36
	bne	.L11
	tst	r3, #18
	ldmeqfd	sp!, {r4, pc}
.LBB8:
	ldr	r3, .L15
	ldr	r4, .L15+4
	ldr	r3, [r3, #0]
	cmp	r3, r2
	ldreq	r0, .L15+8
	beq	.L14
	bl	PW_XE_PROGRAMMING_copy_programming_into_GuiVars
	ldr	r0, .L15+12
.L14:
	bl	e_SHARED_index_keycode_for_gui
	str	r0, [r4, #0]
	bl	DIALOG_close_ok_dialog
	ldr	r3, .L15+16
	mov	r0, #0
	ldr	r1, [r3, #0]
.LBE8:
	ldmfd	sp!, {r4, lr}
.LBB9:
	b	FDTO_PW_XE_PROGRAMMING_draw_screen
.L11:
.LBE9:
	bl	PW_XE_PROGRAMMING_copy_programming_into_GuiVars
.L9:
	mov	r0, r4
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L15+4
	str	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	DEVICE_EXCHANGE_draw_dialog
.L16:
	.align	2
.L15:
	.word	.LANCHOR1
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36867
	.word	36865
	.word	.LANCHOR0
.LFE0:
	.size	FDTO_PW_XE_PROGRAMMING_process_device_exchange_key, .-FDTO_PW_XE_PROGRAMMING_process_device_exchange_key
	.section	.text.FDTO_PW_XE_PROGRAMMING_redraw_screen,"ax",%progbits
	.align	2
	.type	FDTO_PW_XE_PROGRAMMING_redraw_screen, %function
FDTO_PW_XE_PROGRAMMING_redraw_screen:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L18
	ldr	r1, [r3, #0]
	b	FDTO_PW_XE_PROGRAMMING_draw_screen
.L19:
	.align	2
.L18:
	.word	.LANCHOR0
.LFE1:
	.size	FDTO_PW_XE_PROGRAMMING_redraw_screen, .-FDTO_PW_XE_PROGRAMMING_redraw_screen
	.section	.text.PW_XE_PROGRAMMING_process_screen,"ax",%progbits
	.align	2
	.global	PW_XE_PROGRAMMING_process_screen
	.type	PW_XE_PROGRAMMING_process_screen, %function
PW_XE_PROGRAMMING_process_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L99
	ldr	r2, .L99+4
	ldrsh	r3, [r3, #0]
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	cmp	r3, r2
	sub	sp, sp, #44
.LCFI3:
	mov	r4, r0
	mov	r5, r1
	beq	.L23
	cmp	r3, #740
	beq	.L24
	sub	r2, r2, #117
	cmp	r3, r2
	bne	.L85
	cmp	r0, #67
	beq	.L25
	cmp	r0, #2
	bne	.L26
	ldr	r3, .L99+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L26
.L25:
	ldr	r0, .L99+12
	ldr	r1, .L99+16
	mov	r2, #17
	bl	strlcpy
.L26:
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L99+20
	bl	KEYBOARD_process_key
	b	.L20
.L23:
	ldr	r1, .L99+24
	b	.L88
.L24:
	ldr	r1, .L99+28
.L88:
	bl	COMBO_BOX_key_press
	b	.L20
.L85:
	cmp	r0, #4
	beq	.L33
	bhi	.L37
	cmp	r0, #1
	beq	.L30
	bcc	.L29
	cmp	r0, #2
	beq	.L31
	cmp	r0, #3
	bne	.L28
	b	.L32
.L37:
	cmp	r0, #84
	beq	.L35
	bhi	.L38
	cmp	r0, #67
	beq	.L34
	cmp	r0, #80
	bne	.L28
	b	.L35
.L38:
	sub	r3, r0, #36864
	cmp	r3, #6
	bhi	.L28
	ldr	r3, .L99+32
	ldr	r2, .L99+36
	str	r0, [sp, #32]
	cmp	r0, r2
	cmpne	r0, r3
	ldrne	r3, .L99+40
	movne	r2, #0
	strne	r2, [r3, #0]
	mov	r3, #2
	str	r3, [sp, #8]
	ldr	r3, .L99+44
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	ldr	r3, .L99+48
	cmp	r4, r3
	bne	.L40
	ldr	r3, .L99+52
	ldr	r2, [r3, #0]
	cmp	r2, #1
	bne	.L20
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r3, .L99+56
	mov	r1, #82
	ldr	r0, [r3, #0]
	ldr	r2, .L99+40
	b	.L89
.L40:
	ldr	r3, .L99+60
	cmp	r4, r3
	moveq	r2, #0
	bne	.L20
	b	.L90
.L31:
	ldr	r4, .L99+64
	ldr	r0, .L99+32
	ldr	r5, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r5, r0
	beq	.L80
	ldr	r0, .L99+36
	ldr	r4, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r4, r0
	beq	.L80
	ldr	r5, .L99+8
	ldrsh	r3, [r5, #0]
	cmp	r3, #12
	ldrls	pc, [pc, r3, asl #2]
	b	.L80
.L48:
	.word	.L43
	.word	.L44
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L45
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L46
	.word	.L47
.L43:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L99+68
	b	.L91
.L44:
	bl	good_key_beep
	ldr	r1, .L99+12
	mov	r2, #17
	ldr	r0, .L99+16
	bl	strlcpy
	mov	r1, #0
	mov	r2, #17
	ldr	r0, .L99+12
	bl	memset
	mov	r0, #102
	mov	r1, #68
	mov	r2, #17
	mov	r3, #0
	bl	KEYBOARD_draw_keyboard
	b	.L20
.L45:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L99+72
.L91:
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L20
.L47:
	ldr	r4, .L99+40
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L80
	bl	good_key_beep
	ldrsh	r2, [r5, #0]
	ldr	r3, .L99+76
	str	r2, [r3, #0]
	bl	PW_XE_PROGRAMMING_extract_and_store_GuiVars
	ldr	r3, .L99+56
	mov	r2, r4
	ldr	r0, [r3, #0]
	mov	r1, #87
	bl	e_SHARED_start_device_communication
	mov	r2, #1
.L90:
	ldr	r3, .L99+52
	str	r2, [r3, #0]
	b	.L20
.L46:
	ldr	r4, .L99+40
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L80
	bl	good_key_beep
	ldrsh	r2, [r5, #0]
	ldr	r3, .L99+76
	mov	r1, #82
	str	r2, [r3, #0]
	ldr	r3, .L99+56
	mov	r2, r4
	ldr	r0, [r3, #0]
.L89:
	bl	e_SHARED_start_device_communication
	b	.L20
.L35:
	ldr	r3, .L99+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #10
	ldrls	pc, [pc, r3, asl #2]
	b	.L51
.L62:
	.word	.L52
	.word	.L51
	.word	.L53
	.word	.L54
	.word	.L55
	.word	.L56
	.word	.L57
	.word	.L58
	.word	.L59
	.word	.L60
	.word	.L61
.L52:
	ldr	r0, .L99+28
	bl	process_bool
	mov	r0, #0
	bl	Redraw_Screen
	b	.L63
.L53:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L99+80
	b	.L86
.L54:
	ldr	r1, .L99+84
	mov	r3, #1
	mov	r0, r4
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L86:
	mov	r2, #0
	mov	r3, #255
.L87:
	bl	process_uns32
	b	.L63
.L55:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L99+88
	b	.L86
.L56:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L99+92
	b	.L86
.L57:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L99+24
	mov	r3, #24
	b	.L87
.L58:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L99+96
	b	.L86
.L59:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L99+100
	b	.L86
.L60:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L99+104
	b	.L86
.L61:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L99+108
	b	.L86
.L51:
	bl	bad_key_beep
.L63:
	bl	Refresh_Screen
	b	.L20
.L33:
	ldr	r3, .L99+8
	ldrsh	r3, [r3, #0]
	sub	r2, r3, #2
	cmp	r2, #10
	ldrls	pc, [pc, r2, asl #2]
	b	.L64
.L70:
	.word	.L65
	.word	.L65
	.word	.L65
	.word	.L65
	.word	.L82
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L68
	.word	.L78
.L65:
	ldr	r2, .L99+76
	mov	r0, #0
	str	r3, [r2, #0]
	b	.L92
.L68:
	ldr	r3, .L99+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L78
.L64:
	mov	r0, #1
.L30:
	bl	CURSOR_Up
	b	.L20
.L29:
	ldr	r3, .L99+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L32
.L81:
	.word	.L76
	.word	.L32
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L78
	.word	.L79
	.word	.L79
	.word	.L79
	.word	.L79
	.word	.L80
.L76:
	ldr	r3, .L99+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L32
.L82:
	ldr	r3, .L99+76
	ldr	r2, [r3, #0]
	sub	r2, r2, #2
	cmp	r2, #3
	movhi	r2, #2
	bhi	.L93
	b	.L84
.L77:
	ldr	r2, .L99+76
	mov	r0, #6
	str	r3, [r2, #0]
	b	.L92
.L78:
	ldr	r3, .L99+76
	ldr	r2, [r3, #0]
	sub	r2, r2, #7
	cmp	r2, #3
	bls	.L84
	mov	r2, #7
.L93:
	str	r2, [r3, #0]
.L84:
	ldr	r0, [r3, #0]
	b	.L92
.L79:
	ldr	r2, .L99+76
	mov	r0, #11
	str	r3, [r2, #0]
.L92:
	mov	r1, #1
	bl	CURSOR_Select
	b	.L20
.L80:
	bl	bad_key_beep
	b	.L20
.L32:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L20
.L34:
	ldr	r3, .L99+112
	mov	r2, #11
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	b	.L20
.L28:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L20:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L100:
	.align	2
.L99:
	.word	GuiLib_CurStructureNdx
	.word	726
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_ENDHCPName
	.word	GuiVar_GroupName
	.word	FDTO_PW_XE_PROGRAMMING_redraw_screen
	.word	GuiVar_ENNetmask
	.word	GuiVar_ENObtainIPAutomatically
	.word	36867
	.word	36870
	.word	.LANCHOR3
	.word	FDTO_PW_XE_PROGRAMMING_process_device_exchange_key
	.word	36868
	.word	.LANCHOR1
	.word	.LANCHOR0
	.word	36869
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	FDTO_e_SHARED_show_obtain_ip_automatically_dropdown
	.word	FDTO_e_SHARED_show_subnet_dropdown
	.word	.LANCHOR4
	.word	GuiVar_ENIPAddress_1
	.word	GuiVar_ENIPAddress_2
	.word	GuiVar_ENIPAddress_3
	.word	GuiVar_ENIPAddress_4
	.word	GuiVar_ENGateway_1
	.word	GuiVar_ENGateway_2
	.word	GuiVar_ENGateway_3
	.word	GuiVar_ENGateway_4
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	PW_XE_PROGRAMMING_process_screen, .-PW_XE_PROGRAMMING_process_screen
	.section	.bss.g_PW_XE_PROGRAMMING_port,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_PW_XE_PROGRAMMING_port, %object
	.size	g_PW_XE_PROGRAMMING_port, 4
g_PW_XE_PROGRAMMING_port:
	.space	4
	.section	.bss.g_PW_XE_PROGRAMMING_editing_wifi_settings,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_PW_XE_PROGRAMMING_editing_wifi_settings, %object
	.size	g_PW_XE_PROGRAMMING_editing_wifi_settings, 4
g_PW_XE_PROGRAMMING_editing_wifi_settings:
	.space	4
	.section	.bss.g_PW_XE_PROGRAMMING_PW_querying_device,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_PW_XE_PROGRAMMING_PW_querying_device, %object
	.size	g_PW_XE_PROGRAMMING_PW_querying_device, 4
g_PW_XE_PROGRAMMING_PW_querying_device:
	.space	4
	.section	.bss.g_PW_XE_PROGRAMMING_previous_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_PW_XE_PROGRAMMING_previous_cursor_pos, %object
	.size	g_PW_XE_PROGRAMMING_previous_cursor_pos, 4
g_PW_XE_PROGRAMMING_previous_cursor_pos:
	.space	4
	.section	.bss.g_PW_XE_PROGRAMMING_read_after_write_pending,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_PW_XE_PROGRAMMING_read_after_write_pending, %object
	.size	g_PW_XE_PROGRAMMING_read_after_write_pending, 4
g_PW_XE_PROGRAMMING_read_after_write_pending:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_en_programming_pw.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x77
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF0
	.byte	0x1
	.4byte	.LASF1
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x88
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0x44
	.byte	0x1
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	0x2a
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.byte	0x7b
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xba
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"FDTO_PW_XE_PROGRAMMING_redraw_screen\000"
.LASF1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_en_programming_pw.c\000"
.LASF3:
	.ascii	"FDTO_PW_XE_PROGRAMMING_process_device_exchange_key\000"
.LASF0:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"FDTO_PW_XE_PROGRAMMING_draw_screen\000"
.LASF5:
	.ascii	"PW_XE_PROGRAMMING_process_screen\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
