	.file	"ftimes_task.c"
	.text
.Ltext0:
	.section	.text.ftimes_post_event,"ax",%progbits
	.align	2
	.type	ftimes_post_event, %function
ftimes_post_event:
.LFB1:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
.LBB8:
	ldr	r3, .L3
.LBE8:
	stmfd	sp!, {r0, lr}
.LCFI0:
.LBB9:
	mov	r2, #0
.LBE9:
	add	r1, sp, #4
	str	r0, [r1, #-4]!
.LBB10:
	ldr	r0, [r3, #0]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L1
	ldr	r0, .L3+4
	bl	Alert_Message
.L1:
.LBE10:
	ldmfd	sp!, {r3, pc}
.L4:
	.align	2
.L3:
	.word	FTIMES_task_queue
	.word	.LC0
.LFE1:
	.size	ftimes_post_event, .-ftimes_post_event
	.global	__umodsi3
	.global	__udivsi3
	.section	.text.FTIMES_TASK_task,"ax",%progbits
	.align	2
	.global	FTIMES_TASK_task
	.type	FTIMES_TASK_task, %function
FTIMES_TASK_task:
.LFB5:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	fstmfdd	sp!, {d8}
.LCFI2:
.LBB15:
	flds	s16, .L29
.LBE15:
	ldr	r6, .L29+4
	ldr	r4, .L29+8
	rsb	r6, r6, r0
	mov	r3, #4096
	sub	sp, sp, #4
.LCFI3:
	mov	r6, r6, lsr #5
	mov	r5, r4
	str	r3, [r4, #0]
.L24:
	ldr	r3, .L29+12
	mov	r1, sp
	ldr	r0, [r3, #0]
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	bne	.L6
	ldr	r3, [sp, #0]
	ldr	r2, .L29+16
	cmp	r3, r2
	beq	.L8
	add	r2, r2, #4352
	cmp	r3, r2
	bne	.L25
	b	.L28
.L8:
	bl	FTIMES_VARS_clean_and_reload_all_ftimes_data_structures
	ldr	r3, .L29+20
	str	r3, [r4, #0]
	b	.L27
.L28:
.LBB18:
.LBB16:
	ldr	r0, .L29+24
	bl	nm_ListGetFirst
	b	.L26
.L11:
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldr	r0, .L29+24
	bic	r3, r3, #8
	strb	r3, [r1, #0]
	bl	nm_ListGetNext
.L26:
	cmp	r0, #0
	mov	r1, r0
	bne	.L11
.LBE16:
.LBB17:
	ldr	r0, .L29+24
	ldr	r3, [r0, #8]
	cmp	r3, #0
	bne	.L12
	ldr	r0, [r4, #8]
	mov	r1, #600
	bl	__umodsi3
	rsb	r0, r0, #600
	str	r0, [r4, #64]
	b	.L13
.L12:
	bl	nm_ListGetFirst
	mvn	r7, #0
	mov	r1, r0
	b	.L14
.L18:
	ldr	r3, [r1, #64]
	cmp	r3, #0
	bne	.L16
	ldr	r3, [r1, #84]
	cmp	r3, #0
	beq	.L17
.L16:
	cmp	r7, r3
	movcs	r7, r3
.L17:
	ldr	r0, .L29+24
	bl	nm_ListGetNext
	mov	r1, r0
.L14:
	cmp	r1, #0
	bne	.L18
	cmn	r7, #1
	moveq	r3, #1
	streq	r3, [r4, #64]
	beq	.L13
	ldr	r0, [r4, #8]
	mov	r1, #600
	bl	__umodsi3
	rsb	r0, r0, #600
	cmp	r7, r0
	strls	r7, [r4, #64]
	strhi	r0, [r4, #64]
.L13:
.LBE17:
	ldr	r0, .L29+28
	ldr	r1, [r4, #64]
	bl	TDUTILS_add_seconds_to_passed_DT_ptr
	mov	r3, #0
	str	r3, [r4, #28]
	ldr	r7, [r4, #64]
	ldr	r3, [r4, #44]
	add	r7, r7, r3
	ldr	r3, [r4, #48]
	str	r7, [r4, #44]
	cmp	r7, r3
	bcc	.L20
	mov	r0, #100
	mul	r0, r7, r0
	ldr	r1, [r4, #40]
	bl	__udivsi3
	add	r7, r7, #12096
	str	r7, [r4, #48]
	fmsr	s12, r0	@ int
	fuitos	s15, s12
	fsts	s15, [r4, #52]
.L20:
	ldrh	r2, [r4, #12]
	ldrh	r3, [r4, #36]
	cmp	r2, r3
	bcc	.L21
	ldr	r2, [r4, #8]
	ldr	r3, [r4, #32]
	cmp	r2, r3
	bcs	.L22
.L21:
	ldr	r0, [r5, #8]
	mov	r1, #600
	bl	__umodsi3
	cmp	r0, #0
	bne	.L23
	bl	FTIMES_FUNCS_check_for_a_start
.L23:
	bl	FTIMES_FUNCS_maintain_irrigation_list
.L27:
	ldr	r0, .L29+32
	bl	ftimes_post_event
	b	.L6
.L25:
.LBE18:
	ldr	r0, .L29+36
	bl	Alert_Message
.L6:
	bl	xTaskGetTickCount
	ldr	r3, .L29+40
	str	r0, [r3, r6, asl #2]
	b	.L24
.L22:
.LBB19:
	ldr	r3, .L29+44
	ldr	r0, .L29+48
	ldr	r2, [r3, #0]
	ldr	r3, [r4, #60]
	rsb	r3, r3, r2
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fdivs	s15, s15, s16
	fcvtds	d6, s15
	fsts	s15, [r4, #56]
	fmrrd	r1, r2, d6
	bl	Alert_Message_va
	ldr	r3, .L29+52
	str	r3, [r4, #0]
	b	.L6
.L30:
	.align	2
.L29:
	.word	1128792064
	.word	Task_Table
	.word	.LANCHOR0
	.word	FTIMES_task_queue
	.word	4369
	.word	4097
	.word	ft_irrigating_stations_list_hdr
	.word	.LANCHOR0+8
	.word	8721
	.word	.LC1
	.word	task_last_execution_stamp
	.word	my_tick_count
	.word	.LC2
	.word	4098
.LBE19:
.LFE5:
	.size	FTIMES_TASK_task, .-FTIMES_TASK_task
	.section	.text.FTIMES_TASK_restart_calculation,"ax",%progbits
	.align	2
	.global	FTIMES_TASK_restart_calculation
	.type	FTIMES_TASK_restart_calculation, %function
FTIMES_TASK_restart_calculation:
.LFB6:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI4:
	ldr	r4, .L34
.L32:
	mov	r2, #0
	ldr	r0, [r4, #0]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	bne	.L32
	ldr	r0, .L34+4
	bl	ftimes_post_event
	ldmfd	sp!, {r3, r4, pc}
.L35:
	.align	2
.L34:
	.word	FTIMES_task_queue
	.word	4369
.LFE6:
	.size	FTIMES_TASK_restart_calculation, .-FTIMES_TASK_restart_calculation
	.global	ftcs
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"FTimes queue overflow!\000"
.LC1:
	.ascii	"unhandled ftimes event\000"
.LC2:
	.ascii	"Ftimes duration: %5.2f\000"
	.section	.bss.ftcs,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	ftcs, %object
	.size	ftcs, 68
ftcs:
	.space	68
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI1-.LFB5
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x1c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI4-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ftimes/ftimes_task.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x7a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF6
	.byte	0x1
	.4byte	.LASF7
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x1b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x89
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0x38
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x1
	.byte	0x29
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.byte	0x99
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xf3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x13d
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB5
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB6
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"clear_done_with_cycle_for_irrigating_stations\000"
.LASF8:
	.ascii	"ftimes_post_event\000"
.LASF2:
	.ascii	"calculate_seconds_to_advance\000"
.LASF5:
	.ascii	"FTIMES_TASK_restart_calculation\000"
.LASF3:
	.ascii	"iterate\000"
.LASF7:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ftim"
	.ascii	"es/ftimes_task.c\000"
.LASF6:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"ftimes_post_event_with_details\000"
.LASF4:
	.ascii	"FTIMES_TASK_task\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
