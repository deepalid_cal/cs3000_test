	.file	"m_main.c"
	.text
.Ltext0:
	.section	.text.MAIN_MENU_store_prev_cursor_pos,"ax",%progbits
	.align	2
	.type	MAIN_MENU_store_prev_cursor_pos, %function
MAIN_MENU_store_prev_cursor_pos:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L16
	ldr	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r3, #10
	ldrls	pc, [pc, r3, asl #2]
	b	.L1
.L14:
	.word	.L3
	.word	.L4
	.word	.L5
	.word	.L6
	.word	.L7
	.word	.L8
	.word	.L9
	.word	.L10
	.word	.L11
	.word	.L12
	.word	.L13
.L3:
	ldr	r3, .L16+4
	b	.L15
.L4:
	ldr	r3, .L16+8
	b	.L15
.L5:
	ldr	r3, .L16+12
	b	.L15
.L6:
	ldr	r3, .L16+16
	b	.L15
.L7:
	ldr	r3, .L16+20
	b	.L15
.L8:
	ldr	r3, .L16+24
	b	.L15
.L9:
	ldr	r3, .L16+28
	b	.L15
.L10:
	ldr	r3, .L16+32
	b	.L15
.L11:
	ldr	r3, .L16+36
	b	.L15
.L12:
	ldr	r3, .L16+40
.L15:
	str	r0, [r3, #0]
	bx	lr
.L13:
	ldr	r3, .L16+44
	str	r0, [r3, #0]
.L1:
	bx	lr
.L17:
	.align	2
.L16:
	.word	GuiVar_MenuScreenToShow
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LANCHOR4
	.word	.LANCHOR5
	.word	.LANCHOR6
	.word	.LANCHOR7
	.word	.LANCHOR8
	.word	.LANCHOR9
	.word	.LANCHOR10
.LFE16:
	.size	MAIN_MENU_store_prev_cursor_pos, .-MAIN_MENU_store_prev_cursor_pos
	.section	.text.MAIN_MENU_change_screen,"ax",%progbits
	.align	2
	.type	MAIN_MENU_change_screen, %function
MAIN_MENU_change_screen:
.LFB1:
	@ args = 4, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L19
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	ip, [ip, #0]
	ldr	lr, .L19+4
	mov	r4, #36
	mla	ip, r4, ip, lr
	ldr	lr, .L19+8
	sub	sp, sp, #36
.LCFI1:
	ldr	r4, [lr, #0]
	str	r3, [sp, #16]
	str	r4, [ip, #32]
	ldr	ip, .L19+12
	ldr	r3, [sp, #44]
	ldrsh	ip, [ip, #0]
	mov	r4, #0
	str	ip, [lr, #0]
	ldr	lr, .L19+16
	stmib	sp, {r0, r1}
	str	r4, [lr, #0]
	str	r2, [sp, #20]
	mov	lr, #2
	mov	r2, #1
	mov	r0, sp
	str	lr, [sp, #0]
	str	r2, [sp, #24]
	str	ip, [sp, #32]
	str	r3, [sp, #12]
	bl	Change_Screen
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L20:
	.align	2
.L19:
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_GROUP_list_item_index
.LFE1:
	.size	MAIN_MENU_change_screen, .-MAIN_MENU_change_screen
	.section	.text.MAIN_MENU_return_from_sub_menu,"ax",%progbits
	.align	2
	.type	MAIN_MENU_return_from_sub_menu, %function
MAIN_MENU_return_from_sub_menu:
.LFB15:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L22
	ldr	r2, .L22+4
	ldr	r1, [r3, #0]
	str	lr, [sp, #-4]!
.LCFI2:
	strh	r1, [r2, #0]	@ movhi
	mvn	r2, #0
	sub	sp, sp, #36
.LCFI3:
	str	r2, [r3, #0]
	mov	r0, #0
	bl	Redraw_Screen
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L22+8
	mov	r0, sp
	str	r3, [sp, #20]
	mov	r3, #0
	str	r3, [sp, #24]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L23:
	.align	2
.L22:
	.word	.LANCHOR11
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_MAIN_MENU_refresh_menu
.LFE15:
	.size	MAIN_MENU_return_from_sub_menu, .-MAIN_MENU_return_from_sub_menu
	.section	.text.FDTO_MAIN_MENU_show_screen_name_in_title_bar,"ax",%progbits
	.align	2
	.type	FDTO_MAIN_MENU_show_screen_name_in_title_bar, %function
FDTO_MAIN_MENU_show_screen_name_in_title_bar:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L28
	str	lr, [sp, #-4]!
.LCFI4:
	ldr	r3, [r3, #0]
	cmp	r3, #11
	bhi	.L25
	ldr	r2, .L28+4
	mov	r3, r3, asl #1
	ldrsh	r1, [r2, r3]
	cmn	r1, #1
	beq	.L25
	ldr	r3, .L28+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	moveq	r0, #45
	movne	r0, #64
	bl	FDTO_show_screen_name_in_title_bar
.L25:
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L29:
	.align	2
.L28:
	.word	.LANCHOR11
	.word	.LANCHOR12
	.word	GuiLib_LanguageIndex
.LFE0:
	.size	FDTO_MAIN_MENU_show_screen_name_in_title_bar, .-FDTO_MAIN_MENU_show_screen_name_in_title_bar
	.section	.text.FDTO_MAIN_MENU_refresh_menu,"ax",%progbits
	.align	2
	.type	FDTO_MAIN_MENU_refresh_menu, %function
FDTO_MAIN_MENU_refresh_menu:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L50
	stmfd	sp!, {r4, r5, lr}
.LCFI5:
	ldr	r3, [r2, #0]
	mov	r4, r0
	mov	r5, r2
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L47
.L44:
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
.L32:
	bl	FDTO_STATUS_draw_screen
	b	.L47
.L33:
	ldr	r3, .L50+4
	b	.L48
.L34:
	ldr	r3, .L50+8
	b	.L48
.L35:
	ldr	r3, .L50+12
	b	.L48
.L36:
	ldr	r3, .L50+16
	b	.L48
.L37:
	ldr	r3, .L50+20
	b	.L48
.L38:
	ldr	r3, .L50+24
	b	.L48
.L39:
	ldr	r3, .L50+28
	b	.L48
.L40:
	ldr	r3, .L50+32
	b	.L48
.L41:
	ldr	r3, .L50+36
	b	.L48
.L42:
	ldr	r3, .L50+40
	b	.L48
.L43:
	ldr	r3, .L50+44
.L48:
	ldr	r0, [r3, #0]
	b	.L31
.L47:
	mov	r0, #0
.L31:
	ldr	r2, [r5, #0]
	cmp	r2, #0
	beq	.L45
	ldr	r3, .L50+48
	cmp	r4, #0
	strne	r2, [r3, #0]
	movne	r1, #0
	bne	.L49
	ldr	r3, [r3, #0]
	cmn	r3, #1
	beq	.L45
	mov	r1, r4
.L49:
	bl	FDTO_Cursor_Select
.L45:
	ldmfd	sp!, {r4, r5, lr}
	b	FDTO_MAIN_MENU_show_screen_name_in_title_bar
.L51:
	.align	2
.L50:
	.word	GuiVar_MenuScreenToShow
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LANCHOR4
	.word	.LANCHOR5
	.word	.LANCHOR6
	.word	.LANCHOR7
	.word	.LANCHOR8
	.word	.LANCHOR9
	.word	.LANCHOR10
	.word	.LANCHOR11
.LFE13:
	.size	FDTO_MAIN_MENU_refresh_menu, .-FDTO_MAIN_MENU_refresh_menu
	.section	.text.FDTO_MAIN_MENU_jump_to_submenu,"ax",%progbits
	.align	2
	.type	FDTO_MAIN_MENU_jump_to_submenu, %function
FDTO_MAIN_MENU_jump_to_submenu:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI6:
	ldr	r5, .L70
	ldr	r3, .L70+4
	ldr	r2, [r5, #0]
	ldr	r4, .L70+8
	str	r2, [r3, #0]
	mov	r3, #20
	str	r3, [r4, #0]
	bl	FDTO_MAIN_MENU_show_screen_name_in_title_bar
	ldr	r2, [r5, #0]
	sub	r2, r2, #1
	cmp	r2, #10
	ldrls	pc, [pc, r2, asl #2]
	b	.L53
.L65:
	.word	.L54
	.word	.L55
	.word	.L56
	.word	.L57
	.word	.L58
	.word	.L59
	.word	.L60
	.word	.L61
	.word	.L62
	.word	.L63
	.word	.L64
.L54:
	ldr	r0, [r4, #0]
	ldr	r3, .L70+12
	b	.L69
.L55:
	ldr	r0, [r4, #0]
	ldr	r3, .L70+16
.L69:
	ldr	r3, [r3, #0]
	cmp	r0, r3
	movcc	r0, r3
	b	.L66
.L56:
	ldr	r0, [r4, #0]
	ldr	r3, .L70+20
	b	.L69
.L57:
	ldr	r0, [r4, #0]
	ldr	r3, .L70+24
	b	.L69
.L58:
	ldr	r0, [r4, #0]
	ldr	r3, .L70+28
	b	.L69
.L59:
	ldr	r0, [r4, #0]
	ldr	r3, .L70+32
	b	.L69
.L60:
	ldr	r2, .L70+36
	ldr	r3, .L70+40
	ldr	r2, [r2, #0]
	cmp	r2, #0
	moveq	r2, #23
	streq	r2, [r4, #0]
	ldr	r0, [r4, #0]
	b	.L69
.L61:
	ldr	r0, [r4, #0]
	ldr	r3, .L70+44
	b	.L69
.L62:
	ldr	r2, .L70+36
	ldr	r3, .L70+48
	ldr	r2, [r2, #0]
	cmp	r2, #0
	moveq	r2, #22
	streq	r2, [r4, #0]
	ldr	r0, [r4, #0]
	b	.L69
.L63:
	ldr	r0, [r4, #0]
	ldr	r3, .L70+52
	b	.L69
.L64:
	ldr	r0, [r4, #0]
	ldr	r3, .L70+56
	b	.L69
.L53:
	ldr	r0, [r4, #0]
.L66:
	mov	r1, #1
	ldmfd	sp!, {r4, r5, lr}
	b	FDTO_Cursor_Select
.L71:
	.align	2
.L70:
	.word	GuiVar_MenuScreenToShow
	.word	.LANCHOR11
	.word	.LANCHOR13
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	.LANCHOR4
	.word	.LANCHOR5
	.word	GuiVar_MainMenuStationsExist
	.word	.LANCHOR6
	.word	.LANCHOR7
	.word	.LANCHOR8
	.word	.LANCHOR9
	.word	.LANCHOR10
.LFE14:
	.size	FDTO_MAIN_MENU_jump_to_submenu, .-FDTO_MAIN_MENU_jump_to_submenu
	.section	.text.FDTO_MAIN_MENU_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_MAIN_MENU_draw_menu
	.type	FDTO_MAIN_MENU_draw_menu, %function
FDTO_MAIN_MENU_draw_menu:
.LFB17:
	@ args = 0, pretend = 0, frame = 112
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI7:
	ldr	r6, .L103
	sub	sp, sp, #112
.LCFI8:
	mov	r4, r0
	bl	STATION_get_num_stations_in_use
	ldr	r5, .L103+4
	adds	r0, r0, #0
	movne	r0, #1
	str	r0, [r6, #0]
	bl	POC_show_poc_menu_items
	str	r0, [r5, #0]
	bl	POC_at_least_one_POC_has_a_flow_meter
	ldr	r2, [r6, #0]
	ldr	r3, .L103+8
	cmp	r2, #0
	str	r0, [r3, #0]
	movne	r3, #1
	bne	.L73
	ldr	r3, [r5, #0]
	adds	r3, r3, #0
	movne	r3, #1
.L73:
	ldr	r1, .L103+12
	cmp	r2, #0
	str	r3, [r1, #0]
	ldr	r1, .L103+16
	movne	r3, #1
	str	r3, [r1, #0]
	bl	POC_at_least_one_POC_is_a_bypass_manifold
	ldr	r3, .L103+20
	ldr	r5, .L103+24
	mov	r6, #1
	str	r0, [r3, #0]
	bl	SYSTEM_at_least_one_system_has_flow_checking
	ldr	r3, .L103+28
	ldr	r2, .L103+32
	str	r0, [r3, #0]
	ldr	r3, .L103+36
	ldrb	r3, [r3, #52]	@ zero_extendqisi2
	and	r1, r3, #1
	str	r1, [r2, #0]
	ldr	r2, .L103+40
	mov	r3, r3, lsr #3
	and	r3, r3, #1
	str	r3, [r2, #0]
	ldr	r3, .L103+44
	ldr	r2, [r3, #0]
	ldr	r3, .L103+48
	str	r2, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L103+52
	mov	r2, #92
	ldr	r1, .L103+56
	mla	r3, r2, r0, r3
	ldr	ip, [r3, #16]
	cmp	ip, #0
	ldrne	r3, [r3, #80]
	streq	ip, [r1, #0]
	strne	r3, [r1, #0]
	ldr	r3, .L103+60
	str	r6, [r3, #0]
	mov	r3, #0
	str	r3, [r5, #0]
	bl	WEATHER_there_is_a_weather_option_in_the_chain
	cmp	r0, #0
	strne	r6, [r5, #0]
	bne	.L78
	ldr	r3, .L103+52
	mov	r1, #92
	mov	r2, r3
.L80:
	ldr	ip, [r3, #16]
	cmp	ip, #0
	beq	.L79
	mla	ip, r1, r0, r2
	ldrb	ip, [ip, #52]	@ zero_extendqisi2
	and	ip, ip, #3
	cmp	ip, #3
	ldreq	r3, .L103+24
	moveq	r2, #2
	streq	r2, [r3, #0]
	beq	.L78
.L79:
	add	r0, r0, #1
	cmp	r0, #12
	add	r3, r3, #92
	bne	.L80
.L78:
	ldr	r3, .L103+64
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r3, .L103+52
	mov	r0, #92
	mov	r1, r3
.L83:
	ldr	ip, [r3, #16]
	cmp	ip, #0
	beq	.L81
	mla	ip, r0, r2, r1
	ldrb	ip, [ip, #56]	@ zero_extendqisi2
	and	ip, ip, #3
	cmp	ip, #3
	ldreq	r3, .L103+64
	moveq	r2, #1
	streq	r2, [r3, #0]
	beq	.L82
.L81:
	add	r2, r2, #1
	cmp	r2, #12
	add	r3, r3, #92
	bne	.L83
.L82:
	bl	WEATHER_get_et_gage_is_in_use
	cmp	r0, #0
	bne	.L98
	bl	WEATHER_get_rain_bucket_is_in_use
	cmp	r0, #0
	bne	.L98
	bl	WEATHER_get_wind_gage_in_use
	cmp	r0, #0
	bne	.L98
	bl	WEATHER_get_freeze_switch_in_use
	cmp	r0, #0
	bne	.L98
	bl	WEATHER_get_rain_switch_in_use
	cmp	r0, #0
	bne	.L98
	bl	WEATHER_get_SW1_as_rain_switch_in_use
	adds	r0, r0, #0
	movne	r0, #1
	b	.L84
.L98:
	mov	r0, #1
.L84:
	ldr	r3, .L103+68
	mov	r5, #0
	str	r0, [r3, #0]
	bl	MOISTURE_SENSOR_get_num_of_moisture_sensors_connected
	ldr	r3, .L103+72
	adds	r0, r0, #0
	movne	r0, #1
	str	r0, [r3, #0]
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	ldr	r3, .L103+76
	str	r0, [r3, #0]
	bl	SYSTEM_at_least_one_system_has_budget_enabled
	ldr	r3, .L103+80
	str	r0, [r3, #0]
	ldr	r3, .L103+36
	ldrb	r2, [r3, #53]	@ zero_extendqisi2
	ldr	r3, .L103+84
	mov	r2, r2, lsr #4
	and	r2, r2, #1
	str	r2, [r3, #0]
	b	.L85
.L88:
.LBB6:
	mov	r0, r5
	bl	SYSTEM_get_group_at_this_index
	mov	r1, sp
	bl	SYSTEM_get_budget_details
	ldr	r3, [sp, #108]
	cmp	r3, #0
	ldrne	r2, .L103+88
	strne	r3, [r2, #0]
	bne	.L87
.L86:
.LBE6:
	add	r5, r5, #1
.L85:
	bl	SYSTEM_num_systems_in_use
	cmp	r5, r0
	bcc	.L88
.L87:
	cmp	r4, #0
	beq	.L89
	ldr	r3, .L103+92
	mvn	r2, #0
	str	r2, [r3, #0]
	ldr	r3, .L103+96
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldrne	r3, .L103+100
	ldreq	r3, .L103+104
	ldr	r5, [r3, #0]
	b	.L91
.L89:
	ldr	r3, .L103+108
	ldrsh	r5, [r3, #0]
	sub	r3, r5, #20
	cmp	r3, #6
	bhi	.L91
	mov	r0, r5
	bl	MAIN_MENU_store_prev_cursor_pos
.L91:
	mov	r1, r5, asl #16
	mov	r0, #36
	mov	r1, r1, asr #16
	mov	r2, #1
	bl	GuiLib_ShowScreen
	mov	r0, r4
	bl	FDTO_MAIN_MENU_refresh_menu
	add	sp, sp, #112
	ldmfd	sp!, {r4, r5, r6, pc}
.L104:
	.align	2
.L103:
	.word	GuiVar_MainMenuStationsExist
	.word	GuiVar_MainMenuPOCsExist
	.word	GuiVar_MainMenuFlowMetersExist
	.word	GuiVar_MainMenuMainLinesAvailable
	.word	GuiVar_MainMenuManualAvailable
	.word	GuiVar_MainMenuBypassExists
	.word	GuiVar_MainMenuWeatherOptionsExist
	.word	GuiVar_MainMenuFlowCheckingInUse
	.word	GuiVar_MainMenuFLOptionExists
	.word	config_c
	.word	GuiVar_MainMenuHubOptionExists
	.word	display_model_is
	.word	GuiVar_DisplayType
	.word	chain
	.word	GuiVar_MainMenu2WireOptionsExist
	.word	GuiVar_MainMenuCommOptionExists
	.word	GuiVar_MainMenuLightsOptionsExist
	.word	GuiVar_MainMenuWeatherDeviceExists
	.word	GuiVar_MainMenuMoisSensorExists
	.word	GuiVar_IsMaster
	.word	GuiVar_MainMenuBudgetsInUse
	.word	GuiVar_MainMenuAquaponicsMode
	.word	GuiVar_BudgetModeIdx
	.word	.LANCHOR11
	.word	GuiVar_StatusShowLiveScreens
	.word	g_STATUS_last_cursor_position
	.word	GuiVar_MenuScreenToShow
	.word	GuiLib_ActiveCursorFieldNo
.LFE17:
	.size	FDTO_MAIN_MENU_draw_menu, .-FDTO_MAIN_MENU_draw_menu
	.section	.text.FDTO_process_up_and_down_keys,"ax",%progbits
	.align	2
	.type	FDTO_process_up_and_down_keys, %function
FDTO_process_up_and_down_keys:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	str	lr, [sp, #-4]!
.LCFI9:
	bne	.L106
	ldr	r3, .L112
	ldrsh	r3, [r3, #0]
	cmp	r3, #11
	bne	.L107
	b	.L111
.L106:
	cmp	r0, #4
	ldrne	pc, [sp], #4
	ldr	r3, .L112
	ldrsh	r2, [r3, #0]
	ldr	r3, .L112+4
	ldr	r3, [r3, #0]
	cmp	r2, r3
	bne	.L109
.L111:
	ldr	lr, [sp], #4
	b	bad_key_beep
.L109:
	mov	r0, #1
	bl	FDTO_Cursor_Up
	cmp	r0, #0
	ldreq	pc, [sp], #4
	b	.L110
.L107:
	mov	r0, #1
	bl	FDTO_Cursor_Down
	cmp	r0, #0
	ldreq	pc, [sp], #4
.L110:
	ldr	r3, .L112
	ldrsh	r3, [r3, #0]
	cmp	r3, #11
	ldrgt	pc, [sp], #4
.LBB9:
	ldr	r2, .L112+8
	mov	r0, #0
	str	r3, [r2, #0]
	bl	FDTO_Redraw_Screen
	mov	r0, #0
.LBE9:
	ldr	lr, [sp], #4
.LBB10:
	b	FDTO_MAIN_MENU_refresh_menu
.L113:
	.align	2
.L112:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR13
	.word	GuiVar_MenuScreenToShow
.LBE10:
.LFE18:
	.size	FDTO_process_up_and_down_keys, .-FDTO_process_up_and_down_keys
	.section	.text.MAIN_MENU_process_menu,"ax",%progbits
	.align	2
	.global	MAIN_MENU_process_menu
	.type	MAIN_MENU_process_menu, %function
MAIN_MENU_process_menu:
.LFB19:
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L287
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI10:
	ldr	r3, [r3, #0]
	sub	sp, sp, #76
.LCFI11:
	cmp	r3, #0
	mov	r4, r0
	mov	r5, r1
	beq	.L115
	bl	STATUS_process_screen
	b	.L114
.L115:
	ldr	r3, .L287+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L117
	bl	COMM_OPTIONS_process_dialog
	b	.L114
.L117:
	ldr	r3, .L287+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L118
	bl	LIVE_SCREENS_process_dialog
	b	.L114
.L118:
	ldr	r3, .L287+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L119
	bl	TECH_SUPPORT_process_dialog
	b	.L114
.L119:
	ldr	r3, .L287+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L120
	bl	TWO_WIRE_ASSIGNMENT_process_dialog
	b	.L114
.L120:
	ldr	r3, .L287+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L121
	bl	TWO_WIRE_DEBUG_process_dialog
	b	.L114
.L121:
	cmp	r0, #4
	beq	.L123
	bhi	.L128
	cmp	r0, #1
	beq	.L124
	bhi	.L125
	b	.L123
.L128:
	cmp	r0, #80
	beq	.L127
	bhi	.L129
	cmp	r0, #67
	bne	.L122
	b	.L280
.L129:
	cmp	r0, #81
	beq	.L125
	cmp	r0, #84
	bne	.L122
.L127:
	bl	bad_key_beep
	b	.L114
.L123:
	mov	r3, #2
	str	r3, [sp, #4]
	ldr	r3, .L287+24
	add	r0, sp, #4
	str	r3, [sp, #24]
	str	r4, [sp, #28]
	bl	Display_Post_Command
	b	.L114
.L124:
	ldr	r4, .L287+28
	ldrsh	r3, [r4, #0]
	cmp	r3, #19
	ble	.L130
	bl	good_key_beep
	ldrsh	r0, [r4, #0]
	bl	MAIN_MENU_store_prev_cursor_pos
	bl	MAIN_MENU_return_from_sub_menu
	b	.L114
.L130:
	bl	bad_key_beep
	b	.L114
.L125:
	ldr	r3, .L287+32
	ldr	r3, [r3, #0]
	cmn	r3, #1
	bne	.L131
	cmp	r4, #81
	beq	.L132
	ldr	r3, .L287+28
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L134
	blt	.L133
	cmp	r3, #11
	bgt	.L133
	b	.L281
.L134:
	mov	r3, #1
	str	r3, [sp, #4]
	ldr	r3, .L287+36
	add	r0, sp, #4
	str	r3, [sp, #24]
	bl	Display_Post_Command
	mov	r0, r4
	mov	r1, r5
	bl	STATUS_process_screen
	b	.L114
.L281:
	mov	r3, #1
	str	r3, [sp, #4]
	ldr	r3, .L287+40
	add	r0, sp, #4
	str	r3, [sp, #24]
	bl	Display_Post_Command
	b	.L114
.L133:
	bl	bad_key_beep
	b	.L114
.L131:
	cmp	r4, #3
	bne	.L136
	bl	bad_key_beep
	b	.L114
.L136:
	sub	r3, r3, #1
	cmp	r3, #10
	ldrls	pc, [pc, r3, asl #2]
	b	.L132
.L148:
	.word	.L137
	.word	.L138
	.word	.L139
	.word	.L140
	.word	.L141
	.word	.L142
	.word	.L143
	.word	.L144
	.word	.L145
	.word	.L146
	.word	.L147
.L137:
.LBB31:
	ldr	r3, .L287+28
	ldr	r2, .L287+44
	ldrsh	r3, [r3, #0]
	cmp	r4, #2
	str	r3, [r2, #0]
	bne	.L265
	sub	r3, r3, #20
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L151
.L159:
	.word	.L152
	.word	.L153
	.word	.L154
	.word	.L155
	.word	.L156
	.word	.L157
	.word	.L158
.L152:
	ldr	r3, .L287+48
	mov	r0, #1
	str	r3, [sp, #0]
	mov	r1, #55
	ldr	r2, .L287+52
	ldr	r3, .L287+56
	bl	MAIN_MENU_change_screen
	b	.L114
.L153:
	ldr	r3, .L287+48
	mov	r0, #1
	str	r3, [sp, #0]
	mov	r1, #54
	ldr	r2, .L287+60
	ldr	r3, .L287+64
	bl	MAIN_MENU_change_screen
	b	.L114
.L154:
	ldr	r3, .L287+68
	mov	r0, #1
	str	r3, [sp, #0]
	mov	r1, #56
	ldr	r2, .L287+72
	ldr	r3, .L287+76
	bl	MAIN_MENU_change_screen
	b	.L114
.L155:
	ldr	r3, .L287+48
	mov	r0, #1
	str	r3, [sp, #0]
	mov	r1, #49
	ldr	r2, .L287+80
	ldr	r3, .L287+84
	bl	MAIN_MENU_change_screen
	b	.L114
.L156:
	ldr	r3, .L287+48
	mov	r0, #1
	str	r3, [sp, #0]
	mov	r1, #48
	ldr	r2, .L287+88
	ldr	r3, .L287+92
	bl	MAIN_MENU_change_screen
	b	.L114
.L157:
	ldr	r3, .L287+96
	mov	r0, #1
	str	r3, [sp, #0]
	mov	r1, #39
	ldr	r2, .L287+100
	ldr	r3, .L287+104
	bl	MAIN_MENU_change_screen
	b	.L114
.L158:
	ldr	r3, .L287+108
	ldr	r3, [r3, #0]
	cmp	r3, #4096
	bne	.L160
	bl	bad_key_beep
	b	.L114
.L160:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #1
	mov	r1, #79
	ldr	r2, .L287+112
	ldr	r3, .L287+116
	bl	MAIN_MENU_change_screen
	b	.L114
.L151:
	bl	bad_key_beep
	b	.L114
.L265:
	bl	bad_key_beep
	b	.L114
.L138:
.LBE31:
.LBB32:
	ldr	r3, .L287+28
	ldr	r2, .L287+120
	ldrsh	r3, [r3, #0]
	cmp	r4, #2
	str	r3, [r2, #0]
	bne	.L266
	sub	r3, r3, #20
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L163
.L169:
	.word	.L164
	.word	.L165
	.word	.L166
	.word	.L167
	.word	.L168
.L164:
	ldr	r3, .L287+124
	mov	r0, #2
	str	r3, [sp, #0]
	mov	r1, #47
	ldr	r2, .L287+128
	ldr	r3, .L287+132
	bl	MAIN_MENU_change_screen
	b	.L114
.L165:
	ldr	r3, .L287+48
	mov	r0, #2
	str	r3, [sp, #0]
	mov	r1, #50
	ldr	r2, .L287+136
	ldr	r3, .L287+140
	bl	MAIN_MENU_change_screen
	b	.L114
.L166:
	ldr	r3, .L287+48
	mov	r0, #2
	str	r3, [sp, #0]
	mov	r1, #33
	ldr	r2, .L287+144
	ldr	r3, .L287+148
	bl	MAIN_MENU_change_screen
	b	.L114
.L167:
	ldr	r3, .L287+48
	mov	r0, #2
	str	r3, [sp, #0]
	mov	r1, r0
	ldr	r2, .L287+152
	ldr	r3, .L287+156
	bl	MAIN_MENU_change_screen
	b	.L114
.L168:
	ldr	r3, .L287+48
	mov	r0, #2
	str	r3, [sp, #0]
	mov	r1, #16
	ldr	r2, .L287+160
	ldr	r3, .L287+164
	bl	MAIN_MENU_change_screen
	b	.L114
.L163:
	bl	bad_key_beep
	b	.L114
.L266:
	bl	bad_key_beep
	b	.L114
.L139:
.LBE32:
.LBB33:
	ldr	r3, .L287+28
	ldr	r2, .L287+168
	ldrsh	r3, [r3, #0]
	cmp	r4, #2
	str	r3, [r2, #0]
	bne	.L267
	sub	r3, r3, #20
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L172
.L180:
	.word	.L173
	.word	.L174
	.word	.L175
	.word	.L176
	.word	.L177
	.word	.L178
	.word	.L179
.L173:
	ldr	r3, .L287+172
	mov	r0, #3
	str	r3, [sp, #0]
	mov	r1, #35
	ldr	r2, .L287+176
	ldr	r3, .L287+180
	bl	MAIN_MENU_change_screen
	b	.L114
.L174:
	ldr	r3, .L287+172
	mov	r0, #3
	str	r3, [sp, #0]
	mov	r1, #38
	ldr	r2, .L287+184
	ldr	r3, .L287+188
	bl	MAIN_MENU_change_screen
	b	.L114
.L175:
	ldr	r3, .L287+172
	mov	r0, #3
	str	r3, [sp, #0]
	mov	r1, #37
	ldr	r2, .L287+192
	ldr	r3, .L287+196
	bl	MAIN_MENU_change_screen
	b	.L114
.L176:
	ldr	r3, .L287+48
	mov	r0, #3
	str	r3, [sp, #0]
	mov	r1, #46
	ldr	r2, .L287+200
	ldr	r3, .L287+204
	bl	MAIN_MENU_change_screen
	b	.L114
.L177:
	ldr	r3, .L287+172
	mov	r0, #3
	str	r3, [sp, #0]
	mov	r1, #21
	ldr	r2, .L287+208
	ldr	r3, .L287+212
	bl	MAIN_MENU_change_screen
	b	.L114
.L178:
	ldr	r3, .L287+48
	mov	r0, #3
	str	r3, [sp, #0]
	mov	r1, #8
	ldr	r2, .L287+216
	ldr	r3, .L287+220
	bl	MAIN_MENU_change_screen
	b	.L114
.L179:
	ldr	r3, .L287+172
	mov	r0, #3
	str	r3, [sp, #0]
	mov	r1, #34
	ldr	r2, .L287+224
	ldr	r3, .L287+228
	bl	MAIN_MENU_change_screen
	b	.L114
.L172:
	bl	bad_key_beep
	b	.L114
.L267:
	bl	bad_key_beep
	b	.L114
.L140:
.LBE33:
.LBB34:
	ldr	r3, .L287+28
	ldr	r2, .L287+232
	ldrsh	r3, [r3, #0]
	cmp	r4, #2
	str	r3, [r2, #0]
	bne	.L268
	sub	r3, r3, #20
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L183
.L190:
	.word	.L184
	.word	.L185
	.word	.L186
	.word	.L187
	.word	.L188
	.word	.L189
.L184:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #4
	mov	r1, #27
	ldr	r2, .L287+236
	ldr	r3, .L287+240
	bl	MAIN_MENU_change_screen
	b	.L114
.L185:
	ldr	r3, .L287+48
	mov	r0, #4
	str	r3, [sp, #0]
	mov	r1, #73
	ldr	r2, .L287+244
	ldr	r3, .L287+248
	bl	MAIN_MENU_change_screen
	b	.L114
.L186:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #4
	mov	r1, #72
	ldr	r2, .L287+252
	ldr	r3, .L287+256
	bl	MAIN_MENU_change_screen
	b	.L114
.L187:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #4
	mov	r1, #52
	ldr	r2, .L287+260
	ldr	r3, .L287+264
	bl	MAIN_MENU_change_screen
	b	.L114
.L188:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #4
	mov	r1, #22
	ldr	r2, .L287+268
	ldr	r3, .L287+272
	bl	MAIN_MENU_change_screen
	b	.L114
.L189:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #4
	mov	r1, #44
	ldr	r2, .L287+276
	ldr	r3, .L287+280
	bl	MAIN_MENU_change_screen
	b	.L114
.L183:
	bl	bad_key_beep
	b	.L114
.L268:
	bl	bad_key_beep
	b	.L114
.L141:
.LBE34:
.LBB35:
	ldr	r3, .L287+28
	ldr	r2, .L287+284
	ldrsh	r3, [r3, #0]
	cmp	r4, #2
	str	r3, [r2, #0]
	bne	.L269
	sub	r3, r3, #20
	cmp	r3, #4
	ldrls	pc, [pc, r3, asl #2]
	b	.L193
.L199:
	.word	.L194
	.word	.L195
	.word	.L196
	.word	.L197
	.word	.L198
.L194:
	ldr	r3, .L287+172
	mov	r0, #5
	str	r3, [sp, #0]
	mov	r1, #12
	ldr	r2, .L287+288
	ldr	r3, .L287+292
	bl	MAIN_MENU_change_screen
	b	.L114
.L195:
	ldr	r3, .L287+124
	mov	r0, #5
	str	r3, [sp, #0]
	mov	r1, #9
	ldr	r2, .L287+296
	ldr	r3, .L287+300
	bl	MAIN_MENU_change_screen
	b	.L114
.L196:
	ldr	r3, .L287+48
	mov	r0, #5
	str	r3, [sp, #0]
	mov	r1, #11
	ldr	r2, .L287+304
	ldr	r3, .L287+308
	bl	MAIN_MENU_change_screen
	b	.L114
.L197:
	ldr	r3, .L287+172
	mov	r0, #5
	str	r3, [sp, #0]
	mov	r1, #10
	ldr	r2, .L287+312
	ldr	r3, .L287+316
	bl	MAIN_MENU_change_screen
	b	.L114
.L198:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #5
	mov	r1, #75
	ldr	r2, .L287+320
	ldr	r3, .L287+324
	bl	MAIN_MENU_change_screen
	b	.L114
.L193:
	bl	bad_key_beep
	b	.L114
.L269:
	bl	bad_key_beep
	b	.L114
.L142:
.LBE35:
.LBB36:
	ldr	r3, .L287+28
	ldr	r2, .L287+328
	ldrsh	r3, [r3, #0]
	cmp	r4, #2
	str	r3, [r2, #0]
	bne	.L270
	cmp	r3, #20
	beq	.L203
	cmp	r3, #21
	bne	.L271
	b	.L282
.L203:
	bl	good_key_beep
	ldr	r3, .L287+332
	mov	r0, #6
	str	r3, [sp, #0]
	mov	r1, #31
	ldr	r2, .L287+336
	ldr	r3, .L287+340
	bl	MAIN_MENU_change_screen
	b	.L114
.L282:
	ldr	r3, .L287+332
	mov	r0, #6
	str	r3, [sp, #0]
	mov	r1, #32
	ldr	r2, .L287+344
	ldr	r3, .L287+348
	bl	MAIN_MENU_change_screen
	b	.L114
.L271:
	bl	bad_key_beep
	b	.L114
.L270:
	bl	bad_key_beep
	b	.L114
.L143:
.LBE36:
.LBB37:
	ldr	r3, .L287+28
	ldr	r2, .L287+352
	ldrsh	r3, [r3, #0]
	cmp	r4, #2
	str	r3, [r2, #0]
	beq	.L206
	cmp	r4, #81
	bne	.L272
	b	.L283
.L206:
	sub	r3, r3, #20
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L208
.L215:
	.word	.L209
	.word	.L210
	.word	.L211
	.word	.L212
	.word	.L213
	.word	.L214
.L209:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #7
	mov	r1, #58
	ldr	r2, .L287+356
	ldr	r3, .L287+360
	bl	MAIN_MENU_change_screen
	b	.L114
.L210:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #7
	mov	r1, #42
	ldr	r2, .L287+364
	ldr	r3, .L287+368
	bl	MAIN_MENU_change_screen
	b	.L114
.L211:
	ldr	r3, .L287+96
	mov	r0, #7
	str	r3, [sp, #0]
	mov	r1, #39
	ldr	r2, .L287+100
	ldr	r3, .L287+104
	bl	MAIN_MENU_change_screen
	b	.L114
.L212:
	ldr	r3, .L287+172
	mov	r0, #7
	str	r3, [sp, #0]
	mov	r1, #34
	ldr	r2, .L287+224
	ldr	r3, .L287+228
	bl	MAIN_MENU_change_screen
	b	.L114
.L213:
	ldr	r3, .L287+332
	mov	r0, #7
	str	r3, [sp, #0]
	mov	r1, #32
	ldr	r2, .L287+344
	ldr	r3, .L287+348
	bl	MAIN_MENU_change_screen
	b	.L114
.L214:
	ldr	r3, .L287+372
	mov	r0, #7
	str	r3, [sp, #0]
	mov	r1, #71
	ldr	r2, .L287+376
	ldr	r3, .L287+380
	bl	MAIN_MENU_change_screen
	b	.L114
.L208:
	bl	bad_key_beep
	b	.L114
.L283:
	cmp	r3, #20
	bne	.L273
	ldr	r3, .L287+384
	ldr	r2, .L287+388
	ldr	r3, [r3, #0]
	add	r3, r3, r3, asl #2
	cmp	r3, r2
	bhi	.L218
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	bne	.L219
	str	r0, [sp, #0]
	mov	r1, #59
	mov	r0, #7
	ldr	r2, .L287+392
	ldr	r3, .L287+396
	bl	MAIN_MENU_change_screen
	b	.L114
.L219:
	bl	bad_key_beep
	b	.L114
.L273:
	bl	bad_key_beep
	b	.L114
.L272:
	bl	bad_key_beep
	b	.L114
.L144:
.LBE37:
.LBB38:
	ldr	r3, .L287+28
	ldr	r2, .L287+400
	ldrsh	r3, [r3, #0]
	cmp	r4, #2
	str	r3, [r2, #0]
	bne	.L274
	cmp	r3, #20
	beq	.L223
	cmp	r3, #21
	bne	.L275
	b	.L284
.L223:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #8
	mov	r1, #45
	ldr	r2, .L287+404
	ldr	r3, .L287+408
	bl	MAIN_MENU_change_screen
	b	.L114
.L284:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #8
	mov	r1, #61
	ldr	r2, .L287+412
	ldr	r3, .L287+416
	bl	MAIN_MENU_change_screen
	b	.L114
.L275:
	bl	bad_key_beep
	b	.L114
.L274:
	bl	bad_key_beep
	b	.L114
.L145:
.LBE38:
.LBB39:
	ldr	r3, .L287+28
	ldr	r2, .L287+420
	ldrsh	r3, [r3, #0]
	cmp	r4, #2
	str	r3, [r2, #0]
	bne	.L276
	sub	r3, r3, #20
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L227
.L235:
	.word	.L228
	.word	.L229
	.word	.L230
	.word	.L231
	.word	.L232
	.word	.L233
	.word	.L234
.L228:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #9
	mov	r1, #100
	ldr	r2, .L287+424
	ldr	r3, .L287+428
	bl	MAIN_MENU_change_screen
	b	.L114
.L229:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #9
	mov	r1, #101
	ldr	r2, .L287+432
	ldr	r3, .L287+436
	bl	MAIN_MENU_change_screen
	b	.L114
.L230:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #9
	mov	r1, #91
	ldr	r2, .L287+440
	ldr	r3, .L287+444
	bl	MAIN_MENU_change_screen
	b	.L114
.L231:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #9
	mov	r1, #103
	ldr	r2, .L287+448
	ldr	r3, .L287+452
	bl	MAIN_MENU_change_screen
	b	.L114
.L232:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #9
	mov	r1, #84
	ldr	r2, .L287+456
	ldr	r3, .L287+460
	bl	MAIN_MENU_change_screen
	b	.L114
.L233:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #9
	mov	r1, #77
	ldr	r2, .L287+464
	ldr	r3, .L287+468
	bl	MAIN_MENU_change_screen
	b	.L114
.L234:
	ldr	r3, .L287+332
	mov	r0, #9
	str	r3, [sp, #0]
	mov	r1, #86
	ldr	r2, .L287+472
	ldr	r3, .L287+476
	bl	MAIN_MENU_change_screen
	b	.L114
.L227:
	bl	bad_key_beep
	b	.L114
.L276:
	bl	bad_key_beep
	b	.L114
.L146:
.LBE39:
.LBB40:
	ldr	r3, .L287+28
	ldr	r2, .L287+480
	ldrsh	r3, [r3, #0]
	cmp	r4, #2
	str	r3, [r2, #0]
	beq	.L237
	cmp	r4, #81
	bne	.L277
	b	.L285
.L237:
	sub	r2, r3, #20
	cmp	r2, #4
	ldrls	pc, [pc, r2, asl #2]
	b	.L114
.L243:
	.word	.L239
	.word	.L239
	.word	.L240
	.word	.L241
	.word	.L242
.L239:
	ldr	r2, .L287+484
	subs	r3, r3, #20
	movne	r3, #1
	str	r3, [r2, #0]
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #10
	mov	r1, #74
	ldr	r2, .L287+488
	ldr	r3, .L287+492
	bl	MAIN_MENU_change_screen
	b	.L114
.L240:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #10
	mov	r1, #85
	ldr	r2, .L287+496
	ldr	r3, .L287+500
	bl	MAIN_MENU_change_screen
	b	.L114
.L241:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #10
	mov	r1, #82
	ldr	r2, .L287+504
	ldr	r3, .L287+508
	bl	MAIN_MENU_change_screen
	b	.L114
.L242:
	bl	good_key_beep
	mov	r0, #1
	bl	LIVE_SCREENS_draw_dialog
	b	.L114
.L285:
	sub	r2, r3, #20
	cmp	r2, #1
	bhi	.L244
	ldr	r2, .L287+484
	cmp	r3, #20
	movne	r3, #2
	moveq	r3, #3
	str	r3, [r2, #0]
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #10
	mov	r1, #74
	ldr	r2, .L287+488
	ldr	r3, .L287+492
	bl	MAIN_MENU_change_screen
	b	.L114
.L244:
	bl	bad_key_beep
	b	.L114
.L277:
	bl	bad_key_beep
	b	.L114
.L147:
.LBE40:
.LBB41:
	ldr	r3, .L287+28
	ldr	r2, .L287+512
	ldrsh	r3, [r3, #0]
	cmp	r4, #2
	str	r3, [r2, #0]
	beq	.L248
	cmp	r4, #81
	bne	.L278
	b	.L286
.L248:
	sub	r3, r3, #20
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L250
.L258:
	.word	.L251
	.word	.L252
	.word	.L253
	.word	.L254
	.word	.L255
	.word	.L256
	.word	.L257
.L251:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #11
	mov	r1, #67
	ldr	r2, .L287+516
	ldr	r3, .L287+520
	bl	MAIN_MENU_change_screen
	b	.L114
.L252:
	bl	good_key_beep
	mov	r3, #2
	str	r3, [sp, #40]
	ldr	r3, .L287+524
	add	r0, sp, #40
	str	r3, [sp, #60]
	mov	r3, #1
	str	r3, [sp, #64]
	bl	Display_Post_Command
	b	.L114
.L253:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #11
	mov	r1, #19
	ldr	r2, .L287+528
	ldr	r3, .L287+532
	bl	MAIN_MENU_change_screen
	b	.L114
.L254:
	bl	good_key_beep
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	b	.L114
.L255:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #11
	mov	r1, #14
	ldr	r2, .L287+536
	ldr	r3, .L287+540
	bl	MAIN_MENU_change_screen
	b	.L114
.L256:
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r0, #11
	mov	r1, #15
	ldr	r2, .L287+544
	ldr	r3, .L287+548
	bl	MAIN_MENU_change_screen
	b	.L114
.L257:
	mov	r1, #0
	mov	r0, #11
	ldr	r2, .L287+552
	ldr	r3, .L287+556
	str	r1, [sp, #0]
	bl	MAIN_MENU_change_screen
	b	.L114
.L250:
	bl	bad_key_beep
	b	.L114
.L286:
	cmp	r3, #26
	bne	.L279
	bl	good_key_beep
	mov	r0, #1
	bl	TECH_SUPPORT_draw_dialog
	b	.L114
.L279:
	bl	bad_key_beep
	b	.L114
.L278:
	bl	bad_key_beep
	b	.L114
.L132:
.LBE41:
	bl	bad_key_beep
	b	.L114
.L280:
	ldr	r6, .L287+28
	ldrsh	r3, [r6, #0]
	cmp	r3, #11
	ble	.L261
	bl	good_key_beep
	ldrsh	r0, [r6, #0]
	bl	MAIN_MENU_store_prev_cursor_pos
	bl	MAIN_MENU_return_from_sub_menu
	b	.L114
.L261:
	ldr	r3, .L287+560
	ldr	r2, [r3, #0]
	cmp	r2, #0
	beq	.L262
	mov	r2, #0
	str	r2, [r3, #0]
	bl	good_key_beep
	mov	r3, #2
	str	r3, [sp, #4]
	ldr	r3, .L287+564
	add	r0, sp, #4
	str	r3, [sp, #24]
	mov	r3, #1
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L114
.L262:
	bl	KEY_process_global_keys
	b	.L114
.L122:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
	b	.L114
.L218:
.LBB42:
	bl	bad_key_beep
.L114:
.LBE42:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, pc}
.L288:
	.align	2
.L287:
	.word	GuiVar_StatusShowLiveScreens
	.word	g_COMM_OPTIONS_dialog_visible
	.word	g_LIVE_SCREENS_dialog_visible
	.word	g_TECH_SUPPORT_dialog_visible
	.word	g_TWO_WIRE_dialog_visible
	.word	g_TWO_WIRE_DEBUG_dialog_visible
	.word	FDTO_process_up_and_down_keys
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR11
	.word	FDTO_MAIN_MENU_show_screen_name_in_title_bar
	.word	FDTO_MAIN_MENU_jump_to_submenu
	.word	.LANCHOR0
	.word	nm_STATION_GROUP_load_group_name_into_guivar
	.word	FDTO_STATION_GROUP_draw_menu
	.word	STATION_GROUP_process_menu
	.word	FDTO_SCHEDULE_draw_menu
	.word	SCHEDULE_process_menu
	.word	nm_STATION_load_station_number_into_guivar
	.word	FDTO_STATION_draw_menu
	.word	STATION_process_menu
	.word	FDTO_PRIORITY_draw_screen
	.word	PRIORITY_process_screen
	.word	FDTO_PERCENT_ADJUST_draw_screen
	.word	PERCENT_ADJUST_process_screen
	.word	MANUAL_PROGRAMS_load_group_name_into_guivar
	.word	FDTO_MANUAL_PROGRAMS_draw_menu
	.word	MANUAL_PROGRAMS_process_menu
	.word	ftcs
	.word	FDTO_FINISH_TIMES_draw_screen
	.word	FINISH_TIMES_process_screen
	.word	.LANCHOR1
	.word	nm_POC_load_group_name_into_guivar
	.word	FDTO_POC_draw_menu
	.word	POC_process_menu
	.word	FDTO_PUMP_draw_screen
	.word	PUMP_process_screen
	.word	FDTO_LINE_FILL_TIME_draw_screen
	.word	LINE_FILL_TIME_process_screen
	.word	FDTO_ACQUIRE_EXPECTEDS_draw_screen
	.word	ACQUIRE_EXPECTEDS_process_screen
	.word	FDTO_DELAY_BETWEEN_VALVES_draw_screen
	.word	DELAY_BETWEEN_VALVES_process_screen
	.word	.LANCHOR2
	.word	SYSTEM_load_system_name_into_scroll_box_guivar
	.word	FDTO_SYSTEM_draw_menu
	.word	SYSTEM_process_menu
	.word	FDTO_SYSTEM_CAPACITY_draw_screen
	.word	SYSTEM_CAPACITY_process_screen
	.word	FDTO_MAINLINE_BREAK_draw_screen
	.word	MAINLINE_BREAK_process_screen
	.word	FDTO_ON_AT_A_TIME_draw_screen
	.word	ON_AT_A_TIME_process_screen
	.word	FDTO_FLOW_CHECKING_draw_menu
	.word	FLOW_CHECKING_process_menu
	.word	FDTO_ALERT_ACTIONS_draw_screen
	.word	ALERT_ACTIONS_process_screen
	.word	FDTO_MVOR_draw_menu
	.word	MVOR_process_menu
	.word	.LANCHOR3
	.word	FDTO_HISTORICAL_ET_draw_screen
	.word	HISTORICAL_ET_process_screen
	.word	FDTO_WEATHER_draw_screen
	.word	WEATHER_process_screen
	.word	FDTO_WEATHER_SENSORS_draw_screen
	.word	WEATHER_SENSORS_process_screen
	.word	FDTO_RAIN_SWITCH_draw_screen
	.word	RAIN_SWITCH_process_screen
	.word	FDTO_FREEZE_SWITCH_draw_screen
	.word	FREEZE_SWITCH_process_screen
	.word	FDTO_MOISTURE_SENSOR_draw_menu
	.word	MOISTURE_SENSOR_process_menu
	.word	.LANCHOR4
	.word	FDTO_BUDGET_SETUP_draw_menu
	.word	BUDGET_SETUP_process_menu
	.word	FDTO_BUDGETS_draw_menu
	.word	BUDGETS_process_menu
	.word	FDTO_BUDGET_REDUCTION_LIMITS_draw_screen
	.word	BUDGET_REDUCTION_LIMIT_process_screen
	.word	FDTO_BUDGET_FLOW_TYPES_draw_menu
	.word	BUDGET_FLOW_TYPES_process_menu
	.word	FDTO_BUDGET_REPORT_draw_report
	.word	BUDGET_REPORT_process_menu
	.word	.LANCHOR5
	.word	LIGHTS_load_group_name_into_guivar
	.word	FDTO_LIGHTS_draw_menu
	.word	LIGHTS_process_menu
	.word	FDTO_LIGHTS_TEST_draw_screen
	.word	LIGHTS_TEST_process_screen
	.word	.LANCHOR6
	.word	FDTO_TEST_draw_screen
	.word	TEST_process_screen
	.word	FDTO_MANUAL_WATER_draw_screen
	.word	MANUAL_WATER_process_screen
	.word	WALK_THRU_load_group_name_into_guivar
	.word	WALK_THRU_draw_menu
	.word	WALK_THRU_process_menu
	.word	my_tick_count
	.word	9999
	.word	FDTO_TEST_SEQ_draw_screen
	.word	TEST_SEQ_process_screen
	.word	.LANCHOR7
	.word	FDTO_NOW_draw_screen
	.word	NOW_process_screen
	.word	FDTO_TURN_OFF_draw_screen
	.word	TURN_OFF_process_screen
	.word	.LANCHOR8
	.word	FDTO_STATION_HISTORY_draw_report
	.word	STATION_HISTORY_process_report
	.word	FDTO_STATION_REPORT_draw_report
	.word	STATION_REPORT_process_report
	.word	FDTO_POC_USAGE_draw_report
	.word	POC_USAGE_process_report
	.word	FDTO_SYSTEM_REPORT_draw_report
	.word	SYSTEM_REPORT_process_report
	.word	FDTO_HOLDOVER_draw_report
	.word	HOLDOVER_process_report
	.word	FDTO_ET_RAIN_TABLE_draw_report
	.word	ET_RAIN_TABLE_process_report
	.word	FDTO_LIGHTS_REPORT_draw_report
	.word	LIGHTS_REPORT_process_report
	.word	.LANCHOR9
	.word	g_ALERTS_pile_to_show
	.word	FDTO_ALERTS_draw_alerts
	.word	ALERTS_process_report
	.word	FDTO_IRRI_DETAILS_draw_report
	.word	IRRI_DETAILS_process_report
	.word	FDTO_FLOW_RECORDING_draw_report
	.word	FLOW_RECORDING_process_report
	.word	.LANCHOR10
	.word	FDTO_STATIONS_IN_USE_draw_screen
	.word	STATIONS_IN_USE_process_screen
	.word	FDTO_TWO_WIRE_ASSIGNMENT_draw_dialog
	.word	FDTO_FLOWSENSE_draw_screen
	.word	FLOWSENSE_process_screen
	.word	FDTO_LCD_draw_backlight_contrast_and_volume
	.word	LCD_process_backlight_contrast_and_volume
	.word	FDTO_TIME_DATE_draw_screen
	.word	TIME_DATE_process_screen
	.word	FDTO_ABOUT_draw_screen
	.word	ABOUT_process_screen
	.word	GuiVar_MenuScreenToShow
	.word	FDTO_MAIN_MENU_draw_menu
.LFE19:
	.size	MAIN_MENU_process_menu, .-MAIN_MENU_process_menu
	.global	g_MAIN_MENU_active_menu_item
	.section	.bss.g_MAIN_MENU_prev_cursor_pos__weather,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_MAIN_MENU_prev_cursor_pos__weather, %object
	.size	g_MAIN_MENU_prev_cursor_pos__weather, 4
g_MAIN_MENU_prev_cursor_pos__weather:
	.space	4
	.section	.bss.g_MAIN_MENU_prev_cursor_pos__manual,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	g_MAIN_MENU_prev_cursor_pos__manual, %object
	.size	g_MAIN_MENU_prev_cursor_pos__manual, 4
g_MAIN_MENU_prev_cursor_pos__manual:
	.space	4
	.section	.bss.mm_top_cursor_pos_sub_menu,"aw",%nobits
	.align	2
	.set	.LANCHOR13,. + 0
	.type	mm_top_cursor_pos_sub_menu, %object
	.size	mm_top_cursor_pos_sub_menu, 4
mm_top_cursor_pos_sub_menu:
	.space	4
	.section	.bss.g_MAIN_MENU_prev_cursor_pos__lights,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	g_MAIN_MENU_prev_cursor_pos__lights, %object
	.size	g_MAIN_MENU_prev_cursor_pos__lights, 4
g_MAIN_MENU_prev_cursor_pos__lights:
	.space	4
	.section	.bss.g_MAIN_MENU_prev_cursor_pos__usage_reports,"aw",%nobits
	.align	2
	.set	.LANCHOR8,. + 0
	.type	g_MAIN_MENU_prev_cursor_pos__usage_reports, %object
	.size	g_MAIN_MENU_prev_cursor_pos__usage_reports, 4
g_MAIN_MENU_prev_cursor_pos__usage_reports:
	.space	4
	.section	.rodata.CSWTCH.96,"a",%progbits
	.align	1
	.set	.LANCHOR12,. + 0
	.type	CSWTCH.96, %object
	.size	CSWTCH.96, 24
CSWTCH.96:
	.short	1071
	.short	1045
	.short	1012
	.short	977
	.short	1109
	.short	888
	.short	959
	.short	979
	.short	998
	.short	1038
	.short	911
	.short	1051
	.section	.bss.g_MAIN_MENU_prev_cursor_pos__poc_flow,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_MAIN_MENU_prev_cursor_pos__poc_flow, %object
	.size	g_MAIN_MENU_prev_cursor_pos__poc_flow, 4
g_MAIN_MENU_prev_cursor_pos__poc_flow:
	.space	4
	.section	.bss.g_MAIN_MENU_prev_cursor_pos__system_flow,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_MAIN_MENU_prev_cursor_pos__system_flow, %object
	.size	g_MAIN_MENU_prev_cursor_pos__system_flow, 4
g_MAIN_MENU_prev_cursor_pos__system_flow:
	.space	4
	.section	.bss.g_MAIN_MENU_prev_cursor_pos__setup,"aw",%nobits
	.align	2
	.set	.LANCHOR10,. + 0
	.type	g_MAIN_MENU_prev_cursor_pos__setup, %object
	.size	g_MAIN_MENU_prev_cursor_pos__setup, 4
g_MAIN_MENU_prev_cursor_pos__setup:
	.space	4
	.section	.bss.g_MAIN_MENU_active_menu_item,"aw",%nobits
	.align	2
	.set	.LANCHOR11,. + 0
	.type	g_MAIN_MENU_active_menu_item, %object
	.size	g_MAIN_MENU_active_menu_item, 4
g_MAIN_MENU_active_menu_item:
	.space	4
	.section	.bss.g_MAIN_MENU_prev_cursor_pos__budgets,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_MAIN_MENU_prev_cursor_pos__budgets, %object
	.size	g_MAIN_MENU_prev_cursor_pos__budgets, 4
g_MAIN_MENU_prev_cursor_pos__budgets:
	.space	4
	.section	.bss.g_MAIN_MENU_prev_cursor_pos__schedule,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_MAIN_MENU_prev_cursor_pos__schedule, %object
	.size	g_MAIN_MENU_prev_cursor_pos__schedule, 4
g_MAIN_MENU_prev_cursor_pos__schedule:
	.space	4
	.section	.bss.g_MAIN_MENU_prev_cursor_pos__no_water_days,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	g_MAIN_MENU_prev_cursor_pos__no_water_days, %object
	.size	g_MAIN_MENU_prev_cursor_pos__no_water_days, 4
g_MAIN_MENU_prev_cursor_pos__no_water_days:
	.space	4
	.section	.bss.g_MAIN_MENU_prev_cursor_pos__diagnostics,"aw",%nobits
	.align	2
	.set	.LANCHOR9,. + 0
	.type	g_MAIN_MENU_prev_cursor_pos__diagnostics, %object
	.size	g_MAIN_MENU_prev_cursor_pos__diagnostics, 4
g_MAIN_MENU_prev_cursor_pos__diagnostics:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI2-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI4-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI5-.LFB13
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI6-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI7-.LFB17
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x80
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI9-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI10-.LFB19
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/m_main.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x13b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF20
	.byte	0x1
	.4byte	.LASF21
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x653
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x287
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x33a
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x544
	.4byte	.LFB16
	.4byte	.LFE16
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x10e
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x533
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x1
	.byte	0xe2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x491
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x4df
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x577
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST5
	.uleb128 0x7
	.4byte	0x21
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST6
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x125
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x17b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1b8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x205
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x24a
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x2ae
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x35f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x3ac
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x418
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x672
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST7
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB15
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI3
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB0
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB13
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB14
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB17
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI8
	.4byte	.LFE17
	.2byte	0x3
	.byte	0x7d
	.sleb128 128
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB18
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB19
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI11
	.4byte	.LFE19
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF9:
	.ascii	"MAIN_MENU_process_scheduled_irrigation_menu\000"
.LASF5:
	.ascii	"MAIN_MENU_return_from_sub_menu\000"
.LASF21:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/m_main.c\000"
.LASF11:
	.ascii	"MAIN_MENU_process_mainline_menu\000"
.LASF0:
	.ascii	"FDTO_process_up_and_down_keys\000"
.LASF4:
	.ascii	"MAIN_MENU_change_screen\000"
.LASF6:
	.ascii	"FDTO_MAIN_MENU_show_screen_name_in_title_bar\000"
.LASF20:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF17:
	.ascii	"MAIN_MENU_process_setup_menu\000"
.LASF7:
	.ascii	"FDTO_MAIN_MENU_refresh_menu\000"
.LASF2:
	.ascii	"MAIN_MENU_process_no_water_days_menu\000"
.LASF13:
	.ascii	"MAIN_MENU_process_budgets_menu\000"
.LASF3:
	.ascii	"MAIN_MENU_store_prev_cursor_pos\000"
.LASF16:
	.ascii	"MAIN_MENU_process_diagnostics_menu\000"
.LASF14:
	.ascii	"MAIN_MENU_process_manual_menu\000"
.LASF15:
	.ascii	"MAIN_MENU_process_reports_menu\000"
.LASF10:
	.ascii	"MAIN_MENU_process_poc_menu\000"
.LASF12:
	.ascii	"MAIN_MENU_process_weather_menu\000"
.LASF1:
	.ascii	"MAIN_MENU_process_lights_menu\000"
.LASF19:
	.ascii	"MAIN_MENU_process_menu\000"
.LASF18:
	.ascii	"FDTO_MAIN_MENU_draw_menu\000"
.LASF8:
	.ascii	"FDTO_MAIN_MENU_jump_to_submenu\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
