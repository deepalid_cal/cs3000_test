	.file	"moisture_sensor_recorder.c"
	.text
.Ltext0:
	.section	.text.ci_moisture_sensor_recorder_timer_callback,"ax",%progbits
	.align	2
	.global	ci_moisture_sensor_recorder_timer_callback
	.type	ci_moisture_sensor_recorder_timer_callback, %function
ci_moisture_sensor_recorder_timer_callback:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L2
	mov	r1, #0
	mov	r2, #512
	mov	r3, r1
	b	CONTROLLER_INITIATED_post_to_messages_queue
.L3:
	.align	2
.L2:
	.word	410
.LFE0:
	.size	ci_moisture_sensor_recorder_timer_callback, .-ci_moisture_sensor_recorder_timer_callback
	.section	.text.nm_MOISTURE_SENSOR_RECORDER_inc_pointer,"ax",%progbits
	.align	2
	.global	nm_MOISTURE_SENSOR_RECORDER_inc_pointer
	.type	nm_MOISTURE_SENSOR_RECORDER_inc_pointer, %function
nm_MOISTURE_SENSOR_RECORDER_inc_pointer:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #0]
	add	r2, r1, #8192
	add	r3, r3, #24
	add	r2, r2, #16
	cmp	r2, r3
	movls	r3, r1
	str	r3, [r0, #0]
	bx	lr
.LFE2:
	.size	nm_MOISTURE_SENSOR_RECORDER_inc_pointer, .-nm_MOISTURE_SENSOR_RECORDER_inc_pointer
	.section	.text.MOISTURE_SENSOR_RECORDER_add_a_record,"ax",%progbits
	.align	2
	.global	MOISTURE_SENSOR_RECORDER_add_a_record
	.type	MOISTURE_SENSOR_RECORDER_add_a_record, %function
MOISTURE_SENSOR_RECORDER_add_a_record:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L13
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI0:
.LBB4:
	ldr	r4, .L13+4
.LBE4:
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L13+8
	mov	r3, #115
	bl	xQueueTakeMutexRecursive_debug
.LBB5:
	ldr	r6, [r4, #0]
	cmp	r6, #0
	bne	.L8
	ldr	r1, .L13+8
	mov	r2, #57
	ldr	r0, .L13+12
	bl	mem_malloc_debug
	ldr	r3, .L13+16
	ldr	r1, .L13+20
	str	r3, [sp, #0]
	mov	r2, r6
	mov	r3, r6
	str	r6, [r4, #20]
	str	r0, [r4, #0]
	str	r0, [r4, #4]
	str	r0, [r4, #8]
	str	r0, [r4, #12]
	str	r0, [r4, #16]
	ldr	r0, .L13+24
	bl	xTimerCreate
	cmp	r0, #0
	str	r0, [r4, #24]
	bne	.L8
	ldr	r0, .L13+8
	bl	RemovePathFromFileName
	mov	r2, #80
	mov	r1, r0
	ldr	r0, .L13+28
	bl	Alert_Message_va
.L8:
.LBE5:
	ldr	r4, .L13+4
	mov	r0, r5
	ldr	r6, [r4, #4]
	mov	r1, r6
	bl	MOISTURE_SENSOR_fill_out_recorder_record
	mov	r0, r6
	bl	EPSON_obtain_latest_time_and_date
	add	r0, r4, #4
	ldr	r1, [r4, #0]
	bl	nm_MOISTURE_SENSOR_RECORDER_inc_pointer
	ldmib	r4, {r2, r3}
	cmp	r2, r3
	bne	.L9
	add	r0, r4, #8
	ldr	r1, [r4, #0]
	bl	nm_MOISTURE_SENSOR_RECORDER_inc_pointer
.L9:
	ldr	r1, [r4, #4]
	ldr	r2, [r4, #12]
	ldr	r3, .L13+4
	cmp	r1, r2
	bne	.L10
	add	r0, r3, #12
	ldr	r1, [r3, #0]
	bl	nm_MOISTURE_SENSOR_RECORDER_inc_pointer
.L10:
	ldr	r4, .L13+4
	ldr	r2, [r4, #4]
	ldr	r3, [r4, #16]
	cmp	r2, r3
	bne	.L11
	add	r0, r4, #16
	ldr	r1, [r4, #0]
	bl	nm_MOISTURE_SENSOR_RECORDER_inc_pointer
.L11:
	ldr	r0, [r4, #24]
	bl	xTimerIsTimerActive
	subs	r3, r0, #0
	bne	.L12
	mvn	r2, #0
	str	r2, [sp, #0]
	ldr	r2, .L13+4
	mov	r1, #2
	ldr	r0, [r2, #24]
	ldr	r2, .L13+20
	bl	xTimerGenericCommand
.L12:
	ldr	r3, .L13
	ldr	r0, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L14:
	.align	2
.L13:
	.word	moisture_sensor_recorder_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC0
	.word	8208
	.word	ci_moisture_sensor_recorder_timer_callback
	.word	1440000
	.word	.LC1
	.word	.LC2
.LFE3:
	.size	MOISTURE_SENSOR_RECORDER_add_a_record, .-MOISTURE_SENSOR_RECORDER_add_a_record
	.global	msrcs
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/moisture_sensor_recorder.c\000"
.LC1:
	.ascii	"\000"
.LC2:
	.ascii	"Timer NOT CREATED : %s, %u\000"
	.section	.bss.msrcs,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	msrcs, %object
	.size	msrcs, 28
msrcs:
	.space	28
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/moisture_sensor_recorder.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x60
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0x2d
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x24
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x56
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x65
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"ci_moisture_sensor_recorder_timer_callback\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/moisture_sensor_recorder.c\000"
.LASF2:
	.ascii	"MOISTURE_SENSOR_RECORDER_add_a_record\000"
.LASF5:
	.ascii	"nm_moisture_sensor_recorder_verify_allocation\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"nm_MOISTURE_SENSOR_RECORDER_inc_pointer\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
