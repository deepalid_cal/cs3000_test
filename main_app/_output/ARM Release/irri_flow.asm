	.file	"irri_flow.c"
	.text
.Ltext0:
	.section	.text.IRRI_FLOW_extract_system_info_out_of_msg_from_main,"ax",%progbits
	.align	2
	.global	IRRI_FLOW_extract_system_info_out_of_msg_from_main
	.type	IRRI_FLOW_extract_system_info_out_of_msg_from_main, %function
IRRI_FLOW_extract_system_info_out_of_msg_from_main:
.LFB0:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI0:
	sub	sp, sp, #36
.LCFI1:
	mov	r4, r0
	mov	r5, #0
	add	r0, sp, #36
	mov	r8, r1
	str	r5, [r0, #-28]!
	ldr	r1, [r4, #0]
	mov	r2, #1
	bl	memcpy
	ldr	r3, [r4, #0]
	add	r3, r3, #1
	str	r3, [r4, #0]
	ldr	r3, [sp, #8]
	sub	r3, r3, #1
	cmp	r3, #3
	movls	r6, #1
	bls	.L2
	ldr	r0, .L9
	bl	RemovePathFromFileName
	mov	r2, #167
	mov	r6, r5
	mov	r1, r0
	ldr	r0, .L9+4
	bl	Alert_Message_va
	b	.L3
.L7:
.LBB2:
	ldr	r1, [r4, #0]
	mov	r2, #2
	add	r0, sp, #30
	bl	memcpy
	ldr	r1, [r4, #0]
	mov	r2, #8
	add	r1, r1, #2
	str	r1, [r4, #0]
	mov	r0, sp
	bl	memcpy
	ldr	r1, [r4, #0]
	mov	r2, #2
	add	r1, r1, #8
	str	r1, [r4, #0]
	add	r0, sp, #32
	bl	memcpy
	ldr	r1, [r4, #0]
	mov	r2, #2
	add	r1, r1, #2
	str	r1, [r4, #0]
	add	r0, sp, #34
	bl	memcpy
	ldr	r1, [r4, #0]
	mov	r2, #4
	add	r1, r1, #2
	str	r1, [r4, #0]
	add	r0, sp, #12
	bl	memcpy
	ldr	r1, [r4, #0]
	mov	r2, #4
	add	r1, r1, #4
	str	r1, [r4, #0]
	add	r0, sp, #16
	bl	memcpy
	ldr	r1, [r4, #0]
	mov	r2, #4
	add	r1, r1, #4
	str	r1, [r4, #0]
	add	r0, sp, #20
	bl	memcpy
	ldr	r1, [r4, #0]
	mov	r2, #4
	add	r1, r1, #4
	str	r1, [r4, #0]
	add	r0, sp, #24
	bl	memcpy
	ldr	r3, [r4, #0]
	mov	r1, #400
	add	r3, r3, #4
	str	r3, [r4, #0]
	ldr	r3, .L9+8
	ldr	r2, .L9
	ldr	r0, [r3, #0]
	mov	r3, #217
	bl	xQueueTakeMutexRecursive_debug
	ldrh	r0, [sp, #30]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	r7, r0, #0
	beq	.L4
	ldr	r3, .L9+12
	ldr	r3, [r3, #48]
	cmp	r8, r3
	ldmneia	sp, {r2-r3}
	strne	r2, [r7, #464]
	strne	r3, [r7, #468]
	ldrneh	r3, [sp, #34]
	strne	r3, [r7, #100]
.L5:
	ldrh	r2, [sp, #32]
	ldr	r3, .L9+16
	str	r2, [r7, r3]
	ldr	r3, [sp, #12]	@ float
	ldr	r2, [sp, #16]
	str	r3, [r7, #304]	@ float
	ldrb	r3, [r7, #468]	@ zero_extendqisi2
	and	r2, r2, #1
	bic	r3, r3, #32
	orr	r3, r3, r2, asl #5
	strb	r3, [r7, #468]
	ldr	r2, [sp, #20]
	and	r3, r3, #255
	and	r2, r2, #1
	bic	r3, r3, #64
	orr	r3, r3, r2, asl #6
	strb	r3, [r7, #468]
	ldr	r2, [sp, #24]
	ldr	r3, .L9+20
	str	r2, [r7, r3]
	b	.L6
.L4:
	ldr	r0, .L9
	bl	RemovePathFromFileName
	ldr	r2, .L9+24
	mov	r6, r7
	mov	r1, r0
	ldr	r0, .L9+28
	bl	Alert_Message_va
.L6:
	ldr	r3, .L9+8
.LBE2:
	add	r5, r5, #1
.LBB3:
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L2:
.LBE3:
	ldr	r3, [sp, #8]
	cmp	r5, r3
	bcc	.L7
.L3:
	mov	r0, r6
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L10:
	.align	2
.L9:
	.word	.LC0
	.word	.LC1
	.word	system_preserves_recursive_MUTEX
	.word	config_c
	.word	14112
	.word	14124
	.word	261
	.word	.LC2
.LFE0:
	.size	IRRI_FLOW_extract_system_info_out_of_msg_from_main, .-IRRI_FLOW_extract_system_info_out_of_msg_from_main
	.section	.text.IRRI_FLOW_extract_the_poc_info_from_the_token,"ax",%progbits
	.align	2
	.global	IRRI_FLOW_extract_the_poc_info_from_the_token
	.type	IRRI_FLOW_extract_the_poc_info_from_the_token, %function
IRRI_FLOW_extract_the_poc_info_from_the_token:
.LFB1:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #0]
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	ldrb	r7, [r3], #1	@ zero_extendqisi2
	sub	sp, sp, #48
.LCFI3:
	str	r3, [r0, #0]
	sub	r3, r7, #1
	cmp	r3, #11
	mov	r5, r0
	bls	.L19
	ldr	r0, .L21
	bl	RemovePathFromFileName
	ldr	r2, .L21+4
	mov	r4, #0
	mov	r1, r0
	ldr	r0, .L21+8
	bl	Alert_Message_va
	b	.L13
.L18:
	ldr	r1, [r5, #0]
	mov	r2, #48
	mov	r0, sp
	bl	memcpy
	ldr	r3, [r5, #0]
	mov	r1, #400
	add	r3, r3, #48
	str	r3, [r5, #0]
	ldr	r2, .L21
	ldr	r3, .L21+12
	ldr	r0, [r8, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldmia	sp, {r0, r1, r2}
	bl	POC_PRESERVES_get_poc_preserve_ptr_for_these_details
	subs	sl, r0, #0
	beq	.L14
	ldr	r3, [sl, #8]
	ldr	r2, [sp, #0]
	cmp	r2, r3
	beq	.L15
	ldr	r0, .L21
	bl	RemovePathFromFileName
	ldr	r2, .L21+16
	mov	r4, #0
	mov	r1, r0
	ldr	r0, .L21+20
	bl	Alert_Message_va
	b	.L16
.L15:
	add	r1, sp, #8
	mov	r3, sp
	mov	r2, #3
.L17:
	ldr	r0, [r1, #4]!	@ float
	subs	r2, r2, #1
	str	r0, [sl, #148]	@ float
	ldr	r0, [r3, #24]
	str	r0, [sl, #168]
	ldr	r0, [r3, #36]
	add	r3, r3, #4
	str	r0, [sl, #172]
	add	sl, sl, #88
	bne	.L17
	b	.L16
.L14:
	ldr	r0, .L21
	bl	RemovePathFromFileName
	ldr	r2, .L21+24
	mov	r4, sl
	mov	r1, r0
	ldr	r0, .L21+28
	bl	Alert_Message_va
.L16:
	ldr	r0, [r8, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r4, #0
	beq	.L13
	add	r6, r6, #1
	b	.L12
.L19:
	ldr	r8, .L21+32
	mov	r6, #0
	mov	r4, #1
.L12:
	cmp	r6, r7
	bcc	.L18
.L13:
	mov	r0, r4
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L22:
	.align	2
.L21:
	.word	.LC0
	.word	305
	.word	.LC3
	.word	323
	.word	333
	.word	.LC4
	.word	353
	.word	.LC5
	.word	poc_preserves_recursive_MUTEX
.LFE1:
	.size	IRRI_FLOW_extract_the_poc_info_from_the_token, .-IRRI_FLOW_extract_the_poc_info_from_the_token
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/irri_flow.c\000"
.LC1:
	.ascii	"IRRI: system data error in token : %s, %u\000"
.LC2:
	.ascii	"IRRI_FLOW: system not found : %s, %u\000"
.LC3:
	.ascii	"IRRI: poc data error in token : %s, %u\000"
.LC4:
	.ascii	"Irri_Flow: poc_preserves mis-match. : %s, %u\000"
.LC5:
	.ascii	"IRRI_FLOW: poc not found : %s, %u\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x4c
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/irri_flow.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x47
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x78
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x116
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 76
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"IRRI_FLOW_extract_system_info_out_of_msg_from_main\000"
.LASF1:
	.ascii	"IRRI_FLOW_extract_the_poc_info_from_the_token\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/irri_flow.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
