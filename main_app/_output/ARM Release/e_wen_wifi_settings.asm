	.file	"e_wen_wifi_settings.c"
	.text
.Ltext0:
	.section	.text.FDTO_WEN_PROGRAMMING_draw_wifi_settings,"ax",%progbits
	.align	2
	.global	FDTO_WEN_PROGRAMMING_draw_wifi_settings
	.type	FDTO_WEN_PROGRAMMING_draw_wifi_settings, %function
FDTO_WEN_PROGRAMMING_draw_wifi_settings:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L4
	str	lr, [sp, #-4]!
.LCFI0:
	ldrsh	r1, [r3, #0]
	mov	r0, #70
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L5:
	.align	2
.L4:
	.word	.LANCHOR0
.LFE4:
	.size	FDTO_WEN_PROGRAMMING_draw_wifi_settings, .-FDTO_WEN_PROGRAMMING_draw_wifi_settings
	.section	.text.set_wen_programming_struct_from_wifi_guivars,"ax",%progbits
	.align	2
	.global	set_wen_programming_struct_from_wifi_guivars
	.type	set_wen_programming_struct_from_wifi_guivars, %function
set_wen_programming_struct_from_wifi_guivars:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L15
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldr	r3, [r3, #0]
	cmp	r3, #6
	bne	.L7
.LBB2:
	ldr	r3, .L15+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r2, [r3, #164]
	cmp	r2, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r4, [r3, #168]
	cmp	r4, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r1, .L15+8
	mov	r2, #65
	add	r0, r4, #166
	bl	strlcpy
	ldr	r1, .L15+12
	mov	r2, #64
	add	r0, r4, #247
	bl	strlcpy
	ldr	r1, .L15+16
	mov	r2, #33
	add	r0, r4, #120
	bl	strlcpy
	ldr	r1, .L15+20
	mov	r2, #49
	add	r0, r4, #49
	bl	strlcpy
	add	r0, r4, #496
	ldr	r1, .L15+24
	mov	r2, #64
	add	r0, r0, #1
	bl	strlcpy
	add	r0, r4, #432
	ldr	r1, .L15+28
	mov	r2, #64
	add	r0, r0, #1
	bl	strlcpy
	add	r0, r4, #368
	mov	r2, #64
	ldr	r1, .L15+32
	add	r0, r0, #1
	bl	strlcpy
	ldr	r3, .L15+36
	ldr	r0, .L15+40
	ldr	r1, [r3, #0]
	bl	e_SHARED_get_easyGUI_string_at_index
	ldr	r5, .L15+44
	mov	r2, #13
	mov	r1, r0
	add	r0, r4, #153
	bl	strlcpy
	ldr	r1, [r5, #0]
	ldr	r0, .L15+48
	bl	e_SHARED_get_easyGUI_string_at_index
	mov	r2, #5
	mov	r1, r0
	add	r0, r4, #231
	bl	strlcpy
	ldr	r3, .L15+52
	mov	r0, #1616
	ldr	r1, [r3, #0]
	bl	e_SHARED_get_easyGUI_string_at_index
	mov	r2, #10
	mov	r1, r0
	add	r0, r4, #308
	add	r0, r0, #3
	bl	strlcpy
	ldr	r3, .L15+56
	ldr	r0, .L15+60
	ldr	r1, [r3, #0]
	bl	e_SHARED_get_easyGUI_string_at_index
	mov	r2, #4
	mov	r1, r0
	add	r0, r4, #320
	add	r0, r0, #1
	bl	strlcpy
	ldr	r3, .L15+64
	ldr	r0, .L15+68
	ldr	r1, [r3, #0]
	bl	e_SHARED_get_easyGUI_string_at_index
	mov	r2, #7
	mov	r1, r0
	add	r0, r4, #324
	add	r0, r0, #3
	bl	strlcpy
	ldr	r3, .L15+72
	ldr	r0, .L15+76
	ldr	r1, [r3, #0]
	bl	e_SHARED_get_easyGUI_string_at_index
	mov	r2, #13
	mov	r1, r0
	add	r0, r4, #340
	add	r0, r0, #3
	bl	strlcpy
	ldr	r3, .L15+80
	mov	r0, #1296
	ldr	r1, [r3, #0]
	bl	e_SHARED_get_easyGUI_string_at_index
	mov	r2, #9
	mov	r1, r0
	add	r0, r4, #332
	add	r0, r0, #2
	bl	strlcpy
	ldr	r3, .L15+84
	ldr	r0, .L15+88
	ldr	r1, [r3, #0]
	bl	e_SHARED_get_easyGUI_string_at_index
	mov	r2, #13
	mov	r1, r0
	add	r0, r4, #356
	bl	strlcpy
	ldr	r3, [r5, #0]
	ldr	r0, .L15+92
	cmp	r3, #1
	ldreq	r3, .L15+96
	ldrne	r3, .L15+100
	ldr	r1, [r3, #0]
	bl	e_SHARED_get_easyGUI_string_at_index
	mov	r2, #11
	mov	r1, r0
	add	r0, r4, #236
	bl	strlcpy
	ldr	r3, .L15+104
	add	r0, r4, #324
	ldr	r3, [r3, #0]
	add	r0, r0, #1
	str	r3, [r4, #576]
	ldr	r3, .L15+108
	mov	r1, #2
	ldr	r3, [r3, #0]
	ldr	r2, .L15+112
	str	r3, [r4, #564]
	ldr	r3, .L15+116
	ldr	r3, [r3, #0]
	str	r3, [r4, #568]
	ldr	r3, .L15+120
	ldr	r3, [r3, #0]
	str	r3, [r4, #572]
	ldr	r3, .L15+124
	ldr	r3, [r3, #0]
	bl	snprintf
	ldr	r3, .L15+128
	ldr	r3, [r3, #0]
	str	r3, [r4, #584]
	ldr	r3, .L15+132
	ldr	r3, [r3, #0]
	str	r3, [r4, #588]
	ldr	r3, .L15+136
	ldr	r3, [r3, #0]
	str	r3, [r4, #592]
	ldr	r3, .L15+140
	ldr	r3, [r3, #0]
	str	r3, [r4, #580]
	ldmfd	sp!, {r4, r5, pc}
.L7:
.LBE2:
	cmp	r3, #9
	ldmnefd	sp!, {r4, r5, pc}
.LBB3:
	ldr	r3, .L15+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r2, [r3, #180]
	cmp	r2, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r4, [r3, #192]
	cmp	r4, #0
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r5, .L15+44
	ldr	r1, .L15+16
	mov	r2, #33
	add	r0, r4, #210
	bl	strlcpy
	ldr	r3, [r5, #0]
	ldr	r1, .L15+8
	str	r3, [r4, #376]
	mov	r2, #65
	add	r0, r4, #243
	bl	strlcpy
	add	r0, r4, #308
	ldr	r1, .L15+12
	mov	r2, #64
	bl	strlcpy
	ldr	r3, .L15+52
	ldr	r3, [r3, #0]
	str	r3, [r4, #380]
	ldr	r3, .L15+56
	ldr	r3, [r3, #0]
	str	r3, [r4, #384]
	ldr	r3, [r5, #0]
	cmp	r3, #1
	ldreq	r3, .L15+96
	ldrne	r3, .L15+100
	ldr	r3, [r3, #0]
	str	r3, [r4, #372]
	ldr	r3, .L15+144
	ldr	r3, [r3, #0]
	str	r3, [r4, #388]
	ldr	r3, .L15+148
	ldr	r3, [r3, #0]
	str	r3, [r4, #392]
	ldr	r3, .L15+128
	ldr	r3, [r3, #0]
	str	r3, [r4, #400]
	ldr	r3, .L15+132
	ldr	r3, [r3, #0]
	str	r3, [r4, #396]
	ldmfd	sp!, {r4, r5, pc}
.L16:
	.align	2
.L15:
	.word	GuiVar_WENRadioType
	.word	dev_state
	.word	GuiVar_WENKey
	.word	GuiVar_WENPassphrase
	.word	GuiVar_WENSSID
	.word	GuiVar_WENStatus
	.word	GuiVar_WENWPAEAPTLSCredentials
	.word	GuiVar_WENWPAPassword
	.word	GuiVar_WENWPAUsername
	.word	GuiVar_WENRadioMode
	.word	1599
	.word	GuiVar_WENSecurity
	.word	1605
	.word	GuiVar_WENWEPAuthentication
	.word	GuiVar_WENWEPKeySize
	.word	1306
	.word	GuiVar_WENWPAAuthentication
	.word	1618
	.word	GuiVar_WENWPAEAPTTLSOption
	.word	1228
	.word	GuiVar_WENWPAIEEE8021X
	.word	GuiVar_WENWPAPEAPOption
	.word	1384
	.word	1308
	.word	GuiVar_WENWEPKeyType
	.word	GuiVar_WENWPAKeyType
	.word	GuiVar_WENWPAEAPTLSValidateCert
	.word	GuiVar_WENWPAEncryptionCCMP
	.word	.LC0
	.word	GuiVar_WENWPAEncryptionTKIP
	.word	GuiVar_WENWPAEncryptionWEP
	.word	GuiVar_WENWEPTXKey
	.word	GuiVar_WENChangeKey
	.word	GuiVar_WENChangePassphrase
	.word	GuiVar_WENChangePassword
	.word	GuiVar_WENChangeCredentials
	.word	GuiVar_WENWPAEncryption
	.word	GuiVar_WENWPA2Encryption
.LBE3:
.LFE0:
	.size	set_wen_programming_struct_from_wifi_guivars, .-set_wen_programming_struct_from_wifi_guivars
	.section	.text.get_wifi_guivars_from_wen_programming_struct,"ax",%progbits
	.align	2
	.global	get_wifi_guivars_from_wen_programming_struct
	.type	get_wifi_guivars_from_wen_programming_struct, %function
get_wifi_guivars_from_wen_programming_struct:
.LFB1:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L21
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI2:
	ldr	r3, [r3, #0]
	cmp	r3, #6
	bne	.L18
.LBB4:
	ldr	fp, .L21+4
	ldr	r3, [fp, #0]
	cmp	r3, #0
	beq	.L17
	ldr	r2, [r3, #164]
	cmp	r2, #0
	beq	.L17
	ldr	r4, [r3, #168]
	cmp	r4, #0
	beq	.L17
	ldr	r5, [r3, #144]
	cmp	r5, #0
	bne	.L17
	add	r1, r4, #166
	mov	r2, #65
	ldr	r0, .L21+8
	bl	strlcpy
	add	r1, r4, #49
	mov	r2, #49
	ldr	r0, .L21+12
	bl	strlcpy
	add	r1, r4, #247
	mov	r2, #65
	ldr	r0, .L21+16
	bl	strlcpy
	add	r1, r4, #120
	mov	r2, #33
	ldr	r0, .L21+20
	bl	strlcpy
	add	r1, r4, #496
	add	r1, r1, #1
	mov	r2, #65
	ldr	r0, .L21+24
	bl	strlcpy
	add	r1, r4, #432
	add	r1, r1, #1
	mov	r2, #64
	ldr	r0, .L21+28
	bl	strlcpy
	add	r1, r4, #368
	mov	r2, #64
	add	r1, r1, #1
	ldr	r0, .L21+32
	bl	strlcpy
	ldr	r0, .L21+20
	mov	r1, #33
	bl	e_SHARED_string_validation
	ldr	r0, .L21+12
	mov	r1, #49
	bl	e_SHARED_string_validation
	add	r6, r4, #153
	mov	r1, #64
	ldr	r0, .L21+32
	bl	e_SHARED_string_validation
	mov	r0, r6
	bl	strlen
	mov	r8, #5
	ldr	r2, .L21+36
	mov	r3, r5
	str	r8, [sp, #0]
	mov	sl, #3
	add	r7, r4, #308
	add	r7, r7, sl
	add	r9, r4, #324
	mov	r1, r0
	mov	r0, r6
	bl	e_SHARED_get_index_of_easyGUI_string
	ldr	r3, .L21+40
	add	r6, r4, #231
	str	r0, [r3, #0]
	mov	r0, r6
	bl	strlen
	ldr	r2, .L21+44
	mov	r3, r5
	str	sl, [sp, #0]
	mov	r1, r0
	mov	r0, r6
	bl	e_SHARED_get_index_of_easyGUI_string
	ldr	r3, .L21+48
	mov	r6, #1
	str	r0, [r3, #0]
	mov	r0, r7
	bl	strlen
	mov	r2, #1616
	mov	r3, r5
	str	r6, [sp, #0]
	mov	r1, r0
	mov	r0, r7
	bl	e_SHARED_get_index_of_easyGUI_string
	ldr	r3, .L21+52
	add	r7, r4, #320
	add	r7, r7, r6
	str	r0, [r3, #0]
	mov	r0, r7
	bl	strlen
	ldr	r2, .L21+56
	mov	r3, r5
	str	r6, [sp, #0]
	mov	r1, r0
	mov	r0, r7
	bl	e_SHARED_get_index_of_easyGUI_string
	ldr	ip, .L21+60
	add	r7, r9, sl
	str	r0, [ip, #0]
	mov	r0, r7
	str	ip, [sp, #4]
	bl	strlen
	ldr	r2, .L21+64
	mov	r3, r5
	str	r6, [sp, #0]
	mov	r1, r0
	mov	r0, r7
	bl	e_SHARED_get_index_of_easyGUI_string
	ldr	r3, .L21+68
	add	r7, r4, #236
	str	r0, [r3, #0]
	mov	r0, r7
	bl	strlen
	ldr	r2, .L21+72
	mov	r3, r5
	str	r6, [sp, #0]
	mov	r1, r0
	mov	r0, r7
	bl	e_SHARED_get_index_of_easyGUI_string
	ldr	r3, .L21+76
	str	r0, [r3, #0]
	mov	r0, r7
	bl	strlen
	ldr	r2, .L21+72
	mov	r3, r5
	str	r6, [sp, #0]
	mov	r1, r0
	mov	r0, r7
	bl	e_SHARED_get_index_of_easyGUI_string
	ldr	r3, .L21+80
	add	r7, r4, #340
	add	r7, r7, sl
	str	r0, [r3, #0]
	mov	r0, r7
	bl	strlen
	ldr	r2, .L21+84
	mov	r3, r5
	str	r8, [sp, #0]
	mov	r1, r0
	mov	r0, r7
	bl	e_SHARED_get_index_of_easyGUI_string
	ldr	r3, .L21+88
	add	r7, r4, #332
	add	r7, r7, #2
	str	r0, [r3, #0]
	mov	r0, r7
	bl	strlen
	mov	r2, #1296
	mov	r3, r5
	str	sl, [sp, #0]
	mov	r1, r0
	mov	r0, r7
	bl	e_SHARED_get_index_of_easyGUI_string
	ldr	r3, .L21+92
	add	r7, r4, #356
	str	r0, [r3, #0]
	mov	r0, r7
	bl	strlen
	ldr	r2, .L21+96
	mov	r3, r5
	str	r6, [sp, #0]
	mov	r1, r0
	mov	r0, r7
	bl	e_SHARED_get_index_of_easyGUI_string
	ldr	r3, .L21+100
	str	r0, [r3, #0]
	add	r0, r9, r6
	bl	atoi
	ldr	r3, .L21+104
	ldr	r2, [r4, #576]
	ldr	ip, [sp, #4]
	str	r0, [r3, #0]
	ldr	r3, .L21+108
	str	r2, [r3, #0]
	ldr	r2, [r4, #564]
	ldr	r3, .L21+112
	str	r2, [r3, #0]
	ldr	r2, [r4, #568]
	ldr	r3, .L21+116
	str	r2, [r3, #0]
	ldr	r2, [r4, #572]
	ldr	r3, .L21+120
	str	r2, [r3, #0]
	ldr	r2, [r4, #596]
	ldr	r3, .L21+124
	str	r2, [r3, #0]
	ldr	r3, [fp, #0]
	ldr	r2, [ip, #0]
	str	r6, [r3, #144]
	ldr	r3, .L21+128
	str	r5, [r3, #0]
	ldr	r3, .L21+132
	str	r5, [r3, #0]
	ldr	r3, .L21+136
	str	r5, [r3, #0]
	ldr	r3, .L21+140
	str	r5, [r3, #0]
	b	.L20
.L18:
.LBE4:
	cmp	r3, #9
	bne	.L17
.LBB5:
	ldr	r6, .L21+4
	ldr	r3, [r6, #0]
	cmp	r3, #0
	beq	.L17
	ldr	r2, [r3, #180]
	cmp	r2, #0
	beq	.L17
	ldr	r4, [r3, #192]
	cmp	r4, #0
	beq	.L17
	ldr	r5, [r3, #144]
	cmp	r5, #0
	bne	.L17
	mov	r2, #33
	add	r1, r4, #210
	ldr	r0, .L21+20
	bl	strlcpy
	ldr	r0, .L21+20
	mov	r1, #33
	bl	e_SHARED_string_validation
	ldr	r3, .L21+40
	mov	r2, #3
	str	r2, [r3, #0]
	ldr	r3, .L21+48
	ldr	r2, [r4, #376]
	add	r1, r4, #243
	str	r2, [r3, #0]
	ldr	r0, .L21+8
	mov	r2, #65
	bl	strlcpy
	add	r1, r4, #308
	mov	r2, #65
	ldr	r0, .L21+16
	bl	strlcpy
	ldr	r2, [r4, #384]
	ldr	r3, .L21+60
	ldr	r1, [r4, #380]
	str	r2, [r3, #0]
	ldr	r3, .L21+52
	ldr	r0, [r4, #388]
	str	r1, [r3, #0]
	ldr	r3, .L21+68
	ldr	r1, .L21+76
	str	r5, [r3, #0]
	mov	r3, #1
	str	r3, [r1, #0]
	ldr	r1, .L21+80
	str	r3, [r1, #0]
	ldr	r1, .L21+104
	str	r3, [r1, #0]
	ldr	r1, .L21+144
	str	r0, [r1, #0]
	ldr	r0, [r4, #392]
	ldr	r1, .L21+148
	str	r0, [r1, #0]
	ldr	r1, [r6, #0]
	str	r3, [r1, #144]
	ldr	r3, .L21+128
	str	r5, [r3, #0]
	ldr	r3, .L21+132
	str	r5, [r3, #0]
.L20:
	ldr	r3, .L21+152
	str	r2, [r3, #0]
.L17:
.LBE5:
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L22:
	.align	2
.L21:
	.word	GuiVar_WENRadioType
	.word	dev_state
	.word	GuiVar_WENKey
	.word	GuiVar_WENStatus
	.word	GuiVar_WENPassphrase
	.word	GuiVar_WENSSID
	.word	GuiVar_WENWPAEAPTLSCredentials
	.word	GuiVar_WENWPAPassword
	.word	GuiVar_WENWPAUsername
	.word	1599
	.word	GuiVar_WENRadioMode
	.word	1605
	.word	GuiVar_WENSecurity
	.word	GuiVar_WENWEPAuthentication
	.word	1306
	.word	GuiVar_WENWEPKeySize
	.word	1618
	.word	GuiVar_WENWPAAuthentication
	.word	1308
	.word	GuiVar_WENWEPKeyType
	.word	GuiVar_WENWPAKeyType
	.word	1228
	.word	GuiVar_WENWPAEAPTTLSOption
	.word	GuiVar_WENWPAIEEE8021X
	.word	1384
	.word	GuiVar_WENWPAPEAPOption
	.word	GuiVar_WENWEPTXKey
	.word	GuiVar_WENWPAEAPTLSValidateCert
	.word	GuiVar_WENWPAEncryptionCCMP
	.word	GuiVar_WENWPAEncryptionTKIP
	.word	GuiVar_WENWPAEncryptionWEP
	.word	GuiVar_WENRSSI
	.word	GuiVar_WENChangeKey
	.word	GuiVar_WENChangePassphrase
	.word	GuiVar_WENChangePassword
	.word	GuiVar_WENChangeCredentials
	.word	GuiVar_WENWPAEncryption
	.word	GuiVar_WENWPA2Encryption
	.word	.LANCHOR1
.LFE1:
	.size	get_wifi_guivars_from_wen_programming_struct, .-get_wifi_guivars_from_wen_programming_struct
	.section	.text.g_WEN_PROGRAMMING_wifi_values_in_range,"ax",%progbits
	.align	2
	.global	g_WEN_PROGRAMMING_wifi_values_in_range
	.type	g_WEN_PROGRAMMING_wifi_values_in_range, %function
g_WEN_PROGRAMMING_wifi_values_in_range:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L38
	ldr	r2, .L38+4
	ldr	r3, [r3, #0]
	ldr	r2, [r2, #0]
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	cmp	r2, r3
	ldr	r2, .L38+8
	movne	r1, #1
	strne	r1, [r2, #0]
	ldr	r2, [r2, #0]
	mov	r5, r0
	cmp	r2, #0
	beq	.L31
.LBB8:
	ldr	r2, .L38+12
	ldr	r2, [r2, #0]
	cmp	r2, #1
	bne	.L26
	ldr	r1, .L38+16
	ldr	r1, [r1, #0]
	cmp	r1, #0
	bne	.L26
	cmp	r3, #0
	moveq	r4, #10
	beq	.L27
	cmp	r3, #1
	moveq	r4, #26
	beq	.L27
.L26:
	sub	r2, r2, #2
	cmp	r2, #1
	movhi	r4, #0
	bhi	.L27
	ldr	r3, .L38+20
	ldr	r4, [r3, #0]
	cmp	r4, #0
	moveq	r4, #64
	movne	r4, #0
.L27:
	ldr	r0, .L38+24
	bl	strlen
	cmp	r0, r4
	moveq	r0, #1
	ldmeqfd	sp!, {r4, r5, pc}
	ldr	r3, .L38+12
	ldr	r2, [r3, #0]
	ldr	r3, .L38+28
	cmp	r2, #1
	movne	r2, #13
	moveq	r2, #6
	cmp	r5, #0
	str	r2, [r3, #0]
	beq	.L30
	mov	r2, #65
	ldr	r0, .L38+24
	ldr	r1, .L38+32
	bl	strlcpy
	ldr	r3, .L38+8
	mov	r2, #0
	str	r2, [r3, #0]
.L30:
	ldr	r3, .L38+4
	mov	r0, #608
	ldr	r2, [r3, #0]
	ldr	r3, .L38
	str	r2, [r3, #0]
	bl	DIALOG_draw_ok_dialog
	bl	bad_key_beep
	mov	r0, #0
	ldmfd	sp!, {r4, r5, pc}
.L31:
.LBE8:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, pc}
.L39:
	.align	2
.L38:
	.word	GuiVar_WENWEPKeySize
	.word	.LANCHOR1
	.word	GuiVar_WENChangeKey
	.word	GuiVar_WENSecurity
	.word	GuiVar_WENWEPKeyType
	.word	GuiVar_WENWPAKeyType
	.word	GuiVar_WENKey
	.word	.LANCHOR0
	.word	.LC1
.LFE2:
	.size	g_WEN_PROGRAMMING_wifi_values_in_range, .-g_WEN_PROGRAMMING_wifi_values_in_range
	.section	.text.g_WEN_PROGRAMMING_initialize_wifi_guivars,"ax",%progbits
	.align	2
	.global	g_WEN_PROGRAMMING_initialize_wifi_guivars
	.type	g_WEN_PROGRAMMING_initialize_wifi_guivars, %function
g_WEN_PROGRAMMING_initialize_wifi_guivars:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L42
	ldr	r2, .L42+4
	stmfd	sp!, {r4, lr}
.LCFI4:
	str	r2, [r3, #0]
	ldr	r3, .L42+8
	ldr	r3, [r3, #0]
	ldr	r4, [r3, #144]
	cmp	r4, #0
	bne	.L41
	ldr	r1, .L42+12
	mov	r2, #65
	ldr	r0, .L42+16
	bl	strlcpy
	ldr	r1, .L42+12
	mov	r2, #65
	ldr	r0, .L42+20
	bl	strlcpy
	ldr	r1, .L42+12
	mov	r2, #33
	ldr	r0, .L42+24
	bl	strlcpy
	ldr	r1, .L42+12
	mov	r2, #49
	ldr	r0, .L42+28
	bl	strlcpy
	ldr	r1, .L42+12
	mov	r2, #65
	ldr	r0, .L42+32
	bl	strlcpy
	ldr	r1, .L42+12
	mov	r2, #64
	ldr	r0, .L42+36
	bl	strlcpy
	mov	r2, #64
	ldr	r1, .L42+12
	ldr	r0, .L42+40
	bl	strlcpy
	ldr	r0, .L42+24
	mov	r1, #33
	bl	e_SHARED_string_validation
	ldr	r0, .L42+28
	mov	r1, #49
	bl	e_SHARED_string_validation
	ldr	r0, .L42+40
	mov	r1, #64
	bl	e_SHARED_string_validation
	ldr	r3, .L42+44
	mov	r2, #1
	str	r4, [r3, #0]
	ldr	r3, .L42+48
	ldr	r0, .L42+52
	str	r4, [r3, #0]
	ldr	r3, .L42+56
	ldr	r1, .L42+12
	str	r4, [r3, #0]
	ldr	r3, .L42+60
	str	r4, [r3, #0]
	ldr	r3, .L42+64
	str	r4, [r3, #0]
	ldr	r3, .L42+68
	str	r4, [r3, #0]
	ldr	r3, .L42+72
	str	r2, [r3, #0]
	ldr	r3, .L42+76
	mov	r2, #49
	str	r4, [r3, #0]
	ldr	r3, .L42+80
	str	r4, [r3, #0]
	ldr	r3, .L42+84
	str	r4, [r3, #0]
	ldr	r3, .L42+88
	str	r4, [r3, #0]
	ldr	r3, .L42+92
	str	r4, [r3, #0]
	ldr	r3, .L42+96
	str	r4, [r3, #0]
	ldr	r3, .L42+100
	str	r4, [r3, #0]
	ldr	r3, .L42+104
	str	r4, [r3, #0]
	ldr	r3, .L42+108
	str	r4, [r3, #0]
	ldr	r3, .L42+112
	str	r4, [r3, #0]
	ldr	r3, .L42+116
	str	r4, [r3, #0]
	bl	strlcpy
.L41:
	ldr	r2, .L42+120
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L42+124
	str	r3, [r2, #0]
	ldr	r2, .L42+128
	str	r3, [r2, #0]
	ldr	r2, .L42+132
	str	r3, [r2, #0]
	ldmfd	sp!, {r4, pc}
.L43:
	.align	2
.L42:
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36865
	.word	dev_state
	.word	.LC1
	.word	GuiVar_WENKey
	.word	GuiVar_WENPassphrase
	.word	GuiVar_WENSSID
	.word	GuiVar_WENStatus
	.word	GuiVar_WENWPAEAPTLSCredentials
	.word	GuiVar_WENWPAPassword
	.word	GuiVar_WENWPAUsername
	.word	GuiVar_WENRadioMode
	.word	GuiVar_WENRSSI
	.word	GuiVar_CommOptionInfoText
	.word	GuiVar_WENSecurity
	.word	GuiVar_WENWEPAuthentication
	.word	GuiVar_WENWEPKeySize
	.word	GuiVar_WENWEPKeyType
	.word	GuiVar_WENWEPTXKey
	.word	GuiVar_WENWPAAuthentication
	.word	GuiVar_WENWPAEAPTLSValidateCert
	.word	GuiVar_WENWPAEAPTTLSOption
	.word	GuiVar_WENWPAEncryptionCCMP
	.word	GuiVar_WENWPAEncryptionTKIP
	.word	GuiVar_WENWPAEncryptionWEP
	.word	GuiVar_WENWPAIEEE8021X
	.word	GuiVar_WENWPAKeyType
	.word	GuiVar_WENWPAPEAPOption
	.word	GuiVar_WENWPAEncryption
	.word	GuiVar_WENWPA2Encryption
	.word	GuiVar_WENChangeKey
	.word	GuiVar_WENChangePassphrase
	.word	GuiVar_WENChangePassword
	.word	GuiVar_WENChangeCredentials
.LFE3:
	.size	g_WEN_PROGRAMMING_initialize_wifi_guivars, .-g_WEN_PROGRAMMING_initialize_wifi_guivars
	.section	.text.WEN_PROGRAMMING_process_wifi_settings,"ax",%progbits
	.align	2
	.global	WEN_PROGRAMMING_process_wifi_settings
	.type	WEN_PROGRAMMING_process_wifi_settings, %function
WEN_PROGRAMMING_process_wifi_settings:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L179
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI5:
	ldrsh	r2, [r3, #0]
	ldr	r3, .L179+4
	mov	r6, r0
	cmp	r2, r3
	mov	r5, r1
	bne	.L149
	cmp	r0, #67
	beq	.L47
	cmp	r0, #2
	bne	.L147
	ldr	r3, .L179+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L147
.L47:
	ldr	r3, .L179+12
	ldr	r3, [r3, #0]
	cmp	r3, #32
	ldrls	pc, [pc, r3, asl #2]
	b	.L49
.L56:
	.word	.L50
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L51
	.word	.L49
	.word	.L49
	.word	.L52
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L51
	.word	.L49
	.word	.L52
	.word	.L49
	.word	.L53
	.word	.L49
	.word	.L54
	.word	.L53
	.word	.L49
	.word	.L49
	.word	.L55
	.word	.L49
	.word	.L53
	.word	.L49
	.word	.L54
	.word	.L49
	.word	.L53
	.word	.L49
	.word	.L54
.L50:
	ldr	r0, .L179+16
	ldr	r1, .L179+20
	mov	r2, #33
	b	.L161
.L51:
	ldr	r0, .L179+24
	ldr	r1, .L179+20
	mov	r2, #65
	bl	strlcpy
	mov	r4, #1
	b	.L48
.L52:
	ldr	r0, .L179+28
	b	.L170
.L53:
	ldr	r0, .L179+32
	b	.L169
.L54:
	ldr	r0, .L179+36
.L169:
	ldr	r1, .L179+20
	mov	r2, #64
	b	.L161
.L55:
	ldr	r0, .L179+40
.L170:
	ldr	r1, .L179+20
	mov	r2, #65
.L161:
	bl	strlcpy
	b	.L147
.L49:
	ldr	r0, .L179+44
	bl	Alert_Message
.L147:
	mov	r4, #0
.L48:
	mov	r0, r6
	mov	r1, r5
	ldr	r2, .L179+48
	bl	KEYBOARD_process_key
	cmp	r4, #0
	beq	.L44
	mov	r0, #0
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	g_WEN_PROGRAMMING_wifi_values_in_range
.L149:
	cmp	r0, #3
	beq	.L62
	bhi	.L66
	cmp	r0, #1
	beq	.L60
	ldr	r3, .L179+8
	bhi	.L61
	b	.L175
.L66:
	cmp	r0, #67
	beq	.L64
	bhi	.L67
	cmp	r0, #4
	bne	.L58
	b	.L176
.L67:
	cmp	r0, #80
	beq	.L65
	cmp	r0, #84
	bne	.L58
	b	.L65
.L61:
	ldrsh	r3, [r3, #0]
	ldr	r2, .L179+52
	str	r3, [r2, #0]
	cmp	r3, #35
	ldrls	pc, [pc, r3, asl #2]
	b	.L91
.L86:
	.word	.L69
	.word	.L91
	.word	.L91
	.word	.L91
	.word	.L91
	.word	.L91
	.word	.L97
	.word	.L71
	.word	.L91
	.word	.L99
	.word	.L73
	.word	.L91
	.word	.L91
	.word	.L97
	.word	.L74
	.word	.L99
	.word	.L73
	.word	.L91
	.word	.L75
	.word	.L103
	.word	.L77
	.word	.L75
	.word	.L104
	.word	.L105
	.word	.L80
	.word	.L91
	.word	.L81
	.word	.L103
	.word	.L82
	.word	.L91
	.word	.L81
	.word	.L103
	.word	.L82
	.word	.L108
	.word	.L109
	.word	.L110
.L69:
	ldr	r3, .L179+12
	mov	r2, #0
	str	r3, [sp, #0]
	mov	r3, #1
	stmib	sp, {r2, r3}
	ldr	r2, .L179+16
	mov	r0, #39
	mov	r1, #30
	mov	r3, #32
	b	.L153
.L71:
	ldr	r3, .L179+56
	mov	r1, #0
	ldr	r4, [r3, #0]
	mov	r2, #65
	cmp	r4, #1
	ldr	r0, .L179+24
	movne	r4, #10
	moveq	r4, #26
	bl	memset
	ldr	r3, .L179+12
	mov	lr, #0
	str	r3, [sp, #0]
	mov	r3, #1
	stmib	sp, {r3, lr}
	ldr	r2, .L179+24
	mov	r0, #41
	mov	r1, #132
	mov	r3, r4
	b	.L153
.L74:
	ldr	r3, .L179+12
	mov	ip, #0
	str	r3, [sp, #0]
	mov	r3, #1
	stmib	sp, {r3, ip}
	ldr	r2, .L179+24
	mov	r0, #34
	mov	r1, #132
	b	.L163
.L73:
	ldr	r3, .L179+12
	mov	r0, #66
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	mov	r1, #132
	ldr	r2, .L179+28
	b	.L163
.L75:
	ldr	r3, .L179+12
	mov	r0, #0
	str	r3, [sp, #0]
	mov	r3, #1
	stmib	sp, {r0, r3}
	mov	r1, #116
	mov	r0, #59
	b	.L171
.L81:
	ldr	r3, .L179+12
	mov	r1, #0
	str	r3, [sp, #0]
	mov	r3, #1
	stmib	sp, {r1, r3}
	mov	r0, #59
	mov	r1, #132
.L171:
	ldr	r2, .L179+32
	b	.L164
.L77:
	ldr	r3, .L179+12
	mov	r0, #58
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	mov	r1, #148
	b	.L162
.L80:
	ldr	r3, .L179+12
	mov	r2, #0
	str	r3, [sp, #0]
	mov	r3, #1
	stmib	sp, {r2, r3}
	ldr	r2, .L179+40
	mov	r0, #66
	mov	r1, #164
.L163:
	mov	r3, #64
	b	.L153
.L82:
	ldr	r3, .L179+12
	mov	r0, #58
	str	r3, [sp, #0]
	mov	r1, #164
	mov	r3, #0
	str	r3, [sp, #4]
	str	r3, [sp, #8]
.L162:
	ldr	r2, .L179+36
.L164:
	mov	r3, #63
.L153:
	bl	e_SHARED_show_keyboard
	b	.L115
.L65:
	ldr	r3, .L179+8
	ldr	r2, .L179+52
	ldrsh	r3, [r3, #0]
	str	r3, [r2, #0]
	sub	r3, r3, #1
	cmp	r3, #36
	ldrls	pc, [pc, r3, asl #2]
	b	.L91
.L113:
	.word	.L92
	.word	.L93
	.word	.L94
	.word	.L95
	.word	.L96
	.word	.L97
	.word	.L91
	.word	.L98
	.word	.L99
	.word	.L91
	.word	.L100
	.word	.L101
	.word	.L97
	.word	.L91
	.word	.L99
	.word	.L91
	.word	.L102
	.word	.L91
	.word	.L103
	.word	.L91
	.word	.L91
	.word	.L104
	.word	.L105
	.word	.L91
	.word	.L106
	.word	.L91
	.word	.L103
	.word	.L91
	.word	.L107
	.word	.L91
	.word	.L103
	.word	.L91
	.word	.L108
	.word	.L109
	.word	.L110
	.word	.L111
	.word	.L112
.L92:
	ldr	r3, .L179+60
	ldr	r3, [r3, #0]
	cmp	r3, #6
	bne	.L91
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	ldr	r1, .L179+64
	b	.L154
.L93:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r6
	ldr	r1, .L179+68
	b	.L166
.L94:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	ldr	r1, .L179+72
	b	.L165
.L95:
	ldr	r3, .L179+60
	ldr	r3, [r3, #0]
	cmp	r3, #6
	bne	.L91
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	ldr	r1, .L179+76
	b	.L165
.L101:
	ldr	r3, .L179+60
	ldr	r3, [r3, #0]
	cmp	r3, #6
	bne	.L91
	ldr	r1, .L179+80
	mov	r3, #1
	mov	r0, r6
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L165:
	mov	r2, #0
	b	.L157
.L96:
	ldr	r5, .L179+56
	mov	r4, #1
	mov	r2, #0
	mov	r3, r4
	mov	r0, r6
	mov	r1, r5
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	bl	process_uns32
	ldr	r3, .L179+84
	ldr	r2, [r3, #0]
	ldr	r3, [r5, #0]
	cmp	r2, r3
	ldrne	r3, .L179+88
	strne	r4, [r3, #0]
	b	.L116
.L98:
	ldr	r3, .L179+60
	ldr	r3, [r3, #0]
	cmp	r3, #6
	bne	.L91
	mov	r2, #1
	str	r2, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r6
	ldr	r1, .L179+92
	b	.L167
.L100:
	ldr	r3, .L179+60
	ldr	r3, [r3, #0]
	cmp	r3, #6
	bne	.L91
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	ldr	r1, .L179+96
	b	.L165
.L108:
	ldr	r3, .L179+60
	ldr	r3, [r3, #0]
	cmp	r3, #9
	bne	.L121
	ldr	r3, .L179+68
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L91
.L121:
	ldr	r0, .L179+100
	b	.L158
.L109:
	ldr	r0, .L179+104
	b	.L158
.L110:
	ldr	r0, .L179+108
	b	.L158
.L102:
	ldr	r1, .L179+112
	mov	r3, #1
	mov	r0, r6
	mov	r2, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L166:
	mov	r3, #3
.L157:
	bl	process_uns32
	b	.L116
.L106:
	ldr	r1, .L179+116
	mov	r3, #1
	mov	r0, r6
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L154:
	mov	r2, #0
	mov	r3, #5
.L155:
	bl	process_uns32
	b	.L115
.L107:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	ldr	r1, .L179+120
	mov	r2, #0
	b	.L155
.L97:
	ldr	r0, .L179+88
	b	.L158
.L99:
	ldr	r0, .L179+124
	b	.L158
.L103:
	ldr	r0, .L179+128
	b	.L158
.L104:
	ldr	r0, .L179+132
	b	.L158
.L105:
	ldr	r0, .L179+136
.L158:
	bl	process_bool
	b	.L116
.L111:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r6
	ldr	r1, .L179+140
	b	.L155
.L112:
	ldr	r1, .L179+144
	mov	r3, #1
	mov	r2, #0
	mov	r0, r6
	str	r3, [sp, #0]
	str	r2, [sp, #4]
.L167:
	mov	r3, #4
	b	.L155
.L91:
	bl	bad_key_beep
.L115:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Refresh_Screen
.L176:
	ldr	r3, .L179+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #9
	beq	.L124
	bgt	.L126
	cmp	r3, #5
	beq	.L123
	cmp	r3, #6
	bne	.L122
	b	.L124
.L126:
	sub	r3, r3, #34
	cmp	r3, #1
	bhi	.L122
	b	.L177
.L123:
	mov	r0, #3
	b	.L159
.L124:
	mov	r0, #4
.L159:
	mov	r1, #1
.L160:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	CURSOR_Select
.L177:
	ldr	r3, .L179+68
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L127
	ldr	r3, .L179+76
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L128
	ldr	r3, .L179+124
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #10
	bne	.L159
	b	.L168
.L128:
	ldr	r3, .L179+88
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #7
	bne	.L159
	b	.L145
.L127:
	ldr	r3, .L179+96
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L131
	ldr	r3, .L179+80
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L132
	ldr	r3, .L179+124
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #16
	moveq	r0, #15
	b	.L159
.L132:
	ldr	r3, .L179+88
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #14
	moveq	r0, #13
	b	.L159
.L131:
	ldr	r3, .L179+112
	ldr	r1, [r3, #0]
	cmp	r1, #0
	bne	.L135
	ldr	r3, .L179+128
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #20
	moveq	r0, #19
	b	.L159
.L135:
	cmp	r1, #1
	bne	.L137
	ldr	r3, .L179+136
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #24
	moveq	r0, #23
	b	.L160
.L137:
	ldr	r3, .L179+128
	cmp	r1, #2
	ldr	r3, [r3, #0]
	bne	.L139
	cmp	r3, #0
	movne	r0, #28
	moveq	r0, #27
	b	.L159
.L139:
	cmp	r3, #0
	movne	r0, #32
	moveq	r0, #31
	b	.L159
.L122:
	mov	r0, #1
.L60:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	CURSOR_Up
.L175:
	ldrsh	r3, [r3, #0]
	cmp	r3, #4
	beq	.L143
	blt	.L62
	sub	r3, r3, #33
	cmp	r3, #1
	bhi	.L62
	b	.L178
.L143:
	ldr	r3, .L179+76
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L145
.L168:
	mov	r0, #9
	b	.L159
.L145:
	mov	r0, #6
	b	.L159
.L178:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	bad_key_beep
.L62:
	mov	r0, #1
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	CURSOR_Down
.L64:
	ldr	r3, .L179+8
	mov	r0, #0
	ldrsh	r2, [r3, #0]
	ldr	r3, .L179+52
	str	r2, [r3, #0]
	bl	g_WEN_PROGRAMMING_wifi_values_in_range
	ldr	r3, .L179+148
	mov	r2, #11
	str	r2, [r3, #0]
.L58:
	mov	r0, r6
	mov	r1, r5
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	KEY_process_global_keys
.L116:
	mov	r0, #0
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Redraw_Screen
.L44:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, pc}
.L180:
	.align	2
.L179:
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR2
	.word	GuiVar_WENSSID
	.word	GuiVar_GroupName
	.word	GuiVar_WENKey
	.word	GuiVar_WENPassphrase
	.word	GuiVar_WENWPAUsername
	.word	GuiVar_WENWPAPassword
	.word	GuiVar_WENWPAEAPTLSCredentials
	.word	.LC2
	.word	FDTO_WEN_PROGRAMMING_draw_wifi_settings
	.word	.LANCHOR0
	.word	GuiVar_WENWEPKeySize
	.word	GuiVar_WENRadioType
	.word	GuiVar_WENRadioMode
	.word	GuiVar_WENSecurity
	.word	GuiVar_WENWEPAuthentication
	.word	GuiVar_WENWEPKeyType
	.word	GuiVar_WENWPAKeyType
	.word	.LANCHOR1
	.word	GuiVar_WENChangeKey
	.word	GuiVar_WENWEPTXKey
	.word	GuiVar_WENWPAAuthentication
	.word	GuiVar_WENWPAEncryptionCCMP
	.word	GuiVar_WENWPAEncryptionTKIP
	.word	GuiVar_WENWPAEncryptionWEP
	.word	GuiVar_WENWPAIEEE8021X
	.word	GuiVar_WENWPAEAPTTLSOption
	.word	GuiVar_WENWPAPEAPOption
	.word	GuiVar_WENChangePassphrase
	.word	GuiVar_WENChangePassword
	.word	GuiVar_WENWPAEAPTLSValidateCert
	.word	GuiVar_WENChangeCredentials
	.word	GuiVar_WENWPAEncryption
	.word	GuiVar_WENWPA2Encryption
	.word	GuiVar_MenuScreenToShow
.LFE5:
	.size	WEN_PROGRAMMING_process_wifi_settings, .-WEN_PROGRAMMING_process_wifi_settings
	.section	.bss.g_WEN_PROGRAMMING_wifi_saved_key_size,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_WEN_PROGRAMMING_wifi_saved_key_size, %object
	.size	g_WEN_PROGRAMMING_wifi_saved_key_size, 4
g_WEN_PROGRAMMING_wifi_saved_key_size:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%d\000"
.LC1:
	.ascii	"\000"
.LC2:
	.ascii	"Wifi KB prog error\000"
	.section	.bss.g_WEN_PROGRAMMING_wifi_cp_using_keyboard,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_WEN_PROGRAMMING_wifi_cp_using_keyboard, %object
	.size	g_WEN_PROGRAMMING_wifi_cp_using_keyboard, 4
g_WEN_PROGRAMMING_wifi_cp_using_keyboard:
	.space	4
	.section	.bss.g_WEN_PROGRAMMING_wifi_previous_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_WEN_PROGRAMMING_wifi_previous_cursor_pos, %object
	.size	g_WEN_PROGRAMMING_wifi_previous_cursor_pos, 4
g_WEN_PROGRAMMING_wifi_previous_cursor_pos:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI0-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_wen_wifi_settings.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xa1
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x197
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x218
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x93
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x100
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x5
	.4byte	0x21
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x1d4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST4
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x230
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB4
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"get_wifi_guivars_from_wen_programming_struct\000"
.LASF3:
	.ascii	"g_WEN_PROGRAMMING_initialize_wifi_guivars\000"
.LASF1:
	.ascii	"set_wen_programming_struct_from_wifi_guivars\000"
.LASF7:
	.ascii	"g_WEN_PROGRAMMING_wifi_values_in_range\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_wen_wifi_settings.c\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"FDTO_WEN_PROGRAMMING_draw_wifi_settings\000"
.LASF4:
	.ascii	"WEN_PROGRAMMING_process_wifi_settings\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
