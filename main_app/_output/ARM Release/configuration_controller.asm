	.file	"configuration_controller.c"
	.text
.Ltext0:
	.section	.text.init_config_controller,"ax",%progbits
	.align	2
	.type	init_config_controller, %function
init_config_controller:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L4
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldrb	r2, [r3, #116]	@ zero_extendqisi2
	mov	r1, #144
	orr	r2, r2, #15
	strb	r2, [r3, #116]
	ldrb	r2, [r3, #53]	@ zero_extendqisi2
	strb	r1, [r3, #52]
	and	r2, r2, #232
	orr	r2, r2, #9
	strb	r2, [r3, #53]
	mov	r2, #0
	add	r1, r3, #52
	mov	ip, r2
.L2:
	add	r0, r2, #14
	str	ip, [r1, #4]!
	ldrb	lr, [r3, r0, asl #2]	@ zero_extendqisi2
	add	r2, r2, #1
	orr	lr, lr, #32
	cmp	r2, #6
	mov	r5, #0
	ldr	r4, .L4
	strb	lr, [r3, r0, asl #2]
	bne	.L2
	mov	r1, #16
	ldr	r2, .L4+4
	add	r0, r4, #88
	str	r5, [r4, #80]
	str	r5, [r4, #84]
	bl	snprintf
	ldr	r2, .L4+8
	add	r0, r4, #104
	mov	r1, #8
	bl	snprintf
	ldr	r3, .L4+12
	mov	r2, #25
	str	r2, [r4, #128]
	mov	r2, #5
	str	r2, [r4, #132]
	mov	r2, #120
	str	r3, [r4, #48]
	str	r2, [r4, #136]
	mov	r3, #1
	mov	r2, #1024
	str	r5, [r4, #112]
	str	r5, [r4, #120]
	str	r3, [r4, #124]
	str	r2, [r4, #140]
	str	r3, [r4, #144]
	ldmfd	sp!, {r4, r5, pc}
.L5:
	.align	2
.L4:
	.word	.LANCHOR0
	.word	.LC0
	.word	.LC1
	.word	50065
.LFE0:
	.size	init_config_controller, .-init_config_controller
	.section	.text.AD_conversion_and_return_results,"ax",%progbits
	.align	2
	.type	AD_conversion_and_return_results, %function
AD_conversion_and_return_results:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r4, .L15
	mov	r2, #128
	ldr	r3, [r4, #72]
	ldr	r3, .L15+4
	cmp	r0, #0
	str	r2, [r3, #4]
	moveq	r3, #644
	beq	.L14
	cmp	r0, #1
	bne	.L9
	mov	r3, #660
.L14:
	str	r3, [r4, #4]
	b	.L8
.L9:
	ldr	r0, .L15+8
	bl	Alert_Message
.L8:
	ldr	r3, [r4, #8]
	ldr	r2, .L15+4
	orr	r3, r3, #2
	str	r3, [r4, #8]
	mov	r3, #201
.L12:
	subs	r3, r3, #1
	bne	.L10
	ldr	r0, .L15+12
	bl	Alert_Message
	b	.L11
.L10:
	ldr	r1, [r2, #4]
	tst	r1, #128
	beq	.L12
.L11:
	ldr	r3, .L15
	ldr	r0, [r3, #72]
	mov	r0, r0, asl #22
	mov	r0, r0, lsr #22
	ldmfd	sp!, {r4, pc}
.L16:
	.align	2
.L15:
	.word	1074036736
	.word	1073790976
	.word	.LC2
	.word	.LC3
.LFE10:
	.size	AD_conversion_and_return_results, .-AD_conversion_and_return_results
	.section	.text.controller_config_updater,"ax",%progbits
	.align	2
	.type	controller_config_updater, %function
controller_config_updater:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	stmfd	sp!, {r4, lr}
.LCFI2:
	mov	r4, r0
	bne	.L18
	ldr	r0, .L23
	mov	r1, r4
	ldmfd	sp!, {r4, lr}
	b	Alert_Message_va
.L18:
	ldr	r0, .L23+4
	mov	r1, #2
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	ldreq	r3, .L23+8
	moveq	r2, #1
	streq	r2, [r3, #144]
	beq	.L20
.L19:
	cmp	r4, #1
	bne	.L21
.L20:
	ldr	r3, .L23+8
	ldrb	r2, [r3, #52]	@ zero_extendqisi2
	and	r2, r2, #143
	orr	r2, r2, #144
	strb	r2, [r3, #52]
	ldrb	r2, [r3, #53]	@ zero_extendqisi2
	and	r2, r2, #248
	orr	r2, r2, #9
	strb	r2, [r3, #53]
	ldmfd	sp!, {r4, pc}
.L21:
	ldr	r0, .L23+12
	ldmfd	sp!, {r4, lr}
	b	Alert_Message
.L24:
	.align	2
.L23:
	.word	.LC4
	.word	.LC5
	.word	.LANCHOR0
	.word	.LC6
.LFE1:
	.size	controller_config_updater, .-controller_config_updater
	.section	.text.init_file_configuration_controller,"ax",%progbits
	.align	2
	.global	init_file_configuration_controller
	.type	init_file_configuration_controller, %function
init_file_configuration_controller:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI3:
	ldr	r1, .L26
	mov	r3, #148
	str	r3, [sp, #0]
	ldr	r3, .L26+4
	mov	r0, #0
	str	r3, [sp, #8]
	ldr	r3, .L26+8
	mov	r2, #2
	str	r3, [sp, #12]
	ldr	r3, .L26+12
	str	r0, [sp, #4]
	str	r0, [sp, #16]
	bl	FLASH_FILE_find_or_create_variable_file
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.L27:
	.align	2
.L26:
	.word	.LANCHOR1
	.word	controller_config_updater
	.word	init_config_controller
	.word	.LANCHOR0
.LFE2:
	.size	init_file_configuration_controller, .-init_file_configuration_controller
	.section	.text.save_file_configuration_controller,"ax",%progbits
	.align	2
	.global	save_file_configuration_controller
	.type	save_file_configuration_controller, %function
save_file_configuration_controller:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #148
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI4:
	ldr	r1, .L29
	mov	r0, #0
	str	r3, [sp, #0]
	mov	r2, #2
	ldr	r3, .L29+4
	str	r0, [sp, #4]
	str	r0, [sp, #8]
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L30:
	.align	2
.L29:
	.word	.LANCHOR1
	.word	.LANCHOR0
.LFE3:
	.size	save_file_configuration_controller, .-save_file_configuration_controller
	.section	.text.CONFIG_this_controller_originates_commserver_messages,"ax",%progbits
	.align	2
	.global	CONFIG_this_controller_originates_commserver_messages
	.type	CONFIG_this_controller_originates_commserver_messages, %function
CONFIG_this_controller_originates_commserver_messages:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI5:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	ldreq	pc, [sp], #4
.LBB4:
	ldr	r3, .L34
	mov	r2, #56
	ldr	r3, [r3, #80]
	mul	r3, r2, r3
.LBE4:
	ldr	r2, .L34+4
	ldr	r0, [r2, r3]
	adds	r0, r0, #0
	movne	r0, #1
	ldr	pc, [sp], #4
.L35:
	.align	2
.L34:
	.word	.LANCHOR0
	.word	.LANCHOR2
.LFE4:
	.size	CONFIG_this_controller_originates_commserver_messages, .-CONFIG_this_controller_originates_commserver_messages
	.section	.text.CONFIG_is_connected,"ax",%progbits
	.align	2
	.global	CONFIG_is_connected
	.type	CONFIG_is_connected, %function
CONFIG_is_connected:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI6:
	bl	CONFIG_this_controller_originates_commserver_messages
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L40
	ldr	r2, .L40+4
	ldr	r3, [r3, #80]
	mov	r1, #56
	mla	r3, r1, r3, r2
	ldr	r4, [r3, #44]
	cmp	r4, #0
	beq	.L38
	mov	r0, #1
	blx	r4
	ldmfd	sp!, {r4, pc}
.L38:
	ldr	r0, .L40+8
	bl	Alert_Message
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L41:
	.align	2
.L40:
	.word	.LANCHOR0
	.word	.LANCHOR2
	.word	.LC7
.LFE5:
	.size	CONFIG_is_connected, .-CONFIG_is_connected
	.section	.text.CONFIG_this_controller_is_a_configured_hub,"ax",%progbits
	.align	2
	.global	CONFIG_this_controller_is_a_configured_hub
	.type	CONFIG_this_controller_is_a_configured_hub, %function
CONFIG_this_controller_is_a_configured_hub:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI7:
	ldr	r4, .L47
	ldrb	r0, [r4, #52]	@ zero_extendqisi2
	ands	r0, r0, #8
	ldmeqfd	sp!, {r4, pc}
	ldr	r0, [r4, #144]
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
	bl	CONFIG_this_controller_originates_commserver_messages
	cmp	r0, #0
	ldmeqfd	sp!, {r4, pc}
	ldr	r0, [r4, #84]
	sub	r0, r0, #1
	cmp	r0, #2
	movhi	r0, #0
	movls	r0, #1
	ldmfd	sp!, {r4, pc}
.L48:
	.align	2
.L47:
	.word	.LANCHOR0
.LFE6:
	.size	CONFIG_this_controller_is_a_configured_hub, .-CONFIG_this_controller_is_a_configured_hub
	.section	.text.CONFIG_this_controller_is_behind_a_hub,"ax",%progbits
	.align	2
	.global	CONFIG_this_controller_is_behind_a_hub
	.type	CONFIG_this_controller_is_behind_a_hub, %function
CONFIG_this_controller_is_behind_a_hub:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI8:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	ldreq	pc, [sp], #4
	ldr	r3, .L52
	ldr	r0, [r3, #80]
	sub	r0, r0, #1
	cmp	r0, #2
	movhi	r0, #0
	movls	r0, #1
	ldr	pc, [sp], #4
.L53:
	.align	2
.L52:
	.word	.LANCHOR0
.LFE7:
	.size	CONFIG_this_controller_is_behind_a_hub, .-CONFIG_this_controller_is_behind_a_hub
	.section	.text.CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets,"ax",%progbits
	.align	2
	.global	CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets
	.type	CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets, %function
CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	r3, r0, #1
	cmp	r3, #1
	bhi	.L58
	ldr	r3, .L59
	cmp	r0, #1
	ldreq	r0, [r3, #80]
	ldrne	r0, [r3, #84]
	sub	r3, r0, #3
	rsbs	r0, r3, #0
	adc	r0, r0, r3
	bx	lr
.L58:
	mov	r0, #0
	bx	lr
.L60:
	.align	2
.L59:
	.word	.LANCHOR0
.LFE8:
	.size	CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets, .-CONFIG_the_device_on_this_port_is_used_for_controller_to_controller_packets
	.section	.text.init_a_to_d,"ax",%progbits
	.align	2
	.global	init_a_to_d
	.type	init_a_to_d, %function
init_a_to_d:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, lr}
.LCFI9:
	mov	r1, #3
	mov	r0, #1
	bl	clkpwr_setup_adc_ts
	mov	r1, #1
	mov	r0, #13
	bl	clkpwr_clk_en_dis
	mov	r0, #39
	bl	xDisable_ISR
	mov	r3, #0
	mov	r2, #2
	mov	r0, #39
	mov	r1, #21
	str	r3, [sp, #0]
	bl	xSetISR_Vector
	ldr	r3, .L62
	mov	r2, #644
	str	r2, [r3, #4]
	mov	r2, #4
	str	r2, [r3, #8]
	ldmfd	sp!, {r3, pc}
.L63:
	.align	2
.L62:
	.word	1074036736
.LFE9:
	.size	init_a_to_d, .-init_a_to_d
	.section	.text.CONFIG_device_discovery,"ax",%progbits
	.align	2
	.global	CONFIG_device_discovery
	.type	CONFIG_device_discovery, %function
CONFIG_device_discovery:
.LFB11:
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L78
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI10:
	sub	sp, sp, #76
.LCFI11:
	str	r3, [sp, #68]	@ float
	mov	r3, #1107296256
	str	r3, [sp, #72]	@ float
	sub	r3, r0, #1
	cmp	r3, #1
	mov	r5, r0
	bhi	.L64
	cmp	r0, #1
	movne	r6, #66
	moveq	r6, #65
.L69:
	cmp	r5, #1
	bne	.L67
	mov	r0, #0
	bl	AD_conversion_and_return_results
	mov	r4, r0
	mov	r0, #0
	b	.L77
.L67:
	mov	r0, #1
	bl	AD_conversion_and_return_results
	mov	r4, r0
	mov	r0, #1
.L77:
	bl	AD_conversion_and_return_results
	cmp	r4, r0
	bne	.L69
	fmsr	s12, r4	@ int
	flds	s14, [sp, #68]
	flds	s15, [sp, #72]
	fuitos	s13, s12
	fdivs	s14, s13, s14
	fmuls	s14, s14, s15
	fmrs	r0, s14
	bl	__round_UNS16
	cmp	r0, #10
	mov	r4, r0
	bhi	.L70
	cmp	r5, #1
	ldr	r3, .L78+4
	bne	.L71
	ldr	r2, [r3, #80]
	cmp	r0, r2
	strne	r0, [r3, #80]
	bne	.L73
	b	.L72
.L71:
	ldr	r2, [r3, #84]
	cmp	r0, r2
	strne	r0, [r3, #84]
	bne	.L73
	b	.L72
.L70:
	mov	r3, r6
	mov	r1, #64
	ldr	r2, .L78+8
	add	r0, sp, #4
	bl	snprintf
	add	r0, sp, #4
	bl	Alert_Message
	cmp	r5, #1
	ldr	r3, .L78+4
	bne	.L74
	ldr	r2, [r3, #80]
	cmp	r2, #0
	movne	r2, #0
	strne	r2, [r3, #80]
	bne	.L73
	b	.L72
.L74:
	ldr	r2, [r3, #84]
	cmp	r2, #0
	movne	r2, #0
	strne	r2, [r3, #84]
	bne	.L73
.L72:
	ldr	r3, .L78+12
	add	r0, sp, #4
	ldr	r3, [r3, r4, asl #2]
	mov	r1, #64
	str	r3, [sp, #0]
	ldr	r2, .L78+16
	mov	r3, r6
	bl	snprintf
	add	r0, sp, #4
	bl	Alert_Message
	b	.L64
.L73:
	ldr	r3, .L78+12
	ldr	r2, .L78+20
	ldr	r3, [r3, r4, asl #2]
	mov	r1, #64
	str	r3, [sp, #0]
	ldr	r4, .L78+24
	mov	r3, r6
	add	r0, sp, #4
	bl	snprintf
	add	r0, sp, #4
	bl	Alert_Message
	mov	r0, #0
	mov	r1, #4
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	ldr	r2, .L78+28
	ldr	r3, .L78+32
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L78+36
	mov	r2, #1
	str	r2, [r3, #204]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
.L64:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, pc}
.L79:
	.align	2
.L78:
	.word	1149239296
	.word	.LANCHOR0
	.word	.LC8
	.word	.LANCHOR3
	.word	.LC9
	.word	.LC10
	.word	irri_comm_recursive_MUTEX
	.word	.LC11
	.word	1110
	.word	irri_comm
.LFE11:
	.size	CONFIG_device_discovery, .-CONFIG_device_discovery
	.global	config_c
	.global	port_device_table
	.global	comm_device_names
	.section	.rodata.comm_device_names,"a",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	comm_device_names, %object
	.size	comm_device_names, 44
comm_device_names:
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"64.73.242.99\000"
.LC1:
	.ascii	"16001\000"
.LC2:
	.ascii	"AD: poor channel choice.\000"
.LC3:
	.ascii	"AD: never finishes\000"
.LC4:
	.ascii	"CONTLR CONFIG file unexpd update %u\000"
.LC5:
	.ascii	"CONTLR CONFIG file update : to revision %u from %u\000"
.LC6:
	.ascii	"CONTLR CONFIG updater error\000"
.LC7:
	.ascii	"CI: cannot check for connection\000"
.LC8:
	.ascii	"Port %c ID error. (Interface card resistors?)\000"
.LC9:
	.ascii	"Same Port %c Device: %s\000"
.LC10:
	.ascii	"New Port %c Device: %s\000"
.LC11:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/conf"
	.ascii	"iguration/configuration_controller.c\000"
.LC12:
	.ascii	"NONE INSTALLED\000"
.LC13:
	.ascii	"FireLine or StingRay\000"
.LC14:
	.ascii	"Freewave LRS455\000"
.LC15:
	.ascii	"Freewave FGR2\000"
.LC16:
	.ascii	"Lantronix UDS1100\000"
.LC17:
	.ascii	"Sierra Wireless LS300\000"
.LC18:
	.ascii	"Lantronix PremierWave XN\000"
.LC19:
	.ascii	"Lantronix PremierWave XC\000"
.LC20:
	.ascii	"Lantronix PremierWave XE\000"
.LC21:
	.ascii	"Lantronix WiBox\000"
.LC22:
	.ascii	"MultiTech LTE\000"
	.section	.rodata.CONTROLLER_CONFIG_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	CONTROLLER_CONFIG_FILENAME, %object
	.size	CONTROLLER_CONFIG_FILENAME, 25
CONTROLLER_CONFIG_FILENAME:
	.ascii	"CONTROLLER_CONFIGURATION\000"
	.section	.rodata.port_device_table,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	port_device_table, %object
	.size	port_device_table, 616
port_device_table:
	.word	0
	.word	57600
	.word	0
	.word	1
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1
	.word	9600
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	RAVEON_initialize_the_connection_process
	.word	RAVEON_connection_processing
	.word	RAVEON_is_connected
	.word	LR_RAVEON_initialize_device_exchange
	.word	LR_RAVEON_exchange_processing
	.word	1
	.word	57600
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	LR_FREEWAVE_power_control
	.word	LR_FREEWAVE_initialize_the_connection_process
	.word	LR_FREEWAVE_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	LR_FREEWAVE_initialize_device_exchange
	.word	LR_FREEWAVE_exchange_processing
	.word	1
	.word	57600
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	SR_FREEWAVE_power_control
	.word	SR_FREEWAVE_initialize_the_connection_process
	.word	SR_FREEWAVE_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	SR_FREEWAVE_initialize_device_exchange
	.word	SR_FREEWAVE_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	UDS1100_initialize_the_connection_process
	.word	UDS1100_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.word	0
	.word	57600
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	0
	.word	0
	.word	__GENERIC_gr_card_is_connected
	.word	GR_AIRLINK_initialize_device_exchange
	.word	GR_AIRLINK_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	WIBOX_initialize_the_connection_process
	.word	WIBOX_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	PWXC_initialize_the_connection_process
	.word	PWXC_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	WIBOX_initialize_the_connection_process
	.word	WIBOX_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	WIBOX_initialize_the_connection_process
	.word	WIBOX_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.word	1
	.word	115200
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	__GENERIC_gr_card_power_control
	.word	MTSMC_LAT_initialize_the_connection_process
	.word	MTSMC_LAT_connection_processing
	.word	__GENERIC_gr_card_is_connected
	.word	DEVICE_initialize_device_exchange
	.word	DEVICE_exchange_processing
	.section	.bss.config_c,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	config_c, %object
	.size	config_c, 148
config_c:
	.space	148
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI1-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI5-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI6-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI7-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI8-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI9-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x80
	.uleb128 0x2
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI10-.LFB11
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE22:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/configuration/configuration_controller.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x11c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF11
	.byte	0x1
	.4byte	.LASF12
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x2ac
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1a4
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x364
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x229
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x28b
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x29f
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST4
	.uleb128 0x5
	.4byte	0x21
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x2c9
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST6
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x2e1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST7
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x300
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x315
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x33a
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x3ae
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST10
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB10
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB11
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI11
	.4byte	.LFE11
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x74
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"AD_conversion_and_return_results\000"
.LASF9:
	.ascii	"init_a_to_d\000"
.LASF12:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/conf"
	.ascii	"iguration/configuration_controller.c\000"
.LASF4:
	.ascii	"save_file_configuration_controller\000"
.LASF13:
	.ascii	"CONFIG_this_controller_originates_commserver_messag"
	.ascii	"es\000"
.LASF5:
	.ascii	"CONFIG_is_connected\000"
.LASF0:
	.ascii	"init_config_controller\000"
.LASF11:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF6:
	.ascii	"CONFIG_this_controller_is_a_configured_hub\000"
.LASF7:
	.ascii	"CONFIG_this_controller_is_behind_a_hub\000"
.LASF3:
	.ascii	"init_file_configuration_controller\000"
.LASF2:
	.ascii	"controller_config_updater\000"
.LASF8:
	.ascii	"CONFIG_the_device_on_this_port_is_used_for_controll"
	.ascii	"er_to_controller_packets\000"
.LASF10:
	.ascii	"CONFIG_device_discovery\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
