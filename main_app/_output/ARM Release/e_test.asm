	.file	"e_test.c"
	.text
.Ltext0:
	.section	.text.TEST_copy_settings_into_guivars,"ax",%progbits
	.align	2
	.type	TEST_copy_settings_into_guivars, %function
TEST_copy_settings_into_guivars:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L5+8
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r6, .L5+12
	mov	r1, #400
	ldr	r2, .L5+16
	mov	r5, r0
	ldr	r0, [r3, #0]
	mov	r3, #101
	bl	xQueueTakeMutexRecursive_debug
	ldr	r4, .L5+20
	ldr	r1, [r6, #0]
	ldr	r0, [r4, #0]
	sub	r1, r1, #1
	bl	nm_STATION_get_pointer_to_station
	cmp	r0, #0
	beq	.L2
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L5+24
	bl	strlcpy
	ldr	r1, [r6, #0]
	ldr	r0, [r4, #0]
	sub	r1, r1, #1
	ldr	r2, .L5+28
	mov	r3, #4
	bl	STATION_get_station_number_string
	ldr	r0, [r4, #0]
	ldr	r1, .L5+32
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	b	.L3
.L2:
	ldr	r1, .L5+36
	mov	r2, #48
	ldr	r0, .L5+24
	bl	strlcpy
	ldr	r1, .L5+36
	mov	r2, #4
	ldr	r0, .L5+28
	bl	strlcpy
	ldr	r0, .L5+32
	ldr	r1, .L5+36
	mov	r2, #49
	bl	strlcpy
.L3:
	ldr	r3, .L5+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r5, #1
	ldmnefd	sp!, {r4, r5, r6, pc}
	ldr	r3, .L5+40
	ldr	r3, [r3, #136]
	fmsr	s13, r3	@ int
	ldr	r3, .L5+44
	fuitod	d7, s13
	fldd	d6, .L5
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, pc}
.L6:
	.align	2
.L5:
	.word	0
	.word	1078853632
	.word	list_program_data_recursive_MUTEX
	.word	GuiVar_StationInfoNumber
	.word	.LC0
	.word	GuiVar_StationInfoBoxIndex
	.word	GuiVar_StationDescription
	.word	GuiVar_StationInfoNumber_str
	.word	GuiVar_StationInfoControllerName
	.word	.LC1
	.word	config_c
	.word	GuiVar_TestRunTime
.LFE1:
	.size	TEST_copy_settings_into_guivars, .-TEST_copy_settings_into_guivars
	.section	.text.TEST_store_time_if_changed,"ax",%progbits
	.align	2
	.type	TEST_store_time_if_changed, %function
TEST_store_time_if_changed:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L9+4
	flds	s15, .L9
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	flds	s14, [r3, #0]
	ldr	r4, .L9+8
	fmuls	s15, s14, s15
	ldr	r0, [r4, #136]
	ftosizs	s15, s15
	fmrs	r5, s15	@ int
	rsb	r0, r0, r5
	bl	abs
	cmp	r0, #2
	ldmlefd	sp!, {r4, r5, pc}
	mov	r0, #0
	mov	r1, #10
	str	r5, [r4, #136]
	ldmfd	sp!, {r4, r5, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L10:
	.align	2
.L9:
	.word	1114636288
	.word	GuiVar_TestRunTime
	.word	config_c
.LFE0:
	.size	TEST_store_time_if_changed, .-TEST_store_time_if_changed
	.section	.text.FDTO_TEST_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_TEST_draw_screen
	.type	FDTO_TEST_draw_screen, %function
FDTO_TEST_draw_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L14
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	ldrnesh	r4, [r3, #0]
	mov	r5, r0
	bne	.L13
	bl	STATION_find_first_available_station_and_init_station_number_GuiVars
	ldr	r3, .L14+4
	mov	r2, #0
	mov	r4, #3
	str	r2, [r3, #0]
.L13:
	mov	r0, r5
	bl	TEST_copy_settings_into_guivars
	mov	r0, #58
	mov	r1, r4
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L15:
	.align	2
.L14:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
.LFE2:
	.size	FDTO_TEST_draw_screen, .-FDTO_TEST_draw_screen
	.section	.text.TEST_process_screen,"ax",%progbits
	.align	2
	.global	TEST_process_screen
	.type	TEST_process_screen, %function
TEST_process_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI3:
	mov	r4, r0
	mov	r5, r1
	beq	.L22
	bhi	.L26
	cmp	r0, #1
	beq	.L19
	bcc	.L18
	cmp	r0, #2
	beq	.L20
	cmp	r0, #3
	bne	.L17
	b	.L21
.L26:
	cmp	r0, #67
	beq	.L24
	bhi	.L27
	cmp	r0, #16
	beq	.L23
	cmp	r0, #20
	bne	.L17
	b	.L23
.L27:
	cmp	r0, #80
	beq	.L25
	cmp	r0, #84
	bne	.L17
	b	.L25
.L20:
	ldr	r3, .L64+4
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #3
	cmp	r3, #1
	bhi	.L52
	bl	COMM_MNGR_network_is_available_for_normal_use
	cmp	r0, #0
	bne	.L30
	bl	bad_key_beep
	ldr	r0, .L64+8
	b	.L57
.L30:
	ldr	r3, .L64+12
	ldr	r4, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	cmp	r4, r0
	bne	.L31
	ldr	r3, .L64+16
	ldr	r3, [r3, #112]
	cmp	r3, #1
	bne	.L31
	bl	bad_key_beep
	mov	r0, #604
.L57:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	DIALOG_draw_ok_dialog
.L31:
	bl	IRRI_COMM_if_any_2W_cable_is_over_heated
	cmp	r0, #0
	beq	.L32
	bl	bad_key_beep
	mov	r0, #640
	b	.L57
.L32:
	ldr	r4, .L64+20
	ldr	r5, .L64+12
	ldr	r1, [r4, #0]
	ldr	r0, [r5, #0]
	sub	r1, r1, #1
	bl	nm_STATION_get_pointer_to_station
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	r6, r0, #0
	bne	.L33
	bl	bad_key_beep
	ldr	r0, .L64+24
	bl	RemovePathFromFileName
	mov	r2, #200
	mov	r1, r0
	ldr	r0, .L64+28
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_Message_va
.L33:
	add	r0, r6, #516
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	cmp	r0, #0
	beq	.L34
	bl	bad_key_beep
	mov	r0, #612
	b	.L57
.L34:
	ldrb	r3, [r6, #468]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L35
	bl	bad_key_beep
	ldr	r0, .L64+32
	b	.L57
.L35:
	ldr	r6, .L64+36
	ldr	r3, [r6, #0]
	cmp	r3, #0
	bne	.L52
	bl	good_key_beep
	ldr	r3, [r5, #0]
	str	r3, [r6, #4]
	ldr	r3, [r4, #0]
	sub	r3, r3, #1
	str	r3, [r6, #8]
	ldr	r3, .L64+4
	ldrsh	r3, [r3, #0]
	sub	r2, r3, #4
	rsbs	r3, r2, #0
	adc	r3, r3, r2
	cmp	r3, #1
	str	r3, [r6, #16]
	ldrne	r3, .L64+40
	fldsne	s15, .L64
	moveq	r3, #360
	fldsne	s14, [r3, #0]
	fmulsne	s15, s14, s15
	ftouizsne	s15, s15
	fmrsne	r3, s15	@ int
	str	r3, [r6, #12]
	bl	TEST_store_time_if_changed
	ldr	r3, .L64+36
	mov	r2, #1
	str	r2, [r3, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	IRRI_DETAILS_jump_to_irrigation_details
.L25:
	ldr	r3, .L64+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L40
	cmp	r3, #2
	bne	.L52
	b	.L63
.L40:
	ldr	r5, .L64+44
	bl	good_key_beep
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L64+24
	ldr	r3, .L64+48
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L64+12
	ldr	r1, .L64+20
	cmp	r4, #84
	b	.L62
.L63:
	ldr	r2, .L64+52
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L64+40
	ldr	r3, .L64+56
	str	r2, [sp, #0]	@ float
	bl	process_fl
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Refresh_Screen
.L23:
	ldr	r5, .L64+44
	bl	good_key_beep
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L64+24
	ldr	r3, .L64+60
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L64+12
	ldr	r1, .L64+20
	cmp	r4, #20
.L62:
	bne	.L44
	bl	STATION_get_next_available_station
	b	.L45
.L44:
	bl	STATION_get_prev_available_station
.L45:
	mov	r0, #0
	bl	TEST_copy_settings_into_guivars
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #0
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Redraw_Screen
.L22:
	ldr	r3, .L64+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #2
	beq	.L48
	cmp	r3, #4
	beq	.L58
	cmp	r3, #0
	movne	r0, #1
	bne	.L19
	b	.L52
.L48:
	ldr	r3, .L64+64
	ldr	r0, [r3, #0]
	b	.L59
.L18:
	ldr	r3, .L64+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	ldreq	r2, .L64+64
	streq	r3, [r2, #0]
	beq	.L58
	cmp	r3, #3
	bne	.L21
	b	.L52
.L58:
	mov	r0, #2
.L59:
	mov	r1, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	CURSOR_Select
.L52:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	bad_key_beep
.L19:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	CURSOR_Up
.L21:
	mov	r0, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	CURSOR_Down
.L24:
	ldr	r3, .L64+68
	mov	r2, #7
	str	r2, [r3, #0]
	bl	TEST_store_time_if_changed
.L17:
	mov	r0, r4
	mov	r1, r5
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	KEY_process_global_keys
.L65:
	.align	2
.L64:
	.word	1114636288
	.word	GuiLib_ActiveCursorFieldNo
	.word	591
	.word	GuiVar_StationInfoBoxIndex
	.word	tpmicro_comm
	.word	GuiVar_StationInfoNumber
	.word	.LC0
	.word	.LC2
	.word	611
	.word	irri_comm
	.word	GuiVar_TestRunTime
	.word	list_program_data_recursive_MUTEX
	.word	271
	.word	1036831949
	.word	1101004800
	.word	303
	.word	.LANCHOR0
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	TEST_process_screen, .-TEST_process_screen
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_test.c\000"
.LC1:
	.ascii	"\000"
.LC2:
	.ascii	"System not found : %s, %u\000"
	.section	.bss.g_TEST_last_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_TEST_last_cursor_pos, %object
	.size	g_TEST_last_cursor_pos, 4
g_TEST_last_cursor_pos:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_test.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x61
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x4b
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x83
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x9b
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"TEST_process_screen\000"
.LASF0:
	.ascii	"TEST_copy_settings_into_guivars\000"
.LASF2:
	.ascii	"FDTO_TEST_draw_screen\000"
.LASF1:
	.ascii	"TEST_store_time_if_changed\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_test.c\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
