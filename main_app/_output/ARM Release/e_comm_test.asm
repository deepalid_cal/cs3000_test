	.file	"e_comm_test.c"
	.text
.Ltext0:
	.section	.text.COMM_TEST_send_all_program_data_to_the_cloud,"ax",%progbits
	.align	2
	.global	COMM_TEST_send_all_program_data_to_the_cloud
	.type	COMM_TEST_send_all_program_data_to_the_cloud, %function
COMM_TEST_send_all_program_data_to_the_cloud:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI0:
	ldr	r6, .L13
	mov	r4, r0
	ldr	r0, .L13+4
	mov	r5, r1
	bl	Alert_Message
	mov	r0, #34
	bl	NETWORK_CONFIG_on_all_settings_set_or_clear_commserver_change_bits
	cmp	r4, r6
	bne	.L2
	bl	save_file_configuration_network
.L2:
	mov	r0, #34
	bl	WEATHER_on_all_settings_set_or_clear_commserver_change_bits
	cmp	r4, r6
	bne	.L3
	bl	save_file_weather_control
.L3:
	ldr	r6, .L13
	mov	r0, #34
	bl	SYSTEM_on_all_systems_set_or_clear_commserver_change_bits
	cmp	r4, r6
	bne	.L4
	bl	save_file_irrigation_system
.L4:
	mov	r0, #34
	bl	STATION_GROUP_on_all_groups_set_or_clear_commserver_change_bits
	cmp	r4, r6
	bne	.L5
	bl	save_file_station_group
.L5:
	ldr	r6, .L13
	mov	r0, #34
	bl	MANUAL_PROGRAMS_on_all_groups_set_or_clear_commserver_change_bits
	cmp	r4, r6
	bne	.L6
	bl	save_file_manual_programs
.L6:
	mov	r0, #34
	bl	STATION_on_all_stations_set_or_clear_commserver_change_bits
	cmp	r4, r6
	bne	.L7
	bl	save_file_station_info
.L7:
	ldr	r6, .L13
	mov	r0, #34
	bl	POC_on_all_pocs_set_or_clear_commserver_change_bits
	cmp	r4, r6
	bne	.L8
	bl	save_file_POC
.L8:
	mov	r0, #34
	bl	MOISTURE_SENSOR_on_all_moisture_sensors_set_or_clear_commserver_change_bits
	cmp	r4, r6
	bne	.L9
	bl	save_file_moisture_sensor
.L9:
	ldr	r6, .L13
	mov	r0, #34
	bl	LIGHTS_on_all_lights_set_or_clear_commserver_change_bits
	cmp	r4, r6
	bne	.L10
	bl	save_file_LIGHTS
.L10:
	mov	r0, #34
	bl	WALK_THRU_on_all_groups_set_or_clear_commserver_change_bits
	cmp	r4, r6
	bne	.L11
	bl	save_file_walk_thru
.L11:
	ldr	r3, .L13+8
	mov	r2, #1
	str	r2, [r3, #112]
	ldr	r3, .L13+12
	cmp	r5, r3
	bne	.L1
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L13+16
	mov	r1, #2
	ldr	r0, [r3, #152]
	ldr	r2, .L13+20
	mov	r3, #0
	bl	xTimerGenericCommand
.L1:
	ldmfd	sp!, {r3, r4, r5, r6, pc}
.L14:
	.align	2
.L13:
	.word	921
	.word	.LC0
	.word	weather_preserves
	.word	409
	.word	cics
	.word	6000
.LFE0:
	.size	COMM_TEST_send_all_program_data_to_the_cloud, .-COMM_TEST_send_all_program_data_to_the_cloud
	.section	.text.FDTO_COMM_TEST_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_COMM_TEST_draw_screen
	.type	FDTO_COMM_TEST_draw_screen, %function
FDTO_COMM_TEST_draw_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L18
	str	lr, [sp, #-4]!
.LCFI1:
	ldrnesh	r1, [r3, #0]
	sub	sp, sp, #52
.LCFI2:
	bne	.L17
	sub	r3, r1, #81
	rsbs	r1, r3, #0
	adc	r1, r1, r3
	ldr	r3, .L18+4
.LBB4:
	mov	r2, #48
.LBE4:
	str	r1, [r3, #0]
.LBB5:
	mov	r0, sp
	mov	r3, #46
	ldr	r1, .L18+8
	strh	r3, [sp, #48]	@ movhi
	bl	strlcpy
	add	r1, sp, #48
	mov	r0, sp
	bl	strtok
	bl	atoi
	ldr	r3, .L18+12
	add	r1, sp, #48
	str	r0, [r3, #0]
	mov	r0, #0
	bl	strtok
	bl	atoi
	ldr	r3, .L18+16
	add	r1, sp, #48
	str	r0, [r3, #0]
	mov	r0, #0
	bl	strtok
	bl	atoi
	ldr	r3, .L18+20
	add	r1, sp, #48
	str	r0, [r3, #0]
	mov	r0, #0
	bl	strtok
	bl	atoi
	ldr	r3, .L18+24
	str	r0, [r3, #0]
	ldr	r0, .L18+28
	bl	atoi
	ldr	r3, .L18+32
	mov	r1, #0
	str	r0, [r3, #0]
	ldr	r3, .L18+36
	str	r1, [r3, #0]
.L17:
.LBE5:
	mov	r0, #13
	mov	r2, #1
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
	add	sp, sp, #52
	ldmfd	sp!, {pc}
.L19:
	.align	2
.L18:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_CommTestShowDebug
	.word	config_c+88
	.word	GuiVar_CommTestIPOctect1
	.word	GuiVar_CommTestIPOctect2
	.word	GuiVar_CommTestIPOctect3
	.word	GuiVar_CommTestIPOctect4
	.word	config_c+104
	.word	GuiVar_CommTestIPPort
	.word	GuiVar_CommTestSuppressConnections
.LFE2:
	.size	FDTO_COMM_TEST_draw_screen, .-FDTO_COMM_TEST_draw_screen
	.section	.text.COMM_TEST_process_screen,"ax",%progbits
	.align	2
	.global	COMM_TEST_process_screen
	.type	COMM_TEST_process_screen, %function
COMM_TEST_process_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #3
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	mov	r4, r0
	beq	.L22
	bhi	.L27
	cmp	r0, #1
	beq	.L23
	bhi	.L24
	b	.L22
.L27:
	cmp	r0, #80
	beq	.L25
	bhi	.L28
	cmp	r0, #4
	bne	.L21
	b	.L23
.L28:
	cmp	r0, #81
	beq	.L26
	cmp	r0, #84
	bne	.L21
	b	.L25
.L26:
	ldr	r3, .L44
	ldrsh	r3, [r3, #0]
	cmp	r3, #2
	bne	.L25
	bl	good_key_beep
	ldr	r0, .L44+4
	ldr	r1, .L44+8
	ldmfd	sp!, {r4, r5, lr}
	b	COMM_TEST_send_all_program_data_to_the_cloud
.L24:
	ldr	r5, .L44
	ldrsh	r3, [r5, #0]
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L31
.L37:
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L31
	.word	.L31
	.word	.L31
	.word	.L35
	.word	.L36
.L32:
	mov	r0, #40
	ldr	r1, .L44+12
	mov	r2, #49
	bl	ALERTS_parse_comm_command_string
	ldr	r0, .L44+16
	bl	Alert_Message
	ldr	r4, .L44+20
	b	.L38
.L33:
	mov	r0, #3
	ldr	r1, .L44+12
	mov	r2, #49
	bl	ALERTS_parse_comm_command_string
	ldr	r0, .L44+24
	bl	Alert_Message
	ldr	r4, .L44+28
	b	.L38
.L34:
	mov	r0, #68
	ldr	r1, .L44+12
	mov	r2, #49
	bl	ALERTS_parse_comm_command_string
	ldr	r0, .L44+32
	bl	Alert_Message
	ldr	r4, .L44+36
	b	.L38
.L35:
	mov	r0, #46
	ldr	r1, .L44+12
	mov	r2, #49
	bl	ALERTS_parse_comm_command_string
	ldr	r0, .L44+40
	bl	Alert_Message
	ldr	r4, .L44+44
	b	.L38
.L36:
	mov	r0, #57
	ldr	r1, .L44+12
	mov	r2, #49
	bl	ALERTS_parse_comm_command_string
	ldr	r0, .L44+48
	bl	Alert_Message
	mov	r4, #412
	b	.L38
.L31:
	bl	bad_key_beep
	mov	r4, #0
.L38:
	ldrsh	r3, [r5, #0]
	cmp	r3, #10
	beq	.L39
	ldr	r3, .L44+52
	ldr	r0, .L44+12
	ldr	r3, [r3, #0]
	cmp	r3, #3
	beq	.L40
	ldr	r1, .L44+56
	mov	r2, #49
	bl	strlcpy
	bl	Refresh_Screen
	ldr	r0, .L44+60
	bl	DIALOG_draw_ok_dialog
	b	.L39
.L40:
	ldr	r1, .L44+64
	mov	r2, #49
	bl	strlcpy
	bl	Refresh_Screen
	b	.L39
.L25:
	ldmfd	sp!, {r4, r5, lr}
	b	bad_key_beep
.L23:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Up
.L22:
	mov	r0, #1
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Down
.L21:
	cmp	r4, #67
	ldreq	r3, .L44+68
	moveq	r2, #11
	streq	r2, [r3, #0]
	mov	r0, r4
	bl	KEY_process_global_keys
	cmp	r4, #67
	ldmnefd	sp!, {r4, r5, pc}
	mov	r0, #1
	ldmfd	sp!, {r4, r5, lr}
	b	COMM_OPTIONS_draw_dialog
.L39:
	cmp	r4, #0
	ldmeqfd	sp!, {r4, r5, pc}
	bl	good_key_beep
	mov	r1, #0
	mov	r0, r4
	mov	r2, #512
	mov	r3, r1
	ldmfd	sp!, {r4, r5, lr}
	b	CONTROLLER_INITIATED_post_to_messages_queue
.L45:
	.align	2
.L44:
	.word	GuiLib_ActiveCursorFieldNo
	.word	1177
	.word	409
	.word	GuiVar_CommTestStatus
	.word	.LC1
	.word	414
	.word	.LC2
	.word	401
	.word	.LC3
	.word	415
	.word	.LC4
	.word	411
	.word	.LC5
	.word	cics
	.word	.LC6
	.word	587
	.word	.LC7
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	COMM_TEST_process_screen, .-COMM_TEST_process_screen
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Setting ALL Program Data bits\000"
.LC1:
	.ascii	"User-Initiated Check for Updates\000"
.LC2:
	.ascii	"User-Initiated Alerts\000"
.LC3:
	.ascii	"User-Initiated Program Data\000"
.LC4:
	.ascii	"User-Initiated Weather Data\000"
.LC5:
	.ascii	"User-Initiated Rain Indication\000"
.LC6:
	.ascii	"Waiting for device to connect...\000"
.LC7:
	.ascii	"Preparing to send...\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_comm_test.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x65
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x53
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x12c
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x140
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x166
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"COMM_TEST_process_screen\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_comm_test.c\000"
.LASF0:
	.ascii	"COMM_TEST_send_all_program_data_to_the_cloud\000"
.LASF5:
	.ascii	"COMM_TEST_copy_settings_into_guivars\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"FDTO_COMM_TEST_draw_screen\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
