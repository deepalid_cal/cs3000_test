	.file	"code_distribution_transmit.c"
	.text
.Ltext0:
	.section	.text.return_sn_for_this_item_in_the_hub_list,"ax",%progbits
	.align	2
	.type	return_sn_for_this_item_in_the_hub_list, %function
return_sn_for_this_item_in_the_hub_list:
.LFB3:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L9
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
	ldr	r8, .L9+4
	mov	r1, #0
	mvn	r2, #0
	mov	sl, r0
	ldr	r0, [r3, #0]
	mov	r3, r1
	bl	xQueueGenericReceive
	ldr	r0, [r8, #0]
	bl	uxQueueMessagesWaiting
	mov	r4, #0
	mov	r7, r4
	mov	r6, r4
	mov	r5, r4
	mov	fp, r8
	mov	r9, r0
	b	.L2
.L7:
	mov	r2, #0
	ldr	r0, [r8, #0]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericReceive
	cmp	r0, #0
	beq	.L3
	cmp	r7, #0
	bne	.L4
	ldr	r3, [sp, #0]
	cmn	r3, #1
	moveq	r7, #1
	beq	.L4
	add	r6, r6, #1
	cmp	r6, sl
	moveq	r4, r3
.L4:
	ldr	r3, [sp, #0]
	cmp	r3, #0
	bne	.L5
	ldr	r0, .L9+8
	bl	Alert_Message
.L5:
	mov	r2, #0
	ldr	r0, [fp, #0]
	mov	r1, sp
	mov	r3, r2
	bl	xQueueGenericSend
	b	.L6
.L3:
	ldr	r0, .L9+12
	bl	Alert_Message
.L6:
	add	r5, r5, #1
.L2:
	cmp	r5, r9
	bne	.L7
	ldr	r3, .L9
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericSend
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L10:
	.align	2
.L9:
	.word	router_hub_list_MUTEX
	.word	router_hub_list_queue
	.word	.LC2
	.word	.LC3
.LFE3:
	.size	return_sn_for_this_item_in_the_hub_list, .-return_sn_for_this_item_in_the_hub_list
	.section	.text.transmission_completed_clear_hub_distribution_flag_and_perform_a_restart,"ax",%progbits
	.align	2
	.type	transmission_completed_clear_hub_distribution_flag_and_perform_a_restart, %function
transmission_completed_clear_hub_distribution_flag_and_perform_a_restart:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L17
	ldr	r3, [r2, #0]
	cmp	r3, #6
	ldreq	r3, .L17+4
	moveq	r1, #0
	streq	r1, [r3, #140]
	beq	.L13
.L12:
	cmp	r3, #7
	ldreq	r3, .L17+4
	moveq	r1, #0
	streq	r1, [r3, #136]
.L13:
	ldr	r3, [r2, #240]
	cmp	r3, #21
	moveq	r0, #34
	beq	.L16
	cmp	r3, #31
	bxne	lr
	mov	r0, #35
.L16:
	b	SYSTEM_application_requested_restart
.L18:
	.align	2
.L17:
	.word	cdcs
	.word	weather_preserves
.LFE5:
	.size	transmission_completed_clear_hub_distribution_flag_and_perform_a_restart, .-transmission_completed_clear_hub_distribution_flag_and_perform_a_restart
	.section	.text.build_and_send_a_hub_distribution_query_for_this_3000_sn.part.0,"ax",%progbits
	.align	2
	.type	build_and_send_a_hub_distribution_query_for_this_3000_sn.part.0, %function
build_and_send_a_hub_distribution_query_for_this_3000_sn.part.0:
.LFB8:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	ldr	r1, .L26
	sub	sp, sp, #28
.LCFI2:
	mov	r6, r0
	mov	r2, #640
	mov	r0, #32
	bl	mem_malloc_debug
	mov	r1, #0
	mov	r2, #12
	mov	r5, #0
	mov	r4, r0
	mov	r0, sp
	bl	memset
	ldrb	r3, [sp, #0]	@ zero_extendqisi2
	strh	r6, [sp, #2]	@ movhi
	orr	r3, r3, #30
	mov	r6, r6, lsr #16
	strb	r3, [sp, #0]
	strh	r6, [sp, #4]	@ movhi
	mov	r3, #14
	ldr	r6, .L26+4
	strb	r3, [sp, #1]
	mov	r1, sp
	mov	r3, #24
	mov	r2, #12
	mov	r0, r4
	strh	r3, [sp, #10]	@ movhi
	strh	r5, [sp, #6]	@ movhi
	strh	r5, [sp, #8]	@ movhi
	bl	memcpy
	ldr	r1, .L26+8
	mov	r2, #4
	add	r0, r4, #12
	bl	memcpy
	ldr	r3, [r6, #0]
	add	r1, sp, #28
	cmp	r3, #6
	movne	r3, #9
	moveq	r3, #8
	str	r3, [r1, #-16]!
	add	r0, r4, #16
	mov	r2, #4
	bl	memcpy
	ldr	r3, [r6, #0]
	add	r7, r4, #20
	cmp	r3, #6
	bne	.L21
	ldr	r1, .L26+12
	mov	r0, #-2147483648
	bl	convert_code_image_date_to_version_number
	ldr	r1, .L26+12
	str	r0, [sp, #16]
	mov	r0, #-2147483648
	bl	convert_code_image_time_to_edit_count
	add	r1, sp, #16
	mov	r2, #4
	str	r0, [sp, #20]
	mov	r0, r7
	bl	memcpy
	add	r0, r4, #24
	add	r1, sp, #20
	b	.L25
.L21:
	cmp	r3, #7
	bne	.L23
	ldr	r1, .L26+16
	mov	r0, r7
	mov	r2, #4
	bl	memcpy
	ldr	r1, .L26+20
	add	r0, r4, #24
.L25:
	mov	r2, #4
	bl	memcpy
	add	r7, r4, #28
	mov	r5, #1
	b	.L22
.L23:
	ldr	r0, .L26+24
	bl	Alert_Message
.L22:
	mov	r0, r4
	mov	r1, #28
	bl	CRC_calculate_32bit_big_endian
	add	r1, sp, #28
	mov	r2, #4
	str	r0, [r1, #-4]!
	mov	r0, r7
	bl	memcpy
	mov	r1, r4
	mov	r3, #0
	mov	r0, #2
	mov	r2, #32
	bl	attempt_to_copy_packet_and_route_out_another_port
	mov	r0, r4
	ldr	r1, .L26
	ldr	r2, .L26+28
	bl	mem_free_debug
	mov	r0, r5
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L27:
	.align	2
.L26:
	.word	.LC4
	.word	cdcs
	.word	config_c+48
	.word	CS3000_APP_FILENAME
	.word	tpmicro_comm+80
	.word	tpmicro_comm+84
	.word	.LC5
	.word	741
.LFE8:
	.size	build_and_send_a_hub_distribution_query_for_this_3000_sn.part.0, .-build_and_send_a_hub_distribution_query_for_this_3000_sn.part.0
	.section	.text.code_distribution_packet_rate_timer_callback,"ax",%progbits
	.align	2
	.global	code_distribution_packet_rate_timer_callback
	.type	code_distribution_packet_rate_timer_callback, %function
code_distribution_packet_rate_timer_callback:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L29
	b	CODE_DISTRIBUTION_post_event
.L30:
	.align	2
.L29:
	.word	4471
.LFE0:
	.size	code_distribution_packet_rate_timer_callback, .-code_distribution_packet_rate_timer_callback
	.global	__udivsi3
	.section	.text.build_and_send_code_transmission_init_packet,"ax",%progbits
	.align	2
	.global	build_and_send_code_transmission_init_packet
	.type	build_and_send_code_transmission_init_packet, %function
build_and_send_code_transmission_init_packet:
.LFB2:
	@ args = 0, pretend = 0, frame = 108
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI3:
	ldr	r4, .L59
	subs	r5, r0, #0
	sub	sp, sp, #112
.LCFI4:
	bne	.L32
	ldr	r3, [r4, #232]
	ldr	r7, [r4, #0]
	cmp	r3, #48
	movne	r7, r5
	bne	.L33
	sub	r7, r7, #6
	cmp	r7, #1
	movhi	r7, #0
	movls	r7, #1
.L33:
	ldr	r0, [r4, #204]
	ldr	r1, [r4, #228]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, [r4, #224]
	ldr	r6, .L59
	cmp	r0, r3
	str	r0, [sp, #100]
	bne	.L53
	cmp	r7, #0
	beq	.L53
	ldr	r1, [r6, #220]
	ldr	r3, [r6, #212]
	cmp	r1, r3, asl #1
	bls	.L35
	ldr	r0, .L59+4
	bl	Alert_Message_va
	ldr	r3, [r6, #0]
	mov	r2, #0
	cmp	r3, #6
	ldr	r3, .L59+8
	streq	r2, [r3, #140]
	strne	r2, [r3, #136]
	b	.L53
.L32:
	ldr	r2, [r4, #0]
	ldr	r3, [r5, #8]
	sub	r2, r2, #4
	cmp	r2, #3
	movhi	r3, #0
	bhi	.L37
	adds	r3, r3, #0
	movne	r3, #1
.L37:
	ldr	r2, [r4, #232]
	cmp	r2, #32
	bne	.L53
	cmp	r3, #0
	bne	.L35
.L53:
	mov	r0, #36
	bl	SYSTEM_application_requested_restart
	b	.L31
.L35:
	ldr	r3, .L59
	ldr	r2, [r3, #232]
	cmp	r2, #48
	bne	.L39
	ldr	r3, [r3, #0]
	cmp	r3, #6
	addeq	r0, sp, #4
	moveq	r1, #64
	ldreq	r2, .L59+12
	beq	.L56
.L40:
	cmp	r3, #7
	bne	.L41
	add	r0, sp, #4
	mov	r1, #64
	ldr	r2, .L59+16
	b	.L56
.L39:
	ldr	r2, [r5, #12]
	ldr	r1, [r5, #8]
	ldr	r4, .L59
	str	r1, [r3, #204]
	add	r1, r1, r2
	str	r2, [r3, #228]
	str	r1, [r3, #208]
	mov	r1, r2, lsr #11
	movs	r2, r2, asl #21
	str	r1, [r3, #212]
	addne	r1, r1, #1
	strne	r1, [r3, #212]
	ldr	r5, [r4, #212]
.LBB10:
	ldr	r1, .L59+20
	mov	r2, #8
	add	r0, sp, #92
	bl	memcpy
	add	r0, r4, #12
	mov	r1, #0
	mov	r2, #128
	bl	memset
	add	r4, r4, #11
	mov	r1, r5, lsr #3
	mov	r3, #0
	and	r5, r5, #7
	mvn	r2, #0
	b	.L43
.L44:
	add	r3, r3, #1
	strb	r2, [r4, #1]!
.L43:
	cmp	r3, r1
	bne	.L44
	cmp	r5, #0
	addne	r2, sp, #112
	ldr	r6, .L59
	addne	r5, r2, r5
	ldrneb	r2, [r5, #-20]	@ zero_extendqisi2
	addne	r3, r6, r3
	strneb	r2, [r3, #12]
.LBE10:
	mov	r3, #0
	str	r3, [r6, #220]
	ldr	r0, [r6, #204]
	ldr	r1, [r6, #228]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L59+24
	mov	r2, #30
	str	r2, [r3, #0]
	ldr	r3, [r6, #0]
	ldr	r4, .L59
	cmp	r3, #4
	str	r0, [r6, #224]
	bne	.L46
	mov	r3, #12
	str	r3, [r4, #200]
	mov	r3, #20
	str	r3, [r4, #236]
	mov	r3, #21
	str	r3, [r4, #240]
	add	r0, sp, #4
	mov	r1, #64
	ldr	r2, .L59+28
	b	.L57
.L46:
	cmp	r3, #5
	bne	.L47
	str	r2, [r4, #236]
	ldr	r2, .L59+32
	mov	r3, #12
	str	r3, [r4, #200]
	add	r0, sp, #4
	mov	r3, #31
	mov	r1, #64
	str	r3, [r4, #240]
.L57:
	bl	snprintf
	mov	r0, #1
	bl	CODE_DOWNLOAD_draw_dialog
	b	.L41
.L47:
	cmp	r3, #6
	bne	.L48
	mov	r3, #13
	str	r3, [r4, #200]
	mov	r3, #20
	str	r3, [r4, #236]
	mov	r3, #21
	str	r3, [r4, #240]
	add	r0, sp, #4
	mov	r1, #64
	ldr	r2, .L59+36
	b	.L56
.L48:
	cmp	r3, #7
	bne	.L41
	str	r2, [r4, #236]
	ldr	r2, .L59+40
	mov	r3, #13
	str	r3, [r4, #200]
	add	r0, sp, #4
	mov	r3, #31
	mov	r1, #64
	str	r3, [r4, #240]
.L56:
	bl	snprintf
.L41:
	ldr	r5, .L59
	ldr	r0, .L59+44
	add	r1, sp, #4
	ldr	r2, [r5, #228]
	ldr	r3, [r5, #212]
	bl	Alert_Message_va
	mov	r3, #64
	str	r3, [r5, #232]
	mov	r3, #1
	str	r3, [r5, #216]
	ldr	r1, .L59+48
	ldr	r2, .L59+52
	mov	r0, #36
	bl	mem_malloc_debug
	ldrb	r3, [sp, #68]	@ zero_extendqisi2
	add	r1, sp, #68
	orr	r3, r3, #30
	strb	r3, [sp, #68]
	ldr	r3, [r5, #200]
	mov	r2, #12
	strb	r3, [sp, #69]
	mov	r3, #0
	strh	r3, [sp, #70]	@ movhi
	strh	r3, [sp, #72]	@ movhi
	strh	r3, [sp, #74]	@ movhi
	strh	r3, [sp, #76]	@ movhi
	ldr	r3, [r5, #236]
	strh	r3, [sp, #78]	@ movhi
	mov	r4, r0
	bl	memcpy
	ldr	r3, [r5, #224]
	add	r0, r4, #12
	str	r3, [sp, #84]
	ldr	r3, [r5, #228]
	add	r1, sp, #80
	str	r3, [sp, #80]
	ldr	r3, [r5, #212]
	mov	r2, #12
	strh	r3, [sp, #88]	@ movhi
	ldr	r3, [r5, #236]
	add	r6, r4, #24
	strh	r3, [sp, #90]	@ movhi
	bl	memcpy
	ldr	r3, [r5, #236]
	cmp	r3, #20
	bne	.L49
	ldr	r1, .L59+56
	mov	r0, #-2147483648
	bl	convert_code_image_date_to_version_number
	ldr	r1, .L59+56
	str	r0, [sp, #104]
	mov	r0, #-2147483648
	bl	convert_code_image_time_to_edit_count
	add	r1, sp, #104
	mov	r2, #4
	str	r0, [sp, #108]
	mov	r0, r6
	bl	memcpy
	add	r0, r4, #28
	add	r1, sp, #108
	b	.L58
.L49:
	cmp	r3, #30
	bne	.L50
	ldr	r1, .L59+60
	mov	r0, r6
	mov	r2, #4
	bl	memcpy
	ldr	r1, .L59+64
	add	r0, r4, #28
.L58:
	mov	r2, #4
	bl	memcpy
	add	r6, r4, #32
.L50:
	mov	r1, #32
	mov	r0, r4
	bl	CRC_calculate_32bit_big_endian
	ldr	r5, .L59
	add	r1, sp, #112
	mov	r2, #4
	str	r0, [r1, #-12]!
	mov	r0, r6
	bl	memcpy
	ldr	r3, [r5, #200]
	cmp	r3, #12
	bne	.L51
	mov	r0, r4
	mov	r1, #36
	mov	r2, #0
	bl	PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain
	ldr	r0, [r5, #248]
	b	.L52
.L51:
	mov	r0, #2
	mov	r1, r4
	mov	r2, #36
	mov	r3, #0
	bl	attempt_to_copy_packet_and_route_out_another_port
	mov	r0, #2
	mov	r1, r4
	mov	r2, #36
	mov	r3, #0
	bl	attempt_to_copy_packet_and_route_out_another_port
	ldr	r0, [r5, #252]
.L52:
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L59
	mov	r1, #2
	mov	r2, r0
	ldr	r0, [r3, #260]
	mov	r3, #0
	bl	xTimerGenericCommand
	mov	r0, r4
	ldr	r1, .L59+48
	ldr	r2, .L59+68
	bl	mem_free_debug
.L31:
	add	sp, sp, #112
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L60:
	.align	2
.L59:
	.word	cdcs
	.word	.LC6
	.word	weather_preserves
	.word	.LC7
	.word	.LC8
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	.LC13
	.word	.LC4
	.word	381
	.word	CS3000_APP_FILENAME
	.word	tpmicro_comm+80
	.word	tpmicro_comm+84
	.word	499
.LFE2:
	.size	build_and_send_code_transmission_init_packet, .-build_and_send_code_transmission_init_packet
	.global	__umodsi3
	.section	.text.perform_next_code_transmission_state_activity,"ax",%progbits
	.align	2
	.global	perform_next_code_transmission_state_activity
	.type	perform_next_code_transmission_state_activity, %function
perform_next_code_transmission_state_activity:
.LFB7:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI5:
	ldr	r4, .L135
	sub	sp, sp, #40
.LCFI6:
	ldr	r5, [r4, #232]
	cmp	r5, #82
	beq	.L66
	bhi	.L70
	cmp	r5, #80
	beq	.L64
	bhi	.L65
	cmp	r5, #64
	bne	.L62
	b	.L129
.L70:
	cmp	r5, #112
	beq	.L68
	cmp	r5, #128
	beq	.L69
	cmp	r5, #96
	bne	.L62
	b	.L130
.L129:
	ldr	r3, [r4, #200]
	cmp	r3, #13
	bne	.L121
	b	.L122
.L75:
.LBB11:
	add	r0, sp, #20
	ldr	r1, .L135+4
	mov	r2, #8
	bl	memcpy
	cmp	r5, #0
	beq	.L73
	add	r2, sp, #40
	and	r3, r5, #7
	add	r3, r2, r3
	sub	r2, r5, #1
	add	r2, r4, r2, lsr #3
	ldrb	r3, [r3, #-20]	@ zero_extendqisi2
	ldrb	r2, [r2, #12]	@ zero_extendqisi2
	and	r2, r3, r2
	cmp	r2, r3
	beq	.L74
.L73:
.LBE11:
	add	r5, r5, #1
	str	r5, [r4, #216]
.L122:
	ldr	r5, [r4, #216]
	ldr	r3, [r4, #212]
	cmp	r5, r3
	bls	.L75
	b	.L131
.L121:
	ldr	r2, [r4, #216]
	ldr	r3, [r4, #212]
	cmp	r2, r3
	bhi	.L77
	b	.L74
.L131:
	ldr	r3, .L135
	ldr	r3, [r3, #240]
	cmp	r3, #21
	bne	.L124
	b	.L132
.L133:
	ldr	r7, [r3, #228]
	mov	r7, r7, asl #21
	cmp	r7, #0
	movne	r7, r7, lsr #21
	moveq	r7, #2048
.L80:
	add	r8, r7, #20
	ldr	r1, .L135+8
	ldr	r2, .L135+12
	mov	r0, r8
	bl	mem_malloc_debug
	ldrb	r3, [sp, #8]	@ zero_extendqisi2
	ldr	r4, .L135
	orr	r3, r3, #30
	strb	r3, [sp, #8]
	ldr	r3, [r4, #200]
	mov	r6, #0
	strb	r3, [sp, #9]
	strh	r6, [sp, #10]	@ movhi
	strh	r6, [sp, #12]	@ movhi
	strh	r6, [sp, #14]	@ movhi
	strh	r6, [sp, #16]	@ movhi
	ldr	r3, [r4, #240]
	add	r1, sp, #8
	mov	r2, #12
	strh	r3, [sp, #18]	@ movhi
	mov	r5, r0
	bl	memcpy
	ldr	r3, [r4, #216]
	add	r1, sp, #28
	strh	r3, [sp, #28]	@ movhi
	ldr	r3, [r4, #240]
	mov	r2, #4
	add	r0, r5, #12
	strh	r3, [sp, #30]	@ movhi
	bl	memcpy
	ldr	r3, [r4, #216]
	ldr	r1, [r4, #204]
	mov	r3, r3, asl #11
	sub	r3, r3, #2048
	add	sl, r5, #16
	add	r1, r1, r3
	mov	r2, r7
	mov	r0, sl
	bl	memcpy
	ldr	r3, [r4, #216]
	add	r1, r7, #16
	add	r3, r3, #1
	str	r3, [r4, #216]
	mov	r0, r5
	bl	CRC_calculate_32bit_big_endian
	add	sl, sl, r7
	add	r1, sp, #40
	mov	r2, #4
	str	r0, [r1, #-8]!
	mov	r0, sl
	bl	memcpy
	ldr	r1, [r4, #200]
	cmp	r1, #12
	bne	.L81
	mov	r0, r5
	mov	r1, r8
	mov	r2, r6
	bl	PACKET_ROUTER_broadcast_SCAN_TOKEN_CODE_packet_to_other_controllers_on_our_chain
	ldr	r4, [r4, #248]
	b	.L82
.L81:
	cmp	r1, #13
	bne	.L83
	mov	r2, r8
	mov	r3, r6
	mov	r0, #2
	mov	r1, r5
	bl	attempt_to_copy_packet_and_route_out_another_port
	ldr	r3, .L135+16
	ldr	r6, [r4, #252]
	ldr	r3, [r3, #0]
	cmp	r3, #60
	ldrhi	r2, .L135+20
	subhi	r4, r6, #68608
	subhi	r4, r4, #392
	movls	r4, r6
	mlahi	r4, r2, r3, r4
	ldr	r3, .L135+24
	ldr	r3, [r3, #84]
	cmp	r3, #1
	bne	.L82
	ldr	r7, .L135
	mov	r1, #10
	ldr	r0, [r7, #220]
	bl	__umodsi3
	cmp	r0, #0
	moveq	r3, #80
	streq	r3, [r7, #232]
	addeq	r4, r4, r6
	b	.L82
.L83:
	ldr	r0, .L135+28
	bl	Alert_Message_va
	mov	r4, #1000
.L82:
	mov	r0, r5
	ldr	r1, .L135+8
	ldr	r2, .L135+32
	bl	mem_free_debug
	b	.L85
.L132:
	ldr	r0, .L135+36
	b	.L126
.L124:
	cmp	r3, #31
	ldreq	r0, .L135+40
	bne	.L88
	b	.L126
.L134:
	ldr	r0, .L135+44
	b	.L126
.L125:
	cmp	r3, #31
	ldreq	r0, .L135+48
	beq	.L126
.L88:
	ldr	r0, .L135+52
.L126:
	ldr	r4, .L135
	bl	Alert_Message
	ldr	r3, [r4, #200]
	cmp	r3, #12
	bne	.L89
	mov	r0, #1
	bl	CODE_DOWNLOAD_close_dialog
	ldr	r0, [r4, #204]
	ldr	r1, .L135+8
	mov	r2, #1136
	bl	mem_free_debug
.L89:
	mov	r3, #96
	b	.L128
.L64:
	ldr	r6, .L135+56
	mov	r5, #2
	mov	r1, #300
	mov	r2, r1
	mov	r0, r5
	str	r5, [r6, #68]
	bl	RCVD_DATA_enable_hunting_mode
	ldr	r7, .L135+60
	ldr	r3, .L135+64
	ldr	r0, .L135+68
	str	r0, [r7, r3]
	bl	strlen
	ldr	r3, .L135+72
	mov	r2, #12
	str	r0, [r7, r3]
	add	r3, r3, #4
	str	r2, [r7, r3]
	ldr	r0, .L135+76
	bl	strlen
	mov	r3, #1
	str	r3, [sp, #4]
	ldr	r1, .L135+76
	mov	r3, #0
	str	r5, [sp, #0]
	mov	r2, r0
	mov	r0, r5
	bl	AddCopyOfBlockToXmitList
	mov	r3, #81
	str	r3, [r4, #232]
	str	r5, [r6, #72]
	mov	r4, #2000
	b	.L85
.L65:
	ldr	r2, [r0, #0]
	ldr	r3, .L135+80
	cmp	r2, r3
	bne	.L90
	mov	r1, #500
	mov	r2, #300
	mov	r0, #2
	bl	RCVD_DATA_enable_hunting_mode
	ldr	r6, .L135+60
	ldr	r3, .L135+84
	ldr	r0, .L135+88
	mov	r5, #0
	str	r0, [r6, r3]
	bl	strlen
	ldr	r3, .L135+92
	str	r0, [r6, r3]
	sub	r3, r3, #8
	str	r5, [r6, r3]
	ldr	r0, .L135+96
	bl	strlen
	mov	r3, #1
	ldr	r1, .L135+96
	mov	r2, r0
	mov	r0, #2
	stmia	sp, {r0, r3}
	mov	r3, r5
	bl	AddCopyOfBlockToXmitList
	mov	r3, #82
.L128:
	str	r3, [r4, #232]
	b	.L118
.L90:
	ldr	r0, .L135+100
	bl	Alert_Message
	mov	r3, #82
	str	r3, [r4, #232]
	mov	r4, #50
	b	.L85
.L66:
	ldr	r2, [r0, #0]
	ldr	r3, .L135+104
	cmp	r2, r3
	bne	.L91
	ldr	r3, [r0, #8]
	ldrb	r2, [r3, #6]	@ zero_extendqisi2
	strb	r2, [sp, #36]
	ldrb	r2, [r3, #7]	@ zero_extendqisi2
	strb	r2, [sp, #37]
	ldr	r2, [r0, #12]
	add	r0, sp, #36
	cmp	r2, #16
	ldreqb	r3, [r3, #8]	@ zero_extendqisi2
	mov	r2, #0
	streqb	r3, [sp, #38]
	strneb	r2, [sp, #38]
	strb	r2, [sp, #39]
	bl	atol
	ldr	r3, .L135+16
	str	r0, [r3, #0]
	b	.L94
.L91:
	ldr	r0, .L135+108
	bl	Alert_Message
.L94:
	ldr	r0, .L135+112
	bl	strlen
	mov	r4, #2
	mov	r3, #1
	str	r3, [sp, #4]
	ldr	r1, .L135+112
	mov	r3, #0
	str	r4, [sp, #0]
	mov	r2, r0
	mov	r0, r4
	bl	AddCopyOfBlockToXmitList
	mov	r0, r4
	mov	r2, #0
	mov	r1, #100
	bl	RCVD_DATA_enable_hunting_mode
	ldr	r3, .L135
	mov	r2, #64
	str	r2, [r3, #232]
	mov	r4, #500
	b	.L85
.L130:
	ldr	r3, .L135+24
	ldr	r3, [r3, #84]
	cmp	r3, #0
	moveq	r2, #1
	beq	.L95
	ldr	r3, .L135+116
	ldr	r2, [r3, #56]
	rsbs	r2, r2, #1
	movcc	r2, #0
.L95:
	ldr	r3, .L135
	ldr	r1, [r3, #200]
	cmp	r1, #12
	bne	.L96
	ldr	r1, .L135+116
	ldr	r1, [r1, #8]
	cmp	r1, #0
	bne	.L118
.L96:
	cmp	r2, #0
	movne	r2, #0
	strne	r2, [r3, #244]
	movne	r2, #112
	strne	r2, [r3, #232]
	b	.L118
.L68:
	ldr	r3, [r4, #200]
	cmp	r3, #13
	ldr	r3, [r4, #244]
	bne	.L97
	cmp	r3, #0
	bne	.L116
	ldr	r0, .L135+120
	bl	Alert_Message
	b	.L116
.L97:
	cmp	r3, #0
	bne	.L117
	ldr	r0, .L135+124
	bl	Alert_Message
	b	.L117
.L116:
	ldr	r3, .L135+128
	b	.L98
.L117:
	ldr	r3, .L135+132
.L98:
	ldr	r4, .L135
	ldr	r2, [r4, #244]
	add	r2, r2, #1000
	cmp	r2, r3
	str	r2, [r4, #244]
	bcc	.L118
	ldr	r3, [r4, #200]
	cmp	r3, #12
	beq	.L107
.L99:
	cmp	r3, #13
	bne	.L100
	mov	r3, #1
	str	r3, [r4, #256]
	mov	r2, #128
	mov	r1, #0
	add	r0, r4, #12
	bl	memset
	ldr	r0, [r4, #256]
	bl	return_sn_for_this_item_in_the_hub_list
	mov	r1, r0
	ldr	r0, .L135+136
	bl	Alert_Message_va
	ldr	r0, [r4, #256]
	bl	return_sn_for_this_item_in_the_hub_list
.LBB12:
	cmp	r0, #0
	beq	.L107
	bl	build_and_send_a_hub_distribution_query_for_this_3000_sn.part.0
.LBE12:
	cmp	r0, #0
	movne	r3, #128
	strne	r3, [r4, #232]
	bne	.L127
	b	.L107
.L100:
	ldr	r0, .L135+140
	bl	Alert_Message
	b	.L118
.L69:
	ldr	r0, [r4, #256]
	bl	return_sn_for_this_item_in_the_hub_list
.LBB13:
	cmp	r0, #0
	beq	.L102
	bl	build_and_send_a_hub_distribution_query_for_this_3000_sn.part.0
.LBE13:
	cmp	r0, #0
	beq	.L102
	ldr	r0, [r4, #256]
	bl	return_sn_for_this_item_in_the_hub_list
	mov	r1, r0
	ldr	r0, .L135+136
	bl	Alert_Message_va
	str	r5, [r4, #232]
.L127:
	ldr	r3, [r4, #256]
	add	r3, r3, #1
	str	r3, [r4, #256]
	ldr	r4, [r4, #252]
	mov	r4, r4, asl #1
	b	.L85
.L102:
	ldr	r0, .L135+144
	bl	Alert_Message
	ldr	r2, .L135+148
	mov	r4, #0
	mov	r3, r4
.L104:
	ldrb	r1, [r2, #1]!	@ zero_extendqisi2
	add	r3, r3, #1
	cmp	r1, #0
	movne	r4, #1
	cmp	r3, #128
	bne	.L104
	cmp	r4, #0
	ldr	r5, .L135
	beq	.L105
	ldr	r0, .L135+152
	bl	Alert_Message
	mov	r3, #48
	str	r3, [r5, #232]
	mov	r0, #0
	bl	build_and_send_code_transmission_init_packet
	b	.L118
.L105:
	ldr	r0, .L135+156
	bl	Alert_Message
	ldr	r3, [r5, #240]
	cmp	r3, #21
	ldreq	r3, .L135+160
	streq	r4, [r3, #140]
	beq	.L107
	cmp	r3, #31
	ldreq	r3, .L135+160
	streq	r4, [r3, #136]
.L107:
	bl	transmission_completed_clear_hub_distribution_flag_and_perform_a_restart
	b	.L118
.L62:
	ldr	r0, .L135+164
	mov	r1, r5
	bl	Alert_Message_va
.L118:
	mov	r4, #1000
.L85:
	mov	r0, r4
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L135
	mov	r1, #2
	mov	r2, r0
	ldr	r0, [r3, #260]
	mov	r3, #0
	bl	xTimerGenericCommand
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L74:
	ldr	r3, .L135
	ldr	r2, [r3, #220]
	ldr	r1, [r3, #216]
	add	r2, r2, #1
	str	r2, [r3, #220]
	ldr	r2, [r3, #212]
	cmp	r1, r2
	movne	r7, #2048
	bne	.L80
	b	.L133
.L77:
	ldr	r3, [r4, #240]
	cmp	r3, #21
	bne	.L125
	b	.L134
.L136:
	.align	2
.L135:
	.word	cdcs
	.word	.LANCHOR0+8
	.word	.LC4
	.word	962
	.word	.LANCHOR1
	.word	1150
	.word	config_c
	.word	.LC14
	.word	1087
	.word	.LC15
	.word	.LC16
	.word	.LC18
	.word	.LC19
	.word	.LC17
	.word	1073905664
	.word	SerDrvrVars_s
	.word	12756
	.word	.LANCHOR2
	.word	12760
	.word	.LANCHOR3
	.word	4556
	.word	12776
	.word	.LANCHOR4
	.word	12780
	.word	.LANCHOR5
	.word	.LC20
	.word	4590
	.word	.LC21
	.word	.LANCHOR6
	.word	xmit_cntrl
	.word	.LC22
	.word	.LC23
	.word	90000
	.word	60000
	.word	.LC24
	.word	.LC25
	.word	.LC26
	.word	cdcs+11
	.word	.LC27
	.word	.LC28
	.word	weather_preserves
	.word	.LC29
.LFE7:
	.size	perform_next_code_transmission_state_activity, .-perform_next_code_transmission_state_activity
	.global	m7_exit_str
	.global	m7_temp_response_str
	.global	m7_read_temp_str
	.global	m7_plus_plus_plus_str
	.global	m7_ok_response_str
	.global	m7_current_temp
	.section .rodata
	.set	.LANCHOR0,. + 0
.LC1:
	.byte	0
	.byte	1
	.byte	3
	.byte	7
	.byte	15
	.byte	31
	.byte	63
	.byte	127
.LC0:
	.byte	-128
	.byte	1
	.byte	2
	.byte	4
	.byte	8
	.byte	16
	.byte	32
	.byte	64
	.section	.rodata.m7_ok_response_str,"a",%progbits
	.set	.LANCHOR2,. + 0
	.type	m7_ok_response_str, %object
	.size	m7_ok_response_str, 9
m7_ok_response_str:
	.ascii	"\015\012OK\015\012\015\012\000"
	.section	.rodata.m7_plus_plus_plus_str,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	m7_plus_plus_plus_str, %object
	.size	m7_plus_plus_plus_str, 4
m7_plus_plus_plus_str:
	.ascii	"+++\000"
	.section	.bss.m7_current_temp,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	m7_current_temp, %object
	.size	m7_current_temp, 4
m7_current_temp:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC2:
	.ascii	"Hub List contains a 0 entry!\000"
.LC3:
	.ascii	"HUB: unexp end of list\000"
.LC4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/code_distribution_transmit.c\000"
.LC5:
	.ascii	"Code Distribution Error: unexpected mode\000"
.LC6:
	.ascii	"Code Distribution exhausted: %u packets sent\000"
.LC7:
	.ascii	"MAIN CODE: re-starting hub distribution\000"
.LC8:
	.ascii	"TPMICRO CODE: re-starting hub distribution\000"
.LC9:
	.ascii	"MAIN CODE: starting flowsense distribution\000"
.LC10:
	.ascii	"TPMICRO CODE: starting flowsense distribution\000"
.LC11:
	.ascii	"MAIN CODE: starting hub distribution\000"
.LC12:
	.ascii	"TPMICRO CODE: starting hub distribution\000"
.LC13:
	.ascii	"%s %d bytes (%d packets)\000"
.LC14:
	.ascii	"CODE: unexpd class %u\000"
.LC15:
	.ascii	"MAIN CODE: all packets sent from hub\000"
.LC16:
	.ascii	"TPMICRO CODE :  all packets sent from hub\000"
.LC17:
	.ascii	"UPDATE: mid error\000"
.LC18:
	.ascii	"MAIN CODE: completed distribution\000"
.LC19:
	.ascii	"TPMICRO CODE: completed distribution\000"
.LC20:
	.ascii	"RAVEON: failed to enter command mode\000"
.LC21:
	.ascii	"RAVEON: temp read error\000"
.LC22:
	.ascii	"HUB waiting 90 seconds to start query\000"
.LC23:
	.ascii	"FLOWSENSE waiting 60 seconds before reboot\000"
.LC24:
	.ascii	"CODE: query sn %u\000"
.LC25:
	.ascii	"UNEXP CODE DISTRIB CLASS\000"
.LC26:
	.ascii	"CODE: query done\000"
.LC27:
	.ascii	"CODE: hub needs to re-transmit some\000"
.LC28:
	.ascii	"CODE: hub sees all received ok\000"
.LC29:
	.ascii	"CODE DISTRIB: unexpd transmit state: %u\000"
	.section	.rodata.m7_exit_str,"a",%progbits
	.set	.LANCHOR6,. + 0
	.type	m7_exit_str, %object
	.size	m7_exit_str, 6
m7_exit_str:
	.ascii	"atcn\015\000"
	.section	.rodata.m7_temp_response_str,"a",%progbits
	.set	.LANCHOR4,. + 0
	.type	m7_temp_response_str, %object
	.size	m7_temp_response_str, 7
m7_temp_response_str:
	.ascii	"\015\012OK\015\012\000"
	.section	.rodata.m7_read_temp_str,"a",%progbits
	.set	.LANCHOR5,. + 0
	.type	m7_read_temp_str, %object
	.size	m7_read_temp_str, 6
m7_read_temp_str:
	.ascii	"atte\015\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI1-.LFB8
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x84
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI5-.LFB7
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/communication/code_distribution_transmit.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xac
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x261
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x52
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x311
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x1f8
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x2f3
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0x49
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0x8a
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x347
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB8
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI2
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x3
	.byte	0x7d
	.sleb128 132
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB7
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI6
	.4byte	.LFE7
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"set_transmit_bitfield_bits_for_this_many_packets\000"
.LASF6:
	.ascii	"build_and_send_code_transmission_init_packet\000"
.LASF2:
	.ascii	"this_transmit_bit_is_set\000"
.LASF5:
	.ascii	"code_distribution_packet_rate_timer_callback\000"
.LASF7:
	.ascii	"perform_next_code_transmission_state_activity\000"
.LASF3:
	.ascii	"return_sn_for_this_item_in_the_hub_list\000"
.LASF4:
	.ascii	"transmission_completed_clear_hub_distribution_flag_"
	.ascii	"and_perform_a_restart\000"
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/comm"
	.ascii	"unication/code_distribution_transmit.c\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"build_and_send_a_hub_distribution_query_for_this_30"
	.ascii	"00_sn\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
