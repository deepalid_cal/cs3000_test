	.file	"budgets.c"
	.text
.Ltext0:
	.section	.text.nm_handle_budget_reset_record,"ax",%progbits
	.align	2
	.type	nm_handle_budget_reset_record, %function
nm_handle_budget_reset_record:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r2, #472
	ldr	r3, .L2
	mul	r2, r0, r2
	add	r1, r3, r2
	ldr	ip, [r1, #44]
	add	r2, r2, #380
	add	ip, ip, #14080
	mov	r1, #0
	add	r0, r3, r2
	mov	r2, #96
	str	r1, [ip, #72]
	str	r1, [ip, #76]
	str	r1, [ip, #84]
	b	memset
.L3:
	.align	2
.L2:
	.word	poc_preserves
.LFE1:
	.size	nm_handle_budget_reset_record, .-nm_handle_budget_reset_record
	.section	.text.nm_BUDGET_get_poc_usage,"ax",%progbits
	.align	2
	.type	nm_BUDGET_get_poc_usage, %function
nm_BUDGET_get_poc_usage:
.LFB10:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L8
	stmfd	sp!, {r0, r4, r5, r6, r7, lr}
.LCFI0:
	ldr	r2, .L8+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L8+8
	bl	xQueueTakeMutexRecursive_debug
	bl	nm_POC_REPORT_RECORDS_get_most_recently_completed_record
	mov	r7, #0
	mov	r6, r0
	b	.L5
.L7:
	ldr	r0, [r6, #0]
	bl	POC_get_GID_irrigation_system_using_POC_gid
	cmp	r5, r0
	bne	.L6
	fmsr	s13, r7	@ int
	ldr	r0, [r6, #0]
	mov	r1, sp
	fuitod	d7, s13
	fldd	d6, [r6, #48]
	faddd	d7, d7, d6
	ftouizd	s13, d7
	fmrs	r7, s13	@ int
	bl	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	ldr	r3, [sp, #0]
	ldr	r2, [r4, r3, asl #2]
	fmsr	s13, r2	@ int
	fuitod	d7, s13
	fldd	d6, [r6, #48]
	faddd	d7, d7, d6
	ftouizd	s13, d7
	fmrs	r2, s13	@ int
	str	r2, [r4, r3, asl #2]
.L6:
	mov	r0, r6
	bl	nm_POC_REPORT_RECORDS_get_previous_completed_record
	mov	r6, r0
.L5:
	cmp	r6, #0
	bne	.L7
	ldr	r3, .L8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r7
	ldmfd	sp!, {r3, r4, r5, r6, r7, pc}
.L9:
	.align	2
.L8:
	.word	poc_report_completed_records_recursive_MUTEX
	.word	.LC0
	.word	838
.LFE10:
	.size	nm_BUDGET_get_poc_usage, .-nm_BUDGET_get_poc_usage
	.section	.text.update_running_dtcs,"ax",%progbits
	.align	2
	.type	update_running_dtcs, %function
update_running_dtcs:
.LFB0:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r1, r1, asl #16
	sub	sp, sp, #20
.LCFI2:
	mov	r4, r0
	add	r3, sp, #16
	mov	r0, r1, lsr #16
	str	r3, [sp, #0]
	strh	r0, [r4, #4]	@ movhi
	add	r3, sp, #12
	add	r1, sp, #4
	add	r2, sp, #8
	bl	DateToDMY
	ldr	r3, [sp, #8]
	strh	r3, [r4, #8]	@ movhi
	ldr	r3, [sp, #4]
	strh	r3, [r4, #6]	@ movhi
	ldr	r3, [sp, #12]
	strh	r3, [r4, #10]	@ movhi
	ldr	r3, [sp, #16]
	strb	r3, [r4, #18]
	add	sp, sp, #20
	ldmfd	sp!, {r4, pc}
.LFE0:
	.size	update_running_dtcs, .-update_running_dtcs
	.section	.text.getNumManualStarts,"ax",%progbits
	.align	2
	.type	getNumManualStarts, %function
getNumManualStarts:
.LFB6:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI3:
	mov	sl, r0
	mov	r0, r3
	mov	r8, r3
	mov	r5, r1
	mov	r6, r2
	bl	MANUAL_PROGRAMS_get_start_date
	mov	fp, r0
	mov	r0, r8
	bl	MANUAL_PROGRAMS_get_end_date
	ldrh	r3, [sl, #4]
	cmp	r3, r0
	mov	r9, r0
	bhi	.L22
	ldr	r2, [r5, #16]
	cmp	r2, fp
	bcc	.L22
	ldr	r4, [r6, #44]
	mov	r6, #0
	add	r4, r3, r4
	mov	r7, r6
	b	.L13
.L20:
.LBB16:
	cmp	r4, r9
	movls	r6, #0
	movhi	r6, #1
	cmp	r4, fp
	orrcc	r6, r6, #1
	cmp	r6, #0
	bne	.L14
	add	r3, sp, #4
	str	r3, [sp, #0]
	mov	r0, r4
	mov	r1, r6
	mov	r2, r6
	mov	r3, r6
	bl	DateToDMY
	mov	r0, r8
	ldr	r1, [sp, #4]
	bl	MANUAL_PROGRAMS_today_is_a_water_day
	cmp	r0, #0
.L24:
	beq	.L14
	mov	r0, r8
	mov	r1, r6
	bl	MANUAL_PROGRAMS_get_start_time
	ldr	r3, .L25
	cmp	r0, r3
	beq	.L15
	ldr	r3, [r5, #12]
	cmp	r4, r3
	bne	.L16
	ldr	r3, [r5, #4]
	cmp	r0, r3
	bcc	.L15
.L16:
	ldr	r3, [r5, #16]
	cmp	r4, r3
	bne	.L17
	ldr	r3, [r5, #4]
	cmp	r0, r3
	bhi	.L15
.L17:
	ldrh	r3, [sl, #4]
	cmp	r4, r3
	bne	.L18
	ldr	r3, [sl, #0]
	cmp	r3, r0
	bhi	.L15
.L18:
	add	r7, r7, #1
.L15:
	add	r6, r6, #1
	cmp	r6, #6
	b	.L24
.L14:
.LBE16:
	add	r4, r4, #1
.L13:
	ldr	r3, [r5, #16]
	cmp	r4, r3
	bls	.L20
	mov	r6, r7
	b	.L12
.L22:
	mov	r6, #0
.L12:
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L26:
	.align	2
.L25:
	.word	86400
.LFE6:
	.size	getNumManualStarts, .-getNumManualStarts
	.section	.text.calc_time_adjustment_low_level,"ax",%progbits
	.align	2
	.type	calc_time_adjustment_low_level, %function
calc_time_adjustment_low_level:
.LFB13:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI4:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI5:
	fmsr	s17, r0
	mov	r0, r2
	sub	sp, sp, #4
.LCFI6:
	mov	r4, r2
	mov	r7, r3
	mov	r5, r1
	bl	nm_SYSTEM_get_Vp
	fmsr	s16, r0
	mov	r0, r4
	bl	nm_SYSTEM_get_poc_ratio
	fcmpzs	s16
	fmstat
	fmsr	s18, r0
	mov	r0, r7
	fcpyseq	s18, s17
	fmulsne	s18, s17, s18
	fdivsne	s18, s18, s16
	bl	STATION_GROUP_get_budget_reduction_limit_for_this_station
	rsb	r3, r0, #100
	fmsr	s15, r3	@ int
	mov	r6, r0
	fuitos	s16, s15
	flds	s15, .L38
	fmuls	s16, s17, s16
	fmuls	s16, s16, s15
	flds	s15, .L38+4
	fcmpes	s18, s16
	fmstat
	fcpyspl	s16, s18
	movpl	r4, #0
	movmi	r4, #2
	fcmpzs	s16
	fmstat
	fcpysmi	s16, s15
	fcmpes	s17, s16
	fmstat
	fcpysls	s16, s17
	bls	.L31
	tst	r4, #2
	orreq	r4, r4, #1
.L31:
	cmp	r5, #0
	beq	.L32
	mov	r0, r7
	mov	r1, sp
	bl	STATION_PRESERVES_get_index_using_ptr_to_station_struct
	cmp	r0, #0
	beq	.L32
	ldr	r5, .L38+8
	ldr	r2, .L38+12
	ldr	r3, .L38+16
	ldr	r0, [r5, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [sp, #0]
	ldr	r3, .L38+20
	cmp	r6, #0
	add	r3, r3, r2, asl #7
	ldrb	r2, [r3, #71]	@ zero_extendqisi2
	ldr	r0, [r5, #0]
	and	r2, r2, #252
	strb	r2, [r3, #71]
	orrne	r4, r4, r2
	add	r3, r3, #68
	strneb	r4, [r3, #3]
	bl	xQueueGiveMutexRecursive
.L32:
	fmrs	r0, s16
	add	sp, sp, #4
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L39:
	.align	2
.L38:
	.word	1008981770
	.word	0
	.word	station_preserves_recursive_MUTEX
	.word	.LC0
	.word	1180
	.word	station_preserves
.LFE13:
	.size	calc_time_adjustment_low_level, .-calc_time_adjustment_low_level
	.section	.text.getVolume,"ax",%progbits
	.align	2
	.type	getVolume, %function
getVolume:
.LFB5:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI8:
	fmsr	s17, r1
	ldr	r3, .L46+8
	sub	sp, sp, #8
.LCFI9:
	str	r3, [sp, #0]	@ float
	ldr	r3, [r2, #20]
	mov	r5, r0
	cmp	r3, #0
	mov	r4, r2
	beq	.L41
.LBB20:
.LBB21:
	ldrh	r3, [r0, #6]
	ldrh	r0, [r0, #8]
	cmp	r3, #1
	bne	.L42
	sub	r0, r0, #1
	bl	ET_DATA_et_number_for_the_month
	ldrh	r1, [r5, #10]
	fmsr	s16, r0
	ldrh	r0, [r5, #8]
	sub	r0, r0, #1
	bl	NumberOfDaysInMonth
	fmsr	s13, r0	@ int
	fuitos	s15, s13
	b	.L45
.L42:
	bl	ET_DATA_et_number_for_the_month
	ldrh	r1, [r5, #10]
	fmsr	s16, r0
	ldrh	r0, [r5, #8]
	bl	NumberOfDaysInMonth
	fmsr	s14, r0	@ int
	fuitos	s15, s14
.L45:
	fdivs	s16, s16, s15
	bl	WEATHER_TABLES_get_et_ratio
	flds	s15, [r4, #28]	@ int
	ldrh	r1, [r5, #8]
	flds	s20, [r4, #40]
	fuitos	s19, s15
	fmsr	s18, r0
	ldr	r0, [r4, #48]
	bl	STATION_GROUP_get_crop_coefficient_100u
	flds	s15, .L46
	fmuls	s20, s19, s20
	add	r2, sp, #4
	fmsr	s13, r0	@ int
	ldmia	r4, {r0, r1}
	fuitos	s14, s13
	flds	s13, [r4, #36]	@ int
	fmuls	s15, s14, s15
	flds	s14, [r4, #24]	@ int
	fmuls	s16, s15, s16
	fuitos	s15, s14
	fmuls	s18, s16, s18
	fmuls	s18, s18, s15
	fuitos	s15, s13
	fdivs	s19, s18, s20
	fmuls	s17, s17, s19
	fmuls	s17, s17, s15
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	ldr	r2, [sp, #4]
	ldr	r3, .L46+12
	add	r3, r3, r2, asl #7
	ldrh	r3, [r3, #136]
	ldr	r2, [r4, #36]
	mul	r3, r2, r3
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, .L46+4
	fdivs	s15, s14, s15
	fadds	s17, s17, s15
	b	.L44
.L41:
.LBE21:
.LBE20:
	flds	s13, [r2, #16]	@ int
	flds	s15, [sp, #0]
	ldr	r3, [r2, #36]
	fuitos	s14, s13
	fdivs	s15, s14, s15
	fmsr	s14, r3	@ int
	fmuls	s17, s17, s15
	fuitos	s15, s14
	fmuls	s17, s17, s15
.L44:
	fmrs	r0, s17
	add	sp, sp, #8
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, pc}
.L47:
	.align	2
.L46:
	.word	1198153728
	.word	1114636288
	.word	1092616192
	.word	station_preserves
.LFE5:
	.size	getVolume, .-getVolume
	.section	.text.calc_percent_adjust,"ax",%progbits
	.align	2
	.global	calc_percent_adjust
	.type	calc_percent_adjust, %function
calc_percent_adjust:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	fmsr	s13, r2
	ldr	r2, [r1, #0]
	fmsr	s15, r0
	cmp	r2, #0
	ble	.L49
	fmsr	s12, r2	@ int
	fsitos	s14, s12
	fcmpes	s15, s14
	fmstat
	fldshi	s12, .L54
	fmulsls	s15, s15, s13
	fsubshi	s13, s13, s12
	fmacshi	s15, s14, s13
.L49:
	rsb	r2, r3, r2
	bic	r2, r2, r2, asr #31
	str	r2, [r1, #0]
	fmrs	r0, s15
	bx	lr
.L55:
	.align	2
.L54:
	.word	1065353216
.LFE8:
	.size	calc_percent_adjust, .-calc_percent_adjust
	.section	.text.nm_BUDGET_calc_rpoc,"ax",%progbits
	.align	2
	.global	nm_BUDGET_calc_rpoc
	.type	nm_BUDGET_calc_rpoc, %function
nm_BUDGET_calc_rpoc:
.LFB11:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI10:
	fstmfdd	sp!, {d8}
.LCFI11:
	ldr	r7, .L68+4
	sub	sp, sp, #48
.LCFI12:
	mov	r2, #48
	mov	r4, r0
	mov	r6, r1
	mov	r0, sp
	mov	r1, #0
	bl	memset
	mov	r1, sp
	mov	r0, r4
	bl	nm_BUDGET_get_poc_usage
	ldr	r3, .L68+8
	mov	r1, #400
	ldr	r2, .L68+12
	mov	r8, #0
	mov	r9, r7
	mov	sl, r8
	mov	r5, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L68+16
	bl	xQueueTakeMutexRecursive_debug
.L58:
	ldr	fp, [r9, #24]
	ldr	r3, [r9, #44]
	cmp	fp, #0
	beq	.L57
	ldr	r3, [r3, #0]
	cmp	r4, r3
	bne	.L57
	mov	r0, sl
	bl	POC_use_for_budget
	cmp	r0, #0
	beq	.L57
	mov	r0, fp
	bl	POC_get_box_index_0
	mov	r1, r0
	mov	r0, fp
	bl	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	cmp	r0, #0
	beq	.L57
	mov	r1, #0
	bl	POC_get_budget
	cmp	r0, #0
	addne	r8, r8, #1
.L57:
	add	sl, sl, #1
	cmp	sl, #12
	add	r9, r9, #472
	bne	.L58
	flds	s16, .L68
	mov	sl, #0
.L64:
	ldr	r9, [r7, #24]
	ldr	fp, [r7, #44]
	cmp	r9, #0
	beq	.L59
	ldr	r3, [fp, #0]
	cmp	r4, r3
	bne	.L59
	mov	r0, sl
	bl	POC_use_for_budget
	cmp	r0, #0
	beq	.L59
	mov	r0, r9
	bl	POC_get_box_index_0
	mov	r1, r0
	mov	r0, r9
	bl	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	cmp	r0, #0
	beq	.L59
	mov	r1, #0
	bl	POC_get_budget
	cmp	r0, #0
	beq	.L59
	cmp	r8, #1
	fstseq	s16, [r6, #0]
	beq	.L59
	bls	.L59
	cmp	r5, #0
	bne	.L61
	mov	r2, #468
	ldrh	r3, [fp, r2]
	tst	r3, #8064
	beq	.L62
	ldr	r0, .L68+20
	mov	r1, r4
	mov	r2, r9
	bl	Alert_Message_va
.L62:
	fmsr	s13, r8	@ int
	fuitos	s15, s13
	fdivs	s15, s16, s15
	b	.L67
.L61:
	ldr	r3, [sp, sl, asl #2]
	cmp	r3, #0
	moveq	r2, #0
	fmsreq	s14, r2
	fstseq	s14, [r6, #0]
	beq	.L59
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	fmsr	s13, r5	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
.L67:
	fsts	s15, [r6, #0]
.L59:
	add	sl, sl, #1
	cmp	sl, #12
	add	r7, r7, #472
	add	r6, r6, #4
	bne	.L64
	ldr	r3, .L68+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #48
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L69:
	.align	2
.L68:
	.word	1065353216
	.word	poc_preserves
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	894
	.word	.LC1
.LFE11:
	.size	nm_BUDGET_calc_rpoc, .-nm_BUDGET_calc_rpoc
	.section	.text.nm_BUDGET_calc_time_adjustment,"ax",%progbits
	.align	2
	.global	nm_BUDGET_calc_time_adjustment
	.type	nm_BUDGET_calc_time_adjustment, %function
nm_BUDGET_calc_time_adjustment:
.LFB21:
	@ args = 0, pretend = 0, frame = 120
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI13:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI14:
	fmsr	s17, r0
	mov	r0, r1
	sub	sp, sp, #132
.LCFI15:
	add	r8, sp, #12
	stmia	r8, {r2, r3}
	mov	r7, r1
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	mov	r4, r0
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	mov	r5, r0
	mov	r0, r4
	bl	SYSTEM_get_group_with_this_GID
	add	r1, sp, #20
	mov	r6, r0
	bl	SYSTEM_get_budget_details
	ldr	r3, [sp, #20]
	cmp	r3, #1
	bne	.L84
	ldmia	r8, {r1, r2}
	add	r0, sp, #20
	bl	SYSTEM_get_budget_period_index
	subs	r1, r0, #0
	bne	.L84
	ldr	r3, [r5, #468]
	tst	r3, #122880
	beq	.L84
	mov	r0, r6
	bl	nm_SYSTEM_get_budget
	cmp	r0, #0
	beq	.L84
	ldr	r1, [sp, #128]
	cmp	r1, #0
	beq	.L73
	cmp	r1, #1
	bne	.L90
	b	.L93
.L73:
	fmrs	r0, s17
	mov	r2, r6
	mov	r3, r7
	bl	calc_time_adjustment_low_level
	fmsr	s18, r0
	fcmpes	s18, s17
	fmstat
	bpl	.L92
	ldr	r1, .L94+8
	ldrb	r3, [r1, #4]	@ zero_extendqisi2
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	orrs	r2, r3, r2, asl #8
	beq	.L77
	mov	r0, r8
	bl	DT1_IsBiggerThan_DT2
	cmp	r0, #0
	bne	.L77
	ldr	r3, .L94+12
	ldr	r3, [r3, #0]
	cmp	r4, r3
	beq	.L86
.L77:
	ldr	r0, .L94+8
	add	r1, sp, #12
	mov	r2, #6
	bl	memcpy
	ldr	r3, .L94+12
	str	r4, [r3, #0]
	b	.L86
.L93:
	fmrs	r0, s17
	mov	r2, r6
	mov	r3, r7
	bl	calc_time_adjustment_low_level
	fmsr	s18, r0
	fcmpes	s18, s17
	fmstat
	bpl	.L92
	mov	r0, r7
	bl	nm_STATION_get_expected_flow_rate_gpm
	fsubs	s16, s17, s18
	flds	s15, .L94
	fmsr	s14, r0	@ int
	fuitos	s19, s14
	fmuls	s19, s16, s19
	fdivs	s19, s19, s15
	b	.L75
.L90:
	ldr	r0, .L94+16
	mov	r2, r4
	bl	Alert_Message_va
.L86:
	fcpys	s18, s17
.L92:
	flds	s19, .L94+4
.L75:
	ldr	r1, .L94+8
	ldrb	r3, [r1, #4]	@ zero_extendqisi2
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	orrs	r2, r3, r2, asl #8
	beq	.L79
	add	r0, sp, #12
	bl	DT1_IsBiggerThan_DT2
	cmp	r0, #0
	bne	.L79
	ldr	r3, .L94+12
	ldr	r3, [r3, #0]
	cmp	r4, r3
	beq	.L71
.L79:
	mov	r0, r6
	bl	nm_SYSTEM_get_Vp
	fmsr	s16, r0
	mov	r0, r6
	bl	nm_SYSTEM_get_poc_ratio
	ldr	r2, [sp, #128]
	fmsr	s15, r0
	fcvtds	d7, s15
	fmrrd	r0, r1, d7
	fcvtds	d7, s16
	mov	r3, r0
	str	r1, [sp, #0]
	ldr	r0, .L94+20
	mov	r1, r4
	fstd	d7, [sp, #4]
	bl	Alert_Message_va
	ldr	r0, .L94+8
	add	r1, sp, #12
	mov	r2, #6
	bl	memcpy
	ldr	r3, .L94+12
	str	r4, [r3, #0]
	b	.L71
.L84:
	fcpys	s18, s17
	flds	s19, .L94+4
.L71:
	ldr	r3, .L94+24
	fdivs	s17, s18, s17
	ldr	r2, [r5, r3]
	fmsr	s14, r2	@ int
	fuitos	s15, s14
	fadds	s19, s15, s19
	fmrs	r0, s17
	ftouizs	s19, s19
	fmrs	r2, s19	@ int
	str	r2, [r5, r3]
	add	r5, r5, #14144
	fsts	s17, [r5, #16]
	bl	__float32_isnormal
	add	r5, r5, #16
	cmp	r0, #0
	moveq	r3, #0
	streq	r3, [r5, #0]	@ float
	fmrs	r0, s18
	add	sp, sp, #132
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L95:
	.align	2
.L94:
	.word	1114636288
	.word	0
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LC2
	.word	.LC3
	.word	14156
.LFE21:
	.size	nm_BUDGET_calc_time_adjustment, .-nm_BUDGET_calc_time_adjustment
	.section	.text.nm_BUDGET_predicted_volume,"ax",%progbits
	.align	2
	.global	nm_BUDGET_predicted_volume
	.type	nm_BUDGET_predicted_volume, %function
nm_BUDGET_predicted_volume:
.LFB22:
	@ args = 12, pretend = 4, frame = 120
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	sp, sp, #4
.LCFI16:
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI17:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI18:
	flds	s19, .L131+40
	mov	r4, r1
	sub	sp, sp, #120
.LCFI19:
	str	r0, [sp, #4]
	mov	r6, r2
	str	r3, [sp, #180]
	bl	NETWORK_CONFIG_get_controller_off
	cmp	r0, #0
	bne	.L97
	ldrh	r1, [sp, #184]
.LBB29:
	flds	s17, .L131
	mov	r0, r4
.LBE29:
	str	r1, [sp, #8]
.LBB33:
	ldmia	r6, {r1, r2}
	bl	SYSTEM_get_budget_period_index
	fcpys	s18, s19
	fcpys	s21, s17
	mov	r8, r0
	ldr	r0, .L131+16
	bl	nm_ListGetFirst
	mov	r9, r0
	b	.L98
.L120:
	mov	r0, r9
	add	r1, sp, #12
	bl	nm_STATION_get_Budget_data
	cmp	r8, #0
	beq	.L99
	add	r2, r8, #3
	ldr	r1, [sp, #8]
	ldr	r2, [r4, r2, asl #2]
	ldr	r3, [sp, #56]
	rsb	r2, r1, r2
	add	r1, r2, #1
	cmp	r3, r1
	mvnhi	r2, r2
	addhi	r3, r2, r3
	strhi	r3, [sp, #56]
.L99:
	ldr	r0, [sp, #60]
	cmp	r0, #0
	beq	.L100
	ldr	r3, [sp, #64]
	ldr	r2, [sp, #4]
	cmp	r3, r2
	bne	.L100
	ldr	r3, [sp, #24]
	cmp	r3, #0
	beq	.L100
	ldr	r3, [sp, #20]
	cmp	r3, #0
	beq	.L100
	add	r3, r8, #1
	str	r3, [sp, #0]
	add	r2, r8, #4
	ldrh	r1, [r6, #4]
	ldr	r3, [sp, #56]
	ldr	r2, [r4, r2, asl #2]
	add	r3, r1, r3
	cmp	r2, r3
	bcc	.L100
	bl	STATION_GROUP_get_group_with_this_GID
	subs	fp, r0, #0
	beq	.L100
	bl	SCHEDULE_get_start_time
	mov	r7, r0
	mov	r0, fp
	bl	SCHEDULE_get_stop_time
	cmp	r7, r0
	beq	.L125
	ldr	r1, [sp, #188]
	cmp	r1, #0
	beq	.L102
.LBB30:
	ldr	r0, [sp, #4]
	bl	SYSTEM_get_group_with_this_GID
	flds	s17, .L131
	mov	r5, r0
	bl	nm_SYSTEM_get_poc_ratio
	fmsr	s16, r0
	mov	r0, r5
	bl	nm_SYSTEM_get_Vp
	fmsr	s13, r0
	mov	r0, fp
	fdivs	s16, s16, s13
	bl	STATION_GROUP_get_budget_reduction_limit
	fcmpes	s16, s21
	fmstat
	bgt	.L102
	fmsr	s15, r0	@ int
	fcmpezs	s16
	fuitos	s14, s15
	flds	s15, .L131+4
	fmstat
	fdivs	s15, s14, s15
	fsubs	s17, s17, s15
	bls	.L102
	fcmps	s16, s17
	fmstat
	fcpysgt	s17, s16
.L102:
.LBE30:
	mov	lr, r6
	ldmia	lr!, {r0, r1, r2, r3}
	add	ip, sp, #76
	stmia	ip!, {r0, r1, r2, r3}
	ldr	r3, [lr, #0]
	ldr	r1, [sp, #56]
	str	r3, [ip, #0]
	ldrh	r3, [sp, #80]
	add	sl, sp, #76
	add	r1, r1, r3
	mov	r1, r1, asl #16
	mov	r1, r1, lsr #16
	mov	r0, sl
	strh	r1, [sp, #80]	@ movhi
	bl	update_running_dtcs
	ldrh	r1, [sp, #80]
	ldr	r0, [sp, #60]
	bl	PERCENT_ADJUST_get_percentage_100u_for_this_gid
	ldr	r3, .L131+8
	ldrh	r1, [sp, #80]
	flds	s15, [r3, #0]
	fmsr	s13, r0	@ int
	ldr	r0, [sp, #60]
	fuitos	s20, s13
	fdivs	s20, s20, s15
	bl	PERCENT_ADJUST_get_remaining_days_for_this_gid
	mov	r3, #0
	str	r3, [sp, #56]
	ldr	r3, [sp, #32]
	cmp	r3, #0
	str	r0, [sp, #116]
	beq	.L104
	flds	s16, .L131+40
.L112:
	ldrh	r1, [sp, #86]
	ldrh	r0, [sp, #84]
	bl	NumberOfDaysInMonth
	ldr	r2, [sp, #0]
	ldrh	r3, [sp, #82]
	add	ip, r2, #3
	ldr	ip, [r4, ip, asl #2]
	ldrh	r2, [sp, #80]
	rsb	r2, r2, ip
	ldr	ip, [sp, #76]
	cmp	ip, r7
	addls	r2, r2, #1
	rsb	r0, r3, r0
	add	r1, r0, #1
	addls	r5, r0, #2
	movhi	r5, r1
	cmp	r3, #1
	rsbeq	r5, r1, r5
	cmp	r2, r5
	bhi	.L107
	fmsr	s13, r2	@ int
	add	r1, sp, #116
	fmrs	r2, s20
	mov	r3, r5
	fuitos	s13, s13
	fmrs	r0, s13
	bl	calc_percent_adjust
	fmsr	s15, r0
	fcmpezs	s15
	fmstat
	ble	.L101
	add	r0, sp, #76
	fmrs	r1, s15
	add	r2, sp, #12
	bl	getVolume
	fmsr	s14, r0
	fadds	s16, s16, s14
	b	.L101
.L107:
	fmsr	s15, r5	@ int
	add	r1, sp, #116
	fmrs	r2, s20
	mov	r3, r5
	fuitos	s15, s15
	fmrs	r0, s15
	bl	calc_percent_adjust
	fmsr	s15, r0
	fcmpezs	s15
	fmstat
	ble	.L109
	mov	r0, sl
	fmrs	r1, s15
	add	r2, sp, #12
	bl	getVolume
	fmsr	s13, r0
	fadds	s16, s16, s13
.L109:
	ldrh	r3, [sp, #80]
	mov	r0, sl
	add	r5, r5, r3
	ldr	r3, [sp, #76]
	mov	r5, r5, asl #16
	mov	r5, r5, lsr #16
	cmp	r3, r7
	strh	r5, [sp, #80]	@ movhi
	addhi	r5, r5, #1
	strhih	r5, [sp, #80]	@ movhi
	mov	r3, #0
	ldrh	r1, [sp, #80]
	str	r3, [sp, #76]
	bl	update_running_dtcs
	b	.L112
.L104:
	mov	r0, sl
	ldrh	r1, [sp, #80]
	bl	update_running_dtcs
	ldr	r1, [sp, #0]
.LBB31:
	flds	s16, .L131+40
.LBE31:
	ldr	ip, [r4, #4]
	add	r3, r4, r1, asl #2
	str	r8, [sp, #0]
	ldrh	sl, [r3, #12]
	mov	r8, r4
.LBB32:
	ldrh	r5, [sp, #80]
	mov	r4, ip
	b	.L113
.L132:
	.align	2
.L131:
	.word	1065353216
	.word	1120403456
	.word	.LANCHOR2
	.word	.LANCHOR3
	.word	station_info_list_hdr
	.word	.LC0
	.word	454
	.word	.LANCHOR4
	.word	foal_irri+16
	.word	list_foal_irri_recursive_MUTEX
	.word	0
.L118:
	ldr	r2, [sp, #56]
	rsb	r2, r2, r5
	cmp	r2, r3
	bcc	.L114
	cmp	r5, r3
	bne	.L115
	ldr	r2, [sp, #76]
	cmp	r7, r2
	bcc	.L114
.L115:
	cmp	r5, sl
	bne	.L116
	cmp	r7, r4
	bhi	.L114
.L116:
	cmp	r5, r3
	bne	.L117
	ldr	r3, [sp, #76]
	cmp	r3, r7
	bhi	.L114
.L117:
	add	r0, sp, #96
	mov	r1, r5
	bl	update_running_dtcs
	mov	r0, fp
	add	r1, sp, #96
	mov	r2, #0
	bl	SCHEDULE_does_it_irrigate_on_this_date
	cmp	r0, #0
	faddsne	s16, s16, s21
.L114:
	add	r5, r5, #1
.L113:
	cmp	r5, sl
	ldrh	r3, [sp, #80]
	bls	.L118
.LBE32:
	fmrs	r0, s16
	add	r1, sp, #116
	fmrs	r2, s20
	rsb	r3, r3, sl
	mov	r4, r8
	ldr	r8, [sp, #0]
	bl	calc_percent_adjust
	add	r2, sp, #12
	mov	r1, r0	@ float
	add	r0, sp, #76
	bl	getVolume
	fmsr	s13, r0
	fadds	s16, s13, s19
	b	.L101
.L125:
	flds	s16, .L131+40
.L101:
	ldr	r0, [sp, #68]
	fmacs	s18, s17, s16
	cmp	r0, #0
	beq	.L119
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	subs	r5, r0, #0
	beq	.L119
	mov	r3, r5
	mov	r1, r4
	add	r2, sp, #12
	mov	r0, r6
	bl	getNumManualStarts
	mov	r7, r0
	mov	r0, r5
	bl	MANUAL_PROGRAMS_get_run_time
	ldr	r3, .L131+12
	flds	s15, [r3, #0]
	mul	r0, r7, r0
	ldr	r7, [sp, #48]
	mul	r7, r0, r7
	fmsr	s13, r7	@ int
	fuitos	s14, s13
	fdivs	s15, s14, s15
	fadds	s18, s18, s15
.L119:
	ldr	r0, [sp, #72]
	cmp	r0, #0
	beq	.L100
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	subs	r5, r0, #0
	beq	.L100
	mov	r3, r5
	mov	r1, r4
	add	r2, sp, #12
	mov	r0, r6
	bl	getNumManualStarts
	mov	r7, r0
	mov	r0, r5
	bl	MANUAL_PROGRAMS_get_run_time
	ldr	r3, .L131+12
	flds	s15, [r3, #0]
	mul	r0, r7, r0
	ldr	r7, [sp, #48]
	mul	r7, r0, r7
	fmsr	s13, r7	@ int
	fuitos	s14, s13
	fdivs	s15, s14, s15
	fadds	s18, s18, s15
.L100:
	mov	r1, r9
	ldr	r0, .L131+16
	bl	nm_ListGetNext
	mov	r9, r0
.L98:
	cmp	r9, #0
	bne	.L120
.LBE33:
.LBB34:
	ldr	r3, .L131+36
.LBE34:
	fadds	s19, s18, s19
.LBB35:
	mov	r1, #400
	ldr	r0, [r3, #0]
	flds	s18, .L131+40
	ldr	r2, .L131+20
	ldr	r3, .L131+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L131+32
	bl	nm_ListGetFirst
	ldr	r5, .L131+28
	mov	r4, r0
	b	.L121
.L123:
	ldr	r3, [r4, #40]
	ldr	r1, [sp, #4]
	ldr	r3, [r3, #0]
	cmp	r1, r3
	bne	.L122
	ldrb	r1, [r4, #90]	@ zero_extendqisi2
	ldrb	r0, [r4, #91]	@ zero_extendqisi2
	bl	nm_STATION_get_pointer_to_station
	bl	nm_STATION_get_expected_flow_rate_gpm
	ldr	r2, [r4, #48]
	flds	s15, [r5, #0]
	ldr	r3, [r4, #52]
	add	r3, r2, r3
	mul	r3, r0, r3
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	fdivs	s15, s14, s15
	fadds	s18, s18, s15
.L122:
	mov	r1, r4
	ldr	r0, .L131+32
	bl	nm_ListGetNext
	mov	r4, r0
.L121:
	cmp	r4, #0
	bne	.L123
	ldr	r3, .L131+36
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE35:
	fadds	s19, s19, s18
.L97:
	fmrs	r0, s19
	add	sp, sp, #120
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	add	sp, sp, #4
	bx	lr
.LFE22:
	.size	nm_BUDGET_predicted_volume, .-nm_BUDGET_predicted_volume
	.section	.text.nm_handle_sending_budget_record,"ax",%progbits
	.align	2
	.type	nm_handle_sending_budget_record, %function
nm_handle_sending_budget_record:
.LFB14:
	@ args = 20, pretend = 8, frame = 1316
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	sp, sp, #8
.LCFI20:
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI21:
	fstmfdd	sp!, {d8}
.LCFI22:
	ldr	r4, .L137+4
	sub	sp, sp, #1312
.LCFI23:
	sub	sp, sp, #12
.LCFI24:
	mov	r5, r1
	add	r1, sp, #1360
	add	r1, r1, #4
	stmib	r1, {r2, r3}
	ldr	r2, .L137+8
	mov	r8, r0
	mov	r1, #0
	add	r0, sp, #16
	bl	memset
	ldr	r3, .L137+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L137+16
	ldr	r3, .L137+20
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r8
	bl	SYSTEM_get_group_with_this_GID
	add	r9, sp, #16
	mov	sl, #0
	mov	ip, #472
	mov	fp, r4
	mov	r6, r4
	mov	r7, r5
	str	r0, [sp, #12]
.L135:
	ldr	r3, [r6, #24]
	cmp	r3, #0
	beq	.L134
	ldr	r2, [r6, #44]
	ldr	r2, [r2, #0]
	cmp	r2, r8
	bne	.L134
	mul	r4, ip, sl
	ldr	r2, .L137+24
	add	r4, r4, #380
	mov	r1, #400
	str	r3, [r4, fp]
	ldr	r0, [r2, #0]
	ldr	r3, .L137+28
	ldr	r2, .L137+16
	str	ip, [sp, #8]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r4, fp]
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	mov	r1, #0
	bl	POC_get_budget
	ldr	r3, .L137+24
	add	r5, r4, fp
	str	r0, [r5, #4]
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	r0, r9, #40
	add	r1, r6, #380
	mov	r2, #96
	bl	memcpy
	ldr	ip, [sp, #8]
.L134:
	add	sl, sl, #1
	cmp	sl, #12
	add	r6, r6, #472
	add	r9, r9, #96
	bne	.L135
	add	r1, sp, #1536
	ldrh	r3, [r1, #-164]
	add	r1, sp, #1200
	ldr	r0, [sp, #12]
	add	r1, r1, #12
	str	r3, [sp, #1208]
	bl	SYSTEM_get_budget_details
	add	r2, sp, #1536
	mov	r3, #0
	str	r3, [sp, #4]
	ldrh	r3, [r2, #-164]
	add	r2, sp, #1360
	add	r2, r2, #4
	add	r1, sp, #1200
	add	r1, r1, #12
	strh	r3, [sp, #0]	@ movhi
	mov	r0, r8
	ldr	r3, [r2, #4]!
	bl	nm_BUDGET_predicted_volume
	mov	r5, r7
	fmsr	s16, r0
	ldr	r0, [sp, #12]
	bl	nm_SYSTEM_get_used
	ldr	r3, .L137+12
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r8
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	fmsr	s14, r4	@ int
	ldr	r3, .L137+32
	add	ip, sp, #16
	fuitos	s15, s14
	fadds	s16, s16, s15
	flds	s15, .L137
	fadds	s16, s16, s15
	ftouizs	s16, s16
	str	r8, [r0, r3]
	ldr	r3, [r7, #0]
	add	lr, r0, #14080
	str	r3, [lr, #52]
	ldr	r3, [r7, #108]
	add	r0, r0, #14144
	str	r3, [lr, #56]
	ldr	r3, [r7, #12]
	str	r3, [lr, #60]
	ldr	r3, [r7, #16]
	str	r3, [r0, #0]
	ldr	r3, [r7, #4]
	fsts	s16, [lr, #72]	@ int
	str	r3, [lr, #68]
	add	lr, lr, #48
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr, {r0, r1}
	stmia	ip, {r0, r1}
	add	r0, sp, #16
	bl	nm_BUDGET_REPORT_DATA_close_and_start_a_new_record
	add	sp, sp, #300
	add	sp, sp, #1024
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	add	sp, sp, #8
	bx	lr
.L138:
	.align	2
.L137:
	.word	1056964608
	.word	poc_preserves
	.word	1196
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	1240
	.word	list_poc_recursive_MUTEX
	.word	1267
	.word	14128
.LFE14:
	.size	nm_handle_sending_budget_record, .-nm_handle_sending_budget_record
	.section	.text.BUDGET_in_effect_for_any_system,"ax",%progbits
	.align	2
	.global	BUDGET_in_effect_for_any_system
	.type	BUDGET_in_effect_for_any_system, %function
BUDGET_in_effect_for_any_system:
.LFB24:
	@ args = 0, pretend = 0, frame = 112
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI25:
	fstmfdd	sp!, {d8}
.LCFI26:
	flds	s17, .L147
	ldr	r3, .L147+4
	sub	sp, sp, #112
.LCFI27:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L147+8
	ldr	r3, .L147+12
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	mov	r4, #0
	mov	r6, r0
	b	.L140
.L144:
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	mov	r5, r0
	bl	nm_GROUP_get_group_ID
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldr	r3, [r0, #468]
	tst	r3, #122880
	beq	.L141
	mov	r0, r5
	mov	r1, sp
	bl	SYSTEM_get_budget_details
	ldr	r3, [sp, #0]
	cmp	r3, #0
	beq	.L141
	ldr	r3, [sp, #108]
	cmp	r3, #0
	beq	.L141
	mov	r0, r5
	bl	nm_SYSTEM_get_Vp
	fmsr	s15, r0
	fcmpezs	s15
	fmstat
	ble	.L141
	mov	r0, r5
	bl	nm_SYSTEM_get_poc_ratio
	fmsr	s16, r0
	mov	r0, r5
	bl	nm_SYSTEM_get_Vp
	fmsr	s15, r0
	fdivs	s16, s16, s15
	fcmpes	s16, s17
	fmstat
	bmi	.L145
.L141:
	add	r4, r4, #1
.L140:
	cmp	r4, r6
	bne	.L144
	mov	r4, #0
	b	.L143
.L145:
	mov	r4, #1
.L143:
	ldr	r3, .L147+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #112
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, pc}
.L148:
	.align	2
.L147:
	.word	1065353216
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	1915
.LFE24:
	.size	BUDGET_in_effect_for_any_system, .-BUDGET_in_effect_for_any_system
	.section	.text.BUDGET_handle_alerts_at_start_time,"ax",%progbits
	.align	2
	.global	BUDGET_handle_alerts_at_start_time
	.type	BUDGET_handle_alerts_at_start_time, %function
BUDGET_handle_alerts_at_start_time:
.LFB26:
	@ args = 0, pretend = 0, frame = 112
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI28:
	fstmfdd	sp!, {d8}
.LCFI29:
	ldr	r5, .L159+8
	sub	sp, sp, #112
.LCFI30:
	mov	r4, r1
	mov	r6, r0
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	mov	r1, #400
	ldr	r2, .L159+12
	mov	r3, #2160
	mov	r7, r0
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, [r7, #468]
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r7, r7, lsr #13
	and	r7, r7, #15
	cmp	r7, #0
	beq	.L149
	ldr	r3, .L159+16
	mov	r1, #400
	ldr	r2, .L159+12
	ldr	r0, [r3, #0]
	ldr	r3, .L159+20
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r6
	bl	SYSTEM_get_group_with_this_GID
	mov	r1, sp
	mov	r5, r0
	bl	SYSTEM_get_budget_details
	ldr	r3, [sp, #0]
	cmp	r3, #0
	beq	.L151
	ldr	r3, [sp, #108]
	cmp	r3, #0
	beq	.L151
	mov	r0, r5
	bl	nm_SYSTEM_get_poc_ratio
	flds	s17, .L159
	fmsr	s16, r0
	mov	r0, r5
	bl	nm_SYSTEM_get_Vp
	fmsr	s15, r0
	fdivs	s16, s16, s15
	fcmpes	s16, s17
	fmstat
	bpl	.L151
.LBB36:
	mov	r0, r4
	bl	STATION_GROUP_get_budget_reduction_limit
	fmsr	s15, r0	@ int
	mov	r0, r4
	fuitos	s14, s15
	flds	s15, .L159+4
	fsubs	s14, s15, s14
	fdivs	s14, s14, s15
	fcmpes	s16, s14
	fmstat
	fsubsge	s16, s17, s16
	fsubslt	s16, s17, s14
	movge	r5, #0
	movlt	r5, #1
	fmulsge	s16, s16, s15
	fmulslt	s16, s16, s15
	ftouizsge	s16, s16
	ftouizslt	s16, s16
	bl	nm_GROUP_get_name
	mov	r2, r5
	fmrs	r1, s16	@ int
	bl	Alert_budget_group_reduction
.L151:
.LBE36:
	ldr	r3, .L159+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L149:
	add	sp, sp, #112
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L160:
	.align	2
.L159:
	.word	1065353216
	.word	1120403456
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	list_system_recursive_MUTEX
	.word	2168
.LFE26:
	.size	BUDGET_handle_alerts_at_start_time, .-BUDGET_handle_alerts_at_start_time
	.section	.text.nm_BUDGET_get_used,"ax",%progbits
	.align	2
	.global	nm_BUDGET_get_used
	.type	nm_BUDGET_get_used, %function
nm_BUDGET_get_used:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L171+8
	stmfd	sp!, {r4, r5, lr}
.LCFI31:
	fstmfdd	sp!, {d8}
.LCFI32:
	fldd	d8, .L171
	mov	r4, #472
	mla	r4, r0, r4, r3
	ldr	r3, [r4, #24]
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_group_with_this_GID
	mov	r1, #1
	mov	r5, r0
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r1, #2
	cmp	r0, #0
	flddne	d7, [r4, #400]
	mov	r0, r5
	fadddne	d8, d7, d8
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r1, #4
	cmp	r0, #0
	flddne	d7, [r4, #408]
	mov	r0, r5
	fadddne	d8, d8, d7
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r1, #5
	cmp	r0, #0
	flddne	d7, [r4, #416]
	mov	r0, r5
	fadddne	d8, d8, d7
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r1, #6
	cmp	r0, #0
	flddne	d7, [r4, #424]
	mov	r0, r5
	fadddne	d8, d8, d7
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r1, #7
	cmp	r0, #0
	flddne	d7, [r4, #432]
	mov	r0, r5
	fadddne	d8, d8, d7
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r1, #8
	cmp	r0, #0
	flddne	d7, [r4, #440]
	mov	r0, r5
	fadddne	d8, d8, d7
	bl	nm_SYSTEM_get_budget_flow_type
	mov	r1, #9
	cmp	r0, #0
	flddne	d7, [r4, #384]
	mov	r0, r5
	fadddne	d8, d8, d7
	bl	nm_SYSTEM_get_budget_flow_type
	cmp	r0, #0
	flddne	d7, [r4, #392]
	fadddne	d8, d8, d7
	fmrrd	r0, r1, d8
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, pc}
.L172:
	.align	2
.L171:
	.word	0
	.word	0
	.word	poc_preserves+20
.LFE28:
	.size	nm_BUDGET_get_used, .-nm_BUDGET_get_used
	.section	.text.BUDGET_calculate_ratios,"ax",%progbits
	.align	2
	.global	BUDGET_calculate_ratios
	.type	BUDGET_calculate_ratios, %function
BUDGET_calculate_ratios:
.LFB23:
	@ args = 0, pretend = 0, frame = 172
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI33:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI34:
.LBB43:
	fldd	d9, .L194
.LBE43:
	ldr	r3, .L194+8
	sub	sp, sp, #180
.LCFI35:
	ldr	r2, .L194+12
	mov	r5, r0
	mov	sl, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	mov	r3, #1808
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L194+16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L194+12
	ldr	r3, .L194+20
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	mov	r8, #0
	mov	r6, r8
	ldr	r9, .L194+24
	str	r0, [sp, #12]
	b	.L174
.L187:
	mov	r0, r6
	bl	SYSTEM_get_group_at_this_index
	mov	r4, r0
	bl	nm_GROUP_get_group_ID
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldr	r3, .L194+28
	mov	r1, #400
	ldr	r2, .L194+12
	mov	r7, r0
	ldr	r0, [r9, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r9, #0]
	ldr	r7, [r7, #468]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	r1, sp, #20
	bl	SYSTEM_get_budget_details
	ldr	r3, [sp, #20]
	mov	r7, r7, lsr #13
	cmp	r3, #0
	and	r7, r7, #15
	beq	.L175
	add	r0, sp, #20
	ldmia	r5, {r1, r2}
	bl	SYSTEM_get_budget_period_index
	subs	fp, r0, #0
	bne	.L175
	cmp	r7, #0
	bne	.L176
.L175:
	mov	r0, r4
	mov	r1, #0
	bl	nm_SYSTEM_set_Vp
	mov	r0, r4
	mov	r1, #0
	bl	nm_SYSTEM_set_poc_ratio
	cmp	sl, #0
	beq	.L177
	ldr	r3, .L194+32
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L194+12
	ldr	r3, .L194+36
	bl	xQueueTakeMutexRecursive_debug
.LBB44:
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
.LBB45:
	ldr	r1, .L194+40
.LBE45:
	mov	r3, #0
.LBB46:
	mov	ip, #472
.L179:
	mla	r2, ip, r3, r1
	ldr	lr, [r2, #24]
	add	r2, r2, #20
	cmp	lr, #0
	beq	.L178
	ldr	lr, [r2, #24]
	ldr	lr, [lr, #0]
	cmp	r0, lr
	ldreq	lr, [r2, #452]
	addeq	lr, lr, #1
	streq	lr, [r2, #452]
.L178:
.LBE46:
	add	r3, r3, #1
	cmp	r3, #12
	bne	.L179
.LBE44:
	ldr	r3, .L194+32
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L177
.L176:
	ldr	r3, .L194+32
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L194+12
	ldr	r3, .L194+44
	bl	xQueueTakeMutexRecursive_debug
	cmp	sl, #0
	beq	.L180
.LBB47:
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
.LBB48:
	ldr	r2, .L194+40
	mov	r1, #472
.L182:
	mla	r3, r1, fp, r2
	ldr	ip, [r3, #24]
	add	r3, r3, #20
	cmp	ip, #0
	beq	.L181
	ldr	ip, [r3, #24]
	ldr	ip, [ip, #0]
	cmp	r0, ip
	ldreq	ip, [r3, #448]
	addeq	ip, ip, #1
	streq	ip, [r3, #448]
.L181:
.LBE48:
	add	fp, fp, #1
	cmp	fp, #12
	bne	.L182
.L180:
.LBE47:
.LBB49:
	mov	r2, #48
	mov	r1, #0
	add	r0, sp, #132
	bl	memset
	ldr	r3, .L194+48
	mov	r0, r4
	ldr	r1, [r3, #0]	@ float
	bl	nm_SYSTEM_set_poc_ratio
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	add	r1, sp, #132
	ldr	r8, .L194+40
	add	fp, sp, #132
	mov	r7, #0
	str	r0, [sp, #16]
	bl	nm_BUDGET_calc_rpoc
.L186:
	ldr	r3, [r8, #24]
	cmp	r3, #0
	beq	.L183
	ldr	r2, [r8, #44]
	ldr	r1, [sp, #16]
	ldr	r2, [r2, #0]
	cmp	r1, r2
	bne	.L183
	mov	r0, r7
	str	r3, [sp, #8]
	bl	POC_use_for_budget
	ldr	r3, [sp, #8]
	cmp	r0, #0
	beq	.L183
	mov	r0, r3
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	mov	r1, #0
	bl	POC_get_budget
	fmsr	s15, r0	@ int
	mov	r0, r7
	fuitos	s17, s15
	bl	nm_BUDGET_get_used
	flds	s16, [fp, #0]
	fcmpezs	s17
	fmstat
	fmdrr	d7, r0, r1
	ble	.L183
	fcvtsd	s14, d7
	fcvtds	d6, s17
	fcvtds	d8, s16
	mov	r0, r4
	fcvtds	d7, s14
	fmscd	d7, d6, d9
	fdivd	d8, d7, d8
	bl	nm_SYSTEM_get_poc_ratio
	fcvtsd	s16, d8
	fmsr	s15, r0
	fcmpes	s16, s15
	fmstat
	bpl	.L183
	mov	r0, r4
	fmrs	r1, s16
	bl	nm_SYSTEM_set_poc_ratio
.L183:
	add	r7, r7, #1
	cmp	r7, #12
	add	r8, r8, #472
	add	fp, fp, #4
	bne	.L186
.LBE49:
	ldr	r3, .L194+32
	mov	r8, #1
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	mov	r3, #0
	str	r3, [sp, #4]
	ldrh	r3, [r5, #4]
	add	r1, sp, #20
	strh	r3, [sp, #0]	@ movhi
	mov	r2, r5
	ldr	r3, [r5, #0]
	bl	nm_BUDGET_predicted_volume
	mov	r1, r0	@ float
	mov	r0, r4
	bl	nm_SYSTEM_set_Vp
.L177:
	add	r6, r6, #1
.L174:
	ldr	r1, [sp, #12]
	cmp	r6, r1
	bne	.L187
	ldr	r3, .L194+16
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L194+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r8, #1
	bne	.L173
	mov	r0, #20
	bl	WEATHER_TABLES_set_et_ratio
.L173:
	add	sp, sp, #180
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L195:
	.align	2
.L194:
	.word	-343597384
	.word	1072609361
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	list_program_data_recursive_MUTEX
	.word	1810
	.word	system_preserves_recursive_MUTEX
	.word	1820
	.word	poc_preserves_recursive_MUTEX
	.word	1839
	.word	poc_preserves
	.word	1852
	.word	__float32_infinity
.LFE23:
	.size	BUDGET_calculate_ratios, .-BUDGET_calculate_ratios
	.section	.text.BUDGET_reset_budget_values,"ax",%progbits
	.align	2
	.global	BUDGET_reset_budget_values
	.type	BUDGET_reset_budget_values, %function
BUDGET_reset_budget_values:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI36:
	mov	r8, r3
	ldr	r3, .L205
	mov	r6, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L205+4
	mov	r7, r2
	ldr	r2, .L205+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L205+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L205+8
	ldr	r3, .L205+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L205+20
	mov	r1, #400
	ldr	r2, .L205+8
	ldr	r0, [r3, #0]
	ldr	r3, .L205+24
	bl	xQueueTakeMutexRecursive_debug
.LBB50:
	bl	FLOWSENSE_get_controller_index
	mov	r1, #2
	mov	r5, #0
	mov	fp, #1
	mov	sl, r0
	mov	r0, r6
	bl	SYSTEM_get_change_bits_ptr
	mov	r9, r0
.L198:
	mov	r1, r4
	mov	r2, r7
	mov	r0, #1
	bl	DMYToDate
	mov	r3, #2
	stmia	sp, {r3, sl, fp}
	mov	r1, r5
	add	r4, r4, r8
	mov	r3, #1
	str	r9, [sp, #12]
	add	r5, r5, #1
	mov	r2, r0
	mov	r0, r6
	bl	nm_SYSTEM_set_budget_period
	cmp	r4, #12
	subhi	r4, r4, #12
	addhi	r7, r7, #1
	cmp	r5, #24
	bne	.L198
.LBE50:
.LBB51:
	mov	r0, r6
	bl	nm_GROUP_get_group_ID
.LBB52:
	ldr	r6, .L205+28
.LBE52:
	mov	r4, #0
.LBB53:
	mov	r7, #472
.LBE53:
	mov	r8, r0
.L201:
.LBB54:
	mla	r3, r7, r4, r6
	ldr	r0, [r3, #24]
	add	r3, r3, #20
	cmp	r0, #0
	beq	.L199
	ldr	r3, [r3, #24]
	ldr	r3, [r3, #0]
	cmp	r3, r8
	bne	.L199
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	mov	r5, #0
	mov	sl, r0
.L200:
	mov	r1, r5
	mov	r0, sl
	mov	r2, #0
	add	r5, r5, #1
	bl	POC_set_budget
	cmp	r5, #24
	bne	.L200
	mov	r0, r4
	bl	nm_handle_budget_reset_record
.L199:
.LBE54:
	add	r4, r4, #1
	cmp	r4, #12
	bne	.L201
.LBE51:
	ldr	r3, .L205+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L205+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L205
	ldr	r0, [r3, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	xQueueGiveMutexRecursive
.L206:
	.align	2
.L205:
	.word	list_system_recursive_MUTEX
	.word	2573
	.word	.LC0
	.word	system_preserves_recursive_MUTEX
	.word	2574
	.word	poc_preserves_recursive_MUTEX
	.word	2575
	.word	poc_preserves
.LFE29:
	.size	BUDGET_reset_budget_values, .-BUDGET_reset_budget_values
	.section	.text.BUDGET_calculate_square_footage,"ax",%progbits
	.align	2
	.global	BUDGET_calculate_square_footage
	.type	BUDGET_calculate_square_footage, %function
BUDGET_calculate_square_footage:
.LFB30:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI37:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI38:
	flds	s17, .L211
	ldr	r3, .L211+8
	flds	s18, .L211+4
	sub	sp, sp, #76
.LCFI39:
	mov	r1, #400
	ldr	r2, .L211+12
	mov	r6, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L211+16
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r6
	bl	SYSTEM_get_group_at_this_index
	ldr	r0, .L211+20
	bl	nm_ListGetFirst
	mov	r5, #0
	mov	r4, r0
	b	.L208
.L210:
	mov	r0, r4
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	bl	SYSTEM_get_index_for_group_with_this_GID
	cmp	r0, r6
	bne	.L209
	mov	r0, r4
	bl	STATION_get_GID_station_group
	cmp	r0, #0
	beq	.L209
	mov	r0, r4
	bl	STATION_get_square_footage
	cmp	r0, #1000
	bne	.L209
	mov	r0, r4
	add	r1, sp, #12
	bl	nm_STATION_get_Budget_data
	flds	s15, [sp, #48]	@ int
	mov	r1, #0
	fuitos	s14, s15
	flds	s15, [sp, #52]
	fmuls	s14, s14, s17
	fmuls	s14, s14, s18
	fdivs	s15, s14, s15
	fmrs	r0, s15
	bl	__round_float
	mov	r1, #2
	ldr	r5, [sp, #12]
	fmsr	s16, r0
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	str	r5, [sp, #0]
	ftouizs	s16, s16
	mov	r5, #1
	mov	r2, r5
	mov	r3, #2
	str	r5, [sp, #4]
	fmrs	r1, s16	@ int
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_square_footage
.L209:
	mov	r1, r4
	ldr	r0, .L211+20
	bl	nm_ListGetNext
	mov	r4, r0
.L208:
	cmp	r4, #0
	bne	.L210
	ldr	r3, .L211+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #76
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, pc}
.L212:
	.align	2
.L211:
	.word	1070421333
	.word	1203982336
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	2619
	.word	station_info_list_hdr
.LFE30:
	.size	BUDGET_calculate_square_footage, .-BUDGET_calculate_square_footage
	.section	.text.BUDGET_calc_POC_monthly_budget_using_et,"ax",%progbits
	.align	2
	.global	BUDGET_calc_POC_monthly_budget_using_et
	.type	BUDGET_calc_POC_monthly_budget_using_et, %function
BUDGET_calc_POC_monthly_budget_using_et:
.LFB33:
	@ args = 0, pretend = 0, frame = 248
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI40:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI41:
	ldr	r3, .L235+20
	ldr	r5, .L235+24
	sub	sp, sp, #248
.LCFI42:
	mov	r4, r0
	str	r1, [sp, #4]
	mov	r2, #48
	mov	r1, #0
	add	r0, sp, #168
	str	r3, [sp, #236]	@ float
	bl	memset
	mov	r1, #400
	ldr	r2, .L235+28
	ldr	r3, .L235+32
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	mov	r9, #0
	mov	r6, r0
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r5, .L235+36
	ldr	r2, .L235+28
	ldr	r3, .L235+40
	ldr	r0, [r5, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	add	r1, sp, #168
	mov	r0, r6
	bl	nm_BUDGET_get_poc_usage
	mov	r8, r0
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	r1, sp, #8
	bl	SYSTEM_get_budget_details
	ldr	r5, .L235+44
	mov	r4, r9
	mov	r7, r5
.L215:
	ldr	r3, [r7, #24]
	cmp	r3, #0
	beq	.L214
	ldr	r3, [r7, #44]
	ldr	r3, [r3, #0]
	cmp	r6, r3
	bne	.L214
	mov	r0, r4
	bl	POC_use_for_budget
	cmp	r0, #0
	addne	r9, r9, #1
.L214:
	add	r4, r4, #1
	cmp	r4, #12
	add	r7, r7, #472
	bne	.L215
	fldd	d10, .L235
	flds	s19, .L235+8
	mov	fp, #0
	mov	r4, fp
.L227:
	mov	r0, r4
	ldr	sl, [r5, #24]
	bl	POC_get_group_at_this_index
	cmp	sl, #0
	mov	r7, r0
	beq	.L216
	ldr	r3, [r5, #44]
	ldr	r3, [r3, #0]
	cmp	r6, r3
	bne	.L216
	mov	r0, r4
	bl	POC_use_for_budget
	cmp	r0, #0
	beq	.L216
	cmp	fp, #0
	bne	.L217
	mov	r0, r6
	bl	SYSTEM_get_index_for_group_with_this_GID
.LBB58:
	ldr	r3, .L235+48
.LBB59:
	flds	s16, .L235+12
	mov	r1, #400
.LBE59:
	str	r3, [sp, #240]	@ float
	ldr	r3, .L235+52
.LBB60:
	ldr	r2, .L235+28
.LBE60:
	str	r3, [sp, #244]	@ float
.LBB61:
	ldr	r3, .L235+56
.LBE61:
.LBE58:
	mov	sl, r0
.LBB64:
.LBB62:
	ldr	r0, [r3, #0]
	ldr	r3, .L235+60
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, sl
	bl	SYSTEM_get_group_at_this_index
	ldr	r0, .L235+64
	bl	nm_ListGetFirst
	mov	fp, r0
	b	.L218
.L220:
	mov	r0, fp
	bl	STATION_GROUP_get_GID_irrigation_system_for_this_station
	bl	SYSTEM_get_index_for_group_with_this_GID
	cmp	r0, sl
	bne	.L219
	mov	r0, fp
	bl	STATION_get_GID_station_group
	cmp	r0, #0
	beq	.L219
	mov	r0, fp
	bl	STATION_get_square_footage
	fmsr	s13, r0	@ int
	fuitos	s15, s13
	fadds	s16, s16, s15
.L219:
	mov	r1, fp
	ldr	r0, .L235+64
	bl	nm_ListGetNext
	mov	fp, r0
.L218:
	cmp	fp, #0
	bne	.L220
	ldr	r3, .L235+56
.LBE62:
	add	fp, sp, #116
.LBB63:
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.LBE63:
	ftouizs	s18, s16
	flds	s14, [sp, #240]
	flds	s15, [sp, #244]
	mov	sl, #1
	fuitos	s18, s18
	fmuls	s18, s18, s14
	fdivs	s18, s18, s15
.L221:
.LBE64:
	mov	r0, sl
	bl	ET_DATA_et_number_for_the_month
	add	sl, sl, #1
	fmsr	s14, r0
	mov	r0, r7
	fmuls	s16, s18, s14
	bl	POC_get_budget_percent_ET
	flds	s15, [sp, #236]
	cmp	sl, #13
	fmsr	s13, r0	@ int
	fuitos	s14, s13
	fmuls	s16, s16, s14
	fdivs	s16, s16, s15
	fmrs	r2, s16
	str	r2, [fp, #4]!	@ float
	bne	.L221
	mov	fp, #1
.L217:
	cmp	r9, #1
	beq	.L228
	bls	.L222
	cmp	r8, #0
	fmsreq	s13, r9	@ int
	fldseq	s17, .L235+16
	fuitoseq	s15, s13
	beq	.L234
.L223:
	add	r3, sp, #168
	ldr	r3, [r3, r4, asl #2]
	cmp	r3, #0
	beq	.L229
	fmsr	s14, r3	@ int
	fmsr	s13, r8	@ int
	fuitos	s17, s14
	fuitos	s15, s13
.L234:
	fdivs	s17, s17, s15
	b	.L222
.L228:
	flds	s17, .L235+16
	b	.L222
.L229:
	flds	s17, .L235+12
.L222:
	add	r3, sp, #16
	mov	sl, #0
	str	r3, [sp, #0]
.L226:
	ldr	r3, [sp, #0]
	mov	r1, #0
	ldr	r0, [r3, #4]!
	add	r2, sp, #216
	str	r3, [sp, #0]
	bl	DateAndTimeToDTCS
	ldrh	r3, [sp, #224]
	fcvtds	d7, s17
	add	r2, sp, #248
	add	r3, r2, r3, asl #2
	flds	s12, [r3, #-132]
	mov	r1, sl
	mov	r0, r7
	fcvtds	d6, s12
	fmuld	d6, d6, d10
	fmuld	d7, d6, d7
	fcvtsd	s18, d7
	bl	POC_get_budget
	fmsr	s13, r0	@ int
	fuitos	s15, s13
	fsubs	s15, s18, s15
	ftosizs	s15, s15
	fmrs	r0, s15	@ int
	bl	abs
	flds	s15, [sp, #236]
	mov	r1, sl
	fmsr	s14, r0	@ int
	mov	r0, r7
	fsitos	s16, s14
	fmuls	s16, s16, s15
	bl	POC_get_budget
	fmsr	s13, r0	@ int
	fuitos	s15, s13
	fdivs	s16, s16, s15
	fcmpes	s16, s19
	fmstat
	bgt	.L224
	ldr	r2, [sp, #4]
	cmp	r2, #1
	bne	.L225
.L224:
	ftouizs	s18, s18
	mov	r0, r7
	mov	r1, sl
	fmrs	r2, s18	@ int
	bl	POC_set_budget
.L225:
	add	sl, sl, #1
	cmp	sl, #12
	bne	.L226
.L216:
	add	r4, r4, #1
	cmp	r4, #12
	add	r5, r5, #472
	bne	.L227
	add	sp, sp, #248
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L236:
	.align	2
.L235:
	.word	-343597384
	.word	1072609361
	.word	1082130432
	.word	0
	.word	1065353216
	.word	1120403456
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	2829
	.word	poc_preserves_recursive_MUTEX
	.word	2835
	.word	poc_preserves
	.word	1188308096
	.word	1193945088
	.word	list_program_data_recursive_MUTEX
	.word	2692
	.word	station_info_list_hdr
.LFE33:
	.size	BUDGET_calc_POC_monthly_budget_using_et, .-BUDGET_calc_POC_monthly_budget_using_et
	.section	.text.BUDGET_handle_alerts_at_midnight,"ax",%progbits
	.align	2
	.global	BUDGET_handle_alerts_at_midnight
	.type	BUDGET_handle_alerts_at_midnight, %function
BUDGET_handle_alerts_at_midnight:
.LFB25:
	@ args = 20, pretend = 12, frame = 164
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	sp, sp, #12
.LCFI43:
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI44:
	fstmfdd	sp!, {d8, d9, d10, d11, d12}
.LCFI45:
	ldr	r6, .L262+8
	sub	sp, sp, #172
.LCFI46:
	add	r4, sp, #248
	stmia	r4, {r1, r2, r3}
	mov	r5, r0
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	mov	r1, #400
	ldr	r2, .L262+12
	ldr	r3, .L262+16
	mov	r7, r0
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, [r7, #468]
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	mov	r7, r7, lsr #13
	and	r7, r7, #15
	cmp	r7, #0
	beq	.L238
	ldr	r3, .L262+20
	mov	r1, #400
	ldr	r2, .L262+12
	ldr	r0, [r3, #0]
	ldr	r3, .L262+24
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	SYSTEM_get_group_with_this_GID
	add	r1, sp, #12
	mov	r9, r0
	bl	SYSTEM_get_budget_details
	ldr	r3, [sp, #12]
	cmp	r3, #0
	beq	.L238
	mov	r2, #48
	mov	r1, #0
	add	r0, sp, #124
	bl	memset
	mov	r0, r5
	add	r1, sp, #124
	bl	nm_BUDGET_calc_rpoc
	ldr	r3, [sp, #120]
.LBB65:
.LBB66:
	flds	s23, .L262
.LBE66:
.LBE65:
	mov	r2, r4
	adds	r3, r3, #0
	movne	r3, #1
	str	r3, [sp, #4]
	ldrh	r3, [sp, #252]
.LBB75:
.LBB73:
	flds	s22, .L262+4
.LBE73:
.LBE75:
	add	r1, sp, #12
	strh	r3, [sp, #0]	@ movhi
	mov	r0, r5
	ldr	r3, [sp, #248]
	bl	nm_BUDGET_predicted_volume
	ldr	r3, .L262+28
	fcpys	s24, s23
	mov	r1, #400
	ldr	r2, .L262+12
	mov	sl, #0
	add	r4, sp, #124
	mov	r8, sl
	fmsr	s20, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L262+32
	bl	xQueueTakeMutexRecursive_debug
.L252:
.LBB76:
	ldr	r3, .L262+36
	mov	r6, #472
	mla	r6, r8, r6, r3
	ldr	r3, [r6, #4]
	cmp	r3, #0
	beq	.L239
	ldr	r3, [r6, #24]
	ldr	r2, [r3, #0]
	adds	r3, r5, #0
	movne	r3, #1
	cmp	r2, r5
	movne	r3, #0
	cmp	r3, #0
	beq	.L239
	mov	r0, r8
	bl	POC_use_for_budget
	cmp	r0, #0
	beq	.L239
	ldr	r7, .L262+40
	ldr	r2, .L262+12
	ldr	r3, .L262+44
	mov	r1, #400
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r6, #4]
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	mov	fp, r0
	bl	POC_get_budget_gallons_entry_option
	mov	r1, #0
	mov	sl, r0
	mov	r0, fp
	bl	POC_get_budget
	mov	r3, r0
	ldr	r0, [r7, #0]
	str	r3, [sp, #8]
	bl	xQueueGiveMutexRecursive
	ldr	r3, [sp, #8]
	cmp	r3, #0
	bne	.L240
	mov	r0, r9
	bl	nm_GROUP_get_name
	mov	r6, r0
	mov	r0, fp
	bl	nm_GROUP_get_name
	mov	r1, r0
	mov	r0, r6
	bl	Alert_budget_values_not_set
	b	.L239
.L240:
.LBB74:
	ldr	r0, [r6, #4]
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	mov	r1, #0
	bl	POC_get_budget
	flds	s18, .L262+4
	mov	r6, r0
	mov	r0, r8
	bl	nm_BUDGET_get_used
	flds	s21, [r4, #0]
	fmuls	s21, s20, s21
	fmdrr	d7, r0, r1
	fcvtsd	s17, d7
	fmsr	s15, r6	@ int
.LBB67:
	mov	r6, #0
.LBE67:
	fuitos	s16, s15
	fsubs	s15, s16, s17
	fdivs	s21, s15, s21
	fsubs	s19, s23, s21
	fmuls	s19, s19, s22
	fcmps	s19, s22
	fmstat
	fcpysgt	s19, s22
	bl	STATION_GROUP_get_num_groups_in_use
	mov	r7, r0
.LBB72:
	b	.L242
.L245:
.LBB68:
	mov	r0, r6
	bl	STATION_GROUP_get_group_at_this_index
	str	r0, [sp, #8]
	bl	STATION_GROUP_get_GID_irrigation_system
	ldr	r3, [sp, #8]
	cmp	r0, r5
	bne	.L243
.LBB69:
	mov	r0, r3
	bl	STATION_GROUP_get_budget_reduction_limit
	fmsr	s14, r0	@ int
	fuitos	s15, s14
	fcmpes	s19, s15
	fmstat
	movle	r3, #0
	movgt	r3, #1
	cmp	r0, #99
	movhi	r0, #0
	andls	r0, r3, #1
	cmp	r0, #0
	bne	.L255
.L243:
.LBE69:
.LBE68:
	add	r6, r6, #1
.L242:
	cmp	r6, r7
	bne	.L245
	mov	r3, #1
	b	.L244
.L255:
.LBB71:
.LBB70:
	mov	r3, #0
.L244:
.LBE70:
.LBE71:
.LBE72:
	fcmpes	s21, s24
	fmstat
	blt	.L259
	flds	s15, [r4, #0]
	mov	r0, fp
	fmacs	s17, s20, s15
	bl	nm_GROUP_get_name
	fmuls	s17, s17, s18
	fdivs	s16, s17, s16
	fsubs	s18, s18, s16
	ftouizs	s18, s18
	fmrs	r6, s18	@ int
	fmrs	r1, s18	@ int
	b	.L261
.L259:
	fcmpezs	s21
	fmstat
	blt	.L260
	cmp	r3, #1
	bne	.L250
	ldr	r3, [sp, #120]
	cmp	r3, #0
	beq	.L250
	mov	r0, fp
	bl	nm_GROUP_get_name
	mov	r1, #0
.L261:
	bl	Alert_budget_under_budget
	b	.L239
.L250:
	flds	s15, [r4, #0]
	mov	r0, fp
	fmacs	s17, s20, s15
	bl	nm_GROUP_get_name
	fsubs	s17, s17, s16
	fmuls	s18, s17, s18
	fdivs	s16, s18, s16
	ftouizs	s16, s16
	fmrs	r1, s16	@ int
	fmrs	r6, s16	@ int
	bl	Alert_budget_will_exceed_budget
	b	.L239
.L260:
	ldrh	r3, [sp, #252]
	ldr	r2, [sp, #24]
	cmp	r2, r3
	bne	.L251
	mov	r0, fp
	bl	nm_GROUP_get_name
	fsubs	s17, s17, s16
	fmuls	s18, s17, s18
	fdivs	s16, s18, s16
	ftouizs	s16, s16
	fmrs	r1, s16	@ int
	bl	Alert_budget_over_budget_period_ending_today
	b	.L239
.L251:
	flds	s15, [r4, #0]
	fcpys	s14, s17
	mov	r0, fp
	fmacs	s14, s20, s15
	fsubs	s15, s14, s16
	fmuls	s15, s15, s18
	fdivs	s15, s15, s16
	ftouizs	s15, s15
	fmrs	r6, s15	@ int
	bl	nm_GROUP_get_name
	fsubs	s17, s17, s16
	mov	r2, r6
	fmuls	s18, s17, s18
	fdivs	s16, s18, s16
	ftouizs	s16, s16
	fmrs	r1, s16	@ int
	bl	Alert_budget_over_budget
.L239:
.LBE74:
.LBE76:
	add	r8, r8, #1
	cmp	r8, #12
	add	r4, r4, #4
	bne	.L252
	cmp	sl, #2
	bne	.L253
	mov	r0, r9
	mov	r1, #0
	bl	BUDGET_calc_POC_monthly_budget_using_et
.L253:
	ldr	r3, .L262+28
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L238:
	ldr	r3, .L262+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #172
	fldmfdd	sp!, {d8, d9, d10, d11, d12}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	add	sp, sp, #12
	bx	lr
.L263:
	.align	2
.L262:
	.word	1065353216
	.word	1120403456
	.word	system_preserves_recursive_MUTEX
	.word	.LC0
	.word	1986
	.word	list_system_recursive_MUTEX
	.word	1994
	.word	poc_preserves_recursive_MUTEX
	.word	2013
	.word	poc_preserves+20
	.word	list_poc_recursive_MUTEX
	.word	2025
.LFE25:
	.size	BUDGET_handle_alerts_at_midnight, .-BUDGET_handle_alerts_at_midnight
	.section	.text.BUDGET_timer_upkeep,"ax",%progbits
	.align	2
	.global	BUDGET_timer_upkeep
	.type	BUDGET_timer_upkeep, %function
BUDGET_timer_upkeep:
.LFB27:
	@ args = 20, pretend = 16, frame = 176
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	sp, sp, #16
.LCFI47:
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI48:
	ldr	r4, .L317
	sub	sp, sp, #192
.LCFI49:
	add	ip, sp, #228
	stmia	ip, {r0, r1, r2, r3}
	ldmia	ip, {r0, r1}
	str	r0, [sp, #152]
	strh	r1, [sp, #156]	@ movhi
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	ldrb	r0, [r4, #5]	@ zero_extendqisi2
	orrs	r3, r3, r0, asl #8
	beq	.L265
	mov	r0, r1, asl #16
	mov	r0, r0, lsr #16
	rsb	r0, r3, r0
	bl	abs
	ldr	r3, .L317+4
	cmp	r0, r3
	ble	.L265
	ldrh	r1, [sp, #236]
	ldrh	r2, [sp, #234]
	ldr	r0, .L317+8
	ldrh	r3, [sp, #238]
	bl	Alert_Message_va
	mov	r0, r4
	add	r1, sp, #152
	mov	r2, #6
	bl	memcpy
	b	.L264
.L265:
	add	r1, sp, #152
	mov	r2, #6
	ldr	r0, .L317
	bl	memcpy
	ldr	r3, .L317+12
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L317+16
	ldr	r3, .L317+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L317+24
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L317+16
	ldr	r3, .L317+28
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L317+32
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L317+16
	mov	r3, #2272
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	add	r1, sp, #144
	str	r1, [sp, #28]
	str	r0, [sp, #32]
	mov	r0, #0
	str	r0, [sp, #24]
	b	.L267
.L300:
	ldr	r0, [sp, #24]
	bl	SYSTEM_get_group_at_this_index
	str	r0, [sp, #20]
	bl	nm_GROUP_get_group_ID
	mov	r4, r0
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldr	r3, [r0, #468]
	tst	r3, #122880
	beq	.L268
	ldr	r0, [sp, #20]
	add	r1, sp, #40
	bl	SYSTEM_get_budget_details
	ldr	r3, [sp, #52]
	ldr	r2, [sp, #56]
	strh	r3, [sp, #164]	@ movhi
	ldr	r3, [sp, #44]
	add	r7, sp, #160
	add	r5, sp, #152
	mov	r0, r7
	mov	r1, r5
	strh	r2, [sp, #172]	@ movhi
	str	r3, [sp, #160]
	str	r3, [sp, #168]
	bl	DT1_IsBiggerThan_DT2
	subs	r6, r0, #0
	addne	r2, sp, #52
	movne	r6, #0
	strne	r2, [sp, #36]
	bne	.L269
	b	.L316
.L281:
	ldr	r7, .L317+36
.LBB90:
.LBB91:
	ldr	fp, .L317+40
.LBE91:
	ldr	r9, .L317+44
.LBE90:
	add	r6, r6, #1
	mov	r5, #0
.LBB93:
	mov	sl, r6
.L274:
	ldr	r2, .L317+36
	mov	r3, #472
	mla	r3, r5, r3, r2
	ldr	r2, [r3, #24]
	add	r3, r3, #20
	cmp	r2, #0
	beq	.L271
	ldr	r6, [r3, #24]
	ldr	r3, [r6, #0]
	cmp	r3, r4
	bne	.L272
	ldr	r3, [r6, r9]
	cmn	r3, #1
	beq	.L272
	mov	r0, r5
	bl	nm_handle_budget_reset_record
	mvn	r3, #0
	str	r3, [r6, r9]
.L272:
	ldr	r3, [r6, #0]
	cmp	r3, r4
	bne	.L271
.LBB92:
	ldr	r3, .L317+48
	mov	r1, #400
	ldr	r2, .L317+16
	ldr	r0, [fp, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r7, #24]
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	ldr	r1, [sp, #48]
	mov	r6, #22
	sub	r1, r1, #1
	mov	r8, r0
	bl	POC_get_budget
	mov	r3, r0
.L273:
	mov	r1, r6
	mov	r0, r8
	str	r3, [sp, #16]
	bl	POC_get_budget
	add	r1, r6, #1
	sub	r6, r6, #1
	mov	r2, r0
	mov	r0, r8
	bl	POC_set_budget
	cmn	r6, #1
	ldr	r3, [sp, #16]
	bne	.L273
	mov	r0, r8
	mov	r1, #0
	mov	r2, r3
	bl	POC_set_budget
	ldr	r0, [fp, #0]
	bl	xQueueGiveMutexRecursive
.L271:
.LBE92:
.LBE93:
	add	r5, r5, #1
	cmp	r5, #12
	add	r7, r7, #472
	bne	.L274
	ldr	r3, [sp, #28]
	mov	r6, sl
.L275:
.LBB94:
	ldr	r2, [r3, #-4]!
	ldr	r0, [sp, #36]
	str	r2, [r3, #4]
	cmp	r3, r0
	bne	.L275
	add	r3, sp, #188
	str	r3, [sp, #0]
	ldr	r0, [sp, #56]
	add	r1, sp, #176
	add	r2, sp, #180
	add	r3, sp, #184
	bl	DateToDMY
	ldr	r3, [sp, #48]
	add	r0, sp, #176
	cmp	r3, #12
	ldr	r3, [sp, #180]
	add	r7, sp, #48
	subeq	r3, r3, #1
	subne	r3, r3, #2
	str	r3, [sp, #180]
	ldr	r3, [sp, #180]
	mov	r5, #0
	cmp	r3, #0
	addlt	r3, r3, #11
	strlt	r3, [sp, #180]
	ldrlt	r3, [sp, #184]
	mov	r8, #9
	sublt	r3, r3, #1
	strlt	r3, [sp, #184]
	ldmia	r0, {r0, r1, r2}
	bl	DMYToDate
	mov	sl, #1
	str	r0, [sp, #52]
.L279:
	ldr	r9, [r7, #4]!
	bl	FLOWSENSE_get_controller_index
	mov	r1, #9
	mov	fp, r0
	ldr	r0, [sp, #20]
	bl	SYSTEM_get_change_bits_ptr
	mov	r1, r5
	mov	r2, r9
	mov	r3, #0
	add	r5, r5, #1
	stmia	sp, {r8, fp}
	str	sl, [sp, #8]
	str	r0, [sp, #12]
	ldr	r0, [sp, #20]
	bl	nm_SYSTEM_set_budget_period
	cmp	r5, #24
	bne	.L279
.LBE94:
	ldr	r3, [sp, #52]
	strh	r3, [sp, #164]	@ movhi
	ldr	r3, [sp, #44]
	str	r3, [sp, #160]
.L269:
	add	r0, sp, #160
	add	r1, sp, #152
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #0
	beq	.L280
	cmp	r6, #24
	bne	.L281
	b	.L282
.L280:
	cmp	r6, #24
	bne	.L268
.L282:
	ldr	r0, .L317+52
	b	.L315
.L316:
	add	r0, sp, #168
	mov	r1, r5
	bl	DT1_IsBiggerThan_DT2
	subs	r7, r0, #0
	beq	.L283
.LBB95:
	ldr	r8, .L317+36
	ldr	r7, .L317+44
	mov	sl, #472
.L285:
	mla	r3, sl, r6, r8
	ldr	r2, [r3, #24]
	add	r3, r3, #20
	cmp	r2, #0
	beq	.L284
	ldr	r5, [r3, #24]
	ldr	r3, [r5, r7]
	sub	r3, r3, #1
	cmn	r3, #3
	bls	.L284
	ldr	r3, [r5, #0]
	cmp	r3, r4
	bne	.L284
	mov	r0, r6
	bl	nm_handle_budget_reset_record
	ldrh	r3, [sp, #156]
	str	r3, [r5, r7]
.L284:
.LBE95:
	add	r6, r6, #1
	cmp	r6, #12
	bne	.L285
	ldr	r3, [sp, #40]
	cmp	r3, #0
	beq	.L268
	ldr	r3, [sp, #44]
	ldr	r2, [sp, #152]
	cmp	r2, r3
	bne	.L286
	add	r3, sp, #236
	ldmia	r3, {r0, r1, r2}
	stmia	sp, {r0, r1, r2}
	add	r0, sp, #224
	ldmib	r0, {r2, r3}
	add	r1, sp, #40
	mov	r0, r4
	bl	nm_handle_sending_budget_record
.L286:
	ldr	r3, [sp, #152]
	cmp	r3, #0
	bne	.L268
	add	r3, sp, #240
	ldmia	r3, {r0, r1}
	stmia	sp, {r0, r1}
	add	r0, sp, #224
	ldmib	r0, {r1, r2, r3}
	mov	r0, r4
	bl	BUDGET_handle_alerts_at_midnight
	b	.L268
.L298:
.LBB96:
	ldr	sl, .L317+36
	ldr	r9, .L317+56
.LBB97:
	ldr	r8, .L317+40
.LBE97:
.LBE96:
	add	r7, r7, #1
	mov	r5, #0
.LBB100:
.LBB98:
	mov	r6, r4
.L288:
.LBE98:
	mov	r4, #472
	mla	r4, r5, r4, sl
	ldr	r3, [r4, #24]
	add	r4, r4, #20
	cmp	r3, #0
	beq	.L287
	ldr	r3, [r4, #24]
	ldr	r2, [r3, #0]
	cmp	r2, r6
	bne	.L287
	mov	r2, #1
	str	r2, [r3, r9]
	mov	r0, r5
	bl	POC_use_for_budget
	cmp	r0, #0
	beq	.L287
	ldr	r3, [sp, #40]
	cmp	r3, #0
	beq	.L287
.LBB99:
	ldr	r2, .L317+16
	ldr	r3, .L317+60
	mov	r1, #400
	ldr	r0, [r8, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r4, #4]
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	mov	r4, r0
	bl	nm_GROUP_get_name
	mov	r1, #0
	mov	fp, r0
	mov	r0, r4
	bl	POC_get_budget
	mov	r4, r0
	mov	r0, r5
	bl	nm_BUDGET_get_used
	fmdrr	d7, r0, r1
	mov	r0, fp
	mov	r1, r4
	ftouizd	s15, d7
	fmrs	r2, s15	@ int
	bl	Alert_budget_period_ended
	ldr	r0, [r8, #0]
	bl	xQueueGiveMutexRecursive
.L287:
.LBE99:
.LBE100:
	add	r5, r5, #1
	cmp	r5, #12
	bne	.L288
	ldr	r3, [sp, #40]
	mov	r4, r6
	cmp	r3, #0
	beq	.L289
	add	r3, sp, #236
	ldmia	r3, {r0, r1, r2}
	stmia	sp, {r0, r1, r2}
	add	r0, sp, #224
	ldmib	r0, {r2, r3}
	add	r1, sp, #40
	mov	r0, r6
	bl	nm_handle_sending_budget_record
.L289:
	ldr	r5, .L317+36
.LBB101:
.LBB102:
	ldr	r9, .L317+40
.LBE102:
.LBE101:
	mov	r6, #0
.LBB105:
.LBB103:
	mov	sl, r7
.L292:
.LBE103:
	ldr	r1, .L317+36
	mov	r3, #472
	mla	r3, r6, r3, r1
	ldr	r3, [r3, #24]
	cmp	r3, #0
	beq	.L290
	ldr	r8, [r5, #44]
	ldr	r3, [r8, #0]
	cmp	r3, r4
	bne	.L290
	mov	r0, r6
	bl	nm_handle_budget_reset_record
	ldr	r2, .L317+44
	mov	r7, #0
	str	r7, [r8, r2]
.LBB104:
	mov	r1, #400
	ldr	r2, .L317+16
	ldr	r3, .L317+64
	ldr	r0, [r9, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, #24]
	bl	POC_get_index_for_group_with_this_GID
	bl	POC_get_group_at_this_index
	mov	r1, r7
	mov	r7, #1
	mov	r8, r0
	bl	POC_get_budget
	mov	fp, r0
.L291:
	mov	r1, r7
	mov	r0, r8
	bl	POC_get_budget
	sub	r1, r7, #1
	add	r7, r7, #1
	mov	r2, r0
	mov	r0, r8
	bl	POC_set_budget
	cmp	r7, #24
	bne	.L291
	ldr	r1, [sp, #48]
	mov	r0, r8
	sub	r1, r1, #1
	mov	r2, fp
	bl	POC_set_budget
	ldr	r0, [r9, #0]
	bl	xQueueGiveMutexRecursive
.L290:
.LBE104:
.LBE105:
	add	r6, r6, #1
	cmp	r6, #12
	add	r5, r5, #472
	bne	.L292
	mov	r7, sl
	add	r3, sp, #52
.L293:
.LBB106:
	ldr	r2, [r3, #4]!
	ldr	r1, [sp, #28]
	str	r2, [r3, #-4]
	cmp	r3, r1
	bne	.L293
	add	r6, sp, #48
	mov	r8, r6
	mov	r5, #0
	b	.L294
.L295:
	add	r3, sp, #176
	str	r3, [sp, #0]
	add	r1, sp, #188
	add	r3, sp, #180
	ldr	r0, [r8, #4]!
	add	r2, sp, #184
	bl	DateToDMY
	ldr	r2, [sp, #180]
	ldr	r1, [sp, #184]
	add	r2, r2, #1
	ldr	r0, [sp, #188]
	str	r2, [sp, #180]
	bl	DMYToDate
	ldr	r3, [sp, #48]
	add	r1, sp, #192
	add	r3, r5, r3
	add	r3, r1, r3, asl #2
	add	r5, r5, #1
	str	r0, [r3, #-140]
.L294:
	ldr	r3, [sp, #48]
	cmp	r5, r3
	bcc	.L295
	mov	r5, #0
	mov	r8, #9
	mov	sl, #1
.L296:
	ldr	r9, [r6, #4]!
	bl	FLOWSENSE_get_controller_index
	mov	r1, #9
	mov	fp, r0
	ldr	r0, [sp, #20]
	bl	SYSTEM_get_change_bits_ptr
	mov	r1, r5
	mov	r2, r9
	mov	r3, #0
	add	r5, r5, #1
	stmia	sp, {r8, fp}
	str	sl, [sp, #8]
	str	r0, [sp, #12]
	ldr	r0, [sp, #20]
	bl	nm_SYSTEM_set_budget_period
	cmp	r5, #24
	bne	.L296
.LBE106:
	ldr	r3, [sp, #56]
	strh	r3, [sp, #172]	@ movhi
	ldr	r3, [sp, #44]
	str	r3, [sp, #168]
.L283:
	add	r0, sp, #152
	add	r1, sp, #168
	bl	DT1_IsBiggerThanOrEqualTo_DT2
	cmp	r0, #0
	beq	.L297
	cmp	r7, #24
	bne	.L298
	b	.L299
.L297:
	cmp	r7, #24
	bne	.L268
.L299:
	ldr	r0, .L317+68
.L315:
	bl	Alert_Message
.L268:
	ldr	r2, [sp, #24]
	add	r2, r2, #1
	str	r2, [sp, #24]
.L267:
	ldr	r3, [sp, #24]
	ldr	r0, [sp, #32]
	cmp	r3, r0
	bne	.L300
	ldr	r3, .L317+32
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L317+24
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L317+12
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L264:
	add	sp, sp, #192
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	add	sp, sp, #16
	bx	lr
.L318:
	.align	2
.L317:
	.word	.LANCHOR5
	.word	365
	.word	.LC4
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	2270
	.word	system_preserves_recursive_MUTEX
	.word	2271
	.word	poc_preserves_recursive_MUTEX
	.word	poc_preserves
	.word	list_poc_recursive_MUTEX
	.word	14172
	.word	1337
	.word	.LC5
	.word	14164
	.word	2441
	.word	1373
	.word	.LC6
.LFE27:
	.size	BUDGET_timer_upkeep, .-BUDGET_timer_upkeep
	.global	g_BUDGET_using_et_calculate_budget
	.section	.data.SIXTY.8002,"aw",%progbits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	SIXTY.8002, %object
	.size	SIXTY.8002, 4
SIXTY.8002:
	.word	1114636288
	.section	.bss.dt_flag.8209,"aw",%nobits
	.set	.LANCHOR0,. + 0
	.type	dt_flag.8209, %object
	.size	dt_flag.8209, 6
dt_flag.8209:
	.space	6
	.section	.bss.saved_gid.8210,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	saved_gid.8210, %object
	.size	saved_gid.8210, 4
saved_gid.8210:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/budg"
	.ascii	"ets/budgets.c\000"
.LC1:
	.ascii	"BUDGET %d/%d - No measured volume\000"
.LC2:
	.ascii	"Invalid budget mode: %d, POC gid: %d\000"
.LC3:
	.ascii	"Calc_time (%d): M: %d R: %0.2f Vp: %0.0f\000"
.LC4:
	.ascii	"caught BUG DATE: %d/%d/%d\000"
.LC5:
	.ascii	"Budget Rollback Check: Too far back\000"
.LC6:
	.ascii	"Budget Rollover Check: BAD DATE DATA\000"
	.section	.bss.g_BUDGET_using_et_calculate_budget,"aw",%nobits
	.align	2
	.type	g_BUDGET_using_et_calculate_budget, %object
	.size	g_BUDGET_using_et_calculate_budget, 4
g_BUDGET_using_et_calculate_budget:
	.space	4
	.section	.data.SIXTY.8025,"aw",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	SIXTY.8025, %object
	.size	SIXTY.8025, 4
SIXTY.8025:
	.word	1114636288
	.section	.bss.dt_bug_check.8299,"aw",%nobits
	.set	.LANCHOR5,. + 0
	.type	dt_bug_check.8299, %object
	.size	dt_bug_check.8299, 6
dt_bug_check.8299:
	.space	6
	.section	.data.HUNDRED.8026,"aw",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	HUNDRED.8026, %object
	.size	HUNDRED.8026, 4
HUNDRED.8026:
	.word	1120403456
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI0-.LFB10
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x1c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI3-.LFB6
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI4-.LFB13
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x54
	.uleb128 0x7
	.byte	0x5
	.uleb128 0x52
	.uleb128 0x9
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI7-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x24
	.byte	0x5
	.uleb128 0x54
	.uleb128 0x5
	.byte	0x5
	.uleb128 0x52
	.uleb128 0x7
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI10-.LFB11
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x2c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI13-.LFB21
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x30
	.byte	0x5
	.uleb128 0x54
	.uleb128 0x8
	.byte	0x5
	.uleb128 0x52
	.uleb128 0xa
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xc
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0xb4
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI16-.LFB22
	.byte	0xe
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x2
	.byte	0x8b
	.uleb128 0x3
	.byte	0x8a
	.uleb128 0x4
	.byte	0x89
	.uleb128 0x5
	.byte	0x88
	.uleb128 0x6
	.byte	0x87
	.uleb128 0x7
	.byte	0x86
	.uleb128 0x8
	.byte	0x85
	.uleb128 0x9
	.byte	0x84
	.uleb128 0xa
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x40
	.byte	0x5
	.uleb128 0x54
	.uleb128 0xc
	.byte	0x5
	.uleb128 0x52
	.uleb128 0xe
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x10
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xe
	.uleb128 0xb8
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI20-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x3
	.byte	0x8b
	.uleb128 0x4
	.byte	0x8a
	.uleb128 0x5
	.byte	0x89
	.uleb128 0x6
	.byte	0x88
	.uleb128 0x7
	.byte	0x87
	.uleb128 0x8
	.byte	0x86
	.uleb128 0x9
	.byte	0x85
	.uleb128 0xa
	.byte	0x84
	.uleb128 0xb
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xe
	.uleb128 0x34
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xd
	.byte	0x4
	.4byte	.LCFI23-.LCFI22
	.byte	0xe
	.uleb128 0x554
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xe
	.uleb128 0x560
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI25-.LFB24
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI26-.LCFI25
	.byte	0xe
	.uleb128 0x18
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xe
	.uleb128 0x88
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI28-.LFB26
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI29-.LCFI28
	.byte	0xe
	.uleb128 0x1c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI30-.LCFI29
	.byte	0xe
	.uleb128 0x8c
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI31-.LFB28
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI32-.LCFI31
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI33-.LFB23
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xe
	.uleb128 0x3c
	.byte	0x5
	.uleb128 0x54
	.uleb128 0xb
	.byte	0x5
	.uleb128 0x52
	.uleb128 0xd
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xf
	.byte	0x4
	.4byte	.LCFI35-.LCFI34
	.byte	0xe
	.uleb128 0xf0
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI36-.LFB29
	.byte	0xe
	.uleb128 0x34
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x83
	.uleb128 0xa
	.byte	0x82
	.uleb128 0xb
	.byte	0x81
	.uleb128 0xc
	.byte	0x80
	.uleb128 0xd
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI37-.LFB30
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI38-.LCFI37
	.byte	0xe
	.uleb128 0x28
	.byte	0x5
	.uleb128 0x54
	.uleb128 0x6
	.byte	0x5
	.uleb128 0x52
	.uleb128 0x8
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xa
	.byte	0x4
	.4byte	.LCFI39-.LCFI38
	.byte	0xe
	.uleb128 0x74
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI40-.LFB33
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI41-.LCFI40
	.byte	0xe
	.uleb128 0x3c
	.byte	0x5
	.uleb128 0x54
	.uleb128 0xb
	.byte	0x5
	.uleb128 0x52
	.uleb128 0xd
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xf
	.byte	0x4
	.4byte	.LCFI42-.LCFI41
	.byte	0xe
	.uleb128 0x134
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI43-.LFB25
	.byte	0xe
	.uleb128 0xc
	.byte	0x4
	.4byte	.LCFI44-.LCFI43
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x5
	.byte	0x8a
	.uleb128 0x6
	.byte	0x89
	.uleb128 0x7
	.byte	0x88
	.uleb128 0x8
	.byte	0x87
	.uleb128 0x9
	.byte	0x86
	.uleb128 0xa
	.byte	0x85
	.uleb128 0xb
	.byte	0x84
	.uleb128 0xc
	.byte	0x4
	.4byte	.LCFI45-.LCFI44
	.byte	0xe
	.uleb128 0x58
	.byte	0x5
	.uleb128 0x58
	.uleb128 0xe
	.byte	0x5
	.uleb128 0x56
	.uleb128 0x10
	.byte	0x5
	.uleb128 0x54
	.uleb128 0x12
	.byte	0x5
	.uleb128 0x52
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x16
	.byte	0x4
	.4byte	.LCFI46-.LCFI45
	.byte	0xe
	.uleb128 0x104
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI47-.LFB27
	.byte	0xe
	.uleb128 0x10
	.byte	0x4
	.4byte	.LCFI48-.LCFI47
	.byte	0xe
	.uleb128 0x34
	.byte	0x8e
	.uleb128 0x5
	.byte	0x8b
	.uleb128 0x6
	.byte	0x8a
	.uleb128 0x7
	.byte	0x89
	.uleb128 0x8
	.byte	0x88
	.uleb128 0x9
	.byte	0x87
	.uleb128 0xa
	.byte	0x86
	.uleb128 0xb
	.byte	0x85
	.uleb128 0xc
	.byte	0x84
	.uleb128 0xd
	.byte	0x4
	.4byte	.LCFI49-.LCFI48
	.byte	0xe
	.uleb128 0xf4
	.align	2
.LEFDE38:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/budgets/budgets.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x238
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF34
	.byte	0x1
	.4byte	.LASF35
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x108
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x5e2
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x5f9
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0x7c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.byte	0x9c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0xab3
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x1
	.byte	0x64
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x33a
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST0
	.uleb128 0x6
	.4byte	.LASF8
	.byte	0x1
	.byte	0x4a
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x160
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST2
	.uleb128 0x5
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x43f
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST3
	.uleb128 0x7
	.4byte	0x21
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1e3
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x36e
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST5
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x624
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST6
	.uleb128 0x2
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x20d
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x1
	.byte	0xc8
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x1b4
	.byte	0x1
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x6d0
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST7
	.uleb128 0x5
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x4c1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST8
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x767
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST9
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x861
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST10
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x9d1
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST11
	.uleb128 0x2
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x3e0
	.byte	0x1
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x6fa
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST12
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.2byte	0xa0b
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST13
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.2byte	0xa2b
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST14
	.uleb128 0x2
	.4byte	.LASF26
	.byte	0x1
	.2byte	0xa76
	.byte	0x1
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.2byte	0xadd
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST15
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x7a8
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST16
	.uleb128 0x2
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x52f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x576
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x553
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x5b0
	.byte	0x1
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x8b0
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST17
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB10
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI2
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB6
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB13
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI6
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI9
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB11
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI12
	.4byte	.LFE11
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB21
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	.LCFI15
	.4byte	.LFE21
	.2byte	0x3
	.byte	0x7d
	.sleb128 180
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB22
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	.LCFI19
	.4byte	.LFE22
	.2byte	0x3
	.byte	0x7d
	.sleb128 184
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB14
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	.LCFI22
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x3
	.byte	0x7d
	.sleb128 1364
	.4byte	.LCFI24
	.4byte	.LFE14
	.2byte	0x3
	.byte	0x7d
	.sleb128 1376
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI27
	.4byte	.LFE24
	.2byte	0x3
	.byte	0x7d
	.sleb128 136
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB26
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI29
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI30
	.4byte	.LFE26
	.2byte	0x3
	.byte	0x7d
	.sleb128 140
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB28
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI32
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB23
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI34
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	.LCFI35
	.4byte	.LFE23
	.2byte	0x3
	.byte	0x7d
	.sleb128 240
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB29
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB30
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI38
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	.LCFI39
	.4byte	.LFE30
	.2byte	0x3
	.byte	0x7d
	.sleb128 116
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB33
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI41
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	.LCFI42
	.4byte	.LFE33
	.2byte	0x3
	.byte	0x7d
	.sleb128 308
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB25
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI44
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	.LCFI45
	.4byte	.LCFI46
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	.LCFI46
	.4byte	.LFE25
	.2byte	0x3
	.byte	0x7d
	.sleb128 260
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB27
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI48
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	.LCFI49
	.4byte	.LFE27
	.2byte	0x3
	.byte	0x7d
	.sleb128 244
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xb4
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF11:
	.ascii	"calc_percent_adjust\000"
.LASF21:
	.ascii	"nm_BUDGET_get_used\000"
.LASF30:
	.ascii	"nm_handle_budget_period_rollback\000"
.LASF23:
	.ascii	"BUDGET_calculate_ratios\000"
.LASF34:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF35:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/budg"
	.ascii	"ets/budgets.c\000"
.LASF32:
	.ascii	"nm_handle_budget_period_updates\000"
.LASF22:
	.ascii	"nm_calc_POC_ratios\000"
.LASF7:
	.ascii	"nm_BUDGET_get_poc_usage\000"
.LASF15:
	.ascii	"getNumStarts\000"
.LASF9:
	.ascii	"getNumManualStarts\000"
.LASF31:
	.ascii	"nm_handle_budget_value_updates\000"
.LASF10:
	.ascii	"calc_time_adjustment_low_level\000"
.LASF16:
	.ascii	"getIrrigationListVp\000"
.LASF1:
	.ascii	"nm_BUDGET_increment_off_at_start_time\000"
.LASF18:
	.ascii	"nm_handle_sending_budget_record\000"
.LASF13:
	.ascii	"nm_BUDGET_calc_time_adjustment\000"
.LASF19:
	.ascii	"BUDGET_in_effect_for_any_system\000"
.LASF17:
	.ascii	"nm_BUDGET_predicted_volume\000"
.LASF0:
	.ascii	"getVolume\000"
.LASF6:
	.ascii	"nm_handle_budget_reset_record\000"
.LASF33:
	.ascii	"BUDGET_timer_upkeep\000"
.LASF4:
	.ascii	"nm_reset_budget_values\000"
.LASF2:
	.ascii	"nm_BUDGET_increment_on_at_start_time\000"
.LASF26:
	.ascii	"BUDGET_total_square_footage\000"
.LASF12:
	.ascii	"nm_BUDGET_calc_rpoc\000"
.LASF24:
	.ascii	"BUDGET_reset_budget_values\000"
.LASF14:
	.ascii	"getScheduledVp\000"
.LASF8:
	.ascii	"update_running_dtcs\000"
.LASF20:
	.ascii	"BUDGET_handle_alerts_at_start_time\000"
.LASF5:
	.ascii	"BUDGET_calculate_gallons_per_inch_for_square_footag"
	.ascii	"e_of_mainline\000"
.LASF27:
	.ascii	"BUDGET_calc_POC_monthly_budget_using_et\000"
.LASF28:
	.ascii	"BUDGET_handle_alerts_at_midnight\000"
.LASF25:
	.ascii	"BUDGET_calculate_square_footage\000"
.LASF29:
	.ascii	"nm_handle_budget_value_rollback\000"
.LASF3:
	.ascii	"nm_reset_meter_read_dates\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
