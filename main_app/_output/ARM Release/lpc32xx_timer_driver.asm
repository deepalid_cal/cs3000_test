	.file	"lpc32xx_timer_driver.c"
	.text
.Ltext0:
	.global	__udivdi3
	.section	.text.timer_usec_to_val,"ax",%progbits
	.align	2
	.global	timer_usec_to_val
	.type	timer_usec_to_val, %function
timer_usec_to_val:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r6, r7, lr}
.LCFI0:
	mov	r6, #1000
	mov	r7, #0
	mov	r4, r1
	bl	clkpwr_get_clock_rate
	mov	r2, r6
	mov	r3, r7
	mov	r1, #0
	bl	__udivdi3
	umull	r2, r3, r0, r4
	mla	r3, r4, r1, r3
	mov	r0, r2
	mov	r1, r3
	mov	r2, r6
	mov	r3, r7
	bl	__udivdi3
	ldmfd	sp!, {r4, r6, r7, pc}
.LFE0:
	.size	timer_usec_to_val, .-timer_usec_to_val
	.section	.text.timer_ptr_to_timer_num,"ax",%progbits
	.align	2
	.global	timer_ptr_to_timer_num
	.type	timer_ptr_to_timer_num, %function
timer_ptr_to_timer_num:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L11
	cmp	r0, r3
	moveq	r0, #0
	bxeq	lr
	add	r3, r3, #32768
	cmp	r0, r3
	moveq	r0, #1
	bxeq	lr
	add	r3, r3, #49152
	cmp	r0, r3
	moveq	r0, #2
	bxeq	lr
	add	r3, r3, #32768
	cmp	r0, r3
	beq	.L8
.LBB4:
	sub	r3, r3, #212992
	cmp	r0, r3
	moveq	r0, #4
	bxeq	lr
	add	r3, r3, #16384
	cmp	r0, r3
	moveq	r0, #5
	mvnne	r0, #0
	bx	lr
.L8:
.LBE4:
	mov	r0, #3
	bx	lr
.L12:
	.align	2
.L11:
	.word	1074020352
.LFE1:
	.size	timer_ptr_to_timer_num, .-timer_ptr_to_timer_num
	.section	.text.timer_cfg_to_timer_num,"ax",%progbits
	.align	2
	.global	timer_cfg_to_timer_num
	.type	timer_cfg_to_timer_num, %function
timer_cfg_to_timer_num:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L22
	cmp	r0, r3
	moveq	r0, #0
	bxeq	lr
	add	r2, r3, #4
	cmp	r0, r2
	moveq	r0, #1
	bxeq	lr
	add	r2, r3, #8
	cmp	r0, r2
	moveq	r0, #2
	bxeq	lr
	add	r2, r3, #12
	cmp	r0, r2
	beq	.L19
.LBB7:
	add	r2, r3, #16
	cmp	r0, r2
	moveq	r0, #4
	bxeq	lr
	add	r3, r3, #20
	cmp	r0, r3
	moveq	r0, #5
	mvnne	r0, #0
	bx	lr
.L19:
.LBE7:
	mov	r0, #3
	bx	lr
.L23:
	.align	2
.L22:
	.word	.LANCHOR0
.LFE2:
	.size	timer_cfg_to_timer_num, .-timer_cfg_to_timer_num
	.section	.text.timer_delay_cmn,"ax",%progbits
	.align	2
	.global	timer_delay_cmn
	.type	timer_delay_cmn, %function
timer_delay_cmn:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	mov	r4, r0
	mov	r7, r1
	bl	timer_ptr_to_timer_num
	cmp	r0, #0
	ldmltfd	sp!, {r4, r5, r6, r7, pc}
	ldr	r3, .L28
	mov	r1, #1
	ldr	r6, [r3, r0, asl #2]
	mov	r5, #1
	mov	r0, r6
	bl	clkpwr_clk_en_dis
	mov	r3, #2
	str	r3, [r4, #4]
	mov	r3, #0
	str	r3, [r4, #4]
	mov	r0, r6
	str	r5, [r4, #0]
	mov	r1, r5
	str	r3, [r4, #112]
	bl	timer_usec_to_val
	str	r0, [r4, #12]
	str	r7, [r4, #24]
	str	r5, [r4, #20]
	str	r5, [r4, #4]
.L26:
	ldr	r3, [r4, #0]
	tst	r3, #1
	beq	.L26
	mov	r1, #0
	mov	r0, r6
	str	r1, [r4, #4]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	clkpwr_clk_en_dis
.L29:
	.align	2
.L28:
	.word	.LANCHOR1
.LFE3:
	.size	timer_delay_cmn, .-timer_delay_cmn
	.section	.text.timer_open,"ax",%progbits
	.align	2
	.global	timer_open
	.type	timer_open, %function
timer_open:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI2:
	mov	r8, r0
	bl	timer_ptr_to_timer_num
	subs	r5, r0, #0
	movlt	r0, #0
	ldmltfd	sp!, {r4, r5, r6, r7, r8, pc}
	ldr	r3, .L34
	ldr	r4, [r3, r5, asl #2]
	cmp	r4, #0
	bne	.L33
	mov	r1, #1
	str	r1, [r3, r5, asl #2]
	ldr	r3, .L34+4
	ldr	r6, .L34+8
	ldr	r0, [r3, r5, asl #2]
	str	r8, [r6, r5, asl #2]
	bl	clkpwr_clk_en_dis
	ldr	r3, [r6, r5, asl #2]
	add	r7, r6, r5, asl #2
	mov	r2, #255
	str	r4, [r3, #4]
	mov	r0, r7
	str	r4, [r3, #112]
	str	r4, [r3, #20]
	str	r4, [r3, #40]
	str	r4, [r3, #60]
	str	r4, [r3, #8]
	str	r4, [r3, #12]
	str	r4, [r3, #16]
	str	r4, [r3, #24]
	str	r4, [r3, #28]
	str	r4, [r3, #32]
	str	r4, [r3, #36]
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L33:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L35:
	.align	2
.L34:
	.word	.LANCHOR2
	.word	.LANCHOR1
	.word	.LANCHOR0
.LFE4:
	.size	timer_open, .-timer_open
	.section	.text.timer_close,"ax",%progbits
	.align	2
	.global	timer_close
	.type	timer_close, %function
timer_close:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI3:
	bl	timer_cfg_to_timer_num
	cmp	r0, #0
	blt	.L37
	ldr	r3, .L38
	ldr	r2, [r3, r0, asl #2]
	cmp	r2, #1
	bne	.L37
	mov	r1, #0
	str	r1, [r3, r0, asl #2]
	ldr	r3, .L38+4
	ldr	r3, [r3, r0, asl #2]
	str	r1, [r3, #4]
	str	r1, [r3, #112]
	str	r1, [r3, #20]
	str	r1, [r3, #40]
	str	r1, [r3, #60]
	ldr	r3, .L38+8
	ldr	r0, [r3, r0, asl #2]
	bl	clkpwr_clk_en_dis
.L37:
	mvn	r0, #0
	ldr	pc, [sp], #4
.L39:
	.align	2
.L38:
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE5:
	.size	timer_close, .-timer_close
	.section	.text.timer_ioctl,"ax",%progbits
	.align	2
	.global	timer_ioctl
	.type	timer_ioctl, %function
timer_ioctl:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI4:
	mov	r6, r1
	mov	r7, r2
	bl	timer_cfg_to_timer_num
	cmp	r0, #0
	blt	.L69
	ldr	r3, .L84
	ldr	r3, [r3, r0, asl #2]
	cmp	r3, #1
	bne	.L69
	ldr	r3, .L84+4
	ldr	r4, [r3, r0, asl #2]
	cmp	r6, #9
	ldrls	pc, [pc, r6, asl #2]
	b	.L78
.L52:
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
	.word	.L48
	.word	.L49
	.word	.L50
	.word	.L51
.L42:
	ldr	r3, [r4, #4]
	cmp	r7, #0
	orrne	r3, r3, #1
	biceq	r3, r3, #1
	strne	r3, [r4, #4]
	streq	r3, [r4, #4]
	moveq	r0, r7
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	b	.L83
.L43:
	ldr	r3, [r4, #4]
	orr	r3, r3, #2
	str	r3, [r4, #4]
.L54:
	ldr	r0, [r4, #8]
	cmp	r0, #0
	bne	.L54
	ldr	r3, [r4, #4]
	bic	r3, r3, #2
	str	r3, [r4, #4]
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L44:
	ldr	r5, [r7, #0]
	cmp	r5, #0
	strne	r5, [r4, #12]
	bne	.L83
	ldr	r3, .L84+8
	ldr	r1, [r7, #4]
	ldr	r0, [r3, r0, asl #2]
	bl	timer_usec_to_val
	str	r0, [r4, #12]
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L45:
	ldr	r3, [r7, #0]
	cmp	r3, #1
	moveq	r0, #0
	streq	r0, [r4, #112]
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	ldr	r3, [r7, #4]
	cmp	r3, #1
	ldr	r3, [r7, #8]
	bne	.L57
	cmp	r3, #1
	movne	r3, #1
	moveq	r3, #3
	b	.L58
.L57:
	cmp	r3, #1
	moveq	r3, #2
	movne	r3, #0
.L58:
	ldr	r2, [r7, #12]
	cmp	r2, #3
	orrls	r3, r3, r2, asl #2
	strls	r3, [r4, #112]
	bls	.L83
	b	.L78
.L46:
	ldr	r2, [r7, #0]
	cmp	r2, #3
	bhi	.L78
	ldr	r3, [r7, #16]
	add	r0, r2, r2, asl #1
	add	r2, r2, #6
	str	r3, [r4, r2, asl #2]
	ldr	r3, [r7, #4]
	mov	r1, #1
	ldr	r2, [r7, #8]
	mov	r5, r1, asl r0
	add	ip, r0, r1
	add	r0, r0, #2
	cmp	r3, #1
	mov	ip, r1, asl ip
	mov	r1, r1, asl r0
	moveq	r3, r5
	movne	r3, #0
	cmp	r2, #1
	ldr	r2, [r7, #12]
	orreq	r3, r3, r1
	orr	r0, ip, r1
	ldr	r1, [r4, #20]
	orr	r0, r0, r5
	cmp	r2, #1
	mvn	r0, r0
	orreq	r3, r3, ip
	and	r2, r0, r1
	orr	r3, r3, r2
	str	r3, [r4, #20]
.L83:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L47:
	ldr	r3, [r7, #0]
	cmp	r3, #3
	bhi	.L78
	ldr	ip, [r7, #8]
	mov	r2, #1
	add	r3, r3, r3, asl #1
	add	r0, r3, r2
	add	r1, r3, #2
	mov	r1, r2, asl r1
	mov	r0, r2, asl r0
	cmp	ip, #1
	ldr	ip, [r7, #12]
	mov	r3, r2, asl r3
	orr	r2, r1, r0
	orr	r2, r2, r3
	movne	r3, #0
	cmp	ip, #1
	orreq	r3, r3, r0
	ldr	r0, [r7, #4]
	mvn	r2, r2
	cmp	r0, #1
	orreq	r3, r3, r1
	ldr	r1, [r4, #40]
	and	r2, r2, r1
	orr	r3, r3, r2
	str	r3, [r4, #40]
	b	.L83
.L48:
	ldr	r3, [r7, #0]
	cmp	r3, #3
	bhi	.L78
	ldr	r1, [r7, #4]
	ldr	ip, [r7, #8]
	and	r1, r1, #1
	add	r2, r3, #2
	mov	r1, r1, asl r3
	mov	r2, r2, asl #1
	and	ip, ip, #3
	orr	r1, r1, ip, asl r2
	mov	ip, #1
	mov	r3, ip, asl r3
	ldr	r0, [r4, #60]
	mov	ip, #3
	orr	r2, r3, ip, asl r2
	bic	r2, r0, r2
	orr	r2, r1, r2
	str	r2, [r4, #60]
	b	.L83
.L49:
	str	r7, [r4, #0]
	b	.L83
.L50:
	ldr	r3, [r4, #44]
	str	r3, [r7, #8]
	ldr	r3, [r4, #48]
	str	r3, [r7, #12]
	ldr	r3, [r4, #52]
	str	r3, [r7, #16]
	ldr	r3, [r4, #56]
	str	r3, [r7, #20]
	ldr	r3, [r4, #16]
	str	r3, [r7, #4]
	ldr	r3, [r4, #8]
	str	r3, [r7, #0]
	b	.L83
.L51:
	cmp	r7, #1
	ldreq	r0, [r4, #8]
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	cmp	r7, #2
	ldreq	r0, [r4, #0]
	ldmeqfd	sp!, {r4, r5, r6, r7, pc}
	cmp	r7, #0
	bne	.L81
	ldr	r3, .L84+8
	ldr	r0, [r3, r0, asl #2]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	clkpwr_get_clock_rate
.L69:
	mvn	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L78:
	mvn	r0, #6
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L81:
	mvn	r0, #6
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L85:
	.align	2
.L84:
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE6:
	.size	timer_ioctl, .-timer_ioctl
	.section	.text.timer_read,"ax",%progbits
	.align	2
	.global	timer_read
	.type	timer_read, %function
timer_read:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
.LFE7:
	.size	timer_read, .-timer_read
	.section	.text.timer_write,"ax",%progbits
	.align	2
	.global	timer_write
	.type	timer_write, %function
timer_write:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
.LFE8:
	.size	timer_write, .-timer_write
	.section	.text.timer_wait_ms,"ax",%progbits
	.align	2
	.global	timer_wait_ms
	.type	timer_wait_ms, %function
timer_wait_ms:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #1000
	mul	r1, r3, r1
	b	timer_delay_cmn
.LFE9:
	.size	timer_wait_ms, .-timer_wait_ms
	.section	.text.timer_wait_us,"ax",%progbits
	.align	2
	.global	timer_wait_us
	.type	timer_wait_us, %function
timer_wait_us:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	b	timer_delay_cmn
.LFE10:
	.size	timer_wait_us, .-timer_wait_us
	.section	.bss.tmrdat,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	tmrdat, %object
	.size	tmrdat, 24
tmrdat:
	.space	24
	.section	.bss.tmr_init,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	tmr_init, %object
	.size	tmr_init, 24
tmr_init:
	.space	24
	.section	.rodata.timer_num_to_clk_enum,"a",%progbits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	timer_num_to_clk_enum, %object
	.size	timer_num_to_clk_enum, 24
timer_num_to_clk_enum:
	.word	21
	.word	20
	.word	19
	.word	18
	.word	23
	.word	22
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI3-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI4-.LFB6
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE20:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_timer_driver.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x107
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF11
	.byte	0x1
	.4byte	.LASF12
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x73
	.byte	0x1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xa6
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x51
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	0x2a
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xda
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x121
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x16e
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1a7
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x2b1
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x2cd
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x2eb
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x307
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF7:
	.ascii	"timer_read\000"
.LASF4:
	.ascii	"timer_open\000"
.LASF3:
	.ascii	"timer_delay_cmn\000"
.LASF11:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF6:
	.ascii	"timer_ioctl\000"
.LASF9:
	.ascii	"timer_wait_ms\000"
.LASF2:
	.ascii	"timer_usec_to_val\000"
.LASF0:
	.ascii	"timer_ptr_to_timer_num\000"
.LASF1:
	.ascii	"timer_cfg_to_timer_num\000"
.LASF5:
	.ascii	"timer_close\000"
.LASF10:
	.ascii	"timer_wait_us\000"
.LASF12:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_timer_driver.c\000"
.LASF8:
	.ascii	"timer_write\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
