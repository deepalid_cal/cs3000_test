	.file	"e_lr_programming_freewave.c"
	.text
.Ltext0:
	.section	.text.FDTO_LR_PROGRAMMING_lrs_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_LR_PROGRAMMING_lrs_draw_screen
	.type	FDTO_LR_PROGRAMMING_lrs_draw_screen, %function
FDTO_LR_PROGRAMMING_lrs_draw_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI0:
	mov	r4, r0
	mov	r5, r1
	bne	.L2
.LBB6:
	ldr	r3, .L6
	ldr	r6, .L6+4
	ldr	r1, .L6+8
	mov	r2, #9
	ldr	r0, .L6+12
	str	r3, [r6, #0]
	bl	strlcpy
	ldr	r3, .L6+16
	mov	r2, #2
	str	r2, [r3, #0]
	ldr	r3, .L6+20
	mov	r7, #0
	str	r5, [r3, #0]
	ldr	r3, .L6+24
.LBE6:
	ldr	r0, .L6+28
.LBB7:
	str	r4, [r3, #0]
	ldr	r3, .L6+32
	str	r7, [r3, #0]
	ldr	r3, .L6+36
	str	r7, [r3, #0]
	ldr	r3, .L6+40
	str	r7, [r3, #0]
	ldr	r3, .L6+44
	str	r7, [r3, #0]
.LBE7:
	bl	e_SHARED_index_keycode_for_gui
	str	r0, [r6, #0]
	b	.L3
.L2:
	ldr	r3, .L6+48
	ldrsh	r7, [r3, #0]
	cmn	r7, #1
	ldreq	r3, .L6+52
	ldreq	r7, [r3, #0]
.L3:
	mov	r1, r7, asl #16
	mov	r0, #29
	mov	r1, r1, asr #16
	mov	r2, #1
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
	cmp	r4, #1
	ldmnefd	sp!, {r4, r5, r6, r7, pc}
	bl	DEVICE_EXCHANGE_draw_dialog
	mov	r0, r5
	mov	r1, #82
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	LR_FREEWAVE_post_device_query_to_queue
.L7:
	.align	2
.L6:
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	.LC0
	.word	GuiVar_LRSerialNumber
	.word	GuiVar_LRMode
	.word	GuiVar_LRPort
	.word	GuiVar_LRGroup
	.word	36867
	.word	GuiVar_LRBeaconRate
	.word	GuiVar_LRSlaveLinksToRepeater
	.word	GuiVar_LRRepeaterInUse
	.word	GuiVar_LRTransmitPower
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
.LFE3:
	.size	FDTO_LR_PROGRAMMING_lrs_draw_screen, .-FDTO_LR_PROGRAMMING_lrs_draw_screen
	.section	.text.FDTO_LR_PROGRAMMING_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_LR_PROGRAMMING_process_device_exchange_key, %function
FDTO_LR_PROGRAMMING_process_device_exchange_key:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	r3, r0, #36864
	cmp	r3, #6
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	mov	r4, r1
	ldmhifd	sp!, {r4, r5, pc}
	mov	r2, #1
	mov	r3, r2, asl r3
	ands	r5, r3, #109
	bne	.L10
	tst	r3, #18
	ldmeqfd	sp!, {r4, r5, pc}
.LBB10:
	bl	LR_FREEWAVE_copy_settings_into_guivars
	ldr	r0, .L12
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L12+4
	str	r0, [r3, #0]
	bl	DIALOG_close_ok_dialog
	mov	r0, r5
	mov	r1, r4
.LBE10:
	ldmfd	sp!, {r4, r5, lr}
.LBB11:
	b	FDTO_LR_PROGRAMMING_lrs_draw_screen
.L10:
.LBE11:
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L12+4
	str	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	DEVICE_EXCHANGE_draw_dialog
.L13:
	.align	2
.L12:
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
.LFE2:
	.size	FDTO_LR_PROGRAMMING_process_device_exchange_key, .-FDTO_LR_PROGRAMMING_process_device_exchange_key
	.section	.text.LR_PROGRAMMING_lrs_process_screen,"ax",%progbits
	.align	2
	.global	LR_PROGRAMMING_lrs_process_screen
	.type	LR_PROGRAMMING_lrs_process_screen, %function
LR_PROGRAMMING_lrs_process_screen:
.LFB4:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI2:
	mov	r3, r1
	sub	sp, sp, #44
.LCFI3:
	mov	r7, r0
	beq	.L20
	bhi	.L24
	cmp	r0, #1
	beq	.L17
	bcc	.L16
	cmp	r0, #2
	beq	.L18
	cmp	r0, #3
	bne	.L15
	b	.L19
.L24:
	cmp	r0, #84
	beq	.L22
	bhi	.L25
	cmp	r0, #67
	beq	.L21
	cmp	r0, #80
	bne	.L15
	b	.L22
.L25:
	sub	r2, r0, #36864
	cmp	r2, #6
	bhi	.L15
	ldr	r3, .L82
	mov	r2, #0
	str	r2, [r3, #0]
	mov	r3, #3
	str	r3, [sp, #8]
	ldr	r3, .L82+4
	str	r0, [sp, #32]
	str	r3, [sp, #28]
	ldr	r3, .L82+8
	add	r0, sp, #8
	ldr	r3, [r3, #0]
	str	r3, [sp, #36]
	bl	Display_Post_Command
	b	.L14
.L18:
	ldr	r4, .L82+12
	ldr	r0, .L82+16
	ldr	r5, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r5, r0
	beq	.L44
	ldr	r0, .L82+20
	ldr	r4, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r4, r0
	beq	.L44
	ldr	r4, .L82+24
	ldrsh	r3, [r4, #0]
	cmp	r3, #8
	beq	.L29
	cmp	r3, #9
	bne	.L44
	b	.L81
.L29:
	bl	good_key_beep
	ldr	r3, .L82+28
	ldrsh	r2, [r4, #0]
	mov	r1, #82
	str	r2, [r3, #0]
	ldr	r3, .L82+8
	ldr	r0, [r3, #0]
	b	.L78
.L81:
	bl	good_key_beep
	ldr	r3, .L82+28
	ldrsh	r2, [r4, #0]
	str	r2, [r3, #0]
	bl	LR_FREEWAVE_extract_changes_from_guivars
	ldr	r3, .L82+8
	mov	r1, #87
	ldr	r0, [r3, #0]
.L78:
	bl	LR_FREEWAVE_post_device_query_to_queue
	b	.L14
.L20:
	ldr	r3, .L82+24
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #2
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L31
.L35:
	.word	.L32
	.word	.L32
	.word	.L33
	.word	.L31
	.word	.L31
	.word	.L31
	.word	.L31
	.word	.L34
.L32:
	mov	r0, #0
	b	.L79
.L33:
	mov	r0, #2
.L79:
	mov	r1, #1
	bl	CURSOR_Select
	b	.L14
.L34:
	ldr	r3, .L82+32
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L47
	cmp	r3, #3
	movne	r0, #5
	bne	.L79
	b	.L80
.L31:
	mov	r0, #1
.L17:
	bl	CURSOR_Up
	b	.L14
.L16:
	ldr	r3, .L82+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #8
	ldrls	pc, [pc, r3, asl #2]
	b	.L19
.L45:
	.word	.L33
	.word	.L33
	.word	.L41
	.word	.L41
	.word	.L19
	.word	.L42
	.word	.L43
	.word	.L43
	.word	.L44
.L41:
	mov	r0, #4
	b	.L79
.L42:
	ldr	r3, .L82+32
	ldr	r3, [r3, #0]
	cmp	r3, #2
	beq	.L47
	cmp	r3, #3
	bne	.L43
.L80:
	mov	r0, #6
	b	.L79
.L47:
	mov	r0, #7
	b	.L79
.L43:
	mov	r0, #8
	b	.L79
.L44:
	bl	bad_key_beep
	b	.L14
.L19:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L14
.L22:
	ldr	r3, .L82+24
	ldrsh	r3, [r3, #0]
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L49
.L58:
	.word	.L50
	.word	.L51
	.word	.L52
	.word	.L53
	.word	.L54
	.word	.L55
	.word	.L56
	.word	.L57
.L50:
	bl	good_key_beep
.LBB12:
	cmp	r7, #80
	beq	.L60
	cmp	r7, #84
	bne	.L59
	ldr	r3, .L82+32
	ldr	r2, [r3, #0]
	cmp	r2, #2
	beq	.L74
	cmp	r2, #3
	bne	.L67
	b	.L66
.L60:
	ldr	r3, .L82+32
	ldr	r2, [r3, #0]
	cmp	r2, #2
	beq	.L66
	cmp	r2, #3
	bne	.L74
	b	.L67
.L66:
	mov	r2, #7
	b	.L75
.L67:
	mov	r2, #2
	b	.L75
.L74:
	mov	r2, #3
.L75:
	str	r2, [r3, #0]
.L59:
.LBE12:
	mov	r0, #0
	bl	Redraw_Screen
	b	.L68
.L52:
	ldr	r6, .L82+36
	ldr	r5, .L82+40
	mov	r3, #1
	mov	r4, #0
	stmia	sp, {r3, r4}
	ldr	r2, .L82+44
	mov	r3, r5
	mov	r0, r7
	mov	r1, r6
	bl	process_uns32
	ldr	r3, [r6, #0]
	cmp	r3, r5
	ldreq	r3, .L82+48
	streq	r4, [r3, #0]
	b	.L68
.L53:
	ldr	r3, .L82+36
	ldr	r2, [r3, #0]
	ldr	r3, .L82+52
	cmp	r2, r3
	bhi	.L49
	mov	r3, #125
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r7
	ldr	r1, .L82+48
	ldr	r3, .L82+56
	b	.L76
.L54:
	ldr	r1, .L82+60
	mov	r3, #0
	mov	r2, #1
	stmia	sp, {r2, r3}
	mov	r0, r7
	mov	r3, #6
.L76:
	bl	process_uns32
	b	.L68
.L51:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r7
	ldr	r1, .L82+64
	mov	r3, #9
	b	.L76
.L55:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r7
	ldr	r1, .L82+68
	mov	r3, #10
	b	.L76
.L56:
	ldr	r0, .L82+72
	b	.L77
.L57:
	ldr	r0, .L82+76
.L77:
	bl	process_bool
	b	.L68
.L49:
	bl	bad_key_beep
.L68:
	bl	Refresh_Screen
	b	.L14
.L21:
	ldr	r3, .L82+80
	mov	r2, #11
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	b	.L14
.L15:
	mov	r0, r7
	mov	r1, r3
	bl	KEY_process_global_keys
.L14:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L83:
	.align	2
.L82:
	.word	LR_PROGRAMMING_querying_device
	.word	FDTO_LR_PROGRAMMING_process_device_exchange_key
	.word	GuiVar_LRPort
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36867
	.word	36870
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	GuiVar_LRMode
	.word	GuiVar_LRFrequency_WholeNum
	.word	470
	.word	435
	.word	GuiVar_LRFrequency_Decimal
	.word	469
	.word	9875
	.word	GuiVar_LRGroup
	.word	GuiVar_LRBeaconRate
	.word	GuiVar_LRTransmitPower
	.word	GuiVar_LRSlaveLinksToRepeater
	.word	GuiVar_LRRepeaterInUse
	.word	GuiVar_MenuScreenToShow
.LFE4:
	.size	LR_PROGRAMMING_lrs_process_screen, .-LR_PROGRAMMING_lrs_process_screen
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"--------\000"
	.section	.bss.g_LR_PROGRAMMING_previous_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_LR_PROGRAMMING_previous_cursor_pos, %object
	.size	g_LR_PROGRAMMING_previous_cursor_pos, 4
g_LR_PROGRAMMING_previous_cursor_pos:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_lr_programming_freewave.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x73
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x8f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0x45
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xb4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	0x29
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xe1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"LR_PROGRAMMING_initialize_guivars\000"
.LASF3:
	.ascii	"FDTO_LR_PROGRAMMING_lrs_draw_screen\000"
.LASF1:
	.ascii	"FDTO_LR_PROGRAMMING_process_device_exchange_key\000"
.LASF2:
	.ascii	"LR_PROGRAMMING_set_LR_mode\000"
.LASF4:
	.ascii	"LR_PROGRAMMING_lrs_process_screen\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_lr_programming_freewave.c\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
