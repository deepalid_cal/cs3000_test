	.file	"walk_thru.c"
	.text
.Ltext0:
	.section	.text.nm_WALK_THRU_set_station_run_time,"ax",%progbits
	.align	2
	.type	nm_WALK_THRU_set_station_run_time, %function
nm_WALK_THRU_set_station_run_time:
.LFB0:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	sub	sp, sp, #44
.LCFI1:
	str	r3, [sp, #16]
	ldr	r3, [sp, #48]
	mov	lr, r1
	mov	r1, #150
	str	r1, [sp, #0]
	str	r3, [sp, #20]
	mov	r1, #30
	ldr	r3, [sp, #52]
	stmib	sp, {r1, r2}
	ldr	r2, .L2
	str	r3, [sp, #24]
	ldr	r3, [sp, #56]
	str	r2, [sp, #12]
	ldr	r2, .L2+4
	str	r3, [sp, #28]
	add	r3, r0, #604
	str	r3, [sp, #32]
	str	r2, [sp, #40]
	mov	r3, #1
	add	r1, r0, #72
	mov	r2, lr
	str	r3, [sp, #36]
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {pc}
.L3:
	.align	2
.L2:
	.word	49602
	.word	.LC0
.LFE0:
	.size	nm_WALK_THRU_set_station_run_time, .-nm_WALK_THRU_set_station_run_time
	.section	.text.nm_WALK_THRU_set_delay_before_start,"ax",%progbits
	.align	2
	.type	nm_WALK_THRU_set_delay_before_start, %function
nm_WALK_THRU_set_delay_before_start:
.LFB1:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	sub	sp, sp, #44
.LCFI3:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L5
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #604
	str	r3, [sp, #32]
	mov	r3, #2
	str	r3, [sp, #36]
	ldr	r3, .L5+4
	mov	ip, #5
	mov	r1, #300
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #76
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L6:
	.align	2
.L5:
	.word	49603
	.word	.LC1
.LFE1:
	.size	nm_WALK_THRU_set_delay_before_start, .-nm_WALK_THRU_set_delay_before_start
	.section	.text.nm_WALK_THRU_set_box_index_0,"ax",%progbits
	.align	2
	.type	nm_WALK_THRU_set_box_index_0, %function
nm_WALK_THRU_set_box_index_0:
.LFB2:
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI4:
	sub	sp, sp, #44
.LCFI5:
	str	r3, [sp, #16]
	ldr	r3, [sp, #52]
	str	r2, [sp, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #56]
	ldr	r2, .L8
	str	r3, [sp, #24]
	ldr	r3, [sp, #60]
	mov	r4, r1
	str	r3, [sp, #28]
	add	r3, r0, #604
	str	r3, [sp, #32]
	mov	r3, #3
	str	r3, [sp, #36]
	ldr	r3, .L8+4
	mov	ip, #0
	mov	r1, #11
	stmia	sp, {r1, ip}
	str	r2, [sp, #12]
	str	r3, [sp, #40]
	add	r1, r0, #80
	mov	r2, r4
	mov	r3, ip
	bl	SHARED_set_uint32_with_32_bit_change_bits_group
	add	sp, sp, #44
	ldmfd	sp!, {r4, pc}
.L9:
	.align	2
.L8:
	.word	49604
	.word	.LC2
.LFE2:
	.size	nm_WALK_THRU_set_box_index_0, .-nm_WALK_THRU_set_box_index_0
	.section	.text.nm_walk_thru_updater,"ax",%progbits
	.align	2
	.type	nm_walk_thru_updater, %function
nm_walk_thru_updater:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	mov	r1, r0
	bne	.L11
	ldr	r0, .L12
	b	Alert_Message_va
.L11:
	ldr	r0, .L12+4
	b	Alert_Message
.L13:
	.align	2
.L12:
	.word	.LC3
	.word	.LC4
.LFE7:
	.size	nm_walk_thru_updater, .-nm_walk_thru_updater
	.section	.text.nm_WALK_THRU_set_stations,"ax",%progbits
	.align	2
	.global	nm_WALK_THRU_set_stations
	.type	nm_WALK_THRU_set_stations, %function
nm_WALK_THRU_set_stations:
.LFB3:
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI6:
	cmp	r1, #127
	sub	sp, sp, #76
.LCFI7:
	add	r8, r1, #21
	mov	r4, r0
	mov	r5, r2
	mov	fp, r3
	ldr	r9, [sp, #112]
	ldr	sl, [r0, r8, asl #2]
	bhi	.L15
	add	r7, sp, #44
	str	r1, [sp, #0]
	ldr	r2, .L18
	mov	r1, #32
	ldr	r3, .L18+4
	mov	r0, r7
	bl	snprintf
	ldr	r2, [sp, #116]
	mov	r3, #175
	str	r2, [sp, #20]
	ldr	r2, [sp, #120]
	str	r3, [sp, #0]
	str	r2, [sp, #24]
	ldr	r2, [sp, #124]
	mvn	r3, #0
	str	r2, [sp, #28]
	add	r2, r4, #604
	str	r2, [sp, #32]
	mov	r2, #4
	mov	r6, #0
	str	r2, [sp, #36]
	mov	r0, r4
	add	r1, r4, r8, asl #2
	mov	r2, r5
	stmib	sp, {r3, r6}
	str	r6, [sp, #12]
	str	r9, [sp, #16]
	str	r7, [sp, #40]
	bl	SHARED_set_int32_with_32_bit_change_bits_group
	cmp	r0, #1
	bne	.L14
	cmp	fp, r6
	beq	.L14
	cmn	sl, #1
	beq	.L17
	ldr	r0, [r4, #80]
	add	r1, r4, #20
	mov	r2, sl
	mov	r3, r6
	str	r9, [sp, #0]
	bl	Alert_walk_thru_station_added_or_removed
.L17:
	cmn	r5, #1
	beq	.L14
	ldr	r0, [r4, #80]
	add	r1, r4, #20
	mov	r2, r5
	mov	r3, #1
	str	r9, [sp, #0]
	bl	Alert_walk_thru_station_added_or_removed
	b	.L14
.L15:
	ldr	r0, .L18+8
	ldr	r1, .L18+12
	bl	Alert_index_out_of_range_with_filename
.L14:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L19:
	.align	2
.L18:
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	297
.LFE3:
	.size	nm_WALK_THRU_set_stations, .-nm_WALK_THRU_set_stations
	.section	.text.nm_WALK_THRU_set_default_values,"ax",%progbits
	.align	2
	.type	nm_WALK_THRU_set_default_values, %function
nm_WALK_THRU_set_default_values:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI8:
	sub	sp, sp, #36
.LCFI9:
	mov	r4, r0
	mov	r6, r1
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #0
	mov	r7, r0
	beq	.L21
	ldr	r8, .L27
	add	r5, r4, #596
	ldr	r1, [r8, #0]
	mov	r0, r5
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r8, #0]
	add	r0, r4, #600
	bl	SHARED_clear_all_32_bit_change_bits
	add	r9, r4, #604
	ldr	r1, [r8, #0]
	add	r0, r4, #608
	bl	SHARED_clear_all_32_bit_change_bits
	ldr	r1, [r8, #0]
	mov	r0, r9
	bl	SHARED_set_all_32_bit_change_bits
	mov	r0, r4
	mov	r1, #30
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_WALK_THRU_set_station_run_time
	mov	r0, r4
	mov	r1, #5
	mov	r2, #0
	mov	r3, #11
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_WALK_THRU_set_delay_before_start
.LBB8:
	ldr	r3, .L27+4
	mov	sl, #11
	mov	r2, #1
	stmib	sp, {r3, sl}
	mov	r3, #5
	mov	r8, #0
	str	r3, [sp, #28]
	add	r1, r4, #612
	mov	r3, r2
	mov	r0, r4
	str	r8, [sp, #0]
	str	r7, [sp, #12]
	str	r6, [sp, #16]
	str	r5, [sp, #20]
	str	r9, [sp, #24]
	str	r8, [sp, #32]
	bl	SHARED_set_bool_with_32_bit_change_bits_group
.LBE8:
	mov	r0, r4
	mov	r1, r8
	mov	r2, r8
	mov	r3, sl
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	bl	nm_WALK_THRU_set_box_index_0
.L22:
	mov	r1, r8
	mov	r0, r4
	mvn	r2, #0
	mov	r3, #0
	add	r8, r8, #1
	str	sl, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	r5, [sp, #12]
	bl	nm_WALK_THRU_set_stations
	cmp	r8, #128
	bne	.L22
	b	.L26
.L21:
	ldr	r0, .L27+8
	mov	r1, #204
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	Alert_func_call_with_null_ptr_with_filename
.L26:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L28:
	.align	2
.L27:
	.word	walk_thru_recursive_MUTEX
	.word	49605
	.word	.LC8
.LFE10:
	.size	nm_WALK_THRU_set_default_values, .-nm_WALK_THRU_set_default_values
	.section	.text.init_file_walk_thru,"ax",%progbits
	.align	2
	.global	init_file_walk_thru
	.type	init_file_walk_thru, %function
init_file_walk_thru:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI10:
	ldr	r3, .L30
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, .L30+4
	ldr	r1, .L30+8
	ldr	r3, [r3, #0]
	mov	r2, r0
	str	r3, [sp, #4]
	ldr	r3, .L30+12
	str	r3, [sp, #8]
	ldr	r3, .L30+16
	str	r3, [sp, #12]
	ldr	r3, .L30+20
	str	r3, [sp, #16]
	mov	r3, #20
	str	r3, [sp, #20]
	ldr	r3, .L30+24
	bl	FLASH_FILE_initialize_list_and_find_or_create_group_list_file
	add	sp, sp, #24
	ldmfd	sp!, {pc}
.L31:
	.align	2
.L30:
	.word	.LANCHOR2
	.word	walk_thru_recursive_MUTEX
	.word	.LANCHOR0
	.word	nm_walk_thru_updater
	.word	nm_WALK_THRU_set_default_values
	.word	.LANCHOR3
	.word	.LANCHOR1
.LFE8:
	.size	init_file_walk_thru, .-init_file_walk_thru
	.section	.text.save_file_walk_thru,"ax",%progbits
	.align	2
	.global	save_file_walk_thru
	.type	save_file_walk_thru, %function
save_file_walk_thru:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #616
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI11:
	ldr	r1, .L33
	str	r3, [sp, #0]
	ldr	r3, .L33+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, r0
	str	r3, [sp, #4]
	mov	r3, #20
	str	r3, [sp, #8]
	ldr	r3, .L33+8
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L34:
	.align	2
.L33:
	.word	.LANCHOR0
	.word	walk_thru_recursive_MUTEX
	.word	.LANCHOR1
.LFE9:
	.size	save_file_walk_thru, .-save_file_walk_thru
	.section	.text.nm_WAlK_THRU_create_new_group,"ax",%progbits
	.align	2
	.global	nm_WAlK_THRU_create_new_group
	.type	nm_WAlK_THRU_create_new_group, %function
nm_WAlK_THRU_create_new_group:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, lr}
.LCFI12:
	ldr	r0, .L40
	bl	nm_ListGetFirst
	b	.L39
.L38:
	ldr	r3, [r1, #612]
	cmp	r3, #0
	beq	.L37
	ldr	r0, .L40
	bl	nm_ListGetNext
.L39:
	cmp	r0, #0
	mov	r1, r0
	bne	.L38
.L37:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r1, [sp, #4]
	ldr	r2, .L40+4
	ldr	r1, .L40+8
	mov	r3, #616
	ldr	r0, .L40
	bl	nm_GROUP_create_new_group
	ldmfd	sp!, {r2, r3, pc}
.L41:
	.align	2
.L40:
	.word	.LANCHOR1
	.word	nm_WALK_THRU_set_default_values
	.word	.LANCHOR3
.LFE11:
	.size	nm_WAlK_THRU_create_new_group, .-nm_WAlK_THRU_create_new_group
	.section	.text.WALK_THRU_load_controller_name_into_scroll_box_guivar,"ax",%progbits
	.align	2
	.global	WALK_THRU_load_controller_name_into_scroll_box_guivar
	.type	WALK_THRU_load_controller_name_into_scroll_box_guivar, %function
WALK_THRU_load_controller_name_into_scroll_box_guivar:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L47
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, lr}
.LCFI13:
	ldr	r2, .L47+4
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #572
	bl	xQueueTakeMutexRecursive_debug
	bl	COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members
	cmp	r4, r0
	bcs	.L43
	mov	r0, r4
	ldr	r1, .L47+8
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	b	.L44
.L43:
	bl	COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members
	cmp	r4, r0
	bne	.L45
	mov	r1, #0
	ldr	r0, .L47+12
	bl	GuiLib_GetTextPtr
	mov	r1, #48
	ldr	r2, .L47+16
	mov	r3, r0
	ldr	r0, .L47+8
	b	.L46
.L45:
	mov	r1, #0
	mov	r0, #876
	bl	GuiLib_GetTextPtr
	ldr	r2, .L47+20
	mov	r1, #48
	mov	r3, r0
	ldr	r0, .L47+8
.L46:
	bl	snprintf
.L44:
	ldr	r3, .L47
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L48:
	.align	2
.L47:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	GuiVar_ComboBoxItemString
	.word	943
	.word	.LC9
	.word	.LC10
.LFE14:
	.size	WALK_THRU_load_controller_name_into_scroll_box_guivar, .-WALK_THRU_load_controller_name_into_scroll_box_guivar
	.section	.text.WALK_THRU_check_for_station_in_walk_thru,"ax",%progbits
	.align	2
	.global	WALK_THRU_check_for_station_in_walk_thru
	.type	WALK_THRU_check_for_station_in_walk_thru, %function
WALK_THRU_check_for_station_in_walk_thru:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L56
	stmfd	sp!, {r4, r5, lr}
.LCFI14:
	ldr	r0, [r3, #0]
	mov	r4, r1
	ldr	r2, .L56+4
	mov	r1, #400
	ldr	r3, .L56+8
	bl	xQueueTakeMutexRecursive_debug
	cmn	r4, #1
	moveq	r5, #0
	beq	.L50
	ldr	r2, .L56+12
	mov	r3, #0
	mov	r5, r3
.L52:
	ldr	r1, [r3, r2]
	add	r3, r3, #4
	cmp	r1, r4
	moveq	r5, #1
	cmp	r3, #512
	bne	.L52
.L50:
	ldr	r3, .L56
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L57:
	.align	2
.L56:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	599
	.word	.LANCHOR4
.LFE15:
	.size	WALK_THRU_check_for_station_in_walk_thru, .-WALK_THRU_check_for_station_in_walk_thru
	.section	.text.WALK_THRU_get_group_with_this_GID,"ax",%progbits
	.align	2
	.global	WALK_THRU_get_group_with_this_GID
	.type	WALK_THRU_get_group_with_this_GID, %function
WALK_THRU_get_group_with_this_GID:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI15:
	ldr	r4, .L59
	ldr	r3, .L59+4
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L59+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	mov	r2, #0
	ldr	r0, .L59+12
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L60:
	.align	2
.L59:
	.word	walk_thru_recursive_MUTEX
	.word	734
	.word	.LC8
	.word	.LANCHOR1
.LFE19:
	.size	WALK_THRU_get_group_with_this_GID, .-WALK_THRU_get_group_with_this_GID
	.section	.text.WALK_THRU_get_group_at_this_index,"ax",%progbits
	.align	2
	.global	WALK_THRU_get_group_at_this_index
	.type	WALK_THRU_get_group_at_this_index, %function
WALK_THRU_get_group_at_this_index:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI16:
	ldr	r4, .L62
	ldr	r2, .L62+4
	ldr	r3, .L62+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L62+12
	bl	nm_GROUP_get_ptr_to_group_at_this_location_in_list
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L63:
	.align	2
.L62:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	769
	.word	.LANCHOR1
.LFE20:
	.size	WALK_THRU_get_group_at_this_index, .-WALK_THRU_get_group_at_this_index
	.section	.text.WALK_THRU_copy_group_into_guivars,"ax",%progbits
	.align	2
	.global	WALK_THRU_copy_group_into_guivars
	.type	WALK_THRU_copy_group_into_guivars, %function
WALK_THRU_copy_group_into_guivars:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI17:
	ldr	r4, .L67
	mov	r1, #400
	ldr	r2, .L67+4
	ldr	r3, .L67+8
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	WALK_THRU_get_group_at_this_index
	mov	r5, r0
	bl	nm_GROUP_load_common_guivars
	ldr	r2, [r5, #72]
	ldr	r3, .L67+12
	ldr	r0, [r5, #80]
	str	r2, [r3, #0]
	ldr	r2, [r5, #76]
	ldr	r3, .L67+16
	ldr	r1, .L67+20
	str	r2, [r3, #0]
	ldr	r3, .L67+24
	str	r0, [r3, #0]
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	ldr	r2, .L67+28
	mov	r3, #0
.L65:
	ldr	r1, [r5, #84]
	add	r5, r5, #4
	str	r1, [r3, r2]
	add	r3, r3, #4
	cmp	r3, #512
	bne	.L65
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L68:
	.align	2
.L67:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	549
	.word	GuiVar_WalkThruRunTime
	.word	GuiVar_WalkThruDelay
	.word	GuiVar_WalkThruControllerName
	.word	GuiVar_WalkThruBoxIndex
	.word	.LANCHOR4
.LFE13:
	.size	WALK_THRU_copy_group_into_guivars, .-WALK_THRU_copy_group_into_guivars
	.section	.text.WALK_THRU_get_index_for_group_with_this_GID,"ax",%progbits
	.align	2
	.global	WALK_THRU_get_index_for_group_with_this_GID
	.type	WALK_THRU_get_index_for_group_with_this_GID, %function
WALK_THRU_get_index_for_group_with_this_GID:
.LFB21:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI18:
	ldr	r4, .L70
	ldr	r2, .L70+4
	ldr	r3, .L70+8
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r1, r5
	ldr	r0, .L70+12
	bl	nm_GROUP_get_index_for_group_with_this_GID
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r4, r5, pc}
.L71:
	.align	2
.L70:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	805
	.word	.LANCHOR1
.LFE21:
	.size	WALK_THRU_get_index_for_group_with_this_GID, .-WALK_THRU_get_index_for_group_with_this_GID
	.section	.text.WALK_THRU_get_num_groups_in_use,"ax",%progbits
	.align	2
	.global	WALK_THRU_get_num_groups_in_use
	.type	WALK_THRU_get_num_groups_in_use, %function
WALK_THRU_get_num_groups_in_use:
.LFB22:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L76
	stmfd	sp!, {r4, lr}
.LCFI19:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L76+4
	ldr	r3, .L76+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L76+12
	bl	nm_ListGetFirst
	mov	r4, #0
	mov	r1, r0
	b	.L73
.L75:
	ldr	r3, [r1, #612]
	ldr	r0, .L76+12
	cmp	r3, #0
	addne	r4, r4, #1
	bl	nm_ListGetNext
	mov	r1, r0
.L73:
	cmp	r1, #0
	bne	.L75
	ldr	r3, .L76
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L77:
	.align	2
.L76:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	863
	.word	.LANCHOR1
.LFE22:
	.size	WALK_THRU_get_num_groups_in_use, .-WALK_THRU_get_num_groups_in_use
	.section	.text.WALK_THRU_load_group_name_into_guivar,"ax",%progbits
	.align	2
	.global	WALK_THRU_load_group_name_into_guivar
	.type	WALK_THRU_load_group_name_into_guivar, %function
WALK_THRU_load_group_name_into_guivar:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L84
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, lr}
.LCFI20:
	ldr	r2, .L84+4
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L84+8
	bl	xQueueTakeMutexRecursive_debug
	bl	WALK_THRU_get_num_groups_in_use
	cmp	r4, r0
	bcs	.L79
	mov	r0, r4
	bl	WALK_THRU_get_group_at_this_index
	mov	r4, r0
	mov	r1, r4
	ldr	r0, .L84+12
	bl	nm_OnList
	cmp	r0, #1
	bne	.L80
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L84+16
	bl	strlcpy
	b	.L81
.L80:
	ldr	r0, .L84+4
	ldr	r1, .L84+20
	bl	Alert_group_not_found_with_filename
	b	.L81
.L79:
	bl	WALK_THRU_get_num_groups_in_use
	cmp	r4, r0
	bne	.L82
	mov	r1, #0
	ldr	r0, .L84+24
	bl	GuiLib_GetTextPtr
	mov	r1, #48
	ldr	r2, .L84+28
	mov	r3, r0
	ldr	r0, .L84+16
	b	.L83
.L82:
	mov	r1, #0
	mov	r0, #876
	bl	GuiLib_GetTextPtr
	ldr	r2, .L84+32
	mov	r1, #48
	mov	r3, r0
	ldr	r0, .L84+16
.L83:
	bl	snprintf
.L81:
	ldr	r3, .L84
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L85:
	.align	2
.L84:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	702
	.word	.LANCHOR1
	.word	GuiVar_itmGroupName
	.word	714
	.word	943
	.word	.LC9
	.word	.LC10
.LFE18:
	.size	WALK_THRU_load_group_name_into_guivar, .-WALK_THRU_load_group_name_into_guivar
	.section	.text.WALK_THRU_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	WALK_THRU_get_change_bits_ptr
	.type	WALK_THRU_get_change_bits_ptr, %function
WALK_THRU_get_change_bits_ptr:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r0
	add	r2, r3, #600
	mov	r0, r1
	add	r1, r3, #596
	add	r3, r3, #604
	b	SHARED_get_32_bit_change_bits_ptr
.LFE23:
	.size	WALK_THRU_get_change_bits_ptr, .-WALK_THRU_get_change_bits_ptr
	.section	.text.nm_WALK_THRU_store_changes,"ax",%progbits
	.align	2
	.type	nm_WALK_THRU_store_changes, %function
nm_WALK_THRU_store_changes:
.LFB5:
	@ args = 12, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI21:
	mov	r5, r0
	sub	sp, sp, #44
.LCFI22:
	mov	r7, r1
	mov	r4, r2
	ldr	r0, .L105
	mov	r1, r5
	add	r2, sp, #40
	mov	r6, r3
	ldr	fp, [sp, #80]
	ldr	r9, [sp, #88]
	bl	nm_GROUP_find_this_group_in_list
	ldr	r3, [sp, #40]
	cmp	r3, #0
	beq	.L88
	cmp	r7, #1
	bne	.L89
	mov	r0, r5
	bl	nm_GROUP_get_name
	mov	r3, #48
	mov	r2, #0
	stmia	sp, {r3, r4, r6}
	mov	r3, r2
	mov	r1, r0
	mov	r0, #49152
	bl	Alert_ChangeLine_Group
.L89:
	ldr	r0, [sp, #40]
	ldr	r1, [sp, #84]
	bl	WALK_THRU_get_change_bits_ptr
	cmp	r4, #2
	mov	r8, r0
	beq	.L90
	tst	r9, #1
	beq	.L91
.L90:
	mov	r0, r5
	bl	nm_GROUP_get_name
	stmia	sp, {r6, fp}
	str	r8, [sp, #8]
	mov	r1, r0
	ldr	r0, [sp, #40]
	add	r3, r0, #604
	str	r3, [sp, #12]
	mov	r3, #0
	rsb	ip, r3, r7
	rsbs	r2, ip, #0
	str	r3, [sp, #16]
	adc	r2, r2, ip
	mov	r3, r4
	bl	SHARED_set_name_32_bit_change_bits
	cmp	r4, #2
	beq	.L92
.L91:
	tst	r9, #2
	beq	.L93
.L92:
	rsbs	r2, r7, #1
	stmia	sp, {r6, fp}
	ldr	r0, [sp, #40]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r5, #72]
	mov	r3, r4
	bl	nm_WALK_THRU_set_station_run_time
	cmp	r4, #2
	beq	.L94
.L93:
	tst	r9, #4
	beq	.L95
.L94:
	rsbs	r2, r7, #1
	stmia	sp, {r6, fp}
	ldr	r0, [sp, #40]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r5, #76]
	mov	r3, r4
	bl	nm_WALK_THRU_set_delay_before_start
	cmp	r4, #2
	beq	.L96
.L95:
	tst	r9, #8
	beq	.L97
.L96:
	rsbs	r2, r7, #1
	stmia	sp, {r6, fp}
	ldr	r0, [sp, #40]
	str	r8, [sp, #8]
	movcc	r2, #0
	ldr	r1, [r5, #80]
	mov	r3, r4
	bl	nm_WALK_THRU_set_box_index_0
	cmp	r4, #2
	bne	.L97
.L99:
	mov	sl, #0
	str	r5, [sp, #36]
	b	.L98
.L97:
	tst	r9, #16
	bne	.L99
	b	.L100
.L98:
	mov	r3, #0
	stmib	sp, {r3, fp}
	ldr	r0, [sp, #40]
	str	r4, [sp, #0]
	str	r8, [sp, #12]
	mov	r1, sl
	ldr	r2, [r5, #84]
	mov	r3, #1
	add	sl, sl, #1
	bl	nm_WALK_THRU_set_stations
	cmp	sl, #128
	add	r5, r5, #4
	bne	.L98
	cmp	r4, #2
	ldr	r5, [sp, #36]
	beq	.L101
.L100:
	tst	r9, #32
	beq	.L87
.L101:
	ldr	r1, [sp, #40]
	ldr	r3, [r1, #612]
	cmp	r3, #0
	beq	.L103
	ldr	r3, [r5, #612]
	cmp	r3, #0
	bne	.L103
	ldr	r0, .L105
	ldr	r2, .L105+4
	ldr	r3, .L105+8
	bl	nm_ListRemove_debug
	ldr	r0, .L105
	ldr	r1, [sp, #40]
	bl	nm_ListInsertTail
.L103:
	ldr	r0, [sp, #40]
.LBB9:
	ldr	r3, .L105+12
.LBE9:
	rsbs	r7, r7, #1
.LBB10:
	stmib	sp, {r3, r4, r6, fp}
	add	r3, r0, #604
	str	r3, [sp, #24]
	mov	r3, #5
	str	r3, [sp, #28]
.LBE10:
	movcc	r7, #0
.LBB11:
	mov	r3, #0
	str	r3, [sp, #32]
	str	r7, [sp, #0]
	str	r8, [sp, #20]
	add	r1, r0, #612
	ldr	r2, [r5, #612]
	mov	r3, #1
	bl	SHARED_set_bool_with_32_bit_change_bits_group
	b	.L87
.L88:
.LBE11:
	ldr	r0, .L105+4
	mov	r1, #468
	bl	Alert_group_not_found_with_filename
.L87:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L106:
	.align	2
.L105:
	.word	.LANCHOR1
	.word	.LC7
	.word	451
	.word	49605
.LFE5:
	.size	nm_WALK_THRU_store_changes, .-nm_WALK_THRU_store_changes
	.section	.text.WALK_THRU_extract_and_store_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	WALK_THRU_extract_and_store_changes_from_GuiVars
	.type	WALK_THRU_extract_and_store_changes_from_GuiVars, %function
WALK_THRU_extract_and_store_changes_from_GuiVars:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L110
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, lr}
.LCFI23:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L110+4
	ldr	r2, .L110+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, .L110+8
	ldr	r2, .L110+12
	mov	r0, #616
	bl	mem_malloc_debug
	ldr	r3, .L110+16
	mov	r4, r0
	ldr	r0, [r3, #0]
	bl	WALK_THRU_get_group_with_this_GID
	mov	r2, #616
	mov	r1, r0
	mov	r0, r4
	bl	memcpy
	ldr	r1, .L110+20
	mov	r2, #48
	add	r0, r4, #20
	bl	strlcpy
	ldr	r3, .L110+24
	ldr	r1, .L110+28
	ldr	r3, [r3, #0]
	mov	r2, r4
	str	r3, [r4, #72]
	ldr	r3, .L110+32
	ldr	r3, [r3, #0]
	str	r3, [r4, #76]
	ldr	r3, .L110+36
	ldr	r3, [r3, #0]
	str	r3, [r4, #80]
	mov	r3, #0
.L108:
	ldr	r0, [r3, r1]
	add	r3, r3, #4
	cmp	r3, #512
	str	r0, [r2, #84]
	add	r2, r2, #4
	bne	.L108
	ldr	r5, .L110+40
	mov	r6, #0
	ldr	r7, [r5, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	str	r2, [sp, #0]
	mov	r1, r7
	mov	r2, #2
	stmib	sp, {r2, r6}
	mov	r3, r0
	mov	r0, r4
	bl	nm_WALK_THRU_store_changes
	mov	r0, r4
	ldr	r1, .L110+8
	ldr	r2, .L110+44
	bl	mem_free_debug
	ldr	r3, .L110
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	str	r6, [r5, #0]
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, pc}
.L111:
	.align	2
.L110:
	.word	walk_thru_recursive_MUTEX
	.word	657
	.word	.LC8
	.word	661
	.word	g_GROUP_ID
	.word	GuiVar_GroupName
	.word	GuiVar_WalkThruRunTime
	.word	.LANCHOR4
	.word	GuiVar_WalkThruDelay
	.word	GuiVar_WalkThruBoxIndex
	.word	g_GROUP_creating_new
	.word	686
.LFE17:
	.size	WALK_THRU_extract_and_store_changes_from_GuiVars, .-WALK_THRU_extract_and_store_changes_from_GuiVars
	.section	.text.nm_WALK_THRU_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	nm_WALK_THRU_extract_and_store_changes_from_comm
	.type	nm_WALK_THRU_extract_and_store_changes_from_comm, %function
nm_WALK_THRU_extract_and_store_changes_from_comm:
.LFB6:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI24:
	mov	r8, r0
	sub	sp, sp, #36
.LCFI25:
	str	r1, [sp, #12]
	str	r2, [sp, #16]
	mov	r1, r8
	add	r0, sp, #24
	mov	r2, #4
	mov	r9, #0
	mov	r7, r3
	add	r8, r8, #4
	bl	memcpy
	mov	r5, #4
	mov	fp, r9
	b	.L113
.L122:
	mov	r1, r8
	mov	r2, #4
	add	r0, sp, #32
	bl	memcpy
	add	r1, r8, #4
	mov	r2, #4
	add	r0, sp, #28
	bl	memcpy
	add	r1, r8, #8
	mov	r2, #4
	add	r0, sp, #20
	bl	memcpy
	ldr	r0, .L127
	ldr	r1, [sp, #32]
	mov	r2, #1
	bl	nm_GROUP_get_ptr_to_group_with_this_GID
	add	r4, r8, #12
	subs	sl, r0, #0
	bne	.L114
	mov	r0, r7
	bl	nm_WAlK_THRU_create_new_group
	ldr	r3, [sp, #32]
	cmp	r7, #16
	mov	sl, r0
	str	r3, [r0, #16]
	bne	.L126
	ldr	r3, .L127+4
	add	r0, r0, #596
	ldr	r1, [r3, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L126:
	mov	fp, #1
.L114:
	ldr	r1, .L127+8
	mov	r2, #608
	mov	r0, #616
	bl	mem_malloc_debug
	mov	r1, #0
	mov	r2, #616
	mov	r6, r0
	bl	memset
	mov	r0, r6
	mov	r1, sl
	mov	r2, #616
	bl	memcpy
	ldr	r3, [sp, #20]
	tst	r3, #1
	addeq	r5, r5, #12
	beq	.L116
	mov	r1, r4
	add	r0, r6, #20
	mov	r2, #48
	bl	memcpy
	add	r4, r8, #60
	add	r5, r5, #60
.L116:
	ldr	r3, [sp, #20]
	tst	r3, #2
	beq	.L117
	mov	r1, r4
	add	r0, r6, #72
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L117:
	ldr	r3, [sp, #20]
	tst	r3, #4
	beq	.L118
	mov	r1, r4
	add	r0, r6, #76
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L118:
	ldr	r3, [sp, #20]
	tst	r3, #8
	beq	.L119
	mov	r1, r4
	add	r0, r6, #80
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L119:
	ldr	r3, [sp, #20]
	tst	r3, #16
	beq	.L120
	mov	r1, r4
	add	r0, r6, #84
	mov	r2, #512
	bl	memcpy
	add	r4, r4, #512
	add	r5, r5, #512
.L120:
	ldr	r3, [sp, #20]
	tst	r3, #32
	beq	.L121
	mov	r1, r4
	add	r0, r6, #612
	mov	r2, #4
	bl	memcpy
	add	r4, r4, #4
	add	r5, r5, #4
.L121:
	ldr	r3, [sp, #16]
	mov	r0, r6
	stmia	sp, {r3, r7}
	ldr	r3, [sp, #20]
	mov	r1, fp
	str	r3, [sp, #8]
	ldr	r2, [sp, #12]
	mov	r3, #0
	bl	nm_WALK_THRU_store_changes
	mov	r0, r6
	ldr	r1, .L127+8
	ldr	r2, .L127+12
	bl	mem_free_debug
	add	r9, r9, #1
	mov	r8, r4
.L113:
	ldr	r3, [sp, #24]
	cmp	r9, r3
	bcc	.L122
	cmp	r5, #0
	beq	.L123
	cmp	r7, #1
	cmpne	r7, #15
	beq	.L124
	cmp	r7, #16
	bne	.L125
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L125
.L124:
	mov	r0, #20
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L125
.L123:
	ldr	r0, .L127+8
	mov	r1, #752
	bl	Alert_bit_set_with_no_data_with_filename
.L125:
	mov	r0, r5
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L128:
	.align	2
.L127:
	.word	.LANCHOR1
	.word	list_program_data_recursive_MUTEX
	.word	.LC7
	.word	698
.LFE6:
	.size	nm_WALK_THRU_extract_and_store_changes_from_comm, .-nm_WALK_THRU_extract_and_store_changes_from_comm
	.section	.text.WALK_THRU_extract_and_store_group_name_from_GuiVars,"ax",%progbits
	.align	2
	.global	WALK_THRU_extract_and_store_group_name_from_GuiVars
	.type	WALK_THRU_extract_and_store_group_name_from_GuiVars, %function
WALK_THRU_extract_and_store_group_name_from_GuiVars:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L131
	stmfd	sp!, {r4, r5, lr}
.LCFI26:
	ldr	r0, [r3, #0]
	sub	sp, sp, #20
.LCFI27:
	mov	r1, #400
	ldr	r2, .L131+4
	ldr	r3, .L131+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L131+12
	ldr	r0, [r3, #0]
	bl	WALK_THRU_get_group_at_this_index
	subs	r4, r0, #0
	beq	.L130
	bl	FLOWSENSE_get_controller_index
	mov	r1, #2
	mov	r5, r0
	mov	r0, r4
	bl	WALK_THRU_get_change_bits_ptr
	add	r3, r4, #604
	str	r3, [sp, #12]
	mov	r3, #0
	mov	r2, #1
	str	r3, [sp, #16]
	ldr	r1, .L131+16
	mov	r3, #2
	str	r5, [sp, #0]
	str	r2, [sp, #4]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	SHARED_set_name_32_bit_change_bits
.L130:
	ldr	r3, .L131
	ldr	r0, [r3, #0]
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L132:
	.align	2
.L131:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	638
	.word	g_GROUP_list_item_index
	.word	GuiVar_GroupName
.LFE16:
	.size	WALK_THRU_extract_and_store_group_name_from_GuiVars, .-WALK_THRU_extract_and_store_group_name_from_GuiVars
	.section	.text.WALK_THRU_build_data_to_send,"ax",%progbits
	.align	2
	.global	WALK_THRU_build_data_to_send
	.type	WALK_THRU_build_data_to_send, %function
WALK_THRU_build_data_to_send:
.LFB12:
	@ args = 4, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI28:
	mov	r8, r3
	sub	sp, sp, #60
.LCFI29:
	mov	r3, #0
	str	r3, [sp, #56]
	ldr	r3, .L151
	str	r1, [sp, #36]
	mov	r4, r0
	mov	r6, r2
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L151+4
	ldr	r3, .L151+8
	ldr	sl, [sp, #96]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L151+12
	bl	nm_ListGetFirst
	b	.L149
.L137:
	mov	r0, r5
	mov	r1, r8
	bl	WALK_THRU_get_change_bits_ptr
	ldr	r3, [r0, #0]
	cmp	r3, #0
	ldrne	r3, [sp, #56]
	addne	r3, r3, #1
	strne	r3, [sp, #56]
	bne	.L136
.L135:
	ldr	r0, .L151+12
	mov	r1, r5
	bl	nm_ListGetNext
.L149:
	cmp	r0, #0
	mov	r5, r0
	bne	.L137
.L136:
	ldr	r9, [sp, #56]
	cmp	r9, #0
	beq	.L138
	mov	r3, #0
	str	r3, [sp, #56]
	mov	r0, r4
	mov	r3, r6
	add	r1, sp, #48
	ldr	r2, [sp, #36]
	str	sl, [sp, #0]
	bl	PDATA_allocate_space_for_num_changed_groups_in_pucp
	ldr	r3, [r6, #0]
	cmp	r3, #0
	mov	r9, r0
	beq	.L138
	ldr	r0, .L151+12
	bl	nm_ListGetFirst
	ldr	r2, [sp, #36]
	add	r2, r2, #12
	str	r2, [sp, #40]
	mov	r5, r0
	b	.L139
.L145:
	mov	r0, r5
	mov	r1, r8
	bl	WALK_THRU_get_change_bits_ptr
	ldr	r3, [r0, #0]
	mov	r7, r0
	cmp	r3, #0
	beq	.L140
	ldr	r3, [r6, #0]
	cmp	r3, #0
	beq	.L141
	ldr	r3, [sp, #40]
	add	ip, r3, r9
	cmp	ip, sl
	bcs	.L141
	add	r1, r5, #16
	mov	r2, #4
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp
	mov	r1, #4
	mov	r2, r1
	add	r3, sp, #52
	mov	r0, r4
	bl	PDATA_copy_bitfield_info_into_pucp
	ldr	ip, [sp, #28]
	add	r3, r5, #20
	str	r3, [sp, #4]
	mov	r3, #48
	add	fp, r5, #608
	mov	r2, #0
	str	r3, [sp, #8]
	add	r1, sp, #44
	mov	r3, r7
	mov	r0, r4
	str	r2, [sp, #44]
	str	ip, [sp, #12]
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r2, [sp, #36]
	add	r3, r5, #72
	add	r2, r9, r2
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	str	r2, [sp, #32]
	add	r1, sp, #44
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, r0, #12
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #1
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	mov	r2, #4
	str	r2, [sp, #8]
	ldr	r2, [sp, #32]
	add	r3, r5, #76
	str	r3, [sp, #4]
	add	r1, sp, #44
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #2
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #80
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #44
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #3
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #84
	str	r3, [sp, #4]
	mov	r3, #512
	str	r3, [sp, #8]
	add	r1, sp, #44
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r2, #4
	mov	r3, r7
	mov	r0, r4
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	ldr	r2, [sp, #32]
	add	r3, r5, #612
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #44
	str	fp, [sp, #0]
	str	r6, [sp, #16]
	str	sl, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r2
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, r7
	mov	r2, #5
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r7, ip, r0
	cmp	r7, #12
	bls	.L148
	b	.L150
.L141:
	mov	r3, #0
	str	r3, [r6, #0]
	b	.L144
.L150:
	ldr	r3, [sp, #56]
	ldr	r0, [sp, #52]
	add	r3, r3, #1
	add	r1, sp, #44
	mov	r2, #4
	str	r3, [sp, #56]
	add	r9, r9, r7
	bl	memcpy
	b	.L140
.L148:
	ldr	r3, [r4, #0]
	sub	r3, r3, #12
	str	r3, [r4, #0]
.L140:
	mov	r1, r5
	ldr	r0, .L151+12
	bl	nm_ListGetNext
	mov	r5, r0
.L139:
	cmp	r5, #0
	bne	.L145
.L144:
	ldr	r3, [sp, #56]
	cmp	r3, #0
	beq	.L146
	ldr	r0, [sp, #48]
	add	r1, sp, #56
	mov	r2, #4
	bl	memcpy
	b	.L138
.L146:
	ldr	r2, [r4, #0]
	mov	r9, r3
	sub	r2, r2, #4
	str	r2, [r4, #0]
.L138:
	ldr	r3, .L151
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r9
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L152:
	.align	2
.L151:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	318
	.word	.LANCHOR1
.LFE12:
	.size	WALK_THRU_build_data_to_send, .-WALK_THRU_build_data_to_send
	.section	.text.WALK_THRU_get_station_run_time,"ax",%progbits
	.align	2
	.global	WALK_THRU_get_station_run_time
	.type	WALK_THRU_get_station_run_time, %function
WALK_THRU_get_station_run_time:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI30:
	ldr	r4, .L154
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L154+4
	ldr	r3, .L154+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r3, #30
	str	r3, [sp, #0]
	ldr	r3, .L154+12
	add	r1, r5, #72
	str	r3, [sp, #4]
	add	r3, r5, #596
	str	r3, [sp, #8]
	ldr	r3, .L154+16
	mov	r2, #1
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #150
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L155:
	.align	2
.L154:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	897
	.word	nm_WALK_THRU_set_station_run_time
	.word	.LC0
.LFE24:
	.size	WALK_THRU_get_station_run_time, .-WALK_THRU_get_station_run_time
	.section	.text.WALK_THRU_get_delay_before_start,"ax",%progbits
	.align	2
	.global	WALK_THRU_get_delay_before_start
	.type	WALK_THRU_get_delay_before_start, %function
WALK_THRU_get_delay_before_start:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI31:
	ldr	r4, .L157
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L157+4
	ldr	r3, .L157+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L157+12
	mov	r2, #5
	str	r3, [sp, #4]
	add	r3, r5, #596
	str	r3, [sp, #8]
	ldr	r3, .L157+16
	add	r1, r5, #76
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #300
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L158:
	.align	2
.L157:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	918
	.word	nm_WALK_THRU_set_delay_before_start
	.word	.LC1
.LFE25:
	.size	WALK_THRU_get_delay_before_start, .-WALK_THRU_get_delay_before_start
	.section	.text.WALK_THRU_get_box_index,"ax",%progbits
	.align	2
	.global	WALK_THRU_get_box_index
	.type	WALK_THRU_get_box_index, %function
WALK_THRU_get_box_index:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, lr}
.LCFI32:
	ldr	r4, .L160
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L160+4
	ldr	r3, .L160+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L160+12
	mov	r2, #0
	str	r3, [sp, #4]
	add	r3, r5, #596
	str	r3, [sp, #8]
	ldr	r3, .L160+16
	add	r1, r5, #80
	str	r3, [sp, #12]
	mov	r0, r5
	mov	r3, #11
	str	r2, [sp, #0]
	bl	SHARED_get_uint32_32_bit_change_bits_group
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, pc}
.L161:
	.align	2
.L160:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	939
	.word	nm_WALK_THRU_set_box_index_0
	.word	.LC2
.LFE26:
	.size	WALK_THRU_get_box_index, .-WALK_THRU_get_box_index
	.section	.text.WALK_THRU_get_stations,"ax",%progbits
	.align	2
	.global	WALK_THRU_get_stations
	.type	WALK_THRU_get_stations, %function
WALK_THRU_get_stations:
.LFB27:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #127
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI33:
	mov	r5, r0
	sub	sp, sp, #72
.LCFI34:
	mov	r4, r1
	bhi	.L163
	add	r6, sp, #24
	add	r3, r1, #1
	str	r3, [sp, #0]
	mov	r0, r6
	mov	r1, #48
	ldr	r2, .L165
	ldr	r3, .L165+4
	bl	snprintf
	mvn	r3, #0
	mov	r1, #175
	stmib	sp, {r1, r3}
	str	r3, [sp, #0]
	ldr	r3, .L165+8
	add	r2, r4, #21
	str	r3, [sp, #12]
	add	r3, r5, #596
	str	r3, [sp, #16]
	mov	r0, r5
	mov	r1, r4
	add	r2, r5, r2, asl #2
	mov	r3, #128
	str	r6, [sp, #20]
	bl	SHARED_get_int32_from_array_32_bit_change_bits_group
	b	.L164
.L163:
	ldr	r0, .L165+12
	mov	r1, #980
	bl	Alert_index_out_of_range_with_filename
	mvn	r0, #0
.L164:
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, pc}
.L166:
	.align	2
.L165:
	.word	.LC5
	.word	.LC6
	.word	nm_WALK_THRU_set_stations
	.word	.LC8
.LFE27:
	.size	WALK_THRU_get_stations, .-WALK_THRU_get_stations
	.section	.text.WALK_THRU_clean_house_processing,"ax",%progbits
	.align	2
	.global	WALK_THRU_clean_house_processing
	.type	WALK_THRU_clean_house_processing, %function
WALK_THRU_clean_house_processing:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L171
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI35:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L171+4
	ldr	r3, .L171+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L171+12
	ldr	r1, .L171+4
	ldr	r2, .L171+16
	b	.L170
.L169:
	ldr	r1, .L171+4
	ldr	r2, .L171+20
	bl	mem_free_debug
	ldr	r0, .L171+12
	ldr	r1, .L171+4
	ldr	r2, .L171+24
.L170:
	bl	nm_ListRemoveHead_debug
	cmp	r0, #0
	bne	.L169
	ldr	r4, .L171
	str	r0, [sp, #0]
	str	r0, [sp, #4]
	ldr	r1, .L171+28
	ldr	r2, .L171+32
	mov	r3, #616
	ldr	r0, .L171+12
	bl	nm_GROUP_create_new_group
	mov	r3, #616
	str	r3, [sp, #0]
	ldr	r3, [r4, #0]
	mov	r0, #1
	str	r3, [sp, #4]
	mov	r3, #20
	str	r3, [sp, #8]
	mov	r2, r0
	ldr	r1, .L171+36
	ldr	r3, .L171+12
	bl	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	ldr	r0, [r4, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L172:
	.align	2
.L171:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	1003
	.word	.LANCHOR1
	.word	1007
	.word	1011
	.word	1013
	.word	.LANCHOR3
	.word	nm_WALK_THRU_set_default_values
	.word	.LANCHOR0
.LFE28:
	.size	WALK_THRU_clean_house_processing, .-WALK_THRU_clean_house_processing
	.section	.text.WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.type	WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token, %function
WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI36:
	ldr	r4, .L177
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L177+4
	ldr	r3, .L177+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L177+12
	bl	nm_ListGetFirst
	b	.L176
.L175:
	add	r0, r5, #600
	ldr	r1, [r4, #0]
	bl	SHARED_set_all_32_bit_change_bits
	ldr	r0, .L177+12
	mov	r1, r5
	bl	nm_ListGetNext
.L176:
	cmp	r0, #0
	mov	r5, r0
	bne	.L175
	ldr	r4, .L177+16
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L177+4
	ldr	r3, .L177+20
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L177+24
	mov	r2, #1
	str	r2, [r3, #444]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L177
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L178:
	.align	2
.L177:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	1037
	.word	.LANCHOR1
	.word	comm_mngr_recursive_MUTEX
	.word	1059
	.word	comm_mngr
.LFE29:
	.size	WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token, .-WALK_THRU_set_bits_on_all_groups_to_cause_distribution_in_the_next_token
	.section	.text.WALK_THRU_on_all_groups_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	WALK_THRU_on_all_groups_set_or_clear_commserver_change_bits
	.type	WALK_THRU_on_all_groups_set_or_clear_commserver_change_bits, %function
WALK_THRU_on_all_groups_set_or_clear_commserver_change_bits:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI37:
	ldr	r4, .L185
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L185+4
	ldr	r3, .L185+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L185+12
	bl	nm_ListGetFirst
	b	.L184
.L183:
	cmp	r5, #51
	ldr	r1, [r4, #0]
	add	r0, r6, #604
	bne	.L181
	bl	SHARED_clear_all_32_bit_change_bits
	b	.L182
.L181:
	bl	SHARED_set_all_32_bit_change_bits
.L182:
	ldr	r0, .L185+12
	mov	r1, r6
	bl	nm_ListGetNext
.L184:
	cmp	r0, #0
	mov	r6, r0
	bne	.L183
	ldr	r3, .L185
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L186:
	.align	2
.L185:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	1075
	.word	.LANCHOR1
.LFE30:
	.size	WALK_THRU_on_all_groups_set_or_clear_commserver_change_bits, .-WALK_THRU_on_all_groups_set_or_clear_commserver_change_bits
	.section	.text.nm_WALK_THRU_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_WALK_THRU_update_pending_change_bits
	.type	nm_WALK_THRU_update_pending_change_bits, %function
nm_WALK_THRU_update_pending_change_bits:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI38:
	ldr	r5, .L194
	mov	r1, #400
	ldr	r2, .L194+4
	ldr	r3, .L194+8
	mov	r7, r0
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L194+12
	bl	nm_ListGetFirst
	mov	r8, #0
	mov	r6, #1
	mov	r4, r0
	b	.L188
.L191:
	ldr	r3, [r4, #608]
	cmp	r3, #0
	beq	.L189
	cmp	r7, #0
	beq	.L190
	ldr	r2, [r4, #604]
	mov	r1, #2
	orr	r3, r2, r3
	str	r3, [r4, #604]
	ldr	r3, .L194+16
	ldr	r2, .L194+20
	str	r6, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L194+24
	ldr	r0, [r3, #152]
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L193
.L190:
	add	r0, r4, #608
	ldr	r1, [r5, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L193:
	mov	r8, #1
.L189:
	mov	r1, r4
	ldr	r0, .L194+12
	bl	nm_ListGetNext
	mov	r4, r0
.L188:
	cmp	r4, #0
	bne	.L191
	ldr	r3, .L194
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r8, #0
	beq	.L187
	mov	r0, #20
	mov	r1, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L187:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L195:
	.align	2
.L194:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	1112
	.word	.LANCHOR1
	.word	weather_preserves
	.word	60000
	.word	cics
.LFE31:
	.size	nm_WALK_THRU_update_pending_change_bits, .-nm_WALK_THRU_update_pending_change_bits
	.section	.text.WALK_THRU_load_walk_thru_gid_for_token_response,"ax",%progbits
	.align	2
	.global	WALK_THRU_load_walk_thru_gid_for_token_response
	.type	WALK_THRU_load_walk_thru_gid_for_token_response, %function
WALK_THRU_load_walk_thru_gid_for_token_response:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI39:
	str	r3, [r0, #0]
	ldr	r3, .L202
	mov	r4, r0
	ldr	r0, [r3, #232]
	cmp	r0, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
.LBB12:
	ldr	r5, .L202+4
.LBE12:
	ldr	r6, [r3, #236]
.LBB13:
	mov	r1, #400
	ldr	r2, .L202+8
	ldr	r3, .L202+12
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r6
	bl	WALK_THRU_get_group_with_this_GID
	cmp	r0, #0
	moveq	r6, #1
	beq	.L198
	ldr	r6, [r0, #596]
	rsbs	r6, r6, #1
	movcc	r6, #0
.L198:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
.LBE13:
	cmp	r6, #0
	beq	.L201
	ldr	r5, .L202
	mov	r3, #0
	str	r3, [r5, #232]
	ldr	r1, .L202+8
	ldr	r2, .L202+16
	mov	r0, #4
	bl	mem_malloc_debug
	add	r1, r5, #236
	mov	r2, #4
	str	r0, [r4, #0]
	bl	memcpy
	mov	r0, #4
	ldmfd	sp!, {r4, r5, r6, pc}
.L201:
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, pc}
.L203:
	.align	2
.L202:
	.word	irri_comm
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	1180
	.word	1225
.LFE33:
	.size	WALK_THRU_load_walk_thru_gid_for_token_response, .-WALK_THRU_load_walk_thru_gid_for_token_response
	.section	.text.WALK_THRU_load_kick_off_struct,"ax",%progbits
	.align	2
	.global	WALK_THRU_load_kick_off_struct
	.type	WALK_THRU_load_kick_off_struct, %function
WALK_THRU_load_kick_off_struct:
.LFB34:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L207
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI40:
	ldr	r2, .L207+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L207+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r5
	bl	WALK_THRU_get_group_with_this_GID
	subs	r6, r0, #0
	beq	.L205
	ldr	r3, [r6, #80]
	mov	r2, #6
	str	r3, [r4, #0]
	ldr	r3, [r6, #72]
	add	r1, r6, #84
	mul	r3, r2, r3
	add	r0, r4, #4
	str	r3, [r4, #516]
	mov	r2, #512
	bl	memcpy
	mov	r0, r6
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r6, #1
	mov	r1, r0
	add	r0, r4, #520
	bl	strlcpy
	b	.L206
.L205:
	ldr	r0, .L207+12
	mov	r1, r5
	bl	Alert_Message_va
.L206:
	ldr	r3, .L207
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, pc}
.L208:
	.align	2
.L207:
	.word	walk_thru_recursive_MUTEX
	.word	.LC8
	.word	1252
	.word	.LC11
.LFE34:
	.size	WALK_THRU_load_kick_off_struct, .-WALK_THRU_load_kick_off_struct
	.section	.text.WALK_THRU_calculate_chain_sync_crc,"ax",%progbits
	.align	2
	.global	WALK_THRU_calculate_chain_sync_crc
	.type	WALK_THRU_calculate_chain_sync_crc, %function
WALK_THRU_calculate_chain_sync_crc:
.LFB35:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L214
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI41:
	ldr	r5, .L214+4
	mov	r1, #400
	ldr	r2, .L214+8
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L214+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #8]
	mov	r0, #616
	mul	r0, r3, r0
	mov	r1, sp
	ldr	r2, .L214+8
	ldr	r3, .L214+16
	bl	mem_oabia
	subs	r6, r0, #0
	beq	.L210
	ldr	r3, [sp, #0]
	add	r6, sp, #8
	mov	r0, r5
	str	r3, [r6, #-4]!
	bl	nm_ListGetFirst
	mov	r7, #0
	mov	r5, r0
	b	.L211
.L212:
	add	r8, r5, #20
	mov	r0, r8
	bl	strlen
	mov	r1, r8
	mov	r2, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #72
	mov	r2, #4
	mov	r8, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #76
	mov	r2, #4
	add	r0, r8, r0
	add	r7, r0, r7
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #80
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #84
	mov	r2, #512
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	add	r1, r5, #612
	mov	r2, #4
	add	r7, r7, r0
	mov	r0, r6
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r1, r5
	add	r7, r7, r0
	ldr	r0, .L214+4
	bl	nm_ListGetNext
	mov	r5, r0
.L211:
	cmp	r5, #0
	bne	.L212
	mov	r1, r7
	ldr	r0, [sp, #0]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L214+20
	add	r4, r4, #43
	ldr	r1, .L214+8
	ldr	r2, .L214+24
	mov	r6, #1
	str	r0, [r3, r4, asl #2]
	ldr	r0, [sp, #0]
	bl	mem_free_debug
	b	.L213
.L210:
	ldr	r3, .L214+28
	ldr	r0, .L214+32
	ldr	r1, [r3, r4, asl #4]
	bl	Alert_Message_va
.L213:
	ldr	r3, .L214
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.L215:
	.align	2
.L214:
	.word	walk_thru_recursive_MUTEX
	.word	.LANCHOR1
	.word	.LC8
	.word	1310
	.word	1322
	.word	cscs
	.word	1381
	.word	chain_sync_file_pertinants
	.word	.LC12
.LFE35:
	.size	WALK_THRU_calculate_chain_sync_crc, .-WALK_THRU_calculate_chain_sync_crc
	.global	walk_thru_list_item_sizes
	.global	WALK_THRU_DEFAULT_NAME
	.global	WALK_THRU_station_array_for_gui
	.section	.rodata.WALK_THRU_DEFAULT_NAME,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	WALK_THRU_DEFAULT_NAME, %object
	.size	WALK_THRU_DEFAULT_NAME, 10
WALK_THRU_DEFAULT_NAME:
	.ascii	"Walk Thru\000"
	.section	.bss.walk_thru_group_list_hdr,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	walk_thru_group_list_hdr, %object
	.size	walk_thru_group_list_hdr, 20
walk_thru_group_list_hdr:
	.space	20
	.section	.bss.WALK_THRU_station_array_for_gui,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	WALK_THRU_station_array_for_gui, %object
	.size	WALK_THRU_station_array_for_gui, 512
WALK_THRU_station_array_for_gui:
	.space	512
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"StationRunTime\000"
.LC1:
	.ascii	"DelayBeforeStart\000"
.LC2:
	.ascii	"BoxIndex\000"
.LC3:
	.ascii	"WALK THRU file unexpd update %u\000"
.LC4:
	.ascii	"WALK THRU updater error\000"
.LC5:
	.ascii	"%s%d\000"
.LC6:
	.ascii	"StationOrderInUse\000"
.LC7:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/shared_walk_thru.c\000"
.LC8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/walk_thru.c\000"
.LC9:
	.ascii	"%s\000"
.LC10:
	.ascii	" <%s>\000"
.LC11:
	.ascii	"WALK THRU : could not find %u group\000"
.LC12:
	.ascii	"SYNC: no mem to calc checksum %s\000"
	.section	.rodata.walk_thru_list_item_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	walk_thru_list_item_sizes, %object
	.size	walk_thru_list_item_sizes, 8
walk_thru_list_item_sizes:
	.word	616
	.word	616
	.section	.rodata.WALK_THRU_FILENAME,"a",%progbits
	.set	.LANCHOR0,. + 0
	.type	WALK_THRU_FILENAME, %object
	.size	WALK_THRU_FILENAME, 10
WALK_THRU_FILENAME:
	.ascii	"WALK_THRU\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI6-.LFB3
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x70
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI8-.LFB10
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI10-.LFB8
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI11-.LFB9
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI12-.LFB11
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI13-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI14-.LFB15
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI15-.LFB19
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI16-.LFB20
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI17-.LFB13
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI18-.LFB21
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI19-.LFB22
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI20-.LFB18
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI21-.LFB5
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI23-.LFB17
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI24-.LFB6
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI25-.LCFI24
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI26-.LFB16
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI27-.LCFI26
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI28-.LFB12
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI29-.LCFI28
	.byte	0xe
	.uleb128 0x60
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI30-.LFB24
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI31-.LFB25
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI32-.LFB26
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x83
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI33-.LFB27
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI34-.LCFI33
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI35-.LFB28
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI36-.LFB29
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI37-.LFB30
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI38-.LFB31
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI39-.LFB33
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI40-.LFB34
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI41-.LFB35
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE66:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/shared_walk_thru.c"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/walk_thru.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x2e9
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF36
	.byte	0x1
	.4byte	.LASF37
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x130
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x2
	.2byte	0x48e
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0x6c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0x92
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.byte	0xb8
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF5
	.byte	0x2
	.byte	0x53
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.byte	0xde
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x9a
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST4
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x2
	.byte	0x7c
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF9
	.byte	0x2
	.byte	0x8b
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST6
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x2
	.byte	0xd1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x2
	.2byte	0x23a
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x2
	.2byte	0x24f
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST9
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x2
	.2byte	0x2da
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST10
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF14
	.byte	0x2
	.2byte	0x2fd
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST11
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF15
	.byte	0x2
	.2byte	0x21f
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST12
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF16
	.byte	0x2
	.2byte	0x321
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST13
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF17
	.byte	0x2
	.2byte	0x353
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST14
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF18
	.byte	0x2
	.2byte	0x2ba
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST15
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF19
	.byte	0x2
	.2byte	0x377
	.4byte	.LFB23
	.4byte	.LFE23
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x8
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x154
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST16
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF21
	.byte	0x2
	.2byte	0x28b
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST17
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x1da
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST18
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF23
	.byte	0x2
	.2byte	0x27a
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST19
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF24
	.byte	0x2
	.2byte	0x116
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST20
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF25
	.byte	0x2
	.2byte	0x37d
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST21
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF26
	.byte	0x2
	.2byte	0x392
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST22
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF27
	.byte	0x2
	.2byte	0x3a7
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST23
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF28
	.byte	0x2
	.2byte	0x3bd
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST24
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF29
	.byte	0x2
	.2byte	0x3dd
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST25
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF30
	.byte	0x2
	.2byte	0x407
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST26
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF31
	.byte	0x2
	.2byte	0x42f
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST27
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF32
	.byte	0x2
	.2byte	0x450
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST28
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF33
	.byte	0x2
	.2byte	0x4ad
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST29
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF34
	.byte	0x2
	.2byte	0x4d7
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST30
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF35
	.byte	0x2
	.2byte	0x507
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST31
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI5
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI7
	.4byte	.LFE3
	.2byte	0x3
	.byte	0x7d
	.sleb128 112
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB10
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI9
	.4byte	.LFE10
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB8
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB9
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB14
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB15
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB19
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB20
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB13
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB21
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB22
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB18
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB5
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI22
	.4byte	.LFE5
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB17
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB6
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI25
	.4byte	.LFE6
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB16
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI27
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB12
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI29
	.4byte	.LFE12
	.2byte	0x3
	.byte	0x7d
	.sleb128 96
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB24
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LFE24
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB25
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI31
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB26
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LFE26
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB27
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI34
	.4byte	.LFE27
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB28
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB29
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB30
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB31
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI38
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB33
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB34
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI40
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB35
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x124
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF26:
	.ascii	"WALK_THRU_get_delay_before_start\000"
.LASF17:
	.ascii	"WALK_THRU_get_num_groups_in_use\000"
.LASF20:
	.ascii	"nm_WALK_THRU_store_changes\000"
.LASF19:
	.ascii	"WALK_THRU_get_change_bits_ptr\000"
.LASF36:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"nm_WALK_THRU_set_in_use\000"
.LASF35:
	.ascii	"WALK_THRU_calculate_chain_sync_crc\000"
.LASF10:
	.ascii	"nm_WAlK_THRU_create_new_group\000"
.LASF12:
	.ascii	"WALK_THRU_check_for_station_in_walk_thru\000"
.LASF15:
	.ascii	"WALK_THRU_copy_group_into_guivars\000"
.LASF13:
	.ascii	"WALK_THRU_get_group_with_this_GID\000"
.LASF31:
	.ascii	"WALK_THRU_on_all_groups_set_or_clear_commserver_cha"
	.ascii	"nge_bits\000"
.LASF37:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/walk_thru.c\000"
.LASF22:
	.ascii	"nm_WALK_THRU_extract_and_store_changes_from_comm\000"
.LASF7:
	.ascii	"nm_WALK_THRU_set_stations\000"
.LASF27:
	.ascii	"WALK_THRU_get_box_index\000"
.LASF6:
	.ascii	"nm_WALK_THRU_set_default_values\000"
.LASF34:
	.ascii	"WALK_THRU_load_kick_off_struct\000"
.LASF28:
	.ascii	"WALK_THRU_get_stations\000"
.LASF30:
	.ascii	"WALK_THRU_set_bits_on_all_groups_to_cause_distribut"
	.ascii	"ion_in_the_next_token\000"
.LASF11:
	.ascii	"WALK_THRU_load_controller_name_into_scroll_box_guiv"
	.ascii	"ar\000"
.LASF24:
	.ascii	"WALK_THRU_build_data_to_send\000"
.LASF29:
	.ascii	"WALK_THRU_clean_house_processing\000"
.LASF21:
	.ascii	"WALK_THRU_extract_and_store_changes_from_GuiVars\000"
.LASF4:
	.ascii	"nm_WALK_THRU_set_box_index_0\000"
.LASF5:
	.ascii	"nm_walk_thru_updater\000"
.LASF8:
	.ascii	"init_file_walk_thru\000"
.LASF25:
	.ascii	"WALK_THRU_get_station_run_time\000"
.LASF9:
	.ascii	"save_file_walk_thru\000"
.LASF1:
	.ascii	"there_are_no_pending_walk_thru_changes_to_send_to_t"
	.ascii	"he_master\000"
.LASF32:
	.ascii	"nm_WALK_THRU_update_pending_change_bits\000"
.LASF16:
	.ascii	"WALK_THRU_get_index_for_group_with_this_GID\000"
.LASF2:
	.ascii	"nm_WALK_THRU_set_station_run_time\000"
.LASF33:
	.ascii	"WALK_THRU_load_walk_thru_gid_for_token_response\000"
.LASF3:
	.ascii	"nm_WALK_THRU_set_delay_before_start\000"
.LASF18:
	.ascii	"WALK_THRU_load_group_name_into_guivar\000"
.LASF23:
	.ascii	"WALK_THRU_extract_and_store_group_name_from_GuiVars"
	.ascii	"\000"
.LASF14:
	.ascii	"WALK_THRU_get_group_at_this_index\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
