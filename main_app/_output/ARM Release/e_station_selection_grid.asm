	.file	"e_station_selection_grid.c"
	.text
.Ltext0:
	.section	.text.STATION_SELECTION_GRID_save_changes,"ax",%progbits
	.align	2
	.type	STATION_SELECTION_GRID_save_changes, %function
STATION_SELECTION_GRID_save_changes:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, lr}
.LCFI0:
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L12
	ldr	r4, [r3, #0]
	cmp	r4, #7
	mov	r5, r0
	beq	.L4
	cmp	r4, #11
	beq	.L5
	cmp	r4, #1
	bne	.L1
	ldr	r3, .L12+4
	ldr	r3, [r3, #0]
	cmp	r3, #20
	beq	.L6
	cmp	r3, #25
	bne	.L1
	b	.L11
.L6:
	ldr	r2, .L12+8
	mov	r3, #2
	str	r0, [sp, #0]
	str	r4, [sp, #4]
	str	r3, [sp, #8]
	ldr	r0, .L12+12
	ldr	r1, .L12+16
	ldr	r2, [r2, #0]
	bl	STATION_store_group_assignment_changes
	b	.L1
.L11:
	ldr	r6, .L12+8
	ldr	r0, [r6, #0]
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	cmp	r0, #0
	beq	.L1
	mov	r1, #2
	str	r1, [sp, #0]
	ldr	r0, [r6, #0]
	mov	r2, r5
	mov	r3, r4
	b	.L10
.L4:
	ldr	r3, .L12+4
	ldr	r3, [r3, #0]
	cmp	r3, #22
	bne	.L1
	ldr	r4, .L12+8
	ldr	r0, [r4, #0]
	bl	MANUAL_PROGRAMS_get_group_with_this_GID
	cmp	r0, #0
	beq	.L1
	mov	r1, #2
	str	r1, [sp, #0]
	ldr	r0, [r4, #0]
	mov	r2, r5
	mov	r3, #1
.L10:
	bl	STATION_store_manual_programs_assignment_changes
	b	.L1
.L5:
	ldr	r3, .L12+4
	ldr	r3, [r3, #0]
	cmp	r3, #20
	bne	.L1
	mov	r0, #2
	mov	r1, r5
	mov	r2, r0
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, lr}
	b	STATION_store_stations_in_use
.L1:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, pc}
.L13:
	.align	2
.L12:
	.word	g_MAIN_MENU_active_menu_item
	.word	GuiVar_MenuScreenToShow
	.word	g_GROUP_ID
	.word	STATION_get_GID_station_group
	.word	nm_STATION_set_GID_station_group
.LFE15:
	.size	STATION_SELECTION_GRID_save_changes, .-STATION_SELECTION_GRID_save_changes
	.section	.text.__draw_group_name_and_controller_name,"ax",%progbits
	.align	2
	.type	__draw_group_name_and_controller_name, %function
__draw_group_name_and_controller_name:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L17
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r4, [r3, #0]
	ldr	r3, .L17+4
	ldr	r3, [r3, #4]
	b	.L15
.L16:
	sub	r4, r4, #24
.L15:
	ldr	r2, [r4, #8]
	cmp	r2, r3
	bhi	.L16
	ldr	r1, .L17+8
	mov	r2, #49
	ldr	r0, .L17+12
	bl	strlcpy
	ldr	r3, [r4, #4]
	mov	r2, #20
	mul	r3, r2, r3
	ldr	r2, .L17+16
	ldr	r1, .L17+20
	ldr	r0, [r2, r3]
	ldmfd	sp!, {r4, lr}
	b	NETWORK_CONFIG_get_controller_name_str_for_ui
.L18:
	.align	2
.L17:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	GuiVar_GroupName
	.word	GuiVar_StationSelectionGridGroupName
	.word	.LANCHOR2
	.word	GuiVar_StationSelectionGridControllerName
.LFE2:
	.size	__draw_group_name_and_controller_name, .-__draw_group_name_and_controller_name
	.section	.text.draw_station_selection_grid_title,"ax",%progbits
	.align	2
	.type	draw_station_selection_grid_title, %function
draw_station_selection_grid_title:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI2:
	ldr	r3, .L46
	ldr	r3, [r3, #0]
	cmp	r3, #7
	beq	.L22
	cmp	r3, #11
	beq	.L23
	cmp	r3, #1
	bne	.L19
	ldr	r3, .L46+4
	ldr	r3, [r3, #0]
	cmp	r3, #20
	beq	.L24
	cmp	r3, #25
	b	.L45
.L24:
	mov	r1, #0
	ldr	r0, .L46+8
	bl	GuiLib_GetTextPtr
	mov	r1, #0
	mov	r4, r0
	ldr	r0, .L46+12
	bl	GuiLib_GetTextPtr
	mov	r7, r0
	ldr	r0, .L46+16
	b	.L43
.L22:
	ldr	r3, .L46+4
	ldr	r3, [r3, #0]
	cmp	r3, #22
.L45:
	bne	.L19
	mov	r1, #0
	ldr	r0, .L46+8
	bl	GuiLib_GetTextPtr
	mov	r1, #0
	mov	r4, r0
	ldr	r0, .L46+12
	bl	GuiLib_GetTextPtr
	mov	r7, r0
	mov	r0, #980
.L43:
	mov	r1, #0
	bl	GuiLib_GetTextPtr
	mov	r1, #0
	mov	r6, r0
	ldr	r0, .L46+12
	bl	GuiLib_GetTextPtr
	ldr	r3, .L46+20
	ldr	r1, [r3, #0]
	cmp	r1, #0
	movne	r1, #0
	mov	r5, r0
	ldrne	r0, .L46+24
	ldreq	r0, .L46+28
	bl	GuiLib_GetTextPtr
	mov	r1, #129
	ldr	r2, .L46+32
	mov	r3, r4
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r0, [sp, #12]
	ldr	r0, .L46+36
	bl	snprintf
	b	.L19
.L23:
	ldr	r3, .L46+4
	ldr	r3, [r3, #0]
	cmp	r3, #20
	bne	.L19
	mov	r1, #0
	ldr	r0, .L46+40
	bl	GuiLib_GetTextPtr
	mov	r1, #0
	mov	r4, r0
	ldr	r0, .L46+12
	bl	GuiLib_GetTextPtr
	mov	r1, #0
	mov	r5, r0
	ldr	r0, .L46+44
	bl	GuiLib_GetTextPtr
	mov	r1, #129
	ldr	r2, .L46+48
	mov	r3, r4
	str	r5, [sp, #0]
	str	r0, [sp, #4]
	ldr	r0, .L46+36
	bl	snprintf
.L19:
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, pc}
.L47:
	.align	2
.L46:
	.word	g_MAIN_MENU_active_menu_item
	.word	GuiVar_MenuScreenToShow
	.word	1045
	.word	865
	.word	1066
	.word	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
	.word	1102
	.word	875
	.word	.LC0
	.word	GuiVar_StationSelectionGridScreenTitle
	.word	1051
	.word	1070
	.word	.LC1
.LFE14:
	.size	draw_station_selection_grid_title, .-draw_station_selection_grid_title
	.global	__udivsi3
	.section	.text.__move_scroll_box_marker,"ax",%progbits
	.align	2
	.type	__move_scroll_box_marker, %function
__move_scroll_box_marker:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	str	lr, [sp, #-4]!
.LCFI3:
	beq	.L49
	ldr	r3, .L51
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	pc, [sp], #4
.L49:
	ldr	r3, .L51+4
	ldr	r2, [r3, #16]
	ldr	r3, [r3, #8]
	cmp	r2, r3
	ldrls	pc, [sp], #4
.LBB7:
	ldr	r3, .L51+8
	ldr	r0, .L51+12
	ldr	r1, [r3, #0]
	bl	__udivsi3
	fmsr	s15, r0	@ int
	fuitos	s15, s15
	fmrs	r0, s15
	bl	__round_UNS16
	ldr	r3, .L51
	str	r0, [r3, #0]
	ldr	pc, [sp], #4
.L52:
	.align	2
.L51:
	.word	GuiVar_StationSelectionGridMarkerPos
	.word	.LANCHOR1
	.word	GuiVar_StationSelectionGridMarkerSize
	.word	2100
.LBE7:
.LFE1:
	.size	__move_scroll_box_marker, .-__move_scroll_box_marker
	.section	.text.__draw_station_description_for_selected_cursor.constprop.4,"ax",%progbits
	.align	2
	.type	__draw_station_description_for_selected_cursor.constprop.4, %function
__draw_station_description_for_selected_cursor.constprop.4:
.LFB22:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #768
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI4:
	sub	sp, sp, #20
.LCFI5:
	bcs	.L54
.LBB10:
	mov	r5, #20
	ldr	r7, .L58
	mul	r5, r0, r5
	add	r4, r7, r5
	ldr	r3, [r4, #16]
	cmp	r3, #0
	beq	.L57
	mov	r1, #0
	ldr	r0, .L58+4
	bl	GuiLib_GetTextPtr
	ldr	r5, [r7, r5]
	ldr	r1, [r4, #4]
	add	r2, sp, #12
	mov	r3, #8
	mov	r6, r0
	mov	r0, r5
	bl	STATION_get_station_number_string
	add	r5, r5, #65
	str	r5, [sp, #0]
	mov	r1, #48
	ldr	r2, .L58+8
	str	r0, [sp, #4]
	ldr	r3, [r4, #16]
	ldr	r0, .L58+12
	str	r3, [sp, #8]
	mov	r3, r6
	bl	snprintf
	cmp	r0, #0
	bne	.L53
	b	.L57
.L54:
.LBE10:
	ldr	r0, .L58+16
	ldr	r1, .L58+20
	bl	Alert_index_out_of_range_with_filename
.L57:
	ldr	r3, .L58+12
	mov	r2, #0
	strb	r2, [r3, #0]
.L53:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L59:
	.align	2
.L58:
	.word	.LANCHOR2
	.word	1058
	.word	.LC2
	.word	GuiVar_StationDescription
	.word	.LC3
	.word	334
.LFE22:
	.size	__draw_station_description_for_selected_cursor.constprop.4, .-__draw_station_description_for_selected_cursor.constprop.4
	.section	.text.__draw_station_number_for_current_cursor,"ax",%progbits
	.align	2
	.type	__draw_station_number_for_current_cursor, %function
__draw_station_number_for_current_cursor:
.LFB4:
	@ args = 0, pretend = 0, frame = 256
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, [r0, #8]
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI6:
	cmp	r3, #0
	sub	sp, sp, #304
.LCFI7:
	mov	r4, r0
	beq	.L60
	ldr	r3, [r0, #12]
	cmp	r3, #0
	beq	.L60
.LBB13:
	ldr	r3, .L75
	ldr	r3, [r3, #0]
	cmp	r0, r3
	ldreq	r0, [r0, #4]
	beq	.L74
	ldr	r2, .L75+4
	cmp	r3, r2
	bne	.L63
	mov	r0, #0
.L74:
	bl	__draw_station_description_for_selected_cursor.constprop.4
.L63:
	ldr	r3, [r4, #4]
	mov	r2, #20
	ldr	r1, .L75+8
	mul	r2, r3, r2
	add	r3, r1, r2
	ldr	r0, [r3, #8]
	cmp	r0, #1
	bne	.L64
	ldr	r0, [r1, r2]
	ldr	r1, [r3, #4]
	add	r2, sp, #48
	mov	r3, #256
	bl	STATION_get_station_number_string
	b	.L65
.L64:
	add	r0, sp, #48
	ldr	r1, .L75+12
	mov	r2, #256
	bl	strlcpy
.L65:
	ldr	r3, .L75+16
	mov	r2, #3
	ldr	r1, [r3, #8]
	add	r0, sp, #48
	bl	GuiLib_GetTextWidth
	ldr	r3, [r4, #0]
	mov	r1, #12
	mul	r3, r1, r3
	ldr	r2, .L75+20
	add	r1, r2, r3
	ldr	r5, [r2, r3]
	ldr	r3, [r4, #16]
	add	r5, r5, r3
	ldr	r3, [r1, #4]
	cmp	r0, r3
	mov	r6, r0
	moveq	r0, #1
	beq	.L66
	mvn	ip, #0
	ldr	r0, [r4, #8]
	str	ip, [sp, #0]
	ldr	ip, .L75+24
	mov	r2, #14
	str	ip, [sp, #4]
	mov	ip, #3
	str	ip, [sp, #8]
	str	ip, [sp, #12]
	mov	ip, #0
	str	ip, [sp, #16]
	str	ip, [sp, #20]
	ldr	r1, [r1, #8]
	mul	r0, r2, r0
	add	r3, r3, r1
	add	r3, r3, #3
	str	r3, [sp, #24]
	mov	r3, #15
	str	r3, [sp, #40]
	str	r3, [sp, #44]
	sub	r0, r0, #14
	mov	r1, r5
	mov	r3, #2
	str	ip, [sp, #28]
	str	ip, [sp, #32]
	str	ip, [sp, #36]
	bl	CS_DrawStr
.L66:
	ldr	r3, [r4, #0]
	ldr	r2, .L75+20
	mov	r1, #12
	mla	r3, r1, r3, r2
	str	r6, [r3, #4]
	ldr	r3, .L75
	ldr	r3, [r3, #0]
	cmp	r4, r3
	movne	r3, #15
	moveq	r3, #0
	movne	r1, #0
	moveq	r1, #15
	cmp	r0, #1
	bne	.L68
	ldr	r0, [r4, #8]
	mov	r2, #14
	mvn	ip, #0
	mul	r0, r2, r0
	str	ip, [sp, #0]
	add	ip, sp, #48
	str	ip, [sp, #4]
	mov	ip, #3
	str	ip, [sp, #8]
	str	ip, [sp, #12]
	str	r1, [sp, #40]
	mov	ip, #0
	str	r3, [sp, #44]
	sub	r0, r0, #14
	mov	r1, r5
	mov	r3, #2
	str	ip, [sp, #16]
	str	ip, [sp, #20]
	str	ip, [sp, #24]
	str	ip, [sp, #28]
	str	ip, [sp, #32]
	str	ip, [sp, #36]
	bl	CS_DrawStr
.L68:
	ldr	r3, [r4, #0]
	ldr	r2, .L75+20
	mov	r1, #12
	mla	r3, r1, r3, r2
	ldr	r6, .L75+28
	mov	r5, #0
	cmp	r0, #1
	str	r5, [r3, #8]
	strne	r5, [r6, #12]
	bne	.L60
	ldr	r4, [r6, #0]
	ldr	sl, .L75+32
	mov	r7, #160
	mla	sl, r7, r4, sl
	ldr	r8, .L75+36
	b	.L70
.L71:
	ldr	r2, [r6, #4]
	mov	r1, #14
	mla	r1, r2, r1, r4
	ldr	r3, [r6, #0]
	add	r0, sl, r5
	rsb	r1, r3, r1
	mla	r1, r7, r1, r8
	mov	r2, #155
	sub	r1, r1, #1760
	bl	memcpy
	add	r4, r4, #1
	add	r5, r5, #160
.L70:
	cmp	r4, #212
	bls	.L71
.L60:
.LBE13:
	add	sp, sp, #304
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L76:
	.align	2
.L75:
	.word	.LANCHOR0
	.word	.LANCHOR3
	.word	.LANCHOR2
	.word	.LC4
	.word	GuiFont_FontList
	.word	.LANCHOR4
	.word	.LC5
	.word	.LANCHOR1
	.word	primary_display_buf
	.word	utility_display_buf
.LFE4:
	.size	__draw_station_number_for_current_cursor, .-__draw_station_number_for_current_cursor
	.section	.text.__FDTO_move_cursor,"ax",%progbits
	.align	2
	.type	__FDTO_move_cursor, %function
__FDTO_move_cursor:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L105
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI8:
	ldr	r4, [r3, #0]
	cmp	r4, #0
	beq	.L78
	cmp	r0, #4
	bne	.L79
	ldr	r2, [r4, #8]
	cmp	r2, #1
	bne	.L100
	b	.L78
.L81:
	sub	r3, r3, #24
	sub	r0, r0, #1
	b	.L80
.L100:
	mov	r3, r4
	mov	r0, #0
	sub	r2, r2, #1
.L80:
	ldr	r1, [r3, #8]
	cmp	r1, r2
	bhi	.L81
	ldr	r2, [r4, #12]
	b	.L82
.L83:
	sub	r3, r3, #24
	sub	r0, r0, #1
.L82:
	ldr	r1, [r3, #12]
	cmp	r1, r2
	bhi	.L83
	b	.L84
.L79:
	cmp	r0, #0
	bne	.L85
	ldr	r3, .L105+4
	ldr	r2, [r4, #8]
	ldr	r3, [r3, #16]
	sub	r3, r3, #1
	cmp	r2, r3
	movls	r3, r4
	addls	r2, r2, #1
	bls	.L86
	b	.L78
.L87:
	add	r3, r3, #24
	add	r0, r0, #1
.L86:
	ldr	r1, [r3, #8]
	cmp	r1, r2
	bcc	.L87
	ldr	r2, [r4, #12]
	add	r3, r3, #8
	b	.L88
.L89:
	add	r0, r0, #1
.L88:
	ldr	r1, [r3, #4]
	cmp	r1, r2
	bcs	.L84
	ldr	ip, [r3, #24]
	ldr	r1, [r3], #24
	cmp	ip, r1
	beq	.L89
	b	.L84
.L85:
	cmp	r0, #1
	ldr	r2, [r4, #0]
	bne	.L90
	cmp	r2, #0
	mvnne	r0, #0
	bne	.L99
	b	.L78
.L90:
	ldr	r3, .L105+4
	ldr	r3, [r3, #12]
	sub	r1, r3, #1
	cmp	r2, r1
	beq	.L78
	cmp	r3, #0
	movne	r0, #1
	bne	.L99
	b	.L78
.L84:
	cmp	r0, #0
	beq	.L78
.L99:
	mov	r3, #24
	mla	r4, r3, r0, r4
	cmp	r4, #0
	beq	.L92
	ldr	r3, .L105+8
	cmp	r4, r3
	ldrcc	r0, .L105+12
	bcc	.L104
	add	r3, r3, #18432
	cmp	r4, r3
	ldrcs	r0, .L105+16
	bcs	.L104
	b	.L78
.L92:
	ldr	r0, .L105+20
.L104:
	bl	Alert_Message
	ldr	r3, .L105
	ldr	r4, [r3, #0]
.L78:
	ldr	r5, .L105
	ldr	r3, [r5, #0]
	cmp	r4, r3
	beq	.L96
	bl	good_key_beep
	ldr	r3, .L105+4
	ldr	r2, [r4, #8]
	ldr	r0, [r3, #8]
	ldr	r1, [r3, #4]
	ldr	r6, [r5, #0]
	add	ip, r1, r0
	sub	ip, ip, #1
	cmp	r2, ip
	addhi	r2, r2, #1
	rsbhi	r0, r0, r2
	str	r4, [r5, #0]
	strhi	r0, [r3, #4]
	bhi	.L98
	cmp	r2, r1
	strcc	r2, [r3, #4]
.L98:
	bl	__draw_group_name_and_controller_name
	mov	r0, r6
	bl	__draw_station_number_for_current_cursor
	ldr	r3, .L105
	ldr	r0, [r3, #0]
	bl	__draw_station_number_for_current_cursor
	mov	r0, #0
	bl	__move_scroll_box_marker
	ldr	r3, .L105+24
	ldr	r0, .L105+28
	ldrsh	r1, [r3, #0]
	mov	r2, #0
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, r5, r6, lr}
	b	GuiLib_Refresh
.L96:
	ldmfd	sp!, {r4, r5, r6, lr}
	b	bad_key_beep
.L106:
	.align	2
.L105:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR3
	.word	.LC7
	.word	.LC8
	.word	.LC6
	.word	GuiLib_ActiveCursorFieldNo
	.word	858
.LFE7:
	.size	__FDTO_move_cursor, .-__FDTO_move_cursor
	.section	.text.__FDTO_process_cursor_position,"ax",%progbits
	.align	2
	.type	__FDTO_process_cursor_position, %function
__FDTO_process_cursor_position:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI9:
	ldr	r5, .L125
	mov	r4, r0
	ldr	r3, [r5, #0]
	cmp	r3, #0
	beq	.L108
	ldr	r3, .L125+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L125+8
	ldr	r3, .L125+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r5, #0]
	ldr	r1, .L125+16
	ldr	r2, [r3, #4]
	mov	r0, #20
	cmp	r4, #80
	mla	r2, r0, r2, r1
	beq	.L111
	cmp	r4, #84
	beq	.L112
	cmp	r4, #2
	bne	.L119
	ldr	r1, [r2, #8]
	rsbs	r1, r1, #1
	movcc	r1, #0
	str	r1, [r2, #8]
	ldr	r2, .L125+20
	ldr	r2, [r2, #12]
	b	.L122
.L112:
	ldr	r1, [r2, #8]
	cmp	r1, #0
	ldr	r1, .L125+20
	ldrne	r2, [r1, #12]
	bne	.L124
	mov	r0, #1
	str	r0, [r2, #8]
	ldr	r2, [r1, #12]
.L122:
	ldr	r3, [r3, #0]
	sub	r2, r2, #1
	cmp	r3, r2
	bne	.L120
.L117:
	bl	good_key_beep
	b	.L114
.L111:
	mov	r1, #0
	str	r1, [r2, #8]
	ldr	r2, .L125+20
	ldr	r2, [r2, #12]
.L124:
	ldr	r3, [r3, #0]
	sub	r2, r2, #1
	cmp	r3, r2
	beq	.L119
.L120:
	mov	r0, #3
	bl	__FDTO_move_cursor
	b	.L114
.L119:
	bl	bad_key_beep
.L114:
	ldr	r3, .L125
	ldr	r0, [r3, #0]
	bl	__draw_station_number_for_current_cursor
	ldr	r3, .L125+4
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L108:
	ldmfd	sp!, {r4, r5, lr}
	b	bad_key_beep
.L126:
	.align	2
.L125:
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	.LC3
	.word	1075
	.word	.LANCHOR2
	.word	.LANCHOR1
.LFE8:
	.size	__FDTO_process_cursor_position, .-__FDTO_process_cursor_position
	.section	.text.__FDTO_process_manual_program_cursor_position,"ax",%progbits
	.align	2
	.type	__FDTO_process_manual_program_cursor_position, %function
__FDTO_process_manual_program_cursor_position:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI10:
	ldr	r4, .L160
	mov	r5, r0
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L128
	ldr	r3, .L160+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L160+8
	ldr	r3, .L160+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r4, #0]
	mov	r1, #20
	ldr	r3, [r3, #4]
	ldr	r2, .L160+16
	mul	r3, r1, r3
	add	r6, r2, r3
	ldr	r0, [r2, r3]
	ldr	r1, [r6, #4]
	bl	nm_STATION_get_pointer_to_station
	cmp	r5, #80
	mov	r7, r0
	beq	.L131
	cmp	r5, #84
	beq	.L132
	cmp	r5, #2
	bne	.L145
	ldr	r3, [r6, #8]
	cmp	r3, #0
	movne	r3, #0
	strne	r3, [r6, #8]
	ldrne	r2, [r4, #0]
	bne	.L154
	b	.L138
.L132:
	ldr	r3, [r6, #8]
	cmp	r3, #1
	ldreq	r2, [r4, #0]
	ldreq	r3, .L160+20
	beq	.L151
.L138:
	bl	nm_STATION_get_number_of_manual_programs_station_is_assigned_to
	cmp	r0, #1
	bls	.L140
	ldr	r4, .L160+24
	mov	r0, r7
	ldr	r5, [r4, #0]
	bl	STATION_get_GID_manual_program_A
	cmp	r5, r0
	beq	.L140
	mov	r0, r7
	ldr	r4, [r4, #0]
	bl	STATION_get_GID_manual_program_B
	cmp	r4, r0
	beq	.L140
	bl	bad_key_beep
	ldr	r0, .L160+28
	bl	DIALOG_draw_ok_dialog
	ldr	r3, .L160+32
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L135
.L140:
	mov	r3, #1
	str	r3, [r6, #8]
	ldr	r3, .L160
	ldr	r2, [r3, #0]
.L154:
	ldr	r3, .L160+20
	b	.L149
.L131:
	ldr	r3, [r6, #8]
	cmp	r3, #0
	movne	r2, #0
	strne	r2, [r6, #8]
	ldr	r3, .L160+20
	ldrne	r2, [r4, #0]
	bne	.L149
	ldr	r2, [r4, #0]
.L151:
	ldr	r3, [r3, #12]
	ldr	r2, [r2, #0]
	sub	r3, r3, #1
	cmp	r2, r3
	bne	.L146
	b	.L145
.L149:
	ldr	r3, [r3, #12]
	ldr	r2, [r2, #0]
	sub	r3, r3, #1
	cmp	r2, r3
	beq	.L144
.L146:
	mov	r0, #3
	bl	__FDTO_move_cursor
	b	.L135
.L144:
	bl	good_key_beep
	b	.L135
.L145:
	bl	bad_key_beep
.L135:
	ldr	r3, .L160
	ldr	r0, [r3, #0]
	bl	__draw_station_number_for_current_cursor
	ldr	r3, .L160+4
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L128:
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	bad_key_beep
.L161:
	.align	2
.L160:
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	.LC3
	.word	1179
	.word	.LANCHOR2
	.word	.LANCHOR1
	.word	g_GROUP_ID
	.word	623
	.word	.LANCHOR5
.LFE9:
	.size	__FDTO_process_manual_program_cursor_position, .-__FDTO_process_manual_program_cursor_position
	.section	.text.STATION_SELECTION_GRID_station_is_selected,"ax",%progbits
	.align	2
	.global	STATION_SELECTION_GRID_station_is_selected
	.type	STATION_SELECTION_GRID_station_is_selected, %function
STATION_SELECTION_GRID_station_is_selected:
.LFB10:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #0
	cmp	r0, #768
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI11:
	str	r3, [sp, #8]
	bcs	.L163
	ldr	r3, .L165
	mov	r2, #20
	mla	r0, r2, r0, r3
	ldr	r3, [r0, #8]
	str	r3, [sp, #8]
	b	.L164
.L163:
	ldr	r0, .L165+4
	ldr	r1, .L165+8
	bl	Alert_index_out_of_range_with_filename
.L164:
	ldr	r3, .L165+4
	mov	r1, #0
	str	r3, [sp, #0]
	ldr	r3, .L165+12
	add	r0, sp, #8
	str	r3, [sp, #4]
	mov	r2, r1
	ldr	r3, .L165+16
	bl	RC_bool_with_filename
	ldr	r0, [sp, #8]
	ldmfd	sp!, {r1, r2, r3, pc}
.L166:
	.align	2
.L165:
	.word	.LANCHOR2
	.word	.LC3
	.word	1348
	.word	1351
	.word	.LC9
.LFE10:
	.size	STATION_SELECTION_GRID_station_is_selected, .-STATION_SELECTION_GRID_station_is_selected
	.section	.text.STATION_SELECTION_GRID_get_count_of_selected_stations,"ax",%progbits
	.align	2
	.global	STATION_SELECTION_GRID_get_count_of_selected_stations
	.type	STATION_SELECTION_GRID_get_count_of_selected_stations, %function
STATION_SELECTION_GRID_get_count_of_selected_stations:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L171
	mov	r0, #0
	add	r2, r3, #15360
.L169:
	ldr	r1, [r3, #8]
	add	r3, r3, #20
	cmp	r1, #0
	addne	r0, r0, #1
	cmp	r3, r2
	bne	.L169
	bx	lr
.L172:
	.align	2
.L171:
	.word	.LANCHOR2
.LFE11:
	.size	STATION_SELECTION_GRID_get_count_of_selected_stations, .-STATION_SELECTION_GRID_get_count_of_selected_stations
	.section	.text.STATION_SELECTION_GRID_populate_cursors,"ax",%progbits
	.align	2
	.global	STATION_SELECTION_GRID_populate_cursors
	.type	STATION_SELECTION_GRID_populate_cursors, %function
STATION_SELECTION_GRID_populate_cursors:
.LFB12:
	@ args = 8, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI12:
	ldr	r6, [sp, #48]
	cmp	r3, #1
	str	r0, [sp, #4]
	mov	r4, r1
	str	r2, [sp, #8]
	mov	r5, r3
	ldr	r7, [sp, #52]
	bne	.L174
	ldr	r0, .L197
	mov	r1, #0
	mov	r2, #768
	bl	memset
.L174:
	ldr	r3, .L197+4
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L197+8
	ldr	r3, .L197+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [sp, #4]
	cmp	r3, #0
	beq	.L175
	ldr	r0, .L197+16
	bl	nm_ListGetFirst
	mov	sl, #0
	ldr	r8, .L197
	ldr	fp, .L197+16
	mov	r9, sl
	str	r0, [sp, #0]
	b	.L176
.L186:
	ldr	r0, [sp, #0]
	bl	nm_STATION_get_box_index_0
	str	r0, [r8, #0]
	ldr	r0, [sp, #0]
	bl	nm_STATION_get_station_number_0
	str	r0, [r8, #4]
	ldr	r0, [sp, #0]
	bl	nm_STATION_get_description
	str	r0, [r8, #16]
	ldmia	sp, {r0, r3}
	blx	r3
	ldr	r3, [sp, #8]
	cmp	r0, r3
	moveq	r0, #1
	beq	.L177
	cmp	r4, #0
	moveq	r0, r4
	beq	.L177
	ldr	r0, [sp, #0]
	blx	r4
	ldr	r3, [sp, #8]
	rsb	r3, r3, r0
	rsbs	r0, r3, #0
	adc	r0, r0, r3
.L177:
	cmp	r5, #1
	bne	.L178
	cmp	r0, #0
	beq	.L179
	ldr	r0, [sp, #0]
	bl	STATION_station_is_available_for_use
	adds	r0, r0, #0
	movne	r0, #1
.L179:
	str	r0, [r8, #8]
.L178:
	cmp	r6, #0
	bne	.L180
	ldr	r0, [sp, #0]
	bl	nm_STATION_get_physically_available
	b	.L196
.L180:
	cmp	r6, #1
	bne	.L182
	ldr	r0, [sp, #0]
	bl	STATION_station_is_available_for_use
.L196:
	cmp	r0, #1
	bne	.L182
	cmp	r7, #0
	beq	.L183
	cmp	r7, #1
	bne	.L182
	ldr	r3, [r8, #8]
	cmp	r3, #1
	bne	.L182
.L183:
	mov	r3, #1
	str	r3, [r8, #12]
	b	.L185
.L182:
	str	r9, [r8, #12]
.L185:
	ldr	r0, .L197+16
	ldr	r1, [sp, #0]
	bl	nm_ListGetNext
	add	sl, sl, #1
	add	r8, r8, #20
	str	r0, [sp, #0]
.L176:
	ldr	r3, [fp, #8]
	cmp	sl, r3
	bcc	.L186
	ldr	r3, .L197
	mov	r2, #20
	mla	r3, r2, sl, r3
	ldr	r1, .L197+20
	add	r3, r3, #12
	mov	r2, #0
	b	.L187
.L188:
	add	sl, sl, #1
	str	r2, [r3, #-20]
.L187:
	cmp	sl, r1
	add	r3, r3, #20
	bls	.L188
	b	.L189
.L175:
	ldr	r0, .L197+8
	ldr	r1, .L197+24
	bl	Alert_func_call_with_null_ptr_with_filename
.L189:
	ldr	r3, .L197+4
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	xQueueGiveMutexRecursive
.L198:
	.align	2
.L197:
	.word	.LANCHOR2
	.word	list_program_data_recursive_MUTEX
	.word	.LC3
	.word	1426
	.word	station_info_list_hdr
	.word	767
	.word	1494
.LFE12:
	.size	STATION_SELECTION_GRID_populate_cursors, .-STATION_SELECTION_GRID_populate_cursors
	.section	.text.FDTO_STATION_SELECTION_GRID_redraw_calling_screen,"ax",%progbits
	.align	2
	.global	FDTO_STATION_SELECTION_GRID_redraw_calling_screen
	.type	FDTO_STATION_SELECTION_GRID_redraw_calling_screen, %function
FDTO_STATION_SELECTION_GRID_redraw_calling_screen:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L200
	mov	r3, #0
	str	r3, [r2, #0]
	mov	r2, #1
	str	r2, [r0, #0]
	ldr	r2, .L200+4
	mov	r0, r3
	strh	r1, [r2, #0]	@ movhi
	b	FDTO_Redraw_Screen
.L201:
	.align	2
.L200:
	.word	.LANCHOR6
	.word	GuiLib_ActiveCursorFieldNo
.LFE13:
	.size	FDTO_STATION_SELECTION_GRID_redraw_calling_screen, .-FDTO_STATION_SELECTION_GRID_redraw_calling_screen
	.section	.text.FDTO_STATION_SELECTION_GRID_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_STATION_SELECTION_GRID_draw_screen
	.type	FDTO_STATION_SELECTION_GRID_draw_screen, %function
FDTO_STATION_SELECTION_GRID_draw_screen:
.LFB16:
	@ args = 0, pretend = 0, frame = 52
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L261
	mov	r3, #0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI13:
	mov	r4, r0
	sub	sp, sp, #100
.LCFI14:
	str	r3, [r2, #0]
	bne	.L203
	ldr	r2, .L261+4
	str	r1, [r2, #0]
	ldr	r2, .L261+8
	str	r3, [r2, #0]
	ldr	r2, .L261+12
	str	r3, [r2, #0]
.L203:
	ldr	r3, .L261+16
	ldr	r5, [r3, #0]
	cmp	r5, #7
	beq	.L206
	cmp	r5, #11
	beq	.L207
	cmp	r5, #1
	bne	.L204
	ldr	r3, .L261+20
	ldr	r3, [r3, #0]
	cmp	r3, #20
	beq	.L208
	cmp	r3, #25
	bne	.L204
	b	.L259
.L208:
	bl	draw_station_selection_grid_title
	ldr	r3, .L261+4
	str	r5, [sp, #0]
	ldr	r3, [r3, #0]
	ldr	r0, .L261+24
	str	r3, [sp, #4]
	mov	r1, #0
	b	.L258
.L259:
	bl	draw_station_selection_grid_title
	str	r5, [sp, #0]
	b	.L257
.L206:
	ldr	r3, .L261+20
	ldr	r3, [r3, #0]
	cmp	r3, #22
	bne	.L204
	bl	draw_station_selection_grid_title
	mov	r3, #1
	str	r3, [sp, #0]
.L257:
	ldr	r3, .L261+4
	ldr	r0, .L261+28
	ldr	r3, [r3, #0]
	ldr	r1, .L261+32
	str	r3, [sp, #4]
.L258:
	ldr	r3, .L261+36
	ldr	r2, [r3, #0]
	b	.L255
.L207:
	ldr	r3, .L261+20
	ldr	r3, [r3, #0]
	cmp	r3, #20
	bne	.L204
	ldr	r5, .L261+36
	mov	r3, #1
	str	r3, [r5, #0]
	ldr	r3, .L261+40
	mov	r6, #0
	strb	r6, [r3, #0]
	bl	draw_station_selection_grid_title
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	ldr	r0, .L261+44
	ldr	r2, [r5, #0]
	mov	r1, r6
.L255:
	mov	r3, r4
	bl	STATION_SELECTION_GRID_populate_cursors
.L204:
	cmp	r4, #1
	beq	.L212
	ldr	r3, .L261+12
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L213
.L212:
	mov	r0, #67
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
.L213:
.LBB19:
	mov	r0, #1
	bl	__move_scroll_box_marker
	ldr	r3, .L261+48
	mov	r1, #0
	str	r1, [r3, #0]
	mov	r2, #18432
	ldr	r0, .L261+52
	bl	memset
	ldr	r2, .L261+56
	mov	r3, #47
	str	r3, [r2, #0]
	ldr	r3, .L261+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldr	r3, .L261+8
	bne	.L214
	ldr	r1, [r3, #0]
	cmp	r1, #0
	moveq	r1, #1
	streq	r1, [r2, #4]
.L214:
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L245
	ldr	r3, .L261+4
	ldr	r6, [r3, #0]
	ldr	r3, .L261+60
	cmp	r6, #0
	movne	r6, #0
	movne	r2, r6
	bne	.L220
.L218:
	ldr	r2, [r3, #0]
	cmp	r2, #0
	bne	.L217
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L215
.L217:
	add	r6, r6, #1
	cmp	r6, #768
	add	r3, r3, #20
	bne	.L218
	b	.L245
.L220:
	ldr	r1, [r3, #0]
	cmp	r1, #0
	bne	.L215
	ldr	r1, [r3, #4]
	cmp	r1, #0
	bne	.L215
	ldr	r1, [r3, #8]
	add	r3, r3, #20
	cmp	r1, #0
	movne	r6, r2
	add	r2, r2, #1
	cmp	r2, #768
	bne	.L220
	b	.L215
.L245:
	mov	r6, #0
.L215:
	ldr	r9, .L261+56
	mov	r4, #0
	ldr	r0, [r9, #0]
	mov	r1, #14
	str	r4, [r9, #12]
	rsb	r0, r0, #212
	bl	__udivsi3
	ldr	r7, .L261+64
	mov	fp, #1
	mov	r5, fp
	mov	r8, fp
	mov	sl, #0
	fmsr	s15, r0	@ int
	fuitos	s15, s15
	fmrs	r0, s15
	bl	floorf
	mov	r1, #255
	mov	r2, #245760
	fmsr	s15, r0
	ldr	r0, .L261+68
	ftouizs	s15, s15
	fsts	s15, [r9, #8]	@ int
	bl	memset
	str	r4, [sp, #48]
	mov	r4, fp
.L231:
	ldr	r3, [r7, #0]
	cmp	r3, #1
	bne	.L221
	cmp	r8, #1
	bne	.L222
	ldr	r3, .L261+72
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L261+76
	mov	r3, #728
	bl	xQueueTakeMutexRecursive_debug
	cmp	r5, #1
	cmpeq	r4, #1
	movne	r9, #0
	moveq	r9, #1
	bne	.L223
	ldr	r1, .L261+80
	ldr	r0, [r7, #-12]
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	ldr	r0, [r7, #-12]
	mov	r4, r8
	str	r0, [sp, #48]
	bl	GuiLib_Refresh
	b	.L224
.L223:
	add	fp, sp, #52
	mov	r1, fp
	ldr	r0, [r7, #-12]
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	mov	r2, #14
	mvn	r3, #0
	mul	r0, r2, r4
	stmia	sp, {r3, fp}
	mov	r3, #2
	str	r3, [sp, #12]
	mov	r3, #300
	str	r3, [sp, #24]
	mov	r3, #12
	str	r3, [sp, #44]
	sub	r0, r0, #14
	mov	r1, r9
	mov	r3, r8
	str	r8, [sp, #8]
	str	r9, [sp, #16]
	str	r9, [sp, #20]
	str	r9, [sp, #28]
	str	r9, [sp, #32]
	str	r9, [sp, #36]
	str	r9, [sp, #40]
	bl	CS_DrawStr
	add	r4, r4, #1
	mov	fp, r0
.L224:
	ldr	r3, .L261+72
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L222:
	ldr	r3, .L261+56
	mov	r1, #24
	ldr	r2, [r3, #12]
	ldr	r0, .L261+52
	mul	r1, r2, r1
	add	r3, r0, r1
	str	r2, [r0, r1]
	mov	r1, #31
	mul	r1, r5, r1
	str	r5, [r3, #12]
	sub	r1, r1, #7
	str	r1, [r3, #16]
	mov	r1, #14
	mul	r1, r4, r1
	str	sl, [r3, #4]
	add	r1, r1, #57
	str	r1, [r3, #20]
	ldr	r1, .L261+8
	str	r4, [r3, #8]
	ldr	r1, [r1, #0]
	add	r2, r2, #1
	cmp	sl, r6
	cmpeq	r1, #1
	ldreq	r1, .L261+84
	streq	r3, [r1, #0]
	ldr	r3, .L261+56
	ldr	r1, .L261+60
	str	r2, [r3, #12]
	mov	r2, #20
	add	r3, sl, #1
	mla	r2, sl, r2, r1
	b	.L226
.L229:
	add	r2, r2, #20
	ldr	r0, [r2, #12]
	cmp	r0, #1
	moveq	r2, #20
	muleq	r3, r2, r3
	ldreq	r3, [r1, r3]
	beq	.L228
.L227:
	add	r3, r3, #1
.L226:
	cmp	r3, #768
	bne	.L229
	mov	r3, #0
.L228:
	ldr	r0, [sp, #48]
	cmp	r0, r3
	bne	.L230
	add	r5, r5, #1
	cmp	r5, #10
	addhi	r4, r4, #1
	movhi	r8, #0
	movhi	r5, #1
	bhi	.L221
	b	.L260
.L230:
	mov	r5, #1
	add	r4, r4, #1
	str	r3, [sp, #48]
	mov	r8, r5
	b	.L221
.L260:
	mov	r8, #0
.L221:
	add	sl, sl, #1
	cmp	sl, #768
	add	r7, r7, #20
	bne	.L231
	ldr	r3, .L261+56
	str	r4, [r3, #16]
.LBB20:
	ldr	r3, [r3, #8]
	cmp	r4, r3
	ldrls	r3, .L261+88
	movls	r2, #175
	strls	r2, [r3, #0]
	bls	.L233
.L232:
	mov	r1, #12
	mov	r0, r4
	bl	__udivsi3
	mov	r1, r0
	mov	r0, #175
	bl	__udivsi3
	fmsr	s15, r0	@ int
	fuitos	s15, s15
	fmrs	r0, s15
	bl	__round_UNS16
	ldr	r3, .L261+88
	str	r0, [r3, #0]
.L233:
	ldr	r8, .L261+56
.LBE20:
	cmp	fp, #1
	mov	r5, #0
	strne	r5, [r8, #12]
	bne	.L237
	ldr	r4, [r8, #0]
	ldr	sl, .L261+92
	mov	r6, #160
	mla	sl, r6, r4, sl
	ldr	r7, .L261+68
	b	.L235
.L236:
	ldr	r1, [r8, #0]
	add	r0, sl, r5
	rsb	r1, r1, r4
	mla	r1, r6, r1, r7
	mov	r2, #155
	add	r1, r1, #480
	bl	memcpy
	add	r4, r4, #1
	add	r5, r5, #160
.L235:
	cmp	r4, #212
	bls	.L236
.L237:
	ldr	r3, .L261+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L238
	ldr	r3, .L261+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldreq	r2, .L261+84
	streq	r3, [r2, #0]
.L238:
	ldr	r4, .L261+52
	mov	r5, #0
	mov	r7, r4
	mov	r6, #24
.L240:
	ldr	r3, [r4, #8]
	cmp	r3, #0
	beq	.L239
	ldr	r3, [r4, #12]
	cmp	r3, #0
	beq	.L239
	mla	r0, r6, r5, r7
	bl	__draw_station_number_for_current_cursor
.L239:
	add	r5, r5, #1
	cmp	r5, #768
	add	r4, r4, #24
	bne	.L240
.LBB21:
	ldr	r3, .L261+84
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L241
	ldr	r3, .L261+56
	ldr	r7, .L261+48
	ldr	r1, [r3, #16]
	ldr	r2, [r3, #8]
	sub	r1, r1, #1
	cmp	r1, r2
	movls	r2, #1
	rsbhi	r2, r2, r1
	str	r2, [r3, #4]
	ldr	r5, .L261+52
	ldr	r3, [r7, #0]
	mov	r6, #24
	mla	r3, r6, r3, r5
	ldr	r4, .L261+84
	mov	r0, #0
	str	r3, [r4, #0]
	bl	__move_scroll_box_marker
	ldr	r7, [r7, #0]
	bl	__draw_group_name_and_controller_name
	mla	r0, r6, r7, r5
	ldr	r3, [r0, #8]
	cmp	r3, #0
	beq	.L244
	ldr	r3, [r0, #12]
	cmp	r3, #0
	beq	.L244
	bl	__draw_station_number_for_current_cursor
.L244:
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #4]
	bl	__draw_station_description_for_selected_cursor.constprop.4
.L241:
.LBE21:
	bl	GuiLib_Refresh
.LBE19:
	ldr	r2, .L261+12
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L261+8
	str	r3, [r2, #0]
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L262:
	.align	2
.L261:
	.word	.LANCHOR6
	.word	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
	.word	.LANCHOR7
	.word	.LANCHOR5
	.word	g_MAIN_MENU_active_menu_item
	.word	GuiVar_MenuScreenToShow
	.word	STATION_get_GID_station_group
	.word	STATION_get_GID_manual_program_A
	.word	STATION_get_GID_manual_program_B
	.word	g_GROUP_ID
	.word	GuiVar_GroupName
	.word	STATION_station_is_available_for_use
	.word	.LANCHOR8
	.word	.LANCHOR3
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR2+12
	.word	utility_display_buf
	.word	chain_members_recursive_MUTEX
	.word	.LC3
	.word	GuiVar_StationSelectionGridControllerName
	.word	.LANCHOR0
	.word	GuiVar_StationSelectionGridMarkerSize
	.word	primary_display_buf
.LFE16:
	.size	FDTO_STATION_SELECTION_GRID_draw_screen, .-FDTO_STATION_SELECTION_GRID_draw_screen
	.section	.text.STATION_SELECTION_GRID_process_screen,"ax",%progbits
	.align	2
	.global	STATION_SELECTION_GRID_process_screen
	.type	STATION_SELECTION_GRID_process_screen, %function
STATION_SELECTION_GRID_process_screen:
.LFB17:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r4, r5, lr}
.LCFI15:
	mov	r4, r0
	sub	sp, sp, #40
.LCFI16:
	mov	r5, r1
	bhi	.L269
	cmp	r0, #3
	bcs	.L265
	cmp	r0, #1
	bls	.L265
	b	.L266
.L269:
	cmp	r0, #67
	beq	.L268
	bhi	.L270
	cmp	r0, #16
	beq	.L267
	cmp	r0, #20
	bne	.L264
	b	.L267
.L270:
	cmp	r0, #80
	beq	.L266
	cmp	r0, #84
	bne	.L264
.L266:
	ldr	r3, .L295
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L285
.L271:
	ldr	r3, .L295+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L273
	ldr	r3, .L295+8
	ldr	r3, [r3, #0]
	cmp	r3, #25
	b	.L287
.L273:
	cmp	r3, #7
	bne	.L275
	ldr	r3, .L295+8
	ldr	r3, [r3, #0]
	cmp	r3, #22
.L287:
	moveq	r3, #2
	streq	r3, [sp, #4]
	ldreq	r3, .L295+12
	beq	.L289
.L275:
	mov	r3, #2
	str	r3, [sp, #4]
	ldr	r3, .L295+16
.L289:
	add	r0, sp, #4
	str	r3, [sp, #24]
	str	r4, [sp, #28]
	bl	Display_Post_Command
	b	.L263
.L265:
	mov	r3, #2
	str	r3, [sp, #4]
	ldr	r3, .L295+20
	b	.L289
.L267:
	bl	STATION_SELECTION_GRID_save_changes
	ldr	r3, .L295+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L277
	cmp	r3, #7
	bne	.L285
	b	.L294
.L277:
	ldr	r3, .L295+8
	ldr	r3, [r3, #0]
	cmp	r3, #20
	beq	.L279
	cmp	r3, #25
	b	.L293
.L279:
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r3, .L295+24
	ldr	r2, .L295+28
	str	r3, [sp, #0]
	ldr	r3, .L295+32
	mov	r1, r0
	mov	r0, r4
	b	.L291
.L294:
	ldr	r3, .L295+8
	ldr	r3, [r3, #0]
	cmp	r3, #22
.L293:
	bne	.L263
	bl	MANUAL_PROGRAMS_get_num_groups_in_use
	ldr	r3, .L295+36
	ldr	r2, .L295+40
	str	r3, [sp, #0]
	ldr	r3, .L295+44
	mov	r1, r0
	mov	r0, r4
.L291:
	bl	GROUP_process_NEXT_and_PREV
	b	.L263
.L285:
	bl	bad_key_beep
	b	.L263
.L268:
	ldr	r4, .L295+48
	mov	r3, #1
	str	r3, [r4, #0]
	bl	STATION_SELECTION_GRID_save_changes
	ldr	r3, .L295+4
	ldr	r3, [r3, #0]
	cmp	r3, #11
	bne	.L282
	ldr	r3, .L295+8
	ldr	r2, [r3, #0]
	cmp	r2, #20
	bne	.L282
	mov	r2, #0
	str	r2, [r4, #0]
	ldr	r2, .L295+52
	ldr	r1, .L295+56
	ldr	r2, [r2, #0]
	mov	r0, #36
	mla	r2, r0, r2, r1
	ldr	r2, [r2, #4]
	cmp	r2, #0
	movne	r2, #11
	moveq	r2, #0
	str	r2, [r3, #0]
.L282:
	mov	r0, #67
	b	.L288
.L264:
	mov	r0, r4
.L288:
	mov	r1, r5
	bl	KEY_process_global_keys
.L263:
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, pc}
.L296:
	.align	2
.L295:
	.word	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
	.word	g_MAIN_MENU_active_menu_item
	.word	GuiVar_MenuScreenToShow
	.word	__FDTO_process_manual_program_cursor_position
	.word	__FDTO_process_cursor_position
	.word	__FDTO_move_cursor
	.word	STATION_GROUP_copy_group_into_guivars
	.word	STATION_GROUP_extract_and_store_changes_from_GuiVars
	.word	STATION_GROUP_get_group_at_this_index
	.word	MANUAL_PROGRAMS_copy_group_into_guivars
	.word	MANUAL_PROGRAMS_extract_and_store_changes_from_GuiVars
	.word	MANUAL_PROGRAMS_get_group_at_this_index
	.word	.LANCHOR6
	.word	screen_history_index
	.word	ScreenHistory
.LFE17:
	.size	STATION_SELECTION_GRID_process_screen, .-STATION_SELECTION_GRID_process_screen
	.global	g_STATION_SELECTION_GRID_user_pressed_back
	.section	.bss.g_STATION_SELECTION_GRID_dialog_displayed,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	g_STATION_SELECTION_GRID_dialog_displayed, %object
	.size	g_STATION_SELECTION_GRID_dialog_displayed, 4
g_STATION_SELECTION_GRID_dialog_displayed:
	.space	4
	.section	.bss.Group_CP,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	Group_CP, %object
	.size	Group_CP, 18432
Group_CP:
	.space	18432
	.section	.bss.station_selection_struct,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	station_selection_struct, %object
	.size	station_selection_struct, 15360
station_selection_struct:
	.space	15360
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%s %s %s %s %s\000"
.LC1:
	.ascii	"%s %s %s\000"
.LC2:
	.ascii	"%s %c %s: %s\000"
.LC3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_station_selection_grid.c\000"
.LC4:
	.ascii	"---\000"
.LC5:
	.ascii	" \000"
.LC6:
	.ascii	"CUR: move failed - NULL\000"
.LC7:
	.ascii	"CUR: move failed - out-of-range @ start\000"
.LC8:
	.ascii	"CUR: move failed - out-of-range @ end\000"
.LC9:
	.ascii	"Station Selected\000"
	.section	.bss.g_STATION_SELECTION_GRID_user_toggled_display,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	g_STATION_SELECTION_GRID_user_toggled_display, %object
	.size	g_STATION_SELECTION_GRID_user_toggled_display, 4
g_STATION_SELECTION_GRID_user_toggled_display:
	.space	4
	.section	.bss.g_CURSOR_mgmt_add_on,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_CURSOR_mgmt_add_on, %object
	.size	g_CURSOR_mgmt_add_on, 9216
g_CURSOR_mgmt_add_on:
	.space	9216
	.section	.bss.CURSOR_statistics,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	CURSOR_statistics, %object
	.size	CURSOR_statistics, 20
CURSOR_statistics:
	.space	20
	.section	.bss.CURSOR_current_cursor_ptr,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	CURSOR_current_cursor_ptr, %object
	.size	CURSOR_current_cursor_ptr, 4
CURSOR_current_cursor_ptr:
	.space	4
	.section	.bss.g_CURSOR_last_station_selected,"aw",%nobits
	.align	2
	.set	.LANCHOR8,. + 0
	.type	g_CURSOR_last_station_selected, %object
	.size	g_CURSOR_last_station_selected, 4
g_CURSOR_last_station_selected:
	.space	4
	.section	.bss.g_STATION_SELECTION_GRID_user_pressed_back,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	g_STATION_SELECTION_GRID_user_pressed_back, %object
	.size	g_STATION_SELECTION_GRID_user_pressed_back, 4
g_STATION_SELECTION_GRID_user_pressed_back:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI0-.LFB15
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x82
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI2-.LFB14
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI4-.LFB22
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI6-.LFB4
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x14c
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI8-.LFB7
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI9-.LFB8
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI10-.LFB9
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI11-.LFB10
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI12-.LFB12
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x82
	.uleb128 0xa
	.byte	0x81
	.uleb128 0xb
	.byte	0x80
	.uleb128 0xc
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI13-.LFB16
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x88
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI15-.LFB17
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE28:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_station_selection_grid.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x178
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF18
	.byte	0x1
	.4byte	.LASF19
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x129
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x169
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0xdb
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0xca
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1fd
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x25e
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x61b
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF7
	.byte	0x1
	.byte	0xfb
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x5e9
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST2
	.uleb128 0x6
	.4byte	0x33
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST3
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST4
	.uleb128 0x6
	.4byte	0x2a
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST5
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x372
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST6
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x42a
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST7
	.uleb128 0x4
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x490
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST8
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x538
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST9
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x54d
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x57d
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST10
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x5dd
	.4byte	.LFB13
	.4byte	.LFE13
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x656
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST11
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x6ac
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST12
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB15
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB14
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB22
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI5
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB4
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI7
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 332
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB12
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB16
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI14
	.4byte	.LFE16
	.2byte	0x3
	.byte	0x7d
	.sleb128 136
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB17
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI16
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x8c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF9:
	.ascii	"__FDTO_move_cursor\000"
.LASF17:
	.ascii	"STATION_SELECTION_GRID_process_screen\000"
.LASF10:
	.ascii	"__FDTO_process_cursor_position\000"
.LASF0:
	.ascii	"__draw_station_description_for_selected_cursor\000"
.LASF1:
	.ascii	"__draw_station_number_for_current_cursor\000"
.LASF15:
	.ascii	"FDTO_STATION_SELECTION_GRID_redraw_calling_screen\000"
.LASF18:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF16:
	.ascii	"FDTO_STATION_SELECTION_GRID_draw_screen\000"
.LASF13:
	.ascii	"STATION_SELECTION_GRID_get_count_of_selected_statio"
	.ascii	"ns\000"
.LASF14:
	.ascii	"STATION_SELECTION_GRID_populate_cursors\000"
.LASF4:
	.ascii	"__show_cursor\000"
.LASF5:
	.ascii	"__FDTO_draw_all_cursor_positions\000"
.LASF19:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_station_selection_grid.c\000"
.LASF3:
	.ascii	"__resize_scroll_box_marker\000"
.LASF2:
	.ascii	"__move_scroll_box_marker\000"
.LASF11:
	.ascii	"__FDTO_process_manual_program_cursor_position\000"
.LASF6:
	.ascii	"STATION_SELECTION_GRID_save_changes\000"
.LASF12:
	.ascii	"STATION_SELECTION_GRID_station_is_selected\000"
.LASF7:
	.ascii	"__draw_group_name_and_controller_name\000"
.LASF8:
	.ascii	"draw_station_selection_grid_title\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
