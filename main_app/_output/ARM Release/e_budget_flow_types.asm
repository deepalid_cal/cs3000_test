	.file	"e_budget_flow_types.c"
	.text
.Ltext0:
	.section	.text.BUDGET_FLOW_TYPES_process_group,"ax",%progbits
	.align	2
	.global	BUDGET_FLOW_TYPES_process_group
	.type	BUDGET_FLOW_TYPES_process_group, %function
BUDGET_FLOW_TYPES_process_group:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r0, r4, lr}
.LCFI0:
	mov	r4, r0
	beq	.L7
	ldr	r3, .L25
	bhi	.L10
	cmp	r0, #1
	beq	.L4
	bcc	.L3
	cmp	r0, #2
	beq	.L5
	cmp	r0, #3
	bne	.L2
	b	.L3
.L10:
	cmp	r0, #67
	beq	.L9
	bhi	.L11
	cmp	r0, #16
	beq	.L8
	cmp	r0, #20
	bne	.L2
	b	.L8
.L11:
	cmp	r0, #80
	beq	.L5
	cmp	r0, #84
	bne	.L2
.L5:
	ldrsh	r3, [r3, #0]
	cmp	r3, #2
	beq	.L14
	cmp	r3, #3
	bne	.L24
	b	.L15
.L14:
	ldr	r0, .L25+4
	b	.L22
.L15:
	ldr	r0, .L25+8
.L22:
	bl	process_bool
	b	.L16
.L24:
	bl	bad_key_beep
.L16:
	bl	Refresh_Screen
	b	.L17
.L8:
	bl	SYSTEM_num_systems_in_use
	ldr	r3, .L25+12
	ldr	r2, .L25+16
	str	r3, [sp, #0]
	ldr	r3, .L25+20
	mov	r1, r0
	mov	r0, r4
	bl	GROUP_process_NEXT_and_PREV
	b	.L17
.L7:
	mov	r0, #1
	b	.L21
.L4:
	ldrsh	r3, [r3, #0]
	cmp	r3, #1
	beq	.L9
.L21:
	bl	CURSOR_Up
	b	.L23
.L3:
	mov	r0, #1
	bl	CURSOR_Down
.L23:
	mov	r0, #0
	bl	Redraw_Screen
	b	.L17
.L9:
	ldr	r0, .L25+24
	bl	KEY_process_BACK_from_editing_screen
	b	.L17
.L2:
	mov	r0, r4
	bl	KEY_process_global_keys
.L17:
	ldr	r3, .L25
	ldrsh	r2, [r3, #0]
	ldr	r3, .L25+28
	str	r2, [r3, #0]
	ldmfd	sp!, {r3, r4, pc}
.L26:
	.align	2
.L25:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_BudgetFlowTypeMVOR
	.word	GuiVar_BudgetFlowTypeNonController
	.word	BUDGET_FLOW_TYPES_copy_group_into_guivars
	.word	BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars
	.word	SYSTEM_get_group_at_this_index
	.word	FDTO_BUDGET_FLOW_TYPES_return_to_menu
	.word	GuiVar_BudgetFlowTypeIndex
.LFE0:
	.size	BUDGET_FLOW_TYPES_process_group, .-BUDGET_FLOW_TYPES_process_group
	.section	.text.FDTO_BUDGET_FLOW_TYPES_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_BUDGET_FLOW_TYPES_return_to_menu, %function
FDTO_BUDGET_FLOW_TYPES_return_to_menu:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L28
	ldr	r1, .L28+4
	b	FDTO_GROUP_return_to_menu
.L29:
	.align	2
.L28:
	.word	.LANCHOR0
	.word	BUDGET_FLOW_TYPES_extract_and_store_changes_from_GuiVars
.LFE1:
	.size	FDTO_BUDGET_FLOW_TYPES_return_to_menu, .-FDTO_BUDGET_FLOW_TYPES_return_to_menu
	.section	.text.FDTO_BUDGET_FLOW_TYPES_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_BUDGET_FLOW_TYPES_draw_menu
	.type	FDTO_BUDGET_FLOW_TYPES_draw_menu, %function
FDTO_BUDGET_FLOW_TYPES_draw_menu:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI1:
	ldr	r5, .L32
	mov	r4, r0
	mov	r1, #400
	ldr	r2, .L32+4
	mov	r3, #156
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	ldr	r3, .L32+8
	ldr	r1, .L32+12
	str	r3, [sp, #0]
	ldr	r3, .L32+16
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r3, #10
	mov	r2, r0
	mov	r0, r4
	bl	FDTO_GROUP_draw_menu
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r4, #0
	ldrne	r3, .L32+20
	movne	r2, #1
	strne	r2, [r3, #0]
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L33:
	.align	2
.L32:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	SYSTEM_load_system_name_into_scroll_box_guivar
	.word	.LANCHOR0
	.word	BUDGET_FLOW_TYPES_copy_group_into_guivars
	.word	GuiVar_BudgetFlowTypeIndex
.LFE2:
	.size	FDTO_BUDGET_FLOW_TYPES_draw_menu, .-FDTO_BUDGET_FLOW_TYPES_draw_menu
	.section	.text.BUDGET_FLOW_TYPES_process_menu,"ax",%progbits
	.align	2
	.global	BUDGET_FLOW_TYPES_process_menu
	.type	BUDGET_FLOW_TYPES_process_menu, %function
BUDGET_FLOW_TYPES_process_menu:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r6, r7, lr}
.LCFI2:
	ldr	r4, .L35
	mov	r6, r0
	mov	r7, r1
	ldr	r2, .L35+4
	mov	r1, #400
	mov	r3, #176
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	ldr	r2, .L35+8
	mov	r1, r7
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	ldr	r2, .L35+12
	str	r2, [sp, #8]
	ldr	r2, .L35+16
	str	r2, [sp, #12]
	ldr	r2, .L35+20
	mov	r3, r0
	mov	r0, r6
	bl	GROUP_process_menu
	ldr	r0, [r4, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L36:
	.align	2
.L35:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	BUDGET_FLOW_TYPES_process_group
	.word	SYSTEM_get_group_at_this_index
	.word	BUDGET_FLOW_TYPES_copy_group_into_guivars
	.word	.LANCHOR0
.LFE3:
	.size	BUDGET_FLOW_TYPES_process_menu, .-BUDGET_FLOW_TYPES_process_menu
	.section	.bss.g_BUDGET_SETUP_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_BUDGET_SETUP_editing_group, %object
	.size	g_BUDGET_SETUP_editing_group, 4
g_BUDGET_SETUP_editing_group:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budget_flow_types.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_budget_flow_types.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x2c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.byte	0x94
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x9a
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xae
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"BUDGET_FLOW_TYPES_process_menu\000"
.LASF1:
	.ascii	"FDTO_BUDGET_FLOW_TYPES_draw_menu\000"
.LASF0:
	.ascii	"BUDGET_FLOW_TYPES_process_group\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budget_flow_types.c\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"FDTO_BUDGET_FLOW_TYPES_return_to_menu\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
