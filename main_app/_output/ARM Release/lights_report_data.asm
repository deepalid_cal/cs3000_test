	.file	"lights_report_data.c"
	.text
.Ltext0:
	.section	.text.nm_LIGHTS_REPORT_DATA_get_previous_completed_record,"ax",%progbits
	.align	2
	.type	nm_LIGHTS_REPORT_DATA_get_previous_completed_record, %function
nm_LIGHTS_REPORT_DATA_get_previous_completed_record:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L9
	add	r2, r3, #60
	cmp	r0, r2
	bcc	.L7
	add	r1, r2, #10752
	cmp	r0, r1
	bcs	.L7
	ldr	r1, [r3, #12]
	cmp	r1, #1
	beq	.L7
	cmp	r0, r2
	subne	r0, r0, #16
	bne	.L2
	ldr	r0, [r3, #8]
	ldr	r3, .L9+4
	cmp	r0, #1
	moveq	r0, r3
	movne	r0, #0
	b	.L2
.L7:
	mov	r0, #0
.L2:
	ldr	r3, .L9
	ldr	r2, [r3, #4]
	add	r2, r3, r2, asl #4
	add	r2, r2, #60
	cmp	r0, r2
	moveq	r2, #1
	streq	r2, [r3, #12]
	bx	lr
.L10:
	.align	2
.L9:
	.word	.LANCHOR0
	.word	.LANCHOR0+10796
.LFE9:
	.size	nm_LIGHTS_REPORT_DATA_get_previous_completed_record, .-nm_LIGHTS_REPORT_DATA_get_previous_completed_record
	.section	.text.nm_init_lights_report_data_records,"ax",%progbits
	.align	2
	.type	nm_init_lights_report_data_records, %function
nm_init_lights_report_data_records:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldr	r0, .L14
	mov	r1, #0
	mov	r2, #60
	bl	memset
	ldr	r5, .L14
	mov	r4, #0
.L12:
	add	r0, r5, r4, asl #4
.LBB11:
	add	r0, r0, #60
	mov	r1, #0
	mov	r2, #16
.LBE11:
	add	r4, r4, #1
.LBB12:
	bl	memset
.LBE12:
	cmp	r4, #48
	bne	.L12
	ldmfd	sp!, {r4, r5, pc}
.L15:
	.align	2
.L14:
	.word	.LANCHOR0
.LFE1:
	.size	nm_init_lights_report_data_records, .-nm_init_lights_report_data_records
	.section	.text.lights_report_data_ci_timer_callback,"ax",%progbits
	.align	2
	.type	lights_report_data_ci_timer_callback, %function
lights_report_data_ci_timer_callback:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L17
	mov	r1, #0
	mov	r2, #512
	mov	r3, r1
	b	CONTROLLER_INITIATED_post_to_messages_queue
.L18:
	.align	2
.L17:
	.word	406
.LFE5:
	.size	lights_report_data_ci_timer_callback, .-lights_report_data_ci_timer_callback
	.section	.text.nm_lights_report_data_updater,"ax",%progbits
	.align	2
	.type	nm_lights_report_data_updater, %function
nm_lights_report_data_updater:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r4, r0
	bne	.L20
	ldr	r0, .L22
	mov	r1, r4
	ldmfd	sp!, {r4, lr}
	b	Alert_Message_va
.L20:
	ldr	r0, .L22+4
	mov	r1, #1
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	bne	.L21
	ldmfd	sp!, {r4, lr}
	b	nm_init_lights_report_data_records
.L21:
	ldr	r0, .L22+8
	ldmfd	sp!, {r4, lr}
	b	Alert_Message
.L23:
	.align	2
.L22:
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE2:
	.size	nm_lights_report_data_updater, .-nm_lights_report_data_updater
	.section	.text.init_file_lights_report_data,"ax",%progbits
	.align	2
	.global	init_file_lights_report_data
	.type	init_file_lights_report_data, %function
init_file_lights_report_data:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L25
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r4, .L25+4
	sub	sp, sp, #28
.LCFI3:
	str	r3, [sp, #0]
	ldr	r3, .L25+8
	mov	r0, #1
	str	r3, [sp, #4]
	ldr	r3, .L25+12
	ldr	r1, .L25+16
	str	r3, [sp, #8]
	ldr	r3, .L25+20
	mov	r2, r0
	ldr	r3, [r3, #0]
	str	r3, [sp, #12]
	ldr	r3, .L25+24
	str	r3, [sp, #16]
	ldr	r3, .L25+28
	str	r3, [sp, #20]
	mov	r3, #17
	str	r3, [sp, #24]
	mov	r3, r4
	bl	FLASH_FILE_find_or_create_reports_file
	mov	r3, #0
	str	r3, [r4, #24]
	add	sp, sp, #28
	ldmfd	sp!, {r4, pc}
.L26:
	.align	2
.L25:
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR3
	.word	10812
	.word	.LANCHOR1
	.word	lights_report_completed_records_recursive_MUTEX
	.word	nm_lights_report_data_updater
	.word	nm_init_lights_report_data_records
.LFE3:
	.size	init_file_lights_report_data, .-init_file_lights_report_data
	.section	.text.save_file_lights_report_data,"ax",%progbits
	.align	2
	.global	save_file_lights_report_data
	.type	save_file_lights_report_data, %function
save_file_lights_report_data:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L28
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI4:
	ldr	r1, .L28+4
	str	r3, [sp, #0]
	ldr	r3, .L28+8
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, r0
	str	r3, [sp, #4]
	mov	r3, #17
	str	r3, [sp, #8]
	ldr	r3, .L28+12
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L29:
	.align	2
.L28:
	.word	10812
	.word	.LANCHOR1
	.word	lights_report_completed_records_recursive_MUTEX
	.word	.LANCHOR0
.LFE4:
	.size	save_file_lights_report_data, .-save_file_lights_report_data
	.global	__udivsi3
	.section	.text.LIGHTS_REPORT_DATA_start_the_ci_timer_if_it_is_not_running,"ax",%progbits
	.align	2
	.global	LIGHTS_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	.type	LIGHTS_REPORT_DATA_start_the_ci_timer_if_it_is_not_running, %function
LIGHTS_REPORT_DATA_start_the_ci_timer_if_it_is_not_running:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI5:
	ldr	r4, .L33
	ldr	r2, [r4, #0]
	cmp	r2, #0
	bne	.L31
	ldr	r3, .L33+4
	ldr	r0, .L33+8
	str	r3, [sp, #0]
	ldr	r1, .L33+12
	mov	r3, r2
	bl	xTimerCreate
	cmp	r0, #0
	str	r0, [r4, #0]
	bne	.L31
	ldr	r0, .L33+16
	bl	RemovePathFromFileName
	ldr	r2, .L33+20
	mov	r1, r0
	ldr	r0, .L33+24
	bl	Alert_Message_va
.L31:
	ldr	r5, .L33
	ldr	r0, [r5, #0]
	cmp	r0, #0
	beq	.L30
	bl	xTimerIsTimerActive
	subs	r4, r0, #0
	bne	.L30
	ldr	r3, .L33+28
	ldr	r5, [r5, #0]
	ldr	r0, [r3, #48]
	bl	CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records
	mov	r1, #5
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, r4
	mov	r2, r0
	mov	r0, r5
	bl	xTimerGenericCommand
.L30:
	ldmfd	sp!, {r3, r4, r5, pc}
.L34:
	.align	2
.L33:
	.word	.LANCHOR4
	.word	lights_report_data_ci_timer_callback
	.word	.LC3
	.word	12000
	.word	.LC4
	.word	289
	.word	.LC5
	.word	config_c
.LFE6:
	.size	LIGHTS_REPORT_DATA_start_the_ci_timer_if_it_is_not_running, .-LIGHTS_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	.section	.text.nm_LIGHTS_REPORT_DATA_inc_index,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_REPORT_DATA_inc_index
	.type	nm_LIGHTS_REPORT_DATA_inc_index, %function
nm_LIGHTS_REPORT_DATA_inc_index:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #0]
	add	r3, r3, #1
	cmp	r3, #672
	movcs	r3, #0
	str	r3, [r0, #0]
	bx	lr
.LFE7:
	.size	nm_LIGHTS_REPORT_DATA_inc_index, .-nm_LIGHTS_REPORT_DATA_inc_index
	.section	.text.LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_lines,"ax",%progbits
	.align	2
	.global	LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_lines
	.type	LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_lines, %function
LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_lines:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L45
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI6:
	ldr	r6, .L45+4
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L45+8
	ldr	r2, .L45+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r6, #0]
	cmp	r3, #0
	bne	.L39
	mov	r0, #2688
	ldr	r1, .L45+12
	mov	r2, #508
	bl	mem_malloc_debug
	str	r0, [r6, #0]
.L39:
	mov	r1, r4
	mov	r0, r5
	bl	LIGHTS_get_lights_array_index
	ldr	r7, .L45+4
	ldr	r2, .L45+16
	mov	r1, #20
	ldr	r3, [r7, #0]
	mov	r6, #1
	ldr	r8, .L45+20
	mla	r2, r1, r0, r2
	str	r2, [r3, #0]
.LBB13:
	ldr	r3, .L45+24
	mov	r2, #0
	str	r2, [r3, #12]
	ldr	r2, [r3, #4]
	add	r3, r3, r2, asl #4
	add	r0, r3, #60
	bl	nm_LIGHTS_REPORT_DATA_get_previous_completed_record
.LBE13:
	b	.L40
.L44:
	ldrb	r3, [r0, #12]	@ zero_extendqisi2
	cmp	r3, r5
	bne	.L41
	ldrb	r3, [r0, #13]	@ zero_extendqisi2
	cmp	r3, r4
	ldreq	r3, [r7, #0]
	streq	r0, [r3, r6, asl #2]
	addeq	r6, r6, #1
.L41:
	cmp	r6, r8
	bls	.L42
	ldr	r0, .L45+12
	bl	RemovePathFromFileName
	mov	r2, #536
	mov	r1, r0
	ldr	r0, .L45+28
	bl	Alert_Message_va
	b	.L43
.L42:
	bl	nm_LIGHTS_REPORT_DATA_get_previous_completed_record
.L40:
	cmp	r0, #0
	bne	.L44
.L43:
	ldr	r3, .L45
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L46:
	.align	2
.L45:
	.word	lights_report_completed_records_recursive_MUTEX
	.word	.LANCHOR5
	.word	495
	.word	.LC4
	.word	lights_preserves+16
	.word	671
	.word	.LANCHOR0
	.word	.LC6
.LFE11:
	.size	LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_lines, .-LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_lines
	.section	.text.LIGHTS_REPORT_draw_scroll_line,"ax",%progbits
	.align	2
	.global	LIGHTS_REPORT_draw_scroll_line
	.type	LIGHTS_REPORT_draw_scroll_line, %function
LIGHTS_REPORT_draw_scroll_line:
.LFB12:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L49+4
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, lr}
.LCFI7:
	ldr	r2, .L49+8
	sub	sp, sp, #24
.LCFI8:
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L49+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L49+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L48
	ldr	r4, [r3, r4, asl #2]
	mov	r3, #250
	ldrh	r2, [r4, #8]
	mov	r1, #16
	str	r3, [sp, #0]
	add	r0, sp, #8
	mov	r3, #150
	bl	GetDateStr
	mov	r2, #6
	mov	r1, r0
	ldr	r0, .L49+20
	bl	strlcpy
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r1, #16
	ldr	r2, [r4, #4]
	add	r0, sp, #8
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r2, #8
	mov	r1, r0
	ldr	r0, .L49+24
	bl	strlcpy
	flds	s13, [r4, #0]	@ int
	flds	s15, .L49
	ldr	r3, .L49+28
	fuitos	s14, s13
	flds	s13, [r4, #4]	@ int
	ldrh	r2, [r4, #10]
	fdivs	s14, s14, s15
	fsts	s14, [r3, #0]
	fuitos	s14, s13
	ldr	r3, .L49+32
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	ldr	r3, .L49+36
	str	r2, [r3, #0]
.L48:
	ldr	r3, .L49+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #24
	ldmfd	sp!, {r4, pc}
.L50:
	.align	2
.L49:
	.word	1114636288
	.word	lights_report_completed_records_recursive_MUTEX
	.word	.LC4
	.word	586
	.word	.LANCHOR5
	.word	GuiVar_RptDate
	.word	GuiVar_RptStartTime
	.word	GuiVar_RptScheduledMin
	.word	GuiVar_RptManualMin
	.word	GuiVar_RptCycles
.LFE12:
	.size	LIGHTS_REPORT_draw_scroll_line, .-LIGHTS_REPORT_draw_scroll_line
	.section	.text.nm_LIGHTS_REPORT_DATA_close_and_start_a_new_record,"ax",%progbits
	.align	2
	.global	nm_LIGHTS_REPORT_DATA_close_and_start_a_new_record
	.type	nm_LIGHTS_REPORT_DATA_close_and_start_a_new_record, %function
nm_LIGHTS_REPORT_DATA_close_and_start_a_new_record:
.LFB13:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L56
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI9:
	ldr	r5, .L56+4
	sub	sp, sp, #36
.LCFI10:
	mov	r4, r0
	ldrb	r7, [r0, #12]	@ zero_extendqisi2
	ldrb	r8, [r0, #13]	@ zero_extendqisi2
	ldr	r2, .L56+8
	ldr	r0, [r3, #0]
	mov	r6, r1
	ldr	r3, .L56+12
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldmia	r4, {r0, r1, r2, r3}
	ldr	ip, [r5, #4]
	add	ip, r5, ip, asl #4
	add	ip, ip, #60
	stmia	ip, {r0, r1, r2, r3}
.LBB14:
	add	r0, r5, #4
	bl	nm_LIGHTS_REPORT_DATA_inc_index
	ldr	r3, [r5, #4]
	cmp	r3, #0
	moveq	r2, #1
	streq	r2, [r5, #8]
	ldr	r2, [r5, #16]
	cmp	r3, r2
	bne	.L53
	ldr	r0, .L56+16
	bl	nm_LIGHTS_REPORT_DATA_inc_index
.L53:
	ldr	r2, [r5, #4]
	ldr	r3, [r5, #20]
	cmp	r2, r3
	bne	.L54
	ldr	r0, .L56+20
	bl	nm_LIGHTS_REPORT_DATA_inc_index
.L54:
.LBE14:
	ldr	r3, .L56
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	LIGHTS_REPORT_DATA_start_the_ci_timer_if_it_is_not_running
	mov	r0, #17
	mov	r1, #2
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.LBB15:
	mov	r2, #16
	mov	r0, r4
	mov	r1, #0
	bl	memset
.LBE15:
	ldrb	r3, [r6, #4]	@ zero_extendqisi2
	ldrb	r2, [r6, #5]	@ zero_extendqisi2
	strb	r7, [r4, #12]
	orr	r3, r3, r2, asl #8
	strh	r3, [r4, #8]	@ movhi
	ldr	r3, .L56+24
	strb	r8, [r4, #13]
	ldrsh	r3, [r3, #0]
	cmp	r3, #86
	bne	.L51
.LBB16:
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L56+28
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
.L51:
.LBE16:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L57:
	.align	2
.L56:
	.word	lights_report_completed_records_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC4
	.word	639
	.word	.LANCHOR0+16
	.word	.LANCHOR0+20
	.word	GuiLib_CurStructureNdx
	.word	FDTO_LIGHTS_REPORT_redraw_scrollbox
.LFE13:
	.size	nm_LIGHTS_REPORT_DATA_close_and_start_a_new_record, .-nm_LIGHTS_REPORT_DATA_close_and_start_a_new_record
	.section	.text.LIGHTS_REPORT_DATA_free_report_support,"ax",%progbits
	.align	2
	.global	LIGHTS_REPORT_DATA_free_report_support
	.type	LIGHTS_REPORT_DATA_free_report_support, %function
LIGHTS_REPORT_DATA_free_report_support:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI11:
	ldr	r4, .L60
	ldr	r5, .L60+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L60+8
	mov	r3, #768
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, #0]
	cmp	r0, #0
	beq	.L59
	ldr	r1, .L60+8
	mov	r2, #772
	bl	mem_free_debug
	mov	r3, #0
	str	r3, [r5, #0]
.L59:
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L61:
	.align	2
.L60:
	.word	lights_report_completed_records_recursive_MUTEX
	.word	.LANCHOR5
	.word	.LC4
.LFE14:
	.size	LIGHTS_REPORT_DATA_free_report_support, .-LIGHTS_REPORT_DATA_free_report_support
	.global	lights_report_data_revision_record_counts
	.global	lights_report_data_revision_record_sizes
	.global	LIGHTS_REPORT_DATA_FILENAME
	.global	lights_report_data_completed
	.section	.rodata.LIGHTS_REPORT_DATA_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	LIGHTS_REPORT_DATA_FILENAME, %object
	.size	LIGHTS_REPORT_DATA_FILENAME, 22
LIGHTS_REPORT_DATA_FILENAME:
	.ascii	"LIGHTS_REPORT_RECORDS\000"
	.section	.rodata.lights_report_data_revision_record_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	lights_report_data_revision_record_sizes, %object
	.size	lights_report_data_revision_record_sizes, 8
lights_report_data_revision_record_sizes:
	.word	16
	.word	16
	.section	.bss.lights_report_data_completed,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	lights_report_data_completed, %object
	.size	lights_report_data_completed, 10812
lights_report_data_completed:
	.space	10812
	.section	.bss.lights_report_data_ptrs,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	lights_report_data_ptrs, %object
	.size	lights_report_data_ptrs, 4
lights_report_data_ptrs:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"LIGHTS RPRT file unexpd update %u\000"
.LC1:
	.ascii	"LIGHTS RPRT file update : to revision %u from %u\000"
.LC2:
	.ascii	"LIGHTS RPRT updater error\000"
.LC3:
	.ascii	"\000"
.LC4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/lights_report_data.c\000"
.LC5:
	.ascii	"Timer NOT CREATED : %s, %u\000"
.LC6:
	.ascii	"REPORTS: why so many records? : %s, %u\000"
	.section	.rodata.lights_report_data_revision_record_counts,"a",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	lights_report_data_revision_record_counts, %object
	.size	lights_report_data_revision_record_counts, 8
lights_report_data_revision_record_counts:
	.word	672
	.word	672
	.section	.bss.lights_report_data_ci_timer,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	lights_report_data_ci_timer, %object
	.size	lights_report_data_ci_timer, 4
lights_report_data_ci_timer:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI5-.LFB6
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI6-.LFB11
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI7-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI9-.LFB13
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI11-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE22:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/lights_report_data.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x12a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF15
	.byte	0x1
	.4byte	.LASF16
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x49
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x1c4
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x144
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x17c
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.byte	0x4f
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x10c
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x1
	.byte	0xa2
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.byte	0xdb
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x100
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x115
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x135
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1e4
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST5
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x23f
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST6
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x270
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST7
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x2fa
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB11
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB12
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI8
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB13
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI10
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB14
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x74
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"lights_report_data_ci_timer_callback\000"
.LASF14:
	.ascii	"LIGHTS_REPORT_DATA_free_report_support\000"
.LASF7:
	.ascii	"init_file_lights_report_data\000"
.LASF9:
	.ascii	"LIGHTS_REPORT_DATA_start_the_ci_timer_if_it_is_not_"
	.ascii	"running\000"
.LASF16:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/lights_report_data.c\000"
.LASF3:
	.ascii	"nm_LIGHTS_REPORT_DATA_get_previous_completed_record"
	.ascii	"\000"
.LASF15:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF10:
	.ascii	"nm_LIGHTS_REPORT_DATA_inc_index\000"
.LASF12:
	.ascii	"LIGHTS_REPORT_draw_scroll_line\000"
.LASF6:
	.ascii	"nm_lights_report_data_updater\000"
.LASF4:
	.ascii	"nm_init_lights_report_data_records\000"
.LASF0:
	.ascii	"nm_init_lights_report_data_record\000"
.LASF2:
	.ascii	"nm_LIGHTS_REPORT_DATA_increment_next_avail_ptr\000"
.LASF13:
	.ascii	"nm_LIGHTS_REPORT_DATA_close_and_start_a_new_record\000"
.LASF1:
	.ascii	"nm_LIGHTS_REPORT_DATA_get_most_recently_completed_r"
	.ascii	"ecord\000"
.LASF8:
	.ascii	"save_file_lights_report_data\000"
.LASF11:
	.ascii	"LIGHTS_REPORT_DATA_fill_ptrs_and_return_how_many_li"
	.ascii	"nes\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
