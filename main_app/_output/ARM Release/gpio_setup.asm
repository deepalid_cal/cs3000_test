	.file	"gpio_setup.c"
	.text
.Ltext0:
	.section	.text.gpio_setup,"ax",%progbits
	.align	2
	.global	gpio_setup
	.type	gpio_setup, %function
gpio_setup:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	mov	r1, #0
	str	r1, [r3, #0]
	ldr	r2, [r3, #0]
	mvn	r0, #-16777216
	orr	r2, r2, #32
	str	r2, [r3, #0]
	sub	r3, r3, #180224
	mov	r2, #255
	str	r2, [r3, #292]
	mov	r2, #63
	str	r2, [r3, #80]
	mov	r2, #192
	str	r2, [r3, #84]
	mov	r2, #8
	str	r2, [r3, #68]
	mov	r2, #55
	str	r2, [r3, #72]
	mov	r2, #8388608
	str	r2, [r3, #104]
	str	r2, [r3, #112]
	str	r0, [r3, #308]
	str	r2, [r3, #304]
	mov	r2, #3
	str	r2, [r3, #40]
	mov	r2, #60
	str	r2, [r3, #44]
	mov	r2, #1610612736
	str	r2, [r3, #4]
	str	r2, [r3, #16]
	mov	r2, #100663296
	str	r2, [r3, #20]
	ldr	r2, .L2+4
	str	r2, [r3, #8]
	ldr	r2, .L2+8
	str	r2, [r3, #256]
	sub	r3, r3, #147456
	str	r1, [r3, #128]
	bx	lr
.L3:
	.align	2
.L2:
	.word	1074085888
	.word	14306162
	.word	5984
.LFE0:
	.size	gpio_setup, .-gpio_setup
	.section	.text.GPIO_SETUP_learn_and_set_board_hardware_id,"ax",%progbits
	.align	2
	.global	GPIO_SETUP_learn_and_set_board_hardware_id
	.type	GPIO_SETUP_learn_and_set_board_hardware_id, %function
GPIO_SETUP_learn_and_set_board_hardware_id:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L7
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r2, .L7+4
	ldr	r0, .L7+8
	ldr	r1, [r2, #64]
	tst	r1, #64
	movne	r1, #1
	strne	r1, [r3, #0]
	ldr	r2, [r2, #64]
	tst	r2, #128
	ldrne	r2, [r3, #0]
	addne	r2, r2, #2
	strne	r2, [r3, #0]
	ldr	r1, [r3, #0]
	b	Alert_Message_va
.L8:
	.align	2
.L7:
	.word	.LANCHOR0
	.word	1073905664
	.word	.LC0
.LFE1:
	.size	GPIO_SETUP_learn_and_set_board_hardware_id, .-GPIO_SETUP_learn_and_set_board_hardware_id
	.global	board_hardware_id
	.global	display_model_is
	.section	.bss.board_hardware_id,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	board_hardware_id, %object
	.size	board_hardware_id, 4
board_hardware_id:
	.space	4
	.section	.bss.display_model_is,"aw",%nobits
	.align	2
	.type	display_model_is, %object
	.size	display_model_is, 4
display_model_is:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Board Rev: %u\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/gpio_setup.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x47
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xf0
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/board_in"
	.ascii	"it/gpio_setup.c\000"
.LASF1:
	.ascii	"GPIO_SETUP_learn_and_set_board_hardware_id\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"gpio_setup\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
