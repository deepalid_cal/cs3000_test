	.file	"d_device_exchange.c"
	.text
.Ltext0:
	.section	.text.DEVICE_EXCHANGE_draw_dialog,"ax",%progbits
	.align	2
	.global	DEVICE_EXCHANGE_draw_dialog
	.type	DEVICE_EXCHANGE_draw_dialog, %function
DEVICE_EXCHANGE_draw_dialog:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	ldr	r0, .L2+4
	mov	r2, #0
	str	r2, [r3, #0]
	b	DIALOG_draw_ok_dialog
.L3:
	.align	2
.L2:
	.word	GuiVar_DeviceExchangeProgressBar
	.word	598
.LFE0:
	.size	DEVICE_EXCHANGE_draw_dialog, .-DEVICE_EXCHANGE_draw_dialog
	.section	.text.FDTO_DEVICE_EXCHANGE_update_dialog,"ax",%progbits
	.align	2
	.global	FDTO_DEVICE_EXCHANGE_update_dialog
	.type	FDTO_DEVICE_EXCHANGE_update_dialog, %function
FDTO_DEVICE_EXCHANGE_update_dialog:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L9
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	and	r2, r2, #3
	str	r2, [r3, #0]
	ldr	r3, .L9+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldr	r3, .L9+8
	beq	.L5
	ldr	r2, [r3, #0]
	cmp	r2, #199
	addls	r2, r2, #20
	bls	.L8
.L5:
	mov	r2, #200
.L8:
	str	r2, [r3, #0]
	b	FDTO_DIALOG_redraw_ok_dialog
.L10:
	.align	2
.L9:
	.word	GuiVar_SpinnerPos
	.word	GuiVar_DeviceExchangeSyncingRadios
	.word	GuiVar_DeviceExchangeProgressBar
.LFE1:
	.size	FDTO_DEVICE_EXCHANGE_update_dialog, .-FDTO_DEVICE_EXCHANGE_update_dialog
	.section	.text.FDTO_DEVICE_EXCHANGE_close_dialog,"ax",%progbits
	.align	2
	.global	FDTO_DEVICE_EXCHANGE_close_dialog
	.type	FDTO_DEVICE_EXCHANGE_close_dialog, %function
FDTO_DEVICE_EXCHANGE_close_dialog:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L24
	str	lr, [sp, #-4]!
.LCFI0:
	ldr	r3, [r3, #0]
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L12
.L16:
	.word	.L13
	.word	.L14
	.word	.L13
	.word	.L12
	.word	.L14
	.word	.L15
.L13:
	cmp	r0, #2
	beq	.L18
	cmp	r0, #67
	bne	.L12
.L18:
	ldr	r3, .L24+4
	mov	r2, #1
	str	r2, [r3, #0]
	bl	DIALOG_close_ok_dialog
	ldr	r3, .L24+8
	ldr	r2, .L24+12
	ldr	r3, [r3, #0]
	mov	r1, #36
	mla	r3, r1, r3, r2
	mov	r0, #67
	ldr	r3, [r3, #16]
	mov	r1, #0
	blx	r3
	ldr	pc, [sp], #4
.L15:
	cmp	r0, #2
	beq	.L21
	cmp	r0, #67
	bne	.L12
.L21:
	bl	good_key_beep
.L14:
	ldr	r3, .L24+4
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	lr, [sp], #4
	b	DIALOG_close_ok_dialog
.L12:
	ldr	lr, [sp], #4
	b	bad_key_beep
.L25:
	.align	2
.L24:
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	g_DIALOG_modal_result
	.word	screen_history_index
	.word	ScreenHistory
.LFE2:
	.size	FDTO_DEVICE_EXCHANGE_close_dialog, .-FDTO_DEVICE_EXCHANGE_close_dialog
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_device_exchange.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x58
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x1f
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x28
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x41
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"FDTO_DEVICE_EXCHANGE_close_dialog\000"
.LASF1:
	.ascii	"FDTO_DEVICE_EXCHANGE_update_dialog\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_device_exchange.c\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"DEVICE_EXCHANGE_draw_dialog\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
