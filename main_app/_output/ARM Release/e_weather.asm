	.file	"e_weather.c"
	.text
.Ltext0:
	.section	.text.FDTO_WEATHER_show_et_rain_or_wind_in_use_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_WEATHER_show_et_rain_or_wind_in_use_dropdown, %function
FDTO_WEATHER_show_et_rain_or_wind_in_use_dropdown:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
.LFE0:
	.size	FDTO_WEATHER_show_et_rain_or_wind_in_use_dropdown, .-FDTO_WEATHER_show_et_rain_or_wind_in_use_dropdown
	.section	.text.FDTO_WEATHER_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_WEATHER_draw_screen
	.type	FDTO_WEATHER_draw_screen, %function
FDTO_WEATHER_draw_screen:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L7
	str	lr, [sp, #-4]!
.LCFI0:
	ldrnesh	r1, [r3, #0]
	bne	.L6
	bl	WEATHER_copy_group_into_guivars
	mov	r1, #0
.L6:
	mov	r0, #73
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L8:
	.align	2
.L7:
	.word	GuiLib_ActiveCursorFieldNo
.LFE1:
	.size	FDTO_WEATHER_draw_screen, .-FDTO_WEATHER_draw_screen
	.global	__modsi3
	.section	.text.WEATHER_process_screen,"ax",%progbits
	.align	2
	.global	WEATHER_process_screen
	.type	WEATHER_process_screen, %function
WEATHER_process_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L114
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #44
.LCFI2:
	cmp	r3, #740
	mov	r4, r0
	mov	r5, r1
	bne	.L104
	ldr	r3, .L114+4
	ldr	r1, [r3, #0]
	bl	COMBO_BOX_key_press
	b	.L9
.L104:
	cmp	r0, #4
	beq	.L18
	bhi	.L21
	cmp	r0, #1
	beq	.L15
	bcc	.L14
	cmp	r0, #2
	beq	.L16
	cmp	r0, #3
	bne	.L13
	b	.L113
.L21:
	cmp	r0, #67
	beq	.L19
	bhi	.L22
	cmp	r0, #16
	beq	.L18
	cmp	r0, #20
	bne	.L13
	b	.L14
.L22:
	cmp	r0, #80
	beq	.L20
	cmp	r0, #84
	bne	.L13
	b	.L20
.L16:
	ldr	r3, .L114+8
	ldrsh	r2, [r3, #0]
	ldr	r3, .L114+4
	cmp	r2, #29
	ldrls	pc, [pc, r2, asl #2]
	b	.L23
.L54:
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
	.word	.L48
	.word	.L49
	.word	.L50
	.word	.L51
	.word	.L52
	.word	.L53
.L24:
	ldr	r2, .L114+12
	b	.L105
.L25:
	ldr	r2, .L114+16
	b	.L105
.L26:
	ldr	r2, .L114+20
	b	.L105
.L27:
	ldr	r2, .L114+24
	b	.L105
.L28:
	ldr	r2, .L114+28
	b	.L105
.L29:
	ldr	r2, .L114+32
	b	.L105
.L30:
	ldr	r2, .L114+36
	b	.L105
.L31:
	ldr	r2, .L114+40
	b	.L105
.L32:
	ldr	r2, .L114+44
	b	.L105
.L33:
	ldr	r2, .L114+48
	b	.L105
.L34:
	ldr	r2, .L114+52
	b	.L105
.L35:
	ldr	r2, .L114+56
	b	.L105
.L36:
	ldr	r2, .L114+60
	b	.L105
.L37:
	ldr	r2, .L114+64
	b	.L105
.L38:
	ldr	r2, .L114+68
	b	.L105
.L39:
	ldr	r2, .L114+72
	b	.L105
.L40:
	ldr	r2, .L114+76
	b	.L105
.L41:
	ldr	r2, .L114+80
	b	.L105
.L42:
	ldr	r2, .L114+84
	b	.L105
.L43:
	ldr	r2, .L114+88
	b	.L105
.L44:
	ldr	r2, .L114+92
	b	.L105
.L45:
	ldr	r2, .L114+96
	b	.L105
.L46:
	ldr	r2, .L114+100
	b	.L105
.L47:
	ldr	r2, .L114+104
	b	.L105
.L48:
	ldr	r2, .L114+108
	b	.L105
.L49:
	ldr	r2, .L114+112
	b	.L105
.L50:
	ldr	r2, .L114+116
	b	.L105
.L51:
	ldr	r2, .L114+120
	b	.L105
.L52:
	ldr	r2, .L114+124
	b	.L105
.L53:
	ldr	r2, .L114+128
.L105:
	str	r2, [r3, #0]
.L23:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L98
.LBB2:
	bl	good_key_beep
	ldr	r3, .L114+8
	ldrh	r0, [r3, #0]
	cmp	r0, #0
	moveq	r3, #46
	moveq	r4, #168
	beq	.L56
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	cmp	r0, #10
	moveq	r3, #46
	moveq	r4, #223
	beq	.L56
	cmp	r0, #20
	moveq	r3, #46
	ldreq	r4, .L114+132
	beq	.L56
	cmp	r0, #9
	movle	r4, #168
	ble	.L57
	cmp	r0, #19
	ldr	r4, .L114+132
	movle	r4, #223
.L57:
	mov	r1, #10
	bl	__modsi3
	mov	r3, r0, asl #16
	mov	r3, r3, asr #16
	add	r3, r3, r3, asl #3
	mov	r3, r3, asl #1
	add	r3, r3, #46
.L56:
	mov	r2, #3
	str	r2, [sp, #8]
	ldr	r2, .L114+136
	add	r0, sp, #8
	str	r2, [sp, #28]
	str	r4, [sp, #32]
	str	r3, [sp, #36]
	bl	Display_Post_Command
	b	.L9
.L20:
.LBE2:
	ldr	r3, .L114+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #29
	ldrls	pc, [pc, r3, asl #2]
	b	.L58
.L89:
	.word	.L59
	.word	.L60
	.word	.L61
	.word	.L62
	.word	.L63
	.word	.L64
	.word	.L65
	.word	.L66
	.word	.L67
	.word	.L68
	.word	.L69
	.word	.L70
	.word	.L71
	.word	.L72
	.word	.L73
	.word	.L74
	.word	.L75
	.word	.L76
	.word	.L77
	.word	.L78
	.word	.L79
	.word	.L80
	.word	.L81
	.word	.L82
	.word	.L83
	.word	.L84
	.word	.L85
	.word	.L86
	.word	.L87
	.word	.L88
.L59:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+12
	b	.L106
.L60:
	ldr	r1, .L114+16
	mov	r3, #1
	mov	r0, r4
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L106:
	mov	r2, #0
	bl	process_uns32
	b	.L90
.L61:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+20
	b	.L106
.L62:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+24
	b	.L106
.L63:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+28
	b	.L106
.L64:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+32
	b	.L106
.L65:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+36
	b	.L106
.L66:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+40
	b	.L106
.L67:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+44
	b	.L106
.L68:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+48
	b	.L106
.L69:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+52
	b	.L106
.L70:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+56
	b	.L106
.L71:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+60
	b	.L106
.L72:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+64
	b	.L106
.L73:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+68
	b	.L106
.L74:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+72
	b	.L106
.L75:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+76
	b	.L106
.L76:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+80
	b	.L106
.L77:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+84
	b	.L106
.L78:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+88
	b	.L106
.L79:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+92
	b	.L106
.L80:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+96
	b	.L106
.L81:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+100
	b	.L106
.L82:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+104
	b	.L106
.L83:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+108
	b	.L106
.L84:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+112
	b	.L106
.L85:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+116
	b	.L106
.L86:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+120
	b	.L106
.L87:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+124
	b	.L106
.L88:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L114+128
	b	.L106
.L58:
	bl	bad_key_beep
.L90:
	mov	r0, #0
	bl	Redraw_Screen
	b	.L9
.L18:
	ldr	r3, .L114+8
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	movs	r0, r0, asl #16
	beq	.L98
.L91:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L9
.L14:
	ldr	r3, .L114+8
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	bl	STATION_GROUP_get_num_groups_in_use
	sub	r0, r0, #1
	cmp	r4, r0
	beq	.L98
.L92:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L9
.L15:
	ldr	r3, .L114+8
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #9
	movls	r0, r0, asr #16
	addls	r0, r0, #19
	bls	.L112
	sub	r2, r3, #10
	mov	r2, r2, asl #16
	cmp	r2, #589824
	bls	.L111
.L94:
	sub	r3, r3, #20
	mov	r3, r3, asl #16
	cmp	r3, #589824
	bhi	.L98
.L111:
	mov	r0, r0, asr #16
	sub	r0, r0, #10
.L112:
	mov	r1, r4
	b	.L109
.L113:
	ldr	r3, .L114+8
	ldrh	r2, [r3, #0]
	mov	r0, r2, asl #16
	mov	r3, r0, lsr #16
	cmp	r3, #9
	bls	.L110
.L96:
	sub	r1, r3, #10
	mov	r1, r1, asl #16
	cmp	r1, #589824
	bhi	.L97
.L110:
	mov	r0, r0, asr #16
	add	r0, r0, #10
	b	.L108
.L97:
	sub	r3, r3, #20
	mov	r3, r3, asl #16
	cmp	r3, #589824
	bhi	.L98
	mov	r2, r2, asl #16
	mov	r0, r2, asr #16
	sub	r0, r0, #19
.L108:
	mov	r1, #1
.L109:
	bl	CURSOR_Select
	b	.L9
.L98:
	bl	bad_key_beep
	b	.L9
.L19:
	ldr	r3, .L114+140
	mov	r2, #4
	str	r2, [r3, #0]
	bl	WEATHER_extract_and_store_changes_from_GuiVars
.L13:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L9:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L115:
	.align	2
.L114:
	.word	GuiLib_CurStructureNdx
	.word	.LANCHOR0
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupSettingA_0
	.word	GuiVar_GroupSettingA_1
	.word	GuiVar_GroupSettingA_2
	.word	GuiVar_GroupSettingA_3
	.word	GuiVar_GroupSettingA_4
	.word	GuiVar_GroupSettingA_5
	.word	GuiVar_GroupSettingA_6
	.word	GuiVar_GroupSettingA_7
	.word	GuiVar_GroupSettingA_8
	.word	GuiVar_GroupSettingA_9
	.word	GuiVar_GroupSettingB_0
	.word	GuiVar_GroupSettingB_1
	.word	GuiVar_GroupSettingB_2
	.word	GuiVar_GroupSettingB_3
	.word	GuiVar_GroupSettingB_4
	.word	GuiVar_GroupSettingB_5
	.word	GuiVar_GroupSettingB_6
	.word	GuiVar_GroupSettingB_7
	.word	GuiVar_GroupSettingB_8
	.word	GuiVar_GroupSettingB_9
	.word	GuiVar_GroupSettingC_0
	.word	GuiVar_GroupSettingC_1
	.word	GuiVar_GroupSettingC_2
	.word	GuiVar_GroupSettingC_3
	.word	GuiVar_GroupSettingC_4
	.word	GuiVar_GroupSettingC_5
	.word	GuiVar_GroupSettingC_6
	.word	GuiVar_GroupSettingC_7
	.word	GuiVar_GroupSettingC_8
	.word	GuiVar_GroupSettingC_9
	.word	278
	.word	FDTO_WEATHER_show_et_rain_or_wind_in_use_dropdown
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	WEATHER_process_screen, .-WEATHER_process_screen
	.section	.bss.g_WEATHER_combo_box_guivar,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_WEATHER_combo_box_guivar, %object
	.size	g_WEATHER_combo_box_guivar, 4
g_WEATHER_combo_box_guivar:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_weather.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x58
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x47
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x4d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x70
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"WEATHER_process_screen\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_weather.c\000"
.LASF4:
	.ascii	"FDTO_WEATHER_show_et_rain_or_wind_in_use_dropdown\000"
.LASF0:
	.ascii	"FDTO_WEATHER_draw_screen\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
