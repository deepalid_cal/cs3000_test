	.file	"portISR.c"
	.text
.Ltext0:
	.section	.text.vPortISRStartFirstTask,"ax",%progbits
	.align	2
	.global	vPortISRStartFirstTask
	.type	vPortISRStartFirstTask, %function
vPortISRStartFirstTask:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LBB2:
@ 72 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	LDR		R0, =pxCurrentTCB								
	LDR		R0, [R0]										
	LDR		LR, [R0]										
	LDR		R0, =ulCriticalNesting							
	LDMFD	LR!, {R1}											
	STR		R1, [R0]										
	LDMFD	LR!, {R0}											
	MSR		SPSR, R0										
	LDMFD	LR, {R0-R14}^										
	NOP														
	LDR		LR, [LR, #+60]									
	SUBS	PC, LR, #4											
	
@ 0 "" 2
	ldr	r3, .L2
	ldr	r3, [r3, #0]
	ldr	r3, .L2+4
	ldr	r3, [r3, #0]
.LBE2:
	bx	lr
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	pxCurrentTCB
.LFE0:
	.size	vPortISRStartFirstTask, .-vPortISRStartFirstTask
	.section	.text.vPortYieldProcessor,"ax",%progbits
	.align	2
	.global	vPortYieldProcessor
	.type	vPortYieldProcessor, %function
vPortYieldProcessor:
.LFB1:
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
@ 89 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	ADD		LR, LR, #4
@ 0 "" 2
.LBB3:
@ 92 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	STMDB	SP!, {R0}											
	STMDB	SP,{SP}^											
	NOP														
	SUB	SP, SP, #4											
	LDMIA	SP!,{R0}											
	STMDB	R0!, {LR}											
	MOV	LR, R0												
	LDMIA	SP!, {R0}											
	STMDB	LR,{R0-LR}^											
	NOP														
	SUB	LR, LR, #60											
	MRS	R0, SPSR											
	STMDB	LR!, {R0}											
	LDR	R0, =ulCriticalNesting								
	LDR	R0, [R0]											
	STMDB	LR!, {R0}											
	LDR	R0, =pxCurrentTCB									
	LDR	R0, [R0]											
	STR	LR, [R0]											
	
@ 0 "" 2
	ldr	r2, .L5
	ldr	r3, [r2, #0]
	ldr	r3, .L5+4
	ldr	r1, [r3, #0]
.LBE3:
@ 95 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	bl			vTaskSwitchContext
@ 0 "" 2
.LBB4:
@ 98 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	LDR		R0, =pxCurrentTCB								
	LDR		R0, [R0]										
	LDR		LR, [R0]										
	LDR		R0, =ulCriticalNesting							
	LDMFD	LR!, {R1}											
	STR		R1, [R0]										
	LDMFD	LR!, {R0}											
	MSR		SPSR, R0										
	LDMFD	LR, {R0-R14}^										
	NOP														
	LDR		LR, [LR, #+60]									
	SUBS	PC, LR, #4											
	
@ 0 "" 2
	ldr	r2, [r2, #0]
	ldr	r3, [r3, #0]
.LBE4:
.L6:
	.align	2
.L5:
	.word	.LANCHOR0
	.word	pxCurrentTCB
.LFE1:
	.size	vPortYieldProcessor, .-vPortYieldProcessor
	.section	.text.vPreemptiveTick,"ax",%progbits
	.align	2
	.global	vPreemptiveTick
	.type	vPreemptiveTick, %function
vPreemptiveTick:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L8
	mov	r2, #255
	str	lr, [sp, #-4]!
.LCFI0:
	str	r2, [r3, #0]
	ldr	r3, .L8+4
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	bl	vTaskIncrementTick
	ldr	lr, [sp], #4
	b	vTaskSwitchContext
.L9:
	.align	2
.L8:
	.word	1073938432
	.word	.LANCHOR1
.LFE2:
	.size	vPreemptiveTick, .-vPreemptiveTick
	.section	.text.vPortEnterCritical,"ax",%progbits
	.align	2
	.global	vPortEnterCritical
	.type	vPortEnterCritical, %function
vPortEnterCritical:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
@ 211 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	STMDB	SP!, {R0}		
	MRS	R0, CPSR		
	ORR	R0, R0, #0xC0	
	MSR	CPSR, R0		
	LDMIA	SP!, {R0}			
@ 0 "" 2
	ldr	r3, .L11
	ldr	r2, [r3, #0]
	add	r2, r2, #1
	str	r2, [r3, #0]
	bx	lr
.L12:
	.align	2
.L11:
	.word	.LANCHOR0
.LFE3:
	.size	vPortEnterCritical, .-vPortEnterCritical
	.section	.text.vPortExitCritical,"ax",%progbits
	.align	2
	.global	vPortExitCritical
	.type	vPortExitCritical, %function
vPortExitCritical:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L15
	ldr	r2, [r3, #0]
	cmp	r2, #0
	bxeq	lr
	ldr	r2, [r3, #0]
	sub	r2, r2, #1
	str	r2, [r3, #0]
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bxne	lr
@ 230 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	STMDB	SP!, {R0}		
	MRS	R0, CPSR		
	BIC	R0, R0, #0xC0	
	MSR	CPSR, R0		
	LDMIA	SP!, {R0}			
@ 0 "" 2
	bx	lr
.L16:
	.align	2
.L15:
	.word	.LANCHOR0
.LFE4:
	.size	vPortExitCritical, .-vPortExitCritical
	.section	.text.vPortSaveVFPRegisters,"ax",%progbits
	.align	2
	.global	vPortSaveVFPRegisters
	.type	vPortSaveVFPRegisters, %function
vPortSaveVFPRegisters:
.LFB5:
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
@ 247 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	FSTD	D0, [ r0, #+0 * 8 ]         
	FSTD	D1, [ r0, #+1 * 8 ]         
	FSTD	D2, [ r0, #+2 * 8 ]         
	FSTD	D3, [ r0, #+3 * 8 ]         
	FSTD	D4, [ r0, #+4 * 8 ]         
	FSTD	D5, [ r0, #+5 * 8 ]         
	FSTD	D6, [ r0, #+6 * 8 ]         
	FSTD	D7, [ r0, #+7 * 8 ]         
	FSTD	D8, [ r0, #+8 * 8 ]         
	FSTD	D9, [ r0, #+9 * 8 ]         
	FSTD	D10, [ r0, #+10 * 8 ]       
	FSTD	D11, [ r0, #+11 * 8 ]       
	FSTD	D12, [ r0, #+12 * 8 ]       
	FSTD	D13, [ r0, #+13 * 8 ]       
	FSTD	D14, [ r0, #+14 * 8 ]       
	FSTD	D15, [ r0, #+15 * 8 ]       
	FMRX	R2, FPSCR					
	str    R2, [ r0, #+16 * 8 ]        
	BLX R14
@ 0 "" 2
.LFE5:
	.size	vPortSaveVFPRegisters, .-vPortSaveVFPRegisters
	.section	.text.vPortRestoreVFPRegisters,"ax",%progbits
	.align	2
	.global	vPortRestoreVFPRegisters
	.type	vPortRestoreVFPRegisters, %function
vPortRestoreVFPRegisters:
.LFB6:
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
@ 330 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c" 1
	LDR 	R2, [ r0, #+16 * 8 ]        
	FLDD	D0, [ r0, #+0 * 8 ]         
	FLDD	D1, [ r0, #+1 * 8 ]         
	FLDD	D2, [ r0, #+2 * 8 ]         
	FLDD	D3, [ r0, #+3 * 8 ]         
	FLDD	D4, [ r0, #+4 * 8 ]         
	FLDD	D5, [ r0, #+5 * 8 ]         
	FLDD	D6, [ r0, #+6 * 8 ]         
	FLDD	D7, [ r0, #+7 * 8 ]         
	FLDD	D8, [ r0, #+8 * 8 ]         
	FLDD	D9, [ r0, #+9 * 8 ]         
	FLDD	D10, [ r0, #+10 * 8 ]       
	FLDD	D11, [ r0, #+11 * 8 ]       
	FLDD	D12, [ r0, #+12 * 8 ]       
	FLDD	D13, [ r0, #+13 * 8 ]       
	FLDD	D14, [ r0, #+14 * 8 ]       
	FLDD	D15, [ r0, #+15 * 8 ]       
	FMXR 	FPSCR, R2					
	BLX R14
@ 0 "" 2
.LFE6:
	.size	vPortRestoreVFPRegisters, .-vPortRestoreVFPRegisters
	.global	my_tick_count
	.global	ulCriticalNesting
	.section	.bss.my_tick_count,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	my_tick_count, %object
	.size	my_tick_count, 4
my_tick_count:
	.space	4
	.section	.data.ulCriticalNesting,"aw",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	ulCriticalNesting, %object
	.size	ulCriticalNesting, 4
ulCriticalNesting:
	.word	9999
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/portISR.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xa5
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF7
	.byte	0x1
	.4byte	.LASF8
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x44
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x54
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x85
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xd1
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xdb
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xf3
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x146
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"vPortEnterCritical\000"
.LASF6:
	.ascii	"vPortRestoreVFPRegisters\000"
.LASF4:
	.ascii	"vPortExitCritical\000"
.LASF8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/portable/GCC/ARM9_LPC32xx/portISR.c\000"
.LASF1:
	.ascii	"vPortYieldProcessor\000"
.LASF2:
	.ascii	"vPreemptiveTick\000"
.LASF5:
	.ascii	"vPortSaveVFPRegisters\000"
.LASF0:
	.ascii	"vPortISRStartFirstTask\000"
.LASF7:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
