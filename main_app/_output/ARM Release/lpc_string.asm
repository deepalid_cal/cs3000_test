	.file	"lpc_string.c"
	.text
.Ltext0:
	.section	.text.str_upper_to_lower,"ax",%progbits
	.align	2
	.global	str_upper_to_lower
	.type	str_upper_to_lower, %function
str_upper_to_lower:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	b	.L7
.L4:
	sub	r2, r3, #65
	cmp	r2, #25
	addls	r3, r3, #32
	strlsb	r3, [r0, #-1]
.L7:
	ldrb	r3, [r0], #1	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L4
	bx	lr
.LFE0:
	.size	str_upper_to_lower, .-str_upper_to_lower
	.section	.text.str_lower_to_upper,"ax",%progbits
	.align	2
	.global	str_lower_to_upper
	.type	str_lower_to_upper, %function
str_lower_to_upper:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	b	.L14
.L11:
	sub	r2, r3, #97
	cmp	r2, #25
	subls	r3, r3, #32
	strlsb	r3, [r0, #-1]
.L14:
	ldrb	r3, [r0], #1	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L11
	bx	lr
.LFE1:
	.size	str_lower_to_upper, .-str_lower_to_upper
	.section	.text.str_copy,"ax",%progbits
	.align	2
	.global	str_copy
	.type	str_copy, %function
str_copy:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	b	.L16
.L17:
	add	r3, r3, #1
.L16:
	ldrb	r2, [r1, r3]	@ zero_extendqisi2
	cmp	r2, #0
	strb	r2, [r0, r3]
	bne	.L17
	bx	lr
.LFE2:
	.size	str_copy, .-str_copy
	.section	.text.str_ncopy,"ax",%progbits
	.align	2
	.global	str_ncopy
	.type	str_ncopy, %function
str_ncopy:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	sub	r2, r2, #1
	b	.L19
.L21:
	strb	ip, [r0, r3]
	add	r3, r3, #1
.L19:
	ldrb	ip, [r1, r3]	@ zero_extendqisi2
	cmp	ip, #0
	beq	.L20
	cmp	r3, r2
	blt	.L21
.L20:
	mov	r2, #0
	strb	r2, [r0, r3]
	bx	lr
.LFE3:
	.size	str_ncopy, .-str_ncopy
	.section	.text.str_size,"ax",%progbits
	.align	2
	.global	str_size
	.type	str_size, %function
str_size:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #0
	b	.L23
.L24:
	add	r3, r3, #1
.L23:
	ldrb	r2, [r0, r3]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L24
	mov	r0, r3
	bx	lr
.LFE4:
	.size	str_size, .-str_size
	.section	.text.str_ncmp,"ax",%progbits
	.align	2
	.global	str_ncmp
	.type	str_ncmp, %function
str_ncmp:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	mov	r3, #0
	b	.L26
.L28:
	ldrb	r4, [r1], #1	@ zero_extendqisi2
	ldrb	ip, [r0], #1	@ zero_extendqisi2
	cmp	r4, ip
	movne	r2, #0
	movne	r3, #1
	sub	r2, r2, #1
.L26:
	cmp	r2, #0
	bgt	.L28
	mov	r0, r3
	ldmfd	sp!, {r4, pc}
.LFE6:
	.size	str_ncmp, .-str_ncmp
	.section	.text.str_cmp,"ax",%progbits
	.align	2
	.global	str_cmp
	.type	str_cmp, %function
str_cmp:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	mov	r4, r0
	mov	r5, r1
	bl	str_size
	mov	r1, r5
	add	r2, r0, #1
	mov	r0, r4
	ldmfd	sp!, {r4, r5, lr}
	b	str_ncmp
.LFE5:
	.size	str_cmp, .-str_cmp
	.section	.text.val_to_hex_char,"ax",%progbits
	.align	2
	.global	val_to_hex_char
	.type	val_to_hex_char, %function
val_to_hex_char:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r0, r0, #15
	cmp	r0, #9
	addls	r0, r0, #48
	addhi	r0, r0, #87
	bx	lr
.LFE7:
	.size	val_to_hex_char, .-val_to_hex_char
	.section	.text.hex_char_to_val,"ax",%progbits
	.align	2
	.global	hex_char_to_val
	.type	hex_char_to_val, %function
hex_char_to_val:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r0, r0, #255
	sub	r3, r0, #48
	and	r2, r3, #255
	cmp	r2, #9
	bls	.L38
.L35:
	sub	r3, r0, #97
	cmp	r3, #5
	movhi	r0, #0
	movhi	r3, r0
	bhi	.L36
	sub	r3, r0, #87
.L38:
	mov	r0, #1
.L36:
	str	r3, [r1, #0]
	bx	lr
.LFE8:
	.size	hex_char_to_val, .-hex_char_to_val
	.section	.text.dec_char_to_val,"ax",%progbits
	.align	2
	.global	dec_char_to_val
	.type	dec_char_to_val, %function
dec_char_to_val:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r0, r0, #255
	sub	r0, r0, #48
	and	r3, r0, #255
	cmp	r3, #9
	movhi	r0, #0
	str	r0, [r1, #0]
	movhi	r0, #0
	movls	r0, #1
	bx	lr
.LFE9:
	.size	dec_char_to_val, .-dec_char_to_val
	.section	.text.str_makehex,"ax",%progbits
	.align	2
	.global	str_makehex
	.type	str_makehex, %function
str_makehex:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #48
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	strb	r3, [r0, #0]
	mov	r3, #120
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	strb	r3, [r0, #1]
	mov	sl, r2, asl #2
	mov	r8, r0
	mov	r7, r2
	b	.L43
.L44:
	mov	r0, r5, lsr sl
	and	r0, r0, #15
	bl	val_to_hex_char
	sub	r7, r7, #1
	strb	r0, [r8, #1]
.L43:
	cmp	r7, #0
	sub	sl, sl, #4
	add	r8, r8, #1
	bgt	.L44
	cmp	r6, #0
	addge	r4, r4, r6
	mov	r3, #0
	strb	r3, [r4, #2]
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.LFE10:
	.size	str_makehex, .-str_makehex
	.global	__udivsi3
	.section	.text.str_makedec,"ax",%progbits
	.align	2
	.global	str_makedec
	.type	str_makedec, %function
str_makedec:
.LFB11:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI3:
	subs	r5, r1, #0
	sub	sp, sp, #32
.LCFI4:
	moveq	r3, #48
	mov	r6, r0
	streqb	r3, [sp, #0]
	moveq	r4, #1
	movne	r4, #0
	movne	r7, #10
	bne	.L46
	b	.L47
.L51:
	mov	r5, r0
.L46:
	mov	r0, r5
	mov	r1, #10
	bl	__udivsi3
	mul	r3, r7, r0
	cmp	r0, #0
	rsb	r5, r3, r5
	add	r5, r5, #48
	strb	r5, [sp, r4]
	add	r4, r4, #1
	bne	.L51
.L47:
	sub	r3, r4, #1
	mov	r2, r6
	b	.L48
.L49:
	ldrb	r1, [sp, r3]	@ zero_extendqisi2
	sub	r3, r3, #1
	strb	r1, [r2], #1
.L48:
	cmn	r3, #1
	bne	.L49
	mov	r3, #0
	strb	r3, [r6, r4]
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.LFE11:
	.size	str_makedec, .-str_makedec
	.section	.text.str_hex_to_val,"ax",%progbits
	.align	2
	.global	str_hex_to_val
	.type	str_hex_to_val, %function
str_hex_to_val:
.LFB12:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, sl, lr}
.LCFI5:
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	mov	r8, r0
	cmp	r3, #48
	mov	sl, r1
	bne	.L57
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	cmp	r3, #120
	bne	.L57
	bl	str_size
	mov	r5, #0
	mov	r4, #1
	mov	r7, r5
	mov	r6, r0
	b	.L54
.L55:
	ldrb	r0, [r8, r6]	@ zero_extendqisi2
	mov	r1, sp
	bl	hex_char_to_val
	ldr	r3, [sp, #0]
	add	r5, r5, r3, asl r7
	add	r7, r7, #4
	and	r4, r4, r0
.L54:
	sub	r6, r6, #1
	cmp	r6, #1
	bgt	.L55
	b	.L53
.L57:
	mov	r4, #0
	mov	r5, r4
.L53:
	str	r5, [sl, #0]
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, pc}
.LFE12:
	.size	str_hex_to_val, .-str_hex_to_val
	.section	.text.str_dec_to_val,"ax",%progbits
	.align	2
	.global	str_dec_to_val
	.type	str_dec_to_val, %function
str_dec_to_val:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI6:
	mov	r4, r0
	mov	r5, r1
	bl	str_size
	ldrb	ip, [r4, #0]	@ zero_extendqisi2
	mov	r7, #10
	sub	r3, ip, #45
	rsbs	ip, r3, #0
	adc	ip, ip, r3
.LBB4:
	mov	r3, #0
.LBE4:
	sub	r2, r0, #1
.LBB5:
	mov	r0, #1
	mov	r1, r0
	b	.L59
.L61:
.LBE5:
	ldrb	r6, [r4, r2]	@ zero_extendqisi2
	sub	r2, r2, #1
.LBB6:
	sub	r6, r6, #48
	and	r8, r6, #255
	cmp	r8, #9
	movhi	r6, #0
.LBE6:
	mla	r3, r1, r6, r3
	mul	r1, r7, r1
	movhi	r0, #0
	andls	r0, r0, #1
.L59:
	cmp	r2, ip
	bge	.L61
	cmp	ip, #1
	rsbeq	r3, r3, #0
	str	r3, [r5, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.LFE13:
	.size	str_dec_to_val, .-str_dec_to_val
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI0-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI1-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI2-.LFB10
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI3-.LFB11
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI5-.LFB12
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI6-.LFB13
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE26:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc_string.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x13b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF13
	.byte	0x1
	.4byte	.LASF14
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x179
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x2d
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x50
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x74
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x9d
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xc2
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xfe
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xe3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x129
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x14e
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	0x21
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1a1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x1cb
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x204
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST4
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x237
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB6
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB5
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB10
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB11
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI4
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB12
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB13
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x84
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"str_ncmp\000"
.LASF11:
	.ascii	"str_hex_to_val\000"
.LASF15:
	.ascii	"dec_char_to_val\000"
.LASF9:
	.ascii	"str_makehex\000"
.LASF7:
	.ascii	"val_to_hex_char\000"
.LASF2:
	.ascii	"str_copy\000"
.LASF10:
	.ascii	"str_makedec\000"
.LASF1:
	.ascii	"str_lower_to_upper\000"
.LASF12:
	.ascii	"str_dec_to_val\000"
.LASF13:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"str_upper_to_lower\000"
.LASF3:
	.ascii	"str_ncopy\000"
.LASF8:
	.ascii	"hex_char_to_val\000"
.LASF4:
	.ascii	"str_size\000"
.LASF6:
	.ascii	"str_cmp\000"
.LASF14:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc_string.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
