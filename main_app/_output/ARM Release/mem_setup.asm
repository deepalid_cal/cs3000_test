	.file	"mem_setup.c"
	.text
.Ltext0:
	.section	.text.setup_CS1_SRAM_R1LV0416DSB_timing,"ax",%progbits
	.align	2
	.global	setup_CS1_SRAM_R1LV0416DSB_timing
	.type	setup_CS1_SRAM_R1LV0416DSB_timing, %function
setup_CS1_SRAM_R1LV0416DSB_timing:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	mov	r2, #129
	str	r2, [r3, #544]
	mov	r2, #0
	str	r2, [r3, #548]
	str	r2, [r3, #552]
	mov	r2, #5
	str	r2, [r3, #556]
	str	r2, [r3, #560]
	mov	r2, #4
	str	r2, [r3, #564]
	mov	r2, #2
	str	r2, [r3, #568]
	bx	lr
.L3:
	.align	2
.L2:
	.word	822607872
.LFE0:
	.size	setup_CS1_SRAM_R1LV0416DSB_timing, .-setup_CS1_SRAM_R1LV0416DSB_timing
	.section	.text.setup_SDRAM_timing,"ax",%progbits
	.align	2
	.global	setup_SDRAM_timing
	.type	setup_SDRAM_timing, %function
setup_SDRAM_timing:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L5
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r4, .L5+4
	mov	r5, #1
	mov	r6, #0
	str	r5, [r3, #20]
	str	r6, [r3, #104]
	ldr	r3, .L5+8
	str	r5, [r4, #0]
	mov	r0, #33
	str	r6, [r4, #8]
	str	r3, [r4, #36]
	bl	clkpwr_get_clock_rate
	mov	r1, r6
	bl	sdr_sdram_setup
	mov	r3, #32
	str	r5, [r4, #1024]
	str	r5, [r4, #1088]
	str	r5, [r4, #1120]
	str	r5, [r4, #1152]
	str	r3, [r4, #1032]
	str	r3, [r4, #1096]
	str	r3, [r4, #1128]
	str	r3, [r4, #1160]
	ldmfd	sp!, {r4, r5, r6, pc}
.L6:
	.align	2
.L5:
	.word	1073758208
	.word	822607872
	.word	2047
.LFE1:
	.size	setup_SDRAM_timing, .-setup_SDRAM_timing
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/mem_setup.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x45
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x2f
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x49
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"setup_SDRAM_timing\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/board_in"
	.ascii	"it/mem_setup.c\000"
.LASF0:
	.ascii	"setup_CS1_SRAM_R1LV0416DSB_timing\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
