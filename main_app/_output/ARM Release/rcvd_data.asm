	.file	"rcvd_data.c"
	.text
.Ltext0:
	.section	.text.xfer_bytes_to_another_port.isra.0,"ax",%progbits
	.align	2
	.type	xfer_bytes_to_another_port.isra.0, %function
xfer_bytes_to_another_port.isra.0:
.LFB9:
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #4096
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI0:
	ldr	sl, [r0, r3]
	add	r3, r3, #44
	ldrh	r5, [r0, r3]
	mov	sl, sl, asl #16
	mov	sl, sl, lsr #16
.LBB6:
	cmp	r5, sl
.LBE6:
	sub	sp, sp, #72
.LCFI1:
	mov	r6, r0
	mov	r8, r1
.LBB7:
	beq	.L1
	rsb	r7, r5, sl
	addcs	r7, r7, #4096
	mov	r7, r7, asl #16
	mov	r7, r7, lsr #16
.LBE7:
	cmp	r7, #0
	beq	.L1
	ldr	r3, .L13
	mov	r2, #24
	mla	r3, r2, r1, r3
	ldr	r3, [r3, #8]
	cmp	r3, #23
	bls	.L5
	ldr	r3, .L13+4
	add	r0, sp, #8
	ldr	r1, .L13+8
	ldr	r2, [r3, r8, asl #2]
	bl	sprintf
	add	r0, sp, #8
	bl	Alert_Message
	b	.L1
.L5:
	ldr	r2, .L13+12
	mov	r0, r7
	ldr	r1, .L13+16
	bl	mem_malloc_debug
	mov	r2, #0
	mov	r4, r0
	mov	r3, r0
.L7:
	ldrb	r1, [r6, r5]	@ zero_extendqisi2
	add	r5, r5, #1
	mov	r5, r5, asl #16
	cmp	r5, #268435456
	add	r2, r2, #1
	movne	r5, r5, lsr #16
	moveq	r5, #0
	mov	r2, r2, asl #16
	cmp	r5, sl
	strb	r1, [r3], #1
	mov	r2, r2, lsr #16
	bne	.L7
	cmp	r2, r7
	bne	.L8
	ldr	r3, .L13+20
	mov	r1, #2
	strh	r5, [r6, r3]	@ movhi
	mov	r3, #1
	stmia	sp, {r1, r3}
	mov	r0, r8
	mov	r1, r4
	mov	r3, #0
	bl	AddCopyOfBlockToXmitList
	b	.L9
.L8:
	ldr	r0, .L13+24
	bl	Alert_Message
.L9:
	mov	r0, r4
	ldr	r1, .L13+16
	ldr	r2, .L13+28
	bl	mem_free_debug
.L1:
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L14:
	.align	2
.L13:
	.word	xmit_cntrl
	.word	port_names
	.word	.LC0
	.word	873
	.word	.LC1
	.word	4140
	.word	.LC2
	.word	918
.LFE9:
	.size	xfer_bytes_to_another_port.isra.0, .-xfer_bytes_to_another_port.isra.0
	.section	.text.__test_packet_echo,"ax",%progbits
	.align	2
	.global	__test_packet_echo
	.type	__test_packet_echo, %function
__test_packet_echo:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	add	sl, r2, #8
	mov	r6, r2
	mov	r7, r0
	mov	r5, r1
	mov	r2, #50
	ldr	r1, .L18
	mov	r0, sl
	bl	mem_malloc_debug
	ldr	r3, .L18+4
	mov	r1, r5
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	strb	r2, [r0, #0]
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	add	r8, r0, #4
	strb	r2, [r0, #1]
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	strb	r2, [r0, #2]
	strb	r3, [r0, #3]
	mov	r2, r6
	mov	r4, r0
	mov	r0, r8
	bl	memcpy
	ldr	r3, .L18+8
	add	r2, r8, r6
	ldrb	r1, [r3, #3]	@ zero_extendqisi2
	cmp	r7, #1
	strb	r1, [r8, r6]
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	moveq	r0, r7
	strb	r1, [r2, #1]
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	strb	r1, [r2, #2]
	strb	r3, [r2, #3]
	beq	.L16
	adds	r0, r7, #0
	movne	r0, #1
.L16:
	mov	r2, #1
	mov	r3, #0
	str	r2, [sp, #4]
	mov	r1, r4
	mov	r2, sl
	str	r3, [sp, #0]
	bl	AddCopyOfBlockToXmitList
	mov	r0, r4
	ldr	r1, .L18
	mov	r2, #96
	bl	mem_free_debug
	ldr	r1, .L18
	mov	r0, r5
	mov	r2, #98
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	mem_free_debug
.L19:
	.align	2
.L18:
	.word	.LC1
	.word	preamble
	.word	postamble
.LFE0:
	.size	__test_packet_echo, .-__test_packet_echo
	.section	.text.RCVD_DATA_enable_hunting_mode,"ax",%progbits
	.align	2
	.global	RCVD_DATA_enable_hunting_mode
	.type	RCVD_DATA_enable_hunting_mode, %function
RCVD_DATA_enable_hunting_mode:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI3:
	ldr	r6, .L28
	mov	r5, r0
	mul	r6, r5, r6
	ldr	r7, .L28+4
	mov	r4, r1
	mov	r8, r2
	bl	vPortEnterCritical
	add	r0, r6, #28
	ldr	r2, .L28+8
	add	r0, r7, r0
	mov	r1, #0
	bl	memset
	cmp	r4, #200
	add	r6, r7, r6
	moveq	r2, #1
	ldreq	r3, .L28+12
	beq	.L26
	cmp	r4, #300
	bne	.L23
	ldr	r3, .L28+16
	mov	r2, #1
.L26:
	str	r2, [r6, r3]
	b	.L22
.L23:
	cmp	r4, #400
	moveq	r2, #1
	ldreq	r3, .L28+20
	beq	.L26
	cmp	r4, #500
	ldreq	r2, .L28+24
	ldrne	r2, .L28+28
	mov	r3, #1
	str	r3, [r6, r2]
.L22:
	ldr	r3, .L28
	mla	r7, r3, r5, r7
	sub	r3, r3, #132
	str	r8, [r7, r3]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	vPortExitCritical
.L29:
	.align	2
.L28:
	.word	4280
	.word	SerDrvrVars_s
	.word	4212
	.word	4132
	.word	4136
	.word	4140
	.word	4144
	.word	4128
.LFE1:
	.size	RCVD_DATA_enable_hunting_mode, .-RCVD_DATA_enable_hunting_mode
	.section	.text.ring_buffer_analysis_task,"ax",%progbits
	.align	2
	.global	ring_buffer_analysis_task
	.type	ring_buffer_analysis_task, %function
ring_buffer_analysis_task:
.LFB8:
	@ args = 0, pretend = 0, frame = 180
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L91
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI4:
	ldr	r6, [r0, #24]
	rsb	r3, r3, r0
	sub	sp, sp, #180
.LCFI5:
	mov	r3, r3, lsr #5
	cmp	r6, #4
	str	r3, [sp, #8]
	bls	.L31
	ldr	r0, .L91+4
	bl	Alert_Message
.L32:
	b	.L32
.L31:
	ldr	r3, .L91+8
	ldr	r0, [r3, r6, asl #2]
	cmp	r0, #0
	bne	.L33
	ldr	r0, .L91+12
	bl	Alert_Message
.L34:
	b	.L34
.L33:
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	bl	xQueueGenericReceive
	cmp	r0, #1
	beq	.L35
	ldr	r0, .L91+16
	bl	Alert_Message
.L35:
	ldr	r9, .L91+20
	ldr	r7, .L91+24
	mul	r9, r6, r9
	mov	r1, #0
	add	r0, r9, #4224
	add	r0, r0, #16
	mov	r2, #36
	add	r0, r7, r0
	bl	memset
.LBB17:
.LBB18:
	add	r5, r7, r9
.LBE18:
.LBE17:
	mov	r0, r6
	mov	r1, #100
	mov	r2, #0
	bl	RCVD_DATA_enable_hunting_mode
	add	r4, r9, #28
.LBB24:
	add	r0, r5, #4096
	add	r0, r0, #40
.LBE24:
	add	r4, r4, r7
.LBB25:
	str	r0, [sp, #4]
.L75:
.LBE25:
	ldr	r3, .L91+8
	mov	r1, #0
	ldr	r0, [r3, r6, asl #2]
	mov	r2, #100
	mov	r3, r1
	bl	xQueueGenericReceive
	cmp	r0, #1
	bne	.L36
	ldr	r3, .L91+28
	ldr	r2, [r4, r3]
	cmp	r2, #0
	beq	.L37
	mov	r2, #0
	str	r2, [r4, r3]
	ldr	r3, .L91+32
	add	r0, sp, #12
	mov	r1, #64
	ldr	r2, .L91+36
	ldr	r3, [r3, r6, asl #2]
	bl	snprintf
	add	r0, sp, #12
	bl	Alert_Message
.L37:
	ldr	r3, .L91+40
	ldr	r2, [r4, r3]
	cmp	r2, #1
	bne	.L38
	mov	r2, #0
	str	r2, [r4, r3]
	ldr	r3, .L91+32
	add	r0, sp, #12
	mov	r1, #64
	ldr	r2, .L91+44
	ldr	r3, [r3, r6, asl #2]
	bl	snprintf
	add	r0, sp, #12
	bl	Alert_Message
.L38:
	ldr	r3, .L91+48
	ldr	r2, [r4, r3]
	cmp	r2, #1
	bne	.L39
	mov	r2, #0
	str	r2, [r4, r3]
	ldr	r3, .L91+32
	add	r0, sp, #12
	mov	r1, #64
	ldr	r2, .L91+52
	ldr	r3, [r3, r6, asl #2]
	bl	snprintf
	add	r0, sp, #12
	bl	Alert_Message
.L39:
	ldr	r3, .L91+56
	ldr	r2, [r4, r3]
	cmp	r2, #1
	bne	.L40
	mov	r2, #0
	str	r2, [r4, r3]
	ldr	r3, .L91+32
	add	r0, sp, #12
	mov	r1, #64
	ldr	r2, .L91+60
	ldr	r3, [r3, r6, asl #2]
	bl	snprintf
	add	r0, sp, #12
	bl	Alert_Message
.L40:
	ldr	r3, .L91+64
	ldr	r3, [r4, r3]
	cmp	r3, #0
	beq	.L41
.LBB26:
.LBB27:
	ldr	sl, .L91+68
	mov	fp, r9
	mov	r9, r5
.L83:
	ldr	r2, .L91+72
	mov	r0, #4096
	ldrh	r5, [r4, r2]
	ldr	r3, [r4, r0]
	cmp	r5, r3
	beq	.L88
	add	r1, r5, #1
	mov	r1, r1, asl #16
	ldr	r3, [r4, r0]
	cmp	r1, #268435456
	movne	r1, r1, lsr #16
	moveq	r1, #0
	cmp	r1, r3
	beq	.L88
	add	r2, r1, #1
	mov	r2, r2, asl #16
	ldr	r3, [r4, r0]
	cmp	r2, #268435456
	movne	r2, r2, lsr #16
	moveq	r2, #0
	cmp	r2, r3
	beq	.L88
	add	r3, r2, #1
	mov	r3, r3, asl #16
	ldr	r0, [r4, r0]
	cmp	r3, #268435456
	movne	r3, r3, lsr #16
	moveq	r3, #0
	cmp	r3, r0
	beq	.L88
	ldrb	ip, [r4, r5]	@ zero_extendqisi2
	ldrb	r0, [sl, #3]	@ zero_extendqisi2
	cmp	ip, r0
	bne	.L45
	ldrb	ip, [r4, r1]	@ zero_extendqisi2
	ldrb	r0, [sl, #2]	@ zero_extendqisi2
	cmp	ip, r0
	bne	.L45
	ldrb	ip, [r4, r2]	@ zero_extendqisi2
	ldrb	r0, [sl, #1]	@ zero_extendqisi2
	cmp	ip, r0
	bne	.L45
	ldrb	ip, [r4, r3]	@ zero_extendqisi2
	ldrb	r0, [sl, #0]	@ zero_extendqisi2
	cmp	ip, r0
	bne	.L45
	ldr	r0, .L91+76
	add	lr, r3, #1
	strh	r5, [r4, r0]	@ movhi
	add	r0, r0, #2
	strh	r1, [r4, r0]	@ movhi
	add	r0, r0, #2
	strh	r2, [r4, r0]	@ movhi
	mov	lr, lr, asl #16
	add	r0, r0, #2
	strh	r3, [r4, r0]	@ movhi
	mov	ip, #0
	add	r0, r0, #4
	mov	lr, lr, lsr #16
	strh	ip, [r4, r0]	@ movhi
	cmp	lr, #4096
	sub	r0, r0, #2
	strh	lr, [r4, r0]	@ movhi
	streqh	ip, [r4, r0]	@ movhi
.L45:
	ldr	r0, .L91+80
	ldrb	lr, [r4, r5]	@ zero_extendqisi2
	ldrb	ip, [r0, #3]	@ zero_extendqisi2
	cmp	lr, ip
	bne	.L46
	ldrb	ip, [r4, r1]	@ zero_extendqisi2
	ldrb	r1, [r0, #2]	@ zero_extendqisi2
	cmp	ip, r1
	bne	.L46
	ldrb	r1, [r4, r2]	@ zero_extendqisi2
	ldrb	r2, [r0, #1]	@ zero_extendqisi2
	cmp	r1, r2
	bne	.L46
	ldrb	r2, [r4, r3]	@ zero_extendqisi2
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L46
	ldr	r2, .L91+76
	ldrh	r3, [r4, r2]
	ldrb	r2, [r4, r3]	@ zero_extendqisi2
	ldrb	r3, [sl, #3]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L47
	ldr	r3, .L91+84
	ldrh	r3, [r4, r3]
	ldrb	r2, [r4, r3]	@ zero_extendqisi2
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L47
	ldr	r3, .L91+88
	ldrh	r3, [r4, r3]
	ldrb	r2, [r4, r3]	@ zero_extendqisi2
	ldrb	r3, [sl, #1]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L47
	ldr	r3, .L91+92
	ldrh	r3, [r4, r3]
	ldrb	r2, [r4, r3]	@ zero_extendqisi2
	ldrb	r3, [sl, #0]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L47
	ldr	r3, .L91+96
	ldrh	r8, [r4, r3]
	sub	r8, r8, #4
	mov	r8, r8, asl #16
	mov	r8, r8, lsr #16
	cmp	r8, #2080
	strh	r8, [r4, r3]	@ movhi
	bls	.L48
	mov	r0, r6
	bl	Alert_packet_greater_than_512
	b	.L47
.L48:
	ldr	r1, .L91+100
	ldr	r2, .L91+104
	mov	r0, r8
	bl	mem_malloc_debug
	ldr	r3, .L91+108
	mov	r2, #0
	ldrh	r3, [r4, r3]
	mov	r1, r0
.L50:
	ldrb	ip, [r4, r3]	@ zero_extendqisi2
	add	r3, r3, #1
	mov	r3, r3, asl #16
	cmp	r3, #268435456
	movne	r3, r3, lsr #16
	moveq	r3, #0
	add	r2, r2, #1
	mov	r2, r2, asl #16
	cmp	r3, r5
	strb	ip, [r0], #1
	mov	r2, r2, lsr #16
	bne	.L50
	ldr	r3, .L91+96
	ldrh	r3, [r4, r3]
	cmp	r3, r2
	beq	.L51
	ldr	r0, .L91+112
	bl	Alert_Message
	b	.L47
.L51:
	mov	r0, r6
	mov	r2, r8
	bl	Route_Incoming_Packet
.L47:
	ldr	r3, .L91+76
	strh	r5, [r4, r3]	@ movhi
	add	r3, r3, #2
	strh	r5, [r4, r3]	@ movhi
	add	r3, r3, #2
	strh	r5, [r4, r3]	@ movhi
	add	r3, r3, #2
	strh	r5, [r4, r3]	@ movhi
.L46:
	ldr	r0, .L91+72
	ldrh	r3, [r4, r0]
	add	r3, r3, #1
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, #4096
	strh	r3, [r4, r0]	@ movhi
	moveq	r3, #0
	streqh	r3, [r4, r0]	@ movhi
	ldr	r3, .L91+96
	ldrh	r2, [r4, r3]
	add	r2, r2, #1
	strh	r2, [r4, r3]	@ movhi
	b	.L83
.L88:
	mov	r5, r9
	mov	r9, fp
.L41:
.LBE27:
.LBE26:
	ldr	r3, .L91+116
	ldr	r3, [r4, r3]
	cmp	r3, #0
	beq	.L54
.LBB29:
	add	r3, r5, #4160
	add	r3, r3, #36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L54
	add	r3, r5, #4160
	add	r3, r3, #44
	ldr	r3, [r3, #0]
	cmp	r3, #99
	addls	sl, r9, #27
	addls	sl, sl, r7
.LBE29:
.LBB30:
.LBB28:
	movls	r8, #0
.LBE28:
.LBE30:
.LBB31:
	bls	.L55
	b	.L54
.L60:
	add	r3, r5, #4160
	ldr	r1, [r3, #36]
	ldrb	r2, [sl, #1]!	@ zero_extendqisi2
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L56
	add	r0, r8, r9
	add	r3, r5, #4160
	add	r0, r0, #28
	add	r0, r7, r0
	ldr	r2, [r3, #40]
	bl	strncmp
	cmp	r0, #0
	bne	.L56
	add	r3, r5, #4096
	add	r3, r3, #52
	ldr	r3, [r3, #0]
	ldr	r2, [sp, #4]
	cmp	r3, #100
	str	r0, [r2, #0]
	bne	.L57
	mov	r0, #2304
	bl	COMM_MNGR_post_event
	b	.L54
.L57:
	cmp	r3, #200
	bne	.L58
	mov	r0, #122
	bl	CONTROLLER_INITIATED_post_event
	b	.L54
.L58:
	cmp	r3, #300
	bne	.L59
	ldr	r0, .L91+120
	bl	CODE_DISTRIBUTION_post_event
	b	.L54
.L59:
	ldr	r0, .L91+124
	bl	Alert_Message
	b	.L54
.L56:
	add	r8, r8, #1
.L55:
	add	r3, r5, #4160
	add	r3, r3, #44
	ldr	r3, [r3, #0]
	cmp	r8, r3
	bls	.L60
.L54:
.LBE31:
	ldr	r3, .L91+128
	ldr	r3, [r4, r3]
	cmp	r3, #0
	beq	.L61
.LBB32:
.LBB33:
	add	r0, sp, #160
	ldr	r1, .L91+132
	mov	r2, #3
	bl	memcpy
	add	r3, r5, #4160
	add	r3, r3, #48
	ldr	r3, [r3, #0]
	add	r0, r9, #28
	cmp	r3, #0
	addeq	r8, r7, r0
	beq	.L63
	add	r0, r7, r0
	add	r1, sp, #160
	mov	r2, #2
	bl	strncmp
	cmp	r0, #0
	addeq	r8, r9, #30
	addeq	r8, r8, r7
	beq	.L63
	b	.L61
.L90:
	add	r0, sp, #76
	mov	r2, #40
	bl	memset
	mov	r3, #4864
	add	r0, sp, #76
.LBE33:
	str	sl, [sp, #92]
	str	fp, [sp, #96]
.LBB34:
	str	r3, [sp, #76]
	bl	COMM_MNGR_post_event_with_details
	b	.L64
.L84:
	cmp	r3, #300
	bne	.L65
	add	r0, sp, #116
	mov	r2, #24
	bl	memset
	ldr	r3, .L91+136
	add	r0, sp, #116
.LBE34:
	str	sl, [sp, #124]
	str	fp, [sp, #128]
.LBB35:
	str	r3, [sp, #116]
	bl	CODE_DISTRIBUTION_post_event_with_details
	b	.L64
.L65:
	cmp	r3, #200
	bne	.L66
	add	r0, sp, #140
	mov	r2, #20
	bl	memset
	mov	r3, #122
	add	r0, sp, #140
.LBE35:
	str	sl, [sp, #152]
	str	fp, [sp, #156]
.LBB36:
	str	r3, [sp, #140]
	bl	CONTROLLER_INITIATED_post_event_with_details
	b	.L64
.L66:
	ldr	r0, .L91+140
	bl	Alert_Message
.L64:
	add	r3, r5, #4096
	mov	r2, #0
	str	r2, [r3, #44]
.L61:
.LBE36:
.LBE32:
	ldr	r3, .L91+144
	ldr	r3, [r4, r3]
	cmp	r3, #0
	beq	.L67
.LBB38:
.LBB20:
	add	r3, r5, #4160
	ldr	r1, [r3, #56]
	cmp	r1, #0
	beq	.L67
	add	r8, r5, #4160
	add	r8, r8, #52
	ldr	r0, [r8, #0]
	add	r0, r9, r0
	add	r0, r0, #28
	add	r0, r7, r0
	bl	strstr
	add	r2, r5, #4160
	add	r2, r2, #60
	cmp	r0, #0
	beq	.L68
	ldr	r3, [r2, #0]
	add	fp, r9, #28
	add	fp, fp, r7
	add	r3, r0, r3
	cmp	r3, fp
	bls	.L67
	rsb	r3, fp, r3
	add	sl, r3, #1
	ldr	r1, .L91+100
	ldr	r2, .L91+148
	mov	r0, sl
	str	r3, [sp, #0]
	bl	mem_malloc_debug
	ldr	r3, [sp, #0]
	mov	r1, fp
	mov	r2, r3
	mov	r8, r0
	bl	memcpy
	ldr	r3, [sp, #0]
	mov	r1, #0
	strb	r1, [r8, r3]
	add	r3, r5, #4096
	add	r3, r3, #52
	ldr	r3, [r3, #0]
	cmp	r3, #100
	bne	.L69
	add	r0, sp, #76
	mov	r2, #40
	bl	memset
	mov	r3, #4864
	add	r0, sp, #76
.LBE20:
	str	r8, [sp, #92]
	str	sl, [sp, #96]
.LBB21:
	str	r3, [sp, #76]
	bl	COMM_MNGR_post_event_with_details
	b	.L70
.L69:
	cmp	r3, #300
	bne	.L71
	add	r0, sp, #116
	mov	r2, #24
	bl	memset
	ldr	r3, .L91+152
	add	r0, sp, #116
.LBE21:
	str	r8, [sp, #124]
	str	sl, [sp, #128]
.LBB22:
	str	r3, [sp, #116]
	bl	CODE_DISTRIBUTION_post_event_with_details
	b	.L70
.L71:
	cmp	r3, #200
	bne	.L72
	add	r0, sp, #160
	mov	r2, #20
	bl	memset
	mov	r3, #122
	add	r0, sp, #160
.LBE22:
	str	r8, [sp, #172]
	str	sl, [sp, #176]
.LBB23:
	str	r3, [sp, #160]
	bl	CONTROLLER_INITIATED_post_event_with_details
	b	.L70
.L72:
	ldr	r0, .L91+156
	bl	Alert_Message
.L70:
	add	r3, r5, #4096
	mov	r2, #0
	str	r2, [r3, #48]
	b	.L67
.L68:
	add	r3, r5, #4096
	ldr	r1, [r3, #28]
	ldr	r2, [r2, #0]
	add	r3, r3, #28
	cmp	r1, r2
	bls	.L67
.LBB19:
	ldr	r3, [r3, #0]
	add	r3, r3, #1
	rsb	r3, r2, r3
	ldr	r2, [r8, #0]
	cmp	r3, r2
	strhi	r3, [r8, #0]
.L67:
.LBE19:
.LBE23:
.LBE38:
	ldr	r3, .L91+160
	ldr	r3, [r4, r3]
	cmp	r3, #0
	beq	.L36
	cmp	r6, #0
	moveq	r0, r4
	moveq	r1, #1
	beq	.L89
	cmp	r6, #1
	moveq	r0, r4
	moveq	r1, #0
	beq	.L89
	cmp	r6, #2
	beq	.L36
	cmp	r6, #3
	bne	.L36
	mov	r0, r4
	mov	r1, #2
.L89:
	bl	xfer_bytes_to_another_port.isra.0
.L36:
	bl	xTaskGetTickCount
	ldr	r3, .L91+164
	ldr	r2, [sp, #8]
	str	r0, [r3, r2, asl #2]
	b	.L75
.L63:
.LBB39:
.LBB37:
	mov	r0, r8
	add	r1, sp, #160
	bl	strstr
	subs	r3, r0, #0
	beq	.L61
	rsb	r3, r8, r3
	add	fp, r3, #1
	ldr	r1, .L91+100
	mov	r2, #500
	mov	r0, fp
	str	r3, [sp, #0]
	bl	mem_malloc_debug
	ldr	r3, [sp, #0]
	mov	r1, r8
	mov	r2, r3
	mov	sl, r0
	bl	memcpy
	ldr	r3, [sp, #0]
	mov	r1, #0
	strb	r1, [sl, r3]
	add	r3, r5, #4096
	add	r3, r3, #52
	ldr	r3, [r3, #0]
	cmp	r3, #100
	bne	.L84
	b	.L90
.L92:
	.align	2
.L91:
	.word	Task_Table
	.word	.LC3
	.word	rcvd_data_binary_semaphore
	.word	.LC4
	.word	.LC5
	.word	4280
	.word	SerDrvrVars_s
	.word	4196
	.word	port_names
	.word	.LC6
	.word	4200
	.word	.LC7
	.word	4204
	.word	.LC8
	.word	4208
	.word	.LC9
	.word	4100
	.word	preamble
	.word	4124
	.word	4126
	.word	postamble
	.word	4128
	.word	4130
	.word	4132
	.word	4136
	.word	.LC1
	.word	285
	.word	4134
	.word	.LC10
	.word	4108
	.word	4556
	.word	.LC11
	.word	4112
	.word	.LC12
	.word	4573
	.word	.LC13
	.word	4116
	.word	626
	.word	4590
	.word	.LC14
	.word	4104
	.word	task_last_execution_stamp
.LBE37:
.LBE39:
.LFE8:
	.size	ring_buffer_analysis_task, .-ring_buffer_analysis_task
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Port Xfer: %s too many blocks\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/rcvd_data.c\000"
.LC2:
	.ascii	"Data Transfer: lengths don't match\000"
.LC3:
	.ascii	"RING BUF TASK : bad paramter?\000"
.LC4:
	.ascii	"RING BUF TASK : NULL semaphore?\000"
.LC5:
	.ascii	"RING BUF TASK : can't take semaphore?\000"
.LC6:
	.ascii	"%s : PH Tail caught INDEX\000"
.LC7:
	.ascii	"%s : DH Tail caught INDEX\000"
.LC8:
	.ascii	"%s : SH Tail caught INDEX\000"
.LC9:
	.ascii	"%s : TH Tail caught INDEX\000"
.LC10:
	.ascii	"Packet lengths don't match\000"
.LC11:
	.ascii	"Specified HUNT incorrectly set up.\000"
.LC13:
	.ascii	"CR-LF HUNT incorrectly set up.\000"
.LC14:
	.ascii	"TH HUNT incorrectly set up.\000"
.LC12:
	.ascii	"\015\012\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI0-.LFB9
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI2-.LFB0
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI4-.LFB8
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0xd8
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/rcvd_data.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xa1
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF9
	.byte	0x1
	.4byte	.LASF10
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x2db
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x33b
	.byte	0x1
	.uleb128 0x3
	.4byte	0x2a
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x22
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x66
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.byte	0x9c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x162
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1aa
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x236
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x3bd
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB9
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI1
	.4byte	.LFE9
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB8
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI5
	.4byte	.LFE8
	.2byte	0x3
	.byte	0x7d
	.sleb128 216
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF4:
	.ascii	"check_ring_buffer_for_CalsenseTPL_packet\000"
.LASF2:
	.ascii	"__test_packet_echo\000"
.LASF10:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/rcvd_data.c\000"
.LASF1:
	.ascii	"xfer_bytes_to_another_port\000"
.LASF3:
	.ascii	"RCVD_DATA_enable_hunting_mode\000"
.LASF9:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF8:
	.ascii	"ring_buffer_analysis_task\000"
.LASF0:
	.ascii	"__bytes_available_to_process\000"
.LASF7:
	.ascii	"check_ring_buffer_for_a_specified_termination\000"
.LASF5:
	.ascii	"check_ring_buffer_for_a_specified_string\000"
.LASF6:
	.ascii	"check_ring_buffer_for_a_crlf_delimited_string\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
