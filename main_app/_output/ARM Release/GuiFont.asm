	.file	"GuiFont.c"
	.text
.Ltext0:
	.global	GuiFont_LanguageIndex
	.global	GuiFont_FontList
	.global	GuiFont_ChPtrList
	.section	.rodata.Ch000157,"a",%progbits
	.type	Ch000157, %object
	.size	Ch000157, 22
Ch000157:
	.byte	0
	.byte	1
	.byte	1
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	0
	.byte	-64
	.byte	96
	.byte	48
	.byte	24
	.byte	48
	.byte	96
	.byte	-64
	.section	.rodata.Ch000158,"a",%progbits
	.type	Ch000158, %object
	.size	Ch000158, 22
Ch000158:
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	120
	.byte	-52
	.byte	12
	.byte	24
	.byte	48
	.byte	0
	.byte	48
	.section	.rodata.Ch000159,"a",%progbits
	.type	Ch000159, %object
	.size	Ch000159, 21
Ch000159:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	8
	.byte	120
	.byte	-52
	.byte	-36
	.byte	-40
	.byte	-64
	.byte	120
	.section	.rodata.Ch000326,"a",%progbits
	.type	Ch000326, %object
	.size	Ch000326, 20
Ch000326:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	1
	.byte	5
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	11
	.byte	-58
	.byte	7
	.byte	-64
	.byte	-8
	.byte	-20
	.byte	-58
	.section	.rodata.Ch000417,"a",%progbits
	.type	Ch000417, %object
	.size	Ch000417, 24
Ch000417:
	.byte	6
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	4
	.byte	13
	.byte	-122
	.byte	3
	.byte	3
	.byte	59
	.byte	127
	.byte	-25
	.byte	-61
	.byte	-25
	.byte	127
	.byte	59
	.section	.rodata.Ch000418,"a",%progbits
	.type	Ch000418, %object
	.size	Ch000418, 25
Ch000418:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	4
	.byte	6
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	7
	.byte	10
	.byte	32
	.byte	0
	.byte	60
	.byte	126
	.byte	-25
	.byte	-61
	.byte	-1
	.byte	-64
	.byte	-29
	.byte	127
	.byte	62
	.section	.rodata.Ch000419,"a",%progbits
	.type	Ch000419, %object
	.size	Ch000419, 27
Ch000419:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	4
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	0
	.byte	8
	.byte	7
	.byte	14
	.byte	112
	.byte	0
	.byte	59
	.byte	127
	.byte	-25
	.byte	-61
	.byte	-25
	.byte	127
	.byte	59
	.byte	3
	.byte	-57
	.byte	-2
	.byte	124
	.section	.rodata.Ch000327,"a",%progbits
	.type	Ch000327, %object
	.size	Ch000327, 19
Ch000327:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	11
	.byte	-14
	.byte	7
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000486,"a",%progbits
	.type	Ch000486, %object
	.size	Ch000486, 20
Ch000486:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	10
	.byte	-4
	.byte	0
	.byte	120
	.byte	-52
	.byte	120
	.byte	12
	.section	.rodata.Ch000523,"a",%progbits
	.type	Ch000523, %object
	.size	Ch000523, 19
Ch000523:
	.byte	1
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	5
	.byte	6
	.byte	5
	.byte	4
	.byte	3
	.byte	0
	.byte	7
	.byte	6
	.byte	7
	.byte	42
	.byte	-58
	.byte	108
	.byte	56
	.byte	16
	.section	.rodata.Ch000524,"a",%progbits
	.type	Ch000524, %object
	.size	Ch000524, 18
Ch000524:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	6
	.byte	7
	.byte	30
	.byte	-37
	.byte	-1
	.byte	102
	.section	.rodata.Ch000553,"a",%progbits
	.type	Ch000553, %object
	.size	Ch000553, 46
Ch000553:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	16
	.byte	0
	.byte	16
	.byte	4
	.byte	0
	.byte	1
	.byte	-128
	.byte	3
	.byte	-64
	.byte	7
	.byte	-32
	.byte	6
	.byte	96
	.byte	14
	.byte	112
	.byte	13
	.byte	-80
	.byte	29
	.byte	-72
	.byte	25
	.byte	-104
	.byte	57
	.byte	-100
	.byte	49
	.byte	-116
	.byte	112
	.byte	14
	.byte	97
	.byte	-122
	.byte	-32
	.byte	7
	.byte	-1
	.byte	-1
	.byte	127
	.byte	-2
	.section	.rodata.Ch000525,"a",%progbits
	.type	Ch000525, %object
	.size	Ch000525, 20
Ch000525:
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	6
	.byte	7
	.byte	66
	.byte	-52
	.byte	120
	.byte	48
	.byte	120
	.byte	-52
	.section	.rodata.Ch000526,"a",%progbits
	.type	Ch000526, %object
	.size	Ch000526, 20
Ch000526:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	4
	.byte	0
	.byte	6
	.byte	6
	.byte	9
	.byte	62
	.byte	0
	.byte	-52
	.byte	124
	.byte	12
	.byte	120
	.section	.rodata.Ch000527,"a",%progbits
	.type	Ch000527, %object
	.size	Ch000527, 22
Ch000527:
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	6
	.byte	7
	.byte	0
	.byte	-4
	.byte	12
	.byte	24
	.byte	48
	.byte	96
	.byte	-64
	.byte	-4
	.section	.rodata.Ch000196,"a",%progbits
	.type	Ch000196, %object
	.size	Ch000196, 20
Ch000196:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	0
	.byte	112
	.byte	-40
	.byte	-8
	.byte	-64
	.byte	112
	.section	.rodata.Ch000197,"a",%progbits
	.type	Ch000197, %object
	.size	Ch000197, 20
Ch000197:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	96
	.byte	48
	.byte	120
	.byte	96
	.byte	-16
	.byte	96
	.section	.rodata.Ch000529,"a",%progbits
	.type	Ch000529, %object
	.size	Ch000529, 17
Ch000529:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	11
	.byte	-2
	.byte	7
	.byte	-64
	.section	.rodata.Ch000490,"a",%progbits
	.type	Ch000490, %object
	.size	Ch000490, 18
Ch000490:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-2
	.byte	0
	.byte	-52
	.byte	120
	.section	.rodata.Ch000260,"a",%progbits
	.type	Ch000260, %object
	.size	Ch000260, 24
Ch000260:
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.byte	5
	.byte	5
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	12
	.byte	1
	.byte	56
	.byte	108
	.byte	56
	.byte	115
	.byte	-34
	.byte	-52
	.byte	-50
	.byte	115
	.section	.rodata.Ch000460,"a",%progbits
	.type	Ch000460, %object
	.size	Ch000460, 20
Ch000460:
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-76
	.byte	1
	.byte	-4
	.byte	12
	.byte	24
	.byte	48
	.section	.rodata.Ch000262,"a",%progbits
	.type	Ch000262, %object
	.size	Ch000262, 21
Ch000262:
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	6
	.byte	5
	.byte	5
	.byte	6
	.byte	5
	.byte	3
	.byte	4
	.byte	3
	.byte	11
	.byte	-8
	.byte	1
	.byte	48
	.byte	96
	.byte	-64
	.byte	96
	.byte	48
	.section	.rodata.Ch000233,"a",%progbits
	.type	Ch000233, %object
	.size	Ch000233, 20
Ch000233:
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	0
	.byte	3
	.byte	2
	.byte	5
	.byte	0
	.byte	-64
	.byte	32
	.byte	64
	.byte	32
	.byte	-64
	.section	.rodata.Ch000462,"a",%progbits
	.type	Ch000462, %object
	.size	Ch000462, 22
Ch000462:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	28
	.byte	0
	.byte	120
	.byte	-52
	.byte	124
	.byte	12
	.byte	24
	.byte	112
	.section	.rodata.Ch000390,"a",%progbits
	.type	Ch000390, %object
	.size	Ch000390, 38
Ch000390:
	.byte	2
	.byte	0
	.byte	0
	.byte	1
	.byte	4
	.byte	7
	.byte	6
	.byte	8
	.byte	7
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	0
	.byte	3
	.byte	31
	.byte	0
	.byte	63
	.byte	0
	.byte	112
	.byte	0
	.byte	-32
	.byte	0
	.byte	-34
	.byte	0
	.byte	-1
	.byte	0
	.byte	-29
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	127
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000264,"a",%progbits
	.type	Ch000264, %object
	.size	Ch000264, 20
Ch000264:
	.byte	4
	.byte	2
	.byte	1
	.byte	4
	.byte	4
	.byte	4
	.byte	6
	.byte	7
	.byte	4
	.byte	4
	.byte	1
	.byte	7
	.byte	5
	.byte	7
	.byte	66
	.byte	108
	.byte	56
	.byte	-2
	.byte	56
	.byte	108
	.section	.rodata.Ch000392,"a",%progbits
	.type	Ch000392, %object
	.size	Ch000392, 42
Ch000392:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	4
	.byte	7
	.byte	8
	.byte	8
	.byte	7
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	0
	.byte	0
	.byte	62
	.byte	0
	.byte	127
	.byte	0
	.byte	-29
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	127
	.byte	0
	.byte	62
	.byte	0
	.byte	127
	.byte	0
	.byte	-29
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	127
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000393,"a",%progbits
	.type	Ch000393, %object
	.size	Ch000393, 38
Ch000393:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	4
	.byte	7
	.byte	8
	.byte	8
	.byte	6
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	48
	.byte	0
	.byte	62
	.byte	0
	.byte	127
	.byte	0
	.byte	-29
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	127
	.byte	-128
	.byte	61
	.byte	-128
	.byte	3
	.byte	-128
	.byte	7
	.byte	0
	.byte	126
	.byte	0
	.byte	124
	.byte	0
	.section	.rodata.Ch000235,"a",%progbits
	.type	Ch000235, %object
	.size	Ch000235, 23
Ch000235:
	.byte	0
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	1
	.byte	8
	.byte	0
	.byte	64
	.byte	-60
	.byte	76
	.byte	88
	.byte	54
	.byte	106
	.byte	-50
	.byte	-126
	.section	.rodata.Ch000266,"a",%progbits
	.type	Ch000266, %object
	.size	Ch000266, 17
Ch000266:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	2
	.byte	3
	.byte	12
	.byte	4
	.byte	6
	.byte	96
	.byte	-64
	.section	.rodata.Ch000396,"a",%progbits
	.type	Ch000396, %object
	.size	Ch000396, 38
Ch000396:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	7
	.byte	8
	.byte	8
	.byte	7
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	64
	.byte	2
	.byte	-2
	.byte	0
	.byte	-1
	.byte	0
	.byte	-61
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-61
	.byte	-128
	.byte	-1
	.byte	0
	.byte	-61
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-61
	.byte	-128
	.byte	-1
	.byte	0
	.byte	-2
	.byte	0
	.section	.rodata.Ch000236,"a",%progbits
	.type	Ch000236, %object
	.size	Ch000236, 23
Ch000236:
	.byte	0
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	1
	.byte	8
	.byte	0
	.byte	64
	.byte	-60
	.byte	76
	.byte	88
	.byte	60
	.byte	98
	.byte	-60
	.byte	-114
	.section	.rodata.Ch000398,"a",%progbits
	.type	Ch000398, %object
	.size	Ch000398, 30
Ch000398:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	7
	.byte	8
	.byte	8
	.byte	7
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	-16
	.byte	3
	.byte	-2
	.byte	0
	.byte	-1
	.byte	0
	.byte	-61
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-61
	.byte	-128
	.byte	-1
	.byte	0
	.byte	-2
	.byte	0
	.section	.rodata.Ch000268,"a",%progbits
	.type	Ch000268, %object
	.size	Ch000268, 16
Ch000268:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	12
	.byte	2
	.byte	2
	.byte	-64
	.section	.rodata.Ch000160,"a",%progbits
	.type	Ch000160, %object
	.size	Ch000160, 20
Ch000160:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	72
	.byte	48
	.byte	120
	.byte	-52
	.byte	-4
	.byte	-52
	.section	.rodata.Ch000302,"a",%progbits
	.type	Ch000302, %object
	.size	Ch000302, 22
Ch000302:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	7
	.byte	6
	.byte	1
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	24
	.byte	7
	.byte	-4
	.byte	-58
	.byte	-61
	.byte	-58
	.byte	-4
	.byte	-64
	.section	.rodata.Ch000162,"a",%progbits
	.type	Ch000162, %object
	.size	Ch000162, 20
Ch000162:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	24
	.byte	120
	.byte	-52
	.byte	-64
	.byte	-52
	.byte	120
	.section	.rodata.Ch000303,"a",%progbits
	.type	Ch000303, %object
	.size	Ch000303, 30
Ch000303:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	7
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	0
	.byte	9
	.byte	3
	.byte	12
	.byte	-8
	.byte	0
	.byte	60
	.byte	0
	.byte	102
	.byte	0
	.byte	-61
	.byte	0
	.byte	-49
	.byte	0
	.byte	102
	.byte	0
	.byte	63
	.byte	0
	.byte	1
	.byte	-128
	.section	.rodata.Ch000164,"a",%progbits
	.type	Ch000164, %object
	.size	Ch000164, 20
Ch000164:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	4
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	36
	.byte	-4
	.byte	-64
	.byte	-8
	.byte	-64
	.byte	-4
	.section	.rodata.Ch000165,"a",%progbits
	.type	Ch000165, %object
	.size	Ch000165, 19
Ch000165:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	4
	.byte	4
	.byte	2
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	100
	.byte	-4
	.byte	-64
	.byte	-8
	.byte	-64
	.section	.rodata.Ch000166,"a",%progbits
	.type	Ch000166, %object
	.size	Ch000166, 21
Ch000166:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	8
	.byte	120
	.byte	-52
	.byte	-64
	.byte	-36
	.byte	-52
	.byte	124
	.section	.rodata.Ch000305,"a",%progbits
	.type	Ch000305, %object
	.size	Ch000305, 25
Ch000305:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	6
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	8
	.byte	1
	.byte	60
	.byte	102
	.byte	-61
	.byte	96
	.byte	60
	.byte	6
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000205,"a",%progbits
	.type	Ch000205, %object
	.size	Ch000205, 17
Ch000205:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	28
	.byte	-16
	.byte	-40
	.section	.rodata.Ch000306,"a",%progbits
	.type	Ch000306, %object
	.size	Ch000306, 18
Ch000306:
	.byte	0
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	7
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-4
	.byte	7
	.byte	-1
	.byte	24
	.section	.rodata.Ch000207,"a",%progbits
	.type	Ch000207, %object
	.size	Ch000207, 19
Ch000207:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	1
	.byte	0
	.byte	5
	.byte	4
	.byte	7
	.byte	76
	.byte	-16
	.byte	-40
	.byte	-16
	.byte	-64
	.section	.rodata.Ch000307,"a",%progbits
	.type	Ch000307, %object
	.size	Ch000307, 19
Ch000307:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-2
	.byte	1
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000209,"a",%progbits
	.type	Ch000209, %object
	.size	Ch000209, 18
Ch000209:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	3
	.byte	1
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	24
	.byte	-40
	.byte	-16
	.byte	-64
	.section	.rodata.Ch000308,"a",%progbits
	.type	Ch000308, %object
	.size	Ch000308, 20
Ch000308:
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	7
	.byte	7
	.byte	6
	.byte	5
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-74
	.byte	5
	.byte	-61
	.byte	102
	.byte	60
	.byte	24
	.section	.rodata.Ch000309,"a",%progbits
	.type	Ch000309, %object
	.size	Ch000309, 20
Ch000309:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	94
	.byte	6
	.byte	-61
	.byte	-37
	.byte	-1
	.byte	102
	.section	.rodata.Ch000415,"a",%progbits
	.type	Ch000415, %object
	.size	Ch000415, 24
Ch000415:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	1
	.byte	6
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	4
	.byte	13
	.byte	-122
	.byte	3
	.byte	-64
	.byte	-36
	.byte	-2
	.byte	-25
	.byte	-61
	.byte	-25
	.byte	-2
	.byte	-36
	.section	.rodata.Ch000430,"a",%progbits
	.type	Ch000430, %object
	.size	Ch000430, 22
Ch000430:
	.byte	3
	.byte	1
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	6
	.byte	6
	.byte	7
	.byte	5
	.byte	1
	.byte	7
	.byte	4
	.byte	13
	.byte	-42
	.byte	3
	.byte	48
	.byte	-4
	.byte	48
	.byte	54
	.byte	62
	.byte	28
	.section	.rodata.Ch000558,"a",%progbits
	.type	Ch000558, %object
	.size	Ch000558, 21
Ch000558:
	.byte	4
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	8
	.byte	8
	.byte	8
	.byte	0
	.byte	13
	.byte	10
	.byte	4
	.byte	4
	.byte	119
	.byte	112
	.byte	-120
	.byte	-120
	.byte	119
	.byte	112
	.section	.rodata.Ch000416,"a",%progbits
	.type	Ch000416, %object
	.size	Ch000416, 23
Ch000416:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	4
	.byte	7
	.byte	6
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	7
	.byte	10
	.byte	112
	.byte	0
	.byte	62
	.byte	127
	.byte	-29
	.byte	-64
	.byte	-29
	.byte	127
	.byte	62
	.section	.rodata.Ch000434,"a",%progbits
	.type	Ch000434, %object
	.size	Ch000434, 32
Ch000434:
	.byte	4
	.byte	0
	.byte	1
	.byte	3
	.byte	0
	.byte	4
	.byte	8
	.byte	7
	.byte	5
	.byte	4
	.byte	0
	.byte	9
	.byte	7
	.byte	14
	.byte	-74
	.byte	2
	.byte	-63
	.byte	-128
	.byte	99
	.byte	0
	.byte	54
	.byte	0
	.byte	28
	.byte	0
	.byte	24
	.byte	0
	.byte	56
	.byte	0
	.byte	-16
	.byte	0
	.byte	-32
	.byte	0
	.section	.rodata.Ch000504,"a",%progbits
	.type	Ch000504, %object
	.size	Ch000504, 20
Ch000504:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	5
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	6
	.byte	7
	.byte	24
	.byte	120
	.byte	-52
	.byte	-64
	.byte	-52
	.byte	120
	.section	.rodata.Ch000432,"a",%progbits
	.type	Ch000432, %object
	.size	Ch000432, 26
Ch000432:
	.byte	4
	.byte	0
	.byte	1
	.byte	3
	.byte	4
	.byte	4
	.byte	8
	.byte	7
	.byte	5
	.byte	4
	.byte	0
	.byte	9
	.byte	7
	.byte	10
	.byte	86
	.byte	1
	.byte	-63
	.byte	-128
	.byte	99
	.byte	0
	.byte	54
	.byte	0
	.byte	28
	.byte	0
	.byte	8
	.byte	0
	.section	.rodata.Ch000505,"a",%progbits
	.type	Ch000505, %object
	.size	Ch000505, 20
Ch000505:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-14
	.byte	0
	.byte	12
	.byte	124
	.byte	-52
	.byte	124
	.section	.rodata.Ch000506,"a",%progbits
	.type	Ch000506, %object
	.size	Ch000506, 21
Ch000506:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	6
	.byte	7
	.byte	4
	.byte	120
	.byte	-52
	.byte	-4
	.byte	-64
	.byte	-52
	.byte	120
	.section	.rodata.Ch000521,"a",%progbits
	.type	Ch000521, %object
	.size	Ch000521, 20
Ch000521:
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	3
	.byte	4
	.byte	3
	.byte	1
	.byte	4
	.byte	4
	.byte	9
	.byte	-14
	.byte	0
	.byte	96
	.byte	-16
	.byte	96
	.byte	48
	.section	.rodata.Ch000507,"a",%progbits
	.type	Ch000507, %object
	.size	Ch000507, 20
Ch000507:
	.byte	1
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	4
	.byte	4
	.byte	9
	.byte	-28
	.byte	1
	.byte	48
	.byte	96
	.byte	-16
	.byte	96
	.section	.rodata.Ch000577,"a",%progbits
	.type	Ch000577, %object
	.size	Ch000577, 42
Ch000577:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	1
	.byte	14
	.byte	1
	.byte	14
	.byte	4
	.byte	0
	.byte	-1
	.byte	-4
	.byte	-128
	.byte	4
	.byte	-128
	.byte	12
	.byte	-128
	.byte	28
	.byte	-128
	.byte	52
	.byte	-128
	.byte	100
	.byte	-128
	.byte	-60
	.byte	-63
	.byte	-124
	.byte	-29
	.byte	4
	.byte	-74
	.byte	4
	.byte	-100
	.byte	4
	.byte	-120
	.byte	4
	.byte	-1
	.byte	-4
	.section	.rodata.Ch000508,"a",%progbits
	.type	Ch000508, %object
	.size	Ch000508, 21
Ch000508:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	5
	.byte	5
	.byte	5
	.byte	4
	.byte	0
	.byte	6
	.byte	6
	.byte	9
	.byte	60
	.byte	0
	.byte	120
	.byte	-52
	.byte	124
	.byte	12
	.byte	120
	.section	.rodata.Ch000509,"a",%progbits
	.type	Ch000509, %object
	.size	Ch000509, 19
Ch000509:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-14
	.byte	1
	.byte	-64
	.byte	-8
	.byte	-52
	.section	.rodata.Ch000561,"a",%progbits
	.type	Ch000561, %object
	.size	Ch000561, 34
Ch000561:
	.byte	3
	.byte	3
	.byte	5
	.byte	2
	.byte	1
	.byte	4
	.byte	4
	.byte	7
	.byte	3
	.byte	4
	.byte	0
	.byte	9
	.byte	7
	.byte	9
	.byte	0
	.byte	0
	.byte	-1
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-127
	.byte	-128
	.byte	-125
	.byte	-128
	.byte	-58
	.byte	-128
	.byte	-20
	.byte	-128
	.byte	-72
	.byte	-128
	.byte	-112
	.byte	-128
	.byte	-1
	.byte	-128
	.section	.rodata.Ch000383,"a",%progbits
	.type	Ch000383, %object
	.size	Ch000383, 18
Ch000383:
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	5
	.byte	4
	.byte	2
	.byte	4
	.byte	13
	.byte	4
	.byte	4
	.byte	96
	.byte	-16
	.byte	96
	.section	.rodata.Ch000440,"a",%progbits
	.type	Ch000440, %object
	.size	Ch000440, 20
Ch000440:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	6
	.byte	6
	.byte	4
	.byte	3
	.byte	0
	.byte	7
	.byte	6
	.byte	5
	.byte	0
	.byte	108
	.byte	-2
	.byte	108
	.byte	-2
	.byte	108
	.section	.rodata.Ch000441,"a",%progbits
	.type	Ch000441, %object
	.size	Ch000441, 25
Ch000441:
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-112
	.byte	0
	.byte	24
	.byte	126
	.byte	-37
	.byte	-40
	.byte	126
	.byte	27
	.byte	-37
	.byte	126
	.byte	24
	.section	.rodata.Ch000386,"a",%progbits
	.type	Ch000386, %object
	.size	Ch000386, 38
Ch000386:
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	4
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	16
	.byte	16
	.byte	62
	.byte	0
	.byte	127
	.byte	0
	.byte	-29
	.byte	-128
	.byte	1
	.byte	-128
	.byte	3
	.byte	-128
	.byte	15
	.byte	0
	.byte	62
	.byte	0
	.byte	120
	.byte	0
	.byte	-32
	.byte	0
	.byte	-64
	.byte	0
	.byte	-1
	.byte	-128
	.section	.rodata.Ch000443,"a",%progbits
	.type	Ch000443, %object
	.size	Ch000443, 32
Ch000443:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	5
	.byte	7
	.byte	8
	.byte	8
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	9
	.byte	4
	.byte	0
	.byte	120
	.byte	0
	.byte	-52
	.byte	0
	.byte	120
	.byte	0
	.byte	57
	.byte	-128
	.byte	111
	.byte	0
	.byte	-58
	.byte	0
	.byte	-49
	.byte	0
	.byte	121
	.byte	-128
	.section	.rodata.Ch000420,"a",%progbits
	.type	Ch000420, %object
	.size	Ch000420, 21
Ch000420:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	1
	.byte	6
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	4
	.byte	13
	.byte	-122
	.byte	31
	.byte	-64
	.byte	-36
	.byte	-2
	.byte	-25
	.byte	-61
	.section	.rodata.Ch000445,"a",%progbits
	.type	Ch000445, %object
	.size	Ch000445, 21
Ch000445:
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	4
	.byte	3
	.byte	2
	.byte	4
	.byte	3
	.byte	1
	.byte	4
	.byte	3
	.byte	11
	.byte	-12
	.byte	2
	.byte	48
	.byte	96
	.byte	-64
	.byte	96
	.byte	48
	.section	.rodata.Ch000421,"a",%progbits
	.type	Ch000421, %object
	.size	Ch000421, 19
Ch000421:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	4
	.byte	13
	.byte	-22
	.byte	31
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000170,"a",%progbits
	.type	Ch000170, %object
	.size	Ch000170, 22
Ch000170:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	4
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	-52
	.byte	-40
	.byte	-16
	.byte	-32
	.byte	-16
	.byte	-40
	.byte	-52
	.section	.rodata.Ch000171,"a",%progbits
	.type	Ch000171, %object
	.size	Ch000171, 17
Ch000171:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	62
	.byte	-64
	.byte	-8
	.section	.rodata.Ch000172,"a",%progbits
	.type	Ch000172, %object
	.size	Ch000172, 20
Ch000172:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	2
	.byte	7
	.byte	96
	.byte	-58
	.byte	-18
	.byte	-2
	.byte	-42
	.byte	-58
	.section	.rodata.Ch000173,"a",%progbits
	.type	Ch000173, %object
	.size	Ch000173, 20
Ch000173:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	36
	.byte	-52
	.byte	-20
	.byte	-4
	.byte	-36
	.byte	-52
	.section	.rodata.Ch000174,"a",%progbits
	.type	Ch000174, %object
	.size	Ch000174, 18
Ch000174:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	60
	.byte	120
	.byte	-52
	.byte	120
	.section	.rodata.Ch000175,"a",%progbits
	.type	Ch000175, %object
	.size	Ch000175, 19
Ch000175:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	4
	.byte	2
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	100
	.byte	-8
	.byte	-52
	.byte	-8
	.byte	-64
	.section	.rodata.Ch000176,"a",%progbits
	.type	Ch000176, %object
	.size	Ch000176, 20
Ch000176:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	0
	.byte	7
	.byte	2
	.byte	8
	.byte	28
	.byte	120
	.byte	-52
	.byte	-36
	.byte	124
	.byte	14
	.section	.rodata.Ch000325,"a",%progbits
	.type	Ch000325, %object
	.size	Ch000325, 24
Ch000325:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	0
	.byte	7
	.byte	6
	.byte	11
	.byte	56
	.byte	0
	.byte	62
	.byte	110
	.byte	-58
	.byte	110
	.byte	62
	.byte	6
	.byte	-52
	.byte	120
	.section	.rodata.Ch000178,"a",%progbits
	.type	Ch000178, %object
	.size	Ch000178, 22
Ch000178:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	120
	.byte	-52
	.byte	-64
	.byte	120
	.byte	12
	.byte	-52
	.byte	120
	.section	.rodata.Ch000179,"a",%progbits
	.type	Ch000179, %object
	.size	Ch000179, 17
Ch000179:
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	5
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	124
	.byte	-4
	.byte	48
	.section	.rodata.Ch000217,"a",%progbits
	.type	Ch000217, %object
	.size	Ch000217, 20
Ch000217:
	.byte	2
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	3
	.byte	5
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	5
	.byte	0
	.byte	-4
	.byte	24
	.byte	48
	.byte	96
	.byte	-4
	.section	.rodata.Ch000218,"a",%progbits
	.type	Ch000218, %object
	.size	Ch000218, 20
Ch000218:
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	1
	.byte	4
	.byte	2
	.byte	7
	.byte	36
	.byte	48
	.byte	96
	.byte	-64
	.byte	96
	.byte	48
	.section	.rodata.Ch000219,"a",%progbits
	.type	Ch000219, %object
	.size	Ch000219, 16
Ch000219:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	7
	.byte	126
	.byte	-64
	.section	.rodata.Ch000412,"a",%progbits
	.type	Ch000412, %object
	.size	Ch000412, 30
Ch000412:
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	4
	.byte	8
	.byte	8
	.byte	7
	.byte	5
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	78
	.byte	18
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	99
	.byte	0
	.byte	119
	.byte	0
	.byte	54
	.byte	0
	.byte	62
	.byte	0
	.byte	28
	.byte	0
	.section	.rodata.Ch000429,"a",%progbits
	.type	Ch000429, %object
	.size	Ch000429, 26
Ch000429:
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	7
	.byte	10
	.byte	0
	.byte	0
	.byte	124
	.byte	-2
	.byte	-58
	.byte	-64
	.byte	-4
	.byte	126
	.byte	6
	.byte	-58
	.byte	-2
	.byte	124
	.section	.rodata.Ch000413,"a",%progbits
	.type	Ch000413, %object
	.size	Ch000413, 28
Ch000413:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	4
	.byte	8
	.byte	8
	.byte	8
	.byte	7
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	-98
	.byte	20
	.byte	-63
	.byte	-128
	.byte	-55
	.byte	-128
	.byte	-35
	.byte	-128
	.byte	-1
	.byte	-128
	.byte	-9
	.byte	-128
	.byte	99
	.byte	0
	.section	.rodata.Ch000414,"a",%progbits
	.type	Ch000414, %object
	.size	Ch000414, 26
Ch000414:
	.byte	3
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.byte	4
	.byte	6
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	7
	.byte	10
	.byte	0
	.byte	0
	.byte	60
	.byte	126
	.byte	103
	.byte	3
	.byte	127
	.byte	-1
	.byte	-61
	.byte	-57
	.byte	-1
	.byte	123
	.section	.rodata.Ch000478,"a",%progbits
	.type	Ch000478, %object
	.size	Ch000478, 17
Ch000478:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	4
	.byte	9
	.byte	-2
	.byte	1
	.byte	-64
	.section	.rodata.Ch000270,"a",%progbits
	.type	Ch000270, %object
	.size	Ch000270, 21
Ch000270:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	6
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-12
	.byte	2
	.byte	60
	.byte	102
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000272,"a",%progbits
	.type	Ch000272, %object
	.size	Ch000272, 25
Ch000272:
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.byte	6
	.byte	7
	.byte	6
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	8
	.byte	2
	.byte	60
	.byte	102
	.byte	-61
	.byte	3
	.byte	14
	.byte	56
	.byte	96
	.byte	-64
	.byte	-1
	.section	.rodata.Ch000274,"a",%progbits
	.type	Ch000274, %object
	.size	Ch000274, 22
Ch000274:
	.byte	2
	.byte	1
	.byte	0
	.byte	4
	.byte	3
	.byte	5
	.byte	5
	.byte	7
	.byte	5
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	84
	.byte	6
	.byte	28
	.byte	60
	.byte	108
	.byte	-52
	.byte	-1
	.byte	12
	.section	.rodata.Ch000313,"a",%progbits
	.type	Ch000313, %object
	.size	Ch000313, 19
Ch000313:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	6
	.byte	4
	.byte	4
	.byte	6
	.byte	5
	.byte	3
	.byte	4
	.byte	3
	.byte	11
	.byte	-4
	.byte	3
	.byte	-16
	.byte	-64
	.byte	-16
	.section	.rodata.Ch000451,"a",%progbits
	.type	Ch000451, %object
	.size	Ch000451, 16
Ch000451:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	11
	.byte	2
	.byte	2
	.byte	-64
	.section	.rodata.Ch000276,"a",%progbits
	.type	Ch000276, %object
	.size	Ch000276, 25
Ch000276:
	.byte	2
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	6
	.byte	5
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-128
	.byte	1
	.byte	30
	.byte	48
	.byte	96
	.byte	-64
	.byte	-4
	.byte	-26
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000312,"a",%progbits
	.type	Ch000312, %object
	.size	Ch000312, 25
Ch000312:
	.byte	0
	.byte	2
	.byte	1
	.byte	0
	.byte	3
	.byte	7
	.byte	7
	.byte	5
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	4
	.byte	2
	.byte	-1
	.byte	3
	.byte	6
	.byte	12
	.byte	24
	.byte	48
	.byte	96
	.byte	-64
	.byte	-1
	.section	.rodata.Ch000277,"a",%progbits
	.type	Ch000277, %object
	.size	Ch000277, 22
Ch000277:
	.byte	0
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	7
	.byte	7
	.byte	5
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	84
	.byte	5
	.byte	-1
	.byte	3
	.byte	6
	.byte	12
	.byte	24
	.byte	48
	.section	.rodata.Ch000455,"a",%progbits
	.type	Ch000455, %object
	.size	Ch000455, 24
Ch000455:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	3
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-128
	.byte	0
	.byte	120
	.byte	-52
	.byte	12
	.byte	24
	.byte	48
	.byte	96
	.byte	-64
	.byte	-4
	.section	.rodata.Ch000278,"a",%progbits
	.type	Ch000278, %object
	.size	Ch000278, 25
Ch000278:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	6
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	8
	.byte	1
	.byte	60
	.byte	102
	.byte	-61
	.byte	102
	.byte	60
	.byte	102
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000180,"a",%progbits
	.type	Ch000180, %object
	.size	Ch000180, 17
Ch000180:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	62
	.byte	-52
	.byte	120
	.section	.rodata.Ch000181,"a",%progbits
	.type	Ch000181, %object
	.size	Ch000181, 18
Ch000181:
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	86
	.byte	-52
	.byte	120
	.byte	48
	.section	.rodata.Ch000182,"a",%progbits
	.type	Ch000182, %object
	.size	Ch000182, 19
Ch000182:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	2
	.byte	7
	.byte	42
	.byte	-58
	.byte	-42
	.byte	-2
	.byte	108
	.section	.rodata.Ch000183,"a",%progbits
	.type	Ch000183, %object
	.size	Ch000183, 20
Ch000183:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	66
	.byte	-52
	.byte	120
	.byte	48
	.byte	120
	.byte	-52
	.section	.rodata.Ch000184,"a",%progbits
	.type	Ch000184, %object
	.size	Ch000184, 18
Ch000184:
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	102
	.byte	-52
	.byte	120
	.byte	48
	.section	.rodata.Ch000185,"a",%progbits
	.type	Ch000185, %object
	.size	Ch000185, 22
Ch000185:
	.byte	0
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	-4
	.byte	12
	.byte	24
	.byte	48
	.byte	96
	.byte	-64
	.byte	-4
	.section	.rodata.Ch000186,"a",%progbits
	.type	Ch000186, %object
	.size	Ch000186, 18
Ch000186:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	7
	.byte	60
	.byte	-32
	.byte	-64
	.byte	-32
	.section	.rodata.Ch000187,"a",%progbits
	.type	Ch000187, %object
	.size	Ch000187, 20
Ch000187:
	.byte	1
	.byte	1
	.byte	2
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	3
	.byte	1
	.byte	4
	.byte	2
	.byte	7
	.byte	66
	.byte	-64
	.byte	-32
	.byte	96
	.byte	112
	.byte	48
	.section	.rodata.Ch000188,"a",%progbits
	.type	Ch000188, %object
	.size	Ch000188, 18
Ch000188:
	.byte	0
	.byte	1
	.byte	1
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	0
	.byte	3
	.byte	2
	.byte	7
	.byte	60
	.byte	-32
	.byte	96
	.byte	-32
	.section	.rodata.Ch000189,"a",%progbits
	.type	Ch000189, %object
	.size	Ch000189, 18
Ch000189:
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	5
	.byte	5
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	1
	.byte	3
	.byte	0
	.byte	48
	.byte	120
	.byte	-52
	.section	.rodata.Ch000227,"a",%progbits
	.type	Ch000227, %object
	.size	Ch000227, 18
Ch000227:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	7
	.byte	114
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000228,"a",%progbits
	.type	Ch000228, %object
	.size	Ch000228, 22
Ch000228:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	5
	.byte	6
	.byte	6
	.byte	5
	.byte	3
	.byte	0
	.byte	7
	.byte	2
	.byte	7
	.byte	0
	.byte	56
	.byte	68
	.byte	-70
	.byte	-94
	.byte	-70
	.byte	68
	.byte	56
	.section	.rodata.Ch000229,"a",%progbits
	.type	Ch000229, %object
	.size	Ch000229, 21
Ch000229:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	5
	.byte	6
	.byte	6
	.byte	5
	.byte	3
	.byte	0
	.byte	7
	.byte	2
	.byte	7
	.byte	8
	.byte	56
	.byte	68
	.byte	-78
	.byte	-86
	.byte	68
	.byte	56
	.section	.rodata.Ch000314,"a",%progbits
	.type	Ch000314, %object
	.size	Ch000314, 25
Ch000314:
	.byte	1
	.byte	2
	.byte	3
	.byte	5
	.byte	3
	.byte	2
	.byte	3
	.byte	5
	.byte	6
	.byte	4
	.byte	1
	.byte	6
	.byte	3
	.byte	11
	.byte	2
	.byte	4
	.byte	-64
	.byte	-32
	.byte	96
	.byte	112
	.byte	48
	.byte	56
	.byte	24
	.byte	28
	.byte	12
	.section	.rodata.Ch000411,"a",%progbits
	.type	Ch000411, %object
	.size	Ch000411, 24
Ch000411:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	4
	.byte	8
	.byte	8
	.byte	8
	.byte	7
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	-2
	.byte	3
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	127
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000315,"a",%progbits
	.type	Ch000315, %object
	.size	Ch000315, 19
Ch000315:
	.byte	0
	.byte	2
	.byte	2
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	3
	.byte	11
	.byte	-4
	.byte	3
	.byte	-16
	.byte	48
	.byte	-16
	.section	.rodata.Ch000316,"a",%progbits
	.type	Ch000316, %object
	.size	Ch000316, 19
Ch000316:
	.byte	1
	.byte	0
	.byte	3
	.byte	3
	.byte	3
	.byte	6
	.byte	7
	.byte	4
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	2
	.byte	4
	.byte	0
	.byte	24
	.byte	60
	.byte	102
	.byte	-61
	.section	.rodata.Ch000452,"a",%progbits
	.type	Ch000452, %object
	.size	Ch000452, 21
Ch000452:
	.byte	3
	.byte	2
	.byte	1
	.byte	0
	.byte	2
	.byte	5
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	10
	.byte	-86
	.byte	2
	.byte	12
	.byte	24
	.byte	48
	.byte	96
	.byte	-64
	.section	.rodata.Ch000453,"a",%progbits
	.type	Ch000453, %object
	.size	Ch000453, 19
Ch000453:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-4
	.byte	0
	.byte	120
	.byte	-52
	.byte	120
	.section	.rodata.Ch000446,"a",%progbits
	.type	Ch000446, %object
	.size	Ch000446, 21
Ch000446:
	.byte	1
	.byte	2
	.byte	3
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	1
	.byte	4
	.byte	3
	.byte	11
	.byte	-12
	.byte	2
	.byte	-64
	.byte	96
	.byte	48
	.byte	96
	.byte	-64
	.section	.rodata.Ch000510,"a",%progbits
	.type	Ch000510, %object
	.size	Ch000510, 19
Ch000510:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	4
	.byte	9
	.byte	-8
	.byte	1
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000459,"a",%progbits
	.type	Ch000459, %object
	.size	Ch000459, 22
Ch000459:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-32
	.byte	0
	.byte	56
	.byte	96
	.byte	-64
	.byte	-8
	.byte	-52
	.byte	120
	.section	.rodata.Ch000061,"a",%progbits
	.type	Ch000061, %object
	.size	Ch000061, 18
Ch000061:
	.byte	0
	.byte	2
	.byte	2
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	3
	.byte	2
	.byte	7
	.byte	60
	.byte	-32
	.byte	32
	.byte	-32
	.section	.rodata.Ch000454,"a",%progbits
	.type	Ch000454, %object
	.size	Ch000454, 20
Ch000454:
	.byte	2
	.byte	1
	.byte	3
	.byte	3
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	1
	.byte	4
	.byte	4
	.byte	9
	.byte	-16
	.byte	1
	.byte	48
	.byte	112
	.byte	-16
	.byte	48
	.section	.rodata.Ch000062,"a",%progbits
	.type	Ch000062, %object
	.size	Ch000062, 18
Ch000062:
	.byte	2
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	3
	.byte	0
	.byte	32
	.byte	80
	.byte	-120
	.section	.rodata.Ch000063,"a",%progbits
	.type	Ch000063, %object
	.size	Ch000063, 16
Ch000063:
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	5
	.byte	2
	.byte	0
	.byte	6
	.byte	8
	.byte	1
	.byte	0
	.byte	-4
	.section	.rodata.Ch000064,"a",%progbits
	.type	Ch000064, %object
	.size	Ch000064, 17
Ch000064:
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	-128
	.byte	64
	.section	.rodata.Ch000546,"a",%progbits
	.type	Ch000546, %object
	.size	Ch000546, 21
Ch000546:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	10
	.byte	-32
	.byte	3
	.byte	116
	.byte	-72
	.byte	0
	.byte	-8
	.byte	-52
	.section	.rodata.Ch000204,"a",%progbits
	.type	Ch000204, %object
	.size	Ch000204, 18
Ch000204:
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	4
	.byte	5
	.byte	24
	.byte	-20
	.byte	-2
	.byte	-42
	.section	.rodata.Ch000547,"a",%progbits
	.type	Ch000547, %object
	.size	Ch000547, 22
Ch000547:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	10
	.byte	-32
	.byte	1
	.byte	24
	.byte	48
	.byte	0
	.byte	120
	.byte	-52
	.byte	120
	.section	.rodata.Ch000514,"a",%progbits
	.type	Ch000514, %object
	.size	Ch000514, 17
Ch000514:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	5
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	6
	.byte	7
	.byte	124
	.byte	-10
	.byte	-37
	.section	.rodata.Ch000492,"a",%progbits
	.type	Ch000492, %object
	.size	Ch000492, 19
Ch000492:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	4
	.byte	9
	.byte	126
	.byte	0
	.byte	-37
	.byte	-1
	.byte	102
	.section	.rodata.Ch000321,"a",%progbits
	.type	Ch000321, %object
	.size	Ch000321, 20
Ch000321:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	6
	.byte	5
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	6
	.byte	8
	.byte	56
	.byte	60
	.byte	102
	.byte	-64
	.byte	102
	.byte	60
	.section	.rodata.Font_ANSI7Bold,"a",%progbits
	.align	2
	.type	Font_ANSI7Bold, %object
	.size	Font_ANSI7Bold, 24
Font_ANSI7Bold:
	.byte	32
	.byte	1
	.byte	-4
	.byte	0
	.short	222
	.short	-1
	.short	443
	.byte	7
	.byte	11
	.byte	2
	.byte	4
	.byte	8
	.byte	10
	.byte	10
	.byte	9
	.byte	9
	.byte	6
	.byte	6
	.byte	1
	.byte	1
	.space	1
	.section	.rodata.Ch000391,"a",%progbits
	.type	Ch000391, %object
	.size	Ch000391, 40
Ch000391:
	.byte	0
	.byte	4
	.byte	4
	.byte	2
	.byte	4
	.byte	8
	.byte	8
	.byte	6
	.byte	5
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	2
	.byte	0
	.byte	-1
	.byte	-128
	.byte	1
	.byte	-128
	.byte	3
	.byte	-128
	.byte	3
	.byte	0
	.byte	7
	.byte	0
	.byte	6
	.byte	0
	.byte	14
	.byte	0
	.byte	12
	.byte	0
	.byte	28
	.byte	0
	.byte	24
	.byte	0
	.byte	56
	.byte	0
	.byte	48
	.byte	0
	.section	.rodata.Ch000549,"a",%progbits
	.type	Ch000549, %object
	.size	Ch000549, 20
Ch000549:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-8
	.byte	0
	.byte	-52
	.byte	0
	.byte	-52
	.byte	120
	.section	.rodata.Ch000161,"a",%progbits
	.type	Ch000161, %object
	.size	Ch000161, 20
Ch000161:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	36
	.byte	-8
	.byte	-52
	.byte	-8
	.byte	-52
	.byte	-8
	.section	.rodata.Ch000399,"a",%progbits
	.type	Ch000399, %object
	.size	Ch000399, 26
Ch000399:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	8
	.byte	6
	.byte	6
	.byte	8
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	90
	.byte	23
	.byte	-1
	.byte	-128
	.byte	-64
	.byte	0
	.byte	-2
	.byte	0
	.byte	-64
	.byte	0
	.byte	-1
	.byte	-128
	.section	.rodata.Ch000244,"a",%progbits
	.type	Ch000244, %object
	.size	Ch000244, 20
Ch000244:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	0
	.byte	9
	.byte	-8
	.byte	0
	.byte	16
	.byte	32
	.byte	-52
	.byte	120
	.section	.rodata.Ch000206,"a",%progbits
	.type	Ch000206, %object
	.size	Ch000206, 18
Ch000206:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	12
	.byte	112
	.byte	-40
	.byte	112
	.section	.rodata.Ch000461,"a",%progbits
	.type	Ch000461, %object
	.size	Ch000461, 21
Ch000461:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-52
	.byte	0
	.byte	120
	.byte	-52
	.byte	120
	.byte	-52
	.byte	120
	.section	.rodata.Ch000300,"a",%progbits
	.type	Ch000300, %object
	.size	Ch000300, 25
Ch000300:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	4
	.byte	2
	.byte	-61
	.byte	-29
	.byte	-13
	.byte	-45
	.byte	-37
	.byte	-53
	.byte	-49
	.byte	-57
	.byte	-61
	.section	.rodata.Ch000463,"a",%progbits
	.type	Ch000463, %object
	.size	Ch000463, 18
Ch000463:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	6
	.byte	7
	.byte	90
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000397,"a",%progbits
	.type	Ch000397, %object
	.size	Ch000397, 34
Ch000397:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	4
	.byte	7
	.byte	8
	.byte	8
	.byte	7
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	-32
	.byte	1
	.byte	62
	.byte	0
	.byte	127
	.byte	0
	.byte	-29
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-64
	.byte	0
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	127
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000465,"a",%progbits
	.type	Ch000465, %object
	.size	Ch000465, 20
Ch000465:
	.byte	1
	.byte	1
	.byte	0
	.byte	1
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	0
	.byte	4
	.byte	6
	.byte	5
	.byte	0
	.byte	48
	.byte	96
	.byte	-64
	.byte	96
	.byte	48
	.section	.rodata.Ch000301,"a",%progbits
	.type	Ch000301, %object
	.size	Ch000301, 21
Ch000301:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	6
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-8
	.byte	1
	.byte	60
	.byte	102
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000190,"a",%progbits
	.type	Ch000190, %object
	.size	Ch000190, 16
Ch000190:
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	8
	.byte	1
	.byte	0
	.byte	-2
	.section	.rodata.Ch000191,"a",%progbits
	.type	Ch000191, %object
	.size	Ch000191, 17
Ch000191:
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	3
	.byte	2
	.byte	2
	.byte	0
	.byte	-64
	.byte	96
	.section	.rodata.Ch000192,"a",%progbits
	.type	Ch000192, %object
	.size	Ch000192, 20
Ch000192:
	.byte	2
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	0
	.byte	112
	.byte	24
	.byte	120
	.byte	-40
	.byte	120
	.section	.rodata.Ch000335,"a",%progbits
	.type	Ch000335, %object
	.size	Ch000335, 22
Ch000335:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	5
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	0
	.byte	7
	.byte	6
	.byte	11
	.byte	56
	.byte	6
	.byte	62
	.byte	110
	.byte	-58
	.byte	110
	.byte	62
	.byte	6
	.section	.rodata.Ch000231,"a",%progbits
	.type	Ch000231, %object
	.size	Ch000231, 20
Ch000231:
	.byte	2
	.byte	0
	.byte	2
	.byte	0
	.byte	2
	.byte	3
	.byte	5
	.byte	3
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	18
	.byte	48
	.byte	-4
	.byte	48
	.byte	0
	.byte	-4
	.section	.rodata.Ch000232,"a",%progbits
	.type	Ch000232, %object
	.size	Ch000232, 20
Ch000232:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	0
	.byte	3
	.byte	2
	.byte	5
	.byte	0
	.byte	-64
	.byte	32
	.byte	64
	.byte	-128
	.byte	-32
	.section	.rodata.Ch000336,"a",%progbits
	.type	Ch000336, %object
	.size	Ch000336, 19
Ch000336:
	.byte	3
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	3
	.byte	5
	.byte	3
	.byte	2
	.byte	2
	.byte	1
	.byte	5
	.byte	6
	.byte	8
	.byte	-16
	.byte	-40
	.byte	-16
	.byte	-32
	.byte	-64
	.section	.rodata.Ch000234,"a",%progbits
	.type	Ch000234, %object
	.size	Ch000234, 18
Ch000234:
	.byte	1
	.byte	1
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	5
	.byte	24
	.byte	64
	.byte	-64
	.byte	64
	.section	.rodata.Ch000198,"a",%progbits
	.type	Ch000198, %object
	.size	Ch000198, 20
Ch000198:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	0
	.byte	5
	.byte	4
	.byte	7
	.byte	12
	.byte	112
	.byte	-40
	.byte	120
	.byte	24
	.byte	112
	.section	.rodata.Ch000199,"a",%progbits
	.type	Ch000199, %object
	.size	Ch000199, 18
Ch000199:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	1
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	114
	.byte	-64
	.byte	-16
	.byte	-40
	.section	.rodata.Ch000237,"a",%progbits
	.type	Ch000237, %object
	.size	Ch000237, 25
Ch000237:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	0
	.byte	9
	.byte	0
	.byte	0
	.byte	-64
	.byte	32
	.byte	68
	.byte	44
	.byte	-40
	.byte	54
	.byte	106
	.byte	-50
	.byte	-126
	.section	.rodata.Ch000238,"a",%progbits
	.type	Ch000238, %object
	.size	Ch000238, 22
Ch000238:
	.byte	2
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	3
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	48
	.byte	0
	.byte	48
	.byte	96
	.byte	-64
	.byte	-52
	.byte	120
	.section	.rodata.Ch000239,"a",%progbits
	.type	Ch000239, %object
	.size	Ch000239, 24
Ch000239:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	0
	.byte	9
	.byte	0
	.byte	1
	.byte	16
	.byte	32
	.byte	0
	.byte	48
	.byte	120
	.byte	-52
	.byte	-4
	.byte	-52
	.section	.rodata.Ch000000,"a",%progbits
	.type	Ch000000, %object
	.size	Ch000000, 16
Ch000000:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	0
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.rodata.Ch000001,"a",%progbits
	.type	Ch000001, %object
	.size	Ch000001, 18
Ch000001:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	7
	.byte	30
	.byte	-128
	.byte	0
	.byte	-128
	.section	.rodata.Ch000002,"a",%progbits
	.type	Ch000002, %object
	.size	Ch000002, 16
Ch000002:
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	3
	.byte	2
	.byte	3
	.byte	6
	.byte	-96
	.section	.rodata.Ch000003,"a",%progbits
	.type	Ch000003, %object
	.size	Ch000003, 20
Ch000003:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	0
	.byte	80
	.byte	-8
	.byte	80
	.byte	-8
	.byte	80
	.section	.rodata.Ch000004,"a",%progbits
	.type	Ch000004, %object
	.size	Ch000004, 25
Ch000004:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	1
	.byte	9
	.byte	0
	.byte	0
	.byte	32
	.byte	112
	.byte	-88
	.byte	-96
	.byte	112
	.byte	40
	.byte	-88
	.byte	112
	.byte	32
	.section	.rodata.Ch000005,"a",%progbits
	.type	Ch000005, %object
	.size	Ch000005, 21
Ch000005:
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	2
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	3
	.byte	6
	.byte	0
	.byte	-56
	.byte	-40
	.byte	48
	.byte	96
	.byte	-40
	.byte	-104
	.section	.rodata.Ch000006,"a",%progbits
	.type	Ch000006, %object
	.size	Ch000006, 21
Ch000006:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	4
	.byte	96
	.byte	-112
	.byte	96
	.byte	-88
	.byte	-112
	.byte	104
	.section	.rodata.Ch000007,"a",%progbits
	.type	Ch000007, %object
	.size	Ch000007, 16
Ch000007:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	3
	.byte	6
	.byte	-128
	.section	.rodata.Ch000008,"a",%progbits
	.type	Ch000008, %object
	.size	Ch000008, 20
Ch000008:
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	7
	.byte	24
	.byte	32
	.byte	64
	.byte	-128
	.byte	64
	.byte	32
	.section	.rodata.Ch000009,"a",%progbits
	.type	Ch000009, %object
	.size	Ch000009, 20
Ch000009:
	.byte	0
	.byte	1
	.byte	1
	.byte	0
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	0
	.byte	3
	.byte	2
	.byte	7
	.byte	24
	.byte	-128
	.byte	64
	.byte	32
	.byte	64
	.byte	-128
	.section	.rodata.Ch000467,"a",%progbits
	.type	Ch000467, %object
	.size	Ch000467, 20
Ch000467:
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	3
	.byte	1
	.byte	4
	.byte	6
	.byte	5
	.byte	0
	.byte	-64
	.byte	96
	.byte	48
	.byte	96
	.byte	-64
	.section	.rodata.Ch000428,"a",%progbits
	.type	Ch000428, %object
	.size	Ch000428, 20
Ch000428:
	.byte	4
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	6
	.byte	4
	.byte	3
	.byte	4
	.byte	2
	.byte	5
	.byte	7
	.byte	10
	.byte	-16
	.byte	3
	.byte	-40
	.byte	-16
	.byte	-32
	.byte	-64
	.section	.rodata.Ch000516,"a",%progbits
	.type	Ch000516, %object
	.size	Ch000516, 18
Ch000516:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	6
	.byte	7
	.byte	60
	.byte	120
	.byte	-52
	.byte	120
	.section	.rodata.Ch000464,"a",%progbits
	.type	Ch000464, %object
	.size	Ch000464, 19
Ch000464:
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	1
	.byte	3
	.byte	6
	.byte	8
	.byte	90
	.byte	96
	.byte	0
	.byte	96
	.byte	-64
	.section	.rodata.Ch000517,"a",%progbits
	.type	Ch000517, %object
	.size	Ch000517, 20
Ch000517:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	5
	.byte	5
	.byte	5
	.byte	1
	.byte	0
	.byte	6
	.byte	6
	.byte	9
	.byte	60
	.byte	1
	.byte	-8
	.byte	-52
	.byte	-8
	.byte	-64
	.section	.rodata.Ch000536,"a",%progbits
	.type	Ch000536, %object
	.size	Ch000536, 24
Ch000536:
	.byte	2
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	1
	.byte	12
	.byte	80
	.byte	9
	.byte	24
	.byte	48
	.byte	0
	.byte	48
	.byte	120
	.byte	-52
	.byte	-4
	.byte	-52
	.section	.rodata.Ch000518,"a",%progbits
	.type	Ch000518, %object
	.size	Ch000518, 20
Ch000518:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	0
	.byte	6
	.byte	6
	.byte	9
	.byte	60
	.byte	1
	.byte	124
	.byte	-52
	.byte	124
	.byte	12
	.section	.rodata.Ch000469,"a",%progbits
	.type	Ch000469, %object
	.size	Ch000469, 29
Ch000469:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	4
	.byte	5
	.byte	7
	.byte	8
	.byte	6
	.byte	4
	.byte	0
	.byte	9
	.byte	6
	.byte	7
	.byte	0
	.byte	62
	.byte	0
	.byte	99
	.byte	0
	.byte	-35
	.byte	-128
	.byte	-43
	.byte	-128
	.byte	-33
	.byte	0
	.byte	96
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000530,"a",%progbits
	.type	Ch000530, %object
	.size	Ch000530, 21
Ch000530:
	.byte	1
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	3
	.byte	1
	.byte	4
	.byte	4
	.byte	9
	.byte	-52
	.byte	0
	.byte	-64
	.byte	96
	.byte	48
	.byte	96
	.byte	-64
	.section	.rodata.Ch000555,"a",%progbits
	.type	Ch000555, %object
	.size	Ch000555, 25
Ch000555:
	.byte	3
	.byte	3
	.byte	0
	.byte	1
	.byte	0
	.byte	4
	.byte	4
	.byte	7
	.byte	6
	.byte	7
	.byte	0
	.byte	8
	.byte	7
	.byte	9
	.byte	0
	.byte	0
	.byte	60
	.byte	66
	.byte	-103
	.byte	-91
	.byte	-95
	.byte	-91
	.byte	-103
	.byte	66
	.byte	60
	.section	.rodata.Ch000500,"a",%progbits
	.type	Ch000500, %object
	.size	Ch000500, 16
Ch000500:
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	5
	.byte	4
	.byte	0
	.byte	6
	.byte	13
	.byte	1
	.byte	0
	.byte	-4
	.section	.rodata.Font_ANSI11,"a",%progbits
	.align	2
	.type	Font_ANSI11, %object
	.size	Font_ANSI11, 24
Font_ANSI11:
	.byte	32
	.byte	1
	.byte	-4
	.byte	0
	.short	444
	.short	-1
	.short	665
	.byte	9
	.byte	17
	.byte	3
	.byte	6
	.byte	13
	.byte	15
	.byte	16
	.byte	15
	.byte	15
	.byte	8
	.byte	7
	.byte	2
	.byte	2
	.space	1
	.section	.rodata.Font_ANSI13,"a",%progbits
	.align	2
	.type	Font_ANSI13, %object
	.size	Font_ANSI13, 24
Font_ANSI13:
	.byte	32
	.byte	1
	.byte	-13
	.byte	0
	.short	666
	.short	-1
	.short	878
	.byte	11
	.byte	21
	.byte	4
	.byte	7
	.byte	16
	.byte	18
	.byte	20
	.byte	18
	.byte	19
	.byte	9
	.byte	9
	.byte	1
	.byte	2
	.space	1
	.section	.rodata.Ch000501,"a",%progbits
	.type	Ch000501, %object
	.size	Ch000501, 17
Ch000501:
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	-64
	.byte	96
	.section	.rodata.Ch000280,"a",%progbits
	.type	Ch000280, %object
	.size	Ch000280, 18
Ch000280:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	6
	.byte	8
	.byte	-70
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000050,"a",%progbits
	.type	Ch000050, %object
	.size	Ch000050, 21
Ch000050:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	4
	.byte	-16
	.byte	-120
	.byte	-16
	.byte	-96
	.byte	-112
	.byte	-120
	.section	.rodata.Ch000282,"a",%progbits
	.type	Ch000282, %object
	.size	Ch000282, 25
Ch000282:
	.byte	3
	.byte	2
	.byte	1
	.byte	3
	.byte	3
	.byte	6
	.byte	5
	.byte	4
	.byte	6
	.byte	4
	.byte	1
	.byte	6
	.byte	4
	.byte	9
	.byte	0
	.byte	0
	.byte	12
	.byte	24
	.byte	48
	.byte	96
	.byte	-64
	.byte	96
	.byte	48
	.byte	24
	.byte	12
	.section	.rodata.Ch000284,"a",%progbits
	.type	Ch000284, %object
	.size	Ch000284, 25
Ch000284:
	.byte	1
	.byte	2
	.byte	3
	.byte	1
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	4
	.byte	4
	.byte	1
	.byte	6
	.byte	4
	.byte	9
	.byte	0
	.byte	0
	.byte	-64
	.byte	96
	.byte	48
	.byte	24
	.byte	12
	.byte	24
	.byte	48
	.byte	96
	.byte	-64
	.section	.rodata.Ch000470,"a",%progbits
	.type	Ch000470, %object
	.size	Ch000470, 21
Ch000470:
	.byte	2
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	42
	.byte	1
	.byte	48
	.byte	120
	.byte	-52
	.byte	-4
	.byte	-52
	.section	.rodata.Ch000471,"a",%progbits
	.type	Ch000471, %object
	.size	Ch000471, 21
Ch000471:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-52
	.byte	0
	.byte	-8
	.byte	-52
	.byte	-8
	.byte	-52
	.byte	-8
	.section	.rodata.Ch000286,"a",%progbits
	.type	Ch000286, %object
	.size	Ch000286, 23
Ch000286:
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	4
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	5
	.byte	9
	.byte	36
	.byte	0
	.byte	126
	.byte	-61
	.byte	-33
	.byte	-37
	.byte	-34
	.byte	-64
	.byte	126
	.section	.rodata.Ch000473,"a",%progbits
	.type	Ch000473, %object
	.size	Ch000473, 21
Ch000473:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	120
	.byte	0
	.byte	-16
	.byte	-40
	.byte	-52
	.byte	-40
	.byte	-16
	.section	.rodata.Ch000320,"a",%progbits
	.type	Ch000320, %object
	.size	Ch000320, 22
Ch000320:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	1
	.byte	5
	.byte	6
	.byte	5
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	11
	.byte	-58
	.byte	1
	.byte	-64
	.byte	-8
	.byte	-20
	.byte	-58
	.byte	-20
	.byte	-8
	.section	.rodata.Ch000475,"a",%progbits
	.type	Ch000475, %object
	.size	Ch000475, 20
Ch000475:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	1
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	9
	.byte	-52
	.byte	1
	.byte	-8
	.byte	-64
	.byte	-16
	.byte	-64
	.section	.rodata.Ch000345,"a",%progbits
	.type	Ch000345, %object
	.size	Ch000345, 23
Ch000345:
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	6
	.byte	4
	.byte	4
	.byte	6
	.byte	4
	.byte	1
	.byte	6
	.byte	3
	.byte	11
	.byte	12
	.byte	3
	.byte	28
	.byte	48
	.byte	96
	.byte	-64
	.byte	96
	.byte	48
	.byte	28
	.section	.rodata.Ch000477,"a",%progbits
	.type	Ch000477, %object
	.size	Ch000477, 19
Ch000477:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-50
	.byte	1
	.byte	-52
	.byte	-4
	.byte	-52
	.section	.rodata.Ch000322,"a",%progbits
	.type	Ch000322, %object
	.size	Ch000322, 22
Ch000322:
	.byte	5
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	11
	.byte	-58
	.byte	1
	.byte	6
	.byte	62
	.byte	110
	.byte	-58
	.byte	110
	.byte	62
	.section	.rodata.Ch000479,"a",%progbits
	.type	Ch000479, %object
	.size	Ch000479, 19
Ch000479:
	.byte	4
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-66
	.byte	0
	.byte	12
	.byte	-52
	.byte	120
	.section	.rodata.Ch000323,"a",%progbits
	.type	Ch000323, %object
	.size	Ch000323, 22
Ch000323:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	5
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	6
	.byte	8
	.byte	8
	.byte	56
	.byte	108
	.byte	-58
	.byte	-2
	.byte	-64
	.byte	102
	.byte	60
	.section	.rodata.Ch000241,"a",%progbits
	.type	Ch000241, %object
	.size	Ch000241, 22
Ch000241:
	.byte	1
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	1
	.byte	4
	.byte	0
	.byte	9
	.byte	-32
	.byte	0
	.byte	32
	.byte	64
	.byte	0
	.byte	-16
	.byte	96
	.byte	-16
	.section	.rodata.Ch000242,"a",%progbits
	.type	Ch000242, %object
	.size	Ch000242, 24
Ch000242:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	0
	.byte	9
	.byte	64
	.byte	0
	.byte	100
	.byte	-104
	.byte	0
	.byte	-52
	.byte	-20
	.byte	-4
	.byte	-36
	.byte	-52
	.section	.rodata.Ch000243,"a",%progbits
	.type	Ch000243, %object
	.size	Ch000243, 21
Ch000243:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	0
	.byte	9
	.byte	-16
	.byte	0
	.byte	16
	.byte	32
	.byte	120
	.byte	-52
	.byte	120
	.section	.rodata.Ch000347,"a",%progbits
	.type	Ch000347, %object
	.size	Ch000347, 23
Ch000347:
	.byte	0
	.byte	2
	.byte	2
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	11
	.byte	12
	.byte	3
	.byte	-32
	.byte	48
	.byte	24
	.byte	12
	.byte	24
	.byte	48
	.byte	-32
	.section	.rodata.Ch000245,"a",%progbits
	.type	Ch000245, %object
	.size	Ch000245, 20
Ch000245:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	0
	.byte	9
	.byte	-8
	.byte	0
	.byte	-52
	.byte	0
	.byte	-52
	.byte	120
	.section	.rodata.Ch000246,"a",%progbits
	.type	Ch000246, %object
	.size	Ch000246, 23
Ch000246:
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	1
	.byte	8
	.byte	0
	.byte	48
	.byte	96
	.byte	0
	.byte	112
	.byte	24
	.byte	120
	.byte	-40
	.byte	120
	.section	.rodata.Ch000247,"a",%progbits
	.type	Ch000247, %object
	.size	Ch000247, 23
Ch000247:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	1
	.byte	8
	.byte	0
	.byte	48
	.byte	96
	.byte	0
	.byte	112
	.byte	-40
	.byte	-8
	.byte	-64
	.byte	112
	.section	.rodata.Ch000248,"a",%progbits
	.type	Ch000248, %object
	.size	Ch000248, 19
Ch000248:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	1
	.byte	8
	.byte	-16
	.byte	96
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000249,"a",%progbits
	.type	Ch000249, %object
	.size	Ch000249, 20
Ch000249:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	1
	.byte	8
	.byte	-32
	.byte	104
	.byte	-80
	.byte	0
	.byte	-16
	.byte	-40
	.section	.rodata.Ch000010,"a",%progbits
	.type	Ch000010, %object
	.size	Ch000010, 22
Ch000010:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	0
	.byte	32
	.byte	-88
	.byte	112
	.byte	-8
	.byte	112
	.byte	-88
	.byte	32
	.section	.rodata.Ch000011,"a",%progbits
	.type	Ch000011, %object
	.size	Ch000011, 18
Ch000011:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	3
	.byte	5
	.byte	18
	.byte	32
	.byte	-8
	.byte	32
	.section	.rodata.Ch000012,"a",%progbits
	.type	Ch000012, %object
	.size	Ch000012, 17
Ch000012:
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	7
	.byte	3
	.byte	2
	.byte	64
	.byte	-128
	.section	.rodata.Ch000013,"a",%progbits
	.type	Ch000013, %object
	.size	Ch000013, 16
Ch000013:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	5
	.byte	1
	.byte	0
	.byte	-8
	.section	.rodata.Ch000014,"a",%progbits
	.type	Ch000014, %object
	.size	Ch000014, 16
Ch000014:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	8
	.byte	1
	.byte	0
	.byte	-128
	.section	.rodata.Ch000015,"a",%progbits
	.type	Ch000015, %object
	.size	Ch000015, 18
Ch000015:
	.byte	3
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	3
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	1
	.byte	3
	.byte	2
	.byte	7
	.byte	90
	.byte	32
	.byte	64
	.byte	-128
	.section	.rodata.Ch000016,"a",%progbits
	.type	Ch000016, %object
	.size	Ch000016, 20
Ch000016:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	24
	.byte	32
	.byte	80
	.byte	-120
	.byte	80
	.byte	32
	.section	.rodata.Ch000017,"a",%progbits
	.type	Ch000017, %object
	.size	Ch000017, 18
Ch000017:
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	7
	.byte	120
	.byte	64
	.byte	-64
	.byte	64
	.section	.rodata.Ch000018,"a",%progbits
	.type	Ch000018, %object
	.size	Ch000018, 22
Ch000018:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	0
	.byte	112
	.byte	-120
	.byte	8
	.byte	48
	.byte	64
	.byte	-128
	.byte	-8
	.section	.rodata.Ch000019,"a",%progbits
	.type	Ch000019, %object
	.size	Ch000019, 22
Ch000019:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	0
	.byte	112
	.byte	-120
	.byte	8
	.byte	48
	.byte	8
	.byte	-120
	.byte	112
	.section	.rodata.Ch000579,"a",%progbits
	.type	Ch000579, %object
	.size	Ch000579, 25
Ch000579:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	6
	.byte	7
	.byte	9
	.byte	0
	.byte	0
	.byte	12
	.byte	24
	.byte	48
	.byte	24
	.byte	48
	.byte	96
	.byte	48
	.byte	96
	.byte	-64
	.section	.rodata.Ch000480,"a",%progbits
	.type	Ch000480, %object
	.size	Ch000480, 25
Ch000480:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	5
	.byte	4
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	4
	.byte	9
	.byte	0
	.byte	0
	.byte	-58
	.byte	-52
	.byte	-40
	.byte	-16
	.byte	-32
	.byte	-16
	.byte	-40
	.byte	-52
	.byte	-58
	.section	.rodata.Ch000481,"a",%progbits
	.type	Ch000481, %object
	.size	Ch000481, 18
Ch000481:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	9
	.byte	-2
	.byte	0
	.byte	-64
	.byte	-8
	.section	.rodata.Ch000482,"a",%progbits
	.type	Ch000482, %object
	.size	Ch000482, 21
Ch000482:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	4
	.byte	9
	.byte	84
	.byte	1
	.byte	-61
	.byte	-25
	.byte	-1
	.byte	-37
	.byte	-61
	.section	.rodata.Ch000483,"a",%progbits
	.type	Ch000483, %object
	.size	Ch000483, 21
Ch000483:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	74
	.byte	1
	.byte	-52
	.byte	-20
	.byte	-4
	.byte	-36
	.byte	-52
	.section	.rodata.GuiFont_FontList,"a",%progbits
	.align	2
	.type	GuiFont_FontList, %object
	.size	GuiFont_FontList, 28
GuiFont_FontList:
	.word	Font_ANSI7
	.word	Font_ANSI7
	.word	Font_ANSI7Bold
	.word	Font_ANSI11
	.word	Font_ANSI13
	.word	Font_Unicode9_15
	.word	Font_Icon16x16
	.section	.rodata.Ch000485,"a",%progbits
	.type	Ch000485, %object
	.size	Ch000485, 20
Ch000485:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	4
	.byte	1
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-52
	.byte	1
	.byte	-8
	.byte	-52
	.byte	-8
	.byte	-64
	.section	.rodata.Ch000177,"a",%progbits
	.type	Ch000177, %object
	.size	Ch000177, 20
Ch000177:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	68
	.byte	-8
	.byte	-52
	.byte	-8
	.byte	-40
	.byte	-52
	.section	.rodata.Ch000487,"a",%progbits
	.type	Ch000487, %object
	.size	Ch000487, 21
Ch000487:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-116
	.byte	1
	.byte	-8
	.byte	-52
	.byte	-8
	.byte	-40
	.byte	-52
	.section	.rodata.Ch000357,"a",%progbits
	.type	Ch000357, %object
	.size	Ch000357, 21
Ch000357:
	.byte	3
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.byte	4
	.byte	7
	.byte	4
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	4
	.byte	9
	.byte	102
	.byte	0
	.byte	24
	.byte	-1
	.byte	24
	.byte	0
	.byte	-1
	.section	.rodata.Ch000489,"a",%progbits
	.type	Ch000489, %object
	.size	Ch000489, 18
Ch000489:
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-4
	.byte	1
	.byte	-4
	.byte	48
	.section	.rodata.Ch000250,"a",%progbits
	.type	Ch000250, %object
	.size	Ch000250, 21
Ch000250:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	1
	.byte	8
	.byte	96
	.byte	48
	.byte	96
	.byte	0
	.byte	112
	.byte	-40
	.byte	112
	.section	.rodata.Ch000251,"a",%progbits
	.type	Ch000251, %object
	.size	Ch000251, 20
Ch000251:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	1
	.byte	8
	.byte	112
	.byte	48
	.byte	96
	.byte	0
	.byte	-40
	.byte	120
	.section	.rodata.Ch000252,"a",%progbits
	.type	Ch000252, %object
	.size	Ch000252, 19
Ch000252:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	56
	.byte	80
	.byte	0
	.byte	-40
	.byte	120
	.section	.rodata.Ch000253,"a",%progbits
	.type	Ch000253, %object
	.size	Ch000253, 17
Ch000253:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	0
	.byte	7
	.byte	0
	.byte	11
	.byte	-2
	.byte	7
	.byte	-2
	.section	.rodata.Ch000254,"a",%progbits
	.type	Ch000254, %object
	.size	Ch000254, 16
Ch000254:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.rodata.Ch000255,"a",%progbits
	.type	Ch000255, %object
	.size	Ch000255, 21
Ch000255:
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	5
	.byte	5
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	4
	.byte	3
	.byte	11
	.byte	-36
	.byte	4
	.byte	96
	.byte	-16
	.byte	96
	.byte	0
	.byte	96
	.section	.rodata.Ch000256,"a",%progbits
	.type	Ch000256, %object
	.size	Ch000256, 17
Ch000256:
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	5
	.byte	5
	.byte	3
	.byte	3
	.byte	3
	.byte	1
	.byte	5
	.byte	3
	.byte	4
	.byte	6
	.byte	-40
	.byte	80
	.section	.rodata.Ch000257,"a",%progbits
	.type	Ch000257, %object
	.size	Ch000257, 21
Ch000257:
	.byte	3
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	4
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	5
	.byte	9
	.byte	50
	.byte	1
	.byte	102
	.byte	-1
	.byte	102
	.byte	-1
	.byte	102
	.section	.rodata.Ch000258,"a",%progbits
	.type	Ch000258, %object
	.size	Ch000258, 27
Ch000258:
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	4
	.byte	0
	.byte	7
	.byte	2
	.byte	13
	.byte	32
	.byte	1
	.byte	40
	.byte	124
	.byte	-18
	.byte	-22
	.byte	-24
	.byte	124
	.byte	46
	.byte	-82
	.byte	-18
	.byte	124
	.byte	40
	.section	.rodata.Ch000259,"a",%progbits
	.type	Ch000259, %object
	.size	Ch000259, 27
Ch000259:
	.byte	0
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.byte	7
	.byte	6
	.byte	6
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	0
	.byte	0
	.byte	99
	.byte	-109
	.byte	-106
	.byte	108
	.byte	12
	.byte	24
	.byte	48
	.byte	54
	.byte	105
	.byte	-55
	.byte	-58
	.section	.rodata.Ch000020,"a",%progbits
	.type	Ch000020, %object
	.size	Ch000020, 21
Ch000020:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	64
	.byte	16
	.byte	48
	.byte	80
	.byte	-112
	.byte	-8
	.byte	16
	.section	.rodata.Ch000021,"a",%progbits
	.type	Ch000021, %object
	.size	Ch000021, 21
Ch000021:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	16
	.byte	-8
	.byte	-128
	.byte	-16
	.byte	8
	.byte	-120
	.byte	112
	.section	.rodata.Ch000022,"a",%progbits
	.type	Ch000022, %object
	.size	Ch000022, 21
Ch000022:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	32
	.byte	48
	.byte	64
	.byte	-128
	.byte	-16
	.byte	-120
	.byte	112
	.section	.rodata.Ch000023,"a",%progbits
	.type	Ch000023, %object
	.size	Ch000023, 21
Ch000023:
	.byte	0
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	1
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	4
	.byte	-8
	.byte	8
	.byte	16
	.byte	32
	.byte	64
	.byte	-128
	.section	.rodata.Ch000024,"a",%progbits
	.type	Ch000024, %object
	.size	Ch000024, 20
Ch000024:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	36
	.byte	112
	.byte	-120
	.byte	112
	.byte	-120
	.byte	112
	.section	.rodata.Ch000025,"a",%progbits
	.type	Ch000025, %object
	.size	Ch000025, 21
Ch000025:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	4
	.byte	112
	.byte	-120
	.byte	120
	.byte	8
	.byte	16
	.byte	96
	.section	.rodata.Ch000026,"a",%progbits
	.type	Ch000026, %object
	.size	Ch000026, 18
Ch000026:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	4
	.byte	5
	.byte	12
	.byte	-128
	.byte	0
	.byte	-128
	.section	.rodata.Ch000027,"a",%progbits
	.type	Ch000027, %object
	.size	Ch000027, 19
Ch000027:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	4
	.byte	6
	.byte	12
	.byte	64
	.byte	0
	.byte	64
	.byte	-128
	.section	.rodata.Ch000028,"a",%progbits
	.type	Ch000028, %object
	.size	Ch000028, 22
Ch000028:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	2
	.byte	7
	.byte	0
	.byte	16
	.byte	32
	.byte	64
	.byte	-128
	.byte	64
	.byte	32
	.byte	16
	.section	.rodata.Ch000029,"a",%progbits
	.type	Ch000029, %object
	.size	Ch000029, 18
Ch000029:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	3
	.byte	0
	.byte	-8
	.byte	0
	.byte	-8
	.section	.rodata.Ch000425,"a",%progbits
	.type	Ch000425, %object
	.size	Ch000425, 20
Ch000425:
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	4
	.byte	6
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	7
	.byte	10
	.byte	-16
	.byte	3
	.byte	-36
	.byte	-2
	.byte	-25
	.byte	-61
	.section	.rodata.Ch000426,"a",%progbits
	.type	Ch000426, %object
	.size	Ch000426, 23
Ch000426:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	4
	.byte	6
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	7
	.byte	10
	.byte	112
	.byte	0
	.byte	60
	.byte	126
	.byte	-25
	.byte	-61
	.byte	-25
	.byte	126
	.byte	60
	.section	.rodata.Ch000427,"a",%progbits
	.type	Ch000427, %object
	.size	Ch000427, 24
Ch000427:
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	6
	.byte	7
	.byte	6
	.byte	1
	.byte	0
	.byte	8
	.byte	7
	.byte	14
	.byte	112
	.byte	56
	.byte	-36
	.byte	-2
	.byte	-25
	.byte	-61
	.byte	-25
	.byte	-2
	.byte	-36
	.byte	-64
	.section	.rodata.Ch000573,"a",%progbits
	.type	Ch000573, %object
	.size	Ch000573, 19
Ch000573:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	5
	.byte	9
	.byte	7
	.byte	14
	.byte	112
	.byte	-8
	.byte	112
	.byte	32
	.section	.rodata.Ch000163,"a",%progbits
	.type	Ch000163, %object
	.size	Ch000163, 20
Ch000163:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	24
	.byte	-16
	.byte	-40
	.byte	-52
	.byte	-40
	.byte	-16
	.section	.rodata.Ch000060,"a",%progbits
	.type	Ch000060, %object
	.size	Ch000060, 18
Ch000060:
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	1
	.byte	3
	.byte	2
	.byte	7
	.byte	90
	.byte	-128
	.byte	64
	.byte	32
	.section	.rodata.Font_ANSI7,"a",%progbits
	.align	2
	.type	Font_ANSI7, %object
	.size	Font_ANSI7, 24
Font_ANSI7:
	.byte	32
	.byte	1
	.byte	-4
	.byte	0
	.short	0
	.short	-1
	.short	221
	.byte	6
	.byte	11
	.byte	2
	.byte	4
	.byte	8
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	5
	.byte	5
	.byte	1
	.byte	1
	.space	1
	.section	.rodata.Ch000394,"a",%progbits
	.type	Ch000394, %object
	.size	Ch000394, 23
Ch000394:
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	4
	.byte	5
	.byte	4
	.byte	2
	.byte	4
	.byte	6
	.byte	11
	.byte	100
	.byte	2
	.byte	96
	.byte	-16
	.byte	96
	.byte	0
	.byte	96
	.byte	-16
	.byte	96
	.section	.rodata.Ch000293,"a",%progbits
	.type	Ch000293, %object
	.size	Ch000293, 23
Ch000293:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	7
	.byte	1
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-104
	.byte	1
	.byte	62
	.byte	99
	.byte	-64
	.byte	-49
	.byte	-61
	.byte	103
	.byte	61
	.section	.rodata.Ch000294,"a",%progbits
	.type	Ch000294, %object
	.size	Ch000294, 19
Ch000294:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-98
	.byte	7
	.byte	-61
	.byte	-1
	.byte	-61
	.section	.rodata.Ch000295,"a",%progbits
	.type	Ch000295, %object
	.size	Ch000295, 19
Ch000295:
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	5
	.byte	4
	.byte	4
	.byte	5
	.byte	4
	.byte	2
	.byte	4
	.byte	3
	.byte	11
	.byte	-4
	.byte	3
	.byte	-16
	.byte	96
	.byte	-16
	.section	.rodata.Ch000168,"a",%progbits
	.type	Ch000168, %object
	.size	Ch000168, 18
Ch000168:
	.byte	1
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	1
	.byte	4
	.byte	2
	.byte	7
	.byte	60
	.byte	-16
	.byte	96
	.byte	-16
	.section	.rodata.Ch000296,"a",%progbits
	.type	Ch000296, %object
	.size	Ch000296, 19
Ch000296:
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	11
	.byte	-2
	.byte	2
	.byte	12
	.byte	-52
	.byte	120
	.section	.rodata.Ch000493,"a",%progbits
	.type	Ch000493, %object
	.size	Ch000493, 21
Ch000493:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-122
	.byte	1
	.byte	-52
	.byte	120
	.byte	48
	.byte	120
	.byte	-52
	.section	.rodata.Ch000297,"a",%progbits
	.type	Ch000297, %object
	.size	Ch000297, 27
Ch000297:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	7
	.byte	5
	.byte	5
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	0
	.byte	0
	.byte	-61
	.byte	-58
	.byte	-52
	.byte	-40
	.byte	-16
	.byte	-32
	.byte	-16
	.byte	-40
	.byte	-52
	.byte	-58
	.byte	-61
	.section	.rodata.Ch000201,"a",%progbits
	.type	Ch000201, %object
	.size	Ch000201, 20
Ch000201:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	4
	.byte	2
	.byte	9
	.byte	-8
	.byte	0
	.byte	48
	.byte	0
	.byte	48
	.byte	-32
	.section	.rodata.Ch000496,"a",%progbits
	.type	Ch000496, %object
	.size	Ch000496, 19
Ch000496:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	3
	.byte	11
	.byte	-4
	.byte	3
	.byte	-32
	.byte	-64
	.byte	-32
	.section	.rodata.Ch000497,"a",%progbits
	.type	Ch000497, %object
	.size	Ch000497, 21
Ch000497:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	10
	.byte	-86
	.byte	2
	.byte	-64
	.byte	96
	.byte	48
	.byte	24
	.byte	12
	.section	.rodata.Ch000202,"a",%progbits
	.type	Ch000202, %object
	.size	Ch000202, 21
Ch000202:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	1
	.byte	4
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	2
	.byte	-64
	.byte	-40
	.byte	-16
	.byte	-32
	.byte	-16
	.byte	-40
	.section	.rodata.Ch000499,"a",%progbits
	.type	Ch000499, %object
	.size	Ch000499, 18
Ch000499:
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	5
	.byte	5
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	3
	.byte	0
	.byte	48
	.byte	120
	.byte	-52
	.section	.rodata.Ch000102,"a",%progbits
	.type	Ch000102, %object
	.size	Ch000102, 21
Ch000102:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	8
	.byte	120
	.byte	-124
	.byte	-76
	.byte	-84
	.byte	-124
	.byte	120
	.section	.rodata.Ch000261,"a",%progbits
	.type	Ch000261, %object
	.size	Ch000261, 16
Ch000261:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	4
	.byte	14
	.byte	-64
	.section	.rodata.Ch000103,"a",%progbits
	.type	Ch000103, %object
	.size	Ch000103, 18
Ch000103:
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	4
	.byte	2
	.byte	4
	.byte	4
	.byte	96
	.byte	-112
	.byte	96
	.section	.rodata.Ch000263,"a",%progbits
	.type	Ch000263, %object
	.size	Ch000263, 21
Ch000263:
	.byte	0
	.byte	1
	.byte	1
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	0
	.byte	4
	.byte	3
	.byte	11
	.byte	-8
	.byte	1
	.byte	-64
	.byte	96
	.byte	48
	.byte	96
	.byte	-64
	.section	.rodata.Ch000104,"a",%progbits
	.type	Ch000104, %object
	.size	Ch000104, 20
Ch000104:
	.byte	2
	.byte	0
	.byte	2
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	2
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	18
	.byte	32
	.byte	-8
	.byte	32
	.byte	0
	.byte	-8
	.section	.rodata.Ch000265,"a",%progbits
	.type	Ch000265, %object
	.size	Ch000265, 18
Ch000265:
	.byte	3
	.byte	1
	.byte	0
	.byte	3
	.byte	3
	.byte	4
	.byte	6
	.byte	7
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	5
	.byte	7
	.byte	102
	.byte	24
	.byte	-1
	.byte	24
	.section	.rodata.Ch000105,"a",%progbits
	.type	Ch000105, %object
	.size	Ch000105, 20
Ch000105:
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	1
	.byte	3
	.byte	2
	.byte	5
	.byte	0
	.byte	-64
	.byte	32
	.byte	64
	.byte	-128
	.byte	-32
	.section	.rodata.Ch000267,"a",%progbits
	.type	Ch000267, %object
	.size	Ch000267, 16
Ch000267:
	.byte	3
	.byte	1
	.byte	0
	.byte	3
	.byte	3
	.byte	4
	.byte	6
	.byte	7
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	8
	.byte	1
	.byte	0
	.byte	-1
	.section	.rodata.Ch000106,"a",%progbits
	.type	Ch000106, %object
	.size	Ch000106, 20
Ch000106:
	.byte	1
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	1
	.byte	3
	.byte	2
	.byte	5
	.byte	0
	.byte	-64
	.byte	32
	.byte	64
	.byte	32
	.byte	-64
	.section	.rodata.Ch000269,"a",%progbits
	.type	Ch000269, %object
	.size	Ch000269, 25
Ch000269:
	.byte	5
	.byte	3
	.byte	2
	.byte	1
	.byte	3
	.byte	6
	.byte	5
	.byte	4
	.byte	2
	.byte	4
	.byte	1
	.byte	6
	.byte	3
	.byte	11
	.byte	2
	.byte	4
	.byte	12
	.byte	28
	.byte	24
	.byte	56
	.byte	48
	.byte	112
	.byte	96
	.byte	-32
	.byte	-64
	.section	.rodata.Ch000030,"a",%progbits
	.type	Ch000030, %object
	.size	Ch000030, 22
Ch000030:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	1
	.byte	4
	.byte	2
	.byte	7
	.byte	0
	.byte	-128
	.byte	64
	.byte	32
	.byte	16
	.byte	32
	.byte	64
	.byte	-128
	.section	.rodata.Ch000031,"a",%progbits
	.type	Ch000031, %object
	.size	Ch000031, 22
Ch000031:
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	0
	.byte	112
	.byte	-120
	.byte	8
	.byte	16
	.byte	32
	.byte	0
	.byte	32
	.section	.rodata.Ch000032,"a",%progbits
	.type	Ch000032, %object
	.size	Ch000032, 21
Ch000032:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	8
	.byte	112
	.byte	-120
	.byte	-72
	.byte	-80
	.byte	-128
	.byte	120
	.section	.rodata.Ch000033,"a",%progbits
	.type	Ch000033, %object
	.size	Ch000033, 20
Ch000033:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	68
	.byte	32
	.byte	80
	.byte	-120
	.byte	-8
	.byte	-120
	.section	.rodata.Ch000034,"a",%progbits
	.type	Ch000034, %object
	.size	Ch000034, 20
Ch000034:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	36
	.byte	-16
	.byte	-120
	.byte	-16
	.byte	-120
	.byte	-16
	.section	.rodata.Ch000035,"a",%progbits
	.type	Ch000035, %object
	.size	Ch000035, 20
Ch000035:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	24
	.byte	112
	.byte	-120
	.byte	-128
	.byte	-120
	.byte	112
	.section	.rodata.Ch000036,"a",%progbits
	.type	Ch000036, %object
	.size	Ch000036, 18
Ch000036:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	60
	.byte	-16
	.byte	-120
	.byte	-16
	.section	.rodata.Ch000037,"a",%progbits
	.type	Ch000037, %object
	.size	Ch000037, 20
Ch000037:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	36
	.byte	-8
	.byte	-128
	.byte	-16
	.byte	-128
	.byte	-8
	.section	.rodata.Ch000038,"a",%progbits
	.type	Ch000038, %object
	.size	Ch000038, 19
Ch000038:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	100
	.byte	-8
	.byte	-128
	.byte	-16
	.byte	-128
	.section	.rodata.Ch000039,"a",%progbits
	.type	Ch000039, %object
	.size	Ch000039, 21
Ch000039:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	8
	.byte	112
	.byte	-120
	.byte	-128
	.byte	-104
	.byte	-120
	.byte	120
	.section	.rodata.Ch000208,"a",%progbits
	.type	Ch000208, %object
	.size	Ch000208, 19
Ch000208:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	0
	.byte	5
	.byte	4
	.byte	7
	.byte	76
	.byte	120
	.byte	-40
	.byte	120
	.byte	24
	.section	.rodata.Ch000410,"a",%progbits
	.type	Ch000410, %object
	.size	Ch000410, 18
Ch000410:
	.byte	0
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	7
	.byte	5
	.byte	5
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	4
	.byte	13
	.byte	-6
	.byte	31
	.byte	-1
	.byte	24
	.section	.rodata.Ch000563,"a",%progbits
	.type	Ch000563, %object
	.size	Ch000563, 34
Ch000563:
	.byte	8
	.byte	8
	.byte	1
	.byte	0
	.byte	0
	.byte	8
	.byte	8
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	9
	.byte	7
	.byte	9
	.byte	0
	.byte	0
	.byte	-1
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-86
	.byte	-128
	.byte	-108
	.byte	-128
	.byte	-86
	.byte	-128
	.byte	-108
	.byte	-128
	.byte	-86
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-1
	.byte	-128
	.section	.rodata.Ch000439,"a",%progbits
	.type	Ch000439, %object
	.size	Ch000439, 17
Ch000439:
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	3
	.byte	3
	.byte	2
	.byte	-40
	.byte	80
	.section	.rodata.Ch000433,"a",%progbits
	.type	Ch000433, %object
	.size	Ch000433, 28
Ch000433:
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	8
	.byte	8
	.byte	8
	.byte	4
	.byte	0
	.byte	9
	.byte	7
	.byte	10
	.byte	38
	.byte	2
	.byte	-63
	.byte	-128
	.byte	-55
	.byte	-128
	.byte	-35
	.byte	-128
	.byte	-1
	.byte	-128
	.byte	119
	.byte	0
	.byte	99
	.byte	0
	.section	.rodata.Ch000466,"a",%progbits
	.type	Ch000466, %object
	.size	Ch000466, 18
Ch000466:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	7
	.byte	3
	.byte	0
	.byte	-8
	.byte	0
	.byte	-8
	.section	.rodata.Ch000502,"a",%progbits
	.type	Ch000502, %object
	.size	Ch000502, 21
Ch000502:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	6
	.byte	7
	.byte	32
	.byte	120
	.byte	-52
	.byte	12
	.byte	124
	.byte	-52
	.byte	124
	.section	.rodata.Ch000564,"a",%progbits
	.type	Ch000564, %object
	.size	Ch000564, 20
Ch000564:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	0
	.byte	8
	.byte	8
	.byte	8
	.byte	56
	.byte	60
	.byte	66
	.byte	-127
	.byte	66
	.byte	60
	.section	.rodata.Ch000532,"a",%progbits
	.type	Ch000532, %object
	.size	Ch000532, 19
Ch000532:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	4
	.byte	9
	.byte	-14
	.byte	1
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000498,"a",%progbits
	.type	Ch000498, %object
	.size	Ch000498, 19
Ch000498:
	.byte	1
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	1
	.byte	3
	.byte	3
	.byte	11
	.byte	-4
	.byte	3
	.byte	-32
	.byte	96
	.byte	-32
	.section	.rodata.Ch000565,"a",%progbits
	.type	Ch000565, %object
	.size	Ch000565, 20
Ch000565:
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	3
	.byte	6
	.byte	6
	.byte	0
	.byte	8
	.byte	8
	.byte	8
	.byte	56
	.byte	60
	.byte	126
	.byte	-1
	.byte	126
	.byte	60
	.section	.rodata.Ch000448,"a",%progbits
	.type	Ch000448, %object
	.size	Ch000448, 18
Ch000448:
	.byte	2
	.byte	1
	.byte	0
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	6
	.byte	5
	.byte	18
	.byte	48
	.byte	-4
	.byte	48
	.section	.rodata.Ch000534,"a",%progbits
	.type	Ch000534, %object
	.size	Ch000534, 36
Ch000534:
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	4
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	4
	.byte	0
	.byte	9
	.byte	3
	.byte	10
	.byte	0
	.byte	0
	.byte	-32
	.byte	0
	.byte	49
	.byte	0
	.byte	99
	.byte	0
	.byte	54
	.byte	0
	.byte	-20
	.byte	0
	.byte	27
	.byte	-128
	.byte	55
	.byte	-128
	.byte	109
	.byte	-128
	.byte	79
	.byte	-128
	.byte	1
	.byte	-128
	.section	.rodata.Ch000311,"a",%progbits
	.type	Ch000311, %object
	.size	Ch000311, 20
Ch000311:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	7
	.byte	6
	.byte	5
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-86
	.byte	7
	.byte	-61
	.byte	102
	.byte	60
	.byte	24
	.section	.rodata.Ch000540,"a",%progbits
	.type	Ch000540, %object
	.size	Ch000540, 22
Ch000540:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	1
	.byte	12
	.byte	-32
	.byte	7
	.byte	24
	.byte	48
	.byte	0
	.byte	120
	.byte	-52
	.byte	120
	.section	.rodata.Ch000450,"a",%progbits
	.type	Ch000450, %object
	.size	Ch000450, 16
Ch000450:
	.byte	2
	.byte	1
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	8
	.byte	2
	.byte	2
	.byte	-8
	.section	.rodata.Ch000542,"a",%progbits
	.type	Ch000542, %object
	.size	Ch000542, 20
Ch000542:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	11
	.byte	-8
	.byte	3
	.byte	-52
	.byte	0
	.byte	-52
	.byte	120
	.section	.rodata.Ch000220,"a",%progbits
	.type	Ch000220, %object
	.size	Ch000220, 20
Ch000220:
	.byte	0
	.byte	1
	.byte	1
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	0
	.byte	4
	.byte	2
	.byte	7
	.byte	36
	.byte	-64
	.byte	96
	.byte	48
	.byte	96
	.byte	-64
	.section	.rodata.Ch000544,"a",%progbits
	.type	Ch000544, %object
	.size	Ch000544, 25
Ch000544:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	10
	.byte	32
	.byte	0
	.byte	24
	.byte	48
	.byte	0
	.byte	120
	.byte	-52
	.byte	-4
	.byte	-64
	.byte	-52
	.byte	120
	.section	.rodata.Ch000221,"a",%progbits
	.type	Ch000221, %object
	.size	Ch000221, 17
Ch000221:
	.byte	3
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	3
	.byte	6
	.byte	5
	.byte	3
	.byte	3
	.byte	0
	.byte	7
	.byte	4
	.byte	2
	.byte	0
	.byte	118
	.byte	-36
	.section	.rodata.GuiFont_LanguageIndex,"a",%progbits
	.align	1
	.type	GuiFont_LanguageIndex, %object
	.size	GuiFont_LanguageIndex, 4
GuiFont_LanguageIndex:
	.short	0
	.short	1
	.section	.rodata.Ch000222,"a",%progbits
	.type	Ch000222, %object
	.size	Ch000222, 19
Ch000222:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	2
	.byte	7
	.byte	44
	.byte	124
	.byte	-58
	.byte	108
	.byte	-18
	.section	.rodata.Ch000271,"a",%progbits
	.type	Ch000271, %object
	.size	Ch000271, 20
Ch000271:
	.byte	2
	.byte	1
	.byte	3
	.byte	3
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	1
	.byte	4
	.byte	3
	.byte	11
	.byte	-16
	.byte	7
	.byte	48
	.byte	112
	.byte	-16
	.byte	48
	.section	.rodata.Ch000223,"a",%progbits
	.type	Ch000223, %object
	.size	Ch000223, 20
Ch000223:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	5
	.byte	5
	.byte	2
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	5
	.byte	0
	.byte	32
	.byte	96
	.byte	-4
	.byte	96
	.byte	32
	.section	.rodata.Ch000273,"a",%progbits
	.type	Ch000273, %object
	.size	Ch000273, 27
Ch000273:
	.byte	1
	.byte	0
	.byte	4
	.byte	1
	.byte	3
	.byte	6
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	0
	.byte	0
	.byte	60
	.byte	102
	.byte	-61
	.byte	3
	.byte	6
	.byte	12
	.byte	6
	.byte	3
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000224,"a",%progbits
	.type	Ch000224, %object
	.size	Ch000224, 20
Ch000224:
	.byte	3
	.byte	0
	.byte	0
	.byte	3
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	5
	.byte	0
	.byte	16
	.byte	24
	.byte	-4
	.byte	24
	.byte	16
	.section	.rodata.Ch000275,"a",%progbits
	.type	Ch000275, %object
	.size	Ch000275, 24
Ch000275:
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	3
	.byte	7
	.byte	6
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-60
	.byte	0
	.byte	-1
	.byte	-64
	.byte	-4
	.byte	-26
	.byte	3
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000225,"a",%progbits
	.type	Ch000225, %object
	.size	Ch000225, 18
Ch000225:
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	1
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	96
	.byte	-16
	.byte	96
	.section	.rodata.Ch000358,"a",%progbits
	.type	Ch000358, %object
	.size	Ch000358, 21
Ch000358:
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	3
	.byte	7
	.byte	32
	.byte	112
	.byte	-40
	.byte	24
	.byte	112
	.byte	-64
	.byte	-8
	.section	.rodata.Ch000226,"a",%progbits
	.type	Ch000226, %object
	.size	Ch000226, 18
Ch000226:
	.byte	0
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	3
	.byte	0
	.byte	7
	.byte	2
	.byte	5
	.byte	24
	.byte	-14
	.byte	94
	.byte	82
	.section	.rodata.Ch000279,"a",%progbits
	.type	Ch000279, %object
	.size	Ch000279, 25
Ch000279:
	.byte	1
	.byte	0
	.byte	1
	.byte	1
	.byte	3
	.byte	6
	.byte	7
	.byte	7
	.byte	5
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	24
	.byte	0
	.byte	60
	.byte	102
	.byte	-61
	.byte	103
	.byte	63
	.byte	3
	.byte	6
	.byte	12
	.byte	120
	.section	.rodata.Ch000040,"a",%progbits
	.type	Ch000040, %object
	.size	Ch000040, 18
Ch000040:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	102
	.byte	-120
	.byte	-8
	.byte	-120
	.section	.rodata.Ch000041,"a",%progbits
	.type	Ch000041, %object
	.size	Ch000041, 18
Ch000041:
	.byte	1
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	1
	.byte	3
	.byte	2
	.byte	7
	.byte	60
	.byte	-32
	.byte	64
	.byte	-32
	.section	.rodata.Ch000042,"a",%progbits
	.type	Ch000042, %object
	.size	Ch000042, 18
Ch000042:
	.byte	2
	.byte	2
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	2
	.byte	7
	.byte	30
	.byte	16
	.byte	-112
	.byte	96
	.section	.rodata.Ch000043,"a",%progbits
	.type	Ch000043, %object
	.size	Ch000043, 22
Ch000043:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	0
	.byte	-120
	.byte	-112
	.byte	-96
	.byte	-64
	.byte	-96
	.byte	-112
	.byte	-120
	.section	.rodata.Ch000044,"a",%progbits
	.type	Ch000044, %object
	.size	Ch000044, 17
Ch000044:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	2
	.byte	7
	.byte	62
	.byte	-128
	.byte	-16
	.section	.rodata.Ch000045,"a",%progbits
	.type	Ch000045, %object
	.size	Ch000045, 19
Ch000045:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	104
	.byte	-120
	.byte	-40
	.byte	-88
	.byte	-120
	.section	.rodata.Ch000046,"a",%progbits
	.type	Ch000046, %object
	.size	Ch000046, 20
Ch000046:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	36
	.byte	-120
	.byte	-56
	.byte	-88
	.byte	-104
	.byte	-120
	.section	.rodata.Ch000047,"a",%progbits
	.type	Ch000047, %object
	.size	Ch000047, 18
Ch000047:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	60
	.byte	112
	.byte	-120
	.byte	112
	.section	.rodata.Ch000048,"a",%progbits
	.type	Ch000048, %object
	.size	Ch000048, 19
Ch000048:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	100
	.byte	-16
	.byte	-120
	.byte	-16
	.byte	-128
	.section	.rodata.Ch000049,"a",%progbits
	.type	Ch000049, %object
	.size	Ch000049, 21
Ch000049:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	0
	.byte	6
	.byte	2
	.byte	8
	.byte	12
	.byte	112
	.byte	-120
	.byte	-88
	.byte	-104
	.byte	112
	.byte	12
	.section	.rodata.Ch000317,"a",%progbits
	.type	Ch000317, %object
	.size	Ch000317, 17
Ch000317:
	.byte	4
	.byte	4
	.byte	4
	.byte	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	8
	.byte	4
	.byte	0
	.byte	9
	.byte	13
	.byte	1
	.byte	0
	.byte	-1
	.byte	-128
	.section	.rodata.Ch000423,"a",%progbits
	.type	Ch000423, %object
	.size	Ch000423, 17
Ch000423:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	4
	.byte	13
	.byte	-2
	.byte	31
	.byte	-64
	.section	.rodata.Ch000476,"a",%progbits
	.type	Ch000476, %object
	.size	Ch000476, 22
Ch000476:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-56
	.byte	0
	.byte	120
	.byte	-52
	.byte	-64
	.byte	-36
	.byte	-52
	.byte	120
	.section	.rodata.Ch000457,"a",%progbits
	.type	Ch000457, %object
	.size	Ch000457, 23
Ch000457:
	.byte	2
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	5
	.byte	5
	.byte	6
	.byte	5
	.byte	3
	.byte	0
	.byte	7
	.byte	4
	.byte	9
	.byte	32
	.byte	1
	.byte	12
	.byte	28
	.byte	60
	.byte	108
	.byte	-52
	.byte	-2
	.byte	12
	.section	.rodata.Ch000424,"a",%progbits
	.type	Ch000424, %object
	.size	Ch000424, 22
Ch000424:
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	9
	.byte	9
	.byte	9
	.byte	5
	.byte	0
	.byte	10
	.byte	7
	.byte	10
	.byte	-8
	.byte	3
	.byte	-37
	.byte	-128
	.byte	-1
	.byte	-64
	.byte	-52
	.byte	-64
	.section	.rodata.Ch000458,"a",%progbits
	.type	Ch000458, %object
	.size	Ch000458, 22
Ch000458:
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	5
	.byte	4
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	100
	.byte	0
	.byte	-4
	.byte	-64
	.byte	-8
	.byte	12
	.byte	-52
	.byte	120
	.section	.rodata.Ch000522,"a",%progbits
	.type	Ch000522, %object
	.size	Ch000522, 17
Ch000522:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	6
	.byte	7
	.byte	62
	.byte	-52
	.byte	120
	.section	.rodata.GuiFont_ChPtrList,"a",%progbits
	.align	2
	.type	GuiFont_ChPtrList, %object
	.size	GuiFont_ChPtrList, 4664
GuiFont_ChPtrList:
	.word	Ch000000
	.word	Ch000001
	.word	Ch000002
	.word	Ch000003
	.word	Ch000004
	.word	Ch000005
	.word	Ch000006
	.word	Ch000007
	.word	Ch000008
	.word	Ch000009
	.word	Ch000010
	.word	Ch000011
	.word	Ch000012
	.word	Ch000013
	.word	Ch000014
	.word	Ch000015
	.word	Ch000016
	.word	Ch000017
	.word	Ch000018
	.word	Ch000019
	.word	Ch000020
	.word	Ch000021
	.word	Ch000022
	.word	Ch000023
	.word	Ch000024
	.word	Ch000025
	.word	Ch000026
	.word	Ch000027
	.word	Ch000028
	.word	Ch000029
	.word	Ch000030
	.word	Ch000031
	.word	Ch000032
	.word	Ch000033
	.word	Ch000034
	.word	Ch000035
	.word	Ch000036
	.word	Ch000037
	.word	Ch000038
	.word	Ch000039
	.word	Ch000040
	.word	Ch000041
	.word	Ch000042
	.word	Ch000043
	.word	Ch000044
	.word	Ch000045
	.word	Ch000046
	.word	Ch000047
	.word	Ch000048
	.word	Ch000049
	.word	Ch000050
	.word	Ch000051
	.word	Ch000052
	.word	Ch000053
	.word	Ch000054
	.word	Ch000055
	.word	Ch000056
	.word	Ch000057
	.word	Ch000058
	.word	Ch000059
	.word	Ch000060
	.word	Ch000061
	.word	Ch000062
	.word	Ch000063
	.word	Ch000064
	.word	Ch000065
	.word	Ch000066
	.word	Ch000067
	.word	Ch000068
	.word	Ch000069
	.word	Ch000070
	.word	Ch000071
	.word	Ch000072
	.word	Ch000073
	.word	Ch000074
	.word	Ch000075
	.word	Ch000076
	.word	Ch000077
	.word	Ch000078
	.word	Ch000079
	.word	Ch000080
	.word	Ch000081
	.word	Ch000082
	.word	Ch000083
	.word	Ch000084
	.word	Ch000085
	.word	Ch000086
	.word	Ch000087
	.word	Ch000088
	.word	Ch000089
	.word	Ch000090
	.word	Ch000091
	.word	Ch000092
	.word	Ch000093
	.word	Ch000094
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000095
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000096
	.word	Ch000097
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000098
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000099
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000100
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000101
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000102
	.word	Ch000126
	.word	Ch000103
	.word	Ch000104
	.word	Ch000105
	.word	Ch000106
	.word	Ch000126
	.word	Ch000107
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000108
	.word	Ch000126
	.word	Ch000126
	.word	Ch000109
	.word	Ch000110
	.word	Ch000111
	.word	Ch000112
	.word	Ch000126
	.word	Ch000113
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000114
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000115
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000116
	.word	Ch000126
	.word	Ch000117
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000118
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000119
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000120
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000121
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000122
	.word	Ch000126
	.word	Ch000123
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000126
	.word	Ch000124
	.word	Ch000126
	.word	Ch000125
	.word	Ch000126
	.word	Ch000127
	.word	Ch000128
	.word	Ch000129
	.word	Ch000130
	.word	Ch000131
	.word	Ch000132
	.word	Ch000133
	.word	Ch000134
	.word	Ch000135
	.word	Ch000136
	.word	Ch000137
	.word	Ch000138
	.word	Ch000139
	.word	Ch000140
	.word	Ch000141
	.word	Ch000142
	.word	Ch000143
	.word	Ch000144
	.word	Ch000145
	.word	Ch000146
	.word	Ch000147
	.word	Ch000148
	.word	Ch000149
	.word	Ch000150
	.word	Ch000151
	.word	Ch000152
	.word	Ch000153
	.word	Ch000154
	.word	Ch000155
	.word	Ch000156
	.word	Ch000157
	.word	Ch000158
	.word	Ch000159
	.word	Ch000160
	.word	Ch000161
	.word	Ch000162
	.word	Ch000163
	.word	Ch000164
	.word	Ch000165
	.word	Ch000166
	.word	Ch000167
	.word	Ch000168
	.word	Ch000169
	.word	Ch000170
	.word	Ch000171
	.word	Ch000172
	.word	Ch000173
	.word	Ch000174
	.word	Ch000175
	.word	Ch000176
	.word	Ch000177
	.word	Ch000178
	.word	Ch000179
	.word	Ch000180
	.word	Ch000181
	.word	Ch000182
	.word	Ch000183
	.word	Ch000184
	.word	Ch000185
	.word	Ch000186
	.word	Ch000187
	.word	Ch000188
	.word	Ch000189
	.word	Ch000190
	.word	Ch000191
	.word	Ch000192
	.word	Ch000193
	.word	Ch000194
	.word	Ch000195
	.word	Ch000196
	.word	Ch000197
	.word	Ch000198
	.word	Ch000199
	.word	Ch000200
	.word	Ch000201
	.word	Ch000202
	.word	Ch000203
	.word	Ch000204
	.word	Ch000205
	.word	Ch000206
	.word	Ch000207
	.word	Ch000208
	.word	Ch000209
	.word	Ch000210
	.word	Ch000211
	.word	Ch000212
	.word	Ch000213
	.word	Ch000214
	.word	Ch000215
	.word	Ch000216
	.word	Ch000217
	.word	Ch000218
	.word	Ch000219
	.word	Ch000220
	.word	Ch000221
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000222
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000223
	.word	Ch000224
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000225
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000226
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000227
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000228
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000229
	.word	Ch000253
	.word	Ch000230
	.word	Ch000231
	.word	Ch000232
	.word	Ch000233
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000234
	.word	Ch000253
	.word	Ch000253
	.word	Ch000235
	.word	Ch000236
	.word	Ch000237
	.word	Ch000238
	.word	Ch000253
	.word	Ch000239
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000240
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000241
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000242
	.word	Ch000253
	.word	Ch000243
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000244
	.word	Ch000253
	.word	Ch000245
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000246
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000247
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000248
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000249
	.word	Ch000253
	.word	Ch000250
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000253
	.word	Ch000251
	.word	Ch000253
	.word	Ch000252
	.word	Ch000253
	.word	Ch000254
	.word	Ch000255
	.word	Ch000256
	.word	Ch000257
	.word	Ch000258
	.word	Ch000259
	.word	Ch000260
	.word	Ch000261
	.word	Ch000262
	.word	Ch000263
	.word	Ch000264
	.word	Ch000265
	.word	Ch000266
	.word	Ch000267
	.word	Ch000268
	.word	Ch000269
	.word	Ch000270
	.word	Ch000271
	.word	Ch000272
	.word	Ch000273
	.word	Ch000274
	.word	Ch000275
	.word	Ch000276
	.word	Ch000277
	.word	Ch000278
	.word	Ch000279
	.word	Ch000280
	.word	Ch000281
	.word	Ch000282
	.word	Ch000283
	.word	Ch000284
	.word	Ch000285
	.word	Ch000286
	.word	Ch000287
	.word	Ch000288
	.word	Ch000289
	.word	Ch000290
	.word	Ch000291
	.word	Ch000292
	.word	Ch000293
	.word	Ch000294
	.word	Ch000295
	.word	Ch000296
	.word	Ch000297
	.word	Ch000298
	.word	Ch000299
	.word	Ch000300
	.word	Ch000301
	.word	Ch000302
	.word	Ch000303
	.word	Ch000304
	.word	Ch000305
	.word	Ch000306
	.word	Ch000307
	.word	Ch000308
	.word	Ch000309
	.word	Ch000310
	.word	Ch000311
	.word	Ch000312
	.word	Ch000313
	.word	Ch000314
	.word	Ch000315
	.word	Ch000316
	.word	Ch000317
	.word	Ch000318
	.word	Ch000319
	.word	Ch000320
	.word	Ch000321
	.word	Ch000322
	.word	Ch000323
	.word	Ch000324
	.word	Ch000325
	.word	Ch000326
	.word	Ch000327
	.word	Ch000328
	.word	Ch000329
	.word	Ch000330
	.word	Ch000331
	.word	Ch000332
	.word	Ch000333
	.word	Ch000334
	.word	Ch000335
	.word	Ch000336
	.word	Ch000337
	.word	Ch000338
	.word	Ch000339
	.word	Ch000340
	.word	Ch000341
	.word	Ch000342
	.word	Ch000343
	.word	Ch000344
	.word	Ch000345
	.word	Ch000346
	.word	Ch000347
	.word	Ch000348
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000349
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000350
	.word	Ch000351
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000352
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000353
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000354
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000355
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000356
	.word	Ch000378
	.word	Ch000378
	.word	Ch000357
	.word	Ch000358
	.word	Ch000359
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000360
	.word	Ch000378
	.word	Ch000378
	.word	Ch000361
	.word	Ch000362
	.word	Ch000363
	.word	Ch000364
	.word	Ch000378
	.word	Ch000365
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000366
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000367
	.word	Ch000378
	.word	Ch000368
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000369
	.word	Ch000378
	.word	Ch000370
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000371
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000372
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000373
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000374
	.word	Ch000378
	.word	Ch000375
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000378
	.word	Ch000376
	.word	Ch000378
	.word	Ch000377
	.word	Ch000378
	.word	Ch000379
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000380
	.word	Ch000381
	.word	Ch000382
	.word	Ch000383
	.word	Ch000436
	.word	Ch000384
	.word	Ch000385
	.word	Ch000386
	.word	Ch000387
	.word	Ch000388
	.word	Ch000389
	.word	Ch000390
	.word	Ch000391
	.word	Ch000392
	.word	Ch000393
	.word	Ch000394
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000395
	.word	Ch000396
	.word	Ch000397
	.word	Ch000398
	.word	Ch000399
	.word	Ch000400
	.word	Ch000436
	.word	Ch000436
	.word	Ch000401
	.word	Ch000402
	.word	Ch000436
	.word	Ch000403
	.word	Ch000404
	.word	Ch000405
	.word	Ch000406
	.word	Ch000407
	.word	Ch000436
	.word	Ch000408
	.word	Ch000409
	.word	Ch000410
	.word	Ch000411
	.word	Ch000412
	.word	Ch000413
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000414
	.word	Ch000415
	.word	Ch000416
	.word	Ch000417
	.word	Ch000418
	.word	Ch000436
	.word	Ch000419
	.word	Ch000420
	.word	Ch000421
	.word	Ch000436
	.word	Ch000422
	.word	Ch000423
	.word	Ch000424
	.word	Ch000425
	.word	Ch000426
	.word	Ch000427
	.word	Ch000436
	.word	Ch000428
	.word	Ch000429
	.word	Ch000430
	.word	Ch000431
	.word	Ch000432
	.word	Ch000433
	.word	Ch000436
	.word	Ch000434
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000436
	.word	Ch000435
	.word	Ch000436
	.word	Ch000437
	.word	Ch000438
	.word	Ch000439
	.word	Ch000440
	.word	Ch000441
	.word	Ch000442
	.word	Ch000443
	.word	Ch000444
	.word	Ch000445
	.word	Ch000446
	.word	Ch000447
	.word	Ch000448
	.word	Ch000449
	.word	Ch000450
	.word	Ch000451
	.word	Ch000452
	.word	Ch000453
	.word	Ch000454
	.word	Ch000455
	.word	Ch000456
	.word	Ch000457
	.word	Ch000458
	.word	Ch000459
	.word	Ch000460
	.word	Ch000461
	.word	Ch000462
	.word	Ch000463
	.word	Ch000464
	.word	Ch000465
	.word	Ch000466
	.word	Ch000467
	.word	Ch000468
	.word	Ch000469
	.word	Ch000470
	.word	Ch000471
	.word	Ch000472
	.word	Ch000473
	.word	Ch000474
	.word	Ch000475
	.word	Ch000476
	.word	Ch000477
	.word	Ch000478
	.word	Ch000479
	.word	Ch000480
	.word	Ch000481
	.word	Ch000482
	.word	Ch000483
	.word	Ch000484
	.word	Ch000485
	.word	Ch000486
	.word	Ch000487
	.word	Ch000488
	.word	Ch000489
	.word	Ch000490
	.word	Ch000491
	.word	Ch000492
	.word	Ch000493
	.word	Ch000494
	.word	Ch000495
	.word	Ch000496
	.word	Ch000497
	.word	Ch000498
	.word	Ch000499
	.word	Ch000500
	.word	Ch000501
	.word	Ch000502
	.word	Ch000503
	.word	Ch000504
	.word	Ch000505
	.word	Ch000506
	.word	Ch000507
	.word	Ch000508
	.word	Ch000509
	.word	Ch000510
	.word	Ch000511
	.word	Ch000512
	.word	Ch000513
	.word	Ch000514
	.word	Ch000515
	.word	Ch000516
	.word	Ch000517
	.word	Ch000518
	.word	Ch000519
	.word	Ch000520
	.word	Ch000521
	.word	Ch000522
	.word	Ch000523
	.word	Ch000524
	.word	Ch000525
	.word	Ch000526
	.word	Ch000527
	.word	Ch000528
	.word	Ch000529
	.word	Ch000530
	.word	Ch000531
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000532
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000533
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000534
	.word	Ch000535
	.word	Ch000550
	.word	Ch000536
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000537
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000538
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000539
	.word	Ch000550
	.word	Ch000540
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000541
	.word	Ch000550
	.word	Ch000542
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000543
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000544
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000545
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000546
	.word	Ch000550
	.word	Ch000547
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000550
	.word	Ch000548
	.word	Ch000550
	.word	Ch000549
	.word	Ch000550
	.word	Ch000551
	.word	Ch000582
	.word	Ch000582
	.word	Ch000552
	.word	Ch000553
	.word	Ch000554
	.word	Ch000582
	.word	Ch000555
	.word	Ch000556
	.word	Ch000557
	.word	Ch000558
	.word	Ch000582
	.word	Ch000582
	.word	Ch000559
	.word	Ch000560
	.word	Ch000561
	.word	Ch000562
	.word	Ch000563
	.word	Ch000564
	.word	Ch000582
	.word	Ch000565
	.word	Ch000566
	.word	Ch000567
	.word	Ch000568
	.word	Ch000569
	.word	Ch000570
	.word	Ch000571
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000572
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000582
	.word	Ch000573
	.word	Ch000574
	.word	Ch000582
	.word	Ch000575
	.word	Ch000582
	.word	Ch000582
	.word	Ch000576
	.word	Ch000577
	.word	Ch000578
	.word	Ch000579
	.word	Ch000580
	.word	Ch000581
	.word	Ch000582
	.section	.rodata.Ch000456,"a",%progbits
	.type	Ch000456, %object
	.size	Ch000456, 23
Ch000456:
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	72
	.byte	0
	.byte	120
	.byte	-52
	.byte	12
	.byte	24
	.byte	12
	.byte	-52
	.byte	120
	.section	.rodata.Ch000548,"a",%progbits
	.type	Ch000548, %object
	.size	Ch000548, 21
Ch000548:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	10
	.byte	-16
	.byte	1
	.byte	24
	.byte	48
	.byte	0
	.byte	-52
	.byte	120
	.section	.rodata.Ch000543,"a",%progbits
	.type	Ch000543, %object
	.size	Ch000543, 25
Ch000543:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	10
	.byte	0
	.byte	1
	.byte	24
	.byte	48
	.byte	0
	.byte	120
	.byte	-52
	.byte	12
	.byte	124
	.byte	-52
	.byte	124
	.section	.rodata.Ch000541,"a",%progbits
	.type	Ch000541, %object
	.size	Ch000541, 21
Ch000541:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	1
	.byte	12
	.byte	-16
	.byte	7
	.byte	24
	.byte	48
	.byte	0
	.byte	-52
	.byte	120
	.section	.rodata.Font_Unicode9_15,"a",%progbits
	.align	2
	.type	Font_Unicode9_15, %object
	.size	Font_Unicode9_15, 24
Font_Unicode9_15:
	.byte	32
	.byte	1
	.byte	-4
	.byte	0
	.short	879
	.short	-1
	.short	1100
	.byte	16
	.byte	17
	.byte	4
	.byte	6
	.byte	12
	.byte	15
	.byte	16
	.byte	15
	.byte	16
	.byte	15
	.byte	5
	.byte	1
	.byte	2
	.space	1
	.section	.rodata.Ch000550,"a",%progbits
	.type	Ch000550, %object
	.size	Ch000550, 18
Ch000550:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	16
	.byte	0
	.byte	17
	.byte	-2
	.byte	-1
	.byte	-1
	.byte	-1
	.section	.rodata.Ch000551,"a",%progbits
	.type	Ch000551, %object
	.size	Ch000551, 34
Ch000551:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	16
	.byte	0
	.byte	16
	.byte	-32
	.byte	15
	.byte	15
	.byte	-16
	.byte	31
	.byte	-8
	.byte	48
	.byte	12
	.byte	96
	.byte	6
	.byte	-64
	.byte	3
	.byte	96
	.byte	6
	.byte	48
	.byte	12
	.byte	31
	.byte	-8
	.byte	15
	.byte	-16
	.section	.rodata.Ch000552,"a",%progbits
	.type	Ch000552, %object
	.size	Ch000552, 34
Ch000552:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	0
	.byte	14
	.byte	1
	.byte	15
	.byte	18
	.byte	71
	.byte	-1
	.byte	-4
	.byte	-64
	.byte	12
	.byte	-61
	.byte	12
	.byte	-64
	.byte	12
	.byte	-57
	.byte	12
	.byte	-61
	.byte	12
	.byte	-57
	.byte	-116
	.byte	-64
	.byte	12
	.byte	-1
	.byte	-4
	.section	.rodata.Ch000110,"a",%progbits
	.type	Ch000110, %object
	.size	Ch000110, 22
Ch000110:
	.byte	0
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	-56
	.byte	72
	.byte	80
	.byte	56
	.byte	68
	.byte	-120
	.byte	-100
	.section	.rodata.Ch000554,"a",%progbits
	.type	Ch000554, %object
	.size	Ch000554, 40
Ch000554:
	.byte	5
	.byte	5
	.byte	2
	.byte	0
	.byte	0
	.byte	6
	.byte	6
	.byte	9
	.byte	11
	.byte	11
	.byte	0
	.byte	16
	.byte	0
	.byte	16
	.byte	66
	.byte	-112
	.byte	-1
	.byte	-1
	.byte	-64
	.byte	3
	.byte	-61
	.byte	-61
	.byte	-58
	.byte	99
	.byte	-52
	.byte	51
	.byte	-64
	.byte	99
	.byte	-64
	.byte	-61
	.byte	-63
	.byte	-125
	.byte	-64
	.byte	3
	.byte	-63
	.byte	-125
	.byte	-64
	.byte	3
	.byte	-1
	.byte	-1
	.section	.rodata.Ch000111,"a",%progbits
	.type	Ch000111, %object
	.size	Ch000111, 25
Ch000111:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	0
	.byte	9
	.byte	0
	.byte	0
	.byte	-64
	.byte	32
	.byte	72
	.byte	40
	.byte	-48
	.byte	44
	.byte	84
	.byte	-100
	.byte	-124
	.section	.rodata.Ch000556,"a",%progbits
	.type	Ch000556, %object
	.size	Ch000556, 31
Ch000556:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	2
	.byte	10
	.byte	8
	.byte	8
	.byte	0
	.byte	63
	.byte	0
	.byte	64
	.byte	-128
	.byte	-98
	.byte	64
	.byte	33
	.byte	0
	.byte	12
	.byte	0
	.byte	18
	.byte	0
	.byte	0
	.byte	0
	.byte	12
	.byte	0
	.section	.rodata.Ch000112,"a",%progbits
	.type	Ch000112, %object
	.size	Ch000112, 22
Ch000112:
	.byte	2
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	0
	.byte	32
	.byte	0
	.byte	32
	.byte	64
	.byte	-128
	.byte	-120
	.byte	112
	.section	.rodata.Ch000281,"a",%progbits
	.type	Ch000281, %object
	.size	Ch000281, 20
Ch000281:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	4
	.byte	4
	.byte	2
	.byte	3
	.byte	6
	.byte	10
	.byte	-70
	.byte	1
	.byte	96
	.byte	0
	.byte	96
	.byte	-64
	.section	.rodata.Ch000113,"a",%progbits
	.type	Ch000113, %object
	.size	Ch000113, 22
Ch000113:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	0
	.byte	9
	.byte	20
	.byte	1
	.byte	16
	.byte	32
	.byte	80
	.byte	-120
	.byte	-8
	.byte	-120
	.section	.rodata.Ch000283,"a",%progbits
	.type	Ch000283, %object
	.size	Ch000283, 18
Ch000283:
	.byte	3
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	4
	.byte	7
	.byte	7
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	7
	.byte	4
	.byte	4
	.byte	-1
	.byte	0
	.byte	-1
	.section	.rodata.Ch000114,"a",%progbits
	.type	Ch000114, %object
	.size	Ch000114, 23
Ch000114:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	0
	.byte	9
	.byte	-112
	.byte	0
	.byte	16
	.byte	32
	.byte	-8
	.byte	-128
	.byte	-16
	.byte	-128
	.byte	-8
	.section	.rodata.Ch000285,"a",%progbits
	.type	Ch000285, %object
	.size	Ch000285, 24
Ch000285:
	.byte	1
	.byte	0
	.byte	3
	.byte	3
	.byte	3
	.byte	6
	.byte	7
	.byte	5
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-120
	.byte	4
	.byte	60
	.byte	102
	.byte	-61
	.byte	6
	.byte	12
	.byte	24
	.byte	0
	.byte	24
	.section	.rodata.Ch000115,"a",%progbits
	.type	Ch000115, %object
	.size	Ch000115, 21
Ch000115:
	.byte	1
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	1
	.byte	3
	.byte	0
	.byte	9
	.byte	-16
	.byte	0
	.byte	32
	.byte	64
	.byte	-32
	.byte	64
	.byte	-32
	.section	.rodata.Ch000287,"a",%progbits
	.type	Ch000287, %object
	.size	Ch000287, 21
Ch000287:
	.byte	2
	.byte	2
	.byte	1
	.byte	0
	.byte	3
	.byte	5
	.byte	5
	.byte	6
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-38
	.byte	4
	.byte	24
	.byte	60
	.byte	102
	.byte	-1
	.byte	-61
	.section	.rodata.Ch000116,"a",%progbits
	.type	Ch000116, %object
	.size	Ch000116, 23
Ch000116:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	0
	.byte	9
	.byte	-112
	.byte	0
	.byte	104
	.byte	-80
	.byte	-120
	.byte	-56
	.byte	-88
	.byte	-104
	.byte	-120
	.section	.rodata.Ch000289,"a",%progbits
	.type	Ch000289, %object
	.size	Ch000289, 21
Ch000289:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	7
	.byte	6
	.byte	6
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-8
	.byte	1
	.byte	62
	.byte	99
	.byte	-64
	.byte	99
	.byte	62
	.section	.rodata.Ch000348,"a",%progbits
	.type	Ch000348, %object
	.size	Ch000348, 18
Ch000348:
	.byte	3
	.byte	1
	.byte	0
	.byte	3
	.byte	3
	.byte	4
	.byte	7
	.byte	7
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	7
	.byte	3
	.byte	0
	.byte	112
	.byte	-37
	.byte	14
	.section	.rodata.Ch000051,"a",%progbits
	.type	Ch000051, %object
	.size	Ch000051, 22
Ch000051:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	0
	.byte	112
	.byte	-120
	.byte	-128
	.byte	112
	.byte	8
	.byte	-120
	.byte	112
	.section	.rodata.Ch000052,"a",%progbits
	.type	Ch000052, %object
	.size	Ch000052, 17
Ch000052:
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	124
	.byte	-8
	.byte	32
	.section	.rodata.Ch000053,"a",%progbits
	.type	Ch000053, %object
	.size	Ch000053, 17
Ch000053:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	62
	.byte	-120
	.byte	112
	.section	.rodata.Ch000054,"a",%progbits
	.type	Ch000054, %object
	.size	Ch000054, 18
Ch000054:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	46
	.byte	-120
	.byte	80
	.byte	32
	.section	.rodata.Ch000055,"a",%progbits
	.type	Ch000055, %object
	.size	Ch000055, 18
Ch000055:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	54
	.byte	-120
	.byte	-88
	.byte	80
	.section	.rodata.Ch000056,"a",%progbits
	.type	Ch000056, %object
	.size	Ch000056, 20
Ch000056:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	66
	.byte	-120
	.byte	80
	.byte	32
	.byte	80
	.byte	-120
	.section	.rodata.Ch000057,"a",%progbits
	.type	Ch000057, %object
	.size	Ch000057, 18
Ch000057:
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	114
	.byte	-120
	.byte	80
	.byte	32
	.section	.rodata.Ch000058,"a",%progbits
	.type	Ch000058, %object
	.size	Ch000058, 22
Ch000058:
	.byte	0
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	0
	.byte	-8
	.byte	8
	.byte	16
	.byte	32
	.byte	64
	.byte	-128
	.byte	-8
	.section	.rodata.Ch000059,"a",%progbits
	.type	Ch000059, %object
	.size	Ch000059, 18
Ch000059:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	2
	.byte	2
	.byte	4
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	7
	.byte	60
	.byte	-32
	.byte	-128
	.byte	-32
	.section	.rodata.Ch000442,"a",%progbits
	.type	Ch000442, %object
	.size	Ch000442, 31
Ch000442:
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	4
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	4
	.byte	0
	.byte	9
	.byte	5
	.byte	8
	.byte	0
	.byte	97
	.byte	-128
	.byte	-77
	.byte	0
	.byte	-74
	.byte	0
	.byte	108
	.byte	0
	.byte	27
	.byte	0
	.byte	54
	.byte	-128
	.byte	102
	.byte	-128
	.byte	-61
	.byte	0
	.section	.rodata.Ch000520,"a",%progbits
	.type	Ch000520, %object
	.size	Ch000520, 22
Ch000520:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	6
	.byte	7
	.byte	0
	.byte	120
	.byte	-52
	.byte	-64
	.byte	120
	.byte	12
	.byte	-52
	.byte	120
	.section	.rodata.Ch000167,"a",%progbits
	.type	Ch000167, %object
	.size	Ch000167, 18
Ch000167:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	102
	.byte	-52
	.byte	-4
	.byte	-52
	.section	.rodata.Ch000491,"a",%progbits
	.type	Ch000491, %object
	.size	Ch000491, 20
Ch000491:
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	6
	.byte	6
	.byte	5
	.byte	4
	.byte	3
	.byte	0
	.byte	7
	.byte	4
	.byte	9
	.byte	-74
	.byte	0
	.byte	-58
	.byte	108
	.byte	56
	.byte	16
	.section	.rodata.Ch000395,"a",%progbits
	.type	Ch000395, %object
	.size	Ch000395, 32
Ch000395:
	.byte	2
	.byte	2
	.byte	1
	.byte	0
	.byte	4
	.byte	6
	.byte	6
	.byte	7
	.byte	8
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	38
	.byte	24
	.byte	28
	.byte	0
	.byte	62
	.byte	0
	.byte	54
	.byte	0
	.byte	119
	.byte	0
	.byte	99
	.byte	0
	.byte	127
	.byte	0
	.byte	-1
	.byte	-128
	.byte	-63
	.byte	-128
	.section	.rodata.Ch000193,"a",%progbits
	.type	Ch000193, %object
	.size	Ch000193, 19
Ch000193:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	1
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	50
	.byte	-64
	.byte	-16
	.byte	-40
	.byte	-16
	.section	.rodata.Ch000194,"a",%progbits
	.type	Ch000194, %object
	.size	Ch000194, 20
Ch000194:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	0
	.byte	112
	.byte	-40
	.byte	-64
	.byte	-40
	.byte	112
	.section	.rodata.Ch000124,"a",%progbits
	.type	Ch000124, %object
	.size	Ch000124, 20
Ch000124:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	1
	.byte	8
	.byte	112
	.byte	32
	.byte	64
	.byte	0
	.byte	-112
	.byte	112
	.section	.rodata.Ch000195,"a",%progbits
	.type	Ch000195, %object
	.size	Ch000195, 19
Ch000195:
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	50
	.byte	24
	.byte	120
	.byte	-40
	.byte	120
	.section	.rodata.Ch000560,"a",%progbits
	.type	Ch000560, %object
	.size	Ch000560, 25
Ch000560:
	.byte	6
	.byte	5
	.byte	4
	.byte	4
	.byte	4
	.byte	8
	.byte	9
	.byte	8
	.byte	10
	.byte	10
	.byte	0
	.byte	8
	.byte	7
	.byte	9
	.byte	0
	.byte	0
	.byte	60
	.byte	66
	.byte	-121
	.byte	-115
	.byte	-103
	.byte	-79
	.byte	-31
	.byte	66
	.byte	60
	.section	.rodata.Ch000125,"a",%progbits
	.type	Ch000125, %object
	.size	Ch000125, 19
Ch000125:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	2
	.byte	7
	.byte	56
	.byte	-112
	.byte	0
	.byte	-112
	.byte	112
	.section	.rodata.Ch000211,"a",%progbits
	.type	Ch000211, %object
	.size	Ch000211, 19
Ch000211:
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	3
	.byte	4
	.byte	3
	.byte	1
	.byte	4
	.byte	2
	.byte	7
	.byte	50
	.byte	96
	.byte	-16
	.byte	96
	.byte	48
	.section	.rodata.Ch000230,"a",%progbits
	.type	Ch000230, %object
	.size	Ch000230, 18
Ch000230:
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	1
	.byte	4
	.byte	4
	.byte	112
	.byte	-40
	.byte	112
	.section	.rodata.Ch000495,"a",%progbits
	.type	Ch000495, %object
	.size	Ch000495, 23
Ch000495:
	.byte	0
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	3
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-124
	.byte	0
	.byte	-4
	.byte	12
	.byte	24
	.byte	48
	.byte	96
	.byte	-64
	.byte	-4
	.section	.rodata.Ch000212,"a",%progbits
	.type	Ch000212, %object
	.size	Ch000212, 17
Ch000212:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	14
	.byte	-40
	.byte	120
	.section	.rodata.Ch000566,"a",%progbits
	.type	Ch000566, %object
	.size	Ch000566, 38
Ch000566:
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	3
	.byte	10
	.byte	0
	.byte	16
	.byte	-24
	.byte	2
	.byte	30
	.byte	0
	.byte	33
	.byte	0
	.byte	64
	.byte	-128
	.byte	-128
	.byte	64
	.byte	64
	.byte	-128
	.byte	33
	.byte	0
	.byte	31
	.byte	0
	.byte	62
	.byte	0
	.byte	31
	.byte	0
	.byte	62
	.byte	0
	.byte	12
	.byte	0
	.section	.rodata.Ch000290,"a",%progbits
	.type	Ch000290, %object
	.size	Ch000290, 21
Ch000290:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-8
	.byte	1
	.byte	-4
	.byte	-58
	.byte	-61
	.byte	-58
	.byte	-4
	.section	.rodata.Ch000291,"a",%progbits
	.type	Ch000291, %object
	.size	Ch000291, 21
Ch000291:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	7
	.byte	5
	.byte	5
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-100
	.byte	3
	.byte	-1
	.byte	-64
	.byte	-4
	.byte	-64
	.byte	-1
	.section	.rodata.Ch000292,"a",%progbits
	.type	Ch000292, %object
	.size	Ch000292, 20
Ch000292:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	7
	.byte	5
	.byte	5
	.byte	1
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-100
	.byte	7
	.byte	-1
	.byte	-64
	.byte	-4
	.byte	-64
	.section	.rodata.Ch000330,"a",%progbits
	.type	Ch000330, %object
	.size	Ch000330, 17
Ch000330:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	11
	.byte	-2
	.byte	7
	.byte	-64
	.section	.rodata.Ch000331,"a",%progbits
	.type	Ch000331, %object
	.size	Ch000331, 18
Ch000331:
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	4
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	6
	.byte	8
	.byte	-8
	.byte	-26
	.byte	-1
	.byte	-37
	.section	.rodata.Ch000332,"a",%progbits
	.type	Ch000332, %object
	.size	Ch000332, 18
Ch000332:
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	5
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	6
	.byte	8
	.byte	-8
	.byte	-8
	.byte	-20
	.byte	-58
	.section	.rodata.Ch000333,"a",%progbits
	.type	Ch000333, %object
	.size	Ch000333, 20
Ch000333:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	5
	.byte	6
	.byte	5
	.byte	3
	.byte	0
	.byte	7
	.byte	6
	.byte	8
	.byte	56
	.byte	56
	.byte	108
	.byte	-58
	.byte	108
	.byte	56
	.section	.rodata.Ch000334,"a",%progbits
	.type	Ch000334, %object
	.size	Ch000334, 22
Ch000334:
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	5
	.byte	6
	.byte	5
	.byte	1
	.byte	0
	.byte	7
	.byte	6
	.byte	11
	.byte	56
	.byte	6
	.byte	-8
	.byte	-20
	.byte	-58
	.byte	-20
	.byte	-8
	.byte	-64
	.section	.rodata.Ch000298,"a",%progbits
	.type	Ch000298, %object
	.size	Ch000298, 18
Ch000298:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	11
	.byte	-2
	.byte	3
	.byte	-64
	.byte	-2
	.section	.rodata.Ch000299,"a",%progbits
	.type	Ch000299, %object
	.size	Ch000299, 21
Ch000299:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	-92
	.byte	7
	.byte	-61
	.byte	-25
	.byte	-1
	.byte	-37
	.byte	-61
	.section	.rodata.Ch000337,"a",%progbits
	.type	Ch000337, %object
	.size	Ch000337, 23
Ch000337:
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	6
	.byte	5
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	6
	.byte	8
	.byte	0
	.byte	124
	.byte	-58
	.byte	-64
	.byte	112
	.byte	28
	.byte	6
	.byte	-58
	.byte	124
	.section	.rodata.Ch000338,"a",%progbits
	.type	Ch000338, %object
	.size	Ch000338, 21
Ch000338:
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	5
	.byte	4
	.byte	1
	.byte	6
	.byte	3
	.byte	11
	.byte	-26
	.byte	1
	.byte	96
	.byte	-16
	.byte	96
	.byte	108
	.byte	56
	.section	.rodata.Ch000339,"a",%progbits
	.type	Ch000339, %object
	.size	Ch000339, 18
Ch000339:
	.byte	3
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	6
	.byte	8
	.byte	62
	.byte	-58
	.byte	110
	.byte	62
	.section	.rodata.Ch000100,"a",%progbits
	.type	Ch000100, %object
	.size	Ch000100, 18
Ch000100:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	7
	.byte	120
	.byte	-128
	.byte	0
	.byte	-128
	.section	.rodata.Ch000101,"a",%progbits
	.type	Ch000101, %object
	.size	Ch000101, 22
Ch000101:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	120
	.byte	-124
	.byte	-76
	.byte	-92
	.byte	-76
	.byte	-124
	.byte	120
	.section	.rodata.Ch000065,"a",%progbits
	.type	Ch000065, %object
	.size	Ch000065, 20
Ch000065:
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	4
	.byte	5
	.byte	0
	.byte	96
	.byte	16
	.byte	112
	.byte	-112
	.byte	112
	.section	.rodata.Ch000066,"a",%progbits
	.type	Ch000066, %object
	.size	Ch000066, 19
Ch000066:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	2
	.byte	7
	.byte	50
	.byte	-128
	.byte	-32
	.byte	-112
	.byte	-32
	.section	.rodata.Ch000067,"a",%progbits
	.type	Ch000067, %object
	.size	Ch000067, 20
Ch000067:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	4
	.byte	5
	.byte	0
	.byte	96
	.byte	-112
	.byte	-128
	.byte	-112
	.byte	96
	.section	.rodata.Ch000068,"a",%progbits
	.type	Ch000068, %object
	.size	Ch000068, 19
Ch000068:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	2
	.byte	7
	.byte	50
	.byte	16
	.byte	112
	.byte	-112
	.byte	112
	.section	.rodata.Ch000069,"a",%progbits
	.type	Ch000069, %object
	.size	Ch000069, 20
Ch000069:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	0
	.byte	4
	.byte	4
	.byte	5
	.byte	0
	.byte	96
	.byte	-112
	.byte	-16
	.byte	-128
	.byte	96
	.section	.rodata.Ch000107,"a",%progbits
	.type	Ch000107, %object
	.size	Ch000107, 19
Ch000107:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	7
	.byte	70
	.byte	-112
	.byte	-48
	.byte	-96
	.byte	-128
	.section	.rodata.Ch000108,"a",%progbits
	.type	Ch000108, %object
	.size	Ch000108, 18
Ch000108:
	.byte	1
	.byte	1
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	5
	.byte	24
	.byte	64
	.byte	-64
	.byte	64
	.section	.rodata.Ch000109,"a",%progbits
	.type	Ch000109, %object
	.size	Ch000109, 22
Ch000109:
	.byte	0
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	-56
	.byte	72
	.byte	80
	.byte	44
	.byte	84
	.byte	-100
	.byte	-124
	.section	.rodata.Ch000431,"a",%progbits
	.type	Ch000431, %object
	.size	Ch000431, 20
Ch000431:
	.byte	3
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	7
	.byte	10
	.byte	126
	.byte	0
	.byte	-61
	.byte	-25
	.byte	127
	.byte	59
	.section	.rodata.Ch000449,"a",%progbits
	.type	Ch000449, %object
	.size	Ch000449, 17
Ch000449:
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	1
	.byte	3
	.byte	11
	.byte	3
	.byte	2
	.byte	96
	.byte	-64
	.section	.rodata.Ch000436,"a",%progbits
	.type	Ch000436, %object
	.size	Ch000436, 19
Ch000436:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	0
	.byte	11
	.byte	0
	.byte	21
	.byte	-2
	.byte	-1
	.byte	31
	.byte	-1
	.byte	-32
	.section	.rodata.Ch000537,"a",%progbits
	.type	Ch000537, %object
	.size	Ch000537, 24
Ch000537:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	1
	.byte	12
	.byte	96
	.byte	6
	.byte	48
	.byte	96
	.byte	0
	.byte	-8
	.byte	-64
	.byte	-16
	.byte	-64
	.byte	-8
	.section	.rodata.Ch000437,"a",%progbits
	.type	Ch000437, %object
	.size	Ch000437, 16
Ch000437:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.rodata.Ch000538,"a",%progbits
	.type	Ch000538, %object
	.size	Ch000538, 20
Ch000538:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	1
	.byte	12
	.byte	-16
	.byte	15
	.byte	96
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000438,"a",%progbits
	.type	Ch000438, %object
	.size	Ch000438, 19
Ch000438:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	4
	.byte	9
	.byte	62
	.byte	1
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000562,"a",%progbits
	.type	Ch000562, %object
	.size	Ch000562, 22
Ch000562:
	.byte	7
	.byte	7
	.byte	0
	.byte	0
	.byte	0
	.byte	7
	.byte	7
	.byte	14
	.byte	14
	.byte	14
	.byte	0
	.byte	9
	.byte	7
	.byte	9
	.byte	-4
	.byte	0
	.byte	-1
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-1
	.byte	-128
	.section	.rodata.Ch000468,"a",%progbits
	.type	Ch000468, %object
	.size	Ch000468, 23
Ch000468:
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	5
	.byte	5
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	96
	.byte	0
	.byte	120
	.byte	-52
	.byte	12
	.byte	24
	.byte	48
	.byte	0
	.byte	48
	.section	.rodata.Ch000511,"a",%progbits
	.type	Ch000511, %object
	.size	Ch000511, 20
Ch000511:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	1
	.byte	3
	.byte	4
	.byte	11
	.byte	-8
	.byte	3
	.byte	96
	.byte	0
	.byte	96
	.byte	-64
	.section	.rodata.Ch000082,"a",%progbits
	.type	Ch000082, %object
	.size	Ch000082, 18
Ch000082:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	3
	.byte	1
	.byte	0
	.byte	2
	.byte	0
	.byte	4
	.byte	4
	.byte	5
	.byte	24
	.byte	-80
	.byte	-64
	.byte	-128
	.section	.rodata.Ch000214,"a",%progbits
	.type	Ch000214, %object
	.size	Ch000214, 19
Ch000214:
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	4
	.byte	5
	.byte	4
	.byte	-58
	.byte	-42
	.byte	-2
	.byte	108
	.section	.rodata.Ch000569,"a",%progbits
	.type	Ch000569, %object
	.size	Ch000569, 46
Ch000569:
	.byte	4
	.byte	4
	.byte	4
	.byte	1
	.byte	1
	.byte	5
	.byte	5
	.byte	9
	.byte	5
	.byte	5
	.byte	0
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	0
	.byte	124
	.byte	62
	.byte	-69
	.byte	-35
	.byte	-41
	.byte	-21
	.byte	-25
	.byte	-25
	.byte	-21
	.byte	-41
	.byte	-19
	.byte	-73
	.byte	-18
	.byte	119
	.byte	-10
	.byte	111
	.byte	-11
	.byte	-81
	.byte	-5
	.byte	-33
	.byte	-12
	.byte	15
	.byte	-24
	.byte	55
	.byte	-36
	.byte	27
	.byte	-72
	.byte	61
	.byte	126
	.byte	126
	.section	.rodata.Ch000570,"a",%progbits
	.type	Ch000570, %object
	.size	Ch000570, 21
Ch000570:
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	4
	.byte	6
	.byte	6
	.byte	0
	.byte	7
	.byte	9
	.byte	7
	.byte	32
	.byte	56
	.byte	108
	.byte	68
	.byte	-2
	.byte	-18
	.byte	-2
	.section	.rodata.Ch000571,"a",%progbits
	.type	Ch000571, %object
	.size	Ch000571, 23
Ch000571:
	.byte	3
	.byte	3
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	5
	.byte	6
	.byte	6
	.byte	0
	.byte	7
	.byte	7
	.byte	9
	.byte	-112
	.byte	0
	.byte	56
	.byte	108
	.byte	68
	.byte	64
	.byte	-2
	.byte	-18
	.byte	-2
	.section	.rodata.Ch000572,"a",%progbits
	.type	Ch000572, %object
	.size	Ch000572, 21
Ch000572:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	7
	.byte	9
	.byte	7
	.byte	4
	.byte	12
	.byte	18
	.byte	60
	.byte	112
	.byte	-32
	.byte	-64
	.section	.rodata.Ch000120,"a",%progbits
	.type	Ch000120, %object
	.size	Ch000120, 23
Ch000120:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	0
	.byte	4
	.byte	1
	.byte	8
	.byte	0
	.byte	32
	.byte	64
	.byte	0
	.byte	96
	.byte	-112
	.byte	-16
	.byte	-128
	.byte	96
	.section	.rodata.Ch000574,"a",%progbits
	.type	Ch000574, %object
	.size	Ch000574, 19
Ch000574:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	5
	.byte	8
	.byte	8
	.byte	-16
	.byte	32
	.byte	112
	.byte	-8
	.byte	112
	.section	.rodata.Ch000121,"a",%progbits
	.type	Ch000121, %object
	.size	Ch000121, 19
Ch000121:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	2
	.byte	1
	.byte	8
	.byte	-16
	.byte	64
	.byte	-128
	.byte	0
	.byte	-128
	.section	.rodata.Ch000576,"a",%progbits
	.type	Ch000576, %object
	.size	Ch000576, 22
Ch000576:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	1
	.byte	14
	.byte	1
	.byte	14
	.byte	-4
	.byte	31
	.byte	-1
	.byte	-4
	.byte	-128
	.byte	4
	.byte	-1
	.byte	-4
	.section	.rodata.Ch000122,"a",%progbits
	.type	Ch000122, %object
	.size	Ch000122, 20
Ch000122:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	1
	.byte	8
	.byte	-32
	.byte	80
	.byte	-96
	.byte	0
	.byte	-32
	.byte	-112
	.section	.rodata.Ch000578,"a",%progbits
	.type	Ch000578, %object
	.size	Ch000578, 20
Ch000578:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	6
	.byte	5
	.byte	10
	.byte	5
	.byte	0
	.byte	32
	.byte	112
	.byte	-8
	.byte	112
	.byte	32
	.section	.rodata.Ch000123,"a",%progbits
	.type	Ch000123, %object
	.size	Ch000123, 21
Ch000123:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	1
	.byte	8
	.byte	96
	.byte	32
	.byte	64
	.byte	0
	.byte	96
	.byte	-112
	.byte	96
	.section	.rodata.Ch000340,"a",%progbits
	.type	Ch000340, %object
	.size	Ch000340, 19
Ch000340:
	.byte	3
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	4
	.byte	7
	.byte	6
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	6
	.byte	8
	.byte	-86
	.byte	-61
	.byte	102
	.byte	60
	.byte	24
	.section	.rodata.Ch000341,"a",%progbits
	.type	Ch000341, %object
	.size	Ch000341, 19
Ch000341:
	.byte	3
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	4
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	6
	.byte	8
	.byte	-106
	.byte	-61
	.byte	-37
	.byte	-1
	.byte	102
	.section	.rodata.Ch000342,"a",%progbits
	.type	Ch000342, %object
	.size	Ch000342, 20
Ch000342:
	.byte	3
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.byte	3
	.byte	6
	.byte	5
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	6
	.byte	8
	.byte	-110
	.byte	-58
	.byte	108
	.byte	56
	.byte	108
	.byte	-58
	.section	.rodata.Ch000343,"a",%progbits
	.type	Ch000343, %object
	.size	Ch000343, 22
Ch000343:
	.byte	3
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	4
	.byte	7
	.byte	6
	.byte	4
	.byte	3
	.byte	0
	.byte	8
	.byte	6
	.byte	11
	.byte	-86
	.byte	2
	.byte	-61
	.byte	102
	.byte	60
	.byte	24
	.byte	48
	.byte	96
	.section	.rodata.Ch000344,"a",%progbits
	.type	Ch000344, %object
	.size	Ch000344, 23
Ch000344:
	.byte	3
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.byte	3
	.byte	6
	.byte	5
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	6
	.byte	8
	.byte	0
	.byte	-2
	.byte	6
	.byte	12
	.byte	24
	.byte	48
	.byte	96
	.byte	-64
	.byte	-2
	.section	.rodata.Ch000126,"a",%progbits
	.type	Ch000126, %object
	.size	Ch000126, 17
Ch000126:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	0
	.byte	6
	.byte	0
	.byte	11
	.byte	-2
	.byte	7
	.byte	-4
	.section	.rodata.Ch000346,"a",%progbits
	.type	Ch000346, %object
	.size	Ch000346, 17
Ch000346:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	11
	.byte	-2
	.byte	7
	.byte	-64
	.section	.rodata.Ch000070,"a",%progbits
	.type	Ch000070, %object
	.size	Ch000070, 20
Ch000070:
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	2
	.byte	1
	.byte	4
	.byte	2
	.byte	7
	.byte	96
	.byte	32
	.byte	80
	.byte	64
	.byte	-32
	.byte	64
	.section	.rodata.Ch000071,"a",%progbits
	.type	Ch000071, %object
	.size	Ch000071, 20
Ch000071:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	4
	.byte	4
	.byte	7
	.byte	12
	.byte	96
	.byte	-112
	.byte	112
	.byte	16
	.byte	-32
	.section	.rodata.Ch000072,"a",%progbits
	.type	Ch000072, %object
	.size	Ch000072, 18
Ch000072:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	2
	.byte	7
	.byte	114
	.byte	-128
	.byte	-32
	.byte	-112
	.section	.rodata.Ch000073,"a",%progbits
	.type	Ch000073, %object
	.size	Ch000073, 18
Ch000073:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	7
	.byte	120
	.byte	-128
	.byte	0
	.byte	-128
	.section	.rodata.Ch000074,"a",%progbits
	.type	Ch000074, %object
	.size	Ch000074, 20
Ch000074:
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	1
	.byte	3
	.byte	2
	.byte	9
	.byte	-8
	.byte	0
	.byte	32
	.byte	0
	.byte	32
	.byte	-64
	.section	.rodata.Ch000075,"a",%progbits
	.type	Ch000075, %object
	.size	Ch000075, 21
Ch000075:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	2
	.byte	7
	.byte	2
	.byte	-128
	.byte	-112
	.byte	-96
	.byte	-64
	.byte	-96
	.byte	-112
	.section	.rodata.Ch000076,"a",%progbits
	.type	Ch000076, %object
	.size	Ch000076, 16
Ch000076:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	7
	.byte	126
	.byte	-128
	.section	.rodata.Ch000077,"a",%progbits
	.type	Ch000077, %object
	.size	Ch000077, 17
Ch000077:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	28
	.byte	-48
	.byte	-88
	.section	.rodata.Ch000078,"a",%progbits
	.type	Ch000078, %object
	.size	Ch000078, 17
Ch000078:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	4
	.byte	5
	.byte	28
	.byte	-32
	.byte	-112
	.section	.rodata.Ch000079,"a",%progbits
	.type	Ch000079, %object
	.size	Ch000079, 18
Ch000079:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	4
	.byte	5
	.byte	12
	.byte	96
	.byte	-112
	.byte	96
	.section	.rodata.Ch000117,"a",%progbits
	.type	Ch000117, %object
	.size	Ch000117, 21
Ch000117:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	0
	.byte	9
	.byte	-16
	.byte	0
	.byte	16
	.byte	32
	.byte	112
	.byte	-120
	.byte	112
	.section	.rodata.Ch000118,"a",%progbits
	.type	Ch000118, %object
	.size	Ch000118, 20
Ch000118:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	0
	.byte	9
	.byte	-8
	.byte	0
	.byte	16
	.byte	32
	.byte	-120
	.byte	112
	.section	.rodata.Ch000119,"a",%progbits
	.type	Ch000119, %object
	.size	Ch000119, 23
Ch000119:
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	1
	.byte	8
	.byte	0
	.byte	32
	.byte	64
	.byte	0
	.byte	96
	.byte	16
	.byte	112
	.byte	-112
	.byte	112
	.section	.rodata.Ch000444,"a",%progbits
	.type	Ch000444, %object
	.size	Ch000444, 16
Ch000444:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	6
	.byte	-64
	.section	.rodata.Ch000512,"a",%progbits
	.type	Ch000512, %object
	.size	Ch000512, 24
Ch000512:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	5
	.byte	3
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	2
	.byte	0
	.byte	-64
	.byte	-52
	.byte	-40
	.byte	-16
	.byte	-32
	.byte	-16
	.byte	-40
	.byte	-52
	.section	.rodata.Ch000513,"a",%progbits
	.type	Ch000513, %object
	.size	Ch000513, 17
Ch000513:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	4
	.byte	9
	.byte	-2
	.byte	1
	.byte	-64
	.section	.rodata.Ch000488,"a",%progbits
	.type	Ch000488, %object
	.size	Ch000488, 23
Ch000488:
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	72
	.byte	0
	.byte	120
	.byte	-52
	.byte	-64
	.byte	120
	.byte	12
	.byte	-52
	.byte	120
	.section	.rodata.Ch000515,"a",%progbits
	.type	Ch000515, %object
	.size	Ch000515, 17
Ch000515:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	6
	.byte	7
	.byte	124
	.byte	-8
	.byte	-52
	.section	.rodata.Ch000288,"a",%progbits
	.type	Ch000288, %object
	.size	Ch000288, 25
Ch000288:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	8
	.byte	1
	.byte	-4
	.byte	-58
	.byte	-61
	.byte	-58
	.byte	-4
	.byte	-58
	.byte	-61
	.byte	-58
	.byte	-4
	.section	.rodata.Ch000580,"a",%progbits
	.type	Ch000580, %object
	.size	Ch000580, 25
Ch000580:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	16
	.byte	10
	.byte	5
	.byte	0
	.byte	-8
	.byte	31
	.byte	-113
	.byte	-15
	.byte	-120
	.byte	17
	.byte	-113
	.byte	-15
	.byte	-8
	.byte	31
	.section	.rodata.Ch000581,"a",%progbits
	.type	Ch000581, %object
	.size	Ch000581, 29
Ch000581:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	16
	.byte	9
	.byte	7
	.byte	0
	.byte	0
	.byte	32
	.byte	-8
	.byte	95
	.byte	-113
	.byte	-15
	.byte	-119
	.byte	-111
	.byte	-113
	.byte	-15
	.byte	-6
	.byte	31
	.byte	4
	.byte	0
	.section	.rodata.Ch000582,"a",%progbits
	.type	Ch000582, %object
	.size	Ch000582, 18
Ch000582:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	16
	.byte	0
	.byte	16
	.byte	-2
	.byte	-1
	.byte	-1
	.byte	-1
	.section	.rodata.Ch000240,"a",%progbits
	.type	Ch000240, %object
	.size	Ch000240, 23
Ch000240:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	4
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	0
	.byte	9
	.byte	-112
	.byte	0
	.byte	16
	.byte	32
	.byte	-4
	.byte	-64
	.byte	-8
	.byte	-64
	.byte	-4
	.section	.rodata.Ch000472,"a",%progbits
	.type	Ch000472, %object
	.size	Ch000472, 21
Ch000472:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	120
	.byte	0
	.byte	120
	.byte	-52
	.byte	-64
	.byte	-52
	.byte	120
	.section	.rodata.Ch000474,"a",%progbits
	.type	Ch000474, %object
	.size	Ch000474, 21
Ch000474:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	9
	.byte	-52
	.byte	0
	.byte	-8
	.byte	-64
	.byte	-16
	.byte	-64
	.byte	-8
	.section	.rodata.Ch000350,"a",%progbits
	.type	Ch000350, %object
	.size	Ch000350, 22
Ch000350:
	.byte	3
	.byte	1
	.byte	0
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	7
	.byte	3
	.byte	4
	.byte	0
	.byte	8
	.byte	5
	.byte	8
	.byte	16
	.byte	16
	.byte	48
	.byte	112
	.byte	-1
	.byte	112
	.byte	48
	.byte	16
	.section	.rodata.Ch000351,"a",%progbits
	.type	Ch000351, %object
	.size	Ch000351, 22
Ch000351:
	.byte	3
	.byte	4
	.byte	0
	.byte	4
	.byte	3
	.byte	4
	.byte	6
	.byte	7
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	5
	.byte	8
	.byte	16
	.byte	8
	.byte	12
	.byte	14
	.byte	-1
	.byte	14
	.byte	12
	.byte	8
	.section	.rodata.Ch000352,"a",%progbits
	.type	Ch000352, %object
	.size	Ch000352, 18
Ch000352:
	.byte	3
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	5
	.byte	5
	.byte	3
	.byte	3
	.byte	1
	.byte	5
	.byte	6
	.byte	5
	.byte	12
	.byte	112
	.byte	-8
	.byte	112
	.section	.rodata.Ch000353,"a",%progbits
	.type	Ch000353, %object
	.size	Ch000353, 19
Ch000353:
	.byte	0
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	6
	.byte	48
	.byte	-15
	.byte	91
	.byte	85
	.byte	81
	.section	.rodata.Ch000354,"a",%progbits
	.type	Ch000354, %object
	.size	Ch000354, 21
Ch000354:
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	4
	.byte	2
	.byte	4
	.byte	3
	.byte	11
	.byte	-78
	.byte	3
	.byte	96
	.byte	0
	.byte	96
	.byte	-16
	.byte	96
	.section	.rodata.Ch000355,"a",%progbits
	.type	Ch000355, %object
	.size	Ch000355, 34
Ch000355:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	6
	.byte	8
	.byte	8
	.byte	6
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	9
	.byte	0
	.byte	0
	.byte	62
	.byte	0
	.byte	65
	.byte	0
	.byte	-100
	.byte	-128
	.byte	-74
	.byte	-128
	.byte	-80
	.byte	-128
	.byte	-74
	.byte	-128
	.byte	-100
	.byte	-128
	.byte	65
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000356,"a",%progbits
	.type	Ch000356, %object
	.size	Ch000356, 32
Ch000356:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	6
	.byte	8
	.byte	8
	.byte	6
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	9
	.byte	64
	.byte	0
	.byte	62
	.byte	0
	.byte	65
	.byte	0
	.byte	-68
	.byte	-128
	.byte	-74
	.byte	-128
	.byte	-68
	.byte	-128
	.byte	-74
	.byte	-128
	.byte	65
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000080,"a",%progbits
	.type	Ch000080, %object
	.size	Ch000080, 19
Ch000080:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	7
	.byte	76
	.byte	-32
	.byte	-112
	.byte	-32
	.byte	-128
	.section	.rodata.Ch000081,"a",%progbits
	.type	Ch000081, %object
	.size	Ch000081, 19
Ch000081:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	4
	.byte	4
	.byte	7
	.byte	76
	.byte	112
	.byte	-112
	.byte	112
	.byte	16
	.section	.rodata.Ch000359,"a",%progbits
	.type	Ch000359, %object
	.size	Ch000359, 22
Ch000359:
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	3
	.byte	7
	.byte	0
	.byte	112
	.byte	-40
	.byte	24
	.byte	48
	.byte	24
	.byte	-40
	.byte	112
	.section	.rodata.Ch000083,"a",%progbits
	.type	Ch000083, %object
	.size	Ch000083, 20
Ch000083:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	4
	.byte	5
	.byte	0
	.byte	112
	.byte	-128
	.byte	96
	.byte	16
	.byte	-32
	.section	.rodata.Ch000084,"a",%progbits
	.type	Ch000084, %object
	.size	Ch000084, 19
Ch000084:
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	3
	.byte	1
	.byte	3
	.byte	2
	.byte	7
	.byte	50
	.byte	64
	.byte	-32
	.byte	64
	.byte	32
	.section	.rodata.Ch000085,"a",%progbits
	.type	Ch000085, %object
	.size	Ch000085, 17
Ch000085:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	0
	.byte	4
	.byte	4
	.byte	5
	.byte	14
	.byte	-112
	.byte	112
	.section	.rodata.Ch000086,"a",%progbits
	.type	Ch000086, %object
	.size	Ch000086, 18
Ch000086:
	.byte	2
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	10
	.byte	-120
	.byte	80
	.byte	32
	.section	.rodata.Ch000087,"a",%progbits
	.type	Ch000087, %object
	.size	Ch000087, 18
Ch000087:
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	10
	.byte	-120
	.byte	-88
	.byte	80
	.section	.rodata.Ch000088,"a",%progbits
	.type	Ch000088, %object
	.size	Ch000088, 20
Ch000088:
	.byte	2
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	0
	.byte	-120
	.byte	80
	.byte	32
	.byte	80
	.byte	-120
	.section	.rodata.Ch000089,"a",%progbits
	.type	Ch000089, %object
	.size	Ch000089, 20
Ch000089:
	.byte	2
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	7
	.byte	6
	.byte	-120
	.byte	80
	.byte	32
	.byte	64
	.byte	-128
	.section	.rodata.Ch000127,"a",%progbits
	.type	Ch000127, %object
	.size	Ch000127, 16
Ch000127:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.rodata.Ch000128,"a",%progbits
	.type	Ch000128, %object
	.size	Ch000128, 18
Ch000128:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	7
	.byte	78
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000129,"a",%progbits
	.type	Ch000129, %object
	.size	Ch000129, 17
Ch000129:
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	3
	.byte	2
	.byte	-40
	.byte	80
	.section	.rodata.Ch000328,"a",%progbits
	.type	Ch000328, %object
	.size	Ch000328, 21
Ch000328:
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	1
	.byte	5
	.byte	3
	.byte	14
	.byte	-14
	.byte	15
	.byte	24
	.byte	0
	.byte	24
	.byte	-40
	.byte	112
	.section	.rodata.Ch000519,"a",%progbits
	.type	Ch000519, %object
	.size	Ch000519, 19
Ch000519:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	4
	.byte	3
	.byte	1
	.byte	2
	.byte	0
	.byte	5
	.byte	6
	.byte	7
	.byte	112
	.byte	-40
	.byte	-8
	.byte	-32
	.byte	-64
	.section	.rodata.Ch000329,"a",%progbits
	.type	Ch000329, %object
	.size	Ch000329, 24
Ch000329:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	1
	.byte	6
	.byte	5
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	11
	.byte	-122
	.byte	0
	.byte	-64
	.byte	-58
	.byte	-52
	.byte	-40
	.byte	-16
	.byte	-40
	.byte	-52
	.byte	-58
	.section	.rodata.Ch000557,"a",%progbits
	.type	Ch000557, %object
	.size	Ch000557, 16
Ch000557:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	4
	.byte	1
	.byte	10
	.byte	6
	.byte	62
	.byte	-128
	.section	.rodata.Ch000559,"a",%progbits
	.type	Ch000559, %object
	.size	Ch000559, 31
Ch000559:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	13
	.byte	8
	.byte	8
	.byte	0
	.byte	0
	.byte	32
	.byte	0
	.byte	64
	.byte	119
	.byte	-16
	.byte	-119
	.byte	-120
	.byte	-118
	.byte	-120
	.byte	119
	.byte	112
	.byte	8
	.byte	0
	.byte	16
	.byte	0
	.section	.rodata.Ch000169,"a",%progbits
	.type	Ch000169, %object
	.size	Ch000169, 18
Ch000169:
	.byte	3
	.byte	3
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	30
	.byte	24
	.byte	-40
	.byte	112
	.section	.rodata.Ch000200,"a",%progbits
	.type	Ch000200, %object
	.size	Ch000200, 18
Ch000200:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	7
	.byte	120
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000494,"a",%progbits
	.type	Ch000494, %object
	.size	Ch000494, 19
Ch000494:
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-50
	.byte	1
	.byte	-52
	.byte	120
	.byte	48
	.section	.rodata.Ch000361,"a",%progbits
	.type	Ch000361, %object
	.size	Ch000361, 26
Ch000361:
	.byte	0
	.byte	1
	.byte	2
	.byte	0
	.byte	3
	.byte	7
	.byte	6
	.byte	6
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	16
	.byte	0
	.byte	67
	.byte	-61
	.byte	70
	.byte	76
	.byte	24
	.byte	50
	.byte	54
	.byte	106
	.byte	-49
	.byte	-62
	.section	.rodata.Ch000367,"a",%progbits
	.type	Ch000367, %object
	.size	Ch000367, 28
Ch000367:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	0
	.byte	14
	.byte	32
	.byte	16
	.byte	123
	.byte	-34
	.byte	0
	.byte	-61
	.byte	-29
	.byte	-13
	.byte	-45
	.byte	-37
	.byte	-53
	.byte	-49
	.byte	-57
	.byte	-61
	.section	.rodata.Ch000365,"a",%progbits
	.type	Ch000365, %object
	.size	Ch000365, 24
Ch000365:
	.byte	2
	.byte	2
	.byte	1
	.byte	0
	.byte	3
	.byte	5
	.byte	5
	.byte	6
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	0
	.byte	14
	.byte	-48
	.byte	38
	.byte	12
	.byte	24
	.byte	0
	.byte	24
	.byte	60
	.byte	102
	.byte	-1
	.byte	-61
	.section	.rodata.Ch000363,"a",%progbits
	.type	Ch000363, %object
	.size	Ch000363, 27
Ch000363:
	.byte	0
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.byte	7
	.byte	6
	.byte	6
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	0
	.byte	0
	.byte	99
	.byte	-109
	.byte	38
	.byte	-100
	.byte	108
	.byte	24
	.byte	50
	.byte	54
	.byte	106
	.byte	-49
	.byte	-62
	.section	.rodata.Ch000130,"a",%progbits
	.type	Ch000130, %object
	.size	Ch000130, 20
Ch000130:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	5
	.byte	6
	.byte	6
	.byte	5
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	5
	.byte	0
	.byte	108
	.byte	-2
	.byte	108
	.byte	-2
	.byte	108
	.section	.rodata.Ch000131,"a",%progbits
	.type	Ch000131, %object
	.size	Ch000131, 25
Ch000131:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	1
	.byte	9
	.byte	0
	.byte	0
	.byte	16
	.byte	124
	.byte	-42
	.byte	-48
	.byte	124
	.byte	22
	.byte	-42
	.byte	124
	.byte	16
	.section	.rodata.Ch000203,"a",%progbits
	.type	Ch000203, %object
	.size	Ch000203, 16
Ch000203:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	7
	.byte	126
	.byte	-64
	.section	.rodata.Ch000132,"a",%progbits
	.type	Ch000132, %object
	.size	Ch000132, 22
Ch000132:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	-28
	.byte	-84
	.byte	-8
	.byte	48
	.byte	124
	.byte	-44
	.byte	-100
	.section	.rodata.Ch000310,"a",%progbits
	.type	Ch000310, %object
	.size	Ch000310, 25
Ch000310:
	.byte	0
	.byte	1
	.byte	1
	.byte	0
	.byte	3
	.byte	7
	.byte	6
	.byte	6
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	2
	.byte	4
	.byte	-61
	.byte	102
	.byte	126
	.byte	60
	.byte	24
	.byte	60
	.byte	126
	.byte	102
	.byte	-61
	.section	.rodata.Ch000133,"a",%progbits
	.type	Ch000133, %object
	.size	Ch000133, 21
Ch000133:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	2
	.byte	7
	.byte	4
	.byte	120
	.byte	-52
	.byte	120
	.byte	-34
	.byte	-36
	.byte	118
	.section	.rodata.Ch000360,"a",%progbits
	.type	Ch000360, %object
	.size	Ch000360, 19
Ch000360:
	.byte	1
	.byte	0
	.byte	2
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	0
	.byte	4
	.byte	3
	.byte	7
	.byte	112
	.byte	48
	.byte	112
	.byte	-16
	.byte	48
	.section	.rodata.Ch000134,"a",%progbits
	.type	Ch000134, %object
	.size	Ch000134, 16
Ch000134:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	6
	.byte	-64
	.section	.rodata.Ch000362,"a",%progbits
	.type	Ch000362, %object
	.size	Ch000362, 26
Ch000362:
	.byte	0
	.byte	1
	.byte	2
	.byte	0
	.byte	3
	.byte	7
	.byte	6
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	16
	.byte	0
	.byte	67
	.byte	-61
	.byte	70
	.byte	76
	.byte	24
	.byte	62
	.byte	49
	.byte	102
	.byte	-56
	.byte	-49
	.section	.rodata.Ch000135,"a",%progbits
	.type	Ch000135, %object
	.size	Ch000135, 18
Ch000135:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	7
	.byte	60
	.byte	96
	.byte	-64
	.byte	96
	.section	.rodata.Ch000364,"a",%progbits
	.type	Ch000364, %object
	.size	Ch000364, 24
Ch000364:
	.byte	3
	.byte	3
	.byte	0
	.byte	1
	.byte	3
	.byte	4
	.byte	4
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	18
	.byte	1
	.byte	24
	.byte	0
	.byte	24
	.byte	48
	.byte	96
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000136,"a",%progbits
	.type	Ch000136, %object
	.size	Ch000136, 18
Ch000136:
	.byte	0
	.byte	1
	.byte	1
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	0
	.byte	3
	.byte	2
	.byte	7
	.byte	60
	.byte	-64
	.byte	96
	.byte	-64
	.section	.rodata.Ch000366,"a",%progbits
	.type	Ch000366, %object
	.size	Ch000366, 24
Ch000366:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	7
	.byte	5
	.byte	5
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	0
	.byte	14
	.byte	-32
	.byte	28
	.byte	12
	.byte	24
	.byte	0
	.byte	-1
	.byte	-64
	.byte	-4
	.byte	-64
	.byte	-1
	.section	.rodata.Ch000090,"a",%progbits
	.type	Ch000090, %object
	.size	Ch000090, 20
Ch000090:
	.byte	2
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	4
	.byte	5
	.byte	0
	.byte	-8
	.byte	16
	.byte	32
	.byte	64
	.byte	-8
	.section	.rodata.Ch000091,"a",%progbits
	.type	Ch000091, %object
	.size	Ch000091, 20
Ch000091:
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	7
	.byte	36
	.byte	32
	.byte	64
	.byte	-128
	.byte	64
	.byte	32
	.section	.rodata.Ch000092,"a",%progbits
	.type	Ch000092, %object
	.size	Ch000092, 16
Ch000092:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	7
	.byte	126
	.byte	-128
	.section	.rodata.Ch000093,"a",%progbits
	.type	Ch000093, %object
	.size	Ch000093, 20
Ch000093:
	.byte	0
	.byte	1
	.byte	1
	.byte	0
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	0
	.byte	3
	.byte	2
	.byte	7
	.byte	36
	.byte	-128
	.byte	64
	.byte	32
	.byte	64
	.byte	-128
	.section	.rodata.Ch000094,"a",%progbits
	.type	Ch000094, %object
	.size	Ch000094, 17
Ch000094:
	.byte	2
	.byte	1
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	2
	.byte	2
	.byte	0
	.byte	5
	.byte	5
	.byte	2
	.byte	0
	.byte	104
	.byte	-80
	.section	.rodata.Ch000095,"a",%progbits
	.type	Ch000095, %object
	.size	Ch000095, 19
Ch000095:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	44
	.byte	112
	.byte	-120
	.byte	80
	.byte	-40
	.section	.rodata.Ch000096,"a",%progbits
	.type	Ch000096, %object
	.size	Ch000096, 22
Ch000096:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	5
	.byte	5
	.byte	2
	.byte	2
	.byte	0
	.byte	6
	.byte	3
	.byte	7
	.byte	0
	.byte	16
	.byte	48
	.byte	112
	.byte	-4
	.byte	112
	.byte	48
	.byte	16
	.section	.rodata.Ch000097,"a",%progbits
	.type	Ch000097, %object
	.size	Ch000097, 22
Ch000097:
	.byte	3
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	3
	.byte	5
	.byte	5
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	7
	.byte	0
	.byte	32
	.byte	48
	.byte	56
	.byte	-4
	.byte	56
	.byte	48
	.byte	32
	.section	.rodata.Ch000098,"a",%progbits
	.type	Ch000098, %object
	.size	Ch000098, 18
Ch000098:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	96
	.byte	-16
	.byte	96
	.section	.rodata.Ch000099,"a",%progbits
	.type	Ch000099, %object
	.size	Ch000099, 18
Ch000099:
	.byte	0
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	5
	.byte	24
	.byte	-12
	.byte	92
	.byte	84
	.section	.rodata.Ch000137,"a",%progbits
	.type	Ch000137, %object
	.size	Ch000137, 22
Ch000137:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	48
	.byte	-76
	.byte	120
	.byte	-4
	.byte	120
	.byte	-76
	.byte	48
	.section	.rodata.Ch000138,"a",%progbits
	.type	Ch000138, %object
	.size	Ch000138, 18
Ch000138:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	3
	.byte	5
	.byte	18
	.byte	48
	.byte	-4
	.byte	48
	.section	.rodata.Ch000139,"a",%progbits
	.type	Ch000139, %object
	.size	Ch000139, 17
Ch000139:
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	1
	.byte	3
	.byte	7
	.byte	3
	.byte	2
	.byte	96
	.byte	-64
	.section	.rodata.Ch000368,"a",%progbits
	.type	Ch000368, %object
	.size	Ch000368, 24
Ch000368:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	6
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	0
	.byte	14
	.byte	-64
	.byte	15
	.byte	12
	.byte	24
	.byte	0
	.byte	60
	.byte	102
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000369,"a",%progbits
	.type	Ch000369, %object
	.size	Ch000369, 22
Ch000369:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	0
	.byte	14
	.byte	-16
	.byte	15
	.byte	12
	.byte	24
	.byte	0
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000318,"a",%progbits
	.type	Ch000318, %object
	.size	Ch000318, 17
Ch000318:
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	5
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	6
	.byte	-64
	.byte	96
	.section	.rodata.Ch000304,"a",%progbits
	.type	Ch000304, %object
	.size	Ch000304, 25
Ch000304:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	7
	.byte	6
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	24
	.byte	0
	.byte	-4
	.byte	-58
	.byte	-61
	.byte	-58
	.byte	-4
	.byte	-40
	.byte	-52
	.byte	-58
	.byte	-61
	.section	.rodata.Ch000401,"a",%progbits
	.type	Ch000401, %object
	.size	Ch000401, 19
Ch000401:
	.byte	1
	.byte	3
	.byte	3
	.byte	1
	.byte	3
	.byte	6
	.byte	4
	.byte	4
	.byte	6
	.byte	4
	.byte	1
	.byte	6
	.byte	4
	.byte	13
	.byte	-6
	.byte	23
	.byte	-4
	.byte	48
	.byte	-4
	.section	.rodata.Ch000319,"a",%progbits
	.type	Ch000319, %object
	.size	Ch000319, 21
Ch000319:
	.byte	3
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	6
	.byte	8
	.byte	36
	.byte	124
	.byte	6
	.byte	126
	.byte	-58
	.byte	-50
	.byte	118
	.section	.rodata.Ch000405,"a",%progbits
	.type	Ch000405, %object
	.size	Ch000405, 25
Ch000405:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	4
	.byte	13
	.byte	20
	.byte	10
	.byte	-61
	.byte	-29
	.byte	-13
	.byte	-5
	.byte	-37
	.byte	-33
	.byte	-49
	.byte	-57
	.byte	-61
	.section	.rodata.Ch000400,"a",%progbits
	.type	Ch000400, %object
	.size	Ch000400, 24
Ch000400:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	8
	.byte	6
	.byte	6
	.byte	1
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	90
	.byte	31
	.byte	-1
	.byte	-128
	.byte	-64
	.byte	0
	.byte	-2
	.byte	0
	.byte	-64
	.byte	0
	.section	.rodata.Ch000435,"a",%progbits
	.type	Ch000435, %object
	.size	Ch000435, 27
Ch000435:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	6
	.byte	6
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	14
	.byte	0
	.byte	7
	.byte	14
	.byte	28
	.byte	24
	.byte	0
	.byte	60
	.byte	126
	.byte	-25
	.byte	-61
	.byte	-25
	.byte	126
	.byte	60
	.section	.rodata.Ch000402,"a",%progbits
	.type	Ch000402, %object
	.size	Ch000402, 21
Ch000402:
	.byte	6
	.byte	6
	.byte	1
	.byte	2
	.byte	4
	.byte	7
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	1
	.byte	7
	.byte	4
	.byte	13
	.byte	-2
	.byte	1
	.byte	6
	.byte	-58
	.byte	-18
	.byte	124
	.byte	56
	.section	.rodata.Ch000404,"a",%progbits
	.type	Ch000404, %object
	.size	Ch000404, 30
Ch000404:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	-108
	.byte	28
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	-9
	.byte	-128
	.byte	-1
	.byte	-128
	.byte	-35
	.byte	-128
	.byte	-55
	.byte	-128
	.byte	-63
	.byte	-128
	.section	.rodata.Ch000407,"a",%progbits
	.type	Ch000407, %object
	.size	Ch000407, 32
Ch000407:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	7
	.byte	8
	.byte	7
	.byte	1
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	16
	.byte	30
	.byte	-2
	.byte	0
	.byte	-1
	.byte	0
	.byte	-61
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-61
	.byte	-128
	.byte	-1
	.byte	0
	.byte	-2
	.byte	0
	.byte	-64
	.byte	0
	.section	.rodata.Ch000533,"a",%progbits
	.type	Ch000533, %object
	.size	Ch000533, 18
Ch000533:
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	96
	.byte	-32
	.byte	-64
	.section	.rodata.Ch000403,"a",%progbits
	.type	Ch000403, %object
	.size	Ch000403, 18
Ch000403:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	4
	.byte	13
	.byte	-2
	.byte	23
	.byte	-64
	.byte	-1
	.section	.rodata.Ch000408,"a",%progbits
	.type	Ch000408, %object
	.size	Ch000408, 40
Ch000408:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	16
	.byte	0
	.byte	-2
	.byte	0
	.byte	-1
	.byte	0
	.byte	-61
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-61
	.byte	-128
	.byte	-1
	.byte	0
	.byte	-2
	.byte	0
	.byte	-58
	.byte	0
	.byte	-57
	.byte	0
	.byte	-61
	.byte	0
	.byte	-61
	.byte	-128
	.byte	-63
	.byte	-128
	.section	.rodata.Ch000567,"a",%progbits
	.type	Ch000567, %object
	.size	Ch000567, 38
Ch000567:
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	0
	.byte	16
	.byte	0
	.byte	16
	.byte	-24
	.byte	2
	.byte	-4
	.byte	63
	.byte	-5
	.byte	-33
	.byte	-9
	.byte	-17
	.byte	-17
	.byte	-9
	.byte	-9
	.byte	-17
	.byte	-5
	.byte	-33
	.byte	-4
	.byte	31
	.byte	-8
	.byte	63
	.byte	-4
	.byte	31
	.byte	-8
	.byte	63
	.byte	-2
	.byte	127
	.section	.rodata.Ch000409,"a",%progbits
	.type	Ch000409, %object
	.size	Ch000409, 42
Ch000409:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	4
	.byte	7
	.byte	8
	.byte	8
	.byte	7
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	0
	.byte	0
	.byte	62
	.byte	0
	.byte	127
	.byte	0
	.byte	-29
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-32
	.byte	0
	.byte	126
	.byte	0
	.byte	63
	.byte	0
	.byte	3
	.byte	-128
	.byte	1
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	127
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000406,"a",%progbits
	.type	Ch000406, %object
	.size	Ch000406, 30
Ch000406:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	4
	.byte	7
	.byte	8
	.byte	8
	.byte	7
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	-16
	.byte	3
	.byte	62
	.byte	0
	.byte	127
	.byte	0
	.byte	-29
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	127
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000568,"a",%progbits
	.type	Ch000568, %object
	.size	Ch000568, 46
Ch000568:
	.byte	4
	.byte	4
	.byte	4
	.byte	1
	.byte	1
	.byte	5
	.byte	5
	.byte	9
	.byte	5
	.byte	5
	.byte	0
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	0
	.byte	-125
	.byte	-63
	.byte	68
	.byte	34
	.byte	40
	.byte	20
	.byte	24
	.byte	24
	.byte	20
	.byte	40
	.byte	18
	.byte	72
	.byte	17
	.byte	-120
	.byte	9
	.byte	-112
	.byte	10
	.byte	80
	.byte	4
	.byte	32
	.byte	11
	.byte	-16
	.byte	23
	.byte	-56
	.byte	35
	.byte	-28
	.byte	71
	.byte	-62
	.byte	-127
	.byte	-127
	.section	.rodata.Ch000535,"a",%progbits
	.type	Ch000535, %object
	.size	Ch000535, 23
Ch000535:
	.byte	2
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	24
	.byte	0
	.byte	48
	.byte	0
	.byte	48
	.byte	96
	.byte	-64
	.byte	-52
	.byte	120
	.section	.rodata.Ch000545,"a",%progbits
	.type	Ch000545, %object
	.size	Ch000545, 20
Ch000545:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	3
	.byte	10
	.byte	-16
	.byte	3
	.byte	96
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000503,"a",%progbits
	.type	Ch000503, %object
	.size	Ch000503, 20
Ch000503:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-14
	.byte	0
	.byte	-64
	.byte	-8
	.byte	-52
	.byte	-8
	.section	.rodata.Ch000210,"a",%progbits
	.type	Ch000210, %object
	.size	Ch000210, 20
Ch000210:
	.byte	3
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	5
	.byte	0
	.byte	120
	.byte	-64
	.byte	120
	.byte	12
	.byte	120
	.section	.rodata.Ch000531,"a",%progbits
	.type	Ch000531, %object
	.size	Ch000531, 17
Ch000531:
	.byte	3
	.byte	2
	.byte	0
	.byte	3
	.byte	3
	.byte	3
	.byte	5
	.byte	6
	.byte	3
	.byte	3
	.byte	0
	.byte	7
	.byte	8
	.byte	2
	.byte	0
	.byte	118
	.byte	-36
	.section	.rodata.Ch000539,"a",%progbits
	.type	Ch000539, %object
	.size	Ch000539, 24
Ch000539:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	1
	.byte	12
	.byte	80
	.byte	10
	.byte	108
	.byte	-40
	.byte	0
	.byte	-52
	.byte	-20
	.byte	-4
	.byte	-36
	.byte	-52
	.section	.rodata.Ch000213,"a",%progbits
	.type	Ch000213, %object
	.size	Ch000213, 18
Ch000213:
	.byte	2
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	5
	.byte	10
	.byte	-52
	.byte	120
	.byte	48
	.section	.rodata.Ch000484,"a",%progbits
	.type	Ch000484, %object
	.size	Ch000484, 19
Ch000484:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	9
	.byte	-4
	.byte	0
	.byte	120
	.byte	-52
	.byte	120
	.section	.rodata.Ch000370,"a",%progbits
	.type	Ch000370, %object
	.size	Ch000370, 21
Ch000370:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	7
	.byte	7
	.byte	7
	.byte	6
	.byte	4
	.byte	0
	.byte	8
	.byte	0
	.byte	14
	.byte	-14
	.byte	15
	.byte	102
	.byte	0
	.byte	-61
	.byte	102
	.byte	60
	.section	.rodata.Ch000371,"a",%progbits
	.type	Ch000371, %object
	.size	Ch000371, 25
Ch000371:
	.byte	3
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	11
	.byte	32
	.byte	1
	.byte	12
	.byte	24
	.byte	0
	.byte	124
	.byte	6
	.byte	126
	.byte	-58
	.byte	-50
	.byte	118
	.section	.rodata.Ch000372,"a",%progbits
	.type	Ch000372, %object
	.size	Ch000372, 26
Ch000372:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	11
	.byte	64
	.byte	0
	.byte	12
	.byte	24
	.byte	0
	.byte	56
	.byte	108
	.byte	-58
	.byte	-2
	.byte	-64
	.byte	102
	.byte	60
	.section	.rodata.Ch000373,"a",%progbits
	.type	Ch000373, %object
	.size	Ch000373, 20
Ch000373:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	11
	.byte	-16
	.byte	7
	.byte	96
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000374,"a",%progbits
	.type	Ch000374, %object
	.size	Ch000374, 22
Ch000374:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	5
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	11
	.byte	-64
	.byte	7
	.byte	118
	.byte	-36
	.byte	0
	.byte	-8
	.byte	-20
	.byte	-58
	.section	.rodata.Ch000215,"a",%progbits
	.type	Ch000215, %object
	.size	Ch000215, 20
Ch000215:
	.byte	2
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	3
	.byte	5
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	5
	.byte	0
	.byte	-52
	.byte	120
	.byte	48
	.byte	120
	.byte	-52
	.section	.rodata.Ch000376,"a",%progbits
	.type	Ch000376, %object
	.size	Ch000376, 22
Ch000376:
	.byte	3
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	11
	.byte	-16
	.byte	1
	.byte	12
	.byte	24
	.byte	0
	.byte	-58
	.byte	110
	.byte	62
	.section	.rodata.Ch000377,"a",%progbits
	.type	Ch000377, %object
	.size	Ch000377, 21
Ch000377:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	3
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	11
	.byte	-14
	.byte	1
	.byte	108
	.byte	0
	.byte	-58
	.byte	110
	.byte	62
	.section	.rodata.Ch000378,"a",%progbits
	.type	Ch000378, %object
	.size	Ch000378, 18
Ch000378:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	0
	.byte	9
	.byte	0
	.byte	17
	.byte	-2
	.byte	-1
	.byte	-1
	.byte	-128
	.section	.rodata.Ch000216,"a",%progbits
	.type	Ch000216, %object
	.size	Ch000216, 20
Ch000216:
	.byte	2
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.byte	5
	.byte	5
	.byte	4
	.byte	2
	.byte	0
	.byte	6
	.byte	4
	.byte	7
	.byte	6
	.byte	-52
	.byte	120
	.byte	48
	.byte	96
	.byte	-64
	.section	.rodata.Ch000140,"a",%progbits
	.type	Ch000140, %object
	.size	Ch000140, 16
Ch000140:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	5
	.byte	1
	.byte	0
	.byte	-4
	.section	.rodata.Ch000141,"a",%progbits
	.type	Ch000141, %object
	.size	Ch000141, 16
Ch000141:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	7
	.byte	2
	.byte	2
	.byte	-64
	.section	.rodata.Ch000142,"a",%progbits
	.type	Ch000142, %object
	.size	Ch000142, 20
Ch000142:
	.byte	3
	.byte	2
	.byte	1
	.byte	1
	.byte	2
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	1
	.byte	4
	.byte	2
	.byte	7
	.byte	66
	.byte	48
	.byte	112
	.byte	96
	.byte	-32
	.byte	-64
	.section	.rodata.Ch000143,"a",%progbits
	.type	Ch000143, %object
	.size	Ch000143, 20
Ch000143:
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.byte	4
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	24
	.byte	48
	.byte	120
	.byte	-52
	.byte	120
	.byte	48
	.section	.rodata.Ch000144,"a",%progbits
	.type	Ch000144, %object
	.size	Ch000144, 19
Ch000144:
	.byte	2
	.byte	1
	.byte	3
	.byte	3
	.byte	2
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	1
	.byte	4
	.byte	2
	.byte	7
	.byte	112
	.byte	48
	.byte	112
	.byte	-16
	.byte	48
	.section	.rodata.Ch000145,"a",%progbits
	.type	Ch000145, %object
	.size	Ch000145, 22
Ch000145:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	4
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	120
	.byte	-52
	.byte	12
	.byte	56
	.byte	96
	.byte	-64
	.byte	-4
	.section	.rodata.Ch000146,"a",%progbits
	.type	Ch000146, %object
	.size	Ch000146, 22
Ch000146:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	0
	.byte	120
	.byte	-52
	.byte	12
	.byte	56
	.byte	12
	.byte	-52
	.byte	120
	.section	.rodata.Ch000147,"a",%progbits
	.type	Ch000147, %object
	.size	Ch000147, 20
Ch000147:
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.byte	2
	.byte	4
	.byte	4
	.byte	5
	.byte	4
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	72
	.byte	56
	.byte	120
	.byte	-40
	.byte	-4
	.byte	24
	.section	.rodata.Ch000148,"a",%progbits
	.type	Ch000148, %object
	.size	Ch000148, 21
Ch000148:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	16
	.byte	-4
	.byte	-64
	.byte	-8
	.byte	12
	.byte	-52
	.byte	120
	.section	.rodata.Ch000149,"a",%progbits
	.type	Ch000149, %object
	.size	Ch000149, 21
Ch000149:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	32
	.byte	56
	.byte	96
	.byte	-64
	.byte	-8
	.byte	-52
	.byte	120
	.section	.rodata.Ch000528,"a",%progbits
	.type	Ch000528, %object
	.size	Ch000528, 21
Ch000528:
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	3
	.byte	1
	.byte	4
	.byte	4
	.byte	9
	.byte	-52
	.byte	0
	.byte	48
	.byte	96
	.byte	-64
	.byte	96
	.byte	48
	.section	.rodata.Ch000379,"a",%progbits
	.type	Ch000379, %object
	.size	Ch000379, 16
Ch000379:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	0
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.rodata.Ch000349,"a",%progbits
	.type	Ch000349, %object
	.size	Ch000349, 21
Ch000349:
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	3
	.byte	11
	.byte	120
	.byte	3
	.byte	60
	.byte	102
	.byte	-61
	.byte	102
	.byte	-25
	.section	.rodata.Ch000447,"a",%progbits
	.type	Ch000447, %object
	.size	Ch000447, 22
Ch000447:
	.byte	1
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	5
	.byte	5
	.byte	6
	.byte	5
	.byte	3
	.byte	0
	.byte	7
	.byte	5
	.byte	7
	.byte	0
	.byte	68
	.byte	108
	.byte	56
	.byte	-2
	.byte	56
	.byte	108
	.byte	68
	.section	.rodata.Ch000422,"a",%progbits
	.type	Ch000422, %object
	.size	Ch000422, 26
Ch000422:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.byte	1
	.byte	7
	.byte	6
	.byte	7
	.byte	4
	.byte	0
	.byte	8
	.byte	4
	.byte	13
	.byte	6
	.byte	1
	.byte	-64
	.byte	-61
	.byte	-57
	.byte	-50
	.byte	-36
	.byte	-8
	.byte	-36
	.byte	-50
	.byte	-57
	.byte	-61
	.section	.rodata.Ch000575,"a",%progbits
	.type	Ch000575, %object
	.size	Ch000575, 29
Ch000575:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	13
	.byte	9
	.byte	7
	.byte	0
	.byte	32
	.byte	0
	.byte	96
	.byte	0
	.byte	-1
	.byte	-96
	.byte	96
	.byte	48
	.byte	47
	.byte	-8
	.byte	0
	.byte	48
	.byte	0
	.byte	32
	.section	.rodata.Font_Icon16x16,"a",%progbits
	.align	2
	.type	Font_Icon16x16, %object
	.size	Font_Icon16x16, 24
Font_Icon16x16:
	.byte	65
	.byte	1
	.byte	-128
	.byte	0
	.short	1101
	.short	-1
	.short	1165
	.byte	16
	.byte	16
	.byte	0
	.byte	1
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	16
	.byte	16
	.byte	0
	.byte	2
	.space	1
	.section	.rodata.Ch000380,"a",%progbits
	.type	Ch000380, %object
	.size	Ch000380, 18
Ch000380:
	.byte	4
	.byte	2
	.byte	1
	.byte	4
	.byte	4
	.byte	5
	.byte	7
	.byte	8
	.byte	5
	.byte	5
	.byte	1
	.byte	8
	.byte	7
	.byte	8
	.byte	-42
	.byte	24
	.byte	-1
	.byte	24
	.section	.rodata.Ch000381,"a",%progbits
	.type	Ch000381, %object
	.size	Ch000381, 19
Ch000381:
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	5
	.byte	4
	.byte	4
	.byte	2
	.byte	4
	.byte	13
	.byte	5
	.byte	4
	.byte	96
	.byte	-16
	.byte	96
	.byte	-64
	.section	.rodata.Ch000382,"a",%progbits
	.type	Ch000382, %object
	.size	Ch000382, 16
Ch000382:
	.byte	4
	.byte	1
	.byte	1
	.byte	4
	.byte	4
	.byte	5
	.byte	8
	.byte	8
	.byte	5
	.byte	5
	.byte	1
	.byte	8
	.byte	10
	.byte	2
	.byte	2
	.byte	-1
	.section	.rodata.Ch000375,"a",%progbits
	.type	Ch000375, %object
	.size	Ch000375, 24
Ch000375:
	.byte	3
	.byte	1
	.byte	0
	.byte	1
	.byte	3
	.byte	5
	.byte	5
	.byte	6
	.byte	5
	.byte	3
	.byte	0
	.byte	7
	.byte	3
	.byte	11
	.byte	-64
	.byte	1
	.byte	12
	.byte	24
	.byte	0
	.byte	56
	.byte	108
	.byte	-58
	.byte	108
	.byte	56
	.section	.rodata.Ch000384,"a",%progbits
	.type	Ch000384, %object
	.size	Ch000384, 34
Ch000384:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	4
	.byte	6
	.byte	8
	.byte	8
	.byte	6
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	-32
	.byte	1
	.byte	62
	.byte	0
	.byte	127
	.byte	0
	.byte	99
	.byte	0
	.byte	-29
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	99
	.byte	0
	.byte	127
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000385,"a",%progbits
	.type	Ch000385, %object
	.size	Ch000385, 22
Ch000385:
	.byte	3
	.byte	1
	.byte	4
	.byte	4
	.byte	3
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	1
	.byte	5
	.byte	4
	.byte	13
	.byte	-64
	.byte	31
	.byte	24
	.byte	56
	.byte	120
	.byte	-8
	.byte	-40
	.byte	24
	.section	.rodata.Ch000324,"a",%progbits
	.type	Ch000324, %object
	.size	Ch000324, 21
Ch000324:
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	1
	.byte	6
	.byte	3
	.byte	11
	.byte	-56
	.byte	7
	.byte	56
	.byte	108
	.byte	96
	.byte	-16
	.byte	96
	.section	.rodata.Ch000387,"a",%progbits
	.type	Ch000387, %object
	.size	Ch000387, 40
Ch000387:
	.byte	1
	.byte	0
	.byte	1
	.byte	1
	.byte	4
	.byte	7
	.byte	8
	.byte	8
	.byte	7
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	64
	.byte	0
	.byte	62
	.byte	0
	.byte	127
	.byte	0
	.byte	-29
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	3
	.byte	-128
	.byte	15
	.byte	0
	.byte	3
	.byte	-128
	.byte	1
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	127
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000388,"a",%progbits
	.type	Ch000388, %object
	.size	Ch000388, 36
Ch000388:
	.byte	3
	.byte	1
	.byte	0
	.byte	5
	.byte	4
	.byte	6
	.byte	6
	.byte	8
	.byte	6
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	0
	.byte	26
	.byte	14
	.byte	0
	.byte	30
	.byte	0
	.byte	62
	.byte	0
	.byte	54
	.byte	0
	.byte	118
	.byte	0
	.byte	102
	.byte	0
	.byte	-26
	.byte	0
	.byte	-58
	.byte	0
	.byte	-1
	.byte	-128
	.byte	6
	.byte	0
	.section	.rodata.Ch000389,"a",%progbits
	.type	Ch000389, %object
	.size	Ch000389, 36
Ch000389:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	4
	.byte	8
	.byte	6
	.byte	8
	.byte	7
	.byte	4
	.byte	0
	.byte	9
	.byte	4
	.byte	13
	.byte	10
	.byte	1
	.byte	-1
	.byte	-128
	.byte	-64
	.byte	0
	.byte	-34
	.byte	0
	.byte	-1
	.byte	0
	.byte	-29
	.byte	-128
	.byte	1
	.byte	-128
	.byte	-63
	.byte	-128
	.byte	-29
	.byte	-128
	.byte	127
	.byte	0
	.byte	62
	.byte	0
	.section	.rodata.Ch000150,"a",%progbits
	.type	Ch000150, %object
	.size	Ch000150, 20
Ch000150:
	.byte	0
	.byte	1
	.byte	2
	.byte	1
	.byte	2
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	40
	.byte	-4
	.byte	12
	.byte	24
	.byte	48
	.byte	96
	.section	.rodata.Ch000151,"a",%progbits
	.type	Ch000151, %object
	.size	Ch000151, 20
Ch000151:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	36
	.byte	120
	.byte	-52
	.byte	120
	.byte	-52
	.byte	120
	.section	.rodata.Ch000152,"a",%progbits
	.type	Ch000152, %object
	.size	Ch000152, 21
Ch000152:
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	2
	.byte	5
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	0
	.byte	6
	.byte	2
	.byte	7
	.byte	4
	.byte	120
	.byte	-52
	.byte	124
	.byte	12
	.byte	24
	.byte	112
	.section	.rodata.Ch000153,"a",%progbits
	.type	Ch000153, %object
	.size	Ch000153, 18
Ch000153:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	3
	.byte	6
	.byte	42
	.byte	-64
	.byte	0
	.byte	-64
	.section	.rodata.Ch000154,"a",%progbits
	.type	Ch000154, %object
	.size	Ch000154, 19
Ch000154:
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	1
	.byte	3
	.byte	3
	.byte	7
	.byte	42
	.byte	96
	.byte	0
	.byte	96
	.byte	-64
	.section	.rodata.Ch000155,"a",%progbits
	.type	Ch000155, %object
	.size	Ch000155, 22
Ch000155:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	4
	.byte	3
	.byte	3
	.byte	4
	.byte	2
	.byte	0
	.byte	5
	.byte	2
	.byte	7
	.byte	0
	.byte	24
	.byte	48
	.byte	96
	.byte	-64
	.byte	96
	.byte	48
	.byte	24
	.section	.rodata.Ch000156,"a",%progbits
	.type	Ch000156, %object
	.size	Ch000156, 18
Ch000156:
	.byte	2
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	3
	.byte	5
	.byte	5
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	4
	.byte	3
	.byte	0
	.byte	-4
	.byte	0
	.byte	-4
	.text
.Letext0:
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
