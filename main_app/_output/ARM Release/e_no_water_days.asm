	.file	"e_no_water_days.c"
	.text
.Ltext0:
	.section	.text.NOW_update_program,"ax",%progbits
	.align	2
	.type	NOW_update_program, %function
NOW_update_program:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L6
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r2, .L6+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #64
	bl	xQueueTakeMutexRecursive_debug
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r4, r0
	bne	.L2
	mov	r1, #0
	ldr	r0, .L6+8
	bl	GuiLib_GetTextPtr
	b	.L5
.L2:
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	subs	r4, r0, #0
	bne	.L4
	ldr	r0, .L6+4
	mov	r1, #76
	bl	Alert_group_not_found_with_filename
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	mov	r4, r0
.L4:
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L6+12
	str	r0, [r3, #0]
	mov	r0, r4
	bl	nm_GROUP_get_name
.L5:
	mov	r1, r0
	mov	r2, #49
	ldr	r0, .L6+16
	bl	strlcpy
	ldr	r3, .L6
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r4, lr}
	b	Refresh_Screen
.L7:
	.align	2
.L6:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	879
	.word	.LANCHOR0
	.word	GuiVar_NoWaterDaysProg
.LFE0:
	.size	NOW_update_program, .-NOW_update_program
	.section	.text.FDTO_NOW_show_program_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_NOW_show_program_dropdown, %function
FDTO_NOW_show_program_dropdown:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI1:
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r3, .L9
	ldr	r1, .L9+4
	ldr	r3, [r3, #0]
	add	r2, r0, #1
	ldr	r0, .L9+8
	ldr	lr, [sp], #4
	b	FDTO_COMBOBOX_show
.L10:
	.align	2
.L9:
	.word	.LANCHOR1
	.word	nm_NOW_load_program_name_into_guivar
	.word	739
.LFE3:
	.size	FDTO_NOW_show_program_dropdown, .-FDTO_NOW_show_program_dropdown
	.section	.text.nm_NOW_load_program_name_into_guivar,"ax",%progbits
	.align	2
	.type	nm_NOW_load_program_name_into_guivar, %function
nm_NOW_load_program_name_into_guivar:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r4, r0
	bcs	.L12
	mov	r0, r4
	bl	STATION_GROUP_get_group_at_this_index
	cmp	r0, #0
	beq	.L13
	bl	nm_GROUP_get_name
	b	.L14
.L13:
.LBB6:
	ldr	r0, .L15
	mov	r1, #118
.LBE6:
	ldmfd	sp!, {r4, lr}
.LBB7:
	b	Alert_group_not_found_with_filename
.L12:
.LBE7:
	ldr	r0, .L15+4
	mov	r1, #0
	bl	GuiLib_GetTextPtr
.L14:
	mov	r1, r0
	ldr	r0, .L15+8
	mov	r2, #49
	ldmfd	sp!, {r4, lr}
	b	strlcpy
.L16:
	.align	2
.L15:
	.word	.LC0
	.word	879
	.word	GuiVar_ComboBoxItemString
.LFE2:
	.size	nm_NOW_load_program_name_into_guivar, .-nm_NOW_load_program_name_into_guivar
	.section	.text.FDTO_NOW_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_NOW_draw_screen
	.type	FDTO_NOW_draw_screen, %function
FDTO_NOW_draw_screen:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L24
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	ldrnesh	r1, [r3, #0]
	bne	.L22
.LBB10:
	ldr	r3, .L24+4
	ldr	r4, .L24+8
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L24+12
	mov	r3, #138
	bl	xQueueTakeMutexRecursive_debug
	ldr	r5, [r4, #0]
	bl	STATION_GROUP_get_num_groups_in_use
	cmp	r5, r0
	bne	.L19
	mov	r1, #0
	ldr	r0, .L24+16
	bl	GuiLib_GetTextPtr
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L24+20
	b	.L23
.L19:
	ldr	r0, [r4, #0]
	bl	STATION_GROUP_get_group_at_this_index
	subs	r4, r0, #0
	beq	.L21
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L24+24
	str	r0, [r3, #0]
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L24+28
.L23:
	bl	strlcpy
	b	.L20
.L21:
	ldr	r0, .L24+12
	mov	r1, #156
	bl	Alert_group_not_found_with_filename
.L20:
	ldr	r3, .L24+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r1, #0
.L22:
.LBE10:
	mov	r0, #45
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L25:
	.align	2
.L24:
	.word	GuiLib_ActiveCursorFieldNo
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR1
	.word	.LC0
	.word	879
	.word	GuiVar_ComboBoxItemString
	.word	.LANCHOR0
	.word	GuiVar_NoWaterDaysProg
.LFE5:
	.size	FDTO_NOW_draw_screen, .-FDTO_NOW_draw_screen
	.section	.text.NOW_process_screen,"ax",%progbits
	.align	2
	.global	NOW_process_screen
	.type	NOW_process_screen, %function
NOW_process_screen:
.LFB6:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L51
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI4:
	ldrsh	r2, [r3, #0]
	ldr	r3, .L51+4
	sub	sp, sp, #44
.LCFI5:
	cmp	r2, r3
	mov	r4, r0
	bne	.L46
	ldr	r5, .L51+8
	mov	r1, r5
	bl	COMBO_BOX_key_press
	cmp	r4, #2
	cmpne	r4, #67
	bne	.L26
	ldr	r0, [r5, #0]
	bl	NOW_update_program
	b	.L26
.L46:
	cmp	r0, #3
	beq	.L31
	bhi	.L36
	cmp	r0, #1
	beq	.L32
	bhi	.L33
	b	.L31
.L36:
	cmp	r0, #67
	beq	.L34
	bhi	.L37
	cmp	r0, #4
	bne	.L30
	b	.L32
.L37:
	cmp	r0, #80
	beq	.L35
	cmp	r0, #84
	bne	.L30
	b	.L35
.L33:
	ldr	r3, .L51+12
	ldrsh	r4, [r3, #0]
	cmp	r4, #1
	beq	.L39
	cmp	r4, #2
	bne	.L48
	b	.L49
.L39:
	bl	good_key_beep
	ldr	r3, .L51+16
	add	r0, sp, #8
	str	r4, [sp, #8]
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L26
.L49:
	bl	good_key_beep
	ldr	r3, .L51+8
	ldr	r5, [r3, #0]
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r3, .L51+20
	cmp	r5, r0
	ldr	r0, [r3, #0]
	bne	.L41
	mov	r1, r4
	bl	STATION_set_no_water_days_for_all_stations
	b	.L42
.L41:
	ldr	r3, .L51+24
	mov	r2, r4
	ldr	r1, [r3, #0]
	bl	STATION_set_no_water_days_by_group
.L42:
	ldr	r0, .L51+28
	bl	DIALOG_draw_ok_dialog
	b	.L26
.L35:
	ldr	r3, .L51+12
	ldrsh	r5, [r3, #0]
	cmp	r5, #0
	beq	.L44
	cmp	r5, #1
	bne	.L48
	b	.L50
.L44:
	bl	good_key_beep
	mov	r3, #1
	stmia	sp, {r3, r5}
	ldr	r1, .L51+20
	mov	r0, r4
	mov	r2, r5
	mov	r3, #31
	bl	process_uns32
	bl	Refresh_Screen
	b	.L26
.L50:
.LBB11:
	ldr	r6, .L51+32
	mov	r1, #400
	ldr	r2, .L51+36
	mov	r3, #94
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r7, .L51+8
	mov	r2, #0
	mov	r1, r7
	str	r2, [sp, #4]
	str	r5, [sp, #0]
	mov	r3, r0
	mov	r0, r4
	bl	process_uns32
	ldr	r0, [r7, #0]
	bl	NOW_update_program
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	b	.L26
.L48:
.LBE11:
	bl	bad_key_beep
	b	.L26
.L32:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L26
.L31:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L26
.L34:
	ldr	r3, .L51+40
	mov	r2, #8
	str	r2, [r3, #0]
.L30:
	mov	r0, r4
	bl	KEY_process_global_keys
.L26:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L52:
	.align	2
.L51:
	.word	GuiLib_CurStructureNdx
	.word	739
	.word	.LANCHOR1
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_NOW_show_program_dropdown
	.word	GuiVar_NoWaterDaysDays
	.word	.LANCHOR0
	.word	615
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_MenuScreenToShow
.LFE6:
	.size	NOW_process_screen, .-NOW_process_screen
	.section	.bss.g_NOW_program_index,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_NOW_program_index, %object
	.size	g_NOW_program_index, 4
g_NOW_program_index:
	.space	4
	.section	.bss.g_NOW_program_GID,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_NOW_program_GID, %object
	.size	g_NOW_program_GID, 4
g_NOW_program_GID:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_no_water_days.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI3-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI4-.LFB6
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_no_water_days.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x95
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF7
	.byte	0x1
	.4byte	.LASF8
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x5c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0x3c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0x80
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x86
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xa4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xb8
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB5
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI5
	.4byte	.LFE6
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"FDTO_NOW_show_program_dropdown\000"
.LASF8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_no_water_days.c\000"
.LASF4:
	.ascii	"NOW_copy_settings_into_guivars\000"
.LASF5:
	.ascii	"FDTO_NOW_draw_screen\000"
.LASF0:
	.ascii	"nm_NOW_load_program_name_into_guivar\000"
.LASF1:
	.ascii	"NOW_process_program\000"
.LASF6:
	.ascii	"NOW_process_screen\000"
.LASF7:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"NOW_update_program\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
