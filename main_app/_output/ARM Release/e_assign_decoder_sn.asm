	.file	"e_assign_decoder_sn.c"
	.text
.Ltext0:
	.section	.text.TWO_WIRE_jump_to_TP_Micro_alerts,"ax",%progbits
	.align	2
	.type	TWO_WIRE_jump_to_TP_Micro_alerts, %function
TWO_WIRE_jump_to_TP_Micro_alerts:
.LFB0:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L2
	ldr	r2, .L2+4
	ldr	r3, [r3, #0]
	mov	r1, #36
	mla	r3, r1, r3, r2
	ldr	r2, .L2+8
	str	lr, [sp, #-4]!
.LCFI0:
	ldr	r1, [r2, #0]
	sub	sp, sp, #36
.LCFI1:
	str	r1, [r3, #32]
	ldr	r1, .L2+12
	mov	r3, #21
	str	r3, [r2, #0]
	mov	r2, #2
	str	r2, [r1, #0]
	str	r2, [sp, #0]
	mov	r1, #10
	mov	r2, #74
	stmib	sp, {r1, r2}
	ldr	r2, .L2+16
	str	r3, [sp, #32]
	ldr	r3, .L2+20
	str	r2, [sp, #20]
	mov	r0, sp
	mov	r2, #1
	str	r2, [sp, #24]
	str	r3, [sp, #16]
	bl	Change_Screen
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L3:
	.align	2
.L2:
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
	.word	g_ALERTS_pile_to_show
	.word	FDTO_ALERTS_draw_alerts
	.word	ALERTS_process_report
.LFE0:
	.size	TWO_WIRE_jump_to_TP_Micro_alerts, .-TWO_WIRE_jump_to_TP_Micro_alerts
	.section	.text.FDTO_TWO_WIRE_draw_assign_sn,"ax",%progbits
	.align	2
	.global	FDTO_TWO_WIRE_draw_assign_sn
	.type	FDTO_TWO_WIRE_draw_assign_sn, %function
FDTO_TWO_WIRE_draw_assign_sn:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldreq	r3, .L7
	ldrne	r3, .L7+4
	ldreq	r1, [r3, #0]
	ldreq	r3, .L7+8
	ldrnesh	r1, [r3, #0]
	ldreq	r2, [r3, #140]
	ldreq	r3, .L7+12
	mov	r1, r1, asl #16
	str	lr, [sp, #-4]!
.LCFI2:
	mov	r0, #62
	streq	r2, [r3, #0]
	mov	r1, r1, asr #16
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L8:
	.align	2
.L7:
	.word	.LANCHOR0
	.word	GuiLib_ActiveCursorFieldNo
	.word	config_c
	.word	GuiVar_TwoWireDecoderSNToAssign
.LFE2:
	.size	FDTO_TWO_WIRE_draw_assign_sn, .-FDTO_TWO_WIRE_draw_assign_sn
	.section	.text.TWO_WIRE_process_assign_sn,"ax",%progbits
	.align	2
	.global	TWO_WIRE_process_assign_sn
	.type	TWO_WIRE_process_assign_sn, %function
TWO_WIRE_process_assign_sn:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI3:
	mov	r3, r0
	mov	r2, r1
	beq	.L13
	bhi	.L16
	cmp	r0, #0
	beq	.L11
	cmp	r0, #2
	bne	.L10
	b	.L43
.L16:
	cmp	r0, #80
	beq	.L15
	cmp	r0, #84
	beq	.L15
	cmp	r0, #67
	bne	.L10
	b	.L44
.L43:
	ldr	r4, .L45
	ldrsh	r3, [r4, #0]
	cmp	r3, #2
	beq	.L19
	cmp	r3, #3
	beq	.L20
	cmp	r3, #1
	bne	.L40
	ldr	r2, .L45+4
	ldr	r2, [r2, #0]
	cmp	r2, #1024
	bcc	.L40
.LBB4:
	ldr	r1, .L45+8
	ldr	ip, .L45+12
	ldr	r0, [r1, ip]
	cmp	r0, #0
	bne	.L22
	str	r3, [r1, ip]
	ldr	r3, .L45+16
	str	r2, [r1, r3]
	ldr	r3, .L45+20
	mov	r1, #10
	str	r2, [r3, #140]
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	bl	good_key_beep
	b	.L23
.L22:
	bl	bad_key_beep
.L23:
.LBE4:
	ldr	r3, .L45
	ldrsh	r2, [r3, #0]
	b	.L41
.L19:
	mov	r0, #0
	mov	r1, #1
	bl	TWO_WIRE_perform_discovery_process
	ldrsh	r2, [r4, #0]
.L41:
	ldr	r3, .L45+24
	add	r2, r2, #1
	str	r2, [r3, #0]
.L42:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	TWO_WIRE_jump_to_TP_Micro_alerts
.L20:
	bl	good_key_beep
	ldr	sl, .L45+28
	ldr	r9, .L45+4
	mov	r5, #0
	mov	r8, #1
.L25:
	ldr	r2, .L45+32
	mov	r3, #284
	mov	r1, #400
	ldr	r0, [sl, #0]
	bl	xQueueTakeMutexRecursive_debug
	add	r6, r5, #48
	bl	FLOWSENSE_get_controller_index
	mov	r1, r6
	bl	nm_STATION_get_pointer_to_station
	mov	r4, r0
	bl	FLOWSENSE_get_controller_index
	cmp	r4, #0
	mov	r7, r0
	bne	.L24
	mov	r1, r6
	mov	r2, #2
	bl	nm_STATION_create_new_station
	mov	r4, r0
.L24:
	mov	r1, #2
	mov	r0, r4
	bl	STATION_get_change_bits_ptr
	mov	r1, #1
	mov	r2, #0
	mov	r3, #12
	stmia	sp, {r7, r8}
	mov	r6, r0
	str	r0, [sp, #8]
	mov	r0, r4
	bl	nm_STATION_set_physically_available
	mov	r0, r4
	ldr	r1, [r9, #0]
	mov	r2, #1
	mov	r3, #2
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	bl	nm_STATION_set_decoder_serial_number
	mov	r1, r5
	mov	r0, r4
	mov	r2, #1
	mov	r3, #2
	stmia	sp, {r7, r8}
	str	r6, [sp, #8]
	add	r5, r5, #1
	bl	nm_STATION_set_decoder_output
	ldr	r0, [sl, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r5, #2
	bne	.L25
	mov	r4, #0
	ldr	r5, .L45+36
	b	.L26
.L27:
	mov	r0, #2
	bl	vTaskDelay
	add	r4, r4, #1
.L26:
	ldr	r7, [r5, #448]
	cmp	r4, #99
	cmpls	r7, #1
	movne	r7, #0
	moveq	r7, #1
	beq	.L27
	ldr	r4, .L45+40
	mov	r5, #48
.L30:
	bl	FLOWSENSE_get_controller_index
	mov	r3, #60
	str	r3, [r4, #12]
	mov	r3, #1
	str	r7, [r4, #16]
	str	r3, [r4, #0]
	mov	r6, #0
	stmib	r4, {r0, r5}
	b	.L28
.L29:
	mov	r0, #2
	bl	vTaskDelay
	add	r6, r6, #1
.L28:
	ldr	r3, [r4, #0]
	cmp	r6, #99
	cmpls	r3, #1
	movne	r3, #0
	moveq	r3, #1
	beq	.L29
	add	r5, r5, #1
	cmp	r5, #50
	bne	.L30
	ldr	r2, .L45+24
	str	r3, [r2, #0]
	b	.L42
.L15:
	ldr	r1, .L45
	ldrsh	r1, [r1, #0]
	cmp	r1, #0
	bne	.L40
	cmp	r2, #100
	movhi	r2, #25
	bhi	.L33
	cmp	r2, #60
	movhi	r2, #10
	bhi	.L33
	cmp	r2, #30
	movls	r2, #1
	movhi	r2, #5
.L33:
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	mov	r0, r3
	ldr	r1, .L45+4
	mov	r2, #1024
	ldr	r3, .L45+44
	bl	process_uns32
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	Refresh_Screen
.L40:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	bad_key_beep
.L13:
	mov	r0, #1
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	CURSOR_Up
.L11:
	mov	r0, #1
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	CURSOR_Down
.L44:
	ldr	r3, .L45+48
	mov	r2, #11
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	TWO_WIRE_DEBUG_draw_dialog
.L10:
	mov	r0, r3
	mov	r1, r2
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	KEY_process_global_keys
.L46:
	.align	2
.L45:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_TwoWireDecoderSNToAssign
	.word	tpmicro_data
	.word	5068
	.word	5072
	.word	config_c
	.word	.LANCHOR0
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	comm_mngr
	.word	irri_comm
	.word	2097151
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	TWO_WIRE_process_assign_sn, .-TWO_WIRE_process_assign_sn
	.section	.bss.g_TWO_WIRE_last_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_TWO_WIRE_last_cursor_pos, %object
	.size	g_TWO_WIRE_last_cursor_pos, 4
g_TWO_WIRE_last_cursor_pos:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_assign_decoder_sn.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x82
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_assign_decoder_sn.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x61
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x77
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.byte	0x48
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0xa3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xd6
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"TWO_WIRE_process_assign_sn\000"
.LASF4:
	.ascii	"TWO_WIRE_assign_decoder_sn\000"
.LASF5:
	.ascii	"TWO_WIRE_jump_to_TP_Micro_alerts\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_assign_decoder_sn.c\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"FDTO_TWO_WIRE_draw_assign_sn\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
