	.file	"r_rt_poc_flow.c"
	.text
.Ltext0:
	.section	.text.REAL_TIME_POC_FLOW_draw_scroll_line,"ax",%progbits
	.align	2
	.type	REAL_TIME_POC_FLOW_draw_scroll_line, %function
REAL_TIME_POC_FLOW_draw_scroll_line:
.LFB0:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L17
	stmfd	sp!, {r0, r4, lr}
.LCFI0:
	ldr	r2, .L17+4
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #36
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L17+8
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L17+4
	mov	r3, #38
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	POC_get_ptr_to_physically_available_poc
	subs	r4, r0, #0
	beq	.L2
.LBB2:
	bl	nm_GROUP_get_name
	mov	r2, #32
	mov	r1, r0
	ldr	r0, .L17+12
	bl	strlcpy
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	mov	r1, sp
	bl	POC_PRESERVES_get_poc_preserve_for_this_poc_gid
	cmp	r0, #0
	beq	.L3
	ldrb	r3, [r0, #88]	@ zero_extendqisi2
	ands	ip, r3, #1
	beq	.L4
	ldr	r3, [r0, #96]
	cmp	r3, #1
	beq	.L5
.L4:
	ldrb	r3, [r0, #176]	@ zero_extendqisi2
	ands	r1, r3, #1
	beq	.L6
	ldr	r3, [r0, #184]
	cmp	r3, #1
	beq	.L5
.L6:
	ldrb	r3, [r0, #264]	@ zero_extendqisi2
	ands	r2, r3, #1
	beq	.L7
	ldr	r3, [r0, #272]
	cmp	r3, #1
	beq	.L5
.L7:
	cmp	ip, #0
	bne	.L8
	ldr	r3, [r0, #96]
	cmp	r3, #0
	beq	.L15
.L8:
	cmp	r1, #0
	bne	.L9
	ldr	r3, [r0, #184]
	cmp	r3, #0
	beq	.L15
.L9:
	cmp	r2, #0
	movne	r3, #0
	bne	.L5
	ldr	r3, [r0, #272]
	rsbs	r3, r3, #1
	movcc	r3, #0
	b	.L5
.L15:
	mov	r3, #1
.L5:
	flds	s15, [r0, #236]
	ldr	r2, .L17+16
	ftouizs	s15, s15
	str	r3, [r2, #0]
	fmrs	r2, s15	@ int
	flds	s15, [r0, #148]
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	flds	s15, [r0, #324]
	add	r2, r2, r3
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	add	r2, r2, r3
	ldr	r3, .L17+20
	str	r2, [r3, #0]
	b	.L10
.L3:
	ldr	r3, .L17+16
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L17+20
	str	r0, [r3, #0]
	b	.L10
.L2:
.LBE2:
	ldr	r0, .L17+4
	mov	r1, #96
	bl	Alert_group_not_found_with_filename
.L10:
	ldr	r3, .L17+8
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L17
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r3, r4, pc}
.L18:
	.align	2
.L17:
	.word	poc_preserves_recursive_MUTEX
	.word	.LC0
	.word	list_poc_recursive_MUTEX
	.word	GuiVar_LiveScreensPOCFlowName
	.word	GuiVar_LiveScreensPOCFlowMVState
	.word	GuiVar_StatusSystemFlowActual
.LFE0:
	.size	REAL_TIME_POC_FLOW_draw_scroll_line, .-REAL_TIME_POC_FLOW_draw_scroll_line
	.section	.text.FDTO_REAL_TIME_POC_FLOW_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_POC_FLOW_draw_report
	.type	FDTO_REAL_TIME_POC_FLOW_draw_report, %function
FDTO_REAL_TIME_POC_FLOW_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	str	lr, [sp, #-4]!
.LCFI1:
	mov	r2, r0
	bne	.L20
	mvn	r1, #0
	mov	r0, #96
	bl	GuiLib_ShowScreen
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	mov	r0, #0
	bl	POC_populate_pointers_of_POCs_for_display
	ldr	r1, .L22
	mov	r2, r0, asl #16
	mov	r0, #0
	mov	r2, r2, asr #16
	mov	r3, r0
	bl	GuiLib_ScrollBox_Init
	b	.L21
.L20:
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
.L21:
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L23:
	.align	2
.L22:
	.word	REAL_TIME_POC_FLOW_draw_scroll_line
.LFE1:
	.size	FDTO_REAL_TIME_POC_FLOW_draw_report, .-FDTO_REAL_TIME_POC_FLOW_draw_report
	.section	.text.REAL_TIME_POC_FLOW_process_report,"ax",%progbits
	.align	2
	.global	REAL_TIME_POC_FLOW_process_report
	.type	REAL_TIME_POC_FLOW_process_report, %function
REAL_TIME_POC_FLOW_process_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	stmfd	sp!, {r4, lr}
.LCFI2:
	bne	.L28
	ldr	r3, .L29
	ldr	r2, .L29+4
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r4, .L29+8
	ldr	r3, [r3, #4]
	str	r3, [r4, #0]
	bl	KEY_process_global_keys
	ldr	r3, [r4, #0]
	cmp	r3, #10
	ldmnefd	sp!, {r4, pc}
	mov	r0, #1
	ldmfd	sp!, {r4, lr}
	b	LIVE_SCREENS_draw_dialog
.L28:
	mov	r2, #0
	mov	r3, r2
	ldmfd	sp!, {r4, lr}
	b	REPORTS_process_report
.L30:
	.align	2
.L29:
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	REAL_TIME_POC_FLOW_process_report, .-REAL_TIME_POC_FLOW_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_poc_flow.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_rt_poc_flow.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x59
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x20
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x69
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x80
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_poc_flow.c\000"
.LASF1:
	.ascii	"REAL_TIME_POC_FLOW_process_report\000"
.LASF0:
	.ascii	"FDTO_REAL_TIME_POC_FLOW_draw_report\000"
.LASF4:
	.ascii	"REAL_TIME_POC_FLOW_draw_scroll_line\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
