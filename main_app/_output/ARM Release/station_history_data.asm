	.file	"station_history_data.c"
	.text
.Ltext0:
	.section	.text.station_history_ci_timer_callback,"ax",%progbits
	.align	2
	.type	station_history_ci_timer_callback, %function
station_history_ci_timer_callback:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L2
	mov	r1, #0
	mov	r2, #512
	mov	r3, r1
	b	CONTROLLER_INITIATED_post_to_messages_queue
.L3:
	.align	2
.L2:
	.word	402
.LFE5:
	.size	station_history_ci_timer_callback, .-station_history_ci_timer_callback
	.section	.text.nm_init_station_history_record,"ax",%progbits
	.align	2
	.global	nm_init_station_history_record
	.type	nm_init_station_history_record, %function
nm_init_station_history_record:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, #0
	mov	r2, #60
	b	memset
.LFE0:
	.size	nm_init_station_history_record, .-nm_init_station_history_record
	.section	.text.nm_init_station_history_records,"ax",%progbits
	.align	2
	.type	nm_init_station_history_records, %function
nm_init_station_history_records:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r0, .L8
	mov	r1, #0
	mov	r2, #60
	bl	memset
	ldr	r6, .L8
	mov	r4, #0
	mov	r5, #60
.L6:
	add	r4, r4, #1
	mla	r0, r5, r4, r6
	bl	nm_init_station_history_record
	cmp	r4, #3840
	bne	.L6
	ldmfd	sp!, {r4, r5, r6, pc}
.L9:
	.align	2
.L8:
	.word	.LANCHOR0
.LFE1:
	.size	nm_init_station_history_records, .-nm_init_station_history_records
	.section	.text.nm_station_history_updater,"ax",%progbits
	.align	2
	.type	nm_station_history_updater, %function
nm_station_history_updater:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #3
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r4, r0
	bne	.L11
	ldr	r0, .L21
	mov	r1, r4
	ldmfd	sp!, {r4, lr}
	b	Alert_Message_va
.L11:
	ldr	r0, .L21+4
	mov	r1, #3
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	bne	.L12
	bl	nm_init_station_history_records
	b	.L15
.L12:
	cmp	r4, #1
	beq	.L15
	cmp	r4, #2
	bne	.L16
.L15:
	ldr	r3, .L21+8
	ldr	r1, .L21+12
	add	r0, r3, #230400
	mov	r2, #0
.L17:
	strh	r1, [r3, #116]	@ movhi
	strh	r2, [r3, #118]	@ movhi
	add	r3, r3, #60
	cmp	r3, r0
	bne	.L17
	ldmfd	sp!, {r4, pc}
.L16:
	ldr	r0, .L21+16
	ldmfd	sp!, {r4, lr}
	b	Alert_Message
.L22:
	.align	2
.L21:
	.word	.LC0
	.word	.LC1
	.word	.LANCHOR0
	.word	5000
	.word	.LC2
.LFE2:
	.size	nm_station_history_updater, .-nm_station_history_updater
	.section	.text.init_file_station_history,"ax",%progbits
	.align	2
	.global	init_file_station_history
	.type	init_file_station_history, %function
init_file_station_history:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L24
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r4, .L24+4
	sub	sp, sp, #28
.LCFI3:
	str	r3, [sp, #0]
	ldr	r3, .L24+8
	mov	r0, #1
	str	r3, [sp, #4]
	ldr	r3, .L24+12
	ldr	r1, .L24+16
	str	r3, [sp, #8]
	ldr	r3, .L24+20
	mov	r2, #3
	ldr	r3, [r3, #0]
	str	r3, [sp, #12]
	ldr	r3, .L24+24
	str	r3, [sp, #16]
	ldr	r3, .L24+28
	str	r3, [sp, #20]
	mov	r3, #5
	str	r3, [sp, #24]
	mov	r3, r4
	bl	FLASH_FILE_find_or_create_reports_file
	mov	r3, #0
	str	r3, [r4, #24]
	add	sp, sp, #28
	ldmfd	sp!, {r4, pc}
.L25:
	.align	2
.L24:
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR3
	.word	230460
	.word	.LANCHOR1
	.word	station_history_completed_records_recursive_MUTEX
	.word	nm_station_history_updater
	.word	nm_init_station_history_records
.LFE3:
	.size	init_file_station_history, .-init_file_station_history
	.section	.text.save_file_station_history,"ax",%progbits
	.align	2
	.global	save_file_station_history
	.type	save_file_station_history, %function
save_file_station_history:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI4:
	ldr	r4, .L27
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L27+4
	ldr	r3, .L27+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L27+12
	mov	r0, #1
	str	r3, [sp, #0]
	ldr	r3, [r4, #0]
	ldr	r1, .L27+16
	str	r3, [sp, #4]
	mov	r3, #5
	str	r3, [sp, #8]
	mov	r2, #3
	ldr	r3, .L27+20
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldr	r0, [r4, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L28:
	.align	2
.L27:
	.word	station_history_completed_records_recursive_MUTEX
	.word	.LC3
	.word	386
	.word	230460
	.word	.LANCHOR1
	.word	.LANCHOR0
.LFE4:
	.size	save_file_station_history, .-save_file_station_history
	.global	__udivsi3
	.section	.text.STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds,"ax",%progbits
	.align	2
	.global	STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds
	.type	STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds, %function
STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI5:
	ldr	r5, .L32
	mov	r4, r0
	ldr	r6, [r5, #0]
	cmp	r6, #0
	bne	.L30
	mov	r0, #1000
	mov	r1, #5
	mul	r0, r4, r0
	bl	__udivsi3
	ldr	r3, .L32+4
	mov	r2, r6
	str	r3, [sp, #0]
	mov	r3, r6
	mov	r1, r0
	ldr	r0, .L32+8
	bl	xTimerCreate
	cmp	r0, #0
	str	r0, [r5, #0]
	bne	.L30
	ldr	r0, .L32+12
	bl	RemovePathFromFileName
	ldr	r2, .L32+16
	mov	r1, r0
	ldr	r0, .L32+20
	bl	Alert_Message_va
.L30:
	ldr	r6, .L32
	ldr	r0, [r6, #0]
	cmp	r0, #0
	beq	.L29
	bl	xTimerIsTimerActive
	subs	r5, r0, #0
	bne	.L29
	mov	r0, #1000
	mov	r1, #5
	mul	r0, r4, r0
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, r5
	mov	r2, r0
	ldr	r0, [r6, #0]
	bl	xTimerGenericCommand
.L29:
	ldmfd	sp!, {r3, r4, r5, r6, pc}
.L33:
	.align	2
.L32:
	.word	.LANCHOR4
	.word	station_history_ci_timer_callback
	.word	.LC4
	.word	.LC3
	.word	419
	.word	.LC5
.LFE6:
	.size	STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds, .-STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds
	.section	.text.nm_STATION_HISTORY_inc_index,"ax",%progbits
	.align	2
	.global	nm_STATION_HISTORY_inc_index
	.type	nm_STATION_HISTORY_inc_index, %function
nm_STATION_HISTORY_inc_index:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #0]
	add	r3, r3, #1
	cmp	r3, #3840
	movcs	r3, #0
	str	r3, [r0, #0]
	bx	lr
.LFE7:
	.size	nm_STATION_HISTORY_inc_index, .-nm_STATION_HISTORY_inc_index
	.section	.text.nm_STATION_HISTORY_increment_next_avail_ptr,"ax",%progbits
	.align	2
	.global	nm_STATION_HISTORY_increment_next_avail_ptr
	.type	nm_STATION_HISTORY_increment_next_avail_ptr, %function
nm_STATION_HISTORY_increment_next_avail_ptr:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI6:
	ldr	r4, .L41
	add	r0, r4, #4
	bl	nm_STATION_HISTORY_inc_index
	ldr	r3, [r4, #4]
	cmp	r3, #0
	moveq	r2, #1
	streq	r2, [r4, #8]
	ldr	r2, [r4, #16]
	cmp	r3, r2
	bne	.L39
	ldr	r0, .L41+4
	bl	nm_STATION_HISTORY_inc_index
.L39:
	ldr	r2, [r4, #4]
	ldr	r3, [r4, #20]
	cmp	r2, r3
	ldmnefd	sp!, {r4, pc}
	ldr	r0, .L41+8
	ldmfd	sp!, {r4, lr}
	b	nm_STATION_HISTORY_inc_index
.L42:
	.align	2
.L41:
	.word	.LANCHOR0
	.word	.LANCHOR0+16
	.word	.LANCHOR0+20
.LFE8:
	.size	nm_STATION_HISTORY_increment_next_avail_ptr, .-nm_STATION_HISTORY_increment_next_avail_ptr
	.section	.text.nm_STATION_HISTORY_get_previous_completed_record,"ax",%progbits
	.align	2
	.global	nm_STATION_HISTORY_get_previous_completed_record
	.type	nm_STATION_HISTORY_get_previous_completed_record, %function
nm_STATION_HISTORY_get_previous_completed_record:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L51
	add	r2, r3, #60
	cmp	r0, r2
	bcc	.L49
	add	r1, r2, #230400
	cmp	r0, r1
	bcs	.L49
	ldr	r1, [r3, #12]
	cmp	r1, #1
	beq	.L49
	cmp	r0, r2
	subne	r0, r0, #60
	bne	.L44
	ldr	r0, [r3, #8]
	cmp	r0, #1
	addeq	r0, r3, #230400
	movne	r0, #0
	b	.L44
.L49:
	mov	r0, #0
.L44:
	ldr	r3, .L51
	mov	r1, #60
	ldr	r2, [r3, #4]
	add	r2, r2, #1
	mla	r2, r1, r2, r3
	cmp	r0, r2
	moveq	r2, #1
	streq	r2, [r3, #12]
	bx	lr
.L52:
	.align	2
.L51:
	.word	.LANCHOR0
.LFE9:
	.size	nm_STATION_HISTORY_get_previous_completed_record, .-nm_STATION_HISTORY_get_previous_completed_record
	.section	.text.nm_STATION_HISTORY_get_most_recently_completed_record,"ax",%progbits
	.align	2
	.global	nm_STATION_HISTORY_get_most_recently_completed_record
	.type	nm_STATION_HISTORY_get_most_recently_completed_record, %function
nm_STATION_HISTORY_get_most_recently_completed_record:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L54
	mov	r2, #0
	str	r2, [r3, #12]
	ldr	r2, [r3, #4]
	mov	r0, #60
	add	r2, r2, #1
	mla	r0, r2, r0, r3
	b	nm_STATION_HISTORY_get_previous_completed_record
.L55:
	.align	2
.L54:
	.word	.LANCHOR0
.LFE10:
	.size	nm_STATION_HISTORY_get_most_recently_completed_record, .-nm_STATION_HISTORY_get_most_recently_completed_record
	.section	.text.STATION_HISTORY_fill_ptrs_and_return_how_many_lines,"ax",%progbits
	.align	2
	.global	STATION_HISTORY_fill_ptrs_and_return_how_many_lines
	.type	STATION_HISTORY_fill_ptrs_and_return_how_many_lines, %function
STATION_HISTORY_fill_ptrs_and_return_how_many_lines:
.LFB11:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L66
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI7:
	ldr	r6, .L66+4
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L66+8
	ldr	r2, .L66+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r6, #0]
	cmp	r3, #0
	bne	.L57
	mov	r0, #15360
	ldr	r1, .L66+12
	ldr	r2, .L66+16
	bl	mem_malloc_debug
	str	r0, [r6, #0]
.L57:
	mov	r1, r5
	mov	r2, sp
	mov	r0, r4
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	ldr	r2, [sp, #0]
	ldr	r3, .L66+20
	mov	r2, r2, asl #7
	add	r1, r3, r2
	ldrb	r6, [r1, #39]	@ zero_extendqisi2
	mov	r6, r6, lsr #4
	ands	r6, r6, #1
	beq	.L58
	ldrb	r1, [r1, #141]	@ zero_extendqisi2
	tst	r1, #64
	ldreq	r1, .L66+4
	addeq	r2, r2, #16
	ldreq	r1, [r1, #0]
	addeq	r3, r2, r3
	moveq	r6, #1
	movne	r6, #0
	streq	r3, [r1, #0]
.L58:
	bl	nm_STATION_HISTORY_get_most_recently_completed_record
	ldr	r8, .L66+4
	ldr	r7, .L66+24
	b	.L59
.L63:
	ldrb	r3, [r0, #54]	@ zero_extendqisi2
	cmp	r3, r4
	bne	.L60
	ldrb	r3, [r0, #52]	@ zero_extendqisi2
	cmp	r3, r5
	ldreq	r3, [r8, #0]
	streq	r0, [r3, r6, asl #2]
	addeq	r6, r6, #1
.L60:
	cmp	r6, r7
	bls	.L61
	ldr	r0, .L66+12
	bl	RemovePathFromFileName
	ldr	r2, .L66+28
	mov	r1, r0
	ldr	r0, .L66+32
	bl	Alert_Message_va
	b	.L62
.L61:
	bl	nm_STATION_HISTORY_get_previous_completed_record
.L59:
	cmp	r0, #0
	bne	.L63
.L62:
	ldr	r3, .L66
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r6
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, pc}
.L67:
	.align	2
.L66:
	.word	station_history_completed_records_recursive_MUTEX
	.word	.LANCHOR5
	.word	625
	.word	.LC3
	.word	638
	.word	station_preserves
	.word	3839
	.word	674
	.word	.LC6
.LFE11:
	.size	STATION_HISTORY_fill_ptrs_and_return_how_many_lines, .-STATION_HISTORY_fill_ptrs_and_return_how_many_lines
	.section	.text.STATION_HISTORY_draw_scroll_line,"ax",%progbits
	.align	2
	.global	STATION_HISTORY_draw_scroll_line
	.type	STATION_HISTORY_draw_scroll_line, %function
STATION_HISTORY_draw_scroll_line:
.LFB13:
	@ args = 0, pretend = 0, frame = 76
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI8:
	fstmfdd	sp!, {d8}
.LCFI9:
	flds	s16, .L107
	ldr	r3, .L107+12
	sub	sp, sp, #84
.LCFI10:
	str	r3, [sp, #72]	@ float
	ldr	r3, .L107+16
	mov	r0, r0, asl #16
	str	r3, [sp, #76]	@ float
	ldr	r3, .L107+204
	fsts	s16, [sp, #80]
	mov	r4, r0, asr #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L107+20
	ldr	r3, .L107+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L107+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L69
.LBB5:
	ldr	r4, [r3, r4, asl #2]
	flds	s17, .L107+4
	mov	r3, #250
	ldrh	r2, [r4, #28]
	mov	r1, #64
	str	r3, [sp, #0]
	add	r0, sp, #8
	mov	r3, #150
	bl	GetDateStr
	mov	r2, #6
	mov	r1, r0
	ldr	r0, .L107+32
	bl	strlcpy
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r1, #64
	ldr	r2, [r4, #0]
	add	r0, sp, #8
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r2, #8
	mov	r1, r0
	ldr	r0, .L107+36
	bl	strlcpy
	ldrb	r2, [r4, #53]	@ zero_extendqisi2
	flds	s13, [r4, #12]	@ int
	ldr	r3, .L107+40
	flds	s14, [r4, #16]
	mov	r1, #400
	str	r2, [r3, #0]
	ldrh	r3, [r4, #34]
	ldr	r2, .L107+20
	fmsr	s12, r3	@ int
	ldr	r3, .L107+44
	fuitos	s15, s12
	fdivs	s15, s15, s17
	fsts	s15, [r3, #0]
	fuitos	s15, s13
	ldr	r3, .L107+48
	fdivs	s16, s15, s16
	fcvtds	d7, s14
	fsts	s16, [r3, #0]
	ldr	r3, .L107+52
	fstd	d7, [r3, #0]
	ldrh	r3, [r4, #46]
	fmsr	s14, r3	@ int
	ldr	r3, .L107+56
	fuitos	s15, s14
	fsts	s15, [r3, #0]
	ldr	r3, .L107+68
	ldr	r0, [r3, #0]
	ldr	r3, .L107+60
	bl	xQueueTakeMutexRecursive_debug
	ldrb	r1, [r4, #52]	@ zero_extendqisi2
	ldrb	r0, [r4, #54]	@ zero_extendqisi2
	bl	nm_STATION_get_pointer_to_station
	mov	r5, r0
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #1
	bne	.L70
	ldrsh	r3, [r4, #56]
	mov	r0, r5
	fmsr	s15, r3	@ int
	fsitos	s16, s15
	bl	STATION_GROUP_get_soil_storage_capacity_inches_100u
	flds	s15, [sp, #76]
	fmsr	s12, r0	@ int
	fuitos	s14, s12
	fdivs	s14, s14, s15
	flds	s15, .L107+8
	fnmacs	s16, s14, s15
	fcmpezs	s16
	fmstat
	ble	.L71
	mov	r0, r5
	bl	nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag
	cmp	r0, #0
	bne	.L71
	mov	r0, r5
	bl	STATION_GROUP_get_station_precip_rate_in_per_hr_100000u
	flds	s15, [sp, #72]
	flds	s14, [sp, #80]
	ldr	r3, .L107+64
	fmsr	s12, r0	@ int
	fuitos	s13, s12
	fdivs	s15, s13, s15
	fdivs	s15, s15, s14
	fdivs	s16, s16, s15
	fsts	s16, [r3, #0]
	b	.L73
.L71:
	ldr	r3, .L107+64
	mov	r2, #0
	str	r2, [r3, #0]	@ float
	b	.L73
.L108:
	.align	2
.L107:
	.word	1114636288
	.word	1092616192
	.word	1056964608
	.word	1203982336
	.word	1120403456
	.word	.LC3
	.word	917
	.word	.LANCHOR5
	.word	GuiVar_RptDate
	.word	GuiVar_RptStartTime
	.word	GuiVar_RptCycles
	.word	GuiVar_RptScheduledMin
	.word	GuiVar_RptIrrigMin
	.word	GuiVar_RptIrrigGal
	.word	GuiVar_RptFlow
	.word	949
	.word	GuiVar_RptRainMin
	.word	list_program_data_recursive_MUTEX
	.word	GuiVar_RptLastMeasuredCurrent
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	.LC15
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.word	.LC20
	.word	.LC21
	.word	.LC22
	.word	.LC23
	.word	.LC24
	.word	.LC25
	.word	.LC26
	.word	.LC27
	.word	.LC28
	.word	.LC29
	.word	.LC30
	.word	.LC31
	.word	.LC32
	.word	.LC33
	.word	.LC34
	.word	.LC35
	.word	.LC36
	.word	.LC37
	.word	GuiVar_RptFlags
	.word	station_history_completed_records_recursive_MUTEX
	.word	1148846080
.L70:
	ldrh	r3, [r4, #42]
	fmsr	s13, r3	@ int
	ldr	r3, .L107+64
	fuitos	s15, s13
	fdivs	s17, s15, s17
	fsts	s17, [r3, #0]
.L73:
	ldr	r3, .L107+68
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldrh	r3, [r4, #44]
	ldr	r6, [r4, #20]
	fmsr	s15, r3	@ int
	ldr	r3, .L107+72
	ldrb	r8, [r4, #55]	@ zero_extendqisi2
	fuitos	s14, s15
	flds	s15, .L107+208
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
.LBB6:
.LBB7:
	mov	r3, #0
	strb	r3, [sp, #8]
	mov	r3, r6, lsr #8
	tst	r3, #128
	and	r4, r3, #255
	beq	.L74
	add	r0, sp, #8
	ldr	r1, .L107+76
	mov	r2, #64
	bl	strlcat
.L74:
	tst	r6, #2
	and	r5, r6, #255
	beq	.L75
	add	r0, sp, #8
	ldr	r1, .L107+80
	mov	r2, #64
	bl	strlcat
.L75:
	mov	r3, r6, lsr #24
	tst	r3, #4
	and	r7, r3, #255
	beq	.L76
	add	r0, sp, #8
	ldr	r1, .L107+84
	mov	r2, #64
	bl	strlcat
.L76:
	mov	r3, r6, lsr #16
	tst	r3, #2
	and	r6, r3, #255
	beq	.L77
	add	r0, sp, #8
	ldr	r1, .L107+88
	mov	r2, #64
	bl	strlcat
.L77:
	tst	r6, #8
	beq	.L78
	add	r0, sp, #8
	ldr	r1, .L107+92
	mov	r2, #64
	bl	strlcat
.L78:
	tst	r4, #64
	beq	.L79
	add	r0, sp, #8
	ldr	r1, .L107+96
	mov	r2, #64
	bl	strlcat
.L79:
	tst	r4, #32
	beq	.L80
	add	r0, sp, #8
	ldr	r1, .L107+100
	mov	r2, #64
	bl	strlcat
.L80:
	tst	r7, #8
	beq	.L81
	add	r0, sp, #8
	ldr	r1, .L107+104
	mov	r2, #64
	bl	strlcat
.L81:
	tst	r7, #32
	beq	.L82
	add	r0, sp, #8
	ldr	r1, .L107+108
	mov	r2, #64
	bl	strlcat
.L82:
	tst	r7, #64
	beq	.L83
	add	r0, sp, #8
	ldr	r1, .L107+112
	mov	r2, #64
	bl	strlcat
.L83:
	tst	r7, #2
	beq	.L84
	add	r0, sp, #8
	ldr	r1, .L107+116
	mov	r2, #64
	bl	strlcat
.L84:
	tst	r5, #16
	beq	.L85
	add	r0, sp, #8
	ldr	r1, .L107+120
	mov	r2, #64
	bl	strlcat
.L85:
	tst	r4, #8
	beq	.L86
	add	r0, sp, #8
	ldr	r1, .L107+124
	mov	r2, #64
	bl	strlcat
.L86:
	tst	r5, #32
	beq	.L87
	add	r0, sp, #8
	ldr	r1, .L107+128
	mov	r2, #64
	bl	strlcat
.L87:
	tst	r6, #4
	beq	.L88
	add	r0, sp, #8
	ldr	r1, .L107+132
	mov	r2, #64
	bl	strlcat
.L88:
	tst	r8, #1
	bne	.L89
	tst	r8, #2
	beq	.L90
.L89:
	add	r0, sp, #8
	ldr	r1, .L107+136
	mov	r2, #64
	bl	strlcat
.L90:
	tst	r6, #128
	beq	.L91
	add	r0, sp, #8
	ldr	r1, .L107+140
	mov	r2, #64
	bl	strlcat
.L91:
	tst	r6, #1
	beq	.L92
	add	r0, sp, #8
	ldr	r1, .L107+144
	mov	r2, #64
	bl	strlcat
.L92:
	tst	r5, #4
	beq	.L93
	add	r0, sp, #8
	ldr	r1, .L107+148
	mov	r2, #64
	bl	strlcat
.L93:
	tst	r5, #8
	beq	.L94
	add	r0, sp, #8
	ldr	r1, .L107+152
	mov	r2, #64
	bl	strlcat
.L94:
	tst	r6, #16
	beq	.L95
	add	r0, sp, #8
	ldr	r1, .L107+156
	mov	r2, #64
	bl	strlcat
.L95:
	tst	r6, #32
	beq	.L96
	add	r0, sp, #8
	ldr	r1, .L107+160
	mov	r2, #64
	bl	strlcat
.L96:
	tst	r6, #64
	beq	.L97
	add	r0, sp, #8
	ldr	r1, .L107+164
	mov	r2, #64
	bl	strlcat
.L97:
	tst	r4, #4
	beq	.L98
	add	r0, sp, #8
	ldr	r1, .L107+168
	mov	r2, #64
	bl	strlcat
.L98:
	tst	r5, #64
	beq	.L99
	add	r0, sp, #8
	ldr	r1, .L107+172
	mov	r2, #64
	bl	strlcat
.L99:
	tst	r5, #128
	beq	.L100
	add	r0, sp, #8
	ldr	r1, .L107+176
	mov	r2, #64
	bl	strlcat
.L100:
	tst	r4, #16
	beq	.L101
	add	r0, sp, #8
	ldr	r1, .L107+180
	mov	r2, #64
	bl	strlcat
.L101:
	tst	r7, #1
	beq	.L102
	add	r0, sp, #8
	ldr	r1, .L107+184
	mov	r2, #64
	bl	strlcat
.L102:
	tst	r5, #1
	bne	.L103
	add	r0, sp, #8
	ldr	r1, .L107+188
	mov	r2, #64
	bl	strlcat
.L103:
	tst	r4, #2
	beq	.L104
	add	r0, sp, #8
	ldr	r1, .L107+192
	mov	r2, #64
	bl	strlcat
.L104:
	tst	r4, #1
	beq	.L105
	add	r0, sp, #8
	ldr	r1, .L107+196
	mov	r2, #64
	bl	strlcat
.L105:
	add	r0, sp, #8
	bl	strlen
	add	r3, sp, #84
.LBE7:
.LBE6:
	add	r1, sp, #8
	mov	r2, #65
.LBB9:
.LBB8:
	add	r0, r3, r0
	mov	r3, #0
	strb	r3, [r0, #-77]
.LBE8:
.LBE9:
	ldr	r0, .L107+200
	bl	strlcpy
.L69:
.LBE5:
	ldr	r3, .L107+204
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #84
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.LFE13:
	.size	STATION_HISTORY_draw_scroll_line, .-STATION_HISTORY_draw_scroll_line
	.section	.text.STATION_HISTORY_get_rain_min,"ax",%progbits
	.align	2
	.global	STATION_HISTORY_get_rain_min
	.type	STATION_HISTORY_get_rain_min, %function
STATION_HISTORY_get_rain_min:
.LFB14:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI11:
	fstmfdd	sp!, {d8}
.LCFI12:
	ldr	r4, .L110+4
	sub	sp, sp, #4
.LCFI13:
	mov	r2, sp
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L110+8
	mov	r3, #996
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [sp, #0]
	ldr	r3, .L110+12
	ldr	r0, [r4, #0]
	add	r3, r3, r2, asl #7
	ldrh	r3, [r3, #138]
	fmsr	s15, r3	@ int
	fuitos	s16, s15
	flds	s15, .L110
	fdivs	s16, s16, s15
	bl	xQueueGiveMutexRecursive
	fmrs	r0, s16
	add	sp, sp, #4
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, pc}
.L111:
	.align	2
.L110:
	.word	1092616192
	.word	station_preserves_recursive_MUTEX
	.word	.LC3
	.word	station_preserves
.LFE14:
	.size	STATION_HISTORY_get_rain_min, .-STATION_HISTORY_get_rain_min
	.section	.text.STATION_HISTORY_set_rain_min_10u,"ax",%progbits
	.align	2
	.global	STATION_HISTORY_set_rain_min_10u
	.type	STATION_HISTORY_set_rain_min_10u, %function
STATION_HISTORY_set_rain_min_10u:
.LFB15:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, lr}
.LCFI14:
	ldr	r5, .L114
	mov	r4, r2
	mov	r2, sp
	mov	r6, r1
	and	r8, r3, #255
	mov	r7, r0
	bl	STATION_PRESERVES_get_index_using_box_index_and_station_number
	ldr	r2, .L114+4
	mov	r3, #1012
	ldr	r0, [r5, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [sp, #0]
	ldr	r3, .L114+8
	add	r3, r3, r2, asl #7
	ldrh	r2, [r3, #138]
	add	r3, r3, #136
	cmp	r2, r4
	beq	.L113
	strh	r4, [r3, #2]	@ movhi
	mov	r0, r7
	mov	r1, r6
	mov	r2, r4
	mov	r3, r8
	bl	Alert_accum_rain_set_by_station
.L113:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, pc}
.L115:
	.align	2
.L114:
	.word	station_preserves_recursive_MUTEX
	.word	.LC3
	.word	station_preserves
.LFE15:
	.size	STATION_HISTORY_set_rain_min_10u, .-STATION_HISTORY_set_rain_min_10u
	.section	.text.nm_STATION_HISTORY_close_and_start_a_new_record,"ax",%progbits
	.align	2
	.global	nm_STATION_HISTORY_close_and_start_a_new_record
	.type	nm_STATION_HISTORY_close_and_start_a_new_record, %function
nm_STATION_HISTORY_close_and_start_a_new_record:
.LFB16:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L120
	stmfd	sp!, {r4, lr}
.LCFI15:
	ldr	r2, .L120+4
	mov	r4, r0
	sub	sp, sp, #36
.LCFI16:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L120+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #2112
	bcc	.L117
	ldr	r0, .L120+12
	bl	Alert_Message
	b	.L118
.L117:
	ldr	r3, .L120+16
	ldr	r2, .L120+20
	ldr	ip, [r3, #4]
	add	r4, r2, r4, asl #7
	add	lr, r4, #16
	mov	r2, #60
	add	ip, ip, #1
	mla	ip, r2, ip, r3
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr, {r0, r1, r2}
	stmia	ip, {r0, r1, r2}
	bl	nm_STATION_HISTORY_increment_next_avail_ptr
	ldrb	r3, [r4, #141]	@ zero_extendqisi2
	orr	r3, r3, #64
	strb	r3, [r4, #141]
	add	r4, r4, #140
.L118:
	ldr	r3, .L120
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #5
	ldr	r1, .L120+24
	bl	FLASH_STORAGE_if_not_running_start_file_save_timer
	mov	r0, #3600
	bl	STATION_HISTORY_start_ci_timer_if_it_is_not_running__seconds
	ldr	r3, .L120+28
	ldrsh	r3, [r3, #0]
	cmp	r3, #100
	bne	.L116
.LBB10:
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L120+32
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
.L116:
.LBE10:
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L121:
	.align	2
.L120:
	.word	station_history_completed_records_recursive_MUTEX
	.word	.LC3
	.word	1038
	.word	.LC38
	.word	.LANCHOR0
	.word	station_preserves
	.word	7200
	.word	GuiLib_CurStructureNdx
	.word	FDTO_STATION_HISTORY_redraw_scrollbox
.LFE16:
	.size	nm_STATION_HISTORY_close_and_start_a_new_record, .-nm_STATION_HISTORY_close_and_start_a_new_record
	.section	.text.STATION_HISTORY_free_report_support,"ax",%progbits
	.align	2
	.global	STATION_HISTORY_free_report_support
	.type	STATION_HISTORY_free_report_support, %function
STATION_HISTORY_free_report_support:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI17:
	ldr	r4, .L124
	ldr	r5, .L124+4
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L124+8
	ldr	r3, .L124+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, [r5, #0]
	cmp	r0, #0
	beq	.L123
	ldr	r1, .L124+8
	ldr	r2, .L124+16
	bl	mem_free_debug
	mov	r3, #0
	str	r3, [r5, #0]
.L123:
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L125:
	.align	2
.L124:
	.word	station_history_completed_records_recursive_MUTEX
	.word	.LANCHOR5
	.word	.LC3
	.word	1149
	.word	1153
.LFE17:
	.size	STATION_HISTORY_free_report_support, .-STATION_HISTORY_free_report_support
	.global	station_history_revision_record_counts
	.global	station_history_revision_record_sizes
	.global	STATION_HISTORY_FILENAME
	.global	station_history_completed
	.section	.bss.station_history_ci_timer,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	station_history_ci_timer, %object
	.size	station_history_ci_timer, 4
station_history_ci_timer:
	.space	4
	.section	.rodata.STATION_HISTORY_FILENAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	STATION_HISTORY_FILENAME, %object
	.size	STATION_HISTORY_FILENAME, 24
STATION_HISTORY_FILENAME:
	.ascii	"STATION_HISTORY_RECORDS\000"
	.section	.bss.station_history_report_ptrs,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	station_history_report_ptrs, %object
	.size	station_history_report_ptrs, 4
station_history_report_ptrs:
	.space	4
	.section	.rodata.station_history_revision_record_sizes,"a",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	station_history_revision_record_sizes, %object
	.size	station_history_revision_record_sizes, 16
station_history_revision_record_sizes:
	.word	56
	.word	56
	.word	56
	.word	60
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"STA_HSTRY file unexpd update %u\000"
.LC1:
	.ascii	"STA_HSTRY file update : to revision %u from %u\000"
.LC2:
	.ascii	"STA_HSTRY updater error\000"
.LC3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/station_history_data.c\000"
.LC4:
	.ascii	"\000"
.LC5:
	.ascii	"Timer NOT CREATED : %s, %u\000"
.LC6:
	.ascii	"REPORTS: why so many records? : %s, %u\000"
.LC7:
	.ascii	"MLB,\000"
.LC8:
	.ascii	"OFF,\000"
.LC9:
	.ascii	"Mow,\000"
.LC10:
	.ascii	"Rain1,\000"
.LC11:
	.ascii	"Rain2,\000"
.LC12:
	.ascii	"NoW1,\000"
.LC13:
	.ascii	"NoW2,\000"
.LC14:
	.ascii	"2WC,\000"
.LC15:
	.ascii	"2WS,\000"
.LC16:
	.ascii	"2WP,\000"
.LC17:
	.ascii	"ShrtP,\000"
.LC18:
	.ascii	"Shrt,\000"
.LC19:
	.ascii	"Hi,\000"
.LC20:
	.ascii	"NoC,\000"
.LC21:
	.ascii	"Rain3,\000"
.LC22:
	.ascii	"Budget,\000"
.LC23:
	.ascii	"Mois1,\000"
.LC24:
	.ascii	"MVOR,\000"
.LC25:
	.ascii	"StopT,\000"
.LC26:
	.ascii	"StopK,\000"
.LC27:
	.ascii	"SwRn,\000"
.LC28:
	.ascii	"SwFrz,\000"
.LC29:
	.ascii	"Wind,\000"
.LC30:
	.ascii	"Low,\000"
.LC31:
	.ascii	"O1,\000"
.LC32:
	.ascii	"O2,\000"
.LC33:
	.ascii	"NotChk,\000"
.LC34:
	.ascii	"MoisMax,\000"
.LC35:
	.ascii	"NoData,\000"
.LC36:
	.ascii	"WS1,\000"
.LC37:
	.ascii	"WS2,\000"
.LC38:
	.ascii	"Station History close index OOR\000"
	.section	.bss.station_history_completed,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	station_history_completed, %object
	.size	station_history_completed, 230460
station_history_completed:
	.space	230460
	.section	.rodata.station_history_revision_record_counts,"a",%progbits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	station_history_revision_record_counts, %object
	.size	station_history_revision_record_counts, 16
station_history_revision_record_counts:
	.word	10752
	.word	10752
	.word	3840
	.word	3840
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI5-.LFB6
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI6-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI7-.LFB11
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI8-.LFB13
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x20
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x74
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI11-.LFB14
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x10
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x14
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI14-.LFB15
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI15-.LFB16
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI17-.LFB17
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE32:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/station_history_data.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x181
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF17
	.byte	0x1
	.4byte	.LASF18
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x2c6
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x190
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x4e
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF1
	.byte	0x1
	.byte	0x55
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF2
	.byte	0x1
	.byte	0xfa
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x152
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x17a
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x199
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1b7
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x1c6
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST5
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1fe
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x246
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x266
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x384
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x3dc
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST8
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x3ee
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST9
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x401
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST10
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x477
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST11
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB8
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB11
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB13
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI10
	.4byte	.LFE13
	.2byte	0x3
	.byte	0x7d
	.sleb128 116
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB14
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI13
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB15
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB16
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI16
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB17
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x9c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF14:
	.ascii	"STATION_HISTORY_set_rain_min_10u\000"
.LASF16:
	.ascii	"STATION_HISTORY_free_report_support\000"
.LASF2:
	.ascii	"nm_station_history_updater\000"
.LASF10:
	.ascii	"nm_STATION_HISTORY_get_most_recently_completed_reco"
	.ascii	"rd\000"
.LASF5:
	.ascii	"save_file_station_history\000"
.LASF17:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"nm_init_station_history_records\000"
.LASF18:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/station_history_data.c\000"
.LASF8:
	.ascii	"nm_STATION_HISTORY_increment_next_avail_ptr\000"
.LASF0:
	.ascii	"station_history_ci_timer_callback\000"
.LASF6:
	.ascii	"STATION_HISTORY_start_ci_timer_if_it_is_not_running"
	.ascii	"__seconds\000"
.LASF4:
	.ascii	"init_file_station_history\000"
.LASF7:
	.ascii	"nm_STATION_HISTORY_inc_index\000"
.LASF3:
	.ascii	"nm_init_station_history_record\000"
.LASF12:
	.ascii	"STATION_HISTORY_draw_scroll_line\000"
.LASF15:
	.ascii	"nm_STATION_HISTORY_close_and_start_a_new_record\000"
.LASF11:
	.ascii	"STATION_HISTORY_fill_ptrs_and_return_how_many_lines"
	.ascii	"\000"
.LASF19:
	.ascii	"STATION_HISTORY_get_flag_str\000"
.LASF9:
	.ascii	"nm_STATION_HISTORY_get_previous_completed_record\000"
.LASF13:
	.ascii	"STATION_HISTORY_get_rain_min\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
