	.file	"GuiDisplay.c"
	.text
.Ltext0:
	.section	.text.GuiDisplay_Refresh,"ax",%progbits
	.align	2
	.global	GuiDisplay_Refresh
	.type	GuiDisplay_Refresh, %function
GuiDisplay_Refresh:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
	ldr	r0, .L9
	ldr	r6, .L9+4
	ldr	r5, .L9+8
	mov	r2, #0
	mov	r3, r2
	mvn	fp, #0
.L6:
	add	ip, r3, r0
	ldrsh	r1, [ip, #2]
	ldrh	sl, [ip, #2]
	cmp	r1, #0
	blt	.L2
	ldrsh	r7, [r0, r3]
	cmp	r1, #158
	movgt	sl, #159
	add	r7, r7, r2
	mov	sl, sl, asl #16
	add	r8, r7, r6
	mov	r1, #0
	mov	sl, sl, asr #16
	ldrh	r4, [r3, r0]
	add	r7, r7, r5
	b	.L4
.L5:
	ldrb	r9, [r7, r1]	@ zero_extendqisi2
	add	r4, r4, #1
	mov	r4, r4, asl #16
	strb	r9, [r8, r1]
	mov	r4, r4, lsr #16
	add	r1, r1, #1
.L4:
	mov	r9, r4, asl #16
	cmp	sl, r9, asr #16
	bge	.L5
	strh	fp, [ip, #2]	@ movhi
.L2:
	add	r3, r3, #4
	cmp	r3, #960
	add	r2, r2, #160
	bne	.L6
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L10:
	.align	2
.L9:
	.word	GuiLib_DisplayRepaint
	.word	primary_display_buf
	.word	gui_lib_display_buf
.LFE0:
	.size	GuiDisplay_Refresh, .-GuiDisplay_Refresh
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE0:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/library_src/GuiDisplay.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x33
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF0
	.byte	0x1
	.4byte	.LASF1
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x2f9c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/easyGUI/"
	.ascii	"library_src/GuiDisplay.c\000"
.LASF0:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"GuiDisplay_Refresh\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
