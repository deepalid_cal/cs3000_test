	.file	"e_station_decoder_list.c"
	.text
.Ltext0:
	.section	.text.TWO_WIRE_STA_copy_decoder_info_into_guivars,"ax",%progbits
	.align	2
	.type	TWO_WIRE_STA_copy_decoder_info_into_guivars, %function
TWO_WIRE_STA_copy_decoder_info_into_guivars:
.LFB2:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
	ldr	r6, .L14
	mov	r5, r0
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L14+4
	mov	r4, #0
	str	r4, [r3, #0]
	mov	r1, r4
	mov	r2, #5
	str	r4, [r6, #0]
	ldr	r7, .L14+8
	ldr	r8, .L14+12
	ldr	sl, .L14+16
	mov	r9, r0
	ldr	r0, .L14+20
	bl	memset
	mov	r1, r4
	mov	r2, #5
	ldr	r0, .L14+24
	bl	memset
	ldr	r3, .L14+28
	str	r4, [r7, #0]
	str	r4, [r3, #0]
	ldr	r3, .L14+32
	mov	r1, #400
	str	r4, [r3, #0]
	ldr	r3, .L14+36
	ldr	r2, .L14+40
	str	r4, [r3, #0]
	ldr	r4, .L14+44
	mov	r3, #272
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [r8, r5, asl #3]
	ldr	r2, .L14+48
	str	r3, [sl, #0]
	add	r3, r8, r5, asl #3
	ldrh	r1, [r3, #4]
	ldr	r0, [r4, #0]
	str	r1, [r2, #0]
	ldrb	r2, [r3, #6]	@ zero_extendqisi2
	ldr	r3, .L14+52
	str	r2, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, [sl, #0]
	cmp	r3, #0
	beq	.L1
	ldr	r3, .L14+56
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L14+40
	ldr	r3, .L14+60
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L14+64
	bl	nm_ListGetFirst
	mov	sl, r7
	mov	r4, r0
	b	.L3
.L10:
	mov	r0, r4
	bl	nm_STATION_get_box_index_0
	cmp	r0, r9
	bne	.L4
	mov	r0, r4
	bl	nm_STATION_get_decoder_serial_number
	mov	r3, r0
	mov	r0, r4
	str	r3, [sp, #0]
	bl	nm_STATION_get_decoder_output
	mov	r7, r0
	mov	r0, r4
	bl	nm_STATION_get_station_number_0
	ldr	r1, [r8, r5, asl #3]
	ldr	r3, [sp, #0]
	adds	r2, r0, #0
	movne	r2, #1
	cmp	r3, r1
	movne	r2, #0
	cmp	r2, #0
	mov	fp, r0
	beq	.L4
	cmp	r7, #0
	bne	.L5
	ldr	r7, .L14+4
	str	r0, [r7, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r1, fp
	ldr	r2, .L14+20
	mov	r3, #5
	bl	STATION_get_station_number_string
	ldr	r2, [r7, #0]
	ldr	r3, .L14+28
	mov	r0, r4
	str	r2, [r3, #0]
	bl	nm_STATION_get_description
	ldr	r1, .L14+68
	mov	r2, #49
	bl	strncmp
	cmp	r0, #0
	beq	.L6
	mov	r0, r4
	bl	nm_STATION_get_description
	mov	r1, r0
	ldr	r0, .L14+72
	b	.L11
.L6:
	ldr	r0, .L14+72
	ldr	r1, .L14+76
.L11:
	mov	r2, #49
	bl	strlcpy
	ldr	r3, .L14+32
	mov	r2, #1
	str	r2, [r3, #0]
	mov	r2, #0
	ldr	r3, .L14+80
	b	.L13
.L5:
	cmp	r7, #1
	bne	.L4
	str	r0, [r6, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r1, fp
	ldr	r2, .L14+24
	mov	r3, #5
	bl	STATION_get_station_number_string
	ldr	r3, [r6, #0]
	mov	r0, r4
	str	r3, [sl, #0]
	bl	nm_STATION_get_description
	ldr	r1, .L14+68
	mov	r2, #49
	bl	strncmp
	cmp	r0, #0
	beq	.L8
	mov	r0, r4
	bl	nm_STATION_get_description
	mov	r1, r0
	ldr	r0, .L14+84
	b	.L12
.L8:
	ldr	r0, .L14+84
	ldr	r1, .L14+76
.L12:
	mov	r2, #49
	bl	strlcpy
	ldr	r3, .L14+36
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L14+88
	mov	r2, #0
.L13:
	str	r2, [r3, #0]
.L4:
	mov	r1, r4
	ldr	r0, .L14+64
	bl	nm_ListGetNext
	mov	r4, r0
.L3:
	cmp	r4, #0
	bne	.L10
	ldr	r3, .L14+56
	ldr	r0, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	xQueueGiveMutexRecursive
.L1:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L15:
	.align	2
.L14:
	.word	GuiVar_TwoWireStaB
	.word	GuiVar_TwoWireStaA
	.word	.LANCHOR1
	.word	decoder_info_for_display
	.word	GuiVar_TwoWireDecoderSN
	.word	GuiVar_TwoWireStaStrA
	.word	GuiVar_TwoWireStaStrB
	.word	.LANCHOR0
	.word	GuiVar_TwoWireStaAIsSet
	.word	GuiVar_TwoWireStaBIsSet
	.word	.LC0
	.word	tpmicro_data_recursive_MUTEX
	.word	GuiVar_TwoWireDecoderFWVersion
	.word	GuiVar_TwoWireDecoderType
	.word	list_program_data_recursive_MUTEX
	.word	285
	.word	station_info_list_hdr
	.word	.LC1
	.word	GuiVar_TwoWireDescA
	.word	.LC2
	.word	GuiVar_TwoWireOutputOnA
	.word	GuiVar_TwoWireDescB
	.word	GuiVar_TwoWireOutputOnB
.LFE2:
	.size	TWO_WIRE_STA_copy_decoder_info_into_guivars, .-TWO_WIRE_STA_copy_decoder_info_into_guivars
	.section	.text.TWO_WIRE_STA_process_station_number,"ax",%progbits
	.align	2
	.type	TWO_WIRE_STA_process_station_number, %function
TWO_WIRE_STA_process_station_number:
.LFB0:
	@ args = 16, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI1:
	ldr	r6, [sp, #44]
	str	r2, [sp, #0]
	ldr	r2, [sp, #48]
	mov	r8, r3
	mov	fp, r0
	mov	r4, r1
	str	r2, [sp, #4]
	ldr	r5, [sp, #52]
	ldr	r7, [sp, #56]
	bl	good_key_beep
	mov	r3, #1
	str	r3, [r8, #0]
	ldr	r3, .L41
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L41+4
	mov	r3, #103
	bl	xQueueTakeMutexRecursive_debug
	mov	r9, #0
.L36:
	cmp	fp, #84
	ldr	r3, [r4, #0]
	bne	.L17
	cmp	r3, #0
	moveq	r3, #48
	addne	r3, r3, #1
	str	r3, [r4, #0]
	ldr	r3, [r4, #0]
	cmp	r3, #175
	bls	.L20
	b	.L39
.L17:
	cmp	r3, #48
	bne	.L21
.L39:
	str	r9, [r4, #0]
	b	.L20
.L21:
	subhi	r3, r3, #1
	movls	r2, #175
	strhi	r3, [r4, #0]
	strls	r2, [r4, #0]
.L20:
	bl	FLOWSENSE_get_controller_index
	ldr	r1, [r4, #0]
	ldr	r2, [sp, #0]
	mov	r3, #5
	bl	STATION_get_station_number_string
	ldr	r3, [r4, #0]
	adds	r3, r3, #0
	movne	r3, #1
	str	r3, [r8, #0]
	bl	FLOWSENSE_get_controller_index
	ldr	r1, [r4, #0]
	bl	nm_STATION_get_pointer_to_station
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	sl, r0
	beq	.L23
	cmp	r0, #0
	bne	.L24
	cmp	r3, r5
	bne	.L25
.L24:
	ldr	r2, [sp, #4]
	cmp	r3, r2
	bne	.L26
	cmp	r3, r5
	bne	.L23
.L26:
	cmp	r3, r7
	bne	.L27
	cmp	r7, r5
	bne	.L23
.L27:
	cmp	sl, #0
	beq	.L36
	mov	r0, sl
	bl	nm_STATION_get_decoder_serial_number
	cmp	r0, #0
	bne	.L36
	ldr	r3, [r4, #0]
	cmp	r3, r5
	beq	.L36
.L33:
	mov	r0, sl
	bl	nm_STATION_get_description
	ldr	r1, .L41+8
	mov	r2, #49
	bl	strncmp
	cmp	r0, #0
	beq	.L25
	mov	r0, sl
	bl	nm_STATION_get_description
	mov	r1, r0
	mov	r0, r6
	b	.L40
.L25:
	ldr	r1, .L41+12
	mov	r0, r6
.L40:
	mov	r2, #49
	bl	strlcpy
	ldr	r3, .L41
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	Refresh_Screen
.L23:
	cmp	sl, #0
	bne	.L33
	b	.L25
.L42:
	.align	2
.L41:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	.LC1
	.word	.LC2
.LFE0:
	.size	TWO_WIRE_STA_process_station_number, .-TWO_WIRE_STA_process_station_number
	.section	.text.FDTO_TWO_WIRE_STA_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_TWO_WIRE_STA_return_to_menu, %function
FDTO_TWO_WIRE_STA_return_to_menu:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L44
	ldr	r1, .L44+4
	b	FDTO_GROUP_return_to_menu
.L45:
	.align	2
.L44:
	.word	.LANCHOR2
	.word	TWO_WIRE_STA_extract_and_store_changes_from_guivars
.LFE6:
	.size	FDTO_TWO_WIRE_STA_return_to_menu, .-FDTO_TWO_WIRE_STA_return_to_menu
	.section	.text.TWO_WIRE_STA_extract_and_store_changes_from_guivars_for_output,"ax",%progbits
	.align	2
	.type	TWO_WIRE_STA_extract_and_store_changes_from_guivars_for_output, %function
TWO_WIRE_STA_extract_and_store_changes_from_guivars_for_output:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI2:
	mov	fp, r3
	ldr	r3, .L50
	mov	r6, r1
	mov	sl, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r9, r2
	ldr	r3, .L50+4
	ldr	r2, .L50+8
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_get_controller_index
	cmp	r6, #47
	mov	r4, r0
	bls	.L47
	mov	r1, r6
	bl	nm_STATION_get_pointer_to_station
	subs	r7, r0, #0
	bne	.L48
	mov	r0, r4
	mov	r1, r6
	mov	r2, #2
	bl	nm_STATION_create_new_station
	mov	r7, r0
.L48:
	mov	r1, #2
	mov	r0, r7
	bl	STATION_get_change_bits_ptr
	mov	r5, #1
	mov	r1, r5
	mov	r2, #0
	mov	r3, #12
	str	r4, [sp, #0]
	mov	r8, r0
	mov	r0, r7
	stmib	sp, {r5, r8}
	bl	nm_STATION_set_physically_available
	ldr	r3, .L50+12
	stmia	sp, {r4, r5, r8}
	ldr	r1, [r3, #0]
	mov	r0, r7
	mov	r2, r5
	mov	r3, #2
	bl	nm_STATION_set_decoder_serial_number
	mov	r0, r7
	mov	r1, sl
	mov	r2, r5
	mov	r3, #2
	stmia	sp, {r4, r5, r8}
	bl	nm_STATION_set_decoder_output
.L47:
	ldr	r1, [r9, #0]
	cmp	r1, #47
	bls	.L49
	cmp	r1, r6
	beq	.L49
	cmp	r1, fp
	beq	.L49
	mov	r0, r4
	bl	nm_STATION_get_pointer_to_station
	subs	r7, r0, #0
	beq	.L49
	mov	r1, #2
	bl	STATION_get_change_bits_ptr
	mov	r5, #1
	mov	r1, #0
	mov	r2, r5
	mov	r3, #2
	str	r4, [sp, #0]
	mov	r8, r0
	mov	r0, r7
	stmib	sp, {r5, r8}
	bl	nm_STATION_set_decoder_serial_number
	mov	r0, r7
	mov	r1, sl
	mov	r2, #0
	mov	r3, #2
	stmia	sp, {r4, r5, r8}
	bl	nm_STATION_set_decoder_output
	mov	r1, #0
	mov	r0, r7
	mov	r2, r1
	mov	r3, #12
	stmia	sp, {r4, r5, r8}
	bl	nm_STATION_set_physically_available
.L49:
	ldr	r3, .L50
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	str	r6, [r9, #0]
	ldmfd	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L51:
	.align	2
.L50:
	.word	list_program_data_recursive_MUTEX
	.word	402
	.word	.LC0
	.word	GuiVar_TwoWireDecoderSN
.LFE3:
	.size	TWO_WIRE_STA_extract_and_store_changes_from_guivars_for_output, .-TWO_WIRE_STA_extract_and_store_changes_from_guivars_for_output
	.section	.text.TWO_WIRE_STA_extract_and_store_changes_from_guivars,"ax",%progbits
	.align	2
	.type	TWO_WIRE_STA_extract_and_store_changes_from_guivars, %function
TWO_WIRE_STA_extract_and_store_changes_from_guivars:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	ldr	r4, .L53
	ldr	r5, .L53+4
	ldr	r1, [r4, #0]
	ldr	r3, [r5, #0]
	mov	r0, #0
	ldr	r2, .L53+8
	bl	TWO_WIRE_STA_extract_and_store_changes_from_guivars_for_output
	ldr	r1, [r5, #0]
	ldr	r2, .L53+12
	ldr	r3, [r4, #0]
	mov	r0, #1
	ldmfd	sp!, {r4, r5, lr}
	b	TWO_WIRE_STA_extract_and_store_changes_from_guivars_for_output
.L54:
	.align	2
.L53:
	.word	GuiVar_TwoWireStaA
	.word	GuiVar_TwoWireStaB
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE4:
	.size	TWO_WIRE_STA_extract_and_store_changes_from_guivars, .-TWO_WIRE_STA_extract_and_store_changes_from_guivars
	.section	.text.TWO_WIRE_STA_load_decoder_serial_number_into_guivar,"ax",%progbits
	.align	2
	.global	TWO_WIRE_STA_load_decoder_serial_number_into_guivar
	.type	TWO_WIRE_STA_load_decoder_serial_number_into_guivar, %function
TWO_WIRE_STA_load_decoder_serial_number_into_guivar:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI4:
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	mov	r1, #0
	ldr	r0, .L56
	bl	GuiLib_GetTextPtr
	ldr	r2, .L56+4
	mov	r1, #49
	ldr	r2, [r2, r4, asl #3]
	str	r2, [sp, #0]
	ldr	r2, .L56+8
	mov	r3, r0
	ldr	r0, .L56+12
	bl	snprintf
	ldmfd	sp!, {r3, r4, pc}
.L57:
	.align	2
.L56:
	.word	1041
	.word	decoder_info_for_display
	.word	.LC3
	.word	GuiVar_itmGroupName
.LFE7:
	.size	TWO_WIRE_STA_load_decoder_serial_number_into_guivar, .-TWO_WIRE_STA_load_decoder_serial_number_into_guivar
	.section	.text.TWO_WIRE_STA_turn_off_active_outputs,"ax",%progbits
	.align	2
	.type	TWO_WIRE_STA_turn_off_active_outputs, %function
TWO_WIRE_STA_turn_off_active_outputs:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L61
	str	lr, [sp, #-4]!
.LCFI5:
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L59
	ldr	r3, .L61+4
	mov	r1, #0
	ldr	r0, [r3, #0]
	mov	r2, r1
	bl	TWO_WIRE_decoder_solenoid_operation_from_ui
.L59:
	ldr	r3, .L61+8
	ldr	r1, [r3, #0]
	cmp	r1, #1
	ldrne	pc, [sp], #4
.LBB4:
	ldr	r3, .L61+4
	mov	r2, #0
	ldr	r0, [r3, #0]
.LBE4:
	ldr	lr, [sp], #4
.LBB5:
	b	TWO_WIRE_decoder_solenoid_operation_from_ui
.L62:
	.align	2
.L61:
	.word	GuiVar_TwoWireOutputOnA
	.word	GuiVar_TwoWireDecoderSN
	.word	GuiVar_TwoWireOutputOnB
.LBE5:
.LFE1:
	.size	TWO_WIRE_STA_turn_off_active_outputs, .-TWO_WIRE_STA_turn_off_active_outputs
	.section	.text.TWO_WIRE_STA_process_decoder_list,"ax",%progbits
	.align	2
	.type	TWO_WIRE_STA_process_decoder_list, %function
TWO_WIRE_STA_process_decoder_list:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, lr}
.LCFI6:
	mov	r6, r0
	beq	.L69
	bhi	.L73
	cmp	r0, #1
	beq	.L66
	bcc	.L65
	cmp	r0, #2
	beq	.L67
	cmp	r0, #3
	bne	.L64
	b	.L101
.L73:
	cmp	r0, #67
	beq	.L71
	bhi	.L74
	cmp	r0, #16
	beq	.L70
	cmp	r0, #20
	bne	.L64
	b	.L70
.L74:
	cmp	r0, #80
	beq	.L72
	cmp	r0, #84
	bne	.L64
	b	.L72
.L67:
	ldr	r3, .L106
	ldrsh	r3, [r3, #0]
	cmp	r3, #1
	beq	.L76
	cmp	r3, #3
	bne	.L94
	b	.L102
.L76:
	ldr	r3, .L106+4
	mov	r1, #0
	ldr	r2, [r3, #0]
	ldr	r3, .L106+8
	ldr	r0, [r3, #0]
	rsb	r3, r1, r2
	rsbs	r2, r3, #0
	adc	r2, r2, r3
	b	.L97
.L102:
	ldr	r3, .L106+12
	mov	r1, #1
	ldr	r2, [r3, #0]
	ldr	r3, .L106+8
	rsbs	r2, r2, #1
	ldr	r0, [r3, #0]
	movcc	r2, #0
.L97:
	bl	TWO_WIRE_decoder_solenoid_operation_from_ui
.L98:
	mov	r0, #0
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Redraw_Screen
.L72:
	ldr	r3, .L106
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L79
	cmp	r3, #2
	bne	.L94
	b	.L103
.L79:
	ldr	r3, .L106+16
	mov	r0, r6
	str	r3, [sp, #0]
	ldr	r3, .L106+20
	ldr	r1, .L106+24
	ldr	r3, [r3, #0]
	ldr	r2, .L106+28
	str	r3, [sp, #4]
	ldr	r3, .L106+32
	ldr	r3, [r3, #0]
	str	r3, [sp, #8]
	ldr	r3, .L106+36
	ldr	r3, [r3, #0]
	str	r3, [sp, #12]
	ldr	r3, .L106+40
	b	.L96
.L103:
	ldr	r3, .L106+44
	ldr	r1, .L106+32
	str	r3, [sp, #0]
	ldr	r3, .L106+36
	ldr	r2, .L106+48
	ldr	r3, [r3, #0]
	mov	r0, r6
	str	r3, [sp, #4]
	ldr	r3, .L106+24
	ldr	r3, [r3, #0]
	str	r3, [sp, #8]
	ldr	r3, .L106+20
	ldr	r3, [r3, #0]
	str	r3, [sp, #12]
	ldr	r3, .L106+52
.L96:
	bl	TWO_WIRE_STA_process_station_number
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, pc}
.L70:
	bl	TWO_WIRE_STA_turn_off_active_outputs
	ldr	r3, .L106+56
	ldr	r4, .L106+60
	str	r3, [sp, #0]
	ldr	r3, .L106+64
	ldr	r2, .L106+68
	ldr	r1, [r3, #0]
	mov	r0, r6
	mov	r3, #0
	ldr	r5, [r4, #0]
	bl	GROUP_process_NEXT_and_PREV
	ldr	r3, .L106+72
	ldr	r2, [r3, #0]
	ldr	r3, .L106+76
	add	r2, r2, #1
	str	r2, [r3, #0]
	ldr	r3, [r4, #0]
	cmp	r5, r3
	bne	.L98
.L82:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Refresh_Screen
.L69:
	ldr	r3, .L106
	ldrsh	r3, [r3, #0]
	cmp	r3, #2
	beq	.L84
	cmp	r3, #3
	bne	.L94
	b	.L104
.L84:
	mov	r0, #0
	b	.L100
.L104:
	mov	r0, #1
	mov	r1, r0
.L99:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	CURSOR_Select
.L65:
	ldr	r3, .L106
	ldrsh	r1, [r3, #0]
	cmp	r1, #0
	moveq	r0, #2
	beq	.L100
	cmp	r1, #1
	bne	.L94
	b	.L105
.L100:
	mov	r1, #1
	b	.L99
.L105:
	mov	r0, #3
	b	.L99
.L94:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	bad_key_beep
.L66:
	ldr	r3, .L106
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L71
.L95:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	CURSOR_Up
.L101:
	mov	r0, #1
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	CURSOR_Down
.L71:
	bl	TWO_WIRE_STA_turn_off_active_outputs
	ldr	r0, .L106+80
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	KEY_process_BACK_from_editing_screen
.L64:
	mov	r0, r6
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	b	KEY_process_global_keys
.L107:
	.align	2
.L106:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_TwoWireOutputOnA
	.word	GuiVar_TwoWireDecoderSN
	.word	GuiVar_TwoWireOutputOnB
	.word	GuiVar_TwoWireDescA
	.word	.LANCHOR0
	.word	GuiVar_TwoWireStaA
	.word	GuiVar_TwoWireStaStrA
	.word	GuiVar_TwoWireStaB
	.word	.LANCHOR1
	.word	GuiVar_TwoWireStaAIsSet
	.word	GuiVar_TwoWireDescB
	.word	GuiVar_TwoWireStaStrB
	.word	GuiVar_TwoWireStaBIsSet
	.word	TWO_WIRE_STA_copy_decoder_info_into_guivars
	.word	GuiVar_TwoWireDecoderType
	.word	GuiVar_TwoWireNumDiscoveredStaDecoders
	.word	TWO_WIRE_STA_extract_and_store_changes_from_guivars
	.word	g_GROUP_list_item_index
	.word	GuiVar_TwoWireStaDecoderIndex
	.word	FDTO_TWO_WIRE_STA_return_to_menu
.LFE5:
	.size	TWO_WIRE_STA_process_decoder_list, .-TWO_WIRE_STA_process_decoder_list
	.section	.text.FDTO_TWO_WIRE_STA_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_TWO_WIRE_STA_draw_menu
	.type	FDTO_TWO_WIRE_STA_draw_menu, %function
FDTO_TWO_WIRE_STA_draw_menu:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI7:
	mov	r5, r0
	bne	.L109
	ldr	r3, .L115
	mov	r4, #0
	str	r4, [r3, #0]
	mov	r2, #640
	ldr	r0, .L115+4
	mov	r1, r4
	bl	memset
	ldr	r3, .L115+8
	ldr	r2, .L115+4
	add	ip, r3, #4800
.L112:
	ldr	r1, [r3, #220]
	cmp	r1, #0
	beq	.L114
.L110:
	ldrb	r0, [r3, #224]	@ zero_extendqisi2
	sub	lr, r0, #2
	cmp	lr, #1
	bhi	.L111
	ldrh	lr, [r3, #226]
	str	r1, [r2, r4, asl #3]
	add	r1, r2, r4, asl #3
	add	r4, r4, #1
	strh	lr, [r1, #4]	@ movhi
	strb	r0, [r1, #6]
.L111:
	add	r3, r3, #60
	cmp	r3, ip
	bne	.L112
.L114:
	ldr	r3, .L115+12
	str	r4, [r3, #0]
.L109:
	ldr	r3, .L115+16
	mov	r0, r5
	str	r3, [sp, #0]
	ldr	r3, .L115+20
	ldr	r1, .L115+24
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	ldr	r3, .L115+12
	ldr	r2, [r3, #0]
	mov	r3, #66
	bl	FDTO_GROUP_draw_menu
	ldr	r3, .L115
	ldr	r2, [r3, #0]
	ldr	r3, .L115+28
	add	r2, r2, #1
	str	r2, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	b	Refresh_Screen
.L116:
	.align	2
.L115:
	.word	g_GROUP_list_item_index
	.word	decoder_info_for_display
	.word	tpmicro_data
	.word	GuiVar_TwoWireNumDiscoveredStaDecoders
	.word	TWO_WIRE_STA_load_decoder_serial_number_into_guivar
	.word	TWO_WIRE_STA_copy_decoder_info_into_guivars
	.word	.LANCHOR2
	.word	GuiVar_TwoWireStaDecoderIndex
.LFE8:
	.size	FDTO_TWO_WIRE_STA_draw_menu, .-FDTO_TWO_WIRE_STA_draw_menu
	.section	.text.TWO_WIRE_STA_process_menu,"ax",%progbits
	.align	2
	.global	TWO_WIRE_STA_process_menu
	.type	TWO_WIRE_STA_process_menu, %function
TWO_WIRE_STA_process_menu:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L119
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI8:
	ldr	ip, [ip, #0]
	mov	r3, r0
	cmp	ip, #0
	mov	r2, r1
	bne	.L118
	cmp	r0, #67
	bne	.L118
	ldr	r3, .L119+4
	ldr	r2, .L119+8
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r2, [r3, #4]
	ldr	r3, .L119+12
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	add	sp, sp, #16
	ldr	lr, [sp], #4
	b	TWO_WIRE_ASSIGNMENT_draw_dialog
.L118:
	ldr	r1, .L119+16
	mov	r0, r3
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	str	r1, [sp, #8]
	ldr	r3, .L119+20
	ldr	r1, .L119+24
	str	r1, [sp, #12]
	mov	r1, r2
	ldr	r3, [r3, #0]
	ldr	r2, .L119
	bl	GROUP_process_menu
	ldr	r3, .L119+28
	ldr	r2, [r3, #0]
	ldr	r3, .L119+32
	add	r2, r2, #1
	str	r2, [r3, #0]
	add	sp, sp, #16
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L120:
	.align	2
.L119:
	.word	.LANCHOR2
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
	.word	TWO_WIRE_STA_process_decoder_list
	.word	GuiVar_TwoWireNumDiscoveredStaDecoders
	.word	TWO_WIRE_STA_copy_decoder_info_into_guivars
	.word	g_GROUP_list_item_index
	.word	GuiVar_TwoWireStaDecoderIndex
.LFE9:
	.size	TWO_WIRE_STA_process_menu, .-TWO_WIRE_STA_process_menu
	.section	.bss.g_TWO_WIRE_STA_previously_selected_station_A,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_TWO_WIRE_STA_previously_selected_station_A, %object
	.size	g_TWO_WIRE_STA_previously_selected_station_A, 4
g_TWO_WIRE_STA_previously_selected_station_A:
	.space	4
	.section	.bss.g_TWO_WIRE_STA_previously_selected_station_B,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_TWO_WIRE_STA_previously_selected_station_B, %object
	.size	g_TWO_WIRE_STA_previously_selected_station_B, 4
g_TWO_WIRE_STA_previously_selected_station_B:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_station_decoder_list.c\000"
.LC1:
	.ascii	"(description not yet set)\000"
.LC2:
	.ascii	"\000"
.LC3:
	.ascii	"  %s %07d\000"
	.section	.bss.g_TWO_WIRE_STA_editing_decoder,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_TWO_WIRE_STA_editing_decoder, %object
	.size	g_TWO_WIRE_STA_editing_decoder, 4
g_TWO_WIRE_STA_editing_decoder:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI1-.LFB0
	.byte	0xe
	.uleb128 0x2c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x81
	.uleb128 0xa
	.byte	0x80
	.uleb128 0xb
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x30
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x82
	.uleb128 0xa
	.byte	0x81
	.uleb128 0xb
	.byte	0x80
	.uleb128 0xc
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI4-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI5-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI6-.LFB5
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI7-.LFB8
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI8-.LFB9
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE18:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_station_decoder_list.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xeb
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF9
	.byte	0x1
	.4byte	.LASF10
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF11
	.byte	0x1
	.byte	0xd5
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.byte	0xf2
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x57
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x28a
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x18a
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1de
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x290
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST4
	.uleb128 0x7
	.4byte	0x21
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST5
	.uleb128 0x5
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1f5
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST6
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x2a8
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST7
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x2e7
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB7
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB1
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB8
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB9
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x64
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF6:
	.ascii	"TWO_WIRE_STA_load_decoder_serial_number_into_guivar"
	.ascii	"\000"
.LASF1:
	.ascii	"TWO_WIRE_STA_process_station_number\000"
.LASF8:
	.ascii	"TWO_WIRE_STA_process_menu\000"
.LASF9:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"TWO_WIRE_STA_copy_decoder_info_into_guivars\000"
.LASF11:
	.ascii	"TWO_WIRE_STA_turn_off_active_outputs\000"
.LASF2:
	.ascii	"FDTO_TWO_WIRE_STA_return_to_menu\000"
.LASF4:
	.ascii	"TWO_WIRE_STA_extract_and_store_changes_from_guivars"
	.ascii	"\000"
.LASF7:
	.ascii	"FDTO_TWO_WIRE_STA_draw_menu\000"
.LASF3:
	.ascii	"TWO_WIRE_STA_extract_and_store_changes_from_guivars"
	.ascii	"_for_output\000"
.LASF5:
	.ascii	"TWO_WIRE_STA_process_decoder_list\000"
.LASF10:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_station_decoder_list.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
