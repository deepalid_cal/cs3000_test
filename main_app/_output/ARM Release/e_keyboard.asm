	.file	"e_keyboard.c"
	.text
.Ltext0:
	.section	.text.process_OK_button,"ax",%progbits
	.align	2
	.type	process_OK_button, %function
process_OK_button:
.LFB4:
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r1, .L14
	sub	sp, sp, #48
.LCFI1:
	mov	r2, #8
	mov	r4, r0
	add	r0, sp, #40
	bl	memcpy
	bl	good_key_beep
	ldr	r0, .L14+4
	bl	trim_white_space
	mov	r2, #65
	mov	r1, r0
	ldr	r0, .L14+4
	bl	strlcpy
	ldr	r0, .L14+4
	bl	strlen
	cmp	r0, #0
	bne	.L2
	ldr	r3, .L14+8
	cmp	r4, r3
	ldreq	r0, .L14+4
	ldreq	r1, .L14+12
	beq	.L13
.L3:
	ldr	r3, .L14+16
	cmp	r4, r3
	bne	.L4
	ldr	r0, .L14+4
	ldr	r1, .L14+20
.L13:
	mov	r2, #65
	bl	strlcpy
	b	.L2
.L4:
	ldr	r3, .L14+24
	cmp	r4, r3
	bne	.L5
	ldr	r3, .L14+28
	mov	r1, #65
	str	r3, [sp, #0]
	ldr	r2, .L14+32
	add	r3, sp, #40
	ldr	r0, .L14+4
	bl	snprintf
	b	.L6
.L5:
	ldr	r3, .L14+36
	cmp	r4, r3
	ldreq	r3, .L14+40
	beq	.L12
	ldr	r3, .L14+44
	cmp	r4, r3
	bne	.L8
	ldr	r3, .L14+48
.L12:
	str	r3, [sp, #0]
	mov	r1, #65
	ldr	r2, .L14+32
	add	r3, sp, #40
	ldr	r0, .L14+4
	bl	snprintf
	b	.L2
.L8:
	ldr	r3, .L14+52
	cmp	r4, r3
	ldreq	r3, .L14+56
	beq	.L12
	ldr	r1, .L14+60
	mov	r2, #65
	ldr	r0, .L14+4
	bl	strlcpy
	b	.L10
.L2:
	ldr	r3, .L14+24
	cmp	r4, r3
	beq	.L6
	ldr	r3, .L14+36
	cmp	r4, r3
	beq	.L6
.L10:
	ldr	r3, .L14+44
	cmp	r4, r3
	beq	.L6
	ldr	r3, .L14+52
	cmp	r4, r3
	bne	.L11
.L6:
	ldr	r3, .L14+64
	mov	r0, #0
	ldr	r1, [r3, #0]
	bl	SCROLL_BOX_redraw_line
.L11:
.LBB4:
	mov	r3, #2
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #28]
	ldr	r3, .L14+68
	add	r0, sp, #4
	ldr	r2, [r3, #0]
	ldr	r3, .L14+72
	str	r4, [sp, #24]
	strh	r2, [r3, #0]	@ movhi
	bl	Display_Post_Command
.LBE4:
	add	sp, sp, #48
	ldmfd	sp!, {r4, pc}
.L15:
	.align	2
.L14:
	.word	.LC0
	.word	GuiVar_GroupName
	.word	FDTO_ABOUT_draw_screen
	.word	CONTROLLER_DEFAULT_NAME
	.word	FDTO_STATION_draw_menu
	.word	.LC1
	.word	FDTO_MANUAL_PROGRAMS_draw_menu
	.word	MANUAL_PROGRAMS_DEFAULT_NAME
	.word	.LC2
	.word	FDTO_POC_draw_menu
	.word	POC_DEFAULT_NAME
	.word	FDTO_STATION_GROUP_draw_menu
	.word	STATION_GROUP_DEFAULT_NAME
	.word	FDTO_SYSTEM_draw_menu
	.word	IRRIGATION_SYSTEM_DEFAULT_NAME
	.word	.LC3
	.word	g_GROUP_list_item_index
	.word	.LANCHOR0
	.word	GuiLib_ActiveCursorFieldNo
.LFE4:
	.size	process_OK_button, .-process_OK_button
	.section	.text.FDTO_show_keyboard,"ax",%progbits
	.align	2
	.type	FDTO_show_keyboard, %function
FDTO_show_keyboard:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldreq	r2, .L18
	ldr	r3, .L18+4
	moveq	r1, #0
	str	lr, [sp, #-4]!
.LCFI2:
	ldr	r0, .L18+8
	streq	r1, [r2, #0]
	moveq	r2, #49
	streqh	r2, [r3, #0]	@ movhi
	mov	r2, #1
	ldrsh	r1, [r3, #0]
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L19:
	.align	2
.L18:
	.word	GuiVar_KeyboardShiftEnabled
	.word	GuiLib_ActiveCursorFieldNo
	.word	609
.LFE6:
	.size	FDTO_show_keyboard, .-FDTO_show_keyboard
	.section	.text.KEYBOARD_draw_keyboard,"ax",%progbits
	.align	2
	.global	KEYBOARD_draw_keyboard
	.type	KEYBOARD_draw_keyboard, %function
KEYBOARD_draw_keyboard:
.LFB8:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L21
	str	lr, [sp, #-4]!
.LCFI3:
	str	r0, [ip, #0]
	ldr	r0, .L21+4
	sub	sp, sp, #36
.LCFI4:
	str	r1, [r0, #0]
	ldr	r1, .L21+8
	mov	r0, sp
	str	r3, [r1, #0]
	ldr	r3, .L21+12
	str	r2, [r3, #0]
	ldr	r3, .L21+16
	ldrsh	r2, [r3, #0]
	ldr	r3, .L21+20
	str	r2, [r3, #0]
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L21+24
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L22:
	.align	2
.L21:
	.word	GuiVar_KeyboardStartX
	.word	GuiVar_KeyboardStartY
	.word	GuiVar_KeyboardType
	.word	.LANCHOR1
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	FDTO_show_keyboard
.LFE8:
	.size	KEYBOARD_draw_keyboard, .-KEYBOARD_draw_keyboard
	.section	.text.KEYBOARD_process_key,"ax",%progbits
	.align	2
	.global	KEYBOARD_process_key
	.type	KEYBOARD_process_key, %function
KEYBOARD_process_key:
.LFB9:
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #3
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI5:
	mov	r3, r0
	sub	sp, sp, #72
.LCFI6:
	beq	.L28
	bhi	.L33
	cmp	r0, #1
	beq	.L26
	bhi	.L27
	b	.L247
.L33:
	cmp	r0, #67
	beq	.L30
	bhi	.L34
	cmp	r0, #4
	bne	.L24
	b	.L248
.L34:
	cmp	r0, #80
	beq	.L31
	cmp	r0, #84
	bne	.L24
	b	.L249
.L27:
	ldr	r3, .L254
	ldrsh	r5, [r3, #0]
	cmp	r5, #49
	beq	.L30
.L35:
	ldr	r3, .L254+4
	ldr	r7, [r3, #0]
.LBB15:
	sub	r3, r5, #50
	cmp	r3, #49
	ldrls	pc, [pc, r3, asl #2]
	b	.L161
.L88:
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
	.word	.L48
	.word	.L49
	.word	.L50
	.word	.L51
	.word	.L52
	.word	.L53
	.word	.L54
	.word	.L55
	.word	.L56
	.word	.L57
	.word	.L58
	.word	.L59
	.word	.L60
	.word	.L61
	.word	.L62
	.word	.L63
	.word	.L64
	.word	.L65
	.word	.L66
	.word	.L67
	.word	.L68
	.word	.L69
	.word	.L70
	.word	.L71
	.word	.L72
	.word	.L73
	.word	.L74
	.word	.L75
	.word	.L76
	.word	.L77
	.word	.L78
	.word	.L79
	.word	.L80
	.word	.L81
	.word	.L82
	.word	.L83
	.word	.L84
	.word	.L85
	.word	.L86
	.word	.L87
.L86:
	mov	r4, #32
	b	.L37
.L38:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #49
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #49
	moveq	r4, #33
	b	.L37
.L39:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #50
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #50
	moveq	r4, #64
	b	.L37
.L40:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #51
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #51
	moveq	r4, #35
	b	.L37
.L41:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #52
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #52
	moveq	r4, #36
	b	.L37
.L42:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #53
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #53
	moveq	r4, #37
	b	.L37
.L43:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #54
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #54
	moveq	r4, #94
	b	.L37
.L44:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #55
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #55
	moveq	r4, #38
	b	.L37
.L45:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #56
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #56
	moveq	r4, #42
	b	.L37
.L46:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #57
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #57
	moveq	r4, #40
	b	.L37
.L47:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #48
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #48
	moveq	r4, #41
	b	.L37
.L48:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #45
	moveq	r4, #95
	b	.L37
.L49:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #61
	moveq	r4, #43
	b	.L37
.L50:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #113
	moveq	r4, #81
	b	.L37
.L51:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #119
	moveq	r4, #87
	b	.L37
.L52:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #69
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #101
	moveq	r4, #69
	b	.L37
.L53:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #114
	moveq	r4, #82
	b	.L37
.L54:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #116
	moveq	r4, #84
	b	.L37
.L55:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #121
	moveq	r4, #89
	b	.L37
.L56:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #117
	moveq	r4, #85
	b	.L37
.L57:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #105
	moveq	r4, #73
	b	.L37
.L58:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #111
	moveq	r4, #79
	b	.L37
.L59:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #112
	moveq	r4, #80
	b	.L37
.L60:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #91
	moveq	r4, #123
	b	.L37
.L61:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #93
	moveq	r4, #125
	b	.L37
.L62:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #65
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #97
	moveq	r4, #65
	b	.L37
.L63:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #115
	moveq	r4, #83
	b	.L37
.L64:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #68
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #100
	moveq	r4, #68
	b	.L37
.L65:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #70
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #102
	moveq	r4, #70
	b	.L37
.L66:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #103
	moveq	r4, #71
	b	.L37
.L67:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #104
	moveq	r4, #72
	b	.L37
.L68:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #106
	moveq	r4, #74
	b	.L37
.L69:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #107
	moveq	r4, #75
	b	.L37
.L70:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #108
	moveq	r4, #76
	b	.L37
.L71:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #59
	moveq	r4, #58
	b	.L37
.L72:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #39
	moveq	r4, #34
	b	.L37
.L73:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #92
	moveq	r4, #124
	b	.L37
.L74:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #122
	moveq	r4, #90
	b	.L37
.L75:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #120
	moveq	r4, #88
	b	.L37
.L76:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #67
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #99
	moveq	r4, #67
	b	.L37
.L77:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #118
	moveq	r4, #86
	b	.L37
.L78:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r4, #66
	beq	.L37
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #98
	moveq	r4, #66
	b	.L37
.L79:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #110
	moveq	r4, #78
	b	.L37
.L80:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #109
	moveq	r4, #77
	b	.L37
.L81:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #44
	moveq	r4, #60
	b	.L37
.L82:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #46
	moveq	r4, #62
	b	.L37
.L83:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #47
	moveq	r4, #63
	b	.L37
.L84:
	ldr	r3, .L254+12
	ldr	r4, [r3, #0]
	cmp	r4, #1
	movne	r4, #96
	moveq	r4, #126
	b	.L37
.L85:
	bl	good_key_beep
	ldr	r3, .L254+12
	ldr	r2, [r3, #0]
	rsbs	r2, r2, #1
	movcc	r2, #0
	str	r2, [r3, #0]
	b	.L161
.L87:
	ldr	r6, .L254+16
	mov	r0, r6
	bl	strlen
	subs	r8, r0, #0
	beq	.L89
	bl	good_key_beep
	cmp	r8, #1
	ldreq	r3, .L254+12
	add	r6, r6, r8
	mov	r4, #0
	strb	r4, [r6, #-1]
	streq	r8, [r3, #0]
	b	.L37
.L89:
	bl	bad_key_beep
	mov	r4, r8
	b	.L37
.L161:
	mov	r4, #0
.L37:
	cmp	r5, #97
	cmpne	r5, #99
	movne	r5, #0
	moveq	r5, #1
	beq	.L90
	ldr	r6, .L254+16
	mov	r0, r6
	bl	strlen
	cmp	r0, r7
	mov	r8, r0
	bcs	.L91
	bl	good_key_beep
	strb	r4, [r8, r6]
	add	r8, r8, #1
	cmp	r8, r7
	strccb	r5, [r6, r8]
	b	.L92
.L91:
	bl	bad_key_beep
.L92:
	ldr	r3, .L254+12
	ldr	r2, [r3, #0]
	cmp	r2, #1
	moveq	r2, #0
	streq	r2, [r3, #0]
.L90:
	mov	r3, #2
	str	r3, [sp, #36]
	ldr	r3, .L254+20
	add	r0, sp, #36
	str	r3, [sp, #56]
	mov	r3, #0
	str	r3, [sp, #60]
	b	.L233
.L248:
.LBE15:
.LBB16:
	ldr	r3, .L254+8
	ldr	r1, [r3, #0]
	cmp	r1, #0
	beq	.L94
	cmp	r1, #1
	bne	.L159
	b	.L250
.L94:
	ldr	r3, .L254
	ldrh	r0, [r3, #0]
	mov	r2, r0, asl #16
	mov	r3, r2, asr #16
	cmp	r3, #49
	beq	.L240
.L96:
	mov	r1, r2, lsr #16
	sub	r2, r2, #3276800
	cmp	r2, #65536
	bhi	.L97
.L241:
	mov	r0, #97
.L234:
	mov	r1, #1
.L235:
	bl	CURSOR_Select
	b	.L23
.L97:
	cmp	r3, #52
	moveq	r0, #87
	beq	.L234
	cmp	r3, #53
	moveq	r0, #88
	beq	.L234
	sub	r3, r1, #54
	mov	r3, r3, asl #16
	cmp	r3, #196608
	bls	.L246
.L100:
	mov	r0, r0, asl #16
	mov	r0, r0, asr #16
	cmp	r0, #58
	moveq	r0, #93
	beq	.L234
	cmp	r0, #59
	moveq	r0, #94
	beq	.L234
	sub	r1, r1, #60
	mov	r1, r1, asl #16
	cmp	r1, #65536
	bls	.L245
.L103:
	cmp	r0, #85
	suble	r0, r0, #12
	ble	.L234
	cmp	r0, #96
	suble	r0, r0, #11
	ble	.L234
	cmp	r0, #97
	beq	.L244
.L106:
	cmp	r0, #99
	moveq	r0, #95
	beq	.L234
	cmp	r0, #98
	moveq	r0, #91
	bne	.L159
	b	.L234
.L250:
	ldr	r3, .L254
	ldrh	r2, [r3, #0]
	mov	r3, r2, asl #16
	sub	ip, r3, #3276800
	cmp	ip, #589824
	mov	r0, r3, lsr #16
	bls	.L237
.L109:
	cmp	r3, #4194304
	moveq	r0, #52
	beq	.L235
	sub	r3, r0, #74
	mov	r3, r3, asl #16
	cmp	r3, #196608
	movls	r0, #64
	bls	.L234
	sub	r0, r0, #88
	mov	r0, r0, asl #16
	cmp	r0, #131072
	movls	r0, #77
	bls	.L234
	mov	r3, r2, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #99
	moveq	r0, #90
	beq	.L234
	cmp	r3, #49
	bne	.L159
	b	.L245
.L247:
.LBE16:
.LBB17:
	ldr	r3, .L254+8
	ldr	r1, [r3, #0]
	cmp	r1, #0
	beq	.L116
	cmp	r1, #1
	bne	.L159
	b	.L251
.L116:
	ldr	r2, .L254
	ldrsh	r3, [r2, #0]
	ldrh	r1, [r2, #0]
	cmp	r3, #49
	moveq	r0, #56
	beq	.L234
	cmp	r3, #99
	moveq	r0, #60
	beq	.L234
	cmp	r3, #98
	beq	.L246
.L120:
	cmp	r3, #97
	moveq	r0, #51
	beq	.L234
	cmp	r3, #87
	moveq	r0, #52
	beq	.L234
	mov	r3, r1, asl #16
	mov	r0, r3, asr #16
	cmp	r0, #88
	moveq	r0, #53
	beq	.L234
	cmp	r0, #93
	moveq	r0, #58
	beq	.L234
	cmp	r0, #94
	moveq	r0, #59
	beq	.L234
	cmp	r0, #74
	cmpne	r0, #86
	beq	.L241
.L126:
	mov	r2, r3, lsr #16
	sub	r3, r3, #5832704
	cmp	r3, #196608
	bhi	.L127
.L240:
	mov	r0, #98
	b	.L234
.L127:
	sub	r3, r2, #95
	mov	r3, r3, asl #16
	cmp	r3, #65536
	bls	.L245
.L128:
	cmp	r0, #73
	addle	r0, r0, #12
	ble	.L234
	cmp	r0, #85
	addle	r0, r0, #11
	ble	.L234
	b	.L159
.L251:
	ldr	r3, .L254
	ldrh	r2, [r3, #0]
	mov	r3, r2, asl #16
	sub	ip, r3, #3276800
	cmp	ip, #589824
	mov	r0, r3, lsr #16
	movls	r0, #64
	bls	.L235
	cmp	r3, #4194304
	moveq	r0, #76
	beq	.L235
	sub	r3, r0, #74
	mov	r3, r3, asl #16
	cmp	r3, #196608
	movls	r0, #88
	bls	.L235
	sub	r0, r0, #88
	mov	r0, r0, asl #16
	cmp	r0, #131072
	bhi	.L134
.L245:
	mov	r0, #99
	b	.L234
.L134:
	mov	r3, r2, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #99
	bne	.L135
.L246:
	mov	r0, #49
	b	.L234
.L135:
	cmp	r3, #49
	moveq	r0, #55
	bne	.L159
	b	.L234
.L26:
.LBE17:
.LBB18:
	ldr	r2, .L254+8
	ldr	r1, [r2, #0]
	cmp	r1, #0
	beq	.L138
	cmp	r1, #1
	bne	.L159
	b	.L252
.L138:
	ldr	r2, .L254
	ldrsh	r2, [r2, #0]
	cmp	r2, #50
	moveq	r0, #61
	beq	.L238
	cmp	r2, #62
	moveq	r0, #73
	beq	.L238
	cmp	r2, #74
	moveq	r0, #85
	beq	.L238
	cmp	r2, #86
	moveq	r0, #96
	beq	.L238
	cmp	r2, #97
	bne	.L230
	mov	r0, #99
.L238:
	mov	r1, r3
	b	.L235
.L252:
	ldr	r3, .L254
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	moveq	r0, #99
	beq	.L235
.L230:
	bl	CURSOR_Up
	b	.L23
.L28:
.LBE18:
.LBB19:
	ldr	r3, .L254+8
	ldr	r1, [r3, #0]
	cmp	r1, #0
	beq	.L148
	cmp	r1, #1
	bne	.L159
	b	.L253
.L148:
	ldr	r3, .L254
	ldrsh	r3, [r3, #0]
	cmp	r3, #61
	moveq	r0, #50
	beq	.L234
	cmp	r3, #73
	moveq	r0, #62
	beq	.L234
	cmp	r3, #85
	moveq	r0, #74
	beq	.L234
	cmp	r3, #96
	bne	.L153
.L244:
	mov	r0, #86
	b	.L234
.L153:
	cmp	r3, #99
	movne	r0, #1
	bne	.L236
	b	.L241
.L253:
	ldr	r3, .L254
	ldrsh	r3, [r3, #0]
	cmp	r3, #99
	movne	r0, r1
	bne	.L236
.L237:
	mov	r0, #49
	b	.L235
.L236:
	bl	CURSOR_Down
	b	.L23
.L249:
.LBE19:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L159
	ldr	r4, .L254+12
	ldr	r5, [r4, #0]
	cmp	r5, #0
	bne	.L159
	bl	good_key_beep
	ldr	r3, [r4, #0]
	str	r5, [sp, #24]
	rsbs	r3, r3, #1
	movcc	r3, #0
	str	r3, [r4, #0]
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L254+20
	str	r3, [sp, #20]
.L239:
	mov	r0, sp
.L233:
	bl	Display_Post_Command
	b	.L23
.L31:
	ldr	r3, .L254+8
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L159
	ldr	r4, .L254+12
	ldr	r3, [r4, #0]
	cmp	r3, #1
	bne	.L159
	bl	good_key_beep
	ldr	r3, [r4, #0]
	rsbs	r3, r3, #1
	movcc	r3, #0
	str	r3, [r4, #0]
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L254+20
	str	r3, [sp, #20]
	mov	r3, #0
	str	r3, [sp, #24]
	b	.L239
.L159:
	bl	bad_key_beep
	b	.L23
.L30:
	mov	r0, r2
	bl	process_OK_button
	b	.L23
.L24:
	mov	r0, r3
	bl	KEY_process_global_keys
.L23:
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L255:
	.align	2
.L254:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR1
	.word	GuiVar_KeyboardType
	.word	GuiVar_KeyboardShiftEnabled
	.word	GuiVar_GroupName
	.word	FDTO_show_keyboard
.LFE9:
	.size	KEYBOARD_process_key, .-KEYBOARD_process_key
	.section	.bss.g_KEYBOARD_max_string_length,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_KEYBOARD_max_string_length, %object
	.size	g_KEYBOARD_max_string_length, 4
g_KEYBOARD_max_string_length:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC1:
	.ascii	"(description not yet set)\000"
.LC2:
	.ascii	"%s %s\000"
.LC3:
	.ascii	"Unnamed group\000"
.LC0:
	.ascii	"Unnamed\000"
	.section	.bss.g_KEYBOARD_cursor_position_when_keyboard_displayed,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_KEYBOARD_cursor_position_when_keyboard_displayed, %object
	.size	g_KEYBOARD_cursor_position_when_keyboard_displayed, 4
g_KEYBOARD_cursor_position_when_keyboard_displayed:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI0-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI2-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI3-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI5-.LFB9
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x60
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_keyboard.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xa5
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF10
	.byte	0x1
	.4byte	.LASF11
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x412
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x295
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x249
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x3f5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x438
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST2
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.byte	0x8f
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x120
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1b3
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x204
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x45b
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB4
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB6
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB8
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB9
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI6
	.4byte	.LFE9
	.2byte	0x3
	.byte	0x7d
	.sleb128 96
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF6:
	.ascii	"process_left_arrow\000"
.LASF0:
	.ascii	"hide_keyboard\000"
.LASF1:
	.ascii	"process_key_press\000"
.LASF3:
	.ascii	"FDTO_show_keyboard\000"
.LASF4:
	.ascii	"process_up_arrow\000"
.LASF10:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF8:
	.ascii	"KEYBOARD_draw_keyboard\000"
.LASF2:
	.ascii	"process_OK_button\000"
.LASF7:
	.ascii	"process_right_arrow\000"
.LASF9:
	.ascii	"KEYBOARD_process_key\000"
.LASF11:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_keyboard.c\000"
.LASF5:
	.ascii	"process_down_arrow\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
