	.file	"r_rt_electrical.c"
	.text
.Ltext0:
	.section	.text.REAL_TIME_ELECTRICAL_draw_scroll_line,"ax",%progbits
	.align	2
	.global	REAL_TIME_ELECTRICAL_draw_scroll_line
	.type	REAL_TIME_ELECTRICAL_draw_scroll_line, %function
REAL_TIME_ELECTRICAL_draw_scroll_line:
.LFB0:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI0:
	ldr	r4, .L2
	ldr	r3, .L2+4
	mov	r0, r0, asl #16
	mov	r5, r0, asr #16
	mov	r1, r4
	mov	r0, r5
	str	r3, [sp, #0]	@ float
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	mov	r1, r4
	mov	r0, r4
	ldr	r4, .L2+8
	mov	r2, #23
	bl	strlcpy
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L2+12
	mov	r3, #59
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L2+16
	flds	s15, [sp, #0]
	add	r5, r5, #39
	ldr	r3, [r3, r5, asl #2]
	ldr	r0, [r4, #0]
	fmsr	s13, r3	@ int
	ldr	r3, .L2+20
	fuitos	s14, s13
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	ldr	r3, .L2+24
	ldr	r2, [r3, #112]
	ldr	r3, .L2+28
	str	r2, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L3:
	.align	2
.L2:
	.word	GuiVar_LiveScreensElectricalControllerName
	.word	1148846080
	.word	tpmicro_data_recursive_MUTEX
	.word	.LC0
	.word	tpmicro_data
	.word	GuiVar_StatusCurrentDraw
	.word	tpmicro_comm
	.word	GuiVar_StatusFuseBlown
.LFE0:
	.size	REAL_TIME_ELECTRICAL_draw_scroll_line, .-REAL_TIME_ELECTRICAL_draw_scroll_line
	.section	.text.FDTO_REAL_TIME_ELECTRICAL_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_ELECTRICAL_draw_report
	.type	FDTO_REAL_TIME_ELECTRICAL_draw_report, %function
FDTO_REAL_TIME_ELECTRICAL_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, lr}
.LCFI1:
	mov	r4, r0
	bne	.L5
	mov	r2, r4
	mvn	r1, #0
	mov	r0, #94
	bl	GuiLib_ShowScreen
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	bl	FLOWSENSE_we_are_poafs
	cmp	r0, #0
	moveq	r2, r4
	beq	.L6
	bl	COMM_MNGR_number_of_controllers_in_the_chain_based_upon_chain_members
	mov	r2, r0, asl #16
	mov	r2, r2, asr #16
.L6:
	mov	r0, #0
	ldr	r1, .L9
	mov	r3, r0
	bl	GuiLib_ScrollBox_Init
	b	.L7
.L5:
	mov	r0, #0
	bl	GuiLib_ScrollBox_Redraw
.L7:
	ldmfd	sp!, {r4, lr}
	b	GuiLib_Refresh
.L10:
	.align	2
.L9:
	.word	REAL_TIME_ELECTRICAL_draw_scroll_line
.LFE1:
	.size	FDTO_REAL_TIME_ELECTRICAL_draw_report, .-FDTO_REAL_TIME_ELECTRICAL_draw_report
	.section	.text.REAL_TIME_ELECTRICAL_process_report,"ax",%progbits
	.align	2
	.global	REAL_TIME_ELECTRICAL_process_report
	.type	REAL_TIME_ELECTRICAL_process_report, %function
REAL_TIME_ELECTRICAL_process_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	stmfd	sp!, {r4, lr}
.LCFI2:
	bne	.L15
	ldr	r3, .L16
	ldr	r2, .L16+4
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r4, .L16+8
	ldr	r3, [r3, #4]
	str	r3, [r4, #0]
	bl	KEY_process_global_keys
	ldr	r3, [r4, #0]
	cmp	r3, #10
	ldmnefd	sp!, {r4, pc}
	mov	r0, #1
	ldmfd	sp!, {r4, lr}
	b	LIVE_SCREENS_draw_dialog
.L15:
	mov	r2, #0
	mov	r3, r2
	ldmfd	sp!, {r4, lr}
	b	REPORTS_process_report
.L17:
	.align	2
.L16:
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	REAL_TIME_ELECTRICAL_process_report, .-REAL_TIME_ELECTRICAL_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_electrical.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_rt_electrical.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x26
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x47
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x60
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"REAL_TIME_ELECTRICAL_draw_scroll_line\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_electrical.c\000"
.LASF2:
	.ascii	"REAL_TIME_ELECTRICAL_process_report\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"FDTO_REAL_TIME_ELECTRICAL_draw_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
