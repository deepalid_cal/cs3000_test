	.file	"d_comm_options.c"
	.text
.Ltext0:
	.section	.text.FDTO_COMM_OPTIONS_draw_dialog,"ax",%progbits
	.align	2
	.type	FDTO_COMM_OPTIONS_draw_dialog, %function
FDTO_COMM_OPTIONS_draw_dialog:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L13
	ldr	r1, .L13+4
	ldr	r3, [r2, #80]
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI0:
	str	r3, [r1, #0]
	ldr	r1, [r2, #84]
	ldr	r2, .L13+8
	cmp	r3, #5
	cmpne	r3, #0
	str	r1, [r2, #0]
	mov	r7, r0
	moveq	r2, #0
	movne	r2, #1
	beq	.L2
	cmp	r3, #7
	moveq	r2, #0
	beq	.L2
	subs	r2, r3, #10
	movne	r2, #1
.L2:
	ldr	r5, .L13+12
	sub	r1, r1, #1
	str	r2, [r5, #0]
	mov	r2, #56
	mul	r3, r2, r3
	ldr	r2, .L13+16
	ldr	r4, .L13+20
	ldr	r3, [r2, r3]
	ldr	r6, .L13+24
	cmp	r1, #2
	movhi	r1, #0
	movls	r1, #1
	str	r1, [r4, #0]
	str	r3, [r6, #0]
	bl	RADIO_TEST_there_is_a_port_device_to_test
	ldr	r3, .L13+28
	cmp	r7, #1
	str	r0, [r3, #0]
	ldr	r3, .L13+32
	ldrsh	r1, [r3, #0]
	ldr	r3, .L13+36
	str	r1, [r3, #0]
	bne	.L3
	ldr	r3, .L13+40
	ldr	r2, [r3, #0]
	cmp	r2, #0
	bne	.L4
	ldr	r2, [r6, #0]
	cmp	r2, #0
	movne	r2, #50
	bne	.L12
	cmp	r0, #0
	movne	r2, #51
	bne	.L12
	ldr	r2, [r5, #0]
	cmp	r2, #1
	moveq	r2, #52
	beq	.L12
	ldr	r2, [r4, #0]
	cmp	r2, #1
	moveq	r2, #53
	beq	.L12
	ldr	r2, .L13+44
	ldr	r2, [r2, #0]
	cmp	r2, #1
	moveq	r2, #54
	movne	r2, #55
.L12:
	str	r2, [r3, #0]
.L4:
	ldr	r1, [r3, #0]
.L3:
	ldr	r3, .L13+48
	mov	r2, #1
	mov	r1, r1, asl #16
	str	r2, [r3, #0]
	ldr	r0, .L13+52
	mov	r1, r1, asr #16
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	GuiLib_Refresh
.L14:
	.align	2
.L13:
	.word	config_c
	.word	GuiVar_CommOptionPortAIndex
	.word	GuiVar_CommOptionPortBIndex
	.word	GuiVar_CommOptionPortAInstalled
	.word	port_device_table
	.word	GuiVar_CommOptionPortBInstalled
	.word	GuiVar_CommOptionCloudCommTestAvailable
	.word	GuiVar_CommOptionRadioTestAvailable
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	GuiVar_IsAHub
	.word	.LANCHOR2
	.word	594
.LFE0:
	.size	FDTO_COMM_OPTIONS_draw_dialog, .-FDTO_COMM_OPTIONS_draw_dialog
	.section	.text.COMM_OPTIONS_draw_dialog,"ax",%progbits
	.align	2
	.global	COMM_OPTIONS_draw_dialog
	.type	COMM_OPTIONS_draw_dialog, %function
COMM_OPTIONS_draw_dialog:
.LFB1:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L17
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r3, [r3, #0]
	sub	sp, sp, #36
.LCFI2:
	cmp	r3, #0
	mov	r4, r0
	beq	.L16
	bl	task_control_EXIT_device_exchange_hammer
.L16:
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L17+4
	mov	r0, sp
	str	r3, [sp, #20]
	str	r4, [sp, #24]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L18:
	.align	2
.L17:
	.word	in_device_exchange_hammer
	.word	FDTO_COMM_OPTIONS_draw_dialog
.LFE1:
	.size	COMM_OPTIONS_draw_dialog, .-COMM_OPTIONS_draw_dialog
	.section	.text.COMM_OPTIONS_process_dialog,"ax",%progbits
	.align	2
	.global	COMM_OPTIONS_process_dialog
	.type	COMM_OPTIONS_process_dialog, %function
COMM_OPTIONS_process_dialog:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r4, lr}
.LCFI3:
	sub	sp, sp, #36
.LCFI4:
	beq	.L23
	bhi	.L26
	cmp	r0, #0
	beq	.L21
	cmp	r0, #2
	bne	.L20
	b	.L58
.L26:
	cmp	r0, #67
	beq	.L24
	cmp	r0, #81
	bne	.L20
	ldr	r3, .L60
	ldrsh	r3, [r3, #0]
	cmp	r3, #50
	bne	.L30
	ldr	r2, .L60+4
	mov	lr, #13
	str	r3, [r2, #0]
	ldr	r2, .L60+8
	str	r0, [sp, #28]
	str	r3, [r2, #0]
	mov	r3, #11
	mov	r2, #3
	stmia	sp, {r2, r3, lr}
	ldr	r3, .L60+12
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	b	.L53
.L58:
	ldr	r3, .L60
	ldr	r2, .L60+4
	ldrsh	r3, [r3, #0]
	str	r3, [r2, #0]
	sub	r3, r3, #50
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L30
.L38:
	.word	.L31
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
.L31:
	ldr	r3, .L60+8
	mov	r2, #50
	str	r2, [r3, #0]
	mov	r0, #3
	mov	r3, #11
	mov	ip, #13
	stmia	sp, {r0, r3, ip}
	ldr	r3, .L60+12
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	mov	r3, #2
	str	r3, [sp, #28]
.L53:
	ldr	r3, .L60+16
.L55:
	mov	r0, sp
	str	r3, [sp, #16]
	bl	Change_Screen
	ldr	r3, .L60+20
	mov	r2, #0
	str	r2, [r3, #0]
	b	.L19
.L32:
	ldr	r2, .L60+8
	mov	r3, #51
	str	r3, [r2, #0]
	str	r3, [sp, #8]
	ldr	r3, .L60+24
	mov	r1, #2
	mov	r2, #11
	str	r3, [sp, #20]
	mov	r4, #0
	mov	r3, #1
	str	r3, [sp, #24]
	stmia	sp, {r1, r2}
	ldr	r3, .L60+28
	str	r4, [sp, #28]
	b	.L54
.L33:
	ldr	r3, .L60+32
	ldr	r3, [r3, #80]
	sub	r3, r3, #1
	cmp	r3, #8
	ldrls	pc, [pc, r3, asl #2]
	b	.L52
.L46:
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L52
	.word	.L44
	.word	.L52
	.word	.L52
	.word	.L44
.L43:
	bl	task_control_ENTER_device_exchange_hammer
	mov	r3, #11
	mov	r2, #3
	mov	lr, #17
	stmia	sp, {r2, r3, lr}
	ldr	r3, .L60+36
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	str	r3, [sp, #28]
	ldr	r3, .L60+40
	b	.L55
.L44:
	bl	task_control_ENTER_device_exchange_hammer
	mov	r3, #11
	mov	r0, #3
	mov	ip, #68
	stmia	sp, {r0, r3, ip}
	ldr	r3, .L60+44
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	str	r3, [sp, #28]
	ldr	r3, .L60+48
	b	.L55
.L41:
	bl	task_control_ENTER_device_exchange_hammer
	mov	r3, #29
	mov	r1, #3
	mov	r2, #11
	stmia	sp, {r1, r2, r3}
	ldr	r3, .L60+52
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	str	r3, [sp, #28]
	b	.L56
.L40:
	bl	task_control_ENTER_device_exchange_hammer
	mov	r3, #3
	mov	ip, #11
	mov	lr, #30
	stmia	sp, {r3, ip, lr}
	ldr	r3, .L60+56
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	b	.L57
.L42:
	bl	task_control_ENTER_device_exchange_hammer
	mov	r3, #53
	mov	r0, #3
	mov	r1, #11
	stmia	sp, {r0, r1, r3}
	ldr	r3, .L60+60
	mov	r4, #0
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	ldr	r3, .L60+64
	str	r4, [sp, #28]
.L54:
	mov	r0, sp
	str	r3, [sp, #16]
	bl	Change_Screen
	ldr	r3, .L60+20
	str	r4, [r3, #0]
	b	.L19
.L34:
	ldr	r3, .L60+32
	ldr	r4, [r3, #84]
	cmp	r4, #2
	beq	.L49
	cmp	r4, #3
	beq	.L50
	cmp	r4, #1
	bne	.L52
	b	.L59
.L49:
	bl	task_control_ENTER_device_exchange_hammer
	mov	r3, #11
	mov	r2, #3
	mov	lr, #29
	stmia	sp, {r2, r3, lr}
	ldr	r3, .L60+52
	str	r4, [sp, #28]
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
.L56:
	ldr	r3, .L60+68
	b	.L55
.L59:
	bl	task_control_ENTER_device_exchange_hammer
	mov	r3, #11
	mov	r0, #3
	mov	ip, #30
	stmia	sp, {r0, r3, ip}
	ldr	r3, .L60+56
	str	r4, [sp, #24]
	str	r3, [sp, #20]
	mov	r3, #2
.L57:
	str	r3, [sp, #28]
	ldr	r3, .L60+72
	b	.L55
.L50:
	bl	task_control_ENTER_device_exchange_hammer
	mov	r3, #53
	mov	r1, #11
	stmib	sp, {r1, r3}
	ldr	r3, .L60+60
	str	r4, [sp, #0]
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	str	r3, [sp, #28]
	ldr	r3, .L60+64
	b	.L55
.L52:
	bl	bad_key_beep
	bl	task_control_EXIT_device_exchange_hammer
	b	.L19
.L35:
	ldr	r3, .L60+8
	mov	r2, #54
	str	r2, [r3, #0]
	mov	lr, #28
	mov	r3, #11
	mov	r2, #2
	stmia	sp, {r2, r3, lr}
	ldr	r3, .L60+76
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	ldr	r3, .L60+80
	b	.L55
.L36:
	ldr	r3, .L60+8
	mov	r2, #55
	str	r2, [r3, #0]
	mov	r0, #2
	mov	r3, #11
	mov	ip, #99
	stmia	sp, {r0, r3, ip}
	ldr	r3, .L60+84
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	ldr	r3, .L60+88
	b	.L55
.L37:
	ldr	r3, .L60+8
	mov	r2, #56
	str	r2, [r3, #0]
	mov	r1, #2
	mov	r3, #3
	mov	r2, #11
	stmia	sp, {r1, r2, r3}
	ldr	r3, .L60+92
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	ldr	r3, .L60+96
	b	.L55
.L30:
	bl	bad_key_beep
	b	.L19
.L23:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L19
.L21:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L19
.L24:
	bl	good_key_beep
	ldr	r3, .L60
	ldr	r2, .L60+4
	ldrsh	r1, [r3, #0]
	mov	r0, #0
	str	r1, [r2, #0]
	ldr	r2, .L60+20
	str	r0, [r2, #0]
	ldr	r2, .L60+100
	ldr	r2, [r2, #0]
	strh	r2, [r3, #0]	@ movhi
	bl	Redraw_Screen
	b	.L19
.L20:
	bl	KEY_process_global_keys
.L19:
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L61:
	.align	2
.L60:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR1
	.word	GuiVar_MenuScreenToShow
	.word	FDTO_COMM_TEST_draw_screen
	.word	COMM_TEST_process_screen
	.word	.LANCHOR2
	.word	FDTO_RADIO_TEST_draw_radio_comm_test_screen
	.word	RADIO_TEST_process_radio_comm_test_screen
	.word	config_c
	.word	FDTO_EN_PROGRAMMING_draw_screen
	.word	EN_PROGRAMMING_process_screen
	.word	FDTO_WEN_PROGRAMMING_draw_screen
	.word	WEN_PROGRAMMING_process_screen
	.word	FDTO_LR_PROGRAMMING_lrs_draw_screen
	.word	FDTO_LR_PROGRAMMING_raveon_draw_screen
	.word	FDTO_SR_PROGRAMMING_draw_screen
	.word	SR_PROGRAMMING_process_screen
	.word	LR_PROGRAMMING_lrs_process_screen
	.word	LR_PROGRAMMING_raveon_process_screen
	.word	FDTO_HUB_draw_screen
	.word	HUB_process_screen
	.word	FDTO_SERIAL_PORT_INFO_draw_report
	.word	SERIAL_PORT_INFO_process_report
	.word	FDTO_ACTIVATE_OPTIONS_draw_screen
	.word	ACTIVATE_OPTIONS_process_screen
	.word	.LANCHOR0
.LFE2:
	.size	COMM_OPTIONS_process_dialog, .-COMM_OPTIONS_process_dialog
	.global	g_COMM_OPTIONS_dialog_visible
	.global	g_COMM_OPTIONS_cursor_position_when_dialog_displayed
	.section	.bss.g_COMM_OPTIONS_dialog_visible,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_COMM_OPTIONS_dialog_visible, %object
	.size	g_COMM_OPTIONS_dialog_visible, 4
g_COMM_OPTIONS_dialog_visible:
	.space	4
	.section	.bss.g_COMM_OPTIONS_cursor_position_when_dialog_displayed,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_COMM_OPTIONS_cursor_position_when_dialog_displayed, %object
	.size	g_COMM_OPTIONS_cursor_position_when_dialog_displayed, 4
g_COMM_OPTIONS_cursor_position_when_dialog_displayed:
	.space	4
	.section	.bss.g_COMM_OPTIONS_last_cursor_position,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_COMM_OPTIONS_last_cursor_position, %object
	.size	g_COMM_OPTIONS_last_cursor_position, 4
g_COMM_OPTIONS_last_cursor_position:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/d_comm_options.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x59
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x3a
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x90
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xa6
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/d_comm_options.c\000"
.LASF4:
	.ascii	"FDTO_COMM_OPTIONS_draw_dialog\000"
.LASF0:
	.ascii	"COMM_OPTIONS_draw_dialog\000"
.LASF1:
	.ascii	"COMM_OPTIONS_process_dialog\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
