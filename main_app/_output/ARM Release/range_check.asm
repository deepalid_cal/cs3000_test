	.file	"range_check.c"
	.text
.Ltext0:
	.section	.text.RC_bool_with_filename,"ax",%progbits
	.align	2
	.global	RC_bool_with_filename
	.type	RC_bool_with_filename, %function
RC_bool_with_filename:
.LFB0:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, lr}
.LCFI0:
	ldr	r6, [r0, #0]
	mov	r4, r0
	cmp	r6, #1
	mov	r5, r1
	mov	r7, r2
	mov	r8, r3
	movls	r0, #1
	bls	.L2
	ldr	r0, [sp, #32]
	bl	RemovePathFromFileName
	ldr	r3, [sp, #36]
	mov	r1, r7
	str	r3, [sp, #4]
	mov	r2, r6
	mov	r3, r5
	str	r0, [sp, #0]
	mov	r0, r8
	bl	Alert_range_check_failed_bool_with_filename
	mov	r0, #0
	str	r5, [r4, #0]
.L2:
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, pc}
.LFE0:
	.size	RC_bool_with_filename, .-RC_bool_with_filename
	.section	.text.RC_date_with_filename,"ax",%progbits
	.align	2
	.global	RC_date_with_filename
	.type	RC_date_with_filename, %function
RC_date_with_filename:
.LFB1:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI1:
	mov	r4, r3
	ldr	r3, [r0, #0]
	cmp	r3, r1
	bcc	.L5
	cmp	r3, r2
	movls	r0, #1
	bls	.L6
.L5:
	str	r4, [r0, #0]
	ldr	r0, [sp, #20]
	bl	RemovePathFromFileName
	ldr	r2, [sp, #24]
	ldr	r1, [sp, #12]
	str	r2, [sp, #0]
	mov	r2, r4
	mov	r3, r0
	ldr	r0, [sp, #16]
	bl	Alert_range_check_failed_date_with_filename
	mov	r0, #0
.L6:
	ldmfd	sp!, {r3, r4, pc}
.LFE1:
	.size	RC_date_with_filename, .-RC_date_with_filename
	.section	.text.RC_int32_with_filename,"ax",%progbits
	.align	2
	.global	RC_int32_with_filename
	.type	RC_int32_with_filename, %function
RC_int32_with_filename:
.LFB2:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI2:
	ldr	r6, [r0, #0]
	mov	r4, r0
	cmp	r6, r1
	mov	r5, r3
	blt	.L9
	cmp	r6, r2
	movle	r0, #1
	ble	.L10
.L9:
	ldr	r0, [sp, #32]
	bl	RemovePathFromFileName
	ldr	r3, [sp, #36]
	ldr	r1, [sp, #24]
	str	r3, [sp, #4]
	mov	r2, r6
	mov	r3, r5
	str	r0, [sp, #0]
	ldr	r0, [sp, #28]
	bl	Alert_range_check_failed_int32_with_filename
	mov	r0, #0
	str	r5, [r4, #0]
.L10:
	ldmfd	sp!, {r2, r3, r4, r5, r6, pc}
.LFE2:
	.size	RC_int32_with_filename, .-RC_int32_with_filename
	.section	.text.RC_string_with_filename,"ax",%progbits
	.align	2
	.global	RC_string_with_filename
	.type	RC_string_with_filename, %function
RC_string_with_filename:
.LFB3:
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI3:
	mov	r5, r0
	mov	r6, r1
	mov	r7, r2
	mov	r9, r3
	mov	r8, r0
	mov	r4, #1
	mov	sl, #0
	mov	fp, #63
	b	.L13
.L18:
	bl	isprint
	add	sl, sl, #1
	cmp	r0, #0
	moveq	r4, r0
	streqb	fp, [r8, #-1]
.L13:
	cmp	sl, r6
	bne	.L15
	ldrb	r3, [r8, #0]	@ zero_extendqisi2
	cmp	r3, #0
	movne	r3, #0
	strneb	r3, [r8, #0]
	b	.L17
.L15:
	ldrb	r0, [r8], #1	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L18
.L17:
	cmp	r4, #0
	bne	.L19
	mov	r0, r9
	bl	RemovePathFromFileName
	mov	r1, r5
	ldr	r3, [sp, #36]
	mov	r2, r0
	mov	r0, r7
	bl	Alert_range_check_failed_string_with_filename
.L19:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.LFE3:
	.size	RC_string_with_filename, .-RC_string_with_filename
	.section	.text.RC_time_with_filename,"ax",%progbits
	.align	2
	.global	RC_time_with_filename
	.type	RC_time_with_filename, %function
RC_time_with_filename:
.LFB4:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI4:
	mov	r4, r3
	ldr	r3, [r0, #0]
	cmp	r3, r1
	bcc	.L21
	cmp	r3, r2
	movls	r0, #1
	bls	.L22
.L21:
	str	r4, [r0, #0]
	ldr	r0, [sp, #20]
	bl	RemovePathFromFileName
	ldr	r2, [sp, #24]
	ldr	r1, [sp, #12]
	str	r2, [sp, #0]
	mov	r2, r4
	mov	r3, r0
	ldr	r0, [sp, #16]
	bl	Alert_range_check_failed_time_with_filename
	mov	r0, #0
.L22:
	ldmfd	sp!, {r3, r4, pc}
.LFE4:
	.size	RC_time_with_filename, .-RC_time_with_filename
	.section	.text.RC_uint32_with_filename,"ax",%progbits
	.align	2
	.global	RC_uint32_with_filename
	.type	RC_uint32_with_filename, %function
RC_uint32_with_filename:
.LFB5:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI5:
	ldr	r6, [r0, #0]
	mov	r4, r0
	cmp	r6, r1
	mov	r5, r3
	bcc	.L25
	cmp	r6, r2
	movls	r0, #1
	bls	.L26
.L25:
	ldr	r0, [sp, #32]
	bl	RemovePathFromFileName
	ldr	r3, [sp, #36]
	ldr	r1, [sp, #24]
	str	r3, [sp, #4]
	mov	r2, r6
	mov	r3, r5
	str	r0, [sp, #0]
	ldr	r0, [sp, #28]
	bl	Alert_range_check_failed_uint32_with_filename
	mov	r0, #0
	str	r5, [r4, #0]
.L26:
	ldmfd	sp!, {r2, r3, r4, r5, r6, pc}
.LFE5:
	.size	RC_uint32_with_filename, .-RC_uint32_with_filename
	.section	.text.RC_uns8_with_filename,"ax",%progbits
	.align	2
	.global	RC_uns8_with_filename
	.type	RC_uns8_with_filename, %function
RC_uns8_with_filename:
.LFB6:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI6:
	ldrb	r6, [r0, #0]	@ zero_extendqisi2
	and	r1, r1, #255
	cmp	r1, r6
	mov	r4, r0
	and	r2, r2, #255
	and	r5, r3, #255
	bhi	.L29
	cmp	r6, r2
	movls	r0, #1
	bls	.L30
.L29:
	ldr	r0, [sp, #32]
	bl	RemovePathFromFileName
	ldr	r3, [sp, #36]
	ldr	r1, [sp, #24]
	str	r3, [sp, #4]
	mov	r2, r6
	mov	r3, r5
	str	r0, [sp, #0]
	ldr	r0, [sp, #28]
	bl	Alert_range_check_failed_uint32_with_filename
	mov	r0, #0
	strb	r5, [r4, #0]
.L30:
	ldmfd	sp!, {r2, r3, r4, r5, r6, pc}
.LFE6:
	.size	RC_uns8_with_filename, .-RC_uns8_with_filename
	.section	.text.RC_float_with_filename,"ax",%progbits
	.align	2
	.global	RC_float_with_filename
	.type	RC_float_with_filename, %function
RC_float_with_filename:
.LFB7:
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	fmsr	s14, r1
	fstmfdd	sp!, {d8}
.LCFI8:
	flds	s16, [r0, #0]
	mov	r4, r0
	fmsr	s15, r2
	sub	sp, sp, #8
.LCFI9:
	fcmpes	s16, s14
	mov	r5, r3	@ float
	fmstat
	bmi	.L33
	fcmpes	s16, s15
	fmstat
	movle	r0, #1
	ble	.L34
.L33:
	ldr	r0, [sp, #36]
	bl	RemovePathFromFileName
	ldr	r3, [sp, #40]
	fmrs	r2, s16
	ldr	r1, [sp, #28]
	str	r3, [sp, #4]
	mov	r3, r5	@ float
	str	r0, [sp, #0]
	ldr	r0, [sp, #32]
	bl	Alert_range_check_failed_float_with_filename
	mov	r0, #0
	str	r5, [r4, #0]	@ float
.L34:
	add	sp, sp, #8
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, pc}
.LFE7:
	.size	RC_float_with_filename, .-RC_float_with_filename
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI6-.LFB6
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI7-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x1c
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/structures/range_check.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xc1
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x34
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x5c
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x84
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xac
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xe4
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x10c
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x134
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x15e
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI9
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"RC_bool_with_filename\000"
.LASF4:
	.ascii	"RC_time_with_filename\000"
.LASF6:
	.ascii	"RC_uns8_with_filename\000"
.LASF5:
	.ascii	"RC_uint32_with_filename\000"
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/stru"
	.ascii	"ctures/range_check.c\000"
.LASF3:
	.ascii	"RC_string_with_filename\000"
.LASF7:
	.ascii	"RC_float_with_filename\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"RC_date_with_filename\000"
.LASF2:
	.ascii	"RC_int32_with_filename\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
