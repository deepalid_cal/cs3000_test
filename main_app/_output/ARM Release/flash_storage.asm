	.file	"flash_storage.c"
	.text
.Ltext0:
	.section	.text.ff_timer_callback,"ax",%progbits
	.align	2
	.type	ff_timer_callback, %function
ff_timer_callback:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	bl	pvTimerGetTimerID
	cmp	r0, #20
	ldrls	pc, [pc, r0, asl #2]
	b	.L2
.L24:
	.word	.L3
	.word	.L4
	.word	.L5
	.word	.L6
	.word	.L7
	.word	.L8
	.word	.L9
	.word	.L10
	.word	.L11
	.word	.L12
	.word	.L13
	.word	.L14
	.word	.L15
	.word	.L16
	.word	.L17
	.word	.L18
	.word	.L19
	.word	.L20
	.word	.L21
	.word	.L22
	.word	.L23
.L3:
	bl	save_file_configuration_controller
	ldr	pc, [sp], #4
.L4:
	bl	save_file_configuration_network
	ldr	pc, [sp], #4
.L5:
	bl	save_file_system_report_records
	ldr	pc, [sp], #4
.L6:
	bl	save_file_poc_report_records
	ldr	pc, [sp], #4
.L20:
	bl	save_file_lights_report_data
	ldr	pc, [sp], #4
.L7:
	bl	save_file_station_report_data
	ldr	pc, [sp], #4
.L8:
	bl	save_file_station_history
	ldr	pc, [sp], #4
.L9:
	bl	save_file_weather_control
	ldr	pc, [sp], #4
.L10:
	bl	save_file_weather_tables
	ldr	pc, [sp], #4
.L11:
	bl	save_file_tpmicro_data
	ldr	pc, [sp], #4
.L12:
	bl	save_file_station_info
	ldr	pc, [sp], #4
.L13:
	bl	save_file_irrigation_system
	ldr	pc, [sp], #4
.L14:
	bl	save_file_POC
	ldr	pc, [sp], #4
.L15:
	bl	save_file_manual_programs
	ldr	pc, [sp], #4
.L16:
	bl	save_file_station_group
	ldr	pc, [sp], #4
.L17:
	bl	save_file_flowsense
	ldr	pc, [sp], #4
.L18:
	bl	save_file_contrast_and_speaker_vol
	ldr	pc, [sp], #4
.L19:
	bl	save_file_LIGHTS
	ldr	pc, [sp], #4
.L21:
	bl	save_file_moisture_sensor
	ldr	pc, [sp], #4
.L22:
	bl	save_file_budget_report_records
	ldr	pc, [sp], #4
.L23:
	bl	save_file_walk_thru
	ldr	pc, [sp], #4
.L2:
	ldr	r0, .L26
	bl	Alert_Message_va
	ldr	pc, [sp], #4
.L27:
	.align	2
.L26:
	.word	.LC0
.LFE16:
	.size	ff_timer_callback, .-ff_timer_callback
	.section	.text.nm_find_latest_version_and_latest_edit_of_a_filename,"ax",%progbits
	.align	2
	.type	nm_find_latest_version_and_latest_edit_of_a_filename, %function
nm_find_latest_version_and_latest_edit_of_a_filename:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI1:
	sub	sp, sp, #4
.LCFI2:
	mov	sl, r0
	mov	r5, r1
	ldr	r4, .L36
	mov	r0, #184
	mov	r1, r4
	mov	r2, #174
	bl	mem_malloc_debug
	mov	r9, r0
	mov	r1, r5
	mov	r2, #32
	bl	strlcpy
	mov	r0, #184
	mov	r1, r4
	mov	r2, r0
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r8, #0
	str	r8, [r5, #44]
	mvn	fp, #0
	mov	r6, r8
	mov	r7, #76
	ldr	r3, .L36+4
	mla	r7, sl, r7, r3
.L31:
	str	r6, [sp, #0]
	mov	r0, sl
	mov	r1, r9
	mov	r2, r4
	mov	r3, #1
	bl	DfFindDirEntry_NM
	mov	r6, r0
	cmn	r0, #1
	beq	.L29
	add	r8, r8, #1
	ldr	r2, [r4, #44]
	ldr	r3, [r5, #44]
	cmp	r2, r3
	bcc	.L30
	mov	r0, r5
	mov	r1, r4
	mov	r2, #184
	bl	memcpy
	mov	fp, r6
.L30:
	ldr	r3, [r7, #72]
	sub	r3, r3, #1
	cmp	r3, r6
	addne	r6, r6, #1
	bne	.L31
.L29:
	cmp	r8, #1
	bls	.L32
	ldr	r3, [r5, #44]
	str	r3, [r9, #44]
	mov	r6, #0
	str	r6, [r5, #36]
	mvn	fp, #0
	mov	r7, #76
	ldr	r3, .L36+4
	mla	r7, sl, r7, r3
.L34:
	str	r6, [sp, #0]
	mov	r0, sl
	mov	r1, r9
	mov	r2, r4
	mov	r3, #5
	bl	DfFindDirEntry_NM
	mov	r6, r0
	cmn	r0, #1
	beq	.L32
	ldr	r2, [r4, #36]
	ldr	r3, [r5, #36]
	cmp	r2, r3
	bcc	.L33
	mov	r0, r5
	mov	r1, r4
	mov	r2, #184
	bl	memcpy
	mov	fp, r6
.L33:
	ldr	r3, [r7, #72]
	sub	r3, r3, #1
	cmp	r3, r6
	addne	r6, r6, #1
	bne	.L34
.L32:
	ldr	r6, .L36
	mov	r0, r9
	mov	r1, r6
	ldr	r2, .L36+8
	bl	mem_free_debug
	mov	r0, r4
	mov	r1, r6
	mov	r2, #300
	bl	mem_free_debug
	cmp	r8, #0
	beq	.L35
	ldr	r3, [r5, #36]
	cmp	r3, #0
	bne	.L35
	ldr	r3, .L36+12
	ldr	r3, [r3, #68]
	cmp	r3, #1
	beq	.L35
	ldr	r0, .L36+16
	bl	Alert_Message
.L35:
	mov	r0, fp
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L37:
	.align	2
.L36:
	.word	.LC1
	.word	ssp_define
	.word	298
	.word	restart_info
	.word	.LC2
.LFE1:
	.size	nm_find_latest_version_and_latest_edit_of_a_filename, .-nm_find_latest_version_and_latest_edit_of_a_filename
	.section	.text.FLASH_STORAGE_delete_all_files,"ax",%progbits
	.align	2
	.global	FLASH_STORAGE_delete_all_files
	.type	FLASH_STORAGE_delete_all_files, %function
FLASH_STORAGE_delete_all_files:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI3:
	sub	sp, sp, #4
.LCFI4:
	mov	r5, r0
	ldr	r4, .L41
	mov	r0, #184
	mov	r1, r4
	mov	r2, #96
	bl	mem_malloc_debug
	mov	r8, r0
	mov	r1, #0
	mov	r2, #184
	bl	memset
	mov	r0, #184
	mov	r1, r4
	mov	r2, #99
	bl	mem_malloc_debug
	mov	r6, r0
	mov	r1, #0
	mov	r2, #184
	bl	memset
	mov	r0, r5
	mvn	r1, #0
	bl	DfTakeStorageMgrMutex
	mov	r4, #0
	mov	r7, #76
	ldr	r3, .L41+4
	mla	r7, r5, r7, r3
.L40:
	str	r4, [sp, #0]
	mov	r0, r5
	mov	r1, r8
	mov	r2, r6
	mov	r3, #0
	bl	DfFindDirEntry_NM
	mov	r4, r0
	cmn	r0, #1
	beq	.L39
	mov	r0, r5
	mov	r1, r4
	mov	r2, r6
	bl	DfDelFileFromDF_NM
	ldr	r3, [r7, #72]
	sub	r3, r3, #1
	cmp	r3, r4
	addne	r4, r4, #1
	bne	.L40
.L39:
	mov	r0, r5
	bl	DfGiveStorageMgrMutex
	ldr	r4, .L41
	mov	r0, r8
	mov	r1, r4
	mov	r2, #144
	bl	mem_free_debug
	mov	r0, r6
	mov	r1, r4
	mov	r2, #145
	bl	mem_free_debug
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L42:
	.align	2
.L41:
	.word	.LC1
	.word	ssp_define
.LFE0:
	.size	FLASH_STORAGE_delete_all_files, .-FLASH_STORAGE_delete_all_files
	.section	.text.FLASH_STORAGE_request_code_image_version_stamp,"ax",%progbits
	.align	2
	.global	FLASH_STORAGE_request_code_image_version_stamp
	.type	FLASH_STORAGE_request_code_image_version_stamp, %function
FLASH_STORAGE_request_code_image_version_stamp:
.LFB2:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI5:
	sub	sp, sp, #28
.LCFI6:
	mov	r4, r0
	ldr	r3, .L47
	cmp	r0, r3
	beq	.L44
	ldr	r3, .L47+4
	cmp	r0, r3
	bne	.L45
.L44:
	mov	r0, sp
	mov	r1, #0
	mov	r2, #28
	bl	memset
	mov	r3, #55
	str	r3, [sp, #0]
	str	r4, [sp, #4]
	ldr	r3, .L47+8
	ldr	r3, [r3, #16]
	ldr	r0, [r3, #0]
	mov	r1, sp
	mov	r2, #0
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L43
	ldr	r0, .L47+12
	bl	RemovePathFromFileName
	mov	r1, r0
	ldr	r0, .L47+16
	ldr	r2, .L47+20
	bl	Alert_Message_va
	b	.L43
.L45:
	ldr	r0, .L47+24
	bl	Alert_Message
.L43:
	add	sp, sp, #28
	ldmfd	sp!, {r4, pc}
.L48:
	.align	2
.L47:
	.word	CS3000_APP_FILENAME
	.word	TPMICRO_APP_FILENAME
	.word	ssp_define
	.word	.LC1
	.word	.LC3
	.word	351
	.word	.LC4
.LFE2:
	.size	FLASH_STORAGE_request_code_image_version_stamp, .-FLASH_STORAGE_request_code_image_version_stamp
	.section	.text.FLASH_STORAGE_request_code_image_file_read,"ax",%progbits
	.align	2
	.global	FLASH_STORAGE_request_code_image_file_read
	.type	FLASH_STORAGE_request_code_image_file_read, %function
FLASH_STORAGE_request_code_image_file_read:
.LFB3:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	sub	sp, sp, #28
.LCFI8:
	mov	r4, r0
	mov	r5, r1
	ldr	r3, .L54
	cmp	r0, r3
	beq	.L50
	ldr	r3, .L54+4
	cmp	r0, r3
	bne	.L51
.L50:
	cmp	r5, #77
	cmpne	r5, #66
	bne	.L52
	mov	r0, sp
	mov	r1, #0
	mov	r2, #28
	bl	memset
	str	r5, [sp, #0]
	str	r4, [sp, #4]
	ldr	r3, .L54+8
	ldr	r3, [r3, #16]
	ldr	r0, [r3, #0]
	mov	r1, sp
	mov	r2, #0
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L49
	ldr	r0, .L54+12
	bl	RemovePathFromFileName
	mov	r1, r0
	ldr	r0, .L54+16
	ldr	r2, .L54+20
	bl	Alert_Message_va
	b	.L49
.L52:
	ldr	r0, .L54+24
	bl	Alert_Message
	b	.L49
.L51:
	ldr	r0, .L54+28
	bl	Alert_Message
.L49:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, pc}
.L55:
	.align	2
.L54:
	.word	CS3000_APP_FILENAME
	.word	TPMICRO_APP_FILENAME
	.word	ssp_define
	.word	.LC1
	.word	.LC3
	.word	398
	.word	.LC5
	.word	.LC6
.LFE3:
	.size	FLASH_STORAGE_request_code_image_file_read, .-FLASH_STORAGE_request_code_image_file_read
	.section	.text.write_data_to_flash_file,"ax",%progbits
	.align	2
	.global	write_data_to_flash_file
	.type	write_data_to_flash_file, %function
write_data_to_flash_file:
.LFB13:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI9:
	sub	sp, sp, #8
.LCFI10:
	mov	r8, r0
	mov	r9, r1
	mov	r0, #184
	ldr	r1, .L70
	mov	r2, #1792
	bl	mem_malloc_debug
	mov	sl, r0
	mov	r1, #0
	mov	r2, #184
	bl	memset
	mov	r0, sl
	ldr	r1, [r9, #4]
	mov	r2, #32
	bl	strlcpy
	mov	r0, r8
	mvn	r1, #0
	bl	DfTakeStorageMgrMutex
	ldr	r1, [r9, #4]
	ldr	r3, .L70+4
	cmp	r1, r3
	beq	.L57
	ldr	r3, .L70+8
	cmp	r1, r3
	bne	.L58
.L57:
	ldr	r0, [r9, #16]
	bl	convert_code_image_date_to_version_number
	str	r0, [sl, #44]
	ldr	r0, [r9, #16]
	ldr	r1, [r9, #4]
	bl	convert_code_image_time_to_edit_count
	str	r0, [sl, #36]
	ldr	r3, [sl, #44]
	cmp	r3, #0
	beq	.L59
	cmp	r0, #0
	bne	.L60
.L59:
	ldr	r0, .L70+12
	bl	Alert_Message
	b	.L60
.L58:
	mov	r0, r8
	mov	r1, sl
	bl	nm_find_latest_version_and_latest_edit_of_a_filename
	cmn	r0, #1
	ldrne	r3, [sl, #36]
	addne	r3, r3, #1
	moveq	r3, #1
	str	r3, [sl, #36]
	ldr	r3, [r9, #8]
	str	r3, [sl, #44]
.L60:
	ldr	r3, [r9, #20]
	str	r3, [sl, #40]
	mov	r0, r8
	mov	r1, sl
	ldr	r2, [r9, #16]
	bl	DfWriteFileToDF_NM
	cmn	r0, #3
	bne	.L63
	ldr	r0, .L70+16
	bl	Alert_Message
	b	.L64
.L63:
	ldr	r2, [sl, #44]
	ldr	r3, [sl, #36]
	ldr	r1, [sl, #40]
	str	r1, [sp, #0]
	mov	r0, r8
	mov	r1, sl
	bl	Alert_flash_file_writing
.L64:
	ldr	r4, .L70
	ldr	r0, [r9, #16]
	mov	r1, r4
	ldr	r2, .L70+20
	bl	mem_free_debug
	ldr	r7, [r9, #4]
	ldr	fp, [sl, #44]
	ldr	r3, [sl, #36]
	str	r3, [sp, #4]
.LBB4:
	mov	r0, #184
	mov	r1, r4
	ldr	r2, .L70+24
	bl	mem_malloc_debug
	mov	r6, r0
	mov	r0, #184
	mov	r1, r4
	ldr	r2, .L70+28
	bl	mem_malloc_debug
	mov	r5, r0
	mov	r0, r6
	mov	r1, r7
	mov	r2, #32
	bl	strlcpy
	str	fp, [r6, #44]
	ldr	r3, [sp, #4]
	str	r3, [r6, #36]
	mov	r4, #0
	mov	r7, #76
	ldr	r3, .L70+32
	mla	r7, r8, r7, r3
.L67:
	str	r4, [sp, #0]
	mov	r0, r8
	mov	r1, r6
	mov	r2, r5
	mov	r3, #1
	bl	DfFindDirEntry_NM
	mov	r4, r0
	cmn	r0, #1
	beq	.L65
	mov	r0, r6
	mov	r1, r5
	mov	r2, #6
	bl	DfDirEntryCompare
	cmp	r0, #0
	beq	.L66
	mov	r0, r8
	mov	r1, r4
	mov	r2, r5
	bl	DfDelFileFromDF_NM
.L66:
	ldr	r3, [r7, #72]
	sub	r3, r3, #1
	cmp	r4, r3
	addne	r4, r4, #1
	bne	.L67
.L65:
	ldr	r4, .L70
	mov	r0, r6
	mov	r1, r4
	ldr	r2, .L70+36
	bl	mem_free_debug
	mov	r0, r5
	mov	r1, r4
	ldr	r2, .L70+40
	bl	mem_free_debug
.LBE4:
	mov	r0, r8
	bl	DfGiveStorageMgrMutex
	mov	r0, sl
	mov	r1, r4
	ldr	r2, .L70+44
	bl	mem_free_debug
	ldr	r3, [r9, #4]
	ldr	r2, .L70+4
	cmp	r3, r2
	bne	.L68
	ldr	r0, .L70+48
	bl	CODE_DISTRIBUTION_post_event
	b	.L56
.L68:
	ldr	r2, .L70+8
	cmp	r3, r2
	bne	.L56
	ldr	r0, .L70+52
	bl	CODE_DISTRIBUTION_post_event
.L56:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L71:
	.align	2
.L70:
	.word	.LC1
	.word	CS3000_APP_FILENAME
	.word	TPMICRO_APP_FILENAME
	.word	.LC7
	.word	.LC8
	.word	1861
	.word	1709
	.word	1711
	.word	ssp_define
	.word	1770
	.word	1772
	.word	1874
	.word	4437
	.word	4454
.LFE13:
	.size	write_data_to_flash_file, .-write_data_to_flash_file
	.section	.text.FLASH_STORAGE_delete_all_files_with_this_name,"ax",%progbits
	.align	2
	.global	FLASH_STORAGE_delete_all_files_with_this_name
	.type	FLASH_STORAGE_delete_all_files_with_this_name, %function
FLASH_STORAGE_delete_all_files_with_this_name:
.LFB14:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI11:
	sub	sp, sp, #28
.LCFI12:
	cmp	r1, #0
	beq	.L72
	mov	r3, #44
	str	r3, [sp, #0]
	str	r1, [sp, #4]
	mov	r2, #76
	ldr	r3, .L74
	mla	r0, r2, r0, r3
	ldr	r3, [r0, #16]
	ldr	r0, [r3, #0]
	mov	r1, sp
	mov	r2, #0
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L72
	ldr	r0, .L74+4
	bl	RemovePathFromFileName
	mov	r1, r0
	ldr	r0, .L74+8
	ldr	r2, .L74+12
	bl	Alert_Message_va
.L72:
	add	sp, sp, #28
	ldmfd	sp!, {pc}
.L75:
	.align	2
.L74:
	.word	ssp_define
	.word	.LC1
	.word	.LC9
	.word	1919
.LFE14:
	.size	FLASH_STORAGE_delete_all_files_with_this_name, .-FLASH_STORAGE_delete_all_files_with_this_name
	.section	.text.FLASH_STORAGE_flash_storage_task,"ax",%progbits
	.align	2
	.global	FLASH_STORAGE_flash_storage_task
	.type	FLASH_STORAGE_flash_storage_task, %function
FLASH_STORAGE_flash_storage_task:
.LFB15:
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI13:
	sub	sp, sp, #76
.LCFI14:
	ldr	r5, .L100
	rsb	r5, r5, r0
	mov	r5, r5, lsr #5
	ldr	r7, [r0, #24]
	mov	r9, #76
	ldr	r3, .L100+4
	mla	r9, r7, r9, r3
	ldr	r6, [r9, #16]
	ldr	sl, .L100+8
.L97:
	ldr	r0, [r6, #0]
	add	r1, sp, #48
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	bne	.L77
	ldr	r3, [sl, #68]
	cmp	r3, #0
	bne	.L77
	ldr	r3, [sp, #48]
	cmp	r3, #55
	beq	.L80
	bhi	.L82
	cmp	r3, #22
	beq	.L78
	cmp	r3, #44
	bne	.L77
	b	.L99
.L82:
	cmp	r3, #66
	beq	.L81
	cmp	r3, #77
	bne	.L77
	b	.L81
.L80:
.LBB11:
	add	r0, sp, #4
	mov	r1, #0
	mov	r2, #40
	bl	memset
	ldr	r2, [sp, #52]
	ldr	r3, .L100+12
	cmp	r2, r3
	movne	r3, #3840
	moveq	r3, #3584
	str	r3, [sp, #4]
	mov	r0, #184
	ldr	r1, .L100+16
	ldr	r2, .L100+20
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r1, #0
	mov	r2, #184
	bl	memset
	mov	r0, r4
	ldr	r1, [sp, #52]
	mov	r2, #32
	bl	strlcpy
	mov	r0, #0
	mvn	r1, #0
	bl	DfTakeStorageMgrMutex
	mov	r0, #0
	mov	r1, r4
	bl	nm_find_latest_version_and_latest_edit_of_a_filename
	cmn	r0, #1
	beq	.L84
	ldr	r3, [r4, #44]
	str	r3, [sp, #28]
	ldr	r3, [r4, #36]
	str	r3, [sp, #32]
	b	.L85
.L84:
	ldr	r3, [sl, #68]
	cmp	r3, #0
	bne	.L85
	mov	r0, r4
	bl	Alert_flash_file_not_found
.L85:
	mov	r0, #0
	bl	DfGiveStorageMgrMutex
	ldr	r3, [sl, #68]
	cmp	r3, #0
	beq	.L86
	ldr	r0, .L100+24
	mov	r1, r4
	bl	Alert_Message_va
.L86:
	add	r0, sp, #4
	bl	COMM_MNGR_post_event_with_details
	mov	r0, r4
	ldr	r1, .L100+16
	mov	r2, #520
	bl	mem_free_debug
	b	.L77
.L81:
.LBE11:
.LBB12:
	add	r0, sp, #4
	mov	r1, #0
	mov	r2, #24
	bl	memset
	ldr	r3, [sp, #48]
	cmp	r3, #66
	ldreq	r3, .L100+28
	streq	r3, [sp, #4]
	beq	.L88
	cmp	r3, #77
	bne	.L89
	ldr	r2, [sp, #52]
	ldr	r3, .L100+12
	cmp	r2, r3
	ldreq	r3, .L100+32
	ldrne	r3, .L100+36
	str	r3, [sp, #4]
	b	.L88
.L89:
	ldr	r0, .L100+40
	bl	Alert_Message
	b	.L77
.L88:
	mov	r0, #184
	ldr	r1, .L100+16
	mov	r2, #588
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r1, #0
	mov	r2, #184
	bl	memset
	mov	r0, r4
	ldr	r1, [sp, #52]
	mov	r2, #32
	bl	strlcpy
	mov	r0, #0
	mvn	r1, #0
	bl	DfTakeStorageMgrMutex
	mov	r0, #0
	mov	r1, r4
	bl	nm_find_latest_version_and_latest_edit_of_a_filename
	cmn	r0, #1
	beq	.L91
	ldr	r0, [r4, #40]
	add	r1, sp, #44
	ldr	r2, .L100+16
	ldr	r3, .L100+44
	bl	mem_oabia
	cmp	r0, #0
	beq	.L92
	mov	r0, #0
	mov	r1, r4
	ldr	r2, [sp, #44]
	bl	DfReadFileFromDF_NM
	cmp	r0, #0
	beq	.L93
	ldr	r0, [sp, #44]
	ldr	r1, .L100+16
	mov	r2, #620
	bl	mem_free_debug
	mov	r3, #0
	str	r3, [sp, #44]
	b	.L93
.L92:
	ldr	r0, .L100+48
	bl	Alert_Message
	b	.L93
.L91:
	ldr	r3, [sl, #68]
	cmp	r3, #0
	bne	.L93
	mov	r0, r4
	bl	Alert_flash_file_not_found
.L93:
	mov	r0, #0
	bl	DfGiveStorageMgrMutex
	ldr	r3, [sl, #68]
	cmp	r3, #0
	beq	.L94
	ldr	r0, .L100+52
	mov	r1, r4
	bl	Alert_Message_va
.L94:
	ldr	r3, [r4, #40]
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	str	r3, [sp, #12]
	add	r0, sp, #4
	bl	CODE_DISTRIBUTION_post_event_with_details
	mov	r0, r4
	ldr	r1, .L100+16
	ldr	r2, .L100+56
	bl	mem_free_debug
	b	.L77
.L78:
.LBE12:
	ldr	r0, [sp, #72]
	bl	CHAIN_SYNC_inhibit_crc_inclusion_in_the_token_response
	mov	r0, r7
	add	r1, sp, #48
	bl	write_data_to_flash_file
	b	.L77
.L99:
.LBB13:
	mov	r0, r7
	mvn	r1, #0
	bl	DfTakeStorageMgrMutex
	ldr	r4, .L100+16
	mov	r0, #184
	mov	r1, r4
	ldr	r2, .L100+60
	bl	mem_malloc_debug
	mov	fp, r0
	mov	r1, #0
	mov	r2, #184
	bl	memset
	mov	r0, #184
	mov	r1, r4
	ldr	r2, .L100+64
	bl	mem_malloc_debug
	mov	r8, r0
	mov	r1, #0
	mov	r2, #184
	bl	memset
	mov	r0, fp
	ldr	r1, [sp, #52]
	mov	r2, #32
	bl	strlcpy
	mov	r4, #0
.L96:
	str	r4, [sp, #0]
	mov	r0, r7
	mov	r1, fp
	mov	r2, r8
	mov	r3, #1
	bl	DfFindDirEntry_NM
	mov	r4, r0
	cmn	r0, #1
	beq	.L95
	mov	r0, r7
	mov	r1, r4
	mov	r2, r8
	bl	DfDelFileFromDF_NM
	ldr	r3, [r9, #72]
	sub	r3, r3, #1
	cmp	r4, r3
	addne	r4, r4, #1
	bne	.L96
.L95:
	ldr	r4, .L100+16
	mov	r0, fp
	mov	r1, r4
	ldr	r2, .L100+68
	bl	mem_free_debug
	mov	r0, r8
	mov	r1, r4
	ldr	r2, .L100+72
	bl	mem_free_debug
	mov	r0, r7
	bl	DfGiveStorageMgrMutex
.L77:
.LBE13:
	bl	xTaskGetTickCount
	ldr	r3, .L100+76
	str	r0, [r3, r5, asl #2]
	b	.L97
.L101:
	.align	2
.L100:
	.word	Task_Table
	.word	ssp_define
	.word	restart_info
	.word	CS3000_APP_FILENAME
	.word	.LC1
	.word	455
	.word	.LC10
	.word	4420
	.word	4386
	.word	4403
	.word	.LC11
	.word	614
	.word	.LC12
	.word	.LC13
	.word	673
	.word	1636
	.word	1639
	.word	1688
	.word	1690
	.word	task_last_execution_stamp
.LFE15:
	.size	FLASH_STORAGE_flash_storage_task, .-FLASH_STORAGE_flash_storage_task
	.section	.text.FLASH_STORAGE_create_delayed_file_save_timers,"ax",%progbits
	.align	2
	.global	FLASH_STORAGE_create_delayed_file_save_timers
	.type	FLASH_STORAGE_create_delayed_file_save_timers, %function
FLASH_STORAGE_create_delayed_file_save_timers:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI15:
	sub	sp, sp, #4
.LCFI16:
	ldr	r5, .L105
	mov	r4, #0
	ldr	r6, .L105+4
	ldr	r7, .L105+8
	mov	r8, #200
.L103:
	str	r7, [sp, #0]
	mov	r0, r6
	mov	r1, r8
	mov	r2, #0
	mov	r3, r4
	bl	xTimerCreate
	str	r0, [r5, #4]!
	add	r4, r4, #1
	cmp	r4, #21
	bne	.L103
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L106:
	.align	2
.L105:
	.word	.LANCHOR0-4
	.word	.LC14
	.word	ff_timer_callback
.LFE17:
	.size	FLASH_STORAGE_create_delayed_file_save_timers, .-FLASH_STORAGE_create_delayed_file_save_timers
	.section	.text.FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds,"ax",%progbits
	.align	2
	.global	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	.type	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds, %function
FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI17:
	sub	sp, sp, #4
.LCFI18:
	mov	r3, r0
	cmp	r1, #0
	movne	r2, #1000
	mulne	r2, r1, r2
	ldrne	r0, .L112
	umullne	r1, r2, r0, r2
	movne	r2, r2, lsr #2
	moveq	r2, #5
	cmp	r3, #20
	bls	.L109
	ldr	r0, .L112+4
	mov	r1, r3
	bl	Alert_Message_va
	b	.L107
.L109:
	ldr	r1, .L112+8
	ldr	r0, [r1, r3, asl #2]
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, #0
	bl	xTimerGenericCommand
.L107:
	add	sp, sp, #4
	ldmfd	sp!, {pc}
.L113:
	.align	2
.L112:
	.word	-858993459
	.word	.LC15
	.word	.LANCHOR0
.LFE18:
	.size	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds, .-FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	.section	.text.FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file,"ax",%progbits
	.align	2
	.global	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	.type	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file, %function
FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file:
.LFB10:
	@ args = 12, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI19:
	sub	sp, sp, #32
.LCFI20:
	str	r0, [sp, #0]
	mov	r8, r1
	mov	r9, r2
	mov	r7, r3
	ldr	r6, [sp, #68]
	ldr	sl, [sp, #76]
	ldr	fp, [r3, #8]
	cmp	sl, #20
	bhi	.L115
	mul	fp, r6, fp
	mov	r0, fp
	add	r1, sp, #20
	ldr	r2, .L124
	ldr	r3, .L124+4
	bl	mem_oabia
	cmp	r0, #1
	bne	.L116
	ldr	r1, [sp, #72]
	cmp	r1, #0
	beq	.L117
	mov	r0, r1
	mov	r1, #400
	ldr	r2, .L124
	ldr	r3, .L124+8
	bl	xQueueTakeMutexRecursive_debug
.L117:
	ldr	r5, [sp, #20]
	mov	r0, r7
	bl	nm_ListGetFirst
	subs	r4, r0, #0
	beq	.L118
.L122:
	mov	r0, r5
	mov	r1, r4
	mov	r2, r6
	bl	memcpy
	add	r5, r5, r6
	mov	r0, r7
	mov	r1, r4
	bl	nm_ListGetNext
	subs	r4, r0, #0
	bne	.L122
.L118:
	ldr	r3, [sp, #72]
	cmp	r3, #0
	beq	.L120
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L120:
	mov	r3, #22
	str	r3, [sp, #4]
	str	r8, [sp, #8]
	str	r9, [sp, #12]
	str	fp, [sp, #24]
	str	sl, [sp, #28]
	mov	r2, #76
	ldr	r3, .L124+12
	ldr	r1, [sp, #0]
	mla	r3, r2, r1, r3
	ldr	r3, [r3, #16]
	ldr	r0, [r3, #0]
	add	r1, sp, #4
	mov	r2, #0
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L114
	ldr	r0, .L124
	bl	RemovePathFromFileName
	mov	r1, r0
	ldr	r0, .L124+16
	ldr	r2, .L124+20
	bl	Alert_Message_va
	b	.L114
.L116:
	mov	r0, r8
	bl	Alert_flash_file_write_postponed
	mov	r0, sl
	mov	r1, #5
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L114
.L115:
	ldr	r0, .L124+24
	bl	Alert_Message
.L114:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L125:
	.align	2
.L124:
	.word	.LC1
	.word	1526
	.word	1544
	.word	ssp_define
	.word	.LC9
	.word	1583
	.word	.LC16
.LFE10:
	.size	FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file, .-FLASH_STORAGE_make_a_copy_and_write_list_to_flash_file
	.section	.text.FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file,"ax",%progbits
	.align	2
	.global	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	.type	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file, %function
FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file:
.LFB9:
	@ args = 12, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI21:
	sub	sp, sp, #28
.LCFI22:
	mov	sl, r0
	mov	r6, r1
	mov	r8, r2
	mov	r4, r3
	ldr	r9, [sp, #64]
	ldr	fp, [sp, #68]
	ldr	r7, [sp, #72]
	ldr	r3, .L138
	cmp	r1, r3
	beq	.L127
	ldr	r3, .L138+4
	cmp	r1, r3
	bne	.L128
.L127:
	str	r4, [sp, #16]
	b	.L129
.L128:
	cmp	r7, #20
	bhi	.L130
	mov	r0, r9
	add	r1, sp, #16
	ldr	r2, .L138+8
	ldr	r3, .L138+12
	bl	mem_oabia
	cmp	r0, #1
	bne	.L131
	cmp	fp, #0
	beq	.L132
	mov	r0, fp
	mov	r1, #400
	ldr	r2, .L138+8
	ldr	r3, .L138+16
	bl	xQueueTakeMutexRecursive_debug
.L132:
	ldr	r0, [sp, #16]
	mov	r1, r4
	mov	r2, r9
	bl	memcpy
	cmp	r7, #5
	bne	.L133
	ldr	r3, .L138+20
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L138+8
	ldr	r3, .L138+24
	bl	xQueueTakeMutexRecursive_debug
	mov	r4, #0
	ldr	r5, .L138+28
.L135:
	add	r3, r5, r4, asl #7
	ldrb	r3, [r3, #141]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L134
	mov	r0, r4, asl #7
	add	r3, r5, r0
	ldrb	r2, [r3, #141]	@ zero_extendqisi2
	bic	r2, r2, #64
	strb	r2, [r3, #141]
	add	r0, r0, #16
	add	r0, r5, r0
	bl	nm_init_station_history_record
.L134:
	add	r4, r4, #1
	cmp	r4, #2112
	bne	.L135
	ldr	r3, .L138+20
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L133:
	cmp	fp, #0
	beq	.L129
	mov	r0, fp
	bl	xQueueGiveMutexRecursive
	b	.L129
.L131:
	mov	r0, r6
	bl	Alert_flash_file_write_postponed
	mov	r0, r7
	mov	r1, #5
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L126
.L130:
	ldr	r0, .L138+32
	bl	Alert_Message
	b	.L126
.L129:
	mov	r3, #22
	str	r3, [sp, #0]
	str	r6, [sp, #4]
	str	r8, [sp, #8]
	str	r9, [sp, #20]
	str	r7, [sp, #24]
	mov	r2, #76
	ldr	r3, .L138+36
	mla	sl, r2, sl, r3
	ldr	r3, [sl, #16]
	ldr	r0, [r3, #0]
	mov	r1, sp
	mov	r2, #0
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L126
	ldr	r0, .L138+8
	bl	RemovePathFromFileName
	mov	r1, r0
	ldr	r0, .L138+40
	ldr	r2, .L138+44
	bl	Alert_Message_va
.L126:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L139:
	.align	2
.L138:
	.word	CS3000_APP_FILENAME
	.word	TPMICRO_APP_FILENAME
	.word	.LC1
	.word	1406
	.word	1417
	.word	station_preserves_recursive_MUTEX
	.word	1434
	.word	station_preserves
	.word	.LC17
	.word	ssp_define
	.word	.LC9
	.word	1493
.LFE9:
	.size	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file, .-FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	.section	.text.FLASH_FILE_find_or_create_reports_file,"ax",%progbits
	.align	2
	.global	FLASH_FILE_find_or_create_reports_file
	.type	FLASH_FILE_find_or_create_reports_file, %function
FLASH_FILE_find_or_create_reports_file:
.LFB8:
	@ args = 28, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI23:
	sub	sp, sp, #16
.LCFI24:
	mov	r7, r0
	mov	r5, r1
	mov	r4, r2
	str	r3, [sp, #8]
	ldr	sl, [sp, #52]
	ldr	fp, [sp, #56]
	ldr	r8, [sp, #64]
	cmp	r8, #0
	beq	.L141
	mov	r0, r8
	mov	r1, #400
	ldr	r2, .L158
	ldr	r3, .L158+4
	bl	xQueueTakeMutexRecursive_debug
.L141:
	mov	r0, #184
	ldr	r1, .L158
	ldr	r2, .L158+8
	bl	mem_malloc_debug
	mov	r6, r0
	mov	r1, #0
	mov	r2, #184
	bl	memset
	mov	r0, r6
	mov	r1, r5
	mov	r2, #32
	bl	strlcpy
	mov	r0, r7
	mvn	r1, #0
	bl	DfTakeStorageMgrMutex
	mov	r0, r7
	mov	r1, r6
	bl	nm_find_latest_version_and_latest_edit_of_a_filename
	cmn	r0, #1
	beq	.L142
	ldr	r3, [r6, #44]
	cmp	r3, r4
	movcs	r3, #0
	strcs	r3, [sp, #0]
	bcs	.L143
	mov	r0, r5
	bl	Alert_flash_file_found_old_version
	mov	r2, #1
	str	r2, [sp, #0]
.L143:
	ldr	r2, [sl, r4, asl #2]
	str	r2, [sp, #4]
	ldr	r3, [r6, #44]
	ldr	sl, [sl, r3, asl #2]
	ldr	r9, [fp, r3, asl #2]
	ldr	r3, [fp, r4, asl #2]
	cmp	r2, sl
	cmpcs	r3, r9
	bcc	.L144
	ldr	r0, [r6, #40]
	mul	r3, sl, r9
	add	r3, r3, #60
	cmp	r0, r3
	bne	.L144
	ldr	r1, .L158
	ldr	r2, .L158+12
	bl	mem_malloc_debug
	str	r0, [sp, #12]
	mov	r0, r7
	mov	r1, r6
	ldr	r2, [sp, #12]
	bl	DfReadFileFromDF_NM
	ldr	r0, [sp, #8]
	ldr	r1, [sp, #12]
	mov	r2, #60
	bl	memcpy
	cmp	r9, #0
	beq	.L145
	ldr	r3, [sp, #8]
	add	r5, r3, #60
	ldr	r2, [sp, #12]
	add	r4, r2, #60
	mov	fp, #0
	str	r6, [sp, #8]
	ldr	r6, [sp, #4]
.L146:
	mov	r0, r5
	mov	r1, r4
	mov	r2, sl
	bl	memcpy
	add	r4, r4, sl
	add	r5, r5, r6
	add	fp, fp, #1
	cmp	fp, r9
	bne	.L146
	ldr	r6, [sp, #8]
.L145:
	ldr	r0, [sp, #12]
	ldr	r1, .L158
	ldr	r2, .L158+16
	bl	mem_free_debug
	mov	r4, #0
	b	.L147
.L144:
	mov	r0, r5
	bl	Alert_flash_file_size_error
	mov	r4, #1
	b	.L147
.L142:
	ldr	r3, .L158+20
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L148
	ldr	r0, .L158+24
	mov	r1, r5
	bl	Alert_Message_va
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r4, r3
	b	.L147
.L148:
	mov	r0, r5
	bl	Alert_flash_file_not_found
	mov	r2, #0
	str	r2, [sp, #0]
	mov	r4, #1
.L147:
	mov	r0, r7
	bl	DfGiveStorageMgrMutex
	ldr	r3, .L158+20
	ldr	r3, [r3, #68]
	cmp	r3, #0
	bne	.L149
	cmp	r4, #0
	beq	.L150
	ldr	r3, [sp, #72]
	cmp	r3, #0
	beq	.L151
	blx	r3
	b	.L152
.L151:
	ldr	r0, .L158+28
	bl	Alert_Message
	b	.L152
.L150:
	ldr	r2, [sp, #0]
	cmp	r2, #0
	beq	.L149
	ldr	r3, [sp, #68]
	cmp	r3, #0
	beq	.L153
	ldr	r0, [r6, #44]
	blx	r3
	b	.L154
.L153:
	ldr	r0, .L158+32
	bl	Alert_Message
	b	.L154
.L152:
	ldr	r2, [sp, #0]
	orrs	r2, r2, r4
	beq	.L149
.L154:
	ldr	r0, [sp, #76]
	mov	r1, #10
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L149:
	mov	r0, r6
	ldr	r1, .L158
	ldr	r2, .L158+36
	bl	mem_free_debug
	cmp	r8, #0
	beq	.L140
	mov	r0, r8
	bl	xQueueGiveMutexRecursive
.L140:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L159:
	.align	2
.L158:
	.word	.LC1
	.word	1157
	.word	1162
	.word	1225
	.word	1254
	.word	restart_info
	.word	.LC13
	.word	.LC18
	.word	.LC19
	.word	1342
.LFE8:
	.size	FLASH_FILE_find_or_create_reports_file, .-FLASH_FILE_find_or_create_reports_file
	.global	__umodsi3
	.global	__udivsi3
	.section	.text.FLASH_FILE_initialize_list_and_find_or_create_group_list_file,"ax",%progbits
	.align	2
	.global	FLASH_FILE_initialize_list_and_find_or_create_group_list_file
	.type	FLASH_FILE_initialize_list_and_find_or_create_group_list_file, %function
FLASH_FILE_initialize_list_and_find_or_create_group_list_file:
.LFB7:
	@ args = 24, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI25:
	sub	sp, sp, #16
.LCFI26:
	mov	r7, r0
	mov	sl, r1
	mov	r5, r2
	mov	r9, r3
	ldr	r6, [sp, #52]
	ldr	r8, [sp, #56]
	cmp	r8, #0
	beq	.L161
	mov	r0, r8
	mov	r1, #400
	ldr	r2, .L177
	ldr	r3, .L177+4
	bl	xQueueTakeMutexRecursive_debug
.L161:
	mov	r0, r9
	mov	r1, #0
	bl	nm_ListInit
	mov	r0, #184
	ldr	r1, .L177
	mov	r2, #912
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r1, #0
	mov	r2, #184
	bl	memset
	mov	r0, r4
	mov	r1, sl
	mov	r2, #32
	bl	strlcpy
	ldr	fp, [r6, r5, asl #2]
	mov	r0, r7
	mvn	r1, #0
	bl	DfTakeStorageMgrMutex
	mov	r0, r7
	mov	r1, r4
	bl	nm_find_latest_version_and_latest_edit_of_a_filename
	cmn	r0, #1
	beq	.L162
	ldr	r3, [r4, #44]
	cmp	r5, r3
	movls	r3, #0
	strls	r3, [sp, #8]
	bls	.L163
	mov	r0, sl
	bl	Alert_flash_file_found_old_version
	mov	r3, #1
	str	r3, [sp, #8]
.L163:
	ldr	r3, [r4, #44]
	ldr	r6, [r6, r3, asl #2]
	ldr	r5, [r4, #40]
	mov	r0, r5
	mov	r1, r6
	bl	__umodsi3
	cmp	fp, r6
	cmpcs	r0, #0
	bne	.L164
	mov	r0, r5
	mov	r1, r6
	bl	__udivsi3
	cmp	r0, #768
	bhi	.L164
	cmp	r5, #0
	beq	.L164
	mov	r0, r5
	ldr	r1, .L177
	mov	r2, #976
	bl	mem_malloc_debug
	str	r0, [sp, #12]
	mov	r0, r7
	mov	r1, r4
	ldr	r2, [sp, #12]
	bl	DfReadFileFromDF_NM
	ldr	r5, [sp, #12]
	mov	sl, r7
.L165:
	mov	r0, fp
	ldr	r1, .L177
	ldr	r2, .L177+8
	bl	mem_malloc_debug
	mov	r7, r0
	mov	r1, r5
	mov	r2, r6
	bl	memcpy
	mov	r0, r9
	mov	r1, r7
	bl	nm_ListInsertTail
	add	r5, r5, r6
	ldr	r3, [r4, #40]
	rsb	r3, r6, r3
	str	r3, [r4, #40]
	cmp	r3, #0
	bne	.L165
	mov	r7, sl
	ldr	r0, [sp, #12]
	ldr	r1, .L177
	ldr	r2, .L177+12
	bl	mem_free_debug
	mov	r5, #0
	b	.L166
.L164:
	mov	r0, sl
	bl	Alert_flash_file_size_error
	mov	r5, #1
	b	.L166
.L162:
	ldr	r3, .L177+16
	ldr	r3, [r3, #68]
	cmp	r3, #1
	bne	.L167
	ldr	r0, .L177+20
	mov	r1, sl
	bl	Alert_Message_va
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r5, r3
	b	.L166
.L167:
	mov	r0, sl
	bl	Alert_flash_file_not_found
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r5, #1
.L166:
	mov	r0, r7
	bl	DfGiveStorageMgrMutex
	ldr	r3, .L177+16
	ldr	r3, [r3, #68]
	cmp	r3, #0
	bne	.L168
	cmp	r5, #0
	beq	.L169
	ldr	r3, [sp, #64]
	cmp	r3, #0
	beq	.L170
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r9
	ldr	r1, [sp, #68]
	ldr	r2, [sp, #64]
	mov	r3, fp
	bl	nm_GROUP_create_new_group
	b	.L171
.L170:
	ldr	r0, .L177+24
	bl	Alert_Message
	b	.L171
.L169:
	ldr	r3, [sp, #8]
	cmp	r3, #0
	beq	.L168
	ldr	r3, [sp, #60]
	cmp	r3, #0
	beq	.L172
	ldr	r0, [r4, #44]
	blx	r3
	b	.L173
.L172:
	ldr	r0, .L177+28
	bl	Alert_Message
	b	.L173
.L171:
	ldr	r3, [sp, #8]
	orrs	r3, r3, r5
	beq	.L168
.L173:
	ldr	r0, [sp, #72]
	mov	r1, #10
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L168:
	mov	r0, r4
	ldr	r1, .L177
	ldr	r2, .L177+32
	bl	mem_free_debug
	cmp	r8, #0
	beq	.L160
	mov	r0, r8
	bl	xQueueGiveMutexRecursive
.L160:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L178:
	.align	2
.L177:
	.word	.LC1
	.word	901
	.word	987
	.word	1001
	.word	restart_info
	.word	.LC13
	.word	.LC18
	.word	.LC19
	.word	1089
.LFE7:
	.size	FLASH_FILE_initialize_list_and_find_or_create_group_list_file, .-FLASH_FILE_initialize_list_and_find_or_create_group_list_file
	.section	.text.FLASH_FILE_find_or_create_variable_file,"ax",%progbits
	.align	2
	.global	FLASH_FILE_find_or_create_variable_file
	.type	FLASH_FILE_find_or_create_variable_file, %function
FLASH_FILE_find_or_create_variable_file:
.LFB6:
	@ args = 20, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI27:
	mov	r6, r0
	mov	r8, r1
	mov	r5, r2
	mov	r9, r3
	ldr	r7, [sp, #40]
	ldr	fp, [sp, #48]
	cmp	r7, #0
	beq	.L180
	mov	r0, r7
	mov	r1, #400
	ldr	r2, .L194
	mov	r3, #700
	bl	xQueueTakeMutexRecursive_debug
.L180:
	mov	r0, #184
	ldr	r1, .L194
	ldr	r2, .L194+4
	bl	mem_malloc_debug
	mov	r4, r0
	mov	r1, #0
	mov	r2, #184
	bl	memset
	mov	r0, r4
	mov	r1, r8
	mov	r2, #32
	bl	strlcpy
	mov	r0, r6
	mvn	r1, #0
	bl	DfTakeStorageMgrMutex
	mov	r0, r6
	mov	r1, r4
	bl	nm_find_latest_version_and_latest_edit_of_a_filename
	cmn	r0, #1
	beq	.L181
	ldr	r3, [r4, #44]
	cmp	r3, r5
	movcs	sl, #0
	bcs	.L182
	mov	r0, r8
	bl	Alert_flash_file_found_old_version
	mov	sl, #1
.L182:
	ldr	r3, [r4, #40]
	cmp	r3, #0
	beq	.L183
	ldr	r2, [sp, #36]
	cmp	r3, r2
	bhi	.L183
	mov	r0, r6
	mov	r1, r4
	mov	r2, r9
	bl	DfReadFileFromDF_NM
	mov	r5, #0
	b	.L184
.L183:
	mov	r0, r8
	bl	Alert_flash_file_size_error
	mov	r5, #1
	b	.L184
.L181:
	ldr	r3, .L194+8
	ldr	r3, [r3, #68]
	cmp	r3, #0
	beq	.L185
	ldr	r0, .L194+12
	mov	r1, r8
	bl	Alert_Message_va
	mov	sl, #0
	mov	r5, sl
	b	.L184
.L185:
	mov	r0, r8
	bl	Alert_flash_file_not_found
	mov	sl, #0
	mov	r5, #1
.L184:
	mov	r0, r6
	bl	DfGiveStorageMgrMutex
	ldr	r3, .L194+8
	ldr	r3, [r3, #68]
	cmp	r3, #0
	bne	.L186
	cmp	r5, #0
	beq	.L187
	cmp	fp, #0
	beq	.L188
	blx	fp
	b	.L189
.L188:
	ldr	r0, .L194+16
	bl	Alert_Message
	b	.L189
.L187:
	cmp	sl, #0
	beq	.L186
	ldr	r3, [sp, #44]
	cmp	r3, #0
	beq	.L190
	ldr	r0, [r4, #44]
	blx	r3
	b	.L191
.L190:
	ldr	r0, .L194+20
	bl	Alert_Message
	b	.L191
.L189:
	orrs	sl, sl, r5
	beq	.L186
.L191:
	ldr	r0, [sp, #52]
	mov	r1, #10
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L186:
	mov	r0, r4
	ldr	r1, .L194
	ldr	r2, .L194+24
	bl	mem_free_debug
	cmp	r7, #0
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
	mov	r0, r7
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L195:
	.align	2
.L194:
	.word	.LC1
	.word	705
	.word	restart_info
	.word	.LC13
	.word	.LC20
	.word	.LC19
	.word	839
.LFE6:
	.size	FLASH_FILE_find_or_create_variable_file, .-FLASH_FILE_find_or_create_variable_file
	.section	.text.FLASH_STORAGE_if_not_running_start_file_save_timer,"ax",%progbits
	.align	2
	.global	FLASH_STORAGE_if_not_running_start_file_save_timer
	.type	FLASH_STORAGE_if_not_running_start_file_save_timer, %function
FLASH_STORAGE_if_not_running_start_file_save_timer:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI28:
	sub	sp, sp, #4
.LCFI29:
	mov	r4, r0
	mov	r5, r1
	cmp	r0, #20
	bls	.L197
	ldr	r0, .L201
	mov	r1, r4
	bl	Alert_Message_va
	b	.L196
.L197:
	ldr	r3, .L201+4
	ldr	r3, [r3, r0, asl #2]
	cmp	r3, #0
	bne	.L199
	ldr	r0, .L201+8
	mov	r1, r4
	bl	Alert_Message_va
	b	.L196
.L199:
	cmp	r1, #0
	bne	.L200
	ldr	r0, .L201+12
	bl	Alert_Message
	mov	r5, #1
.L200:
	ldr	r3, .L201+4
	ldr	r0, [r3, r4, asl #2]
	bl	xTimerIsTimerActive
	cmp	r0, #0
	bne	.L196
	ldr	r3, .L201+4
	ldr	r0, [r3, r4, asl #2]
	mov	r2, #1000
	mul	r5, r2, r5
	ldr	r3, .L201+16
	umull	r1, r2, r3, r5
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r2, r2, lsr r1
	mov	r3, #0
	bl	xTimerGenericCommand
.L196:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, pc}
.L202:
	.align	2
.L201:
	.word	.LC15
	.word	.LANCHOR0
	.word	.LC21
	.word	.LC22
	.word	-858993459
.LFE19:
	.size	FLASH_STORAGE_if_not_running_start_file_save_timer, .-FLASH_STORAGE_if_not_running_start_file_save_timer
	.global	ff_timers
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"FILE SAVE: %d case not handled\000"
	.space	1
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/flas"
	.ascii	"h_storage/flash_storage.c\000"
	.space	3
.LC2:
	.ascii	"DF: condition should not exist.\000"
.LC3:
	.ascii	"FLASH STORAGE: queue full : %s, %u\000"
	.space	1
.LC4:
	.ascii	"FILE VERSION: not a code file!\000"
	.space	1
.LC5:
	.ascii	"FLASH: unexpd parameter\000"
.LC6:
	.ascii	"FILE READ: not a code file!\000"
.LC7:
	.ascii	"FILE: code revision conversion problem\000"
	.space	1
.LC8:
	.ascii	"FILE: file should not already exist!\000"
	.space	3
.LC9:
	.ascii	"FLASH STORAGE: no room on queue : %s, %u\000"
	.space	3
.LC10:
	.ascii	"File Ver Read: abort due to power fail (%s).\000"
	.space	3
.LC11:
	.ascii	"FLASH STORAGE - improper parameter\000"
	.space	1
.LC12:
	.ascii	"File read: code image memory block not available\000"
	.space	3
.LC13:
	.ascii	"File Read: abort due to power fail (%s).\000"
	.space	3
.LC14:
	.ascii	"\000"
	.space	3
.LC15:
	.ascii	"FILE WRITE: file number %u out of range\000"
.LC16:
	.ascii	"LIST FILE WRITE: file number out of range!\000"
	.space	1
.LC17:
	.ascii	"DATA FILE WRITE: file number out of range!\000"
	.space	1
.LC18:
	.ascii	"Flash List: trying to initialize but no function\000"
	.space	3
.LC19:
	.ascii	"Flash File: trying to update but no function\000"
	.space	3
.LC20:
	.ascii	"Flash File: trying to initialize but no function\000"
	.space	3
.LC21:
	.ascii	"FILE WRITE: timer doesn't exist %u\000"
	.space	1
.LC22:
	.ascii	"file save in 0 seconds?\000"
	.section	.bss.ff_timers,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	ff_timers, %object
	.size	ff_timers, 84
ff_timers:
	.space	84
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI0-.LFB16
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI3-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x1c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI5-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI7-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI9-.LFB13
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI11-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI13-.LFB15
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x70
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI15-.LFB17
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x1c
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI17-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xe
	.uleb128 0x8
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI19-.LFB10
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI20-.LCFI19
	.byte	0xe
	.uleb128 0x44
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI21-.LFB9
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI23-.LFB8
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI24-.LCFI23
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI25-.LFB7
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI26-.LCFI25
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI27-.LFB6
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI28-.LFB19
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI29-.LCFI28
	.byte	0xe
	.uleb128 0x10
	.align	2
.LEFDE30:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/flash_storage/flash_storage.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x18e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF20
	.byte	0x1
	.4byte	.LASF21
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x7f8
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0x95
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x5a
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x13d
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x16b
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST4
	.uleb128 0x6
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x6a0
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x6f0
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x773
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST6
	.uleb128 0x6
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x19d
	.byte	0x1
	.uleb128 0x6
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x20c
	.byte	0x1
	.uleb128 0x6
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x658
	.byte	0x1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x786
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST7
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x85e
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST8
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x88f
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST9
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x5dd
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST10
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x54a
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST11
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x44d
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST12
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x354
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST13
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x2a6
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST14
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x8af
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST15
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB16
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB0
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI4
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB2
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB3
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI8
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB13
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI10
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB14
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI12
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB15
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI14
	.4byte	.LFE15
	.2byte	0x3
	.byte	0x7d
	.sleb128 112
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB17
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI16
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB18
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI18
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB10
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI20
	.4byte	.LFE10
	.2byte	0x3
	.byte	0x7d
	.sleb128 68
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB9
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI22
	.4byte	.LFE9
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB8
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI24
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB7
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI26
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB6
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB19
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI29
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x94
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF18:
	.ascii	"FLASH_FILE_find_or_create_variable_file\000"
.LASF12:
	.ascii	"FLASH_STORAGE_create_delayed_file_save_timers\000"
.LASF19:
	.ascii	"FLASH_STORAGE_if_not_running_start_file_save_timer\000"
.LASF21:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/flas"
	.ascii	"h_storage/flash_storage.c\000"
.LASF0:
	.ascii	"ff_timer_callback\000"
.LASF15:
	.ascii	"FLASH_STORAGE_make_a_copy_and_write_data_to_flash_f"
	.ascii	"ile\000"
.LASF16:
	.ascii	"FLASH_FILE_find_or_create_reports_file\000"
.LASF1:
	.ascii	"nm_find_latest_version_and_latest_edit_of_a_filenam"
	.ascii	"e\000"
.LASF20:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF11:
	.ascii	"FLASH_STORAGE_flash_storage_task\000"
.LASF8:
	.ascii	"return_code_image_version_stamp\000"
.LASF7:
	.ascii	"nm_find_older_files_and_delete\000"
.LASF4:
	.ascii	"FLASH_STORAGE_request_code_image_file_read\000"
.LASF3:
	.ascii	"FLASH_STORAGE_request_code_image_version_stamp\000"
.LASF14:
	.ascii	"FLASH_STORAGE_make_a_copy_and_write_list_to_flash_f"
	.ascii	"ile\000"
.LASF9:
	.ascii	"read_code_image_file_to_memory\000"
.LASF10:
	.ascii	"delete_all_files_with_this_name\000"
.LASF17:
	.ascii	"FLASH_FILE_initialize_list_and_find_or_create_group"
	.ascii	"_list_file\000"
.LASF2:
	.ascii	"FLASH_STORAGE_delete_all_files\000"
.LASF13:
	.ascii	"FLASH_STORAGE_initiate_a_time_delayed_file_save_sec"
	.ascii	"onds\000"
.LASF5:
	.ascii	"write_data_to_flash_file\000"
.LASF6:
	.ascii	"FLASH_STORAGE_delete_all_files_with_this_name\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
