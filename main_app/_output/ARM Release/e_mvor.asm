	.file	"e_mvor.c"
	.text
.Ltext0:
	.section	.text.FDTO_MVOR_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_MVOR_return_to_menu, %function
FDTO_MVOR_return_to_menu:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L2
	ldr	r1, .L2+4
	b	FDTO_GROUP_return_to_menu
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	MVOR_extract_and_store_changes_from_GuiVars
.LFE3:
	.size	FDTO_MVOR_return_to_menu, .-FDTO_MVOR_return_to_menu
	.section	.text.MVOR_process_start_time.isra.0,"ax",%progbits
	.align	2
	.type	MVOR_process_start_time.isra.0, %function
MVOR_process_start_time.isra.0:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI0:
	mov	r3, #1
	mov	r4, r2
	mov	r2, #10
	stmia	sp, {r2, r3}
	mov	r2, #0
	mov	r3, #1440
	mov	r5, r1
	bl	process_uns32
	ldr	r3, [r5, #0]
	mov	r0, #0
	subs	r3, r3, #1440
	movne	r3, #1
	str	r3, [r4, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	Redraw_Screen
.LFE6:
	.size	MVOR_process_start_time.isra.0, .-MVOR_process_start_time.isra.0
	.section	.text.MVOR_process_group,"ax",%progbits
	.align	2
	.type	MVOR_process_group, %function
MVOR_process_group:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #4
	stmfd	sp!, {r0, r1, r4, r5, lr}
.LCFI1:
	mov	r4, r0
	beq	.L11
	bhi	.L15
	cmp	r0, #1
	beq	.L8
	bcc	.L7
	cmp	r0, #2
	beq	.L9
	cmp	r0, #3
	bne	.L6
	b	.L59
.L15:
	cmp	r0, #67
	beq	.L13
	bhi	.L16
	cmp	r0, #16
	beq	.L12
	cmp	r0, #20
	bne	.L6
	b	.L12
.L16:
	cmp	r0, #80
	beq	.L14
	cmp	r0, #84
	bne	.L6
	b	.L14
.L9:
	ldr	r5, .L60+4
	ldrsh	r3, [r5, #0]
	sub	r3, r3, #1
	cmp	r3, #2
	bhi	.L46
	bl	good_key_beep
	ldrsh	r2, [r5, #0]
	ldr	r3, .L60+8
	cmp	r2, #2
	moveq	r2, #1
	beq	.L55
	cmp	r2, #3
	streq	r4, [r3, #216]
	beq	.L20
	mov	r2, #0
.L55:
	str	r2, [r3, #216]
.L20:
	ldr	r2, .L60+12
	flds	s15, .L60
	flds	s14, [r2, #0]
	mov	r2, #0
	str	r2, [r3, #224]
	ldr	r2, .L60+16
	fmuls	s14, s14, s15
	ldr	r2, [r2, #0]
	str	r2, [r3, #228]
	mov	r2, #1
	str	r2, [r3, #212]
	fmuls	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r3, #220]	@ int
	b	.L5
.L14:
	ldr	r3, .L60+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #17
	ldrls	pc, [pc, r3, asl #2]
	b	.L23
.L39:
	.word	.L24
	.word	.L23
	.word	.L23
	.word	.L23
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
	.word	.L38
.L24:
	ldr	r2, .L60+20
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L60+12
	ldr	r3, .L60+24
	str	r2, [sp, #0]	@ float
	bl	process_fl
	b	.L40
.L25:
	mov	r0, r4
	ldr	r1, .L60+28
	ldr	r2, .L60+32
	b	.L56
.L27:
	ldr	r1, .L60+36
	ldr	r2, .L60+40
	mov	r0, r4
.L56:
	bl	MVOR_process_start_time.isra.0
	b	.L40
.L29:
	mov	r0, r4
	ldr	r1, .L60+44
	ldr	r2, .L60+48
	b	.L56
.L31:
	mov	r0, r4
	ldr	r1, .L60+52
	ldr	r2, .L60+56
	b	.L56
.L33:
	mov	r0, r4
	ldr	r1, .L60+60
	ldr	r2, .L60+64
	b	.L56
.L35:
	mov	r0, r4
	ldr	r1, .L60+68
	ldr	r2, .L60+72
	b	.L56
.L37:
	mov	r0, r4
	ldr	r1, .L60+76
	ldr	r2, .L60+80
	b	.L56
.L26:
	mov	r0, r4
	ldr	r1, .L60+84
	ldr	r2, .L60+88
	b	.L56
.L28:
	mov	r0, r4
	ldr	r1, .L60+92
	ldr	r2, .L60+96
	b	.L56
.L30:
	mov	r0, r4
	ldr	r1, .L60+100
	ldr	r2, .L60+104
	b	.L56
.L32:
	mov	r0, r4
	ldr	r1, .L60+108
	ldr	r2, .L60+112
	b	.L56
.L34:
	mov	r0, r4
	ldr	r1, .L60+116
	ldr	r2, .L60+120
	b	.L56
.L36:
	mov	r0, r4
	ldr	r1, .L60+124
	ldr	r2, .L60+128
	b	.L56
.L38:
	mov	r0, r4
	ldr	r1, .L60+132
	ldr	r2, .L60+136
	b	.L56
.L23:
	bl	bad_key_beep
.L40:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	Refresh_Screen
.L12:
	bl	SYSTEM_num_systems_in_use
	ldr	r3, .L60+140
	ldr	r2, .L60+144
	str	r3, [sp, #0]
	ldr	r3, .L60+148
	mov	r1, r0
	mov	r0, r4
	bl	GROUP_process_NEXT_and_PREV
	b	.L5
.L11:
	ldr	r3, .L60+4
	ldrh	r2, [r3, #0]
	cmp	r2, #17
	bhi	.L46
	mov	r2, r2, asl #16
	mov	r2, r2, asr #16
	mov	r1, #1
	ldr	r0, .L60+152
	mov	r3, r1, asl r2
	and	r0, r3, r0
	cmp	r0, #0
	subne	r0, r2, #2
	bne	.L57
	ands	r0, r3, #48
	bne	.L43
	tst	r3, #6
	ldrne	r3, .L60+156
	strne	r2, [r3, #0]
	bne	.L57
	b	.L46
.L43:
	ldr	r3, .L60+160
	ldr	r3, [r3, #0]
	cmp	r3, #0
	moveq	r0, r1
	moveq	r1, r0
	movne	r0, #3
.L57:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Select
.L7:
	ldr	r3, .L60+4
	ldrsh	r0, [r3, #0]
	cmp	r0, #15
	ldrls	pc, [pc, r0, asl #2]
	b	.L46
.L50:
	.word	.L47
	.word	.L48
	.word	.L48
	.word	.L48
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
	.word	.L49
.L47:
	ldr	r3, .L60+156
	ldr	r2, [r3, #0]
	sub	r2, r2, #1
	cmp	r2, #1
	movhi	r2, #1
	strhi	r2, [r3, #0]
	ldr	r0, [r3, #0]
	b	.L58
.L48:
	ldr	r3, .L60+156
	str	r0, [r3, #0]
	mov	r0, #4
	b	.L58
.L49:
	add	r0, r0, #2
.L58:
	mov	r1, #1
	b	.L57
.L46:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	bad_key_beep
.L8:
	ldr	r3, .L60+4
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L13
.L54:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Up
.L59:
	mov	r0, #1
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	CURSOR_Down
.L13:
	ldr	r0, .L60+164
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	KEY_process_BACK_from_editing_screen
.L61:
	.align	2
.L60:
	.word	1114636288
	.word	GuiLib_ActiveCursorFieldNo
	.word	irri_comm
	.word	GuiVar_MVORRunTime
	.word	g_GROUP_ID
	.word	1036831949
	.word	1111490560
	.word	GuiVar_MVOROpen0
	.word	GuiVar_MVOROpen0Enabled
	.word	GuiVar_MVOROpen1
	.word	GuiVar_MVOROpen1Enabled
	.word	GuiVar_MVOROpen2
	.word	GuiVar_MVOROpen2Enabled
	.word	GuiVar_MVOROpen3
	.word	GuiVar_MVOROpen3Enabled
	.word	GuiVar_MVOROpen4
	.word	GuiVar_MVOROpen4Enabled
	.word	GuiVar_MVOROpen5
	.word	GuiVar_MVOROpen5Enabled
	.word	GuiVar_MVOROpen6
	.word	GuiVar_MVOROpen6Enabled
	.word	GuiVar_MVORClose0
	.word	GuiVar_MVORClose0Enabled
	.word	GuiVar_MVORClose1
	.word	GuiVar_MVORClose1Enabled
	.word	GuiVar_MVORClose2
	.word	GuiVar_MVORClose2Enabled
	.word	GuiVar_MVORClose3
	.word	GuiVar_MVORClose3Enabled
	.word	GuiVar_MVORClose4
	.word	GuiVar_MVORClose4Enabled
	.word	GuiVar_MVORClose5
	.word	GuiVar_MVORClose5Enabled
	.word	GuiVar_MVORClose6
	.word	GuiVar_MVORClose6Enabled
	.word	MVOR_copy_group_into_guivars
	.word	MVOR_extract_and_store_changes_from_GuiVars
	.word	SYSTEM_get_group_at_this_index
	.word	262080
	.word	.LANCHOR1
	.word	GuiVar_MVORInEffect
	.word	FDTO_MVOR_return_to_menu
.L6:
	mov	r0, r4
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, lr}
	b	KEY_process_global_keys
.L5:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, pc}
.LFE2:
	.size	MVOR_process_group, .-MVOR_process_group
	.section	.text.FDTO_MVOR_update_screen,"ax",%progbits
	.align	2
	.global	FDTO_MVOR_update_screen
	.type	FDTO_MVOR_update_screen, %function
FDTO_MVOR_update_screen:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L66
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	ldr	r4, .L66+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r2, .L66+8
	mov	r3, #109
	ldr	r5, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L66+12
	ldr	r0, [r3, #0]
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	ldr	r3, .L66+16
	cmp	r0, #0
	beq	.L63
	ldrb	r2, [r0, #468]	@ zero_extendqisi2
	tst	r2, #96
	moveq	r2, #0
	movne	r2, #1
	str	r2, [r4, #0]
	ldrb	r2, [r0, #468]	@ zero_extendqisi2
	mov	r2, r2, lsr #6
	and	r2, r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L66+20
	flds	s15, [r3, #0]
	ldr	r3, .L66+24
	ldr	r3, [r0, r3]
	fmsr	s13, r3	@ int
	ldr	r3, .L66+28
	fuitos	s14, s13
	fdivs	s15, s14, s15
	fsts	s15, [r3, #0]
	b	.L64
.L63:
	str	r0, [r3, #0]
	ldr	r3, .L66+28
	mov	r2, #0
	str	r0, [r4, #0]
	str	r2, [r3, #0]	@ float
.L64:
	ldr	r3, .L66
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L66+4
	ldr	r3, [r3, #0]
	cmp	r5, r3
	beq	.L65
	mov	r0, #0
	ldmfd	sp!, {r4, r5, lr}
	b	FDTO_Redraw_Screen
.L65:
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L67:
	.align	2
.L66:
	.word	system_preserves_recursive_MUTEX
	.word	GuiVar_MVORInEffect
	.word	.LC0
	.word	g_GROUP_ID
	.word	GuiVar_MVORState
	.word	.LANCHOR2
	.word	14124
	.word	GuiVar_MVORTimeRemaining
.LFE1:
	.size	FDTO_MVOR_update_screen, .-FDTO_MVOR_update_screen
	.section	.text.FDTO_MVOR_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_MVOR_draw_menu
	.type	FDTO_MVOR_draw_menu, %function
FDTO_MVOR_draw_menu:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI3:
	ldr	r4, .L69
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L69+4
	ldr	r3, .L69+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	ldr	r3, .L69+12
	ldr	r1, .L69+16
	str	r3, [sp, #0]
	ldr	r3, .L69+20
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r3, #34
	mov	r2, r0
	mov	r0, r5
	bl	FDTO_GROUP_draw_menu
	ldr	r0, [r4, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L70:
	.align	2
.L69:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	382
	.word	SYSTEM_load_system_name_into_scroll_box_guivar
	.word	.LANCHOR0
	.word	MVOR_copy_group_into_guivars
.LFE4:
	.size	FDTO_MVOR_draw_menu, .-FDTO_MVOR_draw_menu
	.section	.text.MVOR_process_menu,"ax",%progbits
	.align	2
	.global	MVOR_process_menu
	.type	MVOR_process_menu, %function
MVOR_process_menu:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r6, r7, lr}
.LCFI4:
	ldr	r4, .L72
	mov	r6, r0
	mov	r7, r1
	ldr	r2, .L72+4
	mov	r1, #400
	mov	r3, #392
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	ldr	r2, .L72+8
	mov	r1, r7
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	ldr	r2, .L72+12
	str	r2, [sp, #8]
	ldr	r2, .L72+16
	str	r2, [sp, #12]
	ldr	r2, .L72+20
	mov	r3, r0
	mov	r0, r6
	bl	GROUP_process_menu
	ldr	r0, [r4, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L73:
	.align	2
.L72:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	MVOR_process_group
	.word	SYSTEM_get_group_at_this_index
	.word	MVOR_copy_group_into_guivars
	.word	.LANCHOR0
.LFE5:
	.size	MVOR_process_menu, .-MVOR_process_menu
	.section	.bss.g_MVOR_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_MVOR_editing_group, %object
	.size	g_MVOR_editing_group, 4
g_MVOR_editing_group:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_mvor.c\000"
	.section	.bss.g_MVOR_prev_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_MVOR_prev_cursor_pos, %object
	.size	g_MVOR_prev_cursor_pos, 4
g_MVOR_prev_cursor_pos:
	.space	4
	.section	.data.SECONDS_PER_HOUR.7786,"aw",%progbits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	SECONDS_PER_HOUR.7786, %object
	.size	SECONDS_PER_HOUR.7786, 4
SECONDS_PER_HOUR.7786:
	.word	1163984896
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI0-.LFB6
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_mvor.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x9b
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF7
	.byte	0x1
	.byte	0x46
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x176
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF1
	.byte	0x1
	.byte	0x93
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x59
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x17c
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x186
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB6
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF7:
	.ascii	"MVOR_process_start_time\000"
.LASF2:
	.ascii	"FDTO_MVOR_update_screen\000"
.LASF3:
	.ascii	"FDTO_MVOR_draw_menu\000"
.LASF4:
	.ascii	"MVOR_process_menu\000"
.LASF0:
	.ascii	"FDTO_MVOR_return_to_menu\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_mvor.c\000"
.LASF1:
	.ascii	"MVOR_process_group\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
