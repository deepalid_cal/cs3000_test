	.file	"weather_control.c"
	.text
.Ltext0:
	.section	.text.nm_WEATHER_set_wind_gage_box_index,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_wind_gage_box_index, %function
nm_WEATHER_set_wind_gage_box_index:
.LFB11:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI0:
	sub	sp, sp, #40
.LCFI1:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	lr, r0
	str	r3, [sp, #20]
	ldr	r0, .L2
	ldr	r3, [sp, #48]
	str	r1, [sp, #4]
	str	r2, [sp, #12]
	ldr	r1, .L2+4
	ldr	r2, .L2+8
	mov	ip, #0
	str	r3, [sp, #24]
	add	r3, r0, #316
	str	r1, [sp, #8]
	str	r3, [sp, #28]
	str	r2, [sp, #36]
	mov	r3, #11
	mov	r1, lr
	mov	r2, ip
	add	r0, r0, #44
	str	ip, [sp, #0]
	str	r3, [sp, #32]
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	61450
	.word	.LC0
.LFE11:
	.size	nm_WEATHER_set_wind_gage_box_index, .-nm_WEATHER_set_wind_gage_box_index
	.section	.text.nm_WEATHER_set_rain_bucket_max_24_hour,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_rain_bucket_max_24_hour, %function
nm_WEATHER_set_rain_bucket_max_24_hour:
.LFB5:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI2:
	sub	sp, sp, #40
.LCFI3:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	ip, r0
	str	r3, [sp, #20]
	ldr	r0, .L5
	ldr	r3, [sp, #48]
	str	r1, [sp, #4]
	str	r3, [sp, #24]
	add	r3, r0, #316
	str	r3, [sp, #28]
	mov	r3, #5
	ldr	r1, .L5+4
	str	r3, [sp, #32]
	ldr	r3, .L5+8
	mov	lr, #60
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	mov	r1, ip
	mov	r2, #1
	mov	r3, #600
	add	r0, r0, #20
	str	lr, [sp, #0]
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L6:
	.align	2
.L5:
	.word	.LANCHOR0
	.word	61444
	.word	.LC1
.LFE5:
	.size	nm_WEATHER_set_rain_bucket_max_24_hour, .-nm_WEATHER_set_rain_bucket_max_24_hour
	.section	.text.nm_WEATHER_set_rain_bucket_max_hourly,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_rain_bucket_max_hourly, %function
nm_WEATHER_set_rain_bucket_max_hourly:
.LFB4:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI4:
	sub	sp, sp, #40
.LCFI5:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	ip, r0
	str	r3, [sp, #20]
	ldr	r0, .L8
	ldr	r3, [sp, #48]
	str	r1, [sp, #4]
	str	r3, [sp, #24]
	add	r3, r0, #316
	str	r3, [sp, #28]
	mov	r3, #4
	ldr	r1, .L8+4
	str	r3, [sp, #32]
	ldr	r3, .L8+8
	mov	lr, #20
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	mov	r1, ip
	mov	r2, #1
	mov	r3, #200
	add	r0, r0, #16
	str	lr, [sp, #0]
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L9:
	.align	2
.L8:
	.word	.LANCHOR0
	.word	61443
	.word	.LC2
.LFE4:
	.size	nm_WEATHER_set_rain_bucket_max_hourly, .-nm_WEATHER_set_rain_bucket_max_hourly
	.section	.text.nm_WEATHER_set_rain_bucket_minimum_inches,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_rain_bucket_minimum_inches, %function
nm_WEATHER_set_rain_bucket_minimum_inches:
.LFB3:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI6:
	sub	sp, sp, #40
.LCFI7:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	ip, r0
	str	r3, [sp, #20]
	ldr	r0, .L11
	ldr	r3, [sp, #48]
	str	r1, [sp, #4]
	str	r3, [sp, #24]
	add	r3, r0, #316
	str	r3, [sp, #28]
	mov	r3, #3
	ldr	r1, .L11+4
	str	r3, [sp, #32]
	ldr	r3, .L11+8
	mov	lr, #10
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	mov	r1, ip
	mov	r2, #1
	mov	r3, #200
	add	r0, r0, #12
	str	lr, [sp, #0]
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L12:
	.align	2
.L11:
	.word	.LANCHOR0
	.word	61442
	.word	.LC3
.LFE3:
	.size	nm_WEATHER_set_rain_bucket_minimum_inches, .-nm_WEATHER_set_rain_bucket_minimum_inches
	.section	.text.nm_WEATHER_set_rain_bucket_box_index,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_rain_bucket_box_index, %function
nm_WEATHER_set_rain_bucket_box_index:
.LFB1:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI8:
	sub	sp, sp, #40
.LCFI9:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	lr, r0
	str	r3, [sp, #20]
	ldr	r0, .L14
	ldr	r3, [sp, #48]
	mov	ip, #0
	str	r3, [sp, #24]
	add	r3, r0, #316
	str	r3, [sp, #28]
	mov	r3, #1
	str	r3, [sp, #32]
	ldr	r3, .L14+4
	str	r1, [sp, #4]
	mov	r1, #61440
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	mov	r1, lr
	mov	r2, ip
	mov	r3, #11
	add	r0, r0, #4
	str	ip, [sp, #0]
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L15:
	.align	2
.L14:
	.word	.LANCHOR0
	.word	.LC4
.LFE1:
	.size	nm_WEATHER_set_rain_bucket_box_index, .-nm_WEATHER_set_rain_bucket_box_index
	.section	.text.nm_WEATHER_set_et_gage_box_index,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_et_gage_box_index, %function
nm_WEATHER_set_et_gage_box_index:
.LFB6:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI10:
	sub	sp, sp, #40
.LCFI11:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	lr, r0
	str	r3, [sp, #20]
	ldr	r0, .L17
	ldr	r3, [sp, #48]
	str	r1, [sp, #4]
	str	r3, [sp, #24]
	add	r3, r0, #316
	str	r3, [sp, #28]
	mov	r3, #6
	ldr	r1, .L17+4
	str	r3, [sp, #32]
	ldr	r3, .L17+8
	mov	ip, #0
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	mov	r1, lr
	mov	r2, ip
	mov	r3, #11
	add	r0, r0, #24
	str	ip, [sp, #0]
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L18:
	.align	2
.L17:
	.word	.LANCHOR0
	.word	61445
	.word	.LC5
.LFE6:
	.size	nm_WEATHER_set_et_gage_box_index, .-nm_WEATHER_set_et_gage_box_index
	.section	.text.nm_WEATHER_set_percent_of_historical_et_cap,"ax",%progbits
	.align	2
	.global	nm_WEATHER_set_percent_of_historical_et_cap
	.type	nm_WEATHER_set_percent_of_historical_et_cap, %function
nm_WEATHER_set_percent_of_historical_et_cap:
.LFB9:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI12:
	sub	sp, sp, #40
.LCFI13:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	ip, r0
	str	r3, [sp, #20]
	ldr	r0, .L20
	ldr	r3, [sp, #48]
	str	r1, [sp, #4]
	str	r3, [sp, #24]
	add	r3, r0, #316
	str	r3, [sp, #28]
	mov	r3, #9
	ldr	r1, .L20+4
	str	r3, [sp, #32]
	ldr	r3, .L20+8
	mov	lr, #150
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	mov	r1, ip
	mov	r2, #10
	mov	r3, #500
	add	r0, r0, #36
	str	lr, [sp, #0]
	bl	SHARED_set_uint32_controller
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L21:
	.align	2
.L20:
	.word	.LANCHOR0
	.word	61449
	.word	.LC6
.LFE9:
	.size	nm_WEATHER_set_percent_of_historical_et_cap, .-nm_WEATHER_set_percent_of_historical_et_cap
	.section	.text.nm_WEATHER_set_reference_et_state_index,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_reference_et_state_index, %function
nm_WEATHER_set_reference_et_state_index:
.LFB17:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI14:
	sub	sp, sp, #40
.LCFI15:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	str	r1, [sp, #4]
	str	r3, [sp, #20]
	ldr	r3, [sp, #48]
	ldr	r1, .L24
	str	r3, [sp, #24]
	ldr	r3, .L24+4
	mov	lr, r0
	str	r3, [sp, #28]
	mov	r3, #19
	str	r3, [sp, #32]
	ldr	r3, .L24+8
	mov	ip, #0
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	ldr	r0, .L24+12
	mov	r1, lr
	mov	r2, ip
	mov	r3, #12
	str	ip, [sp, #0]
	bl	SHARED_set_uint32_controller
	cmp	r0, #1
	bne	.L22
	add	sp, sp, #40
	ldr	lr, [sp], #4
	b	WEATHER_TABLES_cause_et_table_historical_values_to_be_updated
.L22:
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L25:
	.align	2
.L24:
	.word	61467
	.word	.LANCHOR0+316
	.word	.LC7
	.word	.LANCHOR0+120
.LFE17:
	.size	nm_WEATHER_set_reference_et_state_index, .-nm_WEATHER_set_reference_et_state_index
	.section	.text.nm_WEATHER_set_reference_et_county_index,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_reference_et_county_index, %function
nm_WEATHER_set_reference_et_county_index:
.LFB18:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI16:
	sub	sp, sp, #40
.LCFI17:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	ip, r0
	str	r3, [sp, #20]
	ldr	r3, [sp, #48]
	mov	r0, #2
	str	r3, [sp, #24]
	ldr	r3, .L28
	stmia	sp, {r0, r1}
	ldr	r1, .L28+4
	str	r3, [sp, #28]
	mov	r3, #20
	str	r3, [sp, #32]
	ldr	r3, .L28+8
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	ldr	r0, .L28+12
	mov	r1, ip
	mov	r2, #0
	mov	r3, #64
	bl	SHARED_set_uint32_controller
	cmp	r0, #1
	bne	.L26
	add	sp, sp, #40
	ldr	lr, [sp], #4
	b	WEATHER_TABLES_cause_et_table_historical_values_to_be_updated
.L26:
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L29:
	.align	2
.L28:
	.word	.LANCHOR0+316
	.word	61468
	.word	.LC8
	.word	.LANCHOR0+124
.LFE18:
	.size	nm_WEATHER_set_reference_et_county_index, .-nm_WEATHER_set_reference_et_county_index
	.section	.text.nm_WEATHER_set_reference_et_city_index,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_reference_et_city_index, %function
nm_WEATHER_set_reference_et_city_index:
.LFB19:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI18:
	sub	sp, sp, #40
.LCFI19:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	ip, r0
	str	r3, [sp, #20]
	ldr	r3, [sp, #48]
	mov	r0, #13
	str	r3, [sp, #24]
	ldr	r3, .L32
	stmia	sp, {r0, r1}
	ldr	r1, .L32+4
	str	r3, [sp, #28]
	mov	r3, #21
	str	r3, [sp, #32]
	ldr	r3, .L32+8
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	ldr	r0, .L32+12
	mov	r1, ip
	mov	r2, #0
	mov	r3, #153
	bl	SHARED_set_uint32_controller
	cmp	r0, #1
	bne	.L30
	add	sp, sp, #40
	ldr	lr, [sp], #4
	b	WEATHER_TABLES_cause_et_table_historical_values_to_be_updated
.L30:
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L33:
	.align	2
.L32:
	.word	.LANCHOR0+316
	.word	61469
	.word	.LC9
	.word	.LANCHOR0+128
.LFE19:
	.size	nm_WEATHER_set_reference_et_city_index, .-nm_WEATHER_set_reference_et_city_index
	.section	.text.nm_WEATHER_set_reference_et_use_your_own,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_reference_et_use_your_own, %function
nm_WEATHER_set_reference_et_use_your_own:
.LFB15:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI20:
	mov	ip, r1
	ldr	r1, .L36
	mov	lr, r0
	stmia	sp, {r1, r2, r3}
	ldr	r3, [sp, #36]
	ldr	r0, .L36+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #40]
	mov	r1, lr
	str	r3, [sp, #16]
	ldr	r3, .L36+8
	mov	r2, #0
	str	r3, [sp, #20]
	mov	r3, #17
	str	r3, [sp, #24]
	ldr	r3, .L36+12
	str	r3, [sp, #28]
	mov	r3, ip
	bl	SHARED_set_bool_controller
	cmp	r0, #1
	bne	.L34
	add	sp, sp, #32
	ldr	lr, [sp], #4
	b	WEATHER_TABLES_cause_et_table_historical_values_to_be_updated
.L34:
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L37:
	.align	2
.L36:
	.word	61454
	.word	.LANCHOR0+68
	.word	.LANCHOR0+316
	.word	.LC10
.LFE15:
	.size	nm_WEATHER_set_reference_et_use_your_own, .-nm_WEATHER_set_reference_et_use_your_own
	.section	.text.nm_WEATHER_set_freeze_switch_in_use,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_freeze_switch_in_use, %function
nm_WEATHER_set_freeze_switch_in_use:
.LFB25:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI21:
	mov	ip, r1
	ldr	r1, .L39
	mov	lr, r0
	stmia	sp, {r1, r2, r3}
	ldr	r3, [sp, #36]
	ldr	r0, .L39+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #40]
	mov	r1, lr
	str	r3, [sp, #16]
	add	r3, r0, #316
	str	r3, [sp, #20]
	mov	r3, #27
	str	r3, [sp, #24]
	ldr	r3, .L39+8
	mov	r2, #0
	str	r3, [sp, #28]
	add	r0, r0, #256
	mov	r3, ip
	bl	SHARED_set_bool_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L40:
	.align	2
.L39:
	.word	61481
	.word	.LANCHOR0
	.word	.LC11
.LFE25:
	.size	nm_WEATHER_set_freeze_switch_in_use, .-nm_WEATHER_set_freeze_switch_in_use
	.section	.text.nm_WEATHER_set_rain_switch_in_use,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_rain_switch_in_use, %function
nm_WEATHER_set_rain_switch_in_use:
.LFB23:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI22:
	mov	ip, r1
	ldr	r1, .L42
	mov	lr, r0
	stmia	sp, {r1, r2, r3}
	ldr	r3, [sp, #36]
	ldr	r0, .L42+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #40]
	mov	r1, lr
	str	r3, [sp, #16]
	add	r3, r0, #316
	str	r3, [sp, #20]
	mov	r3, #25
	str	r3, [sp, #24]
	ldr	r3, .L42+8
	mov	r2, #0
	str	r3, [sp, #28]
	add	r0, r0, #204
	mov	r3, ip
	bl	SHARED_set_bool_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L43:
	.align	2
.L42:
	.word	61475
	.word	.LANCHOR0
	.word	.LC12
.LFE23:
	.size	nm_WEATHER_set_rain_switch_in_use, .-nm_WEATHER_set_rain_switch_in_use
	.section	.text.nm_WEATHER_set_wind_gage_connected,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_wind_gage_connected, %function
nm_WEATHER_set_wind_gage_connected:
.LFB12:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI23:
	mov	ip, r1
	ldr	r1, .L45
	mov	lr, r0
	stmia	sp, {r1, r2, r3}
	ldr	r3, [sp, #36]
	ldr	r0, .L45+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #40]
	mov	r1, lr
	str	r3, [sp, #16]
	add	r3, r0, #316
	str	r3, [sp, #20]
	mov	r3, #12
	str	r3, [sp, #24]
	ldr	r3, .L45+8
	mov	r2, #0
	str	r3, [sp, #28]
	add	r0, r0, #48
	mov	r3, ip
	bl	SHARED_set_bool_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L46:
	.align	2
.L45:
	.word	61451
	.word	.LANCHOR0
	.word	.LC13
.LFE12:
	.size	nm_WEATHER_set_wind_gage_connected, .-nm_WEATHER_set_wind_gage_connected
	.section	.text.nm_WEATHER_set_rain_bucket_connected,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_rain_bucket_connected, %function
nm_WEATHER_set_rain_bucket_connected:
.LFB2:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI24:
	mov	ip, r1
	ldr	r1, .L48
	mov	lr, r0
	stmia	sp, {r1, r2, r3}
	ldr	r3, [sp, #36]
	ldr	r0, .L48+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #40]
	mov	r1, lr
	str	r3, [sp, #16]
	add	r3, r0, #316
	str	r3, [sp, #20]
	mov	r3, #2
	str	r3, [sp, #24]
	ldr	r3, .L48+8
	mov	r2, #0
	str	r3, [sp, #28]
	add	r0, r0, #8
	mov	r3, ip
	bl	SHARED_set_bool_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L49:
	.align	2
.L48:
	.word	61441
	.word	.LANCHOR0
	.word	.LC14
.LFE2:
	.size	nm_WEATHER_set_rain_bucket_connected, .-nm_WEATHER_set_rain_bucket_connected
	.section	.text.nm_WEATHER_set_et_gage_log_pulses,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_et_gage_log_pulses, %function
nm_WEATHER_set_et_gage_log_pulses:
.LFB8:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI25:
	mov	ip, r1
	ldr	r1, .L51
	mov	lr, r0
	stmia	sp, {r1, r2, r3}
	ldr	r3, [sp, #36]
	ldr	r0, .L51+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #40]
	mov	r1, lr
	str	r3, [sp, #16]
	add	r3, r0, #316
	str	r3, [sp, #20]
	mov	r3, #8
	str	r3, [sp, #24]
	ldr	r3, .L51+8
	mov	r2, #0
	str	r3, [sp, #28]
	add	r0, r0, #32
	mov	r3, ip
	bl	SHARED_set_bool_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L52:
	.align	2
.L51:
	.word	61447
	.word	.LANCHOR0
	.word	.LC15
.LFE8:
	.size	nm_WEATHER_set_et_gage_log_pulses, .-nm_WEATHER_set_et_gage_log_pulses
	.section	.text.nm_WEATHER_set_et_gage_connected,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_et_gage_connected, %function
nm_WEATHER_set_et_gage_connected:
.LFB7:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI26:
	mov	ip, r1
	ldr	r1, .L54
	mov	lr, r0
	stmia	sp, {r1, r2, r3}
	ldr	r3, [sp, #36]
	ldr	r0, .L54+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #40]
	mov	r1, lr
	str	r3, [sp, #16]
	add	r3, r0, #316
	str	r3, [sp, #20]
	mov	r3, #7
	str	r3, [sp, #24]
	ldr	r3, .L54+8
	mov	r2, #0
	str	r3, [sp, #28]
	add	r0, r0, #28
	mov	r3, ip
	bl	SHARED_set_bool_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L55:
	.align	2
.L54:
	.word	61446
	.word	.LANCHOR0
	.word	.LC16
.LFE7:
	.size	nm_WEATHER_set_et_gage_connected, .-nm_WEATHER_set_et_gage_connected
	.section	.text.nm_WEATHER_set_user_defined_state,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_user_defined_state, %function
nm_WEATHER_set_user_defined_state:
.LFB20:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI27:
	mov	ip, r1
	ldr	r1, .L57
	mov	lr, r0
	stmia	sp, {r1, r2, r3}
	ldr	r3, [sp, #36]
	ldr	r0, .L57+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #40]
	mov	r1, #24
	str	r3, [sp, #16]
	add	r3, r0, #316
	str	r3, [sp, #20]
	mov	r3, #22
	str	r3, [sp, #24]
	ldr	r3, .L57+8
	mov	r2, lr
	str	r3, [sp, #28]
	add	r0, r0, #132
	mov	r3, ip
	bl	SHARED_set_string_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L58:
	.align	2
.L57:
	.word	61470
	.word	.LANCHOR0
	.word	.LC17
.LFE20:
	.size	nm_WEATHER_set_user_defined_state, .-nm_WEATHER_set_user_defined_state
	.section	.text.nm_WEATHER_set_user_defined_county,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_user_defined_county, %function
nm_WEATHER_set_user_defined_county:
.LFB21:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI28:
	mov	ip, r1
	ldr	r1, .L60
	mov	lr, r0
	stmia	sp, {r1, r2, r3}
	ldr	r3, [sp, #36]
	ldr	r0, .L60+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #40]
	mov	r1, #24
	str	r3, [sp, #16]
	add	r3, r0, #316
	str	r3, [sp, #20]
	mov	r3, #23
	str	r3, [sp, #24]
	ldr	r3, .L60+8
	mov	r2, lr
	str	r3, [sp, #28]
	add	r0, r0, #156
	mov	r3, ip
	bl	SHARED_set_string_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L61:
	.align	2
.L60:
	.word	61471
	.word	.LANCHOR0
	.word	.LC18
.LFE21:
	.size	nm_WEATHER_set_user_defined_county, .-nm_WEATHER_set_user_defined_county
	.section	.text.nm_WEATHER_set_user_defined_city,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_user_defined_city, %function
nm_WEATHER_set_user_defined_city:
.LFB22:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, lr}
.LCFI29:
	mov	ip, r1
	ldr	r1, .L63
	mov	lr, r0
	stmia	sp, {r1, r2, r3}
	ldr	r3, [sp, #36]
	ldr	r0, .L63+4
	str	r3, [sp, #12]
	ldr	r3, [sp, #40]
	mov	r1, #24
	str	r3, [sp, #16]
	add	r3, r0, #316
	str	r3, [sp, #20]
	ldr	r3, .L63+8
	mov	r2, lr
	str	r3, [sp, #28]
	add	r0, r0, #180
	mov	r3, ip
	str	r1, [sp, #24]
	bl	SHARED_set_string_controller
	add	sp, sp, #32
	ldmfd	sp!, {pc}
.L64:
	.align	2
.L63:
	.word	61472
	.word	.LANCHOR0
	.word	.LC19
.LFE22:
	.size	nm_WEATHER_set_user_defined_city, .-nm_WEATHER_set_user_defined_city
	.section	.text.WEATHER_set_wind_pause_speed,"ax",%progbits
	.align	2
	.type	WEATHER_set_wind_pause_speed, %function
WEATHER_set_wind_pause_speed:
.LFB13:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI30:
	sub	sp, sp, #40
.LCFI31:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	ip, r0
	str	r3, [sp, #20]
	ldr	r3, [sp, #48]
	mov	r0, #15
	str	r3, [sp, #24]
	mov	r3, #13
	stmia	sp, {r0, r1}
	ldr	r1, .L67
	ldr	r0, .L67+4
	str	r3, [sp, #32]
	ldr	r3, .L67+8
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r0, [sp, #28]
	str	r3, [sp, #36]
	sub	r0, r0, #264
	mov	r1, ip
	mov	r2, #2
	mov	r3, #99
	bl	SHARED_set_uint32_controller
	cmp	r0, #1
	bne	.L65
	add	sp, sp, #40
	ldr	lr, [sp], #4
	b	TP_MICRO_COMM_resync_wind_settings
.L65:
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L68:
	.align	2
.L67:
	.word	61452
	.word	.LANCHOR0+316
	.word	.LC20
.LFE13:
	.size	WEATHER_set_wind_pause_speed, .-WEATHER_set_wind_pause_speed
	.section	.text.WEATHER_set_wind_resume_speed,"ax",%progbits
	.align	2
	.type	WEATHER_set_wind_resume_speed, %function
WEATHER_set_wind_resume_speed:
.LFB14:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI32:
	sub	sp, sp, #40
.LCFI33:
	str	r3, [sp, #16]
	ldr	r3, [sp, #44]
	mov	ip, r0
	str	r3, [sp, #20]
	ldr	r3, [sp, #48]
	mov	r0, #10
	str	r3, [sp, #24]
	ldr	r3, .L71
	stmia	sp, {r0, r1}
	ldr	r1, .L71+4
	str	r3, [sp, #28]
	mov	r3, #15
	str	r3, [sp, #32]
	ldr	r3, .L71+8
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r3, [sp, #36]
	ldr	r0, .L71+12
	mov	r1, ip
	mov	r2, #1
	mov	r3, #98
	bl	SHARED_set_uint32_controller
	cmp	r0, #1
	bne	.L69
	add	sp, sp, #40
	ldr	lr, [sp], #4
	b	TP_MICRO_COMM_resync_wind_settings
.L69:
	add	sp, sp, #40
	ldmfd	sp!, {pc}
.L72:
	.align	2
.L71:
	.word	.LANCHOR0+316
	.word	61453
	.word	.LC21
	.word	.LANCHOR0+60
.LFE14:
	.size	WEATHER_set_wind_resume_speed, .-WEATHER_set_wind_resume_speed
	.section	.text.nm_weather_control_updater,"ax",%progbits
	.align	2
	.type	nm_weather_control_updater, %function
nm_weather_control_updater:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #3
	stmfd	sp!, {r0, r1, r4, lr}
.LCFI34:
	mov	r4, r0
	bne	.L74
	ldr	r0, .L81
	mov	r1, r4
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	Alert_Message_va
.L74:
	ldr	r0, .L81+4
	mov	r1, #3
	mov	r2, r4
	bl	Alert_Message_va
	cmp	r4, #0
	ldreq	r3, .L81+8
	mvneq	r2, #0
	streq	r2, [r3, #316]
	beq	.L76
.L75:
	cmp	r4, #1
	bne	.L77
.L76:
	bl	FLOWSENSE_get_controller_index
	mov	r2, #1
	str	r2, [sp, #0]
	ldr	r2, .L81+12
	mov	r1, #0
	str	r2, [sp, #4]
	mov	r2, #11
	mov	r3, r0
	mov	r0, #150
	bl	nm_WEATHER_set_percent_of_historical_et_cap
	b	.L78
.L77:
	cmp	r4, #2
	bne	.L79
.L78:
	ldr	r3, .L81+8
	mov	r2, #0
	str	r2, [r3, #320]
	add	sp, sp, #8
	ldmfd	sp!, {r4, pc}
.L79:
	ldr	r0, .L81+16
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	Alert_Message
.L82:
	.align	2
.L81:
	.word	.LC22
	.word	.LC23
	.word	.LANCHOR0
	.word	.LANCHOR0+308
	.word	.LC24
.LFE28:
	.size	nm_weather_control_updater, .-nm_weather_control_updater
	.section	.text.nm_WEATHER_set_reference_et_number,"ax",%progbits
	.align	2
	.type	nm_WEATHER_set_reference_et_number, %function
nm_WEATHER_set_reference_et_number:
.LFB16:
	@ args = 12, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI35:
	sub	r6, r1, #1
	cmp	r6, #11
	sub	sp, sp, #72
.LCFI36:
	mov	sl, r0
	mov	r4, r1
	mov	r7, r2
	mov	r8, r3
	bhi	.L84
.LBB12:
	add	r5, sp, #40
	str	r1, [sp, #0]
	ldr	r2, .L86
	mov	r1, #32
	ldr	r3, .L86+4
	mov	r0, r5
	bl	snprintf
	ldr	r2, .L86+8
	ldr	r3, .L86+12
	add	r6, r2, r6, asl #2
	ldr	r2, [r6, #1624]
	add	r0, r4, #17
	stmia	sp, {r2, r7}
	ldr	r2, [sp, #100]
	add	r4, r4, #61440
	str	r2, [sp, #16]
	ldr	r2, [sp, #104]
	add	r0, r3, r0, asl #2
	str	r2, [sp, #20]
	ldr	r2, [sp, #108]
	add	r4, r4, #14
	str	r2, [sp, #24]
	add	r2, r3, #316
	str	r2, [sp, #28]
	mov	r2, #18
	str	r2, [sp, #32]
	mov	r1, sl
	mov	r2, #0
	mov	r3, #2000
	str	r4, [sp, #8]
	str	r8, [sp, #12]
	str	r5, [sp, #36]
	bl	SHARED_set_uint32_controller
	cmp	r0, #1
	bne	.L83
	bl	WEATHER_TABLES_cause_et_table_historical_values_to_be_updated
	b	.L83
.L84:
.LBE12:
	ldr	r0, .L86+16
	ldr	r1, .L86+20
	bl	Alert_index_out_of_range_with_filename
.L83:
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L87:
	.align	2
.L86:
	.word	.LC25
	.word	.LC26
	.word	ET_PredefinedData
	.word	.LANCHOR0
	.word	.LC27
	.word	1387
.LFE16:
	.size	nm_WEATHER_set_reference_et_number, .-nm_WEATHER_set_reference_et_number
	.section	.text.nm_WEATHER_set_freeze_switch_connected,"ax",%progbits
	.align	2
	.global	nm_WEATHER_set_freeze_switch_connected
	.type	nm_WEATHER_set_freeze_switch_connected, %function
nm_WEATHER_set_freeze_switch_connected:
.LFB26:
	@ args = 12, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #11
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI37:
	mov	r6, r0
	sub	sp, sp, #64
.LCFI38:
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	bhi	.L89
.LBB15:
	add	r5, sp, #32
	add	r3, r1, #1
	str	r3, [sp, #0]
	mov	r0, r5
	mov	r1, #32
	ldr	r2, .L91
	ldr	r3, .L91+4
	bl	snprintf
	ldr	r2, .L91+8
	ldr	r3, .L91+12
	stmia	sp, {r2, r7}
	ldr	r2, [sp, #88]
	add	r0, r4, #65
	str	r2, [sp, #8]
	ldr	r2, [sp, #92]
	add	r0, r3, r0, asl #2
	str	r2, [sp, #12]
	ldr	r2, [sp, #96]
	mov	r1, r6
	str	r2, [sp, #16]
	add	r2, r3, #316
	str	r2, [sp, #20]
	mov	r2, #28
	str	r2, [sp, #24]
	mov	r3, r8
	mov	r2, #0
	str	r5, [sp, #28]
	bl	SHARED_set_bool_controller
	b	.L88
.L89:
.LBE15:
	ldr	r0, .L91+16
	ldr	r1, .L91+20
	bl	Alert_index_out_of_range_with_filename
.L88:
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L92:
	.align	2
.L91:
	.word	.LC25
	.word	.LC28
	.word	61481
	.word	.LANCHOR0
	.word	.LC27
	.word	2046
.LFE26:
	.size	nm_WEATHER_set_freeze_switch_connected, .-nm_WEATHER_set_freeze_switch_connected
	.section	.text.nm_WEATHER_set_rain_switch_connected,"ax",%progbits
	.align	2
	.global	nm_WEATHER_set_rain_switch_connected
	.type	nm_WEATHER_set_rain_switch_connected, %function
nm_WEATHER_set_rain_switch_connected:
.LFB24:
	@ args = 12, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #11
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI39:
	mov	r6, r0
	sub	sp, sp, #64
.LCFI40:
	mov	r4, r1
	mov	r8, r2
	mov	r7, r3
	bhi	.L94
.LBB18:
	add	r5, sp, #32
	add	r3, r1, #1
	str	r3, [sp, #0]
	mov	r0, r5
	mov	r1, #32
	ldr	r2, .L96
	ldr	r3, .L96+4
	bl	snprintf
	ldr	r2, .L96+8
	ldr	r3, .L96+12
	stmia	sp, {r2, r7}
	ldr	r2, [sp, #88]
	add	r0, r4, #52
	str	r2, [sp, #8]
	ldr	r2, [sp, #92]
	add	r0, r3, r0, asl #2
	str	r2, [sp, #12]
	ldr	r2, [sp, #96]
	mov	r1, r6
	str	r2, [sp, #16]
	add	r2, r3, #316
	str	r2, [sp, #20]
	mov	r2, #26
	str	r2, [sp, #24]
	mov	r3, r8
	mov	r2, #0
	str	r5, [sp, #28]
	bl	SHARED_set_bool_controller
	b	.L93
.L94:
.LBE18:
	ldr	r0, .L96+16
	ldr	r1, .L96+20
	bl	Alert_index_out_of_range_with_filename
.L93:
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L97:
	.align	2
.L96:
	.word	.LC25
	.word	.LC29
	.word	61475
	.word	.LANCHOR0
	.word	.LC27
	.word	1921
.LFE24:
	.size	nm_WEATHER_set_rain_switch_connected, .-nm_WEATHER_set_rain_switch_connected
	.section	.text.nm_weather_control_set_default_values,"ax",%progbits
	.align	2
	.type	nm_weather_control_set_default_values, %function
nm_weather_control_set_default_values:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI41:
	ldr	r7, .L103
	sub	sp, sp, #40
.LCFI42:
	bl	FLOWSENSE_get_controller_index
	ldr	r4, .L103+4
	add	r6, r7, #308
	ldr	r1, [r4, #0]
	add	sl, r7, #316
.LBB19:
	mov	r8, #11
.LBE19:
	mov	r5, r0
	mov	r0, r6
	bl	SHARED_clear_all_32_bit_change_bits
	add	r0, r7, #312
	ldr	r1, [r4, #0]
	bl	SHARED_clear_all_32_bit_change_bits
	add	r0, r7, #320
	ldr	r1, [r4, #0]
	bl	SHARED_clear_all_32_bit_change_bits
	mov	r0, sl
	ldr	r1, [r4, #0]
	bl	SHARED_set_all_32_bit_change_bits
	mov	r4, #0
	mov	r0, r5
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_rain_bucket_box_index
	mov	r0, r4
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_rain_bucket_connected
	mov	r0, #10
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_rain_bucket_minimum_inches
	mov	r0, #20
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_rain_bucket_max_hourly
	mov	r0, #60
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_rain_bucket_max_24_hour
	mov	r0, r5
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_et_gage_box_index
	mov	r0, r4
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_et_gage_connected
	mov	r0, r4
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_et_gage_log_pulses
.LBB20:
	ldr	r3, .L103+8
	ldr	r1, .L103+12
	str	r3, [sp, #8]
	ldr	r3, .L103+16
	mov	r0, r7
	str	r3, [sp, #36]
	ldr	r2, .L103+20
	ldr	r3, .L103+24
	stmia	sp, {r1, r4}
	str	r8, [sp, #12]
	str	sl, [sp, #28]
	str	r5, [sp, #16]
	str	r4, [sp, #20]
	str	r6, [sp, #24]
	str	r4, [sp, #32]
	bl	SHARED_set_time_controller
.LBE20:
	mov	r2, r8
	mov	r0, #150
	mov	r1, r4
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_percent_of_historical_et_cap
.LBB21:
	ldr	r3, .L103+28
	str	sl, [sp, #28]
	str	r3, [sp, #8]
	ldr	r3, .L103+32
	mov	sl, #10
	str	r3, [sp, #36]
	add	r0, r7, #40
	mov	r1, r4
	mov	r2, r4
	mov	r3, #100
	str	r8, [sp, #12]
	str	sl, [sp, #32]
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	str	r5, [sp, #16]
	str	r4, [sp, #20]
	str	r6, [sp, #24]
	bl	SHARED_set_uint32_controller
.LBE21:
	mov	r2, r8
	mov	r0, r5
	mov	r1, r4
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_wind_gage_box_index
	mov	r2, r8
	mov	r0, r4
	mov	r1, r4
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_wind_gage_connected
	mov	r2, r8
	mov	r0, #15
	mov	r1, r4
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	WEATHER_set_wind_pause_speed
	mov	r0, sl
	mov	r2, r8
	mov	r1, r4
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	WEATHER_set_wind_resume_speed
	str	sl, [r7, #56]
	str	sl, [r7, #64]
	mov	r2, r8
	mov	r0, r4
	mov	r1, r4
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_reference_et_use_your_own
	ldr	r8, .L103+36
	mov	r7, #1
	mov	r9, r4
	mov	sl, r6
.L99:
	mov	r4, #0
	str	r5, [sp, #0]
	str	r9, [sp, #4]
	str	sl, [sp, #8]
	mov	r1, r7
	ldr	r0, [r8, #4]!
	mov	r2, r4
	mov	r3, #11
	add	r7, r7, #1
	bl	nm_WEATHER_set_reference_et_number
	cmp	r7, #13
	ldr	r6, .L103+40
	bne	.L99
	mov	r0, r4
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_reference_et_state_index
	mov	r0, #2
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_reference_et_county_index
	mov	r0, r7
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_reference_et_city_index
	ldr	r0, .L103+44
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_user_defined_state
	ldr	r0, .L103+48
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_user_defined_county
	ldr	r0, .L103+52
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_user_defined_city
	mov	r0, r4
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_rain_switch_in_use
	mov	r0, r4
	mov	r1, r4
	mov	r2, #11
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_freeze_switch_in_use
	mov	r7, r4
.L100:
	mov	r0, #0
	mov	r1, r4
	mov	r2, r0
	mov	r3, #11
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	bl	nm_WEATHER_set_rain_switch_connected
	mov	r0, #0
	mov	r1, r4
	mov	r2, r0
	mov	r3, #11
	add	r4, r4, #1
	stmia	sp, {r5, r7}
	str	r6, [sp, #8]
	bl	nm_WEATHER_set_freeze_switch_connected
	cmp	r4, #12
	bne	.L100
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L104:
	.align	2
.L103:
	.word	.LANCHOR0
	.word	weather_control_recursive_MUTEX
	.word	61448
	.word	72000
	.word	.LC30
	.word	61200
	.word	82800
	.word	61485
	.word	.LC31
	.word	ET_PredefinedData+1620
	.word	.LANCHOR0+308
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LANCHOR3
.LFE31:
	.size	nm_weather_control_set_default_values, .-nm_weather_control_set_default_values
	.section	.text.init_file_weather_control,"ax",%progbits
	.align	2
	.global	init_file_weather_control
	.type	init_file_weather_control, %function
init_file_weather_control:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI43:
	ldr	r1, .L106
	mov	r3, #324
	str	r3, [sp, #0]
	ldr	r3, .L106+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, #3
	str	r3, [sp, #4]
	ldr	r3, .L106+8
	str	r3, [sp, #8]
	ldr	r3, .L106+12
	str	r3, [sp, #12]
	mov	r3, #6
	str	r3, [sp, #16]
	ldr	r3, .L106+16
	bl	FLASH_FILE_find_or_create_variable_file
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.L107:
	.align	2
.L106:
	.word	.LANCHOR4
	.word	weather_control_recursive_MUTEX
	.word	nm_weather_control_updater
	.word	nm_weather_control_set_default_values
	.word	.LANCHOR0
.LFE29:
	.size	init_file_weather_control, .-init_file_weather_control
	.section	.text.save_file_weather_control,"ax",%progbits
	.align	2
	.global	save_file_weather_control
	.type	save_file_weather_control, %function
save_file_weather_control:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #324
	stmfd	sp!, {r0, r1, r2, lr}
.LCFI44:
	ldr	r1, .L109
	str	r3, [sp, #0]
	ldr	r3, .L109+4
	mov	r0, #1
	ldr	r3, [r3, #0]
	mov	r2, #3
	str	r3, [sp, #4]
	mov	r3, #6
	str	r3, [sp, #8]
	ldr	r3, .L109+8
	bl	FLASH_STORAGE_make_a_copy_and_write_data_to_flash_file
	ldmfd	sp!, {r1, r2, r3, pc}
.L110:
	.align	2
.L109:
	.word	.LANCHOR4
	.word	weather_control_recursive_MUTEX
	.word	.LANCHOR0
.LFE30:
	.size	save_file_weather_control, .-save_file_weather_control
	.section	.text.FDTO_WEATHER_update_real_time_weather_GuiVars,"ax",%progbits
	.align	2
	.global	FDTO_WEATHER_update_real_time_weather_GuiVars
	.type	FDTO_WEATHER_update_real_time_weather_GuiVars, %function
FDTO_WEATHER_update_real_time_weather_GuiVars:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L116+16
	ldr	r2, .L116+20
	ldr	r0, [r3, #0]
	stmfd	sp!, {r4, r5, lr}
.LCFI45:
	ldr	r3, .L116+24
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L116+28
	ldrh	r2, [r3, #38]
	cmp	r2, #1
	bne	.L112
	ldrh	r3, [r3, #36]
	fmsr	s13, r3	@ int
	ldr	r3, .L116+32
	fsitod	d7, s13
	fldd	d6, .L116
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
.L112:
	ldr	r5, .L116+28
	ldr	r3, .L116+36
	flds	s13, [r5, #52]	@ int
	ldr	r4, .L116+16
	fuitod	d7, s13
	fldd	d6, .L116+8
	ldr	r0, [r4, #0]
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L116+40
	ldr	r2, [r5, #84]
	ldr	r1, [r3, #0]
	cmp	r1, r2
	beq	.L113
	str	r2, [r3, #0]
	ldr	r3, .L116+44
	ldrsh	r3, [r3, #0]
	cmp	r3, #26
	bne	.L114
	bl	GuiLib_Cursor_Up
.L114:
	mov	r0, #0
	bl	FDTO_Redraw_Screen
	b	.L115
.L113:
	bl	GuiLib_Refresh
.L115:
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L117:
	.align	2
.L116:
	.word	0
	.word	1086556160
	.word	0
	.word	1079574528
	.word	weather_preserves_recursive_MUTEX
	.word	.LC32
	.word	869
	.word	weather_preserves
	.word	GuiVar_StatusETGageReading
	.word	GuiVar_StatusRainBucketReading
	.word	GuiVar_ETGageRunawayGage
	.word	GuiLib_ActiveCursorFieldNo
.LFE33:
	.size	FDTO_WEATHER_update_real_time_weather_GuiVars, .-FDTO_WEATHER_update_real_time_weather_GuiVars
	.section	.text.WEATHER_copy_et_gage_settings_into_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_copy_et_gage_settings_into_GuiVars
	.type	WEATHER_copy_et_gage_settings_into_GuiVars, %function
WEATHER_copy_et_gage_settings_into_GuiVars:
.LFB34:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L122+20
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI46:
	ldr	r7, .L122+24
	ldr	r0, [r3, #0]
	ldr	r2, .L122+28
	ldr	r3, .L122+32
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [r7, #28]
	ldr	r3, .L122+36
	ldr	r5, .L122+40
	str	r2, [r3, #0]
	ldr	r6, .L122+44
	ldr	r2, [r7, #32]
	ldr	r3, .L122+48
	mov	r4, #0
	str	r2, [r3, #0]
.L120:
	ldr	r3, [r7, #24]
	ldr	r2, [r5, #4]!
	cmp	r3, r2
	bne	.L119
	ldr	r0, .L122+52
	mov	r1, #3
	ldr	r2, .L122+56
	add	r3, r3, #65
	str	r4, [r6, #0]
	bl	snprintf
.L119:
	add	r4, r4, #1
	cmp	r4, #12
	bne	.L120
	ldr	r3, .L122+20
	ldr	r4, .L122+60
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L122+28
	ldr	r3, .L122+64
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L122+68
	flds	s15, .L122
	ldr	r2, .L122+72
	flds	s13, [r3, #92]	@ int
	ldr	r1, [r3, #80]
	ldr	r0, [r4, #0]
	fuitos	s14, s13
	fldd	d6, .L122+4
	fdivs	s14, s14, s15
	fcvtds	d7, s14
	fmuld	d7, d7, d6
	ftouizd	s13, d7
	fsts	s13, [r2, #0]	@ int
	ldr	r2, .L122+76
	str	r1, [r2, #0]
	ldrh	r2, [r3, #36]
	fmsr	s13, r2	@ int
	ldr	r2, .L122+80
	fsitod	d7, s13
	fldd	d6, .L122+12
	fdivd	d7, d7, d6
	fcvtsd	s14, d7
	fsts	s14, [r2, #0]
	ldr	r2, [r3, #84]
	ldr	r3, .L122+84
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L123:
	.align	2
.L122:
	.word	1149861888
	.word	0
	.word	1079574528
	.word	0
	.word	1086556160
	.word	weather_control_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC32
	.word	929
	.word	GuiVar_ETGageInUse
	.word	.LANCHOR5-4
	.word	.LANCHOR6
	.word	GuiVar_ETGageLogPulses
	.word	GuiVar_ETGageInstalledAt
	.word	.LC33
	.word	weather_preserves_recursive_MUTEX
	.word	949
	.word	weather_preserves
	.word	GuiVar_ETGagePercentFull
	.word	GuiVar_ETGageSkipTonight
	.word	GuiVar_StatusETGageReading
	.word	GuiVar_ETGageRunawayGage
.LFE34:
	.size	WEATHER_copy_et_gage_settings_into_GuiVars, .-WEATHER_copy_et_gage_settings_into_GuiVars
	.section	.text.WEATHER_copy_rain_bucket_settings_into_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_copy_rain_bucket_settings_into_GuiVars
	.type	WEATHER_copy_rain_bucket_settings_into_GuiVars, %function
WEATHER_copy_rain_bucket_settings_into_GuiVars:
.LFB35:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L128+8
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI47:
	ldr	r7, .L128+12
	ldr	r0, [r3, #0]
	ldr	r2, .L128+16
	ldr	r3, .L128+20
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	flds	s11, [r7, #12]	@ int
	fldd	d7, .L128
	ldr	r3, .L128+24
	fuitod	d6, s11
	ldr	r2, [r7, #8]
	flds	s11, [r7, #16]	@ int
	ldr	r5, .L128+28
	str	r2, [r3, #0]
	ldr	r3, .L128+32
	fdivd	d6, d6, d7
	ldr	r6, .L128+36
	mov	r4, #0
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	flds	s11, [r7, #20]	@ int
	ldr	r3, .L128+40
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	fuitod	d6, s11
	ldr	r3, .L128+44
	fdivd	d6, d6, d7
	fcvtsd	s12, d6
	fsts	s12, [r3, #0]
	ldr	r3, .L128+48
	ldr	r3, [r3, #52]
	fmsr	s11, r3	@ int
	ldr	r3, .L128+52
	fuitod	d6, s11
	fdivd	d7, d6, d7
	fcvtsd	s14, d7
	fsts	s14, [r3, #0]
.L126:
	ldr	r3, [r7, #4]
	ldr	r2, [r5, #4]!
	cmp	r3, r2
	bne	.L125
	ldr	r0, .L128+56
	mov	r1, #3
	ldr	r2, .L128+60
	add	r3, r3, #65
	str	r4, [r6, #0]
	bl	snprintf
.L125:
	add	r4, r4, #1
	cmp	r4, #12
	bne	.L126
	ldr	r3, .L128+8
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L129:
	.align	2
.L128:
	.word	0
	.word	1079574528
	.word	weather_control_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC32
	.word	982
	.word	GuiVar_RainBucketInUse
	.word	.LANCHOR5-4
	.word	GuiVar_RainBucketMinimum
	.word	.LANCHOR7
	.word	GuiVar_RainBucketMaximumHourly
	.word	GuiVar_RainBucketMaximum24Hour
	.word	weather_preserves
	.word	GuiVar_StatusRainBucketReading
	.word	GuiVar_RainBucketInstalledAt
	.word	.LC33
.LFE35:
	.size	WEATHER_copy_rain_bucket_settings_into_GuiVars, .-WEATHER_copy_rain_bucket_settings_into_GuiVars
	.section	.text.WEATHER_copy_wind_gage_settings_into_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_copy_wind_gage_settings_into_GuiVars
	.type	WEATHER_copy_wind_gage_settings_into_GuiVars, %function
WEATHER_copy_wind_gage_settings_into_GuiVars:
.LFB36:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L134
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI48:
	ldr	r7, .L134+4
	ldr	r0, [r3, #0]
	ldr	r2, .L134+8
	ldr	r3, .L134+12
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [r7, #48]
	ldr	r3, .L134+16
	ldr	r5, .L134+20
	str	r2, [r3, #0]
	ldr	r2, [r7, #52]
	ldr	r3, .L134+24
	ldr	r6, .L134+28
	str	r2, [r3, #0]
	ldr	r2, [r7, #60]
	ldr	r3, .L134+32
	mov	r4, #0
	str	r2, [r3, #0]
.L132:
	ldr	r3, [r7, #44]
	ldr	r2, [r5, #4]!
	cmp	r3, r2
	bne	.L131
	ldr	r0, .L134+36
	mov	r1, #3
	ldr	r2, .L134+40
	add	r3, r3, #65
	str	r4, [r6, #0]
	bl	snprintf
.L131:
	add	r4, r4, #1
	cmp	r4, #12
	bne	.L132
	ldr	r3, .L134
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L135:
	.align	2
.L134:
	.word	weather_control_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC32
	.word	1039
	.word	GuiVar_WindGageInUse
	.word	.LANCHOR5-4
	.word	GuiVar_WindGagePauseSpeed
	.word	.LANCHOR8
	.word	GuiVar_WindGageResumeSpeed
	.word	GuiVar_WindGageInstalledAt
	.word	.LC33
.LFE36:
	.size	WEATHER_copy_wind_gage_settings_into_GuiVars, .-WEATHER_copy_wind_gage_settings_into_GuiVars
	.section	.text.WEATHER_copy_rain_switch_settings_into_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_copy_rain_switch_settings_into_GuiVars
	.type	WEATHER_copy_rain_switch_settings_into_GuiVars, %function
WEATHER_copy_rain_switch_settings_into_GuiVars:
.LFB37:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI49:
	ldr	r4, .L137
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L137+4
	ldr	r3, .L137+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L137+12
	ldr	r0, [r4, #0]
	ldr	r2, [r3, #204]
	ldr	r3, .L137+16
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L138:
	.align	2
.L137:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1083
	.word	.LANCHOR0
	.word	GuiVar_RainSwitchInUse
.LFE37:
	.size	WEATHER_copy_rain_switch_settings_into_GuiVars, .-WEATHER_copy_rain_switch_settings_into_GuiVars
	.section	.text.WEATHER_copy_freeze_switch_settings_into_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_copy_freeze_switch_settings_into_GuiVars
	.type	WEATHER_copy_freeze_switch_settings_into_GuiVars, %function
WEATHER_copy_freeze_switch_settings_into_GuiVars:
.LFB38:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI50:
	ldr	r4, .L140
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L140+4
	ldr	r3, .L140+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L140+12
	ldr	r0, [r4, #0]
	ldr	r2, [r3, #256]
	ldr	r3, .L140+16
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L141:
	.align	2
.L140:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1108
	.word	.LANCHOR0
	.word	GuiVar_FreezeSwitchInUse
.LFE38:
	.size	WEATHER_copy_freeze_switch_settings_into_GuiVars, .-WEATHER_copy_freeze_switch_settings_into_GuiVars
	.section	.text.WEATHER_copy_historical_et_settings_into_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_copy_historical_et_settings_into_GuiVars
	.type	WEATHER_copy_historical_et_settings_into_GuiVars, %function
WEATHER_copy_historical_et_settings_into_GuiVars:
.LFB39:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI51:
	fstmfdd	sp!, {d8}
.LCFI52:
	flds	s16, .L143
	ldr	r4, .L143+4
	ldr	r5, .L143+8
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L143+12
	ldr	r3, .L143+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [r5, #68]
	ldr	r3, .L143+20
	mov	r1, #24
	str	r2, [r3, #0]
	ldr	r3, [r5, #120]
	ldr	r2, .L143+24
	ldr	r0, .L143+28
	str	r3, [r2, #0]
	ldr	r2, .L143+32
	ldr	r8, .L143+36
	mla	r1, r3, r1, r2
	mov	r2, #25
	bl	strlcpy
	ldr	r3, [r5, #124]
	ldr	r2, .L143+40
	mov	r1, #28
	str	r3, [r2, #0]
	ldr	r2, .L143+44
	ldr	r0, .L143+48
	mla	r1, r3, r1, r2
	mov	r2, #25
	bl	strlcpy
	ldr	r6, .L143+52
	ldr	r1, [r5, #128]
	mov	r7, #76
	str	r1, [r8, #0]
	mov	r2, #25
	mla	r1, r7, r1, r6
	ldr	r0, .L143+56
	bl	strlcpy
	ldr	r2, [r5, #36]
	ldr	r3, .L143+60
	add	r1, r5, #132
	str	r2, [r3, #0]
	ldr	r3, [r8, #0]
	mov	r2, #25
	mla	r6, r7, r3, r6
	ldr	r3, .L143+64
	flds	s14, [r6, #28]	@ int
	ldr	r0, .L143+68
	fuitos	s15, s14
	flds	s14, [r6, #32]	@ int
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r6, #36]	@ int
	ldr	r3, .L143+72
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r6, #40]	@ int
	ldr	r3, .L143+76
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r6, #44]	@ int
	ldr	r3, .L143+80
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r6, #48]	@ int
	ldr	r3, .L143+84
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r6, #52]	@ int
	ldr	r3, .L143+88
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r6, #56]	@ int
	ldr	r3, .L143+92
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r6, #60]	@ int
	ldr	r3, .L143+96
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r6, #64]	@ int
	ldr	r3, .L143+100
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r6, #68]	@ int
	ldr	r3, .L143+104
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	ldr	r3, .L143+108
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	ldr	r3, [r6, #72]
	fmsr	s14, r3	@ int
	ldr	r3, .L143+112
	fuitos	s15, s14
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	bl	strlcpy
	add	r1, r5, #156
	mov	r2, #25
	ldr	r0, .L143+116
	bl	strlcpy
	add	r1, r5, #180
	mov	r2, #25
	ldr	r0, .L143+120
	bl	strlcpy
	flds	s14, [r5, #72]	@ int
	ldr	r3, .L143+124
	ldr	r0, [r4, #0]
	fuitos	s15, s14
	flds	s14, [r5, #76]	@ int
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r5, #80]	@ int
	ldr	r3, .L143+128
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r5, #84]	@ int
	ldr	r3, .L143+132
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r5, #88]	@ int
	ldr	r3, .L143+136
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r5, #92]	@ int
	ldr	r3, .L143+140
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r5, #96]	@ int
	ldr	r3, .L143+144
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r5, #100]	@ int
	ldr	r3, .L143+148
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r5, #104]	@ int
	ldr	r3, .L143+152
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r5, #108]	@ int
	ldr	r3, .L143+156
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	flds	s14, [r5, #112]	@ int
	ldr	r3, .L143+160
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	fuitos	s15, s14
	ldr	r3, .L143+164
	fdivs	s15, s15, s16
	fsts	s15, [r3, #0]
	ldr	r3, [r5, #116]
	fmsr	s14, r3	@ int
	ldr	r3, .L143+168
	fuitos	s15, s14
	fdivs	s16, s15, s16
	fsts	s16, [r3, #0]
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	xQueueGiveMutexRecursive
.L144:
	.align	2
.L143:
	.word	1120403456
	.word	weather_control_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC32
	.word	1133
	.word	GuiVar_HistoricalETUseYourOwn
	.word	g_HISTORICAL_ET_state_index
	.word	GuiVar_HistoricalETState
	.word	States
	.word	g_HISTORICAL_ET_city_index
	.word	g_HISTORICAL_ET_county_index
	.word	Counties
	.word	GuiVar_HistoricalETCounty
	.word	ET_PredefinedData
	.word	GuiVar_HistoricalETCity
	.word	GuiVar_ETGageMaxPercent
	.word	GuiVar_HistoricalET1
	.word	GuiVar_HistoricalETUserState
	.word	GuiVar_HistoricalET2
	.word	GuiVar_HistoricalET3
	.word	GuiVar_HistoricalET4
	.word	GuiVar_HistoricalET5
	.word	GuiVar_HistoricalET6
	.word	GuiVar_HistoricalET7
	.word	GuiVar_HistoricalET8
	.word	GuiVar_HistoricalET9
	.word	GuiVar_HistoricalET10
	.word	GuiVar_HistoricalET11
	.word	GuiVar_HistoricalET12
	.word	GuiVar_HistoricalETUserCounty
	.word	GuiVar_HistoricalETUserCity
	.word	GuiVar_HistoricalETUser1
	.word	GuiVar_HistoricalETUser2
	.word	GuiVar_HistoricalETUser3
	.word	GuiVar_HistoricalETUser4
	.word	GuiVar_HistoricalETUser5
	.word	GuiVar_HistoricalETUser6
	.word	GuiVar_HistoricalETUser7
	.word	GuiVar_HistoricalETUser8
	.word	GuiVar_HistoricalETUser9
	.word	GuiVar_HistoricalETUser10
	.word	GuiVar_HistoricalETUser11
	.word	GuiVar_HistoricalETUser12
.LFE39:
	.size	WEATHER_copy_historical_et_settings_into_GuiVars, .-WEATHER_copy_historical_et_settings_into_GuiVars
	.section	.text.WEATHER_there_is_a_weather_option_in_the_chain,"ax",%progbits
	.align	2
	.global	WEATHER_there_is_a_weather_option_in_the_chain
	.type	WEATHER_there_is_a_weather_option_in_the_chain, %function
WEATHER_there_is_a_weather_option_in_the_chain:
.LFB46:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI53:
	bl	COMM_MNGR_network_is_available_for_normal_use
	subs	r4, r0, #0
	beq	.L146
.LBB24:
	ldr	r3, .L153
	ldr	r2, .L153+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L153+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L153+12
	add	r2, r3, #1104
.L149:
	ldr	r1, [r3, #16]
	cmp	r1, #0
	beq	.L147
	ldr	r1, [r3, #60]
	cmp	r1, #0
	beq	.L147
	ldr	r1, [r3, #64]
	cmp	r1, #0
	bne	.L151
.L147:
	add	r3, r3, #92
	cmp	r3, r2
	bne	.L149
	mov	r4, #0
	b	.L148
.L151:
	mov	r4, #1
.L148:
	ldr	r3, .L153
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L146:
.LBE24:
	mov	r0, r4
	ldmfd	sp!, {r4, pc}
.L154:
	.align	2
.L153:
	.word	chain_members_recursive_MUTEX
	.word	.LC32
	.word	1425
	.word	chain
.LFE46:
	.size	WEATHER_there_is_a_weather_option_in_the_chain, .-WEATHER_there_is_a_weather_option_in_the_chain
	.section	.text.WEATHER_get_et_gage_box_index,"ax",%progbits
	.align	2
	.global	WEATHER_get_et_gage_box_index
	.type	WEATHER_get_et_gage_box_index, %function
WEATHER_get_et_gage_box_index:
.LFB47:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI54:
	ldr	r4, .L156
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L156+4
	ldr	r3, .L156+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L156+12
	ldr	r3, .L156+16
	mov	r1, #0
	str	r3, [sp, #0]
	add	r3, r0, #308
	str	r3, [sp, #4]
	ldr	r3, .L156+20
	mov	r2, #11
	str	r3, [sp, #8]
	add	r0, r0, #24
	mov	r3, r1
	bl	SHARED_get_uint32
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L157:
	.align	2
.L156:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1458
	.word	.LANCHOR0
	.word	nm_WEATHER_set_et_gage_box_index
	.word	.LC5
.LFE47:
	.size	WEATHER_get_et_gage_box_index, .-WEATHER_get_et_gage_box_index
	.section	.text.WEATHER_get_et_gage_is_in_use,"ax",%progbits
	.align	2
	.global	WEATHER_get_et_gage_is_in_use
	.type	WEATHER_get_et_gage_is_in_use, %function
WEATHER_get_et_gage_is_in_use:
.LFB48:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L161
	stmfd	sp!, {r0, r4, lr}
.LCFI55:
	ldr	r2, .L161+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L161+8
	bl	xQueueTakeMutexRecursive_debug
	bl	WEATHER_there_is_a_weather_option_in_the_chain
	subs	r4, r0, #0
	beq	.L159
	ldr	r3, .L161+12
	ldr	r0, .L161+16
	str	r3, [sp, #0]
	mov	r1, #0
	ldr	r2, .L161+20
	add	r3, r0, #280
	bl	SHARED_get_bool
	mov	r4, r0
.L159:
	ldr	r3, .L161
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r3, r4, pc}
.L162:
	.align	2
.L161:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1478
	.word	.LC16
	.word	.LANCHOR0+28
	.word	nm_WEATHER_set_et_gage_connected
.LFE48:
	.size	WEATHER_get_et_gage_is_in_use, .-WEATHER_get_et_gage_is_in_use
	.section	.text.WEATHER_get_et_gage_log_pulses,"ax",%progbits
	.align	2
	.global	WEATHER_get_et_gage_log_pulses
	.type	WEATHER_get_et_gage_log_pulses, %function
WEATHER_get_et_gage_log_pulses:
.LFB49:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI56:
	ldr	r4, .L164
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L164+4
	ldr	r3, .L164+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L164+12
	ldr	r2, .L164+16
	mov	r1, #0
	str	r2, [sp, #0]
	add	r0, r3, #32
	ldr	r2, .L164+20
	add	r3, r3, #308
	bl	SHARED_get_bool
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, pc}
.L165:
	.align	2
.L164:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1507
	.word	.LANCHOR0
	.word	.LC15
	.word	nm_WEATHER_set_et_gage_log_pulses
.LFE49:
	.size	WEATHER_get_et_gage_log_pulses, .-WEATHER_get_et_gage_log_pulses
	.section	.text.WEATHER_get_rain_bucket_box_index,"ax",%progbits
	.align	2
	.global	WEATHER_get_rain_bucket_box_index
	.type	WEATHER_get_rain_bucket_box_index, %function
WEATHER_get_rain_bucket_box_index:
.LFB50:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI57:
	ldr	r4, .L167
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L167+4
	ldr	r3, .L167+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L167+12
	ldr	r3, .L167+16
	mov	r1, #0
	str	r3, [sp, #0]
	add	r3, r0, #308
	str	r3, [sp, #4]
	ldr	r3, .L167+20
	mov	r2, #11
	str	r3, [sp, #8]
	add	r0, r0, #4
	mov	r3, r1
	bl	SHARED_get_uint32
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L168:
	.align	2
.L167:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1525
	.word	.LANCHOR0
	.word	nm_WEATHER_set_rain_bucket_box_index
	.word	.LC4
.LFE50:
	.size	WEATHER_get_rain_bucket_box_index, .-WEATHER_get_rain_bucket_box_index
	.section	.text.WEATHER_get_rain_bucket_is_in_use,"ax",%progbits
	.align	2
	.global	WEATHER_get_rain_bucket_is_in_use
	.type	WEATHER_get_rain_bucket_is_in_use, %function
WEATHER_get_rain_bucket_is_in_use:
.LFB51:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L172
	stmfd	sp!, {r0, r4, lr}
.LCFI58:
	ldr	r2, .L172+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L172+8
	bl	xQueueTakeMutexRecursive_debug
	bl	WEATHER_there_is_a_weather_option_in_the_chain
	subs	r4, r0, #0
	beq	.L170
	ldr	r3, .L172+12
	ldr	r0, .L172+16
	str	r3, [sp, #0]
	mov	r1, #0
	ldr	r2, .L172+20
	add	r3, r0, #300
	bl	SHARED_get_bool
	mov	r4, r0
.L170:
	ldr	r3, .L172
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r3, r4, pc}
.L173:
	.align	2
.L172:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1545
	.word	.LC14
	.word	.LANCHOR0+8
	.word	nm_WEATHER_set_rain_bucket_connected
.LFE51:
	.size	WEATHER_get_rain_bucket_is_in_use, .-WEATHER_get_rain_bucket_is_in_use
	.section	.text.WEATHER_get_rain_bucket_minimum_inches_100u,"ax",%progbits
	.align	2
	.global	WEATHER_get_rain_bucket_minimum_inches_100u
	.type	WEATHER_get_rain_bucket_minimum_inches_100u, %function
WEATHER_get_rain_bucket_minimum_inches_100u:
.LFB52:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI59:
	ldr	r4, .L175
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L175+4
	ldr	r3, .L175+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L175+12
	ldr	r3, .L175+16
	mov	r1, #1
	str	r3, [sp, #0]
	add	r3, r0, #308
	str	r3, [sp, #4]
	ldr	r3, .L175+20
	mov	r2, #200
	str	r3, [sp, #8]
	add	r0, r0, #12
	mov	r3, #10
	bl	SHARED_get_uint32
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L176:
	.align	2
.L175:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1575
	.word	.LANCHOR0
	.word	nm_WEATHER_set_rain_bucket_minimum_inches
	.word	.LC3
.LFE52:
	.size	WEATHER_get_rain_bucket_minimum_inches_100u, .-WEATHER_get_rain_bucket_minimum_inches_100u
	.section	.text.WEATHER_get_rain_bucket_max_hourly_inches_100u,"ax",%progbits
	.align	2
	.global	WEATHER_get_rain_bucket_max_hourly_inches_100u
	.type	WEATHER_get_rain_bucket_max_hourly_inches_100u, %function
WEATHER_get_rain_bucket_max_hourly_inches_100u:
.LFB53:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI60:
	ldr	r4, .L178
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L178+4
	ldr	r3, .L178+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L178+12
	ldr	r3, .L178+16
	mov	r1, #1
	str	r3, [sp, #0]
	add	r3, r0, #308
	str	r3, [sp, #4]
	ldr	r3, .L178+20
	mov	r2, #200
	str	r3, [sp, #8]
	add	r0, r0, #16
	mov	r3, #20
	bl	SHARED_get_uint32
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L179:
	.align	2
.L178:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1595
	.word	.LANCHOR0
	.word	nm_WEATHER_set_rain_bucket_max_hourly
	.word	.LC2
.LFE53:
	.size	WEATHER_get_rain_bucket_max_hourly_inches_100u, .-WEATHER_get_rain_bucket_max_hourly_inches_100u
	.section	.text.WEATHER_get_rain_bucket_max_24_hour_inches_100u,"ax",%progbits
	.align	2
	.global	WEATHER_get_rain_bucket_max_24_hour_inches_100u
	.type	WEATHER_get_rain_bucket_max_24_hour_inches_100u, %function
WEATHER_get_rain_bucket_max_24_hour_inches_100u:
.LFB54:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI61:
	ldr	r4, .L181
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L181+4
	ldr	r3, .L181+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L181+12
	ldr	r3, .L181+16
	mov	r1, #1
	str	r3, [sp, #0]
	add	r3, r0, #308
	str	r3, [sp, #4]
	ldr	r3, .L181+20
	mov	r2, #600
	str	r3, [sp, #8]
	add	r0, r0, #20
	mov	r3, #60
	bl	SHARED_get_uint32
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L182:
	.align	2
.L181:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1615
	.word	.LANCHOR0
	.word	nm_WEATHER_set_rain_bucket_max_24_hour
	.word	.LC1
.LFE54:
	.size	WEATHER_get_rain_bucket_max_24_hour_inches_100u, .-WEATHER_get_rain_bucket_max_24_hour_inches_100u
	.section	.text.WEATHER_get_wind_gage_box_index,"ax",%progbits
	.align	2
	.global	WEATHER_get_wind_gage_box_index
	.type	WEATHER_get_wind_gage_box_index, %function
WEATHER_get_wind_gage_box_index:
.LFB55:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI62:
	ldr	r4, .L184
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L184+4
	ldr	r3, .L184+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L184+12
	ldr	r3, .L184+16
	mov	r1, #0
	str	r3, [sp, #0]
	add	r3, r0, #308
	str	r3, [sp, #4]
	ldr	r3, .L184+20
	mov	r2, #11
	str	r3, [sp, #8]
	add	r0, r0, #44
	mov	r3, r1
	bl	SHARED_get_uint32
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L185:
	.align	2
.L184:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1635
	.word	.LANCHOR0
	.word	nm_WEATHER_set_wind_gage_box_index
	.word	.LC0
.LFE55:
	.size	WEATHER_get_wind_gage_box_index, .-WEATHER_get_wind_gage_box_index
	.section	.text.WEATHER_get_wind_gage_in_use,"ax",%progbits
	.align	2
	.global	WEATHER_get_wind_gage_in_use
	.type	WEATHER_get_wind_gage_in_use, %function
WEATHER_get_wind_gage_in_use:
.LFB56:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L189
	stmfd	sp!, {r0, r4, lr}
.LCFI63:
	ldr	r2, .L189+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L189+8
	bl	xQueueTakeMutexRecursive_debug
	bl	WEATHER_there_is_a_weather_option_in_the_chain
	subs	r4, r0, #0
	beq	.L187
	ldr	r3, .L189+12
	ldr	r0, .L189+16
	str	r3, [sp, #0]
	mov	r1, #0
	ldr	r2, .L189+20
	add	r3, r0, #260
	bl	SHARED_get_bool
	mov	r4, r0
.L187:
	ldr	r3, .L189
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r3, r4, pc}
.L190:
	.align	2
.L189:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1673
	.word	.LC13
	.word	.LANCHOR0+48
	.word	nm_WEATHER_set_wind_gage_connected
.LFE56:
	.size	WEATHER_get_wind_gage_in_use, .-WEATHER_get_wind_gage_in_use
	.section	.text.WEATHER_get_rain_switch_in_use,"ax",%progbits
	.align	2
	.global	WEATHER_get_rain_switch_in_use
	.type	WEATHER_get_rain_switch_in_use, %function
WEATHER_get_rain_switch_in_use:
.LFB57:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L194
	stmfd	sp!, {r0, r4, lr}
.LCFI64:
	ldr	r2, .L194+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L194+8
	bl	xQueueTakeMutexRecursive_debug
	bl	WEATHER_there_is_a_weather_option_in_the_chain
	subs	r4, r0, #0
	beq	.L192
	ldr	r3, .L194+12
	ldr	r0, .L194+16
	str	r3, [sp, #0]
	mov	r1, #0
	ldr	r2, .L194+20
	add	r3, r0, #104
	bl	SHARED_get_bool
	mov	r4, r0
.L192:
	ldr	r3, .L194
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r3, r4, pc}
.L195:
	.align	2
.L194:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1703
	.word	.LC12
	.word	.LANCHOR0+204
	.word	nm_WEATHER_set_rain_switch_in_use
.LFE57:
	.size	WEATHER_get_rain_switch_in_use, .-WEATHER_get_rain_switch_in_use
	.section	.text.WEATHER_get_SW1_as_rain_switch_in_use,"ax",%progbits
	.align	2
	.global	WEATHER_get_SW1_as_rain_switch_in_use
	.type	WEATHER_get_SW1_as_rain_switch_in_use, %function
WEATHER_get_SW1_as_rain_switch_in_use:
.LFB58:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L202
	stmfd	sp!, {r0, r4, lr}
.LCFI65:
	ldr	r2, .L202+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	mov	r3, #1744
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L202+8
	mov	r2, #0
	mov	ip, r3
	mov	r0, #92
.L200:
	ldr	r1, [r3, #16]
	cmp	r1, #0
	beq	.L197
	mla	r1, r0, r2, ip
	ldrb	r1, [r1, #52]	@ zero_extendqisi2
	tst	r1, #2
	beq	.L197
	tst	r1, #1
	beq	.L197
	ldr	r1, [r3, #60]
	cmp	r1, #0
	beq	.L198
	ldr	r1, [r3, #64]
	cmp	r1, #0
	bne	.L197
.L198:
	ldr	r3, .L202+12
	ldr	r0, .L202+16
	str	r3, [sp, #0]
	mov	r1, #0
	ldr	r2, .L202+20
	add	r3, r0, #104
	bl	SHARED_get_bool
	mov	r4, r0
	b	.L199
.L197:
	add	r2, r2, #1
	cmp	r2, #12
	add	r3, r3, #92
	bne	.L200
	mov	r4, #0
.L199:
	ldr	r3, .L202
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r3, r4, pc}
.L203:
	.align	2
.L202:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	chain
	.word	.LC12
	.word	.LANCHOR0+204
	.word	nm_WEATHER_set_rain_switch_in_use
.LFE58:
	.size	WEATHER_get_SW1_as_rain_switch_in_use, .-WEATHER_get_SW1_as_rain_switch_in_use
	.section	.text.WEATHER_get_rain_switch_connected_to_this_controller,"ax",%progbits
	.align	2
	.global	WEATHER_get_rain_switch_connected_to_this_controller
	.type	WEATHER_get_rain_switch_connected_to_this_controller, %function
WEATHER_get_rain_switch_connected_to_this_controller:
.LFB59:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #11
	stmfd	sp!, {r4, r5, lr}
.LCFI66:
	mov	r4, r0
	sub	sp, sp, #60
.LCFI67:
	bhi	.L205
	add	r5, sp, #12
	add	r3, r0, #1
	str	r3, [sp, #0]
	mov	r0, r5
	mov	r1, #48
	ldr	r2, .L207
	ldr	r3, .L207+4
	bl	snprintf
	ldr	r3, .L207+8
	ldr	r2, .L207+12
	add	r0, r4, #52
	str	r2, [sp, #0]
	add	r2, r3, #308
	stmib	sp, {r2, r5}
	add	r0, r3, r0, asl #2
	mov	r1, r4
	mov	r2, #12
	mov	r3, #0
	bl	SHARED_get_bool_from_array
	b	.L206
.L205:
	ldr	r0, .L207+16
	ldr	r1, .L207+20
	bl	Alert_index_out_of_range_with_filename
	mov	r0, #0
.L206:
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, pc}
.L208:
	.align	2
.L207:
	.word	.LC25
	.word	.LC29
	.word	.LANCHOR0
	.word	nm_WEATHER_set_rain_switch_connected
	.word	.LC32
	.word	1793
.LFE59:
	.size	WEATHER_get_rain_switch_connected_to_this_controller, .-WEATHER_get_rain_switch_connected_to_this_controller
	.section	.text.WEATHER_sw1_is_available_and_enabled_as_a_rain_switch_at_this_controller,"ax",%progbits
	.align	2
	.global	WEATHER_sw1_is_available_and_enabled_as_a_rain_switch_at_this_controller
	.type	WEATHER_sw1_is_available_and_enabled_as_a_rain_switch_at_this_controller, %function
WEATHER_sw1_is_available_and_enabled_as_a_rain_switch_at_this_controller:
.LFB60:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #11
	stmfd	sp!, {r4, r5, lr}
.LCFI68:
	mov	r4, r0
	sub	sp, sp, #60
.LCFI69:
	bhi	.L210
	ldr	r3, .L219
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L219+4
	ldr	r3, .L219+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L219+12
	ldr	r0, .L219+16
	str	r3, [sp, #0]
	mov	r1, #0
	ldr	r2, .L219+20
	add	r3, r0, #104
	bl	SHARED_get_bool
	cmp	r0, #0
	beq	.L214
	ldr	r3, .L219+24
	mov	r2, #92
	mla	r3, r2, r4, r3
	ldr	r2, [r3, #16]
	cmp	r2, #0
	beq	.L217
	ldrb	r2, [r3, #52]	@ zero_extendqisi2
	mov	r1, r2, lsr #1
	ands	r1, r1, #1
	moveq	r4, r1
	beq	.L211
	ands	r2, r2, #1
	beq	.L217
	ldr	r2, [r3, #60]
	cmp	r2, #0
	beq	.L212
	ldr	r3, [r3, #64]
	cmp	r3, #0
	movne	r4, #0
	bne	.L211
.L212:
	add	r5, sp, #12
	add	r3, r4, #1
	str	r3, [sp, #0]
	mov	r1, #48
	ldr	r2, .L219+28
	ldr	r3, .L219+32
	mov	r0, r5
	bl	snprintf
	ldr	r3, .L219+36
	ldr	r2, .L219+40
	add	r0, r4, #52
	str	r2, [sp, #0]
	add	r2, r3, #308
	stmib	sp, {r2, r5}
	add	r0, r3, r0, asl #2
	mov	r1, r4
	mov	r2, #12
	mov	r3, #0
	bl	SHARED_get_bool_from_array
.L214:
	mov	r4, r0
	b	.L211
.L217:
	mov	r4, r2
.L211:
	ldr	r3, .L219
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	b	.L213
.L210:
	ldr	r0, .L219+4
	ldr	r1, .L219+44
	bl	Alert_index_out_of_range_with_filename
	mov	r4, #0
.L213:
	mov	r0, r4
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, pc}
.L220:
	.align	2
.L219:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1828
	.word	.LC12
	.word	.LANCHOR0+204
	.word	nm_WEATHER_set_rain_switch_in_use
	.word	chain
	.word	.LC25
	.word	.LC29
	.word	.LANCHOR0
	.word	nm_WEATHER_set_rain_switch_connected
	.word	1873
.LFE60:
	.size	WEATHER_sw1_is_available_and_enabled_as_a_rain_switch_at_this_controller, .-WEATHER_sw1_is_available_and_enabled_as_a_rain_switch_at_this_controller
	.section	.text.WEATHER_get_freeze_switch_in_use,"ax",%progbits
	.align	2
	.global	WEATHER_get_freeze_switch_in_use
	.type	WEATHER_get_freeze_switch_in_use, %function
WEATHER_get_freeze_switch_in_use:
.LFB61:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L224
	stmfd	sp!, {r0, r4, lr}
.LCFI70:
	ldr	r2, .L224+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L224+8
	bl	xQueueTakeMutexRecursive_debug
	bl	WEATHER_there_is_a_weather_option_in_the_chain
	subs	r4, r0, #0
	beq	.L222
	ldr	r3, .L224+12
	ldr	r0, .L224+16
	str	r3, [sp, #0]
	mov	r1, #0
	ldr	r2, .L224+20
	add	r3, r0, #52
	bl	SHARED_get_bool
	mov	r4, r0
.L222:
	ldr	r3, .L224
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	ldmfd	sp!, {r3, r4, pc}
.L225:
	.align	2
.L224:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1886
	.word	.LC11
	.word	.LANCHOR0+256
	.word	nm_WEATHER_set_freeze_switch_in_use
.LFE61:
	.size	WEATHER_get_freeze_switch_in_use, .-WEATHER_get_freeze_switch_in_use
	.section	.text.WEATHER_get_freeze_switch_connected_to_this_controller,"ax",%progbits
	.align	2
	.global	WEATHER_get_freeze_switch_connected_to_this_controller
	.type	WEATHER_get_freeze_switch_connected_to_this_controller, %function
WEATHER_get_freeze_switch_connected_to_this_controller:
.LFB62:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #11
	stmfd	sp!, {r4, r5, lr}
.LCFI71:
	mov	r4, r0
	sub	sp, sp, #60
.LCFI72:
	bhi	.L227
	add	r5, sp, #12
	add	r3, r0, #1
	str	r3, [sp, #0]
	mov	r0, r5
	mov	r1, #48
	ldr	r2, .L229
	ldr	r3, .L229+4
	bl	snprintf
	ldr	r3, .L229+8
	ldr	r2, .L229+12
	add	r0, r4, #65
	str	r2, [sp, #0]
	add	r2, r3, #308
	stmib	sp, {r2, r5}
	add	r0, r3, r0, asl #2
	mov	r1, r4
	mov	r2, #12
	mov	r3, #0
	bl	SHARED_get_bool_from_array
	b	.L228
.L227:
	ldr	r0, .L229+16
	ldr	r1, .L229+20
	bl	Alert_index_out_of_range_with_filename
	mov	r0, #0
.L228:
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, pc}
.L230:
	.align	2
.L229:
	.word	.LC25
	.word	.LC28
	.word	.LANCHOR0
	.word	nm_WEATHER_set_freeze_switch_connected
	.word	.LC32
	.word	1932
.LFE62:
	.size	WEATHER_get_freeze_switch_connected_to_this_controller, .-WEATHER_get_freeze_switch_connected_to_this_controller
	.section	.text.WEATHER_get_a_copy_of_the_wind_settings,"ax",%progbits
	.align	2
	.global	WEATHER_get_a_copy_of_the_wind_settings
	.type	WEATHER_get_a_copy_of_the_wind_settings, %function
WEATHER_get_a_copy_of_the_wind_settings:
.LFB63:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI73:
	ldr	r5, .L232
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L232+4
	ldr	r3, .L232+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L232+12
	ldmia	r3, {r0, r1, r2, r3}
	stmia	r4, {r0, r1, r2, r3}
	ldr	r0, [r5, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L233:
	.align	2
.L232:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1960
	.word	.LANCHOR0+52
.LFE63:
	.size	WEATHER_get_a_copy_of_the_wind_settings, .-WEATHER_get_a_copy_of_the_wind_settings
	.section	.text.WEATHER_get_percent_of_historical_cap_100u,"ax",%progbits
	.align	2
	.global	WEATHER_get_percent_of_historical_cap_100u
	.type	WEATHER_get_percent_of_historical_cap_100u, %function
WEATHER_get_percent_of_historical_cap_100u:
.LFB64:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI74:
	ldr	r4, .L235
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L235+4
	mov	r3, #1984
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L235+8
	ldr	r3, .L235+12
	mov	r1, #10
	str	r3, [sp, #0]
	add	r3, r0, #308
	str	r3, [sp, #4]
	ldr	r3, .L235+16
	mov	r2, #500
	str	r3, [sp, #8]
	add	r0, r0, #36
	mov	r3, #150
	bl	SHARED_get_uint32
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r1, r2, r3, r4, r5, pc}
.L236:
	.align	2
.L235:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	.LANCHOR0
	.word	nm_WEATHER_set_percent_of_historical_et_cap
	.word	.LC6
.LFE64:
	.size	WEATHER_get_percent_of_historical_cap_100u, .-WEATHER_get_percent_of_historical_cap_100u
	.section	.text.WEATHER_get_reference_et_use_your_own,"ax",%progbits
	.align	2
	.global	WEATHER_get_reference_et_use_your_own
	.type	WEATHER_get_reference_et_use_your_own, %function
WEATHER_get_reference_et_use_your_own:
.LFB65:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI75:
	ldr	r4, .L238
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L238+4
	mov	r3, #2016
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L238+8
	ldr	r2, .L238+12
	mov	r1, #0
	str	r2, [sp, #0]
	add	r0, r3, #68
	ldr	r2, .L238+16
	add	r3, r3, #308
	bl	SHARED_get_bool
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, pc}
.L239:
	.align	2
.L238:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	.LANCHOR0
	.word	.LC10
	.word	nm_WEATHER_set_reference_et_use_your_own
.LFE65:
	.size	WEATHER_get_reference_et_use_your_own, .-WEATHER_get_reference_et_use_your_own
	.section	.text.WEATHER_get_reference_et_number,"ax",%progbits
	.align	2
	.global	WEATHER_get_reference_et_number
	.type	WEATHER_get_reference_et_number, %function
WEATHER_get_reference_et_number:
.LFB66:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L243
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI76:
	ldr	r2, .L243+4
	mov	r5, r0
	sub	sp, sp, #68
.LCFI77:
	ldr	r0, [r3, #0]
	mov	r1, #400
	ldr	r3, .L243+8
	bl	xQueueTakeMutexRecursive_debug
	sub	r4, r5, #1
	cmp	r4, #11
	bhi	.L241
	add	r6, sp, #20
	mov	r1, #48
	ldr	r2, .L243+12
	ldr	r3, .L243+16
	mov	r0, r6
	str	r5, [sp, #0]
	bl	snprintf
	mov	r2, #2000
	str	r2, [sp, #0]
	ldr	r2, .L243+20
	ldr	r3, .L243+24
	add	r2, r2, r4, asl #2
	ldr	r2, [r2, #1624]
	add	r0, r5, #17
	str	r2, [sp, #4]
	ldr	r2, .L243+28
	add	r0, r3, r0, asl #2
	str	r2, [sp, #8]
	add	r2, r3, #308
	str	r2, [sp, #12]
	mov	r1, r4
	mov	r2, #12
	mov	r3, #0
	str	r6, [sp, #16]
	bl	SHARED_get_uint32_from_array
	mov	r4, r0
	b	.L242
.L241:
	ldr	r0, .L243+4
	ldr	r1, .L243+32
	bl	Alert_index_out_of_range_with_filename
	ldr	r3, .L243+20
	ldr	r4, [r3, #1624]
.L242:
	ldr	r3, .L243
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r4
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, pc}
.L244:
	.align	2
.L243:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	2052
	.word	.LC25
	.word	.LC26
	.word	ET_PredefinedData
	.word	.LANCHOR0
	.word	nm_WEATHER_set_reference_et_number
	.word	2071
.LFE66:
	.size	WEATHER_get_reference_et_number, .-WEATHER_get_reference_et_number
	.section	.text.WEATHER_get_reference_et_city_index,"ax",%progbits
	.align	2
	.global	WEATHER_get_reference_et_city_index
	.type	WEATHER_get_reference_et_city_index, %function
WEATHER_get_reference_et_city_index:
.LFB67:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI78:
	ldr	r3, .L246
	add	r0, sp, #20
	ldr	r3, [r3, #128]
	mov	r1, #0
	str	r3, [r0, #-4]!
	ldr	r3, .L246+4
	mov	r2, #153
	str	r3, [sp, #4]
	ldr	r3, .L246+8
	str	r1, [sp, #0]
	str	r3, [sp, #8]
	ldr	r3, .L246+12
	str	r3, [sp, #12]
	mov	r3, #13
	bl	RC_uint32_with_filename
	ldr	r0, [sp, #16]
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.L247:
	.align	2
.L246:
	.word	.LANCHOR0
	.word	.LC34
	.word	.LC32
	.word	2103
.LFE67:
	.size	WEATHER_get_reference_et_city_index, .-WEATHER_get_reference_et_city_index
	.section	.text.WEATHER_get_et_and_rain_table_roll_time,"ax",%progbits
	.align	2
	.global	WEATHER_get_et_and_rain_table_roll_time
	.type	WEATHER_get_et_and_rain_table_roll_time, %function
WEATHER_get_et_and_rain_table_roll_time:
.LFB68:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, lr}
.LCFI79:
	ldr	r3, .L249
	add	r0, sp, #20
	ldr	r3, [r3, #0]
	ldr	r1, .L249+4
	str	r3, [r0, #-4]!
	mov	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L249+8
	ldr	r2, .L249+12
	str	r3, [sp, #4]
	ldr	r3, .L249+16
	str	r3, [sp, #8]
	ldr	r3, .L249+20
	str	r3, [sp, #12]
	ldr	r3, .L249+24
	bl	RC_uint32_with_filename
	ldr	r0, [sp, #16]
	add	sp, sp, #20
	ldmfd	sp!, {pc}
.L250:
	.align	2
.L249:
	.word	.LANCHOR0
	.word	61200
	.word	.LC35
	.word	82800
	.word	.LC32
	.word	2133
	.word	72000
.LFE68:
	.size	WEATHER_get_et_and_rain_table_roll_time, .-WEATHER_get_et_and_rain_table_roll_time
	.section	.text.WEATHER_get_change_bits_ptr,"ax",%progbits
	.align	2
	.global	WEATHER_get_change_bits_ptr
	.type	WEATHER_get_change_bits_ptr, %function
WEATHER_get_change_bits_ptr:
.LFB69:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L252
	add	r1, r3, #308
	add	r2, r3, #312
	add	r3, r3, #316
	b	SHARED_get_32_bit_change_bits_ptr
.L253:
	.align	2
.L252:
	.word	.LANCHOR0
.LFE69:
	.size	WEATHER_get_change_bits_ptr, .-WEATHER_get_change_bits_ptr
	.section	.text.WEATHER_extract_and_store_historical_et_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_extract_and_store_historical_et_changes_from_GuiVars
	.type	WEATHER_extract_and_store_historical_et_changes_from_GuiVars, %function
WEATHER_extract_and_store_historical_et_changes_from_GuiVars:
.LFB45:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI80:
	fstmfdd	sp!, {d8}
.LCFI81:
	flds	s16, .L255
	ldr	r7, .L255+4
	sub	sp, sp, #12
.LCFI82:
	bl	FLOWSENSE_get_controller_index
	mov	r4, #1
	mov	r5, r0
	mov	r0, #2
	bl	WEATHER_get_change_bits_ptr
	mov	r1, #400
	ldr	r2, .L255+8
	ldr	r3, .L255+12
	mov	r6, r0
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L255+16
	stmia	sp, {r4, r6}
	ldr	r0, [r3, #0]
	mov	r1, r4
	mov	r3, r5
	mov	r2, #2
	bl	nm_WEATHER_set_reference_et_use_your_own
	ldr	r3, .L255+20
	stmia	sp, {r4, r6}
	ldr	r0, [r3, #0]
	mov	r1, r4
	mov	r3, r5
	mov	r2, #2
	bl	nm_WEATHER_set_reference_et_state_index
	ldr	r3, .L255+24
	stmia	sp, {r4, r6}
	ldr	r0, [r3, #0]
	mov	r1, r4
	mov	r3, r5
	mov	r2, #2
	bl	nm_WEATHER_set_reference_et_county_index
	ldr	r3, .L255+28
	stmia	sp, {r4, r6}
	ldr	r0, [r3, #0]
	mov	r1, r4
	mov	r3, r5
	mov	r2, #2
	bl	nm_WEATHER_set_reference_et_city_index
	ldr	r3, .L255+32
	stmia	sp, {r4, r6}
	ldr	r0, [r3, #0]
	mov	r1, r4
	mov	r3, r5
	mov	r2, #2
	bl	nm_WEATHER_set_percent_of_historical_et_cap
	ldr	r3, .L255+36
	mov	r1, r4
	flds	s15, [r3, #0]
	mov	r2, r4
	mov	r3, #2
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_reference_et_number
	ldr	r3, .L255+40
	mov	r1, #2
	flds	s15, [r3, #0]
	mov	r2, r4
	mov	r3, r1
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_reference_et_number
	ldr	r3, .L255+44
	mov	r2, r4
	flds	s15, [r3, #0]
	mov	r1, #3
	mov	r3, #2
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_reference_et_number
	ldr	r3, .L255+48
	mov	r2, r4
	flds	s15, [r3, #0]
	mov	r1, #4
	mov	r3, #2
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_reference_et_number
	ldr	r3, .L255+52
	mov	r2, r4
	flds	s15, [r3, #0]
	mov	r1, #5
	mov	r3, #2
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_reference_et_number
	ldr	r3, .L255+56
	mov	r2, r4
	flds	s15, [r3, #0]
	mov	r1, #6
	mov	r3, #2
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_reference_et_number
	ldr	r3, .L255+60
	mov	r2, r4
	flds	s15, [r3, #0]
	mov	r1, #7
	mov	r3, #2
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_reference_et_number
	ldr	r3, .L255+64
	mov	r2, r4
	flds	s15, [r3, #0]
	mov	r1, #8
	mov	r3, #2
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_reference_et_number
	ldr	r3, .L255+68
	mov	r2, r4
	flds	s15, [r3, #0]
	mov	r1, #9
	mov	r3, #2
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_reference_et_number
	ldr	r3, .L255+72
	mov	r2, r4
	flds	s15, [r3, #0]
	mov	r1, #10
	mov	r3, #2
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_reference_et_number
	ldr	r3, .L255+76
	mov	r2, r4
	flds	s15, [r3, #0]
	mov	r1, #11
	mov	r3, #2
	str	r5, [sp, #0]
	fmuls	s15, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_reference_et_number
	ldr	r3, .L255+80
	mov	r2, r4
	flds	s15, [r3, #0]
	mov	r1, #12
	mov	r3, #2
	str	r5, [sp, #0]
	fmuls	s16, s15, s16
	stmib	sp, {r4, r6}
	ftouizs	s16, s16
	fmrs	r0, s16	@ int
	bl	nm_WEATHER_set_reference_et_number
	mov	r1, r4
	mov	r3, r5
	ldr	r0, .L255+84
	mov	r2, #2
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_user_defined_state
	mov	r1, r4
	mov	r3, r5
	ldr	r0, .L255+88
	mov	r2, #2
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_user_defined_county
	ldr	r0, .L255+92
	mov	r1, r4
	mov	r2, #2
	mov	r3, r5
	stmia	sp, {r4, r6}
	bl	nm_WEATHER_set_user_defined_city
	ldr	r0, [r7, #0]
	add	sp, sp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L256:
	.align	2
.L255:
	.word	1120403456
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1380
	.word	GuiVar_HistoricalETUseYourOwn
	.word	g_HISTORICAL_ET_state_index
	.word	g_HISTORICAL_ET_county_index
	.word	g_HISTORICAL_ET_city_index
	.word	GuiVar_ETGageMaxPercent
	.word	GuiVar_HistoricalETUser1
	.word	GuiVar_HistoricalETUser2
	.word	GuiVar_HistoricalETUser3
	.word	GuiVar_HistoricalETUser4
	.word	GuiVar_HistoricalETUser5
	.word	GuiVar_HistoricalETUser6
	.word	GuiVar_HistoricalETUser7
	.word	GuiVar_HistoricalETUser8
	.word	GuiVar_HistoricalETUser9
	.word	GuiVar_HistoricalETUser10
	.word	GuiVar_HistoricalETUser11
	.word	GuiVar_HistoricalETUser12
	.word	GuiVar_HistoricalETUserState
	.word	GuiVar_HistoricalETUserCounty
	.word	GuiVar_HistoricalETUserCity
.LFE45:
	.size	WEATHER_extract_and_store_historical_et_changes_from_GuiVars, .-WEATHER_extract_and_store_historical_et_changes_from_GuiVars
	.section	.text.WEATHER_extract_and_store_freeze_switch_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_extract_and_store_freeze_switch_changes_from_GuiVars
	.type	WEATHER_extract_and_store_freeze_switch_changes_from_GuiVars, %function
WEATHER_extract_and_store_freeze_switch_changes_from_GuiVars:
.LFB44:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI83:
	ldr	r4, .L258
	mov	r0, #2
	bl	WEATHER_get_change_bits_ptr
	mov	r1, #400
	ldr	r2, .L258+4
	ldr	r3, .L258+8
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L258+12
	ldr	r6, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r1, #1
	mov	r2, #2
	stmia	sp, {r1, r5}
	mov	r3, r0
	mov	r0, r6
	bl	nm_WEATHER_set_freeze_switch_in_use
	ldr	r0, [r4, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L259:
	.align	2
.L258:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1348
	.word	GuiVar_FreezeSwitchInUse
.LFE44:
	.size	WEATHER_extract_and_store_freeze_switch_changes_from_GuiVars, .-WEATHER_extract_and_store_freeze_switch_changes_from_GuiVars
	.section	.text.WEATHER_extract_and_store_rain_switch_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_extract_and_store_rain_switch_changes_from_GuiVars
	.type	WEATHER_extract_and_store_rain_switch_changes_from_GuiVars, %function
WEATHER_extract_and_store_rain_switch_changes_from_GuiVars:
.LFB43:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI84:
	ldr	r4, .L261
	mov	r0, #2
	bl	WEATHER_get_change_bits_ptr
	mov	r1, #400
	ldr	r2, .L261+4
	ldr	r3, .L261+8
	mov	r5, r0
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L261+12
	ldr	r6, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r1, #1
	mov	r2, #2
	stmia	sp, {r1, r5}
	mov	r3, r0
	mov	r0, r6
	bl	nm_WEATHER_set_rain_switch_in_use
	ldr	r0, [r4, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L262:
	.align	2
.L261:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1319
	.word	GuiVar_RainSwitchInUse
.LFE43:
	.size	WEATHER_extract_and_store_rain_switch_changes_from_GuiVars, .-WEATHER_extract_and_store_rain_switch_changes_from_GuiVars
	.section	.text.WEATHER_extract_and_store_wind_gage_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_extract_and_store_wind_gage_changes_from_GuiVars
	.type	WEATHER_extract_and_store_wind_gage_changes_from_GuiVars, %function
WEATHER_extract_and_store_wind_gage_changes_from_GuiVars:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, lr}
.LCFI85:
	bl	FLOWSENSE_get_controller_index
	ldr	r7, .L264
	mov	r4, #1
	mov	r5, r0
	mov	r0, #2
	bl	WEATHER_get_change_bits_ptr
	mov	r1, #400
	ldr	r2, .L264+4
	ldr	r3, .L264+8
	mov	r6, r0
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L264+12
	stmia	sp, {r4, r6}
	ldr	r0, [r3, #0]
	mov	r1, r4
	mov	r3, r5
	mov	r2, #2
	bl	nm_WEATHER_set_wind_gage_connected
	ldr	r3, .L264+16
	mov	r1, r4
	ldr	r2, [r3, #0]
	ldr	r3, .L264+20
	stmia	sp, {r4, r6}
	ldr	r0, [r3, r2, asl #2]
	mov	r3, r5
	mov	r2, #2
	bl	nm_WEATHER_set_wind_gage_box_index
	ldr	r3, .L264+24
	stmia	sp, {r4, r6}
	ldr	r0, [r3, #0]
	mov	r1, r4
	mov	r3, r5
	mov	r2, #2
	bl	WEATHER_set_wind_pause_speed
	ldr	r3, .L264+28
	stmia	sp, {r4, r6}
	ldr	r0, [r3, #0]
	mov	r1, r4
	mov	r2, #2
	mov	r3, r5
	bl	WEATHER_set_wind_resume_speed
	ldr	r0, [r7, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L265:
	.align	2
.L264:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1288
	.word	GuiVar_WindGageInUse
	.word	.LANCHOR8
	.word	.LANCHOR5
	.word	GuiVar_WindGagePauseSpeed
	.word	GuiVar_WindGageResumeSpeed
.LFE42:
	.size	WEATHER_extract_and_store_wind_gage_changes_from_GuiVars, .-WEATHER_extract_and_store_wind_gage_changes_from_GuiVars
	.section	.text.WEATHER_extract_and_store_rain_bucket_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_extract_and_store_rain_bucket_changes_from_GuiVars
	.type	WEATHER_extract_and_store_rain_bucket_changes_from_GuiVars, %function
WEATHER_extract_and_store_rain_bucket_changes_from_GuiVars:
.LFB41:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI86:
	fstmfdd	sp!, {d8}
.LCFI87:
	flds	s16, .L267
	ldr	r7, .L267+4
	sub	sp, sp, #8
.LCFI88:
	bl	FLOWSENSE_get_controller_index
	mov	r4, #1
	mov	r5, r0
	mov	r0, #2
	bl	WEATHER_get_change_bits_ptr
	mov	r1, #400
	ldr	r2, .L267+8
	ldr	r3, .L267+12
	mov	r6, r0
	ldr	r0, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L267+16
	stmia	sp, {r4, r6}
	ldr	r0, [r3, #0]
	mov	r1, r4
	mov	r3, r5
	mov	r2, #2
	bl	nm_WEATHER_set_rain_bucket_connected
	ldr	r3, .L267+20
	mov	r1, r4
	ldr	r2, [r3, #0]
	ldr	r3, .L267+24
	stmia	sp, {r4, r6}
	ldr	r0, [r3, r2, asl #2]
	mov	r3, r5
	mov	r2, #2
	bl	nm_WEATHER_set_rain_bucket_box_index
	ldr	r3, .L267+28
	mov	r1, r4
	flds	s15, [r3, #0]
	mov	r2, #2
	mov	r3, r5
	stmia	sp, {r4, r6}
	fmuls	s15, s15, s16
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_rain_bucket_minimum_inches
	ldr	r3, .L267+32
	mov	r1, r4
	flds	s15, [r3, #0]
	mov	r2, #2
	mov	r3, r5
	stmia	sp, {r4, r6}
	fmuls	s15, s15, s16
	ftouizs	s15, s15
	fmrs	r0, s15	@ int
	bl	nm_WEATHER_set_rain_bucket_max_hourly
	ldr	r3, .L267+36
	mov	r1, r4
	flds	s15, [r3, #0]
	mov	r2, #2
	mov	r3, r5
	stmia	sp, {r4, r6}
	fmuls	s16, s15, s16
	ftouizs	s16, s16
	fmrs	r0, s16	@ int
	bl	nm_WEATHER_set_rain_bucket_max_24_hour
	ldr	r0, [r7, #0]
	add	sp, sp, #8
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L268:
	.align	2
.L267:
	.word	1120403456
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1252
	.word	GuiVar_RainBucketInUse
	.word	.LANCHOR7
	.word	.LANCHOR5
	.word	GuiVar_RainBucketMinimum
	.word	GuiVar_RainBucketMaximumHourly
	.word	GuiVar_RainBucketMaximum24Hour
.LFE41:
	.size	WEATHER_extract_and_store_rain_bucket_changes_from_GuiVars, .-WEATHER_extract_and_store_rain_bucket_changes_from_GuiVars
	.section	.text.WEATHER_extract_and_store_et_gage_changes_from_GuiVars,"ax",%progbits
	.align	2
	.global	WEATHER_extract_and_store_et_gage_changes_from_GuiVars
	.type	WEATHER_extract_and_store_et_gage_changes_from_GuiVars, %function
WEATHER_extract_and_store_et_gage_changes_from_GuiVars:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, lr}
.LCFI89:
	ldr	r6, .L271
	mov	r0, #2
	bl	WEATHER_get_change_bits_ptr
	mov	r1, #400
	ldr	r2, .L271+4
	ldr	r3, .L271+8
	mov	r4, #1
	mov	r5, r0
	ldr	r0, [r6, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L271+12
	ldr	r7, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r1, r4
	mov	r2, #2
	stmia	sp, {r4, r5}
	mov	r3, r0
	mov	r0, r7
	bl	nm_WEATHER_set_et_gage_connected
	ldr	r3, .L271+16
	ldr	r2, [r3, #0]
	ldr	r3, .L271+20
	ldr	r7, [r3, r2, asl #2]
	bl	FLOWSENSE_get_controller_index
	mov	r1, r4
	mov	r2, #2
	stmia	sp, {r4, r5}
	mov	r3, r0
	mov	r0, r7
	bl	nm_WEATHER_set_et_gage_box_index
	ldr	r3, .L271+24
	ldr	r7, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	mov	r1, r4
	stmia	sp, {r4, r5}
	ldr	r4, .L271+28
	mov	r2, #2
	ldr	r5, .L271+32
	mov	r3, r0
	mov	r0, r7
	ldr	r7, .L271+36
	bl	nm_WEATHER_set_et_gage_log_pulses
	ldr	r0, [r6, #0]
	bl	xQueueGiveMutexRecursive
	mov	r1, #400
	ldr	r3, .L271+40
	ldr	r0, [r4, #0]
	ldr	r2, .L271+4
	bl	xQueueTakeMutexRecursive_debug
	ldr	r1, [r7, #0]
	ldr	r3, .L271+44
	mov	r6, #11
	ldr	r0, [r5, #92]
	mul	r1, r6, r1
	ldr	r3, [r3, #0]
	cmp	r0, r1
	str	r3, [r5, #80]
	beq	.L270
	bl	Alert_remaining_gage_pulses_changed
	ldr	r3, [r7, #0]
	mul	r6, r3, r6
	str	r6, [r5, #92]
.L270:
	ldr	r0, [r4, #0]
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L272:
	.align	2
.L271:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	1203
	.word	GuiVar_ETGageInUse
	.word	.LANCHOR6
	.word	.LANCHOR5
	.word	GuiVar_ETGageLogPulses
	.word	weather_preserves_recursive_MUTEX
	.word	weather_preserves
	.word	GuiVar_ETGagePercentFull
	.word	1213
	.word	GuiVar_ETGageSkipTonight
.LFE40:
	.size	WEATHER_extract_and_store_et_gage_changes_from_GuiVars, .-WEATHER_extract_and_store_et_gage_changes_from_GuiVars
	.section	.text.WEATHER_build_data_to_send,"ax",%progbits
	.align	2
	.global	WEATHER_build_data_to_send
	.type	WEATHER_build_data_to_send, %function
WEATHER_build_data_to_send:
.LFB32:
	@ args = 4, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI90:
	mov	r4, r0
	sub	sp, sp, #44
.LCFI91:
	mov	r0, r3
	mov	r6, r1
	mov	r5, r2
	mov	r8, r3
	ldr	fp, [sp, #80]
	bl	WEATHER_get_change_bits_ptr
	subs	sl, r0, #0
	beq	.L274
	ldr	r3, [r5, #0]
	cmp	r6, fp
	movcs	ip, #0
	movcc	ip, #1
	cmp	r3, #1
	movne	ip, #0
	cmp	ip, #0
	moveq	sl, ip
	beq	.L274
	mov	r1, #4
	mov	ip, #0
	mov	r2, r1
	add	r3, sp, #40
	mov	r0, r4
	str	ip, [sp, #36]
	str	ip, [sp, #28]
	bl	PDATA_copy_bitfield_info_into_pucp
	ldr	r3, .L278
	mov	r1, #400
	ldr	r2, .L278+4
	ldr	r7, .L278+8
	add	r9, r7, #320
	str	r0, [sp, #32]
	ldr	r0, [r3, #0]
	ldr	r3, .L278+12
	bl	xQueueTakeMutexRecursive_debug
	mov	r2, #4
	str	r2, [sp, #8]
	ldr	r2, [sp, #32]
	ldr	ip, [sp, #28]
	add	r3, r2, r6
	str	r3, [sp, #12]
	mov	r2, ip
	mov	r3, sl
	add	r1, sp, #36
	mov	r0, r4
	str	r7, [sp, #4]
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r3, [sp, #32]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #1
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, r0, r3
	add	r3, r7, #4
	str	r3, [sp, #4]
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #8
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #36
	mov	r2, #2
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #12
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #3
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #16
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #36
	mov	r2, #4
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #20
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #5
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #24
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #36
	mov	r2, #6
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #28
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #7
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #32
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #36
	mov	r2, #8
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #36
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #9
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #40
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #36
	mov	r2, #10
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #44
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #11
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #48
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #36
	mov	r2, #12
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #52
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #13
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #60
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #36
	mov	r2, #15
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #68
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #17
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #72
	str	r3, [sp, #4]
	mov	r3, #48
	str	r3, [sp, #8]
	add	r1, sp, #36
	mov	r2, #18
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #120
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #19
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #124
	str	r3, [sp, #4]
	mov	r3, #4
	str	r3, [sp, #8]
	add	r1, sp, #36
	mov	r2, #20
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #128
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #21
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #132
	str	r3, [sp, #4]
	add	r1, sp, #36
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	str	ip, [sp, #32]
	ldr	r2, [sp, #32]
	mov	ip, #24
	add	r3, r2, r6
	str	r3, [sp, #12]
	mov	r2, #22
	mov	r3, sl
	mov	r0, r4
	str	ip, [sp, #8]
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r3, [sp, #32]
	ldr	ip, [sp, #28]
	add	r1, sp, #36
	mov	r2, #23
	str	ip, [sp, #8]
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	r0, r3, r0
	add	r3, r7, #156
	str	r3, [sp, #4]
	add	r3, r0, r6
	str	r0, [sp, #32]
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r3, [sp, #32]
	ldr	ip, [sp, #28]
	add	r1, sp, #36
	mov	r2, ip
	str	ip, [sp, #8]
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	r0, r3, r0
	add	r3, r7, #180
	str	r3, [sp, #4]
	add	r3, r0, r6
	str	r0, [sp, #32]
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r3, [sp, #32]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #25
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, r3, r0
	add	r3, r7, #204
	str	r3, [sp, #4]
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #208
	str	r3, [sp, #4]
	mov	r3, #48
	str	r3, [sp, #8]
	add	r1, sp, #36
	mov	r2, #26
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	add	r3, r7, #256
	str	r3, [sp, #4]
	mov	r2, #4
	str	r2, [sp, #8]
	add	r1, sp, #36
	mov	r2, #27
	str	r9, [sp, #0]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	r7, r7, #260
	add	ip, ip, r0
	add	r3, ip, r6
	str	r3, [sp, #12]
	mov	r0, r4
	mov	r3, sl
	str	ip, [sp, #28]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	ip, [sp, #28]
	mov	r3, #48
	str	r3, [sp, #8]
	add	r1, sp, #36
	mov	r3, sl
	mov	r2, #28
	str	r9, [sp, #0]
	str	r7, [sp, #4]
	str	r5, [sp, #16]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	add	ip, ip, r0
	add	r6, ip, r6
	mov	r0, r4
	str	ip, [sp, #28]
	str	r6, [sp, #12]
	bl	PDATA_copy_var_into_pucp_and_set_32_bit_change_bits
	ldr	r3, [sp, #36]
	ldr	ip, [sp, #28]
	cmp	r3, #0
	add	sl, ip, r0
	beq	.L275
	ldr	r0, [sp, #40]
	add	r1, sp, #36
	mov	r2, #4
	bl	memcpy
	b	.L274
.L275:
	ldr	r3, [r4, #0]
	sub	sl, sl, #8
	sub	r3, r3, #8
	str	r3, [r4, #0]
.L274:
	ldr	r3, .L278
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, sl
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L279:
	.align	2
.L278:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	.LANCHOR0
	.word	462
.LFE32:
	.size	WEATHER_build_data_to_send, .-WEATHER_build_data_to_send
	.section	.text.nm_WEATHER_extract_and_store_changes_from_comm,"ax",%progbits
	.align	2
	.global	nm_WEATHER_extract_and_store_changes_from_comm
	.type	nm_WEATHER_extract_and_store_changes_from_comm, %function
nm_WEATHER_extract_and_store_changes_from_comm:
.LFB27:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI92:
	mov	r5, r0
	sub	sp, sp, #80
.LCFI93:
	mov	r0, r3
	mov	fp, r3
	mov	r6, r1
	mov	r7, r2
	bl	WEATHER_get_change_bits_ptr
	mov	r1, r5
	mov	r2, #4
	add	r4, r5, #8
	mov	r8, r0
	add	r0, sp, #68
	bl	memcpy
	add	r1, r5, #4
	add	r0, sp, #64
	mov	r2, #4
	bl	memcpy
	ldr	r3, [sp, #64]
	tst	r3, #1
	moveq	r5, #8
	beq	.L281
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
.LBB25:
	ldr	r0, .L318
	ldr	ip, .L318+4
	mov	r3, #1
	stmia	sp, {r0, r3, ip}
	ldr	r0, .L318+8
	mov	r3, #0
	str	r3, [sp, #16]
	str	r3, [sp, #32]
	ldr	r3, .L318+12
	str	r0, [sp, #28]
	str	r3, [sp, #36]
	sub	r0, r0, #316
	ldr	r1, [sp, #72]
	ldr	r2, .L318+16
	ldr	r3, .L318+20
	str	r6, [sp, #12]
	str	r7, [sp, #20]
	str	r8, [sp, #24]
	bl	SHARED_set_time_controller
.LBE25:
	add	r4, r5, #12
.LBB26:
	mov	r5, #12
.L281:
.LBE26:
	ldr	r3, [sp, #64]
	tst	r3, #2
	beq	.L282
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_rain_bucket_box_index
.L282:
	ldr	r3, [sp, #64]
	tst	r3, #4
	beq	.L283
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #76
	bl	memcpy
	ldr	r0, [sp, #76]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_rain_bucket_connected
.L283:
	ldr	r3, [sp, #64]
	tst	r3, #8
	beq	.L284
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_rain_bucket_minimum_inches
.L284:
	ldr	r3, [sp, #64]
	tst	r3, #16
	beq	.L285
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_rain_bucket_max_hourly
.L285:
	ldr	r3, [sp, #64]
	tst	r3, #32
	beq	.L286
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_rain_bucket_max_24_hour
.L286:
	ldr	r3, [sp, #64]
	tst	r3, #64
	beq	.L287
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_et_gage_box_index
.L287:
	ldr	r3, [sp, #64]
	tst	r3, #128
	beq	.L288
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #76
	bl	memcpy
	ldr	r0, [sp, #76]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_et_gage_connected
.L288:
	ldr	r3, [sp, #64]
	tst	r3, #256
	beq	.L289
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #76
	bl	memcpy
	ldr	r0, [sp, #76]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_et_gage_log_pulses
.L289:
	ldr	r3, [sp, #64]
	tst	r3, #512
	beq	.L290
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_percent_of_historical_et_cap
.L290:
	ldr	r3, [sp, #64]
	tst	r3, #1024
	beq	.L291
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
.LBB27:
	mov	r2, #0
	mov	r3, #1
	stmia	sp, {r2, r3}
	ldr	r3, .L318+24
	ldr	r0, .L318+8
	str	r3, [sp, #8]
	mov	r3, #10
	str	r3, [sp, #32]
	ldr	r3, .L318+28
	str	r0, [sp, #28]
	str	r3, [sp, #36]
	sub	r0, r0, #276
	ldr	r1, [sp, #72]
	mov	r3, #100
.LBE27:
	add	r4, r4, #4
	add	r5, r5, #4
.LBB28:
	str	r6, [sp, #12]
	str	r2, [sp, #16]
	str	r7, [sp, #20]
	str	r8, [sp, #24]
	bl	SHARED_set_uint32_controller
.L291:
.LBE28:
	ldr	r3, [sp, #64]
	tst	r3, #2048
	beq	.L292
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_wind_gage_box_index
.L292:
	ldr	r3, [sp, #64]
	tst	r3, #4096
	beq	.L293
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #76
	bl	memcpy
	ldr	r0, [sp, #76]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_wind_gage_connected
.L293:
	ldr	r3, [sp, #64]
	tst	r3, #8192
	beq	.L294
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	WEATHER_set_wind_pause_speed
.L294:
	ldr	r3, [sp, #64]
	tst	r3, #32768
	beq	.L295
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	WEATHER_set_wind_resume_speed
.L295:
	ldr	r3, [sp, #64]
	tst	r3, #131072
	beq	.L296
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #76
	bl	memcpy
	ldr	r0, [sp, #76]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_reference_et_use_your_own
.L296:
	ldr	r3, [sp, #64]
	tst	r3, #262144
	beq	.L297
	mov	r9, r4
	mov	sl, #0
.L298:
	mov	r1, r9
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	add	sl, sl, #1
	mov	r3, #0
	stmia	sp, {r3, r7, r8}
	ldr	r0, [sp, #72]
	mov	r1, sl
	mov	r2, #1
	mov	r3, r6
	bl	nm_WEATHER_set_reference_et_number
	cmp	sl, #12
	add	r9, r9, #4
	bne	.L298
	add	r4, r4, #48
	add	r5, r5, #48
.L297:
	ldr	r3, [sp, #64]
	tst	r3, #524288
	beq	.L299
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_reference_et_state_index
.L299:
	ldr	r3, [sp, #64]
	tst	r3, #1048576
	beq	.L300
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_reference_et_county_index
.L300:
	ldr	r3, [sp, #64]
	tst	r3, #2097152
	beq	.L301
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	ldr	r0, [sp, #72]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_reference_et_city_index
.L301:
	ldr	r3, [sp, #64]
	tst	r3, #4194304
	beq	.L302
	mov	r1, r4
	mov	r2, #24
	add	r0, sp, #40
	bl	memcpy
	add	r0, sp, #40
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #24
	add	r5, r5, #24
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_user_defined_state
.L302:
	ldr	r3, [sp, #64]
	tst	r3, #8388608
	beq	.L303
	mov	r1, r4
	mov	r2, #24
	add	r0, sp, #40
	bl	memcpy
	add	r0, sp, #40
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #24
	add	r5, r5, #24
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_user_defined_county
.L303:
	ldr	r3, [sp, #64]
	tst	r3, #16777216
	beq	.L304
	mov	r1, r4
	mov	r2, #24
	add	r0, sp, #40
	bl	memcpy
	add	r0, sp, #40
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #24
	add	r5, r5, #24
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_user_defined_city
.L304:
	ldr	r3, [sp, #64]
	tst	r3, #33554432
	beq	.L305
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #76
	bl	memcpy
	ldr	r0, [sp, #76]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_rain_switch_in_use
.L305:
	ldr	r3, [sp, #64]
	tst	r3, #67108864
	beq	.L306
	mov	r9, r4
	mov	sl, #0
.L307:
	mov	r1, r9
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	mov	r3, #0
	stmia	sp, {r3, r7, r8}
	ldr	r0, [sp, #72]
	mov	r1, sl
	mov	r2, #1
	mov	r3, r6
	add	sl, sl, #1
	bl	nm_WEATHER_set_rain_switch_connected
	cmp	sl, #12
	add	r9, r9, #4
	bne	.L307
	add	r4, r4, #48
	add	r5, r5, #48
.L306:
	ldr	r3, [sp, #64]
	tst	r3, #134217728
	beq	.L308
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #76
	bl	memcpy
	ldr	r0, [sp, #76]
	mov	r1, #1
	mov	r2, r6
	mov	r3, #0
	add	r4, r4, #4
	add	r5, r5, #4
	stmia	sp, {r7, r8}
	bl	nm_WEATHER_set_freeze_switch_in_use
.L308:
	ldr	r3, [sp, #64]
	tst	r3, #268435456
	beq	.L309
	mov	sl, #0
	mov	r9, sl
.L310:
	mov	r1, r4
	mov	r2, #4
	add	r0, sp, #72
	bl	memcpy
	mov	r1, sl
	ldr	r0, [sp, #72]
	mov	r2, #1
	mov	r3, r6
	add	sl, sl, #1
	str	r9, [sp, #0]
	stmib	sp, {r7, r8}
	bl	nm_WEATHER_set_freeze_switch_connected
	cmp	sl, #12
	add	r4, r4, #4
	bne	.L310
	add	r5, r5, #48
.L309:
	cmp	r5, #0
	beq	.L311
	cmp	fp, #1
	cmpne	fp, #15
	beq	.L312
	cmp	fp, #16
	bne	.L313
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	bne	.L313
.L312:
	mov	r0, #6
	mov	r1, #0
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	b	.L313
.L311:
	ldr	r0, .L318+32
	ldr	r1, .L318+36
	bl	Alert_bit_set_with_no_data_with_filename
.L313:
	mov	r0, r5
	add	sp, sp, #80
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L319:
	.align	2
.L318:
	.word	72000
	.word	61448
	.word	.LANCHOR0+316
	.word	.LC30
	.word	61200
	.word	82800
	.word	61485
	.word	.LC31
	.word	.LC27
	.word	2547
.LFE27:
	.size	nm_WEATHER_extract_and_store_changes_from_comm, .-nm_WEATHER_extract_and_store_changes_from_comm
	.section	.text.WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token,"ax",%progbits
	.align	2
	.global	WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token
	.type	WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token, %function
WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token:
.LFB70:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L321
	stmfd	sp!, {r4, lr}
.LCFI94:
	ldr	r4, .L321+4
	ldr	r1, [r3, #0]
	ldr	r0, .L321+8
	bl	SHARED_set_all_32_bit_change_bits
	ldr	r0, [r4, #0]
	mov	r1, #400
	ldr	r2, .L321+12
	ldr	r3, .L321+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L321+20
	ldr	r0, [r4, #0]
	mov	r2, #1
	str	r2, [r3, #444]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L322:
	.align	2
.L321:
	.word	weather_control_recursive_MUTEX
	.word	comm_mngr_recursive_MUTEX
	.word	.LANCHOR0+312
	.word	.LC32
	.word	2158
	.word	comm_mngr
.LFE70:
	.size	WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token, .-WEATHER_CONTROL_set_bits_on_all_settings_to_cause_distribution_in_the_next_token
	.section	.text.WEATHER_on_all_settings_set_or_clear_commserver_change_bits,"ax",%progbits
	.align	2
	.global	WEATHER_on_all_settings_set_or_clear_commserver_change_bits
	.type	WEATHER_on_all_settings_set_or_clear_commserver_change_bits, %function
WEATHER_on_all_settings_set_or_clear_commserver_change_bits:
.LFB71:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI95:
	ldr	r4, .L326
	mov	r5, r0
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L326+4
	ldr	r3, .L326+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r5, #51
	ldr	r0, .L326+12
	ldr	r1, [r4, #0]
	bne	.L324
	bl	SHARED_clear_all_32_bit_change_bits
	b	.L325
.L324:
	bl	SHARED_set_all_32_bit_change_bits
.L325:
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L327:
	.align	2
.L326:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	2168
	.word	.LANCHOR0+316
.LFE71:
	.size	WEATHER_on_all_settings_set_or_clear_commserver_change_bits, .-WEATHER_on_all_settings_set_or_clear_commserver_change_bits
	.section	.text.nm_WEATHER_update_pending_change_bits,"ax",%progbits
	.align	2
	.global	nm_WEATHER_update_pending_change_bits
	.type	nm_WEATHER_update_pending_change_bits, %function
nm_WEATHER_update_pending_change_bits:
.LFB72:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI96:
	ldr	r4, .L332
	ldr	r3, [r4, #320]
	cmp	r3, #0
	beq	.L328
	cmp	r0, #0
	ldr	r5, .L332+4
	beq	.L330
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L332+8
	ldr	r3, .L332+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r2, [r4, #320]
	ldr	r3, [r4, #316]
	ldr	r0, [r5, #0]
	orr	r3, r2, r3
	str	r3, [r4, #316]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L332+16
	mov	r2, #1
	str	r2, [r3, #112]
	mvn	r3, #0
	str	r3, [sp, #0]
	ldr	r3, .L332+20
	mov	r1, #2
	ldr	r0, [r3, #152]
	ldr	r2, .L332+24
	mov	r3, #0
	bl	xTimerGenericCommand
	b	.L331
.L330:
	add	r0, r4, #320
	ldr	r1, [r5, #0]
	bl	SHARED_clear_all_32_bit_change_bits
.L331:
	mov	r0, #6
	mov	r1, #0
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L328:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, pc}
.L333:
	.align	2
.L332:
	.word	.LANCHOR0
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	2202
	.word	weather_preserves
	.word	cics
	.word	60000
.LFE72:
	.size	nm_WEATHER_update_pending_change_bits, .-nm_WEATHER_update_pending_change_bits
	.section	.text.WEATHER_CONTROL_calculate_chain_sync_crc,"ax",%progbits
	.align	2
	.global	WEATHER_CONTROL_calculate_chain_sync_crc
	.type	WEATHER_CONTROL_calculate_chain_sync_crc, %function
WEATHER_CONTROL_calculate_chain_sync_crc:
.LFB73:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L337
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI97:
	ldr	r2, .L337+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L337+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #324
	mov	r1, sp
	ldr	r2, .L337+4
	ldr	r3, .L337+12
	bl	mem_oabia
	subs	r5, r0, #0
	beq	.L335
	ldr	r3, [sp, #0]
	add	r5, sp, #8
	str	r3, [r5, #-4]!
	ldr	r1, .L337+16
	mov	r0, r5
	mov	r2, #4
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+20
	mov	r2, #4
	add	r4, r4, #43
	mov	r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+24
	mov	r2, #4
	add	r6, r0, r6
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+28
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+32
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+36
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+40
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+44
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+48
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+52
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+56
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+60
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+64
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+68
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+72
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+76
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+80
	mov	r2, #48
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+84
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+88
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+92
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	add	r6, r6, r0
	ldr	r0, .L337+96
	bl	strlen
	ldr	r1, .L337+96
	mov	r2, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	add	r6, r6, r0
	ldr	r0, .L337+100
	bl	strlen
	ldr	r1, .L337+100
	mov	r2, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	add	r6, r6, r0
	ldr	r0, .L337+104
	bl	strlen
	ldr	r1, .L337+104
	mov	r2, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+108
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+112
	mov	r2, #48
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	ldr	r1, .L337+116
	mov	r2, #4
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r2, #48
	ldr	r1, .L337+120
	add	r6, r6, r0
	mov	r0, r5
	bl	CHAIN_SYNC_push_data_onto_block
	mov	r5, #1
	add	r1, r6, r0
	ldr	r0, [sp, #0]
	bl	CRC_calculate_32bit_big_endian
	ldr	r3, .L337+124
	ldr	r1, .L337+4
	ldr	r2, .L337+128
	str	r0, [r3, r4, asl #2]
	ldr	r0, [sp, #0]
	bl	mem_free_debug
	b	.L336
.L335:
	ldr	r3, .L337+132
	ldr	r0, .L337+136
	ldr	r1, [r3, r4, asl #4]
	bl	Alert_Message_va
.L336:
	ldr	r3, .L337
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
	ldmfd	sp!, {r2, r3, r4, r5, r6, pc}
.L338:
	.align	2
.L337:
	.word	weather_control_recursive_MUTEX
	.word	.LC32
	.word	2255
	.word	2267
	.word	.LANCHOR0
	.word	.LANCHOR0+4
	.word	.LANCHOR0+8
	.word	.LANCHOR0+12
	.word	.LANCHOR0+16
	.word	.LANCHOR0+20
	.word	.LANCHOR0+24
	.word	.LANCHOR0+28
	.word	.LANCHOR0+32
	.word	.LANCHOR0+36
	.word	.LANCHOR0+40
	.word	.LANCHOR0+44
	.word	.LANCHOR0+48
	.word	.LANCHOR0+52
	.word	.LANCHOR0+60
	.word	.LANCHOR0+68
	.word	.LANCHOR0+72
	.word	.LANCHOR0+120
	.word	.LANCHOR0+124
	.word	.LANCHOR0+128
	.word	.LANCHOR0+132
	.word	.LANCHOR0+156
	.word	.LANCHOR0+180
	.word	.LANCHOR0+204
	.word	.LANCHOR0+208
	.word	.LANCHOR0+256
	.word	.LANCHOR0+260
	.word	cscs
	.word	2371
	.word	chain_sync_file_pertinants
	.word	.LC36
.LFE73:
	.size	WEATHER_CONTROL_calculate_chain_sync_crc, .-WEATHER_CONTROL_calculate_chain_sync_crc
	.global	weather_control_item_sizes
	.global	WEATHER_GuiVar_box_indexes_with_dash_w_option
	.global	WEATHER_GuiVar_WindGageInstalledAtIndex
	.global	WEATHER_GuiVar_RainBucketInstalledAtIndex
	.global	WEATHER_GuiVar_ETGageInstalledAtIndex
	.global	WEATHER_CONTROL_FILENAME
	.section	.bss.WEATHER_GuiVar_ETGageInstalledAtIndex,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	WEATHER_GuiVar_ETGageInstalledAtIndex, %object
	.size	WEATHER_GuiVar_ETGageInstalledAtIndex, 4
WEATHER_GuiVar_ETGageInstalledAtIndex:
	.space	4
	.section	.bss.WEATHER_GuiVar_box_indexes_with_dash_w_option,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	WEATHER_GuiVar_box_indexes_with_dash_w_option, %object
	.size	WEATHER_GuiVar_box_indexes_with_dash_w_option, 48
WEATHER_GuiVar_box_indexes_with_dash_w_option:
	.space	48
	.section	.rodata.STATE_DEFAULT_NAME,"a",%progbits
	.set	.LANCHOR1,. + 0
	.type	STATE_DEFAULT_NAME, %object
	.size	STATE_DEFAULT_NAME, 20
STATE_DEFAULT_NAME:
	.ascii	"(state not yet set)\000"
	.section	.bss.weather_control,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	weather_control, %object
	.size	weather_control, 324
weather_control:
	.space	324
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"WindGageBoxIndex\000"
.LC1:
	.ascii	"RainBucketMax24Hour\000"
.LC2:
	.ascii	"RainBucketMaxHourly\000"
.LC3:
	.ascii	"RainBucketMinInches\000"
.LC4:
	.ascii	"RainBucketBoxIndex\000"
.LC5:
	.ascii	"ETGageBoxIndex\000"
.LC6:
	.ascii	"ETG_HistCapPercent\000"
.LC7:
	.ascii	"ET_StateIndex\000"
.LC8:
	.ascii	"ET_CountyIndex\000"
.LC9:
	.ascii	"ET_CityIndex\000"
.LC10:
	.ascii	"UseYourOwnET\000"
.LC11:
	.ascii	"FreezeSwitch_isInUse\000"
.LC12:
	.ascii	"RainSwitch_IsInUse\000"
.LC13:
	.ascii	"WindGage_isInUse\000"
.LC14:
	.ascii	"RainBucket_isInUse\000"
.LC15:
	.ascii	"ETG_LogPulses\000"
.LC16:
	.ascii	"ETG_isInUse\000"
.LC17:
	.ascii	"UserDefinedState\000"
.LC18:
	.ascii	"UserDefinedCounty\000"
.LC19:
	.ascii	"UserDefinedCity\000"
.LC20:
	.ascii	"WindPauseMPG\000"
.LC21:
	.ascii	"WindResumeMPG\000"
.LC22:
	.ascii	"W CONTROL file unexpd update %u\000"
.LC23:
	.ascii	"WEATHER CONTROL file update : to revision %u from %"
	.ascii	"u\000"
.LC24:
	.ascii	"W CONTROL updater error\000"
.LC25:
	.ascii	"%s%d\000"
.LC26:
	.ascii	"ETUserMonth\000"
.LC27:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/shared_weather_control.c\000"
.LC28:
	.ascii	"HasFreezeSwitch\000"
.LC29:
	.ascii	"HasRainSwitch\000"
.LC30:
	.ascii	"ET_RainTableRollTime\000"
.LC31:
	.ascii	"ETG_HistMinPercent\000"
.LC32:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/weather_control.c\000"
.LC33:
	.ascii	"%c\000"
.LC34:
	.ascii	"Reference ET City\000"
.LC35:
	.ascii	"ET roll time\000"
.LC36:
	.ascii	"SYNC: no mem to calc checksum %s\000"
	.section	.rodata.CITY_DEFAULT_NAME,"a",%progbits
	.set	.LANCHOR3,. + 0
	.type	CITY_DEFAULT_NAME, %object
	.size	CITY_DEFAULT_NAME, 19
CITY_DEFAULT_NAME:
	.ascii	"(city not yet set)\000"
	.section	.rodata.weather_control_item_sizes,"a",%progbits
	.align	2
	.type	weather_control_item_sizes, %object
	.size	weather_control_item_sizes, 16
weather_control_item_sizes:
	.word	320
	.word	320
	.word	324
	.space	4
	.section	.bss.WEATHER_GuiVar_RainBucketInstalledAtIndex,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	WEATHER_GuiVar_RainBucketInstalledAtIndex, %object
	.size	WEATHER_GuiVar_RainBucketInstalledAtIndex, 4
WEATHER_GuiVar_RainBucketInstalledAtIndex:
	.space	4
	.section	.bss.WEATHER_GuiVar_WindGageInstalledAtIndex,"aw",%nobits
	.align	2
	.set	.LANCHOR8,. + 0
	.type	WEATHER_GuiVar_WindGageInstalledAtIndex, %object
	.size	WEATHER_GuiVar_WindGageInstalledAtIndex, 4
WEATHER_GuiVar_WindGageInstalledAtIndex:
	.space	4
	.section	.rodata.COUNTY_DEFAULT_NAME,"a",%progbits
	.set	.LANCHOR2,. + 0
	.type	COUNTY_DEFAULT_NAME, %object
	.size	COUNTY_DEFAULT_NAME, 21
COUNTY_DEFAULT_NAME:
	.ascii	"(county not yet set)\000"
	.section	.rodata.WEATHER_CONTROL_FILENAME,"a",%progbits
	.set	.LANCHOR4,. + 0
	.type	WEATHER_CONTROL_FILENAME, %object
	.size	WEATHER_CONTROL_FILENAME, 16
WEATHER_CONTROL_FILENAME:
	.ascii	"WEATHER_CONTROL\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI0-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI2-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI6-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI8-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI10-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI12-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.byte	0x4
	.4byte	.LCFI14-.LFB17
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI15-.LCFI14
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI16-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI18-.LFB19
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI20-.LFB15
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI21-.LFB25
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI22-.LFB23
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI23-.LFB12
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI24-.LFB2
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI25-.LFB8
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI26-.LFB7
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI27-.LFB20
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.byte	0x4
	.4byte	.LCFI28-.LFB21
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI29-.LFB22
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x83
	.uleb128 0x6
	.byte	0x82
	.uleb128 0x7
	.byte	0x81
	.uleb128 0x8
	.byte	0x80
	.uleb128 0x9
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI30-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI32-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI33-.LCFI32
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.byte	0x4
	.4byte	.LCFI34-.LFB28
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI35-.LFB16
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI36-.LCFI35
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.byte	0x4
	.4byte	.LCFI37-.LFB26
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI38-.LCFI37
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.byte	0x4
	.4byte	.LCFI39-.LFB24
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI40-.LCFI39
	.byte	0xe
	.uleb128 0x58
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI41-.LFB31
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI42-.LCFI41
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI43-.LFB29
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.byte	0x4
	.4byte	.LCFI44-.LFB30
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x82
	.uleb128 0x2
	.byte	0x81
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI45-.LFB33
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI46-.LFB34
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI47-.LFB35
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.byte	0x4
	.4byte	.LCFI48-.LFB36
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI49-.LFB37
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI50-.LFB38
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI51-.LFB39
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI52-.LCFI51
	.byte	0xe
	.uleb128 0x20
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x8
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.byte	0x4
	.4byte	.LCFI53-.LFB46
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.byte	0x4
	.4byte	.LCFI54-.LFB47
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.byte	0x4
	.4byte	.LCFI55-.LFB48
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.byte	0x4
	.4byte	.LCFI56-.LFB49
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.byte	0x4
	.4byte	.LCFI57-.LFB50
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE80:
.LSFDE82:
	.4byte	.LEFDE82-.LASFDE82
.LASFDE82:
	.4byte	.Lframe0
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.byte	0x4
	.4byte	.LCFI58-.LFB51
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE82:
.LSFDE84:
	.4byte	.LEFDE84-.LASFDE84
.LASFDE84:
	.4byte	.Lframe0
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.byte	0x4
	.4byte	.LCFI59-.LFB52
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE84:
.LSFDE86:
	.4byte	.LEFDE86-.LASFDE86
.LASFDE86:
	.4byte	.Lframe0
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.byte	0x4
	.4byte	.LCFI60-.LFB53
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE86:
.LSFDE88:
	.4byte	.LEFDE88-.LASFDE88
.LASFDE88:
	.4byte	.Lframe0
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.byte	0x4
	.4byte	.LCFI61-.LFB54
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE88:
.LSFDE90:
	.4byte	.LEFDE90-.LASFDE90
.LASFDE90:
	.4byte	.Lframe0
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.byte	0x4
	.4byte	.LCFI62-.LFB55
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE90:
.LSFDE92:
	.4byte	.LEFDE92-.LASFDE92
.LASFDE92:
	.4byte	.Lframe0
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.byte	0x4
	.4byte	.LCFI63-.LFB56
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE92:
.LSFDE94:
	.4byte	.LEFDE94-.LASFDE94
.LASFDE94:
	.4byte	.Lframe0
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.byte	0x4
	.4byte	.LCFI64-.LFB57
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE94:
.LSFDE96:
	.4byte	.LEFDE96-.LASFDE96
.LASFDE96:
	.4byte	.Lframe0
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.byte	0x4
	.4byte	.LCFI65-.LFB58
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE96:
.LSFDE98:
	.4byte	.LEFDE98-.LASFDE98
.LASFDE98:
	.4byte	.Lframe0
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.byte	0x4
	.4byte	.LCFI66-.LFB59
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI67-.LCFI66
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE98:
.LSFDE100:
	.4byte	.LEFDE100-.LASFDE100
.LASFDE100:
	.4byte	.Lframe0
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.byte	0x4
	.4byte	.LCFI68-.LFB60
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI69-.LCFI68
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE100:
.LSFDE102:
	.4byte	.LEFDE102-.LASFDE102
.LASFDE102:
	.4byte	.Lframe0
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.byte	0x4
	.4byte	.LCFI70-.LFB61
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE102:
.LSFDE104:
	.4byte	.LEFDE104-.LASFDE104
.LASFDE104:
	.4byte	.Lframe0
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.byte	0x4
	.4byte	.LCFI71-.LFB62
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI72-.LCFI71
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE104:
.LSFDE106:
	.4byte	.LEFDE106-.LASFDE106
.LASFDE106:
	.4byte	.Lframe0
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.byte	0x4
	.4byte	.LCFI73-.LFB63
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE106:
.LSFDE108:
	.4byte	.LEFDE108-.LASFDE108
.LASFDE108:
	.4byte	.Lframe0
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.byte	0x4
	.4byte	.LCFI74-.LFB64
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE108:
.LSFDE110:
	.4byte	.LEFDE110-.LASFDE110
.LASFDE110:
	.4byte	.Lframe0
	.4byte	.LFB65
	.4byte	.LFE65-.LFB65
	.byte	0x4
	.4byte	.LCFI75-.LFB65
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE110:
.LSFDE112:
	.4byte	.LEFDE112-.LASFDE112
.LASFDE112:
	.4byte	.Lframe0
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.byte	0x4
	.4byte	.LCFI76-.LFB66
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI77-.LCFI76
	.byte	0xe
	.uleb128 0x54
	.align	2
.LEFDE112:
.LSFDE114:
	.4byte	.LEFDE114-.LASFDE114
.LASFDE114:
	.4byte	.Lframe0
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.byte	0x4
	.4byte	.LCFI78-.LFB67
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE114:
.LSFDE116:
	.4byte	.LEFDE116-.LASFDE116
.LASFDE116:
	.4byte	.Lframe0
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.byte	0x4
	.4byte	.LCFI79-.LFB68
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x83
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE116:
.LSFDE118:
	.4byte	.LEFDE118-.LASFDE118
.LASFDE118:
	.4byte	.Lframe0
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.align	2
.LEFDE118:
.LSFDE120:
	.4byte	.LEFDE120-.LASFDE120
.LASFDE120:
	.4byte	.Lframe0
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.byte	0x4
	.4byte	.LCFI80-.LFB45
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI81-.LCFI80
	.byte	0xe
	.uleb128 0x1c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI82-.LCFI81
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE120:
.LSFDE122:
	.4byte	.LEFDE122-.LASFDE122
.LASFDE122:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.byte	0x4
	.4byte	.LCFI83-.LFB44
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE122:
.LSFDE124:
	.4byte	.LEFDE124-.LASFDE124
.LASFDE124:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI84-.LFB43
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE124:
.LSFDE126:
	.4byte	.LEFDE126-.LASFDE126
.LASFDE126:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.byte	0x4
	.4byte	.LCFI85-.LFB42
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE126:
.LSFDE128:
	.4byte	.LEFDE128-.LASFDE128
.LASFDE128:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI86-.LFB41
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI87-.LCFI86
	.byte	0xe
	.uleb128 0x1c
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI88-.LCFI87
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE128:
.LSFDE130:
	.4byte	.LEFDE130-.LASFDE130
.LASFDE130:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI89-.LFB40
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x81
	.uleb128 0x6
	.byte	0x80
	.uleb128 0x7
	.align	2
.LEFDE130:
.LSFDE132:
	.4byte	.LEFDE132-.LASFDE132
.LASFDE132:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.byte	0x4
	.4byte	.LCFI90-.LFB32
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI91-.LCFI90
	.byte	0xe
	.uleb128 0x50
	.align	2
.LEFDE132:
.LSFDE134:
	.4byte	.LEFDE134-.LASFDE134
.LASFDE134:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI92-.LFB27
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI93-.LCFI92
	.byte	0xe
	.uleb128 0x74
	.align	2
.LEFDE134:
.LSFDE136:
	.4byte	.LEFDE136-.LASFDE136
.LASFDE136:
	.4byte	.Lframe0
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.byte	0x4
	.4byte	.LCFI94-.LFB70
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE136:
.LSFDE138:
	.4byte	.LEFDE138-.LASFDE138
.LASFDE138:
	.4byte	.Lframe0
	.4byte	.LFB71
	.4byte	.LFE71-.LFB71
	.byte	0x4
	.4byte	.LCFI95-.LFB71
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE138:
.LSFDE140:
	.4byte	.LEFDE140-.LASFDE140
.LASFDE140:
	.4byte	.Lframe0
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.byte	0x4
	.4byte	.LCFI96-.LFB72
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE140:
.LSFDE142:
	.4byte	.LEFDE142-.LASFDE142
.LASFDE142:
	.4byte	.Lframe0
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.byte	0x4
	.4byte	.LCFI97-.LFB73
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE142:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/shared_weather_control.c"
	.file 2 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/weather_control.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x616
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF74
	.byte	0x1
	.4byte	.LASF75
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x52e
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x7d3
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x756
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x10b
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x380
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x2
	.2byte	0x583
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x3c1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x24a
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST1
	.uleb128 0x4
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x20a
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1ca
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x14c
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x28b
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x349
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST6
	.uleb128 0x4
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x58d
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LLST7
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x5d9
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST8
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x624
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST9
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x4e2
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST10
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x79c
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST11
	.uleb128 0x4
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x71f
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST12
	.uleb128 0x4
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x401
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST13
	.uleb128 0x4
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x18c
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST14
	.uleb128 0x4
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x30a
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST15
	.uleb128 0x4
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x2cb
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST16
	.uleb128 0x4
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x66f
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST17
	.uleb128 0x4
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x6ac
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LLST18
	.uleb128 0x4
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x6e9
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST19
	.uleb128 0x4
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x446
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST20
	.uleb128 0x4
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x497
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST21
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x2
	.byte	0xb7
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LLST22
	.uleb128 0x7
	.4byte	0x21
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST23
	.uleb128 0x7
	.4byte	0x2a
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LLST24
	.uleb128 0x7
	.4byte	0x34
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LLST25
	.uleb128 0x4
	.4byte	.LASF28
	.byte	0x2
	.2byte	0x127
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST26
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF30
	.byte	0x2
	.2byte	0x101
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST27
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF31
	.byte	0x2
	.2byte	0x10f
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LLST28
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF32
	.byte	0x2
	.2byte	0x35f
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST29
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF33
	.byte	0x2
	.2byte	0x39d
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST30
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF34
	.byte	0x2
	.2byte	0x3d2
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST31
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF35
	.byte	0x2
	.2byte	0x40b
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LLST32
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF36
	.byte	0x2
	.2byte	0x439
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST33
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF37
	.byte	0x2
	.2byte	0x452
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST34
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF38
	.byte	0x2
	.2byte	0x46b
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST35
	.uleb128 0x7
	.4byte	0x50
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LLST36
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF39
	.byte	0x2
	.2byte	0x5ae
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LLST37
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF40
	.byte	0x2
	.2byte	0x5c2
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LLST38
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF41
	.byte	0x2
	.2byte	0x5df
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LLST39
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF42
	.byte	0x2
	.2byte	0x5f1
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LLST40
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF43
	.byte	0x2
	.2byte	0x605
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LLST41
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF44
	.byte	0x2
	.2byte	0x623
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LLST42
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF45
	.byte	0x2
	.2byte	0x637
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LLST43
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF46
	.byte	0x2
	.2byte	0x64b
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LLST44
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF47
	.byte	0x2
	.2byte	0x65f
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LLST45
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF48
	.byte	0x2
	.2byte	0x685
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LLST46
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF49
	.byte	0x2
	.2byte	0x6a3
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LLST47
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF50
	.byte	0x2
	.2byte	0x6c1
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LLST48
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF51
	.byte	0x2
	.2byte	0x6ed
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LLST49
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF52
	.byte	0x2
	.2byte	0x70a
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LLST50
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF53
	.byte	0x2
	.2byte	0x75a
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LLST51
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF54
	.byte	0x2
	.2byte	0x778
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LLST52
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF55
	.byte	0x2
	.2byte	0x7a3
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LLST53
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF56
	.byte	0x2
	.2byte	0x7bc
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LLST54
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF57
	.byte	0x2
	.2byte	0x7dc
	.4byte	.LFB65
	.4byte	.LFE65
	.4byte	.LLST55
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF58
	.byte	0x2
	.2byte	0x7fe
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LLST56
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF59
	.byte	0x2
	.2byte	0x82e
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	.LLST57
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF60
	.byte	0x2
	.2byte	0x84d
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LLST58
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF61
	.byte	0x2
	.2byte	0x85b
	.4byte	.LFB69
	.4byte	.LFE69
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF62
	.byte	0x2
	.2byte	0x55a
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LLST59
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF63
	.byte	0x2
	.2byte	0x53e
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LLST60
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF64
	.byte	0x2
	.2byte	0x521
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST61
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF65
	.byte	0x2
	.2byte	0x4fe
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LLST62
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF66
	.byte	0x2
	.2byte	0x4da
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST63
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF67
	.byte	0x2
	.2byte	0x4ad
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST64
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF68
	.byte	0x2
	.2byte	0x19e
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LLST65
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF69
	.byte	0x1
	.2byte	0x817
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST66
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF70
	.byte	0x2
	.2byte	0x861
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	.LLST67
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF71
	.byte	0x2
	.2byte	0x876
	.4byte	.LFB71
	.4byte	.LFE71
	.4byte	.LLST68
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF72
	.byte	0x2
	.2byte	0x88f
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LLST69
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF73
	.byte	0x2
	.2byte	0x8ba
	.4byte	.LFB73
	.4byte	.LFE73
	.4byte	.LLST70
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB11
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB5
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI5
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI7
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB1
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI9
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB6
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI11
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB9
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI13
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB17
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI15
	.4byte	.LFE17
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB18
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI17
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB19
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB15
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB25
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB23
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB12
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB2
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB8
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB7
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB20
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB21
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LFE21
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB22
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB13
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI31
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB14
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI33
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB28
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LFE28
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB16
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI35
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI36
	.4byte	.LFE16
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB26
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LCFI38
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI38
	.4byte	.LFE26
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB24
	.4byte	.LCFI39
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI39
	.4byte	.LCFI40
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI40
	.4byte	.LFE24
	.2byte	0x3
	.byte	0x7d
	.sleb128 88
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB31
	.4byte	.LCFI41
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI41
	.4byte	.LCFI42
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI42
	.4byte	.LFE31
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB29
	.4byte	.LCFI43
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI43
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB30
	.4byte	.LCFI44
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI44
	.4byte	.LFE30
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB33
	.4byte	.LCFI45
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI45
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB34
	.4byte	.LCFI46
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI46
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB35
	.4byte	.LCFI47
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI47
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB36
	.4byte	.LCFI48
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI48
	.4byte	.LFE36
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB37
	.4byte	.LCFI49
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI49
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST34:
	.4byte	.LFB38
	.4byte	.LCFI50
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI50
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST35:
	.4byte	.LFB39
	.4byte	.LCFI51
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI51
	.4byte	.LCFI52
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI52
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST36:
	.4byte	.LFB46
	.4byte	.LCFI53
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI53
	.4byte	.LFE46
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST37:
	.4byte	.LFB47
	.4byte	.LCFI54
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI54
	.4byte	.LFE47
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST38:
	.4byte	.LFB48
	.4byte	.LCFI55
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI55
	.4byte	.LFE48
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST39:
	.4byte	.LFB49
	.4byte	.LCFI56
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI56
	.4byte	.LFE49
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST40:
	.4byte	.LFB50
	.4byte	.LCFI57
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI57
	.4byte	.LFE50
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST41:
	.4byte	.LFB51
	.4byte	.LCFI58
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI58
	.4byte	.LFE51
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST42:
	.4byte	.LFB52
	.4byte	.LCFI59
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI59
	.4byte	.LFE52
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST43:
	.4byte	.LFB53
	.4byte	.LCFI60
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI60
	.4byte	.LFE53
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST44:
	.4byte	.LFB54
	.4byte	.LCFI61
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI61
	.4byte	.LFE54
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST45:
	.4byte	.LFB55
	.4byte	.LCFI62
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI62
	.4byte	.LFE55
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST46:
	.4byte	.LFB56
	.4byte	.LCFI63
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI63
	.4byte	.LFE56
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST47:
	.4byte	.LFB57
	.4byte	.LCFI64
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI64
	.4byte	.LFE57
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST48:
	.4byte	.LFB58
	.4byte	.LCFI65
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI65
	.4byte	.LFE58
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST49:
	.4byte	.LFB59
	.4byte	.LCFI66
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI66
	.4byte	.LCFI67
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI67
	.4byte	.LFE59
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST50:
	.4byte	.LFB60
	.4byte	.LCFI68
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI68
	.4byte	.LCFI69
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI69
	.4byte	.LFE60
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST51:
	.4byte	.LFB61
	.4byte	.LCFI70
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI70
	.4byte	.LFE61
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST52:
	.4byte	.LFB62
	.4byte	.LCFI71
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI71
	.4byte	.LCFI72
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI72
	.4byte	.LFE62
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST53:
	.4byte	.LFB63
	.4byte	.LCFI73
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI73
	.4byte	.LFE63
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST54:
	.4byte	.LFB64
	.4byte	.LCFI74
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI74
	.4byte	.LFE64
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST55:
	.4byte	.LFB65
	.4byte	.LCFI75
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI75
	.4byte	.LFE65
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST56:
	.4byte	.LFB66
	.4byte	.LCFI76
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI76
	.4byte	.LCFI77
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI77
	.4byte	.LFE66
	.2byte	0x3
	.byte	0x7d
	.sleb128 84
	.4byte	0
	.4byte	0
.LLST57:
	.4byte	.LFB67
	.4byte	.LCFI78
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI78
	.4byte	.LFE67
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST58:
	.4byte	.LFB68
	.4byte	.LCFI79
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI79
	.4byte	.LFE68
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST59:
	.4byte	.LFB45
	.4byte	.LCFI80
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI80
	.4byte	.LCFI81
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI81
	.4byte	.LCFI82
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI82
	.4byte	.LFE45
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST60:
	.4byte	.LFB44
	.4byte	.LCFI83
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI83
	.4byte	.LFE44
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST61:
	.4byte	.LFB43
	.4byte	.LCFI84
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI84
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST62:
	.4byte	.LFB42
	.4byte	.LCFI85
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI85
	.4byte	.LFE42
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST63:
	.4byte	.LFB41
	.4byte	.LCFI86
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI86
	.4byte	.LCFI87
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI87
	.4byte	.LCFI88
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI88
	.4byte	.LFE41
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST64:
	.4byte	.LFB40
	.4byte	.LCFI89
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI89
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST65:
	.4byte	.LFB32
	.4byte	.LCFI90
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI90
	.4byte	.LCFI91
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI91
	.4byte	.LFE32
	.2byte	0x3
	.byte	0x7d
	.sleb128 80
	.4byte	0
	.4byte	0
.LLST66:
	.4byte	.LFB27
	.4byte	.LCFI92
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI92
	.4byte	.LCFI93
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI93
	.4byte	.LFE27
	.2byte	0x3
	.byte	0x7d
	.sleb128 116
	.4byte	0
	.4byte	0
.LLST67:
	.4byte	.LFB70
	.4byte	.LCFI94
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI94
	.4byte	.LFE70
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST68:
	.4byte	.LFB71
	.4byte	.LCFI95
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI95
	.4byte	.LFE71
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST69:
	.4byte	.LFB72
	.4byte	.LCFI96
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI96
	.4byte	.LFE72
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST70:
	.4byte	.LFB73
	.4byte	.LCFI97
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI97
	.4byte	.LFE73
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x254
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB21
	.4byte	.LFE21-.LFB21
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB36
	.4byte	.LFE36-.LFB36
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB46
	.4byte	.LFE46-.LFB46
	.4byte	.LFB47
	.4byte	.LFE47-.LFB47
	.4byte	.LFB48
	.4byte	.LFE48-.LFB48
	.4byte	.LFB49
	.4byte	.LFE49-.LFB49
	.4byte	.LFB50
	.4byte	.LFE50-.LFB50
	.4byte	.LFB51
	.4byte	.LFE51-.LFB51
	.4byte	.LFB52
	.4byte	.LFE52-.LFB52
	.4byte	.LFB53
	.4byte	.LFE53-.LFB53
	.4byte	.LFB54
	.4byte	.LFE54-.LFB54
	.4byte	.LFB55
	.4byte	.LFE55-.LFB55
	.4byte	.LFB56
	.4byte	.LFE56-.LFB56
	.4byte	.LFB57
	.4byte	.LFE57-.LFB57
	.4byte	.LFB58
	.4byte	.LFE58-.LFB58
	.4byte	.LFB59
	.4byte	.LFE59-.LFB59
	.4byte	.LFB60
	.4byte	.LFE60-.LFB60
	.4byte	.LFB61
	.4byte	.LFE61-.LFB61
	.4byte	.LFB62
	.4byte	.LFE62-.LFB62
	.4byte	.LFB63
	.4byte	.LFE63-.LFB63
	.4byte	.LFB64
	.4byte	.LFE64-.LFB64
	.4byte	.LFB65
	.4byte	.LFE65-.LFB65
	.4byte	.LFB66
	.4byte	.LFE66-.LFB66
	.4byte	.LFB67
	.4byte	.LFE67-.LFB67
	.4byte	.LFB68
	.4byte	.LFE68-.LFB68
	.4byte	.LFB69
	.4byte	.LFE69-.LFB69
	.4byte	.LFB45
	.4byte	.LFE45-.LFB45
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB70
	.4byte	.LFE70-.LFB70
	.4byte	.LFB71
	.4byte	.LFE71-.LFB71
	.4byte	.LFB72
	.4byte	.LFE72-.LFB72
	.4byte	.LFB73
	.4byte	.LFE73-.LFB73
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB21
	.4byte	.LFE21
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB36
	.4byte	.LFE36
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB46
	.4byte	.LFE46
	.4byte	.LFB47
	.4byte	.LFE47
	.4byte	.LFB48
	.4byte	.LFE48
	.4byte	.LFB49
	.4byte	.LFE49
	.4byte	.LFB50
	.4byte	.LFE50
	.4byte	.LFB51
	.4byte	.LFE51
	.4byte	.LFB52
	.4byte	.LFE52
	.4byte	.LFB53
	.4byte	.LFE53
	.4byte	.LFB54
	.4byte	.LFE54
	.4byte	.LFB55
	.4byte	.LFE55
	.4byte	.LFB56
	.4byte	.LFE56
	.4byte	.LFB57
	.4byte	.LFE57
	.4byte	.LFB58
	.4byte	.LFE58
	.4byte	.LFB59
	.4byte	.LFE59
	.4byte	.LFB60
	.4byte	.LFE60
	.4byte	.LFB61
	.4byte	.LFE61
	.4byte	.LFB62
	.4byte	.LFE62
	.4byte	.LFB63
	.4byte	.LFE63
	.4byte	.LFB64
	.4byte	.LFE64
	.4byte	.LFB65
	.4byte	.LFE65
	.4byte	.LFB66
	.4byte	.LFE66
	.4byte	.LFB67
	.4byte	.LFE67
	.4byte	.LFB68
	.4byte	.LFE68
	.4byte	.LFB69
	.4byte	.LFE69
	.4byte	.LFB45
	.4byte	.LFE45
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB70
	.4byte	.LFE70
	.4byte	.LFB71
	.4byte	.LFE71
	.4byte	.LFB72
	.4byte	.LFE72
	.4byte	.LFB73
	.4byte	.LFE73
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF7:
	.ascii	"nm_WEATHER_set_rain_bucket_max_24_hour\000"
.LASF35:
	.ascii	"WEATHER_copy_wind_gage_settings_into_GuiVars\000"
.LASF20:
	.ascii	"nm_WEATHER_set_et_gage_log_pulses\000"
.LASF64:
	.ascii	"WEATHER_extract_and_store_rain_switch_changes_from_"
	.ascii	"GuiVars\000"
.LASF59:
	.ascii	"WEATHER_get_reference_et_city_index\000"
.LASF24:
	.ascii	"nm_WEATHER_set_user_defined_city\000"
.LASF26:
	.ascii	"WEATHER_set_wind_resume_speed\000"
.LASF60:
	.ascii	"WEATHER_get_et_and_rain_table_roll_time\000"
.LASF11:
	.ascii	"nm_WEATHER_set_et_gage_box_index\000"
.LASF46:
	.ascii	"WEATHER_get_rain_bucket_max_24_hour_inches_100u\000"
.LASF36:
	.ascii	"WEATHER_copy_rain_switch_settings_into_GuiVars\000"
.LASF51:
	.ascii	"WEATHER_get_rain_switch_connected_to_this_controlle"
	.ascii	"r\000"
.LASF14:
	.ascii	"nm_WEATHER_set_reference_et_city_index\000"
.LASF23:
	.ascii	"nm_WEATHER_set_user_defined_county\000"
.LASF37:
	.ascii	"WEATHER_copy_freeze_switch_settings_into_GuiVars\000"
.LASF28:
	.ascii	"nm_weather_control_set_default_values\000"
.LASF49:
	.ascii	"WEATHER_get_rain_switch_in_use\000"
.LASF55:
	.ascii	"WEATHER_get_a_copy_of_the_wind_settings\000"
.LASF71:
	.ascii	"WEATHER_on_all_settings_set_or_clear_commserver_cha"
	.ascii	"nge_bits\000"
.LASF47:
	.ascii	"WEATHER_get_wind_gage_box_index\000"
.LASF10:
	.ascii	"nm_WEATHER_set_rain_bucket_box_index\000"
.LASF39:
	.ascii	"WEATHER_get_et_gage_box_index\000"
.LASF41:
	.ascii	"WEATHER_get_et_gage_log_pulses\000"
.LASF48:
	.ascii	"WEATHER_get_wind_gage_in_use\000"
.LASF61:
	.ascii	"WEATHER_get_change_bits_ptr\000"
.LASF2:
	.ascii	"nm_WEATHER_set_reference_et_number\000"
.LASF15:
	.ascii	"nm_WEATHER_set_reference_et_use_your_own\000"
.LASF16:
	.ascii	"nm_WEATHER_set_freeze_switch_in_use\000"
.LASF4:
	.ascii	"nm_WEATHER_set_percent_of_historical_et_min\000"
.LASF72:
	.ascii	"nm_WEATHER_update_pending_change_bits\000"
.LASF40:
	.ascii	"WEATHER_get_et_gage_is_in_use\000"
.LASF8:
	.ascii	"nm_WEATHER_set_rain_bucket_max_hourly\000"
.LASF21:
	.ascii	"nm_WEATHER_set_et_gage_connected\000"
.LASF6:
	.ascii	"nm_WEATHER_set_wind_gage_box_index\000"
.LASF27:
	.ascii	"nm_weather_control_updater\000"
.LASF22:
	.ascii	"nm_WEATHER_set_user_defined_state\000"
.LASF53:
	.ascii	"WEATHER_get_freeze_switch_in_use\000"
.LASF54:
	.ascii	"WEATHER_get_freeze_switch_connected_to_this_control"
	.ascii	"ler\000"
.LASF33:
	.ascii	"WEATHER_copy_et_gage_settings_into_GuiVars\000"
.LASF63:
	.ascii	"WEATHER_extract_and_store_freeze_switch_changes_fro"
	.ascii	"m_GuiVars\000"
.LASF58:
	.ascii	"WEATHER_get_reference_et_number\000"
.LASF42:
	.ascii	"WEATHER_get_rain_bucket_box_index\000"
.LASF32:
	.ascii	"FDTO_WEATHER_update_real_time_weather_GuiVars\000"
.LASF52:
	.ascii	"WEATHER_sw1_is_available_and_enabled_as_a_rain_swit"
	.ascii	"ch_at_this_controller\000"
.LASF65:
	.ascii	"WEATHER_extract_and_store_wind_gage_changes_from_Gu"
	.ascii	"iVars\000"
.LASF68:
	.ascii	"WEATHER_build_data_to_send\000"
.LASF70:
	.ascii	"WEATHER_CONTROL_set_bits_on_all_settings_to_cause_d"
	.ascii	"istribution_in_the_next_token\000"
.LASF57:
	.ascii	"WEATHER_get_reference_et_use_your_own\000"
.LASF44:
	.ascii	"WEATHER_get_rain_bucket_minimum_inches_100u\000"
.LASF43:
	.ascii	"WEATHER_get_rain_bucket_is_in_use\000"
.LASF3:
	.ascii	"nm_WEATHER_set_roll_time\000"
.LASF69:
	.ascii	"nm_WEATHER_extract_and_store_changes_from_comm\000"
.LASF45:
	.ascii	"WEATHER_get_rain_bucket_max_hourly_inches_100u\000"
.LASF31:
	.ascii	"save_file_weather_control\000"
.LASF62:
	.ascii	"WEATHER_extract_and_store_historical_et_changes_fro"
	.ascii	"m_GuiVars\000"
.LASF19:
	.ascii	"nm_WEATHER_set_rain_bucket_connected\000"
.LASF29:
	.ascii	"nm_WEATHER_set_percent_of_historical_et_cap\000"
.LASF5:
	.ascii	"WEATHER_there_is_a_weather_option_in_the_chain\000"
.LASF50:
	.ascii	"WEATHER_get_SW1_as_rain_switch_in_use\000"
.LASF38:
	.ascii	"WEATHER_copy_historical_et_settings_into_GuiVars\000"
.LASF18:
	.ascii	"nm_WEATHER_set_wind_gage_connected\000"
.LASF17:
	.ascii	"nm_WEATHER_set_rain_switch_in_use\000"
.LASF0:
	.ascii	"nm_WEATHER_set_freeze_switch_connected\000"
.LASF74:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF67:
	.ascii	"WEATHER_extract_and_store_et_gage_changes_from_GuiV"
	.ascii	"ars\000"
.LASF75:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/weather_control.c\000"
.LASF13:
	.ascii	"nm_WEATHER_set_reference_et_county_index\000"
.LASF1:
	.ascii	"nm_WEATHER_set_rain_switch_connected\000"
.LASF9:
	.ascii	"nm_WEATHER_set_rain_bucket_minimum_inches\000"
.LASF66:
	.ascii	"WEATHER_extract_and_store_rain_bucket_changes_from_"
	.ascii	"GuiVars\000"
.LASF25:
	.ascii	"WEATHER_set_wind_pause_speed\000"
.LASF56:
	.ascii	"WEATHER_get_percent_of_historical_cap_100u\000"
.LASF30:
	.ascii	"init_file_weather_control\000"
.LASF12:
	.ascii	"nm_WEATHER_set_reference_et_state_index\000"
.LASF73:
	.ascii	"WEATHER_CONTROL_calculate_chain_sync_crc\000"
.LASF34:
	.ascii	"WEATHER_copy_rain_bucket_settings_into_GuiVars\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
