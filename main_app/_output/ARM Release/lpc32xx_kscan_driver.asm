	.file	"lpc32xx_kscan_driver.c"
	.text
.Ltext0:
	.section	.text.kscan_open,"ax",%progbits
	.align	2
	.global	kscan_open
	.type	kscan_open, %function
kscan_open:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L5
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	cmp	r0, r3
	movne	r0, #0
	ldmnefd	sp!, {r4, r5, pc}
	ldr	r4, .L5+4
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L4
	mov	r5, #1
	str	r0, [r4, #4]
	mov	r1, r5
	mov	r0, #12
	str	r5, [r4, #0]
	bl	clkpwr_clk_en_dis
	ldr	r3, [r4, #4]
	mov	r2, #255
	str	r2, [r3, #0]
	str	r2, [r3, #12]
	mov	r2, #2
	str	r2, [r3, #16]
	str	r5, [r3, #20]
	str	r5, [r3, #8]
	ldr	r2, [r3, #16]
	mov	r0, r4
	orr	r2, r2, #2
	str	r2, [r3, #16]
	ldmfd	sp!, {r4, r5, pc}
.L4:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, pc}
.L6:
	.align	2
.L5:
	.word	1074069504
	.word	.LANCHOR0
.LFE0:
	.size	kscan_open, .-kscan_open
	.section	.text.kscan_close,"ax",%progbits
	.align	2
	.global	kscan_close
	.type	kscan_close, %function
kscan_close:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	ldr	r4, .L11
	cmp	r4, r0
	mvnne	r0, #0
	ldmnefd	sp!, {r4, pc}
	ldr	r3, [r4, #0]
	cmp	r3, #1
	bne	.L10
	ldr	r2, [r4, #4]
	mov	r0, #12
	str	r3, [r2, #8]
	ldr	r3, [r2, #16]
	mov	r1, #0
	bic	r3, r3, #2
	str	r3, [r2, #16]
	bl	clkpwr_clk_en_dis
	mov	r0, #0
	str	r0, [r4, #0]
	ldmfd	sp!, {r4, pc}
.L10:
	mvn	r0, #0
	ldmfd	sp!, {r4, pc}
.L12:
	.align	2
.L11:
	.word	.LANCHOR0
.LFE1:
	.size	kscan_close, .-kscan_close
	.section	.text.kscan_ioctl,"ax",%progbits
	.align	2
	.global	kscan_ioctl
	.type	kscan_ioctl, %function
kscan_ioctl:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L34
	cmp	r3, r0
	bne	.L26
	ldr	r0, [r3, #0]
	cmp	r0, #1
	bne	.L26
	cmp	r1, #3
	ldrls	pc, [pc, r1, asl #2]
	b	.L31
.L19:
	.word	.L15
	.word	.L16
	.word	.L17
	.word	.L18
.L15:
	ldr	r1, [r2, #12]
	cmp	r1, #8
	bhi	.L31
	ldr	r3, [r2, #0]
	cmp	r3, #1
	movls	r3, #2
	strls	r3, [r2, #0]
	ldr	r3, .L34
	ldrb	r0, [r2, #0]	@ zero_extendqisi2
	ldr	r3, [r3, #4]
	ldrb	r2, [r2, #4]	@ zero_extendqisi2
	str	r0, [r3, #0]
	str	r2, [r3, #12]
	str	r1, [r3, #20]
	mov	r2, #2
	b	.L33
.L16:
	ldr	r3, [r3, #4]
	mov	r2, #1
	str	r2, [r3, #8]
	b	.L32
.L17:
	cmp	r2, #0
	ldr	r3, [r3, #4]
	beq	.L21
	ldr	r2, [r3, #16]
	orr	r2, r2, #1
.L33:
	str	r2, [r3, #16]
.L32:
	mov	r0, #0
	bx	lr
.L21:
	ldr	r1, [r3, #16]
	mov	r0, r2
	bic	r1, r1, #1
	str	r1, [r3, #16]
	bx	lr
.L18:
	cmp	r2, #1
	ldreq	r3, [r3, #4]
	ldreq	r0, [r3, #8]
	andeq	r0, r0, #1
	bxeq	lr
	cmp	r2, #2
	beq	.L24
	cmp	r2, #0
	bne	.L31
	ldr	r3, .L34
	ldr	r3, [r3, #4]
	ldr	r3, [r3, #16]
	tst	r3, #2
	bne	.L30
	mov	r0, #5
	b	clkpwr_get_base_clock_rate
.L24:
	ldr	r3, .L34
	ldr	r3, [r3, #4]
	ldr	r0, [r3, #4]
	bx	lr
.L26:
	mvn	r0, #0
	bx	lr
.L31:
	mvn	r0, #6
	bx	lr
.L30:
	mov	r0, #32768
	bx	lr
.L35:
	.align	2
.L34:
	.word	.LANCHOR0
.LFE2:
	.size	kscan_ioctl, .-kscan_ioctl
	.section	.text.kscan_read,"ax",%progbits
	.align	2
	.global	kscan_read
	.type	kscan_read, %function
kscan_read:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L42
	stmfd	sp!, {r4, lr}
.LCFI2:
	cmp	r3, r0
	movne	r0, #0
	ldmnefd	sp!, {r4, pc}
	ldr	r0, [r3, #0]
	cmp	r0, #1
	bne	.L41
	cmp	r2, #8
	movlt	r0, r2
	movge	r0, #8
	mov	ip, #0
	b	.L38
.L39:
	ldr	r2, [r3, #4]
	add	r4, ip, #16
	ldr	r2, [r2, r4, asl #2]
	strb	r2, [r1, ip]
	add	ip, ip, #1
.L38:
	cmp	ip, r0
	blt	.L39
	ldmfd	sp!, {r4, pc}
.L41:
	mov	r0, #0
	ldmfd	sp!, {r4, pc}
.L43:
	.align	2
.L42:
	.word	.LANCHOR0
.LFE3:
	.size	kscan_read, .-kscan_read
	.section	.text.kscan_write,"ax",%progbits
	.align	2
	.global	kscan_write
	.type	kscan_write, %function
kscan_write:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
.LFE4:
	.size	kscan_write, .-kscan_write
	.section	.bss.kscandat,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	kscandat, %object
	.size	kscandat, 8
kscandat:
	.space	8
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc32xx_kscan_driver.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x82
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x42
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x80
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xb0
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x13b
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x172
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc32xx_kscan_driver.c\000"
.LASF4:
	.ascii	"kscan_write\000"
.LASF1:
	.ascii	"kscan_close\000"
.LASF2:
	.ascii	"kscan_ioctl\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"kscan_open\000"
.LASF3:
	.ascii	"kscan_read\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
