	.file	"e_poc_decoder_list.c"
	.text
.Ltext0:
	.section	.text.TWO_WIRE_POC_process_decoder_list,"ax",%progbits
	.align	2
	.type	TWO_WIRE_POC_process_decoder_list, %function
TWO_WIRE_POC_process_decoder_list:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L31
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldrsh	r2, [r2, #0]
	sub	sp, sp, #40
.LCFI1:
	cmp	r2, #740
	mov	r3, r0
	bne	.L24
	ldr	r3, .L31+4
	ldr	r1, [r3, #0]
	bl	COMBO_BOX_key_press
	b	.L1
.L24:
	cmp	r0, #4
	beq	.L10
	bhi	.L14
	cmp	r0, #1
	beq	.L7
	bcc	.L9
	cmp	r0, #2
	beq	.L8
	cmp	r0, #3
	bne	.L5
	b	.L9
.L14:
	cmp	r0, #67
	beq	.L12
	bhi	.L15
	cmp	r0, #16
	beq	.L11
	cmp	r0, #20
	bne	.L5
	b	.L11
.L15:
	cmp	r0, #80
	beq	.L13
	cmp	r0, #84
	bne	.L5
	b	.L13
.L8:
	ldr	r3, .L31+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L17
	cmp	r3, #1
	bne	.L26
	b	.L29
.L17:
	bl	good_key_beep
	ldr	r3, .L31+4
	ldr	r2, .L31+12
	str	r2, [r3, #0]
	mov	r3, #3
	str	r3, [sp, #4]
	ldr	r3, .L31+16
	str	r3, [sp, #24]
	mov	r3, #227
	str	r3, [sp, #28]
	mov	r3, #26
	b	.L28
.L29:
	bl	good_key_beep
	ldr	r3, .L31+4
	ldr	r2, .L31+20
	str	r2, [r3, #0]
	mov	r3, #3
	str	r3, [sp, #4]
	ldr	r3, .L31+16
	str	r3, [sp, #24]
	ldr	r3, .L31+24
	str	r3, [sp, #28]
	mov	r3, #46
.L28:
	add	r0, sp, #4
	str	r3, [sp, #32]
	bl	Display_Post_Command
	b	.L1
.L13:
	ldr	r3, .L31+8
	ldrsh	r4, [r3, #0]
	cmp	r4, #0
	beq	.L20
	cmp	r4, #1
	bne	.L26
	b	.L30
.L20:
	ldr	r0, .L31+12
	bl	process_bool
	mov	r0, r4
	bl	Redraw_Screen
	b	.L1
.L30:
	ldr	r0, .L31+20
	bl	process_bool
	b	.L1
.L26:
	bl	bad_key_beep
	b	.L1
.L11:
	mov	r0, r3
	ldr	r2, .L31+28
	ldr	r3, .L31+32
	str	r2, [sp, #0]
	ldr	r2, .L31+36
	ldr	r1, [r3, #0]
	mov	r3, #0
	bl	GROUP_process_NEXT_and_PREV
	ldr	r3, .L31+40
	ldr	r2, [r3, #0]
	ldr	r3, .L31+44
	add	r2, r2, #1
	str	r2, [r3, #0]
	bl	Refresh_Screen
	b	.L1
.L10:
	mov	r0, #1
	b	.L27
.L7:
	ldr	r3, .L31+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L12
.L27:
	bl	CURSOR_Up
	b	.L1
.L9:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L1
.L12:
	ldr	r0, .L31+48
	bl	KEY_process_BACK_from_editing_screen
	b	.L1
.L5:
	mov	r0, r3
	bl	KEY_process_global_keys
.L1:
	add	sp, sp, #40
	ldmfd	sp!, {r4, pc}
.L32:
	.align	2
.L31:
	.word	GuiLib_CurStructureNdx
	.word	.LANCHOR0
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_TwoWirePOCInUse
	.word	FDTO_POC_show_yes_no_dropdown
	.word	GuiVar_TwoWirePOCUsedForBypass
	.word	293
	.word	TWO_WIRE_POC_copy_decoder_info_into_guivars
	.word	GuiVar_TwoWireNumDiscoveredPOCDecoders
	.word	TWO_WIRE_POC_extract_and_store_changes_from_guivars
	.word	g_GROUP_list_item_index
	.word	GuiVar_TwoWirePOCDecoderIndex
	.word	FDTO_TWO_WIRE_POC_return_to_menu
.LFE3:
	.size	TWO_WIRE_POC_process_decoder_list, .-TWO_WIRE_POC_process_decoder_list
	.section	.text.FDTO_TWO_WIRE_POC_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_TWO_WIRE_POC_return_to_menu, %function
FDTO_TWO_WIRE_POC_return_to_menu:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L34
	ldr	r1, .L34+4
	b	FDTO_GROUP_return_to_menu
.L35:
	.align	2
.L34:
	.word	.LANCHOR1
	.word	TWO_WIRE_POC_extract_and_store_changes_from_guivars
.LFE4:
	.size	FDTO_TWO_WIRE_POC_return_to_menu, .-FDTO_TWO_WIRE_POC_return_to_menu
	.section	.text.TWO_WIRE_POC_extract_and_store_changes_from_guivars,"ax",%progbits
	.align	2
	.type	TWO_WIRE_POC_extract_and_store_changes_from_guivars, %function
TWO_WIRE_POC_extract_and_store_changes_from_guivars:
.LFB2:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI2:
	sub	sp, sp, #64
.LCFI3:
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L56
	mov	r1, #400
	ldr	r2, .L56+4
	mov	r4, r0
	ldr	r0, [r3, #0]
	mov	r3, #173
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L56+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L37
	ldr	r3, .L56+12
	mov	r0, r4
	ldr	r5, [r3, #0]
	cmp	r5, #0
	beq	.L38
	ldr	r3, .L56+16
	mov	r1, #11
	ldr	r2, [r3, #0]
	mov	r3, #1
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	subs	r5, r0, #0
	beq	.L39
	mov	r1, #2
	bl	POC_get_change_bits_ptr
	mov	r3, #1
	mov	r1, #0
	str	r3, [sp, #4]
	mov	r2, r1
	mov	r3, r1
	str	r4, [sp, #0]
	str	r0, [sp, #8]
	mov	r0, r5
	bl	nm_POC_set_show_this_poc
.L39:
	mov	r2, #0
	mov	r0, r4
	mov	r1, #12
	mov	r3, r2
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	subs	r5, r0, #0
	beq	.L40
	mov	r1, #2
	bl	POC_get_change_bits_ptr
	mov	r6, r0
	b	.L41
.L40:
	ldr	r3, .L56+16
	mov	r1, #12
	ldr	r2, [r3, #0]
	mov	r0, r4
	bl	nm_POC_create_new_group
	mov	r1, #2
	mov	r7, #1
	mov	r5, r0
	bl	POC_get_change_bits_ptr
	mov	r2, #0
	mov	r3, r2
	mov	r1, #12
	str	r4, [sp, #0]
	str	r7, [sp, #4]
	mov	r6, r0
	str	r0, [sp, #8]
	mov	r0, r5
	bl	nm_POC_set_poc_type
	mov	r1, #48
	ldr	r2, .L56+20
	add	r3, r4, #65
	add	r0, sp, #16
	bl	snprintf
	mov	r2, #0
	mov	r0, r5
	add	r1, sp, #16
	mov	r3, r2
	stmia	sp, {r4, r7}
	str	r6, [sp, #8]
	bl	nm_POC_set_name
.L41:
	mov	r2, #0
	mov	r1, #1
	mov	r0, r5
	mov	r3, r2
	str	r4, [sp, #0]
	stmib	sp, {r1, r6}
	bl	nm_POC_set_show_this_poc
	mov	r7, #0
.L43:
	mov	r1, r7
	mov	r0, r5
	bl	POC_get_decoder_serial_number_for_this_poc_and_level_0
	add	r1, r7, #1
	subs	r3, r0, #0
	bne	.L42
	mov	r2, #1
	str	r2, [sp, #8]
	ldr	r2, .L56+16
	stmia	sp, {r3, r4}
	str	r6, [sp, #12]
	mov	r0, r5
	ldr	r2, [r2, #0]
	bl	nm_POC_set_decoder_serial_number
	b	.L37
.L42:
	cmp	r1, #3
	mov	r7, r1
	bne	.L43
	b	.L55
.L38:
	mov	r1, #12
	mov	r2, r5
	mov	r3, r5
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	subs	r7, r0, #0
	beq	.L45
	mov	r1, #2
	bl	POC_get_change_bits_ptr
	ldr	r8, .L56+16
	mov	r6, #1
	mov	r9, r5
	mov	fp, r6
	mov	sl, r0
.L48:
	mov	r0, r7
	mov	r1, r5
	bl	POC_get_decoder_serial_number_for_this_poc_and_level_0
	ldr	r3, [r8, #0]
	cmp	r0, r3
	bne	.L46
	mov	r2, #0
	mov	r0, r7
	add	r1, r5, #1
	mov	r3, r2
	str	r9, [sp, #0]
	stmib	sp, {r4, fp}
	str	sl, [sp, #12]
	bl	nm_POC_set_decoder_serial_number
	b	.L47
.L46:
	cmp	r0, #0
	movne	r6, #0
.L47:
	add	r5, r5, #1
	cmp	r5, #3
	bne	.L48
	cmp	r6, #0
	beq	.L45
	mov	r3, #1
	mov	r1, #0
	stmib	sp, {r3, sl}
	mov	r0, r7
	mov	r2, r1
	mov	r3, r1
	str	r4, [sp, #0]
	bl	nm_POC_set_show_this_poc
.L45:
	ldr	r7, .L56+16
	mov	r0, r4
	mov	r1, #11
	ldr	r2, [r7, #0]
	mov	r3, #0
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	subs	r5, r0, #0
	beq	.L49
	mov	r1, #2
	bl	POC_get_change_bits_ptr
	mov	r6, r0
	b	.L50
.L49:
	ldr	r2, [r7, #0]
	mov	r1, #11
	mov	r0, r4
	bl	nm_POC_create_new_group
	mov	r1, #2
	mov	r8, #1
	mov	sl, #0
	mov	r5, r0
	bl	POC_get_change_bits_ptr
	mov	r2, #0
	mov	r3, r2
	mov	r1, #11
	str	r4, [sp, #0]
	str	r8, [sp, #4]
	mov	r6, r0
	str	r0, [sp, #8]
	mov	r0, r5
	bl	nm_POC_set_poc_type
	str	sl, [sp, #0]
	stmib	sp, {r4, r8}
	str	r6, [sp, #12]
	mov	r0, r5
	mov	r1, r8
	ldr	r2, [r7, #0]
	mov	r3, sl
	bl	nm_POC_set_decoder_serial_number
	add	r3, r4, #65
	str	r3, [sp, #0]
	mov	r1, #48
	ldr	r2, .L56+24
	ldr	r3, [r7, #0]
	add	r0, sp, #16
	bl	snprintf
	mov	r0, r5
	add	r1, sp, #16
	mov	r2, sl
	mov	r3, sl
	stmia	sp, {r4, r8}
	str	r6, [sp, #8]
	bl	nm_POC_set_name
.L50:
	mov	r2, #0
	mov	r1, #1
	mov	r0, r5
	mov	r3, r2
	str	r4, [sp, #0]
	stmib	sp, {r1, r6}
	bl	nm_POC_set_show_this_poc
.L37:
	bl	POC_PRESERVES_synchronize_preserves_to_file
	ldr	r3, .L56
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	add	sp, sp, #64
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L55:
	ldr	r2, .L56+8
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r2, .L56+12
	ldr	r0, .L56+28
	str	r3, [r2, #0]
	bl	DIALOG_draw_ok_dialog
	ldr	r0, .L56+32
	bl	Alert_Message
	b	.L37
.L57:
	.align	2
.L56:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_TwoWirePOCInUse
	.word	GuiVar_TwoWirePOCUsedForBypass
	.word	GuiVar_TwoWireDecoderSN
	.word	.LC1
	.word	.LC2
	.word	638
	.word	.LC3
.LFE2:
	.size	TWO_WIRE_POC_extract_and_store_changes_from_guivars, .-TWO_WIRE_POC_extract_and_store_changes_from_guivars
	.section	.text.FDTO_POC_show_yes_no_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_POC_show_yes_no_dropdown, %function
FDTO_POC_show_yes_no_dropdown:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L59
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #0]
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L60:
	.align	2
.L59:
	.word	.LANCHOR0
.LFE0:
	.size	FDTO_POC_show_yes_no_dropdown, .-FDTO_POC_show_yes_no_dropdown
	.section	.text.TWO_WIRE_POC_load_decoder_serial_number_into_guivar,"ax",%progbits
	.align	2
	.global	TWO_WIRE_POC_load_decoder_serial_number_into_guivar
	.type	TWO_WIRE_POC_load_decoder_serial_number_into_guivar, %function
TWO_WIRE_POC_load_decoder_serial_number_into_guivar:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r4, lr}
.LCFI4:
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	mov	r1, #0
	ldr	r0, .L62
	bl	GuiLib_GetTextPtr
	ldr	r2, .L62+4
	mov	r1, #49
	ldr	r2, [r2, r4, asl #3]
	str	r2, [sp, #0]
	ldr	r2, .L62+8
	mov	r3, r0
	ldr	r0, .L62+12
	bl	snprintf
	ldmfd	sp!, {r3, r4, pc}
.L63:
	.align	2
.L62:
	.word	1041
	.word	decoder_info_for_display
	.word	.LC4
	.word	GuiVar_itmGroupName
.LFE5:
	.size	TWO_WIRE_POC_load_decoder_serial_number_into_guivar, .-TWO_WIRE_POC_load_decoder_serial_number_into_guivar
	.section	.text.TWO_WIRE_POC_copy_decoder_info_into_guivars,"ax",%progbits
	.align	2
	.type	TWO_WIRE_POC_copy_decoder_info_into_guivars, %function
TWO_WIRE_POC_copy_decoder_info_into_guivars:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI5:
	ldr	r5, .L71
	mov	r6, r0
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L71+4
	mov	r3, #86
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L71+8
	ldr	r4, .L71+12
	ldr	r2, [r3, r6, asl #3]
	add	r6, r3, r6, asl #3
	str	r2, [r4, #0]
	ldr	r3, .L71+16
	ldrh	r2, [r6, #4]
	ldr	r0, [r5, #0]
	str	r2, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r5, .L71+20
	ldr	r2, .L71+24
	mov	r3, #0
	str	r3, [r5, #0]
	str	r3, [r2, #0]
	ldr	r3, [r4, #0]
	cmp	r3, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
.LBB4:
	ldr	r3, .L71+28
	mov	r1, #400
	ldr	r2, .L71+4
	ldr	r0, [r3, #0]
	mov	r3, #105
	bl	xQueueTakeMutexRecursive_debug
	bl	FLOWSENSE_get_controller_index
	mov	r3, #1
	mov	r1, #11
	ldr	r2, [r4, #0]
	mov	r6, r0
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	mov	r1, #12
	mov	r2, #0
	cmp	r0, #0
	movne	r3, #1
	strne	r3, [r5, #0]
	mov	r0, r6
	mov	r3, #1
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	subs	r6, r0, #0
	beq	.L67
	ldr	r5, .L71+12
	mov	r4, #0
.L69:
	mov	r0, r6
	mov	r1, r4
	bl	POC_get_decoder_serial_number_for_this_poc_and_level_0
	ldr	r3, [r5, #0]
	cmp	r0, r3
	bne	.L68
	ldr	r2, .L71+20
	mov	r3, #1
	str	r3, [r2, #0]
	ldr	r2, .L71+24
	str	r3, [r2, #0]
	b	.L67
.L68:
	add	r4, r4, #1
	cmp	r4, #3
	bne	.L69
.L67:
	ldr	r3, .L71+28
	ldr	r0, [r3, #0]
.LBE4:
	ldmfd	sp!, {r4, r5, r6, lr}
.LBB5:
	b	xQueueGiveMutexRecursive
.L72:
	.align	2
.L71:
	.word	tpmicro_data_recursive_MUTEX
	.word	.LC0
	.word	decoder_info_for_display
	.word	GuiVar_TwoWireDecoderSN
	.word	GuiVar_TwoWireDecoderFWVersion
	.word	GuiVar_TwoWirePOCInUse
	.word	GuiVar_TwoWirePOCUsedForBypass
	.word	list_poc_recursive_MUTEX
.LBE5:
.LFE1:
	.size	TWO_WIRE_POC_copy_decoder_info_into_guivars, .-TWO_WIRE_POC_copy_decoder_info_into_guivars
	.section	.text.FDTO_TWO_WIRE_POC_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_TWO_WIRE_POC_draw_menu
	.type	FDTO_TWO_WIRE_POC_draw_menu, %function
FDTO_TWO_WIRE_POC_draw_menu:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI6:
	mov	r5, r0
	bne	.L74
	ldr	r3, .L80
	mov	r4, #0
	str	r4, [r3, #0]
	mov	r2, #640
	ldr	r0, .L80+4
	mov	r1, r4
	bl	memset
	ldr	r3, .L80+8
	ldr	r2, .L80+4
	add	r0, r3, #4800
.L77:
	ldr	r1, [r3, #220]
	cmp	r1, #0
	beq	.L79
.L75:
	ldrb	ip, [r3, #224]	@ zero_extendqisi2
	cmp	ip, #8
	ldreqh	ip, [r3, #226]
	streq	r1, [r2, r4, asl #3]
	add	r3, r3, #60
	addeq	r1, r2, r4, asl #3
	streqh	ip, [r1, #4]	@ movhi
	addeq	r4, r4, #1
	cmp	r3, r0
	bne	.L77
.L79:
	ldr	r3, .L80+12
	str	r4, [r3, #0]
.L74:
	ldr	r3, .L80+16
	mov	r0, r5
	str	r3, [sp, #0]
	ldr	r3, .L80+20
	ldr	r1, .L80+24
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	ldr	r3, .L80+12
	ldr	r2, [r3, #0]
	mov	r3, #65
	bl	FDTO_GROUP_draw_menu
	ldr	r3, .L80
	ldr	r2, [r3, #0]
	ldr	r3, .L80+28
	add	r2, r2, #1
	str	r2, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	b	Refresh_Screen
.L81:
	.align	2
.L80:
	.word	g_GROUP_list_item_index
	.word	decoder_info_for_display
	.word	tpmicro_data
	.word	GuiVar_TwoWireNumDiscoveredPOCDecoders
	.word	TWO_WIRE_POC_load_decoder_serial_number_into_guivar
	.word	TWO_WIRE_POC_copy_decoder_info_into_guivars
	.word	.LANCHOR1
	.word	GuiVar_TwoWirePOCDecoderIndex
.LFE6:
	.size	FDTO_TWO_WIRE_POC_draw_menu, .-FDTO_TWO_WIRE_POC_draw_menu
	.section	.text.TWO_WIRE_POC_process_menu,"ax",%progbits
	.align	2
	.global	TWO_WIRE_POC_process_menu
	.type	TWO_WIRE_POC_process_menu, %function
TWO_WIRE_POC_process_menu:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, .L84
	stmfd	sp!, {r0, r1, r2, r3, lr}
.LCFI7:
	ldr	ip, [ip, #0]
	mov	r3, r0
	cmp	ip, #0
	mov	r2, r1
	bne	.L83
	cmp	r0, #67
	bne	.L83
	ldr	r3, .L84+4
	ldr	r2, .L84+8
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r2, [r3, #4]
	ldr	r3, .L84+12
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	add	sp, sp, #16
	ldr	lr, [sp], #4
	b	TWO_WIRE_ASSIGNMENT_draw_dialog
.L83:
	ldr	r1, .L84+16
	mov	r0, r3
	str	r1, [sp, #0]
	mov	r1, #0
	str	r1, [sp, #4]
	str	r1, [sp, #8]
	ldr	r3, .L84+20
	ldr	r1, .L84+24
	str	r1, [sp, #12]
	mov	r1, r2
	ldr	r3, [r3, #0]
	ldr	r2, .L84
	bl	GROUP_process_menu
	ldr	r3, .L84+28
	ldr	r2, [r3, #0]
	ldr	r3, .L84+32
	add	r2, r2, #1
	str	r2, [r3, #0]
	add	sp, sp, #16
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L85:
	.align	2
.L84:
	.word	.LANCHOR1
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
	.word	TWO_WIRE_POC_process_decoder_list
	.word	GuiVar_TwoWireNumDiscoveredPOCDecoders
	.word	TWO_WIRE_POC_copy_decoder_info_into_guivars
	.word	g_GROUP_list_item_index
	.word	GuiVar_TwoWirePOCDecoderIndex
.LFE7:
	.size	TWO_WIRE_POC_process_menu, .-TWO_WIRE_POC_process_menu
	.section	.bss.g_TWO_WIRE_POC_combo_box_guivar,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_TWO_WIRE_POC_combo_box_guivar, %object
	.size	g_TWO_WIRE_POC_combo_box_guivar, 4
g_TWO_WIRE_POC_combo_box_guivar:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_poc_decoder_list.c\000"
.LC1:
	.ascii	"Bypass Manifold @ %c\000"
.LC2:
	.ascii	"Decoder %06d @ %c\000"
.LC3:
	.ascii	"No decoder slot available in bypass\000"
.LC4:
	.ascii	"  %s %07d\000"
	.section	.bss.g_TWO_WIRE_POC_editing_decoder,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_TWO_WIRE_POC_editing_decoder, %object
	.size	g_TWO_WIRE_POC_editing_decoder, 4
g_TWO_WIRE_POC_editing_decoder:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x64
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI4-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI5-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI6-.LFB6
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI7-.LFB7
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x83
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_poc_decoder_list.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xc2
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF7
	.byte	0x1
	.4byte	.LASF8
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF9
	.byte	0x1
	.byte	0x4c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x15f
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x1d5
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	.LASF2
	.byte	0x1
	.byte	0x93
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x6
	.4byte	.LASF3
	.byte	0x1
	.byte	0x46
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x1db
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST2
	.uleb128 0x8
	.4byte	0x21
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST3
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x1e1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST4
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x20e
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x3
	.byte	0x7d
	.sleb128 100
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB5
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB1
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB6
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB7
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF6:
	.ascii	"TWO_WIRE_POC_process_menu\000"
.LASF9:
	.ascii	"TWO_WIRE_POC_copy_decoder_info_into_guivars\000"
.LASF3:
	.ascii	"FDTO_POC_show_yes_no_dropdown\000"
.LASF5:
	.ascii	"FDTO_TWO_WIRE_POC_draw_menu\000"
.LASF1:
	.ascii	"FDTO_TWO_WIRE_POC_return_to_menu\000"
.LASF8:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_poc_decoder_list.c\000"
.LASF4:
	.ascii	"TWO_WIRE_POC_load_decoder_serial_number_into_guivar"
	.ascii	"\000"
.LASF7:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"TWO_WIRE_POC_process_decoder_list\000"
.LASF2:
	.ascii	"TWO_WIRE_POC_extract_and_store_changes_from_guivars"
	.ascii	"\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
