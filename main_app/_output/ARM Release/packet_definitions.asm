	.file	"packet_definitions.c"
	.text
.Ltext0:
	.section	.text.get_this_packets_message_class,"ax",%progbits
	.align	2
	.global	get_this_packets_message_class
	.type	get_this_packets_message_class, %function
get_this_packets_message_class:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	and	r3, r3, #30
	cmp	r3, #30
	movne	r3, #2000
	ldreq	r3, .L5
	strne	r3, [r1, #0]
	ldrneb	r3, [r0, #0]	@ zero_extendqisi2
	streq	r3, [r1, #0]
	ldreqb	r3, [r0, #1]	@ zero_extendqisi2
	movne	r3, r3, lsr #1
	andne	r3, r3, #15
	str	r3, [r1, #4]
	bx	lr
.L6:
	.align	2
.L5:
	.word	3000
.LFE0:
	.size	get_this_packets_message_class, .-get_this_packets_message_class
	.section	.text.set_this_packets_CS3000_message_class,"ax",%progbits
	.align	2
	.global	set_this_packets_CS3000_message_class
	.type	set_this_packets_CS3000_message_class, %function
set_this_packets_CS3000_message_class:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	strb	r1, [r0, #1]
	orr	r3, r3, #30
	strb	r3, [r0, #0]
	bx	lr
.LFE1:
	.size	set_this_packets_CS3000_message_class, .-set_this_packets_CS3000_message_class
	.global	postamble
	.global	preamble
	.section	.rodata.preamble,"a",%progbits
	.type	preamble, %object
	.size	preamble, 4
preamble:
	.byte	76
	.byte	61
	.byte	46
	.byte	31
	.section	.rodata.postamble,"a",%progbits
	.type	postamble, %object
	.size	postamble, 4
postamble:
	.byte	-120
	.byte	121
	.byte	106
	.byte	91
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/../common_includes/packet_definitions.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x32
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/../commo"
	.ascii	"n_includes/packet_definitions.c\000"
.LASF0:
	.ascii	"get_this_packets_message_class\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"set_this_packets_CS3000_message_class\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
