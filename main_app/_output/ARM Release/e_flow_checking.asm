	.file	"e_flow_checking.c"
	.text
.Ltext0:
	.section	.text.FLOW_CHECKING_process_group,"ax",%progbits
	.align	2
	.type	FLOW_CHECKING_process_group, %function
FLOW_CHECKING_process_group:
.LFB1:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L55
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #44
.LCFI1:
	cmp	r3, #740
	mov	r5, r0
	bne	.L45
	ldr	r1, .L55+4
	bl	COMBO_BOX_key_press
	b	.L1
.L45:
	cmp	r0, #4
	beq	.L10
	bhi	.L14
	cmp	r0, #1
	beq	.L7
	bcc	.L6
	cmp	r0, #2
	beq	.L8
	cmp	r0, #3
	bne	.L5
	b	.L54
.L14:
	cmp	r0, #67
	beq	.L12
	bhi	.L15
	cmp	r0, #16
	beq	.L11
	cmp	r0, #20
	bne	.L5
	b	.L11
.L15:
	cmp	r0, #80
	beq	.L13
	cmp	r0, #84
	bne	.L5
	b	.L13
.L8:
	ldr	r3, .L55+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L37
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L55+12
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L1
.L13:
	ldr	r3, .L55+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L18
.L31:
	.word	.L19
	.word	.L20
	.word	.L21
	.word	.L22
	.word	.L23
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
.L19:
	ldr	r0, .L55+4
	bl	process_bool
	mov	r0, #0
	bl	Redraw_Screen
	b	.L32
.L20:
	ldr	r3, .L55+16
	mov	r2, #1
	mov	ip, #0
	ldr	r3, [r3, #0]
	mov	r0, r5
	stmia	sp, {r2, ip}
	ldr	r1, .L55+20
	mov	r2, #10
	b	.L48
.L23:
	ldr	r3, .L55+20
	mov	r0, #1
	ldr	r2, [r3, #0]
	ldr	r3, .L55+24
	mov	r1, #0
	ldr	r3, [r3, #0]
	stmia	sp, {r0, r1}
	ldr	r1, .L55+16
	mov	r0, r5
	add	r2, r2, #1
.L48:
	sub	r3, r3, #1
.L49:
	bl	process_uns32
	b	.L32
.L26:
	ldr	r3, .L55+16
	mov	r1, #1
	ldr	r2, [r3, #0]
	mov	r3, #0
	stmia	sp, {r1, r3}
	ldr	r1, .L55+24
	mov	r0, r5
	add	r2, r2, #1
	mov	r3, #4000
	b	.L49
.L21:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L55+28
	mov	r0, r5
	b	.L52
.L24:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L55+32
	mov	r0, r5
	b	.L52
.L27:
	mov	r2, #1
	mov	r3, #0
	stmia	sp, {r2, r3}
	ldr	r1, .L55+36
	mov	r0, r5
	b	.L52
.L29:
	ldr	r1, .L55+40
	mov	r2, #1
	mov	r3, #0
	mov	r0, r5
	stmia	sp, {r2, r3}
.L52:
	mov	r3, #200
	b	.L49
.L22:
	ldr	r4, .L55+44
	b	.L50
.L25:
	ldr	r4, .L55+48
	b	.L50
.L28:
	ldr	r4, .L55+52
	b	.L50
.L30:
	ldr	r4, .L55+56
.L50:
	ldr	r3, [r4, #0]
	mov	r2, #1
	rsb	r3, r3, #0
	str	r3, [r4, #0]
	mov	r3, #0
	stmia	sp, {r2, r3}
	mov	r0, r5
	mov	r3, #200
	mov	r1, r4
	bl	process_int32
	ldr	r3, [r4, #0]
	rsb	r3, r3, #0
	str	r3, [r4, #0]
	b	.L32
.L18:
	bl	bad_key_beep
.L32:
	bl	Refresh_Screen
	b	.L1
.L11:
	bl	SYSTEM_num_systems_in_use
	ldr	r3, .L55+60
	ldr	r2, .L55+64
	str	r3, [sp, #0]
	ldr	r3, .L55+68
	mov	r1, r0
	mov	r0, r5
	bl	GROUP_process_NEXT_and_PREV
	b	.L1
.L10:
	ldr	r3, .L55+8
	ldrh	r3, [r3, #0]
	cmp	r3, #11
	bhi	.L37
	mov	r3, r3, asl #16
	mov	r3, r3, asr #16
	mov	r1, #1
	mov	r2, r1, asl r3
	tst	r2, #3072
	subne	r0, r3, #2
	bne	.L51
	ands	r0, r2, #1008
	subne	r0, r3, #3
	bne	.L51
	tst	r2, #14
	ldrne	r2, .L55+72
	strne	r3, [r2, #0]
	beq	.L37
.L51:
	bl	CURSOR_Select
	b	.L1
.L6:
	ldr	r3, .L55+8
	ldrsh	r0, [r3, #0]
	cmp	r0, #9
	ldrls	pc, [pc, r0, asl #2]
	b	.L37
.L41:
	.word	.L38
	.word	.L39
	.word	.L39
	.word	.L39
	.word	.L39
	.word	.L39
	.word	.L39
	.word	.L37
	.word	.L40
	.word	.L40
.L38:
	ldr	r3, .L55+72
	ldr	r2, [r3, #0]
	cmp	r2, #0
	moveq	r2, #1
	streq	r2, [r3, #0]
	ldr	r0, [r3, #0]
	b	.L53
.L39:
	add	r0, r0, #3
	b	.L53
.L40:
	add	r0, r0, #2
.L53:
	mov	r1, #1
	b	.L51
.L37:
	bl	bad_key_beep
	b	.L1
.L7:
	ldr	r3, .L55+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L12
.L47:
	bl	CURSOR_Up
	b	.L1
.L54:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L1
.L12:
	ldr	r0, .L55+76
	bl	KEY_process_BACK_from_editing_screen
	b	.L1
.L5:
	mov	r0, r5
	bl	KEY_process_global_keys
.L1:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L56:
	.align	2
.L55:
	.word	GuiLib_CurStructureNdx
	.word	GuiVar_SystemFlowCheckingInUse
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_FLOW_CHECKING_show_dropdown
	.word	GuiVar_SystemFlowCheckingRange2
	.word	GuiVar_SystemFlowCheckingRange1
	.word	GuiVar_SystemFlowCheckingRange3
	.word	GuiVar_SystemFlowCheckingTolerancePlus1
	.word	GuiVar_SystemFlowCheckingTolerancePlus2
	.word	GuiVar_SystemFlowCheckingTolerancePlus3
	.word	GuiVar_SystemFlowCheckingTolerancePlus4
	.word	GuiVar_SystemFlowCheckingToleranceMinus1
	.word	GuiVar_SystemFlowCheckingToleranceMinus2
	.word	GuiVar_SystemFlowCheckingToleranceMinus3
	.word	GuiVar_SystemFlowCheckingToleranceMinus4
	.word	FLOW_CHECKING_copy_group_into_guivars
	.word	FLOW_CHECKING_extract_and_store_changes_from_GuiVars
	.word	SYSTEM_get_group_at_this_index
	.word	.LANCHOR0
	.word	FDTO_FLOW_CHECKING_return_to_menu
.LFE1:
	.size	FLOW_CHECKING_process_group, .-FLOW_CHECKING_process_group
	.section	.text.FDTO_FLOW_CHECKING_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_FLOW_CHECKING_return_to_menu, %function
FDTO_FLOW_CHECKING_return_to_menu:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L58
	ldr	r1, .L58+4
	b	FDTO_GROUP_return_to_menu
.L59:
	.align	2
.L58:
	.word	.LANCHOR1
	.word	FLOW_CHECKING_extract_and_store_changes_from_GuiVars
.LFE2:
	.size	FDTO_FLOW_CHECKING_return_to_menu, .-FDTO_FLOW_CHECKING_return_to_menu
	.section	.text.FDTO_FLOW_CHECKING_show_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_FLOW_CHECKING_show_dropdown, %function
FDTO_FLOW_CHECKING_show_dropdown:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L61
	mov	r0, #272
	ldr	r2, [r3, #0]
	mov	r1, #34
	b	FDTO_COMBO_BOX_show_no_yes_dropdown
.L62:
	.align	2
.L61:
	.word	GuiVar_SystemFlowCheckingInUse
.LFE0:
	.size	FDTO_FLOW_CHECKING_show_dropdown, .-FDTO_FLOW_CHECKING_show_dropdown
	.section	.text.FDTO_FLOW_CHECKING_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_FLOW_CHECKING_draw_menu
	.type	FDTO_FLOW_CHECKING_draw_menu, %function
FDTO_FLOW_CHECKING_draw_menu:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r4, r5, lr}
.LCFI2:
	ldr	r4, .L64
	mov	r5, r0
	mov	r1, #400
	ldr	r2, .L64+4
	mov	r3, #260
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	ldr	r3, .L64+8
	ldr	r1, .L64+12
	str	r3, [sp, #0]
	ldr	r3, .L64+16
	str	r3, [sp, #4]
	mov	r3, #0
	str	r3, [sp, #8]
	mov	r3, #21
	mov	r2, r0
	mov	r0, r5
	bl	FDTO_GROUP_draw_menu
	ldr	r0, [r4, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L65:
	.align	2
.L64:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	SYSTEM_load_system_name_into_scroll_box_guivar
	.word	.LANCHOR1
	.word	FLOW_CHECKING_copy_group_into_guivars
.LFE3:
	.size	FDTO_FLOW_CHECKING_draw_menu, .-FDTO_FLOW_CHECKING_draw_menu
	.section	.text.FLOW_CHECKING_process_menu,"ax",%progbits
	.align	2
	.global	FLOW_CHECKING_process_menu
	.type	FLOW_CHECKING_process_menu, %function
FLOW_CHECKING_process_menu:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r6, r7, lr}
.LCFI3:
	ldr	r4, .L67
	mov	r6, r0
	mov	r7, r1
	ldr	r2, .L67+4
	mov	r1, #400
	ldr	r3, .L67+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	ldr	r2, .L67+12
	mov	r1, r7
	str	r2, [sp, #0]
	mov	r2, #0
	str	r2, [sp, #4]
	ldr	r2, .L67+16
	str	r2, [sp, #8]
	ldr	r2, .L67+20
	str	r2, [sp, #12]
	ldr	r2, .L67+24
	mov	r3, r0
	mov	r0, r6
	bl	GROUP_process_menu
	ldr	r0, [r4, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L68:
	.align	2
.L67:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	270
	.word	FLOW_CHECKING_process_group
	.word	SYSTEM_get_group_at_this_index
	.word	FLOW_CHECKING_copy_group_into_guivars
	.word	.LANCHOR1
.LFE4:
	.size	FLOW_CHECKING_process_menu, .-FLOW_CHECKING_process_menu
	.section	.bss.g_FLOW_CHECKING_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_FLOW_CHECKING_editing_group, %object
	.size	g_FLOW_CHECKING_editing_group, 4
g_FLOW_CHECKING_editing_group:
	.space	4
	.section	.bss.g_FLOW_CHECKING_prev_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_FLOW_CHECKING_prev_cursor_pos, %object
	.size	g_FLOW_CHECKING_prev_cursor_pos, 4
g_FLOW_CHECKING_prev_cursor_pos:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_flow_checking.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x82
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_flow_checking.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x7f
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF5
	.byte	0x1
	.4byte	.LASF6
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x3d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.byte	0xfc
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0x37
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x102
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x10c
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"FDTO_FLOW_CHECKING_return_to_menu\000"
.LASF3:
	.ascii	"FDTO_FLOW_CHECKING_draw_menu\000"
.LASF6:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_flow_checking.c\000"
.LASF2:
	.ascii	"FDTO_FLOW_CHECKING_show_dropdown\000"
.LASF5:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"FLOW_CHECKING_process_menu\000"
.LASF0:
	.ascii	"FLOW_CHECKING_process_group\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
