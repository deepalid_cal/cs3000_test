	.file	"r_orphan_two_wire_pocs.c"
	.text
.Ltext0:
	.section	.text.ORPHAN_TWO_WIRE_POCS_load_guivars_for_scroll_line,"ax",%progbits
	.align	2
	.type	ORPHAN_TWO_WIRE_POCS_load_guivars_for_scroll_line, %function
ORPHAN_TWO_WIRE_POCS_load_guivars_for_scroll_line:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L2
	mov	r3, r0, asl #16
	mov	r3, r3, asr #16
	mov	r1, #12
	mla	r3, r1, r3, r2
	ldr	r2, [r3, #8]
	ldr	r3, .L2+4
	str	r2, [r3, #0]
	bx	lr
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	GuiVar_RptDecoderSN
.LFE1:
	.size	ORPHAN_TWO_WIRE_POCS_load_guivars_for_scroll_line, .-ORPHAN_TWO_WIRE_POCS_load_guivars_for_scroll_line
	.section	.text.ORPHAN_TWO_WIRE_POCS_populate_list,"ax",%progbits
	.align	2
	.global	ORPHAN_TWO_WIRE_POCS_populate_list
	.type	ORPHAN_TWO_WIRE_POCS_populate_list, %function
ORPHAN_TWO_WIRE_POCS_populate_list:
.LFB0:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L15
	stmfd	sp!, {r0, r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI0:
	mov	r4, #0
	str	r4, [r3, #0]
	bl	FLOWSENSE_get_controller_index
	ldr	r3, .L15+4
	mov	r1, #400
	ldr	r2, .L15+8
	mov	fp, r0
	ldr	r0, [r3, #0]
	mov	r3, #88
	bl	xQueueTakeMutexRecursive_debug
	b	.L5
.L11:
	mov	r0, r4
	bl	POC_get_group_at_this_index
	subs	r5, r0, #0
	beq	.L6
	bl	nm_GROUP_get_group_ID
	mov	sl, r0
	bl	POC_get_type_of_poc
	mov	r8, r0
	mov	r0, sl
	bl	POC_get_box_index_0
	rsb	r2, fp, r0
	rsbs	r3, r2, #0
	adc	r3, r3, r2
	cmp	r8, #10
	moveq	r3, #0
	cmp	r3, #0
	mov	r6, r0
	beq	.L6
	mov	r0, r5
	bl	POC_get_show_for_this_poc
	cmp	r0, #0
	beq	.L6
	ldr	r9, .L15+12
	mov	r5, #0
.L10:
	mov	r0, sl
	mov	r1, r5
	bl	POC_get_decoder_serial_number_for_this_poc_gid_and_level_0
	subs	r7, r0, #0
	beq	.L7
	mov	r3, #121
	ldr	r0, [r9, #0]
	mov	r1, #400
	ldr	r2, .L15+8
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L15+16
.L9:
	ldr	r2, [r3, #220]
	cmp	r7, r2
	beq	.L12
	ldr	r2, .L15+20
	add	r3, r3, #60
	cmp	r3, r2
	bne	.L9
	mov	r3, #0
	b	.L8
.L12:
	mov	r3, #1
.L8:
	ldr	r2, .L15+12
	ldr	r0, [r2, #0]
	str	r3, [sp, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, [sp, #0]
	cmp	r3, #0
	bne	.L7
	ldr	r3, .L15
	mov	r0, #12
	ldr	r2, [r3, #0]
	ldr	ip, .L15+24
	mul	r0, r2, r0
	add	r2, r2, #1
	add	r1, ip, r0
	str	r6, [ip, r0]
	str	r8, [r1, #4]
	str	r7, [r1, #8]
	str	r2, [r3, #0]
.L7:
	add	r5, r5, #1
	cmp	r5, #3
	bne	.L10
.L6:
	add	r4, r4, #1
.L5:
	bl	POC_get_list_count
	cmp	r4, r0
	bcc	.L11
	ldr	r3, .L15+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L15
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L16:
	.align	2
.L15:
	.word	.LANCHOR1
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	tpmicro_data_recursive_MUTEX
	.word	tpmicro_data
	.word	tpmicro_data+4800
	.word	.LANCHOR0
.LFE0:
	.size	ORPHAN_TWO_WIRE_POCS_populate_list, .-ORPHAN_TWO_WIRE_POCS_populate_list
	.section	.text.FDTO_ORPHAN_TWO_WIRE_POCS_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_ORPHAN_TWO_WIRE_POCS_draw_report
	.type	FDTO_ORPHAN_TWO_WIRE_POCS_draw_report, %function
FDTO_ORPHAN_TWO_WIRE_POCS_draw_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI1:
	mvn	r1, #0
	mov	r4, r0
	mov	r2, #1
	mov	r0, #88
	bl	GuiLib_ShowScreen
	ldr	r3, .L18
	ldr	r2, .L18+4
	ldr	r1, [r3, #0]
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	b	FDTO_REPORTS_draw_report
.L19:
	.align	2
.L18:
	.word	.LANCHOR1
	.word	ORPHAN_TWO_WIRE_POCS_load_guivars_for_scroll_line
.LFE2:
	.size	FDTO_ORPHAN_TWO_WIRE_POCS_draw_report, .-FDTO_ORPHAN_TWO_WIRE_POCS_draw_report
	.section	.text.ORPHAN_TWO_WIRE_POCS_process_report,"ax",%progbits
	.align	2
	.global	ORPHAN_TWO_WIRE_POCS_process_report
	.type	ORPHAN_TWO_WIRE_POCS_process_report, %function
ORPHAN_TWO_WIRE_POCS_process_report:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	stmfd	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, sl, lr}
.LCFI2:
	mov	r4, r0
	beq	.L22
	cmp	r0, #67
	bne	.L31
	b	.L32
.L22:
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	cmp	r0, #0
	blt	.L24
	ldr	r3, .L33
	ldr	r2, .L33+4
	ldr	r0, [r3, #0]
	mov	r1, #400
	mov	r3, #196
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r7, #12
	ldr	r6, .L33+8
	mul	r0, r7, r0
	ldr	r5, [r6, r0]
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	mla	r0, r7, r0, r6
	ldr	r8, [r0, #4]
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	mov	r1, r8
	mov	r3, #1
	mla	r6, r7, r0, r6
	mov	r0, r5
	ldr	r6, [r6, #8]
	mov	r2, r6
	bl	nm_POC_get_pointer_to_poc_for_this_box_index_0_and_poc_type_and_decoder_sn
	subs	r7, r0, #0
	beq	.L25
	bl	good_key_beep
	mov	r1, r4
	mov	r0, r7
	bl	POC_get_change_bits_ptr
	mov	r4, #0
	mov	r8, #1
	mov	sl, r0
.L27:
	mov	r0, r7
	mov	r1, r4
	bl	POC_get_decoder_serial_number_for_this_poc_and_level_0
	cmp	r0, r6
	beq	.L26
	cmp	r0, #0
	movne	r8, #0
.L26:
	add	r4, r4, #1
	cmp	r4, #3
	bne	.L27
	cmp	r8, #0
	beq	.L28
	mov	r1, #0
	mov	r3, #1
	stmib	sp, {r3, sl}
	mov	r0, r7
	mov	r2, r1
	mov	r3, r1
	str	r5, [sp, #0]
	bl	nm_POC_set_show_this_poc
	b	.L28
.L25:
	bl	bad_key_beep
	ldr	r0, .L33+12
	bl	Alert_Message
.L28:
	ldr	r3, .L33
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	ORPHAN_TWO_WIRE_POCS_populate_list
	mov	r0, #1
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	Redraw_Screen
.L24:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	bad_key_beep
.L32:
	ldr	r3, .L33+16
	ldr	r2, .L33+20
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r2, [r3, #4]
	ldr	r3, .L33+24
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	TWO_WIRE_ASSIGNMENT_draw_dialog
.L31:
	mov	r2, #0
	mov	r3, r2
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	b	REPORTS_process_report
.L34:
	.align	2
.L33:
	.word	list_poc_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR0
	.word	.LC1
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	ORPHAN_TWO_WIRE_POCS_process_report, .-ORPHAN_TWO_WIRE_POCS_process_report
	.section	.bss.ORPHAN_POCS_list_of_orphans,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	ORPHAN_POCS_list_of_orphans, %object
	.size	ORPHAN_POCS_list_of_orphans, 144
ORPHAN_POCS_list_of_orphans:
	.space	144
	.section	.bss.g_ORPHAN_TWO_WIRE_POCS_num_pocs,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_ORPHAN_TWO_WIRE_POCS_num_pocs, %object
	.size	g_ORPHAN_TWO_WIRE_POCS_num_pocs, 4
g_ORPHAN_TWO_WIRE_POCS_num_pocs:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_orphan_two_wire_pocs.c\000"
.LC1:
	.ascii	"Unable to find POC to remove\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI1-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x82
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_orphan_two_wire_pocs.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF5
	.byte	0x1
	.byte	0xa1
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x3f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xa7
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xaf
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"FDTO_ORPHAN_TWO_WIRE_POCS_draw_report\000"
.LASF0:
	.ascii	"ORPHAN_TWO_WIRE_POCS_populate_list\000"
.LASF2:
	.ascii	"ORPHAN_TWO_WIRE_POCS_process_report\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_orphan_two_wire_pocs.c\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"ORPHAN_TWO_WIRE_POCS_load_guivars_for_scroll_line\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
