	.file	"e_contrast_and_speaker_vol.c"
	.text
.Ltext0:
	.section	.text.FDTO_LCD_draw_backlight_contrast_and_volume,"ax",%progbits
	.align	2
	.global	FDTO_LCD_draw_backlight_contrast_and_volume
	.type	FDTO_LCD_draw_backlight_contrast_and_volume, %function
FDTO_LCD_draw_backlight_contrast_and_volume:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L4
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldrnesh	r4, [r3, #0]
	bne	.L3
	ldr	r3, .L4+4
	ldr	r4, [r3, #0]
	subs	r4, r4, #16
	movne	r4, #1
.L3:
	bl	CONTRAST_AND_SPEAKER_VOL__copy_settings_into_guivars
	mov	r0, #14
	mov	r1, r4
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, lr}
	b	GuiLib_Refresh
.L5:
	.align	2
.L4:
	.word	GuiLib_ActiveCursorFieldNo
	.word	display_model_is
.LFE0:
	.size	FDTO_LCD_draw_backlight_contrast_and_volume, .-FDTO_LCD_draw_backlight_contrast_and_volume
	.section	.text.LCD_process_backlight_contrast_and_volume,"ax",%progbits
	.align	2
	.global	LCD_process_backlight_contrast_and_volume
	.type	LCD_process_backlight_contrast_and_volume, %function
LCD_process_backlight_contrast_and_volume:
.LFB1:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #3
	str	lr, [sp, #-4]!
.LCFI1:
	sub	sp, sp, #36
.LCFI2:
	beq	.L10
	bhi	.L12
	cmp	r0, #0
	beq	.L8
	cmp	r0, #1
	bne	.L7
	b	.L9
.L12:
	cmp	r0, #80
	beq	.L9
	cmp	r0, #84
	beq	.L10
	cmp	r0, #4
	bne	.L7
	b	.L32
.L10:
	ldr	r3, .L33
	ldrsh	r3, [r3, #0]
	cmp	r3, #1
	beq	.L15
	cmp	r3, #2
	beq	.L16
	cmp	r3, #0
	bne	.L6
	bl	LCD_contrast_DARKEN
	b	.L27
.L15:
	bl	LED_backlight_brighter
	b	.L27
.L16:
	bl	SPEAKER_vol_increase
	b	.L27
.L9:
	ldr	r3, .L33
	ldrsh	r3, [r3, #0]
	cmp	r3, #1
	beq	.L19
	cmp	r3, #2
	beq	.L20
	cmp	r3, #0
	bne	.L6
	bl	LCD_contrast_LIGHTEN
	b	.L27
.L19:
	bl	LED_backlight_darker
	b	.L27
.L20:
	bl	SPEAKER_vol_decrease
.L27:
	cmp	r0, #1
	beq	.L17
	bl	bad_key_beep
	b	.L6
.L32:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L6
.L8:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L6
.L7:
	cmp	r0, #67
	ldreq	r3, .L33+4
	moveq	r2, #11
	streq	r2, [r3, #0]
	bl	KEY_process_global_keys
	b	.L6
.L17:
	bl	good_key_beep
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L33+8
	mov	r0, sp
	str	r3, [sp, #20]
	mov	r3, #0
	str	r3, [sp, #24]
	bl	Display_Post_Command
	mov	r0, #15
	mov	r1, #5
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L6:
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L34:
	.align	2
.L33:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_MenuScreenToShow
	.word	FDTO_LCD_draw_backlight_contrast_and_volume
.LFE1:
	.size	LCD_process_backlight_contrast_and_volume, .-LCD_process_backlight_contrast_and_volume
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_contrast_and_speaker_vol.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x46
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x3c
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"FDTO_LCD_draw_backlight_contrast_and_volume\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_contrast_and_speaker_vol.c\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"LCD_process_backlight_contrast_and_volume\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
