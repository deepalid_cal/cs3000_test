	.file	"key_scanner.c"
	.text
.Ltext0:
	.section	.text._ks_repeat_timer_callback,"ax",%progbits
	.align	2
	.type	_ks_repeat_timer_callback, %function
_ks_repeat_timer_callback:
.LFB1:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r4, .L2
	sub	sp, sp, #40
.LCFI1:
	ldr	r0, [r4, #0]
	bl	uxQueueMessagesWaiting
	add	r1, sp, #40
	mov	r3, #2
	str	r3, [r1, #-40]!
	mvn	r2, #0
	mov	r1, sp
	mov	r3, #0
	cmp	r0, #8
	movls	r0, #0
	movhi	r0, #1
	str	r0, [sp, #4]
	ldr	r0, [r4, #0]
	bl	xQueueGenericSend
	add	sp, sp, #40
	ldmfd	sp!, {r4, pc}
.L3:
	.align	2
.L2:
	.word	Key_Scanner_Queue
.LFE1:
	.size	_ks_repeat_timer_callback, .-_ks_repeat_timer_callback
	.section	.text.kscan_isr,"ax",%progbits
	.align	2
	.type	kscan_isr, %function
kscan_isr:
.LFB0:
	@ args = 0, pretend = 0, frame = 44
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r1, .L7
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	sub	sp, sp, #44
.LCFI3:
	add	r2, sp, #4
	mov	r3, #0
.L5:
	add	r0, r3, #16
	ldr	r0, [r1, r0, asl #2]
	add	r3, r3, #1
	cmp	r3, #8
	str	r0, [r2, #4]!
	bne	.L5
	ldr	r5, .L7+4
	mov	r4, #1
	ldr	r0, [r5, #0]
	bl	uxQueueMessagesWaitingFromISR
	add	r1, sp, #44
	str	r4, [r1, #-44]!
	mov	r3, #0
	mov	r1, sp
	add	r2, sp, #40
	cmp	r0, #8
	movls	r0, #0
	movhi	r0, #1
	str	r0, [sp, #4]
	ldr	r0, [r5, #0]
	bl	xQueueGenericSendFromISR
	ldr	r3, .L7
	str	r4, [r3, #8]
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L8:
	.align	2
.L7:
	.word	1074069504
	.word	Key_Scanner_Queue
.LFE0:
	.size	kscan_isr, .-kscan_isr
	.global	__udivsi3
	.section	.text.key_scanner_task,"ax",%progbits
	.align	2
	.global	key_scanner_task
	.type	key_scanner_task, %function
key_scanner_task:
.LFB4:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI4:
	fstmfdd	sp!, {d8}
.LCFI5:
	ldr	r9, .L44+36
	sub	sp, sp, #12
.LCFI6:
	rsb	r9, r9, r0
	mov	r0, #54
	bl	xDisable_ISR
	mov	r0, #0
	mov	r1, r0
	bl	clkpwr_select_enet_iface
	mov	r0, #12
	mov	r1, #1
	bl	clkpwr_clk_en_dis
	ldr	r3, .L44+40
	mov	r1, #3
	mov	r2, #5
	str	r1, [r3, #0]
	mov	r0, #8
	str	r2, [r3, #12]
	mov	r2, #2
	str	r2, [r3, #16]
	str	r0, [r3, #20]
	mov	r0, #1
	str	r0, [r3, #8]
	mov	r4, #0
	ldr	r3, .L44+44
	mov	r0, #54
	str	r4, [sp, #0]
	bl	xSetISR_Vector
	mov	r0, #54
	bl	xEnable_ISR
	ldr	r3, .L44+48
	mov	r2, r4
	str	r3, [sp, #0]
	ldr	r0, .L44+52
	mov	r3, r4
	mov	r1, #100
	bl	xTimerCreate
	ldr	r4, .L44+56
	mov	r9, r9, lsr #5
	cmp	r0, #0
	str	r0, [r4, #108]
	bne	.L10
	ldr	r0, .L44+60
	bl	RemovePathFromFileName
	mov	r2, #432
	mov	r1, r0
	ldr	r0, .L44+64
	bl	Alert_Message_va
.L10:
	ldr	r3, .L44+68
.LBB6:
	fldd	d8, .L44
.LBE6:
.LBB7:
	mov	sl, #0
.LBE7:
	ldmia	r3, {r2-r3}
	str	r2, [r4, #100]
	str	r3, [r4, #104]
	mov	r3, #1
	str	r3, [r4, #0]
	ldr	r4, .L44+56
.L30:
	ldr	r3, .L44+72
	ldr	r1, .L44+76
	ldr	r0, [r3, #0]
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	bne	.L11
	ldr	r3, [r4, #8]
	cmp	r3, #1
	ldreq	r0, .L44+80
	beq	.L42
	ldr	r3, [r4, #0]
	cmp	r3, #1
	beq	.L13
	cmp	r3, #2
	bne	.L11
	b	.L43
.L13:
.LBB8:
	ldr	r3, [r4, #4]
	cmp	r3, #1
	bne	.L11
	ldr	r7, .L44+84
	mov	r6, #0
.L19:
	ldr	r3, [r7], #4
	cmp	r3, #0
	beq	.L15
	mov	r8, r6, asl #4
	mov	r5, #0
.L18:
	ldr	r3, [r7, #-4]
	mov	r2, #1
	ands	r3, r3, r2, asl r5
	beq	.L16
	add	r3, r5, r8
	str	r3, [sp, #4]
	ldr	r3, .L44+88
	mov	r2, #0
	ldr	r0, [r3, #0]
	add	r1, sp, #4
	mov	r3, r2
	str	r2, [sp, #8]
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L17
	ldr	r0, .L44+92
	bl	Alert_Message
.L17:
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, #0
	mov	r2, #100
	ldr	r0, [r4, #108]
	bl	xTimerGenericCommand
	ldr	r0, .L44+96
	mov	r2, #32
	sub	r1, r0, #32
	bl	memcpy
	mov	r3, #0
	str	r3, [r4, #88]
	strh	r3, [r4, #92]	@ movhi
	strh	r3, [r4, #94]	@ movhi
	strh	r3, [r4, #96]	@ movhi
	mov	r3, #2
	str	r6, [r4, #76]
	str	r5, [r4, #80]
	str	sl, [r4, #84]	@ float
	str	r3, [r4, #0]
.L16:
	add	r5, r5, #1
	cmp	r5, #8
	bne	.L18
.L15:
	add	r6, r6, #1
	cmp	r6, #8
	bne	.L19
	b	.L11
.L43:
.LBE8:
.LBB9:
	ldr	r5, [r4, #4]
	cmp	r5, #1
	bne	.L20
	ldr	r3, [r4, #76]
	ldr	r2, [r4, #80]
	add	r3, r3, #3
	ldr	r3, [r4, r3, asl #2]
	ands	r2, r3, r5, asl r2
	bne	.L11
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, r5
	ldr	r0, [r4, #108]
	mov	r3, r2
	bl	xTimerGenericCommand
	str	r5, [r4, #0]
	b	.L11
.L20:
	cmp	r5, #2
	bne	.L11
	ldr	r3, [r4, #104]
	flds	s9, [r4, #84]
	ldr	r2, [r4, #100]
	cmp	r3, #0
	moveq	r3, #100
	streq	r3, [r4, #104]
	flds	s8, [r4, #104]	@ int
	fcvtds	d5, s9
	ldr	r3, [r4, #88]
	fuitod	d7, s8
	add	r3, r3, #1
	str	r3, [r4, #88]
	fmuld	d6, d7, d8
	fcmped	d5, d6
	fmstat
	fldsmi	s15, .L44+8
	faddsmi	s9, s9, s15
	fstsmi	s9, [r4, #84]
	bmi	.L24
.L39:
	fldd	d6, .L44+12
	fmuld	d7, d7, d6
	fcmped	d5, d7
	fmstat
	bmi	.L41
.L40:
	fuitos	s8, s8
	fcmpes	s9, s8
	fmstat
	bpl	.L24
.L41:
	fldd	d7, .L44+20
	faddd	d5, d5, d7
	fcvtsd	s10, d5
	fsts	s10, [r4, #84]
.L24:
	cmp	r3, #10
	movls	r3, #5
	bls	.L28
	cmp	r2, #20
	movcc	r3, r2
	movcs	r3, #20
	sub	r3, r3, #5
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	flds	s13, [r4, #104]	@ int
	fuitos	s15, s13
	fdivs	s15, s14, s15
	flds	s14, [r4, #84]
	fmuls	s15, s15, s14
	ftouizs	s15, s15
	fmrs	r3, s15	@ int
	add	r3, r3, #5
	cmp	r3, #20
	movhi	r3, #20
	bhi	.L28
	cmp	r3, #4
	movls	r3, #5
.L28:
	fmsr	s15, r3	@ int
	mov	r1, #5
	fuitod	d6, s15
	fldd	d7, .L44+28
	fdivd	d7, d7, d6
	ftouizd	s13, d7
	fmrs	r0, s13	@ int
	bl	__udivsi3
	mvn	r3, #0
	str	r3, [sp, #0]
	mov	r1, #2
	mov	r3, #0
	mov	r2, r0
	ldr	r0, [r4, #108]
	bl	xTimerGenericCommand
	ldr	r2, [r4, #76]
	ldr	r3, [r4, #80]
	add	r1, sp, #4
	add	r3, r3, r2, asl #4
	str	r3, [sp, #4]
	ldr	r3, [r4, #88]
	mov	r2, #0
	str	r3, [sp, #8]
	ldr	r3, .L44+88
	ldr	r0, [r3, #0]
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L11
	ldr	r0, .L44+92
.L42:
	bl	Alert_Message
.L11:
.LBE9:
	bl	xTaskGetTickCount
	ldr	r3, .L44+100
	str	r0, [r3, r9, asl #2]
	b	.L30
.L45:
	.align	2
.L44:
	.word	0
	.word	1070596096
	.word	1065353216
	.word	0
	.word	1072168960
	.word	-1717986918
	.word	1071225241
	.word	0
	.word	1083129856
	.word	Task_Table
	.word	1074069504
	.word	kscan_isr
	.word	_ks_repeat_timer_callback
	.word	.LC0
	.word	.LANCHOR0
	.word	.LC1
	.word	.LC2
	.word	.LANCHOR1
	.word	Key_Scanner_Queue
	.word	.LANCHOR0+4
	.word	.LC3
	.word	.LANCHOR0+12
	.word	Key_To_Process_Queue
	.word	.LC4
	.word	.LANCHOR0+44
	.word	task_last_execution_stamp
.LFE4:
	.size	key_scanner_task, .-key_scanner_task
	.global	ks_control
	.global	Regular_Repeat_Profile
	.section	.bss.ks_control,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	ks_control, %object
	.size	ks_control, 112
ks_control:
	.space	112
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"KS Timer\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/key_"
	.ascii	"scanner/key_scanner.c\000"
.LC2:
	.ascii	"Timer NOT CREATED : %s, %u\000"
.LC3:
	.ascii	"KEY SCANNER queue overflow\000"
.LC4:
	.ascii	"KEY TO PROCESS queue overflow\000"
	.section	.rodata.Regular_Repeat_Profile,"a",%progbits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	Regular_Repeat_Profile, %object
	.size	Regular_Repeat_Profile, 8
Regular_Repeat_Profile:
	.word	20
	.word	100
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI2-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x28
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xa
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/key_scanner.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x69
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x7f
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x5f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0x8f
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.byte	0xd6
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x176
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB0
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI3
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	.LCFI6
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF6:
	.ascii	"key_scanner_task\000"
.LASF1:
	.ascii	"kscan_isr\000"
.LASF2:
	.ascii	"__ks_process_state_no_key\000"
.LASF0:
	.ascii	"_ks_repeat_timer_callback\000"
.LASF3:
	.ascii	"__ks_process_state_repeats\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/key_"
	.ascii	"scanner/key_scanner.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
