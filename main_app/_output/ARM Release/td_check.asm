	.file	"td_check.c"
	.text
.Ltext0:
	.section	.text.timer_250ms_callback,"ax",%progbits
	.align	2
	.type	timer_250ms_callback, %function
timer_250ms_callback:
.LFB0:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L18
	str	lr, [sp, #-4]!
.LCFI0:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #36
.LCFI1:
	cmp	r3, #81
	beq	.L6
	bgt	.L11
	cmp	r3, #19
	beq	.L4
	cmp	r3, #78
	beq	.L5
	cmp	r3, #13
	bne	.L1
	b	.L16
.L11:
	cmp	r3, #87
	beq	.L8
	bgt	.L12
	cmp	r3, #83
	bne	.L1
	b	.L17
.L12:
	cmp	r3, #99
	beq	.L9
	cmp	r3, #600
	bne	.L1
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L18+4
	b	.L15
.L4:
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L18+8
.L15:
	str	r3, [sp, #20]
	b	.L14
.L16:
	bl	Refresh_Screen
	b	.L1
.L9:
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L18+12
	b	.L13
.L5:
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L18+16
	b	.L13
.L8:
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L18+20
	b	.L13
.L17:
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L18+24
	b	.L13
.L6:
	mov	r3, #2
	str	r3, [sp, #0]
	ldr	r3, .L18+28
.L13:
	str	r3, [sp, #20]
	mov	r3, #0
	str	r3, [sp, #24]
.L14:
	mov	r0, sp
	bl	Display_Post_Command
.L1:
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L19:
	.align	2
.L18:
	.word	GuiLib_CurStructureNdx
	.word	FDTO_CODE_DOWNLOAD_update_dialog
	.word	FDTO_FLOWSENSE_update_screen
	.word	FDTO_SERIAL_PORT_INFO_draw_report
	.word	FDTO_FLOWSENSE_STATS_draw_report
	.word	FDTO_MEM_draw_report
	.word	FDTO_FLOW_TABLE_redraw_scrollbox
	.word	FDTO_FLOW_CHECKING_STATS_draw_report
.LFE0:
	.size	timer_250ms_callback, .-timer_250ms_callback
	.section	.text.TD_CHECK_task,"ax",%progbits
	.align	2
	.global	TD_CHECK_task
	.type	TD_CHECK_task, %function
TD_CHECK_task:
.LFB1:
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L109
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI2:
	ldr	r9, .L109+4
	sub	sp, sp, #92
.LCFI3:
	str	r3, [sp, #88]	@ float
	ldr	r3, .L109+8
	rsb	r9, r9, r0
	str	r3, [sp, #0]
	ldr	r0, .L109+12
	mov	r1, #50
	mov	r2, #1
	mov	r3, #0
	bl	xTimerCreate
	mov	r9, r9, lsr #5
	subs	r4, r0, #0
	bne	.L21
	ldr	r0, .L109+16
	bl	Alert_Message
	b	.L22
.L21:
	bl	xTaskGetTickCount
	mvn	r3, #0
	mov	r1, #0
	str	r3, [sp, #0]
	mov	r3, r1
	mov	r2, r0
	mov	r0, r4
	bl	xTimerGenericCommand
.L22:
.LBB2:
	add	r4, sp, #48
.L95:
.LBE2:
	ldr	r3, .L109+20
	add	r1, sp, #44
	ldr	r0, [r3, #0]
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	mov	r7, r0
	bne	.L24
	ldr	r3, [sp, #44]
	ldr	r2, .L109+24
	cmp	r3, r2
	bne	.L93
.LBB5:
	bl	FLOWSENSE_we_are_poafs
	ldr	r6, .L109+28
	ldr	r3, .L109+32
	ldr	r2, [r6, #8]
	str	r0, [r3, #0]
	ldr	r3, .L109+36
	str	r2, [r3, #0]
	ldr	r3, .L109+40
	ldr	r2, [r6, #0]
	str	r2, [r3, #0]
	bl	COMM_MNGR_network_is_available_for_normal_use
	ldr	r3, [sp, #48]
	cmp	r3, #0
	mov	r5, r0
	bne	.L27
.LBB3:
	cmp	r0, #0
	str	r7, [r6, #28]
	bne	.L28
	bl	STATION_chain_down_NOW_days_midnight_maintenance
.L28:
	ldr	r3, .L109+44
	add	r1, sp, #92
	str	r3, [r1, #-24]!
	ldr	r3, .L109+48
	mov	r2, #0
	ldr	r0, [r3, #0]
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L29
	ldr	r0, .L109+52
	bl	RemovePathFromFileName
	ldr	r2, .L109+56
	mov	r1, r0
	ldr	r0, .L109+60
	bl	Alert_Message_va
.L29:
	mov	r0, r4
	bl	SERIAL_at_midnight_manage_xmit_and_rcvd_accumulators_and_make_alert_lines
	ldr	r6, .L109+64
	ldr	r8, .L109+68
	mov	r7, #0
.L31:
	ldr	r2, [r6, #4]
	cmp	r2, r8
	bls	.L30
	ldr	r0, .L109+72
	ldr	r1, [r6, #8]
	bl	Alert_Message_va
.L30:
	ldr	r3, .L109+76
	str	r7, [r6, #4]
	add	r6, r6, #12
	cmp	r6, r3
	bne	.L31
.L27:
.LBE3:
	mov	r0, #141
	bl	CONTROLLER_INITIATED_post_event
	ldr	r2, [sp, #48]
	ldr	r3, .L109+80
	cmp	r2, r3
	bne	.L32
	ldr	r3, .L109+84
	ldr	r0, [r3, #48]
	bl	CONTROLLER_INITIATED_ms_after_midnight_to_send_this_controllers_daily_records
	mov	r1, #1
	bl	CONTROLLER_INITIATED_alerts_timer_upkeep
.L32:
	ldr	r2, [sp, #48]
	ldr	r3, .L109+88
	cmp	r2, r3
	bne	.L33
	bl	CONFIG_this_controller_is_behind_a_hub
	subs	r1, r0, #0
	bne	.L33
	ldr	r0, .L109+92
	mov	r2, #512
	mov	r3, r1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
.L33:
	cmp	r5, #0
	beq	.L34
	bl	SYSTEM_PRESERVES_synchronize_preserves_to_file
	bl	POC_PRESERVES_synchronize_preserves_to_file
	add	r1, sp, #92
	mov	r3, #1000
	str	r3, [r1, #-24]!
	ldr	r3, .L109+48
	mov	r2, #0
	ldr	r0, [r3, #0]
	mov	r3, r2
	bl	xQueueGenericSend
	cmp	r0, #1
	beq	.L35
	ldr	r0, .L109+52
	bl	RemovePathFromFileName
	ldr	r2, .L109+96
	mov	r1, r0
	ldr	r0, .L109+60
	bl	Alert_Message_va
.L35:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	beq	.L36
	ldr	r5, [sp, #48]
	cmp	r5, #0
	bne	.L37
	ldr	r3, .L109+100
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L109+52
	ldr	r3, .L109+104
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L109+108
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L109+52
	ldr	r3, .L109+112
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, .L109+116
	ldr	r6, .L109+120
	ldr	r8, .L109+124
	b	.L38
.L45:
	mov	r0, r5
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_group_ID
	cmp	r0, #0
	bne	.L39
	ldr	r0, .L109+52
	bl	RemovePathFromFileName
	mov	r2, #464
	mov	r1, r0
	ldr	r0, .L109+128
	bl	Alert_Message_va
	b	.L40
.L39:
	bl	SYSTEM_PRESERVES_get_system_preserve_ptr_for_this_system_gid
	subs	fp, r0, #0
	beq	.L41
	add	r0, fp, #500
	bl	SYSTEM_PRESERVES_there_is_a_mlb
	cmp	r0, #0
	beq	.L41
	ldrb	r3, [fp, #500]	@ zero_extendqisi2
	cmp	r3, #0
	movne	r3, #504
	ldrneh	sl, [fp, r3]
	ldrneh	r3, [fp, r8]
	movne	fp, #10
	bne	.L43
.L42:
	ldrb	r3, [fp, #501]	@ zero_extendqisi2
	cmp	r3, #0
	movne	r3, #508
	moveq	r3, #512
	ldrneh	sl, [fp, r3]
	ldreqh	sl, [fp, r3]
	ldrneh	r3, [fp, r6]
	ldreqh	r3, [fp, r7]
	movne	fp, #20
	moveq	fp, #30
.L43:
	mov	r0, r5
	str	r3, [sp, #4]
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_name
	ldr	r3, [sp, #4]
	mov	r2, fp
	str	r3, [sp, #0]
	mov	r3, sl
	mov	r1, r0
	ldr	r0, .L109+132
	bl	Alert_mainline_break
.L41:
	add	r5, r5, #1
.L38:
	bl	SYSTEM_num_systems_in_use
	cmp	r5, r0
	bcc	.L45
.L40:
	ldr	r3, .L109+108
	ldr	r5, .L109+136
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L109+100
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	WEATHER_get_rain_bucket_is_in_use
	cmp	r0, #1
	bne	.L46
	ldr	r3, [r5, #52]
	cmp	r3, #0
	beq	.L46
	fmsr	s13, r3	@ int
	flds	s15, [sp, #88]
	fuitos	s14, s13
	fdivs	s14, s14, s15
	fmrs	r0, s14
	bl	Alert_24HourRainTotal
.L46:
	mov	r3, #0
	str	r3, [r5, #52]
	bl	WATERSENSE_verify_watersense_compliance
.L37:
	mov	r0, r4
	bl	WEATHER_TABLES_roll_et_rain_tables
	ldr	r1, [sp, #48]
	cmp	r1, #0
	bne	.L47
	mov	r0, #408
	mov	r2, #512
	mov	r3, r1
	bl	CONTROLLER_INITIATED_post_to_messages_queue
.L47:
	ldr	r5, [sp, #48]
	bl	WEATHER_get_et_and_rain_table_roll_time
	cmp	r5, r0
	bne	.L48
	bl	WEATHER_get_et_gage_is_in_use
	cmp	r0, #0
	bne	.L49
	bl	WEATHER_get_rain_bucket_is_in_use
	cmp	r0, #0
	beq	.L48
.L49:
	mov	r0, #135
	bl	CONTROLLER_INITIATED_post_event
.L48:
	ldr	r3, [sp, #64]
	str	r3, [sp, #0]
	add	r3, sp, #48
	ldmia	r3, {r0, r1, r2, r3}
	bl	BUDGET_timer_upkeep
	bl	FOAL_FLOW__A__build_poc_flow_rates
	bl	FOAL_FLOW__B__build_system_flow_rates
	mov	r0, r4
	bl	REPORT_DATA_maintain_all_accumulators
	mov	r0, r4
	bl	TDCHECK_at_1Hz_rate_check_for_irrigation_schedule_start
	mov	r0, r4
	bl	SYSTEM_check_for_mvor_schedule_start
	mov	r0, r4
	bl	FOAL_IRRI_maintain_irrigation_list
	mov	r0, r4
	bl	TDCHECK_at_1Hz_rate_check_for_lights_schedule_start
	mov	r0, r4
	bl	FOAL_LIGHTS_maintain_lights_list
	bl	FOAL_FLOW_check_for_MLB
	bl	FOAL_FLOW_check_valve_flow
.L36:
	ldr	r5, .L109+140
	bl	IRRI_maintain_irrigation_list
	mov	r0, r4
	bl	IRRI_LIGHTS_maintain_lights_list
	ldr	r3, [r5, #4]
	cmp	r3, #0
	beq	.L50
	ldr	r6, [sp, #48]
	bl	WEATHER_get_et_and_rain_table_roll_time
	cmp	r6, r0
	bne	.L51
.L50:
	bl	FTIMES_TASK_restart_calculation
	mov	r3, #1
	str	r3, [r5, #4]
	b	.L51
.L34:
.LBB4:
	mov	r0, r5
	bl	init_battery_backed_poc_preserves
	ldr	r3, .L109+100
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L109+52
	ldr	r3, .L109+144
	bl	xQueueTakeMutexRecursive_debug
	ldr	r7, .L109+148
	ldr	r6, .L109+152
.L52:
	mla	r0, r7, r5, r6
	add	r5, r5, #1
	add	r0, r0, #16
	bl	nm_SYSTEM_PRESERVES_restart_irrigation_and_block_flow_checking_for_150_seconds
	cmp	r5, #4
	bne	.L52
	ldr	r3, .L109+100
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
.L51:
.LBE4:
	ldr	r5, .L109+156
	mov	r1, #400
	ldr	r2, .L109+52
	mov	r3, #696
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L109+28
	ldr	r2, [r3, #8]
	ldr	r3, .L109+160
	ldr	r1, [r3, #0]
	cmp	r1, r2
	beq	.L53
	str	r2, [r3, #0]
	bl	Refresh_Screen
.L53:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L109+164
	mov	r5, #2
	add	r0, sp, #8
	str	r3, [sp, #28]
	str	r5, [sp, #8]
	str	r4, [sp, #32]
	bl	Display_Post_Command
	ldr	r3, .L109+168
	ldrsh	r3, [r3, #0]
	cmp	r3, #86
	beq	.L67
	bgt	.L82
	cmp	r3, #59
	beq	.L61
	bgt	.L83
	cmp	r3, #34
	beq	.L57
	bgt	.L84
	cmp	r3, #15
	beq	.L55
	cmp	r3, #32
	bne	.L54
	b	.L101
.L84:
	cmp	r3, #44
	beq	.L59
	cmp	r3, #51
	beq	.L60
	cmp	r3, #36
	bne	.L54
	b	.L102
.L83:
	cmp	r3, #77
	beq	.L64
	bgt	.L85
	cmp	r3, #72
	beq	.L62
	cmp	r3, #76
	bne	.L54
	b	.L103
.L85:
	cmp	r3, #79
	blt	.L54
	cmp	r3, #80
	ble	.L65
	cmp	r3, #85
	bne	.L54
	b	.L104
.L82:
	cmp	r3, #97
	beq	.L74
	bgt	.L86
	cmp	r3, #93
	beq	.L70
	bgt	.L87
	cmp	r3, #91
	beq	.L68
	cmp	r3, #92
	bne	.L54
	b	.L105
.L87:
	cmp	r3, #95
	beq	.L72
	mov	r3, #0
	strgt	r5, [sp, #8]
	ldrgt	r2, .L109+172
	bgt	.L100
	b	.L106
.L86:
	cmp	r3, #103
	beq	.L78
	bgt	.L88
	cmp	r3, #100
	beq	.L76
	cmp	r3, #101
	beq	.L77
	cmp	r3, #98
	bne	.L54
	b	.L107
.L88:
	ldr	r2, .L109+176
	cmp	r3, r2
	beq	.L80
	add	r2, r2, #4
	cmp	r3, r2
	beq	.L81
	sub	r2, r2, #5
	cmp	r3, r2
	bne	.L54
	b	.L108
.L65:
	ldr	r3, .L109+140
	ldr	r2, .L109+180
	ldr	r1, [r3, #0]
	cmp	r1, r2
	moveq	r3, #1
	streq	r3, [sp, #8]
	ldreq	r3, .L109+184
	beq	.L98
.L89:
	ldr	r2, [r3, #60]
	ldr	r3, .L109+188
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L54
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L109+192
	str	r0, [r3, #0]
	mov	r3, #2
	str	r3, [sp, #8]
	ldr	r3, .L109+196
	b	.L97
.L81:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+200
	b	.L98
.L60:
	mov	r0, #24576
	bl	RADIO_TEST_post_event
	b	.L54
.L80:
	ldr	r3, .L109+204
	str	r5, [sp, #8]
.L97:
	str	r3, [sp, #28]
	mov	r3, #0
.L99:
	str	r3, [sp, #32]
	b	.L96
.L108:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+208
	b	.L98
.L102:
	ldr	r3, .L109+212
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	beq	.L90
	ldr	r3, .L109+216
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L54
.L90:
	mov	r0, #0
	bl	STATUS_update_screen
	b	.L54
.L104:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+220
	b	.L98
.L74:
	str	r5, [sp, #8]
	ldr	r3, .L109+224
	b	.L97
.L105:
	str	r5, [sp, #8]
	ldr	r3, .L109+228
	b	.L97
.L106:
	ldr	r2, .L109+232
	str	r5, [sp, #8]
.L100:
	str	r2, [sp, #28]
	b	.L99
.L70:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+236
	b	.L98
.L107:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+240
	b	.L98
.L62:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+244
	b	.L98
.L76:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+248
	b	.L98
.L77:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+252
	b	.L98
.L68:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+256
	b	.L98
.L78:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+260
	b	.L98
.L64:
	ldr	r3, .L109+264
	str	r5, [sp, #8]
	str	r3, [sp, #28]
	mov	r3, #1
	b	.L99
.L55:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+268
	b	.L98
.L103:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+272
	b	.L98
.L61:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+276
	b	.L98
.L57:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+280
	b	.L98
.L101:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+284
	b	.L98
.L72:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+288
	b	.L98
.L67:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+292
	b	.L98
.L59:
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L109+296
.L98:
	str	r3, [sp, #28]
.L96:
	add	r0, sp, #8
	bl	Display_Post_Command
.L54:
	ldr	r5, .L109+300
	mov	r6, #8
	str	r6, [r5, #68]
	mov	r0, #4
	bl	vTaskDelay
	str	r6, [r5, #72]
	b	.L24
.L93:
	ldr	r0, .L109+304
	bl	Alert_Message
.L24:
.LBE5:
	bl	xTaskGetTickCount
	ldr	r3, .L109+308
	str	r0, [r3, r9, asl #2]
	b	.L95
.L110:
	.align	2
.L109:
	.word	1120403456
	.word	Task_Table
	.word	timer_250ms_callback
	.word	.LC0
	.word	.LC1
	.word	TD_CHECK_queue
	.word	4369
	.word	comm_mngr
	.word	GuiVar_LiveScreensCommFLEnabled
	.word	GuiVar_LiveScreensCommFLInForced
	.word	GuiVar_NetworkAvailable
	.word	3000
	.word	BYPASS_event_queue
	.word	.LC2
	.word	337
	.word	.LC3
	.word	stuck_key_data
	.word	1999
	.word	.LC4
	.word	stuck_key_data+156
	.word	1800
	.word	config_c
	.word	58500
	.word	414
	.word	438
	.word	system_preserves_recursive_MUTEX
	.word	450
	.word	list_system_recursive_MUTEX
	.word	453
	.word	514
	.word	510
	.word	506
	.word	.LC5
	.word	.LC6
	.word	weather_preserves
	.word	ftcs
	.word	671
	.word	14224
	.word	system_preserves
	.word	chain_members_recursive_MUTEX
	.word	GuiVar_StatusChainDown
	.word	FDTO_show_time_date
	.word	GuiLib_CurStructureNdx
	.word	FDTO_REAL_TIME_POC_FLOW_draw_report
	.word	599
	.word	4097
	.word	FINISH_TIMES_draw_dialog
	.word	FINISH_TIME_calc_start_timestamp
	.word	FINISH_TIME_scrollbox_index
	.word	FDTO_FINISH_TIMES_draw_screen
	.word	FINISH_TIMES_update_dialog
	.word	FDTO_TWO_WIRE_update_discovery_dialog
	.word	FDTO_DEVICE_EXCHANGE_update_dialog
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_StatusShowLiveScreens
	.word	FDTO_IRRI_DETAILS_redraw_scrollbox
	.word	FDTO_REAL_TIME_SYSTEM_FLOW_draw_report
	.word	FDTO_REAL_TIME_BYPASS_FLOW_draw_report
	.word	FDTO_REAL_TIME_ELECTRICAL_draw_report
	.word	FDTO_REAL_TIME_COMMUNICATIONS_update_report
	.word	FDTO_REAL_TIME_WEATHER_update_report
	.word	FDTO_WEATHER_update_real_time_weather_GuiVars
	.word	FDTO_STATION_HISTORY_redraw_scrollbox
	.word	FDTO_STATION_REPORT_redraw_scrollbox
	.word	FDTO_POC_USAGE_redraw_scrollbox
	.word	FDTO_SYSTEM_REPORT_redraw_scrollbox
	.word	FDTO_ET_RAIN_TABLE_update_report
	.word	FDTO_TIME_DATE_update_screen
	.word	FDTO_TWO_WIRE_update_decoder_stats
	.word	FDTO_TEST_SEQ_update_screen
	.word	FDTO_MVOR_update_screen
	.word	FDTO_LIGHTS_TEST_update_screen
	.word	FDTO_REAL_TIME_LIGHTS_redraw_scrollbox
	.word	FDTO_LIGHTS_REPORT_redraw_scrollbox
	.word	MOISTURE_SENSOR_update_measurements
	.word	1073905664
	.word	.LC7
	.word	task_last_execution_stamp
.LFE1:
	.size	TD_CHECK_task, .-TD_CHECK_task
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"250ms timer\000"
.LC1:
	.ascii	"Timer 250ms NOT CREATED!\000"
.LC2:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/rtc/"
	.ascii	"td_check.c\000"
.LC3:
	.ascii	"BYPASS queue full : %s, %u\000"
.LC4:
	.ascii	"Stuck Key: %s, %u repeats\000"
.LC5:
	.ascii	"SYSTEM_PRESERVES : unexp NULL gid : %s, %u\000"
.LC6:
	.ascii	"REMINDER\000"
.LC7:
	.ascii	"TD_CHECK: unk queue command!\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x80
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/rtc/td_check.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x45
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF0
	.byte	0x1
	.4byte	.LASF1
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0x99
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xed
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x3
	.byte	0x7d
	.sleb128 128
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/rtc/"
	.ascii	"td_check.c\000"
.LASF3:
	.ascii	"TD_CHECK_task\000"
.LASF2:
	.ascii	"timer_250ms_callback\000"
.LASF0:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
