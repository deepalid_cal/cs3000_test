	.file	"r_rt_weather.c"
	.text
.Ltext0:
	.section	.text.FDTO_REAL_TIME_WEATHER_update_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_WEATHER_update_report
	.type	FDTO_REAL_TIME_WEATHER_update_report, %function
FDTO_REAL_TIME_WEATHER_update_report:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	bl	WEATHER_get_et_gage_box_index
	ldr	r1, .L5+16
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	bl	WEATHER_get_rain_bucket_box_index
	ldr	r1, .L5+20
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	bl	WEATHER_get_wind_gage_box_index
	ldr	r1, .L5+24
	bl	NETWORK_CONFIG_get_controller_name_str_for_ui
	ldr	r3, .L5+28
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L5+32
	mov	r3, #55
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L5+36
	ldrh	r2, [r3, #36]
	ldr	r1, [r3, #84]
	fmsr	s13, r2	@ int
	ldr	r2, .L5+40
	fsitod	d7, s13
	fldd	d6, .L5
	fdivd	d7, d7, d6
	flds	s13, [r3, #52]	@ int
	fcvtsd	s14, d7
	fsts	s14, [r2, #0]
	fuitod	d7, s13
	fldd	d6, .L5+8
	ldr	r2, .L5+44
	ldr	r0, [r2, #0]
	fdivd	d7, d7, d6
	cmp	r0, r1
	strne	r1, [r2, #0]
	ldr	r2, .L5+48
	ldr	r1, [r3, #100]
	movne	r4, #1
	moveq	r4, #0
	fcvtsd	s14, d7
	fsts	s14, [r2, #0]
	ldr	r2, .L5+52
	str	r1, [r2, #0]
	ldr	r2, [r3, #104]
	ldr	r3, .L5+56
	str	r2, [r3, #0]
	ldr	r3, .L5+28
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	cmp	r4, #1
	bne	.L3
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	b	FDTO_Redraw_Screen
.L3:
	ldmfd	sp!, {r4, lr}
	b	GuiLib_Refresh
.L6:
	.align	2
.L5:
	.word	0
	.word	1086556160
	.word	0
	.word	1079574528
	.word	GuiVar_StatusETGageControllerName
	.word	GuiVar_StatusRainBucketControllerName
	.word	GuiVar_StatusWindGageControllerName
	.word	weather_preserves_recursive_MUTEX
	.word	.LC0
	.word	weather_preserves
	.word	GuiVar_StatusETGageReading
	.word	GuiVar_ETGageRunawayGage
	.word	GuiVar_StatusRainBucketReading
	.word	GuiVar_StatusRainSwitchState
	.word	GuiVar_StatusFreezeSwitchState
.LFE0:
	.size	FDTO_REAL_TIME_WEATHER_update_report, .-FDTO_REAL_TIME_WEATHER_update_report
	.section	.text.FDTO_REAL_TIME_WEATHER_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_REAL_TIME_WEATHER_draw_report
	.type	FDTO_REAL_TIME_WEATHER_draw_report, %function
FDTO_REAL_TIME_WEATHER_draw_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	str	lr, [sp, #-4]!
.LCFI1:
	ldr	r3, .L12
	beq	.L8
	ldr	r2, .L12+4
	ldr	r1, [r2, #0]
	ldr	r2, [r3, #84]
	cmp	r1, r2
	beq	.L9
.L8:
	ldr	r1, [r3, #84]
	mov	r0, #98
	subs	r1, r1, #1
	mvnne	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
.L9:
	ldr	lr, [sp], #4
	b	FDTO_REAL_TIME_WEATHER_update_report
.L13:
	.align	2
.L12:
	.word	weather_preserves
	.word	GuiVar_ETGageRunawayGage
.LFE1:
	.size	FDTO_REAL_TIME_WEATHER_draw_report, .-FDTO_REAL_TIME_WEATHER_draw_report
	.section	.text.REAL_TIME_WEATHER_process_report,"ax",%progbits
	.align	2
	.global	REAL_TIME_WEATHER_process_report
	.type	REAL_TIME_WEATHER_process_report, %function
REAL_TIME_WEATHER_process_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	stmfd	sp!, {r4, lr}
.LCFI2:
	beq	.L16
	cmp	r0, #67
	bne	.L22
	b	.L24
.L16:
	ldr	r3, .L25
	ldrsh	r0, [r3, #0]
	cmp	r0, #0
	bne	.L23
	ldr	r3, .L25+4
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L23
	mov	r1, r0
	bl	CURSOR_Select
	ldmfd	sp!, {r4, lr}
	b	ET_GAGE_clear_runaway_gage
.L23:
	ldmfd	sp!, {r4, lr}
	b	bad_key_beep
.L24:
	ldr	r3, .L25+8
	ldr	r2, .L25+12
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r4, .L25+16
	ldr	r3, [r3, #4]
	str	r3, [r4, #0]
	bl	KEY_process_global_keys
	ldr	r3, [r4, #0]
	cmp	r3, #10
	ldmnefd	sp!, {r4, pc}
	mov	r0, #1
	ldmfd	sp!, {r4, lr}
	b	LIVE_SCREENS_draw_dialog
.L22:
	ldmfd	sp!, {r4, lr}
	b	KEY_process_global_keys
.L26:
	.align	2
.L25:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_ETGageRunawayGage
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	REAL_TIME_WEATHER_process_report, .-REAL_TIME_WEATHER_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_weather.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_rt_weather.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5a
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x27
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x5e
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x77
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"FDTO_REAL_TIME_WEATHER_draw_report\000"
.LASF0:
	.ascii	"FDTO_REAL_TIME_WEATHER_update_report\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_rt_weather.c\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF2:
	.ascii	"REAL_TIME_WEATHER_process_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
