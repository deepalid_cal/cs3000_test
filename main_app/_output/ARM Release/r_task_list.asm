	.file	"r_task_list.c"
	.text
.Ltext0:
	.section	.text.FDTO_TASK_LIST_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_TASK_LIST_draw_report
	.type	FDTO_TASK_LIST_draw_report, %function
FDTO_TASK_LIST_draw_report:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, lr}
.LCFI0:
	mov	r4, r0
	ldmnefd	sp!, {r4, pc}
	ldr	r0, .L3
	bl	vTaskGetRunTimeStats
	mov	r0, #105
	mov	r1, #0
	mov	r2, r4
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, lr}
	b	GuiLib_Refresh
.L4:
	.align	2
.L3:
	.word	GuiVar_TaskList
.LFE0:
	.size	FDTO_TASK_LIST_draw_report, .-FDTO_TASK_LIST_draw_report
	.section	.text.TASK_LIST_process_report,"ax",%progbits
	.align	2
	.global	TASK_LIST_process_report
	.type	TASK_LIST_process_report, %function
TASK_LIST_process_report:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	str	lr, [sp, #-4]!
.LCFI1:
	mov	r3, r0
	mov	r2, r1
	beq	.L8
	bhi	.L10
	cmp	r0, #0
	beq	.L7
	b	.L6
.L10:
	cmp	r0, #4
	beq	.L7
	cmp	r0, #67
	bne	.L6
	b	.L11
.L8:
	bl	good_key_beep
	mov	r0, #1
	ldr	lr, [sp], #4
	b	Redraw_Screen
.L7:
	mov	r0, #0
	mov	r1, r3
	ldr	lr, [sp], #4
	b	TEXT_BOX_up_or_down
.L11:
	ldr	r3, .L12
	mov	r2, #11
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	ldr	lr, [sp], #4
	b	TECH_SUPPORT_draw_dialog
.L6:
	mov	r0, r3
	mov	r1, r2
	ldr	lr, [sp], #4
	b	KEY_process_global_keys
.L13:
	.align	2
.L12:
	.word	GuiVar_MenuScreenToShow
.LFE1:
	.size	TASK_LIST_process_report, .-TASK_LIST_process_report
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_task_list.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x46
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x29
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x4a
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_task_list.c\000"
.LASF1:
	.ascii	"TASK_LIST_process_report\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"FDTO_TASK_LIST_draw_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
