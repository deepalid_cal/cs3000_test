	.file	"irri_time.c"
	.text
.Ltext0:
	.section	.text.nm_IRRITIME_this_program_is_scheduled_to_run,"ax",%progbits
	.align	2
	.global	nm_IRRITIME_this_program_is_scheduled_to_run
	.type	nm_IRRITIME_this_program_is_scheduled_to_run, %function
nm_IRRITIME_this_program_is_scheduled_to_run:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	mov	r5, r0
	mov	r0, r1
	mov	r6, r1
	bl	SCHEDULE_get_enabled
	cmp	r0, #1
	mov	r4, r0
	bne	.L4
	mov	r0, r6
	bl	SCHEDULE_get_schedule_type
	cmp	r0, #3
	bne	.L9
	mov	r4, #0
.L3:
	mov	r0, r5
	mov	r1, r4
	bl	SCHEDULE_get_station_waters_this_day
	cmp	r0, #1
	ldmeqfd	sp!, {r4, r5, r6, pc}
	add	r4, r4, #1
	cmp	r4, #7
	bne	.L3
.L4:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L9:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, pc}
.LFE0:
	.size	nm_IRRITIME_this_program_is_scheduled_to_run, .-nm_IRRITIME_this_program_is_scheduled_to_run
	.section	.text.days_in_this_irrigation_period_for_a_weekly_schedule,"ax",%progbits
	.align	2
	.global	days_in_this_irrigation_period_for_a_weekly_schedule
	.type	days_in_this_irrigation_period_for_a_weekly_schedule, %function
days_in_this_irrigation_period_for_a_weekly_schedule:
.LFB3:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #0
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI1:
	str	r3, [sp, #0]	@ float
	mov	r3, #1065353216
	str	r3, [sp, #4]	@ float
	ldr	r3, .L28
	mov	ip, r0
	str	r3, [sp, #8]	@ float
	mov	r2, #7
	mov	r3, #0
.L12:
	ldr	r4, [ip, #8]
	add	ip, ip, #4
	cmp	r4, #0
	addne	r3, r3, #1
	subs	r2, r2, #1
	bne	.L12
	cmp	r3, #0
	fldseq	s15, [sp, #0]
	beq	.L14
	ldr	r2, [r0, #40]
	cmp	r2, #0
	fmsrne	s13, r3	@ int
	fldsne	s14, [sp, #8]
	fuitosne	s15, s13
	fdivsne	s15, s14, s15
	bne	.L14
.L15:
	ldrb	r3, [r1, #18]	@ zero_extendqisi2
	ldr	r2, [r0, #44]
	ldr	r1, [r1, #0]
	cmp	r1, r2
	bls	.L27
.LBB11:
	add	r3, r3, #1
	cmp	r3, #6
	movhi	r3, #0
	b	.L27
.L19:
.LBE11:
.LBB12:
	add	r3, r3, #1
	cmp	r3, #6
	add	r2, r2, #4
	bls	.L18
	mov	r3, r1
.L27:
	mov	r2, #0
.LBE12:
	add	ip, r0, r3, asl #2
.L18:
	add	r1, ip, r2
	ldr	r1, [r1, #8]
	cmp	r1, #0
	beq	.L19
	flds	s15, [sp, #0]
.L21:
.LBB13:
	cmp	r3, #0
	moveq	r3, #7
	sub	r3, r3, #1
.LBE13:
	add	r2, r3, #2
	flds	s14, [sp, #4]
	ldr	r2, [r0, r2, asl #2]
	cmp	r2, #0
	fadds	s15, s15, s14
	beq	.L21
.L14:
	fmrs	r0, s15
	ldmfd	sp!, {r1, r2, r3, r4, pc}
.L29:
	.align	2
.L28:
	.word	1088421888
.LFE3:
	.size	days_in_this_irrigation_period_for_a_weekly_schedule, .-days_in_this_irrigation_period_for_a_weekly_schedule
	.section	.text.nm_WATERSENSE_days_in_this_irrigation_period,"ax",%progbits
	.align	2
	.global	nm_WATERSENSE_days_in_this_irrigation_period
	.type	nm_WATERSENSE_days_in_this_irrigation_period, %function
nm_WATERSENSE_days_in_this_irrigation_period:
.LFB4:
	@ args = 4, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	fstmfdd	sp!, {d8}
.LCFI3:
	mov	r1, #0
	sub	sp, sp, #72
.LCFI4:
	str	r1, [sp, #48]	@ float
	mov	r1, #1065353216
	str	r1, [sp, #52]	@ float
	mov	r1, #1073741824
	str	r1, [sp, #56]	@ float
	ldr	r1, .L75
	cmp	r0, #0
	str	r1, [sp, #60]	@ float
	ldr	r1, .L75+4
	ldr	r4, [sp, #92]
	str	r1, [sp, #64]	@ float
	ldr	r1, .L75+8
	str	r1, [sp, #68]	@ float
	flds	s16, [sp, #48]
	beq	.L31
	ldr	r0, [r3, #32]
	mov	r1, sp
	bl	FTIMES_FUNCS_load_run_time_calculation_support
	b	.L32
.L31:
	mov	r0, r2
	mov	r1, sp
	bl	SCHEDULE_get_run_time_calculation_support
.L32:
	cmp	r0, #0
	beq	.L33
	ldr	r3, [sp, #0]
	cmp	r3, #0
	beq	.L33
	ldrh	r1, [r4, #10]
	ldrh	r0, [r4, #8]
	bl	NumberOfDaysInMonth
	ldrh	r1, [r4, #10]
	mov	r5, r0
	ldrh	r0, [r4, #8]
	cmp	r0, #1
	subhi	r0, r0, #1
	subls	r1, r1, #1
	movls	r0, #12
	bl	NumberOfDaysInMonth
	ldr	r3, [sp, #4]
	cmp	r3, #18
	ldrls	pc, [pc, r3, asl #2]
	b	.L33
.L44:
	.word	.L68
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L41
	.word	.L41
	.word	.L41
	.word	.L41
	.word	.L41
	.word	.L41
	.word	.L41
	.word	.L41
	.word	.L41
	.word	.L41
	.word	.L41
	.word	.L42
	.word	.L43
.L37:
	ldrh	r3, [r4, #6]
	sub	r2, r5, #1
	cmp	r3, r2
	beq	.L45
	cmp	r3, r5
	bne	.L46
.L45:
	tst	r5, #1
	beq	.L40
.L47:
	cmp	r3, r2
	bne	.L48
	ldr	r1, [r4, #0]
	ldr	r2, [sp, #44]
	cmp	r1, r2
	bhi	.L67
.L48:
	cmp	r3, r5
	b	.L72
.L46:
	sub	r2, r3, #1
	mov	r2, r2, asl #16
	cmp	r2, #65536
	bhi	.L40
	tst	r0, #1
	beq	.L40
.L52:
	cmp	r3, #1
	beq	.L67
	ldr	r2, [r4, #0]
	ldr	r3, [sp, #44]
	cmp	r2, r3
	bhi	.L40
	b	.L67
.L38:
	ldrh	r3, [r4, #6]
	cmp	r3, r5
	bne	.L55
	tst	r5, #1
	beq	.L40
.L56:
	ldr	r3, [sp, #44]
	ldr	r2, [r4, #0]
	cmp	r2, r3
	ldrhi	r3, [sp, #36]
	bhi	.L70
	b	.L40
.L55:
	cmp	r3, #3
	bhi	.L40
	tst	r0, #1
	beq	.L40
.L60:
	cmp	r3, #1
	bne	.L61
	ldr	r3, [sp, #44]
	ldr	r2, [r4, #0]
	cmp	r2, r3
	ldr	r3, [sp, #36]
	bhi	.L74
.L70:
	cmp	r3, #0
	beq	.L67
.L68:
	flds	s16, [sp, #52]
	b	.L33
.L61:
	cmp	r3, #2
	beq	.L65
	cmp	r3, #3
	bne	.L40
	ldr	r2, [r4, #0]
	ldr	r3, [sp, #44]
	cmp	r2, r3
	bhi	.L40
.L65:
	ldr	r3, [sp, #36]
.L74:
	cmp	r3, #0
.L72:
	bne	.L40
.L67:
	flds	s16, [sp, #60]
	b	.L33
.L40:
	flds	s16, [sp, #56]
	b	.L33
.L39:
	mov	r0, sp
	mov	r1, r4
	bl	days_in_this_irrigation_period_for_a_weekly_schedule
	fmsr	s16, r0
	b	.L33
.L41:
	sub	r3, r3, #2
	fmsr	s15, r3	@ int
	fuitos	s16, s15
	b	.L33
.L42:
	flds	s16, [sp, #64]
	b	.L33
.L43:
	flds	s16, [sp, #68]
.L33:
	fmrs	r0, s16
	add	sp, sp, #72
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, pc}
.L76:
	.align	2
.L75:
	.word	1077936128
	.word	1101529088
	.word	1105199104
.LFE4:
	.size	nm_WATERSENSE_days_in_this_irrigation_period, .-nm_WATERSENSE_days_in_this_irrigation_period
	.global	__udivsi3
	.section	.text.WATERSENSE_get_et_value_after_substituting_or_limiting,"ax",%progbits
	.align	2
	.global	WATERSENSE_get_et_value_after_substituting_or_limiting
	.type	WATERSENSE_get_et_value_after_substituting_or_limiting, %function
WATERSENSE_get_et_value_after_substituting_or_limiting:
.LFB6:
	@ args = 4, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI5:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI6:
	mov	r6, r3
	sub	sp, sp, #28
.LCFI7:
	ldr	r7, [sp, #84]
	mov	r4, r2
	sub	r3, r7, #1
	rsbs	r7, r3, #0
	adc	r7, r7, r3
	cmp	r0, #0
	movne	r5, #0
	andeq	r5, r7, #1
	cmp	r5, #0
	beq	.L78
	mov	r0, r1
	bl	STATION_get_GID_station_group
	bl	STATION_GROUP_get_group_with_this_GID
	mov	r5, r0
.L78:
	mov	r0, r6
	add	r1, sp, #20
	bl	WEATHER_TABLES_get_et_table_entry_for_index
	cmp	r0, #1
	bne	.L90
	ldrh	r3, [r4, #6]
	ldrh	r8, [sp, #20]
	cmp	r3, #1
	bne	.L80
.LBB14:
	ldrh	r0, [r4, #4]
	ldr	r1, [r4, #0]
	sub	r0, r0, #1
	mov	r2, sp
	bl	DateAndTimeToDTCS
	ldrh	r0, [sp, #8]
	bl	ET_DATA_et_number_for_the_month
	ldrh	r1, [sp, #10]
	fmsr	s16, r0
	ldrh	r0, [sp, #8]
	b	.L96
.L80:
.LBE14:
	ldrh	r0, [r4, #8]
	bl	ET_DATA_et_number_for_the_month
	ldrh	r1, [r4, #10]
	fmsr	s16, r0
	ldrh	r0, [r4, #8]
.L96:
	bl	NumberOfDaysInMonth
	fmsr	s14, r0	@ int
	fuitos	s15, s14
	fdivs	s16, s16, s15
	bl	WEATHER_get_percent_of_historical_cap_100u
	ldrh	r3, [sp, #22]
	cmp	r3, #0
	fcvtds	d5, s16
	fmsr	s15, r0	@ int
	fuitod	d6, s15
	fldd	d7, .L97
	fcpyd	d10, d7
	fdivd	d6, d6, d7
	fmuld	d6, d5, d6
	fcvtsd	s17, d6
	bne	.L82
.LBB15:
	ldr	r3, .L97+16
	ldr	r9, .L97+20
	flds	s18, [r3, #0]
	mov	sl, #1
	fmuls	s18, s16, s18
.L87:
	mov	r0, sl
	add	r1, sp, #24
	bl	WEATHER_TABLES_get_et_table_entry_for_index
	cmp	r0, #1
	bne	.L83
	ldrh	r3, [sp, #26]
	cmp	r3, #1
	cmpne	r3, #4
	beq	.L84
	cmp	r3, #5
	bne	.L83
.L84:
	ldrh	r3, [sp, #24]
	flds	s15, [r9, #0]
	fmsr	s14, r3	@ int
	fuitos	s16, s14
	fdivs	s16, s16, s15
	fcmpes	s16, s18
	fmstat
	blt	.L83
	fcmpes	s16, s17
	fmstat
	bmi	.L86
.L83:
	add	sl, sl, #1
	cmp	sl, #5
	bne	.L87
	b	.L82
.L86:
.LBE15:
	cmp	r5, #0
	moveq	r7, #0
	andne	r7, r7, #1
	cmp	r7, #0
	beq	.L79
	mov	r0, r5
	bl	nm_GROUP_get_name
	ldrh	r3, [r4, #4]
	mov	r1, #100
	mvn	r6, r6
	add	r6, r6, r3
	mov	r5, r0
	ldrh	r0, [sp, #20]
	bl	__udivsi3
	fcvtds	d7, s16
	mov	r1, r6
	fmuld	d10, d7, d10
	ftouizd	s15, d10
	fmrs	r3, s15	@ int
	mov	r2, r0, asl #16
	mov	r2, r2, lsr #16
	mov	r0, r5
	bl	Alert_ET_Table_substitution_100u
	b	.L79
.L82:
	fmsr	s14, r8	@ int
	flds	s15, .L97+8
	fuitos	s16, s14
	fdivs	s16, s16, s15
	fcmpes	s16, s17
	fmstat
	ble	.L79
	cmp	r5, #0
	moveq	r7, #0
	andne	r7, r7, #1
	cmp	r7, #0
	beq	.L92
	mov	r0, r5
	bl	nm_GROUP_get_name
	ldrh	r3, [r4, #4]
	mov	r1, #100
	mvn	r6, r6
	add	r6, r6, r3
	mov	r5, r0
	ldrh	r0, [sp, #20]
	bl	__udivsi3
	fcvtds	d7, s17
	mov	r1, r6
	fmuld	d10, d7, d10
	ftouizd	s15, d10
	fmrs	r3, s15	@ int
	mov	r2, r0, asl #16
	mov	r2, r2, lsr #16
	mov	r0, r5
	bl	Alert_ET_Table_limited_entry_100u
	b	.L92
.L90:
	flds	s16, .L97+12
	b	.L79
.L92:
	fcpys	s16, s17
.L79:
	fmrs	r0, s16
	add	sp, sp, #28
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L98:
	.align	2
.L97:
	.word	0
	.word	1079574528
	.word	1176256512
	.word	0
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE6:
	.size	WATERSENSE_get_et_value_after_substituting_or_limiting, .-WATERSENSE_get_et_value_after_substituting_or_limiting
	.section	.text.nm_WATERSENSE_average_et_in_this_irrigation_period,"ax",%progbits
	.align	2
	.global	nm_WATERSENSE_average_et_in_this_irrigation_period
	.type	nm_WATERSENSE_average_et_in_this_irrigation_period, %function
nm_WATERSENSE_average_et_in_this_irrigation_period:
.LFB7:
	@ args = 4, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI8:
	fstmfdd	sp!, {d8}
.LCFI9:
	mov	r7, r3
	sub	sp, sp, #8
.LCFI10:
	mov	r3, #0
	str	r3, [sp, #4]	@ float
	subs	r6, r2, #0
	mov	r8, r0
	mov	r5, r1
	ldr	sl, [sp, #44]
	flds	s16, [sp, #4]
	beq	.L100
	mov	r4, #0
.L101:
	add	r4, r4, #1
	mov	r0, r8
	mov	r1, r5
	mov	r2, r7
	mov	r3, r4
	str	sl, [sp, #0]
	bl	WATERSENSE_get_et_value_after_substituting_or_limiting
	cmp	r4, r6
	fmsr	s14, r0
	fadds	s16, s16, s14
	bne	.L101
	fmsr	s14, r4	@ int
	fuitos	s15, s14
	fdivs	s16, s16, s15
.L100:
	fmrs	r0, s16
	add	sp, sp, #8
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.LFE7:
	.size	nm_WATERSENSE_average_et_in_this_irrigation_period, .-nm_WATERSENSE_average_et_in_this_irrigation_period
	.global	__umodsi3
	.section	.text.WATERSENSE_make_min_cycle_adjustment,"ax",%progbits
	.align	2
	.global	WATERSENSE_make_min_cycle_adjustment
	.type	WATERSENSE_make_min_cycle_adjustment, %function
WATERSENSE_make_min_cycle_adjustment:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI11:
	orrs	r6, r2, r0
	mov	r4, r0
	mov	r5, r1
	mov	r7, r3
	bne	.L104
	ldr	r0, .L109
	bl	Alert_Message
	b	.L105
.L104:
	ldr	r6, [r1, #0]
	cmp	r6, #180
	cmphi	r2, #180
	movls	r3, #0
	movhi	r8, #0
	movls	r8, #1
	strls	r3, [r1, #0]
	bls	.L105
	mov	r0, r6
	mov	r1, r2
	bl	__umodsi3
	sub	r3, r0, #1
	cmp	r3, #179
	movhi	r6, r8
	bhi	.L105
	adds	r3, r7, #0
	movne	r3, #1
	cmp	r4, #0
	movne	r3, #0
	cmp	r3, #0
	rsb	r6, r0, r6
	str	r6, [r5, #0]
	ldrneb	r3, [r7, #21]	@ zero_extendqisi2
	mov	r6, r0
	orrne	r3, r3, #1
	strneb	r3, [r7, #21]
.L105:
	mov	r0, r6
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L110:
	.align	2
.L109:
	.word	.LC0
.LFE8:
	.size	WATERSENSE_make_min_cycle_adjustment, .-WATERSENSE_make_min_cycle_adjustment
	.section	.text.nm_WATERSENSE_calculate_run_minutes_for_this_station,"ax",%progbits
	.align	2
	.global	nm_WATERSENSE_calculate_run_minutes_for_this_station
	.type	nm_WATERSENSE_calculate_run_minutes_for_this_station, %function
nm_WATERSENSE_calculate_run_minutes_for_this_station:
.LFB9:
	@ args = 8, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI12:
	fstmfdd	sp!, {d8, d9, d10}
.LCFI13:
	mov	r6, r3
	sub	sp, sp, #24
.LCFI14:
	mov	r3, #0
	str	r3, [sp, #4]	@ float
	ldr	r3, .L135
	cmp	r0, #1
	str	r3, [sp, #8]	@ float
	ldr	r3, .L135+4
	mov	r5, r0
	str	r3, [sp, #12]	@ float
	ldr	r3, .L135+8
	mov	r4, r1
	str	r3, [sp, #16]	@ float
	ldr	r3, .L135+12
	mov	r8, r2
	str	r3, [sp, #20]	@ float
	ldr	r7, [sp, #72]
	flds	s16, [sp, #4]
	bne	.L112
	cmp	r6, #0
	bne	.L114
	b	.L113
.L112:
	cmp	r1, #0
	beq	.L113
	cmp	r2, #0
	beq	.L113
	mov	r0, r1
	bl	STATION_station_is_available_for_use
	cmp	r0, #0
	beq	.L113
	b	.L134
.L114:
	ldr	r3, [r6, #32]
	ldr	r0, [r3, #140]
	b	.L116
.L131:
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
.L116:
	cmp	r0, #0
	beq	.L117
	mov	r1, r4
	mov	r2, r8
	mov	r3, r6
	mov	r0, r5
	flds	s20, [sp, #16]
	str	r7, [sp, #0]
	bl	nm_WATERSENSE_days_in_this_irrigation_period
	fmsr	s18, r0
	bl	roundf
	mov	r3, #0
	str	r3, [sp, #0]
	mov	r1, r4
	mov	r3, r7
	fmsr	s14, r0
	mov	r0, r5
	ftouizs	s14, s14
	fmrs	r2, s14	@ int
	bl	nm_WATERSENSE_average_et_in_this_irrigation_period
	cmp	r5, #0
	fmsr	s19, r0
	beq	.L118
	ldr	r3, [r6, #32]
	flds	s15, [sp, #20]
	ldrh	r2, [r7, #8]
	flds	s14, [r3, #16]	@ int
	add	r2, r2, #4
	ldr	r3, [r3, r2, asl #2]
	fuitos	s17, s14
	fmsr	s14, r3	@ int
	fdivs	s17, s17, s15
	b	.L132
.L118:
	mov	r0, r4
	bl	STATION_GROUP_get_station_precip_rate_in_per_hr_100000u
	flds	s15, [sp, #20]
	ldrh	r1, [r7, #8]
	fmsr	s14, r0	@ int
	mov	r0, r4
	fuitos	s17, s14
	fdivs	s17, s17, s15
	bl	STATION_GROUP_get_station_crop_coefficient_100u
	fmsr	s14, r0	@ int
.L132:
	fuitos	s15, s14
	flds	s14, [sp, #16]
	flds	s13, [sp, #4]
	fcmpes	s18, s13
	fmstat
	ble	.L113
	fdivs	s15, s15, s14
	fmuls	s19, s15, s19
	flds	s15, [sp, #4]
	fcmpes	s19, s15
	fmstat
	ble	.L113
	flds	s15, [sp, #4]
	fcmpes	s17, s15
	fmstat
	ble	.L113
	flds	s15, [sp, #76]
	fdivs	s20, s15, s20
	flds	s15, [sp, #4]
	fcmpes	s20, s15
	fmstat
	fldsgt	s16, [sp, #12]
	fmulsgt	s19, s19, s18
	fdivsgt	s17, s17, s16
	fdivsgt	s16, s19, s17
	fdivsgt	s16, s16, s20
	b	.L113
.L117:
	cmp	r5, #0
	ldrne	r3, [r6, #52]
	fldsne	s15, [sp, #12]
	fmsrne	s14, r3	@ int
	bne	.L133
	mov	r0, r4
	bl	STATION_get_total_run_minutes_10u
	flds	s15, [sp, #8]
	fmsr	s14, r0	@ int
.L133:
	fuitos	s16, s14
	fdivs	s16, s16, s15
.L113:
	fmrs	r0, s16
	add	sp, sp, #24
	fldmfdd	sp!, {d8, d9, d10}
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L134:
	cmp	r5, #0
	beq	.L131
	b	.L114
.L136:
	.align	2
.L135:
	.word	1092616192
	.word	1114636288
	.word	1120403456
	.word	1203982336
.LFE9:
	.size	nm_WATERSENSE_calculate_run_minutes_for_this_station, .-nm_WATERSENSE_calculate_run_minutes_for_this_station
	.section	.text.nm_WATERSENSE_calculate_adjusted_run_time,"ax",%progbits
	.align	2
	.global	nm_WATERSENSE_calculate_adjusted_run_time
	.type	nm_WATERSENSE_calculate_adjusted_run_time, %function
nm_WATERSENSE_calculate_adjusted_run_time:
.LFB10:
	@ args = 8, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI15:
	fstmfdd	sp!, {d8}
.LCFI16:
	fmsr	s17, r3
	mov	r3, #0
	sub	sp, sp, #12
.LCFI17:
	str	r3, [sp, #0]	@ float
	ldr	r3, .L144
	cmp	r0, #0
	str	r3, [sp, #4]	@ float
	ldr	r3, .L144+4
	mov	r4, r1
	str	r3, [sp, #8]	@ float
	ldrne	r3, [r2, #32]
	flds	s16, [sp, #0]
	ldrne	r0, [r3, #140]
	bne	.L139
	mov	r0, r1
	bl	STATION_station_is_available_for_use
	mov	r5, r0
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	cmp	r5, #0
	beq	.L140
.L139:
	cmp	r0, #0
	fldsne	s13, [sp, #32]	@ int
	fldsne	s15, [sp, #8]
	fuitosne	s14, s13
	fdivsne	s15, s14, s15
	flds	s14, [sp, #36]	@ int
	fuitos	s16, s14
	fmulsne	s17, s17, s15
	flds	s15, [sp, #8]
	fdivs	s16, s16, s15
	flds	s15, [sp, #4]
	fmuls	s16, s17, s16
	fcmpes	s16, s15
	fmstat
	fldsmi	s16, [sp, #0]
.L140:
	fmrs	r0, s16
	add	sp, sp, #12
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, pc}
.L145:
	.align	2
.L144:
	.word	1036831949
	.word	1120403456
.LFE10:
	.size	nm_WATERSENSE_calculate_adjusted_run_time, .-nm_WATERSENSE_calculate_adjusted_run_time
	.section	.text.IRRITIME_make_ETTable_substitute_and_limit_alerts_after_we_know_program_ran,"ax",%progbits
	.align	2
	.global	IRRITIME_make_ETTable_substitute_and_limit_alerts_after_we_know_program_ran
	.type	IRRITIME_make_ETTable_substitute_and_limit_alerts_after_we_know_program_ran, %function
IRRITIME_make_ETTable_substitute_and_limit_alerts_after_we_know_program_ran:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #0
	cmpne	r0, #0
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI18:
	mov	r5, r2
	mov	r4, r1
	mov	r6, r0
	beq	.L147
	cmp	r2, #0
	beq	.L147
	ldr	r3, .L149
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L149+4
	ldr	r3, .L149+8
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #0
	beq	.L148
	mov	r0, #0
	mov	r1, r4
	mov	r2, r6
	mov	r3, r0
	str	r5, [sp, #0]
	bl	nm_WATERSENSE_days_in_this_irrigation_period
	bl	roundf
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r1, r4
	mov	r3, r5
	fmsr	s15, r0
	mov	r0, #0
	ftouizs	s15, s15
	fmrs	r2, s15	@ int
	bl	nm_WATERSENSE_average_et_in_this_irrigation_period
.L148:
	ldr	r3, .L149
	ldr	r0, [r3, #0]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	xQueueGiveMutexRecursive
.L147:
	ldr	r0, .L149+4
	ldr	r1, .L149+12
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	Alert_func_call_with_null_ptr_with_filename
.L150:
	.align	2
.L149:
	.word	list_program_data_recursive_MUTEX
	.word	.LC1
	.word	1225
	.word	1245
.LFE11:
	.size	IRRITIME_make_ETTable_substitute_and_limit_alerts_after_we_know_program_ran, .-IRRITIME_make_ETTable_substitute_and_limit_alerts_after_we_know_program_ran
	.section	.data.TWENTY_PERCENT.7506,"aw",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	TWENTY_PERCENT.7506, %object
	.size	TWENTY_PERCENT.7506, 4
TWENTY_PERCENT.7506:
	.word	1045220557
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"WaterSense: cycle=0 (out or range)\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/irri_time.c\000"
	.section	.data.TEN_THOUSAND.7507,"aw",%progbits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	TEN_THOUSAND.7507, %object
	.size	TEN_THOUSAND.7507, 4
TEN_THOUSAND.7507:
	.word	1176256512
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI1-.LFB3
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x5c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI5-.LFB6
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x38
	.byte	0x5
	.uleb128 0x54
	.uleb128 0xa
	.byte	0x5
	.uleb128 0x52
	.uleb128 0xc
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xe
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x54
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI8-.LFB7
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x24
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI11-.LFB8
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI12-.LFB9
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xe
	.uleb128 0x30
	.byte	0x5
	.uleb128 0x54
	.uleb128 0x8
	.byte	0x5
	.uleb128 0x52
	.uleb128 0xa
	.byte	0x5
	.uleb128 0x50
	.uleb128 0xc
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI15-.LFB10
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x14
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI18-.LFB11
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/irri_time.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xf1
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF12
	.byte	0x1
	.4byte	.LASF13
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x5c
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x1fd
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0x70
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xe0
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x245
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST3
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x2bb
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST4
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x361
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST5
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x3ae
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST6
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x45d
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST7
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x4c1
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x3
	.byte	0x7d
	.sleb128 92
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB6
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	.LCFI7
	.4byte	.LFE6
	.2byte	0x3
	.byte	0x7d
	.sleb128 84
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI10
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB8
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB9
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	.LCFI14
	.4byte	.LFE9
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB10
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI16
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI17
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB11
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF6:
	.ascii	"WATERSENSE_get_et_value_after_substituting_or_limit"
	.ascii	"ing\000"
.LASF10:
	.ascii	"nm_WATERSENSE_calculate_adjusted_run_time\000"
.LASF8:
	.ascii	"WATERSENSE_make_min_cycle_adjustment\000"
.LASF13:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/irri_time.c\000"
.LASF2:
	.ascii	"get_substitute_et_value\000"
.LASF9:
	.ascii	"nm_WATERSENSE_calculate_run_minutes_for_this_statio"
	.ascii	"n\000"
.LASF12:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"days_in_this_irrigation_period_for_a_weekly_schedul"
	.ascii	"e\000"
.LASF3:
	.ascii	"nm_IRRITIME_this_program_is_scheduled_to_run\000"
.LASF5:
	.ascii	"nm_WATERSENSE_days_in_this_irrigation_period\000"
.LASF0:
	.ascii	"increment_day_in_weekly_array\000"
.LASF11:
	.ascii	"IRRITIME_make_ETTable_substitute_and_limit_alerts_a"
	.ascii	"fter_we_know_program_ran\000"
.LASF1:
	.ascii	"decrement_day_in_weekly_array\000"
.LASF7:
	.ascii	"nm_WATERSENSE_average_et_in_this_irrigation_period\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
