	.file	"device_GENERIC_FREEWAVE_card.c"
	.text
.Ltext0:
	.section	.text.GENERIC_freewave_card_power_control,"ax",%progbits
	.align	2
	.global	GENERIC_freewave_card_power_control
	.type	GENERIC_freewave_card_power_control, %function
GENERIC_freewave_card_power_control:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, lr}
.LCFI0:
	bne	.L2
	ldr	r4, .L8
	cmp	r1, #0
	ldr	r3, [r4, #12]
	beq	.L3
	tst	r3, #524288
	ldmnefd	sp!, {r4, pc}
	ldr	r3, .L8+4
	ldr	r2, [r3, #80]
	ldr	r3, .L8+8
	ldr	r2, [r3, r2, asl #2]
	bl	Alert_device_powered_on_or_off
	mov	r3, #524288
	b	.L7
.L3:
	tst	r3, #524288
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L8+4
	ldr	r2, [r3, #80]
	ldr	r3, .L8+8
	ldr	r2, [r3, r2, asl #2]
	bl	Alert_device_powered_on_or_off
	mov	r3, #524288
	b	.L6
.L2:
	cmp	r0, #2
	ldmnefd	sp!, {r4, pc}
	ldr	r4, .L8
	cmp	r1, #0
	ldr	r3, [r4, #12]
	beq	.L5
	tst	r3, #4194304
	ldmnefd	sp!, {r4, pc}
	ldr	r3, .L8+4
	ldr	r2, [r3, #84]
	ldr	r3, .L8+8
	ldr	r2, [r3, r2, asl #2]
	bl	Alert_device_powered_on_or_off
	mov	r3, #4194304
.L7:
	str	r3, [r4, #4]
	ldmfd	sp!, {r4, pc}
.L5:
	tst	r3, #4194304
	ldmeqfd	sp!, {r4, pc}
	ldr	r3, .L8+4
	ldr	r2, [r3, #84]
	ldr	r3, .L8+8
	ldr	r2, [r3, r2, asl #2]
	bl	Alert_device_powered_on_or_off
	mov	r3, #4194304
.L6:
	str	r3, [r4, #8]
	ldmfd	sp!, {r4, pc}
.L9:
	.align	2
.L8:
	.word	1073905664
	.word	config_c
	.word	comm_device_names
.LFE0:
	.size	GENERIC_freewave_card_power_control, .-GENERIC_freewave_card_power_control
	.section	.text.GENERIC_freewave_card_is_connected,"ax",%progbits
	.align	2
	.global	GENERIC_freewave_card_is_connected
	.type	GENERIC_freewave_card_is_connected, %function
GENERIC_freewave_card_is_connected:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #1
	ldr	r3, .L18
	ldr	r1, .L18+4
	mov	r2, #56
	bne	.L11
	ldr	r1, [r1, #80]
	mla	r3, r2, r1, r3
	ldr	r3, [r3, #12]
	cmp	r3, #0
	ldr	r3, .L18+8
	ldr	r3, [r3, #0]
	beq	.L12
	tst	r3, #32
	moveq	r3, #0
	movne	r3, #1
	b	.L13
.L12:
	tst	r3, #32
	movne	r3, #0
	moveq	r3, #1
.L13:
	ldr	r2, .L18+12
	b	.L17
.L11:
	ldr	r1, [r1, #84]
	mla	r3, r2, r1, r3
	ldr	r3, [r3, #12]
	cmp	r3, #0
	ldr	r3, .L18+8
	ldr	r3, [r3, #0]
	beq	.L15
	tst	r3, #4
	moveq	r3, #0
	movne	r3, #1
	b	.L16
.L15:
	tst	r3, #4
	movne	r3, #0
	moveq	r3, #1
.L16:
	ldr	r2, .L18+16
.L17:
	ldr	r1, .L18+20
	strb	r3, [r1, r2]
	ldr	r2, .L18+24
	ldr	r3, .L18+20
	mla	r0, r2, r0, r3
	add	r0, r0, #20
	ldrb	r0, [r0, #3]	@ zero_extendqisi2
	bx	lr
.L19:
	.align	2
.L18:
	.word	port_device_table
	.word	config_c
	.word	1073905664
	.word	4303
	.word	8583
	.word	SerDrvrVars_s
	.word	4280
.LFE1:
	.size	GENERIC_freewave_card_is_connected, .-GENERIC_freewave_card_is_connected
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/serial_drvr/device_GENERIC_FREEWAVE_card.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x45
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x5d
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"GENERIC_freewave_card_is_connected\000"
.LASF0:
	.ascii	"GENERIC_freewave_card_power_control\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/seri"
	.ascii	"al_drvr/device_GENERIC_FREEWAVE_card.c\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
