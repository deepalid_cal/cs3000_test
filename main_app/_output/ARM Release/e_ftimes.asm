	.file	"e_ftimes.c"
	.text
.Ltext0:
	.section	.text.DATE_TIME_to_Finish_Times_format.constprop.0,"ax",%progbits
	.align	2
	.type	DATE_TIME_to_Finish_Times_format.constprop.0, %function
DATE_TIME_to_Finish_Times_format.constprop.0:
.LFB7:
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	sub	sp, sp, #68
.LCFI1:
	add	r3, sp, #12
	stmia	r3, {r1, r2}
	add	r3, sp, #64
	mov	r4, r0
	str	r3, [sp, #0]
	ldrh	r0, [sp, #16]
	add	r1, sp, #52
	add	r2, sp, #56
	add	r3, sp, #60
	bl	DateToDMY
	mov	r3, #0
	ldr	r2, [sp, #12]
	add	r0, sp, #20
	mov	r1, #32
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	bl	TDUTILS_time_to_time_string_with_ampm
	mov	r1, r0
	add	r0, sp, #20
	bl	ShaveLeftPad
	mov	r5, r0
	ldr	r0, [sp, #64]
	bl	GetDayShortStr
	mov	r6, r0
	ldr	r0, [sp, #56]
	sub	r0, r0, #1
	bl	GetMonthShortStr
	ldr	r3, [sp, #52]
	mov	r1, #65
	str	r3, [sp, #8]
	ldr	r2, .L2
	mov	r3, r5
	str	r6, [sp, #0]
	str	r0, [sp, #4]
	mov	r0, r4
	bl	snprintf
	mov	r0, r4
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, pc}
.L3:
	.align	2
.L2:
	.word	.LC0
.LFE7:
	.size	DATE_TIME_to_Finish_Times_format.constprop.0, .-DATE_TIME_to_Finish_Times_format.constprop.0
	.global	__udivsi3
	.section	.text.FINISH_TIMES_draw_scroll_line,"ax",%progbits
	.align	2
	.type	FINISH_TIMES_draw_scroll_line, %function
FINISH_TIMES_draw_scroll_line:
.LFB3:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L26
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI2:
	ldr	r7, .L26+4
	mov	r5, r0, asl #16
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L26+8
	mov	r3, #236
	mov	r6, #0
	str	r6, [r7, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L26+12
	bl	nm_ListGetFirst
	mov	r5, r5, asr #16
	ldr	r8, .L26+12
	ldr	r9, .L26+16
	mov	r4, r0
	b	.L5
.L20:
	ldr	r0, [r4, #180]
	bl	SYSTEM_get_index_for_group_with_this_GID
	ldr	r3, [r9, #0]
	cmp	r0, r3
	bne	.L6
	ldr	r3, [r7, #0]
	add	r3, r3, #1
	str	r3, [r7, #0]
	ldr	r3, [r4, #244]
	cmp	r3, #0
	ldr	r3, [r4, #252]
	beq	.L7
	ldr	r2, [r4, #248]
	cmp	r2, #0
	beq	.L7
	cmp	r3, #0
	ldr	r3, .L26+20
	movne	r2, #460
	strne	r2, [r3, #0]
	movne	r2, #0
	bne	.L21
	ldr	r2, [r3, #0]
	cmp	r2, #260
	movcc	r2, #260
	strcc	r2, [r3, #0]
	movcc	r2, #1
	bcc	.L21
	b	.L9
.L7:
	cmp	r3, #0
	beq	.L9
	ldr	r3, .L26+20
	ldr	r2, [r3, #0]
	cmp	r2, #280
	bcs	.L9
	mov	r2, #280
	str	r2, [r3, #0]
	mov	r2, #2
.L21:
	ldr	r3, .L26+24
	str	r2, [r3, #0]
.L9:
	ldr	r3, .L26+28
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L10
	ldr	r0, [r4, #12]
	bl	STATION_GROUP_get_index_for_group_with_this_GID
	mov	r3, r5, asl #16
	cmp	r0, r3, asr #16
	bne	.L11
	ldr	r3, .L26+32
	ldr	r0, [r4, #12]
	str	r0, [r3, #0]
	bl	STATION_GROUP_get_group_with_this_GID
	bl	nm_GROUP_get_name
	mov	r2, #21
	mov	r1, r0
	ldr	r0, .L26+36
	bl	strlcpy
	mov	r0, #1
	mov	r1, r0
	ldr	r2, .L26+40
	ldrh	sl, [r4, #234]
	bl	DMYToDate
	mov	r0, r0, asl #16
	cmp	sl, r0, lsr #16
	bne	.L12
	mov	r1, #65
	ldr	r2, .L26+44
	ldr	r0, .L26+48
	bl	snprintf
	mov	r1, #65
	ldr	r2, .L26+52
	ldr	r0, .L26+56
	bl	snprintf
	b	.L13
.L12:
	ldrh	r3, [r4, #230]
	ldrh	r1, [r4, #232]
	ldrh	r2, [r4, #234]
	orr	r1, r3, r1, asl #16
	ldr	r0, .L26+48
	bl	DATE_TIME_to_Finish_Times_format.constprop.0
	add	r3, r4, #224
	ldr	r0, .L26+56
	ldmia	r3, {r1, r2}
	bl	DATE_TIME_to_Finish_Times_format.constprop.0
.L13:
	ldr	r3, [r4, #244]
	cmp	r3, #1
	bne	.L14
	ldr	sl, [r4, #248]
	cmp	sl, #0
	beq	.L14
	cmp	sl, #3600
	bcc	.L15
	add	r0, sl, #1792
	mov	r1, #3600
	add	r0, r0, #8
	bl	__udivsi3
	mov	r1, #4
	ldr	r2, .L26+60
	mov	sl, r0
	add	r0, sp, #4
	b	.L22
.L15:
	cmp	sl, #59
	bls	.L17
	add	r0, sl, #30
	mov	r1, #60
	bl	__udivsi3
	mov	r1, #4
	ldr	r2, .L26+64
	mov	sl, r0
	add	r0, sp, #4
	b	.L22
.L17:
	ldr	r2, .L26+68
	add	r0, sp, #4
	mov	r1, #4
.L22:
	bl	snprintf
	ldr	r3, [r4, #252]
	mov	r1, #101
	cmp	r3, #1
	add	r3, sp, #4
	str	r3, [sp, #0]
	ldreq	r2, .L26+72
	ldrne	r2, .L26+76
	mov	r3, sl
	ldr	r0, .L26+80
	bl	snprintf
	b	.L11
.L14:
	ldr	r3, [r4, #252]
	ldr	r0, .L26+80
	cmp	r3, #1
	mov	r1, #101
	ldreq	r2, .L26+84
	bne	.L23
	b	.L25
.L10:
	mov	r1, #21
	ldr	r2, .L26+52
	ldr	r0, .L26+36
	bl	snprintf
	mov	r1, #65
	ldr	r2, .L26+52
	ldr	r0, .L26+48
	bl	snprintf
	mov	r1, #101
	ldr	r0, .L26+80
	ldr	r2, .L26+52
	bl	snprintf
	ldr	r0, .L26+56
	mov	r1, #65
.L23:
	ldr	r2, .L26+52
.L25:
	bl	snprintf
	b	.L11
.L6:
	ldr	r0, [r4, #12]
	bl	STATION_GROUP_get_index_for_group_with_this_GID
	mov	r3, r5, asl #16
	cmp	r0, r3, asr #16
	addeq	r5, r5, #1
	moveq	r5, r5, asl #16
	moveq	r5, r5, lsr #16
.L11:
	mov	r1, r4
	ldr	r0, .L26+12
	bl	nm_ListGetNext
	add	r6, r6, #1
	mov	r4, r0
.L5:
	ldr	r3, [r8, #8]
	cmp	r6, r3
	bcc	.L20
	ldr	r3, .L26
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	ldmfd	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, pc}
.L27:
	.align	2
.L26:
	.word	list_program_data_recursive_MUTEX
	.word	.LANCHOR0
	.word	.LC1
	.word	ft_station_groups_list_hdr
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	GuiVar_FinishTimesScrollBoxH
	.word	.LANCHOR3
	.word	.LANCHOR4
	.word	GuiVar_FinishTimesStationGroup
	.word	2011
	.word	.LC2
	.word	GuiVar_FinishTimes
	.word	.LC3
	.word	GuiVar_FinishTimesStartTime
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	GuiVar_FinishTimesNotes
	.word	.LC9
.LFE3:
	.size	FINISH_TIMES_draw_scroll_line, .-FINISH_TIMES_draw_scroll_line
	.section	.text.FDTO_FINISH_TIMES_redraw_scrollbox,"ax",%progbits
	.align	2
	.type	FDTO_FINISH_TIMES_redraw_scrollbox, %function
FDTO_FINISH_TIMES_redraw_scrollbox:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L29
	stmfd	sp!, {r4, lr}
.LCFI3:
	mov	r4, #0
	str	r4, [r3, #0]
	ldr	r3, .L29+4
	mov	r2, #40
	str	r4, [r3, #0]
	ldr	r3, .L29+8
	mov	r0, r4
	str	r4, [r3, #0]
	ldr	r3, .L29+12
	str	r2, [r3, #0]
	ldr	r3, .L29+16
	mov	r2, #3
	str	r2, [r3, #0]
	ldr	r3, .L29+20
	ldr	r2, [r3, #60]
	ldr	r3, .L29+24
	str	r2, [r3, #0]
	bl	FINISH_TIMES_draw_scroll_line
	ldr	r3, .L29+28
	mov	r0, r4
	ldr	r1, [r3, #0]
	mov	r2, #1
	ldmfd	sp!, {r4, lr}
	b	FDTO_SCROLL_BOX_redraw_retaining_topline
.L30:
	.align	2
.L29:
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	GuiVar_ScrollBoxHorizScrollMarker
	.word	.LANCHOR5
	.word	.LANCHOR2
	.word	GuiVar_FinishTimesScrollBoxH
	.word	ftcs
	.word	.LANCHOR6
	.word	.LANCHOR0
.LFE5:
	.size	FDTO_FINISH_TIMES_redraw_scrollbox, .-FDTO_FINISH_TIMES_redraw_scrollbox
	.section	.text.FINISH_TIMES_draw_dialog,"ax",%progbits
	.align	2
	.global	FINISH_TIMES_draw_dialog
	.type	FINISH_TIMES_draw_dialog, %function
FINISH_TIMES_draw_dialog:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L34
	ldr	r2, .L34+4
	ldr	r1, [r3, #0]
	cmp	r1, r2
	ldr	r2, .L34+8
	ldr	r1, .L34+12
	bne	.L32
	flds	s15, [r3, #52]
	ftouizs	s14, s15
	fadds	s15, s15, s15
	fsts	s14, [r2, #0]	@ int
	ftouizs	s15, s15
	fsts	s15, [r1, #0]	@ int
	b	.L33
.L32:
	flds	s15, [r3, #52]
	mov	r0, #0
	str	r0, [r1, #0]
	ftouizs	s15, s15
	fsts	s15, [r2, #0]	@ int
.L33:
	ldr	r0, .L34+16
	b	DIALOG_draw_ok_dialog
.L35:
	.align	2
.L34:
	.word	ftcs
	.word	4097
	.word	GuiVar_FinishTimesCalcPercentComplete
	.word	GuiVar_FinishTimesCalcProgressBar
	.word	603
.LFE0:
	.size	FINISH_TIMES_draw_dialog, .-FINISH_TIMES_draw_dialog
	.section	.text.FDTO_FINISH_TIMES_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_FINISH_TIMES_draw_screen
	.type	FDTO_FINISH_TIMES_draw_screen, %function
FDTO_FINISH_TIMES_draw_screen:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L46
	stmfd	sp!, {r0, r4, r5, lr}
.LCFI4:
	ldr	r2, .L46+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r3, .L46+8
	bl	xQueueTakeMutexRecursive_debug
	cmp	r4, #1
	bne	.L37
	ldr	r3, .L46+12
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r3, .L46+16
	mov	r2, #40
	str	r2, [r3, #0]
	ldr	r3, .L46+20
	mov	r2, #3
	str	r2, [r3, #0]
.L37:
	mov	r0, #0
	bl	FINISH_TIMES_draw_scroll_line
	bl	SYSTEM_num_systems_in_use
	cmp	r0, #1
	bls	.L38
	mvn	r1, #0
	mov	r2, #1
	mov	r0, #80
	bl	GuiLib_ShowScreen
	ldr	r3, .L46+24
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_group_at_this_index
	bl	nm_GROUP_get_name
	mov	r1, #21
	ldr	r2, .L46+28
	mov	r3, r0
	ldr	r0, .L46+32
	bl	snprintf
	b	.L39
.L38:
	mov	r0, #79
	mvn	r1, #0
	mov	r2, #1
	bl	GuiLib_ShowScreen
.L39:
	ldr	r3, .L46+36
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L40
	ldr	r3, .L46+40
	ldr	r2, [r3, #0]
	cmn	r2, #1
	moveq	r2, #0
	streq	r2, [r3, #0]
.L40:
	ldr	r5, .L46+44
	ldr	r3, .L46+48
	ldr	r2, [r5, #0]
	cmp	r2, r3
	beq	.L41
	ldr	r3, .L46+12
	ldr	r2, [r3, #0]
	cmp	r2, #0
	bne	.L41
	mov	r2, #1
	str	r2, [r3, #0]
	bl	FINISH_TIMES_draw_dialog
.L41:
	ldr	r2, [r5, #60]
	ldr	r3, .L46+52
	str	r2, [r3, #0]
	ldr	r2, [r5, #56]	@ float
	ldr	r3, .L46+56
	str	r2, [r3, #0]	@ float
	ldr	r3, .L46
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #0
	bl	GuiLib_ScrollBox_Close
	cmp	r4, #1
	bne	.L42
	ldr	r3, .L46+40
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L42
	ldr	r2, .L46+60
	str	r3, [r2, #0]
	ldr	r2, .L46+64
	str	r3, [r2, #0]
	ldr	r2, .L46+36
	ldr	r0, [r2, #0]
	cmp	r0, #0
	movne	r2, r0, asl #16
	movne	r0, r3
	ldreq	r1, .L46+68
	moveq	r2, r0
	mvneq	r3, #0
	ldrne	r1, .L46+68
	movne	r2, r2, asr #16
	movne	r3, r0
	bl	GuiLib_ScrollBox_Init
	b	.L44
.L42:
	ldr	r3, .L46+36
	ldr	r1, .L46+64
	ldrsh	r2, [r3, #0]
	ldrsh	r1, [r1, #0]
	ldr	r3, .L46+40
	mov	r0, #0
	ldrsh	r3, [r3, #0]
	str	r1, [sp, #0]
	ldr	r1, .L46+68
	bl	GuiLib_ScrollBox_Init_Custom_SetTopLine
.L44:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, lr}
	b	GuiLib_Refresh
.L47:
	.align	2
.L46:
	.word	list_program_data_recursive_MUTEX
	.word	.LC1
	.word	389
	.word	.LANCHOR3
	.word	.LANCHOR2
	.word	GuiVar_FinishTimesScrollBoxH
	.word	.LANCHOR1
	.word	.LC10
	.word	GuiVar_FinishTimesSystemGroup
	.word	.LANCHOR0
	.word	.LANCHOR5
	.word	ftcs
	.word	4098
	.word	.LANCHOR6
	.word	GuiVar_FinishTimesCalcTime
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	.LANCHOR7
	.word	FINISH_TIMES_draw_scroll_line
.LFE4:
	.size	FDTO_FINISH_TIMES_draw_screen, .-FDTO_FINISH_TIMES_draw_screen
	.section	.text.FINISH_TIMES_update_dialog,"ax",%progbits
	.align	2
	.global	FINISH_TIMES_update_dialog
	.type	FINISH_TIMES_update_dialog, %function
FINISH_TIMES_update_dialog:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L51
	ldr	r1, .L51+4
	ldr	r3, [r2, #0]
	str	lr, [sp, #-4]!
.LCFI5:
	cmp	r3, r1
	bne	.L49
	flds	s15, [r2, #52]
	ldr	r3, .L51+8
	ldr	r2, .L51+12
	ftouizs	s15, s15
	ldr	r1, [r3, #0]
	add	r1, r1, #1
	and	r1, r1, #3
	str	r1, [r3, #0]
	fmrs	r3, s15	@ int
	fsts	s15, [r2, #0]	@ int
	ldr	r2, .L51+16
	mov	r3, r3, asl #1
	str	r3, [r2, #0]
	ldr	lr, [sp], #4
	b	FDTO_DIALOG_redraw_ok_dialog
.L49:
	ldr	r2, .L51+20
	cmp	r3, r2
	ldrne	pc, [sp], #4
	ldr	r3, .L51+12
	mov	r2, #100
	str	r2, [r3, #0]
	ldr	r3, .L51+16
	mov	r2, #200
	str	r2, [r3, #0]
	bl	DIALOG_close_ok_dialog
	mov	r0, #1
	ldr	lr, [sp], #4
	b	FDTO_FINISH_TIMES_draw_screen
.L52:
	.align	2
.L51:
	.word	ftcs
	.word	4097
	.word	GuiVar_SpinnerPos
	.word	GuiVar_FinishTimesCalcPercentComplete
	.word	GuiVar_FinishTimesCalcProgressBar
	.word	4098
.LFE1:
	.size	FINISH_TIMES_update_dialog, .-FINISH_TIMES_update_dialog
	.section	.text.FINISH_TIMES_process_screen,"ax",%progbits
	.align	2
	.global	FINISH_TIMES_process_screen
	.type	FINISH_TIMES_process_screen, %function
FINISH_TIMES_process_screen:
.LFB6:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r4, r5, lr}
.LCFI6:
	mov	r4, r0
	sub	sp, sp, #44
.LCFI7:
	mov	r5, r1
	beq	.L56
	bhi	.L58
	cmp	r0, #2
	bne	.L54
	b	.L63
.L58:
	cmp	r0, #20
	beq	.L56
	cmp	r0, #81
	bne	.L54
	bl	FTIMES_TASK_restart_calculation
	bl	good_key_beep
	b	.L53
.L63:
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L64
	str	r0, [r3, #0]
	bl	FINISH_TIMES_draw_scroll_line
	ldr	r3, .L64+4
	ldr	r0, [r3, #0]
	bl	STATION_GROUP_get_index_for_group_with_this_GID
	ldr	r3, .L64+8
	str	r4, [sp, #8]
	str	r0, [r3, #0]
	mov	r3, #54
	str	r3, [sp, #16]
	ldr	r3, .L64+12
	add	r0, sp, #8
	str	r3, [sp, #28]
	mov	r3, #1
	str	r3, [sp, #32]
	mov	r3, #21
	str	r3, [sp, #40]
	ldr	r3, .L64+16
	str	r3, [sp, #24]
	bl	Change_Screen
	mov	r0, #0
	bl	Redraw_Screen
	b	.L53
.L56:
	bl	SYSTEM_num_systems_in_use
	cmp	r0, #1
	bls	.L60
	cmp	r4, #20
	movne	r5, #80
	moveq	r5, #84
	bl	SYSTEM_num_systems_in_use
	mov	r4, #1
	ldr	r1, .L64+20
	mov	r2, #0
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	sub	r3, r0, #1
	mov	r0, r5
	bl	process_uns32
	bl	good_key_beep
	ldr	r3, .L64+24
	add	r0, sp, #8
	str	r4, [sp, #8]
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L53
.L60:
	bl	bad_key_beep
	b	.L53
.L54:
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	ldr	r3, .L64
	mov	r1, r5
	mov	r2, #20
	str	r0, [r3, #0]
	ldr	r3, .L64+28
	mov	r0, r4
	ldr	r3, [r3, #0]
	bl	REPORTS_process_report
.L53:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, pc}
.L65:
	.align	2
.L64:
	.word	.LANCHOR5
	.word	.LANCHOR4
	.word	g_GROUP_list_item_index
	.word	FDTO_SCHEDULE_draw_menu
	.word	SCHEDULE_process_menu
	.word	.LANCHOR1
	.word	FDTO_FINISH_TIMES_redraw_scrollbox
	.word	.LANCHOR2
.LFE6:
	.size	FINISH_TIMES_process_screen, .-FINISH_TIMES_process_screen
	.global	FINISH_TIME_scrollbox_index
	.global	FINISH_TIME_calc_start_timestamp
	.section	.bss.ft_station_group_count,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	ft_station_group_count, %object
	.size	ft_station_group_count, 4
ft_station_group_count:
	.space	4
	.section	.bss.ft_scrollbox_top_line,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	ft_scrollbox_top_line, %object
	.size	ft_scrollbox_top_line, 4
ft_scrollbox_top_line:
	.space	4
	.section	.bss.FINISH_TIME_calc_start_timestamp,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	FINISH_TIME_calc_start_timestamp, %object
	.size	FINISH_TIME_calc_start_timestamp, 4
FINISH_TIME_calc_start_timestamp:
	.space	4
	.section	.bss.ft_max_pixels_to_scroll_horizontally,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	ft_max_pixels_to_scroll_horizontally, %object
	.size	ft_max_pixels_to_scroll_horizontally, 4
ft_max_pixels_to_scroll_horizontally:
	.space	4
	.section	.bss.ft_dialog_up,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	ft_dialog_up, %object
	.size	ft_dialog_up, 4
ft_dialog_up:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%s on %s, %s %d\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_ftimes.c\000"
.LC2:
	.ascii	"Does not Run\000"
.LC3:
	.ascii	"\000"
.LC4:
	.ascii	"hrs\000"
.LC5:
	.ascii	"min\000"
.LC6:
	.ascii	"sec\000"
.LC7:
	.ascii	"(manual programming started during irrigation and f"
	.ascii	"inish time exceeds stop time by %d %s)\000"
.LC8:
	.ascii	"(finish time exceeds stop time by %d %s)\000"
.LC9:
	.ascii	"(manual programming started during irrigation)\000"
.LC10:
	.ascii	"Mainline: %s\000"
	.section	.bss.ft_current_line_GID,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	ft_current_line_GID, %object
	.size	ft_current_line_GID, 4
ft_current_line_GID:
	.space	4
	.section	.bss.FINISH_TIME_scrollbox_index,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	FINISH_TIME_scrollbox_index, %object
	.size	FINISH_TIME_scrollbox_index, 4
FINISH_TIME_scrollbox_index:
	.space	4
	.section	.bss.ft_mainline_index,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	ft_mainline_index, %object
	.size	ft_mainline_index, 4
ft_mainline_index:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI0-.LFB7
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x54
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI3-.LFB5
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI4-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x80
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI5-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI6-.LFB6
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x38
	.align	2
.LEFDE12:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_ftimes.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xaf
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF6
	.byte	0x1
	.4byte	.LASF7
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF8
	.byte	0x1
	.byte	0xb6
	.byte	0x1
	.uleb128 0x3
	.4byte	0x21
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF0
	.byte	0x1
	.byte	0xd8
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x1ee
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x66
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x183
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0x8a
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST4
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x217
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST5
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB7
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI1
	.4byte	.LFE7
	.2byte	0x3
	.byte	0x7d
	.sleb128 84
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB5
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB1
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB6
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI7
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 56
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x4c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"FINISH_TIMES_process_screen\000"
.LASF4:
	.ascii	"FINISH_TIMES_update_dialog\000"
.LASF0:
	.ascii	"FINISH_TIMES_draw_scroll_line\000"
.LASF3:
	.ascii	"FDTO_FINISH_TIMES_draw_screen\000"
.LASF8:
	.ascii	"DATE_TIME_to_Finish_Times_format\000"
.LASF7:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_ftimes.c\000"
.LASF2:
	.ascii	"FINISH_TIMES_draw_dialog\000"
.LASF6:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"FDTO_FINISH_TIMES_redraw_scrollbox\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
