	.file	"port.c"
	.text
.Ltext0:
	.section	.text.pxPortInitialiseStack,"ax",%progbits
	.align	2
	.global	pxPortInitialiseStack
	.type	pxPortInitialiseStack, %function
pxPortInitialiseStack:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r3, r1, #4
	str	r3, [r0, #-4]
	ldr	r3, .L4
	tst	r1, #1
	str	r3, [r0, #-8]
	ldr	r3, .L4+4
	str	r0, [r0, #-12]
	str	r3, [r0, #-16]
	ldr	r3, .L4+8
	str	r2, [r0, #-64]
	str	r3, [r0, #-20]
	ldr	r3, .L4+12
	str	r3, [r0, #-24]
	ldr	r3, .L4+16
	str	r3, [r0, #-28]
	ldr	r3, .L4+20
	str	r3, [r0, #-32]
	ldr	r3, .L4+24
	str	r3, [r0, #-36]
	ldr	r3, .L4+28
	str	r3, [r0, #-40]
	ldr	r3, .L4+32
	str	r3, [r0, #-44]
	ldr	r3, .L4+36
	str	r3, [r0, #-48]
	ldr	r3, .L4+40
	str	r3, [r0, #-52]
	ldr	r3, .L4+44
	str	r3, [r0, #-56]
	ldr	r3, .L4+48
	str	r3, [r0, #-60]
	moveq	r3, #31
	movne	r3, #63
	str	r3, [r0, #-68]
	mov	r3, #0
	str	r3, [r0, #-72]!
	bx	lr
.L5:
	.align	2
.L4:
	.word	-1431655766
	.word	303174162
	.word	286331153
	.word	269488144
	.word	151587081
	.word	134744072
	.word	117901063
	.word	101058054
	.word	84215045
	.word	67372036
	.word	50529027
	.word	33686018
	.word	16843009
.LFE0:
	.size	pxPortInitialiseStack, .-pxPortInitialiseStack
	.section	.text.xPortStartScheduler,"ax",%progbits
	.align	2
	.global	xPortStartScheduler
	.type	xPortStartScheduler, %function
xPortStartScheduler:
.LFB1:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
.LBB5:
.LBB6:
	ldr	r3, .L7
.LBE6:
.LBE5:
	sub	sp, sp, #32
.LCFI1:
.LBB8:
.LBB7:
	mov	r4, #0
	mov	r2, #1
	mov	r1, #2
	mov	r0, #4
	str	r4, [sp, #0]
	bl	xSetISR_Vector
.LBE7:
	mov	r1, r4
	ldr	r0, .L7+4
	bl	timer_open
	add	r2, sp, #24
	mov	r1, #2
	str	r4, [sp, #24]
	str	r4, [sp, #28]
	mov	r5, #1
	mov	r6, r0
	bl	timer_ioctl
	ldr	r3, .L7+8
	mov	r1, #4
	add	r2, sp, r1
	mov	r0, r6
	str	r3, [sp, #20]
	str	r4, [sp, #4]
	str	r5, [sp, #8]
	str	r4, [sp, #12]
	str	r5, [sp, #16]
	bl	timer_ioctl
	mov	r0, r6
	mov	r1, #7
	mov	r2, #255
	bl	timer_ioctl
	mov	r1, r4
	mov	r2, r5
	mov	r0, r6
	bl	timer_ioctl
	mov	r0, #4
	bl	xEnable_ISR
.LBE8:
	bl	vPortISRStartFirstTask
	mov	r0, r4
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, pc}
.L8:
	.align	2
.L7:
	.word	vPreemptiveTick
	.word	1073938432
	.word	65000
.LFE1:
	.size	xPortStartScheduler, .-xPortStartScheduler
	.section	.text.vPortEndScheduler,"ax",%progbits
	.align	2
	.global	vPortEndScheduler
	.type	vPortEndScheduler, %function
vPortEndScheduler:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
.LFE2:
	.size	vPortEndScheduler, .-vPortEndScheduler
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS/portable/GCC/ARM9_LPC32xx/port.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x65
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.byte	0xcc
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xb1
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.4byte	0x5e
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xb3
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xc1
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"xPortStartScheduler\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/OpenRTOS"
	.ascii	"/portable/GCC/ARM9_LPC32xx/port.c\000"
.LASF6:
	.ascii	"vPortISRStartFirstTask\000"
.LASF1:
	.ascii	"vPortEndScheduler\000"
.LASF4:
	.ascii	"prvSetupTimerInterrupt\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"pxPortInitialiseStack\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
