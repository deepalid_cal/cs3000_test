	.file	"e_station_groups.c"
	.text
.Ltext0:
	.section	.text.STATION_GROUP_add_new_group,"ax",%progbits
	.align	2
	.type	STATION_GROUP_add_new_group, %function
STATION_GROUP_add_new_group:
.LFB40:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	ldr	r4, .L2
	ldr	r2, .L2+4
	ldr	r3, .L2+8
	ldr	r0, [r4, #0]
	mov	r1, #400
	bl	xQueueTakeMutexRecursive_debug
	ldr	r0, .L2+12
	mov	r1, #0
	bl	nm_GROUP_create_new_group_from_UI
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L3:
	.align	2
.L2:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	1119
	.word	nm_STATION_GROUP_create_new_group
.LFE40:
	.size	STATION_GROUP_add_new_group, .-STATION_GROUP_add_new_group
	.section	.text.STATION_GROUP_process_crop_coefficient,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_crop_coefficient, %function
STATION_GROUP_process_crop_coefficient:
.LFB34:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L5
	mov	r3, #0
	stmfd	sp!, {r0, r1, lr}
.LCFI1:
	str	r3, [sp, #4]
	ldr	r3, .L5+4
	str	r2, [sp, #0]	@ float
	bl	process_fl
	ldmfd	sp!, {r2, r3, pc}
.L6:
	.align	2
.L5:
	.word	1008981770
	.word	1077936128
.LFE34:
	.size	STATION_GROUP_process_crop_coefficient, .-STATION_GROUP_process_crop_coefficient
	.section	.text.STATION_GROUP_update_mainline_assignment,"ax",%progbits
	.align	2
	.type	STATION_GROUP_update_mainline_assignment, %function
STATION_GROUP_update_mainline_assignment:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L9
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r2, .L9+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	mov	r3, #220
	bl	xQueueTakeMutexRecursive_debug
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	subs	r4, r0, #0
	bne	.L8
	ldr	r0, .L9+4
	mov	r1, #226
	bl	Alert_group_not_found_with_filename
	mov	r0, r4
	bl	SYSTEM_get_group_at_this_index
	mov	r4, r0
.L8:
	mov	r0, r4
	bl	nm_GROUP_get_name
	mov	r2, #49
	mov	r1, r0
	ldr	r0, .L9+8
	bl	strlcpy
	mov	r0, r4
	bl	nm_GROUP_get_group_ID
	ldr	r3, .L9+12
	str	r0, [r3, #0]
	ldr	r3, .L9
	ldr	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L10:
	.align	2
.L9:
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_SystemName
	.word	GuiVar_StationGroupSystemGID
.LFE3:
	.size	STATION_GROUP_update_mainline_assignment, .-STATION_GROUP_update_mainline_assignment
	.section	.text.STATION_GROUP_process_soil_type_changed,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_soil_type_changed, %function
STATION_GROUP_process_soil_type_changed:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L14+4
	ldr	r2, .L14+8
	ldr	r3, [r3, #0]
	ldr	r2, [r2, #0]
	str	lr, [sp, #-4]!
.LCFI3:
	cmp	r2, r3
	ldreq	pc, [sp], #4
	cmp	r0, #0
	beq	.L13
	ldr	r1, .L14+12
	flds	s15, .L14
	mov	r2, r3, asl #2
	add	r1, r1, r2
	flds	s14, [r1, #0]
	ldr	r1, .L14+16
	mov	r0, #7
	fmuls	s14, s14, s15
	ftouizs	s14, s14
	fsts	s14, [r1, #0]	@ int
	fuitos	s14, s14
	ldr	r1, .L14+20
	add	r2, r1, r2
	ldr	r1, [r2, #0]	@ float
	ldr	r2, .L14+24
	fdivs	s14, s14, s15
	str	r1, [r2, #0]	@ float
	ldr	r2, .L14+28
	ldr	r2, [r2, #0]
	mla	r3, r0, r2, r3
	ldr	r2, .L14+32
	add	r3, r2, r3, asl #2
	ldr	r2, [r3, #0]	@ float
	ldr	r3, .L14+36
	fmrs	r0, s14
	str	r2, [r3, #0]	@ float
	bl	WATERSENSE_get_RZWWS
	ldr	r3, .L14+40
	str	r0, [r3, #0]	@ float
.L13:
	ldr	r3, .L14+4
	ldr	r2, .L14+44
	ldr	r3, [r3, #0]
	mov	r3, r3, asl #2
	add	r2, r2, r3
	ldr	r1, [r2, #0]	@ float
	ldr	r2, .L14+48
	str	r1, [r2, #0]	@ float
	ldr	r2, .L14+52
	ldr	r2, [r2, #0]
	add	r3, r3, r2
	ldr	r2, .L14+56
	add	r3, r2, r3, asl #2
	ldr	r2, [r3, #0]	@ float
	ldr	r3, .L14+60
	str	r2, [r3, #0]	@ float
	ldr	pc, [sp], #4
.L15:
	.align	2
.L14:
	.word	1120403456
	.word	GuiVar_StationGroupSoilType
	.word	.LANCHOR0
	.word	MAD
	.word	GuiVar_StationGroupAllowableDepletion
	.word	AW
	.word	GuiVar_StationGroupAvailableWater
	.word	GuiVar_StationGroupPlantType
	.word	ROOT_ZONE_DEPTH
	.word	GuiVar_StationGroupRootZoneDepth
	.word	GuiVar_StationGroupSoilStorageCapacity
	.word	IR
	.word	GuiVar_StationGroupSoilIntakeRate
	.word	GuiVar_StationGroupSlopePercentage
	.word	ASA
	.word	GuiVar_StationGroupAllowableSurfaceAccum
.LFE13:
	.size	STATION_GROUP_process_soil_type_changed, .-STATION_GROUP_process_soil_type_changed
	.section	.text.STATION_GROUP_process_soil_type_changed_dialog__affects_rzwws,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_soil_type_changed_dialog__affects_rzwws, %function
STATION_GROUP_process_soil_type_changed_dialog__affects_rzwws:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L23
	str	lr, [sp, #-4]!
.LCFI4:
	ldr	r3, [r3, #0]
	cmp	r3, #6
	beq	.L19
	cmp	r3, #7
	beq	.L20
	cmp	r3, #2
	bne	.L17
	b	.L22
.L19:
	mov	r0, #1
	b	.L21
.L20:
	mov	r0, #0
.L21:
	bl	STATION_GROUP_process_soil_type_changed
	b	.L17
.L22:
	ldr	r3, .L23+4
	ldr	r2, [r3, #0]
	ldr	r3, .L23+8
	str	r2, [r3, #0]
.L17:
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L24:
	.align	2
.L23:
	.word	g_DIALOG_modal_result
	.word	.LANCHOR0
	.word	GuiVar_StationGroupSoilType
.LFE15:
	.size	STATION_GROUP_process_soil_type_changed_dialog__affects_rzwws, .-STATION_GROUP_process_soil_type_changed_dialog__affects_rzwws
	.section	.text.STATION_GROUP_pre_process_soil_type_changed,"ax",%progbits
	.align	2
	.type	STATION_GROUP_pre_process_soil_type_changed, %function
STATION_GROUP_pre_process_soil_type_changed:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L27
	str	lr, [sp, #-4]!
.LCFI5:
	fstmfdd	sp!, {d8}
.LCFI6:
	ldr	r1, [r3, #0]
	ldr	r3, .L27+4
	ldr	r3, [r3, #0]
	cmp	r1, r3
	beq	.L26
	ldr	r3, .L27+8
	ldr	r0, .L27+12
	flds	s16, [r3, #0]
	ldr	r2, .L27+16
	mov	r3, r1, asl #2
	add	r2, r2, r3
	add	r0, r0, r3
	ldr	r3, .L27+20
	mov	ip, #7
	ldr	r3, [r3, #0]
	ldr	r0, [r0, #0]	@ float
	mla	r3, ip, r3, r1
	ldr	ip, .L27+24
	ldr	r1, [r2, #0]	@ float
	add	r3, ip, r3, asl #2
	ldr	r2, [r3, #0]	@ float
	bl	WATERSENSE_get_RZWWS
	fmsr	s15, r0
	fcmps	s16, s15
	fmstat
	beq	.L26
	mov	r0, #632
	fldmfdd	sp!, {d8}
	ldr	lr, [sp], #4
	b	DIALOG_draw_yes_no_cancel_dialog
.L26:
	mov	r0, #1
	bl	STATION_GROUP_process_soil_type_changed
	fldmfdd	sp!, {d8}
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L28:
	.align	2
.L27:
	.word	.LANCHOR0
	.word	GuiVar_StationGroupSoilType
	.word	GuiVar_StationGroupSoilStorageCapacity
	.word	MAD
	.word	AW
	.word	GuiVar_StationGroupPlantType
	.word	ROOT_ZONE_DEPTH
.LFE14:
	.size	STATION_GROUP_pre_process_soil_type_changed, .-STATION_GROUP_pre_process_soil_type_changed
	.section	.text.STATION_GROUP_update_crop_coefficients,"ax",%progbits
	.align	2
	.type	STATION_GROUP_update_crop_coefficients, %function
STATION_GROUP_update_crop_coefficients:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L30
	str	lr, [sp, #-4]!
.LCFI7:
	ldr	r0, [r3, #0]	@ float
	ldr	r3, .L30+4
	ldr	r1, [r3, #0]	@ float
	ldr	r3, .L30+8
	ldr	r2, [r3, #0]	@ float
	bl	WATERSENSE_get_Kl
	ldr	r3, .L30+12
	str	r0, [r3, #0]	@ float
	ldr	r3, .L30+16
	str	r0, [r3, #0]	@ float
	ldr	r3, .L30+20
	str	r0, [r3, #0]	@ float
	ldr	r3, .L30+24
	str	r0, [r3, #0]	@ float
	ldr	r3, .L30+28
	str	r0, [r3, #0]	@ float
	ldr	r3, .L30+32
	str	r0, [r3, #0]	@ float
	ldr	r3, .L30+36
	str	r0, [r3, #0]	@ float
	ldr	r3, .L30+40
	str	r0, [r3, #0]	@ float
	ldr	r3, .L30+44
	str	r0, [r3, #0]	@ float
	ldr	r3, .L30+48
	str	r0, [r3, #0]	@ float
	ldr	r3, .L30+52
	str	r0, [r3, #0]	@ float
	ldr	r3, .L30+56
	str	r0, [r3, #0]	@ float
	ldr	pc, [sp], #4
.L31:
	.align	2
.L30:
	.word	GuiVar_StationGroupSpeciesFactor
	.word	GuiVar_StationGroupDensityFactor
	.word	GuiVar_StationGroupMicroclimateFactor
	.word	GuiVar_StationGroupKc1
	.word	GuiVar_StationGroupKc2
	.word	GuiVar_StationGroupKc3
	.word	GuiVar_StationGroupKc4
	.word	GuiVar_StationGroupKc5
	.word	GuiVar_StationGroupKc6
	.word	GuiVar_StationGroupKc7
	.word	GuiVar_StationGroupKc8
	.word	GuiVar_StationGroupKc9
	.word	GuiVar_StationGroupKc10
	.word	GuiVar_StationGroupKc11
	.word	GuiVar_StationGroupKc12
.LFE2:
	.size	STATION_GROUP_update_crop_coefficients, .-STATION_GROUP_update_crop_coefficients
	.section	.text.STATION_GROUP_process_group_name_changed,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_group_name_changed, %function
STATION_GROUP_process_group_name_changed:
.LFB0:
	@ args = 0, pretend = 0, frame = 144
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	ip, r1
	str	lr, [sp, #-4]!
.LCFI8:
	mov	lr, r0
	sub	sp, sp, #148
.LCFI9:
	mov	r1, lr
	mov	r3, r2
	add	r0, sp, #52
	mov	r2, ip
	bl	STATION_GROUP_copy_details_into_group_name
	mov	r1, #0
	ldr	r0, .L35
	bl	GuiLib_GetTextPtr
	mov	r1, #48
	ldr	r2, .L35+4
	add	r3, sp, #52
	str	r0, [sp, #0]
	add	r0, sp, #100
	bl	snprintf
	add	r0, sp, #52
	ldr	r1, .L35+8
	mov	r2, #48
	bl	strncmp
	cmp	r0, #0
	beq	.L33
	add	r0, sp, #100
	ldr	r1, .L35+8
	mov	r2, #48
	bl	strncmp
	cmp	r0, #0
	bne	.L34
.L33:
	ldr	r3, .L35+12
	add	r0, sp, #4
	ldr	r1, [r3, #0]
	ldr	r3, .L35+16
	ldr	r2, [r3, #0]
	ldr	r3, .L35+20
	ldr	r3, [r3, #0]
	bl	STATION_GROUP_copy_details_into_group_name
	add	r0, sp, #4
	ldr	r1, .L35+8
	mov	r2, #48
	bl	strncmp
	cmp	r0, #0
	beq	.L34
	ldr	r0, .L35+8
	add	r1, sp, #4
	mov	r2, #48
	bl	strlcpy
.L34:
	bl	STATION_GROUP_extract_and_store_group_name_from_GuiVars
	ldr	r3, .L35+24
	mov	r0, #0
	ldr	r1, [r3, #0]
	bl	SCROLL_BOX_redraw_line
	add	sp, sp, #148
	ldmfd	sp!, {pc}
.L36:
	.align	2
.L35:
	.word	903
	.word	.LC1
	.word	GuiVar_GroupName
	.word	GuiVar_StationGroupPlantType
	.word	GuiVar_StationGroupHeadType
	.word	GuiVar_StationGroupExposure
	.word	g_GROUP_list_item_index
.LFE0:
	.size	STATION_GROUP_process_group_name_changed, .-STATION_GROUP_process_group_name_changed
	.section	.text.STATION_GROUP_process_plant_type_changed,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_plant_type_changed, %function
STATION_GROUP_process_plant_type_changed:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L41+4
	stmfd	sp!, {r4, r5, lr}
.LCFI10:
	ldr	r4, [r3, #0]
	ldr	r3, .L41+8
	mov	r5, r1
	ldr	r3, [r3, #0]
	cmp	r3, r4
	ldmeqfd	sp!, {r4, r5, pc}
	cmp	r0, #0
	beq	.L39
	ldr	r3, .L41+12
	add	r3, r3, r4, asl #2
	ldr	r2, [r3, #0]	@ float
	ldr	r3, .L41+16
	str	r2, [r3, #0]	@ float
	ldr	r3, .L41+20
	mov	r2, #12
	mla	r3, r2, r4, r3
	add	r4, r4, r4, asl #1
	ldr	r2, [r3, #4]	@ float
	ldr	r3, .L41+24
	str	r2, [r3, #0]	@ float
	ldr	r3, .L41+28
	ldr	r0, [r3, #0]
	bl	WATERSENSE_get_Kmc_index_based_on_exposure
	ldr	r3, .L41+32
	add	r4, r4, r0
	add	r4, r3, r4, asl #2
	ldr	r2, [r4, #0]	@ float
	ldr	r3, .L41+36
	str	r2, [r3, #0]	@ float
	bl	STATION_GROUP_update_crop_coefficients
.L39:
	cmp	r5, #0
	beq	.L40
	ldr	r3, .L41+4
	ldr	r2, .L41+40
	ldr	r3, [r3, #0]
	ldr	r2, [r2, #0]
	mov	r1, #7
	mla	r3, r1, r3, r2
	ldr	r2, .L41+44
	add	r3, r2, r3, asl #2
	ldr	r2, [r3, #0]	@ float
	ldr	r3, .L41+48
	str	r2, [r3, #0]	@ float
	ldr	r3, .L41+52
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	ldr	r3, .L41+56
	fuitos	s14, s15
	flds	s15, .L41
	ldr	r1, [r3, #0]	@ float
	fdivs	s15, s14, s15
	fmrs	r0, s15
	bl	WATERSENSE_get_RZWWS
	ldr	r3, .L41+60
	str	r0, [r3, #0]	@ float
.L40:
	ldr	r3, .L41+8
	ldr	r0, [r3, #0]
	ldr	r3, .L41+64
	ldr	r1, [r3, #0]
	ldr	r3, .L41+28
	ldr	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	STATION_GROUP_process_group_name_changed
.L42:
	.align	2
.L41:
	.word	1120403456
	.word	GuiVar_StationGroupPlantType
	.word	.LANCHOR1
	.word	Ks
	.word	GuiVar_StationGroupSpeciesFactor
	.word	Kd
	.word	GuiVar_StationGroupDensityFactor
	.word	GuiVar_StationGroupExposure
	.word	Kmc
	.word	GuiVar_StationGroupMicroclimateFactor
	.word	GuiVar_StationGroupSoilType
	.word	ROOT_ZONE_DEPTH
	.word	GuiVar_StationGroupRootZoneDepth
	.word	GuiVar_StationGroupAllowableDepletion
	.word	GuiVar_StationGroupAvailableWater
	.word	GuiVar_StationGroupSoilStorageCapacity
	.word	GuiVar_StationGroupHeadType
.LFE5:
	.size	STATION_GROUP_process_plant_type_changed, .-STATION_GROUP_process_plant_type_changed
	.section	.text.STATION_GROUP_process_plant_type_changed_dialog__affects_rzwws,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_plant_type_changed_dialog__affects_rzwws, %function
STATION_GROUP_process_plant_type_changed_dialog__affects_rzwws:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L50
	str	lr, [sp, #-4]!
.LCFI11:
	ldr	r3, [r3, #0]
	cmp	r3, #6
	beq	.L46
	cmp	r3, #7
	beq	.L47
	cmp	r3, #2
	bne	.L44
	b	.L49
.L46:
	mov	r0, #1
	mov	r1, r0
	b	.L48
.L47:
	mov	r0, #1
	mov	r1, #0
.L48:
	bl	STATION_GROUP_process_plant_type_changed
	b	.L44
.L49:
	ldr	r3, .L50+4
	ldr	r2, [r3, #0]
	ldr	r3, .L50+8
	str	r2, [r3, #0]
.L44:
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L51:
	.align	2
.L50:
	.word	g_DIALOG_modal_result
	.word	.LANCHOR1
	.word	GuiVar_StationGroupPlantType
.LFE9:
	.size	STATION_GROUP_process_plant_type_changed_dialog__affects_rzwws, .-STATION_GROUP_process_plant_type_changed_dialog__affects_rzwws
	.section	.text.STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff_and_rzwws,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff_and_rzwws, %function
STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff_and_rzwws:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L59
	str	lr, [sp, #-4]!
.LCFI12:
	ldr	r3, [r3, #0]
	cmp	r3, #6
	beq	.L55
	cmp	r3, #7
	beq	.L56
	cmp	r3, #2
	bne	.L53
	b	.L58
.L55:
	mov	r0, #1
	b	.L57
.L56:
	mov	r0, #0
.L57:
	mov	r1, r0
	bl	STATION_GROUP_process_plant_type_changed
	b	.L53
.L58:
	ldr	r3, .L59+4
	ldr	r2, [r3, #0]
	ldr	r3, .L59+8
	str	r2, [r3, #0]
.L53:
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L60:
	.align	2
.L59:
	.word	g_DIALOG_modal_result
	.word	.LANCHOR1
	.word	GuiVar_StationGroupPlantType
.LFE8:
	.size	STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff_and_rzwws, .-STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff_and_rzwws
	.section	.text.STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff, %function
STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L68
	str	lr, [sp, #-4]!
.LCFI13:
	ldr	r3, [r3, #0]
	cmp	r3, #6
	beq	.L64
	cmp	r3, #7
	beq	.L65
	cmp	r3, #2
	bne	.L62
	b	.L67
.L64:
	mov	r0, #1
	mov	r1, r0
	b	.L66
.L65:
	mov	r0, #0
	mov	r1, #1
.L66:
	bl	STATION_GROUP_process_plant_type_changed
	b	.L62
.L67:
	ldr	r3, .L68+4
	ldr	r2, [r3, #0]
	ldr	r3, .L68+8
	str	r2, [r3, #0]
.L62:
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L69:
	.align	2
.L68:
	.word	g_DIALOG_modal_result
	.word	.LANCHOR1
	.word	GuiVar_StationGroupPlantType
.LFE7:
	.size	STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff, .-STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff
	.section	.text.STATION_GROUP_process_exposure_changed,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_exposure_changed, %function
STATION_GROUP_process_exposure_changed:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L73
	ldr	r2, .L73+4
	ldr	r3, [r3, #0]
	ldr	r2, [r2, #0]
	stmfd	sp!, {r4, lr}
.LCFI14:
	cmp	r2, r3
	ldmeqfd	sp!, {r4, pc}
	cmp	r0, #0
	beq	.L72
	ldr	r2, .L73+8
	mov	r0, r3
	ldr	r4, [r2, #0]
	bl	WATERSENSE_get_Kmc_index_based_on_exposure
	ldr	r3, .L73+12
	add	r4, r4, r4, asl #1
	add	r4, r4, r0
	add	r4, r3, r4, asl #2
	ldr	r2, [r4, #0]	@ float
	ldr	r3, .L73+16
	str	r2, [r3, #0]	@ float
	bl	STATION_GROUP_update_crop_coefficients
.L72:
	ldr	r3, .L73+8
	ldr	r0, [r3, #0]
	ldr	r3, .L73+20
	ldr	r1, [r3, #0]
	ldr	r3, .L73+4
	ldr	r2, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	STATION_GROUP_process_group_name_changed
.L74:
	.align	2
.L73:
	.word	GuiVar_StationGroupExposure
	.word	.LANCHOR2
	.word	GuiVar_StationGroupPlantType
	.word	Kmc
	.word	GuiVar_StationGroupMicroclimateFactor
	.word	GuiVar_StationGroupHeadType
.LFE10:
	.size	STATION_GROUP_process_exposure_changed, .-STATION_GROUP_process_exposure_changed
	.section	.text.STATION_GROUP_process_exposure_changed_dialog__affects_crop_coeff,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_exposure_changed_dialog__affects_crop_coeff, %function
STATION_GROUP_process_exposure_changed_dialog__affects_crop_coeff:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L82
	str	lr, [sp, #-4]!
.LCFI15:
	ldr	r3, [r3, #0]
	cmp	r3, #6
	beq	.L78
	cmp	r3, #7
	beq	.L79
	cmp	r3, #2
	bne	.L76
	b	.L81
.L78:
	mov	r0, #1
	b	.L80
.L79:
	mov	r0, #0
.L80:
	bl	STATION_GROUP_process_exposure_changed
	b	.L76
.L81:
	ldr	r3, .L82+4
	ldr	r2, [r3, #0]
	ldr	r3, .L82+8
	str	r2, [r3, #0]
.L76:
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L83:
	.align	2
.L82:
	.word	g_DIALOG_modal_result
	.word	.LANCHOR2
	.word	GuiVar_StationGroupExposure
.LFE12:
	.size	STATION_GROUP_process_exposure_changed_dialog__affects_crop_coeff, .-STATION_GROUP_process_exposure_changed_dialog__affects_crop_coeff
	.section	.text.STATION_GROUP_process_head_type_changed,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_head_type_changed, %function
STATION_GROUP_process_head_type_changed:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L87
	ldr	r1, [r3, #0]
	ldr	r3, .L87+4
	ldr	r3, [r3, #0]
	cmp	r1, r3
	bxeq	lr
	cmp	r0, #0
	ldrne	r2, .L87+8
	addne	r3, r2, r3, asl #2
	ldrne	r2, [r3, #0]	@ float
	ldrne	r3, .L87+12
	strne	r2, [r3, #0]	@ float
	ldr	r3, .L87+16
	ldr	r0, [r3, #0]
	ldr	r3, .L87+20
	ldr	r2, [r3, #0]
	b	STATION_GROUP_process_group_name_changed
.L88:
	.align	2
.L87:
	.word	.LANCHOR3
	.word	GuiVar_StationGroupHeadType
	.word	PR
	.word	GuiVar_StationGroupPrecipRate
	.word	GuiVar_StationGroupPlantType
	.word	GuiVar_StationGroupExposure
.LFE17:
	.size	STATION_GROUP_process_head_type_changed, .-STATION_GROUP_process_head_type_changed
	.section	.text.STATION_GROUP_pre_process_head_type_changed,"ax",%progbits
	.align	2
	.type	STATION_GROUP_pre_process_head_type_changed, %function
STATION_GROUP_pre_process_head_type_changed:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L91
	ldr	r2, .L91+4
	ldr	r3, [r3, #0]
	ldr	r2, [r2, #0]
	str	lr, [sp, #-4]!
.LCFI16:
	cmp	r3, r2
	beq	.L90
	ldr	r2, .L91+8
	add	r3, r2, r3, asl #2
	ldr	r2, .L91+12
	flds	s15, [r3, #0]
	flds	s14, [r2, #0]
	fcmps	s14, s15
	fmstat
	beq	.L90
	mov	r0, #628
	ldr	lr, [sp], #4
	b	DIALOG_draw_yes_no_cancel_dialog
.L90:
	mov	r0, #1
	bl	STATION_GROUP_process_head_type_changed
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L92:
	.align	2
.L91:
	.word	.LANCHOR3
	.word	GuiVar_StationGroupHeadType
	.word	PR
	.word	GuiVar_StationGroupPrecipRate
.LFE18:
	.size	STATION_GROUP_pre_process_head_type_changed, .-STATION_GROUP_pre_process_head_type_changed
	.section	.text.STATION_GROUP_process_head_type_changed_dialog__affects_precip,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_head_type_changed_dialog__affects_precip, %function
STATION_GROUP_process_head_type_changed_dialog__affects_precip:
.LFB19:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L100
	str	lr, [sp, #-4]!
.LCFI17:
	ldr	r3, [r3, #0]
	cmp	r3, #6
	beq	.L96
	cmp	r3, #7
	beq	.L97
	cmp	r3, #2
	bne	.L94
	b	.L99
.L96:
	mov	r0, #1
	b	.L98
.L97:
	mov	r0, #0
.L98:
	bl	STATION_GROUP_process_head_type_changed
	b	.L94
.L99:
	ldr	r3, .L100+4
	ldr	r2, [r3, #0]
	ldr	r3, .L100+8
	str	r2, [r3, #0]
.L94:
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L101:
	.align	2
.L100:
	.word	g_DIALOG_modal_result
	.word	.LANCHOR3
	.word	GuiVar_StationGroupHeadType
.LFE19:
	.size	STATION_GROUP_process_head_type_changed_dialog__affects_precip, .-STATION_GROUP_process_head_type_changed_dialog__affects_precip
	.section	.text.STATION_GROUP_update_moisture_sensor_assignment,"ax",%progbits
	.align	2
	.type	STATION_GROUP_update_moisture_sensor_assignment, %function
STATION_GROUP_update_moisture_sensor_assignment:
.LFB20:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI18:
	subs	r4, r0, #0
	ldreq	r3, .L109
	streq	r4, [r3, #0]
	beq	.L104
	ldr	r5, .L109+4
	mov	r1, #400
	ldr	r2, .L109+8
	ldr	r3, .L109+12
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L109+16
	sub	r4, r4, #1
	ldr	r0, [r3, r4, asl #2]
	bl	MOISTURE_SENSOR_get_group_at_this_index
	ldr	r6, .L109
	subs	r4, r0, #0
	bne	.L105
	ldr	r0, .L109+8
	ldr	r1, .L109+20
	bl	Alert_group_not_found_with_filename
	str	r4, [r6, #0]
	b	.L106
.L105:
	bl	MOISTURE_SENSOR_get_decoder_serial_number
	str	r0, [r6, #0]
.L106:
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
.L104:
	ldr	r3, .L109
	ldr	r1, [r3, #0]
	cmp	r1, #0
	bne	.L107
	ldr	r3, .L109+24
	mov	r0, #1000
	ldrsh	r2, [r3, #0]
	bl	GuiLib_GetTextLanguagePtr
	b	.L108
.L107:
	mov	r0, r4
	bl	nm_GROUP_get_name
.L108:
	mov	r1, r0
	ldr	r0, .L109+28
	mov	r2, #49
	ldmfd	sp!, {r4, r5, r6, lr}
	b	strlcpy
.L110:
	.align	2
.L109:
	.word	GuiVar_StationGroupMoistureSensorDecoderSN
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC0
	.word	571
	.word	.LANCHOR4
	.word	579
	.word	GuiLib_LanguageIndex
	.word	GuiVar_MoisSensorName
.LFE20:
	.size	STATION_GROUP_update_moisture_sensor_assignment, .-STATION_GROUP_update_moisture_sensor_assignment
	.section	.text.FDTO_STATION_GROUP_return_to_menu,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_return_to_menu, %function
FDTO_STATION_GROUP_return_to_menu:
.LFB42:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L112
	ldr	r0, .L112+4
	ldr	r1, .L112+8
	mov	r2, #6
	str	r2, [r3, #0]
	b	FDTO_GROUP_return_to_menu
.L113:
	.align	2
.L112:
	.word	.LANCHOR5
	.word	.LANCHOR6
	.word	STATION_GROUP_extract_and_store_changes_from_GuiVars
.LFE42:
	.size	FDTO_STATION_GROUP_return_to_menu, .-FDTO_STATION_GROUP_return_to_menu
	.section	.text.FDTO_STATION_GROUP_show_moisture_sensor_assignment_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_show_moisture_sensor_assignment_dropdown, %function
FDTO_STATION_GROUP_show_moisture_sensor_assignment_dropdown:
.LFB38:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L123
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI19:
	ldr	r0, [r3, #0]
	cmp	r0, #0
	beq	.L122
	bl	MOISTURE_SENSOR_get_index_for_group_with_this_serial_number
.L122:
	ldr	r3, .L123+4
	mov	r4, r0
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L123+8
	ldr	r3, .L123+12
	bl	xQueueTakeMutexRecursive_debug
	ldr	r6, .L123+16
	ldr	r3, .L123+20
	mov	r5, #0
	str	r5, [r6, #0]
	str	r5, [r3, #0]
	mov	r8, r6
	ldr	r7, .L123+24
	b	.L116
.L121:
	mov	r0, r5
	bl	MOISTURE_SENSOR_get_group_at_this_index
	bl	MOISTURE_SENSOR_get_physically_available
	cmp	r0, #0
	beq	.L117
	ldr	r3, [r6, #0]
	cmp	r3, #23
	bhi	.L118
	cmp	r5, r4
	ldreq	r2, .L123+20
	str	r5, [r7, r3, asl #2]
	streq	r3, [r2, #0]
	add	r3, r3, #1
	str	r3, [r8, #0]
	b	.L117
.L118:
	bl	MOISTURE_SENSOR_get_num_of_moisture_sensors_connected
	mov	r1, r0
	ldr	r0, .L123+28
	bl	Alert_Message_va
	b	.L120
.L117:
	add	r5, r5, #1
.L116:
	bl	MOISTURE_SENSOR_get_list_count
	cmp	r5, r0
	bcc	.L121
.L120:
	ldr	r3, .L123+4
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	bl	MOISTURE_SENSOR_get_num_of_moisture_sensors_connected
	ldr	r1, .L123+32
	mov	r3, r4
	add	r2, r0, #1
	ldr	r0, .L123+36
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	FDTO_COMBOBOX_show
.L124:
	.align	2
.L123:
	.word	GuiVar_StationGroupMoistureSensorDecoderSN
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC0
	.word	1045
	.word	.LANCHOR7
	.word	.LANCHOR8
	.word	.LANCHOR4
	.word	.LC2
	.word	MOISTURE_SENSOR_load_sensor_name_into_scroll_box_guivar
	.word	737
.LFE38:
	.size	FDTO_STATION_GROUP_show_moisture_sensor_assignment_dropdown, .-FDTO_STATION_GROUP_show_moisture_sensor_assignment_dropdown
	.section	.text.FDTO_STATION_GROUP_show_head_type_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_show_head_type_dropdown, %function
FDTO_STATION_GROUP_show_head_type_dropdown:
.LFB32:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L126
	ldr	r0, .L126+4
	ldr	r1, .L126+8
	ldr	r3, [r3, #0]
	mov	r2, #13
	b	FDTO_COMBOBOX_show
.L127:
	.align	2
.L126:
	.word	GuiVar_StationGroupHeadType
	.word	729
	.word	FDTO_COMBOBOX_add_items
.LFE32:
	.size	FDTO_STATION_GROUP_show_head_type_dropdown, .-FDTO_STATION_GROUP_show_head_type_dropdown
	.section	.text.FDTO_STATION_GROUP_show_slope_percentage_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_show_slope_percentage_dropdown, %function
FDTO_STATION_GROUP_show_slope_percentage_dropdown:
.LFB30:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L129
	ldr	r0, .L129+4
	ldr	r1, .L129+8
	ldr	r3, [r3, #0]
	mov	r2, #4
	b	FDTO_COMBOBOX_show
.L130:
	.align	2
.L129:
	.word	GuiVar_StationGroupSlopePercentage
	.word	747
	.word	FDTO_COMBOBOX_add_items
.LFE30:
	.size	FDTO_STATION_GROUP_show_slope_percentage_dropdown, .-FDTO_STATION_GROUP_show_slope_percentage_dropdown
	.section	.text.FDTO_STATION_GROUP_show_soil_type_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_show_soil_type_dropdown, %function
FDTO_STATION_GROUP_show_soil_type_dropdown:
.LFB28:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L132
	ldr	r1, .L132+4
	ldr	r3, [r3, #0]
	mov	r0, #748
	mov	r2, #7
	b	FDTO_COMBOBOX_show
.L133:
	.align	2
.L132:
	.word	GuiVar_StationGroupSoilType
	.word	FDTO_COMBOBOX_add_items
.LFE28:
	.size	FDTO_STATION_GROUP_show_soil_type_dropdown, .-FDTO_STATION_GROUP_show_soil_type_dropdown
	.section	.text.FDTO_STATION_GROUP_show_exposure_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_show_exposure_dropdown, %function
FDTO_STATION_GROUP_show_exposure_dropdown:
.LFB26:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L135
	ldr	r0, .L135+4
	ldr	r1, .L135+8
	ldr	r3, [r3, #0]
	mov	r2, #5
	b	FDTO_COMBOBOX_show
.L136:
	.align	2
.L135:
	.word	GuiVar_StationGroupExposure
	.word	727
	.word	FDTO_COMBOBOX_add_items
.LFE26:
	.size	FDTO_STATION_GROUP_show_exposure_dropdown, .-FDTO_STATION_GROUP_show_exposure_dropdown
	.section	.text.FDTO_STATION_GROUP_show_plant_type_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_show_plant_type_dropdown, %function
FDTO_STATION_GROUP_show_plant_type_dropdown:
.LFB24:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L138
	ldr	r0, .L138+4
	ldr	r1, .L138+8
	ldr	r3, [r3, #0]
	mov	r2, #14
	b	FDTO_COMBOBOX_show
.L139:
	.align	2
.L138:
	.word	GuiVar_StationGroupPlantType
	.word	742
	.word	FDTO_COMBOBOX_add_items
.LFE24:
	.size	FDTO_STATION_GROUP_show_plant_type_dropdown, .-FDTO_STATION_GROUP_show_plant_type_dropdown
	.section	.text.FDTO_STATION_GROUP_show_mainline_assignment_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_show_mainline_assignment_dropdown, %function
FDTO_STATION_GROUP_show_mainline_assignment_dropdown:
.LFB22:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L141
	stmfd	sp!, {r4, lr}
.LCFI20:
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_index_for_group_with_this_GID
	mov	r4, r0
	bl	SYSTEM_num_systems_in_use
	ldr	r1, .L141+4
	mov	r3, r4
	mov	r2, r0
	ldr	r0, .L141+8
	ldmfd	sp!, {r4, lr}
	b	FDTO_COMBOBOX_show
.L142:
	.align	2
.L141:
	.word	GuiVar_StationGroupSystemGID
	.word	SYSTEM_load_system_name_into_scroll_box_guivar
	.word	734
.LFE22:
	.size	FDTO_STATION_GROUP_show_mainline_assignment_dropdown, .-FDTO_STATION_GROUP_show_mainline_assignment_dropdown
	.section	.text.FDTO_CROP_COEFFICIENTS_draw_dialog,"ax",%progbits
	.align	2
	.type	FDTO_CROP_COEFFICIENTS_draw_dialog, %function
FDTO_CROP_COEFFICIENTS_draw_dialog:
.LFB35:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI21:
	ldr	r0, .L144
	mov	r1, #30
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L145:
	.align	2
.L144:
	.word	597
.LFE35:
	.size	FDTO_CROP_COEFFICIENTS_draw_dialog, .-FDTO_CROP_COEFFICIENTS_draw_dialog
	.section	.text.FDTO_STATION_GROUP_close_moisture_sensor_assignment_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_close_moisture_sensor_assignment_dropdown, %function
FDTO_STATION_GROUP_close_moisture_sensor_assignment_dropdown:
.LFB39:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, #0
	str	lr, [sp, #-4]!
.LCFI22:
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	bl	STATION_GROUP_update_moisture_sensor_assignment
	ldr	lr, [sp], #4
	b	FDTO_COMBOBOX_hide
.LFE39:
	.size	FDTO_STATION_GROUP_close_moisture_sensor_assignment_dropdown, .-FDTO_STATION_GROUP_close_moisture_sensor_assignment_dropdown
	.section	.text.FDTO_STATION_GROUP_close_head_type_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_close_head_type_dropdown, %function
FDTO_STATION_GROUP_close_head_type_dropdown:
.LFB33:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI23:
	ldr	r4, .L148
	ldr	r3, .L148+4
	ldr	r2, [r4, #0]
	mov	r0, #0
	str	r2, [r3, #0]
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	str	r0, [r4, #0]
	bl	FDTO_COMBOBOX_hide
	ldmfd	sp!, {r4, lr}
	b	STATION_GROUP_pre_process_head_type_changed
.L149:
	.align	2
.L148:
	.word	GuiVar_StationGroupHeadType
	.word	.LANCHOR3
.LFE33:
	.size	FDTO_STATION_GROUP_close_head_type_dropdown, .-FDTO_STATION_GROUP_close_head_type_dropdown
	.section	.text.FDTO_STATION_GROUP_close_slope_percentage_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_close_slope_percentage_dropdown, %function
FDTO_STATION_GROUP_close_slope_percentage_dropdown:
.LFB31:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI24:
	ldr	r4, .L152
	mov	r0, #0
	mov	r1, r0
	ldr	r5, [r4, #0]
	bl	GuiLib_ScrollBox_GetActiveLine
.LBB22:
	cmp	r5, r0
.LBE22:
	str	r0, [r4, #0]
.LBB24:
	beq	.L151
.LBB23:
	ldr	r3, .L152+4
	ldr	r3, [r3, #0]
	add	r0, r0, r3, asl #2
	ldr	r3, .L152+8
	add	r0, r3, r0, asl #2
	ldr	r2, [r0, #0]	@ float
	ldr	r3, .L152+12
	str	r2, [r3, #0]	@ float
	bl	Refresh_Screen
.L151:
.LBE23:
.LBE24:
	ldmfd	sp!, {r4, r5, lr}
	b	FDTO_COMBOBOX_hide
.L153:
	.align	2
.L152:
	.word	GuiVar_StationGroupSlopePercentage
	.word	GuiVar_StationGroupSoilType
	.word	ASA
	.word	GuiVar_StationGroupAllowableSurfaceAccum
.LFE31:
	.size	FDTO_STATION_GROUP_close_slope_percentage_dropdown, .-FDTO_STATION_GROUP_close_slope_percentage_dropdown
	.section	.text.FDTO_STATION_GROUP_close_soil_type_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_close_soil_type_dropdown, %function
FDTO_STATION_GROUP_close_soil_type_dropdown:
.LFB29:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI25:
	ldr	r4, .L155
	ldr	r3, .L155+4
	ldr	r2, [r4, #0]
	mov	r0, #0
	str	r2, [r3, #0]
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	str	r0, [r4, #0]
	bl	FDTO_COMBOBOX_hide
	ldmfd	sp!, {r4, lr}
	b	STATION_GROUP_pre_process_soil_type_changed
.L156:
	.align	2
.L155:
	.word	GuiVar_StationGroupSoilType
	.word	.LANCHOR0
.LFE29:
	.size	FDTO_STATION_GROUP_close_soil_type_dropdown, .-FDTO_STATION_GROUP_close_soil_type_dropdown
	.section	.text.FDTO_STATION_GROUP_close_mainline_assignment_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_close_mainline_assignment_dropdown, %function
FDTO_STATION_GROUP_close_mainline_assignment_dropdown:
.LFB23:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, #0
	str	lr, [sp, #-4]!
.LCFI26:
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	bl	STATION_GROUP_update_mainline_assignment
	ldr	lr, [sp], #4
	b	FDTO_COMBOBOX_hide
.LFE23:
	.size	FDTO_STATION_GROUP_close_mainline_assignment_dropdown, .-FDTO_STATION_GROUP_close_mainline_assignment_dropdown
	.section	.text.FDTO_STATION_GROUP_draw_menu,"ax",%progbits
	.align	2
	.global	FDTO_STATION_GROUP_draw_menu
	.type	FDTO_STATION_GROUP_draw_menu, %function
FDTO_STATION_GROUP_draw_menu:
.LFB43:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L162
	stmfd	sp!, {r0, r1, r2, r4, lr}
.LCFI27:
	ldr	r2, .L162+4
	mov	r1, #400
	mov	r4, r0
	ldr	r0, [r3, #0]
	ldr	r3, .L162+8
	bl	xQueueTakeMutexRecursive_debug
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r3, .L162+12
	ldr	r1, .L162+16
	str	r3, [sp, #0]
	ldr	r3, .L162+20
	str	r3, [sp, #4]
	mov	r3, #1
	str	r3, [sp, #8]
	mov	r3, #55
	mov	r2, r0
	mov	r0, r4
	bl	FDTO_GROUP_draw_menu
	ldr	r3, .L162+24
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L159
	ldr	r3, .L162+28
	ldr	r0, .L162+16
	ldr	r1, [r3, #0]
	cmp	r1, #0
	movne	r1, #12
	moveq	r1, #13
	bl	FDTO_STATION_SELECTION_GRID_redraw_calling_screen
.L159:
	ldr	r3, .L162
	ldr	r0, [r3, #0]
	add	sp, sp, #12
	ldmfd	sp!, {r4, lr}
	b	xQueueGiveMutexRecursive
.L163:
	.align	2
.L162:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	1684
	.word	nm_STATION_GROUP_load_group_name_into_guivar
	.word	.LANCHOR6
	.word	STATION_GROUP_copy_group_into_guivars
	.word	g_STATION_SELECTION_GRID_user_pressed_back
	.word	GuiVar_StationSelectionGridShowOnlyStationsInThisGroup
.LFE43:
	.size	FDTO_STATION_GROUP_draw_menu, .-FDTO_STATION_GROUP_draw_menu
	.section	.text.MOISTURE_SENSOR_load_sensor_name_into_scroll_box_guivar,"ax",%progbits
	.align	2
	.type	MOISTURE_SENSOR_load_sensor_name_into_scroll_box_guivar, %function
MOISTURE_SENSOR_load_sensor_name_into_scroll_box_guivar:
.LFB37:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r0, r0, asl #16
	stmfd	sp!, {r4, r5, lr}
.LCFI28:
	movs	r5, r0, asr #16
	bne	.L165
.LBB27:
	ldr	r3, .L168
	mov	r1, r5
	ldrsh	r2, [r3, #0]
	mov	r0, #1000
	bl	GuiLib_GetTextLanguagePtr
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L168+4
.LBE27:
	ldmfd	sp!, {r4, r5, lr}
.LBB28:
	b	strlcpy
.L165:
.LBE28:
	ldr	r4, .L168+8
	mov	r1, #400
	ldr	r2, .L168+12
	mov	r3, #1004
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L168+16
	sub	r5, r5, #1
	ldr	r0, [r3, r5, asl #2]
	bl	MOISTURE_SENSOR_get_group_at_this_index
	cmp	r0, #0
	beq	.L166
	bl	nm_GROUP_get_name
	mov	r2, #48
	mov	r1, r0
	ldr	r0, .L168+4
	bl	strlcpy
	b	.L167
.L166:
	ldr	r0, .L168+12
	mov	r1, #1020
	bl	Alert_group_not_found_with_filename
.L167:
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L169:
	.align	2
.L168:
	.word	GuiLib_LanguageIndex
	.word	GuiVar_itmGroupName
	.word	moisture_sensor_items_recursive_MUTEX
	.word	.LC0
	.word	.LANCHOR4
.LFE37:
	.size	MOISTURE_SENSOR_load_sensor_name_into_scroll_box_guivar, .-MOISTURE_SENSOR_load_sensor_name_into_scroll_box_guivar
	.section	.text.STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal,"ax",%progbits
	.align	2
	.global	STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal
	.type	STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal, %function
STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L183
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI29:
	add	r3, r3, r0, asl #2
	ldr	r6, [r3, #0]	@ float
	ldr	r3, .L183+4
	mov	r2, #12
	mla	r3, r2, r0, r3
	mov	r4, r0
	mov	r0, r1
	ldr	r5, [r3, #4]	@ float
	bl	WATERSENSE_get_Kmc_index_based_on_exposure
	ldr	r3, .L183+8
	add	r4, r4, r4, asl #1
	mov	r1, r5	@ float
	add	r4, r4, r0
	add	r4, r3, r4, asl #2
	mov	r0, r6	@ float
	ldr	r2, [r4, #0]	@ float
	bl	WATERSENSE_get_Kl
	ldr	r3, .L183+12
	flds	s14, [r3, #0]
	fmsr	s15, r0
	fcmps	s14, s15
	fmstat
	bne	.L181
	ldr	r3, .L183+16
	flds	s14, [r3, #0]
	fcmps	s14, s15
	fmstat
	bne	.L181
	ldr	r3, .L183+20
	flds	s14, [r3, #0]
	fcmps	s14, s15
	fmstat
	bne	.L181
	ldr	r3, .L183+24
	flds	s14, [r3, #0]
	fcmps	s14, s15
	fmstat
	bne	.L181
	ldr	r3, .L183+28
	flds	s14, [r3, #0]
	fcmps	s14, s15
	fmstat
	bne	.L181
	ldr	r3, .L183+32
	flds	s14, [r3, #0]
	fcmps	s14, s15
	fmstat
	bne	.L181
	ldr	r3, .L183+36
	flds	s14, [r3, #0]
	fcmps	s14, s15
	fmstat
	bne	.L181
	ldr	r3, .L183+40
	flds	s14, [r3, #0]
	fcmps	s14, s15
	fmstat
	bne	.L181
	ldr	r3, .L183+44
	flds	s14, [r3, #0]
	fcmps	s14, s15
	fmstat
	bne	.L181
	ldr	r3, .L183+48
	flds	s14, [r3, #0]
	fcmps	s14, s15
	fmstat
	bne	.L181
	ldr	r3, .L183+52
	flds	s14, [r3, #0]
	fcmps	s14, s15
	fmstat
	bne	.L182
	ldr	r3, .L183+56
	flds	s14, [r3, #0]
	fcmps	s14, s15
	fmstat
	movne	r0, #0
	moveq	r0, #1
	ldmfd	sp!, {r4, r5, r6, pc}
.L181:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L182:
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.L184:
	.align	2
.L183:
	.word	Ks
	.word	Kd
	.word	Kmc
	.word	GuiVar_StationGroupKc1
	.word	GuiVar_StationGroupKc2
	.word	GuiVar_StationGroupKc3
	.word	GuiVar_StationGroupKc4
	.word	GuiVar_StationGroupKc5
	.word	GuiVar_StationGroupKc6
	.word	GuiVar_StationGroupKc7
	.word	GuiVar_StationGroupKc8
	.word	GuiVar_StationGroupKc9
	.word	GuiVar_StationGroupKc10
	.word	GuiVar_StationGroupKc11
	.word	GuiVar_StationGroupKc12
.LFE1:
	.size	STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal, .-STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal
	.section	.text.STATION_GROUP_pre_process_plant_type_changed,"ax",%progbits
	.align	2
	.type	STATION_GROUP_pre_process_plant_type_changed, %function
STATION_GROUP_pre_process_plant_type_changed:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI30:
	fstmfdd	sp!, {d8}
.LCFI31:
	ldr	r4, .L190+4
	ldr	r3, .L190+8
	ldr	r0, [r4, #0]
	ldr	r3, [r3, #0]
	cmp	r0, r3
	beq	.L186
	ldr	r3, .L190+12
	ldr	r1, [r3, #0]
	bl	STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal
	cmp	r0, #0
	bne	.L186
	ldr	r3, .L190+16
	ldr	r2, .L190+20
	flds	s16, [r3, #0]
	ldr	r3, .L190+24
	ldr	r2, [r2, #0]
	ldr	r3, [r3, #0]
	mov	r1, #7
	fmsr	s15, r3	@ int
	ldr	r3, [r4, #0]
	fuitos	s14, s15
	flds	s15, .L190
	mla	r3, r1, r3, r2
	ldr	r2, .L190+28
	add	r3, r2, r3, asl #2
	fdivs	s15, s14, s15
	ldr	r2, .L190+32
	ldr	r1, [r2, #0]	@ float
	ldr	r2, [r3, #0]	@ float
	fmrs	r0, s15
	bl	WATERSENSE_get_RZWWS
	fmsr	s15, r0
	fcmps	s16, s15
	fmstat
	ldrne	r0, .L190+36
	bne	.L189
.L186:
	ldr	r4, .L190+4
	ldr	r3, .L190+8
	ldr	r0, [r4, #0]
	ldr	r3, [r3, #0]
	cmp	r0, r3
	beq	.L187
	ldr	r3, .L190+12
	ldr	r1, [r3, #0]
	bl	STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal
	cmp	r0, #0
	bne	.L187
	ldr	r0, .L190+40
.L189:
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, lr}
	b	DIALOG_draw_yes_no_cancel_dialog
.L187:
	ldr	r3, .L190+8
	ldr	r2, [r4, #0]
	ldr	r3, [r3, #0]
	cmp	r2, r3
	beq	.L188
	ldr	r3, .L190+16
	mov	r1, #7
	flds	s16, [r3, #0]
	ldr	r3, .L190+24
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	ldr	r3, .L190+20
	fuitos	s14, s15
	flds	s15, .L190
	ldr	r3, [r3, #0]
	mla	r3, r1, r2, r3
	ldr	r1, .L190+28
	fdivs	s15, s14, s15
	ldr	r2, .L190+32
	add	r3, r1, r3, asl #2
	ldr	r1, [r2, #0]	@ float
	ldr	r2, [r3, #0]	@ float
	fmrs	r0, s15
	bl	WATERSENSE_get_RZWWS
	fmsr	s15, r0
	fcmps	s16, s15
	fmstat
	ldrne	r0, .L190+44
	bne	.L189
.L188:
	mov	r0, #1
	mov	r1, r0
	bl	STATION_GROUP_process_plant_type_changed
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, lr}
	b	Refresh_Screen
.L191:
	.align	2
.L190:
	.word	1120403456
	.word	.LANCHOR1
	.word	GuiVar_StationGroupPlantType
	.word	GuiVar_StationGroupExposure
	.word	GuiVar_StationGroupSoilStorageCapacity
	.word	GuiVar_StationGroupSoilType
	.word	GuiVar_StationGroupAllowableDepletion
	.word	ROOT_ZONE_DEPTH
	.word	GuiVar_StationGroupAvailableWater
	.word	630
	.word	629
	.word	631
.LFE6:
	.size	STATION_GROUP_pre_process_plant_type_changed, .-STATION_GROUP_pre_process_plant_type_changed
	.section	.text.FDTO_STATION_GROUP_close_plant_type_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_close_plant_type_dropdown, %function
FDTO_STATION_GROUP_close_plant_type_dropdown:
.LFB25:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI32:
	ldr	r4, .L193
	ldr	r3, .L193+4
	ldr	r2, [r4, #0]
	mov	r0, #0
	str	r2, [r3, #0]
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	str	r0, [r4, #0]
	bl	FDTO_COMBOBOX_hide
	ldmfd	sp!, {r4, lr}
	b	STATION_GROUP_pre_process_plant_type_changed
.L194:
	.align	2
.L193:
	.word	GuiVar_StationGroupPlantType
	.word	.LANCHOR1
.LFE25:
	.size	FDTO_STATION_GROUP_close_plant_type_dropdown, .-FDTO_STATION_GROUP_close_plant_type_dropdown
	.section	.text.STATION_GROUP_pre_process_exposure_changed,"ax",%progbits
	.align	2
	.type	STATION_GROUP_pre_process_exposure_changed, %function
STATION_GROUP_pre_process_exposure_changed:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L197
	str	lr, [sp, #-4]!
.LCFI33:
	ldr	r1, [r3, #0]
	ldr	r3, .L197+4
	ldr	r3, [r3, #0]
	cmp	r1, r3
	beq	.L196
	ldr	r3, .L197+8
	ldr	r0, [r3, #0]
	bl	STATION_GROUP_crop_coefficients_for_this_plant_and_exposure_are_equal
	cmp	r0, #0
	bne	.L196
	ldr	r0, .L197+12
	ldr	lr, [sp], #4
	b	DIALOG_draw_yes_no_cancel_dialog
.L196:
	mov	r0, #1
	bl	STATION_GROUP_process_exposure_changed
	ldr	lr, [sp], #4
	b	Refresh_Screen
.L198:
	.align	2
.L197:
	.word	.LANCHOR2
	.word	GuiVar_StationGroupExposure
	.word	GuiVar_StationGroupPlantType
	.word	627
.LFE11:
	.size	STATION_GROUP_pre_process_exposure_changed, .-STATION_GROUP_pre_process_exposure_changed
	.section	.text.STATION_GROUP_process_group,"ax",%progbits
	.align	2
	.type	STATION_GROUP_process_group, %function
STATION_GROUP_process_group:
.LFB41:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L395
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI34:
	ldrsh	r3, [r3, #0]
	sub	sp, sp, #48
.LCFI35:
	cmp	r3, #632
	mov	r4, r0
	mov	r5, r1
	beq	.L208
	bgt	.L216
	cmp	r3, #628
	beq	.L204
	bgt	.L217
	ldr	r2, .L395+4
	cmp	r3, r2
	beq	.L202
	add	r2, r2, #18
	cmp	r3, r2
	beq	.L203
	sub	r2, r2, #30
	cmp	r3, r2
	bne	.L200
	b	.L389
.L217:
	ldr	r2, .L395+8
	cmp	r3, r2
	ldreq	r2, .L395+12
	beq	.L365
	ldrgt	r2, .L395+16
	bgt	.L365
	b	.L390
.L216:
	ldr	r2, .L395+20
	cmp	r3, r2
	beq	.L212
	bgt	.L218
	sub	r2, r2, #8
	cmp	r3, r2
	beq	.L210
	add	r2, r2, #5
	cmp	r3, r2
	beq	.L211
	sub	r2, r2, #7
	cmp	r3, r2
	bne	.L200
	b	.L391
.L218:
	ldr	r2, .L395+24
	cmp	r3, r2
	beq	.L214
	cmp	r3, #748
	beq	.L215
	sub	r2, r2, #5
	cmp	r3, r2
	bne	.L200
	b	.L392
.L202:
	cmp	r0, #67
	beq	.L219
	cmp	r0, #2
	bne	.L220
	ldr	r3, .L395+28
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L220
.L219:
	bl	STATION_GROUP_extract_and_store_group_name_from_GuiVars
.L220:
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L395+32
	bl	KEYBOARD_process_key
	b	.L199
.L390:
	ldr	r2, .L395+36
.L365:
	bl	DIALOG_process_yes_no_cancel_dialog
	b	.L199
.L208:
	ldr	r2, .L395+40
	b	.L365
.L204:
	ldr	r2, .L395+44
	b	.L365
.L203:
	ldr	r2, .L395+48
	b	.L365
.L211:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L267
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+52
	b	.L368
.L392:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L267
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+56
.L368:
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L199
.L391:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L267
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+60
	b	.L368
.L215:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L267
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+64
	b	.L368
.L214:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L267
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+68
	b	.L368
.L210:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L267
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+72
	b	.L368
.L389:
.LBB35:
.LBB36:
	cmp	r0, #3
	beq	.L232
	bhi	.L236
	cmp	r0, #1
	beq	.L309
	ldr	r4, .L395+28
	bhi	.L231
	b	.L393
.L236:
	cmp	r0, #67
	beq	.L234
	bhi	.L237
	cmp	r0, #4
	bne	.L268
	b	.L394
.L237:
	cmp	r0, #80
	beq	.L235
	cmp	r0, #84
	bne	.L268
	b	.L235
.L231:
	ldrsh	r3, [r4, #0]
	cmp	r3, #99
	bne	.L326
	bl	good_key_beep
	mov	r3, #10
	strh	r3, [r4, #0]	@ movhi
	b	.L382
.L235:
	ldr	r3, .L395+28
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #30
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L291
.L254:
	.word	.L242
	.word	.L243
	.word	.L244
	.word	.L245
	.word	.L246
	.word	.L247
	.word	.L248
	.word	.L249
	.word	.L250
	.word	.L251
	.word	.L252
	.word	.L253
.L242:
	mov	r0, r4
	ldr	r1, .L395+76
	b	.L360
.L243:
	ldr	r1, .L395+80
	mov	r0, r4
.L360:
	bl	STATION_GROUP_process_crop_coefficient
	b	.L305
.L244:
	mov	r0, r4
	ldr	r1, .L395+84
	b	.L360
.L245:
	mov	r0, r4
	ldr	r1, .L395+88
	b	.L360
.L246:
	mov	r0, r4
	ldr	r1, .L395+92
	b	.L360
.L247:
	mov	r0, r4
	ldr	r1, .L395+96
	b	.L360
.L248:
	mov	r0, r4
	ldr	r1, .L395+100
	b	.L360
.L249:
	mov	r0, r4
	ldr	r1, .L395+104
	b	.L360
.L250:
	mov	r0, r4
	ldr	r1, .L395+108
	b	.L360
.L251:
	mov	r0, r4
	ldr	r1, .L395+112
	b	.L360
.L252:
	mov	r0, r4
	ldr	r1, .L395+116
	b	.L360
.L253:
	mov	r0, r4
	ldr	r1, .L395+120
	b	.L360
.L394:
	ldr	r3, .L395+28
	ldrsh	r0, [r3, #0]
	cmp	r0, #41
	bgt	.L260
	cmp	r0, #36
	subge	r0, r0, #6
	bge	.L366
	b	.L326
.L260:
	cmp	r0, #99
	bne	.L326
	ldr	r3, .L395+124
	b	.L329
.L393:
	ldrsh	r0, [r4, #0]
	sub	r3, r0, #30
	cmp	r3, #11
	ldrls	pc, [pc, r3, asl #2]
	b	.L326
.L264:
	.word	.L262
	.word	.L262
	.word	.L262
	.word	.L262
	.word	.L262
	.word	.L262
	.word	.L263
	.word	.L263
	.word	.L263
	.word	.L263
	.word	.L263
	.word	.L263
.L262:
	add	r0, r0, #6
	b	.L366
.L263:
	ldr	r3, .L395+124
	str	r0, [r3, #0]
	mov	r0, #99
	b	.L366
.L232:
	ldr	r3, .L395+28
	ldrsh	r3, [r3, #0]
	cmp	r3, #41
	ldreq	r2, .L395+124
	streq	r3, [r2, #0]
	b	.L272
.L234:
	bl	good_key_beep
	ldr	r3, .L395+28
	mov	r2, #10
	strh	r2, [r3, #0]	@ movhi
.L382:
	ldr	r3, .L395+76
	flds	s15, [r3, #0]
	ldr	r3, .L395+80
	flds	s14, [r3, #0]
	fcmps	s15, s14
	fmstat
	bne	.L354
	ldr	r3, .L395+84
	flds	s14, [r3, #0]
	fcmps	s15, s14
	fmstat
	bne	.L354
	ldr	r3, .L395+88
	flds	s14, [r3, #0]
	fcmps	s15, s14
	fmstat
	bne	.L354
	ldr	r3, .L395+92
	flds	s14, [r3, #0]
	fcmps	s15, s14
	fmstat
	bne	.L354
	ldr	r3, .L395+96
	flds	s14, [r3, #0]
	fcmps	s15, s14
	fmstat
	bne	.L354
	ldr	r3, .L395+100
	flds	s14, [r3, #0]
	fcmps	s15, s14
	fmstat
	bne	.L354
	ldr	r3, .L395+104
	flds	s14, [r3, #0]
	fcmps	s15, s14
	fmstat
	bne	.L354
	ldr	r3, .L395+108
	flds	s14, [r3, #0]
	fcmps	s15, s14
	fmstat
	bne	.L354
	ldr	r3, .L395+112
	flds	s14, [r3, #0]
	fcmps	s15, s14
	fmstat
	bne	.L354
	ldr	r3, .L395+116
	flds	s14, [r3, #0]
	fcmps	s15, s14
	fmstat
	bne	.L354
	ldr	r3, .L395+120
	flds	s14, [r3, #0]
	fcmps	s15, s14
	fmstat
	moveq	r3, #0
	movne	r3, #1
	b	.L266
.L354:
	mov	r3, #1
.L266:
	ldr	r2, .L395+128
	mov	r0, #0
	str	r3, [r2, #0]
	bl	Redraw_Screen
	b	.L199
.L212:
.LBE36:
.LBE35:
	cmp	r0, #2
	cmpne	r0, #67
	movne	r1, #0
	moveq	r1, #1
	bne	.L267
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+132
	b	.L368
.L267:
	bl	COMBO_BOX_key_press
	b	.L199
.L200:
	cmp	r4, #4
	beq	.L273
	bhi	.L277
	cmp	r4, #1
	beq	.L270
	bcc	.L269
	cmp	r4, #2
	beq	.L271
	cmp	r4, #3
	bne	.L268
	b	.L272
.L277:
	cmp	r4, #67
	beq	.L275
	bhi	.L278
	cmp	r4, #16
	beq	.L274
	cmp	r4, #20
	bne	.L268
	b	.L274
.L278:
	cmp	r4, #80
	beq	.L276
	cmp	r4, #84
	bne	.L268
	b	.L276
.L271:
	ldr	r3, .L395+28
	ldrsh	r3, [r3, #0]
	cmp	r3, #13
	ldrls	pc, [pc, r3, asl #2]
	b	.L326
.L290:
	.word	.L280
	.word	.L281
	.word	.L282
	.word	.L283
	.word	.L284
	.word	.L326
	.word	.L285
	.word	.L286
	.word	.L326
	.word	.L326
	.word	.L287
	.word	.L288
	.word	.L289
	.word	.L289
.L280:
	mov	r0, #155
	mov	r1, #25
	bl	GROUP_process_show_keyboard
	b	.L199
.L281:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+136
	b	.L368
.L282:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+140
	b	.L368
.L283:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+144
	b	.L368
.L285:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+148
	b	.L368
.L286:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+152
	b	.L368
.L284:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+156
	b	.L368
.L287:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+160
	b	.L368
.L288:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L395+164
	b	.L368
.L289:
	bl	STATION_GROUP_extract_and_store_changes_from_GuiVars
	mov	r3, #3
	str	r3, [sp, #8]
	mov	r3, #67
	str	r3, [sp, #16]
	ldr	r3, .L395+168
	add	r0, sp, #8
	str	r3, [sp, #28]
	ldr	r3, .L395+172
	str	r3, [sp, #24]
	mov	r3, #1
	str	r3, [sp, #32]
	ldr	r3, .L395+28
	ldrsh	r3, [r3, #0]
	sub	r2, r3, #12
	rsbs	r3, r2, #0
	adc	r3, r3, r2
	str	r3, [sp, #36]
	ldr	r3, .L395+176
	ldr	r3, [r3, #0]
	str	r3, [sp, #40]
	bl	Change_Screen
	b	.L199
.L276:
	ldr	r3, .L395+28
	ldrsh	r3, [r3, #0]
	sub	r3, r3, #1
	cmp	r3, #10
	ldrls	pc, [pc, r3, asl #2]
	b	.L291
.L302:
	.word	.L292
	.word	.L293
	.word	.L294
	.word	.L295
	.word	.L296
	.word	.L297
	.word	.L298
	.word	.L299
	.word	.L300
	.word	.L291
	.word	.L301
.L292:
	bl	SYSTEM_num_systems_in_use
	cmp	r0, #1
	bhi	.L303
	ldr	r3, .L395+180
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L291
.L303:
.LBB37:
	ldr	r5, .L395+184
	mov	r1, #400
	ldr	r2, .L395+188
	mov	r3, #247
	ldr	r0, [r5, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	SYSTEM_num_systems_in_use
	ldr	r3, .L395+180
	mov	r6, r0
	ldr	r0, [r3, #0]
	bl	SYSTEM_get_index_for_group_with_this_GID
	add	r1, sp, #48
	mov	r3, #1
	mov	r2, #0
	str	r0, [r1, #-4]!
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	sub	r3, r6, #1
	bl	process_uns32
	ldr	r0, [sp, #44]
	bl	STATION_GROUP_update_mainline_assignment
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #0
	b	.L361
.L293:
.LBE37:
	ldr	r1, .L395+192
	ldr	r3, .L395+196
	ldr	r2, [r1, #0]
	mov	r0, r4
	str	r2, [r3, #0]
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r2, #0
	mov	r3, #13
	bl	process_uns32
	bl	STATION_GROUP_pre_process_plant_type_changed
	b	.L305
.L294:
	ldr	r1, .L395+200
	ldr	r3, .L395+204
	ldr	r2, [r1, #0]
	mov	r0, r4
	str	r2, [r3, #0]
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r2, #0
	mov	r3, #4
	bl	process_uns32
	bl	STATION_GROUP_pre_process_exposure_changed
	b	.L305
.L297:
	ldr	r1, .L395+208
	ldr	r3, .L395+212
	ldr	r2, [r1, #0]
	mov	r0, r4
	str	r2, [r3, #0]
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r2, #0
	mov	r3, #6
	bl	process_uns32
	bl	STATION_GROUP_pre_process_soil_type_changed
	b	.L305
.L298:
	ldr	r5, .L395+216
	mov	r3, #1
	ldr	r6, [r5, #0]
	mov	r2, #0
	str	r3, [sp, #0]
	mov	r0, r4
	mov	r3, #3
	mov	r1, r5
	str	r2, [sp, #4]
	bl	process_uns32
.LBB38:
	ldr	r3, [r5, #0]
	cmp	r6, r3
	beq	.L305
.LBB39:
	ldr	r2, .L395+208
	ldr	r2, [r2, #0]
	add	r3, r3, r2, asl #2
	ldr	r2, .L395+220
	add	r3, r2, r3, asl #2
	ldr	r2, [r3, #0]	@ float
	ldr	r3, .L395+224
	str	r2, [r3, #0]	@ float
	bl	Refresh_Screen
	b	.L305
.L299:
.LBE39:
.LBE38:
	ldr	r3, .L395+228
	mov	r0, r4
	str	r3, [sp, #0]	@ float
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r1, .L395+232
	mov	r2, #0
	ldr	r3, .L395+236
	b	.L362
.L295:
	ldr	r1, .L395+240
	ldr	r3, .L395+244
	ldr	r2, [r1, #0]
	mov	r0, r4
	str	r2, [r3, #0]
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r2, #0
	mov	r3, #12
	bl	process_uns32
	bl	STATION_GROUP_pre_process_head_type_changed
	b	.L305
.L296:
	mov	r3, #0
	ldr	r2, .L395+228
	str	r3, [sp, #4]
	ldr	r1, .L395+248
	ldr	r3, .L395+252
	mov	r0, r4
	str	r2, [sp, #0]	@ float
.L362:
	bl	process_fl
	b	.L305
.L300:
	ldr	r5, .L395+76
	mov	r0, r4
	mov	r1, r5
	bl	STATION_GROUP_process_crop_coefficient
	ldr	r3, [r5, #0]	@ float
	ldr	r2, .L395+80
	str	r3, [r2, #0]	@ float
	ldr	r2, .L395+84
	str	r3, [r2, #0]	@ float
	ldr	r2, .L395+88
	str	r3, [r2, #0]	@ float
	ldr	r2, .L395+92
	str	r3, [r2, #0]	@ float
	ldr	r2, .L395+96
	str	r3, [r2, #0]	@ float
	ldr	r2, .L395+100
	str	r3, [r2, #0]	@ float
	ldr	r2, .L395+104
	str	r3, [r2, #0]	@ float
	ldr	r2, .L395+108
	str	r3, [r2, #0]	@ float
	ldr	r2, .L395+112
	str	r3, [r2, #0]	@ float
	ldr	r2, .L395+116
	str	r3, [r2, #0]	@ float
	ldr	r2, .L395+120
	str	r3, [r2, #0]	@ float
	b	.L305
.L301:
	bl	MOISTURE_SENSOR_get_list_count
	cmp	r0, #0
	beq	.L291
.LBB40:
	ldr	r3, .L395+256
	mov	r1, #400
	ldr	r0, [r3, #0]
	ldr	r2, .L395+188
	mov	r3, #608
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L395+260
	ldr	r3, [r3, #0]
	cmp	r3, #0
	streq	r3, [sp, #44]
	beq	.L308
	mov	r0, #0
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	str	r0, [sp, #44]
.L308:
	bl	MOISTURE_SENSOR_get_num_of_moisture_sensors_connected
	mov	r5, #0
	mov	r2, #1
	add	r1, sp, #44
	stmia	sp, {r2, r5}
	mov	r2, r5
	mov	r3, r0
	mov	r0, r4
	bl	process_uns32
	ldr	r0, [sp, #44]
	bl	STATION_GROUP_update_moisture_sensor_assignment
	ldr	r3, .L395+256
	ldr	r0, [r3, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, r5
.L361:
	bl	Redraw_Screen
	b	.L305
.L291:
.LBE40:
	bl	bad_key_beep
.L305:
	bl	Refresh_Screen
	b	.L199
.L274:
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r3, .L395+264
	ldr	r2, .L395+268
	str	r3, [sp, #0]
	ldr	r3, .L395+272
	mov	r1, r0
	mov	r0, r4
	bl	GROUP_process_NEXT_and_PREV
	b	.L199
.L273:
	ldr	r3, .L395+28
	ldrsh	r3, [r3, #0]
	sub	r2, r3, #6
	cmp	r2, #7
	ldrls	pc, [pc, r2, asl #2]
	b	.L309
.L315:
	.word	.L310
	.word	.L310
	.word	.L320
	.word	.L321
	.word	.L321
	.word	.L322
	.word	.L314
	.word	.L314
.L310:
	ldr	r2, .L395+276
	mov	r0, #5
	str	r3, [r2, #0]
	b	.L366
.L314:
	ldr	r3, .L395+276
	b	.L329
.L309:
	mov	r0, #1
	b	.L367
.L269:
	ldr	r3, .L395+28
	ldrsh	r3, [r3, #0]
	sub	r2, r3, #5
	cmp	r2, #8
	ldrls	pc, [pc, r2, asl #2]
	b	.L272
.L327:
	.word	.L320
	.word	.L321
	.word	.L321
	.word	.L322
	.word	.L323
	.word	.L324
	.word	.L325
	.word	.L326
	.word	.L326
.L320:
	ldr	r3, .L395+276
	ldr	r2, [r3, #0]
	sub	r2, r2, #6
	cmp	r2, #1
	movhi	r2, #6
	bhi	.L364
	b	.L329
.L321:
	ldr	r2, .L395+276
	mov	r0, #8
	str	r3, [r2, #0]
	b	.L366
.L322:
	ldr	r3, .L395+276
	ldr	r2, [r3, #0]
	sub	r2, r2, #9
	cmp	r2, #1
	bls	.L329
	ldr	r2, .L395+128
	ldr	r2, [r2, #0]
	cmp	r2, #0
	movne	r2, #10
	moveq	r2, #9
.L364:
	str	r2, [r3, #0]
.L329:
	ldr	r0, [r3, #0]
	b	.L366
.L323:
	ldr	r3, .L395+280
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r2, #9
	bne	.L371
	b	.L370
.L324:
	ldr	r3, .L395+280
	ldr	r3, [r3, #0]
	cmp	r3, #0
	moveq	r0, #13
	beq	.L366
	mov	r2, #10
.L371:
	ldr	r3, .L395+276
	mov	r0, #11
	str	r2, [r3, #0]
	b	.L366
.L325:
	ldr	r3, .L395+276
	mov	r2, #11
	str	r2, [r3, #0]
.L370:
	mov	r0, #12
.L366:
	mov	r1, #1
	bl	CURSOR_Select
	b	.L199
.L326:
	bl	bad_key_beep
	b	.L199
.L270:
	ldr	r3, .L395+28
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, r4
	beq	.L275
.L367:
	bl	CURSOR_Up
	b	.L199
.L272:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L199
.L275:
	ldr	r0, .L395+284
	bl	KEY_process_BACK_from_editing_screen
	b	.L199
.L268:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L199:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, pc}
.L396:
	.align	2
.L395:
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	630
	.word	STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff_and_rzwws
	.word	STATION_GROUP_process_plant_type_changed_dialog__affects_rzwws
	.word	737
	.word	747
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_STATION_GROUP_draw_menu
	.word	STATION_GROUP_process_plant_type_changed_dialog__affects_crop_coeff
	.word	STATION_GROUP_process_soil_type_changed_dialog__affects_rzwws
	.word	STATION_GROUP_process_head_type_changed_dialog__affects_precip
	.word	STATION_GROUP_process_exposure_changed_dialog__affects_crop_coeff
	.word	FDTO_STATION_GROUP_close_mainline_assignment_dropdown
	.word	FDTO_STATION_GROUP_close_plant_type_dropdown
	.word	FDTO_STATION_GROUP_close_exposure_dropdown
	.word	FDTO_STATION_GROUP_close_soil_type_dropdown
	.word	FDTO_STATION_GROUP_close_slope_percentage_dropdown
	.word	FDTO_STATION_GROUP_close_head_type_dropdown
	.word	GuiVar_StationGroupKc1
	.word	GuiVar_StationGroupKc2
	.word	GuiVar_StationGroupKc3
	.word	GuiVar_StationGroupKc4
	.word	GuiVar_StationGroupKc5
	.word	GuiVar_StationGroupKc6
	.word	GuiVar_StationGroupKc7
	.word	GuiVar_StationGroupKc8
	.word	GuiVar_StationGroupKc9
	.word	GuiVar_StationGroupKc10
	.word	GuiVar_StationGroupKc11
	.word	GuiVar_StationGroupKc12
	.word	.LANCHOR9
	.word	GuiVar_StationGroupKcsAreCustom
	.word	FDTO_STATION_GROUP_close_moisture_sensor_assignment_dropdown
	.word	FDTO_STATION_GROUP_show_mainline_assignment_dropdown
	.word	FDTO_STATION_GROUP_show_plant_type_dropdown
	.word	FDTO_STATION_GROUP_show_exposure_dropdown
	.word	FDTO_STATION_GROUP_show_soil_type_dropdown
	.word	FDTO_STATION_GROUP_show_slope_percentage_dropdown
	.word	FDTO_STATION_GROUP_show_head_type_dropdown
	.word	FDTO_CROP_COEFFICIENTS_draw_dialog
	.word	FDTO_STATION_GROUP_show_moisture_sensor_assignment_dropdown
	.word	FDTO_STATION_SELECTION_GRID_draw_screen
	.word	STATION_SELECTION_GRID_process_screen
	.word	GuiVar_MenuScreenToShow
	.word	GuiVar_StationGroupSystemGID
	.word	list_system_recursive_MUTEX
	.word	.LC0
	.word	GuiVar_StationGroupPlantType
	.word	.LANCHOR1
	.word	GuiVar_StationGroupExposure
	.word	.LANCHOR2
	.word	GuiVar_StationGroupSoilType
	.word	.LANCHOR0
	.word	GuiVar_StationGroupSlopePercentage
	.word	ASA
	.word	GuiVar_StationGroupAllowableSurfaceAccum
	.word	1008981770
	.word	GuiVar_StationGroupSoilStorageCapacity
	.word	1086324736
	.word	GuiVar_StationGroupHeadType
	.word	.LANCHOR3
	.word	GuiVar_StationGroupPrecipRate
	.word	1092616182
	.word	moisture_sensor_items_recursive_MUTEX
	.word	GuiVar_StationGroupMoistureSensorDecoderSN
	.word	STATION_GROUP_copy_group_into_guivars
	.word	STATION_GROUP_extract_and_store_changes_from_GuiVars
	.word	STATION_GROUP_get_group_at_this_index
	.word	.LANCHOR5
	.word	GuiVar_MainMenuMoisSensorExists
	.word	FDTO_STATION_GROUP_return_to_menu
.LFE41:
	.size	STATION_GROUP_process_group, .-STATION_GROUP_process_group
	.section	.text.FDTO_STATION_GROUP_close_exposure_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_STATION_GROUP_close_exposure_dropdown, %function
FDTO_STATION_GROUP_close_exposure_dropdown:
.LFB27:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI36:
	ldr	r4, .L398
	ldr	r3, .L398+4
	ldr	r2, [r4, #0]
	mov	r0, #0
	str	r2, [r3, #0]
	mov	r1, r0
	bl	GuiLib_ScrollBox_GetActiveLine
	str	r0, [r4, #0]
	bl	FDTO_COMBOBOX_hide
	ldmfd	sp!, {r4, lr}
	b	STATION_GROUP_pre_process_exposure_changed
.L399:
	.align	2
.L398:
	.word	GuiVar_StationGroupExposure
	.word	.LANCHOR2
.LFE27:
	.size	FDTO_STATION_GROUP_close_exposure_dropdown, .-FDTO_STATION_GROUP_close_exposure_dropdown
	.section	.text.STATION_GROUP_process_menu,"ax",%progbits
	.align	2
	.global	STATION_GROUP_process_menu
	.type	STATION_GROUP_process_menu, %function
STATION_GROUP_process_menu:
.LFB44:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r6, r7, lr}
.LCFI37:
	ldr	r4, .L401
	mov	r6, r0
	mov	r7, r1
	ldr	r2, .L401+4
	mov	r1, #400
	ldr	r3, .L401+8
	ldr	r0, [r4, #0]
	bl	xQueueTakeMutexRecursive_debug
	bl	STATION_GROUP_get_num_groups_in_use
	ldr	r2, .L401+12
	mov	r1, r7
	str	r2, [sp, #0]
	ldr	r2, .L401+16
	str	r2, [sp, #4]
	ldr	r2, .L401+20
	str	r2, [sp, #8]
	ldr	r2, .L401+24
	str	r2, [sp, #12]
	ldr	r2, .L401+28
	mov	r3, r0
	mov	r0, r6
	bl	GROUP_process_menu
	ldr	r0, [r4, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r6, r7, lr}
	b	xQueueGiveMutexRecursive
.L402:
	.align	2
.L401:
	.word	list_program_data_recursive_MUTEX
	.word	.LC0
	.word	1714
	.word	STATION_GROUP_process_group
	.word	STATION_GROUP_add_new_group
	.word	STATION_GROUP_get_group_at_this_index
	.word	STATION_GROUP_copy_group_into_guivars
	.word	.LANCHOR6
.LFE44:
	.size	STATION_GROUP_process_menu, .-STATION_GROUP_process_menu
	.section	.bss.g_STATION_GROUP_previous_head_type,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_STATION_GROUP_previous_head_type, %object
	.size	g_STATION_GROUP_previous_head_type, 4
g_STATION_GROUP_previous_head_type:
	.space	4
	.section	.bss.g_STATION_GROUP_editing_group,"aw",%nobits
	.align	2
	.set	.LANCHOR6,. + 0
	.type	g_STATION_GROUP_editing_group, %object
	.size	g_STATION_GROUP_editing_group, 4
g_STATION_GROUP_editing_group:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_station_groups.c\000"
.LC1:
	.ascii	"%s %s\000"
.LC2:
	.ascii	"Too many moisture sensors: %d\012\000"
	.section	.bss.g_MOISTURE_SENSORS_count,"aw",%nobits
	.align	2
	.set	.LANCHOR7,. + 0
	.type	g_MOISTURE_SENSORS_count, %object
	.size	g_MOISTURE_SENSORS_count, 4
g_MOISTURE_SENSORS_count:
	.space	4
	.section	.bss.g_STATION_GROUP_previous_exposure,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_STATION_GROUP_previous_exposure, %object
	.size	g_STATION_GROUP_previous_exposure, 4
g_STATION_GROUP_previous_exposure:
	.space	4
	.section	.bss.g_STATION_GROUP_previous_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	g_STATION_GROUP_previous_cursor_pos, %object
	.size	g_STATION_GROUP_previous_cursor_pos, 4
g_STATION_GROUP_previous_cursor_pos:
	.space	4
	.section	.bss.g_CROP_COEFFICIENTS_previous_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR9,. + 0
	.type	g_CROP_COEFFICIENTS_previous_cursor_pos, %object
	.size	g_CROP_COEFFICIENTS_previous_cursor_pos, 4
g_CROP_COEFFICIENTS_previous_cursor_pos:
	.space	4
	.section	.bss.g_STATION_GROUP_previous_plant_type,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_STATION_GROUP_previous_plant_type, %object
	.size	g_STATION_GROUP_previous_plant_type, 4
g_STATION_GROUP_previous_plant_type:
	.space	4
	.section	.bss.g_MOISTURE_SENSORS_active_line,"aw",%nobits
	.align	2
	.set	.LANCHOR8,. + 0
	.type	g_MOISTURE_SENSORS_active_line, %object
	.size	g_MOISTURE_SENSORS_active_line, 4
g_MOISTURE_SENSORS_active_line:
	.space	4
	.section	.bss.g_MOISTURE_SENSORS,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_MOISTURE_SENSORS, %object
	.size	g_MOISTURE_SENSORS, 96
g_MOISTURE_SENSORS:
	.space	96
	.section	.bss.g_STATION_GROUP_previous_soil_type,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_STATION_GROUP_previous_soil_type, %object
	.size	g_STATION_GROUP_previous_soil_type, 4
g_STATION_GROUP_previous_soil_type:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.byte	0x4
	.4byte	.LCFI0-.LFB40
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.byte	0x4
	.4byte	.LCFI1-.LFB34
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI3-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI4-.LFB15
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI5-.LFB14
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0xc
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI7-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI8-.LFB0
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0x98
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI10-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI11-.LFB9
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI12-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI13-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI14-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI15-.LFB12
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.byte	0x4
	.4byte	.LCFI16-.LFB18
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.byte	0x4
	.4byte	.LCFI17-.LFB19
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.byte	0x4
	.4byte	.LCFI18-.LFB20
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.align	2
.LEFDE36:
.LSFDE38:
	.4byte	.LEFDE38-.LASFDE38
.LASFDE38:
	.4byte	.Lframe0
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.byte	0x4
	.4byte	.LCFI19-.LFB38
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE38:
.LSFDE40:
	.4byte	.LEFDE40-.LASFDE40
.LASFDE40:
	.4byte	.Lframe0
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.align	2
.LEFDE40:
.LSFDE42:
	.4byte	.LEFDE42-.LASFDE42
.LASFDE42:
	.4byte	.Lframe0
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.align	2
.LEFDE42:
.LSFDE44:
	.4byte	.LEFDE44-.LASFDE44
.LASFDE44:
	.4byte	.Lframe0
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.align	2
.LEFDE44:
.LSFDE46:
	.4byte	.LEFDE46-.LASFDE46
.LASFDE46:
	.4byte	.Lframe0
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.align	2
.LEFDE46:
.LSFDE48:
	.4byte	.LEFDE48-.LASFDE48
.LASFDE48:
	.4byte	.Lframe0
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.align	2
.LEFDE48:
.LSFDE50:
	.4byte	.LEFDE50-.LASFDE50
.LASFDE50:
	.4byte	.Lframe0
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.byte	0x4
	.4byte	.LCFI20-.LFB22
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE50:
.LSFDE52:
	.4byte	.LEFDE52-.LASFDE52
.LASFDE52:
	.4byte	.Lframe0
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.byte	0x4
	.4byte	.LCFI21-.LFB35
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE52:
.LSFDE54:
	.4byte	.LEFDE54-.LASFDE54
.LASFDE54:
	.4byte	.Lframe0
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.byte	0x4
	.4byte	.LCFI22-.LFB39
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE54:
.LSFDE56:
	.4byte	.LEFDE56-.LASFDE56
.LASFDE56:
	.4byte	.Lframe0
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.byte	0x4
	.4byte	.LCFI23-.LFB33
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE56:
.LSFDE58:
	.4byte	.LEFDE58-.LASFDE58
.LASFDE58:
	.4byte	.Lframe0
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.byte	0x4
	.4byte	.LCFI24-.LFB31
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE58:
.LSFDE60:
	.4byte	.LEFDE60-.LASFDE60
.LASFDE60:
	.4byte	.Lframe0
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.byte	0x4
	.4byte	.LCFI25-.LFB29
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE60:
.LSFDE62:
	.4byte	.LEFDE62-.LASFDE62
.LASFDE62:
	.4byte	.Lframe0
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.byte	0x4
	.4byte	.LCFI26-.LFB23
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE62:
.LSFDE64:
	.4byte	.LEFDE64-.LASFDE64
.LASFDE64:
	.4byte	.Lframe0
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.byte	0x4
	.4byte	.LCFI27-.LFB43
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x82
	.uleb128 0x3
	.byte	0x81
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE64:
.LSFDE66:
	.4byte	.LEFDE66-.LASFDE66
.LASFDE66:
	.4byte	.Lframe0
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.byte	0x4
	.4byte	.LCFI28-.LFB37
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE66:
.LSFDE68:
	.4byte	.LEFDE68-.LASFDE68
.LASFDE68:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI29-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE68:
.LSFDE70:
	.4byte	.LEFDE70-.LASFDE70
.LASFDE70:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI30-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI31-.LCFI30
	.byte	0xe
	.uleb128 0x10
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x4
	.align	2
.LEFDE70:
.LSFDE72:
	.4byte	.LEFDE72-.LASFDE72
.LASFDE72:
	.4byte	.Lframe0
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.byte	0x4
	.4byte	.LCFI32-.LFB25
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE72:
.LSFDE74:
	.4byte	.LEFDE74-.LASFDE74
.LASFDE74:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI33-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE74:
.LSFDE76:
	.4byte	.LEFDE76-.LASFDE76
.LASFDE76:
	.4byte	.Lframe0
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.byte	0x4
	.4byte	.LCFI34-.LFB41
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI35-.LCFI34
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE76:
.LSFDE78:
	.4byte	.LEFDE78-.LASFDE78
.LASFDE78:
	.4byte	.Lframe0
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.byte	0x4
	.4byte	.LCFI36-.LFB27
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE78:
.LSFDE80:
	.4byte	.LEFDE80-.LASFDE80
.LASFDE80:
	.4byte	.Lframe0
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.byte	0x4
	.4byte	.LCFI37-.LFB44
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x83
	.uleb128 0x5
	.byte	0x82
	.uleb128 0x6
	.byte	0x81
	.uleb128 0x7
	.byte	0x80
	.uleb128 0x8
	.align	2
.LEFDE80:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_station_groups.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x373
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF45
	.byte	0x1
	.4byte	.LASF46
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1f1
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x3dd
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0xf1
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x25a
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x45d
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x318
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x1
	.byte	0xd8
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x4
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1b2
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST3
	.uleb128 0x4
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x1dc
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST4
	.uleb128 0x4
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x1ce
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST5
	.uleb128 0x5
	.4byte	.LASF10
	.byte	0x1
	.byte	0xc3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST6
	.uleb128 0x5
	.4byte	.LASF11
	.byte	0x1
	.byte	0x7c
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST7
	.uleb128 0x4
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x107
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST8
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x16a
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST9
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x155
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST10
	.uleb128 0x4
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x140
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST11
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x17f
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST12
	.uleb128 0x4
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x19d
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST13
	.uleb128 0x6
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x1fb
	.4byte	.LFB17
	.4byte	.LFE17
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x209
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LLST14
	.uleb128 0x4
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x217
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LLST15
	.uleb128 0x4
	.4byte	.LASF21
	.byte	0x1
	.2byte	0x22d
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LLST16
	.uleb128 0x6
	.4byte	.LASF22
	.byte	0x1
	.2byte	0x678
	.4byte	.LFB42
	.4byte	.LFE42
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x404
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LLST17
	.uleb128 0x6
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x2f4
	.4byte	.LFB32
	.4byte	.LFE32
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x2e0
	.4byte	.LFB30
	.4byte	.LFE30
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x2ce
	.4byte	.LFB28
	.4byte	.LFE28
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x2bc
	.4byte	.LFB26
	.4byte	.LFE26
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x298
	.4byte	.LFB24
	.4byte	.LFE24
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x278
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LLST18
	.uleb128 0x4
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x31e
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LLST19
	.uleb128 0x4
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x43e
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LLST20
	.uleb128 0x4
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x2fa
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LLST21
	.uleb128 0x4
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x2e6
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LLST22
	.uleb128 0x4
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x2d4
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LLST23
	.uleb128 0x4
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x282
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LLST24
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x692
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LLST25
	.uleb128 0x8
	.4byte	0x2a
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LLST26
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF37
	.byte	0x1
	.byte	0xa3
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST27
	.uleb128 0x4
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x12a
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST28
	.uleb128 0x4
	.4byte	.LASF39
	.byte	0x1
	.2byte	0x2b0
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LLST29
	.uleb128 0x4
	.4byte	.LASF40
	.byte	0x1
	.2byte	0x18f
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST30
	.uleb128 0x2
	.4byte	.LASF41
	.byte	0x1
	.2byte	0x325
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF42
	.byte	0x1
	.2byte	0x478
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LLST31
	.uleb128 0x4
	.4byte	.LASF43
	.byte	0x1
	.2byte	0x2c2
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LLST32
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF44
	.byte	0x1
	.2byte	0x6b0
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	.LLST33
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB40
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE40
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB34
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE34
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB13
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB15
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB14
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI6
	.4byte	.LFE14
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB2
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB0
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI8
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI9
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 152
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB5
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI10
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB9
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LFE9
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB8
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB7
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST12:
	.4byte	.LFB10
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI14
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST13:
	.4byte	.LFB12
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST14:
	.4byte	.LFB18
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE18
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST15:
	.4byte	.LFB19
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST16:
	.4byte	.LFB20
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LFE20
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST17:
	.4byte	.LFB38
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI19
	.4byte	.LFE38
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST18:
	.4byte	.LFB22
	.4byte	.LCFI20
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI20
	.4byte	.LFE22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST19:
	.4byte	.LFB35
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LFE35
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST20:
	.4byte	.LFB39
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI22
	.4byte	.LFE39
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST21:
	.4byte	.LFB33
	.4byte	.LCFI23
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI23
	.4byte	.LFE33
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST22:
	.4byte	.LFB31
	.4byte	.LCFI24
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI24
	.4byte	.LFE31
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST23:
	.4byte	.LFB29
	.4byte	.LCFI25
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI25
	.4byte	.LFE29
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST24:
	.4byte	.LFB23
	.4byte	.LCFI26
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI26
	.4byte	.LFE23
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST25:
	.4byte	.LFB43
	.4byte	.LCFI27
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI27
	.4byte	.LFE43
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST26:
	.4byte	.LFB37
	.4byte	.LCFI28
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI28
	.4byte	.LFE37
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST27:
	.4byte	.LFB1
	.4byte	.LCFI29
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI29
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST28:
	.4byte	.LFB6
	.4byte	.LCFI30
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI30
	.4byte	.LCFI31
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI31
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST29:
	.4byte	.LFB25
	.4byte	.LCFI32
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI32
	.4byte	.LFE25
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST30:
	.4byte	.LFB11
	.4byte	.LCFI33
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI33
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST31:
	.4byte	.LFB41
	.4byte	.LCFI34
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI34
	.4byte	.LCFI35
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI35
	.4byte	.LFE41
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST32:
	.4byte	.LFB27
	.4byte	.LCFI36
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI36
	.4byte	.LFE27
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST33:
	.4byte	.LFB44
	.4byte	.LCFI37
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI37
	.4byte	.LFE44
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x15c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB40
	.4byte	.LFE40-.LFB40
	.4byte	.LFB34
	.4byte	.LFE34-.LFB34
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	.LFB19
	.4byte	.LFE19-.LFB19
	.4byte	.LFB20
	.4byte	.LFE20-.LFB20
	.4byte	.LFB42
	.4byte	.LFE42-.LFB42
	.4byte	.LFB38
	.4byte	.LFE38-.LFB38
	.4byte	.LFB32
	.4byte	.LFE32-.LFB32
	.4byte	.LFB30
	.4byte	.LFE30-.LFB30
	.4byte	.LFB28
	.4byte	.LFE28-.LFB28
	.4byte	.LFB26
	.4byte	.LFE26-.LFB26
	.4byte	.LFB24
	.4byte	.LFE24-.LFB24
	.4byte	.LFB22
	.4byte	.LFE22-.LFB22
	.4byte	.LFB35
	.4byte	.LFE35-.LFB35
	.4byte	.LFB39
	.4byte	.LFE39-.LFB39
	.4byte	.LFB33
	.4byte	.LFE33-.LFB33
	.4byte	.LFB31
	.4byte	.LFE31-.LFB31
	.4byte	.LFB29
	.4byte	.LFE29-.LFB29
	.4byte	.LFB23
	.4byte	.LFE23-.LFB23
	.4byte	.LFB43
	.4byte	.LFE43-.LFB43
	.4byte	.LFB37
	.4byte	.LFE37-.LFB37
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB25
	.4byte	.LFE25-.LFB25
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB41
	.4byte	.LFE41-.LFB41
	.4byte	.LFB27
	.4byte	.LFE27-.LFB27
	.4byte	.LFB44
	.4byte	.LFE44-.LFB44
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB40
	.4byte	.LFE40
	.4byte	.LFB34
	.4byte	.LFE34
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	.LFB19
	.4byte	.LFE19
	.4byte	.LFB20
	.4byte	.LFE20
	.4byte	.LFB42
	.4byte	.LFE42
	.4byte	.LFB38
	.4byte	.LFE38
	.4byte	.LFB32
	.4byte	.LFE32
	.4byte	.LFB30
	.4byte	.LFE30
	.4byte	.LFB28
	.4byte	.LFE28
	.4byte	.LFB26
	.4byte	.LFE26
	.4byte	.LFB24
	.4byte	.LFE24
	.4byte	.LFB22
	.4byte	.LFE22
	.4byte	.LFB35
	.4byte	.LFE35
	.4byte	.LFB39
	.4byte	.LFE39
	.4byte	.LFB33
	.4byte	.LFE33
	.4byte	.LFB31
	.4byte	.LFE31
	.4byte	.LFB29
	.4byte	.LFE29
	.4byte	.LFB23
	.4byte	.LFE23
	.4byte	.LFB43
	.4byte	.LFE43
	.4byte	.LFB37
	.4byte	.LFE37
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB25
	.4byte	.LFE25
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB41
	.4byte	.LFE41
	.4byte	.LFB27
	.4byte	.LFE27
	.4byte	.LFB44
	.4byte	.LFE44
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF3:
	.ascii	"STATION_GROUP_process_moisture_sensor_assignment\000"
.LASF28:
	.ascii	"FDTO_STATION_GROUP_show_plant_type_dropdown\000"
.LASF24:
	.ascii	"FDTO_STATION_GROUP_show_head_type_dropdown\000"
.LASF38:
	.ascii	"STATION_GROUP_pre_process_plant_type_changed\000"
.LASF31:
	.ascii	"FDTO_STATION_GROUP_close_moisture_sensor_assignment"
	.ascii	"_dropdown\000"
.LASF33:
	.ascii	"FDTO_STATION_GROUP_close_slope_percentage_dropdown\000"
.LASF36:
	.ascii	"FDTO_STATION_GROUP_draw_menu\000"
.LASF34:
	.ascii	"FDTO_STATION_GROUP_close_soil_type_dropdown\000"
.LASF44:
	.ascii	"STATION_GROUP_process_menu\000"
.LASF13:
	.ascii	"STATION_GROUP_process_plant_type_changed_dialog__af"
	.ascii	"fects_rzwws\000"
.LASF22:
	.ascii	"FDTO_STATION_GROUP_return_to_menu\000"
.LASF5:
	.ascii	"STATION_GROUP_process_crop_coefficient\000"
.LASF46:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_station_groups.c\000"
.LASF39:
	.ascii	"FDTO_STATION_GROUP_close_plant_type_dropdown\000"
.LASF1:
	.ascii	"MOISTURE_SENSOR_load_sensor_name_into_scroll_box_gu"
	.ascii	"ivar\000"
.LASF37:
	.ascii	"STATION_GROUP_crop_coefficients_for_this_plant_and_"
	.ascii	"exposure_are_equal\000"
.LASF7:
	.ascii	"STATION_GROUP_process_soil_type_changed\000"
.LASF0:
	.ascii	"STATION_GROUP_process_slope_percentage_changed\000"
.LASF17:
	.ascii	"STATION_GROUP_process_exposure_changed_dialog__affe"
	.ascii	"cts_crop_coeff\000"
.LASF18:
	.ascii	"STATION_GROUP_process_head_type_changed\000"
.LASF10:
	.ascii	"STATION_GROUP_update_crop_coefficients\000"
.LASF9:
	.ascii	"STATION_GROUP_pre_process_soil_type_changed\000"
.LASF26:
	.ascii	"FDTO_STATION_GROUP_show_soil_type_dropdown\000"
.LASF19:
	.ascii	"STATION_GROUP_pre_process_head_type_changed\000"
.LASF43:
	.ascii	"FDTO_STATION_GROUP_close_exposure_dropdown\000"
.LASF41:
	.ascii	"CROP_COEFFICIENTS_process_dialog\000"
.LASF12:
	.ascii	"STATION_GROUP_process_plant_type_changed\000"
.LASF2:
	.ascii	"STATION_GROUP_process_mainline_assignment\000"
.LASF11:
	.ascii	"STATION_GROUP_process_group_name_changed\000"
.LASF6:
	.ascii	"STATION_GROUP_update_mainline_assignment\000"
.LASF4:
	.ascii	"STATION_GROUP_add_new_group\000"
.LASF42:
	.ascii	"STATION_GROUP_process_group\000"
.LASF30:
	.ascii	"FDTO_CROP_COEFFICIENTS_draw_dialog\000"
.LASF32:
	.ascii	"FDTO_STATION_GROUP_close_head_type_dropdown\000"
.LASF8:
	.ascii	"STATION_GROUP_process_soil_type_changed_dialog__aff"
	.ascii	"ects_rzwws\000"
.LASF20:
	.ascii	"STATION_GROUP_process_head_type_changed_dialog__aff"
	.ascii	"ects_precip\000"
.LASF16:
	.ascii	"STATION_GROUP_process_exposure_changed\000"
.LASF14:
	.ascii	"STATION_GROUP_process_plant_type_changed_dialog__af"
	.ascii	"fects_crop_coeff_and_rzwws\000"
.LASF29:
	.ascii	"FDTO_STATION_GROUP_show_mainline_assignment_dropdow"
	.ascii	"n\000"
.LASF23:
	.ascii	"FDTO_STATION_GROUP_show_moisture_sensor_assignment_"
	.ascii	"dropdown\000"
.LASF25:
	.ascii	"FDTO_STATION_GROUP_show_slope_percentage_dropdown\000"
.LASF35:
	.ascii	"FDTO_STATION_GROUP_close_mainline_assignment_dropdo"
	.ascii	"wn\000"
.LASF45:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF27:
	.ascii	"FDTO_STATION_GROUP_show_exposure_dropdown\000"
.LASF21:
	.ascii	"STATION_GROUP_update_moisture_sensor_assignment\000"
.LASF40:
	.ascii	"STATION_GROUP_pre_process_exposure_changed\000"
.LASF15:
	.ascii	"STATION_GROUP_process_plant_type_changed_dialog__af"
	.ascii	"fects_crop_coeff\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
