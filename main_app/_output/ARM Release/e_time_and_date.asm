	.file	"e_time_and_date.c"
	.text
.Ltext0:
	.section	.text.FDTO_TIME_DATE_show_time_zone_dropdown,"ax",%progbits
	.align	2
	.type	FDTO_TIME_DATE_show_time_zone_dropdown, %function
FDTO_TIME_DATE_show_time_zone_dropdown:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	ldr	r0, .L2+4
	ldr	r1, .L2+8
	ldr	r3, [r3, #0]
	mov	r2, #8
	b	FDTO_COMBOBOX_show
.L3:
	.align	2
.L2:
	.word	GuiVar_DateTimeTimeZone
	.word	750
	.word	FDTO_COMBOBOX_add_items
.LFE2:
	.size	FDTO_TIME_DATE_show_time_zone_dropdown, .-FDTO_TIME_DATE_show_time_zone_dropdown
	.section	.text.TIME_DATE_store_changes.constprop.2,"ax",%progbits
	.align	2
	.type	TIME_DATE_store_changes.constprop.2, %function
TIME_DATE_store_changes.constprop.2:
.LFB16:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	mov	r4, r1
	sub	sp, sp, #48
.LCFI1:
	bne	.L5
.LBB36:
	add	r0, sp, #8
	bl	EPSON_obtain_latest_complete_time_and_date
	add	r0, sp, #28
	bl	EPSON_obtain_latest_complete_time_and_date
	ldr	r3, .L12
	ldr	r2, .L12+4
	ldr	r3, [r3, #0]
	strb	r3, [sp, #46]
	ldr	r3, .L12+8
	ldr	r3, [r3, #0]
	strh	r3, [sp, #36]	@ movhi
	ldr	r3, .L12+12
	ldr	r3, [r3, #0]
	strh	r3, [sp, #34]	@ movhi
	ldr	r3, .L12+16
	ldr	r3, [r3, #0]
	strh	r3, [sp, #38]	@ movhi
	ldr	r3, .L12+20
	ldr	r3, [r3, #0]
	cmp	r3, #12
	bne	.L6
	ldr	r3, [r2, #0]
	cmp	r3, #0
	movne	r3, #12
	moveq	r3, #0
	b	.L11
.L6:
	ldr	r2, [r2, #0]
	cmp	r2, #0
	addne	r3, r3, #12
.L11:
	strh	r3, [sp, #40]	@ movhi
	ldr	r3, .L12+24
	ldrh	r0, [sp, #40]
	ldrh	r1, [r3, #0]
	ldr	r3, .L12+28
	strh	r1, [sp, #42]	@ movhi
	ldrh	r2, [r3, #0]
	add	r4, sp, #28
	strh	r2, [sp, #44]	@ movhi
	bl	HMSToTime
	ldr	r3, .L12+12
	str	r0, [sp, #28]
	ldr	r0, [r3, #0]
	ldr	r3, .L12+8
	ldr	r1, [r3, #0]
	ldr	r3, .L12+16
	ldr	r2, [r3, #0]
	bl	DMYToDate
	mov	r2, #2
	strh	r0, [sp, #32]	@ movhi
	ldmia	r4, {r0, r1}
	bl	EPSON_set_date_time
	cmp	r0, #0
	beq	.L4
	ldr	r5, .L12+32
	mov	r1, #400
	ldr	r0, [r5, #0]
	ldr	r2, .L12+36
	ldr	r3, .L12+40
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L12+44
	ldmia	r4, {r0, r1}
	add	r2, r3, #464
	strh	r1, [r2, #0]	@ movhi
	mov	r2, #2
	str	r2, [r3, #468]
	mov	r2, #1
	str	r0, [r3, #460]
	str	r2, [r3, #456]
	ldr	r0, [r5, #0]
	bl	xQueueGiveMutexRecursive
	b	.L4
.L5:
.LBE36:
	mov	r0, #2
	bl	NETWORK_CONFIG_get_change_bits_ptr
	ldr	r3, .L12+48
	mov	r1, #1
	str	r1, [sp, #0]
	mov	r2, #2
	str	r0, [sp, #4]
	ldr	r0, [r3, #0]
	mov	r3, r4
	bl	NETWORK_CONFIG_set_time_zone
.L4:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, pc}
.L13:
	.align	2
.L12:
	.word	GuiVar_DateTimeDayOfWeek
	.word	GuiVar_DateTimeAMPM
	.word	GuiVar_DateTimeMonth
	.word	GuiVar_DateTimeDay
	.word	GuiVar_DateTimeYear
	.word	GuiVar_DateTimeHour
	.word	GuiVar_DateTimeMin
	.word	GuiVar_DateTimeSec
	.word	comm_mngr_recursive_MUTEX
	.word	.LC0
	.word	538
	.word	comm_mngr
	.word	GuiVar_DateTimeTimeZone
.LFE16:
	.size	TIME_DATE_store_changes.constprop.2, .-TIME_DATE_store_changes.constprop.2
	.global	__umodsi3
	.section	.text.FDTO_TIME_DATE_update_screen,"ax",%progbits
	.align	2
	.global	FDTO_TIME_DATE_update_screen
	.type	FDTO_TIME_DATE_update_screen, %function
FDTO_TIME_DATE_update_screen:
.LFB11:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L17
	stmfd	sp!, {r4, lr}
.LCFI2:
	ldr	r3, [r3, #0]
	sub	sp, sp, #20
.LCFI3:
	cmp	r3, #0
	bne	.L14
	mov	r0, sp
	bl	EPSON_obtain_latest_complete_time_and_date
	ldrb	r2, [sp, #18]	@ zero_extendqisi2
	ldr	r3, .L17+4
	ldrh	r4, [sp, #12]
	str	r2, [r3, #0]
	ldrh	r2, [sp, #8]
	ldr	r3, .L17+8
	mov	r0, r4
	str	r2, [r3, #0]
	ldrh	r2, [sp, #6]
	ldr	r3, .L17+12
	mov	r1, #12
	str	r2, [r3, #0]
	ldrh	r2, [sp, #10]
	ldr	r3, .L17+16
	str	r2, [r3, #0]
	bl	__umodsi3
	ldr	r3, .L17+20
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	cmp	r0, #0
	moveq	r2, #12
	str	r0, [r3, #0]
	streq	r2, [r3, #0]
	ldrh	r2, [sp, #14]
	ldr	r3, .L17+24
	cmp	r4, #11
	str	r2, [r3, #0]
	ldr	r3, .L17+28
	ldrh	r2, [sp, #16]
	movls	r4, #0
	str	r2, [r3, #0]
	ldr	r3, .L17+32
	movhi	r4, #1
	str	r4, [r3, #0]
	bl	GuiLib_Refresh
.L14:
	add	sp, sp, #20
	ldmfd	sp!, {r4, pc}
.L18:
	.align	2
.L17:
	.word	GuiVar_DateTimeShowButton
	.word	GuiVar_DateTimeDayOfWeek
	.word	GuiVar_DateTimeMonth
	.word	GuiVar_DateTimeDay
	.word	GuiVar_DateTimeYear
	.word	GuiVar_DateTimeHour
	.word	GuiVar_DateTimeMin
	.word	GuiVar_DateTimeSec
	.word	GuiVar_DateTimeAMPM
.LFE11:
	.size	FDTO_TIME_DATE_update_screen, .-FDTO_TIME_DATE_update_screen
	.section	.text.FDTO_TIME_DATE_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_TIME_DATE_draw_screen
	.type	FDTO_TIME_DATE_draw_screen, %function
FDTO_TIME_DATE_draw_screen:
.LFB13:
	@ args = 0, pretend = 0, frame = 20
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L23
	stmfd	sp!, {r4, lr}
.LCFI4:
	ldrnesh	r1, [r3, #0]
	sub	sp, sp, #20
.LCFI5:
	bne	.L22
.LBB37:
	ldr	r3, .L23+4
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r3, .L23+8
	mov	r2, #3
	str	r2, [r3, #0]
	mov	r0, sp
	bl	EPSON_obtain_latest_complete_time_and_date
	ldrb	r2, [sp, #18]	@ zero_extendqisi2
	ldr	r3, .L23+12
	ldrh	r4, [sp, #12]
	str	r2, [r3, #0]
	ldrh	r2, [sp, #8]
	ldr	r3, .L23+16
	mov	r0, r4
	str	r2, [r3, #0]
	ldrh	r2, [sp, #6]
	ldr	r3, .L23+20
	mov	r1, #12
	str	r2, [r3, #0]
	ldrh	r2, [sp, #10]
	ldr	r3, .L23+24
	str	r2, [r3, #0]
	bl	__umodsi3
	ldr	r3, .L23+28
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	cmp	r0, #0
	moveq	r2, #12
	str	r0, [r3, #0]
	streq	r2, [r3, #0]
	ldrh	r2, [sp, #14]
	ldr	r3, .L23+32
	cmp	r4, #11
	str	r2, [r3, #0]
	ldrh	r2, [sp, #16]
	ldr	r3, .L23+36
	movls	r4, #0
	str	r2, [r3, #0]
	ldr	r3, .L23+40
	movhi	r4, #1
	str	r4, [r3, #0]
	bl	NETWORK_CONFIG_get_time_zone
	ldr	r3, .L23+44
	mov	r2, #0
	mov	r1, #7
	str	r0, [r3, #0]
	ldr	r3, .L23+48
	str	r2, [r3, #0]
.L22:
.LBE37:
	mov	r0, #15
	mov	r2, #1
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
	add	sp, sp, #20
	ldmfd	sp!, {r4, pc}
.L24:
	.align	2
.L23:
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	GuiVar_DateTimeDayOfWeek
	.word	GuiVar_DateTimeMonth
	.word	GuiVar_DateTimeDay
	.word	GuiVar_DateTimeYear
	.word	GuiVar_DateTimeHour
	.word	GuiVar_DateTimeMin
	.word	GuiVar_DateTimeSec
	.word	GuiVar_DateTimeAMPM
	.word	GuiVar_DateTimeTimeZone
	.word	GuiVar_DateTimeShowButton
.LFE13:
	.size	FDTO_TIME_DATE_draw_screen, .-FDTO_TIME_DATE_draw_screen
	.section	.text.TIME_DATE_process_screen,"ax",%progbits
	.align	2
	.global	TIME_DATE_process_screen
	.type	TIME_DATE_process_screen, %function
TIME_DATE_process_screen:
.LFB14:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L123
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI6:
	ldrsh	r2, [r3, #0]
	ldr	r3, .L123+4
	sub	sp, sp, #48
.LCFI7:
	cmp	r2, r3
	mov	r6, r0
	mov	r4, r1
	bne	.L101
	ldr	r1, .L123+8
	bl	COMBO_BOX_key_press
	b	.L25
.L101:
	cmp	r0, #3
	beq	.L33
	bhi	.L36
	cmp	r0, #1
	beq	.L31
	ldr	r3, .L123+12
	bhi	.L32
	b	.L120
.L36:
	cmp	r0, #80
	beq	.L35
	cmp	r0, #84
	beq	.L35
	cmp	r0, #4
	bne	.L103
	b	.L121
.L32:
	ldrsh	r3, [r3, #0]
	cmp	r3, #6
	beq	.L38
	cmp	r3, #7
	bne	.L91
	b	.L122
.L38:
	bl	good_key_beep
	bl	FLOWSENSE_get_controller_index
	mov	r4, #0
	mov	r1, r0
	mov	r0, #1
	bl	TIME_DATE_store_changes.constprop.2
	ldr	r3, .L123+16
	mov	r1, r4
	str	r4, [r3, #0]
	ldr	r3, .L123+20
	ldr	r0, [r3, #0]
	bl	CURSOR_Select
	mov	r0, r4
	b	.L113
.L122:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #8]
	ldr	r3, .L123+24
	add	r0, sp, #8
	str	r3, [sp, #28]
	bl	Display_Post_Command
	b	.L25
.L35:
	ldr	r3, .L123+16
	ldr	r5, [r3, #0]
	ldr	r3, .L123+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L40
.L48:
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L40
	.word	.L47
.L41:
.LBB62:
	bl	good_key_beep
	cmp	r6, #84
	ldr	r3, .L123+28
	ldr	r2, .L123+32
	bne	.L49
	ldr	r1, [r3, #0]
	cmp	r1, #11
	addls	r1, r1, #1
	bls	.L106
	ldr	r3, [r2, #0]
.LBB63:
	ldr	r2, .L123+36
	cmp	r3, r2
	ldrhi	r3, .L123+40
.LBE63:
	ldr	r2, .L123+32
.LBB64:
	addls	r3, r3, #1
.LBE64:
	str	r3, [r2, #0]
	mov	r2, #1
	b	.L107
.L49:
	cmp	r6, #80
	bne	.L51
	ldr	r1, [r3, #0]
	cmp	r1, #1
	bls	.L53
	sub	r1, r1, #1
.L106:
	str	r1, [r3, #0]
	b	.L51
.L53:
	ldr	r3, [r2, #0]
.LBB65:
	ldr	r2, .L123+40
	cmp	r3, r2
	ldrls	r3, .L123+44
.LBE65:
	ldr	r2, .L123+32
.LBB66:
	subhi	r3, r3, #1
.LBE66:
	str	r3, [r2, #0]
	mov	r2, #12
.L107:
	ldr	r3, .L123+28
	str	r2, [r3, #0]
.L51:
	ldr	r6, .L123+28
	ldr	r4, .L123+32
	ldr	r7, .L123+48
	ldr	r0, [r6, #0]
	ldr	r1, [r4, #0]
	ldr	r8, [r7, #0]
	bl	NumberOfDaysInMonth
	cmp	r8, r0
	bls	.L55
	ldr	r0, [r6, #0]
	ldr	r1, [r4, #0]
	bl	NumberOfDaysInMonth
	str	r0, [r7, #0]
.L55:
.LBB67:
	ldr	r0, [r7, #0]
	ldr	r1, [r6, #0]
	ldr	r2, [r4, #0]
	b	.L110
.L42:
.LBE67:
.LBE62:
.LBB68:
	bl	good_key_beep
	cmp	r6, #84
	ldr	r4, .L123+28
	ldr	r7, .L123+48
	bne	.L57
	ldr	r6, .L123+32
	ldr	r0, [r4, #0]
	ldr	r1, [r6, #0]
	ldr	r8, [r7, #0]
	bl	NumberOfDaysInMonth
	cmp	r8, r0
	ldrcc	r3, [r7, #0]
	addcc	r3, r3, #1
	bcc	.L108
	ldr	r3, [r4, #0]
	cmp	r3, #11
	addls	r3, r3, #1
	strls	r3, [r4, #0]
	bls	.L61
	ldr	r3, [r6, #0]
.LBB69:
	ldr	r2, .L123+36
	cmp	r3, r2
	ldrhi	r3, .L123+40
.LBE69:
	ldr	r2, .L123+32
.LBB70:
	addls	r3, r3, #1
.LBE70:
	str	r3, [r2, #0]
	ldr	r3, .L123+28
	mov	r2, #1
	str	r2, [r3, #0]
.L61:
	mov	r2, #1
	ldr	r3, .L123+48
	b	.L68
.L57:
	cmp	r6, #80
	bne	.L59
	ldr	r3, [r7, #0]
	cmp	r3, #1
	bls	.L63
	sub	r3, r3, #1
.L108:
	str	r3, [r7, #0]
	b	.L59
.L63:
	ldr	r3, [r4, #0]
	cmp	r3, #1
	subhi	r3, r3, #1
	strhi	r3, [r4, #0]
	bhi	.L65
	ldr	r2, .L123+32
.LBB71:
	ldr	r1, .L123+40
.LBE71:
	ldr	r3, [r2, #0]
.LBB72:
	cmp	r3, r1
	ldrls	r3, .L123+44
	subhi	r3, r3, #1
.LBE72:
	str	r3, [r2, #0]
	ldr	r3, .L123+28
	mov	r2, #12
	str	r2, [r3, #0]
.L65:
	ldr	r3, .L123+28
	ldr	r0, [r3, #0]
	ldr	r3, .L123+32
	ldr	r1, [r3, #0]
	bl	NumberOfDaysInMonth
	ldr	r3, .L123+48
	str	r0, [r3, #0]
.L59:
.LBB73:
	ldr	r3, .L123+48
	ldr	r0, [r3, #0]
	ldr	r3, .L123+28
	ldr	r1, [r3, #0]
	ldr	r3, .L123+32
	ldr	r2, [r3, #0]
.L110:
	bl	DMYToDate
	bl	DayOfWeek
.LBE73:
	ldr	r3, .L123+52
	str	r0, [r3, #0]
.L111:
	ldr	r3, .L123+16
	mov	r2, #1
	str	r2, [r3, #0]
	b	.L56
.L43:
.LBE68:
.LBB74:
	bl	good_key_beep
	cmp	r6, #84
	ldr	r3, .L123+32
	bne	.L67
	ldr	r2, [r3, #0]
.LBB75:
	ldr	r1, .L123+36
	cmp	r2, r1
	ldrhi	r2, .L123+40
	addls	r2, r2, #1
.L68:
.LBE75:
	str	r2, [r3, #0]
	b	.L59
.L67:
	cmp	r6, #80
	bne	.L59
	ldr	r3, [r3, #0]
.LBB76:
	ldr	r2, .L123+40
	cmp	r3, r2
	ldrls	r3, .L123+44
.LBE76:
	ldr	r2, .L123+32
.LBB77:
	subhi	r3, r3, #1
.LBE77:
	str	r3, [r2, #0]
	b	.L59
.L44:
.LBE74:
.LBB78:
	ldr	r3, .L123+56
	ldr	r2, .L123+60
	ldr	r3, [r3, #0]
	cmp	r3, #12
	bne	.L71
	ldr	r3, [r2, #0]
	cmp	r3, #0
	movne	r3, #12
	moveq	r3, #0
	b	.L109
.L71:
	ldr	r2, [r2, #0]
	cmp	r2, #0
	addne	r3, r3, #12
.L109:
	str	r3, [sp, #44]
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r6
	mov	r3, #23
	add	r1, sp, #44
	bl	process_uns32
	ldr	r4, [sp, #44]
	mov	r1, #12
	mov	r0, r4
	bl	__umodsi3
	ldr	r3, .L123+56
	cmp	r0, #0
	moveq	r2, #12
	str	r0, [r3, #0]
	streq	r2, [r3, #0]
	ldr	r3, .L123+60
	cmp	r4, #11
	movls	r4, #0
	movhi	r4, #1
	str	r4, [r3, #0]
	ldr	r3, .L123+64
	mov	r2, #55
	str	r2, [r3, #0]
	b	.L111
.L45:
.LBE78:
.LBB79:
	mov	r4, #1
	mov	r2, #0
	mov	r3, #59
	mov	r0, r6
	ldr	r1, .L123+68
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	bl	process_uns32
	ldr	r3, .L123+64
	mov	r2, #55
	str	r2, [r3, #0]
	b	.L112
.L46:
.LBE79:
.LBB80:
	mov	r4, #1
	mov	r0, r6
	ldr	r1, .L123+64
	mov	r2, #0
	mov	r3, #59
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	bl	process_uns32
.L112:
	ldr	r3, .L123+16
	str	r4, [r3, #0]
	b	.L56
.L47:
.LBE80:
	mov	r3, #1
	str	r3, [sp, #0]
	mov	r2, #0
	mov	r0, r6
	ldr	r1, .L123+8
	mov	r3, #7
	str	r2, [sp, #4]
	bl	process_uns32
	b	.L56
.L40:
	bl	bad_key_beep
.L56:
	ldr	r3, .L123+16
	ldr	r3, [r3, #0]
	cmp	r5, r3
	bne	.L76
	ldr	r3, .L123+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #7
	bne	.L77
.L76:
	mov	r0, #0
.L113:
	bl	Redraw_Screen
	b	.L25
.L77:
	bl	Refresh_Screen
	b	.L25
.L121:
	ldr	r3, .L123+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #7
	ldrls	pc, [pc, r3, asl #2]
	b	.L78
.L83:
	.word	.L91
	.word	.L91
	.word	.L91
	.word	.L80
	.word	.L80
	.word	.L80
	.word	.L119
	.word	.L82
.L80:
	ldr	r2, .L123+20
	str	r3, [r2, #0]
	ldr	r3, .L123+72
	b	.L118
.L82:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	beq	.L91
	ldr	r3, .L123+16
	ldr	r1, [r3, #0]
	cmp	r1, #1
	bne	.L119
	b	.L117
.L78:
	mov	r0, #1
	b	.L116
.L120:
	ldrsh	r3, [r3, #0]
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L105
.L89:
	.word	.L87
	.word	.L87
	.word	.L87
	.word	.L88
	.word	.L88
	.word	.L88
.L87:
	ldr	r2, .L123+72
	str	r3, [r2, #0]
.L119:
	ldr	r3, .L123+20
.L118:
	ldr	r0, [r3, #0]
	b	.L114
.L88:
	ldr	r2, .L123+20
	str	r3, [r2, #0]
	ldr	r3, .L123+16
	ldr	r1, [r3, #0]
	cmp	r1, #1
	movne	r0, #7
	bne	.L114
.L117:
	mov	r0, #6
	b	.L115
.L114:
	mov	r1, #1
.L115:
	bl	CURSOR_Select
	b	.L25
.L31:
	bl	FLOWSENSE_we_are_a_master_one_way_or_another
	cmp	r0, #0
	beq	.L91
	mov	r0, r6
.L116:
	bl	CURSOR_Up
	b	.L25
.L91:
	bl	bad_key_beep
	b	.L25
.L33:
	ldr	r3, .L123+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #5
	ldreq	r2, .L123+20
	streq	r3, [r2, #0]
.L105:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L25
.L103:
	cmp	r0, #67
	bne	.L94
	bl	FLOWSENSE_get_controller_index
	mov	r1, r0
	mov	r0, #0
	bl	TIME_DATE_store_changes.constprop.2
	ldr	r3, .L123+76
	mov	r2, #11
	str	r2, [r3, #0]
.L94:
	mov	r0, r6
	mov	r1, r4
	bl	KEY_process_global_keys
.L25:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L124:
	.align	2
.L123:
	.word	GuiLib_CurStructureNdx
	.word	750
	.word	GuiVar_DateTimeTimeZone
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_DateTimeShowButton
	.word	.LANCHOR1
	.word	FDTO_TIME_DATE_show_time_zone_dropdown
	.word	GuiVar_DateTimeMonth
	.word	GuiVar_DateTimeYear
	.word	2077
	.word	1993
	.word	2078
	.word	GuiVar_DateTimeDay
	.word	GuiVar_DateTimeDayOfWeek
	.word	GuiVar_DateTimeHour
	.word	GuiVar_DateTimeAMPM
	.word	GuiVar_DateTimeSec
	.word	GuiVar_DateTimeMin
	.word	.LANCHOR0
	.word	GuiVar_MenuScreenToShow
.LFE14:
	.size	TIME_DATE_process_screen, .-TIME_DATE_process_screen
	.section	.bss.g_TIME_DATE_previous_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_TIME_DATE_previous_cursor_pos, %object
	.size	g_TIME_DATE_previous_cursor_pos, 4
g_TIME_DATE_previous_cursor_pos:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_time_and_date.c\000"
	.section	.bss.g_TIME_DATE_previous_time_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_TIME_DATE_previous_time_cursor_pos, %object
	.size	g_TIME_DATE_previous_time_cursor_pos, 4
g_TIME_DATE_previous_time_cursor_pos:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.byte	0x4
	.4byte	.LCFI0-.LFB16
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI2-.LFB11
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x1c
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI4-.LFB13
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x1c
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI6-.LFB14
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_time_and_date.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xdf
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF14
	.byte	0x1
	.4byte	.LASF15
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x5e
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x81
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0xaa
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x1e3
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x26c
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x142
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x1a9
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x1c3
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF16
	.byte	0x1
	.byte	0x91
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.4byte	0x39
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LLST0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x23a
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x292
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST2
	.uleb128 0x2
	.4byte	.LASF10
	.byte	0x1
	.byte	0xc5
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x100
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x164
	.byte	0x1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x2b4
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB16
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI1
	.4byte	.LFE16
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB11
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI3
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB13
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI5
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB14
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI7
	.4byte	.LFE14
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF9:
	.ascii	"FDTO_TIME_DATE_draw_screen\000"
.LASF4:
	.ascii	"TIME_DATE_copy_settings_into_guivars\000"
.LASF8:
	.ascii	"FDTO_TIME_DATE_update_screen\000"
.LASF16:
	.ascii	"FDTO_TIME_DATE_show_time_zone_dropdown\000"
.LASF15:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_time_and_date.c\000"
.LASF11:
	.ascii	"TIME_DATE_process_day\000"
.LASF14:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF12:
	.ascii	"TIME_DATE_process_hour\000"
.LASF10:
	.ascii	"TIME_DATE_process_month\000"
.LASF13:
	.ascii	"TIME_DATE_process_screen\000"
.LASF0:
	.ascii	"TIME_DATE_increment_year\000"
.LASF1:
	.ascii	"TIME_DATE_decrement_year\000"
.LASF7:
	.ascii	"TIME_DATE_process_seconds\000"
.LASF2:
	.ascii	"TIME_DATE_get_day_of_week\000"
.LASF5:
	.ascii	"TIME_DATE_process_year\000"
.LASF6:
	.ascii	"TIME_DATE_process_minutes\000"
.LASF3:
	.ascii	"TIME_DATE_store_changes\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
