	.file	"lpc_arm922t_cp15_driver.c"
	.text
.Ltext0:
	.section	.text.cp15_decode_level2,"ax",%progbits
	.align	2
	.type	cp15_decode_level2, %function
cp15_decode_level2:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	and	r3, r0, #3
	cmp	r3, #2
	beq	.L4
	cmp	r3, #3
	beq	.L5
	cmp	r3, #1
	bne	.L9
.LBB10:
	eor	r3, r1, r0
	mov	r3, r3, lsr #16
	mov	r3, r3, asl #16
	cmp	r3, #0
	bne	.L9
	mov	r0, r0, lsr #16
	mov	r1, r1, asl #16
	mov	r0, r0, asl #16
	orr	r0, r0, r1, lsr #16
	bx	lr
.L4:
.LBE10:
	eor	r3, r1, r0
	bic	r3, r3, #4080
	bic	r3, r3, #15
	cmp	r3, #0
	bne	.L9
	bic	r0, r0, #4080
	mov	r1, r1, asl #20
	bic	r0, r0, #15
	orr	r0, r0, r1, lsr #20
	bx	lr
.L5:
	eor	r3, r1, r0
	bic	r3, r3, #1020
	bic	r3, r3, #3
	cmp	r3, #0
	bne	.L10
	bic	r0, r0, #1020
	mov	r1, r1, asl #22
	bic	r0, r0, #3
	orr	r0, r0, r1, lsr #22
	bx	lr
.L9:
	mov	r0, #0
	bx	lr
.L10:
	mov	r0, #0
	bx	lr
.LFE0:
	.size	cp15_decode_level2, .-cp15_decode_level2
	.section	.text.cp15_map_virtual_to_physical,"ax",%progbits
	.align	2
	.global	cp15_map_virtual_to_physical
	.type	cp15_map_virtual_to_physical, %function
cp15_map_virtual_to_physical:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
@ 255 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r3, c1, c0, 0
@ 0 "" 2
	tst	r3, #1
	bxeq	lr
	ldr	r3, .L26
	mov	r2, r0, lsr #20
	ldr	r3, [r3, #0]
	ldr	r3, [r3, r2, asl #2]
	and	r2, r3, #3
	cmp	r2, #2
	beq	.L15
	cmp	r2, #3
	beq	.L16
	cmp	r2, #1
	movne	r0, #0
	bxne	lr
	bic	r3, r3, #1020
	mov	r2, r0, lsr #12
	bic	r3, r3, #3
	and	r2, r2, #255
	b	.L25
.L15:
	bic	r0, r0, #-16777216
	mov	r3, r3, lsr #20
	bic	r0, r0, #15728640
	mov	r3, r3, asl #20
	orr	r0, r3, r0
	bx	lr
.L16:
	bic	r3, r3, #4080
	mov	r2, r0, asl #12
	bic	r3, r3, #15
	mov	r2, r2, lsr #22
.L25:
	ldr	r3, [r3, r2, asl #2]
	and	r2, r3, #3
	cmp	r2, #2
	beq	.L19
	cmp	r2, #3
	beq	.L20
	cmp	r2, #1
	bne	.L24
	mov	r3, r3, lsr #16
	mov	r0, r0, asl #16
	mov	r3, r3, asl #16
	orr	r0, r3, r0, lsr #16
	bx	lr
.L19:
	bic	r3, r3, #4080
	mov	r0, r0, asl #20
	bic	r3, r3, #15
	orr	r0, r3, r0, lsr #20
	bx	lr
.L20:
	bic	r3, r3, #1020
	mov	r0, r0, asl #22
	bic	r3, r3, #3
	orr	r0, r3, r0, lsr #22
	bx	lr
.L24:
	mov	r0, #0
	bx	lr
.L27:
	.align	2
.L26:
	.word	.LANCHOR0
.LFE1:
	.size	cp15_map_virtual_to_physical, .-cp15_map_virtual_to_physical
	.section	.text.cp15_map_physical_to_virtual,"ax",%progbits
	.align	2
	.global	cp15_map_physical_to_virtual
	.type	cp15_map_physical_to_virtual, %function
cp15_map_physical_to_virtual:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI0:
	mov	r4, r0
@ 386 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r3, c1, c0, 0
@ 0 "" 2
	tst	r3, #1
	moveq	r0, r0
	ldmeqfd	sp!, {r4, r5, r6, r7, r8, pc}
	ldr	r3, .L40
	mov	r5, #0
	ldr	r8, [r3, #0]
.L37:
	ldr	r7, [r8], #4
	and	r3, r7, #3
	cmp	r3, #2
	beq	.L33
	cmp	r3, #3
	beq	.L34
	cmp	r3, #1
	bne	.L31
	bic	r7, r7, #1020
	bic	r7, r7, #3
	ldr	r0, [r7, #0]
	cmp	r0, #0
	beq	.L31
	mov	r1, r4
	bl	cp15_decode_level2
	cmp	r0, #0
	beq	.L31
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L33:
	eor	r7, r7, r4
	mov	r7, r7, lsr #20
	mov	r7, r7, asl #20
	cmp	r7, #0
	bne	.L31
	bic	r0, r4, #-16777216
	bic	r0, r0, #15728640
	orr	r0, r0, r5, asl #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L34:
	bic	r7, r7, #4080
	bic	r7, r7, #15
	mov	r6, #0
.L36:
	ldr	r0, [r6, r7]
	cmp	r0, #0
	beq	.L35
	mov	r1, r4
	bl	cp15_decode_level2
	cmp	r0, #0
	ldmnefd	sp!, {r4, r5, r6, r7, r8, pc}
.L35:
	add	r6, r6, #4
	cmp	r6, #4096
	bne	.L36
.L31:
	add	r5, r5, #1
	cmp	r5, #4096
	bne	.L37
	mov	r0, #0
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L41:
	.align	2
.L40:
	.word	.LANCHOR0
.LFE2:
	.size	cp15_map_physical_to_virtual, .-cp15_map_physical_to_virtual
	.section	.text.cp15_force_cache_coherence,"ax",%progbits
	.align	2
	.global	cp15_force_cache_coherence
	.type	cp15_force_cache_coherence, %function
cp15_force_cache_coherence:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bic	r3, r0, #31
	b	.L43
.L44:
@ 533 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MOV r0, r3
@ 0 "" 2
@ 535 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r0, c7, c14, 1
@ 0 "" 2
@ 537 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r0, c7, c5, 1
@ 0 "" 2
	add	r3, r3, #32
.L43:
	cmp	r3, r1
	bcc	.L44
	bic	r0, r0, #1020
	bic	r0, r0, #3
	b	.L45
.L46:
@ 571 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MOV r0, r0
@ 0 "" 2
@ 573 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r0, c8, c5, 1
@ 0 "" 2
@ 574 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	NOP
@ 0 "" 2
@ 575 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	NOP
@ 0 "" 2
	add	r0, r0, #1024
.L45:
	cmp	r0, r1
	bcc	.L46
	bx	lr
.LFE3:
	.size	cp15_force_cache_coherence, .-cp15_force_cache_coherence
	.section	.text.cp15_init_mmu_trans_table,"ax",%progbits
	.align	2
	.global	cp15_init_mmu_trans_table
	.type	cp15_init_mmu_trans_table, %function
cp15_init_mmu_trans_table:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
@ 649 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r3, c1, c0, 0
@ 0 "" 2
	tst	r3, #1
	mvnne	r0, #0
	ldmnefd	sp!, {r4, r5, r6, pc}
	mov	r3, r0, asl #18
	movs	r3, r3, lsr #18
	bne	.L58
	mov	r2, r3
.L49:
	str	r2, [r0, r3]
	add	r3, r3, #4
	cmp	r3, #16384
	bne	.L49
	b	.L62
.L56:
	ldr	r4, [r1, #12]
	and	r3, r4, #3
	cmp	r3, #1
	beq	.L52
	cmp	r3, #2
	bne	.L51
	ldr	r5, [r1, #4]
	ldr	r3, [r1, #8]
	mov	r5, r5, lsr #20
	mov	r3, r3, lsr #20
	mov	r3, r3, asl #20
	mov	ip, #0
	add	r5, r0, r5, asl #2
	orr	r4, r4, #16
.L54:
	orr	r6, r4, r3
	str	r6, [r5, ip, asl #2]
	add	ip, ip, #1
	cmp	ip, r2
	add	r3, r3, #1048576
	bne	.L54
	b	.L51
.L52:
	ldr	r5, [r1, #4]
	ldr	r3, [r1, #8]
	mov	r5, r5, lsr #20
	bic	r3, r3, #1020
	bic	r3, r3, #3
	mov	ip, #0
	add	r5, r0, r5, asl #2
.L55:
	orr	r6, r3, r4
	str	r6, [r5, ip, asl #2]
	add	ip, ip, #1
	cmp	ip, r2
	add	r3, r3, #1048576
	bne	.L55
.L51:
	add	r1, r1, #16
.L62:
	ldr	r2, [r1, #0]
	cmp	r2, #0
	bne	.L56
	mov	r0, r2
	ldmfd	sp!, {r4, r5, r6, pc}
.L58:
	mvn	r0, #0
	ldmfd	sp!, {r4, r5, r6, pc}
.LFE4:
	.size	cp15_init_mmu_trans_table, .-cp15_init_mmu_trans_table
	.section	.text.cp15_set_vmmu_addr,"ax",%progbits
	.align	2
	.global	cp15_set_vmmu_addr
	.type	cp15_set_vmmu_addr, %function
cp15_set_vmmu_addr:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L64
	str	r0, [r3, #0]
	bx	lr
.L65:
	.align	2
.L64:
	.word	.LANCHOR0
.LFE5:
	.size	cp15_set_vmmu_addr, .-cp15_set_vmmu_addr
	.section	.text.cp15_get_ttb,"ax",%progbits
	.align	2
	.global	cp15_get_ttb
	.type	cp15_get_ttb, %function
cp15_get_ttb:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
@ 776 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r0, c2, c0, 0
@ 0 "" 2
	bx	lr
.LFE6:
	.size	cp15_get_ttb, .-cp15_get_ttb
	.section	.text.cp15_dcache_flush,"ax",%progbits
	.align	2
	.global	cp15_dcache_flush
	.type	cp15_dcache_flush, %function
cp15_dcache_flush:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
.LFE7:
	.size	cp15_dcache_flush, .-cp15_dcache_flush
	.section	.text.cp15_write_buffer_flush,"ax",%progbits
	.align	2
	.global	cp15_write_buffer_flush
	.type	cp15_write_buffer_flush, %function
cp15_write_buffer_flush:
.LFB8:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
.LFE8:
	.size	cp15_write_buffer_flush, .-cp15_write_buffer_flush
	.section	.text.cp15_mmu_enabled,"ax",%progbits
	.align	2
	.global	cp15_mmu_enabled
	.type	cp15_mmu_enabled, %function
cp15_mmu_enabled:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
@ 917 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r0, c1, c0, 0
@ 0 "" 2
	and	r0, r0, #1
	bx	lr
.LFE9:
	.size	cp15_mmu_enabled, .-cp15_mmu_enabled
	.section	.text.cp15_get_mmu_control_reg,"ax",%progbits
	.align	2
	.global	cp15_get_mmu_control_reg
	.type	cp15_get_mmu_control_reg, %function
cp15_get_mmu_control_reg:
.LFB10:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
@ 966 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r0, c1, c0, 0
@ 0 "" 2
	bx	lr
.LFE10:
	.size	cp15_get_mmu_control_reg, .-cp15_get_mmu_control_reg
	.section	.text.cp15_set_mmu_control_reg,"ax",%progbits
	.align	2
	.global	cp15_set_mmu_control_reg
	.type	cp15_set_mmu_control_reg, %function
cp15_set_mmu_control_reg:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
@ 1010 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r3, c1, c0, 0
@ 0 "" 2
	bx	lr
.LFE11:
	.size	cp15_set_mmu_control_reg, .-cp15_set_mmu_control_reg
	.section	.text.cp15_set_mmu,"ax",%progbits
	.align	2
	.global	cp15_set_mmu
	.type	cp15_set_mmu, %function
cp15_set_mmu:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LBB11:
@ 966 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r3, c1, c0, 0
@ 0 "" 2
.LBE11:
@ 1070 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r3, c1, c0, 0
@ 0 "" 2
	bx	lr
.LFE12:
	.size	cp15_set_mmu, .-cp15_set_mmu
	.section	.text.cp15_invalidate_cache,"ax",%progbits
	.align	2
	.global	cp15_invalidate_cache
	.type	cp15_invalidate_cache, %function
cp15_invalidate_cache:
.LFB13:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
@ 1118 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MOV r0, #0
@ 0 "" 2
@ 1119 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r0, c7, c7, 0
@ 0 "" 2
	bx	lr
.LFE13:
	.size	cp15_invalidate_cache, .-cp15_invalidate_cache
	.section	.text.cp15_invalidate_tlb,"ax",%progbits
	.align	2
	.global	cp15_invalidate_tlb
	.type	cp15_invalidate_tlb, %function
cp15_invalidate_tlb:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
@ 1167 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MOV r0, #0
	MCR p15, 0, r0, c8, c7, 0
	NOP
	NOP
	NOP
	
@ 0 "" 2
	bx	lr
.LFE14:
	.size	cp15_invalidate_tlb, .-cp15_invalidate_tlb
	.section	.text.cp15_set_transtable_base,"ax",%progbits
	.align	2
	.global	cp15_set_transtable_base
	.type	cp15_set_transtable_base, %function
cp15_set_transtable_base:
.LFB15:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
@ 1232 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r3, c2, c0, 0
@ 0 "" 2
	bx	lr
.LFE15:
	.size	cp15_set_transtable_base, .-cp15_set_transtable_base
	.section	.text.cp15_set_icache,"ax",%progbits
	.align	2
	.global	cp15_set_icache
	.type	cp15_set_icache, %function
cp15_set_icache:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LBB12:
@ 966 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r3, c1, c0, 0
@ 0 "" 2
.LBE12:
@ 1287 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r3, c1, c0, 0
@ 0 "" 2
	bx	lr
.LFE16:
	.size	cp15_set_icache, .-cp15_set_icache
	.section	.text.cp15_set_dcache,"ax",%progbits
	.align	2
	.global	cp15_set_dcache
	.type	cp15_set_dcache, %function
cp15_set_dcache:
.LFB17:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LBB13:
@ 966 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MRC p15, 0, r3, c1, c0, 0
@ 0 "" 2
.LBE13:
@ 1341 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r3, c1, c0, 0
@ 0 "" 2
	bx	lr
.LFE17:
	.size	cp15_set_dcache, .-cp15_set_dcache
	.section	.text.cp15_set_domain_access,"ax",%progbits
	.align	2
	.global	cp15_set_domain_access
	.type	cp15_set_domain_access, %function
cp15_set_domain_access:
.LFB18:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
@ 1395 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c" 1
	MCR p15, 0, r3, c3, c0, 0
@ 0 "" 2
	bx	lr
.LFE18:
	.size	cp15_set_domain_access, .-cp15_set_domain_access
	.global	virtual_tlb_addr
	.section	.bss.virtual_tlb_addr,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	virtual_tlb_addr, %object
	.size	virtual_tlb_addr, 4
virtual_tlb_addr:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI0-.LFB2
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.align	2
.LEFDE32:
.LSFDE34:
	.4byte	.LEFDE34-.LASFDE34
.LASFDE34:
	.4byte	.Lframe0
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.align	2
.LEFDE34:
.LSFDE36:
	.4byte	.LEFDE36-.LASFDE36
.LASFDE36:
	.4byte	.Lframe0
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.align	2
.LEFDE36:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/lpc_arm922t_cp15_driver.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x1a5
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF17
	.byte	0x1
	.4byte	.LASF18
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF19
	.byte	0x1
	.byte	0x94
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x3c1
	.byte	0x1
	.uleb128 0x4
	.4byte	0x21
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0xf5
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x174
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x205
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x276
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x2eb
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x303
	.4byte	.LFB6
	.4byte	.LFE6
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x32d
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x365
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x390
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	0x29
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x3ef
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x41d
	.4byte	.LFB12
	.4byte	.LFE12
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x45a
	.4byte	.LFB13
	.4byte	.LFE13
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x48c
	.4byte	.LFB14
	.4byte	.LFE14
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x4cb
	.4byte	.LFB15
	.4byte	.LFE15
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x4fb
	.4byte	.LFB16
	.4byte	.LFE16
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF15
	.byte	0x1
	.2byte	0x531
	.4byte	.LFB17
	.4byte	.LFE17
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x570
	.4byte	.LFB18
	.4byte	.LFE18
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB2
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB4
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0xac
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB17
	.4byte	.LFE17-.LFB17
	.4byte	.LFB18
	.4byte	.LFE18-.LFB18
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB17
	.4byte	.LFE17
	.4byte	.LFB18
	.4byte	.LFE18
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF12:
	.ascii	"cp15_invalidate_tlb\000"
.LASF4:
	.ascii	"cp15_set_vmmu_addr\000"
.LASF19:
	.ascii	"cp15_decode_level2\000"
.LASF16:
	.ascii	"cp15_set_domain_access\000"
.LASF0:
	.ascii	"cp15_map_virtual_to_physical\000"
.LASF8:
	.ascii	"cp15_mmu_enabled\000"
.LASF17:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF20:
	.ascii	"cp15_get_mmu_control_reg\000"
.LASF3:
	.ascii	"cp15_init_mmu_trans_table\000"
.LASF9:
	.ascii	"cp15_set_mmu_control_reg\000"
.LASF11:
	.ascii	"cp15_invalidate_cache\000"
.LASF15:
	.ascii	"cp15_set_dcache\000"
.LASF13:
	.ascii	"cp15_set_transtable_base\000"
.LASF5:
	.ascii	"cp15_get_ttb\000"
.LASF2:
	.ascii	"cp15_force_cache_coherence\000"
.LASF18:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/board_in"
	.ascii	"it/lpc_arm922t_cp15_driver.c\000"
.LASF14:
	.ascii	"cp15_set_icache\000"
.LASF6:
	.ascii	"cp15_dcache_flush\000"
.LASF1:
	.ascii	"cp15_map_physical_to_virtual\000"
.LASF10:
	.ascii	"cp15_set_mmu\000"
.LASF7:
	.ascii	"cp15_write_buffer_flush\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
