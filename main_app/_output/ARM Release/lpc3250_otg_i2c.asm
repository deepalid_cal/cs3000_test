	.file	"lpc3250_otg_i2c.c"
	.text
.Ltext0:
	.section	.text.otg_i2c_init,"ax",%progbits
	.align	2
	.global	otg_i2c_init
	.type	otg_i2c_init, %function
otg_i2c_init:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L4
	ldr	r2, [r3, #4084]
	orr	r2, r2, #30
	str	r2, [r3, #4084]
	mov	r2, r3
.L2:
	ldr	r1, [r2, #4088]
	ldr	r3, .L4
	and	r1, r1, #30
	cmp	r1, #30
	bne	.L2
	ldr	r2, [r3, #776]
	mov	r0, #0
	orr	r2, r2, #256
	str	r2, [r3, #776]
	mov	r2, #65
	str	r2, [r3, #784]
	str	r2, [r3, #780]
	mov	r2, #256
	str	r2, [r3, #776]
	bx	lr
.L5:
	.align	2
.L4:
	.word	822214656
.LFE0:
	.size	otg_i2c_init, .-otg_i2c_init
	.section	.text.otg_i2c_start,"ax",%progbits
	.align	2
	.global	otg_i2c_start
	.type	otg_i2c_start, %function
otg_i2c_start:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
.LFE1:
	.size	otg_i2c_start, .-otg_i2c_start
	.section	.text.otg_i2c_stop,"ax",%progbits
	.align	2
	.global	otg_i2c_stop
	.type	otg_i2c_stop, %function
otg_i2c_stop:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L10
	ldr	r2, [r3, #4084]
	bic	r2, r2, #12
	str	r2, [r3, #4084]
.L8:
	ldr	r2, [r3, #4088]
	and	r2, r2, #28
	cmp	r2, #12
	bne	.L8
	mov	r0, #0
	bx	lr
.L11:
	.align	2
.L10:
	.word	822214656
.LFE2:
	.size	otg_i2c_stop, .-otg_i2c_stop
	.section	.text.otg_i2c_delete,"ax",%progbits
	.align	2
	.global	otg_i2c_delete
	.type	otg_i2c_delete, %function
otg_i2c_delete:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
.LFE3:
	.size	otg_i2c_delete, .-otg_i2c_delete
	.section	.text.otg_i2c_read,"ax",%progbits
	.align	2
	.global	otg_i2c_read
	.type	otg_i2c_read, %function
otg_i2c_read:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L27
	mov	r3, #344
	and	r0, r0, #255
	str	r3, [r2, #768]
.L14:
	ldr	r3, [r2, #772]
	tst	r3, #2048
	beq	.L14
	ldr	r3, .L27
	str	r0, [r3, #768]
.L15:
	ldr	r1, [r2, #772]
	ldr	r3, .L27
	tst	r1, #2048
	beq	.L15
	ldr	r2, .L27+4
	str	r2, [r3, #768]
.L16:
	ldr	r2, [r3, #772]
	tst	r2, #2048
	ldr	r2, .L27
	beq	.L16
	mov	r3, #0
	str	r3, [r2, #768]
.L17:
	ldr	r1, [r2, #772]
	ldr	r3, .L27
	tst	r1, #2048
	beq	.L17
	mov	r2, #512
	str	r2, [r3, #768]
.L18:
	ldr	r2, [r3, #772]
	tst	r2, #2048
	ldr	r2, .L27
	beq	.L18
.L20:
	ldr	r1, [r2, #772]
	ldr	r3, .L27
	tst	r1, #512
	bne	.L20
	ldr	r2, [r3, #768]
	ldr	r0, [r3, #768]
	and	r0, r0, #255
	bx	lr
.L28:
	.align	2
.L27:
	.word	822214656
	.word	345
.LFE4:
	.size	otg_i2c_read, .-otg_i2c_read
	.section	.text.otg_i2c_write,"ax",%progbits
	.align	2
	.global	otg_i2c_write
	.type	otg_i2c_write, %function
otg_i2c_write:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, .L36
	mov	r3, #344
	and	r0, r0, #255
	and	r1, r1, #255
	str	r3, [r2, #768]
.L30:
	ldr	r3, [r2, #772]
	tst	r3, #2048
	beq	.L30
	ldr	r3, .L36
	str	r0, [r3, #768]
.L31:
	ldr	r0, [r2, #772]
	ldr	r3, .L36
	tst	r0, #2048
	beq	.L31
	orr	r1, r1, #512
	str	r1, [r3, #768]
.L32:
	ldr	r2, [r3, #772]
	tst	r2, #2048
	beq	.L32
	bx	lr
.L37:
	.align	2
.L36:
	.word	822214656
.LFE5:
	.size	otg_i2c_write, .-otg_i2c_write
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE10:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/lpc3250_otg_i2c.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x90
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF6
	.byte	0x1
	.4byte	.LASF7
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x5
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x14
	.4byte	.LFB1
	.4byte	.LFE1
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x1a
	.4byte	.LFB2
	.4byte	.LFE2
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0x22
	.4byte	.LFB3
	.4byte	.LFE3
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0x28
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0x44
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x44
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"otg_i2c_stop\000"
.LASF4:
	.ascii	"otg_i2c_read\000"
.LASF5:
	.ascii	"otg_i2c_write\000"
.LASF0:
	.ascii	"otg_i2c_init\000"
.LASF7:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/LPC3250/"
	.ascii	"lpc3250_otg_i2c.c\000"
.LASF1:
	.ascii	"otg_i2c_start\000"
.LASF6:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"otg_i2c_delete\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
