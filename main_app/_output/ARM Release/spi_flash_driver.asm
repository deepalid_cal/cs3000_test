	.file	"spi_flash_driver.c"
	.text
.Ltext0:
	.section	.text.identify_the_device,"ax",%progbits
	.align	2
	.global	identify_the_device
	.type	identify_the_device, %function
identify_the_device:
.LFB0:
	@ args = 0, pretend = 0, frame = 12
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI0:
	sub	sp, sp, #16
.LCFI1:
	mov	r5, r0
	mov	r1, #76
	ldr	r3, .L9
	mla	r1, r0, r1, r3
	ldr	r3, [r1, #12]
	mov	r2, #159
	str	r2, [r3, #8]
	mov	r2, #0
	str	r2, [r3, #8]
	str	r2, [r3, #8]
	str	r2, [r3, #8]
	ldr	r1, [r1, #4]
	ldr	r2, .L9+4
	str	r1, [r2, #8]
	mov	r2, #2
	str	r2, [r3, #4]
.L2:
	ldr	r2, [r3, #12]
	tst	r2, #16
	bne	.L2
	mov	r2, #0
	str	r2, [r3, #4]
	mov	r1, #76
	ldr	r2, .L9
	mla	r2, r1, r5, r2
	ldr	r1, [r2, #4]
	ldr	r2, .L9+4
	str	r1, [r2, #4]
	ldr	r2, [r3, #8]
	str	r2, [sp, #4]
	ldr	r2, [r3, #8]
	str	r2, [sp, #4]
	ldr	r2, [r3, #8]
	str	r2, [sp, #8]
	ldr	r3, [r3, #8]
	str	r3, [sp, #12]
	mov	r4, #1
	ldr	r6, .L9+8
	mov	r7, #12
.L5:
	add	r0, r4, r4, asl #1
	add	r0, r6, r0, asl #2
	add	r1, sp, #4
	mov	r2, r7
	bl	memcmp
	cmp	r0, #0
	ldreq	r3, .L9+12
	streq	r4, [r3, r5, asl #2]
	beq	.L1
.L3:
	add	r4, r4, #1
	cmp	r4, #4
	bne	.L5
	ldr	r2, [sp, #4]
	ldr	r3, [sp, #8]
	ldr	r1, [sp, #12]
	str	r1, [sp, #0]
	ldr	r0, .L9+16
	mov	r1, r5
	bl	Alert_Message_va
.L1:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L10:
	.align	2
.L9:
	.word	.LANCHOR0
	.word	1073905664
	.word	.LANCHOR1
	.word	.LANCHOR2
	.word	.LC1
.LFE0:
	.size	identify_the_device, .-identify_the_device
	.section	.text.wait_for_flash_device_to_be_idle,"ax",%progbits
	.align	2
	.global	wait_for_flash_device_to_be_idle
	.type	wait_for_flash_device_to_be_idle, %function
wait_for_flash_device_to_be_idle:
.LFB1:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI2:
	sub	sp, sp, #4
.LCFI3:
	mov	r7, r0
	mov	r6, r1
	mov	r2, #76
	ldr	r3, .L25
	mla	r3, r2, r0, r3
	ldr	r4, [r3, #12]
	ldr	r3, [r4, #12]
	tst	r3, #4
	beq	.L12
.L20:
	ldr	r3, [r4, #8]
	str	r3, [sp, #0]
	ldr	r3, [r4, #12]
	tst	r3, #4
	bne	.L20
.L12:
	mov	r3, #5
	str	r3, [r4, #8]
	mov	r3, #0
	str	r3, [r4, #8]
	mov	r2, #76
	ldr	r3, .L25
	mla	r3, r2, r7, r3
	ldr	r2, [r3, #4]
	ldr	r3, .L25+4
	str	r2, [r3, #8]
	mov	r3, #2
	str	r3, [r4, #4]
.L14:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L14
	ldr	r3, [r4, #8]
	str	r3, [sp, #0]
	ldr	r3, [r4, #8]
	str	r3, [sp, #0]
	ldr	r3, [sp, #0]
	tst	r3, #1
	beq	.L15
	mov	r5, #0
	ldr	r8, .L25+8
	mov	sl, #100
	ldr	r9, .L25+12
.L19:
	add	r5, r5, #1
	cmp	r6, #1
	bne	.L16
	umull	r2, r3, r8, r5
	mov	r3, r3, lsr #5
	mul	r3, sl, r3
	cmp	r5, r3
	bne	.L17
	mov	r0, r9
	mov	r1, r5
	bl	Alert_Message_va
.L17:
	mov	r0, #1
	bl	vTaskDelay
.L16:
	mov	r3, #0
	str	r3, [r4, #8]
.L18:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L18
	ldr	r3, [r4, #8]
	str	r3, [sp, #0]
	ldr	r3, [sp, #0]
	tst	r3, #1
	bne	.L19
.L15:
	mov	r3, #0
	str	r3, [r4, #4]
	mov	r2, #76
	ldr	r3, .L25
	mla	r7, r2, r7, r3
	ldr	r2, [r7, #4]
	ldr	r3, .L25+4
	str	r2, [r3, #4]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L26:
	.align	2
.L25:
	.word	.LANCHOR0
	.word	1073905664
	.word	1374389535
	.word	.LC2
.LFE1:
	.size	wait_for_flash_device_to_be_idle, .-wait_for_flash_device_to_be_idle
	.section	.text.send_command_to_flash_device,"ax",%progbits
	.align	2
	.global	send_command_to_flash_device
	.type	send_command_to_flash_device, %function
send_command_to_flash_device:
.LFB3:
	@ args = 20, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI4:
	sub	sp, sp, #4
.LCFI5:
	mov	r5, r0
	mov	r6, r2
	mov	r7, r3
	ldrb	sl, [sp, #36]	@ zero_extendqisi2
	ldrh	r8, [sp, #44]
	mov	r2, #76
	ldr	r3, .L38
	mla	r3, r2, r0, r3
	ldr	r4, [r3, #12]
	cmp	r1, #1
	bne	.L28
	mov	r1, #0
	bl	wait_for_flash_device_to_be_idle
.L28:
	str	r6, [r4, #8]
	ldr	r3, [sp, #32]
	cmp	r3, #1
	bne	.L29
	and	r3, r7, #16711680
	mov	r3, r3, lsr #16
	str	r3, [r4, #8]
	and	r3, r7, #65280
	mov	r3, r3, lsr #8
	str	r3, [r4, #8]
	and	r7, r7, #255
	str	r7, [r4, #8]
.L29:
	ldr	r3, [sp, #40]
	cmp	r3, #1
	streq	sl, [r4, #8]
	beq	.L31
	ldr	r3, [sp, #48]
	cmp	r3, #1
	andeq	r3, r8, #255
	streq	r3, [r4, #8]
	moveq	r8, r8, lsr #8
	streq	r8, [r4, #8]
.L31:
	mov	r2, #76
	ldr	r3, .L38
	mla	r3, r2, r5, r3
	ldr	r2, [r3, #4]
	ldr	r3, .L38+4
	str	r2, [r3, #8]
	mov	r3, #2
	str	r3, [r4, #4]
.L32:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L32
	mov	r3, #0
	str	r3, [r4, #4]
	mov	r2, #76
	ldr	r3, .L38
	mla	r5, r2, r5, r3
	ldr	r2, [r5, #4]
	ldr	r3, .L38+4
	str	r2, [r3, #4]
	ldr	r3, [r4, #12]
	tst	r3, #4
	beq	.L27
.L35:
	ldr	r3, [r4, #8]
	str	r3, [sp, #0]
	ldr	r3, [r4, #12]
	tst	r3, #4
	bne	.L35
.L27:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L39:
	.align	2
.L38:
	.word	.LANCHOR0
	.word	1073905664
.LFE3:
	.size	send_command_to_flash_device, .-send_command_to_flash_device
	.section	.text.flash_device_is_write_enabled,"ax",%progbits
	.align	2
	.global	flash_device_is_write_enabled
	.type	flash_device_is_write_enabled, %function
flash_device_is_write_enabled:
.LFB4:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #4
.LCFI6:
	mov	r2, #76
	ldr	r3, .L47
	mla	r3, r2, r0, r3
	ldr	r3, [r3, #12]
	ldr	r2, [r3, #12]
	tst	r2, #4
	beq	.L41
.L44:
	ldr	r2, [r3, #8]
	str	r2, [sp, #0]
	ldr	r2, [r3, #12]
	tst	r2, #4
	bne	.L44
.L41:
	mov	r2, #5
	str	r2, [r3, #8]
	mov	r2, #0
	str	r2, [r3, #8]
	mov	r1, #76
	ldr	r2, .L47
	mla	r2, r1, r0, r2
	ldr	r1, [r2, #4]
	ldr	r2, .L47+4
	str	r1, [r2, #8]
	mov	r2, #2
	str	r2, [r3, #4]
.L43:
	ldr	r2, [r3, #12]
	tst	r2, #16
	bne	.L43
	mov	r2, #0
	str	r2, [r3, #4]
	mov	r1, #76
	ldr	r2, .L47
	mla	r0, r1, r0, r2
	ldr	r1, [r0, #4]
	ldr	r2, .L47+4
	str	r1, [r2, #4]
	ldr	r2, [r3, #8]
	str	r2, [sp, #0]
	ldr	r3, [r3, #8]
	str	r3, [sp, #0]
	ldr	r3, [sp, #0]
	tst	r3, #2
	moveq	r0, #0
	movne	r0, #1
	add	sp, sp, #4
	bx	lr
.L48:
	.align	2
.L47:
	.word	.LANCHOR0
	.word	1073905664
.LFE4:
	.size	flash_device_is_write_enabled, .-flash_device_is_write_enabled
	.section	.text.unlock_and_write_enable_flash_device,"ax",%progbits
	.align	2
	.global	unlock_and_write_enable_flash_device
	.type	unlock_and_write_enable_flash_device, %function
unlock_and_write_enable_flash_device:
.LFB5:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI7:
	sub	sp, sp, #24
.LCFI8:
	mov	r5, r0
	mov	r2, #76
	ldr	r3, .L79
	mla	r3, r2, r0, r3
	ldr	r4, [r3, #12]
	ldr	r3, .L79+4
	ldr	r2, [r3, r0, asl #2]
	cmp	r2, #3
	bne	.L50
	ldr	r3, [r4, #12]
	tst	r3, #4
	beq	.L51
.L70:
	ldr	r3, [r4, #8]
	str	r3, [sp, #20]
	ldr	r3, [r4, #12]
	tst	r3, #4
	bne	.L70
.L51:
	mov	r3, #5
	str	r3, [r4, #8]
	mov	r3, #0
	str	r3, [r4, #8]
	mov	r2, #76
	ldr	r3, .L79
	mla	r3, r2, r5, r3
	ldr	r2, [r3, #4]
	ldr	r3, .L79+8
	str	r2, [r3, #8]
	mov	r3, #2
	str	r3, [r4, #4]
.L53:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L53
	mov	r3, #0
	str	r3, [r4, #4]
	mov	r2, #76
	ldr	r3, .L79
	mla	r3, r2, r5, r3
	ldr	r2, [r3, #4]
	ldr	r3, .L79+8
	str	r2, [r3, #4]
	ldr	r3, [r4, #8]
	str	r3, [sp, #20]
	ldr	r3, [r4, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #20]
	tst	r3, #60
	beq	.L54
	ldr	r3, [sp, #20]
	tst	r3, #4
	beq	.L55
	ldr	r0, .L79+12
	bl	Alert_Message
.L55:
	ldr	r3, [sp, #20]
	tst	r3, #8
	beq	.L56
	ldr	r0, .L79+16
	bl	Alert_Message
.L56:
	ldr	r3, [sp, #20]
	tst	r3, #16
	beq	.L57
	ldr	r0, .L79+20
	bl	Alert_Message
.L57:
	ldr	r3, [sp, #20]
	tst	r3, #32
	beq	.L54
	ldr	r0, .L79+24
	bl	Alert_Message
.L54:
	mov	r0, r5
	mov	r1, #1
	bl	wait_for_flash_device_to_be_idle
	mov	r3, #6
	str	r3, [r4, #8]
	mov	r2, #76
	ldr	r3, .L79
	mla	r3, r2, r5, r3
	ldr	r2, [r3, #4]
	ldr	r3, .L79+8
	str	r2, [r3, #8]
	mov	r3, #2
	str	r3, [r4, #4]
.L58:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L58
	mov	r3, #0
	str	r3, [r4, #4]
	ldr	r0, .L79
	mov	r1, #76
	mul	r1, r5, r1
	add	r3, r0, r1
	ldr	r2, [r3, #4]
	ldr	r3, .L79+8
	str	r2, [r3, #4]
	ldr	r1, [r0, r1]
	str	r1, [r3, #4]
	mov	r1, #152
	str	r1, [r4, #8]
	str	r2, [r3, #8]
	mov	r3, #2
	str	r3, [r4, #4]
.L59:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L59
	mov	r6, #0
	str	r6, [r4, #4]
	ldr	r1, .L79
	mov	r2, #76
	mul	r2, r5, r2
	add	r3, r1, r2
	ldr	r0, [r3, #4]
	ldr	r3, .L79+8
	str	r0, [r3, #4]
	ldr	r2, [r1, r2]
	str	r2, [r3, #8]
	mov	r0, r5
	mov	r1, #1
	bl	wait_for_flash_device_to_be_idle
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	str	r6, [sp, #8]
	str	r6, [sp, #12]
	str	r6, [sp, #16]
	mov	r0, r5
	mov	r1, #1
	mov	r2, #6
	mov	r3, r6
	bl	send_command_to_flash_device
	mov	r0, r5
	bl	flash_device_is_write_enabled
	cmp	r0, #1
	moveq	r0, #1
	beq	.L60
	ldr	r0, .L79+28
	bl	Alert_Message
	mov	r0, r6
	b	.L60
.L50:
	mov	r6, #0
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	str	r6, [sp, #8]
	str	r6, [sp, #12]
	str	r6, [sp, #16]
	mov	r1, #1
	cmp	r2, r1
	movne	r2, #6
	moveq	r2, #80
	mov	r3, r6
	bl	send_command_to_flash_device
	mov	r3, #1
	str	r3, [r4, #8]
	str	r6, [r4, #8]
	ldr	r2, .L79
	mov	r3, #76
	mul	r3, r5, r3
	add	r1, r2, r3
	ldr	r2, [r2, r3]
	ldr	r3, .L79+8
	str	r2, [r3, #4]
	ldr	r2, [r1, #4]
	str	r2, [r3, #8]
	mov	r3, #2
	str	r3, [r4, #4]
.L62:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L62
	mov	r3, #0
	str	r3, [r4, #4]
	ldr	r1, .L79
	mov	r2, #76
	mul	r2, r5, r2
	add	r3, r1, r2
	ldr	r0, [r3, #4]
	ldr	r3, .L79+8
	str	r0, [r3, #4]
	ldr	r2, [r1, r2]
	str	r2, [r3, #8]
	mov	r0, r5
	mov	r1, #1
	bl	wait_for_flash_device_to_be_idle
	ldr	r3, [r4, #12]
	tst	r3, #4
	beq	.L63
.L71:
	ldr	r3, [r4, #8]
	str	r3, [sp, #20]
	ldr	r3, [r4, #12]
	tst	r3, #4
	bne	.L71
.L63:
	mov	r3, #5
	str	r3, [r4, #8]
	mov	r3, #0
	str	r3, [r4, #8]
	mov	r2, #76
	ldr	r3, .L79
	mla	r3, r2, r5, r3
	ldr	r2, [r3, #4]
	ldr	r3, .L79+8
	str	r2, [r3, #8]
	mov	r3, #2
	str	r3, [r4, #4]
.L65:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L65
	mov	r3, #0
	str	r3, [r4, #4]
	mov	r2, #76
	ldr	r3, .L79
	mla	r3, r2, r5, r3
	ldr	r2, [r3, #4]
	ldr	r3, .L79+8
	str	r2, [r3, #4]
	ldr	r3, [r4, #8]
	str	r3, [sp, #20]
	ldr	r3, [r4, #8]
	str	r3, [sp, #20]
	ldr	r3, [sp, #20]
	tst	r3, #188
	bne	.L66
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	str	r3, [sp, #12]
	str	r3, [sp, #16]
	mov	r0, r5
	mov	r1, #1
	mov	r2, #6
	bl	send_command_to_flash_device
	mov	r0, r5
	bl	flash_device_is_write_enabled
	cmp	r0, #1
	moveq	r0, #1
	beq	.L60
	ldr	r0, .L79+28
	bl	Alert_Message
	mov	r0, #0
	b	.L60
.L66:
	ldr	r0, .L79+32
	bl	Alert_Message
	mov	r0, #0
.L60:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, pc}
.L80:
	.align	2
.L79:
	.word	.LANCHOR0
	.word	.LANCHOR2
	.word	1073905664
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
.LFE5:
	.size	unlock_and_write_enable_flash_device, .-unlock_and_write_enable_flash_device
	.section	.text.lock_flash_device,"ax",%progbits
	.align	2
	.global	lock_flash_device
	.type	lock_flash_device, %function
lock_flash_device:
.LFB6:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
.LCFI9:
	sub	sp, sp, #36
.LCFI10:
	mov	r5, r0
	add	r0, sp, #20
	ldr	r1, .L101
	mov	r2, #10
	bl	memcpy
	mov	r2, #76
	ldr	r3, .L101+4
	mla	r3, r2, r5, r3
	ldr	r4, [r3, #12]
	ldr	r3, .L101+8
	ldr	r2, [r3, r5, asl #2]
	cmp	r2, #3
	bne	.L82
	mov	r6, #0
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	str	r6, [sp, #8]
	str	r6, [sp, #12]
	str	r6, [sp, #16]
	mov	r0, r5
	mov	r1, #1
	mov	r2, #6
	mov	r3, r6
	bl	send_command_to_flash_device
	mov	r3, #1
	str	r3, [r4, #8]
	str	r6, [r4, #8]
	mov	r3, #128
	str	r3, [r4, #8]
	ldr	r2, .L101+4
	mov	r3, #76
	mul	r3, r5, r3
	add	r1, r2, r3
	ldr	r2, [r2, r3]
	ldr	r3, .L101+12
	str	r2, [r3, #4]
	ldr	r2, [r1, #4]
	str	r2, [r3, #8]
	mov	r3, #2
	str	r3, [r4, #4]
.L83:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L83
	mov	r6, #0
	str	r6, [r4, #4]
	ldr	r2, .L101+4
	mov	r3, #76
	mul	r3, r5, r3
	add	r1, r2, r3
	ldr	sl, [r1, #4]
	ldr	r7, .L101+12
	str	sl, [r7, #4]
	ldr	r8, [r2, r3]
	str	r8, [r7, #8]
	mov	r0, r5
	mov	r1, #1
	bl	wait_for_flash_device_to_be_idle
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	str	r6, [sp, #8]
	str	r6, [sp, #12]
	str	r6, [sp, #16]
	mov	r0, r5
	mov	r1, #1
	mov	r2, #6
	mov	r3, r6
	bl	send_command_to_flash_device
	mov	r3, #66
	str	r3, [r4, #8]
	str	r8, [r7, #4]
	str	sl, [r7, #8]
	mov	r3, #2
	str	r3, [r4, #4]
	add	r2, sp, #19
	add	r1, sp, #29
.L85:
	ldrb	r3, [r2, #1]!	@ zero_extendqisi2
	str	r3, [r4, #8]
.L84:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L84
	cmp	r2, r1
	bne	.L85
	mov	r6, #0
	str	r6, [r4, #4]
	ldr	r1, .L101+4
	mov	r2, #76
	mul	r2, r5, r2
	add	r3, r1, r2
	ldr	r0, [r3, #4]
	ldr	r3, .L101+12
	str	r0, [r3, #4]
	ldr	r2, [r1, r2]
	str	r2, [r3, #8]
	mov	r0, r5
	mov	r1, #1
	bl	wait_for_flash_device_to_be_idle
	mov	r0, r6
	b	.L86
.L82:
	mov	r6, #0
	str	r6, [sp, #0]
	str	r6, [sp, #4]
	str	r6, [sp, #8]
	str	r6, [sp, #12]
	str	r6, [sp, #16]
	mov	r0, r5
	mov	r1, #1
	cmp	r2, r1
	movne	r2, #6
	moveq	r2, #80
	mov	r3, r6
	bl	send_command_to_flash_device
	mov	r3, #1
	str	r3, [r4, #8]
	str	r6, [r4, #8]
	ldr	r2, .L101+4
	mov	r3, #76
	mul	r3, r5, r3
	add	r1, r2, r3
	ldr	r2, [r2, r3]
	ldr	r3, .L101+12
	str	r2, [r3, #4]
	ldr	r2, [r1, #4]
	str	r2, [r3, #8]
	mov	r3, #2
	str	r3, [r4, #4]
.L88:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L88
	mov	r3, #0
	str	r3, [r4, #4]
	ldr	r1, .L101+4
	mov	r2, #76
	mul	r2, r5, r2
	add	r3, r1, r2
	ldr	r0, [r3, #4]
	ldr	r3, .L101+12
	str	r0, [r3, #4]
	ldr	r2, [r1, r2]
	str	r2, [r3, #8]
	mov	r0, r5
	mov	r1, #1
	bl	wait_for_flash_device_to_be_idle
	ldr	r3, [r4, #12]
	tst	r3, #4
	beq	.L89
.L94:
	ldr	r3, [r4, #8]
	str	r3, [sp, #32]
	ldr	r3, [r4, #12]
	tst	r3, #4
	bne	.L94
.L89:
	mov	r3, #5
	str	r3, [r4, #8]
	mov	r3, #0
	str	r3, [r4, #8]
	mov	r2, #76
	ldr	r3, .L101+4
	mla	r3, r2, r5, r3
	ldr	r2, [r3, #4]
	ldr	r3, .L101+12
	str	r2, [r3, #8]
	mov	r3, #2
	str	r3, [r4, #4]
.L91:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L91
	mov	r3, #0
	str	r3, [r4, #4]
	mov	r2, #76
	ldr	r3, .L101+4
	mla	r5, r2, r5, r3
	ldr	r2, [r5, #4]
	ldr	r3, .L101+12
	str	r2, [r3, #4]
	ldr	r3, [r4, #8]
	str	r3, [sp, #32]
	ldr	r3, [r4, #8]
	str	r3, [sp, #32]
	ldr	r3, [sp, #32]
	tst	r3, #188
	moveq	r0, #1
	beq	.L86
	ldr	r0, .L101+16
	bl	Alert_Message
	mov	r0, #0
.L86:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, pc}
.L102:
	.align	2
.L101:
	.word	.LANCHOR3
	.word	.LANCHOR0
	.word	.LANCHOR2
	.word	1073905664
	.word	.LC9
.LFE6:
	.size	lock_flash_device, .-lock_flash_device
	.section	.text.init_FLASH_DRIVE_SSP_channel,"ax",%progbits
	.align	2
	.global	init_FLASH_DRIVE_SSP_channel
	.type	init_FLASH_DRIVE_SSP_channel, %function
init_FLASH_DRIVE_SSP_channel:
.LFB2:
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI11:
	sub	sp, sp, #24
.LCFI12:
	mov	r4, r0
	mov	r3, #8
	str	r3, [sp, #0]
	mov	r3, #0
	str	r3, [sp, #4]
	mov	r3, #1
	str	r3, [sp, #8]
	str	r3, [sp, #12]
	ldr	r2, .L106
	str	r2, [sp, #16]
	str	r3, [sp, #20]
	mov	r2, #76
	ldr	r3, .L106+4
	mla	r3, r2, r0, r3
	ldr	r0, [r3, #12]
	mov	r1, sp
	bl	ssp_open
	ldr	r3, .L106+8
	str	r0, [r3, #0]
	mov	r0, r4
	bl	identify_the_device
	cmp	r4, #0
	bne	.L104
	ldr	r3, .L106+12
	ldr	r3, [r3, #0]
	cmp	r3, #1
	moveq	r2, #16
	ldreq	r3, .L106+16
	streq	r2, [r3, #0]
	ldreq	r0, .L106+20
	movne	r2, #32
	ldrne	r3, .L106+16
	strne	r2, [r3, #0]
	ldrne	r0, .L106+24
	bl	Alert_Message
.L104:
	mov	r0, r4
	bl	lock_flash_device
	add	sp, sp, #24
	ldmfd	sp!, {r4, pc}
.L107:
	.align	2
.L106:
	.word	24000000
	.word	.LANCHOR0
	.word	.LANCHOR4
	.word	.LANCHOR2
	.word	display_model_is
	.word	.LC10
	.word	.LC11
.LFE2:
	.size	init_FLASH_DRIVE_SSP_channel, .-init_FLASH_DRIVE_SSP_channel
	.section	.text.SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming,"ax",%progbits
	.align	2
	.global	SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming
	.type	SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming, %function
SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming:
.LFB7:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI13:
	sub	sp, sp, #24
.LCFI14:
	mov	r5, r0
	mov	r7, r1
	mov	r8, r2
	mov	r6, r3
	mov	r2, #76
	ldr	r3, .L137
	mla	r3, r2, r0, r3
	ldr	r4, [r3, #12]
	ldr	r3, .L137+4
	ldr	r3, [r3, #68]
	cmp	r3, #1
	mvneq	r4, #9
	beq	.L109
	cmp	r6, #0
	bne	.L110
	ldr	r0, .L137+8
	bl	Alert_Message
	mvn	r4, #10
	b	.L109
.L110:
	cmp	r6, #4096
	bls	.L111
	ldr	r0, .L137+12
	bl	Alert_Message
	mvn	r4, #11
	b	.L109
.L111:
	bl	unlock_and_write_enable_flash_device
	cmp	r0, #0
	beq	.L112
	ldr	r3, .L137+4
	ldr	r3, [r3, #68]
	cmp	r3, #0
	mvnne	r4, #9
	bne	.L113
	mov	sl, #1
	str	sl, [sp, #0]
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	str	r3, [sp, #12]
	str	r3, [sp, #16]
	mov	r0, r5
	mov	r1, sl
	mov	r2, #32
	mov	r3, r7
	bl	send_command_to_flash_device
	mov	r0, r5
	mov	r1, sl
	bl	wait_for_flash_device_to_be_idle
	ldr	r3, .L137+16
	ldr	r3, [r3, r5, asl #2]
	sub	r3, r3, #2
	cmp	r3, sl
	bls	.L114
	ldr	r3, .L137+4
	ldr	r3, [r3, #68]
	cmp	r3, #0
	movne	r4, #0
	beq	.L135
	b	.L113
.L136:
	mov	sl, r6
	mov	r9, #76
	ldr	r3, .L137
	mla	r9, r5, r9, r3
	ldr	fp, .L137+4
.L122:
	cmp	sl, #256
	movcc	r6, sl
	movcs	r6, #256
	rsb	sl, r6, sl
	mov	r3, #0
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	str	r3, [sp, #8]
	str	r3, [sp, #12]
	str	r3, [sp, #16]
	mov	r0, r5
	mov	r1, #1
	mov	r2, #6
	bl	send_command_to_flash_device
	mov	r3, #2
	str	r3, [r4, #8]
	and	r2, r7, #16711680
	mov	r2, r2, lsr #16
	str	r2, [r4, #8]
	and	r2, r7, #65280
	mov	r2, r2, lsr #8
	str	r2, [r4, #8]
	and	r2, r7, #255
	str	r2, [r4, #8]
	add	r7, r7, r6
	ldr	r1, [r9, #4]
	ldr	r2, .L137+20
	str	r1, [r2, #8]
	str	r3, [r4, #4]
	cmp	r6, #0
	beq	.L116
	mov	r1, r8
	mov	r2, r6
.L118:
	ldrb	r3, [r1], #1	@ zero_extendqisi2
	str	r3, [r4, #8]
	sub	r2, r2, #1
.L117:
	ldr	r3, [r4, #12]
	tst	r3, #16
	bne	.L117
	cmp	r2, #0
	bne	.L118
	add	r8, r8, r6
.L116:
	mov	r3, #0
	str	r3, [r4, #4]
	ldr	r2, [r9, #4]
	ldr	r3, .L137+20
	str	r2, [r3, #4]
	ldr	r3, [r4, #12]
	tst	r3, #4
	beq	.L119
.L129:
	ldr	r3, [r4, #8]
	str	r3, [sp, #20]
	ldr	r3, [r4, #12]
	tst	r3, #4
	bne	.L129
.L119:
	cmp	sl, #0
	bne	.L121
	mov	r4, #0
	b	.L113
.L114:
	ldr	r3, .L137+4
	ldr	r3, [r3, #68]
	cmp	r3, #0
	movne	r4, #0
	beq	.L136
	b	.L113
.L121:
	ldr	r3, [fp, #68]
	cmp	r3, #0
	beq	.L122
	mov	r4, #0
	b	.L113
.L135:
	mov	sl, r6
	mov	r4, #0
	mov	r6, #1
	ldr	r9, .L137+4
.L115:
	str	r4, [sp, #0]
	str	r4, [sp, #4]
	str	r4, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	mov	r0, r5
	mov	r1, r6
	mov	r2, #6
	mov	r3, r4
	bl	send_command_to_flash_device
	str	r6, [sp, #0]
	ldrb	r3, [r8], #1	@ zero_extendqisi2
	str	r3, [sp, #4]
	str	r6, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	mov	r0, r5
	mov	r1, r4
	mov	r2, #2
	mov	r3, r7
	bl	send_command_to_flash_device
	add	r7, r7, #1
	subs	sl, sl, #1
	beq	.L128
	ldr	r3, [r9, #68]
	cmp	r3, #0
	beq	.L115
	mov	r4, #0
	b	.L113
.L112:
	ldr	r0, .L137+24
	bl	Alert_Message
	mvn	r4, #12
	b	.L113
.L128:
	mov	r4, #0
.L113:
	mov	r0, r5
	bl	lock_flash_device
	mov	r0, r5
	bl	flash_device_is_write_enabled
	cmp	r0, #1
	bne	.L123
	ldr	r0, .L137+28
	bl	Alert_Message
.L123:
	ldr	r3, .L137+4
	ldr	r3, [r3, #68]
	cmp	r3, #0
	movne	r2, #1
	ldrne	r3, .L137+4
	strne	r2, [r3, #84]
.L109:
	mov	r0, r4
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
.L138:
	.align	2
.L137:
	.word	.LANCHOR0
	.word	restart_info
	.word	.LC12
	.word	.LC13
	.word	.LANCHOR2
	.word	1073905664
	.word	.LC14
	.word	.LC15
.LFE7:
	.size	SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming, .-SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_byte_programming
	.section	.text.SPI_FLASH_fast_read_as_much_as_needed_to_buffer,"ax",%progbits
	.align	2
	.global	SPI_FLASH_fast_read_as_much_as_needed_to_buffer
	.type	SPI_FLASH_fast_read_as_much_as_needed_to_buffer, %function
SPI_FLASH_fast_read_as_much_as_needed_to_buffer:
.LFB8:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI15:
	sub	sp, sp, #4
.LCFI16:
	mov	sl, r0
	mov	r8, r1
	mov	r7, r3
	mov	r6, r2
	mov	r4, r3
	ldr	r3, .L164
	ldr	r3, [r3, #68]
	cmp	r3, #1
	mvneq	r0, #9
	beq	.L140
	mov	r9, #76
	ldr	r3, .L164+4
	mla	r9, sl, r9, r3
	ldr	r5, [r9, #12]
	mov	r0, sl
	mov	r1, #0
	bl	wait_for_flash_device_to_be_idle
	mov	r3, #11
	str	r3, [r5, #8]
	and	r3, r8, #16711680
	mov	r3, r3, lsr #16
	str	r3, [r5, #8]
	and	r3, r8, #65280
	mov	r3, r3, lsr #8
	str	r3, [r5, #8]
	and	r8, r8, #255
	str	r8, [r5, #8]
	mov	r3, #0
	str	r3, [r5, #8]
	ldr	r2, [r9, #4]
	ldr	r3, .L164+8
	str	r2, [r3, #8]
	mov	r3, #2
	str	r3, [r5, #4]
.L141:
	ldr	r3, [r5, #12]
	tst	r3, #16
	bne	.L141
	ldr	r3, [r5, #12]
	tst	r3, #4
	beq	.L142
.L157:
	ldr	r3, [r5, #8]
	str	r3, [sp, #0]
	ldr	r3, [r5, #12]
	tst	r3, #4
	bne	.L157
	b	.L142
.L145:
	str	r1, [r5, #8]
	sub	r4, r4, #1
	add	r3, r3, #1
	cmp	r3, #8
	beq	.L156
.L152:
	cmp	r4, #0
	bne	.L145
.L156:
	ldr	r2, [r5, #12]
	tst	r2, #16
	bne	.L156
	cmp	r3, #0
	beq	.L147
	mov	ip, r6
	mov	r2, r3
.L148:
	ldr	r7, [r5, #8]
	strb	r7, [ip], #1
	subs	r2, r2, #1
	bne	.L148
	add	r6, r6, r3
.L147:
	cmp	r4, #0
	bne	.L155
	b	.L150
.L142:
	cmp	r7, #0
	beq	.L150
	mov	r1, #0
	mov	r0, #1
.L155:
	cmp	r4, #0
	moveq	r3, r1
	bne	.L163
	b	.L156
.L150:
	mov	r3, #0
	str	r3, [r5, #4]
	mov	r2, #76
	ldr	r3, .L164+4
	mla	sl, r2, sl, r3
	ldr	r2, [sl, #4]
	ldr	r3, .L164+8
	str	r2, [r3, #4]
	ldr	r3, .L164
	ldr	r3, [r3, #68]
	cmp	r3, #1
	moveq	r2, #1
	ldreq	r3, .L164
	streq	r2, [r3, #88]
	mov	r0, #0
.L140:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
.L163:
	str	r1, [r5, #8]
	sub	r4, r4, #1
	mov	r3, r0
	b	.L152
.L165:
	.align	2
.L164:
	.word	restart_info
	.word	.LANCHOR0
	.word	1073905664
.LFE8:
	.size	SPI_FLASH_fast_read_as_much_as_needed_to_buffer, .-SPI_FLASH_fast_read_as_much_as_needed_to_buffer
	.global	ssp_define
	.global	known_JEDEC_READ_ID_results
	.global	flash_1_mr_buffer
	.global	flash_0_mr_buffer
	.section .rodata
	.align	2
	.set	.LANCHOR3,. + 0
.LC0:
	.byte	85
	.byte	85
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.section	.bss.flash_0_mr_buffer,"aw",%nobits
	.align	2
	.type	flash_0_mr_buffer, %object
	.size	flash_0_mr_buffer, 164
flash_0_mr_buffer:
	.space	164
	.section	.bss.flash_1_mr_buffer,"aw",%nobits
	.align	2
	.type	flash_1_mr_buffer, %object
	.size	flash_1_mr_buffer, 164
flash_1_mr_buffer:
	.space	164
	.section	.bss.flash_device_id,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	flash_device_id, %object
	.size	flash_device_id, 8
flash_device_id:
	.space	8
	.section	.rodata.known_JEDEC_READ_ID_results,"a",%progbits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	known_JEDEC_READ_ID_results, %object
	.size	known_JEDEC_READ_ID_results, 48
known_JEDEC_READ_ID_results:
	.word	0
	.word	0
	.word	0
	.word	191
	.word	37
	.word	74
	.word	127
	.word	157
	.word	70
	.word	191
	.word	38
	.word	66
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC1:
	.ascii	"Flash part not recognized %d: %01X, %01X, %01X\000"
	.space	1
.LC2:
	.ascii	"FLASH: always busy! %u\000"
	.space	1
.LC3:
	.ascii	"SST26 WSE set!\000"
	.space	1
.LC4:
	.ascii	"SST26 WSP set!\000"
	.space	1
.LC5:
	.ascii	"SST26 WPLD set!\000"
.LC6:
	.ascii	"SST26 SEC set!\000"
	.space	1
.LC7:
	.ascii	"FLASH: not WEL\000"
	.space	1
.LC8:
	.ascii	"FLASH: blocks not unlocked.\000"
.LC9:
	.ascii	"FLASH: blocks not sucessfully locked.\000"
	.space	2
.LC10:
	.ascii	"Display ID: Kyocera\000"
.LC11:
	.ascii	"Display ID: AZ Display\000"
	.space	1
.LC12:
	.ascii	"FLASH: trying to write zero bytes!\000"
	.space	1
.LC13:
	.ascii	"FLASH: trying to program too many bytes!\000"
	.space	3
.LC14:
	.ascii	"FLASH: device wouldn't write enable.\000"
	.space	3
.LC15:
	.ascii	"Still WRITE enabled!\000"
	.section	.rodata.ssp_define,"a",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	ssp_define, %object
	.size	ssp_define, 152
ssp_define:
	.word	32
	.word	1073741824
	.word	33554432
	.word	537411584
	.word	FLASH_STORAGE_task_queue_0
	.word	Flash_0_MUTEX
	.word	flash_0_mr_buffer
	.word	14
	.word	15
	.word	29
	.word	30
	.ascii	"CALSENSE BOOTCODE FLASHFS_0"
	.space	1
	.word	15
	.word	131072
	.word	536870912
	.word	134217728
	.word	537444352
	.word	FLASH_STORAGE_task_queue_1
	.word	Flash_1_MUTEX
	.word	flash_1_mr_buffer
	.word	0
	.word	1
	.word	60
	.word	61
	.ascii	"CALSENSE ALLFILES FLASHFS_1"
	.space	1
	.word	60
	.section	.bss.sspid,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	sspid, %object
	.size	sspid, 4
sspid:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI6-.LFB4
	.byte	0xe
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI7-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI9-.LFB6
	.byte	0xe
	.uleb128 0x1c
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x88
	.uleb128 0x3
	.byte	0x87
	.uleb128 0x4
	.byte	0x86
	.uleb128 0x5
	.byte	0x85
	.uleb128 0x6
	.byte	0x84
	.uleb128 0x7
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x40
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI11-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x20
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI13-.LFB7
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI15-.LFB8
	.byte	0xe
	.uleb128 0x20
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xe
	.uleb128 0x24
	.align	2
.LEFDE16:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/flash_storage/spi_flash_driver.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xdb
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF9
	.byte	0x1
	.4byte	.LASF10
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x150
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x19d
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x243
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x282
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x2a4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x38b
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST5
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x203
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST6
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x45b
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x546
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST8
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI5
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI8
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB6
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 28
	.4byte	.LCFI10
	.4byte	.LFE6
	.2byte	0x3
	.byte	0x7d
	.sleb128 64
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB2
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI12
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	.LCFI14
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB8
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 32
	.4byte	.LCFI16
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x5c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"lock_flash_device\000"
.LASF3:
	.ascii	"flash_device_is_write_enabled\000"
.LASF2:
	.ascii	"send_command_to_flash_device\000"
.LASF1:
	.ascii	"wait_for_flash_device_to_be_idle\000"
.LASF10:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/flas"
	.ascii	"h_storage/spi_flash_driver.c\000"
.LASF9:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"identify_the_device\000"
.LASF4:
	.ascii	"unlock_and_write_enable_flash_device\000"
.LASF6:
	.ascii	"init_FLASH_DRIVE_SSP_channel\000"
.LASF8:
	.ascii	"SPI_FLASH_fast_read_as_much_as_needed_to_buffer\000"
.LASF7:
	.ascii	"SPI_FLASH_erase_4k_sector_and_write_up_to_4k_using_"
	.ascii	"byte_programming\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
