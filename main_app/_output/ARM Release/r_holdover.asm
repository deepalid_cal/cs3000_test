	.file	"r_holdover.c"
	.text
.Ltext0:
	.section	.text.FDTO_HOLDOVER_clear_values,"ax",%progbits
	.align	2
	.type	FDTO_HOLDOVER_clear_values, %function
FDTO_HOLDOVER_clear_values:
.LFB0:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r4, r5, r6, lr}
.LCFI0:
	mov	r5, r0
	bl	STATION_get_first_available_station
	mov	r4, #0
	mov	r6, r0
	b	.L2
.L3:
	bl	nm_STATION_get_box_index_0
	add	r4, r4, #1
	str	r0, [sp, #0]
	mov	r0, r6
	bl	nm_STATION_get_station_number_0
	add	r1, sp, #4
	add	r0, r0, #1
	str	r0, [sp, #4]
	mov	r0, sp
	bl	STATION_get_next_available_station
	mov	r6, r0
.L2:
	cmp	r4, r5
	mov	r0, r6
	bne	.L3
	bl	nm_STATION_get_box_index_0
	str	r0, [sp, #0]
	mov	r0, r6
	bl	nm_STATION_get_station_number_0
	mov	r2, #0
	mov	r3, r2
	mov	r1, r0
	str	r0, [sp, #4]
	ldr	r0, [sp, #0]
	bl	STATION_HISTORY_set_rain_min_10u
	ldmia	sp, {r0, r1}
	mov	r2, #2
	bl	STATION_set_ignore_moisture_balance_at_next_irrigation_by_station
	mov	r1, r4, asl #16
	mov	r1, r1, lsr #16
	mov	r0, #0
	bl	GuiLib_ScrollBox_RedrawLine
	bl	GuiLib_Refresh
	ldmfd	sp!, {r2, r3, r4, r5, r6, pc}
.LFE0:
	.size	FDTO_HOLDOVER_clear_values, .-FDTO_HOLDOVER_clear_values
	.section	.text.nm_HOLDOVER_load_guivars_for_scroll_line,"ax",%progbits
	.align	2
	.type	nm_HOLDOVER_load_guivars_for_scroll_line, %function
nm_HOLDOVER_load_guivars_for_scroll_line:
.LFB1:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI1:
	fstmfdd	sp!, {d8}
.LCFI2:
	ldr	r3, .L12+4
	sub	sp, sp, #36
.LCFI3:
	str	r3, [sp, #16]	@ float
	ldr	r3, .L12+8
	mov	r0, r0, asl #16
	str	r3, [sp, #20]	@ float
	ldr	r3, .L12+12
	mov	r6, r0, asr #16
	str	r3, [sp, #24]	@ float
	bl	STATION_get_first_available_station
	mov	r5, #0
	mov	r4, r0
	b	.L5
.L6:
	bl	nm_STATION_get_box_index_0
	add	r5, r5, #1
	str	r0, [sp, #28]
	mov	r0, r4
	bl	nm_STATION_get_station_number_0
	add	r1, sp, #32
	add	r0, r0, #1
	str	r0, [sp, #32]
	add	r0, sp, #28
	bl	STATION_get_next_available_station
	mov	r4, r0
.L5:
	cmp	r5, r6
	mov	r0, r4
	bcc	.L6
	bl	nm_STATION_get_box_index_0
	str	r0, [sp, #28]
	mov	r0, r4
	bl	nm_STATION_get_station_number_0
	ldr	r3, .L12+16
	mov	r2, sp
	mov	r1, r0
	str	r0, [sp, #32]
	ldr	r0, [sp, #28]
	str	r0, [r3, #0]
	mov	r3, #16
	bl	STATION_get_station_number_string
	mov	r1, #4
	ldr	r2, .L12+20
	mov	r3, r0
	ldr	r0, .L12+24
	bl	snprintf
	mov	r0, r4
	bl	WEATHER_get_station_uses_daily_et
	cmp	r0, #1
	bne	.L7
	mov	r0, r4
	bl	STATION_get_moisture_balance
	fmsr	s16, r0
	mov	r0, r4
	bl	STATION_GROUP_get_soil_storage_capacity_inches_100u
	flds	s15, [sp, #20]
	fmsr	s12, r0	@ int
	fuitos	s14, s12
	fdivs	s14, s14, s15
	flds	s15, .L12
	fnmacs	s16, s14, s15
	fcmpezs	s16
	fmstat
	ble	.L8
	mov	r0, r4
	bl	nm_STATION_get_ignore_moisture_balance_at_next_irrigation_flag
	cmp	r0, #0
	bne	.L8
	mov	r0, r4
	bl	STATION_GROUP_get_station_precip_rate_in_per_hr_100000u
	flds	s15, [sp, #16]
	flds	s14, [sp, #24]
	ldr	r3, .L12+28
	fmsr	s12, r0	@ int
	fuitos	s13, s12
	fdivs	s15, s13, s15
	fdivs	s15, s15, s14
	fdivs	s16, s16, s15
	fsts	s16, [r3, #0]
	b	.L4
.L8:
	ldr	r3, .L12+28
	mov	r2, #0
	str	r2, [r3, #0]	@ float
	b	.L4
.L7:
	ldr	r0, [sp, #28]
	ldr	r1, [sp, #32]
	bl	STATION_HISTORY_get_rain_min
	ldr	r3, .L12+28
	str	r0, [r3, #0]	@ float
.L4:
	add	sp, sp, #36
	fldmfdd	sp!, {d8}
	ldmfd	sp!, {r4, r5, r6, pc}
.L13:
	.align	2
.L12:
	.word	1056964608
	.word	1203982336
	.word	1120403456
	.word	1114636288
	.word	GuiVar_RptController
	.word	.LC0
	.word	GuiVar_RptStation
	.word	GuiVar_RptRainMin
.LFE1:
	.size	nm_HOLDOVER_load_guivars_for_scroll_line, .-nm_HOLDOVER_load_guivars_for_scroll_line
	.section	.text.FDTO_HOLDOVER_draw_report,"ax",%progbits
	.align	2
	.global	FDTO_HOLDOVER_draw_report
	.type	FDTO_HOLDOVER_draw_report, %function
FDTO_HOLDOVER_draw_report:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI4:
	ldr	r4, .L16
	mov	r5, r0
	mov	r2, #1
	mov	r0, #84
	mvn	r1, #0
	bl	GuiLib_ShowScreen
	cmp	r5, #1
	ldreq	r3, .L16+4
	moveq	r2, #0
	mov	r1, #400
	ldr	r0, [r4, #0]
	streqb	r2, [r3, #0]
	mov	r3, #177
	ldr	r2, .L16+8
	bl	xQueueTakeMutexRecursive_debug
	bl	STATION_get_num_stations_in_use
	ldr	r2, .L16+12
	mov	r1, r0
	mov	r0, r5
	bl	FDTO_REPORTS_draw_report
	ldr	r0, [r4, #0]
	ldmfd	sp!, {r4, r5, lr}
	b	xQueueGiveMutexRecursive
.L17:
	.align	2
.L16:
	.word	list_program_data_recursive_MUTEX
	.word	GuiVar_StationDescription
	.word	.LC1
	.word	nm_HOLDOVER_load_guivars_for_scroll_line
.LFE2:
	.size	FDTO_HOLDOVER_draw_report, .-FDTO_HOLDOVER_draw_report
	.section	.text.HOLDOVER_process_report,"ax",%progbits
	.align	2
	.global	HOLDOVER_process_report
	.type	HOLDOVER_process_report, %function
HOLDOVER_process_report:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	stmfd	sp!, {r4, lr}
.LCFI5:
	mov	r4, r0
	sub	sp, sp, #36
.LCFI6:
	bne	.L22
	bl	good_key_beep
	ldr	r3, .L23
	mov	r0, #0
	mov	r1, r0
	str	r4, [sp, #0]
	str	r3, [sp, #20]
	bl	GuiLib_ScrollBox_GetActiveLine
	str	r0, [sp, #24]
	mov	r0, sp
	bl	Display_Post_Command
	b	.L18
.L22:
	mov	r2, #0
	mov	r3, r2
	bl	REPORTS_process_report
.L18:
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L24:
	.align	2
.L23:
	.word	FDTO_HOLDOVER_clear_values
.LFE3:
	.size	HOLDOVER_process_report, .-HOLDOVER_process_report
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"%s\000"
.LC1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_holdover.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x81
	.uleb128 0x5
	.byte	0x80
	.uleb128 0x6
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xe
	.uleb128 0x18
	.byte	0x5
	.uleb128 0x50
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x3c
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI4-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI5-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/r_holdover.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x1f
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x5c
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xa8
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xb9
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI3
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 60
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI6
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"nm_HOLDOVER_load_guivars_for_scroll_line\000"
.LASF2:
	.ascii	"FDTO_HOLDOVER_draw_report\000"
.LASF0:
	.ascii	"FDTO_HOLDOVER_clear_values\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/r_holdover.c\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF3:
	.ascii	"HOLDOVER_process_report\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
