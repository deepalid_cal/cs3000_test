	.file	"e_budget_reduction_limits.c"
	.text
.Ltext0:
	.section	.text.REDUCTION_LIMIT_process_percentage,"ax",%progbits
	.align	2
	.type	REDUCTION_LIMIT_process_percentage, %function
REDUCTION_LIMIT_process_percentage:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #72
	stmfd	sp!, {r0, r1, lr}
.LCFI0:
	movhi	r1, #4
	bhi	.L2
	cmp	r1, #48
	movls	r1, #1
	movhi	r1, #2
.L2:
	cmp	r0, #80
	str	r1, [sp, #0]
	mov	r3, #0
	mov	r1, r2
	movne	r0, #80
	moveq	r0, #84
	mvn	r2, #99
	str	r3, [sp, #4]
	bl	process_int32
	add	sp, sp, #8
	ldr	lr, [sp], #4
	b	Refresh_Screen
.LFE0:
	.size	REDUCTION_LIMIT_process_percentage, .-REDUCTION_LIMIT_process_percentage
	.section	.text.FDTO_BUDGET_REDUCTION_LIMITS_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_BUDGET_REDUCTION_LIMITS_draw_screen
	.type	FDTO_BUDGET_REDUCTION_LIMITS_draw_screen, %function
FDTO_BUDGET_REDUCTION_LIMITS_draw_screen:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L10
	str	lr, [sp, #-4]!
.LCFI1:
	ldrnesh	r1, [r3, #0]
	bne	.L9
	bl	BUDGET_REDUCTION_LIMITS_copy_group_into_guivars
	mov	r1, #0
.L9:
	mov	r0, #11
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L11:
	.align	2
.L10:
	.word	GuiLib_ActiveCursorFieldNo
.LFE1:
	.size	FDTO_BUDGET_REDUCTION_LIMITS_draw_screen, .-FDTO_BUDGET_REDUCTION_LIMITS_draw_screen
	.global	__modsi3
	.section	.text.BUDGET_REDUCTION_LIMIT_process_screen,"ax",%progbits
	.align	2
	.global	BUDGET_REDUCTION_LIMIT_process_screen
	.type	BUDGET_REDUCTION_LIMIT_process_screen, %function
BUDGET_REDUCTION_LIMIT_process_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #16
	stmfd	sp!, {r0, r4, lr}
.LCFI2:
	mov	r4, r0
	beq	.L17
	bhi	.L20
	cmp	r0, #1
	beq	.L15
	bcc	.L14
	cmp	r0, #3
	beq	.L16
	cmp	r0, #4
	bne	.L13
	b	.L17
.L20:
	cmp	r0, #67
	beq	.L18
	bhi	.L21
	cmp	r0, #20
	bne	.L13
	b	.L14
.L21:
	cmp	r0, #80
	beq	.L19
	cmp	r0, #84
	bne	.L13
.L19:
	ldr	r3, .L40
	ldrsh	r3, [r3, #0]
	cmp	r3, #9
	ldrls	pc, [pc, r3, asl #2]
	b	.L37
.L33:
	.word	.L23
	.word	.L24
	.word	.L25
	.word	.L26
	.word	.L27
	.word	.L28
	.word	.L29
	.word	.L30
	.word	.L31
	.word	.L32
.L23:
	mov	r0, r4
	ldr	r2, .L40+4
	b	.L38
.L24:
	ldr	r2, .L40+8
	mov	r0, r4
.L38:
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	REDUCTION_LIMIT_process_percentage
.L25:
	mov	r0, r4
	ldr	r2, .L40+12
	b	.L38
.L26:
	mov	r0, r4
	ldr	r2, .L40+16
	b	.L38
.L27:
	mov	r0, r4
	ldr	r2, .L40+20
	b	.L38
.L28:
	mov	r0, r4
	ldr	r2, .L40+24
	b	.L38
.L29:
	mov	r0, r4
	ldr	r2, .L40+28
	b	.L38
.L30:
	mov	r0, r4
	ldr	r2, .L40+32
	b	.L38
.L31:
	mov	r0, r4
	ldr	r2, .L40+36
	b	.L38
.L32:
	mov	r0, r4
	ldr	r2, .L40+40
	b	.L38
.L17:
	ldr	r3, .L40
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	movs	r0, r0, asl #16
	beq	.L37
.L34:
	mov	r0, #1
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Up
.L14:
	ldr	r3, .L40
	mov	r1, #10
	ldrsh	r0, [r3, #0]
	bl	__modsi3
	mov	r0, r0, asl #16
	mov	r4, r0, asr #16
	bl	STATION_GROUP_get_num_groups_in_use
	sub	r0, r0, #1
	cmp	r4, r0
	beq	.L37
.L35:
	mov	r0, #1
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Down
.L15:
	ldr	r3, .L40
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	cmp	r0, #589824
	movls	r0, r0, asr #16
	addls	r0, r0, #9
	movls	r1, r4
	bls	.L39
	b	.L37
.L16:
	ldr	r3, .L40
	ldrh	r0, [r3, #0]
	mov	r0, r0, asl #16
	cmp	r0, #589824
	bhi	.L37
	mov	r0, r0, asr #16
	add	r0, r0, #10
	mov	r1, #1
.L39:
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	CURSOR_Select
.L37:
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	bad_key_beep
.L18:
	ldr	r3, .L40+44
	mov	r2, #5
	str	r2, [r3, #0]
	str	r1, [sp, #0]
	bl	BUDGET_REDUCTION_LIMITS_extract_and_store_changes_from_GuiVars
	ldr	r1, [sp, #0]
.L13:
	mov	r0, r4
	add	sp, sp, #4
	ldmfd	sp!, {r4, lr}
	b	KEY_process_global_keys
.L41:
	.align	2
.L40:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_BudgetReductionLimit_0
	.word	GuiVar_BudgetReductionLimit_1
	.word	GuiVar_BudgetReductionLimit_2
	.word	GuiVar_BudgetReductionLimit_3
	.word	GuiVar_BudgetReductionLimit_4
	.word	GuiVar_BudgetReductionLimit_5
	.word	GuiVar_BudgetReductionLimit_6
	.word	GuiVar_BudgetReductionLimit_7
	.word	GuiVar_BudgetReductionLimit_8
	.word	GuiVar_BudgetReductionLimit_9
	.word	GuiVar_MenuScreenToShow
.LFE2:
	.size	BUDGET_REDUCTION_LIMIT_process_screen, .-BUDGET_REDUCTION_LIMIT_process_screen
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x81
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI1-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x3
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_budget_reduction_limits.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x59
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF2
	.byte	0x1
	.4byte	.LASF3
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF4
	.byte	0x1
	.byte	0x37
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x57
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x6b
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"FDTO_BUDGET_REDUCTION_LIMITS_draw_screen\000"
.LASF1:
	.ascii	"BUDGET_REDUCTION_LIMIT_process_screen\000"
.LASF3:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_budget_reduction_limits.c\000"
.LASF2:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF4:
	.ascii	"REDUCTION_LIMIT_process_percentage\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
