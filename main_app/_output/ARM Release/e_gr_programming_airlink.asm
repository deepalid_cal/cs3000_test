	.file	"e_gr_programming_airlink.c"
	.text
.Ltext0:
	.section	.text.FDTO_GR_PROGRAMMING_AIRLINK_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_GR_PROGRAMMING_AIRLINK_draw_screen
	.type	FDTO_GR_PROGRAMMING_AIRLINK_draw_screen, %function
FDTO_GR_PROGRAMMING_AIRLINK_draw_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L5
	stmfd	sp!, {r4, r5, lr}
.LCFI0:
	mov	r5, r1
	sub	sp, sp, #40
.LCFI1:
	mov	r4, r0
	ldrnesh	r1, [r3, #0]
	bne	.L3
.LBB10:
	ldr	r3, .L5+4
	ldr	r2, .L5+8
	ldr	r1, .L5+12
	str	r2, [r3, #0]
	ldr	r0, .L5+16
	mov	r2, #49
	bl	strlcpy
	ldr	r1, .L5+20
	mov	r2, #49
	ldr	r0, .L5+24
	bl	strlcpy
	ldr	r1, .L5+28
	mov	r2, #49
	ldr	r0, .L5+32
	bl	strlcpy
	ldr	r1, .L5+36
	mov	r2, #49
	ldr	r0, .L5+40
	bl	strlcpy
	ldr	r1, .L5+44
	mov	r2, #49
	ldr	r0, .L5+48
	bl	strlcpy
	ldr	r1, .L5+52
	mov	r2, #49
	ldr	r0, .L5+56
	bl	strlcpy
	ldr	r1, .L5+60
	mov	r2, #49
	ldr	r0, .L5+64
	bl	strlcpy
	ldr	r1, .L5+60
	mov	r2, #49
	ldr	r0, .L5+68
	bl	strlcpy
	ldr	r1, .L5+60
	mov	r2, #49
	ldr	r0, .L5+72
	bl	strlcpy
	ldr	r1, .L5+60
	mov	r2, #49
	ldr	r0, .L5+76
	bl	strlcpy
	ldr	r1, .L5+80
	mov	r2, #49
	ldr	r0, .L5+84
	bl	strlcpy
	ldr	r1, .L5+88
	ldr	r0, .L5+92
	mov	r2, #49
	bl	strlcpy
.LBE10:
	ldr	r3, .L5+96
	mov	r1, #0
	str	r5, [r3, #0]
.L3:
	mov	r0, #23
	mov	r2, #1
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
	cmp	r4, #1
	bne	.L1
.LBB11:
	bl	DEVICE_EXCHANGE_draw_dialog
.LBB12:
	ldr	r3, .L5+100
	mov	r0, sp
	str	r4, [r3, #0]
	mov	r3, #4352
	str	r3, [sp, #0]
	str	r5, [sp, #32]
	bl	COMM_MNGR_post_event_with_details
.L1:
.LBE12:
.LBE11:
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, pc}
.L6:
	.align	2
.L5:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36865
	.word	.LC0
	.word	GuiVar_GRMake
	.word	.LC1
	.word	GuiVar_GRModel
	.word	.LC2
	.word	GuiVar_GRPhoneNumber
	.word	.LC3
	.word	GuiVar_GRIPAddress
	.word	.LC4
	.word	GuiVar_GRSIMID
	.word	.LC5
	.word	GuiVar_GRNetworkState
	.word	.LC6
	.word	GuiVar_GRCarrier
	.word	GuiVar_GRService
	.word	GuiVar_GRRSSI
	.word	GuiVar_GRECIO
	.word	.LC7
	.word	GuiVar_GRRadioSerialNum
	.word	.LC8
	.word	GuiVar_GRFirmwareVersion
	.word	.LANCHOR0
	.word	.LANCHOR1
.LFE3:
	.size	FDTO_GR_PROGRAMMING_AIRLINK_draw_screen, .-FDTO_GR_PROGRAMMING_AIRLINK_draw_screen
	.section	.text.FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key, %function
FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	r3, r0, #36864
	cmp	r3, #6
	str	lr, [sp, #-4]!
.LCFI2:
	ldrhi	pc, [sp], #4
	mov	r2, #1
	mov	r3, r2, asl r3
	tst	r3, #109
	bne	.L9
	tst	r3, #18
	ldreq	pc, [sp], #4
	ldr	r0, .L11
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L11+4
	str	r0, [r3, #0]
	ldr	lr, [sp], #4
	b	DEVICE_EXCHANGE_draw_dialog
.L9:
.LBB15:
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L11+4
	str	r0, [r3, #0]
	bl	DIALOG_close_ok_dialog
	ldr	r3, .L11+8
	mov	r0, #0
	ldr	r1, [r3, #0]
.LBE15:
	ldr	lr, [sp], #4
.LBB16:
	b	FDTO_GR_PROGRAMMING_AIRLINK_draw_screen
.L12:
	.align	2
.L11:
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	.LANCHOR0
.LBE16:
.LFE2:
	.size	FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key, .-FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key
	.section	.text.GR_PROGRAMMING_AIRLINK_process_screen,"ax",%progbits
	.align	2
	.global	GR_PROGRAMMING_AIRLINK_process_screen
	.type	GR_PROGRAMMING_AIRLINK_process_screen, %function
GR_PROGRAMMING_AIRLINK_process_screen:
.LFB4:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	str	lr, [sp, #-4]!
.LCFI3:
	mov	r3, r0
	sub	sp, sp, #36
.LCFI4:
	mov	r2, r1
	beq	.L16
	bhi	.L18
	cmp	r0, #2
	bne	.L14
	b	.L26
.L18:
	sub	r1, r0, #36864
	cmp	r1, #6
	bhi	.L14
	ldr	r2, .L27
	ldr	r1, .L27+4
	str	r0, [sp, #24]
	cmp	r0, r1
	cmpne	r0, r2
	ldrne	r2, .L27+8
	movne	r1, #0
	strne	r1, [r2, #0]
	mov	r2, #2
	str	r2, [sp, #0]
	ldr	r2, .L27+12
	str	r2, [sp, #20]
	b	.L25
.L26:
	ldr	r3, .L27+16
	ldrsh	r3, [r3, #0]
	cmp	r3, #0
	bne	.L24
	ldr	r3, .L27+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L24
	bl	good_key_beep
	mov	r3, #3
	str	r3, [sp, #0]
	ldr	r3, .L27+20
	str	r3, [sp, #20]
	mov	r3, #1
	str	r3, [sp, #24]
	ldr	r3, .L27+24
	ldr	r3, [r3, #0]
	str	r3, [sp, #28]
.L25:
	mov	r0, sp
	bl	Display_Post_Command
	b	.L13
.L24:
	bl	bad_key_beep
	b	.L13
.L16:
	ldr	r3, .L27+28
	mov	r2, #11
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	b	.L13
.L14:
	mov	r0, r3
	mov	r1, r2
	bl	KEY_process_global_keys
.L13:
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L28:
	.align	2
.L27:
	.word	36867
	.word	36870
	.word	.LANCHOR1
	.word	FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange_key
	.word	GuiLib_ActiveCursorFieldNo
	.word	FDTO_GR_PROGRAMMING_AIRLINK_draw_screen
	.word	.LANCHOR0
	.word	GuiVar_MenuScreenToShow
.LFE4:
	.size	GR_PROGRAMMING_AIRLINK_process_screen, .-GR_PROGRAMMING_AIRLINK_process_screen
	.section	.bss.GR_PROGRAMMING_AIRLINK_querying_device,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	GR_PROGRAMMING_AIRLINK_querying_device, %object
	.size	GR_PROGRAMMING_AIRLINK_querying_device, 4
GR_PROGRAMMING_AIRLINK_querying_device:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Sierra Wireless AirLink\000"
.LC1:
	.ascii	"-----\000"
.LC2:
	.ascii	"-----------\000"
.LC3:
	.ascii	"---.---.---.---\000"
.LC4:
	.ascii	"--------------------\000"
.LC5:
	.ascii	"-------------\000"
.LC6:
	.ascii	"----\000"
.LC7:
	.ascii	"---------------\000"
.LC8:
	.ascii	"---------\000"
	.section	.bss.g_GR_PROGRAMMING_AIRLINK_port,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_GR_PROGRAMMING_AIRLINK_port, %object
	.size	g_GR_PROGRAMMING_AIRLINK_port, 4
g_GR_PROGRAMMING_AIRLINK_port:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI0-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI3-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE4:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_gr_programming_airlink.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x75
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF3
	.byte	0x1
	.4byte	.LASF4
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x30
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0x97
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x77
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0x41
	.byte	0x1
	.uleb128 0x4
	.4byte	0x29
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	0x32
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xb9
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB3
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI1
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"FDTO_GR_PROGRAMMING_AIRLINK_draw_screen\000"
.LASF2:
	.ascii	"GR_PROGRAMMING_AIRLINK_initialize_guivars\000"
.LASF0:
	.ascii	"start_gr_device_inquiry\000"
.LASF6:
	.ascii	"GR_PROGRAMMING_AIRLINK_process_screen\000"
.LASF3:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"FDTO_GR_PROGRAMMING_AIRLINK_process_device_exchange"
	.ascii	"_key\000"
.LASF4:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_gr_programming_airlink.c\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
