	.file	"dialog.c"
	.text
.Ltext0:
	.section	.text.DIALOG_close_yes_no_cancel_dialog,"ax",%progbits
	.align	2
	.type	DIALOG_close_yes_no_cancel_dialog, %function
DIALOG_close_yes_no_cancel_dialog:
.LFB9:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L2
	mov	r0, #0
	str	r0, [r3, #0]
	ldr	r3, .L2+4
	ldr	r2, [r3, #0]
	ldr	r3, .L2+8
	strh	r2, [r3, #0]	@ movhi
	b	Redraw_Screen
.L3:
	.align	2
.L2:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	GuiLib_ActiveCursorFieldNo
.LFE9:
	.size	DIALOG_close_yes_no_cancel_dialog, .-DIALOG_close_yes_no_cancel_dialog
	.section	.text.DIALOG_close_yes_no_dialog,"ax",%progbits
	.align	2
	.type	DIALOG_close_yes_no_dialog, %function
DIALOG_close_yes_no_dialog:
.LFB14:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L5
	mov	r0, #0
	str	r0, [r3, #0]
	ldr	r3, .L5+4
	ldr	r2, [r3, #0]
	ldr	r3, .L5+8
	strh	r2, [r3, #0]	@ movhi
	b	Redraw_Screen
.L6:
	.align	2
.L5:
	.word	.LANCHOR2
	.word	.LANCHOR1
	.word	GuiLib_ActiveCursorFieldNo
.LFE14:
	.size	DIALOG_close_yes_no_dialog, .-DIALOG_close_yes_no_dialog
	.section	.text.FDTO_DIALOG_redraw_yes_no_dialog,"ax",%progbits
	.align	2
	.type	FDTO_DIALOG_redraw_yes_no_dialog, %function
FDTO_DIALOG_redraw_yes_no_dialog:
.LFB11:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L8
	str	lr, [sp, #-4]!
.LCFI0:
	ldrh	r0, [r3, #0]
	mov	r1, #97
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L9:
	.align	2
.L8:
	.word	GuiLib_CurStructureNdx
.LFE11:
	.size	FDTO_DIALOG_redraw_yes_no_dialog, .-FDTO_DIALOG_redraw_yes_no_dialog
	.section	.text.FDTO_DIALOG_redraw_yes_no_cancel_dialog,"ax",%progbits
	.align	2
	.type	FDTO_DIALOG_redraw_yes_no_cancel_dialog, %function
FDTO_DIALOG_redraw_yes_no_cancel_dialog:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L11
	str	lr, [sp, #-4]!
.LCFI1:
	ldrh	r0, [r3, #0]
	mov	r1, #97
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L12:
	.align	2
.L11:
	.word	GuiLib_CurStructureNdx
.LFE6:
	.size	FDTO_DIALOG_redraw_yes_no_cancel_dialog, .-FDTO_DIALOG_redraw_yes_no_cancel_dialog
	.section	.text.FDTO_DIALOG_redraw_ok_dialog,"ax",%progbits
	.align	2
	.global	FDTO_DIALOG_redraw_ok_dialog
	.type	FDTO_DIALOG_redraw_ok_dialog, %function
FDTO_DIALOG_redraw_ok_dialog:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L14
	str	lr, [sp, #-4]!
.LCFI2:
	ldrh	r0, [r3, #0]
	mov	r1, #99
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L15:
	.align	2
.L14:
	.word	GuiLib_CurStructureNdx
.LFE1:
	.size	FDTO_DIALOG_redraw_ok_dialog, .-FDTO_DIALOG_redraw_ok_dialog
	.section	.text.DIALOG_a_dialog_is_visible,"ax",%progbits
	.align	2
	.global	DIALOG_a_dialog_is_visible
	.type	DIALOG_a_dialog_is_visible, %function
DIALOG_a_dialog_is_visible:
.LFB0:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	movne	r0, #1
	bxne	lr
	ldr	r3, .L20+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L19
	ldr	r3, .L20+8
	ldr	r0, [r3, #0]
	adds	r0, r0, #0
	movne	r0, #1
	bx	lr
.L19:
	mov	r0, #1
	bx	lr
.L21:
	.align	2
.L20:
	.word	.LANCHOR3
	.word	.LANCHOR0
	.word	.LANCHOR2
.LFE0:
	.size	DIALOG_a_dialog_is_visible, .-DIALOG_a_dialog_is_visible
	.section	.text.DIALOG_draw_ok_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_draw_ok_dialog
	.type	DIALOG_draw_ok_dialog, %function
DIALOG_draw_ok_dialog:
.LFB3:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI3:
	mov	r3, #2
	sub	sp, sp, #36
.LCFI4:
	str	r3, [sp, #0]
	ldr	r3, .L23
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L24:
	.align	2
.L23:
	.word	FDTO_DIALOG_draw_ok_dialog
.LFE3:
	.size	DIALOG_draw_ok_dialog, .-DIALOG_draw_ok_dialog
	.section	.text.DIALOG_close_ok_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_close_ok_dialog
	.type	DIALOG_close_ok_dialog, %function
DIALOG_close_ok_dialog:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L26
	mov	r0, #0
	str	r0, [r3, #0]
	ldr	r3, .L26+4
	ldr	r2, [r3, #0]
	ldr	r3, .L26+8
	strh	r2, [r3, #0]	@ movhi
	b	Redraw_Screen
.L27:
	.align	2
.L26:
	.word	.LANCHOR3
	.word	.LANCHOR1
	.word	GuiLib_ActiveCursorFieldNo
.LFE4:
	.size	DIALOG_close_ok_dialog, .-DIALOG_close_ok_dialog
	.section	.text.DIALOG_process_ok_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_process_ok_dialog
	.type	DIALOG_process_ok_dialog, %function
DIALOG_process_ok_dialog:
.LFB5:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #83
	str	lr, [sp, #-4]!
.LCFI5:
	mov	r3, r0
	sub	sp, sp, #36
.LCFI6:
	mov	r2, r1
	beq	.L31
	bhi	.L33
	cmp	r0, #2
	beq	.L30
	cmp	r0, #67
	bne	.L29
	b	.L30
.L33:
	sub	r3, r0, #36864
	cmp	r3, #6
	bhi	.L29
	b	.L42
.L31:
	bl	KEY_process_ENG_SPA
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L43
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	b	.L28
.L30:
	ldr	r1, .L43+4
	ldr	r0, [r1, #0]
	cmp	r0, #0
	bne	.L29
.L35:
	ldr	r1, .L43+8
	ldr	ip, .L43+12
	ldrsh	r1, [r1, #0]
	cmp	r1, ip
	bne	.L36
	ldr	r3, .L43+16
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L37
	b	.L29
.L36:
	ldr	ip, .L43+20
	cmp	r1, ip
	bne	.L38
	mov	r1, r2
	mov	r0, r3
	ldr	r2, .L43+24
	bl	FDTO_TWO_WIRE_close_discovery_dialog
	b	.L28
.L38:
	ldr	ip, .L43+28
	cmp	r1, ip
	bne	.L39
	mov	r0, r3
	bl	FDTO_DEVICE_EXCHANGE_close_dialog
	b	.L28
.L39:
	ldr	ip, .L43+32
	cmp	r1, ip
	bne	.L37
	cmp	r3, #2
	beq	.L29
.L40:
	ldr	r1, .L43+36
	mov	ip, #1
	str	ip, [r1, #0]
	ldr	r1, .L43+24
	mov	ip, #36
	str	r0, [r1, #0]
	ldr	r1, .L43+40
	mov	r0, #26
	strh	r0, [r1, #0]	@ movhi
	ldr	r1, .L43+44
	ldr	r0, .L43+48
	ldr	r1, [r1, #0]
	mla	r1, ip, r1, r0
	ldr	r0, [r1, #4]
	ldr	r1, .L43+52
	str	r0, [r1, #0]
	mov	r0, r3
	mov	r1, r2
	bl	KEY_process_global_keys
	b	.L28
.L37:
	bl	good_key_beep
	ldr	r3, .L43+36
	mov	r2, #1
	str	r2, [r3, #0]
	bl	DIALOG_close_ok_dialog
	b	.L28
.L42:
	ldr	r3, .L43+44
	ldr	r2, .L43+48
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r3, [r3, #16]
	blx	r3
	b	.L28
.L29:
	bl	bad_key_beep
.L28:
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L44:
	.align	2
.L43:
	.word	FDTO_DIALOG_redraw_ok_dialog
	.word	GuiVar_CodeDownloadState
	.word	GuiLib_CurStructureNdx
	.word	635
	.word	GuiVar_StopKeyPending
	.word	599
	.word	.LANCHOR3
	.word	598
	.word	603
	.word	.LANCHOR4
	.word	GuiLib_ActiveCursorFieldNo
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE5:
	.size	DIALOG_process_ok_dialog, .-DIALOG_process_ok_dialog
	.section	.text.DIALOG_draw_yes_no_cancel_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_draw_yes_no_cancel_dialog
	.type	DIALOG_draw_yes_no_cancel_dialog, %function
DIALOG_draw_yes_no_cancel_dialog:
.LFB8:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI7:
	mov	r3, #2
	sub	sp, sp, #36
.LCFI8:
	str	r3, [sp, #0]
	ldr	r3, .L46
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L47:
	.align	2
.L46:
	.word	FDTO_DIALOG_draw_yes_no_cancel_dialog
.LFE8:
	.size	DIALOG_draw_yes_no_cancel_dialog, .-DIALOG_draw_yes_no_cancel_dialog
	.section	.text.DIALOG_process_yes_no_cancel_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_process_yes_no_cancel_dialog
	.type	DIALOG_process_yes_no_cancel_dialog, %function
DIALOG_process_yes_no_cancel_dialog:
.LFB10:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #3
	stmfd	sp!, {r4, lr}
.LCFI9:
	mov	r3, r0
	sub	sp, sp, #36
.LCFI10:
	mov	r4, r2
	beq	.L52
	bhi	.L55
	cmp	r0, #1
	beq	.L50
	cmp	r0, #2
	bne	.L49
	b	.L67
.L55:
	cmp	r0, #67
	beq	.L53
	cmp	r0, #83
	bne	.L49
	bl	KEY_process_ENG_SPA
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L68
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	b	.L48
.L67:
	ldr	r2, .L68+4
	ldrsh	r1, [r2, #0]
	ldr	r2, .L68+8
	cmp	r1, #98
	beq	.L59
	cmp	r1, #99
	beq	.L60
	cmp	r1, #97
	moveq	r3, #6
	bne	.L57
	b	.L60
.L59:
	mov	r3, #7
.L60:
	str	r3, [r2, #0]
.L57:
	ldr	r3, [r2, #0]
	cmp	r3, #0
	beq	.L49
	bl	good_key_beep
	b	.L66
.L53:
	bl	good_key_beep
	ldr	r3, .L68+8
	mov	r2, #2
	str	r2, [r3, #0]
.L66:
	cmp	r4, #0
	beq	.L63
	blx	r4
.L63:
	bl	DIALOG_close_yes_no_cancel_dialog
	b	.L48
.L50:
	bl	CURSOR_Up
	b	.L48
.L52:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L48
.L49:
	bl	bad_key_beep
.L48:
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L69:
	.align	2
.L68:
	.word	FDTO_DIALOG_redraw_yes_no_cancel_dialog
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR4
.LFE10:
	.size	DIALOG_process_yes_no_cancel_dialog, .-DIALOG_process_yes_no_cancel_dialog
	.section	.text.DIALOG_draw_yes_no_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_draw_yes_no_dialog
	.type	DIALOG_draw_yes_no_dialog, %function
DIALOG_draw_yes_no_dialog:
.LFB13:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
.LCFI11:
	mov	r3, #2
	sub	sp, sp, #36
.LCFI12:
	str	r3, [sp, #0]
	ldr	r3, .L71
	str	r0, [sp, #24]
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {pc}
.L72:
	.align	2
.L71:
	.word	FDTO_DIALOG_draw_yes_no_dialog
.LFE13:
	.size	DIALOG_draw_yes_no_dialog, .-DIALOG_draw_yes_no_dialog
	.section	.text.DIALOG_process_yes_no_dialog,"ax",%progbits
	.align	2
	.global	DIALOG_process_yes_no_dialog
	.type	DIALOG_process_yes_no_dialog, %function
DIALOG_process_yes_no_dialog:
.LFB15:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #3
	stmfd	sp!, {r4, lr}
.LCFI13:
	mov	r4, r2
	sub	sp, sp, #36
.LCFI14:
	beq	.L77
	bhi	.L80
	cmp	r0, #1
	beq	.L75
	cmp	r0, #2
	bne	.L74
	b	.L92
.L80:
	cmp	r0, #67
	beq	.L78
	cmp	r0, #83
	bne	.L74
	bl	KEY_process_ENG_SPA
	mov	r3, #1
	str	r3, [sp, #0]
	ldr	r3, .L94
	mov	r0, sp
	str	r3, [sp, #20]
	bl	Display_Post_Command
	b	.L73
.L92:
	ldr	r3, .L94+4
	ldrsh	r2, [r3, #0]
	ldr	r3, .L94+8
	cmp	r2, #97
	beq	.L83
	cmp	r2, #98
	bne	.L82
	b	.L93
.L83:
	mov	r2, #6
	b	.L88
.L93:
	mov	r2, #7
.L88:
	str	r2, [r3, #0]
.L82:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L74
	bl	good_key_beep
	b	.L91
.L78:
	bl	good_key_beep
	ldr	r3, .L94+8
	mov	r2, #7
	str	r2, [r3, #0]
.L91:
	cmp	r4, #0
	beq	.L87
	blx	r4
.L87:
	bl	DIALOG_close_yes_no_dialog
	b	.L73
.L75:
	bl	CURSOR_Up
	b	.L73
.L77:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L73
.L74:
	bl	bad_key_beep
.L73:
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.L95:
	.align	2
.L94:
	.word	FDTO_DIALOG_redraw_yes_no_dialog
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR4
.LFE15:
	.size	DIALOG_process_yes_no_dialog, .-DIALOG_process_yes_no_dialog
	.section	.text.DIALOG_close_all_dialogs,"ax",%progbits
	.align	2
	.global	DIALOG_close_all_dialogs
	.type	DIALOG_close_all_dialogs, %function
DIALOG_close_all_dialogs:
.LFB16:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L112
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L97
	b	DIALOG_close_ok_dialog
.L97:
	ldr	r3, .L112+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L98
	b	DIALOG_close_yes_no_cancel_dialog
.L98:
	ldr	r3, .L112+8
	ldr	r0, [r3, #0]
	cmp	r0, #0
	beq	.L99
	b	DIALOG_close_yes_no_dialog
.L99:
	ldr	r2, .L112+12
	ldr	r3, [r2, #0]
	cmp	r3, #0
	strne	r0, [r2, #0]
	ldrne	r3, .L112+16
	bne	.L107
	ldr	r1, .L112+20
	ldr	r2, [r1, #0]
	cmp	r2, #0
	strne	r3, [r1, #0]
	bne	.L109
	ldr	r1, .L112+24
	ldr	r3, [r1, #0]
	cmp	r3, #0
	strne	r2, [r1, #0]
	ldrne	r3, .L112+28
	bne	.L110
.LBB4:
	ldr	r1, .L112+32
	ldr	r2, [r1, #0]
	cmp	r2, #0
	strne	r3, [r1, #0]
	ldrne	r2, .L112+36
	bne	.L111
	ldr	r1, .L112+40
	ldr	r3, [r1, #0]
	cmp	r3, #0
	beq	.L104
	ldr	r3, .L112+44
	str	r2, [r1, #0]
.L110:
	ldr	r1, [r3, #0]
	ldr	r3, .L112+48
	mov	r0, r2
	strh	r1, [r3, #0]	@ movhi
	b	.L108
.L104:
	ldr	r2, .L112+52
	ldr	r1, [r2, #0]
	cmp	r1, #0
	beq	.L105
	str	r3, [r2, #0]
	ldr	r2, .L112+56
.L111:
	ldr	r1, [r2, #0]
	ldr	r2, .L112+48
	strh	r1, [r2, #0]	@ movhi
.L109:
	mov	r0, r3
	b	.L108
.L105:
	ldr	r3, .L112+60
	ldr	r2, [r3, #0]
	cmp	r2, #0
	bxeq	lr
	mov	r0, #0
	str	r0, [r3, #0]
	ldr	r3, .L112+64
.L107:
	ldr	r2, [r3, #0]
	ldr	r3, .L112+48
	strh	r2, [r3, #0]	@ movhi
.L108:
.LBE4:
.LBB5:
	b	Redraw_Screen
.L113:
	.align	2
.L112:
	.word	.LANCHOR3
	.word	.LANCHOR0
	.word	.LANCHOR2
	.word	g_COMM_OPTIONS_dialog_visible
	.word	g_COMM_OPTIONS_cursor_position_when_dialog_displayed
	.word	g_COPY_DIALOG_visible
	.word	g_LIVE_SCREENS_dialog_visible
	.word	g_LIVE_SCREENS_cursor_position_when_dialog_displayed
	.word	g_TWO_WIRE_dialog_visible
	.word	g_TWO_WIRE_ASSIGNMENT_cursor_position_when_dialog_displayed
	.word	g_TWO_WIRE_DEBUG_dialog_visible
	.word	g_TWO_WIRE_DEBUG_cursor_position_when_dialog_displayed
	.word	GuiLib_ActiveCursorFieldNo
	.word	g_TECH_SUPPORT_dialog_visible
	.word	g_TECH_SUPPORT_cursor_position_when_dialog_displayed
	.word	g_HUB_dialog_visible
	.word	g_HUB_cursor_position_when_dialog_displayed
.LBE5:
.LFE16:
	.size	DIALOG_close_all_dialogs, .-DIALOG_close_all_dialogs
	.section	.text.FDTO_DIALOG_draw_yes_no_dialog,"ax",%progbits
	.align	2
	.type	FDTO_DIALOG_draw_yes_no_dialog, %function
FDTO_DIALOG_draw_yes_no_dialog:
.LFB12:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI15:
	mov	r4, r0
	bl	DIALOG_close_all_dialogs
	ldr	r3, .L115
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L115+4
	mov	r0, r4, asl #16
	ldrsh	r1, [r3, #0]
	ldr	r3, .L115+8
	mov	r0, r0, lsr #16
	str	r1, [r3, #0]
	ldr	r3, .L115+12
	mov	r1, #0
	str	r1, [r3, #0]
	mov	r1, #97
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, lr}
	b	GuiLib_Refresh
.L116:
	.align	2
.L115:
	.word	.LANCHOR2
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR1
	.word	.LANCHOR4
.LFE12:
	.size	FDTO_DIALOG_draw_yes_no_dialog, .-FDTO_DIALOG_draw_yes_no_dialog
	.section	.text.FDTO_DIALOG_draw_yes_no_cancel_dialog,"ax",%progbits
	.align	2
	.type	FDTO_DIALOG_draw_yes_no_cancel_dialog, %function
FDTO_DIALOG_draw_yes_no_cancel_dialog:
.LFB7:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI16:
	mov	r4, r0
	bl	DIALOG_close_all_dialogs
	ldr	r3, .L118
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L118+4
	mov	r0, r4, asl #16
	ldrsh	r1, [r3, #0]
	ldr	r3, .L118+8
	mov	r0, r0, lsr #16
	str	r1, [r3, #0]
	ldr	r3, .L118+12
	mov	r1, #0
	str	r1, [r3, #0]
	mov	r1, #97
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, lr}
	b	GuiLib_Refresh
.L119:
	.align	2
.L118:
	.word	.LANCHOR0
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR1
	.word	.LANCHOR4
.LFE7:
	.size	FDTO_DIALOG_draw_yes_no_cancel_dialog, .-FDTO_DIALOG_draw_yes_no_cancel_dialog
	.section	.text.FDTO_DIALOG_draw_ok_dialog,"ax",%progbits
	.align	2
	.type	FDTO_DIALOG_draw_ok_dialog, %function
FDTO_DIALOG_draw_ok_dialog:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI17:
	mov	r4, r0
	bl	DIALOG_close_all_dialogs
	ldr	r3, .L121
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r3, .L121+4
	mov	r0, r4, asl #16
	ldrsh	r1, [r3, #0]
	ldr	r3, .L121+8
	mov	r0, r0, lsr #16
	str	r1, [r3, #0]
	ldr	r3, .L121+12
	mov	r1, #0
	str	r1, [r3, #0]
	mov	r1, #99
	bl	GuiLib_ShowScreen
	ldmfd	sp!, {r4, lr}
	b	GuiLib_Refresh
.L122:
	.align	2
.L121:
	.word	.LANCHOR3
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR1
	.word	.LANCHOR4
.LFE2:
	.size	FDTO_DIALOG_draw_ok_dialog, .-FDTO_DIALOG_draw_ok_dialog
	.global	g_DIALOG_ok_dialog_visible
	.global	g_DIALOG_modal_result
	.section	.bss.g_DIALOG_yes_no_dialog_visible,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_DIALOG_yes_no_dialog_visible, %object
	.size	g_DIALOG_yes_no_dialog_visible, 4
g_DIALOG_yes_no_dialog_visible:
	.space	4
	.section	.bss.g_DIALOG_cursor_position_when_dialog_displayed,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_DIALOG_cursor_position_when_dialog_displayed, %object
	.size	g_DIALOG_cursor_position_when_dialog_displayed, 4
g_DIALOG_cursor_position_when_dialog_displayed:
	.space	4
	.section	.bss.g_DIALOG_yes_no_cancel_dialog_visible,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_DIALOG_yes_no_cancel_dialog_visible, %object
	.size	g_DIALOG_yes_no_cancel_dialog_visible, 4
g_DIALOG_yes_no_cancel_dialog_visible:
	.space	4
	.section	.bss.g_DIALOG_ok_dialog_visible,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_DIALOG_ok_dialog_visible, %object
	.size	g_DIALOG_ok_dialog_visible, 4
g_DIALOG_ok_dialog_visible:
	.space	4
	.section	.bss.g_DIALOG_modal_result,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_DIALOG_modal_result, %object
	.size	g_DIALOG_modal_result, 4
g_DIALOG_modal_result:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI0-.LFB11
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI1-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI3-.LFB3
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI5-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI7-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI9-.LFB10
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.byte	0x4
	.4byte	.LCFI11-.LFB13
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xe
	.uleb128 0x28
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI13-.LFB15
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.align	2
.LEFDE26:
.LSFDE28:
	.4byte	.LEFDE28-.LASFDE28
.LASFDE28:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI15-.LFB12
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE28:
.LSFDE30:
	.4byte	.LEFDE30-.LASFDE30
.LASFDE30:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI16-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE30:
.LSFDE32:
	.4byte	.LEFDE32-.LASFDE32
.LASFDE32:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI17-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE32:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/dialog.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x177
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF16
	.byte	0x1
	.4byte	.LASF17
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x232
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x134
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x1c9
	.4byte	.LFB14
	.4byte	.LFE14
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x1a7
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LLST0
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x112
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0x60
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0x43
	.4byte	.LFB0
	.4byte	.LFE0
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0x86
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.byte	0x92
	.4byte	.LFB4
	.4byte	.LFE4
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.byte	0xae
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST4
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x128
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST5
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x150
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST6
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF11
	.byte	0x1
	.2byte	0x1bd
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LLST7
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF12
	.byte	0x1
	.2byte	0x1e5
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST8
	.uleb128 0x8
	.4byte	0x21
	.4byte	.LFB16
	.4byte	.LFE16
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF13
	.byte	0x1
	.2byte	0x1ae
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST9
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x119
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST10
	.uleb128 0x9
	.4byte	.LASF15
	.byte	0x1
	.byte	0x77
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST11
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB11
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE11
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB6
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB5
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI6
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB8
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI7
	.4byte	.LCFI8
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI8
	.4byte	.LFE8
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB10
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE10
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB13
	.4byte	.LCFI11
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI11
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI12
	.4byte	.LFE13
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LFB15
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI13
	.4byte	.LCFI14
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI14
	.4byte	.LFE15
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LFB12
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LFE12
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST10:
	.4byte	.LFB7
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI16
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST11:
	.4byte	.LFB2
	.4byte	.LCFI17
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI17
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x9c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.4byte	.LFB16
	.4byte	.LFE16-.LFB16
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LFB11
	.4byte	.LFE11
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LFB13
	.4byte	.LFE13
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LFB16
	.4byte	.LFE16
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF7:
	.ascii	"DIALOG_close_ok_dialog\000"
.LASF14:
	.ascii	"FDTO_DIALOG_draw_yes_no_cancel_dialog\000"
.LASF8:
	.ascii	"DIALOG_process_ok_dialog\000"
.LASF6:
	.ascii	"DIALOG_draw_ok_dialog\000"
.LASF9:
	.ascii	"DIALOG_draw_yes_no_cancel_dialog\000"
.LASF16:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF18:
	.ascii	"DIALOG_close_all_dialogs\000"
.LASF5:
	.ascii	"DIALOG_a_dialog_is_visible\000"
.LASF4:
	.ascii	"FDTO_DIALOG_redraw_ok_dialog\000"
.LASF10:
	.ascii	"DIALOG_process_yes_no_cancel_dialog\000"
.LASF13:
	.ascii	"FDTO_DIALOG_draw_yes_no_dialog\000"
.LASF1:
	.ascii	"DIALOG_close_yes_no_dialog\000"
.LASF17:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/ui/d"
	.ascii	"ialog.c\000"
.LASF3:
	.ascii	"FDTO_DIALOG_redraw_yes_no_cancel_dialog\000"
.LASF0:
	.ascii	"DIALOG_close_yes_no_cancel_dialog\000"
.LASF15:
	.ascii	"FDTO_DIALOG_draw_ok_dialog\000"
.LASF2:
	.ascii	"FDTO_DIALOG_redraw_yes_no_dialog\000"
.LASF12:
	.ascii	"DIALOG_process_yes_no_dialog\000"
.LASF11:
	.ascii	"DIALOG_draw_yes_no_dialog\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
