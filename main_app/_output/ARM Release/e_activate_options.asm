	.file	"e_activate_options.c"
	.text
.Ltext0:
	.section	.text.generate_activation_code,"ax",%progbits
	.align	2
	.type	generate_activation_code, %function
generate_activation_code:
.LFB0:
	@ args = 0, pretend = 0, frame = 676
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r6, r7, lr}
.LCFI0:
	sub	sp, sp, #676
.LCFI1:
	add	r3, sp, #240
	str	r3, [sp, #592]
	add	r3, sp, #340
	str	r3, [sp, #596]
	add	r3, sp, #440
	str	r3, [sp, #600]
	add	r3, sp, #636
	str	r3, [sp, #608]
	add	r3, sp, #540
	str	r3, [sp, #616]
	mov	r6, r1
	add	r3, sp, #656
	mov	r1, #0
	str	r3, [sp, #624]
	strb	r1, [r0, #0]
	mov	r4, r0
	mov	r7, r2
	ldr	r0, .L3
	ldr	r2, .L3+4
	add	r3, sp, #592
	bl	hashids_init3
	cmp	r0, #0
	beq	.L1
	mov	r1, r4
	mov	r2, #1
	mov	r3, sp
	stmia	sp, {r6-r7}
	bl	hashids_encode
.L1:
	add	sp, sp, #676
	ldmfd	sp!, {r4, r6, r7, pc}
.L4:
	.align	2
.L3:
	.word	.LC0
	.word	.LC1
.LFE0:
	.size	generate_activation_code, .-generate_activation_code
	.section	.text.save_activated_option,"ax",%progbits
	.align	2
	.type	save_activated_option, %function
save_activated_option:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	bl	good_key_beep
	ldr	r4, .L6
	mov	r1, #400
	ldr	r0, [r4, #0]
	ldr	r2, .L6+4
	mov	r3, #138
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L6+8
	mov	r2, #1
	str	r2, [r3, #204]
	ldr	r0, [r4, #0]
	bl	xQueueGiveMutexRecursive
	mov	r0, #0
	mov	r1, r0
	ldmfd	sp!, {r4, lr}
	b	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
.L7:
	.align	2
.L6:
	.word	irri_comm_recursive_MUTEX
	.word	.LC2
	.word	irri_comm
.LFE1:
	.size	save_activated_option, .-save_activated_option
	.section	.text.FDTO_ACTIVATE_OPTIONS_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_ACTIVATE_OPTIONS_draw_screen
	.type	FDTO_ACTIVATE_OPTIONS_draw_screen, %function
FDTO_ACTIVATE_OPTIONS_draw_screen:
.LFB2:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	ldrne	r3, .L11
	str	lr, [sp, #-4]!
.LCFI3:
	ldrnesh	r1, [r3, #0]
	bne	.L10
	mov	r1, #0
	ldr	r0, .L11+4
	mov	r2, #65
	bl	memset
	mov	r1, #0
.L10:
	mov	r0, #3
	mov	r2, #1
	bl	GuiLib_ShowScreen
	ldr	lr, [sp], #4
	b	GuiLib_Refresh
.L12:
	.align	2
.L11:
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_GroupName
.LFE2:
	.size	FDTO_ACTIVATE_OPTIONS_draw_screen, .-FDTO_ACTIVATE_OPTIONS_draw_screen
	.section	.text.ACTIVATE_OPTIONS_process_screen,"ax",%progbits
	.align	2
	.global	ACTIVATE_OPTIONS_process_screen
	.type	ACTIVATE_OPTIONS_process_screen, %function
ACTIVATE_OPTIONS_process_screen:
.LFB3:
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L37
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI4:
	ldrsh	r2, [r3, #0]
	ldr	r3, .L37+4
	sub	sp, sp, #32
.LCFI5:
	cmp	r2, r3
	mov	r4, r0
	mov	r5, r1
	bne	.L32
	cmp	r0, #67
	beq	.L16
	cmp	r0, #2
	bne	.L17
	ldr	r3, .L37+8
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L17
.L16:
	ldr	r0, .L37+12
	ldr	r1, .L37+16
	mov	r2, #21
	bl	strlcpy
.L17:
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L37+20
	bl	KEYBOARD_process_key
	b	.L13
.L32:
	cmp	r0, #2
	beq	.L22
	bhi	.L24
	cmp	r0, #0
	beq	.L20
	cmp	r0, #1
	bne	.L19
	b	.L21
.L24:
	cmp	r0, #4
	beq	.L21
	bcc	.L20
	cmp	r0, #67
	bne	.L19
	b	.L35
.L22:
	ldr	r3, .L37+8
	ldrsh	r4, [r3, #0]
	cmp	r4, #0
	beq	.L26
	cmp	r4, #1
	bne	.L33
	b	.L36
.L26:
	bl	good_key_beep
	ldr	r1, .L37+12
	mov	r2, #65
	ldr	r0, .L37+16
	bl	strlcpy
	mov	r0, #158
	mov	r1, #91
	mov	r2, #10
	mov	r3, r4
	bl	KEYBOARD_draw_keyboard
	b	.L13
.L36:
	ldr	r5, .L37+16
	ldr	r4, .L37+24
	mov	r0, r5
	bl	strlen
	sub	r3, r0, #1
	ldrb	r2, [r5, r3]	@ zero_extendqisi2
	add	r0, sp, #32
	strb	r2, [r0, #-1]!
	mov	r2, #0
	strb	r2, [r5, r3]
	ldr	r3, [r4, #48]
	mov	r7, r2
	mov	r6, r3
	bl	atoi
	adds	r1, r6, r0
	adc	r2, r7, r0, asr #31
	mov	r0, sp
	bl	generate_activation_code
	mov	r0, r5
	mov	r1, sp
	bl	strcmp
	cmp	r0, r7
	bne	.L28
	ldrb	r3, [sp, #31]	@ zero_extendqisi2
	cmp	r3, #49
	bne	.L29
	ldrb	r3, [r4, #52]	@ zero_extendqisi2
	orr	r3, r3, #1
	strb	r3, [r4, #52]
	bl	save_activated_option
	mov	r0, #580
	b	.L34
.L29:
	cmp	r3, #50
	bne	.L30
	ldrb	r3, [r4, #52]	@ zero_extendqisi2
	orr	r3, r3, #8
	strb	r3, [r4, #52]
	bl	save_activated_option
	ldr	r0, .L37+28
.L34:
	bl	DIALOG_draw_ok_dialog
	b	.L13
.L30:
	cmp	r3, #51
	bne	.L28
	ldrb	r3, [r4, #53]	@ zero_extendqisi2
	orr	r3, r3, #16
	strb	r3, [r4, #53]
	bl	save_activated_option
	ldr	r0, .L37+32
	b	.L34
.L28:
	bl	bad_key_beep
	ldr	r0, .L37+36
	b	.L34
.L33:
	bl	bad_key_beep
	b	.L13
.L21:
	mov	r0, #1
	bl	CURSOR_Up
	b	.L13
.L20:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L13
.L35:
	ldr	r3, .L37+40
	ldr	r2, .L37+44
	ldr	r3, [r3, #0]
	mov	ip, #36
	mla	r3, ip, r3, r2
	ldr	r2, [r3, #4]
	ldr	r3, .L37+48
	str	r2, [r3, #0]
	bl	KEY_process_global_keys
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	b	.L13
.L19:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L13:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, pc}
.L38:
	.align	2
.L37:
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_ActivationCode
	.word	GuiVar_GroupName
	.word	FDTO_ACTIVATE_OPTIONS_draw_screen
	.word	config_c
	.word	581
	.word	582
	.word	583
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
.LFE3:
	.size	ACTIVATE_OPTIONS_process_screen, .-ACTIVATE_OPTIONS_process_screen
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"\000"
.LC1:
	.ascii	"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY"
	.ascii	"Z1234567890\000"
.LC2:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_activate_options.c\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x2b4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI2-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI3-.LFB2
	.byte	0xe
	.uleb128 0x4
	.byte	0x8e
	.uleb128 0x1
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xe
	.uleb128 0x34
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_activate_options.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x6c
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x46
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0x83
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xa7
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xcf
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x3
	.byte	0x7d
	.sleb128 692
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	.LCFI5
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 52
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_activate_options.c\000"
.LASF2:
	.ascii	"FDTO_ACTIVATE_OPTIONS_draw_screen\000"
.LASF3:
	.ascii	"ACTIVATE_OPTIONS_process_screen\000"
.LASF1:
	.ascii	"save_activated_option\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF0:
	.ascii	"generate_activation_code\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
