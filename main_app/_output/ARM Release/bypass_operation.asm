	.file	"bypass_operation.c"
	.text
.Ltext0:
	.section	.bss.GuiFont_LanguageActive,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageActive, %object
	.size	GuiFont_LanguageActive, 2
GuiFont_LanguageActive:
	.space	2
	.section	.bss.GuiFont_LanguageTextDir,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageTextDir, %object
	.size	GuiFont_LanguageTextDir, 2
GuiFont_LanguageTextDir:
	.space	2
	.section	.bss.GuiFont_DecimalChar,"aw",%nobits
	.align	2
	.type	GuiFont_DecimalChar, %object
	.size	GuiFont_DecimalChar, 2
GuiFont_DecimalChar:
	.space	2
	.section	.bss.GuiFont_LanguageCharSets,"aw",%nobits
	.align	2
	.type	GuiFont_LanguageCharSets, %object
	.size	GuiFont_LanguageCharSets, 2
GuiFont_LanguageCharSets:
	.space	2
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS, 12
IRRIGATION_SYSTEM_FLOW_CHECKING_RANGE_DEFAULTS:
	.word	30
	.word	65
	.word	100
	.section	.rodata.IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS,"a",%progbits
	.align	2
	.type	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, %object
	.size	IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS, 16
IRRIGATION_SYSTEM_FLOW_CHECKING_TOLERANCE_DEFAULTS:
	.word	5
	.word	10
	.word	10
	.word	15
	.global	bypass
	.section	.bss.bypass,"aw",%nobits
	.align	2
	.type	bypass, %object
	.size	bypass, 564
bypass:
	.space	564
	.global	BYPASS_transition_flow_rates
	.section	.rodata.BYPASS_transition_flow_rates,"a",%progbits
	.align	2
	.type	BYPASS_transition_flow_rates, %object
	.size	BYPASS_transition_flow_rates, 72
BYPASS_transition_flow_rates:
	.word	0
	.word	34
	.word	51
	.word	68
	.word	85
	.word	85
	.word	160
	.word	250
	.word	350
	.word	200
	.word	200
	.space	28
	.section	.text.init_bypass,"ax",%progbits
	.align	2
	.type	init_bypass, %function
init_bypass:
.LFB0:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI0:
	add	fp, sp, #4
.LCFI1:
	sub	sp, sp, #8
.LCFI2:
	ldr	r0, .L6
	mov	r1, #0
	mov	r2, #564
	bl	memset
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L2
.L5:
	ldr	r2, .L6
	ldr	r3, [fp, #-8]
	add	r3, r3, #6
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldr	r2, .L6+4	@ float
	str	r2, [r3, #0]	@ float
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L3
.L4:
	ldr	r2, .L6
	ldr	r3, [fp, #-8]
	mov	r1, #42
	mul	r1, r3, r1
	ldr	r3, [fp, #-12]
	add	r3, r1, r3
	add	r3, r3, #9
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldr	r2, .L6+4	@ float
	str	r2, [r3, #0]	@ float
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L3:
	ldr	r3, [fp, #-12]
	cmp	r3, #41
	bls	.L4
	ldr	r2, .L6
	ldr	r3, [fp, #-8]
	add	r3, r3, #135
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldr	r2, .L6+4	@ float
	str	r2, [r3, #0]	@ float
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L2:
	ldr	r3, [fp, #-8]
	cmp	r3, #2
	bls	.L5
	ldr	r3, .L6
	mov	r2, #0
	str	r2, [r3, #16]
	ldr	r3, .L6
	mov	r2, #0
	str	r2, [r3, #20]
	ldr	r3, .L6
	mov	r2, #100
	str	r2, [r3, #8]
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L7:
	.align	2
.L6:
	.word	bypass
	.word	0
.LFE0:
	.size	init_bypass, .-init_bypass
	.section .rodata
	.align	2
.LC0:
	.ascii	"BYPASS level %u not a NC MV\000"
	.align	2
.LC1:
	.ascii	"BYPASS level %u HAS NO DECODER\000"
	.section	.text.bypass_configuration_is_valid,"ax",%progbits
	.align	2
	.type	bypass_configuration_is_valid, %function
bypass_configuration_is_valid:
.LFB1:
	@ args = 0, pretend = 0, frame = 60
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI3:
	add	fp, sp, #4
.LCFI4:
	sub	sp, sp, #60
.LCFI5:
	str	r0, [fp, #-64]
	mov	r3, #1
	str	r3, [fp, #-8]
	ldr	r3, .L14
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #4]
	sub	r3, fp, #60
	mov	r0, r2
	mov	r1, r3
	bl	POC_get_fm_choice_and_rate_details
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L9
.L13:
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L10
	ldr	r3, .L14
	ldr	r1, [r3, #0]
	ldr	r2, [fp, #-12]
	mov	r3, #96
	mov	r0, #88
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #1
	beq	.L10
	mov	r3, #0
	str	r3, [fp, #-8]
	ldr	r3, .L14
	ldr	r3, [r3, #20]
	cmp	r3, #0
	beq	.L10
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	ldr	r0, .L14+4
	mov	r1, r3
	bl	Alert_Message_va
.L10:
	ldr	r3, .L14
	ldr	r1, [r3, #0]
	ldr	r2, [fp, #-12]
	mov	r3, #92
	mov	r0, #88
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L11
	mov	r3, #0
	str	r3, [fp, #-8]
	ldr	r3, .L14
	ldr	r3, [r3, #20]
	cmp	r3, #0
	beq	.L11
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	ldr	r0, .L14+8
	mov	r1, r3
	bl	Alert_Message_va
.L11:
	ldr	r2, [fp, #-12]
	mvn	r3, #55
	mov	r2, r2, asl #4
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L12
	mov	r3, #0
	str	r3, [fp, #-8]
.L12:
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L9:
	ldr	r2, [fp, #-12]
	ldr	r3, [fp, #-64]
	cmp	r2, r3
	bcc	.L13
	ldr	r3, .L14
	mov	r2, #0
	str	r2, [r3, #20]
	ldr	r3, [fp, #-8]
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L15:
	.align	2
.L14:
	.word	bypass
	.word	.LC0
	.word	.LC1
.LFE1:
	.size	bypass_configuration_is_valid, .-bypass_configuration_is_valid
	.section .rodata
	.align	2
.LC2:
	.ascii	"Linking Bypass\000"
	.align	2
.LC3:
	.ascii	"More than ONE BYPASS?\000"
	.section	.text.verify_or_link_bypass_engine,"ax",%progbits
	.align	2
	.type	verify_or_link_bypass_engine, %function
verify_or_link_bypass_engine:
.LFB2:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI6:
	add	fp, sp, #4
.LCFI7:
	sub	sp, sp, #16
.LCFI8:
	mov	r3, #0
	str	r3, [fp, #-12]
	bl	FLOWSENSE_get_controller_index
	str	r0, [fp, #-16]
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L17
.L21:
	ldr	r1, .L23
	ldr	r2, [fp, #-8]
	mov	r3, #32
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #12
	bne	.L18
	ldr	r1, .L23
	ldr	r2, [fp, #-8]
	mov	r3, #28
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-16]
	cmp	r2, r3
	bne	.L18
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L19
	mov	r3, #1
	str	r3, [fp, #-12]
	ldr	r1, .L23
	ldr	r2, [fp, #-8]
	mov	r3, #24
	mov	r0, #472
	mul	r2, r0, r2
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	mov	r0, r3
	ldr	r1, [fp, #-16]
	bl	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	str	r0, [fp, #-20]
	ldr	r3, .L23+4
	ldr	r3, [r3, #16]
	cmp	r3, #0
	beq	.L20
	ldr	r3, .L23+4
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L20
	ldr	r3, .L23+4
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-8]
	mov	r1, #472
	mul	r1, r3, r1
	ldr	r3, .L23+8
	add	r3, r1, r3
	cmp	r2, r3
	beq	.L18
.L20:
	ldr	r0, .L23+12
	bl	Alert_Message
	bl	init_bypass
	ldr	r3, .L23+4
	ldr	r2, [fp, #-20]
	str	r2, [r3, #4]
	ldr	r3, [fp, #-8]
	mov	r2, #472
	mul	r2, r3, r2
	ldr	r3, .L23+8
	add	r2, r2, r3
	ldr	r3, .L23+4
	str	r2, [r3, #0]
	ldr	r3, .L23+4
	mov	r2, #1
	str	r2, [r3, #16]
	b	.L18
.L19:
	ldr	r0, .L23+16
	bl	Alert_Message
.L18:
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L17:
	ldr	r3, [fp, #-8]
	cmp	r3, #11
	bls	.L21
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	bne	.L16
	bl	init_bypass
.L16:
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L24:
	.align	2
.L23:
	.word	poc_preserves
	.word	bypass
	.word	poc_preserves+20
	.word	.LC2
	.word	.LC3
.LFE2:
	.size	verify_or_link_bypass_engine, .-verify_or_link_bypass_engine
	.section .rodata
	.align	2
.LC4:
	.ascii	"Bypass: levels out of range\000"
	.align	2
.LC5:
	.ascii	"BYPASS sync or linkage problem\000"
	.align	2
.LC6:
	.ascii	"BYPASS : Unexpected Bermad Reed choice\000"
	.align	2
.LC7:
	.ascii	"BYPASS READING K & O error\000"
	.align	2
.LC8:
	.ascii	"BYPASS could not use flow reading!\000"
	.section	.text.receive_flow_reading_update,"ax",%progbits
	.align	2
	.type	receive_flow_reading_update, %function
receive_flow_reading_update:
.LFB3:
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI9:
	add	fp, sp, #4
.LCFI10:
	sub	sp, sp, #96
.LCFI11:
	str	r0, [fp, #-100]
	ldr	r3, .L45+8	@ float
	str	r3, [fp, #-32]	@ float
	ldr	r3, .L45+12	@ float
	str	r3, [fp, #-36]	@ float
	ldr	r3, .L45+16	@ float
	str	r3, [fp, #-40]	@ float
	bl	verify_or_link_bypass_engine
	mov	r3, #0
	str	r3, [fp, #-16]
	mov	r3, #0
	str	r3, [fp, #-44]
	mov	r3, #0
	str	r3, [fp, #-20]
	mov	r3, #0
	str	r3, [fp, #-48]
	bl	FLOWSENSE_get_controller_index
	ldr	r3, [fp, #-100]
	ldr	r1, [r3, #4]
	sub	r2, fp, #44
	sub	r3, fp, #48
	bl	POC_PRESERVES_get_poc_working_details_ptr_for_these_parameters
	str	r0, [fp, #-16]
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	bls	.L26
	ldr	r0, .L45+20
	bl	Alert_Message
.L26:
	ldr	r3, [fp, #-48]
	cmp	r3, #2
	bhi	.L27
	ldr	r3, [fp, #-16]
	cmp	r3, #0
	beq	.L27
	ldr	r3, [fp, #-44]
	cmp	r3, #0
	beq	.L27
	ldr	r3, [fp, #-44]
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-44]
	ldr	r3, [r3, #8]
	mov	r0, r2
	mov	r1, r3
	bl	nm_POC_get_pointer_to_poc_with_this_gid_and_box_index
	str	r0, [fp, #-20]
	ldr	r3, .L45+24
	ldr	r2, [r3, #4]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bne	.L28
	ldr	r3, .L45+24
	ldr	r2, [r3, #0]
	ldr	r3, [fp, #-44]
	cmp	r2, r3
	beq	.L29
.L28:
	ldr	r0, .L45+28
	bl	Alert_Message
	b	.L30
.L29:
	ldr	r3, [fp, #-44]
	ldr	r2, [r3, #4]
	sub	r3, fp, #96
	mov	r0, r2
	mov	r1, r3
	bl	POC_get_fm_choice_and_rate_details
	mov	r3, r0
	cmp	r3, #0
	beq	.L31
	ldr	r2, [fp, #-48]
	mvn	r3, #91
	mov	r2, r2, asl #4
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L32
	ldr	r2, [fp, #-48]
	mvn	r3, #91
	mov	r2, r2, asl #4
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #8
	bls	.L33
	ldr	r2, [fp, #-48]
	mvn	r3, #91
	mov	r2, r2, asl #4
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #17
	bhi	.L33
	ldr	r2, [fp, #-48]
	mvn	r3, #79
	mov	r2, r2, asl #4
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L34
	ldr	r2, [fp, #-48]
	mvn	r3, #79
	mov	r2, r2, asl #4
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #1
	bne	.L35
.L34:
	ldr	r2, [fp, #-48]
	mvn	r3, #79
	mov	r2, r2, asl #4
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L36
	mov	r3, #6
	str	r3, [fp, #-8]
	ldr	r3, .L45+32	@ float
	str	r3, [fp, #-12]	@ float
	b	.L37
.L36:
	mov	r3, #60
	str	r3, [fp, #-8]
	ldr	r3, .L45+36	@ float
	str	r3, [fp, #-12]	@ float
.L37:
	ldr	r3, [fp, #-100]
	ldr	r3, [r3, #12]
	cmp	r3, #0
	beq	.L38
	ldr	r3, [fp, #-100]
	ldr	r2, [r3, #16]
	ldr	r3, [fp, #-8]
	cmp	r2, r3
	bcs	.L38
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-100]
	ldr	r2, [r2, #12]
	fmsr	s13, r2	@ int
	fuitod	d7, s13
	fldd	d6, .L45
	fdivd	d6, d6, d7
	flds	s15, [fp, #-12]
	fcvtds	d7, s15
	fmuld	d7, d6, d7
	fcvtsd	s15, d7
	ldr	r2, .L45+24
	add	r3, r3, #6
	mov	r3, r3, asl #2
	add	r3, r2, r3
	fsts	s15, [r3, #0]
	b	.L40
.L38:
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-40]	@ float
	ldr	r1, .L45+24
	add	r3, r3, #6
	mov	r3, r3, asl #2
	add	r3, r1, r3
	str	r2, [r3, #0]	@ float
	b	.L40
.L35:
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-40]	@ float
	ldr	r1, .L45+24
	add	r3, r3, #6
	mov	r3, r3, asl #2
	add	r3, r1, r3
	str	r2, [r3, #0]	@ float
	ldr	r0, .L45+40
	bl	Alert_Message
	b	.L41
.L40:
	b	.L41
.L33:
	ldr	r3, [fp, #-100]
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L42
	ldr	r2, [fp, #-48]
	mvn	r3, #87
	mov	r2, r2, asl #4
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s13, r3	@ int
	fuitos	s14, s13
	flds	s15, [fp, #-32]
	fdivs	s14, s14, s15
	ldr	r3, [fp, #-100]
	ldr	r3, [r3, #8]
	fmsr	s13, r3	@ int
	fuitos	s15, s13
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-24]
	ldr	r2, [fp, #-48]
	mvn	r3, #83
	mov	r2, r2, asl #4
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s13, r3	@ int
	fsitos	s14, s13
	flds	s15, [fp, #-32]
	fdivs	s14, s14, s15
	flds	s15, [fp, #-36]
	fmuls	s14, s14, s15
	ldr	r2, [fp, #-48]
	mvn	r3, #83
	mov	r2, r2, asl #4
	sub	r1, fp, #4
	add	r2, r1, r2
	add	r3, r2, r3
	ldr	r3, [r3, #0]
	fmsr	s15, r3	@ int
	fsitos	s13, s15
	flds	s15, [fp, #-32]
	fdivs	s15, s13, s15
	fmuls	s15, s14, s15
	fsts	s15, [fp, #-28]
	ldr	r3, [fp, #-48]
	flds	s14, [fp, #-24]
	flds	s15, [fp, #-28]
	fadds	s14, s14, s15
	flds	s15, [fp, #-36]
	fdivs	s15, s14, s15
	ldr	r2, .L45+24
	add	r3, r3, #6
	mov	r3, r3, asl #2
	add	r3, r2, r3
	fsts	s15, [r3, #0]
	ldr	r3, [fp, #-48]
	ldr	r2, .L45+24
	add	r3, r3, #6
	mov	r3, r3, asl #2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-40]
	fcmpes	s14, s15
	fmstat
	movpl	r3, #0
	movmi	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L44
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-40]	@ float
	ldr	r1, .L45+24
	add	r3, r3, #6
	mov	r3, r3, asl #2
	add	r3, r1, r3
	str	r2, [r3, #0]	@ float
	b	.L44
.L42:
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-40]	@ float
	ldr	r1, .L45+24
	add	r3, r3, #6
	mov	r3, r3, asl #2
	add	r3, r1, r3
	str	r2, [r3, #0]	@ float
	b	.L44
.L41:
	b	.L25
.L32:
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-40]	@ float
	ldr	r1, .L45+24
	add	r3, r3, #6
	mov	r3, r3, asl #2
	add	r3, r1, r3
	str	r2, [r3, #0]	@ float
	b	.L25
.L31:
	ldr	r0, .L45+44
	bl	Alert_Message
	b	.L25
.L44:
	mov	r0, r0	@ nop
.L30:
	b	.L25
.L27:
	ldr	r0, .L45+48
	bl	Alert_Message
.L25:
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L46:
	.align	2
.L45:
	.word	0
	.word	1085227008
	.word	1203982336
	.word	1084227584
	.word	0
	.word	.LC4
	.word	bypass
	.word	.LC5
	.word	1114636288
	.word	1142292480
	.word	.LC6
	.word	.LC7
	.word	.LC8
.LFE3:
	.size	receive_flow_reading_update, .-receive_flow_reading_update
	.section	.text.calculate_working_averages,"ax",%progbits
	.align	2
	.type	calculate_working_averages, %function
calculate_working_averages:
.LFB4:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI12:
	add	fp, sp, #4
.LCFI13:
	sub	sp, sp, #28
.LCFI14:
	str	r0, [fp, #-32]
	ldr	r3, .L56+4	@ float
	str	r3, [fp, #-20]	@ float
	ldr	r3, .L56+8	@ float
	str	r3, [fp, #-24]	@ float
	ldr	r3, .L56+12	@ float
	str	r3, [fp, #-28]	@ float
	mov	r3, #0
	str	r3, [fp, #-8]
	b	.L48
.L55:
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-32]
	cmp	r2, r3
	bcs	.L49
	ldr	r3, [fp, #-8]
	mov	r2, #168
	mul	r2, r3, r2
	ldr	r3, .L56+16
	add	r3, r2, r3
	mov	r0, r3
	mov	r1, #4
	mov	r2, #42
	bl	Roll_FIFO
	ldr	r2, .L56+20
	ldr	r3, [fp, #-8]
	add	r3, r3, #6
	mov	r3, r3, asl #2
	add	r3, r2, r3
	ldr	r2, [r3, #0]	@ float
	ldr	r0, .L56+20
	ldr	r1, [fp, #-8]
	mov	r3, #36
	mov	ip, #168
	mul	r1, ip, r1
	add	r1, r0, r1
	add	r3, r1, r3
	str	r2, [r3, #0]	@ float
	ldr	r3, [fp, #-28]	@ float
	str	r3, [fp, #-16]	@ float
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L50
.L51:
	ldr	r2, .L56+20
	ldr	r3, [fp, #-8]
	mov	r1, #42
	mul	r1, r3, r1
	ldr	r3, [fp, #-12]
	add	r3, r1, r3
	add	r3, r3, #9
	mov	r3, r3, asl #2
	add	r3, r2, r3
	flds	s15, [r3, #0]
	flds	s14, [fp, #-16]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-16]
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L50:
	ldr	r3, [fp, #-12]
	cmp	r3, #41
	bls	.L51
	flds	s14, [fp, #-16]
	flds	s15, .L56
	fdivs	s15, s14, s15
	fsts	s15, [fp, #-16]
	ldr	r2, .L56+20
	ldr	r3, [fp, #-8]
	add	r3, r3, #135
	mov	r3, r3, asl #2
	add	r3, r2, r3
	flds	s14, [r3, #0]
	flds	s15, [fp, #-20]
	fmuls	s14, s14, s15
	flds	s13, [fp, #-24]
	flds	s15, [fp, #-16]
	fmuls	s15, s13, s15
	fadds	s15, s14, s15
	ldr	r2, .L56+20
	ldr	r3, [fp, #-8]
	add	r3, r3, #135
	mov	r3, r3, asl #2
	add	r3, r2, r3
	fsts	s15, [r3, #0]
	b	.L52
.L49:
	mov	r3, #0
	str	r3, [fp, #-12]
	b	.L53
.L54:
	ldr	r2, [fp, #-28]	@ float
	ldr	r1, .L56+20
	ldr	r3, [fp, #-8]
	mov	r0, #42
	mul	r0, r3, r0
	ldr	r3, [fp, #-12]
	add	r3, r0, r3
	add	r3, r3, #9
	mov	r3, r3, asl #2
	add	r3, r1, r3
	str	r2, [r3, #0]	@ float
	ldr	r3, [fp, #-12]
	add	r3, r3, #1
	str	r3, [fp, #-12]
.L53:
	ldr	r3, [fp, #-12]
	cmp	r3, #41
	bls	.L54
	ldr	r2, [fp, #-28]	@ float
	ldr	r1, .L56+20
	ldr	r3, [fp, #-8]
	add	r3, r3, #135
	mov	r3, r3, asl #2
	add	r3, r1, r3
	str	r2, [r3, #0]	@ float
.L52:
	ldr	r3, [fp, #-8]
	add	r3, r3, #1
	str	r3, [fp, #-8]
.L48:
	ldr	r3, [fp, #-8]
	cmp	r3, #2
	bls	.L55
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
.L57:
	.align	2
.L56:
	.word	1109917696
	.word	1060320051
	.word	1050253722
	.word	0
	.word	bypass+36
	.word	bypass
.LFE4:
	.size	calculate_working_averages, .-calculate_working_averages
	.section .rodata
	.align	2
.LC9:
	.ascii	"Switching to OPERATIONAL\000"
	.align	2
.LC10:
	.ascii	"Second level opened\000"
	.align	2
.LC11:
	.ascii	"Third level opened\000"
	.align	2
.LC12:
	.ascii	"Second level opened f: %5.2f, t: %5.2f\000"
	.align	2
.LC13:
	.ascii	"First level opened f: %5.2f, t: %5.2f\000"
	.align	2
.LC14:
	.ascii	"Third level opened f: %5.2f, t: %5.2f\000"
	.align	2
.LC15:
	.ascii	"BYPASS not 3 stages!\000"
	.align	2
.LC16:
	.ascii	"BYPASS state out of range\000"
	.align	2
.LC17:
	.ascii	"Switching to PASSIVE\000"
	.align	2
.LC18:
	.ascii	"BYPASS OPERATION K & O error\000"
	.section	.text.one_hertz_processing,"ax",%progbits
	.align	2
	.global	one_hertz_processing
	.type	one_hertz_processing, %function
one_hertz_processing:
.LFB5:
	@ args = 0, pretend = 0, frame = 76
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {r4, fp, lr}
.LCFI15:
	add	fp, sp, #8
.LCFI16:
	sub	sp, sp, #80
.LCFI17:
	ldr	r3, .L89	@ float
	str	r3, [fp, #-32]	@ float
	ldr	r3, .L89+4	@ float
	str	r3, [fp, #-36]	@ float
	bl	verify_or_link_bypass_engine
	ldr	r3, .L89+8
	ldr	r3, [r3, #16]
	cmp	r3, #0
	beq	.L58
	ldr	r3, .L89+8
	ldr	r3, [r3, #4]
	mov	r0, r3
	bl	POC_get_bypass_number_of_levels
	str	r0, [fp, #-20]
	ldr	r0, [fp, #-20]
	bl	bypass_configuration_is_valid
	mov	r3, r0
	cmp	r3, #0
	beq	.L58
	ldr	r0, [fp, #-20]
	bl	calculate_working_averages
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	ldr	r2, [r3, #4]
	sub	r3, fp, #84
	mov	r0, r2
	mov	r1, r3
	bl	POC_get_fm_choice_and_rate_details
	mov	r3, r0
	cmp	r3, #0
	beq	.L60
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	ldr	r3, [r3, #352]
	cmp	r3, #0
	beq	.L61
	ldr	r3, .L89+12	@ float
	str	r3, [fp, #-12]	@ float
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L62
.L63:
	ldr	r2, .L89+8
	ldr	r3, [fp, #-16]
	add	r3, r3, #135
	mov	r3, r3, asl #2
	add	r3, r2, r3
	flds	s15, [r3, #0]
	flds	s14, [fp, #-12]
	fadds	s15, s14, s15
	fsts	s15, [fp, #-12]
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L62:
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bcc	.L63
	ldr	r3, .L89+8
	ldr	r3, [r3, #8]
	cmp	r3, #100
	bne	.L64
	ldr	r3, .L89+8
	mov	r2, #200
	str	r2, [r3, #8]
	ldr	r0, .L89+16
	bl	Alert_Message
	ldr	r3, [fp, #-20]
	cmp	r3, #2
	bne	.L65
	ldr	r3, .L89+8
	mov	r2, #20
	str	r2, [r3, #12]
	ldr	r0, .L89+20
	bl	Alert_Message
	b	.L66
.L65:
	ldr	r3, .L89+8
	mov	r2, #30
	str	r2, [r3, #12]
	ldr	r0, .L89+24
	bl	Alert_Message
.L66:
	ldr	r3, .L89+8
	mov	r2, #0
	str	r2, [r3, #552]
	b	.L67
.L64:
	ldr	r3, .L89+8
	ldr	r3, [r3, #552]
	add	r2, r3, #1
	ldr	r3, .L89+8
	str	r2, [r3, #552]
	ldr	r3, .L89+8
	ldr	r3, [r3, #12]
	cmp	r3, #20
	beq	.L69
	cmp	r3, #30
	beq	.L70
	cmp	r3, #10
	bne	.L67
.L68:
	ldr	r2, [fp, #-84]
	ldr	r3, .L89+28
	ldr	r3, [r3, r2, asl #2]
	fmsr	s10, r3	@ int
	fuitos	s14, s10
	flds	s15, [fp, #-32]
	fmuls	s15, s14, s15
	ldr	r3, .L89+8
	fsts	s15, [r3, #556]
	ldr	r3, .L89+8
	flds	s14, [r3, #556]
	flds	s15, [fp, #-12]
	fcmpes	s14, s15
	fmstat
	movhi	r3, #0
	movls	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L86
	ldr	r3, [fp, #-12]	@ float
	str	r3, [fp, #-24]	@ float
	ldr	r3, .L89+8
	ldr	r3, [r3, #556]	@ float
	str	r3, [fp, #-28]	@ float
	flds	s15, [fp, #-24]
	fcvtds	d7, s15
	flds	s13, [fp, #-28]
	fcvtds	d5, s13
	fmrrd	r3, r4, d5
	mov	r2, r4
	str	r2, [sp, #0]
	ldr	r0, .L89+32
	fmrrd	r1, r2, d7
	bl	Alert_Message_va
	ldr	r3, .L89+8
	mov	r2, #20
	str	r2, [r3, #12]
	b	.L86
.L69:
	ldr	r3, .L89+8
	ldr	r3, [r3, #552]
	cmp	r3, #41
	bls	.L87
	ldr	r2, [fp, #-84]
	ldr	r3, .L89+28
	ldr	r3, [r3, r2, asl #2]
	fmsr	s11, r3	@ int
	fuitos	s14, s11
	flds	s15, [fp, #-36]
	fmuls	s15, s14, s15
	ldr	r3, .L89+8
	fsts	s15, [r3, #560]
	ldr	r2, [fp, #-68]
	ldr	r3, .L89+28
	ldr	r3, [r3, r2, asl #2]
	fmsr	s15, r3	@ int
	fuitos	s14, s15
	flds	s15, [fp, #-32]
	fmuls	s15, s14, s15
	ldr	r3, .L89+8
	fsts	s15, [r3, #556]
	ldr	r3, .L89+8
	flds	s14, [r3, #560]
	flds	s15, [fp, #-12]
	fcmpes	s14, s15
	fmstat
	movlt	r3, #0
	movge	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L73
	ldr	r3, [fp, #-12]	@ float
	str	r3, [fp, #-24]	@ float
	ldr	r3, .L89+8
	ldr	r3, [r3, #560]	@ float
	str	r3, [fp, #-28]	@ float
	flds	s15, [fp, #-24]
	fcvtds	d7, s15
	flds	s13, [fp, #-28]
	fcvtds	d5, s13
	fmrrd	r3, r4, d5
	mov	r2, r4
	str	r2, [sp, #0]
	ldr	r0, .L89+36
	fmrrd	r1, r2, d7
	bl	Alert_Message_va
	ldr	r3, .L89+8
	mov	r2, #10
	str	r2, [r3, #12]
	b	.L87
.L73:
	ldr	r3, .L89+8
	flds	s14, [r3, #556]
	flds	s15, [fp, #-12]
	fcmpes	s14, s15
	fmstat
	movhi	r3, #0
	movls	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L87
	ldr	r3, [fp, #-20]
	cmp	r3, #3
	bne	.L87
	ldr	r3, [fp, #-12]	@ float
	str	r3, [fp, #-24]	@ float
	ldr	r3, .L89+8
	ldr	r3, [r3, #556]	@ float
	str	r3, [fp, #-28]	@ float
	flds	s15, [fp, #-24]
	fcvtds	d7, s15
	flds	s13, [fp, #-28]
	fcvtds	d5, s13
	fmrrd	r3, r4, d5
	mov	r2, r4
	str	r2, [sp, #0]
	ldr	r0, .L89+40
	fmrrd	r1, r2, d7
	bl	Alert_Message_va
	ldr	r3, .L89+8
	mov	r2, #30
	str	r2, [r3, #12]
	b	.L87
.L70:
	ldr	r3, .L89+8
	ldr	r3, [r3, #552]
	cmp	r3, #41
	bls	.L88
	ldr	r2, [fp, #-68]
	ldr	r3, .L89+28
	ldr	r3, [r3, r2, asl #2]
	fmsr	s11, r3	@ int
	fuitos	s14, s11
	flds	s15, [fp, #-36]
	fmuls	s15, s14, s15
	ldr	r3, .L89+8
	fsts	s15, [r3, #560]
	ldr	r3, .L89+8
	flds	s14, [r3, #560]
	flds	s15, [fp, #-12]
	fcmpes	s14, s15
	fmstat
	movlt	r3, #0
	movge	r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L88
	ldr	r3, [fp, #-12]	@ float
	str	r3, [fp, #-24]	@ float
	ldr	r3, .L89+8
	ldr	r3, [r3, #560]	@ float
	str	r3, [fp, #-28]	@ float
	flds	s15, [fp, #-24]
	fcvtds	d7, s15
	flds	s13, [fp, #-28]
	fcvtds	d5, s13
	fmrrd	r3, r4, d5
	mov	r2, r4
	str	r2, [sp, #0]
	ldr	r0, .L89+32
	fmrrd	r1, r2, d7
	bl	Alert_Message_va
	ldr	r3, .L89+8
	mov	r2, #20
	str	r2, [r3, #12]
	b	.L88
.L86:
	mov	r0, r0	@ nop
	b	.L67
.L87:
	mov	r0, r0	@ nop
	b	.L67
.L88:
	mov	r0, r0	@ nop
.L67:
	ldr	r3, .L89+8
	ldr	r3, [r3, #12]
	cmp	r3, #10
	bne	.L75
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r3, r3, #88
	mov	r0, r3
	ldr	r1, .L89+44
	bl	POC_PRESERVES_set_master_valve_energized_bit
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r3, r3, #176
	mov	r0, r3
	mov	r1, #444
	bl	POC_PRESERVES_set_master_valve_energized_bit
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r3, r3, #264
	mov	r0, r3
	mov	r1, #444
	bl	POC_PRESERVES_set_master_valve_energized_bit
	b	.L58
.L75:
	ldr	r3, .L89+8
	ldr	r3, [r3, #12]
	cmp	r3, #20
	bne	.L76
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r3, r3, #88
	mov	r0, r3
	mov	r1, #444
	bl	POC_PRESERVES_set_master_valve_energized_bit
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r3, r3, #176
	mov	r0, r3
	ldr	r1, .L89+44
	bl	POC_PRESERVES_set_master_valve_energized_bit
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r3, r3, #264
	mov	r0, r3
	mov	r1, #444
	bl	POC_PRESERVES_set_master_valve_energized_bit
	b	.L58
.L76:
	ldr	r3, .L89+8
	ldr	r3, [r3, #12]
	cmp	r3, #30
	bne	.L77
	ldr	r3, [fp, #-20]
	cmp	r3, #3
	beq	.L78
	ldr	r0, .L89+48
	bl	Alert_Message
.L78:
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r3, r3, #88
	mov	r0, r3
	mov	r1, #444
	bl	POC_PRESERVES_set_master_valve_energized_bit
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r3, r3, #176
	mov	r0, r3
	mov	r1, #444
	bl	POC_PRESERVES_set_master_valve_energized_bit
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r3, r3, #264
	mov	r0, r3
	ldr	r1, .L89+44
	bl	POC_PRESERVES_set_master_valve_energized_bit
	b	.L58
.L77:
	ldr	r0, .L89+52
	bl	Alert_Message
	ldr	r3, .L89+8
	mov	r2, #100
	str	r2, [r3, #8]
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L79
.L80:
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r2, r3, #88
	ldr	r3, [fp, #-16]
	mov	r1, #88
	mul	r3, r1, r3
	add	r3, r2, r3
	mov	r0, r3
	ldr	r1, .L89+56
	bl	POC_PRESERVES_set_master_valve_energized_bit
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L79:
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bcc	.L80
	b	.L58
.L61:
	ldr	r3, .L89+8
	ldr	r3, [r3, #8]
	cmp	r3, #100
	beq	.L81
	ldr	r0, .L89+60
	bl	Alert_Message
.L81:
	ldr	r3, .L89+8
	mov	r2, #100
	str	r2, [r3, #8]
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L82
.L83:
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r2, r3, #88
	ldr	r3, [fp, #-16]
	mov	r1, #88
	mul	r3, r1, r3
	add	r3, r2, r3
	mov	r0, r3
	ldr	r1, .L89+56
	bl	POC_PRESERVES_set_master_valve_energized_bit
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L82:
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bcc	.L83
	b	.L58
.L60:
	ldr	r0, .L89+64
	bl	Alert_Message
	mov	r3, #0
	str	r3, [fp, #-16]
	b	.L84
.L85:
	ldr	r3, .L89+8
	ldr	r3, [r3, #0]
	add	r2, r3, #88
	ldr	r3, [fp, #-16]
	mov	r1, #88
	mul	r3, r1, r3
	add	r3, r2, r3
	mov	r0, r3
	ldr	r1, .L89+56
	bl	POC_PRESERVES_set_master_valve_energized_bit
	ldr	r3, [fp, #-16]
	add	r3, r3, #1
	str	r3, [fp, #-16]
.L84:
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-20]
	cmp	r2, r3
	bcc	.L85
.L58:
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}
.L90:
	.align	2
.L89:
	.word	1066192077
	.word	1063675494
	.word	bypass
	.word	0
	.word	.LC9
	.word	.LC10
	.word	.LC11
	.word	BYPASS_transition_flow_rates
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.word	333
	.word	.LC15
	.word	.LC16
	.word	555
	.word	.LC17
	.word	.LC18
.LFE5:
	.size	one_hertz_processing, .-one_hertz_processing
	.section	.text.BYPASS_get_transition_rate_for_this_flow_meter_type,"ax",%progbits
	.align	2
	.global	BYPASS_get_transition_rate_for_this_flow_meter_type
	.type	BYPASS_get_transition_rate_for_this_flow_meter_type, %function
BYPASS_get_transition_rate_for_this_flow_meter_type:
.LFB6:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
.LCFI18:
	add	fp, sp, #0
.LCFI19:
	sub	sp, sp, #8
.LCFI20:
	str	r0, [fp, #-8]
	mov	r3, #0
	str	r3, [fp, #-4]
	ldr	r3, [fp, #-8]
	cmp	r3, #7
	bhi	.L92
	ldr	r3, .L93
	ldr	r2, [fp, #-8]
	ldr	r3, [r3, r2, asl #2]
	str	r3, [fp, #-4]
.L92:
	ldr	r3, [fp, #-4]
	mov	r0, r3
	add	sp, fp, #0
	ldmfd	sp!, {fp}
	bx	lr
.L94:
	.align	2
.L93:
	.word	BYPASS_transition_flow_rates
.LFE6:
	.size	BYPASS_get_transition_rate_for_this_flow_meter_type, .-BYPASS_get_transition_rate_for_this_flow_meter_type
	.section .rodata
	.align	2
.LC19:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/bypass_operation.c\000"
	.section	.text.BYPASS_task,"ax",%progbits
	.align	2
	.global	BYPASS_task
	.type	BYPASS_task, %function
BYPASS_task:
.LFB7:
	@ args = 0, pretend = 0, frame = 28
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
.LCFI21:
	add	fp, sp, #4
.LCFI22:
	sub	sp, sp, #28
.LCFI23:
	str	r0, [fp, #-32]
	ldr	r2, [fp, #-32]
	ldr	r3, .L102
	rsb	r3, r3, r2
	mov	r3, r3, lsr #5
	str	r3, [fp, #-8]
	bl	init_bypass
.L101:
	ldr	r3, .L102+4
	ldr	r2, [r3, #0]
	sub	r3, fp, #28
	mov	r0, r2
	mov	r1, r3
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	mov	r3, r0
	cmp	r3, #1
	bne	.L96
	ldr	r3, .L102+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L102+12
	ldr	r3, .L102+16
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, .L102+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	mov	r1, #400
	ldr	r2, .L102+12
	ldr	r3, .L102+24
	bl	xQueueTakeMutexRecursive_debug
	ldr	r3, [fp, #-28]
	cmp	r3, #2000
	beq	.L99
	ldr	r2, .L102+28
	cmp	r3, r2
	beq	.L100
	cmp	r3, #1000
	beq	.L98
	b	.L97
.L99:
	sub	r3, fp, #28
	mov	r0, r3
	bl	receive_flow_reading_update
	b	.L97
.L98:
	bl	one_hertz_processing
	b	.L97
.L100:
	ldr	r3, .L102+32
	mov	r2, #1
	str	r2, [r3, #20]
	mov	r0, r0	@ nop
.L97:
	ldr	r3, .L102+20
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
	ldr	r3, .L102+8
	ldr	r3, [r3, #0]
	mov	r0, r3
	bl	xQueueGiveMutexRecursive
.L96:
	bl	xTaskGetTickCount
	mov	r1, r0
	ldr	r3, .L102+36
	ldr	r2, [fp, #-8]
	str	r1, [r3, r2, asl #2]
	b	.L101
.L103:
	.align	2
.L102:
	.word	Task_Table
	.word	BYPASS_event_queue
	.word	poc_preserves_recursive_MUTEX
	.word	.LC19
	.word	947
	.word	list_poc_recursive_MUTEX
	.word	949
	.word	3000
	.word	bypass
	.word	task_last_execution_stamp
.LFE7:
	.size	BYPASS_task, .-BYPASS_task
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI3-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI6-.LFB2
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI9-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI10-.LCFI9
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI12-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI15-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI18-.LFB6
	.byte	0xe
	.uleb128 0x4
	.byte	0x8b
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xd
	.uleb128 0xb
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI21-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xc
	.uleb128 0xb
	.uleb128 0x4
	.align	2
.LEFDE14:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/irrigation/bypass_operation.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xbe
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.byte	0x88
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.byte	0xad
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST1
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.byte	0xfb
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST2
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x148
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST3
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x207
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST4
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x25c
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST5
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x38c
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST6
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x39b
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST7
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB1
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI4
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB2
	.4byte	.LCFI6
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI6
	.4byte	.LCFI7
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI7
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB3
	.4byte	.LCFI9
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI9
	.4byte	.LCFI10
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI10
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB4
	.4byte	.LCFI12
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI12
	.4byte	.LCFI13
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI13
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LFB5
	.4byte	.LCFI15
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI15
	.4byte	.LCFI16
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI16
	.4byte	.LFE5
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB6
	.4byte	.LCFI18
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI18
	.4byte	.LCFI19
	.2byte	0x2
	.byte	0x7d
	.sleb128 4
	.4byte	.LCFI19
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LFB7
	.4byte	.LCFI21
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI21
	.4byte	.LCFI22
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI22
	.4byte	.LFE7
	.2byte	0x2
	.byte	0x7b
	.sleb128 4
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x54
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF0:
	.ascii	"init_bypass\000"
.LASF2:
	.ascii	"verify_or_link_bypass_engine\000"
.LASF3:
	.ascii	"receive_flow_reading_update\000"
.LASF5:
	.ascii	"one_hertz_processing\000"
.LASF6:
	.ascii	"BYPASS_get_transition_rate_for_this_flow_meter_type"
	.ascii	"\000"
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/irri"
	.ascii	"gation/bypass_operation.c\000"
.LASF4:
	.ascii	"calculate_working_averages\000"
.LASF7:
	.ascii	"BYPASS_task\000"
.LASF1:
	.ascii	"bypass_configuration_is_valid\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
