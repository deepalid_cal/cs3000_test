	.file	"sdr_sdram.c"
	.text
.Ltext0:
	.global	__udivsi3
	.section	.text.sdr_sdram_setup,"ax",%progbits
	.align	2
	.global	sdr_sdram_setup
	.type	sdr_sdram_setup, %function
sdr_sdram_setup:
.LFB0:
	@ args = 0, pretend = 0, frame = 4
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L4
	mov	r2, #114688
	stmfd	sp!, {r0, r4, r5, r6, lr}
.LCFI0:
	str	r2, [r3, #104]
	ldr	r2, [r3, #104]
	mov	r5, r0
	bic	r2, r2, #7340032
	str	r2, [r3, #104]
	mov	r6, r1
	bl	sdram_find_config
	cmp	r0, #0
	beq	.L1
	ldr	r4, .L4+4
	ldr	r3, .L4+8
	mov	r0, r0, asl #7
	orr	r0, r0, #2
	str	r0, [r4, #256]
	str	r3, [r4, #260]
	mov	r3, #17
	str	r3, [r4, #40]
	mov	r0, r5
	bl	sdram_adjust_timing
	ldr	r3, .L4+12
	ldr	r0, .L4+16
	str	r3, [r4, #32]
	mov	r1, #100
	bl	timer_wait_us
	ldr	r3, .L4+20
	ldr	r0, .L4+16
	str	r3, [r4, #32]
	mov	r3, #0
	str	r3, [r4, #36]
	mov	r1, #10
	bl	timer_wait_us
	ldr	r0, .L4+16
	mov	r1, #10
	bl	timer_wait_us
	mov	r1, #128000
	mov	r0, r5
	bl	__udivsi3
	mov	r3, #147
	mov	r2, #48
	mov	r1, #1
	mov	r0, r0, asl #17
	mov	r0, r0, lsr #21
	str	r0, [r4, #36]
	str	r3, [r4, #32]
	ldr	r3, .L4+24
	ldr	r0, .L4+16
	ldr	r3, [r3, #0]
	mov	r3, r2, asl r3
	add	r3, r3, #-2147483648
	ldr	r3, [r3, #0]
	str	r3, [sp, #0]
	bl	timer_wait_us
	cmp	r6, #0
	beq	.L3
	mov	r3, #-2147483648
	ldr	r3, [r3, #0]
	ldr	r0, .L4+16
	mov	r1, #1
	str	r3, [sp, #0]
	bl	timer_wait_us
.L3:
	ldr	r0, .L4+16
	mov	r3, #24
	mov	r1, #1
	str	r3, [r4, #32]
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	timer_wait_us
.L1:
	add	sp, sp, #4
	ldmfd	sp!, {r4, r5, r6, pc}
.L5:
	.align	2
.L4:
	.word	1073758208
	.word	822607872
	.word	770
	.word	403
	.word	1074020352
	.word	275
	.word	modeshift
.LFE0:
	.size	sdr_sdram_setup, .-sdr_sdram_setup
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.byte	0x80
	.uleb128 0x5
	.align	2
.LEFDE0:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/board_init/sdr_sdram.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x32
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF0
	.byte	0x1
	.4byte	.LASF1
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x34
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	.LLST0
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB0
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE0
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB0
	.4byte	.LFE0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF1:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/board_in"
	.ascii	"it/sdr_sdram.c\000"
.LASF2:
	.ascii	"sdr_sdram_setup\000"
.LASF0:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
