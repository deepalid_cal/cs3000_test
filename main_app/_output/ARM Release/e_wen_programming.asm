	.file	"e_wen_programming.c"
	.text
.Ltext0:
	.section	.text.get_guivars_from_wen_programming_struct,"ax",%progbits
	.align	2
	.type	get_guivars_from_wen_programming_struct, %function
get_guivars_from_wen_programming_struct:
.LFB1:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L14
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	ldr	r3, [r3, #0]
	ldr	r5, .L14+4
	cmp	r3, #6
.LBB4:
	ldr	r3, [r5, #0]
.LBE4:
	bne	.L2
.LBB5:
	cmp	r3, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldr	r2, [r3, #164]
	cmp	r2, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldr	r4, [r3, #168]
	cmp	r4, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldr	r2, [r3, #268]
	ldr	r3, .L14+8
	mov	r1, r4
	str	r2, [r3, #0]
	ldr	r0, .L14+12
	mov	r2, #49
	bl	strlcpy
	ldr	r0, .L14+12
	mov	r1, #49
	bl	e_SHARED_string_validation
	ldr	r1, [r5, #0]
	mov	r2, #49
	add	r1, r1, #200
	ldr	r0, .L14+16
	bl	strlcpy
	ldr	r1, [r5, #0]
	mov	r2, #49
	add	r1, r1, #232
	ldr	r0, .L14+20
	bl	strlcpy
	ldr	r1, [r5, #0]
	mov	r2, #49
	add	r1, r1, #247
	ldr	r0, .L14+24
	bl	strlcpy
	ldr	r0, .L14+16
	mov	r1, #49
	bl	e_SHARED_string_validation
	ldr	r0, .L14+20
	mov	r1, #49
	bl	e_SHARED_string_validation
	ldr	r0, .L14+24
	mov	r1, #49
	bl	e_SHARED_string_validation
	mov	r1, #49
	ldr	r0, .L14+28
	bl	e_SHARED_string_validation
	ldr	r2, [r4, #116]
	ldr	r3, .L14+32
	cmp	r2, #0
	movne	r2, #1
	strne	r2, [r3, #0]
	streq	r2, [r3, #0]
	ldrne	r0, .L14+36
	mov	r2, #17
	ldrne	r1, .L14+40
	ldreq	r0, .L14+36
	addeq	r1, r4, #98
	bl	strlcpy
	add	r0, r4, #620
	bl	atoi
	ldr	r3, .L14+44
	str	r0, [r3, #0]
	add	r0, r4, #624
	add	r0, r0, #2
	bl	atoi
	ldr	r3, .L14+48
	str	r0, [r3, #0]
	add	r0, r4, #632
	bl	atoi
	ldr	r3, .L14+52
	str	r0, [r3, #0]
	add	r0, r4, #636
	add	r0, r0, #2
	bl	atoi
	ldr	r3, .L14+56
	str	r0, [r3, #0]
	add	r0, r4, #660
	bl	atoi
	ldr	r3, .L14+60
	str	r0, [r3, #0]
	add	r0, r4, #664
	add	r0, r0, #2
	bl	atoi
	ldr	r3, .L14+64
	str	r0, [r3, #0]
	add	r0, r4, #672
	bl	atoi
	ldr	r3, .L14+68
	str	r0, [r3, #0]
	add	r0, r4, #676
	add	r0, r0, #2
	bl	atoi
	ldr	r3, .L14+72
	ldr	r2, [r4, #600]
	rsb	r2, r2, #32
	str	r0, [r3, #0]
	b	.L13
.L2:
.LBE5:
.LBB6:
	cmp	r3, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldr	r5, [r3, #180]
	cmp	r5, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldr	r4, [r3, #192]
	cmp	r4, #0
	ldmeqfd	sp!, {r4, r5, r6, pc}
	ldr	r3, [r5, #4]
	ldr	r2, .L14+32
	cmp	r3, #0
	ldr	r6, .L14+4
	ldrne	r1, .L14+76
	addeq	r1, r4, #45
	str	r3, [r2, #0]
	ldr	r0, .L14+36
	mov	r2, #17
	bl	strlcpy
	ldr	r1, .L14+80
	ldr	r0, .L14+36
	bl	strtok
	ldr	r1, [r6, #0]
	mov	r2, #49
	add	r1, r1, #232
	ldr	r0, .L14+20
	bl	strlcpy
	ldr	r0, .L14+20
	bl	strlen
	cmp	r0, #0
	bne	.L8
	ldr	r0, .L14+20
	ldr	r1, .L14+84
	mov	r2, #49
	bl	strlcpy
.L8:
	ldr	r3, [r6, #0]
	add	r0, r4, #169
	ldr	r2, [r3, #268]
	ldr	r3, .L14+8
	str	r2, [r3, #0]
	bl	atoi
	ldr	r3, .L14+60
	str	r0, [r3, #0]
	add	r0, r4, #175
	bl	atoi
	ldr	r3, .L14+64
	str	r0, [r3, #0]
	add	r0, r4, #181
	bl	atoi
	ldr	r3, .L14+68
	str	r0, [r3, #0]
	add	r0, r4, #187
	bl	atoi
	ldr	r3, .L14+72
	str	r0, [r3, #0]
	add	r0, r4, #21
	bl	atoi
	ldr	r3, .L14+44
	str	r0, [r3, #0]
	add	r0, r4, #27
	bl	atoi
	ldr	r3, .L14+48
	str	r0, [r3, #0]
	add	r0, r4, #33
	bl	atoi
	ldr	r3, .L14+52
	str	r0, [r3, #0]
	add	r0, r4, #39
	bl	atoi
	ldr	r3, .L14+56
	add	r1, r5, #32
	mov	r2, #49
	str	r0, [r3, #0]
	ldr	r0, .L14+24
	bl	strlcpy
	ldr	r0, .L14+24
	bl	strlen
	cmp	r0, #0
	bne	.L9
	ldr	r0, .L14+24
	ldr	r1, .L14+84
	mov	r2, #49
	bl	strlcpy
.L9:
	ldr	r1, .L14+24
	mov	r2, #49
	ldr	r0, .L14+12
	bl	strlcpy
	add	r1, r5, #8
	mov	r2, #49
	ldr	r0, .L14+16
	bl	strlcpy
	ldr	r0, .L14+16
	bl	strlen
	cmp	r0, #0
	bne	.L10
	ldr	r0, .L14+16
	ldr	r1, .L14+84
	mov	r2, #49
	bl	strlcpy
.L10:
	ldr	r2, [r5, #0]
.L13:
	ldr	r3, .L14+88
	str	r2, [r3, #0]
	ldmfd	sp!, {r4, r5, r6, pc}
.L15:
	.align	2
.L14:
	.word	GuiVar_WENRadioType
	.word	dev_state
	.word	GuiVar_ENObtainIPAutomatically
	.word	GuiVar_WENMACAddress
	.word	GuiVar_ENModel
	.word	GuiVar_ENFirmwareVer
	.word	GuiVar_ENMACAddress
	.word	GuiVar_ENSubnetMask
	.word	GuiVar_ENDHCPNameNotSet
	.word	GuiVar_ENDHCPName
	.word	.LC0
	.word	GuiVar_ENIPAddress_1
	.word	GuiVar_ENIPAddress_2
	.word	GuiVar_ENIPAddress_3
	.word	GuiVar_ENIPAddress_4
	.word	GuiVar_ENGateway_1
	.word	GuiVar_ENGateway_2
	.word	GuiVar_ENGateway_3
	.word	GuiVar_ENGateway_4
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	GuiVar_ENNetmask
.LBE6:
.LFE1:
	.size	get_guivars_from_wen_programming_struct, .-get_guivars_from_wen_programming_struct
	.section	.text.FDTO_WEN_PROGRAMMING_draw_screen,"ax",%progbits
	.align	2
	.global	FDTO_WEN_PROGRAMMING_draw_screen
	.type	FDTO_WEN_PROGRAMMING_draw_screen, %function
FDTO_WEN_PROGRAMMING_draw_screen:
.LFB6:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #1
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI1:
	mov	r5, r0
	mov	r6, r1
	bne	.L17
	ldr	r3, .L26
	ldr	r4, [r3, #0]
	cmp	r4, #0
	bne	.L17
.LBB10:
	ldr	r3, .L26+4
	ldr	r7, .L26+8
	ldr	r1, .L26+12
	mov	r2, #49
	ldr	r0, .L26+16
	str	r3, [r7, #0]
	bl	strlcpy
	ldr	r3, .L26+20
	ldr	r1, .L26+12
	str	r5, [r3, #0]
	mov	r2, #17
	ldr	r0, .L26+24
	bl	strlcpy
	ldr	r3, .L26+28
	ldr	r1, .L26+32
	str	r4, [r3, #0]
	ldr	r3, .L26+36
	mov	r2, #49
	str	r4, [r3, #0]
	ldr	r3, .L26+40
	ldr	r0, .L26+44
	str	r4, [r3, #0]
	ldr	r3, .L26+48
	str	r4, [r3, #0]
	bl	strlcpy
	ldr	r3, .L26+52
	ldr	r1, .L26+12
	str	r4, [r3, #0]
	ldr	r3, .L26+56
	mov	r2, #49
	str	r4, [r3, #0]
	ldr	r3, .L26+60
	ldr	r0, .L26+64
	str	r4, [r3, #0]
	ldr	r3, .L26+68
	str	r4, [r3, #0]
	bl	strlcpy
	ldr	r1, .L26+12
	mov	r2, #49
	ldr	r0, .L26+72
	bl	strlcpy
	ldr	r1, .L26+12
	mov	r2, #49
	ldr	r0, .L26+76
	bl	strlcpy
	mov	r2, #49
	ldr	r1, .L26+12
	ldr	r0, .L26+80
	bl	strlcpy
.LBE10:
	bl	g_WEN_PROGRAMMING_initialize_wifi_guivars
	ldr	r0, .L26+84
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L26+88
	ldr	r2, .L26+92
	cmp	r6, #1
	str	r6, [r3, #0]
	ldreq	r2, [r2, #80]
	ldr	r3, .L26+96
	ldrne	r2, [r2, #84]
	str	r0, [r7, #0]
	b	.L24
.L17:
.LBB11:
	ldr	r3, .L26+100
	ldr	r4, [r3, #0]
	cmp	r4, #11
	bne	.L20
.L25:
	mov	r2, #0
.L24:
	str	r2, [r3, #0]
	b	.L19
.L20:
	cmp	r4, #13
	beq	.L25
.L21:
	ldr	r3, .L26+104
	ldrsh	r4, [r3, #0]
	cmn	r4, #1
	ldreq	r3, .L26+108
	ldreq	r4, [r3, #0]
.L19:
.LBE11:
	mov	r1, r4, asl #16
	mov	r0, #68
	mov	r1, r1, asr #16
	mov	r2, #1
	bl	GuiLib_ShowScreen
	bl	GuiLib_Refresh
	cmp	r5, #1
	ldmnefd	sp!, {r4, r5, r6, r7, pc}
	ldr	r3, .L26
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldmnefd	sp!, {r4, r5, r6, r7, pc}
	bl	DEVICE_EXCHANGE_draw_dialog
	ldr	r3, .L26+88
	ldr	r2, .L26+112
	ldr	r0, [r3, #0]
	mov	r1, #82
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	b	e_SHARED_start_device_communication
.L27:
	.align	2
.L26:
	.word	.LANCHOR0
	.word	36865
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	.LC4
	.word	GuiVar_ENModel
	.word	GuiVar_ENObtainIPAutomatically
	.word	GuiVar_ENDHCPName
	.word	GuiVar_ENIPAddress_1
	.word	.LC5
	.word	GuiVar_ENIPAddress_2
	.word	GuiVar_ENIPAddress_3
	.word	GuiVar_ENSubnetMask
	.word	GuiVar_ENIPAddress_4
	.word	GuiVar_ENGateway_1
	.word	GuiVar_ENGateway_2
	.word	GuiVar_ENGateway_3
	.word	GuiVar_ENMACAddress
	.word	GuiVar_ENGateway_4
	.word	GuiVar_WENMACAddress
	.word	GuiVar_ENFirmwareVer
	.word	GuiVar_CommOptionInfoText
	.word	36867
	.word	.LANCHOR1
	.word	config_c
	.word	GuiVar_WENRadioType
	.word	.LANCHOR2
	.word	GuiLib_ActiveCursorFieldNo
	.word	.LANCHOR3
	.word	.LANCHOR4
.LFE6:
	.size	FDTO_WEN_PROGRAMMING_draw_screen, .-FDTO_WEN_PROGRAMMING_draw_screen
	.section	.text.FDTO_WEN_PROGRAMMING_process_device_exchange_key,"ax",%progbits
	.align	2
	.type	FDTO_WEN_PROGRAMMING_process_device_exchange_key, %function
FDTO_WEN_PROGRAMMING_process_device_exchange_key:
.LFB4:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI2:
	mov	r4, r0
	bl	e_SHARED_get_info_text_from_programming_struct
	sub	r3, r4, #36864
	cmp	r3, #6
	ldmhifd	sp!, {r4, pc}
	mov	r2, #1
	mov	r3, r2, asl r3
	tst	r3, #73
	bne	.L30
	tst	r3, #36
	bne	.L32
	tst	r3, #18
	ldmeqfd	sp!, {r4, pc}
.LBB14:
	ldr	r3, .L36
	ldr	r4, .L36+4
	ldr	r3, [r3, #0]
	cmp	r3, r2
	ldreq	r0, .L36+8
	beq	.L35
	bl	get_guivars_from_wen_programming_struct
	bl	get_wifi_guivars_from_wen_programming_struct
	ldr	r0, .L36+12
.L35:
	bl	e_SHARED_index_keycode_for_gui
	str	r0, [r4, #0]
	bl	DIALOG_close_ok_dialog
	ldr	r3, .L36+16
	mov	r0, #0
	ldr	r1, [r3, #0]
.LBE14:
	ldmfd	sp!, {r4, lr}
.LBB15:
	b	FDTO_WEN_PROGRAMMING_draw_screen
.L32:
.LBE15:
	bl	get_guivars_from_wen_programming_struct
	bl	get_wifi_guivars_from_wen_programming_struct
.L30:
	mov	r0, r4
	bl	e_SHARED_index_keycode_for_gui
	ldr	r3, .L36+4
	str	r0, [r3, #0]
	ldmfd	sp!, {r4, lr}
	b	DEVICE_EXCHANGE_draw_dialog
.L37:
	.align	2
.L36:
	.word	.LANCHOR5
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	36867
	.word	36865
	.word	.LANCHOR1
.LFE4:
	.size	FDTO_WEN_PROGRAMMING_process_device_exchange_key, .-FDTO_WEN_PROGRAMMING_process_device_exchange_key
	.section	.text.FDTO_WEN_PROGRAMMING_redraw_screen,"ax",%progbits
	.align	2
	.type	FDTO_WEN_PROGRAMMING_redraw_screen, %function
FDTO_WEN_PROGRAMMING_redraw_screen:
.LFB5:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L39
	ldr	r1, [r3, #0]
	b	FDTO_WEN_PROGRAMMING_draw_screen
.L40:
	.align	2
.L39:
	.word	.LANCHOR1
.LFE5:
	.size	FDTO_WEN_PROGRAMMING_redraw_screen, .-FDTO_WEN_PROGRAMMING_redraw_screen
	.section	.text.WEN_PROGRAMMING_process_screen,"ax",%progbits
	.align	2
	.global	WEN_PROGRAMMING_process_screen
	.type	WEN_PROGRAMMING_process_screen, %function
WEN_PROGRAMMING_process_screen:
.LFB7:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L136
	ldr	r2, .L136+4
	ldrsh	r3, [r3, #0]
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
.LCFI3:
	cmp	r3, r2
	sub	sp, sp, #48
.LCFI4:
	mov	r4, r0
	mov	r5, r1
	beq	.L44
	bgt	.L47
	cmp	r3, #70
	bne	.L42
	b	.L134
.L47:
	ldr	r2, .L136+8
	cmp	r3, r2
	beq	.L45
	cmp	r3, #740
	bne	.L42
	b	.L135
.L44:
	cmp	r0, #67
	beq	.L48
	cmp	r0, #2
	bne	.L49
	ldr	r3, .L136+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #49
	bne	.L49
.L48:
	ldr	r0, .L136+16
	ldr	r1, .L136+20
	mov	r2, #17
	bl	strlcpy
.L49:
	mov	r0, r4
	mov	r1, r5
	ldr	r2, .L136+24
	bl	KEYBOARD_process_key
	b	.L41
.L45:
	ldr	r1, .L136+28
	b	.L123
.L135:
	ldr	r1, .L136+32
.L123:
	bl	COMBO_BOX_key_press
	b	.L41
.L134:
	bl	WEN_PROGRAMMING_process_wifi_settings
	b	.L41
.L42:
	cmp	r4, #4
	beq	.L56
	bhi	.L60
	cmp	r4, #1
	beq	.L53
	bcc	.L52
	cmp	r4, #2
	beq	.L54
	cmp	r4, #3
	bne	.L51
	b	.L55
.L60:
	cmp	r4, #84
	beq	.L58
	bhi	.L61
	cmp	r4, #67
	beq	.L57
	cmp	r4, #80
	bne	.L51
	b	.L58
.L61:
	sub	r3, r4, #36864
	cmp	r3, #6
	bhi	.L51
	ldr	r3, .L136+36
	ldr	r2, .L136+40
	add	r0, sp, #12
	cmp	r4, r2
	cmpne	r4, r3
	ldrne	r3, .L136+44
	movne	r2, #0
	strne	r2, [r3, #0]
	mov	r3, #2
	str	r3, [sp, #12]
	ldr	r3, .L136+48
	str	r4, [sp, #36]
	str	r3, [sp, #32]
	bl	Display_Post_Command
	ldr	r3, .L136+52
	cmp	r4, r3
	bne	.L63
	ldr	r3, .L136+56
	ldr	r2, [r3, #0]
	cmp	r2, #1
	bne	.L41
	mov	r2, #0
	str	r2, [r3, #0]
	ldr	r3, .L136+60
	mov	r1, #82
	ldr	r0, [r3, #0]
	ldr	r2, .L136+44
	b	.L124
.L63:
	ldr	r3, .L136+64
	cmp	r4, r3
	moveq	r2, #0
	bne	.L41
	b	.L125
.L54:
	ldr	r4, .L136+68
	ldr	r0, .L136+36
	ldr	r5, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r5, r0
	beq	.L112
	ldr	r0, .L136+40
	ldr	r4, [r4, #0]
	bl	e_SHARED_index_keycode_for_gui
	cmp	r4, r0
	beq	.L112
	ldr	r5, .L136+12
	ldrsh	r3, [r5, #0]
	cmp	r3, #13
	ldrls	pc, [pc, r3, asl #2]
	b	.L112
.L72:
	.word	.L66
	.word	.L67
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L68
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L112
	.word	.L69
	.word	.L70
	.word	.L71
.L66:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #12]
	ldr	r3, .L136+72
	b	.L126
.L67:
	bl	good_key_beep
	ldr	r1, .L136+16
	mov	r2, #17
	ldr	r0, .L136+20
	bl	strlcpy
	mov	r1, #0
	mov	r2, #17
	ldr	r0, .L136+16
	bl	memset
	mov	r0, #102
	mov	r1, #68
	mov	r2, #17
	mov	r3, #0
	bl	KEYBOARD_draw_keyboard
	b	.L41
.L68:
	bl	good_key_beep
	mov	r3, #1
	str	r3, [sp, #12]
	ldr	r3, .L136+76
.L126:
	add	r0, sp, #12
	str	r3, [sp, #32]
	bl	Display_Post_Command
	b	.L41
.L69:
	ldr	r1, .L136+80
	mov	r2, #70
	str	r2, [sp, #20]
	ldr	r2, .L136+84
	str	r1, [sp, #28]
	ldr	r1, .L136+88
	str	r2, [sp, #32]
	mov	r2, #1
	str	r2, [sp, #36]
	str	r2, [r1, #0]
	ldr	r2, .L136+92
	mov	r3, #2
	str	r3, [sp, #12]
	mov	r3, #11
	str	r3, [sp, #16]
	str	r3, [r2, #0]
	b	.L128
.L71:
	ldr	r3, .L136+44
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L73
	mov	r0, #1
	bl	g_WEN_PROGRAMMING_wifi_values_in_range
	cmp	r0, #0
	beq	.L74
	bl	good_key_beep
	ldr	r3, .L136+12
	ldr	r5, .L136+96
	ldrsh	r2, [r3, #0]
	ldr	r3, .L136+100
	str	r2, [r3, #0]
.LBB20:
	ldr	r3, .L136+104
	ldr	r3, [r3, #0]
	cmp	r3, #6
.LBB21:
	ldr	r3, [r5, #0]
.LBE21:
	bne	.L75
.LBB22:
	cmp	r3, #0
	beq	.L76
	ldr	r2, [r3, #164]
	cmp	r2, #0
	beq	.L76
	ldr	r4, [r3, #168]
	cmp	r4, #0
	beq	.L76
	ldr	r2, .L136+32
	ldr	r0, .L136+108
	ldr	r2, [r2, #0]
	str	r2, [r3, #268]
	ldr	r3, .L136+28
	ldr	r3, [r3, #0]
	rsb	r3, r3, #32
	str	r3, [r4, #600]
	bl	strlen
	ldr	r1, .L136+108
	mov	r2, r0
	ldr	r0, .L136+16
	bl	strncmp
	cmp	r0, #0
	add	r0, r4, #98
	bne	.L77
	ldr	r3, [r4, #116]
	cmp	r3, #0
	ldrne	r1, [r5, #0]
	addne	r1, r1, #247
	bne	.L117
.L77:
	ldr	r1, .L136+16
.L117:
	ldr	r7, .L136+112
	mov	r2, #18
	bl	strlcpy
	ldr	r6, .L136+116
	ldr	r3, [r7, #0]
	ldr	r5, .L136+120
	str	r3, [sp, #0]
	ldr	r3, [r6, #0]
	ldr	r8, .L136+124
	str	r3, [sp, #4]
	ldr	r3, [r5, #0]
	mov	r1, #16
	str	r3, [sp, #8]
	ldr	r2, .L136+128
	ldr	r3, [r8, #0]
	add	r0, r4, #604
	bl	snprintf
	ldr	r3, [r8, #0]
	mov	r1, #6
	ldr	r2, .L136+132
	add	r0, r4, #620
	bl	snprintf
	add	r0, r4, #624
	ldr	r3, [r7, #0]
	mov	r1, #6
	ldr	r2, .L136+132
	add	r0, r0, #2
	bl	snprintf
	ldr	r7, .L136+136
	ldr	r3, [r6, #0]
	mov	r1, #6
	ldr	r2, .L136+132
	add	r0, r4, #632
	bl	snprintf
	add	r0, r4, #636
	ldr	r3, [r5, #0]
	mov	r1, #6
	ldr	r2, .L136+132
	add	r0, r0, #2
	bl	snprintf
	ldr	r6, .L136+140
	ldr	r3, [r7, #0]
	ldr	r5, .L136+144
	str	r3, [sp, #0]
	ldr	r3, [r6, #0]
	ldr	r8, .L136+148
	str	r3, [sp, #4]
	ldr	r3, [r5, #0]
	mov	r1, #16
	str	r3, [sp, #8]
	ldr	r2, .L136+128
	ldr	r3, [r8, #0]
	add	r0, r4, #644
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L136+132
	ldr	r3, [r8, #0]
	add	r0, r4, #660
	bl	snprintf
	add	r0, r4, #664
	mov	r1, #6
	ldr	r2, .L136+132
	ldr	r3, [r7, #0]
	add	r0, r0, #2
	bl	snprintf
	add	r0, r4, #672
	mov	r1, #6
	ldr	r2, .L136+132
	ldr	r3, [r6, #0]
	bl	snprintf
	add	r0, r4, #676
	add	r0, r0, #2
	b	.L119
.L75:
.LBE22:
.LBB23:
	cmp	r3, #0
	beq	.L76
	ldr	r5, [r3, #180]
	cmp	r5, #0
	beq	.L76
	ldr	r4, [r3, #192]
	cmp	r4, #0
	beq	.L76
	ldr	r0, .L136+16
	ldr	r1, .L136+152
	mov	r2, #7
	bl	strncmp
	cmp	r0, #0
	add	r0, r4, #45
	bne	.L79
	ldr	r3, [r5, #4]
	cmp	r3, #0
	addne	r1, r5, #32
	bne	.L118
.L79:
	ldr	r1, .L136+16
.L118:
	mov	r2, #18
	bl	strlcpy
	ldr	r3, .L136+28
	ldr	r2, [r3, #0]
	cmp	r2, #0
	moveq	r2, #1
	streq	r2, [r3, #0]
	ldr	r3, [r3, #0]
	str	r3, [r5, #0]
	ldr	r3, .L136+124
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ldr	r3, .L136+32
	bne	.L82
	ldr	r2, .L136+112
	ldr	r2, [r2, #0]
	cmp	r2, #0
	moveq	r2, #1
	streq	r2, [r3, #0]
.L82:
	ldr	r2, .L136+96
	ldr	r3, [r3, #0]
	ldr	r2, [r2, #0]
	cmp	r3, #0
	str	r3, [r2, #268]
	add	r0, r4, #4
	beq	.L83
	mov	r5, #0
	mov	r1, #17
	ldr	r2, .L136+128
	mov	r3, r5
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	str	r5, [sp, #8]
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L136+132
	mov	r3, r5
	add	r0, r4, #21
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L136+132
	mov	r3, r5
	add	r0, r4, #27
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L136+132
	mov	r3, #4
	add	r0, r4, #33
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L136+132
	mov	r3, r5
	add	r0, r4, #39
	bl	snprintf
	mov	r1, #17
	ldr	r2, .L136+128
	mov	r3, r5
	add	r0, r4, #152
	str	r5, [sp, #0]
	str	r5, [sp, #4]
	str	r5, [sp, #8]
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L136+132
	mov	r3, r5
	add	r0, r4, #169
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L136+132
	mov	r3, r5
	add	r0, r4, #175
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L136+132
	mov	r3, r5
	add	r0, r4, #181
	bl	snprintf
	add	r0, r4, #187
	mov	r1, #6
	ldr	r2, .L136+132
	mov	r3, r5
	b	.L120
.L83:
	ldr	r7, .L136+112
	ldr	r6, .L136+116
	ldr	r3, [r7, #0]
	ldr	r5, .L136+120
	str	r3, [sp, #0]
	ldr	r3, [r6, #0]
	ldr	r8, .L136+124
	str	r3, [sp, #4]
	ldr	r3, [r5, #0]
	mov	r1, #17
	str	r3, [sp, #8]
	ldr	r2, .L136+128
	ldr	r3, [r8, #0]
	bl	snprintf
	ldr	r3, [r8, #0]
	mov	r1, #6
	ldr	r2, .L136+132
	add	r0, r4, #21
	bl	snprintf
	ldr	r3, [r7, #0]
	mov	r1, #6
	ldr	r2, .L136+132
	ldr	r7, .L136+136
	add	r0, r4, #27
	bl	snprintf
	ldr	r3, [r6, #0]
	mov	r1, #6
	ldr	r2, .L136+132
	add	r0, r4, #33
	bl	snprintf
	ldr	r3, [r5, #0]
	mov	r1, #6
	ldr	r2, .L136+132
	add	r0, r4, #39
	bl	snprintf
	ldr	r6, .L136+140
	ldr	r3, [r7, #0]
	ldr	r5, .L136+144
	str	r3, [sp, #0]
	ldr	r3, [r6, #0]
	ldr	r8, .L136+148
	str	r3, [sp, #4]
	ldr	r3, [r5, #0]
	mov	r1, #17
	str	r3, [sp, #8]
	ldr	r2, .L136+128
	ldr	r3, [r8, #0]
	add	r0, r4, #152
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L136+132
	ldr	r3, [r8, #0]
	add	r0, r4, #169
	bl	snprintf
	mov	r1, #6
	ldr	r2, .L136+132
	ldr	r3, [r7, #0]
	add	r0, r4, #175
	bl	snprintf
	add	r0, r4, #181
	mov	r1, #6
	ldr	r2, .L136+132
	ldr	r3, [r6, #0]
	bl	snprintf
	add	r0, r4, #187
.L119:
	ldr	r2, .L136+132
	ldr	r3, [r5, #0]
	mov	r1, #6
.L120:
	bl	snprintf
.L76:
.LBE23:
.LBE20:
	bl	set_wen_programming_struct_from_wifi_guivars
	ldr	r3, .L136+60
	ldr	r2, .L136+44
	ldr	r0, [r3, #0]
	mov	r1, #87
	bl	e_SHARED_start_device_communication
	mov	r2, #1
.L125:
	ldr	r3, .L136+56
	str	r2, [r3, #0]
	b	.L41
.L74:
	bl	bad_key_beep
	mov	r3, #2
	ldr	r2, .L136+80
	str	r3, [sp, #12]
	mov	r3, #11
	str	r3, [sp, #16]
	mov	r3, #70
	str	r3, [sp, #20]
	ldr	r3, .L136+84
	str	r2, [sp, #28]
	ldr	r2, .L136+88
	str	r3, [sp, #32]
	mov	r3, #1
	str	r3, [r2, #0]
	str	r3, [sp, #36]
	ldr	r3, .L136+92
	mov	r2, #13
	str	r2, [r3, #0]
.L128:
	add	r0, sp, #12
	bl	Change_Screen
	b	.L41
.L73:
	ldr	r0, .L136+156
	bl	DIALOG_draw_ok_dialog
	b	.L112
.L70:
	ldr	r4, .L136+44
	ldr	r3, [r4, #0]
	cmp	r3, #0
	bne	.L112
	bl	good_key_beep
	ldrsh	r2, [r5, #0]
	ldr	r3, .L136+100
	mov	r1, #82
	str	r2, [r3, #0]
	ldr	r3, .L136+60
	mov	r2, r4
	ldr	r0, [r3, #0]
.L124:
	bl	e_SHARED_start_device_communication
	b	.L41
.L58:
	ldr	r3, .L136+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #10
	ldrls	pc, [pc, r3, asl #2]
	b	.L85
.L96:
	.word	.L86
	.word	.L85
	.word	.L87
	.word	.L88
	.word	.L89
	.word	.L90
	.word	.L91
	.word	.L92
	.word	.L93
	.word	.L94
	.word	.L95
.L86:
	ldr	r0, .L136+32
	bl	process_bool
	mov	r0, #0
	bl	Redraw_Screen
	b	.L97
.L87:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L136+124
	b	.L121
.L88:
	ldr	r1, .L136+112
	mov	r3, #1
	mov	r0, r4
	str	r3, [sp, #0]
	str	r3, [sp, #4]
.L121:
	mov	r2, #0
	mov	r3, #255
.L122:
	bl	process_uns32
	b	.L97
.L89:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L136+116
	b	.L121
.L90:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L136+120
	b	.L121
.L91:
	mov	r3, #1
	mov	r2, #0
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	mov	r0, r4
	ldr	r1, .L136+28
	mov	r3, #24
	b	.L122
.L92:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L136+148
	b	.L121
.L93:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L136+136
	b	.L121
.L94:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L136+140
	b	.L121
.L95:
	mov	r3, #1
	str	r3, [sp, #0]
	str	r3, [sp, #4]
	mov	r0, r4
	ldr	r1, .L136+144
	b	.L121
.L85:
	bl	bad_key_beep
.L97:
	bl	Refresh_Screen
	b	.L41
.L56:
	ldr	r3, .L136+12
	ldrsh	r3, [r3, #0]
	sub	r2, r3, #2
	cmp	r2, #11
	ldrls	pc, [pc, r2, asl #2]
	b	.L98
.L104:
	.word	.L99
	.word	.L99
	.word	.L99
	.word	.L99
	.word	.L114
	.word	.L109
	.word	.L109
	.word	.L109
	.word	.L109
	.word	.L98
	.word	.L102
	.word	.L130
.L99:
	ldr	r2, .L136+100
	mov	r0, #0
	str	r3, [r2, #0]
	b	.L127
.L102:
	ldr	r3, .L136+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	beq	.L130
.L98:
	mov	r0, #1
	b	.L129
.L53:
	mov	r0, r4
.L129:
	bl	CURSOR_Up
	b	.L41
.L52:
	ldr	r3, .L136+12
	ldrsh	r3, [r3, #0]
	cmp	r3, #12
	ldrls	pc, [pc, r3, asl #2]
	b	.L55
.L113:
	.word	.L108
	.word	.L55
	.word	.L109
	.word	.L109
	.word	.L109
	.word	.L109
	.word	.L110
	.word	.L111
	.word	.L111
	.word	.L111
	.word	.L111
	.word	.L55
	.word	.L112
.L108:
	ldr	r3, .L136+32
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L55
.L114:
	ldr	r3, .L136+100
	ldr	r2, [r3, #0]
	sub	r2, r2, #2
	cmp	r2, #3
	movhi	r2, #2
	bhi	.L131
	b	.L116
.L109:
	ldr	r2, .L136+100
	mov	r0, #6
	str	r3, [r2, #0]
	b	.L127
.L110:
	ldr	r3, .L136+100
	ldr	r2, [r3, #0]
	sub	r2, r2, #7
	cmp	r2, #3
	bls	.L116
	mov	r2, #7
.L131:
	str	r2, [r3, #0]
.L116:
	ldr	r0, [r3, #0]
	b	.L127
.L111:
	ldr	r2, .L136+100
	str	r3, [r2, #0]
.L130:
	mov	r0, #11
.L127:
	mov	r1, #1
	bl	CURSOR_Select
	b	.L41
.L112:
	bl	bad_key_beep
	b	.L41
.L55:
	mov	r0, #1
	bl	CURSOR_Down
	b	.L41
.L57:
	ldr	r3, .L136+160
	mov	r2, #11
	str	r2, [r3, #0]
	ldr	r3, .L136+88
	mov	r2, #0
	mov	r0, r4
	str	r2, [r3, #0]
	mov	r1, r5
	bl	KEY_process_global_keys
	mov	r0, #1
	bl	COMM_OPTIONS_draw_dialog
	b	.L41
.L51:
	mov	r0, r4
	mov	r1, r5
	bl	KEY_process_global_keys
.L41:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, r8, pc}
.L137:
	.align	2
.L136:
	.word	GuiLib_CurStructureNdx
	.word	609
	.word	726
	.word	GuiLib_ActiveCursorFieldNo
	.word	GuiVar_ENDHCPName
	.word	GuiVar_GroupName
	.word	FDTO_WEN_PROGRAMMING_redraw_screen
	.word	GuiVar_ENNetmask
	.word	GuiVar_ENObtainIPAutomatically
	.word	36867
	.word	36870
	.word	.LANCHOR4
	.word	FDTO_WEN_PROGRAMMING_process_device_exchange_key
	.word	36868
	.word	.LANCHOR5
	.word	.LANCHOR1
	.word	36869
	.word	GuiVar_CommOptionDeviceExchangeResult
	.word	FDTO_e_SHARED_show_obtain_ip_automatically_dropdown
	.word	FDTO_e_SHARED_show_subnet_dropdown
	.word	WEN_PROGRAMMING_process_wifi_settings
	.word	FDTO_WEN_PROGRAMMING_draw_wifi_settings
	.word	.LANCHOR0
	.word	.LANCHOR2
	.word	dev_state
	.word	.LANCHOR3
	.word	GuiVar_WENRadioType
	.word	.LC0
	.word	GuiVar_ENIPAddress_2
	.word	GuiVar_ENIPAddress_3
	.word	GuiVar_ENIPAddress_4
	.word	GuiVar_ENIPAddress_1
	.word	.LC6
	.word	.LC7
	.word	GuiVar_ENGateway_2
	.word	GuiVar_ENGateway_3
	.word	GuiVar_ENGateway_4
	.word	GuiVar_ENGateway_1
	.word	.LC1
	.word	589
	.word	GuiVar_MenuScreenToShow
.LFE7:
	.size	WEN_PROGRAMMING_process_screen, .-WEN_PROGRAMMING_process_screen
	.section	.bss.g_WEN_PROGRAMMING_read_after_write_pending,"aw",%nobits
	.align	2
	.set	.LANCHOR5,. + 0
	.type	g_WEN_PROGRAMMING_read_after_write_pending, %object
	.size	g_WEN_PROGRAMMING_read_after_write_pending, 4
g_WEN_PROGRAMMING_read_after_write_pending:
	.space	4
	.section	.bss.g_WEN_PROGRAMMING_port,"aw",%nobits
	.align	2
	.set	.LANCHOR1,. + 0
	.type	g_WEN_PROGRAMMING_port, %object
	.size	g_WEN_PROGRAMMING_port, 4
g_WEN_PROGRAMMING_port:
	.space	4
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"Not Set\000"
.LC1:
	.ascii	"not set\000"
.LC2:
	.ascii	"\015\012\000"
.LC3:
	.ascii	"unknown\000"
.LC4:
	.ascii	"\000"
.LC5:
	.ascii	"0.0.0.0\000"
.LC6:
	.ascii	"%d.%d.%d.%d\000"
.LC7:
	.ascii	"%d\000"
	.section	.bss.g_WEN_PROGRAMMING_return_to_cp_after_wifi,"aw",%nobits
	.align	2
	.set	.LANCHOR2,. + 0
	.type	g_WEN_PROGRAMMING_return_to_cp_after_wifi, %object
	.size	g_WEN_PROGRAMMING_return_to_cp_after_wifi, 4
g_WEN_PROGRAMMING_return_to_cp_after_wifi:
	.space	4
	.section	.bss.g_WEN_PROGRAMMING_previous_cursor_pos,"aw",%nobits
	.align	2
	.set	.LANCHOR3,. + 0
	.type	g_WEN_PROGRAMMING_previous_cursor_pos, %object
	.size	g_WEN_PROGRAMMING_previous_cursor_pos, 4
g_WEN_PROGRAMMING_previous_cursor_pos:
	.space	4
	.section	.bss.g_WEN_PROGRAMMING_PW_querying_device,"aw",%nobits
	.align	2
	.set	.LANCHOR4,. + 0
	.type	g_WEN_PROGRAMMING_PW_querying_device, %object
	.size	g_WEN_PROGRAMMING_PW_querying_device, 4
g_WEN_PROGRAMMING_PW_querying_device:
	.space	4
	.section	.bss.g_WEN_PROGRAMMING_editing_wifi_settings,"aw",%nobits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	g_WEN_PROGRAMMING_editing_wifi_settings, %object
	.size	g_WEN_PROGRAMMING_editing_wifi_settings, 4
g_WEN_PROGRAMMING_editing_wifi_settings:
	.space	4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI1-.LFB6
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI2-.LFB4
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI3-.LFB7
	.byte	0xe
	.uleb128 0x18
	.byte	0x8e
	.uleb128 0x1
	.byte	0x88
	.uleb128 0x2
	.byte	0x87
	.uleb128 0x3
	.byte	0x86
	.uleb128 0x4
	.byte	0x85
	.uleb128 0x5
	.byte	0x84
	.uleb128 0x6
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0x48
	.align	2
.LEFDE8:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/screens/e_wen_programming.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0xa3
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF8
	.byte	0x1
	.4byte	.LASF9
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x1
	.2byte	0x1dc
	.byte	0x1
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x1
	.2byte	0x1f9
	.byte	0x1
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x101
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x2
	.4byte	.LASF2
	.byte	0x1
	.2byte	0x1b5
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x243
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST1
	.uleb128 0x5
	.4byte	0x2a
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST2
	.uleb128 0x6
	.4byte	.LASF4
	.byte	0x1
	.2byte	0x236
	.4byte	.LFB5
	.4byte	.LFE5
	.byte	0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x7
	.4byte	.LASF5
	.byte	0x1
	.byte	0x56
	.byte	0x1
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x27f
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB6
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI1
	.4byte	.LFE6
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB4
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB7
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 24
	.4byte	.LCFI4
	.4byte	.LFE7
	.2byte	0x3
	.byte	0x7d
	.sleb128 72
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF5:
	.ascii	"set_wen_programming_struct_from_guivars\000"
.LASF7:
	.ascii	"WEN_PROGRAMMING_process_screen\000"
.LASF9:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/scre"
	.ascii	"ens/e_wen_programming.c\000"
.LASF3:
	.ascii	"get_guivars_from_wen_programming_struct\000"
.LASF6:
	.ascii	"FDTO_WEN_PROGRAMMING_draw_screen\000"
.LASF0:
	.ascii	"g_WEN_PROGRAMMING_handle_redraw_cursor\000"
.LASF4:
	.ascii	"FDTO_WEN_PROGRAMMING_redraw_screen\000"
.LASF2:
	.ascii	"g_WEN_PROGRAMMING_initialize_guivars\000"
.LASF8:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"FDTO_WEN_PROGRAMMING_process_device_exchange_key\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
