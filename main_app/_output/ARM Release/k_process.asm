	.file	"k_process.c"
	.text
.Ltext0:
	.section	.text.KEY_process_BACK_from_editing_screen,"ax",%progbits
	.align	2
	.global	KEY_process_BACK_from_editing_screen
	.type	KEY_process_BACK_from_editing_screen, %function
KEY_process_BACK_from_editing_screen:
.LFB1:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
.LCFI0:
	sub	sp, sp, #36
.LCFI1:
	mov	r4, r0
	bl	good_key_beep
	mov	r3, #1
	mov	r0, sp
	str	r3, [sp, #0]
	str	r4, [sp, #20]
	bl	Display_Post_Command
	add	sp, sp, #36
	ldmfd	sp!, {r4, pc}
.LFE1:
	.size	KEY_process_BACK_from_editing_screen, .-KEY_process_BACK_from_editing_screen
	.section	.text.KEY_process_ENG_SPA,"ax",%progbits
	.align	2
	.global	KEY_process_ENG_SPA
	.type	KEY_process_ENG_SPA, %function
KEY_process_ENG_SPA:
.LFB2:
	@ args = 0, pretend = 0, frame = 36
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L8
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	ldr	r3, [r3, #0]
	sub	sp, sp, #36
.LCFI3:
	cmp	r3, #0
	bne	.L3
	bl	DIALOG_a_dialog_is_visible
	cmp	r0, #0
	bne	.L3
	ldr	r3, .L8+4
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L3
	ldr	r3, .L8+8
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L3
	ldr	r3, .L8+12
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L3
	ldr	r3, .L8+16
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L3
	ldr	r3, .L8+20
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L3
	ldr	r3, .L8+24
	ldr	r5, [r3, #0]
	cmp	r5, #0
	bne	.L3
	ldr	r4, .L8+28
	bl	good_key_beep
	ldrsh	r3, [r4, #0]
	cmp	r3, #0
	moveq	r0, #1
	movne	r0, r5
	bl	GuiLib_SetLanguage
	ldrsh	r0, [r4, #0]
	bl	CONTRAST_AND_SPEAKER_VOL_set_screen_language
	mov	r0, #15
	mov	r1, #10
	bl	FLASH_STORAGE_initiate_a_time_delayed_file_save_seconds
	ldr	r3, .L8+32
	mov	r2, #36
	ldr	r3, [r3, #0]
	ldr	r1, .L8+36
	mul	r2, r3, r2
	mov	r0, sp
	add	r3, r1, r2
	ldr	r2, [r1, r2]
	str	r2, [sp, #0]
	ldr	r2, [r3, #4]
	str	r2, [sp, #4]
	ldr	r2, [r3, #8]
	str	r2, [sp, #8]
	ldr	r2, [r3, #20]
	str	r2, [sp, #20]
	ldr	r2, [r3, #28]
	str	r2, [sp, #28]
	ldr	r2, [r3, #16]
	ldr	r3, [r3, #12]
	str	r2, [sp, #16]
	str	r3, [sp, #12]
	mov	r3, #0
	str	r3, [sp, #24]
	bl	Display_Post_Command
	b	.L2
.L3:
	bl	bad_key_beep
.L2:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, pc}
.L9:
	.align	2
.L8:
	.word	g_COPY_DIALOG_visible
	.word	g_COMM_OPTIONS_dialog_visible
	.word	GuiVar_DeviceExchangeSyncingRadios
	.word	g_LIVE_SCREENS_dialog_visible
	.word	g_TECH_SUPPORT_dialog_visible
	.word	g_TWO_WIRE_dialog_visible
	.word	g_TWO_WIRE_DEBUG_dialog_visible
	.word	GuiLib_LanguageIndex
	.word	screen_history_index
	.word	ScreenHistory
.LFE2:
	.size	KEY_process_ENG_SPA, .-KEY_process_ENG_SPA
	.section	.text.KEY_process_global_keys,"ax",%progbits
	.align	2
	.global	KEY_process_global_keys
	.type	KEY_process_global_keys, %function
KEY_process_global_keys:
.LFB3:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #67
	stmfd	sp!, {r4, lr}
.LCFI4:
	beq	.L13
	cmp	r0, #83
	beq	.L14
	cmp	r0, #48
	bne	.L18
	b	.L19
.L13:
.LBB4:
	ldr	r4, .L20
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L18
	bl	good_key_beep
	bl	SCROLL_BOX_close_all_scroll_boxes
	bl	KEY_PROCESS_free_memory_allocated_for_screen_before_changing_screen
	ldr	r3, [r4, #0]
	sub	r3, r3, #1
	cmp	r3, #0
	str	r3, [r4, #0]
	beq	.L16
	ldr	r2, .L20+4
	mov	r1, #36
	mla	r2, r1, r3, r2
	ldr	r1, [r2, #32]
	ldr	r2, .L20+8
	str	r1, [r2, #0]
.L16:
	ldr	r2, .L20+4
	mov	r0, #36
	mla	r0, r3, r0, r2
.LBE4:
	ldmfd	sp!, {r4, lr}
.LBB5:
	b	Display_Post_Command
.L14:
.LBE5:
	ldmfd	sp!, {r4, lr}
	b	KEY_process_ENG_SPA
.L19:
	ldr	r3, .L20+12
	ldr	r3, [r3, #8]
	cmp	r3, #0
	beq	.L18
	bl	good_key_beep
	mov	r0, #0
	bl	IRRI_COMM_process_stop_command
	ldr	r3, .L20+16
	mov	r2, #1
	str	r2, [r3, #0]
	ldr	r2, .L20+20
	mov	r3, #0
	str	r3, [r2, #0]
	ldr	r0, .L20+24
	ldr	r2, .L20+28
	str	r3, [r2, #0]
	ldmfd	sp!, {r4, lr}
	b	DIALOG_draw_ok_dialog
.L18:
	ldmfd	sp!, {r4, lr}
	b	bad_key_beep
.L21:
	.align	2
.L20:
	.word	screen_history_index
	.word	ScreenHistory
	.word	GuiVar_MenuScreenToShow
	.word	irri_irri
	.word	GuiVar_StopKeyPending
	.word	GuiVar_ScrollBoxHorizScrollPos
	.word	635
	.word	GuiVar_ScrollBoxHorizScrollMarker
.LFE3:
	.size	KEY_process_global_keys, .-KEY_process_global_keys
	.section	.text.Key_Processing_Task,"ax",%progbits
	.align	2
	.global	Key_Processing_Task
	.type	Key_Processing_Task, %function
Key_Processing_Task:
.LFB4:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L37
	stmfd	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, lr}
.LCFI5:
	ldr	r5, .L37+4
	mov	r2, #0
	mov	r1, #2
	stmia	r3, {r1, r2}
	ldr	r1, .L37+8
	ldr	r4, .L37+12
	str	r1, [r3, #20]
	mov	r1, #1
	str	r1, [r3, #24]
	ldr	r1, .L37+16
	str	r2, [r3, #32]
	str	r1, [r3, #16]
	ldr	r7, .L37+20
	ldr	r3, .L37+24
	ldr	r6, .L37+28
	rsb	r5, r5, r0
	mov	r5, r5, lsr #5
	str	r2, [r4, #0]
	str	r2, [r3, #0]
.L34:
	ldr	r0, [r6, #0]
	mov	r1, sp
	mov	r2, #100
	mov	r3, #0
	bl	xQueueGenericReceive
	cmp	r0, #1
	bne	.L23
.LBB6:
	ldr	r1, [sp, #4]
	cmp	r1, #2000
	bcc	.L24
	ldr	ip, [sp, #0]
	ldr	r0, .L37+32
	mov	r2, #0
	mov	r3, r2
.L26:
	ldr	lr, [r2, r0]
	cmp	lr, ip
	ldreq	r2, .L37+32
	moveq	r0, #12
	mlaeq	r3, r0, r3, r2
	streq	r1, [r3, #4]
	beq	.L24
.L25:
	add	r3, r3, #1
	cmp	r3, #13
	add	r2, r2, #12
	bne	.L26
.L24:
	ldr	r3, .L37+36
	ldr	r2, [r3, #12]
	tst	r2, #16384
	bne	.L27
	mov	r2, #16384
	str	r2, [r3, #4]
	bl	CONTRAST_AND_SPEAKER_VOL_get_speaker_volume
	mov	r1, r0
	ldr	r0, .L37+40
	bl	postContrast_or_Volume_Event
	b	.L35
.L27:
	ldr	r3, [r4, #0]
	ldr	r2, .L37
	mov	r1, #36
	mla	r3, r1, r3, r2
	ldr	r3, [r3, #16]
	cmp	r3, #0
	beq	.L35
	ldr	r2, .L37+44
	ldmia	sp, {r0-r1}
	ldr	r2, [r2, #0]
	cmp	r2, #1
	bne	.L29
	bl	DIALOG_process_ok_dialog
	b	.L35
.L29:
	ldr	r2, .L37+48
	ldr	r2, [r2, #0]
	cmp	r2, #1
	bne	.L30
	bl	COPY_DIALOG_process_dialog
	b	.L35
.L30:
	blx	r3
	b	.L35
.L23:
.LBE6:
	ldr	r3, .L37+52
	cmp	r7, r3
	bls	.L28
	sub	r7, r7, #500
	cmp	r7, r3
	bhi	.L28
	ldr	r3, .L37+36
	mov	r2, #16384
	str	r2, [r3, #8]
	ldr	r0, .L37+40
	mov	r1, #0
	bl	postContrast_or_Volume_Event
	mov	r0, #20480
	bl	RADIO_TEST_post_event
	ldr	r3, .L37+56
	ldr	r3, [r3, #0]
	cmp	r3, #0
	bne	.L28
	bl	speaker_disable
	mov	sl, #67
	ldr	r8, .L37
	b	.L31
.L32:
	bl	DIALOG_close_all_dialogs
	mov	r3, #0
	str	r3, [sp, #4]
	ldr	r3, [r4, #0]
	mov	r2, #36
	mla	r3, r2, r3, r8
	str	sl, [sp, #0]
	ldr	r3, [r3, #16]
	ldmia	sp, {r0-r1}
	blx	r3
.L31:
	ldr	r9, [r4, #0]
	cmp	r9, #0
	bne	.L32
	bl	DIALOG_close_all_dialogs
	ldr	r3, .L37+24
	str	r9, [r3, #0]
	ldr	r3, .L37+60
	ldr	r2, [r3, #0]
	cmp	r2, #0
	strne	r9, [r3, #0]
	bl	speaker_enable
	ldr	r2, [r4, #0]
	ldr	r3, .L37
	mov	r0, #36
	mla	r0, r2, r0, r3
	bl	Display_Post_Command
	b	.L28
.L35:
.LBB7:
	ldr	r7, .L37+64
.L28:
.LBE7:
	bl	xTaskGetTickCount
	ldr	r3, .L37+68
	str	r0, [r3, r5, asl #2]
	b	.L34
.L38:
	.align	2
.L37:
	.word	ScreenHistory
	.word	Task_Table
	.word	FDTO_MAIN_MENU_draw_menu
	.word	screen_history_index
	.word	MAIN_MENU_process_menu
	.word	10000
	.word	GuiVar_MenuScreenToShow
	.word	Key_To_Process_Queue
	.word	.LANCHOR0
	.word	1073905664
	.word	13107
	.word	g_DIALOG_ok_dialog_visible
	.word	g_COPY_DIALOG_visible
	.word	499
	.word	GuiVar_CodeDownloadState
	.word	GuiVar_StatusShowLiveScreens
	.word	900000
	.word	task_last_execution_stamp
.LFE4:
	.size	Key_Processing_Task, .-Key_Processing_Task
	.global	stuck_key_data
	.section	.data.stuck_key_data,"aw",%progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	stuck_key_data, %object
	.size	stuck_key_data, 156
stuck_key_data:
	.word	4
	.word	0
	.word	.LC0
	.word	0
	.word	0
	.word	.LC1
	.word	1
	.word	0
	.word	.LC2
	.word	3
	.word	0
	.word	.LC3
	.word	2
	.word	0
	.word	.LC4
	.word	20
	.word	0
	.word	.LC5
	.word	16
	.word	0
	.word	.LC6
	.word	84
	.word	0
	.word	.LC7
	.word	80
	.word	0
	.word	.LC8
	.word	48
	.word	0
	.word	.LC9
	.word	81
	.word	0
	.word	.LC10
	.word	83
	.word	0
	.word	.LC11
	.word	67
	.word	0
	.word	.LC12
	.section	.rodata.str1.1,"aMS",%progbits,1
.LC0:
	.ascii	"UP KEY\000"
.LC1:
	.ascii	"DOWN KEY\000"
.LC2:
	.ascii	"LEFT KEY\000"
.LC3:
	.ascii	"RIGHT KEY\000"
.LC4:
	.ascii	"SELECT KEY\000"
.LC5:
	.ascii	"NEXT KEY\000"
.LC6:
	.ascii	"PREV KEY\000"
.LC7:
	.ascii	"PLUS KEY\000"
.LC8:
	.ascii	"MINUS KEY\000"
.LC9:
	.ascii	"STOP KEY\000"
.LC10:
	.ascii	"HELP KEY\000"
.LC11:
	.ascii	"ENG_SPA KEY\000"
.LC12:
	.ascii	"BACK KEY\000"
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xe
	.uleb128 0x2c
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI2-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xe
	.uleb128 0x30
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI4-.LFB3
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI5-.LFB4
	.byte	0xe
	.uleb128 0x28
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8a
	.uleb128 0x2
	.byte	0x89
	.uleb128 0x3
	.byte	0x88
	.uleb128 0x4
	.byte	0x87
	.uleb128 0x5
	.byte	0x86
	.uleb128 0x6
	.byte	0x85
	.uleb128 0x7
	.byte	0x84
	.uleb128 0x8
	.byte	0x81
	.uleb128 0x9
	.byte	0x80
	.uleb128 0xa
	.align	2
.LEFDE6:
	.text
.Letext0:
	.file 1 "C:/CS3000/cs3_branches/chain_sync/main_app/src/key_scanner/k_process.c"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x77
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF4
	.byte	0x1
	.4byte	.LASF5
	.4byte	0
	.4byte	0
	.4byte	.Ldebug_ranges0+0
	.uleb128 0x2
	.4byte	.LASF6
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x86
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x9d
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST1
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xde
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST2
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.2byte	0x130
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST3
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB1
	.4byte	.LCFI0
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI0
	.4byte	.LCFI1
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	.LCFI1
	.4byte	.LFE1
	.2byte	0x2
	.byte	0x7d
	.sleb128 44
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LFB2
	.4byte	.LCFI2
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI2
	.4byte	.LCFI3
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	.LCFI3
	.4byte	.LFE2
	.2byte	0x2
	.byte	0x7d
	.sleb128 48
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB3
	.4byte	.LCFI4
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI4
	.4byte	.LFE3
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LFB4
	.4byte	.LCFI5
	.2byte	0x2
	.byte	0x7d
	.sleb128 0
	.4byte	.LCFI5
	.4byte	.LFE4
	.2byte	0x2
	.byte	0x7d
	.sleb128 40
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF2:
	.ascii	"KEY_process_global_keys\000"
.LASF0:
	.ascii	"KEY_process_BACK_from_editing_screen\000"
.LASF6:
	.ascii	"KEY_process_BACK\000"
.LASF3:
	.ascii	"Key_Processing_Task\000"
.LASF5:
	.ascii	"C:/CS3000/cs3_branches/chain_sync/main_app/src/key_"
	.ascii	"scanner/k_process.c\000"
.LASF4:
	.ascii	"GNU C 4.6.2 20110921 (release) [ARM/embedded-4_6-br"
	.ascii	"anch revision 182083]\000"
.LASF1:
	.ascii	"KEY_process_ENG_SPA\000"
	.ident	"GCC: (GNU) 4.6.2 20110921 (release) [ARM/embedded-4_6-branch revision 182083]"
